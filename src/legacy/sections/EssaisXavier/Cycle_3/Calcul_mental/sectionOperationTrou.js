import { j3pBarre, j3pDiv, j3pElement, j3pFocus, j3pGetRandomInt, j3pRestriction, j3pValeurde } from 'src/legacy/core/functions'
import ValidationZones from 'src/legacy/outils/calculatrice/ValidationZones'
import { j3pAffiche, j3pDesactive } from 'src/lib/mathquill/functions'
import textesGeneriques from 'src/lib/core/textes'

const { cBien, cFaux, essaieEncore, regardeCorrection, tempsDepasse } = textesGeneriques

/*
        Auteur  Xavier JEROME
        Date    17/07/2018
        Opération à trou pour réviser les tables d’addition et de multiplication
 */

export const params = {
  outils: ['mathquill'],
  /**
   * Ce qui est paramétrable dans l’éditeur de graphe
   * @type {LegacySectionParamsParametres}
   */
  parametres: [
    ['indication', '', 'string', 'Indication en bas à gauche'], // Texte que l’élève peut faire apparaître en bas de l’écran
    ['nbrepetitionsmax', 10, 'entier', 'Nombre de répétitions maximum de la section.<BR>Il peut être inférieur si on répond juste à toutes les questions'], // Obligatoire: nombre de questions posées pendant l’exercice
    ['nbchances', 2, 'entier', 'Nombre d’essais par répétition'], // Nombre d’essais autorisés avant de déclarer un échec et de passer à la question
    ['limite', 0, 'entier', 'Temps disponible par répétition, en secondes'], // S’il n’est pas vide, le temps de réponse sera limité
    ['s_operation', 'fois', 'string', '"fois" pour multiplication et "plus" pour addition'], //  Opération à réviser
    ['s_facteur1', '2-5', 'string', 'Nombres entiers possibles pour la première opérande<br>Exemple: "2-5;10;100" ce qui signifie de 2 à 5, 10 et 100'],
    ['s_facteur2', '2-9', 'string', 'Nombres entiers possibles pour l' + 'opérande à chercher<br>Exemple: "2-5;10;100" ce qui signifie de 2 à 5, 10 et 100'],
    ['n_aide', 1, 'entier', '0 pour aucune aide<BR>1 pour une explication des erreurs dans le cas de la multiplication.'] // Niveau d’aide autorisé
  ],
  pe: [
    { pe_1: 'Notion totalement maitrisée.' },
    { pe_2: 'Notion comprise.' },
    { pe_3: 'Notion fragile.' },
    { pe_4: 'Notion non comprise.' }
  ]
}

const cssPrefix = 'opTrou'
const textes = {
  phraseTitre: 'Opérations à trou.', //  Titre de la section
  phraseNbChances: 'Il te reste £e essai£p.', //  Ne pas supprimer
  phraseEnonceFois: '£a$\\times$&1& = £r',
  phraseEnoncePlus: '£a + &1& = £r',
  phraseSolution1: '£a$\\times$£b = £r',
  phraseSolution2: '£a + £b = £r',
  Aide01: 'Attention: £a$\\times$£b est égal à £r ',
  Aide02: 'Attention: £a + £b  est égal à £r '
}

/**
 * section OperationTrou
 * @this {Parcours}
 */
export default function main () {
  const me = this
  const stor = this.storage

  let n, i, j

  function _Donnees () {
    /*      "presentation1"   6 cadres complets
        "presentation1bis"   manque le cadre indication en bas à droite
        "presentation2"   manque le cadre réponse à droite
        "presentation3"   un seul grand cadre en plus des deux entêtes
*/
    this.structure = 'presentation1'

    // Si indication != "" alors un lien "Indication" s’affiche dans la zone Inférieure Gauche (this.zones.IG
    // une indication<br>de deux lignes...";
    // Cette indication peut être changée par l’utilisateur dans la page paramètres
    this.indication = 'Avant de valider, vérifie que l’opération écrite est juste.' //  N’est utilisé que si le paramètre passé en entrée est vide
    // pour le cas de presentation1bis
    // En effet pas d’indication possible pour cette présentation
    // this.surchargeindication = false;

    this.video = [] // ["tableur1,Titre Video 1,Video 1","lebouquet,Titre Video2,Video 2"]

    // Nombre de répétitions de l’exercice
    this.nbrepetitionsmax = 10 // N’est utilisé que si le paramètre passé en entrée est vide

    this.nbetapes = 1 // Si chaque question est divisée en plusieurs étapes

    // Nombre de chances dont dispose l’élève pour répondre à la question
    this.nbchances = 2
    // Par défaut, pas de temps limite.
    this.limite = 0 //  N’est utilisé que si le paramètre passé en entrée est vide

    this.s_operation = 'fois'
    this.s_facteur1 = '2-5;9'
    this.s_facteur2 = '2-9'

    this.n_aide = 1
    /*
      Phrase d’état renvoyée par la section
    */
    this.pe_1 = 'Notion totalement maitrisée'
    this.pe_2 = 'Notion comprise'
    this.pe_3 = 'Notion fragile.'
    this.pe_4 = 'Notion non comprise.'

    /// ////////////////////////////////////////////////////////////////////////////////////////
    //
    //               Ajouter ici toutes les constantes nécessaires à la section
    //
    /// ////////////////////////////////////////////////////////////////////////////////////////
  }

  /// ////////////////////////////////////////////////////////////////////////////////////////
  //
  //              Ajouter ici toutes les fonctions nécessaires à la section
  //
  /// ////////////////////////////////////////////////////////////////////////////////////////

  //  Transforme une chaine d’intervalles en un tableau de valeurs
  function intervalles_to_tab (intervalles) {
    const valeurs = []
    const tableau = intervalles.match(/\d*-\d*|\d*/g)
    for (i = 0; i < tableau.length; i++) {
      if (tableau[i].length != 0) {
        if (tableau[i].indexOf('-') == -1) { valeurs.push(parseFloat(tableau[i])) } else {
          for (j = parseFloat(tableau[i].substring(0, tableau[i].indexOf('-'))); j <= tableau[i].substring(tableau[i].indexOf('-') + 1); j++) { valeurs.push(j) }
        }
      }
    }
    return valeurs
  } // intervalles_to_tab

  //   Affiche la correction sous l’énoncé
  function affiche_correction (fin) {
    // fonction appelée pour afficher la correction (que la réponse soit bonne ou fausse

    j3pElement(cssPrefix + 'explication1').innerHTML = '' //  Pour effacer  un éventuel précédent affichage
    if (!reponse.bonneReponse) {
      if (fin) { //  On affiche la bonne réponse s’il n’a pas trouvé au dernier essai
        j3pAffiche(cssPrefix + 'zone_consigne1', cssPrefix + 'corr11', '\n',
          { style: me.styles.grand.correction })
        j3pAffiche(cssPrefix + 'zone_consigne1', cssPrefix + 'corr12', stor.facteur2.toString(),
          { style: me.styles.grand.correction })
        j3pElement(cssPrefix + 'corr12').style.paddingLeft = '60px'
      } // fin

      const rep_eleve = j3pValeurde(cssPrefix + 'consigne1inputmq1')
      if ((me.donneesSection.n_aide >= 1) && reponse.aRepondu) {
        j3pAffiche(cssPrefix + 'explication2', cssPrefix + 'corr21', (me.donneesSection.s_operation == 'plus' ? textes.Aide02 : textes.Aide01),
          {
            style: me.styles.grand.correction,
            a: stor.facteur1,
            b: rep_eleve,
            r: (me.donneesSection.s_operation == 'plus' ? parseFloat(stor.facteur1) + parseFloat(rep_eleve) : stor.facteur1 * rep_eleve)
          })
      } // Aide
    } // Bonne réponse
  } //  affiche_correction

  // Affiche le nombre de tentatives qu’il reste s’il y a plusieurs chances
  function affiche_chances () {
    j3pElement(cssPrefix + 'zone_chances').innerHTML = ''
    if (me.donneesSection.nbchances > 1) {
      j3pAffiche(cssPrefix + 'zone_chances', cssPrefix + 'chances', textes.phraseNbChances,
        {
          e: me.donneesSection.nbchances - me.essaiCourant,
          p: (me.donneesSection.nbchances - me.essaiCourant > 1 ? 's' : '')
        })
    } // Pour le pluriel
  }

  /// ////////////////////////////////////////////////////////////////////////////////////////
  //
  //              Déroulement de la section Enonce, Correction, navigation
  //
  /// ////////////////////////////////////////////////////////////////////////////////////////

  //  Permet de désigner la section
  const la_section = me.SectionOperationTrou // Modification du nom n°4

  switch (this.etat) {
    /// ////////////////////////////////////////////////////////////////////////////////////////
    //
    //            Phase énoncé
    //
    /// ////////////////////////////////////////////////////////////////////////////////////////

    case 'enonce': {
      if (this.debutDeLaSection) {
        // Construit les données de la section
        this.donneesSection = new _Donnees()

        this.surcharge()
        // Construction de la page
        this.construitStructurePage(this.donneesSection.structure)
        this.donneesSection.nbrepetitions = this.donneesSection.nbrepetitionsmax // C’est cette valeur qui est affichée dans l’entête
        this.donneesSection.nbitems = this.donneesSection.nbetapes * this.donneesSection.nbrepetitionsmax // Initialisation à ne pas modifier
        this.score = 0
        this.afficheTitre(textes.phraseTitre)
        // Affiche l’indication en bas à gauche
        if (this.donneesSection.indication) this.indication(this.zones.IG, this.donneesSection.indication)
        /*
                 Par convention,`
                `   this.typederreurs[0] = nombre de bonnes réponses
                    this.typederreurs[1] = nombre de fois où l’élève a utilisé une chance supplémentaire
                    this.typederreurs[2] = nombre de mauvaises réponses
                    this.typederreurs[10] = nombre de fois où l’élève a eu faux à cause d’un temps limite
                    LES AUTRES peuvent être utilisées par la détermination de la pe dans le cas d’une section qualitative
                 */
        this.typederreurs = [0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0]

        //  Paramètres et variables particuliers à la section
        // Transforme l’intervalle pour qu’il corresponde au codage du nombre
        stor.t_facteur1 = intervalles_to_tab(me.donneesSection.s_facteur1)
        stor.t_facteur2 = intervalles_to_tab(me.donneesSection.s_facteur2)

        //  Tableaux permettant de ne pas poser 2 fois la même question
        stor.questions = [] // Contient les question qui n’ont pas encore été posées
        for (i = 0; i <= stor.t_facteur1.length - 1; i++) {
          for (j = 0; j <= stor.t_facteur2.length - 1; j++) { stor.questions.push([stor.t_facteur1[i], stor.t_facteur2[j]]) }
        }

        stor.erreurs = [] // Contient questions qui ont provoqué une erreur pour les représenter en priorité
      } else {
        // A chaque répétition, on nettoie la scène
        this.videLesZones()
      } // Code exécuté à chaque nouvelle question: Partie à ne pas modifier

      //  Partie exécutée à chaque essai

      /// ////////////////////////////////////////////////////////////////////////////////////////
      //
      //               Création de l’énoncé
      //
      /// ////////////////////////////////////////////////////////////////////////////////////////

      //  Création de l’énoncé
      n = j3pGetRandomInt(0, stor.questions.length - 1) // La question est choisie parmi celles qui n’ont pas encore été posées
      stor.facteur1 = stor.questions[n][0] // Une question est un triplet rang arrondi difficulté
      stor.facteur2 = stor.questions[n][1]
      stor.questions.splice(n, 1) // On la retire des questions à poser

      if (this.donneesSection.s_operation == 'plus') { stor.reponse = stor.facteur1 + stor.facteur2 } else { stor.reponse = stor.facteur1 * stor.facteur2 } // On met les réponses possibles

      //  Création des conteneurs
      // Création du conteneur dans lequel se trouveront toutes les consignes
      j3pDiv(this.zones.MG, { id: cssPrefix + 'consignes', contenu: '', coord: [0, 0], style: me.styles.etendre('petit.enonce', { padding: '10px', marginTop: '25px' }) })

      // Dans ce conteneur, on crée un div par phrase (cela forcera le retour à la ligne)
      j3pDiv(cssPrefix + 'consignes', cssPrefix + 'zone_consigne1', '')

      // Affiche le nombre de chances qu’il reste
      j3pDiv(cssPrefix + 'consignes', { id: cssPrefix + 'zone_chances', contenu: '', style: me.styles.etendre('moyen.enonce', { marginTop: '30px' }) })
      affiche_chances()

      // Si on veut rajouter des explications, en cas de réponse fausse par exemple
      j3pDiv(this.zones.MG, { id: cssPrefix + 'explications', contenu: '', coord: [10, 200], style: me.styles.petit.explications })
      j3pDiv(cssPrefix + 'explications', cssPrefix + 'explication1', '')
      j3pDiv(cssPrefix + 'explications', cssPrefix + 'explication2', '')
      j3pDiv(cssPrefix + 'explications', { id: cssPrefix + 'explication3', coord: [10, 10] })

      // On crée ici le div qui contiendra toutes les remarques sur ce qu’on pense de la réponse de l’élève (le plus souvant dans le cadre MD)
      j3pDiv(this.zones.MD, { id: cssPrefix + 'correction', contenu: '', coord: [0, 0], style: me.styles.etendre('petit.correction', { padding: '10px' }) })
      //  Création des conteneurs

      //  Initialisation des zones de saisie
      { j3pAffiche(cssPrefix + 'zone_consigne1', cssPrefix + 'consigne1', (this.donneesSection.s_operation == 'plus' ? textes.phraseEnoncePlus : textes.phraseEnonceFois),
        {
          style: me.styles.grand.enonce,
          a: stor.facteur1,
          r: stor.reponse,
          inputmq1: { width: '12px' }
        })

      // Le nom de chaque zone de saisie est construit de la même façon pour les zones input et inputmq :
      // le nom input est toujours précédé du nom du span qui contient la zone (ce span est le deuxième paramètre de la fonction j3pAffiche

      // On rajoute les contraintes de saisie
      j3pRestriction(cssPrefix + 'consigne1inputmq1', '0-9') // on n’autorise que les chiffres

      //  Permet de refaire passer la zone de saisie en noir lors du deuxième essai
      j3pElement(cssPrefix + 'consigne1inputmq1').addEventListener('keypress', function () {
        j3pElement(cssPrefix + 'consigne1inputmq1').style.color = ''
      }, false)

      // On rajoute toutes les informations qui permettront de valider la saisie
      j3pElement(cssPrefix + 'consigne1inputmq1').typeReponse = ['nombre', 'exact']
      j3pElement(cssPrefix + 'consigne1inputmq1').reponse = stor.facteur2

      // Paramétrage de la validation
      // mes_zones_saisie est un tableau contenant le nom de chacune des zones (on pourrait aussi y mettre une liste déroulante)
      const mes_zones_saisie = [cssPrefix + 'consigne1inputmq1']
      // validePerso est souvant un tableau vide
      // Mais parfois certaines zones ne peuvent pas être validée de manière automatique
      // Donc la validation de ces zones devra se faire "à la main".
      // Par contre on vérifiera si elles sont remplies ou non.
      const mes_zones_valide_perso = []

      // fcts_valid est un objet permettant la validation. Il contient le nom des zones (tableau précédent) et un deuxième paramètre validePerso
      la_section.fcts_valid = new ValidationZones({ zones: mes_zones_saisie, validePerso: mes_zones_valide_perso, parcours: me })
      } //  Initialisation des zones de saisie

      j3pFocus(cssPrefix + 'consigne1inputmq1')

      this.finEnonce() // Obligatoire
      //  Partie exécutée à chaque essai
      break }// case "enonce":

    /// ////////////////////////////////////////////////////////////////////////////////////////
    //
    //            Phase correction
    //
    /// ////////////////////////////////////////////////////////////////////////////////////////

    case 'correction': {
      // Permet de nommer facilement les conteneurs
      const fcts_valid = la_section.fcts_valid // ici je crée juste une variable pour raccourcir le nom de l’object fcts_valid

      //  Efface le nombre de chances qui restent
      j3pElement(cssPrefix + 'zone_chances').innerHTML = ''

      //  Efface les éléments de correction
      j3pElement(cssPrefix + 'explication1').innerHTML = ''
      j3pElement(cssPrefix + 'explication2').innerHTML = ''
      j3pElement(cssPrefix + 'explication3').innerHTML = ''
      j3pElement(cssPrefix + 'explication2').style.marginTop = '20px'

      // ce qui suit sert pour la validation de toutes les zones
      // le tableau contenant toutes les zones de saisie
      var reponse = fcts_valid.validationGlobale() // Ne pas modifier
      // A cet instant reponse contient deux éléments :
      // reponse.aRepondu qui me dit si toutes les zones ont bien été complétées ou non
      // reponse.bonneReponse qui me dit si les zones qui ne sont pas dans le tableau valide_pero ont des réponses correctes ou non.
      // Si reponse.aRepondu vaut true, la fonction met aussi en bleu ou rouge les réponses bonnes ou fausses.
      // Si c’est la dernière tentative de répondre, on barre également les zones fausses

      // Réponse vide ou incomplète et la limite de temps n’est pas atteinte
      if (!reponse.aRepondu && !this.isElapsed) {
        this.reponseManquante(cssPrefix + 'correction')

        affiche_chances() //  affiche le nombre de chances qui restent
      } else {
        // Une réponse a été saisie
        if (reponse.bonneReponse) {
          this._stopTimer()
          this.score++
          j3pElement(cssPrefix + 'correction').style.color = this.styles.cbien
          j3pElement(cssPrefix + 'correction').innerHTML = cBien

          // Même si la réponse est bonne on peut afficher des éléments de correction/explication :
          // affiche_correction(true);

          this.typederreurs[0]++ //  Compte les bonnes réponses
          this.cacheBoutonValider() //  Plus de réponse à donner
          this.etat = 'navigation'
          this.sectionCourante() //  Passe à la question suivante
        } else {
          // Mauvaise réponse
          stor.erreurs.push([stor.facteur1, stor.facteur2]) // Mémorise l’erreur
          if (this.donneesSection.nbchances == 1) stor.erreurs.push([stor.facteur1, stor.facteur2]) // Il faudra réussir 2 fois pour confirmer son erreur

          j3pElement(cssPrefix + 'correction').style.color = this.styles.cfaux // Commentaires à droite en rouge

          // Fini à cause de la limite de temps
          if (this.isElapsed) {
            this._stopTimer()

            j3pElement(cssPrefix + 'correction').innerHTML = tempsDepasse // Commentaires à droite

            j3pDesactive(cssPrefix + 'consigne1inputmq1') // Désactiver toutes les zones de saisie
            j3pBarre(cssPrefix + 'consigne1inputmq1')

            this.typederreurs[10]++ //  Compte les temps dépassés
            this.cacheBoutonValider() //  Plus de réponse à donner

            // L’élève avait un temps limite mais ne l’a pas respecté. On affiche alors la correction
            affiche_correction(true)

            this.etat = 'navigation'
            this.sectionCourante() //  Passe à la question suivante
          } else {
          // Réponse fausse
            j3pElement(cssPrefix + 'correction').innerHTML = cFaux // Commentaires à droite en rouge

            // Il reste des essais
            if (this.essaiCourant < this.donneesSection.nbchances) {
              j3pElement(cssPrefix + 'correction').innerHTML += '<br>' + essaieEncore // Commentaires à droite

              this.typederreurs[1]++ //  Compte les essais ratés

              affiche_chances() //  affiche le nombre de chances qui restent

              // indication éventuelle ici
              affiche_correction(false)
            } else {
              // Il ne reste plus d’essais
              this._stopTimer()
              this.cacheBoutonValider() //  Plus de réponse à donner

              j3pElement(cssPrefix + 'correction').innerHTML += '<br>' + regardeCorrection // Commentaires à droite
              //
              // Là il ne peut plus se corriger. On lui affiche alors la solution
              affiche_correction(true)

              this.typederreurs[2]++ //  Compte les réponses fausses

              this.etat = 'navigation'
              this.sectionCourante() //  Passe à la question suivante
            } // Il ne reste plus d’essais
          } // Réponse fausse
        } // Mauvaise réponse
        // Une réponse a été saisie
      } // Une réponse a été saisie

      // Obligatoire
      this.finCorrection()
      break }// case "correction":

    /// ////////////////////////////////////////////////////////////////////////////////////////
    //
    //            Phase correction
    //
    /// ////////////////////////////////////////////////////////////////////////////////////////

    case 'navigation': {
      //  Si on a posé toutes les questions, on rajoute les erreurs et on continue
      if (stor.questions.length == 0) {
        stor.questions = stor.questions.concat(stor.erreurs)
        stor.erreurs.length = 0 // Et on vide le tableau des erreurs
      }
      //    Arrête les questions si toutes les réponses sont justes
      if (stor.questions.length == 0) me.donneesSection.nbitems = me.questionCourante

      if (this.sectionTerminee()) {
        // On détermine la phrase d’état renvoyée par la section
        if ((this.typederreurs[1] == 0) && (this.typederreurs[2] == 0) && (this.typederreurs[10] == 0)) {
          //  Aucune erreur: Notion comprise
          if (this.donneesSection.limite != 0) this.parcours.pe = me.donneesSection.pe_1 //  Aucune erreur en temps limité: Notion totalement maitrisée
          else this.parcours.pe = me.donneesSection.pe_2
        } else if (
          ((this.typederreurs[2] == 0) && (this.typederreurs[10] == 0)) ||
                           ((me.donneesSection.nbchances <= 1) && ((this.typederreurs[2] + this.typederreurs[10]) <= 1))
        ) {
          this.parcours.pe = me.donneesSection.pe_3
        } else {
        //  Aucune erreur sans temps limité: Notion comprise
          this.parcours.pe = me.donneesSection.pe_4
        }

        this.afficheBoutonSectionSuivante()
        this.focus('sectioncontinuer')
      } else {
        this.etat = 'enonce'
        this.afficheBoutonSuite()
        this.focus('boutoncontinuer')
      }
      // Obligatoire
      this.finNavigation()

      break
    }// case "navigation":
  } //  switch etat
} //  SectionPosition
