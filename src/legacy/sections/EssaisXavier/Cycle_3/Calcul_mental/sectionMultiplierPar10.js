import { j3pAddContent, j3pAddElt, j3pBarre, j3pEmpty, j3pFocus, j3pGetRandomInt, j3pNombreBienEcrit, j3pRemplacePoint, j3pRestriction, j3pShowError } from 'src/legacy/core/functions'
import ValidationZones from 'src/legacy/outils/calculatrice/ValidationZones'
import { j3pAffiche } from 'src/lib/mathquill/functions'
import textesGeneriques from 'src/lib/core/textes'

const { cBien, cFaux, essaieEncore, regardeCorrection, tempsDepasse } = textesGeneriques

/*
        Auteur  Xavier JEROME
        Date    13/07/2019
        Calcul mental x10; x0,1; /10
*/
const niveauParDefaut = 'nn est un nombre quelconque entre 0,001 et 999 avec au maximum 3 chiffres significatifs (sauf pour le type 4).'
const listeNiveaux = [
  'nn vaut 10, 100 ou 1 000 et le deuxième facteur est restreint à 10 (ou 0,1 pour les types 2 et 5).',
  'nn est un entier à un chiffre significatif jusqu’à 9 000 et le deuxième facteur est restreint à 10 (ou 0,1 pour les types 2 et 5).',
  'nn vaut 10, 100 ou 1 000.',
  'nn est un entier à un chiffre significatif de 1 à 9 000.',
  'nn vaut 0,1; 0,01 ou 0,001 (sauf le type 4).',
  'Le premier facteur vaut 0,n; 0,0n ou 0,00n avec n entre 1 et 9 (sauf le type 4).',
  niveauParDefaut
]
const espaceParDefaut = 'Les erreurs d’écriture des nombres (espaces et zéros inutiles) sont signalées sans compter d’erreur.'
const listeEspaces = [
  'Aucune contrainte d’écriture.',
  espaceParDefaut,
  'Les nombres qui ne sont pas correctement écrits ne sont pas acceptés.'
]

export const params = {
  outils: ['mathquill'],
  /**
   * Ce qui est paramétrable dans l’éditeur de graphe
   * @type {LegacySectionParamsParametres}
   */
  parametres: [
    // Modifie le titre pour tenir compte des paramètres
    ['titre', 'Multiplier ou diviser par 10.', 'string', 'Titre de l’exercice.'],
    // Texte que l’élève peut faire apparaître en bas de l’écran
    ['indication', 'Transforme l’opération 10 x 0,1 en 10 dixièmes.', 'string', 'Indication en bas à gauche.'],
    // Obligatoire: nombre de questions posées pendant l’exercice
    ['nbrepetitionsmax', 10, 'entier', 'Nombre de répétitions maximum de la section.\nIl peut être inférieur si on répond juste à toutes les questions'],
    // Nombre d’essais autorisés avant de déclarer un échec et de passer à la question
    ['nbchances', 2, 'entier', 'Nombre d’essais par répétition'],
    // S’il n’est pas vide, le temps de réponse sera limité
    ['limite', 0, 'entier', 'Temps disponible par répétition, en secondes'],
    ['b_type1', true, 'boolean', 'Questions du type: nn x 10.'],
    ['b_type2', true, 'boolean', 'Questions du type: nn x 0,1.'],
    ['b_type3', true, 'boolean', 'Questions du type: nn ÷ 10.'],
    ['b_type4', true, 'boolean', 'Questions du type: 1 ÷ 10.'],
    ['b_type5', true, 'boolean', 'Questions du type: nn ÷ 0,1.'],
    ['l_niveau', niveauParDefaut, 'liste', 'On pose toutes les questions jusqu’au niveau maximal demandé.\nLes questions sont sous la forme nn x 10; 10 x 0.1; nn÷10 ;1÷10 ; nn÷0.1.\nLe deuxième facteur vaut 10, 100 ou 1 000 (ou0,1, 0,01 ou 0,001 pour les types 2 et 5)', listeNiveaux],
    ['l_espaces', espaceParDefaut, 'liste', 'Permet de choisir si on tient compte de l’écriture des nombres', listeEspaces],
    ['b_aide1', true, 'boolean', 'Traduction des opérations en dizaine, dixième, etc (niveau 1, 3 et 5).'],
    ['b_aide2', true, 'boolean', 'Indication du déplacement de la virgule.']
  ],

  pe: [
    { pe_1: 'Notion totalement maitrisée.' },
    { pe_2: 'Notion comprise.' },
    { pe_3: 'Notion fragile.' },
    { pe_4: 'Notion non comprise.' },
    { pe_5: 'Notion comprise, mais la séparation des classes n’est pas acquise.' },
    { pe_6: 'Notion comprise, mais il reste des zéros inutiles.' }
  ]
}

const textes = {
  titreExo: 'Multiplier ou diviser par 10.',
  nomPositions1: ['dizaines', 'centaines', 'milliers'],
  nomPositions2: ['dixièmes', 'centièmes', 'millièmes'],
  nomOperation: ['multiplier', 'diviser'],
  nomSens: ['droite', 'gauche'],
  phraseNbChances: 'Il te reste £e essai£p.', //  Ne pas supprimer
  phraseEnonceFois: '£a$\\times$£b = @1@',
  phraseEnonceDiviser: '£a$\\div$£b = @1@',
  Aide01: 'Attention: c’est £a$\\times$£b qui est égal à £r.',
  Aide02: 'Attention: c’est £a$\\div$£b qui est égal à £r.',
  Aide03: 'Traduis l’opération par: £a £b font ... ',
  Aide04: 'Traduis l’opération par: combien faut-il de £b pour faire £a?',
  Aide05: 'Traduis l’opération par: dans £a unité£s, il y a £b ... ?',
  Aide06: 'Pour £m par £a, il faut décaler la virgule de £b rang£s vers la £d.',
  Aide07: 'Diviser par £a revient à multiplier par £b.',
  Aide08: ' : Evite d’écrire des zéros inutiles.',
  Aide09: '£n : Sépare correctement les classes par des espaces.'
}

export function upgradeParametres (parametres) {
  // les ' sont devenus ’ dans les choix proposés
  parametres.l_niveau = parametres.l_niveau.replace(/'/g, '’')
  parametres.l_espaces = parametres.l_espaces.replace(/'/g, '’')
}

/**
 * section
 * @this {Parcours}
 */
export default function main () {
  //  Permet de désigner la section
  const me = this
  const ds = me.donneesSection
  const stor = me.storage

  /// ////////////////////////////////////////////////////////////////////////////////////////
  //
  //                  Données et paramètres de la section
  //
  /// ////////////////////////////////////////////////////////////////////////////////////////

  /// ////////////////////////////////////////////////////////////////////////////////////////
  //
  //              Ajouter ici toutes les fonctions nécessaires à la section
  //
  /// ////////////////////////////////////////////////////////////////////////////////////////

  //  Reformate un nombre
  function corrigeNombre (nombre) {
    //  Corrige un nombre saisi sous forme de texte
    //  Enlève les espaces, les zéros inutiles
    //  Met une virgule
    //  corrige la séparation des classes
    const motif = /^\d{0,3}(\s\d{3}){0,5}(,(\d{3}\s){0,5}\d{0,3})?$/
    if (nombre.length === 0) {
      return [nombre, true, true, nombre]
    } else {
      nombre = nombre.trim() // Supprime les espaces de début et de fin
      nombre = nombre.replace('.', ',') // Remplace les points par des virgules
      if (nombre.indexOf(',') === -1) nombre += ',' //  Rajoute la virgule si c’est un entier
      while (nombre !== nombre.replace('  ', ' ')) nombre = nombre.replace('  ', ' ') //  Cas où il y a plus d’un espace entre les classes
      let repZeros = nombre // Nombre avant d’enlever les 0 inutiles
      const classes = nombre.match(motif) //  On teste si les espaces sont bien placés
      if (repZeros.endsWith(',')) repZeros = repZeros.substr(0, repZeros.length - 1) //  Enlève la virgule si c’est un entier
      if (repZeros[0] === ',') repZeros = '0' + repZeros // Si on a enlevé le zéro des unités
      while ((nombre[0] === ' ') || (nombre[0] === '0')) nombre = nombre.substr(1) //  Enlève les 0 et les espaces à gauche inutiles
      while (nombre.endsWith(' ') || nombre.endsWith('0')) nombre = nombre.substr(0, nombre.length - 1) //  Enlève les 0 et les espaces à droite inutiles
      if (nombre[0] === ',') nombre = '0' + nombre // Si on a enlevé le zéro des unités
      if (nombre.endsWith(',')) nombre = nombre.substr(0, nombre.length - 1) //  Enlève la virgule si c’est un entier
      const zeros = (nombre === repZeros) // Si le nombre a changé, c’est qu’il y avait des 0 inutiles
      nombre = j3pNombreBienEcrit(nombre.replace(/ /g, '').replace(',', '.'))
      return [nombre, classes, zeros, repZeros]
    }
    // Non vide
  } //  corrige_nombre

  //  Crée les infos de la question
  function facteurs (type, niveau, item1, item2) {
    const facteur1 = [[[10, 100, 1000], [1, 10, 100, 1000], [10, 100, 1000], [10, 100, 1000], [0.1, 0.01, 0.001], [0.1, 0.01, 0.001], [1, 0.1, 0.01, 0.001]],
      [[10], [1, 10], [10, 100, 1000], [10, 100, 1000], [0.1, 0.01, 0.001], [0.1, 0.01, 0.001], [1, 0.1, 0.01, 0.001]],
      [[10, 100, 1000], [10, 100, 1000], [10, 100, 1000], [10, 100, 1000], [0.1, 0.01, 0.001], [0.1, 0.01, 0.001], [1, 0.1, 0.01, 0.001]],
      [[1], [1], [1], [1]],
      [[1, 10, 100, 1000], [1, 10, 100, 1000], [1, 10, 100, 1000], [1, 10, 100, 1000], [0.1, 0.01, 0.001], [0.1, 0.01, 0.001], [1, 0.1, 0.01, 0.001]]][type - 1][niveau - 1][item1 - 1]
    const multiplicateur = [1, 2, 1, 2, 1, 2, 3][niveau - 1]
    const facteur2 = [[[10], [10], [100, 1000], [10, 100, 1000], [10, 100, 1000], [10, 100, 1000], [10, 100, 1000]],
      [[0.1], [0.1], [0.01, 0.001], [0.1, 0.01, 0.001], [0.1, 0.01, 0.001], [0.1, 0.01, 0.001], [0.1, 0.01, 0.001]],
      [[10], [10], [100, 1000], [10, 100, 1000], [10, 100, 1000], [10, 100, 1000], [10, 100, 1000]],
      [[10], [10], [100, 1000], [10, 100, 1000]],
      [[0.1], [0.1], [0.01, 0.001], [0.1, 0.01, 0.001], [0.1, 0.01, 0.001], [0.1, 0.01, 0.001], [0.1, 0.01, 0.001]]][type - 1][niveau - 1][item2 - 1]

    return [type, facteur1, multiplicateur, facteur2, niveau]
  } // intervalles_to_tab

  //   Affiche la correction sous l’énoncé
  function afficheCorrection (fin) {
    // fonction appelée pour afficher la correction (que la réponse soit bonne ou fausse

    let chaineReponseEleve //  Réponse élève au format corrigé
    if (ds.n_espaces === 2) { // Si on tient compte des erreurs d’écriture, on corrige l’écriture
      // en es6 on peut faire du `const [a, b] = tab` pour faire `const a = tab[0]; const b = tab[1]` mais pas ici
      // FIXME cette écriture n’est pas valide
      [chaineReponseEleve, stor.espaces, stor.zeros, stor.repZeros] = corrigeNombre(stor.input1.value) // Remplace la saisie par un nombre plus propre
    } else {
      chaineReponseEleve = stor.input1.value
    }// Sinon la zone de saisie a déjà été corrigée
    const reponseEleve = parseFloat(chaineReponseEleve.replace(/ /g, '').replace(',', '.'))//  Réponse permettant les calculs

    if (fin && !stor.reponse.bonneReponse) { //  On affiche la bonne réponse s’il n’a pas trouvé au dernier essai
      j3pAddElt(stor.ZoneConsigne1, 'span', stor.chaineBonneReponse, { style: me.styles.etendre('moyen.correction', { paddingLeft: '30px' }) })
    }

    if (!stor.reponse.bonneReponse && (chaineReponseEleve !== stor.chaineBonneReponse)) {
      j3pEmpty(stor.ZoneExplication2) //  Pour effacer  un éventuel précédent affichage

      if ((ds.b_aide1 || ds.b_aide2) && stor.reponse.aRepondu) {
        //  Si l’aide est autorisée, on donne une opération qui donne le résultat de l’élève
        stor.ZoneExplication21 = j3pAddElt(stor.ZoneExplication2, 'div')
        let hypothese
        switch (stor.type) {
          case 1: // Idem au type 2
          case 2: {
            hypothese = Math.round(reponseEleve / stor.facteur2)
            if (hypothese * stor.facteur2 === reponseEleve) {
              j3pAffiche(stor.ZoneExplication21, '', textes.Aide01,
                {
                  a: j3pNombreBienEcrit(hypothese),
                  b: stor.chaineFacteur2,
                  r: j3pNombreBienEcrit(reponseEleve)
                })
            }
            break
          } // case type 1 et 2
          case 3: // Idem au type 4
          case 4: // Idem au type 5
          case 5: {
            hypothese = Math.round(reponseEleve * stor.facteur2)
            if (hypothese / stor.facteur2 === reponseEleve) {
              j3pAffiche(stor.ZoneExplication21, '', textes.Aide02,
                {
                  a: j3pNombreBienEcrit(hypothese),
                  b: stor.chaineFacteur2,
                  r: j3pNombreBienEcrit(reponseEleve)
                })
            }
            break
          } // case type 3 et 4
        } // switch type

        if (ds.b_aide1 && ((stor.codeQuestion[4] === 1) || (stor.codeQuestion[4] === 3))) {
          // Dans les cas simples on traduit l’opération en une phrase
          stor.ZoneExplication22 = j3pAddElt(stor.ZoneExplication2, 'div')
          switch (stor.type) {
            case 1: {
              j3pAffiche(stor.ZoneExplication22, '', textes.Aide03,
                {
                  a: stor.chaineFacteur1,
                  b: textes.nomPositions1[Math.round(Math.log10(stor.facteur2)) - 1]
                })
              break
            } // case type 1
            case 2: {
              j3pAffiche(stor.ZoneExplication22, '', textes.Aide03,
                {
                  a: stor.chaineFacteur1,
                  b: textes.nomPositions2[Math.round(-Math.log10(stor.facteur2)) - 1]
                })
              break
            } // case type 2
            case 3: {
              j3pAffiche(stor.ZoneExplication22, '', textes.Aide04,
                {
                  style: me.styles.moyen.correction,
                  a: stor.chaineFacteur1,
                  b: textes.nomPositions1[Math.round(Math.log10(stor.facteur2)) - 1]
                })
              break
            } // case type 3
            case 4: {
              j3pAffiche(stor.ZoneExplication22, '', textes.Aide05,
                {
                  a: stor.chaineFacteur1,
                  b: stor.chaineFacteur2,
                  s: (stor.facteur1 > 1 ? 's' : '')
                })
              break
            } // case type 4
            case 5: {
              j3pAffiche(stor.ZoneExplication22, '', textes.Aide04,
                {
                  a: stor.chaineFacteur1,
                  b: textes.nomPositions2[Math.round(-Math.log10(stor.facteur2)) - 1]
                })
              break
            } // case type 3
          } // switch type
        } // Traduire l’opération

        if (ds.b_aide2) {
          stor.ZoneExplication23 = j3pAddElt(stor.ZoneExplication2, 'div')
          let deplacement
          switch (stor.type) { // Décalage de la virgule
            case 1: {
              deplacement = Math.round(Math.log10(stor.facteur2))
              j3pAffiche(stor.ZoneExplication23, '', textes.Aide06,
                {
                  m: textes.nomOperation[0],
                  a: stor.chaineFacteur2,
                  b: deplacement,
                  s: (deplacement > 1 ? 's' : ''),
                  d: textes.nomSens[0]
                })
              break
            } // case type 1
            case 2: {
              deplacement = Math.round(-Math.log10(stor.facteur2))
              j3pAffiche(stor.ZoneExplication23, '', textes.Aide06,
                {
                  m: textes.nomOperation[0],
                  a: stor.chaineFacteur2,
                  b: deplacement,
                  s: (deplacement > 1 ? 's' : ''),
                  d: textes.nomSens[1]
                })
              break
            } // case type 2
            case 3: // Identique à 4
            case 4: {
              deplacement = Math.round(Math.log10(stor.facteur2))
              j3pAffiche(stor.ZoneExplication23, '', textes.Aide06,
                {
                  m: textes.nomOperation[1],
                  a: stor.chaineFacteur2,
                  b: deplacement,
                  s: (deplacement > 1 ? 's' : ''),
                  d: textes.nomSens[1]
                })
              break
            } // case type 3 et 4
            case 5: {
              j3pAffiche(stor.ZoneExplication23, '', textes.Aide07,
                {
                  a: stor.chaineFacteur2,
                  b: 1 / stor.facteur2
                })
              break
            } // case type 5
          } // switch type
        } // Déplacement virgule
      } // Aide
    } // Mauvaise réponse

    //  Indique si il y avait des zeros inutiles
    let indexDebut, finInutile
    if (stor.reponse.aRepondu && !stor.zeros && (ds.n_espaces >= 1)) {
      if (reponseEleve === 0) {
        if (!stor.reponseInitiale.includes(',')) indexDebut = stor.reponseInitiale.length - 1 // Si le nombre vaut 0 écrit sans virgule, tout ce qui est à gauche du 0 de droite est inutile
        else {
          indexDebut = stor.reponseInitiale.indexOf(',') - 1 //  S’il y avait une virgule alors c’est le caractère d’avant
          while (stor.reponseInitiale[indexDebut] !== '0' && indexDebut >= 0) indexDebut-- //  A moins que ce ne soit un espace
        }
      } else {
        indexDebut = 0
        while (['0', ' '].includes(stor.reponseInitiale[indexDebut])) indexDebut++ // On cherche le premier chiffre qui ne soit pas un zéro
        if (stor.reponseInitiale[indexDebut] === ',') {
          while (stor.reponseInitiale[indexDebut] !== '0' && indexDebut >= 0) indexDebut--
        } //  Si c’est un nombre plus petit que 1 on recher le premier 0
      }
      const debutInutile = stor.reponseInitiale.substring(0, indexDebut) // Contient les zéros inutiles en début de chaine

      let indexFin //  Index de fin de la partie utile
      if (reponseEleve === 0) {
        if (stor.reponseInitiale.includes(',')) indexFin = stor.reponseInitiale.indexOf(',') - 1 // Si le nombre vaut 0, tout ce qui est à partir de la virguel est inutile
        else indexFin = stor.reponseInitiale.length - 1
      } else { //  Sinon il n’y a pas de partie inutile
        indexFin = indexDebut - 1
        for (let i = 0; i <= chaineReponseEleve.length - 1; i++) {
          if (chaineReponseEleve[i] !== ' ') {
            indexFin = stor.reponseInitiale.indexOf(chaineReponseEleve[i], indexFin + 1)
          }
        }
      }
      finInutile = stor.reponseInitiale.substring(indexFin + 1)
      if ((debutInutile.length + finInutile.length !== 0)) {
        stor.ZoneExplication27 = j3pAddElt(stor.ZoneExplication2, 'div')
        stor.ZoneExplication27.style = me.styles.moyen.correction //  Affiche le nombre correctement écrit
        j3pAddElt(stor.ZoneExplication27, 'span', debutInutile).style.color = me.styles.cfaux // Les zéros du début en rouge
        j3pAddElt(stor.ZoneExplication27, 'span', stor.reponseInitiale.substring(indexDebut, indexFin + 1)) // Les chiffres utiles
        j3pAddElt(stor.ZoneExplication27, 'span', finInutile).style.color = me.styles.cfaux // Les zéros de la fin en rouge
        j3pAddElt(stor.ZoneExplication27, 'span', textes.Aide08)
      } // Erreur à afficher
    } //  Erreur zéros

    //  Indique si les classes étaient correctement séparées
    if (stor.reponse.aRepondu && !stor.espaces && (ds.n_espaces >= 1)) {
      stor.ZoneExplication28 = j3pAddElt(stor.ZoneExplication2, 'div')
      j3pAffiche(stor.ZoneExplication28, '', textes.Aide09,
        {
          n: chaineReponseEleve
        }) //  Affiche le nombre correctement écrit
    }
    //  Erreur espace 1

    if (stor.reponse.aRepondu && (ds.n_espaces >= 2)) { // Dans les cas où on comptabilise les erreurs
      if (chaineReponseEleve === stor.chaineBonneReponse) { // s’il n’y a pas d’autre cause d’erreur
        if (!stor.espaces) me.typederreurs[6]++ // Si les espaces sont mal placés
        else if (!stor.zeros && (indexDebut + finInutile.length !== 0)) me.typederreurs[7]++
      } // Zéros inutiles
    }
  } //  affiche_correction

  // Affiche le nombre de tentatives qu’il reste s’il y a plusieurs chances
  function afficheChances () {
    j3pEmpty(stor.ZoneChances)
    if (ds.nbchances > 1) {
      j3pAffiche(stor.ZoneChances, '', textes.phraseNbChances,
        {
          e: ds.nbchances - me.essaiCourant,
          p: (ds.nbchances - me.essaiCourant > 1 ? 's' : '') // Pour le pluriel
        })
    }
  } // afficheChances

  // Ajoute les fonctions de contrôle à un input
  function ajouteControle (input) {
    // Remplace le point par la virgule
    input.addEventListener('input', j3pRemplacePoint)
    //  Empêche qu’il y ait deux virgules dans le nombre
    input.addEventListener('keydown', function pasDeuxVirgules (event) {
      if ((event.key === ',') || (event.key === '.')) if ((input.value.includes(',')) || (input.value.includes('.'))) event.preventDefault()
    }, false)
  } // ajouteControle

  /// ////////////////////////////////////////////////////////////////////////////////////////
  //
  //              Déroulement de la section Enonce, Correction, navigation
  //
  /// ////////////////////////////////////////////////////////////////////////////////////////
  function enonceMain () {
    /// ////////////////////////////////////////////////////////////////////////////////////////
    //
    //               Création de l’énoncé
    //
    /// ////////////////////////////////////////////////////////////////////////////////////////
    let n
    //  Création de l’énoncé// codeQuestion contient les informations permettant de construire la question
    if (stor.repeter && stor.erreurs.length !== 0) { // On choisit la question une fois sur deux dans les erreurs passées
      n = j3pGetRandomInt(0, stor.erreurs.length - 1) // La question est choisie parmi les erreurs passées
      stor.codeQuestion = stor.erreurs[n]
      stor.erreurs.splice(n, 1) // On la retire des questions à poser
      stor.repeter = false // La prochaine fois on prendra une nouvelle question
    } else {
      n = j3pGetRandomInt(0, stor.questions.length - 1) // La question est choisie parmi celles qui n’ont pas encore été posées
      stor.codeQuestion = stor.questions[n]
      stor.questions.splice(n, 1) // On la retire des questions à poser
      stor.repeter = true
    }

    stor.type = stor.codeQuestion[0]
    stor.facteur1 = stor.codeQuestion[1]
    stor.multiplicateur = stor.codeQuestion[2]
    stor.facteur2 = stor.codeQuestion[3]

    switch (stor.multiplicateur) {
      case 1: {
        stor.nombreMultiplicateur = 1
        break
      }
      case 2: {
        stor.nombreMultiplicateur = j3pGetRandomInt(2, 9)
        break
      }
      case 3: {
        stor.nombreMultiplicateur = j3pGetRandomInt(1, 999)
        break
      }
    }

    // Calcul de la bonne réponse
    stor.laBonneReponse = Math.round((stor.type >= 3 ? stor.facteur1 * stor.nombreMultiplicateur / stor.facteur2 : stor.facteur1 * stor.nombreMultiplicateur * stor.facteur2) * 1000000) / 1000000
    stor.chaineBonneReponse = j3pNombreBienEcrit(stor.laBonneReponse)
    stor.chaineFacteur1 = j3pNombreBienEcrit(Math.round(stor.facteur1 * stor.nombreMultiplicateur * 1000) / 1000) // Pour supprimer des LSB parasites
    stor.chaineFacteur2 = j3pNombreBienEcrit(stor.facteur2) // Pour transformer le point en virgule et rajouter les espaces
    // Création de l’énoncé

    //  Création des conteneurs
    // Création du conteneur dans lequel se trouveront toutes les consignes
    stor.ZoneConsignes = j3pAddElt(me.zonesElts.MG, 'div', '', { style: { padding: '10px', marginTop: '25px' } })

    // Dans ce conteneur, on crée un div par phrase (cela forcera le retour à la ligne)
    stor.ZoneConsigne1 = j3pAddElt(stor.ZoneConsignes, 'div')

    // Affiche le nombre de chances qu’il reste
    stor.ZoneChances = j3pAddElt(stor.ZoneConsignes, 'div', '', { style: me.styles.etendre('moyen.enonce', { marginTop: '30px' }) })
    afficheChances()

    // Si on veut rajouter des explications, en cas de réponse fausse par exemple
    stor.ZoneExplications = j3pAddElt(me.zonesElts.MG, 'div', '', { style: me.styles.etendre('moyen.explications', { padding: '10px', marginTop: '15px' }) })

    // Pas utilisé dans cette section
    stor.ZoneExplication2 = j3pAddElt(stor.ZoneExplications, 'div')

    // On crée ici le div qui contiendra toutes les remarques sur ce qu’on pense de la réponse de l’élève (le plus souvant dans le cadre MD)
    stor.ZoneCorrection = j3pAddElt(me.zonesElts.MD, 'div', '', { style: me.styles.etendre('petit.correction', { padding: '10px' }) })
    //  Création des conteneurs

    //  Initialisation des zones de saisie
    const chaine = stor.type >= 3 ? textes.phraseEnonceDiviser : textes.phraseEnonceFois // Choisit la question entre multiplier et diviser
    const options = {
      style: me.styles.moyen.enonce,
      a: stor.chaineFacteur1, // Pour supprimer des LSB parasites
      b: stor.chaineFacteur2,
      input1: { dynamique: true, width: '12px', correction: stor.laBonneReponse }
    }
    const elt1 = j3pAffiche(stor.ZoneConsigne1, '', chaine, options)
    stor.input1 = elt1.inputList[0]
    // Le nom de chaque zone de saisie est construit de la même façon pour les zones input et inputmq :
    // le nom input est toujours précédé du nom du span qui contient la zone (ce span est le deuxième paramètre de la fonction j3pAffiche

    // On rajoute les contraintes de saisie
    j3pRestriction(stor.input1, '0-9., ') // on n’autorise que les chiffres

    //  Améliore la gestion de la virgule
    ajouteControle(stor.input1)

    //  Limite la zone de saisie
    stor.input1.maxLength = 10

    // On rajoute toutes les informations qui permettront de valider la saisie
    stor.input1.typeReponse = ['texte']
    stor.input1.reponse = [stor.chaineBonneReponse]

    // fctsValid est un objet permettant la validation. Il contient le nom des zones (tableau précédent) et un deuxième paramètre validePerso
    stor.fctsValid = new ValidationZones({ zones: [stor.input1], parcours: me })
    //  Initialisation des zones de saisie

    j3pFocus(stor.input1)

    me.finEnonce() // Obligatoire
    //  Partie exécutée à chaque essai
  }
  switch (me.etat) {
    /// ////////////////////////////////////////////////////////////////////////////////////////
    //
    //            Phase énoncé
    //
    /// ////////////////////////////////////////////////////////////////////////////////////////

    case 'enonce': {
      //  Construit l’énoncé: exécuté au début de chaque question
      if (me.debutDeLaSection) { //  Code exécuté uniquement au lancement de la section
        // Construction de la page
        me.construitStructurePage('presentation1')

        ds.nbrepetitions = ds.nbrepetitionsmax // C’est cette valeur qui est affichée dans l’entête
        me.afficheTitre(textes.titreExo)
        // Affiche l’indication en bas à gauche
        if (ds.indication !== '') me.indication(me.zonesElts.IG, ds.indication)
        me.typederreurs = [0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0]
        //  Paramètres et variables particuliers à la section
        ds.n_niveau = listeNiveaux.indexOf(ds.l_niveau) + 1

        stor.listeTypes = []
        if (ds.b_type1) stor.listeTypes.push(1)
        if (ds.b_type2) stor.listeTypes.push(2)
        if (ds.b_type3) stor.listeTypes.push(3)
        if (ds.b_type4) stor.listeTypes.push(4)
        if (ds.b_type5) stor.listeTypes.push(5)

        ds.n_espaces = listeEspaces.indexOf(ds.l_espaces)

        // Controle la progression des questions
        stor.rangItem2Max = [1, 1, 2, 3, 3, 3, 3] // le nombre max de possibilités pour le facteur 2 en fonction du niveau
        stor.rangItem1Max = [[3, 1, 3, 1, 4], [4, 2, 3, 1, 4], [3, 3, 3, 1, 4], [3, 3, 3, 1, 4], [3, 3, 3, 0, 3], [3, 3, 3, 0, 3], [4, 4, 4, 0, 4]] // le nombre max de possibilités pour le facteur 1 en fonction du niveau et du type

        if (ds.n_niveau >= 4) stor.rangType = stor.listeTypes.length // A partir du niveau 4 on introduit tous les types du premier coup
        else stor.rangType = 1

        if (ds.n_niveau >= 7) stor.rangNiveau = 2 // On commence toujours au niveau 1 sauf pour le niveau 7 où on commence directement au niveau 2
        else stor.rangNiveau = 1

        if (ds.n_niveau === 1) stor.rangItem1 = 1 // Si le niveau n’est pas 1 on commence tout de suite par toutes les questions du niveau 1
        else stor.rangItem1 = 4

        if (stor.listeTypes.length === 0) j3pShowError('Il n’y a pas de question.')

        //  Tableaux permettant de ne pas poser 2 fois la même question
        stor.questions = [] // Contient les question qui n’ont pas encore été posées
        // Une question est définie par (type de question,facteur1,multiplicateur,facteur2,niveau)
        // multiplicateur vaut 1: facteur1 pas modifié; 2 facteur1 multiplié par un entier de 1 à 9; 3 facteur1 multiplié par un entier de 1 à 999
        for (let i = 1; i <= stor.rangNiveau; i++) {
          for (let j = 1; j <= stor.rangType; j++) {
            for (let k = 1; k <= stor.rangItem2Max[i - 1]; k++) {
              for (let l = 1; l <= Math.min(stor.rangItem1, stor.rangItem1Max[i - 1][stor.listeTypes[j - 1] - 1]); l++) { stor.questions.push(facteurs(stor.listeTypes[j - 1], i, l, k)) }
            }
          }
        }

        stor.erreurs = [] // Contient questions qui ont provoqué une erreur pour les représenter en priorité
        stor.repeter = false // Bascule permettant de prendre alternativement des questions nouvelles et des anciennes erreurs

        //  Début de section
      } else { //  Code exécuté à chaque nouvelle question: Partie à ne pas modifier
        // A chaque répétition, on nettoie la scène
        me.videLesZones()
      } // Code exécuté à chaque nouvelle question: Partie à ne pas modifier

      //  Partie exécutée à chaque essai
      enonceMain()
      break
    } // case "enonce":

    /// ////////////////////////////////////////////////////////////////////////////////////////
    //
    //            Phase correction
    //
    /// ////////////////////////////////////////////////////////////////////////////////////////

    case 'correction': {
      const fctsValid = stor.fctsValid // ici je crée juste une variable pour raccourcir le nom de l’object fctsValid

      //  Efface le nombre de chances qui restent
      j3pEmpty(stor.ZoneChances)

      //  Efface les éléments de correction
      //  Pas utilisé dans cette section
      j3pEmpty(stor.ZoneExplication2)

      stor.input1.value = stor.input1.value.trim() // on enlève les espaces en début et en fin
      if (stor.input1.value.length === 1 && stor.input1.value === ',') stor.input1.value = '' // On élimine ce cas particulier
      stor.reponseInitiale = stor.input1.value // On mémorise la réponse initiale

      if (ds.n_espaces < 2) { //  Si on ne tient pas compte des espaces et des zéros inutiles
      // Corrige l’écriture du nombre pour le comparer à la bonne réponse

        const resultat = corrigeNombre(stor.input1.value)
        stor.input1.value = resultat[0]
        stor.espaces = resultat[1]
        stor.zeros = resultat[2]
        stor.repZeros = resultat[3]
      } // Remplace la saisie par un nombre plus propre
      // ce qui suit sert pour la validation de toutes les zones
      // le tableau contenant toutes les zones de saisie
      stor.reponse = fctsValid.validationGlobale() // Ne pas modifier
      // A cet instant reponse contient deux éléments :
      // reponse.aRepondu qui me dit si toutes les zones ont bien été complétées ou non
      // reponse.bonneReponse qui me dit si les zones qui ne sont pas dans le tableau valide_pero ont des réponses correctes ou non.
      // Si reponse.aRepondu vaut true, la fonction met aussi en bleu ou rouge les réponses bonnes ou fausses.
      // Si c’est la dernière tentative de répondre, on barre également les zones fausses

      if ((!stor.reponse.aRepondu) && (!me.isElapsed)) {
        // Réponse vide ou incomplète et la limite de temps n’est pas atteinte
        me.reponseManquante(stor.ZoneCorrection)
        afficheChances() //  affiche le nombre de chances qui restent
      } else {
        // Une réponse a été saisie
        if (stor.reponse.bonneReponse) { // Bonne réponse
          me._stopTimer()
          me.score++
          stor.ZoneCorrection.style.color = me.styles.cbien
          j3pAddContent(stor.ZoneCorrection, cBien, { replace: true })

          // Même si la réponse est bonne on peut afficher des éléments de correction/explication :
          afficheCorrection(true)

          // Si toutes les erreurs ont été corrigées
          // Augmente le niveau de difficulté
          if (stor.erreurs.length === 0) {
            if (stor.rangItem1 === 1) { // Si c’est le tout début, on rajoute les 2 questions suivantes
              stor.rangItem1 = 4
              for (let i = 2; i <= 3; i++) { stor.questions.push(facteurs(stor.listeTypes[stor.rangType - 1], 1, i, 1)) }
            } else {
              if (stor.rangType < stor.listeTypes.length) { // Si on est au niveau 1, on rajoute les types un par un
                stor.rangType++
                for (let k = 1; k <= stor.rangItem2Max[0]; k++) {
                  for (let l = 1; l <= stor.rangItem1Max[stor.rangNiveau - 1][stor.listeTypes[stor.rangType - 1] - 1]; l++) { stor.questions.push(facteurs(stor.listeTypes[stor.rangType - 1], 1, l, k)) }
                }
              } else { // Pour les niveaux suivants, on rajoute tous les types d’un coup
                if (stor.rangNiveau < ds.n_niveau) {
                  stor.rangNiveau++
                  for (let j = 1; j <= stor.listeTypes.length; j++) {
                    for (let k = 1; k <= stor.rangItem2Max[stor.rangNiveau - 1]; k++) {
                      for (let l = 1; l <= stor.rangItem1Max[stor.rangNiveau - 1][stor.listeTypes[j - 1] - 1]; l++) { stor.questions.push(facteurs(stor.listeTypes[j - 1], stor.rangNiveau, l, k)) }
                    }
                  }
                  stor.questions = stor.questions.filter(function supprimeQuestion (element) { return element[4] >= stor.rangNiveau - 2 })// On ne laisse que les questions des 3 derniers niveaux
                } // Il reste des niveaux
              } // types du niveau 1
            } // tout début
          } // Augmenter la difficulté

          me.typederreurs[0]++ //  Compte les bonnes réponses
          me.cacheBoutonValider() //  Plus de réponse à donner
          me.etat = 'navigation'
          me.sectionCourante() //  Passe à la question suivante
          // Bonne réponse
        } else { // Mauvaise réponse
          if (!stor.erreurs.includes(stor.codeQuestion)) {
            stor.questions.push(stor.codeQuestion) // On reposera la question
            stor.erreurs.push(stor.codeQuestion)
          } // Mémorise l’erreur
          stor.repeter = false // On ne repose pas la question au coup d’après

          stor.ZoneCorrection.style.color = me.styles.cfaux // Commentaires à droite en rouge

          if (me.isElapsed) { // Fini à cause de la limite de temps
            me._stopTimer()

            j3pAddContent(stor.ZoneCorrection, tempsDepasse, { replace: true }) // Commentaires à droite

            j3pBarre(stor.input1)

            me.typederreurs[10]++ //  Compte les temps dépassés
            me.cacheBoutonValider() //  Plus de réponse à donner

            // L’élève avait un temps limite mais ne l’a pas respecté. On affiche alors la correction
            afficheCorrection(true)

            me.etat = 'navigation'
            me.sectionCourante() //  Passe à la question suivante
          } else { // Réponse fausse
            j3pAddContent(stor.ZoneCorrection, cFaux, { replace: true }) // Commentaires à droite en rouge

            if (me.essaiCourant < ds.nbchances) { // Il reste des essais
              j3pAddContent(stor.ZoneCorrection, '<br>' + essaieEncore) // Commentaires à droite

              me.typederreurs[1]++ //  Compte les essais ratés

              afficheChances() //  affiche le nombre de chances qui restent

              // indication éventuelle ici
              afficheCorrection(false)
            } else {
              me._stopTimer()
              me.cacheBoutonValider() //  Plus de réponse à donner

              j3pAddContent(stor.ZoneCorrection, '<br>' + regardeCorrection) // Commentaires à droite
              //
              // Là il ne peut plus se corriger. On lui affiche alors la solution
              afficheCorrection(true)

              me.typederreurs[2]++ //  Compte les réponses fausses

              me.etat = 'navigation'
              me.sectionCourante() //  Passe à la question suivante
            } // Il ne reste plus d’essais
          } // Réponse fausse
        } // Mauvaise réponse
      } // Une réponse a été saisie

      me.finCorrection() // Obligatoire
      break
    }// case "correction":

    /// ////////////////////////////////////////////////////////////////////////////////////////
    //
    //            Phase correction
    //
    /// ////////////////////////////////////////////////////////////////////////////////////////

    case 'navigation': {
      //  Si on a posé toutes les questions, on rajoute les erreurs et on continue
      if (stor.questions.length === 0) stor.questions = stor.questions.concat(stor.erreurs)

      //    Arrête les questions si toutes les réponses sont justes
      if (stor.questions.length === 0) ds.nbitems = me.questionCourante

      if (me.sectionTerminee()) { // sectionTerminee
        // On détermine la phrase d’état renvoyée par la section
        if ((me.typederreurs[1] === 0) && (me.typederreurs[2] === 0) && (me.typederreurs[10] === 0)) { //  Aucune erreur
          if (ds.limite !== 0) {
            me.parcours.pe = ds.pe_1 //  Aucune erreur en temps limité: Notion totalement maitrisée
          } else {
            me.parcours.pe = ds.pe_2 //  Aucune erreur sans temps limité: Notion comprise
          } //  Aucune erreur
        } else if (((me.typederreurs[2] === 0) && (me.typederreurs[10] === 0)) || //  A pu corriger ses erreurs
          ((ds.nbchances <= 1) && ((me.typederreurs[2] + me.typederreurs[10]) <= 1))) { // ou une seule erreur s’il n’y a pas plusieurs chances
          me.parcours.pe = ds.pe_3 //  Notion fragile
        } else if (me.typederreurs[6] + me.typederreurs[7] === me.typederreurs[1] + me.typederreurs[2] + me.typederreurs[10]) { //  Les erreurs sont dues uniquement à l’écriture des nombres
          if (me.typederreurs[6] > me.typederreurs[7]) me.parcours.pe = ds.pe_5 //  Les erreurs sont dues principalement à la séparation des classes.
          else me.parcours.pe = ds.pe_6 //  Les erreurs sont dues principalement aux zéros inutiles
        } else me.parcours.pe = ds.pe_4
        //  Notion non comprise si on n’a pas trouve au moins une réponse lorsqu’il y a plusieurs essais
        //     ou si on n’a pas trouve au moins deux réponses lorsqu’il y a plusieurs essais

        me.afficheBoutonSectionSuivante()
        me.focus('sectioncontinuer')
        // sectionTerminee
      } else { // Question suivante
        me.etat = 'enonce'
        me.afficheBoutonSuite()
        me.focus('boutoncontinuer')
      } // Question suivante

      me.finNavigation() // Obligatoire

      break
    } // case "navigation":
  } //  switch etat
} //  Section
