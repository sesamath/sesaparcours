import { j3pBarre, j3pDiv, j3pElement, j3pFocus, j3pGetRandomInt, j3pRestriction, j3pSpan, j3pValeurde } from 'src/legacy/core/functions'
import ValidationZones from 'src/legacy/outils/calculatrice/ValidationZones'
import { j3pAffiche, j3pDesactive } from 'src/lib/mathquill/functions'
import textesGeneriques from 'src/lib/core/textes'

const { cBien, cFaux, essaieEncore, regardeCorrection, tempsDepasse } = textesGeneriques

/**
 * FIXME pas mal de bugs dans la correction affichée, la refacto du 2023-02-01 en corrige certains mais ça continue de raconter un peu n’importe quoi à la correction (à cette date la section n’est utilisée que dans qq ressources de commun)
 * Trouver tous les diviseurs d’un nombre
 * Fait d’après la section diviseursdirect en introduisant une progression des difficultés
 * L’aide est étoffée
 * La présentation est mise au goût du jour
 * @fileoverview
 * @author Xavier JEROME <xavier.jerome@ac-versailles.fr>
 * @since 07/08/2018
 */

// Phrases d’état renvoyée par la section
const pe1 = 'Notion totalement maitrisée'
const pe2 = 'Notion comprise'
const pe3 = 'Notion fragile.'
const pe4 = 'Notion non comprise.'
const pe5 = 'Notion fragile: oubli de 1 ou du nombre.'
const pe6 = 'Notion fragile: propose des nombres qui ne sont pas des diviseurs.'
const pe7 = 'Notion fragile: oubli le diviseur associé.'
const pe8 = 'Notion fragile: oubli de diviseurs.'

export const params = {
  outils: ['mathquill'],
  /**
   * Ce qui est paramétrable dans l’éditeur de graphe
   * @type {LegacySectionParamsParametres}
   */
  parametres: [
    ['indication', 'Cherche une multiplication qui donne ce nombre.', 'string', 'Indication en bas à gauche'], // Texte que l’élève peut faire apparaître en bas de l’écran
    ['nbrepetitionsmax', 10, 'entier', 'Nombre de répétitions maximum de la section.<BR>Il peut être inférieur si on répond juste à toutes les questions'], // Obligatoire: nombre de questions posées pendant l’exercice
    ['nbchances', 2, 'entier', 'Nombre d’essais par répétition'], // Nombre d’essais autorisés avant de déclarer un échec et de passer à la question
    ['limite', 0, 'entier', 'Temps disponible par répétition, en secondes'], // S’il n’est pas vide, le temps de réponse sera limité
    ['s_difficulte', '1;4', 'string', "Une suite de nombres indiquant les cas autorisés parmi<br>1 pour un produit de deux nombres premiers dans une table<br>2 pour un nombre quelconque dans une table<br>3 pour un nombre pair jusqu'à 60 (on rajoute principalement les nombres égaux à 2 fois un nombre premier)<br>4 pour les doubles de 60 à 100<br>5 pour les nombres premiers jusqu'à 40."],
    ['n_aide', 1, 'entier', '0 pour aucune aide<BR>1 pour une explication des erreurs'] // Niveau d’aide autorisé
  ],

  pe: [
    { pe_1: pe1 },
    { pe_2: pe2 },
    { pe_3: pe3 },
    { pe_4: pe4 },
    { pe_5: pe5 },
    { pe_6: pe6 },
    { pe_7: pe7 },
    { pe_8: pe8 }
  ]
}

const prefix = 'Diviseurs'

const nbPremiers = [2, 3, 5, 7, 11, 13, 17, 19, 23, 29, 31, 37]

const textes = {
  phraseTitre: 'Donner tous les diviseurs d’un nombre entier.', //  Titre de la section
  phraseNbChances: 'Il te reste £e essai£p.', //  Ne pas supprimer
  phraseEnonce1: 'Quels sont tous les diviseurs de £r&nbsp;?<br>Sépare chaque nombre par un point virgule.<br>&1&',
  phraseEnonce2: 'Quels sont tous les diviseurs de £r&nbsp;?<br>Sépare chaque nombre par un point virgule.<br>',
  phraseSolution: 'Tous les diviseurs sont: £r',
  Aide01: 'Tu as oublié 1. 1 divise tous les nombres.',
  Aide02: 'Tu as oublié £r. Tout nombre est divisible par lui-même.',
  Aide03: 'Tu es sûr que tu peux multiplier £a par un nombre entier pour obtenir £r?',
  Aide04: 'Tu as bien mis £a. Or £a$\\times$£b est égal à £r. Donc £b est aussi un diviseur de £r'
}

//  Transforme une chaine d’intervalles en un tableau d’entiers
const listeToTab = (liste) => liste
  .replace(/[[\]]/g, '') // on vire d’éventuels crochets
  .split(/[,; ]/) // on découpe sur , ou ; ou espace
  .map(Number) // converti en number
  .filter(n => Number.isInteger(n)) // et on ne garde que les entiers

/**
 * section Diviseurs
 * @this {Parcours}
 */
export default function main () {
  const me = this
  const stor = this.storage
  const ds = this.donneesSection

  //   Affiche la correction sous l’énoncé
  function afficheCorrection (fin, repValidGlob) {
    // fonction appelée pour afficher la correction (que la réponse soit bonne ou fausse

    j3pElement(prefix + 'explication1').innerHTML = '' //  Pour effacer  un éventuel précédent affichage
    if (!repValidGlob.bonneReponse) {
      let oublie1 = false
      let faux = false
      let moitie = false
      let paire = false
      if (fin) { //  On affiche la bonne réponse s’il n’a pas trouvé au dernier essai
        j3pAffiche(prefix + 'explication1', prefix + 'corr11', textes.phraseSolution,
          {
            style: me.styles.grand.correction,
            r: stor.reponse
          })

        j3pElement(prefix + 'zone_consigne1').innerHTML = '' //  Efface l’énoncé
        j3pAffiche(prefix + 'zone_consigne1', prefix + 'consigne1', textes.phraseEnonce2,
          {
            style: me.styles.moyen.enonce,
            r: stor.nombre
          })
      } // fin
      if ((ds.n_aide >= 1) && repValidGlob.aRepondu) {
        j3pElement(prefix + 'explication2').innerHTML = '' //  Pour effacer  un éventuel précédent affichage
        if (stor.reponsesEleve[0] !== 1) {
          j3pAffiche(prefix + 'explication2', prefix + 'corr21', textes.Aide01 + '\n',
            { style: me.styles.grand.correction })
          oublie1 = true
        } // 1 oublié

        if (stor.reponsesEleve[stor.reponsesEleve.length - 1] !== stor.nombre) {
          j3pAffiche(prefix + 'explication2', prefix + 'corr22', textes.Aide02 + '\n',
            {
              style: me.styles.grand.correction,
              r: stor.nombre
            })
          oublie1 = true
        } // nombre oublié

        let debut = true
        let span, i, j
        for ((i = 0, j = 0); i < stor.diviseurs.length; i++) {
          if (stor.diviseurs[i] === stor.reponsesEleve[j]) {
            if (fin) {
              span = j3pSpan(prefix + 'zone_consigne1', { id: 'rep' + i + j, contenu: (debut ? '' : '; ') + stor.diviseurs[i] })
              span.style.fontSize = me.styles.moyen.enonce.fontSize
              span.style.color = me.styles.cbien
            }
            j++
            debut = false
          } else if (stor.reponsesEleve.includes(stor.diviseurs[i])) {
            for (let k = j; k < stor.reponsesEleve.indexOf((stor.diviseurs[i])); (k++, j++)) {
              if (fin) {
                span = j3pSpan(prefix + 'zone_consigne1', { id: 'rep' + i + j, contenu: (debut ? '' : '; ') + stor.reponsesEleve[k] })
                span.style.fontSize = me.styles.moyen.enonce.fontSize
                span.style.color = me.styles.cfaux
                j3pBarre('rep' + i + j)
              }
              debut = false
              faux = true
              if ((ds.n_aide >= 1) && repValidGlob.aRepondu) {
                j3pAffiche(prefix + 'explication2', prefix + 'corr23' + i + j, textes.Aide03 + '\n',
                  {
                    style: me.styles.grand.correction,
                    r: stor.nombre,
                    a: stor.reponsesEleve[k]
                  })
              }
            }
          } else {
            for (let k = j; stor.reponsesEleve[k] < stor.diviseurs[i]; (k++, j++)) {
              if (fin) {
                span = j3pSpan(prefix + 'zone_consigne1', { id: 'rep' + i + j, contenu: (debut ? '' : '; ') + stor.reponsesEleve[k] })
                span.style.fontSize = me.styles.moyen.enonce.fontSize
                span.style.color = me.styles.cfaux
                j3pBarre('rep' + i + j)
              }
              debut = false
              faux = true
              if ((ds.n_aide >= 1) && repValidGlob.aRepondu) {
                j3pAffiche(prefix + 'explication2', prefix + 'corr23' + i + j, textes.Aide03 + '\n',
                  {
                    style: me.styles.grand.correction,
                    r: stor.nombre,
                    a: stor.reponsesEleve[k]
                  })
              }
            }
            if (fin) {
              span = j3pSpan(prefix + 'zone_consigne1', { id: 'rep' + i + j, contenu: (debut ? '' : '; ') + stor.diviseurs[i] })
              span.style.fontSize = me.styles.moyen.enonce.fontSize
              span.style.color = me.styles.cfaux
            }
            debut = false
            if ((ds.n_aide >= 1) && repValidGlob.aRepondu) {
              if ((stor.diviseurs[i] !== 1) && (stor.diviseurs[i] !== stor.nombre)) {
                if (stor.reponsesEleve.includes(stor.diviseurs[stor.diviseurs.length - 1 - i])) {
                  j3pAffiche(prefix + 'explication2', prefix + 'corr23' + i + j, textes.Aide04 + '\n',
                    {
                      style: me.styles.grand.correction,
                      r: stor.nombre,
                      a: stor.diviseurs[stor.diviseurs.length - 1 - i],
                      b: stor.diviseurs[i]
                    })
                  moitie = true
                } else {
                  // Nombre faux
                  paire = true
                }
              }
            }
          }
        }
        for (let k = j; k < stor.reponsesEleve.length; (k++, j++)) {
          if (fin) {
            span = j3pSpan(prefix + 'zone_consigne1', { id: 'rep' + i + j, contenu: (debut ? '' : '; ') + stor.reponsesEleve[k] })
            span.style.fontSize = me.styles.moyen.enonce.fontSize
            span.style.color = me.styles.cfaux
            j3pBarre('rep' + i + j)
          }
          debut = false
          faux = true
          if ((ds.n_aide >= 1) && repValidGlob.aRepondu) {
            j3pAffiche(prefix + 'explication2', prefix + 'corr23' + i + j, textes.Aide03 + '\n',
              {
                style: me.styles.grand.correction,
                r: stor.nombre,
                a: stor.reponsesEleve[k]
              })
          }
        }
        if (oublie1) me.typederreurs[3]++
        if (faux) me.typederreurs[4]++
        if (moitie) me.typederreurs[5]++
        if (paire) me.typederreurs[6]++
      } // a répondu
    } // Bonne réponse
  } //  afficheCorrection

  // Affiche le nombre de tentatives qu’il reste s’il y a plusieurs chances
  function afficheChances () {
    j3pElement(prefix + 'zone_chances').innerHTML = ''
    if (ds.nbchances > 1) {
      j3pAffiche(prefix + 'zone_chances', prefix + 'chances', textes.phraseNbChances,
        {
          e: ds.nbchances - me.essaiCourant + 1,
          p: (ds.nbchances - me.essaiCourant + 1 > 1 ? 's' : '')
        })
    } // Pour le pluriel
  }

  switch (this.etat) {
    case 'enonce': {
      //  Construit l’énoncé: exécuté au début de chaque question
      if (this.debutDeLaSection) {
      //  Code exécuté uniquement au lancement de la section
        // Construction de la page
        this.construitStructurePage('presentation1')
        this.surcharge({ nbrepetitions: ds.nbrepetitionsmax })
        this.afficheTitre(textes.phraseTitre)

        // Affiche l’indication en bas à gauche
        if (ds.indication) this.indication(this.zones.IG, ds.indication)

        this.typederreurs = [0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0]

        //  Paramètres et variables particuliers à la section
        // Transforme l’intervalle pour qu’il corresponde au codage du nombre
        stor.t_difficulte = listeToTab(ds.s_difficulte).sort()

        //  Tableaux de tableaux donnant tous les produits possibles par difficulté
        stor.produits = []
        stor.produits[1] = [] // 1 Tous les produits de tables avec 2 nombres premiers
        const tPremiers = [2, 3, 5, 7]
        for (let i = 0; i <= 3; i++) {
          for (let j = i; j <= 3; j++) {
            stor.produits[1].push(tPremiers[i] * tPremiers[j])
          }
        }

        stor.produits[2] = [] // 2 Tous les produits de tables sans doublons
        for (let i = 2; i <= 9; i++) {
          for (let j = i; j <= 9; j++) {
            if (!stor.produits[2].includes(i * j)) { stor.produits[2].push(i * j) }
          }
        }

        stor.produits[3] = [] // 3 Tous les doubles jusqu'à 60 (On rajoute surtout 2 fois un nombre premier)
        for (let i = 5; i <= 30; i++) {
          stor.produits[3].push(i * 2)
        }

        stor.produits[4] = [] // 4 Tous les les doubles de 60 à 100
        for (let i = 31; i <= 50; i++) {
          stor.produits[4].push(i * 2)
        }

        stor.produits[5] = [] // 5 Tous les nombres premiers jusqu'à 40
        stor.produits[5] = [...nbPremiers]

        stor.difficulte_en_cours = 0
        // Contient les question qui n’ont pas encore été posées (pour ne pas poser 2 fois la même question)
        /** @type {number[]} */
        stor.questions = stor.produits[stor.t_difficulte[0]].slice()

        stor.erreurs = [] // Contient questions qui ont provoqué une erreur pour les représenter en priorité
      } else {
        // A chaque répétition, on nettoie la scène
        this.videLesZones()
      }

      //  Partie exécutée à chaque essai
      //  Création de l’énoncé
      const n = j3pGetRandomInt(0, stor.questions.length - 1) // La question est choisie parmi celles qui n’ont pas encore été posées
      /** @type {number} */
      stor.nombre = stor.questions[n] // Une question est un triplet rang arrondi difficulté
      stor.questions.splice(n, 1) // On la retire des questions à poser
      /** @type {number[]} */
      stor.diviseurs = [] // Contient la liste ordonnée des diviseurs
      for (let i = 1; i <= Math.sqrt(stor.nombre); i++) {
        if (stor.nombre % i === 0) {
          stor.diviseurs.push(i)
          if (i * i !== stor.nombre) {
            stor.diviseurs.push(Math.round(stor.nombre / i))
          }
        }
      }
      stor.diviseurs.sort()
      stor.reponse = stor.diviseurs.join(';')

      //  Création des conteneurs
      // Création du conteneur dans lequel se trouveront toutes les consignes
      j3pDiv(this.zones.MG, { id: prefix + 'consignes', contenu: '', coord: [0, 0], style: me.styles.etendre('petit.enonce', { padding: '10px', marginTop: '25px' }) })

      // Dans ce conteneur, on crée un div par phrase (cela forcera le retour à la ligne)
      j3pDiv(prefix + 'consignes', prefix + 'zone_consigne1', '')

      // Affiche le nombre de chances qu’il reste
      j3pDiv(prefix + 'consignes', { id: prefix + 'zone_chances', contenu: '', style: me.styles.etendre('moyen.enonce', { marginTop: '30px' }) })
      afficheChances()

      // Si on veut rajouter des explications, en cas de réponse fausse par exemple
      j3pDiv(this.zones.MG, { id: prefix + 'explications', contenu: '', coord: [10, 200], style: me.styles.petit.explications })
      j3pDiv(prefix + 'explications', prefix + 'explication1', '')
      j3pDiv(prefix + 'explications', prefix + 'explication2', '')
      j3pDiv(prefix + 'explications', prefix + 'explication3', '')
      j3pElement(prefix + 'explication2').style.marginTop = '20px'
      j3pElement(prefix + 'explication3').style.marginTop = '20px'

      // On crée ici le div qui contiendra toutes les remarques sur ce qu’on pense de la réponse de l’élève (le plus souvant dans le cadre MD)
      j3pDiv(this.zones.MD, { id: prefix + 'correction', contenu: '', coord: [0, 0], style: me.styles.etendre('petit.correction', { padding: '10px' }) })

      //  Initialisation des zones de saisie
      j3pAffiche(prefix + 'zone_consigne1', prefix + 'consigne1', textes.phraseEnonce1,
        {
          style: me.styles.moyen.enonce,
          r: stor.nombre,
          inputmq1: { width: '12px', correction: '' }
        })

      // Le nom de chaque zone de saisie est construit de la même façon pour les zones input et inputmq :
      // le nom input est toujours précédé du nom du span qui contient la zone (ce span est le deuxième paramètre de la fonction j3pAffiche

      // On rajoute les contraintes de saisie
      j3pRestriction(prefix + 'consigne1inputmq1', '0-9;') // on n’autorise que les chiffres

      //  Permet de refaire passer la zone de saisie en noir lors du deuxième essai
      j3pElement(prefix + 'consigne1inputmq1').addEventListener('keypress', function () {
        j3pElement(prefix + 'consigne1inputmq1').style.color = ''
      }, false)

      // On rajoute toutes les informations qui permettront de valider la saisie
      j3pElement(prefix + 'consigne1inputmq1').typeReponse = ['Texte']

      // Paramétrage de la validation
      // zonesSaisie est un tableau contenant le nom de chacune des zones (on pourrait aussi y mettre une liste déroulante)
      const zonesSaisie = [prefix + 'consigne1inputmq1']
      // validePerso est souvant un tableau vide
      // Mais parfois certaines zones ne peuvent pas être validée de manière automatique
      // Donc la validation de ces zones devra se faire "à la main".
      // Par contre on vérifiera si elles sont remplies ou non.
      const zonesValidePerso = [prefix + 'consigne1inputmq1']

      // fctsValid est un objet permettant la validation. Il contient le nom des zones (tableau précédent) et un deuxième paramètre validePerso
      stor.fctsValid = new ValidationZones({ parcours: me, zones: zonesSaisie, validePerso: zonesValidePerso })

      j3pFocus(prefix + 'consigne1inputmq1')

      this.finEnonce() // Obligatoire
      //  Partie exécutée à chaque essai
      break
    } // case "enonce":

    case 'correction': {
      const fctsValid = stor.fctsValid

      //  Efface le nombre de chances qui restent
      j3pElement(prefix + 'zone_chances').innerHTML = ''

      //  Efface les éléments de correction
      j3pElement(prefix + 'explication1').innerHTML = ''
      j3pElement(prefix + 'explication2').innerHTML = ''
      j3pElement(prefix + 'explication3').innerHTML = ''

      // ce qui suit sert pour la validation de toutes les zones
      // le tableau contenant toutes les zones de saisie
      const repValidGlob = fctsValid.validationGlobale() // Ne pas modifier
      // A cet instant reponse contient deux éléments :
      // reponse.aRepondu qui me dit si toutes les zones ont bien été complétées ou non
      // reponse.bonneReponse qui me dit si les zones qui ne sont pas dans le tableau valide_pero ont des réponses correctes ou non.
      // Si reponse.aRepondu vaut true, la fonction met aussi en bleu ou rouge les réponses bonnes ou fausses.
      // Si c’est la dernière tentative de répondre, on barre également les zones fausses

      // On teste manuellement si la réponse est bonne
      if (repValidGlob.aRepondu) {
        stor.reponsesEleve = listeToTab(j3pValeurde(prefix + 'consigne1inputmq1'))
        stor.reponsesEleve.sort()
        let juste = stor.reponsesEleve.length === stor.diviseurs.length
        if (juste) {
          for (let i = 0; i < stor.reponsesEleve.length && juste; i++) { juste = juste && stor.reponsesEleve[i] === stor.diviseurs[i] }
        }
        fctsValid.zones.bonneReponse[0] = juste
        fctsValid.coloreUneZone(prefix + 'consigne1inputmq1')
        repValidGlob.bonneReponse = juste
      } else if (!this.isElapsed) {
        // Réponse vide ou incomplète et la limite de temps n’est pas atteinte
        this.reponseManquante(prefix + 'correction')
        afficheChances() //  affiche le nombre de chances qui restent
        return this.finCorrection() // on reste en correction
      }

      // Une réponse a été saisie (ou on a atteint la limite de temps)
      if (repValidGlob.bonneReponse) {
        // Bonne réponse
        this.score++
        j3pElement(prefix + 'correction').style.color = this.styles.cbien
        j3pElement(prefix + 'correction').innerHTML = cBien

        // Même si la réponse est bonne on peut afficher des éléments de correction/explication :
        // afficheCorrection(true);

        // Augmente le niveau de difficulté
        // On rajoute les nombres de la difficulté suivante
        if (stor.difficulte_en_cours < stor.t_difficulte.length - 1) {
          stor.difficulte_en_cours++
          for (let i = 0; i <= stor.produits[stor.t_difficulte[stor.difficulte_en_cours]].length - 1; i++) {
            if (!stor.questions.includes(stor.produits[stor.t_difficulte[stor.difficulte_en_cours]][i])) { stor.questions.push(stor.produits[stor.t_difficulte[stor.difficulte_en_cours]][i]) }
          }
        } // On augmente la difficulté

        this.typederreurs[0]++ //  Compte les bonnes réponses
        return this.finCorrection('navigation', true)
      }

      // Mauvaise réponse
      stor.erreurs.push(stor.nombre) // Mémorise l’erreur
      if (ds.nbchances === 1) stor.erreurs.push(stor.nombre) // Il faudra réussir 2 fois pour confirmer son erreur

      j3pElement(prefix + 'correction').style.color = this.styles.cfaux // Commentaires à droite en rouge

      // Fini à cause de la limite de temps
      if (this.isElapsed) {
        j3pElement(prefix + 'correction').innerHTML = tempsDepasse // Commentaires à droite
        j3pDesactive(prefix + 'consigne1inputmq1')
        j3pBarre(prefix + 'consigne1inputmq1')
        this.typederreurs[10]++ //  Compte les temps dépassés
        // L’élève avait un temps limite mais ne l’a pas respecté. On affiche alors la correction
        afficheCorrection(true, repValidGlob)
        return this.finCorrection('navigation', true)
      }

      // Réponse fausse, commentaires à droite en rouge
      j3pElement(prefix + 'correction').innerHTML = cFaux
      if (me.essaiCourant < ds.nbchances) {
        // Il reste des essais
        j3pElement(prefix + 'correction').innerHTML += '<br>' + essaieEncore // Commentaires à droite
        this.typederreurs[1]++ //  Compte les essais ratés
        afficheChances() //  affiche le nombre de chances qui restent
        afficheCorrection(false, repValidGlob)
        // Il reste des essais, on reste en correction
        return this.finCorrection()
      }

      // erreur au dernier essai
      j3pElement(prefix + 'correction').innerHTML += '<br>' + regardeCorrection // Commentaires à droite
      // Là il ne peut plus se corriger. On lui affiche alors la solution
      afficheCorrection(true, repValidGlob)
      this.typederreurs[2]++ //  Compte les réponses fausses
      return this.finCorrection('navigation', true)
    } // case "correction":

    case 'navigation': {
      //  Si on a posé toutes les questions, on rajoute les erreurs et on continue
      if (stor.questions.length === 0) {
        stor.questions = stor.questions.concat(stor.erreurs)
        stor.erreurs.length = 0 // Et on vide le tableau des erreurs
      }
      //    Arrête les questions si toutes les réponses sont justes
      if (stor.questions.length === 0) ds.nbitems = me.questionCourante

      if (this.sectionTerminee()) {
        // On détermine la phrase d’état renvoyée par la section
        if ((this.typederreurs[1] === 0) && (this.typederreurs[2] === 0) && (this.typederreurs[10] === 0)) {
          //  Aucune erreur: Notion comprise
          if (ds.limite !== 0) this.parcours.pe = ds.pe_1 //  Aucune erreur en temps limité: Notion totalement maitrisée
          else this.parcours.pe = ds.pe_2
        } else if (this.typederreurs[2] + this.typederreurs[10] <= 2) {
          //  A pu corriger ses erreurs: Notion fragile
          if (this.typederreurs[3] >= 2) this.parcours.pe = ds.pe_5
          else if (this.typederreurs[4] >= 2) this.parcours.pe = ds.pe_6
          else if (this.typederreurs[5] >= 2) this.parcours.pe = ds.pe_7
          else if (this.typederreurs[6] >= 2) this.parcours.pe = ds.pe_8
          else this.parcours.pe = ds.pe_3
        } else {
          //  Notion non comprise
          this.parcours.pe = ds.pe_4
        }
      } else {
        this.etat = 'enonce'
      }
      // Obligatoire
      this.finNavigation()
      break
    }// case "navigation":
  } //  switch etat
} //  SectionPosition
