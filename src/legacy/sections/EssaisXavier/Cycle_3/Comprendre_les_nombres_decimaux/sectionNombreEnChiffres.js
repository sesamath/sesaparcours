import $ from 'jquery'
import { j3pActive, j3pBarre, j3pDiv, j3pElement, j3pEmpty, j3pFocus, j3pGetRandomInt, j3pNombreBienEcrit, j3pRestriction, j3pSpan, j3pValeurde } from 'src/legacy/core/functions'
import ValidationZones from 'src/legacy/outils/calculatrice/ValidationZones'
import { j3pAffiche, j3pDesactive } from 'src/lib/mathquill/functions'
import TableauConversion from 'src/legacy/outils/tableauconversion/TableauConversion'
import { entierEnMot } from 'src/lib/outils/conversion/nombreEnMots'
import { j3pCreeFenetres, j3pToggleFenetres, j3pCreeBoutonFenetre } from 'src/legacy/core/functionsJqDialog'
import textesGeneriques from 'src/lib/core/textes'

const { cBien, cFaux, essaieEncore, regardeCorrection, tempsDepasse } = textesGeneriques

/*
        Auteur  Xavier JEROME
        Date    15/08/2018
        Ecrire un nombre en chiffres
 */

export const params = {
  outils: ['mathquill'],
  /**
   * Ce qui est paramétrable dans l’éditeur de graphe
   * @type {LegacySectionParamsParametres}
   */
  parametres: [
    ['indication', '', 'string', 'Indication en bas à gauche'], // Texte que l’élève peut faire apparaître en bas de l’écran
    ['nbrepetitionsmax', 10, 'entier', 'Nombre de répétitions maximum de la section.<BR>Il peut être inférieur si on répond juste à toutes les questions'], // Obligatoire: nombre de questions posées pendant l’exercice
    ['nbchances', 2, 'entier', 'Nombre d’essais par répétition'], // Nombre d’essais autorisés avant de déclarer un échec et de passer à la question
    ['limite', 0, 'entier', 'Temps disponible par répétition, en secondes'], // S’il n’est pas vide, le temps de réponse sera limité
    ['n_niveau', 1, 'entier', "1 pour jusqu’aux unités de mille<BR>2 pour jusqu’aux centaines de mille<BR>jusqu'à 4 pour jusqu’aux centaines de milliards (le max)"], // Niveau de difficulté
    ['n_difficulte', 1, 'entier', '1 pour sans pièges<BR>2 pour les cas avec des zéros.'], // Niveau de difficulté
    ['n_aide', 4, 'entier', '0 pour aucune aide<BR>1 pour suggérer de lire le nombre<BR>2 pour présenter un tableau de conversion<BR>3 pour permettre de remplir un tableau de conversion<BR>4 pour donner la correction au final dans un tableau rempli'] // Niveau d’aide autorisé
  ],

  pe: [
    { pe_1: 'Notion totalement maitrisée.' },
    { pe_2: 'Notion comprise.' },
    { pe_3: 'Notion fragile.' },
    { pe_4: 'Notion non comprise.' },
    { pe_5: 'Notion fragile malgré le tableau de conversion vide.' },
    { pe_6: 'Notion fragile malgré le tableau de conversion complet.' },
    { pe_7: 'Notion fragile sans utilisation de l’aide.' },
    { pe_8: 'Notion comprise, mais la séparation des classes n’est pas acquise.' },
    { pe_9: 'Notion comprise, mais il reste des zéros inutiles.' },
    { pe_10: 'Notion comprise, mais des étourderies.' }
  ]
}

const idPrefix = 'NombreEnChiffres'
const textes = {
  phraseTitre: 'Ecrire un nombre en chiffres.', //  Titre de la section
  phraseNbChances: 'Il te reste £e essai£p.', //  Ne pas supprimer
  phraseEnonce: 'Ecris le nombre £n en chiffres @1@.',
  phraseSolution1: "Ce nombre s'écrit £r.",
  phraseSolution3: '£n : Sépare correctement les classes par des espaces.',
  phraseSolution6: ' : Evite d’écrire des zéros inutiles.',
  Aide00: 'J’ai compris mon erreur.',
  Aide01: 'Je ne comprends pas mon erreur.',
  Aide02: 'Je ne comprends pas comment cela m’aide.',
  Aide04: 'Regarde ce tableau.',
  Aide05: 'Je ne sais pas le remplir.',
  Aide07: 'Ecris le nombre dans le tableau<br>en mettant un chiffre dans chaque colonne<BR>en commençant par la droite.',
  Aide08: 'Lis ta réponse à haute voix et<BR>vérifie qu’elle corresponde au nombre en lettres.',
  Aide09: 'J’attends la correction.'
}

/**
 * section NombreEnChiffres
 * @this {Parcours}
 */
export default function main () {
  const me = this
  const stor = this.storage

  /// ////////////////////////////////////////////////////////////////////////////////////////
  //
  //                  Données et paramètres de la section
  //
  /// ////////////////////////////////////////////////////////////////////////////////////////

  function _Donnees () {
    /*      "presentation1"   6 cadres complets
            "presentation1bis"   manque le cadre indication en bas à droite
            "presentation2"   manque le cadre réponse à droite
            "presentation3"   un seul grand cadre en plus des deux entêtes
    */
    this.structure = 'presentation1'

    // Si indication != "" alors un lien "Indication" s’affiche dans la zone Inférieure Gauche (this.zones.IG
    // une indication<br>de deux lignes...";
    // Cette indication peut être changée par l’utilisateur dans la page paramètres
    this.indication = 'Lis ta réponse à haute voix et vérifie qu’elle correspond au nombre en lettres.' //  N’est utilisé que si le paramètre passé en entrée est vide
    // pour le cas de presentation1bis
    // En effet pas d’indication possible pour cette présentation
    // this.surchargeindication = false;

    this.video = [] // ["tableur1,Titre Video 1,Video 1","lebouquet,Titre Video2,Video 2"]

    // Nombre de répétitions de l’exercice
    this.nbrepetitionsmax = 5 // N’est utilisé que si le paramètre passé en entrée est vide
    this.nbetapes = 1 // Si chaque question est divisée en plusieurs étapes

    // Nombre de chances dont dispose l’élève pour répondre à la question
    this.nbchances = 2
    // Par défaut, pas de temps limite.
    this.limite = 0 //  N’est utilisé que si le paramètre passé en entrée est vide

    this.n_niveau = 1
    this.n_difficulte = 1
    this.n_aide = 4

    this.b_aide = false // Indique si les écrans d’aide sont affichés et interdit de répondre
    /*
    Phrase d’état renvoyée par la section
    */
    this.pe_1 = 'Notion totalement maitrisée'
    this.pe_2 = 'Notion comprise'
    this.pe_3 = 'Notion fragile'
    this.pe_4 = 'Notion non comprise.'
    this.pe_5 = 'Notion fragile malgré le tableau de conversion vide'
    this.pe_6 = 'Notion fragile malgré le tableau de conversion complet'
    this.pe_7 = 'Notion fragile sans utilisation de l’aide'
    this.pe_8 = 'Notion comprise, mais la séparation des classes n’est pas acquise.'
    this.pe_9 = 'Notion comprise, mais il reste des zéros inutiles.'
    this.pe_10 = 'Notion comprise, mais des étourderies.'

    /// ////////////////////////////////////////////////////////////////////////////////////////
    //
    //               Ajouter ici toutes les constantes nécessaires à la section
    //
    /// ////////////////////////////////////////////////////////////////////////////////////////
  }

  /// ////////////////////////////////////////////////////////////////////////////////////////
  //
  //              Ajouter ici toutes les fonctions nécessaires à la section
  //
  /// ////////////////////////////////////////////////////////////////////////////////////////

  //  Reformate un nombre
  function corrige_nombre (nombre) {
    //  Corrige un nombre saisi sous forme de texte
    //  Enlève les espaces, les zéros inutiles
    //  Met une virgule
    //  corrige la séparation des classes
    const motif = /^\d{0,3}(\s\d{3}){0,5}([,](\d{3}\s){0,5}\d{0,3}){0,1}$/
    if (nombre.length == 0) return [nombre]
    let classes = true
    nombre = nombre.trim() // Supprime les espaces inutiles
    nombre = nombre.replace('.', ',') // Remplace les points par des virgules
    if (nombre.indexOf(',') === -1) nombre += ',' //  Rajoute la virgule si c’est un entier
    while (nombre !== nombre.replace('  ', ' ')) nombre = nombre.replace('  ', ' ') //  Cas où il y a plus d’un espace entre les classes
    let rep_zeros = nombre // Nombre avant d’enlever les 0 inutiles
    if (rep_zeros.endsWith(',')) rep_zeros = rep_zeros.substr(0, rep_zeros.length - 1) //  Enlève la virgule si c’est un entier
    if (rep_zeros[0] == ',') rep_zeros = '0' + rep_zeros // Si on a enlevé le zéro des unités
    if (nombre[0] == ',') nombre = '0' + nombre // Si on a enlevé le zéro des unités
    while ((nombre[0] == ' ') || (nombre[0] == '0')) nombre = nombre.substr(1) //  Enlève les 0 et les espaces à gauche inutiles
    while (nombre.endsWith(' ') || nombre.endsWith('0')) nombre = nombre.substr(0, nombre.length - 1) //  Enlève les 0 et les espaces à droite inutiles
    if (nombre[0] == ',') nombre = '0' + nombre // Si on a enlevé le zéro des unités
    if (nombre.endsWith(',')) nombre = nombre.substr(0, nombre.length - 1) //  Enlève la virgule si c’est un entier
    const zeros = (nombre == rep_zeros) // Si le nombre a changé, c’est qu’il y avait des 0 inutiles
    if (!nombre.match(motif)) {
      classes = false
      while (nombre !== nombre.replace(' ', '')) nombre = nombre.replace(' ', '') //  Cas où il y a plus d’un espace entre les classes
      nombre = nombre.replace(',', '.') // Remplace les points par des virgules
      nombre = j3pNombreBienEcrit(parseFloat(nombre))
      nombre = nombre.replace('.', ',') // Remplace les points par des virgules
    } //  match
    return [nombre, classes, zeros, rep_zeros]
    // Non vide
  } //  corrige_nombre

  //   Redimensionne la zone de saisie si on a changé le contenu
  function Redimensionne (nomzone) {
    // fonction appelée pour afficher la correction (que la réponse soit bonne ou fausse
    const font_zone = j3pElement(nomzone).style.fontFamily
    const taille_font_zone = j3pElement(nomzone).style.fontSize
    const texte_zone = j3pElement(nomzone).value
    const longueur = $('#' + nomzone).textWidth(texte_zone, font_zone, taille_font_zone)
    const decalage = 6
    j3pElement(nomzone).style.width = (longueur + 2 * decalage) + 'px'
  }// Redimensionne

  //   Affiche la correction sous l’énoncé
  // fonction appelée pour afficher la correction (que la réponse soit bonne ou fausse
  function affiche_correction (fin) {
    let i
    const s_rep_eleve = j3pValeurde(idPrefix + 'consigne1input1')
    // Si la réponse est juste, il n’y a rien à apporter de plus
    if (!reponse.bonneReponse && fin) {
      j3pElement(idPrefix + 'explication1').innerHTML = '' //  Pour effacer  un éventuel précédent affichage
      j3pAffiche(idPrefix + 'explication1', idPrefix + 'corr1', textes.phraseSolution1,
        {
          styletexte: '',
          r: stor.s_nombre //  Nombre formaté avec les espaces
        })

      j3pElement(idPrefix + 'explication3').innerHTML = '' //  Pour effacer  un éventuel précédent affichage
      affiche_tableau(3)
    }
    // bonne réponse
    //  Indique si il y avait des zeros inutiles
    if (!me.zeros_1) {
      j3pDiv(idPrefix + 'explication2', idPrefix + 'corr27', '')
      var s_debut1 = ''
      if (parseFloat(s_rep_eleve) == 0) {
        s_debut1 = me.rep_zeros_1.substring(0, me.rep_zeros_1.length - 1)
      } else {
        for (i = 0; i <= me.s_rep_eleve1.length - 1 && (me.s_rep_eleve1.charAt(i) == '0' || me.s_rep_eleve1.charAt(i) == ' '); i++) { s_debut1 += me.s_rep_eleve1.charAt(i) }
      }
      if ((s_debut1.length != 0) && (me.donneesSection.n_aide >= 1)) {
        j3pSpan(idPrefix + 'corr27', { id: idPrefix + 'Aide271', contenu: s_debut1 }).style.color = me.styles.cfaux
        j3pSpan(idPrefix + 'corr27', { id: idPrefix + 'Aide272', contenu: s_rep_eleve })
        j3pAffiche(idPrefix + 'corr27', idPrefix + 'corr270', textes.phraseSolution6,
          { styletexte: '' }) //  Affiche le nombre correctement écrit
      }
      // Erreur à afficher
    }
    //  Erreur zéros 1

    //  Indique si les classes étaient correctement séparées
    if ((!me.espaces_1) && (me.donneesSection.n_aide >= 1)) {
      j3pDiv(idPrefix + 'explication2', idPrefix + 'corr21', '')
      j3pAffiche(idPrefix + 'corr21', idPrefix + 'corr210', textes.phraseSolution3,
        {
          styletexte: '',
          n: me.s_rep_eleve1
        }) //  Affiche le nombre correctement écrit
    }
    //  Erreur espace 1
    if (!me.espaces_1) me.typederreurs[6]++ //  Les séparations de classes ne sont pas correctes
    if (!me.zeros_1 && (s_debut1.length !== 0)) me.typederreurs[7]++ // Zéros inutiles

    // Détecte une simple erreur de chiffre
    let etourderie = stor.s_nombre.length === s_rep_eleve.length
    for (i = 0; i < stor.s_nombre.length && etourderie; i++) { etourderie = etourderie && (stor.s_nombre.charAt(stor.s_nombre.length - i) === s_rep_eleve.charAt(s_rep_eleve.length - i) || (s_rep_eleve.charAt(s_rep_eleve.length - i) !== ' ' && s_rep_eleve.charAt(s_rep_eleve.length - i) !== '0' && stor.s_nombre.charAt(stor.s_nombre.length - i) !== ' ' && stor.s_nombre.charAt(stor.s_nombre.length - i) !== '0')) }
    if (etourderie) me.typederreurs[3]++
  } // affiche correction

  //   Affiche un tableau des positions des chiffres
  function affiche_tableau (type) {
    const elt = j3pElement(idPrefix + 'explication3')
    switch (type) {
      case 0 :
        j3pEmpty(elt)
        break

      case 1 :
        // Un tableau vide non modifiable
        // On crée un div dans explications
        elt.innerHTML = ''
        me.tableau = new TableauConversion(idPrefix + 'explication3',
          {

            idDIV: 'tableau',
            format: {
              virgule: [false], // On ne met la virgule que si nécessaire
              epaisseurs: {},
              couleurs: {}
            },
            lignes: [[(stor.rang_en_cours > 9 ? ['.', '.', '.'] : []), // milliards si besoin
              (stor.rang_en_cours > 6 ? ['.', '.', '.'] : []),
              (stor.rang_en_cours > 3 ? ['.', '.', '.'] : []),
              ['.', '.', '.'], // Unités
              ([]), // Décimaux si besoin
              []]] // Jamais de millionièmes
          })
        break

      case 2 :
        // Un tableau vide modifiable
        elt.innerHTML = ''
        me.tableau = new TableauConversion(idPrefix + 'explication3',
          {

            idDIV: 'tableau',
            format: {
              virgule: [false], // On ne met la virgule que si nécessaire
              epaisseurs: {},
              couleurs: {}
            },
            lignes: [[(stor.rang_en_cours > 9 ? ['?_', '?_', '?_'] : []), // milliards si besoin
              (stor.rang_en_cours > 6 ? ['?_', '?_', '?_'] : []),
              (stor.rang_en_cours > 3 ? ['?_', '?_', '?_'] : []),
              ['?_', '?_', '?_'], // Unités
              ([]), // Décimaux si besoin
              []]]
          })
        break

      case 3 : { // Un tableau avec le nombre demandé
        elt.innerHTML = ''
        me.tableau = new TableauConversion(idPrefix + 'explication3',
          {

            idDIV: 'tableau',
            format: {
              virgule: [false], // On ne met la virgule que si nécessaire
              epaisseurs: {},
              couleurs: {}
            },
            lignes: [[(stor.rang_en_cours > 9 ? [(stor.tableau_reponse[0] === ' ' ? '.' : stor.tableau_reponse[0]), (stor.tableau_reponse[1] === ' ' ? '.' : stor.tableau_reponse[1]), stor.tableau_reponse[2]] : []), // milliards si besoin
              (stor.rang_en_cours > 6 ? [(stor.tableau_reponse[3] === ' ' ? '.' : stor.tableau_reponse[3]), (stor.tableau_reponse[4] === ' ' ? '.' : stor.tableau_reponse[4]), stor.tableau_reponse[5]] : []),
              (stor.rang_en_cours > 3 ? [(stor.tableau_reponse[6] === ' ' ? '.' : stor.tableau_reponse[6]), (stor.tableau_reponse[7] === ' ' ? '.' : stor.tableau_reponse[7]), stor.tableau_reponse[8]] : []),
              [(stor.tableau_reponse[9] === ' ' ? '.' : stor.tableau_reponse[9]), (stor.tableau_reponse[10] === ' ' ? '.' : stor.tableau_reponse[10]), stor.tableau_reponse[11]], // Unités
              ([]), // Décimaux si besoin
              []]]
          })
        break
      }
    }
  }

  //   Cache tous les écrans d’aide
  function cache_aide () {
    //  Obligé de tous les faire pour le cas où la limite de temps intervient pendant l’aide
    ['Aide0', 'Aide1', 'Aide3', 'Aide4', 'Aide6', 'Aide7'].forEach(function (item) {
      $('div[name="' + item + '"]').dialog('close')
    })
    affiche_tableau(0)
    me.afficheBoutonValider()
    me.donneesSection.b_aide = false
    j3pActive(idPrefix + 'consigne1input1')
    j3pFocus(idPrefix + 'consigne1input1')
  }

  // Affiche le nombre de tentatives qu’il reste s’il y a plusieurs chances
  function affiche_chances () {
    j3pElement(idPrefix + 'zone_chances').innerHTML = ''
    if (me.donneesSection.nbchances > 1) {
      j3pAffiche(idPrefix + 'zone_chances', idPrefix + 'chances', textes.phraseNbChances,
        {
          e: me.donneesSection.nbchances - me.essaiCourant,
          p: (me.donneesSection.nbchances - me.essaiCourant > 1 ? 's' : '')
        })
    }
    // Pour le pluriel
  }

  /// ////////////////////////////////////////////////////////////////////////////////////////
  //
  //              Déroulement de la section Enonce, Correction, navigation
  //
  /// ////////////////////////////////////////////////////////////////////////////////////////

  //  Permet de désigner la section
  const la_section = me.SectionNombreEnChiffres // Modification du nom n°4
  let i

  switch (this.etat) {
    /// ////////////////////////////////////////////////////////////////////////////////////////
    //
    //            Phase énoncé
    //
    /// ////////////////////////////////////////////////////////////////////////////////////////

    case 'enonce': {
      //  Construit l’énoncé: exécuté au début de chaque question
      if (this.debutDeLaSection) {
      //  Code exécuté uniquement au lancement de la section
        //  Partie à ne pas changer
        // Construit les données de la section
        this.donneesSection = new _Donnees()

        this.surcharge()

        // Construction de la page
        this.construitStructurePage(this.donneesSection.structure)

        this.donneesSection.nbrepetitions = this.donneesSection.nbrepetitionsmax // C’est cette valeur qui est affichée dans l’entête
        this.donneesSection.nbitems = this.donneesSection.nbetapes * this.donneesSection.nbrepetitionsmax // Initialisation à ne pas modifier

        this.score = 0

        this.afficheTitre(textes.phraseTitre)

        // Affiche l’indication en bas à gauche
        if (this.donneesSection.indication) this.indication(this.zones.IG, this.donneesSection.indication)

        //  Paramètres à renseigner
        /*
           Par convention,`
          `   this.typederreurs[0] = nombre de bonnes réponses
              this.typederreurs[1] = nombre de fois où l’élève a utilisé une chance supplémentaire
              this.typederreurs[2] = nombre de mauvaises réponses
              this.typederreurs[3] = nombre d’étourderies
              this.typederreurs[4] = nombre de fois où il a utilisé le tableau vide
              this.typederreurs[5] = nombre de fois où il a utilisé le tableau rempli
              this.typederreurs[6] = nombre de fois où les classes ne sont pas séparées
              this.typederreurs[7] = nombre de fois où il y a des zéros inutiles
              this.typederreurs[10] = nombre de fois où l’élève a eu faux à cause d’un temps limite
              LES AUTRES peuvent être utilisées par la détermination de la pe dans le cas d’une section qualitative
           */
        this.typederreurs = [0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0]

        // Permet l’utilisation du compte à rebours si un temps est défini dans les paramètre

        //  Paramètres et variables particuliers à la section
        // rang est la position maximum de la question
        stor.rang_en_cours = 4 //  Au niveau 1 on commence aux dizaines, sinon aux milliers
        stor.difficulte_en_cours = 1 // On commence sans cas particulier 2 pour un zéro par groupe 3 pour 2 zéros par groupe 4 pour un groupe de 0 5 pour plusieurs groupes de 0
      } else {
      //  Code exécuté à chaque nouvelle question: Partie à ne pas modifier
        // A chaque répétition, on nettoie la scène
        this.videLesZones()
      } // Code exécuté à chaque nouvelle question: Partie à ne pas modifier

      //  Partie exécutée à chaque essai
      //  Partie à ne pas modifier
      /// ////////////////////////////////////////////////////////////////////////////////////////
      //
      //               Création de l’énoncé
      //
      /// ////////////////////////////////////////////////////////////////////////////////////////

      //  Création de l’énoncé
      //    Conventions de la manipulation des nombres affichés
      /**************************************************************************************
           *
           *                       999 999 999 999
           *     question         12             1
           *     n_niveau  1                 - ---
           *               2               --- ---
           *               3           --- --- ---
           *               4       --- --- --- ---
           *
           ***************************************************************************************/
      //  Le nombre sous forme de tableau de chiffres
      // Modifié en attendant es6
      // stor.tableau_reponse = new Array(11).fill(' ') // Crée un nombre vide
      stor.tableau_reponse = []
      for (i = 0; i < 11; i++) stor.tableau_reponse.push(' ')

      for (i = 11; i >= 12 - (stor.rang_en_cours - (stor.rang_en_cours >= 6 ? j3pGetRandomInt(0, 2) : 0)); i--) {
        //  On choisit le nombre de chiffres significatifs rajoutés avant
        stor.tableau_reponse[i] = j3pGetRandomInt(1, 9)
      }

      let groupe
      for (groupe = 1; groupe <= 7 && stor.tableau_reponse[12 - (groupe + 3)] != ' '; groupe += 3) {
        // On rajoute des zéros à chaque groupe selon la difficulté
        switch (stor.difficulte_en_cours) {
          case 2:
            stor.tableau_reponse[12 - (j3pGetRandomInt(0, 2) + groupe)] = 0
            break

          case 3:
            var conserve = j3pGetRandomInt(0, 2)
            for (i = 0; i <= 2; i++) {
              if (i != conserve) {
                stor.tableau_reponse[12 - (i + groupe)] = 0
              }
            }
            break

          case 5:
            switch (j3pGetRandomInt(1, 3)) {
              case 1:
                stor.tableau_reponse[12 - (j3pGetRandomInt(0, 2) + groupe)] = 0
                break
              case 2: {
                const conserve = j3pGetRandomInt(0, 2)
                for (i = 0; i <= 2; i++) {
                  if (i != conserve) {
                    stor.tableau_reponse[12 - (i + groupe)] = 0
                  }
                }
                break
              }
              case 3:
                if (stor.tableau_reponse[12 - (groupe + 3)] != ' ') {
                  for (i = 0; i <= 2; i++) stor.tableau_reponse[12 - (i + groupe)] = 0
                }
                break
            }
            // random
        }
      }
      // difficulte_en_cours
      if (stor.difficulte_en_cours == 4) {
        groupe = j3pGetRandomInt(0, Math.round(stor.rang_en_cours / 3) - 1) * 3 + 1
        if (stor.tableau_reponse[12 - (groupe + 3)] != ' ') {
          for (i = 0; i <= 2; i++) stor.tableau_reponse[12 - (i + groupe)] = 0
        }
      }

      // nombre est le nombre proposé
      stor.nombre = parseFloat(stor.tableau_reponse.join('')) // On le transforme en nombre réel
      stor.s_nombre = j3pNombreBienEcrit(stor.nombre)

      //  Création des conteneurs
      // Création du conteneur dans lequel se trouveront toutes les consignes
      j3pDiv(this.zones.MG, {
        id: idPrefix + 'consignes',
        contenu: '',
        coord: [0, 0],
        style: me.styles.etendre('petit.enonce', { padding: '10px', marginTop: '25px' })
      })

      // Dans ce conteneur, on crée un div par phrase (cela forcera le retour à la ligne)
      j3pDiv(idPrefix + 'consignes', idPrefix + 'zone_consigne1', '')

      // Affiche le nombre de chances qu’il reste
      j3pDiv(idPrefix + 'consignes', {
        id: idPrefix + 'zone_chances',
        contenu: '',
        style: me.styles.etendre('moyen.enonce', { marginTop: '30px' })
      })
      affiche_chances()

      // Si on veut rajouter des explications, en cas de réponse fausse par exemple
      j3pDiv(this.zones.MG, {
        id: idPrefix + 'explications',
        contenu: '',
        coord: [10, 200],
        style: me.styles.petit.explications
      })
      j3pDiv(idPrefix + 'explications', idPrefix + 'explication1', '')
      j3pDiv(idPrefix + 'explications', idPrefix + 'explication2', '')
      j3pDiv(idPrefix + 'explications', { id: idPrefix + 'explication3', coord: [10, 10] })
      j3pElement(idPrefix + 'explication2').style.marginTop = '20px'
      j3pElement(idPrefix + 'explication3').style.marginTop = '20px'

      // On crée ici le div qui contiendra toutes les remarques sur ce qu’on pense de la réponse de l’élève (le plus souvant dans le cadre MD)
      j3pDiv(this.zones.MD, {
        id: idPrefix + 'correction',
        contenu: '',
        coord: [0, 0],
        style: me.styles.etendre('petit.correction', { padding: '10px' })
      })

      //  Initialisation des zones de saisie
      {
        j3pAffiche(idPrefix + 'zone_consigne1', idPrefix + 'consigne1', textes.phraseEnonce, {
          n: entierEnMot(Math.round(stor.nombre)),
          input1: { dynamique: true, width: '12px' }
        })

        // Le nom de chaque zone de saisie est construit de la même façon pour les zones input et inputmq :
        // le nom input est toujours précédé du nom du span qui contient la zone (ce span est le deuxième paramètre de la fonction j3pAffiche

        // On rajoute les contraintes de saisie
        j3pRestriction(idPrefix + 'consigne1input1', '0-9 ') // on n’autorise que les chiffres

        //  Permet de refaire passer la zone de saisie en noir lors du deuxième essai
        j3pElement(idPrefix + 'consigne1input1').addEventListener('keypress', function () {
          j3pElement(idPrefix + 'consigne1input1').style.color = ''
        }, false)

        // On rajoute toutes les informations qui permettront de valider la saisie
        j3pElement(idPrefix + 'consigne1input1').typeReponse = ['texte']
        j3pElement(idPrefix + 'consigne1input1').reponse = [stor.s_nombre]

        // Paramétrage de la validation
        // mes_zones_saisie est un tableau contenant le nom de chacune des zones (on pourrait aussi y mettre une liste déroulante)
        const mes_zones_saisie = [idPrefix + 'consigne1input1']
        // validePerso est souvant un tableau vide
        // Mais parfois certaines zones ne peuvent pas être validée de manière automatique
        // Donc la validation de ces zones devra se faire "à la main".
        // Par contre on vérifiera si elles sont remplies ou non.
        const mes_zones_valide_perso = []

        // fcts_valid est un objet permettant la validation. Il contient le nom des zones (tableau précédent) et un deuxième paramètre validePerso
        la_section.fcts_valid = new ValidationZones({
          parcours: me,
          zones: mes_zones_saisie,
          validePerso: mes_zones_valide_perso
        })
      } //  Initialisation des zones de saisie

      //  Prépare les écrans d’aide
      this.fenetresjq = [
        {
          name: 'Aide0',
          title: 'Aides',
          width: 400,
          height: 50 + 2 * 50,
          left: '90%',
          top: '50%',
          id: 'fenetreAide0'
        }, // Propose de l’aide
        {
          name: 'Aide1',
          title: 'Aides',
          width: 400,
          height: 50 + 70 + 3 * 50,
          left: '90%',
          top: '50%',
          id: 'fenetreAide1'
        }, // Demande de lire le nombre
        {
          name: 'Aide3',
          title: 'Aides',
          width: 400,
          height: 50 + 2 * 50,
          left: '90%',
          top: '50%',
          id: 'fenetreAide3'
        }, // Propose un tableau des positions
        {
          name: 'Aide4',
          title: 'Aides',
          width: 400,
          height: 50 + 2 * 50,
          left: '90%',
          top: '50%',
          id: 'fenetreAide4'
        }, // Affiche le tableau
        {
          name: 'Aide6',
          title: 'Aides',
          width: 400,
          height: 50 + 60 + 2 * 50,
          left: '90%',
          top: '50%',
          id: 'fenetreAide6'
        }, // Tableau à remplir
        {
          name: 'Aide7',
          title: 'Aides',
          width: 400,
          height: 50 + 2 * 50,
          left: '90%',
          top: '50%',
          id: 'fenetreAide7'
        }] // Tableau rempli
      j3pCreeFenetres(this)

      // Aide 0
      j3pCreeBoutonFenetre('Bouton01', 'fenetreAide0', {}, textes.Aide01) // Je ne comprends pas mon erreur
      j3pElement('BTN_fenetreBouton01').onclick = function onclick (event) {
        j3pToggleFenetres('Aide0')
        j3pToggleFenetres('Aide1')
      } //  Cache l’écran actuel et affiche l’écran suivant
      j3pCreeBoutonFenetre('Bouton02', 'fenetreAide0', {}, textes.Aide00) // J’ai compris mon erreur
      j3pElement('BTN_fenetreBouton02').onclick = function onclick (event) { cache_aide() } // Sort de l’aide

      // Aide 1
      j3pDiv('fenetreAide1', {
        id: 'idaide1',
        contenu: textes.Aide08,
        style: this.styles.etendre('toutpetit.enonce')
      })
      if (me.donneesSection.n_aide > 1) {
        j3pCreeBoutonFenetre('Bouton11', 'fenetreAide1', {}, textes.Aide02) // Je ne comprends pas comment cela m’aide.
        j3pElement('BTN_fenetreBouton11').onclick = function onclick (event) {
          j3pToggleFenetres('Aide1')
          j3pToggleFenetres('Aide3')
        }
      }
      j3pCreeBoutonFenetre('Bouton14', 'fenetreAide1', {}, textes.Aide09) // J’attends la correction.
      j3pElement('BTN_fenetreBouton14').onclick = function onclick (event) { cache_aide() }
      j3pCreeBoutonFenetre('Bouton13', 'fenetreAide1', {}, textes.Aide00) // J’ai compris mon erreur
      j3pElement('BTN_fenetreBouton13').onclick = function onclick (event) { cache_aide() }

      // Aide 3
      j3pCreeBoutonFenetre('Bouton31', 'fenetreAide3', {}, textes.Aide04) // Regarde ce tableau.
      j3pElement('BTN_fenetreBouton31').onclick = function onclick (event) {
        j3pToggleFenetres('Aide3')
        affiche_tableau(1)
        j3pToggleFenetres('Aide4')
        me.typederreurs[4]++
      } // Affiche un tableau vide
      j3pCreeBoutonFenetre('Bouton32', 'fenetreAide3', {}, textes.Aide00) // J’ai compris mon erreur
      j3pElement('BTN_fenetreBouton32').onclick = function onclick (event) { cache_aide() }

      // Aide 4
      if (me.donneesSection.n_aide > 2) {
        j3pCreeBoutonFenetre('Bouton42', 'fenetreAide4', {}, textes.Aide02) // Je ne comprends pas comment cela m’aide.
        j3pElement('BTN_fenetreBouton42').onclick = function onclick (event) {
          j3pToggleFenetres('Aide4')
          affiche_tableau(2)
          j3pToggleFenetres('Aide6')
        }
      } else {
        j3pCreeBoutonFenetre('Bouton42', 'fenetreAide4', {}, textes.Aide09) // J’attends la correction.
        j3pElement('BTN_fenetreBouton42').onclick = function onclick (event) { cache_aide() }
      }
      j3pCreeBoutonFenetre('Bouton43', 'fenetreAide4', {}, textes.Aide00) // J’ai compris mon erreur
      j3pElement('BTN_fenetreBouton43').onclick = function onclick (event) { cache_aide() }

      // Aide 6
      j3pDiv('fenetreAide6', {
        id: 'idaide6',
        contenu: textes.Aide07,
        style: this.styles.etendre('toutpetit.enonce')
      }) //  Consigne différente s’il y a effectivement une virgule ou pas
      if (me.donneesSection.n_aide > 3) {
        j3pCreeBoutonFenetre('Bouton61', 'fenetreAide6', {}, textes.Aide05) // Je ne sais pas le remplir.
        j3pElement('BTN_fenetreBouton61').onclick = function onclick (event) {
          j3pToggleFenetres('Aide6')
          affiche_tableau(3)
          j3pToggleFenetres('Aide7')
          me.typederreurs[5]++
        } //  Affiche un tableau déjà remplis
      } else {
        j3pCreeBoutonFenetre('Bouton61', 'fenetreAide6', {}, textes.Aide09) // J’attends la correction.
        j3pElement('BTN_fenetreBouton61').onclick = function onclick (event) { cache_aide() }
      }
      j3pCreeBoutonFenetre('Bouton62', 'fenetreAide6', {}, textes.Aide00) // J’ai compris mon erreur
      j3pElement('BTN_fenetreBouton62').onclick = function onclick (event) { cache_aide() }

      // Aide 7
      j3pCreeBoutonFenetre('Bouton71', 'fenetreAide7', {}, textes.Aide09) // J’attends la correction.
      j3pElement('BTN_fenetreBouton71').onclick = function onclick (event) { cache_aide() } //  Affiche la réponse si c’est autorisé
      j3pCreeBoutonFenetre('Bouton72', 'fenetreAide7', {}, textes.Aide00) // J’ai compris mon erreur
      j3pElement('BTN_fenetreBouton72').onclick = function onclick (event) { cache_aide() }

      //  Interdit la fermeture des écrans d’aide avec escape
      ;['Aide0', 'Aide1', 'Aide3', 'Aide4', 'Aide6', 'Aide7'].forEach((item) => $('div[name="' + item + '"]').dialog({ closeOnEscape: false }))
      //  Prépare les écrans d’aide

      //  Pour l’instant sans effet, mais sera là lorsque le focus sur la première question sera libéré
      j3pFocus(idPrefix + 'consigne1input1', true)

      this.finEnonce() // Obligatoire
      //  Partie exécutée à chaque essai
      break
    }
    // case "enonce":

    /// ////////////////////////////////////////////////////////////////////////////////////////
    //
    //            Phase correction
    //
    /// ////////////////////////////////////////////////////////////////////////////////////////

    case 'correction': {
      if (!me.donneesSection.b_aide || this.isElapsed) {
        // On ne tient pas compte des réponses pendant la phase d’aide, sauf si la limite de temps est atteinte
        // Permet de nommer facilement les conteneurs
        const fcts_valid = la_section.fcts_valid // ici je crée juste une variable pour raccourcir le nom de l’object fcts_valid

        //  Efface le nombre de chances qui restent
        j3pElement(idPrefix + 'zone_chances').innerHTML = ''

        //  Efface les éléments de correction
        j3pElement(idPrefix + 'explication1').innerHTML = ''
        j3pElement(idPrefix + 'explication2').innerHTML = ''
        j3pElement(idPrefix + 'explication3').innerHTML = ''

        this.s_rep_eleve1 = j3pValeurde(idPrefix + 'consigne1input1') //  Récupère la réponse
        this.rep_eleve1 = parseFloat(this.s_rep_eleve1)
        // Modifié en attendant es6
        //        [j3pElement(idPrefix + 'consigne1input1').value, this.espaces_1, this.zeros_1, this.rep_zeros_1] = corrige_nombre(j3pValeurde(idPrefix + 'consigne1input1')) // Remplace la saisie par un nombre plus propre
        const resultat = corrige_nombre(j3pValeurde(idPrefix + 'consigne1input1'))
        j3pElement(idPrefix + 'consigne1input1').value = resultat[0]
        this.espaces_1 = resultat[1]
        this.zeros_1 = resultat[2]
        this.rep_zeros_1 = resultat[3]

        Redimensionne(idPrefix + 'consigne1input1')

        // ce qui suit sert pour la validation de toutes les zones
        // le tableau contenant toutes les zones de saisie
        var reponse = fcts_valid.validationGlobale() // Ne pas modifier
        // A cet instant reponse contient deux éléments :
        // reponse.aRepondu qui me dit si toutes les zones ont bien été complétées ou non
        // reponse.bonneReponse qui me dit si les zones qui ne sont pas dans le tableau valide_pero ont des réponses correctes ou non.
        // Si reponse.aRepondu vaut true, la fonction met aussi en bleu ou rouge les réponses bonnes ou fausses.
        // Si c’est la dernière tentative de répondre, on barre également les zones fausses

        // Réponse vide ou incomplète et la limite de temps n’est pas atteinte
        if ((!reponse.aRepondu) && (!this.isElapsed)) {
          this.reponseManquante(idPrefix + 'correction')

          affiche_chances() //  affiche le nombre de chances qui restent
        } else {
        // Une réponse a été saisie
          if (reponse.bonneReponse) {
            // Bonne réponse
            this.score++
            j3pElement(idPrefix + 'correction').style.color = this.styles.cbien
            j3pElement(idPrefix + 'correction').innerHTML = cBien

            // Même si la réponse est bonne on peut afficher des éléments de correction/explication :
            affiche_correction(true)

            // Augmente le niveau de difficulté
            // Pour le niveau 1 on passe de dizaines à milliers en augmentant de 1
            // Pour les autres niveaux, on commence par unités de milles, puis classe des milliers, millions, etc selon le niveau
            const rang_precedent = stor.rang_en_cours
            if (this.donneesSection.n_niveau > 1) {
              (stor.rang_en_cours == 4 ? stor.rang_en_cours = 6 : stor.rang_en_cours = stor.rang_en_cours + (stor.rang_en_cours < this.donneesSection.n_niveau * 3 ? 3 : 0))
            }
            if (rang_precedent == stor.rang_en_cours && this.donneesSection.n_difficulte == 2 && stor.difficulte_en_cours < 5) stor.difficulte_en_cours++

            this.typederreurs[0]++ //  Compte les bonnes réponses
            this.cacheBoutonValider() //  Plus de réponse à donner
            this.etat = 'navigation'
            this.sectionCourante() //  Passe à la question suivante
          } else {
          // Mauvaise réponse
            j3pElement(idPrefix + 'correction').style.color = this.styles.cfaux // Commentaires à droite en rouge

            // Fini à cause de la limite de temps
            if (this.isElapsed) {
              cache_aide() //  Cache tous les écrans d’aide

              j3pElement(idPrefix + 'correction').innerHTML = tempsDepasse // Commentaires à droite

              j3pDesactive(idPrefix + 'consigne1input1')
              j3pBarre(idPrefix + 'consigne1input1')

              this.typederreurs[10]++ //  Compte les temps dépassés
              this.cacheBoutonValider() //  Plus de réponse à donner

              // L’élève avait un temps limite mais ne l’a pas respecté. On affiche alors la correction
              affiche_correction(true)

              this.etat = 'navigation'
              this.sectionCourante() //  Passe à la question suivante
            } else {
            // Réponse fausse
              j3pElement(idPrefix + 'correction').innerHTML = cFaux // Commentaires à droite en rouge

              // Il reste des essais
              if (this.essaiCourant < this.donneesSection.nbchances) {
                j3pElement(idPrefix + 'correction').innerHTML += '<br>' + essaieEncore // Commentaires à droite

                this.typederreurs[1]++ //  Compte les essais ratés

                affiche_chances() //  affiche le nombre de chances qui restent

                affiche_correction(false)

                //  Affiche l’écran d’aide
                if (this.donneesSection.n_aide > 0) {
                  j3pToggleFenetres('Aide0')
                  j3pDesactive(idPrefix + 'consigne1input1')
                  me.donneesSection.b_aide = true //  Interdit la saisie pendant les écrans d’aide
                  this.cacheBoutonValider()
                }
                // indication éventuelle ici
              } else {
              // Il ne reste plus d’essais
                j3pElement(idPrefix + 'correction').innerHTML += '<br>' + regardeCorrection // Commentaires à droite
                //
                // Là il ne peut plus se corriger. On lui affiche alors la solution
                affiche_correction(true)

                this.typederreurs[2]++ //  Compte les réponses fausses

                this.etat = 'navigation'
                this.sectionCourante() //  Passe à la question suivante
              }
            }
          }
        }
        this.finCorrection()
      }
      //  Hors phase d’aide
      break
    }
    // case "correction":

    /// ////////////////////////////////////////////////////////////////////////////////////////
    //
    //            Phase navigation
    //
    /// ////////////////////////////////////////////////////////////////////////////////////////

    case 'navigation': {
      if (this.sectionTerminee()) {
        // On détermine la phrase d’état renvoyée par la section
        if (this.typederreurs[1] + this.typederreurs[2] + this.typederreurs[10] == this.typederreurs[3]) {
          this.parcours.pe = me.donneesSection.pe_10
        } else if ((this.typederreurs[1] == 0) && (this.typederreurs[2] == 0) && (this.typederreurs[10] == 0)) {
          //  Aucune erreur: Notion comprise
          if (this.donneesSection.limite != 0) {
            this.parcours.pe = me.donneesSection.pe_1
          } else if (this.typederreurs[6] >= 2) {
            //  Aucune erreur en temps limité: Notion totalement maitrisée
            this.parcours.pe = me.donneesSection.pe_8
          } else if (this.typederreurs[7] >= 2) {
            this.parcours.pe = me.donneesSection.pe_9
          } else {
            this.parcours.pe = me.donneesSection.pe_2
          }
        } else if (this.typederreurs[2] + this.typederreurs[10] <= 2) {
        //  Aucune erreur sans temps limité: Notion comprise
          if (this.typederreurs[5] >= 2) {
            this.parcours.pe = me.donneesSection.pe_6
          } else if (this.typederreurs[4] >= 2) {
            this.parcours.pe = me.donneesSection.pe_5
          } else if (me.donneesSection.n_aide > 0) {
            this.parcours.pe = me.donneesSection.pe_7
          } else {
          //  A pu corriger ses erreurs: Notion fragile
            this.parcours.pe = me.donneesSection.pe_3
          }
        } else {
          this.parcours.pe = me.donneesSection.pe_4
        } //  Notion non comprise

        this.afficheBoutonSectionSuivante()
        this.focus('sectioncontinuer')
      } else {
        this.etat = 'enonce'
        this.afficheBoutonSuite()
        this.focus('boutoncontinuer')
      }
      // Obligatoire
      this.finNavigation()

      break
    } // case "navigation":
  } //  switch etat
} //  SectionNombreEnChiffres
