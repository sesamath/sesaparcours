import { j3pAddElt, j3pBarre, j3pChaine, j3pDiv, j3pElement, j3pEmpty, j3pEnvironEgal, j3pFocus, j3pGetRandomInt, j3pNombreBienEcrit, j3pRemplacePoint, j3pRestriction, j3pShowError, j3pSpan, j3pValeurde } from 'src/legacy/core/functions'
import { colorKo } from 'src/legacy/core/StylesJ3p'
import ValidationZones from 'src/legacy/outils/calculatrice/ValidationZones'
import { j3pAffiche } from 'src/lib/mathquill/functions'
import BulleAide from 'src/legacy/outils/bulleAide/BulleAide'
import textesGeneriques from 'src/lib/core/textes'

const { cBien, cFaux, essaieEncore, regardeCorrection, tempsDepasse } = textesGeneriques

/*
        Auteur  Xavier JEROME
        Date    25/06/2018
        Encadrer un nombre

        Remarques
            Fait à partir de l’exercice J3P cm2exN1_44
            développé par Abdel Sarraf en 2012
 */

export const params = {
  outils: ['mathquill'],
  /**
   * Ce qui est paramétrable dans l’éditeur de graphe
   * @type {LegacySectionParamsParametres}
   */
  parametres: [
    ['titre', 'Encadre un nombre à ... près.', 'string', 'Titre de l’exercice.'],
    ['indication', 'Vérifie que tes deux nombres sont bien consécutifs<br> et qu’il n’y a que des zéros après le rang demandé.', 'string', 'Indication en bas à gauche'], // Texte que l’élève peut faire apparaître en bas de l’écran
    ['nbrepetitionsmax', 10, 'entier', 'Nombre de répétitions maximum de la stor.<BR>Il peut être inférieur si on répond juste à toutes les questions'], // Obligatoire: nombre de questions posées pendant l’exercice
    ['nbchances', 2, 'entier', 'Nombre d’essais par répétition'], // Nombre d’essais autorisés avant de déclarer un échec et de passer à la question
    ['limite', 0, 'entier', 'Temps disponible par répétition, en secondes'], // S’il n’est pas vide, le temps de réponse sera limité
    ['b_niveau', true, 'boolean', '<u>True</u> si on ajoute les cas particuliers: 2.99999 (retenue à droite) et 0.00007 (0 à gauche).'], // Niveau de difficulté
    ['t_rangs', [1, -2], 'array', "Intervalle des rangs autorisés<BR>de 3 pour millier (le max)<BR>0 pour unité<BR>jusqu'à -3 pour millièmes (le min)"], // Rangs autorisés
    ['l_espaces', 'Les erreurs d’écriture des nombres (espaces et zéros inutiles) sont signalées sans compter d’erreur.', 'liste', 'Permet de choisir si on tient compte de l’écriture des nombres', ['Aucune contrainte d’écriture.',
      'Les erreurs d’écriture des nombres (espaces et zéros inutiles) sont signalées sans compter d’erreur.',
      'Les nombres qui ne sont pas correctement écrits ne sont pas acceptés.']],
    ['b_aide', true, 'boolean', '<u>True</u> pour une explication des erreurs.']
  ],

  pe: [
    { pe_1: 'Notion totalement maitrisée.' },
    { pe_2: 'Notion comprise.' },
    { pe_3: 'Notion fragile.' },
    { pe_4: 'Notion non comprise.' },
    { pe_5: 'Notion comprise, mais la séparation des classes n’est pas acquise.' },
    { pe_6: 'Notion comprise, mais il reste des zéros inutiles.' },
    { pe_7: 'Notion de dixième non comprise.' },
    { pe_8: 'Notion de dixièmes consécutifs non comprise.' },
    { pe_9: 'Erreur dans l’ordre des nombres.' }
  ]
}

const textes = {
  nom_positions: ['millième', 'centième', 'dixième', 'unité', 'dizaine', 'centaine', 'millier'],
  phraseNbChances: 'Il te reste £e essai£p.', //  Ne pas supprimer
  phraseEnonce: 'Encadre £n £e£r près<BR>@1@ < £n < @2@',
  phraseSolution1: '£a < £n < £b',
  phraseSolution2: '£n n’est pas un£e £r.',
  phraseSolution3: '£n : Sépare correctement les classes par des espaces.',
  phraseSolution4: 'Les nombres £a ; £n et £b ne sont pas dans l’ordre croissant.',
  phraseSolution5: 'Les nombres £a et £b ne sont pas deux £rs consécuti£es.',
  phraseSolution6: '£a : Evite d’écrire des zéros inutiles.',
  Aide01: 'Tu dois toujours mettre un espace<br>entre les milliers et les centaines<br>et après les millièmes.',
  Aide02: 'Dans un£e £r, il n’y a aucun chiffre autre que 0 à droite des £rs.  ',
  Aide03: '£a n’est pas plus petit que £b.',
  Aide04: 'Voici un exemple d£l£rs consécuti£es:<br>£u1£v ; £u2£v ; £u3£v ; £u4£v ; £u5£v ; £u6£v ; £u7£v',
  Aide05: 'Supprime les zéros à gauche et à droite du nombre: ',
  Aide06: 'même si ici, tu peux mettre le zero des £rs pour préciser que le nombre est un£e £r.'
}

// nos constantes
const prefix = 'encadrement'

/**
 * section Encadrement
 * @this {Parcours}
 */
export default function main () {
  const me = this
  const stor = this.storage
  const ds = this.donneesSection

  //  Reformate un nombre
  function corrigeNombre (nombre) {
    //  Corrige un nombre saisi sous forme de texte
    //  Enlève les espaces, les zéros inutiles
    //  Met une virgule
    //  corrige la séparation des classes
    const motif = /^\d{0,3}(\s\d{3}){0,5}(,(\d{3}\s){0,5}\d{0,3})?$/
    if (nombre.length === 0) return [nombre, true, true, nombre]
    // Non vide
    nombre = nombre.trim() // Supprime les espaces de début et de fin
    nombre = nombre.replace('.', ',') // Remplace les points par des virgules
    if (nombre.indexOf(',') === -1) nombre += ',' //  Rajoute la virgule si c’est un entier
    while (nombre !== nombre.replace('  ', ' ')) nombre = nombre.replace('  ', ' ') //  Cas où il y a plus d’un espace entre les classes
    let repZeros = nombre // Nombre avant d’enlever les 0 inutiles
    const classes = nombre.match(motif) //  On teste si les espaces sont bien placés
    if (repZeros.endsWith(',')) repZeros = repZeros.substr(0, repZeros.length - 1) //  Enlève la virgule si c’est un entier
    if (repZeros[0] === ',') repZeros = '0' + repZeros // Si on a enlevé le zéro des unités
    while ((nombre[0] === ' ') || (nombre[0] === '0')) nombre = nombre.substr(1) //  Enlève les 0 et les espaces à gauche inutiles
    while (nombre.endsWith(' ') || nombre.endsWith('0')) nombre = nombre.substr(0, nombre.length - 1) //  Enlève les 0 et les espaces à droite inutiles
    if (nombre[0] === ',') nombre = '0' + nombre // Si on a enlevé le zéro des unités
    if (nombre.endsWith(',')) nombre = nombre.substr(0, nombre.length - 1) //  Enlève la virgule si c’est un entier
    const zeros = (nombre === repZeros) // Si le nombre a changé, c’est qu’il y avait des 0 inutiles
    nombre = j3pNombreBienEcrit(nombre.replace(/ /g, '').replace(',', '.'))
    return [nombre, classes, zeros, repZeros]
  } //  corrige_nombre

  //   Affiche la correction sous l’énoncé
  function afficheCorrection (fin) {
    // fonction appelée pour afficher la correction (que la réponse soit bonne ou fausse

    j3pEmpty(stor.divExplication1) //  Pour effacer  un éventuel précédent affichage
    if (fin && !stor.reponse.bonneReponse) { //  On affiche la bonne réponse s’il n’a pas trouvé au dernier essai
      j3pAffiche(stor.divExplication1, prefix + 'corr11', textes.phraseSolution1, {
        style: me.styles.moyen.correction,
        n: stor.s_nombre,
        a: stor.sReponse1,
        b: stor.sReponse2
      })
    } // fin

    if (stor.reponse.aRepondu) {
      stor.divExplication2.innerHTML = '' //  Pour effacer  un éventuel précédent affichage

      let sRepEleve1 //  Réponse élève au format corrigé
      let sRepEleve2 //  Réponse élève au format corrigé
      let tmp
      if (ds.n_espaces === 2) { // Si on tient compte des erreurs d’écriture, on corrige l’écriture
        tmp = corrigeNombre(j3pValeurde(prefix + 'consigne1input1')) // Remplace la saisie par un nombre plus propre
        sRepEleve1 = tmp[0]
        stor.espaces_1 = tmp[1]
        stor.zeros_1 = tmp[2]
        stor.repZeros_1 = tmp[3]
        tmp = corrigeNombre(j3pValeurde(prefix + 'consigne1input2'))
        sRepEleve2 = tmp[0]
        stor.espaces_2 = tmp[1]
        stor.zeros_2 = tmp[2]
        stor.repZeros_2 = tmp[3]
      } else {
        // Remplace la saisie par un nombre plus propre
        // Sinon la zone de saisie a déjà été corrigée
        sRepEleve1 = j3pValeurde(prefix + 'consigne1input1')
        sRepEleve2 = j3pValeurde(prefix + 'consigne1input2')
      }
      // Modifié en attendant es6
      //     let tRepEleve1 = (' '.repeat(7 - (sRepEleve1.indexOf(',') == -1 ? sRepEleve1.length : sRepEleve1.indexOf(','))) + sRepEleve1).padEnd(15, ' ').split('') //  Transforme la réponse en tableau
      //      let tRepEleve2 = (' '.repeat(7 - (sRepEleve2.indexOf(',') == -1 ? sRepEleve2.length : sRepEleve2.indexOf(','))) + sRepEleve2).padEnd(15, ' ').split('') //  Transforme la réponse en tableau
      let tRepEleve1 = ''
      let nbSpaces = 7 - (sRepEleve1.indexOf(',') === -1 ? sRepEleve1.length : sRepEleve1.indexOf(','))
      let i
      for (i = 0; i < nbSpaces; i++) tRepEleve1 += ' '
      tRepEleve1 += sRepEleve1
      while (tRepEleve1.length < 15) tRepEleve1 += ' '
      tRepEleve1 = tRepEleve1.split('')

      let tRepEleve2 = ''
      nbSpaces = 7 - (sRepEleve2.indexOf(',') === -1 ? sRepEleve2.length : sRepEleve2.indexOf(','))
      for (i = 0; i < nbSpaces; i++) tRepEleve2 += ' '
      tRepEleve2 += sRepEleve2
      while (tRepEleve2.length < 15) tRepEleve2 += ' '
      tRepEleve2 = tRepEleve2.split('')

      const repEleve1 = parseFloat(sRepEleve1.replace(/ /g, '').replace(',', '.')) //  Réponse utilisable dans les calculs
      const repEleve2 = parseFloat(sRepEleve2.replace(/ /g, '').replace(',', '.')) //  Réponse utilisable dans les calculs
      const repNombre = parseFloat(stor.s_nombre.replace(/ /g, '').replace(',', '.'))

      //  Rang de la question tenant compte des espaces et des virgules et dans l’ordre d’écriture
      const questionAdaptee = 15 - (stor.question > 3 ? ((stor.question > 6 ? ((stor.question > 9 ? stor.question + 3 : stor.question + 2)) : stor.question + 1)) : stor.question)

      //  Indique si il y avait des zeros inutiles
      if (!stor.zeros_1 && (ds.n_espaces >= 1)) {
        j3pDiv(stor.divExplication2, prefix + 'corr27', '')
        let indexDebut1 //  Index de début de la partie utile
        if (repEleve1 === 0) {
          if (!stor.reponseInitiale1.includes(',')) indexDebut1 = stor.reponseInitiale1.length - 1 // Si le nombre vaut 0 écrit sans virgule, tout ce qui est à gauche du 0 de droite est inutile
          else {
            indexDebut1 = stor.reponseInitiale1.indexOf(',') - 1 //  S’il y avait une virgule alors c’est le caractère d’avant
            while (stor.reponseInitiale1[indexDebut1] !== '0' && indexDebut1 >= 0) indexDebut1-- //  A moins que ce ne soit un espace
          }
        } else {
          indexDebut1 = 0
          while (['0', ' '].includes(stor.reponseInitiale1[indexDebut1])) indexDebut1++ // On cherche le premier chiffre qui ne soit pas un zéro
          if (stor.reponseInitiale1[indexDebut1] === ',') { while (stor.reponseInitiale1[indexDebut1] !== '0' && indexDebut1 >= 0) indexDebut1-- } //  Si c’est un nombre plus petit que 1 on recher le premier 0
        }
        stor.debutInutile1 = stor.reponseInitiale1.substring(0, indexDebut1) // Contient les zéros inutiles en début de chaine

        let indexFin1 //  Index de fin de la partie utile
        if (repEleve1 === 0) {
          if (stor.reponseInitiale1.includes(',')) indexFin1 = stor.reponseInitiale1.indexOf(',') - 1 // Si le nombre vaut 0, tout ce qui est à partir de la virguel est inutile
          else indexFin1 = stor.reponseInitiale1.length - 1
        } else {
          //  Sinon il n’y a pas de partie inutile
          indexFin1 = indexDebut1 - 1
          for (i = 0; i <= sRepEleve1.length - 1; i++) {
            if (sRepEleve1[i] !== ' ') { indexFin1 = stor.reponseInitiale1.indexOf(sRepEleve1[i], indexFin1 + 1) }
          }
        }
        stor.finInutile1 = stor.reponseInitiale1.substring(indexFin1 + 1)
        if ((stor.debutInutile1.length + stor.finInutile1.length !== 0) && ds.b_aide) {
          const { parent } = j3pAffiche(prefix + 'corr27', '', textes.phraseSolution6,
            {
              style: me.styles.moyen.correction,
              r: textes.nom_positions[stor.question - 1 - 3], //  Chaine correspondant au rang demandé
              e: ((stor.question >= 7) && (stor.question <= 9) ? 'e' : ''),
              a: stor.repZeros_1
            }) //  Affiche le nombre correctement écrit
          const spanBulle = j3pAddElt(parent, 'span')
          const bulle = new BulleAide(spanBulle, textes.Aide05)

          j3pAddElt(bulle.bulleDiv, 'br')
          j3pAddElt(bulle.bulleDiv, 'span', stor.debutInutile1, { style: { color: colorKo } })
          j3pAddElt(bulle.bulleDiv, 'span', stor.reponseInitiale1.substring(indexDebut1, indexFin1 + 1))
          j3pAddElt(bulle.bulleDiv, 'span', stor.finInutile1, { style: { color: colorKo } })
          if (tRepEleve1[questionAdaptee] === '0') {
            j3pAddElt(bulle.bulleDiv, 'br')
            j3pAddElt(bulle.bulleDiv, 'span', j3pChaine(textes.Aide06, {
              style: me.styles.moyen.correction,
              r: textes.nom_positions[stor.question - 1 - 3], //  Chaine correspondant au rang demandé
              e: ((stor.question >= 7) && (stor.question <= 9) ? 'e' : '')
            }))
          }
          stor.bulles_aide.push(bulle)
        } // Erreur à afficher
      } //  Erreur zéros 1

      if (!stor.zeros_2 && (ds.n_espaces >= 1)) {
        const divCorr = j3pAddElt(stor.divExplication2, 'div')
        let indexDebut2 //  Index de début de la partie utile
        if (repEleve2 === 0) {
          if (!stor.reponseInitiale2.includes(',')) indexDebut2 = stor.reponseInitiale2.length - 1 // Si le nombre vaut 0 écrit sans virgule, tout ce qui est à gauche du 0 de droite est inutile
          else {
            indexDebut2 = stor.reponseInitiale2.indexOf(',') - 1 //  S’il y avait une virgule alors c’est le caractère d’avant
            while (stor.reponseInitiale2[indexDebut2] !== '0' && indexDebut2 >= 0) indexDebut2-- //  A moins que ce ne soit un espace
          }
        } else {
          indexDebut2 = 0
          while (['0', ' '].includes(stor.reponseInitiale2[indexDebut2])) indexDebut2++ // On cherche le premier chiffre qui ne soit pas un zéro
          if (stor.reponseInitiale2[indexDebut2] === ',') { while (stor.reponseInitiale2[indexDebut2] !== '0' && indexDebut2 >= 0) indexDebut2-- } //  Si c’est un nombre plus petit que 1 on recher le premier 0
        }
        stor.debutInutile2 = stor.reponseInitiale2.substring(0, indexDebut2) // Contient les zéros inutiles en début de chaine

        let indexFin2 //  Index de fin de la partie utile
        if (repEleve2 === 0) {
          if (stor.reponseInitiale2.includes(',')) indexFin2 = stor.reponseInitiale2.indexOf(',') - 1 // Si le nombre vaut 0, tout ce qui est à partir de la virguel est inutile
          else indexFin2 = stor.reponseInitiale2.length - 1
        } else {
          //  Sinon il n’y a pas de partie inutile
          indexFin2 = indexDebut2 - 1
          for (i = 0; i <= sRepEleve2.length - 1; i++) {
            if (sRepEleve2[i] !== ' ') { indexFin2 = stor.reponseInitiale2.indexOf(sRepEleve2[i], indexFin2 + 1) }
          }
        }
        stor.finInutile2 = stor.reponseInitiale2.substring(indexFin2 + 1)
        if ((stor.debutInutile2.length + stor.finInutile2.length !== 0) && ds.b_aide) {
          const { parent } = j3pAffiche(divCorr, '', textes.phraseSolution6, {
            style: me.styles.moyen.correction,
            r: textes.nom_positions[stor.question - 1 - 3], //  Chaine correspondant au rang demandé
            e: ((stor.question >= 7) && (stor.question <= 9) ? 'e' : ''),
            a: stor.repZeros_2
          }) //  Affiche le nombre correctement écrit
          const spanBulle = j3pAddElt(parent, 'span')
          const bulleAide = new BulleAide(spanBulle, textes.Aide05)
          stor.bulles_aide.push(bulleAide)
          j3pAddElt(bulleAide.bulleDiv, 'br')
          j3pAddElt(bulleAide.bulleDiv, 'span', stor.debutInutile2, { style: { color: colorKo } })
          j3pAddElt(bulleAide.bulleDiv, 'span', stor.reponseInitiale2.substring(indexDebut2, indexFin2 + 1))
          j3pAddElt(bulleAide.bulleDiv, 'span', stor.finInutile2, { style: { color: colorKo } })
          if (tRepEleve2[questionAdaptee] === '0') {
            j3pAddElt(bulleAide.bulleDiv, 'br')
            j3pAddElt(bulleAide.bulleDiv, 'span', j3pChaine(textes.Aide06, {
              style: me.styles.moyen.correction,
              r: textes.nom_positions[stor.question - 1 - 3], //  Chaine correspondant au rang demandé
              e: ((stor.question >= 7) && (stor.question <= 9) ? 'e' : '')
            }))
          }
        } // Erreur à afficher
      } //  Erreur zéros 2

      //  Indique si les classes étaient correctement séparées
      if ((!stor.espaces_1) && (ds.n_espaces >= 1)) {
        j3pDiv(stor.divExplication2, prefix + 'corr21', '')
        j3pAffiche(prefix + 'corr21', prefix + 'corr210', textes.phraseSolution3,
          {
            style: me.styles.moyen.correction,
            n: sRepEleve1
          }) //  Affiche le nombre correctement écrit
        j3pSpan(prefix + 'corr210', { id: prefix + 'Aide21', contenu: '' })
        stor.bulles_aide.push(new BulleAide(prefix + 'Aide21', textes.Aide01))
      } //  Erreur espace 1

      if ((!stor.espaces_2) && (ds.n_espaces >= 1)) {
        j3pDiv(stor.divExplication2, prefix + 'corr22', '')
        j3pAffiche(prefix + 'corr22', prefix + 'corr220', textes.phraseSolution3,
          {
            style: me.styles.moyen.correction,
            n: sRepEleve2
          }) //  Affiche le nombre correctement écrit
        j3pSpan(prefix + 'corr220', { id: prefix + 'Aide22', contenu: '' })
        stor.bulles_aide.push(new BulleAide(prefix + 'Aide22', textes.Aide01))
      } //  Erreur espace 2

      //  Regarde si le nombre en bien un dixième, etc
      // for (i of tRepEleve1.keys()) correct1 = correct1 && ((i <= questionAdaptee) || (tRepEleve1[i] == '0') || (tRepEleve1[i] == ' ') || (tRepEleve1[i] == ','))
      const correct1 = tRepEleve1.every(function (rep, i) {
        return i <= questionAdaptee || rep === '0' || rep === ' ' || rep === ','
      })
      if (!correct1 && ds.b_aide) {
        j3pDiv(stor.divExplication2, prefix + 'corr23', '')
        j3pAffiche(prefix + 'corr23', prefix + 'corr230', textes.phraseSolution2,
          {
            style: me.styles.moyen.correction,
            n: sRepEleve1, //  Nombre formaté avec les espaces
            r: textes.nom_positions[stor.question - 1 - 3], //  Chaine correspondant au rang demandé
            e: ((stor.question >= 7) && (stor.question <= 9) ? 'e' : '')
          })
        const spanBulle = j3pAddElt(prefix + 'corr230', 'span')
        const bulle = new BulleAide(spanBulle, j3pChaine(textes.Aide02, {
          style: me.styles.moyen.correction,
          r: textes.nom_positions[stor.question - 1 - 3], //  Chaine correspondant au rang demandé
          e: ((stor.question >= 7) && (stor.question <= 9) ? 'e' : '')
        }))
        stor.bulles_aide.push(bulle)
        j3pSpan(bulle.bulleDiv, { contenu: tRepEleve1.slice(0, questionAdaptee + 1).join('').trimLeft() })
        j3pSpan(bulle.bulleDiv, { contenu: tRepEleve1.slice(questionAdaptee + 1, 14).join('').trimRight() }).style.color = me.styles.cfaux
      } //  Erreur dixième 1

      // let correct2 = true
      // for (i of tRepEleve2.keys()) correct2 = correct2 && ((i <= questionAdaptee) || (tRepEleve2[i] == '0') || (tRepEleve2[i] == ' ') || (tRepEleve2[i] == ','))
      const correct2 = tRepEleve2.every(function (rep, i) {
        return i <= questionAdaptee || rep === '0' || rep === ' ' || rep === ','
      })
      if (!correct2 && ds.b_aide) {
        const divExplic = j3pAddElt(stor.divExplication2, 'div')
        const { parent } = j3pAffiche(divExplic, '', textes.phraseSolution2, {
          style: me.styles.moyen.correction,
          n: sRepEleve2, //  Nombre formaté avec les espaces
          r: textes.nom_positions[stor.question - 1 - 3], //  Chaine correspondant au rang demandé
          e: ((stor.question >= 7) && (stor.question <= 9) ? 'e' : '') // Met le féminin
        })
        const spanBulle = j3pAddElt(parent, 'span')
        const bulle = new BulleAide(spanBulle, j3pChaine(textes.Aide02, {
          style: me.styles.moyen.correction,
          r: textes.nom_positions[stor.question - 1 - 3], //  Chaine correspondant au rang demandé
          e: ((stor.question >= 7) && (stor.question <= 9) ? 'e' : '')
        }))
        stor.bulles_aide.push(bulle)
        j3pAddElt(bulle.bulleDiv, 'span', tRepEleve2.slice(0, questionAdaptee + 1).join('').trimStart())
        j3pAddElt(bulle.bulleDiv, 'span', tRepEleve2.slice(questionAdaptee + 1, 14).join('').trimEnd(), { style: { color: colorKo } })
      } //  Erreur dixième 2

      //  Regarde si les nombres sont dans le bon ordre
      if (correct1 && correct2 && ds.b_aide) {
        if (!((repEleve1 < repNombre) && (repNombre < repEleve2))) {
          const divExplic = j3pAddElt(stor.divExplication2, 'div')
          const { parent } = j3pAffiche(divExplic, '', textes.phraseSolution4, {
            style: me.styles.moyen.correction,
            n: stor.s_nombre,
            a: sRepEleve1,
            b: sRepEleve2
          })
          const spanBulle = j3pAddElt(parent, 'span')
          let content
          if (!(repEleve1 < repNombre)) {
            content = j3pChaine(textes.Aide03,
              {
                style: me.styles.moyen.correction,
                b: stor.s_nombre,
                a: sRepEleve1
              })
            if ((repEleve1 > repNombre) && (repNombre > repEleve2)) content += '\n'
          }
          if (!(repNombre < repEleve2)) {
            content = j3pChaine(textes.Aide03, {
              style: me.styles.moyen.correction,
              a: stor.s_nombre,
              b: sRepEleve2
            })
          }
          // toujours vrai ?
          if (content) {
            const bulle = new BulleAide(spanBulle, content)
            stor.bulles_aide.push(bulle)
          }
        } else if (!j3pEnvironEgal((repEleve2 - repEleve1) / Math.pow(10, stor.question - 7), 1, 0.01)) {
          // Bon ordre
          // Regarde si les deux nombres sont consécutifs
          const divExplic = j3pAddElt(stor.divExplication2, 'div')
          const { parent } = j3pAffiche(divExplic, '', textes.phraseSolution5, {
            style: me.styles.moyen.correction,
            r: textes.nom_positions[stor.question - 1 - 3], //  Chaine correspondant au rang demandé
            a: sRepEleve1,
            b: sRepEleve2,
            e: ((stor.question >= 7) && (stor.question <= 9) ? 've' : 'f')
          })
          const spanBulle = j3pAddElt(parent, 'span')
          const bulle = new BulleAide(spanBulle, j3pChaine(textes.Aide04, {
            style: me.styles.moyen.correction,
            r: textes.nom_positions[stor.question - 1 - 3], //  Chaine correspondant au rang demandé
            l: (stor.question === 7 ? "'" : 'e '),
            e: ((stor.question >= 7) && (stor.question <= 9) ? 've' : 'f'),
            a: sRepEleve1,
            u: stor.tableau_reponse1.slice(0, questionAdaptee).join('').trimLeft() + ((stor.question === 6) && (stor.tableau_reponse1[7] !== ',') ? ',' : ''),
            // Modifié en attendant es6
            //                v: (stor.question === 10 ? ' ' : '') + (stor.question > 7 ? '0'.repeat(stor.question - 7) : '')
            v: (stor.question === 10 ? ' ' : '') + (stor.question > 7 ? '00000000000000000'.slice(0, stor.question - 7) : '')
          }))
          stor.bulles_aide.push(bulle)
        } //  Consécutifs
      } //  Deux nombres corrects

      if (!correct1 || !correct2) me.typederreurs[3]++ // Rang incorrect
      else if (!((repEleve1 < repNombre) && (repNombre < repEleve2))) me.typederreurs[5]++ // Ordre incorrect
      else if (!j3pEnvironEgal((repEleve2 - repEleve1) / Math.pow(10, stor.question - 7), 1, 0.01)) me.typederreurs[4]++ // Non consécutifs
      if (ds.n_espaces >= 2) {
        if (stor.reponse1.includes(sRepEleve1) && stor.reponse2.includes(sRepEleve2)) {
          if (!stor.espaces_1 || !stor.espaces_2) me.typederreurs[6]++ //  Les séparations de classes ne sont pas correctes
          else if ((!stor.zeros_1 && (stor.debutInutile1.length + stor.finInutile1.length !== 0)) && (!stor.zeros_2 && (stor.debutInutile2.length + stor.finInutile2.length !== 0))) me.typederreurs[7]++
        } // Zéros inutiles
      }
    } //  A répondu
  } //  affiche_correction

  // Affiche le nombre de tentatives qu’il reste s’il y a plusieurs chances
  function afficheChances () {
    j3pEmpty(stor.divChances)
    if (ds.nbchances > 1) {
      j3pAffiche(stor.divChances, prefix + 'chances', textes.phraseNbChances,
        {
          style: me.styles.moyen.enonce,
          e: ds.nbchances - me.essaiCourant,
          p: (ds.nbchances - me.essaiCourant > 1 ? 's' : '')
        })
    } // Pour le pluriel
  }

  // Ajoute les fonctions de contrôle à un input
  function ajouteControle (input) {
    // Remplace le point par la virgule
    j3pElement(input).addEventListener('input', j3pRemplacePoint)
    //  Empêche qu’il y ait deux virgules dans le nombre
    j3pElement(input).addEventListener('keydown', function pasDeuxVirgules (event) {
      if ((event.key === ',') || (event.key === '.')) if ((j3pValeurde(input).includes(',')) || (j3pValeurde(input).includes('.'))) event.preventDefault()
    }, false)
  } // ajouteControle

  //  Les variables des travail de la section
  let i, n

  /// ////////////////////////////////////////////////////////////////////////////////////////
  //
  //              Déroulement de la section Enonce, Correction, navigation
  //
  /// ////////////////////////////////////////////////////////////////////////////////////////

  switch (this.etat) {
    /// ////////////////////////////////////////////////////////////////////////////////////////
    //
    //            Phase énoncé
    //
    /// ////////////////////////////////////////////////////////////////////////////////////////

    case 'enonce': {
      //  Construit l’énoncé: exécuté au début de chaque question
      if (this.debutDeLaSection) {
        this.surcharge({ nbrepetitions: ds.nbrepetitionsmax })
        // Construction de la page
        this.construitStructurePage('presentation1')

        this.afficheTitre(ds.titre)
        // Affiche l’indication en bas à gauche
        if (ds.indication !== '') this.indication(this.zones.IG, ds.indication)

        this.typederreurs = [0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0]

        // Contient les liens vers les bulles d’aide
        stor.bulles_aide = []

        //  Paramètres et variables particuliers à la section
        ds.n_espaces = ['Aucune contrainte d’écriture.',
          'Les erreurs d’écriture des nombres (espaces et zéros inutiles) sont signalées sans compter d’erreur.',
          'Les nombres qui ne sont pas correctement écrits ne sont pas acceptés.'].indexOf(ds.l_espaces)

        // Transforme l’intervalle pour qu’il corresponde au codage du nombre
        ds.t_rangs_adapte = []
        ds.t_rangs_adapte[0] = ds.t_rangs[0] + 7
        ds.t_rangs_adapte[1] = ds.t_rangs[1] + 7

        //  t_rang_en_cours permet une difficulté croissante
        stor.t_rang_en_cours = []
        if (ds.t_rangs_adapte[0] >= 6) {
          if (ds.t_rangs_adapte[1] <= 6) {
            stor.t_rang_en_cours[0] = 6
          } else {
            // On commence par les dixièmes s’ils font partie de l’intervalle
            stor.t_rang_en_cours[0] = ds.t_rangs_adapte[1]
          }
        } else {
          // Sinon on prend le plus proche
          stor.t_rang_en_cours[0] = ds.t_rangs_adapte[1]
        }
        stor.t_rang_en_cours[1] = stor.t_rang_en_cours[0]
        stor.difficulte_en_cours = 0 // On commence sans cas particulier

        //  Teste la validité des paramètres
        if (ds.t_rangs[0] < -3 || ds.t_rangs[0] > 3) j3pShowError('Le paramètre rang n’est pas correct', { vanishAfter: 5 })
        if (ds.t_rangs[1] < -3 || ds.t_rangs[1] > 3) j3pShowError('Le paramètre rang n’est pas correct', { vanishAfter: 5 })

        //  Tableaux permettant de ne pas poser 2 fois la même question
        stor.questions = [] // Contient les question qui n’ont pas encore été posées
        for (i = stor.t_rang_en_cours[1]; i <= stor.t_rang_en_cours[0]; i++) { stor.questions.push([i, 0]); stor.questions.push([i, 0]) } //  2 essais au point de départ

        stor.erreurs = [] // Contient questions qui ont provoqué une erreur pour les représenter en priorité
        stor.repeter = false // Bascule permettant de prendre alternativement des questions nouvelles et des anciennes erreurs
      } else {
        // A chaque répétition, on nettoie la scène
        // Efface les bulles d’aide
        for (i = stor.bulles_aide.length - 1; i > -1; i--) {
          stor.bulles_aide[i].disable()
          stor.bulles_aide.pop()
        }
        this.videLesZones()
      } // Code exécuté à chaque nouvelle question: Partie à ne pas modifier

      //  Partie exécutée à chaque essai
      /// ////////////////////////////////////////////////////////////////////////////////////////
      //
      //               Création de l’énoncé
      //
      /// ////////////////////////////////////////////////////////////////////////////////////////

      //  Création de l’énoncé
      {
        //    Conventions de la manipulation des nombres affichés
        /**************************************************************************************
 *
 *                  999 999, 999 999
 *     rangs          3   0 -1 -3
 *     question    12              1
 *
 *
 *
 ***************************************************************************************/
        // codeQuestion contient les informations permettant de construire la question
        if (stor.repeter && stor.erreurs.length) {
          // On choisit la question une fois sur deux dans les erreurs passées
          n = j3pGetRandomInt(0, stor.erreurs.length - 1) // La question est choisie parmi celles qui n’ont pas encore été posées
          stor.codeQuestion = stor.erreurs[n]
          stor.erreurs.splice(n, 1) // On la retire des questions à poser
          stor.repeter = false // La prochaine fois on prendra une nouvelle question
        } else {
          n = j3pGetRandomInt(0, stor.questions.length - 1) // La question est choisie parmi celles qui n’ont pas encore été posées
          stor.codeQuestion = stor.questions[n]
          stor.questions.splice(n, 1) // On la retire des questions à poser
          stor.repeter = true
        }
        // question est le rang demandé;
        // difficulté vaut 2 si le nombre de gauche est 0, 2 si le rang vaut 9, 0 sinon
        stor.question = Number(stor.codeQuestion[0]) // Une question est un couple rang difficulté
        stor.difficulte = Number(stor.codeQuestion[1])

        //  reponse1 est le nombre encadrant à gauche
        //  Le nombre sous forme de tableau de chiffres avec les millionièmes à gauche et sans virgule
        // Modifié en attendant es6
        // this.tableau_reponse1 = new Array(12).fill(' ') // Crée un nombre vide
        stor.tableau_reponse1 = []
        for (i = 0; i < 12; i++) stor.tableau_reponse1.push(' ')

        //  Fixe le rang demandé: 0 pour difficulté 1, 9 pour difficulté 2 et 1 à 8 sinon
        if (stor.difficulte === 1) {
          if (stor.question < 7) {
            stor.tableau_reponse1[stor.question - 1] = 0
          } else {
            stor.tableau_reponse1[stor.question - 1] = ' '
          }
          // à partir des dizaines
        } else if (stor.difficulte === 2) {
          stor.tableau_reponse1[stor.question - 1] = 9
        } else {
          stor.tableau_reponse1[stor.question - 1] = j3pGetRandomInt(1, 8)
        }

        if (stor.tableau_reponse1[6] === ' ') stor.tableau_reponse1[6] = 0 // On s’assure qu’il y a un 0 aux unités
        if (stor.difficulte !== 1) { // Mais pas en difficulté 1
          for (i = stor.question - 1; i >= 8; i--) { // Et même jusqu’au rang demandé si besoin
            stor.tableau_reponse1[i - 1] = 0
          }
        }
        for (i = stor.question + 1; i <= 6; i++) { // Et jusqu’aux dixièmes dans le cas d’un nombre décimal
          stor.tableau_reponse1[i - 1] = 0
        }

        let chiffres
        //  On choisit le nombre de chiffres significatifs rajoutés avant
        if (stor.difficulte !== 1) { // Mais pas en difficulté 1
          chiffres = j3pGetRandomInt(0, 3)
          for (i = stor.question + 1; i <= Math.min(stor.question + chiffres, 12); i++) { // On rajoute les chiffres nécessaires un à un
            stor.tableau_reponse1[i - 1] = j3pGetRandomInt(1, 9)
          }
        }

        if ((stor.difficulte === 2) && (stor.tableau_reponse1[0] === 9)) {
          stor.tableau_reponse1[0] = 8 // Par sécurité pour ne pas atteindre le 1 000 000
        }

        //  reponse2 est le nombre encadrant à droite
        stor.tableau_reponse2 = stor.tableau_reponse1.slice()
        stor.tableau_reponse2[stor.question - 1] = stor.tableau_reponse1[stor.question - 1] + 1

        for (i = stor.question - 1; i <= 11; i++) {
          if (stor.tableau_reponse2[i] === 10) {
            stor.tableau_reponse2[i] = 0
            stor.tableau_reponse2[i + 1] = stor.tableau_reponse2[i + 1] === ' '
              ? stor.tableau_reponse2[i + 1] = 1 // FIXME on veut vraiment affecter 1 ici ?
              : stor.tableau_reponse2[i + 1] + 1
          }
        } // reporte la retenue

        if (stor.difficulte === 1) { // Rajoute les 0 en difficulté 1
          for (i = stor.question - 1; i >= 8; i--) stor.tableau_reponse2[i - 1] = 0
        }

        //  nombre est le nombre à encadrer
        stor.tableau_nombre = stor.tableau_reponse1.slice()

        chiffres = j3pGetRandomInt(1, 3) // Au moins un chiffre significatif juste après
        for (i = stor.question - 1; i >= stor.question - chiffres; i--) { // On rajoute les chiffres nécessaires un à un
          stor.tableau_nombre[i - 1] = j3pGetRandomInt(1, 9)
        }

        if (stor.difficulte === 1) { // Rajoute les 0 en difficulté 1
          for (i = stor.question - chiffres - 1; i >= 8; i--) { stor.tableau_nombre[i - 1] = 0 }
        }

        stor.tableau_reponse1.splice(6, 0, ',') // On rajoute la virgule
        stor.tableau_reponse2.splice(6, 0, ',') // On rajoute la virgule
        stor.tableau_nombre.splice(6, 0, ',') // On rajoute la virgule
        stor.tableau_reponse1 = stor.tableau_reponse1.reverse() // On remet le nombre à l’endroit
        stor.tableau_reponse2 = stor.tableau_reponse2.reverse() // On remet le nombre à l’endroit
        stor.tableau_nombre = stor.tableau_nombre.reverse() // On remet le nombre à l’endroit

        stor.tableau_reponse1.splice(3, 0, ' ') // On rajoute les espaces des milliers
        stor.tableau_reponse2.splice(3, 0, ' ')
        stor.tableau_nombre.splice(3, 0, ' ')
        stor.tableau_reponse1.splice(11, 0, ' ') // On rajoute les espaces des millièmes
        stor.tableau_reponse2.splice(11, 0, ' ')
        stor.tableau_nombre.splice(11, 0, ' ')

        stor.sReponse1 = stor.tableau_reponse1.join('').trim() // On transforme les nombres en chaines sans espaces inutiles
        stor.sReponse2 = stor.tableau_reponse2.join('').trim()
        stor.s_nombre = stor.tableau_nombre.join('').trim()
        if (stor.sReponse1.endsWith(',')) stor.sReponse1 = stor.sReponse1.substr(0, stor.sReponse1.length - 1) //  Enlève la virgule si c’est un entier
        if (stor.sReponse2.endsWith(',')) stor.sReponse2 = stor.sReponse2.substr(0, stor.sReponse2.length - 1)
        if (stor.s_nombre.endsWith(',')) stor.s_nombre = stor.s_nombre.substr(0, stor.s_nombre.length - 1)

        stor.reponse1 = [stor.sReponse1] // On met les réponses possibles
        let sReponse1 = stor.sReponse1
        while ((sReponse1.endsWith('0') && sReponse1.indexOf(',') !== -1) || sReponse1.endsWith(',')) {
          sReponse1 = sReponse1.substr(0, sReponse1.length - 1)
          stor.reponse1.push(sReponse1)
          //  Avec ou sans les zéros à droite jusqu’au rang demandé
        }

        stor.reponse2 = [stor.sReponse2]
        let sReponse2 = stor.sReponse2
        while ((sReponse2.endsWith('0') && sReponse2.indexOf(',') !== -1) || sReponse2.endsWith(',')) {
          sReponse2 = sReponse2.substr(0, sReponse2.length - 1)
          stor.reponse2.push(sReponse2)
        }
      } // Création de l’énoncé

      //  Création des conteneurs
      // Création du conteneur dans lequel se trouveront toutes les consignes
      const style = me.styles.etendre('moyen.enonce', { padding: '10px', marginTop: '25px' })
      const divConsignes = j3pAddElt(this.zonesElts.MG, 'div', '', { style })
      // Dans ce conteneur, on crée un div par phrase (cela forcera le retour à la ligne)
      const divConsigne1 = j3pAddElt(divConsignes, 'div')
      // Affiche le nombre de chances qu’il reste
      stor.divChances = j3pAddElt(divConsignes, 'div', '', { style: me.styles.etendre('moyen.enonce', { marginTop: '30px' }) })
      afficheChances()

      // Si on veut rajouter des explications, en cas de réponse fausse par exemple
      const divExplications = j3pAddElt(this.zonesElts.MG, 'div', '', { style: me.styles.etendre('moyen.explications', { padding: '10px', marginTop: '15px' }) })
      stor.divExplication1 = j3pAddElt(divExplications, 'div')
      stor.divExplication2 = j3pAddElt(divExplications, 'div')
      stor.divExplication2 = j3pAddElt(this.zonesElts.MD, 'div')
      stor.divCorrection = j3pAddElt(this.zonesElts.MD, 'div', '', { style: me.styles.etendre('petit.correction', { padding: '10px' }) })
      //  Création des conteneurs

      //  Initialisation des zones de saisie
      j3pAffiche(divConsigne1, prefix + 'consigne1', textes.phraseEnonce, {
        style: me.styles.moyen.enonce,
        n: stor.s_nombre, //  Nombre formaté avec les espaces
        r: textes.nom_positions[stor.question - 1 - 3], //  Chaine correspondant au rang demandé
        e: (stor.question === 7 ? "à l'" : ((stor.question >= 8) && (stor.question <= 9) ? 'à la ' : 'au ')),
        input1: { dynamique: true, width: '12px' },
        input2: { dynamique: true, width: '12px' }
      })

      // Le nom de chaque zone de saisie est construit de la même façon pour les zones input et inputmq :
      // le nom input est toujours précédé du nom du span qui contient la zone (ce span est le deuxième paramètre de la fonction j3pAffiche

      // On rajoute les contraintes de saisie
      j3pRestriction(prefix + 'consigne1input1', '0-9,. ') // on n’autorise que les chiffres
      j3pRestriction(prefix + 'consigne1input2', '0-9,. ') // on n’autorise que les chiffres

      //  Améliore la gestion de la virgule
      ajouteControle(prefix + 'consigne1input1')
      ajouteControle(prefix + 'consigne1input2')

      //  Limite la zone de saisie
      j3pElement(prefix + 'consigne1input1').maxLength = 10
      j3pElement(prefix + 'consigne1input2').maxLength = 10

      // On rajoute toutes les informations qui permettront de valider la saisie
      j3pElement(prefix + 'consigne1input1').typeReponse = ['texte']
      j3pElement(prefix + 'consigne1input1').reponse = stor.reponse1
      j3pElement(prefix + 'consigne1input2').typeReponse = ['texte']
      j3pElement(prefix + 'consigne1input2').reponse = stor.reponse2

      // Paramétrage de la validation
      // zonesSaisie est un tableau contenant le nom de chacune des zones (on pourrait aussi y mettre une liste déroulante)
      const zonesSaisie = [prefix + 'consigne1input1', prefix + 'consigne1input2']
      // validePerso est souvant un tableau vide
      // Mais parfois certaines zones ne peuvent pas être validée de manière automatique
      // Donc la validation de ces zones devra se faire "à la main".
      // Par contre on vérifiera si elles sont remplies ou non.
      const zonesValidePerso = []

      // fctsValid est un objet permettant la validation. Il contient le nom des zones (tableau précédent) et un deuxième paramètre validePerso
      stor.fctsValid = new ValidationZones({ parcours: me, zones: zonesSaisie, validePerso: zonesValidePerso })
      //  Initialisation des zones de saisie

      j3pFocus(prefix + 'consigne1input1')

      this.finEnonce() // Obligatoire
      //  Partie exécutée à chaque essai
      break
    } // case "enonce":

    /// ////////////////////////////////////////////////////////////////////////////////////////
    //
    //            Phase correction
    //
    /// ////////////////////////////////////////////////////////////////////////////////////////

    case 'correction': {
      const fctsValid = stor.fctsValid // ici je crée juste une variable pour raccourcir le nom de l’object fctsValid

      //  Efface le nombre de chances qui restent
      j3pEmpty(stor.divChances)

      // Efface les bulles d’aide
      for (i = stor.bulles_aide.length - 1; i > -1; i--) {
        stor.bulles_aide[i].disable()
        stor.bulles_aide.pop()
      }

      //  Efface les éléments de correction
      stor.divExplication1.innerHTML = ''
      stor.divExplication2.innerHTML = ''

      j3pElement(prefix + 'consigne1input1').value = j3pValeurde(prefix + 'consigne1input1').trim() // on enlève les espaces en début et en fin
      if (j3pElement(prefix + 'consigne1input1').value.length === 1 && j3pElement(prefix + 'consigne1input1').value === ',') j3pElement(prefix + 'consigne1input1').value = '' // On élimine ce cas particulier
      stor.reponseInitiale1 = j3pElement(prefix + 'consigne1input1').value // On mémorise la réponse initiale

      j3pElement(prefix + 'consigne1input2').value = j3pValeurde(prefix + 'consigne1input2').trim() // on enlève les espaces en début et en fin
      if (j3pElement(prefix + 'consigne1input2').value.length === 1 && j3pElement(prefix + 'consigne1input2').value === ',') j3pElement(prefix + 'consigne1input2').value = '' // On élimine ce cas particulier
      stor.reponseInitiale2 = j3pElement(prefix + 'consigne1input2').value// On mémorise la réponse initiale
      //
      if (ds.n_espaces < 2) { //  Si on ne tient pas compte des espaces et des zéros inutiles
        // Corrige l’écriture du nombre pour le comparer à la bonne réponse
        // Modifié en attendant es6
        //        [j3pElement(prefix + 'consigne1input1').value, stor.espaces_1, stor.zeros_1, stor.repZeros_1] = corrigeNombre(j3pValeurde(prefix + 'consigne1input1')); // Remplace la saisie par un nombre plus propre
        //        [j3pElement(prefix + 'consigne1input2').value, stor.espaces_2, stor.zeros_2, stor.repZeros_2] = corrigeNombre(j3pValeurde(prefix + 'consigne1input2'))
        const resultat1 = corrigeNombre(j3pValeurde(prefix + 'consigne1input1'))
        j3pElement(prefix + 'consigne1input1').value = resultat1[0]
        stor.espaces_1 = resultat1[1]
        stor.zeros_1 = resultat1[2]
        stor.repZeros_1 = resultat1[3]
        const resultat2 = corrigeNombre(j3pValeurde(prefix + 'consigne1input2'))
        j3pElement(prefix + 'consigne1input2').value = resultat2[0]
        stor.espaces_2 = resultat2[1]
        stor.zeros_2 = resultat2[2]
        stor.repZeros_2 = resultat2[3]
        //  stor.espaces: vrai si il y a une erreur d’espaces
        //  stor.zeros: vrai s’il y a des zéros inutiles
        //  stor.repZeros: écriture du nombre avec ses zéros inutiles mais sans les espaces
      }

      // ce qui suit sert pour la validation de toutes les zones
      // le tableau contenant toutes les zones de saisie
      stor.reponse = fctsValid.validationGlobale() // Ne pas modifier
      // A cet instant reponse contient deux éléments :
      // reponse.aRepondu qui me dit si toutes les zones ont bien été complétées ou non
      // reponse.bonneReponse qui me dit si les zones qui ne sont pas dans le tableau valide_pero ont des réponses correctes ou non.
      // Si reponse.aRepondu vaut true, la fonction met aussi en bleu ou rouge les réponses bonnes ou fausses.
      // Si c’est la dernière tentative de répondre, on barre également les zones fausses

      // Réponse vide ou incomplète et la limite de temps n’est pas atteinte
      if ((!stor.reponse.aRepondu) && (!this.isElapsed)) {
        this.reponseManquante(stor.divCorrection)
        afficheChances() //  affiche le nombre de chances qui restent
      } else {
        // Une réponse a été saisie
        // Bonne réponse
        if (stor.reponse.bonneReponse) {
          this.score++
          stor.divCorrection.style.color = this.styles.cbien
          stor.divCorrection.innerHTML = cBien

          // Même si la réponse est bonne on peut afficher des éléments de correction/explication :
          afficheCorrection(true)

          // Si toutes les erreurs ont été corrigées
          // Augmente le niveau de difficulté
          // On élargit les questions posés vers le haut et vers le bas si possible
          // Si on atteint la plage maximale, on rajoute la difficulté
          if (stor.erreurs.length === 0) {
            if ((stor.t_rang_en_cours[0] >= ds.t_rangs_adapte[0]) && (stor.t_rang_en_cours[1] <= ds.t_rangs_adapte[1]) && (stor.difficulte_en_cours < 2) && ds.b_niveau) {
              stor.difficulte_en_cours++
              for (i = stor.t_rang_en_cours[1]; i <= stor.t_rang_en_cours[0]; i++) stor.questions.push([i, stor.difficulte_en_cours])
            }
            if (stor.t_rang_en_cours[0] < ds.t_rangs_adapte[0]) {
              stor.t_rang_en_cours[0]++
              stor.questions.push([stor.t_rang_en_cours[0], 0])
            }
            if (stor.t_rang_en_cours[1] > ds.t_rangs_adapte[1]) {
              stor.t_rang_en_cours[1]--
              stor.questions.push([stor.t_rang_en_cours[1], 0])
            }
          } // Augmenter la difficulté

          this.typederreurs[0]++ //  Compte les bonnes réponses
          this.cacheBoutonValider() //  Plus de réponse à donner
          this.etat = 'navigation'
          this.sectionCourante() //  Passe à la question suivante
        } else {
          // Mauvaise réponse
          if (!stor.erreurs.includes(stor.codeQuestion)) {
            stor.questions.push(stor.codeQuestion) // On reposera la question
            stor.erreurs.push(stor.codeQuestion)
          } // Mémorise l’erreur
          stor.repeter = false // On ne repose pas la question au coup d’après

          stor.divCorrection.style.color = this.styles.cfaux // Commentaires à droite en rouge

          // Fini à cause de la limite de temps
          if (this.isElapsed) {
            this._stopTimer()

            stor.divCorrection.innerHTML = tempsDepasse // Commentaires à droite

            if (!fctsValid.zones.bonneReponse[0]) j3pBarre(prefix + 'consigne1input1')
            if (!fctsValid.zones.bonneReponse[1]) j3pBarre(prefix + 'consigne1input2')

            this.typederreurs[10]++ //  Compte les temps dépassés
            this.cacheBoutonValider() //  Plus de réponse à donner

            // L’élève avait un temps limite mais ne l’a pas respecté. On affiche alors la correction
            afficheCorrection(true)

            this.etat = 'navigation'
            this.sectionCourante() //  Passe à la question suivante
          } else {
            // Réponse fausse
            stor.divCorrection.innerHTML = cFaux // Commentaires à droite en rouge

            // Il reste des essais
            if (me.essaiCourant < ds.nbchances) {
              stor.divCorrection.innerHTML += '<br>' + essaieEncore // Commentaires à droite

              this.typederreurs[1]++ //  Compte les essais ratés

              afficheChances() //  affiche le nombre de chances qui restent

              // indication éventuelle ici
              afficheCorrection(false)
            } else {
              // Il ne reste plus d’essais
              stor.divCorrection.innerHTML += '<br>' + regardeCorrection // Commentaires à droite
              // Là il ne peut plus se corriger. On lui affiche alors la solution
              afficheCorrection(true)
              this.typederreurs[2]++ //  Compte les réponses fausses
              this.etat = 'navigation'
              this.sectionCourante() //  Passe à la question suivante
            } // Il ne reste plus d’essais
          } // Réponse fausse
        } // Mauvaise réponse
      } // Une réponse a été saisie

      // Obligatoire
      this.finCorrection()
      break // case "correction":
    }

    /// ////////////////////////////////////////////////////////////////////////////////////////
    //
    //            Phase correction
    //
    /// ////////////////////////////////////////////////////////////////////////////////////////

    case 'navigation': {
      //  Si on a posé toutes les questions, on rajoute les erreurs et on continue
      if (stor.questions.length === 0) stor.questions = stor.questions.concat(stor.erreurs)

      //    Arrête les questions si toutes les réponses sont justes
      if (stor.questions.length === 0) ds.nbitems = me.questionCourante

      if (this.sectionTerminee()) {
        // On détermine la phrase d’état renvoyée par la section
        if ((this.typederreurs[1] === 0) && (this.typederreurs[2] === 0) && (this.typederreurs[10] === 0)) {
          //  Aucune erreur
          if (ds.limite !== 0) this.parcours.pe = ds.pe_1 // Aucune erreur en temps limité: Notion totalement maitrisée
          else this.parcours.pe = ds.pe_2 //  Aucune erreur sans temps limité: Notion comprise
        } else if (
          ((this.typederreurs[2] === 0) && (this.typederreurs[10] === 0)) || //  A pu corriger ses erreurs
          ((ds.nbchances <= 1) && ((this.typederreurs[2] + this.typederreurs[10]) <= 1)) // ou une seule erreur s’il n’y a pas plusieurs chances
        ) {
          // ou une seule erreur s’il n’y a pas plusieurs chances
          this.parcours.pe = ds.pe_3 //  Notion fragile
        } else if (this.typederreurs[6] + this.typederreurs[7] === this.typederreurs[1] + this.typederreurs[2] + this.typederreurs[10]) {
          //  Les erreurs sont dues uniquement à l’écriture des nombres
          if (this.typederreurs[6] > this.typederreurs[7]) {
            this.parcours.pe = ds.pe_5 //  Les erreurs sont dues principalement à la séparation des classes.
          } else {
            this.parcours.pe = ds.pe_6 //  Les erreurs sont dues principalement aux zéros inutiles
          }
        } else if (this.typederreurs[3] > 1) {
          this.parcours.pe = ds.pe_7 //  Notion de dixième non comprise
        } else if (this.typederreurs[4] > 1) {
          this.parcours.pe = ds.pe_8 //  Notion de dixièmes consécutifs non comprise
        } else if (this.typederreurs[5] > 1) {
          this.parcours.pe = ds.pe_9 //  Erreur dans l’ordre des nombres
        } else {
          this.parcours.pe = ds.pe_4 //  Notion non comprise si on n’a pas trouve au moins une réponse lorsqu’il y a plusieurs essais
        }
        // ou si on n’a pas trouve au moins deux réponses lorsqu’il y a plusieurs essais
      } else {
        this.etat = 'enonce'
      }
      // Obligatoire
      this.finNavigation()
      break // case "navigation":
    }
  } //  switch etat
} //  SectionPosition
