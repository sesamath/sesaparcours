import { j3pAddElt, j3pBarre, j3pDiv, j3pElement, j3pFocus, j3pGetRandomInt, j3pValeurde } from 'src/legacy/core/functions'
import ValidationZones from 'src/legacy/outils/calculatrice/ValidationZones'
import { j3pAffiche, j3pDesactive } from 'src/lib/mathquill/functions'
import TableauConversion from 'src/legacy/outils/tableauconversion/TableauConversion'
import { j3pCreeBoutonFenetre } from 'src/legacy/core/functionsJqDialog'
import textesGeneriques from 'src/lib/core/textes'

const { cBien, cFaux, essaieEncore, regardeCorrection, tempsDepasse } = textesGeneriques

/*
      Auteur  Xavier JEROME
      Date    18/08/2018
      Lien entre les positions des chiffres dans un nombre
*/

export const params = {
  outils: ['mathquill'],
  /**
   * Ce qui est paramétrable dans l’éditeur de graphe
   * @type {LegacySectionParamsParametres}
   */
  parametres: [
    ['indication', '', 'string', 'Indication en bas à gauche'], // Texte que l’élève peut faire apparaître en bas de l’écran
    ['nbrepetitionsmax', 10, 'entier', 'Nombre de répétitions maximum de la section.<BR>Il peut être inférieur si on répond juste à toutes les questions'], // Obligatoire: nombre de questions posées pendant l’exercice
    ['nbchances', 2, 'entier', 'Nombre d’essais par répétition'], // Nombre d’essais autorisés avant de déclarer un échec et de passer à la question
    ['limite', 0, 'entier', 'Temps disponible par répétition, en secondes'], // S’il n’est pas vide, le temps de réponse sera limité
    ['n_niveau', 1, 'entier', '1 pour jusqu’aux unités de mille<BR>2 pour jusqu’aux dizaines de mille<BR>3 pour jusqu’aux centaines de mille (le max)'], // Niveau de difficulté
    ['n_decimaux', 0, 'entier', "0 pour pas de décimaux<BR>1 pour jusqu’aux dixièmes<BR>jusqu'à 3 pour jusqu’aux millièmes (le max)"], // Nombre de décimales
    ['n_ecartmax', 2, 'entier', 'Ecart maximum entre deux positions (de 1 à 4).'], // Niveau de difficulté
    ['s_question', '1;3', 'string', "Une suite de nombres indiquant les types de questions parmi<br>1 pour '10 dizaines représentent 1 ???'<br>2 pour 'Pour faire une centaine, il faut ??? dizaines'<br>3 pour 'Pour faire une centaine, il faut 10 ???'"],
    ['n_tableau', 2, 'entier', 'Permet d’afficher un tableau des positions:<br>0 pour jamais de tableau<br>1 pour proposer un tableau avec les deux nombres à la correction<br>2 pour proposer un tableau vide en cas d’erreur<br>3 pour proposer un tableau avec les deux nombres en cas d’erreur<br>4 pour le tableau vide est proposé en permanence.'], // Niveau de difficulté
    ['n_aide', 1, 'entier', '0 pour aucune aide<BR>1 pour une explication des erreurs'] // Niveau d’aide autorisé
  ],
  pe: [
    { pe_1: 'Notion totalement maitrisée.' },
    { pe_2: 'Notion comprise.' },
    { pe_3: 'Notion fragile.' },
    { pe_4: 'Notion non comprise.' },
    { pe_5: 'Notion non comprise malgré le tableau.' },
    { pe_6: 'Notion non comprise sans utilisation de l’aide.' }]
}

const textes = {
  nom_positions: ['millième', 'centième', 'dixième', 'unité', 'dizaine', 'centaine', 'millier', 'dizaine de milliers', 'centaine de milliers'],
  nom_positions_s: ['millièmes', 'centièmes', 'dixièmes', 'unités', 'dizaines', 'centaines', 'milliers', 'dizaines de milliers', 'centaines de milliers'],
  nom_quantite: ['', '1', '10', '100', '1 000', '10 000'],
  phraseTitre: 'Liens positionnels.', //  Titre de la section
  bouton_tableau: 'Tu peux t’aider d’un tableau',
  phraseNbChances: 'Il te reste £e essai£p.', //  Ne pas supprimer
  phraseEnonce1: '£a £n représentent 1 #1#',
  phraseEnonce2: 'Pour faire un£e £m, il faut #1# £n',
  phraseEnonce3: 'Pour faire un£e £m, il faut £a #1#',
  phraseSolution1: '£a £n représentent 1 £m',
  phraseSolution2: 'Pour faire un£e £m, il faut £a £n',
  Aide01: 'Attention: ce sont £a £p ou £b £n qui représentent 1 £m ',
  Aide02: 'Attention: ce sont £a £p qui représentent 1 £m ',
  Aide03: 'Attention: £a £n font un£e £m'
}

const idPrefix = 'LienPositionnel'

/**
 * section LienPositionnel
 * @this {Parcours}
 */
export default function main () {
  const me = this
  const stor = this.storage
  /// ////////////////////////////////////////////////////////////////////////////////////////
  //
  //                  Données et paramètres de la section
  //
  /// ////////////////////////////////////////////////////////////////////////////////////////
  let n, i, j
  function _Donnees () {
    /*      "presentation1"   6 cadres complets
        "presentation1bis"   manque le cadre indication en bas à droite
        "presentation2"   manque le cadre réponse à droite
        "presentation3"   un seul grand cadre en plus des deux entêtes
*/
    this.structure = 'presentation1'

    // Si indication != "" alors un lien "Indication" s’affiche dans la zone Inférieure Gauche (this.zones.IG
    // une indication<br>de deux lignes...";
    // Cette indication peut être changée par l’utilisateur dans la page paramètres
    this.indication = "Et si c'étaient des pièces et des billets, tu ferais comment?" //  N’est utilisé que si le paramètre passé en entrée est vide
    // pour le cas de presentation1bis
    // En effet pas d’indication possible pour cette présentation
    // this.surchargeindication = false;

    this.video = [] // ["tableur1,Titre Video 1,Video 1","lebouquet,Titre Video2,Video 2"]

    // Nombre de répétitions de l’exercice
    this.nbrepetitionsmax = 10 // N’est utilisé que si le paramètre passé en entrée est vide
    this.nbetapes = 1 // Si chaque question est divisée en plusieurs étapes
    // Nombre de chances dont dispose l’élève pour répondre à la question
    this.nbchances = 2
    // Par défaut, pas de temps limite.
    this.limite = 0 //  N’est utilisé que si le paramètre passé en entrée est vide

    this.n_niveau = 1
    this.n_decimaux = 0
    this.s_question = '1;3'
    this.n_ecartmax = 2
    this.n_tableau = 2

    this.n_aide = 1
    /*
        Phrase d’état renvoyée par la section
        */
    this.pe_1 = 'Notion totalement maitrisée'
    this.pe_2 = 'Notion comprise'
    this.pe_3 = 'Notion fragile.'
    this.pe_4 = 'Notion non comprise.'
    this.pe_5 = 'Notion non comprise malgré le tableau.'
    this.pe_6 = 'Notion non comprise sans utilisation de l’aide.'

    /// ////////////////////////////////////////////////////////////////////////////////////////
    //
    //               Ajouter ici toutes les constantes nécessaires à la section
    //
    /// ////////////////////////////////////////////////////////////////////////////////////////
  } // _Donnees

  /// ////////////////////////////////////////////////////////////////////////////////////////
  //
  //              Ajouter ici toutes les fonctions nécessaires à la section
  //
  /// ////////////////////////////////////////////////////////////////////////////////////////

  //  Transforme une chaine d’intervalles en un tableau de valeurs
  function liste_to_tab (intervalles) {
    const valeurs = []
    const tableau = intervalles.match(/\d*/g)
    for (i = 0; i < tableau.length; i++) {
      if (tableau[i].length != 0) { valeurs.push(parseFloat(tableau[i])) }
    }
    return valeurs
  } // liste_to_tab

  //   Affiche la correction sous l’énoncé
  function affiche_correction (fin) {
    // fonction appelée pour afficher la correction (que la réponse soit bonne ou fausse

    j3pElement(idPrefix + 'explication1').innerHTML = '' //  Pour effacer  un éventuel précédent affichage
    if (fin && !reponse.bonneReponse) { //  On affiche la bonne réponse s’il n’a pas trouvé au dernier essai
      j3pAffiche(idPrefix + 'explication1', idPrefix + 'corr11', (stor.t_question[stor.type] == 1 ? textes.phraseSolution1 : textes.phraseSolution2),
        {
          styletexte: '',
          m: textes.nom_positions[stor.rang],
          n: textes.nom_positions_s[stor.rang - stor.ecart],
          a: textes.nom_quantite[stor.ecart + 1],
          e: (((stor.rang >= 3) && (stor.rang <= 5)) || stor.rang >= 7 ? 'e' : '')
        })
      if (me.donneesSection.n_tableau >= 1) affiche_tableau(2)
    } // fin

    if (reponse.aRepondu && me.donneesSection.n_aide == 1) {
      j3pElement(idPrefix + 'explication2').innerHTML = '' //  Pour effacer  un éventuel précédent affichage

      const s_rep_eleve1 = j3pValeurde(idPrefix + 'consigne1liste1')
      let rep_eleve1
      let correction
      switch (stor.t_question[stor.type]) {
        case 1: {
          rep_eleve1 = stor.liste.indexOf(s_rep_eleve1) //  Récupère la réponse
          correction = rep_eleve1 - (stor.rang - (3 - me.donneesSection.n_decimaux) + 1)
          if (stor.rang - stor.ecart + correction >= 0 && stor.rang - stor.ecart + correction <= 8 && stor.ecart + 1 + correction >= 2 && stor.ecart + 1 + correction <= 5) {
            j3pAffiche(idPrefix + 'explication2', idPrefix + 'corr11', textes.Aide01,
              {
                styletexte: '',
                m: s_rep_eleve1,
                a: textes.nom_quantite[stor.ecart + 1],
                p: textes.nom_positions_s[stor.rang - stor.ecart + correction],
                n: textes.nom_positions_s[stor.rang - stor.ecart],
                b: textes.nom_quantite[stor.ecart + 1 + correction]
              })
          } else
            if (stor.rang - stor.ecart + correction >= 0 && stor.rang - stor.ecart + correction <= 8) {
              j3pAffiche(idPrefix + 'explication2', idPrefix + 'corr11', textes.Aide02,
                {
                  styletexte: '',
                  m: s_rep_eleve1,
                  a: textes.nom_quantite[stor.ecart + 1],
                  p: textes.nom_positions_s[stor.rang - stor.ecart + correction]
                })
            } else
              if (stor.ecart + 1 + correction >= 2 && stor.ecart + 1 + correction <= 5) {
                j3pAffiche(idPrefix + 'explication2', idPrefix + 'corr11', textes.Aide02,
                  {
                    styletexte: '',
                    m: s_rep_eleve1,
                    p: textes.nom_positions_s[stor.rang - stor.ecart],
                    a: textes.nom_quantite[stor.ecart + 1 + correction]
                  })
              }
          break } // Type 1
        case 2: {
          rep_eleve1 = textes.nom_quantite.indexOf(s_rep_eleve1) //  Récupère la réponse
          correction = rep_eleve1 - (stor.ecart + 1)
          if (rep_eleve1 != '1' && stor.rang + correction >= 0 && stor.rang + correction <= 8) {
            j3pAffiche(idPrefix + 'explication2', idPrefix + 'corr11', textes.Aide03,
              {
                styletexte: '',
                m: textes.nom_positions[stor.rang + correction],
                e: (((stor.rang + correction >= 3) && (stor.rang + correction <= 5)) || stor.rang + correction >= 7 ? 'e' : ''),
                n: textes.nom_positions_s[stor.rang - stor.ecart],
                a: s_rep_eleve1
              })
          }
          break } // Type 2
        case 3: {
          rep_eleve1 = stor.liste.indexOf(s_rep_eleve1) //  Récupère la réponse
          correction = rep_eleve1 - (stor.rang - stor.ecart - (3 - me.donneesSection.n_decimaux) + 1)
          if (stor.rang + correction >= 0 && stor.rang + correction <= 8) {
            j3pAffiche(idPrefix + 'explication2', idPrefix + 'corr11', textes.Aide03,
              {
                styletexte: '',
                m: textes.nom_positions[stor.rang + correction],
                e: (((stor.rang + correction >= 3) && (stor.rang + correction <= 5)) || stor.rang + correction >= 7 ? 'e' : ''),
                n: s_rep_eleve1,
                a: textes.nom_quantite[stor.ecart + 1]
              })
          }
          break } // Type 2
      } // type

      if (!fin && !reponse.bonneReponse && (me.donneesSection.n_tableau == 2 || me.donneesSection.n_tableau == 3)) {
        j3pCreeBoutonFenetre('Bouton_tab', idPrefix + 'explication3', {}, textes.bouton_tableau)
        j3pElement('BTN_fenetreBouton_tab').onclick = function onclick (event) { me.typederreurs[4]++; affiche_tableau((me.donneesSection.n_tableau == 2 ? 1 : 2)) }
      } // Affiche bouton
    } //  A répondu
  } //  affiche_correction

  //   Affiche un tableau des positions des chiffres
  function affiche_tableau (type) {
    switch (type) {
      case 0 : j3pElement(idPrefix + 'explication3').innerHTML = ''; break

      case 1 : { // Un tableau vide non modifiable
        // On crée un div dans explications
        j3pElement(idPrefix + 'explication3').innerHTML = ''
        me.tableau = new TableauConversion(idPrefix + 'explication3',
          {
            idDIV: 'tableau',
            format: {
              virgule: [stor.rang - stor.ecart < 3], // On ne met la virgule que si nécessaire
              epaisseurs: {},
              couleurs: {}
            },
            lignes: [[[], // milliards si besoin
              [],
              (stor.rang >= 6 ? ['.', '.', '.'] : []),
              ['.', '.', '.'], // Unités
              (stor.rang - stor.ecart < 3 ? ['.', '.', '.'] : []), // Décimaux si besoin
              []]] // Jamais de millionièmes
          })
        break }

      case 2 : { // Un tableau vide modifiable
        j3pElement(idPrefix + 'explication3').innerHTML = ''
        me.tableau = new TableauConversion(idPrefix + 'explication3',
          {

            idDIV: 'tableau',
            format: {
              virgule: [stor.rang - stor.ecart < 3, stor.rang < 3], // On ne met la virgule que si nécessaire
              epaisseurs: {},
              couleurs: {}
            },
            lignes: [TableauConversion.creeLigne(stor.nombre2, (stor.rang - stor.ecart >= 3 ? 11 : 11 + 3 - (stor.rang - stor.ecart)), true, [(stor.rang >= 3 ? 2 : 3), (stor.rang - stor.ecart < 3 ? 4 : 3)]),
              TableauConversion.creeLigne(stor.nombre1, (stor.rang >= 3 ? 11 : 11 + 3 - stor.rang), true, [(stor.rang >= 3 ? 2 : 3), (stor.rang - stor.ecart < 3 ? 4 : 3)])]
          })
        break }
    } // type
  }// affiche tableau

  // Affiche le nombre de tentatives qu’il reste s’il y a plusieurs chances
  function affiche_chances () {
    j3pElement(idPrefix + 'zone_chances').innerHTML = ''
    if (me.donneesSection.nbchances > 1) {
      j3pAffiche(idPrefix + 'zone_chances', idPrefix + 'chances', textes.phraseNbChances,
        {
          e: me.donneesSection.nbchances - me.essaiCourant,
          p: (me.donneesSection.nbchances - me.essaiCourant > 1 ? 's' : '')
        })
    } // Pour le pluriel
  }

  /// ////////////////////////////////////////////////////////////////////////////////////////
  //
  //              Déroulement de la section Enonce, Correction, navigation
  //
  /// ////////////////////////////////////////////////////////////////////////////////////////

  //  Permet de désigner la section
  const la_section = me.SectionLienPositionnel // Modification du nom n°4

  switch (this.etat) {
    /// ////////////////////////////////////////////////////////////////////////////////////////
    //
    //            Phase énoncé
    //
    /// ////////////////////////////////////////////////////////////////////////////////////////

    case 'enonce': {
      //  Construit l’énoncé: exécuté au début de chaque question
      if (this.debutDeLaSection) {
        //  Code exécuté uniquement au lancement de la section
        //  Partie à ne pas changer
        // Construit les données de la section
        this.donneesSection = new _Donnees()

        this.surcharge()

        // Construction de la page
        this.construitStructurePage(this.donneesSection.structure)

        this.donneesSection.nbrepetitions = this.donneesSection.nbrepetitionsmax // C’est cette valeur qui est affichée dans l’entête
        this.donneesSection.nbitems = this.donneesSection.nbetapes * this.donneesSection.nbrepetitionsmax // Initialisation à ne pas modifier

        this.score = 0

        this.afficheTitre(textes.phraseTitre)

        // Affiche l’indication en bas à gauche
        if (this.donneesSection.indication) this.indication(this.zones.IG, this.donneesSection.indication)

        //  Paramètres à renseigner
        /*
                 Par convention,`
                `   this.typederreurs[0] = nombre de bonnes réponses
                    this.typederreurs[1] = nombre de fois où l’élève a utilisé une chance supplémentaire
                    this.typederreurs[2] = nombre de mauvaises réponses
                    this.typederreurs[4] = nombre de fois où il a utilisé le tableau
                    this.typederreurs[10] = nombre de fois où l’élève a eu faux à cause d’un temps limite
                    LES AUTRES peuvent être utilisées par la détermination de la pe dans le cas d’une section qualitative
                 */
        this.typederreurs = [0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0]

        // Permet l’utilisation du compte à rebours si un temps est défini dans les paramètre

        //  Paramètres et variables particuliers à la section
        //  permet une difficulté croissante
        stor.rang_en_cours = 2 //  On commence par les centaines
        stor.decimaux_en_cours = 0 //  On commence par pas de décimaux
        stor.ecart_en_cours = 1
        stor.type_en_cours = 0 //  On commence par le premier type

        stor.t_question = liste_to_tab(me.donneesSection.s_question) //  Les types de questions autorisés
        //  Tableaux permettant de ne pas poser 2 fois la même question
        stor.questions = [] // Contient les question qui n’ont pas encore été posées [rang, ecart, type]
        stor.questions.push([4, 1, 0])
        stor.questions.push([5, 1, 0])

        stor.erreurs = [] // Contient questions qui ont provoqué une erreur pour les représenter en priorité
      } else {
        // A chaque répétition, on nettoie la scène
        this.videLesZones()
      } // Code exécuté à chaque nouvelle question: Partie à ne pas modifier

      //  Partie exécutée à chaque essai
      //  Partie à ne pas modifier

      /// ////////////////////////////////////////////////////////////////////////////////////////
      //
      //               Création de l’énoncé
      //
      /// ////////////////////////////////////////////////////////////////////////////////////////

      //    Conventions de la manipulation des nombres affichés
      /**************************************************************************************
 *
 *                       999 999, 9 9 9
 *     rang              8     3      0
 *     question          6     0 -1  -3
 *     n_decimaux                 1 2 3
 *     n_niveau          321
 *
 ***************************************************************************************/

      //  Création de l’énoncé
      // question est le rang demandé;
      n = j3pGetRandomInt(0, stor.questions.length - 1) // La question est choisie parmi celles qui n’ont pas encore été posées
      stor.rang = stor.questions[n][0] // Rang le plus élevé
      stor.ecart = stor.questions[n][1] // Ecart entre les deux rangs comparés
      stor.type = stor.questions[n][2] // type de question soit 1 ou 2 soit 3 ou 4 selon multiple ou sous-multiple
      stor.questions.splice(n, 1) // On la retire des questions à poser

      // Modifié en attendant es6
      //          if (stor.rang >= 3) stor.nombre1 = parseFloat('1' + '0'.repeat(stor.rang - 3))
      //          else stor.nombre1 = 1
      //          if (stor.rang - stor.ecart >= 3) stor.nombre2 = parseFloat('1' + '0'.repeat(stor.rang - stor.ecart - 3))
      //          else stor.nombre2 = 1
      stor.nombre1 = 1
      for (i = 3; i < stor.rang; i++) stor.nombre1 *= 10
      stor.nombre2 = 1
      for (i = 3 + stor.ecart; i < stor.rang; i++) stor.nombre2 *= 10

      //  Création des conteneurs
      { // Création du conteneur dans lequel se trouveront toutes les consignes
        j3pDiv(this.zones.MG, { id: idPrefix + 'consignes', contenu: '', coord: [0, 0], style: me.styles.etendre('petit.enonce', { padding: '10px', marginTop: '25px' }) })

        // Dans ce conteneur, on crée un div par phrase (cela forcera le retour à la ligne)
        j3pDiv(idPrefix + 'consignes', idPrefix + 'zone_consigne1', '')

        // Affiche le nombre de chances qu’il reste
        j3pDiv(idPrefix + 'consignes', { id: idPrefix + 'zone_chances', contenu: '', style: me.styles.etendre('moyen.enonce', { marginTop: '30px' }) })
        affiche_chances()

        // Si on veut rajouter des explications, en cas de réponse fausse par exemple
        const divExplications = j3pDiv(this.zones.MG, { id: idPrefix + 'explications', contenu: '', coord: [10, 200], style: me.styles.petit.explications })
        j3pAddElt(divExplications, 'div', { id: idPrefix + 'explication1' })
        j3pAddElt(divExplications, 'div', { id: idPrefix + 'explication2' })
        j3pDiv(divExplications, { id: idPrefix + 'explication3', coord: [10, 10] })
        j3pElement(idPrefix + 'explication2').style.marginTop = '20px'
        j3pElement(idPrefix + 'explication3').style.marginTop = '20px'

        // On crée ici le div qui contiendra toutes les remarques sur ce qu’on pense de la réponse de l’élève (le plus souvant dans le cadre MD)
        j3pDiv(this.zones.MD, { id: idPrefix + 'correction', contenu: '', coord: [0, 0], style: me.styles.etendre('petit.correction', { padding: '10px' }) })
      } //  Création des conteneurs

      //  Initialisation des zones de saisie
      { switch (stor.t_question[stor.type]) {
        case 1: {
          stor.liste = textes.nom_positions.slice(3 - me.donneesSection.n_decimaux, me.donneesSection.n_niveau + 3 + 3)
          stor.liste.unshift('')
          j3pAffiche(idPrefix + 'zone_consigne1', idPrefix + 'consigne1', textes.phraseEnonce1,
            {
              styletexte: this.styles.petit.enonce,
              n: textes.nom_positions_s[stor.rang - stor.ecart],
              a: textes.nom_quantite[stor.ecart + 1],
              liste1: {
                texte: stor.liste,
                taillepolice: this.styles.petit.enonce.fontSize
              }
            })

          // On rajoute toutes les informRations qui permettront de valider la saisie
          j3pElement(idPrefix + 'consigne1liste1').reponse = stor.rang - (3 - me.donneesSection.n_decimaux) + 1
          break } // Type 1
        case 2: {
          j3pAffiche(idPrefix + 'zone_consigne1', idPrefix + 'consigne1', textes.phraseEnonce2,
            {
              styletexte: this.styles.petit.enonce,
              m: textes.nom_positions[stor.rang],
              n: textes.nom_positions_s[stor.rang - stor.ecart],
              e: (((stor.rang >= 3) && (stor.rang <= 5)) || stor.rang >= 7 ? 'e' : ''),
              liste1: {
                texte: textes.nom_quantite,
                taillepolice: this.styles.petit.enonce.fontSize
              }
            })

          // On rajoute toutes les informations qui permettront de valider la saisie
          j3pElement(idPrefix + 'consigne1liste1').reponse = stor.ecart + 1
          break } // Type 2
        case 3: {
          stor.liste = textes.nom_positions_s.slice(3 - me.donneesSection.n_decimaux, me.donneesSection.n_niveau + 3 + 3)
          stor.liste.unshift('')
          j3pAffiche(idPrefix + 'zone_consigne1', idPrefix + 'consigne1', textes.phraseEnonce3,
            {
              styletexte: this.styles.petit.enonce,
              m: textes.nom_positions[stor.rang],
              a: textes.nom_quantite[stor.ecart + 1],
              e: (((stor.rang >= 3) && (stor.rang <= 5)) || stor.rang >= 7 ? 'e' : ''),
              liste1: {
                texte: stor.liste,
                taillepolice: this.styles.petit.enonce.fontSize
              }
            })

          // On rajoute toutes les informations qui permettront de valider la saisie
          j3pElement(idPrefix + 'consigne1liste1').reponse = stor.rang - stor.ecart - (3 - me.donneesSection.n_decimaux) + 1
          break } // Type 3
      } // type

      //  Permet de refaire passer la zone de saisie en noir lors du deuxième essai
      j3pElement(idPrefix + 'consigne1liste1').addEventListener('click', function () {
        j3pElement(idPrefix + 'consigne1liste1').style.color = ''
      }, false)

      // Paramétrage de la validation
      // mes_zones_saisie est un tableau contenant le nom de chacune des zones (on pourrait aussi y mettre une liste déroulante)
      const mes_zones_saisie = [idPrefix + 'consigne1liste1']
      // validePerso est souvant un tableau vide
      // Mais parfois certaines zones ne peuvent pas être validée de manière automatique
      // Donc la validation de ces zones devra se faire "à la main".
      // Par contre on vérifiera si elles sont remplies ou non.
      const mes_zones_valide_perso = []

      // fcts_valid est un objet permettant la validation. Il contient le nom des zones (tableau précédent) et un deuxième paramètre validePerso
      la_section.fcts_valid = new ValidationZones({ parcours: me, zones: mes_zones_saisie, validePerso: mes_zones_valide_perso })
      } //  Initialisation des zones de saisie

      if (me.donneesSection.n_tableau == 4) {
        j3pCreeBoutonFenetre('Bouton_tab', idPrefix + 'explication3', {}, textes.bouton_tableau)
        j3pElement('BTN_fenetreBouton_tab').onclick = function onclick (event) {
          me.typederreurs[4]++; affiche_tableau(1)
        }
      } // Tableau permanent

      j3pFocus(idPrefix + 'consigne1liste1')

      this.finEnonce() // Obligatoire
      break
    } // case "enonce"

    /// ////////////////////////////////////////////////////////////////////////////////////////
    //
    //            Phase correction
    //
    /// ////////////////////////////////////////////////////////////////////////////////////////

    case 'correction': {
      const fcts_valid = la_section.fcts_valid // ici je crée juste une variable pour raccourcir le nom de l’object fcts_valid

      //  Efface le nombre de chances qui restent
      j3pElement(idPrefix + 'zone_chances').innerHTML = ''

      //  Efface les éléments de correction
      j3pElement(idPrefix + 'explication1').innerHTML = ''
      j3pElement(idPrefix + 'explication2').innerHTML = ''

      // ce qui suit sert pour la validation de toutes les zones
      // le tableau contenant toutes les zones de saisie
      var reponse = fcts_valid.validationGlobale() // Ne pas modifier
      // A cet instant reponse contient deux éléments :
      // reponse.aRepondu qui me dit si toutes les zones ont bien été complétées ou non
      // reponse.bonneReponse qui me dit si les zones qui ne sont pas dans le tableau valide_pero ont des réponses correctes ou non.
      // Si reponse.aRepondu vaut true, la fonction met aussi en bleu ou rouge les réponses bonnes ou fausses.
      // Si c’est la dernière tentative de répondre, on barre également les zones fausses

      // Réponse vide ou incomplète et la limite de temps n’est pas atteinte
      if ((!reponse.aRepondu) && (!this.isElapsed)) {
        this.reponseManquante(idPrefix + 'correction')

        affiche_chances() //  affiche le nombre de chances qui restent
      } else {
        // Bonne réponse
        if (reponse.bonneReponse) {
          this._stopTimer()
          this.score++
          j3pElement(idPrefix + 'correction').style.color = this.styles.cbien
          j3pElement(idPrefix + 'correction').innerHTML = cBien

          j3pElement(idPrefix + 'explication3').innerHTML = '' // Plus besoin du tableau

          // Même si la réponse est bonne on peut afficher des éléments de correction/explication :

          // Augmente le niveau de difficulté et on rajoute à la liste des questions
          if (stor.rang_en_cours < me.donneesSection.n_niveau + 2) {
            stor.rang_en_cours++
            for (i = 0; i <= stor.type_en_cours; i++) {
              for (j = 1; j <= stor.ecart_en_cours; j++) {
                if (stor.rang_en_cours + 3 - j >= 3 - stor.decimaux_en_cours) { stor.questions.push([stor.rang_en_cours + 3, j, i]) }
              }
            }
          } // On augmente le rang

          if (stor.decimaux_en_cours < me.donneesSection.n_decimaux) {
            stor.decimaux_en_cours++
            for (i = 0; i <= stor.type_en_cours; i++) {
              for (j = 1; j <= stor.ecart_en_cours; j++) {
                if (stor.rang_en_cours + 3 - j >= 3 - stor.decimaux_en_cours) { stor.questions.push([3 - stor.decimaux_en_cours + j, j, i]) }
              }
            }
          } // On augmente le rang du côté des décimaux

          if (stor.type_en_cours < stor.t_question.length - 1) {
            stor.type_en_cours++
            for (i = 3 - stor.decimaux_en_cours + 1; i <= stor.rang_en_cours + 3; i++) {
              for (j = 1; j <= stor.ecart_en_cours; j++) {
                if (i - j >= 3 - stor.decimaux_en_cours) { stor.questions.push([i, j, stor.type_en_cours]) }
              }
            }
          } // On augmente le type de questions

          if (stor.ecart_en_cours < me.donneesSection.n_ecartmax) {
            stor.ecart_en_cours++
            for (i = 3 - stor.decimaux_en_cours + 1; i <= stor.rang_en_cours + 3; i++) {
              for (j = 0; j <= stor.type_en_cours; j++) {
                if (i - stor.ecart_en_cours >= 3 - stor.decimaux_en_cours) { stor.questions.push([i, stor.ecart_en_cours, j]) }
              }
            }
          } // On augmente l’écart

          this.typederreurs[0]++ //  Compte les bonnes réponses
          this.cacheBoutonValider() //  Plus de réponse à donner
          this.etat = 'navigation'
          this.sectionCourante() //  Passe à la question suivante
        } else {
        // Mauvaise réponse
          stor.erreurs.push([stor.rang, stor.ecart, stor.type]) // Mémorise l’erreur
          if (this.donneesSection.nbchances == 1) stor.erreurs.push([stor.rang, stor.ecart, stor.type]) // Il faudra réussir 2 fois pour confirmer son erreur

          j3pElement(idPrefix + 'correction').style.color = this.styles.cfaux // Commentaires à droite en rouge

          // Fini à cause de la limite de temps
          if (this.isElapsed) {
            this._stopTimer()

            j3pElement(idPrefix + 'correction').innerHTML = tempsDepasse // Commentaires à droite

            j3pDesactive(idPrefix + 'consigne1liste1') // Désactiver toutes les zones de saisie
            j3pBarre(idPrefix + 'consigne1liste1')

            this.typederreurs[10]++ //  Compte les temps dépassés
            this.cacheBoutonValider() //  Plus de réponse à donner

            // L’élève avait un temps limite mais ne l’a pas respecté. On affiche alors la correction
            affiche_correction(true)

            this.etat = 'navigation'
            this.sectionCourante() //  Passe à la question suivante
          } else {
          // Réponse fausse
            j3pElement(idPrefix + 'correction').innerHTML = cFaux // Commentaires à droite en rouge

            // Il reste des essais
            if (this.essaiCourant < this.donneesSection.nbchances) {
              j3pElement(idPrefix + 'correction').innerHTML += '<br>' + essaieEncore // Commentaires à droite

              this.typederreurs[1]++ //  Compte les essais ratés

              affiche_chances() //  affiche le nombre de chances qui restent

              // indication éventuelle ici
              affiche_correction(false)
            } else {
            // Il ne reste plus d’essais
              j3pElement(idPrefix + 'correction').innerHTML += '<br>' + regardeCorrection // Commentaires à droite
              //
              // Là il ne peut plus se corriger. On lui affiche alors la solution
              affiche_correction(true)

              this.typederreurs[2]++ //  Compte les réponses fausses

              this.etat = 'navigation'
              this.sectionCourante() //  Passe à la question suivante
            } // Il ne reste plus d’essais
          } // Réponse fausse
        } // Mauvaise réponse
        // Une réponse a été saisie
      } // Une réponse a été saisie

      // Obligatoire
      this.finCorrection()
      break }// case "correction":

    /// ////////////////////////////////////////////////////////////////////////////////////////
    //
    //            Phase correction
    //
    /// ////////////////////////////////////////////////////////////////////////////////////////

    case 'navigation': {
      //  Si on a posé toutes les questions, on rajoute les erreurs et on continue
      if (stor.questions.length == 0) {
        stor.questions = stor.questions.concat(stor.erreurs)
        stor.erreurs.length = 0 // Et on vide le tableau des erreurs
      }
      //    Arrête les questions si toutes les réponses sont justes
      if (stor.questions.length == 0) me.donneesSection.nbitems = me.questionCourante

      if (this.sectionTerminee()) {
        // On détermine la phrase d’état renvoyée par la section
        if ((this.typederreurs[1] == 0) && (this.typederreurs[2] == 0) && (this.typederreurs[10] == 0)) {
          //  Aucune erreur: Notion comprise
          if (this.donneesSection.limite != 0) this.parcours.pe = me.donneesSection.pe_1 //  Aucune erreur en temps limité: Notion totalement maitrisée
          else this.parcours.pe = me.donneesSection.pe_2
        } else if ((this.typederreurs[2] == 0) && (this.typederreurs[10] <= 2)) {
          //  Aucune erreur sans temps limité: Notion comprise
          this.parcours.pe = me.donneesSection.pe_3
        } else if (me.donneesSection.n_tableau >= 2) {
          //  A pu corriger ses erreurs: Notion fragile
          if (this.typederreurs[4] >= 2) this.parcours.pe = me.donneesSection.pe_5
          else this.parcours.pe = me.donneesSection.pe_6
        } else { this.parcours.pe = me.donneesSection.pe_4 } //  Notion non comprise
        this.afficheBoutonSectionSuivante()
        this.focus('sectioncontinuer')
      } else {
        this.etat = 'enonce'
        this.afficheBoutonSuite()
        this.focus('boutoncontinuer')
      }
      // Obligatoire
      this.finNavigation()

      break }// case "navigation":
  } //  switch etat
} //  SectionPosition
