import $ from 'jquery'
import { j3pAddElt, j3pBarre, j3pChaine, j3pDiv, j3pElement, j3pEmpty, j3pFocus, j3pGetRandomInt, j3pNombreBienEcrit, j3pRestriction, j3pValeurde } from 'src/legacy/core/functions'
import { colorKo } from 'src/legacy/core/StylesJ3p'
import ValidationZones from 'src/legacy/outils/calculatrice/ValidationZones'
import BulleAide from 'src/legacy/outils/bulleAide/BulleAide'
import { j3pAffiche, j3pDesactive } from 'src/lib/mathquill/functions'
import textesGeneriques from 'src/lib/core/textes'

const { cBien, cFaux, essaieEncore, regardeCorrection, tempsDepasse } = textesGeneriques

/**
 * Encadrer un nombre
 * @fileOverview
 * @author Xavier JEROME
 * @since 2018-07-13
 */

export const params = {

  outils: ['mathquill'],

  /**
   * Ce qui est paramétrable dans l’éditeur de graphe
   * @type {LegacySectionParamsParametres}
   */
  parametres: [
    ['indication', 'Encadre d’abord le nombre.', 'string', 'Indication en bas à gauche'], // Texte que l’élève peut faire apparaître en bas de l’écran
    ['nbrepetitionsmax', 10, 'entier', 'Nombre de répétitions maximum de la section.<BR>Il peut être inférieur si on répond juste à toutes les questions'], // Obligatoire: nombre de questions posées pendant l’exercice
    ['nbchances', 2, 'entier', 'Nombre d’essais par répétition'], // Nombre d’essais autorisés avant de déclarer un échec et de passer à la question
    ['limite', 0, 'entier', 'Temps disponible par répétition, en secondes'], // S’il n’est pas vide, le temps de réponse sera limité
    ['n_niveau', 1, 'entier', '1 pour sans pièges<BR>2 pour les cas particuliers: 2.99999 (retenue à droite) et 0.00007 (0 à gauche).'], // Niveau de difficulté
    ['t_rangs', [1, -2], 'array', "Intervalle des rangs autorisés<BR>de 3 pour millier (le max)<BR>0 pour unité<BR>jusqu'à -3 pour millièmes (le min)"], // Rangs autorisés
    ['s_arrondi', 'Les deux', 'string', "'Approché' pour valeur approchée par défaut ou par excès<br>'Arrondi' pour valeur arrondie<br>'Les deux' pour les deux"],
    ['n_aide', 1, 'entier', '0 pour aucune aide<BR>1 pour une explication des erreurs'] // Niveau d’aide autorisé
  ],

  pe: [
    { pe_1: 'Notion totalement maitrisée.' },
    { pe_2: 'Notion comprise.' },
    { pe_3: 'Notion comprise, mais la séparation des classes n’est pas acquise.' },
    { pe_4: 'Notion comprise, mais il reste des zéros inutiles.' },
    { pe_5: 'Notion fragile.' },
    { pe_6: 'Notion de dixième non comprise.' },
    { pe_7: 'Notion de dixièmes consécutifs non comprise.' },
    { pe_8: 'Erreur dans l’ordre des nombres.' }
  ]
}

// on reconstruit un objet standard pour le traiter dans le case navigation
const phrasesEtat = {}
for (const pe of params.pe) {
  for (const [key, value] of Object.entries(pe)) {
    phrasesEtat[key] = value
  }
}

const prefix = 'approche'
const textes = {
  nom_positions: ['millième', 'centième', 'dixième', 'unité', 'dizaine', 'centaine', 'millier'],
  nom_arrondi: ['approchée par défaut', 'approchée par excès', 'arrondie'],
  nom_choix: ['petit', 'grand', 'proche'],
  phraseTitre: 'Donner une valeur approchée à ... près.', //  Titre de la section
  phraseNbChances: 'Il te reste £e essai£p.', //  Ne pas supprimer
  phraseEnonce: '£u valeur £a £e£r de £n est @1@',
  phraseSolution1: '£u valeur £a £e£r de £n est £b',
  phraseSolution2: '£n n’est pas £u valeur £a <b>£e£r</b>.',
  phraseSolution3: '£n : Sépare correctement les classes par des espaces.',
  phraseSolution4: '£a est trop éloigné de £n',
  phraseSolution5: 'Tu as choisi le mauvais nombre.<br>Relis bien la consigne.',
  phraseSolution6: '£a : Evite d’écrire des zéros inutiles.',
  phraseSolution10: 'On commence par encadrer £n £e£r près<BR>£a < £n < £b',
  Aide01: 'Tu dois toujours mettre un espace<br>entre les milliers et les centaines<br>et après les millièmes.',
  Aide02: 'Dans un£e £r, il n’y a aucun chiffre autre que 0 à droite des £rs.  ',
  Aide03: 'Tu dois d’abord encadrer £n £e£r près.<br>Puis choisir le bon nombre.',
  Aide04: '£u valeur £a £e£r est le nombre le plus £c.',
  Aide05: 'Supprime les zéros à gauche et à droite du nombre: ',
  Aide06: 'même si ici, tu peux mettre le zero des £rs pour préciser que le nombre est un£e £r.',
  Aide07: 'Comme le chiffre à droite des £rs est £a,<br>le plus proche est £b'
}

/**
 * section Approche
 * @this {Parcours}
 */
export default function main () {
  const me = this
  const ds = this.donneesSection
  const stor = this.storage
  /// ////////////////////////////////////////////////////////////////////////////////////////
  //
  //                  Données et paramètres de la section
  //
  /// ////////////////////////////////////////////////////////////////////////////////////////

  /// ////////////////////////////////////////////////////////////////////////////////////////
  //
  //              Ajouter ici toutes les fonctions nécessaires à la section
  //
  /// ////////////////////////////////////////////////////////////////////////////////////////

  //  Reformate un nombre
  function corrigeNombre (nombre) {
    //  Corrige un nombre saisi sous forme de texte
    //  Enlève les espaces, les zéros inutiles
    //  Met une virgule
    //  corrige la séparation des classes
    const motif = /^\d{0,3}(\s\d{3}){0,5}(,(\d{3}\s){0,5}\d{0,3})?$/
    let classes = true
    if (nombre.length === 0) return [nombre, true]
    nombre = nombre.trim() // Supprime les espaces inutiles
    nombre = nombre.replace('.', ',') // Remplace les points par des virgules
    if (!nombre.includes(',')) nombre += ',' //  Rajoute la virgule si c’est un entier
    while (nombre !== nombre.replace('  ', ' ')) nombre = nombre.replace('  ', ' ') //  Cas où il y a plus d’un espace entre les classes
    let repZeros = nombre // Nombre avant d’enlever les 0 inutiles
    if (repZeros.endsWith(',')) repZeros = repZeros.substr(0, repZeros.length - 1) //  Enlève la virgule si c’est un entier
    if (repZeros[0] === ',') repZeros = '0' + repZeros // Si on a enlevé le zéro des unités
    if (nombre[0] === ',') nombre = '0' + nombre // Si on a enlevé le zéro des unités
    while ((nombre[0] === ' ') || (nombre[0] === '0')) nombre = nombre.substring(1) //  Enlève les 0 et les espaces à gauche inutiles
    while (nombre.endsWith(' ') || nombre.endsWith('0')) nombre = nombre.substring(0, nombre.length - 2) //  Enlève les 0 et les espaces à droite inutiles
    if (nombre[0] === ',') nombre = '0' + nombre // Si on a enlevé le zéro des unités
    if (nombre.endsWith(',')) nombre = nombre.substr(0, nombre.length - 1) //  Enlève la virgule si c’est un entier
    const zeros = (nombre === repZeros) // Si le nombre a changé, c’est qu’il y avait des 0 inutiles
    if (!nombre.match(motif)) {
      classes = false
      while (nombre !== nombre.replace(' ', '')) nombre = nombre.replace(' ', '') //  Cas où il y a plus d’un espace entre les classes
      nombre = nombre.replace(',', '.') // Remplace les points par des virgules
      nombre = j3pNombreBienEcrit(parseFloat(nombre))
      nombre = nombre.replace('.', ',') // Remplace les points par des virgules
    } //  match
    return [nombre, classes, zeros, repZeros]
  } //  corrigeNombre

  //   redimensionne la zone de saisie si on a changé le contenu
  function redimensionne (nomzone) {
    // fonction appelée pour afficher la correction (que la réponse soit bonne ou fausse
    const fontZone = j3pElement(nomzone).style.fontFamily
    const tailleFontZone = j3pElement(nomzone).style.fontSize
    const texteZone = j3pElement(nomzone).value
    const longueur = $('#' + nomzone).textWidth(texteZone, fontZone, tailleFontZone)
    const decalage = 6
    j3pElement(nomzone).style.width = (longueur + 2 * decalage) + 'px'
  } // redimensionne

  //   Affiche la correction sous l’énoncé
  function afficheCorrection (fin) {
    // fonction appelée pour afficher la correction (que la réponse soit bonne ou fausse)
    const divExplication = stor.divExplication1
    divExplication.innerHTML = '' //  Pour effacer  un éventuel précédent affichage
    let strDebut1 = ''
    let strFin1 = ''
    if (fin && !stor.validation.bonneReponse) { //  On affiche la bonne réponse s’il n’a pas trouvé au dernier essai
      j3pAffiche(divExplication, '', textes.phraseSolution10,
        {
          styletexte: '',
          n: stor.strNombre, //  Nombre formaté avec les espaces
          r: textes.nom_positions[stor.question - 1 - 3], //  Chaine correspondant au rang demandé
          e: (stor.question === 7 ? "à l'" : ((stor.question >= 8) && (stor.question <= 9) ? 'à la ' : 'au ')),
          a: stor.strReponse1,
          b: stor.strReponse2
        })

      j3pAffiche(divExplication, '', '\n' + textes.Aide04,
        {
          styletexte: '',
          u: (stor.arrondi === 3 ? 'La' : 'Une'),
          a: textes.nom_arrondi[stor.arrondi - 1],
          r: textes.nom_positions[stor.question - 1 - 3], //  Chaine correspondant au rang demandé
          e: (stor.question === 7 ? "à l'" : ((stor.question >= 8) && (stor.question <= 9) ? 'à la ' : 'au ')),
          c: textes.nom_choix[stor.arrondi - 1]
        })

      if (stor.arrondi === 3) {
        j3pAffiche(divExplication, '', '\n' + textes.Aide07,
          {
            styletexte: '',
            a: stor.tableau_nombre[stor.arrondi_adaptee],
            r: textes.nom_positions[stor.question - 1 - 3], //  Chaine correspondant au rang demandé
            b: stor.s_reponse
          })
      }

      j3pAffiche(divExplication, '', '\n' + textes.phraseSolution1,
        {
          styletexte: '',
          u: (stor.arrondi === 3 ? 'La' : 'Une'),
          a: textes.nom_arrondi[stor.arrondi - 1],
          n: stor.strNombre, //  Nombre formaté avec les espaces
          r: textes.nom_positions[stor.question - 1 - 3], //  Chaine correspondant au rang demandé
          e: (stor.question === 7 ? "à l'" : ((stor.question >= 8) && (stor.question <= 9) ? 'à la ' : 'au ')),
          b: stor.s_reponse
        })
    }
    // fin

    if (stor.validation.aRepondu) {
      j3pEmpty(stor.divExplication2)

      const reponseEleve = j3pValeurde(prefix + 'consigne1input1') //  Récupère la réponse
      // Modifié en attendant es6
      //      var reponsesEleve = (' '.repeat(7 - (!reponseEleve.includes(',')) ? reponseEleve.length : reponseEleve.indexOf(','))) + reponseEleve).padEnd(15, ' ').split('') //  Transforme la réponse en tableau
      let reponsesEleve = ''
      const nbSpaces = 7 - (reponseEleve.includes(',') ? reponseEleve.indexOf(',') : reponseEleve.length)
      for (let i = 0; i < nbSpaces; i++) reponsesEleve += ' '
      reponsesEleve += reponseEleve
      while (reponsesEleve.length < 15) reponsesEleve += ' '
      reponsesEleve = reponsesEleve.split('')

      const repEleve1 = parseFloat(reponseEleve.replace(',', '.').replace(' ', ''))
      const repNombre = parseFloat(stor.strNombre.replace(',', '.').replace(' ', ''))

      //  Rang de la question tenant compte des espaces et des virgules et dans l’ordre d’écriture
      const questionAdaptee = 15 - (stor.question > 3 ? ((stor.question > 6 ? ((stor.question > 9 ? stor.question + 3 : stor.question + 2)) : stor.question + 1)) : stor.question)

      //  Indique si il y avait des zeros inutiles
      if (!stor.zeros1) {
        j3pDiv(stor.divExplication2, prefix + 'corr27', '')
        strDebut1 = ''
        if (repEleve1 === 0) {
          if (stor.repZeros1.includes(',')) {
            strDebut1 = stor.repZeros1.substring(0, stor.repZeros1.indexOf(',') - 1)
          } else {
            strDebut1 = stor.repZeros1.substring(0, stor.repZeros1.length - 1)
          }
        } else if (stor.repZeros1.includes(reponseEleve)) {
          strDebut1 = stor.repZeros1.substring(0, stor.repZeros1.indexOf(reponseEleve))
        } else {
          for (let i = 0; i <= 6; i++) {
            if ((reponsesEleve[i] !== ' ') && (reponsesEleve[i] !== '0') && (reponsesEleve[i] !== ',')) {
              if (stor.repZeros1.includes(reponsesEleve[i])) {
                strDebut1 = stor.repZeros1.substring(0, stor.repZeros1.indexOf(reponsesEleve[i]))
                break
              }
            } else strDebut1 = stor.repZeros1.substring(0, Math.min(i, 5))
          }
        }
        if (repEleve1 === 0) {
          if (!reponseEleve.includes(',')) {
            strFin1 = stor.repZeros1.substring(stor.repZeros1.indexOf(','))
          } else {
            // Modifié en attendant es6
            //              strFin1 = (' '.repeat(7 - (!stor.repZeros1.includes(',')) ? stor.repZeros1.length : stor.repZeros1.indexOf(','))) + stor.repZeros1).padEnd(15, ' ').substring(questionAdaptee + 1).trimRight()
            const aSoustraire = stor.repZeros1.includes(',') ? stor.repZeros1.indexOf(',') : stor.repZeros1.length
            const nbSpaceFini = 7 - aSoustraire
            for (let i = 0; i < nbSpaceFini; i++) strFin1 += ' '
            strFin1 += stor.repZeros1
            strFin1 = strFin1.substring(questionAdaptee + 1).trimEnd()
          }
        } else if (stor.repZeros1.includes(reponseEleve)) strFin1 = stor.repZeros1.substring(stor.repZeros1.indexOf(reponseEleve) + reponseEleve.length)
        else {
          for (let i = 0; i <= reponseEleve.length - 1; i++) {
            if (stor.repZeros1.indexOf(reponseEleve.substring(i)) !== -1) {
              strFin1 = stor.repZeros1.substring(stor.repZeros1.indexOf(reponseEleve.substring(i)) + reponseEleve.length - i)
              break
            }
          }
        }
        if ((strDebut1.length + strFin1.length !== 0) && (me.donneesSection.n_aide >= 1)) {
          const { parent } = j3pAffiche(prefix + 'corr27', '', textes.phraseSolution6, {
            styletexte: '',
            r: textes.nom_positions[stor.question - 1 - 3], //  Chaine correspondant au rang demandé
            e: ((stor.question >= 7) && (stor.question <= 9) ? 'e' : ''),
            a: stor.repZeros1
          }) //  Affiche le nombre correctement écrit
          const span = j3pAddElt(parent, 'span')
          const bulle = new BulleAide(span, textes.Aide05)
          // on veut ajouter des trucs à cette bulle
          j3pAddElt(bulle.bulleDiv, 'br')
          j3pAddElt(bulle.bulleDiv, 'span', strDebut1, { style: { color: colorKo } })
          j3pAddElt(bulle.bulleDiv, 'span', reponseEleve)
          j3pAddElt(bulle.bulleDiv, 'span', strFin1, { style: { color: colorKo } })
          if (reponsesEleve[questionAdaptee] === '0') {
            j3pAddElt(bulle.bulleDiv, 'br')
            const contentSup = j3pChaine(textes.Aide06, {
              styletexte: '',
              r: textes.nom_positions[stor.question - 1 - 3], //  Chaine correspondant au rang demandé
              e: ((stor.question >= 7) && (stor.question <= 9) ? 'e' : '')
            })
            j3pAddElt(bulle.bulleDiv, 'span', contentSup)
          }
          stor.bullesAide.push(bulle)
        }
        // Erreur à afficher
      }
      //  Erreur zéros 1

      //  Indique si les classes étaient correctement séparées
      if ((!stor.espaces1) && (me.donneesSection.n_aide >= 1)) {
        const div = j3pAddElt(stor.divExplication2, 'div')
        const { parent } = j3pAffiche(div, '', textes.phraseSolution3,
          {
            styletexte: '',
            n: reponseEleve
          }) //  Affiche le nombre correctement écrit
        const spanBulle = j3pAddElt(parent, 'span')
        stor.bullesAide.push(new BulleAide(spanBulle, textes.Aide01))
      }
      //  Erreur espace 1

      //  Regarde si le nombre en bien un dixième, etc
      let correct1 = true
      for (const [i, rep] of reponsesEleve.entries()) {
        correct1 = correct1 && ((i <= questionAdaptee) || (rep === '0') || (rep === ' ') || (rep === ','))
      }
      if (me.donneesSection.n_aide >= 1) {
        if (!correct1) {
          const div = j3pAddElt(stor.divExplication2, 'div')
          const { parent } = j3pAffiche(div, '', textes.phraseSolution2, {
            styletexte: '',
            u: (stor.arrondi === 3 ? 'la' : 'une'),
            a: textes.nom_arrondi[stor.arrondi - 1],
            r: textes.nom_positions[stor.question - 1 - 3], //  Chaine correspondant au rang demandé
            e: (stor.question === 7 ? "à l'" : ((stor.question >= 8) && (stor.question <= 9) ? 'à la ' : 'au ')),
            n: reponseEleve
          })
          const spanBulle = j3pAddElt(parent, 'span')
          const content = j3pChaine(textes.Aide02, {
            styletexte: '',
            r: textes.nom_positions[stor.question - 1 - 3], //  Chaine correspondant au rang demandé
            e: ((stor.question >= 7) && (stor.question <= 9) ? 'e' : '')
          })
          const bulle = new BulleAide(spanBulle, content)
          j3pAddElt(bulle.bulleDiv, 'span', reponsesEleve.slice(0, questionAdaptee + 1).join('').trimStart())
          j3pAddElt(bulle.bulleDiv, 'span', reponsesEleve.slice(questionAdaptee + 1, 14).join('').trimEnd(), { style: { color: colorKo } })
          stor.bullesAide.push(bulle)
          //  Erreur dixième 1
        } else if (Math.abs((repNombre - repEleve1)) / Math.pow(10, stor.question - 7) > 1) {
          //  Regarde si la réponse est trop éloignée
          const div = j3pAddElt(stor.divExplication2, 'div')
          const { parent } = j3pAffiche(div, '', textes.phraseSolution4, {
            styletexte: '',
            n: stor.strNombre,
            a: reponseEleve
          })
          const spanBulle = j3pAddElt(parent, 'span')
          const content = j3pChaine(textes.Aide03,
            {
              styletexte: '',
              n: stor.strNombre,
              r: textes.nom_positions[stor.question - 1 - 3], //  Chaine correspondant au rang demandé
              e: (stor.question === 7 ? "à l'" : ((stor.question >= 8) && (stor.question <= 9) ? 'à la ' : 'au '))
            })
          stor.bullesAide.push(new BulleAide(spanBulle, content))
          //  Il a choisi le mauvais
        } else if (((reponseEleve === stor.strReponse1) || (reponseEleve === stor.strReponse2)) && (reponseEleve !== stor.s_reponse)) {
          const div = j3pAddElt(stor.divExplication2, 'div')
          const { parent } = j3pAffiche(div, '', textes.phraseSolution5, { styletexte: '' })
          const span = j3pAddElt(parent, 'span')
          const contenuBulle = j3pChaine(textes.Aide04, {
            styletexte: '',
            u: (stor.arrondi === 3 ? 'La' : 'Une'),
            a: textes.nom_arrondi[stor.arrondi - 1],
            r: textes.nom_positions[stor.question - 1 - 3], //  Chaine correspondant au rang demandé
            e: (stor.question === 7 ? "à l'" : ((stor.question >= 8) && (stor.question <= 9) ? 'à la ' : 'au ')),
            c: textes.nom_choix[stor.arrondi - 1]
          })
          const bulle = new BulleAide(span, contenuBulle)
          if (stor.arrondi === 3) {
            j3pAddElt(bulle.bulleDiv, 'br')
            j3pAddElt(bulle.bulleDiv, 'span', j3pChaine(textes.Aide07, {
              styletexte: '',
              a: stor.tableau_nombre[stor.arrondi_adaptee],
              r: textes.nom_positions[stor.question - 1 - 3], //  Chaine correspondant au rang demandé
              b: stor.s_reponse
            }))
          }
          stor.bullesAide.push(bulle)
          // Complément arrondi
        }
      }
      // Le mauvais

      if (!correct1) me.typederreurs[3]++ // Rang incorrect
      else if (Math.abs((repNombre - repEleve1)) / Math.pow(10, stor.question - 7) > 1) me.typederreurs[4]++ // Ordre incorrect
      else if (((reponseEleve === stor.strReponse1) || (reponseEleve === stor.strReponse2)) && (reponseEleve !== stor.s_reponse)) me.typederreurs[5]++ // Non consécutifs
      if (!stor.espaces1) me.typederreurs[6]++ //  Les séparations de classes ne sont pas correctes
      if (!stor.zeros1 && (strDebut1.length + strFin1.length !== 0)) me.typederreurs[7]++ // Zéros inutiles
    }
    //  A répondu
  } //  afficheCorrection

  // Affiche le nombre de tentatives qu’il reste s’il y a plusieurs chances
  function afficheChances () {
    j3pElement(prefix + 'zone_chances').innerHTML = ''
    if (me.donneesSection.nbchances > 1) {
      j3pAffiche(prefix + 'zone_chances', prefix + 'chances', textes.phraseNbChances, {
        e: me.donneesSection.nbchances - me.essaiCourant,
        p: (me.donneesSection.nbchances - me.essaiCourant > 1 ? 's' : '')
      })
    }
    // Pour le pluriel
  }

  /// ////////////////////////////////////////////////////////////////////////////////////////
  //
  //              Déroulement de la section Enonce, Correction, navigation
  //
  /// ////////////////////////////////////////////////////////////////////////////////////////

  switch (this.etat) {
    /// ////////////////////////////////////////////////////////////////////////////////////////
    //
    //            Phase énoncé
    //
    /// ////////////////////////////////////////////////////////////////////////////////////////

    case 'enonce': {
      //  Construit l’énoncé: exécuté au début de chaque question
      if (this.debutDeLaSection) {
        //  Code exécuté uniquement au lancement de la section
        this.surcharge({ nbrepetitions: ds.nbrepetitionsmax })

        // Construction de la page
        this.construitStructurePage('presentation1')
        this.afficheTitre(textes.phraseTitre)
        // Affiche l’indication en bas à gauche
        if (this.donneesSection.indication) this.indication(this.zones.IG, this.donneesSection.indication)

        /*
                 Par convention,`
                `   this.typederreurs[0] = nombre de bonnes réponses
                    this.typederreurs[1] = nombre de fois où l’élève a utilisé une chance supplémentaire
                    this.typederreurs[2] = nombre de mauvaises réponses
                    this.typederreurs[3] = nombre de fois où le rang est incorrect
                    this.typederreurs[4] = nombre de fois où le nombre est trop éloigné
                    this.typederreurs[5] = nombre de fois où il a choisi le mauvais
                    this.typederreurs[6] = nombre de fois où les classes ne sont pas séparées
                    this.typederreurs[7] = nombre de fois où il y a des zéros inutiles
                    this.typederreurs[10] = nombre de fois où l’élève a eu faux à cause d’un temps limite
                    LES AUTRES peuvent être utilisées par la détermination de la pe dans le cas d’une section qualitative
                 */
        this.typederreurs = [0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0]

        // Permet l’utilisation du compte à rebours si un temps est défini dans les paramètre

        // Contient les liens vers les bulles d’aide
        stor.bullesAide = []

        //  Paramètres et variables particuliers à la section
        me.donneesSection.t_rangs_adapte = []
        me.donneesSection.t_rangs_adapte[0] = me.donneesSection.t_rangs[0] + 7
        me.donneesSection.t_rangs_adapte[1] = me.donneesSection.t_rangs[1] + 7

        //  t_rang_en_cours permet une difficulté croissante
        stor.t_rang_en_cours = []
        if (me.donneesSection.t_rangs_adapte[0] >= 6) {
          if (me.donneesSection.t_rangs_adapte[1] <= 6) stor.t_rang_en_cours[0] = 6 // On commence par les dixièmes s’ils font partie de l’intervalle
          else stor.t_rang_en_cours[0] = me.donneesSection.t_rangs_adapte[1]
        } else {
          // Sinon on prend le plus proche
          stor.t_rang_en_cours[0] = me.donneesSection.t_rangs_adapte[1]
        }
        stor.t_rang_en_cours[1] = stor.t_rang_en_cours[0]
        stor.difficulte_en_cours = 0 // On commence sans cas particulier
        stor.arrondiEnCours = []
        if (me.donneesSection.s_arrondi !== 'Arrondi') stor.arrondiEnCours.push(1)
        else stor.arrondiEnCours.push(3)

        //  Tableaux permettant de ne pas poser 2 fois la mêstor.question
        stor.questions = [] // Contient les question qui n’ont pas encore été posées
        for (let i = stor.t_rang_en_cours[1]; i <= stor.t_rang_en_cours[0]; i++) {
          stor.questions.push([i, stor.arrondiEnCours[0], 0])
          stor.questions.push([i, stor.arrondiEnCours[0], 0])
        }
        //  2 essais au point de départ

        stor.erreurs = [] // Contient questions qui ont provoqué une erreur pour les représenter en priorité
      } else {
        //  Code exécuté à chaque nouvelle question: Partie à ne pas modifier
        // A chaque répétition, on nettoie la scène
        // Efface les bulles d’aide
        for (let i = stor.bullesAide.length - 1; i > -1; i--) {
          stor.bullesAide[i].disable()
          stor.bullesAide.pop()
        }
        this.videLesZones()
      }

      /// ////////////////////////////////////////////////////////////////////////////////////////
      //
      //               Création de l’énoncé
      //
      /// ////////////////////////////////////////////////////////////////////////////////////////

      //  Création de l’énoncé
      {
      //    Conventions de la manipulation des nombres affichés
      /**************************************************************************************
       *
       *                  999 999, 999 999
       *     rangs          3   0 -1 -3
       *     question    12              1
       *
       *
       *
       ***************************************************************************************/
        // question est le rang demandé;
        // difficulté vaut 2 si le nombre de gauche est 0, 2 si le rang vaut 9, 0 sinon
        const n = j3pGetRandomInt(0, stor.questions.length - 1) // La question est choisie parmi celles qui n’ont pas encore été posées
        stor.question = stor.questions[n][0] // Une question est un triplet rang arrondi difficulté
        stor.arrondi = stor.questions[n][1]
        stor.difficulte = stor.questions[n][2]
        stor.questions.splice(n, 1) // On la retire des questions à poser

        //  reponse1 est le nombre encadrant à gauche
        //  Le nombre sous forme de tableau de chiffres avec les millionièmes à gauche et sans virgule
        // Modifié en attendant es6
        // fill n’existe pas sur IE11
        // stor.tableau_reponse1 = new Array(12).fill(' ') // Crée un nombre vide
        stor.tableau_reponse1 = []
        for (let i = 0; i < 12; i++) stor.tableau_reponse1.push(' ')

        //  Fixe le rang demandé: 0 pour difficulté 1, 9 pour difficulté 2 et 1 à 8 sinon
        if (stor.difficulte === 1) {
          if (stor.question < 7) { stor.tableau_reponse1[stor.question - 1] = 0 } else { stor.tableau_reponse1[stor.question - 1] = ' ' }
        } else if (stor.difficulte === 2) {
        // à partir des dizaines
          stor.tableau_reponse1[stor.question - 1] = 9
        } else {
          stor.tableau_reponse1[stor.question - 1] = j3pGetRandomInt(1, 8)
        }

        if (stor.tableau_reponse1[6] === ' ') stor.tableau_reponse1[6] = 0 // On s’assure qu’il y a un 0 aux unités
        if (stor.difficulte !== 1) { // Mais pas en difficulté 1
          for (let i = stor.question - 1; i >= 8; i--) {
          // Et même jusqu’au rang demandé si besoin
            stor.tableau_reponse1[i - 1] = 0
          }
        }
        for (let i = stor.question + 1; i <= 6; i++) {
        // Et jusqu’aux dixièmes dans le cas d’un nombre décimal
          stor.tableau_reponse1[i - 1] = 0
        }

        let chiffres
        //  On choisit le nombre de chiffres significatifs rajoutés avant
        if (stor.difficulte !== 1) { // Mais pas en difficulté 1
          chiffres = j3pGetRandomInt(0, 3)
          for (let i = stor.question + 1; i <= Math.min(stor.question + chiffres, 12); i++) {
          // On rajoute les chiffres nécessaires un à un
            stor.tableau_reponse1[i - 1] = j3pGetRandomInt(1, 9)
          }
        }

        if ((stor.difficulte === 2) && (stor.tableau_reponse1[0] === 9)) stor.tableau_reponse1[0] = 8 // Par sécurité pour ne pas atteindre le 1 000 000

        //  reponse2 est le nombre encadrant à droite
        stor.tableau_reponse2 = stor.tableau_reponse1.slice()
        stor.tableau_reponse2[stor.question - 1] = stor.tableau_reponse1[stor.question - 1] + 1

        for (let i = stor.question - 1; i <= 11; i++) {
          if (stor.tableau_reponse2[i] === 10) {
            stor.tableau_reponse2[i] = 0
            stor.tableau_reponse2[i + 1] = (stor.tableau_reponse2[i + 1] === ' ' ? stor.tableau_reponse2[i + 1] = 1 : stor.tableau_reponse2[i + 1] + 1)
          }
        }
        // reporte la retenue

        if (stor.difficulte === 1) { // Rajoute les 0 en difficulté 1
          for (let i = stor.question - 1; i >= 8; i--) { stor.tableau_reponse2[i - 1] = 0 }
        }

        //  nombre est le nombre à encadrer
        stor.tableau_nombre = stor.tableau_reponse1.slice()

        chiffres = j3pGetRandomInt(1, 3) // Au moins un chiffre significatif juste après
        for (let i = stor.question - 1; i >= stor.question - chiffres; i--) {
        // On rajoute les chiffres nécessaires un à un
          stor.tableau_nombre[i - 1] = j3pGetRandomInt(1, 9)
        }

        if (stor.difficulte === 1) { // Rajoute les 0 en difficulté 1
          for (let i = stor.question - chiffres - 1; i >= 8; i--) {
            stor.tableau_nombre[i - 1] = 0
          }
        }

        stor.tableau_reponse1.splice(6, 0, ',') // On rajoute la virgule
        stor.tableau_reponse2.splice(6, 0, ',') // On rajoute la virgule
        stor.tableau_nombre.splice(6, 0, ',') // On rajoute la virgule
        stor.tableau_reponse1 = stor.tableau_reponse1.reverse() // On remet le nombre à l’endroit
        stor.tableau_reponse2 = stor.tableau_reponse2.reverse() // On remet le nombre à l’endroit
        stor.tableau_nombre = stor.tableau_nombre.reverse() // On remet le nombre à l’endroit

        stor.tableau_reponse1.splice(3, 0, ' ') // On rajoute les espaces des milliers
        stor.tableau_reponse2.splice(3, 0, ' ')
        stor.tableau_nombre.splice(3, 0, ' ')
        stor.tableau_reponse1.splice(11, 0, ' ') // On rajoute les espaces des millièmes
        stor.tableau_reponse2.splice(11, 0, ' ')
        stor.tableau_nombre.splice(11, 0, ' ')

        stor.strReponse1 = stor.tableau_reponse1.join('').trim() // On transforme les nombres en chaines sans espaces inutiles
        stor.strReponse2 = stor.tableau_reponse2.join('').trim()
        stor.strNombre = stor.tableau_nombre.join('').trim()
        if (stor.strReponse1.endsWith(',')) stor.strReponse1 = stor.strReponse1.substr(0, stor.strReponse1.length - 1) //  Enlève la virgule si c’est un entier
        if (stor.strReponse2.endsWith(',')) stor.strReponse2 = stor.strReponse2.substr(0, stor.strReponse2.length - 1)
        if (stor.strNombre.endsWith(',')) stor.strNombre = stor.strNombre.substr(0, stor.strNombre.length - 1)

        stor.reponse1 = [stor.strReponse1] // On met les réponses possibles
        let strReponse1 = stor.strReponse1
        while ((strReponse1.endsWith('0') && strReponse1.includes(',')) || strReponse1.endsWith(',')) {
          strReponse1 = strReponse1.substr(0, strReponse1.length - 1)
          stor.reponse1.push(strReponse1)
        }
        //  Avec ou sans les zéros à droite jusqu’au rang demandé

        stor.reponse2 = [stor.strReponse2]
        let strReponse2 = stor.strReponse2
        while ((strReponse2.endsWith('0') && strReponse2.includes(',')) || strReponse2.endsWith(',')) {
          strReponse2 = strReponse2.substr(0, strReponse2.length - 1)
          stor.reponse2.push(strReponse2)
        }

        //  Place du chiffre déterminant l’arrondi en tenant compte des espaces et des virgules et dans l’ordre d’écriture
        stor.arrondi_adaptee = 15 - (stor.question - 1 > 3 ? ((stor.question - 1 > 6 ? ((stor.question - 1 > 9 ? stor.question - 1 + 3 : stor.question - 1 + 2)) : stor.question - 1 + 1)) : stor.question - 1)

        switch (stor.arrondi) {
          case 1 :
            stor.reponse = stor.reponse1
            stor.s_reponse = stor.strReponse1
            break
          case 2 :
            stor.reponse = stor.reponse2
            stor.s_reponse = stor.strReponse2
            break
          case 3 :
            if (stor.tableau_nombre[stor.arrondi_adaptee] < 5) {
              stor.reponse = stor.reponse1
              stor.s_reponse = stor.strReponse1
            } else {
              stor.reponse = stor.reponse2
              stor.s_reponse = stor.strReponse2
            }
            break
        }
      // swith arrondi
      } // Création de l’énoncé

      //  Création des conteneurs
      // Création du conteneur dans lequel se trouveront toutes les consignes
      j3pDiv(this.zones.MG, {
        id: prefix + 'consignes',
        contenu: '',
        coord: [0, 0],
        style: me.styles.etendre('petit.enonce', { padding: '10px', marginTop: '25px' })
      })

      // Dans ce conteneur, on crée un div par phrase (cela forcera le retour à la ligne)
      j3pDiv(prefix + 'consignes', prefix + 'zone_consigne1', '')

      // Affiche le nombre de chances qu’il reste
      j3pDiv(prefix + 'consignes', {
        id: prefix + 'zone_chances',
        contenu: '',
        style: me.styles.etendre('moyen.enonce', { marginTop: '30px' })
      })
      afficheChances()

      // Si on veut rajouter des explications, en cas de réponse fausse par exemple
      const ctExpl = j3pDiv(this.zones.MG, {
        contenu: '',
        coord: [10, 200],
        style: me.styles.petit.explications
      })
      stor.divExplication1 = j3pAddElt(ctExpl, 'div')
      stor.divExplication2 = j3pAddElt(ctExpl, 'div', '', { style: { marginTop: '20px' } })
      stor.divExplication3 = j3pAddElt(ctExpl, 'div')

      // On crée ici le div qui contiendra toutes les remarques sur ce qu’on pense de la réponse de l’élève (le plus souvant dans le cadre MD)
      stor.divCorrection = j3pAddElt(this.zones.MD, 'div', '', { style: me.styles.etendre('petit.correction', { padding: '10px' }) })
      // fin création des conteneurs

      //  Initialisation des zones de saisie
      j3pAffiche(prefix + 'zone_consigne1', prefix + 'consigne1', textes.phraseEnonce, {
        styletexte: '',
        u: (stor.arrondi === 3 ? 'La' : 'Une'),
        a: textes.nom_arrondi[stor.arrondi - 1],
        n: stor.strNombre, //  Nombre formaté avec les espaces
        r: textes.nom_positions[stor.question - 1 - 3], //  Chaine correspondant au rang demandé
        e: (stor.question === 7 ? "à l'" : ((stor.question >= 8) && (stor.question <= 9) ? 'à la ' : 'au ')),
        input1: { dynamique: true, width: '12px' }
      })

      // Le nom de chaque zone de saisie est construit de la même façon pour les zones input et inputmq :
      // le nom input est toujours précédé du nom du span qui contient la zone (ce span est le deuxième paramètre de la fonction j3pAffiche

      // On rajoute les contraintes de saisie
      const input = j3pElement(prefix + 'consigne1input1')
      j3pRestriction(input, '0-9,. ') // on n’autorise que les chiffres

      //  Permet de refaire passer la zone de saisie en noir lors du deuxième essai
      input.addEventListener('keypress', () => { input.style.color = '' })

      //  Empêche qu’il y ait deux virgules dans le nombre
      input.addEventListener('keydown', (event) => {
        if ((event.key === ',') || (event.key === '.')) {
          const value = j3pValeurde(input)
          if (value.includes(',') || value.includes('.')) {
            event.preventDefault()
          }
        }
      })

      // On rajoute toutes les informations qui permettront de valider la saisie
      input.typeReponse = ['texte']
      input.reponse = stor.reponse

      // Paramétrage de la validation
      // zonesSaisie est un tableau contenant le nom de chacune des zones (on pourrait aussi y mettre une liste déroulante)
      const zonesSaisie = [input]
      // validePerso est souvant un tableau vide
      // Mais parfois certaines zones ne peuvent pas être validée de manière automatique
      // Donc la validation de ces zones devra se faire "à la main".
      // Par contre on vérifiera si elles sont remplies ou non.
      const zonesValidePerso = []

      // fctsValid est un objet permettant la validation. Il contient le nom des zones (tableau précédent) et un deuxième paramètre validePerso
      stor.fctsValid = new ValidationZones({
        parcours: me,
        zones: zonesSaisie,
        validePerso: zonesValidePerso
      })
      //  Initialisation des zones de saisie

      j3pFocus(input)

      this.finEnonce() // Obligatoire
      //  Partie exécutée à chaque essai
      break
    }

    /// ////////////////////////////////////////////////////////////////////////////////////////
    //
    //            Phase correction
    //
    /// ////////////////////////////////////////////////////////////////////////////////////////

    case 'correction': {
      const fctsValid = stor.fctsValid // ici je crée juste une variable pour raccourcir le nom de l’object fctsValid

      //  Efface le nombre de chances qui restent
      j3pElement(prefix + 'zone_chances').innerHTML = ''

      // Efface les bulles d’aide
      for (let i = stor.bullesAide.length - 1; i > -1; i--) {
        stor.bullesAide[i].disable()
        stor.bullesAide.pop()
      }

      //  Efface les éléments de correction
      j3pEmpty(stor.divExplication1)
      j3pEmpty(stor.divExplication2)
      j3pEmpty(stor.divExplication3)

      // Modifié en attendant es6
      //    [j3pElement(prefix + 'consigne1input1').value, stor.espaces1, stor.zeros1, stor.repZeros1] = corrigeNombre(j3pValeurde(prefix + 'consigne1input1')) // Remplace la saisie par un nombre plus propre
      const resultat = corrigeNombre(j3pValeurde(prefix + 'consigne1input1'))
      j3pElement(prefix + 'consigne1input1').value = resultat[0]
      stor.espaces1 = resultat[1]
      stor.zeros1 = resultat[2]
      stor.repZeros1 = resultat[3]

      redimensionne(prefix + 'consigne1input1')

      // ce qui suit sert pour la validation de toutes les zones
      // le tableau contenant toutes les zones de saisie
      stor.validation = fctsValid.validationGlobale() // Ne pas modifier
      // A cet instant stor.validation contient deux éléments :
      // stor.validation.aRepondu qui me dit si toutes les zones ont bien été complétées ou non
      // stor.validation.bonneReponse qui me dit si les zones qui ne sont pas dans le tableau valide_pero ont des réponses correctes ou non.
      // Si stor.validation.aRepondu vaut true, la fonction met aussi en bleu ou rouge les réponses bonnes ou fausses.
      // Si c’est la dernière tentative de répondre, on barre également les zones fausses

      // Réponse vide ou incomplète et la limite de temps n’est pas atteinte
      if ((!stor.validation.aRepondu) && (!this.isElapsed)) {
        this.reponseManquante(stor.divCorrection)
        afficheChances() //  affiche le nombre de chances qui restent
        return this.finCorrection()
      }

      // Une réponse a été saisie

      // Bonne réponse
      if (stor.validation.bonneReponse) {
        this.score++
        stor.divCorrection.style.color = this.styles.cbien
        stor.divCorrection.innerHTML = cBien

        // Même si la réponse est bonne on peut afficher des éléments de correction/explication :
        afficheCorrection(true)

        // Augmente le niveau de difficulté
        // On élargit les questions posés vers le haut et vers le bas si possible
        // Si on atteint la plage maximale, on rajoute la difficulté
        const arrondiPrecedent = stor.arrondiEnCours.length
        if ((stor.arrondiEnCours.length === 2) && (me.donneesSection.s_arrondi === 'Les deux')) stor.arrondiEnCours.push(3)
        if ((stor.arrondiEnCours.length === 1) && (me.donneesSection.s_arrondi !== 'Arrondi')) stor.arrondiEnCours.push(2)
        for (let i = arrondiPrecedent + 1; i <= stor.arrondiEnCours.length; i++) {
          for (let j = stor.t_rang_en_cours[1]; j <= stor.t_rang_en_cours[0]; j++) {
            stor.questions.push([j, i, 0])
          }
        }

        if ((arrondiPrecedent === stor.arrondiEnCours.length) && (stor.t_rang_en_cours[0] >= me.donneesSection.t_rangs_adapte[0]) && (stor.t_rang_en_cours[1] <= me.donneesSection.t_rangs_adapte[1]) && (stor.difficulte_en_cours < 2) && (me.donneesSection.n_niveau >= 2)) {
          stor.difficulte_en_cours++
          for (let i = stor.t_rang_en_cours[1]; i <= stor.t_rang_en_cours[0]; i++) {
            for (let j = 0; j <= stor.arrondiEnCours.length - 1; j++) { stor.questions.push([i, stor.arrondiEnCours[j], stor.difficulte_en_cours]) }
          }
        }
        if (stor.t_rang_en_cours[0] < me.donneesSection.t_rangs_adapte[0]) {
          stor.t_rang_en_cours[0]++
          for (let j = 0; j <= stor.arrondiEnCours.length - 1; j++) { stor.questions.push([stor.t_rang_en_cours[0], stor.arrondiEnCours[j], 0]) }
        }
        if (stor.t_rang_en_cours[1] > me.donneesSection.t_rangs_adapte[1]) {
          stor.t_rang_en_cours[1]--
          for (let j = 0; j <= stor.arrondiEnCours.length - 1; j++) { stor.questions.push([stor.t_rang_en_cours[1], stor.arrondiEnCours[j], 0]) }
        }
        this.typederreurs[0]++ //  Compte les bonnes réponses
        return this.finCorrection('navigation', true)
      }

      // Mauvaise réponse
      stor.erreurs.push([stor.question, stor.arrondi, stor.difficulte]) // Mémorise l’erreur
      if (this.donneesSection.nbchances === 1) stor.erreurs.push([stor.question, stor.arrondi, stor.difficulte]) // Il faudra réussir 2 fois pour confirmer son erreur

      stor.divCorrection.style.color = this.styles.cfaux // Commentaires à droite en rouge

      // Fini à cause de la limite de temps
      if (this.isElapsed) {
        stor.divCorrection.innerHTML = tempsDepasse // Commentaires à droite

        j3pDesactive(prefix + 'consigne1input1') // Désactiver toutes les zones de saisie
        j3pBarre(prefix + 'consigne1input1')
        this.typederreurs[10]++ //  Compte les temps dépassés
        // L’élève avait un temps limite mais ne l’a pas respecté. On affiche alors la correction
        afficheCorrection(true)
        return this.finCorrection('navigation', true)
      }

      // Réponse fausse sans limite de temps
      stor.divCorrection.innerHTML = cFaux // Commentaires à droite en rouge

      if (this.essaiCourant < this.donneesSection.nbchances) {
        // Il reste des essais
        stor.divCorrection.innerHTML += '<br>' + essaieEncore // Commentaires à droite
        this.typederreurs[1]++ //  Compte les essais ratés
        afficheChances() //  affiche le nombre de chances qui restent
        afficheCorrection(false)
        return this.finCorrection() // on reste en état correction
        // Il reste des essais
      }

      // Il ne reste plus d’essais
      stor.divCorrection.innerHTML += '<br>' + regardeCorrection // Commentaires à droite
      // Là il ne peut plus se corriger. On lui affiche alors la solution
      afficheCorrection(true)
      this.typederreurs[2]++ //  Compte les réponses fausses
      this.finCorrection('navigation', true)
      break
    }

    /// ////////////////////////////////////////////////////////////////////////////////////////
    //
    //            Phase correction
    //
    /// ////////////////////////////////////////////////////////////////////////////////////////

    case 'navigation': {
      //  Si on a posé toutes les questions, on rajoute les erreurs et on continue
      if (stor.questions.length === 0) {
        stor.questions = stor.questions.concat(stor.erreurs)
        stor.erreurs.length = 0 // Et on vide le tableau des erreurs
      }
      //    Arrête les questions si toutes les réponses sont justes
      if (stor.questions.length === 0) me.donneesSection.nbitems = me.questionCourante

      if (this.sectionTerminee()) {
        // On détermine la phrase d’état renvoyée par la section
        if ((this.typederreurs[1] === 0) && (this.typederreurs[2] === 0) && (this.typederreurs[10] === 0)) {
          //  Aucune erreur: Notion comprise
          if (this.typederreurs[6] > 1) this.parcours.pe = phrasesEtat.pe_3 //  Notion comprise, mais la séparation des classes n’est pas acquise.
          else if (this.typederreurs[7] > 1) this.parcours.pe = phrasesEtat.pe_4 //  Notion comprise, mais il reste des zéros inutiles
          else if (this.donneesSection.limite !== 0) this.parcours.pe = phrasesEtat.pe_1 //  Aucune erreur en temps limité: Notion totalement maitrisée
          else this.parcours.pe = phrasesEtat.pe_2 //  Aucune erreur sans temps limité: Notion comprise
        } else if ((this.typederreurs[2] === 0) && (this.typederreurs[10] === 0)) {
          //  A pu corriger ses erreurs: Notion fragile
          this.parcours.pe = phrasesEtat.pe_5
        } else if (this.typederreurs[3] > 1) {
          //  Notion de dixième non comprise
          this.parcours.pe = phrasesEtat.pe_6
        } else if (this.typederreurs[4] > 1) {
          //  Notion de dixièmes consécutifs non comprise
          this.parcours.pe = phrasesEtat.pe_7
        } else if (this.typederreurs[5] > 1) {
          //  Erreur dans l’ordre des nombres
          this.parcours.pe = phrasesEtat.pe_8
        } else {
          //  Notion fragile (au cas où)
          this.parcours.pe = phrasesEtat.pe_5
        }
      } else {
        this.etat = 'enonce'
      }
      this.finNavigation()
      break
    }
  }
} //  SectionPosition
