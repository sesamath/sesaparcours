import $ from 'jquery'
import { j3pActive, j3pAddContent, j3pAddElt, j3pBarre, j3pDiv, j3pElement, j3pEmpty, j3pFocus, j3pGetRandomInt, j3pNombreBienEcrit, j3pRestriction, j3pValeurde } from 'src/legacy/core/functions'
import ValidationZones from 'src/legacy/outils/calculatrice/ValidationZones'
import { j3pAffiche, j3pDesactive } from 'src/lib/mathquill/functions'
import TableauConversion from 'src/legacy/outils/tableauconversion/TableauConversion'
import { nombreEnMots } from 'src/lib/outils/conversion/nombreEnMots'
import { j3pCreeFenetres, j3pToggleFenetres, j3pCreeBoutonFenetre } from 'src/legacy/core/functionsJqDialog'
import { j3pCreeEllipse, j3pCreeSVG } from 'src/legacy/core/functionsSvg'
import textesGeneriques from 'src/lib/core/textes'

const { cBien, cFaux, essaieEncore, regardeCorrection, tempsDepasse } = textesGeneriques

/**
 * @module
 * @type {LegacySection}
 */

/**
 * Donner la position d’un chiffre dans un nombre décimal
 * Fait à partir de l’exercice flash cm2exN1_16
 * @author Xavier JEROME
 * @since 22/05/2018
 * @fileOverview
 */

const niveauParDefaut = 'Jusqu’aux unités de mille'
const listeNiveaux = [
  niveauParDefaut,
  'Jusqu’aux centaines de mille',
  'Jusqu’aux centaines de millions',
  'Jusqu’aux centaines de milliards'
]
const aideParDefaut = 'Donne la correction au final'
const listeAide = [
  'Aucune aide',
  'Suggère de lire le nombre',
  'Affiche le nombre en lettres',
  'Présente un tableau de conversion',
  'Permet de remplir un tableau de conversion',
  'Présente un tableau de conversion rempli',
  aideParDefaut
]

/** @type {LegacySectionParams} */
export const params = {
  outils: ['mathquill'],

  /**
   * Ce qui est paramétrable dans l’éditeur de graphe
   * @type {LegacySectionParamsParametres}
   */
  parametres: [
    ['titre', 'Quel est le chiffre des ...?', 'string', 'Titre de l’exercice.'],
    // Texte que l’élève peut faire apparaître en bas de l’écran
    ['indication', 'Lis le nombre à haute voix et vérifie que ta réponse est cohérente avec ce que tu dis.', 'string', 'Indication en bas à gauche.'],
    // Nombre d’essais autorisés avant de déclarer un échec et de passer à la question suivante
    ['nbchances', 2, 'entier', 'Nombre d’essais par question'],
    // S’il n’est pas vide, le temps de réponse sera limité
    ['limite', 0, 'entier', 'Temps disponible par répétition, en secondes'],
    // Niveau de difficulté
    ['l_niveau', niveauParDefaut, 'liste', 'Les chiffres qui pourront être demandés.', listeNiveaux],
    // Nombre de décimales
    ['l_decimaux', 'Pas de décimaux', 'liste', 'Les chiffres décimaux qui pourront être demandés.', ['Pas de décimaux', 'Jusqu’aux dixièmes', 'Jusqu’aux centièmes', 'Jusqu’aux millièmes']],
    // Niveau d’aide autorisé
    ['l_aide', aideParDefaut, 'liste', 'Une aide de plus en plus proche du résultat est proposée.\nOn peut indiquer le niveau où s’arrêtera l’aide.', listeAide]
  ],

  pe: [
    { pe_1: 'Notion totalement maitrisée.' },
    { pe_2: 'Notion comprise.' },
    { pe_3: 'Notion fragile.' },
    { pe_4: 'Notion non comprise malgré le nombre écrit en lettres.' },
    { pe_5: 'Notion non comprise malgré le tableau de conversion vide.' },
    { pe_6: 'Notion non comprise malgré le tableau de conversion complet.' },
    { pe_7: 'Notion non comprise.' },
    { pe_8: 'Confusion entre dizaine et dixième.' }
  ]
}

/** Le nb max de questions que l'on pourra poser (ça augmente au fil des erreurs jusqu'à ce max) */
const maxRepetitions = 15

const textes = {
  nom_positions: ['millièmes', 'centièmes', 'dixièmes', 'unités', 'dizaines', 'centaines', 'milliers', 'dizaines de milliers', 'centaines de milliers', 'unités de millions', 'dizaines de millions', 'centaines de millions', 'unités de milliards', 'dizaines de milliards', 'centaines de milliards'],
  phraseNbChances: 'Il te reste £e essai£p.', //  Ne pas supprimer
  phraseEnonce: 'Dans le nombre £n le chiffre des £r est @1@.',
  phraseSolution1: 'Dans le nombre £n le chiffre des £r est £c.',
  phraseSolution2: 'Tu as confondu £r et £s.\n£r ressemble à une douz<b>aine</b> d’oeufs.\n£s ressemble à un cinqu<b>ième</b> de gâteau.',
  phraseSolution3: 'Tu as confondu £r et £s.\n£s ressemble à un cinqu<b>ième</b> de gâteau.',
  Aide00: 'J’ai compris mon erreur.',
  Aide01: 'Je ne comprends pas mon erreur.',
  Aide02: 'Je ne comprends pas comment cela m’aide.',
  Aide03: 'Je ne sais pas lire ce nombre.',
  Aide04: 'Regarde ce tableau.',
  Aide05: 'Je ne sais pas le remplir.',
  Aide06: 'Ecris le nombre dans le tableau.\nLa réponse est dans la colonne demandée.',
  Aide07: 'Ecris un chiffre dans chaque colonne\nen commençant par la droite.',
  Aide08: 'Lis ce nombre à haute voix et\nvérifie que ce que tu dis est cohérent avec ta réponse.',
  Aide09: 'J’attends la correction.',
  Aide10: 'Ecris un chiffre dans chaque colonne\nen plaçant la virgule au bon endroit.'
}

export function upgradeParametres (parametres) {
  // les ' sont devenus ’ dans les choix proposés
  if (parametres.l_niveau) parametres.l_niveau = parametres.l_niveau.replace(/'/g, '’')
  if (parametres.l_decimaux) parametres.l_decimaux = parametres.l_decimaux.replace(/'/g, '’')
}

/**
 * section Position
 * @this {Parcours}
 */
export default function main () {
  const me = this
  const stor = this.storage
  const prefix = 'Position'
  const ds = this.donneesSection

  /// ////////////////////////////////////////////////////////////////////////////////////////
  //
  //               Ajouter ici toutes les constantes nécessaires à la section
  //
  /// ////////////////////////////////////////////////////////////////////////////////////////

  const chiffresInit = ['1', '2', '3', '4', '5', '6', '7', '8', '9', '1', '2', '3', '4', '5', '6', '7', '8', '9'] // Pour minimiser les doublons dans le nombre

  /// ////////////////////////////////////////////////////////////////////////////////////////
  //
  //              Ajouter ici toutes les fonctions nécessaires à la section
  //
  /// ////////////////////////////////////////////////////////////////////////////////////////

  //   Affiche la correction sous l’énoncé
  function afficheCorrection (fin) {
    // fonction appelée pour afficher la correction (que la réponse soit bonne ou fausse
    const reponseEleve = parseFloat(j3pValeurde(prefix + 'consigne1input1').replace(/ /g, '').replace(',', '.')) //  Réponse utilisable dans les calculs

    j3pElement(prefix + 'explication1').innerHTML = '' //  Pour effacer  un éventuel précédent affichage
    j3pElement(prefix + 'explication2').innerHTML = '' //  Pour effacer  un éventuel précédent affichage
    j3pElement(prefix + 'explication3').innerHTML = '' //  Pour effacer  un éventuel précédent affichage

    // Si la réponse est juste, il n’y a rien à apporter de plus
    if (!fin || !stor.reponse.bonneReponse) {
      j3pAffiche(prefix + 'explication1', prefix + 'corr1', textes.phraseSolution1,
        {
          style: me.styles.moyen.correction,
          n: j3pNombreBienEcrit(stor.nombre), //  Nombre formaté avec les espaces
          r: textes.nom_positions[stor.question - 1], //  Chaine correspondant au rang demandé
          c: stor.laBonneReponse
        })

      // Regarde s’il y a confusion entre dixième et dizaine, etc
      let confusion = 0
      if (
        ((stor.question === 5) && (stor.tableauNombre[13] === reponseEleve)) ||
        (((stor.question === 3) && (stor.tableauNombre[10] === reponseEleve)))
      ) {
        confusion = 5
      } else if (
        ((stor.question === 6) && (stor.tableauNombre[14] === reponseEleve)) ||
        (((stor.question === 2) && (stor.tableauNombre[9] === reponseEleve)))
      ) {
        confusion = 6
      } else if (
        ((stor.question === 7) && (stor.tableauNombre[15] === reponseEleve)) ||
        ((stor.question === 1) && (stor.tableauNombre[8] === reponseEleve))
      ) {
        confusion = 7
      }

      if (confusion === 7) {
        j3pAffiche(prefix + 'explication2', prefix + 'corr2', textes.phraseSolution3, {
          style: me.styles.moyen.correction,
          //  Nombre formaté avec les espaces
          r: textes.nom_positions[confusion - 1].substring(0, textes.nom_positions[confusion - 1].length - 1),
          //  Chaine correspondant au rang demandé
          s: textes.nom_positions[8 - confusion - 1].substring(0, textes.nom_positions[8 - confusion - 1].length - 1)
        })
      } else if (confusion !== 0) {
        // confusion 5 ou 6
        j3pAffiche(prefix + 'explication2', prefix + 'corr2', textes.phraseSolution2, {
          style: me.styles.moyen.correction,
          //  Nombre formaté avec les espaces
          r: textes.nom_positions[confusion - 1].substring(0, textes.nom_positions[confusion - 1].length - 1),
          //  Chaine correspondant au rang demandé
          s: textes.nom_positions[8 - confusion - 1].substring(0, textes.nom_positions[8 - confusion - 1].length - 1)
        })
      }

      afficheTableau(3)
      // Entoure la bonne réponse dans le tableau
      const svg = j3pCreeSVG(prefix + 'explication3', {
        id: 'enonce_cercle_svg',
        width: 800,
        height: 150,
        style: {
          position: 'absolute',
          top: 0,
          left: 0
        }
      })
      j3pCreeEllipse(svg, {
        id: 'uneellipse',
        cx: 25 + 50 * ((15 - stor.question) - (3 - Math.floor((stor.rang - 1) / 3)) * 3),
        cy: 90,
        rayon_x: 25,
        rayon_y: 50,
        couleur: '#000000',
        couleurRemplissage: '#ff0000',
        epaisseur: 3,
        opaciteRemplissage: 0.3
      })

      if (confusion !== 0) {
        me.typederreurs[6]++
      } //  Compte les confusions dizaine/dixième
    } // Réponse fausse
  }

  //   Affiche un tableau des positions des chiffres
  function afficheTableau (type) {
    const conteneur = j3pElement(prefix + 'explication3')
    // on vide
    j3pEmpty(conteneur)
    switch (type) {
      case 0:
        // rien à faire
        break

      case 1: // Un tableau vide non modifiable
        // On crée un div dans explications
        // eslint-disable-next-line no-new
        new TableauConversion(conteneur, {

          idDIV: 'tableau',
          format: {
            relative: true,
            virgule: [(stor.rang_decimaux !== 0)], // On ne met la virgule que si nécessaire
            epaisseurs: {},
            couleurs: {}
          },
          lignes: [[(stor.rang > 9 ? ['.', '.', '.'] : []), // milliards si besoin
            (stor.rang > 6 ? ['.', '.', '.'] : []),
            (stor.rang > 3 ? ['.', '.', '.'] : []),
            ['.', '.', '.'], // Unités
            (stor.rang_decimaux === 0 ? [] : ['.', '.', '.']), // Décimaux si besoin
            []]] // Jamais de millionièmes
        })
        break

      case 2: // Un tableau vide modifiable
        // eslint-disable-next-line no-new
        new TableauConversion(conteneur, {

          idDIV: 'tableau',
          format: {
            relative: true,
            virgule: [(stor.rang_decimaux !== 0)], // On ne met la virgule que si nécessaire
            epaisseurs: {},
            couleurs: {}
          },
          lignes: [[(stor.rang > 9 ? ['?_', '?_', '?_'] : []), // milliards si besoin
            (stor.rang > 6 ? ['?_', '?_', '?_'] : []),
            (stor.rang > 3 ? ['?_', '?_', '?_'] : []),
            ['?_', '?_', '?_'], // Unités
            (stor.rang_decimaux === 0 ? [] : ['?_', '?_', '?_']), // Décimaux si besoin
            []]]
        })
        break

      case 3: // Un tableau avec le nombre demandé
        // eslint-disable-next-line no-new
        new TableauConversion(conteneur, {

          idDIV: 'tableau',
          format: {
            relative: true,
            virgule: [(stor.rang_decimaux !== 0)], // On ne met la virgule que si nécessaire
            epaisseurs: {},
            couleurs: {}
          },
          lignes: [[(stor.rang > 9 ? [(stor.tableauNombre[0] === 0 ? '.' : stor.tableauNombre[0]), (stor.tableauNombre[1] === 0 ? '.' : stor.tableauNombre[1]), stor.tableauNombre[2]] : []), // milliards si besoin
            (stor.rang > 6 ? [(stor.tableauNombre[3] === 0 ? '.' : stor.tableauNombre[3]), (stor.tableauNombre[4] === 0 ? '.' : stor.tableauNombre[4]), stor.tableauNombre[5]] : []),
            (stor.rang > 3 ? [(stor.tableauNombre[6] === 0 ? '.' : stor.tableauNombre[6]), (stor.tableauNombre[7] === 0 ? '.' : stor.tableauNombre[7]), stor.tableauNombre[8]] : []),
            [(stor.tableauNombre[9] === 0 ? '.' : stor.tableauNombre[9]), (stor.tableauNombre[10] === 0 ? '.' : stor.tableauNombre[10]), stor.tableauNombre[11]], // Unités
            (stor.rang_decimaux === 0 ? [] : [stor.tableauNombre[13], (stor.tableauNombre[14] === 0 ? '.' : stor.tableauNombre[14]), (stor.tableauNombre[15] === 0 ? '.' : stor.tableauNombre[15])]), // Décimaux si besoin
            []]]
        })
        break

      default: console.error(Error('type de tableau non géré : ' + type))
    }
  }

  //   Cache tous les écreans d’aide
  function cacheAide () {
    //  Obligé de tous les faire pour le cas où la limite de temps intervient pendant l’aide
    ['Aide0', 'Aide1', 'Aide2', 'Aide3', 'Aide4', 'Aide5', 'Aide6', 'Aide7'].forEach(function (item) {
      $('div[name="' + item + '"]').dialog('close')
    })
    afficheTableau(0)
    me.afficheBoutonValider()
    j3pActive(prefix + 'consigne1input1')
    j3pFocus(prefix + 'consigne1input1', true)
    // Indique si les écrans d’aide sont affichés et interdit de répondre
    stor.hasAide = false
  }

  // Affiche le nombre de tentatives qu’il reste s’il y a plusieurs chances
  function afficheChances () {
    j3pElement(prefix + 'zone_chances').innerHTML = ''
    if (ds.nbchances > 1) {
      j3pAffiche(prefix + 'zone_chances', prefix + 'chances', textes.phraseNbChances,
        {
          style: me.styles.moyen.enonce,
          e: ds.nbchances - me.essaiCourant,
          p: (ds.nbchances - me.essaiCourant > 1 ? 's' : '')
        })
    } // Pour le pluriel
  }

  /// ////////////////////////////////////////////////////////////////////////////////////////
  //
  //              Déroulement de la section Enonce, Correction, navigation
  //
  /// ////////////////////////////////////////////////////////////////////////////////////////

  switch (this.etat) {
    /// ////////////////////////////////////////////////////////////////////////////////////////
    //
    //            Phase énoncé
    //
    /// ////////////////////////////////////////////////////////////////////////////////////////

    case 'enonce': {
      //  Construit l’énoncé: exécuté au début de chaque question
      if (this.debutDeLaSection) {
        //  Code exécuté uniquement au lancement de la section
        // Construction de la page
        this.construitStructurePage('presentation1')
        this.afficheTitre(ds.titre)

        // Affiche l’indication en bas à gauche
        if (ds.indication !== '') this.indication(this.zones.IG, ds.indication)
        // FIN Partie à ne pas changer

        /*
         Par convention,`
        `   this.typederreurs[0] = nombre de bonnes réponses
            this.typederreurs[1] = nombre de fois où l’élève a utilisé une chance supplémentaire
            this.typederreurs[2] = nombre de mauvaises réponses
            this.typederreurs[3] = nombre de fois où il a utilisé la lecture du nombre
            this.typederreurs[4] = nombre de fois où il a utilisé le tableau vide
            this.typederreurs[5] = nombre de fois où il a utilisé le tableau rempli
            this.typederreurs[6] = nombre de fois où il a confondu dizaine et dixième
            this.typederreurs[10] = nombre de fois où l’élève a eu faux à cause d’un temps limite
            LES AUTRES peuvent être utilisées par la détermination de la pe dans le cas d’une section qualitative
         */
        this.typederreurs = [0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0]

        //  Paramètres et variables particuliers à la section
        ds.n_decimaux = ['Pas de décimaux', 'Jusqu’aux dixièmes', 'Jusqu’aux centièmes', 'Jusqu’aux millièmes'].indexOf(ds.l_decimaux)
        // index du niveau
        ds.n_niveau = listeNiveaux.indexOf(ds.l_niveau) + 1
        ds.n_aide = listeAide.indexOf(ds.l_aide)

        // rang est la position maximum de la question
        stor.rang = (ds.n_niveau === 1 ? 2 : 4) //  Au niveau 1 on commence aux dizaines, sinon aux milliers

        // rang_decimaux est la position maximum de la question
        // Si les décimaux sont autorisés, on commence aux dixièmes
        stor.rang_decimaux = (ds.n_decimaux === 0 ? 0 : 1)

        // Tableaux permettant de ne pas poser 2 fois la même question
        stor.questions = [] // Contient les question qui n’ont pas encore été posées
        for (let i = 3 + 1 - stor.rang_decimaux; i <= 3 + stor.rang; i++) {
          stor.questions.push(i)
        }
        const nbrepetitions = stor.questions.length // C’est cette valeur qui est affichée dans l’entête
        this.surcharge({ nbrepetitions })

        stor.erreurs = [] // Contient les erreurs
        stor.repeter = false // Bascule permettant de prendre alternativement des questions nouvelles et des anciennes erreurs
        // FIN Paramètres et variables particuliers à la section
      } else {
        this.videLesZones()
      }

      //  Partie exécutée à chaque essai

      /// ////////////////////////////////////////////////////////////////////////////////////////
      //
      //               Création de l’énoncé
      //
      /// ////////////////////////////////////////////////////////////////////////////////////////

      //  Création de l’énoncé
      //    Conventions de la manipulation des nombres affichés
      /**************************************************************************************
       *
       *                       999 999 999 999,999
       *     question         15                 1
       *     n_niveau  1                 - ---
       *               2               --- ---
       *               3           --- --- ---
       *               4       --- --- --- ---
       *     n_decimaux                        123
       *     rang             12             1
       *     n_decimaux                        123
       *
       ***************************************************************************************/
      let n
      // codeQuestion contient les informations permettant de construire la question
      if (stor.repeter && stor.erreurs.length !== 0) {
        // On choisit la question une fois sur deux dans les erreurs passées
        n = j3pGetRandomInt(0, stor.erreurs.length - 1) // La question est choisie parmi les erreurs passées
        stor.codeQuestion = stor.erreurs[n]
        stor.erreurs.splice(n, 1) // On la retire des questions à poser
        stor.repeter = false // La prochaine fois on prendra une nouvelle question
      } else {
        n = j3pGetRandomInt(0, stor.questions.length - 1) // La question est choisie parmi celles qui n’ont pas encore été posées
        stor.codeQuestion = stor.questions[n]
        stor.questions.splice(n, 1) // On la retire des questions à poser
        stor.repeter = true
      }
      stor.question = stor.codeQuestion // question est le rang demandé

      // réponse est le chiffre correspondant au rang question
      stor.laBonneReponse = j3pGetRandomInt(1, 9) // Chiffre qui correspond à la réponse

      // Copie de toute les paires de chiffres non nuls
      // On retire le chiffre qui sert de réponse des chiffres disponibles pour que la réponse soit unique
      const chiffres = chiffresInit.filter(nb => nb !== String(stor.laBonneReponse))

      //  Le nombre sous forme de tableau de chiffres avec les millièmes à gauche et sans virgule
      stor.tableauNombre = (new Array(15)).fill('0') // Crée un nombre avec que des zéros

      for (let i = 3 + 1 - stor.rang_decimaux; i <= 3 + stor.rang; i++) {
        // On rajoute les chiffres nécessaires un à un
        n = j3pGetRandomInt(0, Math.min(chiffres.length - 1, stor.rang_decimaux + stor.rang)) //  Minimise les doublons
        stor.tableauNombre[i - 1] = chiffres[n]
        chiffres.splice(n, 1)
      } // On supprime le chiffre utilisé
      stor.tableauNombre[stor.question - 1] = stor.laBonneReponse // On remplace le chiffre qui correspond à la réponse
      stor.tableauNombre.splice(3, 0, '.') // On rajoute la virgule
      stor.tableauNombre = stor.tableauNombre.reverse() // On remet le nombre à l’endroit

      // nombre est le nombre proposé
      stor.nombre = parseFloat(stor.tableauNombre.join('')) // On le transforme en nombre réel
      // Création de l’énoncé

      //  Création des conteneurs
      // Création du conteneur dans lequel se trouveront toutes les consignes
      j3pDiv(this.zones.MG, {
        id: prefix + 'consignes',
        style: me.styles.etendre('moyen.enonce', { padding: '10px', marginTop: '25px' })
      })

      // Dans ce conteneur, on crée un div par phrase (cela forcera le retour à la ligne)
      j3pDiv(prefix + 'consignes', prefix + 'zone_consigne1', '')

      // Affiche le nombre de chances qu’il reste
      j3pDiv(prefix + 'consignes', {
        id: prefix + 'zone_chances',
        style: me.styles.etendre('moyen.enonce', { marginTop: '30px' })
      })
      afficheChances()

      // Si on veut rajouter des explications, en cas de réponse fausse par exemple
      const divExplications = j3pDiv(this.zones.MG, {
        id: prefix + 'explications',
        style: me.styles.etendre('moyen.explications', { padding: '10px', marginTop: '15px' })
      })
      j3pDiv(divExplications, prefix + 'explication1', '') //  Contient la correction
      j3pDiv(divExplications, {
        id: prefix + 'explication2',
        style: {
          marginTop: '20px'
        }
      })
      j3pDiv(divExplications, {
        id: prefix + 'explication3',
        style: {
          position: 'relative',
          marginTop: '20px'
        }
      })

      // On crée ici le div qui contiendra toutes les remarques sur ce qu’on pense de la réponse de l’élève (le plus souvant dans le cadre MD)
      stor.correction = j3pAddElt(this.zonesElts.MD, 'div', '', { style: me.styles.etendre('petit.correction', { padding: '10px' }) })
      // FIN Création des conteneurs

      //  Initialisation des zones de saisie
      j3pAffiche(prefix + 'zone_consigne1', prefix + 'consigne1', textes.phraseEnonce, {
        style: me.styles.moyen.enonce,
        n: j3pNombreBienEcrit(stor.nombre), //  Nombre formaté avec les espaces
        r: textes.nom_positions[stor.question - 1], //  Chaine correspondant au rang demandé
        input1: {
          dynamique: true,
          width: '50px',
          correction: stor.laBonneReponse
        }
      })

      // Le nom de chaque zone de saisie est construit de la même façon pour les zones input et inputmq :
      // le nom input est toujours précédé du nom du span qui contient la zone (ce span est le deuxième paramètre de la fonction j3pAffiche

      // On rajoute les contraintes de saisie
      j3pRestriction(prefix + 'consigne1input1', '0-9') // on n’autorise que les chiffres

      //  Limite la zone de saisie
      j3pElement(prefix + 'consigne1input1').maxLength = 1

      // On rajoute toutes les informations qui permettront de valider la saisie
      j3pElement(prefix + 'consigne1input1').typeReponse = ['nombre', 'exact']
      j3pElement(prefix + 'consigne1input1').reponse = stor.laBonneReponse

      // Paramétrage de la validation
      // zonesSaisie est un tableau contenant le nom de chacune des zones (on pourrait aussi y mettre une liste déroulante)
      const zonesSaisie = [prefix + 'consigne1input1']
      // validePerso est souvant un tableau vide
      // Mais parfois certaines zones ne peuvent pas être validée de manière automatique
      // Donc la validation de ces zones devra se faire "à la main".
      // Par contre on vérifiera si elles sont remplies ou non.
      const zonesValidePerso = []

      // fctsValid est un objet permettant la validation. Il contient le nom des zones (tableau précédent) et un deuxième paramètre validePerso
      stor.fctsValid = new ValidationZones({ parcours: me, zones: zonesSaisie, validePerso: zonesValidePerso })
      //  Initialisation des zones de saisie

      //  Prépare les écrans d’aide
      this.fenetresjq = [
        { name: 'Aide0', title: 'Aides', width: 400, height: 50 + 2 * 50, left: '90%', top: '50%', id: 'fenetreAide0' }, // Propose de l’aide
        { name: 'Aide1', title: 'Aides', width: 400, height: 50 + 70 + 3 * 50, left: '90%', top: '50%', id: 'fenetreAide1' }, // Demande de lire le nombre
        { name: 'Aide2', title: 'Aides', width: 400, height: 50 + 80 + 2 * 50, left: '90%', top: '50%', id: 'fenetreAide2' }, // Ecris le nombre en lettres
        { name: 'Aide3', title: 'Aides', width: 400, height: 50 + 2 * 50, left: '90%', top: '50%', id: 'fenetreAide3' }, // Propose un tableau des positions
        { name: 'Aide4', title: 'Aides', width: 400, height: 50 + 2 * 50, left: '90%', top: '50%', id: 'fenetreAide4' }, // Affiche le tableau
        { name: 'Aide5', title: 'Aides', width: 400, height: 50 + 60 + 2 * 50, left: '90%', top: '50%', id: 'fenetreAide5' }, // Explique comment remplir le tableau
        { name: 'Aide6', title: 'Aides', width: 400, height: 50 + 60 + 2 * 50, left: '90%', top: '50%', id: 'fenetreAide6' }, // Tableau à remplir
        { name: 'Aide7', title: 'Aides', width: 400, height: 50 + 2 * 50, left: '90%', top: '50%', id: 'fenetreAide7' }] // Tableau rempli
      j3pCreeFenetres(this)

      // Aide 0
      j3pCreeBoutonFenetre('Bouton01', 'fenetreAide0', {}, textes.Aide01) // Je ne comprends pas mon erreur
      j3pElement('BTN_fenetreBouton01').onclick = function onclick () { j3pToggleFenetres('Aide0'); j3pToggleFenetres('Aide1') } //  Cache l’écran actuel et affiche l’écran suivant
      j3pCreeBoutonFenetre('Bouton02', 'fenetreAide0', {}, textes.Aide00) // J’ai compris mon erreur
      j3pElement('BTN_fenetreBouton02').onclick = cacheAide // Sort de l’aide

      // Aide 1
      j3pDiv('fenetreAide1', { id: 'idaide1', contenu: textes.Aide08, style: this.styles.etendre('toutpetit.enonce') })
      if (ds.n_aide > 2) {
        j3pCreeBoutonFenetre('Bouton11', 'fenetreAide1', {}, textes.Aide02) // Je ne comprends pas comment cela m’aide.
        j3pElement('BTN_fenetreBouton11').onclick = function onclick () { j3pToggleFenetres('Aide1'); j3pToggleFenetres('Aide3') }
      }
      if (ds.n_aide > 1) { // Si l’aide n’est pas limitée à un écran
        j3pCreeBoutonFenetre('Bouton12', 'fenetreAide1', {}, textes.Aide03) // Je ne sais pas lire ce nombre
        j3pElement('BTN_fenetreBouton12').onclick = function onclick () { j3pToggleFenetres('Aide1'); j3pToggleFenetres('Aide2'); me.typederreurs[3]++ } // Comptabilise l’utilisation de cette aide
      } else {
        j3pCreeBoutonFenetre('Bouton14', 'fenetreAide1', {}, textes.Aide09) // J’attends la correction.
        j3pElement('BTN_fenetreBouton14').onclick = cacheAide
      }
      j3pCreeBoutonFenetre('Bouton13', 'fenetreAide1', {}, textes.Aide00) // J’ai compris mon erreur
      j3pElement('BTN_fenetreBouton13').onclick = cacheAide

      // Aide 2
      j3pDiv('fenetreAide2', { id: 'idaide2', contenu: nombreEnMots(stor.nombre, { maxDecimales: stor.rang_decimaux }), style: this.styles.etendre('toutpetit.enonce') })
      if (ds.n_aide > 2) {
        j3pCreeBoutonFenetre('Bouton21', 'fenetreAide2', {}, textes.Aide02) // Je ne comprends pas comment cela m’aide.
        j3pElement('BTN_fenetreBouton21').onclick = function onclick () { j3pToggleFenetres('Aide2'); j3pToggleFenetres('Aide3') }
      } else {
        j3pCreeBoutonFenetre('Bouton23', 'fenetreAide2', {}, textes.Aide09) // J’attends la correction.
        j3pElement('BTN_fenetreBouton23').onclick = cacheAide
      }
      j3pCreeBoutonFenetre('Bouton22', 'fenetreAide2', {}, textes.Aide00) // J’ai compris mon erreur
      j3pElement('BTN_fenetreBouton22').onclick = cacheAide

      // Aide 3
      j3pCreeBoutonFenetre('Bouton31', 'fenetreAide3', {}, textes.Aide04) // Regarde ce tableau.
      j3pElement('BTN_fenetreBouton31').onclick = function onclick () { j3pToggleFenetres('Aide3'); afficheTableau(1); j3pToggleFenetres('Aide4'); me.typederreurs[4]++ } // Affiche un tableau vide
      j3pCreeBoutonFenetre('Bouton32', 'fenetreAide3', {}, textes.Aide00) // J’ai compris mon erreur
      j3pElement('BTN_fenetreBouton32').onclick = cacheAide

      // Aide 4
      if (ds.n_aide > 3) {
        j3pCreeBoutonFenetre('Bouton42', 'fenetreAide4', {}, textes.Aide02) // Je ne comprends pas comment cela m’aide.
        j3pElement('BTN_fenetreBouton42').onclick = function onclick () { j3pToggleFenetres('Aide4'); j3pToggleFenetres('Aide5') }
      } else {
        j3pCreeBoutonFenetre('Bouton42', 'fenetreAide4', {}, textes.Aide09) // J’attends la correction.
        j3pElement('BTN_fenetreBouton42').onclick = cacheAide
      }
      j3pCreeBoutonFenetre('Bouton43', 'fenetreAide4', {}, textes.Aide00) // J’ai compris mon erreur
      j3pElement('BTN_fenetreBouton43').onclick = cacheAide

      // Aide 5
      j3pDiv('fenetreAide5', { id: 'idaide5', contenu: textes.Aide06, style: this.styles.etendre('toutpetit.enonce') })
      j3pCreeBoutonFenetre('Bouton51', 'fenetreAide5', {}, textes.Aide05) // Je ne sais pas le remplir.
      j3pElement('BTN_fenetreBouton51').onclick = function onclick () { j3pToggleFenetres('Aide5'); afficheTableau(2); j3pToggleFenetres('Aide6') } // Affiche un tableau modifiable
      j3pCreeBoutonFenetre('Bouton53', 'fenetreAide5', {}, textes.Aide00) // J’ai compris mon erreur
      j3pElement('BTN_fenetreBouton53').onclick = cacheAide

      // Aide 6
      j3pDiv('fenetreAide6', {
        id: 'idaide6',
        //  Consigne différente s’il y a effectivement une virgule ou pas
        contenu: (stor.rang_decimaux === 0 ? textes.Aide07 : textes.Aide10),
        style: this.styles.etendre('toutpetit.enonce')
      })
      if (ds.n_aide > 4) {
        j3pCreeBoutonFenetre('Bouton61', 'fenetreAide6', {}, textes.Aide05) // Je ne sais pas le remplir.
        j3pElement('BTN_fenetreBouton61').onclick = function onclick () { j3pToggleFenetres('Aide6'); afficheTableau(3); j3pToggleFenetres('Aide7'); me.typederreurs[5]++ } //  Affiche un tableau déjà remplis
      } else {
        j3pCreeBoutonFenetre('Bouton61', 'fenetreAide6', {}, textes.Aide09) // J’attends la correction.
        j3pElement('BTN_fenetreBouton61').onclick = cacheAide
      }
      j3pCreeBoutonFenetre('Bouton62', 'fenetreAide6', {}, textes.Aide00) // J’ai compris mon erreur
      j3pElement('BTN_fenetreBouton62').onclick = cacheAide

      // Aide 7
      j3pCreeBoutonFenetre('Bouton71', 'fenetreAide7', {}, textes.Aide09) // J’attends la correction.
      j3pElement('BTN_fenetreBouton71').onclick = function onclick () {
        cacheAide()
        //  Affiche la réponse si c’est autorisé
        if (ds.n_aide > 5) afficheCorrection(false)
      }
      j3pCreeBoutonFenetre('Bouton72', 'fenetreAide7', {}, textes.Aide00) // J’ai compris mon erreur
      j3pElement('BTN_fenetreBouton72').onclick = cacheAide

      //  Interdit la fermeture des écrans d’aide avec escape
      ;['Aide0', 'Aide1', 'Aide2', 'Aide3', 'Aide4', 'Aide5', 'Aide6', 'Aide7'].forEach(function bloqueEsc (item) {
        $('div[name="' + item + '"]').dialog({ closeOnEscape: false })
      })
      //  Prépare les écrans d’aide

      //  Pour l’instant sans effet, mais sera là lorsque le focus sur la première question sera libéré
      j3pFocus(prefix + 'consigne1input1', true)

      this.finEnonce() // Obligatoire
      //  Partie exécutée à chaque essai
      break
    } // case "enonce":

    /// ////////////////////////////////////////////////////////////////////////////////////////
    //
    //            Phase correction
    //
    /// ////////////////////////////////////////////////////////////////////////////////////////

    case 'correction': {
      if (stor.hasAide && !this.isElapsed) {
        // On ne tient pas compte des réponses pendant la phase d’aide, sauf si la limite de temps est atteinte
        return this.finCorrection()
      }
      const fctsValid = stor.fctsValid // ici je crée juste une variable pour raccourcir le nom de l’object fctsValid

      //  Efface le nombre de chances qui restent
      j3pElement(prefix + 'zone_chances').innerHTML = ''

      //  Efface les éléments de correction
      j3pElement(prefix + 'explication1').innerHTML = ''
      j3pElement(prefix + 'explication2').innerHTML = ''
      j3pElement(prefix + 'explication3').innerHTML = ''

      // ce qui suit sert pour la validation de toutes les zones
      // le tableau contenant toutes les zones de saisie
      stor.reponse = fctsValid.validationGlobale() // Ne pas modifier
      // A cet instant reponse contient deux éléments :
      // reponse.aRepondu qui me dit si toutes les zones ont bien été complétées ou non
      // reponse.bonneReponse qui me dit si les zones qui ne sont pas dans le tableau valide_pero ont des réponses correctes ou non.
      // Si reponse.aRepondu vaut true, la fonction met aussi en bleu ou rouge les réponses bonnes ou fausses.
      // Si c’est la dernière tentative de répondre, on barre également les zones fausses

      // Réponse vide ou incomplète et la limite de temps n’est pas atteinte
      if ((!stor.reponse.aRepondu) && (!this.isElapsed)) {
        // Réponse vide
        this.reponseManquante(stor.correction)
        afficheChances() //  affiche le nombre de chances qui restent
        return this.finCorrection() // on reste en correction
      }

      // Une réponse a été saisie
      if (stor.reponse.bonneReponse) {
        //  Bonne réponse
        this.score++
        stor.correction.style.color = this.styles.cbien
        j3pAddContent(stor.correction, cBien, { replace: true })

        // Même si la réponse est bonne on peut afficher des éléments de correction/explication :
        afficheCorrection(true)

        // Si toutes les erreurs ont été corrigées
        // Augmente le niveau de difficulté
        // Pour le niveau 1 on passe de dizaines à milliers en augmentant de 1
        // Pour les autres niveaux, on commence par unités de milles, puis classe des milliers, millions, etc selon le niveau
        if (stor.erreurs.length === 0) {
          const rangPrecedent = stor.rang
          switch (ds.n_niveau) {
            case 1:
              stor.rang++
              stor.rang = Math.min(stor.rang, 4)
              break

            default:
              if (stor.rang === 4) stor.rang = 6
              else stor.rang += 3
              stor.rang = Math.min(stor.rang, ds.n_niveau * 3)
          }
          // On rajoute les nouvelles questions disponibles
          for (let i = rangPrecedent + 1 + 3; i <= 3 + stor.rang; i++) {
            if (ds.nbrepetitions < maxRepetitions) {
              stor.questions.push(i)
              this.ajouteRepetition()
            }
          }

          // Pour les décimaux on commence par dixièmes puis on incrémente de 1 à chaque passage jusqu’aux millièmes
          if (stor.rang_decimaux < ds.n_decimaux) {
            const rangPrecedentDec = stor.rang_decimaux
            stor.rang_decimaux++
            for (let i = 3 - rangPrecedentDec; i <= 3 - stor.rang_decimaux + 1; i++) {
              if (ds.nbrepetitions < maxRepetitions) {
                stor.questions.push(i)
                this.ajouteRepetition()
              }
            }
          }
        } // Augmenter la difficulté

        this.typederreurs[0]++ //  Compte les bonnes réponses
        //  Passe à la question suivante
        return this.finCorrection('navigation', true)
      }

      // Mauvaise réponse
      if (!stor.erreurs.includes(stor.codeQuestion) && ds.nbrepetitions < maxRepetitions) {
        stor.questions.push(stor.codeQuestion) // On reposera la question
        this.ajouteRepetition()
        stor.erreurs.push(stor.codeQuestion)
      } // Mémorise l’erreur
      stor.repeter = false // On ne repose pas la question au coup d’après

      // Commentaires à droite en rouge
      stor.correction.style.color = this.styles.cfaux

      if (this.isElapsed) {
        // Fini à cause de la limite de temps
        cacheAide() //  Cache tous les écrans d’aide
        j3pAddContent(stor.correction, tempsDepasse, { replace: true })
        j3pBarre(prefix + 'consigne1input1')
        this.typederreurs[10]++ //  Compte les temps dépassés

        // L’élève avait un temps limite mais ne l’a pas respecté. On affiche alors la correction
        afficheCorrection(true)

        //  Passe à la question suivante
        return this.finCorrection('navigation', true)
      }

      // Réponse fausse
      // Commentaires à droite en rouge
      j3pAddContent(stor.correction, cFaux, { replace: true })

      if (this.essaiCourant < ds.nbchances) {
        // Il reste des essais
        j3pAddContent(stor.correction, '<br>' + essaieEncore)
        this.typederreurs[1]++ //  Compte les essais ratés
        afficheChances() //  affiche le nombre de chances qui restent
        //  Affiche l’écran d’aide
        if (ds.n_aide > 0) {
          if (stor.rang_decimaux === 0) {
            j3pToggleFenetres('Aide0')
          } else if (ds.n_aide > 2) {
            j3pToggleFenetres('Aide3')
          }
          j3pDesactive(prefix + 'consigne1input1')
          stor.hasAide = true //  Interdit la saisie pendant les écrans d’aide
        }

        // Il reste des essais, on reste en correction
        return this.finCorrection()
      }

      // Il ne reste plus d’essais
      j3pAddContent(stor.correction, '<br>' + regardeCorrection) // Commentaires à droite
      // Là il ne peut plus se corriger. On lui affiche alors la solution
      afficheCorrection(true)

      this.typederreurs[2]++ //  Compte les réponses fausses

      this.finCorrection('navigation', true)
      break
    } // case "correction":

    /// ////////////////////////////////////////////////////////////////////////////////////////
    //
    //            Phase navigation
    //
    /// ////////////////////////////////////////////////////////////////////////////////////////

    case 'navigation': {
      //  Si on a posé toutes les questions, on continue avec les erreurs
      if (!stor.questions.length && ds.nbrepetitions < maxRepetitions) {
        stor.questions = [...stor.erreurs]
        this.ajouteRepetition(stor.questions.length)
      }

      if (this.sectionTerminee()) {
        // On détermine la phrase d’état renvoyée par la section
        if ((ds.limite !== 0) && (this.typederreurs[1] === 0) && (this.typederreurs[2] === 0) && (this.typederreurs[10] === 0)) {
          //  Aucune erreur en temps limité: Notion totalement maitrisée
          this.parcours.pe = ds.pe_1
        } else if ((this.typederreurs[1] === 0) && (this.typederreurs[2] === 0) && (this.typederreurs[10] === 0)) {
          //  Aucune erreur sans temps limité: Notion comprise
          this.parcours.pe = ds.pe_2
        } else if (
          ((this.typederreurs[2] === 0) && (this.typederreurs[10] === 0)) || //  A pu corriger ses erreurs
            ((ds.nbchances <= 1) && ((this.typederreurs[2] + this.typederreurs[10]) <= 1))
        ) {
          //  A pu corriger ses erreurs: Notion fragile
          this.parcours.pe = ds.pe_3
        } else if (this.typederreurs[6] > 1) {
          //  Confusion entre dizaine et dixième
          this.parcours.pe = ds.pe_8
        } else if (this.typederreurs[5] > 1) {
          //  Notion non comprise malgré le tableau de conversion complet
          this.parcours.pe = ds.pe_6
        } else if (this.typederreurs[4] > 1) {
          //  Notion non comprise malgré le tableau de conversion vide
          this.parcours.pe = ds.pe_5
        } else if (this.typederreurs[3] > 1) {
        //  Notion non comprise malgré le nombre écrit en lettres
          this.parcours.pe = ds.pe_4
        } else {
          //  Notion non comprise
          this.parcours.pe = ds.pe_7
        }
      } else {
        this.etat = 'enonce'
      }
      this.finNavigation()

      break
    } // case "navigation":
  } //  switch etat
} //  SectionPosition
