import { j3pBarre, j3pDiv, j3pElement, j3pFocus, j3pGetRandomInt, j3pNombreBienEcrit, j3pRemplacePoint, j3pRestriction, j3pShowError, j3pValeurde } from 'src/legacy/core/functions'
import ValidationZones from 'src/legacy/outils/calculatrice/ValidationZones'
import { j3pAffiche } from 'src/lib/mathquill/functions'
import textesGeneriques from 'src/lib/core/textes'

const { cBien, cFaux, essaieEncore, regardeCorrection, tempsDepasse } = textesGeneriques

/*
        Auteur  Xavier JEROME
        Date    28/07/2019
        Calcul mental de pourcentages
 */

export const params = {
  outils: ['mathquill'],
  /**
   * Ce qui est paramétrable dans l’éditeur de graphe
   * @type {LegacySectionParamsParametres}
   */
  parametres: [
    ['titre', 'Calcul mental de pourcentages', 'string', 'Titre de l’exercice.'], // Modifie le titre pour tenir compte des paramètres
    ['indication', 'Une réduction de 40% signifie que chaque fois que tu payes 100€, on te rembourse 40€.', 'string', 'Indication en bas à gauche.'], // Texte que l’élève peut faire apparaître en bas de l’écran
    ['nbrepetitionsmax', 10, 'entier', 'Nombre de répétitions maximum de la stor.<BR>Il peut être inférieur si on répond juste à toutes les questions'], // Obligatoire: nombre de questions posées pendant l’exercice
    ['nbchances', 2, 'entier', 'Nombre d’essais par répétition'], // Nombre d’essais autorisés avant de déclarer un échec et de passer à la question
    ['limite', 0, 'entier', 'Temps disponible par répétition, en secondes'], // S’il n’est pas vide, le temps de réponse sera limité
    ['b_enonce1', true, 'boolean', 'Nombre d’élèves à l’AS du collège (Nombres entiers).'],
    ['b_enonce2', true, 'boolean', 'Jetons dans une boîte (Nombres entiers).'],
    ['b_enonce3', true, 'boolean', 'Augmentation/réduction de prix. (Nombres décimaux).'],
    ['b_enonce4', true, 'boolean', 'Coût de livraison/Acompte de commande.(Nombre décimal).'],
    ['b_type1', true, 'boolean', 'Un article qui valait 100€ a augmenté de P€.<br>Quelle est l’augmentation pour un article de D€?'],
    ['b_type2', true, 'boolean', 'Le prix d’un article a augmenté de P%.<br>Quelle est l’augmentation pour un article de D€?'],
    ['b_type3', true, 'boolean', 'Le prix d’un article a augmenté de P%.<br>Quel est le nouveau prix d’un article de D€?'],
    ['b_type4', true, 'boolean', 'Un article qui valait D€ a augmenté de R€.<br>Quel est le pourcentage d’augmentation?'],
    ['b_type5', true, 'boolean', 'Un article qui valait D€ vaut maintenant F€.<br>Quel est le pourcentage d’augmentation?'],
    ['l_niveau', 'Calcul mental simple avec des entiers (x5, x9, x11, x25).', 'liste', 'Difficulté du calcul mental', ['Aucune difficulté de calcul mental.', 'Calcul mental simple avec des entiers (x5, x9, x11, x25).', 'Calcul mental simple avec des décimaux.']],
    ['b_multiple', true, 'boolean', 'Si la valeur de départ est un multiple de 100.'],
    ['b_diviseur', true, 'boolean', 'Si la valeur de départ est un diviseur de 100.'],
    ['b_aide', true, 'boolean', '<u>True</u> pour une suggestion de calcul mental.']
  ],
  pe: [
    { pe_1: 'Notion totalement maitrisée.' },
    { pe_2: 'Notion comprise.' },
    { pe_3: 'Notion fragile.' },
    { pe_4: 'Notion non comprise.' },
    { pe_5: 'Oubli de calcul de la valeur finale.' }
  ]
}

const idPrefix = 'Pourcentages1'
const textes = {
  phraseNbChances: 'Il te reste £e essai£p.', //  Ne pas supprimer
  enonces: [ // Enoncé 1
    [false, //  Non décimal
      // Question de type 1
      [ // Augmentation
        ['Dans un collège, 100 élèves participaient à l’AS l’année dernière. Il y en a £p de plus cette année.<br>' +
        'On suppose que cette augmentation est proportionnelle au nombre d’élèves.<br>' +
        'De combien augmentera le nombre d’élèves s’il y a £d élèves à l’AS? @1@ élèves.', // Question
        'Pour chaque groupe de 100 élèves, il y en a £p de plus l’année suivante.<br>' +
          'Combien de groupes de 100 élèves y a-t-il?', // Aide multiple
        'Pour chaque groupe de 100 élèves, il y en a £p de plus l’année suivante.<br>' +
          'Combien de fois moins que 100 élèves y a-t-il?', // Aide diviseur
        'Pour chaque groupe de 100 élèves, il y en a £p de plus l’année suivante.<br>' +
          'Il y a £n $\\times$ 100 élèves, il y aura donc £n $\\times$ £p = £v élèves de plus l’année suivante.', // Réponse multiple
        'Pour chaque groupe de 100 élèves, il y en a £p de plus l’année suivante.<br>' +
          'Il y a £n fois moins que 100 élèves, il y aura donc £p ÷ £n = £v élèves de plus l’année suivante.'], // Réponse diviseur
        // diminution
        ['Dans un collège, 100 élèves participaient à l’AS l’année dernière. Il y en a £p de moins cette année.<br>' +
        'On suppose que cette diminution est proportionnelle au nombre d’élèves.<br>' +
        'De combien diminuera le nombre d’élèves s’il y a £d élèves à l’AS? @1@ élèves.', // Question
        'Pour chaque groupe de 100 élèves, il y en a £p de moins l’année suivante.<br>' +
          'Combien de groupes de 100 élèves y a-t-il?', // Aide multiple
        'Pour chaque groupe de 100 élèves, il y en a £p de moins l’année suivante.<br>' +
          'Combien de fois moins que 100 élèves y a-t-il?', // Aide diviseur
        'Pour chaque groupe de 100 élèves, il y en a £p de moins l’année suivante.<br>' +
          'Il y a £n $\\times$ 100 élèves, il y aura donc £n $\\times$ £p = £v élèves de moins l’année suivante.', // Réponse multiple
        'Pour chaque groupe de 100 élèves, il y en a £p de moins l’année suivante.<br>' +
          'Il y a £n fois moins que 100 élèves, il y aura donc £p ÷ £n = £v élèves de moins l’année suivante.'] // Réponse diviseur
      ],
      // Question de type 2
      [ // Augmentation
        ['Chaque année, le nombre d’élèves d’un collège participant à l’AS augmente de £p%.<br>' +
        'De combien augmentera le nombre d’élèves s’il y a £d élèves à l’AS? @1@ élèves.', // Question
        '£p% signifie que pour chaque groupe de 100 élèves, il y en a £p de plus l’année suivante.<br>' +
          'Combien de groupes de 100 élèves y a-t-il?', // Aide multiple
        '£p% signifie que pour chaque groupe de 100 élèves, il y en a £p de plus l’année suivante.<br>' +
          'Combien de fois moins que 100 élèves y a-t-il?', // Aide diviseur
        '£p% signifie que pour chaque groupe de 100 élèves, il y en a £p de plus l’année suivante.<br>' +
          'Il y a £n $\\times$ 100 élèves, il y aura donc £n $\\times$ £p = £v élèves de plus l’année suivante.', // Réponse multiple
        '£p% signifie que pour chaque groupe de 100 élèves, il y en a £p de plus l’année suivante.<br>' +
          'Il y a £n fois moins que 100 élèves, il y aura donc £p ÷ £n = £v élèves de plus l’année suivante.'], // Réponse diviseur
        // diminution
        ['Chaque année, le nombre d’élèves d’un collège participant à l’AS diminue de £p%.<br>' +
        'De combien diminuera le nombre d’élèves s’il y a £d élèves à l’AS? @1@ élèves.', // Question
        '£p% signifie que pour chaque groupe de 100 élèves, il y en a £p de moins l’année suivante.<br>' +
          'Combien de groupes de 100 élèves y a-t-il?', // Aide multiple
        '£p% signifie que pour chaque groupe de 100 élèves, il y en a £p de moins l’année suivante.<br>' +
          'Combien de fois moins que 100 élèves y a-t-il?', // Aide diviseur
        '£p% signifie que pour chaque groupe de 100 élèves, il y en a £p de moins l’année suivante.<br>' +
          'Il y a £n $\\times$ 100 élèves, il y aura donc £n $\\times$ £p = £v élèves de moins l’année suivante.', // Réponse multiple
        '£p% signifie que pour chaque groupe de 100 élèves, il y en a £p de moins l’année suivante.<br>' +
          'Il y a £n fois moins que 100 élèves, il y aura donc £p ÷ £n = £v élèves de moins l’année suivante.'] // Réponse diviseur
      ],
      // Question de type 3
      [ // Augmentation
        ['Chaque année, le nombre d’élèves d’un collège participant à l’AS augmente de £p%.<br>' +
        'Combien y aura-t-il d’élèves l’année suivante s’il y a £d élèves à l’AS? @1@ élèves.', // Question
        '£p% signifie que pour chaque groupe de 100 élèves, il y en a £p de plus l’année suivante.<br>' +
          'Combien de groupes de 100 élèves y a-t-il?<br>' +
          'N’oublie pas de calculer le nombre total d’élèves.', // Aide multiple
        '£p% signifie que pour chaque groupe de 100 élèves, il y en a £p de plus l’année suivante.<br>' +
          'Combien de fois moins que 100 élèves y a-t-il?<br>' +
          'N’oublie pas de calculer le nombre total d’élèves.', // Aide diviseur
        '£p% signifie que pour chaque groupe de 100 élèves, il y en a £p de plus l’année suivante.<br>' +
          'Il y a £n $\\times$ 100 élèves, il y aura donc £n $\\times$ £p = £v élèves de plus l’année suivante.<br>' +
          'Le nombre total d’élèves est de £d + £v = £f.', // Réponse multiple
        '£p% signifie que pour chaque groupe de 100 élèves, il y en a £p de plus l’année suivante.<br>' +
          'Il y a £n fois moins que 100 élèves, il y aura donc £p ÷ £n = £v élèves de plus l’année suivante.<br>' +
          'Le nombre total d’élèves est de £d + £v = £f.'], // Réponse diviseur
        // diminution
        ['Chaque année, le nombre d’élèves d’un collège participant à l’AS diminue de £p%.<br>' +
        'Combien y aura-t-il d’élèves l’année suivante s’il y a £d élèves à l’AS? @1@ élèves.', // Question
        '£p% signifie que pour chaque groupe de 100 élèves, il y en a £p de moins l’année suivante.<br>' +
          'Combien de groupes de 100 élèves y a-t-il?<br>' +
          'N’oublie pas de calculer le nombre total d’élèves.', // Aide multiple
        '£p% signifie que pour chaque groupe de 100 élèves, il y en a £p de moins l’année suivante.<br>' +
          'Combien de fois moins que 100 élèves y a-t-il?<br>' +
          'N’oublie pas de calculer le nombre total d’élèves.', // Aide diviseur
        '£p% signifie que pour chaque groupe de 100 élèves, il y en a £p de moins l’année suivante.<br>' +
          'Il y a £n $\\times$ 100 élèves, il y aura donc £n $\\times$ £p = £v élèves de moins l’année suivante.<br>' +
          'Le nombre total d’élèves est de £d - £v = £f.', // Réponse multiple
        '£p% signifie que pour chaque groupe de 100 élèves, il y en a £p de moins l’année suivante.<br>' +
          'Il y a £n fois moins que 100 élèves, il y aura donc £p ÷ £n = £v élèves de moins l’année suivante.<br>' +
          'Le nombre total d’élèves est de £d - £v = £f.'] // Réponse diviseur
      ],
      // Question de type 4
      [ // Augmentation
        ['L’année dernière £d élèves participaient à l’AS. Il y en a £v de plus cette année.<br>' +
        'De quel pourcentage a augmenté le nombre d’élèves participant à l’AS? @1@%.', // Question
        'Trouver le pourcentage revient à chercher combien il y aurait eu d’élèves en plus s’il y avait eu 100 élèves l’année dernière.<br>' +
          'Combien de groupes de 100 élèves y a-t-il?', // Aide multiple
        'Trouver le pourcentage revient à chercher combien il y aurait eu d’élèves en plus s’il y avait eu 100 élèves l’année dernière.<br>' +
          'Par combien faut-il multiplier £d pour obtenir 100?', // Aide diviseur
        'Trouver le pourcentage revient à chercher combien il y aurait eu d’élèves en plus s’il y avait eu 100 élèves l’année dernière.<br>' +
          '£d ÷ £n = 100, il y aurait donc eu £v ÷ £n = £p élèves de plus cette année.<br>' +
          'Le nombre d’élèves participant à l’AS a augmenté de £p%.', // Réponse multiple
        'Trouver le pourcentage revient à chercher combien il y aurait eu d’élèves en plus s’il y avait eu 100 élèves l’année dernière.<br>' +
          '£d $\\times$ £n = 100, il y aurait donc eu £v $\\times$ £n = £p élèves de plus cette année.<br>' +
          'Le nombre d’élèves participant à l’AS a augmenté de £p%.'], // Réponse diviseur
        // diminution
        ['L’année dernière £d élèves participaient à l’AS. Il y en a £v de moins cette année.<br>' +
        'De quel pourcentage a diminué le nombre d’élèves participant à l’AS? @1@%.', // Question
        'Trouver le pourcentage revient à chercher combien il y aurait eu d’élèves en moins s’il y avait eu 100 élèves l’année dernière.<br>' +
          'Combien de groupes de 100 élèves y a-t-il?', // Aide multiple
        'Trouver le pourcentage revient à chercher combien il y aurait eu d’élèves en moins s’il y avait eu 100 élèves l’année dernière.<br>' +
          'Par combien faut-il multiplier £d pour obtenir 100?', // Aide diviseur
        'Trouver le pourcentage revient à chercher combien il y aurait eu d’élèves en moins s’il y avait eu 100 élèves l’année dernière.<br>' +
          '£d ÷ £n = 100, il y aurait donc eu £v ÷ £n = £p élèves de moins cette année.<br>' +
          'Le nombre d’élèves participant à l’AS a diminué de £p%.', // Réponse multiple
        'Trouver le pourcentage revient à chercher combien il y aurait eu d’élèves en moins s’il y avait eu 100 élèves l’année dernière.<br>' +
          '£d $\\times$ £n = 100, il y aurait donc eu £v $\\times$ £n = £p élèves de moins cette année.<br>' +
          'Le nombre d’élèves participant à l’AS a diminué de £p%.'] // Réponse diviseur
      ],
      // Question de type 5
      [ // Augmentation
        ['L’année dernière £d élèves participaient à l’AS. Il y en a £f cette année.<br>' +
        'De quel pourcentage a augmenté le nombre d’élèves participant à l’AS? @1@%.', // Question
        'Trouver le pourcentage revient à chercher combien il y aurait eu d’élèves en plus s’il y avait eu 100 élèves l’année dernière.<br>' +
          'Combien de groupes de 100 élèves y a-t-il?<br>' +
          'Pense à calculer d’abord combien il y a eu d’élèves en plus.', // Aide multiple
        'Trouver le pourcentage revient à chercher combien il y aurait eu d’élèves en plus s’il y avait eu 100 élèves l’année dernière.<br>' +
          'Par combien faut-il multiplier £d pour obtenir 100?<br>' +
          'Pense à calculer d’abord combien il y a eu d’élèves en plus.', // Aide diviseur
        'Trouver le pourcentage revient à chercher combien il y aurait eu d’élèves en plus s’il y avait eu 100 élèves l’année dernière.<br>' +
          'Il y a eu £f - £d = £v élèves en plus cette année.<br>' +
          '£d ÷ £n = 100, il y aurait donc eu £v ÷ £n = £p élèves de plus cette année.<br>' +
          'Le nombre d’élèves participant à l’AS a augmenté de £p%.', // Réponse multiple
        'Trouver le pourcentage revient à chercher combien il y aurait eu d’élèves en plus s’il y avait eu 100 élèves l’année dernière.<br>' +
          'Il y a eu £f - £d = £v élèves en plus cette année.<br>' +
          '£d $\\times$ £n = 100, il y aurait donc eu £v $\\times$ £n = £p élèves de plus cette année.<br>' +
          'Le nombre d’élèves participant à l’AS a augmenté de £p%.'], // Réponse diviseur
        // diminution
        ['L’année dernière £d élèves participaient à l’AS. Il y en a £f cette année.<br>' +
        'De quel pourcentage a diminué le nombre d’élèves participant à l’AS? @1@%.', // Question
        'Trouver le pourcentage revient à chercher combien il y aurait eu d’élèves en moins s’il y avait eu 100 élèves l’année dernière.<br>' +
          'Combien de groupes de 100 élèves y a-t-il?<br>' +
          'Pense à calculer d’abord combien il y a eu d’élèves en moins.', // Aide multiple
        'Trouver le pourcentage revient à chercher combien il y aurait eu d’élèves en moins s’il y avait eu 100 élèves l’année dernière.<br>' +
          'Par combien faut-il multiplier £d pour obtenir 100?<br>' +
          'Pense à calculer d’abord combien il y a eu d’élèves en moins.', // Aide diviseur
        'Trouver le pourcentage revient à chercher combien il y aurait eu d’élèves en moins s’il y avait eu 100 élèves l’année dernière.<br>' +
          'Il y a eu £d - £f = £v élèves en moins cette année.<br>' +
          '£d ÷ £n = 100, il y aurait donc eu £v ÷ £n = £p élèves de moins cette année.<br>' +
          'Le nombre d’élèves participant à l’AS a diminué de £p%.', // Réponse multiple
        'Trouver le pourcentage revient à chercher combien il y aurait eu d’élèves en moins s’il y avait eu 100 élèves l’année dernière.<br>' +
          'Il y a eu £d - £f = £v élèves en moins cette année.<br>' +
          '£d $\\times$ £n = 100, il y aurait donc eu £v $\\times$ £n = £p élèves de moins cette année.<br>' +
          'Le nombre d’élèves participant à l’AS a diminué de £p%.'] // Réponse diviseur
      ]
    ],
    //  Enoncé 2
    [false, //  Non décimal
      // Question de type 1
      [ // Augmentation
        ['Pierre avait 100 jetons au début de la partie. Il a gagné £p jetons.<br>' +
        'Il espère gagner la même proportion de jetons à chaque partie.<br>' +
        'Combien de jetons espère-t-il gagner s’il a £d jetons au départ? @1@ jetons.', // Question
        'Pour chaque paquet de 100 jetons, il espère en gagner £p.<br>' +
          'Combien de paquets de 100 jetons a-t-il?', // Aide multiple
        'Pour chaque paquet de 100 jetons, il espère en gagner £p.<br>' +
          'Combien de fois moins que 100 jetons a-t-il?', // Aide diviseur
        'Pour chaque paquet de 100 jetons, il espère en gagner £p.<br>' +
          'Il a £n $\\times$ 100 jetons, il espère donc gagner £n $\\times$ £p = £v jetons.', // Réponse multiple
        'Pour chaque paquet de 100 jetons, il espère en gagner £p.<br>' +
          'Il a £n fois moins que 100 jetons, il espère donc gagner £p ÷ £n = £v jetons.'], // Réponse diviseur
        // diminution
        ['Une boîte de 100 jetons contient £p jetons rouges.<br>' +
        'Toutes les boîtes contiennent la même proportion de jetons rouges.<br>' +
        'Combien y a-t-il de jetons rouges dans une boîte de £d jetons? @1@ jetons rouges.', // Question
        'Dans chaque paquet de 100 jetons, il y en a £p de rouges.<br>' +
          'Combien de paquets de 100 jetons y a-t-il?', // Aide multiple
        'Dans chaque paquet de 100 jetons, il y en a £p de rouges.<br>' +
          'Combien de fois moins que 100 jetons y a-t-il?', // Aide diviseur
        'Dans chaque paquet de 100 jetons, il y en a £p de rouges.<br>' +
          'Il y a £n $\\times$ 100 jetons, il y a donc £n $\\times$ £p = £v jetons rouges.', // Réponse multiple
        'Dans chaque paquet de 100 jetons, il y en a £p de rouges.<br>' +
          'Il a £n fois moins que 100 jetons, il y a donc £p ÷ £n = £v jetons rouges.'] // Réponse diviseur
      ],
      // Question de type 2
      [ // Augmentation
        ['A chaque partie, Pierre espère gagner £p% de jetons en plus.<br>' +
        'Combien de jetons espère-t-il gagner s’il a £d jetons au départ? @1@ jetons.', // Question
        '£p% signifie que pour chaque paquet de 100 jetons, il espère en gagner £p.<br>' +
          'Combien de paquets de 100 jetons a-t-il?', // Aide multiple
        '£p% signifie que pour chaque paquet de 100 jetons, il espère en gagner £p.<br>' +
          'Combien de fois moins que 100 jetons a-t-il?', // Aide diviseur
        '£p% signifie que pour chaque paquet de 100 jetons, il espère en gagner £p.<br>' +
          'Il a £n $\\times$ 100 jetons, il espère donc gagner £n $\\times$ £p = £v jetons.', // Réponse multiple
        '£p% signifie que pour chaque paquet de 100 jetons, il espère en gagner £p.<br>' +
          'Il a £n fois moins que 100 jetons, il espère donc gagner £p ÷ £n = £v jetons.'], // Réponse diviseur
        // diminution
        ['Toutes les boîtes contiennent £p% de jetons rouges.<br>' +
        'Combien y a-t-il de jetons rouges dans une boîte de £d jetons? @1@ jetons rouges.', // Question
        '£p% signifie que dans chaque paquet de 100 jetons, il y en a £p de rouges.<br>' +
          'Combien de paquets de 100 jetons y a-t-il?', // Aide multiple
        '£p% signifie que dans chaque paquet de 100 jetons, il y en a £p de rouges.<br>' +
          'Combien de fois moins que 100 jetons y a-t-il?', // Aide diviseur
        '£p% signifie que dans chaque paquet de 100 jetons, il y en a £p de rouges.<br>' +
          'Il y a £n $\\times$ 100 jetons, il y a donc £n $\\times$ £p = £v jetons rouges.', // Réponse multiple
        '£p% signifie que dans chaque paquet de 100 jetons, il y en a £p de rouges.<br>' +
          'Il a £n fois moins que 100 jetons, il y a donc £p ÷ £n = £v jetons rouges.'] // Réponse diviseur
      ],
      // Question de type 3
      [ // Augmentation
        ['A chaque partie, Pierre espère gagner £p% de jetons en plus.<br>' +
        'Combien de jetons aura-t-il après une partie s’il a £d jetons au départ? @1@ jetons.', // Question
        '£p% signifie que pour chaque paquet de 100 jetons, il espère en gagner £p.<br>' +
          'Combien de paquets de 100 jetons a-t-il?<br>' +
          'N’oublie pas de calculer le nombre total de jetons.', // Aide multiple
        '£p% signifie que pour chaque paquet de 100 jetons, il espère en gagner £p.<br>' +
          'Combien de fois moins que 100 jetons a-t-il?<br>' +
          'N’oublie pas de calculer le nombre total de jetons.', // Aide diviseur
        '£p% signifie que pour chaque paquet de 100 jetons, il espère en gagner £p.<br>' +
          'Il a £n $\\times$ 100 jetons, il espère donc gagner £n $\\times$ £p = £v jetons.<br>' +
          'Le nombre total de jetons est de £d + £v = £f.', // Réponse multiple
        '£p% signifie que pour chaque paquet de 100 jetons, il espère en gagner £p.<br>' +
          'Il a £n fois moins que 100 jetons, il espère donc gagner £p ÷ £n = £v jetons.<br>' +
          'Le nombre total de jetons est de £d + £v = £f.'], // Réponse diviseur
        // diminution
        ['Toutes les boîtes contiennent £p% de jetons rouges. Les autres jetons sont verts.<br>' +
        'Combien y a-t-il de jetons verts dans une boîte de £d jetons? @1@ jetons verts.', // Question
        '£p% signifie que dans chaque paquet de 100 jetons, il y en a £p de rouges.<br>' +
          'Combien de paquets de 100 jetons y a-t-il?<br>' +
          'N’oublie pas de calculer le nombre de jetons final.', // Aide multiple
        '£p% signifie que dans chaque paquet de 100 jetons, il y en a £p de rouges.<br>' +
          'Combien de fois moins que 100 jetons y a-t-il?<br>' +
          'N’oublie pas de calculer le nombre de jetons final.', // Aide diviseur
        '£p% signifie que dans chaque paquet de 100 jetons, il y en a £p de rouges.<br>' +
          'Il y a £n $\\times$ 100 jetons, il y a donc £n $\\times$ £p = £v jetons rouges.<br>' +
          'Le nombre de jetons verts est de £d - £v = £f.', // Réponse multiple
        '£p% signifie que dans chaque paquet de 100 jetons, il y en a £p de rouges.<br>' +
          'Il a £n fois moins que 100 jetons, il y a donc £p ÷ £n = £v jetons rouges.<br>' +
          'Le nombre de jetons verts est de £d - £v = £f.'] // Réponse diviseur
      ],
      // Question de type 4
      [ // Augmentation
        ['Pierre avait £d jetons au début de la partie. Il a gagné £v jetons.<br>' +
        'De quel pourcentage a augmenté son nombre de jetons? @1@%.', // Question
        'Trouver le pourcentage revient à chercher combien il aurait gagné de jetons s’il avait eu 100 jetons en début de partie.<br>' +
          'Combien de paquets de 100 jetons a-t-il?', // Aide multiple
        'Trouver le pourcentage revient à chercher combien il aurait gagné de jetons s’il avait eu 100 jetons en début de partie.<br>' +
          'Combien de fois moins que 100 jetons a-t-il?', // Aide diviseur
        'Trouver le pourcentage revient à chercher combien il aurait gagné de jetons s’il avait eu 100 jetons en début de partie.<br>' +
          '£d ÷ £n = 100, il aurait donc gagné £v ÷ £n = £p jetons.<br>' +
          'Son nombre de jetons a augmenté de £p%.', // Réponse multiple
        'Trouver le pourcentage revient à chercher combien il aurait gagné de jetons s’il avait eu 100 jetons en début de partie.<br>' +
          '£d $\\times$ £n = 100, il aurait donc gagné £v $\\times$ £n = £p jetons.<br>' +
          'Son nombre de jetons a augmenté de £p%.'], // Réponse diviseur
        // diminution
        ['Une boîte de £d jetons contient £v jetons rouges. <br>' +
        'Quel est le pourcentage de jetons rouges? @1@%.', // Question
        'Trouver le pourcentage revient à chercher combien il y aurait de jetons rouges dans une boîte de 100 jetons.<br>' +
          'Combien de paquets de 100 jetons y a-t-il?', // Aide multiple
        'Trouver le pourcentage revient à chercher combien il y aurait de jetons rouges dans une boîte de 100 jetons.<br>' +
          'Combien de fois moins que 100 jetons y a-t-il?', // Aide diviseur
        'Trouver le pourcentage revient à chercher combien il y aurait de jetons rouges dans une boîte de 100 jetons.<br>' +
          '£d ÷ £n = 100, il y aurait donc £v ÷ £n = £p jetons rouges<br>' +
          'Il y a £p% de jetons rouges.', // Réponse multiple
        'Trouver le pourcentage revient à chercher combien il y aurait de jetons rouges dans une boîte de 100 jetons.<br>' +
          '£d $\\times$ £n = 100, il y aurait donc £v $\\times$ £n = £p jetons rouges.<br>' +
          'Il y a £p% de jetons rouges.'] // Réponse diviseur
      ],
      // Question de type 5
      [ // Augmentation
        ['Pierre avait £d jetons au début de la partie. Il a maintenant £f jetons.<br>' +
        'De quel pourcentage a augmenté son nombre de jetons? @1@%.', // Question
        'Trouver le pourcentage revient à chercher combien il aurait gagné de jetons s’il avait eu 100 jetons en début de partie.<br>' +
          'Combien de paquets de 100 jetons a-t-il?<br>' +
          'Pense à calculer d’abord combien il a gagné de jetons.', // Aide multiple
        'Trouver le pourcentage revient à chercher combien il aurait gagné de jetons s’il avait eu 100 jetons en début de partie.<br>' +
          'Combien de fois moins que 100 jetons a-t-il<br>' +
          'Pense à calculer d’abord combien il a gagné de jetons.', // Aide diviseur
        'Trouver le pourcentage revient à chercher combien il aurait gagné de jetons s’il avait eu 100 jetons en début de partie.<br>' +
          'Il a gagné £f - £d = £v jetons.<br>' +
          '£d ÷ £n = 100, il aurait donc gagné £v ÷ £n = £p jetons.<br>' +
          'Son nombre de jetons a augmenté de £p%.', // Réponse multiple
        'Trouver le pourcentage revient à chercher combien il aurait gagné de jetons s’il avait eu 100 jetons en début de partie.<br>' +
          'Il a gagné £f - £d = £v jetons.<br>' +
          '£d $\\times$ £n = 100, il a donc gagné £v $\\times$ £n = £p jetons.<br>' +
          'Son nombre de jetons a augmenté de £p%.'], // Réponse diviseur
        // diminution
        ['Une boîte de £d jetons contient £f jetons rouges. Les autres sont verts.<br>' +
        'Quel est le pourcentage de jetons verts? @1@%.', // Question
        'Trouver le pourcentage revient à chercher combien il y aurait de jetons verts dans une boîte de 100 jetons.<br>' +
          'Combien de paquets de 100 jetons y a-t-il?<br>' +
          'Pense à calculer d’abord combien il y a de jetons verts.', // Aide multiple
        'Trouver le pourcentage revient à chercher combien il y aurait de jetons verts dans une boîte de 100 jetons.<br>' +
          'Combien de fois moins que 100 jetons y a-t-il?<br>' +
          'Pense à calculer d’abord combien il y a de jetons verts.', // Aide diviseur
        'Trouver le pourcentage revient à chercher combien il y aurait de jetons verts dans une boîte de 100 jetons.<br>' +
          'Il y a £d - £f = £v jetons verts.<br>' +
          '£d ÷ £n = 100, il y aurait donc £v ÷ £n = £p jetons verts.<br>' +
          'Il y a £p% de jetons verts.', // Réponse multiple
        'Trouver le pourcentage revient à chercher combien il y aurait de jetons verts dans une boîte de 100 jetons.<br>' +
          'Il y a £d - £f = £v jetons verts.<br>' +
          '£d $\\times$ £n = 100, il y aurait donc £v $\\times$ £n = £p jetons verts.<br>' +
          'Il y a £p% de jetons verts.'] // Réponse diviseur
      ]
    ],
    //  Enoncé 3
    [true, //  Décimal
      // Question de type 1
      [ // Augmentation
        ['Un article qui valait 100€ a augmenté de £p€.<br>' +
        'L’augmentation est proportionnelle au prix initial.<br>' +
        'Quelle est l’augmentation pour un article de £d€? @1@€.', // Question
        'A chaque fois que tu payes 100€, tu dois payer £p€ en plus.<br>' +
          'Combien de fois as-tu payé 100€?', // Aide multiple
        'A chaque fois que tu payes 100€, tu dois payer £p€ en plus.<br>' +
          'Tu a payé combien de fois moins que 100€?', // Aide diviseur
        'A chaque fois que tu payes 100€, tu dois payer £p€ en plus.<br>' +
          'Tu as payé £n $\\times$ 100€, donc l’article a augmenté de  £n $\\times$ £p = £v€.', // Réponse multiple
        'A chaque fois que tu payes 100€, tu dois payer £p€ en plus.<br>' +
          'Tu as payé £n fois moins que 100€, donc l’article a augmenté de £p ÷ £n = £v€.'], // Réponse diviseur
        // diminution
        ['Pour un article de 100€ la réduction est de £p€.<br>' +
        'La réduction est proportionnelle au prix de l’article.<br>' +
        'Quel est la réduction pour un article de £d€? @1@€.', // Question
        'A chaque fois que tu payes 100€, on te rend £p€.<br>' +
          'Combien de fois as-tu payé 100€?', // Aide multiple
        'A chaque fois que tu payes 100€, on te rend £p€.<br>' +
          'Tu a payé combien de fois moins que 100€?', // Aide diviseur
        'A chaque fois que tu payes 100€, on te rend £p€.<br>' +
          'Tu as payé £n $\\times$ 100€, donc on te rend £n $\\times$ £p = £v€.', // Réponse multiple
        'A chaque fois que tu payes 100€, on te rend £p€.<br>' +
          'Il y a £n fois moins que 100€, donc on te rend £p ÷ £n = £v€.'] // Réponse diviseur
      ],
      // Question de type 2
      [ // Augmentation
        ['Le prix d’un article a augmenté de £p%.<br>' +
        'Quelle est l’augmentation pour un article de £d€? @1@€.', // Question
        '£p% signifie qu’à chaque fois que tu payes 100€, tu dois payer £p€ en plus.<br>' +
          'Combien de fois as-tu payé 100€?', // Aide multiple
        '£p% signifie qu’à chaque fois que tu payes 100€, tu dois payer £p€ en plus.<br>' +
          'Tu a payé combien de fois moins que 100€?', // Aide diviseur
        '£p% signifie qu’à chaque fois que tu payes 100€, tu dois payer £p€ en plus.<br>' +
          'Tu as payé £n $\\times$ 100€, donc l’article a augmenté de  £n $\\times$ £p = £v€.', // Réponse multiple
        '£p% signifie qu’à chaque fois que tu payes 100€, tu dois payer £p€ en plus.<br>' +
          'Tu as payé £n fois moins que 100€, donc l’article a augmenté de £p ÷ £n = £v€.'], // Réponse diviseur
        // diminution
        ['Un magasin fait une réduction de £p%.<br>' +
        'Quelle est la réduction pour un article de £d€? @1@€.', // Question
        '£p% signifie qu’à chaque fois que tu payes 100€, on te rend £p€.<br>' +
          'Combien de fois as-tu payé 100€?', // Aide multiple
        '£p% signifie qu’à chaque fois que tu payes 100€, on te rend £p€.<br>' +
          'Tu a payé combien de fois moins que 100€?', // Aide diviseur
        '£p% signifie qu’à chaque fois que tu payes 100€, on te rend £p€.<br>' +
          'Tu as payé £n $\\times$ 100€, donc on te rend £n $\\times$ £p = £v€.', // Réponse multiple
        '£p% signifie qu’à chaque fois que tu payes 100€, on te rend £p€.<br>' +
          'Il y a £n fois moins que 100€, donc on te rend £p ÷ £n = £v€.'] // Réponse diviseur
      ],
      // Question de type 3
      [ // Augmentation
        ['Le prix d’un article a augmenté de £p%.<br>' +
        'Quel est le nouveau prix d’un article de £d€? @1@€.', // Question
        '£p% signifie qu’à chaque fois que tu payes 100€, tu dois payer £p€ en plus.<br>' +
          'Combien de fois as-tu payé 100€?<br>' +
          'N’oublie pas de calculer le prix total.', // Aide multiple
        '£p% signifie qu’à chaque fois que tu payes 100€, tu dois payer £p€ en plus.<br>' +
          'Tu a payé combien de fois moins que 100€?<br>' +
          'N’oublie pas de calculer le prix total.', // Aide diviseur
        '£p% signifie qu’à chaque fois que tu payes 100€, tu dois payer £p€ en plus.<br>' +
          'Tu as payé £n $\\times$ 100€, donc l’article a augmenté de  £n $\\times$ £p = £v€.<br>' +
          'Le prix total est de £d + £v = £f€.', // Réponse multiple
        '£p% signifie qu’à chaque fois que tu payes 100€, tu dois payer £p€ en plus.<br>' +
          'Tu as payé £n fois moins que 100€, donc l’article a augmenté de £p ÷ £n = £v€.<br>' +
          'Le prix total est de £d + £v = £f€.'], // Réponse diviseur
        // diminution
        ['Un magasin fait une réduction de £p%.<br>' +
        'Quel est le nouveau prix pour un article de £d€? @1@€.', // Question
        '£p% signifie qu’à chaque fois que tu payes 100€, on te rend £p€.<br>' +
          'Combien de fois as-tu payé 100€?<br>' +
          'N’oublie pas de calculer le prix total.', // Aide multiple
        '£p% signifie qu’à chaque fois que tu payes 100€, on te rend £p€.<br>' +
          'Tu a payé combien de fois moins que 100€?<br>' +
          'N’oublie pas de calculer le prix total.', // Aide diviseur
        '£p% signifie qu’à chaque fois que tu payes 100€, on te rend £p€.<br>' +
          'Tu as payé £n $\\times$ 100€, donc on te rend £n $\\times$ £p = £v€.<br>' +
          'Le prix total est de £d - £v = £f€.', // Réponse multiple
        '£p% signifie qu’à chaque fois que tu payes 100€, on te rend £p€.<br>' +
          'Il y a £n fois moins que 100€, donc on te rend £p ÷ £n = £v€.<br>' +
          'Le prix total est de £d - £v = £f€.'] // Réponse diviseur
      ],
      // Question de type 4
      [ // Augmentation
        ['Un article qui valait £d€ a augmenté de £v€.<br>' +
        'Quel est le pourcentage d’augmentation? @1@%.', // Question
        'Trouver le pourcentage revient à chercher quelle aurait été l’augmentation si l’article avait coûté 100€.<br>' +
          'Combien de fois as-tu payé 100€?', // Aide multiple
        'Trouver le pourcentage revient à chercher quelle aurait été l’augmentation si l’article avait coûté 100€.<br>' +
          'Tu a payé combien de fois moins que 100€?', // Aide diviseur
        'Trouver le pourcentage revient à chercher quelle aurait été l’augmentation si l’article avait coûté 100€.<br>' +
          '£d ÷ £n = 100, il aurait donc augmenté de £v ÷ £n = £p€.<br>' +
          'Le prix a augmenté de £p%.', // Réponse multiple
        'Trouver le pourcentage revient à chercher quelle aurait été l’augmentation si l’article avait coûté 100€.<br>' +
          '£d $\\times$ £n = 100, il aurait donc augmenté de £v $\\times$ £n = £p€.<br>' +
          'Le prix a augmenté de £p%.'], // Réponse diviseur
        // diminution
        ['Un article qui valait £d€ est vendu avec une réduction £v€.<br>' +
        'Quel est le pourcentage de réduction? @1@%.', // Question
        'Trouver le pourcentage revient à chercher quelle aurait été la réduction si l’article avait coûté 100€.<br>' +
          'Combien de fois as-tu payé 100€?', // Aide multiple
        'Trouver le pourcentage revient à chercher quelle aurait été la réduction si l’article avait coûté 100€.<br>' +
          'Tu a payé combien de fois moins que 100€?', // Aide diviseur
        'Trouver le pourcentage revient à chercher quelle aurait été la réduction si l’article avait coûté 100€.<br>' +
          '£d ÷ £n = 100, la réduction aurait donc été de £v ÷ £n = £p€.<br>' +
          'Le pourcentage de réduction est de £p%.', // Réponse multiple
        'Trouver le pourcentage revient à chercher quelle aurait été la réduction si l’article avait coûté 100€.<br>' +
          '£d $\\times$ £n = 100, la réduction aurait donc été de £v $\\times$ £n = £p€.<br>' +
          'Le pourcentage de réduction est de £p%.'] // Réponse diviseur
      ],
      // Question de type 5
      [ // Augmentation
        ['Un article qui valait £d€ vaut maintenant £f€.<br>' +
        'Quel est le pourcentage d’augmentation? @1@%.', // Question
        'Trouver le pourcentage revient à chercher quelle aurait été l’augmentation si l’article avait coûté 100€.<br>' +
          'Combien de fois as-tu payé 100€?<br>' +
          'Pense à calculer d’abord de combien le prix de l’article a augmenté.', // Aide multiple
        'Trouver le pourcentage revient à chercher quelle aurait été l’augmentation si l’article avait coûté 100€.<br>' +
          'Tu a payé combien de fois moins que 100€?<br>' +
          'Pense à calculer d’abord de combien le prix de l’article a augmenté.', // Aide diviseur
        'Trouver le pourcentage revient à chercher quelle aurait été l’augmentation si l’article avait coûté 100€.<br>' +
          'Le prix a augmenté de £f - £d = £v€.<br>' +
          '£d ÷ £n = 100, il aurait donc augmenté de £v ÷ £n = £p€.<br>' +
          'Le prix a augmenté de £p%.', // Réponse multiple
        'Trouver le pourcentage revient à chercher quelle aurait été l’augmentation si l’article avait coûté 100€.<br>' +
          'Le prix a augmenté de £f - £d = £v€.<br>' +
          '£d $\\times$ £n = 100, il aurait donc augmenté de £v $\\times$ £n = £p€.<br>' +
          'Le prix a augmenté de £p%.'], // Réponse diviseur
        // diminution
        ['Un article qui valait £d€ vaut maintenant £f€.<br>' +
        'Quel est le pourcentage de réduction? @1@%.', // Question
        'Trouver le pourcentage revient à chercher quelle aurait été la réduction si l’article avait coûté 100€.<br>' +
          'Combien de fois as-tu payé 100€?<br>' +
          'Pense à calculer d’abord de combien le prix de l’article a diminué.', // Aide multiple
        'Trouver le pourcentage revient à chercher quelle aurait été la réduction si l’article avait coûté 100€.<br>' +
          'Tu a payé combien de fois moins que 100€?<br>' +
          'Pense à calculer d’abord de combien le prix de l’article a diminué.', // Aide diviseur
        'Trouver le pourcentage revient à chercher quelle aurait été la réduction si l’article avait coûté 100€.<br>' +
          'Le prix a diminué de £d - £f = £v€.<br>' +
          '£d ÷ £n = 100, la réduction aurait donc été de £v ÷ £n = £p€.<br>' +
          'Le pourcentage de réduction est de £p%.', // Réponse multiple
        'Trouver le pourcentage revient à chercher quelle aurait été la réduction si l’article avait coûté 100€.<br>' +
          'Le prix a diminué de £d - £f = £v€.<br>' +
          '£d $\\times$ £n = 100, la réduction aurait donc été de £v $\\times$ £n = £p€.<br>' +
          'Le pourcentage de réduction est de £p%.'] // Réponse diviseur
      ]
    ],
    //  Enoncé 4
    [true, //  Décimal
      // Question de type 1
      [ // Augmentation
        ['La livraison d’un article de  100€ coûte £p€.<br>' +
        'Le prix de la livraison est proportionnel au prix d’achat.<br>' +
        'Quel est le prix de livraison d’un article de £d€? @1@€.', // Question
        'A chaque fois que tu payes 100€, tu dois payer £p€ pour la livraison.<br>' +
          'Combien de fois as-tu payé 100€?', // Aide multiple
        'A chaque fois que tu payes 100€, tu dois payer £p€ pour la livraison.<br>' +
          'Tu a payé combien de fois moins que 100€?', // Aide diviseur
        'A chaque fois que tu payes 100€, tu dois payer £p€ pour la livraison.<br>' +
          'Tu as payé £n $\\times$ 100€, donc la livraison coûte  £n $\\times$ £p = £v€.', // Réponse multiple
        'A chaque fois que tu payes 100€, tu dois payer £p€ pour la livraison.<br>' +
          'Tu as payé £n fois moins que 100€, donc la livraison coûte £p ÷ £n = £v€.'], // Réponse diviseur
        // diminution
        ['Pour commander un article de 100€, il faut payer £p€ à la commande.<br>' +
        'Le prix à payer à la commande est proportionnel au prix de l’article.<br>' +
        'Combien doit-on payer pour commander un article de £d€? @1@€.', // Question
        'A chaque fois que tu commandes 100€, tu dois payer £p€ à la commande.<br>' +
          'Combien de fois as-tu commandé 100€?', // Aide multiple
        'A chaque fois que tu commandes 100€, tu dois payer £p€ à la commande.<br>' +
          'Tu a commandé combien de fois moins que 100€?', // Aide diviseur
        'A chaque fois que tu commandes 100€, tu dois payer £p€ à la commande.<br>' +
          'Tu as commandé £n $\\times$ 100€, donc tu dois payer £n $\\times$ £p = £v€ à la commande.', // Réponse multiple
        'A chaque fois que tu commandes 100€, tu dois payer £p€ à la commande.<br>' +
          'Tu as commandé £n fois moins que 100€, donc tu dois payer £p ÷ £n = £v€ à la commande.'] // Réponse diviseur
      ],
      // Question de type 2
      [ // Augmentation
        ['La livraison d’un article coûte £p% du prix d’achat.<br>' +
        'Quel est le prix de livraison d’un article de £d€? @1@€.', // Question
        '£p% signifie qu’à chaque fois que tu payes 100€, tu dois payer £p€ pour la livraison.<br>' +
          'Combien de fois as-tu payé 100€?', // Aide multiple
        '£p% signifie qu’à chaque fois que tu payes 100€, tu dois payer £p€ pour la livraison.<br>' +
          'Tu a payé combien de fois moins que 100€?', // Aide diviseur
        '£p% signifie qu’à chaque fois que tu payes 100€, tu dois payer £p€ pour la livraison.<br>' +
          'Tu as payé £n $\\times$ 100€, donc la livraison coûte  £n $\\times$ £p = £v€.', // Réponse multiple
        '£p% signifie qu’à chaque fois que tu payes 100€, tu dois payer £p€ pour la livraison.<br>' +
          'Tu as payé £n fois moins que 100€, donc la livraison coûte £p ÷ £n = £v€.'], // Réponse diviseur
        // diminution
        ['Dans un magasin il faut payer £p% à la commande.<br>' +
        'Combien doit-on payer pour commander un article de £d€? @1@€.', // Question
        '£p% signifie qu’à chaque fois que tu commandes 100€, tu dois payer £p€ à la commande.<br>' +
          'Combien de fois as-tu commandé 100€?', // Aide multiple
        '£p% signifie qu’à chaque fois que tu commandes 100€, tu dois payer £p€ à la commande.<br>' +
          'Tu a commandé combien de fois moins que 100€?', // Aide diviseur
        '£p% signifie qu’à chaque fois que tu commandes 100€, tu dois payer £p€ à la commande.<br>' +
          'Tu as commandé £n $\\times$ 100€, donc tu dois payer £n $\\times$ £p = £v€ à la commande.', // Réponse multiple
        '£p% signifie qu’à chaque fois que tu commandes 100€, tu dois payer £p€ à la commande.<br>' +
          'Tu as commandé £n fois moins que 100€, donc tu dois payer £p ÷ £n = £v€ à la commande.'] // Réponse diviseur
      ],
      // Question de type 3
      [ // Augmentation
        ['La livraison d’un article coûte £p% du prix d’achat.<br>' +
        'Quel est le prix total d’un article de £d€ avec sa livraison? @1@€.', // Question
        '£p% signifie qu’à chaque fois que tu payes 100€, tu dois payer £p€ pour la livraison.<br>' +
          'Combien de fois as-tu payé 100€?<br>' +
          'N’oublie pas de calculer le prix total.', // Aide multiple
        '£p% signifie qu’à chaque fois que tu payes 100€, tu dois payer £p€ pour la livraison.<br>' +
          'Tu a payé combien de fois moins que 100€?<br>' +
          'N’oublie pas de calculer le prix total.', // Aide diviseur
        '£p% signifie qu’à chaque fois que tu commandes 100€, tu dois payer £p€ pour la livraison.<br>' +
          'Tu as payé £n $\\times$ 100€, donc la livraison coûte £n $\\times$ £p = £v€.<br>' +
          'Le prix total est de £d + £v = £f€.', // Réponse multiple
        '£p% signifie qu’à chaque fois que tu commandes 100€, tu dois payer £p€ pour la livraison.<br>' +
          'Tu as payé £n fois moins que 100€, donc la livraison coûte £p ÷ £n = £v€.<br>' +
          'Le prix total est de £d + £v = £f€.'], // Réponse diviseur
        // diminution
        ['Dans un magasin il faut payer £p% à la commande.<br>' +
        'Que restera-t-il à payer pour un article de £d€? @1@€.', // Question
        '£p% signifie qu’à chaque fois que tu commandes 100€, tu dois payer £p€ à la commande.<br>' +
          'Combien de fois as-tu commandé 100€?<br>' +
          'N’oublie pas de calculer le prix total.', // Aide multiple
        '£p% signifie qu’à chaque fois que tu commandes 100€, tu dois payer £p€ à la commande.<br>' +
          'Tu a commandé combien de fois moins que 100€?<br>' +
          'N’oublie pas de calculer le prix total.', // Aide diviseur
        '£p% signifie qu’à chaque fois que tu commandes 100€, tu dois payer £p€ à la commande.<br>' +
          'Tu as commandé £n $\\times$ 100€, donc tu dois payer  £n $\\times$ £p = £v€ à la commande.<br>' +
          'Le prix total est de £d - £v = £f€.', // Réponse multiple
        '£p% signifie qu’à chaque fois que tu commandes 100€, tu dois payer £p€ à la commande.<br>' +
          'Il y a £n fois moins que 100€, donc tu dois payer  £p ÷ £n = £v€ à la commande.<br>' +
          'Le prix total est de £d - £v = £f€.'] // Réponse diviseur
      ],
      // Question de type 4
      [ // Augmentation
        ['Pour faire livrer un article qui vaut £d€, il faut payer £v€.<br>' +
        'Quel pourcentage du prix d’achat doit-on payer  pour faire livrer un article? @1@%.', // Question
        'Trouver le pourcentage revient à chercher quel aurait été le prix de la livraison si l’article avait coûté 100€.<br>' +
          'Combien de fois as-tu payé 100€?', // Aide multiple
        'Trouver le pourcentage revient à chercher quel aurait été le prix de la livraison si l’article avait coûté 100€.<br>' +
          'Tu a payé combien de fois moins que 100€?', // Aide diviseur
        'Trouver le pourcentage revient à chercher quel aurait été le prix de la livraison si l’article avait coûté 100€.<br>' +
          '£d ÷ £n = 100, la livraison aurait donc coûté £v ÷ £n = £p€.<br>' +
          'La livraison coûte £p% du prix d’achat.', // Réponse multiple
        'Trouver le pourcentage revient à chercher quel aurait été le prix de la livraison si l’article avait coûté 100€.<br>' +
          '£d $\\times$ £n = 100, la livraison aurait donc coûté £v $\\times$ £n = £p€.<br>' +
          'La livraison coûte £p% du prix d’achat.'], // Réponse diviseur
        // diminution
        ['Pour commander un article qui vaut £d€, il faut payer £v€ à la commande.<br>' +
        'Quel pourcentage du prix d’achat doit-on payer à la commande? @1@%.', // Question
        'Trouver le pourcentage revient à chercher combien on aurait payé à la commande si l’article avait coûté 100€.<br>' +
          'Combien de fois as-tu commandé 100€?', // Aide multiple
        'Trouver le pourcentage revient à chercher combien on aurait payé à la commande si l’article avait coûté 100€.<br>' +
          'Tu a commandé combien de fois moins que 100€?', // Aide diviseur
        'Trouver le pourcentage revient à chercher combien on aurait payé à la commande si l’article avait coûté 100€.<br>' +
          '£d ÷ £n = 100, il aurait donc fallu payer £v ÷ £n = £p€ à la commande.<br>' +
          'Il faut payer £p% à la commande.', // Réponse multiple
        'Trouver le pourcentage revient à chercher combien on aurait payé à la commande si l’article avait coûté 100€.<br>' +
          '£d $\\times$ £n = 100, il aurait donc fallu payer £v $\\times$ £n = £p€ à la commande.<br>' +
          'Il faut payer £p% à la commande.'] // Réponse diviseur
      ],
      // Question de type 5
      [ // Augmentation
        ['Pour acheter et faire livrer un article, il faut payer £f€. L’article seul coûte £d€.<br>' +
        'Quel pourcentage du prix d’achat doit-on payer pour faire livrer un article? @1@%.', // Question
        'Trouver le pourcentage revient à chercher quel aurait été le prix de la livraison si l’article avait coûté 100€.<br>' +
          'Combien de fois as-tu payé 100€?<br>' +
          'Pense à calculer d’abord le prix de la livraison.', // Aide multiple
        'Trouver le pourcentage revient à chercher quel aurait été le prix de la livraison si l’article avait coûté 100€.<br>' +
          'Tu a payé combien de fois moins que 100€?<br>' +
          'Pense à calculer d’abord le prix de la livraison.', // Aide diviseur
        'Trouver le pourcentage revient à chercher quel aurait été le prix de la livraison si l’article avait coûté 100€.<br>' +
          'Le prix de la livraison est de £f - £d = £v€.<br>' +
          '£d ÷ £n = 100, le prix de la livraison est donc de £v ÷ £n = £p€.<br>' +
          'Le prix de la livraison est de £p% du prix d’achat.', // Réponse multiple
        'Trouver le pourcentage revient à chercher quel aurait été le prix de la livraison si l’article avait coûté 100€.<br>' +
          'Le prix a augmenté de £f - £d = £v€.<br>' +
          '£d $\\times$ £n = 100, il aurait donc augmenté de £v $\\times$ £n = £p€.<br>' +
          'Le prix de la livraison est de £p% du prix d’achat.'], // Réponse diviseur
        // diminution
        ['Pour commander un article, il faut payer une partie à la commande et £f€ à la livraison.<br>' +
        'Quel pourcentage doit-on payer à la commande pour cet article qui coûte £d€? @1@%.', // Question
        'Trouver le pourcentage revient à chercher combien on aurait payé à la commande si l’article avait coûté 100€.<br>' +
          'Combien de fois as-tu commandé 100€?<br>' +
          'Pense à calculer d’abord combien tu as payé à la commande.', // Aide multiple
        'Trouver le pourcentage revient à chercher combien on aurait payé à la commande si l’article avait coûté 100€.<br>' +
          'Tu a commandé combien de fois moins que 100€?<br>' +
          'Pense à calculer d’abord combien tu as payé à la commande.', // Aide diviseur
        'Trouver le pourcentage revient à chercher combien on aurait payé à la commande si l’article avait coûté 100€.<br>' +
          'A la commande, on a payé £d - £f = £v€.<br>' +
          '£d ÷ £n = 100, il aurait donc fallu payer  £v ÷ £n = £p€ à la commande.<br>' +
          'Il faut payer £p% à la commande.', // Réponse multiple
        'Trouver le pourcentage revient à chercher combien on aurait payé à la commande si l’article avait coûté 100€.<br>' +
          'A la commande, on a payé £d - £f = £v€.<br>' +
          '£d $\\times$ £n = 100, il aurait donc fallu payer  £v $\\times$ £n = £p€ à la commande.<br>' +
          'Il faut payer £p% à la commande.'] // Réponse diviseur
      ]
    ]
  ]
}

/**
 * section Pourcentages1
 * @this {Parcours}
 */
export default function main () {
  const stor = this.storage
  const me = this

  // Permet de nommer facilement les conteneurs
  let ds = this.donneesSection

  /// ////////////////////////////////////////////////////////////////////////////////////////
  //
  //                  Données et paramètres de la section
  //
  /// ////////////////////////////////////////////////////////////////////////////////////////

  function Donnees () {
    /*      "presentation1"   6 cadres complets
            "presentation1bis"   manque le cadre indication en bas à droite
            "presentation2"   manque le cadre réponse à droite
            "presentation3"   un seul grand cadre en plus des deux entêtes
    */
    this.structure = 'presentation1'

    // Le titre peut être changé par l’utilisateur dans la page paramètres
    this.titre = 'Calcul mental de pourcentages' //  Titre de la section

    // Si indication != "" alors un lien "Indication" s’affiche dans la zone Inférieure Gauche (this.zones.IG
    // une indication<br>de deux lignes...";
    // Cette indication peut être changée par l’utilisateur dans la page paramètres
    this.indication = 'Une réduction de 40% signifie que chaque fois que tu payes 100€, on te rembourse 40€.'
    // pour le cas de presentation1bis
    // En effet pas d’indication possible pour cette présentation
    // this.surchargeindication = false;

    this.video = [] // ["tableur1,Titre Video 1,Video 1","lebouquet,Titre Video2,Video 2"]

    // Nombre de répétitions de l’exercice
    this.nbrepetitionsmax = 10
    this.nbetapes = 1 // Si chaque question est divisée en plusieurs étapes
    // Nombre de chances dont dispose l’élève pour répondre à la question
    this.nbchances = 2
    // Par défaut, pas de temps limite.
    this.limite = 0

    this.b_enonce1 = true
    this.b_enonce2 = true
    this.b_enonce3 = true
    this.b_enonce4 = true
    this.b_type1 = true
    this.b_type2 = true
    this.b_type3 = true
    this.b_type4 = true
    this.b_type5 = true
    this.l_niveau = 'Calcul mental simple avec des entiers (x5, x9, x11, x25).'
    this.b_multiple = true
    this.b_diviseur = true

    this.b_aide = true
    /*
    Phrase d’état renvoyée par la section
    */
    this.pe_1 = 'Notion totalement maitrisée.'
    this.pe_2 = 'Notion comprise.'
    this.pe_3 = 'Notion fragile.'
    this.pe_4 = 'Notion non comprise.'
    this.pe_5 = 'Oubli de calcul de la valeur finale.'

    /// ////////////////////////////////////////////////////////////////////////////////////////
    //
    //               Ajouter ici toutes les constantes nécessaires à la section
    //
    /// ////////////////////////////////////////////////////////////////////////////////////////
  }

  /// ////////////////////////////////////////////////////////////////////////////////////////
  //
  //              Ajouter ici toutes les fonctions nécessaires à la section
  //
  /// ////////////////////////////////////////////////////////////////////////////////////////

  //  Transforme une chaine d’intervalles en un tableau de valeurs
  stor.intervallesToTableau = function intervallesToTableau (intervalles) {
    const valeurs = []
    const tableau = intervalles.match(/\d*-\d*|\d*/g)
    for (let i = 0; i < tableau.length; i++) {
      if (tableau[i].length != 0) {
        if (tableau[i].indexOf('-') == -1) {
          valeurs.push(parseFloat(tableau[i]))
        } else {
          for (j = parseFloat(tableau[i].substring(0, tableau[i].indexOf('-'))); j <= tableau[i].substring(tableau[i].indexOf('-') + 1); j++) { valeurs.push(j) }
        }
      }
    }
    return valeurs
  } // intervalles_to_tab

  stor.choixValeurDepart = function choixValeurDepart (codeDepart) {
    switch (codeDepart) {
      case 1: return 200
      case 2: return j3pGetRandomInt(2, 9) * 100
      case 3: return 50
      case 4: return [25, 20, 10][j3pGetRandomInt(0, 2)]
      case 5: return [5, 4, 2, 1][j3pGetRandomInt(0, 3)]
      default: console.error(Error(`codeDepart ${codeDepart} non géré`))
    }
  } // choixValeurDepart

  function choixPourcentage (depart) {
    const niveauAdapte = ((ds.n_niveau == 3 && !textes.enonces[stor.enonceQuestion - 1][0]) ? 2 : ds.n_niveau) //  Si l’énoncé n’est pas adapté aux nombres décimaux, on prend le niveau 2
    switch (depart) {
      case 200:
        switch (niveauAdapte) {
          case 1: {
            if (j3pGetRandomInt(1, 4) == 1) {
              return j3pGetRandomInt(2, 9)
            }
            //  Calcul mental simple
            return j3pGetRandomInt(2, 19) * 5
          }
          case 2: {
            switch (j3pGetRandomInt(2, 4)) { //  Astuces de calcul mental
              case 2:
                return j3pGetRandomInt(1, 9) * 10 - 1
              case 3:
                return j3pGetRandomInt(1, 9) * 10 - 2
              case 4:
                return j3pGetRandomInt(1, 9) * 11
            }
            break
          }
          case 3: {
            switch (j3pGetRandomInt(1, 7)) { //  Astuces de calcul mental
              case 5:
              case 6:
              case 7:
              case 1:
                return j3pGetRandomInt(2, 190) * 5 / (j3pGetRandomInt(1, 3) == 1 ? 100 : 10)
              case 2:
                return (j3pGetRandomInt(1, 9) * 10 - 1) / (j3pGetRandomInt(1, 3) == 1 ? 100 : 10)
              case 3:
                return (j3pGetRandomInt(1, 9) * 10 - 2) / (j3pGetRandomInt(1, 3) == 1 ? 100 : 10)
              case 4:
                return (j3pGetRandomInt(1, 9) * 11) / (j3pGetRandomInt(1, 3) == 1 ? 100 : 10)
            }
            break
          }
        }
        break // case 200

      case 300:
      case 500:
      case 700:
      case 900:
      case 400:
      case 600:
      case 800:
        switch (niveauAdapte) {
          case 1:
            if (j3pGetRandomInt(1, 4) == 1) {
              return j3pGetRandomInt(2, 9)
            }
            //  Calcul mental simple
            return j3pGetRandomInt(2, 9) * 10
          case 2:
            switch (j3pGetRandomInt(1, 4)) { //  Astuces de calcul mental
              case 1: return j3pGetRandomInt(0, 9) * 10 + 5
              case 2: return j3pGetRandomInt(1, 9) * 10 - 1
              case 3: return j3pGetRandomInt(1, 9) * 10 - 2
              case 4: return j3pGetRandomInt(1, 9) * 11
            }
            break

          case 3: {
            switch (j3pGetRandomInt(1, 4)) { //  Astuces de calcul mental
              case 1:
                return j3pGetRandomInt(2, 19) * 5 / (j3pGetRandomInt(1, 3) == 1 ? 100 : 10)
              case 2:
                return (j3pGetRandomInt(1, 9) * 10 - 1) / (j3pGetRandomInt(1, 3) == 1 ? 100 : 10)
              case 3:
                return (j3pGetRandomInt(1, 9) * 10 - 2) / (j3pGetRandomInt(1, 3) == 1 ? 100 : 10)
              case 4:
                return (j3pGetRandomInt(1, 9) * 11) / (j3pGetRandomInt(1, 3) == 1 ? 100 : 10)
            }
            break
          }
        }
        break
      case 50:
        switch (niveauAdapte) {
          case 1: return j3pGetRandomInt(1, 4) * 20 + j3pGetRandomInt(1, 4) * 2
          case 2: return j3pGetRandomInt(1, 49) * 2
          case 3:
            switch (j3pGetRandomInt(1, 3)) { //  Astuces de calcul mental
              case 1:
                return j3pGetRandomInt(1, 99)
              case 2:
                return j3pGetRandomInt(1, 99) / 10
              case 3:
                return j3pGetRandomInt(1, 49) * 2 / 100
            }
            break
        }
        break // case 50

      case 25:
        switch (niveauAdapte) {
          case 1: return j3pGetRandomInt(1, 10) * 4
          case 2: return j3pGetRandomInt(1, 24) * 4
          case 3:
            switch (j3pGetRandomInt(1, 3)) { //  Astuces de calcul mental
              case 1: return j3pGetRandomInt(1, 99)
              case 2: return j3pGetRandomInt(1, 99) * 2 / 10
              case 3: return j3pGetRandomInt(1, 24) * 4 / 100
            }
            break
        }
        break // case 25

      case 20:
        switch (niveauAdapte) {
          case 1: return j3pGetRandomInt(1, 10) * 5
          case 2: return j3pGetRandomInt(1, 19) * 5
          case 3:
            switch (j3pGetRandomInt(1, 3)) { //  Astuces de calcul mental
              case 1: return j3pGetRandomInt(1, 99)
              case 2: return j3pGetRandomInt(1, 99) / 10
              case 3: return j3pGetRandomInt(1, 19) * 5 / 100
            }
            break
        }
        break // case 20

      case 10:
        switch (niveauAdapte) {
          case 1:
          case 2:
            return j3pGetRandomInt(1, 9) * 10
          case 3:
            switch (j3pGetRandomInt(1, 2)) { //  Astuces de calcul mental
              case 1: return j3pGetRandomInt(1, 99)
              case 2: return j3pGetRandomInt(1, 99) / 10
            }
            break
        }
        break // case 10

      case 5:
        switch (j3pGetRandomInt(1, 2)) {
          case 1: return j3pGetRandomInt(1, 50) * 20 / 100
          case 2: return j3pGetRandomInt(5, 49) * 20 / 10
        }
        break

      case 4:
        switch (j3pGetRandomInt(1, 2)) {
          case 1: return j3pGetRandomInt(1, 40) * 25 / 100
          case 2: return j3pGetRandomInt(4, 39) * 25 / 10
        }
        break

      case 2:
        switch (j3pGetRandomInt(1, 2)) {
          case 1: return j3pGetRandomInt(1, 20) * 50 / 100
          case 2: return j3pGetRandomInt(2, 19) * 50 / 10
        }
        break

      case 1: return j3pGetRandomInt(1, 99)
    } // switch depart
  } //  choixPourcentage

  //   Affiche la correction sous l’énoncé
  function afficheCorrection (fin) {
    // fonction appelée pour afficher la correction (que la réponse soit bonne ou fausse

    const reponseEleve = parseFloat(j3pValeurde(idPrefix + 'consigne1input1').replace(/ /g, '').replace(',', '.')) //  Réponse utilisable dans les calculs

    j3pElement(idPrefix + 'explication1').innerHTML = '' //  Pour effacer  un éventuel précédent affichage
    j3pElement(idPrefix + 'explication2').innerHTML = '' //  Pour effacer  un éventuel précédent affichage

    if (!stor.reponse.bonneReponse) {
      if (fin) { //  On affiche la bonne réponse s’il n’a pas trouvé au dernier essai
        j3pAffiche(idPrefix + 'explication1', idPrefix + 'corr11', textes.enonces[stor.enonceQuestion - 1][stor.typeQuestion][stor.codeQuestion[2]][3 + (stor.codeQuestion[3] >= 3 ? 1 : 0)],
          {
            style: me.styles.moyen.correction,
            d: j3pNombreBienEcrit(stor.valeurDepart),
            f: j3pNombreBienEcrit(stor.valeurFinale),
            v: j3pNombreBienEcrit(stor.variation),
            p: j3pNombreBienEcrit(stor.pourcentage),
            n: j3pNombreBienEcrit(stor.ratio)
          })
      } else {
        if (ds.b_aide && stor.reponse.aRepondu) {
          j3pDiv(idPrefix + 'explication2', idPrefix + 'corr21', '')
          j3pAffiche(idPrefix + 'corr21', idPrefix + 'corr210', textes.enonces[stor.enonceQuestion - 1][stor.typeQuestion][stor.codeQuestion[2]][1 + (stor.codeQuestion[3] >= 3 ? 1 : 0)],
            {
              d: j3pNombreBienEcrit(stor.valeurDepart),
              f: j3pNombreBienEcrit(stor.valeurFinale),
              v: j3pNombreBienEcrit(stor.variation),
              p: j3pNombreBienEcrit(stor.pourcentage),
              n: j3pNombreBienEcrit(stor.ratio)
            })
        }
        // Aide
      }
      // Pas fini
      if (reponseEleve == stor.variation) me.typederreurs[3]++ //  Compte les fois où il a oublié de calculer la valeur finale
    }
    // Réponse fausse
  } //  affiche_correction

  // Affiche le nombre de tentatives qu’il reste s’il y a plusieurs chances
  stor.afficheChances = function afficheChances () {
    j3pElement(idPrefix + 'zone_chances').innerHTML = ''
    if (ds.nbchances > 1) {
      j3pAffiche(idPrefix + 'zone_chances', idPrefix + 'chances', textes.phraseNbChances,
        {
          style: me.styles.moyen.enonce,
          e: ds.nbchances - me.essaiCourant,
          p: (ds.nbchances - me.essaiCourant > 1 ? 's' : '')
        })
    }
    // Pour le pluriel
  }

  // Ajoute les fonctions de contrôle à un input
  stor.ajouteControle = function ajouteControle (input) {
    // Remplace le point par la virgule
    j3pElement(input).addEventListener('input', j3pRemplacePoint)
    //  Empêche qu’il y ait deux virgules dans le nombre
    j3pElement(input).addEventListener('keydown', function pasDeuxVirgules (event) {
      if ((event.key == ',') || (event.key == '.')) if ((j3pValeurde(input).includes(',')) || (j3pValeurde(input).includes('.'))) event.preventDefault()
    }, false)
  } // ajouteControle

  //  Les variables des travail de la section
  let i, j, n

  /// ////////////////////////////////////////////////////////////////////////////////////////
  //
  //              Déroulement de la section Enonce, Correction, navigation
  //
  /// ////////////////////////////////////////////////////////////////////////////////////////

  switch (this.etat) {
    /// ////////////////////////////////////////////////////////////////////////////////////////
    //
    //            Phase énoncé
    //
    /// ////////////////////////////////////////////////////////////////////////////////////////

    case 'enonce': {
      //  Construit l’énoncé: exécuté au début de chaque question
      if (this.debutDeLaSection) {
      //  Code exécuté uniquement au lancement de la section
        //  Partie à ne pas changer
        // Construit les données de la section
        this.donneesSection = new Donnees()

        // Permet de nommer facilement les conteneurs (au lancement de la section donneesSection n’existe pas

        this.surcharge()
        ds = this.donneesSection

        // Construction de la page
        this.construitStructurePage(ds.structure)

        ds.nbrepetitions = ds.nbrepetitionsmax // C’est cette valeur qui est affichée dans l’entête
        ds.nbitems = ds.nbetapes * ds.nbrepetitionsmax // Initialisation à ne pas modifier

        this.score = 0

        this.afficheTitre(ds.titre)

        // Affiche l’indication en bas à gauche
        if (ds.indication !== '') this.indication(this.zones.IG, ds.indication)

        //  Paramètres à renseigner
        /*
           Par convention,`
          `   this.typederreurs[0] = nombre de bonnes réponses
              this.typederreurs[1] = nombre de fois où l’élève a utilisé une chance supplémentaire
              this.typederreurs[2] = nombre de mauvaises réponses
              this.typederreurs[3] = oubli de calcul de la valeur finale
              this.typederreurs[10] = nombre de fois où l’élève a eu faux à cause d’un temps limite
              LES AUTRES peuvent être utilisées par la détermination de la pe dans le cas d’une section qualitative
           */
        this.typederreurs = [0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0]

        //  Paramètres et variables particuliers à la section
        stor.t_enonce = []
        if (ds.b_enonce1) stor.t_enonce.push(1)
        if (ds.b_enonce2) stor.t_enonce.push(2)
        if (ds.b_enonce3) stor.t_enonce.push(3)
        if (ds.b_enonce4) stor.t_enonce.push(4)

        stor.t_type = []
        if (ds.b_type1) stor.t_type.push(1)
        if (ds.b_type2) stor.t_type.push(2)
        if (ds.b_type3) stor.t_type.push(3)
        if (ds.b_type4) stor.t_type.push(4)
        if (ds.b_type5) stor.t_type.push(5)

        ds.n_niveau = ['Aucune difficulté de calcul mental.', 'Calcul mental simple avec des entiers (x5, x9, x11, x25).', 'Calcul mental simple avec des décimaux.'].indexOf(ds.l_niveau) + 1

        stor.rangType = 1
        if (ds.b_multiple) {
          stor.rangMultiple = 1
        } else {
          stor.rangMultiple = 3
        }// Si les multiples sont autorisés 200, sinon 50

        //  Teste la validité des paramètres
        if (stor.t_enonce.length == 0 || stor.t_type.length == 0 || (!ds.b_multiple && !ds.b_diviseur)) {
          j3pShowError('Il n’y a aucune question avec ces paramètres.')
        }

        //  Tableau permettant de ne pas poser 2 fois la même question
        // Une question est définie par (type de question,énoncé,augmentation,multiple)
        // multiple vaut 1: 200; 2: de 200 à 900; 3: 50; 4: 25,  20, 10; 5: 5, 4, 2, 1
        stor.questions = [] // Contient les question qui n’ont pas encore été posées
        for (i = 0; i <= stor.t_enonce.length - 1; i++) {
          stor.questions.push([stor.t_type[0], // le premier type de la liste
            stor.t_enonce[i], // tous les énoncés
            0, // Augmentation
            stor.rangMultiple])
          stor.questions.push([stor.t_type[0], // le premier type de la liste
            stor.t_enonce[i], // tous les énoncés
            1, // Diminution
            stor.rangMultiple])
        }
        stor.erreurs = [] // Contient questions qui ont provoqué une erreur pour les représenter en priorité
        stor.repeter = false // Bascule permettant de prendre alternativement des questions nouvelles et des anciennes erreurs
      } else {
        // A chaque répétition, on nettoie la scène
        this.videLesZones()
      }
      //  Partie exécutée à chaque essai
      /// ////////////////////////////////////////////////////////////////////////////////////////
      //
      //               Création de l’énoncé
      //
      /// ////////////////////////////////////////////////////////////////////////////////////////

      //  Création de l’énoncé
      // codeQuestion contient les informations permettant de construire la question
      if (stor.repeter && stor.erreurs.length != 0) { // On choisit la question une fois sur deux dans les erreurs passées
        n = j3pGetRandomInt(0, stor.erreurs.length - 1) // La question est choisie parmi les erreurs passées
        stor.codeQuestion = stor.erreurs[n]
        stor.erreurs.splice(n, 1) // On la retire des questions à poser
        stor.repeter = false // La prochaine fois on prendra une nouvelle question
      } else {
        n = j3pGetRandomInt(0, stor.questions.length - 1) // La question est choisie parmi celles qui n’ont pas encore été posées
        stor.codeQuestion = stor.questions[n]
        stor.questions.splice(n, 1) // On la retire des questions à poser
        stor.repeter = true
      }
      stor.typeQuestion = stor.codeQuestion[0]
      stor.enonceQuestion = stor.codeQuestion[1]
      stor.valeurDepart = stor.choixValeurDepart(stor.codeQuestion[3])
      stor.ratio = (stor.codeQuestion[3] >= 3 ? Math.round(100 / stor.valeurDepart) : Math.round(stor.valeurDepart / 100))
      stor.pourcentage = choixPourcentage(stor.valeurDepart)
      stor.variation = stor.valeurDepart * stor.pourcentage / 100
      stor.valeurFinale = (stor.codeQuestion[2] == 0 ? stor.valeurDepart + stor.variation : stor.valeurDepart - stor.variation)

      let f_laBonneReponse
      switch (stor.typeQuestion) { // La réponse dépend du type de question
        case 1:
        case 2: {
          f_laBonneReponse = stor.variation
          break
        }
        case 3: {
          f_laBonneReponse = stor.valeurFinale
          break
        }
        case 4:
        case 5: {
          f_laBonneReponse = stor.pourcentage
          break
        }
      }
      stor.laBonneReponse = f_laBonneReponse

      //  Création des conteneurs
      j3pDiv(this.zones.MG, {
        id: idPrefix + 'consignes',
        contenu: '',
        style: me.styles.etendre('moyen.enonce', { padding: '10px', marginTop: '25px' })
      })

      // Dans ce conteneur, on crée un div par phrase (cela forcera le retour à la ligne)
      j3pDiv(idPrefix + 'consignes', idPrefix + 'zone_consigne1', '')

      // Affiche le nombre de chances qu’il reste
      j3pDiv(idPrefix + 'consignes', {
        id: idPrefix + 'zone_chances',
        contenu: '',
        style: me.styles.etendre('moyen.enonce', { marginTop: '30px' })
      })
      stor.afficheChances()

      // Si on veut rajouter des explications, en cas de réponse fausse par exemple
      j3pDiv(this.zones.MG, {
        id: idPrefix + 'explications',
        contenu: '',
        style: me.styles.etendre('moyen.explications', { padding: '10px', marginTop: '15px' })
      })
      j3pDiv(idPrefix + 'explications', idPrefix + 'explication1', '') //  Contient la correction
      j3pDiv(idPrefix + 'explications', idPrefix + 'explication2', '')

      // On crée ici le div qui contiendra toutes les remarques sur ce qu’on pense de la réponse de l’élève (le plus souvant dans le cadre MD)
      j3pDiv(this.zones.MD, {
        id: idPrefix + 'correction',
        contenu: '',
        coord: [0, 0],
        style: me.styles.etendre('petit.correction', { padding: '10px' })
      })

      //  Initialisation des zones de saisie
      {
        j3pAffiche(idPrefix + 'zone_consigne1', idPrefix + 'consigne1', textes.enonces[stor.enonceQuestion - 1][stor.typeQuestion][stor.codeQuestion[2]][0],
          {
            style: me.styles.moyen.enonce,
            d: j3pNombreBienEcrit(stor.valeurDepart),
            f: j3pNombreBienEcrit(stor.valeurFinale),
            v: j3pNombreBienEcrit(stor.variation),
            p: j3pNombreBienEcrit(stor.pourcentage),
            input1: { dynamique: true, width: '12px' }
          })

        // Le nom de chaque zone de saisie est construit de la même façon pour les zones input et inputmq :
        // le nom input est toujours précédé du nom du span qui contient la zone (ce span est le deuxième paramètre de la fonction j3pAffiche

        // On rajoute les contraintes de saisie
        j3pRestriction(idPrefix + 'consigne1input1', '0-9., ') // on n’autorise que les chiffres

        //  Améliore la gestion de la virgule
        stor.ajouteControle(idPrefix + 'consigne1input1')

        //  Limite la zone de saisie
        j3pElement(idPrefix + 'consigne1input1').maxLength = 10

        // On rajoute toutes les informations qui permettront de valider la saisie
        j3pElement(idPrefix + 'consigne1input1').typeReponse = ['nombre', 'exact']
        j3pElement(idPrefix + 'consigne1input1').reponse = stor.laBonneReponse

        // Paramétrage de la validation
        // mes_zones_saisie est un tableau contenant le nom de chacune des zones (on pourrait aussi y mettre une liste déroulante)
        const mes_zones_saisie = [idPrefix + 'consigne1input1']
        // validePerso est souvant un tableau vide
        // Mais parfois certaines zones ne peuvent pas être validée de manière automatique
        // Donc la validation de ces zones devra se faire "à la main".
        // Par contre on vérifiera si elles sont remplies ou non.
        const mes_zones_valide_perso = []

        // fcts_valid est un objet permettant la validation. Il contient le nom des zones (tableau précédent) et un deuxième paramètre validePerso
        stor.fcts_valid = new ValidationZones({ parcours: me, zones: mes_zones_saisie, validePerso: mes_zones_valide_perso })
      } //  Initialisation des zones de saisie

      j3pFocus(idPrefix + 'consigne1input1')

      this.finEnonce() // Obligatoire
      break
    } // case "enonce":

    /// ////////////////////////////////////////////////////////////////////////////////////////
    //
    //            Phase correction
    //
    /// ////////////////////////////////////////////////////////////////////////////////////////

    case 'correction': {
      const fcts_valid = stor.fcts_valid // ici je crée juste une variable pour raccourcir le nom de l’object fcts_valid

      //  Efface le nombre de chances qui restent
      j3pElement(idPrefix + 'zone_chances').innerHTML = ''

      //  Efface les éléments de correction
      j3pElement(idPrefix + 'explication1').innerHTML = ''
      j3pElement(idPrefix + 'explication2').innerHTML = ''

      j3pElement(idPrefix + 'consigne1input1').value = j3pNombreBienEcrit(j3pValeurde(idPrefix + 'consigne1input1')) // Remet l’écriture correcte de la réponse

      // ce qui suit sert pour la validation de toutes les zones
      // le tableau contenant toutes les zones de saisie
      stor.reponse = fcts_valid.validationGlobale() // Ne pas modifier
      // A cet instant reponse contient deux éléments :
      // reponse.aRepondu qui me dit si toutes les zones ont bien été complétées ou non
      // reponse.bonneReponse qui me dit si les zones qui ne sont pas dans le tableau valide_pero ont des réponses correctes ou non.
      // Si reponse.aRepondu vaut true, la fonction met aussi en bleu ou rouge les réponses bonnes ou fausses.
      // Si c’est la dernière tentative de répondre, on barre également les zones fausses

      // Réponse vide ou incomplète et la limite de temps n’est pas atteinte
      if ((!stor.reponse.aRepondu) && (!this.isElapsed)) {
        this.reponseManquante(idPrefix + 'correction')
        stor.afficheChances() //  affiche le nombre de chances qui restent
      } else {
      // Une réponse a été saisie
        if (stor.reponse.bonneReponse) {
        // Bonne réponse
          this.score++
          j3pElement(idPrefix + 'correction').style.color = this.styles.cbien
          j3pElement(idPrefix + 'correction').innerHTML = cBien

          // Même si la réponse est bonne on peut afficher des éléments de correction/explication :
          // Pas utilisé dans cette section  afficheCorrection(true);

          // Si toutes les erreurs ont été corrigées
          // Augmente le niveau de difficulté
          if (stor.erreurs.length == 0) {
            if (stor.rangType < stor.t_type.length) { // On introduit d’abourd tous les types de question
              stor.rangType++
              for (i = 0; i <= stor.t_enonce.length - 1; i++) {
                stor.questions.push([stor.t_type[stor.rangType - 1], // le premier type de la liste
                  stor.t_enonce[i], // tous les énoncés
                  0, // Augmentation
                  (ds.b_multiple ? 1 : 3)]) // Si les multiples sont autorisés 200, sinon 50
                stor.questions.push([stor.t_type[stor.rangType - 1], // le premier type de la liste
                  stor.t_enonce[i], // tous les énoncés
                  1, // Diminution
                  (ds.b_multiple ? 1 : 3)]) // Si les multiples sont autorisés 200, sinon 50
              }
            } else if ((ds.b_multiple && stor.rangMultiple < 2) || (ds.b_diviseur && stor.rangMultiple < 4) || (ds.b_diviseur && (ds.n_niveau == 3) && stor.rangMultiple < 5)) { // On augmente la difficulté de la valeur de départ
              stor.rangMultiple++
              for (i = 0; i <= stor.t_enonce.length - 1; i++) {
                for (j = 0; j <= stor.t_type.length - 1; j++) {
                  if (stor.rangMultiple != 5 || (textes.enonces[stor.t_enonce[i] - 1][0] && ds.n_niveau == 3)) {
                    stor.questions.push([stor.t_type[j],
                      stor.t_enonce[i], // tous les énoncés
                      0, // Augmentation
                      stor.rangMultiple]) // Si les multiples sont autorisés 200, sinon 50
                    stor.questions.push([stor.t_type[j],
                      stor.t_enonce[i], // tous les énoncés
                      1, // Diminution
                      stor.rangMultiple]) // Si les multiples sont autorisés 200, sinon 50
                  }
                }
              }
            }
            ; // Il reste des niveaux
            if (stor.rangType > 1 && stor.rangMultiple >= 4) stor.questions = stor.questions.filter(function supprimeQuestion (element) { return element[0] != 1 && element[3] >= 2 })// On supprime les questions les plus faciles
          }
          ; // Augmenter la difficulté

          this.typederreurs[0]++ //  Compte les bonnes réponses
          this.cacheBoutonValider() //  Plus de réponse à donner
          this.etat = 'navigation'
          this.sectionCourante() //  Passe à la question suivante
        } else {
          // Mauvaise réponse
          if (!stor.erreurs.includes(stor.codeQuestion)) {
            stor.questions.push(stor.codeQuestion) // On reposera la question
            stor.erreurs.push(stor.codeQuestion)
          }
          // Mémorise l’erreur
          stor.repeter = false // On ne repose pas la question au coup d’après

          j3pElement(idPrefix + 'correction').style.color = this.styles.cfaux // Commentaires à droite en rouge

          // Fini à cause de la limite de temps
          if (this.isElapsed) {
            j3pElement(idPrefix + 'correction').innerHTML = tempsDepasse // Commentaires à droite

            j3pBarre(idPrefix + 'consigne1input1')

            this.typederreurs[10]++ //  Compte les temps dépassés
            this.cacheBoutonValider() //  Plus de réponse à donner

            // L’élève avait un temps limite mais ne l’a pas respecté. On affiche alors la correction
            afficheCorrection(true)

            this.etat = 'navigation'
            this.sectionCourante() //  Passe à la question suivante
          } else {
          // Réponse fausse
            j3pElement(idPrefix + 'correction').innerHTML = cFaux // Commentaires à droite en rouge

            // Il reste des essais
            if (this.essaiCourant < ds.nbchances) {
              j3pElement(idPrefix + 'correction').innerHTML += '<br>' + essaieEncore // Commentaires à droite

              this.typederreurs[1]++ //  Compte les essais ratés

              stor.afficheChances() //  affiche le nombre de chances qui restent

              // indication éventuelle ici
              afficheCorrection(false)
            } else {
            // Il ne reste plus d’essais
              j3pElement(idPrefix + 'correction').innerHTML += '<br>' + regardeCorrection // Commentaires à droite
              //
              // Là il ne peut plus se corriger. On lui affiche alors la solution
              afficheCorrection(true)

              this.typederreurs[2]++ //  Compte les réponses fausses

              this.etat = 'navigation'
              this.sectionCourante() //  Passe à la question suivante
            } // Il ne reste plus d’essais
          } // Réponse fausse
        } // Mauvaise réponse
      } // Une réponse a été saisie

      this.finCorrection()
      break
    }
    // case "correction":

    /// ////////////////////////////////////////////////////////////////////////////////////////
    //
    //            Phase correction
    //
    /// ////////////////////////////////////////////////////////////////////////////////////////

    case 'navigation':
      //  Si on a posé toutes les questions, on rajoute les erreurs et on continue
      if (stor.questions.length == 0) stor.questions = stor.questions.concat(stor.erreurs)

      //    Arrête les questions si toutes les réponses sont justes
      if (stor.questions.length == 0) ds.nbitems = me.questionCourante

      if (this.sectionTerminee()) {
        // On détermine la phrase d’état renvoyée par la section
        if ((this.typederreurs[1] == 0) && (this.typederreurs[2] == 0) && (this.typederreurs[10] == 0)) {
          //  Aucune erreur
          if (ds.limite != 0) {
            //  Aucune erreur en temps limité: Notion totalement maitrisée
            this.parcours.pe = ds.pe_1
          } else {
            this.parcours.pe = ds.pe_2
          }
        } else if (
          ((this.typederreurs[2] == 0) && (this.typederreurs[10] == 0)) || //  A pu corriger ses erreurs
          ((ds.nbchances <= 1) && ((this.typederreurs[2] + this.typederreurs[10]) <= 1)) // ou une seule erreur s’il n’y a pas plusieurs chances
        ) {
          this.parcours.pe = ds.pe_3
        } else if (this.typederreurs[3] > 1) {
        //  Notion fragile
          this.parcours.pe = ds.pe_5
        } else {
          this.parcours.pe = ds.pe_4
        } //  Notion non comprise si on n’a pas trouve au moins une réponse lorsqu’il y a plusieurs essais
        //     ou si on n’a pas trouve au moins deux réponses lorsqu’il y a plusieurs essais

        this.afficheBoutonSectionSuivante()
        this.focus('sectioncontinuer')
      } else {
        this.etat = 'enonce'
        this.afficheBoutonSuite()
        this.focus('boutoncontinuer')
      }
      // Obligatoire
      this.finNavigation()

      break // case "navigation":
  }
  ; //  switch etat
} //  SectionPosition
