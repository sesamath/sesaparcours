import { j3pBarre, j3pDiv, j3pElement, j3pFocus, j3pGetRandomInt, j3pValeurde } from 'src/legacy/core/functions'
import ValidationZones from 'src/legacy/outils/calculatrice/ValidationZones'
import { j3pAffiche, j3pDesactive } from 'src/lib/mathquill/functions'
import textesGeneriques from 'src/lib/core/textes'

const { cBien, cFaux, essaieEncore, regardeCorrection, tempsDepasse } = textesGeneriques

/*
        Auteur  Xavier JEROME
        Date    19/07/2018
        Lien entre les multiples et sous-multiples et la grandeur de référence
 */

// Phrases d’état renvoyée par la section
const pe1 = 'Notion totalement maitrisée'
const pe2 = 'Notion comprise'
const pe5 = 'Notion fragile.'
const pe6 = 'Notion non comprise.'

export const params = {

  outils: ['mathquill'],

  /**
   * Ce qui est paramétrable dans l’éditeur de graphe
   * @type {LegacySectionParamsParametres}
   */
  parametres: [
    ['indication', 'Imagine deux objets qui mesurent les longueurs demandées.', 'string', 'Indication en bas à gauche'], // Texte que l’élève peut faire apparaître en bas de l’écran
    ['nbrepetitionsmax', 10, 'entier', 'Nombre de répétitions maximum de la section.<BR>Il peut être inférieur si on répond juste à toutes les questions'], // Obligatoire: nombre de questions posées pendant l’exercice
    ['nbchances', 2, 'entier', 'Nombre d’essais par répétition'], // Nombre d’essais autorisés avant de déclarer un échec et de passer à la question
    ['limite', 0, 'entier', 'Temps disponible par répétition, en secondes'], // S’il n’est pas vide, le temps de réponse sera limité
    ['n_niveau', 1, 'entier', '1 pour uniquement les m<BR>2 pour m et g<br>3 pour m, g, L et s'], // Niveau de difficulté
    ['n_aide', 1, 'entier', '0 pour aucune aide<BR>1 pour une explication des erreurs'] // Niveau d’aide autorisé
  ],

  // nos constantes sont camelCase mais on ne touche pas aux propriétés pour ne pas casser les graphes déjà faits
  pe: [
    { pe_1: pe1 },
    { pe_2: pe2 },
    { pe_5: pe5 },
    { pe_6: pe6 }
  ]
}

const textes = {
  nom_multiple: ['m', 'c', 'd', '', 'da', 'h', 'k'],
  nom_unite: ['m', 'g', 'L', 's'],
  nom_quantite: ['', '1', '10', '100', '1000'],
  phraseTitre: 'Compare chaque multiple ou sous-multiple à l’unité de base.', //  Titre de la section
  phraseNbChances: 'Il te reste £e essai£p.', //  Ne pas supprimer
  phraseEnonce1: '1 £m£u est égal à #1# £u',
  phraseEnonce2: '1 #1# est égal à £a £u',
  phraseEnonce3: '1 £u est égal à #1# £m£u',
  phraseEnonce4: '1 £u est égal à £a #1#',
  phraseSolution1: '1 £m£u est égal à £a £n£u',
  Aide01: 'Attention: c’est 1 £v qui est égal à £a £u',
  Aide02: 'Attention: c’est £a £v qui est égal à 1 £u'
}

const cssPrefix = 'Conversion1'

/**
 * section Conversion1
 * @this {Parcours}
 */
export default function main () {
  const me = this
  const stor = this.storage
  const ds = this.donneesSection

  let n, i, j

  /// ////////////////////////////////////////////////////////////////////////////////////////
  //
  //              Ajouter ici toutes les fonctions nécessaires à la section
  //
  /// ////////////////////////////////////////////////////////////////////////////////////////

  //   Affiche la correction sous l’énoncé
  function afficheCorrection (fin) {
    // fonction appelée pour afficher la correction (que la réponse soit bonne ou fausse
    j3pElement(cssPrefix + 'explication1').innerHTML = '' //  Pour effacer  un éventuel précédent affichage
    if (fin && !stor.reponse.bonneReponse) {
      //  On affiche la bonne réponse s’il n’a pas trouvé au dernier essai
      j3pAffiche(cssPrefix + 'explication1', cssPrefix + 'corr11', textes.phraseSolution1,
        {
          styletexte: '',
          m: (((stor.type == 1) || (stor.type == 2)) ? textes.nom_multiple[stor.nom_multiple - 1] : ''),
          u: textes.nom_unite[stor.unite - 1],
          a: textes.nom_quantite[stor.quantite],
          n: (((stor.type == 1) || (stor.type == 2)) ? '' : textes.nom_multiple[stor.nom_multiple - 1])
        })
    }

    if (stor.reponse.aRepondu && me.donneesSection.n_aide == 1) {
      j3pElement(cssPrefix + 'explication2').innerHTML = '' //  Pour effacer  un éventuel précédent affichage

      let repEleve1

      switch (stor.type) {
        case 1: {
          repEleve1 = textes.nom_quantite.indexOf(j3pValeurde(cssPrefix + 'consigne1liste1')) //  Récupère la réponse
          if (repEleve1 != 1 && !(repEleve1 == 4 && stor.unite == 3)) {
            /* On ne corrige pas 1000L=1hL */
            j3pAffiche(cssPrefix + 'explication2', cssPrefix + 'corr11', textes.Aide01,
              {
                styletexte: '',
                u: textes.nom_unite[stor.unite - 1],
                v: textes.nom_multiple[stor.nom_multiple - 1 + repEleve1 - stor.quantite] + textes.nom_unite[stor.unite - 1],
                a: j3pValeurde(cssPrefix + 'consigne1liste1')
              })
          }
          break } // Type 1
        case 2: {
          repEleve1 = me.choix_unite.indexOf(j3pValeurde(cssPrefix + 'consigne1liste1')) //  Récupère la réponse
          if (repEleve1 != 4) {
            if (repEleve1 > 4) {
              j3pAffiche(cssPrefix + 'explication2', cssPrefix + 'corr11', textes.Aide01,
                {
                  styletexte: '',
                  u: textes.nom_unite[stor.unite - 1],
                  v: j3pValeurde(cssPrefix + 'consigne1liste1'),
                  a: textes.nom_quantite[stor.quantite + repEleve1 - stor.nom_multiple]
                })
            } else {
              j3pAffiche(cssPrefix + 'explication2', cssPrefix + 'corr11', textes.Aide02,
                {
                  styletexte: '',
                  u: textes.nom_unite[stor.unite - 1],
                  a: textes.nom_quantite[5 - repEleve1],
                  v: j3pValeurde(cssPrefix + 'consigne1liste1')
                })
            }
          }
          break } // Type 2
        case 3: {
          repEleve1 = textes.nom_quantite.indexOf(j3pValeurde(cssPrefix + 'consigne1liste1')) //  Récupère la réponse
          if (repEleve1 != 1 && stor.unite != 4) {
            /* On ne corrige pas 1s = 10 ms */
            j3pAffiche(cssPrefix + 'explication2', cssPrefix + 'corr11', textes.Aide02,
              {
                styletexte: '',
                u: textes.nom_unite[stor.unite - 1],
                a: j3pValeurde(cssPrefix + 'consigne1liste1'),
                v: textes.nom_multiple[stor.nom_multiple - 1 - repEleve1 + stor.quantite] + textes.nom_unite[stor.unite - 1]
              })
          }
          break
        } // Type 3
        case 4: {
          repEleve1 = me.choix_unite.indexOf(j3pValeurde(cssPrefix + 'consigne1liste1')) //  Récupère la réponse
          if (repEleve1 != 4) {
            if (repEleve1 < 4) {
              j3pAffiche(cssPrefix + 'explication2', cssPrefix + 'corr11', textes.Aide02,
                {
                  styletexte: '',
                  u: textes.nom_unite[stor.unite - 1],
                  a: textes.nom_quantite[stor.quantite - repEleve1 + stor.nom_multiple],
                  v: j3pValeurde(cssPrefix + 'consigne1liste1')
                })
            } else {
              j3pAffiche(cssPrefix + 'explication2', cssPrefix + 'corr11', textes.Aide01,
                {
                  styletexte: '',
                  u: textes.nom_unite[stor.unite - 1],
                  v: j3pValeurde(cssPrefix + 'consigne1liste1'),
                  a: textes.nom_quantite[repEleve1 - 3]
                })
            }
          }
          break } // Type 4
      } // type
    } //  A répondu
  } //  afficheCorrection

  // Affiche le nombre de tentatives qu’il reste s’il y a plusieurs chances
  function afficheChances () {
    j3pElement(cssPrefix + 'zone_chances').innerHTML = ''
    if (me.donneesSection.nbchances > 1) {
      j3pAffiche(cssPrefix + 'zone_chances', cssPrefix + 'chances', textes.phraseNbChances,
        {
          e: me.donneesSection.nbchances - me.essaiCourant,
          p: (me.donneesSection.nbchances - me.essaiCourant > 1 ? 's' : '')
        })
    } // Pour le pluriel
  }

  /// ////////////////////////////////////////////////////////////////////////////////////////
  //
  //              Déroulement de la section Enonce, Correction, navigation
  //
  /// ////////////////////////////////////////////////////////////////////////////////////////

  switch (this.etat) {
    /// ////////////////////////////////////////////////////////////////////////////////////////
    //
    //            Phase énoncé
    //
    /// ////////////////////////////////////////////////////////////////////////////////////////

    case 'enonce': {
      //  Construit l’énoncé: exécuté au début de chaque question
      if (this.debutDeLaSection) {
      //  Code exécuté uniquement au lancement de la section
        this.surcharge({ nbrepetitions: ds.nbrepetitionsmax })
        // Construction de la page
        this.construitStructurePage('presentation1')

        this.afficheTitre(textes.phraseTitre)
        // Affiche l’indication en bas à gauche
        if (ds.indication) this.indication(this.zones.IG, ds.indication)
        /*
                 Par convention,`
                `   this.typederreurs[0] = nombre de bonnes réponses
                    this.typederreurs[1] = nombre de fois où l’élève a utilisé une chance supplémentaire
                    this.typederreurs[2] = nombre de mauvaises réponses
                    this.typederreurs[10] = nombre de fois où l’élève a eu faux à cause d’un temps limite
                    LES AUTRES peuvent être utilisées par la détermination de la pe dans le cas d’une section qualitative
                 */
        this.typederreurs = [0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0]

        // Permet l’utilisation du compte à rebours si un temps est défini dans les paramètre

        //  Paramètres et variables particuliers à la section
        //  permet une difficulté croissante
        stor.difficulte_en_cours = 1 //  1: mm,cm,km 2: dm 3:dam 4: hm
        stor.type_en_cours = 1 //  type de question en cours
        stor.unite_en_cours = 1 //  1: m  2:g   3:L  4:s

        //  Tableaux permettant de ne pas poser 2 fois la même question
        stor.questions = [] // Contient les question qui n’ont pas encore été posées [multiple, type, unité]
        stor.questions.push([1, 3, 1])
        stor.questions.push([2, 3, 1])
        stor.questions.push([7, 1, 1])
        if (ds.n_niveau > 1) {
          stor.questions.push([1, 3, 2])
          stor.questions.push([2, 3, 2])
          stor.questions.push([7, 1, 2])
          stor.unite_en_cours = 2
        }

        stor.erreurs = [] // Contient questions qui ont provoqué une erreur pour les représenter en priorité
        // Paramètres et variables particuliers à la section
      } else { // A chaque répétition, on nettoie la scène
        this.videLesZones()
      } // Code exécuté à chaque nouvelle question: Partie à ne pas modifier

      /// ////////////////////////////////////////////////////////////////////////////////////////
      //
      //               Création de l’énoncé
      //
      /// ////////////////////////////////////////////////////////////////////////////////////////

      //  Création de l’énoncé
      // question est le rang demandé
      // difficulté vaut 2 si le nombre de gauche est 0, 2 si le rang vaut 9, 0 sinon
      n = j3pGetRandomInt(0, stor.questions.length - 1) // La question est choisie parmi celles qui n’ont pas encore été posées
      stor.nom_multiple = stor.questions[n][0] // Une question est un triplet multiple
      stor.type = stor.questions[n][1] // type de question soit 1 ou 2 soit 3 ou 4 selon multiple ou sous-multiple
      stor.unite = stor.questions[n][2] // unité: m, g, L, s
      stor.questions.splice(n, 1) // On la retire des questions à poser

      if (stor.nom_multiple < 4) stor.quantite = 5 - stor.nom_multiple
      if (stor.nom_multiple > 4) stor.quantite = stor.nom_multiple - 3

      if ((stor.type == 2) || (stor.type == 4)) {
        this.choix_unite = textes.nom_multiple.slice()
        if (stor.unite == 3) this.choix_unite.splice(6, 1) // Supprime les kL
        for (i = 0; i < this.choix_unite.length; i++) this.choix_unite[i] += textes.nom_unite[stor.unite - 1]
        this.choix_unite.splice(0, 0, '')
      }
      // Création de l’énoncé

      //  Création des conteneurs
      // Création du conteneur dans lequel se trouveront toutes les consignes
      j3pDiv(this.zones.MG, { id: cssPrefix + 'consignes', contenu: '', coord: [0, 0], style: me.styles.etendre('petit.enonce', { padding: '10px', marginTop: '25px' }) })

      // Dans ce conteneur, on crée un div par phrase (cela forcera le retour à la ligne)
      j3pDiv(cssPrefix + 'consignes', cssPrefix + 'zone_consigne1', '')

      // Affiche le nombre de chances qu’il reste
      j3pDiv(cssPrefix + 'consignes', { id: cssPrefix + 'zone_chances', contenu: '', style: me.styles.etendre('moyen.enonce', { marginTop: '30px' }) })
      afficheChances()

      // Si on veut rajouter des explications, en cas de réponse fausse par exemple
      const divExplications = j3pDiv(this.zones.MG, { coord: [10, 200], style: me.styles.petit.explications })
      j3pDiv(divExplications, { id: cssPrefix + 'explication1' })
      j3pDiv(divExplications, { id: cssPrefix + 'explication2', style: { marginTop: '20px' } })
      j3pDiv(divExplications, { id: cssPrefix + 'explication3', coord: [10, 10] })
      // On crée ici le div qui contiendra toutes les remarques sur ce qu’on pense de la réponse de l’élève (le plus souvant dans le cadre MD)
      j3pDiv(this.zones.MD, { id: cssPrefix + 'correction', coord: [0, 0], style: me.styles.etendre('petit.correction', { padding: '10px' }) })
      //  Création des conteneurs

      //  Initialisation des zones de saisie
      switch (stor.type) {
        case 1: {
          j3pAffiche(cssPrefix + 'zone_consigne1', cssPrefix + 'consigne1', textes.phraseEnonce1,
            {
              styletexte: this.styles.petit.enonce,
              m: textes.nom_multiple[stor.nom_multiple - 1],
              u: textes.nom_unite[stor.unite - 1],
              liste1: {
                texte: textes.nom_quantite,
                fontSize: this.styles.petit.enonce.fontSize,
                correction: stor.quantite
              }
            })

          // On rajoute toutes les informations qui permettront de valider la saisie
          j3pElement(cssPrefix + 'consigne1liste1').reponse = stor.quantite
          break } // Type 1
        case 2: {
          j3pAffiche(cssPrefix + 'zone_consigne1', cssPrefix + 'consigne1', textes.phraseEnonce2,
            {
              styletexte: this.styles.petit.enonce,
              a: textes.nom_quantite[stor.quantite],
              u: textes.nom_unite[stor.unite - 1],
              liste1: {
                texte: this.choix_unite,
                fontSize: this.styles.petit.enonce.fontSize,
                correction: stor.nom_multiple
              }
            })

          // On rajoute toutes les informations qui permettront de valider la saisie
          j3pElement(cssPrefix + 'consigne1liste1').reponse = stor.nom_multiple
          break } // Type 2
        case 3: {
          j3pAffiche(cssPrefix + 'zone_consigne1', cssPrefix + 'consigne1', textes.phraseEnonce3,
            {
              styletexte: this.styles.petit.enonce,
              m: textes.nom_multiple[stor.nom_multiple - 1],
              u: textes.nom_unite[stor.unite - 1],
              liste1: {
                texte: textes.nom_quantite,
                fontSize: this.styles.petit.enonce.fontSize,
                correction: stor.quantite
              }
            })

          // On rajoute toutes les informations qui permettront de valider la saisie
          j3pElement(cssPrefix + 'consigne1liste1').reponse = stor.quantite
          break } // Type 3
        case 4: {
          j3pAffiche(cssPrefix + 'zone_consigne1', cssPrefix + 'consigne1', textes.phraseEnonce4,
            {
              styletexte: this.styles.petit.enonce,
              a: textes.nom_quantite[stor.quantite],
              u: textes.nom_unite[stor.unite - 1],
              liste1: {
                texte: this.choix_unite,
                fontSize: this.styles.petit.enonce.fontSize,
                correction: stor.nom_multiple
              }
            })

          // On rajoute toutes les informations qui permettront de valider la saisie
          j3pElement(cssPrefix + 'consigne1liste1').reponse = stor.nom_multiple
          break } // Type 4
      } // type

      //  Permet de refaire passer la zone de saisie en noir lors du deuxième essai
      j3pElement(cssPrefix + 'consigne1liste1').addEventListener('click', function () {
        j3pElement(cssPrefix + 'consigne1liste1').style.color = ''
      }, false)

      // Paramétrage de la validation
      // zonesSaisie est un tableau contenant le nom de chacune des zones (on pourrait aussi y mettre une liste déroulante)
      const zonesSaisie = [cssPrefix + 'consigne1liste1']
      // validePerso est souvant un tableau vide
      // Mais parfois certaines zones ne peuvent pas être validée de manière automatique
      // Donc la validation de ces zones devra se faire "à la main".
      // Par contre on vérifiera si elles sont remplies ou non.
      const zonesValidePerso = []

      // fctsValid est un objet permettant la validation. Il contient le nom des zones (tableau précédent) et un deuxième paramètre validePerso
      stor.fctsValid = new ValidationZones({ parcours: me, zones: zonesSaisie, validePerso: zonesValidePerso })
      //  Initialisation des zones de saisie

      j3pFocus(cssPrefix + 'consigne1liste1')

      this.finEnonce() // Obligatoire
      break }// case "enonce":

    /// ////////////////////////////////////////////////////////////////////////////////////////
    //
    //            Phase correction
    //
    /// ////////////////////////////////////////////////////////////////////////////////////////

    case 'correction': {
      // Permet de nommer facilement les conteneurs
      const fctsValid = stor.fctsValid // ici je crée juste une variable pour raccourcir le nom de l’object fctsValid

      //  Efface le nombre de chances qui restent
      j3pElement(cssPrefix + 'zone_chances').innerHTML = ''

      //  Efface les éléments de correction
      j3pElement(cssPrefix + 'explication1').innerHTML = ''
      j3pElement(cssPrefix + 'explication2').innerHTML = ''
      j3pElement(cssPrefix + 'explication3').innerHTML = ''

      // ce qui suit sert pour la validation de toutes les zones
      // le tableau contenant toutes les zones de saisie
      stor.reponse = fctsValid.validationGlobale() // Ne pas modifier
      // A cet instant reponse contient deux éléments :
      // reponse.aRepondu qui me dit si toutes les zones ont bien été complétées ou non
      // reponse.bonneReponse qui me dit si les zones qui ne sont pas dans le tableau valide_pero ont des réponses correctes ou non.
      // Si reponse.aRepondu vaut true, la fonction met aussi en bleu ou rouge les réponses bonnes ou fausses.
      // Si c’est la dernière tentative de répondre, on barre également les zones fausses

      // Réponse vide ou incomplète et la limite de temps n’est pas atteinte
      if ((!stor.reponse.aRepondu) && (!this.isElapsed)) {
        this.reponseManquante(cssPrefix + 'correction')

        afficheChances() //  affiche le nombre de chances qui restent
      } else {
        // Une réponse a été saisie
        if (stor.reponse.bonneReponse) {
          // Bonne réponse
          this.score++
          j3pElement(cssPrefix + 'correction').style.color = this.styles.cbien
          j3pElement(cssPrefix + 'correction').innerHTML = cBien

          // Même si la réponse est bonne on peut afficher des éléments de correction/explication :

          // Augmente le niveau de difficulté
          // On rajoute les dm, puis les dam, et hm
          // En parallèle on rajoute l’autre type de question
          // Puis ont rajoute g, puis L, puis s
          const difficultePrecedente = stor.difficulte_en_cours
          if (stor.difficulte_en_cours < 4) {
            stor.difficulte_en_cours++
            for (i = 1; i <= stor.type_en_cours; i++) {
              for (j = 1; j <= stor.unite_en_cours; j++) {
                const multiple = (stor.difficulte_en_cours == 2 ? 3 : (stor.difficulte_en_cours == 3 ? 5 : 6))
                if (!((j == 3) && (multiple == 7)) && ((j != 4) || (multiple == 1))) { stor.questions.push([multiple, (multiple < 4 ? i + 2 : i), j]) }
              }
            }
          } // On augmente la difficulté

          const typePrecedent = stor.type_en_cours
          if (stor.type_en_cours < 2) {
            stor.type_en_cours++
            for (i = 7; i > 0; i = (i > stor.difficulte_en_cours + (stor.difficulte_en_cours == 2 ? 1 : 2) ? stor.difficulte_en_cours + (stor.difficulte_en_cours == 2 ? 1 : 2) : i - 1)) {
              for (j = 1; j <= stor.unite_en_cours; j++) {
                if (!((j == 3) && (i == 7)) && ((j != 4) || (i == 1)) && (i != 4)) { stor.questions.push([i, (i < 4 ? stor.type_en_cours + 2 : stor.type_en_cours), j]) }
              }
            }
          } // On rajout l’autre type

          if ((difficultePrecedente == stor.difficulte_en_cours) && (typePrecedent == stor.type_en_cours)) {
            if (ds.n_niveau == 2 ? stor.unite_en_cours < 2 : (ds.n_niveau == 3 ? stor.unite_en_cours < 4 : false)) {
              stor.unite_en_cours++
              for (i = 1; i <= 7; i++) {
                for (j = 1; j <= 2; j++) {
                  if (!((stor.unite_en_cours == 3) && (i == 7)) /* Pas de kL */ && ((stor.unite_en_cours != 4) || ((i == 1)/* que les ms */ && (j == 1)/* sinon on propose ks */)) && (i != 4)/* Pas de m en m */) { stor.questions.push([i, (i < 4 ? j + 2 : j), stor.unite_en_cours]) }
                }
              }
            }
          } // On rajoute une nouvelle unité

          this.typederreurs[0]++ //  Compte les bonnes réponses
          this.cacheBoutonValider() //  Plus de réponse à donner
          this.etat = 'navigation'
          this.sectionCourante() //  Passe à la question suivante
          // Bonne réponse
        } else {
          // Mauvaise réponse
          stor.erreurs.push([stor.nom_multiple, stor.type, stor.unite]) // Mémorise l’erreur
          if (ds.nbchances == 1) stor.erreurs.push([stor.nom_multiple, stor.type, stor.unite]) // Il faudra réussir 2 fois pour confirmer son erreur

          j3pElement(cssPrefix + 'correction').style.color = this.styles.cfaux // Commentaires à droite en rouge

          // Fini à cause de la limite de temps
          if (this.isElapsed) {
            this._stopTimer()

            j3pElement(cssPrefix + 'correction').innerHTML = tempsDepasse // Commentaires à droite

            j3pDesactive(cssPrefix + 'consigne1liste1') // Désactiver toutes les zones de saisie
            j3pBarre(cssPrefix + 'consigne1liste1')

            this.typederreurs[10]++ //  Compte les temps dépassés
            this.cacheBoutonValider() //  Plus de réponse à donner

            // L’élève avait un temps limite mais ne l’a pas respecté. On affiche alors la correction
            afficheCorrection(true)

            this.etat = 'navigation'
            this.sectionCourante() //  Passe à la question suivante
            // Fini à cause de la limite de temps
          } else {
            // Réponse fausse
            j3pElement(cssPrefix + 'correction').innerHTML = cFaux // Commentaires à droite en rouge

            // Il reste des essais
            if (this.essaiCourant < ds.nbchances) {
              j3pElement(cssPrefix + 'correction').innerHTML += '<br>' + essaieEncore // Commentaires à droite

              this.typederreurs[1]++ //  Compte les essais ratés

              afficheChances() //  affiche le nombre de chances qui restent

              // indication éventuelle ici
              afficheCorrection(false)
            // Il reste des essais
            } else {
            // Il ne reste plus d’essais
              this.cacheBoutonValider() //  Plus de réponse à donner

              j3pElement(cssPrefix + 'correction').innerHTML += '<br>' + regardeCorrection // Commentaires à droite
              //
              // Là il ne peut plus se corriger. On lui affiche alors la solution
              afficheCorrection(true)

              this.typederreurs[2]++ //  Compte les réponses fausses

              this.etat = 'navigation'
              this.sectionCourante() //  Passe à la question suivante
            } // Il ne reste plus d’essais
          } // Réponse fausse
        } // Mauvaise réponse
        // Une réponse a été saisie
      } // Une réponse a été saisie

      // Obligatoire
      this.finCorrection()
      break
    } // case "correction":

    /// ////////////////////////////////////////////////////////////////////////////////////////
    //
    //            Phase correction
    //
    /// ////////////////////////////////////////////////////////////////////////////////////////

    case 'navigation':
      //  Si on a posé toutes les questions, on rajoute les erreurs et on continue
      if (stor.questions.length == 0) {
        stor.questions = stor.questions.concat(stor.erreurs)
        stor.erreurs.length = 0 // Et on vide le tableau des erreurs
      }
      //    Arrête les questions si toutes les réponses sont justes
      if (stor.questions.length == 0) me.donneesSection.nbitems = me.questionCourante

      if (this.sectionTerminee()) {
        // On détermine la phrase d’état renvoyée par la section
        if ((this.typederreurs[1] == 0) && (this.typederreurs[2] == 0) && (this.typederreurs[10] == 0)) {
          //  Aucune erreur: Notion comprise
          if (ds.limite != 0) {
            //  Aucune erreur en temps limité: Notion totalement maitrisée
            this.parcours.pe = pe1
          } else {
            //  Aucune erreur sans temps limité: Notion comprise
            this.parcours.pe = pe2
          }
        } else if ((this.typederreurs[2] == 0) && (this.typederreurs[10] == 0)) {
          //  A pu corriger ses erreurs: Notion fragile
          this.parcours.pe = pe5
        } else {
          //  Notion non comprise
          this.parcours.pe = pe6
        }
      } else {
        this.etat = 'enonce'
      }
      // Obligatoire
      this.finNavigation()

      break // case "navigation":
  } //  switch etat
} //  SectionPosition
