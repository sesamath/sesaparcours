import { j3pAddElt, j3pChaine, j3pEstPremier, j3pFocus, j3pGetBornesIntervalle, j3pGetRandomInt, j3pRestriction, j3pStyle, j3pValeurde } from 'src/legacy/core/functions'
import ValidationZones from 'src/legacy/outils/calculatrice/ValidationZones'
import { j3pAffiche } from 'src/lib/mathquill/functions'
import textesGeneriques from 'src/lib/core/textes'

const { cBien, essaieEncore, regardeCorrection, tempsDepasse } = textesGeneriques

/*
    JP Vanroyen
    mai 2013
    Partir de ce fichier pour créer une section à UNE etape

    EN BAS, vous trouverez le code NON COMMENTE si vous ne désirez pas copier-coller les commentaires

*/
export const params = {
  outils: ['calculatrice', 'mathquill'],
  /**
   * Ce qui est paramétrable dans l’éditeur de graphe
   * @type {LegacySectionParamsParametres}
   */
  parametres: [
    ['nbrepetitions', 2, 'entier', 'Nombre de répétitions de la section'],
    ['indication', '', 'string', 'Indication pour chaque répétition'],
    ['limite', 0, 'entier', 'Temps disponible par répétition, en secondes'],
    ['nbchances', 2, 'entier', 'Nombre d’essais par répétition'],
    ['entier', '[2;100]', 'string', 'Intervalle auquel appartient le nombre'],
    ['b_accolades', true, 'boolean', 'Exiger aux élèves d’écrire les accolades'],
    ['b_avecaide', true, 'boolean', 'Donne de l’aide lors de la correction'],
    ['b_avecpremiers', true, 'boolean', 'On permet l’apparition de nombres premiers dans l’énoncé']
  ]
}

/**
 * section diviseursdirect
 * @this {Parcours}
 */
export default function main () {
  const me = this
  const stor = me.storage
  let ds = me.donneesSection

  function getDonnees () {
    return {
      typesection: 'primaire', //

      // Nombre de répétitions de l’exercice
      nbrepetitions: 3,
      nbetapes: 1,

      // Si indication != "" alors un lien "Indication" s’affiche dans la zone Inférieure Gauche (zones.IG
      // une indication<br>de deux lignes...";
      indication: 'Si tu ne vois pas "{" dans ton clavier, essaye de taper "alt (" ou "alt gr (".',
      video: [], // ["tableur1,Titre Video 1,Video 1","lebouquet,Titre Video2,Video 2"]

      // pour le cas de presentation1bis
      // En effet pas d’indication possible pour cette présentation
      // surchargeindication: false;

      // Nombre de chances dont dispose l’él§ve pour répondre à la question
      nbchances: 3,

      structure: 'presentation1', //  || "presentation2" || "presentation3"  || "presentation1bis"

      // Paramètres de la section, avec leurs valeurs par défaut.
      n_parametre1: 0,
      s_parametre2: '[4;13]',
      b_parametre3: true,
      t_parametre4: [1, 2, 3],

      /*
          Les textes présents dans la section
          Sont donc accessibles dans le code par donneesSection.textes.phrase1
          Possibilité d’écrire dans le code :
          var lestextes: donneesSection.textes;
          puis accès à l’aide de lestextes.phrase1
          */
      textes: {
        // phrase1 : "Trouve tous les diviseurs de ",
        titreExo: 'Diviseurs d’un nombre naturel',
        phrase1: 'Trouve tous les diviseurs naturels de £a.',
        phrase1b: 'Donne l’ensemble des diviseurs naturels de £a.',
        phrase2: 'La solution était £s.',
        phrase3: 'Donne ta réponse entre accolades {...}&nbsp;!',
        phrase4: 'Sépare les diviseurs par des ";"&nbsp;!',
        phrase5a: 'Les accolades manquent ou elles sont mal placées.',
        phrase5b: 'Ne commence ni ne finis pas la liste par un ";"&nbsp;!',
        phrase5c: 'Les accolades sont déjà placées, il ne faut pas en ajouter.',
        phrase6: 'Il te manque des diviseurs.<br />',
        phrase7: 'Il te manque un diviseur.<br />',
        phrase8: 'Il te manque £n diviseurs.<br />',
        phrase9: 'Tu as mis des diviseurs faux.<br />',
        phrase10: 'Tu as mis un diviseur faux.<br />',
        phrase11: 'Tu as mis £n diviseurs faux.<br />',
        phrase12: 'Enlève tous les nombres plus grands que £n ou égaux à zéro.<br />'
      },

      // Par défaut, pas de temps limite. Décommentez pour avoir un temps limite par défaut. En secondes.

      // limite: 10;
      /*
          Phrase d’état renvoyée par la section
          Si pe=0 alors la section est quantitative et renvoie un score entre 0 et 1.
          Dans le cas où la section est qualitative, lister les différents pe renvoyées :
              pe_1: "toto"
              pe_2: "tata"
              etc
          Dans la partie navigation, il faudra affecter la variable parcours.pe à l’une des précédentes.
          Par exemple,
          parcours.pe: donneesSection.pe_2
          */
      entier: '[2;100]',
      b_accolades: true,
      b_avecaide: true
    }
  }

  // console.log("debut du code : ",me.etat)
  function enonceMain () {
    const [minInt, maxInt] = j3pGetBornesIntervalle(ds.entier)
    stor.nb = j3pGetRandomInt(minInt, maxInt)
    stor.nbpremier = j3pEstPremier(stor.nb)
    if (!ds.b_avecpremiers && stor.nbpremier) {
      if (stor.nb > 10) stor.nb -= 1
      // il y avait un j3pRandom(5), qui plante depuis longtemps, je sais pas ce que l’auteur a voulu faire, on tire un nb entre 1 et 5
      else stor.nb *= j3pGetRandomInt(1, 5)
    }
    const conteneurG = j3pAddElt(me.zonesElts.MG, 'div', '', { style: { padding: '10px' } })
    stor.conteneur = j3pAddElt(conteneurG, 'div', '', { style: me.styles.petit.enonce })
    for (let i = 1; i <= 4; i++) stor['zoneCons' + i] = j3pAddElt(stor.conteneur, 'div', '')
    if (ds.b_accolades) j3pAffiche(stor.zoneCons1, '', ds.textes.phrase1b, { a: stor.nb })
    else j3pAffiche(stor.zoneCons1, '', ds.textes.phrase1, { a: stor.nb })
    j3pAffiche(stor.zoneCons2, '', ds.textes.phrase4)
    if (ds.b_accolades) j3pAffiche(stor.zoneCons3, '', ds.textes.phrase3)
    j3pStyle(stor.zoneCons4, { paddingTop: '10px' })
    let elt
    if (ds.b_accolades) elt = j3pAffiche(stor.zoneCons4, '', '@1@', { input1: { width: '12px', dynamique: true } })
    else elt = j3pAffiche(stor.zoneCons4, '', '{@1@}', { input1: { width: '12px', dynamique: true } })
    const maRestriction = (ds.b_accolades) ? '\\d;{}' : '\\d;'
    stor.zoneInput = elt.inputList[0]
    j3pRestriction(stor.zoneInput, maRestriction)
    j3pFocus(stor.zoneInput)
    const zones = [stor.zoneInput]
    stor.validationZones = new ValidationZones({ zones, validePerso: zones, parcours: me })
    stor.zoneExpli = j3pAddElt(conteneurG, 'div', '', { style: me.styles.etendre('moyen.explications', { paddingTop: '15px' }) })

    /// //////////////////////////////////////
    /* FIN DU CODE PRINCIPAL DE LA SECTION */
    /// //////////////////////////////////////

    stor.zoneCorr = j3pAddElt(me.zonesElts.MD, 'div', '', { style: me.styles.etendre('moyen.correction', { padding: '15px' }) })

    // Obligatoire
    me.finEnonce()
  }
  switch (me.etat) {
    case 'enonce':
      // code exécuté au lancement de la section
      if (me.debutDeLaSection) {
        me.donneesSection = getDonnees()
        ds = me.donneesSection

        me.surcharge()

        // Construction de la page
        me.construitStructurePage({ structure: ds.structure, ratioGauche: 0.5 })

        // par convention, me.stockage[0] contient la solution
        // les autres peuvent contenir ce que vous voulez, en général des valeurs que l’on doit mémoriser entre les différents appels de la section
        me.stockage = [0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0]

        /*
                 Par convention,`
                `   me.typederreurs[0] = nombre de bonnes réponses
                    me.typederreurs[1] = nombre de fois où l’élève a utilisé une chance supplémentaire
                    me.typederreurs[2] = nombre de mauvaises réponses
                    me.typederreurs[10] = nombre de fois où l’élève a eu faux à cause d’un temps limite
                    LES AUTRES peuvent être utilisées par la détermination de la pe dans le cas d’une section qualitative
                 */
        me.typederreurs = [0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0]

        me.score = 0

        me.afficheTitre(ds.textes.titreExo)

        if (ds.indication !== '' && ds.b_accolades) me.indication(me.zonesElts.IG, ds.indication)

        // code de création des fenêtres
      } else {
        me.videLesZones()
      }

      /// //////////////////////////////////
      /* LE CODE PRINCIPAL DE LA SECTION */
      /// //////////////////////////////////
      enonceMain()

      break // case "enonce":

    case 'correction':
      {
        const validationZones = stor.validationZones

        let i = 0

        let toofew = 0
        let toomany = 0
        let toobig = false
        let accolades = true
        let pointvirgule = false

        let repEleve = j3pValeurde(stor.zoneInput)
        me.stockage[0] = '{'
        if (!ds.b_accolades) repEleve = '{' + repEleve + '}'
        if (repEleve.indexOf('{') !== 0 || repEleve.indexOf('}') !== repEleve.length - 1) accolades = false
        if (repEleve.indexOf('{;') > -1 || repEleve.indexOf(';}') > -1) pointvirgule = true

        let smaller = '{'
        while (i < stor.nb) {
          i++
          const istr = i.toString()
          const middlenum = ';' + istr + ';'
          const lastnum = ';' + istr + '}'
          const firstnum = '{' + istr + ';'
          const onlynum = '{' + istr + '}'
          const inthemiddle = repEleve.indexOf(middlenum) > -1
          const islast = repEleve.indexOf(lastnum) > -1
          const isfirst = repEleve.indexOf(firstnum) > -1
          const isonly = repEleve.indexOf(onlynum) > -1
          if (inthemiddle || islast || isfirst || isonly) smaller += istr + ';'
          if (stor.nb % i === 0) {
            if (!(inthemiddle || islast || isfirst || isonly)) toofew++
            me.stockage[0] += istr
            if (i < stor.nb) me.stockage[0] += ';'
          } else {
            if (inthemiddle || islast || isfirst || isonly) toomany++
          }
        }
        smaller += '}'
        if (repEleve.length > smaller.length - 1) toobig = true

        me.stockage[0] += '}'

        // On teste si une réponse a été saisie
        if ((repEleve === '' || repEleve === '{}') && (!me.isElapsed)) {
          me.reponseManquante(stor.zoneCorr)
          j3pFocus(stor.zoneInput)
          me.afficheBoutonValider()
        } else {
        // Une réponse a été saisie
        // Bonne réponse
          validationZones.zones.bonneReponse[0] = (toofew === 0 && toomany === 0 && accolades && !toobig)
          validationZones.coloreUneZone(stor.zoneInput.id)
          if (validationZones.zones.bonneReponse[0]) {
            me.score++
            stor.zoneCorr.style.color = me.styles.cbien
            stor.zoneCorr.innerHTML = cBien
            me.typederreurs[0]++
            me.etat = 'navigation'
            me.sectionCourante()
          } else {
          // Pas de bonne réponse
            stor.zoneCorr.style.color = me.styles.cfaux

            // A cause de la limite de temps :
            if (me.isElapsed) { // limite de temps
              stor.zoneCorr.innerHTML = tempsDepasse
              me.typederreurs[10]++
              j3pAffiche(stor.zoneExpli, '', ds.textes.phrase2, { s: me.stockage[0] })
              /*
                         *   RECOPIER LA CORRECTION ICI !
                         */
              me.etat = 'navigation'
              me.sectionCourante()
            } else {
            // Réponse fausse :
              if (!accolades) stor.zoneCorr.innerHTML = ds.b_accolades ? ds.textes.phrase5a : ds.textes.phrase5c
              else if (pointvirgule) stor.zoneCorr.innerHTML = ds.textes.phrase5b
              else {
                stor.zoneCorr.innerHTML = ''
                if (toofew > 0 && !ds.b_avecaide) stor.zoneCorr.innerHTML += ds.textes.phrase6
                else {
                  if (toofew === 1) stor.zoneCorr.innerHTML += ds.textes.phrase7
                  else if (toofew > 1) stor.zoneCorr.innerHTML += j3pChaine(ds.textes.phrase8, { n: toofew })
                }
                if ((toomany > 0 || toobig) && !ds.b_avecaide) stor.zoneCorr.innerHTML += ds.textes.phrase9
                else {
                  if (toomany === 1) stor.zoneCorr.innerHTML += ds.textes.phrase10
                  else if (toomany > 0) stor.zoneCorr.innerHTML += j3pChaine(ds.textes.phrase11, { n: toomany })
                  if (toobig) stor.zoneCorr.innerHTML += j3pChaine(ds.textes.phrase12, { n: stor.nb })
                }
              }

              if (me.essaiCourant < ds.nbchances) {
                stor.zoneCorr.innerHTML += '<br>' + essaieEncore
                me.typederreurs[1]++
              // indication éventuelle ici
              } else {
              // Erreur au nème essai
                validationZones.zones.bonneReponse[0] = false
                validationZones.coloreUneZone(stor.zoneInput.id)
                stor.zoneCorr.innerHTML += '<br>' + regardeCorrection
                j3pAffiche(stor.zoneExpli, '', ds.textes.phrase2, { s: me.stockage[0] })
                me.typederreurs[2]++
                me.etat = 'navigation'
                me.sectionCourante()
              }
            }
          }
        }
      }
      // Obligatoire
      me.finCorrection()
      break // case "correction":

    case 'navigation':

      if (me.sectionTerminee()) {
        // On détermine la phrase d’état renvoyée par la section
        me.parcours.pe = me.score / ds.nbitems
        me.afficheBoutonSectionSuivante()
        me.focus('sectioncontinuer')
      } else {
        me.etat = 'enonce'
        me.afficheBoutonSuite()
        me.focus('boutoncontinuer')
      }
      // Obligatoire
      me.finNavigation()

      break // case "navigation":
  }
  // console.log("fin du code : ",me.etat)
}
