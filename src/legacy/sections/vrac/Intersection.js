import { Vector2D } from 'src/legacy/sections/vrac/Vector2D'
import { Polynomial } from 'src/legacy/sections/vrac/Polynomial'

/**
 * class Intersection
 * These contents were written by Kevin Lindsey (http://www.kevlindev.com    http://www.kevlindev.com/gui/math/intersection/Intersection.js)
 * copyright 2006 Kevin Lindsey (kevin@kevlindev.com)
 */
export function Intersection (status) {
  if (arguments.length > 0) {
    this.init(status)
  }
}

Intersection.prototype.init = function (status) {
  this.status = status
  this.points = []
}
Intersection.prototype.appendPoint = function (point) {
  this.points.push(point)
}
Intersection.intersectBezier3Line = function (p1, p2, p3, p4, a1, a2) {
  let a, b, c, d // temporary variables
  const min = a1.min(a2) // used to determine if point is on line segment
  const max = a1.max(a2) // used to determine if point is on line segment
  const result = new Intersection('No Intersection')

  // Start with Bezier using Bernstein polynomials for weighting functions:
  //     (1-t^3)P1 + 3t(1-t)^2P2 + 3t^2(1-t)P3 + t^3P4
  //
  // Expand and collect terms to form linear combinations of original Bezier
  // controls.  This ends up with a vector cubic in t:
  //     (-P1+3P2-3P3+P4)t^3 + (3P1-6P2+3P3)t^2 + (-3P1+3P2)t + P1
  //             /\                  /\                /\       /\
  //             ||                  ||                ||       ||
  //             c3                  c2                c1       c0

  // Calculate the coefficients
  a = p1.multiply(-1)
  b = p2.multiply(3)
  c = p3.multiply(-3)
  d = a.add(b.add(c.add(p4)))
  const c3 = new Vector2D(d.x, d.y)

  a = p1.multiply(3)
  b = p2.multiply(-6)
  c = p3.multiply(3)
  d = a.add(b.add(c))
  const c2 = new Vector2D(d.x, d.y)

  a = p1.multiply(-3)
  b = p2.multiply(3)
  c = a.add(b)
  const c1 = new Vector2D(c.x, c.y)

  const c0 = new Vector2D(p1.x, p1.y)

  // Convert line to normal form: ax + by + c = 0
  // Find normal to line: negative inverse of original line’s slope
  const n = new Vector2D(a1.y - a2.y, a2.x - a1.x)

  // Determine new c coefficient
  const cl = a1.x * a2.y - a2.x * a1.y

  // ?Rotate each cubic coefficient using line for new coordinate system?
  // Find roots of rotated cubic
  const roots = new Polynomial(
    n.dot(c3),
    n.dot(c2),
    n.dot(c1),
    n.dot(c0) + cl
  ).getRoots()

  // Any roots in closed interval [0,1] are intersections on Bezier, but
  // might not be on the line segment.
  // Find intersections and calculate point coordinates
  for (let i = 0; i < roots.length; i++) {
    const t = roots[i]

    if (t >= 0 && t <= 1) {
      // We’re within the Bezier curve
      // Find point on Bezier
      const p5 = p1.lerp(p2, t)
      const p6 = p2.lerp(p3, t)
      const p7 = p3.lerp(p4, t)

      const p8 = p5.lerp(p6, t)
      const p9 = p6.lerp(p7, t)

      const p10 = p8.lerp(p9, t)

      // See if point is on line segment
      // Had to make special cases for vertical and horizontal lines due
      // to slight errors in calculation of p10
      if (a1.x == a2.x) {
        if (min.y <= p10.y && p10.y <= max.y) {
          result.status = 'Intersection'
          result.appendPoint(p10)
        }
      } else if (a1.y == a2.y) {
        if (min.x <= p10.x && p10.x <= max.x) {
          result.status = 'Intersection'
          result.appendPoint(p10)
        }
      } else if (p10.gte(min) && p10.lte(max)) {
        result.status = 'Intersection'
        result.appendPoint(p10)
      }
    }
  }

  return result
}
