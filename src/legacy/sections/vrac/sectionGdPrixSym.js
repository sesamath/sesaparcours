import { j3pAddElt, j3pBasculeAffichage, j3pBoule, j3pDetruit, j3pDiv, j3pElement, j3pFocus } from 'src/legacy/core/functions'
import { j3pCreeGroupe, j3pCreePoint, j3pCreeRectangle, j3pCreeSegment, j3pCreeSVG } from 'src/legacy/core/functionsSvg'
import { Intersection } from 'src/legacy/sections/vrac/Intersection'
import { Point2D } from 'src/legacy/sections/vrac/Point2D'
import { addElement } from 'src/lib/utils/dom/main'

/**
 * une partie du code vient de http://www.kevlindev.com
 */

/*
 Développeur : Hervé JULIER
 Date début : 21/11/2014
 Repris par Daniel Caillibaud en 2022-07 pour le faire à peu près fonctionner (ça déconne encore mais on peut faire une partie)
 puis une autre refonte en 2023-07 avec le passage à la v2 (avec le passage à vite c’est la seule section qui s’est mise à planter dès le chargement)
 */

/* Afin de comprendre les divers "cas" utilisés dans le code, voici à quoi ils correspondent :
Cas 1 : Au premier tour :                 A2 x x A3
                         (labelisé J1) -> A1 x

Cas 2 :                                           point de référence (labelisé A dans l’interface du jeu) ->   A0x
                               point suivant : A1 centre de symétrie (labelisé B dans l’interface du jeu) ->   A1x
                                                     point suivant : A2 symétrique de A0 par rapport à A1 ->   A2x xA3   <- point suivant : A3 obtenu en déplaçant A2 d’un cran (9 possibilités) (pour définir l’accélération et le virage)

Cas 3 : Lors d’une sortie de circuit :                            A0x
                                                                    |
                                                                    |
                                                                  A1x xA3  <- choix d’un point clignotant autours de inter (repositionnement de la voiture après crash)
                                                                    |/
     ces pointillés représentent le bord du circuit -> ------------ xinter -----  <- inter est l’intersection entre [A1A2] et le bord du circuit
  A2 symétrique de A0 par rapport à A1 (A2 est hors circuit) ->   A2x

Cas 4 : Redémarrage après une sortie de circuit :                  x
                                                                   |    xA3  <- on fait clignoter les multiples directions autours de A2 pour avancer d’une case
                                                                   |   /
                                                                 A1x  xA2   <- l’ancien A3 est renommé A2
                                                                   | /
                                     bord du circuit -> ---------- x -----  <- ancien point inter
                                            ancien point A2 ->     x

 */

export const params = {
  /**
   * Ce qui est paramétrable dans l’éditeur de graphe
   * @type {LegacySectionParamsParametres}
   */
  parametres: [
    // ["nbrepetitions",1000,"entier","NE PAS TOUCHER À CE PARAMÈTRE"],
    // ["indication","","string","aucun"],
    // ["limite","","entier","Temps disponible par répétition, en secondes"],    //plus le courage de coder un temps limite
    // ["nbchances",2,"entier",""],
    ['choixCircuit', 'libre', 'string', 'Choix du circuit : "circuit1", "circuit2", "circuit3" ou "libre". Dans ce dernier cas, les joueurs peuvent choisir le circuit sur lequel concourir.'],
    ['toursPenalite', 1, 'entier', 'Nombre de tours que devra passer le joueur qui est rentré dans le bas-côté (entier positif ou nul).'],
    ['pointGagneBonneReponse', 1, 'entier', 'Nombre de points gagnés quand le joueur clique sur le bon symétrique (entier positif ou nul).'],
    ['pointPerduMauvaiseReponse', -1, 'entier', 'Nombre de points perdus quand le joueur ne clique pas sur le bon symétrique (entier NÉGATIF ou nul).'],
    ['pointPerduBasCote', -1, 'entier', 'Nombre de points perdus quand le joueur percute le bas-côté (entier NÉGATIF ou nul).'],
    ['pointGagneLigneArrivee', 5, 'entier', 'Nombre de points gagnés quand le joueur atteint ou franchit la ligne d’arrivée (entier positif ou nul).'],
    ['ecartQuadr', 10, 'entier', 'Ecartement entre 2 lignes du quadrillage (entier entre 5 et 20). Plus c’est petit, plus la course sera longue et moins l’élève risque de sortir du circuit.']
  ]
}

const textes = {
  phraseActiverAnimation: 'Activer les animations',
  phraseDesactiverAnimation: 'Désactiver les animations',
  phraseJ1Gagnant: 'En regardant les scores, le gagnant de cette partie est le joueur 1',
  phraseJ2Gagnant: 'En regardant les scores, le gagnant de cette partie est le joueur 2',
  phraseEgalite: 'En regardant les scores, les deux joueurs sont à égalité !',
  phraseJoueur: 'Joueur ',
  phraseLigneArrivee: ', vous avez franchi la ligne d’arrivée en premier, félicitations ! Vous gagnez donc ',
  phrasePoints: ' points.',
  phraseQuitter: 'Quitter',
  phraseChoixDirection: 'Vous avez le choix de la direction que vous voulez prendre :\n\nchoisissez un des emplacements clignotant.',
  phraseCrash: 'Vous êtes rentrés dans le bas-côté du circuit.\nIl va vous falloir ',
  phraseCrashFin: ' tours pour remettre votre voiture en piste.\n\nChoisissez l’emplacement d’où vous redémarrerez dans ',
  phraseTours: ' tours.',
  phraseAvancerUneCase: 'Pour la première étape, vous n’avez pas le choix : vous n’avancez que d’une case.',
  phraseIlReste: 'Il vous reste encore ',
  phraseIlResteFin: ' tour(s) à attendre avant de pouvoir continuer la course.\n\nCliquez sur le bouton "Passer".',
  phrasePasser: 'Passer',
  phraseChoixSymetrique: 'Cliquez sur l’emplacement du symétrique du point A par rapport au point B (même s’il est en dehors du circuit)',
  phraseScoreJ1: 'Score J1',
  phraseScoreJ2: 'Score J2',
  phraseBienvenue: 'Bienvenue dans :',
  phraseCommencer: 'Commencer',
  phraseRevoirRegles: 'Revoir les règles du jeu',
  phraseContinuer: 'Continuer la partie',
  phraseTourJoueur: 'Tour du joueur ',
  phraseTitre: 'Grand prix symétrie',
  phraseRegles1: 'Règles du jeu',
  phraseRegles2: ' se joue à deux. Le gagnant est celui qui obtiendra le plus de points à l’issue d’un tour du circuit.',
  phraseRegles3: 'Voici comment gagner des points :',
  phraseRegles4: 'ne pas se tromper lorsqu’on clique sur le symétrique d’un point par rapport à un autre : ',
  phrasePoint: ' point,',
  phraseRegles5: 'franchir le premier la ligne d’arrivée : ',
  phraseRegles6: 'Voici comment perdre des points :',
  phraseRegles7: 'se tromper lorsqu’on clique sur le symétrique d’un point par rapport à un autre : ',
  phraseRegles8: 'rentrer dans le bas-côté du circuit, c’est à dire que la direction ou la vitesse du véhicule n’a pas été maîtrisée : ',
  phraseChoixCircuit: 'Sur quel circuit voulez-vous jouer ?',
  phraseCircuit1: 'circuit1',
  phraseCircuit2: 'circuit2',
  phraseCircuit3: 'circuit3'
}

// CODER CAR BUG : la couleur pour J1 et J2 ne fonctionne pas si le prof la change dans les paramètres !!!!
//        ["couleurJ2","#0000FF","string","Couleur du joueur 2 (string en hexadecimal). Par défaut, c’est bleu."],
const couleurJ1 = '#800000'
const couleurJ2 = '#0000FF'
/**
 * Épaisseur des croix
 * @type {number}
 */
const epaisseur = 1.5
/**
 * delai entre 2 animations, par exemple passer de l’emplacement cliqué (boule) à la création de la croix et l’animation du trajet
 * @type {number}
 */
const delaiAnimation = 500
/**
 * Durée (en ms) de l’animation du segment qui s’agrandi (quelque soient la distance séparant les coordonnées d’origine de celles d’arrivée)
 * @type {number}
 */
const dureeAnimation = 1000
/**
 * Durée en ms entre deux clignotements (sur les 8 positions possibles de A3)
 * @type {number}
 */
const dureeClignotement = 500

// on peut passer à la section
/**
 * section GdPrixSym
 * @this {Parcours}
 */
export default function main () {
  /**
   * Retourne la couleur du joueur courant
   * @returns {string}
   */
  function couleurJoueurCourant () {
    return (stor.numJoueur == 1) ? couleurJ1 : couleurJ2
  }

  function basculeAnim () {
    // quand les animations sont activées, ça les désactive, et vice et versa
    if (stor.delai === delaiAnimation) { // "Désactiver les animations"
      stor.delai = 0
      stor.duree = 0
      stor.boutonDesactiveAnim.innerText = textes.phraseActiverAnimation // "Activer les animations";
    } else {
      stor.delai = delaiAnimation
      stor.duree = dureeAnimation
      stor.boutonDesactiveAnim.innerText = textes.phraseDesactiverAnimation // "Désactiver les animations";
    }
  } // basculeAnim

  function cacherBoutonPasser () {
    stor.boutonPasser.style.visibility = 'hidden'
  }

  /**
   * réactive le bouton d’id indiqué (élément ou string) et lui redonne les couleurs d’origine
   * @param bouton
   */
  function afficherBoutonPasser (bouton) {
    stor.boutonPasser.style.visibility = 'visible'
    j3pFocus(stor.boutonPasser)
  }

  function secoueOverlay () {
    // "secoue" le div "overlay" pour simuler un crash de la voiture dans le bas côté

    function rand (min, max, integer) {
      // renvoie un nombre aléatoire.
      // si integer = true, le nombre sera entier entre min (inclu) et max (inclu) ; sinon, il sera décimal entre min (inclu) et max (EXCLU)
      if (!integer) {
        return Math.random() * (max - min) + min
      } else {
        return Math.floor(Math.random() * (max - min + 1) + min)
      }
    }

    const dessinSVG = j3pElement('dessinSVG')

    for (let i = 0; i < 10; i++) {
      setTimeout(function () {
        const deltaX = rand(4, 10, true)
        const deltaY = rand(4, 10, true)
        dessinSVG.style.transform = 'translate(' + deltaX + 'px,' + deltaY + 'px)'
      }, i * 50)
    } // for

    setTimeout(function () {
      dessinSVG.style.transform = 'translate(0px,0px)'
    }, 500)
  }

  function flashElement (el) {
    // fait un effet de transition CSS à l’élément el (ou d’id el). L’effet dépend de l’élément passé en paramètre.
    if (typeof el === 'string') el = j3pElement(el)
    if (el.id === 'info') { // active une ombre autours de l’élément "info" avec un effet d’animation CSS pour attirer l’oeil du joueur (car le texte inclus vient de changer)
      if (el.style.transitionProperty) { // on réinit aux valeurs par défaut
        el.style.transitionProperty = ''
        el.style.transitionDuration = ''
      }

      el.style.boxShadow = '0px 0px 8px 8px #B7B7B7' // ombre colorée instantanée blanchâtre

      setTimeout(function () { // l’ombre s’estompe
        el.style.transitionProperty = 'box-shadow'
        el.style.transitionDuration = '2s'
        el.style.boxShadow = '' // plus d’ombre colorée
      }, 200)
    } else if (el === stor.encadreCircuit) { // met le cadre entourant le SVG de la couleur du joueur (avec une transition)
      if (!el.style.transitionProperty) {
        el.style.transitionProperty = 'stroke'
        el.style.transitionDuration = '2s'
      }

      el.style.stroke = couleurJoueurCourant()
    } else if (el === stor.overlay) { // effet appelé quand l’élève se trompe en plaçant le symétrique A2 : on fait un flash rouge de l’overlay
      el.style.backgroundColor = me.styles.cfaux
      setTimeout(function () {
        el.style.backgroundColor = 'rgba(0,0,0,0.6)'
      }, 500)
    }
  } // flashElement

  function creeCircuit () {
    // s’il n’existe pas déjà, on créé le circuit choisi par les joueurs ou paramétré par le prof
    let noeuds
    if (!stor.creationGdPrix) { // ne faire ce qui suit que si le circuit n’existe pas déjà
      stor.creationGdPrix = true
      if (stor.choixCircuit === textes.phraseCircuit3) { // "circuit3"
        // liste des noeuds du path (obtenu grâce à inkscape) contenant des moveto (M), courbes de bézier cubiques (C pour coord absolues, c pour coord relatives)
        noeuds = 'M 105.4,3.232 C 8.623,11.59 14.07,118.2 10.7,190.5 -0.6208,433.6 156.6,461.1 256,305.4 362.8,486.9 456.9,389.1 463.1,224 464.9,163.1 464.6,95.44 427.1,44.31 401.5,9.473 353.9,-5.215 312.7,10.38 273.7,25.14 239.9,102.2 199.7,109.5 141.9,119.8 177.2,-2.967 105.4,3.232 Z M 349.4,92.01 C 402.9,127.2 364.6,323.7 360.2,308.3 350.4,268.7 335,193.4 290.8,195.5 180.3,200.7 104.6,380.4 97.53,267.6 95.78,239.6 94.44,218.7 95.75,190.5 96.54,173.4 98.12,149 101.5,132.2 163.4,289.5 276.1,175.5 349.4,92.01 Z'
      } else if (stor.choixCircuit === textes.phraseCircuit2) { // "circuit2"
        noeuds = 'M 232.1,3.396 C 189.9,-1.954 169.8,48.42 169.7,89.46 157.2,119.2 128.3,33.48 79.75,39.46 42.77,44.02 20.12,130.8 10.7,190.5 -1.795,346.6 130.9,395.3 255.1,403.2 287.7,404.2 321.3,406.3 353.3,398.8 446,361.5 495.5,197.3 445.2,124.9 392.2,53.57 314.5,3.786 232.1,3.396 Z M 237.9,92.78 C 292,100.1 351.2,126.8 369.3,179.5 389.5,238.7 344.9,311.7 294.8,319.8 259.2,325.5 93.31,289.5 95.75,190.5 96.48,166.3 99.18,131.2 109.7,149.5 137.7,193.9 173.7,209.2 201.1,181.5 230.1,152.3 226.1,132.5 237.9,92.78 Z'
      } else { // "circuit1"
        noeuds = 'm 326.11703,10.887875 c 81.73178,-5.7693021 94.23194,24.038761 109.61676,63.462331 15.3848,40.385124 -3.84621,89.424194 -30.76963,117.309154 30.76963,22.11566 84.61644,88.46262 31.73117,171.15595 C 383.81005,444.5471 115.53748,375.31546 36.690337,270.50647 1.1129671,219.54432 5.9207271,158.96665 27.074837,121.46618 94.383367,21.464931 243.4237,16.657178 326.11703,10.887875 Z M 172.26895,114.73533 c -50.00062,7.69241 -90.385753,62.50078 -72.1163,97.1166 18.26949,34.61577 56.73149,55.76989 97.11661,74.03934 41.34668,18.26945 90.38574,46.15444 141.34793,39.42356 50.96217,-5.7693 21.15412,-58.65457 -0.96155,-74.03938 -23.07722,-16.34635 -44.23132,-51.92366 -39.42357,-68.27004 7.6924,-25.96186 35.57736,-46.15443 50.96218,-75.00095 12.50013,-31.731153 -127.88622,-0.96154 -176.9253,6.73087 z'
      }
      noeuds = convertitEnAbsolu(noeuds) // nécessaire pour que la fonction intersection fonctionne bien
      const circuit = j3pElement('circuit')
      creeChemin(circuit, {
        id: 'route',
        d: noeuds,
        couleurRemplissage: '#F3F9AE',
        couleur: 'black',
        epaisseur: '3px'
      })

      // on replace la ligne de départ au-dessus de la route pour qu’elle soit visible
      circuit.appendChild(stor.ligneDepart)
    }
  }

  function determineCoordLigneDepart () {
    // renvoie les coordonnées de la ligne de départ qui est la référence pour le quadrillage : une ligne horizontale de quadrillage y passera
    // let ordonneeLigneDepart = Math.round(stor.ligneDepart.pathSegList.getItem(0).y); //cette ligne suffit pour remplacer y1 de coordLigneDepart et ça marche sous firefox mais pas sous chromium...
    const d = stor.ligneDepart.getAttribute('d')

    const [, x1, y1, x2, y2] = /^M ([0-9]+\.?[0-9]+),([0-9]+\.?[0-9]+) ([0-9]+\.?[0-9]+),([0-9]+\.?[0-9]+)$/.exec(d)
    return {
      x1: parseFloat(x1),
      y1: parseFloat(y1),
      x2: parseFloat(x2),
      y2: parseFloat(y2)
    }
  }

  /**
   * appelée par la correction quand l’un des joueurs a franchi la ligne d’arrivée (appellera finCorrection au clic sur Quitter)
   */
  function finPartie () {
    changeScore(stor.pointGagneLigneArrivee)
    let finPhrase, couleurGagnant
    if (stor.J1.score > stor.J2.score) {
      finPhrase = textes.phraseJ1Gagnant // "En regardant les scores, le gagnant de cette partie est le joueur 1";
      couleurGagnant = couleurJ1
    } else if (stor.J2.score > stor.J1.score) {
      finPhrase = textes.phraseJ2Gagnant // "En regardant les scores, le gagnant de cette partie est le joueur 2";
      couleurGagnant = couleurJ2
    } else {
      finPhrase = textes.phraseEgalite // "En regardant les scores, les deux joueurs sont à égalité !";
    }

    j3pElement('info').innerHTML = textes.phraseJoueur + stor.numJoueur + textes.phraseLigneArrivee + '<strong>' + stor.pointGagneLigneArrivee + textes.phrasePoints + '</strong>' // "Joueur " + stor.numJoueur + ", vous avez franchi la ligne d’arrivée en premier, félicitations ! Vous gagnez donc <strong>" + stor.pointGagneLigneArrivee + " points</strong>.";

    // on renomme le bouton "Revoir les règles du jeu" en "Quitter", qui appellera finCorrection vers navigation
    stor.boutonReglesJeu.innerText = textes.phraseQuitter
    stor.boutonReglesJeu.onclick = () => me.finCorrection('navigation', true)

    // animation d’un div qui affiche le nom du gagnant
    //* *****************************************************************************
    setTimeout(function () { // on attend que le premier ayant franchi la ligne d’arrivée lise son message et comprenne qu’il a gagné bcp de points avant d’afficher le divGagnant donnant le nom du gagnant en fonction des scores
      const oH = stor.cadreG.offsetHeight
      const oW = stor.cadreG.offsetWidth
      const ratioSVG = stor.largeurCircuit / stor.hauteurCircuit
      const largeurOverlay = stor.overlay.offsetWidth
      const ratioCadreG = oW / oH
      let largeurCircuit, largeurDivGagnant, absBordGaucheCircuit, ordMilieu
      if (ratioCadreG > ratioSVG) { // le cadreG est plus large que le SVG : il y a de l’espace vide à côté du SVG. On est sûrement en mode paysage.
        const hauteurCadreG = stor.cadreG.offsetHeight
        largeurCircuit = Math.round(hauteurCadreG * ratioSVG) // largeur du circuit (entouré de rouge ou bleu)
        largeurDivGagnant = 170
        absBordGaucheCircuit = largeurOverlay - largeurCircuit - 270 - 13 // 260+10 est la largeur du cadreD + marge
        ordMilieu = Math.round(stor.cadreD.offsetHeight / 2 - 85)
      } else { // mode portrait
        largeurDivGagnant = 170
        absBordGaucheCircuit = 10
        largeurCircuit = Math.round(largeurOverlay - 270) // 260+10 est la largeur du cadreD + marge
        const hauteurCircuit = Math.round(largeurCircuit / ratioSVG)
        ordMilieu = Math.round(hauteurCircuit / 2)
      }
      const absMilieu = Math.round((absBordGaucheCircuit + largeurOverlay) / 2)
      const divGagnant = j3pDiv(stor.overlay, {
        id: 'divGagnant',
        contenu: finPhrase,
        coord: [absMilieu - 85, ordMilieu - 85],
        style: me.styles.etendre('petit.enonce', {
          display: 'block',
          width: largeurDivGagnant + 'px',
          position: 'absolute',
          color: couleurGagnant,
          margin: '0px auto'
        })
      })
      divGagnant.style.padding = '10px'
      divGagnant.style.backgroundColor = 'white'
      divGagnant.style.border = 'thick outset'
      divGagnant.style.borderColor = couleurGagnant
      divGagnant.style.textAlign = 'center' // non mis dans la 3e ligne précédente sinon ça ne marche pas (bug ?)

      // on anime divGagnant
      divGagnant.style.transform = 'skew(85deg,85deg)' // position initiale : divGagnant quasiment en plein écran
      setTimeout(function () { // attendre qqs millisecondes avant de faire tourner (sinon l’animation passe directement en étape finale bizarrement)
        const divGagnant = j3pElement('divGagnant')
        divGagnant.style.transitionProperty = 'transform'
        divGagnant.style.transitionDuration = '5s'
        divGagnant.style.transform = 'skew(0deg,0deg)'
      }, 100)

      // on rend divGagnant draggable
      styleUserSelect(divGagnant) // L’utilisateur ne pourra plus sélectionner le texte de l’élément
      divGagnant.addEventListener('mousedown', mouseDown, false)
      window.addEventListener('mouseup', mouseUp, false)

      let deltaX, deltaY

      function mouseDown (e) {
        deltaX = e.clientX - e.target.offsetLeft
        deltaY = e.clientY - e.target.offsetTop
        window.addEventListener('mousemove', divMove, true)
      }

      function mouseUp () {
        window.removeEventListener('mousemove', divMove, true)
      }

      function divMove (e) {
        const divGagnant = j3pElement('divGagnant')
        divGagnant.style.left = (e.clientX - deltaX) + 'px'
        divGagnant.style.top = (e.clientY - deltaY) + 'px'
      }
    }, delaiAnimation * 4) // setTimeout
  } // finPartie

  function j3pMorpheSegment (segm, coord, temps) {
    // implémentation de l’animation avec setTimeout
    if (typeof segm === 'string') segm = j3pElement(segm)

    function anim (segm, coord, temps) { // anim début
      for (const xOuY in coord) { // xOuY vaut "x1" puis "y1" puis "x2" puis "y2"
        if (xOuY) {
          const xOuYOld = parseFloat(segm.getAttribute(xOuY)) // ou segm[xOuY].baseVal.value (car segm[xOuY] est un objet de type SVGAnimatedLength)
          if (!delta[xOuY] && delta[xOuY] !== 0) {
            delta[xOuY] = coord[xOuY] - xOuYOld // écart entre les positions avant et après animation
            if (temps == 0) {
              theta[xOuY] = delta[xOuY]
            } else {
              theta[xOuY] = thetaDuree * delta[xOuY] / temps
            }
          }
          segm.setAttribute(xOuY, xOuYOld + theta[xOuY]) // si on met Math.ceil(xOuYOld + theta[xOuY]) , ça ne change pas l’attribut si theta est trop proche de 0 (du coup il n’y a pas d’animation)
        }
      } // fin for

      const t1 = new Date().getTime()
      const dureeEcoule = t1 - t0 // en ms
      if (dureeEcoule < temps) {
        setTimeout(anim, thetaDuree, segm, coord, temps)
      } // if
    } // anim

    const thetaDuree = 100 // arbitraire : mettre moins pour avoir une animation plus fluide mais qui consomme plus de ressources CPU
    const delta = {}
    const theta = {} // indispensable (voir fonction anim)

    const t0 = new Date().getTime()
    setTimeout(anim, thetaDuree, segm, coord, temps) // segm, coord, temps sont les paramètres qui seront passés à la fonction anim
  } // j3pMorpheSegment

  // affiche l’emplacement (élément d’id id) (caché auparavant avec element.style.fill = "none" par exemple)
  function afficheEmpl (element) {
    if (typeof element === 'string') element = j3pElement(element)
    let couleur = couleurJoueurCourant() // #80000 par ex
    couleur = couleur.substring(1, couleur.length) // enlève le # de début
    element.style.fill = 'url("#degradeboule' + couleur + '")'
  } // afficheEmpl

  // fonction presque équivalente à j3pBasculeAffichage mais en modifiant fill au lieu de display : ça permet alors d’utiliser la propriété CSS pointer-events:all pour que l’élément soit cliquable même s’il est invisible
  function basculeFill (element) {
    if (typeof element === 'string') element = j3pElement(element)
    if (element.style.fill === 'none') {
      let couleur = couleurJoueurCourant() // #80000 par ex
      couleur = couleur.substring(1, couleur.length) // enlève le # de début
      element.style.fill = 'url("#degradeboule' + couleur + '")'
    } else {
      element.style.fill = 'none'
    } // if
  } // basculeFill

  // teste si le segment d’extrémités point1 et point2 ({x:number, y:number}) coupe la ligne d’arrivée (en allant dans le sens du circuit, c’est à dire point1 plus bas que point2)
  function testerFinPartie (point1, point2) {
    const segmentVirtuel = j3pCreeSegment('dessinSVG', {
      id: 'segmentVirtuel',
      x1: point1.x,
      y1: point1.y,
      x2: point2.x,
      y2: point2.y,
      couleur: 'green',
      epaisseur: 0
    })

    const inters = intersection(stor.ligneDepart, segmentVirtuel)
    j3pDetruit('segmentVirtuel')

    const basVersHaut = (point1.y > point2.y) // vrai si on monte. Attention : axe des ordonnées inversé

    if (inters.length > 0 && basVersHaut && me.questionCourante > 11) { // ligne franchie, dans le bon sens, et pas vers le début du jeu
      stor.joueur.gagne = true
    }
  }

  /**
   * Appelé au clic sur une boule clignotante
   * @param {MouseEvent} e
   */
  function arreterClign (e) {
    // arrête le clignotement des emplacements possibles de A3, puis affiche un moment l’emplacement cliqué, puis le fait disparaître, puis affiche une croix au nouvel emplacement et morphe [A1A2] en [A1A3]
    // puis enregistre la position choisie dans stor.J1.A3 ou stor.J2.A3
    // Si cette fonction est appelée après le redémarrage suite à une sortie de circuit, ça créé le trajet [A2A3] (en l’animant) (en fait, ce segment est créé dans placerPoint mais animé dans arreterClign)

    //        j3pDetruit("undefinedgroupepointtemp");   //détruit tous les points qu’on a testé dans la fonction getEmplacements (let temp = ...)

    clearTimeout(stor.timer) // arrête le clignotement des emplacements
    for (const boule of stor.boules) {
      boule.removeEventListener('mousedown', arreterClign, false)
      if (e.currentTarget !== boule) j3pDetruit(boule) // on détruit tous les emplacements sauf celui sur lequel on a cliqué
    }
    // il n’en reste qu’une, celle sur laquelle on a cliqué
    const boule = e.currentTarget
    stor.boules = [boule]

    const A3 = {}
    A3.x = boule.cx.baseVal.value
    A3.y = boule.cy.baseVal.value

    const joueur = stor.joueur
    joueur.A3 = A3 // on enregistre les coord de l’emplacement cliqué dans les variables du joueur en cours

    const delai = stor.delai
    afficheEmpl(boule) // on affiche l’emplacement cliqué quelques millisecondes

    setTimeout(function () {
      j3pDetruit(boule) // détruit l’emplacement
      stor.boules = []
      const point = j3pCreePoint(j3pElement('pointsJ' + stor.numJoueur), {
        nom: '',
        x: A3.x,
        y: A3.y,
        taille: joueur.taille,
        couleur: couleurJoueurCourant(),
        taillepolice: stor.taillePolice,
        epaisseur,
        decal: [4, 4]
      }) // [4,4] revient à mettre le nom pile poil sur le point, mais ce sera changé dans afficheLabelsPoints
      point.id = 'A3J' + stor.numJoueur

      if (joueur.cas !== 4) {
        j3pDetruit('A2J' + stor.numJoueur) // on détruit la croix précédente au moment où l’emplacement A3 est cliqué
        joueur.A2 = {}
      }

      const trajet = j3pElement('segm')
      trajet.id = 'segmentAGarder'
      j3pMorpheSegment(trajet, { x2: A3.x, y2: A3.y }, stor.duree) // le segment [A1A2] morphe en [A1A3] dans le cas 2, ou alors agrandit le segment [inter inter] en [inter A3] dans le cas 3

      setTimeout(function () {
        testerFinPartie(joueur.A1, A3)

        if (joueur.gagne) {
          finPartie()
        } else {
          // on passe au joueur suivant
          me.finCorrection('enonce', true)
        }
      }, stor.duree) // setTimeout
    }, delai) // setTimeout
  } // arreterClign

  /**
   * Affiche les boules clignotantes sur lesquelles on peut cliquer (ça appelera arreterClign)
   * @param emplacements
   */
  function clignoterDirections (emplacements) {
    // fait clignoter les emplacements (tableau de points {x:number, y:number}) possibles du joueur
    // si c’est le premier coup du joueur, il y a au max 3 emplacements qui clignotent,
    // si le joueur n’est pas sorti du circuit (cas 2) (cad A2 est à l’intérieur), il y en a 9 max (on ne propose pas ceux en dehors du circuit) autours du point A2
    // si le joueur est sorti du circuit (cas 3), il y en a au moins 1 autour du point inter
    // si le joueur redémarre après une sortie de circuit (cas 4), il y en a 9 max

    function anim () { // anim début
      for (const boule of stor.boules) {
        basculeFill(boule) // si j’avais utilisé j3pBasculeAffichage(boule); ça n’aurait pas permis que l’emplacement soit cliquable quand il est éteint (cad en display="none")
      }
      stor.timer = setTimeout(anim, dureeClignotement)
    } // anim

    const { cas } = stor.joueur
    if (cas == 4) {
      const info = j3pElement('info')
      flashElement(info)
      info.style.color = couleurJoueurCourant()
      info.innerHTML = '<br><br>' + textes.phraseChoixDirection // "<br><br>Vous avez le choix de la direction que vous voulez prendre :<br><br>choisissez un des emplacements clignotant.";
    }

    const dessinSVG = j3pElement('dessinSVG')
    const length = emplacements.length
    const couleur = couleurJoueurCourant()
    stor.boules = []
    for (let i = 0; i < length; i++) {
      const boule = j3pBoule(dessinSVG, {
        couleur,
        cx: emplacements[i].x,
        cy: emplacements[i].y,
        diametre: stor.tailleBoule
      })
      boule.addEventListener('mousedown', arreterClign, false)
      boule.style.pointerEvents = 'all' // pour faire en sorte que l’emplacement soit cliquable même lorsqu’il est invisible
      stor.boules.push(boule)
    } // for

    stor.timer = setTimeout(anim, dureeClignotement)
  } // clignoterDirections

  function determineAutreJoueur () {
    // si c’est le tour de J1, ça renvoie stor.J2 et vis et versa
    return stor['J' + (3 - stor.numJoueur)]
  }

  function getEmplacements (inter, A1) {
    // getEmplacements début
    // inter est un point {x:number, y:number} correspondant soit à l’intersection de [A1A2] avec le bord de la route, soit à A2 si le joueur n’est pas sorti de la route
    // Renvoie un tableau des points {x:number, y:number} sur le quadrillage à l’intérieur de la route et qui sont situés dans un carré de côté 1 unité (=stor.ECARTQUADR) autours du point inter.
    // Si aucun point n’est trouvé dans ce petit carré (cas rare où on sort de la route en passant par une sorte d’impasse étroite), on cherche ceux dans un carré de côté 1,5 puis 2 puis 2,5 unités...
    // Ne renvoie jamais la position courante de l’autre joueur (cad les voitures ne peuvent pas se superposer).
    // Ne renvoie jamais un tableau vide.
    // Si c’est le 1er tour du joueur, ça ne propose que 3 emplacements au maximum.
    // (L’argument A1 me sert uniquement à déterminer quels points seront dans la route.)

    function HJRechercheDansTableau (point, tab) {
      // Recherche l’élément point {x:number, y:number} dans le tableau tab (contenant des points de la forme {x:number, y:number}) et retourne l’indice si l’élément est trouvé. S’il n’est pas trouvé retourne -1
      // fonction implémentée car tab.indexOf(point) ne peut pas fonctionner car point est une autre instanciation du point qui a été mis précédemment dans le tableau donc il ne trouvera jamais point dans le tableau

      for (let k = 0, xEl, yEl; k < tab.length; k++) {
        xEl = tab[k].x
        yEl = tab[k].y
        if (point.x == xEl && point.y == yEl) {
          return k
        }
      }
      return -1
    } // HJRechercheDansTableau

    const ordonneeLigneDepart = Math.round(determineCoordLigneDepart().y1)
    const delta = ordonneeLigneDepart % stor.ecartQuadrillage // ordonnée de la ligne de quadrillage horizontale la plus haute
    let cote = stor.ecartQuadrillage // taille du côté du carré qui contient les points qu’on cherche

    const route = j3pElement('route')
    const dessinSVG = j3pElement('dessinSVG')
    const tabPointsTestes = [] // liste de tous les points testés, pour éviter de les retester quand on agrandit le carré
    const tabPointsAGarder = [] // liste de tous les points qui conviendront

    while (tabPointsAGarder.length === 0) { // si on a fini de tester tous les points dans le carré et qu’aucun ne convient, on agrandira le carré
      const xmin = Math.ceil((inter.x - cote) / stor.ecartQuadrillage) * stor.ecartQuadrillage // définition de la plus petite abscisse à tester (cad dans le carré et sur le quadrillage)
      const xmax = Math.floor((inter.x + cote) / stor.ecartQuadrillage) * stor.ecartQuadrillage
      let ymin, ymax
      if (me.questionCourante <= 2) { // c’est le 1er tour du joueur
        ymin = inter.y // on avance forcément d’une seule case
        ymax = inter.y
      } else {
        ymin = Math.ceil((inter.y - delta - cote) / stor.ecartQuadrillage) * stor.ecartQuadrillage + delta
        ymax = Math.floor((inter.y - delta + cote) / stor.ecartQuadrillage) * stor.ecartQuadrillage + delta
      }

      for (let i = xmin, point = {}, segmentVirtuel, interTemp; i <= xmax; i += stor.ecartQuadrillage) {
        for (let j = ymin; j <= ymax; j += stor.ecartQuadrillage) {
          point.x = i
          point.y = j
          // pour débugage, décommenter la ligne ci-dessous ainsi que   j3pDetruit("undefinedgroupepointtemp");   de la fonction arreterClign
          //                    let temp = j3pCreePoint(j3pElement("pointsJ"+stor.numJoueur), {nom:"temp", x:point.x, y:point.y, taille:joueur.taille, couleur:"green", taillepolice:1, epaisseur:1.5*3, decal:[3,63]});

          if (HJRechercheDansTableau(point, tabPointsTestes) == -1) { // si le point n’a pas encore été testé
            tabPointsTestes.push({ x: point.x, y: point.y }) // et non tabPointsTestes.push(point); car la variable point est une référence donc quand sa valeur changera, elle changera aussi dans le contenu de tabPointsTestes
            segmentVirtuel = j3pCreeSegment(dessinSVG, {
              id: 'seg',
              x1: A1.x,
              y1: A1.y,
              x2: point.x,
              y2: point.y,
              couleur: 'green',
              epaisseur: 0
            })
            interTemp = intersection(route, segmentVirtuel)
            j3pDetruit('seg')

            const autreJoueur = determineAutreJoueur()
            let pointCourantAutreJoueur = autreJoueur.A3
            if (!pointCourantAutreJoueur.x) pointCourantAutreJoueur = autreJoueur.A1 // au premier tour de J1, le point A3J2 n’existe pas : seul A1J2 existe      Attention : quand je pense que A3 n’est pas défini, il vaut en fait {} c’est pour ça que je teste plutôt pointCourantAutreJoueur.x
            const pointSurAutreJoueur = (point.x == pointCourantAutreJoueur.x && point.y == pointCourantAutreJoueur.y)

            if (interTemp.length === 0 && !pointSurAutreJoueur) { // si le segment [A1 point] ne coupe pas la route, c’est que le point est à l’intérieur donc on le met dans la liste des emplacements possibles (en fait, dans de rares cas, il peut y avoir 2 points d’intersection (interTemp.length=2) ce qui signifie que point est certainement à l’intérieur mais on peut quand même enlever ce point : on en trouvera d’autres). Voir fichier geogebra pour le dessin explicatif.
              tabPointsAGarder.push({ x: point.x, y: point.y }) // et non tabPointsAGarder.push(point); car la variable point est une référence donc quand sa valeur changera, elle changera aussi dans le contenu de tabPointsAGarder
            }
          } else { // point a déjà été testé : on passe alors au point suivant
            // j3pDetruit("undefinedgroupepointtemp");   //détruit le point qu’on vient de tester (let temp = ...)
          } // if
        } // for
      } // for

      cote += Math.ceil(stor.ecartQuadrillage / 2) // 10 ; 15 ; 20 ; 25 ; ... si on part de ECARTQUADR = 10
    } // while

    return tabPointsAGarder
  } // getEmplacements

  function placerPoint (xA2, yA2) {
    // Si le joueur ne sort pas de la route, ça créé un point de coord (xA2:number, yA2:number) en animant le trajet effectué par un segment [A1A2] en pointillés.
    // Si [A1A2] intersecte le bas-côté du circuit, ça place un point "inter" à cette intersection en animant le trajet, puis renomme "inter" en A2

    function distance (A, B) {
      // renvoie la distance AB (number) où A et B sont des points {x:number , y:number}
      return Math.sqrt(Math.pow(B.x - A.x, 2) + Math.pow(B.y - A.y, 2))
    }

    function plusProcheDe (tab, orig) {
      // parmi les points contenus dans le tableau tab, ça renvoie celui le plus proche du point orig {x:number , y:number}
      // si tab est vide, ça renvoie undefined
      let pointMin = tab[0]
      for (const point of tab.slice(1)) {
        pointMin = (distance(point, orig) < distance(pointMin, orig)) ? point : pointMin
      }
      return pointMin
    }

    const { joueur } = stor
    const A2 = { x: xA2, y: yA2 }
    joueur.A2 = A2

    const dessinSVG = j3pElement('dessinSVG')
    const pointsJ = j3pElement('pointsJ' + stor.numJoueur)
    const couleur = couleurJoueurCourant()
    const A1 = joueur.A1

    const segmentsAGarderJ = j3pElement('segmentsAGarderJ' + stor.numJoueur)
    const segm = j3pCreeSegment(segmentsAGarderJ, {
      id: 'segm',
      x1: A1.x,
      y1: A1.y,
      x2: A1.x,
      y2: A1.y,
      couleur,
      epaisseur: 1,
      opacite: 1,
      pointilles: '1,1'
    }) // segment limité à un point car sera agrandi avec j3pMorpheSegment
    const segmVirtuel = j3pCreeSegment(dessinSVG, {
      id: 'segmV',
      x1: A1.x,
      y1: A1.y,
      x2: A2.x,
      y2: A2.y,
      couleur,
      epaisseur: 0
    }) // 0 d’épaisseur donc ne sera pas affiché. Ne sert que pour déterminer l’intersection avec route
    const interTab = intersection('route', segmVirtuel)
    const inter = plusProcheDe(interTab, A1) // le segment [A1A2] peut couper plusieurs fois la route donc le point d’intersection qui nous intéresse est celui le plus proche de A1
    j3pDetruit('segmV')

    if (!inter) { // le joueur n’est pas rentré dans le bas-côté : inter == "undefined"
      joueur.cas = 2
      j3pMorpheSegment(segm, { x2: A2.x, y2: A2.y }, stor.duree)

      setTimeout(function () { // on attend que j3pMorpheSegment ait fini son animation avant d’animer la suite
        testerFinPartie(A1, A2)

        const point = j3pCreePoint(pointsJ, {
          nom: '',
          x: A2.x,
          y: A2.y,
          taille: joueur.taille,
          couleur,
          epaisseur,
          decal: [4, 4]
        }) // [4,4] revient à mettre le nom pile poil sur le point, mais ce sera changé dans afficheLabelsPoints
        point.id = 'A2J' + stor.numJoueur
        const info = j3pElement('info')
        flashElement(info)

        if (joueur.gagne) {
          finPartie()
        } else {
          info.innerHTML = textes.phraseChoixDirection // "Vous avez le choix de la direction que vous voulez prendre :<br><br>choisissez un des emplacements clignotant.";
          const emplacements = getEmplacements(A2, A2) // emplacements possibles autours de A2
          setTimeout(clignoterDirections, 500, emplacements) // on créé les emplacements (autours de A2) qqs ms après l’affichage du point symétrique A2
        }
      }, stor.duree) // setTimeout
    } else { // le joueur est rentré dans le bas-côté
      inter.x = Math.round(inter.x) // pour éviter un bug dû à la gestion des réels par js (170.000000003 au lieu de 170)
      inter.y = Math.round(inter.y)

      joueur.cas = 3
      joueur.toursPenalite = ds.toursPenalite
      changeScore(stor.pointPerduBasCote)
      joueur.A2 = inter
      j3pMorpheSegment(segm, { x2: inter.x, y2: inter.y }, stor.duree) // [A1A2] (=[A1 inter]) du cas 3
      segm.id = 'segmentAGarder'

      testerFinPartie(A1, inter)

      setTimeout(function () { // on attend que j3pMorpheSegment ait fini son animation avant d’animer la suite
        const point = j3pCreePoint(pointsJ, {
          nom: '',
          x: inter.x,
          y: inter.y,
          taille: joueur.taille,
          couleur,
          epaisseur,
          decal: [4, 4]
        }) // [4,4] revient à mettre le nom pile poil sur le point, mais ce sera changé dans afficheLabelsPoints
        point.id = 'A2J' + stor.numJoueur
        flashElement(stor.overlay)
        secoueOverlay()
        const info = j3pElement('info')
        flashElement(info)

        if (joueur.gagne) {
          finPartie()
        } else {
          info.innerHTML = textes.phraseCrash + '<strong>' + (ds.toursPenalite + 1) + '</strong>' + textes.phraseCrashFin + (ds.toursPenalite + 1) + textes.phraseTours // "Vous êtes rentrés dans le bas-côté du circuit.<br>Il va vous falloir <strong>" + (ds.toursPenalite+1) + "</strong> tours pour remettre votre voiture en piste.<br><br>Choisissez l’emplacement d’où vous redémarrerez dans " + (ds.toursPenalite+1) + " tours.";
          j3pCreeSegment(segmentsAGarderJ, {
            id: 'segm',
            x1: inter.x,
            y1: inter.y,
            x2: inter.x,
            y2: inter.y,
            couleur,
            epaisseur: 1,
            opacite: 1,
            pointilles: '1,1'
          }) // segment limité à un point car sera agrandi avec j3pMorpheSegment en [A2A3] (cas 3)
          const emplacements = getEmplacements(inter, A1) // emplacements possibles autours du point d’intersection. emplacements contient au moins un point.
          setTimeout(clignoterDirections, 500, emplacements) // on créé les emplacements (autours de inter) qqs ms après l’affichage du point inter
        }
      }, stor.duree)
    }
  } // placerPoint

  function premierCoup () {
    // premier coup du joueur 1 ou 2 (numJoueur = number 1 ou 2) : on l’oblige à avancer d’une case
    const info = j3pElement('info')
    flashElement(info)
    flashElement(stor.encadreCircuit) // met le cadre entourant le SVG de la couleur du joueur

    animTourJoueur()

    info.style.color = couleurJoueurCourant()
    info.innerHTML = textes.phraseAvancerUneCase // "Pour la première étape, vous n’avez pas le choix : vous n’avancez que d’une case."
    setTimeout(me.sectionCourante.bind(me), 2000)
  } // premierCoup

  function coupApresCrash () {
    // coup qui suit le crash du joueur 1 ou 2 (numJoueur = number 1 ou 2) : on l’oblige à avancer d’une case
    const info = j3pElement('info')
    flashElement(info)
    flashElement(stor.encadreCircuit) // met le cadre entourant le SVG de la couleur du joueur

    const couleur = couleurJoueurCourant()
    info.style.color = couleur
    info.innerHTML = textes.phraseIlReste + '<strong>' + stor.joueur.toursPenalite + '</strong>' + textes.phraseIlResteFin // "Il vous reste encore <strong>" + joueur.toursPenalite + "</strong> tour(s) à attendre avant de pouvoir continuer la course.<br><br>Cliquez sur le bouton \"Passer\".";
    afficherBoutonPasser(stor.boutonPasser)
  } // coupApresCrash

  function styleUserSelect (texte) {
    // pour empêcher que le contenu de ce texte soit sélectionnable
    texte.style.MozUserSelect = 'none'
    texte.style.WebkitUserSelect = 'none'
    texte.style.MsUserSelect = 'none'
    texte.style.userSelect = 'none'
  }

  /**
   * Si bool est true ça affiche les labels à côté des points A0 et A1 du joueur en cours, sinon ça efface ces labels
   * @param bool
   */
  function afficheLabelsPoints (bool) {
    // afficheLabelsPoints début
    // si bool = true, alors ça affiche "A" et "B" à côté des points A0 et A1 du joueur en cours, sinon ça efface ces labels
    // si cette fonction est appelée au tout début, on affiche "J1" et "J2" à côté des premiers points (pour les 2 joueurs et non juste pour le joueur en cours)

    let rayon = Math.round(0.8 * stor.ecartQuadrillage + 2) // rayon du cercle permettant de définir l’écart entre le point et son label
    const ecartAncre = Math.round(0.2 * stor.ecartQuadrillage + 2) // pour gérer le fait que l’ancre du texte se trouve en bas à gauche du texte, et qu’elle change de place quand le texte change de taille

    if (me.questionCourante <= 2) { // on est au tout début : "J1" et "J2" sont déjà créés
      rayon = Math.round(rayon * 0.7) // on diminue un peu le rayon sinon le label semble loin du point (contrairement au cas où la droite est à 45°)

      for (let i = 1; i <= 2; i++) {
        const point = j3pElement('A1J' + i)
        const texte = point.getElementsByTagNameNS('http://www.w3.org/2000/svg', 'text')[0]
        const A1 = stor['J' + i].A1

        if (bool) {
          styleUserSelect(texte)
          texte.x.baseVal[0].value = Math.round(0 + A1.x) - ecartAncre
          texte.y.baseVal[0].value = Math.round(rayon + A1.y) + ecartAncre
        } else {
          texte.innerHTML = ''
        }
      }
      return
    }

    // cas où les points A0 et A1 existent : on place les labels "A" et "B" perpendiculairement à la direction de la droite [A0A1]
    const pointA0 = j3pElement('A0J' + stor.numJoueur)
    const pointA1 = j3pElement('A1J' + stor.numJoueur)
    const texteA0 = pointA0.getElementsByTagNameNS('http://www.w3.org/2000/svg', 'text')[0]
    const texteA1 = pointA1.getElementsByTagNameNS('http://www.w3.org/2000/svg', 'text')[0]

    if (bool) {
      styleUserSelect(texteA0)
      styleUserSelect(texteA1)
      texteA0.innerHTML = 'A'
      //            texteA0.style.stroke = "black";
      //            texteA0.style.strokeWidth = 1;
      texteA1.innerHTML = 'B'
      //            texteA1.style.stroke = "black";
      //            texteA1.style.strokeWidth = 1;

      const { A0, A1 } = stor.joueur
      let posX0, posY0, posX1, posY1
      if (A1.x == A0.x) { // droite verticale
        rayon = Math.round(rayon * 0.7) // on diminue un peu le rayon sinon le label semble loin du point (contrairement au cas où la droite est à 45°)
        posX0 = rayon
        posY0 = 0
        posX1 = rayon // ou -rayon si on veut alterner la position de chaque côté de [A0A1]
        posY1 = 0
      } else if (A1.y == A0.y) { // droite horizontale (donc la perpendiculaire est verticale)
        rayon = Math.round(rayon * 0.7) // on diminue un peu le rayon sinon le label semble loin du point (contrairement au cas où la droite est à 45°)
        posX0 = 0
        posX1 = 0
        posY0 = rayon
        posY1 = rayon // ou -rayon
      } else {
        let coeffDir = (A1.y - A0.y) / (A1.x - A0.x)
        coeffDir = -1 / coeffDir // coeff directeur de la droite perpendiculaire à [A0A1]
        // on détermine les coord du label pour A0
        posX0 = Math.sqrt(rayon * rayon / (1 + coeffDir * coeffDir)) // changement de variable : on cherche d’abord l’intersection de la droite et du cercle centrés sur l’origine
        posY0 = coeffDir * posX0
        // on détermine les coord du label pour A1
        posX1 = posX0 // changement de variable : on cherche d’abord l’intersection de la droite et du cercle centrés sur l’origine        //ou -posX0 pour alterner la position de chaque côté de [A0A1]
        posY1 = coeffDir * posX1
      } // if

      // on se replace autours de A0
      posX0 = Math.round(posX0 + A0.x) - ecartAncre
      posY0 = Math.round(posY0 + A0.y) + ecartAncre
      texteA0.x.baseVal[0].value = posX0
      texteA0.y.baseVal[0].value = posY0

      // on se replace autours de A1
      posX1 = Math.round(posX1 + A1.x) - ecartAncre
      posY1 = Math.round(posY1 + A1.y) + ecartAncre

      // les 2 lignes suivantes sont pour débuguer pour vérifier l’emplacement de l’ancre
      //            let point = j3pCreePoint(j3pElement("dessinSVG"), {id:"test",nom:"", x:posX0+ecartAncre, y:posY0-ecartAncre, taille:joueur.taille, couleur:"green", taillepolice: stor.taillePolice, epaisseur:1.5, decal:[4,4]});
      //            let point = j3pCreePoint(j3pElement("dessinSVG"), {id:"test",nom:"", x:posX1+ecartAncre, y:posY1-ecartAncre, taille:joueur.taille, couleur:"green", taillepolice: stor.taillePolice, epaisseur:1.5, decal:[4,4]});

      texteA1.x.baseVal[0].value = posX1
      texteA1.y.baseVal[0].value = posY1
    } else { // bool == false
      texteA0.innerHTML = ''
      texteA1.innerHTML = ''
    }
  } // afficheLabelsPoints

  function convertitCoord (x, y) {
    // convertit les coordonnées absolues x et y (number) en coordonnées dans le repère du SVG : renvoie un objet {x,y}

    const largeurCircuit = stor.largeurCircuit
    const hauteurCircuit = stor.hauteurCircuit
    const oH = stor.cadreG.offsetHeight // quand on est en mode portrait (donc avec de l’espace vide en dessous du SVG), oH = hauteur du SVG + hauteur de l’espace vide en dessous
    const oW = stor.cadreG.offsetWidth // quand on est en mode paysage (donc avec de l’espace vide à gauche du SVG), oW = largeur du SVG + largeur de l’espace vide à sa gauche
    const ratioSVG = largeurCircuit / hauteurCircuit
    const ratioCadreG = oW / oH
    let centreX, centreY
    // le if qui suit permet de détecter l’abs et l’ord du point où on a cliqué (dans le repère du SVG, et non dans celui de la page)
    if (ratioCadreG > ratioSVG) { // le cadreG est plus large que le SVG : il y a de l’espace vide à gauche du SVG. On est sûrement en mode paysage.
      const oWreel = oH * ratioSVG // largeur en px du dessin
      centreX = ((largeurCircuit / oWreel) * x + largeurCircuit - (largeurCircuit / oWreel) * oW) // soit f la fonction affine qui transforme l’abscisse en px à l’écran en l’abscisse en px dans le SVG ; on a alors f(oW-oWreel) = 0 et f(oW) = LARGCIRCUIT donc on trouve le coeff directeur et l’ord à l’origine ; puis on cherche f(e.layerX) = centreX
      centreY = y * hauteurCircuit / oH
    } else { // mode portrait
      centreX = x * largeurCircuit / oW
      const oHreel = oW / ratioSVG // hauteur en px du dessin
      centreY = y * hauteurCircuit / oHreel
    }

    return { x: centreX, y: centreY }
  }

  // fonction appelée par demanderPlacerSym quand le joueur clique à un emplacement du circuit
  // elle place une boule de la couleur du joueur à l’emplacement cliqué sur le SVG
  function placerSym (e) {
    const circuit = j3pElement('dessinSVG')
    const coordSVG = convertitCoord(e.layerX, e.layerY) // coordSVG contient les coord (dans le repère SVG) du point où on a cliqué
    const ordonneeLigneDepart = Math.round(determineCoordLigneDepart().y1)
    const delta = ordonneeLigneDepart % stor.ecartQuadrillage // ordonnée de la ligne de quadrillage horizontale la plus haute
    const centreX = Math.round(coordSVG.x / stor.ecartQuadrillage) * stor.ecartQuadrillage // on arrondit à stor.ECARTQUADR près
    const centreY = Math.round((coordSVG.y - delta) / stor.ecartQuadrillage) * stor.ecartQuadrillage + delta // on arrondit à stor.ECARTQUADR près

    if (centreY <= stor.hauteurCircuit && centreX >= 0) { // si on clique à gauche ou en dessous du circuit (malheureusement ça fait partie de dessinSVG), alors placer le symétrique sera redemandé
      circuit.removeEventListener('click', placerSym, false)
      stor.bouleSym = j3pBoule(j3pElement('dessinSVG'), {
        couleur: couleurJoueurCourant(),
        cx: centreX + 'px',
        cy: centreY + 'px',
        diametre: stor.tailleBoule
      })
      afficheLabelsPoints(false) // on efface les labels à côté des points A0 (labelisé A) et A1 (labelisé B)

      me.sectionCourante() // on appelle la correction pour vérifier que le symétrique est bien placé
    }
  } // placerSym

  function demanderPlacerSym () {
    const info = j3pElement('info')
    flashElement(info)
    flashElement(stor.encadreCircuit) // met le cadre entourant le SVG de la couleur du joueur

    const couleur = couleurJoueurCourant()
    info.style.color = couleur
    info.innerHTML = textes.phraseChoixSymetrique // "Cliquez sur l’emplacement du symétrique du point A par rapport au point B (même s’il est en dehors du circuit)";

    const circuit = j3pElement('dessinSVG')
    circuit.addEventListener('click', placerSym, false)
  } // demanderPlacerSym

  function redim () {
    // fonction appelée à chaque fois que la fenêtre du navigateur est redimensionnée, et lorsqu’on réaffiche l’overlay après l’avoir fermé.
    // Permet de redimensionner le cadre de droite à la bonne hauteur.

    // détermination de la hauteur du SVG pour définir cadreD
    const oH = stor.cadreG.offsetHeight
    const oW = stor.cadreG.offsetWidth
    const ratioSVG = stor.largeurCircuit / stor.hauteurCircuit
    const ratioCadreG = oW / oH
    if (ratioCadreG > ratioSVG) { // le cadreG est plus large que le SVG : il y a de l’espace vide à côté du SVG. On est sûrement en mode paysage.
      stor.cadreD.style.height = oH + 'px'
    } else { // mode portrait
      stor.cadreD.style.height = oW / ratioSVG + 'px'
    }
  } // redim

  function creeChemin (svg, { couleur, couleurRemplissage, d, epaisseur, id, opacite, opaciteRemplissage, pointilles }) {
    // j’ai pompé et modifié la fonction j3pCreeSegment de j3poutils.js
    // créé dans le svg (élément ou string) un chemin (path)
    // objet= {id,d,couleur,epaisseur,opacite,couleurRemplissage,opaciteRemplissage}
    // d : "M 20,50 C 29,59 19,62 18,66 z" par exemple (obtenu facilement grâce à inkscape par exemple)
    // couleur : chaine comme "red" ou "rgb(x,y,z)" x<255 ou
    // opacite : chaine comprise entre 0, 0.1 etc jusque "1.0"
    // epaisseur : entier
    // id

    const path = document.createElementNS('http://www.w3.org/2000/svg', 'path')
    if (id) path.setAttribute('id', id)
    path.setAttribute('d', d)
    let style = ''

    if (couleurRemplissage) {
      style += 'fill:' + couleurRemplissage + ';'
    } else {
      style += 'fill:none;'
    }
    if (opaciteRemplissage) style += 'fill-opacity:' + opaciteRemplissage + ';'
    if (couleur) style += 'stroke:' + couleur + ';'
    if (typeof epaisseur === 'number') style += 'stroke-width:' + epaisseur + ';'
    if (opacite) style += 'stroke-opacity:' + opacite + ';'

    if (pointilles) {
      style += 'stroke-dasharray:' + pointilles + ';'
    }

    path.setAttribute('style', style)

    if (typeof (svg) === 'string') {
      const elementconteneur = j3pElement(svg)
      elementconteneur.appendChild(path)
    } else {
      svg.appendChild(path)
    }

    return path
  } // fonction creeChemin

  function convertitEnAbsolu (noeuds) {
    // s’il y a des commandes relatives de courbes de bézier cubiques (c minuscule) dans noeuds (string), ça les convertit en absolues (C majuscule)
    // PS : si createSVGPathSegCurvetoCubicAbs (pour transformer un path contenant des coord relatives en coord absolues) était implémentée dans firefox, on aurait pu modifier le path directement
    //            x1,y1      x2,y2      x,y
    // Par ex, si noeuds = "M 1,1 C 2,2 3,3 4,4 M 10,10 c 1,1 2,2 3,3 c 100,100 200,200 300,300 z" ou plus clairement :
    // noeuds = "                      M 1,1
    //           C 2,2        3,3       4,4
    // z                              M 10,10
    //           c 1,1        2,2       3,3    -> cette ligne sera transformée en    C 11,11   12,12   13,13
    //           c 100,100  200,200   300,300  -> cette ligne sera transformée en    C 113,113   213,213   313,313
    // z";

    function strInAbs (s) {
      // renvoie l’abscisse (number) à partir de la string s
      // par ex strInAbs("123.4,56.78") -> 123.4
      return Number(s.substring(0, s.indexOf(',')))
    }

    function strInOrd (s) {
      // renvoie l’ordonnée (number) à partir de la string s
      // par ex strInAbs("123.4,56.78") -> 56.78
      return Number(s.substring(s.indexOf(',') + 1, s.length))
    }

    function estLettre (char) {
      // renvoie vrai si char est une lettre (cad pas un chiffre ni le signe "-" ni "+")
      return isNaN(char) && char !== '-' && char !== '+'
    }

    const d = noeuds.split(' ')
    let indexc = d.indexOf('c')

    let xOld, yOld, x, y, i, coord
    if (d[0] === 'm') {
      d[0] = 'M'
      if (!estLettre(d[2].charAt(0))) { // si d[2] correspond à des coordonnées relatives (et non pas une lettre), c’est qu’on commence par un segment (c’est le cas pour ligneDepart) et non juste par le crayon posé (ce qui est le cas pour circuit)
        xOld = strInAbs(d[1])
        yOld = strInOrd(d[1])
        x = Math.round((strInAbs(d[2]) + xOld) * 100000) / 100000 // arrondi à 10^-5 près pour éviter des bugs de calculs avec des nombres réels
        y = Math.round((strInOrd(d[2]) + yOld) * 100000) / 100000
        d[2] = x + ',' + y
      }
    }

    while (indexc !== -1) { // boucle car plusieurs "c" sont écrits (pour 2 sous-chemins)
      d[indexc] = 'C'
      xOld = strInAbs(d[indexc - 1])
      yOld = strInOrd(d[indexc - 1])

      for (i = indexc + 1; i < d.length; i++) { // on scanne à partir du "c" trouvé jusqu'à la fin du tableau, et on breakera quand on trouvera une autre lettre
        coord = d[i]
        if (estLettre(coord.charAt(0))) { // si le 1er char de coord est une lettre, on quitte la boucle for
          break
        }

        x = Math.round((strInAbs(coord) + xOld) * 100000) / 100000 // arrondi à 10^-5 près pour éviter des bugs de calculs avec des nombres réels
        y = Math.round((strInOrd(coord) + yOld) * 100000) / 100000
        d[i] = x + ',' + y
        if ((i - indexc) % 3 === 0) { // une fois qu’on a passé 2 points, le 3e point correspond au point final de la courbe de bézier mais aussi au point de départ de la suivante
          xOld = x
          yOld = y
          // j3pCreePoint(j3pElement("dessinSVG"), {nom:"", x:x, y:y, taille:joueur.taille, couleur:"blue", epaisseur:1, decal:[4,18]});   //point sur la courbe
          // } else {
          // j3pCreePoint(j3pElement("dessinSVG"), {nom:"", x:x, y:y, taille:joueur.taille, couleur:"red", epaisseur:1, decal:[4,18]});    //point de contrôle
        }
      } // for

      indexc = d.indexOf('c')
    } // while

    return d.join(' ')
  } // fin convertitEnAbsolu

  function creeOverlay () {
    // affiche un overlay dans lequel on affiche le svg maître

    // on créé l’overlay (div)
    //* *****************************************************************************
    stor.overlay = j3pAddElt(document.body, 'div', '', {
      style: {
        width: '100%',
        height: '100%',
        position: 'absolute',
        // on le cache au début
        display: 'none',
        backgroundColor: 'rgba(0,0,0,0.6)',
        overflowX: 'hidden' // masque les ascenseurs (sinon lors de animTourJoueur, un ascenceur horizontal est créé lorsque le texte sort de l’écran)
      }
    })

    // on créé le cadre de gauche qui contiendra le SVG
    //* *****************************************************************************
    stor.cadreG = j3pAddElt(stor.overlay, 'div', '', {
      style: {
        position: 'fixed',
        height: '95%',
        top: '10px',
        bottom: '10px',
        left: '10px',
        right: '280px', // laisse la place pour le cadre de droite
        minHeight: '400px'
      }
    })

    // on créé le SVG principal
    //* *****************************************************************************
    const dessinSVG = j3pCreeSVG(stor.cadreG, { id: 'dessinSVG', width: '100%', height: '100%' })
    dessinSVG.setAttribute('viewBox', '0 0 ' + stor.largeurCircuit + ' ' + stor.hauteurCircuit)
    dessinSVG.setAttribute('preserveAspectRatio', 'xMaxYMin meet')

    // rectangle entourant le circuit (utilisé uniquement pour faire un fond à dessinSVG)
    //* *****************************************************************************
    const circuit = j3pCreeGroupe(dessinSVG, 'circuit')

    stor.encadreCircuit = j3pCreeRectangle(circuit, {
      x: 0,
      y: 0,
      width: stor.largeurCircuit,
      height: stor.hauteurCircuit,
      couleurRemplissage: 'grey',
      epaisseur: '5px',
      couleur: 'lightgrey'
    }) // fill gris foncé, contour gris clair

    // on créé la ligne de départ. Le circuit sera créé dans creeCircuit(), une fois que les joueurs auront choisi leur circuit (si le paramétrage de la section leur a laissé cette possibilité).
    // liste des noeuds du path (obtenu grâce à inkscape) contenant des moveto (M), courbes de bézier cubiques (C pour coord absolues, c pour coord relatives)
    //* *****************************************************************************
    let noeuds = 'm 10.702337,190.48439 85.04773,0' // segment en fait, mais galère à créer avec j3pCreeSegment car dans la zone de dessin d’inkscape, (0;0) est en bas à gauche mais pour l’éditeur XML d’inkscape c’est en haut à gauche (comme pour firefox)
    noeuds = convertitEnAbsolu(noeuds) // nécessaire pour que la fonction intersection fonctionne bien
    stor.ligneDepart = creeChemin(circuit, { d: noeuds, couleur: 'black', epaisseur: '3px' }) // segment en fait, mais galère à créer avec j3pCreeSegment car dans la zone de dessin d’inkscape, (0;0) est en bas à gauche mais pour l’éditeur XML d’inkscape c’est en haut à gauche (comme pour firefox)

    // on créé le quadrillage
    //* *****************************************************************************
    const epaisseurQuadrillage = '0.2px'
    const couleurQuadrillage = '#800080'

    const quadrillage = j3pCreeGroupe(dessinSVG, 'quadrillage')
    const verticales = j3pCreeGroupe(quadrillage, 'verticales')
    const horizontales = j3pCreeGroupe(quadrillage, 'horizontales')

    for (let i = 0; i <= stor.largeurCircuit; i = i + stor.ecartQuadrillage) { // verticales
      j3pCreeSegment(verticales, {
        x1: i,
        y1: 0,
        x2: i,
        y2: stor.hauteurCircuit,
        couleur: couleurQuadrillage,
        epaisseur: epaisseurQuadrillage,
        opacite: 0.7
      })
    }

    const coordLigneDepart = determineCoordLigneDepart()
    const ordonneeLigneDepart = Math.round(coordLigneDepart.y1)
    const delta = ordonneeLigneDepart % stor.ecartQuadrillage // ordonnée de l’horizontale du quadrillage qui est la plus haute

    for (let i = delta; i <= stor.hauteurCircuit; i = i + stor.ecartQuadrillage) { // horizontales
      j3pCreeSegment(horizontales, {
        x1: 0,
        y1: i,
        x2: stor.largeurCircuit,
        y2: i,
        couleur: couleurQuadrillage,
        epaisseur: epaisseurQuadrillage,
        opacite: 0.7
      })
    }

    // on créé les ##points de départ
    //* *****************************************************************************
    j3pCreeGroupe(dessinSVG, 'segmentsAGarderJ1') // groupe vide pour l’instant
    j3pCreeGroupe(dessinSVG, 'segmentsAGarderJ2')
    // on récupère les abscisses des extrémités de la ligne de départ
    const x1 = coordLigneDepart.x1 // 10.702337
    const x2 = coordLigneDepart.x2 // 95.75007
    const nbEcart = ((x2 - x1) / stor.ecartQuadrillage) // longueur de la ligne de départ où l’unité serait l’écart entre 2 lignes de quadrillage

    // les points de départ sont vers le mileu de la ligne de départ, écartés l’un de l’autre soit de 2 unités, soit 4 unités (selon que la ligne de départ est grande ou non)
    if (nbEcart >= 3.5 && nbEcart < 6.5) {
      stor.J1.A1 = {
        x: Math.round(0.5 * (x1 + x2) / stor.ecartQuadrillage) * stor.ecartQuadrillage - stor.ecartQuadrillage,
        y: ordonneeLigneDepart
      }
      stor.J2.A1 = { x: stor.J1.A1.x + 2 * stor.ecartQuadrillage, y: ordonneeLigneDepart }
    } else {
      stor.J1.A1 = {
        x: Math.round(0.5 * (x1 + x2) / stor.ecartQuadrillage) * stor.ecartQuadrillage - 2 * stor.ecartQuadrillage,
        y: ordonneeLigneDepart
      }
      stor.J2.A1 = { x: stor.J1.A1.x + 4 * stor.ecartQuadrillage, y: ordonneeLigneDepart }
    }

    const pointsJ1 = j3pCreeGroupe(dessinSVG, 'pointsJ1')
    const pointsJ2 = j3pCreeGroupe(dessinSVG, 'pointsJ2')
    const A1J1 = j3pCreePoint(pointsJ1, {
      nom: 'J1',
      x: stor.J1.A1.x,
      y: stor.J1.A1.y,
      taille: stor.J1.taille,
      couleur: couleurJ1,
      taillepolice: stor.taillePolice,
      epaisseur,
      decal: [4, 4]
    })
    A1J1.id = 'A1J1'
    const A1J2 = j3pCreePoint(pointsJ2, {
      nom: 'J2',
      x: stor.J2.A1.x,
      y: stor.J2.A1.y,
      taille: stor.J2.taille,
      couleur: couleurJ2,
      taillepolice: stor.taillePolice,
      epaisseur,
      decal: [4, 4]
    })
    A1J2.id = 'A1J2'
    afficheLabelsPoints(true) // pour bien positionner les étiquettes

    // on créé le cadre de droite qui contiendra les infos textuelles et les boutons
    //* *****************************************************************************
    stor.cadreD = j3pDiv(stor.overlay, {
      style: {
        position: 'fixed',
        width: '260px',
        top: '10px',
        bottom: '10px',
        right: '10px',
        // minHeight: "400px",  //inutile car pour le cadre de gauche, minHeight est déjà défini et il influe sur le cadre de droite
        background: 'grey'
      }
    })

    window.addEventListener('resize', redim, false)

    // on créé les zones de score des joueurs
    //* *****************************************************************************
    // création des conteneurs
    const conteneurJ1 = j3pDiv(stor.cadreD, {
      id: 'conteneurJ1',
      contenu: '',
      style: {
        width: '48%',
        height: '60px',
        border: '3px solid' + couleurJ1,
        margin: '1%',
        position: 'relative'
      }
    })
    const conteneurJ2 = j3pDiv(stor.cadreD, {
      id: 'conteneurJ2',
      contenu: '',
      style: {
        width: '48%',
        height: '60px',
        border: '3px solid' + couleurJ2,
        margin: '1%',
        position: 'relative'
      }
    })
    conteneurJ1.style.verticalAlign = 'top'
    conteneurJ2.style.verticalAlign = 'top'
    conteneurJ1.style.display = 'inline-block'
    conteneurJ2.style.display = 'inline-block'

    const clipJ1 = j3pDiv('conteneurJ1', {
      id: 'clipJ1',
      contenu: '',
      coord: [25, 25],
      style: { width: '46%', position: 'absolute' }
    })
    const clipJ2 = j3pDiv('conteneurJ2', {
      id: 'clipJ2',
      contenu: '',
      coord: [25, 25],
      style: { width: '46%', position: 'absolute' }
    })
    clipJ1.style.clip = 'rect(0px, 120px, 30px, 0px)'
    clipJ2.style.clip = 'rect(0px, 120px, 30px, 0px)'

    // création des titres
    const texteJ1 = j3pDiv('conteneurJ1', {
      id: 'texteJ1',
      contenu: textes.phraseScoreJ1, // "Score J1"
      style: me.styles.etendre('petit.enonce', { height: '50%', width: '100%', color: couleurJ1 })
    })
    const texteJ2 = j3pDiv('conteneurJ2', {
      id: 'texteJ2',
      contenu: textes.phraseScoreJ2, // "Score J2"
      style: me.styles.etendre('petit.enonce', { height: '50%', width: '100%', color: couleurJ2 })
    })
    texteJ1.style.textAlign = 'center'
    texteJ2.style.textAlign = 'center'
    styleUserSelect(texteJ1)
    styleUserSelect(texteJ2)

    // création des scores
    const scoreJ1 = j3pDiv('clipJ1', {
      id: 'scoreJ1',
      contenu: '<strong>0</strong>',
      style: me.styles.etendre('petit.enonce', { width: '100%', color: couleurJ1 })
    }) // div étant animé plus tard par la fonction changeScore
    const scoreJ2 = j3pDiv('clipJ2', {
      id: 'scoreJ2',
      contenu: '<strong>0</strong>',
      style: me.styles.etendre('petit.enonce', { width: '100%', color: couleurJ2 })
    })
    scoreJ1.style.textAlign = 'center'
    scoreJ2.style.textAlign = 'center'
    scoreJ1.style.display = 'inline-block'
    scoreJ2.style.display = 'inline-block'
    styleUserSelect(scoreJ1)
    styleUserSelect(scoreJ2)

    // on créé la zone d’info textuelle
    //* *****************************************************************************

    // création de infoTour (zone indiquant à quel joueur c’est le tour)
    const infoTour = j3pDiv(stor.cadreD, {
      id: 'infoTour',
      contenu: '',
      style: me.styles.etendre('petit.enonce', { width: '100%', padding: '5px' })
    })
    infoTour.style.overflow = 'auto'
    infoTour.style.textAlign = 'center'
    styleUserSelect(infoTour)

    // création de info (zone de consignes)
    const info = j3pDiv(stor.cadreD, {
      id: 'info',
      contenu: textes.phraseBienvenue + '<br>    <strong>"' + textes.phraseTitre + '"</strong> !!', // "Bienvenue dans :<br>    <strong>\"Grand prix symétrie\"<\strong> !!"
      style: me.styles.etendre('petit.enonce', { width: '100%', height: '45%', color: 'aqua', padding: '5px' })
    })
    info.style.overflow = 'auto'
    info.style.textAlign = 'justify'
    info.style.maxHeight = '400px'
    styleUserSelect(info)

    const btnProps = { className: 'MepBoutonsFenetre', style: { margin: '20px auto', display: 'block' } }

    // bouton "Passer"
    stor.boutonPasser = j3pAddElt(stor.cadreD, 'button', textes.phrasePasser, btnProps)
    stor.boutonPasser.style.visibility = 'hidden' // pas display none pour qu’il conserve son espace (les boutons qui suivent ne changeront pas de place quand il sera visible)
    stor.boutonPasser.onclick = () => {
      cacherBoutonPasser()
      me.sectionCourante()
    }

    // bouton "Désactiver les animations"
    stor.boutonDesactiveAnim = j3pAddElt(stor.cadreD, 'button', textes.phraseDesactiverAnimation, btnProps)
    stor.boutonDesactiveAnim.onclick = basculeAnim

    // bouton "Revoir les règles du jeu" (qui devient quitter quand c’est fini
    stor.boutonReglesJeu = j3pAddElt(stor.cadreD, 'button', textes.phraseRevoirRegles, btnProps)
    stor.boutonReglesJeu.onclick = function () {
      j3pBasculeAffichage(stor.overlay)
      me.cacheBoutonSuite()
    }
  } // creeOverlay

  /**
     * tracer la courbe de Bézier cubique où p1 et p4 sont les extrémités et p3 et p4 sont les points de contrôle (coordonnées absolues)
     * inutilisé
     * @param {point2D} p1
     * @param {point2D} p2
     * @param {point2D} p3
     * @param {point2D} p4
     * @param {string} couleur
     * /
    function tracerBezier3 (p1, p2, p3, p4, couleur) {
      let d = 'M ' + p1.x + ',' + p1.y + ' C' + p2.x + ',' + p2.y + ' ' + p3.x + ',' + p3.y + ' ' + p4.x + ',' + p4.y
      return creeChemin(j3pElement('dessinSVG'), { id: 'bezier3', d: d, couleur: couleur, epaisseur: '1px' })
    } /* */

  /**
     * détermine l’intersection du paramètre path avec le paramètre ligne <br>
     * Merci à http://svground.fr/
     * @param {string} path (ou élément SVG de type path) qui ne possède que des M ou des C (pas de commandes relatives, cad on a déjà utilisé convertitEnAbsolu(path)) ou alors qu’un M et un L (dans le cas de "ligneDepart")
     * @param {string} ligne (ou élément SVG de type line)
     * @returns {array} Un tableau de Point2D
     */
  function intersection (path, ligne) {
    /* Les 3 lignes qui suivent fonctionnent sous chromium mais pas sous firefox (v34) car isPointInFill n’y est pas encore implémenté donc il a fallu implémenter une fonction équivalente (un peu plus puissante) : "intersection")
          let point = j3pElement("dessinSVG").createSVGPoint();
          point.x = 70;    point.y = 30;
          */

    function convertitNoeudsStringEnArray (noeudsOld) {
      // à partir d’une string comme par ex noeudsOld = "M 326.11703,10.887875 C 407.84881,5.11857 420.34897,34.92664 435.73379,74.35021 451.11859,114.73533 431.88758,163.7744 404.96416,191.65936 435.73379,213.77502 489.5806,280.12198 436.69533,362.81531 C 383.81005,444.5471 115.53748,375.31546 36.690337,270.50647 1.1129671,219.54432 5.9207271,158.96665 27.074837,121.46618 94.383367,21.464931 243.4237,16.657178 326.11703,10.887875 Z M 172.26895,114.73533 C 122.26833,122.42774 81.8832,177.23611 100.15265,211.85193 118.42214,246.4677 156.88414,267.62182 197.26926,285.89127 238.61594,304.16072 287.655,332.04571 338.61719,325.31483 389.57936,319.54553 359.77131,266.66026 337.65564,251.27545 314.57842,234.9291 293.42432,199.35179 298.23207,183.00541 305.92447,157.04355 333.80943,136.85098 349.19425,108.00446 361.69438,76.27331 221.30803,107.04292 172.26895,114.73533 z"
      // renvoie un tableau d’objets qui sont par ex {type:2, x:1, y:2} ou {type:4, x:3, y:4} ou {type:6, x1:10, y1:11, x2:12, y2:13, x:14, y:15}
      // ne sert que pour la compatibilité des navigateurs (comme chromium) qui ne supportent pas la méthode pathSegList de path

      // let noeudsOld = "M 326.11703,10.887875 C 407.84881,5.11857 420.34897,34.92664 435.73379,74.35021 451.11859,114.73533 431.88758,163.7744 404.96416,191.65936 435.73379,213.77502 489.5806,280.12198 436.69533,362.81531 C 383.81005,444.5471 115.53748,375.31546 36.690337,270.50647 1.1129671,219.54432 5.9207271,158.96665 27.074837,121.46618 94.383367,21.464931 243.4237,16.657178 326.11703,10.887875 Z M 172.26895,114.73533 C 122.26833,122.42774 81.8832,177.23611 100.15265,211.85193 118.42214,246.4677 156.88414,267.62182 197.26926,285.89127 238.61594,304.16072 287.655,332.04571 338.61719,325.31483 389.57936,319.54553 359.77131,266.66026 337.65564,251.27545 314.57842,234.9291 293.42432,199.35179 298.23207,183.00541 305.92447,157.04355 333.80943,136.85098 349.19425,108.00446 361.69438,76.27331 221.30803,107.04292 172.26895,114.73533 z";
      // let noeudsOld = "M 10.7,19.48 85,1"; //ligneDepart
      noeudsOld = noeudsOld.match(/[MCZzL] ?[0-9.,\- ]*/g) // on transforme la string en ["M 326.11703,10.887875 ","C 407.84881,5.11857 420.34897,34.92664.........703,10.887875 ","Z ",...,"z"]

      // on duplique le tableau
      const noeuds = []
      let i, noeud
      for (i = 0; i < noeudsOld.length; i++) {
        noeuds[i] = noeudsOld[i]
      }

      // on transforme chaque élément noeud du tableau noeuds en tableau : par ex "M 1.1,2.2 " en ["M" , "1.1,2.2"]
      for (i = 0; i < noeuds.length; i++) {
        noeud = noeuds[i] // noeud est du type string
        noeud = noeud.split(' ') // noeud est maintenant du type array
        if (noeud[noeud.length - 1] == '') noeud.pop() // si noeud finissait par un espace comme dans "M 1.1,2.2 " alors le tableau noeud est ["M" , "1.1,2.2" , ""] donc on supprime le dernier élément
        noeuds[i] = noeud
      }

      // chaque élément noeud (array) du tableau noeuds est lu pour créer le tableau listPointsAlt :
      // si par ex noeud = ["M" , "1.1,2.2"] alors l’élément de listPointsAlt sera {type:2, x:1.1, y:2.2} (c’est M)
      // si noeud = ["M" , "1,2", "3,4"] alors 2 éléments de listPointsAlt seront créés : {type:2, x:1, y:2} (c’est M) et {type:4, x:3, y:4} (c’est L)
      // si noeud = ["C" , "1,2", "3,4", "5,6", "10,11", "12,13", "14,15", ...] alors plusieurs éléments de listPointsAlt seront créés : {type:6, x1:1, y1:2, x2:3, y2:4, x:5, y:6} (c’est C) et {type:6, x1:10, y1:11, x2:12, y2:13, x:14, y:15}, etc...

      function extraitCoord (s) {
        // renvoie {x:floatant, y:floatant} où x et y sont extraits de s = "x,y"
        const coord = s.split(',')
        return {
          x: parseFloat(coord[0]),
          y: parseFloat(coord[1])
        }
      }

      const listPointsAlt = []
      let point, point2, point3
      for (i = 0; i < noeuds.length; i++) {
        point = {}
        noeud = noeuds[i]

        switch (noeud[0]) {
          case 'M':
            point = extraitCoord(noeud[1])
            point.type = 2 // le couple de coordonnées juste après le M correspondent bien au M
            listPointsAlt.push(point)

            // s’il y a un autre couple de coordonnées après le premier couple, il correspond à un L
            if (noeud[2]) {
              // point = {}; //sinon la variable point est passée en référence et lorsque sa 2e affectation la modifie (ci-dessous), ça change le contenu du tableau listPointsAlt qui avait été créé avant
              point = extraitCoord(noeud[2])
              point.type = 4

              listPointsAlt.push(point)
            }

            if (noeud[3]) console.error(Error('Cas non pris en charge par le programme : il y a au moins 3 couples de coordonnées après un noeud M. Le tableau de noeuds est :'), noeuds, 'et le noeud posant problème a pour index ' + i)
            break

          case 'C':
            for (let j = 1; j < noeud.length - 2; j = j + 3) { // tous les 3 couples de coordonnées, c’est un nouveau point de type C qu’il faut créer
              point = extraitCoord(noeud[j])
              point2 = extraitCoord(noeud[j + 1])
              point3 = extraitCoord(noeud[j + 2])
              point.x1 = point.x
              point.y1 = point.y
              point.x2 = point2.x
              point.y2 = point2.y
              point.x = point3.x
              point.y = point3.y
              point.type = 6

              listPointsAlt.push(point)
            }

            break

          case 'Z': // ne rien faire car ce noeud (qui ferme la courbe) n’a aucune incidence sur la détermination des intersections
            break

          case 'z': // ne rien faire car ce noeud (qui ferme la courbe) n’a aucune incidence sur la détermination des intersections
            break

          default:
            console.error(Error('Le noeud est d’un type non pris en charge par le programme. Le tableau de noeuds est :'), noeuds, 'et le noeud posant problème a pour index ' + i)
        }
      }

      return listPointsAlt
    } // fin convertitNoeudsStringEnArray

    if (typeof path === 'string') path = j3pElement(path)
    if (typeof ligne === 'string') ligne = j3pElement(ligne)

    const a1 = new Point2D(ligne.x1.baseVal.value, ligne.y1.baseVal.value)
    const a2 = new Point2D(ligne.x2.baseVal.value, ligne.y2.baseVal.value)

    const noeuds = path.getAttribute('d') // "M 326.11703,10.887875 C 407.84881,5.11857 420.34897,34.92664 435.73379,74.35021 451.11859,114.73533 431.88758,163.7744 404.96416,191.65936 435.73379,213.77502 489.5806,280.12198 436.69533,362.81531 C 383.81005,444.5471 115.53748,375.31546 36.690337,270.50647 1.1129671,219.54432 5.9207271,158.96665 27.074837,121.46618 94.383367,21.464931 243.4237,16.657178 326.11703,10.887875 Z M 172.26895,114.73533 C 122.26833,122.42774 81.8832,177.23611 100.15265,211.85193 118.42214,246.4677 156.88414,267.62182 197.26926,285.89127 238.61594,304.16072 287.655,332.04571 338.61719,325.31483 389.57936,319.54553 359.77131,266.66026 337.65564,251.27545 314.57842,234.9291 293.42432,199.35179 298.23207,183.00541 305.92447,157.04355 333.80943,136.85098 349.19425,108.00446 361.69438,76.27331 221.30803,107.04292 172.26895,114.73533 z"
    const listPointsAlt = convertitNoeudsStringEnArray(noeuds)

    const nombreItems = listPointsAlt.length

    const interTab = []
    let type, point, p1, p2, p3, p4, inter
    for (let i = 0; i < nombreItems; i++) {
      point = listPointsAlt[i]
      type = point.type

      if (type == 2) { // commande M
        p1 = new Point2D(point.x, point.y)
      } else if (type == 6) { // commande C (majuscule)
        if (!p1) p1 = p4 // si le point précédent n’est pas défini par un M, alors le 4e point de l’étape d’avant est le 1er de l’étape en cours
        p2 = new Point2D(point.x1, point.y1) // point de contrôle de la courbe de bézier
        p3 = new Point2D(point.x2, point.y2) // point de contrôle de la courbe de bézier
        p4 = new Point2D(point.x, point.y)
        //                tracerBezier3(p1, p2, p3, p4, "red");

        inter = Intersection.intersectBezier3Line(p1, p2, p3, p4, a1, a2)
        if (inter.status === 'Intersection') {
          for (let j = 0; j < inter.points.length; j++) { // scanne tous les points d’intersection de la courbe de Bézier cubique avec la ligne (car il peut y en avoir plusieurs si elle a une forme de "S")
            interTab.push(inter.points[j])
            //                        j3pCreePoint(j3pElement("pointsJ"+stor.numJoueur), {nom:Math.round(inter.points[j].x)+","+Math.round(inter.points[j].y), x:inter.points[j].x, y:inter.points[j].y, taille:joueur.taille, taillepolice: stor.taillePolice, couleur:"green", epaisseur:1, decal:[4,18]});
          }
        }

        p1 = null // on a finit d’affecter p1, p2, p3 et p4 donc à la prochaine étape il faut forcer la redéfinition de p1
      } else if (type == 4) { // commande L       dans le cas où path = "ligneDepart" dont d est du type "M 1,2 3,4" c’est à dire "M 1,2 L 3,4"
        p2 = p1 // p2 point de contrôle. Astuce pourrie pour que le path "ligneDepart" à base de M et de L soit vu comme une courbe de bézier cubique pour pouvoir y appliquer Intersection.intersectBezier3Line
        p4 = new Point2D(point.x, point.y)
        p3 = p4 // p3 point de contrôle
        //                tracerBezier3(p1, p2, p3, p4, "red");

        inter = Intersection.intersectBezier3Line(p1, p2, p3, p4, a1, a2)
        if (inter.status === 'Intersection') {
          interTab.push(inter.points[0]) // l’intersection de 2 lignes est un point tout au plus
          //                    j3pCreePoint(j3pElement("pointsJ"+stor.numJoueur), {nom:"i", x:inter.points[0].x, y:inter.points[0].y, taille:joueur.taille, taillepolice: stor.taillePolice, couleur:"green", epaisseur:1, decal:[10,10]});
        }
        p1 = null // inutile
      } // if fin    //normalement, il n’y a pas de pathSegType == 7 (lettre "c" coord relatives) car la variable noeuds a déjà été modifiée pour qu’il n’y ait que des "C" (coord absolues)
    } // for fin

    return interTab
  } // intersection

  function corrigerSym () {
    // corrigerSym début
    // corrige le point symétrique A2. Elle est appelée dans la partie "correction" de la stor.

    function symetrique (A, B) {
      // A et B sont deux points (objets = {x:number, y:number})
      // renvoie les coordonnées du point symétrique de A par rapport à B sous forme d’objet {x:number, y:number}
      return { x: 2 * B.x - A.x, y: 2 * B.y - A.y }
    }

    function barreBoule (x, y) { // barreBoule début
      // barre la boule située aux coord (x, y) à l’aide de deux traits rouges en biais
      const ecart = stor.ecartQuadrillage
      const groupe = j3pCreeGroupe(j3pElement('dessinSVG'), 'barresFaux')
      j3pCreeSegment(groupe, {
        id: 'barre1',
        x1: x - ecart / 2,
        y1: y + ecart * 5 / 8,
        x2: x + ecart / 2,
        y2: y - ecart * 3 / 8,
        couleur: me.styles.cfaux,
        epaisseur: 1
      })
      j3pCreeSegment(groupe, {
        id: 'barre2',
        x1: x - ecart / 2,
        y1: y + ecart * 3 / 8,
        x2: x + ecart / 2,
        y2: y - ecart * 5 / 8,
        couleur: me.styles.cfaux,
        epaisseur: 1
      })
    } // barreBoule fin

    function detruitAvecFondu (boule) {
      // detruit l’élément d’id indiqué (string) en le faisant disparaître progressivement
      if (typeof boule === 'string') boule = j3pElement(boule)
      boule.style.transitionProperty = 'opacity'
      boule.style.transitionDuration = Math.max(1, Math.round((stor.duree) / 1000)) + 's'
      boule.style.opacity = '0'
      setTimeout(j3pDetruit, Math.max(1000, stor.duree), boule)
    }

    const { bouleSym: { cx, cy } } = stor
    const A2eleve = { x: cx.baseVal.value, y: cy.baseVal.value }

    const { A0, A1 } = stor.joueur
    const A2 = symetrique(A0, A1)
    const bienPlace = (A2eleve.x == A2.x && A2eleve.y == A2.y)

    if (bienPlace) {
      changeScore(stor.pointGagneBonneReponse)
      setTimeout(j3pDetruit, stor.delai, stor.bouleSym)
      setTimeout(placerPoint, stor.delai, A2.x, A2.y)
    } else {
      flashElement(stor.overlay) // l’overlay devient rouge qqs ms pour montrer que l’élève s’est trompé
      changeScore(stor.pointPerduMauvaiseReponse)
      setTimeout(barreBoule, stor.delai, A2eleve.x, A2eleve.y)
      setTimeout(detruitAvecFondu, 50 + stor.delai * 4, stor.bouleSym) // 50 pour que les 3 lignes qui viennent soient lancées un peu après la ligne précédente, cad qu’on commence à faire disparaître les barres rouges (et la boule) APRÈS les avoir créées
      setTimeout(detruitAvecFondu, 50 + stor.delai * 4, 'barresFaux')
      setTimeout(placerPoint, stor.delai * 4, A2.x, A2.y)
    }
  } // corrigerSym fin

  function changeScore (delta) {
    // ajoute delta au score du joueur en cours, avec une animation CSS (score qui tourne)
    const divScoreId = 'scoreJ' + stor.numJoueur
    const divScoreJ = j3pElement(divScoreId)

    // si les animation sont désactivées c’est plus simple
    if (!stor.delai) {
      stor.joueur.score += delta
      divScoreJ.innerText = stor.joueur.score
      return
    }

    // sinon on crée un div qui remplacera l’autre à la fin de l’animation
    if (j3pElement('divScoreNew', null)) {
      // si un score est déjà en train d'être changé par cette animation (par ex il peut y avoir l’animation quand on
      // cherche le symétrique, puis celle quand on sort du circuit presque en même temps), alors on reviendra dans 500ms
      return setTimeout(() => changeScore(delta), 500)
    }

    const angle = 40
    // faut le faire asap pour que dans la function finPartie(), l’affichage du gagnant (en comparant les scores) soit le bon même si l’animation du score n’est pas finie
    stor.joueur.score += delta
    const sens = (delta > 0) ? '-' : '+'
    const autreSens = (delta < 0) ? '-' : '+'

    const divScoreNew = j3pAddElt('clipJ' + stor.numJoueur, 'div', stor.joueur.score, {
      id: 'divScoreNew',
      style: me.styles.etendre('petit.enonce', { width: '100%', position: 'absolute', color: couleurJoueurCourant(), fontWeight: 'bold' })
    })
    divScoreNew.style.textAlign = 'center'
    divScoreNew.style.display = 'inline-block'
    styleUserSelect(divScoreNew)
    divScoreNew.style.transform = 'rotate(' + autreSens + angle + 'deg)'
    divScoreNew.style.transformOrigin = '50% 500%'

    const duree = Math.ceil(stor.delai * 3 / 1000)
    divScoreJ.style.transitionProperty = 'transform'
    divScoreJ.style.transitionDuration = duree + 's'
    divScoreNew.style.transitionProperty = 'transform'
    divScoreNew.style.transitionDuration = duree + 's'

    divScoreJ.style.transform = 'rotate(' + sens + angle + 'deg)'
    divScoreNew.style.transform = 'rotate(' + sens + 0 + 'deg)'

    divScoreJ.style.transformOrigin = '50% 500%'
    divScoreNew.style.transformOrigin = '50% 500%'

    setTimeout(() => {
      j3pDetruit(divScoreJ)
      divScoreNew.id = divScoreId
    }, duree * 1000)
  } // changeScore

  function animTourJoueur () {
    if (!stor.delai) return // pas d’animation
    // animTourJoueur début
    // animation CSS au milieu de l’écran : texte tiré par une voiture et défilant de la gauche vers la droite en traversant l’écran pour indiquer le tour du joueur

    // on lance avec un petit délai, sinon quand on est en train d’utiliser le paragraphe "cheat" il ne fait pas l’animation du 1er joueur car l’overlay est affiché après avoir appelé cette fonction
    setTimeout(function () {
      const cadreDHeight = stor.cadreD.offsetHeight
      if (!cadreDHeight) return // si l’overlay est fermé, on ne fait pas l’animation

      // création du conteneur pour l’animation CSS défilant vers la droite
      const divConteneur = j3pDiv(stor.overlay, { id: 'divConteneur', contenu: '' })
      divConteneur.style.position = 'absolute'
      divConteneur.style.margin = Math.round(cadreDHeight / 2) + 'px auto' // on positionne dans la bande du milieu

      // création du texte "Tour du joueur 1" par ex
      const divTexte = j3pDiv('divConteneur', {
        id: 'divTexte',
        contenu: textes.phraseTourJoueur + stor.numJoueur,
        style: me.styles.grand.enonce
      }) // "Tour du joueur "
      divTexte.style.display = 'inline-block' // pour que divTexte et SVGAnim se mettent l’un à côté de l’autre (au lieu de l’un en dessous de l’autre)
      divTexte.style.verticalAlign = 'middle'
      divTexte.style.fontSize = '40px'
      divTexte.style.padding = '7px'
      const couleur = couleurJoueurCourant()
      divTexte.style.color = couleur
      divTexte.style.backgroundColor = 'rgba(255,255,255,0.7)' // blanc un peu transparent

      // création du conteneur SVG qui ne contiendra que la voiture
      const SVGAnim = j3pCreeSVG('divConteneur', { id: 'SVGAnim', width: '117px', height: '42px' }) // c’est ici qu’on gère le zoom de la voiture
      SVGAnim.style.display = 'inline-block'
      SVGAnim.style.verticalAlign = 'middle'
      SVGAnim.setAttribute('viewBox', '233 47 135 43') // on affiche la fenêtre au bon endroit de l’image SVG qui va être créée ensuite (cf fichier .svg)

      // création du dessin en SVG

      // création du remplissage par des couleurs
      let d = 'm 280.85196,76.138514 c -5.18624,0.173612 -10.37249,0.347223 -15.55873,0.520835 -1.63527,1.084797 -2.46862,3.173552 -2.23659,5.09398 0.22243,1.222064 1.29418,2.212278 2.5301,2.339964 2.3835,0.451725 4.80962,-0.137678 7.0841,-0.837049 2.73701,-0.920305 5.48597,-2.281577 7.28357,-4.615753 0.50124,-0.710559 1.03751,-1.507626 0.98564,-2.41719 -0.0293,-0.02826 -0.0588,-0.05653 -0.0881,-0.08479 z'
      creeChemin(SVGAnim, { id: 'colorRoueArr', d, couleur, couleurRemplissage: '#999999' })
      d = 'm 340.85196,76.138514 c -5.18624,0.173612 -10.37249,0.347223 -15.55873,0.520835 -1.63527,1.084797 -2.46862,3.173552 -2.23659,5.09398 0.22243,1.222064 1.29418,2.212278 2.5301,2.339964 2.3835,0.451725 4.80962,-0.137678 7.0841,-0.837049 2.73701,-0.920305 5.48597,-2.281577 7.28357,-4.615753 0.50124,-0.710559 1.03751,-1.507626 0.98564,-2.41719 -0.0293,-0.02826 -0.0588,-0.05653 -0.0881,-0.08479 z'
      creeChemin(SVGAnim, { id: 'colorRoueAvant', d, couleur, couleurRemplissage: '#999999' })
      d = 'm 342.75,77.2372 c 10.26704,0.0209 13.90536,1.74518 17.7589,-0.9852 3.8131,-2.70173 5.74228,-7.2533 2.07269,-8.35281 C 349.79184,64.06704 347.61021,64.50675 340.5,60.6122 c -3.12123,-1.709618 -10.96822,-8.865206 -14.86941,-9.70474 -17.01987,-3.66266 -43.43471,-1.69605 -49.27298,5.305665 -6.46752,7.756355 -22.45482,6.297715 -22.9681,13.215365 0.0346,5.17889 1.52421,9.29117 10.25,7.25'

      creeChemin(SVGAnim, {
        id: 'colorCarrosserie',
        d,
        couleur,
        couleurRemplissage: couleur
      })
      //            d = "m 302.92534,51.504657 c -5.11337,0.01629 -10.39554,0.46202 -15.07768,2.666175 -1.09958,0.66101 -2.23346,2.174926 -1.40685,3.444575 1.00244,1.030023 2.47236,1.462807 3.77119,2.020321 4.03493,1.419447 8.34469,1.708185 12.56862,2.128373 5.35169,0.367399 10.72315,0.42116 16.07444,0.147827 1.12156,-0.05729 2.25688,0.251234 3.40964,-0.0779 -1.18147,-2.919122 -2.30305,-6.316469 -3.45972,-9.252246 -4.5335,-0.643063 -9.11673,-0.858743 -13.68823,-1.034948 -0.73035,-0.01949 -1.46085,-0.0335 -2.19141,-0.04218 z";
      //            creeChemin(SVGAnim,{id:"colorFenetre", d:d, couleurRemplissage:"white"});

      // création des contours
      d = 'm 342.74973,77.37105 c 6.13568,-0.425329 12.82392,2.431064 18.50809,-0.962454 4.70131,-2.269852 5.0429,-8.937462 -1.23437,-9.813456 -8.23025,-2.005756 -14.41261,-1.215057 -24.98814,-10.317534 -4.31128,-3.478478 -8.60847,-6.417331 -14.34042,-6.899194 -13.01107,-1.09379 -26.88008,-1.766811 -40.42573,3.342483 -4.59961,1.73493 -7.20045,6.921869 -12.15447,8.232518 -4.77347,1.765966 -10.21389,2.435045 -14.09107,6.021398 -2.80495,4.473616 0.82352,11.130946 6.21409,10.393884 0.17997,-0.02461 0.35256,-0.03597 1.45638,-0.29456 -5.64034,1.0924 -10.58728,-5.408543 -7.04444,-9.734913 4.39118,-4.110231 10.94969,-3.787301 16.08095,-6.542105 4.64696,-1.833339 7.29834,-6.585015 12.21309,-7.914556 8.38998,-2.8849 17.42557,-3.223862 26.22193,-3.31057 6.04895,0.187865 12.47374,-0.06917 18.04419,2.331862 7.77638,3.351849 13.29284,11.048192 21.90131,12.787646 4.73978,1.420268 12.32901,2.769917 14.2365,4.210778 1.48813,4.618757 -3.47507,8.262441 -7.60254,8.602946 -4.31911,0.256831 -8.6998,-0.927568 -12.99535,-0.134173 z'
      creeChemin(SVGAnim, { id: 'carrosserie', d, couleur: 'black' })
      d = 'm 327.22564,61.987199 c 0.002,0.01845 -1.76726,0.44608 -4.96837,0.459511 -1.12596,0.0048 -2.87173,-0.0066 -4.38515,0.08641 -4.91503,0.300681 -12.66588,0.620365 -20.47643,-0.361674 -2.0939,-0.262951 -4.29563,-0.63158 -6.46445,-1.257105 -1.3618,-0.388928 -2.78608,-0.884438 -4.05151,-1.722985 -0.24979,-0.154564 -0.50148,-0.338021 -0.73336,-0.552639 -0.45705,-0.423043 -0.83175,-0.962212 -0.96986,-1.63275 -0.11071,-0.952771 0.38057,-1.783585 0.99561,-2.309922 2.47283,-2.221807 5.99106,-2.739496 8.82419,-3.19191 4.38168,-0.730539 8.94756,-0.516198 12.47417,-0.317954 0,0 0,10e-7 0,10e-7 9.75615,0.543441 15.89081,1.893707 15.88617,1.926025 -0.0363,0.252501 -6.20713,-0.84096 -15.91966,-1.216798 0,0 0,0 0,0 -3.51471,-0.134298 -8.0517,-0.294682 -12.2966,0.450776 -2.83355,0.517193 -6.21405,0.992979 -8.36254,3.010188 -0.44618,0.447111 -0.84195,1.012261 -0.71421,1.503022 0.0398,0.406442 0.32444,0.767119 0.69971,1.099856 0.19028,0.168715 0.403,0.329496 0.61595,0.48389 1.1309,0.761441 2.47313,1.221863 3.80086,1.613537 2.10105,0.613751 4.25858,0.983636 6.32616,1.258 7.67299,1.016998 15.46577,0.816756 20.33858,0.649065 1.5706,-0.05409 3.26966,0.01395 4.41913,0.05528 3.16104,0.114143 4.94735,-0.166785 4.96161,-0.03182 z'
      creeChemin(SVGAnim, { id: 'fenetre', d, couleur: 'black' })
      d = 'm 322.55487,76.731623 c 0,0.191776 -9.01783,0.505672 -20.14576,0.683386 -11.13472,0.177821 -20.16276,-0.02475 -20.15933,-0.329833 0.002,-0.191725 9.02178,-0.392243 20.14496,-0.569941 11.13157,-0.177831 20.16013,-0.08873 20.16013,0.216388 z'
      creeChemin(SVGAnim, { id: 'sousVoiture', d, couleur: 'black' })
      d = 'm 263.72877,78.212585 c -0.25245,1.424379 -1.01027,2.920121 -0.36231,4.330648 0.75796,1.30246 2.40802,1.597553 3.79277,1.645482 2.25299,0.01023 4.42972,-0.687087 6.54893,-1.378259 2.66012,-0.99694 5.15905,-2.650638 6.76275,-5.063408 0.28463,-0.428222 0.45327,-1.071123 0.90267,-1.883455 0.17445,1.164798 -0.0495,2.051276 -0.47554,2.938254 -1.11315,2.317335 -3.36393,3.827631 -5.57385,5.012664 -3.17626,1.589993 -6.92269,2.411621 -10.41136,1.435673 -2.02856,-0.530293 -3.21154,-2.906715 -2.58546,-4.872781 0.23191,-0.823575 0.82533,-1.53745 1.4014,-2.164818 z'
      creeChemin(SVGAnim, { id: 'roueArr', d, couleur: 'black' })
      d = 'm 323.72877,78.212585 c -0.25245,1.424379 -1.01027,2.920121 -0.36231,4.330648 0.75796,1.30246 2.40802,1.597553 3.79277,1.645482 2.25299,0.01023 4.42972,-0.687087 6.54893,-1.378259 2.66012,-0.99694 5.15905,-2.650638 6.76275,-5.063408 0.28463,-0.428222 0.45327,-1.071123 0.90267,-1.883455 0.17445,1.164798 -0.0495,2.051276 -0.47554,2.938254 -1.11315,2.317335 -3.36393,3.827631 -5.57385,5.012664 -3.17626,1.589993 -6.92269,2.411621 -10.41136,1.435673 -2.02856,-0.530293 -3.21154,-2.906715 -2.58546,-4.872781 0.23191,-0.823575 0.82533,-1.53745 1.4014,-2.164818 z'
      creeChemin(SVGAnim, { id: 'roueAvant', d, couleur: 'black' })
      d = 'm 330.65918,78.846139 c 1.02802,0.239605 2.23698,-0.431622 2.91915,-1.243493 0.9422,0.892469 -0.21256,1.762662 -0.92629,2.332999 -0.84009,0.480045 -2.03845,0.58799 -2.78908,-0.11319 -0.41186,-0.432133 -0.46891,-1.396851 0.11233,-1.709147 0.1852,0.27769 0.44755,0.677745 0.68389,0.732831 z'
      creeChemin(SVGAnim, { id: 'enjolAvant', d, couleur: 'black' })
      d = 'm 270.65918,78.846139 c 1.02802,0.239605 2.23698,-0.431622 2.91915,-1.243493 0.9422,0.892469 -0.21256,1.762662 -0.92629,2.332999 -0.84009,0.480045 -2.03845,0.58799 -2.78908,-0.11319 -0.41186,-0.432133 -0.46891,-1.396851 0.11233,-1.709147 0.1852,0.27769 0.44755,0.677745 0.68389,0.732831 z'
      creeChemin(SVGAnim, { id: 'enjolArr', d, couleur: 'black' })
      d = 'M 253.49778,68.599895 233.08007,62.932633'
      creeChemin(SVGAnim, { id: 'cableHaut', d, couleur: 'black' })
      d = 'm 253.33173,72.010429 -20.25166,3.781854'
      creeChemin(SVGAnim, { id: 'cableBas', d, couleur: 'black' })

      divConteneur.style.left = -divConteneur.offsetWidth + 'px' // au début, le texte est décalé d’autant de px que sa taille à gauche de l’écran afin qu’il soit en dehors  //(à placer après avoir créé son contenu car celui-ci change sa width)
      // ligne suivante pour debuguage pour ne pas que le SVG soit hors de l’écran
      //            divConteneur.style.left = 100+"px";

      // création de l’animation en CSS
      const largeurOverlay = stor.overlay.offsetWidth
      divConteneur.style.transitionDuration = '2s'
      divConteneur.style.transitionTimingFunction = 'cubic-bezier(0.3,1,0.7,0)'
      divConteneur.style.transform = 'translateX(' + (largeurOverlay + divConteneur.offsetWidth) + 'px)'
      setTimeout(j3pDetruit, 2100, 'divConteneur') // on détruit 2,1s après, cad quand l’animation est finie
    }, 200)

    // change le div d’id #infoTour pour indiquer à qui c’est le tour de jouer (sans attendre la fin de l’anim)
    const infoTour = j3pElement('infoTour')
    infoTour.style.color = couleurJoueurCourant(stor.numJoueur)
    infoTour.innerHTML = '<strong>' + textes.phraseTourJoueur + stor.numJoueur // "Tour du joueur "
  } // animTourJoueur

  function init () {
    // définition des constantes et initialisation des variables
    stor.largeurCircuit = 470
    stor.hauteurCircuit = 410
    stor.ecartQuadrillage = Math.min(Math.max(5, ds.ecartQuadr), 20) // signifie qu’entre deux points de quadrillage il y a ECARTQUADR px (nombre compris entre 5 et 20) (10 par défaut)
    stor.taillePolice = Math.round(stor.ecartQuadrillage * 0.4) + 4
    stor.choixCircuit = '' // sera défini à "circuit1", "circuit2" ou "circuit3" si la section est paramétrée ainsi, ou lorsque les joueurs l’auront sélectionné dans la liste déroulante si la section est paramétrée à "libre"
    stor.tailleBoule = Math.ceil(stor.ecartQuadrillage * 2 / 3)
    /**
     *  numéro du joueur courant (1 ou 2)
     * @type {number}
     */
    stor.numJoueur = 1 // tour du joueur en cours
    /**
     * duree changée par basculeAnim entre 0 et dureeAnimation
     * @type {number}
     */
    stor.duree = dureeAnimation // variable (qui peut être changée par basculeAnim)
    stor.delai = delaiAnimation // variable (qui peut être changée par basculeAnim)
    /** nombre de tours de pénalité pour le joueur qui est sorti du circuit (cad qu’il redémarrera dans toursPenalite+1 tours au */
    if (!Number.isInteger(ds.toursPenalite) || ds.toursPenalite < 0) {
      console.error(Error('Paramètre toursPenalite invalide => 1 imposé'))
      ds.toursPenalite = 1
    }
    stor.pointGagneBonneReponse = Math.max(0, ds.pointGagneBonneReponse) // points gagnés quand le joueur clique sur le bon symétrique (1 par défaut)
    stor.pointPerduMauvaiseReponse = Math.min(0, ds.pointPerduMauvaiseReponse) // points perdus quand le joueur clique sur le mauvais symétrique (-1 par défaut)
    stor.pointPerduBasCote = Math.min(0, ds.pointPerduBasCote) // points perdus quand le joueur rentre dans le bas côté (-1 par défaut)
    stor.pointGagneLigneArrivee = Math.max(0, ds.pointGagneLigneArrivee) // points gagnés quand le joueur franchi la ligne d’arrivée (5 par défaut)

    stor.J1 = {
      A0: {},
      A1: {},
      A2: {},
      A3: {},
      taille: Math.round(stor.ecartQuadrillage * 0.8),
      cas: 1,
      score: 0,
      toursPenalite: 0,
      gagne: false
    }
    stor.J2 = {
      A0: {},
      A1: {},
      A2: {},
      A3: {},
      // pas la même taille de croix que J1 car quand les croix se superposent, celle de J2 est forcément en avant-plan
      taille: Math.round(stor.ecartQuadrillage * 0.6),
      cas: 1,
      score: 0,
      toursPenalite: 0,
      gagne: false
    }
    stor.joueur = stor.J1

    stor.reglesJeu = addElement(me.zonesElts.MG, 'div')
    stor.reglesJeu.innerHTML = '<h1>' + textes.phraseRegles1 + '</h1><br><br><em>' + textes.phraseTitre + '</em>' + textes.phraseRegles2 + '<br><br>' +
              textes.phraseRegles3 +
              '<ul><li>' + textes.phraseRegles4 + stor.pointGagneBonneReponse + textes.phrasePoint + '</li>' +
              '<li>' + textes.phraseRegles5 + stor.pointGagneLigneArrivee + textes.phrasePoints + '</li></ul><br>' +
              textes.phraseRegles6 +
              '<ul><li>' + textes.phraseRegles7 + stor.pointPerduMauvaiseReponse + textes.phrasePoint + '</li>' +
              '<li>' + textes.phraseRegles8 + stor.pointPerduBasCote + textes.phrasePoint + '</li></ul><br>'

    if (ds.choixCircuit === 'libre') {
      const choixCircuit = j3pDiv(me.zonesElts.MG, { id: 'choixCircuit', contenu: textes.phraseChoixCircuit }) // "Sur quel circuit voulez-vous jouer ?"
      choixCircuit.style.position = '' // enlève "absolute"
      choixCircuit.style.margin = 'auto 20px'
      const listeDeroulante = j3pAddElt(choixCircuit, 'select', '', { style: { margin: '0 1rem' } })
      const circuits = [textes.phraseCircuit1, textes.phraseCircuit2, textes.phraseCircuit3]
      for (const txt of circuits) {
        // ["circuit1","circuit2","circuit3"]
        j3pAddElt(listeDeroulante, 'option', txt)
      }
      stor.choixCircuit = textes.phraseCircuit1 // "circuit1"    //on met le circuit1 par défaut
      listeDeroulante.addEventListener('change', (event) => {
        stor.choixCircuit = circuits[event.target.options.selectedIndex]
      })
    } else { // le prof a mis le paramètre choixCircuit à "circuit1", "circuit2" ou "circuit3"
      stor.choixCircuit = ds.choixCircuit
    }

    stor.reglesJeu.style.position = 'static' // enlève "absolute"
    stor.reglesJeu.style.margin = 'auto 20px'

    const boutonJouer = j3pAddElt(me.zonesElts.MG, 'button', 'JOUER', {
      className: 'MepBoutonsFenetre',
      style: {
        // on centre le bouton "JOUER" horizontalement
        display: 'block',
        margin: '0 auto', // 0 pour haut et bas, auto pour gauche et droite
        width: '250px'
      }
    })

    stor.creationGdPrix = false

    // au clic sur le bouton jouer on rappellera la section en mode énoncé
    boutonJouer.onclick = function () {
      // on vire le div qui contient le choix du circuit
      j3pDetruit('choixCircuit')
      // reste les règle du jeu, le bouton jouer devient continuer
      boutonJouer.innerText = textes.phraseContinuer
      boutonJouer.onclick = () => j3pBasculeAffichage(stor.overlay)

      // et on crée le svg que l’on affiche
      creeOverlay()
      creeCircuit()
      j3pBasculeAffichage(stor.overlay)
      redim()
      // et on veut rester en énoncé (pour activer le bouton commencer du joueur 1)
      me.etat = 'enonce'
      // et rester sur la 1re question, 1re étape (joueur 1)
      // me.repetitionCourante = 0
      // me.questionCourante = 0
      // me.etapeCourante = 0
      me.sectionCourante()
    }

    // important d’appeler finEnonce, même si on est obligé d’annuler une partie de ce qu’il fait (passer en correction et incrémenter les questions)
    me.finEnonce()
    // on veut pas du bouton OK, c’est le bouton jouer qui déclenche le lancement
    me.cacheBoutonValider()
    // et on ne veut pas non plus afficher numéro de question et score qui n’ont pas de sens ici
    me.zonesElts.question.style.display = 'none'
    me.zonesElts.score.style.display = 'none'
  } // init

  // raccourcis
  const me = this
  const ds = this.donneesSection
  const stor = this.storage
  this.logIfDebug('call avec l’état', this.etat, 'questionCourante', this.questionCourante)

  switch (this.etat) {
    case 'enonce': {
      // code exécuté au lancement de la section
      if (this.debutDeLaSection) {
        // les propriétés de donneesSection qui ne sont pas paramétrables
        this.surcharge({
          // Nombre de répétitions de l’exercice
          nbrepetitions: 99,
          nbetapes: 2,
          // Nombre de chances dont dispose l’élève pour répondre à la question
          nbchances: 2
        })
        // Construction de la page
        this.construitStructurePage('presentation3')
        this.score = 0
        this.afficheTitre(textes.phraseTitre) // "Grand prix symétrie"
        if (ds.indication) this.indication(this.zones.IG, ds.indication)

        init()
        return
      }

      /// //////////////////////////////////
      /* LE CODE PRINCIPAL DE LA SECTION */
      /// //////////////////////////////////
      stor.numJoueur = this.questionCourante % 2 === 0 ? 2 : 1
      stor.joueur = stor['J' + stor.numJoueur] // joueur est une référence à stor.J1 ou stor.J2
      const joueur = stor.joueur

      me.logIfDebug('Joueur ' + stor.numJoueur + ' :   ', joueur, 'question', me.questionCourante)

      // à mettre à la fin de l’énoncé normalement. Ça doit être appelé avant me.sectionCourante(); mais dans le cas où joueur.cas == 3 && joueur.toursPenalite == 0 , me.sectionCourante() est appelée manuellement (au lieu d'être appelée avec setTimeout) donc si j’avais laissé this.finEnonce() à la fin, ces 2 fonctions auraient été appelé dans le mauvais ordre
      this.finEnonce()

      // on veut pas du bouton ok mis par finEnonce
      this.cacheBoutonValider()

      if (stor.J1.gagne || stor.J2.gagne) {
        // un des joueurs a gagné
        console.error(Error('On est en état énoncé avec un joueur qui a gagné (on aurait dû passer en navigation directement en sortant de correction)'))
        // on passe automatiquement en navigation (pour que le bouton "Section suivante" s’affiche
        me.sectionCourante('navigation')
        return
      }

      // pour questionCourante, au début c’est particulier
      // 1 affichage des règles au départ
      // 2 affichage du circuit et message "bienvenue", avec passage automatique à 3 après 2s
      // 3 premier tour joueur 1
      // 4 premier tour joueur 2
      // ensuite les impairs correspondent au joueur 1 et les pairs au joueur2

      if (this.questionCourante === 2) {
        // on vient d’afficher le circuit, on laisse la phrase de bienvenue 1s
        setTimeout(() => me.sectionCourante('enonce'), 1000)
        return
      }

      // décommenter le if suit pour sauter directement à l’étape qui vous intéresse
      // if (this.questionCourante === 3) {
      // // à modifier
      //   const A1J1 = { x: 60, y: 240 }
      //   const A3J1 = { x: 60, y: 220 }
      //   const A1J2 = { x: 390, y: 90 }
      //   const A3J2 = { x: 390, y: 70 }
      //   j3pDetruit('A1J1')
      //   j3pDetruit('A1J2')
      //   j3pCreePoint(j3pElement('pointsJ1'), { nom: 'A', x: A1J1.x, y: A1J1.y, taille: stor.J1.taille, taillepolice: stor.taillePolice, couleur: couleurJ1, epaisseur, decal: [14, 4] }).id = 'A1J1'
      //   stor.J1.A0 = A1J1
      //   j3pCreePoint(j3pElement('pointsJ1'), { nom: 'B', x: A3J1.x, y: A3J1.y, taille: stor.J1.taille, taillepolice: stor.taillePolice, couleur: couleurJ1, epaisseur, decal: [14, 4] }).id = 'A3J1'
      //   stor.J1.A3 = A3J1
      //   j3pCreePoint(j3pElement('pointsJ2'), { nom: 'A', x: A1J2.x, y: A1J2.y, taille: stor.J2.taille, taillepolice: stor.taillePolice, couleur: couleurJ2, epaisseur, decal: [14, 4] }).id = 'A1J2'
      //   stor.J2.A1 = A1J2
      //   j3pCreePoint(j3pElement('pointsJ2'), { nom: 'B', x: A3J2.x, y: A3J2.y, taille: stor.J2.taille, taillepolice: stor.taillePolice, couleur: couleurJ2, epaisseur, decal: [14, 4] }).id = 'A3J2'
      //   stor.J2.A3 = A3J2
      //   // et on redémarre de là
      //   me.questionCourante = 42
      //   me.etat = 'enonce'
      //   return me.sectionCourante()
      // } // cheat fin

      if (this.questionCourante < 5) {
        // c’est le premier tour du joueur 1 (q3) ou 2 (q4)
        joueur.cas = 1
        premierCoup()
        return
      }

      animTourJoueur()

      const A0 = j3pElement('A0J' + stor.numJoueur, null) // A0 n’existe pas au premier coup

      // le joueur est sorti du circuit au tour précédent
      if (joueur.cas === 3) {
        if (joueur.toursPenalite === 0) {
          afficheLabelsPoints(false) // efface les étiquettes
          if (A0) A0.id = ''
          joueur.A0 = {}
          // le point A3 (le joueur se repositionne dans le circuit, sur le quadrillage) devient A2 (centre à partir duquel clignotent les multiples emplacements pour avancer d’une case)
          j3pElement('A3J' + stor.numJoueur).id = 'A2J' + stor.numJoueur
          joueur.A2 = joueur.A3
          joueur.A3 = {} // inutile
          joueur.cas = 4

          me.sectionCourante()
          return
        }
        coupApresCrash()
        joueur.toursPenalite -= 1
      } else if (joueur.cas === 4) {
        joueur.cas = 2 // il faut revenir au cas normal

        // le point A2 devient A0
        j3pElement('A2J' + stor.numJoueur).id = 'A0J' + stor.numJoueur
        joueur.A0 = joueur.A2
        joueur.A2 = {} // inutile

        // le point A3 devient A1
        j3pElement('A1J' + stor.numJoueur).id = '' // on efface l’id de l’ancien A1
        j3pElement('A3J' + stor.numJoueur).id = 'A1J' + stor.numJoueur
        joueur.A1 = joueur.A3
        joueur.A3 = {} // inutile

        afficheLabelsPoints(true) // affiche "A" à côté du point A0 et "B" à côté du point "A1"

        demanderPlacerSym()

        // cas 2 : le joueur n’est pas sorti du circuit au tour précédent
      } else {
        if (A0) {
          A0.id = ''
          A0.getElementsByTagNameNS('http://www.w3.org/2000/svg', 'text')[0].innerHTML = '' // efface l’étiquette "A" du point A0
        }
        // le point A1 devient A0
        j3pElement('A1J' + stor.numJoueur).id = 'A0J' + stor.numJoueur
        joueur.A0 = joueur.A1

        // j3pElement("A2J"+stor.numJoueur).id = "";  //inutile car A2 a été détruit lors de la création de A3

        // le point A3 devient A1
        j3pElement('A3J' + stor.numJoueur).id = 'A1J' + stor.numJoueur
        joueur.A1 = joueur.A3
        joueur.A3 = {} // inutile

        afficheLabelsPoints(true) // affiche "A" à côté du point A0 et "B" à côté du point "A1"

        demanderPlacerSym()
      }

      break // case "enonce":
    }

    case 'correction': {
      const joueur = stor['J' + stor.numJoueur] // joueur est une référence à stor.J1 ou stor.J2
      const { A1, A2, cas } = joueur
      // on ne corrige pas si c’est le premier coup d’un des joueurs
      if (me.questionCourante < 5) {
        // on place le point A2 un cran au dessus du point de départ A1 puis on affiche 3 boules pour l’emplacement de A3
        placerPoint(A1.x, A1.y - stor.ecartQuadrillage)
        return this.finCorrection('enonce')
      }

      // ce n’est pas le premier tour d’un des joueurs

      if (cas == 3) {
        // le joueur est sorti du circuit mais n’a pas fini ses pénalités (passer quelques tours) (correction appelée grâce à coupApresCrash)
        return this.finCorrection('enonce', true)
      }

      if (cas == 4) {
        // le joueur est sorti du circuit et a fini ses pénalités (correction appelée grâce à coupApresCrash)
        j3pCreeSegment('segmentsAGarderJ' + stor.numJoueur, {
          id: 'segm',
          x1: A2.x,
          y1: A2.y,
          x2: A2.x,
          y2: A2.y,
          couleur: couleurJoueurCourant(),
          epaisseur: 1,
          opacite: 1,
          pointilles: '1,1'
        }) // segment limité à un point car sera agrandi avec j3pMorpheSegment en [A2A3] (cas 4)
        clignoterDirections(getEmplacements(A2, A2))
      } else {
        // le joueur n’est pas sorti du circuit au tour précédent
        corrigerSym()
      }

      this.finCorrection('enonce')
      break
    }

    case 'navigation':
      if (!stor.joueur.gagne) this.notif(Error('état navigation sans joueur gagnant'))
      // ça c’est pour que le modèle considère la section terminée et envoie le résultat pour enregistrement
      this.questionCourante = this.donneesSection.nbitems
      this.finNavigation()
      break // case "navigation":
  }
}
