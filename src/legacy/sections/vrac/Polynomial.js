// classe Polynomial
export function Polynomial () {
  this.init(arguments)
}

Polynomial.prototype.init = function (coefs) {
  this.coefs = []
  for (let i = coefs.length - 1; i >= 0; i--) this.coefs.push(coefs[i])
  this._variable = 't'
  this._s = 0
}
Polynomial.prototype.simplify = function () {
  for (let i = this.getDegree(); i >= 0; i--) {
    if (Math.abs(this.coefs[i]) <= Polynomial.TOLERANCE) this.coefs.pop()
    else break
  }
}
Polynomial.prototype.getDegree = function () {
  return this.coefs.length - 1
}
Polynomial.prototype.getRoots = function () {
  let result
  this.simplify()
  switch (this.getDegree()) {
    case 0:
      result = []
      break
    case 1:
      result = this.getLinearRoot() // appel à une fonction que j’ai virée car ce cas ne tombera jamais
      break
    case 2:
      result = this.getQuadraticRoots() // idem
      break
    case 3:
      result = this.getCubicRoots()
      break
    case 4:
      result = this.getQuarticRoots() // idem
      break
    default:
      result = []
  }
  return result
}
Polynomial.prototype.getCubicRoots = function () {
  const results = []
  if (this.getDegree() == 3) {
    const c3 = this.coefs[3]
    const c2 = this.coefs[2] / c3
    const c1 = this.coefs[1] / c3
    const c0 = this.coefs[0] / c3
    const a = (3 * c1 - c2 * c2) / 3
    const b = (2 * c2 * c2 * c2 - 9 * c1 * c2 + 27 * c0) / 27
    const offset = c2 / 3
    let discrim = b * b / 4 + a * a * a / 27
    const halfB = b / 2
    let tmp
    if (Math.abs(discrim) <= Polynomial.TOLERANCE) discrim = 0
    if (discrim > 0) {
      const e = Math.sqrt(discrim)
      tmp = -halfB + e
      let root = (tmp >= 0)
        ? Math.pow(tmp, 1 / 3)
        : -Math.pow(-tmp, 1 / 3)
      tmp = -halfB - e
      if (tmp >= 0) {
        root += Math.pow(tmp, 1 / 3)
      } else {
        root -= Math.pow(-tmp, 1 / 3)
      }
      results.push(root - offset)
    } else if (discrim < 0) {
      const distance = Math.sqrt(-a / 3)
      const angle = Math.atan2(Math.sqrt(-discrim), -halfB) / 3
      const cos = Math.cos(angle)
      const sin = Math.sin(angle)
      const sqrt3 = Math.sqrt(3)
      results.push(2 * distance * cos - offset)
      results.push(-distance * (cos + sqrt3 * sin) - offset)
      results.push(-distance * (cos - sqrt3 * sin) - offset)
    } else {
      if (halfB >= 0) tmp = -Math.pow(halfB, 1 / 3)
      else tmp = Math.pow(-halfB, 1 / 3)
      results.push(2 * tmp - offset)
      results.push(-tmp - offset)
    }
  }
  return results
}
// avec ces constantes
Polynomial.TOLERANCE = 0.000001
Polynomial.ACCURACY = 6
