// Classe Vector2D
export function Vector2D (x, y) {
  if (arguments.length > 0) {
    this.x = x
    this.y = y
  }
}

Vector2D.prototype.dot = function (that) {
  return this.x * that.x + this.y * that.y
}
