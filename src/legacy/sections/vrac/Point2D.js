// Classe Point2D
export function Point2D (x, y) {
  if (arguments.length > 0) {
    this.x = x
    this.y = y
  }
}

Point2D.prototype.add = function (that) {
  return new Point2D(this.x + that.x, this.y + that.y)
}
Point2D.prototype.multiply = function (scalar) {
  return new Point2D(this.x * scalar, this.y * scalar)
}
Point2D.prototype.lte = function (that) {
  return (this.x <= that.x && this.y <= that.y)
}
Point2D.prototype.gte = function (that) {
  return (this.x >= that.x && this.y >= that.y)
}
Point2D.prototype.lerp = function (that, t) {
  return new Point2D(this.x + (that.x - this.x) * t, this.y + (that.y - this.y) * t)
}
Point2D.prototype.min = function (that) {
  return new Point2D(Math.min(this.x, that.x), Math.min(this.y, that.y))
}
Point2D.prototype.max = function (that) {
  return new Point2D(Math.max(this.x, that.x), Math.max(this.y, that.y))
}
