import { j3pAjouteBouton, j3pAjouteZoneTexte, j3pGetRandomElts, j3pDefinirclasse, j3pDetruit, j3pDiv, j3pElement, j3pFocus, j3pGetRandomInt, j3pModale, j3pValeurde } from 'src/legacy/core/functions'
import CompteEstBon from 'src/legacy/outils/compteEstBon/CompteEstBon'
import textesGeneriques from 'src/lib/core/textes'
import { setStyle } from 'src/lib/utils/css'
import { safeEval } from 'src/lib/utils/object'

const { cBien, cFaux, regardeCorrection, reponseManquante, tempsDepasse } = textesGeneriques

/*
    JP Vanroyen
    juin-juillet 2013
*/

export const params = {
  outils: ['calculatrice'],
  /**
   * Ce qui est paramétrable dans l’éditeur de graphe
   * @type {LegacySectionParamsParametres}
   */
  parametres: [
    ['nbrepetitions', 2, 'entier', 'aucun'],
    ['indication', '', 'string', 'aucun'],
    ['limite', 0, 'entier', 'Temps disponible par répétition, en secondes'],
    ['nbchances', 2, 'entier', 'Nombre d’erreurs possibles (pour tenter de les corriger)'],

    ['nboperation', 4, 'entier', '2 || 3 || 4. Respectivement Addition&Soustraction || Addition&Soustraction&Multiplication || Addition&Soustraction&Multiplication&Division'],
    ['nombres', [1, 2, 3, 5, 7, 10, 25, 50, 100], 'array', 'Les étiquettes proposées sont choisies parmi les nombres de ce tableau'],
    ['compte', '[100;300]', 'string', 'Le compte à trouver appartient à cet intervalle'],
    ['nbeti', 5, 'entier', 'Le nombre d’étiquettes : entre 3 et 6'],
    ['soustitre', '', 'string', 'Le sous-titre du noeud']
  ]
}

// extraitTermes("(56*1)-(89*41+(1478/5)") ==>  [56,1,89,41,1478,5]
function extraitTermes (expr) {
  const estspec = (sch) => carspec.includes(sch)
  const estchiffre = (sch) => /^[0-9]$/.test(sch)

  const carspec = ['(', ')', '+', '-', '/', '*']
  let compteur = 0
  let debut = -1
  const tab = []

  while (compteur < expr.length) {
    const sch = expr.charAt(compteur)
    if (estchiffre(sch) && debut === -1) {
      debut = compteur
    }
    if (estspec(sch) && debut !== -1) {
      tab.push(expr.substring(debut, compteur))
      debut = -1
    }
    compteur++
    if ((compteur === expr.length) && (debut !== -1)) tab.push(expr.substring(debut, expr.length))
  }
  return tab
}

// (35-2)*(1+(3+6))-1 ==> 3+6
// parentheseinterieure('2*(3*(3+5))')
function parentheseinterieure (expr) {
  if (expr.indexOf('(') === -1) return ''
  let courant = 0
  let max = 0
  let indice = -1
  for (let k = 0; k < expr.length; k++) {
    const c = expr.charAt(k)
    if (c === '(') {
      courant++
      if (courant > max) {
        max = courant
        indice = k
      }
    }
    if (c === ')') courant--
  }

  return expr.substring(indice + 1, expr.indexOf(')', indice))
}

function calcul (a, b, ope) {
  switch (ope) {
    case 0: return a + b
    case 1: return a - b
    case 2: return a * b
    case 3: return a / b
    default: console.error(Error(`ope ${ope} non géré`))
  }
}

/**
 * section compteestbon
 * @this {Parcours}
 */
export default function main () {
  const me = this
  const stor = this.storage
  const ds = this.donneesSection

  function initialisation () {
    const ceb = stor.ceb

    switch (me.donneesSection.nboperation) {
      case 2:
        ceb.op = [0, 1]
        break
      case 3:
        ceb.op = [0, 1, 2]
        break
      case 4:
        ceb.op = [0, 1, 2, 3]
        break
    }
    ceb.barre = ceb.op
    ceb.liste = me.donneesSection.nombres
    ceb.nbeti = me.donneesSection.nbeti

    // ceb.nbeti = 5;
    let trouve = false
    let tab
    // pour éviter une boucle infinie
    let nbTries = 0
    while (!trouve && nbTries < 100) {
      let tab2
      do {
        ceb.compte = j3pGetRandomInt(me.donneesSection.compte)
        tab = j3pGetRandomElts(ceb.liste, ceb.nbeti)
        tab2 = []
        for (let k = 0; k < tab.length; k++) tab2.push(Number(tab[k]))
        // tab2 = [10,5,50,25];
      } while (tab2.includes(ceb.compte))
      // bug si le compte est l’une des étiquettes

      ceb.uncompte = new CompteEstBon(tab2, ceb.compte, ceb.op.length)
      trouve = ceb.uncompte.ceb
      if (trouve) {
        if (ceb.op.length <= 5) {
          const solutions = ceb.uncompte.affichesolutions(true)
          // on coupe les lignes
            .split('<br>')
          // et on vire la dernière ligne vide
            .filter(Boolean)
          ceb.solutions = solutions
          const min = Math.min(...(solutions.map(s => s.length)))
          // On estime arbitrairement qu’une expression < 12 chars est trop facile
          if (min < 12) {
            trouve = false
          }
        }
      }
      nbTries++
    }

    for (let k = 0; k < tab.length; k++) {
      const div = j3pDiv('conteneurdroit', {
        coord: [100 * k + ceb.mef.margecdroit, ceb.mef.margecdroit],
        contenu: ''
      })
      ajouteBouton(div, { id: 'bouton0' + k, className: 'MepBoutons2', value: tab[k] }, true)
    }

    for (let k = 0; k < ceb.barre.length; k++) {
      j3pDiv('conteneurgauche', { id: 'conteneurboutonG' + k, coord: [8, 8 + 60 * k], contenu: '' })
      ajouteBouton('conteneurboutonG' + k, { id: 'boutonG' + k, className: ceb.styles[k] })
    }

    const numero = ceb.barre.length
    const div = j3pDiv('conteneurgauche', { coord: [8, 23 + 60 * numero], contenu: '' })
    ajouteBouton(div, { id: 'boutonGavant', className: 'MepBoutonsArriere' })
    j3pElement('conteneursup').innerHTML = 'Il faut obtenir : <b>' + ceb.compte + '</b>'

    ceb.plateau[0] = { ligne: tab, operation: { a: -1, b: -1, op: -1 }, classe: [] }
    for (let k = 0; k < ceb.barre.length; k++) { ceb.plateau[0].classe[k] = 'MepBoutons2' }

    ceb.ligne = 0
  }

  function reconstruction (numeroligne) {
    const ceb = stor.ceb
    j3pDetruit('conteneurdroit')
    j3pDiv(me.zones.MG, {
      id: 'conteneurdroit',
      contenu: '',
      coord: [100, 70],
      style: me.styles.etendre('moyen.explications', {
        border: '1px solid #000',
        width: '600px',
        height: '360px',
        background: '#f0f0f0',
        padding: '5px'
      })
    }
    )

    for (let u = 0; u <= numeroligne; u++) {
      for (let k = 0; k < (ceb.nbeti - u); k++) {
        j3pDiv('conteneurdroit', {
          id: 'conteneurbouton' + u + '' + k,
          coord: [100 * k + ceb.mef.margecdroit, 60 * u + ceb.mef.margecdroit],
          contenu: ''
        })
        if (u === numeroligne) ceb.plateau[u].classe[k] = 'MepBoutons2'
        ajouteBouton('conteneurbouton' + u + '' + k, { id: 'bouton' + u + '' + k, className: ceb.plateau[u].classe[k], value: ceb.plateau[u].ligne[k] }, true)
      }
      if ((u === numeroligne)) {
        ceb.plateau[u].operation = { a: -1, b: -1, op: -1 }
      }
    }
    ceb.etat = 0
    ceb.ligne = numeroligne
    ceb.plateau.pop()
    // console.log('ceb.plateau', ceb.plateau)
  }

  function recule () {
    if (stor.ceb.ligne >= 1) reconstruction(stor.ceb.ligne - 1)
  }

  function corrigeexpression () {
    const ceb = stor.ceb

    if (j3pElement('conteneurbas').style.display === 'none') {
      return { bool: false, mes: 'pas trouve' }
    }

    const reponse = j3pValeurde('reponse1')

    if (!reponse) {
      return { bool: false, mes: 'Il faut saisir une expression' }
    }

    const termesInitiaux = ceb.plateau[0].ligne
    const pioche = [...termesInitiaux] // faut cloner pour pas modifier le plateau
    const termes = extraitTermes(reponse).map(Number)
    let correct = true
    let message = ''
    for (const terme of termes) {
      const pos = pioche.indexOf(terme)
      if (pos === -1) {
        correct = false
        if (ceb.plateau[0].ligne.includes(terme)) {
          message = terme + ' est utilisé plus d’une fois'
        } else {
          message = terme + ' ne figure pas dans les nombres initiaux'
        }
      } else {
        // ok, on vire le terme de la pioche
        pioche.splice(pos, 1)
      }
    }
    if (correct) {
      const evaluation = safeEval(reponse)
      if (evaluation !== ceb.compte) {
        correct = false
        message = 'L’expression proposée ne donne pas comme résultat : ' + ceb.compte
      }
    }

    return { bool: correct, mes: message }
  }

  function nouvelleligne () {
    const ceb = stor.ceb
    for (let k = 0; k < ceb.plateau[ceb.ligne].ligne.length; k++) {
      j3pElement('bouton' + (ceb.ligne) + '' + k).removeEventListener('click', onClick)
    }

    const a = ceb.plateau[ceb.ligne].ligne[Number(ceb.plateau[ceb.ligne].operation.a)]
    const b = ceb.plateau[ceb.ligne].ligne[Number(ceb.plateau[ceb.ligne].operation.b)]

    const resultat = calcul(a, b, ceb.plateau[ceb.ligne].operation.op)
    // resultat remplace les etiquettes ceb.plateau[ceb.ligne].operation.a et ceb.plateau[ceb.ligne].operation.b

    ceb.ligne++
    const k = 0
    j3pDiv('conteneurdroit', {
      id: 'conteneurbouton' + ceb.ligne + '' + k,
      coord: [ceb.mef.margecdroit, 60 * ceb.ligne + ceb.mef.margecdroit],
      contenu: ''
    })
    ajouteBouton('conteneurbouton' + ceb.ligne + '' + k, { id: 'bouton' + ceb.ligne + '' + k, className: 'MepBoutons2', value: resultat }, true)

    ceb.plateau.push({ ligne: [], operation: { a: -1, b: -1, op: -1 }, classe: [] })

    ceb.plateau[ceb.ligne].classe[0] = 'MepBoutons2'

    ceb.plateau[ceb.ligne].ligne.push(resultat)
    let compteur = 1
    const eltPlateau = ceb.plateau[ceb.ligne - 1]
    for (let k = 0; k < eltPlateau.ligne.length; k++) {
      if ((k !== Number(eltPlateau.operation.a)) && (k !== Number(eltPlateau.operation.b))) {
        j3pDiv('conteneurdroit', {
          id: 'conteneurbouton' + ceb.ligne + '' + compteur,
          coord: [100 * compteur + ceb.mef.margecdroit, 60 * ceb.ligne + ceb.mef.margecdroit],
          contenu: ''
        })

        ajouteBouton('conteneurbouton' + ceb.ligne + '' + compteur, { id: 'bouton' + ceb.ligne + '' + compteur, className: 'MepBoutons2', value: eltPlateau.ligne[k] }, true)
        ceb.plateau[ceb.ligne].classe[compteur] = 'MepBoutons2'
        compteur++
        ceb.plateau[ceb.ligne].ligne.push(eltPlateau.ligne[k])
      }
    }

    if (ceb.plateau[ceb.ligne].ligne[0] === ceb.compte) {
      j3pElement('conteneursupdroit').innerHTML = 'Le compte est bon !'
      ceb.trouve = true
      j3pElement('conteneurbas').style.display = ''
      j3pElement('bouton' + ceb.ligne + '0').style.color = '#FFF'
      j3pElement('bouton' + ceb.ligne + '0').style.background = '#800'
      j3pElement('conteneursupdroit').style.color = '#FFF'
      j3pElement('conteneursupdroit').style.background = '#800'

      j3pElement('conteneurbasgauche').innerHTML = ceb.compte + ' = '
      j3pElement('reponse1').style.display = ''
      j3pFocus('reponse1')

      j3pElement('conteneurbas').style.background = '#a00'
      j3pElement('conteneurbasgauche').style.color = '#FFF'
    }
  }

  function onClick (event) {
    const ceb = stor.ceb
    if (ceb.trouve) return

    const id = event.target.id
    if (id.substring(0, 7) === 'bouton' + ceb.ligne) ceb.action = 0// clic sur etiquette
    if (id.substring(0, 7) === 'boutonG') ceb.action = 10 + Number(id.substring(7))
    if (id === 'boutonGesc') ceb.action = 4
    if (id === 'boutonGapres') ceb.action = 2
    if (id === 'boutonGavant') ceb.action = 3

    if ((ceb.plateau[ceb.plateau.length - 1].ligne.length === 1) && (ceb.action !== 3)) return

    switch (ceb.etat) {
      case 0:
        switch (ceb.action) {
          case 0:
            ceb.etat = 1
            ceb.etiquettecourante1 = id.substring(7)
            ceb.plateau[ceb.ligne].operation = { a: ceb.etiquettecourante1, b: -1, op: -1 }
            for (let k = 0; k < ceb.plateau[ceb.ligne].ligne.length; k++) {
              ceb.plateau[ceb.ligne].classe[ceb.etiquettecourante1] = 'MepBoutons2'
              j3pDefinirclasse('bouton' + ceb.ligne + '' + k, 'MepBoutons2')
            }
            ceb.plateau[ceb.ligne].classe[ceb.etiquettecourante1] = 'MepBoutons2select'
            j3pDefinirclasse('bouton' + ceb.ligne + '' + ceb.etiquettecourante1, 'MepBoutons2select')
            break
          case 2:
            ceb.etat = 0
            break
          case 3:
            ceb.etat = 0
            recule()
            break
          case 4:
            ceb.plateau[ceb.ligne].operation = { a: -1, b: -1, op: -1 }
            break
          default:
            // 10,11,12,13
            ceb.etat = 0
        }
        break
      case 1:// on vient de cliquer sur une première étiquette
        switch (ceb.action) {
          case 0:
            ceb.etat = 1
            ceb.etiquettecourante1 = id.substring(7)
            ceb.plateau[ceb.ligne].operation = { a: ceb.etiquettecourante1, b: -1, op: -1 }
            for (let k = 0; k < ceb.plateau[ceb.ligne].ligne.length; k++) {
              ceb.plateau[ceb.ligne].classe[ceb.etiquettecourante1] = 'MepBoutons2'
              j3pDefinirclasse('bouton' + ceb.ligne + '' + k, 'MepBoutons2')
            }
            ceb.plateau[ceb.ligne].classe[ceb.etiquettecourante1] = 'MepBoutons2select'
            j3pDefinirclasse('bouton' + ceb.ligne + '' + ceb.etiquettecourante1, 'MepBoutons2select')
            break
          case 2:
            ceb.etat = 0
            break
          case 3:
            ceb.etat = 0
            recule()
            break
          case 4:
            ceb.etat = 0
            ceb.etiquettecourante1 = -1
            ceb.plateau[ceb.ligne].operation = { a: -1, b: -1, op: -1 }
            break
          default:
            // 10,11,12,13
            ceb.etat = 2
            ceb.operationcourante = ceb.action - 10
            ceb.plateau[ceb.ligne].operation = { a: ceb.etiquettecourante1, b: -1, op: ceb.operationcourante }
            for (let k = 0; k < ceb.op.length; k++) {
              j3pDefinirclasse('boutonG' + k, ceb.styles[k])
            }
            j3pDefinirclasse('boutonG' + ceb.operationcourante, ceb.styles[ceb.operationcourante] + 'Select')
        }
        break
      case 2:// on vient de cliquer sur l’operation

        switch (ceb.action) {
          case 0:
            ceb.etat = 0
            ceb.etiquettecourante2 = id.substring(7)
            ceb.plateau[ceb.ligne].operation = {
              a: ceb.etiquettecourante1,
              b: ceb.etiquettecourante2,
              op: ceb.operationcourante
            }

            if (ceb.operationcourante === 3) {
              const resultat = calcul(ceb.plateau[ceb.ligne].ligne[ceb.etiquettecourante1], ceb.plateau[ceb.ligne].ligne[ceb.etiquettecourante2], ceb.operationcourante)
              // console.log(ceb.plateau[ceb.ligne].ligne[ceb.etiquettecourante1], ceb.plateau[ceb.ligne].ligne[ceb.etiquettecourante2], ceb.operationcourante)
              if (Math.abs(Math.round(resultat) - resultat) > 0.00000001) {
                ceb.etat = 2
                return
              }
            }

            for (let k = 0; k < ceb.barre.length; k++) { j3pDefinirclasse('boutonG' + k, ceb.styles[k]) }
            ceb.plateau[ceb.ligne].classe[ceb.etiquettecourante2] = 'MepBoutons2select'
            j3pDefinirclasse('bouton' + ceb.ligne + '' + ceb.etiquettecourante2, 'MepBoutons2select')
            nouvelleligne()
            break
          case 2:
            ceb.etat = 0
            break
          case 3:
            ceb.etat = 0
            recule()
            break
          case 4:
            ceb.etat = 2
            ceb.operationcourante = -1
            ceb.plateau[ceb.ligne].operation = { a: ceb.etiquettecourante1, b: -1, op: -1 }

            break
          default:
            // 10,11,12,13
            ceb.etat = 2
            ceb.operationcourante = ceb.action - 10
            for (let k = 0; k < ceb.op.length; k++) {
              j3pDefinirclasse('boutonG' + k, ceb.styles[k])
            }
            j3pDefinirclasse('boutonG' + ceb.operationcourante, ceb.styles[ceb.operationcourante] + 'Select')
        }
        break
    }
    stor.ceb = ceb
  }

  function ajouteBouton (container, btnProps, big) {
    const btn = j3pAjouteBouton(container, onClick, btnProps)
    if (big) {
      setStyle(btn, {
        width: '80px',
        fontSize: '24px',
        fontFamily: 'Arial',
        fontWeight: 'normal'
      })
    } else {
      setStyle(btn, {
        width: '60px',
        fontSize: '24px',
        fontFamily: 'Arial',
        fontWeight: 'normal',
        padding: '5px'
      })
    }
    return btn
  }

  switch (this.etat) {
    case 'enonce':
      // code exécuté au lancement de la section
      if (this.debutDeLaSection) {
        this.validOnEnter = false // ex donneesSection.touche_entree
        // Construction de la page
        this.construitStructurePage('presentation3')
        /*
         Par convention,`
        `   this.typederreurs[0] = nombre de bonnes réponses
            this.typederreurs[1] = nombre de fois où l’élève a utilisé une chance supplémentaire
            this.typederreurs[2] = nombre de mauvaises réponses
            this.typederreurs[10] = nombre de fois où l’élève a eu faux à cause d’un temps limite
            LES AUTRES peuvent être utilisées par la détermination de la pe dans le cas d’une section qualitative
         */
        this.typederreurs = [0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0]
        const letitre = (ds.soustitre === '') ? 'Le compte est bon' : 'Le compte est bon : ' + ds.soustitre
        this.afficheTitre(letitre)
        if (ds.indication) this.indication(this.zones.IG, ds.indication)
      } else {
        // A chaque répétition, on nettoie la scène
        this.videLesZones()
      }

      stor.scorecourant = 1

      stor.ceb = {
        compte: 0,
        trouve: false,
        op: [0, 1, 2, 3], // +,-,*,/
        nbeti: 5, // le nombre d’étiquetets
        liste: [1, 3, 5, 7, 10, 13, 25, 50, 100], // les étiquettes disponibles
        barre: [0, 1, 2, 3], // les opérations disponibles
        ligne: 1, // la ligne courante
        etat: 0, // 0 : rien, 1 : apres clic sur première etiquette, 2 apres clic sur operation, 3 : apres clic sur seconde etiquette
        action: 0, // 0 : clic sur etiquette, 1 : clic sur operation, 2 : avance, 3 : recule , 4 : annule
        etiquettecourante1: -1,
        etiquettecourante2: -1,
        operationcourante: -1,
        plateau: [
          { ligne: [], operation: { a: -1, b: -1, op: -1 }, classe: [] }
        ],
        solutions: [],
        historique: [],
        mef: { margecdroit: 10 },
        styles: ['MepBoutonsAddition', 'MepBoutonsSoustraction', 'MepBoutonsMultiplication', 'MepBoutonsDivision']
      }

      // LE CODE
      j3pDiv(this.zones.MG, {
        id: 'conteneursup',
        contenu: '',
        coord: [100, 10],
        style: this.styles.etendre('moyen.explications', {
          color: '#000',
          border: '1px solid #000',
          width: '300px',
          height: '50px',
          background: '#f0f0f0',
          padding: '5px',
          paddingLeft: '15px'
        })
      }
      )
      j3pDiv(this.zones.MG, {
        id: 'conteneursupdroit',
        contenu: '',
        coord: [400, 10],
        style: this.styles.etendre('moyen.explications', {
          border: '1px solid #000',
          width: '300px',
          height: '50px',
          background: '#f0f0f0',
          padding: '5px'
        })
      }
      )
      j3pDiv(this.zones.MG, {
        id: 'conteneurdroit',
        contenu: '',
        coord: [100, 70],
        style: this.styles.etendre('moyen.explications', {
          border: '1px solid #000',
          width: '600px',
          height: '360px',
          background: '#f0f0f0',
          padding: '5px'
        })
      }
      )
      j3pDiv(this.zones.MG, {
        id: 'conteneurgauche',
        contenu: '',
        coord: [10, 110],
        style: this.styles.etendre('moyen.explications', {
          border: '1px solid #000',
          width: '80px',
          height: '320px',
          background: '#f0f0f0',
          padding: '5px'
        })
      }
      )

      j3pDiv(this.zones.MG, {
        id: 'conteneurbas',
        contenu: '',
        coord: [100, 440],
        style: this.styles.etendre('moyen.explications', {
          border: '1px solid #000',
          width: '500px',
          height: '50px',
          background: '#f0f0ff',
          padding: '5px'
        })
      }
      )
      j3pDiv('conteneurbas', {
        id: 'conteneurbasgauche',
        contenu: '',
        coord: [0, 2],
        style: this.styles.etendre('moyen.explications', { width: '100px', height: '50px', padding: '5px' })
      }
      )
      j3pDiv('conteneurbas', {
        id: 'conteneurbasdroit',
        contenu: '',
        coord: [100, 0],
        style: this.styles.etendre('moyen.explications', { width: '450px', height: '50px', padding: '5px' })
      }
      )
      j3pDiv(this.zones.MG, {
        id: 'explications',
        contenu: '',
        coord: [710, 100],
        style: this.styles.etendre('moyen.explications', { width: '150px', height: '50px', padding: '5px' })
      }
      )
      j3pDiv(this.zones.MG, {
        id: 'solutions',
        contenu: '',
        coord: [210, 380],
        style: this.styles.etendre('moyen.explications', { width: '150px', height: '50px', padding: '5px' })
      }
      )

      j3pAjouteZoneTexte('conteneurbasdroit', {
        id: 'reponse1',
        maxchars: '50',
        restrict: /[0-9(),*/+-]/,
        texte: '',
        tailletexte: 24,
        width: 380
      })
      j3pElement('reponse1').style.padding = '0px'
      j3pElement('reponse1').style.display = 'none'
      j3pElement('reponse1').style.color = '#000'
      j3pElement('conteneurbas').style.display = 'none'

      initialisation()
      this.finEnonce()
      break

    case 'correction': {
      const ob = corrigeexpression()
      if (ob.mes === 'pas trouve') {
        const tab = stor.ceb.solutions
        if (me.essaiCourant < ds.nbchances) {
          j3pModale({
            titre: 'Voici le début du calcul',
            contenu: parentheseinterieure(tab[0])
          })
          stor.scorecourant -= 0.2
          if (stor.scorecourant < 0) stor.scorecourant = 0
          return // on reste en correction pour l’essai suivant
        }
        // dernier essai
        j3pModale({ titre: 'Voici une solution possible', contenu: tab[0] })
        stor.scorecourant -= 0.6
        if (stor.scorecourant < 0) stor.scorecourant = 0
        return this.finCorrection('navigation', true)
      }

      // On teste si une réponse a été saisie
      // FIXME clic sur OK sans rien faire affiche le début de la solution
      const repEleve = j3pValeurde('reponse1')
      if (!repEleve && !this.isElapsed) {
        this.reponseManquante() // on lui passe rien parce qu’on veut l’afficher dans une modale (pourquoi… ?)
        j3pModale({ titre: 'ERREUR', contenu: '<br>' + reponseManquante })
        j3pFocus('reponse1')
        this.typederreurs[3]++
      } else {
        // Une réponse a été saisie
        if (ob.bool) {
          // Bonne réponse
          this._stopTimer()
          this.score += stor.scorecourant
          j3pElement('explications').style.color = this.styles.cbien
          j3pElement('explications').innerHTML = cBien

          this.typederreurs[0]++
          this.etat = 'navigation'
          this.sectionCourante()
        } else {
          // Pas de bonne réponse
          j3pElement('explications').style.color = this.styles.cfaux

          // A cause de la limite de temps :
          if (this.isElapsed) { // limite de temps
            j3pElement('explications').innerHTML = tempsDepasse
            this.typederreurs[10]++
            j3pElement('explications').innerHTML += '<br>La solution était ' + this.stockage[0]
            /*
             *   RECOPIER LA CORRECTION ICI !
             */
            this.etat = 'navigation'
            this.sectionCourante()
          } else {
            // Réponse fausse :
            j3pElement('explications').innerHTML = cFaux

            // S’il y a plus de deux chances,on appelle à nouveau le bouton Valider
            if (this.essaiCourant < ds.nbchances) {
              if (!ob.bool && (ob.mes.indexOf('réponse') === -1)) {
                j3pModale({
                  titre: 'ERREUR',
                  contenu: ob.mes
                })
              }
              this.typederreurs[1]++
              stor.scorecourant -= 0.2
              if (stor.scorecourant < 0) stor.scorecourant = 0
            } else {
              // Erreur au dernier essai
              j3pElement('explications').innerHTML += '<br>' + regardeCorrection
              // j3pElement("explications").innerHTML+="<br>La solution était "+this.stockage[0]
              // stor.ceb.solutions
              j3pElement('solutions').innerHTML = stor.ceb.solutions[0].substring(stor.ceb.solutions[0].indexOf('=') + 1)
              this.typederreurs[2]++
              this.etat = 'navigation'
              this.sectionCourante()
              stor.scorecourant -= 0.2
              if (stor.scorecourant < 0) stor.scorecourant = 0
              this.score += stor.scorecourant
            }
          }
        }
      }
      // Obligatoire
      this.finCorrection()
      break // case "correction":
    }

    case 'navigation':
      if (!this.sectionTerminee()) {
        this.etat = 'enonce'
      }
      this.finNavigation()
      break
  }
}
