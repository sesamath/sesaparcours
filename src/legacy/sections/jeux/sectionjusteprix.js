import { j3pAddElt, j3pAjouteBouton, j3pAjouteZoneTexte, j3pDiv, j3pElement, j3pEmpty, j3pFocus, j3pGetRandomInt, j3pNombre, j3pVirgule } from 'src/legacy/core/functions'
import { j3pAffiche, j3pDesactive } from 'src/lib/mathquill/functions'
import { j3pBaseUrl } from 'src/lib/core/constantes'
import textesGeneriques from 'src/lib/core/textes'

const { tempsDepasse } = textesGeneriques

// les sons
const getWavUrl = (id) => `${j3pBaseUrl}static/justeprix/${id}.wav`
const audios = {
  plus: getWavUrl('plus'),
  moins: getWavUrl('moins'),
  bravo: getWavUrl('bravo'),
  perdu: getWavUrl('perdu')
}

const textes = {
  enonce1: 'J’ai choisi un nombre compris entre £a et £b, tu dois le deviner.\nÀ chaque essai, je te dirai si c’est plus, moins ou si tu as trouvé !',
  enonce1AvecDecimales: 'J’ai choisi un nombre compris entre £a et £b avec £{nbDecimales} décimales, tu dois le deviner.\nÀ chaque essai, je te dirai si c’est plus, moins ou si tu as trouvé !',
  enonce2: '\n\nClique sur le bouton Démarrer lorsque tu es prêt.',
  enonce3: 'Propose un nombre : ',
  reponse1: 'Proposition précédente :\n',
  reponse1bis: 'Propositions précédentes :\n',
  reponseplus: '£a : le nombre cherché est plus grand',
  reponsemoins: '£a : le nombre cherché est plus petit',
  correctionPerdu: 'Perdu ! Le nombre à trouver était £a',
  correctionDetail: 'Il te reste encore £a chance(s).',
  correctionBien: 'Bravo ! Tu as trouvé en £a coups !',
  correctionBien2: 'Bravo ! Tu as trouvé du premier coup !',
  correctionPlus: 'Le nombre cherché est plus grand que £a !\n',
  correctionMoins: 'Le nombre cherché est plus petit que £a !\n',
  correctionOut: '£a n’est pas compris entre £b et £c !\n'
}

function play (id, restart) {
  if (window.HTMLAudioElement) {
    try {
      /** @type {HTMLAudioElement} */
      const elt = j3pElement(id)
      if (restart) elt.currentTime = 0
      elt.play().catch(e => console.error(e))
    } catch (e) {
      console.error(e)
    }
  }
}

export const params = {
  /**
   * Ce qui est paramétrable dans l’éditeur de graphe
   * @type {LegacySectionParamsParametres}
   */
  parametres: [
    ['nbrepetitions', 1, 'entier', 'Nombre de répétitions de la section'],
    ['indication', '', 'string', 'Indication pour chaque répétition'],
    ['limite', 0, 'entier', 'Temps disponible par répétition, en secondes'],
    ['nbchances', 12, 'entier', 'Nombre d’essais par répétition'],
    ['son', 'avec', 'liste', 'Avec ou sans son', ['avec', 'sans']],
    ['NombreMin', 0, 'entier', 'Minimum du nombre recherché'],
    ['NombreMax', 1000, 'entier', 'Maximum du nombre recherché'],
    ['NombreDecimales', 0, 'number', 'Nombre de décimales (laisser 0 pour n’avoir que des entiers'],
    ['afficheHistorique', true, 'boolean', 'Pour savoir si l’on affiche les précédentes réponses de l’élève'],
    ['aide_affiche_nombre', false, 'boolean', 'On peut aider en affichant les nombres possibles et en grisant ceux qui ne sont plus possibles']
  ]
}

function playAudio (conteneur, fichier, idson) {
  if (window.HTMLAudioElement) {
    try {
      if (!audios[fichier]) throw Error(`Le son ${fichier} n’est pas géré`)
      const audio = document.createElement('audio')
      audio.setAttribute('id', idson)
      const urlfichier = audios[fichier]
      audio.setAttribute('src', urlfichier)
      j3pElement(conteneur).appendChild(audio)
    } catch (e) {
      console.error(e)
    }
  } else {
    console.error('Ce navigateur ne gère pas les HTMLAudioElement, la gestion du son est ignorée')
  }
}

/**
 * section justeprix
 * @this {Parcours}
 */
export default function main () {
  const me = this
  const ds = this.donneesSection
  const stor = this.storage

  function changeCouleurNombres () {
    for (let k = ds.NombreMin; k <= ds.NombreMax; k++) {
      if (!me.stockage[2][k]) {
        // je change la couleur en gris
        j3pElement('nombres' + k).style.color = 'rgb(120,120,120)'
      }
    }
  }

  function randomdecBis (borneinf, bornesup, nbDecimales) {
    const puiss = Math.pow(10, nbDecimales)
    let nb
    do {
      nb = j3pGetRandomInt(borneinf * puiss, bornesup * puiss)
      // je ne veux pas de multiple de 10 pour avoir exactement le nb de décimales demandé
    } while (nb % 10 === 0)
    return nb / puiss
  }

  function effaceCorrection () {
    j3pElement('explications').innerHTML = ''
    me.afficheBoutonValider()
  }
  function afficheQuestion () {
    // j’efface le texte initial et le bouton
    j3pElement('zoneConsigne').innerHTML = ''
    j3pElement('zoneBouton').innerHTML = ''
    // la consigne :
    j3pAffiche('Enonce', 'affiche3', textes.enonce3 + '\n\n', { style: { fontSize: '24px' } })
    // la zone de saisie :
    j3pAjouteZoneTexte('Enonce', { id: 'reponse1', maxchars: '7', restrict: /[0-9,-]/, texte: '', tailletexte: 24, fond: '#FFC2D1', taille: 5 })
    j3pFocus('reponse1')
    // j’affiche le bouton OK
    me.afficheBoutonValider()
    // la zone explications :
    j3pAddElt('Enonce', 'div', '', { id: 'explications', style: { position: 'relative', top: '300px', fontSize: '18px' } })
    // si aide_affiche_nombre je les mets tous, sauf si y’en a trop
    if (ds.aide_affiche_nombre && (ds.NombreDecimales || (ds.NombreMax - ds.NombreMin) > 20)) {
      if (ds.NombreDecimales) console.error(Error('Pas d’affichage des nombres possibles avec les décimaux'))
      else console.error(Error('Pas d’affichage des nombres possibles s’ils sont plus de 20'))
      ds.aide_affiche_nombre = false
    }
    if (ds.aide_affiche_nombre) {
      for (let k = ds.NombreMin; k <= ds.NombreMax; k++) {
        j3pElement('nombres' + k).innerText = String(k)
      }
    }
  }

  switch (this.etat) {
    case 'enonce': {
      if (this.debutDeLaSection) {
        if (ds.nbchances === 'illimite') ds.nbchances = 0
        this.construitStructurePage('presentation2')
        this.stockage = [0, 0, 0, 0]
        stor.solution = 0
        this.typederreurs = [0, 0, 0, 0, 0, 0]

        this.afficheTitre('Le nombre inconnu')
        if (ds.indication) this.indication(this.zones.IG, ds.indication)

        // Pour cet exercice, on charge le son :
        if (ds.son === 'avec') {
          playAudio(this.zones.HG, 'plus', 'son1')
          playAudio(this.zones.HG, 'moins', 'son2')
          playAudio(this.zones.HG, 'bravo', 'son3')
          playAudio(this.zones.HG, 'perdu', 'son4')
        }
      } else {
        j3pEmpty(this.zonesElts.MG)
      }
      stor.histoAffiche = []
      const o = this.styles.petit.enonce
      const p = this.styles.toutpetit.enonce
      const enonce = j3pDiv(this.zones.MG, 'Enonce', '', [20, 50], o)
      j3pAddElt(enonce, 'div', '', { id: 'zoneConsigne' })
      let laconsigne
      const params = {
        a: ds.NombreMin,
        b: ds.NombreMax,
        style: { fontSize: '24px' }
      }
      if (ds.NombreDecimales) {
        laconsigne = textes.enonce1AvecDecimales
        params.nbDecimales = ds.NombreDecimales
        // la réponse, en tenant compte du nb de decimales
        stor.solution = randomdecBis(ds.NombreMin, ds.NombreMax, ds.NombreDecimales)
      } else {
        laconsigne = textes.enonce1
        // la réponse :
        stor.solution = j3pGetRandomInt(ds.NombreMin, ds.NombreMax)
      }
      j3pAffiche('zoneConsigne', 'affiche1', laconsigne, params)
      j3pAffiche('zoneConsigne', 'affiche2', textes.enonce2 + '\n\n', { style: { fontSize: '24px' } })
      const zoneBouton = j3pAddElt('Enonce', 'div', '', { id: 'zoneBouton' })
      j3pAjouteBouton(zoneBouton, afficheQuestion, { id: 'btn_affiche_question', className: 'MepBoutons', value: 'Démarrer' })
      if (ds.afficheHistorique) {
        stor.divHisto = j3pDiv(this.zones.MG, { coord: [450, 50], style: p })
      }
      if (ds.aide_affiche_nombre) {
        // un tableau de booléens que j’initialise à true pour savoir si les chiffres sont grisés ou pas
        me.stockage[2] = []
        for (let k = ds.NombreMin; k <= ds.NombreMax; k++) {
          me.stockage[2][k] = true
          // je créé les span
          j3pAddElt(this.zones.IG, 'span', k, { id: 'nombres' + k, style: { padding: '2px 3px', fontSize: '22px', color: 'black' } })
        }
      }
      this.finEnonce()
      // cas particulier de cette section, on masque le bouton OK pendant la consigne de départ
      this.cacheBoutonValider()
      break // case "enonce":
    }

    case 'correction': {
      const inputReponse = j3pElement('reponse1')
      const reponse = inputReponse.value.trim()
      j3pElement('explications').innerHTML = ''

      // on teste si une réponse a été saisie
      if (!reponse && !this.isElapsed) {
        this.reponseManquante('explications')
        return this.finCorrection()
      }

      // une réponse a été saisie
      const rep = j3pNombre(reponse)
      const cbon = (rep === stor.solution)
      if (cbon) {
        this.score++
        j3pDesactive(inputReponse)
        j3pElement('explications').style.color = this.styles.cbien
        // Bravo, tu as trouvé en X coups
        const msg = (this.essaiCourant === 1) ? textes.correctionBien2 : textes.correctionBien
        j3pAffiche('explications', 'affiche4', msg, {
          a: ds.numeroessai,
          style: { fontSize: 24, couleur: '#0A0' }
        })
        if (ds.son === 'avec') {
          play('son3', false)
        }
        this.typederreurs[0]++
        return this.finCorrection('navigation', true)
      }

      // Pas de Bonne réponse
      j3pElement('explications').style.color = this.styles.cfaux
      if (this.isElapsed) {
        // à cause de la limite de temps :
        j3pElement('explications').innerHTML = tempsDepasse
        this.typederreurs[10]++
        return this.finCorrection('navigation', true)
      }

      // réponse fausse :
      // S’il y a plusieurs chances,on appelle à nouveau le bouton Valider
      if (this.essaiCourant >= ds.nbchances) {
        // c’est perdu !
        j3pAffiche('explications', 'affiche4', textes.correctionPerdu, { a: j3pVirgule(stor.solution) })
        if (ds.son === 'avec') {
          play('son4', false)
        }
        this.typederreurs[2]++
        return this.finCorrection('navigation', true)
      }

      let plusMoins
      if (rep < stor.solution) {
        if (rep < ds.NombreMin) {
          // cas particulier si l’on ne répond pas dans l’intervalle'
          j3pAffiche('explications', 'affiche8', textes.correctionOut, {
            a: (rep),
            b: ds.NombreMin,
            c: ds.NombreMax
          })
        } else {
          j3pAffiche('explications', 'affiche8', textes.correctionPlus, { a: (rep) })
        }
        if (ds.son === 'avec') {
          play('son1', false)
        }
        plusMoins = 'plus'
        // on va masquer les nb plus bons...
        if (ds.aide_affiche_nombre) {
          for (let k = ds.NombreMin; k <= rep; k++) {
            me.stockage[2][k] = false
          }
          // j’appelle la fonction qui me change les styles de mes nombres
          changeCouleurNombres()
        }
      } else {
        if (rep > ds.NombreMax) {
          // cas particulier si l’on ne répond pas dans l’intervalle'
          j3pAffiche('explications', 'affiche8', textes.correctionOut, {
            a: (rep),
            b: ds.NombreMin,
            c: ds.NombreMax
          })
        } else {
          j3pAffiche('explications', 'affiche8', textes.correctionMoins, { a: (rep) })
        }
        if (ds.son === 'avec') {
          play('son2', false)
        }
        plusMoins = 'moins'
        // on va masquer les nb plus bons...
        if (ds.aide_affiche_nombre) {
          for (let k = rep; k <= ds.NombreMax; k++) {
            me.stockage[2][k] = false
          }
          // j’appelle la fonction qui me change les styles de mes nombres
          changeCouleurNombres()
        }
      }
      j3pAffiche('explications', 'affiche4', textes.correctionDetail, { a: (ds.nbchances - this.essaiCourant) })
      this.typederreurs[1]++
      // indication éventuelle ici

      // on ajoute un écouteur à la zone pour supprimer le texte lors de l’appui sur une touche
      inputReponse.addEventListener('keypress', effaceCorrection.bind(null, rep, plusMoins))

      if (ds.afficheHistorique) {
        // pour éviter qu’on affiche à chaque fois qu’on clique sur la zone...
        if (!stor.histoAffiche[this.essaiCourant]) {
          if (this.essaiCourant === 1) {
            // titre Proposition précédente
            j3pAffiche(stor.divHisto, '', textes.reponse1, { style: { fontSize: 18 } })
          } else if (this.essaiCourant === 2) {
            // titre au pluriel
            j3pEmpty(stor.divHisto)
            j3pAffiche(stor.divHisto, '', textes.reponse1bis, { style: { fontSize: 18 } })
            // faut remettre la 1re rep précédente
            j3pAffiche(stor.divHisto, '', stor.firstHisto, { style: { fontSize: 18 } })
          }
          // la réponse qui vient d'être faite
          const repVirg = j3pVirgule(rep)
          let lastAjout
          if (plusMoins === 'plus') {
            lastAjout = j3pAffiche(stor.divHisto, '', textes.reponseplus + '\n', {
              a: repVirg,
              style: { fontSize: 18 }
            })
          } else {
            lastAjout = j3pAffiche(stor.divHisto, 'affiche7' + this.essaiCourant, textes.reponsemoins + '\n', {
              a: repVirg,
              style: { fontSize: 18 }
            })
          }
          if (this.essaiCourant === 1) {
            stor.firstHisto = lastAjout.parent.innerHTML
          }
        }
        stor.histoAffiche[this.essaiCourant] = true
      }
      j3pFocus(inputReponse)
      inputReponse.value = ''
      // on reste dans l’état correction
      this.finCorrection()
      break // case "correction":
    }

    case 'navigation':
      if (!this.sectionTerminee()) {
        this.etat = 'enonce'
      }
      this.finNavigation(true)
      break // case "navigation":
  }
}
