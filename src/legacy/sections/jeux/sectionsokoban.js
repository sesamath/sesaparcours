import { j3pElement } from 'src/legacy/core/functions'
import Sokoban from 'src/legacy/outils/sokoban/Sokoban'

/**
 * Un jeu de sokoban
 * @author JP Vanroyen
 * @since 2012-2013
 * @fileOverview
 */

export const params = {
  /**
   * Ce qui est paramétrable dans l’éditeur de graphe
   * @type {LegacySectionParamsParametres}
   */
  parametres: [
    ['fichier', 'serie1', 'string', 'serie0|serie1|serie2 (serie0: 10 tableaux très simples, serie1 : 8 tableaux, serie2 : 32 tableaux)'],
    ['level', 0, 'entier', 'Niveau de la série de départ'],
    ['navigation', true, 'boolean', 'Boutons de navigation'],
    ['son', false, 'boolean', 'Son actif ou inactif']
  ]
}

/**
 * section sokoban
 * @this {Parcours}
 */
export default function main () {
  const stor = this.storage

  switch (this.etat) {
    case 'enonce':
      if (this.debutDeLaSection) {
        this.construitStructurePage('presentation3')
        this.validOnEnter = false // ex donneesSection.touche_entree
      } else {
        this.videLesZones()
      }

      stor.sokoban = new Sokoban(this.zones.MG, {
        fichier: this.donneesSection.fichier,
        tableaucourant: this.donneesSection.level,
        dureeAnimation: 6,
        dureeDeplacement: 24,
        delai: 10,
        navigation: this.donneesSection.navigation,
        son: this.donneesSection.son
      })
      this.afficheTitre('Sokoban niveau ' + (this.donneesSection.level + 1))
      // en HD on ne veut pas de l’état (question et score), on affiche une consigne
      this.zonesElts.HD.innerText = 'Utilise les flèches du clavier pour pousser les chats sur leur couffin.'
      this.finEnonce()
      // on gère nos propres boutons dans MG
      this.cacheBoutonValider()
      break

    // FIXME on ne passe jamais dans le reste du code car il n’y a pas les boutons classiques de navigation
    case 'correction':
      var cbon = true // FIXME
      if (cbon) {
        this.score++
        j3pElement('explications').style.color = '#0A0'
        j3pElement('explications').innerHTML = 'C’est bien !'
        this.typederreurs[0]++
      } else {
        j3pElement('explications').style.color = '#A00'

        if (this.isElapsed) { // limite de temps
          j3pElement('explications').innerHTML = 'Temps dépassé !'
          this.typederreurs[10]++
        } else {
          j3pElement('explications').innerHTML = 'C’est faux !'
          this.typederreurs[1]++
        }
      }
      this.finCorrection('navigation', true)
      break // case "correction":

    case 'navigation':
      if (this.sectionTerminee()) {
        this.parcours.pe = this.score / this.donneesSection.nbitems
      } else {
        this.etat = 'enonce'
      }
      this.finNavigation(true)
      break // case "navigation":
  }
}
