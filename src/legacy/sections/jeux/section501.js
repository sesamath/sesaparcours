/**
 * Exercice de calcul mental
 * @author JP Vanroyen
 * @since 2012-2013
 * @fileOverview
 */

import { j3pChaine, j3pAddElt, j3pArrondi, j3pDistance, j3pDiv, j3pEmpty, j3pFocus, j3pGetParamsDroite, j3pGetRandomInt, j3pIntersectionDroites, j3pRestriction, j3pShowError, j3pStyle, j3pValeurde, j3pVirgule, j3pBarre } from 'src/legacy/core/functions'
import { j3pAffiche, j3pDesactive } from 'src/lib/mathquill/functions'
import Repere from 'src/legacy/outils/geometrie/Repere'
import { phraseBloc } from 'src/legacy/core/functionsPhrase'
import textesGeneriques from 'src/lib/core/textes'

const { cBien, cFaux, essaieEncore, tempsDepasse } = textesGeneriques

const structure = 'presentation3'

export const params = {
  outils: ['calculatrice'],
  /**
   * Ce qui est paramétrable dans l’éditeur de graphe
   * @type {LegacySectionParamsParametres}
   */
  parametres: [
    ['nbrepetitions', 10, 'entier', 'aucun'],
    ['limite', 5, 'entier', 'aucun'],
    ['nbchances', 1, 'entier', 'Nombre d’erreurs possibles (pour tenter de les corriger)'],
    ['a', '[2;9]', 'string', 'Intervalle d’appartenance du premier opérande'],
    ['b', '[2;9]', 'string', 'Intervalle d’appartenance du second opérande'],
    ['min', 0, 'entier', 'valeur minimale d’au moins un des deux opérandes'],
    ['operation', 'fois', 'liste', 'Deux types de calcul : une somme ou un produit', ['plus', 'fois']],
    ['mode', 'exact', 'liste', 'préciser si le résultat attendu est exact ou non', ['approche', 'exact']],
    ['scoreIndulgent', false, 'boolean', 'Mettre true pour ne pas tenir compte du temps s’il reste inférieur à la limite. En mode exact le score de chaque question sera 0 ou 1, en mode approche 1 pour une réponse exacte et 0 pour 20% d’erreur (linéaire entre les deux)'],
    ['graphique', true, 'boolean', 'En mode "approche", affichage ou non du graphique'],
    ['suivant', false, 'boolean', 'Mettre true pour le passage manuel au suivant (si false le programme passe automatiquement à la question suivante)'],
    ['delaiinterquestion', 2000, 'entier', 'Ignoré si suivant=true. Délai en ms pour le passage automatique à la question suivante'],
    ['titre', '', 'string', 'La chaine de caractère affichée après le titre Calcul mental'],
    ['lignesniveau', [3, 0.1, 90, 10, 0.3, 50], 'array', 'Deux lignes de niveau pour le calcul du score.<br>Ligne 90 : (3;0)-(0;0.1)<br>Ligne 50 : (10;0)-(0;0.5)']
  ]
}

const textes = {
  titreExo: 'Calcul mental £t',
  passageSuite: 'Passage automatique à la prochaine question dans £s s.',
  bonusTps: 'Bonus Temps cumulé de £s secondes',
  tpsEcoule: 'Temps écoulé pour la question : £d secondes.',
  tauxErreur: 'Erreur de £e %',
  leScore: 'Score obtenu : £s.',
  txtRep: 'La réponse était £r.'
}

/**
 * section 501
 * @this {Parcours}
 */
export default function main () {
  const me = this
  const stor = me.storage
  const ds = me.donneesSection
  // FIXME la gestion du temps global ne fonctionne pas en janvier 2022
  function afficheBonus () {
    if (hasTempsLimite && stor.tempsglobal !== 1000000) {
      phraseBloc(stor.zoneExpli, textes.bonusTps, { s: (stor.tempsBonus / 1000) })
    }
  }

  function afficheQuestion (question, conteneur) {
    const styleGrand = me.styles.etendre('grand.explications', {})
    j3pStyle(conteneur, { fontSize: styleGrand.fontSize })
    const elts = j3pAffiche(conteneur, '', question,
      {
        input1: { dynamique: true, texte: '', width: '20px', maxlength: '6' }
      })
    stor.inputElt = elts.inputList[0]
    j3pRestriction(stor.inputElt, '0-9')
    stor.zoneExpli = j3pAddElt(conteneur, 'div', '', { style: styleGrand })
    stor.zoneScore = j3pAddElt(conteneur, 'div', '', { style: styleGrand })

    if (ds.mode === 'approche' && ds.graphique) {
      stor.divGraphique = j3pAddElt(conteneur, 'div', '', { style: styleGrand })
    }

    j3pFocus(stor.inputElt)
  } // afficheQuestion

  function dessineApproche (dureeReponse, pcErr) {
    if (j3pArrondi(pcErr, 3) === 0) {
      stor.zoneExpli.style.color = me.styles.cbien
      phraseBloc(stor.zoneExpli, cBien)
      phraseBloc(stor.zoneExpli, textes.tpsEcoule, { d: j3pVirgule(dureeReponse) })
      // on a fini, rien à dessiner
      return
    }
    phraseBloc(stor.zoneExpli, textes.tauxErreur, { e: j3pVirgule(j3pArrondi((pcErr) * 100, 1)) })
    phraseBloc(stor.zoneExpli, textes.tpsEcoule, { d: j3pVirgule(dureeReponse) })
    const [l0, l1, , l3, l4] = ds.lignesniveau
    const isGrand = (pcErr > 0.6 || dureeReponse > 16)
    const repere = new Repere({
      idConteneur: stor.divGraphique,
      aimantage: false,
      visible: true,
      trame: true,
      fixe: true,
      larg: 300,
      haut: 250,
      pasdunegraduationX: isGrand ? 4 : 2,
      pixelspargraduationX: 30,
      pasdunegraduationY: isGrand ? 0.2 : 0.1,
      pixelspargraduationY: 30,
      xO: 25,
      yO: 225,
      debuty: 0,
      negatifs: false,
      objets: []
    })
    repere.add({
      type: 'point',
      nom: 'A',
      par1: l0,
      par2: 0,
      fixe: true,
      visible: true,
      etiquette: false,
      style: { couleur: '#F0F', epaisseur: 2, taille: 18, taillepoint: 0 }
    })
    repere.add({
      type: 'point',
      nom: 'B',
      par1: 0,
      par2: l1,
      fixe: true,
      visible: true,
      etiquette: false,
      style: { couleur: '#F0F', epaisseur: 2, taille: 18, taillepoint: 0 }
    })
    repere.add({
      type: 'segment',
      nom: 's1',
      par1: 'A',
      par2: 'B',
      style: { couleur: '#F0F', epaisseur: 2 }/*, graduations:{type:1,nb:3,couleur:"#000",longueur:10,epaisseur:3,opacite:1} */
    })
    repere.add({
      type: 'point',
      nom: 'C',
      par1: l3,
      par2: 0,
      fixe: true,
      visible: true,
      etiquette: false,
      style: { couleur: '#00F', epaisseur: 2, taille: 18, taillepoint: 0 }
    })
    repere.add({
      type: 'point',
      nom: 'D',
      par1: 0,
      par2: l4,
      fixe: true,
      visible: true,
      etiquette: false,
      style: { couleur: '#00F', epaisseur: 2, taille: 18, taillepoint: 0 }
    })
    repere.add({
      type: 'segment',
      nom: 's2',
      par1: 'C',
      par2: 'D',
      style: { couleur: '#00F', epaisseur: 2 }/*, graduations:{type:1,nb:3,couleur:"#000",longueur:10,epaisseur:3,opacite:1} */
    })
    repere.add({
      type: 'polygone',
      nom: 'pol1',
      par1: ['A', 'B', 'D', 'C'],
      style: { couleur: '#00F', couleurRemplissage: '#00F', opaciteRemplissage: 0.3 }
    })
    repere.add({
      type: 'polygone',
      nom: 'pol2',
      par1: ['_O', 'A', 'B'],
      style: { couleur: '#F0F', couleurRemplissage: '#F0F', opaciteRemplissage: 0.3 }
    })
    repere.add({
      type: 'point',
      nom: 'M1',
      par1: dureeReponse,
      par2: 0,
      fixe: true,
      visible: true,
      etiquette: false,
      style: { couleur: '#00F', epaisseur: 2, taille: 18, taillepoint: 0 }
    })
    repere.add({
      type: 'point',
      nom: 'M2',
      par1: 0,
      par2: pcErr,
      fixe: true,
      visible: true,
      etiquette: false,
      style: { couleur: '#00F', epaisseur: 2, taille: 18, taillepoint: 0 }
    })
    repere.add({
      type: 'segment',
      nom: 's3',
      par1: 'M',
      par2: 'M1',
      style: { couleur: '#F00', epaisseur: 2 }/*, graduations:{type:1,nb:3,couleur:"#000",longueur:10,epaisseur:3,opacite:1} */
    })
    repere.add({
      type: 'segment',
      nom: 's4',
      par1: 'M',
      par2: 'M2',
      style: { couleur: '#F00', epaisseur: 2 }/*, graduations:{type:1,nb:3,couleur:"#000",longueur:10,epaisseur:3,opacite:1} */
    })
    repere.add({
      type: 'point',
      nom: 'M',
      par1: dureeReponse,
      par2: pcErr,
      fixe: true,
      visible: true,
      etiquette: false,
      style: { couleur: '#F00', epaisseur: 3, taille: 18, taillepoint: 6 }
    })
    repere.add({
      type: 'point',
      nom: 'Temps',
      par1: 14,
      par2: 0,
      fixe: true,
      visible: true,
      etiquette: true,
      style: { couleur: '#000', epaisseur: 3, taille: 14, taillepoint: 0 }
    })
    repere.add({
      type: 'point',
      nom: 'Erreur',
      par1: 1,
      par2: 0.6,
      fixe: true,
      visible: true,
      etiquette: true,
      style: { couleur: '#000', epaisseur: 3, taille: 14, taillepoint: 0 }
    })
    repere.construit()
  } // dessineApproche

  /**
   * Retourne le score entre 0 et 100
   * @param dureeReponse
   * @param pcErr
   * @returns {number|number}
   */
  function getScore (dureeReponse, pcErr) {
    if (ds.scoreIndulgent) {
      if (ds.mode === 'exact') return pcErr === 0 ? 1 : 0
      return Math.max(0, 100 - pcErr * 500)
    }
    const estSurSegment = (m, a, b) => Math.abs(j3pDistance(a, m) + j3pDistance(m, b) - j3pDistance(a, b)) < 0.00001
    /**
     * on vérifie si m est sous la droite d1 dont on a l’équation cartésienne ax+by+c=0
     * @param {Point} m
     * @param {Droite} d1
     * @returns {Boolean}
     */
    function estSousDroite (m, d1) {
      const { a, b, c } = d1
      const { x, y } = m
      return y < (-a * x - c) / b
    }
    // on utilise ça comme coordonnée d’un point pour afficher graphiquement le score
    const m = { x: dureeReponse, y: pcErr }
    // [0,0],[2,0],[1,1],[0,2],[4,0],[2,2],[0,4],[90,50]
    // initialisés à partir de ds.lignesniveau
    const { o, a1, a2, a3, b1, b2, b3 } = stor.pointsNiveaux
    const om = j3pGetParamsDroite(o, m)
    const a1a2 = j3pGetParamsDroite(a1, a2)
    const a2a3 = j3pGetParamsDroite(a2, a3) // Rémi le 27/12/2022 : a2 étant le milieu de [a1a3], les variables a1a2 et a2a3 sont égales
    // A moins que quelque chose ne m'échappe...
    const b1b2 = j3pGetParamsDroite(b1, b2)
    const b2b3 = j3pGetParamsDroite(b2, b3) // Rémi le 27/12/2022 : idem pour b1b2 et b2b3

    let k1 = j3pIntersectionDroites(om, a1a2)
    if (k1 && !estSurSegment(k1, a1, a2)) k1 = j3pIntersectionDroites(om, a2a3) // Rémi le 27/12/2022 : je ne vois pas à quoi cela peut servir...
    let k2 = j3pIntersectionDroites(om, b1b2)
    if (k2 && !estSurSegment(k2, b1, b2)) k2 = j3pIntersectionDroites(om, b2b3) // Rémi le 27/12/2022 : je ne vois pas à quoi cela peut servir...
    // Rémi le 27/12/2022 : k1 et k2 sont les points d’intersection des droites (OM) et (a1a2) et (b1b2) respectivement

    const [l1, l2] = stor.niveaux // [90, 50] dans le paramétrage initial
    let teta, score
    if (estSousDroite(m, a1a2)) {
      teta = j3pDistance(o, m) / j3pDistance(o, k1)
      score = j3pArrondi(100 - teta * (100 - l1), 1)
    } else if (estSousDroite(m, b1b2)) {
      teta = j3pDistance(m, k1) / j3pDistance(k1, k2)
      score = j3pArrondi(l1 - teta * (l1 - l2), 1)
    } else {
      // Il est en dehors de la ligne des 90%, donc je lui mets 0
      score = 0
    }
    if (!Number.isFinite(score) || score < 0 || score > 100) {
      console.error(Error('calcul de score foireux'), { score, dureeReponse, pcErr, pointsNiveaux: stor.pointsNiveaux })
      me.notif(`calcul de score foireux ${score} dans la section 501 avec m=`, { m, pointsNiveaux: stor.pointsNiveaux })
      // le bug doit profiter à l’élève
      score = 100
    }

    return score
  }

  let hasTempsLimite = false

  switch (me.etat) {
    case 'enonce': {
      // code exécuté UNE SEULE FOIS avant le premier item
      if (me.debutDeLaSection) {
        // check des intervalles et du min
        const min = ds.min
        const re = /^\[([0-9]+);([0-9]+)]$/
        let chunks = re.exec(ds.a)
        if (!chunks) return j3pShowError('Intervalle invalide pour le paramètre a, impossible de lancer l’exercice')
        const [, aMin, aMax] = chunks
        chunks = re.exec(ds.b)
        if (!chunks) return j3pShowError('Intervalle invalide pour le paramètre b, impossible de lancer l’exercice')
        const [, bMin, bMax] = chunks
        if (min > Math.min(aMax, bMax)) return j3pShowError(`Paramètre min incohérent (${min} supérieur aux max des deux intervalles ${aMax} pour ${ds.a} et ${bMax} pour ${ds.b})`)
        // params ok on stocke ça (pour toutes les répétitions suivantes)
        stor.limites = { min, aMin, aMax, bMin, bMax }

        // calcul des points & droites des niveaux
        // me.lignesniveau = [3,0.1,90,6,0.25,50]
        const [x1, y1, niv1, x2, y2, niv2] = ds.lignesniveau
        // on affecte ça utilisé par getScore
        stor.pointsNiveaux = {
          o: { x: 0, y: 0 },
          a1: { x: x1, y: 0 },
          a2: { x: x1 / 2, y: y1 / 2 },
          a3: { x: 0, y: y1 },
          b1: { x: x2, y: 0 },
          b2: { x: x2 / 2, y: y2 / 2 },
          b3: { x: 0, y: y2 }
        }
        stor.niveaux = [niv1, niv2] // [l1, l2]
        stor.tempsBonus = 0

        // init de la page
        me.construitStructurePage({ structure, ratioGauche: 0.6 })
        me.afficheTitre(j3pChaine(textes.titreExo, { t: ds.titre }))
      } else {
        if (stor.timerId) {
          clearTimeout(stor.timerId)
          delete stor.timerId
        }
        me.videLesZones() // ça va remettre les boutons après effacement de MG
      }

      // code énoncé exécuté à chaque répétition (première ou pas)
      // j3pDiv(me.zonesElts.MG, { id: 'CAR', contenu: '', coord: [100, 50], style: me.styles.etendre('petit.explications', {}) })
      hasTempsLimite = ds.limite > 0
      stor.tsEnonce = Date.now()
      j3pDiv(me.zonesElts.MG, { id: 'unconteneur', contenu: '', coord: [300, 30], style: { width: '200px', height: '30px' } })

      // tirage du calcul à faire
      const { min, aMin, aMax, bMin, bMax } = stor.limites // déterminé d’après donneesSection au début de la section
      let nombre1, nombre2
      do {
        nombre1 = j3pGetRandomInt(aMin, aMax)
        nombre2 = j3pGetRandomInt(bMin, bMax)
      } while ((nombre1 < min) && (nombre2 < min))
      // on stocke la bonne réponse
      stor.reponse = (ds.operation === 'fois')
        ? Math.round(parseInt(nombre1) * parseInt(nombre2))
        : Math.round(parseInt(nombre1) + parseInt(nombre2))

      const debutquestion = me.questionCourante + ')'
      stor.conteneur = j3pAddElt(me.zonesElts.MG, 'div', '', { style: me.styles.etendre('moyen.enonce', { padding: '40px' }) })
      const numQuest = j3pAddElt(stor.conteneur, 'span', debutquestion)
      j3pStyle(numQuest, { color: '#444' })
      const leSpan = j3pAddElt(stor.conteneur, 'span', ' ')
      const chop = ds.operation === 'fois' ? '×' : '+'
      const choix = j3pGetRandomInt(0, 1)
      const question = (choix === 0)
        ? `${nombre1} ${chop} ${nombre2} = @1@`
        : `${nombre2} ${chop} ${nombre1} = @1@`
      afficheQuestion(question, leSpan)
      me.finEnonce()
      break
    } // case "enonce":

    case 'correction': {
      j3pEmpty(stor.zoneExpli)
      // on teste si une réponse a été saisie
      const repEleve = j3pValeurde(stor.inputElt)
      if (!repEleve && !me.isElapsed) {
        me.reponseManquante(stor.zoneExpli)
        stor.inputElt.focus()
        return // on reste en correction
      }

      // une réponse a été saisie
      stor.tsCorrection = Date.now()

      const dureeReponse = j3pArrondi((stor.tsCorrection - stor.tsEnonce) / 1000, 1)
      // Pourcentage d’erreur
      const pcErr = stor.reponse === 0
        // si la bonne réponse est 0 le pourcentage est 0 ou 1
        ? Math.abs(repEleve) < 1e-9
          ? 1
          : 0
        // sinon on peut le calculer
        : Math.abs(stor.reponse - repEleve) / stor.reponse

      const bonResultat = pcErr < 1e-9
      const cbon = (ds.mode === 'exact') ? bonResultat : true
      j3pStyle(stor.inputElt, { color: bonResultat ? me.styles.cbien : me.styles.cfaux })
      if (cbon && !me.isElapsed) {
        // bon ou approché (même de très très loin)
        if (ds.mode === 'approche') {
          const scoreQuest = j3pArrondi((getScore(dureeReponse, pcErr) / 100), 2)
          if (repEleve) dessineApproche(dureeReponse, pcErr)
          phraseBloc(stor.zoneExpli, textes.leScore, { s: j3pVirgule(scoreQuest) })
          me.score += scoreQuest
          if (!bonResultat) j3pBarre(stor.inputElt) // on vient ici alors que le résultat peut être faux... On a vu plus logique...
        } else {
          // mode exact
          me.score++
          stor.zoneExpli.style.color = me.styles.cbien
          stor.zoneExpli.innerHTML = cBien
          me.typederreurs[0]++

          stor.tempsBonus += (ds.limite * 1000) - (stor.tsCorrection - stor.tsEnonce)
          afficheBonus()
        }
        j3pDesactive(stor.inputElt)
        // on passe d’office en navigation tout de suite
        me.finCorrection('navigation', true)
      } else {
        // Pas de bonne réponse
        stor.tempsBonus -= 4000
        stor.zoneExpli.style.color = me.styles.cfaux
        // à cause de la limite de temps :
        if (me.isElapsed) { // limite de temps
          stor.zoneExpli.innerHTML = tempsDepasse
          me.typederreurs[10]++
          phraseBloc(stor.zoneExpli, textes.txtRep, { r: stor.reponse })
          afficheBonus()
          stor.tempsdepasse = true
          j3pDesactive(stor.inputElt)
          me.finCorrection('navigation', true)
        } else {
          // réponse fausse :
          stor.zoneExpli.innerHTML = cFaux
          // S’il y a plusieurs chances,on appelle à nouveau le bouton Valider
          if (me.essaiCourant < ds.nbchances) {
            stor.zoneExpli.innerHTML += '<br>' + essaieEncore
            me.typederreurs[1]++
            // on laisse le bouton valider (déjà là) et l’état correction
            me.finCorrection()
          } else {
            // Erreur au nème essai
            phraseBloc(stor.zoneExpli, textes.txtRep, { r: stor.reponse })
            afficheBonus()
            j3pBarre(stor.inputElt)
            me.typederreurs[2]++
            me.finCorrection('navigation', true)
          }
        }
      }

      break
    } // case "correction":

    case 'navigation':
      if (me.sectionTerminee()) {
        me.parcours.pe = me.score / ds.nbitems
        me.afficheBoutonSectionSuivante()
        if (hasTempsLimite && stor.tempsglobal !== 1000000) {
          stor.tempsglobal += stor.tempsBonus / 1000
        }
      } else {
        me.etat = 'enonce'
        if (!ds.suivant) {
          me.cacheBoutonSuite()
          // on passera à la question suivante automatiquement après x secondes,
          // mais faut mémoriser le timer pour le désactiver si on passe au suivant avec le bouton ou entrée
          phraseBloc(stor.zoneExpli, j3pChaine(textes.passageSuite, { s: Math.round(ds.delaiinterquestion / 1000) }))
          stor.timerId = setTimeout(me.sectionCourante.bind(this), ds.delaiinterquestion)
        }
      }
      me.finNavigation(true)
      break // case "navigation":
  }
}
