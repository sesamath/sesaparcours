import { j3pAddElt, j3pAjouteBouton, j3pShowError } from 'src/legacy/core/functions'
/**
 * Section permettant de choisir le nœud suivant en cliquant sur un bouton
 * Il y aura autant de boutons que de choix définis dans les paramètres (5 max)
 * @fileOverview
 * @author Tommy Barroy <t.barroy@laposte.net>
 * @since 2017-10
 */
export const params = {
  /**
   * Ce qui est paramétrable dans l’éditeur de graphe
   * @type {LegacySectionParamsParametres}
   */
  parametres: [
    ['limite', 60, 'entier', 'Temps maximum pour choisir (sinon ce sera le premier choix, mettre 0 pour ne pas limiter)'],
    ['Demande', '', 'string', 'Explication du choix'],
    ['Titre', '', 'string', 'Titre de la page'],
    ['Choix1', '', 'string', 'Choix1'],
    ['Choix2', '', 'string', 'Choix2'],
    ['Choix3', '', 'string', 'Choix3'],
    ['Choix4', '', 'string', 'Choix4'],
    ['Choix5', '', 'string', 'Choix5']
  ],
  // dans le cas d’une section QUANTITATIVE
  pe: [
    { pe_1: 'Choix1' },
    { pe_2: 'Choix2' },
    { pe_3: 'Choix3' },
    { pe_4: 'Choix4' },
    { pe_5: 'Choix5' }
  ]
}

/**
 * section choix
 * @this {Parcours}
 */
export default function main () {
  const me = this
  const ds = me.donneesSection
  const suffixes = [1, 2, 3, 4, 5]
  const stor = me.storage

  function valide (i) {
    const pe = ds['pe_' + i]
    if (!pe) return j3pShowError(Error('Valeur de choix invalide : ' + i))
    me.parcours.pe = pe
    me.etat = 'navigation'
    me.finNavigation()
  }

  switch (this.etat) {
    case 'enonce':
      // on est appelé une seule fois, forcément au début de la section, inutile de tester

      // ce nœud choix n’a pas de score, il ne comptera pas dans le calcul du pourcentage global du graphe (et on ne précise pas de score)
      this.setPassive()

      // check des params
      if (suffixes.every((suffix) => ds['Choix' + suffix] === '')) {
        ds.Choix1 = 'Aucun choix n’a été défini dans les paramètres'
      }
      // Construction de la page
      this.construitStructurePage('presentation3', 0.75)
      if (ds.Titre) this.afficheTitre(ds.Titre)

      stor.lesdiv = {}
      stor.lesdiv.conteneur = stor.lesdiv.conteneur = j3pAddElt(me.zones.MG, 'div', {
        style: me.styles.etendre('toutpetit.enonce', { margin: '10px' })
      })
      if (ds.Demande) {
        j3pAddElt(stor.lesdiv.conteneur, 'p', ds.Demande, {
          style: {
            textAlign: 'center',
            fontWeight: 'bold',
            marginBottom: '2em'
          }
        })
      }
      stor.lesdiv.travail = j3pAddElt(stor.lesdiv.conteneur, 'div')
      for (const suffix of suffixes) {
        const choix = 'Choix' + suffix
        if (ds[choix] !== '') {
          j3pAjouteBouton(stor.lesdiv.travail, valide.bind(null, suffix), { value: ds[choix], className: 'MepBoutons', style: { marginBottom: '2em', display: 'block' } })
        }
      }
      this.finEnonce()
      // on veut pas du bouton sectionSuivante mis par finEnonce (pour les sections passives, sinon c'est le bouton valider)
      // puisqu’on sort de cette section en cliquant sur un des choix ci-dessus
      this.cacheBoutonSectionSuivante()
      this.etat = 'correction' // on rectifie, car pour les sections passives c'est navigation après finEnonce
      break

    case 'correction': {
      // forcément limite de temps atteinte, mais on blinde quand même
      if (!this.isElapsed) return j3pShowError(Error('Appel incohérent (mode correction sans atteinte du temps limite)'))
      // temps dépassé, on prend le premier choix
      const firstSuffix = suffixes.find((suffix) => ds['Choix' + suffix] !== '')
      if (firstSuffix) {
        // faut le setTimeout pour laisser le modèle finir ses traitements sur cette section
        setTimeout(valide.bind(null, firstSuffix), 0)
      } else {
        j3pShowError(Error('Temps maximum atteint mais aucun choix par défaut à faire !'))
      }
      // il ne faut pas appeler finCorrection(), c'est l'appel de valide qui va appeler finNavigation pour passer à la section suivante
      break
    }
  }
}
