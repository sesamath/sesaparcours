import { j3pAddContent, j3pAddElt, j3pAjouteBouton, j3pArrondi, j3pClone, j3pEmpty, j3pShowError, j3pShuffle } from 'src/legacy/core/functions'
import { addDefaultTable } from 'src/legacy/themes/table'
import { getMtgCore } from 'src/lib/outils/mathgraph'
import getDefaultProgramme from './defaultProgramme'
import { j3pAfficheCroixFenetres, j3pCreeFenetres, j3pToggleFenetres } from 'src/legacy/core/functionsJqDialog'
import { j3pAffiche, unLatexify } from 'src/lib/mathquill/functions'
import ZoneStyleMathquill3 from 'src/legacy/outils/zoneStyleMathquill/ZoneStyleMathquill3'
import { j3pCreeSVG } from 'src/legacy/core/functionsSvg'
import { placeJouerSon, placeJouerVideo } from 'src/legacy/outils/zoneStyleMathquill/functions'
import textesGeneriques from 'src/lib/core/textes'

const { cBien, cFaux, essaieEncore, reponseIncomplete, tempsDepasse } = textesGeneriques

async function initEditor (container, values, btn) {
  const { default: editor } = await import('./editorProgramme.js')
  return editor(container, values, btn) // la fct qui prend un conteneur et les params comme arguments
}

/**
 * Ajouter une description ici
 * @fileOverview
 * @author Tommy Barroy <t.barroy@laposte.net>
 * @since 2017-10
 */
export const params = {
  outils: ['calculatrice', 'mathquill'],
  /**
   * Ce qui est paramétrable dans l’éditeur de graphe
   * @type {LegacySectionParamsParametres}
   */
  parametres: [
    ['nbrepetitions', 9, 'entier', 'Nombre de répétitions de la section'],
    ['limite', 0, 'entier', 'Temps disponible par répétition, en secondes'],
    ['nbchances', 2, 'entier', 'Nombre d’essais par répétition'],
    ['Liste', JSON.stringify(getDefaultProgramme()), 'multiEditor', 'Json figure de base et figure attendue.', initEditor],
    ['ordonne', 'Oui', 'liste', '<u>Oui</u>:  Exercices dans l’ordre', ['Oui', 'Non', 'Oui_par_Niveau', 'Aléa_par_niveau']],
    ['Calculatrice', 'Non', 'liste', '<u>Oui</u>:  Une calculatrice est disponible', ['Oui', 'Non']],
    ['theme', 'zonesAvecImageDeFond', 'liste', 'Modèle pour l’affichage', ['standard', 'zonesAvecImageDeFond']]
  ],
  // dans le cas d’une section QUANTITATIVE
  pe: [
  ]
}

/**
 * section choix
 * @this {Parcours}
 */
export default function main () {
  const me = this
  const ds = me.donneesSection
  const stor = me.storage
  const genMGEx = 'TWF0aEdyYXBoSmF2YTEuMAAAABM+TMzNAAJmcv###wEA#wEAAAAAAAAAAAM7AAACbwAAAQEAAAAAAAAAAAAAADf#####AAAAAQAKQ0NhbGNDb25zdAD#####AAJwaQAWMy4xNDE1OTI2NTM1ODk3OTMyMzg0Nv####8AAAABAApDQ29uc3RhbnRlQAkh+1RELRj#####AAAAAQAKQ1BvaW50QmFzZQD#####AYAA#wAQAAFIAAAAAAAAAAAAQAgAAAAAAAAAAAkAAUB4IAAAAAAAQHZuFHrhR64AAAACAP####8BgAD#ABAAAUMAAAAAAAAAAABACAAAAAAAAAAACQABQHoQAAAAAABATvCj1wo9cf####8AAAABABRDRHJvaXRlRGlyZWN0aW9uRml4ZQD#####AYAA#wEQAAABAAACBAAAAAEBP#AAAAAAAAAAAAADAP####8BgAD#ARAAAAEAAAIEAAAAAgA#8AAAAAAAAP####8AAAABAA9DUG9pbnRMaWVEcm9pdGUA#####wGAAP8AEAABRQAAAAAAAAAAAEAIAAAAAAAAAAAJAAFAb6AAAAAAAAAAAAQAAAAEAP####8BgAD#ABAAAUYAAAAAAAAAAABACAAAAAAAAAAACQABwHVwAAAAAAAAAAAD#####wAAAAEACENTZWdtZW50AP####8AgAD#ABAAAAEAAAAEAAAAAgAAAAUAAAAFAP####8AgAD#ABAAAAEAAAAEAAAAAQAAAAYAAAAEAP####8A#wAAARAAAUcAAAAAAAAAAABACAAAAAAAAAAACAABP94KcvBTl4MAAAAIAAAABAD#####AP8AAAEQAAFEAAAAAAAAAAAAQAgAAAAAAAAAAAgAAT#VgIGEjaj7AAAABwAAAAMA#####wH#AAABEAAAAQAAAAQAAAAJAD#wAAAAAAAAAAAAAwD#####Af8AAAEQAAABAAAABAAAAAoBP#AAAAAAAAD#####AAAAAQAQQ0ludERyb2l0ZURyb2l0ZQD#####Af8AAAAQAAAAAAAAAAAAAABACAAAAAAAAAADbGltCAAAAAALAAAADAAAAAQA#####wH#AAAAEAAAAAAAAAAAAAAAQAgAAAAAAAAAAAgAAcB6IAAAAAAAAAAACwAAAAQA#####wH#AAAAEAAAAAAAAAAAAAAAQAgAAAAAAAAAAAgAAcB+8AAAAAAAAAAADAAAAAUA#####wD#AAAAEAAAAQAAAAQAAAANAAAADgAAAAUA#####wD#AAAAEAAAAQAAAAQAAAANAAAADwAAAAIA#####wH#AAAAEAABQQAAAAAAAAAAAEAIAAAAAAAAAAAIAAFAfxAAAAAAAEBbuFHrhR64AAAAAgD#####Af8AAAAQAAFCAAAAAAAAAAAAQAgAAAAAAAAAAAgAAUCAaAAAAAAAQFu4UeuFHrj#####AAAAAQAJQ0xvbmd1ZXVyAP####8AAAASAAAAE#####8AAAABABRDSW1wbGVtZW50YXRpb25Qcm90bwD#####ABJNZXN1cmUgZGUgbG9uZ3VldXIAAAAFAAAAAgAAAAIAAAACAAAACv####8AAAABAAtDTWVkaWF0cmljZQAAAAAVAQAAAAAQAAABAAAAAQAAAAIAAAAK#####wAAAAEAB0NNaWxpZXUAAAAAFQEAAAAAEAAAAQAABQAAAAACAAAACv####8AAAACAAlDQ2VyY2xlT1IAAAAAFQEAAAAAAAABAAAAFwAAAAFAMAAAAAAAAAH#####AAAAAQAQQ0ludERyb2l0ZUNlcmNsZQAAAAAVAAAAFgAAABj#####AAAAAQAQQ1BvaW50TGllQmlwb2ludAAAAAAVAQAAAAAQAAABAAAFAAEAAAAZAAAABwEAAAAVAAAAAgAAAAr#####AAAAAQAPQ1ZhbGV1ckFmZmljaGVlAQAAABUB#wAAAQAAAAAAGhEAAAAAAAEAAAABAAAAAQAAAAAAAAAAAAAAAAEAAAAbAAAACAD#####ABJNZXN1cmUgZGUgbG9uZ3VldXIAAAAFAAAAAgAAAAIAAAACAAAABQAAAAkAAAAAHQEAAAAAEAAAAQAAAAEAAAACAAAABQAAAAoAAAAAHQEAAAAAEAAAAQAABQAAAAACAAAABQAAAAsAAAAAHQEAAAAAAAABAAAAHwAAAAFAMAAAAAAAAAEAAAAMAAAAAB0AAAAeAAAAIAAAAA0AAAAAHQEAAAAAEAAAAQAABQABAAAAIQAAAAcBAAAAHQAAAAIAAAAFAAAADgEAAAAdAf8AAAEAAAAAACIRAAAAAAABAAAAAQAAAAEAAAAAAAAAAAAAAAABAAAAIwAAAAgA#####wASTWVzdXJlIGRlIGxvbmd1ZXVyAAAABQAAAAIAAAACAAAABgAAAAkAAAAJAAAAACUBAAAAABAAAAEAAAABAAAABgAAAAkAAAAKAAAAACUBAAAAABAAAAEAAAUAAAAABgAAAAkAAAALAAAAACUBAAAAAAAAAQAAACcAAAABQDAAAAAAAAABAAAADAAAAAAlAAAAJgAAACgAAAANAAAAACUBAAAAABAAAAEAAAUAAQAAACkAAAAHAQAAACUAAAAGAAAACQAAAA4BAAAAJQH#AAABAAAAAAAqEQAAAAAAAQAAAAEAAAABAAAAAAAAAAAAAAAAAQAAACsAAAAIAP####8AEk1lc3VyZSBkZSBsb25ndWV1cgAAAAUAAAACAAAAAgAAAAYAAAABAAAACQAAAAAtAQAAAAAQAAABAAAAAQAAAAYAAAABAAAACgAAAAAtAQAAAAAQAAABAAAFAAAAAAYAAAABAAAACwAAAAAtAQAAAAAAAAEAAAAvAAAAAUAwAAAAAAAAAQAAAAwAAAAALQAAAC4AAAAwAAAADQAAAAAtAQAAAAAQAAABAAAFAAEAAAAxAAAABwEAAAAtAAAABgAAAAEAAAAOAQAAAC0B#wAAAQAAAAAAMhEAAAAAAAEAAAABAAAAAQAAAAAAAAAAAAAAAAEAAAAz#####wAAAAEAB0NDYWxjdWwA#####wABQQAFQ0QvQ0X#####AAAAAQAKQ09wZXJhdGlvbgP#####AAAAAQAPQ1Jlc3VsdGF0VmFsZXVyAAAAGwAAABEAAAAjAAAADwD#####AAFCAAVGRy9GSAAAABADAAAAEQAAACsAAAARAAAAMwAAABT##########w=='

  function traiteMathQuill (ch) {
    ch = unLatexify(ch)
    // mathlive peut mettre des {} vides
    ch = ch.replace(/\\{\\}/g, '')
    ch = ch.replace(/\^(\d)(\d)/g, '^$1*$2') // On peut avoir par exemple 2^35^4 pour 2^3*5^4
    ch = stor.mtgAppLecteur.addImplicitMult('fhdsjkqfhdksj2', ch)// Traitement des multiplications implicites
    return ch
  }
  function initSection () {
    // chargement mtg
    me.construitStructurePage('presentation1bis')
    try {
      stor.Liste = JSON.parse(ds.Liste)
      const aComp = []
      //
      for (let prog = 0; prog < stor.Liste.liste.length; prog++) {
        let aCompte = true
        if (stor.Liste.liste[prog].yaImage) {
          if (stor.Liste.liste[prog].image !== 'string') {
            if (stor.Liste.liste[prog].image.type === 'clone') {
              aCompte = false
              stor.Liste.liste[prog].image = j3pClone(aComp[stor.Liste.liste[prog].image.num])
            }
          }
          if (aCompte) aComp.push(j3pClone(stor.Liste.liste[prog].image))
        }
        aCompte = true
        if (stor.Liste.liste[prog].yaSon) {
          if (stor.Liste.liste[prog].son !== 'string') {
            if (stor.Liste.liste[prog].son.type === 'clone') {
              aCompte = false
              stor.Liste.liste[prog].son = j3pClone(aComp[stor.Liste.liste[prog].son.num])
            }
          }
          if (aCompte) aComp.push(j3pClone(stor.Liste.liste[prog].son))
        }
        aCompte = true
        if (stor.Liste.liste[prog].yaFigure) {
          if (stor.Liste.liste[prog].figureTxt !== 'string') {
            if (stor.Liste.liste[prog].figureTxt.type === 'clone') {
              aCompte = false
              stor.Liste.liste[prog].figureTxt = j3pClone(aComp[stor.Liste.liste[prog].figureTxt.num])
            }
          }
          if (aCompte) aComp.push(j3pClone(stor.Liste.liste[prog].figureTxt))
        }
      }
    } catch (error) {
      console.error(error)
      j3pShowError('Parametres invalide')
      stor.Legendes = getDefaultProgramme()
    }
    let titre = stor.Liste.titre
    if (!titre || titre === '') titre = ' '
    me.afficheTitre(titre)
    stor.lesExos = []
    const chOrdonne = ds.ordonne
    stor.ExoParniv = []
    let nivDispos = []
    if (chOrdonne === 'Oui_par_Niveau' || chOrdonne === 'Aléa_par_niveau') {
      for (let i = 0; i < stor.Liste.liste.length; i++) {
        const nivExo = stor.Liste.liste[i].niv || 0
        if (yaniv(nivExo, stor.ExoParniv)) {
          metExoAniv(nivExo, stor.Liste.liste[i], stor.ExoParniv)
        } else {
          nivDispos.push(nivExo)
          stor.ExoParniv.push({ niv: nivExo, exos: [j3pClone(stor.Liste.liste[i])] })
        }
      }
      for (let i = 0; i < stor.ExoParniv.length; i++) {
        stor.ExoParniv[i].exos = j3pShuffle(stor.ExoParniv[i].exos)
      }
      stor.consExoParniv = j3pClone(stor.ExoParniv)
      if (chOrdonne === 'Oui_par_Niveau') {
        nivDispos = nivDispos.sort()
      } else {
        nivDispos = j3pShuffle(nivDispos)
      }
    }
    switch (chOrdonne) {
      case 'Non': {
        const exPos = j3pShuffle(stor.Liste.liste)
        for (let i = 0; i < ds.nbrepetitions; i++) {
          stor.lesExos.push(exPos[i % exPos.length])
        }
        break
      }
      case 'Oui':
        for (let i = 0; i < ds.nbrepetitions; i++) {
          stor.lesExos.splice(0, 0, stor.Liste.liste[i % stor.Liste.liste.length])
        }
        break
      default: {
        let cmpt = -1
        while (stor.lesExos.length < ds.nbrepetitions) {
          if (stor.ExoParniv.length === 0) stor.ExoParniv = j3pClone(stor.consExoParniv)
          cmpt++
          if (cmpt > nivDispos.length - 1) cmpt = 0
          metDansExo(cmpt, nivDispos)
        }
      }
    }
    getMtgCore({ withMathJax: true })
      .then(
        // success
        (mtgAppLecteur) => {
          stor.mtgAppLecteur = mtgAppLecteur
          stor.listePourCalc = mtgAppLecteur.createList('TWF0aEdyYXBoSmF2YTEuMAAAABM+TMzNAAJmcv###wEA#wEAAAAAAAAAAANLAAACfwAAAQEAAAAAAAAAAQAAAAv#####AAAAAQAKQ0NhbGNDb25zdAD#####AAJwaQAWMy4xNDE1OTI2NTM1ODk3OTMyMzg0Nv####8AAAABAApDQ29uc3RhbnRlQAkh+1RELRj#####AAAAAQAKQ1BvaW50QmFzZQD#####AAAAAAAOAAFVAMAkAAAAAAAAQBAAAAAAAAAAAAUAAEAvMan752yLQC8xqfvnbIv#####AAAAAQAUQ0Ryb2l0ZURpcmVjdGlvbkZpeGUA#####wEAAAAAEAAAAQAAAAEAAAABAT#wAAAAAAAA#####wAAAAEAD0NQb2ludExpZURyb2l0ZQD#####AAAAAAAOAAFWAMAAAAAAAAAAQBAAAAAAAAAAAAUAAUA#Man752yLAAAAAv####8AAAABAAhDU2VnbWVudAD#####AAAAAAAQAAABAAAAAQAAAAEAAAAD#####wAAAAEAB0NNaWxpZXUA#####wEAAAAAEAAAAAAAAAAAAAAAQAgAAAAAAAAAAAUAAAAAAQAAAAP#####AAAAAgAMQ0NvbW1lbnRhaXJlAP####8AAAAAAAAAAAAAAAAAQBgAAAAAAAAAAAAAAAUMAAAAAAABAAAAAAAAAAEAAAAAAAAAAAABMf####8AAAABAAlDTG9uZ3VldXIA#####wAAAAEAAAAD#####wAAAAEAB0NDYWxjdWwA#####wABeAABMQAAAAE#8AAAAAAAAP####8AAAABAAVDRm9uYwD#####AAFmAAMyKnj#####AAAAAQAKQ09wZXJhdGlvbgIAAAABQAAAAAAAAAD#####AAAAAgARQ1ZhcmlhYmxlRm9ybWVsbGUAAAAAAAF4AAAACQD#####AAF5AARmKHgp#####wAAAAEADkNBcHBlbEZvbmN0aW9uAAAACf####8AAAABAA9DUmVzdWx0YXRWYWxldXIAAAAIAAAAB###########')
          enonceMain()
        },
        // failure
        (error) => {
          j3pShowError(error, { message: 'mathgraph n’est pas correctement chargé', mustNotify: true })
        })
      // plantage dans le code de success
      .catch(j3pShowError)
  }
  function yaniv (niv, tab) {
    for (let i = 0; i < tab.length; i++) {
      if (tab[i].niv === niv) return true
    }
    return false
  }
  function metDansExo (NumNiv, tabNiv) {
    const NivRech = tabNiv[NumNiv]
    for (let i = 0; i < stor.ExoParniv.length; i++) {
      if (stor.ExoParniv[i].niv === NivRech) {
        if (stor.ExoParniv[i].exos.length > 0) {
          rangeDansExo(stor.ExoParniv[i].exos.pop())
        }
        if (stor.ExoParniv[i].exos.length === 0) {
          stor.ExoParniv.splice(i, 1)
        }
      }
    }
  }
  function rangeDansExo (unex) {
    for (let i = 0; i < stor.lesExos.length; i++) {
      if (stor.lesExos[i].niv === unex.niv) {
        stor.lesExos.splice(i, 0, unex)
        return
      }
    }
    stor.lesExos.splice(0, 0, unex)
  }
  function metExoAniv (nivExo, exo, exoParNiv) {
    for (let i = 0; i < exoParNiv.length; i++) {
      if (exoParNiv[i].niv === nivExo) {
        exoParNiv[i].exos.push(j3pClone(exo))
        return
      }
    }
  }
  function boutonfen () {
    j3pToggleFenetres('Calculatrice')
  }
  function creeLesDiv () {
    stor.lesdiv = {}
    stor.lesdiv.conteneur = j3pAddElt(me.zonesElts.MG, 'div', {
      style: me.styles.etendre('toutpetit.enonce', { padding: '10px' })
    })
    stor.lesdiv.calculatrice = j3pAddElt(me.zonesElts.MD, 'div', {
      style: me.styles.etendre('toutpetit.enonce', { padding: '10px' })
    })
    const tab2 = addDefaultTable(stor.lesdiv.conteneur, 1, 1)[0][0]
    stor.lesdiv.enonceG = addDefaultTable(tab2, 1, 1)[0][0]
    stor.lesdiv.figure = addDefaultTable(tab2, 1, 1)[0][0]
    stor.lesdiv.image = addDefaultTable(tab2, 1, 1)[0][0]
    stor.lesdiv.video = addDefaultTable(tab2, 1, 1)[0][0]
    stor.lesdiv.son = addDefaultTable(tab2, 1, 1)[0][0]
    stor.lesdiv.enonce = addDefaultTable(tab2, 1, 1)[0][0]
    stor.lesdiv.enonceG.style.padding = '0px'
    stor.lesdiv.figure.style.padding = '0px'
    stor.lesdiv.image.style.padding = '0px'
    stor.lesdiv.enonceG.style.padding = '0px'
    stor.lesdiv.video.style.padding = '0px'
    stor.lesdiv.son.style.padding = '0px'
    stor.lesdiv.image.style.display = 'none'
    stor.lesdiv.video.style.display = 'none'
    stor.lesdiv.son.style.display = 'none'
    stor.lesdiv.travail = addDefaultTable(stor.lesdiv.conteneur, 1, 1)[0][0]
    stor.lesdiv.explications = j3pAddElt(stor.lesdiv.conteneur, 'div')
    stor.lesdiv.solution = j3pAddElt(stor.lesdiv.conteneur, 'div')
    stor.lesdiv.correction = j3pAddElt(me.zonesElts.MD, 'div', {
      style: me.styles.etendre('toutpetit.enonce', { padding: '10px' })
    })
    stor.lesdiv.explications.style.color = me.styles.cfaux
    stor.lesdiv.solution.style.color = me.styles.colorCorrection
    tab2.classList.add('enonce')
    stor.lesdiv.travail.classList.add('travail')
    me.zonesElts.MG.classList.add('fond')
  }

  function calculeImage (formx, formfonction) {
    const list = stor.listePourCalc
    list.giveFormula2('x', formx)
    list.giveFormula2('f', formfonction)
    list.calculateNG()
    return String(stor.listePourCalc.valueOf('y'))
  }

  function egaliteFormelle (formfonction1, formfonction2) {
    let ok = true
    const formFonctionText = formfonction1 + '-(' + formfonction2 + ')'
    for (let i = -15; i < 15; i++) {
      ok = ok && Math.abs(Number(calculeImage(String(i / 10), formFonctionText))) < Math.pow(10, -9)
    }
    return ok
  }

  function enonceMain () {
    me.videLesZones()
    creeLesDiv()
    if (ds.Calculatrice === 'Oui') {
      me.fenetresjq = [{ name: 'Calculatrice', title: 'Une calculatrice', left: 600, top: 10 }]
      j3pCreeFenetres(me)
      j3pAfficheCroixFenetres('Calculatrice')
      j3pAjouteBouton(stor.lesdiv.calculatrice, boutonfen, { className: 'MepBoutons', value: 'Calculatrice' })
    }
    stor.lexo = stor.lesExos.pop()
    j3pAffiche(stor.lesdiv.enonceG, null, stor.lexo.consigneG)
    stor.lesdiv.enonceG.style.whiteSpace = 'normal'
    j3pAffiche(stor.lesdiv.enonce, null, stor.lexo.consigne)
    stor.lesdiv.enonce.style.whiteSpace = 'normal'
    const aff = addDefaultTable(stor.lesdiv.travail, 1, 5)
    j3pAddContent(aff[0][1], '&nbsp;')
    j3pAddContent(aff[0][3], '&nbsp;')
    j3pAffiche(aff[0][0], null, stor.lexo.textAvant)
    if (stor.lexo.textApres) j3pAffiche(aff[0][4], null, stor.lexo.textApres)
    if (stor.lexo.yaFigure) {
      j3pCreeSVG(stor.lesdiv.figure, { id: 'fhdsjkqfhdksj', width: 37 + stor.lexo.width * 340, height: 54 + stor.lexo.height * 253 })
      j3pCreeSVG(stor.lesdiv.figure, { id: 'fhdsjkqfhdksj2', width: 0, height: 0 })
      stor.mtgAppLecteur.removeAllDoc()
      stor.mtgAppLecteur.addDoc('fhdsjkqfhdksj', stor.lexo.figureTxt, true)
      stor.mtgAppLecteur.addDoc('fhdsjkqfhdksj2', genMGEx, true)
      stor.mtgAppLecteur.calculateAndDisplayAll(true)
    } else {
      stor.lesdiv.figure.style.display = 'none'
      j3pCreeSVG(stor.lesdiv.figure, { id: 'fhdsjkqfhdksj2', width: 0, height: 0 })
      stor.mtgAppLecteur.removeAllDoc()
      stor.mtgAppLecteur.addDoc('fhdsjkqfhdksj2', genMGEx, true)
      stor.mtgAppLecteur.calculateAndDisplayAll(true)
    }
    if (stor.lexo.yaImage) {
      stor.lesdiv.image.style.display = ''
      const ui = addDefaultTable(stor.lesdiv.image, 1, 1)[0][0]
      const immm = j3pAddElt(ui, 'img', '', { src: stor.lexo.image, width: stor.lexo.widthImage, height: stor.lexo.heightImage })
      immm.style.width = stor.lexo.widthImage + 'px'
      immm.style.height = stor.lexo.heightImage + 'px'
    }
    if (stor.lexo.yaVideo) {
      stor.lesdiv.video.style.display = ''
      stor.lesdiv.video.style.border = '1px solid black'
      placeJouerVideo(stor.lesdiv.video, stor.lexo, 'video', 'widthVideo', stor.mtgAppLecteur)
    }
    if (stor.lexo.yaSon) {
      stor.lesdiv.son.style.display = ''
      const contui = addDefaultTable(stor.lesdiv.son, 1, 1)[0][0]
      const ui = j3pAddElt(contui, 'div')
      ui.style.border = '1px solid black'
      ui.style.borderRadius = '5px'
      ui.style.padding = '5px'
      ui.style.paddingTop = '0px'
      ui.style.background = '#aeef59'
      ui.style.textAlign = 'center'
      placeJouerSon(ui, stor.lexo, stor, 'son')
    }
    stor.zone = new ZoneStyleMathquill3(aff[0][2], { restric: stor.lexo.restric, enter: () => { me.sectionCourante() } })
    setTimeout(() => {
      stor.zone.focus()
    }, 100)
    me.finEnonce()
  }
  function affcofo (isFin) {
    let yaexplik = false
    switch (stor.lexo.typeEgal) {
      case 'stricte':
        for (let i = 0; i < stor.lexo.errPrev.length; i++) {
          if (stor.zone.reponse() === stor.lexo.errPrev[i].rep) {
            j3pAffiche(stor.lesdiv.explications, null, stor.lexo.errPrev[i].remede)
            yaexplik = true
          }
        }
        break
      case 'formelle': {
        const acomp = traiteMathQuill(stor.zone.reponse().replace(/²/g, '^{2}').replace(/×/g, '*'))
        for (let i = 0; i < stor.lexo.errPrev.length; i++) {
          const acomp2 = traiteMathQuill(stor.lexo.errPrev[i].rep.replace(/²/g, '^{2}').replace(/×/g, '*'))
          const laComp = egaliteFormelle(acomp, acomp2)
          if (laComp) {
            j3pAffiche(stor.lesdiv.explications, null, stor.lexo.errPrev[i].remede)
            yaexplik = true
          }
        }
        if (stor.errSimple && !yaexplik) {
          yaexplik = true
          j3pAffiche(stor.lesdiv.explications, null, 'Tu peux simplifier l’écriture de ce résultat !')
        }
        break
      }
    }
    if (yaexplik) stor.lesdiv.explications.classList.add('explique')
    if (isFin) {
      stor.zone.barre()
      stor.zone.disable()
      j3pAffiche(stor.lesdiv.solution, null, stor.lexo.correction)
      stor.lesdiv.solution.classList.add('correction')
    }
  }

  function isRepOk () {
    stor.lesdiv.explications.classList.remove('explique')
    j3pEmpty(stor.lesdiv.explications)
    stor.errSimple = false
    let ok = false
    let okSimple = false
    switch (stor.lexo.typeEgal) {
      case 'stricte': {
        for (let j = 0; j < stor.lexo.repPrev.length; j++) {
          if (stor.zone.reponse() === stor.lexo.repPrev[j]) {
            ok = true
          }
        }
        if (!ok) {
          stor.zone.corrige(false)
          return false
        }
        return true
      }
      case 'formelle': {
        const acomp = traiteMathQuill(stor.zone.reponse().replace(/²/g, '^{2}').replace(/×/g, '*'))
        for (let j = 0; j < stor.lexo.repPrev.length; j++) {
          const acomp2 = traiteMathQuill(stor.lexo.repPrev[j].replace(/²/g, '^{2}').replace(/×/g, '*'))
          const laComp = egaliteFormelle(acomp, acomp2)
          if (laComp) {
            if (stor.lexo.repPrev[j].length >= stor.zone.reponse().length) {
              ok = true
            } else {
              okSimple = true
            }
          }
        }
        if (!ok) {
          stor.zone.corrige(false)
          if (okSimple) stor.errSimple = true
          return false
        }
        return true
      }
    }
  }

  function yaReponse () {
    if (me.isElapsed) return true
    if (stor.zone.reponse() === '') {
      stor.zone.focus()
      return false
    }
    return true
  }

  switch (this.etat) {
    case 'enonce':
      // code exécuté au lancement de la section
      if (me.debutDeLaSection) {
        initSection()
      } else {
        enonceMain()
      }
      break // case "enonce":

    case 'correction':
      // On teste si une réponse a été saisie

      if (yaReponse()) {
        // Bonne réponse
        if (isRepOk()) {
          me._stopTimer()
          stor.lesdiv.correction.style.color = me.styles.cbien
          stor.lesdiv.correction.innerHTML = cBien

          stor.zone.corrige(true)
          stor.zone.disable()

          me.score = j3pArrondi(me.score + 1, 1)
          me.typederreurs[0]++
          me.etat = 'navigation'
          me.sectionCourante()
        } else {
          // Pas de bonne réponse
          stor.lesdiv.correction.style.color = me.styles.cfaux
          stor.lesdiv.correction.innerHTML = cFaux
          if (me.isElapsed) {
            // A cause de la limite de temps :
            me._stopTimer()

            stor.lesdiv.correction.innerHTML += '<br>' + tempsDepasse + '<BR>'
            me.typederreurs[10]++

            affcofo(true)

            me.etat = 'navigation'
            me.sectionCourante()
          } else {
            // Réponse fausse :
            if (me.essaiCourant < me.donneesSection.nbchances) {
              stor.lesdiv.correction.innerHTML += '<br>' + essaieEncore

              affcofo(false)

              me.typederreurs[1]++
            } else {
              // Erreur au nème essai
              me._stopTimer()
              me.cacheBoutonValider()

              affcofo(true)

              me.typederreurs[2]++
              me.etat = 'navigation'
              me.sectionCourante()
            }
          }
        }
      } else {
        // pas de réponse
        stor.lesdiv.correction.style.color = me.styles.cfaux
        stor.lesdiv.correction.innerHTML = reponseIncomplete
        me.afficheBoutonValider()
      }

      // Obligatoire
      me.finCorrection()
      break // case "correction":

    case 'navigation':

      if (me.sectionTerminee()) {
        // On détermine la phrase d’état renvoyée par la section
        me.afficheBoutonSectionSuivante()
        me.focus('sectioncontinuer')
      } else {
        me.etat = 'enonce'
        me.afficheBoutonSuite()
        me.focus('boutoncontinuer')
      }
      // Obligatoire
      me.finNavigation()
      break // case "navigation":
  }
}
