import { j3pAddContent, j3pAddElt, j3pAjouteBouton, j3pArrondi, j3pClone, j3pEmpty, j3pGetNewId, j3pShowError, j3pShuffle } from 'src/legacy/core/functions'
import BulleAide from 'src/legacy/outils/bulleAide/BulleAide'
import Etiquettes from 'src/legacy/outils/etiquette/Etiquettes'
import PlaceEtiquettes from 'src/legacy/outils/etiquette/PlaceEtiquettes'
import { addDefaultTable } from 'src/legacy/themes/table'
import { getMtgCore } from 'src/lib/outils/mathgraph'
import getDefaultProgramme from './defaultProgramme'
import { j3pAffiche } from 'src/lib/mathquill/functions'
import { j3pCreeSVG } from 'src/legacy/core/functionsSvg'
import './etikMake.css'
import { placeJouerSon, placeJouerSonEtiquettes, placeJouerVideo } from 'src/legacy/outils/zoneStyleMathquill/functions'
import textesGeneriques from 'src/lib/core/textes'

const { cBien, cFaux, essaieEncore, reponseIncomplete, tempsDepasse } = textesGeneriques

async function initEditor (container, values, btn) {
  const { default: editor } = await import('./editorProgramme.js')
  return editor(container, values, btn) // la fct qui prend un conteneur et les params comme arguments
}

/**
 * Ajouter une description ici
 * @fileOverview
 * @author Tommy Barroy <t.barroy@laposte.net>
 * @since 2017-10
 */
export const params = {
  outils: ['calculatrice', 'mathquill'],
  /**
   * Ce qui est paramétrable dans l’éditeur de graphe
   * @type {LegacySectionParamsParametres}
   */
  parametres: [
    ['nbrepetitions', 9, 'entier', 'Nombre de répétitions de la section'],
    ['limite', 0, 'entier', 'Temps disponible par répétition, en secondes'],
    ['nbchances', 2, 'entier', 'Nombre d’essais par répétition'],
    ['Liste', JSON.stringify(getDefaultProgramme()), 'multiEditor', 'Json figure de base et figure attendue.', initEditor],
    ['ordonne', 'Oui', 'liste', '<u>Oui</u>:  Exercices dans l’ordre', ['Oui', 'Non', 'Oui_par_Niveau', 'Aléa_par_niveau']],
    ['Calculatrice', 'Non', 'liste', '<u>Oui</u>:  Une calculatrice est disponible', ['Oui', 'Non']],
    ['theme', 'zonesAvecImageDeFond', 'liste', 'Modèle pour l’affichage', ['standard', 'zonesAvecImageDeFond']]
  ]
}

/**
 * section choix
 * @this {Parcours}
 */
export default function main () {
  const me = this
  const ds = me.donneesSection
  const stor = me.storage

  function initSection () {
    // chargement mtg
    me.construitStructurePage('presentation3')
    try {
      stor.Liste = JSON.parse(ds.Liste)
      const aComp = []
      //
      for (let prog = 0; prog < stor.Liste.liste.length; prog++) {
        let aCompte = true
        if (stor.Liste.liste[prog].yaImage) {
          if (stor.Liste.liste[prog].image !== 'string') {
            if (stor.Liste.liste[prog].image.type === 'clone') {
              aCompte = false
              stor.Liste.liste[prog].image = j3pClone(aComp[stor.Liste.liste[prog].image.num])
            }
          }
          if (aCompte) aComp.push(j3pClone(stor.Liste.liste[prog].image))
        }
        aCompte = true
        if (stor.Liste.liste[prog].yaSon) {
          if (stor.Liste.liste[prog].son !== 'string') {
            if (stor.Liste.liste[prog].son.type === 'clone') {
              aCompte = false
              stor.Liste.liste[prog].son = j3pClone(aComp[stor.Liste.liste[prog].son.num])
            }
          }
          if (aCompte) aComp.push(j3pClone(stor.Liste.liste[prog].son))
        }
        aCompte = true
        if (stor.Liste.liste[prog].yaFigure) {
          if (stor.Liste.liste[prog].figureTxt !== 'string') {
            if (stor.Liste.liste[prog].figureTxt.type === 'clone') {
              aCompte = false
              stor.Liste.liste[prog].figureTxt = j3pClone(aComp[stor.Liste.liste[prog].figureTxt.num])
            }
          }
          if (aCompte) aComp.push(j3pClone(stor.Liste.liste[prog].figureTxt))
        }
        for (let i = 0; i < stor.Liste.liste[prog].etiquettes.length; i++) {
          aCompte = true
          if (typeof stor.Liste.liste[prog].etiquettes[i].mod !== 'string') {
            if (stor.Liste.liste[prog].etiquettes[i].mod.type === 'clone') {
              aCompte = false
              stor.Liste.liste[prog].etiquettes[i].mod = j3pClone(aComp[stor.Liste.liste[prog].etiquettes[i].mod.num])
            }
          }
          if (aCompte) aComp.push(j3pClone(stor.Liste.liste[prog].etiquettes[i].mod))
          for (let j = 0; j < stor.Liste.liste[prog].etiquettes[i].contAs.length; j++) {
            aCompte = true
            if (typeof stor.Liste.liste[prog].etiquettes[i].contAs[j].cont !== 'string') {
              if (stor.Liste.liste[prog].etiquettes[i].contAs[j].cont.type === 'clone') {
                aCompte = false
                stor.Liste.liste[prog].etiquettes[i].contAs[j].cont = j3pClone(aComp[stor.Liste.liste[prog].etiquettes[i].contAs[j].cont.num])
              }
            }
            if (aCompte) aComp.push(j3pClone(stor.Liste.liste[prog].etiquettes[i].contAs[j].cont))
          }
        }
      }
    } catch (error) {
      console.error(error)
      j3pShowError('Parametres invalide')
      stor.Legendes = getDefaultProgramme()
    }
    let titre = stor.Liste.titre
    if (!titre || titre === '') titre = ' '
    me.afficheTitre(titre)
    stor.lesExos = []
    const chOrdonne = ds.ordonne
    stor.ExoParniv = []
    let nivDispos = []
    if (chOrdonne === 'Oui_par_Niveau' || chOrdonne === 'Aléa_par_niveau') {
      for (let i = 0; i < stor.Liste.liste.length; i++) {
        const nivExo = stor.Liste.liste[i].niv || 0
        if (yaniv(nivExo, stor.ExoParniv)) {
          metExoAniv(nivExo, stor.Liste.liste[i], stor.ExoParniv)
        } else {
          nivDispos.push(nivExo)
          stor.ExoParniv.push({ niv: nivExo, exos: [j3pClone(stor.Liste.liste[i])] })
        }
      }
      for (let i = 0; i < stor.ExoParniv.length; i++) {
        stor.ExoParniv[i].exos = j3pShuffle(stor.ExoParniv[i].exos)
      }
      stor.consExoParniv = j3pClone(stor.ExoParniv)
      if (chOrdonne === 'Oui_par_Niveau') {
        nivDispos = nivDispos.sort()
      } else {
        nivDispos = j3pShuffle(nivDispos)
      }
    }
    switch (chOrdonne) {
      case 'Non': {
        const exPos = j3pShuffle(stor.Liste.liste)
        for (let i = 0; i < ds.nbrepetitions; i++) {
          stor.lesExos.push(exPos[i % exPos.length])
        }
        break
      }
      case 'Oui':
        for (let i = 0; i < ds.nbrepetitions; i++) {
          stor.lesExos.splice(0, 0, stor.Liste.liste[i % stor.Liste.liste.length])
        }
        break
      default: {
        let cmpt = -1
        while (stor.lesExos.length < ds.nbrepetitions) {
          if (stor.ExoParniv.length === 0) stor.ExoParniv = j3pClone(stor.consExoParniv)
          cmpt++
          if (cmpt > nivDispos.length - 1) cmpt = 0
          metDansExo(cmpt, nivDispos)
        }
      }
    }
    getMtgCore({ withMathJax: true })
      .then(
        // success
        (mtgAppLecteur) => {
          stor.mtgAppLecteur = mtgAppLecteur
          enonceMain()
        },
        // failure
        (error) => {
          j3pShowError(error, { message: 'mathgraph n’est pas correctement chargé', mustNotify: true })
        })
      // plantage dans le code de success
      .catch(j3pShowError)
  }

  function creeLesDiv () {
    stor.lesdiv = {}
    stor.lesdiv.conteneur = j3pAddElt(me.zonesElts.MG, 'div', {
      style: me.styles.etendre('toutpetit.enonce', { padding: '10px' })
    })
    const tab2 = addDefaultTable(stor.lesdiv.conteneur, 1, 3)
    stor.lesdiv.enonceG = tab2[0][0]
    stor.lesdiv.correction = tab2[0][2]
    stor.lesdiv.calculatrice = tab2[0][1]
    stor.lesdiv.correction.style.padding = '10px'
    stor.lesdiv.calculatrice.style.padding = '10px'
    stor.tabEn = addDefaultTable(stor.lesdiv.conteneur, 1, 1)[0][0]
    stor.lesdiv.figure = addDefaultTable(stor.tabEn, 1, 1)[0][0]
    stor.lesdiv.image = addDefaultTable(stor.tabEn, 1, 1)[0][0]
    stor.lesdiv.video = addDefaultTable(stor.tabEn, 1, 1)[0][0]
    stor.lesdiv.son = addDefaultTable(stor.tabEn, 1, 1)[0][0]
    stor.lesdiv.figure.style.display = 'none'
    stor.lesdiv.video.style.display = 'none'
    stor.lesdiv.image.style.display = 'none'
    stor.lesdiv.son.style.display = 'none'
    stor.lesdiv.travail = j3pAddElt(stor.lesdiv.conteneur, 'div')
    stor.aMontre = j3pAddElt(stor.lesdiv.conteneur, 'div')
    j3pAddContent(stor.aMontre, '<br><b>Regarde la correction</b>:<br><br>')
    stor.aMontre.style.display = 'none'
    stor.lesdiv.solution = j3pAddElt(stor.lesdiv.conteneur, 'div')
    stor.lesdiv.solution.style.color = me.styles.colorCorrection
    stor.lesdiv.enonceG.classList.add('enonce')
    me.zonesElts.MG.classList.add('fond')
  }
  function yaniv (niv, tab) {
    for (let i = 0; i < tab.length; i++) {
      if (tab[i].niv === niv) return true
    }
    return false
  }
  function metDansExo (NumNiv, tabNiv) {
    const NivRech = tabNiv[NumNiv]
    for (let i = 0; i < stor.ExoParniv.length; i++) {
      if (stor.ExoParniv[i].niv === NivRech) {
        if (stor.ExoParniv[i].exos.length > 0) {
          rangeDansExo(stor.ExoParniv[i].exos.pop())
        }
        if (stor.ExoParniv[i].exos.length === 0) {
          stor.ExoParniv.splice(i, 1)
        }
      }
    }
  }
  function rangeDansExo (unex) {
    for (let i = 0; i < stor.lesExos.length; i++) {
      if (stor.lesExos[i].niv === unex.niv) {
        stor.lesExos.splice(i, 0, unex)
        return
      }
    }
    stor.lesExos.splice(0, 0, unex)
  }
  function metExoAniv (nivExo, exo, exoParNiv) {
    for (let i = 0; i < exoParNiv.length; i++) {
      if (exoParNiv[i].niv === nivExo) {
        exoParNiv[i].exos.push(j3pClone(exo))
        return
      }
    }
  }
  function vireAide () {
    if (stor.bulleAide) {
      for (let i = 0; i < stor.bulleAide; i++) {
        stor.bulleAide[i].destroy()
      }
    }
    stor.bulleAide = []
  }
  function enonceMain () {
    vireAide()
    me.videLesZones()
    creeLesDiv()
    stor.lexo = stor.lesExos.pop()
    stor.mtgAppLecteur.removeAllDoc()
    const eeen = stor.lesdiv.enonceG
    j3pAffiche(eeen, null, stor.lexo.consigne)
    j3pAddContent(eeen, '\n')
    if (stor.lexo.yaFigure) {
      stor.lesdiv.figure.style.display = ''
      stor.tabEn.classList.add('enonce')
      const ui = addDefaultTable(stor.lesdiv.figure, 1, 1)[0][0]
      const id = j3pGetNewId('mtgyh')
      j3pCreeSVG(ui, { id, width: 37 + stor.lexo.width * 340, height: 54 + stor.lexo.height * 253 })
      stor.mtgAppLecteur.addDoc(id, stor.lexo.figureTxt, true)
    }
    if (stor.lexo.yaImage) {
      stor.lesdiv.image.style.display = ''
      stor.tabEn.classList.add('enonce')
      const ui = addDefaultTable(stor.lesdiv.image, 1, 1)[0][0]
      const immm = j3pAddElt(ui, 'img', '', { src: stor.lexo.image, width: stor.lexo.widthImage, height: stor.lexo.heightImage })
      immm.style.width = stor.lexo.widthImage + 'px'
      immm.style.height = stor.lexo.heightImage + 'px'
    }
    if (stor.lexo.yaSon) {
      stor.lesdiv.son.style.display = ''
      stor.tabEn.classList.add('enonce')
      const contui = addDefaultTable(stor.lesdiv.son, 1, 1)[0][0]
      const ui = j3pAddElt(contui, 'div')
      ui.style.border = '1px solid black'
      ui.style.borderRadius = '5px'
      ui.style.padding = '5px'
      ui.style.paddingTop = '0px'
      ui.style.background = '#aeef59'
      ui.style.textAlign = 'center'
      placeJouerSon(ui, stor.lexo, stor, 'son')
    }
    if (stor.lexo.yaVideo) {
      stor.tabEn.classList.add('enonce')
      // const dou = j3pAddElt(stor.lesdiv.video, 'div')
      stor.lesdiv.video.style.padding = '0px'
      stor.lesdiv.video.style.display = ''
      // dou.style.border = '1px solid black'
      placeJouerVideo(stor.lesdiv.video, stor.lexo, 'video', 'widthVideo', stor.mtgAppLecteur)
    }
    const tttn = stor.lesdiv.travail

    const tabTravail = addDefaultTable(tttn, 1, 2)
    tabTravail[0][0].style.verticalAlign = 'top'
    tabTravail[0][0].classList.add('pourFitContent')
    afficheMesEtiquettes(tabTravail[0][1], stor.lexo.etiquettes)
    afficheMonTexte(tabTravail[0][0], stor.lexo.consigneG)
    stor.aEff = tabTravail[0][1]

    stor.mtgAppLecteur.calculateAndDisplayAll(true)
    me.finEnonce()
  }
  function afficheMonTexte (dou, koi) {
    // faudra que je gère les gras italic au dessus de ma place
    const ou = j3pAddElt(dou, 'div')
    ou.classList.add('travail')
    ou.style.marginRight = '10px'
    const aaf = koi.split('▢')
    stor.listPlaceEtik = []
    for (let i = 0; i < aaf.length - 1; i++) {
      const apush = {}
      const spanT = j3pAddElt(ou, 'span')
      spanT.classList.add('pourSpaces')
      j3pAffiche(spanT, null, aaf[i])
      const spanEtik = j3pAddElt(ou, 'span')
      apush.place = new PlaceEtiquettes(spanEtik, 'etik' + i, stor.testEtik[i].etik, {})
      stor.listPlaceEtik.push(apush)
    }
    const spanT = j3pAddElt(ou, 'span')
    spanT.classList.add('pourSpaces')
    j3pAffiche(spanT, null, aaf[aaf.length - 1])
  }
  function affCont (div2, buf2) {
    if (typeof buf2 === 'string') {
      j3pAffiche(div2, null, buf2)
    } else {
      const el = buf2
      switch (el.type) {
        case undefined:
        case 'mathgraph': {
          div2.style.verticalAlign = 'middle'
          const ui = addDefaultTable(div2, 1, 1)[0][0]
          const newId = j3pGetNewId('mtgetih')
          const svg = j3pCreeSVG(ui, { id: newId, width: 37 + el.content.width * 340, height: 54 + el.content.height * 253 })
          stor.mtgAppLecteur.addDoc(newId, el.content.txtFigure, true)
          svg.style.position = 'relative'
          stor.mtgAppLecteur.display(newId)
        }
          break
        case 'image': {
          const limm = j3pAddElt(div2, 'img', '', { src: el.content.txtFigure, width: el.content.width, height: el.content.height })
          limm.style.width = el.content.width + 'px'
          limm.style.height = el.content.height + 'px'
          limm.style.marginTop = '2px'
          limm.style.marginBottom = '2px'
        }
          break
        case 'son': {
          const ui = j3pAddElt(div2, 'div')
          ui.style.textAlign = 'center'
          placeJouerSon(ui, el.content, stor, 'txtFigure')
        }
      }
    }
  }
  function faisId (s) {
    if (typeof s === 'string') return s
    return s.content.txtFigure
  }
  function afficheMesEtiquettes (ou, ki) {
    const tabDchoices = addDefaultTable(ou, 3, 1)
    stor.testEtik = []
    stor.etikAt = []
    stor.spanListBut = []
    ou.style.border = '1px solid black'
    ou.style.background = '#aaa'
    tabDchoices[1][0].style.textAlign = 'center'
    tabDchoices[0][0].style.textAlign = 'center'
    tabDchoices[0][0].style.padding = '2px'
    tabDchoices[1][0].style.padding = '2px'
    tabDchoices[2][0].style.padding = '2px'
    j3pAddContent(tabDchoices[0][0], 'Etiquettes')
    j3pAddContent(tabDchoices[1][0], '&nbsp;')
    for (let i = 0; i < ki.length; i++) {
      const etijApuh = {
        div: j3pAddElt(tabDchoices[2][0], 'div')
      }
      let listEtik = ki[i].contAs.map(elt => elt.cont)
      listEtik.push(ki[i].mod)
      listEtik = j3pShuffle(listEtik)
      stor.etikAt.push(faisId(ki[i].mod))
      etijApuh.etik = new Etiquettes(etijApuh.div, 'etik' + i, listEtik, {
        dispo: 'colonne',
        colonne: 1,
        reutilisable: true,
        mtgAppLecteur: stor.mtgAppLecteur,
        codePlace: (ki.length > 1) ? (i + 1) : undefined,
        direct: true
      })
      for (let j = 0; j < etijApuh.etik.divARet.length; j++) {
        const el = listEtik[j]
        if (typeof el !== 'string') {
          switch (el.type) {
            case undefined:
            case 'mathgraph': {
              const ui = addDefaultTable(etijApuh.etik.divARet[j].princ, 1, 1)[0][0]
              const newId = j3pGetNewId('mtgEti')
              const svg = j3pCreeSVG(ui, { id: newId, width: 37 + el.content.width * 340, height: 54 + el.content.height * 253 })
              stor.mtgAppLecteur.addDoc(newId, el.content.txtFigure, true)
              svg.style.position = 'relative'
            }
              break
            case 'image': {
              const limm = j3pAddElt(etijApuh.etik.divARet[j].princ, 'img', '', { src: el.content.txtFigure, width: el.content.width, height: el.content.height })
              limm.style.width = el.content.width + 'px'
              limm.style.height = el.content.height + 'px'
              limm.style.marginTop = '2px'
              limm.style.marginBottom = '2px'
            }
              break
            case 'son':
              etijApuh.etik.divARet[j].princ.classList.add('pourFitContent')
              placeJouerSonEtiquettes(etijApuh.etik.divARet[j].princ, el.content, stor, 'txtFigure', j)
          }
        }
      }
      stor.testEtik.push(etijApuh)
      const spanBut = j3pAddElt(tabDchoices[1][0], 'span')
      spanBut.style.cursor = 'pointer'
      spanBut.style.width = '20px'
      spanBut.style.height = '20px'
      spanBut.style.border = '1px solid black'
      spanBut.style.borderRadius = '7px'
      j3pAddContent(spanBut, '&nbsp;' + (i + 1) + '&nbsp;')
      spanBut.addEventListener('click', () => {
        stor.testEtik[i].div.classList.remove('cacheTruc')
        stor.testEtik[i].div.classList.add('montreTruc')
        spanBut.style.background = '#b97676'
        for (let k = 0; k < ki.length; k++) {
          if (k === i) continue
          stor.testEtik[k].div.classList.add('cacheTruc')
          stor.testEtik[k].div.classList.remove('montreTruc')
          stor.spanListBut[k].style.background = ''
        }
      })
      stor.spanListBut.push(spanBut)
      if (i !== 0) {
        etijApuh.div.classList.add('cacheTruc')
      } else {
        spanBut.style.background = '#b97676'
        etijApuh.div.classList.add('montreTruc')
      }
      j3pAddContent(tabDchoices[1][0], '&nbsp;')
    }
    if (ki.length === 1) tabDchoices[1][0].style.display = 'none'
  }
  function affcofo (isFin) {
    if (isFin) {
      for (let i = stor.listPlaceEtik.length - 1; i > -1; i--) {
        stor.listPlaceEtik[i].place.corrige(stor.ckOK[i])
        if (!stor.ckOK[i]) {
          stor.listPlaceEtik[i].place.barre()
          stor.listPlaceEtik[i].divCo = j3pAddElt(stor.listPlaceEtik[i].place.iddivconteneur, 'span')
          stor.listPlaceEtik[i].divCo.style.display = 'inline-block'
          affCont(stor.listPlaceEtik[i].divCo, stor.lexo.etiquettes[i].mod)
          stor.listPlaceEtik[i].divCo.style.border = '1px solid black'
          stor.listPlaceEtik[i].divCo.style.borderRadius = '7px'
          stor.listPlaceEtik[i].divCo.style.padding = '2px'
          stor.listPlaceEtik[i].divCo.style.background = '#b4b4b4'
          stor.listPlaceEtik[i].divCo.style.color = me.styles.colorCorrection
          stor.listPlaceEtik[i].divCo.style.margin = '2px'
          stor.listPlaceEtik[i].divCo.style.display = 'none'
        }
      }
      for (let i = stor.listPlaceEtik.length - 1; i > -1; i--) {
        stor.listPlaceEtik[i].place.disable()
      }
      for (let i = 0; i < stor.testEtik.length; i++) {
        stor.testEtik[i].etik.detruit()
      }
      j3pEmpty(stor.aEff)
      stor.leBouCo = j3pAjouteBouton(stor.aEff, voirCo, { value: 'Voir la correction' })
      stor.coViz = false
      stor.aEff.style.border = '0px'
      stor.aEff.style.background = ''
      stor.aEff.style.verticalAlign = 'top'
      stor.aEff.style.padding = '5px'
    }
  }
  function voirCo () {
    stor.coViz = !stor.coViz
    stor.leBouCo.value = (stor.coViz) ? 'Voir mon travail' : 'Voir la correction'
    for (let i = 0; i < stor.listPlaceEtik.length; i++) {
      if (!stor.ckOK[i]) {
        stor.listPlaceEtik[i].place.hideShow(stor.coViz)
        stor.listPlaceEtik[i].divCo.style.display = (stor.coViz) ? 'inline-block' : 'none'
      }
    }
  }
  function isRepOk () {
    vireAide()
    stor.ckOK = []
    let ok = true
    for (let i = 0; i < stor.listPlaceEtik.length; i++) {
      j3pEmpty(stor.listPlaceEtik[i].place.placeAide)
      const idRep = faisId(stor.listPlaceEtik[i].place.contenu)
      const idAt = stor.etikAt[i]
      stor.ckOK[i] = idRep === idAt
      if (idRep !== idAt) {
        ok = false
        const apu = chercheAvert(i, idRep)
        if (apu !== '') {
          stor.bulleAide.push(new BulleAide(stor.listPlaceEtik[i].place.placeAide, apu, {}))
        }
      }
    }
    return ok
  }
  function metToutVert () {
    for (let i = stor.listPlaceEtik.length - 1; i > -1; i--) {
      stor.listPlaceEtik[i].place.corrige(true)
    }
  }
  function disaTout () {
    for (let i = stor.listPlaceEtik.length - 1; i > -1; i--) {
      stor.listPlaceEtik[i].place.disable()
    }
    for (let i = 0; i < stor.testEtik.length; i++) {
      stor.testEtik[i].etik.detruit()
    }
    stor.aEff.style.display = 'none'
  }
  function chercheAvert (nb, idRep) {
    for (let j = 0; j < stor.lexo.etiquettes[nb].contAs.length; j++) {
      const idComp = faisId(stor.lexo.etiquettes[nb].contAs[j].cont)
      if (idComp === idRep) return stor.lexo.etiquettes[nb].contAs[j].avert
    }
  }
  function yaReponse () {
    if (me.isElapsed) return true
    for (let i = 0; i < stor.listPlaceEtik.length; i++) {
      if (!stor.listPlaceEtik[i].place.contenu) return false
    }
    return true
  }

  switch (this.etat) {
    case 'enonce':
      // code exécuté au lancement de la section
      if (me.debutDeLaSection) {
        initSection()
      } else {
        enonceMain()
      }
      break // case "enonce":

    case 'correction':
      // On teste si une réponse a été saisie

      if (!yaReponse()) {
        // pas de réponse
        stor.lesdiv.correction.style.color = me.styles.cfaux
        stor.lesdiv.correction.innerHTML = reponseIncomplete
        return this.finCorrection()
      }

      // Bonne réponse
      if (isRepOk()) {
        stor.lesdiv.correction.style.color = me.styles.cbien
        stor.lesdiv.correction.innerHTML = cBien

        metToutVert()
        disaTout()

        me.score = j3pArrondi(me.score + 1, 1)
        me.typederreurs[0]++
        return this.finCorrection('navigation', true)
      }

      // Pas de bonne réponse
      stor.lesdiv.correction.style.color = me.styles.cfaux
      stor.lesdiv.correction.innerHTML = cFaux
      if (me.isElapsed) {
        // A cause de la limite de temps :
        stor.lesdiv.correction.innerHTML += '<br>' + tempsDepasse + '<BR>'
        me.typederreurs[10]++

        affcofo(true)
        return this.finCorrection('navigation', true)
      }

      // Réponse fausse :
      if (me.essaiCourant < me.donneesSection.nbchances) {
        stor.lesdiv.correction.innerHTML += '<br>' + essaieEncore
        affcofo(false)
        me.typederreurs[1]++
        return this.finCorrection()
      }

      // Erreur au dernier essai
      affcofo(true)
      me.typederreurs[2]++
      return this.finCorrection('navigation', true)
      // case "correction":

    case 'navigation':
      if (!me.sectionTerminee()) {
        me.etat = 'enonce'
        me.focus('boutoncontinuer')
      }
      me.finNavigation()
      break // case "navigation":
  }
}
