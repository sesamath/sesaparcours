export default function getDefaultProgramme () {
  return {
    titre: 'Un carré',
    liste: [
      {
        consigneG: 'Un carré est un ▢ avec\n- ▢\net\n- ▢',
        consigne: 'Complète le texte suivant\n',
        etiquettes: [{ mod: 'quadrilatère', contAs: [{ avert: 'Un carré a quatre côtés !', cont: 'triangle' }, { avert: 'Un carré a quatre côtés !', cont: 'cercle' }, { avert: 'Un carré a quatre côtés !', cont: 'hexagone' }] }, { mod: 'quatre angles droits', contAs: [{ avert: 'Ce n’est  pas assez !', cont: 'trois angles droits' }, { avert: 'Ce n’est  pas assez !', cont: 'deux angles droits' }, { avert: 'Ce n’est  pas assez !', cont: 'un angle droit' }] }, { mod: 'quatre côtés de même longeur', contAs: [{ avert: 'Ce n’est  pas assez !', cont: 'deux côtés de même longueur' }, { avert: 'C’est trop', cont: 'cinq côtés de même longueur' }] }],
        yaFigure: false,
        nom: 'exercice carré',
        yaImage: false,
        yaSon: false,
        niv: 0
      }]
  }
}
