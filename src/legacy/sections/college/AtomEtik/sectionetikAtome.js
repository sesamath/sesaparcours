import { j3pAddContent, j3pAddElt, j3pAjouteBouton, j3pArrondi, j3pClone, j3pEmpty, j3pGetNewId, j3pShowError, j3pShuffle } from 'src/legacy/core/functions'
import BulleAide from 'src/legacy/outils/bulleAide/BulleAide'
import Etiquettes from 'src/legacy/outils/etiquette/Etiquettes'
import PlaceEtiquettes from 'src/legacy/outils/etiquette/PlaceEtiquettes'
import { addDefaultTable } from 'src/legacy/themes/table'
import { getMtgCore } from 'src/lib/outils/mathgraph'
import getDefaultProgramme from './defaultProgramme'
import { j3pAffiche } from 'src/lib/mathquill/functions'
import { j3pCreeSVG } from 'src/legacy/core/functionsSvg'
import { placeJouerSon, placeJouerSonEtiquettes } from 'src/legacy/outils/zoneStyleMathquill/functions'
import './resMake.css'
import textesGeneriques from 'src/lib/core/textes'
const { cBien, cFaux, essaieEncore, tempsDepasse } = textesGeneriques
async function initeditor (container, values, btn) {
  const { default: stor } = await import('./editorProgramme.js')
  return stor(container, values, btn) // la fct qui prend un conteneur et les params comme arguments
}

/**
 * Ajouter une description ici
 * @fileOverview
 * @author Tommy Barroy <t.barroy@laposte.net>
 * @since 2017-10
 */
export const params = {
  outils: ['calculatrice', 'mathquill'],
  /**
   * Ce qui est paramétrable dans l’éditeur de graphe
   * @type {LegacySectionParamsParametres}
   */
  parametres: [
    ['nbrepetitions', 1, 'entier', 'Nombre de répétitions de la section'],
    ['nbchances', 2, 'entier', 'Nombre d’essais par répétition'],
    ['limite', 0, 'entier', 'Temps disponible par répétition, en secondes'],
    ['Liste', JSON.stringify(getDefaultProgramme()), 'multiEditor', 'Json figure de base et figure attendue.', initeditor],
    ['ordonne', 'Oui', 'liste', '<u>Oui</u>:  Exercices dans l’ordre', ['Oui', 'Non']],
    ['theme', 'zonesAvecImageDeFond', 'liste', 'Modèle pour l’affichage', ['standard', 'zonesAvecImageDeFond']]
  ]
}

/**
 * section choix
 * @this {Parcours}
 */
export default function main () {
  const me = this
  const ds = me.donneesSection
  const stor = me.storage

  function initSection () {
    // chargement mtg
    me.construitStructurePage('presentation3')
    try {
      stor.Liste = JSON.parse(ds.Liste)
      const aComp = []
      //
      for (let prog = 0; prog < stor.Liste.liste.length; prog++) {
        let aCompte = true
        if (stor.Liste.liste[prog].image !== 'string') {
          if (stor.Liste.liste[prog].image.type === 'clone') {
            aCompte = false
            stor.Liste.liste[prog].image = j3pClone(aComp[stor.Liste.liste[prog].image.num])
          }
        }
        if (aCompte) aComp.push(j3pClone(stor.Liste.liste[prog].image))
        aCompte = true
        if (stor.Liste.liste[prog].yaSon) {
          if (stor.Liste.liste[prog].son !== 'string') {
            if (stor.Liste.liste[prog].son.type === 'clone') {
              aCompte = false
              stor.Liste.liste[prog].son = j3pClone(aComp[stor.Liste.liste[prog].son.num])
            }
          }
          if (aCompte) aComp.push(j3pClone(stor.Liste.liste[prog].son))
        }
        aCompte = true
        if (stor.Liste.liste[prog].txtFigure !== 'string') {
          if (stor.Liste.liste[prog].txtFigure.type === 'clone') {
            aCompte = false
            stor.Liste.liste[prog].txtFigure = j3pClone(aComp[stor.Liste.liste[prog].txtFigure.num])
          }
        }
        if (aCompte) aComp.push(j3pClone(stor.Liste.liste[prog].txtFigure))
      }
    } catch (error) {
      console.error(error)
      j3pShowError('Parametres invalide')
      stor.Legendes = getDefaultProgramme()
    }
    let titre = stor.Liste.titre
    if (!titre || titre === '') titre = ' '
    me.afficheTitre(titre)
    stor.lesExos = []
    if (ds.ordonne === 'Non') {
      const exPos = j3pShuffle(stor.Liste.liste)
      for (let i = 0; i < ds.nbrepetitions; i++) {
        stor.lesExos.push(exPos[i % exPos.length])
      }
    } else {
      for (let i = 0; i < ds.nbrepetitions; i++) {
        stor.lesExos.splice(0, 0, stor.Liste.liste[i % stor.Liste.liste.length])
      }
    }
    getMtgCore({ withMathJax: true, withApiPromise: true })
      .then(
        // success
        (mtgAppLecteur) => {
          stor.mtgAppLecteur = mtgAppLecteur
          enonceMain()
        },
        // failure
        (error) => {
          j3pShowError(error, { message: 'mathgraph n’est pas correctement chargé', mustNotify: true })
        })
      // plantage dans le code de success
      .catch(j3pShowError)
  }
  function creeLesDiv () {
    stor.lesdiv = {}
    stor.lesdiv.conteneur = j3pAddElt(me.zonesElts.MG, 'div', {
      style: me.styles.etendre('toutpetit.enonce', { padding: '10px' })
    })

    stor.concot = addDefaultTable(stor.lesdiv.conteneur, 1, 1)[0][0]
    const tab2 = addDefaultTable(stor.concot, 1, 4)
    stor.lesdiv.enonceG = addDefaultTable(tab2[0][0], 1, 1)[0][0]
    stor.lesdiv.son = addDefaultTable(tab2[0][0], 1, 1)[0][0]
    stor.lesdiv.son.style.display = 'none'
    const tatab = addDefaultTable(stor.lesdiv.conteneur, 1, 2)
    stor.lesdiv.travail = j3pAddElt(tatab[0][0], 'div')
    stor.lesdiv.etikDiv = j3pAddElt(tatab[0][1], 'div')
    stor.lesdiv.etikDiv.style.padding = '10px'
    tatab[0][1].style.verticalAlign = 'top'
    stor.lesdiv.travail.style.overflow = 'hidden'
    stor.lesdiv.travail.style.border = '3px black solid'
    stor.lesdiv.correction = j3pAddElt(tatab[0][1], 'div', {
      style: me.styles.etendre('toutpetit.enonce', { padding: '10px' })
    })
    stor.lesdiv.zoneBout1 = tab2[0][1]
    stor.lesdiv.zoneBout2 = tab2[0][2]
    stor.lesdiv.zoneBout3 = tab2[0][3]
    stor.lesdiv.zoneBout1.style.paddingLeft = '30px'
    stor.lesdiv.zoneBout2.style.paddingLeft = '30px'
    stor.lesdiv.zoneBout3.style.paddingLeft = '30px'
    stor.lesdiv.zoneBout2.style.display = 'none'
    stor.lesdiv.zoneBout3.style.display = 'none'
    tab2[0][0].classList.add('enonce')
    stor.lesdiv.travail.classList.add('travail')
    me.zonesElts.MG.classList.add('fond')
    stor.tab2 = tab2
  }
  function fauxOK () {
    me.sectionCourante()
  }
  function agrandiMax () {
    stor.isAgrand = true
    stor.lesdiv.zoneBout1.style.display = 'none'
    stor.lesdiv.zoneBout2.style.display = ''
    stor.lesdiv.zoneBout3.style.display = ''
    me.zonesElts.HG.style.display = 'none'
    me.zonesElts.HD.style.display = 'none'
    const taillEnonce = stor.concot.getBoundingClientRect()
    stor.tailllimm = j3pClone(stor.limmm3.getBoundingClientRect())
    stor.tailltravail = j3pClone(stor.lesdiv.travail.getBoundingClientRect())
    stor.tailleMG = j3pClone(me.zonesElts.MG.getBoundingClientRect())
    const tailleEttik = j3pClone(stor.lesdiv.etikDiv.getBoundingClientRect())
    const multMaxX = (stor.tailleMG.width - tailleEttik.width) / stor.tailltravail.width
    const multMaxy = (stor.tailleMG.height + 100 - taillEnonce.height) / stor.tailltravail.height
    const aMul = (Math.min(multMaxX, multMaxy) - 0.05)
    stor.lesdiv.travail.style.width = stor.tailltravail.width * aMul + 'px'
    stor.limmm3.style.width = stor.tailllimm.width * aMul + 'px'
    stor.lesdiv.travail.style.height = stor.tailltravail.height * aMul + 'px'
    stor.limmm3.style.height = stor.tailllimm.height * aMul + 'px'
    me.zonesElts.MG.style.height = (stor.tailleMG.height + 120) + 'px'
    stor.aMul = aMul
    gereMove()
    me.cacheBoutonValider()
  }
  function agrandiMin () {
    stor.isAgrand = false
    stor.lesdiv.zoneBout1.style.display = ''
    stor.lesdiv.zoneBout2.style.display = 'none'
    stor.lesdiv.zoneBout3.style.display = 'none'
    me.zonesElts.HG.style.display = ''
    me.zonesElts.HD.style.display = ''
    stor.lesdiv.travail.style.width = stor.tailltravail.width + 'px'
    stor.limmm3.style.width = stor.tailllimm.width + 'px'
    stor.lesdiv.travail.style.height = stor.tailltravail.height + 'px'
    stor.limmm3.style.height = stor.tailllimm.height + 'px'
    stor.lesdiv.travail.style.background = '#f00'
    me.zonesElts.MG.style.height = stor.tailleMG.height + 'px'
    stor.aMul = 1
    gereMove()
    if (me.etat === 'correction') me.afficheBoutonValider()
  }
  function faisId (s) {
    if (typeof s === 'string') return s
    return s.content.txtFigure
  }
  function enonceMain () {
    stor.aMul = 1
    me.videLesZones()
    creeLesDiv()
    stor.lexo = stor.lesExos.pop()
    if (stor.lexo.consigneG !== '') {
      j3pAffiche(stor.lesdiv.enonceG, null, stor.lexo.consigneG)
      stor.lesdiv.enonceG.style.whiteSpace = 'normal'
    } else {
      stor.lesdiv.enonceG.style.display = 'none'
    }
    if (stor.lexo.yaSon) {
      stor.lesdiv.son.style.display = ''
      const contui = addDefaultTable(stor.lesdiv.son, 1, 1)[0][0]
      const ui = j3pAddElt(contui, 'div')
      ui.style.border = '1px solid black'
      ui.style.borderRadius = '5px'
      ui.style.padding = '5px'
      ui.style.paddingTop = '0px'
      ui.style.background = '#aeef59'
      ui.style.textAlign = 'center'
      placeJouerSon(ui, stor.lexo, stor, 'son')
    }
    if (stor.lexo.consigneG === '' && !stor.lexo.yaSon) stor.tab2[0][0].style.display = 'none'
    stor.idSvg2 = j3pGetNewId()
    stor.limmm3 = j3pAddElt(stor.lesdiv.travail, 'img', '', { src: stor.lexo.image })
    stor.limmm3.style.position = 'relative'
    stor.limmm3.style.top = '0px'
    stor.limmm3.style.left = '0px'
    stor.lesdiv.travail.style.minWidth = (287 + stor.lexo.width * 355) + 'px'
    stor.leMG2 = j3pCreeSVG(stor.lesdiv.travail, { id: stor.idSvg2, width: (287 + stor.lexo.width * 355), height: (149 + stor.lexo.height * 280) })
    // stor.leMG2.style.border = '1px solid black'
    stor.mtgAppLecteur.removeAllDoc()
    stor.mtgAppLecteur.addDoc(stor.idSvg2, stor.lexo.txtFigure, false)
    stor.mtgAppLecteur.setApiDoc(stor.idSvg2)
    stor.mtgAppLecteur.calculate(stor.idSvg2)
    stor.etiquettes = []
    stor.mesZone = []
    stor.boonesRep = []
    stor.mtgAppLecteur.display(stor.idSvg2).then(() => {
      stor.mesZone = []
      stor.leMG2.childNodes[1].setAttribute('fill', 'rgb(0,0,0,0)')
      stor.leMG2.style.position = 'relative'
      stor.leMG2.style.top = -(149 + stor.lexo.height * 280) + 'px'
      stor.leMG2.style.left = '0px'
      stor.limmm3.style.width = (287 + stor.lexo.width * 355) + 'px'
      stor.limmm3.style.height = (149 + stor.lexo.height * 280) + 'px'
      stor.lesdiv.travail.style.height = (149 + stor.lexo.height * 280 + 1) + 'px'
      stor.lesdiv.travail.style.width = (287 + stor.lexo.width * 355) + 'px'
      let tailleavrire = (149 + stor.lexo.height * 280) * 2
      stor.tailleaVireCons = tailleavrire
      stor.listMondiv = []
      for (let i = 0; i < stor.lexo.listZone.length; i++) {
        if (stor.lexo.listZone[i].type === 'rectangle') {
          for (let k = 0; k < stor.lexo.listZone[i].surface.length; k++) {
            stor.mtgAppLecteur.setHidden({ elt: stor.lexo.listZone[i].surface[k] })
          }
          const p1 = stor.mtgAppLecteur.getPointPosition({ a: stor.lexo.listZone[i].p3 })
          stor.etiquettes.push(stor.lexo.listZone[i].etik)
          stor.mesZone.push({ p1, height: stor.lexo.listZone[i].lHei })
          stor.boonesRep.push(faisId(stor.lexo.listZone[i].etik.mod))
        }
        if (stor.lexo.listZone[i].type === 'label') {
          const mondiv = j3pAddElt(stor.lesdiv.travail, 'div')
          mondiv.classList.add('pourFitContent')
          mondiv.style.position = 'relative'
          mondiv.style.verticalAlign = 'middle'
          mondiv.style.textAlign = 'center'
          mondiv.style.fontFamily = 'Bitstream Véracité Sans'
          mondiv.style.pointerEvents = 'none'
          mondiv.style.fontSize = stor.lexo.listZone[i].taille + 'px'
          j3pAffiche(mondiv, null, stor.lexo.listZone[i].texte)
          const p1 = stor.mtgAppLecteur.getPointPosition({ a: stor.lexo.listZone[i].p1 })
          mondiv.style.top = (p1.y - tailleavrire) + 'px'
          mondiv.style.left = p1.x + 'px'
          const aaj = mondiv.getBoundingClientRect()
          tailleavrire += aaj.height
          stor.lexo.listZone[i].mondiv = mondiv
          stor.listMondiv.push(mondiv)
          mondiv.y = p1.y
          mondiv.x = p1.x
          mondiv.larg = aaj.width
          mondiv.height = aaj.height
        }
        if (stor.lexo.listZone[i].type === 'cache') {
          for (let k = 0; k < stor.lexo.listZone[i].surface.length; k++) {
            stor.mtgAppLecteur.setHidden({ elt: stor.lexo.listZone[i].surface[k] })
          }
          const p1 = stor.mtgAppLecteur.getPointPosition({ a: stor.lexo.listZone[i].p1 })
          const p2 = stor.mtgAppLecteur.getPointPosition({ a: stor.lexo.listZone[i].p2 })
          const p3 = stor.mtgAppLecteur.getPointPosition({ a: stor.lexo.listZone[i].p3 })
          const p4 = stor.mtgAppLecteur.getPointPosition({ a: stor.lexo.listZone[i].p4 })
          const x = Math.min(p1.x, p2.x, p3.x, p4.x)
          const y = Math.min(p1.y, p2.y, p3.y, p4.y)
          const x2 = Math.max(p1.x, p2.x, p3.x, p4.x)
          const y2 = Math.max(p1.y, p2.y, p3.y, p4.y)
          const mondiv = j3pAddElt(stor.lesdiv.travail, 'div')
          mondiv.y = y
          mondiv.x = x
          mondiv.larg = (x2 - x)
          mondiv.height = (y2 - y)
          mondiv.style.position = 'relative'
          mondiv.style.top = y - tailleavrire + 'px'
          mondiv.style.left = x + 'px'
          mondiv.style.width = (x2 - x) + 'px'
          mondiv.style.height = (y2 - y) + 'px'
          mondiv.style.background = '#fff'
          tailleavrire += (y2 - y)
          stor.listMondiv.push(mondiv)
        }
        stor.mtgAppLecteur.setHidden({ elt: stor.lexo.listZone[i].p1 })
        if (stor.lexo.listZone[i].type !== 'label') {
          stor.mtgAppLecteur.setHidden({ elt: stor.lexo.listZone[i].texte })
          stor.mtgAppLecteur.setHidden({ elt: stor.lexo.listZone[i].p2 })
          stor.mtgAppLecteur.setHidden({ elt: stor.lexo.listZone[i].p3 })
          stor.mtgAppLecteur.setVisible({ elt: stor.lexo.listZone[i].poly })
          stor.mtgAppLecteur.setColor({ elt: stor.lexo.listZone[i].poly, opacity: 0, color: '#000' })
        }
      }
      afficheMesEtiquettes(stor.lesdiv.etikDiv, stor.etiquettes)
      afficheMonTexte(stor.lesdiv.travail, tailleavrire)
    })
    j3pAjouteBouton(stor.lesdiv.zoneBout1, agrandiMax, { value: '⬜' })
    j3pAjouteBouton(stor.lesdiv.zoneBout2, agrandiMin, { value: '◽' })
    stor.boutFauxOK = j3pAjouteBouton(stor.lesdiv.zoneBout3, fauxOK, { value: 'ok', className: 'big ok' })
    me.finEnonce()
  }
  function afficheMonTexte (dou, taille) {
    stor.listPlaceEtik = []
    // faudra que je gère les gras italic au dessus de ma place
    for (let i = 0; i < stor.mesZone.length; i++) {
      const mondiv = j3pAddElt(dou, 'div')
      mondiv.style.position = 'relative'
      mondiv.style.background = '#fff'
      mondiv.style.verticalAlign = 'middle'
      mondiv.style.textAlign = 'center'
      mondiv.style.top = (stor.mesZone[i].p1.y - taille) + 'px'
      mondiv.style.left = stor.mesZone[i].p1.x + 'px'
      mondiv.classList.add('pourFitContent')
      const apush = {}
      const spanEtik = j3pAddElt(mondiv, 'span')
      apush.place = new PlaceEtiquettes(spanEtik, 'etik' + i, stor.testEtik[i].etik, { heightFix: stor.mesZone[i].height, sansBord: true })
      stor.listPlaceEtik.push(apush)
      const acompte = mondiv.getBoundingClientRect()
      taille += acompte.height
      stor.mesZone[i].mondiv = mondiv
      mondiv.y = stor.mesZone[i].p1.y
      mondiv.x = stor.mesZone[i].p1.x
      mondiv.larg = acompte.width
      mondiv.height = acompte.height
      stor.listMondiv.push(mondiv)
      mondiv.etik = true
      mondiv.place = apush.place
      mondiv.style.background = '#C0C0C0'
      mondiv.style.border = '1px solid black'
      mondiv.style.borderRadius = '10px'
    }
  }
  function afficheMesEtiquettes (ou, ki) {
    ou.style.verticalAlign = 'top'
    const tabDchoices = addDefaultTable(ou, 3, 1)
    stor.testEtik = []
    stor.etikAt = []
    stor.spanListBut = []
    ou.style.border = '1px solid black'
    ou.style.background = '#aaa'
    tabDchoices[1][0].style.textAlign = 'center'
    tabDchoices[0][0].style.textAlign = 'center'
    tabDchoices[0][0].style.padding = '2px'
    tabDchoices[1][0].style.padding = '2px'
    tabDchoices[2][0].style.padding = '2px'
    j3pAddContent(tabDchoices[0][0], 'Etiquettes')
    j3pAddContent(tabDchoices[1][0], '&nbsp;')
    for (let i = 0; i < ki.length; i++) {
      const etijApuh = {
        div: j3pAddElt(tabDchoices[2][0], 'div')
      }
      let listEtik = ki[i].contAs.map(elt => elt.cont)
      listEtik.push(ki[i].mod)
      listEtik = j3pShuffle(listEtik)
      stor.etikAt.push(faisId(ki[i].mod))
      etijApuh.etik = new Etiquettes(etijApuh.div, 'etik' + i, listEtik, {
        dispo: 'colonne',
        colonne: 1,
        reutilisable: true,
        codePlace: (ki.length > 1) ? (i + 1) : undefined,
        direct: true
      })
      for (let j = 0; j < etijApuh.etik.divARet.length; j++) {
        const el = listEtik[j]
        if (typeof el !== 'string') {
          etijApuh.etik.divARet[j].princ.classList.add('pourFitContent')
          placeJouerSonEtiquettes(etijApuh.etik.divARet[j].princ, el.content, stor, 'txtFigure', j)
        }
      }
      stor.testEtik.push(etijApuh)
      const spanBut = j3pAddElt(tabDchoices[1][0], 'span')
      spanBut.style.cursor = 'pointer'
      spanBut.style.width = '20px'
      spanBut.style.height = '20px'
      spanBut.style.border = '1px solid black'
      spanBut.style.borderRadius = '7px'
      j3pAddContent(spanBut, '&nbsp;' + (i + 1) + '&nbsp;')
      spanBut.addEventListener('click', () => {
        stor.testEtik[i].div.classList.remove('cacheTruc')
        stor.testEtik[i].div.classList.add('montreTruc')
        spanBut.style.background = '#b97676'
        for (let k = 0; k < ki.length; k++) {
          if (k === i) continue
          stor.testEtik[k].div.classList.add('cacheTruc')
          stor.testEtik[k].div.classList.remove('montreTruc')
          stor.spanListBut[k].style.background = ''
        }
      })
      stor.spanListBut.push(spanBut)
      if (i !== 0) {
        etijApuh.div.classList.add('cacheTruc')
      } else {
        spanBut.style.background = '#b97676'
        etijApuh.div.classList.add('montreTruc')
      }
      j3pAddContent(tabDchoices[1][0], '&nbsp;')
    }
    if (ki.length === 1) tabDchoices[1][0].style.display = 'none'
  }
  function vireAide () {
    if (stor.bulleAide) {
      for (let i = 0; i < stor.bulleAide; i++) {
        stor.bulleAide[i].destroy()
      }
    }
    stor.bulleAide = []
  }
  function isRepOk () {
    vireAide()
    stor.ckOK = []
    let ok = true
    for (let i = 0; i < stor.listPlaceEtik.length; i++) {
      j3pEmpty(stor.listPlaceEtik[i].place.placeAide)
      const idRep = faisId(stor.listPlaceEtik[i].place.contenu)
      const idAt = stor.etikAt[i]
      stor.ckOK[i] = idRep === idAt
      if (idRep !== idAt) {
        ok = false
        const apu = chercheAvert(i, idRep)
        if (apu !== '') {
          stor.bulleAide.push(new BulleAide(stor.listPlaceEtik[i].place.placeAide, apu, {}))
        }
      }
    }
    return ok
  }
  function chercheAvert (nb, idRep) {
    for (let j = 0; j < stor.etiquettes[nb].contAs.length; j++) {
      const idComp = faisId(stor.etiquettes[nb].contAs[j].cont)
      if (idComp === idRep) return stor.etiquettes[nb].contAs[j].avert
    }
  }
  function metToutVert () {
    for (let i = stor.listPlaceEtik.length - 1; i > -1; i--) {
      stor.listPlaceEtik[i].place.corrige(true)
    }
  }
  function affcofo (isFin) {
    if (isFin) {
      for (let i = stor.listPlaceEtik.length - 1; i > -1; i--) {
        stor.listPlaceEtik[i].place.corrige(stor.ckOK[i])
        if (!stor.ckOK[i]) {
          stor.listPlaceEtik[i].place.barre()
          stor.listPlaceEtik[i].divCo = j3pAddElt(stor.listPlaceEtik[i].place.iddivconteneur, 'span')
          stor.listPlaceEtik[i].divCo.style.display = 'inline-block'
          affCont(stor.listPlaceEtik[i].divCo, stor.etiquettes[i].mod)
          stor.listPlaceEtik[i].divCo.style.border = ''
          stor.listPlaceEtik[i].divCo.style.background = '#C0C0C0'
          stor.listPlaceEtik[i].divCo.style.color = me.styles.colorCorrection
          stor.listPlaceEtik[i].divCo.style.margin = '1px'
          stor.listPlaceEtik[i].divCo.style.display = 'none'
        }
      }
      for (let i = stor.listPlaceEtik.length - 1; i > -1; i--) {
        stor.listPlaceEtik[i].place.disable()
      }
      for (let i = 0; i < stor.testEtik.length; i++) {
        stor.testEtik[i].etik.detruit()
      }
      j3pEmpty(stor.lesdiv.etikDiv)
      stor.lesdiv.etikDiv.style.background = ''
      stor.lesdiv.etikDiv.style.border = '0px'
      stor.leBouCo = j3pAjouteBouton(stor.lesdiv.etikDiv, voirCo, { value: 'Voir la correction' })
      stor.coViz = false
    }
    gereMove()
  }
  function affCont (div2, buf2) {
    if (typeof buf2 === 'string') {
      j3pAffiche(div2, null, buf2)
    } else {
      const el = buf2
      const ui = j3pAddElt(div2, 'div')
      ui.style.textAlign = 'center'
      placeJouerSon(ui, el.content, stor, 'txtFigure')
    }
  }
  function voirCo () {
    stor.coViz = !stor.coViz
    stor.leBouCo.value = (stor.coViz) ? 'Voir mon travail' : 'Voir la correction'
    for (let i = 0; i < stor.listPlaceEtik.length; i++) {
      if (!stor.ckOK[i]) {
        stor.listPlaceEtik[i].place.hideShow(stor.coViz)
        stor.listPlaceEtik[i].divCo.style.display = (stor.coViz) ? 'inline-block' : 'none'
      }
    }
    gereMove()
  }
  function gereMove () {
    const tailleLim = j3pClone(stor.limmm3.getBoundingClientRect())
    let tailleavrire = 149 + stor.lexo.height * 280 + tailleLim.height
    for (let i = 0; i < stor.listMondiv.length; i++) {
      stor.listMondiv[i].style.top = (stor.listMondiv[i].y * stor.aMul - tailleavrire) + 'px'
      stor.listMondiv[i].style.left = (stor.listMondiv[i].x * stor.aMul) + 'px'
      stor.listMondiv[i].style.width = stor.listMondiv[i].larg * stor.aMul + 'px'
      stor.listMondiv[i].style.height = stor.listMondiv[i].height * stor.aMul + 'px'
      tailleavrire += stor.listMondiv[i].height * stor.aMul
      if (stor.listMondiv[i].etik) {
        stor.listMondiv[i].style.width = (stor.listMondiv[i].larg * stor.aMul + 2) + 'px'
        stor.listMondiv[i].style.height = (stor.listMondiv[i].height * stor.aMul + 2) + 'px'
      }
    }
  }
  function disaTout () {
    for (let i = stor.listPlaceEtik.length - 1; i > -1; i--) {
      stor.listPlaceEtik[i].place.disable()
    }
    for (let i = 0; i < stor.testEtik.length; i++) {
      stor.testEtik[i].etik.detruit()
    }
    stor.lesdiv.etikDiv.style.display = 'none'
  }
  function yaReponse () {
    if (me.isElapsed) return true
    for (let i = 0; i < stor.listPlaceEtik.length; i++) {
      if (!stor.listPlaceEtik[i].place.contenu) return false
    }
    return true
  }

  switch (this.etat) {
    case 'enonce':
      // code exécuté au lancement de la section
      if (me.debutDeLaSection) {
        initSection()
      } else {
        enonceMain()
      }
      break // case "enonce":

    case 'correction':
      // On teste si une réponse a été saisie

      if (!yaReponse()) {
        // pas de réponse
        stor.lesdiv.correction.style.color = me.styles.cfaux
        stor.lesdiv.correction.innerHTML = 'Réponse incomplète !'
        return this.finCorrection()
      }

      // Bonne réponse
      if (isRepOk()) {
        stor.lesdiv.correction.style.color = me.styles.cbien
        stor.lesdiv.correction.innerHTML = cBien
        stor.boutFauxOK.style.display = 'none'
        metToutVert()
        disaTout()

        me.score = j3pArrondi(me.score + 1, 1)
        me.typederreurs[0]++
        return this.finCorrection('navigation', true)
      }
      // Pas de bonne réponse
      stor.lesdiv.correction.style.color = me.styles.cfaux
      stor.lesdiv.correction.innerHTML = cFaux
      if (me.isElapsed) {
        // A cause de la limite de temps :
        stor.lesdiv.correction.innerHTML += '<br>' + tempsDepasse + '<BR>'
        me.typederreurs[10]++
        stor.boutFauxOK.style.display = 'none'
        affcofo(true)
        return this.finCorrection('navigation', true)
      }

      // Réponse fausse :
      if (me.essaiCourant < me.donneesSection.nbchances) {
        stor.lesdiv.correction.innerHTML += '<br>' + essaieEncore
        affcofo(false)
        me.typederreurs[1]++
        return this.finCorrection()
      }
      stor.boutFauxOK.style.display = 'none'
      // Erreur au dernier essai
      affcofo(true)
      me.typederreurs[2]++
      return this.finCorrection('navigation', true)
      // case "correction":

    case 'navigation':

      if (me.sectionTerminee()) {
        // On détermine la phrase d’état renvoyée par la section
        me.afficheBoutonSectionSuivante()
        me.focus('sectioncontinuer')
      } else {
        me.etat = 'enonce'
        me.afficheBoutonSuite()
        me.focus('boutoncontinuer')
      }
      // Obligatoire
      me.finNavigation()
      break // case "navigation":
  }
}
