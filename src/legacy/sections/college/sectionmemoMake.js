import { j3pAddContent, j3pAddElt, j3pAjouteBouton, j3pArrondi, j3pClone, j3pDetruit, j3pEmpty, j3pGetNewId, j3pShowError, j3pShuffle } from 'src/legacy/core/functions'
import { addDefaultTable } from 'src/legacy/themes/table'
import { getMtgCore } from 'src/lib/outils/mathgraph'
import Memory from 'src/legacy/outils/memory/Memory'
import getDefaultProgramme from './defaultProgramme'
import { j3pCreeSVG } from 'src/legacy/core/functionsSvg'
import { j3pAffiche } from 'src/lib/mathquill/functions'
import { j3pAfficheCroixFenetres, j3pCreeFenetres, j3pToggleFenetres } from 'src/legacy/core/functionsJqDialog'
import textesGeneriques from 'src/lib/core/textes'

const { tempsDepasse } = textesGeneriques

async function initEditor (container, values, btn) {
  const { default: editor } = await import('./editorProgramme.js')
  return editor(container, values, btn) // la fct qui prend un conteneur et les params comme arguments
}

/**
 * Ajouter une description ici
 * @fileOverview
 * @author Tommy Barroy <t.barroy@laposte.net>
 * @since 2017-10
 */
export const params = {
  outils: ['calculatrice', 'mathquill'],
  /**
   * Ce qui est paramétrable dans l’éditeur de graphe
   * @type {LegacySectionParamsParametres}
   */
  parametres: [
    ['limite', 60, 'entier', 'Temps maximum pour choisir (sinon ce sera le premier choix, mettre 0 pour ne pas limiter)'],
    ['Cartes', JSON.stringify(getDefaultProgramme()), 'multiEditor', 'Json figure de base et figure attendue.', initEditor],
    ['Calculatrice', 'Non', 'liste', '<u>Oui</u>:  Une calculatrice est disponible', ['Oui', 'Non']],
    ['antimemory', 'Oui', 'liste', '<u>Oui</u>:  Les cartes sont tout le temps visibles', ['Oui', 'Non']],
    ['theme', 'standard', 'liste', 'Modèle pour l’affichage', ['standard', 'zonesAvecImageDeFond']]
  ],
  // dans le cas d’une section QUANTITATIVE
  pe: [
  ]
}

/**
 * section choix
 * @this {Parcours}
 */
export default function main () {
  const me = this
  const ds = me.donneesSection
  const stor = me.storage
  const aleaBase = [1, 2, 3, 4, 5]

  function initSection () {
    // chargement mtg
    me.construitStructurePage('presentation3')
    try {
      stor.Cartes = JSON.parse(ds.Cartes)
    } catch (error) {
      j3pShowError('Cartes mises en paramètre invalide')
      stor.Cartes = getDefaultProgramme()
    }
    if (ds.antimemory === 'Oui') ds.score = '0 ou 100%'
    const dedeb = (ds.antimemory === 'Oui') ? 'Paires: ' : 'Memory: '
    if (ds.Montre_separe === 'Oui' && ds.antimemory === 'Non') {
      if (ds.limite > 0) ds.limite += 10
    }
    let titre = stor.Cartes.titre
    if (!titre || titre === '') titre = ' '
    me.afficheTitre(dedeb + titre)
    getMtgCore({ withMathJax: true })
      .then(
        // success
        (mtgAppLecteur) => {
          stor.mtgAppLecteur = mtgAppLecteur
          enonceMain()
        },
        // failure
        (error) => {
          j3pShowError(error, { message: 'mathgraph n’est pas correctement chargé', mustNotify: true })
        })
    // plantage dans le code de success
      .catch(j3pShowError)
  }
  function boutonfen () {
    j3pToggleFenetres('Calculatrice')
  }
  function creeLesDiv () {
    stor.lesdiv = {}
    stor.lesdiv.conteneur = j3pAddElt(me.zonesElts.MG, 'div', {
      style: me.styles.etendre('toutpetit.enonce', { padding: '10px' })
    })
    const uu = addDefaultTable(stor.lesdiv.conteneur, 1, 5)
    stor.lesdiv.enonce = uu[0][0]
    uu[0][1].style.width = '20px'
    uu[0][3].style.width = '20px'
    stor.lesdiv.travail = addDefaultTable(stor.lesdiv.conteneur, 1, 1)[0][0]
    stor.lesdiv.explications = j3pAddElt(stor.lesdiv.conteneur, 'div')
    stor.lesdiv.correction = uu[0][4]
    stor.lesdiv.calculatrice = uu[0][2]
    stor.lesdiv.explications.style.color = me.styles.cfaux
    stor.divmatgraA = addDefaultTable(stor.lesdiv.conteneur, 1, 1)[0][0]
    stor.divmatgraB = addDefaultTable(stor.lesdiv.conteneur, 1, 1)[0][0]
    stor.divmatgraA.style.display = 'none'
    stor.divmatgraB.style.display = 'none'
    stor.lesdiv.enonce.classList.add('enonce')
    stor.lesdiv.travail.classList.add('travail')
    me.zonesElts.MG.classList.add('fond')
  }
  function renvAlea () {
    if (stor.tabAleaBase.length === 0) stor.tabAleaBase = j3pShuffle(aleaBase)
    return stor.tabAleaBase.pop()
  }

  function enonceMain () {
    me.videLesZones()
    creeLesDiv()
    stor.tabAleaBase = j3pShuffle(aleaBase)
    stor.mtgAppLecteur.removeAllDoc()
    if (ds.Calculatrice === 'Oui') {
      me.fenetresjq = [{ name: 'Calculatrice', title: 'Une calculatrice', left: 600, top: 10 }]
      j3pCreeFenetres(me)
      j3pAfficheCroixFenetres('Calculatrice')
      j3pAjouteBouton(stor.lesdiv.calculatrice, boutonfen, { className: 'MepBoutons', value: 'Calculatrice' })
    }

    // met deux mg pour les fonctions
    if (stor.Cartes.carteChoix !== 'expl') {
      if (stor.Cartes.CartesGen.modeA === 'genC') {
        stor.svgIdA = j3pGetNewId('mtg2')
        j3pCreeSVG(stor.divmatgraA, { id: stor.svgIdA, width: 0, height: 0 })
        stor.mtgAppLecteur.addDoc(stor.svgIdA, stor.Cartes.CartesGen.genA, true)
      }
      if (stor.Cartes.CartesGen.modeB === 'genC') {
        stor.svgIdB = j3pGetNewId('mtgX')
        j3pCreeSVG(stor.divmatgraB, { id: stor.svgIdB, width: 0, height: 0 })
        stor.mtgAppLecteur.addDoc(stor.svgIdB, stor.Cartes.CartesGen.genB, true)
      }

      stor.svgIdAG = []
      if (stor.Cartes.CartesGen.modeA === 'liste') {
        for (let i = Number(stor.Cartes.CartesGen.minA) - 1; i < Number(stor.Cartes.CartesGen.maxA) + 1; i++) {
          if (typeof stor.Cartes.CartesGen.liste[i] !== 'string') {
            stor.svgIdAG[i] = j3pClone(stor.Cartes.CartesGen.liste[i])
          }
        }
      }
    }
    const couples = []
    if (stor.Cartes.carteChoix === 'expl') {
      const gg = j3pShuffle(j3pClone(stor.Cartes.CartesExpl))
      for (let i = 0; i < Number(stor.Cartes.nbCouples); i++) {
        couples.push(donneEl(gg.pop()))
      }
    } else {
      // verif diff en minA et maxA laisse assez de couples
      const iminA = Number(stor.Cartes.CartesGen.minA)
      const imaxA = Number(stor.Cartes.CartesGen.maxA)
      // fais 2 listes ( en fonction de liste ou gen )
      let tabPourI = []
      for (let i = iminA; i < imaxA + 1; i++) tabPourI.push(i)
      tabPourI = j3pShuffle(tabPourI)
      for (let i = 0; i < tabPourI.length; i++) {
        const aa = donneImAi(tabPourI[i])
        const bb = donneImBi(tabPourI[i])
        if (aa === undefined || aa === '') continue
        couples.push([aa, bb])
        if (couples.length === Number(stor.Cartes.nbCouples)) break
      }
    }
    const ggh = ((ds.antimemory === 'Non') || (stor.Cartes.consigne !== ''))
    if (ds.antimemory === 'Non') {
      j3pAffiche(stor.lesdiv.enonce, null, 'Observe bien les cartes ci dessous&nbsp;')
      j3pAjouteBouton(stor.lesdiv.enonce, clicPret, { value: 'Commencer' })
    } else {
      if (stor.Cartes.consigne !== '') {
        j3pAffiche(stor.lesdiv.enonce, null, stor.Cartes.consigne)
      }
    }
    stor.lesdiv.enonce.style.display = ggh ? '' : 'none'
    const onComplete = (/* score */) => me.sectionCourante()
    stor.memory = Memory.create(stor.lesdiv.travail, couples, onComplete, { antimemory: ds.antimemory === 'Oui' })
    me.finEnonce()
    me.cacheBoutonValider()
  }
  function donneEl (c) {
    return [donEl(c[0]), donEl(c[1])]
  }
  function donEl (e) {
    if (typeof e === 'string') return e
    if (e.type === 'mathgraph' || e.type === undefined) return { type: 'mathgraph', content: { width: e.content.dimx, height: e.content.dimy, txtFigure: e.content.txtFigure } }
    return j3pClone(e)
  }
  function donneImBi (i) {
    if (stor.Cartes.CartesGen.modeB === 'genC') {
      stor.mtgAppLecteur.giveFormula2(stor.svgIdB, 'A', i)
      stor.mtgAppLecteur.giveFormula2(stor.svgIdB, 'ALEA', renvAlea())
      stor.mtgAppLecteur.calculate(stor.svgIdB, true)
      return '$' + stor.mtgAppLecteur.getLatexCode(stor.svgIdB, '#formule') + '$'
    } else {
      return { type: 'mathgraph', content: { width: stor.Cartes.CartesGen.genBMGx.x, height: stor.Cartes.CartesGen.genBMGx.y, txtFigure: stor.Cartes.CartesGen.genBMG, A: i, ALEA: renvAlea() } }
    }
  }

  function donneImAi (i) {
    if (stor.Cartes.CartesGen.modeA === 'liste') {
      if (!stor.Cartes.CartesGen.liste[i - 1]) return ''
      if (typeof stor.Cartes.CartesGen.liste[i - 1] === 'string') {
        return stor.Cartes.CartesGen.liste[i - 1]
      } else {
        return { type: 'mathgraph', content: { width: stor.svgIdAG[i - 1].content.dimx, height: stor.svgIdAG[i - 1].content.dimy, txtFigure: stor.svgIdAG[i - 1].content.txtFigure } }
      }
    } else if (stor.Cartes.CartesGen.modeA === 'genC') {
      stor.mtgAppLecteur.giveFormula2(stor.svgIdA, 'A', i)
      stor.mtgAppLecteur.giveFormula2(stor.svgIdA, 'ALEA', renvAlea())
      stor.mtgAppLecteur.calculate(stor.svgIdA, true)
      return '$' + stor.mtgAppLecteur.getLatexCode(stor.svgIdA, '#formule') + '$'
    } else {
      return { type: 'mathgraph', content: { width: stor.Cartes.CartesGen.genAMGx.x, height: stor.Cartes.CartesGen.genAMGx.y, txtFigure: stor.Cartes.CartesGen.genAMG, A: i, ALEA: renvAlea() } }
    }
  }
  function clicPret () {
    j3pEmpty(stor.lesdiv.enonce)
    if (stor.Cartes.consigne !== '') {
      j3pAffiche(stor.lesdiv.enonce, null, stor.Cartes.consigne)
    }
    stor.memory.clicPretListener()
    me.afficheBoutonValider()
  }
  function isRepOk () {
    stor.memory.disable()
    return stor.memory.isOk()
  }

  function afficheCorrection () {
    stor.lesdiv.explications.classList.add('explique')
    j3pAddContent(stor.lesdiv.explications, 'Clique une carte pour voir la carte qui lui correspond.')
  }

  switch (this.etat) {
    case 'enonce':
      // code exécuté au lancement de la section
      if (me.debutDeLaSection) {
        initSection()
      } else {
        stor.raz = true
        enonceMain()
      }
      break // case "enonce":

    case 'correction':
      // On teste si une réponse a été saisie
      if (stor.lesdiv === undefined) return
      stor.lesdiv.explications.innerHTML = ''
      stor.lesdiv.correction.innerHTML = ''

      if (isRepOk()) {
        // Bonne réponse
        stor.lesdiv.correction.style.color = this.styles.cbien
        stor.lesdiv.correction.innerHTML = 'Terminé.'
        this.score = j3pArrondi(this.score + stor.memory.getScore(), 1)

        this.typederreurs[0]++
        return this.finCorrection('navigation', true)
      }

      // Pas de bonne réponse
      stor.lesdiv.correction.style.color = this.styles.cfaux
      j3pDetruit(stor.lesdiv.enonce)
      if (me.isElapsed) {
        stor.lesdiv.correction.innerHTML = tempsDepasse + '<BR>'
      } else {
        stor.lesdiv.correction.innerHTML = 'Plus d’essais possibles' + '<BR>'
      }
      this.typederreurs[10]++
      afficheCorrection(true)
      return this.finCorrection('navigation', true)

    case 'navigation':
      if (!this.sectionTerminee()) {
        this.etat = 'enonce'
      }
      this.finNavigation()
      break // case "navigation":
  }
}
