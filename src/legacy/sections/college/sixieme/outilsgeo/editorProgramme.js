import { j3pAddContent, j3pAddElt, j3pAddTxt, j3pAjouteBouton, j3pAjouteCaseCoche, j3pArrondi, j3pClone, j3pDetruit, j3pEmpty, j3pGetNewId, j3pGetRandomInt, j3pShuffle } from 'src/legacy/core/functions'
import { InitSlider } from 'src/legacy/outils/slider/slider'
import { affCorrectionConst, isRepOkConst, j3pModale2 } from 'src/legacy/outils/zoneStyleMathquill/functions'
import { getMtgCore } from 'src/lib/outils/mathgraph'
import { j3pAffiche } from 'src/lib/mathquill/functions'
import { addDefaultTable } from 'src/legacy/themes/table'
import ZoneStyleMathquill1 from 'src/legacy/outils/zoneStyleMathquill/ZoneStyleMathquill1'
import ListeDeroulante from 'src/legacy/outils/listeDeroulante/ListeDeroulante'
import ZoneStyleMathquill2 from 'src/legacy/outils/zoneStyleMathquill/ZoneStyleMathquill2'
import loadMq from 'src/lib/mathquill/loadMathquill'
import { j3pBaseUrl } from 'src/lib/core/constantes'
import ProgConst from './ProgConst'
import imRgle from 'src/legacy/sections/college/sixieme/Elementgeo/images/reg.png'
import imRgle2 from 'src/legacy/sections/college/sixieme/Elementgeo/images/reg2.png'
import imRap from 'src/legacy/sections/college/sixieme/Elementgeo/images/rap.png'
import imEq from 'src/legacy/sections/college/sixieme/Elementgeo/images/equerre.png'
import imCompas from 'src/legacy/sections/college/sixieme/Elementgeo/images/comp.png'
import imRot from 'src/legacy/sections/college/sixieme/Elementgeo/images/tourn.png'
import imPlus from 'src/legacy/css/j3pcss/zoomplus.png'
import './eitorfit.css'

const ldOptions = { dansModale: true, decalage: 8 }

export default function editorProgramme (container, values) {
  const valuesDeb = values
  return new Promise((resolve, reject) => {
    function calcule (form) {
      const list = editor.listePourCalc
      list.giveFormula2('x', form)
      list.calculateNG()
      return String(editor.listePourCalc.valueOf('x'))
    }

    function initEditor () {
      editor.plo = []
      editor.imagePoint = j3pBaseUrl + 'sections/college/sixieme/Elementgeo/images/Poiint.png'
      editor.imageRegle = j3pBaseUrl + 'sections/college/sixieme/Elementgeo/images/reg.png'
      editor.imageEquerre = j3pBaseUrl + 'sections/college/sixieme/Elementgeo/images/equerre.png'
      editor.imageRegle2 = j3pBaseUrl + 'sections/college/sixieme/Elementgeo/images/reg2.png'
      editor.imageRapporteur = j3pBaseUrl + 'sections/college/sixieme/Elementgeo/images/rap.png'
      editor.imageCompas = j3pBaseUrl + 'sections/college/sixieme/Elementgeo/images/comp.png'
      editor.imageGomme = j3pBaseUrl + 'sections/college/sixieme/Elementgeo/images/gomm.png'
      editor.imageRouge = j3pBaseUrl + 'sections/college/sixieme/Elementgeo/images/rouge.png'
      editor.imageCrayon = j3pBaseUrl + 'sections/college/sixieme/Elementgeo/images/CRAY3.png'
      editor.imageVerrou = j3pBaseUrl + 'sections/college/sixieme/Elementgeo/images/verrouv.png'
      editor.imageVerrou2 = j3pBaseUrl + 'sections/college/sixieme/Elementgeo/images/verrfer.png'
      editor.tempo = 20
      editor.tempodeb = 900
      editor.depCentreCo = 2
      editor.tempocligne = 400
      editor.reglebasx = 300
      editor.reglebasy = 230
      editor.nbClignoteMAx = 10
      editor.distcol = 15
      editor.polyhaut = ['#poly0', '#poly1', '#poly2', '#poly3', '#poly4', '#poly5', '#poly6', '#poly7']
      editor.polydroite = ['#polyd7', '#polyd6', '#polyd5', '#polyd4', '#polyd3', '#polyd2', '#polyd1']
      editor.ptsabouge = ['#regpt1', '#eq1', '#rappt1', '#compt1', '#compt2', '#ptCa1', '#ptCa2', 'Q', '#raprap10']
      editor.idImCray = '#ImLEcray'
      editor.idImCrayPt = '#ptLEcray'
      editor.pointpourpo = '#ptpourpt'
      editor.idRegfleGrad = ['#reg4', '#reg1', '#reg3', '#reg2']
      editor.idRegle = ['#regs1', '#regs2', '#regs3', '#regsu1', '#regsu2', '#regsu3', '#regsu4', '#regs4', '#regs5', '#regs6', '#regs7', '#titrait', '#rgb']
      editor.idReglePoly = ['#regpo1', '#regpo2', '#regpo3', '#fghjk']
      editor.idPtRegle = { pt: '#regpt1', ma: '#mqreg', pt2: '#regpt2', ptCap: '#ptcapregle', ptCap2: '#regptcap2' }
      editor.reglettletemps = ['#regtt']
      editor.regtra = ['#rgTra1', '#regTra2', '#regTra3', '#regIm2', '#regsegseg']
      editor.idEquerre = ['#eqseg1', '#eqseg2', '#eqseg3', '#eqseg4', '#eqb']
      editor.idEquerrePoly = ['#eqpoly1', '#eqpoly2', '#eqpoly3']
      editor.EquerSurfa = '#eqsurf1'
      editor.idPtEquerre = { pt: '#eq1', pt2: '#pteqma0', ma: '#eqma1' }
      editor.DtEque = [['#eqqq1', '#eqqq2'], ['#eqqq3', '#eqqq4'], ['#eqqq5', '#eqqq6']]
      editor.equttletemps = ['#eqdt1', '#eqdt2', '#eqdt3']
      editor.idRap = ['#rapc2', '#rap14', '#rap15', '#rap16', '#rapl1', '#rapl2', '#rapl3', '#rapl4', '#rapl5', '#rapl6', '#raps1', '#raps2', '#raps3', '#raps4', '#raps5', '#raprap1', '#raprap2', '#raprap3', '#raprap4']
      editor.idRapPoly = ['#rap12', '#rap13', '#rappo1', '#rappo2']
      editor.idPRap = { pt: '#rappt1', ma: '#rapma', pt2: '#rapp2', pt3: 626 }
      editor.rapttletemps = '#raptt'
      editor.idComp1 = ['#compsur1', '#compsur2', '#compsur3', '#compsur4', '#compma', '#compsur7', '#compsur6']
      editor.idComp = ['#comppoly1', '#comppoly2', '#comppoly3', '#comppoly4', '#comppoly5', '#comppoly6']
      editor.idCompPt = { pointe: '#compt1', geremine: '#compt2', mine: '#compt3', range: '#compt4', compcoupt: '#compcoupt' }
      editor.idCompttletps = '#comptt'
      editor.idCoparcd = ['#ctrac1', '#ctrap1', '#ctrap2', '#ctraa1']
      editor.idCoparcs = ['#ctrac2', '#ctrap3', '#ctrap4', '#ctraa2']
      editor.compaTra = ['#comppoly7', '#comppoly8', '#comppoly9', '#comppoly10', '#comppoly12', '#comptrasur1', '#comptrasur2', '#comptrasur3', '#comptrasur4', '#comptrasur5', '#comptrasur6']

      editor.nomtextdr = [['#pd1', '#pd1t', '#ss1'], ['#pd2', '#pd2t', '#ss2'], ['#pd3', '#pd3t', '#ss3'], ['#pd4', '#pd4t', '#ss4'], ['#pd5', '#pd5t', '#ss5'], ['#pd6', '#pd6t', '#ss6'], ['#pd7', '#pd7t', '#ss7'], ['#pd8', '#pd8t', '#ss8'], ['#pd9', '#pd9t', '#ss9'], ['#pd10', '#pd10t', '#ss10'], ['#pd11', '#pd11t', '#ss11'], ['#pd12', '#pd12t', '#ss12'], ['#pd13', '#pd13t', '#ss13'], ['#pd14', '#pd15t', '#ss15'], ['#pd15', '#pd14t', '#ss14']]
    }

    function menuBase () {
      editor.modifEnCours = false
      for (let i = 0; i < editor.ProgrammeLT.length; i++) {
        if (!editor.ProgrammeLT[i].nom) editor.ProgrammeLT[i].nom = 'exercice ' + i
        if (!editor.ProgrammeLT[i].niv) editor.ProgrammeLT[i].niv = 0
      }
      j3pEmpty(editor.lesdiv.div1)
      const tabEx = addDefaultTable(editor.lesdiv.div1, editor.ProgrammeLT.length + 3, 1)
      const tabEx1 = addDefaultTable(tabEx[0][0], 1, 2)
      j3pAddContent(tabEx1[0][0], '<b><u>Exercices disponibles</u>:</b>&nbsp;&nbsp;')
      j3pAjouteBouton(tabEx1[0][0], ajouteExoTout, { value: 'Dupliquer tous les exercices' })
      for (let i = 0; i < editor.ProgrammeLT.length; i++) {
        const tabee = addDefaultTable(tabEx[i + 1][0], 1, 4)
        tabee[0][0].style.padding = '5px'
        tabee[0][1].style.padding = '5px'
        tabee[0][2].style.padding = '5px'
        tabee[0][3].style.padding = '5px'
        tabee[0][3].parentNode.style.border = '1px solid black'
        j3pAddContent(tabee[0][0], '<b><u>Exercice ' + i + '</u></b>: ' + editor.ProgrammeLT[i].nom + ' ( niv ' + editor.ProgrammeLT[i].niv + ') ')
        j3pAjouteBouton(tabee[0][1], modifExo(i), { value: 'Modifier' })
        j3pAjouteBouton(tabee[0][2], EffaceExo(i), { value: 'Effacer' })
        j3pAjouteBouton(tabee[0][3], duplikExo(i), { value: 'Dupliquer' })
      }
      j3pAddContent(tabEx[editor.ProgrammeLT.length + 2][0], '<br>')
      j3pAjouteBouton(tabEx[editor.ProgrammeLT.length + 2][0], ajouteExo, { value: '+' })
    }
    function ajouteExoTout () {
      for (let i = editor.ProgrammeLT.length - 1; i > -1; i--) {
        const aDup = j3pClone(editor.ProgrammeLT[i])
        editor.ProgrammeLT.push(aDup)
      }
      menuBase()
    }
    function ajouteExo () {
      editor.ProgrammeLT.push({ nom: 'Nouvel Exercice', fig: [], prog: [], niv: 0 })
      menuBase()
    }
    function EffaceExo (i) {
      return () => {
        editor.ProgrammeLT.splice(i, 1)
        menuBase()
      }
    }
    function duplikExo (i) {
      return () => {
        const aDup = j3pClone(editor.ProgrammeLT[i])
        editor.ProgrammeLT.push(aDup)
        menuBase()
      }
    }
    function modifExo (i) {
      return () => {
        modifierExo(i)
      }
    }
    function modifierExo (i) {
      editor.raz = true
      editor.SvgId = { dep: '', test: '' }
      editor.lesdiv.div2.style.display = ''
      editor.lesdiv.div3.style.display = ''
      faisChoixExos(i)
      creeLesDiv2()
      affichelistedep()
      afficheMenu()
      remetCool()
      j3pEmpty(editor.lesdiv.div1)
      j3pAddContent(editor.lesdiv.div1, '<br>')
      j3pAddContent(editor.lesdiv.div1, '<b>Modification de l’exercice ' + editor.numExo + ':&nbsp;&nbsp;&nbsp;</b>')
      const inputNom = j3pAddElt(editor.lesdiv.div1, 'input', '', { size: 20, value: editor.lenom })
      inputNom.addEventListener('change', () => {
        editor.lenom = inputNom.value
      })
      j3pAddContent(editor.lesdiv.div1, '<br><br>')
      j3pAjouteBouton(editor.lesdiv.div1, valModExoT, { value: 'Valider' })
      j3pAddContent(editor.lesdiv.div1, '&nbsp;&nbsp;')
      j3pAjouteBouton(editor.lesdiv.div1, annuleModExo, { value: 'Annuler' })
      j3pAddContent(editor.lesdiv.div1, '<br><br>')
      editor.modifEnCours = true
    }
    function valModExoT () {
      if (editor.blop) {
        const yy = j3pAddElt(editor.lesdiv.div1, 'div')
        j3pAddContent(yy, 'Avertissement ')
        j3pAddContent(yy, '\n')
        j3pAddContent(yy, 'Un exercice est en cours\n de modification.')
        j3pAddContent(yy, '\n')
        j3pAjouteBouton(yy, () => {
          j3pDetruit(yy)
        }, { value: 'Annuler' })
        j3pAddContent(yy, '\n')
        j3pAddContent(yy, '\n')
        j3pAjouteBouton(yy, () => {
          j3pDetruit(yy)
          valModExo()
          remetCool()
        }, { value: 'Tant pis' })
      } else {
        valModExo()
      }
    }
    function valModExo () {
      editor.ProgrammeLT[editor.numExo].nom = editor.lenom
      editor.ProgrammeLT[editor.numExo].prog = j3pClone(editor.Programme)
      editor.ProgrammeLT[editor.numExo].fig = editor.progfig
      editor.ProgrammeLT[editor.numExo].yac = editor.yaaacentre
      editor.ProgrammeLT[editor.numExo].yar = editor.yaaaarot
      editor.ProgrammeLT[editor.numExo].yaz = editor.yaaaazom
      editor.ProgrammeLT[editor.numExo].zoomin = editor.zoomin
      editor.ProgrammeLT[editor.numExo].zoomax = editor.zoomax
      editor.ProgrammeLT[editor.numExo].limcercle = editor.limcercle
      editor.ProgrammeLT[editor.numExo].nbseg = 100
      editor.ProgrammeLT[editor.numExo].nbdte = editor.limdte
      editor.ProgrammeLT[editor.numExo].nbddte = editor.limddte
      editor.ProgrammeLT[editor.numExo].nbarc = editor.limarc
      editor.ProgrammeLT[editor.numExo].nbAt = editor.cocheTrait.checked
      menuBase()
      editor.lesdiv.div2.style.display = 'none'
      editor.lesdiv.div3.style.display = 'none'
      editor.lesdiv.div4.style.display = 'none'
      editor.modifEnCours = false
    }

    function annuleModExo () {
      menuBase()
      remetCool()
      editor.lesdiv.div2.style.display = 'none'
      editor.lesdiv.div3.style.display = 'none'
      editor.lesdiv.div4.style.display = 'none'
      editor.modifEnCours = false
    }

    function derecupe (t, f) {
      for (let j = editor.progConst.nomprisProg.length - 1; j > -1; j--) {
        const regex = new RegExp(editor.progConst.nomprisProg[j].com, 'g')
        t = t.replace(regex, editor.progConst.nomprisProg[j].ki)
      }
      if (f) {
        t = t.replace(editor.nomPosProg[editor.nomPosProg.length - 1], trouveNewHaz())
      }
      return t
    }
    function retrouvePoint (p1) {
      for (let i = 0; i < editor.progConst.listePointPris.length; i++) {
        if (editor.progConst.listePointPris[i].nom === p1) return editor.progConst.listePointPris[i]
      }
    }

    function afficheMenu () {
      reNomRecupe()
      remetCool()
      razDiv4()
      j3pEmpty(editor.lesdiv.modifProg)
      j3pEmpty(editor.lesdiv.prog)
      const tabafficheMenu = addDefaultTable(editor.lesdiv.prog, editor.Programme.length + 1, 5)
      editor.listeBoutABlok = []
      editor.liCaCo = []
      editor.tabafficheMenu = tabafficheMenu
      for (let i = 0; i < editor.Programme.length; i++) {
        j3pAddTxt(tabafficheMenu[i][0], (i + 1) + '⯁')
        tabafficheMenu[i][0].style.borderLeft = '1px solid black'
        tabafficheMenu[i][0].style.borderTop = '1px solid black'
        tabafficheMenu[i][0].style.borderBottom = '1px solid black'
        tabafficheMenu[i][1].style.borderTop = '1px solid black'
        tabafficheMenu[i][1].style.borderBottom = '1px solid black'
        tabafficheMenu[i][1].style.borderRight = '1px solid black'
        tabafficheMenu[i][2].style.borderTop = '1px solid black'
        tabafficheMenu[i][2].style.borderBottom = '1px solid black'
        tabafficheMenu[i][2].style.borderTop = '1px solid black'
        tabafficheMenu[i][3].style.borderBottom = '1px solid black'
        tabafficheMenu[i][0].style.background = '#f3bb44'
        tabafficheMenu[i][1].style.background = '#f3bb44'
        tabafficheMenu[i][2].style.background = '#f3bb44'
        tabafficheMenu[i][3].style.background = '#f3bb44'
        tabafficheMenu[i][3].style.borderRight = '1px solid black'
        let aaaf = editor.progConst.recupe(editor.Programme[i].text)
        for (let j = 0; j < 20; j++) {
          const acherch = '§' + j + '§'
          const leReg = new RegExp(acherch, 'g')
          if (aaaf.indexOf(acherch) !== -1) {
            const parQuoiRempl = chopeFormule(j, editor.Programme[i])
            aaaf = aaaf.replace(leReg, '"' + parQuoiRempl + '"')
          }
        }
        if (editor.Programme[i].cond) {
          if (editor.Programme[i].cond.length > 0) {
            aaaf = aaaf.replace('µµ', '$\\widehat{' + editor.progConst.recupe(editor.Programme[i].nom + editor.Programme[i].cond[0].cop + editor.Programme[i].cond[0].cop2) + '}$')
            if (aaaf.indexOf('µ0µ') !== -1) aaaf = aaaf.replace('µ0µ', '$\\widehat{' + editor.progConst.recupe(editor.Programme[i].nom + editor.Programme[i].cond[0].cop + editor.Programme[i].cond[0].cop2) + '}$')
            if (aaaf.indexOf('µ1µ') !== -1) aaaf = aaaf.replace('µ1µ', '$\\widehat{' + editor.progConst.recupe(editor.Programme[i].nom + editor.Programme[i].cond[1].cop + editor.Programme[i].cond[1].cop2) + '}$')
          }
        }
        j3pAffiche(tabafficheMenu[i][1], null, aaaf)
        j3pAddContent(tabafficheMenu[i][2], '&nbsp;')
        const boutSub = j3pAjouteBouton(tabafficheMenu[i][2], makeSupPrig(i), { value: 'Suppr' })
        j3pAddContent(tabafficheMenu[i][2], '&nbsp;')
        j3pAjouteBouton(tabafficheMenu[i][2], makeModPrig(i), { value: 'Modif' })
        j3pAddContent(tabafficheMenu[i][2], '&nbsp;')
        tabafficheMenu[i][1].style.paddingRight = '5px'
        tabafficheMenu[i][2].style.paddingBottom = '5px'
        editor.listeBoutABlok.push(tabafficheMenu[i][2])
        editor.liCaCo[i] = { cont: tabafficheMenu[i][4], elem: tabafficheMenu[i][0], elem2: tabafficheMenu[i][1] }
        tabafficheMenu[i][4].style.display = 'none'
        const duprogdep = retrouvelesDependantsde(editor.Programme[i].nom, editor.Programme[i].type, i)
        boutSub.addEventListener('mouseover', () => {
          tabafficheMenu[i][0].style.background = '#F00'
          tabafficheMenu[i][1].style.background = '#F00'
          for (let k = 0; k < duprogdep.length; k++) {
            tabafficheMenu[duprogdep[k]][0].style.background = '#F00'
            tabafficheMenu[duprogdep[k]][1].style.background = '#F00'
          }
        })
        boutSub.addEventListener('mouseout', () => {
          tabafficheMenu[i][0].style.background = '#f3bb44'
          tabafficheMenu[i][1].style.background = '#f3bb44'
          for (let k = 0; k < duprogdep.length; k++) {
            tabafficheMenu[duprogdep[k]][0].style.background = '#f3bb44'
            tabafficheMenu[duprogdep[k]][1].style.background = '#f3bb44'
          }
        })
      }
      tabafficheMenu[editor.Programme.length][0].style.paddingTop = '10px'
      editor.listeBoutABlok.push(j3pAjouteBouton(tabafficheMenu[editor.Programme.length][0], ajPrg, { value: 'Ajouter' }))
      if (editor.Programme.length > 1) {
        j3pAddContent(tabafficheMenu[editor.Programme.length][0], '&nbsp;&nbsp;')
        editor.listeBoutABlok.push(j3pAjouteBouton(tabafficheMenu[editor.Programme.length][0], combine, { value: 'Combiner' }))
      }
    }
    function chopeFormule (num, tab) {
      if (tab.cond) return tab.cond[num].formule
      let comptForm = -1
      for (let i = 0; i < tab.cont.length; i++) {
        if (tab.cont[i].cond) {
          for (let j = 0; j < tab.cont[i].cond.length; j++) {
            if (tab.cont[i].cond[j].formule) comptForm++
            if (num === comptForm) return tab.cont[i].cond[j].formule
          }
        }
      }
      return '0'
    }
    function combine () {
      editor.editAVire = []
      editor.editAVire2 = []
      chCool2()
      razDiv4()
      editor.lisTaComb = []
      for (let i = 0; i < editor.listeBoutABlok.length; i++) {
        editor.listeBoutABlok[i].style.display = 'none'
      }
      for (let i = 0; i < editor.liCaCo.length; i++) {
        editor.liCaCo[i].cont.style.display = ''
        editor.liCaCo[i].cont.style.background = '#aaa'
        editor.liCaCo[i].cont.style.border = '1 px solid black'
        j3pEmpty(editor.liCaCo[i])
        editor.lisTaComb[i] = j3pAjouteCaseCoche(editor.liCaCo[i].cont)
        editor.lisTaComb[i].addEventListener('change', () => {
          const tabJ = []
          for (let k = 0; k < editor.lisTaComb.length; k++) {
            if (editor.lisTaComb[k].checked) tabJ.push(k)
          }
          if (editor.lisTaComb[i].checked) {
            let maxEk = 0
            for (let k = 0; k < tabJ.length - 1; k++) {
              maxEk = Math.max(maxEk, tabJ[k + 1] - tabJ[k])
            }
            if (maxEk < 2) {
              editor.liCaCo[i].elem.style.background = '#F00'
              editor.liCaCo[i].elem2.style.background = '#F00'
            } else {
              editor.lisTaComb[i].checked = false
            }
          } else {
            if (tabJ.length === 0 || i === tabJ[0] - 1 || i === tabJ[tabJ.length - 1] + 1) {
              editor.liCaCo[i].elem.style.background = '#f3bb44'
              editor.liCaCo[i].elem2.style.background = '#f3bb44'
            } else {
              editor.lisTaComb[i].checked = true
            }
          }
          verfiSiyaCombine()
        })
        if (editor.Programme[i].type === 'multi') {
          editor.liCaCo[i].cont.style.display = 'none'
        }
      }
      j3pAjouteBouton(editor.tabafficheMenu[editor.Programme.length][0], annuleComb, { value: 'Annuler' })
    }
    function annuleComb () {
      editor.lesCombines = []
      remetCool()
      afficheMenu()
    }
    function verfiSiyaCombine () {
      let yena2 = false
      let yena1 = false
      let yaeutrou = false
      let foPa = false
      for (let i = 0; i < editor.liCaCo.length; i++) {
        if (editor.lisTaComb[i].checked) {
          if (yena1) { yena2 = true } else { yena1 = true }
          if (yaeutrou) foPa = true
        } else {
          if (yena1) yaeutrou = true
        }
      }
      j3pEmpty(editor.lesdiv.modifProg)
      if (yena2 && !foPa) {
        j3pAjouteBouton(editor.lesdiv.modifProg, faisCombonine, { value: 'OK' })
      }
    }
    function faisCombonine () {
      for (let i = 0; i < editor.liCaCo.length; i++) {
        editor.liCaCo[i].cont.style.display = 'none'
      }
      editor.lesCombines = []
      editor.liProg = -1
      editor.newP = { type: 'multi', text: ' ', cont: [] }
      for (let i = 0; i < editor.liCaCo.length; i++) {
        if (editor.lisTaComb[i].checked) {
          if (editor.liProg === -1) editor.liProg = i
          editor.lesCombines.push(i)
          editor.newP.cont.push(j3pClone(editor.Programme[i]))
          editor.newP.cont[editor.newP.cont.length - 1].need = true
        }
      }
      editor.CnewComb = true
      faisCombonine2()
    }
    function faisCombonine2 () {
      editor.editAVire2 = []
      j3pEmpty(editor.lesdiv.modifProg)
      for (let i = 0; i < editor.listeBoutABlok.length; i++) {
        editor.listeBoutABlok[i].style.display = 'none'
      }
      editor.editAVire = []
      chCool2()
      const tabProgEl = addDefaultTable(editor.lesdiv.modifProg, editor.newP.cont.length + 3, 3)
      tabProgEl[0][0].style.border = '1px solid black'
      tabProgEl[1][0].style.border = '1px solid black'
      tabProgEl[1][0].style.background = '#fff'
      tabProgEl[1][0].style.paddingBottom = '10px'
      tabProgEl[1][0].style.paddingLeft = '10px'
      tabProgEl[1][0].style.paddingRight = '10px'
      tabProgEl[0][0].style.background = '#fff'
      editor.tabProgEl = tabProgEl
      tabProgEl[0][0].style.paddingBottom = '5px'
      tabProgEl[0][0].style.paddingLeft = '10px'
      tabProgEl[2][0].style.paddingLeft = '10px'
      tabProgEl[3][0].style.paddingLeft = '10px'
      j3pAddContent(tabProgEl[0][0], '<b>Combinaison</b>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;')
      j3pAjouteBouton(tabProgEl[0][0], annuleConst, { value: 'Annuler' })
      j3pAddContent(tabProgEl[0][0], '&nbsp;&nbsp;')
      j3pAjouteBouton(tabProgEl[0][0], separeConst, { value: 'Dé - combiner' })
      const tabProgtxt = addDefaultTable(tabProgEl[1][0], 2, 2)
      j3pAddContent(tabProgtxt[0][0], '<b>TEXT</b>&nbsp;&nbsp;:')
      const inputText = j3pAddElt(tabProgtxt[0][1], 'input', '', { value: editor.newP.text })
      inputText.addEventListener('change', () => {
        editor.newP.text = inputText.value.replace(/'/g, 'ʼ')
      })
      inputText.addEventListener('blur', () => {
        editor.newP.text = inputText.value.replace(/'/g, 'ʼ')
      })
      let contLong = 0
      const tabDej = []
      for (let i = 0; i < editor.newP.cont.length; i++) {
        tabProgEl[2 + i][0].style.background = '#ffd6d6'
        tabProgEl[2 + i][0].style.border = '1px solid black'
        tabProgEl[2 + i][0].style.padding = '5px'
        const tabCont = addDefaultTable(tabProgEl[2 + i][0], 2, 3)
        j3pAddContent(tabCont[0][0], '&nbsp;exigé&nbsp;')
        tabCont[0][0].style.border = '1px solid black'
        tabCont[1][0].style.border = '1px solid black'
        tabCont[0][1].style.padding = '5px'
        tabCont[1][1].style.padding = '5px'
        tabCont[0][2].style.borderLeft = '1px solid black'
        tabCont[1][2].style.borderLeft = '1px solid black'
        tabCont[0][2].style.padding = '5px'
        tabCont[1][2].style.padding = '5px'
        const caseExi = j3pAjouteCaseCoche(tabCont[1][0])
        caseExi.checked = editor.newP.cont[i].need
        caseExi.addEventListener('change', () => {
          editor.newP.cont[i].need = caseExi.checked
        })
        const tabProgtxti = addDefaultTable(tabCont[1][1], 3, 1)
        j3pAddContent(tabProgtxti[0][0], '<u>Text intermed.</u>&nbsp;&nbsp;:')
        const inputTexti = j3pAddElt(tabProgtxti[1][0], 'input', '', { value: editor.newP.cont[i].text })
        inputTexti.addEventListener('change', () => {
          editor.newP.cont[i].text = inputTexti.value
        })
        inputTexti.addEventListener('blur', () => {
          editor.newP.cont[i].text = inputTexti.value
        })
        j3pAddContent(tabCont[0][1], '<u><b>' + editor.newP.cont[i].type + ' ' + editor.progConst.recupe(editor.newP.cont[i].nom) + '</b></u>')
        switch (editor.newP.cont[i].type) {
          case 'point':
            met2bout(tabDej, editor.newP.cont[i].nom, editor.newP.cont[i].nom, tabProgtxt[1][1], inputText, tabProgtxti[2][0], inputTexti)
            if (editor.newP.cont[i].cond.length > 0) {
              j3pAddContent(tabCont[0][2], '&nbsp;Mess. d\'erreur:&nbsp;')
              const tabErr = addDefaultTable(tabCont[1][2], 3, 1)
              switch (editor.newP.cont[i].cond[0].t) {
                case 'ap':
                  met2bout(tabDej, editor.newP.cont[i].cond[0].ki.nom, editor.newP.cont[i].cond[0].ki.nom, tabProgtxt[1][1], inputText, tabProgtxti[2][0], inputTexti)
                  j3pAddContent(tabErr[0][0], 'Condition 1: appartenance&nbsp;')
                  met1bout(tabErr[1][0], editor.newP.cont[i].cond[0], tabErr[2][0], [{ ki: editor.newP.cont[i].cond[0].ki.nom, quoi: editor.newP.cont[i].cond[0].ki.nom }, { ki: editor.newP.cont[i].nom, quoi: editor.newP.cont[i].nom }])
                  break
                case 'long':
                  met2bout(tabDej, '§' + contLong + '§', 'Lg1_' + editor.newP.cont[i].nom, tabProgtxt[1][1], inputText, tabProgtxti[2][0], inputTexti)
                  met2bout(tabDej, editor.newP.cont[i].cond[0].cop, editor.newP.cont[i].cond[0].cop, tabProgtxt[1][1], inputText, tabProgtxti[2][0], inputTexti)
                  j3pAddContent(tabErr[0][0], 'Condition 1: longueur&nbsp;')
                  met1bout(tabErr[1][0], editor.newP.cont[i].cond[0], tabErr[2][0], [{ ki: '§' + contLong + '§', quoi: 'Lg1_' + editor.newP.cont[i].nom }, { ki: editor.newP.cont[i].cond[0].cop, quoi: editor.newP.cont[i].cond[0].cop }, { ki: editor.newP.cont[i].nom, quoi: editor.newP.cont[i].nom }])
                  contLong++
                  break
                case 'ang':
                  met2bout(tabDej, 'µ0µ', 'Nom_Ang1_' + editor.newP.cont[i].nom, tabProgtxt[1][1], inputText, tabProgtxti[2][0], inputTexti)
                  met2bout(tabDej, '§' + contLong + '§', 'Mes_Ang1_' + editor.newP.cont[i].nom, tabProgtxt[1][1], inputText, tabProgtxti[2][0], inputTexti)
                  j3pAddContent(tabErr[0][0], 'Condition 1: angle&nbsp;')
                  met1bout(tabErr[1][0], editor.newP.cont[i].cond[0], tabErr[2][0], [{ ki: 'µ0µ', quoi: 'Nom_Ang1_' + editor.newP.cont[i].nom }, { ki: '§' + contLong + '§', quoi: 'Mes_Ang1_' + editor.newP.cont[i].nom }, { ki: editor.newP.cont[i].nom, quoi: editor.newP.cont[i].nom }])
                  contLong++
                  break
                case 'sur':
                  met2bout(tabDej, '¨¨', 'Pt_Col_' + editor.newP.cont[i].nom, tabProgtxt[1][1], inputText, tabProgtxti[2][0], inputTexti)
                  j3pAddContent(tabErr[0][0], 'Condition 1: sur point&nbsp;')
                  met1bout(tabErr[1][0], editor.newP.cont[i].cond[0], tabErr[2][0], [{ ki: '¨¨', quoi: 'Pt_Col_' + editor.newP.cont[i].nom }, { ki: editor.newP.cont[i].nom, quoi: editor.newP.cont[i].nom }])
                  break
              }
            }
            if (editor.newP.cont[i].cond.length > 1) {
              const tabErr = addDefaultTable(tabCont[1][2], 3, 1)
              switch (editor.newP.cont[i].cond[1].t) {
                case 'ap':
                  met2bout(tabDej, editor.newP.cont[i].cond[1].cont1, editor.newP.cont[i].cond[1].cont1, tabProgtxt[1][1], inputText, tabProgtxti[2][0], inputTexti)
                  j3pAddContent(tabErr[0][0], 'Condition 2: appartenance&nbsp;')
                  met1bout(tabErr[1][0], editor.newP.cont[i].cond[1], tabErr[2][0], [{ ki: editor.newP.cont[i].cond[1].ki.nom, quoi: editor.newP.cont[i].cond[1].ki.nom }, { ki: editor.newP.cont[i].nom, quoi: editor.newP.cont[i].nom }])
                  break
                case 'long':
                  met2bout(tabDej, '§' + contLong + '§', 'Lg2_' + editor.newP.cont[i].nom, tabProgtxt[1][1], inputText, tabProgtxti[2][0], inputTexti)
                  met2bout(tabDej, editor.newP.cont[i].cond[1].cop, editor.newP.cont[i].cond[1].cop, tabProgtxt[1][1], inputText, tabProgtxti[2][0], inputTexti)
                  j3pAddContent(tabErr[0][0], 'Condition 2: longueur&nbsp;')
                  met1bout(tabErr[1][0], editor.newP.cont[i].cond[1], tabErr[2][0], [{ ki: '§' + contLong + '§', quoi: 'Lg2_' + editor.newP.cont[i].nom }, { ki: editor.newP.cont[i].cond[1].cop, quoi: editor.newP.cont[i].cond[1].cop }, { ki: editor.newP.cont[i].nom, quoi: editor.newP.cont[i].nom }])
                  contLong++
                  break
                case 'ang':
                  met2bout(tabDej, 'µ1µ', 'Nom_Ang2_' + editor.newP.cont[i].nom, tabProgtxt[1][1], inputText, tabProgtxti[2][0], inputTexti)
                  met2bout(tabDej, '§' + contLong + '§', 'Mes_Ang2_' + editor.newP.cont[i].nom, tabProgtxt[1][1], inputText, tabProgtxti[2][0], inputTexti)
                  j3pAddContent(tabErr[0][0], 'Condition 2: angle&nbsp;')
                  met1bout(tabErr[1][0], editor.newP.cont[i].cond[1], tabErr[2][0], [{ ki: 'µ0µ', quoi: 'Nom_Ang2_' + editor.newP.cont[i].nom }, { ki: '§' + contLong + '§', quoi: 'Mes_Ang2_' + editor.newP.cont[i].nom }, { ki: editor.newP.cont[i].nom, quoi: editor.newP.cont[i].nom }])
                  contLong++
                  break
              }
            }
            break
          case 'droite': {
            j3pAddContent(tabCont[0][2], '&nbsp;Mess. d\'erreur:&nbsp;')
            const tabErr = addDefaultTable(tabCont[1][2], 3, 1)
            switch (editor.newP.cont[i].condDroite) {
              case '2 points':
                met2bout(tabDej, editor.newP.cont[i].pointExt1, editor.newP.cont[i].pointExt1, tabProgtxt[1][1], inputText, tabProgtxti[2][0], inputTexti)
                met2bout(tabDej, editor.newP.cont[i].pointExt2, editor.newP.cont[i].pointExt2, tabProgtxt[1][1], inputText, tabProgtxti[2][0], inputTexti)
                if (editor.newP.cont[i].nomDroiteP && editor.newP.cont[i].nomDroiteP !== '') {
                  met2bout(tabDej, editor.newP.cont[i].nomDroiteP, editor.newP.cont[i].nomDroiteP, tabProgtxt[1][1], inputText, tabProgtxti[2][0], inputTexti)
                }
                j3pAddContent(tabErr[0][0], 'Condition 2 points')
                break
              case 'parallèle':
              case 'perpendiculaire':
                met2bout(tabDej, editor.newP.cont[i].nomDroiteP, editor.newP.cont[i].nomDroiteP, tabProgtxt[1][1], inputText, tabProgtxti[2][0], inputTexti)
                met2bout(tabDej, editor.newP.cont[i].pointExt1, editor.newP.cont[i].pointExt1, tabProgtxt[1][1], inputText, tabProgtxti[2][0], inputTexti)
                met2bout(tabDej, editor.newP.cont[i].odt, editor.newP.cont[i].odt, tabProgtxt[1][1], inputText, tabProgtxti[2][0], inputTexti)
                j3pAddContent(tabErr[0][0], 'Condition ' + editor.newP.cont[i].condDroite)
            }
            met1bout(tabErr[1][0], editor.newP.cont[i], tabErr[2][0], [{ ki: editor.newP.cont[i].pointExt1, quoi: editor.newP.cont[i].pointExt1 }, { ki: editor.newP.cont[i].nom, quoi: editor.newP.cont[i].nom }])
            met2bout(tabDej, editor.newP.cont[i].nom, editor.newP.cont[i].nom, tabProgtxt[1][1], inputText, tabProgtxti[2][0], inputTexti)
          }
            break
          case 'cercle':
            met2bout(tabDej, editor.newP.cont[i].nom, editor.newP.cont[i].nom, tabProgtxt[1][1], inputText, tabProgtxti[2][0], inputTexti)
            met2bout(tabDej, editor.newP.cont[i].centre, 'centre de ' + editor.newP.cont[i].nom, tabProgtxt[1][1], inputText, tabProgtxti[2][0], inputTexti)
            if (editor.newP.cont[i].mod === 'rayon') {
              met2bout(tabDej, editor.newP.cont[i].rayon, 'rayon de ' + editor.newP.cont[i].nom, tabProgtxt[1][1], inputText, tabProgtxti[2][0], inputTexti)
            } else {
              met2bout(tabDej, editor.newP.cont[i].par, 'point de ' + editor.newP.cont[i].nom, tabProgtxt[1][1], inputText, tabProgtxti[2][0], inputTexti)
            }
            break
          case 'segment':
            met2bout(tabDej, editor.newP.cont[i].pointExt1, editor.newP.cont[i].pointExt1, tabProgtxt[1][1], inputText, tabProgtxti[2][0], inputTexti)
            met2bout(tabDej, editor.newP.cont[i].pointExt2, editor.newP.cont[i].pointExt2, tabProgtxt[1][1], inputText, tabProgtxti[2][0], inputTexti)
            break
          case 'demidroite':
            switch (editor.newP.cont[i].condDemidroite) {
              case '2 points':
                met2bout(tabDej, editor.newP.cont[i].pointDdroite, editor.newP.cont[i].pointDdroite, tabProgtxt[1][1], inputText, tabProgtxti[2][0], inputTexti)
                met2bout(tabDej, editor.newP.cont[i].origine, editor.newP.cont[i].origine, tabProgtxt[1][1], inputText, tabProgtxti[2][0], inputTexti)
                break
              case 'Angle':
                met2bout(tabDej, editor.newP.cont[i].origine, editor.newP.cont[i].origine, tabProgtxt[1][1], inputText, tabProgtxti[2][0], inputTexti)
                met2bout(tabDej, editor.newP.cont[i].angleDD, 'angle demidroite', tabProgtxt[1][1], inputText, tabProgtxti[2][0], inputTexti)
            }
            break
        }
      }
      j3pAjouteBouton(tabProgEl[editor.newP.cont.length + 2][0], valideComb, { value: 'Valider' })
      tabProgEl[editor.newP.cont.length + 2][0].style.background = '#fff'
      tabProgEl[editor.newP.cont.length + 2][0].style.border = '1px solid black'
      tabProgEl[editor.newP.cont.length + 2][0].style.padding = '5px'
    }
    function separeConst () {
      const apush = j3pClone(editor.newP.cont)
      editor.Programme.splice(editor.liProg, 1)
      for (let i = apush.length - 1; i > -1; i--) {
        editor.Programme.splice(editor.liProg, 0, apush[i])
      }
      for (let hj = editor.editAVire.length - 1; hj > -1; hj--) {
        editor.editAVire[hj].disable()
        editor.editAVire.pop()
      }
      for (let hj = editor.editAVire2.length - 1; hj > -1; hj--) {
        editor.editAVire2[hj].disable()
        editor.editAVire2.pop()
      }
      j3pEmpty(editor.lesdiv.modifProg)
      remetCool()
      afficheMenu()
    }
    function met2bout (tabDej, ki, quoi, ou1, zone1, ou2, zone2) {
      if (ki !== undefined) {
        j3pAjouteBouton(ou2, () => {
          insert(derecupe(ki), zone2)
        }, { value: editor.progConst.recupe(quoi) })
        j3pAddContent(ou2, ' ')
      }
      if (ki !== undefined) {
        if (tabDej.indexOf(ki) === -1) {
          j3pAjouteBouton(ou1, () => {
            insert(derecupe(ki), zone1)
          }, { value: editor.progConst.recupe(quoi) })
          j3pAddContent(ou1, ' ')
        }
      }
      if (tabDej.indexOf(ki) === -1) tabDej.push(ki)
    }
    function valideComb () {
      if (!editor.CnewComb) {
        editor.Programme[editor.liProg] = j3pClone(editor.newP)
      } else {
        for (let i = editor.lesCombines.length - 1; i > -1; i--) {
          editor.Programme.splice(editor.lesCombines[i], 1)
        }
        editor.Programme.splice(editor.liProg, 0, editor.newP)
      }
      editor.lesCombines = []
      remetCool()
      afficheMenu()
    }
    function makeSupPrig (i) {
      return () => {
        if (editor.blop) return
        const duprogdep = retrouvelesDependantsde(editor.Programme[i].nom, editor.Programme[i].type, i)
        for (let k = duprogdep.length - 1; k > 0; k--) {
          editor.Programme.splice(duprogdep[k], 1)
        }
        editor.Programme.splice(i, 1)
        afficheMenu()
      }
    }
    function met1bout (ou, tabCond, ou2, tabkoi) {
      const inputText = j3pAddElt(ou, 'input', '', { value: tabCond.err || '' })
      inputText.addEventListener('change', () => {
        tabCond.err = inputText.value
      })
      inputText.addEventListener('blur', () => {
        tabCond.err = inputText.value
      })
      for (let i = 0; i < tabkoi.length; i++) {
        j3pAjouteBouton(ou2, () => {
          insert(derecupe(tabkoi[i].ki), inputText)
        }, { value: editor.progConst.recupe(tabkoi[i].quoi) })
        j3pAddContent(ou2, ' ')
      }
    }
    function makeModPrig (i) {
      return () => {
        if (editor.blop) return
        editor.newProg = j3pClone(editor.Programme[i])
        editor.tabafficheMenu[i][0].style.background = '#f00'
        editor.tabafficheMenu[i][1].style.background = '#f00'
        editor.liProg = i
        switch (editor.newProg.type) {
          case 'point':
            if (editor.newProg.cond.length > 0) {
              const laCond = editor.newProg.cond[0]
              switch (laCond.t) {
                case 'ap':
                  editor.newProg.cond1 = 'Sur objet'
                  editor.newProg.cont1 = laCond.ki.nom
                  editor.newProg.type1 = laCond.ki.type
                  editor.newProg.errcond1Sur = laCond.err
                  break
                case 'long':
                  editor.newProg.cond1 = 'Longueur'
                  editor.newProg.errcond1Sur = laCond.err
                  editor.newProg.pointlong1 = laCond.cop
                  editor.newProg.long1 = laCond.formule.replace(/$/g, '')
                  editor.newProg.long1Type = (laCond.affiche === 'valeur') ? 'cm' : 'formule'
                  break
                case 'ang':
                  editor.newProg.cond1 = 'Angle'
                  editor.newProg.errcond1Sur = laCond.err
                  editor.newProg.point11 = laCond.cop
                  editor.newProg.point12 = laCond.cop2
                  editor.newProg.mezangle = laCond.formule
                  break
                case 'sur':
                  editor.newProg.cond1 = 'Sur point'
                  editor.newProg.errcond1Sur = laCond.err
                  editor.newProg.pointsur1 = laCond.cop
              }
            }
            if (editor.newProg.cond.length > 1) {
              const laCond = editor.newProg.cond[1]
              switch (laCond.t) {
                case 'ap':
                  editor.newProg.cond2 = 'Sur objet'
                  editor.newProg.errcond2Sur = laCond.err
                  editor.newProg.cont2 = laCond.ki.nom
                  editor.newProg.type2 = laCond.ki.type
                  editor.newProg.errcond2Sur = laCond.ki.err
                  break
                case 'long':
                  editor.newProg.cond2 = 'Longueur'
                  editor.newProg.errcond2Sur = laCond.err
                  editor.newProg.pointlong2 = laCond.cop
                  editor.newProg.long2 = laCond.formule.replace(/$/g, '')
                  editor.newProg.long2Type = (laCond.affiche === 'valeur') ? 'cm' : 'formule'
                  break
                case 'ang':
                  editor.newProg.cond2 = 'Angle'
                  editor.newProg.errcond2Sur = laCond.err
                  editor.newProg.point21 = laCond.cop
                  editor.newProg.point22 = laCond.cop2
                  editor.newProg.mezangle2 = laCond.formule
                  break
              }
            }
            editor.newProg.pointprox = editor.newProg.prox || editor.newProg.noprox
            if (editor.newProg.prox) {
              editor.newProg.prox = 'plus'
            }
            if (editor.newProg.noprox) {
              editor.newProg.prox = 'moins'
            }
            break
          case 'droite':
            switch (editor.newProg.condd) {
              case 'pass':
                editor.newProg.pointExt1 = editor.newProg.pass
                editor.newProg.pointExt2 = editor.newProg.pass2
                if (editor.newProg.nom === '(' + editor.newProg.pass + editor.newProg.pass2 + ')') {
                  editor.newProg.nomDroite = ''
                } else {
                  editor.newProg.nomDroite = editor.newProg.nom
                }
                editor.newProg.condDroite = '2 points'
                break
              case 'parallèle':
                editor.newProg.nomDroiteP = editor.newProg.nom
                editor.newProg.condDroite = 'parallèle'
                editor.newProg.pointExt1 = editor.newProg.pass
                break
              case 'perpendiculaire':
                editor.newProg.nomDroiteP = editor.newProg.nom
                editor.newProg.condDroite = 'perpendiculaire'
                editor.newProg.pointExt1 = editor.newProg.pass
            }
            break
          case 'cercle':
            editor.newProg.nomCercle = editor.newProg.nom
            editor.newProg.centreCercle = editor.newProg.centre
            if (editor.newProg.mode === 'rayon') {
              editor.newProg.condCercle = 'centre & rayon'
              editor.newProg.longCercle = editor.newProg.rayon.replace(/$/g, '')
              editor.newProg.longCercleType = (editor.newProg.affiche === 'valeur') ? 'cm' : 'formule'
            } else {
              editor.newProg.condCercle = 'centre & point'
              editor.newProg.pointCercle = editor.newProg.par
            }
            break
          case 'demidroite':
            editor.newProg.condDemidroite = editor.newProg.condDef
            editor.newProg.pointDdroite = editor.newProg.point
            if (editor.newProg.condDemidroite !== '2 points') {
              editor.newProg.nomDdte = editor.newProg.nom
              editor.newProg.angleDD = editor.newProg.angle
            }
            break
          case 'multi':
            editor.newP = j3pClone(editor.Programme[i])
            editor.CnewComb = false
            faisCombonine2()
            return
          case 'efface':
            editor.newProg.elements = j3pClone(editor.newProg.ki)
        }
        modifProg()
      }
    }

    function ajPrg () {
      if (editor.blop) return
      editor.newProg = { text: ' ' }
      editor.liProg = -1
      modifProg()
    }
    function annuleConst () {
      for (let hj = editor.editAVire.length - 1; hj > -1; hj--) {
        editor.editAVire[hj].disable()
        editor.editAVire.pop()
      }
      for (let hj = editor.editAVire2.length - 1; hj > -1; hj--) {
        editor.editAVire2[hj].disable()
        editor.editAVire2.pop()
      }
      j3pEmpty(editor.lesdiv.modifProg)
      remetCool()
      afficheMenu()
    }
    function chCool1 () {
      editor.blop = true
      editor.lesdiv.divUp.style.background = '#3b3d3d'
      editor.lesdiv.div1.style.background = '#3b3d3d'
      editor.lesdiv.div3.style.backgroundImage = 'linear-gradient(to right, rgba(50, 50, 50, 0.8) 30%, rgba(80, 80, 80, 0.8))'
      editor.lesdiv.div4.style.backgroundImage = 'linear-gradient(to right, rgba(50, 50, 50, 0.8) 30%, rgba(80, 80, 80, 0.8))'
    }
    function chCool2 () {
      editor.blop = true
      editor.lesdiv.divUp.style.background = '#3b3d3d'
      editor.lesdiv.div1.style.background = '#3b3d3d'
      editor.lesdiv.div2.style.backgroundImage = 'linear-gradient(to right, rgba(50, 50, 50, 0.8) 30%, rgba(80, 80, 80, 0.8))'
      editor.lesdiv.div4.style.backgroundImage = 'linear-gradient(to right, rgba(50, 50, 50, 0.8) 30%, rgba(80, 80, 80, 0.8))'
    }
    function modifProg () {
      j3pEmpty(editor.lesdiv.modifProg)
      for (let i = 0; i < editor.listeBoutABlok.length; i++) {
        editor.listeBoutABlok[i].style.display = 'none'
      }
      editor.editAVire = []
      editor.editAVire2 = []
      chCool2()
      razDiv4()
      const tabProgEl = addDefaultTable(editor.lesdiv.modifProg, 7, 3)
      tabProgEl[0][0].style.border = '1px solid black'
      tabProgEl[1][0].style.border = '1px solid black'
      tabProgEl[1][0].style.background = '#fff'
      tabProgEl[1][0].style.paddingBottom = '10px'
      tabProgEl[1][0].style.paddingLeft = '10px'
      tabProgEl[1][0].style.paddingRight = '10px'
      tabProgEl[0][0].style.background = '#fff'
      editor.tabProgEl = tabProgEl
      tabProgEl[0][0].style.paddingBottom = '5px'
      tabProgEl[0][0].style.paddingLeft = '10px'
      tabProgEl[2][0].style.paddingLeft = '10px'
      tabProgEl[3][0].style.paddingLeft = '10px'
      tabProgEl[4][0].style.paddingLeft = '10px'
      tabProgEl[5][0].style.paddingLeft = '10px'
      tabProgEl[6][0].style.paddingLeft = '10px'
      const tititre = (editor.liProg === -1) ? '<b>Nouvelle constuction</b>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;' : '<b>Modifier une constuction</b>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;<br><i>( La modification entraine du nom entrainera automatiquement<br> la suppression des constructions suivantes )</i>'
      j3pAddContent(tabProgEl[0][0], tititre)
      j3pAjouteBouton(tabProgEl[0][0], annuleConst, { value: 'Annuler' })
      const tabType = addDefaultTable(tabProgEl[1][0], 1, 7)
      j3pAddContent(tabType[0][0], '<b>Type</b>:&nbsp;')
      const tabTypeVal = []
      function ajType (str) {
        tabTypeVal.push(j3pAddElt(tabType[0][tabTypeVal.length + 1], 'button'))
        tabTypeVal[tabTypeVal.length - 1].val = str
        tabTypeVal[tabTypeVal.length - 1].cont = tabType[0][tabTypeVal.length]
        j3pAddTxt(tabTypeVal[tabTypeVal.length - 1], str)
        tabType[0][tabTypeVal.length].style.paddingBottom = '5px'
        tabType[0][tabTypeVal.length].style.paddingLeft = '5px'
        tabType[0][tabTypeVal.length].style.paddingRight = '5px'
        if (editor.newProg.type === str) tabTypeVal[tabTypeVal.length - 1].cont.style.background = '#f00'
      }
      ajType('point')
      ajType('segment')
      ajType('droite')
      ajType('cercle')
      ajType('demidroite')
      ajType('efface')
      for (let i = 0; i < tabTypeVal.length; i++) {
        tabTypeVal[i].addEventListener('click', () => {
          for (let j = 0; j < tabTypeVal.length; j++) {
            tabTypeVal[j].cont.style.background = ''
          }
          tabTypeVal[i].cont.style.background = '#f00'
          editor.newProg.type = tabTypeVal[i].val
          for (let hj = editor.editAVire.length - 1; hj > -1; hj--) {
            editor.editAVire[hj].disable()
            editor.editAVire.pop()
          }
          for (let hj = editor.editAVire2.length - 1; hj > -1; hj--) {
            editor.editAVire2[hj].disable()
            editor.editAVire2.pop()
          }
          affSelonType()
        })
      }
      if (!editor.newProg.type) {
        tabTypeVal[0].cont.style.background = '#f00'
        editor.newProg.type = 'point'
      }
      affSelonType()
      const tabVal = addDefaultTable(tabProgEl[6][0], 1, 3)
      j3pAddContent(tabVal[0][1], '&nbsp;')
      j3pAjouteBouton(tabVal[0][0], vrifValidNew, { value: 'Valider' })
      tabProgEl[6][0].style.paddingBottom = '5px'
      editor.ouafficheprob = tabVal[0][2]
      editor.ouafficheprob.style.color = '#f00'
      for (let i = 2; i < editor.tabProgEl.length; i++) {
        editor.tabProgEl[i][0].style.border = '1px solid black'
        editor.tabProgEl[i][0].style.background = '#fff'
        editor.tabProgEl[i][0].style.visibility = 'visible'
      }
    }
    function yadejacenom (n) {
      for (let i = 0; i < editor.progfig.length; i++) {
        if (editor.progfig[i].nom === n) {
          return true
        }
      }
      for (let i = 0; i < editor.Programme.length; i++) {
        if (editor.Programme[i].nom === n) {
          return true
        }
      }
      return false
    }
    function vrifValidNew () {
      let prob = ''
      switch (editor.newProg.type) {
        case 'point':
          switch (editor.newProg.cond1) {
            case 'Sur objet':
              if (!editor.newProg.cont1) {
                prob = 'Il faut donner le contenant de la condition 1'
              }
              break
            case 'Longueur':
              if (!editor.newProg.long1) {
                prob = 'Il faut donner la longueur de la condition 1'
              }
              if (!editor.newProg.pointlong1) {
                prob = 'Il faut donner le point de départ pour la longueur de la condition 1'
              }
              break
            case 'Angle':
              if (!editor.newProg.point11) {
                prob = 'Il manque 1 point pour définir l’angle de la condition 1'
              }
              if (!editor.newProg.point12) {
                prob = 'Il manque 1 point pour définir l’angle de la condition 1'
              }
              if (editor.newProg.point11 === editor.newProg.point12) {
                prob = 'If faut deux points différents pour l’angle de la condition 1'
              }
              if (!editor.newProg.mezangle || editor.newProg.mezangle === '') {
                prob = 'Il faut indiquer une mesure pour l’angle de la condition 1'
              }
              break
            case 'Sur point':
              if (!editor.newProg.pointsur1) {
                prob = 'Il faut donner le point de la condition 1'
              }
              break
            case 'Aucune':
              if (!editor.newProg.zone) {
                prob = 'Il faut indiquer une zone pour le placement<br>lors de la correction.'
              }
          }
          switch (editor.newProg.cond2) {
            case 'Sur objet':
              if (!editor.newProg.cont2) {
                prob = 'Il faut donner le contenant de la condition 2'
              }
              break
            case 'Longueur':
              if (!editor.newProg.long2) {
                prob = 'Il faut donner la longueur de la condition 2'
              }
              if (!editor.newProg.pointlong2) {
                prob = 'Il faut donner le point de départ pour la longueur de la condition 2'
              }
              if (editor.newProg.cond1 === 'Longueur') {
                if (editor.newProg.pointlong2 === editor.newProg.pointlong1) {
                  prob = 'Les points de départ pour les longueur des conditions doivent être différents'
                }
              }
              break
            case 'Angle':
              if (!editor.newProg.point21) {
                prob = 'Il manque 1 point pour définir l’angle de la condition 2'
              }
              if (!editor.newProg.point22) {
                prob = 'Il manque 1 point pour définir l’angle de la condition 2'
              }
              if (editor.newProg.point21 === editor.newProg.point22) {
                prob = 'If faut deux points différents pour l’angle de la condition 2'
              }
              if (editor.newProg.cond1 === 'Angle') {
                if (editor.newProg.point21 === editor.newProg.point11) {
                  prob = 'Les sommets pour les angles des conditions doivent être différents'
                }
              }
              if (!editor.newProg.mezangle2 || editor.newProg.mezangle2 === '') {
                prob = 'Il faut indiquer une mesure pour l’angle de la condition 2'
              }
              break
          }
          break
        case 'segment':
          if (!editor.newProg.pointExt1) prob = 'Il faut donner les deux extrémités du segment'
          if (!editor.newProg.pointExt2) prob = 'Il faut donner les deux extrémités du segment'
          if (prob === '' && editor.newProg.pointExt2 === editor.newProg.pointExt1) prob = 'Les deux extrémités doivent être distinctes'
          break
        case 'droite':
          switch (editor.newProg.condDroite) {
            case '2 points':
              if (!editor.newProg.pointExt1) prob = 'Il faut donner deux points pour la droite'
              if (!editor.newProg.pointExt2) prob = 'Il faut donner deux points pour la droite'
              if (prob === '' && editor.newProg.pointExt2 === editor.newProg.pointExt1) prob = 'Il faut donner deux points distincts'
              break
            case 'parallèle':
            case 'perpendiculaire':
              if (!editor.newProg.pointExt1) prob = 'Il faut donner un point pour cette droite'
              if (!editor.newProg.odt) prob = 'Il faut donner une direction de référence pour cette droite'
              if (!editor.newProg.nomDroiteP) prob = 'Il faut donner un nom à cette droite'
          }
          break
        case 'cercle':
          if (!editor.newProg.nomCercle) prob = 'Il faut donner un nom à ce cercle'
          if (!editor.newProg.centreCercle) prob = 'Il faut donner un centre à ce cercle'
          if (editor.newProg.condCercle === 'centre & rayon') {
            if (!editor.newProg.longCercle || editor.newProg.longCercle === '') {
              prob = 'Il faut donner une longueur à ce cercle'
            }
          } else {
            if (!editor.newProg.pointCercle) prob = 'Il faut donner un point à ce cercle'
            if (prob === '' && editor.newProg.pointCercle === editor.newProg.centreCercle) prob = 'Il faut donner deux points différents'
          }
          break
        case 'demidroite':
          switch (editor.newProg.condDemidroite) {
            case '2 points':
              if (!editor.newProg.pointDdroite) prob = 'Il manque un point pour définir cette demidroite'
              if (!editor.newProg.origine) prob = 'Il manque un point pour définir cette demidroite'
              if (editor.newProg.pointDdroite === editor.newProg.origine) prob = 'Il faut deux points distincts pour définir cette demidroite'
              break
            case 'Angle':
              if (!editor.newProg.origine) prob = 'Il manque un point pour définir cette demidroite'
              if (!editor.newProg.pointDdroite) prob = 'Il manque un point pour définir cette demidroite'
              if (!editor.newProg.nomDdte) prob = 'Il faut un nom pour cette demi-droite'
          }
          break
        case 'efface':
          if (editor.newProg.ki.length < 1) prob = 'Il faut indiquer les éléments à supprimer'
          for (let i = 0; i < editor.newProg.ki.length - 1; i++) {
            const aComp1 = editor.newProg.ki[i].nom
            for (let j = i + 1; j < editor.newProg.ki.length; j++) {
              const aComp2 = editor.newProg.ki[j].nom
              if (aComp1 === aComp2) prob = 'Le même élément est indiqué plusieurs fois'
            }
          }
          break
      }
      if (prob === '') {
        const apush = { type: editor.newProg.type, text: editor.newProg.text }
        switch (editor.newProg.type) {
          case 'point':
            apush.nom = editor.newProg.nom
            apush.cond = []
            switch (editor.newProg.cond1) {
              case 'Sur objet': {
                const type = retrouveType(derecupe(editor.newProg.cont1))
                const err = (editor.newProg.errcond1Sur === '') ? undefined : editor.newProg.errcond1Sur
                const ki = { type, nom: derecupe(editor.newProg.cont1) }
                const apushCond = { t: 'ap', ki, err }
                apush.cond.push(apushCond)
              }
                break
              case 'Longueur': {
                const aaf = (editor.newProg.long1Type === 'cm') ? editor.newProg.long1 : editor.newProg.long1
                const err = (editor.newProg.errcond1Sur === '') ? undefined : editor.newProg.errcond1Sur
                const apushCond = { t: 'long', cop: editor.newProg.pointlong1, formule: aaf, affiche: (editor.newProg.long1Type === 'cm') ? 'valeur' : 'formule', err }
                apush.cond.push(apushCond)
              }
                break
              case 'Angle': {
                const err = (editor.newProg.errcond1Sur === '') ? undefined : editor.newProg.errcond1Sur
                const apushCond = { t: 'ang', cop: editor.newProg.point11, cop2: editor.newProg.point12, formule: editor.newProg.mezangle, affiche: 'valeur', err }
                apush.cond.push(apushCond)
              }
                break
              case 'Sur point': {
                const err = (editor.newProg.errcond1Sur === '') ? undefined : editor.newProg.errcond1Sur
                apush.cond.push({ t: 'sur', cop: editor.newProg.pointsur1, err })
              }
                break
              case 'Aucune':
                apush.zone = editor.newProg.zone
            }
            switch (editor.newProg.cond2) {
              case 'Sur objet': {
                const err = (editor.newProg.errcond2Sur === '') ? undefined : editor.newProg.errcond2Sur
                const type = retrouveType(derecupe(editor.newProg.cont2))
                const ki = { type, nom: derecupe(editor.newProg.cont2) }
                const apushCond = { t: 'ap', ki, err }
                apush.cond.push(apushCond)
              }
                break
              case 'Longueur': {
                const err = (editor.newProg.errcond2Sur === '') ? undefined : editor.newProg.errcond2Sur
                const aaf = (editor.newProg.long2Type === 'cm') ? editor.newProg.long2 : editor.newProg.long2
                const apushCond = { t: 'long', cop: editor.newProg.pointlong2, formule: aaf, affiche: (editor.newProg.long2Type === 'cm') ? 'valeur' : 'formule', err }
                apush.cond.push(apushCond)
              }
                break
              case 'Angle': {
                const err = (editor.newProg.errcond2Sur === '') ? undefined : editor.newProg.errcond2Sur
                const apushCond = { t: 'ang', cop: editor.newProg.point21, cop2: editor.newProg.point22, formule: editor.newProg.mezangle2, affiche: 'valeur', err }
                apush.cond.push(apushCond)
              }
                break
            }
            switch (editor.newProg.cond1) {
              case 'Sur objet':
              case 'Angle':
              case 'Longueur':
                switch (editor.newProg.cond2) {
                  case 'Sur objet':
                  case 'Angle':
                  case 'Longueur':
                    if (editor.newProg.prox === 'plus') {
                      apush.prox = editor.newProg.pointprox
                    } else {
                      apush.noprox = editor.newProg.pointprox
                    }
                }
            }
            break
          case 'segment':
            apush.nom = '[' + editor.newProg.pointExt1 + editor.newProg.pointExt2 + ']'
            apush.pointExt1 = editor.newProg.pointExt1
            apush.pointExt2 = editor.newProg.pointExt2
            break
          case 'droite':
            switch (editor.newProg.condDroite) {
              case '2 points':
                apush.pass = editor.newProg.pointExt1
                apush.pass2 = editor.newProg.pointExt2
                apush.nom = editor.newProg.nomDroite || ''
                if (apush.nom === '') apush.nom = '(' + apush.pass + apush.pass2 + ')'
                apush.condd = 'pass'
                break
              case 'parallèle':
                apush.nom = editor.newProg.nomDroiteP
                apush.condd = 'parallèle'
                apush.pass = editor.newProg.pointExt1
                apush.odt = editor.newProg.odt
                break
              case 'perpendiculaire':
                apush.nom = editor.newProg.nomDroiteP
                apush.condd = 'perpendiculaire'
                apush.pass = editor.newProg.pointExt1
                apush.odt = editor.newProg.odt
            }
            break
          case 'cercle':
            apush.nom = editor.newProg.nomCercle
            apush.centre = editor.newProg.centreCercle
            if (editor.newProg.condCercle === 'centre & rayon') {
              apush.mode = 'rayon'
              apush.rayon = (editor.newProg.longCercleType === 'cm') ? editor.newProg.longCercle : editor.newProg.longCercle
              apush.affiche = (editor.newProg.longCercleType === 'cm') ? 'valeur' : 'formule'
            } else {
              apush.mode = 'passant'
              apush.par = editor.newProg.pointCercle
            }
            break
          case 'demidroite':
            apush.nom = (editor.newProg.condDemidroite === '2 points') ? '[' + editor.newProg.origine + editor.newProg.pointDdroite + ')' : editor.newProg.nomDdte
            apush.origine = editor.newProg.origine
            apush.condDef = editor.newProg.condDemidroite
            apush.point = editor.newProg.pointDdroite
            if (editor.newProg.condDemidroite !== '2 points') {
              apush.angle = editor.newProg.angleDD
            }
            break
          case 'efface':
            apush.nom = ''
            apush.ki = j3pClone(editor.newProg.ki)
        }
        if (editor.liProg === -1) {
          if (apush.nom !== '' && yadejacenom(apush.nom)) {
            j3pAddContent(editor.ouafficheprob, 'Ce nom est déjà utilisé')
            setTimeout(() => { j3pEmpty(editor.ouafficheprob) }, 1500)
            return
          } else {
            editor.Programme.push(apush)
          }
        } else {
          effectueCh(apush, editor.Programme[editor.liProg])
          editor.Programme[editor.liProg] = j3pClone(apush)
        }
        for (let i = editor.editAVire.length - 1; i > -1; i--) {
          editor.editAVire[i].disable()
          editor.editAVire.splice(i, 1)
        }
        for (let i = editor.editAVire2.length - 1; i > -1; i--) {
          editor.editAVire2[i].disable()
          editor.editAVire2.splice(i, 1)
        }
        remetCool()
        afficheMenu()
      } else {
        j3pAddContent(editor.ouafficheprob, prob)
        setTimeout(() => { j3pEmpty(editor.ouafficheprob) }, 1500)
      }
    }
    function effectueCh (lenouv, lold, li) {
      if (lenouv.nom !== lold.nom) {
        retrouveListeProgMod(lenouv.nom, lold.nom, li)
      }
    }
    function retrouveType (n) {
      for (let i = 0; i < editor.progConst.progfig.length; i++) {
        if (editor.progConst.progfig[i].nom === n) return editor.progConst.progfig[i].type
      }
      for (let i = 0; i < editor.Programme.length; i++) {
        if (editor.Programme[i].nom === n) return editor.Programme[i].type
      }
    }
    function ajCondPoint1 (str, tabTypeCond, tabCondTitre, tti) {
      tabTypeCond.push(j3pAddElt(tabCondTitre[0][tabTypeCond.length + 1], 'button'))
      tabTypeCond[tabTypeCond.length - 1].val = str
      tabTypeCond[tabTypeCond.length - 1].cont = tabCondTitre[0][tabTypeCond.length]
      j3pAddTxt(tabTypeCond[tabTypeCond.length - 1], str)
      tabTypeCond[tabTypeCond.length - 1].cont.style.paddingBottom = '5px'
      tabTypeCond[tabTypeCond.length - 1].cont.style.paddingLeft = '5px'
      tabTypeCond[tabTypeCond.length - 1].cont.style.paddingRight = '5px'
      if (tti === str) tabTypeCond[tabTypeCond.length - 1].cont.style.background = '#f00'
    }
    function effaceCondPoint2 () {
      editor.tabProgEl[4][0].style.display = 'none'
      editor.tabProgEl[4][0].parentNode.style.display = 'none'
    }
    function affSelonCondPoint1 (tabCond, bool) {
      if (bool) {
        for (let hj = editor.editAVire.length - 1; hj > -1; hj--) {
          editor.editAVire[hj].disable()
          editor.editAVire.pop()
        }
      }
      for (let hj = editor.editAVire2.length - 1; hj > -1; hj--) {
        editor.editAVire2[hj].disable()
        editor.editAVire2.pop()
      }
      j3pEmpty(tabCond[1][0])
      const condAcomp = (bool) ? editor.newProg.cond1 : editor.newProg.cond2
      switch (condAcomp) {
        case 'Aucune':
          if (bool) {
            const tOu = addDefaultTable(tabCond[1][0], 2, 1)
            const tOu2 = addDefaultTable(tOu[0][0], 1, 2)
            j3pAddContent(tOu2[0][0], '<u>zone</u>:&nbsp;')
            j3pAddContent(tOu[1][0], '<i>Pour le placement du point à la correction.</i>')
            tOu[1][0].style.color = '#258f06'
            const ll = recupeZones()
            ll.splice(0, 0, '...')
            const listeCont1 = ListeDeroulante.create(tOu2[0][1], ll, {
              onChange: () => {
                editor.newProg.zone = listeCont1.reponse
              },
              select: ll.indexOf(editor.newProg.zone)
            })
            editor.editAVire.push(listeCont1)
            if (ll.length === 1) {
              j3pEmpty(tOu[1][0])
              tOu[1][0].style.color = '#F00'
              j3pAddContent(tOu[1][0], 'Il faut au préalable créer une "Zone circulaire" <br> ou une "Zone segment" dans la figure de départ.')
            }
            effaceCondPoint2()
          }
          break
        case 'Sur objet': {
          const tabObj1 = addDefaultTable(tabCond[1][0], 3, 2)
          j3pAddContent(tabObj1[0][0], 'Point sur:&nbsp;')
          const ll = recupeContenants()
          for (let i = 0; i < ll.length; i++) {
            ll[i] = editor.progConst.recupe(ll[i])
          }
          ll.splice(0, 0, '...')
          const listeCont1 = ListeDeroulante.create(tabObj1[0][1], ll, {
            onChange: () => {
              if (bool) {
                editor.newProg.cont1 = derecupe(listeCont1.reponse)
                editor.newProg.type1 = editor.progConst.typeDe(editor.newProg.cont1)
              } else {
                editor.newProg.cont2 = derecupe(listeCont1.reponse)
                editor.newProg.type2 = editor.progConst.typeDe(editor.newProg.cont2)
              }
            },
            select: (bool) ? ll.indexOf(editor.progConst.recupe(editor.newProg.cont1)) : ll.indexOf(editor.progConst.recupe(editor.newProg.cont2))
          })
          j3pAddContent(tabObj1[2][0], '&nbsp;')
          if (bool) {
            editor.tabProgEl[4][0].style.display = ''
            editor.tabProgEl[4][0].parentNode.style.display = ''
            affSelonCondPoint1(editor.tabCond2, false)
          }
          if (bool) {
            editor.editAVire.push(listeCont1)
          } else {
            editor.editAVire2.push(listeCont1)
          }
          const tabObj2 = addDefaultTable(tabCond[1][0], 1, 1)
          affTextErreur(tabObj2[0][0], (bool) ? 'errcond1Sur' : 'errcond2Sur')
          if (ll.length === 1) {
            j3pEmpty(tabObj1[1][0])
            tabObj1[1][0].style.color = '#F00'
            j3pAddContent(tabObj1[1][0], 'Il faut au préalable créer un objet <br> ( une zone, une droite , un cercle , ... ) <br> dans le programme ou la figure de départ.')
          }
        }
          break
        case 'Longueur': {
          const tabLong = addDefaultTable(tabCond[1][0], 5, 1)
          const tabTypeLong = addDefaultTable(tabLong[1][0], 1, 3)
          j3pAddContent(tabTypeLong[0][0], 'Longueur donnée en&nbsp;')
          afficheExplik(tabLong[1][0], 'Longueur donnée en cm:<br><i>La valeur est calculée avant d\'être affichée.</i><br>Lngueur donnée en calcul:<br><i>La formule est affichée telle quelle.</i>')
          function makeLong1Cm () {
            tabTypeLong[0][1].style.background = '#f00'
            tabTypeLong[0][2].style.background = '#fff'
            if (bool) {
              editor.newProg.long1Type = 'cm'
            } else {
              editor.newProg.long2Type = 'cm'
            }
          }
          function makeLong1Form () {
            tabTypeLong[0][1].style.background = '#fff'
            tabTypeLong[0][2].style.background = '#f00'
            if (bool) {
              editor.newProg.long1Type = 'form'
            } else {
              editor.newProg.long2Type = 'form'
            }
          }
          j3pAjouteBouton(tabTypeLong[0][1], makeLong1Cm, { value: 'cm' })
          tabTypeLong[0][1].style.padding = '5px'
          j3pAjouteBouton(tabTypeLong[0][2], makeLong1Form, { value: 'calcul' })
          tabTypeLong[0][2].style.padding = '5px'
          const longAcomp = (bool) ? editor.newProg.long1Type : editor.newProg.long2Type
          if (longAcomp === 'cm') {
            makeLong1Cm()
          } else {
            makeLong1Form()
          }
          const tabformule = addDefaultTable(tabLong[2][0], 1, 3)
          j3pAddContent(tabformule[0][0], 'Longueur&nbsp;')
          const inputForm = new ZoneStyleMathquill2(tabformule[0][1], {
            restric: '0123456789,/+-*()à',
            contenu: (bool) ? editor.progConst.recupe(editor.newProg.long1) : editor.progConst.recupe(editor.newProg.long2)
          })
          j3pAddContent(tabformule[0][2], '&nbsp;<i>( Utilisez <b>à</b> pour une valeur aléatoire comprise entre 0 et 1. )</i>')
          tabformule[0][2].style.color = '#085d03'
          inputForm.onchange = () => {
            if (bool) {
              editor.newProg.long1 = derecupe(inputForm.reponse())
            } else {
              editor.newProg.long2 = derecupe(inputForm.reponse())
            }
          }
          if (bool) {
            editor.editAVire.push(inputForm)
          } else {
            editor.editAVire2.push(inputForm)
          }
          const lesPoint = recupePointsTotal((editor.liProg === -1) ? editor.Programme.length : editor.liProg)
          for (let i = 0; i < lesPoint.length; i++) {
            for (let j = i + 1; j < lesPoint.length; j++) {
              j3pAjouteBouton(tabLong[3][0], () => {
                inputForm.majaffiche(lesPoint[i] + lesPoint[j])
                setTimeout(function () { inputForm.focus() }, 10)
              }, { value: lesPoint[i] + lesPoint[j] })
            }
          }
          const lesPoint2 = j3pClone(lesPoint)
          lesPoint2.splice(0, 0, '...')
          const tabPointDep = addDefaultTable(tabLong[0][0], 2, 2)
          j3pAddContent(tabPointDep[0][0], 'Depuis le point :&nbsp;')
          const listeCont1 = ListeDeroulante.create(tabPointDep[0][1], lesPoint2, {
            onChange: () => {
              if (bool) { editor.newProg.pointlong1 = derecupe(listeCont1.reponse) } else {
                editor.newProg.pointlong2 = derecupe(listeCont1.reponse)
              }
            },
            select: (bool) ? lesPoint2.indexOf(editor.progConst.recupe(editor.newProg.pointlong1)) : lesPoint2.indexOf(editor.progConst.recupe(editor.newProg.pointlong2))
          })
          if (lesPoint2.length === 1) {
            j3pEmpty(tabPointDep[1][0])
            tabPointDep[1][0].style.color = '#F00'
            j3pAddContent(tabPointDep[1][0], 'Il faut au préalable créer un point <br>  dans le programme ou la figure de départ.')
          }
          if (bool) {
            editor.editAVire.push(listeCont1)
          } else {
            editor.editAVire2.push(listeCont1)
          }
          j3pAddContent(tabLong[4][0], '&nbsp;')
          if (bool) {
            editor.tabProgEl[4][0].style.display = ''
            editor.tabProgEl[4][0].parentNode.style.display = ''
            affSelonCondPoint1(editor.tabCond2, false)
          }
          const tabObj2 = addDefaultTable(tabCond[1][0], 1, 1)
          affTextErreur(tabObj2[0][0], (bool) ? 'errcond1Sur' : 'errcond2Sur')
        }
          break
        case 'Angle': {
          const tabAng = addDefaultTable(tabCond[1][0], 4, 1)
          const tabcentre = addDefaultTable(tabAng[0][0], 1, 2)
          const tabAutre = addDefaultTable(tabAng[1][0], 1, 2)
          const tabMez = addDefaultTable(tabAng[3][0], 1, 3)
          j3pAddContent(tabcentre[0][0], 'sommet:&nbsp;')
          j3pAddContent(tabAutre[0][0], 'autre point:&nbsp;')
          j3pAddContent(tabMez[0][0], 'mesure =&nbsp;')
          j3pAddContent(tabMez[0][2], '&nbsp;°')
          j3pAddContent(tabAng[3][0], '&nbsp;')
          const ll = recupePointsTotal((editor.liProg === -1) ? editor.Programme.length : editor.liProg)
          ll.splice(0, 0, '...')
          const listeCont1 = ListeDeroulante.create(tabcentre[0][1], ll, {
            onChange: () => {
              if (bool) { editor.newProg.point11 = derecupe(listeCont1.reponse) } else {
                editor.newProg.point21 = derecupe(listeCont1.reponse)
              }
            },
            select: (bool) ? ll.indexOf(editor.progConst.recupe(editor.newProg.point11)) : ll.indexOf(editor.progConst.recupe(editor.newProg.point21))
          })
          if (bool) {
            editor.editAVire.push(listeCont1)
          } else {
            editor.editAVire2.push(listeCont1)
          }
          const listeCont2 = ListeDeroulante.create(tabAutre[0][1], ll, {
            onChange: () => {
              if (bool) { editor.newProg.point12 = derecupe(listeCont2.reponse) } else {
                editor.newProg.point22 = derecupe(listeCont2.reponse)
              }
            },
            select: (bool) ? ll.indexOf(editor.progConst.recupe(editor.newProg.point12)) : ll.indexOf(editor.progConst.recupe(editor.newProg.point22))
          })
          if (bool) {
            editor.editAVire.push(listeCont2)
            editor.newProg.mezangle = String(editor.newProg.mezangle || 90)
          } else {
            editor.editAVire2.push(listeCont2)
            editor.newProg.mezangle2 = String(editor.newProg.mezangle2 || 90)
          }
          if (ll.length < 3) {
            tabAng[3][0].style.color = '#F00'
            j3pAddContent(tabAng[3][0], 'Il faut au préalable créer 2 points <br>dans le programme ou la figure de départ.')
          } else {
            afficheExplik(tabAng[3][0], '<b>sommet</b> pour le sommet de l\'angle,<br> <b>autre point</b> pour déterminer le premier côté de l\'angle.')
          }
          const mezAngle = j3pAddElt(tabMez[0][1], 'input', '', {
            type: 'number',
            value: (bool) ? editor.newProg.mezangle : editor.newProg.mezangle2,
            size: 4,
            min: '1',
            max: '180'
          })
          mezAngle.addEventListener('change', () => {
            if (bool) {
              editor.newProg.mezangle = String(mezAngle.value)
            } else {
              editor.newProg.mezangle2 = String(mezAngle.value)
            }
          })
          if (bool) {
            editor.tabProgEl[4][0].style.display = ''
            editor.tabProgEl[4][0].parentNode.style.display = ''
            affSelonCondPoint1(editor.tabCond2, false)
          }
          const tabObj2 = addDefaultTable(tabCond[1][0], 1, 1)
          affTextErreur(tabObj2[0][0], (bool) ? 'errcond1Sur' : 'errcond2Sur')
        }
          break
        case 'Sur point': {
          const tabObj1 = addDefaultTable(tabCond[1][0], 2, 2)
          j3pAddContent(tabObj1[0][0], 'sur le Point:&nbsp;')
          const ll = recupePointsTotal(-1)
          ll.splice(0, 0, '...')
          const listeCont1 = ListeDeroulante.create(tabObj1[0][1], ll, {
            onChange: () => {
              editor.newProg.pointsur1 = derecupe(listeCont1.reponse)
            },
            select: ll.indexOf(editor.progConst.recupe(editor.newProg.pointsur1))
          })
          if (ll.length < 2) {
            tabObj1[1][1].style.color = '#F00'
            j3pAddContent(tabObj1[1][1], 'Il faut au préalable créer 1 point <br>dans la figure de départ.<br><i>Ne pas oubliez de le rendre non visible.</i>')
          } else {
            afficheExplik(tabObj1[1][1], 'Pensez à rendre ce point non visible.')
          }
          editor.editAVire.push(listeCont1)
          effaceCondPoint2()
          const tabObj2 = addDefaultTable(tabCond[1][0], 1, 1)
          affTextErreur(tabObj2[0][0], 'errcond1Sur')
        }
      }
      if (!bool) {
        switch (editor.newProg.cond1) {
          case 'Sur objet':
          case 'Angle':
          case 'Longueur':
            switch (editor.newProg.cond2) {
              case 'Sur objet':
              case 'Angle':
              case 'Longueur':
                ajProx(tabCond)
            }
        }
      }
      affTextPoint()
    }
    function ajProx (tab) {
      j3pEmpty(tab[2][0])
      const tabProx = addDefaultTable(tab[2][0], 2, 1)
      const tab1 = addDefaultTable(tabProx[0][0], 1, 4)
      j3pAddContent(tab1[0][0], '<i>optionnel</i> seul le point le&nbsp;')
      const ll = ['plus', 'moins']
      const listeCont1 = ListeDeroulante.create(tab1[0][1], ll, {
        onChange: () => {
          editor.newProg.prox = listeCont1.reponse
        },
        select: ll.indexOf(editor.newProg.prox),
        choix0: true
      })
      j3pAddContent(tab1[0][2], '&nbsp;proche du point&nbsp;')
      const ll2 = recupePointsTotal((editor.liProg === -1) ? editor.Programme.length : editor.liProg)
      ll2.splice(0, 0, '...')
      const listeCont2 = ListeDeroulante.create(tab1[0][3], ll2, {
        onChange: () => {
          editor.newProg.pointprox = derecupe(listeCont2.reponse)
        },
        select: ll2.indexOf(editor.progConst.recupe(editor.newProg.pointprox))
      })
      editor.editAVire2.push(listeCont1, listeCont2)
      j3pAddContent(tabProx[1][0], 'Si aucun point n’est précisé, plusieurs réponses peuvent être autorisées.')
    }
    function insert (tagType, zoneText) {
      zoneText.focus()
      const startSelection = zoneText.value.substring(0, zoneText.selectionStart)
      const currentSelection = zoneText.value.substring(zoneText.selectionStart)
      zoneText.value = startSelection + tagType + currentSelection
      zoneText.focus()
    }
    function affSelonType () {
      for (let i = 2; i < editor.tabProgEl.length - 1; i++) {
        j3pEmpty(editor.tabProgEl[i][0])
      }
      switch (editor.newProg.type) {
        case 'point': {
          editor.tabProgEl[3][0].style.display = ''
          editor.tabProgEl[3][0].parentNode.style.display = ''
          editor.tabProgEl[4][0].style.display = ''
          editor.tabProgEl[4][0].parentNode.style.display = ''
          const tabNom = addDefaultTable(editor.tabProgEl[2][0], 2, 2)
          const yapasdenom = editor.newProg.nom === undefined
          editor.newProg.nom = editor.newProg.nom || trouveNewHaz()
          const aaj = (!yapasdenom) ? editor.progConst.recupe(editor.newProg.nom) : editor.nomPosProg[editor.nomPosProg.length - 1]
          j3pAddContent(tabNom[0][0], '<b>Nom:</b>&nbsp;' + aaj)
          const tabCond = addDefaultTable(editor.tabProgEl[3][0], 3, 1)
          const tabCondTitre = addDefaultTable(tabCond[0][0], 1, 6)
          j3pAddContent(tabCondTitre[0][0], '<b>Condition 1:</b>&nbsp;')
          const tabCond2 = addDefaultTable(editor.tabProgEl[4][0], 3, 1)
          const tabCondTitre2 = addDefaultTable(tabCond2[0][0], 1, 6)
          editor.tabCond2 = tabCond2
          j3pAddContent(tabCondTitre2[0][0], '<b>Condition 2:</b>&nbsp;')
          const tabTypeCond = []
          const tabTypeCond2 = []
          ajCondPoint1('Aucune', tabTypeCond, tabCondTitre, editor.newProg.cond1)
          ajCondPoint1('Sur objet', tabTypeCond, tabCondTitre, editor.newProg.cond1)
          ajCondPoint1('Longueur', tabTypeCond, tabCondTitre, editor.newProg.cond1)
          ajCondPoint1('Angle', tabTypeCond, tabCondTitre, editor.newProg.cond1)
          ajCondPoint1('Sur point', tabTypeCond, tabCondTitre, editor.newProg.cond1)
          ajCondPoint1('Aucune', tabTypeCond2, tabCondTitre2, editor.newProg.cond2)
          ajCondPoint1('Sur objet', tabTypeCond2, tabCondTitre2, editor.newProg.cond2)
          ajCondPoint1('Longueur', tabTypeCond2, tabCondTitre2, editor.newProg.cond2)
          ajCondPoint1('Angle', tabTypeCond2, tabCondTitre2, editor.newProg.cond2)
          effaceCondPoint2()
          for (let i = 0; i < tabTypeCond.length; i++) {
            tabTypeCond[i].addEventListener('click', () => {
              for (let j = 0; j < tabTypeCond.length; j++) {
                tabTypeCond[j].cont.style.background = ''
              }
              tabTypeCond[i].cont.style.background = '#f00'
              editor.newProg.cond1 = tabTypeCond[i].val
              affSelonCondPoint1(tabCond, true)
            })
          }
          for (let i = 0; i < tabTypeCond2.length; i++) {
            tabTypeCond2[i].addEventListener('click', () => {
              for (let j = 0; j < tabTypeCond2.length; j++) {
                tabTypeCond2[j].cont.style.background = ''
              }
              tabTypeCond2[i].cont.style.background = '#f00'
              editor.newProg.cond2 = tabTypeCond2[i].val
              affSelonCondPoint1(tabCond2, false)
            })
          }
          if (!editor.newProg.cond1) {
            tabTypeCond[0].cont.style.background = '#f00'
            editor.newProg.cond1 = 'Aucune'
          }
          if (!editor.newProg.cond2) {
            tabTypeCond2[0].cont.style.background = '#f00'
            editor.newProg.cond2 = 'Aucune'
          }
          affSelonCondPoint1(tabCond, true)
        }
          break
        case 'segment': {
          editor.tabProgEl[3][0].style.display = 'none'
          editor.tabProgEl[3][0].parentNode.style.display = 'none'
          editor.tabProgEl[4][0].style.display = 'none'
          editor.tabProgEl[4][0].parentNode.style.display = 'none'
          j3pAddContent(editor.tabProgEl[2][0], '<br>')
          const tabSeg = addDefaultTable(editor.tabProgEl[2][0], 2, 7)
          j3pAddContent(tabSeg[0][0], '<b>extrémités :</b>&nbsp;')
          const ll = recupePointsTotal((editor.liProg === -1) ? editor.Programme.length : editor.liProg)
          ll.splice(0, 0, '...')
          const listeCont1 = ListeDeroulante.create(tabSeg[0][2], ll, {
            onChange: () => {
              editor.newProg.pointExt1 = derecupe(listeCont1.reponse)
            },
            select: ll.indexOf(editor.progConst.recupe(editor.newProg.pointExt1))
          })
          j3pAddContent(tabSeg[0][3], '&nbsp;et&nbsp;')
          const listeCont2 = ListeDeroulante.create(tabSeg[0][4], ll, {
            onChange: () => {
              editor.newProg.pointExt2 = derecupe(listeCont2.reponse)
            },
            select: ll.indexOf(editor.progConst.recupe(editor.newProg.pointExt2))
          })
          editor.editAVire.push(listeCont1, listeCont2)
          j3pAddContent(tabSeg[1][0], '&nbsp;')
          if (editor.newProg.pointExt1 !== undefined && editor.newProg.pointExt2 !== undefined) {
            editor.newProg.nom = '[' + editor.newProg.pointExt1 + editor.newProg.pointExt2 + ']'
          } else {
            editor.newProg.nom = undefined
          }
          affTextPoint()
          if (ll.length < 3) {
            const tabSegErr = addDefaultTable(editor.tabProgEl[2][0], 1, 1)
            tabSegErr[0][0].style.color = '#F00'
            j3pAddContent(tabSegErr[0][0], 'Il faut au préalable créer 2 points <br>dans le programme ou la figure de départ.')
          }
        }
          break
        case 'droite': {
          editor.tabProgEl[3][0].style.display = ''
          editor.tabProgEl[3][0].parentNode.style.display = ''
          editor.tabProgEl[4][0].style.display = ''
          editor.tabProgEl[4][0].parentNode.style.display = ''
          const tabDef = addDefaultTable(editor.tabProgEl[2][0], 2, 4)
          j3pAddContent(tabDef[0][0], '<b>définie par :&nbsp</b>')
          const tabTypeCond = []
          ajCondPoint1('2 points', tabTypeCond, tabDef)
          ajCondPoint1('parallèle', tabTypeCond, tabDef)
          ajCondPoint1('perpendiculaire', tabTypeCond, tabDef)
          for (let i = 0; i < tabTypeCond.length; i++) {
            tabTypeCond[i].addEventListener('click', () => {
              for (let j = 0; j < tabTypeCond.length; j++) {
                tabTypeCond[j].cont.style.background = ''
              }
              tabTypeCond[i].cont.style.background = '#f00'
              editor.newProg.condDroite = tabTypeCond[i].val
              affDroite()
            })
          }
          if (!editor.newProg.condDroite || editor.newProg.condDroite === '2 points') {
            tabTypeCond[0].cont.style.background = '#f00'
            editor.newProg.condDroite = '2 points'
          }
          if (editor.newProg.condDroite === 'parallèle') {
            tabTypeCond[1].cont.style.background = '#f00'
          }
          if (editor.newProg.condDroite === 'perpendiculaire') {
            tabTypeCond[2].cont.style.background = '#f00'
          }
          if (editor.newProg.nom !== undefined) {
            if (editor.newProg.nom.indexOf('Hz') !== -1) editor.newProg.nom = undefined
          }
          affDroite()
          affTextPoint()
        }
          break
        case 'cercle': {
          editor.tabProgEl[3][0].style.display = ''
          editor.tabProgEl[3][0].parentNode.style.display = ''
          editor.tabProgEl[4][0].style.display = ''
          editor.tabProgEl[4][0].parentNode.style.display = ''
          const tabDef = addDefaultTable(editor.tabProgEl[2][0], 2, 4)
          j3pAddContent(tabDef[0][0], '<b>défini par :&nbsp</b>')
          const tabTypeCond = []
          ajCondPoint1('centre & point', tabTypeCond, tabDef)
          ajCondPoint1('centre & rayon', tabTypeCond, tabDef)
          for (let i = 0; i < tabTypeCond.length; i++) {
            tabTypeCond[i].addEventListener('click', () => {
              for (let j = 0; j < tabTypeCond.length; j++) {
                tabTypeCond[j].cont.style.background = ''
              }
              tabTypeCond[i].cont.style.background = '#f00'
              editor.newProg.condCercle = tabTypeCond[i].val
              affCercle()
            })
          }
          if (!editor.newProg.condCercle || editor.newProg.condCercle === 'centre & point') {
            tabTypeCond[0].cont.style.background = '#f00'
            editor.newProg.condCercle = 'centre & point'
          } else {
            tabTypeCond[1].cont.style.background = '#f00'
            editor.newProg.condCercle = 'centre & rayon'
          }
          affCercle()
          if (editor.newProg.nom !== undefined) {
            if (editor.newProg.nom.indexOf('Hz') !== -1) editor.newProg.nom = undefined
          }
          affTextPoint()
        }
          break
        case 'demidroite': {
          editor.tabProgEl[3][0].style.display = ''
          editor.tabProgEl[3][0].parentNode.style.display = ''
          const tabDef = addDefaultTable(editor.tabProgEl[2][0], 2, 4)
          j3pAddContent(tabDef[0][0], '<b>définie par :&nbsp</b>')
          const tabTypeCond = []
          ajCondPoint1('2 points', tabTypeCond, tabDef)
          ajCondPoint1('Angle', tabTypeCond, tabDef)
          for (let i = 0; i < tabTypeCond.length; i++) {
            tabTypeCond[i].addEventListener('click', () => {
              for (let j = 0; j < tabTypeCond.length; j++) {
                tabTypeCond[j].cont.style.background = ''
              }
              tabTypeCond[i].cont.style.background = '#f00'
              editor.newProg.condDemidroite = tabTypeCond[i].val
              affDemiDroite()
            })
          }
          if (!editor.newProg.condDemidroite || editor.newProg.condDemidroite === '2 points') {
            tabTypeCond[0].cont.style.background = '#f00'
            editor.newProg.condDemidroite = '2 points'
          } else {
            tabTypeCond[1].cont.style.background = '#f00'
            editor.newProg.condDemidroite = 'Angle'
          }
          affDemiDroite()
          if (editor.newProg.nom !== undefined) {
            if (editor.newProg.nom.indexOf('Hz') !== -1) editor.newProg.nom = undefined
          }
          affTextPoint()
        }
          break
        case 'efface': {
          editor.tabProgEl[3][0].style.display = 'none'
          editor.tabProgEl[3][0].parentNode.style.display = 'none'
          editor.tabProgEl[4][0].style.display = 'none'
          editor.tabProgEl[4][0].parentNode.style.display = 'none'
          j3pAddContent(editor.tabProgEl[2][0], '<br>')
          const tabSeg = addDefaultTable(editor.tabProgEl[2][0], 1, 2)
          j3pAddContent(tabSeg[0][0], '<b>Eléments à effacer :</b>&nbsp;')
          if (!Array.isArray(editor.newProg.ki)) {
            editor.newProg.ki = []
          }
          affListKi()
          affTextPoint()
        }
      }
    }
    function affListKi () {
      j3pEmpty(editor.tabProgEl[2][0])
      const tabEl = addDefaultTable(editor.tabProgEl[2][0], editor.newProg.ki.length + 2, 1)
      j3pAddContent(tabEl[0][0], '<u><b>Elément(s) à effacer</b></u>')
      for (let i = 0; i < editor.newProg.ki.length; i++) {
        const tabEll = addDefaultTable(tabEl[i + 1][0], 1, 2)
        j3pAddContent(tabEll[0][0], '&nbsp;' + editor.newProg.ki[i].type + ' ' + editor.progConst.recupe(editor.newProg.ki[i].nom) + '&nbsp;')
        j3pAjouteBouton(tabEll[0][1], () => {
          editor.newProg.ki.splice(i, 1)
          affListKi()
        }, { value: 'Suppr' })
      }
      const tabSeg = addDefaultTable(tabEl[editor.newProg.ki.length + 1][0], 3, 1)
      j3pAddContent(tabSeg[0][0], '&nbsp;')
      j3pAddContent(tabSeg[2][0], '&nbsp;')
      const tabSegAj = addDefaultTable(tabSeg[1][0], 1, 3)
      const ll = recupeLescrea((editor.liProg === -1) ? editor.Programme.length : editor.liProg)
      ll.splice(0, 0, '...')
      const listeCont1 = ListeDeroulante.create(tabSegAj[0][0], ll)
      j3pAddContent(tabSegAj[0][1], '&nbsp;')
      j3pAjouteBouton(tabSegAj[0][2], () => {
        if (listeCont1.reponse === '...') return
        editor.newProg.ki.push({ type: retrouveType(derecupe(listeCont1.reponse)), nom: derecupe(listeCont1.reponse) })
        affListKi()
      }, { value: 'Ajouter' })
    }
    function affCercle () {
      for (let hj = editor.editAVire.length - 1; hj > -1; hj--) {
        editor.editAVire[hj].disable()
        editor.editAVire.pop()
      }
      j3pEmpty(editor.tabProgEl[3][0])
      j3pEmpty(editor.tabProgEl[4][0])
      switch (editor.newProg.condCercle) {
        case 'centre & point': {
          const tabNom = addDefaultTable(editor.tabProgEl[3][0], 2, 2)
          j3pAddContent(tabNom[0][0], '<b>Nom</b>:&nbsp;')
          const inputNom = j3pAddElt(tabNom[0][1], 'input', { value: editor.newProg.nomCercle || '', size: 40 })
          inputNom.addEventListener('change', () => {
            editor.newProg.nomCercle = inputNom.value
          })
          j3pAddContent(tabNom[1][0], '&nbsp;')
          const tabOdtPoint = addDefaultTable(editor.tabProgEl[4][0], 4, 2)
          j3pAddContent(tabOdtPoint[0][0], 'centre :&nbsp;')
          j3pAddContent(tabOdtPoint[1][0], '&nbsp;')
          j3pAddContent(tabOdtPoint[2][0], 'point :&nbsp;')
          j3pAddContent(tabOdtPoint[3][0], '&nbsp;')
          const ll = recupePointsTotal((editor.liProg === -1) ? editor.Programme.length : editor.liProg)
          ll.splice(0, 0, '...')
          const listeCont1 = ListeDeroulante.create(tabOdtPoint[0][1], ll, {
            onChange: () => {
              editor.newProg.centreCercle = derecupe(listeCont1.reponse)
            },
            select: ll.indexOf(editor.progConst.recupe(editor.newProg.centreCercle))
          })
          const listeCont2 = ListeDeroulante.create(tabOdtPoint[2][1], ll, {
            onChange: () => {
              editor.newProg.pointCercle = derecupe(listeCont2.reponse)
            },
            select: ll.indexOf(editor.progConst.recupe(editor.newProg.pointCercle))
          })
          if (ll.length < 3) {
            const tabOdtPointErr = addDefaultTable(editor.tabProgEl[4][0], 1, 1)
            tabOdtPointErr[0][0].style.color = '#F00'
            j3pAddContent(tabOdtPointErr[0][0], 'Il faut au préalable créer 2 points <br>dans le programme ou la figure de départ.')
          }
          editor.editAVire.push(listeCont1, listeCont2)
        }
          break
        case 'centre & rayon': {
          const tabNom = addDefaultTable(editor.tabProgEl[3][0], 4, 2)
          j3pAddContent(tabNom[0][0], '<b>Nom</b>:&nbsp;')
          const inputNom = j3pAddElt(tabNom[0][1], 'input', { value: editor.newProg.nomCercle || '', size: 40 })
          inputNom.addEventListener('change', () => {
            editor.newProg.nomCercle = inputNom.value
          })
          j3pAddContent(tabNom[1][0], '&nbsp;')
          j3pAddContent(tabNom[2][0], 'centre :&nbsp;')
          const ll = recupePointsTotal((editor.liProg === -1) ? editor.Programme.length : editor.liProg)
          ll.splice(0, 0, '...')
          const listeCont1 = ListeDeroulante.create(tabNom[2][1], ll, {
            onChange: () => {
              editor.newProg.centreCercle = derecupe(listeCont1.reponse)
            },
            select: ll.indexOf(editor.progConst.recupe(editor.newProg.centreCercle))
          })
          if (ll.length < 3) {
            const tabOdtPointErr = addDefaultTable(editor.tabProgEl[3][0], 1, 1)
            tabOdtPointErr[0][0].style.color = '#F00'
            j3pAddContent(tabOdtPointErr[0][0], 'Il faut au préalable créer 1 point <br>dans le programme ou la figure de départ.')
          }
          const tabLong = addDefaultTable(editor.tabProgEl[4][0], 5, 1)
          const tabTypeLong = addDefaultTable(tabLong[0][0], 1, 3)
          j3pAddContent(tabTypeLong[0][0], 'Longueur donnée en&nbsp;')
          function makeLong1Cm () {
            tabTypeLong[0][1].style.background = '#f00'
            tabTypeLong[0][2].style.background = '#fff'
            editor.newProg.longCercleType = 'cm'
          }
          function makeLong1Form () {
            tabTypeLong[0][1].style.background = '#fff'
            tabTypeLong[0][2].style.background = '#f00'
            editor.newProg.longCercleType = 'form'
          }
          j3pAjouteBouton(tabTypeLong[0][1], makeLong1Cm, { value: 'cm' })
          tabTypeLong[0][1].style.padding = '5px'
          j3pAjouteBouton(tabTypeLong[0][2], makeLong1Form, { value: 'calcul' })
          tabTypeLong[0][2].style.padding = '5px'
          const longAcomp = editor.newProg.longCercleType
          if (longAcomp === 'cm') {
            makeLong1Cm()
          } else {
            makeLong1Form()
          }
          afficheExplik(tabLong[1][0], 'Longueur donnée en cm:<br><i>La valeur est calculée avant d\'être affichée.</i><br>Lngueur donnée en calcul:<br><i>La formule est affichée telle quelle.</i>')
          const tabformule = addDefaultTable(tabLong[2][0], 1, 3)
          j3pAddContent(tabformule[0][0], 'Longueur&nbsp;')
          const inputForm = new ZoneStyleMathquill2(tabformule[0][1], {
            restric: '0123456789,/+-*()',
            contenu: editor.newProg.longCercle
          })
          inputForm.onchange = () => {
            editor.newProg.longCercle = inputForm.reponse()
          }
          tabformule[0][2].style.color = '#085d03'
          j3pAddContent(tabformule[0][2], '&nbsp;<i>Utilisez <b>à</b> pour une valeur aléatoire comprise entre 0 et 1.</i>')
          editor.editAVire.push(inputForm)
          const lesPoint = recupePointsTotal((editor.liProg === -1) ? editor.Programme.length : editor.liProg)
          for (let i = 0; i < lesPoint.length; i++) {
            for (let j = i + 1; j < lesPoint.length; j++) {
              j3pAjouteBouton(tabLong[2][0], () => {
                inputForm.majaffiche(lesPoint[i] + lesPoint[j])
                setTimeout(function () { inputForm.focus() }, 10)
              }, { value: lesPoint[i] + lesPoint[j] })
            }
          }
        }
          break
      }
    }
    function affDemiDroite () {
      for (let hj = editor.editAVire.length - 1; hj > -1; hj--) {
        editor.editAVire[hj].disable()
        editor.editAVire.pop()
      }
      j3pEmpty(editor.tabProgEl[3][0])
      j3pEmpty(editor.tabProgEl[4][0])
      switch (editor.newProg.condDemidroite) {
        case '2 points': {
          editor.tabProgEl[4][0].style.display = 'none'
          editor.tabProgEl[4][0].parentNode.style.display = 'none'
          const tabOdtPoint = addDefaultTable(editor.tabProgEl[3][0], 4, 2)
          j3pAddContent(tabOdtPoint[0][0], 'origine :&nbsp;')
          j3pAddContent(tabOdtPoint[1][0], '&nbsp;')
          j3pAddContent(tabOdtPoint[2][0], 'point :&nbsp;')
          j3pAddContent(tabOdtPoint[3][0], '&nbsp;')
          const ll = recupePointsTotal((editor.liProg === -1) ? editor.Programme.length : editor.liProg)
          ll.splice(0, 0, '...')
          const listeCont1 = ListeDeroulante.create(tabOdtPoint[0][1], ll, {
            onChange: () => {
              editor.newProg.origine = derecupe(listeCont1.reponse)
            },
            select: ll.indexOf(editor.progConst.recupe(editor.newProg.origine))
          })
          const listeCont2 = ListeDeroulante.create(tabOdtPoint[2][1], ll, {
            onChange: () => {
              editor.newProg.pointDdroite = derecupe(listeCont2.reponse)
            },
            select: ll.indexOf(editor.progConst.recupe(editor.newProg.pointDdroite))
          })
          if (ll.length < 3) {
            const tabOdtPointErr = addDefaultTable(editor.tabProgEl[3][0], 1, 1)
            tabOdtPointErr[0][0].style.color = '#F00'
            j3pAddContent(tabOdtPointErr[0][0], 'Il faut au préalable créer 2 points <br>dans le programme ou la figure de départ.')
          }
          editor.editAVire.push(listeCont1, listeCont2)
        }
          break
        case 'Angle': {
          editor.tabProgEl[4][0].style.display = ''
          editor.tabProgEl[4][0].parentNode.style.display = ''
          const tabNom = addDefaultTable(editor.tabProgEl[3][0], 4, 2)
          j3pAddContent(tabNom[0][0], '<b>Nom</b>:&nbsp;')
          editor.newProg.nomDdte = editor.newProg.nomDdte || ''
          const inputNom = j3pAddElt(tabNom[0][1], 'input', { value: editor.newProg.nomDdte, size: 40 })
          inputNom.addEventListener('change', () => {
            editor.newProg.nomDdte = inputNom.value
          })
          j3pAddContent(tabNom[1][0], '&nbsp;')
          j3pAddContent(tabNom[2][0], 'origine :&nbsp;')
          j3pAddContent(tabNom[3][0], 'point :&nbsp;')
          const ll = recupePointsTotal((editor.liProg === -1) ? editor.Programme.length : editor.liProg)
          ll.splice(0, 0, '...')
          const listeCont1 = ListeDeroulante.create(tabNom[2][1], ll, {
            onChange: () => {
              editor.newProg.origine = derecupe(listeCont1.reponse)
            },
            select: ll.indexOf(editor.progConst.recupe(editor.newProg.origine))
          })
          const listeCont2 = ListeDeroulante.create(tabNom[3][1], ll, {
            onChange: () => {
              editor.newProg.pointDdroite = derecupe(listeCont2.reponse)
            },
            select: ll.indexOf(editor.progConst.recupe(editor.newProg.pointDdroite))
          })
          const tabLong = addDefaultTable(editor.tabProgEl[4][0], 2, 3)
          j3pAddContent(tabLong[0][0], 'Angle :&nbsp;')
          j3pAddContent(tabLong[0][2], '&nbsp;°')
          editor.editAVire.push(listeCont1, listeCont2)
          editor.newProg.angleDD = editor.newProg.angleDD || 90
          const mezAngle = j3pAddElt(tabLong[0][1], 'input', '', {
            type: 'number',
            value: editor.newProg.angleDD,
            size: 4,
            min: '1',
            max: '180'
          })
          mezAngle.addEventListener('change', () => {
            editor.newProg.angleDD = mezAngle.value
          })
          const tabOdtPointErr = addDefaultTable(editor.tabProgEl[4][0], 1, 1)
          if (ll.length < 3) {
            tabOdtPointErr[0][0].style.color = '#F00'
            j3pAddContent(tabOdtPointErr[0][0], 'Il faut au préalable créer 2 points <br>dans le programme ou la figure de départ.')
          } else {
            afficheExplik(tabOdtPointErr[0][0], '<b>origine</b> pour le sommet de l\'angle,<br> <b>point</b> pour déterminer le premier côté de l\'angle.')
          }
        }
          break
      }
    }
    function affDroite () {
      for (let hj = editor.editAVire.length - 1; hj > -1; hj--) {
        editor.editAVire[hj].disable()
        editor.editAVire.pop()
      }
      j3pEmpty(editor.tabProgEl[3][0])
      j3pEmpty(editor.tabProgEl[4][0])
      switch (editor.newProg.condDroite) {
        case '2 points': {
          j3pAddContent(editor.tabProgEl[3][0], '<br>')
          const tabSeg = addDefaultTable(editor.tabProgEl[3][0], 2, 7)
          j3pAddContent(tabSeg[0][0], '<b>Points :</b>&nbsp;')
          const ll = recupePointsTotal((editor.liProg === -1) ? editor.Programme.length : editor.liProg)
          ll.splice(0, 0, '...')
          const listeCont1 = ListeDeroulante.create(tabSeg[0][2], ll, {
            onChange: () => {
              editor.newProg.pointExt1 = derecupe(listeCont1.reponse)
            },
            select: ll.indexOf(editor.progConst.recupe(editor.newProg.pointExt1))
          })
          j3pAddContent(tabSeg[0][3], '&nbsp;et&nbsp;')
          const listeCont2 = ListeDeroulante.create(tabSeg[0][4], ll, {
            onChange: () => {
              editor.newProg.pointExt2 = derecupe(listeCont2.reponse)
            },
            select: ll.indexOf(editor.progConst.recupe(editor.newProg.pointExt2))
          })
          editor.editAVire.push(listeCont1, listeCont2)
          j3pAddContent(tabSeg[1][0], '&nbsp;')
          affTextPoint()
          if (ll.length < 3) {
            const tabSegErr = addDefaultTable(editor.tabProgEl[3][0], 1, 1)
            tabSegErr[0][0].style.color = '#F00'
            j3pAddContent(tabSegErr[0][0], 'Il faut au préalable créer 2 points <br>dans le programme ou la figure de départ.')
          }
          const tabNom = addDefaultTable(editor.tabProgEl[4][0], 2, 2)
          j3pAddContent(tabNom[0][0], '<b>Nom</b>( optionnel ) :&nbsp;')
          const inputNom = j3pAddElt(tabNom[0][1], 'input', { value: editor.newProg.nomDroite || '', size: 40 })
          inputNom.addEventListener('change', () => {
            editor.newProg.nomDroite = inputNom.value
          })
          j3pAddContent(tabNom[1][0], '&nbsp;')
        }
          break
        case 'parallèle': {
          const tabNom = addDefaultTable(editor.tabProgEl[3][0], 2, 2)
          j3pAddContent(tabNom[0][0], '<b>Nom</b>:&nbsp;')
          const inputNom = j3pAddElt(tabNom[0][1], 'input', { value: editor.newProg.nomDroiteP || '', size: 40 })
          inputNom.addEventListener('change', () => {
            editor.newProg.nomDroiteP = inputNom.value
          })
          j3pAddContent(tabNom[1][0], '&nbsp;')
          const tabOdtPoint = addDefaultTable(editor.tabProgEl[4][0], 4, 2)
          j3pAddContent(tabOdtPoint[0][0], 'passant par :&nbsp;')
          j3pAddContent(tabOdtPoint[1][0], '&nbsp;')
          j3pAddContent(tabOdtPoint[2][0], 'parallèle à :&nbsp;')
          j3pAddContent(tabOdtPoint[3][0], '&nbsp;')
          const ll = recupePointsTotal((editor.liProg === -1) ? editor.Programme.length : editor.liProg)
          ll.splice(0, 0, '...')
          const listeCont1 = ListeDeroulante.create(tabOdtPoint[0][1], ll, {
            onChange: () => {
              editor.newProg.pointExt1 = derecupe(listeCont1.reponse)
            },
            select: ll.indexOf(editor.progConst.recupe(editor.newProg.pointExt1))
          })
          if (ll.length < 2) {
            const tabOdtPointErr = addDefaultTable(editor.tabProgEl[4][0], 1, 1)
            tabOdtPointErr[0][0].style.color = '#F00'
            j3pAddContent(tabOdtPointErr[0][0], 'Il faut au préalable créer 1 point <br>dans le programme ou la figure de départ.')
          }
          const ll2 = recupeligneTotal()
          ll2.splice(0, 0, '...')
          const listeCont2 = ListeDeroulante.create(tabOdtPoint[2][1], ll2, {
            onChange: () => {
              editor.newProg.odt = derecupe(listeCont2.reponse)
            },
            select: ll2.indexOf(editor.progConst.recupe(editor.newProg.odt))
          })
          if (ll2.length < 2) {
            const tabOdtPointErr = addDefaultTable(editor.tabProgEl[4][0], 1, 1)
            tabOdtPointErr[0][0].style.color = '#F00'
            j3pAddContent(tabOdtPointErr[0][0], 'Il faut au préalable créer 1 ligne <br>dans le programme ou la figure de départ.')
          }
          editor.editAVire.push(listeCont1, listeCont2)
        }
          break
        case 'perpendiculaire':{
          const tabNom = addDefaultTable(editor.tabProgEl[3][0], 2, 2)
          j3pAddContent(tabNom[0][0], '<b>Nom</b>:&nbsp;')
          const inputNom = j3pAddElt(tabNom[0][1], 'input', { value: editor.newProg.nomDroiteP || '', size: 40 })
          inputNom.addEventListener('change', () => {
            editor.newProg.nomDroiteP = inputNom.value
          })
          j3pAddContent(tabNom[1][0], '&nbsp;')
          const tabOdtPoint = addDefaultTable(editor.tabProgEl[4][0], 4, 2)
          j3pAddContent(tabOdtPoint[0][0], 'passant par :&nbsp;')
          j3pAddContent(tabOdtPoint[1][0], '&nbsp;')
          j3pAddContent(tabOdtPoint[2][0], 'perpendiculaire à:&nbsp;')
          j3pAddContent(tabOdtPoint[3][0], '&nbsp;')
          const ll = recupePointsTotal((editor.liProg === -1) ? editor.Programme.length : editor.liProg)
          ll.splice(0, 0, '...')
          const listeCont1 = ListeDeroulante.create(tabOdtPoint[0][1], ll, {
            onChange: () => {
              editor.newProg.pointExt1 = derecupe(listeCont1.reponse)
            },
            select: ll.indexOf(editor.progConst.recupe(editor.newProg.pointExt1))
          })
          if (ll.length < 2) {
            const tabOdtPointErr = addDefaultTable(editor.tabProgEl[4][0], 1, 1)
            tabOdtPointErr[0][0].style.color = '#F00'
            j3pAddContent(tabOdtPointErr[0][0], 'Il faut au préalable créer 1 point <br>dans le programme ou la figure de départ.')
          }
          const ll2 = recupeligneTotal()
          ll2.splice(0, 0, '...')
          const listeCont2 = ListeDeroulante.create(tabOdtPoint[2][1], ll2, {
            onChange: () => {
              editor.newProg.odt = derecupe(listeCont2.reponse)
            },
            select: ll2.indexOf(editor.progConst.recupe(editor.newProg.odt))
          })
          if (ll2.length < 2) {
            const tabOdtPointErr = addDefaultTable(editor.tabProgEl[4][0], 1, 1)
            tabOdtPointErr[0][0].style.color = '#F00'
            j3pAddContent(tabOdtPointErr[0][0], 'Il faut au préalable créer 1 ligne <br>dans le programme ou la figure de départ.')
          }
          editor.editAVire.push(listeCont1, listeCont2)
        }
      }
    }
    function affTextPoint () {
      j3pEmpty(editor.tabProgEl[5][0])
      const tabText = addDefaultTable(editor.tabProgEl[5][0], 3, 2)
      j3pAddContent(tabText[0][0], '<b>Texte:</b>&nbsp;')
      const zoneText = j3pAddElt(tabText[0][1], 'input', '', { size: 40, value: editor.newProg.text })
      zoneText.addEventListener('change', () => {
        editor.newProg.text = zoneText.value.replace(/'/g, 'ʼ')
      })
      zoneText.addEventListener('blur', () => {
        editor.newProg.text = zoneText.value.replace(/'/g, 'ʼ')
      })
      const lp = recupePointsTotal((editor.liProg === -1) ? editor.Programme.length : editor.liProg)
      if (editor.newProg.nom !== undefined) {
        const koiapush = (editor.newProg.nom === editor.progConst.recupe(editor.newProg.nom)) ? editor.nomPosProg[editor.nomPosProg.length - 1] : editor.progConst.recupe(editor.newProg.nom)
        lp.push(koiapush)
      }
      for (let i = 0; i < lp.length; i++) {
        j3pAjouteBouton(tabText[1][1], () => {
          insert(derecupe(lp[i], true), zoneText)
        }, { value: lp[i] })
        j3pAddContent(tabText[1][1], ' ')
      }
      if (editor.newProg.type === 'point') {
        if (editor.newProg.cond1 === 'Longueur') {
          j3pAjouteBouton(tabText[1][1], () => {
            insert('§0§', zoneText)
          }, { value: 'Lg_1' })
          j3pAddContent(tabText[1][1], ' ')
        }
        if (editor.newProg.cond2 === 'Longueur') {
          j3pAjouteBouton(tabText[1][1], () => {
            insert('§1§', zoneText)
          }, { value: 'Lg_2' })
          j3pAddContent(tabText[1][1], ' ')
        }
        if (editor.newProg.cond1 === 'Angle') {
          j3pAjouteBouton(tabText[1][1], () => {
            insert('§0§', zoneText)
          }, { value: 'Mes. Ang cond1' })
          j3pAddContent(tabText[1][1], ' ')
          j3pAjouteBouton(tabText[1][1], () => {
            insert('µ0µ', zoneText)
          }, { value: 'Nom Ang cond1' })
          j3pAddContent(tabText[1][1], ' ')
        }
        if (editor.newProg.cond2 === 'Angle') {
          j3pAjouteBouton(tabText[1][1], () => {
            insert('§1§', zoneText)
          }, { value: 'Mes. Ang cond2' })
          j3pAddContent(tabText[1][1], ' ')
          j3pAjouteBouton(tabText[1][1], () => {
            insert('µ1µ', zoneText)
          }, { value: 'Nom Ang cond2' })
          j3pAddContent(tabText[1][1], ' ')
        }
      }
      afficheExplik(tabText[2][1], 'Les noms des points aléaltoires sont de la forme <b>Hz1</b> , <b>Hz2</b>, ...<br> Utilisez les boutons pour retrouver leur nom aléatoire.<br> <i>Ils seront remplacés par les nouveaux noms dans l\'exercice.</i>')
    }
    function affTextErreur (ou, ki) {
      j3pEmpty(ou)
      const tabText = addDefaultTable(ou, 3, 2)
      j3pAddContent(tabText[0][0], '<b>Message d’erreur:</b>&nbsp;')
      const zoneText = j3pAddElt(tabText[0][1], 'input', '', { size: 40, value: editor.newProg[ki] || '' })
      zoneText.addEventListener('change', () => {
        editor.newProg[ki] = zoneText.value.replace(/'/g, 'ʼ')
      })
      zoneText.addEventListener('blur', () => {
        editor.newProg[ki] = zoneText.value.replace(/'/g, 'ʼ')
      })
      const lp = recupePointsTotal((editor.liProg === -1) ? editor.Programme.length : editor.liProg)
      const koiapush = (editor.newProg.nom === editor.progConst.recupe(editor.newProg.nom)) ? editor.nomPosProg[editor.nomPosProg.length - 1] : editor.progConst.recupe(editor.newProg.nom)
      lp.push(koiapush)
      for (let i = 0; i < lp.length; i++) {
        j3pAjouteBouton(tabText[1][1], () => {
          insert(derecupe(lp[i], true), zoneText)
          editor.newProg[ki] = zoneText.value
        }, { value: lp[i] })
        j3pAddContent(tabText[1][1], ' ')
      }
      if (editor.newProg.type === 'point') {
        if (editor.newProg.cond1 === 'Longueur') {
          j3pAjouteBouton(tabText[1][1], () => {
            insert('§0§', zoneText)
            editor.newProg[ki] = zoneText.value
          }, { value: 'Lg_1' })
          j3pAddContent(tabText[1][1], ' ')
        }
        if (editor.newProg.cond2 === 'Longueur') {
          j3pAjouteBouton(tabText[1][1], () => {
            insert('§1§', zoneText)
            editor.newProg[ki] = zoneText.value
          }, { value: 'Lg_2' })
          j3pAddContent(tabText[1][1], ' ')
        }
        if (editor.newProg.cond1 === 'Angle') {
          j3pAjouteBouton(tabText[1][1], () => {
            insert('§0§', zoneText)
            editor.newProg[ki] = zoneText.value
          }, { value: 'Mes_Ang_1' })
          j3pAddContent(tabText[1][1], ' ')
          j3pAjouteBouton(tabText[1][1], () => {
            insert('µ0µ', zoneText)
            editor.newProg[ki] = zoneText.value
          }, { value: 'Nom_Ang_1' })
          j3pAddContent(tabText[1][1], ' ')
        }
        if (editor.newProg.cond2 === 'Angle') {
          j3pAjouteBouton(tabText[1][1], () => {
            insert('§1§', zoneText)
            editor.newProg[ki] = zoneText.value
          }, { value: 'Mes_Ang_2' })
          j3pAddContent(tabText[1][1], ' ')
          j3pAjouteBouton(tabText[1][1], () => {
            insert('µ1µ', zoneText)
            editor.newProg[ki] = zoneText.value
          }, { value: 'Nom_Ang_2' })
          j3pAddContent(tabText[1][1], ' ')
        }
      }
      j3pAddContent(tabText[2][0], '&nbsp;')
    }
    function faisChoixExos (i) {
      editor.numExo = i
      editor.lenom = editor.ProgrammeLT[i].nom
      editor.progConst = new ProgConst(ds, editor, 'FigEditor')
      editor.progConst.ds = j3pClone(values)
      editor.Programme = j3pClone(editor.ProgrammeLT[i].prog)
      editor.progfig = j3pClone(editor.ProgrammeLT[i].fig)
      editor.yaaacentre = editor.ProgrammeLT[i].yac
      editor.yaaaarot = editor.ProgrammeLT[i].yar
      editor.yaaaazom = editor.ProgrammeLT[i].yaz
      editor.zoomin = editor.ProgrammeLT[i].zoomin
      editor.zoomax = editor.ProgrammeLT[i].zoomax
      editor.limcercle = editor.ProgrammeLT[i].limcercle
      editor.limseg = editor.ProgrammeLT[i].nbseg
      editor.limdte = editor.ProgrammeLT[i].nbdte
      editor.limddte = editor.ProgrammeLT[i].nbddte
      editor.limarc = editor.ProgrammeLT[i].nbarc
      editor.yatrait = editor.ProgrammeLT[i].nbAt
    }
    function choixExos () {
      for (let i = 0; i < editor.plo.length; i++) {
        editor.plo[i].disable()
      }
      editor.plo = []
      editor.devmov = false
      editor.ptCo = []
      editor.segCo = []
      editor.demiDco = []
      editor.arcco = []
      editor.dteco = []
      editor.clignEff = []
      editor.nomco = []
      editor.onco = false
      editor.progConst.nbseg = 0
      editor.nbdte = -1
      editor.nbddte = -1
      editor.nbarc = -1
      /**
       * @typedef NomPris
       * @type Object
       * @property {string} ki C’est quoi ?
       * @property {Object} com C’est quoi ?
       */
      /**
       * La liste des noms déjà pris dans la figure
       * @type {NomPris[]}
       */
      editor.progConst.nomspris = []
      editor.progConst.nomsprisD = []
      editor.condVerif = []
      editor.existeVerif = []
      editor.longcalculs = []
      editor.progConst.nomDebSeg = []
      editor.progConst.nomDebDte = []
      editor.hasard2 = []
      reNomRecupe()
      const tb = []
      for (let a = 0; a < editor.progfig.length; a++) {
        if (editor.progfig[a].type === 'point') {
          tb.push({ n: editor.progfig[a].nom, x: parseFloat(editor.progfig[a].cox), y: parseFloat(editor.progfig[a].coy) })
        }
      }
      editor.progConst.unite = 27.33576561109424
      editor.progConst.uniteOld = 27.33576561109424
      for (let i = 0; i < tb.length - 1; i++) {
        for (let j = i + 1; j < tb.length; j++) {
          editor.longcalculs.push({
            ki: tb[i].n + tb[j].n,
            form: editor.progConst.dist({ x: tb[i].x, y: tb[i].y }, { x: tb[j].x, y: tb[j].y }) / editor.progConst.unite
          })
        }
      }
      if (editor.yaaaazom) {
        editor.modiggg = (j3pGetRandomInt(editor.zoomin * 100, editor.zoomax * 100) / 100)
      } else {
        editor.modiggg = 1
      }
      for (let i = 0; i < editor.longcalculs.length; i++) {
        editor.longcalculs[i].form = j3pArrondi(editor.longcalculs[i].form * editor.modiggg, 1)
      }
      editor.listeSegPris = []
      editor.listePointPris = []
      editor.listeDtePris = []
      editor.listeDemiDPris = []
      editor.listeArcPris = []
      editor.listeZonePris = []
      editor.listeZonePrisSeg = []
      editor.progConst.progfig = editor.progfig
      editor.progConst.listeSegPris = []
      editor.progConst.listePointPris = []
      editor.progConst.listeDtePris = []
      editor.progConst.listeDemiDPris = []
      editor.progConst.listeArcPris = []
      editor.progConst.ptCo = []
      editor.progConst.segCo = []
      editor.progConst.demiDco = []
      editor.progConst.arcco = []
      editor.progConst.dteco = []
      editor.progConst.clignEff = []
      editor.progConst.nomco = []
      editor.progConst.nomDebDte = []

      editor.zoom = 1
      if (editor.mondivBUlle !== undefined) {
        j3pDetruit(editor.mondivBUlle)
        editor.mondivBUlle = undefined
      }
      editor.listeFonc = []
    }
    function choixExos2 () {
      const artebuf = []
      let mu, om
      for (let i = 0; i < editor.plo.length; i++) {
        editor.plo[i].disable()
      }
      editor.plo = []
      editor.progConst2.devmov = false
      editor.progConst2.ptCo = []
      editor.progConst2.segCo = []
      editor.progConst2.demiDco = []
      editor.progConst2.arcco = []
      editor.progConst2.dteco = []
      editor.progConst2.clignEff = []
      editor.progConst2.nomco = []
      editor.progConst2.okmouv = true
      editor.progConst2.onco = false
      editor.progConst2.nbseg = 0
      editor.progConst2.nbdte = -1
      editor.progConst2.nbddte = -1
      editor.progConst2.nbarc = -1
      /**
       * @typedef NomPris
       * @type Object
       * @property {string} ki C’est quoi ?
       * @property {Object} com C’est quoi ?
       */
      /**
       * La liste des noms déjà pris dans la figure
       * @type {NomPris[]}
       */
      editor.progConst2.nomspris = []
      editor.progConst2.me = { etat: 'correction', styles: { cfaux: { color: '#fff' } } }
      editor.progConst2.nomsprisD = []
      editor.condVerif = []
      editor.existeVerif = []
      editor.longcalculs = []
      editor.progConst2.nomDebSeg = []
      editor.progConst2.nomDebDte = []
      editor.progConst2.nbAt = editor.cocheTrait.checked
      let a, b, text, bud, haz
      editor.hasard2 = []
      editor.MonMenu = []
      reNomRecupe2()
      const tb = []
      for (let a = 0; a < editor.progfig.length; a++) {
        if (editor.progfig[a].type === 'point') {
          tb.push({ n: editor.progfig[a].nom, x: parseFloat(editor.progfig[a].cox), y: parseFloat(editor.progfig[a].coy) })
        }
      }
      editor.progConst.unite = 27.33576561109424
      for (let i = 0; i < tb.length - 1; i++) {
        for (let j = i + 1; j < tb.length; j++) {
          editor.longcalculs.push({
            ki: tb[i].n + tb[j].n,
            form: editor.progConst2.dist({ x: tb[i].x, y: tb[i].y }, { x: tb[j].x, y: tb[j].y }) / editor.progConst.unite
          })
          editor.longcalculs.push({
            ki: tb[j].n + tb[i].n,
            form: editor.progConst2.dist({ x: tb[i].x, y: tb[i].y }, { x: tb[j].x, y: tb[j].y }) / editor.progConst.unite
          })
        }
      }

      let nombuf, laval
      for (a = 0; a < editor.Programme.length; a++) {
        if (editor.Programme[a].type !== 'multi') {
          if (editor.Programme[a].type === 'segment') editor.progConst2.nbseg++
          text = editor.Programme[a].text
          bud = []
          nombuf = editor.progConst2.recupe(editor.Programme[a].nom)
          text = editor.progConst2.recupe(text)
          if (editor.Programme[a].cond !== undefined) {
            for (let i = 0; i < editor.Programme[a].cond.length; i++) {
              editor.condVerif.push(editor.Programme[a].cond[i])
              editor.condVerif[editor.condVerif.length - 1].etape = a
              editor.condVerif[editor.condVerif.length - 1].pour = nombuf
              if (editor.Programme[a].cond[i].err) editor.condVerif[editor.condVerif.length - 1].err = editor.Programme[a].cond[i].err
              if (editor.Programme[a].cond[i].t === 'ap') {
                editor.condVerif[editor.condVerif.length - 1].pt = nombuf
              }
              if (editor.Programme[a].cond[i].t === 'long') {
                editor.condVerif[editor.condVerif.length - 1].co1 = nombuf
                bud.push({ formule: editor.progConst2.recupe(editor.Programme[a].cond[i].formule) })
                if (editor.Programme[a].cond[i].formule2 !== undefined) {
                  bud[bud.length - 1].formule2 = editor.progConst2.recupe(editor.Programme[a].cond[i].formule2)
                }
                if (bud[bud.length - 1].formule.includes('haz')) {
                  haz = j3pGetRandomInt(editor.Programme[a].cond[i].hazmin, editor.Programme[a].cond[i].hazmax)
                  // eslint-disable-next-line prefer-regex-literals
                  const regex = new RegExp('haz', 'g')
                  bud[bud.length - 1].formule = bud[bud.length - 1].formule.replace(regex, haz)
                }
                if (bud[bud.length - 1].formule.includes('à')) {
                  haz = j3pGetRandomInt(0, 100) / 100
                  // eslint-disable-next-line prefer-regex-literals
                  const regex = new RegExp('à', 'g')
                  bud[bud.length - 1].formule = bud[bud.length - 1].formule.replace(regex, haz)
                }
                if (editor.Programme[a].cond[i].affiche !== 'formule') {
                  const mesx = []
                  for (let j = 0; j < editor.longcalculs.length; j++) {
                    mesx.push({ ki: editor.progConst2.recupe(editor.longcalculs[j].ki), koi: editor.longcalculs[j].form })
                  }
                  for (let j = 0; j < mesx.length; j++) {
                    const regex2 = new RegExp(mesx[j].ki, 'g')
                    bud[bud.length - 1].formule = bud[bud.length - 1].formule.replace(regex2, mesx[j].koi)
                  }
                  while (bud[bud.length - 1].formule.includes('\\times')) {
                    bud[bud.length - 1].formule = bud[bud.length - 1].formule.replace('\\times', '*')
                  }
                  if (bud[bud.length - 1].formule.includes('\\frac{')) {
                    bud[bud.length - 1].formule = '(' + bud[bud.length - 1].formule.substring(6, bud[bud.length - 1].formule.indexOf('}{')) + ')/(' + bud[bud.length - 1].formule.substring(bud[bud.length - 1].formule.indexOf('}{') + 2, bud[bud.length - 1].formule.length - 1) + ')'
                  }
                  bud[bud.length - 1].formule = calcule(bud[bud.length - 1].formule)
                  if (bud[bud.length - 1].formule.includes('/')) {
                    bud[bud.length - 1].formule = String(j3pArrondi(parseInt(bud[bud.length - 1].formule.substring(0, bud[bud.length - 1].formule.indexOf('/'))) / parseInt(bud[bud.length - 1].formule.substring(bud[bud.length - 1].formule.indexOf('/') + 1)), 1))
                  }
                  bud[bud.length - 1].formule = String(j3pArrondi(parseFloat(bud[bud.length - 1].formule), 1))
                }

                const nombufco = editor.progConst2.recupe(editor.Programme[a].cond[i].cop)
                editor.longcalculs.push({ ki: nombuf + nombufco, form: bud[bud.length - 1].formule })
                editor.longcalculs.push({ ki: nombufco + nombuf, form: bud[bud.length - 1].formule })
                const regex2 = new RegExp('§' + i + '§', 'g')
                if (bud[bud.length - 1].formule2 !== undefined) {
                  text = text.replace(regex2, calcule(bud[bud.length - 1].formule2.replace('f', bud[bud.length - 1].formule)).replace('.', ','))
                } else {
                  text = text.replace(regex2, bud[bud.length - 1].formule.replace('.', ','))
                }
                editor.condVerif[editor.condVerif.length - 1].cb = bud[bud.length - 1].formule
              }
              if (editor.Programme[a].cond[i].t === 'ang') {
                editor.condVerif[editor.condVerif.length - 1].co1 = nombuf
                bud.push({ formule: editor.Programme[a].cond[i].formule })
                bud[bud.length - 1].formule = editor.progConst.calculFormule(bud[bud.length - 1].formule, editor.Programme[a].cond[i].affiche, editor.Programme[a].cond[i].hazmin, editor.Programme[a].cond[i].hazmax)
                const regex2 = new RegExp('§' + i + '§', 'g')
                text = text.replace(regex2, bud[bud.length - 1].formule.replace('.', ','))
                text = text.replace('µµ', '$\\widehat{' + editor.progConst2.recupe(editor.Programme[a].nom + editor.Programme[a].cond[i].cop + editor.Programme[a].cond[i].cop2) + '}$')
                if (text.indexOf('µ0µ') !== -1) text = text.replace('µ0µ', '$\\widehat{' + editor.progConst2.recupe(editor.Programme[a].nom + editor.Programme[a].cond[0].cop + editor.Programme[a].cond[0].cop2) + '}$')
                if (text.indexOf('µ1µ') !== -1) text = text.replace('µ1µ', '$\\widehat{' + editor.progConst2.recupe(editor.Programme[a].nom + editor.Programme[a].cond[1].cop + editor.Programme[a].cond[1].cop2) + '}$')
                editor.condVerif[editor.condVerif.length - 1].cb = bud[bud.length - 1].formule
              }
            }
          }
          if (editor.Programme[a].type === 'cercle') {
            if (editor.Programme[a].mode === 'rayon') {
              if (editor.Programme[a].affiche === 'valeur') {
                laval = editor.progConst.calculFormule(editor.Programme[a].rayon, true, editor.Programme[a].hazmin, editor.Programme[a].hazmax)
              } else {
                laval = editor.progConst2.recupe(editor.Programme[a].rayon)
              }
              text = text.replace('£0£', laval)
            } else {
              laval = editor.Programme[a].centre + editor.Programme[a].par
              laval = editor.progConst2.recupe(laval)
            }
          }
          editor.existeVerif.push({ type: editor.Programme[a].type, nom: nombuf, etape: a, errOub: editor.Programme[a].errOub, exa: editor.Programme[a].exa })
          if (editor.Programme[a].type === 'cercle') {
            editor.condVerif.push({ t: 'cercle', nom: nombuf, centre: editor.progConst2.recupe(editor.Programme[a].centre), rayon: laval })
            editor.Programme[a].rayon = laval
          }
          if (editor.Programme[a].type === 'droite' && !editor.Programme[a].exa) {
            editor.condVerif.push({
              t: 'droite',
              nom: nombuf,
              condd: editor.Programme[a].condd,
              pass: editor.Programme[a].pass,
              pass2: editor.Programme[a].pass2,
              odt: editor.Programme[a].odt
            })
            if (editor.Programme[a].pass2 === undefined) editor.condVerif[editor.condVerif.length - 1].pass2 = ''
            if (editor.Programme[a].odt === undefined) editor.condVerif[editor.condVerif.length - 1].odt = ''
            editor.condVerif[editor.condVerif.length - 1].pass = editor.progConst2.recupe(editor.condVerif[editor.condVerif.length - 1].pass)
            editor.condVerif[editor.condVerif.length - 1].pass2 = editor.progConst2.recupe(editor.condVerif[editor.condVerif.length - 1].pass2)
            editor.condVerif[editor.condVerif.length - 1].odt = editor.progConst2.recupe(editor.condVerif[editor.condVerif.length - 1].odt)
          }
        } else {
          text = editor.Programme[a].text
          for (b = 0; b < editor.Programme[a].cont.length; b++) {
            if (editor.Programme[a].cont[b].type === 'segment') editor.progConst2.nbseg++
            bud = []
            if (editor.Programme[a].cont[b].type !== 'af') {
              nombuf = editor.progConst2.recupe(editor.Programme[a].cont[b].nom)
            }
            if (editor.Programme[a].cont[b].cond !== undefined) {
              for (let i = 0; i < editor.Programme[a].cont[b].cond.length; i++) {
                editor.condVerif.push(editor.Programme[a].cont[b].cond[i])
                editor.condVerif[editor.condVerif.length - 1].etape = a
                editor.condVerif[editor.condVerif.length - 1].pour = nombuf
                if (editor.Programme[a].cont[b].need === false) editor.condVerif[editor.condVerif.length - 1].noneed = true
                if (editor.Programme[a].cont[b].cond[i].t === 'long') {
                  editor.condVerif[editor.condVerif.length - 1].co1 = nombuf
                  bud.push({ formule: editor.progConst2.recupe(editor.Programme[a].cont[b].cond[i].formule) })
                  if (editor.Programme[a].cont[b].cond[i].formule2 !== undefined) {
                    bud[bud.length - 1].formule2 = editor.progConst2.recupe(editor.Programme[a].cont[b].cond[i].formule2)
                  }
                  if (bud[bud.length - 1].formule.includes('haz')) {
                    haz = j3pGetRandomInt(editor.Programme[a].cont[b].cond[i].hazmin, editor.Programme[a].cont[b].cond[i].hazmax)
                    // eslint-disable-next-line prefer-regex-literals
                    const regex = new RegExp('haz', 'g')
                    bud[bud.length - 1].formule = bud[bud.length - 1].formule.replace(regex, haz)
                  }
                  if (bud[bud.length - 1].formule.includes('à')) {
                    haz = j3pGetRandomInt(0, 100) / 100
                    // eslint-disable-next-line prefer-regex-literals
                    const regex = new RegExp('à', 'g')
                    bud[bud.length - 1].formule = bud[bud.length - 1].formule.replace(regex, haz)
                  }
                  if (editor.Programme[a].cont[b].cond[i].affiche !== 'formule') {
                    const mesx = []
                    for (let j = 0; j < editor.longcalculs.length; j++) {
                      mesx.push({ ki: editor.longcalculs[j].ki, koi: editor.longcalculs[j].form, ki2: editor.progConst2.recupe(editor.longcalculs[j].ki) })
                    }
                    for (let j = 0; j < mesx.length; j++) {
                      let regex2 = new RegExp(mesx[j].ki, 'g')
                      bud[bud.length - 1].formule = bud[bud.length - 1].formule.replace(regex2, mesx[j].koi)
                      regex2 = new RegExp(mesx[j].ki2, 'g')
                      bud[bud.length - 1].formule = bud[bud.length - 1].formule.replace(regex2, mesx[j].koi)
                      if (mesx[j].ki2.length > 1) {
                        regex2 = new RegExp(mesx[j].ki2[1] + mesx[j].ki2[0], 'g')
                        bud[bud.length - 1].formule = bud[bud.length - 1].formule.replace(regex2, mesx[j].koi)
                      }
                    }
                    while (bud[bud.length - 1].formule.includes('\\times')) {
                      bud[bud.length - 1].formule = bud[bud.length - 1].formule.replace('\\times', '*')
                    }
                    if (bud[bud.length - 1].formule.includes('\\frac{')) {
                      bud[bud.length - 1].formule = '(' + bud[bud.length - 1].formule.substring(6, bud[bud.length - 1].formule.indexOf('}{')) + ')/(' + bud[bud.length - 1].formule.substring(bud[bud.length - 1].formule.indexOf('}{') + 2, bud[bud.length - 1].formule.length - 1) + ')'
                    }
                    bud[bud.length - 1].formule = calcule(bud[bud.length - 1].formule)
                    if (bud[bud.length - 1].formule.includes('/')) {
                      bud[bud.length - 1].formule = String(j3pArrondi(parseInt(bud[bud.length - 1].formule.substring(0, bud[bud.length - 1].formule.indexOf('/'))) / parseInt(bud[bud.length - 1].formule.substring(bud[bud.length - 1].formule.indexOf('/') + 1)), 1))
                    }
                    bud[bud.length - 1].formule = String(j3pArrondi(parseFloat(bud[bud.length - 1].formule), 1))
                  }
                  let nombufco = editor.Programme[a].cont[b].cond[i].cop
                  for (let j = 0; j < editor.progConst.nomprisProg.length; j++) {
                    const regex = new RegExp('£' + editor.progConst.nomprisProg[j].ki, 'g')
                    nombufco = nombufco.replace(regex, editor.progConst.nomprisProg[j].com)
                  }
                  editor.longcalculs.push({ ki: nombuf + nombufco, form: bud[bud.length - 1].formule })
                  editor.longcalculs.push({ ki: nombufco + nombuf, form: bud[bud.length - 1].formule })
                  if (bud[bud.length - 1].formule2 !== undefined) {
                    text = text.replace('§' + i + '§', calcule(bud[bud.length - 1].formule2.replace('f', bud[bud.length - 1].formule)).replace('.', ','))
                  } else {
                    if (editor.Programme[a].cont[b].cond[i].paf !== true) text = text.replace('§' + i + '§', bud[bud.length - 1].formule.replace('.', ','))
                  }
                  editor.condVerif[editor.condVerif.length - 1].cb = bud[bud.length - 1].formule
                  for (mu = 0; mu < artebuf.length; mu++) {
                    if (artebuf[mu].nom === editor.condVerif[editor.condVerif.length - 1].cb || artebuf[mu].nom2 === editor.condVerif[editor.condVerif.length - 1].cb) {
                      editor.condVerif[editor.condVerif.length - 1].cb = artebuf[mu].val
                    }
                  }
                  artebuf.push({
                    nom: editor.progConst2.recupe(editor.condVerif[editor.condVerif.length - 1].co1 + editor.condVerif[editor.condVerif.length - 1].cop),
                    nom2: editor.progConst2.recupe(editor.condVerif[editor.condVerif.length - 1].cop + editor.condVerif[editor.condVerif.length - 1].co1),
                    val: editor.condVerif[editor.condVerif.length - 1].cb
                  })
                }
                if (editor.Programme[a].cont[b].cond[i].t === 'ap') {
                  editor.condVerif[editor.condVerif.length - 1].pt = nombuf
                }
                if (editor.Programme[a].cont[b].cond[i].t === 'ang') {
                  editor.condVerif[editor.condVerif.length - 1].co1 = nombuf
                  bud.push({ formule: editor.Programme[a].cont[b].cond[i].formule })
                  if (bud[bud.length - 1].formule.includes('¤')) {
                    for (om = editor.condVerif.length - 1; om > -1; om--) {
                      bud[bud.length - 1].formule = bud[bud.length - 1].formule.replace('¤' + om, editor.condVerif[om].cb)
                    }
                  }
                  bud[bud.length - 1].formule = editor.progConst.calculFormule(bud[bud.length - 1].formule, editor.Programme[a].cont[b].cond[i].affiche, editor.Programme[a].cont[b].cond[i].hazmin, editor.Programme[a].cont[b].cond[i].hazmax)
                  text = text.replace('§' + i + '§', bud[bud.length - 1].formule.replace('.', ','))
                  text = text.replace('µµ', '$\\widehat{' + editor.progConst2.recupe(editor.Programme[a].cont[b].nom + editor.Programme[a].cont[b].cond[i].cop + editor.Programme[a].cont[b].cond[i].cop2) + '}$')
                  if (text.indexOf('µ0µ') !== -1) text = text.replace('µ0µ', '$\\widehat{' + editor.progConst2.recupe(editor.Programme[a].cont[b].nom + editor.Programme[a].cont[b].cond[0].cop + editor.Programme[a].cont[b].cond[0].cop2) + '}$')
                  if (text.indexOf('µ1µ') !== -1) text = text.replace('µ1µ', '$\\widehat{' + editor.progConst2.recupe(editor.Programme[a].cont[b].nom + editor.Programme[a].cont[b].cond[1].cop + editor.Programme[a].cont[b].cond[1].cop2) + '}$')
                  editor.condVerif[editor.condVerif.length - 1].cb = bud[bud.length - 1].formule
                }
              }
            }
            if (editor.Programme[a].cont[b].type === 'cercle') {
              if (editor.Programme[a].cont[b].mode === 'rayon') {
                if (editor.Programme[a].cont[b].affiche === 'valeur') {
                  laval = editor.progConst.calculFormule(editor.Programme[a].cont[b].rayon, true, editor.Programme[a].cont[b].hazmin, editor.Programme[a].cont[b].hazmax)
                } else {
                  laval = editor.Programme[a].cont[b].rayon
                  for (let i = 0; i < editor.progConst.nomprisProg.length; i++) {
                    const regex = new RegExp(editor.progConst.nomprisProg[i].ki, 'g')
                    laval = laval.replace(regex, editor.progConst.nomprisProg[i].com)
                  }
                }
                text = text.replace('£0£', laval)
              } else {
                laval = editor.progConst2.recupe(editor.Programme[a].cont[b].centre + editor.Programme[a].cont[b].par)
              }
            }
            if (editor.Programme[a].cont[b].need !== false) {
              editor.existeVerif.push({
                type: editor.Programme[a].cont[b].type,
                nom: nombuf,
                etape: a,
                errOub: editor.Programme[a].cont[b].errOub
              })
            }
            if (editor.Programme[a].cont[b].type === 'cercle') {
              editor.condVerif.push({ t: 'cercle', nom: nombuf, centre: editor.progConst2.recupe(editor.Programme[a].cont[b].centre), rayon: laval })
              if (editor.Programme[a].cont[b].need === false) editor.condVerif[editor.condVerif.length - 1].noneed = true
            }
            if (editor.Programme[a].cont[b].type === 'demidroite') {
              editor.condVerif.push({ t: 'demidroite', nom: nombuf, origine: editor.progConst2.recupe(editor.Programme[a].cont[b].origine), point: editor.progConst2.recupe(editor.Programme[a].cont[b].point) })
              if (editor.Programme[a].cont[b].need === false) editor.condVerif[editor.condVerif.length - 1].noneed = true
            }
            if (editor.Programme[a].cont[b].type === 'droite' && editor.Programme[a].cont[b].need !== false) {
              editor.condVerif.push({
                t: 'droite',
                nom: nombuf,
                condd: editor.Programme[a].cont[b].condd,
                pass: editor.Programme[a].cont[b].pass,
                pass2: editor.Programme[a].cont[b].pass2,
                odt: editor.Programme[a].cont[b].odt,
                err: editor.Programme[a].cont[b].err
              })
              if (editor.Programme[a].cont[b].pass2 === undefined) editor.condVerif[editor.condVerif.length - 1].pass2 = ''
              if (editor.Programme[a].cont[b].odt === undefined) editor.condVerif[editor.condVerif.length - 1].odt = ''
              editor.condVerif[editor.condVerif.length - 1].pass = editor.progConst2.recupe(editor.condVerif[editor.condVerif.length - 1].pass)
              editor.condVerif[editor.condVerif.length - 1].pass2 = editor.progConst2.recupe(editor.condVerif[editor.condVerif.length - 1].pass2)
              editor.condVerif[editor.condVerif.length - 1].odt = editor.progConst2.recupe(editor.condVerif[editor.condVerif.length - 1].odt)
            }
          }
        }
        text = editor.progConst2.recupe(text)
        editor.MonMenu.push(
          {
            name: 'men' + a,
            label: (a + 1) + '♦  ' + text,
            callback: function (menu/*, choice, event */) {
            }
          }
        )
      }
      editor.progConst2.condVerif = j3pClone(editor.condVerif)
      editor.progConst2.progfig = editor.progfig
      editor.progConst2.listeSegPris = []
      editor.progConst2.listePointPris = []
      editor.progConst2.listeDtePris = []
      editor.progConst2.listeDemiDPris = []
      editor.progConst2.listeArcPris = []
      editor.progConst2.ptCo = []
      editor.progConst2.segCo = []
      editor.progConst2.demiDco = []
      editor.progConst2.arcco = []
      editor.progConst2.dteco = []
      editor.progConst2.clignEff = []
      editor.progConst2.nomco = []
      editor.progConst2.nomDebDte = []
      editor.progConst2.divparent = container
      editor.progConst2.zoom = 1
      if (editor.mondivBUlle !== undefined) {
        j3pDetruit(editor.mondivBUlle)
        editor.mondivBUlle = undefined
      }
      editor.listeFonc = []
    }
    function reNomRecupe () {
      editor.progConst.nomprisProg = []
      editor.nomPosProg = ['Z', 'Y', 'X', 'W', 'V', 'U', 'T', 'S', 'R', 'Q', 'P', 'O', 'N', 'M', 'L', 'K', 'J', 'I', 'H', 'G', 'F', 'E', 'D', 'C', 'B', 'A']
      for (let a = 0; a < editor.progfig.length; a++) {
        if (editor.progfig[a].type === 'point') {
          for (let i = editor.nomPosProg.length - 1; i > -1; i--) {
            if (editor.nomPosProg[i] === editor.progfig[a].nom) {
              editor.nomPosProg.splice(i, 1)
              break
            }
          }
        }
      }
      for (let a = 0; a < editor.Programme.length; a++) {
        if (editor.Programme[a].type === 'point') {
          if (editor.Programme[a].nom.includes('Hz')) {
            for (let i = editor.nomPosProg.length - 1; i > -1; i--) {
              if (editor.nomPosProg[i] === editor.Programme[a].nom) {
                editor.nomPosProg.splice(i, 1)
                break
              }
            }
          }
        }
        if (editor.Programme[a].type === 'multi') {
          for (let j = 0; j < editor.Programme[a].cont.length; j++) {
            if (editor.Programme[a].cont[j].type === 'point') {
              if (editor.Programme[a].cont[j].nom.includes('Hz')) {
                for (let i = editor.nomPosProg.length - 1; i > -1; i--) {
                  if (editor.nomPosProg[i] === editor.Programme[a].cont[j].nom) {
                    editor.nomPosProg.splice(i, 1)
                    break
                  }
                }
              }
            }
          }
        }
      }
      for (let a = 0; a < editor.progfig.length; a++) {
        if (editor.progfig[a].type === 'point') {
          if (editor.progfig[a].nom.includes('Hz')) {
            editor.progConst.nomprisProg.push({ ki: editor.progfig[a].nom, com: editor.nomPosProg.pop() })
          }
        }
      }
      for (let a = 0; a < editor.Programme.length; a++) {
        if (editor.Programme[a].type !== 'multi') {
          if (editor.Programme[a].type === 'point') {
            if (editor.Programme[a].nom.includes('Hz')) {
              editor.progConst.nomprisProg.push({ ki: editor.Programme[a].nom, com: editor.nomPosProg.pop() })
            }
          }
        } else {
          for (let b = 0; b < editor.Programme[a].cont.length; b++) {
            if (editor.Programme[a].cont[b].type === 'point') {
              if (editor.Programme[a].cont[b].nom.includes('Hz')) {
                editor.progConst.nomprisProg.push({ ki: editor.Programme[a].cont[b].nom, com: editor.nomPosProg.pop() })
              }
            }
          }
        }
      }
    }
    function afficheExplik (ou, txt, bool) {
      const tomuch = addDefaultTable(ou, 1, 2)
      j3pAjouteBouton(tomuch[0][0], () => { faiv(tomuch[0][1]) }, { value: ' ? ' })
      tomuch[0][1].style.color = '#850593'
      tomuch[0][1].style.background = '#b0ee7f'
      tomuch[0][1].style.padding = '5px'
      tomuch[0][1].style.border = '1px solid black'
      j3pAddContent(tomuch[0][1], txt)
      tomuch[0][1].style.display = 'none'
      j3pAddContent(ou, '\n')
    }
    function afficheExplik2 (ou, txt) {
      j3pEmpty(ou)
      afficheExplik(ou, txt)
    }
    function faiv (el) {
      el.style.display = (el.style.display === '') ? 'none' : ''
    }
    function reNomRecupe2 () {
      editor.progConst2.nomprisProg = []
      editor.nomPosProg = j3pShuffle(['Z', 'Y', 'X', 'W', 'V', 'U', 'T', 'S', 'R', 'Q', 'P', 'O', 'N', 'M', 'L', 'K', 'J', 'I', 'H', 'G', 'F', 'E', 'D', 'C', 'B', 'A'])
      for (let a = 0; a < editor.progfig.length; a++) {
        if (editor.progfig[a].type === 'point') {
          for (let i = editor.nomPosProg.length - 1; i > -1; i--) {
            if (editor.nomPosProg[i] === editor.progfig[a].nom) {
              editor.nomPosProg.splice(i, 1)
              break
            }
          }
        }
      }
      for (let a = 0; a < editor.Programme.length; a++) {
        if (editor.Programme[a].type === 'point') {
          if (editor.Programme[a].nom.includes('Hz')) {
            for (let i = editor.nomPosProg.length - 1; i > -1; i--) {
              if (editor.nomPosProg[i] === editor.Programme[a].nom) {
                editor.nomPosProg.splice(i, 1)
                break
              }
            }
          }
        }
        if (editor.Programme[a].type === 'multi') {
          for (let j = 0; j < editor.Programme[a].cont.length; j++) {
            if (editor.Programme[a].cont[j].type === 'point') {
              if (editor.Programme[a].cont[j].nom.includes('Hz')) {
                for (let i = editor.nomPosProg.length - 1; i > -1; i--) {
                  if (editor.nomPosProg[i] === editor.Programme[a].cont[j].nom) {
                    editor.nomPosProg.splice(i, 1)
                    break
                  }
                }
              }
            }
          }
        }
      }
      for (let a = 0; a < editor.progfig.length; a++) {
        if (editor.progfig[a].type === 'point') {
          if (editor.progfig[a].nom.includes('Hz')) {
            editor.progConst2.nomprisProg.push({ ki: editor.progfig[a].nom, com: editor.nomPosProg.pop() })
          }
        }
      }
      for (let a = 0; a < editor.Programme.length; a++) {
        if (editor.Programme[a].type !== 'multi') {
          if (editor.Programme[a].type === 'point') {
            if (editor.Programme[a].nom.includes('Hz')) {
              editor.progConst2.nomprisProg.push({ ki: editor.Programme[a].nom, com: editor.nomPosProg.pop() })
            }
          }
        } else {
          for (let b = 0; b < editor.Programme[a].cont.length; b++) {
            if (editor.Programme[a].cont[b].type === 'point') {
              if (editor.Programme[a].cont[b].nom.includes('Hz')) {
                editor.progConst2.nomprisProg.push({ ki: editor.Programme[a].cont[b].nom, com: editor.nomPosProg.pop() })
              }
            }
          }
        }
      }
    }
    function creeLesDiv () {
      try {
        valuesDeb.Programme = JSON.parse(values.Programme)
      } catch (e) {
        editor.divUp = j3pAddElt(container, 'div')
        j3pAddContent(editor.divUp, 'La version de cette ressource est trop ancienne.\n')
        j3pAddContent(editor.divUp, 'Elle ne peut être modifiée avec cet éditeur.\n')
        j3pAddContent(editor.divUp, 'Pour éditer cette ressource, il faut utiliser le bouton "saisie brute" sur la page précédente.\n')
        j3pAddContent(editor.divUp, 'Cette')
        const spanAp = j3pAddElt(editor.divUp, 'span')
        j3pAddContent(spanAp, '&nbsp;<b>page internet</b>&nbsp;')
        j3pAddContent(editor.divUp, 'détaille la création d’une ressource de ce type.\n')
        spanAp.addEventListener('click', () => {
          window.open('https://aide.labomep.sesamath.net/doku.php?id=executer_un_programme_de_construction', 'Tutoriel création', 'left=50,top=50')
        })
        spanAp.style.cursor = 'pointer'
        spanAp.style.color = '#155ea9'
        editor.btnValider.style.display = 'none'
        editor.foKit = true
        j3pAddContent(editor.divUp, '\n')
        j3pAddContent(editor.divUp, '\n')
        j3pAddContent(editor.divUp, 'Cliquer sur le bouton <b>Annuler</b> pour quitter cet éditeur.\n')
        return
      }
      editor.ProgrammeLT = j3pClone(values.Programme.exos)
      editor.lesdiv = {}
      editor.lesdiv.conteneur = container
      editor.lesdiv.divUp = j3pAddElt(editor.lesdiv.conteneur, 'div')
      editor.lesdiv.divUp.style.border = '5px solid black'
      editor.lesdiv.divUp.style.borderRadius = '10px'
      editor.lesdiv.divUp.style.padding = '10px'
      editor.lesdiv.divUp.style.background = '#47ffff'
      editor.lesdiv.div1 = j3pAddElt(editor.lesdiv.conteneur, 'div')
      editor.lesdiv.div1.style.border = '5px solid black'
      editor.lesdiv.div1.style.borderRadius = '10px'
      editor.lesdiv.div1.style.padding = '10px'
      editor.lesdiv.div1.style.background = '#ccccff'
      editor.lesdiv.div1.style.minWidth = '900px'
      editor.lesdiv.div1.classList.add('pourFitContent')
      editor.lesdiv.div2 = j3pAddElt(editor.lesdiv.conteneur, 'div')
      editor.lesdiv.div2.style.border = '5px solid black'
      editor.lesdiv.div2.style.borderRadius = '10px'
      editor.lesdiv.div2.style.backgroundImage = 'linear-gradient(to left, rgba(245, 169, 107, 0.8) 30%, rgba(245, 220, 187, 0.4))'
      editor.lesdiv.div2.style.padding = '10px'
      editor.lesdiv.div2.style.display = 'none'
      editor.lesdiv.div2.style.minWidth = '900px'
      editor.lesdiv.div2.classList.add('pourFitContent')
      editor.lesdiv.div3 = j3pAddElt(editor.lesdiv.conteneur, 'div')
      editor.lesdiv.div3.style.border = '5px solid black'
      editor.lesdiv.div3.style.borderRadius = '10px'
      editor.lesdiv.div3.style.backgroundImage = 'linear-gradient(to right, rgba(187, 187, 187, 0.8) 30%, rgba(224, 224, 224, 0.5))'
      editor.lesdiv.div3.style.padding = '10px'
      editor.lesdiv.div3.style.display = 'none'
      editor.lesdiv.div3.style.minWidth = '900px'
      editor.lesdiv.div3.classList.add('pourFitContent')
      editor.lesdiv.div4 = j3pAddElt(editor.lesdiv.conteneur, 'div')
      editor.lesdiv.div4.style.border = '5px solid black'
      editor.lesdiv.div4.style.borderRadius = '10px'
      editor.lesdiv.div4.style.backgroundImage = 'linear-gradient(to right, rgba(187, 187, 187, 0.8) 30%, rgba(224, 224, 224, 0.5))'
      editor.lesdiv.div4.style.padding = '10px'
      editor.lesdiv.div4.style.display = 'none'
      editor.lesdiv.div4.style.minWidth = '900px'
      editor.lesdiv.div4.classList.add('pourFitContent')

      const yuyu = j3pAddElt(editor.lesdiv.divUp, 'span')
      j3pAddContent(yuyu, '<b><u>Titre de la ressource:</u></b>&nbsp;')
      editor.titre = valuesDeb.Programme.titre || 'Suivre un programme de construction'
      editor.inputTitre = j3pAddElt(yuyu, 'input', '', {
        value: editor.titre,
        size: 50
      })
      editor.inputTitre.addEventListener('change', () => {
        editor.titre = editor.inputTitre.value.replace(/'/g, 'ʼ')
      })
      j3pAddContent(editor.lesdiv.divUp, '\n')
      j3pAddContent(editor.lesdiv.divUp, '\n')

      const yu2 = j3pAddElt(editor.lesdiv.divUp, 'span')
      j3pAddContent(yu2, '<b><u>Nombre d’exercices proposées ( entre 1 et 20 )</u></b>:&nbsp;')
      editor.nbrepetitions = j3pAddElt(editor.lesdiv.divUp, 'input', '', {
        type: 'number',
        value: valuesDeb.nbrepetitions,
        size: 4,
        min: '1',
        max: '10'
      })
      editor.nbrepetitions.addEventListener('change', () => {
        const averif = editor.nbrepetitions.value
        if (averif === '' || isNaN(averif) || (averif < 1) || (averif > 20)) editor.nbrepetitions.value = '1'
      })
      j3pAddContent(editor.lesdiv.divUp, '\n')
      j3pAddContent(editor.lesdiv.divUp, '\n')

      const yuch = j3pAddElt(editor.lesdiv.divUp, 'span')
      j3pAddContent(yuch, '<b><u>Nombre de tentatives autorisées ( entre 1 et 20 )</u></b>:&nbsp;')
      editor.nbchances = j3pAddElt(editor.lesdiv.divUp, 'input', '', {
        type: 'number',
        value: valuesDeb.nbchances,
        size: 4,
        min: '1',
        max: '10'
      })
      editor.nbchances.addEventListener('change', () => {
        const averif = editor.nbchances.value
        if (averif === '' || isNaN(averif) || (averif < 1) || (averif > 20)) editor.nbchances.value = '1'
      })
      j3pAddContent(editor.lesdiv.divUp, '\n')
      j3pAddContent(editor.lesdiv.divUp, '\n')

      const yu9921 = j3pAddElt(editor.lesdiv.divUp, 'span')
      const ldOptions23 = { dansModale: true, speMod: 10, speMod2: 70, j3pCont: container, choix0: true }
      j3pAddContent(yu9921, '<b><u>Exercices ordonnés:</u></b>&nbsp;')
      const ll9921 = (valuesDeb.Sujet_ordonne) ? ['Oui', 'Non'] : ['Non', 'Oui']
      editor.ordonneL = ListeDeroulante.create(yu9921, ll9921, ldOptions23)
      j3pAddContent(editor.lesdiv.divUp, '\n')

      const yu9917 = addDefaultTable(editor.lesdiv.divUp, 5, 7)
      j3pAddContent(yu9917[0][0], '<b><u>Instruments autorisés</u>:</b>&nbsp;&nbsp;')
      metsInstrume(yu9917[0][1], imRgle, 'Regle')
      metsInstrume(yu9917[0][2], imRgle2, 'Regle_grad')
      metsInstrume(yu9917[0][3], imRap, 'Rapporteur')
      metsInstrume(yu9917[0][4], imEq, 'Equerre')
      metsInstrume(yu9917[0][5], imCompas, 'Compas')
      function metsInstrume (ou, im, quoi) {
        const tabregle1 = addDefaultTable(ou, 2, 1)
        const lim = j3pAddElt(tabregle1[0][0], 'img', { src: im })
        lim.style.width = '30px'
        lim.style.height = '30px'
        lim.style.background = '#fff'
        tabregle1[0][0].style.textAlign = 'center'
        tabregle1[1][0].style.textAlign = 'center'
        tabregle1[0][0].style.width = '20px'
        tabregle1[0][0].style.height = '20px'
        j3pAddContent(tabregle1[0][0], '&nbsp;')
        const caseco = j3pAjouteCaseCoche(tabregle1[1][0])
        editor[quoi] = values[quoi]
        caseco.checked = editor[quoi]
        caseco.addEventListener('change', () => {
          editor[quoi] = caseco.checked
        })
      }
      j3pAddContent(yu9917[2][0], '<b><u>Boutons disponibles</u>:</b>&nbsp;&nbsp;')
      metsInstrume(yu9917[2][1], imRot, 'Rotation')
      metsInstrume(yu9917[2][2], imPlus, 'Agrandir')

      const yu991e = j3pAddElt(editor.lesdiv.divUp, 'span')
      const tabPrec = addDefaultTable(yu991e, 1, 7)
      j3pAddContent(tabPrec[0][0], '<b><u>Précisions attendues:</u></b>&nbsp;')
      j3pAddContent(tabPrec[0][1], 'longueurs :&nbsp;')
      editor.PrecisLong = valuesDeb.Precis_long
      const inputPrecLOng = j3pAddElt(tabPrec[0][2], 'input', '', {
        type: 'number',
        value: valuesDeb.Precis_long,
        min: 1,
        max: 10,
        size: 3
      })
      inputPrecLOng.addEventListener('change', () => {
        const averif = inputPrecLOng.value
        if (averif === '' || isNaN(averif) || (averif < 1) || (averif > 10)) inputPrecLOng.value = '1'
        editor.PrecisLong = inputPrecLOng.value
      })
      j3pAddContent(tabPrec[0][3], '&nbsp;mm;&nbsp;&nbsp;&nbsp;angles :&nbsp;')

      editor.PrecisAng = valuesDeb.Precis_ang
      const inputPrecLAng = j3pAddElt(tabPrec[0][4], 'input', '', {
        type: 'number',
        value: valuesDeb.Precis_ang,
        min: 1,
        max: 10,
        size: 3
      })
      inputPrecLAng.addEventListener('change', () => {
        const averif = inputPrecLAng.value
        if (averif === '' || isNaN(averif) || (averif < 1) || (averif > 10)) inputPrecLAng.value = '1'
        editor.PrecisAng = inputPrecLAng.value
      })
      j3pAddContent(tabPrec[0][5], '&nbsp;°')

      j3pAddContent(editor.lesdiv.divUp, '\n')

      const yu991 = j3pAddElt(editor.lesdiv.divUp, 'span')
      j3pAddContent(yu991, '<b><u>Theme:</u></b>&nbsp;')
      const ll991 = (valuesDeb.theme === 'standard') ? ['standard', 'zonesAvecImageDeFond'] : ['zonesAvecImageDeFond', 'standard']
      editor.themeL = ListeDeroulante.create(yu991, ll991, ldOptions23)
      j3pAddContent(editor.lesdiv.divUp, '\n')
    }
    function creeLesDiv2 () {
      j3pEmpty(editor.lesdiv.div2)
      j3pEmpty(editor.lesdiv.div3)
      const tab1 = addDefaultTable(editor.lesdiv.div2, 2, 1)
      const tabTraiConst = addDefaultTable(tab1[1][0], 1, 2)
      j3pAddContent(tabTraiConst[0][0], 'L\'élève doit supprimer les traits de constructions :&nbsp;')
      editor.cocheTrait = j3pAjouteCaseCoche(tabTraiConst[0][0], { label: '' })
      editor.cocheTrait.checked = editor.yatrait
      const tabdep = addDefaultTable(tab1[0][0], 2, 5)
      editor.lesdiv.tabhaz = addDefaultTable(tab1[0][0], 1, 1)[0][0]
      const tabsuite = addDefaultTable(editor.lesdiv.div3, 4, 2)
      j3pAffiche(tabdep[0][1], null, '&nbsp;&nbsp;')
      j3pAffiche(tabdep[0][3], null, '&nbsp;&nbsp;')
      j3pAffiche(tabdep[0][0], null, 'Figure de départ')
      afficheExplik(tabdep[0][0], 'Figure qui apparait au début de l\'exercice.<br> L\'élève ne peut pas l\'effacer.')
      tabdep[0][0].style.background = '#000'
      tabdep[0][0].style.color = '#fff'
      tabdep[0][2].style.color = '#fff'
      tabdep[0][0].style.textAlign = 'center'
      j3pAffiche(tabdep[0][2], null, '&nbsp;&nbsp;Elements de la figure de départ&nbsp;&nbsp;')
      tabdep[0][2].style.background = '#002980'
      tabdep[0][2].style.textAlign = 'center'
      tabdep[0][4].style.background = '#ccc'
      tabdep[0][4].style.textAlign = 'center'
      editor.lesdiv.figdep = tabdep[1][0]
      editor.lesdiv.modifDep = tabdep[1][2]
      editor.lesdiv.actDepTitre = tabdep[0][4]
      editor.lesdiv.actDep = tabdep[1][4]
      j3pAffiche(tabsuite[0][0], null, '&nbsp;&nbsp;Programme donné à  l\'élève&nbsp;&nbsp;')
      afficheExplik(tabsuite[0][0], 'Programme de construction à réaliser <br>à l\'aide des outils de géométrie virtuels.')
      tabsuite[0][0].style.background = '#d33dfd'
      editor.lesdiv.prog = tabsuite[1][0]
      editor.lesdiv.prog.style.marginRight = '10px'
      editor.lesdiv.prog.style.verticalAlign = 'top'
      editor.lesdiv.modifProg = tabsuite[1][1]
      editor.lesdiv.combine = tabsuite[2][0]
      editor.lesdiv.actDep.style.verticalAlign = 'top'
      razDiv4()
    }
    function razDiv4 () {
      editor.lesdiv.div4.style.display = ''
      j3pEmpty(editor.lesdiv.div4)
      j3pAddContent(editor.lesdiv.div4, '<b>TEST</b>\n')
      j3pAjouteBouton(editor.lesdiv.div4, testCo, { className: 'MepBoutons', value: 'Générer cet exercice' })
    }
    function testCo () {
      if (editor.blop) return
      j3pEmpty(editor.lesdiv.div4)
      j3pAddContent(editor.lesdiv.div4, '<b>TEST</b>&nbsp;&nbsp;&nbsp;')
      const coboot1 = j3pAddElt(editor.lesdiv.div4, 'span')
      j3pAjouteBouton(coboot1, voirCo, { value: 'Voir une correction' })
      j3pAddContent(coboot1, '&nbsp;&nbsp;&nbsp;')
      j3pAjouteBouton(coboot1, corrigeVoir, { value: 'Corriger cette construction' })
      j3pAddContent(coboot1, '&nbsp;&nbsp;&nbsp;')
      j3pAjouteBouton(coboot1, testCo, { value: 'Effacer tout' })
      const coboot = j3pAddElt(editor.lesdiv.div4, 'span')
      j3pAddContent(editor.lesdiv.div4, '\n\n')
      editor.progConst2 = new ProgConst(ds, editor, 'Fig1')
      editor.progConst2.consigne = coboot1
      editor.progConst2.coboot = coboot
      editor.progConst2.coboot1 = coboot1
      editor.progConst2.ds = {
        Rotation: editor.Rotation,
        Agrandir: editor.Agrandir,
        Regle: editor.Regle,
        Regle_grad: editor.Regle_grad,
        Equerre: editor.Equerre,
        Rapporteur: editor.Rapporteur,
        Compas: editor.Compas,
        Precis_long: editor.PrecisLong,
        dev: false
      }
      editor.progConst2.mtgAppLecteur = editor.mtgAppLecteur
      editor.progConst2.listePourCalc = editor.listePourCalc
      editor.progConst2.Programme = j3pClone(editor.Programme)
      const menuDiv = j3pAddElt(editor.lesdiv.div4, 'div')
      j3pAddContent(editor.lesdiv.div4, '\n')
      editor.progConst2.leDivBull = j3pAddElt(editor.lesdiv.div4, 'div')
      editor.progConst2.zonefig = j3pAddElt(editor.lesdiv.div4, 'div')
      choixExos2()
      editor.progConst2.unite = 27.33576561109424
      for (let i = 0; i < editor.MonMenu.length; i++) {
        j3pAffiche(menuDiv, null, editor.MonMenu[i].label + '\n')
      }
      j3pAddContent(menuDiv, '\n')

      menuDiv.style.border = '2px solid black'
      menuDiv.style.background = '#fcabab'

      editor.progConst2.whereAfficheAide = j3pAddElt(editor.lesdiv.div4, 'div')
      editor.progConst2.whereAfficheAide.style.border = '3px solid black'
      editor.progConst2.whereAfficheAide.style.borderRadius = '7px'
      editor.progConst2.whereAfficheAide.style.background = '#2eb0b4'
      editor.progConst2.blokRemove = true
      editor.progConst2.yaOldSvg = editor.SvgId.test
      editor.progConst2.makefig()
      editor.SvgId.test = editor.progConst2.svgId
      editor.lesdiv.explications = j3pAddElt(editor.lesdiv.div4, 'div')
      editor.lesdiv.explications.style.color = '#a92727'
      editor.progConst2.mepact = editor.lesdiv.div4
    }
    function voirCo () {
      editor.onco = true
      editor.progConst2.Programme = j3pClone(editor.Programme)
      editor.progConst2.desactivezone()
      editor.progConst2.debutAnimeRepere()
      j3pEmpty(editor.progConst2.coboot1)
      editor.progConst2.apresCo = () => { testCo() }
    }
    function corrigeVoir () {
      j3pEmpty(editor.lesdiv.explications)
      if (isRepOkConst(editor, editor.progConst2, { Precis_long: editor.PrecisLong, Precis_ang: editor.PrecisAng })) {
        editor.progConst2.faisFond(true)
        editor.progConst2.zonefig.style.border = '3px solid #008073'
      } else {
        editor.progConst2.zonefig.style.border = '3px solid #D64700'
        affCorrectionConst(false, editor, editor.progConst2)
      }
    }

    function okPlaceC (num) {
      okPlace(num)
      j3pEmpty(editor.lesdiv.actDepTitre)
      j3pEmpty(editor.lesdiv.actDep)
      affichelistedep()
      remetCool()
    }

    function retrouveTagsZone (num) {
      for (let i = 0; i < editor.progConst.listeZone.length; i++) {
        if (editor.progConst.listeZone[i].nom === editor.progfig[num].nom) return editor.progConst.listeZone[i].tags
      }
    }
    function ajZone (ki) {
      if (!ki.c) {
        ki.c = { x: 200, y: 230 }
      }
      if (!ki.p) {
        ki.p = { x: 200, y: 300 }
      }
      if (!ki.nom) ki.nom = newNomZone()
      ki.tags = editor.progConst.makeZone(ki)
      editor.progConst.listeZone.push(ki)
      return ki
    }
    function newNomZone () {
      const tbN = editor.progConst.listeZone.map(el => el.nom)
      let aret = ''
      let cmpt = 0
      do {
        cmpt++
        aret = 'Zone ' + cmpt
      } while (tbN.indexOf(aret) !== -1)
      return aret
    }

    function ajZoneDep () {
      const tatag = ajZone({})
      editor.progfig.push({ type: 'zone', nom: tatag.nom, dep: [] })
      return editor.progfig.length - 1
    }

    function ajZoneSeg (ki) {
      if (!ki.c) {
        ki.c = { x: 200, y: 230 }
      }
      if (!ki.p) {
        ki.p = { x: 230, y: 230 }
      }
      if (!ki.nom) ki.nom = newNomZone()
      ki.tags = editor.progConst.makeZoneSeg(ki)
      editor.progConst.listeZone.push(ki)
      return ki
    }

    function ajZoneDepSeg () {
      const tatag = ajZoneSeg({})
      editor.progfig.push({ type: 'zoneseg', nom: tatag.nom, dep: [] })
      return editor.progfig.length - 1
    }

    function okPlaceCSeg (num) {
      okPlaceSeg(num)
      j3pEmpty(editor.lesdiv.actDepTitre)
      j3pEmpty(editor.lesdiv.actDep)
      affichelistedep()
      remetCool()
    }
    function okPlaceSeg (num) {
      const nn = editor.progfig[num]
      const nTags = retrouveTagsZone(num)
      nn.c = editor.mtgAppLecteur.getPointPosition({ a: nTags.centre })
      nn.p = editor.mtgAppLecteur.getPointPosition({ a: nTags.point })
      editor.progConst.mtgAppLecteur.setHidden({ elt: nTags.centre })
      editor.progConst.mtgAppLecteur.setHidden({ elt: nTags.point })
    }
    function okPlace (num) {
      const nn = editor.progfig[num]
      const nTags = retrouveTagsZone(num)
      nn.c = editor.mtgAppLecteur.getPointPosition({ a: nTags.centre })
      nn.p = editor.mtgAppLecteur.getPointPosition({ a: nTags.point })
      editor.progConst.mtgAppLecteur.setHidden({ elt: nTags.centre })
      editor.progConst.mtgAppLecteur.setHidden({ elt: nTags.point })
    }
    function replaceTouDeb (ki) {
      const num = retrouveDep(ki, 'num')
      const amod = retrouveListeDep([num])
      for (let i = 0; i < amod.length; i++) {
        replaceDeb(amod[i])
      }
    }

    function replaceDeb (num) {
      const ob = editor.progfig[num]
      editor.mtgAppLecteur.setApiDoc(svgId)
      switch (ob.type) {
        case 'point': {
          let x = (ob.cox) ? ob.cox : ''
          let y = (ob.coy) ? ob.coy : ''
          const h1 = ob.hazmin
          const h2 = ob.hazmax
          x = x.replace(/ /g, '')
          y = y.replace(/ /g, '')
          x = editor.progConst.recupe(x)
          y = editor.progConst.recupe(y)
          for (let i = 0; i < editor.progConst.listco.length; i++) {
            let regex = new RegExp(String(editor.progConst.listco[i].n) + '.x', 'g')
            x = x.replace(regex, parseFloat(editor.progConst.listco[i].x))
            y = y.replace(regex, parseFloat(editor.progConst.listco[i].x))
            regex = new RegExp(String(editor.progConst.listco[i].n) + '.y', 'g')
            x = x.replace(regex, parseFloat(editor.progConst.listco[i].y))
            y = y.replace(regex, parseFloat(editor.progConst.listco[i].y))
          }
          x = parseFloat(editor.progConst.calculFormule(x, 'valeur', h1, h2))
          y = parseFloat(editor.progConst.calculFormule(y, 'valeur', h1, h2))
          if (ob.sur) {
            const surkoi = retrouveDep({ nom: derecupe(ob.sur) }, 'objet')
            const coef = (!ob.coef) ? j3pGetRandomInt(0, 100) : Number(ob.coef)
            switch (surkoi.type) {
              case 'zoneseg': {
                const baseC = surkoi.c
                const baseP = surkoi.p
                x = String(baseC.x + ob.coef / 100 * (baseP.x - baseC.x))
                y = String(baseC.y + ob.coef / 100 * (baseP.y - baseC.y))
              }
                break
              case 'zone': {
                const baseC = surkoi.c
                const baseP = surkoi.p
                x = String(baseC.x + (editor.progConst.dist(baseC, baseP) * ob.coef / 100) * Math.cos(ob.coef * Math.PI / 10))
                y = String(baseC.y + (editor.progConst.dist(baseC, baseP) * ob.coef / 100) * Math.sin(ob.coef * Math.PI / 10))
              }
                break
              case 'segment': {
                const baseC = editor.progConst.getPointPosition2(editor.progConst.svgId, retrouvePoint(editor.progConst.recupe(surkoi.dep[0])).point)
                const baseP = editor.progConst.getPointPosition2(editor.progConst.svgId, retrouvePoint(editor.progConst.recupe(surkoi.dep[1])).point)
                x = String(baseC.x + ob.coef / 100 * (baseP.x - baseC.x))
                y = String(baseC.y + ob.coef / 100 * (baseP.y - baseC.y))
              }
                break
              case 'droite': {
                const leseg = editor.progConst.retrouveDroite(ob.sur)
                const nt1 = editor.progConst.newTag()
                const nt2 = editor.progConst.newTag()
                const nt3 = editor.progConst.newTag()
                const nt4 = editor.progConst.newTag()
                editor.progConst.mtgAppLecteur.addIntLineLine({
                  tag: nt1,
                  name: nt1,
                  d2: leseg,
                  d: 'segbord1'
                })
                editor.progConst.mtgAppLecteur.addIntLineLine({
                  tag: nt2,
                  name: nt2,
                  d2: leseg,
                  d: 'segbord2'
                })
                editor.progConst.mtgAppLecteur.addIntLineLine({
                  tag: nt3,
                  name: nt3,
                  d2: leseg,
                  d: 'segbord3'
                })
                editor.progConst.mtgAppLecteur.addIntLineLine({
                  tag: nt4,
                  name: nt4,
                  d2: leseg,
                  d: 'segbord4'
                })
                const coo1 = editor.progConst.getPointPosition2(editor.progConst.svgId, nt1)
                const coo2 = editor.progConst.getPointPosition2(editor.progConst.svgId, nt2)
                const coo3 = editor.progConst.getPointPosition2(editor.progConst.svgId, nt3)
                const coo4 = editor.progConst.getPointPosition2(editor.progConst.svgId, nt4)
                const litOk = []
                if (coo1.x !== undefined) litOk.push(coo1)
                if (coo2.x !== undefined) litOk.push(coo2)
                if (coo3.x !== undefined) litOk.push(coo3)
                if (coo4.x !== undefined) litOk.push(coo4)
                const baseC = litOk[0]
                const baseP = litOk[1]
                editor.progConst.mtgAppLecteur.deleteElt({ elt: nt1 })
                editor.progConst.mtgAppLecteur.deleteElt({ elt: nt2 })
                editor.progConst.mtgAppLecteur.deleteElt({ elt: nt3 })
                editor.progConst.mtgAppLecteur.deleteElt({ elt: nt4 })
                x = String(baseC.x + ob.coef / 100 * (baseP.x - baseC.x))
                y = String(baseC.y + ob.coef / 100 * (baseP.y - baseC.y))
              }
                break
              case 'demi-droite': {
                const leseg = editor.progConst.retrouveDemidOb(ob.sur)
                const nt1 = editor.progConst.newTag()
                const nt2 = editor.progConst.newTag()
                const nt3 = editor.progConst.newTag()
                const nt4 = editor.progConst.newTag()
                editor.progConst.mtgAppLecteur.addIntLineLine({
                  tag: nt1,
                  name: nt1,
                  d2: leseg.ligne,
                  d: 'segbord1'
                })
                editor.progConst.mtgAppLecteur.addIntLineLine({
                  tag: nt2,
                  name: nt2,
                  d2: leseg.ligne,
                  d: 'segbord2'
                })
                editor.progConst.mtgAppLecteur.addIntLineLine({
                  tag: nt3,
                  name: nt3,
                  d2: leseg.ligne,
                  d: 'segbord3'
                })
                editor.progConst.mtgAppLecteur.addIntLineLine({
                  tag: nt4,
                  name: nt4,
                  d2: leseg.ligne,
                  d: 'segbord4'
                })
                const coo1 = editor.progConst.getPointPosition2(editor.progConst.svgId, nt1)
                const coo2 = editor.progConst.getPointPosition2(editor.progConst.svgId, nt2)
                const coo3 = editor.progConst.getPointPosition2(editor.progConst.svgId, nt3)
                const coo4 = editor.progConst.getPointPosition2(editor.progConst.svgId, nt4)
                const litOk = []
                if (coo1.x !== undefined) litOk.push(coo1)
                if (coo2.x !== undefined) litOk.push(coo2)
                if (coo3.x !== undefined) litOk.push(coo3)
                if (coo4.x !== undefined) litOk.push(coo4)
                const baseC = litOk[0]
                const baseP = editor.progConst.getPointPosition2(editor.progConst.svgId, leseg.point1)
                editor.progConst.mtgAppLecteur.deleteElt({ elt: nt1 })
                editor.progConst.mtgAppLecteur.deleteElt({ elt: nt2 })
                editor.progConst.mtgAppLecteur.deleteElt({ elt: nt3 })
                editor.progConst.mtgAppLecteur.deleteElt({ elt: nt4 })
                x = String(baseC.x + ob.coef / 100 * (baseP.x - baseC.x))
                y = String(baseC.y + ob.coef / 100 * (baseP.y - baseC.y))
              }
                break
              case 'cercle': {
                const leseg = editor.progConst.retrouveArcOb(ob.sur)
                /*
                const litOk = []
                for (let i = 13; i < 25; i++) {
                  const nt1 = editor.progConst.newTag()
                  const nt2 = editor.progConst.newTag()
                  const nt3 = editor.progConst.newTag()
                  const nt4 = editor.progConst.newTag()
                  const nt12 = editor.progConst.newTag()
                  const nt22 = editor.progConst.newTag()
                  const nt32 = editor.progConst.newTag()
                  const nt42 = editor.progConst.newTag()
                  editor.progConst.mtgAppLecteur.addIntLineCircle({
                    tag: nt1,
                    name: nt1,
                    tag2: nt12,
                    name2: nt12,
                    c: leseg['arc' + (i - 12)],
                    d: 'segbord1'
                  })
                  editor.progConst.mtgAppLecteur.addIntLineCircle({
                    tag: nt2,
                    name: nt2,
                    tag2: nt22,
                    name2: nt22,
                    c: leseg['arc' + (i - 12)],
                    d: 'segbord1'
                  })
                  editor.progConst.mtgAppLecteur.addIntLineCircle({
                    tag: nt3,
                    name: nt3,
                    tag2: nt32,
                    name2: nt32,
                    c: leseg['arc' + (i - 12)],
                    d: 'segbord1'
                  })
                  editor.progConst.mtgAppLecteur.addIntLineCircle({
                    tag: nt4,
                    name: nt4,
                    tag2: nt42,
                    name2: nt42,
                    c: leseg['arc' + (i - 12)],
                    d: 'segbord1'
                  })
                  const coo1 = editor.progConst.getPointPosition2(editor.progConst.svgId, nt1)
                  const coo2 = editor.progConst.getPointPosition2(editor.progConst.svgId, nt2)
                  const coo3 = editor.progConst.getPointPosition2(editor.progConst.svgId, nt3)
                  const coo4 = editor.progConst.getPointPosition2(editor.progConst.svgId, nt4)
                  const coo12 = editor.progConst.getPointPosition2(editor.progConst.svgId, nt12)
                  const coo22 = editor.progConst.getPointPosition2(editor.progConst.svgId, nt22)
                  const coo32 = editor.progConst.getPointPosition2(editor.progConst.svgId, nt32)
                  const coo42 = editor.progConst.getPointPosition2(editor.progConst.svgId, nt42)
                  if (coo1.x !== undefined) litOk.push(coo1)
                  if (coo2.x !== undefined) litOk.push(coo2)
                  if (coo3.x !== undefined) litOk.push(coo3)
                  if (coo4.x !== undefined) litOk.push(coo4)
                  if (coo12.x !== undefined) litOk.push(coo12)
                  if (coo22.x !== undefined) litOk.push(coo22)
                  if (coo32.x !== undefined) litOk.push(coo32)
                  if (coo42.x !== undefined) litOk.push(coo42)
                }
                */
                const baseC = editor.progConst.getPointPosition2(editor.progConst.svgId, leseg.centre)
                const baseP = editor.progConst.getPointPosition2(editor.progConst.svgId, leseg.p1)
                x = String(baseC.x + (editor.progConst.dist(baseC, baseP)) * Math.cos(ob.coef / 50 * Math.PI))
                y = String(baseC.y + (editor.progConst.dist(baseC, baseP)) * Math.sin(ob.coef / 50 * Math.PI))
              }
                break
            }
            ob.coef = coef
          }
          if (ob.ext1) {
            const co1 = editor.mtgAppLecteur.getPointPosition({ a: retrouvePoint(editor.progConst.recupe(ob.ext1)).point })
            const co2 = editor.mtgAppLecteur.getPointPosition({ a: retrouvePoint(editor.progConst.recupe(ob.ext2)).point })
            x = (co1.x + co2.x) / 2
            y = (co1.y + co2.y) / 2
          }
          if (ob.centreRot) {
            const baseC = editor.mtgAppLecteur.getPointPosition({ a: retrouvePoint(editor.progConst.recupe(ob.centreRot)).point })
            const baseP = editor.mtgAppLecteur.getPointPosition({ a: retrouvePoint(editor.progConst.recupe(ob.image)).point })
            const angle = editor.progConst.calculFormule(String(ob.angle).replace(/à/g, String(ob.coef)).replace(/,/g, '.'), 'valeur') * Math.PI / 180
            const xM = baseP.x - baseC.x
            const yM = baseP.y - baseC.y
            x = xM * Math.cos(angle) + yM * Math.sin(angle) + baseC.x
            y = -xM * Math.sin(angle) + yM * Math.cos(angle) + baseC.y
          }
          if (ob.sur1) {
            const ob1 = editor.progConst.retrouveTruc(editor.progConst.recupe(ob.sur1))
            const ob2 = editor.progConst.retrouveTruc(editor.progConst.recupe(ob.sur2))
            const tagList = []
            let tagListPos = []
            switch (ob1.type) {
              case 'line':
                switch (ob2.type) {
                  case 'line': {
                    const intTag = editor.progConst.newTag()
                    editor.progConst.mtgAppLecteur.addIntLineLine({
                      tag: intTag,
                      name: intTag,
                      d2: ob1.tab[0],
                      d: ob2.tab[0]
                    })
                    tagList.push(intTag)
                  }
                    break
                  case 'cerc':
                    for (let i = 0; i < ob2.tab.length; i++) {
                      const intTag = editor.progConst.newTag()
                      const intTag2 = editor.progConst.newTag()
                      editor.progConst.mtgAppLecteur.addIntLineCircle({
                        tag: intTag,
                        name: intTag,
                        tag2: intTag2,
                        name2: intTag2,
                        c: ob2.tab[i],
                        d: ob1.tab[0]
                      })
                      tagList.push(intTag, intTag2)
                    }
                    break
                }
                break
              case 'cerc':
                switch (ob2.type) {
                  case 'line':
                    for (let i = 0; i < ob1.tab.length; i++) {
                      const intTag = editor.progConst.newTag()
                      const intTag2 = editor.progConst.newTag()
                      editor.progConst.mtgAppLecteur.addIntLineCircle({
                        tag: intTag,
                        name: intTag,
                        tag2: intTag2,
                        name2: intTag2,
                        c: ob1.tab[i],
                        d: ob2.tab[0]
                      })
                      tagList.push(intTag, intTag2)
                    }
                    break
                  case 'cerc':
                    for (let i = 0; i < ob1.tab.length; i++) {
                      for (let j = 0; j < ob2.tab.length; j++) {
                        const intTag = editor.progConst.newTag()
                        const intTag2 = editor.progConst.newTag()
                        editor.progConst.mtgAppLecteur.addIntCircleCircle({
                          tag: intTag,
                          name: intTag,
                          tag2: intTag2,
                          name2: intTag2,
                          c: ob1.tab[i],
                          c2: ob2.tab[j]
                        })
                        tagList.push(intTag, intTag2)
                      }
                    }
                    break
                }
                break
            }
            for (let i = tagList.length - 1; i > -1; i--) {
              const coo = editor.progConst.mtgAppLecteur.getPointPosition({ a: tagList[i] })
              if (coo.x === undefined || coo.y === undefined) continue
              if (coo.cox < -30) return
              if (coo.coy < -20) return
              if (coo.coy > 720) return
              if (coo.cox > 630) return
              tagListPos.push({ x: String(coo.x), y: String(coo.y) })
              editor.progConst.mtgAppLecteur.deleteElt({ elt: tagList[i] })
            }
            if (tagListPos.length === 0) return
            tagListPos = j3pShuffle(tagListPos)
            const posBuf = tagListPos.pop()
            x = posBuf.x
            y = posBuf.y
          }
          editor.mtgAppLecteur.setPointPosition(svgId, '#' + retrouvePoint(editor.progConst.recupe(ob.nom)).point, Number(x), Number(y), true)
          editor.progConst.listco.push({ n: editor.progConst.recupe(ob.nom), x, y })
        }
          break
        case 'segment': {
          const monSeg = retrouveSeg(ob)
          const de1 = editor.mtgAppLecteur.getPointPosition({ a: retrouvePoint(editor.progConst.recupe(ob.dep[0])).point })
          const de2 = editor.mtgAppLecteur.getPointPosition({ a: retrouvePoint(editor.progConst.recupe(ob.dep[1])).point })
          editor.mtgAppLecteur.setPointPosition(svgId, monSeg.point1, de1.x, de1.y, true)
          editor.mtgAppLecteur.setPointPosition(svgId, monSeg.point2, de2.x, de2.y, true)
          if (ob.aff) {
            if (ob.placeGarde) {
              const tagInt = editor.progConst.newTag()
              const tagPerp = editor.progConst.newTag()
              const tagcerc = editor.progConst.newTag()
              const tagPoint = editor.progConst.newTag()
              const tagPoint2 = editor.progConst.newTag()
              editor.progConst.mtgAppLecteur.calculate(editor.progConst.svgId)
              editor.progConst.mtgAppLecteur.addFreePoint({
                tag: tagInt,
                name: tagInt,
                x: de1.x + ob.placeGarde.abs * (de2.x - de1.x),
                y: de1.y + ob.placeGarde.abs * (de2.y - de1.y)
              })
              editor.progConst.mtgAppLecteur.addLinePerp({
                tag: tagPerp,
                a: tagInt,
                d: monSeg.ligne
              })
              editor.progConst.mtgAppLecteur.addCircleOr({
                tag: tagcerc,
                o: tagInt,
                r: ob.placeGarde.dist / editor.progConst.unite / 1.948717100000019
              })
              editor.progConst.mtgAppLecteur.addIntLineCircle({
                tag: tagPoint,
                name: tagPoint,
                tag2: tagPoint2,
                name2: tagPoint2,
                c: tagcerc,
                d: tagPerp
              })
              const toComp = (ob.placeGarde.ou12) ? tagPoint : tagPoint2
              const cooG = editor.progConst.mtgAppLecteur.getPointPosition({ a: toComp })
              editor.progConst.mtgAppLecteur.deleteElt({ elt: tagInt })
              editor.progConst.mtgAppLecteur.setPointPosition(editor.progConst.svgId, monSeg.pointNom, cooG.x, cooG.y, true)
            } else {
              let fax = ob.plx
              if (editor.progConst.txtF === 'FigEditor') {
                fax -= 200
              }
              editor.progConst.mtgAppLecteur.setPointPosition(editor.progConst.svgId, monSeg.pointNom, fax + 100, ob.ply - 170, true)
            }
            editor.progConst.mtgAppLecteur.setText(editor.progConst.svgId, monSeg.aff, ob.nom, true)
            editor.progConst.setColor2(editor.progConst.svgId, monSeg.aff, 0, 0, 0, true)
          }
        }
          break
        case 'droite': {
          const monSeg = retrouveDroite(ob)
          if (!ob.spe) {
            const pt1 = editor.mtgAppLecteur.getPointPosition({ a: retrouvePoint(editor.progConst.recupe(ob.dep[0])).point })
            const pt2 = editor.mtgAppLecteur.getPointPosition({ a: retrouvePoint(editor.progConst.recupe(ob.dep[1])).point })
            let a = (pt1.y - pt2.y) / (pt1.x - pt2.x)
            if (pt1.x === pt2.x) a = maxInt
            let b = pt1.y - a * pt1.x
            let lasol1
            if (ob.cox < 1000) {
              const sol = editor.progConst.getInstersectingPoints(a, b, pt1.x, pt1.y, ob.cox)
              let s1 = pt2.x - pt1.x
              let s2 = sol[0].x - pt1.x
              lasol1 = (s1 * s2 > 0) ? sol[1] : sol[0]
              if (pt1.x === pt2.x) {
                s1 = pt2.y - pt1.y
                s2 = sol[0].y - pt1.y
                if (s1 * s2 > 0) { lasol1 = sol[1] } else { lasol1 = sol[0] }
              }
            } else {
              lasol1 = pt1
            }
            b = pt2.y - a * pt2.x
            let lasol2
            if (ob.coy < 1000) {
              const sol = editor.progConst.getInstersectingPoints(a, b, pt2.x, pt2.y, ob.coy)
              let s1 = pt1.x - pt2.x
              let s2 = sol[0].x - pt2.x
              lasol2 = (s1 * s2 > 0) ? sol[1] : lasol2 = sol[0]
              if (pt1.x === pt2.x) {
                s1 = pt1.y - pt2.y
                s2 = sol[0].y - pt2.y
                lasol2 = (s1 * s2 > 0) ? sol[1] : lasol2 = sol[0]
              }
            } else {
              lasol2 = pt2
            }
            editor.mtgAppLecteur.setPointPosition(svgId, monSeg.point1, lasol1.x, lasol1.y, true)
            editor.mtgAppLecteur.setPointPosition(svgId, monSeg.point2, lasol2.x, lasol2.y, true)
            if (ob.aff) {
              if (ob.placeGarde) {
                const tagInt = editor.progConst.newTag()
                const tagPerp = editor.progConst.newTag()
                const tagcerc = editor.progConst.newTag()
                const tagPoint = editor.progConst.newTag()
                const tagPoint2 = editor.progConst.newTag()
                editor.progConst.mtgAppLecteur.calculate(editor.progConst.svgId)
                editor.progConst.mtgAppLecteur.addFreePoint({
                  tag: tagInt,
                  name: tagInt,
                  x: lasol1.x + ob.placeGarde.abs * (lasol2.x - lasol1.x),
                  y: lasol1.y + ob.placeGarde.abs * (lasol2.y - lasol1.y)
                })
                editor.progConst.mtgAppLecteur.addLinePerp({
                  tag: tagPerp,
                  a: tagInt,
                  d: monSeg.ligne
                })
                editor.progConst.mtgAppLecteur.addCircleOr({
                  tag: tagcerc,
                  o: tagInt,
                  r: ob.placeGarde.dist / editor.progConst.unite / 1.948717100000019
                })
                editor.progConst.mtgAppLecteur.addIntLineCircle({
                  tag: tagPoint,
                  name: tagPoint,
                  tag2: tagPoint2,
                  name2: tagPoint2,
                  c: tagcerc,
                  d: tagPerp
                })
                const toComp = (ob.placeGarde.ou12) ? tagPoint : tagPoint2
                const cooG = editor.progConst.mtgAppLecteur.getPointPosition({ a: toComp })
                editor.progConst.mtgAppLecteur.deleteElt({ elt: tagInt })
                editor.progConst.mtgAppLecteur.setPointPosition(editor.progConst.svgId, monSeg.pointNom, cooG.x, cooG.y, true)
              } else {
                let fax = ob.plx
                if (editor.progConst.txtF === 'FigEditor') {
                  fax -= 200
                }
                editor.progConst.mtgAppLecteur.setPointPosition(editor.progConst.svgId, monSeg.pointNom, fax + 100, ob.ply - 170, true)
              }
              editor.progConst.mtgAppLecteur.setText(editor.progConst.svgId, monSeg.aff, ob.nom, true)
              editor.progConst.setColor2(editor.progConst.svgId, monSeg.aff, 0, 0, 0, true)
            }
          } else {
            const pt1 = editor.mtgAppLecteur.getPointPosition({ a: retrouvePoint(editor.progConst.recupe(ob.pass)).point })
            const nn = editor.progConst.giveAB(ob.odt)
            let pt2
            if (ob.spe === 'parallèle') {
              pt2 = { x: 1000 }
              pt2.y = nn.a * pt2.x + (pt1.y - nn.a * pt1.x)
            } else {
              if (nn.a === 0) {
                pt2 = { x: pt1.x, y: 1000 }
              } else {
                pt2 = { x: 1000 }
                pt2.y = (-1 / nn.a) * pt2.x + (pt1.y - (-1 / nn.a) * pt1.x)
              }
            }
            editor.progConst.mtgAppLecteur.setPointPosition(editor.progConst.svgId, monSeg.point1, pt1.x, pt1.y, true)
            editor.progConst.mtgAppLecteur.setPointPosition(editor.progConst.svgId, monSeg.point2, pt2.x, pt2.y, true)
            if (ob.aff) {
              if (ob.placeGarde) {
                const tagInt = editor.progConst.newTag()
                const tagPerp = editor.progConst.newTag()
                const tagPerpA = editor.progConst.newTag()
                const tagcerc = editor.progConst.newTag()
                const tagPoint = editor.progConst.newTag()
                const tagPointA = editor.progConst.newTag()
                const tagPoint2 = editor.progConst.newTag()
                editor.progConst.mtgAppLecteur.calculate(editor.progConst.svgId)
                editor.progConst.mtgAppLecteur.addLinePerp({
                  tag: tagPerpA,
                  a: 'Z2',
                  d: monSeg.ligne
                })
                editor.progConst.mtgAppLecteur.addIntLineLine({
                  tag: tagPointA,
                  name: tagPointA,
                  d2: monSeg.ligne,
                  d: tagPerpA
                })
                const ccoG = editor.progConst.getPointPosition2(editor.progConst.svgId, tagPointA)
                editor.progConst.mtgAppLecteur.addFreePoint({
                  tag: tagInt,
                  name: tagInt,
                  x: pt1.x + ob.placeGarde.abs * (ccoG.x - pt1.x),
                  y: pt1.y + ob.placeGarde.abs * (ccoG.y - pt1.y)
                })
                editor.progConst.mtgAppLecteur.addLinePerp({
                  tag: tagPerp,
                  a: tagInt,
                  d: monSeg.ligne
                })
                editor.progConst.mtgAppLecteur.addCircleOr({
                  tag: tagcerc,
                  o: tagInt,
                  r: ob.placeGarde.dist / editor.progConst.unite / 1.948717100000019
                })
                editor.progConst.mtgAppLecteur.addIntLineCircle({
                  tag: tagPoint,
                  name: tagPoint,
                  tag2: tagPoint2,
                  name2: tagPoint2,
                  c: tagcerc,
                  d: tagPerp
                })
                const toComp = (!ob.placeGarde.ou12) ? tagPoint : tagPoint2
                const cooG = editor.progConst.mtgAppLecteur.getPointPosition({ a: toComp })
                editor.progConst.mtgAppLecteur.setPointPosition(editor.progConst.svgId, '#' + monSeg.pointNom, cooG.x, cooG.y, true)
                editor.progConst.mtgAppLecteur.deleteElt({ elt: tagPerpA })
                editor.progConst.mtgAppLecteur.deleteElt({ elt: tagInt })
              } else {
                let fax = ob.plx
                if (editor.progConst.txtF === 'FigEditor') {
                  fax -= 200
                }
                editor.progConst.mtgAppLecteur.setPointPosition(editor.progConst.svgId, '#' + monSeg.pointNom, fax + 100, ob.ply - 170, true)
              }
              editor.progConst.mtgAppLecteur.setText(editor.progConst.svgId, '#' + monSeg.aff, ob.nom, true)
            }
          }
        }
          break
        case 'demi-droite': {
          const monSeg = retrouveDemiDroite(ob)
          const pt1 = editor.mtgAppLecteur.getPointPosition({ a: retrouvePoint(editor.progConst.recupe(ob.dep[0])).point })
          const pt2 = editor.mtgAppLecteur.getPointPosition({ a: retrouvePoint(editor.progConst.recupe(ob.dep[1])).point })
          let a = (pt1.y - pt2.y) / (pt1.x - pt2.x)
          if (pt1.x === pt2.x) a = maxInt
          let b = pt1.y - a * pt1.x
          b = pt2.y - a * pt2.x
          let lasol2
          if (ob.coy < 1000 || ob.cox < 1000) {
            const sol = editor.progConst.getInstersectingPoints(a, b, pt2.x, pt2.y, ob.coy)
            let s1 = pt1.x - pt2.x
            let s2 = sol[0].x - pt2.x
            lasol2 = (s1 * s2 > 0) ? sol[1] : lasol2 = sol[0]
            if (pt1.x === pt2.x) {
              s1 = pt1.y - pt2.y
              s2 = sol[0].y - pt2.y
              lasol2 = (s1 * s2 > 0) ? sol[1] : lasol2 = sol[0]
            }
          } else {
            lasol2 = pt2
          }
          editor.mtgAppLecteur.setPointPosition(svgId, monSeg.point1, pt1.x, pt1.y, true)
          editor.mtgAppLecteur.setPointPosition(svgId, monSeg.point2, lasol2.x, lasol2.y, true)
          if (ob.aff) {
            if (ob.placeGarde) {
              const tagInt = editor.progConst.newTag()
              const tagPerp = editor.progConst.newTag()
              const tagcerc = editor.progConst.newTag()
              const tagPoint = editor.progConst.newTag()
              const tagPoint2 = editor.progConst.newTag()
              editor.progConst.mtgAppLecteur.calculate(editor.progConst.svgId)
              editor.progConst.mtgAppLecteur.addFreePoint({
                tag: tagInt,
                name: tagInt,
                x: pt1.x + ob.placeGarde.abs * (lasol2.x - pt1.x),
                y: pt1.y + ob.placeGarde.abs * (lasol2.y - pt1.y)
              })
              editor.progConst.mtgAppLecteur.addLinePerp({
                tag: tagPerp,
                a: tagInt,
                d: monSeg.ligne
              })
              editor.progConst.mtgAppLecteur.addCircleOr({
                tag: tagcerc,
                o: tagInt,
                r: ob.placeGarde.dist / editor.progConst.unite / 1.948717100000019
              })
              editor.progConst.mtgAppLecteur.addIntLineCircle({
                tag: tagPoint,
                name: tagPoint,
                tag2: tagPoint2,
                name2: tagPoint2,
                c: tagcerc,
                d: tagPerp
              })
              const toComp = (ob.placeGarde.ou12) ? tagPoint : tagPoint2
              const cooG = editor.progConst.mtgAppLecteur.getPointPosition({ a: toComp })
              editor.progConst.mtgAppLecteur.deleteElt({ elt: tagInt })
              editor.progConst.mtgAppLecteur.setPointPosition(editor.progConst.svgId, monSeg.pointNom, cooG.x, cooG.y, true)
            } else {
              let fax = ob.plx
              if (editor.progConst.txtF === 'FigEditor') {
                fax -= 200
              }
              editor.progConst.mtgAppLecteur.setPointPosition(editor.progConst.svgId, monSeg.pointNom, fax + 100, ob.ply - 170, true)
            }
            editor.progConst.mtgAppLecteur.setText(editor.progConst.svgId, monSeg.aff, ob.nom, true)
            editor.progConst.setColor2(editor.progConst.svgId, monSeg.aff, 0, 0, 0, true)
          }
        }
          break
        case 'cercle': {
          const monSeg = retrouveCercle(ob)
          const pt1 = editor.mtgAppLecteur.getPointPosition({ a: retrouvePoint(editor.progConst.recupe(ob.dep[0])).point })
          let r, pt2
          if (ob.def === 'passe') {
            pt2 = editor.mtgAppLecteur.getPointPosition({ a: retrouvePoint(editor.progConst.recupe(ob.dep[1])).point })
            r = editor.progConst.dist(pt1, pt2)
          } else {
            r = parseFloat(editor.progConst.calculFormule(ob.ray, 'valeur', 1, 100, ob.coef)) * editor.progConst.unite
            pt2 = { x: pt1.x + 20, y: pt1.y }
          }
          editor.progConst.mtgAppLecteur.setPointPosition(svgId, monSeg.centre, pt1.x, pt1.y, true)
          for (let i = 1; i < 13; i++) {
            editor.mtgAppLecteur.setPointPosition(svgId, monSeg['p' + i], pt1.x + r * Math.cos(i * 30 * Math.PI / 180), pt1.y + r * Math.sin(i * 30 * Math.PI / 180), true)
          }
          if (ob.affnom) {
            if (ob.placeGarde) {
              editor.progConst.placeNomPoint(retrouvePoint(editor.progConst.recupe(ob.dep[0])).point, r, ob.placeGarde, monSeg.pointNom, true)
            } else {
              let fax = ob.plx
              if (editor.progConst.txtF === 'FigEditor') {
                fax -= 200
              }
              editor.progConst.mtgAppLecteur.setPointPosition(this.progConst.svgId, '#' + ob.pointNom, fax, ob.ply, true)
            }
            editor.progConst.mtgAppLecteur.setText(editor.progConst.svgId, '#' + monSeg.aff, ob.nom, true)
            editor.progConst.setColor2(editor.progConst.svgId, monSeg.aff, ob.affnom, 0, 0, 0, true)
          }
        }
          break
      }
    }
    function retrouveSeg (ob) {
      const nom = (ob.aff) ? ob.aff : editor.progConst.recupe(ob.nom)
      for (let i = 0; i < editor.progConst.listeSegPris.length; i++) {
        if (editor.progConst.listeSegPris[i].nom === nom) return editor.progConst.listeSegPris[i]
      }
    }
    function retrouveCercle (ob) {
      const nom = editor.progConst.recupe(ob.nom)
      for (let i = 0; i < editor.progConst.listeArcPris.length; i++) {
        if (editor.progConst.listeArcPris[i].nom === nom) return editor.progConst.listeArcPris[i]
      }
    }
    function retrouveDroite (ob) {
      const nom = (ob.aff) ? ob.aff : editor.progConst.recupe(ob.nom)
      for (let i = 0; i < editor.progConst.listeDtePris.length; i++) {
        if (editor.progConst.listeDtePris[i].nom === nom) return editor.progConst.listeDtePris[i]
      }
      for (let i = 0; i < editor.progConst.listeDemiDPris.length; i++) {
        if (editor.progConst.listeDemiDPris[i].nom === nom) return editor.progConst.listeDemiDPris[i]
      }
      for (let i = 0; i < editor.progConst.listeSegPris.length; i++) {
        if (editor.progConst.listeSegPris[i].nom === nom) return editor.progConst.listeSegPris[i]
      }
    }
    function retrouveDemiDroite (ob) {
      const nom = (ob.aff) ? ob.aff : editor.progConst.recupe(ob.nom)
      for (let i = 0; i < editor.progConst.listeDemiDPris.length; i++) {
        if (editor.progConst.listeDemiDPris[i].nom === nom) return editor.progConst.listeDemiDPris[i]
      }
      for (let i = 0; i < editor.progConst.listeSegPris.length; i++) {
        if (editor.progConst.listeSegPris[i].nom === nom) return editor.progConst.listeSegPris[i]
      }
    }
    function okNomDeb () {
      const nom = editor.zoneNom.reponse()
      if (nom === '') {
        setTimeout(() => { editor.zoneNom.focus() }, 10)
        prvieN('Il faut nommer ce point,<br> ou choisir "nom aléatoire".')
        return
      }
      editor.newPoint = { type: 'point', nom }
      editor.zoneNom.disable()
      editor.editAVire = []
      nomPoint()
    }
    function nomAleaPoint () {
      const nom = trouveNewHaz()
      editor.newPoint = { type: 'point', nom }
      editor.zoneNom.disable()
      editor.editAVire = []
      nomPoint()
    }
    function nomPoint () {
      j3pEmpty(editor.tabDepAct[0][0])
      j3pAddContent(editor.tabDepAct[0][0], 'Point ' + editor.newPoint.nom)
      const nbpoint = nbpoints1()
      const nbobj1 = nbobj()
      const nbli = nblignes()
      j3pAjouteBouton(editor.tabDepAct[1][0], pointFix, { value: 'Point Fixe' })
      if (nbobj1 > 0) j3pAjouteBouton(editor.tabDepAct[2][0], pointSur, { value: 'Sur objet' })
      if (nbpoint > 1) j3pAjouteBouton(editor.tabDepAct[3][0], pointMilieu, { value: 'Milieu' })
      if (nbpoint > 1) j3pAjouteBouton(editor.tabDepAct[4][0], pointRot, { value: 'Rotation' })
      if (nbpoint > 1) j3pAjouteBouton(editor.tabDepAct[5][0], pointSym, { value: 'Symétrie' })
      if (nbli > 1) j3pAjouteBouton(editor.tabDepAct[6][0], pointInter, { value: 'Intersection' })
      j3pEmpty(editor.ouExplik)
    }
    /**
     * Retourne un nom HzN où N est entier et le nom n’existe pas dans nomprisProg[i].ki
     * @returns {string}
     */
    function trouveNewHaz () {
      const nomsExistants = editor.progConst.nomprisProg.map(nomPris => nomPris.ki)
      for (let i = 0; i < editor.Programme.length; i++) {
        if (editor.Programme[i].type === 'point') {
          nomsExistants.push(editor.Programme[i].nom)
        }
      }
      let i = 1
      while (nomsExistants.includes(`Hz${i}`)) i++
      return `Hz${i}`
    }
    function pointFix () {
      vireBouts()
      j3pAffiche(editor.tabDepAct[1][0], null, 'Clique sur la figure pour placer le point')
      editor.mtgAppLecteur.getDoc(svgId).defaultCursor = 'crosshair'
      editor.progConst.funcDown = (ev) => {
        const where = editor.progConst.leSvg.getBoundingClientRect()
        const x = ev.clientX - where.x
        const y = ev.clientY - where.y
        const cco = ccoBien(x, y)
        editor.newPoint.cox = cco.x
        editor.newPoint.coy = cco.y
        editor.progConst.funcDown = () => {}
        editor.newPoint.dep = []
        editor.progfig.push(editor.newPoint)
        j3pEmpty(editor.lesdiv.actDepTitre)
        j3pEmpty(editor.lesdiv.actDep)
        affichelistedep()
        remetCool()
      }
    }
    function ccoBien (x, y) {
      const base = editor.progConst.mtgAppLecteur.getPointPosition({ a: 'emil' })
      const base0 = editor.progConst.mtgAppLecteur.getPointPosition({ a: 'ptCa3' })
      return { x: String(-42 + (x - base0.x) / (base.x - base0.x) * 342), y: String(-2 + (y - base0.y) / (base.y - base0.y) * 402) }
    }
    function pointMilieu () {
      vireBouts()
      const ll = ['...']
      for (let i = 0; i < editor.progConst.progfig.length; i++) {
        if (editor.progfig[i].type === 'point') ll.push(editor.progConst.recupe(editor.progConst.progfig[i].nom))
      }
      const ttab = addDefaultTable(editor.tabDepAct[1][0], 1, 5)
      j3pAddTxt(ttab[0][0], 'point milieu de [&nbsp;')
      j3pAddTxt(ttab[0][3], '&nbsp;]&nbsp;')
      editor.liste1 = ListeDeroulante.create(ttab[0][1], ll, ldOptions)
      editor.liste2 = ListeDeroulante.create(ttab[0][1], ll, ldOptions)
      j3pAjouteBouton(ttab[0][4], choixMilieu, { value: 'OK' })
      editor.editAVire = [editor.liste1, editor.liste2]
    }
    function choixMilieu () {
      if (!editor.liste1.changed) {
        prvieN('Il faut renseigner les deux points')
        return
      }
      if (!editor.liste2.changed) {
        prvieN('Il faut renseigner les deux points')
        return
      }
      if (editor.liste1.reponse === editor.liste2.reponse) {
        prvieN('Il faut donner deux points distincts')
        return
      }
      editor.editAVire = []
      editor.newPoint.ext1 = derecupe(editor.liste1.reponse)
      editor.newPoint.ext2 = derecupe(editor.liste2.reponse)
      editor.newPoint.dep = [derecupe(editor.liste1.reponse), derecupe(editor.liste2.reponse)]
      editor.liste1.disable()
      editor.liste2.disable()
      editor.progfig.push(editor.newPoint)
      j3pEmpty(editor.lesdiv.actDepTitre)
      j3pEmpty(editor.lesdiv.actDep)
      affichelistedep()
      remetCool()
    }
    function pointSym () {
      vireBouts()
      const ll = ['...']
      for (let i = 0; i < editor.progConst.progfig.length; i++) {
        if (editor.progfig[i].type === 'point') ll.push(editor.progConst.recupe(editor.progConst.progfig[i].nom))
      }
      const ttab = addDefaultTable(editor.tabDepAct[1][0], 4, 1)
      j3pAddTxt(ttab[0][0], 'centre:&nbsp;')
      j3pAddTxt(ttab[1][0], 'image de:&nbsp;')
      editor.liste1 = ListeDeroulante.create(ttab[0][0], ll, ldOptions)
      editor.liste2 = ListeDeroulante.create(ttab[1][0], ll, ldOptions)
      j3pAjouteBouton(ttab[3][0], choixSym, { value: 'OK' })
      editor.editAVire = [editor.liste1, editor.liste2]
    }
    function choixSegment () {
      if (!editor.liste1.changed) {
        prvieN('Il faut renseigner les deux extrémités')
        return
      }
      if (!editor.liste2.changed) {
        prvieN('Il faut renseigner les deux extrémités')
        return
      }
      if (editor.liste1.reponse === editor.liste2.reponse) {
        prvieN('Il faut deux points distincts')
        return
      }
      editor.liste1.disable()
      editor.liste2.disable()
      editor.editAVire = []
      j3pEmpty(editor.tabDepAct[0][0])
      j3pEmpty(editor.tabDepAct[1][0])
      j3pAddContent(editor.tabDepAct[0][0], 'Segment [' + editor.liste1.reponse + editor.liste2.reponse + ']')
      j3pAddContent(editor.tabDepAct[1][0], 'Nom à afficher<br> <i>( Si ce segment représente une droite )</i>')
      const ttab = addDefaultTable(editor.tabDepAct[2][0], 1, 3)
      j3pAddTxt(ttab[0][1], '&nbsp;')
      editor.zoneNom = new ZoneStyleMathquill1(ttab[0][0], { restric: '()0123456789ABCDEFGHIJKLMNOPQRSTUVWXYZabcdefghijklmnopqrstuvwxyz\'', lim: 3, limite: 3, enter: faisFinSeg })
      j3pAjouteBouton(ttab[0][2], faisFinSeg, { value: 'OK' })
      j3pAddContent(editor.tabDepAct[3][0], '<i>Laisser vide si rien à afficher</i>')
      editor.newDroite = { type: 'segment', nom: '[' + derecupe(editor.liste1.reponse) + derecupe(editor.liste2.reponse) + ']', dep: [derecupe(editor.liste1.reponse), derecupe(editor.liste2.reponse)] }
      editor.editAVire = [editor.zoneNom]
      afficheExplik2(editor.ouExplik, 'Au cas où les deux points ne sont pas visibles,<br>il peut être judicieux de nommer cette ligne.')
    }
    function faisFinSeg () {
      const yy = editor.zoneNom.reponse().replace(/ /g, '')
      editor.newDroite.aff = (yy !== '') ? yy : undefined
      editor.zoneNom.disable()
      if (!editor.newDroite.aff) {
        editor.progfig.push(editor.newDroite)
        j3pEmpty(editor.lesdiv.actDepTitre)
        j3pEmpty(editor.lesdiv.actDep)
        affichelistedep()
        remetCool()
      } else {
        editor.newDroite.nom = yy
        const newt = editor.progConst.newTag()
        const a = editor.progConst.retrouvePoint(editor.progConst.recupe(editor.newDroite.dep[0]))
        const b = editor.progConst.retrouvePoint(editor.progConst.recupe(editor.newDroite.dep[1]))
        editor.progConst.mtgAppLecteur.addLineAB({
          tag: newt,
          a,
          b
        })
        vireBouts()
        j3pAffiche(editor.tabDepAct[1][0], null, 'Clique sur la figure pour placer le nom de la droite')
        editor.mtgAppLecteur.getDoc(svgId).defaultCursor = 'crosshair'
        editor.progConst.funcDown = (ev) => {
          const where = editor.progConst.leSvg.getBoundingClientRect()
          const x = ev.clientX - where.x
          const y = ev.clientY - where.y
          const cco = ccoBien(x, y)
          editor.newDroite.plx = Number(cco.x)
          editor.newDroite.ply = Number(cco.y)
          editor.newDroite.placeGarde = givePLaceNom(newt, x, y, a, b)
          editor.progConst.funcDown = () => {}
          editor.progfig.push(editor.newDroite)
          j3pEmpty(editor.lesdiv.actDepTitre)
          j3pEmpty(editor.lesdiv.actDep)
          affichelistedep()
          remetCool()
        }
      }
    }
    function faisFinDdte () {
      const yy = editor.zoneNom.reponse().replace(/ /g, '')
      editor.newDroite.aff = (yy !== '') ? yy : undefined
      editor.zoneNom.disable()
      if (!editor.newDroite.aff) {
        editor.progfig.push(editor.newDroite)
        j3pEmpty(editor.lesdiv.actDepTitre)
        j3pEmpty(editor.lesdiv.actDep)
        affichelistedep()
        remetCool()
      } else {
        editor.newDroite.nom = yy
        const newt = editor.progConst.newTag()
        const a = editor.progConst.retrouvePoint(editor.progConst.recupe(editor.newDroite.dep[0]))
        const b = editor.progConst.retrouvePoint(editor.progConst.recupe(editor.newDroite.dep[1]))
        editor.progConst.mtgAppLecteur.addLineAB({
          tag: newt,
          a,
          b
        })
        vireBouts()
        j3pAffiche(editor.tabDepAct[1][0], null, 'Clique sur la figure pour placer le nom de la droite')
        editor.mtgAppLecteur.getDoc(svgId).defaultCursor = 'crosshair'
        editor.progConst.funcDown = (ev) => {
          const where = editor.progConst.leSvg.getBoundingClientRect()
          const x = ev.clientX - where.x
          const y = ev.clientY - where.y
          const cco = ccoBien(x, y)
          editor.newDroite.plx = Number(cco.x)
          editor.newDroite.ply = Number(cco.y)
          editor.newDroite.placeGarde = givePLaceNom(newt, x, y, a, b)
          editor.progConst.funcDown = () => {}
          editor.progfig.push(editor.newDroite)
          j3pEmpty(editor.lesdiv.actDepTitre)
          j3pEmpty(editor.lesdiv.actDep)
          affichelistedep()
          remetCool()
        }
      }
      editor.editAVire = []
    }
    function choixDemiDroite () {
      if (!editor.liste1.changed) {
        prvieN('Il faut renseigner l’origine')
        return
      }
      if (!editor.liste2.changed) {
        prvieN('Il faut renseigner le point de passage')
        return
      }
      if (editor.liste1.reponse === editor.liste2.reponse) {
        prvieN('Il faut renseigner deux points distincts')
        return
      }
      editor.liste1.disable()
      editor.liste2.disable()
      j3pEmpty(editor.tabDepAct[0][0])
      j3pEmpty(editor.tabDepAct[1][0])
      j3pAddContent(editor.tabDepAct[0][0], 'Demi-droite [' + editor.liste1.reponse + editor.liste2.reponse + ')')
      j3pAddContent(editor.tabDepAct[1][0], 'Nom à afficher<br> <i>( Si cette ligne représente une droite )</i>')
      const ttab = addDefaultTable(editor.tabDepAct[2][0], 1, 3)
      j3pAddTxt(ttab[0][1], '&nbsp;')
      editor.zoneNom = new ZoneStyleMathquill1(ttab[0][0], { restric: '()0123456789ABCDEFGHIJKLMNOPQRSTUVWXYZabcdefghijklmnopqrstuvwxyz\'', lim: 3, limite: 3, enter: faisFinDroite })
      j3pAjouteBouton(ttab[0][2], faisFinDdte, { value: 'OK' })
      j3pAddContent(editor.tabDepAct[3][0], '<i>Laisser vide si rien à afficher</i>')
      editor.newDroite = { type: 'demi-droite', nom: '[' + derecupe(editor.liste1.reponse) + derecupe(editor.liste2.reponse) + ')', dep: [derecupe(editor.liste1.reponse), derecupe(editor.liste2.reponse)], p1: derecupe(editor.liste1.reponse), p2: derecupe(editor.liste2.reponse), coy: 10001 }
      editor.editAVire = [editor.zoneNom]
      afficheExplik2(editor.ouExplik, 'Au cas où les deux points ne sont pas visibles,<br>il peut être judicieux de nommer cette ligne.')
    }

    function vireBouts () {
      j3pEmpty(editor.tabDepAct[1][0])
      j3pEmpty(editor.tabDepAct[2][0])
      j3pEmpty(editor.tabDepAct[3][0])
      j3pEmpty(editor.tabDepAct[4][0])
      j3pEmpty(editor.tabDepAct[5][0])
      j3pEmpty(editor.tabDepAct[6][0])
    }

    function blokDep (b) {
      for (let i = 0; i < editor.tabBoutDep.length; i++) {
        editor.tabBoutDep[i][0].disabled = b
        editor.tabBoutDep[i][1].disabled = b
      }
      editor.boutAjDep.disabled = b
      for (let i = 0; i < editor.acache.length; i++) {
        editor.acache[i].style.display = 'none'
      }
      j3pEmpty(editor.lesdiv.modifDep)
      editor.lesdiv.actDepTitre = j3pAddElt(editor.lesdiv.modifDep, 'div')
      editor.lesdiv.actDep = j3pAddElt(editor.lesdiv.modifDep, 'div')
      chCool1()
    }
    function ajouteDep () {
      if (editor.blop) return
      razDiv4()
      blokDep(true)
      j3pAffiche(editor.lesdiv.actDepTitre, null, 'Nouvel Element')
      editor.lesdiv.actDepTitre.style.border = '1px solid black'
      editor.lesdiv.actDepTitre.style.background = '#fff'
      editor.lesdiv.actDep.style.border = '1px solid black'
      editor.lesdiv.actDep.style.background = '#fff'
      editor.tabDepAct = addDefaultTable(editor.lesdiv.actDep, 9, 3)
      j3pAddContent(editor.tabDepAct[0][0], '<b>Type:&nbsp;</b>')
      editor.divAvide = j3pAddElt(editor.lesdiv.modifDep, 'div')
      j3pAjouteBouton(editor.divAvide, annuleDep, { value: 'Annuler ' })
      j3pAjouteBouton(editor.tabDepAct[0][1], chZone, { value: 'Zone circulaire ' })
      j3pAjouteBouton(editor.tabDepAct[1][1], chZoneSeg, { value: 'Zone segment ' })
      j3pAjouteBouton(editor.tabDepAct[2][1], chPoint, { value: 'Point ' })
      editor.prvien = j3pAddElt(editor.divAvide, 'div')
      editor.prvien.style.background = '#fff'
      editor.prvien.style.border = '1px solid black'
      editor.prvien.style.color = '#f00'
      editor.prvien.style.display = 'none'
      editor.prvien.style.padding = '5px'
      let cmpt = 3
      const nbpoint = nbpoints1()
      const nbligne = nblignes()
      if (nbpoint > 1) {
        j3pAjouteBouton(editor.tabDepAct[cmpt][1], chSegment, { value: 'Segment ' })
        cmpt++
      }
      if (nbpoint > 1 || (nbpoint > 0 && nbligne > 0)) {
        j3pAjouteBouton(editor.tabDepAct[cmpt][1], chDroite, { value: 'Droite ' })
        cmpt++
      }
      if (nbpoint > 1) {
        j3pAjouteBouton(editor.tabDepAct[cmpt][1], chDDroite, { value: 'Demi-droite ' })
        cmpt++
      }
      if (nbpoint > 0) {
        j3pAjouteBouton(editor.tabDepAct[cmpt][1], chCercle, { value: 'Cercle ' })
      }
      editor.editAVire = []
      editor.ouExplik = j3pAddElt(editor.lesdiv.actDep, 'div')
      afficheExplik2(editor.ouExplik, 'Les <b>Zones circulaires</b> et les <b>Zones segments</b><br> ne seront pas visibles par l\'élève.<br>Elles permettent de définir de zones de placement aléatoire<br> pour des points ( choisir "Point sur objet" ).')
    }
    function prvieN (tt) {
      editor.prvien.style.display = ''
      j3pEmpty(editor.prvien)
      j3pAddContent(editor.prvien, tt)
      setTimeout(() => {
        try {
          editor.prvien.style.display = 'none'
        } catch (e) {
          console.error('doit etre effacé déjà')
        }
      }, 2000)
    }
    function annuleDep () {
      affichelistedep()
      for (let i = editor.editAVire.length - 1; i > -1; i--) {
        editor.editAVire[i].disable()
        editor.editAVire.splice(i, 1)
      }
      remetCool()
    }
    function chEffectue (str) {
      for (let i = 0; i < 8; i++) {
        j3pEmpty(editor.tabDepAct[i][1])
      }
      const tatab = addDefaultTable(editor.tabDepAct[0][0], 2, 3)
      j3pAffiche(tatab[0][0], null, '&nbsp;' + str + '&nbsp;')
      return tatab
    }
    function chZone () {
      const nn = ajZoneDep()
      j3pEmpty(editor.divAvide)
      chEffectue('Zone circulaire ')
      j3pAffiche(editor.tabDepAct[1][0], null, 'Clique sur OK quand la zone est bien placée')
      editor.tabDepAct[1][0].style.color = '#0eb0cb'
      const nTags = retrouveTagsZone(nn)
      editor.progConst.mtgAppLecteur.setVisible({ elt: nTags.centre })
      editor.progConst.mtgAppLecteur.setVisible({ elt: nTags.point })
      editor.boutOKDep = j3pAjouteBouton(editor.tabDepAct[2][0], () => { okPlaceC(nn) }, { value: 'OK' })
    }
    function chZoneSeg () {
      const nn = ajZoneDepSeg()
      j3pEmpty(editor.divAvide)
      chEffectue('Zone segment ')
      j3pAffiche(editor.tabDepAct[1][0], null, 'Clic quand la zone segment est bien placée')
      editor.tabDepAct[1][0].style.color = '#0eb0cb'
      const nTags = retrouveTagsZone(nn)
      editor.progConst.mtgAppLecteur.setVisible({ elt: nTags.centre })
      editor.progConst.mtgAppLecteur.setVisible({ elt: nTags.point })
      editor.boutOKDep = j3pAjouteBouton(editor.tabDepAct[2][0], () => { okPlaceCSeg(nn) }, { value: 'OK' })
    }
    function chPoint () {
      const tatab = chEffectue('Point ')
      editor.zoneNom = new ZoneStyleMathquill1(tatab[0][1], { restric: "ABCDEFGHIJKLMNOPQRSTUVWXYZabcdefghijklmnopqrstuvwxyz0123456789'", lim: 1, limite: 1, enter: okNomDeb })
      j3pAjouteBouton(tatab[1][1], nomAleaPoint, { value: 'nom aléatoire' })
      j3pAddContent(tatab[0][2], '&nbsp;')
      j3pAjouteBouton(tatab[0][2], okNomDeb, { value: 'OK' })
      setTimeout(() => { editor.zoneNom.focus() }, 10)
      editor.editAVire = [editor.zoneNom]
      afficheExplik2(editor.ouExplik, 'Pour choisr un nom aléatoire, <br>cliquez sur le bouton "<b>nom aléatoire</b>".<br><i>Pour nous simplifier la vie, il seront appelés A, B, C, ... dans cet éditeur,<br>mais prendront un autre nom en exercice.</i><br> Si vous choisissez un nom précis,<br>tapez le dans la zone puis cliquez sur "OK".')
    }
    function chSegment () {
      j3pEmpty(editor.ouExplik)
      chEffectue('Segment ')
      const ll = ['...']
      for (let i = 0; i < editor.progConst.progfig.length; i++) {
        if (editor.progfig[i].type === 'point') ll.push(editor.progConst.recupe(editor.progConst.progfig[i].nom))
      }
      const ttab = addDefaultTable(editor.tabDepAct[1][0], 1, 5)
      j3pAddTxt(ttab[0][0], '[&nbsp;')
      j3pAddTxt(ttab[0][3], '&nbsp;]')
      editor.liste1 = ListeDeroulante.create(ttab[0][1], ll, ldOptions)
      editor.liste2 = ListeDeroulante.create(ttab[0][1], ll, ldOptions)
      j3pAjouteBouton(ttab[0][4], choixSegment, { value: 'OK' })
      editor.editAVire = [editor.liste1, editor.liste2]
    }
    function chDroite () {
      chEffectue('Droite ')
      j3pEmpty(editor.ouExplik)
      const nbpoint = nbpoints1()
      const nbligne = nblignes()
      if (nbligne === 0) {
        choixDroite1()
        return
      }
      if (nbpoint < 2) {
        ChoixDroiteParaperp()
        return
      }
      j3pAjouteBouton(editor.tabDepAct[1][0], choixDroite1, { value: 'Définie par 2 points' })
      j3pAjouteBouton(editor.tabDepAct[2][0], ChoixDroiteParaperp, { value: 'Parallèle/Perpend.' })
    }
    function chDDroite () {
      j3pEmpty(editor.ouExplik)
      chEffectue('Demi-droite ')
      const ll = ['...']
      for (let i = 0; i < editor.progfig.length; i++) {
        if (editor.progfig[i].type === 'point') ll.push(editor.progConst.recupe(editor.progConst.progfig[i].nom))
      }
      const ttab = addDefaultTable(editor.tabDepAct[1][0], 1, 5)
      j3pAddTxt(ttab[0][0], '[&nbsp;')
      j3pAddTxt(ttab[0][3], '&nbsp;)')
      editor.liste1 = ListeDeroulante.create(ttab[0][1], ll, ldOptions)
      editor.liste2 = ListeDeroulante.create(ttab[0][2], ll, ldOptions)
      j3pAjouteBouton(ttab[0][4], choixDemiDroite, { value: 'OK' })
      editor.editAVire = [editor.liste1, editor.liste2]
    }
    function chCercle () {
      j3pEmpty(editor.ouExplik)
      chEffectue('Cercle ')
      choixCerclepassantOuBien()
    }
    function choixCerclepassantOuBien () {
      j3pEmpty(editor.tabDepAct[1][0])
      j3pEmpty(editor.tabDepAct[2][0])
      const nbpoint = nbpoints1()
      if (nbpoint < 2) {
        choixCerclerayon()
        return
      }
      j3pAjouteBouton(editor.tabDepAct[1][0], choixCerclepassant1, { value: 'Défini par 2 points' })
      j3pAjouteBouton(editor.tabDepAct[2][0], choixCerclerayon, { value: 'Défini par centre et rayon' })
    }
    function choixCerclerayon () {
      j3pEmpty(editor.tabDepAct[1][0])
      j3pEmpty(editor.tabDepAct[2][0])
      const ttab = addDefaultTable(editor.tabDepAct[2][0], 1, 2)
      j3pAddTxt(ttab[0][0], 'centre:&nbsp;')
      const ll = ['...']
      for (let i = 0; i < editor.progfig.length; i++) {
        if (editor.progfig[i].type === 'point') ll.push(editor.progConst.recupe(editor.progConst.progfig[i].nom))
      }
      editor.liste1 = ListeDeroulante.create(ttab[0][1], ll, ldOptions)
      editor.newDroite = { type: 'cercle', def: 'rayon' }

      const tabTypeLongX = addDefaultTable(editor.tabDepAct[3][0], 2, 1)

      const tabformule = addDefaultTable(tabTypeLongX[1][0], 2, 3)
      j3pAddContent(tabformule[0][0], 'Rayon:&nbsp;')
      tabformule[0][2].style.color = '#085d03'
      j3pAddContent(tabformule[0][2], '&nbsp;<i>( Utilisez <b>à</b> pour une valeur aléatoire comprise entre 0 et 1. )</i>')
      editor.inputForm = new ZoneStyleMathquill2(tabformule[0][1], {
        restric: '0123456789,/+-*()',
        contenu: editor.newDroite.long
      })
      editor.inputForm.onchange = () => {
        editor.newDroite.long = derecupe(editor.inputForm.reponse())
      }
      const lesPoint = recupePointsDeb()
      for (let i = 0; i < lesPoint.length; i++) {
        for (let j = i + 1; j < lesPoint.length; j++) {
          j3pAjouteBouton(tabformule[1][1], () => {
            editor.inputForm.majaffiche(lesPoint[i] + lesPoint[j])
            setTimeout(function () { editor.inputForm.focus() }, 10)
          }, { value: lesPoint[i] + lesPoint[j] })
        }
      }
      const ttab4 = addDefaultTable(editor.tabDepAct[1][0], 3, 3)
      j3pAddTxt(ttab4[0][0], 'Nom:&nbsp;')
      j3pAddTxt(ttab4[1][0], 'Nom visible:&nbsp;')
      editor.cohceVisible = j3pAjouteCaseCoche(ttab4[1][1], { label: '' })
      editor.zoneNom = new ZoneStyleMathquill1(ttab4[0][1], { restric: 'ABCDEFGHIJKLMNOPQRSTUVWXYZ()abcdefghijklmnopqrstuvwxyz0123456789\'', lim: 3, limite: 3, enter: faisFinCerclepass })
      j3pAjouteBouton(ttab4[0][2], faisFinCercleRayon, { value: 'OK' })
      editor.editAVire = [editor.inputForm, editor.zoneNom, editor.liste1]
    }

    function faisFinCercleRayon () {
      if (!editor.liste1.changed) {
        prvieN('Il faut renseigner le centre')
        return
      }
      const yy = editor.zoneNom.reponse().replace(/ /g, '')
      if (yy === '') {
        prvieN('Il faut nommer ce cercle')
        return
      }
      const laform = editor.inputForm.reponse().replace(/ /g, '')
      if (laform === '') {
        prvieN('Il faut renseigner le rayon')
        return
      }
      editor.liste1.disable()
      editor.zoneNom.disable()
      editor.inputForm.disable()
      editor.newDroite = { type: 'cercle', def: 'ray', centre: derecupe(editor.liste1.reponse), ray: derecupe(laform).replace(/,/g, '.'), dep: [derecupe(editor.liste1.reponse)], affiche: editor.newDroite.longType }
      editor.newDroite.aff = (editor.cohceVisible.checked) ? yy : ''
      editor.newDroite.nom = yy
      const newtc = editor.progConst.newTag()
      const a = editor.progConst.retrouvePoint(editor.progConst.recupe(editor.newDroite.dep[0]))
      editor.progConst.mtgAppLecteur.addCircleOr({
        tag: newtc,
        o: a,
        r: parseFloat(editor.progConst.calculFormule(derecupe(laform), 'valeur', 1, 100))
      })
      if (editor.newDroite.aff) {
        editor.newDroite.affnom = true
        vireBouts()
        j3pAffiche(editor.tabDepAct[1][0], null, 'Clique sur la figure pour placer le nom du cercle')
        editor.mtgAppLecteur.getDoc(svgId).defaultCursor = 'crosshair'
        editor.progConst.funcDown = (ev) => {
          const where = editor.progConst.leSvg.getBoundingClientRect()
          const x = ev.clientX - where.x
          const y = ev.clientY - where.y
          const cco = ccoBien(x, y)
          editor.newDroite.plx = Number(cco.x)
          editor.newDroite.ply = Number(cco.y)
          editor.newDroite.placeGarde = givePLaceNomCercle(newtc, x, y, a)
          editor.progConst.funcDown = () => {}
          editor.progfig.push(editor.newDroite)
          j3pEmpty(editor.lesdiv.actDepTitre)
          j3pEmpty(editor.lesdiv.actDep)
          affichelistedep()
          remetCool()
        }
      } else {
        editor.progfig.push(editor.newDroite)
        j3pEmpty(editor.lesdiv.actDepTitre)
        j3pEmpty(editor.lesdiv.actDep)
        affichelistedep()
        remetCool()
      }
      editor.editAVire = []
    }
    function remetCool () {
      editor.blop = false
      editor.lesdiv.divUp.style.background = '#47ffff'
      editor.lesdiv.div1.style.background = '#ccccff'
      editor.lesdiv.div2.style.backgroundImage = 'linear-gradient(to left, rgba(245, 169, 107, 0.8) 30%, rgba(245, 220, 187, 0.4))'
      editor.lesdiv.div3.style.backgroundImage = 'linear-gradient(to right, rgba(187, 187, 187, 0.8) 30%, rgba(224, 224, 224, 0.5))'
      editor.lesdiv.div4.style.backgroundImage = 'linear-gradient(to right, rgba(187, 187, 187, 0.8) 30%, rgba(224, 224, 224, 0.5))'
    }
    function choixCerclepassant1 () {
      j3pEmpty(editor.tabDepAct[1][0])
      j3pEmpty(editor.tabDepAct[2][0])
      const ttab = addDefaultTable(editor.tabDepAct[2][0], 1, 2)
      const ttab2 = addDefaultTable(editor.tabDepAct[3][0], 1, 2)
      j3pAddTxt(ttab[0][0], 'centre:&nbsp;')
      j3pAddTxt(ttab2[0][0], 'passant par:&nbsp;')
      const ll = ['...']
      for (let i = 0; i < editor.progfig.length; i++) {
        if (editor.progfig[i].type === 'point') ll.push(editor.progConst.recupe(editor.progConst.progfig[i].nom))
      }
      editor.liste1 = ListeDeroulante.create(ttab[0][1], ll, ldOptions)
      editor.liste2 = ListeDeroulante.create(ttab2[0][1], ll, ldOptions)
      const ttab4 = addDefaultTable(editor.tabDepAct[1][0], 3, 3)
      j3pAddTxt(ttab4[0][0], 'Nom:&nbsp;')
      j3pAddTxt(ttab4[1][0], 'Nom visible:&nbsp;')
      editor.cohceVisible = j3pAjouteCaseCoche(ttab4[1][1], { label: '' })
      editor.zoneNom = new ZoneStyleMathquill1(ttab4[0][1], { restric: 'ABCDEFGHIJKLMNOPQRSTUVWXYZ()abcdefghijklmnopqrstuvwxyz0123456789\'', lim: 3, limite: 3, enter: faisFinCerclepass })
      j3pAjouteBouton(ttab4[0][2], faisFinCerclepass, { value: 'OK' })
      editor.editAVire = [editor.liste1, editor.liste2, editor.zoneNom]
    }

    function faisFinCerclepass () {
      if (!editor.liste1.changed) {
        prvieN('Il faut renseigner le centre')
        return
      }
      if (!editor.liste2.changed) {
        prvieN('Il faut renseigner le point de passage')
        return
      }
      if (editor.liste1.reponse === editor.liste2.reponse) {
        prvieN('Il faut renseigner deux points distincts')
        return
      }
      const yy = editor.zoneNom.reponse().replace(/ /g, '')
      if (yy === '') {
        prvieN('Il faut nommer ce cercle')
        return
      }
      editor.liste1.disable()
      editor.liste2.disable()
      editor.zoneNom.disable()
      editor.newDroite = { type: 'cercle', def: 'passe', centre: derecupe(editor.liste1.reponse), par: derecupe(editor.liste2.reponse), dep: [derecupe(editor.liste1.reponse), derecupe(editor.liste2.reponse)] }
      editor.newDroite.aff = (editor.cohceVisible.checked) ? yy : ''
      editor.newDroite.nom = yy
      const newtc = editor.progConst.newTag()
      const a = editor.progConst.retrouvePoint(editor.progConst.recupe(editor.newDroite.dep[0]))
      const b = editor.progConst.retrouvePoint(editor.progConst.recupe(editor.newDroite.dep[1]))
      editor.progConst.mtgAppLecteur.addCircleOA({
        tag: newtc,
        o: a,
        a: b
      })
      if (editor.newDroite.aff) {
        editor.newDroite.affnom = true
        vireBouts()
        j3pAffiche(editor.tabDepAct[1][0], null, 'Clique sur la figure pour placer le nom du cercle')
        editor.mtgAppLecteur.getDoc(svgId).defaultCursor = 'crosshair'
        editor.progConst.funcDown = (ev) => {
          const where = editor.progConst.leSvg.getBoundingClientRect()
          const x = ev.clientX - where.x
          const y = ev.clientY - where.y
          const cco = ccoBien(x, y)
          editor.newDroite.plx = Number(cco.x)
          editor.newDroite.ply = Number(cco.y)
          editor.newDroite.placeGarde = givePLaceNomCercle(newtc, x, y, a)
          editor.progConst.funcDown = () => {}
          editor.progfig.push(editor.newDroite)
          j3pEmpty(editor.lesdiv.actDepTitre)
          j3pEmpty(editor.lesdiv.actDep)
          affichelistedep()
          remetCool()
        }
      } else {
        editor.progfig.push(editor.newDroite)
        j3pEmpty(editor.lesdiv.actDepTitre)
        j3pEmpty(editor.lesdiv.actDep)
        affichelistedep()
        remetCool()
      }
      editor.editAVire = []
    }

    function chPlace (i) {
      return () => {
        blokDep(true)
        j3pAffiche(editor.lesdiv.actDepTitre, null, 'Mofidier un élément')
        editor.lesdiv.actDepTitre.style.border = '1px solid black'
        editor.lesdiv.actDepTitre.style.background = '#fff'
        editor.lesdiv.actDep.style.border = '1px solid black'
        editor.lesdiv.actDep.style.background = '#fff'
        j3pAffiche(editor.lesdiv.actDep, null, 'Clique sur la figure pour placer le nom de la droite')
        editor.mtgAppLecteur.getDoc(svgId).defaultCursor = 'crosshair'
        if (!editor.progfig[i].spe) {
          const newt = editor.progConst.newTag()
          const a = editor.progConst.retrouvePoint(editor.progConst.recupe(editor.progfig[i].dep[0]))
          const b = editor.progConst.retrouvePoint(editor.progConst.recupe(editor.progfig[i].dep[1]))
          editor.progConst.mtgAppLecteur.addLineAB({
            tag: newt,
            a,
            b
          })
          editor.progConst.funcDown = (ev) => {
            const where = editor.progConst.leSvg.getBoundingClientRect()
            const x = ev.clientX - where.x
            const y = ev.clientY - where.y
            const cco = ccoBien(x, y)
            editor.progfig[i].plx = Number(cco.x)
            editor.progfig[i].ply = Number(cco.y)
            editor.progfig[i].placeGarde = givePLaceNom(newt, x, y, a, b)
            editor.progConst.funcDown = () => {}
            j3pEmpty(editor.lesdiv.actDepTitre)
            j3pEmpty(editor.lesdiv.actDep)
            affichelistedep()
            remetCool()
          }
        } else {
          const newt = editor.progConst.newTag()
          const a = editor.progConst.retrouvePoint(editor.progConst.recupe(editor.progfig[i].dep[0]))
          const d = editor.progConst.retrouveDroite(editor.progConst.recupe(editor.progfig[i].dep[1]))
          const ff = (editor.progfig[i].spe === 'parallèle') ? editor.progConst.mtgAppLecteur.addLinePar : editor.progConst.mtgAppLecteur.addLinePerp
          ff({
            tag: newt,
            a,
            d,
            color: '#f00'
          })
          editor.progConst.funcDown = (ev) => {
            const where = editor.progConst.leSvg.getBoundingClientRect()
            const x = ev.clientX - where.x
            const y = ev.clientY - where.y
            const cco = ccoBien(x, y)
            editor.progfig[i].plx = Number(cco.x)
            editor.progfig[i].ply = Number(cco.y)
            editor.progfig[i].placeGarde = givePLaceNomP(newt, x, y, a)
            editor.progConst.funcDown = () => {}
            j3pEmpty(editor.lesdiv.actDepTitre)
            j3pEmpty(editor.lesdiv.actDep)
            affichelistedep()
            remetCool()
          }
        }
      }
    }
    function chPlaceC (i) {
      return () => {
        blokDep(true)
        j3pAffiche(editor.lesdiv.actDepTitre, null, 'Mofidier un élément')
        editor.lesdiv.actDepTitre.style.border = '1px solid black'
        editor.lesdiv.actDepTitre.style.background = '#fff'
        editor.lesdiv.actDep.style.border = '1px solid black'
        editor.lesdiv.actDep.style.background = '#fff'
        j3pAffiche(editor.lesdiv.actDep, null, 'Clique sur la figure pour placer le nom du cercle')
        editor.mtgAppLecteur.getDoc(svgId).defaultCursor = 'crosshair'

        const newtc = editor.progConst.newTag()
        const a = editor.progConst.retrouvePoint(editor.progConst.recupe(editor.progfig[i].dep[0]))
        if (editor.progfig[i].def === 'passe') {
          const b = editor.progConst.retrouvePoint(editor.progConst.recupe(editor.progfig[i].dep[1]))
          editor.progConst.mtgAppLecteur.addCircleOA({
            tag: newtc,
            o: a,
            a: b
          })
        } else {
          editor.progConst.mtgAppLecteur.addCircleOr({
            tag: newtc,
            o: a,
            r: parseFloat(editor.progConst.calculFormule(derecupe(editor.progfig[i].ray), 'valeur', 1, 100))
          })
        }

        editor.progConst.funcDown = (ev) => {
          const where = editor.progConst.leSvg.getBoundingClientRect()
          const x = ev.clientX - where.x
          const y = ev.clientY - where.y
          const cco = ccoBien(x, y)
          editor.progfig[i].plx = Number(cco.x)
          editor.progfig[i].affnom = true
          editor.progfig[i].ply = Number(cco.y)
          editor.progfig[i].placeGarde = givePLaceNomCercle(newtc, x, y, a)
          editor.progConst.funcDown = () => {}
          j3pEmpty(editor.lesdiv.actDepTitre)
          j3pEmpty(editor.lesdiv.actDep)
          affichelistedep()
          remetCool()
        }
      }
    }
    function faisFinDroite () {
      const yy = editor.zoneNom.reponse().replace(/ /g, '')
      editor.newDroite.aff = (yy !== '') ? yy : undefined
      editor.zoneNom.disable()
      editor.newDroite.cox = 10001
      editor.newDroite.coy = 10001
      const newt = editor.progConst.newTag()
      const a = editor.progConst.retrouvePoint(editor.progConst.recupe(editor.newDroite.dep[0]))
      const b = editor.progConst.retrouvePoint(editor.progConst.recupe(editor.newDroite.dep[1]))
      editor.progConst.mtgAppLecteur.addLineAB({
        tag: newt,
        a,
        b
      })
      if (editor.newDroite.aff) {
        vireBouts()
        editor.newDroite.nom = yy
        j3pAffiche(editor.tabDepAct[1][0], null, 'Clique sur la figure pour placer le nom de la droite')
        editor.mtgAppLecteur.getDoc(svgId).defaultCursor = 'crosshair'
        editor.progConst.funcDown = (ev) => {
          const where = editor.progConst.leSvg.getBoundingClientRect()
          const x = ev.clientX - where.x
          const y = ev.clientY - where.y
          const cco = ccoBien(x, y)
          editor.newDroite.plx = Number(cco.x)
          editor.newDroite.ply = Number(cco.y)
          editor.newDroite.placeGarde = givePLaceNom(newt, x, y, a, b)
          editor.progConst.funcDown = () => {}
          editor.progfig.push(editor.newDroite)
          j3pEmpty(editor.lesdiv.actDepTitre)
          j3pEmpty(editor.lesdiv.actDep)
          affichelistedep()
          remetCool()
        }
      } else {
        editor.progfig.push(editor.newDroite)
        j3pEmpty(editor.lesdiv.actDepTitre)
        j3pEmpty(editor.lesdiv.actDep)
        affichelistedep()
        remetCool()
      }
      editor.editAVire = []
    }

    function givePLaceNom (ob, x, y, a, b) {
      const tagPoint = editor.progConst.newTag()
      const tagDroite = editor.progConst.newTag()
      const tagInter = editor.progConst.newTag()
      const tagSym = editor.progConst.newTag()
      const tagSym2 = editor.progConst.newTag()
      const tagCer = editor.progConst.newTag()
      editor.progConst.mtgAppLecteur.addFreePoint({
        tag: tagPoint,
        name: tagPoint,
        x,
        y
      })
      editor.progConst.mtgAppLecteur.addLinePerp({
        tag: tagDroite,
        d: ob,
        a: tagPoint
      })
      editor.progConst.mtgAppLecteur.addIntLineLine({
        tag: tagInter,
        name: tagInter,
        d: tagDroite,
        d2: ob
      })
      const aCons = editor.progConst.mtgAppLecteur.getPointPosition({ a: tagInter })
      const aa = editor.progConst.mtgAppLecteur.getPointPosition({ a })
      const bb = editor.progConst.mtgAppLecteur.getPointPosition({ a: b })
      const aret = { abs: (aCons.x - aa.x) / (bb.x - aa.x) }
      if (bb.x === aa.x) aret.abs = (aCons.y - aa.y) / (bb.y - aa.y)
      aret.dist = editor.progConst.dist(aCons, { x, y }) * 1.948717100000019
      editor.progConst.mtgAppLecteur.addCircleOr({
        o: tagInter,
        r: 1,
        tag: tagCer
      })
      editor.progConst.mtgAppLecteur.addIntLineCircle({
        c: tagCer,
        d: tagDroite,
        tag: tagSym,
        name: tagSym,
        tag2: tagSym2,
        name2: tagSym2
      })
      const cooSym = editor.progConst.mtgAppLecteur.getPointPosition({ a: tagSym })
      const cooSym2 = editor.progConst.mtgAppLecteur.getPointPosition({ a: tagSym2 })
      aret.ou12 = editor.progConst.dist(cooSym, { x, y }) < editor.progConst.dist(cooSym2, { x, y })
      return aret
    }
    function givePLaceNomCercle (ob, x, y, a) {
      const tagPoint = editor.progConst.newTag()
      const tagDroite = editor.progConst.newTag()
      const tagInter1 = editor.progConst.newTag()
      const tagInter2 = editor.progConst.newTag()
      editor.progConst.mtgAppLecteur.addFreePoint({
        tag: tagPoint,
        name: tagPoint,
        x,
        y
      })
      editor.progConst.mtgAppLecteur.addLineAB({
        tag: tagDroite,
        a,
        b: tagPoint
      })
      editor.progConst.mtgAppLecteur.addIntLineCircle({
        tag: tagInter1,
        name: tagInter1,
        tag2: tagInter2,
        name2: tagInter2,
        d: tagDroite,
        c: ob
      })
      const aCons1 = editor.progConst.mtgAppLecteur.getPointPosition({ a: tagInter1 })
      const aCons2 = editor.progConst.mtgAppLecteur.getPointPosition({ a: tagInter2 })
      const aCons = (editor.progConst.dist(aCons1, { x, y }) > editor.progConst.dist(aCons2, { x, y })) ? aCons2 : aCons1
      const aret = { ou12: editor.progConst.dist(aCons1, { x, y }) <= editor.progConst.dist(aCons2, { x, y }) }
      const aa = editor.progConst.mtgAppLecteur.getPointPosition({ a })
      aret.dist = editor.progConst.dist(aCons, { x, y }) * 1.948717100000019
      const rayon = editor.progConst.dist(aCons, aa)
      aret.procha = editor.progConst.dist(aa, { x, y }) < rayon
      aret.rapx = (aCons.x - aa.x) / rayon
      aret.rapy = (aCons.y - aa.y) / rayon
      return aret
    }
    function givePLaceNomP (ob, x, y, a) {
      const tagPoint = editor.progConst.newTag()
      const tagDroite = editor.progConst.newTag()
      const tagDroiteA = editor.progConst.newTag()
      const tagInter = editor.progConst.newTag()
      const b = editor.progConst.newTag()
      const tagSym = editor.progConst.newTag()
      editor.progConst.mtgAppLecteur.addFreePoint({
        tag: tagPoint,
        name: tagPoint,
        x,
        y
      })
      editor.progConst.mtgAppLecteur.addLinePerp({
        tag: tagDroite,
        d: ob,
        a: tagPoint
      })
      editor.progConst.mtgAppLecteur.addIntLineLine({
        tag: tagInter,
        name: tagInter,
        d: tagDroite,
        d2: ob
      })
      editor.progConst.mtgAppLecteur.addLinePerp({
        tag: tagDroiteA,
        d: ob,
        a: 'Z2'
      })
      editor.progConst.mtgAppLecteur.addIntLineLine({
        tag: b,
        name: b,
        d: tagDroiteA,
        d2: ob
      })
      const aCons = editor.progConst.mtgAppLecteur.getPointPosition({ a: tagInter })
      const aa = editor.progConst.mtgAppLecteur.getPointPosition({ a })
      const bb = editor.progConst.mtgAppLecteur.getPointPosition({ a: b })
      const aret = { abs: (aCons.x - aa.x) / (bb.x - aa.x) }
      if (bb.x === aa.x) aret.abs = (aCons.y - aa.y) / (bb.y - aa.y)
      aret.dist = editor.progConst.dist(aCons, { x, y }) * 1.948717100000019
      editor.progConst.mtgAppLecteur.addImPointSymCent({
        o: tagInter,
        a: tagPoint,
        name: tagSym,
        tag: tagSym
      })
      const cooSym = editor.progConst.mtgAppLecteur.getPointPosition({ a: tagSym })
      const cooComp = editor.progConst.mtgAppLecteur.getPointPosition({ a: 'ptCa1' })
      aret.ou12 = editor.progConst.dist(cooComp, cooSym) > editor.progConst.dist(cooComp, { x, y })
      return aret
    }

    function choixDroite () {
      if (!editor.liste1.changed) {
        prvieN('Il faut renseigner les deux points')
        return
      }
      if (!editor.liste2.changed) {
        prvieN('Il faut renseigner les deux points')
        return
      }
      if (editor.liste1.reponse === editor.liste2.reponse) {
        prvieN('Il faut deux points distincts')
        return
      }
      editor.liste1.disable()
      editor.liste2.disable()
      editor.editAVire = []
      editor.newDroite = { type: 'droite', nom: '(' + derecupe(editor.liste1.reponse) + derecupe(editor.liste2.reponse) + ')', dep: [derecupe(editor.liste1.reponse), derecupe(editor.liste2.reponse)] }
      j3pEmpty(editor.tabDepAct[0][0])
      j3pEmpty(editor.tabDepAct[1][0])
      j3pAddTxt(editor.tabDepAct[0][0], 'Droite (' + editor.liste1.reponse + editor.liste2.reponse + ')')
      j3pAddTxt(editor.tabDepAct[1][0], 'Nom à afficher')
      const ttab = addDefaultTable(editor.tabDepAct[2][0], 1, 3)
      j3pAddTxt(ttab[0][1], '&nbsp;')
      editor.zoneNom = new ZoneStyleMathquill1(ttab[0][0], { restric: '()0123456789ABCDEFGHIJKLMNOPQRSTUVWXYZabcdefghijklmnopqrstuvwxyz\'', lim: 3, limite: 3, enter: faisFinDroite })
      j3pAjouteBouton(ttab[0][2], faisFinDroite, { value: 'OK' })
      j3pAddContent(editor.tabDepAct[3][0], '<i>Laisser vide si rien à afficher</i>')
      editor.editAVire = [editor.zoneNom]
      afficheExplik2(editor.ouExplik, 'Au cas où les deux points ne sont pas visibles,<br>il peut être judicieux de nommer cette droite.')
    }
    function ChoixDroiteParaperp () {
      editor.newDroite = { type: 'droite' }
      j3pEmpty(editor.tabDepAct[1][0])
      j3pEmpty(editor.tabDepAct[2][0])
      const ll = ['Choisir', 'parallèle', 'perpendiculaire']
      const ttab = addDefaultTable(editor.tabDepAct[2][0], 3, 5)
      j3pAddTxt(ttab[0][1], 'à&nbsp;')
      const yy = recupeLigne()
      yy.splice(0, 0, 'Choisir')
      editor.liste1 = ListeDeroulante.create(ttab[0][0], ll, ldOptions)
      editor.liste2 = ListeDeroulante.create(ttab[0][1], yy, ldOptions)
      j3pAddTxt(ttab[1][0], 'passant par&nbsp;')
      const pp = recupePointsDeb()
      pp.splice(0, 0, '...')
      editor.liste3 = ListeDeroulante.create(ttab[1][1], pp, ldOptions)
      j3pAjouteBouton(ttab[2][0], choixDroitep, { value: 'OK' })
      editor.editAVire = [editor.liste1, editor.liste2, editor.liste3]
    }
    function choixDroite1 () {
      j3pEmpty(editor.tabDepAct[2][0])
      j3pEmpty(editor.tabDepAct[1][0])
      const ll = ['...']
      for (let i = 0; i < editor.progConst.progfig.length; i++) {
        if (editor.progfig[i].type === 'point') ll.push(editor.progConst.recupe(editor.progConst.progfig[i].nom))
      }
      const ttab = addDefaultTable(editor.tabDepAct[1][0], 1, 5)
      j3pAddTxt(ttab[0][0], '(&nbsp;')
      j3pAddTxt(ttab[0][3], '&nbsp;)')
      editor.liste1 = ListeDeroulante.create(ttab[0][1], ll, ldOptions)
      editor.liste2 = ListeDeroulante.create(ttab[0][1], ll, ldOptions)
      j3pAjouteBouton(ttab[0][4], choixDroite, { value: 'OK' })
      editor.editAVire = [editor.liste1, editor.liste2]
    }
    function choixDroitep () {
      if (!editor.liste1.changed) {
        prvieN('Il faut choisir "parallèle" ou "perpendiculaire"')
        return
      }
      if (!editor.liste2.changed) {
        prvieN('Il faut donner une ligne de référence')
        return
      }
      if (!editor.liste3.changed) {
        prvieN('Il faut donner le point de passage')
        return
      }
      editor.liste1.disable()
      editor.liste2.disable()
      editor.liste3.disable()
      editor.newDroite.pass = derecupe(editor.liste3.reponse)
      editor.newDroite.odt = derecupe(editor.liste2.reponse)
      editor.newDroite.dep = [derecupe(editor.liste3.reponse), derecupe(editor.liste2.reponse)]
      editor.newDroite.spe = (editor.liste1.reponse === 'parallèle') ? 'parallèle' : 'perp'
      j3pEmpty(editor.tabDepAct[2][0])
      j3pEmpty(editor.tabDepAct[1][0])
      j3pAddTxt(editor.tabDepAct[1][0], 'Nom à afficher')
      const ttab = addDefaultTable(editor.tabDepAct[2][0], 1, 3)
      j3pAddTxt(ttab[0][1], '&nbsp;')
      editor.zoneNom = new ZoneStyleMathquill1(ttab[0][0], { restric: '()0123456789ABCDEFGHIJKLMNOPQRSTUVWXYZabcdefghijklmnopqrstuvwxyz\'', lim: 3, limite: 3, enter: faisFinDroite })
      j3pAjouteBouton(ttab[0][2], choixDroitep2, { value: 'OK' })
      editor.editAVire = [editor.zoneNom]
    }
    function choixDroitep2 () {
      if (editor.zoneNom.reponse() === '') {
        prvieN('Il faut nommer cette droite')
        return
      }
      editor.newDroite.aff = editor.zoneNom.reponse().replace(/ /g, '')
      editor.newDroite.nom = editor.zoneNom.reponse().replace(/ /g, '')
      editor.zoneNom.disable()
      editor.newDroite.cox = 10001
      editor.newDroite.coy = 10001
      vireBouts()
      j3pAffiche(editor.tabDepAct[1][0], null, 'Clique sur la figure pour placer le nom de la droite')
      editor.mtgAppLecteur.getDoc(svgId).defaultCursor = 'crosshair'
      const newt = editor.progConst.newTag()
      const a = editor.progConst.retrouvePoint(editor.progConst.recupe(editor.newDroite.dep[0]))
      const d = editor.progConst.retrouveDroite(editor.progConst.recupe(editor.newDroite.dep[1]))
      const ff = (editor.newDroite.spe === 'parallèle') ? editor.progConst.mtgAppLecteur.addLinePar : editor.progConst.mtgAppLecteur.addLinePerp
      ff({
        tag: newt,
        a,
        d,
        color: '#f00'
      })
      editor.progConst.funcDown = (ev) => {
        const where = editor.progConst.leSvg.getBoundingClientRect()
        const x = ev.clientX - where.x
        const y = ev.clientY - where.y
        const cco = ccoBien(x, y)
        editor.newDroite.plx = Number(cco.x)
        editor.newDroite.ply = Number(cco.y)
        editor.newDroite.placeGarde = givePLaceNomP(newt, x, y, a)
        editor.progConst.funcDown = () => {}
        editor.progfig.push(editor.newDroite)
        j3pEmpty(editor.lesdiv.actDepTitre)
        j3pEmpty(editor.lesdiv.actDep)
        affichelistedep()
        remetCool()
      }
      editor.editAVire = []
    }

    function nbpoints1 (num) {
      let nb = 0
      const lim = (num === undefined) ? editor.progfig.length : num
      for (let i = 0; i < lim; i++) {
        if (editor.progfig[i].type === 'point') nb++
      }
      return nb
    }
    function nblignes (num) {
      let nb = 0
      const lim = (num === undefined) ? editor.progfig.length : num
      for (let i = 0; i < lim; i++) {
        if (editor.progfig[i].type === 'segment') nb++
        if (editor.progfig[i].type === 'droite') nb++
        if (editor.progfig[i].type === 'demi-droite') nb++
      }
      return nb
    }
    function nbobj (num) {
      let nb = 0
      const lim = (num === undefined) ? editor.progfig.length : num
      for (let i = 0; i < lim; i++) {
        if (editor.progfig[i].type === 'segment') nb++
        if (editor.progfig[i].type === 'droite') nb++
        if (editor.progfig[i].type === 'demi-droite') nb++
        if (editor.progfig[i].type === 'zone') nb++
        if (editor.progfig[i].type === 'zoneseg') nb++
        if (editor.progfig[i].type === 'cercle') nb++
      }
      return nb
    }
    function recupeligneTotal (lim) {
      const aret = []
      for (let i = 0; i < editor.progfig.length; i++) {
        if (editor.progfig[i].type === 'segment') aret.push(editor.progConst.recupe(editor.progfig[i].nom))
        if (editor.progfig[i].type === 'droite') aret.push(editor.progConst.recupe(editor.progfig[i].nom))
        if (editor.progfig[i].type === 'demidroite') aret.push(editor.progConst.recupe(editor.progfig[i].nom))
      }
      const lalim = lim || editor.Programme.length
      for (let i = 0; i < lalim; i++) {
        if (editor.Programme[i].type === 'segment') aret.push(editor.progConst.recupe(editor.Programme[i].nom))
        if (editor.Programme[i].type === 'droite') aret.push(editor.progConst.recupe(editor.Programme[i].nom))
        if (editor.Programme[i].type === 'demidroite') aret.push(editor.progConst.recupe(editor.Programme[i].nom))
      }
      return aret
    }
    function recupeLigne (num) {
      const aret = []
      const lim = (num === undefined) ? editor.progfig.length : num
      for (let i = 0; i < lim; i++) {
        if (editor.progfig[i].type === 'segment') aret.push(editor.progConst.recupe(editor.progfig[i].nom))
        if (editor.progfig[i].type === 'droite') aret.push(editor.progConst.recupe(editor.progfig[i].nom))
        if (editor.progfig[i].type === 'demidroite') aret.push(editor.progConst.recupe(editor.progfig[i].nom))
      }
      return aret
    }

    function recupeLescrea (n) {
      const aret = []
      const lim = n || editor.Programme.length
      for (let i = 0; i < lim; i++) {
        if (editor.Programme[i].type === 'segment') aret.push(editor.progConst.recupe(editor.Programme[i].nom))
        if (editor.Programme[i].type === 'droite') aret.push(editor.progConst.recupe(editor.Programme[i].nom))
        if (editor.Programme[i].type === 'demidroite') aret.push(editor.progConst.recupe(editor.Programme[i].nom))
        if (editor.Programme[i].type === 'cercle') aret.push(editor.Programme[i].nom)
        if (editor.Programme[i].type === 'point') aret.push(editor.progConst.recupe(editor.Programme[i].nom))
      }
      return aret
    }
    function recupeContenants () {
      const aret = []
      for (let i = 0; i < editor.progfig.length; i++) {
        if (editor.progfig[i].type === 'segment') aret.push(editor.progConst.recupe(editor.progfig[i].nom))
        if (editor.progfig[i].type === 'droite') aret.push(editor.progConst.recupe(editor.progfig[i].nom))
        if (editor.progfig[i].type === 'demi-droite') aret.push(editor.progConst.recupe(editor.progfig[i].nom))
        if (editor.progfig[i].type === 'cercle') aret.push(editor.progConst.recupe(editor.progfig[i].nom))
      }
      for (let i = 0; i < editor.Programme.length; i++) {
        if (editor.Programme[i].type === 'segment') aret.push(editor.Programme[i].nom)
        if (editor.Programme[i].type === 'droite') aret.push(editor.Programme[i].nom)
        if (editor.Programme[i].type === 'demidroite') aret.push(editor.Programme[i].nom)
        if (editor.Programme[i].type === 'cercle') aret.push(editor.Programme[i].nom)
      }
      return aret
    }
    function recupeZones () {
      const aret = []
      for (let i = 0; i < editor.progfig.length; i++) {
        if (editor.progfig[i].type === 'zone') aret.push(editor.progConst.recupe(editor.progfig[i].nom))
        if (editor.progfig[i].type === 'zoneseg') aret.push(editor.progConst.recupe(editor.progfig[i].nom))
      }
      return aret
    }
    /*
    function recupeContenantsProgramme () {
      const aret = []
      for (let i = 0; i < editor.Programme.length; i++) {
        if (editor.Programme[i].type === 'segment') aret.push(editor.Programme[i].nom)
        if (editor.Programme[i].type === 'droite') aret.push(editor.Programme[i].nom)
        if (editor.Programme[i].type === 'demidroite') aret.push(editor.Programme[i].nom)
        if (editor.Programme[i].type === 'cercle') aret.push(editor.Programme[i].nom)
      }
      return aret
    }
     */
    function recupePointsTotal (n) {
      const aret = []
      for (let i = 0; i < editor.progConst.progfig.length; i++) {
        if (editor.progfig[i].type === 'point') aret.push(editor.progConst.recupe(editor.progConst.progfig[i].nom))
      }
      for (let i = 0; i < n; i++) {
        if (editor.Programme[i].type === 'point') aret.push(editor.progConst.recupe(editor.Programme[i].nom))
      }
      return aret
    }
    function recupePointsDeb (num) {
      const aret = []
      const lim = (num === undefined) ? editor.progfig.length : num
      for (let i = 0; i < lim; i++) {
        if (editor.progfig[i].type === 'point') aret.push(editor.progConst.recupe(editor.progConst.progfig[i].nom))
      }
      return aret
    }
    function retrouveDep (ob, quoi) {
      for (let i = 0; i < editor.progfig.length; i++) {
        let ok = true
        if (ob.nom) ok = ok && editor.progfig[i].nom === ob.nom
        if (ob.type) ok = ok && editor.progfig[i].type === ob.type
        if (ok) {
          if (quoi === 'objet') return editor.progfig[i]
          if (quoi === 'num') return i
        }
      }
    }
    function retrouvelesDependantsde (lenomacherche, letype, idep) {
      const aret = []
      for (let i = idep + 1; i < editor.Programme.length; i++) {
        let meti = false
        const proprog = editor.Programme[i]
        if (proprog.zone === lenomacherche) meti = true
        if (proprog.pointExt1 === lenomacherche) meti = true
        if (proprog.pointExt2 === lenomacherche) meti = true
        if (proprog.pass === lenomacherche) meti = true
        if (proprog.pass2 === lenomacherche) meti = true
        if (proprog.odt === lenomacherche) meti = true
        if (proprog.centre === lenomacherche) meti = true
        if (proprog.par === lenomacherche) meti = true
        if (letype === 'point') {
          if (proprog.rayon) {
            if (proprog.rayon.indexOf(lenomacherche) !== -1) meti = true
          }
        }
        if (proprog.origine === lenomacherche) meti = true
        if (proprog.point === lenomacherche) meti = true
        if (proprog.ki) {
          if (proprog.ki.nom === lenomacherche) meti = true
        }
        if (proprog.cond) {
          if (proprog.cond.length > 0) {
            if (proprog.cond[0].cop === lenomacherche) meti = true
            if (proprog.cond[0].cop2 === lenomacherche) meti = true
            if (proprog.cond[0].formule) {
              if (letype === 'point') {
                if (proprog.cond[0].formule.indexOf(lenomacherche) !== -1) meti = true
              }
            }
            if (proprog.cond[0].ki) {
              if (proprog.cond[0].ki.nom === lenomacherche) meti = true
            }
          }
          if (proprog.cond.length > 1) {
            if (proprog.cond[1].cop === lenomacherche) meti = true
            if (proprog.cond[1].cop2 === lenomacherche) meti = true
            if (proprog.cond[1].formule) {
              if (letype === 'point') {
                if (proprog.cond[1].formule.indexOf(lenomacherche) !== -1) meti = true
              }
            }
            if (proprog.cond[1].ki) {
              if (proprog.cond[1].ki.nom === lenomacherche) meti = true
            }
          }
        }
        if (proprog.type === 'multi') {
          for (let j = 0; j < proprog.cont.length; j++) {
            const proprog2 = editor.Programme[i].cont[j]
            if (proprog2.zone === lenomacherche) meti = true
            if (proprog2.pointExt1 === lenomacherche) meti = true
            if (proprog2.pointExt2 === lenomacherche) meti = true
            if (proprog2.pass === lenomacherche) meti = true
            if (proprog2.pass2 === lenomacherche) meti = true
            if (proprog2.odt === lenomacherche) meti = true
            if (proprog2.centre === lenomacherche) meti = true
            if (proprog2.par === lenomacherche) meti = true
            if (letype === 'point') {
              if (proprog2.rayon) {
                if (proprog2.rayon.indexOf(lenomacherche) !== -1) meti = true
              }
            }
            if (proprog2.origine === lenomacherche) meti = true
            if (proprog2.point === lenomacherche) meti = true
            if (proprog2.ki) {
              if (proprog2.ki.nom === lenomacherche) meti = true
            }
            if (proprog2.cond) {
              if (proprog2.cond.length > 0) {
                if (proprog2.cond[0].cop === lenomacherche) meti = true
                if (proprog2.cond[0].cop2 === lenomacherche) meti = true
                if (proprog2.cond[0].formule) {
                  if (letype === 'point') {
                    if (proprog2.cond[0].formule.indexOf(lenomacherche) !== -1) meti = true
                  }
                }
                if (proprog2.cond[0].ki) {
                  if (proprog2.cond[0].ki.nom === lenomacherche) meti = true
                }
              }
              if (proprog2.cond.length > 1) {
                if (proprog2.cond[1].cop === lenomacherche) meti = true
                if (proprog2.cond[1].cop2 === lenomacherche) meti = true
                if (proprog2.cond[1].formule) {
                  if (letype === 'point') {
                    if (proprog2.cond[1].formule.indexOf(lenomacherche) !== -1) meti = true
                  }
                }
                if (proprog2.cond[1].ki) {
                  if (proprog2.cond[1].ki.nom === lenomacherche) meti = true
                }
              }
            }
          }
        }
        if (meti) aret.push(i)
      }
      for (let i = 0; i < aret.length; i++) {
        const aaj = retrouvelesDependantsde(editor.Programme[aret[i]].nom, editor.Programme[aret[i]].type, aret[i])
        for (let j = 0; j < aaj.length; j++) {
          if (aret.indexOf(aaj[j]) === -1) aret.push(aaj[j])
        }
      }
      return aret
    }
    function supprimeDep (num) {
      const duprogdep = retrouvelesDependantsde(editor.progfig[num].nom, editor.progfig[num].type, -1)
      const avire = retrouveListeDep([num])
      for (let i = 0; i < avire.length; i++) {
        const aaj = retrouvelesDependantsde(editor.progfig[avire[i]].nom, editor.progfig[avire[i]].type, -1)
        for (let j = 0; j < aaj.length; j++) {
          if (duprogdep.indexOf(aaj[j]) === -1) duprogdep.push(aaj[j])
        }
      }
      if (duprogdep.length === 0) {
        FinsupprimeDep(num, [])
        return
      }
      const yy = j3pModale2({ titre: 'Avertissement ', contenu: '', divparent: container })
      const oudiv = j3pAddElt(yy, 'div')
      oudiv.style.color = '#f00'
      j3pAddContent(oudiv, 'Certaines constructions du programme élève<br>dépendent de cet élément.<br>Elles seront également supprimées du Programme.')
      j3pAddContent(yy, '\n')
      j3pAddContent(yy, '\n')
      j3pAjouteBouton(yy, () => {
        j3pDetruit(yy)
        j3pDetruit(yy.masque)
      }, { value: 'Annuler' })
      j3pAddContent(yy, '\n')
      j3pAddContent(yy, '\n')
      j3pAjouteBouton(yy, () => {
        j3pDetruit(yy)
        j3pDetruit(yy.masque)
        FinsupprimeDep(num, duprogdep)
      }, { value: 'Tant pis' })
    }
    function FinsupprimeDep (num, tabP) {
      if (editor.blop) return
      razDiv4()
      const avire = retrouveListeDep([num])
      for (let i = avire.length - 1; i > -1; i--) {
        editor.progfig.splice(avire[i], 1)
      }
      for (let i = tabP.length - 1; i > -1; i--) {
        editor.Programme.splice(tabP[i], 1)
      }
      afficheMenu()
      affichelistedep()
      remetCool()
    }
    function retrouveListeDep (tab) {
      const aret = j3pClone(tab)
      for (let i = tab[0] + 1; i < editor.progfig.length; i++) {
        for (const a of aret) {
          if (!editor.progfig[i].dep) editor.progfig[i].dep = []
          if (editor.progfig[i].dep.includes(editor.progfig[a].nom)) {
            aret.push(i)
            break
          }
        }
      }
      return aret
    }
    function modifDep (num) {
      if (editor.blop) return
      razDiv4()
      blokDep(true)
      j3pAddContent(editor.lesdiv.actDepTitre, '<b><u>Modifier un élement</u></b>')
      editor.lesdiv.actDepTitre.style.border = '1px solid black'
      editor.lesdiv.actDepTitre.style.background = '#fff'
      editor.lesdiv.actDep.style.border = '1px solid black'
      editor.lesdiv.actDep.style.background = '#fff'
      const amodif = editor.progfig[num]
      editor.divAvide = j3pAddElt(editor.lesdiv.modifDep, 'div')
      editor.editAVire = []
      j3pAjouteBouton(editor.divAvide, annuleDep, { value: 'Annuler ' })
      if (amodif.type === 'zone') {
        const nTags = retrouveTagsZone(num)
        editor.tabDepAct = addDefaultTable(editor.lesdiv.actDep, 7, 1)
        j3pAffiche(editor.tabDepAct[0][0], null, amodif.nom)
        j3pAffiche(editor.tabDepAct[1][0], null, 'Clic quand la zone est bien placée')
        editor.mtgAppLecteur.setVisible({ elt: nTags.centre })
        editor.mtgAppLecteur.setVisible({ elt: nTags.point })
        editor.boutOKDep = j3pAjouteBouton(editor.tabDepAct[2][0], () => { okPlaceC(num) }, { value: 'OK' })
      }
      if (amodif.type === 'zoneseg') {
        const nTags = retrouveTagsZone(num)
        editor.tabDepAct = addDefaultTable(editor.lesdiv.actDep, 7, 1)
        j3pAffiche(editor.tabDepAct[0][0], null, amodif.nom)
        j3pAffiche(editor.tabDepAct[1][0], null, 'Clic quand la zone segment est bien placée')
        editor.mtgAppLecteur.setVisible({ elt: nTags.centre })
        editor.mtgAppLecteur.setVisible({ elt: nTags.point })
        editor.boutOKDep = j3pAjouteBouton(editor.tabDepAct[2][0], () => { okPlaceCSeg(num) }, { value: 'OK' })
      }
      if (amodif.type === 'point') {
        editor.tabDepAct = addDefaultTable(editor.lesdiv.actDep, 8, 2)
        const aaj = (amodif.nom !== editor.progConst.recupe(amodif.nom)) ? ' (' + editor.progConst.recupe(amodif.nom) + ')' : ''
        j3pAffiche(editor.tabDepAct[0][0], null, 'Point ' + amodif.nom + aaj)
        j3pAjouteBouton(editor.tabDepAct[0][1], () => { ChgNomPoint(amodif) }, { value: 'Modifier' })
        let aaj2 = 'Point Fixe&nbsp;'
        if (amodif.sur) aaj2 = 'Point sur ' + editor.progConst.recupe(amodif.sur) + '&nbsp;'
        if (amodif.sur1) aaj2 = 'Point d\'intersection&nbsp;'
        if (amodif.ext1) {
          aaj2 = 'Point milieu de  [' + editor.progConst.recupe(amodif.ext1) + editor.progConst.recupe(amodif.ext2) + ']&nbsp;'
        }
        if (amodif.centreRot) {
          if (amodif.angle === 180) {
            aaj2 = 'Image de ' + editor.progConst.recupe(amodif.image) + '&nbsp;\n par symétrie&nbsp;\n centre ' + editor.progConst.recupe(amodif.centreRot) + '&nbsp;'
          } else {
            aaj2 = 'Image de ' + editor.progConst.recupe(amodif.image) + '&nbsp;\n par rotation&nbsp;\n centre ' + editor.progConst.recupe(amodif.centreRot) + '&nbsp;\n angle ' + amodif.angle + '°&nbsp;'
          }
        }
        j3pAffiche(editor.tabDepAct[1][0], null, aaj2)
        j3pAjouteBouton(editor.tabDepAct[1][1], () => { ChgTypePoint(amodif) }, { value: 'Modifier' })
      }
      if (amodif.type === 'droite') {
        editor.tabDepAct = addDefaultTable(editor.lesdiv.actDep, 8, 2)
        j3pAffiche(editor.tabDepAct[0][0], null, 'Droite ' + editor.progConst.recupe(amodif.nom) + '&nbsp;')
        j3pAjouteBouton(editor.tabDepAct[0][1], () => { ChgNomDte(amodif) }, { value: 'Modifier' })
        let aaj2 = 'Passant par 2 points&nbsp;'
        if (amodif.spe) {
          if (amodif.spe === 'perp') {
            aaj2 = 'Perpendiculaire&nbsp;'
          } else {
            aaj2 = 'Parallèle&nbsp;'
          }
        }
        j3pAffiche(editor.tabDepAct[1][0], null, aaj2)
        j3pAjouteBouton(editor.tabDepAct[1][1], () => { ChgTypeDte(amodif) }, { value: 'Modifier' })
      }
      if (amodif.type === 'segment') {
        editor.tabDepAct = addDefaultTable(editor.lesdiv.actDep, 8, 2)
        chEffectue('Segment ')
        const ll = ['...']
        for (let i = 0; i < num; i++) {
          if (editor.progfig[i].type === 'point') ll.push(editor.progConst.recupe(editor.progConst.progfig[i].nom))
        }
        const ttab = addDefaultTable(editor.tabDepAct[1][0], 1, 5)
        j3pAddTxt(ttab[0][0], '[&nbsp;')
        j3pAddTxt(ttab[0][3], '&nbsp;]')
        editor.liste1 = ListeDeroulante.create(ttab[0][1], ll, ldOptions)
        editor.liste2 = ListeDeroulante.create(ttab[0][1], ll, ldOptions)
        j3pAjouteBouton(ttab[0][4], () => { choixSegmentMod(amodif) }, { value: 'OK' })
        editor.editAVire = [editor.liste1, editor.liste2]
      }
      if (amodif.type === 'cercle') {
        editor.tabDepAct = addDefaultTable(editor.lesdiv.actDep, 8, 2)
        j3pAffiche(editor.tabDepAct[0][0], null, 'Cercle ' + editor.progConst.recupe(amodif.nom) + '&nbsp;')
        j3pAjouteBouton(editor.tabDepAct[0][1], () => { ChgNomCercle(amodif) }, { value: 'Modifier' })
        let aaj2 = 'Par centre et point&nbsp;'
        if (amodif.def !== 'passe') {
          aaj2 = 'Par centre et rayon&nbsp;'
        }
        j3pAffiche(editor.tabDepAct[1][0], null, aaj2)
        j3pAjouteBouton(editor.tabDepAct[1][1], () => { ChgTypeCercle(amodif) }, { value: 'Modifier' })
      }
      if (amodif.type === 'demi-droite') {
        editor.tabDepAct = addDefaultTable(editor.lesdiv.actDep, 8, 2)
        chEffectue('Demi-droite ')
        const ll = ['...']
        for (let i = 0; i < num; i++) {
          if (editor.progfig[i].type === 'point') ll.push(editor.progConst.recupe(editor.progConst.progfig[i].nom))
        }
        const ttab = addDefaultTable(editor.tabDepAct[1][0], 1, 5)
        j3pAddTxt(ttab[0][0], '[&nbsp;')
        j3pAddTxt(ttab[0][3], '&nbsp;]')
        editor.liste1 = ListeDeroulante.create(ttab[0][1], ll, ldOptions)
        editor.liste2 = ListeDeroulante.create(ttab[0][1], ll, ldOptions)
        j3pAjouteBouton(ttab[0][4], () => { choixDemiDroiteMod(amodif) }, { value: 'OK' })
        editor.editAVire = [editor.liste1, editor.liste2]
      }
    }
    function ChgNomCercle (amodif) {
      const tatab = chEffectue('Cercle ')
      editor.zoneNom = new ZoneStyleMathquill1(tatab[0][1], { restric: "ABCDEFGHIJKLMNOPQRSTUVWXYZabcdefghijklmnopqrstuvwxyz0123456789'()", lim: 4, limite: 4, enter: () => { okNomDebCercle(amodif) } })
      j3pAjouteBouton(tatab[0][2], () => { okNomDebCercle(amodif) }, { value: 'OK' })
      editor.cocheNomCertM = j3pAjouteCaseCoche(tatab[1][2], { label: '' })
      editor.cocheNomCertM.checked = amodif.aff !== '' && amodif.aff !== undefined
      j3pAddContent(tatab[1][0], 'Nom visible:&nbsp;')
      setTimeout(() => { editor.zoneNom.focus() }, 10)
      editor.editAVire = [editor.zoneNom]
      editor.prvien = editor.tabDepAct[1][0]
      j3pEmpty(editor.prvien)
      editor.prvien.style.color = '#f00'
    }
    function okNomDebCercle (amodif) {
      let nNom = ''
      if (editor.zoneNom.reponse() === '') {
        prvieN('Il faut nommer ce cercle')
        return
      }
      for (let i = 0; i < editor.progfig.length; i++) {
        if (editor.progfig[i].nom === editor.zoneNom.reponse() && amodif !== editor.progfig[i]) {
          prvieN('Ce nom est déjà pris')
          return
        }
      }
      nNom = editor.zoneNom.reponse()
      const yaAf = amodif.aff !== '' && amodif.aff
      amodif.aff = (editor.cocheNomCertM.checked) ? nNom : ''
      const arev = retrouveListeDep([retrouveDep({ nom: amodif.nom }, 'num')])
      const oldnom = amodif.nom
      amodif.nom = nNom
      retrouveListeProgMod(amodif.nom, oldnom)
      editor.zoneNom.disable()
      editor.editAVire = []
      afficheMenu()
      for (let i = 0; i < arev.length; i++) {
        if (editor.progfig[arev[i]].sur) if (editor.progfig[arev[i]].sur === oldnom) editor.progfig[arev[i]].sur = amodif.nom
        if (editor.progfig[arev[i]].sur1) if (editor.progfig[arev[i]].sur1 === oldnom) editor.progfig[arev[i]].sur1 = amodif.nom
        if (editor.progfig[arev[i]].sur2) if (editor.progfig[arev[i]].sur2 === oldnom) editor.progfig[arev[i]].sur2 = amodif.nom
        if (editor.progfig[arev[i]].odt) if (editor.progfig[arev[i]].odt === oldnom) editor.progfig[arev[i]].odt = amodif.nom
        for (let j = 0; j < editor.progfig[arev[i]].dep.length; j++) {
          if (editor.progfig[arev[i]].dep[j] === oldnom) editor.progfig[arev[i]].dep[j] = amodif.nom
        }
      }
      if (!yaAf && amodif.aff !== '' && amodif.aff) {
        vireBouts()
        j3pAffiche(editor.tabDepAct[1][0], null, 'Clique sur la figure pour placer le nom du cercle')
        editor.mtgAppLecteur.getDoc(svgId).defaultCursor = 'crosshair'
        const newtc = editor.progConst.newTag()
        const a = editor.progConst.retrouvePoint(editor.progConst.recupe(editor.newDroite.dep[0]))
        const b = editor.progConst.retrouvePoint(editor.progConst.recupe(editor.newDroite.dep[1]))
        editor.progConst.mtgAppLecteur.addCircleOA({
          tag: newtc,
          o: a,
          a: b
        })
        editor.progConst.funcDown = (ev) => {
          const where = editor.progConst.leSvg.getBoundingClientRect()
          const x = ev.clientX - where.x
          const y = ev.clientY - where.y
          const cco = ccoBien(x, y)
          amodif.plx = Number(cco.x)
          amodif.affnom = true
          amodif.ply = Number(cco.y)
          amodif.placeGarde = givePLaceNomCercle(newtc, x, y, a)
          editor.progConst.funcDown = () => {}
          j3pEmpty(editor.lesdiv.actDepTitre)
          j3pEmpty(editor.lesdiv.actDep)
          affichelistedep()
          remetCool()
        }
      } else {
        if (amodif.aff === '' || !amodif.aff) {
          amodif.plx = undefined
          amodif.ply = undefined
          amodif.placeGarde = undefined
          amodif.affnom = false
        }
        j3pEmpty(editor.lesdiv.actDepTitre)
        j3pEmpty(editor.lesdiv.actDep)
        affichelistedep()
        remetCool()
      }
    }
    function retrouveListeProgMod (nom, oldnom, idepart) {
      const ptdep = idepart + 1 || 0
      for (let i = ptdep; i < editor.Programme.length; i++) {
        const proprog = editor.Programme[i]
        const atrouv = new RegExp(oldnom, 'g')
        faisremp('text', atrouv, nom, proprog)
        faisremp('nom', atrouv, nom, proprog)
        faisremp('zone', atrouv, nom, proprog)
        faisremp('pointExt1', atrouv, nom, proprog)
        faisremp('pointExt2', atrouv, nom, proprog)
        faisremp('pass', atrouv, nom, proprog)
        faisremp('pass2', atrouv, nom, proprog)
        faisremp('odt', atrouv, nom, proprog)
        faisremp('centre', atrouv, nom, proprog)
        faisremp('par', atrouv, nom, proprog)
        faisremp('rayon', atrouv, nom, proprog)
        faisremp('origine', atrouv, nom, proprog)
        faisremp('point', atrouv, nom, proprog)
        if (proprog.ki) {
          faisremp('nom', atrouv, nom, proprog.ki)
        }
        if (proprog.cond) {
          if (proprog.cond.length > 0) {
            faisremp('err', atrouv, nom, proprog.cond[0])
            faisremp('cop', atrouv, nom, proprog.cond[0])
            faisremp('formule', atrouv, nom, proprog.cond[0])
            faisremp('cop2', atrouv, nom, proprog.cond[0])
            if (proprog.cond[0].ki) {
              faisremp('nom', atrouv, nom, proprog.cond[0].ki)
            }
          }
          if (proprog.cond.length > 1) {
            faisremp('err', atrouv, nom, proprog.cond[1])
            faisremp('cop', atrouv, nom, proprog.cond[1])
            faisremp('formule', atrouv, nom, proprog.cond[1])
            faisremp('cop2', atrouv, nom, proprog.cond[1])
            if (proprog.cond[1].ki) {
              faisremp('nom', atrouv, nom, proprog.cond[1].ki)
            }
          }
        }
        if (proprog.type === 'multi') {
          for (let j = 0; j < proprog.cont.length; j++) {
            const proprog2 = proprog.cont[j]
            faisremp('text', atrouv, nom, proprog2)
            faisremp('nom', atrouv, nom, proprog2)
            faisremp('zone', atrouv, nom, proprog2)
            faisremp('pointExt1', atrouv, nom, proprog2)
            faisremp('pointExt2', atrouv, nom, proprog2)
            faisremp('pass', atrouv, nom, proprog2)
            faisremp('pass2', atrouv, nom, proprog2)
            faisremp('odt', atrouv, nom, proprog2)
            faisremp('centre', atrouv, nom, proprog2)
            faisremp('par', atrouv, nom, proprog2)
            faisremp('rayon', atrouv, nom, proprog2)
            faisremp('origine', atrouv, nom, proprog2)
            faisremp('point', atrouv, nom, proprog2)
            if (proprog2.ki) {
              faisremp('nom', atrouv, nom, proprog2.ki)
            }
            if (proprog2.cond) {
              if (proprog2.cond.length > 0) {
                faisremp('err', atrouv, nom, proprog2.cond[0])
                faisremp('cop', atrouv, nom, proprog2.cond[0])
                faisremp('formule', atrouv, nom, proprog2.cond[0])
                faisremp('cop2', atrouv, nom, proprog2.cond[0])
                if (proprog2.cond[0].ki) {
                  faisremp('nom', atrouv, nom, proprog2.cond[0].ki)
                }
              }
              if (proprog2.cond.length > 1) {
                faisremp('err', atrouv, nom, proprog2.cond[1])
                faisremp('cop', atrouv, nom, proprog2.cond[1])
                faisremp('formule', atrouv, nom, proprog2.cond[1])
                faisremp('cop2', atrouv, nom, proprog2.cond[1])
                if (proprog2.cond[1].ki) {
                  faisremp('nom', atrouv, nom, proprog2.cond[1].ki)
                }
              }
            }
          }
        }
        afficheMenu()
        /*
        { text: //tout
          nom:  //point
  zone: //que zone
  pointExt1: //point
  pointExt2: //point
  pass: //point
  pass2: //point
  odt: //droite
  centre: //point
  par: //point
  rayon: //point
  origine: //point
  point: //point
  angle: nombre
  ki {type nom //tout }
  cond: [] { err: //tout
             cop: //point
             formule: //point
             cop2: //point
             ki {type nom //tout}
           }
         */
      }
    }
    function faisremp (s, atrouv, nom, prog) {
      if (prog[s]) prog[s] = prog[s].replace(atrouv, nom)
    }
    function ChgTypeCercle (amodif) {
      const op = chEffectue('Cercle ')
      j3pEmpty(op[0][0])
      j3pEmpty(editor.ouExplik)
      const li = retrouveDep(amodif, 'num')
      const nbpoint = nbpoints1(li)
      j3pEmpty(editor.tabDepAct[1][0])
      if (nbpoint < 2) {
        choixCerclerayonMod(amodif)
        return
      }
      j3pAjouteBouton(editor.tabDepAct[1][0], () => { choixCerclepassant1Mod(amodif) }, { value: 'Défini par 2 points' })
      j3pAjouteBouton(editor.tabDepAct[2][0], () => { choixCerclerayonMod(amodif) }, { value: 'Défini par centre et rayon' })
    }
    function choixCerclerayonMod (amodif) {
      j3pEmpty(editor.tabDepAct[1][0])
      j3pEmpty(editor.tabDepAct[2][0])
      const ttab = addDefaultTable(editor.tabDepAct[2][0], 1, 2)
      j3pAddTxt(ttab[0][0], 'centre:&nbsp;')
      const li = retrouveDep(amodif, 'num')
      const ll = ['...']
      for (let i = 0; i < li; i++) {
        if (editor.progfig[i].type === 'point') ll.push(editor.progConst.recupe(editor.progConst.progfig[i].nom))
      }
      editor.liste1 = ListeDeroulante.create(ttab[0][1], ll, ldOptions)

      const tabTypeLongX = addDefaultTable(editor.tabDepAct[3][0], 2, 1)

      const tabformule = addDefaultTable(tabTypeLongX[1][0], 2, 3)
      j3pAddContent(tabformule[0][0], 'Rayon:&nbsp;')
      tabformule[0][2].style.color = '#085d03'
      j3pAddContent(tabformule[0][2], '&nbsp;<i>( Utilisez <b>à</b> pour une valeur aléatoire comprise entre 0 et 1. )</i>')
      editor.inputForm = new ZoneStyleMathquill2(tabformule[0][1], {
        restric: '0123456789,/+-*()',
        contenu: editor.newDroite.long
      })
      editor.inputForm.onchange = () => {
        editor.newDroite.long = derecupe(editor.inputForm.reponse())
      }
      const lesPoint = recupePointsDeb(li)
      for (let i = 0; i < lesPoint.length; i++) {
        for (let j = i + 1; j < lesPoint.length; j++) {
          j3pAjouteBouton(tabformule[1][1], () => {
            editor.inputForm.majaffiche(lesPoint[i] + lesPoint[j])
            setTimeout(function () { editor.inputForm.focus() }, 10)
          }, { value: lesPoint[i] + lesPoint[j] })
        }
      }
      const ttab4 = addDefaultTable(editor.tabDepAct[1][0], 3, 3)

      j3pAjouteBouton(ttab4[0][2], () => { faisFinCercleRayonMod(amodif) }, { value: 'OK' })
      editor.editAVire = [editor.inputForm, editor.liste1]
    }
    function faisFinCercleRayonMod (amodif) {
      if (!editor.liste1.changed) {
        prvieN('Il faut renseigner le centre')
        return
      }
      const laform = editor.inputForm.reponse().replace(/ /g, '')
      if (laform === '') {
        prvieN('Il faut renseigner le rayon')
        return
      }
      editor.liste1.disable()
      editor.inputForm.disable()
      amodif.def = 'ray'
      amodif.centre = derecupe(editor.liste1.reponse)
      amodif.ray = derecupe(laform).replace(/,/g, '.')
      amodif.dep = [derecupe(editor.liste1.reponse)]
      j3pEmpty(editor.lesdiv.actDepTitre)
      j3pEmpty(editor.lesdiv.actDep)
      affichelistedep()
      remetCool()
    }
    function choixCerclepassant1Mod (amodif) {
      j3pEmpty(editor.tabDepAct[1][0])
      j3pEmpty(editor.tabDepAct[2][0])
      const ttab = addDefaultTable(editor.tabDepAct[2][0], 1, 2)
      const ttab2 = addDefaultTable(editor.tabDepAct[3][0], 1, 2)
      j3pAddTxt(ttab[0][0], 'centre:&nbsp;')
      j3pAddTxt(ttab2[0][0], 'passant par:&nbsp;')
      const ll = ['...']
      const li = retrouveDep(amodif, 'num')
      for (let i = 0; i < li; i++) {
        if (editor.progfig[i].type === 'point') ll.push(editor.progConst.recupe(editor.progConst.progfig[i].nom))
      }
      editor.liste1 = ListeDeroulante.create(ttab[0][1], ll, ldOptions)
      editor.liste2 = ListeDeroulante.create(ttab2[0][1], ll, ldOptions)
      const ttab4 = addDefaultTable(editor.tabDepAct[1][0], 3, 3)
      j3pAjouteBouton(ttab4[0][2], () => { faisFinCerclepassMod(amodif) }, { value: 'OK' })
      editor.editAVire = [editor.liste1, editor.liste2]
    }
    function faisFinCerclepassMod (amodif) {
      if (!editor.liste1.changed) {
        prvieN('Il faut renseigner le centre')
        return
      }
      if (!editor.liste2.changed) {
        prvieN('Il faut renseigner le point de passage')
        return
      }
      if (editor.liste1.reponse === editor.liste2.reponse) {
        prvieN('Il faut renseigner deux points distincts')
        return
      }
      editor.liste1.disable()
      editor.liste2.disable()
      amodif.def = 'passe'
      amodif.centre = derecupe(editor.liste1.reponse)
      amodif.par = derecupe(editor.liste2.reponse)
      amodif.dep = [derecupe(editor.liste1.reponse), derecupe(editor.liste2.reponse)]
      editor.editAVire = []
      j3pEmpty(editor.lesdiv.actDepTitre)
      j3pEmpty(editor.lesdiv.actDep)
      affichelistedep()
      remetCool()
    }
    function choixDemiDroiteMod (amodif) {
      if (!editor.liste1.changed) {
        prvieN('Il faut renseigner l’origine')
        return
      }
      if (!editor.liste2.changed) {
        prvieN('Il faut renseigner le point de passage')
        return
      }
      if (editor.liste1.reponse === editor.liste2.reponse) {
        prvieN('Il faut renseigner deux points distincts')
        return
      }
      editor.liste1.disable()
      editor.liste2.disable()
      j3pEmpty(editor.tabDepAct[0][0])
      j3pEmpty(editor.tabDepAct[1][0])
      j3pAddContent(editor.tabDepAct[0][0], 'Demi-droite [' + editor.liste1.reponse + editor.liste2.reponse + ')')
      j3pAddContent(editor.tabDepAct[1][0], 'Nom à afficher<br> <i>( Si cette ligne représente une droite )</i>')
      const ttab = addDefaultTable(editor.tabDepAct[2][0], 1, 3)
      j3pAddTxt(ttab[0][1], '&nbsp;')
      editor.zoneNom = new ZoneStyleMathquill1(ttab[0][0], { restric: '()0123456789ABCDEFGHIJKLMNOPQRSTUVWXYZabcdefghijklmnopqrstuvwxyz\'', lim: 3, limite: 3, enter: () => { faisFinDdteMod(amodif) } })
      j3pAjouteBouton(ttab[0][2], () => { faisFinDdteMod(amodif) }, { value: 'OK' })
      j3pAddContent(editor.tabDepAct[3][0], '<i>Laisser vide si rien à afficher</i>')
      amodif.oldNom = amodif.nom
      amodif.nom = '[' + derecupe(editor.liste1.reponse) + derecupe(editor.liste2.reponse) + ')'
      amodif.dep = [derecupe(editor.liste1.reponse), derecupe(editor.liste2.reponse)]
      amodif.p1 = derecupe(editor.liste1.reponse)
      amodif.p2 = derecupe(editor.liste2.reponse)
      editor.editAVire = [editor.zoneNom]
      afficheExplik2(editor.ouExplik, 'Au cas où les deux points ne sont pas visibles,<br>il peut être judicieux de nommer cette ligne.')
    }
    function faisFinDdteMod (amodif) {
      const yy = editor.zoneNom.reponse().replace(/ /g, '')
      amodif.aff = (yy !== '') ? yy : undefined
      editor.zoneNom.disable()
      if (amodif.aff) amodif.nom = amodif.aff
      const arev = retrouveListeDep([retrouveDep({ nom: amodif.oldNom }, 'num')])
      retrouveListeProgMod(amodif.nom, amodif.oldnom)
      afficheMenu()
      for (let i = 0; i < arev.length; i++) {
        if (editor.progfig[arev[i]].sur) if (editor.progfig[arev[i]].sur === amodif.oldnom) editor.progfig[arev[i]].sur = amodif.nom
        if (editor.progfig[arev[i]].sur1) if (editor.progfig[arev[i]].sur1 === amodif.oldnom) editor.progfig[arev[i]].sur1 = amodif.nom
        if (editor.progfig[arev[i]].sur2) if (editor.progfig[arev[i]].sur2 === amodif.oldnom) editor.progfig[arev[i]].sur2 = amodif.nom
        if (editor.progfig[arev[i]].odt) if (editor.progfig[arev[i]].odt === amodif.oldnom) editor.progfig[arev[i]].odt = amodif.nom
        for (let j = 0; j < editor.progfig[arev[i]].dep.length; j++) {
          if (editor.progfig[arev[i]].dep[j] === amodif.oldnom) editor.progfig[arev[i]].dep[j] = amodif.nom
        }
      }
      if (!amodif.aff) {
        amodif.plx = undefined
        amodif.ply = undefined
        amodif.placeGarde = undefined
        j3pEmpty(editor.lesdiv.actDepTitre)
        j3pEmpty(editor.lesdiv.actDep)
        affichelistedep()
        remetCool()
      } else {
        const newt = editor.progConst.newTag()
        const a = editor.progConst.retrouvePoint(editor.progConst.recupe(amodif.dep[0]))
        const b = editor.progConst.retrouvePoint(editor.progConst.recupe(amodif.dep[1]))
        editor.progConst.mtgAppLecteur.addLineAB({
          tag: newt,
          a,
          b
        })
        vireBouts()
        j3pAffiche(editor.tabDepAct[1][0], null, 'Clique sur la figure pour placer le nom de la droite')
        editor.mtgAppLecteur.getDoc(svgId).defaultCursor = 'crosshair'
        editor.progConst.funcDown = (ev) => {
          const where = editor.progConst.leSvg.getBoundingClientRect()
          const x = ev.clientX - where.x
          const y = ev.clientY - where.y
          const cco = ccoBien(x, y)
          amodif.plx = Number(cco.x)
          amodif.ply = Number(cco.y)
          amodif.placeGarde = givePLaceNom(newt, x, y, a, b)
          editor.progConst.funcDown = () => {}
          j3pEmpty(editor.lesdiv.actDepTitre)
          j3pEmpty(editor.lesdiv.actDep)
          affichelistedep()
          remetCool()
        }
      }
      editor.editAVire = []
    }
    function choixSegmentMod (amodif) {
      if (!editor.liste1.changed) {
        prvieN('Il faut renseigner les deux extrémités')
        return
      }
      if (!editor.liste2.changed) {
        prvieN('Il faut renseigner les deux extrémités')
        return
      }
      if (editor.liste1.reponse === editor.liste2.reponse) {
        prvieN('Il faut deux points distincts')
        return
      }
      editor.liste1.disable()
      editor.liste2.disable()
      editor.editAVire = []
      j3pEmpty(editor.tabDepAct[0][0])
      j3pEmpty(editor.tabDepAct[1][0])
      j3pAddContent(editor.tabDepAct[0][0], 'Segment [' + editor.liste1.reponse + editor.liste2.reponse + ']')
      j3pAddContent(editor.tabDepAct[1][0], 'Nom à afficher<br> <i>( Si ce segment représente une droite )</i>')
      const ttab = addDefaultTable(editor.tabDepAct[2][0], 1, 3)
      j3pAddTxt(ttab[0][1], '&nbsp;')
      editor.zoneNom = new ZoneStyleMathquill1(ttab[0][0], { restric: '()0123456789ABCDEFGHIJKLMNOPQRSTUVWXYZabcdefghijklmnopqrstuvwxyz\'', lim: 3, limite: 3, enter: () => { faisFinSegMod(amodif) } })
      j3pAjouteBouton(ttab[0][2], () => { faisFinSegMod(amodif) }, { value: 'OK' })
      j3pAddContent(editor.tabDepAct[3][0], '<i>Laisser vide si rien à afficher</i>')
      amodif.oldNom = amodif.nom
      amodif.nom = '[' + derecupe(editor.liste1.reponse) + derecupe(editor.liste2.reponse) + ']'
      amodif.dep = [derecupe(editor.liste1.reponse), derecupe(editor.liste2.reponse)]
      editor.editAVire = [editor.zoneNom]
      afficheExplik2(editor.ouExplik, 'Au cas où les deux points ne sont pas visibles,<br>il peut être judicieux de nommer cette ligne.')
    }
    function faisFinSegMod (amodif) {
      const yy = editor.zoneNom.reponse().replace(/ /g, '')
      amodif.aff = (yy !== '') ? yy : undefined
      editor.zoneNom.disable()
      if (amodif.aff) amodif.nom = amodif.aff
      const arev = retrouveListeDep([retrouveDep({ nom: amodif.oldNom }, 'num')])
      retrouveListeProgMod(amodif.nom, amodif.oldnom)
      afficheMenu()
      for (let i = 0; i < arev.length; i++) {
        if (editor.progfig[arev[i]].sur) if (editor.progfig[arev[i]].sur === amodif.oldnom) editor.progfig[arev[i]].sur = amodif.nom
        if (editor.progfig[arev[i]].sur1) if (editor.progfig[arev[i]].sur1 === amodif.oldnom) editor.progfig[arev[i]].sur1 = amodif.nom
        if (editor.progfig[arev[i]].sur2) if (editor.progfig[arev[i]].sur2 === amodif.oldnom) editor.progfig[arev[i]].sur2 = amodif.nom
        if (editor.progfig[arev[i]].odt) if (editor.progfig[arev[i]].odt === amodif.oldnom) editor.progfig[arev[i]].odt = amodif.nom
        for (let j = 0; j < editor.progfig[arev[i]].dep.length; j++) {
          if (editor.progfig[arev[i]].dep[j] === amodif.oldnom) editor.progfig[arev[i]].dep[j] = amodif.nom
        }
      }
      if (!amodif.aff) {
        amodif.plx = undefined
        amodif.ply = undefined
        amodif.placeGarde = undefined
        j3pEmpty(editor.lesdiv.actDepTitre)
        j3pEmpty(editor.lesdiv.actDep)
        affichelistedep()
        remetCool()
      } else {
        const newt = editor.progConst.newTag()
        const a = editor.progConst.retrouvePoint(editor.progConst.recupe(amodif.dep[0]))
        const b = editor.progConst.retrouvePoint(editor.progConst.recupe(amodif.dep[1]))
        editor.progConst.mtgAppLecteur.addLineAB({
          tag: newt,
          a,
          b
        })
        vireBouts()
        j3pAffiche(editor.tabDepAct[1][0], null, 'Clique sur la figure pour placer le nom de la droite')
        editor.mtgAppLecteur.getDoc(svgId).defaultCursor = 'crosshair'
        editor.progConst.funcDown = (ev) => {
          const where = editor.progConst.leSvg.getBoundingClientRect()
          const x = ev.clientX - where.x
          const y = ev.clientY - where.y
          const cco = ccoBien(x, y)
          amodif.plx = Number(cco.x)
          amodif.ply = Number(cco.y)
          amodif.placeGarde = givePLaceNom(newt, x, y, a, b)
          editor.progConst.funcDown = () => {}
          j3pEmpty(editor.lesdiv.actDepTitre)
          j3pEmpty(editor.lesdiv.actDep)
          affichelistedep()
          remetCool()
        }
      }
    }
    function ChgNomDte (amodif) {
      const tatab = chEffectue('Droite ')
      editor.zoneNom = new ZoneStyleMathquill1(tatab[0][1], { restric: "ABCDEFGHIJKLMNOPQRSTUVWXYZabcdefghijklmnopqrstuvwxyz0123456789'()", lim: 4, limite: 4, enter: () => { okNomDebDte(amodif) } })
      j3pAjouteBouton(tatab[0][2], () => { okNomDebDte(amodif) }, { value: 'OK' })
      setTimeout(() => { editor.zoneNom.focus() }, 10)
      editor.editAVire = [editor.zoneNom]
      editor.prvien = j3pAddElt(tatab[1][1], 'div')
      editor.prvien.style.color = '#f00'
    }
    function okNomDebDte (amodif) {
      let nNom = ''
      if (editor.zoneNom.reponse() === '') {
        if (amodif.spe) {
          prvieN('Il faut nommer cette droite')
          return
        } else {
          nNom = '(' + amodif.dep[0] + amodif.dep[1] + ')'
          amodif.aff = undefined
        }
      }
      if (editor.zoneNom.reponse() !== '') {
        nNom = editor.zoneNom.reponse()
        for (let i = 0; i < editor.progfig.length; i++) {
          if (editor.progfig[i].nom === editor.zoneNom.reponse()) {
            prvieN('Ce nom est déjà pris')
            return
          }
        }
      } else {
        amodif.aff = undefined
      }
      const arev = retrouveListeDep([retrouveDep({ nom: amodif.nom }, 'num')])
      const oldnom = amodif.nom
      amodif.nom = nNom
      if (amodif.spe) amodif.aff = nNom
      editor.zoneNom.disable()
      editor.editAVire = []
      for (let i = 0; i < arev.length; i++) {
        if (editor.progfig[arev[i]].sur) if (editor.progfig[arev[i]].sur === oldnom) editor.progfig[arev[i]].sur = amodif.nom
        if (editor.progfig[arev[i]].sur1) if (editor.progfig[arev[i]].sur1 === oldnom) editor.progfig[arev[i]].sur1 = amodif.nom
        if (editor.progfig[arev[i]].sur2) if (editor.progfig[arev[i]].sur2 === oldnom) editor.progfig[arev[i]].sur2 = amodif.nom
        if (editor.progfig[arev[i]].odt) if (editor.progfig[arev[i]].odt === oldnom) editor.progfig[arev[i]].odt = amodif.nom
        for (let j = 0; j < editor.progfig[arev[i]].dep.length; j++) {
          if (editor.progfig[arev[i]].dep[j] === oldnom) editor.progfig[arev[i]].dep[j] = amodif.nom
        }
      }
      retrouveListeProgMod(amodif.nom, oldnom)
      afficheMenu()
      j3pEmpty(editor.lesdiv.actDepTitre)
      j3pEmpty(editor.lesdiv.actDep)
      affichelistedep()
      remetCool()
    }
    function ChgTypeDte (amodif) {
      const op = chEffectue('Droite ')
      j3pEmpty(op[0][0])
      j3pEmpty(editor.ouExplik)
      const li = retrouveDep(amodif, 'num')
      const nbpoint = nbpoints1(li)
      const nbligne = nblignes(li)
      if (nbligne === 0) {
        choixDroite1Mod(amodif)
        return
      }
      if (nbpoint < 2) {
        ChoixDroiteParaperpMod(amodif)
        return
      }
      j3pEmpty(editor.tabDepAct[1][0])
      j3pAjouteBouton(editor.tabDepAct[1][0], () => { choixDroite1Mod(amodif) }, { value: 'Définie par 2 points' })
      j3pAjouteBouton(editor.tabDepAct[2][0], () => { ChoixDroiteParaperpMod(amodif) }, { value: 'Parallèle/Perpend.' })
    }
    function choixDroite1Mod (amodif) {
      j3pEmpty(editor.tabDepAct[2][0])
      j3pEmpty(editor.tabDepAct[1][0])
      const ll = ['...']
      const li = retrouveDep(amodif, 'num')
      for (let i = 0; i < li; i++) {
        if (editor.progfig[i].type === 'point') ll.push(editor.progConst.recupe(editor.progConst.progfig[i].nom))
      }
      const ttab = addDefaultTable(editor.tabDepAct[1][0], 1, 5)
      j3pAddTxt(ttab[0][0], '(&nbsp;')
      j3pAddTxt(ttab[0][3], '&nbsp;)')
      editor.liste1 = ListeDeroulante.create(ttab[0][1], ll, ldOptions)
      editor.liste2 = ListeDeroulante.create(ttab[0][1], ll, ldOptions)
      j3pAjouteBouton(ttab[0][4], () => { choixDroiteMod(amodif) }, { value: 'OK' })
      editor.editAVire = [editor.liste1, editor.liste2]
    }
    function choixDroiteMod (amodif) {
      editor.liste1.disable()
      editor.liste2.disable()
      editor.editAVire = []
      const arev = retrouveListeDep([retrouveDep({ nom: amodif.nom }, 'num')])
      const oldnom = amodif.nom
      amodif.nom = (amodif.aff) ? amodif.aff : '(' + derecupe(editor.liste1.reponse) + derecupe(editor.liste2.reponse) + ')'
      for (let i = 0; i < arev.length; i++) {
        if (editor.progfig[arev[i]].sur) if (editor.progfig[arev[i]].sur === oldnom) editor.progfig[arev[i]].sur = amodif.nom
        if (editor.progfig[arev[i]].sur1) if (editor.progfig[arev[i]].sur1 === oldnom) editor.progfig[arev[i]].sur1 = amodif.nom
        if (editor.progfig[arev[i]].sur2) if (editor.progfig[arev[i]].sur2 === oldnom) editor.progfig[arev[i]].sur2 = amodif.nom
        if (editor.progfig[arev[i]].odt) if (editor.progfig[arev[i]].odt === oldnom) editor.progfig[arev[i]].odt = amodif.nom
        for (let j = 0; j < editor.progfig[arev[i]].dep.length; j++) {
          if (editor.progfig[arev[i]].dep[j] === oldnom) editor.progfig[arev[i]].dep[j] = amodif.nom
        }
      }
      amodif.dep = [derecupe(editor.liste1.reponse), derecupe(editor.liste2.reponse)]
      amodif.odt = undefined
      amodif.spe = undefined
      amodif.pass = undefined
      amodif.cox = undefined
      amodif.coy = undefined
      editor.editAVire = []
      retrouveListeProgMod(amodif.nom, oldnom)
      afficheMenu()
      j3pEmpty(editor.lesdiv.actDepTitre)
      j3pEmpty(editor.lesdiv.actDep)
      affichelistedep()
      remetCool()
    }
    function ChoixDroiteParaperpMod (amodif) {
      j3pEmpty(editor.tabDepAct[1][0])
      j3pEmpty(editor.tabDepAct[2][0])
      const ll = ['Choisir', 'parallèle', 'perpendiculaire']
      const ttab = addDefaultTable(editor.tabDepAct[2][0], 3, 5)
      j3pAddTxt(ttab[0][1], 'à&nbsp;')
      const li = retrouveDep(amodif, 'num')
      const yy = recupeLigne(li)
      yy.splice(0, 0, 'Choisir')
      editor.liste1 = ListeDeroulante.create(ttab[0][0], ll, ldOptions)
      editor.liste2 = ListeDeroulante.create(ttab[0][1], yy, ldOptions)
      j3pAddTxt(ttab[1][0], 'passant par&nbsp;')
      const pp = recupePointsDeb(li)
      pp.splice(0, 0, '...')
      editor.liste3 = ListeDeroulante.create(ttab[1][1], pp, ldOptions)
      j3pAjouteBouton(ttab[2][0], () => { choixDroitepMod(amodif) }, { value: 'OK' })
      editor.editAVire = [editor.liste1, editor.liste2, editor.liste3]
    }
    function choixDroitepMod (amodif) {
      if (!editor.liste1.changed) {
        prvieN('Il faut choisir "parallèle" ou "perpendiculaire"')
        return
      }
      if (!editor.liste2.changed) {
        prvieN('Il faut donner une ligne de référence')
        return
      }
      if (!editor.liste3.changed) {
        prvieN('Il faut donner le point de passage')
        return
      }
      editor.liste1.disable()
      editor.liste2.disable()
      editor.liste3.disable()
      amodif.pass = derecupe(editor.liste3.reponse)
      amodif.odt = derecupe(editor.liste2.reponse)
      amodif.dep = [derecupe(editor.liste3.reponse), derecupe(editor.liste2.reponse)]
      amodif.spe = (editor.liste1.reponse === 'parallèle') ? 'parallèle' : 'perp'
      amodif.cox = 10001
      amodif.coy = 10001
      if (!amodif.aff || amodif.aff === '') {
        j3pEmpty(editor.tabDepAct[2][0])
        ChgNomDte(amodif)
      } else {
        j3pEmpty(editor.lesdiv.actDepTitre)
        j3pEmpty(editor.lesdiv.actDep)
        affichelistedep()
        remetCool()
      }
    }
    function ChgNomPoint (amodif) {
      const tatab = chEffectue('Point ')
      editor.zoneNom = new ZoneStyleMathquill1(tatab[0][1], { restric: "ABCDEFGHIJKLMNOPQRSTUVWXYZabcdefghijklmnopqrstuvwxyz0123456789'", lim: 0, limite: 0, enter: () => { okNomDeb2(amodif) } })
      j3pAjouteBouton(tatab[1][1], () => { nomAleaPoint2(amodif) }, { value: 'nom aléatoire' })
      j3pAddContent(tatab[0][2], '&nbsp;')
      j3pAjouteBouton(tatab[0][2], () => { okNomDeb2(amodif) }, { value: 'OK' })
      setTimeout(() => { editor.zoneNom.focus() }, 10)
      editor.editAVire = [editor.zoneNom]
      editor.prvien = j3pAddElt(tatab[1][1], 'div')
      editor.prvien.style.color = '#f00'
    }
    function okNomDeb2 (amod) {
      if (editor.zoneNom.reponse() === '') {
        prvieN('Il faut nommer ce point')
        return
      }
      for (let i = 0; i < editor.progfig.length; i++) {
        if (editor.progfig[i].nom === editor.zoneNom.reponse()) {
          prvieN('Ce nom est déjà pris')
          return
        }
      }
      const oldnom = amod.nom
      const arev = retrouveListeDep([retrouveDep({ nom: amod.nom }, 'num')])
      amod.nom = (editor.zoneNom.reponse().indexOf('*') !== -1) ? trouveNewHaz() : editor.zoneNom.reponse()
      editor.zoneNom.disable()
      editor.editAVire = []
      for (let i = 0; i < arev.length; i++) {
        editor.progfig[arev[i]].nom = editor.progfig[arev[i]].nom.replace(oldnom, amod.nom)
        if (editor.progfig[arev[i]].sur) editor.progfig[arev[i]].sur = editor.progfig[arev[i]].sur.replace(oldnom, amod.nom)
        if (editor.progfig[arev[i]].centreRot) editor.progfig[arev[i]].centreRot = editor.progfig[arev[i]].centreRot.replace(oldnom, amod.nom)
        if (editor.progfig[arev[i]].pass) editor.progfig[arev[i]].pass = editor.progfig[arev[i]].pass.replace(oldnom, amod.nom)
        if (editor.progfig[arev[i]].image) editor.progfig[arev[i]].image = editor.progfig[arev[i]].image.replace(oldnom, amod.nom)
        if (editor.progfig[arev[i]].p1) editor.progfig[arev[i]].p1 = editor.progfig[arev[i]].p1.replace(oldnom, amod.nom)
        if (editor.progfig[arev[i]].p2) editor.progfig[arev[i]].p2 = editor.progfig[arev[i]].p2.replace(oldnom, amod.nom)
        if (editor.progfig[arev[i]].ext1) editor.progfig[arev[i]].ext1 = editor.progfig[arev[i]].ext1.replace(oldnom, amod.nom)
        if (editor.progfig[arev[i]].ext2) editor.progfig[arev[i]].ext2 = editor.progfig[arev[i]].ext2.replace(oldnom, amod.nom)
        if (editor.progfig[arev[i]].centre) editor.progfig[arev[i]].centre = editor.progfig[arev[i]].centre.replace(oldnom, amod.nom)
        if (editor.progfig[arev[i]].par) editor.progfig[arev[i]].par = editor.progfig[arev[i]].par.replace(oldnom, amod.nom)
        if (!editor.progfig[arev[i]].dep) editor.progfig[arev[i]].dep = []
        for (let j = 0; j < editor.progfig[arev[i]].dep.length; j++) {
          editor.progfig[arev[i]].dep[j] = editor.progfig[arev[i]].dep[j].replace(oldnom, amod.nom)
        }
      }
      retrouveListeProgMod(amod.nom, oldnom)
      afficheMenu()
      j3pEmpty(editor.lesdiv.actDepTitre)
      j3pEmpty(editor.lesdiv.actDep)
      affichelistedep()
      remetCool()
    }
    function nomAleaPoint2 (amod) {
      const oldnom = amod.nom
      const arev = retrouveListeDep([retrouveDep({ nom: amod.nom }, 'num')])
      amod.nom = ''
      amod.nom = trouveNewHaz()
      editor.zoneNom.disable()
      editor.editAVire = []
      for (let i = 0; i < arev.length; i++) {
        editor.progfig[arev[i]].nom = editor.progfig[arev[i]].nom.replace(oldnom, amod.nom)
        if (editor.progfig[arev[i]].sur) editor.progfig[arev[i]].sur = editor.progfig[arev[i]].sur.replace(oldnom, amod.nom)
        if (editor.progfig[arev[i]].ext1) editor.progfig[arev[i]].ext1 = editor.progfig[arev[i]].ext1.replace(oldnom, amod.nom)
        if (editor.progfig[arev[i]].ext2) editor.progfig[arev[i]].ext2 = editor.progfig[arev[i]].ext2.replace(oldnom, amod.nom)
        if (!editor.progfig[arev[i]].dep) editor.progfig[arev[i]].dep = []
        for (let j = 0; j < editor.progfig[arev[i]].dep.length; j++) {
          editor.progfig[arev[i]].dep[j] = editor.progfig[arev[i]].dep[j].replace(oldnom, amod.nom)
        }
      }
      retrouveListeProgMod(amod.nom, oldnom)
      afficheMenu()
      j3pEmpty(editor.lesdiv.actDepTitre)
      j3pEmpty(editor.lesdiv.actDep)
      affichelistedep()
      remetCool()
    }
    function ChgTypePoint (amod) {
      j3pEmpty(editor.tabDepAct[0][1])
      j3pEmpty(editor.tabDepAct[1][1])
      j3pEmpty(editor.tabDepAct[1][0])
      j3pEmpty(editor.tabDepAct[2][0])
      const lenum = retrouveDep({ nom: amod.nom }, 'num')
      const nbpoint = nbpoints1(lenum)
      const nbobj1 = nbobj(lenum)
      const nbli = nblignes(lenum)
      j3pAjouteBouton(editor.tabDepAct[1][0], () => { pointFix2(amod) }, { value: 'Point Fixe' })
      if (nbobj1 > 0) j3pAjouteBouton(editor.tabDepAct[2][0], () => { pointSur2(amod) }, { value: 'Sur objet' })
      if (nbpoint > 1) j3pAjouteBouton(editor.tabDepAct[3][0], () => { pointMilieu2(amod) }, { value: 'Milieu' })
      if (nbpoint > 1) j3pAjouteBouton(editor.tabDepAct[4][0], () => { pointRot2(amod) }, { value: 'Rotation' })
      if (nbpoint > 1) j3pAjouteBouton(editor.tabDepAct[5][0], () => { pointSym2(amod) }, { value: 'Symétrie' })
      if (nbli > 1) j3pAjouteBouton(editor.tabDepAct[6][0], () => { pointInter2(amod) }, { value: 'Intersection' })
    }
    function pointFix2 (amod) {
      vireBouts()
      j3pAffiche(editor.tabDepAct[1][0], null, 'Clique sur la figure pour placer le point')
      editor.mtgAppLecteur.getDoc(svgId).defaultCursor = 'crosshair'
      editor.progConst.funcDown = (ev) => {
        const where = editor.progConst.leSvg.getBoundingClientRect()
        const x = ev.clientX - where.x
        const y = ev.clientY - where.y
        const cco = ccoBien(x, y)
        amod.cox = cco.x
        amod.coy = cco.y
        editor.progConst.funcDown = () => {}
        amod.dep = []
        amod.sur = null
        amod.coef = null
        amod.ext1 = null
        amod.ext2 = null
        amod.haz = null
        amod.centreRot = null
        amod.sur1 = null
        amod.sur2 = null
        j3pEmpty(editor.lesdiv.actDepTitre)
        j3pEmpty(editor.lesdiv.actDep)
        affichelistedep()
        remetCool()
      }
    }
    function pointSur () {
      vireBouts()
      const ll = ['Choisir']
      for (let i = 0; i < editor.progfig.length; i++) {
        if (editor.progfig[i].type !== 'point') ll.push(editor.progConst.recupe(editor.progfig[i].nom))
      }
      const ttab = addDefaultTable(editor.tabDepAct[1][0], 1, 4)
      j3pAddTxt(ttab[0][0], 'point sur&nbsp;')
      editor.listeSur = ListeDeroulante.create(ttab[0][1], ll, ldOptions)
      j3pAddTxt(ttab[0][2], '&nbsp;')
      j3pAjouteBouton(ttab[0][3], choixSur, { value: 'OK' })
      editor.editAVire = [editor.listeSur]
    }
    function pointSur2 (amod) {
      vireBouts()
      const ll = ['Choisir']
      const lenum = retrouveDep({ nom: amod.nom }, 'num')
      for (let i = 0; i < lenum; i++) {
        if (editor.progfig[i].type !== 'point') ll.push(editor.progConst.recupe(editor.progfig[i].nom))
      }
      const ttab = addDefaultTable(editor.tabDepAct[1][0], 1, 4)
      j3pAddTxt(ttab[0][0], 'point sur&nbsp;')
      editor.listeSur = ListeDeroulante.create(ttab[0][1], ll, ldOptions)
      j3pAddTxt(ttab[0][2], '&nbsp;')
      j3pAjouteBouton(ttab[0][3], () => { choixSur2(amod) }, { value: 'OK' })
    }
    function choixSur2 (amod) {
      if (!editor.listeSur.changed) {
        prvieN('Il faut renseigner le contenant')
        return
      }
      amod.sur = derecupe(editor.listeSur.reponse)
      amod.haz = true
      amod.dep = [derecupe(editor.listeSur.reponse)]
      editor.listeSur.disable()
      amod.ext1 = null
      amod.ext2 = null
      amod.cox = null
      amod.coy = null
      amod.fix = false
      amod.centreRot = null
      amod.sur1 = null
      amod.sur2 = null
      j3pEmpty(editor.lesdiv.actDepTitre)
      j3pEmpty(editor.lesdiv.actDep)
      affichelistedep()
      remetCool()
    }
    function choixSur () {
      if (!editor.listeSur.changed) {
        prvieN('Il faut renseigner le contenant')
        return
      }
      editor.newPoint.sur = derecupe(editor.listeSur.reponse)
      editor.newPoint.haz = true
      editor.newPoint.dep = [derecupe(editor.listeSur.reponse)]
      editor.listeSur.disable()
      editor.progfig.push(editor.newPoint)
      j3pEmpty(editor.lesdiv.actDepTitre)
      j3pEmpty(editor.lesdiv.actDep)
      affichelistedep()
      remetCool()
      editor.editAVire = []
    }
    function pointMilieu2 (amod) {
      vireBouts()
      const ll = ['...']
      const lenum = retrouveDep({ nom: amod.nom }, 'num')
      for (let i = 0; i < lenum; i++) {
        if (editor.progfig[i].type === 'point' && editor.progfig[i].nom !== amod.nom) ll.push(editor.progConst.recupe(editor.progfig[i].nom))
      }
      const ttab = addDefaultTable(editor.tabDepAct[1][0], 1, 5)
      j3pAddTxt(ttab[0][0], 'point milieu de [&nbsp;')
      j3pAddTxt(ttab[0][3], '&nbsp;]&nbsp;')
      editor.liste1 = ListeDeroulante.create(ttab[0][1], ll, ldOptions)
      editor.liste2 = ListeDeroulante.create(ttab[0][1], ll, ldOptions)
      j3pAjouteBouton(ttab[0][4], () => { choixMilieu2(amod) }, { value: 'OK' })
    }
    function choixMilieu2 (amod) {
      if (!editor.liste1.changed) {
        prvieN('Il faut donner deux points')
        return
      }
      if (!editor.liste2.changed) {
        prvieN('Il faut donner deux points')
        return
      }
      if (editor.liste1.reponse === editor.liste2.reponse) {
        prvieN('Il faut donner deux points distincts')
        return
      }
      amod.ext1 = derecupe(editor.liste1.reponse)
      amod.ext2 = derecupe(editor.liste2.reponse)
      amod.dep = [derecupe(editor.liste1.reponse), derecupe(editor.liste1.reponse)]
      editor.liste1.disable()
      editor.liste2.disable()
      amod.cox = null
      amod.coy = null
      amod.sur = null
      amod.coef = null
      amod.haz = null
      amod.sur1 = null
      amod.sur2 = null
      amod.centreRot = null
      j3pEmpty(editor.lesdiv.actDepTitre)
      j3pEmpty(editor.lesdiv.actDep)
      affichelistedep()
      remetCool()
    }
    function pointRot () {
      vireBouts()
      const ll = ['...']
      for (let i = 0; i < editor.progConst.progfig.length; i++) {
        if (editor.progfig[i].type === 'point') ll.push(editor.progConst.recupe(editor.progConst.progfig[i].nom))
      }
      const ttab = addDefaultTable(editor.tabDepAct[1][0], 4, 1)
      j3pAddTxt(ttab[0][0], 'centre:&nbsp;')
      j3pAddTxt(ttab[1][0], 'image de:&nbsp;')
      editor.liste1 = ListeDeroulante.create(ttab[0][0], ll, ldOptions)
      editor.liste2 = ListeDeroulante.create(ttab[1][0], ll, ldOptions)
      const ttab3 = addDefaultTable(ttab[2][0], 1, 4)
      j3pAddTxt(ttab3[0][0], 'angle:&nbsp;')
      j3pAddTxt(ttab3[0][2], '&nbsp;°')
      editor.liste3 = new ZoneStyleMathquill1(ttab3[0][1], { restric: '0123456789+-*/à(),', limite: 30 })
      j3pAddContent(ttab3[0][3], '&nbsp;<i>( Utilisez <b>à</b> pour une valeur aléatoire comprise entre 0 et 1. )</i>')
      j3pAjouteBouton(ttab[3][0], choixRot, { value: 'OK' })
      editor.editAVire = [editor.liste1, editor.liste2, editor.liste3]
    }
    function pointRot2 (amod) {
      vireBouts()
      const ll = ['...']
      const lenum = retrouveDep({ nom: amod.nom }, 'num')
      for (let i = 0; i < lenum; i++) {
        if (editor.progfig[i].type === 'point' && editor.progfig[i].nom !== amod.nom) ll.push(editor.progConst.recupe(editor.progfig[i].nom))
      }
      const ttab = addDefaultTable(editor.tabDepAct[1][0], 4, 1)
      j3pAddTxt(ttab[0][0], 'centre:&nbsp;')
      j3pAddTxt(ttab[1][0], 'image de:&nbsp;')
      editor.liste1 = ListeDeroulante.create(ttab[0][0], ll, ldOptions)
      editor.liste2 = ListeDeroulante.create(ttab[1][0], ll, ldOptions)
      const ttab3 = addDefaultTable(ttab[2][0], 1, 3)
      j3pAddTxt(ttab3[0][0], 'angle:&nbsp;')
      j3pAddTxt(ttab3[0][2], '&nbsp;°')
      editor.liste3 = new ZoneStyleMathquill1(ttab3[0][1], { restric: '0123456789+-*/(),à', limite: 30 })
      j3pAjouteBouton(ttab[3][0], () => { choixRot2(amod) }, { value: 'OK' })
      editor.editAVire = [editor.liste1, editor.liste2, editor.liste3]
    }
    function pointSym2 (amod) {
      vireBouts()
      const ll = ['...']
      const lenum = retrouveDep({ nom: amod.nom }, 'num')
      for (let i = 0; i < lenum; i++) {
        if (editor.progfig[i].type === 'point' && editor.progfig[i].nom !== amod.nom) ll.push(editor.progConst.recupe(editor.progfig[i].nom))
      }
      const ttab = addDefaultTable(editor.tabDepAct[1][0], 4, 1)
      j3pAddTxt(ttab[0][0], 'centre:&nbsp;')
      j3pAddTxt(ttab[1][0], 'image de:&nbsp;')
      editor.liste1 = ListeDeroulante.create(ttab[0][0], ll, ldOptions)
      editor.liste2 = ListeDeroulante.create(ttab[1][0], ll, ldOptions)
      j3pAjouteBouton(ttab[3][0], () => { choixSym2(amod) }, { value: 'OK' })
      editor.editAVire = [editor.liste1, editor.liste2]
    }
    function pointInter () {
      vireBouts()
      const ll = ['Choisir']
      for (let i = 0; i < editor.progfig.length; i++) {
        if (editor.progfig[i].type !== 'point' && editor.progfig[i].type.indexOf('one') === -1) ll.push(editor.progConst.recupe(editor.progfig[i].nom))
      }
      const ttab = addDefaultTable(editor.tabDepAct[1][0], 1, 4)
      editor.listeSur = ListeDeroulante.create(ttab[0][0], ll, ldOptions)
      j3pAddTxt(ttab[0][2], '&nbsp;et&nbsp;')
      editor.listeSur2 = ListeDeroulante.create(ttab[0][0], ll, ldOptions)
      j3pAjouteBouton(ttab[0][3], choixInter, { value: 'OK' })
      editor.editAVire = [editor.listeSur, editor.listeSur2]
    }
    function choixInter () {
      if (!editor.listeSur.changed) {
        prvieN('Il faut renseigner les deux contenants')
        return
      }
      if (!editor.listeSur2.changed) {
        prvieN('Il faut renseigner les deux contenants')
        return
      }
      if (editor.listeSur.reponse === editor.listeSur2.reponse) {
        prvieN('Il faut renseigner deux contenants différents')
        return
      }
      editor.newPoint.sur1 = derecupe(editor.listeSur.reponse)
      editor.newPoint.sur2 = derecupe(editor.listeSur2.reponse)
      editor.newPoint.haz = false
      editor.newPoint.dep = [derecupe(editor.listeSur.reponse), derecupe(editor.listeSur2.reponse)]
      editor.listeSur.disable()
      editor.listeSur2.disable()
      editor.progfig.push(editor.newPoint)
      j3pEmpty(editor.lesdiv.actDepTitre)
      j3pEmpty(editor.lesdiv.actDep)
      affichelistedep()
      remetCool()
      editor.editAVire = []
    }
    function choixInter2 (amod) {
      if (!editor.listeSur.changed) {
        prvieN('Il faut renseigner deux contenants')
        return
      }
      if (!editor.listeSur2.changed) {
        prvieN('Il faut renseigner deux contenants')
        return
      }
      if (editor.listeSur2.reponse === editor.listeSur.reponse) {
        prvieN('Il faut renseigner deux contenants distincts')
        return
      }
      amod.sur1 = derecupe(editor.listeSur.reponse)
      amod.sur2 = derecupe(editor.listeSur2.reponse)
      amod.dep = [derecupe(editor.listeSur.reponse), derecupe(editor.listeSur2.reponse)]
      amod.cox = null
      amod.coy = null
      amod.sur = null
      amod.coef = null
      amod.haz = null
      amod.centreRot = null
      editor.listeSur.disable()
      editor.listeSur2.disable()
      j3pEmpty(editor.lesdiv.actDepTitre)
      j3pEmpty(editor.lesdiv.actDep)
      affichelistedep()
      remetCool()
      editor.editAVire = []
    }
    function pointInter2 (amod) {
      vireBouts()
      const ll = ['...']
      const lenum = retrouveDep({ nom: amod.nom }, 'num')
      for (let i = 0; i < lenum; i++) {
        if (editor.progfig[i].type !== 'point' && editor.progfig[i].type.indexOf('one') === -1) ll.push(editor.progConst.recupe(editor.progfig[i].nom))
      }
      const ttab = addDefaultTable(editor.tabDepAct[1][0], 1, 4)
      editor.listeSur = ListeDeroulante.create(ttab[0][0], ll, ldOptions)
      j3pAddTxt(ttab[0][2], '&nbsp;et&nbsp;')
      editor.listeSur2 = ListeDeroulante.create(ttab[0][0], ll, ldOptions)
      j3pAjouteBouton(ttab[0][3], () => { choixInter2(amod) }, { value: 'OK' })
      editor.editAVire = [editor.listeSur, editor.listeSur2]
    }
    function choixSym () {
      if (!editor.liste1.changed) {
        prvieN('Il faut donner le centre')
        return
      }
      if (!editor.liste2.changed) {
        prvieN('Il faut donner le point de départ')
        return
      }
      if (editor.liste1.reponse === editor.liste2.reponse) {
        prvieN('Il faut donner deux points distincts')
        return
      }
      editor.editAVire = []
      editor.newPoint.centreRot = derecupe(editor.liste1.reponse)
      editor.newPoint.image = derecupe(editor.liste2.reponse)
      editor.newPoint.angle = 180
      editor.newPoint.dep = [derecupe(editor.liste1.reponse), derecupe(editor.liste2.reponse)]
      editor.liste1.disable()
      editor.liste2.disable()
      editor.progfig.push(editor.newPoint)
      j3pEmpty(editor.lesdiv.actDepTitre)
      j3pEmpty(editor.lesdiv.actDep)
      affichelistedep()
      remetCool()
    }
    function choixSym2 (amod) {
      if (!editor.liste1.changed) {
        prvieN('Il faut donner le centre')
        return
      }
      if (!editor.liste2.changed) {
        prvieN('Il faut donner le point de départ')
        return
      }
      if (editor.liste1.reponse === editor.liste2.reponse) {
        prvieN('Il faut donner deux points distincts')
        return
      }
      editor.editAVire = []
      amod.centreRot = derecupe(editor.liste1.reponse)
      amod.image = derecupe(editor.liste2.reponse)
      amod.angle = 180
      amod.dep = [derecupe(editor.liste1.reponse), derecupe(editor.liste2.reponse)]
      amod.cox = null
      amod.coy = null
      amod.sur = null
      amod.coef = null
      amod.haz = null
      amod.sur1 = null
      amod.sur2 = null
      editor.liste1.disable()
      editor.liste2.disable()
      j3pEmpty(editor.lesdiv.actDepTitre)
      j3pEmpty(editor.lesdiv.actDep)
      affichelistedep()
      remetCool()
    }
    function choixRot () {
      if (!editor.liste1.changed) {
        prvieN('Il faut donner le centre')
        return
      }
      if (!editor.liste2.changed) {
        prvieN('Il faut donner le point de départ')
        return
      }
      if (editor.liste1.reponse === editor.liste2.reponse) {
        prvieN('Il faut donner deux points distincts')
        return
      }
      if (editor.liste3.reponse() === '') {
        prvieN('Il faut donner l’angle')
        return
      }
      if (Number(editor.liste3.reponse()) === 0) {
        prvieN('L’angle ne doit pas être nul')
        return
      }
      editor.editAVire = []
      editor.newPoint.centreRot = derecupe(editor.liste1.reponse)
      editor.newPoint.image = derecupe(editor.liste2.reponse)
      editor.newPoint.angle = editor.liste3.reponse()
      editor.newPoint.haz = editor.newPoint.angle.indexOf('à') !== -1
      editor.newPoint.dep = [derecupe(editor.liste1.reponse), derecupe(editor.liste2.reponse)]
      editor.liste1.disable()
      editor.liste2.disable()
      editor.liste3.disable()
      editor.progfig.push(editor.newPoint)
      j3pEmpty(editor.lesdiv.actDepTitre)
      j3pEmpty(editor.lesdiv.actDep)
      affichelistedep()
      remetCool()
    }
    function choixRot2 (amod) {
      if (!editor.liste1.changed) {
        prvieN('Il faut donner le centre')
        return
      }
      if (!editor.liste2.changed) {
        prvieN('Il faut donner le point de départ')
        return
      }
      if (editor.liste1.reponse === editor.liste2.reponse) {
        prvieN('Il faut donner deux points distincts')
        return
      }
      if (editor.liste3.reponse() === '') {
        prvieN('Il faut donner l’angle')
        return
      }
      if (Number(editor.liste3.reponse()) === 0) {
        prvieN('L’angle ne doit pas être nul')
        return
      }
      editor.editAVire = []
      amod.centreRot = derecupe(editor.liste1.reponse)
      amod.image = derecupe(editor.liste2.reponse)
      amod.angle = editor.liste3.reponse()
      amod.haz = amod.angle.indexOf('à') !== -1
      amod.dep = [derecupe(editor.liste1.reponse), derecupe(editor.liste2.reponse)]
      editor.liste1.disable()
      editor.liste2.disable()
      editor.liste3.disable()
      amod.cox = null
      amod.coy = null
      amod.sur = null
      amod.coef = null
      amod.sur1 = null
      amod.sur2 = null
      j3pEmpty(editor.lesdiv.actDepTitre)
      j3pEmpty(editor.lesdiv.actDep)
      affichelistedep()
      remetCool()
    }
    function affichelistedep () {
      editor.progConst.mtgAppLecteur = editor.mtgAppLecteur
      editor.progConst.listePourCalc = editor.listePourCalc
      editor.progConst.zonefig = editor.lesdiv.figdep
      editor.progConst.funcDown = () => {}
      editor.progConst.virecentre = () => {}
      choixExos()
      editor.progConst.blokRemove = true
      editor.progConst.yaOldSvg = editor.SvgId.dep
      editor.progConst.makefig()
      editor.SvgId.dep = editor.progConst.svgId
      svgId = editor.progConst.svgId
      j3pEmpty(editor.lesdiv.modifDep)
      editor.lesdiv.modifDep.style.verticalAlign = 'top'
      editor.tabBoutDep = []
      editor.cursoor = []
      editor.caseCoche = []
      editor.caseCoche2 = []
      const tab = addDefaultTable(editor.lesdiv.modifDep, editor.progfig.length + 2, 6)
      editor.boutAjDep = j3pAjouteBouton(tab[editor.progfig.length + 1][0], ajouteDep, { value: 'Ajouter un élèment' })
      j3pAddElt(tab[0][0], 'b', 'Elément')
      tab[0][0].style.border = '1px solid black'
      tab[0][0].style.padding = '0px 5px 5px 5px'
      tab[0][0].style.background = '#47faf3'
      editor.acache = [tab[0][3], tab[0][4]]
      for (let i = 0; i < editor.progfig.length; i++) {
        tab[i + 1][0].parentNode.style.border = '1px solid black'
        tab[i + 1][1].style.padding = '0px 5px 5px 5px'
        tab[i + 1][2].style.padding = '0px 5px 5px 5px'
        tab[i + 1][2].style.background = '#acf8f3'
        tab[i + 1][0].style.background = '#acf8f3'
        tab[i + 1][1].style.background = '#acf8f3'
        tab[i + 1][3].style.background = '#acf8f3'
        switch (editor.progfig[i].type) {
          case 'zone':
          case 'zoneseg':
            j3pAffiche(tab[i + 1][0], null, '&nbsp;' + editor.progfig[i].nom + '&nbsp;')
            break
          case 'droite':
            j3pAddTxt(tab[i + 1][0], 'Droite ' + editor.progConst.recupe(editor.progfig[i].nom))
            if (editor.progfig[i].aff !== undefined) {
              j3pAjouteBouton(tab[i + 1][4], chPlace(i), { value: 'Dépl. nom' })
            }
            break
          case 'demi-droite': {
            const tabaOub = addDefaultTable(tab[i + 1][0], 4, 1)
            j3pAddTxt(tabaOub[0][0], 'Demi-droite ' + editor.progConst.recupe(editor.progfig[i].nom))
            if (editor.progfig[i].cox < 1000) {
              const abu = j3pGetNewId('slid')
              tabaOub[1][0].id = abu + '_div'
              editor.cursoor.push(InitSlider(abu, editor.progfig[i].cox, 200, makFoncSlide4(i), editor.mtgAppLecteur))
            }
            if (editor.progfig[i].aff !== undefined) {
              j3pAjouteBouton(tab[i + 1][4], chPlace(i), { value: 'Dépl. nom' })
            }
          }
            break
          case 'segment': {
            if (editor.progfig[i].aff !== undefined) {
              j3pAjouteBouton(tab[i + 1][4], chPlace(i), { value: 'Dépl. nom' })
            }
            const aaj = '&nbsp;<b>' + editor.progConst.recupe(editor.progfig[i].nom) + '</b>'
            j3pAffiche(tab[i + 1][0], null, '&nbsp;' + editor.progfig[i].type + ' ' + aaj + '&nbsp;')
          }
            break
          case 'cercle': {
            if (editor.progfig[i].affnom) {
              j3pAjouteBouton(tab[i + 1][4], chPlaceC(i), { value: 'Dépl. nom' })
            }
            const aaj = '&nbsp;<b>' + editor.progConst.recupe(editor.progfig[i].nom) + '</b>'
            j3pAffiche(tab[i + 1][0], null, '&nbsp;' + editor.progfig[i].type + ' ' + aaj + '&nbsp;')
          }
            break
          default: {
            const aaj = '&nbsp;<b>' + editor.progConst.recupe(editor.progfig[i].nom) + '</b>'
            j3pAffiche(tab[i + 1][0], null, '&nbsp;' + editor.progfig[i].type + ' ' + aaj + '&nbsp;')
          }
        }
        editor.tabBoutDep[i] = []
        editor.tabBoutDep[i][0] = j3pAjouteBouton(tab[i + 1][1], () => { supprimeDep(i) }, { value: 'Suppr' })
        editor.tabBoutDep[i][1] = j3pAjouteBouton(tab[i + 1][2], () => { modifDep(i) }, { value: 'Modif' })
        tab[i + 1][1].style.paddingTop = '5px'
        tab[i + 1][2].style.paddingTop = '5px'
        if (editor.progfig[i].type !== 'zone' && editor.progfig[i].type !== 'zoneseg') {
          editor.caseCoche[i] = j3pAjouteCaseCoche(tab[i + 1][3], { label: '' })
          editor.caseCoche[i].addEventListener('change', makFoncViz(i))
          editor.acache.push(tab[i + 1][3])
          j3pEmpty(tab[0][3])
          j3pAddElt(tab[0][3], 'b', 'Visible')
          tab[0][3].style.background = '#47faf3'
          tab[0][3].style.border = '1px solid black'
          tab[i + 1][3].style.borderLeft = '1px solid black'
          tab[0][3].style.padding = '0px 5px 5px 5px'
          tab[i + 1][3].style.padding = '0px 5px 5px 5px'
          editor.caseCoche[i].checked = (editor.progfig[i].visible !== false)
        }
        if (editor.progfig[i].haz) {
          editor.caseCoche2[i] = j3pAjouteCaseCoche(tab[i + 1][4], { label: '' })
          editor.caseCoche2[i].checked = editor.progfig[i].fix === true
          editor.progfig[i].fix = editor.caseCoche2[i].checked === true
          j3pEmpty(tab[0][4])
          j3pAddElt(tab[0][4], 'b', 'Fixe')
          tab[0][4].style.border = '1px solid black'
          tab[0][4].style.padding = '0px 5px 5px 5px'
          tab[i + 1][4].style.border = '1px solid black'
          tab[i + 1][4].style.padding = '0px 5px 5px 5px'
          tab[i + 1][5].style.border = '1px solid black'
          editor.acache.push(tab[i + 1][4])
          editor.progfig[i].coef = editor.progfig[i].coef || j3pGetRandomInt(20, 80)
          editor.cursoor.push(InitSlider(tab[i + 1][5], editor.progfig[i].coef, 100, getSliderFunction(i), editor.mtgAppLecteur))
          editor.acache.push(tab[i + 1][5])
          if (editor.caseCoche2[i].checked) tab[i + 1][5].style.display = 'none'
          editor.caseCoche2[i].addEventListener('change', getMouseDownListener(i, tab[i + 1][5]))
        }
        const arev = retrouveListeDep([retrouveDep({ nom: editor.progfig[i].nom }, 'num')])
        editor.tabBoutDep[i][0].addEventListener('mouseover', () => {
          for (let u = 0; u < arev.length; u++) {
            tab[arev[u] + 1][0].style.background = '#ef3939'
          }
        })
        editor.tabBoutDep[i][0].addEventListener('mouseout', () => {
          for (let u = 0; u < arev.length; u++) {
            tab[arev[u] + 1][0].style.background = '#acf8f3'
          }
        })
      }
    }

    function getSliderFunction (numob) {
      return (val) => {
        const ob = editor.progfig[numob]
        ob.coef = val
        replaceTouDeb({ nom: ob.nom })
      }
    }
    function makFoncSlide4 (numob) {
      return (val) => {
        const ob = editor.progfig[numob]
        const monSeg = retrouveDemiDroite(ob)
        const pt1 = editor.mtgAppLecteur.getPointPosition(svgId, retrouvePoint(editor.progConst.recupe(ob.dep[0])))
        const pt2 = editor.mtgAppLecteur.getPointPosition(svgId, retrouvePoint(editor.progConst.recupe(ob.dep[1])))
        let a = (pt1.y - pt2.y) / (pt1.x - pt2.x)
        if (pt1.x === pt2.x) a = maxInt
        ob.coy = val
        const b = pt2.y - a * pt2.x
        let lasol2
        const sol = editor.progConst.getInstersectingPoints(a, b, pt2.x, pt2.y, ob.coy)
        let s1 = pt1.x - pt2.x
        let s2 = sol[0].x - pt2.x
        lasol2 = (s1 * s2 > 0) ? sol[1] : sol[0]
        if (pt1.x === pt2.x) {
          s1 = pt1.y - pt2.y
          s2 = sol[0].y - pt2.y
          lasol2 = (s1 * s2 > 0) ? sol[1] : sol[0]
        }
        editor.mtgAppLecteur.setPointPosition(svgId, monSeg[1], lasol2.x, lasol2.y, true)
      }
    }

    function getMouseDownListener (numob, numslide) {
      return () => {
        const ob = editor.progfig[numob]
        ob.fix = editor.caseCoche2[numob].checked
        numslide.style.display = (ob.fix) ? 'none' : ''
      }
    }
    function makFoncViz (numob) {
      return () => {
        const ob = editor.progfig[numob]
        ob.visible = editor.caseCoche[numob].checked
        const amod = []
        const amodt = []
        switch (ob.type) {
          case 'point': {
            const lp = retrouvePoint(editor.progConst.recupe(ob.nom))
            amod.push(lp.point)
            amodt.push(lp.aff)
          }
            break
          case 'segment': {
            const lp = retrouveSeg(ob)
            amod.push(lp.ligne)
            if (lp.aff) amodt.push(lp.aff)
            break
          }
          case 'droite': {
            const lp = retrouveDroite(ob)
            amod.push(lp.ligne)
            if (lp.aff && lp.viz) amodt.push(lp.aff)
            break
          }
          case 'demi-droite': {
            const lp = retrouveDemiDroite(ob)
            amod.push(lp.ligne)
            if (lp.aff) amodt.push(lp.aff)
            break
          }
          case 'cercle': {
            const lp = retrouveCercle(ob)
            for (let i = 13; i < 25; i++) amod.push(lp['arc' + (i - 12)])
            if (lp.aff) amodt.push(lp.aff)
            break
          }
        }
        if (ob.visible) {
          for (let i = 0; i < amod.length; i++) {
            editor.mtgAppLecteur.setApiDoc(svgId)
            editor.mtgAppLecteur.setVisible({ elt: amod[i] })
          }
          for (let i = 0; i < amodt.length; i++) {
            editor.mtgAppLecteur.setVisible({ elt: amodt[i] })
            editor.mtgAppLecteur.setText(svgId, amodt[i], editor.progConst.recupe(ob.nom), true)
          }
        } else {
          for (let i = 0; i < amod.length; i++) {
            editor.mtgAppLecteur.setApiDoc(svgId)
            editor.mtgAppLecteur.setHidden({ elt: amod[i] })
          }
          for (let i = 0; i < amodt.length; i++) {
            editor.mtgAppLecteur.setHidden({ elt: amodt[i] })
            editor.mtgAppLecteur.setText(svgId, amodt[i], ' ', true)
          }
        }
        editor.mtgAppLecteur.updateFigure(svgId)
      }
    }

    const editor = {
      mepact: container
    }

    let svgId
    const maxInt = 999999999999
    let ds

    function faisFin () {
      if (document.fullscreenElement) document.exitFullscreen()
      const newValues = {
        Programme: JSON.stringify({ titre: editor.titre, exos: editor.ProgrammeLT }),
        Precis_ang: editor.PrecisAng,
        Precis_long: editor.PrecisLong,
        Regle: editor.Regle,
        Regle_grad: editor.Regle_grad,
        Compas: editor.Compas,
        Rapporteur: editor.Rapporteur,
        Equerre: editor.Equerre,
        Rotation: editor.Rotation,
        Agrandir: editor.Agrandir,
        Sujet_ordonne: editor.ordonneL.reponse === 'Oui',
        theme: editor.themeL.reponse,
        nbrepetitions: editor.nbrepetitions.value,
        nbchances: editor.nbchances.value
      }
      resolve(newValues)
    }

    try {
      const btnsContainer = j3pAddElt(container, 'div', 'Chargement en cours…', { style: { margin: '1rem auto', textAlign: 'center' } })
      loadMq()
        .then(() => {
          container.parentNode.style.opacity = 1
          j3pEmpty(btnsContainer)
          const btnProps = { style: { margin: '0.2rem', fontSize: '120%' } }
          const btnAnnuler = j3pAddElt(btnsContainer, 'button', 'Annuler', btnProps)
          editor.btnValider = j3pAddElt(btnsContainer, 'button', 'Valider', btnProps)
          btnAnnuler.addEventListener('click', () => {
            if (document.fullscreenElement) document.exitFullscreen()
            resolve()
          })
          editor.btnValider.addEventListener('click', () => {
            if (editor.modifEnCours) {
              const yy = j3pModale2({ titre: 'Avertissement ', contenu: 'Un exercice est en cours\n de modification.', divparent: container })
              j3pAddContent(yy, '\n')
              j3pAjouteBouton(yy, () => {
                j3pDetruit(yy)
                j3pDetruit(yy.masque)
              }, { value: 'Annuler' })
              j3pAddContent(yy, '\n')
              j3pAddContent(yy, '\n')
              j3pAjouteBouton(yy, () => {
                j3pDetruit(yy)
                j3pDetruit(yy.masque)
                faisFin()
              }, { value: 'Tant pis' })
              return
            }
            faisFin()
          })
          getMtgCore({ withMathJax: true, withApi: true })
            .then(mtgAppLecteur => {
              editor.mtgAppLecteur = mtgAppLecteur
              editor.listePourCalc = mtgAppLecteur.createList('TWF0aEdyYXBoSmF2YTEuMAAAABI+TMzNAAJmcv###wEA#wEAAAAAAAAAAAQsAAACvQAAAQEAAAAAAAAAAQAAAAP#####AAAAAQAKQ0NhbGNDb25zdAD#####AAJwaQAWMy4xNDE1OTI2NTM1ODk3OTMyMzg0Nv####8AAAABAApDQ29uc3RhbnRlQAkh+1RELRj#####AAAAAQAHQ0NhbGN1bAD#####AAFlAAZleHAoMSn#####AAAAAgAJQ0ZvbmN0aW9uBwAAAAE#8AAAAAAAAAAAAAIA#####wABeAABMAAAAAEAAAAAAAAAAP###############w==')
              initEditor()
              creeLesDiv()
              if (editor.foKit) return
              menuBase()
            })
        })
        .catch(reject)
    } catch (error) {
      reject(error)
    }
  })
}
