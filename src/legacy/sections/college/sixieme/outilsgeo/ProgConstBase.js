import $ from 'jquery'
import { j3pModale2 } from 'src/legacy/outils/zoneStyleMathquill/functions'
import { getTextFigure, isNomCercleValide, isNomDroiteValide } from 'src/legacy/sections/college/sixieme/outilsgeo/txtFig2'
import { j3pAddContent, j3pAddElt, j3pAjouteBouton, j3pArrondi, j3pClone, j3pDetruit, j3pEmpty, j3pGetNewId, j3pGetRandomBool, j3pGetRandomInt, j3pImage, j3pNotify, j3pShuffle } from 'src/legacy/core/functions'
import { j3pAffiche } from 'src/lib/mathquill/functions'
import { addDefaultTable } from 'src/legacy/themes/table'
import { j3pCreeSVG } from 'src/legacy/core/functionsSvg'
import ZoneStyleMathquill1 from 'src/legacy/outils/zoneStyleMathquill/ZoneStyleMathquill1'
import { j3pBaseUrl } from 'src/lib/core/constantes'
import './progconst.css'

class ProgConstBase {
  constructor (ds, me, fig) {
    this.txtF = fig
    switch (fig) {
      case 'Fig1':
        this.width = 820
        this.height = 460
        this.bx = 761
        this.xptcase = 814
        this.lXcentre = 400
        this.lYcentre = 230
        break
      case 'FigEditor':
        this.width = 400
        this.height = 460
        this.bx = 757.5
        this.xptcase = 814
        this.lXcentre = 200
        this.lYcentre = 230
        break
      case 'FigLibre':
        this.width = 990
        this.height = 768
        this.bx = 934
        this.xptcase = 930 + 57
        this.lXcentre = 450
        this.lYcentre = 365
    }
    this.ds = ds
    this.me = me
    this.imagePoint = j3pBaseUrl + 'sections/college/sixieme/Elementgeo/images/Poiint.png'
    this.imageRegle = j3pBaseUrl + 'sections/college/sixieme/Elementgeo/images/reg.png'
    this.imageEquerre = j3pBaseUrl + 'sections/college/sixieme/Elementgeo/images/equerre.png'
    this.imageRegle2 = j3pBaseUrl + 'sections/college/sixieme/Elementgeo/images/reg2.png'
    this.imageRapporteur = j3pBaseUrl + 'sections/college/sixieme/Elementgeo/images/rap.png'
    this.imageCompas = j3pBaseUrl + 'sections/college/sixieme/Elementgeo/images/comp.png'
    this.imageGomme = j3pBaseUrl + 'sections/college/sixieme/Elementgeo/images/gomm.png'
    this.imageRouge = j3pBaseUrl + 'sections/college/sixieme/Elementgeo/images/rouge.png'
    this.imageCrayon = j3pBaseUrl + 'sections/college/sixieme/Elementgeo/images/CRAY3.png'
    this.imageVerrou = j3pBaseUrl + 'sections/college/sixieme/Elementgeo/images/verrouv.png'
    this.imageVerrou2 = j3pBaseUrl + 'sections/college/sixieme/Elementgeo/images/verrfer.png'
    this.tempo = 20
    // a change
    this.tempodeb = 500
    this.depCentreCo = 2
    this.tempocligne = 400
    this.reglebasx = 300
    this.reglebasy = this.lYcentre
    this.nbClignoteMAx = 10

    ///
    this.distcol = 15
    // html cases
    this.polyhaut = ['poly0', 'poly1', 'poly2', 'poly3', 'poly4', 'poly5', 'poly6', 'poly7']
    this.polydroite = ['#polyd7', '#polyd6', '#polyd5', '#polyd4', '#polyd3', '#polyd2', '#polyd1']
    this.ptsabouge = ['B', '#eq1', '#rappt1', '#compt1', '#compt2', '#ptCa1', '#ptCa2', 'Q', '#raprap10']
    this.idImCray = '#ImLEcray'
    this.idImCrayPt = '#ptLEcray'
    this.pointpourpo = '#ptpourpt'
    // regle
    this.idRegfleGrad = ['#reg4', '#reg1', '#reg3', '#reg2']
    this.idRegle = ['#regs1', '#regs2', '#regs3', '#regsu1', '#regsu2', '#regsu3', '#regsu4', '#regs4', '#regs5', '#regs6', '#regs7', '#titrait', '#rgb']
    this.idReglePoly = ['#regpo1', '#regpo2', '#regpo3', '#fghjk']
    this.idPtRegle = { pt: '#regpt1', ma: '#mqreg', ptCap: '#ptcapregle', ptCap2: '#regptcap2' }
    // , pt2: '#regpt2'
    this.reglettletemps = ['#regtt']
    this.regtra = ['#rgTra1', '#regTra2', '#regTra3', '#regIm2', '#regsegseg']
    // equerre
    this.idEquerre = ['#eqseg1', '#eqseg2', '#eqseg3', '#eqseg4', '#eqb']
    this.idEquerrePoly = ['#eqpoly1', '#eqpoly2']
    // , '#eqpoly3'
    this.EquerSurfa = '#eqsurf1'
    this.idPtEquerre = { pt: '#eq1', pt2: '#pteqma0', ma: '#eqma1' }
    this.DtEque = [['#eqqq1', '#eqqq2'], ['#eqqq3', '#eqqq4'], ['#eqqq5', '#eqqq6']]
    this.equttletemps = ['#eqdt1', '#eqdt2', '#eqdt3']
    /// raporteur
    this.idRap = ['#rapc2', '#rap14', '#rap15', '#rap16', '#rapl1', '#rapl2', '#rapl3', '#rapl4', '#rapl5', '#rapl6', '#raps1', '#raps2', '#raps3', '#raps4', '#raps5', '#raprap1', '#raprap2', '#raprap3', '#raprap4', '#rappo9', '#rap1', '#raprap13', '#raprap12']
    this.idRapPoly = ['#rap12', '#rap13', '#rappo1', '#rappo2']
    this.idPRap = { pt: 'rappt1', pt3: 626 }
    // , ma: '#rapma',  pt2: '#rapp2'
    this.rapttletemps = '#raptt'
    // compas
    this.idComp1 = ['#compsur1', '#compsur2', '#compsur3', '#compsur4', '#compma1', '#compma2', '#compsur7', '#compsur6']
    this.idComp = ['#comppoly1', '#comppoly2', '#comppoly3', '#comppoly4', '#comppoly5', '#comppoly6']
    this.idCompPt = { pointe: 'compt1', geremine: 'compt2', mine: '#compt3', range: '#compt4', compcoupt: '#compcoupt' }
    this.idCompttletps = '#comptt'
    this.idCoparcd = ['#ctrac1', '#ctrap1', '#ctrap2', '#ctraa1']
    this.idCoparcs = ['#ctrac2', '#ctrap3', '#ctrap4', '#ctraa2']
    this.compaTra = ['#comppoly7', '#comppoly8', '#comppoly9', '#comppoly10', '#comppoly12', '#comptrasur1', '#comptrasur2', '#comptrasur3', '#comptrasur4', '#comptrasur5', '#comptrasur6']

    this.makeFoncOutList = this.makeFoncOut.bind(this)
    this.makeFoncOverList = this.makeFoncOver.bind(this)
    this.makePourVoirList = this.makePourVoir.bind(this)

    this.makeOvGoList = this.makeOvGo.bind(this)
    this.makeOvGoAList = this.makeOvGoA.bind(this)
    this.makeOvGoNList = this.makeOvGoN.bind(this)
    this.makeOuGoList = this.makeOuGo.bind(this)
    this.makeOuGoAList = this.makeOuGoA.bind(this)
    this.makeOuGoNList = this.makeOuGoN.bind(this)
    this.makecliGoList = this.makecliGo.bind(this)
    this.makecliGoAList = this.makecliGoA.bind(this)
    this.makecliGoNList = this.makecliGoN.bind(this)
    this.sortQuestDList = this.sortQuestD.bind(this)
    this.sortQuestList = this.sortQuest.bind(this)
    this.affaide2LIst = this.affaide2.bind(this)
    this.maxInt = 999999999
  }

  sortOnDistProp = (a, b) => a.dist - b.dist

  calcule (form) {
    const list = this.listePourCalc
    list.giveFormula2('x', form)
    list.calculateNG()
    return String(this.listePourCalc.valueOf('x'))
  }

  affaide () {
    const yy = this.affModale('', '<b>Aide Construction</b>')
    this.tabaidegh = addDefaultTable(yy, 1, 1)[0][0]
    const tfty = addDefaultTable(this.tabaidegh, 9, 2)
    j3pAffiche(tfty[1][0], null, 'Placer un point')
    const f1 = function () { this.affaide2LIst('pt') }
    const f2 = function () { this.affaide2LIst('regle') }
    const f3 = function () { this.affaide2LIst('regle2') }
    const f4 = function () { this.affaide2LIst('equerre') }
    const f5 = function () { this.affaide2LIst('rapporteur') }
    const f6 = function () { this.affaide2LIst('compas') }
    const f7 = function () { this.affaide2LIst('gomme') }
    const f8 = function () { this.affaide2LIst('renomme') }
    j3pAjouteBouton(tfty[1][1], f1.bind(this), { className: '', value: 'Voir' })
    if (this.ds.Regle_grad === true || this.ds.Regle_grad === 'true') {
      j3pAffiche(tfty[2][0], null, 'Utiliser la règle graduée')
      j3pAjouteBouton(tfty[2][1], f2.bind(this), { className: '', value: 'Voir' })
    }
    if (this.ds.Regle === true || this.ds.Regle === 'true') {
      j3pAffiche(tfty[3][0], null, 'Utiliser la règle')
      j3pAjouteBouton(tfty[3][1], f3.bind(this), { className: '', value: 'Voir' })
    }
    if (this.ds.Equerre === true || this.ds.Equerre === 'true') {
      j3pAffiche(tfty[4][0], null, 'Utiliser l’équerre')
      j3pAjouteBouton(tfty[4][1], f4.bind(this), { className: '', value: 'Voir' })
    }
    if (this.ds.Rapporteur === true || this.ds.Rapporteur === 'true') {
      j3pAffiche(tfty[5][0], null, 'Utiliser le rapporteur')
      j3pAjouteBouton(tfty[5][1], f5.bind(this), { className: '', value: 'Voir' })
    }
    if (this.ds.Compas === true || this.ds.Compas === 'true') {
      j3pAffiche(tfty[6][0], null, 'Utiliser le compas')
      j3pAjouteBouton(tfty[6][1], f6.bind(this), { className: '', value: 'Voir' })
    }
    j3pAffiche(tfty[7][0], null, 'Utiliser la gomme')
    j3pAjouteBouton(tfty[7][1], f7.bind(this), { className: '', value: 'Voir' })
    j3pAffiche(tfty[8][0], null, 'Renommer une droite/un cercle&nbsp;&nbsp;')
    j3pAjouteBouton(tfty[8][1], f8.bind(this), { className: '', value: 'Voir' })
  }

  affaide2 (koi) {
    j3pEmpty(this.tabaidegh)
    const tyiyui = addDefaultTable(this.tabaidegh, 10, 1)
    let ptihd, froirf, kflmkmds, fjdslkdi
    switch (koi) {
      case 'pt':
        ptihd = addDefaultTable(tyiyui[0][0], 1, 3)
        j3pAffiche(ptihd[0][0], null, '1. Clique sur ')
        this.metsImage(ptihd[0][1], this.imagePoint)
        j3pAffiche(ptihd[0][2], null, ' quand tu veux placer puis nommer un point. ')
        froirf = addDefaultTable(tyiyui[1][0], 1, 1)
        j3pAffiche(froirf[0][0], null, '2. Clique sur la figure pour nommer (ou renommer) un point.')
        break
      case 'regle':
        ptihd = addDefaultTable(tyiyui[0][0], 1, 3)
        j3pAffiche(ptihd[0][0], null, '1. Clique sur ')
        this.metsImage(ptihd[0][1], this.imageRegle2)
        j3pAffiche(ptihd[0][2], null, ' pour faire apparaitre la règle. ')
        froirf = addDefaultTable(tyiyui[1][0], 1, 1)
        j3pAffiche(froirf[0][0], null, '2. En cliquant sur la règle (et en laissant appuyé) tu peux déplacer la règle.')
        kflmkmds = addDefaultTable(tyiyui[2][0], 1, 3)
        j3pAffiche(kflmkmds[0][0], null, '3. En cliquant sur ')
        j3pAffiche(kflmkmds[0][2], null, ' (et en laissant appuyé) tu peux faire tourner la règle.')
        this.metsImage(kflmkmds[0][1], this.imageRouge)
        fjdslkdi = addDefaultTable(tyiyui[3][0], 1, 3)
        j3pAffiche(fjdslkdi[0][0], null, '4. Clique sur ')
        j3pAffiche(fjdslkdi[0][2], null, ' quand tu souhaites tracer.')
        this.metsImage(fjdslkdi[0][1], this.imageCrayon)
        ptihd = addDefaultTable(tyiyui[4][0], 2, 1)
        j3pAffiche(ptihd[0][0], null, '5. Approche le crayon du bord de la règle. ')
        j3pAffiche(ptihd[1][0], null, '6. Clique pour commencer le segment, puis clique pour terminer le tracé. ')
        break
      case 'regle2':
        ptihd = addDefaultTable(tyiyui[0][0], 1, 3)
        j3pAffiche(ptihd[0][0], null, '1. Clique sur ')
        this.metsImage(ptihd[0][1], this.imageRegle)
        j3pAffiche(ptihd[0][2], null, ' pour faire apparaitre la règle. ')
        froirf = addDefaultTable(tyiyui[1][0], 1, 1)
        j3pAffiche(froirf[0][0], null, '2. En cliquant sur la règle (et en laissant appuyé) tu peux déplacer la règle.')
        kflmkmds = addDefaultTable(tyiyui[2][0], 1, 3)
        j3pAffiche(kflmkmds[0][0], null, '3. En cliquant sur ')
        j3pAffiche(kflmkmds[0][2], null, ' (et en laissant appuyé) tu peux faire tourner la règle.')
        this.metsImage(kflmkmds[0][1], this.imageRouge)
        fjdslkdi = addDefaultTable(tyiyui[3][0], 1, 3)
        j3pAffiche(fjdslkdi[0][0], null, '4. Clique sur ')
        j3pAffiche(fjdslkdi[0][2], null, ' quand tu souhaites tracer.')
        this.metsImage(fjdslkdi[0][1], this.imageCrayon)
        ptihd = addDefaultTable(tyiyui[4][0], 3, 1)
        j3pAffiche(ptihd[0][0], null, '5. Approche le crayon du bord de la règle. ')
        j3pAffiche(ptihd[1][0], null, '6. Lâche le crayon pour tracer la droite. ')
        j3pAffiche(ptihd[2][0], null, '<i><b>remarque:</b> Si le tracé passe par des points, ils sont indiqués en rouge. \nDans ce cas, le tracé d’une partie de la droite est possible.</i>')
        break
      case 'gomme':
        ptihd = addDefaultTable(tyiyui[0][0], 1, 3)
        j3pAffiche(ptihd[0][0], null, '1. Clique sur ')
        this.metsImage(ptihd[0][1], this.imageGomme)
        j3pAffiche(ptihd[0][2], null, ' quand tu veux effacer une construction. ')
        froirf = addDefaultTable(tyiyui[1][0], 1, 1)
        j3pAffiche(froirf[0][0], null, '2. Quand tu survoles un élément que tu peux effacer, il devient rouge.')
        kflmkmds = addDefaultTable(tyiyui[2][0], 1, 1)
        j3pAffiche(kflmkmds[0][0], null, '3.Clique sur l’élément pour l’effacer.')
        break
      case 'compas':
        ptihd = addDefaultTable(tyiyui[0][0], 1, 3)
        j3pAffiche(ptihd[0][0], null, '1. Clique sur ')
        this.metsImage(ptihd[0][1], this.imageCompas)
        j3pAffiche(ptihd[0][2], null, ' pour faire apparaitre le compas. ')
        froirf = addDefaultTable(tyiyui[1][0], 1, 3)
        j3pAffiche(froirf[0][0], null, '2. En cliquant sur le compas (et en laissant appuyé) tu peux déplacer le compas.')
        kflmkmds = addDefaultTable(tyiyui[2][0], 1, 3)
        this.metsImage(kflmkmds[0][1], this.imageRouge)
        j3pAffiche(kflmkmds[0][0], null, '3. En cliquant sur ')
        j3pAffiche(kflmkmds[0][2], null, ' tu peux faire tourner et écarter le compas.')
        fjdslkdi = addDefaultTable(tyiyui[3][0], 1, 3)
        j3pAffiche(fjdslkdi[0][0], null, '4. Clique sur ')
        j3pAffiche(fjdslkdi[0][2], null, ' quand tu souhaites tracer.')
        this.metsImage(fjdslkdi[0][1], this.imageCrayon)
        ptihd = addDefaultTable(tyiyui[4][0], 2, 1)
        j3pAffiche(ptihd[0][0], null, '5. Approche le crayon de la mine du compas puis lâche le. ')
        j3pAffiche(ptihd[1][0], null, '6. Déplace la souris pour tracer. Clique quand tu as finis ')
        froirf = addDefaultTable(tyiyui[5][0], 1, 5)
        j3pAffiche(froirf[0][0], null, '\n&nbsp;&nbsp;<i><b>Clique sur  </b></i>')
        j3pAffiche(froirf[0][2], null, '\n&nbsp;<i><b>ou </b></i>&nbsp;')
        j3pAffiche(froirf[0][4], null, '\n<i><b> pour vérouiller l’écartement.</i></b>')
        this.metsImage(froirf[0][1], this.imageVerrou)
        this.metsImage(froirf[0][3], this.imageVerrou2)
        break
      case 'rapporteur':
        ptihd = addDefaultTable(tyiyui[0][0], 1, 3)
        j3pAffiche(ptihd[0][0], null, '1. Clique sur ')
        this.metsImage(ptihd[0][1], this.imageRapporteur)
        j3pAffiche(ptihd[0][2], null, ' pour faire apparaitre le rapporteur. ')
        froirf = addDefaultTable(tyiyui[1][0], 1, 1)
        j3pAffiche(froirf[0][0], null, '2. En cliquant sur le rapporteur (et en laissant appuyé) tu peux le déplacer.')
        kflmkmds = addDefaultTable(tyiyui[2][0], 1, 3)
        this.metsImage(kflmkmds[0][1], this.imageRouge)
        j3pAffiche(kflmkmds[0][0], null, '3. En cliquant sur ')
        j3pAffiche(kflmkmds[0][2], null, ' tu peux le faire tourner.')
        fjdslkdi = addDefaultTable(tyiyui[3][0], 1, 3)
        j3pAffiche(fjdslkdi[0][0], null, '4. Clique sur ')
        j3pAffiche(fjdslkdi[0][2], null, ' quand tu souhaites tracer.')
        this.metsImage(fjdslkdi[0][1], this.imageCrayon)
        ptihd = addDefaultTable(tyiyui[4][0], 1, 1)
        j3pAffiche(ptihd[0][0], null, '5. Approche le crayon du bord du rapporteur. ')
        break
      case 'equerre':
        ptihd = addDefaultTable(tyiyui[0][0], 1, 3)
        j3pAffiche(ptihd[0][0], null, '1. Clique sur ')
        this.metsImage(ptihd[0][1], this.imageEquerre)
        j3pAffiche(ptihd[0][2], null, ' pour faire apparaitre l’équerre. ')
        froirf = addDefaultTable(tyiyui[1][0], 1, 1)
        j3pAffiche(froirf[0][0], null, '2. En cliquant sur l’équerre (et en laissant appuyé) tu peux la déplacer.')
        kflmkmds = addDefaultTable(tyiyui[2][0], 1, 3)
        this.metsImage(kflmkmds[0][1], this.imageRouge)
        j3pAffiche(kflmkmds[0][0], null, '3. En cliquant sur ')
        j3pAffiche(kflmkmds[0][2], null, ' tu peux la faire tourner.')
        fjdslkdi = addDefaultTable(tyiyui[3][0], 1, 3)
        j3pAffiche(fjdslkdi[0][0], null, '4. Clique sur ')
        j3pAffiche(fjdslkdi[0][2], null, ' quand tu souhaites tracer.')
        this.metsImage(fjdslkdi[0][1], this.imageCrayon)
        ptihd = addDefaultTable(tyiyui[4][0], 1, 1)
        j3pAffiche(ptihd[0][0], null, '5. Approche le crayon d’un côté de l’équerre. ')
        break
      case 'renomme':
        j3pAffiche(tyiyui[0][0], null, 'Tu peux renommer une droite ou un cercle que tu as tracé en cliquant dessus. \n<i>(Quand il/elle devient bleu)</i> ')
        break
    }
  }

  metsImage (ou, koi) {
    const ptitab = addDefaultTable(ou, 1, 3)
    ptitab[0][0].style.width = '5px'
    ptitab[0][2].style.width = '5px'
    j3pImage({ conteneur: ptitab[0][1], src: koi, larg: 20 })
  }

  transColor (t) {
    let t1 = Number(t[0]).toString(16)
    if (t1.length === 1) t1 = '0' + t1
    let t2 = Number(t[1]).toString(16)
    if (t2.length === 1) t2 = '0' + t2
    let t3 = Number(t[2]).toString(16)
    if (t3.length === 1) t3 = '0' + t3
    return '#' + t1 + t2 + t3
  }

  setColor2 (a, b, c1, c2, c3, f, t) {
    const hh = this.transColor([c1, c2, c3])
    try {
      this.mtgAppLecteur.setColor({ elt: b.replace('#', ''), color: hh, opacity: (t === 0) ? 0 : 1 })
    } catch (e) {
      // eslint-disable-next-line no-console
      console.log('pas pas modif un objet pas visible')
    }
  }

  copiede (tab) {
    if (Array.isArray(tab)) {
      const ret = []
      for (let i = 0; i < tab.length; i++) {
        if (Array.isArray(tab[i])) {
          ret[i] = this.copiede(tab[i])
        } else {
          ret[i] = tab[i]
        }
      }
      return ret
    } else {
      const ret = {}
      for (const prop in tab) {
        ret.prop = tab[prop]
      }
      return ret
    }
  }

  consoleDev (t) {
    if (!this.ds.dev) return
    if (t === this.devmov) return
    this.comtdev++
    if (this.comtdev > 10) {
      j3pEmpty(this.dev)
      this.comtdev = 0
    }
    j3pAffiche(this.dev, null, t + '\n')
    this.devmov = t
  }

  faiszomm (bool, f, jkl) {
    this.viregommEtrot()
    this.ptmil = this.mtgAppLecteur.getPointPosition({ absCoord: true, a: 'Z2' })
    if (jkl === true) {
      const xmin = this.pbbase.x - this.distab
      const xmax = this.pbbase.x + this.distab
      const ymin = this.pbbase.y - this.distab
      const ymax = this.pbbase.y + this.distab
      const acox = Math.max(xmax - 2 * this.lXcentre, -xmin)
      const acoy = Math.max(ymax - 2 * this.lYcentre, -ymin)
      if (acox <= 0 && acoy <= 0) return false
      if (acox > acoy) {
        this.mod = this.lXcentre / (this.lXcentre + acox)
      } else {
        this.mod = this.lYcentre / (this.lYcentre + acoy)
      }
      this.zoom *= this.mod
    } else {
      if (f !== true) {
        if (bool && this.zoom >= 2) return
        if (!bool && this.zoom <= 0.8) return
        this.mod = 1.1

        if (!bool) this.mod = 1 / 1.1
        this.zoom *= this.mod
      } else {
        this.mod = 1 / this.zoom
        this.zoom = 1
      }
    }
    if (this.regleVisible) {
      this.zoomize('B')
      this.zoomize('#pttttt')
    }
    if (this.equerreVisible) {
      this.zoomize('eq1')
      this.zoomize('#pteqma0')
    }
    if (this.rapporteurVisible) {
      this.zoomize('rappt1')
      this.zoomize('raprap10')
    }
    if (this.compasVisible) {
      this.zoomize('#compt1')
      this.zoomize('#compt2')
      this.zoomize('#ptverB')
    }
    this.zoomize('#ptCa1')
    this.zoomize('#ptCa2')
    this.zoomize('Q')
    for (let i = 0; i < this.listeSegPris.length; i++) {
      this.zoomize(this.listeSegPris[i].point1)
      this.zoomize(this.listeSegPris[i].point2)
      if (this.listeSegPris[i].pointNom) this.zoomize(this.listeSegPris[i].pointNom)
    }
    for (let i = 0; i < this.listeDtePris.length; i++) {
      this.zoomize(this.listeDtePris[i].point1)
      this.zoomize(this.listeDtePris[i].point2)
      if (this.listeDtePris[i].pointNom) {
        this.zoomize(this.listeDtePris[i].pointNom)
      }
    }
    for (let i = 0; i < this.listeDemiDPris.length; i++) {
      this.zoomize(this.listeDemiDPris[i].point1)
      this.zoomize(this.listeDemiDPris[i].point2)
      if (this.listeDemiDPris[i].pointNom) this.zoomize(this.listeDemiDPris[i].pointNom)
    }
    for (let i = 0; i < this.listeArcPris.length; i++) {
      this.zoomize(this.listeArcPris[i].centre)
      this.zoomize(this.listeArcPris[i].p1)
      this.zoomize(this.listeArcPris[i].p2)
      this.zoomize(this.listeArcPris[i].p3)
      this.zoomize(this.listeArcPris[i].p4)
      this.zoomize(this.listeArcPris[i].p5)
      this.zoomize(this.listeArcPris[i].p6)
      this.zoomize(this.listeArcPris[i].p7)
      this.zoomize(this.listeArcPris[i].p8)
      this.zoomize(this.listeArcPris[i].p9)
      this.zoomize(this.listeArcPris[i].p10)
      this.zoomize(this.listeArcPris[i].p11)
      this.zoomize(this.listeArcPris[i].p12)
      if (this.listeArcPris[i].pointNom) this.zoomize(this.listeArcPris[i].pointNom)
    }
    for (let i = 0; i < this.listePointPris.length; i++) {
      this.zoomize(this.listePointPris[i].point)
    }
    for (let i = this.ConsSegnb; i < this.segCo.length; i++) {
      this.zoomize(this.segCo[i].html.point1)
      this.zoomize(this.segCo[i].html.point2)
    }
    for (let i = this.ConsDtenb; i < this.dteco.length; i++) {
      this.zoomize(this.dteco[i].html.point1)
      this.zoomize(this.dteco[i].html.point2)
    }
    for (let i = this.ConsDemiDnb; i < this.demiDco.length; i++) {
      this.zoomize(this.demiDco[i].html.point1)
      this.zoomize(this.demiDco[i].html.point2)
    }
    for (let i = this.ConsArcnb; i < this.arcco.length; i++) {
      this.zoomize(this.arcco[i].html.centre)
      this.zoomize(this.arcco[i].html.p1)
      this.zoomize(this.arcco[i].html.p2)
    }
    for (let i = this.ConsPtnb; i < this.ptCo.length; i++) {
      this.zoomize(this.ptCo[i].html.point)
    }
    for (let i = 0; i < this.nomco.length; i++) {
      this.zoomize(this.nomco[i].html[0])
    }

    this.zoomize('#unit1')
    this.zoomize('#unit2')
    this.mtgAppLecteur.updateFigure(this.svgId)
    const a = this.mtgAppLecteur.getPointPosition({ absCoord: true, a: 'unit1' })
    const b = this.mtgAppLecteur.getPointPosition({ absCoord: true, a: 'unit2' })
    this.unite = this.dist(a, b)
    this.mtgAppLecteur.giveFormula2(this.svgId, 'Lc', this.unitebase / this.unite)
    if (jkl === true) return true
  }

  retrouveNomDroite (ki) {
    let aret = false
    this.nomsprisD.forEach((el) => {
      if (el.ki === ki) aret = el
    })
    return aret
  }

  zoomize (ki) {
    const pt = this.mtgAppLecteur.getPointPosition({ absCoord: true, a: ki.replace('#', '') })
    const newx = {}
    const xM = pt.x - this.lXcentre
    const yM = pt.y - this.lYcentre
    newx.x = xM * this.mod + this.lXcentre
    newx.y = yM * this.mod + this.lYcentre
    this.mtgAppLecteur.setPointPosition(this.svgId, ki, newx.x, newx.y, false)
  }

  makefig () {
    this.leslongCoConst = []
    this.listeZone = []
    this.listeTag = 0
    this.svgId = j3pGetNewId(this.txtF)
    this.angleTotal = 0
    this.zoomTotal = 1
    this.equerreVisible = this.txtF !== 'FigEditor'
    this.rapporteurVisible = this.txtF !== 'FigEditor'
    this.regleVisible = this.txtF !== 'FigEditor'
    this.compasVisible = this.txtF !== 'FigEditor'
    // adapte avec données
    this.regleEstGraduee = true
    this.captureFond = false
    this.capcray = false
    this.laCoool = [150, 150, 150]
    this.capRegle = false
    this.yacoul = false
    this.capRegle2 = false
    this.poitilEncours = false
    this.capEquerre2 = false
    this.PtRegle = false
    this.PtRegle2 = false
    this.capEquer = false
    this.PtCompS = false
    this.PtCompS2 = false
    this.lalabase = 0
    this.lala = 0
    this.BBloque = false
    this.lagomm = false
    this.lepoint = false
    this.lepoint22 = false
    this.lepoint3 = false
    this.lepoint4 = false
    this.lagrad = false
    this.x = this.y = 0
    this.bloqueEventm = false
    this.comtdev = 0
    this.capRap = false
    this.capComp = false
    this.capEquerre2 = false
    this.capComp2 = false
    this.capRap2 = false
    const txtFigure = getTextFigure(this.txtF)

    j3pEmpty(this.zonefig)
    if (!this.blokRemove) {
      this.leSvg = j3pCreeSVG(this.zonefig, { id: this.svgId, width: this.width, height: this.height, style: { border: 'solid black 1px' } })
      this.mtgAppLecteur.removeAllDoc()
    } else {
      if (this.yaOldSvg !== '') {
        this.mtgAppLecteur.removeDoc(this.yaOldSvg)
      }
      this.leSvg = j3pCreeSVG(this.zonefig, { id: this.svgId, width: this.width, height: this.height, style: { border: 'solid black 1px' } })
    }
    this.mtgAppLecteur.addDoc(this.svgId, txtFigure, true)
    this.mtgAppLecteur.setApiDoc(this.svgId)
    this.mtgAppLecteur.calculateFirstTime(this.svgId)
    this.mtgAppLecteur.display(this.svgId)
    // chope point pour centre
    this.mtgAppLecteur.setPointPosition(this.svgId, '#ptCase', this.xptcase, 3, true)
    this.ptbaseX = this.mtgAppLecteur.getPointPosition({ absCoord: true, a: 'Z2' })
    this.mtgAppLecteur.addLengthMeasure({ a: 'Z9', b: 'Z10' })
    // affiche les case horizontale
    if (this.txtF !== 'FigEditor') {
      let nbh = 3
      this.tabh = [
        { k: 'point', im: 'PointIm', pt: '#Pointpt', func: this.point.bind(this) },
        { k: 'gomme', im: 'Imgom', pt: '#ptImgom', func: this.gomme.bind(this) },
        { k: 'centre', im: 'Imcent', pt: '#ptImcent', func: this.recentrebis.bind(this) }
      ]
      if (this.ds.Regle || this.ds.Regle_grad || this.ds.Equerre || this.ds.Rapporteur || this.ds.Compas) {
        nbh++
        this.tabh.splice(0, 0, { k: 'point1', im: 'Imcray', pt: '#ptImcray', func: this.point2.bind(this) })
      }
      if (this.ds.Rotation === true || this.ds.Rotation === 'true') {
        nbh++
        this.tabh.push({ k: 'rotation', im: 'Imrot', pt: '#ptImrot', func: this.rot.bind(this) })
      }
      if (this.ds.Agrandir === true || this.ds.Agrandir === 'true') {
        nbh += 2
        this.tabh.push({ k: 'plus', im: 'Implus', pt: '#ptImplus', func: this.agrand.bind(this) })
        this.tabh.push({ k: 'moins', im: 'Immoins', pt: '#ptImmoins', func: this.reduit.bind(this) })
      }
      if (this.yaaapointill) {
        nbh += 1
        this.tabh.push({ k: 'pointille', im: 'ImPointille', pt: '#ptImPointille', func: this.pointill.bind(this) })
      }
      let bx = this.bx
      let ix = -54
      for (let i = 0; i < this.polyhaut.length; i++) {
        if (i < nbh) {
          this.mtgAppLecteur.setPointPosition(this.svgId, this.tabh[i].pt, bx + ix * i, 5, true)
          this.mtgAppLecteur.setVisible({ elt: this.tabh[i].im })
        }
      }
      for (let i = 0; i < this.polyhaut.length; i++) {
        if (i < nbh) {
          this.mtgAppLecteur.addEventListener(this.svgId, '#' + this.tabh[i].im, 'mouseover', this.makeFoncOverList(i, 0, this.tabh[i].k))
          this.mtgAppLecteur.addEventListener(this.svgId, '#' + this.tabh[i].im, 'mouseout', this.makeFoncOutList(i, 0, this.tabh[i].k))
          this.mtgAppLecteur.addEventListener(this.svgId, '#' + this.tabh[i].im, 'click', this.tabh[i].func)
          this.mtgAppLecteur.addEventListener(this.svgId, '#' + this.tabh[i].im, 'touchstart', this.makePourVoirList(this.polyhaut[i]))
        }
      }

      this.nbh = nbh
      // affiche les case verticale
      this.tabv = []
      if (this.ds.Regle === true || this.ds.Regle === 'true') {
        this.tabv.push({ k: 'regle', im: '#Imreg2', pt: '#ptImreg2', func: this.regle.bind(this) })
      }
      if (this.ds.Regle_grad === true || this.ds.Regle_grad === 'true') {
        this.tabv.push({ k: 'regle', im: '#Imreg', pt: '#ptImreg', func: this.regle2.bind(this) })
      }
      if (this.ds.Equerre === true || this.ds.Equerre === 'true') {
        this.tabv.push({ k: 'equerre', im: '#Imeq', pt: '#ptImeq', func: this.equerre.bind(this) })
      }
      if (this.ds.Rapporteur === true || this.ds.Rapporteur === 'true') {
        this.tabv.push({ k: 'rapporteur', im: '#Imrap', pt: '#ptImrap', func: this.rapporteur.bind(this) })
      }
      if (this.ds.Compas === true || this.ds.Compas === 'true') {
        this.tabv.push({ k: 'compas', im: '#Imcomp', pt: '#ptImcomp', func: this.compas.bind(this) })
      }
      bx = 56
      ix = 51
      for (let i = 0; i < this.polydroite.length; i++) {
        this.setVisible2(this.svgId, this.polydroite[i], false, false)
        if (i < this.tabv.length) {
          this.mtgAppLecteur.setPointPosition(this.svgId, this.tabv[i].pt, this.bx, bx + ix * i, false)
          this.setVisible2(this.svgId, this.tabv[i].im, true, false)
        }
      }
      if (this.txtF === 'FigLibre') {
        this.mtgAppLecteur.setPointPosition(this.svgId, '#ptCoul', 552, 3, false)
        this.mtgAppLecteur.setPointPosition(this.svgId, '#ptptpt', 552, 3, false)
        this.mtgAppLecteur.updateFigure(this.svgId)
        const ff1 = () => {
          this.mtgAppLecteur.setColor({ elt: 'polyCoul', color: '#a60000', opacity: 0.8 })
          this.mtgAppLecteur.setVisible({ elt: 'polyCoul' })
          this.mtgAppLecteur.setLineStyle({ elt: 'polyCoul', lineStyle: 'line' })
          this.mtgAppLecteur.getDoc(this.svgId).defaultCursor = 'pointer'
        }
        const ff2 = () => {
          this.mtgAppLecteur.setColor({ elt: 'polyCoul', color: '#a60000', opacity: 0 })
          this.mtgAppLecteur.getDoc(this.svgId).defaultCursor = ''
        }
        const ff3 = () => {
          this.viregommEtrot(false, true)
          this.faisCoul()
        }
        this.mtgAppLecteur.addEventListener(this.svgId, '#polyCoul', 'mouseover', ff1.bind(this))
        this.mtgAppLecteur.addEventListener(this.svgId, '#polyCoul', 'mouseout', ff2.bind(this))
        this.mtgAppLecteur.addEventListener(this.svgId, '#polyCoul', 'click', ff3.bind(this))
        this.mtgAppLecteur.addEventListener(this.svgId, '#polyCoul', 'touchstart', ff3.bind(this))
        this.mtgAppLecteur.updateFigure(this.svgId)
      }
      for (let i = 0; i < this.polydroite.length; i++) {
        if (i < this.tabv.length) {
          this.mtgAppLecteur.addEventListener(this.svgId, this.tabv[i].im, 'mouseover', this.makeFoncOverList(i, 1))
          this.mtgAppLecteur.addEventListener(this.svgId, this.tabv[i].im, 'mouseout', this.makeFoncOutList(i, 1))
          this.mtgAppLecteur.addEventListener(this.svgId, this.tabv[i].im, 'click', this.tabv[i].func)
          this.mtgAppLecteur.addEventListener(this.svgId, this.tabv[i].im, 'touchstart', this.makePourVoirList(this.polydroite[i]))
        }
      }
    }

    this.functionmousedown = function (ev) {
      if (this.bloqueEventm) return
      if (this.onco) return
      this.consoleDev('mousedown Fig')
      if (this.PtRegle) {
        this.gereFinRegle()
      }
      if (this.PtCompS) {
        this.gereFinCOmpas()
      }
      if (this.unecapt()) return
      if (this.lepoint || this.lepoint4) return
      this.captureFond = true
      this.mtgAppLecteur.getDoc(this.svgId).defaultCursor = 'pointer'
      this.x = ev.clientX
      this.y = ev.clientY
    }
    this.functiontouchstart = function (ev) {
      if (this.bloqueEventm) return
      this.consoleDev('touchstart Fig')
      if (this.larot) return
      if (this.lagomm) {
        this.grossitout()
        return
      }
      if (this.PtRegle) {
        this.x = ev.changedTouches[0].clientX
        this.y = ev.changedTouches[0].clientY
        const GG = this.zonefig.getBoundingClientRect()

        this.x = this.x - GG.x
        this.y = this.y - GG.y
        this.mtgAppLecteur.setPointPosition(this.svgId, '#pointlibre', this.x, this.y, true)
        this.PtRegle = false
        this.PtRegle2 = true
        this.captureFond = false
        return
      }
      if (this.PtCompS) {
        this.x = ev.changedTouches[0].clientX
        this.y = ev.changedTouches[0].clientY
        const GG = this.zonefig.getBoundingClientRect()

        this.x = this.x - GG.x
        this.y = this.y - GG.y
        this.mtgAppLecteur.setPointPosition(this.svgId, '#pointlibre', this.x, this.y, true)
        this.F = false
        this.PtCompS = false
        this.PtCompS2 = true
        this.captureFond = false
        return
      }
      if (this.lepoint2) {
        this.point22(ev)
        this.lepoint2 = false
        return
      }
      if (this.lepoint) {
        this.point3(ev)
        this.lepoint = false
        return
      }
      if (this.unecapt()) return
      if (this.lepoint || this.lepoint4) return

      this.x = ev.changedTouches[0].clientX
      this.y = ev.changedTouches[0].clientY
      const GG = this.zonefig.getBoundingClientRect()
      this.x = this.x - GG.x
      this.y = this.y - GG.y
      if (this.x > 420 && this.y < 72) return
      if (this.x > 740 && this.y < 320) return
      let tmoio = this.getPointPosition2(this.svgId, '#pttttt')
      const gfdjkslg = { x: this.x, y: this.y }
      if (this.dist(gfdjkslg, tmoio) < 20) return
      tmoio = this.getPointPosition2(this.svgId, '#pteqma0')
      if (this.dist(gfdjkslg, tmoio) < 20) return
      tmoio = this.getPointPosition2(this.svgId, this.idCompPt.geremine)
      if (this.dist(gfdjkslg, tmoio) < 20) return
      tmoio = this.getPointPosition2(this.svgId, 'raprap10')
      if (this.dist(gfdjkslg, tmoio) < 20) return
      tmoio = this.getPointPosition2(this.svgId, '#pourlim')
      if (this.dist(gfdjkslg, tmoio) < 20) return
      tmoio = this.getPointPosition2(this.svgId, this.idCompPt.mine)
      if (this.dist(gfdjkslg, tmoio) < 20) return
      for (let i = 0; i < this.listePointPris.length; i++) {
        tmoio = this.getPointPosition2(this.svgId, this.listePointPris[i].suiv)
        if (this.dist(gfdjkslg, tmoio) < 20) return
      }
      this.captureFond = true
      this.mtgAppLecteur.setPointPosition(this.svgId, '#pointlibre', this.x, this.y, true)
    }
    this.functionmousemove = function (ev) {
      if (this.bloqueEventm) return
      this.consoleDev('mousemove Fig')
      this.mousemove(ev)
    }
    this.functionmouseup = function (ev) {
      if (this.bloqueEventm) return
      this.consoleDev('mouseup Fig')
      this.mouseup(ev)
    }
    this.functiontouchmove = function (ev) {
      this.consoleDev('touchmove Fig')
      this.ttouchemove(ev)
    }
    this.functiontouchend = function () {
      if (this.bloqueEventm) return
      this.consoleDev('touchend Fig')
      const bup = this.getPointPosition2(this.svgId, '#pointlibre')
      this.mtgAppLecteur.setPointPosition(this.svgId, '#pointlibre', 1000, 1000, false)
      this.mouseup({ clientX: bup.x, clientY: bup.y })
    }
    this.functiontouchleave = function () {
      this.consoleDev('touchleave Fig')
      const bup = this.getPointPosition2(this.svgId, '#pointlibre')
      this.mtgAppLecteur.setPointPosition(this.svgId, '#pointlibre', 1000, 1000, false)
      this.mouseup({ clientX: bup.x, clientY: bup.y })
    }
    this.functionbor = function () {
      this.BBloque = true
    }
    this.mtgAppLecteur.addCallBackToSVGListener(this.svgId, 'mousedown', this.functionmousedown.bind(this))
    this.mtgAppLecteur.addCallBackToSVGListener(this.svgId, 'touchstart', this.functiontouchstart.bind(this))
    this.mtgAppLecteur.addCallBackToSVGListener(this.svgId, 'mousemove', this.functionmousemove.bind(this))
    this.mtgAppLecteur.addCallBackToSVGListener(this.svgId, 'mouseup', this.functionmouseup.bind(this))
    this.mtgAppLecteur.addCallBackToSVGListener(this.svgId, 'touchmove', this.functiontouchmove.bind(this))
    this.mtgAppLecteur.addCallBackToSVGListener(this.svgId, 'touchend', this.functiontouchend.bind(this))
    this.mtgAppLecteur.addCallBackToSVGListener(this.svgId, 'touchleave', this.functiontouchleave.bind(this))
    this.mtgAppLecteur.addEventListener(this.svgId, '#bord1', 'mousedown', this.functionbor.bind(this))
    this.mtgAppLecteur.addEventListener(this.svgId, '#bord2', 'mousedown', this.functionbor.bind(this))
    this.mtgAppLecteur.addEventListener(this.svgId, '#bord3', 'mousedown', this.functionbor.bind(this))
    this.mtgAppLecteur.addEventListener(this.svgId, '#bord4', 'mousedown', this.functionbor.bind(this))
    if (this.txtF !== 'FigEditor') {
      this.regle(false)
      this.equerre(false)
      this.rapporteur(false)
      this.compas(false, false, false)
      this.renvoieCrayon(false)
      this.mtgAppLecteur.setPointPosition(this.svgId, this.idImCrayPt, 1500, 42, false)
    } else {
      this.mtgAppLecteur.addCallBackToSVGListener(this.svgId, 'mousedown', (ev) => { this.funcDown(ev) })
    }
    this.mtgAppLecteur.updateFigure(this.svgId)

    setTimeout(() => {
      if (this.txtF !== 'FigEditor') {
        const pt = this.getPointPosition2(this.svgId, '#ptcentrot')
        this.mtgAppLecteur.setPointPosition(this.svgId, '#ptbougrot', pt.x, pt.y - 50, false)
      }
      this.recentre(false, false, false, true)
      this.nomDebPoint = []
      this.unitebase = this.unite
      let a
      this.listco = []
      for (a = 0; a < this.progfig.length; a++) {
        if (this.progfig[a].type === 'zone') {
          this.ajzone(this.progfig[a])
        }
        if (this.progfig[a].type === 'zoneseg') {
          this.ajzoneSeg(this.progfig[a])
        }
        if (this.progfig[a].type === 'point') {
          this.nomDebPoint.push(this.recupe(this.progfig[a].nom))
          this.ajpoint(this.recupe(this.progfig[a].nom), this.progfig[a])
        }
        if (this.progfig[a].type === 'segment') {
          this.ajseg(this.progfig[a])
          const nnom = this.recupe(this.progfig[a].nom)
          if (nnom) this.nomDebSeg.push(nnom)
          this.nbseg++
        }
        if (this.progfig[a].type === 'cercle' || this.progfig[a].type === 'arc') {
          this.ajcerc(this.progfig[a])
        }
        if (this.progfig[a].type === 'droite') {
          if (this.progfig[a].spe !== undefined) {
            this.ajdrspe(this.progfig[a])
          } else {
            this.ajdr(this.progfig[a])
          }
        }
        if (this.progfig[a].type === 'demi-droite') {
          this.ajdremi(this.progfig[a])
        }
      }
      // gere zoom et centre
      if (this.yaaacentre) {
        this.bary = { x: 0, y: 0 }
        for (let i = 0; i < this.listePointPris.length; i++) {
          const { x, y } = this.getPointPosition2(this.svgId, this.listePointPris[i].point)
          this.bary.x += x
          this.bary.y += y
        }
        if (this.listePointPris.length === 0) {
          this.bary = { x: this.width / 2, y: this.height / 2 }
        } else {
          this.bary.x = this.bary.x / this.listePointPris.length
          this.bary.y = this.bary.y / this.listePointPris.length
        }
        this.recentre(true, true, false)
      }
      this.mtgAppLecteur.calculate(this.svgId)

      if (this.yaaaarot) {
        this.bary = { x: 0, y: 0 }
        for (let i = 0; i < this.listePointPris.length; i++) {
          const { x, y } = this.getPointPosition2(this.svgId, this.listePointPris[i].point)
          this.bary.x += x
          this.bary.y += y
        }
        this.bary.x = this.bary.x / this.listePointPris.length
        this.bary.y = this.bary.y / this.listePointPris.length
        if (!this.yaaacentre) this.bary = { x: this.lXcentre, y: this.lYcentre }
        this.rottout(true, true, false)
      }
      this.mtgAppLecteur.calculate(this.svgId)

      if (this.yaaaazom) {
        this.bary = { x: 0, y: 0 }
        for (let i = 0; i < this.listePointPris.length; i++) {
          const { x, y } = this.getPointPosition2(this.svgId, this.listePointPris[i].point)
          this.bary.x += x
          this.bary.y += y
        }
        this.bary.x = this.bary.x / this.listePointPris.length
        this.bary.y = this.bary.y / this.listePointPris.length
        if (!this.yaaacentre) this.bary = { x: this.lXcentre, y: this.lYcentre }
        this.zoomtout()
      }
      this.ConsPtnb = this.listePointPris.length
      this.ConsSegnb = this.listeSegPris.length
      this.ConsArcnb = this.listeArcPris.length
      this.ConsDtenb = this.listeDtePris.length
      this.ConsDemiDnb = this.listeDemiDPris.length
      if (this.limseg !== undefined) this.nbseg = this.limseg
      if (this.limdte !== undefined) this.nbdte = this.limdte
      if (this.limddte !== undefined) this.nbddte = this.limddte
      if (this.limarc !== undefined) this.nbarc = this.limarc
      if (this.txtF === 'FigLibre') {
        this.faiszomm(true)
        this.faiszomm(true)
        this.faiszomm(true)
        this.recentre()
      }
      if (this.txtF === 'FigEditor') {
        this.viregommEtrot = () => {}
        this.faiszomm(false)
        this.faiszomm(false)
        this.faiszomm(false)
        this.faiszomm(false)
        this.faiszomm(false)
        this.faiszomm(false)
        this.faiszomm(false)
      }
      this.consoleDev('v3')
      this.mtgAppLecteur.updateFigure(this.svgId)
    }, 100)
  }

  ajzone (ki) {
    const apush = j3pClone(ki)
    if (this.txtF === 'FigEditor') {
      apush.tags = this.makeZone(ki)
    }
    this.listeZone.push(apush)
  }

  ajzoneSeg (ki) {
    const apush = j3pClone(ki)
    if (this.txtF === 'FigEditor') {
      apush.tags = this.makeZoneSeg(ki)
    }
    this.listeZone.push(apush)
  }

  makeZone (ki) {
    const zu = {}
    zu.centre = this.newTag()
    zu.point = this.newTag()
    zu.cercle = this.newTag()
    zu.surf = this.newTag()
    zu.texte = this.newTag()
    const xc = Number(ki.c.x)
    const yc = Number(ki.c.y)
    const xp = Number(ki.p.x)
    const yp = Number(ki.p.y)
    this.mtgAppLecteur.addFreePoint({
      tag: zu.centre,
      name: zu.centre,
      color: '#00F',
      pointStyle: 'biground',
      x: xc,
      y: yc,
      hiddenName: true,
      hidden: true
    })
    this.mtgAppLecteur.addFreePoint({
      tag: zu.point,
      name: zu.point,
      color: '#00F',
      pointStyle: 'biground',
      x: xp,
      y: yp,
      hiddenName: true,
      hidden: true
    })
    this.mtgAppLecteur.addCircleOA({
      o: zu.centre,
      a: zu.point,
      tag: zu.cercle
    })
    this.mtgAppLecteur.addSurfaceCircle({
      c: zu.cercle,
      tag: zu.surf,
      color: '#0000FF',
      opacity: 0.2
    })
    this.mtgAppLecteur.addLinkedText({
      a: zu.centre,
      tag: zu.texte,
      hAlign: 'center',
      vAlign: 'top',
      color: '#00F',
      text: ki.nom
    })
    // this.mtgAppLecteur.reclassBefore({ elt: zu.point, elt2: 'unit2' })
    // this.mtgAppLecteur.reclassMin({ elt: zu.centre, elt2: 'unit2' })
    // this.mtgAppLecteur.reclassMin({ elt: zu.surf, elt2: 'unit2' })
    // this.mtgAppLecteur.reclassMin({ elt: zu.texte, elt2: 'unit2' })
    return zu
  }

  makeZoneSeg (ki) {
    const zu = {}
    zu.centre = this.newTag()
    zu.point = this.newTag()
    zu.segment = this.newTag()
    zu.mid = this.newTag()
    zu.texte = this.newTag()
    const xc = Number(ki.c.x)
    const yc = Number(ki.c.y)
    const xp = Number(ki.p.x)
    const yp = Number(ki.p.y)
    this.mtgAppLecteur.addFreePoint({
      tag: zu.centre,
      name: zu.centre,
      color: '#00F',
      pointStyle: 'biground',
      x: xc,
      y: yc,
      hiddenName: true,
      hidden: true
    })
    this.mtgAppLecteur.addFreePoint({
      tag: zu.point,
      name: zu.point,
      color: '#00F',
      pointStyle: 'biground',
      x: xp,
      y: yp,
      hiddenName: true,
      hidden: true
    })
    this.mtgAppLecteur.addSegment({
      a: zu.centre,
      b: zu.point,
      tag: zu.segment
    })
    this.mtgAppLecteur.addMidpoint({
      a: zu.centre,
      b: zu.point,
      tag: zu.mid,
      hidden: true
    })
    this.mtgAppLecteur.addLinkedText({
      a: zu.mid,
      tag: zu.texte,
      hAlign: 'center',
      vAlign: 'top',
      color: '#00F',
      text: ki.nom
    })
    // this.mtgAppLecteur.reclassBefore({ elt: zu.point, elt2: 'unit2' })
    // this.mtgAppLecteur.reclassMin({ elt: zu.centre, elt2: 'unit2' })
    // this.mtgAppLecteur.reclassMin({ elt: zu.surf, elt2: 'unit2' })
    // this.mtgAppLecteur.reclassMin({ elt: zu.texte, elt2: 'unit2' })
    return zu
  }

  makePourVoir (k) {
    const aret = function () {
      this.bloqueEventm = true
      this.setColor2(this.svgId, k, 255, 0, 0, false)
      this.setVisible2(this.svgId, k, true, false)
      this.setLineStyle2(this.svgId, k, 1, 5, false)
      this.mtgAppLecteur.updateFigure(this.svgId)
    }
    return aret.bind(this)
  }

  grossitout (b) {
    this.degros = true
    for (let i = (this.txtF === 'FigLibre') ? 0 : this.ConsSegnb; i < this.listeSegPris.length; i++) {
      this.groSS(this.listeSegPris[i].ligne, b)
    }
    for (let i = (this.txtF === 'FigLibre') ? 0 : this.ConsDtenb; i < this.listeDtePris.length; i++) {
      this.groSS(this.listeDtePris[i].ligne, b)
    }
    for (let i = (this.txtF === 'FigLibre') ? 0 : this.ConsDemiDnb; i < this.listeDemiDPris.length; i++) {
      this.groSS(this.listeDemiDPris[i].ligne, b)
    }
    for (let i = (this.txtF === 'FigLibre') ? 0 : this.ConsArcnb; i < this.listeArcPris.length; i++) {
      for (let j = 13; j < 25; j++) {
        this.groSS(this.listeArcPris[i]['arc' + (j - 12)], b)
      }
    }
    for (let i = (this.txtF === 'FigLibre') ? 0 : this.ConsPtnb; i < this.listePointPris.length; i++) {
      this.groSSpoint(this.listePointPris[i], b)
    }
  }

  groSS (k, b) {
    if (b !== false) {
      this.setColor2(this.svgId, k, 255, 0, 0, true)
      this.setLineStyle2(this.svgId, k, 0, 7, true)
    } else {
      this.setColor2(this.svgId, k, 100, 100, 100, true)
      this.setLineStyle2(this.svgId, k, 0, 2, true)
    }
  }

  groSSpoint (k, b) {
    if (b !== false) {
      this.setColor2(this.svgId, k.point, 255, 0, 0, true)
      this.setVisible2(this.svgId, k.surf, true, true)
    } else {
      this.setColor2(this.svgId, k.point, k.color[0], k.color[1], k.color[2], true)
      this.setVisible2(this.svgId, k.surf, true, true)
    }
  }

  retrouvePoint (ki) {
    for (let i = 0; i < this.listePointPris.length; i++) {
      if (this.listePointPris[i].nom === ki) return this.listePointPris[i].point
    }
    if (this.ptCo) {
      for (let i = 0; i < this.ptCo.length; i++) {
        if (this.ptCo[i].nom === ki) return this.ptCo[i].html.point
      }
    }
  }

  retrouveDroite (ki) {
    let sansdollar = ki
    if (sansdollar) {
      while (sansdollar.indexOf('$') !== -1) {
        sansdollar = sansdollar.replace('$', '')
      }
      for (let i = 0; i < this.listeDtePris.length; i++) {
        if (this.listeDtePris[i].nom === ki || this.listeDtePris[i].nom === sansdollar) return this.listeDtePris[i].ligne
      }
      for (let i = 0; i < this.listeDemiDPris.length; i++) {
        if (this.listeDemiDPris[i].nom === ki) return this.listeDemiDPris[i].ligne
      }
      for (let i = 0; i < this.listeSegPris.length; i++) {
        if (this.listeSegPris[i].nom === ki) return this.listeSegPris[i].ligne
      }

      if (this.dteco) {
        for (let i = 0; i < this.dteco.length; i++) {
          if (this.dteco[i].nom === ki || this.dteco[i].nom === sansdollar) return this.dteco[i].html.ligne
        }
      }
      if (this.demiDco) {
        for (let i = 0; i < this.demiDco.length; i++) {
          if (this.demiDco[i].nom === ki) return this.demiDco[i].html.ligne
        }
      }
      if (this.segCo) {
        for (let i = 0; i < this.segCo.length; i++) {
          if (this.segCo[i].nom === ki) return this.segCo[i].html.ligne
        }
      }
    }
  }

  retrouvePointsDroite (ki) {
    for (let i = 0; i < this.listeDtePris.length; i++) {
      if (this.listeDtePris[i].nom === ki) return { p1: this.listeDtePris[i].point1, p2: this.listeDtePris[i].point2 }
    }
    for (let i = 0; i < this.listeDemiDPris.length; i++) {
      if (this.listeDemiDPris[i].nom === ki) return { p1: this.listeDemiDPris[i].point1, p2: this.listeDemiDPris[i].point2 }
    }
    for (let i = 0; i < this.listeSegPris.length; i++) {
      if (this.listeSegPris[i].nom) {
        const nomcomp = this.listeSegPris[i].nom.replace('[', '').replace(']', '')
        if (ki.indexOf(nomcomp) !== -1) return { p1: this.listeSegPris[i].point1, p2: this.listeSegPris[i].point2 }
      }
    }

    if (this.dteco) {
      for (let i = 0; i < this.dteco.length; i++) {
        if (this.dteco[i].nom === ki) return { p1: this.dteco[i].html.point1, p2: this.dteco[i].html.point2 }
      }
    }
    if (this.demiDco) {
      for (let i = 0; i < this.demiDco.length; i++) {
        if (this.demiDco[i].nom === ki) return { p1: this.demiDco[i].html.point1, p2: this.demiDco[i].html.point2 }
      }
    }
    if (this.segCo) {
      for (let i = 0; i < this.segCo.length; i++) {
        if (this.segCo[i].nom === ki) return { p1: this.segCo[i].html.point1, p2: this.segCo[i].html.point2 }
      }
    }
  }

  retrouveSegOb (ki) {
    if (ki) {
      while (ki.indexOf('$') !== -1) {
        ki = ki.replace('$', '')
      }
      for (let i = 0; i < this.listeSegPris.length; i++) {
        if (this.listeSegPris[i].nom === ki) return this.listeSegPris[i]
      }
    }
  }

  retrouveDemidOb (ki) {
    if (ki) {
      while (ki.indexOf('$') !== -1) {
        ki = ki.replace('$', '')
      }
      for (let i = 0; i < this.listeDemiDPris.length; i++) {
        if (this.listeDemiDPris[i].nom === ki) return this.listeDemiDPris[i]
      }
    }
  }

  retrouveArcOb (ki) {
    if (ki) {
      while (ki.indexOf('$') !== -1) {
        ki = ki.replace('$', '')
      }
      for (let i = 0; i < this.listeArcPris.length; i++) {
        if (this.listeArcPris[i].nom === ki) return this.listeArcPris[i]
      }
    }
  }

  retrouveArc (ki) {
    if (ki) {
      while (ki.indexOf('$') !== -1) {
        ki = ki.replace('$', '')
      }
      for (let i = 0; i < this.listeArcPris.length; i++) {
        let acomp = this.listeArcPris[i].nom
        if (acomp) {
          while (acomp.indexOf('$') !== -1) {
            acomp = acomp.replace('$', '')
          }
          if (acomp === ki) {
            const aret = []
            for (let j = 13; j < 25; j++) {
              aret.push(this.listeArcPris[i]['arc' + (j - 12)])
            }
            return aret
          }
        }
      }
      if (this.arcco) {
        for (let i = 0; i < this.arcco.length; i++) {
          let acomp = this.arcco[i].nom
          if (acomp) {
            while (acomp.indexOf('$') !== -1) {
              acomp = acomp.replace('$', '')
            }
            if (acomp === ki) {
              const aret = []
              aret.push(this.arcco[i].html.arc1)
              return aret
            }
          }
        }
      }
    }
  }

  retrouveTruc (ki) {
    let aret = this.retrouveDroite(ki)
    if (aret) return { type: 'line', tab: [aret] }
    aret = this.retrouveArc(ki)
    if (aret) return { type: 'cerc', tab: aret }
  }

  retrouveTag (ki) {
    let aret = this.retrouveDroite(ki)
    if (aret) return [aret]
    aret = this.retrouveArc(ki)
    if (aret) return aret
    aret = this.retrouvePoint(ki)
    if (aret) return [aret]
  }

  placeNomPoint (tagCentre, rayon, placeGarde, pointNom, foc) {
    const corrigeRap = (!foc) ? 1 : 1.948717100000019
    const ob = this.newTag()
    this.mtgAppLecteur.addCircleOr({
      o: tagCentre,
      r: rayon / this.unite,
      tag: ob
    })
    const tagPointX = this.newTag()
    const coA = this.mtgAppLecteur.getPointPosition({ a: tagCentre })
    this.mtgAppLecteur.addFreePoint({
      tag: tagPointX,
      name: tagPointX,
      x: coA.x + 10,
      y: coA.y,
      color: 'red'
    })
    const tagLineX = this.newTag()
    this.mtgAppLecteur.addLineAB({
      tag: tagLineX,
      a: tagCentre,
      b: tagPointX,
      color: 'red'
    })
    const tagInt = this.newTag()
    this.mtgAppLecteur.addIntLineCircle({
      tag: tagInt,
      name: tagInt,
      d: tagLineX,
      c: ob,
      color: 'red'
    })
    const coX = this.mtgAppLecteur.getPointPosition({ a: tagInt })
    const ray = this.dist(coX, coA)
    const tagPointB = this.newTag()
    this.mtgAppLecteur.addFreePoint({
      tag: tagPointB,
      name: tagPointB,
      x: coA.x + ray * placeGarde.rapx,
      y: coA.y + ray * placeGarde.rapy,
      color: 'green',
      pointStyle: 'bigmult'
    })
    const tagPetiC = this.newTag()
    this.mtgAppLecteur.addCircleOr({
      o: tagPointB,
      r: placeGarde.dist / this.unite / corrigeRap,
      color: 'green',
      tag: tagPetiC
    })
    const tagIntF1 = this.newTag()
    const tagIntF2 = this.newTag()
    const tagDroiteF = this.newTag()
    this.mtgAppLecteur.addLineAB({
      tag: tagDroiteF,
      a: tagCentre,
      b: tagPointB,
      color: 'blue'
    })
    this.mtgAppLecteur.addIntLineCircle({
      tag: tagIntF1,
      name: tagIntF1,
      tag2: tagIntF2,
      name2: tagIntF2,
      d: tagDroiteF,
      c: tagPetiC
    })
    let tagProch, tagLoin
    const aCons1 = this.mtgAppLecteur.getPointPosition({ a: tagIntF1 })
    const aCons2 = this.mtgAppLecteur.getPointPosition({ a: tagIntF2 })
    if (this.dist(aCons2, coA) > this.dist(aCons1, coA)) {
      tagProch = aCons1
      tagLoin = aCons2
    } else {
      tagProch = aCons2
      tagLoin = aCons1
    }
    const cooG = (placeGarde.procha) ? tagProch : tagLoin
    this.mtgAppLecteur.deleteElt({ elt: ob })
    this.mtgAppLecteur.deleteElt({ elt: tagPointX })
    this.mtgAppLecteur.deleteElt({ elt: tagPointB })
    this.mtgAppLecteur.setPointPosition(this.svgId, '#' + pointNom, cooG.x, cooG.y, true)
  }

  ajcerc (f) {
    const zu = this.creeNewArc((f.color) ? j3pClone(f.color) : [0, 0, 0])
    zu.nom = f.nom
    zu.viz = f.viz !== false && f.visible !== false
    this.listeArcPris.push(zu)
    if (Array.isArray(f.coo)) {
      this.mtgAppLecteur.setPointPosition(this.svgId, zu.centre, Number(f.coo[0].x) + 100, Number(f.coo[0].y) - 170, false)
      for (let i = 1; i < 13; i++) {
        this.mtgAppLecteur.setPointPosition(this.svgId, zu['p' + i], Number(f.coo[i].x) + 100, Number(f.coo[i].y) - 170, false)
      }
    } else {
      let nompasse, r
      const nomcentre = this.recupe(f.centre)
      if (f.def === 'passe') {
        nompasse = this.recupe(f.par)
      }
      let pt2
      const pt1 = this.getPointPosition2(this.svgId, this.retrouvePoint(nomcentre))
      if (f.def === 'passe') {
        pt2 = this.getPointPosition2(this.svgId, this.retrouvePoint(nompasse))
        r = this.dist(pt1, pt2)
      } else {
        r = parseFloat(this.calculFormule(f.ray, 'valeur', 1, 100)) * this.unite
        nompasse = 'Q1'
        pt2 = this.getPointPosition2(this.svgId, nompasse)
      }
      this.mtgAppLecteur.setPointPosition(this.svgId, zu.centre, pt1.x, pt1.y, false)
      for (let i = 1; i < 13; i++) {
        this.mtgAppLecteur.setPointPosition(this.svgId, zu['p' + i], pt1.x + r * Math.cos(i * 30 * Math.PI / 180), pt1.y + r * Math.sin(i * 30 * Math.PI / 180), false)
      }
      if (f.affnom !== undefined) {
        this.nomspris.push(f.nom)
        this.creeNewNom(zu)
        if (f.placeGarde) {
          this.placeNomPoint(this.retrouvePoint(nomcentre), r, f.placeGarde, zu.pointNom)
        } else {
          let fax = f.plx
          if (this.txtF === 'FigEditor') {
            fax -= 200
          }
          this.mtgAppLecteur.setPointPosition(this.svgId, '#' + zu.pointNom, fax + 100, f.ply - 170, true)
        }
        this.setColor2(this.svgId, zu.aff, f.affnom, 0, 0, 0, true)
        this.setVisible2(this.svgId, zu.aff, zu.viz)
      }
    }
    for (let i = 13; i < 25; i++) {
      this.setVisible2(this.svgId, zu['arc' + (i - 12)], zu.viz, true)
      this.setLineStyle2(this.svgId, zu['arc' + (i - 12)], (f.pointille) ? 2 : 0, 2, true)
      this.setColor2(this.svgId, zu['arc' + (i - 12)], zu.color[0], zu.color[1], zu.color[2], true)
    }
    if (this.txtF === 'FigLibre') {
      const tyty = (f.coo[1].x === f.coo[12].x && f.coo[1].y === f.coo[12].y) ? 'cercle' : 'arc'
      this.faitFuncGomm(zu, tyty)
    }
  }

  penteDe (tag) {
    const coPtCa3 = this.getPointPosition2(this.svgId, 'ptCa3')
    const coPtCa2 = this.getPointPosition2(this.svgId, 'ptCa2')
    // cherche un point sur tag
    let gtrouvUnPoint = false
    let gtrouvUnPoint2 = false
    const tagLinkPoint = this.newTag()
    const tagLinkPoint2 = this.newTag()
    let tagEncours = tagLinkPoint
    for (let i = 1; i < 100; i++) {
      const cooLink = { x: coPtCa2.x + i * (coPtCa3.x - coPtCa2.x) / 100, y: coPtCa2.y + i * (coPtCa3.y - coPtCa2.y) / 100 }
      try {
        this.mtgAppLecteur.addLinkedPointLine({
          d: tag,
          x: cooLink.x,
          y: cooLink.y,
          name: tagEncours,
          tag: tagEncours
        })
        if (gtrouvUnPoint) {
          gtrouvUnPoint2 = true
        } else {
          gtrouvUnPoint = true
          tagEncours = tagLinkPoint2
        }
      } catch (e) {

      }
      if (gtrouvUnPoint2) break
    }
    if (!gtrouvUnPoint2) return 0
    const cooLink = this.getPointPosition2(this.svgId, tagLinkPoint)
    const cooLink2 = this.getPointPosition2(this.svgId, tagLinkPoint2)
    // const AB = { x: 10, y: 0 }
    // const AC = { x: cooLink2.x - cooLink.x, y: cooLink2.y - cooLink.y }
    this.mtgAppLecteur.deleteElt({ elt: tagLinkPoint })
    this.mtgAppLecteur.deleteElt({ elt: tagLinkPoint2 })
    if (cooLink2.x === cooLink.x) return 90
    return 180 / Math.PI * Math.atan((cooLink2.x - cooLink.x) / (cooLink2.y - cooLink.y))
  }

  calculFormule (f, g, a1, a2) {
    let haz
    for (let j = this.nomprisProg.length - 1; j > -1; j--) {
      const regex = new RegExp(this.nomprisProg[j].ki, 'g')
      f = f.replace(regex, this.nomprisProg[j].com)
    }
    if (f.includes('haz')) {
      haz = j3pGetRandomInt(a1, a2)
      // eslint-disable-next-line prefer-regex-literals
      const regex = new RegExp('haz', 'g')
      f = f.replace(regex, haz)
    }
    if (f.includes('à')) {
      haz = j3pGetRandomInt(0, 100) / 100
      // eslint-disable-next-line prefer-regex-literals
      const regex = new RegExp('à', 'g')
      f = f.replace(regex, haz)
    }
    if (g) {
      this.faisLongCo()
      for (let j = 0; j < this.leslongCo.length; j++) {
        let regex = new RegExp(this.leslongCo[j].nom1, 'g')
        f = f.replace(regex, this.leslongCo[j].long)
        regex = new RegExp(this.leslongCo[j].nom2, 'g')
        f = f.replace(regex, this.leslongCo[j].long)
      }
      if (this.leslongCoConst) {
        for (let j = 0; j < this.leslongCoConst.length; j++) {
          let regex = new RegExp(this.leslongCoConst[j].nom1, 'g')
          f = f.replace(regex, this.leslongCoConst[j].long)
          regex = new RegExp(this.leslongCoConst[j].nom2, 'g')
          f = f.replace(regex, this.leslongCoConst[j].long)
        }
      }
      while (f.includes('\\times')) {
        f = f.replace('\\times', '*')
      }
      if (f.includes('\\frac{')) {
        f = f.replace('\\frac{', '(').replace('}{', ')/(').replace('}', ')').replace(/\$/g, '')
      }
      f = this.calcule(f)
      if (f.includes('/')) {
        f = String(j3pArrondi(parseInt(f.substring(0, f.indexOf('/'))) / parseInt(f.substring(f.indexOf('/') + 1)), 1))
      }
    }
    return f
  }

  zoomizeX (ki) {
    const pt = this.getPointPosition2(this.svgId, ki)
    const newx = {}
    const xM = pt.x - this.bary.x
    const yM = pt.y - this.bary.y
    newx.x = xM * this.modiggg + this.bary.x
    newx.y = yM * this.modiggg + this.bary.y
    this.mtgAppLecteur.setPointPosition(this.svgId, ki, newx.x, newx.y, false)
  }

  zoomtout () {
    for (let i = 0; i < this.listeSegPris.length; i++) {
      this.zoomizeX(this.listeSegPris[i].point1)
      this.zoomizeX(this.listeSegPris[i].point2)
      if (this.listeSegPris[i].pointNom) this.zoomizeX(this.listeSegPris[i].pointNom)
    }
    for (let i = 0; i < this.listeDtePris.length; i++) {
      this.zoomizeX(this.listeDtePris[i].point1)
      this.zoomizeX(this.listeDtePris[i].point2)
      if (this.listeDtePris[i].pointNom) this.zoomizeX(this.listeDtePris[i].pointNom)
    }
    for (let i = 0; i < this.listeDemiDPris.length; i++) {
      this.zoomizeX(this.listeDemiDPris[i].point1)
      this.zoomizeX(this.listeDemiDPris[i].point2)
      if (this.listeDemiDPris[i].pointNom) this.zoomizeX(this.listeDemiDPris[i].pointNom)
    }
    for (let i = 0; i < this.listeArcPris.length; i++) {
      this.zoomizeX(this.listeArcPris[i].centre)
      this.zoomizeX(this.listeArcPris[i].p1)
      this.zoomizeX(this.listeArcPris[i].p2)
      this.zoomizeX(this.listeArcPris[i].p3)
      this.zoomizeX(this.listeArcPris[i].p4)
      this.zoomizeX(this.listeArcPris[i].p5)
      this.zoomizeX(this.listeArcPris[i].p6)
      this.zoomizeX(this.listeArcPris[i].p7)
      this.zoomizeX(this.listeArcPris[i].p8)
      this.zoomizeX(this.listeArcPris[i].p9)
      this.zoomizeX(this.listeArcPris[i].p10)
      this.zoomizeX(this.listeArcPris[i].p11)
      this.zoomizeX(this.listeArcPris[i].p12)
      if (this.listeArcPris[i].pointNom) this.zoomizeX(this.listeArcPris[i].pointNom)
    }
    for (let i = 0; i < this.listePointPris.length; i++) {
      this.zoomizeX(this.listePointPris[i].point)
    }
  }

  newTag () {
    this.listeTag++
    return 'T' + this.listeTag + 't'
  }

  newTagCalc () {
    this.listeTag++
    let aret = ''
    for (let i = 0; i < this.listeTag; i++) {
      aret += 'A'
    }
    return aret
  }

  creeNewPoint (n, m, color) {
    const zu = {}
    zu.point = this.newTag()
    zu.cercle = this.newTag()
    zu.suiv = this.newTag()
    zu.surf = this.newTag()
    zu.aff = this.newTag()
    zu.nom = n
    zu.color = color
    this.mtgAppLecteur.addFreePoint({
      tag: zu.point,
      name: zu.point,
      color: this.transColor(color),
      pointStyle: 'bigmult',
      x: 100,
      y: 100,
      hiddenName: true
    })
    this.mtgAppLecteur.fixPoint({ a: zu.point })
    this.mtgAppLecteur.addCircleOr({
      o: zu.point,
      r: 'Z9Z10',
      tag: zu.cercle,
      hidden: true,
      opacity: 0
    })
    this.mtgAppLecteur.addLinkedPointCircle({
      absCoord: true,
      tag: zu.suiv,
      name: zu.suiv,
      c: zu.cercle,
      color: 'white',
      pointStyle: 'pixel',
      x: 110,
      y: 110,
      hiddenName: true
    })
    this.mtgAppLecteur.addLinkedText({
      a: zu.suiv,
      tag: zu.aff,
      hAlign: 'center',
      vAlign: 'middle',
      color: this.transColor(color),
      text: m
    })
    this.mtgAppLecteur.addSurfaceCircle({
      c: zu.cercle,
      tag: zu.surf,
      color: '#FF0000',
      hidden: true
    })
    this.mtgAppLecteur.reclassBefore({ elt: zu.point, elt2: 'unit2' })
    this.mtgAppLecteur.reclassMin({ elt: zu.cercle, elt2: 'unit2' })
    this.mtgAppLecteur.reclassMin({ elt: zu.suiv, elt2: 'unit2' })
    this.mtgAppLecteur.reclassMin({ elt: zu.aff, elt2: 'unit2' })
    this.mtgAppLecteur.reclassMin({ elt: zu.surf, elt2: 'unit2' })
    return zu
  }

  placeOkPoint (p, force) {
    if (p.x === undefined || p.y === undefined) return false
    this.mtgAppLecteur.addFreePoint({
      name: 'aVireZ',
      tag: 'aVireZ',
      x: p.x,
      y: p.y
    })
    this.mtgAppLecteur.addLinePerp({
      d: 'bordn1',
      a: 'aVireZ',
      tag: 'Laperp1'
    })
    this.mtgAppLecteur.addIntLineLine({
      d: 'Laperp1',
      d2: 'bordn1',
      name: 'intint1',
      tag: 'intint1'
    })
    const cooA = this.getPointPosition2(this.svgId, 'intint1')
    if (cooA.x === undefined) return false
    this.mtgAppLecteur.addLinePerp({
      d: 'bordn2',
      a: 'aVireZ',
      tag: 'Laperp2'
    })
    this.mtgAppLecteur.addIntLineLine({
      d: 'Laperp2',
      d2: 'bordn2',
      name: 'intint2',
      tag: 'intint2'
    })
    const cooB = this.getPointPosition2(this.svgId, 'intint2')
    this.mtgAppLecteur.deleteElt({ elt: 'aVireZ' })
    if (cooB.x === undefined) return false
    if (cooA.x === undefined) return false
    if (force) return true
    for (let i = 0; i < this.listePointPris.length; i++) {
      const coop = this.getPointPosition2(this.svgId, this.listePointPris[i].point)
      if (this.dist(coop, p) < this.unite) return false
    }
    return true
  }

  retrouveZone (n) {
    for (let i = 0; i < this.listeZone.length; i++) {
      if (this.listeZone[i].nom === n) return this.listeZone[i]
    }
  }

  modifCoo (p) {
    const decx = p.x - 200
    const decy = p.y - 230
    return { x: 300 + 2 * decx, y: 400 + decy * 27 / 14 }
  }

  ajpoint (n, a) {
    const zu = this.creeNewPoint(n, n, (a.color) ? j3pClone(a.color) : [0, 150, 0])
    zu.viz = (a.viz !== false && a.visible !== false)
    this.listePointPris.push(zu)
    this.nomspris.push(n)
    this.setVisible2(this.svgId, zu.point, zu.viz, true)
    this.setVisible2(this.svgId, zu.suiv, zu.viz, true)
    this.setVisible2(this.svgId, zu.aff, zu.viz, true)
    // calcul x et y
    if (a.sur) {
      switch (this.typeDe(this.recupe(a.sur))) {
        case 'Zone': {
          const lazone = this.retrouveZone(a.dep[0])
          const baseC = this.modifCoo(lazone.c)
          const baseP = this.modifCoo(lazone.p)
          a.coef = (a.fix || this.txtF === 'FigEditor') ? (a.coef || j3pGetRandomInt(0, 100)) : j3pGetRandomInt(0, 100)
          if (lazone.type === 'zone') {
            a.cox = String(baseC.x + (this.dist(baseC, baseP) * a.coef / 100) * Math.cos(a.coef * Math.PI / 50))
            a.coy = String(baseC.y + (this.dist(baseC, baseP) * a.coef / 100) * Math.sin(a.coef * Math.PI / 50))
          } else {
            a.cox = String(baseC.x + a.coef / 100 * (baseP.x - baseC.x))
            a.coy = String(baseC.y + a.coef / 100 * (baseP.y - baseC.y))
          }
        }
          break
        case 'segment': {
          const leseg = this.retrouveSegOb(this.recupe(a.sur))
          const baseC = this.getPointPosition2(this.svgId, leseg.point1)
          const baseP = this.getPointPosition2(this.svgId, leseg.point2)
          let dej = false
          let nbpasse = 0
          do {
            nbpasse++
            a.coef = (!dej) ? (a.coef = (a.fix) ? (a.coef || j3pGetRandomInt(0, 100)) : j3pGetRandomInt(0, 100)) : j3pGetRandomInt(0, 100)
            a.cox = String(baseC.x + a.coef / 100 * (baseP.x - baseC.x) - 100)
            a.coy = String(baseC.y + a.coef / 100 * (baseP.y - baseC.y) + 170)
            dej = true
          } while (!this.placeOkPoint({ x: Number(a.cox), y: Number(a.coy) } && nbpasse < 100))
          if (this.txtF === 'FigEditor') {
            a.cox = String(baseC.x + a.coef / 100 * (baseP.x - baseC.x) + 100)
          }
        }
          break
        case 'droite': {
          const leseg = this.retrouveDroite(a.sur)
          const nt1 = this.newTag()
          const nt2 = this.newTag()
          const nt3 = this.newTag()
          const nt4 = this.newTag()
          this.mtgAppLecteur.addIntLineLine({
            tag: nt1,
            name: nt1,
            d2: leseg,
            d: 'segbord1'
          })
          this.mtgAppLecteur.addIntLineLine({
            tag: nt2,
            name: nt2,
            d2: leseg,
            d: 'segbord2'
          })
          this.mtgAppLecteur.addIntLineLine({
            tag: nt3,
            name: nt3,
            d2: leseg,
            d: 'segbord3'
          })
          this.mtgAppLecteur.addIntLineLine({
            tag: nt4,
            name: nt4,
            d2: leseg,
            d: 'segbord4'
          })
          const coo1 = this.getPointPosition2(this.svgId, nt1)
          const coo2 = this.getPointPosition2(this.svgId, nt2)
          const coo3 = this.getPointPosition2(this.svgId, nt3)
          const coo4 = this.getPointPosition2(this.svgId, nt4)
          const litOk = []
          if (coo1.x !== undefined) litOk.push(coo1)
          if (coo2.x !== undefined) litOk.push(coo2)
          if (coo3.x !== undefined) litOk.push(coo3)
          if (coo4.x !== undefined) litOk.push(coo4)
          const baseC = litOk[0]
          const baseP = litOk[1]
          this.mtgAppLecteur.deleteElt({ elt: nt1 })
          this.mtgAppLecteur.deleteElt({ elt: nt2 })
          this.mtgAppLecteur.deleteElt({ elt: nt3 })
          this.mtgAppLecteur.deleteElt({ elt: nt4 })
          a.coef = (a.fix || this.txtF === 'FigEditor') ? (a.coef || j3pGetRandomInt(0, 100)) : j3pGetRandomInt(0, 100)
          a.cox = String(baseC.x + a.coef / 100 * (baseP.x - baseC.x) - 100)
          a.coy = String(baseC.y + a.coef / 100 * (baseP.y - baseC.y) + 170)
          if (this.txtF === 'FigEditor') {
            a.cox = String(baseC.x + a.coef / 100 * (baseP.x - baseC.x) + 100)
          }
        }
          break
        case 'demi-droite': {
          const leseg = this.retrouveDemidOb(a.sur)
          const nt1 = this.newTag()
          const nt2 = this.newTag()
          const nt3 = this.newTag()
          const nt4 = this.newTag()
          this.mtgAppLecteur.addIntLineLine({
            tag: nt1,
            name: nt1,
            d2: leseg.ligne,
            d: 'segbord1'
          })
          this.mtgAppLecteur.addIntLineLine({
            tag: nt2,
            name: nt2,
            d2: leseg.ligne,
            d: 'segbord2'
          })
          this.mtgAppLecteur.addIntLineLine({
            tag: nt3,
            name: nt3,
            d2: leseg.ligne,
            d: 'segbord3'
          })
          this.mtgAppLecteur.addIntLineLine({
            tag: nt4,
            name: nt4,
            d2: leseg.ligne,
            d: 'segbord4'
          })
          const coo1 = this.getPointPosition2(this.svgId, nt1)
          const coo2 = this.getPointPosition2(this.svgId, nt2)
          const coo3 = this.getPointPosition2(this.svgId, nt3)
          const coo4 = this.getPointPosition2(this.svgId, nt4)
          const litOk = []
          if (coo1.x !== undefined) litOk.push(coo1)
          if (coo2.x !== undefined) litOk.push(coo2)
          if (coo3.x !== undefined) litOk.push(coo3)
          if (coo4.x !== undefined) litOk.push(coo4)
          const baseC = litOk[0]
          const baseP = this.getPointPosition2(this.svgId, leseg.point1)
          this.mtgAppLecteur.deleteElt({ elt: nt1 })
          this.mtgAppLecteur.deleteElt({ elt: nt2 })
          this.mtgAppLecteur.deleteElt({ elt: nt3 })
          this.mtgAppLecteur.deleteElt({ elt: nt4 })
          a.coef = (a.fix || this.txtF === 'FigEditor') ? (a.coef || j3pGetRandomInt(0, 100)) : j3pGetRandomInt(0, 100)
          a.cox = String(baseC.x + a.coef / 100 * (baseP.x - baseC.x) - 100)
          a.coy = String(baseC.y + a.coef / 100 * (baseP.y - baseC.y) + 170)
          if (this.txtF === 'FigEditor') {
            a.cox = String(baseC.x + a.coef / 100 * (baseP.x - baseC.x) + 100)
          }
        }
          break
        case 'cercle': {
          // chope inter cercle avec 4 bords
          const leseg = this.retrouveArcOb(a.sur)
          const baseC = this.getPointPosition2(this.svgId, leseg.centre)
          const baseP = this.getPointPosition2(this.svgId, leseg.p1)
          a.coef = (a.fix || this.txtF === 'FigEditor') ? (a.coef || j3pGetRandomInt(0, 100)) : j3pGetRandomInt(0, 100)
          a.cox = String(baseC.x + (this.dist(baseC, baseP)) * Math.cos(a.coef / 50 * Math.PI) - 100)
          a.coy = String(170 + baseC.y + (this.dist(baseC, baseP)) * Math.sin(a.coef / 50 * Math.PI))
          if (this.txtF === 'FigEditor') {
            a.cox = String(baseC.x + (this.dist(baseC, baseP)) * Math.cos(a.coef / 50 * Math.PI) + 100)
          }
        }
          break
      }
    }
    if (a.ext1) {
      const baseC = this.mtgAppLecteur.getPointPosition({ a: this.retrouvePoint(this.recupe(a.ext1)) })
      const baseP = this.mtgAppLecteur.getPointPosition({ a: this.retrouvePoint(this.recupe(a.ext2)) })
      if (this.txtF === 'FigEditor') {
        a.cox = String(baseC.x + 0.5 * (baseP.x - baseC.x) + 100)
      } else {
        a.cox = String(baseC.x + 0.5 * (baseP.x - baseC.x) - 100)
      }
      a.coy = String(baseC.y + 0.5 * (baseP.y - baseC.y) + 170)
    }
    if (a.centreRot) {
      a.coef = (a.fix || this.txtF === 'FigEditor') ? (a.coef || j3pGetRandomInt(0, 100)) : j3pGetRandomInt(0, 100)
      const baseC = this.mtgAppLecteur.getPointPosition({ a: this.retrouvePoint(this.recupe(a.centreRot)) })
      const baseP = this.mtgAppLecteur.getPointPosition({ a: this.retrouvePoint(this.recupe(a.image)) })
      const angle = this.calculFormule(String(a.angle).replace(/à/g, String(a.coef)).replace(/,/g, '.'), 'valeur') * Math.PI / 180
      const xM = baseP.x - baseC.x
      const yM = baseP.y - baseC.y
      a.cox = String(xM * Math.cos(angle) + yM * Math.sin(angle) + baseC.x - 100)
      a.coy = String(-xM * Math.sin(angle) + yM * Math.cos(angle) + baseC.y + 170)
      if (this.txtF === 'FigEditor') {
        a.cox = String(xM * Math.cos(angle) + yM * Math.sin(angle) + baseC.x + 100)
      }
    }
    if (a.sur1) {
      const ob1 = this.retrouveTruc(this.recupe(a.sur1))
      const ob2 = this.retrouveTruc(this.recupe(a.sur2))
      let iob1, iob2
      for (let i = 0; i < this.progfig.length; i++) {
        if (this.progfig[i].nom === a.sur1) {
          iob1 = this.progfig[i]
        }
      }
      for (let i = 0; i < this.progfig.length; i++) {
        if (this.progfig[i].nom === a.sur2) {
          iob2 = this.progfig[i]
        }
      }
      for (let i = 0; i < ob1.tab.length; i++) {
        this.setVisible2(this.svgId, ob1.tab[i], true)
        this.setColor2(this.svgId, ob1.tab[i], 255, 0, 0, true)
      }
      for (let i = 0; i < ob2.tab.length; i++) {
        this.setVisible2(this.svgId, ob2.tab[i], true)
        this.setColor2(this.svgId, ob2.tab[i], 255, 0, 0, true)
      }
      const tagList = []
      let tagListPos = []
      switch (ob1.type) {
        case 'line':
          switch (ob2.type) {
            case 'line': {
              const intTag = this.newTag()
              this.mtgAppLecteur.addIntLineLine({
                tag: intTag,
                name: intTag,
                d2: ob1.tab[0],
                d: ob2.tab[0]
              })
              tagList.push(intTag)
            }
              break
            case 'cerc':
              for (let i = 0; i < ob2.tab.length; i++) {
                const intTag = this.newTag()
                const intTag2 = this.newTag()
                this.mtgAppLecteur.addIntLineCircle({
                  tag: intTag,
                  name: intTag,
                  tag2: intTag2,
                  name2: intTag2,
                  c: ob2.tab[i],
                  d: ob1.tab[0]
                })
                tagList.push(intTag, intTag2)
              }
              break
          }
          break
        case 'cerc':
          switch (ob2.type) {
            case 'line':
              for (let i = 0; i < ob1.tab.length; i++) {
                const intTag = this.newTag()
                const intTag2 = this.newTag()
                this.mtgAppLecteur.addIntLineCircle({
                  tag: intTag,
                  name: intTag,
                  tag2: intTag2,
                  name2: intTag2,
                  c: ob1.tab[i],
                  d: ob2.tab[0]
                })
                tagList.push(intTag, intTag2)
              }
              break
            case 'cerc':
              for (let i = 0; i < ob1.tab.length; i++) {
                for (let j = 0; j < ob2.tab.length; j++) {
                  const intTag = this.newTag()
                  const intTag2 = this.newTag()
                  this.mtgAppLecteur.addIntCircleCircle({
                    tag: intTag,
                    name: intTag,
                    tag2: intTag2,
                    name2: intTag2,
                    c: ob1.tab[i],
                    c2: ob2.tab[j]
                  })
                  tagList.push(intTag, intTag2)
                }
              }
              break
          }
          break
      }
      for (let i = 0; i < ob1.tab.length; i++) {
        this.setColor2(this.svgId, ob1.tab[i], 0, 0, 0, true)
        this.setVisible2(this.svgId, ob1.tab[i], (iob1.visible !== false))
      }
      for (let i = 0; i < ob2.tab.length; i++) {
        this.setColor2(this.svgId, ob2.tab[i], 0, 0, 0, true)
        this.setVisible2(this.svgId, ob2.tab[i], (iob2.visible !== false))
      }
      // vire les inexistants
      for (let i = tagList.length - 1; i > -1; i--) {
        const coo = this.mtgAppLecteur.getPointPosition({ a: tagList[i] })
        if (coo.x === undefined || coo.y === undefined) continue
        if (coo.cox < -30) continue
        if (coo.coy < -20) continue
        if (coo.coy > 720) continue
        if (coo.cox > 630) continue
        if (this.txtF === 'FigEditor') {
          tagListPos.push({ x: String(coo.x + 100), y: String(coo.y + 170) })
        } else {
          tagListPos.push({ x: String(coo.x - 100), y: String(coo.y + 170) })
        }
        this.mtgAppLecteur.deleteElt({ elt: tagList[i] })
      }
      if (tagListPos.length === 0) return
      tagListPos = j3pShuffle(tagListPos)
      const posBuf = tagListPos.pop()
      a.cox = posBuf.x
      a.coy = posBuf.y
    }
    let x = a.cox.replace(/ /g, '')
    let y = a.coy.replace(/ /g, '')
    x = this.recupe(x)
    y = this.recupe(y)
    for (let i = 0; i < this.listco.length; i++) {
      let regex = new RegExp(this.listco[i].n + '.x', 'g')
      x = x.replace(regex, parseFloat(this.listco[i].x))
      y = y.replace(regex, parseFloat(this.listco[i].x))
      regex = new RegExp(this.listco[i].n + '.y', 'g')
      x = x.replace(regex, parseFloat(this.listco[i].y))
      y = y.replace(regex, parseFloat(this.listco[i].y))
    }
    x = parseFloat(this.calculFormule(x, 'valeur', a.hazmin, a.hazmax))
    y = parseFloat(this.calculFormule(y, 'valeur', a.hazmin, a.hazmax))
    if (this.txtF === 'FigEditor') x -= 200
    this.mtgAppLecteur.setPointPosition(this.svgId, zu.point, x + 100, y - 170, false)
    this.listco.push({ n, x, y })
    if (this.txtF === 'FigLibre') this.faitFuncGomm(zu, 'pt')
  }

  ajseg (a) {
    const zu = this.creeNewSeg((a.color) ? j3pClone(a.color) : [0, 0, 0])
    this.listeSegPris.push(zu)
    zu.viz = a.viz !== false && a.visible !== false
    this.setVisible2(this.svgId, zu.ligne, zu.viz, true)
    this.setColor2(this.svgId, zu.ligne, zu.color[0], zu.color[1], zu.color[2], true)
    // retrouve coo 2 pt
    let pt1, pt2
    if (a.x1) {
      pt1 = { x: Number(a.x1) + 100, y: Number(a.y1) - 170 }
      pt2 = { x: Number(a.x2) + 100, y: Number(a.y2) - 170 }
    } else {
      if (a.aff) {
        const point1 = this.retrouvepoint(this.recupe(a.dep[0]))
        const point2 = this.retrouvepoint(this.recupe(a.dep[1]))
        pt1 = this.getPointPosition2(this.svgId, point1)
        pt2 = this.getPointPosition2(this.svgId, point2)
      } else {
        const point1 = this.retrouvepoint(this.recupe(a.nom)[1])
        const point2 = this.retrouvepoint(this.recupe(a.nom)[2])
        pt1 = this.getPointPosition2(this.svgId, point1)
        pt2 = this.getPointPosition2(this.svgId, point2)
      }
    }
    this.mtgAppLecteur.setPointPosition(this.svgId, zu.point1, pt2.x, pt2.y, true)
    this.mtgAppLecteur.setPointPosition(this.svgId, zu.point2, pt1.x, pt1.y, true)
    if (a.aff !== undefined || a.affnom !== undefined) {
      const laf = a.aff || a.affnom
      zu.nom = laf
      this.nomspris.push(laf)
      this.creeNewNom(zu)
      if (a.placeGarde) {
        const tagInt = this.newTag()
        const tagPerp = this.newTag()
        const tagcerc = this.newTag()
        const tagPoint = this.newTag()
        const tagPoint2 = this.newTag()
        this.mtgAppLecteur.calculate(this.svgId)
        this.mtgAppLecteur.addFreePoint({
          tag: tagInt,
          name: tagInt,
          x: pt1.x + a.placeGarde.abs * (pt2.x - pt1.x),
          y: pt1.y + a.placeGarde.abs * (pt2.y - pt1.y)
        })
        this.mtgAppLecteur.addLinePerp({
          tag: tagPerp,
          a: tagInt,
          d: zu.ligne,
          color: '#f00'
        })
        this.mtgAppLecteur.addCircleOr({
          tag: tagcerc,
          o: tagInt,
          r: a.placeGarde.dist / this.unite
        })
        this.mtgAppLecteur.addIntLineCircle({
          tag: tagPoint,
          name: tagPoint,
          tag2: tagPoint2,
          name2: tagPoint2,
          c: tagcerc,
          d: tagPerp
        })
        const toComp = (!a.placeGarde.ou12) ? tagPoint : tagPoint2
        const cooG = this.mtgAppLecteur.getPointPosition({ a: toComp })
        this.mtgAppLecteur.deleteElt({ elt: tagInt })
        this.mtgAppLecteur.setPointPosition(this.svgId, '#' + zu.pointNom, cooG.x, cooG.y, true)
      } else {
        let fax = a.plx
        if (this.txtF === 'FigEditor') {
          fax -= 200
        }
        this.mtgAppLecteur.setPointPosition(this.svgId, '#' + zu.pointNom, fax + 100, a.ply - 170, true)
      }
      this.setColor2(this.svgId, zu.aff, 0, 0, 0, true)
      this.setVisible2(this.svgId, zu.aff, zu.viz, true)
    } else {
      zu.nom = this.recupe(a.nom)
    }
    this.setLineStyle2(this.svgId, zu.ligne, (a.pointille) ? 2 : 0, 2, true)
    if (this.txtF === 'FigLibre') this.faitFuncGomm(zu, 'seg')
  }

  retrouvepoint (ki) {
    for (let i = 0; i < this.listePointPris.length; i++) {
      if (this.listePointPris[i].nom === ki) return this.listePointPris[i].point
    }
    return ''
  }

  pointilleVire () {

  }

  ajdremi (f) {
    let zu, pt1, pt2
    if (f.x1) {
      zu = this.creeNewDemiDroite((f.color) ? j3pClone(f.color) : [0, 0, 0])
      zu.viz = f.viz !== false && f.visible !== false
      this.listeDemiDPris.push(zu)
      pt1 = { x: Number(f.x1) + 100, y: Number(f.y1) - 170 }
      pt2 = { x: Number(f.x2) + 100, y: Number(f.y2) - 170 }
      this.mtgAppLecteur.setPointPosition(this.svgId, zu.point1, pt1.x, pt1.y, true)
      this.mtgAppLecteur.setPointPosition(this.svgId, zu.point2, pt2.x, pt2.y, true)
      this.faitFuncGomm(zu, 'demid')
    } else {
      if (f.coy >= 1000) {
        zu = this.creeNewDemiDroite((f.color) ? j3pClone(f.color) : [0, 0, 0])
        this.listeDemiDPris.push(zu)
      } else {
        zu = this.creeNewSeg((f.color) ? j3pClone(f.color) : [0, 0, 0])
        this.listeSegPris.push(zu)
      }
      zu.viz = f.viz !== false && f.visible !== false
      pt1 = this.getPointPosition2(this.svgId, this.retrouvePoint(this.recupe(f.p1)))
      pt2 = this.getPointPosition2(this.svgId, this.retrouvePoint(this.recupe(f.p2)))
      let a = (pt1.y - pt2.y) / (pt1.x - pt2.x)
      if (pt1.x === pt2.x) a = this.maxInt
      const b = pt1.y - a * pt1.x
      let lasol2
      if (f.coy < 1000) {
        const sol = this.getInstersectingPoints(a, b, pt2.x, pt2.y, f.coy)
        const s1 = pt1.x - pt2.x
        const s2 = sol[0].x - pt2.x
        lasol2 = (s1 * s2 > 0) ? sol[1] : sol[0]
      } else {
        lasol2 = pt2
      }
      this.mtgAppLecteur.setPointPosition(this.svgId, zu.point1, pt1.x, pt1.y, false)
      this.mtgAppLecteur.setPointPosition(this.svgId, zu.point2, lasol2.x, lasol2.y, false)
      if (f.aff !== undefined || f.affnom !== undefined) {
        const laf = f.aff || f.affnom
        zu.nom = laf
        this.nomspris.push(laf)
        this.creeNewNom(zu)
        if (f.placeGarde) {
          const tagInt = this.newTag()
          const tagPerp = this.newTag()
          const tagcerc = this.newTag()
          const tagPoint = this.newTag()
          const tagPoint2 = this.newTag()
          this.mtgAppLecteur.calculate(this.svgId)
          this.mtgAppLecteur.addFreePoint({
            tag: tagInt,
            name: tagInt,
            x: pt1.x + f.placeGarde.abs * (lasol2.x - pt1.x),
            y: pt1.y + f.placeGarde.abs * (lasol2.y - pt1.y)
          })
          this.mtgAppLecteur.addLinePerp({
            tag: tagPerp,
            a: tagInt,
            d: zu.ligne
          })
          this.mtgAppLecteur.addCircleOr({
            tag: tagcerc,
            o: tagInt,
            r: f.placeGarde.dist / this.unite
          })
          this.mtgAppLecteur.addIntLineCircle({
            tag: tagPoint,
            name: tagPoint,
            tag2: tagPoint2,
            name2: tagPoint2,
            c: tagcerc,
            d: tagPerp
          })
          const toComp = (f.placeGarde.ou12) ? tagPoint : tagPoint2
          const cooG = this.mtgAppLecteur.getPointPosition({ a: toComp })
          this.mtgAppLecteur.deleteElt({ elt: tagInt })
          this.mtgAppLecteur.setPointPosition(this.svgId, '#' + zu.pointNom, cooG.x, cooG.y, true)
        } else {
          let fax = f.plx
          if (this.txtF === 'FigEditor') {
            fax -= 200
          }
          this.mtgAppLecteur.setPointPosition(this.svgId, '#' + zu.pointNom, fax + 100, f.ply - 170, true)
        }
        this.setColor2(this.svgId, zu.aff, 0, 0, 0, true)
        this.setVisible2(this.svgId, zu.aff, zu.viz, true)
      } else {
        zu.nom = this.recupe(f.nom)
      }
    }
    this.setVisible2(this.svgId, zu.ligne, zu.viz, true)
    this.setColor2(this.svgId, zu.ligne, zu.color[0], zu.color[1], zu.color[2], true)
    this.setLineStyle2(this.svgId, zu.ligne, (f.pointille) ? 2 : 0, 2, true)
  }

  ajdr (f) {
    let zu, bbuf, pt1, pt2
    if (f.x1) {
      zu = this.creeNewDroite([0, 0, 0])
      this.listeDtePris.push(zu)
      pt1 = { x: Number(f.x1) + 100, y: Number(f.y1) - 170 }
      pt2 = { x: Number(f.x2) + 100, y: Number(f.y2) - 170 }
      zu.viz = f.viz !== false && f.visible !== false
      if (f.aff !== undefined && zu.viz) {
        this.nomDebDte.push(f.aff)
        zu.nom = f.aff
        this.nomspris.push(f.aff)
        this.creeNewNom(zu)
        this.mtgAppLecteur.setPointPosition(this.svgId, '#' + zu.pointName, Number(f.x3) + 100, Number(f.y3) - 170, true)
        this.mtgAppLecteur.setText(this.svgId, '#' + zu.aff, f.aff, true)
        this.setColor2(this.svgId, zu.aff, 0, 150, 0, true)
      }
      this.mtgAppLecteur.setPointPosition(this.svgId, zu.point1, pt1.x, pt1.y, false)
      this.mtgAppLecteur.setPointPosition(this.svgId, zu.point2, pt2.x, pt2.y, false)
      this.faitFuncGomm(zu, 'dte')
    } else {
      let p1, p2
      if (f.dep) {
        p1 = this.recupe(f.dep[0])
        p2 = this.recupe(f.dep[1])
      } else {
        p1 = this.recupe(f.nom)[1]
        p2 = this.recupe(f.nom)[2]
      }
      const point1 = this.retrouvepoint(p1)
      const point2 = this.retrouvepoint(p2)
      pt1 = this.getPointPosition2(this.svgId, point1)
      pt2 = this.getPointPosition2(this.svgId, point2)
      if ((f.cox >= 1000) && (f.coy >= 1000)) {
        zu = this.creeNewDroite([0, 0, 0])
        this.listeDtePris.push(zu)
        if (f.aff !== undefined || f.affnom !== undefined) {
          const laf = f.aff || f.affnom
          this.nomDebDte.push(laf)
        } else {
          this.nomDebDte.push(f.nom)
        }
      } else {
        if (f.cox >= 1000 || f.coy >= 1000) {
          zu = this.creeNewDemiDroite([0, 0, 0])
          this.listeDemiDPris.push(zu)
          if (f.cox >= 1000) {
            bbuf = p1
            p1 = p2
            p2 = bbuf
            bbuf = f.cox
            f.cox = f.coy
            f.coy = bbuf
          }
        } else {
          zu = this.creeNewSeg([0, 0, 0])
          this.listeSegPris.push(zu)
          this.nomDebSeg.push('(' + p1 + p2 + ')')
        }
      }
      zu.nom = '(' + p1 + p2 + ')'
      zu.viz = f.viz !== false && f.visible !== false
      // retrouve coo 2 pt
      let a = (pt1.y - pt2.y) / (pt1.x - pt2.x)
      if (pt1.x === pt2.x) a = this.maxInt
      let b = pt1.y - a * pt1.x
      let lasol1
      if (f.cox < 1000) {
        const sol = this.getInstersectingPoints(a, b, pt1.x, pt1.y, f.cox)
        let s1 = pt2.x - pt1.x
        let s2 = sol[0].x - pt1.x
        lasol1 = (s1 * s2 > 0) ? sol[1] : sol[0]
        if (pt1.x === pt2.x) {
          s1 = pt2.y - pt1.y
          s2 = sol[0].y - pt1.y
          if (s1 * s2 > 0) { lasol1 = sol[1] } else { lasol1 = sol[0] }
        }
      } else {
        lasol1 = pt1
      }
      b = pt2.y - a * pt2.x
      let lasol2
      if (f.coy < 1000) {
        const sol = this.getInstersectingPoints(a, b, pt2.x, pt2.y, f.coy)
        let s1 = pt1.x - pt2.x
        let s2 = sol[0].x - pt2.x
        lasol2 = (s1 * s2 > 0) ? sol[1] : lasol2 = sol[0]
        if (pt1.x === pt2.x) {
          s1 = pt1.y - pt2.y
          s2 = sol[0].y - pt2.y
          lasol2 = (s1 * s2 > 0) ? sol[1] : lasol2 = sol[0]
        }
      } else {
        lasol2 = pt2
      }
      this.mtgAppLecteur.setPointPosition(this.svgId, zu.point1, lasol1.x, lasol1.y, true)
      this.mtgAppLecteur.setPointPosition(this.svgId, zu.point2, lasol2.x, lasol2.y, true)
      if ((f.aff !== undefined || f.affnom !== undefined)) {
        const laf = f.aff || f.affnom
        zu.nom = laf
        this.creeNewNom(zu)
        if (f.placeGarde) {
          const tagInt = this.newTag()
          const tagPerp = this.newTag()
          const tagcerc = this.newTag()
          const tagPoint = this.newTag()
          const tagPoint2 = this.newTag()
          this.mtgAppLecteur.calculate(this.svgId)
          this.mtgAppLecteur.addFreePoint({
            tag: tagInt,
            name: tagInt,
            x: lasol1.x + f.placeGarde.abs * (lasol2.x - lasol1.x),
            y: lasol1.y + f.placeGarde.abs * (lasol2.y - lasol1.y)
          })
          this.mtgAppLecteur.addLinePerp({
            tag: tagPerp,
            a: tagInt,
            d: zu.ligne
          })
          this.mtgAppLecteur.addCircleOr({
            tag: tagcerc,
            o: tagInt,
            r: f.placeGarde.dist / this.unite
          })
          this.mtgAppLecteur.addIntLineCircle({
            tag: tagPoint,
            name: tagPoint,
            tag2: tagPoint2,
            name2: tagPoint2,
            c: tagcerc,
            d: tagPerp
          })
          const toComp = (f.placeGarde.ou12) ? tagPoint : tagPoint2
          const cooG = this.mtgAppLecteur.getPointPosition({ a: toComp })
          this.mtgAppLecteur.deleteElt({ elt: tagInt })
          this.mtgAppLecteur.setPointPosition(this.svgId, '#' + zu.pointNom, cooG.x, cooG.y, true)
        } else {
          let fax = f.plx
          if (this.txtF === 'FigEditor') {
            fax -= 200
          }
          this.mtgAppLecteur.setPointPosition(this.svgId, '#' + zu.pointNom, fax + 100, f.ply - 170, true)
        }
        this.setColor2(this.svgId, zu.aff, 0, 0, 0, true)
        this.setVisible2(this.svgId, zu.aff, zu.viz, true)
        if (zu.viz) {
          this.nomspris.push(laf)
        }
      }
    }
    this.setVisible2(this.svgId, zu.ligne, zu.viz, true)
    this.setColor2(this.svgId, zu.ligne, zu.color[0], zu.color[1], zu.color[2], true)
    this.setLineStyle2(this.svgId, zu.ligne, (f.pointille) ? 2 : 0, 2, true)
  }

  ajdrspe (f) {
    const viz = (f.visible !== false)
    let a, b, pt2
    const zu = this.creeNewDroite([0, 0, 0])
    zu.viz = viz
    this.listeDtePris.push(zu)
    this.nomDebDte.push(f.nom)
    this.setVisible2(this.svgId, zu.ligne, zu.viz, true)
    this.setColor2(this.svgId, zu.ligne, 0, 0, 0, true)
    this.setLineStyle2(this.svgId, zu.ligne, (f.pointille) ? 2 : 0, 2, true)
    // retrouve coo 2 pt
    const pt1 = this.getPointPosition2(this.svgId, this.retrouvePoint(this.recupe(f.pass)))
    const nn = this.giveAB(f.odt)
    if (f.spe === 'parallèle') {
      a = nn.a
      b = pt1.y - a * pt1.x
      pt2 = { x: 1000 }
      pt2.y = a * pt2.x + b
    } else {
      a = -1 / nn.a
      if (nn.a === 0) {
        pt2 = { x: pt1.x, y: 1000 }
      } else {
        b = pt1.y - a * pt1.x
        pt2 = { x: 1000 }
        pt2.y = a * pt2.x + b
      }
    }
    this.mtgAppLecteur.setPointPosition(this.svgId, zu.point1, pt1.x, pt1.y, false)
    this.mtgAppLecteur.setPointPosition(this.svgId, zu.point2, pt2.x, pt2.y, false)
    this.nomspris.push(f.nom)
    zu.nom = f.nom
    if (f.affichenom !== false && zu.viz) {
      this.creeNewNom(zu)
      if (f.placeGarde) {
        const tagInt = this.newTag()
        const tagPerp = this.newTag()
        const tagPerpA = this.newTag()
        const tagcerc = this.newTag()
        const tagPoint = this.newTag()
        const tagPointA = this.newTag()
        const tagPoint2 = this.newTag()
        this.mtgAppLecteur.calculate(this.svgId)
        this.mtgAppLecteur.addLinePerp({
          tag: tagPerpA,
          a: 'Z2',
          d: zu.ligne
        })
        this.mtgAppLecteur.addIntLineLine({
          tag: tagPointA,
          name: tagPointA,
          d2: zu.ligne,
          d: tagPerpA
        })
        const ccoG = this.getPointPosition2(this.svgId, tagPointA)
        this.mtgAppLecteur.addFreePoint({
          tag: tagInt,
          name: tagInt,
          x: pt1.x + f.placeGarde.abs * (ccoG.x - pt1.x),
          y: pt1.y + f.placeGarde.abs * (ccoG.y - pt1.y)
        })
        this.mtgAppLecteur.addLinePerp({
          tag: tagPerp,
          a: tagInt,
          d: zu.ligne
        })
        this.mtgAppLecteur.addCircleOr({
          tag: tagcerc,
          o: tagInt,
          r: f.placeGarde.dist / this.unite
        })
        this.mtgAppLecteur.addIntLineCircle({
          tag: tagPoint,
          name: tagPoint,
          tag2: tagPoint2,
          name2: tagPoint2,
          c: tagcerc,
          d: tagPerp
        })
        const toComp = (f.placeGarde.ou12) ? tagPoint : tagPoint2
        const cooG = this.mtgAppLecteur.getPointPosition({ a: toComp })
        // this.mtgAppLecteur.deleteElt({ elt: tagInt })
        this.mtgAppLecteur.setPointPosition(this.svgId, '#' + zu.pointNom, cooG.x, cooG.y, true)
        this.mtgAppLecteur.deleteElt({ elt: tagPerpA })
        this.mtgAppLecteur.deleteElt({ elt: tagInt })
      } else {
        let fax = f.plx
        if (this.txtF === 'FigEditor') {
          fax -= 200
        }
        this.mtgAppLecteur.setPointPosition(this.svgId, '#' + zu.pointNom, fax + 100, f.ply - 170, true)
      }
      this.mtgAppLecteur.setText(this.svgId, '#' + zu.aff, f.nom, true)
      this.setColor2(this.svgId, zu.aff, true, 0, 0, 0, true)
    }
  }

  mouseup (ev) {
    this.consoleDev('ttt1')
    if (this.onco) return
    if (this.PtRegle2) {
      this.setVisible2(this.svgId, '#poly0', false, false)
      this.consoleDev('finregle?')
      this.gereFinRegle()
      this.PtRegle2 = false
      return
    }
    if (this.PtCompS2) {
      this.gereFinCOmpas()
      this.PtCompS2 = false
      return
    }
    if (this.larot) {
      const zt = this.getPointPosition2(this.svgId, '#ptvizrot')
      this.mtgAppLecteur.setPointPosition(this.svgId, '#ptbougrot', zt.x, zt.y, true)
    }
    if (this.lepoint3) {
      if (this.aplace === 'dte') {
        const kk = this.distPtDtebis(this.a1, this.acons, this.a2)
        if (kk > 1) return
        this.mtgAppLecteur.setPointPosition(this.svgId, '#repd1', 1000, 1000, true)
        this.mtgAppLecteur.setPointPosition(this.svgId, '#repd2', 1000, 1010, true)
      } else {
        const kk = this.dist(this.acons, this.a1)
        if (Math.abs(kk - this.a2) > this.unite) return
        this.mtgAppLecteur.setPointPosition(this.svgId, '#repcour1', 1000, 1000, true)
        this.mtgAppLecteur.setPointPosition(this.svgId, '#repcour2', 1000, 1010, true)
      }
      if (this.mondivBUlle !== undefined) {
        j3pDetruit(this.mondivBUlle)
        this.mondivBUlle = undefined
      }
      this.aplace = undefined
    }
    /*
    if (this.me.etat === 'correction') {
      this.setColor2(this.svgId, '#ddp1', 0, 0, 0, false)
      this.setColor2(this.svgId, '#ddp2', 0, 0, 0, false)
      this.setColor2(this.svgId, '#ddp3', 0, 0, 0, false)
      this.setColor2(this.svgId, '#ddp4', 0, 0, 0, false)
    }
    */
    this.lepoint3 = false
    if (this.regleVisible) {
      const pt = this.getPointPosition2(this.svgId, this.idPtRegle.pt)
      const zan = this.mtgAppLecteur.valueOf(this.svgId, 'reglangleC')
      this.mtgAppLecteur.setPointPosition(this.svgId, '#pottt', pt.x + 219 * this.zoom * Math.cos(zan * Math.PI / 180), pt.y + 219 * this.zoom * Math.sin(zan * Math.PI / 180), false)
    }
    if (this.equerreVisible) {
      const pt = this.getPointPosition2(this.svgId, this.idPtEquerre.pt)
      const zan = this.mtgAppLecteur.valueOf(this.svgId, 'equerangleC')
      this.mtgAppLecteur.setPointPosition(this.svgId, '#pteqma0', pt.x + 124 * this.zoom * Math.cos(zan * Math.PI / 180), pt.y - 124 * this.zoom * Math.sin(zan * Math.PI / 180), false)
    }
    if (this.rapporteurVisible) {
      const pt = this.getPointPosition2(this.svgId, '#rappt1')
      const zan = this.mtgAppLecteur.valueOf(this.svgId, 'rapangleC')
      this.mtgAppLecteur.setPointPosition(this.svgId, '#raprap10', pt.x + 82 * this.zoom * Math.cos(zan * Math.PI / 180), pt.y - 82 * this.zoom * Math.sin(zan * Math.PI / 180), false)
    }
    if (this.compasVisible) {
      this.mtgAppLecteur.getDoc(this.svgId).defaultCursor = null
      const df = this.getPointPosition2(this.svgId, this.idCompPt.range)
      this.mtgAppLecteur.setPointPosition(this.svgId, '#' + this.idCompPt.geremine, df.x, df.y, false)
    }
    // this.mtgAppLecteur.giveFormula2(this.svgId, 'equerangleC', 'equerangl2', true)
    if (this.lepoint || this.lepoint4) {
      this.ptgardeaucasou = undefined
      this.setVisible2(this.svgId, '#ptbalade', false, true)
      this.setVisible2(this.svgId, '#segbal1', false, true)
      this.setVisible2(this.svgId, '#segbal2', false, true)
      this.mtgAppLecteur.getDoc(this.svgId).defaultCursor = null
      if (this.BBloque) {
        this.metToutFalse()
        return
      }
      this.lepoint = this.lepoint4 = false
      const lm = this.getPointPosition2(this.svgId, '#ptbalade')
      const ppp1 = this.mtgAppLecteur.valueOf(this.svgId, 'PROJ1')
      const ppp2 = this.mtgAppLecteur.valueOf(this.svgId, 'PROJ2')
      if (ppp1 < 0 || ppp2 < 0 || ppp1 > 1 || ppp2 > 2) {
        return
      }
      if (this.vpi) {
        for (let j = 0; j < this.nomspris.length; j++) {
          if (this.typeDe(this.nomspris[j]) === 'point') {
            if (this.affDe(this.nomspris[j]) === this.listePointPris[this.vpin].aff) {
              if (j < this.ConsPtnb) {
                this.affModale('Tu ne peux pas renommer ce point !')
                this.metToutFalse()
                this.viregommEtrot()
                return
              }
              this.ptgardeaucasou = j3pClone(this.nomspris[j])
              this.nomspris.splice(j, 1)
              this.affModaleQuest('Renommer ce point : ', this.listePointPris[this.vpin])
              return
            }
          }
        }
      }
      const monnewp = this.creeNewPoint(' ', ' ', [0, 0, 0])
      monnewp.viz = true
      monnewp.color = j3pClone(this.laCoool)
      this.mtgAppLecteur.setPointPosition(this.svgId, monnewp.point, lm.x, lm.y, true)
      this.setColor2(this.svgId, monnewp.point, this.laCoool[0], this.laCoool[1], this.laCoool[2], true)
      this.mtgAppLecteur.setText(this.svgId, '#' + monnewp.aff, '?', true)
      if (this.laCoool[0] === 150 && this.laCoool[1] === 150 && this.laCoool[2] === 150) {
        this.setColor2(this.svgId, monnewp.aff, 0, 0, 0, true)
      } else {
        this.setColor2(this.svgId, monnewp.aff, this.laCoool[0], this.laCoool[1], this.laCoool[2], true)
      }
      this.listePointPris.push(monnewp)
      monnewp.nom = undefined
      this.faitFuncGomm(monnewp, 'pt')
      this.metToutFalse()
      this.affModaleQuest('Ce point s’appelle : ', monnewp)
      return
    }
    // verif place matos ?
    // fin verif matos
    // vire tous les captures
    if (!this.capCAP) { this.metToutFalse() }
    this.capCAP = false
    this.mtgAppLecteur.setPointPosition(this.svgId, '#ptbalade', 1000, 1000, false)
    this.mtgAppLecteur.updateFigure(this.svgId)
    this.consoleDev('ttt')
    // verif point bleu point rouge
    // Pres de la règle
    let df = this.mtgAppLecteur.valueOf(this.svgId, 'PtRegle')
    if (df !== -1 && this.regleVisible) {
      this.gereRegle(ev)
      return
    }
    df = this.mtgAppLecteur.valueOf(this.svgId, 'PtEquC')
    if (df !== -1 && this.equerreVisible) {
      this.gereEquerre(ev, 0)
      return
    }
    df = this.mtgAppLecteur.valueOf(this.svgId, 'ptEquCC')
    if (df !== -1 && this.equerreVisible) {
      this.gereEquerre(ev, 1)
      return
    }
    df = this.mtgAppLecteur.valueOf(this.svgId, 'ptEquH')
    if (df !== -1 && this.equerreVisible) {
      this.gereEquerre(ev, 2)
      return
    }
    df = this.mtgAppLecteur.valueOf(this.svgId, 'ptRap')
    if (df !== -1 && this.rapporteurVisible) {
      this.setVisible2(this.svgId, '#poly0', false, false)
      this.gereRapporteur()
      return
    }
    df = this.mtgAppLecteur.valueOf(this.svgId, 'compTratraC')
    if (df !== -1 && this.compasVisible) {
      this.gereCompas(ev)
      return
    }
    this.renvoieCrayon()
    if (this.mtgAppLecteur.getDoc(this.svgId).defaultCursor === 'none') this.mtgAppLecteur.getDoc(this.svgId).defaultCursor = ''
  }

  inter2cercles (c1, r1, c2, r2) {
    const t = [c1.x, c1.y, r1]
    const t2 = [c2.x, c2.y, r2]
    return this.calculerIntersectionsCercles(t, t2)
  }

  calculerIntersectionsCercles (c1, c2) {
    const Xc1 = c1[0] // abscisse du centre du premier cercle
    const Yc1 = c1[1] // ordonnée du centre du premier cercle
    const Rc1 = c1[2] // rayon du premier cercle
    const Xc2 = c2[0] // abscisse du centre du second cercle
    const Yc2 = c2[1] // ordonnée du centre du second cercle
    const Rc2 = c2[2] // rayon du second cercle

    let XIa, XIb, YIa, YIb
    if (Yc1 === Yc2) {
      const a = Math.abs(Xc1 - Xc2)
      XIa = XIb = (Math.pow(Rc1, 2) - Math.pow(a, 2) - Math.pow(Rc2, 2)) / (-2 * a)
      YIa = Math.sqrt(Math.pow(Rc1, 2) - Math.pow(a - XIa, 2))
      YIb = -YIa
      YIb += Yc1
      YIa += Yc1
      XIa = XIb = XIa + Math.min(Xc1, Xc2)
    } else {
      const a = (-Math.pow(Xc1, 2) - Math.pow(Yc1, 2) + Math.pow(Xc2, 2) + Math.pow(Yc2, 2) + Math.pow(Rc1, 2) - Math.pow(Rc2, 2)) / (2 * (Yc2 - Yc1))
      const d = (Xc2 - Xc1) / (Yc2 - Yc1)
      const A = Math.pow(d, 2) + 1
      const B = -2 * Xc1 + 2 * Yc1 * d - 2 * a * d
      const C = Math.pow(Xc1, 2) + Math.pow(Yc1, 2) - 2 * Yc1 * a + Math.pow(a, 2) - Math.pow(Rc1, 2)
      const delta = Math.pow(B, 2) - 4 * A * C
      XIa = (-B + Math.sqrt(delta)) / (2 * A)
      XIb = (-B - Math.sqrt(delta)) / (2 * A)
      YIa = a - ((-B + Math.sqrt(delta)) / (2 * A)) * d
      YIb = a - ((-B - Math.sqrt(delta)) / (2 * A)) * d
    }

    if (isNaN(XIa) || isNaN(YIa) || isNaN(XIb) || isNaN(YIb)) { return false } // si les cercles ne se touchent pas ou bien sont identiques
    return [{ x: XIa, y: YIa }, { x: XIb, y: YIb }] // coordonnées des deux points d’intersection [abscisse,ordonnée] (nb : seront identiques si les cercles ne se touchent qu’en un seul point)
  }

  point () {
    if (this.unecapt()) return
    this.lagomm = false
    this.lagomVire()
    this.larot = false
    this.larotVire()
    this.poitilEncours = false
    this.pointilleVire()
    this.lepoint = !this.lepoint
    this.equerreVisible = true
    this.rapporteurVisible = true
    this.compasVisible = true
    this.vpi = false
    this.equerre()
    this.rapporteur()
    this.compas()
    if (this.lepoint) {
      this.mtgAppLecteur.getDoc(this.svgId).defaultCursor = 'none'
      this.mtgAppLecteur.giveFormula2(this.svgId, 'tttttttttt', this.unitebase / this.unite)
      this.setVisible2(this.svgId, '#segbal1', true, false)
      this.setVisible2(this.svgId, '#segbal2', true, false)
      this.mtgAppLecteur.setPointPosition(this.svgId, '#ptbalade', 730, 20, false)
      this.mtgAppLecteur.updateFigure(this.svgId)
    }
  }

  point3 (ev) {
    if (this.unecapt()) return
    this.lagomm = false
    this.lagomVire()
    this.larotVire()
    this.lepoint4 = true
    this.lepoint2 = this.lepoint22 = false
    this.equerreVisible = true
    this.rapporteurVisible = true
    this.compasVisible = true
    this.equerre()
    this.rapporteur()
    this.compas()

    this.x = ev.changedTouches[0].clientX
    this.y = ev.changedTouches[0].clientY
    const GG = this.zonefig.getBoundingClientRect()
    this.mtgAppLecteur.setPointPosition(this.svgId, '#pointlibre', this.x - GG.x, this.y - GG.y, true)
    this.mtgAppLecteur.setPointPosition(this.svgId, '#ptbalade', this.x - GG.x, this.y - GG.y, true)
    this.x = this.x - GG.x
    this.y = this.y - GG.y

    this.mtgAppLecteur.giveFormula2(this.svgId, 'tttttttttt', this.unitebase / this.unite)
    this.setVisible2(this.svgId, '#segbal1', true, false)
    this.setVisible2(this.svgId, '#segbal2', true, false)
    this.mtgAppLecteur.updateFigure(this.svgId)
  }

  point2 () {
    if (this.unecapt()) return
    this.viregommEtrot()
    this.lepoint2 = !this.lepoint2
    // this.mtgAppLecteur.setPointPosition(this.svgId, this.idImCrayPt, this.x,d.y, true)
    this.setVisible2(this.svgId, this.idImCrayPt, false, true)
    this.setVisible2(this.svgId, this.idImCray, true, true)
    if (this.lepoint2) {
      this.mtgAppLecteur.getDoc(this.svgId).defaultCursor = 'none'
      if (this.regleVisible && !this.lagrad) {
        const listpoooo = this.listpointaligne()
        if (listpoooo.length > 0) {
          this.bloquepoint = true
          let i
          for (i = 0; i < Math.min(listpoooo.length, 4); i++) {
            this.mtgAppLecteur.setPointPosition(this.svgId, '#dp' + (i + 1), listpoooo[i].x, listpoooo[i].y, false)
          }
          for (let j = i; j < 4; j++) {
            this.mtgAppLecteur.setPointPosition(this.svgId, '#dp' + (j + 1), listpoooo[listpoooo.length - 1].x, listpoooo[listpoooo.length - 1].y, false)
          }
        } else {
          this.bloquepoint = false
          this.eteintPoint()
        }
      } else {
        this.bloquepoint = false
        this.eteintPoint()
      }
    }
    this.bloqueEventm = false
  }

  point22 (ev) {
    this.BBloque = false
    if (this.unecapt()) return
    this.lepoint22 = true
    this.montrepoint1()
    this.x = ev.changedTouches[0].clientX
    this.y = ev.changedTouches[0].clientY
    const GG = this.zonefig.getBoundingClientRect()
    /*
    const Gco = j3pGetPos(this.mepact)
    GG.x = GG.x - Gco.x / 2
    GG.y = GG.y - Gco.y / 2
     */
    this.mtgAppLecteur.setPointPosition(this.svgId, '#pointlibre', this.x - GG.x, this.y - GG.y, true)
    this.mtgAppLecteur.setPointPosition(this.svgId, this.idImCrayPt, this.x - GG.x, this.y - GG.y, true)
    this.x = this.x - GG.x
    this.y = this.y - GG.y
    this.setVisible2(this.svgId, this.idImCrayPt, false, true)
    this.setVisible2(this.svgId, this.idImCray, true, true)
    if (this.lepoint2) {
      this.mtgAppLecteur.getDoc(this.svgId).defaultCursor = 'none'
      if (this.regleVisible && !this.lagrad) {
        const listpoooo = this.listpointaligne()
        if (listpoooo.length > 0) {
          this.bloquepoint = true
          let i
          for (i = 0; i < Math.min(listpoooo.length, 4); i++) {
            this.mtgAppLecteur.setPointPosition(this.svgId, '#dp' + (i + 1), listpoooo[i].x, listpoooo[i].y, false)
          }
          for (let j = i; j < 4; j++) {
            this.mtgAppLecteur.setPointPosition(this.svgId, '#dp' + (j + 1), listpoooo[listpoooo.length - 1].x, listpoooo[listpoooo.length - 1].y, false)
          }
        } else {
          this.bloquepoint = false
          this.eteintPoint()
        }
      } else {
        this.bloquepoint = false
        this.eteintPoint()
      }
    }
    this.bloqueEventm = false
  }

  makeFoncOver (i, z, k) {
    let ttab = this.polyhaut
    if (z === 1) ttab = this.polydroite
    const aret = function () {
      if (!this.okmouv) return
      if (this.mtgAppLecteur.getDoc(this.svgId).pointCapture) return
      if (!this.bloqueEventm) this.BBloque = true
      if (!this.lepoint) this.mtgAppLecteur.getDoc(this.svgId).defaultCursor = 'pointer'
      this.mtgAppLecteur.setColor({ elt: ttab[i].replace('#', ''), color: '#a60000', opacity: 0.8 })
      this.mtgAppLecteur.setVisible({ elt: ttab[i].replace('#', '') })
      this.mtgAppLecteur.setLineStyle({ elt: ttab[i].replace('#', ''), lineStyle: 'line' })
    }
    if (k === 'gomme') this.lagommontre = aret
    if (k === 'rotation') this.larotmontre = aret
    if (k === 'centre') this.montrexzntre = aret
    if (k === 'point1') this.montrepoint1 = aret
    if (k === 'pointille') this.pointillemontre = aret
    return aret.bind(this)
  }

  makeFoncOut (i, z, k) {
    let ttab = this.polyhaut
    if (z === 1) ttab = this.polydroite
    if (k === 'gomme') {
      this.lagomVire = function () {
        this.BBloque = false
        if (this.lagomm) return
        if (!this.lepoint) this.mtgAppLecteur.getDoc(this.svgId).defaultCursor = null
        this.mtgAppLecteur.setHidden({ elt: ttab[i] })

        if (this.degros) {
          this.grossitout(false)
          this.degros = false
        }

        this.mtgAppLecteur.updateFigure(this.svgId)
      }
      return this.lagomVire.bind(this)
    }
    if (k === 'pointille') {
      this.pointilleVire = function () {
        this.BBloque = false
        if (this.poitilEncours) return
        if (!this.lepoint) this.mtgAppLecteur.getDoc(this.svgId).defaultCursor = null
        this.setVisible2(this.svgId, ttab[i], false, false)

        if (this.degros) {
          this.grossitout(false)
          this.degros = false
        }

        this.mtgAppLecteur.updateFigure(this.svgId)
      }
      return this.pointilleVire.bind(this)
    }
    if (k === 'rotation') {
      this.larotVire = function () {
        this.mtgAppLecteur.getDoc(this.svgId).defaultCursor = null
        this.mtgAppLecteur.setHidden({ elt: ttab[i] })
        this.mtgAppLecteur.setHidden({ elt: 'polyrot' })
        // this.mtgAppLecteur.setHidden({ elt: 'segrot' })
        this.mtgAppLecteur.setHidden({ elt: 'segrot2' })
        this.mtgAppLecteur.setHidden({ elt: 'fondrot' })
        this.mtgAppLecteur.setHidden({ elt: 'ptvizrot' })
        this.mtgAppLecteur.setHidden({ elt: 'ptbougrot' })
        this.mtgAppLecteur.setHidden({ elt: 'cerclerot' })
      }
      const aret = function () {
        this.BBloque = false
        if (this.larot) return
        if (!this.lepoint) this.mtgAppLecteur.getDoc(this.svgId).defaultCursor = null
        this.mtgAppLecteur.setHidden({ elt: ttab[i] })
        // this.mtgAppLecteur.updateFigure(this.svgId)
      }
      return aret.bind(this)
    }
    if (k === 'point') {
      this.lepointVire = function () {
        if (this.lepoint) return
        this.mtgAppLecteur.getDoc(this.svgId).defaultCursor = null
        this.mtgAppLecteur.setHidden({ elt: ttab[i].replace('#', '') })
      }
      return this.lepointVire.bind(this)
    }
    if (k === 'centre') {
      this.virecentre = function () {
        this.BBloque = false
        if (!this.lepoint) this.mtgAppLecteur.getDoc(this.svgId).defaultCursor = null
        this.mtgAppLecteur.setHidden({ elt: ttab[i].replace('#', '') })
        // this.mtgAppLecteur.updateFigure(this.svgId)
      }
      return this.virecentre.bind(this)
    }
    if (k === 'plus') {
      this.vireplus = function () {
        this.BBloque = false
        if (!this.lepoint) this.mtgAppLecteur.getDoc(this.svgId).defaultCursor = null
        this.mtgAppLecteur.setHidden({ elt: ttab[i] })
        // this.mtgAppLecteur.updateFigure(this.svgId)
      }
      return this.vireplus.bind(this)
    }
    if (k === 'moins') {
      this.viremoins = function () {
        this.BBloque = false
        if (!this.lepoint) this.mtgAppLecteur.getDoc(this.svgId).defaultCursor = null
        this.mtgAppLecteur.setHidden({ elt: ttab[i] })
        // this.mtgAppLecteur.updateFigure(this.svgId)
      }
      return this.viremoins.bind(this)
    }
    const aret = function () {
      this.BBloque = false
      if (!this.lepoint) this.mtgAppLecteur.getDoc(this.svgId).defaultCursor = null
      this.mtgAppLecteur.setHidden({ elt: ttab[i].replace('#', '') })
    }
    return aret.bind(this)
  }

  virecentre () {
    console.error(Error('il ne faut plus utilisé virecentre() qui a été supprimé'))
  }

  distPtSeg (seg, pt, nom) {
    const tagPerp = this.newTag()
    const tagInt = this.newTag()
    this.mtgAppLecteur.addLinePerp({
      a: pt,
      d: seg,
      tag: tagPerp,
      hidden: true
    })
    this.mtgAppLecteur.addIntLineLine({
      d: seg,
      d2: tagPerp,
      tag: tagInt,
      name: tagInt,
      hidden: true
    })
    const coo1 = this.getPointPosition2(this.svgId, tagInt)
    const coo2 = this.getPointPosition2(this.svgId, pt)
    if (coo1.x === undefined || coo1.y === undefined) {
      let tagA = this.retrouveTag(nom[1])
      if (tagA === undefined) {
        tagA = this.reconstruit(nom[1])
      } else {
        tagA = tagA[0]
      }
      const cooA = this.getPointPosition2(this.svgId, tagA)
      let tagB = this.retrouveTag(nom[2])
      if (tagB === undefined) {
        tagB = this.reconstruit(nom[2])
      } else {
        tagB = tagB[0]
      }
      const cooB = this.getPointPosition2(this.svgId, tagB)
      return Math.min(this.dist(coo2, cooA) / this.unite, this.dist(coo2, cooB) / this.unite)
    } else {
      return this.dist(coo1, coo2) / this.unite
    }
  }

  distPtDteTag (pt, dte) {
    const tagDte = this.newTag()
    this.mtgAppLecteur.addLinePerp({
      tag: tagDte,
      a: pt,
      d: dte,
      hidden: true
    })
    const tagPt = this.newTag()
    this.mtgAppLecteur.addIntLineLine({
      d2: tagDte,
      d: dte,
      name: tagPt,
      tag: tagPt,
      hidden: true
    })
    const coo1 = this.getPointPosition2(this.svgId, tagPt)
    const coo2 = this.getPointPosition2(this.svgId, pt)
    return this.dist(coo1, coo2) / this.unite
  }

  distPtDDteTag (pt, dte, nom) {
    const tagDte = this.newTag()
    this.mtgAppLecteur.addLinePerp({
      tag: tagDte,
      a: pt,
      d: dte,
      hidden: true
    })
    const tagPt = this.newTag()
    this.mtgAppLecteur.addIntLineLine({
      d2: tagDte,
      d: dte,
      name: tagPt,
      tag: tagPt,
      hidden: true
    })
    const coo1 = this.getPointPosition2(this.svgId, tagPt)
    const coo2 = this.getPointPosition2(this.svgId, pt)
    if (coo1.x === undefined || coo1.y === undefined) {
      const tagAp = this.retrouveDprog(nom)
      if (tagAp && tagAp.origine) {
        let tagA = this.retrouveTag(this.recupe(tagAp.origine))
        if (tagA === undefined) {
          tagA = this.reconstruit(tagAp.origine)
        } else {
          tagA = tagA[0]
        }
        const cooA = this.getPointPosition2(this.svgId, tagA)
        return this.dist(coo2, cooA) / this.unite
      } else {
        let tagA = this.retrouveTag(nom[1])
        if (tagA === undefined) {
          tagA = this.reconstruit(nom[1])
        } else {
          tagA = tagA[0]
        }
        const cooA = this.getPointPosition2(this.svgId, tagA)
        return this.dist(coo2, cooA) / this.unite
      }
    } else {
      return this.dist(coo1, coo2) / this.unite
    }
  }

  reconstruit (ki) {
    let jaiMon = -1
    for (let i = 0; i < this.lesReconstruits.length; i++) {
      if (this.lesReconstruits[i].nom.replace(/$/g, '') === this.recupe(ki).replace(/$/g, '')) return this.lesReconstruits[i].tag
    }
    for (let i = 0; i < this.progfig.length; i++) {
      if (this.recupe(this.progfig[i].nom).replace(/$/g, '') === ki.replace(/$/g, '')) {
        const tatag = this.retrouveTag(this.recupe(this.progfig[i].nom))
        if (this.progfig[i].type === 'point') return tatag[0]
        return tatag
      }
    }
    for (let i = 0; i < this.Programme.length; i++) {
      if (this.Programme[i].type === 'multi') {
        for (let j = 0; j < this.Programme[i].cont.length; j++) {
          if (this.recupe(this.Programme[i].cont[j].nom).replace(/$/g, '') === ki.replace(/$/g, '')) {
            jaiMon = j3pClone(this.Programme[i].cont[j])
            break
          } else if (this.Programme[i].cont[j].type === 'point') {
            if (this.Programme[i].cont[j].cond) {
              if (this.Programme[i].cont[j].cond.length > 0) {
                if (this.Programme[i].cont[j].cond[0].ki) {
                  if (this.Programme[i].cont[j].cond[0].ki.nom === ki) {
                    jaiMon = j3pClone(this.Programme[i].cont[j].cond[0].ki)
                  }
                }
              }
              if (jaiMon === -1) {
                if (this.Programme[i].cont[j].cond.length > 1) {
                  if (this.Programme[i].cont[j].cond[1].ki) {
                    if (this.Programme[i].cont[j].cond[1].ki.nom === ki) {
                      jaiMon = j3pClone(this.Programme[i].cont[j].cond[1].ki)
                    }
                  }
                }
              }
            }
          }
        }
      }
    }
    if (jaiMon === -1) {
      if (ki.indexOf('[') !== -1) {
        if (ki.indexOf(']') !== -1) {
          const tagARet = this.newTag()
          let lapProx2 = this.retrouveTag(this.recupe(ki[1]))
          if (lapProx2 === undefined) {
            lapProx2 = this.reconstruit(this.recupe(ki[1]))
          } else {
            lapProx2 = lapProx2[0]
          }
          let lapProx = this.retrouveTag(this.recupe(ki[2]))
          if (lapProx === undefined) {
            lapProx = this.reconstruit(this.recupe(ki[2]))
          } else {
            lapProx = lapProx[0]
          }
          this.mtgAppLecteur.addSegment({
            a: lapProx,
            b: lapProx2,
            tag: tagARet,
            hidden: true
          })
          return this.finishReconstruit({ nom: ki, type: 'segment' }, tagARet)
        } else {
          const tagARet = this.newTag()
          let lapProx2 = this.retrouveTag(this.recupe(ki[1]))
          if (lapProx2 === undefined) {
            lapProx2 = this.reconstruit(this.recupe(ki[1]))
          } else {
            lapProx2 = lapProx2[0]
          }
          let lapProx = this.retrouveTag(this.recupe(ki[2]))
          if (lapProx === undefined) {
            lapProx = this.reconstruit(this.recupe(ki[2]))
          } else {
            lapProx = lapProx[0]
          }
          this.mtgAppLecteur.addRay({
            a: lapProx,
            o: lapProx2,
            tag: tagARet,
            hidden: true
          })
          return this.finishReconstruit({ nom: ki, type: 'demidroite' }, tagARet)
        }
      } else {
        if (ki.indexOf('(') !== -1) {
          const tagARet = this.newTag()
          let lapProx2 = this.retrouveTag(this.recupe(ki[1]))
          if (lapProx2 === undefined) {
            lapProx2 = this.reconstruit(this.recupe(ki[1]))
          } else {
            lapProx2 = lapProx2[0]
          }
          let lapProx = this.retrouveTag(this.recupe(ki[2]))
          if (lapProx === undefined) {
            lapProx = this.reconstruit(this.recupe(ki[2]))
          } else {
            lapProx = lapProx[0]
          }
          this.mtgAppLecteur.addLineAB({
            a: lapProx,
            b: lapProx2,
            tag: tagARet,
            hidden: true
          })
          return this.finishReconstruit({ nom: ki, type: 'droite' }, tagARet)
        }
      }
      return -1
    }
    switch (jaiMon.type) {
      case 'point':
        if (jaiMon.cond.length > 0) {
          const laCond = jaiMon.cond[0]
          switch (laCond.t) {
            case 'ap': {
              let lap = this.retrouveTag(this.recupe(jaiMon.cond[0].ki.nom))
              if (lap === undefined) {
                lap = [this.reconstruit(jaiMon.cond[0].ki.nom)]
              }
              const typlap = jaiMon.cond[0].ki.type
              if (jaiMon.cond.length > 1) {
                switch (jaiMon.cond[1].t) {
                  case 'ap': {
                    let lap2 = this.retrouveTag(this.recupe(jaiMon.cond[1].ki.nom))
                    if (lap2 === undefined) {
                      lap2 = [this.reconstruit(jaiMon.cond[1].ki.nom)]
                    }
                    const typlap2 = jaiMon.cond[1].ki.type
                    const result = []
                    switch (typlap) {
                      case 'segment':
                      case 'droite':
                      case 'demi-droite':
                      case 'demidroite':
                        switch (typlap2) {
                          case 'segment':
                          case 'droite':
                          case 'demi-droite':
                          case 'demidroite': {
                            const tagInt = this.newTag()
                            this.mtgAppLecteur.addIntLineLine({
                              d: lap[0],
                              d2: lap2[0],
                              tag: tagInt,
                              name: tagInt,
                              hidden: true
                            })
                            result.push(tagInt)
                          }
                            break
                          case 'cercle':
                            for (let i = 0; i < lap2.length; i++) {
                              const tagInt = this.newTag()
                              const tagInt2 = this.newTag()
                              this.mtgAppLecteur.addIntLineCircle({
                                d: lap[0],
                                c: lap2[i],
                                tag: tagInt,
                                name: tagInt,
                                tag2: tagInt2,
                                name2: tagInt2,
                                hidden: true
                              })
                              result.push(tagInt, tagInt2)
                            }
                            break
                        }
                        break
                      case 'cercle':
                        switch (typlap2) {
                          case 'segment':
                          case 'droite':
                          case 'demi-droite':
                          case 'demidroite':
                            for (let i = 0; i < lap.length; i++) {
                              const tagInt = this.newTag()
                              const tagInt2 = this.newTag()
                              this.mtgAppLecteur.addIntLineCircle({
                                d: lap2[0],
                                c: lap[i],
                                tag: tagInt,
                                name: tagInt,
                                tag2: tagInt2,
                                name2: tagInt2,
                                hidden: true
                              })
                              result.push(tagInt, tagInt2)
                            }
                            break
                          case 'cercle':
                            for (let i = 0; i < lap.length; i++) {
                              for (let j = 0; j < lap2.length; j++) {
                                const tagInt = this.newTag()
                                const tagInt2 = this.newTag()
                                this.mtgAppLecteur.addIntCircleCircle({
                                  c2: lap[i],
                                  c: lap2[j],
                                  tag: tagInt,
                                  name: tagInt,
                                  tag2: tagInt2,
                                  name2: tagInt2,
                                  hidden: true
                                })
                                result.push(tagInt, tagInt2)
                              }
                            }
                            break
                        }
                        break
                    }
                    return this.finishReconstruit(jaiMon, this.choisPOint(result, jaiMon))
                  }
                  case 'long': {
                    // j'ai ap en 1 et long en 2
                    // tarce un cercle puis cherche pt intersec
                    let lapProx = this.retrouveTag(this.recupe(jaiMon.cond[1].cop))
                    if (lapProx === undefined) {
                      lapProx = this.reconstruit(this.recupe(jaiMon.cond[1].cop))
                    } else {
                      lapProx = lapProx[0]
                    }
                    const didi = this.retrouveLaval(jaiMon.nom, 2)
                    const tagCC = this.newTag()
                    this.mtgAppLecteur.addCircleOr({
                      o: lapProx,
                      r: Number(didi) * this.unite,
                      tag: tagCC,
                      hidden: true
                    })
                    const result = []
                    switch (typlap) {
                      case 'segment':
                      case 'droite':
                      case 'demi-droite':
                      case 'demidroite':
                        {
                          const tagInt = this.newTag()
                          const tagInt2 = this.newTag()
                          this.mtgAppLecteur.addIntLineCircle({
                            d: lap[0],
                            c: tagCC,
                            tag: tagInt,
                            name: tagInt,
                            tag2: tagInt2,
                            name2: tagInt2,
                            hidden: true
                          })
                          result.push(tagInt, tagInt2)
                        }
                        break
                      case 'cercle':
                        for (let i = 0; i < lap.length; i++) {
                          const tagInt = this.newTag()
                          const tagInt2 = this.newTag()
                          this.mtgAppLecteur.addIntCircleCircle({
                            c: lap[i],
                            c2: tagCC,
                            tag: tagInt,
                            name: tagInt,
                            tag2: tagInt2,
                            name2: tagInt2,
                            hidden: true
                          })
                          result.push(tagInt, tagInt2)
                        }
                        break
                    }
                    return this.finishReconstruit(jaiMon, this.choisPOint(result, jaiMon))
                  }
                  case 'ang': {
                    let lapProxSom = this.retrouveTag(this.recupe(jaiMon.cond[1].cop))
                    if (lapProxSom === undefined) {
                      lapProxSom = this.reconstruit(this.recupe(jaiMon.cond[1].cop))
                    } else {
                      lapProxSom = lapProxSom[0]
                    }
                    let lapProxPt = this.retrouveTag(this.recupe(jaiMon.cond[1].cop2))
                    if (lapProxPt === undefined) {
                      lapProxPt = this.reconstruit(this.recupe(jaiMon.cond[1].cop2))
                    } else {
                      lapProxPt = lapProxPt[0]
                    }
                    const tagDD1 = this.newTag()
                    const tagDD2 = this.newTag()
                    const tagDD1PT = this.newTag()
                    const tagDD2PT = this.newTag()
                    this.mtgAppLecteur.addImPointRotation({
                      o: lapProxSom,
                      a: lapProxPt,
                      x: Number(jaiMon.cond[1].formule),
                      tag: tagDD1PT,
                      hidden: true
                    })
                    this.mtgAppLecteur.addImPointRotation({
                      o: lapProxSom,
                      a: lapProxPt,
                      x: -Number(jaiMon.cond[1].formule),
                      tag: tagDD2PT,
                      hidden: true
                    })
                    this.mtgAppLecteur.addRay({
                      o: lapProxSom,
                      a: tagDD1PT,
                      tag: tagDD1,
                      hidden: true
                    })
                    this.mtgAppLecteur.addRay({
                      o: lapProxSom,
                      a: tagDD2PT,
                      tag: tagDD2,
                      hidden: true
                    })
                    const result = []
                    switch (typlap) {
                      case 'segment':
                      case 'droite':
                      case 'demi-droite':
                      case 'demidroite':
                        {
                          const tagInt = this.newTag()
                          const tagInt2 = this.newTag()
                          this.mtgAppLecteur.addIntLineLine({
                            d: lap[0],
                            d2: tagDD1,
                            tag: tagInt,
                            name: tagInt,
                            hidden: true
                          })
                          this.mtgAppLecteur.addIntLineLine({
                            d: lap[0],
                            d2: tagDD2,
                            tag: tagInt2,
                            name: tagInt2,
                            hidden: true
                          })
                          result.push(tagInt, tagInt2)
                        }
                        break
                      case 'cercle':
                        for (let i = 0; i < lap.length; i++) {
                          const tagInt = this.newTag()
                          const tagInt2 = this.newTag()
                          const tagInt3 = this.newTag()
                          const tagInt4 = this.newTag()
                          this.mtgAppLecteur.addIntLineCircle({
                            c: lap[i],
                            d: tagDD1,
                            tag: tagInt,
                            name: tagInt,
                            tag2: tagInt2,
                            name2: tagInt2,
                            hidden: true
                          })
                          this.mtgAppLecteur.addIntLineCircle({
                            c: lap[i],
                            d: tagDD2,
                            tag: tagInt3,
                            name: tagInt3,
                            tag2: tagInt4,
                            name2: tagInt4,
                            hidden: true
                          })
                          result.push(tagInt, tagInt2, tagInt3, tagInt4)
                        }
                        break
                    }
                    return this.finishReconstruit(jaiMon, this.choisPOint(result, jaiMon))
                  }
                }
              } else {
                switch (typlap) {
                  case 'segment':
                  case 'droite':
                  case 'demi-droite':
                  case 'demidroite': {
                    const coPtCa3 = this.getPointPosition2(this.svgId, 'ptCa3')
                    const coPtCa2 = this.getPointPosition2(this.svgId, 'ptCa2')
                    // cherche un point sur tag
                    let gtrouvUnPoint = false
                    let tagLinkPointF
                    for (let i = 1; i < 100; i++) {
                      const tagLinkPoint = this.newTag()
                      const cooLink = { x: coPtCa2.x + i * (coPtCa3.x - coPtCa2.x) / 100, y: coPtCa2.y + i * (coPtCa3.y - coPtCa2.y) / 100 }
                      try {
                        this.mtgAppLecteur.addLinkedPointLine({
                          d: lap[0],
                          x: cooLink.x,
                          y: cooLink.y,
                          name: tagLinkPoint,
                          tag: tagLinkPoint,
                          hiiden: true
                        })
                        tagLinkPointF = tagLinkPoint
                        if (this.placeOkPoint(this.getPointPosition2(this.svgId, tagLinkPoint))) {
                          gtrouvUnPoint = true
                          break
                        } else {
                          gtrouvUnPoint = false
                        }
                      } catch (e) {
                        gtrouvUnPoint = false
                      }
                      if (gtrouvUnPoint) break
                    }
                    return this.finishReconstruit(jaiMon, tagLinkPointF)
                  }
                  case 'cercle': {
                    const coPtCa3 = this.getPointPosition2(this.svgId, 'ptCa3')
                    const coPtCa2 = this.getPointPosition2(this.svgId, 'ptCa2')
                    // cherche un point sur tag
                    let gtrouvUnPoint = false
                    let tagLinkPointF
                    for (let j = 0; j < lap.length; j++) {
                      for (let i = 1; i < 100; i++) {
                        const tagLinkPoint = this.newTag()
                        const cooLink = { x: coPtCa2.x + i * (coPtCa3.x - coPtCa2.x) / 100, y: coPtCa2.y + i * (coPtCa3.y - coPtCa2.y) / 100 }
                        try {
                          this.mtgAppLecteur.addLinkedPointCircle({
                            c: lap[j],
                            x: cooLink.x,
                            y: cooLink.y,
                            name: tagLinkPoint,
                            tag: tagLinkPoint,
                            hidden: true
                          })
                          tagLinkPointF = tagLinkPoint
                          if (this.placeOkPoint(this.getPointPosition2(this.svgId, tagLinkPoint))) {
                            gtrouvUnPoint = true
                            break
                          } else {
                            gtrouvUnPoint = false
                          }
                        } catch (e) {
                          gtrouvUnPoint = false
                        }
                        if (gtrouvUnPoint) break
                      }
                      if (gtrouvUnPoint) break
                    }
                    return this.finishReconstruit(jaiMon, tagLinkPointF)
                  }
                }
              }
            }
              break
            case 'long': {
              // j'ai ap en 1 et long en 2
              // tarce un cercle puis cherche pt intersec
              let lapProx = this.retrouveTag(this.recupe(jaiMon.cond[0].cop))
              if (lapProx === undefined) {
                lapProx = this.reconstruit(this.recupe(jaiMon.cond[0].cop))
              } else {
                lapProx = lapProx[0]
              }
              const didi = this.retrouveLaval(jaiMon.nom, 1)
              const tagCC = this.newTag()
              this.mtgAppLecteur.addCircleOr({
                o: lapProx,
                r: Number(didi) * this.unite,
                tag: tagCC,
                hidden: true
              })
              if (jaiMon.cond.length > 1) {
                switch (jaiMon.cond[1].t) {
                  case 'ap': {
                    let lap2 = this.retrouveTag(this.recupe(jaiMon.cond[1].ki.nom))
                    if (lap2 === undefined) {
                      lap2 = [this.reconstruit(jaiMon.cond[1].ki.nom)]
                    }
                    const typlap2 = jaiMon.cond[1].ki.type
                    const result = []
                    switch (typlap2) {
                      case 'segment':
                      case 'droite':
                      case 'demi-droite':
                      case 'demidroite': {
                        const tagInt = this.newTag()
                        const tagInt2 = this.newTag()
                        this.mtgAppLecteur.addIntLineCircle({
                          d: lap2[0],
                          c: tagCC,
                          tag: tagInt,
                          name: tagInt,
                          tag2: tagInt2,
                          name2: tagInt2,
                          hidden: true
                        })
                        result.push(tagInt, tagInt2)
                      }
                        break
                      case 'cercle':
                        for (let i = 0; i < lap2.length; i++) {
                          const tagInt = this.newTag()
                          const tagInt2 = this.newTag()
                          this.mtgAppLecteur.addIntCircleCircle({
                            c: lap2[i],
                            c2: tagCC,
                            tag: tagInt,
                            name: tagInt,
                            tag2: tagInt2,
                            name2: tagInt2,
                            hidden: true
                          })
                          result.push(tagInt, tagInt2)
                        }
                        break
                    }
                    return this.finishReconstruit(jaiMon, this.choisPOint(result, jaiMon))
                  }
                  case 'long': {
                    // j'ai ap en 1 et long en 2
                    // tarce un cercle puis cherche pt intersec
                    let lapProx2 = this.retrouveTag(this.recupe(jaiMon.cond[1].cop))
                    if (lapProx2 === undefined) {
                      lapProx2 = this.reconstruit(this.recupe(jaiMon.cond[1].cop))
                    } else {
                      lapProx2 = lapProx2[0]
                    }
                    const didi2 = this.retrouveLaval(jaiMon.nom, 2)
                    const tagCC2 = this.newTag()
                    this.mtgAppLecteur.addCircleOr({
                      o: lapProx2,
                      r: Number(didi2) * this.unite,
                      tag: tagCC2,
                      hidden: true
                    })
                    const tagInt = this.newTag()
                    const tagInt2 = this.newTag()
                    this.mtgAppLecteur.addIntCircleCircle({
                      c: tagCC2,
                      c2: tagCC,
                      tag: tagInt,
                      name: tagInt,
                      tag2: tagInt2,
                      name2: tagInt2,
                      hidden: true
                    })
                    if (jaiMon.noprox) {
                      let lapProx3 = this.retrouveTag(this.recupe(jaiMon.noprox))
                      if (lapProx3 === undefined) {
                        lapProx2 = this.reconstruit(this.recupe(jaiMon.cond[1].cop))
                      } else {
                        lapProx3 = lapProx3[0]
                      }
                      const pos1 = this.getPointPosition2(this.svgId, tagInt)
                      const pos2 = this.getPointPosition2(this.svgId, tagInt2)
                      const posM = this.getPointPosition2(this.svgId, lapProx3)
                      const tagaDon = (this.dist(posM, pos1) > this.dist(posM, pos2)) ? tagInt : tagInt2
                      return this.finishReconstruit(jaiMon, this.choisPOint([tagaDon], jaiMon))
                    }
                    if (jaiMon.prox) {
                      let lapProx3 = this.retrouveTag(this.recupe(jaiMon.prox))
                      if (lapProx3 === undefined) {
                        lapProx2 = this.reconstruit(this.recupe(jaiMon.cond[1].cop))
                      } else {
                        lapProx3 = lapProx3[0]
                      }
                      const pos1 = this.getPointPosition2(this.svgId, tagInt)
                      const pos2 = this.getPointPosition2(this.svgId, tagInt2)
                      const posM = this.getPointPosition2(this.svgId, lapProx3)
                      const tagaDon = (this.dist(posM, pos1) < this.dist(posM, pos2)) ? tagInt : tagInt2
                      return this.finishReconstruit(jaiMon, this.choisPOint([tagaDon], jaiMon))
                    }
                    return this.finishReconstruit(jaiMon, this.choisPOint(j3pShuffle([tagInt, tagInt2]), jaiMon))
                    /// a mod
                  }
                  case 'ang': {
                    let lapProxSom = this.retrouveTag(this.recupe(jaiMon.cond[1].cop))
                    if (lapProxSom === undefined) {
                      lapProxSom = this.reconstruit(this.recupe(jaiMon.cond[1].cop))
                    } else {
                      lapProxSom = lapProxSom[0]
                    }
                    let lapProxPt = this.retrouveTag(this.recupe(jaiMon.cond[1].cop2))
                    if (lapProxPt === undefined) {
                      lapProxPt = this.reconstruit(this.recupe(jaiMon.cond[1].cop2))
                    } else {
                      lapProxPt = lapProxPt[0]
                    }
                    const tagDD1 = this.newTag()
                    const tagDD2 = this.newTag()
                    const tagDD1PT = this.newTag()
                    const tagDD2PT = this.newTag()
                    this.mtgAppLecteur.addImPointRotation({
                      o: lapProxSom,
                      a: lapProxPt,
                      x: Number(jaiMon.cond[1].formule),
                      tag: tagDD1PT,
                      hidden: true
                    })
                    this.mtgAppLecteur.addImPointRotation({
                      o: lapProxSom,
                      a: lapProxPt,
                      x: -Number(jaiMon.cond[1].formule),
                      tag: tagDD2PT,
                      hidden: true
                    })
                    this.mtgAppLecteur.addRay({
                      o: lapProxSom,
                      a: tagDD1PT,
                      tag: tagDD1,
                      hidden: true
                    })
                    this.mtgAppLecteur.addRay({
                      o: lapProxSom,
                      a: tagDD2PT,
                      tag: tagDD2,
                      hidden: true
                    })
                    const result = []
                    const tagInt = this.newTag()
                    const tagInt2 = this.newTag()
                    const tagInt3 = this.newTag()
                    const tagInt4 = this.newTag()
                    this.mtgAppLecteur.addIntLineCircle({
                      c: tagCC,
                      d: tagDD1,
                      tag: tagInt,
                      name: tagInt,
                      tag2: tagInt2,
                      name2: tagInt2,
                      hidden: true
                    })
                    this.mtgAppLecteur.addIntLineCircle({
                      c: tagCC,
                      d: tagDD2,
                      tag: tagInt3,
                      name: tagInt3,
                      tag2: tagInt4,
                      name2: tagInt4,
                      hidden: true
                    })
                    result.push(tagInt, tagInt2, tagInt3, tagInt4)
                    return this.finishReconstruit(jaiMon, this.choisPOint(result, jaiMon))
                  }
                }
              } else {
                const coPtCa3 = this.getPointPosition2(this.svgId, 'ptCa3')
                const coPtCa2 = this.getPointPosition2(this.svgId, 'ptCa2')
                // cherche un point sur tag
                let gtrouvUnPoint = false
                let tagLinkPointF
                for (let i = 1; i < 100; i++) {
                  const tagLinkPoint = this.newTag()
                  const cooLink = { x: coPtCa2.x + i * (coPtCa3.x - coPtCa2.x) / 100, y: coPtCa2.y + i * (coPtCa3.y - coPtCa2.y) / 100 }
                  try {
                    this.mtgAppLecteur.addLinkedPointCircle({
                      c: tagCC,
                      x: cooLink.x,
                      y: cooLink.y,
                      name: tagLinkPoint,
                      tag: tagLinkPoint
                    })
                    tagLinkPointF = tagLinkPoint
                    if (this.placeOkPoint(this.getPointPosition2(this.svgId, tagLinkPoint))) {
                      gtrouvUnPoint = true
                      break
                    } else {
                      gtrouvUnPoint = false
                    }
                  } catch (e) {
                    gtrouvUnPoint = false
                  }
                  if (gtrouvUnPoint) break
                }
                return this.finishReconstruit(jaiMon, tagLinkPointF)
              }
              break
            }
            case 'ang': {
              let lapProxSom = this.retrouveTag(this.recupe(jaiMon.cond[0].cop))
              if (lapProxSom === undefined) {
                lapProxSom = this.reconstruit(this.recupe(jaiMon.cond[0].cop))
              } else {
                lapProxSom = lapProxSom[0]
              }
              let lapProxPt = this.retrouveTag(this.recupe(jaiMon.cond[0].cop2))
              if (lapProxPt === undefined) {
                lapProxPt = this.reconstruit(this.recupe(jaiMon.cond[0].cop2))
              } else {
                lapProxPt = lapProxPt[0]
              }
              const tagDD1 = this.newTag()
              const tagDD2 = this.newTag()
              const tagDD1PT = this.newTag()
              const tagDD2PT = this.newTag()
              this.mtgAppLecteur.addImPointRotation({
                o: lapProxSom,
                a: lapProxPt,
                x: Number(jaiMon.cond[0].formule),
                tag: tagDD1PT,
                hidden: true
              })
              this.mtgAppLecteur.addImPointRotation({
                o: lapProxSom,
                a: lapProxPt,
                x: -Number(jaiMon.cond[0].formule),
                tag: tagDD2PT,
                hidden: true
              })
              this.mtgAppLecteur.addRay({
                o: lapProxSom,
                a: tagDD1PT,
                tag: tagDD1,
                hidden: true
              })
              this.mtgAppLecteur.addRay({
                o: lapProxSom,
                a: tagDD2PT,
                tag: tagDD2,
                hidden: true
              })
              //
              if (jaiMon.cond.length > 1) {
                switch (jaiMon.cond[1].t) {
                  case 'ap': {
                    let lap = this.retrouveTag(this.recupe(jaiMon.cond[1].ki.nom))
                    if (lap === undefined) {
                      lap = [this.reconstruit(jaiMon.cond[1].ki.nom)]
                    }
                    const typlap = jaiMon.cond[1].ki.type
                    const result = []
                    switch (typlap) {
                      case 'segment':
                      case 'droite':
                      case 'demi-droite':
                      case 'demidroite': {
                        const tagInt = this.newTag()
                        const tagInt2 = this.newTag()
                        this.mtgAppLecteur.addIntLineLine({
                          d: lap[0],
                          d2: tagDD1,
                          tag: tagInt,
                          name: tagInt,
                          hidden: true
                        })
                        this.mtgAppLecteur.addIntLineLine({
                          d: lap[0],
                          d2: tagDD2,
                          tag: tagInt2,
                          name: tagInt2,
                          hidden: true
                        })
                        result.push(tagInt, tagInt2)
                      }
                        break
                      case 'cercle':
                        for (let i = 0; i < lap.length; i++) {
                          const tagInt = this.newTag()
                          const tagInt2 = this.newTag()
                          const tagInt3 = this.newTag()
                          const tagInt4 = this.newTag()
                          this.mtgAppLecteur.addIntLineCircle({
                            c: lap[i],
                            d: tagDD1,
                            tag: tagInt,
                            name: tagInt,
                            tag2: tagInt2,
                            name2: tagInt2,
                            hidden: true
                          })
                          this.mtgAppLecteur.addIntLineCircle({
                            c: lap[i],
                            d: tagDD2,
                            tag: tagInt3,
                            name: tagInt3,
                            tag2: tagInt4,
                            name2: tagInt4,
                            hidden: true
                          })
                          result.push(tagInt, tagInt2, tagInt3, tagInt4)
                        }
                        break
                    }
                    return this.finishReconstruit(jaiMon, this.choisPOint(result, jaiMon))
                  }
                  case 'long': {
                    // j'ai ap en 1 et long en 2
                    // tarce un cercle puis cherche pt intersec
                    let lapProx2 = this.retrouveTag(this.recupe(jaiMon.cond[1].cop))
                    if (lapProx2 === undefined) {
                      lapProx2 = this.reconstruit(this.recupe(jaiMon.cond[1].cop))
                    } else {
                      lapProx2 = lapProx2[0]
                    }
                    const didi2 = this.retrouveLaval(jaiMon.nom, 2)
                    const tagCC2 = this.newTag()
                    this.mtgAppLecteur.addCircleOr({
                      o: lapProx2,
                      r: Number(didi2) * this.unite,
                      tag: tagCC2,
                      hidden: true
                    })

                    const result = []
                    const tagInt = this.newTag()
                    const tagInt2 = this.newTag()
                    const tagInt3 = this.newTag()
                    const tagInt4 = this.newTag()
                    this.mtgAppLecteur.addIntLineCircle({
                      c: tagCC2,
                      d: tagDD1,
                      tag: tagInt,
                      name: tagInt,
                      tag2: tagInt2,
                      name2: tagInt2,
                      hidden: true
                    })
                    this.mtgAppLecteur.addIntLineCircle({
                      c: tagCC2,
                      d: tagDD2,
                      tag: tagInt3,
                      name: tagInt3,
                      tag2: tagInt4,
                      name2: tagInt4,
                      hidden: true
                    })
                    result.push(tagInt, tagInt2, tagInt3, tagInt4)
                    return this.finishReconstruit(jaiMon, this.choisPOint(result, jaiMon))
                    /// a mod
                  }
                  case 'ang': {
                    let lapProxSom2 = this.retrouveTag(this.recupe(jaiMon.cond[1].cop))
                    if (lapProxSom2 === undefined) {
                      lapProxSom2 = this.reconstruit(this.recupe(jaiMon.cond[1].cop))
                    } else {
                      lapProxSom2 = lapProxSom2[0]
                    }
                    let lapProxPt2 = this.retrouveTag(this.recupe(jaiMon.cond[1].cop2))
                    if (lapProxPt2 === undefined) {
                      lapProxPt2 = this.reconstruit(this.recupe(jaiMon.cond[1].cop2))
                    } else {
                      lapProxPt2 = lapProxPt2[0]
                    }
                    const tagDD12 = this.newTag()
                    const tagDD22 = this.newTag()
                    const tagDD1PT2 = this.newTag()
                    const tagDD2PT2 = this.newTag()
                    this.mtgAppLecteur.addImPointRotation({
                      o: lapProxSom2,
                      a: lapProxPt2,
                      x: Number(jaiMon.cond[1].formule),
                      tag: tagDD1PT2,
                      hidden: true
                    })
                    this.mtgAppLecteur.addImPointRotation({
                      o: lapProxSom2,
                      a: lapProxPt2,
                      x: -Number(jaiMon.cond[1].formule),
                      tag: tagDD2PT2,
                      hidden: true
                    })
                    this.mtgAppLecteur.addRay({
                      o: lapProxSom2,
                      a: tagDD1PT2,
                      tag: tagDD12,
                      hidden: true
                    })
                    this.mtgAppLecteur.addRay({
                      o: lapProxSom2,
                      a: tagDD2PT2,
                      tag: tagDD22,
                      hidden: true
                    })
                    const result = []
                    const tagInt = this.newTag()
                    const tagInt2 = this.newTag()
                    const tagInt3 = this.newTag()
                    const tagInt4 = this.newTag()
                    this.mtgAppLecteur.addIntLineLine({
                      d2: tagDD12,
                      d: tagDD1,
                      tag: tagInt,
                      name: tagInt,
                      hidden: true
                    })
                    this.mtgAppLecteur.addIntLineLine({
                      d2: tagDD22,
                      d: tagDD1,
                      tag: tagInt2,
                      name: tagInt2,
                      hidden: true
                    })
                    this.mtgAppLecteur.addIntLineLine({
                      d2: tagDD12,
                      d: tagDD2,
                      tag: tagInt3,
                      name: tagInt3,
                      hidden: true
                    })
                    this.mtgAppLecteur.addIntLineLine({
                      d2: tagDD22,
                      d: tagDD2,
                      tag: tagInt4,
                      name: tagInt4,
                      hidden: true
                    })
                    result.push(tagInt, tagInt2, tagInt3, tagInt4)
                    return this.finishReconstruit(jaiMon, this.choisPOint(result, jaiMon))
                  }
                }
              } else {
                const coPtCa3 = this.getPointPosition2(this.svgId, 'ptCa3')
                const coPtCa2 = this.getPointPosition2(this.svgId, 'ptCa2')
                // cherche un point sur tag
                let gtrouvUnPoint = false
                let tagLinkPointF
                for (let i = 1; i < 100; i++) {
                  const tagLinkPoint1 = this.newTag()
                  const tagLinkPoint2 = this.newTag()
                  const cooLink = { x: coPtCa2.x + i * (coPtCa3.x - coPtCa2.x) / 100, y: coPtCa2.y + i * (coPtCa3.y - coPtCa2.y) / 100 }
                  let gtrouvUnPoint1, gtrouvUnPoint2
                  try {
                    this.mtgAppLecteur.addLinkedPointLine({
                      c: tagDD1,
                      x: cooLink.x,
                      y: cooLink.y,
                      name: tagLinkPoint1,
                      tag: tagLinkPoint1,
                      hidden: true
                    })
                    if (this.placeOkPoint(this.getPointPosition2(this.svgId, tagLinkPoint1))) {
                      gtrouvUnPoint1 = true
                      break
                    } else {
                      gtrouvUnPoint1 = false
                    }
                  } catch (e) {
                    gtrouvUnPoint1 = false
                  }
                  try {
                    this.mtgAppLecteur.addLinkedPointLine({
                      c: tagDD2,
                      x: cooLink.x,
                      y: cooLink.y,
                      name: tagLinkPoint2,
                      tag: tagLinkPoint2,
                      hidden: true
                    })
                    if (this.placeOkPoint(this.getPointPosition2(this.svgId, tagLinkPoint2))) {
                      gtrouvUnPoint2 = true
                      break
                    } else {
                      gtrouvUnPoint2 = false
                    }
                  } catch (e) {
                    gtrouvUnPoint2 = false
                  }
                  gtrouvUnPoint = gtrouvUnPoint1 || gtrouvUnPoint2
                  if (gtrouvUnPoint) {
                    if (!gtrouvUnPoint1) {
                      tagLinkPointF = tagLinkPoint2
                      break
                    }
                    if (!gtrouvUnPoint2) {
                      tagLinkPointF = tagLinkPoint1
                      break
                    }
                    tagLinkPointF = (j3pGetRandomBool()) ? tagLinkPoint1 : tagLinkPoint2
                    break
                  }
                }
                return this.finishReconstruit(jaiMon, tagLinkPointF)
              }
              //
            }
              break
            case 'sur': return this.finishReconstruit(jaiMon, this.retrouveTag(this.recupe(jaiMon.cond[0].cop))[0])
          }
        } else {
          const pt = {}
          do {
            const x = j3pGetRandomInt(1, 99)
            const y = j3pGetRandomInt(1, 99)
            const pointDep = this.getPointPosition2(this.svgId, 'ptCa1')
            const pointDep2 = this.getPointPosition2(this.svgId, 'ptCa2')
            const pointDep3 = this.getPointPosition2(this.svgId, 'ptCa3')
            const vec1 = { x: pointDep2.x - pointDep.x, y: pointDep2.y - pointDep.y }
            const vec2 = { x: pointDep3.x - pointDep.x, y: pointDep3.y - pointDep.y }
            const vecX = { x: x * vec1.x, y: x * vec1.y }
            const vecY = { x: y * vec2.x, y: y * vec2.y }
            pt.x = pointDep.x + vecX.x + vecY.x
            pt.y = pointDep.y + vecX.y + vecY.y
          } while (!this.placeOkPoint(pt, false))
          const tagP = this.newTag()
          this.mtgAppLecteur.addFreePoint({
            tag: tagP,
            name: tagP,
            x: pt.x,
            y: pt.y,
            hidden: true
          })
          return this.finishReconstruit(jaiMon, tagP)
        }
        break
      case 'segment': {
        const tagARet = this.newTag()
        let lapProx2 = this.retrouveTag(this.recupe(jaiMon.pointExt1))
        if (lapProx2 === undefined) {
          lapProx2 = this.reconstruit(this.recupe(jaiMon.pointExt1))
        } else {
          lapProx2 = lapProx2[0]
        }
        let lapProx = this.retrouveTag(this.recupe(jaiMon.pointExt2))
        if (lapProx === undefined) {
          lapProx = this.reconstruit(this.recupe(jaiMon.pointExt2))
        } else {
          lapProx = lapProx[0]
        }
        this.mtgAppLecteur.addSegment({
          a: lapProx,
          b: lapProx2,
          tag: tagARet,
          hidden: true
        })
        return this.finishReconstruit(jaiMon, tagARet)
      }
      case 'droite': {
        const tagARet = this.newTag()
        let lapProx2 = this.retrouveTag(this.recupe(jaiMon.pass))
        if (lapProx2 === undefined) {
          if (!jaiMon.pass) {
            lapProx2 = this.retrouveTag(this.recupe(jaiMon.nom[1]))[0]
          } else {
            lapProx2 = this.reconstruit(this.recupe(jaiMon.pass))
          }
        } else {
          lapProx2 = lapProx2[0]
        }
        if (!jaiMon.pass) {
          const lapProx = this.retrouveTag(this.recupe(jaiMon.nom[2]))[0]
          this.mtgAppLecteur.addLineAB({
            a: lapProx,
            b: lapProx2,
            tag: tagARet,
            hidden: true
          })
          return this.finishReconstruit(jaiMon, tagARet)
        } else if (jaiMon.pass2) {
          let lapProx = this.retrouveTag(this.recupe(jaiMon.pass2))
          if (lapProx === undefined) {
            lapProx = this.reconstruit(this.recupe(jaiMon.pass2))
          } else {
            lapProx = lapProx[0]
          }
          this.mtgAppLecteur.addLineAB({
            a: lapProx,
            b: lapProx2,
            tag: tagARet,
            hidden: true
          })
          return this.finishReconstruit(jaiMon, tagARet)
        } else {
          let lap = this.retrouveTag(this.recupe(jaiMon.odt))
          if (lap === undefined) {
            lap = [this.reconstruit(this.recupe(jaiMon.odt))]
          }
          const tagARet = this.newTag()
          if (jaiMon.condd === 'parallèle' || jaiMon.spe === 'parallèle') {
            // paralle
            this.mtgAppLecteur.addLinePar({
              a: lapProx2,
              d: lap[0],
              tag: tagARet,
              hidden: true
            })
            return this.finishReconstruit(jaiMon, tagARet)
          } else {
            this.mtgAppLecteur.addLinePerp({
              a: lapProx2,
              d: lap[0],
              tag: tagARet,
              hidden: true
            })
            return this.finishReconstruit(jaiMon, tagARet)
          }
        }
      }
      case 'cercle':
        console.error('jaiMon', jaiMon)
        break
      case 'demi-droite':
      case 'demidroite': {
        if (jaiMon.origine) {
          const tagARet = this.newTag()
          let lapProx2 = this.retrouveTag(this.recupe(jaiMon.origine))
          if (lapProx2 === undefined) {
            lapProx2 = this.reconstruit(this.recupe(jaiMon.origine))
          } else {
            lapProx2 = lapProx2[0]
          }
          let lapProx = this.retrouveTag(this.recupe(jaiMon.point))
          if (lapProx === undefined) {
            lapProx = this.reconstruit(this.recupe(jaiMon.point))
          } else {
            lapProx = lapProx[0]
          }
          if (jaiMon.condDef === '2 points') {
            this.mtgAppLecteur.addRay({
              o: lapProx2,
              a: lapProx,
              tag: tagARet,
              hidden: true
            })
            return this.finishReconstruit(jaiMon, tagARet)
          } else {
            const tagRot = this.newTag()
            this.mtgAppLecteur.addImPointRotation({
              o: lapProx2,
              a: lapProx,
              x: Number(jaiMon.angle),
              tag: tagRot,
              name: tagRot,
              hidden: true
            })
            this.mtgAppLecteur.addRay({
              o: lapProx2,
              a: tagRot,
              tag: tagARet,
              hidden: true
            })
            return this.finishReconstruit(jaiMon, tagARet)
          }
        } else {
          // vieille def avec nom
          const tagARet = this.newTag()
          let lapProx2 = this.retrouveTag(this.recupe(jaiMon.nom)[1])
          if (lapProx2 === undefined) {
            lapProx2 = this.reconstruit(this.recupe(jaiMon.nom)[1])
          } else {
            lapProx2 = lapProx2[0]
          }
          let lapProx = this.retrouveTag(this.recupe(jaiMon.nom)[2])
          if (lapProx === undefined) {
            lapProx = this.reconstruit(this.recupe(jaiMon.nom)[2])
          } else {
            lapProx = lapProx[0]
          }
          this.mtgAppLecteur.addRay({
            o: lapProx2,
            a: lapProx,
            tag: tagARet,
            hidden: true
          })
          return this.finishReconstruit(jaiMon, tagARet)
        }
      }
    }
  }

  finishReconstruit (jaimon, tag) {
    this.lesReconstruits.push({ nom: this.recupe(jaimon.nom), tag: j3pClone(tag) })
    if (jaimon.type === 'point') {
      this.ajouteLongCo(this.recupe(jaimon.nom), tag)
    }
    return tag
  }

  ajouteLongCo (ki, tag) {
    const pt2 = this.getPointPosition2(this.svgId, tag)
    for (let i = 0; i < this.nomspris.length; i++) {
      if (this.typeDe(this.nomspris[i]) !== 'point') continue
      const pt1 = this.getPointPosition2(this.svgId, this.htmlPoint(this.nomspris[i]))
      this.leslongCoConst.push({
        nom1: this.nomspris[i] + ki,
        nom2: ki + this.nomspris[i],
        long: this.dist(pt1, pt2) / this.unite
      })
    }
    for (let i = 0; i < this.lesReconstruits.length; i++) {
      if (Array.isArray(this.lesReconstruits[i].tag)) continue
      const pt1 = this.getPointPosition2(this.svgId, this.lesReconstruits[i].tag)
      this.leslongCoConst.push({
        nom1: this.lesReconstruits[i].nom + ki,
        nom2: ki + this.lesReconstruits[i].nom,
        long: this.dist(pt1, pt2) / this.unite
      })
    }
  }

  choisPOint (result, jaiMon) {
    // vire les pas place ok
    if (result.length === 1) return result[0]
    for (let i = result.length - 1; i > -1; i--) {
      if (!this.placeOkPoint(this.getPointPosition2(this.svgId, result[i]), true)) {
        if (result.length === 1) {
          return result[i]
        } else {
          result.splice(i, 1)
        }
      }
    }
    // si yen a plusiseurs verif si ya prox ou pas
    if (result.length > 1) {
      if (jaiMon.prox) {
        let lapProx = this.retrouvePoint(this.recupe(jaiMon.prox))
        if (lapProx === undefined) {
          lapProx = this.reconstruit(jaiMon.prox)
        }
        const l1 = this.dist(this.getPointPosition2(this.svgId, result[0]), lapProx)
        const l2 = this.dist(this.getPointPosition2(this.svgId, result[1]), lapProx)
        if (l1 < l2) {
          return result[0]
        } else {
          return result[1]
        }
      }
      if (jaiMon.noprox) {
        let lapProx = this.retrouvePoint(this.recupe(jaiMon.noprox))
        if (lapProx === undefined) {
          lapProx = this.reconstruit(jaiMon.noprox)
        }
        const l1 = this.dist(this.getPointPosition2(this.svgId, result[0]), lapProx)
        const l2 = this.dist(this.getPointPosition2(this.svgId, result[1]), lapProx)
        if (l1 < l2) {
          return result[1]
        } else {
          return result[0]
        }
      }
    }
    return result[0]
  }

  retrouveLaval (ki, num) {
    let li = -1
    let gtrouv = false
    for (let i = 0; i < this.condVerif.length; i++) {
      if (this.recupe(ki) === this.condVerif[i].pour) {
        if (num === 1) {
          li = i
          break
        }
        if (num === 2) {
          if (gtrouv) {
            li = i
            break
          } else {
            gtrouv = true
          }
        }
      }
    }
    if (li === -1) {
      j3pNotify('pour Tom, pas trouvé laval')
      return 5
    }
    return this.calculFormule(this.condVerif[li].cb, true) / this.unite
  }

  ttouchemove () {
    if (this.larot) {
      this.rottout()
      return
    }
    if (!this.mtgAppLecteur.getDoc(this.svgId).pointCapture) return
    const tag = this.mtgAppLecteur.getDoc(this.svgId).pointCapture.tag
    switch (tag) {
      case 'pointlibre': this.touchMovePointLibre(); break
      case 'compt2': this.touchMoveCompt2(); break
      case '#pteqma0': this.touchMovePtEqma0(); break
      case 'raprap10': this.touchMoveRaprap10(); break
      case 'pottt': this.touchMovePttttt(); break
    }
  }

  touchMovePointLibre () {
    if (this.capRegle2) {
      // a vire pour dirige vers capregle
      const newg = this.getPointPosition2(this.svgId, '#pointlibre')
      const newx = newg.x
      const newy = newg.y
      const deltax = newx - this.x
      const deltay = newy - this.y
      this.colleRegle(deltax, deltay, newg.x, newg.y)
      return
    }
    if (this.capEquerre2) {
      const newg = this.getPointPosition2(this.svgId, '#pointlibre')
      const newx = newg.x
      const newy = newg.y
      const deltax = newx - this.x
      const deltay = newy - this.y
      this.colleEquerre(deltax, deltay)
      return
    }
    if (this.capComp2) {
      const newg = this.getPointPosition2(this.svgId, '#pointlibre')
      const newx = newg.x
      const newy = newg.y
      const deltax = newx - this.x
      const deltay = newy - this.y

      this.colleComp(deltax, deltay)
      return
    }

    let newg, newx, newy, deltax, deltay, l1
    if (this.capRap2) {
      newg = this.getPointPosition2(this.svgId, '#pointlibre')
      newx = newg.x
      newy = newg.y
      deltax = newx - this.x
      deltay = newy - this.y
      const pt = { x: this.baserap.x + deltax, y: this.baserap.y + deltay }
      const lp = []
      for (let i = 0; i < this.listePointPris.length; i++) {
        if (this.listePointPris[i].viz) {
          const pt2 = this.getPointPosition2(this.svgId, this.listePointPris[i].point)
          if (this.dist(pt2, pt) < this.unite / 2) {
            lp.push({
              co: pt2,
              dist: this.dist(pt2, pt)
            })
          }
        }
      }
      lp.sort(this.sortOnDistProp)
      if (lp.length > 0) {
        deltax = lp[0].co.x - this.baserap.x
        deltay = lp[0].co.y - this.baserap.y
      }
      this.mtgAppLecteur.setPointPosition(this.svgId, '#' + this.idPRap.pt, this.baserap.x + deltax, this.baserap.y + deltay, false)
      this.mtgAppLecteur.setPointPosition(this.svgId, '#raprap10', this.baserap2.x + deltax, this.baserap2.y + deltay, false)
      this.mtgAppLecteur.updateFigure(this.svgId)
    }
    if (this.captureFond) {
      newg = this.getPointPosition2(this.svgId, '#pointlibre')
      newx = newg.x
      newy = newg.y
      deltax = newx - this.x
      deltay = newy - this.y

      this.x = newx
      this.y = newy

      if (this.okmouv) {
        for (let i = this.ConsDemiDnb; i < this.demiDco.length; i++) {
          this.mtgAppLecteur.translatePoint(this.svgId, this.demiDco[i].html.point1, deltax, deltay, false)
          this.mtgAppLecteur.translatePoint(this.svgId, this.demiDco[i].html.point2, deltax, deltay, false)
        }
        for (let i = this.ConsSegnb; i < this.segCo.length; i++) {
          this.mtgAppLecteur.translatePoint(this.svgId, this.segCo[i].html.point1, deltax, deltay, false)
          this.mtgAppLecteur.translatePoint(this.svgId, this.segCo[i].html.point2, deltax, deltay, false)
        }
        for (let i = this.ConsDtenb; i < this.dteco.length; i++) {
          this.mtgAppLecteur.translatePoint(this.svgId, this.dteco[i].html.point1, deltax, deltay, false)
          this.mtgAppLecteur.translatePoint(this.svgId, this.dteco[i].html.point2, deltax, deltay, false)
        }
        for (let i = this.ConsArcnb; i < this.arcco.length; i++) {
          this.mtgAppLecteur.translatePoint(this.svgId, this.arcco[i].html.centre, deltax, deltay, false)
          this.mtgAppLecteur.translatePoint(this.svgId, this.arcco[i].html.p1, deltax, deltay, false)
          this.mtgAppLecteur.translatePoint(this.svgId, this.arcco[i].html.p2, deltax, deltay, false)
        }
        for (let i = this.ConsPtnb; i < this.ptCo.length; i++) {
          this.mtgAppLecteur.translatePoint(this.svgId, this.ptCo[i].html.point, deltax, deltay, false)
        }
        for (let i = 0; i < this.nomco.length; i++) {
          this.mtgAppLecteur.translatePoint(this.svgId, this.nomco[i].html[0], deltax, deltay, false)
        }
        if (this.regleVisible) {
          this.mtgAppLecteur.translatePoint(this.svgId, 'B', deltax, deltay, false)
          this.mtgAppLecteur.translatePoint(this.svgId, '#pttttt', deltax, deltay, false)
        }
        if (this.equerreVisible) {
          this.mtgAppLecteur.translatePoint(this.svgId, 'eq1', deltax, deltay, false)
          this.mtgAppLecteur.translatePoint(this.svgId, '#pteqma0', deltax, deltay, false)
        }
        if (this.rapporteurVisible) {
          this.mtgAppLecteur.translatePoint(this.svgId, 'rappt1', deltax, deltay, false)
          this.mtgAppLecteur.translatePoint(this.svgId, 'raprap10', deltax, deltay, false)
        }
        if (this.compasVisible) {
          this.mtgAppLecteur.translatePoint(this.svgId, 'compt1', deltax, deltay, false)
          this.mtgAppLecteur.translatePoint(this.svgId, 'compt2', deltax, deltay, false)
          this.mtgAppLecteur.translatePoint(this.svgId, 'ptverB', deltax, deltay, false)
        }
        this.mtgAppLecteur.translatePoint(this.svgId, 'Q', deltax, deltay, false)
        for (let i = 0; i < this.listeSegPris.length; i++) {
          this.mtgAppLecteur.translatePoint(this.svgId, this.listeSegPris[i].point1, deltax, deltay, false)
          this.mtgAppLecteur.translatePoint(this.svgId, this.listeSegPris[i].point2, deltax, deltay, false)
          if (this.listeSegPris[i].pointNom) this.mtgAppLecteur.translatePoint(this.svgId, this.listeSegPris[i].pointNom, deltax, deltay, false)
        }
        for (let i = 0; i < this.listeDtePris.length; i++) {
          this.mtgAppLecteur.translatePoint(this.svgId, this.listeDtePris[i].point1, deltax, deltay, false)
          this.mtgAppLecteur.translatePoint(this.svgId, this.listeDtePris[i].point2, deltax, deltay, false)
          if (this.listeDtePris[i].pointNom) this.mtgAppLecteur.translatePoint(this.svgId, this.listeDtePris[i].pointNom, deltax, deltay, false)
        }
        for (let i = 0; i < this.listeDemiDPris.length; i++) {
          this.mtgAppLecteur.translatePoint(this.svgId, this.listeDemiDPris[i].point1, deltax, deltay, false)
          this.mtgAppLecteur.translatePoint(this.svgId, this.listeDemiDPris[i].point2, deltax, deltay, false)
          if (this.listeDemiDPris[i].pointNom) this.mtgAppLecteur.translatePoint(this.svgId, this.listeDemiDPris[i].pointNom, deltax, deltay, false)
        }
        for (let i = 0; i < this.listeArcPris.length; i++) {
          this.mtgAppLecteur.translatePoint(this.svgId, this.listeArcPris[i].centre, deltax, deltay, false)
          this.mtgAppLecteur.translatePoint(this.svgId, this.listeArcPris[i].p1, deltax, deltay, false)
          this.mtgAppLecteur.translatePoint(this.svgId, this.listeArcPris[i].p2, deltax, deltay, false)
          this.mtgAppLecteur.translatePoint(this.svgId, this.listeArcPris[i].p3, deltax, deltay, false)
          this.mtgAppLecteur.translatePoint(this.svgId, this.listeArcPris[i].p4, deltax, deltay, false)
          this.mtgAppLecteur.translatePoint(this.svgId, this.listeArcPris[i].p5, deltax, deltay, false)
          this.mtgAppLecteur.translatePoint(this.svgId, this.listeArcPris[i].p6, deltax, deltay, false)
          this.mtgAppLecteur.translatePoint(this.svgId, this.listeArcPris[i].p7, deltax, deltay, false)
          this.mtgAppLecteur.translatePoint(this.svgId, this.listeArcPris[i].p8, deltax, deltay, false)
          this.mtgAppLecteur.translatePoint(this.svgId, this.listeArcPris[i].p9, deltax, deltay, false)
          this.mtgAppLecteur.translatePoint(this.svgId, this.listeArcPris[i].p10, deltax, deltay, false)
          this.mtgAppLecteur.translatePoint(this.svgId, this.listeArcPris[i].p11, deltax, deltay, false)
          this.mtgAppLecteur.translatePoint(this.svgId, this.listeArcPris[i].p12, deltax, deltay, false)
          if (this.listeArcPris[i].pointNom) this.mtgAppLecteur.translatePoint(this.svgId, this.listeArcPris[i].pointNom, deltax, deltay, false)
        }
        for (let i = 0; i < this.listePointPris.length; i++) {
          this.mtgAppLecteur.translatePoint(this.svgId, this.listePointPris[i].point, deltax, deltay, false)
        }

        this.mtgAppLecteur.translatePoint(this.svgId, '#ptCa1', deltax, deltay, false)
        this.mtgAppLecteur.translatePoint(this.svgId, '#ptCa2', deltax, deltay, false)
        this.mtgAppLecteur.updateFigure(this.svgId)
      }
      return
    } // this.captureFond
    if (this.lepoint22) {
      newg = this.getPointPosition2(this.svgId, '#pointlibre')
      this.mtgAppLecteur.setPointPosition(this.svgId, this.idImCrayPt, newg.x, newg.y - 40, true)
      if (this.regleVisible) {
        const df = this.mtgAppLecteur.valueOf(this.svgId, 'PtRegle')
        if (df !== -1) {
          if (this.lagrad) {
            this.setVisible2(this.svgId, '#regtt', true, true)
            this.setVisible2(this.svgId, '#laddregle', false, true)
            this.setVisible2(this.svgId, '#da', false, true)
            this.setVisible2(this.svgId, '#d1', false, true)
            this.setVisible2(this.svgId, '#d2', false, true)
            this.setVisible2(this.svgId, '#d3', false, true)
            this.setVisible2(this.svgId, '#d4', false, true)
            this.setVisible2(this.svgId, '#dz', false, true)
          } else {
            this.setVisible2(this.svgId, '#regtt', false, true)
            if (!this.bloquepoint) {
              this.setVisible2(this.svgId, '#laddregle', true, true)
              this.setVisible2(this.svgId, '#da', false, true)
              this.setVisible2(this.svgId, '#d1', false, true)
              this.setVisible2(this.svgId, '#d2', false, true)
              this.setVisible2(this.svgId, '#d3', false, true)
              this.setVisible2(this.svgId, '#d4', false, true)
              this.setVisible2(this.svgId, '#dz', false, true)
            } else {
              this.setVisible2(this.svgId, '#laddregle', false, true)
              const jk = newg
              const p1 = this.getPointPosition2(this.svgId, '#dpa')
              const p2 = this.getPointPosition2(this.svgId, '#dpz')
              const a = (p1.y - p2.y) / (p1.x - p2.x)
              const b = p1.y - a * p1.x
              const a2 = -1 / a
              const b2 = jk.y - a2 * jk.x
              const co = { x: (b2 - b) / (a - a2) }
              co.y = a * co.x + b
              if (p1.x === p2.x) {
                co.x = p1.x
                co.y = jk.y
              }
              if (p1.y === p2.y) {
                co.y = p1.y
                co.x = jk.x
              }
              if (a > 9999999 || a < -9999999) {
                co.x = p1.x
                co.y = jk.y
              } if (a < 0.001 && a > -0.001) {
                co.x = jk.x
                co.y = p1.y
              }
              this.allumelesbons(co.x, co.y)
            }
          }
        }
      }
      if (this.rapporteurVisible) {
        const df = this.mtgAppLecteur.valueOf(this.svgId, 'ptRap')
        if (df !== -1) {
          const newy = this.getPointPosition2(this.svgId, this.idPRap.pt)
          const GG = this.zonefig.getBoundingClientRect()
          const newx = Math.atan2(newy.x - (this.x - GG.x), (this.y - GG.y) - newy.y) * 180 / Math.PI
          this.mtgAppLecteur.giveFormula2(this.svgId, 'anglecray', -newx + 140, true)
          this.mtgAppLecteur.updateFigure(this.svgId)
        }
      }
      return
    } // this.lepoint22

    if (this.PtRegle2) {
      const newg = this.getPointPosition2(this.svgId, '#pointlibre')
      this.x = newg.x
      this.y = newg.y
      const pt = this.getPointPosition2(this.svgId, this.idPtRegle.ptCap2)
      this.mtgAppLecteur.setPointPosition(this.svgId, this.regtra[0], pt.x, pt.y, true)
      this.mtgAppLecteur.setPointPosition(this.svgId, this.idPtRegle.ptCap, this.x, this.y, true)
      this.mtgAppLecteur.updateFigure(this.svgId)
      return
    }
    if (this.PtCompS2) {
      newg = this.getPointPosition2(this.svgId, '#pointlibre')
      newx = newg.x
      newy = newg.y
      this.x = newx
      this.y = newy
      this.mtgAppLecteur.setPointPosition(this.svgId, '#compcoupt', this.x, this.y, true)
      let aaa = this.mtgAppLecteur.valueOf(this.svgId, 'compangleCC')
      let vd = this.mtgAppLecteur.valueOf(this.svgId, 'vd')
      let vi = this.mtgAppLecteur.valueOf(this.svgId, 'vi')
      if ((aaa > 0 && aaa < vd) || (aaa < 0 && aaa > vi)) {
        this.justav = undefined
        return
      }
      if (this.distangle(vd, aaa) < this.distangle(vi, aaa)) {
        if (aaa < 0) aaa += 360
        if (this.justav === 'vi' && (vd - vi > 340)) aaa = vi + 359.99
        vd = Math.max(vd, aaa)
        this.justav = 'vd'
      } else {
        if (aaa > 0) aaa -= 360
        if (this.justav === 'vd' && (vd - vi > 300)) aaa = vd - 359.99
        this.justav = 'vi'
        vi = Math.min(vi, aaa)
      }
      this.mtgAppLecteur.giveFormula2(this.svgId, 'vd', vd)
      this.mtgAppLecteur.giveFormula2(this.svgId, 'vi', vi)
      this.mtgAppLecteur.updateFigure(this.svgId)
      return
    }
    if (this.lepoint4) {
      newg = this.getPointPosition2(this.svgId, '#pointlibre')
      newx = newg.x
      newy = newg.y
      this.x = newx
      this.y = newy

      let lm = { x: this.x, y: this.y - 40 }
      this.mtgAppLecteur.getDoc(this.svgId).defaultCursor = 'none'
      this.vpin = 0
      this.vpi = false
      let yaregl = false

      let gtrouv = false
      let gpos = []
      // cherche dabord si ya deja un point
      for (let i = 0; i < this.listePointPris.length; i++) {
        if (this.listePointPris[i].viz) {
          l1 = this.getPointPosition2(this.svgId, this.listePointPris[i].point)
          if (this.dist(l1, lm) < this.distcol) {
            gtrouv = true
            this.vpin = i
            this.vpi = true
            gpos.push({
              t: 'pt',
              pt: l1,
              dist: this.dist(l1, lm)
            })
          }
        }
      }
      // cherche prox regle
      if (!gtrouv) {
        let co, a, b
        if (this.regleVisible) {
          const l1 = this.getPointPosition2(this.svgId, '#regreg0')
          const l2 = this.getPointPosition2(this.svgId, '#regreg2')
          let popos
          if (l2.x !== l1.x) {
            a = (l2.y - l1.y) / (l2.x - l1.x)
            b = l1.y - a * l1.x
            const a2 = -1 / a
            const b2 = lm.y - a2 * lm.x
            co = { x: (b2 - b) / (a - a2) }
            co.y = a * co.x + b
            popos = lm.x > Math.min(l1.x, l2.x) && lm.x < Math.max(l1.x, l2.x)
          } else {
            co = {
              x: l2.x,
              y: lm.y
            }
            popos = lm.y > Math.min(l1.y, l2.y) && lm.y < Math.max(l1.y, l2.y)
          }
          if (popos) {
            if (this.dist(co, lm) < this.distcol) {
              yaregl = true
              gpos.push({
                t: 'reg',
                a,
                b,
                x: l2.x,
                ang: this.regule(Math.atan2(l2.x - l1.x, l1.y - l2.y) * 180 / Math.PI),
                co: j3pClone(co),
                dist: this.dist(co, lm)
              })
            }
          }
        }
        /// cherche ici si pres de extremites ligne
        // seg
        for (let i = 0; i < this.listeSegPris.length; i++) {
          l1 = this.getPointPosition2(this.svgId, this.listeSegPris[i].point1)
          const l2 = this.getPointPosition2(this.svgId, this.listeSegPris[i].point2)
          let popos = false
          let a
          if (this.dist(l1, lm) < this.distcol) {
            gpos.push({
              t: 'pt',
              pt: j3pClone(l1),
              dist: this.dist(l1, lm)
            })
          } else if (this.dist(l2, lm) < this.distcol) {
            gpos.push({
              t: 'pt',
              pt: j3pClone(l2),
              dist: this.dist(l2, lm)
            })
          } else {
            if (l2.x !== l1.x) {
              a = (l2.y - l1.y) / (l2.x - l1.x)
              b = l1.y - a * l1.x
              let a2 = -1 / a
              if (a === 0) a2 = 99999999999
              const b2 = lm.y - a2 * lm.x
              co = { x: (b2 - b) / (a - a2) }
              co.y = a * co.x + b
              popos = lm.x > Math.min(l1.x, l2.x) && lm.x < Math.max(l1.x, l2.x)
            } else {
              co = {
                x: l2.x,
                y: lm.y
              }
              popos = lm.y > Math.min(l1.y, l2.y) && lm.y < Math.max(l1.y, l2.y)
            }
            if (popos) {
              if (this.dist(co, lm) < this.distcol) {
                gpos.push({
                  t: 'seg',
                  a,
                  b,
                  dist: this.dist(co, lm),
                  co: j3pClone(co)
                })
              }
            }
          }
        }
        /// droite
        for (let i = 0; i < this.listeDtePris.length; i++) {
          if (this.listeDtePris[i].viz === false) continue
          const l1 = this.getPointPosition2(this.svgId, this.listeDtePris[i].point1)
          const l2 = this.getPointPosition2(this.svgId, this.listeDtePris[i].point2)
          let a, co
          if (l2.x !== l1.x) {
            a = (l2.y - l1.y) / (l2.x - l1.x)
            const b = l1.y - a * l1.x
            const a2 = -1 / a
            const b2 = lm.y - a2 * lm.x
            co = { x: (b2 - b) / (a - a2) }
            co.y = a * co.x + b
          } else {
            co = {
              x: l2.x,
              y: lm.y
            }
          }
          if (this.dist(co, lm) < this.distcol) {
            gpos.push({
              t: 'seg',
              a,
              b,
              dist: this.dist(co, lm),
              co: j3pClone(co)
            })
          }
          // cherche ici si pres de ligne
        }
        // demi
        for (let i = 0; i < this.listeDemiDPris.length; i++) {
          const l1 = this.getPointPosition2(this.svgId, this.listeDemiDPris[i].point1)
          const l2 = this.getPointPosition2(this.svgId, this.listeDemiDPris[i].point2)
          let popos = false
          let a, co
          if (this.dist(l1, lm) < this.distcol) {
            gpos.push({
              t: 'pt',
              pt: j3pClone(l1),
              dist: this.dist(l1, lm)
            })
          } else {
            if (l2.x !== l1.x) {
              a = (l2.y - l1.y) / (l2.x - l1.x)
              b = l1.y - a * l1.x
              const a2 = -1 / a
              const b2 = lm.y - a2 * lm.x
              co = { x: (b2 - b) / (a - a2) }
              co.y = a * co.x + b
              popos = (lm.x > l1.x) === (l2.x > l1.x)
            } else {
              co = {
                x: l2.x,
                y: lm.y
              }
              popos = (lm.y > l1.y) === (l2.y > l1.y)
            }
            if (popos) {
              if (this.dist(co, lm) < this.distcol) {
                gpos.push({
                  t: 'seg',
                  a,
                  b,
                  dist: this.dist(co, lm),
                  co: j3pClone(co)
                })
              }
            }
            /// //////////////////
          }
          // cherche ici si pres de ligne
        }
        /// les arc
        for (let i = 0; i < this.listeArcPris.length; i++) {
          /// /////////////////*
          const l1 = this.getPointPosition2(this.svgId, this.listeArcPris[i].p1)
          const l2 = this.getPointPosition2(this.svgId, this.listeArcPris[i].p12)
          const l3 = this.getPointPosition2(this.svgId, this.listeArcPris[i].centre)
          if (l1.x !== l2.x || l1.y !== l2.y) {
            if (this.dist(l1, lm) < this.distcol) {
              gpos.push({
                t: 'pt',
                pt: j3pClone(l1),
                dist: this.dist(l1, lm)
              })
            } else if (this.dist(l2, lm) < this.distcol) {
              gpos.push({
                t: 'pt',
                pt: j3pClone(l2),
                dist: this.dist(l2, lm)
              })
            } else {
              let force = false
              const r = this.dist(l1, l3)
              let vpointM = -180 + Math.atan2(lm.x - l3.x, lm.y - l3.y) * 180 / Math.PI
              let vpoint1 = -180 + Math.atan2(l1.x - l3.x, l1.y - l3.y) * 180 / Math.PI
              let vpoint2 = -180 + Math.atan2(l2.x - l3.x, l2.y - l3.y) * 180 / Math.PI
              vpoint1 = this.regule(vpoint1)
              vpoint2 = this.regule(vpoint2)
              if (vpoint2 < vpoint1) vpoint2 += 360
              vpointM = this.regule(vpointM)
              if (this.dist(l1, l2) < 1) force = true
              if ((vpointM > vpoint1 && vpointM < vpoint2) || force) {
                const zi = Math.atan2(lm.x - l3.x, lm.y - l3.y) - Math.PI / 2
                co = { x: l3.x + r * Math.cos(zi) }
                co.y = l3.y - r * Math.sin(zi)
                if (this.dist(co, lm) < this.distcol) {
                  gpos.push({
                    t: 'c',
                    c: j3pClone(l3),
                    r,
                    co: j3pClone(co),
                    dist: this.dist(co, lm)
                  })
                }
              }
            }
          }
        }
      }

      if (this.vpi) {
        lm = gpos[0].pt
      } else {
        if (gpos.length === 1) {
          if (gpos[0].co !== undefined) {
            lm = gpos[0].co
          } else { lm = gpos[0].pt }
          if (gpos[0].ang !== undefined) this.mtgAppLecteur.giveFormula2(this.svgId, 'anglebal', -gpos[0].ang, true)
        }
        if (gpos.length > 1) {
          if (yaregl) {
            const poreg = j3pClone(gpos[0])
            for (let i = gpos.length - 1; i > -1; i--) {
              if (gpos[i].t === 'reg' || gpos[i].t === 'pt' || (gpos[i].t === 'seg' && (Math.abs(gpos[i].a - poreg.a) < 0.01))) gpos.splice(i, 1)
            }
            gpos.sort((a, b) => a.dist - b.dist)
            if (gpos.length > 0) {
              if (gpos[0].t === 'seg') {
                lm.x = (poreg.b - gpos[0].b) / (gpos[0].a - poreg.a)
                lm.y = poreg.a * lm.x + poreg.b
                if (this.dist(lm, gpos[0].co) > 6) lm = j3pClone(gpos[0].co)
              } else {
                const nnn = this.getInstersectingPoints(poreg.a, poreg.b, gpos[0].c.x, gpos[0].c.y, gpos[0].r)
                switch (nnn.length) {
                  case 0:
                    lm = j3pClone(poreg.co)
                    break
                  case 1:
                    lm = j3pClone(nnn[0])
                    break
                  case 2:
                    if (this.dist(nnn[0], lm) < this.dist(nnn[1], lm)) {
                      lm = j3pClone(nnn[0])
                    } else {
                      lm = j3pClone(nnn[1])
                    }
                }
              }
            } else {
              lm = j3pClone(poreg.co)
              this.mtgAppLecteur.giveFormula2(this.svgId, 'anglebal', -poreg.ang, true)
            }
          } else {
            gpos.sort((a, b) => a.dist - b.dist)
            if (gpos[0].t === 'pt') {
              lm = j3pClone(gpos[0].pt)
            } else {
              // on vire les élément pt (cf https://developer.mozilla.org/fr/docs/Web/JavaScript/Reference/Global_Objects/Array/filter)
              gpos = gpos.filter(elt => elt.t !== 'pt')
              let foverif = gpos.length > 1
              while (foverif) {
                foverif = false
                if (gpos[0].t === 'seg' && gpos[1].t === 'seg') {
                  if (Math.abs(gpos[0].a - gpos[1].a) < 0.01) {
                    gpos.splice(1, 1)
                    foverif = true
                  }
                }
                if (gpos[0].t === 'c' && gpos[1].t === 'c') {
                  if (this.dist(gpos[0].c, gpos[1].c) < 5) {
                    gpos.splice(1, 1)
                    foverif = true
                  }
                }
                if (gpos.length === 1) foverif = false
              }
              if (gpos.length === 1) {
                lm = j3pClone(gpos[0].co)
              } else {
                if (gpos[0].t === 'seg') {
                  if (gpos[1].t === 'seg') {
                    lm.x = (gpos[1].b - gpos[0].b) / (gpos[0].a - gpos[1].a)
                    lm.y = gpos[1].a * lm.x + gpos[1].b
                    if (this.dist(lm, gpos[0].co) > 6) lm = j3pClone(gpos[0].co)
                  } else {
                    let nnn
                    try {
                      nnn = this.getInstersectingPoints(gpos[0].a, gpos[0].b, gpos[1].c.x, gpos[1].c.y, gpos[1].r)
                    } catch (error) {
                      const dataSup = {
                        pbTom: {
                          gpos,
                          histoire: 'Il ne devrait y avoir que deux seg'
                        }
                      }
                      j3pNotify('Anomalie pour Tom', dataSup)
                    }
                    switch (nnn?.length) {
                      case 0:
                        lm = j3pClone(gpos[0].co)
                        break
                      case 1:
                        lm = j3pClone(nnn[0])
                        break
                      case 2:
                        if (this.dist(nnn[0], lm) < this.dist(nnn[1], lm)) {
                          lm = j3pClone(nnn[0])
                        } else {
                          lm = j3pClone(nnn[1])
                        }
                    }
                  }
                } else {
                  if (gpos[1].t === 'seg') {
                    const nnn = this.getInstersectingPoints(gpos[1].a, gpos[1].b, gpos[0].c.x, gpos[0].c.y, gpos[0].r)
                    switch (nnn.length) {
                      case 0:
                        lm = j3pClone(gpos[0].co)
                        break
                      case 1:
                        lm = j3pClone(nnn[0])
                        break
                      case 2:
                        if (this.dist(nnn[0], lm) < this.dist(nnn[1], lm)) {
                          lm = j3pClone(nnn[0])
                        } else {
                          lm = j3pClone(nnn[1])
                        }
                    }
                  } else {
                    const nnn = this.inter2cercles(gpos[0].c, gpos[0].r, gpos[1].c, gpos[1].r)
                    switch (nnn.length) {
                      case 0:
                        lm = j3pClone(gpos[0].co)
                        break
                      case 1:
                        lm = j3pClone(nnn[0])
                        break
                      case 2:
                        if (this.dist(nnn[0], lm) < this.dist(nnn[1], lm)) {
                          lm = j3pClone(nnn[0])
                        } else {
                          lm = j3pClone(nnn[1])
                        }
                    }
                  }
                }
              }
            }

            ///
            ///
          }
        }
      }

      this.mtgAppLecteur.setPointPosition(this.svgId, '#ptbalade', lm.x, lm.y, false)
      this.mtgAppLecteur.updateFigure(this.svgId)
    }
  } // touchMovePointLibre

  touchMoveCompt2 () {
    if (!this.verouV) {
      const { x, y } = this.getPointPosition2(this.svgId, '#pointVerC')
      this.mtgAppLecteur.setPointPosition(this.svgId, 'compt2', x, y, true)
      return
    }
    const l1 = this.getPointPosition2(this.svgId, '#compt3')
    const lp = []
    for (let i = 0; i < this.listePointPris.length; i++) {
      if (this.listePointPris[i].viz) {
        const pt = this.getPointPosition2(this.svgId, this.listePointPris[i].point)
        if (this.dist(l1, pt) < this.unite / 2) {
          lp.push({
            dist: this.dist(l1, pt),
            pt: j3pClone(pt)
          })
        }
      }
    }
    if (lp.length > 0) {
      const l3 = this.getPointPosition2(this.svgId, this.idCompPt.pointe)
      // const l2 = this.getPointPosition2(this.svgId, 'compt2')
      this.mtgAppLecteur.setPointPosition(this.svgId, '#compPLA2', l3.x, l3.y, true)
      this.mtgAppLecteur.setPointPosition(this.svgId, '#compPLA1', lp[0].pt.x, lp[0].pt.y, true)
      this.mtgAppLecteur.calculate(this.svgId)
      const { x, y } = this.getPointPosition2(this.svgId, '#compPLA3')
      this.mtgAppLecteur.setPointPosition(this.svgId, 'compt2', x, y, true)
    }
  } // touchMoveCompt2

  touchMovePtEqma0 () {
    this.fochgeq = false
    const l1 = this.getPointPosition2(this.svgId, this.idPtEquerre.pt)
    const pt4 = this.getPointPosition2(this.svgId, '#Eq234')
    for (let i = 0; i < this.listeDtePris.length; i++) {
      // test prox coin
      // test aver
      const pt2 = this.getPointPosition2(this.svgId, this.listeDtePris[i].point1)
      const pt3 = this.getPointPosition2(this.svgId, this.listeDtePris[i].point2)

      if (this.distPtDtebis(pt2, l1, pt3) < 0.1) {
        const aver = Math.atan2(pt2.x - pt3.x, pt3.y - pt2.y) * 180 / Math.PI
        const aver2 = Math.atan2(pt4.x - l1.x, l1.y - pt4.y) * 180 / Math.PI
        let aver3, mm
        if (aver > aver2 - 5 && aver < aver2 + 5) {
          aver3 = 45 - aver
          mm = true
        }
        if (aver > aver2 + 90 - 5 && aver < aver2 + 90 + 5) {
          aver3 = 45 - aver + 90
          mm = true
        }
        if (aver > aver2 - 90 - 5 && aver < aver2 - 90 + 5) {
          aver3 = 45 - aver - 90
          mm = true
        }
        if (aver > aver2 - 180 - 5 && aver < aver2 - 180 + 5) {
          aver3 = 45 - aver + 180
          mm = true
        }
        if (mm) {
          this.mtgAppLecteur.setPointPosition(this.svgId, '#pteqma0', l1.x + 219 * this.zoom * Math.cos((aver3) * Math.PI / 180), l1.y - 219 * this.zoom * Math.sin((aver3) * Math.PI / 180))
        }
        break
      }
    }
  } // touchMovePtEqma0

  touchMoveRaprap10 () {
    const l1 = this.getPointPosition2(this.svgId, this.idPRap.pt)
    const pt4 = this.getPointPosition2(this.svgId, '#ptreprap')
    for (let i = 0; i < this.listeDtePris.length; i++) {
      if (!this.listeDtePris[i].viz) continue
      const pt2 = this.getPointPosition2(this.svgId, this.listeDtePris[i].point1)
      const pt3 = this.getPointPosition2(this.svgId, this.listeDtePris[i].point2)
      if (this.distPtDtebis(pt2, l1, pt3) < 0.1) {
        const aver = Math.atan2(pt2.x - pt3.x, pt3.y - pt2.y) * 180 / Math.PI
        const aver2 = Math.atan2(pt4.x - l1.x, l1.y - pt4.y) * 180 / Math.PI
        let aver3, mm
        if (this.distangle(aver, aver2) < 5) {
          aver3 = -aver + 180
          mm = true
        }
        if (this.distangle(aver + 180, aver2) < 5) {
          aver3 = -aver
          mm = true
        }
        if (mm) {
          this.mtgAppLecteur.setPointPosition(this.svgId, 'raprap10', l1.x + 82 * this.zoom * Math.cos(aver3 * Math.PI / 180), l1.y - 82 * this.zoom * Math.sin(aver3 * Math.PI / 180))
        }
        break
      }
    }
    for (let i = 0; i < this.listeDemiDPris.length; i++) {
      if (!this.listeDemiDPris[i].viz) continue
      const pt2 = this.getPointPosition2(this.svgId, this.listeDemiDPris[i].point1)
      const pt3 = this.getPointPosition2(this.svgId, this.listeDemiDPris[i].point2)
      if (this.distPtDemiDtebis(pt2, l1, pt3) < 0.1) {
        const aver = Math.atan2(pt2.x - pt3.x, pt3.y - pt2.y) * 180 / Math.PI
        const aver2 = Math.atan2(pt4.x - l1.x, l1.y - pt4.y) * 180 / Math.PI
        let aver3, mm
        if (this.distangle(aver, aver2) < 5) {
          aver3 = -aver + 180
          mm = true
        }
        if (this.distangle(aver + 180, aver2) < 5) {
          aver3 = -aver
          mm = true
        }
        if (mm) {
          this.mtgAppLecteur.setPointPosition(this.svgId, 'raprap10', l1.x + 82 * this.zoom * Math.cos(aver3 * Math.PI / 180), l1.y - 82 * this.zoom * Math.sin(aver3 * Math.PI / 180))
        }
        break
      }
    }
    for (let i = 0; i < this.listeSegPris.length; i++) {
      if (!this.listeSegPris[i].viz) continue
      const pt2 = this.getPointPosition2(this.svgId, this.listeSegPris[i].point1)
      const pt3 = this.getPointPosition2(this.svgId, this.listeSegPris[i].point2)
      if (this.distPtSeg2(pt2, pt3, l1) < 0.1) {
        const aver = Math.atan2(pt2.x - pt3.x, pt3.y - pt2.y) * 180 / Math.PI
        const aver2 = Math.atan2(pt4.x - l1.x, l1.y - pt4.y) * 180 / Math.PI
        let aver3, mm
        if (this.distangle(aver, aver2) < 5) {
          aver3 = -aver + 180
          mm = true
        }
        if (this.distangle(aver + 180, aver2) < 5) {
          aver3 = -aver
          mm = true
        }
        if (mm) {
          this.mtgAppLecteur.setPointPosition(this.svgId, 'raprap10', l1.x + 82 * this.zoom * Math.cos(aver3 * Math.PI / 180), l1.y - 82 * this.zoom * Math.sin(aver3 * Math.PI / 180))
        }
        break
      }
    }
  } // touchMoveRaprap10

  touchMovePttttt () {
    const psou = this.getPointPosition2(this.svgId, '#pottt2')
    const psou2 = this.getPointPosition2(this.svgId, '#fghyu')
    this.mtgAppLecteur.setPointPosition(this.svgId, '#regpt1', psou.x, psou.y, true)
    this.mtgAppLecteur.setPointPosition(this.svgId, '#pttttt', psou2.x, psou2.y, true)
    for (let i = 0; i < this.listePointPris.length; i++) {
      if (this.listePointPris[i].viz) {
        if (i < this.ConsPtnb) {
          this.setColor2(this.svgId, this.listePointPris[i].point, 0, 0, 0, true)
        } else {
          this.setColor2(this.svgId, this.listePointPris[i].point, 0, 150, 0, true)
        }
      }
    }
  }

  centreDe (n) {
    for (let i = 0; i < this.listeArcPris.length; i++) {
      if (this.listeArcPris[i].nom === n) return this.listeArcPris[i].centre
    }
    return 'rien'
  }

  point1De (n) {
    for (let i = 0; i < this.listeDtePris.length; i++) {
      if (this.listeDtePris[i].nom === n) return this.listeDtePris[i].point1
    }
    return 'rien'
  }

  affDe (n) {
    for (let i = 0; i < this.listePointPris.length; i++) {
      if (this.listePointPris[i].nom === n) return this.listePointPris[i].aff
    }
    return 'rien'
  }

  typeDe (n) {
    for (let i = 0; i < this.listeDtePris.length; i++) {
      if (this.listeDtePris[i].nom === n) return 'droite'
    }
    for (let i = 0; i < this.listeDemiDPris.length; i++) {
      if (this.listeDemiDPris[i].nom === n) return 'demi-droite'
    }
    for (let i = 0; i < this.listeArcPris.length; i++) {
      if (this.listeArcPris[i].nom === n) return 'cercle'
    }
    for (let i = 0; i < this.listePointPris.length; i++) {
      if (this.listePointPris[i].nom === n) return 'point'
    }
    for (let i = 0; i < this.listeSegPris.length; i++) {
      if (this.listeSegPris[i].nom === n) return 'segment'
    }
    if (n.indexOf('zone') !== -1) return 'zone'
    if (n.indexOf('Zone') !== -1) return 'Zone'
    return 'rien'
  }

  mousemove (ev) {
    // les visibles pas toujours => FIXME (DC) je comprends pas ce commentaire
    if (this.onco) return
    if (this.lepoint3) {
      const newx = ev.clientX
      const newy = ev.clientY
      this.x = newx
      this.y = newy
      const GG = this.zonefig.getBoundingClientRect()
      this.mtgAppLecteur.setPointPosition(this.svgId, '#' + this.pointNom, this.x - GG.x, this.y - GG.y, true)
      this.acons = { x: this.x - GG.x, y: this.y - GG.y }
      return
    }

    let pt, pt2, lp, b2, co, a, a2, b
    if (this.mtgAppLecteur.getDoc(this.svgId).pointCapture) {
      if (this.larot) {
        this.rottout()
        return
      }
      if (this.mtgAppLecteur.getDoc(this.svgId).pointCapture.tag === 'compt2') {
        if (!this.verouV) {
          const l1 = this.getPointPosition2(this.svgId, '#pointVerC')
          this.mtgAppLecteur.setPointPosition(this.svgId, '#compt2', l1.x, l1.y, true)
          return
        }
        const l1 = this.getPointPosition2(this.svgId, '#compt3')
        lp = []
        for (let i = 0; i < this.listePointPris.length; i++) {
          if (this.listePointPris[i].viz) {
            pt = this.getPointPosition2(this.svgId, this.listePointPris[i].point)
            if (this.dist(l1, pt) < this.unite / 2) {
              lp.push({
                dist: this.dist(l1, pt),
                pt: j3pClone(pt)
              })
            }
          }
        }
        if (lp.length > 0) {
          const l3 = this.getPointPosition2(this.svgId, this.idCompPt.pointe)
          this.mtgAppLecteur.setPointPosition(this.svgId, '#compPLA2', l3.x, l3.y, true)
          this.mtgAppLecteur.setPointPosition(this.svgId, '#compPLA1', lp[0].pt.x, lp[0].pt.y, true)
          this.mtgAppLecteur.calculate(this.svgId)
          const { x, y } = this.getPointPosition2(this.svgId, '#compPLA3')
          this.mtgAppLecteur.setPointPosition(this.svgId, '#compt2', x, y, true)
          return
        }
      }

      if (this.mtgAppLecteur.getDoc(this.svgId).pointCapture.tag === 'pteqma0') {
        this.fochgeq = false
        const l1 = this.getPointPosition2(this.svgId, this.idPtEquerre.pt)
        const pt4 = this.getPointPosition2(this.svgId, '#Eq234')
        for (let i = 0; i < this.listeDtePris.length; i++) {
          // test prox coin
          // test aver
          const pt2 = this.getPointPosition2(this.svgId, this.listeDtePris[i].point1)
          const pt3 = this.getPointPosition2(this.svgId, this.listeDtePris[i].point2)

          if (this.distPtDtebis(pt2, l1, pt3) < 0.1) {
            const aver = Math.atan2(pt2.x - pt3.x, pt3.y - pt2.y) * 180 / Math.PI
            const aver2 = Math.atan2(pt4.x - l1.x, l1.y - pt4.y) * 180 / Math.PI
            let aver3, mm
            if (aver > aver2 - 5 && aver < aver2 + 5) {
              aver3 = 45 - aver
              mm = true
            }
            if (aver > aver2 + 90 - 5 && aver < aver2 + 90 + 5) {
              aver3 = 45 - aver + 90
              mm = true
            }
            if (aver > aver2 - 90 - 5 && aver < aver2 - 90 + 5) {
              aver3 = 45 - aver - 90
              mm = true
            }
            if (aver > aver2 - 180 - 5 && aver < aver2 - 180 + 5) {
              aver3 = 45 - aver + 180
              mm = true
            }
            if (mm) {
              this.mtgAppLecteur.setPointPosition(this.svgId, '#pteqma0', l1.x + 219 * this.zoom * Math.cos((aver3) * Math.PI / 180), l1.y - 219 * this.zoom * Math.sin((aver3) * Math.PI / 180))
            }
            break
          }
        }
      }
      let l1
      if (this.mtgAppLecteur.getDoc(this.svgId).pointCapture.tag === 'raprap10') {
        l1 = this.getPointPosition2(this.svgId, this.idPRap.pt)
        const pt4 = this.getPointPosition2(this.svgId, '#ptreprap')
        for (let i = 0; i < this.listeDtePris.length; i++) {
          if (!this.listeDtePris[i].viz) continue
          const pt2 = this.getPointPosition2(this.svgId, this.listeDtePris[i].point1)
          const pt3 = this.getPointPosition2(this.svgId, this.listeDtePris[i].point2)
          if (this.distPtDtebis(pt2, l1, pt3) < 0.1) {
            const aver = Math.atan2(pt2.x - pt3.x, pt3.y - pt2.y) * 180 / Math.PI
            const aver2 = Math.atan2(pt4.x - l1.x, l1.y - pt4.y) * 180 / Math.PI
            let aver3, mm
            if (this.distangle(aver, aver2) < 5) {
              aver3 = -aver + 180
              mm = true
            }
            if (this.distangle(aver + 180, aver2) < 5) {
              aver3 = -aver
              mm = true
            }
            if (mm) {
              this.mtgAppLecteur.setPointPosition(this.svgId, 'raprap10', l1.x + 82 * this.zoom * Math.cos(aver3 * Math.PI / 180), l1.y - 82 * this.zoom * Math.sin(aver3 * Math.PI / 180))
            }
            break
          }
        }
        for (let i = 0; i < this.listeDemiDPris.length; i++) {
          if (!this.listeDemiDPris[i].viz) continue
          const pt2 = this.getPointPosition2(this.svgId, this.listeDemiDPris[i].point1)
          const pt3 = this.getPointPosition2(this.svgId, this.listeDemiDPris[i].point2)
          if (this.distPtDemiDtebis(pt2, l1, pt3) < 0.1) {
            const aver = Math.atan2(pt2.x - pt3.x, pt3.y - pt2.y) * 180 / Math.PI
            const aver2 = Math.atan2(pt4.x - l1.x, l1.y - pt4.y) * 180 / Math.PI
            let aver3, mm
            if (this.distangle(aver, aver2) < 5) {
              aver3 = -aver + 180
              mm = true
            }
            if (this.distangle(aver + 180, aver2) < 5) {
              aver3 = -aver
              mm = true
            }
            if (mm) {
              this.mtgAppLecteur.setPointPosition(this.svgId, 'raprap10', l1.x + 82 * this.zoom * Math.cos(aver3 * Math.PI / 180), l1.y - 82 * this.zoom * Math.sin(aver3 * Math.PI / 180))
            }
            break
          }
        }
        for (let i = 0; i < this.listeSegPris.length; i++) {
          if (!this.listeSegPris[i].viz) continue
          const pt2 = this.getPointPosition2(this.svgId, this.listeSegPris[i].point1)
          const pt3 = this.getPointPosition2(this.svgId, this.listeSegPris[i].point2)
          if (this.distPtSeg2(pt2, pt3, l1) < 0.1) {
            const aver = Math.atan2(pt2.x - pt3.x, pt3.y - pt2.y) * 180 / Math.PI
            const aver2 = Math.atan2(pt4.x - l1.x, l1.y - pt4.y) * 180 / Math.PI
            let aver3, mm
            if (this.distangle(aver, aver2) < 5) {
              aver3 = -aver + 180
              mm = true
            }
            if (this.distangle(aver + 180, aver2) < 5) {
              aver3 = -aver
              mm = true
            }
            if (mm) {
              this.mtgAppLecteur.setPointPosition(this.svgId, 'raprap10', l1.x + 82 * this.zoom * Math.cos(aver3 * Math.PI / 180), l1.y - 82 * this.zoom * Math.sin(aver3 * Math.PI / 180))
            }
            break
          }
        }
      }
      if (this.mtgAppLecteur.getDoc(this.svgId).pointCapture.tag === 'pottt') {
        for (let i = 0; i < this.listePointPris.length; i++) {
          if (this.listePointPris[i].viz) {
            pt = this.getPointPosition2(this.svgId, this.listePointPris[i].point)
            if (i < this.ConsPtnb) {
              this.setColor2(this.svgId, this.listePointPris[i].point, 0, 0, 0, true)
            } else {
              this.setColor2(this.svgId, this.listePointPris[i].point, 0, 150, 0, true)
            }
          }
        }
        const psou = this.getPointPosition2(this.svgId, '#pottt2')
        const psou2 = this.getPointPosition2(this.svgId, '#fghyu')
        this.mtgAppLecteur.setPointPosition(this.svgId, '#regpt1', psou.x, psou.y, true)
        this.mtgAppLecteur.setPointPosition(this.svgId, '#pttttt', psou2.x, psou2.y, true)
      }
      this.viregommEtrot(true)
      return
    }
    if (this.PtRegle) {
      const newx = ev.clientX
      const newy = ev.clientY
      const deltax = newx - this.x
      const deltay = newy - this.y
      this.x = newx
      this.y = newy
      // pt = this.getPointPosition2(this.svgId, this.idPtRegle.ptCap2)
      // this.mtgAppLecteur.setPointPosition(this.svgId, this.SegRegle[1], pt.x, pt.y, true)
      pt = this.getPointPosition2(this.svgId, this.idPtRegle.ptCap2)
      this.mtgAppLecteur.setPointPosition(this.svgId, this.regtra[0], pt.x, pt.y, false)
      this.mtgAppLecteur.translatePoint(this.svgId, this.idPtRegle.ptCap, deltax, deltay, false)
      this.mtgAppLecteur.updateFigure(this.svgId)
      this.mtgAppLecteur.getDoc(this.svgId).defaultCursor = 'pointer'
      return
    }
    if (this.PtCompS) {
      const newx = ev.clientX
      const newy = ev.clientY
      const deltax = newx - this.x
      const deltay = newy - this.y
      this.x = newx
      this.y = newy
      this.mtgAppLecteur.translatePoint(this.svgId, '#compcoupt', deltax, deltay, true)
      let aaa = this.mtgAppLecteur.valueOf(this.svgId, 'compangleCC')
      let vd = this.mtgAppLecteur.valueOf(this.svgId, 'vd')
      let vi = this.mtgAppLecteur.valueOf(this.svgId, 'vi')
      if ((aaa > 0 && aaa < vd) || (aaa < 0 && aaa > vi)) {
        this.justav = undefined
        return
      }
      if (this.distangle(vd, aaa) < this.distangle(vi, aaa)) {
        if (aaa < 0) aaa += 360
        if (this.justav === 'vi' && (vd - vi > 340)) aaa = vi + 359.99
        vd = Math.max(vd, aaa)
        this.justav = 'vd'
      } else {
        if (aaa > 0) aaa -= 360
        if (this.justav === 'vd' && (vd - vi > 300)) aaa = vd - 359.99
        this.justav = 'vi'
        vi = Math.min(vi, aaa)
      }

      this.mtgAppLecteur.giveFormula2(this.svgId, 'vd', vd)
      this.mtgAppLecteur.giveFormula2(this.svgId, 'vi', vi)
      return
    }
    if (this.lepoint2) {
      const newx = ev.clientX
      const newy = ev.clientY
      this.x = newx
      this.y = newy
      const GG = this.zonefig.getBoundingClientRect()
      this.mtgAppLecteur.setPointPosition(this.svgId, this.idImCrayPt, this.x - GG.x, this.y - GG.y, true)
      this.mtgAppLecteur.getDoc(this.svgId).defaultCursor = 'none'
      if (this.regleVisible) {
        const df = this.mtgAppLecteur.valueOf(this.svgId, 'PtRegle')
        if (df !== -1) {
          if (this.lagrad) {
            this.setVisible2(this.svgId, '#regtt', true, true)
            this.setVisible2(this.svgId, '#laddregle', false, true)
            this.setVisible2(this.svgId, '#da', false, true)
            this.setVisible2(this.svgId, '#d1', false, true)
            this.setVisible2(this.svgId, '#d2', false, true)
            this.setVisible2(this.svgId, '#d3', false, true)
            this.setVisible2(this.svgId, '#d4', false, true)
            this.setVisible2(this.svgId, '#dz', false, true)
          } else {
            this.setVisible2(this.svgId, '#regtt', false, true)
            if (!this.bloquepoint) {
              this.setVisible2(this.svgId, '#laddregle', true, true)
              this.setVisible2(this.svgId, '#da', false, true)
              this.setVisible2(this.svgId, '#d1', false, true)
              this.setVisible2(this.svgId, '#d2', false, true)
              this.setVisible2(this.svgId, '#d3', false, true)
              this.setVisible2(this.svgId, '#d4', false, true)
              this.setVisible2(this.svgId, '#dz', false, true)
            } else {
              this.setVisible2(this.svgId, '#laddregle', false, true)
              const jk = { x: this.x - GG.x, y: this.y - GG.y }
              const p1 = this.getPointPosition2(this.svgId, '#dpa')
              const p2 = this.getPointPosition2(this.svgId, '#dpz')
              a = (p1.y - p2.y) / (p1.x - p2.x)
              b = p1.y - a * p1.x
              a2 = -1 / a
              b2 = jk.y - a2 * jk.x
              co = { x: (b2 - b) / (a - a2) }
              co.y = a * co.x + b
              if (p1.x === p2.x) {
                co.x = p1.x
                co.y = jk.y
              }
              if (p1.y === p2.y) {
                co.y = p1.y
                co.x = jk.x
              }
              if (a > 9999999 || a < -9999999) {
                co.x = p1.x
                co.y = jk.y
              } if (a < 0.001 && a > -0.001) {
                co.x = jk.x
                co.y = p1.y
              }
              this.allumelesbons(co.x, co.y)
            }
          }
        }
      }
      if (this.rapporteurVisible) {
        const df = this.mtgAppLecteur.valueOf(this.svgId, 'ptRap')
        if (df !== -1) {
          const newy = this.getPointPosition2(this.svgId, this.idPRap.pt)
          const newx = Math.atan2(newy.x - (this.x - GG.x), (this.y - GG.y) - newy.y) * 180 / Math.PI
          this.mtgAppLecteur.giveFormula2(this.svgId, 'anglecray', -newx + 140, true)
          this.mtgAppLecteur.updateFigure(this.svgId)
        }
      }
    }
    if (this.capcray) {
      const newx = ev.clientX
      const newy = ev.clientY
      const deltax = newx - this.x
      const deltay = newy - this.y
      this.x = newx
      this.y = newy
      this.mtgAppLecteur.translatePoint(this.svgId, this.idImCrayPt, deltax, deltay, true)
      return
    }

    let l1, l2
    if (this.lepoint) {
      const newx = ev.clientX
      const newy = ev.clientY
      this.x = newx
      this.y = newy

      const GG = this.zonefig.getBoundingClientRect()
      // GG.x = GG.x - me.zonesElts.MG.scrollLeft
      // GG.y = GG.y - me.zonesElts.MG.scrollTop
      let lm = { x: this.x - GG.x, y: this.y - GG.y }
      this.mtgAppLecteur.getDoc(this.svgId).defaultCursor = 'none'
      this.vpin = 0
      this.vpi = false
      let yaregl = false

      let gtrouv = false
      let gpos = []
      // cherche dabord si ya deja un point
      for (let i = 0; i < this.listePointPris.length; i++) {
        if (this.listePointPris[i].viz) {
          l1 = this.getPointPosition2(this.svgId, this.listePointPris[i].point)
          if (this.dist(l1, lm) < this.distcol) {
            gtrouv = true
            this.vpin = i
            this.vpi = true
            gpos.push({
              t: 'pt',
              pt: l1,
              dist: this.dist(l1, lm)
            })
          }
        }
      }
      // cherche prox regle
      if (!gtrouv) {
        let co
        if (this.regleVisible) {
          l1 = this.getPointPosition2(this.svgId, '#regreg0')
          l2 = this.getPointPosition2(this.svgId, '#regreg2')
          let popos
          if (l2.x !== l1.x) {
            a = (l2.y - l1.y) / (l2.x - l1.x)
            b = l1.y - a * l1.x
            a2 = -1 / a
            b2 = lm.y - a2 * lm.x
            co = { x: (b2 - b) / (a - a2) }
            co.y = a * co.x + b
            popos = lm.x > Math.min(l1.x, l2.x) && lm.x < Math.max(l1.x, l2.x)
          } else {
            co = {
              x: l2.x,
              y: lm.y
            }
            popos = lm.y > Math.min(l1.y, l2.y) && lm.y < Math.max(l1.y, l2.y)
          }
          if (popos) {
            if (this.dist(co, lm) < this.distcol) {
              yaregl = true
              gpos.push({
                t: 'reg',
                a,
                b,
                x: l2.x,
                ang: this.regule(Math.atan2(l2.x - l1.x, l1.y - l2.y) * 180 / Math.PI),
                co: j3pClone(co),
                dist: this.dist(co, lm)
              })
            }
          }
        }
        /// cherche ici si pres de extremites ligne
        // seg
        for (let i = 0; i < this.listeSegPris.length; i++) {
          l1 = this.getPointPosition2(this.svgId, this.listeSegPris[i].point1)
          l2 = this.getPointPosition2(this.svgId, this.listeSegPris[i].point2)
          let a
          let popos = false
          if (this.dist(l1, lm) < this.distcol) {
            gpos.push({
              t: 'pt',
              pt: j3pClone(l1),
              dist: this.dist(l1, lm)
            })
          } else if (this.dist(l2, lm) < this.distcol) {
            gpos.push({
              t: 'pt',
              pt: j3pClone(l2),
              dist: this.dist(l2, lm)
            })
          } else {
            if (l2.x !== l1.x) {
              a = (l2.y - l1.y) / (l2.x - l1.x)
              b = l1.y - a * l1.x
              a2 = -1 / a
              if (a === 0) a2 = 99999999999
              b2 = lm.y - a2 * lm.x
              co = { x: (b2 - b) / (a - a2) }
              co.y = a * co.x + b
              popos = lm.x > Math.min(l1.x, l2.x) && lm.x < Math.max(l1.x, l2.x)
            } else {
              co = {
                x: l2.x,
                y: lm.y
              }
              a = Infinity
              popos = lm.y > Math.min(l1.y, l2.y) && lm.y < Math.max(l1.y, l2.y)
            }
            if (popos) {
              if (this.dist(co, lm) < this.distcol) {
                gpos.push({
                  t: 'seg',
                  a,
                  b,
                  dist: this.dist(co, lm),
                  co: j3pClone(co)
                })
              }
            }
          }
        }
        /// droite
        for (let i = 0; i < this.listeDtePris.length; i++) {
          if (this.listeDtePris[i].viz === false) continue
          l1 = this.getPointPosition2(this.svgId, this.listeDtePris[i].point1)
          l2 = this.getPointPosition2(this.svgId, this.listeDtePris[i].point2)
          a = undefined
          if (l2.x !== l1.x) {
            a = (l2.y - l1.y) / (l2.x - l1.x)
            b = l1.y - a * l1.x
            a2 = -1 / a
            b2 = lm.y - a2 * lm.x
            co = { x: (b2 - b) / (a - a2) }
            co.y = a * co.x + b
          } else {
            co = {
              x: l2.x,
              y: lm.y
            }
            a = Infinity
          }
          if (this.dist(co, lm) < this.distcol) {
            gpos.push({
              t: 'seg',
              a,
              b,
              dist: this.dist(co, lm),
              co: j3pClone(co)
            })
          }
          // cherche ici si pres de ligne
        }
        // demi
        for (let i = 0; i < this.listeDemiDPris.length; i++) {
          l1 = this.getPointPosition2(this.svgId, this.listeDemiDPris[i].point1)
          l2 = this.getPointPosition2(this.svgId, this.listeDemiDPris[i].point2)
          let popos = false
          let a
          if (this.dist(l1, lm) < this.distcol) {
            gpos.push({
              t: 'pt',
              pt: j3pClone(l1),
              dist: this.dist(l1, lm)
            })
          } else {
            if (l2.x !== l1.x) {
              a = (l2.y - l1.y) / (l2.x - l1.x)
              b = l1.y - a * l1.x
              a2 = -1 / a
              b2 = lm.y - a2 * lm.x
              co = { x: (b2 - b) / (a - a2) }
              co.y = a * co.x + b
              popos = (lm.x > l1.x) === (l2.x > l1.x)
            } else {
              co = {
                x: l2.x,
                y: lm.y
              }
              a = Infinity
              popos = (lm.y > l1.y) === (l2.y > l1.y)
            }
            if (popos) {
              if (this.dist(co, lm) < this.distcol) {
                gpos.push({
                  t: 'seg',
                  a,
                  b,
                  dist: this.dist(co, lm),
                  co: j3pClone(co)
                })
              }
            }
          }
          // cherche ici si pres de ligne
        }
        /// les arc
        for (let i = 0; i < this.listeArcPris.length; i++) {
          l1 = this.getPointPosition2(this.svgId, this.listeArcPris[i].p1)
          l2 = this.getPointPosition2(this.svgId, this.listeArcPris[i].p12)
          const l3 = this.getPointPosition2(this.svgId, this.listeArcPris[i].centre)
          if (l1.x !== l2.x || l1.y !== l2.y) {
            if (this.dist(l1, lm) < this.distcol) {
              gpos.push({
                t: 'pt',
                pt: j3pClone(l1),
                dist: this.dist(l1, lm)
              })
            } else if (this.dist(l2, lm) < this.distcol) {
              gpos.push({
                t: 'pt',
                pt: j3pClone(l2),
                dist: this.dist(l2, lm)
              })
            } else {
              const force = this.dist(l1, l2) < 1
              const r = this.dist(l1, l3)
              let vpointM = -180 + Math.atan2(lm.x - l3.x, lm.y - l3.y) * 180 / Math.PI
              let vpoint1 = -180 + Math.atan2(l1.x - l3.x, l1.y - l3.y) * 180 / Math.PI
              let vpoint2 = -180 + Math.atan2(l2.x - l3.x, l2.y - l3.y) * 180 / Math.PI
              vpoint1 = this.regule(vpoint1)
              vpoint2 = this.regule(vpoint2)
              if (vpoint2 < vpoint1) vpoint2 += 360
              vpointM = this.regule(vpointM)
              if (vpointM < vpoint1) vpointM += 360
              if ((vpointM > vpoint1 && vpointM < vpoint2) || force) {
                const zi = Math.atan2(lm.x - l3.x, lm.y - l3.y) - Math.PI / 2
                co = { x: l3.x + r * Math.cos(zi) }
                co.y = l3.y - r * Math.sin(zi)
                if (this.dist(co, lm) < this.distcol) {
                  gpos.push({
                    t: 'c',
                    c: j3pClone(l3),
                    r,
                    co: j3pClone(co),
                    dist: this.dist(co, lm)
                  })
                }
              }
            }
          }
        }
      }

      if (this.vpi) {
        lm = gpos[0].pt
      } else {
        if (gpos.length === 1) {
          if (gpos[0].co !== undefined) {
            lm = gpos[0].co
          } else { lm = gpos[0].pt }
          if (gpos[0].ang !== undefined) this.mtgAppLecteur.giveFormula2(this.svgId, 'anglebal', -gpos[0].ang, true)
        }
        if (gpos.length > 1) {
          if (yaregl) {
            const poreg = j3pClone(gpos[0])
            for (let i = gpos.length - 1; i > -1; i--) {
              if (gpos[i].t === 'reg' || gpos[i].t === 'pt' || (gpos[i].t === 'seg' && (Math.abs(gpos[i].a - poreg.a) < 0.01))) gpos.splice(i, 1)
            }
            gpos.sort(this.sortOnDistProp)
            if (gpos.length > 0) {
              if (gpos[0].t === 'seg') {
                lm.x = (poreg.b - gpos[0].b) / (gpos[0].a - poreg.a)
                lm.y = poreg.a * lm.x + poreg.b
                if (Math.abs(gpos[0].a) > 99999999) {
                  lm.x = gpos[0].co.x
                  lm.y = poreg.a * lm.x + poreg.b
                }
                if (Math.abs(poreg.a) > 99999999) {
                  lm.x = poreg.co.x
                  lm.y = gpos[0].a * lm.x + gpos[0].b
                }
                if (this.dist(lm, gpos[0].co) > 6) lm = j3pClone(gpos[0].co)
              } else {
                const nnn = this.getInstersectingPoints(poreg.a, poreg.b, gpos[0].c.x, gpos[0].c.y, gpos[0].r)
                switch (nnn.length) {
                  case 0:
                    lm = j3pClone(poreg.co)
                    break
                  case 1:
                    lm = j3pClone(nnn[0])
                    break
                  case 2:
                    if (this.dist(nnn[0], lm) < this.dist(nnn[1], lm)) {
                      lm = j3pClone(nnn[0])
                    } else {
                      lm = j3pClone(nnn[1])
                    }
                }
              }
            } else {
              lm = j3pClone(poreg.co)
              this.mtgAppLecteur.giveFormula2(this.svgId, 'anglebal', -poreg.ang, true)
            }
          } else {
            gpos.sort(this.sortOnDistProp)
            if (gpos[0].t === 'pt') {
              lm = j3pClone(gpos[0].pt)
            } else {
              // on vire les élément pt (cf https://developer.mozilla.org/fr/docs/Web/JavaScript/Reference/Global_Objects/Array/filter)
              gpos = gpos.filter(elt => elt.t !== 'pt')
              let foverif = gpos.length > 1
              while (foverif) {
                foverif = false
                if (gpos[0].t === 'seg' && gpos[1].t === 'seg') {
                  if (Math.abs(gpos[0].a - gpos[1].a) < 0.01) {
                    gpos.splice(1, 1)
                    foverif = true
                  }
                }
                if (gpos[0].t === 'c' && gpos[1].t === 'c') {
                  if (this.dist(gpos[0].c, gpos[1].c) < 5) {
                    gpos.splice(1, 1)
                    foverif = true
                  }
                }
                if (gpos.length === 1) foverif = false
              }
              if (gpos.length === 1) {
                lm = j3pClone(gpos[0].co)
              } else {
                if (gpos[0].t === 'seg') {
                  if (gpos[1].t === 'seg') {
                    lm.x = (gpos[1].b - gpos[0].b) / (gpos[0].a - gpos[1].a)
                    lm.y = gpos[1].a * lm.x + gpos[1].b
                    if (Math.abs(gpos[0].a) > 99999999) {
                      lm.x = gpos[0].co.x
                      lm.y = gpos[1].a * lm.x + gpos[1].b
                    }
                    if (Math.abs(gpos[1].a) > 99999999) {
                      lm.x = gpos[1].co.x
                      lm.y = gpos[0].a * lm.x + gpos[0].b
                    }
                    if (this.dist(lm, gpos[0].co) > 10) lm = j3pClone(gpos[0].co)
                  } else {
                    const nnn = this.getInstersectingPoints(gpos[0].a, gpos[0].b, gpos[1].c.x, gpos[1].c.y, gpos[1].r)
                    switch (nnn.length) {
                      case 0:
                        lm = j3pClone(gpos[0].co)
                        break
                      case 1:
                        lm = j3pClone(nnn[0])
                        break
                      case 2:
                        if (this.dist(nnn[0], lm) < this.dist(nnn[1], lm)) {
                          lm = j3pClone(nnn[0])
                        } else {
                          lm = j3pClone(nnn[1])
                        }
                    }
                  }
                } else {
                  if (gpos[1].t === 'seg') {
                    const nnn = this.getInstersectingPoints(gpos[1].a, gpos[1].b, gpos[0].c.x, gpos[0].c.y, gpos[0].r)
                    switch (nnn.length) {
                      case 0:
                        lm = j3pClone(gpos[0].co)
                        break
                      case 1:
                        lm = j3pClone(nnn[0])
                        break
                      case 2:
                        if (this.dist(nnn[0], lm) < this.dist(nnn[1], lm)) {
                          lm = j3pClone(nnn[0])
                        } else {
                          lm = j3pClone(nnn[1])
                        }
                    }
                  } else {
                    const nnn = this.inter2cercles(gpos[0].c, gpos[0].r, gpos[1].c, gpos[1].r)
                    switch (nnn.length) {
                      case 0:
                        lm = j3pClone(gpos[0].co)
                        break
                      case 1:
                        lm = j3pClone(nnn[0])
                        break
                      case 2:
                        if (this.dist(nnn[0], lm) < this.dist(nnn[1], lm)) {
                          lm = j3pClone(nnn[0])
                        } else {
                          lm = j3pClone(nnn[1])
                        }
                    }
                  }
                }
              }
            }
          }
        }
      }

      this.mtgAppLecteur.setPointPosition(this.svgId, '#ptbalade', lm.x, lm.y, false)
      this.mtgAppLecteur.updateFigure(this.svgId)
    }
    if (ev.buttons !== 1) {
      this.captureFond = this.capcray = this.capRegle = this.capRegle2 = this.capEquerre2 = this.capEquer = this.capComp = this.capComp2 = this.capRap = this.capRap2 = this.PtRegle = this.PtRegle2 = this.PtCompS = this.PtCompS2 = false
      return
    }
    if (this.captureFond) {
      const newx = ev.clientX
      const newy = ev.clientY
      const deltax = newx - this.x
      const deltay = newy - this.y
      this.x = newx
      this.y = newy

      if (this.okmouv) {
        for (let i = this.ConsDemiDnb; i < this.demiDco.length; i++) {
          this.mtgAppLecteur.translatePoint(this.svgId, this.demiDco[i].html.point1, deltax, deltay, false)
          this.mtgAppLecteur.translatePoint(this.svgId, this.demiDco[i].html.point2, deltax, deltay, false)
        }
        for (let i = this.ConsSegnb; i < this.segCo.length; i++) {
          this.mtgAppLecteur.translatePoint(this.svgId, this.segCo[i].html.point1, deltax, deltay, false)
          this.mtgAppLecteur.translatePoint(this.svgId, this.segCo[i].html.point2, deltax, deltay, false)
        }
        for (let i = this.ConsDtenb; i < this.dteco.length; i++) {
          this.mtgAppLecteur.translatePoint(this.svgId, this.dteco[i].html.point1, deltax, deltay, false)
          this.mtgAppLecteur.translatePoint(this.svgId, this.dteco[i].html.point2, deltax, deltay, false)
        }
        for (let i = this.ConsArcnb; i < this.arcco.length; i++) {
          this.mtgAppLecteur.translatePoint(this.svgId, this.arcco[i].html.centre, deltax, deltay, false)
          this.mtgAppLecteur.translatePoint(this.svgId, this.arcco[i].html.p1, deltax, deltay, false)
          this.mtgAppLecteur.translatePoint(this.svgId, this.arcco[i].html.p2, deltax, deltay, false)
        }
        for (let i = this.ConsPtnb; i < this.ptCo.length; i++) {
          this.mtgAppLecteur.translatePoint(this.svgId, this.ptCo[i].html.point, deltax, deltay, false)
        }
        for (let i = 0; i < this.nomco.length; i++) {
          this.mtgAppLecteur.translatePoint(this.svgId, this.nomco[i].html[0], deltax, deltay, false)
        }
        if (this.regleVisible) {
          this.mtgAppLecteur.translatePoint(this.svgId, 'B', deltax, deltay, false)
          this.mtgAppLecteur.translatePoint(this.svgId, '#pttttt', deltax, deltay, false)
        }
        if (this.equerreVisible) {
          this.mtgAppLecteur.translatePoint(this.svgId, '#eq1', deltax, deltay, false)
          this.mtgAppLecteur.translatePoint(this.svgId, '#pteqma0', deltax, deltay, false)
        }
        if (this.rapporteurVisible) {
          this.mtgAppLecteur.translatePoint(this.svgId, '#rappt1', deltax, deltay, false)
          this.mtgAppLecteur.translatePoint(this.svgId, '#raprap10', deltax, deltay, false)
        }
        if (this.compasVisible) {
          this.mtgAppLecteur.translatePoint(this.svgId, '#compt1', deltax, deltay, false)
          this.mtgAppLecteur.translatePoint(this.svgId, '#compt2', deltax, deltay, false)
          this.mtgAppLecteur.translatePoint(this.svgId, '#ptverB', deltax, deltay, false)
        }
        this.mtgAppLecteur.translatePoint(this.svgId, 'Q', deltax, deltay, false)
        for (let i = 0; i < this.listeSegPris.length; i++) {
          this.mtgAppLecteur.translatePoint(this.svgId, this.listeSegPris[i].point1, deltax, deltay, false)
          this.mtgAppLecteur.translatePoint(this.svgId, this.listeSegPris[i].point2, deltax, deltay, false)
          if (this.listeSegPris[i].pointNom) this.mtgAppLecteur.translatePoint(this.svgId, this.listeSegPris[i].pointNom, deltax, deltay, false)
        }
        for (let i = 0; i < this.listeDtePris.length; i++) {
          this.mtgAppLecteur.translatePoint(this.svgId, this.listeDtePris[i].point1, deltax, deltay, false)
          this.mtgAppLecteur.translatePoint(this.svgId, this.listeDtePris[i].point2, deltax, deltay, false)
          if (this.listeDtePris[i].pointNom) this.mtgAppLecteur.translatePoint(this.svgId, this.listeDtePris[i].pointNom, deltax, deltay, false)
        }
        for (let i = 0; i < this.listeDemiDPris.length; i++) {
          this.mtgAppLecteur.translatePoint(this.svgId, this.listeDemiDPris[i].point1, deltax, deltay, false)
          this.mtgAppLecteur.translatePoint(this.svgId, this.listeDemiDPris[i].point2, deltax, deltay, false)
          if (this.listeDemiDPris[i].pointNom) this.mtgAppLecteur.translatePoint(this.svgId, this.listeDemiDPris[i].pointNom, deltax, deltay, false)
        }
        for (let i = 0; i < this.listeArcPris.length; i++) {
          this.mtgAppLecteur.translatePoint(this.svgId, this.listeArcPris[i].centre, deltax, deltay, false)
          this.mtgAppLecteur.translatePoint(this.svgId, this.listeArcPris[i].p1, deltax, deltay, false)
          this.mtgAppLecteur.translatePoint(this.svgId, this.listeArcPris[i].p2, deltax, deltay, false)
          this.mtgAppLecteur.translatePoint(this.svgId, this.listeArcPris[i].p3, deltax, deltay, false)
          this.mtgAppLecteur.translatePoint(this.svgId, this.listeArcPris[i].p4, deltax, deltay, false)
          this.mtgAppLecteur.translatePoint(this.svgId, this.listeArcPris[i].p5, deltax, deltay, false)
          this.mtgAppLecteur.translatePoint(this.svgId, this.listeArcPris[i].p6, deltax, deltay, false)
          this.mtgAppLecteur.translatePoint(this.svgId, this.listeArcPris[i].p7, deltax, deltay, false)
          this.mtgAppLecteur.translatePoint(this.svgId, this.listeArcPris[i].p8, deltax, deltay, false)
          this.mtgAppLecteur.translatePoint(this.svgId, this.listeArcPris[i].p9, deltax, deltay, false)
          this.mtgAppLecteur.translatePoint(this.svgId, this.listeArcPris[i].p10, deltax, deltay, false)
          this.mtgAppLecteur.translatePoint(this.svgId, this.listeArcPris[i].p11, deltax, deltay, false)
          this.mtgAppLecteur.translatePoint(this.svgId, this.listeArcPris[i].p12, deltax, deltay, false)
          if (this.listeArcPris[i].pointNom) this.mtgAppLecteur.translatePoint(this.svgId, this.listeArcPris[i].pointNom, deltax, deltay, false)
        }
        for (let i = 0; i < this.listePointPris.length; i++) {
          this.mtgAppLecteur.translatePoint(this.svgId, this.listePointPris[i].point, deltax, deltay, false)
        }

        this.mtgAppLecteur.translatePoint(this.svgId, '#ptCa1', deltax, deltay, false)
        this.mtgAppLecteur.translatePoint(this.svgId, '#ptCa2', deltax, deltay, false)
        this.mtgAppLecteur.updateFigure(this.svgId)
      }
      return
    }
    if (this.capRegle) {
      const newx = ev.clientX
      const newy = ev.clientY
      const deltax = newx - this.x
      const deltay = newy - this.y
      this.colleRegle(deltax, deltay, newx, newy)
      this.mtgAppLecteur.getDoc(this.svgId).defaultCursor = 'pointer'
      return
    }
    if (this.capEquer) {
      const newx = ev.clientX
      const newy = ev.clientY
      const deltax = newx - this.x
      const deltay = newy - this.y
      this.colleEquerre(deltax, deltay)
      return
    }
    if (this.capRap) {
      const newx = ev.clientX
      const newy = ev.clientY
      let deltax = newx - this.x
      let deltay = newy - this.y
      const pt = { x: this.baserap.x + deltax, y: this.baserap.y + deltay }
      const lp = []
      for (let i = 0; i < this.listePointPris.length; i++) {
        if (this.listePointPris[i].viz) {
          pt2 = this.getPointPosition2(this.svgId, this.listePointPris[i].point)
          if (this.dist(pt2, pt) < this.unite / 2) {
            lp.push({
              co: pt2,
              dist: this.dist(pt2, pt)
            })
          }
        }
      }
      lp.sort(this.sortOnDistProp)
      if (lp.length > 0) {
        deltax = lp[0].co.x - this.baserap.x
        deltay = lp[0].co.y - this.baserap.y
      }

      this.mtgAppLecteur.setPointPosition(this.svgId, '#' + this.idPRap.pt, this.baserap.x + deltax, this.baserap.y + deltay, false)
      this.mtgAppLecteur.setPointPosition(this.svgId, '#raprap10', this.baserap2.x + deltax, this.baserap2.y + deltay, false)
      this.mtgAppLecteur.updateFigure(this.svgId)
    }
    if (this.capComp) {
      const newx = ev.clientX
      const newy = ev.clientY
      const deltax = newx - this.x
      const deltay = newy - this.y
      this.colleComp(deltax, deltay)
    }
  }

  faitcoxd (pt2, pt3, aver3, l3) {
    const a = (pt3.y - pt2.y) / (pt3.x - pt2.x)
    const b = pt2.y - a * pt2.x
    const a2 = -1 / a
    const b2 = l3.y - a2 * l3.x
    let co = { x: (b - b2) / (a2 - a) }
    co.y = a2 * co.x + b2
    if (a < 0.001 && a > -0.001) {
      co.x = l3.x
      co.y = pt2.y
    }
    if (a > 9999999 || a < -9999999) {
      co.x = pt2.x
      co.y = l3.y
    }
    co = {
      x: co.x + this.unite * 0.06 * Math.cos(aver3 * Math.PI / 180),
      y: co.y + this.unite * 0.06 * Math.sin(aver3 * Math.PI / 180)
    }
    return co
  }

  listpointaligne () {
    let pp
    const p1 = this.getPointPosition2(this.svgId, '#dpa')
    const p2 = this.getPointPosition2(this.svgId, '#dpz')
    const gogol = []
    const ghj = []
    for (let i = 0; i < this.listePointPris.length; i++) {
      if (this.listePointPris[i].viz) {
        pp = this.getPointPosition2(this.svgId, this.listePointPris[i].point)
        if (this.dist(pp, p1) > this.distcol && this.dist(pp, p2) > this.distcol) {
          if (this.distPtSeg2(p1, p2, pp) * this.unite < 1) gogol.push({ ki: pp, d: this.dist(pp, p1) })
        }
      }
    }
    gogol.sort(function (a, b) { return a.d - b.d })

    for (let i = 0; i < gogol.length; i++) {
      ghj.push(gogol[i].ki)
    }
    return ghj
  }

  allumelesbons (x, y) {
    const pa = this.getPointPosition2(this.svgId, '#dpa')
    const p1 = this.getPointPosition2(this.svgId, '#dp1')
    const p2 = this.getPointPosition2(this.svgId, '#dp2')
    const p3 = this.getPointPosition2(this.svgId, '#dp3')
    const p4 = this.getPointPosition2(this.svgId, '#dp4')
    const pz = this.getPointPosition2(this.svgId, '#dpz')
    this.pl = 0
    if (Math.abs(pa.x - pz.x) > 0.01) {
      if (pa.x > pz.x) {
        if (x < p1.x) this.pl = 1
        if (x < p2.x) this.pl = 2
        if (x < p3.x) this.pl = 3
        if (x < p4.x) this.pl = 4
      } else {
        if (x > p1.x) this.pl = 1
        if (x > p2.x) this.pl = 2
        if (x > p3.x) this.pl = 3
        if (x > p4.x) this.pl = 4
      }
    } else {
      if (pa.y > pz.y) {
        if (y < p1.y) this.pl = 1
        if (y < p2.y) this.pl = 2
        if (y < p3.y) this.pl = 3
        if (y < p4.y) this.pl = 4
      } else {
        if (y > p1.y) this.pl = 1
        if (y > p2.y) this.pl = 2
        if (y > p3.y) this.pl = 3
        if (y > p4.y) this.pl = 4
      }
    }
    this.setVisible2(this.svgId, '#da', this.pl === 4, true)
    this.setVisible2(this.svgId, '#d1', this.pl === 1 || this.pl === 0, true)
    this.setVisible2(this.svgId, '#d2', this.pl === 2 || this.pl === 1, true)
    this.setVisible2(this.svgId, '#d3', this.pl === 3 || this.pl === 2, true)
    this.setVisible2(this.svgId, '#d4', this.pl === 4 || this.pl === 3, true)
    this.setVisible2(this.svgId, '#dz', this.pl === 0, true)
  }

  rottout (b, b2) {
    if (b !== true) {
      this.lala = this.lalabase - this.mtgAppLecteur.valueOf(this.svgId, 'MesRot')
      this.lalabase = this.mtgAppLecteur.valueOf(this.svgId, 'MesRot')
      if (isNaN(this.lala)) return
      this.rotationne('#ptCa1')
      this.rotationne('#ptCa2')
      this.rotationne('#Q')
    } else {
      this.lala = j3pGetRandomInt(10, 350)
      // this.lala = 0
    }
    if (this.regleVisible) this.rotationne('B')
    if (this.equerreVisible) this.rotationne('eq1')
    if (this.rapporteurVisible) this.rotationne('rappt1')
    if (this.compasVisible) {
      this.zoomize('compt1')
      this.zoomize('compt2')
      this.zoomize('ptverB')
    }
    for (let i = 0; i < this.listeSegPris.length; i++) {
      this.rotationne(this.listeSegPris[i].point1)
      this.rotationne(this.listeSegPris[i].point2)
      if (this.listeSegPris[i].pointNom) this.rotationne(this.listeSegPris[i].pointNom)
    }
    for (let i = 0; i < this.listeDtePris.length; i++) {
      this.rotationne(this.listeDtePris[i].point1)
      this.rotationne(this.listeDtePris[i].point2)
      if (this.listeDtePris[i].pointNom) this.rotationne(this.listeDtePris[i].pointNom)
    }
    for (let i = 0; i < this.listeDemiDPris.length; i++) {
      this.rotationne(this.listeDemiDPris[i].point1)
      this.rotationne(this.listeDemiDPris[i].point2)
      if (this.listeDemiDPris[i].pointNom) this.rotationne(this.listeDemiDPris[i].pointNom)
    }
    for (let i = 0; i < this.listeArcPris.length; i++) {
      this.rotationne(this.listeArcPris[i].centre)
      this.rotationne(this.listeArcPris[i].p1)
      this.rotationne(this.listeArcPris[i].p2)
      this.rotationne(this.listeArcPris[i].p3)
      this.rotationne(this.listeArcPris[i].p4)
      this.rotationne(this.listeArcPris[i].p5)
      this.rotationne(this.listeArcPris[i].p6)
      this.rotationne(this.listeArcPris[i].p7)
      this.rotationne(this.listeArcPris[i].p8)
      this.rotationne(this.listeArcPris[i].p9)
      this.rotationne(this.listeArcPris[i].p10)
      this.rotationne(this.listeArcPris[i].p11)
      this.rotationne(this.listeArcPris[i].p12)
      if (this.listeArcPris[i].pointNom) this.rotationne(this.listeArcPris[i].pointNom)
    }
    for (let i = 0; i < this.listePointPris.length; i++) {
      this.rotationne(this.listePointPris[i].point)
    }
    for (let i = this.ConsSegnb; i < this.segCo.length; i++) {
      this.rotationne(this.segCo[i].html.point1)
      this.rotationne(this.segCo[i].html.point2)
    }
    for (let i = this.ConsDtenb; i < this.dteco.length; i++) {
      this.rotationne(this.dteco[i].html.point1)
      this.rotationne(this.dteco[i].html.point2)
    }
    for (let i = this.ConsDemiDnb; i < this.demiDco.length; i++) {
      this.rotationne(this.demiDco[i].html.point1)
      this.rotationne(this.demiDco[i].html.point2)
    }
    for (let i = this.ConsArcnb; i < this.arcco.length; i++) {
      this.rotationne(this.arcco[i].html.centre)
      this.rotationne(this.arcco[i].html.p1)
      this.rotationne(this.arcco[i].html.p2)
    }
    for (let i = this.ConsPtnb; i < this.ptCo.length; i++) {
      this.rotationne(this.ptCo[i].html.point)
    }
    for (let i = 0; i < this.nomco.length; i++) {
      this.rotationne(this.nomco[i].html[0])
    }
    if (b2 !== false) this.mtgAppLecteur.updateFigure(this.svgId)
  }

  rotationne (ki) {
    const pt = this.getPointPosition2(this.svgId, ki)
    const ptMil = this.getPointPosition2(this.svgId, 'Z2')
    const newx = {}
    const zi = this.lala * Math.PI / 180
    const xM = pt.x - ptMil.x
    const yM = pt.y - ptMil.y
    newx.x = xM * Math.cos(zi) + yM * Math.sin(zi) + ptMil.x
    newx.y = -xM * Math.sin(zi) + yM * Math.cos(zi) + ptMil.y
    this.mtgAppLecteur.setPointPosition(this.svgId, ki, newx.x, newx.y, false)
  }

  creeNewDroite (color) {
    const ss = {}
    ss.point1 = this.newTag()
    ss.point2 = this.newTag()
    ss.ligne = this.newTag()
    ss.color = color
    this.mtgAppLecteur.addFreePoint({
      absCoord: true,
      tag: ss.point1,
      name: ss.point1,
      hidden: true,
      x: 100,
      y: 100
    })
    this.mtgAppLecteur.addFreePoint({
      absCoord: true,
      tag: ss.point2,
      name: ss.point2,
      hidden: true,
      x: 110,
      y: 110
    })
    this.mtgAppLecteur.addLineAB({
      a: ss.point1,
      b: ss.point2,
      color: this.transColor(ss.color),
      thickness: 2,
      tag: ss.ligne
    })
    this.mtgAppLecteur.reclassBefore({ elt: ss.point1, elt2: 'unit2' })
    this.mtgAppLecteur.reclassBefore({ elt: ss.point2, elt2: 'unit2' })
    this.mtgAppLecteur.reclassBefore({ elt: ss.ligne, elt2: 'unit2' })
    return ss
  }

  creeNewDemiDroite (color) {
    const ss = {}
    ss.point1 = this.newTag()
    ss.point2 = this.newTag()
    ss.ligne = this.newTag()
    ss.color = color
    this.mtgAppLecteur.addFreePoint({
      absCoord: true,
      tag: ss.point1,
      name: ss.point1,
      hidden: true,
      x: 100,
      y: 100
    })
    this.mtgAppLecteur.addFreePoint({
      absCoord: true,
      tag: ss.point2,
      name: ss.point2,
      hidden: true,
      x: 110,
      y: 110
    })
    this.mtgAppLecteur.addRay({
      o: ss.point1,
      a: ss.point2,
      color: this.transColor(ss.color),
      thickness: 3,
      tag: ss.ligne
    })
    this.mtgAppLecteur.reclassBefore({ elt: ss.point1, elt2: 'unit2' })
    this.mtgAppLecteur.reclassBefore({ elt: ss.point2, elt2: 'unit2' })
    this.mtgAppLecteur.reclassBefore({ elt: ss.ligne, elt2: 'unit2' })
    return ss
  }

  creeNewSeg (color) {
    const ss = {}
    ss.point1 = this.newTag()
    ss.point2 = this.newTag()
    ss.ligne = this.newTag()
    ss.color = color
    this.mtgAppLecteur.addFreePoint({
      absCoord: true,
      tag: ss.point1,
      name: ss.point1,
      hidden: true,
      x: 100,
      y: 100
    })
    this.mtgAppLecteur.addFreePoint({
      absCoord: true,
      tag: ss.point2,
      name: ss.point2,
      hidden: true,
      x: 110,
      y: 110
    })
    this.mtgAppLecteur.addSegment({
      a: ss.point1,
      b: ss.point2,
      color: this.transColor(ss.color),
      thickness: 3,
      tag: ss.ligne
    })
    this.mtgAppLecteur.reclassBefore({ elt: ss.point1, elt2: 'unit2' })
    this.mtgAppLecteur.reclassBefore({ elt: ss.point2, elt2: 'unit2' })
    this.mtgAppLecteur.reclassBefore({ elt: ss.ligne, elt2: 'unit2' })
    return ss
  }

  creeNewArc (color) {
    const ss = {}
    ss.centre = this.newTag()
    ss.color = j3pClone(color)
    ss.p1 = this.newTag()
    ss.p2 = this.newTag()
    ss.p3 = this.newTag()
    ss.p4 = this.newTag()
    ss.p5 = this.newTag()
    ss.p6 = this.newTag()
    ss.p7 = this.newTag()
    ss.p8 = this.newTag()
    ss.p9 = this.newTag()
    ss.p10 = this.newTag()
    ss.p11 = this.newTag()
    ss.p12 = this.newTag()
    ss.arc1 = this.newTag()
    ss.arc2 = this.newTag()
    ss.arc3 = this.newTag()
    ss.arc4 = this.newTag()
    ss.arc5 = this.newTag()
    ss.arc6 = this.newTag()
    ss.arc7 = this.newTag()
    ss.arc8 = this.newTag()
    ss.arc9 = this.newTag()
    ss.arc10 = this.newTag()
    ss.arc11 = this.newTag()
    ss.arc12 = this.newTag()
    this.mtgAppLecteur.addFreePoint({
      absCoord: true,
      tag: ss.p1,
      name: ss.p1,
      hidden: true,
      x: 50,
      y: 100
    })
    this.mtgAppLecteur.addFreePoint({
      absCoord: true,
      tag: ss.p2,
      name: ss.p2,
      hidden: true,
      x: 55,
      y: 110
    })
    this.mtgAppLecteur.addFreePoint({
      absCoord: true,
      tag: ss.p3,
      name: ss.p3,
      hidden: true,
      x: 60,
      y: 100
    })
    this.mtgAppLecteur.addFreePoint({
      absCoord: true,
      tag: ss.p4,
      name: ss.p4,
      hidden: true,
      x: 65,
      y: 110
    })
    this.mtgAppLecteur.addFreePoint({
      absCoord: true,
      tag: ss.p5,
      name: ss.p5,
      hidden: true,
      x: 70,
      y: 100
    })
    this.mtgAppLecteur.addFreePoint({
      absCoord: true,
      tag: ss.p6,
      name: ss.p6,
      hidden: true,
      x: 80,
      y: 110
    })
    this.mtgAppLecteur.addFreePoint({
      absCoord: true,
      tag: ss.p7,
      name: ss.p7,
      hidden: true,
      x: 90,
      y: 100
    })
    this.mtgAppLecteur.addFreePoint({
      absCoord: true,
      tag: ss.p8,
      name: ss.p8,
      hidden: true,
      x: 95,
      y: 110
    })
    this.mtgAppLecteur.addFreePoint({
      absCoord: true,
      tag: ss.p9,
      name: ss.p9,
      hidden: true,
      x: 100,
      y: 110
    })
    this.mtgAppLecteur.addFreePoint({
      absCoord: true,
      tag: ss.p10,
      name: ss.p10,
      hidden: true,
      x: 105,
      y: 100
    })
    this.mtgAppLecteur.addFreePoint({
      absCoord: true,
      tag: ss.p11,
      name: ss.p11,
      hidden: true,
      x: 110,
      y: 110
    })
    this.mtgAppLecteur.addFreePoint({
      absCoord: true,
      tag: ss.p12,
      name: ss.p12,
      hidden: true,
      x: 115,
      y: 110
    })
    this.mtgAppLecteur.addFreePoint({
      absCoord: true,
      tag: ss.centre,
      name: ss.centre,
      hidden: true,
      x: 100,
      y: 100
    })
    this.mtgAppLecteur.addArcOAB({
      o: ss.centre,
      a: ss.p1,
      b: ss.p2,
      color: this.transColor(ss.color),
      thickness: 3,
      tag: ss.arc1
    })
    this.mtgAppLecteur.addArcOAB({
      o: ss.centre,
      a: ss.p2,
      b: ss.p3,
      color: this.transColor(ss.color),
      thickness: 3,
      tag: ss.arc2
    })
    this.mtgAppLecteur.addArcOAB({
      o: ss.centre,
      a: ss.p3,
      b: ss.p4,
      color: this.transColor(ss.color),
      thickness: 3,
      tag: ss.arc3
    })
    this.mtgAppLecteur.addArcOAB({
      o: ss.centre,
      a: ss.p4,
      b: ss.p5,
      color: this.transColor(ss.color),
      thickness: 3,
      tag: ss.arc4
    })
    this.mtgAppLecteur.addArcOAB({
      o: ss.centre,
      a: ss.p5,
      b: ss.p6,
      color: this.transColor(ss.color),
      thickness: 3,
      tag: ss.arc5
    })
    this.mtgAppLecteur.addArcOAB({
      o: ss.centre,
      a: ss.p6,
      b: ss.p7,
      color: this.transColor(ss.color),
      thickness: 3,
      tag: ss.arc6
    })
    this.mtgAppLecteur.addArcOAB({
      o: ss.centre,
      a: ss.p7,
      b: ss.p8,
      color: this.transColor(ss.color),
      thickness: 3,
      tag: ss.arc7
    })
    this.mtgAppLecteur.addArcOAB({
      o: ss.centre,
      a: ss.p8,
      b: ss.p9,
      color: this.transColor(ss.color),
      thickness: 3,
      tag: ss.arc8
    })
    this.mtgAppLecteur.addArcOAB({
      o: ss.centre,
      a: ss.p9,
      b: ss.p10,
      color: this.transColor(ss.color),
      thickness: 3,
      tag: ss.arc9
    })
    this.mtgAppLecteur.addArcOAB({
      o: ss.centre,
      a: ss.p10,
      b: ss.p11,
      color: this.transColor(ss.color),
      thickness: 3,
      tag: ss.arc10
    })
    this.mtgAppLecteur.addArcOAB({
      o: ss.centre,
      a: ss.p11,
      b: ss.p12,
      color: this.transColor(ss.color),
      thickness: 3,
      tag: ss.arc11
    })
    this.mtgAppLecteur.addArcOAB({
      o: ss.centre,
      a: ss.p12,
      b: ss.p1,
      color: this.transColor(ss.color),
      thickness: 3,
      tag: ss.arc12
    })
    this.mtgAppLecteur.reclassBefore({ elt: ss.arc1, elt2: 'unit2' })
    this.mtgAppLecteur.reclassBefore({ elt: ss.arc2, elt2: 'unit2' })
    this.mtgAppLecteur.reclassBefore({ elt: ss.arc3, elt2: 'unit2' })
    this.mtgAppLecteur.reclassBefore({ elt: ss.arc4, elt2: 'unit2' })
    this.mtgAppLecteur.reclassBefore({ elt: ss.arc5, elt2: 'unit2' })
    this.mtgAppLecteur.reclassBefore({ elt: ss.arc6, elt2: 'unit2' })
    this.mtgAppLecteur.reclassBefore({ elt: ss.arc7, elt2: 'unit2' })
    this.mtgAppLecteur.reclassBefore({ elt: ss.arc8, elt2: 'unit2' })
    this.mtgAppLecteur.reclassBefore({ elt: ss.arc9, elt2: 'unit2' })
    this.mtgAppLecteur.reclassBefore({ elt: ss.arc10, elt2: 'unit2' })
    this.mtgAppLecteur.reclassBefore({ elt: ss.arc11, elt2: 'unit2' })
    this.mtgAppLecteur.reclassBefore({ elt: ss.arc12, elt2: 'unit2' })
    return ss
  }

  finalement (b) {
    if (b === true) {
      let p1, p2, ttub, koi, ss
      if (this.pl === 0) {
        if (this.listeDemiDPris.length === this.nbddte) {
          this.affModale('Tu as tracé trop de demi-droites !', 'Erreur', true)
          this.renvoieCrayon()
          return
        }
        p1 = this.getPointPosition2(this.svgId, '#dp1')
        p2 = this.getPointPosition2(this.svgId, '#dpa')
        ss = this.creeNewDemiDroite(this.laCoool)
        ttub = this.listeDemiDPris
        koi = 'demid'
      } else if (this.pl === 4) {
        if (this.listeDemiDPris.length === this.nbddte) {
          this.affModale('Tu as tracé trop de demi-droites !', 'Erreur', true)
          this.renvoieCrayon()
          return
        }
        p1 = this.getPointPosition2(this.svgId, '#dp4')
        p2 = this.getPointPosition2(this.svgId, '#dpz')
        ss = this.creeNewDemiDroite(this.laCoool)
        ttub = this.listeDemiDPris
        koi = 'demid'
      } else {
        p1 = this.getPointPosition2(this.svgId, '#dp' + (this.pl))
        try {
          p2 = this.getPointPosition2(this.svgId, '#dp' + (this.pl + 1))
        } catch (e) {
          j3pDetruit('modale2')
          j3pDetruit('j3pmasque2')
          j3pNotify('Pour Tommy progcontbase ligne 4063', { notifyData: { pl: this.pl, pl2: this.pl + 1, nom: '#dp' + (this.pl + 1) } })
          return
        }
        if (this.yaseg(p1, p2)) {
          j3pDetruit('modale2')
          j3pDetruit('j3pmasque2')
          // FIXME cette ligne était dans main, faut la garder ?
          this.renvoieCrayon()
          return
        }
        if (this.listeSegPris.length === this.nbseg) {
          j3pDetruit('modale2')
          j3pDetruit('j3pmasque2')
          this.affModale('Tu as tracé trop de segments !', 'Erreur', true)
          this.renvoieCrayon()
          return
        }
        ss = this.creeNewSeg(this.laCoool)
        ttub = this.listeSegPris
        this.setVisible2(this.svgId, ss.ligne, true, true)
        koi = 'seg'
      }
      ss.viz = true
      ttub.push(ss)
      this.setVisible2(this.svgId, ss.ligne, true, true)
      this.mtgAppLecteur.setPointPosition(this.svgId, ss.point1, p1.x, p1.y, true)
      this.mtgAppLecteur.setPointPosition(this.svgId, ss.point2, p2.x, p2.y, true)
      this.setLineStyle2(this.svgId, ss.ligne, 0, 2, true)
      this.setColor2(this.svgId, ss.ligne, this.laCoool[0], this.laCoool[1], this.laCoool[2], true)
      this.renvoieCrayon()
      this.faitFuncGomm(ss, koi)
      j3pDetruit('modale2')
      j3pDetruit('j3pmasque2')
    } else {
      if (b === false) {
        j3pDetruit('modale2')
        j3pDetruit('j3pmasque2')
      }
      const p1 = this.getPointPosition2(this.svgId, '#regtttt1')
      const p2 = this.getPointPosition2(this.svgId, '#regtttt2')
      if (this.listeDtePris.length === this.nbdte) {
        j3pDetruit('modale2')
        j3pDetruit('j3pmasque2')
        this.affModale('Tu as tracé trop de droites !', 'Erreur', true)
        this.renvoieCrayon()
        return
      }
      const ss = this.creeNewDroite(this.laCoool)
      ss.viz = true
      this.setVisible2(this.svgId, ss.ligne, true, true)
      this.mtgAppLecteur.setPointPosition(this.svgId, ss.point1, p1.x, p1.y, true)
      this.mtgAppLecteur.setPointPosition(this.svgId, ss.point2, p2.x, p2.y, true)
      this.setLineStyle2(this.svgId, ss.ligne, 0, 3, true)
      this.setColor2(this.svgId, ss.ligne, this.laCoool[0], this.laCoool[1], this.laCoool[2], true)
      this.renvoieCrayon()
      this.listeDtePris.push(ss)
      this.faitFuncGomm(ss, 'dte')
      j3pDetruit('modale2')
      j3pDetruit('j3pmasque2')
      this.faisNommDroite(ss, 'droite')
    }
  }

  setLineStyle2 (a, b, c, d) {
    const com = ['-', '.', '--', '-..', '-...', '---']
    try {
      this.mtgAppLecteur.setLineStyle({ elt: b.replace('#', ''), lineStyle: com[c] })
      this.mtgAppLecteur.setThickness({ elt: b.replace('#', ''), thickness: d })
    } catch (e) {
      // eslint-disable-next-line no-console
      console.log('pas pas modif un objet non visible')
    }
  }

  gereRegle (ev) {
    if (!this.lagrad) {
      if (!this.bloquepoint) {
        this.finalement()
        return
      }
      if (this.pl === undefined || this.pl === null) {
        j3pNotify('Pour Tommy progConstBase', { notifyData: { pl: this.pl, bloquepoint: this.bloquepoint } })
        this.finalement()
        return
      }
      this.toutDroite()
      return
    }
    this.PtRegle = true
    this.lepoint22 = false
    let difx, dify
    const pt = this.getPointPosition2(this.svgId, this.idImCrayPt)
    const pt2 = this.getPointPosition2(this.svgId, 'R')
    this.x = ev.clientX
    this.y = ev.clientY
    this.renvoieCrayon()
    difx = dify = 0
    this.setVisible2(this.svgId, this.regtra[2], true, true)
    // this.setVisible2(this.svgId, this.regtra[3], true, true)
    // this.setVisible2(this.svgId, this.regtra[4], true, true)
    // capt pt pas loin
    const lp = []
    for (let i = 0; i < this.listePointPris.length; i++) {
      const pt3 = this.getPointPosition2(this.svgId, this.listePointPris[i].point)
      if (this.dist(pt2, pt3) < this.unite / 4) {
        lp.push({
          pt: j3pClone(pt3),
          dist: this.dist(pt2, pt3)
        })
      }
    }
    lp.sort(this.sortOnDistProp)
    if (lp.length > 0) {
      difx = lp[0].pt.x - pt2.x
      dify = lp[0].pt.y - pt2.y
    }
    this.mtgAppLecteur.setPointPosition(this.svgId, this.regtra[0], pt2.x + difx, pt2.y + dify, true)
    this.mtgAppLecteur.setPointPosition(this.svgId, this.regtra[1], pt2.x + difx, pt2.y + dify, true)
    this.mtgAppLecteur.setPointPosition(this.svgId, this.idPtRegle.ptCap, pt.x, pt.y, true)
  }

  faisNommDroite (ss, k) {
    let buf1, buf2
    this.mtgAppLecteur.getDoc(this.svgId).defaultCursor = ''
    if (k === 'droite') {
      buf1 = 'la droite'
      buf2 = 'cette droite'
    }
    if (k === 'cercle') {
      buf1 = 'le cercle'
      buf2 = 'ce cercle'
    }
    this.whereAfficheAide.style.display = 'none'
    const yy = j3pModale2({ titre: 'Nommer ' + buf1, contenu: '', divparent: this.divparent })
    this.stab1 = addDefaultTable(yy, 3, 1)
    this.stab2 = addDefaultTable(this.stab1[0][0], 1, 3)
    this.stab1[1][0].style.color = this.me.styles.cfaux
    j3pAffiche(this.stab1[1][0], null, '<i>(Laisse vide si tu ne veux pas nommer ' + buf2 + '.)</i>')
    // FIXME pourquoi on ne peut pas mettre de majuscule sur les noms de droite ? Faudrait autoriser (AB) non ?
    const restric = (k === 'cercle') ? '()ABCDEFGHIJKLMNOPQRSTUVWXYZ0123456789' : '()abcdefghijklmnopqrstuvwxyz1234567890'
    const ff = () => { this.sortQuestDList(ss, k) }
    this.affmodQ = new ZoneStyleMathquill1(this.stab2[0][1], {
      ailleurs: 'modale2', // FIXME cette propriété n’existe pas dans les valeurs que prend le constructeur de ZoneStyleMathquill1
      center: true,
      restric,
      limite: 4,
      limitenb: 4,
      enter: ff.bind(this)
    })
    j3pAjouteBouton(this.stab2[0][2], ff.bind(this), { className: 'ok', value: 'OK' })
    const elem = $('.croix')[0]
    j3pDetruit(elem)
    setTimeout(() => { this.affmodQ.focus() }, 120)
    yy.style.cursor = 'default'
    yy.masque.style.cursor = 'default'
  }

  /**
   *
   * @private
   * @param {string} k cercle|droite
   * @param num
   */
  nomme (k, num) {
    if (this.bloquenomme === true) return
    let buf1, buf2
    this.mtgAppLecteur.getDoc(this.svgId).defaultCursor = ''
    if (k === 'droite') {
      buf1 = 'la droite'
      buf2 = 'cette droite'
    }
    if (k === 'cercle') {
      buf1 = 'le cercle'
      buf2 = 'ce cercle'
    }
    this.whereAfficheAide.style.display = 'none'
    const yy = j3pModale2({ titre: 'Nommer/renommer ' + buf1, contenu: '', divparent: this.divparent })
    this.stab1 = addDefaultTable(yy, 3, 1)
    this.stab2 = addDefaultTable(this.stab1[0][0], 1, 3)
    this.stab1[1][0].style.color = this.me.styles.cfaux
    j3pAffiche(this.stab1[1][0], null, '<i>(Laisse vide si tu ne veux pas nommer ' + buf2 + '.)</i>')
    let lares = '()abcdefghijklmnopqrstuvwxyz1234567890'
    if (k === 'cercle') lares = '()ABCDEFGHIJKLMNOPQRSTUVWXYZ0123456789'
    const onEnter = () => this.sortQuestD2(k, num)
    this.affmodQ = new ZoneStyleMathquill1(this.stab2[0][1], {
      ailleurs: 'modale2', // FIXME cette propriété n’existe pas dans les valeurs que prend le constructeur de ZoneStyleMathquill1
      center: true,
      restric: lares,
      limite: 4,
      limitenb: 4,
      enter: onEnter
    })
    j3pAjouteBouton(this.stab2[0][2], onEnter, { className: 'ok', value: 'OK' })
    yy.style.cursor = 'default'
    yy.masque.style.cursor = 'default'
    const elem = $('.croix')[0]
    j3pDetruit(elem)
    setTimeout(() => { this.affmodQ.focus() }, 100)
  }

  gereFinRegle () {
    this.PtRegle = false

    this.setVisible2(this.svgId, this.regtra[2], false, false)
    // this.setVisible2(this.svgId, this.regtra[3], false, false)
    this.setVisible2(this.svgId, this.regtra[4], false, false)
    if (this.listeSegPris.length === this.nbseg) {
      this.affModale('Tu as tracé trop de segments !', 'Erreur', true)
      this.renvoieCrayon()
      this.mtgAppLecteur.updateFigure(this.svgId)
      return
    }

    const ss = this.creeNewSeg(this.laCoool)
    this.setVisible2(this.svgId, ss.ligne, true, true)
    this.setVisible2(this.svgId, ss.point1, false, true)
    this.setVisible2(this.svgId, ss.point2, false, true)
    const p1 = this.getPointPosition2(this.svgId, this.regtra[1])
    const p2 = this.getPointPosition2(this.svgId, this.regtra[0])
    if (this.dist(p1, p2) < this.distcol) {
      this.mtgAppLecteur.updateFigure(this.svgId)
      return
    }
    this.mtgAppLecteur.setPointPosition(this.svgId, ss.point1, p1.x, p1.y, false)
    this.mtgAppLecteur.setPointPosition(this.svgId, this.idPtRegle.ptCap, 1000, 1000, false)
    const lp = []
    ss.viz = true
    for (let i = 0; i < this.listePointPris.length; i++) {
      const pt = this.getPointPosition2(this.svgId, this.listePointPris[i].point)
      if (this.dist(pt, p2) < this.unite * 0.2) {
        lp.push({
          pt: j3pClone(pt),
          dist: this.dist(p2, pt)
        })
      }
    }
    lp.sort(this.sortOnDistProp)
    if (lp.length > 0) {
      this.mtgAppLecteur.setPointPosition(this.svgId, ss.point2, lp[0].pt.x, lp[0].pt.y, false)
    } else {
      this.mtgAppLecteur.setPointPosition(this.svgId, ss.point2, p2.x, p2.y, false)
    }
    this.listeSegPris.push(j3pClone(ss))
    this.mtgAppLecteur.updateFigure(this.svgId)
    this.setColor2(this.svgId, ss.ligne, this.laCoool[0], this.laCoool[1], this.laCoool[2], true)
    this.setVisible2(this.svgId, ss.ligne, true, true)
    this.faitFuncGomm(ss, 'seg')
  }

  gereEquerre (ev, n) {
    const p1 = this.getPointPosition2(this.svgId, this.DtEque[n][0])
    const p2 = this.getPointPosition2(this.svgId, this.DtEque[n][1])
    this.renvoieCrayon()
    if (this.listeDtePris.length === this.nbdte) {
      this.affModale('Tu as tracé trop de droites !', 'Erreur', true)
      this.renvoieCrayon()
      return
    }
    const ss = this.creeNewDroite(this.laCoool)
    ss.viz = true
    this.setVisible2(this.svgId, ss.ligne, true, true)
    this.mtgAppLecteur.setPointPosition(this.svgId, ss.point1, p1.x, p1.y, true)
    this.mtgAppLecteur.setPointPosition(this.svgId, ss.point2, p2.x, p2.y, true)
    this.setLineStyle2(this.svgId, ss.ligne, 0, 2, true)
    this.setColor2(this.svgId, ss.ligne, this.laCoool[0], this.laCoool[1], this.laCoool[2], true)
    this.listeDtePris.push(ss)
    this.faitFuncGomm(ss, 'dte')
    this.faisNommDroite(ss, 'droite')
  }

  gereRapporteur () {
    const p1 = this.getPointPosition2(this.svgId, this.idPRap.pt)
    const p2 = this.getPointPosition2(this.svgId, '#rapptcol')
    this.renvoieCrayon(false)
    if (this.listeDemiDPris.length === this.nbddte) {
      this.affModale('Tu as tracé trop de demi-droites !', 'Erreur', true)
      this.renvoieCrayon()
      this.mtgAppLecteur.updateFigure(this.svgId)
      return
    }
    const ss = this.creeNewDemiDroite(this.laCoool)
    this.setVisible2(this.svgId, ss.ligne, true, true)
    this.mtgAppLecteur.setPointPosition(this.svgId, ss.point1, p1.x, p1.y, false)
    this.mtgAppLecteur.setPointPosition(this.svgId, ss.point2, p2.x, p2.y, false)
    this.setLineStyle2(this.svgId, ss.ligne, 0, 2, true)
    this.setColor2(this.svgId, ss.ligne, this.laCoool[0], this.laCoool[1], this.laCoool[2], true)
    ss.viz = true
    this.listeDemiDPris.push(ss)

    this.mtgAppLecteur.updateFigure(this.svgId)
    this.faitFuncGomm(ss, 'demid')
  }

  gereCompas (ev) {
    this.equerreVisible = true
    this.rapporteurVisible = true
    this.regleVisible = true
    this.lagrad = false
    this.regle()
    this.equerre()
    this.rapporteur()
    this.justav = undefined
    // fais apparaitre 2 arc sd si de 5
    let pt = this.getPointPosition2(this.svgId, this.idCompPt.pointe)
    let pt2 = this.getPointPosition2(this.svgId, this.idCompPt.mine)
    /// fais zoom spe
    this.distab = this.dist(pt, pt2)
    this.pbbase = pt
    if (this.faiszomm(false, false, true)) {
      this.mtgAppLecteur.calculate(this.svgId)
      pt = this.getPointPosition2(this.svgId, this.idCompPt.pointe)
      pt2 = this.getPointPosition2(this.svgId, this.idCompPt.mine)
      this.distab = this.dist(pt, pt2)
      this.pbbase = pt
    }
    const GG = this.zonefig.getBoundingClientRect()
    this.x = ev.clientX
    this.y = ev.clientY
    this.renvoieCrayon()
    this.verold = this.verouV
    this.compas(true)

    for (let i = 0; i < this.compaTra.length; i++) {
      this.setVisible2(this.svgId, this.compaTra[i], true, true)
    }
    this.setVisible2(this.svgId, this.idCoparcd[3], true, true)
    this.setVisible2(this.svgId, this.idCoparcs[3], true, true)
    this.setVisible2(this.svgId, '#verouv', false, true)
    this.setVisible2(this.svgId, '#verfer', false, true)
    this.setVisible2(this.svgId, this.idCoparcs[3], true, true)
    this.mtgAppLecteur.setPointPosition(this.svgId, this.idCoparcd[0], pt.x, pt.y, false)
    this.mtgAppLecteur.setPointPosition(this.svgId, this.idCoparcd[1], pt2.x, pt2.y, false)
    this.mtgAppLecteur.setPointPosition(this.svgId, this.idCoparcs[0], pt.x, pt.y, false)
    this.mtgAppLecteur.setPointPosition(this.svgId, this.idCoparcs[1], pt2.x, pt2.y, false)
    this.mtgAppLecteur.setPointPosition(this.svgId, '#compcoupt', this.x - GG.x, this.y - GG.y, false)
    this.mtgAppLecteur.giveFormula2(this.svgId, 'vd', 0.01)
    this.mtgAppLecteur.giveFormula2(this.svgId, 'vi', -0.01)
    this.mtgAppLecteur.updateFigure(this.svgId)
    this.PtCompS = true
  }

  gereFinCOmpas () {
    let ycercle = false
    this.PtCompS = false
    this.PtCompS2 = false
    this.compas(true, true)
    this.setVisible2(this.svgId, '#compcoupt', false, true)
    const p1 = this.getPointPosition2(this.svgId, this.idCompPt.pointe)
    const p2 = this.getPointPosition2(this.svgId, this.idCoparcd[2])
    const p3 = this.getPointPosition2(this.svgId, this.idCoparcs[2])
    const vi = 90 - Math.atan2(p3.x - p1.x, p1.y - p3.y) * 180 / Math.PI
    let vd = 90 - Math.atan2(p2.x - p1.x, p1.y - p2.y) * 180 / Math.PI
    if (vd < vi) vd += 360
    let ai
    for (let i = 0; i < this.compaTra.length; i++) {
      this.setVisible2(this.svgId, this.compaTra[i], false, true)
    }
    this.setVisible2(this.svgId, this.idCoparcd[3], false, true)
    this.setVisible2(this.svgId, this.idCoparcs[3], false, true)
    // cree new arc
    if (this.listeArcPris.length === this.nbarc) {
      this.affModale('Tu as tracé trop d’arcs de cercle !', 'Erreur', true)
      this.renvoieCrayon()
      if (this.zoom < 1) this.faiszomm(false, true)
      return
    }
    const ss = this.creeNewArc(this.laCoool)
    const r = this.dist(p1, p2)
    for (let i = 1; i < 13; i++) {
      ai = vi + (i - 1) * (vd - vi) / 11
      this.mtgAppLecteur.setPointPosition(this.svgId, ss['p' + i], p1.x + r * Math.cos(ai * Math.PI / 180), p1.y - r * Math.sin(ai * Math.PI / 180), false)
    }
    if (vd - vi > 355.1) {
      ycercle = true
      for (let i = 1; i < 13; i++) {
        ai = i * 360 / 11
        this.mtgAppLecteur.setPointPosition(this.svgId, ss['p' + i], p1.x + r * Math.cos(ai * Math.PI / 180), p1.y - r * Math.sin(ai * Math.PI / 180), false)
      }
    } else {
      this.setVisible2(this.svgId, ss.arc12, false)
    }
    this.mtgAppLecteur.setPointPosition(this.svgId, ss.centre, p1.x, p1.y, false)
    this.mtgAppLecteur.updateFigure(this.svgId)
    ss.viz = true
    this.listeArcPris.push(ss)
    let ju = 'arc'
    if (ycercle) ju = 'cercle'
    this.faitFuncGomm(ss, ju)
    if (this.zoom < 1) this.faiszomm(false, true)
    if (ycercle) this.faisNommDroite(ss, 'cercle')
  }

  viregommEtrot (b) {
    this.lagomm = false
    this.lagomVire()
    if (b !== true) {
      this.larot = false
      this.larotVire()
    }
    if (b !== 'p') {
      this.poitilEncours = false
      this.pointilleVire()
    }
    this.lepoint = false
    this.lepoint4 = false
    this.lepointVire()
  }

  colleComp (deltax, deltay) {
    let lm = { x: this.basecomp.x + deltax, y: this.basecomp.y + deltay }
    const lmold = j3pClone(lm)

    this.vpi = false
    let gtrouv = false
    let gpos = []
    for (let i = 0; i < this.listePointPris.length; i++) {
      if (this.listePointPris[i].viz) {
        const l1 = this.getPointPosition2(this.svgId, this.listePointPris[i].point)
        if (this.dist(l1, lm) < this.distcol) {
          gtrouv = true
          this.vpin = i
          this.vpi = true
          gpos.push({
            t: 'pt',
            pt: l1,
            dist: this.dist(l1, lm)
          })
        }
      }
    }

    // cherche prox regle
    let yaregl
    if (!gtrouv) {
      if (this.regleVisible) {
        let a, b, co
        const l1 = this.getPointPosition2(this.svgId, '#regreg0')
        const l2 = this.getPointPosition2(this.svgId, '#regreg2')
        let popos
        if (l2.x !== l1.x) {
          // FIXME, si on passe pas dans ce if a, b et co restent undefined
          a = (l2.y - l1.y) / (l2.x - l1.x)
          b = l1.y - a * l1.x
          const a2 = -1 / a
          const b2 = lm.y - a2 * lm.x
          co = { x: (b2 - b) / (a - a2) }
          co.y = a * co.x + b
          popos = lm.x > Math.min(l1.x, l2.x) && lm.x < Math.max(l1.x, l2.x)
        } else {
          co = {
            x: l2.x,
            y: lm.y
          }
          popos = lm.y > Math.min(l1.y, l2.y) && lm.y < Math.max(l1.y, l2.y)
        }
        if (popos) {
          if (this.dist(co, lm) < this.distcol) {
            yaregl = true
            gpos.push({
              t: 'reg',
              a,
              b,
              x: l2.x,
              ang: this.regule(Math.atan2(l2.x - l1.x, l1.y - l2.y) * 180 / Math.PI),
              co: j3pClone(co),
              dist: this.dist(co, lm)
            })
          }
        }
      }

      // seg
      for (let i = 0; i < this.listeSegPris.length; i++) {
        /// /////////////////*
        const l1 = this.getPointPosition2(this.svgId, this.listeSegPris[i].point1)
        const l2 = this.getPointPosition2(this.svgId, this.listeSegPris[i].point2)
        let a, b, co
        let popos = false
        if (this.dist(l1, lm) < this.distcol) {
          gpos.push({
            t: 'pt',
            pt: j3pClone(l1),
            dist: this.dist(l1, lm)
          })
        } else if (this.dist(l2, lm) < this.distcol) {
          gpos.push({
            t: 'pt',
            pt: j3pClone(l2),
            dist: this.dist(l2, lm)
          })
        } else {
          if (l2.x !== l1.x) {
            a = (l2.y - l1.y) / (l2.x - l1.x)
            b = l1.y - a * l1.x
            let a2 = -1 / a
            if (a === 0) a2 = 99999999999
            const b2 = lm.y - a2 * lm.x
            co = { x: (b2 - b) / (a - a2) }
            co.y = a * co.x + b
            popos = lm.x > Math.min(l1.x, l2.x) && lm.x < Math.max(l1.x, l2.x)
          } else {
            co = {
              x: l2.x,
              y: lm.y
            }
            popos = lm.y > Math.min(l1.y, l2.y) && lm.y < Math.max(l1.y, l2.y)
          }
          if (popos) {
            if (this.dist(co, lm) < this.distcol) {
              gpos.push({
                t: 'seg',
                a,
                b,
                dist: this.dist(co, lm),
                co: j3pClone(co)
              })
            }
          }
        }
      }

      /// droite
      for (let i = 0; i < this.listeDtePris.length; i++) {
        /// /////////////////
        if (this.listeDtePris[i].viz === false) continue
        const l1 = this.getPointPosition2(this.svgId, this.listeDtePris[i].point1)
        const l2 = this.getPointPosition2(this.svgId, this.listeDtePris[i].point2)
        let a, b, co
        if (l2.x !== l1.x) {
          a = (l2.y - l1.y) / (l2.x - l1.x)
          b = l1.y - a * l1.x
          const a2 = -1 / a
          const b2 = lm.y - a2 * lm.x
          co = { x: (b2 - b) / (a - a2) }
          co.y = a * co.x + b
        } else {
          co = {
            x: l2.x,
            y: lm.y
          }
        }
        if (this.dist(co, lm) < this.distcol) {
          gpos.push({
            t: 'seg',
            a,
            b,
            dist: this.dist(co, lm),
            co: j3pClone(co)
          })
        }
      }

      // demi
      for (let i = 0; i < this.listeDemiDPris.length; i++) {
        const l1 = this.getPointPosition2(this.svgId, this.listeDemiDPris[i].point1)
        const l2 = this.getPointPosition2(this.svgId, this.listeDemiDPris[i].point2)
        let a, b, co
        let popos = false
        if (this.dist(l1, lm) < this.distcol) {
          gpos.push({
            t: 'pt',
            pt: j3pClone(l1),
            dist: this.dist(l1, lm)
          })
        } else {
          if (l2.x !== l1.x) {
            a = (l2.y - l1.y) / (l2.x - l1.x)
            b = l1.y - a * l1.x
            const a2 = -1 / a
            const b2 = lm.y - a2 * lm.x
            co = { x: (b2 - b) / (a - a2) }
            co.y = a * co.x + b
            popos = (lm.x > l1.x) === (l2.x > l1.x)
          } else {
            co = {
              x: l2.x,
              y: lm.y
            }
            popos = (lm.y > l1.y) === (l2.y > l1.y)
          }
          if (popos) {
            if (this.dist(co, lm) < this.distcol) {
              gpos.push({
                t: 'seg',
                a,
                b,
                dist: this.dist(co, lm),
                co: j3pClone(co)
              })
            }
          }
        }
      }

      // les arc
      for (let i = 0; i < this.listeArcPris.length; i++) {
        const l1 = this.getPointPosition2(this.svgId, this.listeArcPris[i].p1)
        const l2 = this.getPointPosition2(this.svgId, this.listeArcPris[i].p12)
        const l3 = this.getPointPosition2(this.svgId, this.listeArcPris[i].centre)
        if (l1.x !== l2.x || l1.y !== l2.y) {
          if (this.dist(l1, lm) < this.distcol) {
            gpos.push({
              t: 'pt',
              pt: j3pClone(l1),
              dist: this.dist(l1, lm)
            })
          } else if (this.dist(l2, lm) < this.distcol) {
            gpos.push({
              t: 'pt',
              pt: j3pClone(l2),
              dist: this.dist(l2, lm)
            })
          } else {
            let force = false
            const r = this.dist(l1, l3)
            let vpointM = -180 + Math.atan2(lm.x - l3.x, lm.y - l3.y) * 180 / Math.PI
            let vpoint1 = -180 + Math.atan2(l1.x - l3.x, l1.y - l3.y) * 180 / Math.PI
            let vpoint2 = -180 + Math.atan2(l2.x - l3.x, l2.y - l3.y) * 180 / Math.PI
            vpoint1 = this.regule(vpoint1)
            vpoint2 = this.regule(vpoint2)
            if (vpoint2 < vpoint1) vpoint2 += 360
            vpointM = this.regule(vpointM)
            if (this.dist(l1, l2) < 1) force = true
            if ((vpointM > vpoint1 && vpointM < vpoint2) || force) {
              const zi = Math.atan2(lm.x - l3.x, lm.y - l3.y) - Math.PI / 2
              const co = { x: l3.x + r * Math.cos(zi) }
              co.y = l3.y - r * Math.sin(zi)
              if (this.dist(co, lm) < this.distcol) {
                gpos.push({
                  t: 'c',
                  c: j3pClone(l3),
                  r,
                  co: j3pClone(co),
                  dist: this.dist(co, lm)
                })
              }
            }
          }
        }
      }
    }

    if (this.vpi) {
      lm = gpos[0].pt
    } else {
      if (gpos.length === 1) {
        if (gpos[0].co !== undefined) {
          lm = gpos[0].co
        } else { lm = gpos[0].pt }
        if (gpos[0].ang !== undefined) this.mtgAppLecteur.giveFormula2(this.svgId, 'anglebal', -gpos[0].ang, true)
      }
      if (gpos.length > 1) {
        if (yaregl) {
          const poreg = j3pClone(gpos[0])
          for (let i = gpos.length - 1; i > -1; i--) {
            if (gpos[i].t === 'reg' || gpos[i].t === 'pt' || (gpos[i].t === 'seg' && (Math.abs(gpos[i].a - poreg.a) < 0.01))) gpos.splice(i, 1)
          }
          gpos.sort(this.sortOnDistProp)
          if (gpos.length > 0) {
            if (gpos[0].t === 'seg') {
              lm.x = (poreg.b - gpos[0].b) / (gpos[0].a - poreg.a)
              lm.y = poreg.a * lm.x + poreg.b
              if (this.dist(lm, gpos[0].co) > 6) lm = j3pClone(gpos[0].co)
            } else {
              const nnn = this.getInstersectingPoints(poreg.a, poreg.b, gpos[0].c.x, gpos[0].c.y, gpos[0].r)
              switch (nnn.length) {
                case 0:
                  lm = j3pClone(poreg.co)
                  break
                case 1:
                  lm = j3pClone(nnn[0])
                  break
                case 2:
                  if (this.dist(nnn[0], lm) < this.dist(nnn[1], lm)) {
                    lm = j3pClone(nnn[0])
                  } else {
                    lm = j3pClone(nnn[1])
                  }
              }
            }
          } else {
            lm = j3pClone(poreg.co)
            this.mtgAppLecteur.giveFormula2(this.svgId, 'anglebal', -poreg.ang, true)
          }
        } else {
          gpos.sort(this.sortOnDistProp)
          if (gpos[0].t === 'pt') {
            lm = j3pClone(gpos[0].pt)
          } else {
            // on vire les élément pt (cf https://developer.mozilla.org/fr/docs/Web/JavaScript/Reference/Global_Objects/Array/filter)
            gpos = gpos.filter(elt => elt.t !== 'pt')
            let foverif = gpos.length > 1
            while (foverif) {
              foverif = false
              if (gpos[0].t === 'seg' && gpos[1].t === 'seg') {
                if (Math.abs(gpos[0].a - gpos[1].a) < 0.01) {
                  gpos.splice(1, 1)
                  foverif = true
                }
              }
              if (gpos[0].t === 'c' && gpos[1].t === 'c') {
                if (this.dist(gpos[0].c, gpos[1].c) < 10) {
                  gpos.splice(1, 1)
                  foverif = true
                }
              }
              if (gpos.length === 1) foverif = false
            }
            if (gpos.length === 1) {
              lm = j3pClone(gpos[0].co)
            } else {
              if (gpos[0].t === 'seg') {
                if (gpos[1].t === 'seg') {
                  lm.x = (gpos[1].b - gpos[0].b) / (gpos[0].a - gpos[1].a)
                  lm.y = gpos[1].a * lm.x + gpos[1].b
                  if (this.dist(lm, gpos[0].co) > 6) lm = j3pClone(gpos[0].co)
                } else {
                  let nnn
                  try {
                    nnn = this.getInstersectingPoints(gpos[0].a, gpos[0].b, gpos[1].c.x, gpos[1].c.y, gpos[1].r)
                  } catch (error) {
                    const dataSup = {
                      Tommy: {
                        gpos,
                        histoire: 'ca devrait etre au 0 type seg et au 1 type c ?'
                      }
                    }
                    j3pNotify('Pb pour Tommy', dataSup)
                  }
                  switch (nnn.length) {
                    case 0:
                      lm = j3pClone(gpos[0].co)
                      break
                    case 1:
                      lm = j3pClone(nnn[0])
                      break
                    case 2:
                      if (this.dist(nnn[0], lm) < this.dist(nnn[1], lm)) {
                        lm = j3pClone(nnn[0])
                      } else {
                        lm = j3pClone(nnn[1])
                      }
                  }
                }
              } else {
                if (gpos[1].t === 'seg') {
                  const nnn = this.getInstersectingPoints(gpos[1].a, gpos[1].b, gpos[0].c.x, gpos[0].c.y, gpos[0].r)
                  switch (nnn.length) {
                    case 0:
                      lm = j3pClone(gpos[0].co)
                      break
                    case 1:
                      lm = j3pClone(nnn[0])
                      break
                    case 2:
                      if (this.dist(nnn[0], lm) < this.dist(nnn[1], lm)) {
                        lm = j3pClone(nnn[0])
                      } else {
                        lm = j3pClone(nnn[1])
                      }
                  }
                } else {
                  let nnn
                  try {
                    nnn = this.inter2cercles(gpos[0].c, gpos[0].r, gpos[1].c, gpos[1].r)
                  } catch (error) {
                    const dataSup = {
                      pbTom: {
                        gpos,
                        histoire: 'Il devrait y avoir 2 cercles'
                      }
                    }
                    j3pNotify('Anomalie pour Tom', dataSup)
                  }
                  switch (nnn.length) {
                    case 0:
                      lm = j3pClone(gpos[0].co)
                      break
                    case 1:
                      lm = j3pClone(nnn[0])
                      break
                    case 2:
                      if (this.dist(nnn[0], lm) < this.dist(nnn[1], lm)) {
                        lm = j3pClone(nnn[0])
                      } else {
                        lm = j3pClone(nnn[1])
                      }
                  }
                }
              }
            }
          }
        }
      }
    }

    deltax = deltax + lm.x - lmold.x
    deltay = deltay + lm.y - lmold.y
    this.mtgAppLecteur.setPointPosition(this.svgId, '#' + this.idCompPt.pointe, this.basecomp.x + deltax, this.basecomp.y + deltay, false)
    this.mtgAppLecteur.setPointPosition(this.svgId, '#ptverB', this.basecompv.x + deltax, this.basecompv.y + deltay, false)
    this.mtgAppLecteur.setPointPosition(this.svgId, '#' + this.idCompPt.geremine, this.basecompgere.x + deltax, this.basecompgere.y + deltay, false)
    this.mtgAppLecteur.updateFigure(this.svgId)
  }

  distangle (a1, a2) {
    const b1 = this.regule(a1)
    const b2 = this.regule(a2)
    const g = Math.max(b1, b2)
    const p = Math.min(b1, b2)
    return Math.min(g - p, 360 - g + p)
  }

  renvoieCrayon (b) {
    this.lepoint2 = this.lepoint22 = false
    this.mtgAppLecteur.setPointPosition(this.svgId, this.idImCrayPt, 1500, 42, false)
    if (b !== false) this.mtgAppLecteur.updateFigure(this.svgId)
  }

  recentrebis () {
    this.recentre(false, true)
  }

  recentre (b, b2, b3, force) {
    this.consoleDev('recentre')
    this.lepoint = false
    this.lepoint4 = false
    if (this.txtF !== 'FigEditor') this.lepointVire()
    if (this.unecapt() && !force) return
    this.consoleDev('recentre2')
    if (this.mtgAppLecteur.getDoc(this.svgId).pointCapture) return
    if (b2) this.montrexzntre()
    this.metToutFalse()
    const X = this.lXcentre
    const Y = this.lYcentre
    this.consoleDev('recentre3')
    const pt = this.getPointPosition2(this.svgId, 'Z2')
    let deltax, deltay
    if (b === true) {
      deltax = this.lXcentre - this.bary.x
      deltay = this.lYcentre - this.bary.y
    } else {
      deltax = X - pt.x
      deltay = Y - pt.y
    }
    if (b !== true) {
      for (let i = 0; i < this.ptsabouge.length; i++) {
        this.mtgAppLecteur.translatePoint(this.svgId, this.ptsabouge[i], deltax, deltay, false)
      }
    }
    for (let i = 0; i < this.listeSegPris.length; i++) {
      this.mtgAppLecteur.translatePoint(this.svgId, this.listeSegPris[i].point1, deltax, deltay, false)
      this.mtgAppLecteur.translatePoint(this.svgId, this.listeSegPris[i].point2, deltax, deltay, false)
      if (this.listeSegPris[i].pointNom) this.mtgAppLecteur.translatePoint(this.svgId, this.listeSegPris[i].pointNom, deltax, deltay, false)
    }
    for (let i = 0; i < this.listeDtePris.length; i++) {
      this.mtgAppLecteur.translatePoint(this.svgId, this.listeDtePris[i].point1, deltax, deltay, false)
      this.mtgAppLecteur.translatePoint(this.svgId, this.listeDtePris[i].point2, deltax, deltay, false)
      if (this.listeDtePris[i].pointNom) this.mtgAppLecteur.translatePoint(this.svgId, this.listeDtePris[i].pointNom, deltax, deltay, false)
    }
    for (let i = 0; i < this.listeDemiDPris.length; i++) {
      this.mtgAppLecteur.translatePoint(this.svgId, this.listeDemiDPris[i].point1, deltax, deltay, false)
      this.mtgAppLecteur.translatePoint(this.svgId, this.listeDemiDPris[i].point2, deltax, deltay, false)
      if (this.listeDemiDPris[i].pointNom) this.mtgAppLecteur.translatePoint(this.svgId, this.listeDemiDPris[i].pointNom, deltax, deltay, false)
    }
    for (let i = 0; i < this.listeArcPris.length; i++) {
      this.mtgAppLecteur.translatePoint(this.svgId, this.listeArcPris[i].centre, deltax, deltay, false)
      for (let j = 1; j < 13; j++) {
        this.mtgAppLecteur.translatePoint(this.svgId, this.listeArcPris[i]['p' + j], deltax, deltay, false)
      }
      if (this.listeArcPris[i].pointNom) this.mtgAppLecteur.translatePoint(this.svgId, this.listeArcPris[i].pointNom, deltax, deltay, false)
    }
    for (let i = 0; i < this.listePointPris.length; i++) {
      this.mtgAppLecteur.translatePoint(this.svgId, this.listePointPris[i].point, deltax, deltay, false)
    }
    for (let i = this.ConsSegnb; i < this.segCo.length; i++) {
      this.mtgAppLecteur.translatePoint(this.svgId, this.segCo[i].html.point1, deltax, deltay, false)
      this.mtgAppLecteur.translatePoint(this.svgId, this.segCo[i].html.point2, deltax, deltay, false)
    }
    for (let i = this.ConsDtenb; i < this.dteco.length; i++) {
      this.mtgAppLecteur.translatePoint(this.svgId, this.dteco[i].html.point1, deltax, deltay, false)
      this.mtgAppLecteur.translatePoint(this.svgId, this.dteco[i].html.point2, deltax, deltay, false)
    }
    for (let i = this.ConsDemiDnb; i < this.demiDco.length; i++) {
      this.mtgAppLecteur.translatePoint(this.svgId, this.demiDco[i].html.point1, deltax, deltay, false)
      this.mtgAppLecteur.translatePoint(this.svgId, this.demiDco[i].html.point2, deltax, deltay, false)
    }
    for (let i = this.ConsArcnb; i < this.arcco.length; i++) {
      this.mtgAppLecteur.translatePoint(this.svgId, this.arcco[i].html.centre, deltax, deltay, false)
      this.mtgAppLecteur.translatePoint(this.svgId, this.arcco[i].html.p1, deltax, deltay, false)
      this.mtgAppLecteur.translatePoint(this.svgId, this.arcco[i].html.p2, deltax, deltay, false)
    }
    for (let i = this.ConsPtnb; i < this.ptCo.length; i++) {
      this.mtgAppLecteur.translatePoint(this.svgId, this.ptCo[i].html.point, deltax, deltay, false)
    }
    for (let i = 0; i < this.nomco.length; i++) {
      this.mtgAppLecteur.translatePoint(this.svgId, this.nomco[i].html[0], deltax, deltay, false)
    }

    if (this.PtArrive !== undefined) {
      this.PtArrive.x += deltax
      this.PtArrive.y += deltay
    }
    if (this.txtF !== 'FigEditor') {
      this.equerreVisible = true
      this.rapporteurVisible = true
      this.regleVisible = true
      this.compasVisible = true
      this.lagrad = false
      this.regle(false)
      this.equerre(false)
      this.rapporteur(false)
      this.compas(false, false, false)
      this.eteintPoint(false)
    }
    if (b3 !== false) this.mtgAppLecteur.updateFigure(this.svgId)
    setTimeout(this.virecentre.bind(this), 500)
    this.consoleDev('recentreFin')
  }

  rot () {
    this.viregommEtrot(true)
    let zt
    this.larot = !this.larot
    this.lepoint = false
    this.lepoint4 = false
    this.lepointVire()
    this.centreRota = this.mtgAppLecteur.getPointPosition({ absCoord: true, a: 'Z2' })
    if (this.larot) {
      this.mtgAppLecteur.setVisible({ elt: 'polyrot' })
      // this.mtgAppLecteur.setVisible({ elt: 'segrot' })
      this.mtgAppLecteur.setVisible({ elt: 'segrot2' })
      this.mtgAppLecteur.setVisible({ elt: 'fondrot' })
      this.mtgAppLecteur.setVisible({ elt: 'ptbougrot' })
      this.mtgAppLecteur.setVisible({ elt: 'ptvizrot' })
      this.mtgAppLecteur.setVisible({ elt: 'cerclerot' })
    } else {
      this.mtgAppLecteur.setHidden({ elt: 'polyrot' })
      // this.mtgAppLecteur.setHidden({ elt: 'segrot' })
      this.mtgAppLecteur.setHidden({ elt: 'segrot2' })
      this.mtgAppLecteur.setHidden({ elt: 'fondrot' })
      this.mtgAppLecteur.setHidden({ elt: 'ptbougrot' })
      this.mtgAppLecteur.setHidden({ elt: 'ptvizrot' })
      this.mtgAppLecteur.setHidden({ elt: 'cerclerot' })
    }
    if (this.larot) {
      zt = this.mtgAppLecteur.getPointPosition({ absCoord: true, a: 'ptvizrot' })
      this.mtgAppLecteur.setPointPosition(this.svgId, '#ptbougrot', zt.x, zt.y, false)
      this.larotmontre()
    } else { this.larotVire() }
    this.equerreVisible = true
    this.rapporteurVisible = true
    this.regleVisible = true
    this.compasVisible = true
    this.lagrad = false
    this.regle(false)
    this.equerre(false)
    this.rapporteur(false)
    this.compas()
  }

  agrand () {
    this.faiszomm(true)
    this.bloqueEventm = false
    setTimeout(this.vireplus.bind(this), 50)
  }

  eteintPoint (b) {
    for (let i = 0; i < this.listePointPris.length; i++) {
      if (this.listePointPris[i].viz) {
        this.setColor2(this.svgId, this.listePointPris[i].point, this.listePointPris[i].color[0], this.listePointPris[i].color[1], this.listePointPris[i].color[2], false)
      }
    }
    if (b !== false) this.mtgAppLecteur.updateFigure(this.svgId)
  }

  gomme () {
    if (this.unecapt()) return
    this.whereAfficheAide.style.display = 'none'
    const acons = this.bloqueEventm
    this.yacoul = true
    this.faisCoul()
    this.eteintPoint(false)
    this.lepoint = false
    this.lepoint4 = false
    this.lepointVire()
    this.poitilEncours = false
    this.pointilleVire()
    this.larot = false
    this.larotVire()
    this.lagomm = !this.lagomm
    if (!this.lagomm) {
      this.lagomVire()
    } else {
      this.lagommontre()
    }
    this.equerreVisible = true
    this.rapporteurVisible = true
    this.regleVisible = true
    this.compasVisible = true
    this.lagrad = false
    this.regle(false)
    this.equerre(false)
    this.rapporteur(false)
    this.compas()
    if (this.lagomm) this.whereAf('gomme')
    if (this.lagomm && acons) {
      this.functiontouchstart()
    }
  }

  pointill () {
    if (this.unecapt()) return
    // const acons = this.bloqueEventm
    this.yacoul = true
    this.faisCoul()
    this.eteintPoint(false)
    this.lepoint = false
    this.lepoint4 = false
    this.lepointVire()
    this.poitilEncours = !this.poitilEncours
    if (!this.poitilEncours) {
      this.pointilleVire()
    } else {
      this.pointillemontre()
    }
    this.viregommEtrot('p')
    this.equerreVisible = true
    this.rapporteurVisible = true
    this.regleVisible = true
    this.compasVisible = true
    this.lagrad = false
    this.regle(false)
    this.equerre(false)
    this.rapporteur(false)
    this.compas()
    if (this.poitilEncours) {
      this.pointillemontre()
    }
  }

  reduit () {
    this.faiszomm(false)
    this.bloqueEventm = false
    setTimeout(this.vireplus.bind(this), 50)
  }

  colleEquerre (deltax, deltay) {
    let mm = false
    let aver3, kiki
    let pt = { x: this.baseeq.x + deltax, y: this.baseeq.y + deltay }
    const ptS = { x: this.baseeq.x + deltax, y: this.baseeq.y + deltay }
    let pt4 = { x: this.baseeq3.x + deltax, y: this.baseeq3.y + deltay }
    const pt5 = { x: this.baseeq4.x + deltax, y: this.baseeq4.y + deltay }

    if (this.regleVisible) {
      const aver2 = Math.atan2(pt.x - pt4.x, pt4.y - pt.y) * 180 / Math.PI
      // cherche si droite passe pres du coin
      const pt3 = this.getPointPosition2(this.svgId, this.idPtRegle.pt)
      const pt2 = this.getPointPosition2(this.svgId, '#rg234')
      const aver = Math.atan2(pt2.x - pt3.x, pt3.y - pt2.y) * 180 / Math.PI
      if (this.distPtDtebis(pt3, pt, pt2) < 0.5) {
        if (this.distangle(aver, aver2 - 90) < 5) {
          const a = (pt2.y - pt3.y) / (pt2.x - pt3.x)
          const a2 = (a === 0) ? 9999999999 : -1 / a
          const b = pt3.y - a * pt3.x
          let b2 = pt.y - a2 * pt.x
          const ag = j3pClone(pt)
          if (pt2.x === pt3.x) {
            pt.x = pt2.x
          } else {
            pt = { x: (b2 - b) / (a - a2) }
            pt.y = a2 * pt.x + b2
          }
          if (Math.abs(a) < Math.pow(10, -5)) {
            pt.x = ag.x
            pt.y = pt2.y
          }
          if (a < -99999999 || a > 99999999) {
            pt.x = pt3.x
            pt.y = ag.y
          }
          const aver3 = aver + 90
          let lp = []
          const l2 = {
            x: pt.x + 300 * this.zoom * Math.cos((aver3 + 90) * Math.PI / 180),
            y: pt.y + 300 * this.zoom * Math.sin((aver3 + 90) * Math.PI / 180)
          }
          // test si ya point pas loin
          for (let i = 0; i < this.listePointPris.length; i++) {
            const l1 = this.getPointPosition2(this.svgId, this.listePointPris[i].point)
            if (Math.abs(a) < Math.pow(10, -5)) {
              if (this.distPtDtebis(pt, l1, l2) < 0.5 && (l1.y > Math.min(pt.y, l2.y) - 0.1 * this.unite && (l1.y < Math.max(pt.y, l2.y) + 0.1 * this.unite))) {
                lp.push({
                  dist: this.distPtDtebis(pt, l1, l2),
                  co: j3pClone(l1)
                })
              }
            } else {
              if (this.distPtDtebis(pt, l1, l2) < 0.5 && (l1.x > Math.min(pt.x, l2.x) - 0.1 * this.unite && (l1.x < Math.max(pt.x, l2.x) + 0.1 * this.unite))) {
                lp.push({
                  dist: this.distPtDtebis(pt, l1, l2),
                  co: j3pClone(l1)
                })
              }
            }
          }
          if (lp.length > 0) {
            lp = lp.sort((a, b) => a.dist - b.dist)
            const co = {
              x: lp[0].co.x - 0.06 * this.unite * Math.cos(aver3 * Math.PI / 180),
              y: lp[0].co.y - 0.06 * this.unite * Math.sin(aver3 * Math.PI / 180)
            }
            const ag = j3pClone(pt)
            b2 = co.y - a2 * co.x
            pt = { x: (b2 - b) / (a - a2) }
            pt.y = a2 * pt.x + b2
            if (Math.abs(a) < Math.pow(10, -5)) {
              if (pt4.y > pt.y) {
                pt.x = lp[0].co.x - 0.06 * this.unite
              } else {
                pt.x = lp[0].co.x + 0.06 * this.unite
              }
              pt.y = ag.y
            }
            if (a < -99999999 || a > 99999999) {
              if (pt4.x > pt.x) {
                pt.y = lp[0].co.y + 0.06 * this.unite
              } else {
                pt.y = lp[0].co.y - 0.06 * this.unite
              }
              pt.x = ag.x
            }
          }

          this.mtgAppLecteur.setPointPosition(this.svgId, '#pteqma0', pt.x + 124 * this.zoom * Math.cos((aver3 - 45 + 180) * Math.PI / 180), pt.y + 124 * this.zoom * Math.sin((aver3 - 45 + 180) * Math.PI / 180), false)
          this.mtgAppLecteur.setPointPosition(this.svgId, this.idPtEquerre.pt, pt.x, pt.y, false)
          this.mtgAppLecteur.updateFigure(this.svgId)
          return
        }

        if (this.distangle(aver, aver2) < 5) {
          const a = (pt2.y - pt3.y) / (pt2.x - pt3.x)
          const a2 = (a === 0) ? 9999999999 : -1 / a
          const b = pt3.y - a * pt3.x
          // FIXME expliquer pourquoi on prend ici la let pt défini au tout début de colleEquerre, c’est pas un bug ?
          let b2 = pt.y - a2 * pt.x
          const ag = j3pClone(pt)
          pt = { x: (b2 - b) / (a - a2) }
          pt.y = a2 * pt.x + b2
          aver3 = aver
          if (Math.abs(a) < Math.pow(10, -5)) {
            pt.x = ag.x
            pt.y = pt2.y
          }
          if (a < -99999999 || a > 99999999) {
            pt.x = pt3.x
            pt.y = ag.y
          }
          let lp = []
          const l2 = {
            x: pt.x + 150 * this.zoom * Math.cos((aver3 + 180) * Math.PI / 180),
            y: pt.y + 150 * this.zoom * Math.sin((aver3 + 180) * Math.PI / 180)
          }
          // test si ya point pas loin
          for (let i = 0; i < this.listePointPris.length; i++) {
            if (Math.abs(a) < Math.pow(10, -5)) {
              const l1 = this.getPointPosition2(this.svgId, this.listePointPris[i].point)
              if (this.distPtDtebis(pt, l1, l2) < 0.5 && (l1.y > Math.min(pt.y, l2.y) - 0.1 * this.unite && (l1.y < Math.max(pt.y, l2.y) + 0.1 * this.unite))) {
                lp.push({ dist: this.distPtDtebis(pt, l1, l2), co: j3pClone(l1) })
              }
            } else {
              const l1 = this.getPointPosition2(this.svgId, this.listePointPris[i].point)
              if (this.distPtDtebis(pt, l1, l2) < 0.5 && (l1.x > Math.min(pt.x, l2.x) - 0.1 * this.unite && (l1.x < Math.max(pt.x, l2.x) + 0.1 * this.unite))) {
                lp.push({ dist: this.distPtDtebis(pt, l1, l2), co: j3pClone(l1) })
              }
            }
          }
          if (lp.length > 0) {
            lp = lp.sort((a, b) => a.dist - b.dist)
            const co = {
              x: lp[0].co.x + 0.06 * this.unite * Math.cos((aver3 + 90) * Math.PI / 180),
              y: lp[0].co.y + 0.06 * this.unite * Math.sin((aver3 + 90) * Math.PI / 180)
            }
            b2 = co.y - a2 * co.x
            const ag = j3pClone(pt)
            pt = { x: (b2 - b) / (a - a2) }
            pt.y = a2 * pt.x + b2
            if (Math.abs(a) < Math.pow(10, -5)) {
              if (pt4.x < pt.x) {
                pt.x = lp[0].co.x - 0.06 * this.unite
              } else {
                pt.x = lp[0].co.x + 0.06 * this.unite
              }
              pt.y = ag.y
            }
            if (a < -99999999 || a > 99999999) {
              if (pt4.y < pt.y) {
                pt.y = lp[0].co.y - 0.06 * this.unite
              } else {
                pt.y = lp[0].co.y + 0.06 * this.unite
              }
              pt.x = ag.x
            }
          }
          this.mtgAppLecteur.setPointPosition(this.svgId, '#pteqma0', pt.x + 124 * this.zoom * Math.cos((aver3 - 45 + 180) * Math.PI / 180), pt.y + 124 * this.zoom * Math.sin((aver3 - 45 + 180) * Math.PI / 180), false)
          this.mtgAppLecteur.setPointPosition(this.svgId, this.idPtEquerre.pt, pt.x, pt.y, false)
          this.mtgAppLecteur.updateFigure(this.svgId)
          return
        }
      } else {
        const pt3 = this.getPointPosition2(this.svgId, '#regptf1')
        const pt2 = this.getPointPosition2(this.svgId, '#regptf2')
        const aver = Math.atan2(pt2.x - pt3.x, pt3.y - pt2.y) * 180 / Math.PI
        if (this.distPtDtebis(pt3, pt, pt2) < 0.5) {
          if (this.distangle(aver, aver2 + 90) < 5) {
            const a = (pt2.y - pt3.y) / (pt2.x - pt3.x)
            const a2 = (a === 0) ? 9999999999 : -1 / a
            const b = pt3.y - a * pt3.x
            let b2 = pt.y - a2 * pt.x
            const ag = j3pClone(pt)
            pt = { x: (b2 - b) / (a - a2) }
            pt.y = a2 * pt.x + b2
            if (Math.abs(a) < Math.pow(10, -5)) {
              pt.x = ag.x
              pt.y = pt2.y
            }
            if (a < -99999999 || a > 99999999) {
              pt.x = pt3.x
              pt.y = ag.y
            }
            aver3 = aver - 90

            let lp = []
            const l2 = {
              x: pt.x + 300 * this.zoom * Math.cos((aver3 + 90) * Math.PI / 180),
              y: pt.y + 300 * this.zoom * Math.sin((aver3 + 90) * Math.PI / 180)
            }
            // test si ya point pas loin
            for (let i = 0; i < this.listePointPris.length; i++) {
              const l1 = this.getPointPosition2(this.svgId, this.listePointPris[i].point)
              if (this.distPtDtebis(pt, l1, l2) < 0.5 && (l1.x > Math.min(pt.x, l2.x) - 0.1 * this.unite && (l1.x < Math.max(pt.x, l2.x) + 0.1 * this.unite))) {
                lp.push({ dist: this.distPtDtebis(pt, l1, l2), co: j3pClone(l1) })
              }
            }
            if (lp.length > 0) {
              lp = lp.sort(this.sortOnDistProp)
              const co = {
                x: lp[0].co.x - 0.06 * this.unite * Math.cos(aver3 * Math.PI / 180),
                y: lp[0].co.y - 0.06 * this.unite * Math.sin(aver3 * Math.PI / 180)
              }
              b2 = co.y - a2 * co.x
              pt = { x: (b2 - b) / (a - a2) }
              pt.y = a2 * pt.x + b2
            }

            this.mtgAppLecteur.setPointPosition(this.svgId, '#pteqma0', pt.x + 124 * this.zoom * Math.cos((aver3 - 45 + 180) * Math.PI / 180), pt.y + 124 * this.zoom * Math.sin((aver3 - 45 + 180) * Math.PI / 180), false)
            this.mtgAppLecteur.setPointPosition(this.svgId, this.idPtEquerre.pt, pt.x, pt.y, false)
            this.mtgAppLecteur.updateFigure(this.svgId)
            return
          }

          if (this.distangle(aver, aver2 + 180) < 5) {
            const a = (pt2.y - pt3.y) / (pt2.x - pt3.x)
            const a2 = (a === 0) ? 9999999999 : -1 / a
            const b = pt3.y - a * pt3.x
            let b2 = pt.y - a2 * pt.x
            const ag = j3pClone(pt)
            pt = { x: (b2 - b) / (a - a2) }
            pt.y = a2 * pt.x + b2
            aver3 = aver + 180
            if (Math.abs(a) < Math.pow(10, -5)) {
              pt.x = ag.x
              pt.y = pt2.y
            }
            if (a < -99999999 || a > 99999999) {
              pt.x = pt3.x
              pt.y = ag.y
            }
            let lp = []
            const l2 = {
              x: pt.x + 150 * this.zoom * Math.cos((aver3 + 180) * Math.PI / 180),
              y: pt.y + 150 * this.zoom * Math.sin((aver3 + 180) * Math.PI / 180)
            }
            // test si ya point pas loin
            for (let i = 0; i < this.listePointPris.length; i++) {
              const l1 = this.getPointPosition2(this.svgId, this.listePointPris[i].point)
              if (this.distPtDtebis(pt, l1, l2) < 0.5 && (l1.x > Math.min(pt.x, l2.x) - 0.1 * this.unite && (l1.x < Math.max(pt.x, l2.x) + 0.1 * this.unite))) {
                lp.push({ dist: this.distPtDtebis(pt, l1, l2), co: j3pClone(l1) })
              }
            }
            if (lp.length > 0) {
              lp = lp.sort(this.sortOnDistProp)
              const co = {
                x: lp[0].co.x + 0.06 * this.unite * Math.cos((aver3 + 90) * Math.PI / 180),
                y: lp[0].co.y + 0.06 * this.unite * Math.sin((aver3 + 90) * Math.PI / 180)
              }
              b2 = co.y - a2 * co.x
              pt = { x: (b2 - b) / (a - a2) }
              pt.y = a2 * pt.x + b2
            }

            this.mtgAppLecteur.setPointPosition(this.svgId, '#pteqma0', pt.x + 124 * this.zoom * Math.cos((aver3 - 45 + 180) * Math.PI / 180), pt.y + 124 * this.zoom * Math.sin((aver3 - 45 + 180) * Math.PI / 180), false)
            this.mtgAppLecteur.setPointPosition(this.svgId, this.idPtEquerre.pt, pt.x, pt.y, false)
            this.mtgAppLecteur.updateFigure(this.svgId)
            return
          }
        }
      }
    }
    // pour chaque point test distance a seg regle
    const lp = []
    const lp2 = []
    mm = false
    for (let i = 0; i < this.listeDtePris.length; i++) {
      // test prox coin

      // test aver
      if (this.listeDtePris[i].viz === false) continue
      const pt2 = this.getPointPosition2(this.svgId, this.listeDtePris[i].point1)
      const pt3 = this.getPointPosition2(this.svgId, this.listeDtePris[i].point2)

      if (this.distPtDtebis(pt2, pt, pt3) < 0.5) {
        const aver = Math.atan2(pt2.x - pt3.x, pt3.y - pt2.y) * 180 / Math.PI
        const aver2 = Math.atan2(pt4.x - pt.x, pt.y - pt4.y) * 180 / Math.PI
        if (this.distangle(aver, aver2) < 5) {
          aver3 = 45 - aver
          mm = true
          kiki = true
        }
        if (this.distangle(aver, aver2 - 180) < 5) {
          aver3 = 45 - aver + 180
          mm = true
          kiki = true
        }

        if (this.distangle(aver, aver2 + 90) < 5) {
          aver3 = 45 - aver + 90
          mm = true
          kiki = false
        }
        if (this.distangle(aver, aver2 - 90) < 5) {
          aver3 = 45 - aver - 90
          mm = true
          kiki = false
        }

        if (mm) {
          lp2.push({ pt2, pt3, aver3 })
          break
        }
      }
    }

    for (let i = 0; i < this.listeSegPris.length; i++) {
      // test prox coin
      if (mm) break

      // test aver
      if (this.listeSegPris[i].viz === false) continue
      const pt2 = this.getPointPosition2(this.svgId, this.listeSegPris[i].point1)
      const pt3 = this.getPointPosition2(this.svgId, this.listeSegPris[i].point2)
      if (this.distPtSeg2(pt2, pt3, pt) < 1 || this.distPtSeg2(pt, pt4, pt2) < 1 || this.distPtSeg2(pt, pt4, pt3) < 1 || this.distPtSeg2(pt, pt5, pt2) < 1 || this.distPtSeg2(pt, pt5, pt3) < 1) {
        const distComp = this.dist(pt2, pt3)
        const regLo = 20
        const ptDehors = this.dist(pt2, pt) > distComp - regLo || this.dist(pt3, pt) > distComp - regLo
        const pt4Dehors = this.dist(pt2, pt4) > distComp - regLo || this.dist(pt3, pt4) > distComp - regLo
        const pt5Dehors = this.dist(pt2, pt5) > distComp - regLo || this.dist(pt3, pt5) > distComp - regLo
        let ptautreDehors = true
        for (let u = 1; u < 5; u++) {
          const pt7 = { x: pt.x + u * (pt5.x - pt.x) / 5, y: pt.y + u * (pt5.y - pt.y) / 5 }
          ptautreDehors = ptautreDehors && (this.dist(pt2, pt7) > distComp - regLo || this.dist(pt3, pt7) > distComp - regLo)
        }
        for (let u = 1; u < 10; u++) {
          const pt7 = { x: pt.x + u * (pt4.x - pt.x) / 5, y: pt.y + u * (pt4.y - pt.y) / 5 }
          ptautreDehors = ptautreDehors && (this.dist(pt2, pt7) > distComp - regLo || this.dist(pt3, pt7) > distComp - regLo)
        }
        if (ptDehors && pt4Dehors && pt5Dehors && ptautreDehors) continue

        const aver = this.regule(Math.atan2(pt2.x - pt3.x, pt3.y - pt2.y) * 180 / Math.PI)
        const aver2 = this.regule(Math.atan2(pt4.x - pt.x, pt.y - pt4.y) * 180 / Math.PI)
        let pt6 = {
          x: pt.x + 11 * this.unite * this.zoom * Math.cos((aver2 - 90) * Math.PI / 180),
          y: pt.y + 11 * this.unite * this.zoom * Math.sin((aver2 - 90) * Math.PI / 180)
        }

        if (this.distPtSeg2(pt, pt6, pt2) < 1 || this.distPtSeg2(pt, pt6, pt3) < 1 || this.distPtSeg2(pt2, pt3, pt6) < 1 || this.distPtSeg2(pt2, pt3, pt) < 1) {
          if (this.distangle(aver, aver2) < 5) {
            aver3 = 45 - aver
            mm = true
            kiki = true
          }
          if (this.distangle(aver, aver2 - 180) < 5) {
            aver3 = 45 - aver + 180
            mm = true
            kiki = true
          }
        }

        pt6 = {
          x: pt.x + 5.5 * this.unite * this.zoom * Math.cos((aver2) * Math.PI / 180),
          y: pt.y + 5.5 * this.unite * this.zoom * Math.sin((aver2) * Math.PI / 180)
        }

        if (this.distPtSeg2(pt, pt6, pt2) < 1 || this.distPtSeg2(pt, pt6, pt3) < 1 || this.distPtSeg2(pt2, pt3, pt6) < 1 || this.distPtSeg2(pt2, pt3, pt) < 1) {
          if (this.distangle(aver, aver2 + 90) < 5) {
            aver3 = 45 - aver + 90
            mm = true
            kiki = false
          }
          if (this.distangle(aver, aver2 - 90) < 5) {
            aver3 = 45 - aver - 90
            mm = true
            kiki = false
          }
        }
        if (mm) {
          lp2.push({ pt2, pt3, aver3, seg: true })
          break
        }
      }
    }

    for (let i = 0; i < this.listeDemiDPris.length; i++) {
      // test prox coin
      if (mm) break

      // test aver
      if (this.listeDemiDPris[i].viz === false) continue
      const pt2 = this.getPointPosition2(this.svgId, this.listeDemiDPris[i].point1)
      const pt3 = this.getPointPosition2(this.svgId, this.listeDemiDPris[i].point2)

      if (this.distPtDtebis(pt2, pt3, pt) < 3) {
        const aver = this.regule(Math.atan2(pt2.x - pt3.x, pt3.y - pt2.y) * 180 / Math.PI)
        const aver2 = this.regule(Math.atan2(pt4.x - pt.x, pt.y - pt4.y) * 180 / Math.PI)
        pt4 = {
          x: pt.x + 11 * this.unite * this.zoom * Math.cos((aver2 - 90) * Math.PI / 180),
          y: pt.y + 11 * this.unite * this.zoom * Math.sin((aver2 - 90) * Math.PI / 180)
        }

        if (this.distPtDemiDtebis(pt2, pt, pt3) < 5 || this.distPtDemiDtebis(pt2, pt4, pt3) < 5) {
          if (this.distangle(aver, aver2) < 5) {
            aver3 = 45 - aver
            mm = true
            kiki = true
          }
          if (this.distangle(aver, aver2 - 180) < 5) {
            aver3 = 45 - aver + 180
            mm = true
            kiki = true
          }
        }

        pt4 = {
          x: pt.x + 5.5 * this.unite * this.zoom * Math.cos((aver2) * Math.PI / 180),
          y: pt.y + 5.5 * this.unite * this.zoom * Math.sin((aver2) * Math.PI / 180)
        }

        if (this.distPtDemiDtebis(pt2, pt, pt3) < 5 || this.distPtDemiDtebis(pt2, pt4, pt3) < 5) {
          if (this.distangle(aver, aver2 + 90) < 5) {
            aver3 = 45 - aver + 90
            mm = true
            kiki = false
          }
          if (this.distangle(aver, aver2 - 90) < 5) {
            aver3 = 45 - aver - 90
            mm = true
            kiki = false
          }
        }
        if (mm) lp2.push({ pt2, pt3, aver3 })

        break
      }
    }
    let zzz
    if (lp2.length !== 0) {
      zzz = lp2[0]
      const a = (zzz.pt2.y - zzz.pt3.y) / (zzz.pt2.x - zzz.pt3.x)
      const b = zzz.pt3.y - a * zzz.pt3.x
      const a2 = (a === 0) ? 9999999999 : -1 / a
      const b2 = pt.y - a2 * pt.x
      const co = { x: (b2 - b) / (a - a2) }
      co.y = a * co.x + b
      if (Math.abs(a) > 99999999) {
        co.x = zzz.pt2.x
        co.y = pt.y
      }
      if (Math.abs(a) < 0.00001) {
        co.x = pt.x
        co.y = zzz.pt2.y
      }
      deltax = co.x - this.baseeq.x
      deltay = co.y - this.baseeq.y

      const pasb = { x: this.baseeq.x + deltax, y: this.baseeq.y + deltay }
      let pasb2
      if (!kiki) {
        pasb2 = {
          x: this.baseeq.x + deltax + 300 * this.zoom * Math.cos((zzz.aver3 + 45) * Math.PI / 180),
          y: this.baseeq.y + deltay - 300 * this.zoom * Math.sin((zzz.aver3 + 45) * Math.PI / 180)
        }
      } else {
        pasb2 = {
          x: this.baseeq.x + deltax + 150 * this.zoom * Math.cos((zzz.aver3 - 45) * Math.PI / 180),
          y: this.baseeq.y + deltay - 150 * this.zoom * Math.sin((zzz.aver3 - 45) * Math.PI / 180)
        }
      }

      for (let i = 0; i < this.listePointPris.length; i++) {
        if (this.listePointPris[i].viz === false) continue
        const l1 = this.getPointPosition2(this.svgId, this.listePointPris[i].point)
        if (this.distPtSeg2(pasb, pasb2, l1) * this.unite < this.distcol) {
          lp.push({ dist: this.distPtSeg2(pasb, pasb2, l1) * this.unite, co: j3pClone(l1) })
        }
      }
      if (lp.length > 0) {
        let mmmod = -45
        if (kiki) mmmod = +45
        lp.sort(this.sortOnDistProp)
        const co = {
          x: lp[0].co.x + 0.06 * this.unite * Math.cos((zzz.aver3 + mmmod) * Math.PI / 180),
          y: lp[0].co.y - 0.06 * this.unite * Math.sin((zzz.aver3 + mmmod) * Math.PI / 180)
        }
        const b2 = co.y - a2 * co.x
        pt = { x: (b2 - b) / (a - a2) }
        pt.y = a2 * pt.x + b2
        if (Math.abs(a) < 0.00001) {
          pt.x = co.x
          pt.y = zzz.pt2.y
        }
        if (Math.abs(a) > 99999999) {
          if (pt5.y > ptS.y) {
            pt.x = zzz.pt2.x + 0.06 * this.unite
          } else {
            pt.x = zzz.pt2.x - 0.06 * this.unite
          }
          pt.y = co.y
        }
        deltax = pt.x - this.baseeq.x
        deltay = pt.y - this.baseeq.y
      }

      this.mtgAppLecteur.setPointPosition(this.svgId, '#pteqma0', this.baseeq.x + deltax + 124 * this.zoom * Math.cos(zzz.aver3 * Math.PI / 180), this.baseeq.y + deltay - 124 * this.zoom * Math.sin(zzz.aver3 * Math.PI / 180), false)
      this.zaconseq = zzz.aver3
      this.fochgeq = true
    }

    this.mtgAppLecteur.setPointPosition(this.svgId, this.idPtEquerre.pt, this.baseeq.x + deltax, this.baseeq.y + deltay, false)

    if (!mm) {
      this.mtgAppLecteur.setPointPosition(this.svgId, '#pteqma0', this.baseeq22.x + deltax, this.baseeq22.y + deltay, true)
      this.mtgAppLecteur.updateFigure(this.svgId)
      if (this.fochgeq) {
        const aver2 = (this.zaconseq + 2)
        this.baseeq22 = {
          x: this.baseeq.x + 124 * this.zoom * Math.cos(aver2 * Math.PI / 180),
          y: this.baseeq.y - 124 * this.zoom * Math.sin(aver2 * Math.PI / 180)
        }
        this.mtgAppLecteur.setPointPosition(this.svgId, '#pteqma0', this.baseeq22.x + deltax, this.baseeq22.y + deltay, true)
        this.fochgeq = false
      }
    }
    if (mm) {
      // FIXME zzz peut être undefined si on est pas passé dans le if qui lui affecte ses propriétés (bcp plus haut)
      this.baseeq22 = {
        x: this.baseeq.x + deltax + 124 * this.zoom * Math.cos(zzz.aver3 * Math.PI / 180),
        y: this.baseeq.y + deltay - 124 * this.zoom * Math.sin(zzz.aver3 * Math.PI / 180)
      }
    }
    this.mtgAppLecteur.updateFigure(this.svgId)
    const pt1 = this.getPointPosition2(this.svgId, '#Eq234')
    this.baseeq3 = { x: pt1.x - deltax, y: pt1.y - deltay }
    const pt2 = this.getPointPosition2(this.svgId, '#eq456')
    this.baseeq4 = { x: pt2.x - deltax, y: pt2.y - deltay }
  } // colleEquerre

  colleRegle (deltax, deltay, newx, newy) {
    let l3 = { x: this.basereg.x + deltax, y: this.basereg.y + deltay }
    const aver2 = this.mtgAppLecteur.valueOf(this.svgId, 'reglangleC') - 13 + 90
    const ang = aver2 * Math.PI / 180
    let ang2 = ang - Math.PI / 2
    const l1 = { x: l3.x - this.unite * 0.06 * Math.cos(ang), y: l3.y - this.unite * 0.06 * Math.sin(ang) }
    const l2 = { x: l1.x + this.unite * 20 * Math.cos(ang2), y: l1.y + this.unite * 20 * Math.sin(ang2) }
    let a = (l2.y - l1.y) / (l2.x - l1.x)
    let b = l1.y - a * l1.x
    let a2 = -1 / a
    if (Math.abs(a) < 0.00001) a2 = 9999999999
    if (a > 99999999 || a < -99999999) {
      a2 = 0
    }
    let mm = false
    let zan
    // pour chaque point test distance a seg regle
    if (this.equerreVisible) {
      // cherche si droite passe pres du coin
      const pt = this.getPointPosition2(this.svgId, this.idPtEquerre.pt)
      const pt2 = this.getPointPosition2(this.svgId, '#Eq234')
      const aver = Math.atan2(pt2.x - pt.x, pt.y - pt2.y) * 180 / Math.PI
      if (this.distPtDtebis(l1, pt, l2) < 0.5) {
        if (this.distangle(aver2, aver + 90) < 5) {
          const a = (pt2.y - pt.y) / (pt2.x - pt.x)
          const a2 = (a === 0) ? 9999999999 : -1 / a
          const b = pt.y - a2 * pt.x
          const b2 = l3.y - a * l3.x
          const lf = j3pClone(l3)
          l3 = { x: (b - b2) / (a - a2) }
          l3.y = a * l3.x + b2
          if (a > 999999999 || a < -99999999) {
            l3.x = lf.x
            l3.y = pt.y
            const aver3 = aver + 13
            this.mtgAppLecteur.setPointPosition(this.svgId, '#pttttt', l3.x + 219 * this.zoom * Math.cos(aver3 * Math.PI / 180), l3.y + 219 * this.zoom * Math.sin(aver3 * Math.PI / 180), false)
            this.mtgAppLecteur.setPointPosition(this.svgId, this.idPtRegle.pt, l3.x, l3.y, false)
            this.mtgAppLecteur.updateFigure(this.svgId)
            return
          }
          if (a < 0.0001 && a > -0.0001) {
            l3.x = pt.x
            l3.y = lf.y
            const aver3 = aver + 13
            this.mtgAppLecteur.setPointPosition(this.svgId, '#pttttt', l3.x + 219 * this.zoom * Math.cos(aver3 * Math.PI / 180), l3.y + 219 * this.zoom * Math.sin(aver3 * Math.PI / 180), false)
            this.mtgAppLecteur.setPointPosition(this.svgId, this.idPtRegle.pt, l3.x, l3.y, false)
            this.mtgAppLecteur.updateFigure(this.svgId)
            return
          }
          const aver3 = aver + 13
          this.mtgAppLecteur.setPointPosition(this.svgId, '#pttttt', l3.x + 219 * this.zoom * Math.cos(aver3 * Math.PI / 180), l3.y + 219 * this.zoom * Math.sin(aver3 * Math.PI / 180), false)
          this.mtgAppLecteur.setPointPosition(this.svgId, this.idPtRegle.pt, l3.x, l3.y, false)
          this.mtgAppLecteur.updateFigure(this.svgId)
          return
        }

        if (this.distangle(aver2, aver - 180) < 5) {
          const a = (pt2.y - pt.y) / (pt2.x - pt.x)
          const a2 = (a === 0) ? 9999999999 : -1 / a
          const b = pt.y - a * pt.x
          const b2 = l3.y - a2 * l3.x
          const lf = j3pClone(l3)

          l3 = { x: (b2 - b) / (a - a2) }
          l3.y = a2 * l3.x + b2
          if (a > 999999999 || a < -99999999) {
            l3.y = lf.y
            l3.x = pt.x
          }
          if (a < 0.00001 && a > -0.00001) {
            l3.x = lf.x
            l3.y = pt.y
          }
          const aver3 = aver + 13 + 90
          this.mtgAppLecteur.setPointPosition(this.svgId, '#pttttt', l3.x + 219 * this.zoom * Math.cos(aver3 * Math.PI / 180), l3.y + 219 * this.zoom * Math.sin(aver3 * Math.PI / 180), false)
          this.mtgAppLecteur.setPointPosition(this.svgId, this.idPtRegle.pt, l3.x, l3.y, false)
          this.mtgAppLecteur.updateFigure(this.svgId)
          return
        }
      }
    }
    const lp = []
    for (let i = 0; i < this.listePointPris.length; i++) {
      if (this.listePointPris[i].viz) {
        const pt = this.getPointPosition2(this.svgId, this.listePointPris[i].point)
        if (i < this.ConsPtnb) {
          this.setColor2(this.svgId, this.listePointPris[i].point, 0, 150, 0, true)
        } else {
          this.setColor2(this.svgId, this.listePointPris[i].point, 100, 100, 100, true)
        }

        const b2 = pt.y - a2 * pt.x
        const co = { x: (b2 - b) / (a - a2) }
        co.y = a * co.x + b
        if (Math.abs(a) < 0.00001) {
          co.x = pt.x
          co.y = l2.y
        }
        if (a > 999999999 || a < -99999999) {
          co.x = l2.x
          co.y = pt.y
        }
        const popos = pt.x > Math.min(l1.x, l2.x) - this.unite / 5 && pt.x < Math.max(l1.x, l2.x) + this.unite / 5
        if (popos) {
          if (this.dist(co, pt) < this.unite / 2) {
            lp.push({
              co: j3pClone(co),
              dist: this.dist(co, pt),
              pt: j3pClone(pt),
              num: i
            })
          }
        }
      }
    }
    lp.sort((a, b) => a.dist - b.dist)
    if (lp.length > 1) {
      let ang3 = this.regule(Math.atan2(lp[1].pt.x - lp[0].pt.x, lp[0].pt.y - lp[1].pt.y) * 180 / Math.PI - 90 + 13)
      let ang4 = this.regule(this.mtgAppLecteur.valueOf(this.svgId, 'reglangleC'))
      if (this.distangle(ang3, ang4) < 8 || this.distangle(ang3, ang4 + 180) < 8) {
        if (Math.abs(this.regule(ang3) - this.regule(ang4)) > 50) {
          ang3 += 180
          const sisi = j3pClone(lp[0])
          lp[0] = j3pClone(lp[1])
          lp[1] = j3pClone(sisi)
        }
        this.setColor2(this.svgId, this.listePointPris[lp[1].num].point, 250, 0, 0, true)
        this.setColor2(this.svgId, this.listePointPris[lp[0].num].point, 250, 0, 0, true)
        a = (lp[1].pt.y - lp[0].pt.y) / (lp[1].pt.x - lp[0].pt.x)
        b = lp[0].pt.y - a * lp[0].pt.x
        const a2 = (a === 0) ? 9999999999 : -1 / a
        const b2 = this.basereg.y + deltay - a2 * (this.basereg.x + deltax)
        let co = { x: (b2 - b) / (a - a2) }
        co.y = a * co.x + b
        if (a > 999999999 || a < -99999999) {
          co.x = lp[0].pt.x
          co.y = this.basereg.y + deltay
        }
        if (Math.abs(a) < 0.00001) {
          co.y = lp[0].pt.y
          co.x = this.basereg.x + deltax
        }
        ang4 = ang3 + 90
        co = {
          x: co.x + this.unite * 0.06 * Math.cos(ang4 * Math.PI / 180),
          y: co.y + this.unite * 0.06 * Math.sin(ang4 * Math.PI / 180)
        }
        deltax = co.x - this.basereg.x
        deltay = co.y - this.basereg.y
        const zan = ang3
        this.mtgAppLecteur.setPointPosition(this.svgId, '#pttttt', this.basereg.x + deltax + 219 * this.zoom * Math.cos(zan * Math.PI / 180), this.basereg.y + deltay + 219 * this.zoom * Math.sin(zan * Math.PI / 180), false)
        this.basereg2 = {
          x: this.basereg.x + 219 * this.zoom * Math.cos(zan * Math.PI / 180),
          y: this.basereg.y + 219 * this.zoom * Math.sin(zan * Math.PI / 180)
        }
        mm = true
        this.fochgreg = true
      }
    }
    if (!mm) {
      ang2 = this.regule(ang * 180 / Math.PI)
      let gtrouv = false
      for (let i = 0; i < this.listeDtePris.length; i++) {
        // test prox coin
        mm = false

        // test aver
        if (this.listeDtePris[i].viz === false) continue
        const pt2 = this.getPointPosition2(this.svgId, this.listeDtePris[i].point1)
        const pt3 = this.getPointPosition2(this.svgId, this.listeDtePris[i].point2)

        if (this.distPtDtebis(pt2, l1, pt3) < this.distcol / this.unite && this.distPtDtebis(pt2, l2, pt3) < this.distcol / this.unite) {
          let aver3 = this.regule(Math.atan2(pt2.x - pt3.x, pt3.y - pt2.y) * 180 / Math.PI)
          zan = aver3 - 90 + 13

          if (Math.abs(this.distangle(aver3, ang2)) > 100) {
            zan += 180
            aver3 += 180
          }
          gtrouv = true
          const co = this.faitcoxd(pt2, pt3, aver3, l3)

          deltax = co.x - this.basereg.x
          deltay = co.y - this.basereg.y

          this.mtgAppLecteur.setPointPosition(this.svgId, '#pttttt', this.basereg.x + deltax + 219 * this.zoom * Math.cos(zan * Math.PI / 180), this.basereg.y + deltay + 219 * this.zoom * Math.sin(zan * Math.PI / 180), false)
          this.basereg2 = {
            x: this.basereg.x + 219 * this.zoom * Math.cos(zan * Math.PI / 180),
            y: this.basereg.y + 219 * this.zoom * Math.sin(zan * Math.PI / 180)
          }
          mm = true
          this.fochgreg = true
          break
        }
      }

      for (let i = 0; i < this.listeSegPris.length; i++) {
        // test prox coin
        if (gtrouv) break
        mm = false

        // test aver
        if (this.listeSegPris[i].viz === false) continue
        const pt2 = this.getPointPosition2(this.svgId, this.listeSegPris[i].point1)
        const pt3 = this.getPointPosition2(this.svgId, this.listeSegPris[i].point2)

        if (this.distPtDtebis(pt2, l1, pt3) < this.distcol / this.unite && this.distPtDtebis(pt2, l2, pt3) < this.distcol / this.unite) {
          let aver3 = this.regule(Math.atan2(pt2.x - pt3.x, pt3.y - pt2.y) * 180 / Math.PI)
          zan = aver3 - 90 + 13

          if (Math.abs(this.distangle(aver3, ang2)) > 100) {
            zan += 180
            aver3 += 180
          }

          const co = this.faitcoxd(pt2, pt3, aver3, l3)
          const cobis = {
            x: co.x + 20 * this.unite * this.zoom * Math.cos((aver3 - 90) * Math.PI / 180),
            y: co.y - 20 * this.unite * this.zoom * Math.sin((aver3 - 90) * Math.PI / 180)
          }

          // faut vérifier qu’une extrmeité au moins est entre l1 et l2
          if (this.distPtSeg2(co, cobis, pt2) > 5 && this.distPtSeg2(co, cobis, pt3) > 5 && this.distPtSeg2(pt2, pt3, co) > 5 && this.distPtSeg2(pt2, pt3, cobis) > 5) continue

          gtrouv = true
          deltax = co.x - this.basereg.x
          deltay = co.y - this.basereg.y

          this.mtgAppLecteur.setPointPosition(this.svgId, '#pttttt', this.basereg.x + deltax + 219 * this.zoom * Math.cos(zan * Math.PI / 180), this.basereg.y + deltay + 219 * this.zoom * Math.sin(zan * Math.PI / 180), false)
          this.basereg2 = {
            x: this.basereg.x + 219 * this.zoom * Math.cos(zan * Math.PI / 180),
            y: this.basereg.y + 219 * this.zoom * Math.sin(zan * Math.PI / 180)
          }
          mm = true
          this.fochgreg = true

          break
        }
      }

      for (let i = 0; i < this.listeDemiDPris.length; i++) {
        // test prox coin
        if (gtrouv) break
        mm = false

        // test aver
        if (this.listeDemiDPris[i].viz === false) continue
        const pt2 = this.getPointPosition2(this.svgId, this.listeDemiDPris[i].point1)
        const pt3 = this.getPointPosition2(this.svgId, this.listeDemiDPris[i].point2)

        if (this.distPtDtebis(pt2, l1, pt3) + this.distPtDtebis(pt2, l2, pt3) < this.distcol / this.unite * 2) {
          let aver3 = this.regule(Math.atan2(pt2.x - pt3.x, pt3.y - pt2.y) * 180 / Math.PI)
          let zan = aver3 - 90 + 13

          if (Math.abs(this.distangle(aver3, ang2)) > 100) {
            zan += 180
            aver3 += 180
          }

          const co = this.faitcoxd(pt2, pt3, aver3, l3)
          const cobis = {
            x: co.x + 20 * this.unite * this.zoom * Math.cos((aver3 - 90) * Math.PI / 180),
            y: co.y - 20 * this.unite * this.zoom * Math.sin((aver3 - 90) * Math.PI / 180)
          }

          // faut vérifier qu’une extrmeité au moins est entre l1 et l2
          if (this.distPtDemiDtebis(pt2, co, pt3) > 5 && this.distPtDemiDtebis(pt2, cobis, pt3) > 5) continue

          gtrouv = true
          deltax = co.x - this.basereg.x
          deltay = co.y - this.basereg.y

          this.mtgAppLecteur.setPointPosition(this.svgId, '#pttttt', this.basereg.x + deltax + 219 * this.zoom * Math.cos(zan * Math.PI / 180), this.basereg.y + deltay + 219 * this.zoom * Math.sin(zan * Math.PI / 180), false)
          this.basereg2 = {
            x: this.basereg.x + 219 * this.zoom * Math.cos(zan * Math.PI / 180),
            y: this.basereg.y + 219 * this.zoom * Math.sin(zan * Math.PI / 180)
          }
          mm = true
          this.fochgreg = true

          break
        }
      }

      if (!gtrouv && lp.length !== 0) {
        deltax += lp[0].pt.x - lp[0].co.x
        deltay += lp[0].pt.y - lp[0].co.y
        this.setColor2(this.svgId, this.listePointPris[lp[0].num].point, 250, 0, 0, true)
      }
    }

    this.mtgAppLecteur.setPointPosition(this.svgId, this.idPtRegle.pt, this.basereg.x + deltax, this.basereg.y + deltay, false)
    if (!mm) {
      if (!this.fochgreg) {
        this.mtgAppLecteur.setPointPosition(this.svgId, '#pttttt', this.basereg2.x + deltax, this.basereg2.y + deltay, false)
      } else {
        zan = this.regule(this.mtgAppLecteur.valueOf(this.svgId, 'reglangleC')) + 2
        this.mtgAppLecteur.setPointPosition(this.svgId, '#pttttt', this.basereg.x + +deltax + 219 * this.zoom * Math.cos(zan * Math.PI / 180), this.basereg.y + +deltay + 219 * this.zoom * Math.sin(zan * Math.PI / 180), false)
        this.basereg2 = {
          x: this.basereg.x + deltax + 219 * this.zoom * Math.cos(zan * Math.PI / 180),
          y: this.basereg.y + deltay + 219 * this.zoom * Math.sin(zan * Math.PI / 180)
        }
        this.basereg = { x: this.basereg.x + deltax, y: this.basereg.y + deltay }
        this.x = newx
        this.y = newy
        this.fochgreg = false
      }
    }
    this.mtgAppLecteur.updateFigure(this.svgId)
  }

  faitFuncGomm (k, t) {
    if (k === 'name') {
      this.mtgAppLecteur.addEltListener({ elt: t.aff, eventName: 'mousedown', callBack: this.makecliGoNList(t) })
      this.mtgAppLecteur.addEltListener({ elt: t.aff, eventName: 'touchstart', callBack: this.makecliGoNList(t) })
      this.mtgAppLecteur.addEltListener({ elt: t.aff, eventName: 'mouseover', callBack: this.makeOvGoNList(t) })
      this.mtgAppLecteur.addEltListener({ elt: t.aff, eventName: 'mouseout', callBack: this.makeOuGoNList(t) })
      return
    }
    if (t === 'pt') {
      this.setVisible2(this.svgId, k.cercle, true, true)
      this.setColor2(this.svgId, k.cercle, 0, 0, 0, true, 0)
      this.mtgAppLecteur.addEltListener({ elt: k.cercle, eventName: 'mouseover', callBack: this.makeOvGoList(k, t, k.cercle) })
      this.mtgAppLecteur.addEltListener({ elt: k.cercle, eventName: 'mouseout', callBack: this.makeOuGoList(k, t, k.cercle, k.cool) })
      this.mtgAppLecteur.addEltListener({ elt: k.cercle, eventName: 'mousedown', callBack: this.makecliGoList(k, t, k.cercle) })
      this.mtgAppLecteur.addEltListener({ elt: k.cercle, eventName: 'touchstart', callBack: this.makecliGoList(k, t, k.cercle) })
      return
    }
    if (t === 'arc' || t === 'cercle') {
      for (let i = 13; i < 25; i++) {
        try {
          this.mtgAppLecteur.addEltListener({ elt: k['arc' + (i - 12)], eventName: 'mouseover', callBack: this.makeOvGoAList(k, t) })
          this.mtgAppLecteur.addEltListener({ elt: k['arc' + (i - 12)], eventName: 'mouseout', callBack: this.makeOuGoAList(k, t, k.cool) })
          this.mtgAppLecteur.addEltListener({ elt: k['arc' + (i - 12)], eventName: 'click', callBack: this.makecliGoAList(k, t) })
        } catch (e) {
        }
      }
    } else {
      this.mtgAppLecteur.addEltListener({ elt: k.ligne, eventName: 'mouseover', callBack: this.makeOvGoList(k, t) })
      this.mtgAppLecteur.addEltListener({ elt: k.ligne, eventName: 'mouseout', callBack: this.makeOuGoList(k, t) })
      this.mtgAppLecteur.addEltListener({ elt: k.ligne, eventName: 'mousedown', callBack: this.makecliGoList(k, t, k.ligne) })
      this.mtgAppLecteur.addEventListener({ elt: k.ligne, eventName: 'touchstart', callBack: this.makecliGoList(k, t, k.ligne) })
    }
  }

  makeOvGo (g, t) {
    const ff = function () {
      if (this.me.etat !== 'correction') return
      if (this.lagomm) {
        this.mtgAppLecteur.getDoc(this.svgId).defaultCursor = 'pointer'
        if (t !== 'pt') {
          this.setColor2(this.svgId, g.ligne, 255, 0, 0, true)
          this.setLineStyle2(this.svgId, g.ligne, 0, 5, true)
        } else {
          this.setColor2(this.svgId, g.point, 255, 0, 0, true)
        }
        if (g.aff) {
          this.mtgAppLecteur.setText(this.svgId, '#' + g.aff, g.nom, true)
          this.setColor2(this.svgId, g.aff, 160, 0, 0, true)
        }
      } else if (this.poitilEncours) {
        if (t === 'pt') return
        this.mtgAppLecteur.getDoc(this.svgId).defaultCursor = 'pointer'
        this.setColor2(this.svgId, g.ligne, 0, 255, 0, true)
        this.setLineStyle2(this.svgId, g.ligne, 0, 5, true)
        if (g.aff) {
          this.mtgAppLecteur.setText(this.svgId, '#' + g.aff, g.nom)
          this.setColor2(this.svgId, g.aff, 0, 255, 0, true)
        }
      } else if (this.yacoul) {
        this.mtgAppLecteur.getDoc(this.svgId).defaultCursor = 'pointer'
        if (t !== 'pt') {
          this.setLineStyle2(this.svgId, g.ligne, 0, 5, true)
          this.setColor2(this.svgId, g.ligne, this.laCoool[0], this.laCoool[1], this.laCoool[2], true)
        } else {
          this.setColor2(this.svgId, g.point, this.laCoool[0], this.laCoool[1], this.laCoool[2], true)
        }
        if (g.aff) {
          this.mtgAppLecteur.setText(this.svgId, '#' + g.aff, g.nom, true)
          this.setColor2(this.svgId, g.aff, this.laCoool[0], this.laCoool[1], this.laCoool[2], true)
        }
      } else {
        if (this.unecapt()) return
        if (this.lepoint || this.lepoint2 || this.lepoint4 || this.lepoint22) return
        if (t !== 'dte') return
        this.mtgAppLecteur.getDoc(this.svgId).defaultCursor = 'pointer'
        this.setColor2(this.svgId, g.ligne, 0, 0, 255, true)
        this.setLineStyle2(this.svgId, g.ligne, 0, 5, true)
        if (g.aff) {
          this.mtgAppLecteur.setText(this.svgId, '#' + g.aff, g.nom)
          this.setColor2(this.svgId, g.aff, 0, 0, 255, true)
        }
      }
    }
    return ff.bind(this)
  }

  makeOuGo (g, t) {
    const ff = function () {
      if (this.me.etat !== 'correction') return
      const cool = j3pClone(g.color)
      this.mtgAppLecteur.getDoc(this.svgId).defaultCursor = null
      const ki = (t === 'pt') ? g.point : g.ligne
      this.setColor2(this.svgId, ki, cool[0], cool[1], cool[2], true)
      if (t !== 'pt') {
        this.setLineStyle2(this.svgId, ki, (g.pointille) ? 2 : 0, 2, true)
      }
      if (g.aff && g.nom) {
        this.mtgAppLecteur.setText(this.svgId, '#' + g.aff, g.nom, true)
        this.setColor2(this.svgId, g.aff, cool[0], cool[1], cool[2], true)
      }
    }
    return ff.bind(this)
  }

  retrouveI (tab, nb, g) {
    for (let i = 0; i < tab.length; i++) {
      if (tab[i][nb] === g) return tab[i]
    }
  }

  makecliGo (g, t) {
    let tbu, kel
    const asup = []
    if (t === 'seg') {
      tbu = this.listeSegPris
      asup.push(g.point1, g.point2)
      kel = 'point1'
    }
    if (t === 'dte') {
      tbu = this.listeDtePris
      asup.push(g.point1, g.point2)
      kel = 'point1'
    }
    if (t === 'demid') {
      tbu = this.listeDemiDPris
      asup.push(g.point1, g.point2)
      kel = 'point1'
    }
    if (t === 'pt') {
      tbu = this.listePointPris
      asup.push(g.point)
      kel = 'point'
    }
    const ff = function () {
      if (this.me.etat !== 'correction') return
      if (this.lagomm) {
        if (g.pointNom) {
          asup.push(g.pointNom)
        }
        for (let i = 0; i < asup.length; i++) {
          this.mtgAppLecteur.deleteElt({ elt: asup[i] })
        }
        let cbongTrouv = false
        for (let i = 0; i < tbu.length; i++) {
          if (tbu[i][kel] === g[kel]) {
            tbu.splice(i, 1)
            cbongTrouv = true
            break
          }
        }
        if (!cbongTrouv) console.error('ai effacé un truc inconnu')
        if (g.nom) {
          for (let i = 0; i < this.nomspris.length; i++) {
            if (this.nomspris[i] === g.nom) {
              this.nomspris.splice(i, 1)
              break
            }
          }
        }
      } else if (this.poitilEncours) {
        if (t === 'pt') return
        this.setColor2(this.svgId, g.ligne, 100, 100, 100, true)
        g.pointille = !g.pointille
        this.setLineStyle2(this.svgId, g.ligne, (g.pointille) ? 2 : 0, 2, true)
      } else if (this.yacoul) {
        if (t !== 'pt') {
          this.setColor2(this.svgId, g.ligne, this.laCoool[0], this.laCoool[1], this.laCoool[2], true)
          this.setLineStyle2(this.svgId, g.ligne, 0, 2, true)
        } else {
          this.setColor2(this.svgId, g.point, this.laCoool[0], this.laCoool[1], this.laCoool[2], true)
        }
        g.color = j3pClone(this.laCoool)
        if (g.aff) {
          this.mtgAppLecteur.setText(this.svgId, '#' + g.aff, g.nom, true)
          this.setColor2(this.svgId, g.aff, this.laCoool[0], this.laCoool[1], this.laCoool[2], true)
        }
      } else {
        if (this.unecapt() || this.lepoint || this.lepoint2 || this.lepoint22 || this.lepoint4) return
        if (t === 'dte') { this.nomme('droite', g) }
      }
    }
    return ff.bind(this)
  }

  faisCoul () {
    this.yacoul = !this.yacoul
    if (this.txtF !== 'FigLibre') return
    this.setVisible2(this.svgId, '#pptout', this.yacoul)
    this.setVisible2(this.svgId, '#pstout', this.yacoul)
    this.setVisible2(this.svgId, '#pprouge', this.yacoul)
    this.setVisible2(this.svgId, '#psrouge', this.yacoul)
    this.setVisible2(this.svgId, '#ppbleu', this.yacoul)
    this.setVisible2(this.svgId, '#psbleu', this.yacoul)
    this.setVisible2(this.svgId, '#ppnoir', this.yacoul)
    this.setVisible2(this.svgId, '#psnoir', this.yacoul)
    this.setVisible2(this.svgId, '#ppgris', this.yacoul)
    this.setVisible2(this.svgId, '#psgris', this.yacoul)
    this.setVisible2(this.svgId, '#ppvert', this.yacoul)
    this.setVisible2(this.svgId, '#psvert', this.yacoul)
    if (this.yacoul) {
      const Ccoil = {
        rouge: [255, 0, 0],
        bleu: [0, 0, 255],
        noir: [0, 0, 0],
        gris: [150, 150, 150],
        vert: [0, 200, 0]
      }
      const lescoul = ['rouge', 'bleu', 'vert', 'gris', 'noir']
      lescoul.forEach((el) => {
        this.mtgAppLecteur.addEventListener(this.svgId, '#pp' + el, 'mouseover', () => {
          this.mtgAppLecteur.getDoc(this.svgId).defaultCursor = 'pointer'
        })
        this.mtgAppLecteur.addEventListener(this.svgId, '#pp' + el, 'mouseout', () => {
          this.mtgAppLecteur.getDoc(this.svgId).defaultCursor = ''
        })
        this.mtgAppLecteur.addEventListener(this.svgId, '#pp' + el, 'click', () => {
          this.laCoool = Ccoil[el]
          this.setColor2(this.svgId, '#sSsColor', Ccoil[el][0], Ccoil[el][1], Ccoil[el][2])
        })
        this.mtgAppLecteur.addEventListener(this.svgId, '#pp' + el, 'touchstart', () => {
          this.laCoool = Ccoil[el][el]
          this.setColor2(this.svgId, '#sSsColor', Ccoil[el][0], Ccoil[el][1], Ccoil[el][2])
        })
      })
    }
  }

  makeOvGoA (g, t) {
    const ff = function () {
      if (this.me.etat !== 'correction') return
      if (this.lagomm) {
        this.mtgAppLecteur.getDoc(this.svgId).defaultCursor = 'pointer'
        for (let i = 13; i < 25; i++) {
          try {
            this.setColor2(this.svgId, g['arc' + (i - 12)], 255, 0, 0, true)
            this.setLineStyle2(this.svgId, g['arc' + (i - 12)], 0, 5, true)
          } catch (e) {
            // eslint-disable-next-line no-console
            console.log(e)
          }
        }
        if (g.pointNom) {
          this.mtgAppLecteur.setText(this.svgId, '#' + g.aff, g.nom, true)
          this.setColor2(this.svgId, g.aff, 200, 0, 0, true)
        }
      } else if (this.yacoul) {
        this.mtgAppLecteur.getDoc(this.svgId).defaultCursor = 'pointer'
        for (let i = 13; i < 25; i++) {
          try {
            this.setColor2(this.svgId, g['arc' + (i - 12)], this.laCoool[0], this.laCoool[1], this.laCoool[2], true)
            this.setLineStyle2(this.svgId, g['arc' + (i - 12)], 0, 5, true)
          } catch (e) {
            // eslint-disable-next-line no-console
            console.log(e)
          }
        }
        if (g.pointNom) {
          this.mtgAppLecteur.setText(this.svgId, '#' + g.aff, g.nom, true)
          this.setColor2(this.svgId, g.aff, this.laCoool[0], this.laCoool[1], this.laCoool[2], true)
        }
      } else {
        if (this.unecapt()) return
        if (this.lepoint || this.lepoint2 || this.lepoint22 || this.lepoint4) return
        if (t !== 'cercle') return
        this.mtgAppLecteur.getDoc(this.svgId).defaultCursor = 'pointer'
        for (let i = 13; i < 25; i++) {
          try {
            this.setColor2(this.svgId, g['arc' + (i - 12)], 0, 0, 255, true)
            this.setLineStyle2(this.svgId, g['arc' + (i - 12)], 0, 5, true)
          } catch (e) {
            // eslint-disable-next-line no-console
            console.log(e)
          }
        }
        if (g.pointNom) {
          this.mtgAppLecteur.setText(this.svgId, '#' + g.aff, g.nom, true)
          this.setColor2(this.svgId, g.aff, 0, 0, 255, true)
        }
      }
    }
    return ff.bind(this)
  }

  makeOuGoA (g) {
    const ff = function () {
      if (this.me.etat !== 'correction') return
      const cool = g.color
      for (let i = 13; i < 25; i++) {
        this.mtgAppLecteur.getDoc(this.svgId).defaultCursor = null
        try {
          this.setColor2(this.svgId, g['arc' + (i - 12)], cool[0], cool[1], cool[2], true)
          this.setLineStyle2(this.svgId, g['arc' + (i - 12)], 0, 2, true)
        } catch (e) {
          // eslint-disable-next-line no-console
          console.log(e)
        }
      }
      if (g.pointNom) {
        this.mtgAppLecteur.setText(this.svgId, '#' + g.aff, g.nom, true)
        this.setColor2(this.svgId, g.aff, 0, 0, 0, true)
      }
    }
    return ff.bind(this)
  }

  makecliGoA (g, t) {
    const ff = function () {
      if (this.me.etat !== 'correction') return
      if (this.lagomm) {
        for (let j = 1; j < 13; j++) {
          this.mtgAppLecteur.deleteElt({ elt: g['p' + j] })
        }
        this.mtgAppLecteur.deleteElt({ elt: g.centre })
        if (g.pointNom) {
          this.mtgAppLecteur.deleteElt({ elt: g.pointNom })
          for (let i = 0; i < this.nomspris.length; i++) {
            if (this.nomspris[i] === g.nom) {
              this.nomspris.splice(i, 1)
              break
            }
          }
        }
        for (let i = 0; i < this.listeArcPris.length; i++) {
          if (this.listeArcPris[i].centre === g.centre) {
            this.listeArcPris.splice(i, 1)
            break
          }
        }
      } else if (this.yacoul) {
        for (let i = 13; i < 25; i++) {
          this.setColor2(this.svgId, g['arc' + (i - 12)], this.laCoool[0], this.laCoool[1], this.laCoool[2], true)
          this.setLineStyle2(this.svgId, g['arc' + (i - 12)], 0, 2, true)
        }
        g.color = j3pClone(this.laCoool)
      } else {
        if (this.unecapt() || this.lepoint || this.lepoint2 || this.lepoint22 || this.lepoint4) return
        if (t === 'cercle') { this.nomme('cercle', g) }
      }
    }
    return ff.bind(this)
  }

  makeOvGoN (g) {
    const ff = function () {
      if (this.me.etat !== 'correction') return
      if (!this.lagomm) return
      this.mtgAppLecteur.getDoc(this.svgId).defaultCursor = 'pointer'
      this.mtgAppLecteur.setText(this.svgId, g.aff, g.nom, true)
      this.setColor2(this.svgId, g.aff, 255, 0, 0, true)
    }
    return ff.bind(this)
  }

  makeOuGoN (g, cool) {
    cool = cool || [100, 100, 100]
    const ff = function () {
      if (this.me.etat !== 'correction') return
      if (!this.lagomm) return
      this.mtgAppLecteur.getDoc(this.svgId).defaultCursor = null
      this.mtgAppLecteur.setText(this.svgId, g.aff, g.nom, true)
      this.setColor2(this.svgId, g.aff, cool[0], cool[1], cool[2], true)
    }
    return ff.bind(this)
  }

  makecliGoN (g) {
    const ff = function () {
      if (this.me.etat !== 'correction') return
      if (!this.lagomm) return
      this.mtgAppLecteur.deleteElt({ elt: g.pointName })
      g.pointName = undefined
      g.aff = undefined
      g.nom = undefined
      for (let i = 0; i < this.nomspris.length; i++) {
        if (this.nomspris[i] === g.nom) {
          this.nomspris.splice(i, 1)
          return
        }
      }
    }
    return ff.bind(this)
  }

  unecapt () {
    return this.captureFond || this.capcray || this.capRegle || this.capEquer || this.capRap || this.capComp || this.PtRegle || this.PtCompS || this.BBloque || this.lepoint3 || this.capRegle2 || this.capEquerre2 || this.capComp2 || this.capRap2 || this.PtCompS2
  }

  unecaptOK () {
    return this.captureFond || this.capcray || this.capRegle || this.capEquer || this.capRap || this.capComp || this.PtRegle || this.PtCompS || this.lepoint3 || this.capRegle2 || this.capEquerre2 || this.capComp2 || this.capRap2 || this.PtCompS2
  }

  metToutFalse () {
    this.captureFond = this.capcray = this.capRegle = this.capRegle2 = this.capEquerre2 = this.capEquer = this.capComp = this.capRap = this.PtRegle = this.PtRegle2 = this.PtCompS = this.PtCompS2 = this.BBloque = this.capComp2 = this.capRap2 = false
  }

  setVisible2 (a, b, c) {
    try {
      if (c) {
        this.mtgAppLecteur.setVisible({ elt: b.replace('#', '') })
      } else {
        this.mtgAppLecteur.setHidden({ elt: b.replace('#', '') })
      }
    } catch (e) {
      // objet pas visible

      // console.log(e)
    }
  }

  regle (b) {
    this.consoleDev('lance regle')
    if (this.regleVisible && this.lagrad) {
      for (let i = 0; i < this.idRegfleGrad.length; i++) {
        this.setVisible2(this.svgId, this.idRegfleGrad[i], false, false)
      }
      this.lagrad = false
      this.bloqueEventm = false
      this.whereAf('regle')
      return
    }
    this.regleVisible = !this.regleVisible
    for (let i = 0; i < this.idRegle.length; i++) {
      this.setVisible2(this.svgId, this.idRegle[i], this.regleVisible, false)
    }
    for (let i = 0; i < this.idReglePoly.length; i++) {
      this.setVisible2(this.svgId, this.idReglePoly[i], this.regleVisible, true)
    }
    for (let i = 0; i < this.idRegfleGrad.length; i++) {
      this.setVisible2(this.svgId, this.idRegfleGrad[i], false, false)
    }
    // this.setVisible2(this.svgId, this.idPtRegle.pt2, this.regleVisible, false)
    this.setVisible2(this.svgId, this.idPtRegle.ma, this.regleVisible, false)
    if (this.regleVisible && this.me.etat === 'correction') {
      this.whereAf('regle')
      this.viregommEtrot()
      this.mtgAppLecteur.setPointPosition(this.svgId, this.idPtRegle.pt, this.reglebasx, this.reglebasy, false)
      this.mtgAppLecteur.setPointPosition(this.svgId, '#pttttt', this.reglebasx + (219 * this.zoom), this.reglebasy + (47 * this.zoom), false)
      if (b !== false) this.mtgAppLecteur.updateFigure(this.svgId)
      for (let i = 0; i < this.idReglePoly.length; i++) {
        const ff11 = function () {
          if (this.unecapt()) return
          if (this.lepoint || this.lepoint4) return
          if (this.mtgAppLecteur.getDoc(this.svgId).pointCapture) return
          this.mtgAppLecteur.getDoc(this.svgId).defaultCursor = 'pointer'
        }
        const ff22 = function () {
          if (this.unecapt()) return
          if (this.lepoint || this.lepoint4) return
          this.mtgAppLecteur.getDoc(this.svgId).defaultCursor = null
        }
        const ff33 = function (ev) {
          if (this.lepoint || this.lepoint4) return
          if (this.unecapt()) return
          const tmoio = this.getPointPosition2(this.svgId, '#pttttt')
          this.x = ev.changedTouches[0].clientX
          this.y = ev.changedTouches[0].clientY
          const GG = this.zonefig.getBoundingClientRect()
          /*
          const Gco = j3pGetPos(this.mepact)
          GG.x = GG.x - Gco.x / 2
          GG.y = GG.y - Gco.y / 2
          */
          const gfdjkslg = { x: this.x - GG.x, y: this.y - GG.y }
          if (this.dist(gfdjkslg, tmoio) < 15) return
          this.capRegle2 = true

          this.basereg = this.getPointPosition2(this.svgId, this.idPtRegle.pt)
          this.basereg2 = this.getPointPosition2(this.svgId, '#pttttt')

          this.mtgAppLecteur.setPointPosition(this.svgId, '#pointlibre', this.x - GG.x, this.y - GG.y, true)
          this.x = this.x - GG.x
          this.y = this.y - GG.y
        }
        const ff44 = function (ev) {
          if (this.mtgAppLecteur.getDoc(this.svgId).pointCapture) return
          if (this.lepoint || this.lepoint4) return
          if (this.unecapt()) return
          this.capRegle = true
          this.x = ev.clientX
          this.y = ev.clientY
          this.basereg = this.getPointPosition2(this.svgId, this.idPtRegle.pt)
          this.basereg2 = this.getPointPosition2(this.svgId, '#pttttt')
        }
        this.mtgAppLecteur.addEventListener(this.svgId, this.idReglePoly[i], 'mouseover', ff11.bind(this))
        this.mtgAppLecteur.addEventListener(this.svgId, this.idReglePoly[i], 'mouseout', ff22.bind(this))
        this.mtgAppLecteur.addEventListener(this.svgId, this.idReglePoly[i], 'touchstart', ff33.bind(this))
        this.mtgAppLecteur.addEventListener(this.svgId, this.idReglePoly[i], 'mousedown', ff44.bind(this))
      }
    } else {
      this.whereAfficheAide.style.display = 'none'
      this.mtgAppLecteur.setPointPosition(this.svgId, this.idPtRegle.pt, 5000, 5000, false)
      this.mtgAppLecteur.setPointPosition(this.svgId, '#pttttt', 5000 + (219 * this.zoom), 5000 + (47 * this.zoom), false)
      this.eteintPoint(false)
      if (b !== false) this.mtgAppLecteur.updateFigure(this.svgId)
    }
    this.bloqueEventm = false
  }

  whereAf (koi) {
    j3pEmpty(this.whereAfficheAide)
    this.whereAfficheAide.style.display = ''
    const tyiyui = addDefaultTable(this.whereAfficheAide, 10, 1)
    let ptihd, froirf, kflmkmds, fjdslkdi
    switch (koi) {
      case 'pt':
        ptihd = addDefaultTable(tyiyui[0][0], 1, 3)
        j3pAffiche(ptihd[0][0], null, '1. Clique sur ')
        this.metsImage(ptihd[0][1], this.imagePoint)
        j3pAffiche(ptihd[0][2], null, ' quand tu veux nommer un point. ')
        froirf = addDefaultTable(tyiyui[1][0], 1, 1)
        j3pAffiche(froirf[0][0], null, '2. Clique sur la figure pour nommer (ou renommer) un point.')
        break
      case 'regle2':
        ptihd = addDefaultTable(tyiyui[0][0], 1, 3)
        j3pAffiche(ptihd[0][0], null, '1. Clique sur ')
        this.metsImage(ptihd[0][1], this.imageRegle2)
        j3pAffiche(ptihd[0][2], null, ' pour faire apparaitre la règle. ')
        froirf = addDefaultTable(tyiyui[1][0], 1, 1)
        j3pAffiche(froirf[0][0], null, '2. En cliquant sur la règle (et en laissant appuyé) tu peux déplacer la règle.')
        kflmkmds = addDefaultTable(tyiyui[2][0], 1, 3)
        j3pAffiche(kflmkmds[0][0], null, '3. En cliquant sur ')
        j3pAffiche(kflmkmds[0][2], null, ' (et en laissant appuyé) tu peux faire tourner la règle.')
        this.metsImage(kflmkmds[0][1], this.imageRouge)
        fjdslkdi = addDefaultTable(tyiyui[3][0], 1, 3)
        j3pAffiche(fjdslkdi[0][0], null, '4. Clique sur ')
        j3pAffiche(fjdslkdi[0][2], null, ' quand tu souhaites tracer.')
        this.metsImage(fjdslkdi[0][1], this.imageCrayon)
        ptihd = addDefaultTable(tyiyui[4][0], 2, 1)
        j3pAffiche(ptihd[0][0], null, '5. Approche le crayon du bord de la règle. ')
        j3pAffiche(ptihd[1][0], null, '6. Clique pour commencer le segment, puis clique pour terminer le tracé. ')
        break
      case 'regle':
        ptihd = addDefaultTable(tyiyui[0][0], 1, 3)
        j3pAffiche(ptihd[0][0], null, '1. Clique sur ')
        this.metsImage(ptihd[0][1], this.imageRegle)
        j3pAffiche(ptihd[0][2], null, ' pour faire apparaitre la règle. ')
        froirf = addDefaultTable(tyiyui[1][0], 1, 1)
        j3pAffiche(froirf[0][0], null, '2. En cliquant sur la règle (et en laissant appuyé) tu peux déplacer la règle.')
        kflmkmds = addDefaultTable(tyiyui[2][0], 1, 3)
        j3pAffiche(kflmkmds[0][0], null, '3. En cliquant sur ')
        j3pAffiche(kflmkmds[0][2], null, ' (et en laissant appuyé) tu peux faire tourner la règle.')
        this.metsImage(kflmkmds[0][1], this.imageRouge)
        fjdslkdi = addDefaultTable(tyiyui[3][0], 1, 3)
        j3pAffiche(fjdslkdi[0][0], null, '4. Clique sur ')
        j3pAffiche(fjdslkdi[0][2], null, ' quand tu souhaites tracer.')
        this.metsImage(fjdslkdi[0][1], this.imageCrayon)
        ptihd = addDefaultTable(tyiyui[4][0], 3, 1)
        j3pAffiche(ptihd[0][0], null, '5. Approche le crayon du bord de la règle. ')
        j3pAffiche(ptihd[1][0], null, '6. Lâche le crayon pour tracer la droite. ')
        j3pAffiche(ptihd[2][0], null, '<i><b>remarque:</b> Si le tracé passe par des points, ils sont indiqués en rouge. \nDans ce cas, le tracé d’une partie de la droite est possible.</i>')
        break
      case 'gomme':
        ptihd = addDefaultTable(tyiyui[0][0], 1, 3)
        j3pAffiche(ptihd[0][0], null, '1. Clique sur ')
        this.metsImage(ptihd[0][1], this.imageGomme)
        j3pAffiche(ptihd[0][2], null, ' quand tu veux effacer une construction. ')
        froirf = addDefaultTable(tyiyui[1][0], 1, 1)
        j3pAffiche(froirf[0][0], null, '2. Quand tu survoles un élément que tu peux effacer, il devient rouge.')
        kflmkmds = addDefaultTable(tyiyui[2][0], 1, 1)
        j3pAffiche(kflmkmds[0][0], null, '3.Clique sur l’élément pour l’effacer.')
        break
      case 'compas':
        ptihd = addDefaultTable(tyiyui[0][0], 1, 3)
        j3pAffiche(ptihd[0][0], null, '1. Clique sur ')
        this.metsImage(ptihd[0][1], this.imageCompas)
        j3pAffiche(ptihd[0][2], null, ' pour faire apparaitre le compas. ')
        froirf = addDefaultTable(tyiyui[1][0], 1, 3)
        j3pAffiche(froirf[0][0], null, '2. En cliquant sur le compas (et en laissant appuyé) tu peux déplacer le compas.')
        kflmkmds = addDefaultTable(tyiyui[2][0], 1, 3)
        this.metsImage(kflmkmds[0][1], this.imageRouge)
        j3pAffiche(kflmkmds[0][0], null, '3. En cliquant sur ')
        j3pAffiche(kflmkmds[0][2], null, ' tu peux faire tourner et écarter le compas.')
        fjdslkdi = addDefaultTable(tyiyui[3][0], 1, 3)
        j3pAffiche(fjdslkdi[0][0], null, '4. Clique sur ')
        j3pAffiche(fjdslkdi[0][2], null, ' quand tu souhaites tracer.')
        this.metsImage(fjdslkdi[0][1], this.imageCrayon)
        ptihd = addDefaultTable(tyiyui[4][0], 2, 1)
        j3pAffiche(ptihd[0][0], null, '5. Approche le crayon de la mine du compas puis lâche le. ')
        j3pAffiche(ptihd[1][0], null, '6. Déplace la souris pour tracer. Clique quand tu as finis ')
        froirf = addDefaultTable(tyiyui[5][0], 1, 5)
        j3pAffiche(froirf[0][0], null, '\n&nbsp;&nbsp;<i><b>Clique sur  </b></i>')
        j3pAffiche(froirf[0][2], null, '\n&nbsp;<i><b>ou </b></i>&nbsp;')
        j3pAffiche(froirf[0][4], null, '\n<i><b> pour vérouiller l’écartement.</i></b>')
        this.metsImage(froirf[0][1], this.imageVerrou)
        this.metsImage(froirf[0][3], this.imageVerrou2)
        break
      case 'rapporteur':
        ptihd = addDefaultTable(tyiyui[0][0], 1, 3)
        j3pAffiche(ptihd[0][0], null, '1. Clique sur ')
        this.metsImage(ptihd[0][1], this.imageRapporteur)
        j3pAffiche(ptihd[0][2], null, ' pour faire apparaitre le rapporteur. ')
        froirf = addDefaultTable(tyiyui[1][0], 1, 1)
        j3pAffiche(froirf[0][0], null, '2. En cliquant sur le rapporteur (et en laissant appuyé) tu peux le déplacer.')
        kflmkmds = addDefaultTable(tyiyui[2][0], 1, 3)
        this.metsImage(kflmkmds[0][1], this.imageRouge)
        j3pAffiche(kflmkmds[0][0], null, '3. En cliquant sur ')
        j3pAffiche(kflmkmds[0][2], null, ' tu peux le faire tourner.')
        fjdslkdi = addDefaultTable(tyiyui[3][0], 1, 3)
        j3pAffiche(fjdslkdi[0][0], null, '4. Clique sur ')
        j3pAffiche(fjdslkdi[0][2], null, ' quand tu souhaites tracer.')
        this.metsImage(fjdslkdi[0][1], this.imageCrayon)
        ptihd = addDefaultTable(tyiyui[4][0], 1, 1)
        j3pAffiche(ptihd[0][0], null, '5. Approche le crayon du bord du rapporteur. ')
        break
      case 'equerre':
        ptihd = addDefaultTable(tyiyui[0][0], 1, 3)
        j3pAffiche(ptihd[0][0], null, '1. Clique sur ')
        this.metsImage(ptihd[0][1], this.imageEquerre)
        j3pAffiche(ptihd[0][2], null, ' pour faire apparaitre l’équerre. ')
        froirf = addDefaultTable(tyiyui[1][0], 1, 1)
        j3pAffiche(froirf[0][0], null, '2. En cliquant sur l’équerre (et en laissant appuyé) tu peux la déplacer.')
        kflmkmds = addDefaultTable(tyiyui[2][0], 1, 3)
        this.metsImage(kflmkmds[0][1], this.imageRouge)
        j3pAffiche(kflmkmds[0][0], null, '3. En cliquant sur ')
        j3pAffiche(kflmkmds[0][2], null, ' tu peux la faire tourner.')
        fjdslkdi = addDefaultTable(tyiyui[3][0], 1, 3)
        j3pAffiche(fjdslkdi[0][0], null, '4. Clique sur ')
        j3pAffiche(fjdslkdi[0][2], null, ' quand tu souhaites tracer.')
        this.metsImage(fjdslkdi[0][1], this.imageCrayon)
        ptihd = addDefaultTable(tyiyui[4][0], 1, 1)
        j3pAffiche(ptihd[0][0], null, '5. Approche le crayon d’un côté de l’équerre. ')
        break
      case 'renomme':
        j3pAffiche(tyiyui[0][0], null, 'Tu peux renommer une droite ou un cercle que tu as tracé en cliquant dessus. \n<i>(Quand il/elle devient bleu)</i> ')
        break
    }
    j3pAjouteBouton(this.whereAfficheAide, () => { this.whereAfficheAide.style.display = 'none' }, { value: 'Cacher' })
  }

  getPointPosition2 (a, b) {
    this.mtgAppLecteur.setApiDoc(this.svgId)
    return this.mtgAppLecteur.getPointPosition({ a: b.replace('#', ''), absCoord: true })
  }

  regle2 () {
    if (this.regleVisible && !this.lagrad) {
      for (let i = 0; i < this.idRegfleGrad.length; i++) {
        this.setVisible2(this.svgId, this.idRegfleGrad[i], true, false)
      }
      this.lagrad = true
      this.bloqueEventm = false
      this.mtgAppLecteur.updateFigure(this.svgId)
      this.whereAf('regle2')
      return
    }
    this.regleVisible = !this.regleVisible
    this.lagrad = this.regleVisible
    for (let i = 0; i < this.idRegle.length; i++) {
      this.setVisible2(this.svgId, this.idRegle[i], this.regleVisible, false)
    }
    for (let i = 0; i < this.idReglePoly.length; i++) {
      this.setVisible2(this.svgId, this.idReglePoly[i], this.regleVisible, true)
    }
    for (let i = 0; i < this.idRegfleGrad.length; i++) {
      this.setVisible2(this.svgId, this.idRegfleGrad[i], this.regleVisible, false)
    }
    // this.setVisible2(this.svgId, this.idPtRegle.pt2, this.regleVisible, false)
    this.setVisible2(this.svgId, this.idPtRegle.ma, this.regleVisible, false)
    if (this.regleVisible && this.me.etat === 'correction') {
      this.viregommEtrot()
      this.whereAf('regle2')
      this.mtgAppLecteur.setPointPosition(this.svgId, this.idPtRegle.pt, this.reglebasx, this.reglebasy, true)
      this.mtgAppLecteur.setPointPosition(this.svgId, '#pttttt', this.reglebasx + (219 * this.zoom), this.reglebasy + (47 * this.zoom), true)
      for (let i = 0; i < this.idReglePoly.length; i++) {
        const ff11 = function () {
          if (this.unecapt()) return
          if (this.lepoint || this.lepoint4) return
          if (this.mtgAppLecteur.getDoc(this.svgId).pointCapture) return
          this.mtgAppLecteur.getDoc(this.svgId).defaultCursor = 'pointer'
        }
        const ff22 = function () {
          if (this.unecapt()) return
          if (this.lepoint || this.lepoint4) return
          this.mtgAppLecteur.getDoc(this.svgId).defaultCursor = null
        }
        const ff33 = function (ev) {
          if (this.mtgAppLecteur.getDoc(this.svgId).pointCapture) return
          if (this.lepoint || this.lepoint4) return
          if (this.unecapt()) return
          this.capRegle = true
          this.x = ev.clientX
          this.y = ev.clientY
          this.basereg = this.getPointPosition2(this.svgId, this.idPtRegle.pt)
          this.basereg2 = this.getPointPosition2(this.svgId, '#pttttt')
          ev.preventDefault()
        }
        const ff44 = function (ev) {
          if (this.lepoint || this.lepoint4) return
          if (this.unecapt()) return
          const tmoio = this.getPointPosition2(this.svgId, '#pttttt')
          this.x = ev.changedTouches[0].clientX
          this.y = ev.changedTouches[0].clientY
          const GG = this.zonefig.getBoundingClientRect()
          /*
          const Gco = j3pGetPos(this.mepact)
          GG.x = GG.x - Gco.x / 2
          GG.y = GG.y - Gco.y / 2
          */
          const gfdjkslg = { x: this.x - GG.x, y: this.y - GG.y }
          if (this.dist(gfdjkslg, tmoio) < 15) return
          this.capRegle2 = true

          this.basereg = this.getPointPosition2(this.svgId, this.idPtRegle.pt)
          this.basereg2 = this.getPointPosition2(this.svgId, '#pttttt')

          this.mtgAppLecteur.setPointPosition(this.svgId, '#pointlibre', this.x - GG.x, this.y - GG.y, true)
          this.x = this.x - GG.x
          this.y = this.y - GG.y
        }
        this.mtgAppLecteur.addEventListener(this.svgId, this.idReglePoly[i], 'mouseover', ff11.bind(this))
        this.mtgAppLecteur.addEventListener(this.svgId, this.idReglePoly[i], 'mouseout', ff22.bind(this))
        this.mtgAppLecteur.addEventListener(this.svgId, this.idReglePoly[i], 'mousedown', ff33.bind(this))
        this.mtgAppLecteur.addEventListener(this.svgId, this.idReglePoly[i], 'touchstart', ff44.bind(this))
      }
    } else {
      this.whereAfficheAide.style.display = 'none'
      this.mtgAppLecteur.setPointPosition(this.svgId, this.idPtRegle.pt, 5000, 5000, false)
      this.mtgAppLecteur.setPointPosition(this.svgId, '#pttttt', 5000 + (219 * this.zoom), 5000 + (47 * this.zoom), false)
      this.eteintPoint(false)
      this.mtgAppLecteur.updateFigure(this.svgId)
    }
    this.bloqueEventm = false
  }

  compas (b, f, b2) {
    this.compasVisible = !this.compasVisible
    if (f !== true) {
      this.verouV = true
    } else {
      this.verouV = this.verold
    }
    for (let i = 0; i < this.idComp.length; i++) {
      this.setVisible2(this.svgId, this.idComp[i], this.compasVisible, true)
    }
    for (let i = 0; i < this.idComp1.length; i++) {
      this.setVisible2(this.svgId, this.idComp1[i], this.compasVisible, true)
    }
    this.setVisible2(this.svgId, '#compsur5', this.compasVisible, true)
    this.setColor2(this.svgId, '#compsur5', 0, 0, 0, true)
    this.setVisible2(this.svgId, '#verouv', this.verouV && this.compasVisible, true)
    this.setVisible2(this.svgId, '#verfer', !this.verouV && this.compasVisible, true)
    this.setVisible2(this.svgId, this.idCompPt.pointe, false, true)
    this.setVisible2(this.svgId, this.idCompPt.geremine, this.compasVisible, true)
    if (this.compasVisible) {
      this.viregommEtrot()
      if (b !== true) {
        this.mtgAppLecteur.setPointPosition(this.svgId, '#' + this.idCompPt.pointe, 350, 300, true)
        this.mtgAppLecteur.setPointPosition(this.svgId, '#' + this.idCompPt.geremine, 350 + 100 * this.zoom, 300 - 50 * this.zoom, true)
      }
      if (this.me.etat === 'correction') {
        this.whereAf('compas')
        for (let i = 0; i < this.idComp.length; i++) {
          if (this.idComp[i] === '#comppoly3') continue
          const ff1 = function () {
            if (this.unecapt()) return
            if (this.onco) return
            if (this.mtgAppLecteur.getDoc(this.svgId).pointCapture) return
            this.mtgAppLecteur.getDoc(this.svgId).defaultCursor = 'pointer'
          }
          const ff2 = function () {
            if (this.mtgAppLecteur.getDoc(this.svgId).pointCapture) return
            this.mtgAppLecteur.getDoc(this.svgId).defaultCursor = null
          }
          const ff3 = function (ev) {
            if (this.mtgAppLecteur.getDoc(this.svgId).pointCapture) return
            if (this.unecapt()) return
            this.capComp = true
            this.x = ev.clientX
            this.y = ev.clientY
            this.basecomp = this.getPointPosition2(this.svgId, this.idCompPt.pointe)
            this.basecompgere = this.getPointPosition2(this.svgId, this.idCompPt.geremine)

            this.basecompv = this.getPointPosition2(this.svgId, 'ptverB')

            ev.preventDefault()
          }
          const ff4 = function (ev) {
            if (this.mtgAppLecteur.getDoc(this.svgId).pointCapture) return
            if (this.unecapt()) return
            const tmoio = this.getPointPosition2(this.svgId, this.idCompPt.geremine)
            this.x = ev.changedTouches[0].clientX
            this.y = ev.changedTouches[0].clientY
            const GG = this.zonefig.getBoundingClientRect()
            /*
            const Gco = j3pGetPos(this.mepact)
            GG.x = GG.x - Gco.x / 2
            GG.y = GG.y - Gco.y / 2
            */
            const gfdjkslg = { x: this.x - GG.x, y: this.y - GG.y }
            if (this.dist(gfdjkslg, tmoio) < 15) return

            this.mtgAppLecteur.setPointPosition(this.svgId, '#pointlibre', this.x - GG.x, this.y - GG.y, true)
            this.x = this.x - GG.x
            this.y = this.y - GG.y
            this.capComp2 = true

            this.basecomp = this.getPointPosition2(this.svgId, this.idCompPt.pointe)
            this.basecompgere = this.getPointPosition2(this.svgId, this.idCompPt.geremine)

            this.basecompv = this.getPointPosition2(this.svgId, 'ptverB')
          }
          this.mtgAppLecteur.addEventListener(this.svgId, this.idComp[i], 'mouseover', ff1.bind(this))
          this.mtgAppLecteur.addEventListener(this.svgId, this.idComp[i], 'mouseout', ff2.bind(this))
          this.mtgAppLecteur.addEventListener(this.svgId, this.idComp[i], 'mousedown', ff3.bind(this))
          this.mtgAppLecteur.addEventListener(this.svgId, this.idComp[i], 'touchstart', ff4.bind(this))
        }
        const ff11 = function () {
          if (this.unecapt()) return
          if (this.mtgAppLecteur.getDoc(this.svgId).pointCapture) return
          this.mtgAppLecteur.getDoc(this.svgId).defaultCursor = 'pointer'
          this.setColor2(this.svgId, '#compsur5', 255, 0, 0, true)
        }
        const ff22 = function () {
          if (this.mtgAppLecteur.getDoc(this.svgId).pointCapture) return
          this.mtgAppLecteur.getDoc(this.svgId).defaultCursor = null
          this.setColor2(this.svgId, '#compsur5', 0, 0, 0, true)
        }
        const ff33 = function (ev) {
          if (this.mtgAppLecteur.getDoc(this.svgId).pointCapture) return
          this.capComp = false
          this.capCAP = true
          this.setColor2(this.svgId, '#compsur5', 0, 0, 0, true)
          this.gereCompas(ev)
        }
        this.setVisible2(this.svgId, '#gih', this.compasVisible, true)
        this.setColor2(this.svgId, '#gih', 0, 0, 0, true, 0)
        this.mtgAppLecteur.addEventListener(this.svgId, '#comppoly3', 'mouseover', ff11.bind(this))
        this.mtgAppLecteur.addEventListener(this.svgId, '#comppoly3', 'mouseout', ff22.bind(this))
        this.mtgAppLecteur.addEventListener(this.svgId, '#comppoly3', 'mouseup', ff33.bind(this))
        this.mtgAppLecteur.addEventListener(this.svgId, '#gih', 'mouseover', ff11.bind(this))
        this.mtgAppLecteur.addEventListener(this.svgId, '#gih', 'mouseout', ff22.bind(this))
        this.mtgAppLecteur.addEventListener(this.svgId, '#gih', 'mouseup', ff33.bind(this))
        if (this.verouV) {
          this.mtgAppLecteur.addEventListener(this.svgId, '#verouv', 'mousedown', this.changVer.bind(this))
        } else {
          this.mtgAppLecteur.addEventListener(this.svgId, '#verfer', 'mousedown', this.changVer.bind(this))
        }
      }
    } else {
      this.whereAfficheAide.style.display = 'none'
      this.setVisible2(this.svgId, this.idCompPt.pointe, false, false)
      this.setVisible2(this.svgId, this.idCompPt.geremine, false, false)
      if (b !== true) {
        this.mtgAppLecteur.setPointPosition(this.svgId, '#' + this.idCompPt.pointe, 1000, 1000, false)
        this.mtgAppLecteur.setPointPosition(this.svgId, '#' + this.idCompPt.geremine, 1030, 1000, false)
      }
      if (b2 !== false) this.mtgAppLecteur.updateFigure(this.svgId)
    }
    this.bloqueEventm = false
  }

  changVer () {
    if (this.mtgAppLecteur.getDoc(this.svgId).pointCapture) return
    if (this.unecapt()) return
    let pt
    this.verouV = !this.verouV
    let a1, a2
    if (!this.verouV) {
      a1 = '#verouv'
      a2 = '#verfer'
      pt = this.getPointPosition2(this.svgId, this.idCompPt.geremine)
      this.mtgAppLecteur.setPointPosition(this.svgId, '#ptverB', pt.x, pt.y, true)
    } else {
      a2 = '#verouv'
      a1 = '#verfer'
    }
    this.setVisible2(this.svgId, a1, false, true)
    this.setVisible2(this.svgId, a2, true, true)
    this.mtgAppLecteur.updateFigure(this.svgId)
    this.mtgAppLecteur.addEventListener(this.svgId, a2, 'mousedown', this.changVer.bind(this))
    this.bloqueEventm = false
  }

  equerre (b) {
    this.equerreVisible = !this.equerreVisible
    for (let i = 0; i < this.idEquerre.length; i++) {
      this.setVisible2(this.svgId, this.idEquerre[i], this.equerreVisible, false)
    }
    for (let i = 0; i < this.idEquerrePoly.length; i++) {
      this.setVisible2(this.svgId, this.idEquerrePoly[i], this.equerreVisible, true)
    }
    // this.setVisible2(this.svgId, this.idPtEquerre.pt, this.equerreVisible, true)
    this.setVisible2(this.svgId, this.EquerSurfa, this.equerreVisible, false)
    this.setVisible2(this.svgId, this.idPtEquerre.pt2, this.equerreVisible, false)
    this.setVisible2(this.svgId, this.idPtEquerre.ma, this.equerreVisible, false)
    if (this.equerreVisible && this.me.etat === 'correction') {
      this.whereAf('equerre')
      this.viregommEtrot()
      this.mtgAppLecteur.setPointPosition(this.svgId, this.idPtEquerre.pt, this.lXcentre, this.lYcentre, true)
      const zan = this.mtgAppLecteur.valueOf(this.svgId, 'equerangleC')
      this.mtgAppLecteur.setPointPosition(this.svgId, '#pteqma0', this.lXcentre + 124 * this.zoom * Math.cos(zan * Math.PI / 180), this.lYcentre - 124 * this.zoom * Math.sin(zan * Math.PI / 180), true)
      for (let i = 0; i < this.idEquerrePoly.length; i++) {
        const ff11 = function () {
          // if (this.unecapt()) return
          if (this.mtgAppLecteur.getDoc(this.svgId).pointCapture) return
          this.mtgAppLecteur.getDoc(this.svgId).defaultCursor = 'pointer'
          // this.setVisible2(this.svgId, this.idPtEquerre.pt2, true, true)
          // this.setVisible2(this.svgId, this.idPtEquerre.ma, true, true)
        }
        const ff22 = function () {
          // if (this.mtgAppLecteur.getDoc(this.svgId).pointCapture) return
          this.mtgAppLecteur.getDoc(this.svgId).defaultCursor = null
          // this.setVisible2(this.svgId, this.idPtEquerre.pt2, false, true)
          // this.setVisible2(this.svgId, this.idPtEquerre.ma, false, true)
        }
        const ff33 = function (ev) {
          if (this.mtgAppLecteur.getDoc(this.svgId).pointCapture) return
          if (this.unecapt()) return
          this.capEquer = true
          this.x = ev.clientX
          this.y = ev.clientY
          this.baseeq = this.getPointPosition2(this.svgId, this.idPtEquerre.pt)
          this.baseeq22 = this.getPointPosition2(this.svgId, '#pteqma0')
          this.baseeq3 = this.getPointPosition2(this.svgId, '#Eq234')
          this.baseeq4 = this.getPointPosition2(this.svgId, '#eq456')
          ev.preventDefault()
        }
        const ff44 = function (ev) {
          if (this.lepoint || this.lepoint4) return
          if (this.unecapt()) return
          const tmoio = this.getPointPosition2(this.svgId, '#pteqma0')
          this.x = ev.changedTouches[0].clientX
          this.y = ev.changedTouches[0].clientY
          const GG = this.zonefig.getBoundingClientRect()
          /*
          const Gco = j3pGetPos(this.mepact)
          GG.x = GG.x - Gco.x / 2
          GG.y = GG.y - Gco.y / 2
          */
          const gfdjkslg = { x: this.x - GG.x, y: this.y - GG.y }
          if (this.dist(gfdjkslg, tmoio) < 15) return
          this.capEquerre2 = true

          this.baseeq = this.getPointPosition2(this.svgId, this.idPtEquerre.pt)
          this.baseeq22 = this.getPointPosition2(this.svgId, '#pteqma0')
          this.baseeq3 = this.getPointPosition2(this.svgId, '#Eq234')
          this.baseeq4 = this.getPointPosition2(this.svgId, '#eq456')

          this.mtgAppLecteur.setPointPosition(this.svgId, '#pointlibre', this.x - GG.x, this.y - GG.y, true)
          this.x = this.x - GG.x
          this.y = this.y - GG.y
        }
        this.mtgAppLecteur.addEventListener(this.svgId, this.idEquerrePoly[i], 'mouseover', ff11.bind(this))
        this.mtgAppLecteur.addEventListener(this.svgId, this.idEquerrePoly[i], 'mouseout', ff22.bind(this))
        this.mtgAppLecteur.addEventListener(this.svgId, this.idEquerrePoly[i], 'mousedown', ff33.bind(this))
        this.mtgAppLecteur.addEventListener(this.svgId, this.idEquerrePoly[i], 'touchstart', ff44.bind(this))
      }
    } else {
      this.whereAfficheAide.style.display = 'none'
      this.mtgAppLecteur.setPointPosition(this.svgId, this.idPtEquerre.pt, 1000, 1000, false)
      if (b !== false) this.mtgAppLecteur.updateFigure(this.svgId)
    }
    this.bloqueEventm = false
  }

  rapporteur (b) {
    this.rapporteurVisible = !this.rapporteurVisible
    for (let i = 0; i < this.idRap.length; i++) {
      this.setVisible2(this.svgId, this.idRap[i], this.rapporteurVisible, true)
    }
    for (let i = 0; i < this.idRapPoly.length; i++) {
      this.setVisible2(this.svgId, this.idRapPoly[i], this.rapporteurVisible, true)
    }
    if (this.rapporteurVisible && this.me.etat === 'correction') {
      this.whereAf('rapporteur')
      this.viregommEtrot()
      this.mtgAppLecteur.setPointPosition(this.svgId, '#' + this.idPRap.pt, 350, this.lYcentre, true)
      this.mtgAppLecteur.setPointPosition(this.svgId, '#raprap10', 350, 200, true)
      for (let i = 0; i < this.idRapPoly.length; i++) {
        const ff11 = function () {
          // if (this.unecapt()) return
          if (this.mtgAppLecteur.getDoc(this.svgId).pointCapture) return
          this.mtgAppLecteur.getDoc(this.svgId).defaultCursor = 'pointer'
          // this.setVisible2(this.svgId, this.idPRap.ma, true, true)
        }
        const ff22 = function () {
          if (this.mtgAppLecteur.getDoc(this.svgId).pointCapture) return
          this.mtgAppLecteur.getDoc(this.svgId).defaultCursor = null
          // this.setVisible2(this.svgId, this.idPRap.pt2, false, true)
          // this.setVisible2(this.svgId, this.idPRap.ma, false, true)
        }
        const ff33 = function (ev) {
          if (this.mtgAppLecteur.getDoc(this.svgId).pointCapture) return
          if (this.unecapt()) return
          this.capRap = true
          this.x = ev.clientX
          this.y = ev.clientY
          this.baserap = this.getPointPosition2(this.svgId, this.idPRap.pt)
          this.baserap2 = this.getPointPosition2(this.svgId, 'raprap10')
          ev.preventDefault()
        }
        const ff44 = function (ev) {
          if (this.lepoint || this.lepoint4) return
          if (this.unecapt()) return
          const tmoio = this.getPointPosition2(this.svgId, 'raprap10')
          this.x = ev.changedTouches[0].clientX
          this.y = ev.changedTouches[0].clientY
          const GG = this.zonefig.getBoundingClientRect()
          /*
          const Gco = j3pGetPos(this.mepact)
          GG.x = GG.x - Gco.x / 2
          GG.y = GG.y - Gco.y / 2
          */
          const gfdjkslg = { x: this.x - GG.x, y: this.y - GG.y }
          if (this.dist(gfdjkslg, tmoio) < 15) return
          this.capRap2 = true

          this.baserap = this.getPointPosition2(this.svgId, this.idPRap.pt)
          this.baserap2 = this.getPointPosition2(this.svgId, 'raprap10')
          this.mtgAppLecteur.setPointPosition(this.svgId, '#pointlibre', this.x - GG.x, this.y - GG.y, true)
          this.x = this.x - GG.x
          this.y = this.y - GG.y
        }
        this.mtgAppLecteur.addEventListener(this.svgId, this.idRapPoly[i], 'mouseover', ff11.bind(this))
        this.mtgAppLecteur.addEventListener(this.svgId, this.idRapPoly[i], 'mouseout', ff22.bind(this))
        this.mtgAppLecteur.addEventListener(this.svgId, this.idRapPoly[i], 'mousedown', ff33.bind(this))
        this.mtgAppLecteur.addEventListener(this.svgId, this.idRapPoly[i], 'touchstart', ff44.bind(this))
      }
    } else {
      this.whereAfficheAide.style.display = 'none'
      this.mtgAppLecteur.setPointPosition(this.svgId, '#' + this.idPRap.pt, 1000, 1000, false)
      if (b !== false) this.mtgAppLecteur.updateFigure(this.svgId)
    }
    this.bloqueEventm = false
  }

  regule (n) {
    while (n < -180) { n += 360 }
    while (n > 180) { n -= 360 }
    return n
  }

  dist (p1, p2) {
    return Math.sqrt((p1.x - p2.x) * (p1.x - p2.x) + (p1.y - p2.y) * (p1.y - p2.y))
  }

  affModale (texte, titre, close) {
    if (titre === undefined) titre = 'Erreur'
    this.whereAfficheAide.style.display = 'none'
    const yy = j3pModale2({ titre, contenu: '', divparent: this.divparent })
    j3pAffiche(yy, null, texte)
    /// /PB ICI
    yy.style.color = (!close) ? '#096788' : '#af2020'
    yy.style.cursor = 'default'
    yy.masque.style.cursor = 'default'
    if (close) {
      j3pAddContent(yy, '\n\n')
      j3pAjouteBouton(yy, () => {
        j3pDetruit('modale2')
        j3pDetruit('j3pmasque2')
      },
      { value: 'OK' })
    }
    return yy
  }

  toutDroite () {
    this.mtgAppLecteur.getDoc(this.svgId).defaultCursor = ''
    this.whereAfficheAide.style.display = 'none'
    const yy = j3pModale2({ titre: 'Tracer toute la droite ?', contenu: '', divparent: this.divparent })
    const tabgh = addDefaultTable(yy, 1, 5)
    const ft1 = function () { this.finalement(false) }
    const ft2 = function () { this.finalement(true) }
    j3pAjouteBouton(tabgh[0][0], ft1.bind(this), { className: '', value: 'Oui' })
    j3pAjouteBouton(tabgh[0][2], ft2.bind(this), { className: '', value: 'Non' })
    tabgh[0][1].style.width = '40px'
    tabgh[0][3].style.width = '40px'
    const elem = $('.croix')[0]
    j3pDetruit(elem)
    const ghj = function () {
      j3pDetruit('modale2')
      j3pDetruit('j3pmasque2')
      this.renvoieCrayon()
    }
    j3pAjouteBouton(tabgh[0][4], ghj.bind(this), { className: '', value: 'Annuler' })
    yy.style.cursor = 'default'
    yy.masque.style.cursor = 'default'
  }

  affModaleQuest (texte, ki) {
    this.bloquenomme = true
    this.mtgAppLecteur.getDoc(this.svgId).defaultCursor = ''
    this.whereAfficheAide.style.display = 'none'
    const yy = j3pModale2({ titre: 'Nommer le point', contenu: '', divparent: this.divparent })
    this.stab1 = addDefaultTable(yy, 3, 1)
    this.stab2 = addDefaultTable(this.stab1[0][0], 1, 3)
    this.stab1[1][0].style.color = this.me.styles.cfaux
    j3pAffiche(this.stab2[0][0], null, texte)
    const onEnter = () => { this.sortQuestList(ki) }
    this.affmodQ = new ZoneStyleMathquill1(this.stab2[0][1], {
      ailleurs: 'modale2',
      center: true,
      restric: 'ABCDEFGHIJKLMNOPQRSTUVWXYZ',
      limite: 0,
      limitenb: 0,
      enter: () => { onEnter(ki) }
    })
    j3pAjouteBouton(this.stab2[0][2], onEnter, { className: 'ok', value: 'OK' })
    const elem = $('.croix')[0]
    j3pDetruit(elem)
    const onEnter2 = this.annuleQuest.bind(this)
    j3pAjouteBouton(this.stab1[2][0], function () { onEnter2(ki) }, { className: '', value: 'Annuler' })
    setTimeout(() => { this.affmodQ.focus() }, 120)
    yy.style.cursor = 'default'
    yy.masque.style.cursor = 'default'
  }

  annuleQuest (ki) {
    this.affmodQ.disable()
    j3pDetruit('modale2')
    j3pDetruit('j3pmasque2')
    this.viregommEtrot()
    this.bloquenomme = false
    if (this.ptgardeaucasou !== undefined) {
      this.nomspris.push(this.ptgardeaucasou)
      this.mtgAppLecteur.setText(this.svgId, ki.aff, this.ptgardeaucasou, true)
    } else {
      this.listePointPris.pop()
      this.mtgAppLecteur.deleteElt({ elt: ki.point })
    }
  }

  sortQuest (a) {
    const nom = this.affmodQ.reponse()?.trim()
    if (nom === '') {
      return j3pAddContent(this.stab1[1][0], 'Il faut donner un nom !', { replace: true })
    }
    if (this.nomspris.includes(nom)) {
      setTimeout(() => { this.affmodQ.focus() }, 10)
      return j3pAddContent(this.stab1[1][0], `Il y a déjà un élément ${nom} !`, { replace: true })
    }
    this.nomspris.push(nom)
    a.nom = nom
    this.mtgAppLecteur.setText(this.svgId, '#' + a.aff, nom, true)
    this.affmodQ.disable()
    j3pDetruit('modale2')
    j3pDetruit('j3pmasque2')
    this.viregommEtrot()
    this.bloquenomme = false
  }

  getNom (k) {
    // reponse() ne devrait jamais retourner de null|undefined, si c’est toujours une string le ?. ne sert à rien, juste au cas où…
    const nom = this.affmodQ.reponse()?.trim()
    if (this.nomspris.includes(nom)) {
      j3pAddContent(this.stab1[1][0], `Il y a déjà un élément ${nom} !`, { replace: true })
      return false
    }
    if (nom.length !== 0) {
      if (k === 'droite' && !isNomDroiteValide(nom)) {
        j3pAddContent(this.stab1[1][0], 'Ce nom est incorrect !', { replace: true })
        return false
      }
      if (k === 'cercle' && !isNomCercleValide(nom)) {
        // il ne faut pas toujours les parenthèses sur le cercle
        // if (isNomCercleValide(`(${nom})`)) {
        //   j3pAddContent(this.stab1[1][0], 'Il faut mettre le nom entre parenthèses !', { replace: true })
        // } else {
        j3pAddContent(this.stab1[1][0], 'Ce nom est incorrect !', { replace: true })
        // }
        return false
      }
    }
    this.affmodQ.disable()
    j3pDetruit('modale2')
    j3pDetruit('j3pmasque2')
    this.viregommEtrot()
    return nom ?? ''
  }

  creeNewNom (ss) {
    ss.pointNom = this.newTag()
    ss.aff = this.newTag()
    this.mtgAppLecteur.addFreePoint({
      absCoord: true,
      tag: ss.pointNom,
      name: ss.pointNom,
      hidden: true,
      x: 100,
      y: 100
    })
    this.mtgAppLecteur.addLinkedText({
      a: ss.pointNom,
      tag: ss.aff,
      text: ss.nom,
      vAlign: 'middle',
      hAlign: 'center'
    })
    this.mtgAppLecteur.reclassBefore({ elt: ss.pointNom, elt2: 'unit2' })
    this.mtgAppLecteur.reclassBefore({ elt: ss.aff, elt2: 'unit2' })
  }

  sortQuestD (ss, k) {
    const nom = this.getNom(k)
    if (nom === false) return // y’a déjà eu l’avertissement
    let j1, j2, p0, p1
    if (k === 'droite') {
      p0 = ss.point1
      p1 = ss.point2
      this.aplace = 'dte'
      j1 = '#repd1'
      j2 = '#repd2'
    }
    if (k === 'cercle') {
      p0 = ss.centre
      p1 = ss.p1
      this.aplace = 'cerc'
      j1 = '#repcour1'
      j2 = '#repcour2'
    }
    if (nom !== '') {
      this.a1 = this.getPointPosition2(this.svgId, p0)
      this.a2 = this.getPointPosition2(this.svgId, p1)
      this.mtgAppLecteur.setPointPosition(this.svgId, j1, this.a1.x, this.a1.y, true)
      this.mtgAppLecteur.setPointPosition(this.svgId, j2, this.a2.x, this.a2.y, true)
      if (this.aplace === 'cerc') this.a2 = this.dist(this.a1, this.a2)
      this.nomspris.push(nom)
      ss.nom = nom
      this.creeNewNom(ss)
      this.lepoint3 = true
      this.mtgAppLecteur.setPointPosition(this.svgId, '#' + ss.pointNom, this.lXcentre, this.lYcentre, true)
      this.pointNom = ss.pointNom
      this.mtgAppLecteur.updateFigure(this.svgId)
      this.acons = { x: this.lXcentre, y: this.lYcentre }
      this.affBulle('Place le nom sur la figure')
    } else {
      ss.pointNom = undefined
      ss.aff = undefined
      ss.nom = undefined
    }
  } // sortQuestD

  sortQuestD2 (k, g) {
    const nom = this.getNom(k)
    if (nom === false) return // y’a déjà eu l’avertissement
    let gard, tub, j1, j2, p, p0, p1
    if (g.nom) {
      for (let i = 0; i < this.nomspris.length; i++) {
        if (this.nomspris[i] === g.nom) {
          this.nomspris.splice(i, 1)
          break
        }
      }
      this.mtgAppLecteur.deleteElt({ elt: g.pointNom })
      g.nom = undefined
      g.pointName = undefined
      g.aff = undefined
    }
    if (k === 'droite') {
      tub = this.listeDtePris
      this.aplace = 'dte'
      j1 = '#repd1'
      j2 = '#repd2'
      for (let i = 0; i < tub.length; i++) {
        if (tub[i].ligne === g.ligne) gard = i
      }
      p = tub[gard]
      p0 = p.point1
      p1 = p.point2
    }
    if (k === 'cercle') {
      tub = this.listeArcPris
      this.aplace = 'cerc'
      j1 = '#repcour1'
      j2 = '#repcour2'
      for (let i = 0; i < tub.length; i++) {
        if (tub[i].p2 === g.p2) gard = i
      }
      p = tub[gard]
      p0 = p.centre
      p1 = p.p1
    }
    if (nom !== '') {
      this.a1 = this.getPointPosition2(this.svgId, p0)
      this.a2 = this.getPointPosition2(this.svgId, p1)
      this.mtgAppLecteur.setPointPosition(this.svgId, j1, this.a1.x, this.a1.y, true)
      this.mtgAppLecteur.setPointPosition(this.svgId, j2, this.a2.x, this.a2.y, true)
      if (this.aplace === 'cerc') this.a2 = this.dist(this.a1, this.a2)
      this.nomspris.push(nom)
      p.nom = nom
      this.creeNewNom(p)
      this.lepoint3 = true
      this.mtgAppLecteur.setPointPosition(this.svgId, '#' + p.pointNom, this.lXcentre, this.lYcentre, true)
      this.pointNom = p.pointNom
      this.mtgAppLecteur.updateFigure(this.svgId)
      this.acons = { x: this.lXcentre, y: this.lYcentre }
      this.affBulle('Place le nom sur la figure')
    } else {
      g.pointNom = undefined
      g.aff = undefined
      g.nom = undefined
    }
  } // sortQuestD2

  segok (a, b, f, g, exa) {
    if (exa !== undefined) {
      return (this.dist(a, f) < 0.1 && this.dist(b, g) < 0.1) || (this.dist(a, g) < 0.1 && this.dist(b, f) < 0.1)
    }
    return (this.distPtSeg2(g, f, a) <= this.ds.Precis_long / 10 && this.distPtSeg2(g, f, b) <= this.ds.Precis_long / 10)
  }

  dteok (a, b, f, g) {
    return (this.distPtDtebis(f, a, g) < this.ds.Precis_long / 10) && (this.distPtDtebis(f, b, g) < this.ds.Precis_long / 10)
  }

  demidteok (a, b, f, g) {
    return (this.distPtDemiDtebis(f, a, g) < this.ds.Precis_long / 10) && (this.distPtDemiDtebis(f, b, g) < this.ds.Precis_long / 10)
  }

  cercleok (p1, p2, p3, ray) {
    if (this.dist(p1, p2) / this.unite > this.ds.Precis_long / 10) return 'centre'
    if (Math.abs(this.dist(p2, p3) / this.unite - parseFloat(ray)) > this.ds.Precis_long / 10) return 'rayon'
    return true
  }

  retrouveCentreRayon (nom) {
    for (let i = 0; i < this.listeArcPris.length; i++) {
      if (this.listeArcPris[i].nom === nom) {
        const cooC = this.getPointPosition2(this.svgId, this.listeArcPris[i].centre)
        const coo1 = this.getPointPosition2(this.svgId, this.listeArcPris[i].p1)
        return { centre: cooC, rayon: this.dist(cooC, coo1) / this.unite }
      }
    }
    for (let i = 0; i < this.condVerif.length; i++) {
      if (this.condVerif[i].nom === nom) {
        // je dois retrouver centre ou le reconstruire
        let lapProx = this.retrouveTag(this.recupe(this.condVerif[i].centre))
        if (lapProx === undefined) {
          lapProx = this.reconstruit(this.recupe(this.condVerif[i].centre))
        } else {
          lapProx = lapProx[0]
        }
        const cooC = this.getPointPosition2(this.svgId, lapProx)
        const lalong = this.calculFormule(this.recupe(this.condVerif[i].rayon), 'valeur')
        return { centre: cooC, rayon: lalong }
      }
    }
  }

  retrouveDprog (nom) {
    for (let i = 0; i < this.condVerif.length; i++) {
      if (this.condVerif[i].nom === nom) {
        return this.condVerif[i]
      }
    }
  }

  yaseg (pt1, pt2, exa, pointille) {
    let pt3, pt4, ok, okpoint
    const nums = []
    ok = false
    okpoint = true
    for (let j = 0; j < this.listeSegPris.length; j++) {
      pt3 = this.getPointPosition2(this.svgId, this.listeSegPris[j].point1)
      pt4 = this.getPointPosition2(this.svgId, this.listeSegPris[j].point2)
      ok = ok || (this.segok(pt1, pt2, pt3, pt4, exa) && this.listeSegPris[j].viz)
      if (this.segok(pt1, pt2, pt3, pt4, exa)) nums.push(j)
    }
    if (this.yaaapointill) {
      for (let i = 0; i < nums.length; i++) {
        const ac1 = Boolean(this.listeSegPris[nums[i]].pointille)
        const ac2 = Boolean(pointille)
        okpoint = okpoint && (ac1 === ac2)
      }
      if (!okpoint) return 'pointille'
    }
    if (exa !== undefined || this.yaaapointill) return ok
    if (!ok) {
      // verif les droites
      for (let j = 0; j < this.listeDtePris.length; j++) {
        pt3 = this.getPointPosition2(this.svgId, this.listeDtePris[j].point1)
        pt4 = this.getPointPosition2(this.svgId, this.listeDtePris[j].point2)
        ok = ok || (this.dteok(pt1, pt2, pt3, pt4) && this.listeDtePris[j].viz)
      }
      if (!ok) {
        // verif les demi-droites
        for (let j = 0; j < this.listeDemiDPris.length; j++) {
          pt3 = this.getPointPosition2(this.svgId, this.listeDemiDPris[j].point1)
          pt4 = this.getPointPosition2(this.svgId, this.listeDemiDPris[j].point2)
          ok = ok || (this.demidteok(pt1, pt2, pt3, pt4) && this.listeDemiDPris[j].viz)
        }
        if (!ok) {
          // verif les combi

        }
      }
    }
    return ok
  }

  yasegRenv (pt1, pt2) {
    let pt3, pt4, ok
    for (let j = 0; j < this.listeSegPris.length; j++) {
      pt3 = this.getPointPosition2(this.svgId, this.listeSegPris[j].point1)
      pt4 = this.getPointPosition2(this.svgId, this.listeSegPris[j].point2)
      if (this.segok(pt1, pt2, pt3, pt4) && this.listeSegPris[j].viz) return this.listeSegPris[j].ligne
    }

    if (!ok) {
      // verif les droites
      for (let j = 0; j < this.listeDtePris.length; j++) {
        pt3 = this.getPointPosition2(this.svgId, this.listeDtePris[j].point1)
        pt4 = this.getPointPosition2(this.svgId, this.listeDtePris[j].point2)
        if (this.dteok(pt1, pt2, pt3, pt4) && this.listeDtePris[j].viz) return this.listeDtePris[j].ligne
      }
      if (!ok) {
        // verif les demi-droites
        for (let j = 0; j < this.listeDemiDPris.length; j++) {
          pt3 = this.getPointPosition2(this.svgId, this.listeDemiDPris[j].point1)
          pt4 = this.getPointPosition2(this.svgId, this.listeDemiDPris[j].point2)
          if (this.demidteok(pt1, pt2, pt3, pt4) && this.listeDemiDPris[j].viz) return this.listeDemiDPris[j].ligne
        }
      }
    }
  }

  yadroite (pt1, pt2) {
    let pt3, pt4
    for (let j = 0; j < this.listeSegPris.length; j++) {
      pt3 = this.getPointPosition2(this.svgId, this.listeSegPris[j].point1)
      pt4 = this.getPointPosition2(this.svgId, this.listeSegPris[j].point2)
      if (this.distPtSeg2(pt3, pt4, pt1) <= 0.1 && this.distPtSeg2(pt3, pt4, pt2) <= 0.1 && this.dist(pt1, pt3) > 5 && this.dist(pt1, pt4) > 5 && this.dist(pt2, pt3) > 5 && this.dist(pt2, pt4) > 5) return true
    }
    return false
  }

  yademid (pt1, pt2, exa) {
    let pt3, pt4
    for (let j = 0; j < this.listeSegPris.length; j++) {
      pt3 = this.getPointPosition2(this.svgId, this.listeSegPris[j].point1)
      pt4 = this.getPointPosition2(this.svgId, this.listeSegPris[j].point2)
      if (exa) {
        if ((this.dist(pt3, pt1) < 0.1 || this.dist(pt4, pt1) < 0.1) && this.distPtSeg2(pt3, pt4, pt2) < 0.1 && this.dist(pt1, pt2) < this.dist(pt3, pt4)) return true
      } else {
        if (this.distPtSeg2(pt3, pt4, pt2) < 0.1 && this.distPtSeg2(pt3, pt4, pt1) < 0.1) return true
      }
    }
    for (let j = 0; j < this.listeDemiDPris.length; j++) {
      pt3 = this.getPointPosition2(this.svgId, this.listeDemiDPris[j].point1)
      pt4 = this.getPointPosition2(this.svgId, this.listeDemiDPris[j].point2)
      if ((this.dist(pt3, pt1) < 0.1) && (this.distPtSeg2(pt3, pt4, pt2) < 0.1 || this.distPtSeg2(pt1, pt2, pt4) < 0.1)) return true
    }
    if (!exa) {
      for (let j = 0; j < this.listeDtePris.length; j++) {
        pt3 = this.getPointPosition2(this.svgId, this.listeDtePris[j].point1)
        pt4 = this.getPointPosition2(this.svgId, this.listeDtePris[j].point2)
        const yap1 = this.distPtSeg2(pt3, pt4, pt1) <= 0.1
        const yap2 = this.distPtSeg2(pt3, pt4, pt2) <= 0.1
        const yap3 = this.distPtSeg2(pt1, pt2, pt3) <= 0.1
        const yap4 = this.distPtSeg2(pt1, pt2, pt4) <= 0.1
        const p1egalp3 = this.dist(pt1, pt3) < 0.1
        const p1egalp4 = this.dist(pt1, pt4) < 0.1
        const p2egalp3 = this.dist(pt2, pt3) < 0.1
        const p2egalp4 = this.dist(pt2, pt4) < 0.1
        if (p1egalp3) {
          if (p2egalp4 || yap2 || yap4) return true
        }
        if (p1egalp4) {
          if (p2egalp3 || yap2 || yap3) return true
        }
        if (p2egalp3) {
          if (p1egalp4 || yap1 || yap4) return true
        }
        if (p2egalp4) {
          if (p1egalp3 || yap1 || yap3) return true
        }
        if (!p1egalp3 && !p1egalp4 && !p2egalp3 && !p2egalp4) {
          let nbtrue = 0
          if (yap1) nbtrue++
          if (yap2) nbtrue++
          if (yap3) nbtrue++
          if (yap4) nbtrue++
          if (nbtrue > 1) return true
        }
      }
    }
    return false
  }

  lang (pt1, pt2, pt3) {
    return Math.abs(this.regule((Math.atan2(pt1.x - pt2.x, pt2.y - pt1.y) - Math.atan2(pt3.x - pt2.x, pt2.y - pt3.y)) * 180 / Math.PI))
  }

  faisLongCo () {
    this.leslongCo = []
    for (let i = 0; i < this.nomspris.length; i++) {
      if (this.typeDe(this.nomspris[i]) !== 'point') continue
      const pt1 = this.getPointPosition2(this.svgId, this.htmlPoint(this.nomspris[i]))
      for (let j = i + 1; j < this.nomspris.length; j++) {
        if (i === j) continue
        if (this.typeDe(this.nomspris[j]) !== 'point') continue
        const pt2 = this.getPointPosition2(this.svgId, this.htmlPoint(this.nomspris[j]))
        this.leslongCo.push({
          nom1: this.nomspris[i] + this.nomspris[j],
          nom2: this.nomspris[j] + this.nomspris[i],
          long: this.dist(pt1, pt2) / this.unite
        })
      }
    }
  }

  distPtSeg2 (e, f, r) {
    if (r.x + this.ds.Precis_long / 10 * 3 * this.unite < Math.min(e.x, f.x)) return 100000
    if (r.x - this.ds.Precis_long / 10 * 3 * this.unite > Math.max(e.x, f.x)) return 100000
    const a1 = (e.y - f.y) / (e.x - f.x)
    const b1 = e.y - a1 * e.x
    const a2 = -1 / a1
    const b2 = r.y - a2 * r.x
    const c = {}
    c.x = (b2 - b1) / (a1 - a2)
    c.y = a1 * c.x + b1
    if (a1 > 99999999 || a1 < -999999999) {
      c.x = e.x
      c.y = r.y
    }
    if (a1 > -0.0001 && a1 < 0.0001) {
      c.y = e.y
      c.x = r.x
    }
    if (e.x === f.x) {
      c.x = e.x
      c.y = r.y
    }
    if (e.y === f.y) {
      c.y = e.y
      c.x = r.x
    }
    return this.dist(c, r) / this.unite * this.zoom
  }

  distPtDtebis (e, r, f) {
    const a1 = (e.y - f.y) / (e.x - f.x)
    const b1 = e.y - a1 * e.x
    const a2 = -1 / a1
    const b2 = r.y - a2 * r.x
    const c = {}
    c.x = (b2 - b1) / (a1 - a2)
    c.y = a1 * c.x + b1
    if (a1 > 99999999 || a1 < -99999999) {
      c.x = e.x
      c.y = r.y
    }
    if (a1 < 0.0001 && a1 > -0.0001) {
      c.y = e.y
      c.x = r.x
    }
    /*
    if (Math.abs(e.x - f.x) < 0.0000001) {
      c.x = e.x
      c.y = r.y
    }
    if (Math.abs(e.y - f.y) < 0.0000001) {
      c.y = e.y
      c.x = r.x
    }
    */
    return this.dist(c, r) / this.unite
  }

  distPtDemiDtebis (e, r, f) {
    if (e.x < f.x) {
      if (r.x < e.x) return this.dist(e, r)
    }
    if (e.x > f.x) {
      if (r.x > e.x) return this.dist(e, r)
    }
    if (e.x === f.x) {
      if (e.y < f.y) {
        if (r.y < e.y) return this.dist(e, r)
      } else {
        if (r.y > e.y) return this.dist(e, r)
      }
    }
    const a1 = (e.y - f.y) / (e.x - f.x)
    const b1 = e.y - a1 * e.x
    const a2 = -1 / a1
    const b2 = r.y - a2 * r.x
    const c = {}
    c.x = (b2 - b1) / (a1 - a2)
    c.y = a1 * c.x + b1
    if (Math.abs(e.x - f.x) < 0.0000001) {
      c.x = e.x
      c.y = r.y
    }
    if (Math.abs(e.y - f.y) < 0.0000001) {
      c.y = e.y
      c.x = r.x
    }
    return this.dist(c, r) / this.unite
  }

  distPtDte (seg, pt) {
    const a = this.htmlPoint(pt)
    const b = this.htmlPoint(seg[1])
    const c = this.htmlPoint(seg[2])
    const r = this.getPointPosition2(this.svgId, a)
    const e = this.getPointPosition2(this.svgId, b)
    const f = this.getPointPosition2(this.svgId, c)
    return this.distPtDtebis(e, r, f)
  }

  htmlPoint (ki) {
    for (let i = 0; i < this.listePointPris.length; i++) {
      if (this.listePointPris[i].nom === ki) {
        return this.listePointPris[i].point
      }
    }
    return 'Q'
  }

  afficheIm (koi, b) {
    switch (koi) {
      case 'point':
        this.mtgAppLecteur.setPointPosition(this.svgId, '#Pointpt', 752, 10, true)
        this.setVisible2(this.svgId, '#PointIm', b, true)
        break
      case 'regle':
        this.mtgAppLecteur.setPointPosition(this.svgId, '#ptImreg', 696, 10, true)
        this.setVisible2(this.svgId, '#Imreg', b, true)
        break
      case 'regle2':
        this.mtgAppLecteur.setPointPosition(this.svgId, '#ptImreg2', 700, 10, true)
        this.setVisible2(this.svgId, '#Imreg2', b, true)
        break
      case 'point2':
        this.mtgAppLecteur.setPointPosition(this.svgId, '#ptImcray', 752, 10, true)
        this.setVisible2(this.svgId, '#Imcray', b, true)
        break
      case 'compas':
        this.mtgAppLecteur.setPointPosition(this.svgId, '#ptImcomp', 752, 62, true)
        this.setVisible2(this.svgId, '#Imcomp', b, true)
        break
      case 'equerre':
        this.mtgAppLecteur.setPointPosition(this.svgId, '#ptImeq', 752, 62, true)
        this.setVisible2(this.svgId, '#Imeq', b, true)
        break
      case 'rap':
        this.mtgAppLecteur.setPointPosition(this.svgId, '#ptImrap', 752, 62, true)
        this.setVisible2(this.svgId, '#Imrap', b, true)
        break
      case 'gomme':
        this.mtgAppLecteur.setPointPosition(this.svgId, '#ptImgom', 752, 62, true)
        this.setVisible2(this.svgId, '#Imgom', b, true)
        break
    }
  }

  htmlcoPoint (ki) {
    for (let i = 0; i < this.ptCo.length; i++) {
      if (this.ptCo[i].nom === ki) return this.ptCo[i].html.point
    }
    ki = this.recupe(ki)
    for (let i = 0; i < this.ptCo.length; i++) {
      if (this.ptCo[i].nom === ki) return this.ptCo[i].html.point
    }
    for (let i = 0; i < this.listePointPris.length; i++) {
      if (this.listePointPris[i].nom === ki) return this.listePointPris[i].point
    }
  }

  recupe (t) {
    if (!t) return null
    for (let j = this.nomprisProg.length - 1; j > -1; j--) {
      const regex = new RegExp(this.nomprisProg[j].ki, 'g')
      t = t.replace(regex, this.nomprisProg[j].com)
    }
    return t
  }

  giveAB (n) {
    let ok = false
    let pt1, pt2
    for (let i = 0; i < this.dteco.length; i++) {
      if (this.dteco[i].nom === n) {
        pt1 = this.getPointPosition2(this.svgId, this.dteco[i].html.point1)
        pt2 = this.getPointPosition2(this.svgId, this.dteco[i].html.point2)
        ok = true
        break
      }
    }
    if (!ok) {
      for (let i = 0; i < this.segCo.length; i++) {
        if (this.segCo[i].nom === n) {
          pt1 = this.getPointPosition2(this.svgId, this.segCo[i].html.point1)
          pt2 = this.getPointPosition2(this.svgId, this.segCo[i].html.point2)
          ok = true
          break
        }
      }
    }
    if (!ok) {
      for (let i = 0; i < this.demiDco.length; i++) {
        if (this.demiDco[i].nom === n) {
          pt1 = this.getPointPosition2(this.svgId, this.demiDco[i].html.point1)
          pt2 = this.getPointPosition2(this.svgId, this.demiDco[i].html.point2)
          ok = true
          break
        }
      }
    }
    if (!ok) {
      n = this.recupe(n)
      try {
        pt1 = this.getPointPosition2(this.svgId, this.htmlcoPoint(n[1]))
        pt2 = this.getPointPosition2(this.svgId, this.htmlcoPoint(n[2]))
      } catch (e) {
        const obdep = this.retrouveI(this.listeDtePris, 'nom', n)
        pt1 = this.getPointPosition2(this.svgId, obdep.point1)
        pt2 = this.getPointPosition2(this.svgId, obdep.point2)
      }
    }
    const a = (pt2.y - pt1.y) / (pt2.x - pt1.x)
    const b = pt1.y - a * pt1.x
    if (pt2.x === pt1.x) {
      return { a: this.maxInt, b: pt1.x, pt1, pt2 }
    }
    return { a, b, pt1, pt2 }
  }

  affBulle (t) {
    const divStyle = {
      padding: '7px',
      margin: '2px',
      border: 'solid 2px',
      display: '',
      // en haut à droite, du parent, on décale un peu
      backgroundColor: '#aaaaaa',
      color: '#0404B4',
      borderRadius: '5px',
      verticalAlign: 'middle'
    }
    if (this.mondivBUlle !== undefined) j3pDetruit(this.mondivBUlle)
    this.leDivBull.classList.add('pourFitContent')
    this.mondivBUlle = j3pAddElt(this.leDivBull, 'div', '', { style: divStyle })
    this.leDivBull.style.minHeight = '48px'
    for (let j = this.nomprisProg.length - 1; j > -1; j--) {
      const regex = new RegExp(this.nomprisProg[j].ki, 'g')
      t = t.replace(regex, this.nomprisProg[j].com)
    }
    j3pAffiche(this.mondivBUlle, null, t)
  }

  getInstersectingPoints (a, b, cx, cy, r) {
    const lst = []
    const A = 1 + a * a
    const B = 2 * (-cx + a * b - a * cy)
    const C = cx * cx + cy * cy + b * b - 2 * b * cy - r * r
    const delta = B * B - 4 * A * C
    if (a === this.maxInt) {
      return [
        { x: cx, y: cy + r },
        { x: cx, y: cy - r }
      ]
    }
    let x, y
    if (delta > 0) {
      x = (-B - Math.sqrt(delta)) / (2 * A)
      y = a * x + b
      lst.push({ x, y })

      x = (-B + Math.sqrt(delta)) / (2 * A)
      y = a * x + b
      lst.push({ x, y })
    } else if (delta === 0) {
      x = -B / (2 * A)
      y = a * x + b
      lst.push({ x, y })
    }

    return lst
  }
}

export default ProgConstBase
