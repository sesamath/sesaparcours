import { j3pAjouteBouton, j3pArrondi, j3pClone, j3pDetruit, j3pEmpty, j3pGetRandomBool, j3pGetRandomInt, j3pNotify, j3pShuffle } from 'src/legacy/core/functions'
import { addDefaultTable } from 'src/legacy/themes/table'
import { mqNormalise } from 'src/lib/outils/conversion/casFormat'
import ProgConstBase from './ProgConstBase'

class ProgConst extends ProgConstBase {
  rempDans (koi, ki, tt) {
    const regex = new RegExp('§' + koi + '§', 'g')
    tt = tt.replace(regex, ki)
    return tt
  }

  animeRepere () {
    if (this.raz) return
    if (this.jarte) {
      setTimeout(this.animeRepere.bind(this), this.tempodeb)
      return
    }
    if (!this.consigne) return

    if (this.yapause) {
      setTimeout(this.animeRepere.bind(this), this.tempodeb)
      if (this.firstpause) {
        j3pEmpty(this.tabeffa[0][1])
        const ffpau = function () {
          this.yapause = false
          j3pEmpty(this.tabeffa[0][1])
          const ggpzu = function () {
            this.yapause = true
            this.firstpause = true
          }
          j3pAjouteBouton(this.tabeffa[0][1], ggpzu.bind(this), { className: '', value: 'Pause' })
        }
        j3pAjouteBouton(this.tabeffa[0][1], ffpau.bind(this), { className: '', value: 'Reprendre' })
        this.firstpause = false
      }
      return
    }
    let dx, dy, i, pt1, pt2, nnn, a, b, r, pt3, tt, kokp, a2, b2, dista, oktrouv, fautagrand, ch, r1, r2, pt4, nb
    switch (this.jensuisou) {
      case 'aF0':
        this.cptaf++
        if (this.cptaf > this.afaire.ki.length) {
          if (this.dansmulti) {
            this.jensuioumulti++
          } else {
            this.comptProg++
          }
          this.jensuisou = 'prog'
          setTimeout(this.animeRepere.bind(this), this.tempodeb)
          return
        }
        this.jensuisou = 'aF1'
        setTimeout(this.animeRepere.bind(this), this.tempo)
        break
      case 'aF1':
        this.afficheIm('point', true)
        this.PtArrive = this.getPointPosition2(this.svgId, this.htmlcoPoint(this.recupe(this.afaire.ki[this.cptaf - 1])))
        if (this.clignEff.length === 0) {
          if (this.afaire.ap !== undefined) {
            switch (this.afaire.ap.t) {
              case 'droite':
                kokp = this.dteco
                nb = this.ConsDtenb
                break
            }
            let i
            for (i = 0; i < kokp.length; i++) {
              if (kokp[i].nom === this.afaire.ap.nom) {
                this.kiclignote = kokp[i].html.ligne
                break
              }
            }
            this.clignEff.push({ ki: this.kiclignote, cool: nb > i })
            this.lalacool = nb > i
            this.numclignote = 0
            this.faitClignote()
            this.jensuisou = 'aF20'
            return
          }
        }
        this.ptplace = { x: 740, y: 30 }
        this.affichePtbalade()
        this.jensuisou = 'aF2'
        setTimeout(this.animeRepere.bind(this), this.tempo)
        break
      case 'aF20':
        this.ptplace = { x: 740, y: 30 }
        this.affichePtbalade()
        this.jensuisou = 'aF2'
        setTimeout(this.animeRepere.bind(this), this.tempo)
        break
      case 'aF2':
        dx = this.PtArrive.x - this.ptplace.x
        dy = this.PtArrive.y - this.ptplace.y
        if (Math.abs(dx) > Math.abs(dy)) {
          if (dx > 0) {
            this.ptplace.x = this.ptplace.x + 2
          } else {
            this.ptplace.x = this.ptplace.x - 2
          }
          if (dy > 0) {
            this.ptplace.y = this.ptplace.y + 2 * Math.abs(dy) / Math.abs(dx)
          } else {
            this.ptplace.y = this.ptplace.y - 2 * Math.abs(dy) / Math.abs(dx)
          }
        } else {
          if (dy > 0) {
            this.ptplace.y = this.ptplace.y + 2
          } else {
            this.ptplace.y = this.ptplace.y - 2
          }
          if (dx > 0) {
            this.ptplace.x = this.ptplace.x + 2 * Math.abs(dx) / Math.abs(dy)
          } else {
            this.ptplace.x = this.ptplace.x - 2 * Math.abs(dx) / Math.abs(dy)
          }
        }
        if ((Math.abs(dx) < 3 && Math.abs(dy) < 3)) {
          this.ptplace = j3pClone(this.PtArrive)
          this.jensuisou = 'aF3'
          this.deplacePtbalade()
          setTimeout(this.animeRepere.bind(this), this.tempodeb)
          return
        }
        this.deplacePtbalade()
        setTimeout(this.animeRepere.bind(this), this.tempo)
        break
      case 'aF3':
        for (let i = 0; i < this.ptCo.length; i++) {
          if (this.ptCo[i].nom === this.recupe(this.afaire.ki[this.cptaf - 1])) {
            this.setVisible2(this.svgId, this.ptCo[i].html.point, true, true)
            this.setVisible2(this.svgId, this.ptCo[i].html.aff, true, true)
            this.setVisible2(this.svgId, this.ptCo[i].html.suiv, true, true)
            this.setColor2(this.svgId, this.ptCo[i].html.point, 0, 0, 255, true)
            this.setColor2(this.svgId, this.ptCo[i].html.aff, 0, 0, 255, true)
            this.mtgAppLecteur.setText(this.svgId, this.ptCo[i].html.aff, this.ptCo[i].nom, true)
          }
        }
        this.cahchePointballade()
        this.jensuisou = 'aF0'
        setTimeout(this.animeRepere.bind(this), this.tempo)
        break
      case 'Efface': {
        this.pause = false
        this.firstpause = false
        j3pEmpty(this.coboot)
        this.tabeffa = addDefaultTable(this.coboot, 1, 3)
        const laf1 = function () {
          this.yapause = true
          this.firstpause = true
        }
        const laf2 = function () {
          this.accel = !this.accel
          this.bouAc.value = (this.accel) ? '>' : '>>>'
          if (!this.accel) {
            this.tempo = 20
            this.tempodeb = 500
            this.tempocligne = 400
          } else {
            this.tempo = 12
            this.tempodeb = 250
            this.tempocligne = 200
          }
        }
        j3pAjouteBouton(this.tabeffa[0][1], laf1.bind(this), { className: '', value: 'Pause' })
        this.bouAc = j3pAjouteBouton(this.tabeffa[0][2], laf2.bind(this), { className: '', value: '>>>' })
        this.recentre()
        // EffaceTout(false)
        this.jensuisou = 'prog'
        this.comptProg = 0
        this.nbpointsLIbres = 0
        this.remetDeb()
        setTimeout(this.animeRepere.bind(this), this.tempo)
      }
        break
      case 'prog':
        this.dervi = this.dervi2 = this.dervi3 = this.dervi5 = this.yamulmul = undefined
        this.passmult = 0
        if (this.compasVisible) this.compas()
        if (this.zoom !== 1) this.deZoomAll()
        this.racour = false
        if (this.mondivBUlle !== undefined) {
          j3pDetruit(this.mondivBUlle)
          this.mondivBUlle = undefined
        }
        for (let i = this.clignEff.length - 1; i > -1; i--) {
          if (this.clignEff[i].cool) {
            this.setColor2(this.svgId, this.clignEff[i].ki, 100, 100, 100, true)
          } else {
            this.setColor2(this.svgId, this.clignEff[i].ki, 100, 100, 250, true)
          }
          this.clignEff.splice(i, 1)
        }
        this.afficheIm('point', false)
        this.faudrazoom = false
        this.mtgAppLecteur.setPointPosition(this.svgId, this.idImCrayPt, 900, 500, true)
        if (this.fautvireRegle && this.regleVisible) {
          this.lagrad = false
          this.afficheIm('regle', false)
          this.regle()
        }
        if (this.comptProg === this.Programme.length) {
          this.jensuisou = 'fini'
          setTimeout(this.animeRepere.bind(this), this.tempodeb)
          return
        }
        switch (this.Programme[this.comptProg].type) {
          case 'point':
            this.jensuisou = 'pointDeb'
            this.afaire = this.Programme[this.comptProg]
            if (this.Programme[this.comptProg].textco !== undefined) {
              this.after = this.jensuisou
              this.jensuisou = 'previensco'
            }
            break
          case 'segment':
            this.jensuisou = 'segment'
            this.afaire = this.Programme[this.comptProg]
            if (this.Programme[this.comptProg].textco !== undefined) {
              this.after = this.jensuisou
              this.jensuisou = 'previensco'
            }
            break
          case 'cercle':
            this.jensuisou = 'cercle'
            this.afaire = this.Programme[this.comptProg]
            if (this.Programme[this.comptProg].textco !== undefined) {
              this.after = this.jensuisou
              this.jensuisou = 'previensco'
            }
            break
          case 'droite':
            this.jensuisou = 'droite'
            this.afaire = this.Programme[this.comptProg]
            if (this.Programme[this.comptProg].textco !== undefined) {
              this.after = this.jensuisou
              this.jensuisou = 'previensco'
            }
            break
          case 'af':
            this.afaire = j3pClone(this.Programme[this.comptProg])
            this.jensuisou = 'aF0'
            this.cptaf = 0
            if (this.Programme[this.comptProg].textco !== undefined) {
              this.after = this.jensuisou
              this.jensuisou = 'previensco'
            }
            break
          case 'demi-droite':
            this.jensuisou = 'demidroite'
            this.afaire = j3pClone(this.Programme[this.comptProg])
            if (this.Programme[this.comptProg].textco !== undefined) {
              this.after = this.jensuisou
              this.jensuisou = 'previensco'
            }
            break
          case 'multi':
            if (!this.dansmulti) {
              this.dansmulti = true
              this.jensuioumulti = 0
            }
            if (this.jensuioumulti === this.Programme[this.comptProg].cont.length) {
              this.comptProg++
              this.dansmulti = false
              setTimeout(this.animeRepere.bind(this), this.tempo)
              return
            }
            if (this.Programme[this.comptProg].cont[this.jensuioumulti].noco === true) {
              this.jensuioumulti++
              setTimeout(this.animeRepere.bind(this), this.tempo)
              return
            }
            switch (this.Programme[this.comptProg].cont[this.jensuioumulti].type) {
              case 'point':
                this.jensuisou = 'pointDeb'
                this.afaire = this.Programme[this.comptProg].cont[this.jensuioumulti]
                break
              case 'segment':
                this.jensuisou = 'segment'
                this.afaire = this.Programme[this.comptProg].cont[this.jensuioumulti]
                break
              case 'cercle':
                this.jensuisou = 'cercle'
                this.afaire = this.Programme[this.comptProg].cont[this.jensuioumulti]
                break
              case 'af':
                this.afaire = j3pClone(this.Programme[this.comptProg].cont[this.jensuioumulti])
                this.jensuisou = 'aF0'
                this.cptaf = 0
                break
              case 'droite':
                this.jensuisou = 'droite'
                this.afaire = this.Programme[this.comptProg].cont[this.jensuioumulti]
                break
              case 'demi-droite':
                this.jensuisou = 'demidroite'
                this.afaire = this.Programme[this.comptProg].cont[this.jensuioumulti]
                break
              case 'efface':
                this.jensuisou = 'efface1'
                this.afaire = j3pClone(this.Programme[this.comptProg].cont[this.jensuioumulti].ki)
                break
            }
            if (this.Programme[this.comptProg].cont[this.jensuioumulti].text !== '') {
              this.after = this.jensuisou
              this.jensuisou = 'previens'
            }
            break
          case 'efface':
            this.jensuisou = 'efface1'
            this.afaire = j3pClone(this.Programme[this.comptProg].ki)
            break
        }
        setTimeout(this.animeRepere.bind(this), this.tempo)
        break
      case 'efface1':
        this.afficheIm('gomme', true)
        if (this.afaire.length === 0) {
          this.afficheIm('gomme', false)
          if (this.dansmulti) {
            this.jensuioumulti++
          } else {
            this.comptProg++
          }
          this.jensuisou = 'prog'
        } else {
          this.aEfTruc = this.afaire.pop()
          this.jensuisou = 'efface2'
        }
        setTimeout(this.animeRepere.bind(this), this.tempodeb)
        break
      case 'efface2': {
        const len = this.recupe(this.aEfTruc.nom)
        switch (this.aEfTruc.type) {
          case 'segment':
            // cherche dans segment
            for (i = 0; i < this.segCo.length; i++) {
              if (!this.segCo[i].viz) continue
              if ((this.segCo[i].nom === len) || (this.segCo[i].nom === '[' + len[2] + len[1] + ']')) {
                this.kiclignote = this.segCo[i].html.ligne
                dx = i
                nb = this.ConsSegnb
                this.segCo[i].viz = false
              }
            }
            break
          case 'droite':
            // cherche dans segment
            oktrouv = false
            if (this.aEfTruc.spe !== undefined) {
              this.dervi2 = true
              this.jensuisou = 'ParaRE0'
              this.LeptAretenir = j3pClone(this.PtArrive)
              this.PtArrive = this.getPointPosition2(this.svgId, this.htmlcoPoint(this.recupe(this.aEfTruc.pass)))
              this.aEfTruc.pass = this.afaire1.ki.pass
              this.aEfTruc.nom = this.afaire1.ki.nom
              setTimeout(this.animeRepere.bind(this), this.tempo)
              return
            }

            for (i = 0; i < this.dteco.length; i++) {
              if (!this.dteco[i].viz) continue
              if ((this.dteco[i].nom === len) || (this.dteco[i].nom === '(' + len[2] + len[1] + ')')) {
                this.kiclignote = this.dteco[i].html.ligne
                nb = this.ConsDtenb
                oktrouv = true
                dx = i
                this.dteco[i].viz = false
                break
              }
            }
            if (!oktrouv) {
              for (i = 0; i < this.segCo.length; i++) {
                if (!this.segCo[i].viz) continue
                if ((this.segCo[i].nom === len) || (this.segCo[i].nom === '(' + len[2] + len[1] + ')')) {
                  this.kiclignote = this.segCo[i].html.ligne
                  dx = i
                  nb = this.ConsSegnb
                  this.segCo[i].viz = false
                  break
                }
              }
            }
            if (!oktrouv) {
              for (i = 0; i < this.demiDco.length; i++) {
                if (!this.demiDco[i].viz) continue
                if ((this.demiDco[i].nom === len) || (this.demiDco[i].nom === '(' + len[2] + len[1] + ')')) {
                  this.kiclignote = this.demiDco[i].html.ligne
                  dx = i
                  nb = this.ConsDemiDnb
                  this.demiDco[i].viz = false
                  break
                }
              }
            }
            break
          case 'demi-droite':
            oktrouv = false
            for (i = 0; i < this.demiDco.length; i++) {
              if (!this.demiDco[i].viz) continue
              if ((this.demiDco[i].nom === len) || (this.demiDco[i].nom === '[' + len[2] + len[1] + ')')) {
                this.kiclignote = this.demiDco[i].html.ligne
                oktrouv = true
                fautagrand = false
                dx = i
                nb = this.ConsDemiDnb
                this.demiDco[i].viz = false
                break
              }
              if (len.length === 4) {
                pt1 = this.getPointPosition2(this.svgId, this.demiDco[i].html.point1)
                pt2 = this.getPointPosition2(this.svgId, this.demiDco[i].html.point2)
                pt3 = this.getPointPosition2(this.svgId, this.htmlcoPoint(len[1]))
                pt4 = this.getPointPosition2(this.svgId, this.htmlcoPoint(len[2]))
                if (this.dist(pt3, pt1) <= 0.1 * this.unite && this.distPtDemiDtebis(pt1, pt4, pt2) <= 0.1 * this.unite) {
                  this.kiclignote = this.demiDco[i].html.ligne
                  oktrouv = true
                  fautagrand = false
                  dx = i
                  nb = this.ConsDtenb
                  this.demiDco[i].viz = false
                  break
                }
              }
            }
            if (!oktrouv) {
              for (i = 0; i < this.segCo.length; i++) {
                if (!this.segCo[i].viz) continue
                if ((this.segCo[i].nom === len) || (this.segCo[i].nom === '[' + len[2] + len[1] + ')')) {
                  this.kiclignote = this.segCo[i].html.ligne
                  dx = i
                  nb = this.ConsSegnb
                  this.segCo[i].viz = false
                  break
                }
              }
            }
            break
          case 'cercle':
            for (i = 0; i < this.arcco.length; i++) {
              if (!this.arcco[i].viz) continue
              if (this.arcco[i].nom === len) {
                this.kiclignote = this.arcco[i].html.arc1
                dx = i
                nb = this.ConsArcnb
                this.arcco[i].viz = false
              }
            }
            break
          case 'point':
            // cherche dans segment
            for (i = 0; i < this.ptCo.length; i++) {
              if (!this.ptCo[i].viz) continue
              if ((this.ptCo[i].nom === len)) {
                this.kiclignote = this.ptCo[i].html.point
                dx = i
                nb = this.ConsSegnb
                this.ptCo[i].viz = false
              }
            }
            break
        }
        this.fautagrand = false
        this.numclignote = 8
        this.jensuisou = 'efface3'
        this.faitClignote()
      }
        break
      case 'efface3':
        this.jensuisou = 'efface1'
        this.setVisible2(this.svgId, this.kiclignote, false)
        setTimeout(this.animeRepere.bind(this), this.tempodeb)
        break
      case 'droite': {
        this.afaire1 = j3pClone(this.afaire)
        this.afaire1.pass = this.recupe(this.afaire1.pass)
        pt1 = this.getPointPosition2(this.svgId, this.htmlcoPoint(this.afaire1.pass))
        switch (this.afaire1.condd) {
          case 'parallèle': {
            let cont1 = this.retrouveTruc(this.recupe(this.afaire1.odt))
            let lesPoints = this.retrouvePointsDroite(this.recupe(this.afaire1.odt))
            if (cont1 === undefined) {
              // ca je le virerai plus tard, c'est dans une autre branche
              const p1 = this.retrouvePoint(this.recupe(this.afaire1.odt)[1])
              const p2 = this.retrouvePoint(this.recupe(this.afaire1.odt)[2])
              const tatag = this.newTag()
              this.mtgAppLecteur.addLineAB({
                a: p1,
                b: p2,
                tag: tatag,
                hidden: true
              })
              cont1 = { type: 'droite', tab: [tatag] }
              lesPoints = { p1, p2 }
            }
            lesPoints.c1 = this.getPointPosition2(this.svgId, lesPoints.p1)
            lesPoints.c2 = this.getPointPosition2(this.svgId, lesPoints.p2)
            const lapProx = this.retrouveTag(this.recupe(this.afaire1.pass))[0]
            const unTagperp = this.newTag()
            const unTagperp2 = this.newTag()
            const unTagpara = this.newTag()
            const unTagcercl = this.newTag()
            const unTagpoint = this.newTag()
            const unTagpoint2 = this.newTag()
            const unTagpointInt = this.newTag()
            this.mtgAppLecteur.addLinePerp({
              a: lapProx,
              d: cont1.tab[0],
              tag: unTagperp,
              hidden: true
            })
            this.mtgAppLecteur.addLinePerp({
              a: lesPoints.p1,
              d: unTagperp,
              tag: unTagperp2,
              hidden: true
            })
            this.mtgAppLecteur.addIntLineLine({
              d2: unTagperp,
              d: unTagperp2,
              tag: unTagpointInt,
              name: unTagpointInt,
              hidden: true
            })
            this.mtgAppLecteur.addLinePar({
              a: lapProx,
              d: cont1.tab[0],
              tag: unTagpara,
              hidden: true
            })
            this.mtgAppLecteur.addCircleOr({
              o: unTagpointInt,
              r: 0.1,
              tag: unTagcercl,
              hidden: true
            })
            this.mtgAppLecteur.addIntLineCircle({
              d: unTagperp,
              c: unTagcercl,
              name: unTagpoint,
              tag: unTagpoint,
              name2: unTagpoint2,
              tag2: unTagpoint2,
              hidden: true
            })
            const cooWhere = this.getPointPosition2(this.svgId, unTagpointInt)
            const coo1 = this.getPointPosition2(this.svgId, unTagpoint)
            const coo2 = this.getPointPosition2(this.svgId, unTagpoint2)
            this.afaire1.ang = Math.atan2(coo1.x - coo2.x, coo2.y - coo1.y) + Math.PI / 2
            this.bary = { x: (pt1.x + cooWhere.x) / 2, y: (pt1.y + cooWhere.y) / 2 }
            this.PtArrive = j3pClone(cooWhere)
            this.afaire1.oupla = { x: 0, y: 0 }
            if (this.ds.Equerre === true || this.ds.Equerre === 'true') {
              this.jensuisou = 'ParaRE0'
              this.tagPerpAIde = this.newTag()
              this.mtgAppLecteur.addLineAB({
                lineStyle: 'dashdash',
                a: lesPoints.p1,
                b: lesPoints.p2,
                tag: this.tagPerpAIde
              })
            } else {
              this.jensuisou = 'ComParaRE0'
              setTimeout(this.animeRepere.bind(this), this.tempo)
              return
            }
          }
            break
          case 'perpendiculaire': {
            let cont1 = this.retrouveTruc(this.recupe(this.afaire1.odt))
            let lesPoints = this.retrouvePointsDroite(this.recupe(this.afaire1.odt))
            if (cont1 === undefined) {
              // ca je le virerai plus tard, c'est dans une autre branche
              const p1 = this.retrouvePoint(this.recupe(this.afaire1.odt)[1])
              const p2 = this.retrouvePoint(this.recupe(this.afaire1.odt)[2])
              const tatag = this.newTag()
              this.mtgAppLecteur.addLineAB({
                a: p1,
                b: p2,
                tag: tatag,
                hidden: true
              })
              cont1 = { type: 'droite', tab: [tatag] }
              lesPoints = { p1, p2 }
            }
            lesPoints.c1 = this.getPointPosition2(this.svgId, lesPoints.p1)
            lesPoints.c2 = this.getPointPosition2(this.svgId, lesPoints.p2)
            const lapProx = this.retrouveTag(this.recupe(this.afaire1.pass))[0]
            const unTagperp = this.newTag()
            const unTagperp2 = this.newTag()
            const unTagcercl = this.newTag()
            const unTagpoint = this.newTag()
            const unTagpoint2 = this.newTag()
            const unTagpointInt = this.newTag()
            this.mtgAppLecteur.addLinePerp({
              a: lapProx,
              d: cont1.tab[0],
              tag: unTagperp,
              hidden: true
            })
            this.mtgAppLecteur.addLinePerp({
              a: lesPoints.p1,
              d: unTagperp,
              tag: unTagperp2,
              hidden: true
            })
            this.mtgAppLecteur.addIntLineLine({
              d2: unTagperp,
              d: unTagperp2,
              tag: unTagpointInt,
              name: unTagpointInt,
              hidden: true
            })
            this.mtgAppLecteur.addCircleOr({
              o: unTagpointInt,
              r: 0.1,
              tag: unTagcercl,
              hidden: true
            })
            this.mtgAppLecteur.addIntLineCircle({
              d: unTagperp,
              c: unTagcercl,
              name: unTagpoint,
              tag: unTagpoint,
              name2: unTagpoint2,
              tag2: unTagpoint2,
              hidden: true
            })
            const coo1 = this.getPointPosition2(this.svgId, unTagpoint)
            const coo2 = this.getPointPosition2(this.svgId, unTagpoint2)
            const cooWhere = this.getPointPosition2(this.svgId, unTagpointInt)
            if (!this.placeOkPoint(coo1, true)) {
              ch = j3pClone(coo2)
            } else {
              ch = j3pClone(coo1)
            }
            this.afaire1.ang = Math.atan2(pt1.x - ch.x, ch.y - pt1.y)
            this.bary = { x: (pt1.x + ch.x) / 2, y: (pt1.y + ch.y) / 2 }
            this.PtArrive = j3pClone(cooWhere)
            this.afaire1.oupla = j3pClone(cooWhere)

            // il faudra peut etre mdodifie ca
            this.afaire1.garde = { x: (pt1.x - ch.x) / 2, y: (pt1.y - ch.y) / 2 }
            const delta1 = {
              x: cooWhere.x + 124 * this.zoom * Math.cos(this.afaire1.ang - Math.PI / 4),
              y: cooWhere.y + 124 * this.zoom * Math.sin(this.afaire1.ang - Math.PI / 4)
            }
            const delta2 = {
              x: cooWhere.x + 124 * this.zoom * Math.cos((this.afaire1.ang + Math.PI) - Math.PI / 4),
              y: cooWhere.y + 124 * this.zoom * Math.sin((this.afaire1.ang + Math.PI) - Math.PI / 4)
            }
            this.fot = false
            if (this.dist(delta1, pt1) > this.dist(delta2, pt1)) {
              this.afaire1.ang += Math.PI
              this.fot = true
            }
            if (this.ds.Equerre === true || this.ds.Equerre === 'true') {
              this.jensuisou = 'PerPEq0'
              this.tagPerpAIde = this.newTag()
              this.mtgAppLecteur.addLineAB({
                lineStyle: 'dashdash',
                a: lesPoints.p1,
                b: lesPoints.p2,
                tag: this.tagPerpAIde
              })
            } else {
              this.jensuisou = 'ComPerp0'
              setTimeout(this.animeRepere.bind(this), this.tempo)
              return
            }
          }
            break
          default:
            this.afaire1.pass2 = this.recupe(this.afaire1.pass2)
            pt2 = this.getPointPosition2(this.svgId, this.htmlcoPoint(this.afaire1.pass2))
            this.bary = { x: (pt1.x + pt2.x) / 2, y: (pt1.y + pt2.y) / 2 }
            this.pt1 = pt1
            this.pt2 = pt2
            this.jensuisou = 'droite2'
        }
        this.jensuiouPlace = 'go'
        const fffkl = function () { this.placeBienComp(true) }
        setTimeout(fffkl.bind(this), this.tempo)
      }
        break
      case 'demidroite': {
        this.afaire1 = j3pClone(this.afaire)
        this.afaire1.pass = this.recupe(this.afaire1.pass)
        pt1 = this.getPointPosition2(this.svgId, this.htmlcoPoint(this.afaire1.pass))
        switch (this.afaire1.condd) {
          case 'ang':
            // det en fonction du nom 2 pt ou la droite
            kokp = this.recupe(this.afaire1.odt).replace(')', '').replace('(', '').replace('[', '').replace(']', '')
            if (('abcdefghijklmnopqrstuvwxyz0123456789'.includes(kokp[0])) || ('0123456789abcdefghijklmnopqrstuvwxyz'.includes(kokp[kokp.length - 1])) || (kokp.length === 1)) {
              // droite avec nom

            } else {
              // droite avec 2 points
              pt2 = this.getPointPosition2(this.svgId, this.htmlcoPoint(kokp[0]))
              pt3 = this.getPointPosition2(this.svgId, this.htmlcoPoint(kokp[1]))
              ch = Math.atan2(pt3.x - pt2.x, pt2.y - pt3.y)
              this.afaire1.odt1 = this.htmlcoPoint(kokp[0])
              this.afaire1.odt2 = this.htmlcoPoint(kokp[1])
              this.afaire1.ang = ch
              a = (pt2.y - pt3.y) / (pt2.x - pt3.x)
              b = pt2.y - a * pt2.x
              a2 = -1 / a
              b2 = pt1.y - a2 * pt1.x
              if (pt2.x === pt3.x) {
                ch = { y: pt1.y }
                ch.x = pt2.x
              } else if (pt2.y === pt3.y) {
                ch = { y: pt2.y }
                ch.x = pt1.x
              } else {
                ch = { x: (b2 - b) / (a - a2) }
                ch.y = a2 * ch.x + b2
              }
              this.bary = { x: (pt1.x + ch.x) / 2, y: (pt1.y + ch.y) / 2 }
            }
            this.PtArrive = j3pClone(pt1)
            this.afaire1.oupla = { x: ch.x - pt1.x, y: ch.y - pt1.y }
            if (this.ds.Equerre === true || this.ds.Equerre === 'true') {
              this.jensuisou = 'ParaRE0'
            } else {
              this.jensuisou = 'ComParaRE0'
              setTimeout(this.animeRepere.bind(this), this.tempo)
              return
            }
            break
          default:
            this.afaire1.pass2 = this.recupe(this.afaire1.pass2)
            pt2 = this.getPointPosition2(this.svgId, this.htmlcoPoint(this.afaire1.pass2))
            this.bary = { x: (pt1.x + pt2.x) / 2, y: (pt1.y + pt2.y) / 2 }
            this.pt1 = pt1
            this.pt2 = pt2
            this.jensuisou = 'demidroite2'
        }
        this.jensuiouPlace = 'go'
        const fffkl = function () { this.placeBienComp(true) }
        setTimeout(fffkl.bind(this), this.tempo)
      }
        break
      case 'droite2': {
        if (this.afaire1.exa) {
          this.regle2()
          this.afficheIm('regle', true)
        } else {
          this.regle()
          this.afficheIm('regle2', true)
        }
        this.cacheTourne()
        const angle = Math.atan2(this.pt1.x - this.pt2.x, this.pt2.y - this.pt1.y) + Math.PI / 2
        this.CooBase = {
          x: this.bary.x - 10 * this.unite * this.zoom * Math.cos(angle) + 0.6 * this.zoom * Math.cos((angle + Math.PI / 2)),
          y: this.bary.y - 10 * this.unite * this.zoom * Math.sin(angle) + 0.6 * this.zoom * Math.sin((angle + Math.PI / 2))
        }
        this.delta = {
          x: 219 * this.zoom * Math.cos(angle + 13 * Math.PI / 180),
          y: 219 * this.zoom * Math.sin(angle + 13 * Math.PI / 180)
        }
        this.coRx = this.CooBase.x + 50 * Math.cos((angle + Math.PI / 2))
        this.coRy = this.CooBase.y + 50 * Math.sin((angle + Math.PI / 2))
        this.mtgAppLecteur.giveFormula2(this.svgId, 'reglangleC', 'reglangle', false)
        this.mtgAppLecteur.updateFigure(this.svgId)
        this.jensuisou = 'droite3'
        setTimeout(this.animeRepere.bind(this), this.tempo)
        break
      }
      case 'demidroite2': {
        if (this.afaire1.exa) {
          this.regle2()
          this.afficheIm('regle', true)
        } else {
          this.regle()
          this.afficheIm('regle2', true)
        }
        this.cacheTourne()
        const angle = Math.atan2(this.pt1.x - this.pt2.x, this.pt2.y - this.pt1.y) + Math.PI / 2
        this.CooBase = {
          x: this.bary.x - 10 * this.unite * this.zoom * Math.cos(angle) + 0.6 * this.zoom * Math.cos((angle + Math.PI / 2)),
          y: this.bary.y - 10 * this.unite * this.zoom * Math.sin(angle) + 0.6 * this.zoom * Math.sin((angle + Math.PI / 2))
        }
        this.delta = {
          x: 219 * this.zoom * Math.cos(angle + 13 * Math.PI / 180),
          y: 219 * this.zoom * Math.sin(angle + 13 * Math.PI / 180)
        }
        this.coRx = this.CooBase.x + 50 * Math.cos((angle + Math.PI / 2))
        this.coRy = this.CooBase.y + 50 * Math.sin((angle + Math.PI / 2))
        this.jensuisou = 'demidroite3'
        setTimeout(this.animeRepere.bind(this), this.tempo)
        break
      }
      case 'droite3':
        dx = this.CooBase.x - this.coRx
        dy = this.CooBase.y - this.coRy
        if (Math.abs(dx) > Math.abs(dy)) {
          if (dx > 0) {
            this.coRx = this.coRx + 2
          } else {
            this.coRx = this.coRx - 2
          }
          if (dy > 0) {
            this.coRy = this.coRy + 2 * Math.abs(dy) / Math.abs(dx)
          } else {
            this.coRy = this.coRy - 2 * Math.abs(dy) / Math.abs(dx)
          }
        } else {
          if (dy > 0) {
            this.coRy = this.coRy + 2
          } else {
            this.coRy = this.coRy - 2
          }
          if (dx > 0) {
            this.coRx = this.coRx + 2 * Math.abs(dx) / Math.abs(dy)
          } else {
            this.coRx = this.coRx - 2 * Math.abs(dx) / Math.abs(dy)
          }
        }
        if ((Math.abs(dx) < 3 && Math.abs(dy) < 3)) {
          this.coRx = this.CooBase.x
          this.coRy = this.CooBase.y
          this.mtgAppLecteur.setPointPosition(this.svgId, this.idPtRegle.pt, this.coRx, this.coRy, false)
          this.mtgAppLecteur.setPointPosition(this.svgId, '#pttttt', this.coRx + this.delta.x, this.coRy + this.delta.y, false)
          this.mtgAppLecteur.updateFigure(this.svgId)
          this.jensuiouPlace = 'zo'
          this.jensuisou = 'droite4'
          setTimeout(this.animeRepere.bind(this), this.tempodeb)
          return
        }
        this.mtgAppLecteur.setPointPosition(this.svgId, this.idPtRegle.pt, this.coRx, this.coRy, false)
        this.mtgAppLecteur.setPointPosition(this.svgId, '#pttttt', this.coRx + this.delta.x, this.coRy + this.delta.y, false)
        this.mtgAppLecteur.giveFormula2(this.svgId, 'reglangleC', 'reglangle', false)
        this.mtgAppLecteur.updateFigure(this.svgId)
        setTimeout(this.animeRepere.bind(this), this.tempo)
        break
      case 'demidroite3':
        dx = this.CooBase.x - this.coRx
        dy = this.CooBase.y - this.coRy
        if (Math.abs(dx) > Math.abs(dy)) {
          if (dx > 0) {
            this.coRx = this.coRx + 2
          } else {
            this.coRx = this.coRx - 2
          }
          if (dy > 0) {
            this.coRy = this.coRy + 2 * Math.abs(dy) / Math.abs(dx)
          } else {
            this.coRy = this.coRy - 2 * Math.abs(dy) / Math.abs(dx)
          }
        } else {
          if (dy > 0) {
            this.coRy = this.coRy + 2
          } else {
            this.coRy = this.coRy - 2
          }
          if (dx > 0) {
            this.coRx = this.coRx + 2 * Math.abs(dx) / Math.abs(dy)
          } else {
            this.coRx = this.coRx - 2 * Math.abs(dx) / Math.abs(dy)
          }
        }
        if ((Math.abs(dx) < 3 && Math.abs(dy) < 3)) {
          this.coRx = this.CooBase.x
          this.coRy = this.CooBase.y
          this.mtgAppLecteur.setPointPosition(this.svgId, this.idPtRegle.pt, this.coRx, this.coRy, false)
          this.mtgAppLecteur.setPointPosition(this.svgId, '#pttttt', this.coRx + this.delta.x, this.coRy + this.delta.y, false)
          this.mtgAppLecteur.updateFigure(this.svgId)
          this.jensuiouPlace = 'zo'
          this.jensuisou = 'demidroite4'
          setTimeout(this.animeRepere.bind(this), this.tempodeb)
          return
        }
        this.mtgAppLecteur.setPointPosition(this.svgId, this.idPtRegle.pt, this.coRx, this.coRy, false)
        this.mtgAppLecteur.setPointPosition(this.svgId, '#pttttt', this.coRx + this.delta.x, this.coRy + this.delta.y, false)
        this.mtgAppLecteur.updateFigure(this.svgId)
        setTimeout(this.animeRepere.bind(this), this.tempo)
        break
      case 'droite4':
        if (this.afaire1.exa) {
          this.jensuisou = 'droite4bis'
          this.animeRepere()
          return
        }
        this.afficheIm('point2', true)
        this.setVisible2(this.svgId, this.idImCray, true, true)
        this.mtgAppLecteur.setPointPosition(this.svgId, this.idImCrayPt, this.lXcentre, this.lYcentre, true)
        this.setVisible2(this.svgId, '#laddregle', true, true)
        this.setVisible2(this.svgId, '#regtt', false, true)
        this.jensuisou = 'droite5'
        setTimeout(this.animeRepere.bind(this), this.tempodeb)
        break
      case 'droite4bis':
        this.afficheIm('point2', true)
        this.setVisible2(this.svgId, this.idImCray, true, true)
        this.ghjdfsy = this.getPointPosition2(this.svgId, '#regpt1')
        this.mtgAppLecteur.setPointPosition(this.svgId, this.idImCrayPt, this.ghjdfsy.x, this.ghjdfsy.y, true)
        this.setVisible2(this.svgId, '#laddregle', false, true)
        this.setVisible2(this.svgId, '#regtt', true, true)
        this.jensuisou = 'droite5bis'
        this.ww = 0
        this.newSeg()
        setTimeout(this.animeRepere.bind(this), this.tempodeb)
        break
      case 'demidroite4':
        this.afficheIm('point2', true)
        this.setVisible2(this.svgId, this.idImCray, true, true)
        if (this.afaire1.exa) {
          this.jensuisou = 'demidroite4bis'
          this.animeRepere()
          return
        }
        this.mtgAppLecteur.setPointPosition(this.svgId, this.idImCrayPt, this.lXcentre, this.lYcentre, true)
        this.pt2 = this.getPointPosition2(this.svgId, this.htmlcoPoint(this.recupe(this.afaire1.pass2)))
        this.pt1 = this.getPointPosition2(this.svgId, this.htmlcoPoint(this.recupe(this.afaire1.pass)))
        pt1 = j3pClone(this.pt1)
        pt3 = this.getPointPosition2(this.svgId, '#dpa')
        if (this.dist(pt1, pt3) > this.dist(this.pt2, pt3)) {
          pt1 = j3pClone(this.pt2)
          pt2 = j3pClone(this.pt1)
        }
        this.mtgAppLecteur.setPointPosition(this.svgId, '#dp1', this.pt2.x, this.pt2.y, true)
        this.mtgAppLecteur.setPointPosition(this.svgId, '#dp2', pt1.x, pt1.y, true)
        this.mtgAppLecteur.setPointPosition(this.svgId, '#dp3', pt1.x, pt1.y, true)
        this.mtgAppLecteur.setPointPosition(this.svgId, '#dp4', pt1.x, pt1.y, true)
        this.setVisible2(this.svgId, '#laddregle', false, true)
        this.setVisible2(this.svgId, '#regtt', false, true)
        this.allumelesbons(this.lXcentre, this.lYcentre)
        this.jensuisou = 'demidroite5'
        setTimeout(this.animeRepere.bind(this), this.tempodeb * 2)
        break
      case 'demidroite4bis':
        this.pt2 = this.getPointPosition2(this.svgId, this.htmlcoPoint(this.recupe(this.afaire1.pass2)))
        this.pt1 = this.getPointPosition2(this.svgId, this.htmlcoPoint(this.recupe(this.afaire1.pass)))
        this.mtgAppLecteur.setPointPosition(this.svgId, this.idImCrayPt, this.pt2.x, this.pt2.y, true)
        this.setVisible2(this.svgId, '#laddregle', false, true)
        this.setVisible2(this.svgId, '#regtt', true, true)
        this.CooBase = j3pClone(this.pt2)
        this.setVisible2(this.svgId, '#da', false, true)
        this.setVisible2(this.svgId, '#d1', false, true)
        this.setVisible2(this.svgId, '#d2', false, true)
        this.setVisible2(this.svgId, '#d3', false, true)
        this.setVisible2(this.svgId, '#d4', false, true)
        this.setVisible2(this.svgId, '#dz', false, true)
        this.newSeg()
        this.jensuisou = 'demidroite5bis'
        this.ww = 0
        this.ghjdfsy = this.getPointPosition2(this.svgId, '#regpt1')
        if (this.dist(this.ghjdfsy, this.pt1) > this.dist(this.ghjdfsy, this.pt2)) {
          this.ghjdfsy = { x: 2 * this.ghjdfsy.x - this.lXcentre, y: 2 * this.ghjdfsy.y - this.lYcentre }
        }
        setTimeout(this.animeRepere.bind(this), this.tempodeb)
        break
      case 'demidroite5':
        this.newDDte2(this.pt2.x, this.pt2.y, this.pt1.x, this.pt1.y, '[' + this.recupe(this.afaire1.pass) + this.recupe(this.afaire1.pass2) + ')', true)
        pt1 = this.getPointPosition2(this.svgId, '#dpa')
        pt1 = { x: (pt1.x + this.pt1.x) / 2, y: (pt1.y + this.pt1.y) / 2 }
        this.mtgAppLecteur.setPointPosition(this.svgId, this.idImCrayPt, pt1.x, pt1.y, true)
        this.allumelesbons(pt1.x, pt1.y)
        this.jensuisou = 'demidroite6'
        setTimeout(this.animeRepere.bind(this), this.tempodeb)
        break
      case 'demidroite5bis':
        this.ww++
        this.mtgAppLecteur.setPointPosition(this.svgId, this.idImCrayPt, -(16 - this.ww) * (this.ghjdfsy.x - this.pt2.x) / 16 + this.ghjdfsy.x, -(16 - this.ww) * (this.ghjdfsy.y - this.pt2.y) / 16 + this.ghjdfsy.y, true)
        this.mtgAppLecteur.setPointPosition(this.svgId, this.segCo[this.segCo.length - 1].html.point1, -(16 - this.ww) * (this.ghjdfsy.x - this.pt2.x) / 16 + this.ghjdfsy.x, -(16 - this.ww) * (this.ghjdfsy.y - this.pt2.y) / 16 + this.ghjdfsy.y, true)
        if (this.ww === 16) {
          this.jensuisou = 'demidroite6bis'
          setTimeout(this.animeRepere.bind(this), this.tempo)
          return
        }
        setTimeout(this.animeRepere.bind(this), this.tempo)
        break
      case 'demidroite6':
        this.afficheIm('point2', false)
        this.afficheIm('regle2', false)
        this.regle()
        this.mtgAppLecteur.setPointPosition(this.svgId, this.idImCrayPt, 900, 500, true)
        if (this.after) {
          this.jensuisou = this.after
          this.after = this.after2
          this.afaire1 = j3pClone(this.afaireAcons)
        } else {
          if (this.dansmulti) {
            this.jensuioumulti++
          } else {
            this.comptProg++
          }
          this.jensuisou = 'prog'
        }
        this.setVisible2(this.svgId, '#da', false, true)
        this.setVisible2(this.svgId, '#d1', false, true)
        this.setVisible2(this.svgId, '#d2', false, true)
        this.setVisible2(this.svgId, '#d3', false, true)
        this.setVisible2(this.svgId, '#d4', false, true)
        this.setVisible2(this.svgId, '#dz', false, true)
        setTimeout(this.animeRepere.bind(this), this.tempodeb)
        break
      case 'droite5': {
        this.afficheIm('point2', false)
        this.afficheIm('regle2', false)
        this.regle()
        this.newDte2(this.bary.x, this.bary.y, this.bary.x + (this.pt1.x - this.pt2.x), this.bary.y + (this.pt1.y - this.pt2.y), this.afaire1.nom)
        const ang = Math.atan2(this.pt1.x - this.pt2.x, this.pt2.y - this.pt1.y)
        this.mtgAppLecteur.setPointPosition(this.svgId, this.idImCrayPt, 5000, 5000, true)
        if (this.afaire1.nom !== '' && !(this.afaire1.cachenom === true) && (this.afaire1.need !== false)) {
          pt1 = this.getPointPosition2(this.svgId, this.htmlcoPoint(this.afaire1.pass))
          this.newNom2(this.CooBase.x + 40 * Math.cos(ang + Math.PI / 3)
            , this.CooBase.y + 40 * Math.sin(ang + Math.PI / 3)
            , this.afaire1.nom)
        }
        if (this.dansmulti) {
          this.jensuioumulti++
        } else {
          this.comptProg++
        }
        this.jensuisou = 'prog'
        setTimeout(this.animeRepere.bind(this), this.tempodeb)
        break
      }
      case 'droite5bis':
        this.ww++
        this.mtgAppLecteur.setPointPosition(this.svgId, this.idImCrayPt, this.ghjdfsy.x + (this.lXcentre - this.ghjdfsy.x) / 8 * this.ww, this.ghjdfsy.y + (this.lYcentre - this.ghjdfsy.y) / 8 * this.ww, true)
        this.mtgAppLecteur.setPointPosition(this.svgId, this.segCo[this.segCo.length - 1].html.point2, this.ghjdfsy.x + (this.lXcentre - this.ghjdfsy.x) / 8 * this.ww, this.ghjdfsy.y + (this.lYcentre - this.ghjdfsy.y) / 8 * this.ww, true)
        if (this.ww === 16) {
          this.jensuisou = 'droite6bis'
          setTimeout(this.animeRepere.bind(this), this.tempodeb)
          return
        }
        setTimeout(this.animeRepere.bind(this), this.tempo)
        break
      case 'droite6bis':
        this.afficheIm('point2', false)
        this.afficheIm('regle', false)
        this.regle2()
        if (this.dansmulti) {
          this.jensuioumulti++
        } else {
          this.comptProg++
        }
        this.jensuisou = 'prog'
        setTimeout(this.animeRepere.bind(this), this.tempodeb)
        break
      case 'demidroite6bis':
        this.afficheIm('point2', false)
        this.afficheIm('regle', false)
        this.regle2()
        if (this.dansmulti) {
          this.jensuioumulti++
        } else {
          this.comptProg++
        }
        this.jensuisou = 'prog'
        setTimeout(this.animeRepere.bind(this), this.tempodeb)
        break
      case 'ComPerp0': {
        this.afficheIm('compas', true)
        this.compas()
        this.cacheTourne()
        this.CooBase = this.getPointPosition2(this.svgId, this.htmlcoPoint(this.afaire1.pass))
        this.coRx = this.CooBase.x + 50
        this.coRy = this.CooBase.y + 50
        pt2 = this.getPointPosition2(this.svgId, this.afaire1.odt1)
        pt3 = this.getPointPosition2(this.svgId, this.afaire1.odt2)
        dista = this.distPtDtebis(pt2, this.CooBase, pt3)
        a = (pt2.y - pt3.y) / (pt2.x - pt3.x)
        b = pt2.y - a * pt2.x
        let sol = this.getInstersectingPoints(a, b, this.CooBase.x, this.CooBase.y, dista * this.unite + 50)
        this.afaire1.garde = j3pClone(sol[1])
        this.mtgAppLecteur.setPointPosition(this.svgId, '#compPLA2', this.CooBase.x, this.CooBase.y, true)
        this.mtgAppLecteur.setPointPosition(this.svgId, '#compPLA1', sol[0].x, sol[0].y, true)
        sol = this.getPointPosition2(this.svgId, '#compPLA3')

        this.delta = { x: sol.x - this.CooBase.x, y: sol.y - this.CooBase.y }
        this.mtgAppLecteur.setPointPosition(this.svgId, '#' + this.idCompPt.pointe, this.coRx, this.coRy, false)
        this.mtgAppLecteur.setPointPosition(this.svgId, '#' + this.idCompPt.geremine, this.coRx + this.delta.x, this.coRy + this.delta.y, false)

        this.mtgAppLecteur.updateFigure(this.svgId)
        this.jensuisou = 'ComPerp1'
        setTimeout(this.animeRepere.bind(this), this.tempo)
        break
      }
      case 'ComPerp1':
        dx = this.CooBase.x - this.coRx
        dy = this.CooBase.y - this.coRy
        if (Math.abs(dx) > Math.abs(dy)) {
          if (dx > 0) {
            this.coRx = this.coRx + 4
          } else {
            this.coRx = this.coRx - 2
          }
          if (dy > 0) {
            this.coRy = this.coRy + 4 * Math.abs(dy) / Math.abs(dx)
          } else {
            this.coRy = this.coRy - 4 * Math.abs(dy) / Math.abs(dx)
          }
        } else {
          if (dy > 0) {
            this.coRy = this.coRy + 4
          } else {
            this.coRy = this.coRy - 4
          }
          if (dx > 0) {
            this.coRx = this.coRx + 4 * Math.abs(dx) / Math.abs(dy)
          } else {
            this.coRx = this.coRx - 4 * Math.abs(dx) / Math.abs(dy)
          }
        }
        if ((Math.abs(dx) < 4 && Math.abs(dy) < 4)) {
          this.coRx = this.CooBase.x
          this.coRy = this.CooBase.y
          this.mtgAppLecteur.setPointPosition(this.svgId, '#' + this.idCompPt.pointe, this.coRx, this.coRy, false)
          this.mtgAppLecteur.setPointPosition(this.svgId, '#' + this.idCompPt.geremine, this.coRx + this.delta.x, this.coRy + this.delta.y, false)
          this.mtgAppLecteur.updateFigure(this.svgId)
          this.jensuiouPlace = 'zo'
          this.jensuisou = 'ComPerp2'
          this.CooBase = j3pClone(this.pt2)
          this.pt2 = { x: this.coRx, y: this.coRy }
          this.coRx = this.coRx + this.delta.x
          this.coRy = this.coRy + this.delta.y
          setTimeout(this.animeRepere.bind(this), this.tempo)
          return
        }
        this.mtgAppLecteur.setPointPosition(this.svgId, '#' + this.idCompPt.pointe, this.coRx, this.coRy, false)
        this.mtgAppLecteur.setPointPosition(this.svgId, '#' + this.idCompPt.geremine, this.coRx + this.delta.x, this.coRy + this.delta.y, false)
        this.mtgAppLecteur.updateFigure(this.svgId)
        setTimeout(this.animeRepere.bind(this), this.tempo)
        break
      case 'ComPerp2':
        this.compas(true)
        for (let i = 0; i < this.compaTra.length; i++) {
          this.setVisible2(this.svgId, this.compaTra[i], true, true)
        }
        pt1 = this.getPointPosition2(this.svgId, this.idCompPt.mine)
        this.mtgAppLecteur.setPointPosition(this.svgId, '#compcoupt', pt1.x, pt1.y, true)
        this.newArc3({ x: this.coRx, y: this.coRy }, pt1, this.afaire1.com)
        this.jensuisou = 'ComPerp3'
        this.llanglebase = Math.atan2(pt1.x - this.lXcentre, pt1.y - this.lYcentre) * 180 / Math.PI - 90
        this.llangle = this.llanglebase
        this.dista = this.dist({ x: this.coRx, y: this.coRy }, { x: pt1.x, y: pt1.y })
        this.bary = { x: this.coRx, y: this.coRy }
        this.placeBienComp()
        break
      case 'ComPerp3':
        this.llangle = this.llangle + 3
        if (this.llangle > this.llanglebase + 358) {
          this.llangle = this.llanglebase + 359.999
        }
        this.mtgAppLecteur.setPointPosition(this.svgId, this.arcco[this.arcco.length - 1].html.p2, this.coRx + this.dista * Math.cos(this.llangle * Math.PI / 180), this.coRy - this.dista * Math.sin(this.llangle * Math.PI / 180), true)
        this.mtgAppLecteur.setPointPosition(this.svgId, '#compcoupt', this.coRx + this.dista * Math.cos(this.llangle * Math.PI / 180), this.coRy - this.dista * Math.sin(this.llangle * Math.PI / 180), true)
        if (this.llangle === this.llanglebase + 359.999) {
          this.jensuisou = 'ComPerp4'
          setTimeout(this.animeRepere.bind(this), this.tempodeb)
          return
        }
        setTimeout(this.animeRepere.bind(this), this.tempo)
        break
      case 'PerPEq0':
        // fait aparaitre équerre
        if (this.afaire1.text !== undefined) {
          this.affBulle(this.recupe(this.afaire1.text))
        }
        this.afficheIm('equerre', true)
        this.equerre()
        this.cacheTourne()
        if (this.dervi === undefined) {
          this.CooBase = j3pClone(this.PtArrive)
        } else {
          this.CooBase = j3pClone(this.derviGarde)
        }
        this.coRx = this.CooBase.x + 50
        this.coRy = this.CooBase.y + 50
        this.mtgAppLecteur.setPointPosition(this.svgId, this.idPtEquerre.pt, this.coRx, this.coRy, false)
        this.delta = {
          x: 124 * this.zoom * Math.cos(this.afaire1.ang - Math.PI / 4),
          y: 124 * this.zoom * Math.sin(this.afaire1.ang - Math.PI / 4)
        }
        this.mtgAppLecteur.setPointPosition(this.svgId, '#pteqma0', this.coRx + this.delta.x, this.coRy + this.delta.y, false)
        this.mtgAppLecteur.updateFigure(this.svgId)
        this.jensuisou = 'PerPEq1'
        setTimeout(this.animeRepere.bind(this), this.tempo)
        break
      case 'PerPEq1':
        dx = this.CooBase.x - this.coRx
        dy = this.CooBase.y - this.coRy
        if (Math.abs(dx) > Math.abs(dy)) {
          if (dx > 0) {
            this.coRx = this.coRx + 2
          } else {
            this.coRx = this.coRx - 2
          }
          if (dy > 0) {
            this.coRy = this.coRy + 2 * Math.abs(dy) / Math.abs(dx)
          } else {
            this.coRy = this.coRy - 2 * Math.abs(dy) / Math.abs(dx)
          }
        } else {
          if (dy > 0) {
            this.coRy = this.coRy + 2
          } else {
            this.coRy = this.coRy - 2
          }
          if (dx > 0) {
            this.coRx = this.coRx + 2 * Math.abs(dx) / Math.abs(dy)
          } else {
            this.coRx = this.coRx - 2 * Math.abs(dx) / Math.abs(dy)
          }
        }

        if ((Math.abs(dx) < 3 && Math.abs(dy) < 3)) {
          this.coRx = this.CooBase.x
          this.coRy = this.CooBase.y
          this.jensuisou = 'PerPEq2'
          this.mtgAppLecteur.setPointPosition(this.svgId, this.idPtEquerre.pt, this.coRx, this.coRy, false)
          this.mtgAppLecteur.setPointPosition(this.svgId, '#pteqma0', this.coRx + this.delta.x, this.coRy + this.delta.y, false)
          this.mtgAppLecteur.updateFigure(this.svgId)
          setTimeout(this.animeRepere.bind(this), this.tempodeb)
          return
        }
        this.mtgAppLecteur.setPointPosition(this.svgId, this.idPtEquerre.pt, this.coRx, this.coRy, false)
        this.mtgAppLecteur.setPointPosition(this.svgId, '#pteqma0', this.coRx + this.delta.x, this.coRy + this.delta.y, false)
        this.mtgAppLecteur.updateFigure(this.svgId)
        setTimeout(this.animeRepere.bind(this), this.tempo)
        break
      case 'PerPEq2': {
        this.afficheIm('point2', true)
        this.setVisible2(this.svgId, this.idImCray, true, true)
        if (this.fot) {
          this.mtgAppLecteur.setPointPosition(this.svgId, this.idImCrayPt, this.CooBase.x + 20 * this.zoom * Math.cos(this.afaire1.ang), this.CooBase.y + 20 * this.zoom * Math.sin(this.afaire1.ang), true)
        } else {
          this.mtgAppLecteur.setPointPosition(this.svgId, this.idImCrayPt, this.CooBase.x + 20 * this.zoom * Math.cos(this.afaire1.ang - Math.PI / 2), this.CooBase.y + 20 * this.zoom * Math.sin(this.afaire1.ang - Math.PI / 2), true)
        }
        this.jensuisou = 'PerPEq3'
        let hjgjk = this.afaire1.nom
        if (this.afaire1.retnom !== undefined) {
          hjgjk = this.afaire1.retnom
        }
        this.newDte2(this.CooBase.x, this.CooBase.y, this.CooBase.x + this.afaire1.garde.x, this.CooBase.y + this.afaire1.garde.y, hjgjk)
        setTimeout(this.animeRepere.bind(this), this.tempodeb)
        break
      }
      case 'PerPEq3':
        this.afficheIm('point2', false)
        this.mtgAppLecteur.setPointPosition(this.svgId, this.idImCrayPt, 1000, 1000, true)
        this.setVisible2(this.svgId, this.idImCray, false, true)
        this.equerre()
        this.afficheIm('equerre', false)
        if (this.afaire1.nom !== '' && !(this.afaire1.cachenom === true) && this.afaire1.need !== false && this.dervi === undefined) {
          pt1 = this.getPointPosition2(this.svgId, this.htmlcoPoint(this.afaire1.pass))

          this.newNom2(this.CooBase.x + 40 * Math.cos(this.afaire1.ang + Math.PI / 3)
            , this.CooBase.y + 40 * Math.sin(this.afaire1.ang + Math.PI / 3)
            , this.afaire1.nom)
        }
        if (this.dervi === undefined) {
          if (this.dansmulti) {
            this.jensuioumulti++
          } else {
            this.comptProg++
          }
          this.jensuisou = 'prog'
        } else {
          this.jensuisou = this.after
        }
        if (this.tagPerpAIde) {
          this.mtgAppLecteur.deleteElt({ elt: this.tagPerpAIde })
          this.tagPerpAIde = undefined
        }
        setTimeout(this.animeRepere.bind(this), this.tempodeb)
        break
      case 'previens': {
        tt = this.recupe(this.Programme[this.comptProg].cont[this.jensuioumulti].text)
        let contlong = -1
        for (let i = 0; i < this.Programme[this.comptProg].cont.length; i++) {
          if (this.Programme[this.comptProg].cont[i].cb) {
            contlong++
            tt = this.rempDans(contlong, String(this.Programme[this.comptProg].cont[i].cb).replace('.', ','), tt)
          }
          if (this.Programme[this.comptProg].cont[i].longueur) {
            contlong++
            tt = this.rempDans(contlong, this.Programme[this.comptProg].cont[i].longueur, tt)
          }
          if (this.Programme[this.comptProg].cont[i].long) {
            contlong++
            tt = this.rempDans(contlong, this.Programme[this.comptProg].cont[i].long, tt)
          }
          if (this.Programme[this.comptProg].cont[i].rayon) {
            contlong++
            tt = this.rempDans(contlong, this.Programme[this.comptProg].cont[i].rayon, tt)
          }
          if (this.Programme[this.comptProg].cont[i].cond) {
            if (Array.isArray(this.Programme[this.comptProg].cont[i].cond)) {
              if (this.Programme[this.comptProg].cont[i].cond.length > 0) {
                if (this.Programme[this.comptProg].cont[i].cond[0].cb) {
                  contlong++
                  tt = this.rempDans(contlong, String(this.Programme[this.comptProg].cont[i].cond[0].cb).replace('.', ','), tt)
                }
                if (this.Programme[this.comptProg].cont[i].cond[0].angle) {
                  contlong++
                  tt = this.rempDans(contlong, this.Programme[this.comptProg].cont[i].cond[0].angle, tt)
                }
                if (this.Programme[this.comptProg].cont[i].cond[0].long) {
                  contlong++
                  tt = this.rempDans(contlong, this.Programme[this.comptProg].cont[i].cond[0].long, tt)
                }
                if (this.Programme[this.comptProg].cont[i].cond[0].longueur) {
                  contlong++
                  tt = this.rempDans(contlong, this.Programme[this.comptProg].cont[i].cond[0].longueur, tt)
                }
              }
              if (this.Programme[this.comptProg].cont[i].cond.length > 1) {
                if (this.Programme[this.comptProg].cont[i].cond[1].cb) {
                  contlong++
                  tt = this.rempDans(contlong, String(this.Programme[this.comptProg].cont[i].cond[1].cb).replace('.', ','), tt)
                }
                if (this.Programme[this.comptProg].cont[i].cond[1].angle) {
                  contlong++
                  tt = this.rempDans(contlong, this.Programme[this.comptProg].cont[i].cond[1].angle, tt)
                }
                if (this.Programme[this.comptProg].cont[i].cond[1].long) {
                  contlong++
                  tt = this.rempDans(contlong, this.Programme[this.comptProg].cont[i].cond[1].long, tt)
                }
                if (this.Programme[this.comptProg].cont[i].cond[1].longueur) {
                  contlong++
                  tt = this.rempDans(contlong, this.Programme[this.comptProg].cont[i].cond[1].longueur, tt)
                }
              }
            }
          }
        }
        if (tt !== '') this.affBulle(tt)
        this.jensuisou = this.after
        this.after = this.after2
        setTimeout(this.animeRepere.bind(this), this.tempodeb)
      }
        break
      case 'previensco':
        tt = this.recupe(this.Programme[this.comptProg].textco)
        if (tt !== '') this.affBulle(tt)
        this.jensuisou = this.after
        setTimeout(this.animeRepere.bind(this), this.tempodeb)
        break
      case 'ComParaRE0':
        // faut choisr in point sur .odt
        // le plus pres possible de .pass

        // si il dépasse 14 cm en créer 1
        this.afaire1.odt = this.recupe(this.afaire1.odt)
        pt2 = this.getPointPosition2(this.svgId, this.htmlcoPoint(this.afaire1.odt[1]))
        pt3 = this.getPointPosition2(this.svgId, this.htmlcoPoint(this.afaire1.odt[2]))
        kokp = this.getPointPosition2(this.svgId, this.htmlcoPoint(this.afaire1.pass))
        ch = []
        for (let i = 0; i < this.ptCo.length; i++) {
          pt1 = this.getPointPosition2(this.svgId, this.ptCo[i].html.point)
          dx = this.distPtDtebis(pt2, pt1, pt3)
          dy = this.dist(kokp, pt1) / this.unite
          if ((dx < 0.000001) && (dy < 14)) ch.push({ nom: this.ptCo[i].nom, dist: dy })
        }
        ch = ch.sort(function (a, b) { return b.dist - a.dist })
        if (ch.length > 0) {
          this.afaire1.formule = ch[ch.length - 1].nom + this.afaire1.pass
        }
        this.monPointBase = ch[ch.length - 1].nom
        this.monangle = Math.atan2(pt2.x - pt3.x, pt2.y - pt3.y) + Math.PI / 2
        dista = this.getPointPosition2(this.svgId, this.htmlcoPoint(this.monPointBase))
        this.jensuisou = 'CompasPrendMesure00'
        this.after = 'ComParaRE1'
        setTimeout(this.animeRepere.bind(this), this.tempo)

        break
      case 'ComParaRE1':
        this.jensuisou = 'CompasPrendMesure3'
        this.after = 'ComParaRE2'

        this.CooBase = this.getPointPosition2(this.svgId, this.idCompPt.mine)
        pt1 = this.getPointPosition2(this.svgId, '#' + this.idCompPt.pointe)
        pt2 = this.getPointPosition2(this.svgId, '#' + this.idCompPt.geremine)
        this.coRx = pt1.x
        this.coRy = pt1.y
        this.delta = { x: pt2.x - this.coRx, y: pt2.y - this.coRy }
        pt2 = this.getPointPosition2(this.svgId, this.htmlcoPoint(this.monPointBase))
        pt3 = this.getPointPosition2(this.svgId, this.htmlcoPoint(this.afaire1.pass))
        this.monangle = Math.atan2(pt2.x - pt3.x, pt2.y - pt3.y) + Math.PI / 2

        setTimeout(this.animeRepere.bind(this), this.tempo)
        break
      case 'ComParaRE2':
        this.jensuisou = 'CompasPrendMesure3'
        this.after = 'ComParaRE3'

        this.CooBase = this.getPointPosition2(this.svgId, this.htmlcoPoint(this.afaire1.pass))
        pt1 = this.getPointPosition2(this.svgId, '#' + this.idCompPt.pointe)
        pt2 = this.getPointPosition2(this.svgId, '#' + this.idCompPt.geremine)
        this.coRx = pt1.x
        this.coRy = pt1.y
        this.delta = { x: pt2.x - this.coRx, y: pt2.y - this.coRy }
        pt2 = this.getPointPosition2(this.svgId, this.htmlcoPoint(this.afaire1.odt[1]))
        pt3 = this.getPointPosition2(this.svgId, this.htmlcoPoint(this.afaire1.odt[2]))
        this.monangle = Math.atan2(pt2.x - pt3.x, pt2.y - pt3.y) + Math.PI / 2

        setTimeout(this.animeRepere.bind(this), this.tempo)
        break
      case 'ComParaRE3':
        this.jensuisou = 'TraceDroite0'
        this.after = 'prog'
        this.afaire1.pass2 = this.getPointPosition2(this.svgId, this.idCompPt.mine)
        this.compas()
        this.afficheIm('compas', false)
        setTimeout(this.animeRepere.bind(this), this.tempo)
        break
      case 'TraceDroite0': {
        this.regle()
        this.cacheTourne()
        if (typeof this.afaire1.pass === 'string') {
          pt1 = this.getPointPosition2(this.svgId, this.htmlcoPoint(this.afaire1.pass))
        } else { pt1 = j3pClone(this.afaire1.pass) }
        if (typeof this.afaire1.pass2 === 'string') {
          pt2 = this.getPointPosition2(this.svgId, this.htmlcoPoint(this.afaire1.pass2))
        } else { pt2 = j3pClone(this.afaire1.pass2) }

        pt4 = { x: (pt1.x + pt2.x) / 2, y: (pt1.y + pt2.y) / 2 }
        if (pt1.x > pt2.x) {
          pt3 = j3pClone(pt2)
          pt2 = j3pClone(pt1)
          pt1 = j3pClone(pt3)
        }
        let angle = Math.atan2(pt2.x - pt1.x, pt1.y - pt2.y)
        this.monangle = angle * 180 / Math.PI + 13 - 90
        angle = Math.atan2(pt2.x - pt1.x, pt2.y - pt1.y) - Math.PI / 2
        this.afaire1.placecray = j3pClone(pt4)
        this.CooBase = { x: pt4.x - this.unite * 10 * Math.cos(angle), y: pt4.y + this.unite * 10 * Math.sin(angle) }

        this.coRx = this.CooBase.x + 70 * Math.cos(angle - Math.PI / 2)
        this.coRy = this.CooBase.y - 70 * Math.sin(angle - Math.PI / 2)
        // this.CooBase = j3pClone(pt4)
        this.mtgAppLecteur.setPointPosition(this.svgId, this.idPtRegle.pt, this.coRx, this.coRy, true)

        this.mtgAppLecteur.giveFormula2(this.svgId, 'reglangleC', this.monangle)
        this.mtgAppLecteur.updateFigure(this.svgId)
        this.afficheIm('regle2', true)
        this.jensuisou = 'TraceDroite1'
        setTimeout(this.animeRepere.bind(this), this.tempodeb)
        break
      }
      case 'TraceDroite1':
        dx = this.CooBase.x - this.coRx
        dy = this.CooBase.y - this.coRy
        if (Math.abs(dx) > Math.abs(dy)) {
          if (dx > 0) {
            this.coRx = this.coRx + 2
          } else {
            this.coRx = this.coRx - 2
          }
          if (dy > 0) {
            this.coRy = this.coRy + 2 * Math.abs(dy) / Math.abs(dx)
          } else {
            this.coRy = this.coRy - 2 * Math.abs(dy) / Math.abs(dx)
          }
        } else {
          if (dy > 0) {
            this.coRy = this.coRy + 2
          } else {
            this.coRy = this.coRy - 2
          }
          if (dx > 0) {
            this.coRx = this.coRx + 2 * Math.abs(dx) / Math.abs(dy)
          } else {
            this.coRx = this.coRx - 2 * Math.abs(dx) / Math.abs(dy)
          }
        }
        if (Math.abs(dx) < 3 || Math.abs(dy) < 3) {
          this.coRx = this.CooBase.x
          this.coRy = this.CooBase.y
          this.jensuisou = 'TraceDroite2'
          this.mtgAppLecteur.setPointPosition(this.svgId, this.idPtRegle.pt, this.coRx, this.coRy, true)
          setTimeout(this.animeRepere.bind(this), this.tempodeb)
          return
        }
        this.mtgAppLecteur.setPointPosition(this.svgId, this.idPtRegle.pt, this.coRx, this.coRy, true)
        setTimeout(this.animeRepere.bind(this), this.tempo)
        break
      case 'TraceDroite2':
        this.afficheIm('point2', true)
        this.setVisible2(this.svgId, this.idImCray, true, true)
        this.mtgAppLecteur.setPointPosition(this.svgId, this.idImCrayPt, this.afaire1.placecray.x, this.afaire1.placecray.y, true)
        this.setVisible2(this.svgId, '#laddregle', true, true)
        this.setVisible2(this.svgId, '#regtt', false, true)
        pt1 = this.getPointPosition2(this.svgId, this.htmlcoPoint(this.afaire1.pass))
        pt2 = j3pClone(this.afaire1.pass2)
        this.newDte2(pt1.x, pt1.y, pt2.x, pt2.y, this.afaire1.nom)
        this.jensuisou = 'TraceDroite3'
        setTimeout(this.animeRepere.bind(this), this.tempodeb)
        break
      case 'TraceDroite3':
        this.regle()
        this.afficheIm('point2', false)
        this.afficheIm('regle2', false)
        if (this.afaire1.nom !== '' && !(this.afaire1.cachenom === true) && this.afaire1.need !== false) {
          pt1 = this.getPointPosition2(this.svgId, this.htmlcoPoint(this.afaire1.pass))
          this.newNom2(this.CooBase.x + 40 * Math.cos(this.afaire1.ang + Math.PI / 3)
            , this.CooBase.y + 40 * Math.sin(this.afaire1.ang + Math.PI / 3)
            , this.afaire1.nom)
        }
        if (this.dansmulti) {
          this.jensuioumulti++
        } else {
          this.comptProg++
        }
        this.jensuisou = 'prog'
        setTimeout(this.animeRepere.bind(this), this.tempodeb)
        break
      case 'ParaRE0':
        // fait aparaitre équerre
        this.afficheIm('equerre', true)
        this.equerre()
        this.cacheTourne()
        this.CooBase = {
          x: this.PtArrive.x + 150 * this.zoom * Math.cos(this.afaire1.ang + Math.PI / 2) + 0.06 * this.unite * this.zoom * Math.cos(this.afaire1.ang),
          y: this.PtArrive.y + 150 * this.zoom * Math.sin(this.afaire1.ang + Math.PI / 2) + 0.06 * this.unite * this.zoom * Math.sin(this.afaire1.ang)
        }
        this.coRx = this.CooBase.x + 50
        this.coRy = this.CooBase.y + 50
        this.mtgAppLecteur.setPointPosition(this.svgId, this.idPtEquerre.pt, this.coRx, this.coRy, false)
        this.delta = {
          x: 124 * this.zoom * Math.cos(this.afaire1.ang - Math.PI / 4),
          y: 124 * this.zoom * Math.sin(this.afaire1.ang - Math.PI / 4)
        }
        this.mtgAppLecteur.setPointPosition(this.svgId, '#pteqma0', this.coRx + this.delta.x, this.coRy + this.delta.y, false)
        this.mtgAppLecteur.updateFigure(this.svgId)
        this.jensuisou = 'ParaRE1'
        setTimeout(this.animeRepere.bind(this), this.tempo)
        break
      case 'ParaRE1':
        dx = this.CooBase.x - this.coRx
        dy = this.CooBase.y - this.coRy
        if (Math.abs(dx) > Math.abs(dy)) {
          if (dx > 0) {
            this.coRx = this.coRx + 2
          } else {
            this.coRx = this.coRx - 2
          }
          if (dy > 0) {
            this.coRy = this.coRy + 2 * Math.abs(dy) / Math.abs(dx)
          } else {
            this.coRy = this.coRy - 2 * Math.abs(dy) / Math.abs(dx)
          }
        } else {
          if (dy > 0) {
            this.coRy = this.coRy + 2
          } else {
            this.coRy = this.coRy - 2
          }
          if (dx > 0) {
            this.coRx = this.coRx + 2 * Math.abs(dx) / Math.abs(dy)
          } else {
            this.coRx = this.coRx - 2 * Math.abs(dx) / Math.abs(dy)
          }
        }
        if ((Math.abs(dx) < 3 && Math.abs(dy) < 3)) {
          this.coRx = this.CooBase.x
          this.coRy = this.CooBase.y
          this.jensuisou = 'ParaRE2'
          this.mtgAppLecteur.setPointPosition(this.svgId, this.idPtEquerre.pt, this.coRx, this.coRy, false)
          this.mtgAppLecteur.setPointPosition(this.svgId, '#pteqma0', this.coRx + this.delta.x, this.coRy + this.delta.y, false)
          this.mtgAppLecteur.updateFigure(this.svgId)
          setTimeout(this.animeRepere.bind(this), this.tempodeb)
          return
        }
        this.mtgAppLecteur.setPointPosition(this.svgId, this.idPtEquerre.pt, this.coRx, this.coRy, false)
        this.mtgAppLecteur.setPointPosition(this.svgId, '#pteqma0', this.coRx + this.delta.x, this.coRy + this.delta.y, false)
        this.mtgAppLecteur.updateFigure(this.svgId)
        setTimeout(this.animeRepere.bind(this), this.tempo)
        break
      case 'ParaRE2':
        this.afficheIm('regle2', true)
        this.regle()
        this.cacheTourne()
        pt1 = this.getPointPosition2(this.svgId, this.htmlcoPoint(this.afaire1.pass))
        pt2 = this.getPointPosition2(this.svgId, this.idPtEquerre.pt)
        this.CooBase = {
          x: pt1.x + 150 * this.zoom * Math.cos(this.afaire1.ang + Math.PI / 2) + 0.06 * this.unite * this.zoom * Math.cos(this.afaire1.ang),
          y: pt1.y + 150 * this.zoom * Math.sin(this.afaire1.ang + Math.PI / 2) + 0.06 * this.unite * this.zoom * Math.sin(this.afaire1.ang)
        }
        this.CooBase = {
          x: (this.CooBase.x + 2 * pt2.x) / 3,
          y: (this.CooBase.y + 2 * pt2.y) / 3
        }
        this.coRx = this.CooBase.x + 50
        this.coRy = this.CooBase.y + 50
        this.delta = {
          x: 219 * this.zoom * Math.cos(this.afaire1.ang + 13 * Math.PI / 180),
          y: 219 * this.zoom * Math.sin(this.afaire1.ang + 13 * Math.PI / 180)
        }
        this.mtgAppLecteur.setPointPosition(this.svgId, this.idPtRegle.pt, this.coRx, this.coRy, false)
        this.mtgAppLecteur.setPointPosition(this.svgId, '#pttttt', this.coRx + this.delta.x, this.coRy + this.delta.y, false)
        this.mtgAppLecteur.giveFormula2(this.svgId, 'reglangleC', 'reglangle', false)
        this.mtgAppLecteur.updateFigure(this.svgId)
        this.jensuisou = 'ParaRE3'
        setTimeout(this.animeRepere.bind(this), this.tempo)
        break
      case 'ParaRE3':
        dx = this.CooBase.x - this.coRx
        dy = this.CooBase.y - this.coRy
        if (Math.abs(dx) > Math.abs(dy)) {
          if (dx > 0) {
            this.coRx = this.coRx + 2
          } else {
            this.coRx = this.coRx - 2
          }
          if (dy > 0) {
            this.coRy = this.coRy + 2 * Math.abs(dy) / Math.abs(dx)
          } else {
            this.coRy = this.coRy - 2 * Math.abs(dy) / Math.abs(dx)
          }
        } else {
          if (dy > 0) {
            this.coRy = this.coRy + 2
          } else {
            this.coRy = this.coRy - 2
          }
          if (dx > 0) {
            this.coRx = this.coRx + 2 * Math.abs(dx) / Math.abs(dy)
          } else {
            this.coRx = this.coRx - 2 * Math.abs(dx) / Math.abs(dy)
          }
        }
        if (Math.abs(dx) < 3 || Math.abs(dy) < 3) {
          this.coRx = this.CooBase.x
          this.coRy = this.CooBase.y
          this.jensuisou = 'ParaRE4'
          this.mtgAppLecteur.setPointPosition(this.svgId, this.idPtRegle.pt, this.coRx, this.coRy, false)
          this.mtgAppLecteur.setPointPosition(this.svgId, '#pttttt', this.coRx + this.delta.x, this.coRy + this.delta.y, false)
          this.mtgAppLecteur.updateFigure(this.svgId)
          pt1 = this.getPointPosition2(this.svgId, this.htmlcoPoint(this.afaire1.pass))
          this.CooBase = {
            x: pt1.x + 150 * this.zoom * Math.cos(this.afaire1.ang + Math.PI / 2) + 0.06 * this.unite * this.zoom * Math.cos(this.afaire1.ang),
            y: pt1.y + 150 * this.zoom * Math.sin(this.afaire1.ang + Math.PI / 2) + 0.06 * this.unite * this.zoom * Math.sin(this.afaire1.ang)
          }
          pt2 = this.getPointPosition2(this.svgId, this.idPtEquerre.pt)
          this.coRx = pt2.x
          this.coRy = pt2.y
          this.delta = {
            x: 124 * this.zoom * Math.cos(this.afaire1.ang - Math.PI / 4),
            y: 124 * this.zoom * Math.sin(this.afaire1.ang - Math.PI / 4)
          }
          setTimeout(this.animeRepere.bind(this), this.tempodeb)
          return
        }
        this.mtgAppLecteur.setPointPosition(this.svgId, this.idPtRegle.pt, this.coRx, this.coRy, false)
        this.mtgAppLecteur.setPointPosition(this.svgId, '#pttttt', this.coRx + this.delta.x, this.coRy + this.delta.y, false)
        this.mtgAppLecteur.updateFigure(this.svgId)
        setTimeout(this.animeRepere.bind(this), this.tempo)
        break
      case 'ParaRE4':
        dx = this.CooBase.x - this.coRx
        dy = this.CooBase.y - this.coRy
        if (Math.abs(dx) > Math.abs(dy)) {
          if (dx > 0) {
            this.coRx = this.coRx + 2
          } else {
            this.coRx = this.coRx - 2
          }
          if (dy > 0) {
            this.coRy = this.coRy + 2 * Math.abs(dy) / Math.abs(dx)
          } else {
            this.coRy = this.coRy - 2 * Math.abs(dy) / Math.abs(dx)
          }
        } else {
          if (dy > 0) {
            this.coRy = this.coRy + 2
          } else {
            this.coRy = this.coRy - 2
          }
          if (dx > 0) {
            this.coRx = this.coRx + 2 * Math.abs(dx) / Math.abs(dy)
          } else {
            this.coRx = this.coRx - 2 * Math.abs(dx) / Math.abs(dy)
          }
        }
        if (Math.abs(dx) < 3 && Math.abs(dy) < 3) {
          this.coRx = this.CooBase.x
          this.coRy = this.CooBase.y
          this.jensuisou = 'ParaRE5'
          this.mtgAppLecteur.setPointPosition(this.svgId, this.idPtEquerre.pt, this.coRx, this.coRy, false)
          this.mtgAppLecteur.setPointPosition(this.svgId, '#pteqma0', this.coRx + this.delta.x, this.coRy + this.delta.y, false)
          this.mtgAppLecteur.updateFigure(this.svgId)
          setTimeout(this.animeRepere.bind(this), this.tempodeb)
          return
        }
        this.mtgAppLecteur.setPointPosition(this.svgId, this.idPtEquerre.pt, this.coRx, this.coRy, false)
        this.mtgAppLecteur.setPointPosition(this.svgId, '#pteqma0', this.coRx + this.delta.x, this.coRy + this.delta.y, false)
        this.mtgAppLecteur.updateFigure(this.svgId)
        setTimeout(this.animeRepere.bind(this), this.tempo)
        break
      case 'ParaRE5':
        this.afficheIm('point2', true)
        pt1 = this.getPointPosition2(this.svgId, this.htmlcoPoint(this.afaire1.pass))
        this.mtgAppLecteur.setPointPosition(this.svgId, this.idImCrayPt, pt1.x, pt1.y, true)
        this.setVisible2(this.svgId, this.idImCray, true, true)
        this.jensuisou = 'ParaRE6'
        setTimeout(this.animeRepere.bind(this), this.tempodeb)
        break
      case 'ParaRE6':
        pt1 = this.getPointPosition2(this.svgId, this.htmlcoPoint(this.afaire1.pass))
        this.newDte2(pt1.x, pt1.y, pt1.x + 100 * this.zoom * Math.cos(this.afaire1.ang + Math.PI / 2), pt1.y + 100 * this.zoom * Math.sin(this.afaire1.ang + Math.PI / 2), this.afaire1.nom)
        this.jensuisou = 'ParaRE7'
        setTimeout(this.animeRepere.bind(this), this.tempodeb)
        break
      case 'ParaRE7':
        this.afficheIm('point2', false)
        this.afficheIm('regle2', false)
        this.afficheIm('equerre', false)
        this.regle()
        this.equerre()
        this.renvoieCrayon()
        this.setVisible2(this.svgId, this.idImCray, false, true)
        if (!this.dervi2 && !this.dervi3) {
          this.jensuisou = 'prog'
          if (this.dansmulti) {
            this.jensuioumulti++
          } else {
            this.comptProg++
          }
          if (this.afaire1.nom !== '' && !(this.afaire1.cachenom === true) && this.afaire1.need !== false) {
            pt1 = this.getPointPosition2(this.svgId, this.htmlcoPoint(this.afaire1.pass))
            this.newNom2(this.CooBase.x + 40 * Math.cos(this.afaire1.ang + Math.PI / 3)
              , this.CooBase.y + 40 * Math.sin(this.afaire1.ang + Math.PI / 3)
              , this.afaire1.nom)
          }
          setTimeout(this.animeRepere.bind(this), this.tempo)
        } else {
          if (this.dervi2) {
            this.jensuisou = 'Second'
            this.dervi2 = false
          } else {
            this.jensuisou = 'pointPlace0'
            this.dervi3 = false
          }
          this.PtArrive = j3pClone(this.LeptAretenir)
          this.clignEff.push({ ki: this.dteco[this.dteco.length - 1].html.ligne, cool: false })
          this.kiclignote = this.dteco[this.dteco.length - 1].html.ligne
          this.lalacool = false
          this.numclignote = 0
          this.fautagrand = false
          this.faitClignote()
        }

        break
      // POINT DEB
      case 'pointDeb':
        this.avoir = ['ù']
        this.ptplace = { x: 740, y: 30 }
        if (this.afaire.nom.includes('Hz')) this.nomPbuf = this.corres(this.afaire.nom)
        if (this.afaire.cond.length === 0) {
          if (this.afaire.zone) {
            const lazone = this.retrouveZone(this.afaire.zone)
            const baseC = this.modifCoo(lazone.c)
            const baseP = this.modifCoo(lazone.p)
            const coef = j3pGetRandomInt(0, 100)
            baseC.x = baseC.x + 100
            baseC.y = baseC.y - 170
            baseP.x = baseP.x + 100
            baseP.y = baseP.y - 170
            this.PtArrive = {}
            if (lazone.type === 'zone') {
              this.PtArrive.x = baseC.x + (this.dist(baseC, baseP) * coef / 100) * Math.cos(coef * Math.PI / 50)
              this.PtArrive.y = baseC.y + (this.dist(baseC, baseP) * coef / 100) * Math.sin(coef * Math.PI / 50)
            } else {
              this.PtArrive.x = baseC.x + coef / 100 * (baseP.x - baseC.x)
              this.PtArrive.y = baseC.y + coef / 100 * (baseP.y - baseC.y)
            }
          } else {
            this.PtArrive = j3pClone(this.placePointLIbre[this.nbpointsLIbres])
          }
          this.jensuisou = 'pointPlace0'
        } else {
          this.faudrazoom = true
          if (this.afaire.cond.length === 1) {
            this.nbcond = 1
            switch (this.afaire.cond[0].t) {
              case 'long':
                // faut déterminer compas ou regle
                // si ya pas regle grad -> compas
                // si ya pas compas -> regle grad
                // si ya les deux
                // prefere compas si c’est report d’une longueur
                // sinon c’est regle grad
                if (this.ds.Regle_grad === false || this.ds.Equerre === 'false' || this.ds.Equerre === undefined) {
                  if (this.ds.Compas === false || this.ds.Equerre === 'false' || this.ds.Compas === undefined) {
                    console.error('je sais pas faire encore !')
                    return
                  } else {
                    ch = 'compas'
                  }
                } else {
                  if (this.ds.Compas === false || this.ds.Compas === 'false' || this.ds.Compas === undefined) {
                    ch = 'regle'
                  } else {
                    if (this.afaire.cond[0].affiche === 'valeur') {
                      ch = 'regle'
                    } else {
                      if (this.justeUneLongueur(this.afaire.cond[0].cb) !== 'oui') {
                        ch = 'regle'
                      } else {
                        ch = 'compas'
                      }
                    }
                  }
                }
                this.PtArrive = {}
                this.monPointBase = this.afaire.cond[0].cop
                if (this.monPointBase.includes('Hz')) this.monPointBase = this.corres(this.monPointBase)
                this.monPointBase0 = this.monPointBase
                this.CooBase = this.getPointPosition2(this.svgId, this.htmlcoPoint(this.monPointBase))
                dx = 0
                a2 = this.getPointPosition2(this.svgId, '#ptCa1')
                b2 = this.getPointPosition2(this.svgId, '#ptCa2')
                b = this.getPointPosition2(this.svgId, '#ptCa3')
                do {
                  dx++
                  if (this.angleaupif.length === 0) {
                    this.angleaupif = j3pShuffle([0, 20, 40, 60, 80, 100, 120, 140, 160, 200, 220, 240, 260, 280, 300, 320, 340])
                  }
                  this.monangle = this.angleaupif.pop()
                  if (this.monangle > 180) this.monangle -= 360
                  if (this.monangle < -180) this.monangle += 360
                  this.monanglecons = this.monangle - 90 + 13
                  this.monangle = this.monangle * Math.PI / 180
                  if (this.afaire.cond[0].affiche === 'formule') {
                    r = parseFloat(this.calculFormule(this.afaire.cond[0].formule, true, this.afaire.cond[0].hazmin, this.afaire.cond[0].hazmax))
                  } else {
                    r = this.afaire.cond[0].cb
                  }
                  this.PtArrive.x = this.CooBase.x + r * this.unite * Math.cos(this.monangle)
                  this.PtArrive.y = this.CooBase.y - r * this.unite * Math.sin(this.monangle)
                } while (!this.pasDedans(this.PtArrive, a2, b2, b) && dx < 100)

                this.avoir.push(this.monPointBase)
                this.afaire1 = j3pClone(this.afaire.cond[0])
                this.afaire1.laval = r
                if (this.afaire1.co1.includes('Hz')) this.afaire1.co1 = this.corres(this.afaire1.co1)
                for (let j = this.nomprisProg.length - 1; j > -1; j--) {
                  const regex = new RegExp(this.nomprisProg[j].ki, 'g')
                  this.afaire1.formule = this.afaire1.formule.replace(regex, this.nomprisProg[j].com)
                }
                if (ch === 'regle') {
                  if (this.afaire.cond[0].affiche === 'formule') {
                    this.jensuisou = 'RegleMesure0'
                    this.after = 'ReglePlace0'
                    setTimeout(this.animeRepere.bind(this), this.tempo)
                    return
                  } else {
                    this.jensuisou = 'ReglePlace0'
                    this.afaire1.laval = this.afaire1.cb
                  }
                } else {
                  this.avoir.push(this.monPointBase)
                  this.avoir.push(this.afaire1.formule[0])
                  this.jensuisou = 'CompasPrendMesure00'
                  this.after = 'pointPlace0'
                  setTimeout(this.animeRepere.bind(this), this.tempo)
                  return
                }

                break
              case 'ap': {
                let len = this.afaire.cond[0].ki.nom
                if (this.afaire.cond[0].ki.mask !== undefined) len = this.afaire.cond[0].ki.mask
                len = this.recupe(len)
                switch (this.afaire.cond[0].ki.type) {
                  case 'segment': {
                    for (let i = 0; i < this.segCo.length; i++) {
                      if ((this.segCo[i].nom === len) || (this.segCo[i].nom === '[' + len[2] + len[1] + ']')) {
                        this.kiclignote = this.segCo[i].html.ligne
                        nb = this.ConsSegnb
                        dx = i
                        break
                      }
                    }
                    const segOb = this.retrouveSegOb(len)
                    const coA = this.getPointPosition2(this.svgId, segOb.point1)
                    const coB = this.getPointPosition2(this.svgId, segOb.point2)
                    do {
                      const coef = j3pGetRandomInt(1, 99)
                      this.PtArrive = { x: coA.x + coef / 100 * (coB.x - coA.x), y: coA.y + coef / 100 * (coB.y - coA.y) }
                    } while (!this.placeOkPoint(this.PtArrive))
                  }
                    break
                  case 'droite': {
                    fautagrand = false
                    for (let i = 0; i < this.dteco.length; i++) {
                      if ((this.dteco[i].nom === len) || (this.dteco[i].nom === '(' + len[2] + len[1] + ')')) {
                        this.kiclignote = this.dteco[i].html.ligne
                        nb = this.ConsDtenb
                        dx = i
                        fautagrand = false
                        break
                      }
                    }
                    const leseg = this.retrouveDroite(len)
                    const nt1 = this.newTag()
                    const nt2 = this.newTag()
                    const nt3 = this.newTag()
                    const nt4 = this.newTag()
                    this.mtgAppLecteur.addIntLineLine({
                      tag: nt1,
                      name: nt1,
                      d2: leseg,
                      d: 'segbord1'
                    })
                    this.mtgAppLecteur.addIntLineLine({
                      tag: nt2,
                      name: nt2,
                      d2: leseg,
                      d: 'segbord2'
                    })
                    this.mtgAppLecteur.addIntLineLine({
                      tag: nt3,
                      name: nt3,
                      d2: leseg,
                      d: 'segbord3'
                    })
                    this.mtgAppLecteur.addIntLineLine({
                      tag: nt4,
                      name: nt4,
                      d2: leseg,
                      d: 'segbord4'
                    })
                    const coo1 = this.getPointPosition2(this.svgId, nt1)
                    const coo2 = this.getPointPosition2(this.svgId, nt2)
                    const coo3 = this.getPointPosition2(this.svgId, nt3)
                    const coo4 = this.getPointPosition2(this.svgId, nt4)
                    const litOk = []
                    if (coo1.x !== undefined) litOk.push(coo1)
                    if (coo2.x !== undefined) litOk.push(coo2)
                    if (coo3.x !== undefined) litOk.push(coo3)
                    if (coo4.x !== undefined) litOk.push(coo4)
                    const coA = litOk[0]
                    const coB = litOk[1]
                    this.mtgAppLecteur.deleteElt({ elt: nt1 })
                    this.mtgAppLecteur.deleteElt({ elt: nt2 })
                    this.mtgAppLecteur.deleteElt({ elt: nt3 })
                    this.mtgAppLecteur.deleteElt({ elt: nt4 })
                    do {
                      const coef = j3pGetRandomInt(1, 99)
                      this.PtArrive = { x: coA.x + coef / 100 * (coB.x - coA.x), y: coA.y + coef / 100 * (coB.y - coA.y) }
                    } while (!this.placeOkPoint(this.PtArrive))
                  }
                    break
                  case 'cercle':
                    fautagrand = false
                    for (let i = 0; i < this.arcco.length; i++) {
                      if (this.arcco[i].nom === len) {
                        this.kiclignote = this.arcco[i].html.arc1
                        dx = i
                        nb = this.ConsArcnb
                      }
                    }
                    break
                  case 'demi-droite': {
                    fautagrand = false
                    for (let i = 0; i < this.demiDco.length; i++) {
                      if ((this.demiDco[i].nom === len) || (this.demiDco[i].nom === '[' + len[2] + len[1] + ')')) {
                        this.kiclignote = this.demiDco[i].html.ligne
                        nb = this.ConsDtenb
                        dx = i
                        fautagrand = false
                        break
                      }
                    }
                    const leseg = this.retrouveDemidOb(len)
                    const nt1 = this.newTag()
                    const nt2 = this.newTag()
                    const nt3 = this.newTag()
                    const nt4 = this.newTag()
                    this.mtgAppLecteur.addIntLineLine({
                      tag: nt1,
                      name: nt1,
                      d2: leseg.ligne,
                      d: 'segbord1'
                    })
                    this.mtgAppLecteur.addIntLineLine({
                      tag: nt2,
                      name: nt2,
                      d2: leseg.ligne,
                      d: 'segbord2'
                    })
                    this.mtgAppLecteur.addIntLineLine({
                      tag: nt3,
                      name: nt3,
                      d2: leseg.ligne,
                      d: 'segbord3'
                    })
                    this.mtgAppLecteur.addIntLineLine({
                      tag: nt4,
                      name: nt4,
                      d2: leseg.ligne,
                      d: 'segbord4'
                    })
                    const coo1 = this.getPointPosition2(this.svgId, nt1)
                    const coo2 = this.getPointPosition2(this.svgId, nt2)
                    const coo3 = this.getPointPosition2(this.svgId, nt3)
                    const coo4 = this.getPointPosition2(this.svgId, nt4)
                    const litOk = []
                    if (coo1.x !== undefined) litOk.push(coo1)
                    if (coo2.x !== undefined) litOk.push(coo2)
                    if (coo3.x !== undefined) litOk.push(coo3)
                    if (coo4.x !== undefined) litOk.push(coo4)
                    const coA = this.getPointPosition2(this.svgId, leseg.point1)
                    const coB = litOk[0]
                    this.mtgAppLecteur.deleteElt({ elt: nt1 })
                    this.mtgAppLecteur.deleteElt({ elt: nt2 })
                    this.mtgAppLecteur.deleteElt({ elt: nt3 })
                    this.mtgAppLecteur.deleteElt({ elt: nt4 })
                    do {
                      const coef = j3pGetRandomInt(1, 99)
                      this.PtArrive = { x: coA.x + coef / 100 * (coB.x - coA.x), y: coA.y + coef / 100 * (coB.y - coA.y) }
                    } while (!this.placeOkPoint(this.PtArrive))
                  }
                    break
                }
                if (dx === undefined) { this.kiclignote = [len[1], len[2]] } else {
                  // FIXME ne pas utiliser le i utilisé dans plein de boucles for parmi les 120 lignes qui précèdent, passer par une variable maxTruc que l’on initialise aux bons endroits si vraiment faut se rappeler d’une borne max qq part
                  this.clignEff.push({ ki: this.kiclignote, cool: nb > dx })
                  this.lalacool = nb > dx
                }
                this.jensuisou = 'pointPlace0'
                this.numclignote = 0
                this.fautagrand = fautagrand
                this.faitClignote(true)
              }
                break
                // si une appartient
                // fait apparaitre en rouge
                // place point
              case 'ang': {
                const tagRot = this.newTag()
                const tagRot2 = this.newTag()
                this.mtgAppLecteur.addImPointRotation({
                  a: this.htmlcoPoint(this.recupe(this.afaire.cond[0].cop2)),
                  o: this.htmlcoPoint(this.recupe(this.afaire.cond[0].cop)),
                  tag: tagRot,
                  name: tagRot,
                  x: Number(this.afaire.cond[0].formule)
                })
                this.mtgAppLecteur.addImPointRotation({
                  a: this.htmlcoPoint(this.recupe(this.afaire.cond[0].cop2)),
                  o: this.htmlcoPoint(this.recupe(this.afaire.cond[0].cop)),
                  tag: tagRot2,
                  name: tagRot2,
                  x: -Number(this.afaire.cond[0].formule)
                })
                const tagDD1 = this.newTag()
                const tagDD2 = this.newTag()
                this.mtgAppLecteur.addRay({
                  o: this.htmlcoPoint(this.recupe(this.afaire.cond[0].cop)),
                  a: tagRot,
                  tag: tagDD1
                })
                this.mtgAppLecteur.addRay({
                  o: this.htmlcoPoint(this.recupe(this.afaire.cond[0].cop)),
                  a: tagRot2,
                  tag: tagDD2
                })
                const nt1 = this.newTag()
                const nt2 = this.newTag()
                const nt3 = this.newTag()
                const nt4 = this.newTag()
                this.mtgAppLecteur.addIntLineLine({
                  tag: nt1,
                  name: nt1,
                  d2: tagDD1,
                  d: 'segbord1'
                })
                this.mtgAppLecteur.addIntLineLine({
                  tag: nt2,
                  name: nt2,
                  d2: tagDD1,
                  d: 'segbord2'
                })
                this.mtgAppLecteur.addIntLineLine({
                  tag: nt3,
                  name: nt3,
                  d2: tagDD1,
                  d: 'segbord3'
                })
                this.mtgAppLecteur.addIntLineLine({
                  tag: nt4,
                  name: nt4,
                  d2: tagDD1,
                  d: 'segbord4'
                })
                const coo1 = this.getPointPosition2(this.svgId, nt1)
                const coo2 = this.getPointPosition2(this.svgId, nt2)
                const coo3 = this.getPointPosition2(this.svgId, nt3)
                const coo4 = this.getPointPosition2(this.svgId, nt4)
                const litOk = []
                if (coo1.x !== undefined) litOk.push(coo1)
                if (coo2.x !== undefined) litOk.push(coo2)
                if (coo3.x !== undefined) litOk.push(coo3)
                if (coo4.x !== undefined) litOk.push(coo4)
                const nt12 = this.newTag()
                const nt22 = this.newTag()
                const nt32 = this.newTag()
                const nt42 = this.newTag()
                this.mtgAppLecteur.addIntLineLine({
                  tag: nt12,
                  name: nt12,
                  d2: tagDD2,
                  d: 'segbord1'
                })
                this.mtgAppLecteur.addIntLineLine({
                  tag: nt22,
                  name: nt22,
                  d2: tagDD2,
                  d: 'segbord2'
                })
                this.mtgAppLecteur.addIntLineLine({
                  tag: nt32,
                  name: nt32,
                  d2: tagDD2,
                  d: 'segbord3'
                })
                this.mtgAppLecteur.addIntLineLine({
                  tag: nt42,
                  name: nt42,
                  d2: tagDD2,
                  d: 'segbord4'
                })
                const coo12 = this.getPointPosition2(this.svgId, nt12)
                const coo22 = this.getPointPosition2(this.svgId, nt22)
                const coo32 = this.getPointPosition2(this.svgId, nt32)
                const coo42 = this.getPointPosition2(this.svgId, nt42)
                const litOk2 = []
                if (coo12.x !== undefined) litOk2.push(coo12)
                if (coo22.x !== undefined) litOk2.push(coo22)
                if (coo32.x !== undefined) litOk2.push(coo32)
                if (coo42.x !== undefined) litOk2.push(coo42)
                this.mtgAppLecteur.deleteElt({ elt: tagRot })
                this.mtgAppLecteur.deleteElt({ elt: tagRot2 })
                const coA = this.getPointPosition2(this.svgId, this.htmlcoPoint(this.recupe(this.afaire.cond[0].cop)))
                const coD = this.getPointPosition2(this.svgId, this.htmlcoPoint(this.recupe(this.afaire.cond[0].cop2)))
                // boucle pour choper un point { un seg au hasard, un point , puis verif }
                do {
                  const coB = (j3pGetRandomBool()) ? litOk[0] : litOk2[0]
                  const coef = j3pGetRandomInt(1, 99)
                  this.PtArrive = { x: coA.x + coef / 100 * (coB.x - coA.x), y: coA.y + coef / 100 * (coB.y - coA.y) }
                } while (!this.placeOkPoint(this.PtArrive))
                this.afaire1 = j3pClone(this.afaire.cond[0])
                dista = -Math.atan2(this.PtArrive.x - coA.x, coA.y - this.PtArrive.y)
                this.afaire1.monangle = -Math.atan2(coD.x - coA.x, coA.y - coD.y) + Math.PI / 2 + Math.PI / 2
                this.avoir = [this.recupe(this.afaire1.cop), this.PtArrive]
                this.afaire1.modan = -Math.PI / 2
                if (this.distangle(dista * 180 / Math.PI, this.afaire1.monangle * 180 / Math.PI + 90) < 90) {
                  this.afaire1.monangle += Math.PI
                  this.afaire1.modan = Math.PI / 2
                }
                this.afaire1.dista = dista * 180 / Math.PI
                // cest parti mon kiki
                this.jensuisou = 'Rap0'
                this.after = 'pointPlace0'
                this.after2 = undefined
                if (!this.yaseg(coA, coD, false, undefined)) {
                  this.jensuisou = 'demidroite'
                  this.after = 'Rap0'
                  this.afaireAcons = j3pClone(this.afaire1)
                  this.afaire = { condd: '', pass: this.afaireAcons.cop2, pass2: this.afaireAcons.cop }
                  this.after2 = 'pointPlace0'
                }
              }
                break
              case 'sur':
                this.PtArrive = this.getPointPosition2(this.svgId, this.htmlcoPoint(this.recupe(this.afaire.cond[0].cop)))
                this.jensuiouPlace = 'go'
                this.jensuisou = 'pointPlace0'
                break
            }
          } else {
            this.nbcond = 2
            // si 2 cond
            // on gere dabaord les app
            this.afaire1 = j3pClone(this.afaire.cond[0])
            this.afaire2 = j3pClone(this.afaire.cond[1])
            if (this.afaire.cond[1].t === 'ap') {
              this.afaire1 = j3pClone(this.afaire.cond[1])
              this.afaire2 = j3pClone(this.afaire.cond[0])
            }
            if (this.afaire.cond[0].t !== 'ap') {
              if (this.afaire.cond[1].t === 'ang' && this.afaire.cond[0].t !== 'ang') {
                this.afaire1 = j3pClone(this.afaire.cond[1])
                this.afaire2 = j3pClone(this.afaire.cond[0])
              }
            }
            this.jensuisou = 'First'

            // trouve place point
            switch (this.afaire1.t) {
              case 'ap':
                switch (this.afaire2.t) {
                  case 'ap': {
                    let cont1 = this.retrouveTruc(this.recupe(this.afaire1.ki.nom))
                    let cont2 = this.retrouveTruc(this.recupe(this.afaire2.ki.nom))
                    if (cont2 === undefined) {
                      cont2 = this.reconstruit(this.recupe(this.afaire2.ki.nom))
                      if (!cont2) {
                        // ca je le virerai plus tard, c'est dans une autre branche
                        const p1 = this.retrouvePoint(this.recupe(this.afaire2.ki.nom)[1])
                        const p2 = this.retrouvePoint(this.recupe(this.afaire2.ki.nom)[2])
                        const tatag = this.newTag()
                        this.mtgAppLecteur.addLineAB({
                          a: p1,
                          b: p2,
                          tag: tatag,
                          hidden: true
                        })
                        cont2 = { type: 'droite', tab: [tatag] }
                      } else {
                        cont2 = { type: 'droite', tab: [cont2] }
                      }
                    }
                    if (cont1 === undefined) {
                      cont1 = this.reconstruit(this.recupe(this.afaire1.ki.nom))
                      if (!cont1) {
                        // ca je le virerai plus tard, c'est dans une autre branche
                        const p1 = this.retrouvePoint(this.recupe(this.afaire1.ki.nom)[1])
                        const p2 = this.retrouvePoint(this.recupe(this.afaire1.ki.nom)[2])
                        const tatag = this.newTag()
                        this.mtgAppLecteur.addLineAB({
                          a: p1,
                          b: p2,
                          tag: tatag,
                          hidden: true
                        })
                        cont1 = { type: 'droite', tab: [tatag] }
                      } else {
                        cont1 = { type: 'droite', tab: [cont1] }
                      }
                    }
                    switch (this.afaire1.ki.type) {
                      case 'droite':
                      case 'demi-droite':
                      case 'segment':
                        switch (this.afaire2.ki.type) {
                          case 'droite':
                          case 'demi-droite':
                          case 'segment': {
                            const nPt = this.newTag()
                            this.mtgAppLecteur.addIntLineLine({
                              d: cont1.tab[0],
                              d2: cont2.tab[0],
                              tag: nPt,
                              name: nPt
                            })
                            this.PtArrive = this.mtgAppLecteur.getPointPosition({ a: nPt })
                            this.mtgAppLecteur.deleteElt({ elt: nPt })
                          }
                            break
                          default: {
                            let litOk = []
                            for (let i = 0; i < cont2.tab.length; i++) {
                              const nt1 = this.newTag()
                              const nt2 = this.newTag()
                              this.mtgAppLecteur.addIntLineCircle({
                                tag: nt1,
                                name: nt1,
                                tag2: nt2,
                                name2: nt2,
                                c: cont2.tab[i],
                                d: cont1.tab[0]
                              })
                              const coo1 = this.getPointPosition2(this.svgId, nt1)
                              const coo2 = this.getPointPosition2(this.svgId, nt2)
                              if (coo1.x !== undefined) {
                                if (this.placeOkPoint(coo1, true)) {
                                  litOk.push(coo1)
                                }
                              }
                              if (coo2.x !== undefined) {
                                if (this.placeOkPoint(coo2, true)) {
                                  litOk.push(coo2)
                                }
                              }
                              if (litOk.length > 1) {
                                if (this.afaire.prox !== undefined) {
                                  pt3 = this.getPointPosition2(this.svgId, this.htmlcoPoint(this.recupe(this.afaire.prox)))
                                  if (this.dist(pt3, litOk[0]) < this.dist(pt3, litOk[1])) {
                                    litOk = [litOk[0]]
                                  } else {
                                    litOk = [litOk[1]]
                                  }
                                }
                                if (this.afaire.noprox !== undefined) {
                                  pt3 = this.getPointPosition2(this.svgId, this.htmlcoPoint(this.recupe(this.afaire.noprox)))
                                  if (this.dist(pt3, litOk[0]) < this.dist(pt3, litOk[1])) {
                                    litOk = [litOk[1]]
                                  } else {
                                    litOk = [litOk[0]]
                                  }
                                }
                              }
                              this.mtgAppLecteur.deleteElt({ elt: nt1 })
                              this.mtgAppLecteur.deleteElt({ elt: nt2 })
                            }
                            litOk = j3pShuffle(litOk)
                            this.PtArrive = litOk.pop()
                          }
                        }
                        break
                      default: {
                        switch (this.afaire2.ki.type) {
                          case 'droite':
                          case 'demi-droite':
                          case 'segment': {
                            let litOk = []
                            for (let i = 0; i < cont2.tab.length; i++) {
                              const nt1 = this.newTag()
                              const nt2 = this.newTag()
                              this.mtgAppLecteur.addIntLineCircle({
                                tag: nt1,
                                name: nt1,
                                tag2: nt2,
                                name2: nt2,
                                c: cont1.tab[i],
                                d: cont2.tab[0]
                              })
                              const coo1 = this.getPointPosition2(this.svgId, nt1)
                              const coo2 = this.getPointPosition2(this.svgId, nt2)
                              if (coo1.x !== undefined) {
                                if (this.placeOkPoint(coo1, true)) {
                                  litOk.push(coo1)
                                }
                              }
                              if (coo2.x !== undefined) {
                                if (this.placeOkPoint(coo2, true)) {
                                  litOk.push(coo2)
                                }
                              }
                              this.mtgAppLecteur.deleteElt({ elt: nt1 })
                              this.mtgAppLecteur.deleteElt({ elt: nt2 })
                            }
                            if (litOk.length > 1) {
                              if (this.afaire.prox !== undefined) {
                                pt3 = this.getPointPosition2(this.svgId, this.htmlcoPoint(this.recupe(this.afaire.prox)))
                                if (this.dist(pt3, litOk[0]) < this.dist(pt3, litOk[1])) {
                                  litOk = [litOk[0]]
                                } else {
                                  litOk = [litOk[1]]
                                }
                              }
                              if (this.afaire.noprox !== undefined) {
                                pt3 = this.getPointPosition2(this.svgId, this.htmlcoPoint(this.recupe(this.afaire.noprox)))
                                if (this.dist(pt3, litOk[0]) < this.dist(pt3, litOk[1])) {
                                  litOk = [litOk[1]]
                                } else {
                                  litOk = [litOk[0]]
                                }
                              }
                            }
                            litOk = j3pShuffle(litOk)
                            this.PtArrive = litOk.pop()
                          }
                            break
                          default: {
                            let litOk = []
                            for (let i = 0; i < cont2.tab.length; i++) {
                              for (let j = 0; j < cont1.tab.length; j++) {
                                const nt1 = this.newTag()
                                const nt2 = this.newTag()
                                this.mtgAppLecteur.addIntCircleCircle({
                                  tag: nt1,
                                  name: nt1,
                                  tag2: nt2,
                                  name2: nt2,
                                  c: cont2.tab[i],
                                  d: cont1.tab[j]
                                })
                                const coo1 = this.getPointPosition2(this.svgId, nt1)
                                const coo2 = this.getPointPosition2(this.svgId, nt2)
                                if (coo1.x !== undefined) {
                                  if (this.placeOkPoint(coo1, true)) {
                                    litOk.push(coo1)
                                  }
                                }
                                if (coo2.x !== undefined) {
                                  if (this.placeOkPoint(coo2, true)) {
                                    litOk.push(coo2)
                                  }
                                }
                                this.mtgAppLecteur.deleteElt({ elt: nt1 })
                                this.mtgAppLecteur.deleteElt({ elt: nt2 })
                              }
                            }
                            if (litOk.length > 1) {
                              if (this.afaire.prox !== undefined) {
                                pt3 = this.getPointPosition2(this.svgId, this.htmlcoPoint(this.recupe(this.afaire.prox)))
                                if (this.dist(pt3, litOk[0]) < this.dist(pt3, litOk[1])) {
                                  litOk = [litOk[0]]
                                } else {
                                  litOk = [litOk[1]]
                                }
                              }
                              if (this.afaire.noprox !== undefined) {
                                pt3 = this.getPointPosition2(this.svgId, this.htmlcoPoint(this.recupe(this.afaire.noprox)))
                                if (this.dist(pt3, litOk[0]) < this.dist(pt3, litOk[1])) {
                                  litOk = [litOk[1]]
                                } else {
                                  litOk = [litOk[0]]
                                }
                              }
                            }
                            litOk = j3pShuffle(litOk)
                            this.PtArrive = litOk.pop()
                          }
                        }
                      }
                    }
                  }
                    break
                  case 'long': {
                    nnn = this.afaire2.cop
                    for (let i = this.nomprisProg.length - 1; i > -1; i--) {
                      const regex = new RegExp(this.nomprisProg[i].ki, 'g')
                      nnn = nnn.replace(regex, this.nomprisProg[i].com)
                    }
                    pt3 = this.getPointPosition2(this.svgId, this.htmlcoPoint(nnn))
                    if (this.afaire2.cb !== undefined) {
                      r = this.afaire2.cb
                      r = mqNormalise(this.recupe(r)).replace(/\$/g, '')
                    } else {
                      this.afaire2.cb = mqNormalise(this.afaire2.formule).replace(/\$/g, '')
                      r = mqNormalise(this.afaire2.formule).replace(/\$/g, '')
                    }
                    let ssaute = false
                    if (this.afaire2.formule !== undefined) {
                      ssaute = this.justeUneLongueur(this.recupe(this.afaire2.formule))
                    }
                    if ((!this.justeUneLongueur(this.afaire2.cb) && (!ssaute)) || this.afaire2.affiche !== 'formule') {
                      if (this.afaire2.estmult) {
                        this.ch = 'compas'
                        if (this.ds.Compas === false || this.ds.Compas === 'false' || this.ds.Compas === undefined) this.ch = 'regle'
                      } else { this.ch = 'regle' }
                    } else {
                      this.ch = 'compas'
                      if (this.ds.Compas === false || this.ds.Compas === 'false' || this.ds.Compas === undefined) this.ch = 'regle'
                    }
                    if (this.afaire1.ki.type !== 'cercle') {
                      nnn = this.giveABs(this.afaire1)
                      a = nnn.a
                      b = nnn.b
                      pt1 = nnn.pt1
                      pt2 = nnn.pt2
                      if (this.distPtDtebis(pt1, pt3, pt2) > this.distcol / this.unite) {
                        this.ch = 'compas'
                      }
                      r = this.calculeFormule(String(r))
                      const sol = this.getInstersectingPoints(a, b, pt3.x, pt3.y, r * this.unite)
                      if (this.afaire1.ki.type === 'segment') {
                        for (let i = sol.length - 1; i > -1; i--) {
                          if (sol[i].x < Math.min(pt1.x, pt2.x) || sol[i].x > Math.max(pt1.x, pt2.x)) sol.splice(i, 1)
                        }
                      }
                      if (this.afaire1.ki.type === 'demi-droite') {
                        for (let i = sol.length - 1; i > -1; i--) {
                          if (pt1.x > pt2.x) {
                            if (sol[i].x > pt1.x) sol.splice(i, 1)
                          } else if (pt1.x < pt2.x) {
                            if (sol[i].x < pt1.x) sol.splice(i, 1)
                          } else if (pt1.y > pt2.y) {
                            if (sol[i].y > pt1.y) sol.splice(i, 1)
                          } else {
                            if (sol[i].y < pt1.y) sol.splice(i, 1)
                          }
                        }
                      }
                      this.PtArrive = sol[j3pGetRandomInt(0, sol.length - 1)]
                      if (sol.length > 1) {
                        if (this.afaire.prox !== undefined) {
                          pt3 = this.getPointPosition2(this.svgId, this.htmlcoPoint(this.recupe(this.afaire.prox)))
                          if (this.dist(pt3, sol[0]) < this.dist(pt3, sol[1])) {
                            this.PtArrive = j3pClone(sol[0])
                          } else {
                            this.PtArrive = j3pClone(sol[1])
                          }
                        }
                        if (this.afaire.noprox !== undefined) {
                          pt3 = this.getPointPosition2(this.svgId, this.htmlcoPoint(this.recupe(this.afaire.noprox)))
                          if (this.dist(pt3, sol[0]) < this.dist(pt3, sol[1])) {
                            this.PtArrive = j3pClone(sol[1])
                          } else {
                            this.PtArrive = j3pClone(sol[0])
                          }
                        }
                      }
                      this.monangle = Math.atan2(pt2.x - pt1.x, pt1.y - pt2.y) - Math.PI / 2 - 0.01
                    }
                    break
                  }
                  case 'nap':
                    break
                  case 'ang': {
                    const cont1 = this.retrouveTruc(this.recupe(this.afaire1.ki.nom))

                    const tagRot = this.newTag()
                    const tagRot2 = this.newTag()
                    this.mtgAppLecteur.addImPointRotation({
                      a: this.htmlcoPoint(this.recupe(this.afaire2.cop2)),
                      o: this.htmlcoPoint(this.recupe(this.afaire2.cop)),
                      tag: tagRot,
                      name: tagRot,
                      x: Number(this.afaire2.formule)
                    })
                    this.mtgAppLecteur.addImPointRotation({
                      a: this.htmlcoPoint(this.recupe(this.afaire2.cop2)),
                      o: this.htmlcoPoint(this.recupe(this.afaire2.cop)),
                      tag: tagRot2,
                      name: tagRot2,
                      x: -Number(this.afaire2.formule)
                    })
                    const tagDD1 = this.newTag()
                    const tagDD2 = this.newTag()
                    this.mtgAppLecteur.addRay({
                      o: this.htmlcoPoint(this.recupe(this.afaire2.cop)),
                      a: tagRot,
                      tag: tagDD1
                    })
                    this.mtgAppLecteur.addRay({
                      o: this.htmlcoPoint(this.recupe(this.afaire2.cop)),
                      a: tagRot2,
                      tag: tagDD2
                    })

                    switch (this.afaire1.ki.type) {
                      case 'droite':
                      case 'demi-droite':
                      case 'segment': {
                        let litOk = []
                        const nPt = this.newTag()
                        const nPt2 = this.newTag()
                        this.mtgAppLecteur.addIntLineLine({
                          d: cont1.tab[0],
                          d2: tagDD1,
                          tag: nPt,
                          name: nPt
                        })
                        this.mtgAppLecteur.addIntLineLine({
                          d: cont1.tab[0],
                          d2: tagDD2,
                          tag: nPt2,
                          name: nPt2
                        })
                        const coo1 = this.getPointPosition2(this.svgId, nPt)
                        const coo2 = this.getPointPosition2(this.svgId, nPt2)
                        this.mtgAppLecteur.deleteElt({ elt: nPt })
                        this.mtgAppLecteur.deleteElt({ elt: nPt2 })
                        if (coo1.x !== undefined) {
                          if (this.placeOkPoint(coo1, true)) {
                            litOk.push(coo1)
                          }
                        }
                        if (coo2.x !== undefined) {
                          if (this.placeOkPoint(coo2, true)) {
                            litOk.push(coo2)
                          }
                        }
                        litOk = j3pShuffle(litOk)
                        this.PtArrive = litOk.pop()
                      }
                        break
                      default: {
                        let litOk = []
                        for (let i = 0; i < cont1.tab.length; i++) {
                          const nt1 = this.newTag()
                          const nt2 = this.newTag()
                          const nt3 = this.newTag()
                          const nt4 = this.newTag()
                          this.mtgAppLecteur.addIntLineCircle({
                            tag: nt1,
                            name: nt1,
                            tag2: nt2,
                            name2: nt2,
                            c: cont1.tab[i],
                            d: tagDD1
                          })
                          this.mtgAppLecteur.addIntLineCircle({
                            tag: nt3,
                            name: nt3,
                            tag2: nt4,
                            name2: nt4,
                            c: cont1.tab[i],
                            d: tagDD2
                          })
                          const coo1 = this.getPointPosition2(this.svgId, nt1)
                          const coo2 = this.getPointPosition2(this.svgId, nt2)
                          const coo3 = this.getPointPosition2(this.svgId, nt3)
                          const coo4 = this.getPointPosition2(this.svgId, nt4)
                          this.mtgAppLecteur.deleteElt({ elt: nt1 })
                          this.mtgAppLecteur.deleteElt({ elt: nt2 })
                          this.mtgAppLecteur.deleteElt({ elt: nt3 })
                          this.mtgAppLecteur.deleteElt({ elt: nt4 })
                          if (coo1.x !== undefined) {
                            if (this.placeOkPoint(coo1, true)) {
                              litOk.push(coo1)
                            }
                          }
                          if (coo2.x !== undefined) {
                            if (this.placeOkPoint(coo2, true)) {
                              litOk.push(coo2)
                            }
                          }
                          if (coo3.x !== undefined) {
                            if (this.placeOkPoint(coo3, true)) {
                              litOk.push(coo3)
                            }
                          }
                          if (coo4.x !== undefined) {
                            if (this.placeOkPoint(coo4, true)) {
                              litOk.push(coo4)
                            }
                          }
                        }
                        litOk = j3pShuffle(litOk)
                        this.PtArrive = litOk.pop()
                      }
                    }

                    this.mtgAppLecteur.deleteElt({ elt: tagRot })
                    this.mtgAppLecteur.deleteElt({ elt: tagRot2 })
                    const coA = this.getPointPosition2(this.svgId, this.htmlcoPoint(this.recupe(this.afaire2.cop)))
                    const coD = this.getPointPosition2(this.svgId, this.htmlcoPoint(this.recupe(this.afaire2.cop2)))

                    dista = -Math.atan2(this.PtArrive.x - coA.x, coA.y - this.PtArrive.y)
                    this.afaire2.monangle = -Math.atan2(coD.x - coA.x, coA.y - coD.y) + Math.PI / 2 + Math.PI / 2
                    this.avoir = [this.recupe(this.afaire2.cop), this.PtArrive]
                    this.afaire2.modan = -Math.PI / 2
                    if (this.distangle(dista * 180 / Math.PI, this.afaire2.monangle * 180 / Math.PI + 90) < 90) {
                      this.afaire2.monangle += Math.PI
                      this.afaire2.modan = Math.PI / 2
                    }
                    this.afaire2.dista = dista * 180 / Math.PI

                    // cest parti mon kiki
                    this.jensuisou = 'First'
                    this.after = undefined
                    this.after2 = undefined

                    /// /
                    /// / FIN COPIE
                    ///
                  }
                    break
                  default:
                    console.error('Cas a faire', this.afaire1, this.afaire2)
                }
                break
              case 'long':
                switch (this.afaire2.t) {
                  case 'long': {
                    nnn = this.afaire2.cop
                    for (let i = this.nomprisProg.length - 1; i > -1; i--) {
                      const regex = new RegExp(this.nomprisProg[i].ki, 'g')
                      nnn = nnn.replace(regex, this.nomprisProg[i].com)
                    }
                    pt1 = this.getPointPosition2(this.svgId, this.htmlcoPoint(nnn))
                    if (this.afaire2.cb === undefined) this.afaire2.cb = this.afaire2.formule
                    r1 = this.calculeFormule(this.afaire2.cb) * this.unite
                    this.afaire2.centre = nnn

                    nnn = this.afaire1.cop
                    for (let i = this.nomprisProg.length - 1; i > -1; i--) {
                      const regex = new RegExp(this.nomprisProg[i].ki, 'g')
                      nnn = nnn.replace(regex, this.nomprisProg[i].com)
                    }
                    pt2 = this.getPointPosition2(this.svgId, this.htmlcoPoint(nnn))
                    if (this.afaire1.cb === undefined) this.afaire1.cb = this.afaire1.formule
                    r2 = this.calculeFormule(this.afaire1.cb) * this.unite
                    const sol = this.inter2cercles(pt1, r1, pt2, r2)
                    this.afaire1.laval = r1 / this.unite
                    this.afaire2.laval = r2 / this.unite

                    if (Math.abs(this.afaire1.laval - this.afaire2.laval) < 0.001) this.racour = true
                    a2 = this.getPointPosition2(this.svgId, '#ptCa1')
                    b2 = this.getPointPosition2(this.svgId, '#ptCa2')
                    b = this.getPointPosition2(this.svgId, '#ptCa3')
                    tt = []
                    for (let i = 0; i < sol.length; i++) {
                      if (this.pasDedans(sol[i], a2, b2, b)) tt.push(sol[i])
                    }
                    if (tt.length === 0) {
                      // ca sort
                      this.jensuisou = 'fini'
                      setTimeout(this.animeRepere.bind(this), this.tempo)
                      return
                    }
                    this.PtArrive = j3pClone(tt[j3pGetRandomInt(0, tt.length - 1)])
                    if (this.afaire.prox !== undefined) {
                      pt3 = this.getPointPosition2(this.svgId, this.htmlcoPoint(this.recupe(this.afaire.prox)))
                      if (this.dist(pt3, sol[0]) < this.dist(pt3, sol[1])) {
                        this.PtArrive = j3pClone(sol[0])
                      } else {
                        this.PtArrive = j3pClone(sol[1])
                      }
                    }
                    if (this.afaire.noprox !== undefined) {
                      pt3 = this.getPointPosition2(this.svgId, this.htmlcoPoint(this.recupe(this.afaire.noprox)))
                      const oklmpo = []
                      for (let i = 0; i < tt.length; i++) {
                        oklmpo.push({ k: j3pClone(tt[i]), dist: this.dist(pt3, tt[i]) })
                      }
                      oklmpo.sort(this.sortOnDistProp)
                      this.PtArrive = j3pClone(oklmpo[oklmpo.length - 1].k)
                    }
                    this.afaire1.monangle = Math.PI / 2 - Math.atan2(this.PtArrive.x - pt2.x, pt2.y - this.PtArrive.y)
                    this.afaire2.monangle = Math.PI / 2 - Math.atan2(this.PtArrive.x - pt1.x, pt1.y - this.PtArrive.y)
                    this.monangle = this.afaire1.monangle
                    this.afaire1.centre = nnn
                    this.after = 'changed'
                    this.bary = this.bari2([pt1, this.PtArrive])
                    setTimeout(this.animeRepere.bind(this), this.tempo)
                    this.ch = 'compas'
                    return
                  } // long
                } // switch afaire2.t
                break
              case 'ang':
                switch (this.afaire2.t) {
                  case 'ang': {
                    // calcule ax+b des deux premiers demi d1 et d2
                    ch = this.donneAb(this.afaire1.cop, this.afaire1.cop2, this.afaire1.cb, this.afaire1.sens)
                    pt4 = this.donneAb(this.afaire2.cop, this.afaire2.cop2, this.afaire2.cb, this.afaire2.sens)

                    pt3 = this.getPointPosition2(this.svgId, this.htmlcoPoint(this.recupe(this.afaire1.cop)))
                    pt1 = this.getPointPosition2(this.svgId, this.htmlcoPoint(this.recupe(this.afaire2.cop)))
                    pt2 = this.getPointPosition2(this.svgId, this.htmlcoPoint(this.recupe(this.afaire1.cop2)))
                    b = this.getPointPosition2(this.svgId, this.htmlcoPoint(this.recupe(this.afaire2.cop2)))
                    const sol = []
                    for (let i = 0; i < ch.length; i++) {
                      for (r = 0; r < pt4.length; r++) {
                        if (ch[i].a !== pt4[r].a) {
                          nnn = { x: (ch[i].b - pt4[r].b) / (pt4[r].a - ch[i].a) }
                          nnn.y = pt4[r].a * nnn.x + pt4[r].b
                          if (this.distangle(this.afaire1.cb, this.lang(nnn, pt3, pt2)) < 0.5 && this.distangle(this.afaire2.cb, this.lang(nnn, pt1, b)) < 0.5) sol.push(j3pClone(nnn))
                        }
                      }
                    }
                    tt = []
                    a2 = this.getPointPosition2(this.svgId, '#ptCa1')
                    b2 = this.getPointPosition2(this.svgId, '#ptCa2')
                    r2 = this.getPointPosition2(this.svgId, '#ptCa3')
                    for (let i = 0; i < sol.length; i++) {
                      if (this.pasDedans(sol[i], a2, b2, r2)) tt.push(sol[i])
                    }

                    if (tt.length === 0) {
                      // ca sort
                      this.jensuisou = 'fini'
                      setTimeout(this.animeRepere.bind(this), this.tempo)
                      return
                    }
                    this.PtArrive = j3pClone(tt[j3pGetRandomInt(0, tt.length - 1)])
                    /*
                    if (this.afaire.prox !== undefined) {
                      pt3 = this.getPointPosition2(this.svgId,  this.htmlcoPoint(this.recupe(this.afaire.prox)))
                      if (this.dist(pt3, sol[0]) < this.dist(pt3, sol[1])) {
                        this.PtArrive = j3pClone(sol[0])
                      } else {
                        this.PtArrive = j3pClone(sol[1])
                      }
                    }
                    if (this.afaire.noprox !== undefined) {
                      pt3 = this.getPointPosition2(this.svgId,  this.htmlcoPoint(this.recupe(this.afaire.noprox)))
                      if (this.dist(pt3, sol[0]) < this.dist(pt3, sol[1])) {
                        this.PtArrive = j3pClone(sol[1])
                      } else {
                        this.PtArrive = j3pClone(sol[0])
                      }
                    }

                       */

                    dista = -Math.atan2(this.PtArrive.x - pt3.x, pt3.y - this.PtArrive.y)
                    this.afaire1.monangle = -Math.atan2(pt2.x - pt3.x, pt3.y - pt2.y)
                    this.avoir = [this.recupe(this.afaire1.cop), this.PtArrive]
                    this.afaire1.modan = Math.PI / 2
                    if (this.distangle(dista * 180 / Math.PI, this.afaire1.monangle * 180 / Math.PI + 90) < 90) {
                      this.afaire1.monangle += Math.PI
                      this.afaire1.modan = -Math.PI / 2
                    }
                    this.afaire1.dista = dista * 180 / Math.PI

                    dista = -Math.atan2(this.PtArrive.x - pt1.x, pt1.y - this.PtArrive.y)
                    this.afaire2.monangle = -Math.atan2(b.x - pt1.x, pt1.y - b.y)
                    this.afaire2.modan = Math.PI / 2
                    if (this.distangle(dista * 180 / Math.PI, this.afaire2.monangle * 180 / Math.PI + 90) < 90) {
                      this.afaire2.monangle += Math.PI
                      this.afaire2.modan = -Math.PI / 2
                    }
                    this.afaire2.dista = dista * 180 / Math.PI
                    this.after = 'Second'
                    // vir sol hors cadre
                    // prend 1 au pif
                    break
                  }
                  case 'long': {
                    ch = this.donneAb(this.afaire1.cop, this.afaire1.cop2, this.afaire1.cb, this.afaire1.sens)
                    pt3 = this.getPointPosition2(this.svgId, this.htmlcoPoint(this.recupe(this.afaire1.cop)))
                    pt1 = this.getPointPosition2(this.svgId, this.htmlcoPoint(this.recupe(this.afaire2.cop)))
                    pt4 = this.getPointPosition2(this.svgId, this.htmlcoPoint(this.recupe(this.afaire1.cop2)))
                    r = this.calculeFormule(this.afaire2.cb)
                    const sol = []
                    for (let i = 0; i < ch.length; i++) {
                      nnn = this.getInstersectingPoints(ch[i].a, ch[i].b, pt1.x, pt1.y, r * this.unite)
                      for (let j = 0; j < nnn.length; j++) {
                        if (this.distangle(this.afaire1.cb, this.lang(nnn[j], pt3, pt4)) < 0.5) sol.push(j3pClone(nnn[j]))
                      }
                    }
                    tt = []
                    a2 = this.getPointPosition2(this.svgId, '#ptCa1')
                    b2 = this.getPointPosition2(this.svgId, '#ptCa2')
                    b = this.getPointPosition2(this.svgId, '#ptCa3')
                    for (let i = 0; i < sol.length; i++) {
                      if (this.pasDedans(sol[i], a2, b2, b)) tt.push(sol[i])
                    }

                    if (tt.length === 0) {
                      // ca sort
                      this.jensuisou = 'fini'
                      setTimeout(this.animeRepere.bind(this), this.tempo)
                      return
                    }
                    this.PtArrive = j3pClone(tt[j3pGetRandomInt(0, tt.length - 1)])

                    if (this.afaire.prox !== undefined) {
                      pt1 = this.getPointPosition2(this.svgId, this.htmlcoPoint(this.recupe(this.afaire.prox)))
                      const oklmpo = []
                      for (let i = 0; i < tt.length; i++) {
                        oklmpo.push({ k: j3pClone(tt[i]), dist: this.dist(pt1, tt[i]) })
                      }
                      oklmpo.sort(this.sortOnDistProp)
                      this.PtArrive = j3pClone(oklmpo[0].k)
                    }
                    if (this.afaire.noprox !== undefined) {
                      pt1 = this.getPointPosition2(this.svgId, this.htmlcoPoint(this.recupe(this.afaire.noprox)))
                      const oklmpo = []
                      for (let i = 0; i < tt.length; i++) {
                        oklmpo.push({ k: j3pClone(tt[i]), dist: this.dist(pt1, tt[i]) })
                      }
                      oklmpo.sort(this.sortOnDistProp)
                      this.PtArrive = j3pClone(oklmpo[oklmpo.length - 1].k)
                    }

                    dista = -Math.atan2(this.PtArrive.x - pt3.x, pt3.y - this.PtArrive.y)
                    this.afaire1.monangle = -Math.atan2(pt4.x - pt3.x, pt3.y - pt4.y)

                    this.avoir = [this.recupe(this.afaire1.cop), this.PtArrive]
                    this.afaire1.modan = Math.PI / 2
                    if (this.distangle(dista * 180 / Math.PI, this.afaire1.monangle * 180 / Math.PI + 90) < 90) {
                      this.afaire1.monangle += Math.PI
                      this.afaire1.modan = -Math.PI / 2
                    }
                    this.afaire1.dista = dista * 180 / Math.PI
                    this.after = 'Second'
                    break
                  }
                }
                break
            }

            // POUR LONG
            // sors regle et compas
            // place regle en bas
            // fait compas prend longueur 1
            // fais arc ROUGE
            // sauf si long est donnée sur segtrace avec debut extrem ?

            // POUR angle
            // sors rapporteur
            // trace angle ROUGE

            // POUR appartient
            // fait apparaitre en ROUGE

            // puis INTERSEC
          }
        }
        this.jensuiouPlace = 'go'
        setTimeout(this.placeBien.bind(this), this.tempo)
        break
      case 'RegleMesure0':
        this.a2 = this.afaire1.formule
        this.faisLongCo()
        this.listeAmesure = []
        for (let i = 0; i < this.leslongCo.length; i++) {
          oktrouv = false
          if (this.a2.includes(this.leslongCo[i].nom1)) {
            this.listeAmesure.push({
              ki: this.leslongCo[i].nom1,
              kiv: this.leslongCo[i].nom1,
              val: this.leslongCo[i].long,
              donne: false
            })
            if (this.afaire1.mask !== undefined) {
              for (let j = 0; j < this.afaire1.mask.length; j++) {
                this.listeAmesure[this.listeAmesure.length - 1].kiv = this.listeAmesure[this.listeAmesure.length - 1].kiv.replace(this.afaire1.mask[j][0], this.afaire1.mask[j][1])
              }
            }
            oktrouv = true
          }
          if (this.a2.includes(this.leslongCo[i].nom2) && !oktrouv) {
            this.listeAmesure.push({
              ki: this.leslongCo[i].nom2,
              kiv: this.leslongCo[i].nom2,
              val: this.leslongCo[i].long,
              donne: false
            })
          }
        }
        this.jensuisou = 'RegleMesure1'
        setTimeout(this.animeRepere.bind(this), this.tempo)
        break
      case 'RegleMesure1':
        if (this.mondivBUlle !== undefined) {
          j3pDetruit(this.mondivBUlle)
          this.mondivBUlle = undefined
        }
        a = -1
        for (let i = this.listeAmesure.length - 1; i > -1; i--) {
          if (!this.listeAmesure[i].donne) a = i
        }
        if (a !== -1) {
          this.jensuisou = 'RegleMesure2'
          this.la = a
          setTimeout(this.animeRepere.bind(this), this.tempo)
        } else {
          this.jensuisou = 'RegleMesure7'
          setTimeout(this.animeRepere.bind(this), this.tempo)
        }
        break
      case 'RegleMesure2':
        if (this.afaire1.text !== undefined) {
          tt = this.afaire1.text
          for (let i = 10; i > -1; i--) {
            const regex = new RegExp('§' + i + '§', 'g')
            tt = tt.replace(regex, '$' + this.a2 + '$')
          }
          if (this.afaire1.maskformule !== undefined) {
            tt = tt.replace('£0£', '$' + this.afaire1.maskformule + '$')
          } else {
            tt = tt.replace('£0£', '$' + this.afaire1.formule + '$')
          }
          const aaj = this.recupe(this.listeAmesure[this.la].kiv)
          if ('abcdefghijklmnopqrstuvwxyz'.indexOf(aaj[0]) !== -1 || 'abcdefghijklmnopqrstuvwxyz'.indexOf(aaj[1]) !== -1) {
            this.affBulle(tt)
          } else {
            this.affBulle(tt + '<br> Mesure de ' + aaj)
          }
        }
        pt1 = this.getPointPosition2(this.svgId, this.htmlcoPoint(this.listeAmesure[this.la].ki[0]))
        pt2 = this.getPointPosition2(this.svgId, this.htmlcoPoint(this.listeAmesure[this.la].ki[1]))
        if (pt2.x < pt1.x) {
          b2 = j3pClone(pt1)
          pt1 = j3pClone(pt2)
          pt2 = j3pClone(b2)
        }
        this.mtgAppLecteur.setPointPosition(this.svgId, '#ptrepR1', pt1.x, pt1.y, true)
        this.mtgAppLecteur.setPointPosition(this.svgId, '#ptrepR2', pt2.x, pt2.y, true)
        this.CooBase = this.getPointPosition2(this.svgId, '#ptrepR3')

        try {
          this.monPointBase = this.afaire.nom[1]
        } catch (e) {
          j3pNotify('pour tom', {
            affaire: this.afaire,
            nom: this.afaire.nom,
            dansmulti: this.dansmulti,
            jensuioumulti: this.jensuioumulti
          })
        }
        this.monangle = Math.atan2(pt2.x - pt1.x, pt1.y - pt2.y)
        if (!this.afaire.nom) {
          this.nomarriv = this.htmlcoPoint(this.afaire1.nom[2])
        } else {
          this.nomarriv = this.htmlcoPoint(this.afaire.nom[2])
        }

        this.monangle = this.monangle * 180 / Math.PI + (13 - 90)
        this.mtgAppLecteur.giveFormula2(this.svgId, 'reglangleC', this.monangle)
        this.mtgAppLecteur.updateFigure(this.svgId)
        this.jensuisou = 'RegleMesure30'
        this.avoirX = [pt2, pt1]
        this.bary = this.bari2(this.avoirX)
        this.CooBase.x += this.lXcentre - this.bary.x
        this.CooBase.y += this.lYcentre - this.bary.y
        this.coRx = this.CooBase.x + 70 * Math.cos((this.monangle - 13 + 90) / 180 * Math.PI)
        this.coRy = this.CooBase.y + 70 * Math.sin((this.monangle - 13 + 90) / 180 * Math.PI)
        this.pt2 = { x: pt2.x + this.lXcentre - this.bary.x, y: pt2.y + this.lYcentre - this.bary.y }
        setTimeout(this.placeBienComp.bind(this), this.tempodeb)
        break
      case 'RegleMesure30':
        this.afficheIm('regle', true)
        this.regle2()
        this.cacheTourne()
        this.jensuisou = 'RegleMesure3'
      // eslint-disable-next-line no-fallthrough
      case 'RegleMesure3':
        dx = this.CooBase.x - this.coRx
        dy = this.CooBase.y - this.coRy
        if (Math.abs(dx) > Math.abs(dy)) {
          if (dx > 0) {
            this.coRx = this.coRx + 2
          } else {
            this.coRx = this.coRx - 2
          }
          if (dy > 0) {
            this.coRy = this.coRy + 2 * Math.abs(dy) / Math.abs(dx)
          } else {
            this.coRy = this.coRy - 2 * Math.abs(dy) / Math.abs(dx)
          }
        } else {
          if (dy > 0) {
            this.coRy = this.coRy + 2
          } else {
            this.coRy = this.coRy - 2
          }
          if (dx > 0) {
            this.coRx = this.coRx + 2 * Math.abs(dx) / Math.abs(dy)
          } else {
            this.coRx = this.coRx - 2 * Math.abs(dx) / Math.abs(dy)
          }
        }
        if (Math.abs(dx) < 3 && Math.abs(dy) < 3) {
          this.coRx = this.CooBase.x
          this.coRy = this.CooBase.y
          this.mtgAppLecteur.setPointPosition(this.svgId, this.idPtRegle.pt, this.coRx, this.coRy, true)
          this.jensuiouPlace = 'zo'
          this.jensuisou = 'RegleMesure4'
          this.bary = j3pClone(this.pt2)
          setTimeout(this.placeBienComp.bind(this), this.tempodeb)
          return
        }
        this.mtgAppLecteur.setPointPosition(this.svgId, this.idPtRegle.pt, this.coRx, this.coRy, true)
        setTimeout(this.animeRepere.bind(this), this.tempo)

        break
      case 'RegleMesure4': {
        this.jensuisou = 'RegleMesure5'
        this.jensuiouPlace = 'zo2'
        this.lezoom = 2
        this.zoom = 1
        const fffkl = function () { this.zoomBien(true) }
        setTimeout(fffkl.bind(this), this.tempo)
      }
        break
      case 'RegleMesure5': {
        if (this.mondivBUlle !== undefined) {
          j3pDetruit(this.mondivBUlle)
          this.mondivBUlle = undefined
        }
        tt = this.afaire1.text || ''
        for (let i = 10; i > -1; i--) {
          const regex = new RegExp('§' + i + '§', 'g')
          tt = tt.replace(regex, '$' + this.a2 + '$')
        }
        tt = tt.replace('£0£', '$' + this.afaire1.formule + '$')
        const aaj = this.recupe(this.listeAmesure[this.la].kiv)
        if ('abcdefghijklmnopqrstuvwxyz'.indexOf(aaj[0]) !== -1 || 'abcdefghijklmnopqrstuvwxyz'.indexOf(aaj[1]) !== -1) {
          this.affBulle(tt + '<br> Je peux prendre $' + j3pArrondi(this.listeAmesure[this.la].val, 1) + '$ cm')
        } else {
          this.affBulle(tt + '<br> ' + this.recupe(this.listeAmesure[this.la].kiv) + ' mesure environ $' + j3pArrondi(this.listeAmesure[this.la].val, 1) + '$ cm')
        }
        this.jensuisou = 'RegleMesure6'
        this.listeAmesure[this.la].donne = true
        setTimeout(this.animeRepere.bind(this), this.tempodeb * 2)
      }
        break
      case 'RegleMesure6':
        this.regle2()
        this.afficheIm('regle2', false)
        if (this.zoom !== 1) this.deZoomAll()
        this.jensuisou = 'RegleMesure1'
        setTimeout(this.animeRepere.bind(this), this.tempo)
        break
      case 'RegleMesure7': {
        if (this.mondivBUlle !== undefined) {
          j3pDetruit(this.mondivBUlle)
          this.mondivBUlle = undefined
        }

        tt = 'Une fois les mesures effectuées, <br>'
        let buf = this.afaire1.co1 + this.afaire1.cop
        if (this.afaire1.mask !== undefined) {
          for (let j = 0; j < this.afaire1.mask.length; j++) {
            buf = buf.replace(this.afaire1.mask[j][0], this.afaire1.mask[j][1])
          }
        }
        tt += '$' + buf
        for (let j = this.nomprisProg.length - 1; j > -1; j--) {
          const regex = new RegExp(this.nomprisProg[j].ki, 'g')
          tt = tt.replace(regex, this.nomprisProg[j].com)
        }
        tt += '$ mesure environ $' + j3pArrondi(this.afaire1.laval, 1) + '$ cm'
        tt = tt.replace('£0£', '$' + this.afaire1.formule + '$')
        this.affBulle(tt)
        this.jensuisou = this.after
        this.after = this.after2
        this.after2 = undefined
        this.monangle = this.monanglecons
        setTimeout(this.animeRepere.bind(this), this.tempodeb)
        break
      }
      case 'CompasPrendMesure00':
        if (this.afaire1.text !== undefined) {
          for (let i = 10; i > -1; i--) {
            const regex = new RegExp('§' + i + '§', 'g')
            this.afaire1.text = this.afaire1.text.replace(regex, '$' + this.afaire1.formule + '$')
          }
          this.affBulle(this.afaire1.text)
        }
        this.afaire1.formule = this.afaire1.formule.replace(/ /g, '')
        pt1 = this.getPointPosition2(this.svgId, this.htmlcoPoint(this.afaire1.formule[0]))
        pt2 = this.getPointPosition2(this.svgId, this.htmlcoPoint(this.afaire1.formule[1]))
        this.mtgAppLecteur.setPointPosition(this.svgId, '#compPLA2', pt1.x, pt1.y, true)
        this.mtgAppLecteur.setPointPosition(this.svgId, '#compPLA1', pt2.x, pt2.y, true)
        this.pt2 = this.getPointPosition2(this.svgId, '#compPLA3')
        this.jensuisou = 'CompasPrendMesure0'
        this.avoirX = [pt2, this.getPointPosition2(this.svgId, '#compPLA4'), pt1]
        this.bary = this.bari2(this.avoirX)
        setTimeout(this.placeBienComp.bind(this), this.tempodeb)
        break
      case 'CompasPrendMesure0':
        this.mtgAppLecteur.updateFigure(this.svgId)
        this.mtgAppLecteur.calculate(this.svgId)
        pt1 = this.getPointPosition2(this.svgId, this.htmlcoPoint(this.afaire1.formule[0]))
        pt2 = this.getPointPosition2(this.svgId, this.htmlcoPoint(this.afaire1.formule[1]))
        this.mtgAppLecteur.giveFormula2(this.svgId, 'compangleC', 'compangle', true)
        this.mtgAppLecteur.updateFigure(this.svgId)
        this.mtgAppLecteur.setPointPosition(this.svgId, '#compPLA2', pt1.x, pt1.y, true)
        this.mtgAppLecteur.setPointPosition(this.svgId, '#compPLA1', pt2.x, pt2.y, true)
        this.mtgAppLecteur.updateFigure(this.svgId)
        this.mtgAppLecteur.calculate(this.svgId)
        this.pt2 = this.getPointPosition2(this.svgId, '#compPLA3')
        if (this.after !== 'ComParaRE2' && this.after !== 'ComParaRE3') {
          this.afficheIm('compas', true)
          this.compas()
          this.cacheTourne()
          this.mtgAppLecteur.setPointPosition(this.svgId, '#' + this.idCompPt.pointe, pt1.x + 70, pt1.y + 70, false)
          this.mtgAppLecteur.setPointPosition(this.svgId, '#' + '#' + this.idCompPt.geremine, this.pt2.x + 100, this.pt2.y + 100, false)
          this.delta = { x: this.pt2.x + 100 - pt1.x - 70, y: this.pt2.y + 100 - pt1.y - 70 }
          this.mtgAppLecteur.updateFigure(this.svgId)
          this.jensuisou = 'CompasrendMesure1'
          this.CooBase = pt1
          this.coRx = pt1.x + 70
          this.coRy = pt1.y + 70
          this.cooBaseO = j3pClone(this.CooBase)
        } else {
          this.jensuisou = 'CompasrendMesure3'
        }
        setTimeout(this.animeRepere.bind(this), this.tempodeb)
        break
      case 'CompasrendMesure1':
        dx = this.CooBase.x - this.coRx
        dy = this.CooBase.y - this.coRy
        if (Math.abs(dx) > Math.abs(dy)) {
          if (dx > 0) {
            this.coRx = this.coRx + 4
          } else {
            this.coRx = this.coRx - 2
          }
          if (dy > 0) {
            this.coRy = this.coRy + 4 * Math.abs(dy) / Math.abs(dx)
          } else {
            this.coRy = this.coRy - 4 * Math.abs(dy) / Math.abs(dx)
          }
        } else {
          if (dy > 0) {
            this.coRy = this.coRy + 4
          } else {
            this.coRy = this.coRy - 4
          }
          if (dx > 0) {
            this.coRx = this.coRx + 4 * Math.abs(dx) / Math.abs(dy)
          } else {
            this.coRx = this.coRx - 4 * Math.abs(dx) / Math.abs(dy)
          }
        }
        if (Math.abs(dx) < 4 || Math.abs(dy) < 4) {
          this.coRx = this.CooBase.x
          this.coRy = this.CooBase.y
          this.mtgAppLecteur.setPointPosition(this.svgId, '#' + this.idCompPt.pointe, this.coRx, this.coRy, false)
          this.mtgAppLecteur.setPointPosition(this.svgId, '#' + this.idCompPt.geremine, this.coRx + this.delta.x, this.coRy + this.delta.y, false)
          this.mtgAppLecteur.updateFigure(this.svgId)
          this.jensuiouPlace = 'zo'
          this.jensuisou = 'CompasPrendMesure2'
          this.CooBase = j3pClone(this.pt2)
          this.pt2 = { x: this.coRx, y: this.coRy }
          this.coRx = this.coRx + this.delta.x
          this.coRy = this.coRy + this.delta.y
          setTimeout(this.animeRepere.bind(this), this.tempo)
          return
        }
        this.mtgAppLecteur.setPointPosition(this.svgId, '#' + this.idCompPt.pointe, this.coRx, this.coRy, false)
        this.mtgAppLecteur.setPointPosition(this.svgId, '#' + this.idCompPt.geremine, this.coRx + this.delta.x, this.coRy + this.delta.y, false)
        this.mtgAppLecteur.updateFigure(this.svgId)
        setTimeout(this.animeRepere.bind(this), this.tempo)
        break
      case 'CompasPrendMesure2':
        dx = this.CooBase.x - this.coRx
        dy = this.CooBase.y - this.coRy
        if (Math.abs(dx) > Math.abs(dy)) {
          if (dx > 0) {
            this.coRx = this.coRx + 2
          } else {
            this.coRx = this.coRx - 2
          }
          if (dy > 0) {
            this.coRy = this.coRy + 2 * Math.abs(dy) / Math.abs(dx)
          } else {
            this.coRy = this.coRy - 2 * Math.abs(dy) / Math.abs(dx)
          }
        } else {
          if (dy > 0) {
            this.coRy = this.coRy + 2
          } else {
            this.coRy = this.coRy - 2
          }
          if (dx > 0) {
            this.coRx = this.coRx + 2 * Math.abs(dx) / Math.abs(dy)
          } else {
            this.coRx = this.coRx - 2 * Math.abs(dx) / Math.abs(dy)
          }
        }
        if ((Math.abs(dx) < 3 && Math.abs(dy)) < 3) {
          this.coRx = this.CooBase.x
          this.coRy = this.CooBase.y
          this.mtgAppLecteur.setPointPosition(this.svgId, '#' + this.idCompPt.geremine, this.coRx, this.coRy, true)
          this.mtgAppLecteur.updateFigure(this.svgId)
          this.jensuiouPlace = 'zo'
          this.jensuisou = 'CompasPrendMesure3'

          pt1 = this.getPointPosition2(this.svgId, this.htmlcoPoint(this.monPointBase))
          this.CooBase = pt1
          this.delta = { x: this.coRx - this.pt2.x, y: this.coRy - this.pt2.y }
          this.coRx = 2 * this.pt2.x - this.CooBase.x
          this.coRy = 2 * this.pt2.y - this.CooBase.y
          this.bary.x -= this.pt2.x - this.CooBase.x
          this.bary.y -= this.pt2.y - this.CooBase.y
          this.CooBase = j3pClone(this.cooBaseO)
          if (this.dervi5) {
            this.jensuisou = this.after
            setTimeout(this.animeRepere.bind(this), this.tempo)
            return
          } else {
            const fgt = function () {
              this.setVisible2(this.svgId, '#verouv', false, true)
              this.setVisible2(this.svgId, '#verfer', true, true)
              setTimeout(this.placeBienComp.bind(this), this.tempodeb)
            }
            setTimeout(fgt.bind(this), this.tempodeb)
          }
          return
        }
        this.mtgAppLecteur.setPointPosition(this.svgId, '#' + this.idCompPt.geremine, this.coRx, this.coRy, false)
        this.mtgAppLecteur.updateFigure(this.svgId)
        setTimeout(this.animeRepere.bind(this), this.tempo)
        break
      case 'CompasPrendMesure30':
        this.mtgAppLecteur.giveFormula2(this.svgId, 'compangleC', 'compangle', true)
        this.CooBase = this.getPointPosition2(this.svgId, this.htmlcoPoint(this.afaire1.centre))
        pt1 = this.getPointPosition2(this.svgId, '#' + this.idCompPt.pointe)
        pt2 = this.getPointPosition2(this.svgId, '#' + this.idCompPt.geremine)
        this.coRx = pt1.x
        this.coRy = pt1.y
        this.delta = { x: pt2.x - this.coRx, y: pt2.y - this.coRy }
        this.after = this.after2
        this.monangle = this.afaire1.monangle
        this.jensuisou = 'CompasPrendMesure3'
        setTimeout(this.animeRepere.bind(this), this.tempo)
        break
      case 'CompasPrendMesure3':
        this.aaa = 0
        dx = this.CooBase.x - this.coRx
        dy = this.CooBase.y - this.coRy
        if (Math.abs(dx) > Math.abs(dy)) {
          if (dx > 0) {
            this.coRx = this.coRx + 3
          } else {
            this.coRx = this.coRx - 3
          }
          if (dy > 0) {
            this.coRy = this.coRy + 3 * Math.abs(dy) / Math.abs(dx)
          } else {
            this.coRy = this.coRy - 3 * Math.abs(dy) / Math.abs(dx)
          }
        } else {
          if (dy > 0) {
            this.coRy = this.coRy + 3
          } else {
            this.coRy = this.coRy - 3
          }
          if (dx > 0) {
            this.coRx = this.coRx + 3 * Math.abs(dx) / Math.abs(dy)
          } else {
            this.coRx = this.coRx - 3 * Math.abs(dx) / Math.abs(dy)
          }
        }
        if ((Math.abs(dx) < 4 && Math.abs(dy) < 4)) {
          this.coRx = this.CooBase.x
          this.coRy = this.CooBase.y
          this.mtgAppLecteur.setPointPosition(this.svgId, '#' + this.idCompPt.pointe, this.coRx, this.coRy, false)
          this.mtgAppLecteur.setPointPosition(this.svgId, '#' + this.idCompPt.geremine, this.coRx + this.delta.x, this.coRy + this.delta.y, false)
          this.mtgAppLecteur.updateFigure(this.svgId)
          this.jensuiouPlace = 'zo'
          this.jensuisou = 'CompasPrendMesure4'
          if (this.monangle === undefined) this.monangle = this.afaire1.monangle
          this.monangle = this.monangle * 180 / Math.PI
          pt1 = this.mtgAppLecteur.valueOf(this.svgId, 'compangleC')
          this.avoirX = [this.CooBase, this.PtArrive]
          this.bary = this.bari2(this.avoirX)
          setTimeout(this.placeBienComp.bind(this), this.tempodeb)
          return
        }
        this.mtgAppLecteur.setPointPosition(this.svgId, '#' + this.idCompPt.pointe, this.coRx, this.coRy, false)
        this.mtgAppLecteur.setPointPosition(this.svgId, '#' + this.idCompPt.geremine, this.coRx + this.delta.x, this.coRy + this.delta.y, false)
        this.mtgAppLecteur.updateFigure(this.svgId)

        setTimeout(this.animeRepere.bind(this), this.tempo)
        break
      case 'CompasPrendMesure4':
        pt1 = this.mtgAppLecteur.valueOf(this.svgId, 'compangleC')
        pt2 = this.mtgAppLecteur.valueOf(this.svgId, 'AaAaAa2')
        this.consssser = this.regule(this.monangle - pt2) - this.regule(this.monangle - pt1)
        if ((Math.abs(this.regule(pt2) - this.regule(this.monangle)) <= 2.1) || this.yamulmul) {
          if (!this.yamulmul) {
            this.mtgAppLecteur.giveFormula2(this.svgId, 'compangleC', this.monangle + this.consssser, true)
            this.mtgAppLecteur.updateFigure(this.svgId)
          }

          this.jensuisou = 'CompasPrendMesure5'
          setTimeout(this.animeRepere.bind(this), this.tempodeb)
          return
        }
        if (!this.direct(pt2, this.monangle)) {
          pt1--
          pt1--
        } else {
          pt1++
          pt1++
        }
        this.mtgAppLecteur.giveFormula2(this.svgId, 'compangleC', pt1, true)
        this.mtgAppLecteur.updateFigure(this.svgId)
        setTimeout(this.animeRepere.bind(this), this.tempo)
        break
      case 'CompasPrendMesure5':
        this.afficheIm('point2', true)
        pt1 = this.getPointPosition2(this.svgId, this.idCompPt.mine)
        this.mtgAppLecteur.setPointPosition(this.svgId, this.idImCrayPt, pt1.x, pt1.y, true)
        this.setVisible2(this.svgId, this.idImCray, true, true)
        this.jensuisou = 'CompasPrendMesure6'
        setTimeout(this.animeRepere.bind(this), this.tempodeb)
        break
      case 'CompasPrendMesure6':
        pt1 = this.getPointPosition2(this.svgId, this.idCompPt.mine)
        pt2 = this.getPointPosition2(this.svgId, '#' + this.idCompPt.pointe)
        this.newArc2(pt1, pt2, 'tr1')
        this.jensuisou = 'CompasPrendMesure7'
        setTimeout(this.animeRepere.bind(this), this.tempodeb)
        break
      case 'CompasPrendMesure7':
        this.renvoieCrayon()
        this.afficheIm('point2', false)
        if (this.passmult > 0) {
          this.passmult--
          this.yamulmul = true
          this.jensuisou = 'CompasPrendMesure3'
          this.CooBase = this.getPointPosition2(this.svgId, this.idCompPt.mine)
          const pt = this.getPointPosition2(this.svgId, '#' + this.idCompPt.pointe)
          this.coRx = pt.x
          this.coRy = pt.y
          setTimeout(this.animeRepere.bind(this), this.tempo)
          return
        }
        if (!this.racour) {
          if (this.after === 'ComParaRE1' || this.after === 'ComParaRE2' || this.after === 'ComParaRE3') {
            this.jensuisou = this.after
            setTimeout(this.animeRepere.bind(this), this.tempodeb)
            return
          }
        }
        if (this.after !== 'Second') {
          this.compas()
          this.afficheIm('compas', false)
          this.souvCompasSecond = false
        } else {
          this.souvCompasSecond = true
        }
        this.jensuisou = this.after
        setTimeout(this.animeRepere.bind(this), this.tempo)
        break
      case 'changed':
        break
      // FIRST
      case 'First':
        switch (this.afaire1.t) {
          case 'ap': {
            let pasAgrand = false
            // affiche cond
            if (this.afaire1.text !== '' && this.afaire1.text) this.affBulle(this.recupe(this.afaire1.text))
            this.jensuisou = 'Second'
            let len = this.afaire1.ki.nom
            if (this.afaire1.ki.mask !== undefined) len = this.afaire1.ki.mask
            len = this.recupe(len)
            let i = 0 // on l’utilise en sortie du switch 120 lignes plus bas (pas de for (let i d’ici là)
            switch (this.afaire1.ki.type) {
              case 'segment':
                // cherche dans segment
                fautagrand = false
                for (i = 0; i < this.segCo.length; i++) {
                  if ((this.segCo[i].nom === len) || (this.segCo[i].nom === '[' + len[2] + len[1] + ']')) {
                    this.kiclignote = this.segCo[i].html.ligne
                    dx = i
                    nb = this.ConsSegnb
                    this.lalacool = i <= this.ConsSegnb
                  }
                }
                break
              case 'droite':
                // cherche dans segment
                oktrouv = false
                fautagrand = true
                if (this.afaire1.ki.spe !== undefined) {
                  this.dervi2 = true
                  this.jensuisou = 'ParaRE0'
                  this.LeptAretenir = j3pClone(this.PtArrive)
                  let cont1 = this.retrouveTruc(this.recupe(this.afaire1.ki.odt))
                  if (cont1 === undefined) {
                    // ca je le virerai plus tard, c'est dans une autre branche
                    const p1 = this.retrouvePoint(this.recupe(this.afaire1.ki.odt)[1])
                    const p2 = this.retrouvePoint(this.recupe(this.afaire1.ki.odt)[2])
                    const tatag = this.newTag()
                    this.mtgAppLecteur.addLineAB({
                      a: p1,
                      b: p2,
                      tag: tatag,
                      hidden: true
                    })
                    cont1 = { type: 'droite', tab: [tatag] }
                  }
                  const lapProx = this.retrouveTag(this.recupe(this.afaire1.ki.pass))[0]
                  const unTagperp = this.newTag()
                  const unTagpointInt = this.newTag()
                  this.mtgAppLecteur.addLinePerp({
                    a: lapProx,
                    d: cont1.tab[0],
                    tag: unTagperp,
                    hidden: true,
                    color: 'red'
                  })
                  this.mtgAppLecteur.addIntLineLine({
                    d2: unTagperp,
                    d: cont1.tab[0],
                    tag: unTagpointInt,
                    name: unTagpointInt,
                    hidden: true
                  })
                  this.PtArrive = this.getPointPosition2(this.svgId, unTagpointInt)
                  this.afaire1.pass = this.afaire1.ki.pass
                  this.afaire1.nom = this.afaire1.ki.nom
                  setTimeout(this.animeRepere.bind(this), this.tempo)
                  return
                }

                for (i = 0; i < this.dteco.length; i++) {
                  const acompNom = (this.dteco[i].nom) ? this.recupe(this.dteco[i].nom) : '@'
                  if (!this.dteco[i].viz) continue
                  if ((acompNom === len) || (acompNom === '(' + len[2] + len[1] + ')')) {
                    this.kiclignote = this.dteco[i].html.ligne
                    nb = this.ConsDtenb
                    oktrouv = true
                    fautagrand = false
                    dx = i
                    break
                  }
                }
                if (!oktrouv) {
                  for (i = 0; i < this.segCo.length; i++) {
                    if ((this.segCo[i].nom === len) || (this.segCo[i].nom === '(' + len[2] + len[1] + ')')) {
                      this.kiclignote = this.segCo[i].html.ligne
                      dx = i
                      nb = this.ConsSegnb
                      this.lalacool = i <= this.ConsSegnb
                      break
                    }
                  }
                }
                if (!oktrouv) {
                  for (i = 0; i < this.demiDco.length; i++) {
                    if ((this.demiDco[i].nom === len) || (this.demiDco[i].nom === '(' + len[2] + len[1] + ')')) {
                      this.kiclignote = this.demiDco[i].html.ligne
                      dx = i
                      nb = this.ConsDemiDnb
                      break
                    }
                  }
                }
                if (fautagrand && dx !== undefined) {
                  dista = this.distPtSegCo(dx)
                  if (Math.abs(dista) < 100000) fautagrand = false
                }
                break
              case 'demi-droite':
                oktrouv = false
                fautagrand = true
                for (i = 0; i < this.demiDco.length; i++) {
                  if (!this.demiDco[i].viz) continue
                  const acompNom = (this.demiDco[i].nom) ? this.recupe(this.demiDco[i].nom) : '@'
                  if ((acompNom === len) || (acompNom === '[' + len[2] + len[1] + ')')) {
                    this.kiclignote = this.demiDco[i].html.ligne
                    oktrouv = true
                    fautagrand = false
                    dx = i
                    nb = this.ConsDemiDnb
                    break
                  }
                  if (len.length === 4) {
                    pt1 = this.getPointPosition2(this.svgId, this.demiDco[i].html.point1)
                    pt2 = this.getPointPosition2(this.svgId, this.demiDco[i].html.point2)
                    pt3 = this.getPointPosition2(this.svgId, this.htmlcoPoint(len[1]))
                    pt4 = this.getPointPosition2(this.svgId, this.htmlcoPoint(len[2]))
                    if (this.dist(pt3, pt1) <= 0.1 * this.unite && this.distPtDemiDtebis(pt1, pt4, pt2) <= 0.1 * this.unite) {
                      this.kiclignote = this.demiDco[i].html.ligne
                      oktrouv = true
                      fautagrand = false
                      dx = i
                      nb = this.ConsDtenb
                      break
                    }
                  }
                }
                if (!oktrouv) {
                  for (i = 0; i < this.segCo.length; i++) {
                    if ((this.segCo[i].nom === len) || (this.segCo[i].nom === '[' + len[2] + len[1] + ')')) {
                      this.kiclignote = this.segCo[i].html.ligne
                      dx = i
                      nb = this.ConsSegnb
                      this.lalacool = i <= this.ConsSegnb
                      break
                    }
                  }
                }
                if (fautagrand && dx !== undefined) {
                  dista = this.distPtSegCo(dx)
                  if (Math.abs(dista) < 100000) fautagrand = false
                }
                break
              case 'cercle':
                pasAgrand = true
                fautagrand = false
                for (i = 0; i < this.arcco.length; i++) {
                  if (this.arcco[i].html.nom === len) {
                    this.kiclignote = [this.arcco[i].html.arc1, this.arcco[i].html.arc2, this.arcco[i].html.arc3, this.arcco[i].html.arc4, this.arcco[i].html.arc5, this.arcco[i].html.arc6, this.arcco[i].html.arc7, this.arcco[i].html.arc8, this.arcco[i].html.arc9, this.arcco[i].html.arc10, this.arcco[i].html.arc11, this.arcco[i].html.arc12]
                    dx = i
                    nb = this.ConsArcnb
                  }
                }
                break
            }
            if (dx === undefined) { this.kiclignote = [len[1], len[2]] } else {
              // FIXME ne pas utiliser le i utilisé dans plein de boucles for parmi les 120 lignes qui précèdent, passer par une variable maxTruc que l’on initialise aux bons endroits si vraiment faut se rappeler d’une borne max qq part
              this.clignEff.push({ ki: this.kiclignote, cool: nb > dx })
              this.lalacool = nb > dx
            }
            this.numclignote = 0
            this.fautagrand = fautagrand
            this.faitClignote(pasAgrand)
            break
          }
          case 'long':
            // faut déterminer compas ou regle
            // si ya pas regle grad -> compas
            // si ya pas compas -> regle grad
            // si ya les deux
            // prefere compas si c’est report d’une longueur
            // sinon c’est regle grad
            this.monPointBase = this.afaire1.cop
            if (this.monPointBase.includes('Hz')) this.monPointBase = this.corres(this.monPointBase)
            this.monPointBase0 = this.monPointBase
            this.CooBase = this.getPointPosition2(this.svgId, this.htmlcoPoint(this.monPointBase))

            this.avoir.push(this.CooBase)
            this.afaire1.laval = this.dist(this.CooBase, this.PtArrive)
            this.afaire1.laval = this.afaire1.laval / this.unite
            if (this.afaire1.co1.includes('Hz')) this.afaire1.co1 = this.corres(this.afaire1.co1)
            this.afaire1.formule = this.recupe(this.afaire1.formule)
            if (this.ch === 'regle') {
              if (this.afaire1.affiche === 'formule') {
                this.jensuisou = 'RegleMesure0'
                this.after = 'ReglePlace0'
                this.avoir = ['ù']
                setTimeout(this.animeRepere.bind(this), this.tempo)
              } else {
                this.jensuisou = 'ReglePlace0'
                setTimeout(this.animeRepere.bind(this), this.tempo)
              }
            } else {
              this.avoir.push(this.monPointBase)
              if (this.afaire1.affiche !== 'formule') {
                this.jensuisou = 'CompasRegle0'
                this.after = 'CompasPrendMesure30'
                this.after2 = 'Second'
                this.avoir = ['ù']
                setTimeout(this.animeRepere.bind(this), this.tempo)
              } else {
                if (this.justeUneLongueur(this.afaire1.formule)) {
                  this.avoir.push(this.afaire1.formule[0])
                  this.after = 'Second'
                  this.jensuisou = 'CompasPrendMesure00'
                  setTimeout(this.animeRepere.bind(this), this.tempodeb)
                } else {
                  this.jensuisou = 'RegleMesure0'
                  this.after = 'CompasRegle0'
                  this.after2 = 'Second'
                  setTimeout(this.animeRepere.bind(this), this.tempo)
                }
              }
            }
            break
          case 'ang':
            this.afaire = j3pClone(this.afaire1)
            this.jensuisou = 'Rap0'
            this.after = 'Second'
            if (this.afaire1.cb === '90' && (this.ds.Equerre === true || this.ds.Equerre === 'true')) {
              this.jensuisou = 'PerPEq0'
              this.afaire1.condd = 'perpendiculaire'
              this.afaire1.odt = '(' + this.afaire1.cop + this.afaire1.cop2 + ')'
              this.afaire1.pass = this.afaire1.cop
              pt2 = this.getPointPosition2(this.svgId, this.htmlcoPoint(this.recupe(this.afaire1.cop)))
              pt3 = this.getPointPosition2(this.svgId, this.htmlcoPoint(this.recupe(this.afaire1.cop2)))
              this.afaire1.garde = { x: (this.PtArrive.x - pt2.x), y: (this.PtArrive.y - pt2.y) }
              this.afaire1.ang = Math.atan2(pt3.x - pt2.x, pt2.y - pt3.y)
              this.dervi = true
              this.derviGarde = pt2
              this.fot = true
            }
            setTimeout(this.animeRepere.bind(this), this.tempo)
            break
        }
        break
      case 'Rap0':
        this.afficheIm('rap', true)
        if (!this.afaire1) this.afaire1 = j3pClone(this.afaire2)
        if (this.afaire1.text !== '' && this.afaire1.text) {
          tt = this.afaire1.text.replace('µµ', '$\\widehat{' + this.afaire1.co1 + this.afaire1.cop + this.afaire1.cop2 + '}$')
          if (tt.indexOf('µ0µ') !== -1) tt = this.afaire1.text.replace('µ0µ', '$\\widehat{' + this.afaire1.co1 + this.afaire1.cop + this.afaire1.cop2 + '}$')
          if (tt.indexOf('µ1µ') !== -1) tt = this.afaire1.text.replace('µ1µ', '$\\widehat{' + this.afaire1.co1 + this.afaire1.cop + this.afaire1.cop2 + '}$')
          tt = tt.replace('§0§', this.afaire1.cb)
          this.affBulle(tt)
        }
        this.jensuisou = 'Rap1'
        this.CooBase = this.getPointPosition2(this.svgId, this.htmlcoPoint(this.recupe(this.afaire1.cop)))
        this.coRx = this.CooBase.x + 50 * Math.cos(this.afaire1.monangle + Math.PI / 2)
        this.coRy = this.CooBase.y + 50 * Math.sin(this.afaire1.monangle + Math.PI / 2)
        this.rapporteur()
        this.mtgAppLecteur.setPointPosition(this.svgId, '#' + this.idPRap.pt, this.coRx, this.coRy, true)
        this.cacheTourne()
        this.monangle = 60
        this.delta = {
          x: 82 * this.zoom * Math.cos(this.afaire1.monangle + this.monangle * Math.PI / 180),
          y: -82 * this.zoom * Math.sin(this.afaire1.monangle + this.monangle * Math.PI / 180)
        }
        setTimeout(this.animeRepere.bind(this), this.tempo)
        break
      case 'Rap1':
        dx = this.CooBase.x - this.coRx
        dy = this.CooBase.y - this.coRy
        if (Math.abs(dx) > Math.abs(dy)) {
          if (dx > 0) {
            this.coRx = this.coRx + 2
          } else {
            this.coRx = this.coRx - 2
          }
          if (dy > 0) {
            this.coRy = this.coRy + 2 * Math.abs(dy) / Math.abs(dx)
          } else {
            this.coRy = this.coRy - 2 * Math.abs(dy) / Math.abs(dx)
          }
        } else {
          if (dy > 0) {
            this.coRy = this.coRy + 2
          } else {
            this.coRy = this.coRy - 2
          }
          if (dx > 0) {
            this.coRx = this.coRx + 2 * Math.abs(dx) / Math.abs(dy)
          } else {
            this.coRx = this.coRx - 2 * Math.abs(dx) / Math.abs(dy)
          }
        }
        if ((Math.abs(dx) < 3 && Math.abs(dy) < 3)) {
          this.coRx = this.CooBase.x
          this.coRy = this.CooBase.y
          this.mtgAppLecteur.setPointPosition(this.svgId, '#' + this.idPRap.pt, this.coRx, this.coRy, false)
          this.mtgAppLecteur.setPointPosition(this.svgId, '#raprap10', this.coRx + this.delta.x, this.coRy + this.delta.y, false)
          this.mtgAppLecteur.updateFigure(this.svgId)

          this.jensuisou = 'Rap2'
          setTimeout(this.animeRepere.bind(this), this.tempo)
          return
        }
        this.mtgAppLecteur.setPointPosition(this.svgId, '#' + this.idPRap.pt, this.coRx, this.coRy, false)
        this.mtgAppLecteur.setPointPosition(this.svgId, '#raprap10', this.coRx + this.delta.x, this.coRy + this.delta.y, false)
        this.mtgAppLecteur.updateFigure(this.svgId)
        setTimeout(this.animeRepere.bind(this), this.tempo)
        break
      case 'Rap2':
        this.monangle -= 2
        if (this.monangle === 0) {
          this.mtgAppLecteur.setPointPosition(this.svgId, '#raprap10', this.coRx + 82 * this.zoom * Math.cos(this.afaire1.monangle), this.coRy - 82 * this.zoom * Math.sin(this.afaire1.monangle), true)

          this.jensuisou = 'Rap3'
          setTimeout(this.animeRepere.bind(this), this.tempodeb)
          return
        }
        this.mtgAppLecteur.setPointPosition(this.svgId, '#raprap10', this.coRx + 82 * this.zoom * Math.cos(this.afaire1.monangle + this.monangle * Math.PI / 180), this.coRy - 82 * this.zoom * Math.sin(this.afaire1.monangle + this.monangle * Math.PI / 180), true)
        setTimeout(this.animeRepere.bind(this), this.tempo)
        break
      case 'Rap3':
        this.afficheIm('point2', true)
        this.setVisible2(this.svgId, this.idImCray, true, true)
        this.mtgAppLecteur.setPointPosition(this.svgId, this.idImCrayPt, this.coRx + 165 * this.zoom * Math.cos(this.afaire1.monangle + this.afaire1.modan), this.coRy - 165 * this.zoom * Math.sin(this.afaire1.monangle + this.afaire1.modan), false)
        this.mtgAppLecteur.giveFormula2(this.svgId, 'anglecray', (this.afaire1.monangle + this.afaire1.modan) * 180 / Math.PI + 220, false)
        this.monangle = 0
        this.mtgAppLecteur.updateFigure(this.svgId)

        setTimeout(this.animeRepere.bind(this), this.tempodeb)
        this.jensuisou = 'Rap4'
        break
      case 'Rap4':
        if (this.afaire1.modan > 0) { this.monangle-- } else { this.monangle++ }
        if (this.distangle((this.afaire1.monangle + this.monangle * Math.PI / 180) * 180 / Math.PI, this.afaire1.dista - 90 + this.afaire1.modan * 180 / Math.PI) < 1) {
          this.jensuisou = 'Rap5'
          this.mtgAppLecteur.setPointPosition(this.svgId, this.idImCrayPt, this.coRx + 165 * this.zoom * Math.cos((this.afaire1.dista + 90) * Math.PI / 180), this.coRy - 165 * this.zoom * Math.sin((this.afaire1.dista + 90) * Math.PI / 180), false)
          this.mtgAppLecteur.giveFormula2(this.svgId, 'anglecray', (this.dista + 90) + 220, false)
          this.mtgAppLecteur.updateFigure(this.svgId)
          setTimeout(this.animeRepere.bind(this), this.tempodeb)
          return
        }
        this.mtgAppLecteur.setPointPosition(this.svgId, this.idImCrayPt, this.coRx + 165 * this.zoom * Math.cos(this.afaire1.monangle + this.monangle * Math.PI / 180 + this.afaire1.modan), this.coRy - 165 * this.zoom * Math.sin(this.afaire1.monangle + this.afaire1.modan + this.monangle * Math.PI / 180), false)
        this.mtgAppLecteur.giveFormula2(this.svgId, 'anglecray', (this.afaire1.monangle + this.afaire1.modan) * 180 / Math.PI + this.monangle + 220, false)

        this.mtgAppLecteur.updateFigure(this.svgId)
        setTimeout(this.animeRepere.bind(this), this.tempo)
        break
      case 'Rap5':
        pt1 = this.getPointPosition2(this.svgId, this.idPRap.pt)
        pt2 = this.getPointPosition2(this.svgId, this.idImCrayPt)
        this.newDDte2(pt1.x, pt1.y, pt2.x, pt2.y, this.afaire1.nom)
        this.jensuisou = 'Rap6'
        setTimeout(this.animeRepere.bind(this), this.tempodeb)
        break
      case 'Rap6':
        this.afficheIm('point2', false)
        this.rapporteur()
        this.afficheIm('rap', false)
        this.jensuisou = 'Rap7'
        this.renvoieCrayon()
        setTimeout(this.animeRepere.bind(this), this.tempodeb)
        break
      case 'Rap7':
        this.jensuisou = this.after
        if (this.jensuisou !== 'prog') {
          this.jensuiouPlace = 'go'
          setTimeout(this.placeBien.bind(this), this.tempo)
        } else {
          setTimeout(this.animeRepere.bind(this), this.tempo)
        }
        break
      // SECOND
      case 'Second':
        switch (this.afaire2.t) {
          case 'ap': {
            if (this.souvCompasSecond) {
              this.compas()
              this.afficheIm('compas', false)
              this.souvCompasSecond = false
            }
            // affiche cond
            if (this.afaire2.text !== '' && this.afaire2.text) this.affBulle(this.recupe(this.afaire2.text))
            this.jensuisou = 'pointPlace0'
            let len = this.afaire2.ki.nom
            if (this.afaire2.ki.mask !== undefined) len = this.afaire2.ki.mask
            len = this.recupe(len)
            switch (this.afaire2.ki.type) {
              case 'segment':
                // cherche dans segment
                for (let i = 0; i < this.segCo.length; i++) {
                  if ((this.segCo[i].nom === len) || (this.segCo[i].nom === '[' + len[2] + len[1] + ']')) {
                    this.kiclignote = this.segCo[i].html.ligne
                    nb = this.ConsSegnb
                    dx = i
                    break
                  }
                }
                break
              case 'droite':
                oktrouv = false
                fautagrand = true
                if (this.afaire2.ki.spe !== undefined) {
                  this.dervi3 = true
                  this.jensuisou = 'ParaRE0'
                  this.LeptAretenir = j3pClone(this.PtArrive)
                  this.PtArrive = this.getPointPosition2(this.svgId, this.htmlcoPoint(this.recupe(this.afaire2.ki.pass)))
                  this.afaire2.pass = this.afaire2.ki.pass
                  this.afaire2.nom = this.afaire2.ki.nom
                  this.afaire1 = j3pClone(this.afaire2)
                  setTimeout(this.animeRepere.bind(this), this.tempo)
                  return
                } else {
                  for (let i = 0; i < this.dteco.length; i++) {
                    const acompNom = (this.dteco[i].nom) ? this.recupe(this.dteco[i].nom) : '@'
                    if (!this.dteco[i].viz) continue
                    if ((acompNom === len) || (acompNom === '(' + len[2] + len[1] + ')')) {
                      this.kiclignote = this.dteco[i].html.ligne
                      oktrouv = true
                      fautagrand = false
                      dx = i
                      nb = this.ConsDtenb
                      break
                    }
                  }
                  if (!oktrouv) {
                    for (let i = 0; i < this.segCo.length; i++) {
                      if ((this.segCo[i].nom === len) || (this.segCo[i].nom === '(' + len[2] + len[1] + ')')) {
                        this.kiclignote = this.segCo[i].html.ligne
                        dx = i
                        nb = this.ConsSegnb
                        break
                      }
                    }
                  }
                  if (!oktrouv) {
                    for (let i = 0; i < this.demiDco.length; i++) {
                      if ((this.demiDco[i].nom === len) || (this.demiDco[i].nom === '(' + len[2] + len[1] + ')')) {
                        this.kiclignote = this.demiDco[i].html.ligne
                        dx = i
                        nb = this.ConsDemiDnb
                        break
                      }
                    }
                  }
                  if (fautagrand && dx !== undefined) {
                    dista = this.distPtSegCo(dx)
                    if (Math.abs(dista) < 100000) fautagrand = false
                  }
                }
                break
              case 'cercle':
                fautagrand = false
                for (let i = 0; i < this.arcco.length; i++) {
                  if (this.arcco[i].nom === len) {
                    this.kiclignote = this.arcco[i].html.arc1
                    dx = i
                    nb = this.ConsArcnb
                  }
                }
                break
              case 'demi-droite':
                oktrouv = false
                fautagrand = true
                for (let i = 0; i < this.demiDco.length; i++) {
                  if (!this.demiDco[i].viz) continue
                  if ((this.demiDco[i].nom === len) || (this.demiDco[i].nom === '[' + len[2] + len[1] + ')')) {
                    this.kiclignote = this.demiDco[i].html.ligne
                    oktrouv = true
                    fautagrand = false
                    dx = i
                    nb = this.ConsDemiDnb
                    break
                  }
                  if (len.length === 4) {
                    pt1 = this.getPointPosition2(this.svgId, this.demiDco[i].html.point1)
                    pt2 = this.getPointPosition2(this.svgId, this.demiDco[i].html.point2)
                    pt3 = this.getPointPosition2(this.svgId, this.htmlcoPoint(len[1]))
                    pt4 = this.getPointPosition2(this.svgId, this.htmlcoPoint(len[2]))
                    if (this.dist(pt3, pt1) <= 0.1 * this.unite && this.distPtDemiDtebis(pt1, pt4, pt2) <= 0.1 * this.unite) {
                      this.kiclignote = this.demiDco[i].html.ligne
                      oktrouv = true
                      fautagrand = false
                      dx = i
                      nb = this.ConsDtenb
                      break
                    }
                  }
                }
                if (!oktrouv) {
                  for (let i = 0; i < this.segCo.length; i++) {
                    if ((this.segCo[i].nom === len) || (this.segCo[i].nom === '[' + len[2] + len[1] + ')')) {
                      this.kiclignote = this.segCo[i].html.ligne
                      dx = i
                      nb = this.ConsSegnb
                      break
                    }
                  }
                }
                if (fautagrand && dx !== undefined) {
                  dista = this.distPtSegCo(dx)
                  if (Math.abs(dista) < 100000) fautagrand = false
                }
                break
            }
            if (dx === undefined) { this.kiclignote = [len[1], len[2]] } else {
              // FIXME ne pas utiliser le i utilisé dans plein de boucles for parmi les 120 lignes qui précèdent, passer par une variable maxTruc que l’on initialise aux bons endroits si vraiment faut se rappeler d’une borne max qq part
              this.clignEff.push({ ki: this.kiclignote, cool: nb > dx })
              this.lalacool = nb > dx
            }
            this.numclignote = 0
            this.fautagrand = fautagrand
            this.faitClignote()
            break
          }
          case 'long':
            this.monangle = undefined
            // faut déterminer compas ou regle
            // si ya pas regle grad -> compas
            // si ya pas compas -> regle grad
            // si ya les deux
            // prefere compas si c’est report d’une longueur
            // sinon c’est regle grad

            this.monPointBase = this.afaire2.cop
            if (this.monPointBase.includes('Hz')) this.monPointBase = this.corres(this.monPointBase)
            this.monPointBase0 = this.monPointBase
            this.CooBase = this.getPointPosition2(this.svgId, this.htmlcoPoint(this.monPointBase))

            this.avoir.push(this.CooBase)
            this.afaire1 = j3pClone(this.afaire2)
            try {
              this.afaire1.laval = this.dist(this.CooBase, this.PtArrive)
            } catch (e) {
              j3pNotify('pour tom', {
                CooBase: this.CooBase,
                PtArrive: this.PtArrive,
                cop: this.afaire2.cop,
                monPointBase: this.monPointBase
              })
              this.jensuisou = 'fini'
              setTimeout(this.animeRepere.bind(this), this.tempo)
              return
            }
            this.afaire1.laval = this.afaire1.laval / this.unite
            if (this.afaire1.co1 === undefined) this.afaire1.co1 = this.afaire1.cop
            if (this.afaire1.co1.includes('Hz')) this.afaire1.co1 = this.corres(this.afaire1.co1)
            for (let j = this.nomprisProg.length - 1; j > -1; j--) {
              const regex = new RegExp(this.nomprisProg[j].ki, 'g')
              this.afaire1.formule = this.afaire1.formule.replace(regex, this.nomprisProg[j].com)
            }
            if (this.ch === 'regle') {
              if (this.souvCompasSecond) {
                this.compas()
                this.afficheIm('compas', false)
                this.souvCompasSecond = false
              }
              if (this.afaire1.affiche === 'formule') {
                this.jensuisou = 'RegleMesure0'
                this.after = 'ReglePlace0'
                this.avoir = ['ù']
                setTimeout(this.animeRepere.bind(this), this.tempo)
              } else {
                this.jensuisou = 'ReglePlace0'
                setTimeout(this.animeRepere.bind(this), this.tempo)
              }
            } else {
              this.souvCompasSecond = false
              this.avoir.push(this.monPointBase)
              if (this.afaire1.centre === undefined) this.afaire1.centre = this.afaire1.cop
              pt1 = this.getPointPosition2(this.svgId, this.htmlcoPoint(this.afaire1.centre))
              this.afaire1.monangle = Math.PI / 2 - Math.atan2(this.PtArrive.x - pt1.x, pt1.y - this.PtArrive.y)
              if (this.afaire1.affiche !== 'formule') {
                this.jensuisou = 'CompasRegle0'
                this.after = 'CompasPrendMesure30'
                this.after2 = 'pointPlace0'
                this.avoir = ['ù']
                setTimeout(this.animeRepere.bind(this), this.tempo)
              } else {
                if (this.justeUneLongueur(this.afaire1.formule) === 'oui') {
                  this.avoir.push(this.afaire1.formule[0])
                  this.after = 'pointPlace0'
                  this.jensuisou = 'CompasPrendMesure00'
                  if (this.racour) this.jensuisou = 'CompasPrendMesure3'
                  setTimeout(this.animeRepere.bind(this), this.tempo)
                } else if (this.afaire1.estmult) {
                  this.passmult = this.afaire1.lemul - 1
                  this.avoir.push(this.afaire1.formule[0])
                  this.after = 'pointPlace0'
                  this.jensuisou = 'CompasPrendMesure00'
                  if (this.racour) this.jensuisou = 'CompasPrendMesure3'
                  setTimeout(this.animeRepere.bind(this), this.tempo)
                } else if (this.justeUneLongueur(this.afaire1.formule)) {
                  this.passmult = Number(this.justeUneLongueur(this.afaire1.formule)) - 1
                  this.avoir.push(this.lepremsLettre(this.afaire1.formule))
                  this.after = 'pointPlace0'
                  this.jensuisou = 'CompasPrendMesure00'
                  this.afaire1.formule = this.afaire1.formule.replace(/0|1|2|3|4|5|6|7|8|9/g, '').replace(/ /g, '').replace('\\times', '')
                  if (this.racour) this.jensuisou = 'CompasPrendMesure3'
                  setTimeout(this.animeRepere.bind(this), this.tempo)
                } else {
                  this.jensuisou = 'RegleMesure0'
                  this.after = 'CompasRegle0'
                  this.after2 = 'pointPlace0'
                  setTimeout(this.animeRepere.bind(this), this.tempo)
                }
              }
            }
            break
          case 'ang': {
            if (this.souvCompasSecond) {
              this.compas()
              this.afficheIm('compas', false)
              this.souvCompasSecond = false
            }
            this.afaire1 = j3pClone(this.afaire2)
            this.avoir = [this.recupe(this.afaire1.cop), this.PtArrive]
            this.jensuisou = 'Rap0'
            this.after = 'pointPlace0'
            const pt2 = this.getPointPosition2(this.svgId, this.htmlcoPoint(this.recupe(this.afaire1.cop)))
            const pt3 = this.getPointPosition2(this.svgId, this.htmlcoPoint(this.recupe(this.afaire1.cop2)))
            if (this.afaire1.cb === '90' && (this.ds.Equerre === true || this.ds.Equerre === 'true')) {
              this.jensuisou = 'PerPEq0'
              this.afaire1.condd = 'perpendiculaire'
              this.afaire1.odt = '(' + this.afaire1.cop + this.afaire1.cop2 + ')'
              this.afaire1.pass = this.afaire1.cop
              this.afaire1.garde = { x: (this.PtArrive.x - pt2.x), y: (this.PtArrive.y - pt2.y) }
              this.afaire1.ang = Math.atan2(pt3.x - pt2.x, pt2.y - pt3.y)
              this.dervi = true
              this.derviGarde = pt2
            }
            if (!this.yaseg(pt2, pt3, false, undefined)) {
              this.after = this.jensuisou
              this.jensuisou = 'demidroite'
              this.afaire = { condd: '', pass: this.afaire2.cop2, pass2: this.afaire2.cop }
              this.after2 = 'pointPlace0'
            }
            this.jensuiouPlace = 'go'
            setTimeout(this.placeBien.bind(this), this.tempo)
            break
          }
        }
        break
      case 'pointPlace0':
        this.afficheIm('point', true)
        this.jensuisou = 'pointPlace'
        this.nbpointsLIbres++
        this.affichePtbalade()
        setTimeout(this.animeRepere.bind(this), this.tempodeb)
        break
      case 'pointPlace':
        if (!this.PtArrive) {
          j3pNotify('pour tom ya pas PtArrive', {
            Programme: this.Programme,
            comptProg: this.comptProg
          })
          return
        }
        dx = this.PtArrive.x - this.ptplace.x
        dy = this.PtArrive.y - this.ptplace.y
        if (Math.abs(dx) > Math.abs(dy)) {
          if (dx > 0) {
            this.ptplace.x = this.ptplace.x + 3
          } else {
            this.ptplace.x = this.ptplace.x - 3
          }
          if (dy > 0) {
            this.ptplace.y = this.ptplace.y + 3 * Math.abs(dy) / Math.abs(dx)
          } else {
            this.ptplace.y = this.ptplace.y - 3 * Math.abs(dy) / Math.abs(dx)
          }
        } else {
          if (dy > 0) {
            this.ptplace.y = this.ptplace.y + 3
          } else {
            this.ptplace.y = this.ptplace.y - 3
          }
          if (dx > 0) {
            this.ptplace.x = this.ptplace.x + 3 * Math.abs(dx) / Math.abs(dy)
          } else {
            this.ptplace.x = this.ptplace.x - 3 * Math.abs(dx) / Math.abs(dy)
          }
        }
        if (Math.abs(dx) < 3 || Math.abs(dy) < 3) {
          this.ptplace.x = this.PtArrive.x
          this.ptplace.y = this.PtArrive.y
          if (this.dansmulti) {
            this.jensuioumulti++
          } else {
            this.comptProg++
          }
          this.jensuisou = 'prog'
          this.cahchePointballade()
          this.metNewPoint()
          setTimeout(this.animeRepere.bind(this), this.tempodeb)
          return
        }
        this.deplacePtbalade()
        setTimeout(this.animeRepere.bind(this), this.tempo)
        break
      case 'ReglePlace0':
        this.monangle = this.monanglecons
        if (this.afaire1.text !== '' && this.afaire1.text !== undefined) {
          tt = this.afaire1.text
          if (this.afaire1.laval !== undefined) {
            for (let i = 10; i > -1; i--) {
              const regex = new RegExp('§' + i + '§', 'g')
              tt = tt.replace(regex, '$' + String(j3pArrondi(this.afaire1.laval, 1)).replace('.', ',') + '$')
            }
          } else {
            for (let i = 10; i > -1; i--) {
              const regex = new RegExp('§' + i + '§', 'g')
              tt = tt.replace(regex, '$' + this.afaire1.cb.replace('.', ',') + '$ cm')
            }
          }
          this.affBulle(tt)
        }
        this.afficheIm('regle', true)
        this.jensuisou = 'ReglePlace'
        this.regle2()
        this.cacheTourne()
        this.monPointBase = this.monPointBase0

        pt1 = this.getPointPosition2(this.svgId, this.htmlcoPoint(this.monPointBase))
        pt2 = j3pClone(this.PtArrive)

        if (pt2.x < pt1.x) {
          this.CooBase = j3pClone(pt1)
          pt1 = j3pClone(pt2)
          pt2 = j3pClone(this.CooBase)
        }

        this.monangle = Math.atan2(pt2.x - pt1.x, pt1.y - pt2.y)
        this.mtgAppLecteur.setPointPosition(this.svgId, '#ptrepR1', pt1.x, pt1.y, true)
        this.mtgAppLecteur.setPointPosition(this.svgId, '#ptrepR2', pt2.x, pt2.y, true)
        this.CooBase = this.getPointPosition2(this.svgId, '#ptrepR3')

        this.monangle = this.monangle * 180 / Math.PI + (13 - 90)

        this.mtgAppLecteur.giveFormula2(this.svgId, 'reglangleC', this.monangle)
        this.mtgAppLecteur.updateFigure(this.svgId)
        this.jensuisou = 'ReglePlace'
        this.bary = { x: (pt1.x + pt2.x) / 2, y: (pt1.y + pt2.y) / 2 }
        this.CooBase.x += this.lXcentre - this.bary.x
        this.CooBase.y += this.lYcentre - this.bary.y
        this.coRx = this.CooBase.x + 70 * Math.cos((this.monangle - 13 + 90) / 180 * Math.PI)
        this.coRy = this.CooBase.y + 70 * Math.sin((this.monangle - 13 + 90) / 180 * Math.PI)
        this.placeBienComp()
        break
      case 'ReglePlace':
        dx = this.CooBase.x - this.coRx
        dy = this.CooBase.y - this.coRy
        if (Math.abs(dx) > Math.abs(dy)) {
          if (dx > 0) {
            this.coRx = this.coRx + 2
          } else {
            this.coRx = this.coRx - 2
          }
          if (dy > 0) {
            this.coRy = this.coRy + 2 * Math.abs(dy) / Math.abs(dx)
          } else {
            this.coRy = this.coRy - 2 * Math.abs(dy) / Math.abs(dx)
          }
        } else {
          if (dy > 0) {
            this.coRy = this.coRy + 2
          } else {
            this.coRy = this.coRy - 2
          }
          if (dx > 0) {
            this.coRx = this.coRx + 2 * Math.abs(dx) / Math.abs(dy)
          } else {
            this.coRx = this.coRx - 2 * Math.abs(dx) / Math.abs(dy)
          }
        }
        if ((Math.abs(dx) < 3 && Math.abs(dy) < 3)) {
          this.coRx = this.CooBase.x
          this.coRy = this.CooBase.y
          this.mtgAppLecteur.setPointPosition(this.svgId, this.idPtRegle.pt, this.coRx, this.coRy, true)

          this.jensuiouPlace = 'zo'
          this.jensuisou = 'ReglePlace3'
          setTimeout(this.zoomBien.bind(this), this.tempodeb)
          return
        }
        this.mtgAppLecteur.setPointPosition(this.svgId, this.idPtRegle.pt, this.coRx, this.coRy, true)
        setTimeout(this.animeRepere.bind(this), this.tempo)
        break
      case 'ReglePlace3':
        this.fautvireRegle = true
        this.afficheIm('point', true)
        this.jensuisou = 'pointPlace'
        this.affichePtbalade()
        setTimeout(this.animeRepere.bind(this), this.tempo)
        break
      case 'segment': {
        while (this.afaire.nom.includes('Hz')) {
          for (let i = this.nomprisProg.length - 1; i > -1; i--) {
            const regex = new RegExp(this.nomprisProg[i].ki, 'g')
            this.afaire.nom = this.afaire.nom.replace(regex, this.nomprisProg[i].com)
          }
        }
        // test si il est pas déjà tracé
        const ext1 = this.getPointPosition2(this.svgId, this.htmlcoPoint(this.afaire.nom[1]))
        const ext2 = this.getPointPosition2(this.svgId, this.htmlcoPoint(this.afaire.nom[2]))
        const ret = this.yasegCo(ext1, ext2)
        if (ret === true) {
          if (this.dansmulti) {
            this.jensuioumulti++
          } else {
            this.comptProg++
          }
          this.jensuisou = 'prog'
          setTimeout(this.animeRepere.bind(this), this.tempo)
          return
        }
        this.jensuisou = 'RegleSegPlace0'
        this.jensuiouPlace = 'go'
        this.avoir = [this.afaire.nom[1], this.afaire.nom[2]]
        this.nomPbuf = this.afaire.nom
        this.coPointille = this.afaire.pointille
        setTimeout(this.placeBien.bind(this), this.tempodeb)
        return
      }
      case 'RegleSegPlace0':
        this.afficheIm('regle', true)
        this.jensuisou = 'RegleSegPlace'
        this.regle2()
        pt1 = this.getPointPosition2(this.svgId, this.htmlcoPoint(this.afaire.nom[1]))
        pt2 = this.getPointPosition2(this.svgId, this.htmlcoPoint(this.afaire.nom[2]))
        if (pt1.x < pt2.x) {
          this.monPointBase = this.afaire.nom[1]
          this.monangle = Math.atan2(pt2.x - pt1.x, pt1.y - pt2.y)
          this.nomarriv = this.htmlcoPoint(this.afaire.nom[2])
        } else {
          this.monPointBase = this.afaire.nom[2]
          this.monangle = Math.atan2(pt1.x - pt2.x, pt2.y - pt1.y)
          this.nomarriv = this.htmlcoPoint(this.afaire.nom[1])
        }
        this.monangle = this.monangle * 180 / Math.PI + (13.5 - 90)

        this.mtgAppLecteur.giveFormula2(this.svgId, 'reglangleC', this.monangle)
        this.mtgAppLecteur.updateFigure(this.svgId)
        this.CooBase = this.getPointPosition2(this.svgId, this.htmlcoPoint(this.monPointBase))
        this.coRx = this.CooBase.x + 70 * Math.cos((this.monangle - 13 + 90) / 180 * Math.PI)
        this.coRy = this.CooBase.y + 70 * Math.sin((this.monangle - 13 + 90) / 180 * Math.PI)
        this.cacheTourne()
      // eslint-disable-next-line no-fallthrough
      case 'RegleSegPlace':
        dx = this.CooBase.x - this.coRx
        dy = this.CooBase.y - this.coRy
        if (Math.abs(dx) > Math.abs(dy)) {
          if (dx > 0) {
            this.coRx = this.coRx + 2
          } else {
            this.coRx = this.coRx - 2
          }
          if (dy > 0) {
            this.coRy = this.coRy + 2 * Math.abs(dy) / Math.abs(dx)
          } else {
            this.coRy = this.coRy - 2 * Math.abs(dy) / Math.abs(dx)
          }
        } else {
          if (dy > 0) {
            this.coRy = this.coRy + 2
          } else {
            this.coRy = this.coRy - 2
          }
          if (dx > 0) {
            this.coRx = this.coRx + 2 * Math.abs(dx) / Math.abs(dy)
          } else {
            this.coRx = this.coRx - 2 * Math.abs(dx) / Math.abs(dy)
          }
        }
        if (Math.abs(dx) < 3 || Math.abs(dy) < 3) {
          this.coRx = this.CooBase.x + this.unite / 10 * Math.cos(this.monangle)
          this.coRy = this.CooBase.y - this.unite / 10 * Math.sin(this.monangle)
          this.fautvireRegle = true
          this.afficheIm('point2', true)
          this.jensuisou = 'TraceSeg0'
          this.mtgAppLecteur.setPointPosition(this.svgId, this.idPtRegle.pt, this.coRx, this.coRy, true)
          setTimeout(this.animeRepere.bind(this), this.tempodeb)
          return
        }
        this.mtgAppLecteur.setPointPosition(this.svgId, this.idPtRegle.pt, this.coRx, this.coRy, true)
        setTimeout(this.animeRepere.bind(this), this.tempo)
        break
      case 'TraceSeg0':
        this.jensuisou = 'TraceSeg'
        this.PtArrive = this.getPointPosition2(this.svgId, this.nomarriv)
        this.ptplace = { x: this.coRx, y: this.coRy }
        // affichesegment de crayon a cryon 2
        this.newSeg()
        this.setVisible2(this.svgId, this.idImCray, true, true)
        this.mtgAppLecteur.setPointPosition(this.svgId, this.idImCrayPt, this.ptplace.x, this.ptplace.y, true)
        setTimeout(this.animeRepere.bind(this), this.tempodeb)
        this.setVisible2(this.svgId, '#da', false, true)
        this.setVisible2(this.svgId, '#d1', false, true)
        this.setVisible2(this.svgId, '#d2', false, true)
        this.setVisible2(this.svgId, '#d3', false, true)
        this.setVisible2(this.svgId, '#d4', false, true)
        this.setVisible2(this.svgId, '#dz', false, true)
        this.setVisible2(this.svgId, '#laddregle', false, true)
        break
      case 'TraceSeg':
        // deplace pointseg jsqua fini
        dx = this.PtArrive.x - this.ptplace.x
        dy = this.PtArrive.y - this.ptplace.y
        if (Math.abs(dx) > Math.abs(dy)) {
          if (dx > 0) {
            this.ptplace.x = this.ptplace.x + 3
          } else {
            this.ptplace.x = this.ptplace.x - 3
          }
          if (dy > 0) {
            this.ptplace.y = this.ptplace.y + 3 * Math.abs(dy) / Math.abs(dx)
          } else {
            this.ptplace.y = this.ptplace.y - 3 * Math.abs(dy) / Math.abs(dx)
          }
        } else {
          if (dy > 0) {
            this.ptplace.y = this.ptplace.y + 3
          } else {
            this.ptplace.y = this.ptplace.y - 3
          }
          if (dx > 0) {
            this.ptplace.x = this.ptplace.x + 3 * Math.abs(dx) / Math.abs(dy)
          } else {
            this.ptplace.x = this.ptplace.x - 3 * Math.abs(dx) / Math.abs(dy)
          }
        }
        if ((Math.abs(dx) < 3 && Math.abs(dy) < 3)) {
          this.ptplace.x = this.PtArrive.x
          this.ptplace.y = this.PtArrive.y
          this.setVisible2(this.svgId, this.idImCray, false, true)
          if (this.dansmulti) {
            this.jensuioumulti++
          } else {
            this.comptProg++
          }
          this.jensuisou = 'prog'
          this.afficheIm('point2', false)
          this.setVisible2(this.svgId, this.idImCray, false, true)
          this.mtgAppLecteur.setPointPosition(this.svgId, this.idImCrayPt, this.ptplace.x, this.ptplace.y, true)
          this.mtgAppLecteur.setPointPosition(this.svgId, this.segCo[this.segCo.length - 1].html.point2, this.ptplace.x, this.ptplace.y, true)
          setTimeout(this.animeRepere.bind(this), this.tempodeb)
          return
        }
        this.mtgAppLecteur.setPointPosition(this.svgId, this.idImCrayPt, this.ptplace.x, this.ptplace.y, true)
        this.mtgAppLecteur.setPointPosition(this.svgId, this.segCo[this.segCo.length - 1].html.point2, this.ptplace.x, this.ptplace.y, true)
        setTimeout(this.animeRepere.bind(this), this.tempo)
        break
      case 'cercle':
        this.PtArrive = {}
        this.afaire1 = this.afaire
        for (let j = this.nomprisProg.length - 1; j > -1; j--) {
          const regex = new RegExp(this.nomprisProg[j].ki, 'g')
          this.afaire1.centre = this.afaire1.centre.replace(regex, this.nomprisProg[j].com)
        }
        if (this.afaire.mode === 'rayon') {
          this.afaire.laval = this.calculeFormule(this.afaire.rayon)
          if (this.afaire.affiche === 'formule') {
            this.afaire1.formule = this.afaire.rayon
            this.afaire1.formule = this.recupe(this.afaire1.formule)
            this.afaire1.co1 = '$le  rayon$ '
            this.afaire1.cop = ''
            // determine si que une longueur
            // si non
            if (this.justeUneLongueur(this.afaire1.formule) !== 'oui') {
              this.jensuisou = 'RegleMesure0'
              this.after = 'CompasRegle0'
            } else {
              this.jensuisou = 'CompasPrendMesure00'
              this.after = 'CompasRegle5'
              this.monPointBase = this.afaire1.centre
              this.dervi5 = true
            }
            // si oui
          } else {
            this.jensuisou = 'CompasRegle0'
            this.after = 'CompasRegle5'
          }
        } else {
          for (let j = this.nomprisProg.length - 1; j > -1; j--) {
            const regex = new RegExp(this.nomprisProg[j].ki, 'g')
            this.afaire1.par = this.afaire1.par.replace(regex, this.nomprisProg[j].com)
          }
          this.jensuisou = 'cerclePas'
        }
        setTimeout(this.animeRepere.bind(this), this.tempo)
        break
      case 'CompasRegle0':
        // fait apparaitre regle en bas
        if (this.mondivBUlle !== undefined) {
          j3pDetruit(this.mondivBUlle)
          this.mondivBUlle = undefined
        }
        if (this.afaire1.type !== 'cercle') {
          this.affBulle('Je prends $' + String(j3pArrondi(this.calculeFormule(this.afaire1.cb), 1)).replace('.', ',') + '$ cm pour $' + this.afaire1.centre + this.afaire1.co1 + '$')
        } else {
          this.affBulle('Je prends $' + String(this.afaire1.rayon).replace('.', ',') + '$ cm pour rayon.')
        }
        this.recentre()
        this.afficheIm('regle', true)
        this.regle2()
        this.cacheTourne()
        this.mtgAppLecteur.setPointPosition(this.svgId, this.idPtRegle.pt, 100, 300, false)
        this.mtgAppLecteur.giveFormula2(this.svgId, 'reglangleC', 13.3, false)
        this.jensuisou = 'CompasRegle1'
        this.mtgAppLecteur.updateFigure(this.svgId)
        setTimeout(this.animeRepere.bind(this), this.tempodeb)
        break
      case 'CompasRegle1':
        // fait apparaitre compas en bas
        this.afficheIm('compas', true)
        this.compas()
        this.cacheTourne()
        this.mtgAppLecteur.giveFormula2(this.svgId, 'compangleC', 'compangle', true)
        this.mtgAppLecteur.updateFigure(this.svgId)
        this.mtgAppLecteur.setPointPosition(this.svgId, '#compPLA2', 100, 298, true)
        this.mtgAppLecteur.setPointPosition(this.svgId, '#compPLA1', 100 + this.afaire1.laval * this.unite * this.zoom, 298, true)
        this.pt2 = this.getPointPosition2(this.svgId, '#compPLA3')

        this.mtgAppLecteur.setPointPosition(this.svgId, '#' + this.idCompPt.pointe, 150, 248, false)
        this.delta = { x: this.pt2.x - 150 + 70, y: this.pt2.y - 248 - 70 }
        this.mtgAppLecteur.setPointPosition(this.svgId, '#' + this.idCompPt.geremine, 150 + this.delta.x, 248 + this.delta.y, false)

        this.jensuisou = 'CompasRegle2'
        this.coRx = 150
        this.coRy = 248
        this.mtgAppLecteur.updateFigure(this.svgId)
        setTimeout(this.animeRepere.bind(this), this.tempodeb)
        break
      case 'CompasRegle2':
        // place pointe
        this.coRy += 4
        this.coRx -= 4
        if ((this.coRx - 100) < 4) {
          this.coRx = 100
          this.coRy = 298
        }
        this.mtgAppLecteur.setPointPosition(this.svgId, '#' + this.idCompPt.pointe, this.coRx, this.coRy, false)
        this.mtgAppLecteur.setPointPosition(this.svgId, '#' + this.idCompPt.geremine, this.coRx + this.delta.x, this.coRy + this.delta.y, false)
        this.mtgAppLecteur.updateFigure(this.svgId)
        if (this.coRx === 100) {
          this.jensuisou = 'CompasRegle3'
          this.coRx = this.coRx + this.delta.x
          this.coRy = this.coRy + this.delta.y
          setTimeout(this.animeRepere.bind(this), this.tempo)
          return
        }
        setTimeout(this.animeRepere.bind(this), this.tempo)
        break
      case 'CompasRegle3':
        // place min
        this.coRx--
        this.coRy++
        if (Math.abs(this.pt2.x - this.coRx) < 1.5) {
          this.coRx = this.pt2.x
          this.coRy = this.pt2.y
        }
        this.mtgAppLecteur.setPointPosition(this.svgId, '#' + this.idCompPt.geremine, this.coRx, this.coRy, true)
        if (this.coRx === this.pt2.x) {
          this.jensuisou = 'CompasRegle4'
          setTimeout(this.animeRepere.bind(this), this.tempodeb)
          return
        }
        setTimeout(this.animeRepere.bind(this), this.tempo)
        break
      case 'CompasRegle4': {
        // enleve regle
        // bloque compas
        this.setVisible2(this.svgId, '#verouv', false, true)
        this.setVisible2(this.svgId, '#verfer', true, true)
        this.jensuisou = this.after
        const laffff = function () {
          this.afficheIm('regle', false)
          this.regle2()
          setTimeout(this.animeRepere.bind(this), this.tempodeb)
        }
        setTimeout(laffff.bind(this), this.tempodeb)
      }
        break
      case 'CompasRegle5':
        // deplace compas jusque centre
        this.PtArrive = this.getPointPosition2(this.svgId, this.htmlcoPoint(this.afaire1.centre))
        this.bary = j3pClone(this.PtArrive)
        this.jensuisou = 'CompasRegle60'
        setTimeout(this.placeBienComp.bind(this), this.tempo)
        break
      case 'CompasRegle60':
        // trace cercle
        pt1 = this.getPointPosition2(this.svgId, '#' + this.idCompPt.pointe)
        pt2 = this.getPointPosition2(this.svgId, '#' + this.idCompPt.geremine)
        this.delta = { x: pt2.x - pt1.x, y: pt2.y - pt1.y }
        this.coRx = pt1.x
        this.coRy = pt1.y
        this.CooBase = j3pClone(this.PtArrive)
        this.jensuisou = 'CompasRegle6'
        setTimeout(this.animeRepere.bind(this), this.tempo)
        break
      case 'CompasRegle6':
        dx = this.CooBase.x - this.coRx
        dy = this.CooBase.y - this.coRy
        if (Math.abs(dx) > Math.abs(dy)) {
          if (dx > 0) {
            this.coRx = this.coRx + 4
          } else {
            this.coRx = this.coRx - 4
          }
          if (dy > 0) {
            this.coRy = this.coRy + 4 * Math.abs(dy) / Math.abs(dx)
          } else {
            this.coRy = this.coRy - 4 * Math.abs(dy) / Math.abs(dx)
          }
        } else {
          if (dy > 0) {
            this.coRy = this.coRy + 4
          } else {
            this.coRy = this.coRy - 4
          }
          if (dx > 0) {
            this.coRx = this.coRx + 4 * Math.abs(dx) / Math.abs(dy)
          } else {
            this.coRx = this.coRx - 4 * Math.abs(dx) / Math.abs(dy)
          }
        }
        if ((Math.abs(dx) < 5 && Math.abs(dy) < 5)) {
          this.coRx = this.CooBase.x
          this.coRy = this.CooBase.y
          this.afficheIm('point2', true)
          this.jensuisou = 'CompasRegle7'
          this.mtgAppLecteur.setPointPosition(this.svgId, '#' + this.idCompPt.pointe, this.coRx, this.coRy, false)
          this.mtgAppLecteur.setPointPosition(this.svgId, '#' + this.idCompPt.geremine, this.coRx + this.delta.x, this.coRy + this.delta.y, false)
          this.mtgAppLecteur.updateFigure(this.svgId)
          setTimeout(this.animeRepere.bind(this), this.tempodeb)
          return
        }
        this.mtgAppLecteur.setPointPosition(this.svgId, '#' + this.idCompPt.pointe, this.coRx, this.coRy, false)
        this.mtgAppLecteur.setPointPosition(this.svgId, '#' + this.idCompPt.geremine, this.coRx + this.delta.x, this.coRy + this.delta.y, false)
        this.mtgAppLecteur.updateFigure(this.svgId)
        setTimeout(this.animeRepere.bind(this), this.tempo)
        break
      case 'CompasRegle7':
        // enleve compas
        this.compas(true)
        for (let i = 0; i < this.compaTra.length; i++) {
          this.setVisible2(this.svgId, this.compaTra[i], true, true)
        }
        pt1 = this.getPointPosition2(this.svgId, this.idCompPt.mine)
        this.mtgAppLecteur.setPointPosition(this.svgId, '#compcoupt', pt1.x, pt1.y, true)
        this.newArc3({ x: this.coRx, y: this.coRy }, pt1, this.afaire1.nom)
        this.jensuisou = 'CompasRegle8'

        this.llanglebase = Math.atan2(pt1.x - this.lXcentre, pt1.y - this.lYcentre) * 180 / Math.PI - 90
        this.llangle = this.llanglebase
        this.dista = this.dist({ x: this.coRx, y: this.coRy }, { x: pt1.x, y: pt1.y })
        setTimeout(this.animeRepere.bind(this), this.tempodeb)
        break
      case 'CompasRegle8':
        this.llangle = this.llangle + 3
        if (this.llangle > this.llanglebase + 358) {
          this.llangle = this.llanglebase + 359.999
        }
        this.mtgAppLecteur.setPointPosition(this.svgId, this.arcco[this.arcco.length - 1].html.p2, this.coRx + this.dista * Math.cos(this.llangle * Math.PI / 180), this.coRy - this.dista * Math.sin(this.llangle * Math.PI / 180), true)
        this.mtgAppLecteur.setPointPosition(this.svgId, '#compcoupt', this.coRx + this.dista * Math.cos(this.llangle * Math.PI / 180), this.coRy - this.dista * Math.sin(this.llangle * Math.PI / 180), true)
        if (this.llangle === this.llanglebase + 359.999) {
          this.jensuisou = 'CompasRegle9'
          setTimeout(this.animeRepere.bind(this), this.tempodeb)
          return
        }
        setTimeout(this.animeRepere.bind(this), this.tempo)
        break
      case 'CompasRegle9':
        this.afficheIm('compas', false)
        this.afficheIm('point2', false)
        for (let i = 0; i < this.compaTra.length; i++) {
          this.setVisible2(this.svgId, this.compaTra[i], false, true)
        }
        if (this.dansmulti) {
          this.jensuioumulti++
        } else {
          this.comptProg++
        }
        if (this.afaire1.nom !== '' && this.afaire1.need !== false) {
          pt1 = this.getPointPosition2(this.svgId, this.idCompPt.mine)
          pt2 = this.getPointPosition2(this.svgId, '#' + this.idCompPt.pointe)
          ch = Math.atan2(pt1.x - pt2.x, pt2.y - pt1.y) - Math.PI / 2
          this.newNom2(pt1.x + 25 * Math.cos(ch)
            , pt1.y + 25 * Math.sin(ch)
            , this.afaire1.nom)
        }
        this.jensuisou = 'prog'
        setTimeout(this.animeRepere.bind(this), this.tempodeb)
        break
      case 'cerclePas':
        pt1 = this.getPointPosition2(this.svgId, this.htmlcoPoint(this.afaire1.par))
        pt2 = this.getPointPosition2(this.svgId, this.htmlcoPoint(this.afaire1.centre))
        this.mtgAppLecteur.setPointPosition(this.svgId, '#compPLA2', pt2.x, pt2.y, true)
        this.mtgAppLecteur.setPointPosition(this.svgId, '#compPLA1', pt1.x, pt1.y, true)
        pt3 = this.getPointPosition2(this.svgId, '#compPLA4')

        tt = [pt1, pt2, pt3]
        this.bary = this.bari2(tt)
        this.placeBienComp()
        this.jensuisou = 'cerclePas1'
        break
      case 'cerclePas1':
        // fait apparaitre compas en bas
        this.afficheIm('compas', true)
        this.compas()
        this.cacheTourne()
        pt1 = this.getPointPosition2(this.svgId, this.htmlcoPoint(this.afaire1.par))
        pt2 = this.getPointPosition2(this.svgId, this.htmlcoPoint(this.afaire1.centre))
        this.mtgAppLecteur.setPointPosition(this.svgId, '#compPLA2', pt2.x, pt2.y, true)
        this.mtgAppLecteur.setPointPosition(this.svgId, '#compPLA1', pt1.x, pt1.y, true)
        pt1 = this.getPointPosition2(this.svgId, '#compPLA3')

        this.mtgAppLecteur.setPointPosition(this.svgId, '#' + this.idCompPt.pointe, pt2.x + 50, pt2.y + 50, false)
        this.mtgAppLecteur.setPointPosition(this.svgId, '#' + this.idCompPt.geremine, pt1.x + 70, pt1.y + 70, false)
        this.delta = { x: pt1.x + 70 - pt2.x - 50, y: pt1.y + 70 - pt2.y - 50 }
        this.jensuisou = 'cerclePas2'
        this.coRx = pt2.x + 50
        this.coRy = pt2.y + 50
        this.mtgAppLecteur.updateFigure(this.svgId)
        this.pt1 = pt2
        this.pt2 = pt1
        setTimeout(this.animeRepere.bind(this), this.tempodeb)
        break
      case 'cerclePas2':
        this.coRy = this.coRy - 4
        this.coRx = this.coRx - 4
        if (Math.abs(this.coRx - this.pt1.x) < 4) {
          this.coRx = this.pt1.x
          this.coRy = this.pt1.y
        }
        this.mtgAppLecteur.setPointPosition(this.svgId, '#' + this.idCompPt.pointe, this.coRx, this.coRy, false)
        this.mtgAppLecteur.setPointPosition(this.svgId, '#' + this.idCompPt.geremine, this.coRx + this.delta.x, this.coRy + this.delta.y, false)
        this.mtgAppLecteur.updateFigure(this.svgId)
        if (this.coRx === this.pt1.x) {
          this.jensuisou = 'cerclePas3'
          this.coRx = this.coRx + this.delta.x
          this.coRy = this.coRy + this.delta.y
          setTimeout(this.animeRepere.bind(this), this.tempo)
          return
        }
        setTimeout(this.animeRepere.bind(this), this.tempo)
        break
      case 'cerclePas3':
        this.coRx--
        this.coRx--
        this.coRy--
        this.coRy--
        if (Math.abs(this.pt2.x - this.coRx) < 2) {
          this.coRx = this.pt2.x
          this.coRy = this.pt2.y
        }
        this.mtgAppLecteur.setPointPosition(this.svgId, '#' + this.idCompPt.geremine, this.coRx, this.coRy, true)
        if (this.coRx === this.pt2.x) {
          this.jensuisou = 'cerclePas4'
          setTimeout(this.animeRepere.bind(this), this.tempodeb)
          return
        }
        setTimeout(this.animeRepere.bind(this), this.tempo)
        break
      case 'cerclePas4':
        this.bary = this.getPointPosition2(this.svgId, '#' + this.idCompPt.pointe)
        this.placeBienComp()
        this.coRx = this.lXcentre
        this.coRy = this.lYcentre
        this.jensuisou = 'CompasRegle7'
        break
      case 'fini': {
        for (let i = 3; i < this.polyhaut.length; i++) {
          this.setVisible2(this.svgId, this.polyhaut[i], false, true)
          if (i < this.nbh) {
            this.setVisible2(this.svgId, this.tabh[i].im, true, true)
            this.mtgAppLecteur.addEventListener(this.svgId, '#' + this.tabh[i].im, 'mouseover', this.makeFoncOverList(i, 0, this.tabh[i].k))
            this.mtgAppLecteur.addEventListener(this.svgId, '#' + this.tabh[i].im, 'mouseout', this.makeFoncOutList(i, 0, this.tabh[i].k))
            this.mtgAppLecteur.addEventListener(this.svgId, '#' + this.tabh[i].im, 'click', this.tabh[i].func)
            this.mtgAppLecteur.addEventListener(this.svgId, '#' + this.tabh[i].im, 'touchstart', this.makePourVoirList(this.polyhaut[i]))
          }
        }
        j3pEmpty(this.coboot)
        const laff = function () {
          const pt = this.getPointPosition2(this.svgId, '#ptcentrot')
          this.mtgAppLecteur.setPointPosition(this.svgId, '#ptbougrot', pt.x, pt.y - 50, true)
          this.rottout()
          this.onco = false
          this.voirRepere()
          this.apresCo()
        }
        j3pAjouteBouton(this.coboot, laff.bind(this), { className: 'MepBoutons', value: 'Effacer la correction' })
        this.okmouv = true
        break
      }
    }
  }

  yasegCo (pt1, pt2) {
    let pt3, pt4, ok
    ok = false
    for (let j = 0; j < this.segCo.length; j++) {
      if (!this.segCo[j].viz) continue
      pt3 = this.getPointPosition2(this.svgId, this.segCo[j].html.point1)
      pt4 = this.getPointPosition2(this.svgId, this.segCo[j].html.point2)
      ok = ok || this.segok(pt1, pt2, pt3, pt4, false)
    }
    if (ok) return true
    for (let j = 0; j < this.dteco.length; j++) {
      if (!this.dteco[j].viz) continue
      pt3 = this.getPointPosition2(this.svgId, this.dteco[j].html.point1)
      pt4 = this.getPointPosition2(this.svgId, this.dteco[j].html.point2)
      ok = ok || this.dteok(pt1, pt2, pt3, pt4)
    }
    if (ok) return true
    for (let j = 0; j < this.demiDco.length; j++) {
      if (!this.demiDco[j].viz) continue
      pt3 = this.getPointPosition2(this.svgId, this.demiDco[j].html.point1)
      pt4 = this.getPointPosition2(this.svgId, this.demiDco[j].html.point2)
      ok = ok || this.demidteok(pt1, pt2, pt3, pt4)
    }
    return ok
  }

  zoomBien () {
    let pt, pt2
    if (this.raz) return
    if (!this.consigne) return
    switch (this.jensuiouPlace) {
      case 'zo':
        this.jensuiouPlace = 'zo2'
        // determine ici zoomax
        // pour voir tous les points interessants
        this.zomax = []
        for (let i = 0; i < this.avoir.length; i++) {
          if (this.avoir[i] !== 'ù') {
            if (this.isObject(this.avoir[i])) { pt = j3pClone(this.avoir[i]) } else {
              pt = this.getPointPosition2(this.svgId, this.htmlcoPoint(this.avoir[i]))
            }
          } else {
            pt = { x: this.PtArrive.x, y: this.PtArrive.y }
          }
          this.zomax[i] = 1
          for (let j = 1; j < 20; j++) {
            pt2 = { x: this.lXcentre + (1 + j / 20) * (pt.x - this.lXcentre) }
            pt2.y = this.lYcentre + (1 + j / 20) * (pt.y - this.lYcentre)
            if (pt2.x > 40 && pt2.x < 780 && pt2.y > 40 && pt2.y < 320) {
              this.zomax[i] = 1 + j / 20
            }
          }
        }
        this.lezoom = 2
        for (let i = 0; i < this.zomax.length; i++) {
          this.lezoom = Math.min(this.lezoom, this.zomax[i])
        }
        this.zoom = 1
      // eslint-disable-next-line no-fallthrough
      case 'zo2':
        if (this.zoom >= this.lezoom) {
          setTimeout(this.animeRepere.bind(this), this.tempodeb)
          return
        }
        this.zoomAll()
        setTimeout(this.zoomBien.bind(this), this.tempo)
        break
    }
  }

  deplaceAll (b) {
    let kx, ky, aret
    aret = false
    const dx = this.lXcentre - this.bary.x
    const dy = this.lYcentre - this.bary.y
    if (Math.abs(dx) > Math.abs(dy)) {
      if (dx > 0) {
        kx = 2
      } else {
        kx = -2
      }
      if (dy > 0) {
        ky = 2 * Math.abs(dy) / Math.abs(dx)
      } else {
        ky = -2 * Math.abs(dy) / Math.abs(dx)
      }
    } else {
      if (dy > 0) {
        ky = 2
      } else {
        ky = -2
      }
      if (dx > 0) {
        kx = 2 * Math.abs(dx) / Math.abs(dy)
      } else {
        kx = -2 * Math.abs(dx) / Math.abs(dy)
      }
    }
    if (isNaN(kx)) kx = 0
    if (isNaN(ky)) ky = 0
    if ((Math.abs(dx) < 5 && Math.abs(dy) < 5)) {
      kx = dx
      ky = dy
      aret = true
    }
    if (b === true) {
      this.bary.x += kx
      this.bary.y += ky
    }
    if (this.PtArrive !== undefined) {
      this.PtArrive.x += kx
      this.PtArrive.y += ky
    }
    if (this.pt1 !== undefined) {
      this.pt1.x += kx
      this.pt1.y += ky
    }
    if (this.pt2 !== undefined) {
      this.pt2.x += kx
      this.pt2.y += ky
    }
    for (let i = this.ConsPtnb; i < this.ptCo.length; i++) {
      this.mtgAppLecteur.translatePoint(this.svgId, this.ptCo[i].html.point, kx, ky, false)
    }
    for (let i = 0; i < this.nomco.length; i++) {
      this.mtgAppLecteur.translatePoint(this.svgId, this.nomco[i].html[0], kx, ky, false)
    }
    for (let i = this.ConsSegnb; i < this.segCo.length; i++) {
      this.mtgAppLecteur.translatePoint(this.svgId, this.segCo[i].html.point1, kx, ky, false)
      this.mtgAppLecteur.translatePoint(this.svgId, this.segCo[i].html.point2, kx, ky, false)
    }
    for (let i = this.ConsDemiDnb; i < this.demiDco.length; i++) {
      this.mtgAppLecteur.translatePoint(this.svgId, this.demiDco[i].html.point1, kx, ky, false)
      this.mtgAppLecteur.translatePoint(this.svgId, this.demiDco[i].html.point2, kx, ky, false)
    }
    for (let i = this.ConsDtenb; i < this.dteco.length; i++) {
      this.mtgAppLecteur.translatePoint(this.svgId, this.dteco[i].html.point1, kx, ky, false)
      this.mtgAppLecteur.translatePoint(this.svgId, this.dteco[i].html.point2, kx, ky, false)
    }
    for (let i = this.ConsArcnb; i < this.arcco.length; i++) {
      this.mtgAppLecteur.translatePoint(this.svgId, this.arcco[i].html.centre, kx, ky, false)
      for (let j = 1; j < 13; j++) {
        this.mtgAppLecteur.translatePoint(this.svgId, this.arcco[i].html['p' + j], kx, ky, false)
      }
    }
    for (let i = 0; i < this.listeSegPris.length; i++) {
      this.mtgAppLecteur.translatePoint(this.svgId, this.listeSegPris[i].point1, kx, ky, false)
      this.mtgAppLecteur.translatePoint(this.svgId, this.listeSegPris[i].point2, kx, ky, false)
      if (this.listeSegPris[i].pointNom) this.mtgAppLecteur.translatePoint(this.svgId, this.listeSegPris[i].pointNom, kx, ky, false)
    }
    for (let i = 0; i < this.listeDtePris.length; i++) {
      this.mtgAppLecteur.translatePoint(this.svgId, this.listeDtePris[i].point1, kx, ky, false)
      this.mtgAppLecteur.translatePoint(this.svgId, this.listeDtePris[i].point2, kx, ky, false)
      if (this.listeDtePris[i].pointNom) this.mtgAppLecteur.translatePoint(this.svgId, this.listeDtePris[i].pointNom, kx, ky, false)
    }
    for (let i = 0; i < this.listeDemiDPris.length; i++) {
      this.mtgAppLecteur.translatePoint(this.svgId, this.listeDemiDPris[i].point1, kx, ky, false)
      this.mtgAppLecteur.translatePoint(this.svgId, this.listeDemiDPris[i].point2, kx, ky, false)
      if (this.listeDemiDPris[i].pointNom) this.mtgAppLecteur.translatePoint(this.svgId, this.listeDemiDPris[i].pointNom, kx, ky, false)
    }
    for (let i = 0; i < this.listeArcPris.length; i++) {
      this.mtgAppLecteur.translatePoint(this.svgId, this.listeArcPris[i].centre, kx, ky, false)
      for (let j = 1; j < 13; j++) {
        this.mtgAppLecteur.translatePoint(this.svgId, this.listeArcPris[i]['p' + j], kx, ky, false)
      }
      if (this.listeArcPris[i].pointNom) this.mtgAppLecteur.translatePoint(this.svgId, this.listeArcPris[i].pointNom, kx, ky, false)
    }
    for (let i = 0; i < this.listePointPris.length; i++) {
      this.mtgAppLecteur.translatePoint(this.svgId, this.listePointPris[i].point, kx, ky, false)
    }
    if (this.compasVisible) {
      this.mtgAppLecteur.translatePoint(this.svgId, '#' + this.idCompPt.pointe, kx, ky, false)
      this.mtgAppLecteur.translatePoint(this.svgId, '#' + this.idCompPt.geremine, kx, ky, false)
    }
    if (this.regleVisible) {
      this.mtgAppLecteur.translatePoint(this.svgId, '#regpt1', kx, ky, false)
    }
    this.mtgAppLecteur.translatePoint(this.svgId, '#ptCa1', kx, ky, false)
    this.mtgAppLecteur.translatePoint(this.svgId, '#ptCa2', kx, ky, false)
    this.mtgAppLecteur.updateFigure(this.svgId)
    return aret
  }

  placeBienComp () {
    if (this.raz) return
    if (!this.consigne) return
    if (this.deplaceAll(true)) {
      setTimeout(this.animeRepere.bind(this), this.tempodeb)
      this.jarte = false
      return
    }
    this.jarte = true
    setTimeout(this.placeBienComp.bind(this), this.tempo)
  }

  placeBien () {
    if (this.raz) return
    if (!this.consigne) return
    this.bary = this.bari(this.ptCo, this.PtArrive)
    if (this.deplaceAll()) {
      setTimeout(this.animeRepere.bind(this), this.tempodeb)
      return
    }
    setTimeout(this.placeBien.bind(this), this.tempo)
  }

  voirRepere () {
    this.faiszomm(true, true)
    this.recentre()
    this.effaceLaCorrection()
    // remetTout()
    // EffaceTout(true)
    j3pEmpty(this.coboot)
    const laff = function () {
      this.onco = true
      this.debutAnimeRepere()
    }
    j3pAjouteBouton(this.coboot, laff.bind(this), { className: 'MepBoutons', value: 'Voir une correction' })
  }

  debutAnimeRepere () {
    this.okmouv = false
    this.jensuisou = 'Efface'
    this.dansmulti = false
    this.raz = false
    this.fautvireRegle = false
    this.placePointLIbre = [{ x: 402, y: this.lYcentre }, { x: 200, y: this.lYcentre }]
    this.angleaupif = j3pShuffle([0, 20, 40, 60, 80, 100, 120, 140, 160, 200, 220, 240, 260, 280, 300, 320, 340])
    const pt = this.getPointPosition2(this.svgId, '#ptcentrot')
    this.mtgAppLecteur.setPointPosition(this.svgId, '#ptbougrot', pt.x, pt.y - 50, true)
    this.yapause = false
    this.faiszomm(true, true)
    this.rottout()
    this.gardeTout()
    for (let i = 3; i < this.polyhaut.length; i++) {
      this.setVisible2(this.svgId, this.polyhaut[i], false, true)
      if (i < this.nbh) {
        this.setVisible2(this.svgId, this.tabh[i].im, false, true)
      }
    }
    this.viregommEtrot()
    // remetFigDeb
    this.tempo = 20
    this.tempodeb = 500
    this.tempocligne = 400
    this.animeRepere()
  }

  faitClignote (pasAgrand) {
    if (this.raz) return
    if (!this.consigne) return
    let c = 1
    let aum = this.kiclignote
    let fofo = false
    if (typeof aum === 'string') {
      aum = aum.replace(/0|1|2|3|4|5|6|7|8|9|t|T/g, '')
      if (aum.length > 0) fofo = true
    } else {
      fofo = true
    }
    if (fofo && !pasAgrand) {
      this.agrandZ = '0'
      this.faitAgrand()
      return
    }
    this.numclignote++
    if (this.accel) {
      if (Array.isArray(this.kiclignote)) {
        for (let i = 0; i < this.kiclignote.length; i++) {
          this.setColor2(this.svgId, this.kiclignote[i], 255, 0, 0, true)
        }
      } else {
        this.setColor2(this.svgId, this.kiclignote, 255, 0, 0, true)
      }
      this.numclignote = this.nbClignoteMAx + 1
    }
    if (this.numclignote > this.nbClignoteMAx) {
      if (this.fautagrand) {
        this.agrandZ = '0'
        this.faitAgrand()
        return
      } else {
        setTimeout(this.animeRepere.bind(this), this.tempodeb)
        return
      }
    }
    if (this.numclignote > this.nbClignoteMAx / 2) c = 2
    if (this.numclignote % 2 === 0) {
      if (Array.isArray(this.kiclignote)) {
        for (let i = 0; i < this.kiclignote.length; i++) {
          this.setColor2(this.svgId, this.kiclignote[i], 255, 0, 0, true)
        }
      } else {
        this.setColor2(this.svgId, this.kiclignote, 255, 0, 0, true)
      }
    } else {
      if (this.lalacool) {
        if (Array.isArray(this.kiclignote)) {
          for (let i = 0; i < this.kiclignote.length; i++) {
            this.setColor2(this.svgId, this.kiclignote[i], 100, 100, 100, true)
          }
        } else {
          this.setColor2(this.svgId, this.kiclignote, 100, 100, 100, true)
        }
      } else {
        if (Array.isArray(this.kiclignote)) {
          for (let i = 0; i < this.kiclignote.length; i++) {
            this.setColor2(this.svgId, this.kiclignote[i], 100, 100, 250, true)
          }
        } else {
          this.setColor2(this.svgId, this.kiclignote, 100, 100, 250, true)
        }
      }
    }
    setTimeout(() => { this.faitClignote(pasAgrand) }, this.tempocligne / c)
  }

  effaceLaCorrection () {
    this.recentre()
    for (let i = this.ConsPtnb; i < this.ptCo.length; i++) {
      this.setVisible2(this.svgId, this.ptCo[i].html.point, false, false)
      this.setVisible2(this.svgId, this.ptCo[i].html.aff, false, false)
    }
    for (let i = 0; i < this.nomco.length; i++) {
      this.setVisible2(this.svgId, this.nomco[i].html[1], false, false)
    }
    for (let i = this.ConsSegnb; i < this.segCo.length; i++) {
      this.setVisible2(this.svgId, this.segCo[i].html.ligne, false, true)
    }
    for (let i = this.ConsDemiDnb; i < this.demiDco.length; i++) {
      this.setVisible2(this.svgId, this.demiDco[i].html.ligne, false, false)
    }
    for (let i = this.ConsDtenb; i < this.dteco.length; i++) {
      this.setVisible2(this.svgId, this.dteco[i].html.ligne, false, false)
    }
    for (let i = this.ConsArcnb; i < this.arcco.length; i++) {
      this.setVisible2(this.svgId, this.arcco[i].html.arc1, false, false)
    }

    for (let i = 0; i < this.ConsPtnb; i++) {
      if (this.ptCo[i].viz === false) {
        this.setVisible2(this.svgId, this.ptCo[i].html.point, false, false)
        this.setVisible2(this.svgId, this.ptCo[i].html.aff, false, false)
      }
    }
    for (let i = 0; i < this.ConsSegnb; i++) {
      if (this.segCo[i].viz === false) {
        this.setVisible2(this.svgId, this.segCo[i].html.ligne, false, false)
      }
    }
    for (let i = 0; i < this.ConsDemiDnb; i++) {
      if (this.demiDco[i].viz === false) {
        this.setVisible2(this.svgId, this.demiDco[i].html.point1, false, false)
      }
    }
    for (let i = 0; i < this.ConsDtenb; i++) {
      if (this.dteco[i].viz === false) {
        this.setVisible2(this.svgId, this.dteco[i].html.ligne, false, false)
      }
    }
    for (let i = 0; i < this.ConsArcnb; i++) {
      if (this.arcco[i].viz === false) {
        this.setVisible2(this.svgId, this.arcco[i].html.arc1, false, false)
      }
    }

    this.mtgAppLecteur.updateFigure(this.svgId)
  }

  faitAgrand () {
    if (this.raz) return
    if (!this.consigne) return
    if (this.yapause) {
      setTimeout(this.faitAgrand.bind(this), this.tempodeb)
      if (this.firstpause) {
        j3pEmpty(this.tabeffa[0][1])
        const ffenv = function () {
          this.yapause = false
          j3pEmpty(this.tabeffa[0][1])
          const ffenvbis = function () {
            this.yapause = true
            this.firstpause = true
          }
          j3pAjouteBouton(this.tabeffa[0][1], ffenvbis.bind(this), { className: '', value: 'Pause' })
        }
        j3pAjouteBouton(this.tabeffa[0][1], ffenv.bind(this), { className: '', value: 'Reprendre' })
        this.firstpause = false
      }
      return
    }
    let lenum, p1, p2, mil, dx, dy
    switch (this.agrandZ) {
      case '0':
        // on met im regle
        this.afficheIm('regle2', true)
        // on on fait apparaitre regle à 50 50
        // recupe les coo des deux points
        // calcule l’angle
        // mets l’angle ppour regle en dessous
        this.regle()
        this.cacheTourne()
        if (Array.isArray(this.kiclignote)) {
          lenum = { html: { point1: this.htmlcoPoint(this.kiclignote[0]), point2: this.htmlcoPoint(this.kiclignote[1]) } }
        } else {
          lenum = this.retrouve(this.kiclignote)
        }
        p1 = this.getPointPosition2(this.svgId, lenum.html.point1)
        p2 = this.getPointPosition2(this.svgId, lenum.html.point2)
        this.monangle = Math.atan2(p2.x - p1.x, p1.y - p2.y)
        mil = { x: (p1.x + p2.x) / 2, y: (p1.y + p2.y) / 2 }

        this.CooBase = {
          x: mil.x + 10 * this.unite * Math.cos(this.monangle - Math.PI / 2),
          y: mil.y + 10 * this.unite * Math.sin(this.monangle - Math.PI / 2)
        }
        // this.CooBase = j3pClone(mil)
        this.CooBase = {
          x: this.CooBase.x + 0.06 * this.unite * Math.cos(this.monangle + Math.PI / 2),
          y: this.CooBase.y + 0.06 * this.unite * Math.sin(this.monangle + Math.PI / 2)
        }
        // r = parseFloat(this.afaire.cond[0].cb)
        // this.PtArrive.x = this.CooBase.x + r * this.unite * Math.cos(this.monangle)
        // this.PtArrive.y = this.CooBase.y + r * this.unite * Math.sin(this.monangle)
        this.mtgAppLecteur.giveFormula2(this.svgId, 'reglangleC', (this.monangle + Math.PI / 2) * 180 / Math.PI + 13)
        this.mtgAppLecteur.updateFigure(this.svgId)
        this.coRx = this.CooBase.x - 70 * Math.cos(this.monangle + (-13) / 180 * Math.PI)
        this.coRy = this.CooBase.y - 70 * Math.sin(this.monangle + (-13) / 180 * Math.PI)
        this.agrandZ = '1'
        this.mil = mil
        setTimeout(this.faitAgrand.bind(this), this.tempodeb)
        break
      case '1':
        dx = this.CooBase.x - this.coRx
        dy = this.CooBase.y - this.coRy
        if (Math.abs(dx) > Math.abs(dy)) {
          if (dx > 0) {
            this.coRx = this.coRx + 2
          } else {
            this.coRx = this.coRx - 2
          }
          if (dy > 0) {
            this.coRy = this.coRy + 2 * Math.abs(dy) / Math.abs(dx)
          } else {
            this.coRy = this.coRy - 2 * Math.abs(dy) / Math.abs(dx)
          }
        } else {
          if (dy > 0) {
            this.coRy = this.coRy + 2
          } else {
            this.coRy = this.coRy - 2
          }
          if (dx > 0) {
            this.coRx = this.coRx + 2 * Math.abs(dx) / Math.abs(dy)
          } else {
            this.coRx = this.coRx - 2 * Math.abs(dx) / Math.abs(dy)
          }
        }
        if ((Math.abs(dx) < 3 && Math.abs(dy) < 3)) {
          this.coRx = this.CooBase.x
          this.coRy = this.CooBase.y
          this.mtgAppLecteur.setPointPosition(this.svgId, this.idPtRegle.pt, this.coRx, this.coRy, true)
          this.agrandZ = '2'
          setTimeout(this.faitAgrand.bind(this), this.tempodeb)
          return
        }
        this.mtgAppLecteur.setPointPosition(this.svgId, this.idPtRegle.pt, this.coRx, this.coRy, true)
        setTimeout(this.faitAgrand.bind(this), this.tempo)
        break
      case '2':
        // fait apparaitre crayon et dte
        // le mm nom a la droite
        this.afficheIm('point2', true)
        this.mtgAppLecteur.setPointPosition(this.svgId, this.idImCrayPt, this.mil.x, this.mil.y, true)
        this.setVisible2(this.svgId, '#regtt', false, true)
        this.setVisible2(this.svgId, '#laddregle', true, true)
        if (Array.isArray(this.kiclignote)) {
          lenum = { html: { point1: this.htmlcoPoint(this.kiclignote[0]), point2: this.htmlcoPoint(this.kiclignote[1]) } }
        } else {
          lenum = this.retrouve(this.kiclignote)
        }
        p1 = this.getPointPosition2(this.svgId, lenum.html.point1)
        p2 = this.getPointPosition2(this.svgId, lenum.html.point2)
        if (String(this.kiclignote).includes('[') && String(this.kiclignote).includes(']')) {
          this.newSeg2(p1.x, p1.y, p2.x, p2.y)
          this.kiclignote = this.segCo[this.segCo.length - 1].html.ligne
        } else {
          this.newDte2(p1.x, p1.y, p2.x, p2.y, lenum.nom)
          this.kiclignote = this.dteco[this.dteco.length - 1].html.ligne
        }

        this.fautagrand = false
        this.agrandZ = '3'
        setTimeout(this.faitAgrand.bind(this), this.tempodeb)
        break
      case '3':
        this.regle()
        this.renvoieCrayon()
        this.afficheIm('point2', false)
        this.afficheIm('regle2', false)
        this.numclignote = 8
        this.clignEff.push({ ki: this.kiclignote, cool: true })
        this.faitClignote()
    }
  }

  retrouve (ki) {
    if (Array.isArray(ki)) {
      return { html: { point1: this.htmlcoPoint(ki[1]), point2: this.htmlcoPoint(ki[2]) } }
    }

    for (let i = 0; i < this.demiDco.length; i++) {
      if (ki === this.demiDco[i].html.ligne) return j3pClone(this.demiDco[i])
    }
    for (let i = 0; i < this.segCo.length; i++) {
      if (ki === this.segCo[i].html.ligne) return j3pClone(this.segCo[i])
    }
    for (let i = 0; i < this.dteco.length; i++) {
      if (ki === this.dteco[i].html.ligne) return j3pClone(this.dteco[i])
    }
  }

  donneAb (a, b, c, s) {
    let a1, a2
    const p1 = this.getPointPosition2(this.svgId, this.htmlcoPoint(this.recupe(a)))
    const p2 = this.getPointPosition2(this.svgId, this.htmlcoPoint(this.recupe(b)))
    const p3 = {
      x: (p2.x - p1.x) * Math.cos(c * Math.PI / 180) + (p2.y - p1.y) * Math.sin(c * Math.PI / 180) + p1.x,
      y: (p1.x - p2.x) * Math.sin(c * Math.PI / 180) + (p2.y - p1.y) * Math.cos(c * Math.PI / 180) + p1.y
    }
    const p4 = {
      x: (p2.x - p1.x) * Math.cos(-c * Math.PI / 180) + (p2.y - p1.y) * Math.sin(-c * Math.PI / 180) + p1.x,
      y: (p1.x - p2.x) * Math.sin(-c * Math.PI / 180) + (p2.y - p1.y) * Math.cos(-c * Math.PI / 180) + p1.y
    }
    a1 = (p3.y - p1.y) / (p3.x - p1.x)
    if (p3.x === p1.x) a1 = this.maxInt
    const b1 = p1.y - a1 * p1.x
    a2 = (p4.y - p1.y) / (p4.x - p1.x)
    if (p4.x === p1.x) a2 = this.maxInt
    const b2 = p1.y - a2 * p1.x
    if (s === 'direct') return [{ a: a1, b: b1 }]
    if (s === 'indirect') return [{ a: a2, b: b2 }]
    return [{ a: a1, b: b1 }, { a: a2, b: b2 }]
  }

  giveABs (ki) {
    if (ki.ki.spe === undefined) return this.giveAB(ki.ki.nom)
    /// /
    /// /
    /// /
    let ch

    const pt1 = this.getPointPosition2(this.svgId, this.htmlcoPoint(this.recupe(ki.ki.pass)))
    const nn = this.giveAB(this.recupe(ki.ki.odt))
    const a = nn.a
    const b = pt1.y - a * pt1.x
    const b3 = nn.b
    const pt2 = { x: 1000 }
    pt2.y = a * pt2.x + b

    ki.ang = Math.atan2(pt1.x - pt2.x, pt2.y - pt1.y)
    const a2 = -1 / a
    const b2 = pt1.y - a2 * pt1.x
    if (pt2.x === pt1.x) {
      ch = { y: pt1.y }
      ch.x = pt2.x
    } else if (pt2.y === pt1.y) {
      ch = { y: nn.pt2.y }
      ch.x = nn.pt1.x
    } else {
      ch = { x: (b3 - b2) / (a2 - a) }
      ch.y = a2 * ch.x + b2
    }
    this.bary = { x: (pt1.x + ch.x) / 2, y: (pt1.y + ch.y) / 2 }
    ki.oupla = { x: ch.x - pt1.x, y: ch.y - pt1.y }

    return { a, b, pt1, pt2 }
  }

  giveABs2 (ki) {
    if (ki.ki.spe === undefined) return this.giveAB(ki.ki.nom)
    /// /
    /// /
    /// /
    let ch

    const pt1 = this.getPointPosition2(this.svgId, this.htmlPoint(this.recupe(ki.ki.pass)))
    const nn = this.giveAB2(this.recupe(ki.ki.odt))
    const a = nn.a
    const b = pt1.y - a * pt1.x
    const b3 = nn.b
    const pt2 = { x: 1000 }
    pt2.y = a * pt2.x + b

    ki.ang = Math.atan2(pt1.x - pt2.x, pt2.y - pt1.y)
    const a2 = -1 / a
    const b2 = pt1.y - a2 * pt1.x
    if (pt2.x === pt1.x) {
      ch = { y: pt1.y }
      ch.x = pt2.x
    } else if (pt2.y === pt1.y) {
      ch = { y: nn.pt2.y }
      ch.x = nn.pt1.x
    } else {
      ch = { x: (b3 - b2) / (a2 - a) }
      ch.y = a2 * ch.x + b2
    }
    this.bary = { x: (pt1.x + ch.x) / 2, y: (pt1.y + ch.y) / 2 }
    ki.oupla = { x: ch.x - pt1.x, y: ch.y - pt1.y }

    return { a, b, pt1, pt2 }
  }

  affichePtbalade () {
    this.mtgAppLecteur.giveFormula2(this.svgId, 'tttttttttt', this.unitebase / this.unite)
    this.setVisible2(this.svgId, '#segbal1', true, false)
    this.setVisible2(this.svgId, '#segbal2', true, false)
    this.mtgAppLecteur.setPointPosition(this.svgId, '#ptbalade', this.ptplace.x, this.ptplace.y, false)
    this.mtgAppLecteur.updateFigure(this.svgId)
  }

  deplacePtbalade () {
    this.mtgAppLecteur.setPointPosition(this.svgId, '#ptbalade', this.ptplace.x, this.ptplace.y, true)
  }

  cahchePointballade () {
    this.setVisible2(this.svgId, '#segbal1', false, false)
    this.setVisible2(this.svgId, '#segbal2', false, false)
    this.mtgAppLecteur.updateFigure(this.svgId)
  }

  metNewPoint () {
    this.ptCo.push({ html: this.creeNewPoint(this.nomPbuf, this.nomPbuf, [0, 0, 255]), nom: this.nomPbuf })
    const monp = this.ptCo[this.ptCo.length - 1]
    this.mtgAppLecteur.setPointPosition(this.svgId, monp.html.point, this.ptplace.x, this.ptplace.y, true)
  }

  gardeTout () {
    this.recentre()
  }

  remetDeb () {
    let cpt

    this.ptCo = []
    this.segCo = []
    this.demiDco = []
    this.arcco = []
    this.dteco = []
    this.clignEff = []
    this.nomco = []

    cpt = 0
    for (let i = 0; i < this.ConsSegnb; i++) {
      this.copieSeg(cpt)
      cpt++
    }

    cpt = 0
    for (let i = 0; i < this.ConsPtnb; i++) {
      this.copiePoint(cpt)
      cpt++
    }

    cpt = 0
    for (let i = 0; i < this.ConsDtenb; i++) {
      this.copieDroite(cpt)
      cpt++
    }

    cpt = 0
    for (let i = 0; i < this.ConsDemiDnb; i++) {
      this.copieDemiD(cpt)
      cpt++
    }

    cpt = 0
    for (let i = 0; i < this.ConsArcnb; i++) {
      this.copieArc(cpt)
      cpt++
    }

    this.mtgAppLecteur.updateFigure(this.svgId)
  }

  copiePoint (k) {
    const viz = this.listePointPris[k].viz !== false
    this.ptCo.push({ html: this.listePointPris[k], viz })
    const monp = this.ptCo[this.ptCo.length - 1]
    monp.nom = this.nomDebPoint[k]
    if (viz) this.mtgAppLecteur.setText(this.svgId, monp.html.aff, monp.nom, true)
    this.setVisible2(this.svgId, monp.html.point, viz, true)
    this.setVisible2(this.svgId, monp.html.aff, viz, true)
    this.setVisible2(this.svgId, monp.html.suiv, viz, true)
  }

  newSeg () {
    this.segCo.push({ html: this.creeNewSeg([100, 100, 255]) })
    const monp = this.segCo[this.segCo.length - 1]
    monp.nom = this.nomPbuf
    this.mtgAppLecteur.setPointPosition(this.svgId, monp.html.point1, this.CooBase.x, this.CooBase.y, true)
    this.mtgAppLecteur.setPointPosition(this.svgId, monp.html.point2, this.CooBase.x, this.CooBase.y, true)
    this.setVisible2(this.svgId, monp.html.ligne, true, true)
    this.setLineStyle2(this.svgId, monp.html.ligne, (this.coPointille) ? 2 : 0, 2, true)
    monp.pointille = this.coPointille
  }

  newSeg2 (x, y, z, t) {
    this.segCo.push({ html: this.creeNewSeg([100, 100, 255]) })
    const monp = this.segCo[this.segCo.length - 1]
    monp.nom = this.nomPbuf
    this.mtgAppLecteur.setPointPosition(this.svgId, monp.html.point1, x, y, true)
    this.mtgAppLecteur.setPointPosition(this.svgId, monp.html.point2, z, t, true)
    this.setVisible2(this.svgId, monp.html.ligne, true, true)
    this.setColor2(this.svgId, monp.html.ligne, 100, 100, 255, true)
  }

  copieSeg (k) {
    this.segCo.push({ html: j3pClone(this.listeSegPris[k]), viz: this.listeSegPris[k].viz })
    const monp = this.segCo[this.segCo.length - 1]
    monp.nom = this.nomDebSeg[k]
    this.setVisible2(this.svgId, monp.html.ligne, this.listeSegPris[k].viz, true)
  }

  newDte2 (x1, y1, x2, y2, n, viz) {
    viz = true
    this.dteco.push({ html: this.creeNewDroite([100, 100, 255]), viz, nom: n })
    const monp = this.dteco[this.dteco.length - 1]
    this.mtgAppLecteur.setPointPosition(this.svgId, monp.html.point1, x1, y1, true)
    this.mtgAppLecteur.setPointPosition(this.svgId, monp.html.point2, x2, y2, true)
    this.setVisible2(this.svgId, monp.html.ligne, viz, true)
    this.setColor2(this.svgId, monp.html.ligne, 100, 100, 255, true)
    this.mtgAppLecteur.setThickness({ elt: monp.html.ligne, thickness: 3 })
  }

  newDDte2 (x1, y1, x2, y2, n, viz) {
    viz = true
    this.demiDco.push({ html: this.creeNewDemiDroite([100, 100, 255]), viz })
    const monp = this.demiDco[this.demiDco.length - 1]
    monp.nom = n
    this.mtgAppLecteur.setPointPosition(this.svgId, monp.html.point1, x1, y1, true)
    this.mtgAppLecteur.setPointPosition(this.svgId, monp.html.point2, x2, y2, true)
    this.setVisible2(this.svgId, monp.html.ligne, viz, true)
    this.setColor2(this.svgId, monp.html.ligne, 100, 100, 255, true)
  }

  copieArc (k) {
    this.arcco.push({ html: j3pClone(this.listeArcPris[k]), viz: this.listeArcPris[k].viz })
    const monp = this.arcco[this.arcco.length - 1]
    for (let i = 0; i < this.nomspris.length; i++) {
      if (this.typeDe(this.nomspris[i]) === 'cercle') {
        if (this.centreDe(this.nomspris[i]) === this.listeArcPris[k].centre) monp.nom = this.nomspris[i]
      }
    }
    const viz = this.listeArcPris[k].viz !== false
    this.setVisible2(this.svgId, monp.html.arc1, viz, true)
  }

  newArc2 (pt1, pt2, n) {
    const r = this.dist(pt1, pt2)
    const ang = Math.atan2(pt1.x - pt2.x, pt2.y - pt1.y) - Math.PI / 2

    this.arcco.push({ html: this.creeNewArc([100, 100, 255]) })
    const monp = this.arcco[this.arcco.length - 1]
    monp.nom = n
    this.mtgAppLecteur.setPointPosition(this.svgId, monp.html.centre, pt2.x, pt2.y, false)
    this.mtgAppLecteur.setPointPosition(this.svgId, monp.html.p1, pt2.x + r * Math.cos(ang + 0.1), pt2.y + r * Math.sin(ang + 0.1), false)
    this.mtgAppLecteur.setPointPosition(this.svgId, monp.html.p2, pt2.x + r * Math.cos(ang - 0.1), pt2.y + r * Math.sin(ang - 0.1), false)
    for (let i = 13; i < 25; i++) this.setVisible2(this.svgId, monp.html['arc' + (i - 12)], false, false)
    this.setVisible2(this.svgId, monp.html.arc1, true, false)
    this.setColor2(this.svgId, monp.html.arc1, 0, 0, 255, false)
    this.mtgAppLecteur.updateFigure(this.svgId)
  }

  creeNewArcCo3 () {
    const ss = {}
    ss.centre = this.newTag()
    ss.p1 = this.newTag()
    ss.p2 = this.newTag()
    ss.arc1 = this.newTag()
    ss.color = '#aaa'
    this.mtgAppLecteur.addFreePoint({
      absCoord: true,
      tag: ss.p1,
      name: ss.p1,
      hidden: true,
      x: 50,
      y: 100
    })
    this.mtgAppLecteur.addFreePoint({
      absCoord: true,
      tag: ss.p2,
      name: ss.p2,
      hidden: true,
      x: 55,
      y: 110
    })
    this.mtgAppLecteur.addFreePoint({
      absCoord: true,
      tag: ss.centre,
      name: ss.centre,
      hidden: true,
      x: 100,
      y: 100
    })
    this.mtgAppLecteur.addArcDirectOAB({
      o: ss.centre,
      a: ss.p1,
      b: ss.p2,
      color: ss.color,
      thickness: 3,
      tag: ss.arc1
    })
    return ss
  }

  newArc3 (pt1, pt2, n) {
    this.arcco.push({ html: this.creeNewArcCo3() })
    const monp = this.arcco[this.arcco.length - 1]
    monp.nom = n
    this.mtgAppLecteur.setPointPosition(this.svgId, monp.html.centre, pt1.x, pt1.y, false)
    this.mtgAppLecteur.setPointPosition(this.svgId, monp.html.p1, pt2.x, pt2.y, false)
    this.mtgAppLecteur.setPointPosition(this.svgId, monp.html.p2, pt2.x, pt2.y, false)
    this.setVisible2(this.svgId, monp.html.arc1, true, false)
    this.setColor2(this.svgId, monp.html.arc1, 0, 0, 255, false)
    this.mtgAppLecteur.updateFigure(this.svgId)
  }

  copieDroite (k) {
    this.dteco.push({ html: j3pClone(this.listeDtePris[k]), viz: this.listeDtePris[k].viz, nom: this.listeDtePris[k].nom })
    const monp = this.dteco[this.dteco.length - 1]
    for (let i = 0; i < this.nomspris.length; i++) {
      if (this.typeDe(this.nomspris[i]) === 'droite') {
        if (this.point1De(this.nomspris[i]) === this.listeDtePris[k].point1) monp.nom = this.nomspris[i]
      }
    }
    const viz = this.listeDtePris[k].viz !== false
    this.setVisible2(this.svgId, monp.html.ligne, viz, true)
  }

  newNom2 (x, y, n) {
    const aa = this.newTag()
    const bb = this.newTag()
    this.mtgAppLecteur.addFreePoint({
      absCoord: true,
      tag: aa,
      name: aa,
      x,
      y,
      hidden: true
    })
    this.mtgAppLecteur.addLinkedText({
      a: aa,
      tag: bb,
      text: n,
      color: '#0000FF',
      vAlign: 'middle',
      hAlign: 'center'
    })
    this.nomco.push({ html: [aa, bb] })
    const monp = this.nomco[this.nomco.length - 1]
    monp.nom = n
    this.setVisible2(this.svgId, monp.html[1], true, false)
    this.mtgAppLecteur.setText(this.svgId, monp.html[1], n, false)
    this.setColor2(this.svgId, monp.html[1], 0, 0, 255, false)
    this.mtgAppLecteur.setPointPosition(this.svgId, monp.html[0], x, y, true)
    this.mtgAppLecteur.updateFigure(this.svgId)
  }

  copieDemiD (k) {
    this.demiDco.push({ html: j3pClone(this.listeDemiDPris[k]), viz: this.listeDemiDPris[k].viz })
    const monp = this.demiDco[this.demiDco.length - 1]
    this.setVisible2(this.svgId, monp.html.ligne, true, true)
    const viz = this.listeDemiDPris[k].viz !== false
    this.setVisible2(this.svgId, monp.html.ligne, viz, true)
  }

  lepremsLettre (f) {
    for (let j = this.nomprisProg.length - 1; j > -1; j--) {
      const regex = new RegExp(this.nomprisProg[j].ki, 'g')
      f = f.replace(regex, this.nomprisProg[j].com)
    }
    f = f.replace(/\\times/g, '')
    f = f.replace(/0|1|2|3|4|5|6|7|8|9/g, '')
    return f[0]
  }

  justeUneLongueur (f) {
    f = f.replace(/ /g, '')
    for (let j = this.nomprisProg.length - 1; j > -1; j--) {
      const regex = new RegExp(this.nomprisProg[j].ki, 'g')
      f = f.replace(regex, this.nomprisProg[j].com)
    }
    f = f.replace('\\times', '*')
    if (f.length < 2) return false
    const tabLettre = ['A', 'B', 'C', 'D', 'E', 'F', 'G', 'H', 'I', 'J', 'K', 'L', 'M', 'N', 'O', 'P', 'Q', 'R', 'S', 'T', 'U', 'V', 'W', 'X', 'Y', 'Z', 'a', 'b', 'c', 'd', 'e', 'f', 'g', 'h', 'i', 'j', 'k', 'l', 'm', 'n', 'o', 'p', 'q', 'r', 's', 't', 'u', 'v', 'w', 'x', 'y', 'z']
    for (let i = 0; i < tabLettre.length; i++) {
      for (let j = 0; j < tabLettre.length; j++) {
        if (i === j) continue
        const regex = new RegExp(tabLettre[i] + tabLettre[j], 'g')
        f = f.replace(regex, 'x')
        const regex2 = new RegExp(tabLettre[j] + tabLettre[i], 'g')
        f = f.replace(regex2, 'x')
      }
    }
    const xsplit = f.split('x').length - 1
    if (xsplit !== 1) return false
    if (f === 'x') return 'oui'
    const haz = '+-{}(),./'
    if (haz.includes(f[0]) || haz.includes(f[1])) return false
    return f.replace('x', '').replace('*', '')
  }

  pasDedans (ki, p1, p2, p3) {
    if (ki.x < Math.min(p1.x, p2.x, p3.x) + 20) return false
    if (ki.y < Math.min(p1.y, p2.y, p3.y) + 20) return false
    if (ki.x > Math.max(p1.x, p2.x, p3.x) - 20) return false
    if (ki.y > Math.max(p1.y, p2.y, p3.y) - 20) return false
    return true
  }

  giveCR (k) {
    let p1, p2
    for (let i = 0; i < this.arcco.length; i++) {
      if (this.arcco[i].nom === k) {
        p1 = this.getPointPosition2(this.svgId, this.arcco[i].html.centre)
        p2 = this.getPointPosition2(this.svgId, this.arcco[i].html.p1)
        return { x: p1.x, y: p1.y, r: this.dist(p1, p2) }
      }
    }
  }

  isObject (objValue) {
    return objValue && typeof objValue === 'object' && objValue.constructor === Object
  }

  direct (a, b) {
    const k = this.regule(a)
    const l = this.regule(b)
    if (l > k) {
      return (l - k < 180)
    } else {
      return (k - l > 180)
    }
  }

  distPtSegCo (jk) {
    const lem = this.segCo[jk]
    const r = this.PtArrive
    const e = this.getPointPosition2(this.svgId, lem.html.point1)
    const f = this.getPointPosition2(this.svgId, lem.html.point2)
    if (r.x + this.ds.Precis_long / this.unite < Math.min(e.x, f.x)) return 100000
    if (r.x - this.ds.Precis_long / this.unite > Math.max(e.x, f.x)) return -100000
    const a1 = (e.y - f.y) / (e.x - f.x)
    const b1 = e.y - a1 * e.x
    const a2 = -1 / a1
    const b2 = r.y - a2 * r.x
    const c = {}
    c.x = (b2 - b1) / (a1 - a2)
    c.y = a1 * c.x + b1
    if (e.x === f.x) {
      c.x = e.x
      c.y = r.y
    }
    return this.dist(c, r) / this.unite
  }

  deZoomAll () {
    this.yapause = true
    this.lalalalile = Math.sqrt(Math.cbrt(Math.cbrt(1 / this.zoom)))
    this.lalalilo = 0
    this.mod = 1
    setTimeout(this.dezoomale2.bind(this), this.tempo)
  }

  giveAB2 (n) {
    let pt1, pt2
    let ok = false
    for (let i = 0; i < this.dteco.length; i++) {
      if (this.dteco[i].nom === n) {
        pt1 = this.getPointPosition2(this.svgId, this.dteco[i].html.point1)
        pt2 = this.getPointPosition2(this.svgId, this.dteco[i].html.point2)
        ok = true
        break
      }
    }
    if (!ok) {
      n = this.recupe(n)
      pt1 = this.getPointPosition2(this.svgId, this.htmlPoint(n[1]))
      pt2 = this.getPointPosition2(this.svgId, this.htmlPoint(n[2]))
    }
    const a = (pt2.y - pt1.y) / (pt2.x - pt1.x)
    const b = pt1.y - a * pt1.x
    if (pt2.x === pt1.x) {
      return { a: this.maxInt, b: pt1.x }
    }
    return { a, b, pt1, pt2 }
  }

  cacheTourne () {
    try {
      this.setVisible2(this.svgId, this.idPtRegle.pt2, false, true)
    } catch (e) {
      // eslint-disable-next-line no-console
      console.log(e)
    }
    try {
      this.setVisible2(this.svgId, this.idPtRegle.ma, false, true)
    } catch (e) {
      // eslint-disable-next-line no-console
      console.log(e)
    }
    try {
      this.setVisible2(this.svgId, '#' + this.idCompPt.geremine, false, true)
    } catch (e) {
      // eslint-disable-next-line no-console
      console.log(e)
    }
    try {
      this.setVisible2(this.svgId, this.idCompPt.ma, false, true)
    } catch (e) {
      // eslint-disable-next-line no-console
      console.log(e)
    }
    try {
      this.setVisible2(this.svgId, this.idPtEquerre.ma, false, true)
    } catch (e) {
      // eslint-disable-next-line no-console
      console.log(e)
    }
    try {
      this.setVisible2(this.svgId, this.idPRap.ma, false, true)
    } catch (e) {
      // eslint-disable-next-line no-console
      console.log(e)
    }
    try {
      this.setVisible2(this.svgId, this.idPRap.pt, false, true)
    } catch (e) {
      // eslint-disable-next-line no-console
      console.log(e)
    }
    try {
      this.setVisible2(this.svgId, this.idPRap.pt2, false, true)
    } catch (e) {
      // eslint-disable-next-line no-console
      console.log(e)
    }
    try {
      this.setVisible2(this.svgId, '#pteqma0', false, true)
    } catch (e) {
      // eslint-disable-next-line no-console
      console.log(e)
    }
    try {
      this.setVisible2(this.svgId, '#eqb', false, true)
    } catch (e) {
      // eslint-disable-next-line no-console
      console.log(e)
    }
    try {
      this.setVisible2(this.svgId, '#rgb', false, true)
    } catch (e) {
      // eslint-disable-next-line no-console
      console.log(e)
    }
    try {
      this.setVisible2(this.svgId, '#raprap12', false, true)
    } catch (e) {
      // eslint-disable-next-line no-console
      console.log(e)
    }
    try {
      this.setVisible2(this.svgId, '#raprap13', false, true)
    } catch (e) {
      // eslint-disable-next-line no-console
      console.log(e)
    }
  }

  bari (tab, pt) {
    let gu, jk
    let sx = 0
    let sy = 0
    for (let i = 0; i < tab.length; i++) {
      gu = this.getPointPosition2(this.svgId, tab[i].html.point)
      sx += gu.x
      sy += gu.y
    }
    jk = 0
    if (pt !== undefined) {
      sx += pt.x
      sy += pt.y
      jk = 1
    }
    return { x: sx / (tab.length + jk), y: sy / (tab.length + jk) }
  }

  bari2 (tab) {
    let sx, sy
    sx = sy = 0
    for (let i = 0; i < tab.length; i++) {
      sx += tab[i].x
      sy += tab[i].y
    }
    return { x: sx / (tab.length), y: sy / (tab.length) }
  }

  faisFond (b) {
    if (b) {
      try {
        this.setColor2(this.svgId, '#ddp1', 0, 138, 115, true)
      } catch (e) {
        // eslint-disable-next-line no-console
        console.log(e, 'en setColor marche pas si pas visible')
      }
      try {
        this.setColor2(this.svgId, '#ddp2', 0, 138, 115, true)
      } catch (e) {
        // eslint-disable-next-line no-console
        console.log(e, 'en setColor marche pas si pas visible')
      }
      try {
        this.setColor2(this.svgId, '#ddp3', 0, 138, 115, true)
      } catch (e) {
        // eslint-disable-next-line no-console
        console.log(e, 'en setColor marche pas si pas visible')
      }
      try {
        this.setColor2(this.svgId, '#ddp4', 0, 138, 115, true)
      } catch (e) {
        // eslint-disable-next-line no-console
        console.log(e, 'en setColor marche pas si pas visible')
      }
      this.zonefig.style.border = '3px solid #008A73'
    } else {
      this.zonefig.style.border = '3px solid #D64700'
      try {
        this.setColor2(this.svgId, '#ddp1', 214, 71, 0, true)
      } catch (e) {
        // eslint-disable-next-line no-console
        console.log(e, 'en setColor marche pas si pas visible')
      }
      try {
        this.setColor2(this.svgId, '#ddp2', 214, 71, 0, true)
      } catch (e) {
        // eslint-disable-next-line no-console
        console.log(e, 'en setColor marche pas si pas visible')
      }
      try {
        this.setColor2(this.svgId, '#ddp3', 214, 71, 0, true)
      } catch (e) {
        // eslint-disable-next-line no-console
        console.log(e, 'en setColor marche pas si pas visible')
      }
      try {
        this.setColor2(this.svgId, '#ddp4', 214, 71, 0, true)
      } catch (e) {
        // eslint-disable-next-line no-console
        console.log(e, 'en setColor marche pas si pas visible')
      }
    }
  }

  corres (ki) {
    for (let i = 0; i < this.nomprisProg.length; i++) {
      if (this.nomprisProg[i].ki === ki) return this.nomprisProg[i].com
    }
    return 'PAS TROUVE'
  }

  slang (pt1, pt2, pt3) {
    return this.regule((Math.atan2(pt3.x - pt2.x, pt2.y - pt3.y) - Math.atan2(pt1.x - pt2.x, pt2.y - pt1.y)) * 180 / Math.PI) > 0
  }

  desactivezone () {
    this.viregommEtrot()
    this.setVisible2(this.svgId, this.regtra[2], false, true)
    // this.setVisible2(this.svgId, this.regtra[3], false, true)
    // this.setVisible2(this.svgId, this.regtra[4], false, true)
    this.PtRegle = false
    this.PtRegle2 = false
    this.equerreVisible = true
    this.rapporteurVisible = true
    this.regleVisible = true
    this.compasVisible = true
    this.lagrad = false
    this.regle()
    this.equerre()
    this.rapporteur()
    this.compas()
    this.eteintPoint()
    this.lepoint = this.lepoint2 = this.lepoint22 = this.lepoint4 = false
    for (let i = 0; i < this.polydroite.length; i++) {
      try {
        this.setVisible2(this.svgId, this.polydroite[i][0], false, true)
      } catch (e) {
        // eslint-disable-next-line no-console
        console.log(e)
      }
      try {
        this.setVisible2(this.svgId, this.polydroite[i][1], false, true)
      } catch (e) {
        // eslint-disable-next-line no-console
        console.log(e)
      }
    }
    for (let i = 0; i < this.polyhaut.length; i++) {
      this.setVisible2(this.svgId, this.polyhaut[i], false, true)
    }
    for (let i = 0; i < this.tabh.length; i++) {
      this.setVisible2(this.svgId, this.tabh[i].im, false, true)
    }
    for (let i = 0; i < this.tabv.length; i++) {
      this.setVisible2(this.svgId, this.tabv[i].im, false, true)
    }
  }

  colorTout (cool) {
    let lacool
    if (cool === true) lacool = [0, 138, 115]
    if (cool === false) lacool = [214, 71, 0]
    for (let i = this.ConsSegnb; i < this.listeSegPris.length; i++) {
      this.setColor2(this.svgId, this.listeSegPris[i].ligne, lacool[0], lacool[1], lacool[2], true)
    }
    for (let i = this.ConsPtnb; i < this.listePointPris.length; i++) {
      this.setColor2(this.svgId, this.listePointPris[i].point, lacool[0], lacool[1], lacool[2], true)
      this.setColor2(this.svgId, this.listePointPris[i].aff, lacool[0], lacool[1], lacool[2], true)
      this.setColor2(this.svgId, this.listePointPris[i].suiv, lacool[0], lacool[1], lacool[2], true)
      this.mtgAppLecteur.setText(this.svgId, this.listePointPris[i].aff, this.listePointPris[i].nom, true)
    }
    for (let i = this.ConsDtenb; i < this.listeDtePris.length; i++) {
      this.setColor2(this.svgId, this.listeDtePris[i].ligne, lacool[0], lacool[1], lacool[2], true)
    }
    for (let i = this.ConsDemiDnb; i < this.listeDemiDPris.length; i++) {
      this.setColor2(this.svgId, this.listeDemiDPris[i].ligne, lacool[0], lacool[1], lacool[2], true)
    }
    for (let i = this.ConsArcnb; i < this.listeArcPris.length; i++) {
      for (let j = 13; j < 25; j++) {
        try {
          this.setColor2(this.svgId, this.listeArcPris[i]['arc' + (j - 12)], lacool[0], lacool[1], lacool[2], true)
        } catch (e) {
          // eslint-disable-next-line no-console
          console.log('arc pas visible')
        }
      }
    }
  }

  zoomAll () {
    this.zoom = 1.01 * this.zoom
    this.PtArrive.x = this.lXcentre + 1.01 * (this.PtArrive.x - this.lXcentre)
    this.PtArrive.y = this.lYcentre + 1.01 * (this.PtArrive.y - this.lYcentre)
    for (let i = 0; i < this.ptCo.length; i++) {
      this.zoomizeCo(this.ptCo[i].html.point)
    }
    for (let i = 0; i < this.nomco.length; i++) {
      this.zoomizeCo(this.nomco[i].html[0])
    }
    for (let i = 0; i < this.segCo.length; i++) {
      this.zoomizeCo(this.segCo[i].html.point1)
      this.zoomizeCo(this.segCo[i].html.point2)
    }
    for (let i = 0; i < this.demiDco.length; i++) {
      this.zoomizeCo(this.demiDco[i].html.point1)
      this.zoomizeCo(this.demiDco[i].html.point2)
    }
    for (let i = 0; i < this.dteco.length; i++) {
      this.zoomizeCo(this.dteco[i].html.point1)
      this.zoomizeCo(this.dteco[i].html.point2)
    }
    for (let i = 0; i < this.arcco.length; i++) {
      this.zoomizeCo(this.arcco[i].html.centre)
      for (let j = 1; j < 13; j++) {
        this.zoomizeCo(this.arcco[i].html['p' + j])
      }
    }

    for (let i = this.ConsSegnb; i < this.listeSegPris.length; i++) {
      this.zoomizeCo(this.listeSegPris[i].point1)
      this.zoomizeCo(this.listeSegPris[i].point2)
    }
    for (let i = this.ConsDtenb; i < this.listeDtePris.length; i++) {
      this.zoomizeCo(this.listeDtePris[i].point1)
      this.zoomizeCo(this.listeDtePris[i].point2)
      if (this.listeDtePris[i].pointNom) this.zoomizeCo(this.listeDtePris[i].pointNom)
    }
    for (let i = this.ConsDemiDnb; i < this.listeDemiDPris.length; i++) {
      this.zoomizeCo(this.listeDemiDPris[i].point1)
      this.zoomizeCo(this.listeDemiDPris[i].point2)
    }
    for (let i = this.ConsArcnb; i < this.listeArcPris.length; i++) {
      this.zoomizeCo(this.listeArcPris[i].centre)
      for (let j = 1; j < 13; j++) {
        this.zoomizeCo(this.listeArcPris[i]['p' + j])
      }
      if (this.listeArcPris[i].pointNom) this.zoomizeCo(this.listeArcPris[i].pointNom)
    }
    for (let i = this.ConsPtnb; i < this.listePointPris.length; i++) {
      this.zoomizeCo(this.listePointPris[i].point)
    }

    if (this.regleVisible) {
      this.zoomizeCo('#regpt1')
    }
    if (this.compasVisible) {
      this.zoomizeCo('#' + this.idCompPt.pointe)
      this.zoomizeCo('#' + this.idCompPt.geremine)
    }
    this.zoomizeCo('#ptCa1')
    this.zoomizeCo('#ptCa2')
    this.zoomizeCo('#unit1')
    this.zoomizeCo('#unit2')
    this.mtgAppLecteur.updateFigure(this.svgId)
    const a = this.getPointPosition2(this.svgId, '#unit1')
    const b = this.getPointPosition2(this.svgId, '#unit2')
    this.unite = this.dist(a, b)
  }

  dezoomale2 () {
    if (this.raz) return
    if (!this.consigne) return
    this.mod = this.lalalalile
    this.lalalilo++
    if (this.lalalilo === 18) this.mod = 1 / this.zoom
    this.zoom = this.zoom * this.mod
    this.PtArrive.x = this.lXcentre + this.mod * (this.PtArrive.x - this.lXcentre)
    this.PtArrive.y = this.lYcentre + this.mod * (this.PtArrive.y - this.lYcentre)
    for (let i = 0; i < this.ptCo.length; i++) {
      this.zoomizeCo2(this.ptCo[i].html.point)
    }
    for (let i = 0; i < this.segCo.length; i++) {
      this.zoomizeCo2(this.segCo[i].html.point1)
      this.zoomizeCo2(this.segCo[i].html.point2)
    }
    for (let i = 0; i < this.demiDco.length; i++) {
      this.zoomizeCo2(this.demiDco[i].html.point1)
      this.zoomizeCo2(this.demiDco[i].html.point2)
    }
    for (let i = 0; i < this.dteco.length; i++) {
      this.zoomizeCo2(this.dteco[i].html.point1)
      this.zoomizeCo2(this.dteco[i].html.point2)
    }
    for (let i = 0; i < this.arcco.length; i++) {
      this.zoomizeCo2(this.arcco[i].html.centre)
      for (let j = 1; j < 13; j++) {
        this.zoomizeCo2(this.arcco[i].html['p' + j])
      }
    }
    for (let i = 0; i < this.nomco.length; i++) {
      this.zoomizeCo2(this.nomco[i].html[0])
    }
    for (let i = this.ConsSegnb; i < this.listeSegPris.length; i++) {
      this.zoomizeCo2(this.listeSegPris[i].point1)
      this.zoomizeCo2(this.listeSegPris[i].point2)
    }
    for (let i = this.ConsDtenb; i < this.listeDtePris.length; i++) {
      this.zoomizeCo2(this.listeDtePris[i].point1)
      this.zoomizeCo2(this.listeDtePris[i].point2)
      if (this.listeDtePris[i].pointNom) this.zoomizeCo2(this.listeDtePris[i].pointNom)
    }
    for (let i = this.ConsDemiDnb; i < this.listeDemiDPris.length; i++) {
      this.zoomizeCo2(this.listeDemiDPris[i].point1)
      this.zoomizeCo2(this.listeDemiDPris[i].point2)
    }
    for (let i = this.ConsArcnb; i < this.listeArcPris.length; i++) {
      this.zoomizeCo2(this.listeArcPris[i].centre)
      for (let j = 1; j < 13; j++) {
        this.zoomizeCo2(this.listeArcPris[i]['p' + j])
      }
      if (this.listeArcPris[i].pointNom) this.zoomizeCo2(this.listeArcPris[i].pointNom)
    }
    for (let i = this.ConsPtnb; i < this.listePointPris.length; i++) {
      this.zoomizeCo2(this.listePointPris[i].point)
    }

    this.zoomizeCo2('#ptCa1')
    this.zoomizeCo2('#ptCa2')
    this.zoomizeCo2('#unit1')
    this.zoomizeCo2('#unit2')
    const a = this.getPointPosition2(this.svgId, '#unit1')
    const b = this.getPointPosition2(this.svgId, '#unit2')
    this.unite = this.dist(a, b)
    if (this.lalalilo < 18) {
      setTimeout(this.dezoomale2.bind(this), this.tempo)
    } else { this.yapause = false }
    this.mtgAppLecteur.updateFigure(this.svgId)
  }

  zoomizeCo (ki) {
    const pt = this.getPointPosition2(this.svgId, ki)
    const newx = {}
    const xM = pt.x - this.lXcentre
    const yM = pt.y - this.lYcentre
    newx.x = xM * 1.01 + this.lXcentre
    newx.y = yM * 1.01 + this.lYcentre
    this.mtgAppLecteur.setPointPosition(this.svgId, ki, newx.x, newx.y, false)
  }

  zoomizeCo2 (ki) {
    const pt = this.getPointPosition2(this.svgId, ki)
    const newx = {}
    const xM = pt.x - this.lXcentre
    const yM = pt.y - this.lYcentre
    newx.x = xM * this.mod + this.lXcentre
    newx.y = yM * this.mod + this.lYcentre
    this.mtgAppLecteur.setPointPosition(this.svgId, ki, newx.x, newx.y, false)
  }

  calculeFormule (f) {
    let ff1, ff2, pt1, pt2
    if (f.includes('\\frac{')) {
      ff1 = f.substring(6, f.indexOf('}{'))
      ff2 = f.substring(f.indexOf('}{') + 2, f.length - 1)
    } else {
      ff2 = '1'
      ff1 = f
    }
    for (let j = this.nomprisProg.length - 1; j > -1; j--) {
      const regex = new RegExp(this.nomprisProg[j].ki, 'g')
      ff1 = ff1.replace(regex, this.nomprisProg[j].com)
      ff2 = ff2.replace(regex, this.nomprisProg[j].com)
    }
    /// refais leslongco
    const leslongCO = []
    for (let i = 0; i < this.ptCo.length; i++) {
      pt1 = this.getPointPosition2(this.svgId, this.ptCo[i].html.point)
      for (let j = 0; j < this.ptCo.length; j++) {
        if (i === j) continue
        pt2 = this.getPointPosition2(this.svgId, this.ptCo[j].html.point)
        leslongCO.push({
          nom1: this.ptCo[i].nom + this.ptCo[j].nom,
          nom2: this.ptCo[j].nom + this.ptCo[i].nom,
          long: this.dist(pt1, pt2) / this.unite
        })
      }
    }
    for (let j = 0; j < leslongCO.length; j++) {
      let regex = new RegExp(leslongCO[j].nom1, 'g')
      ff1 = ff1.replace(regex, leslongCO[j].long)
      ff2 = ff2.replace(regex, leslongCO[j].long)
      regex = new RegExp(leslongCO[j].nom2, 'g')
      ff1 = ff1.replace(regex, leslongCO[j].long)
      ff2 = ff2.replace(regex, leslongCO[j].long)
    }
    while (ff1.includes('\\times')) {
      ff1 = ff1.replace('\\times', '*')
    }
    while (ff2.includes('\\times')) {
      ff2 = ff2.replace('\\times', '*')
    }
    ff1 = this.calcule(ff1)
    ff2 = this.calcule(ff2)
    return parseFloat(ff1) / parseFloat(ff2)
  }
}

export default ProgConst
