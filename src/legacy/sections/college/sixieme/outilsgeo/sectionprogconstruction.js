import { j3pAddElt, j3pAjouteBouton, j3pArrondi, j3pClone, j3pDetruit, j3pEmpty, j3pGetRandomInt, j3pShowError, j3pShuffle } from 'src/legacy/core/functions'
import MenuContextuel from 'src/legacy/outils/menuContextuel'
import { affCorrectionConst, isRepOkConst } from 'src/legacy/outils/zoneStyleMathquill/functions'
import { j3pAffiche } from 'src/lib/mathquill/functions'
import { addDefaultTable } from 'src/legacy/themes/table'
import { getMtgCore } from 'src/lib/outils/mathgraph'
import textesGeneriques from 'src/lib/core/textes'

import ProgConst from './ProgConst'
const { cBien, cFaux, essaieEncore, reponseIncomplete, tempsDepasse } = textesGeneriques

async function initEditor (container, values) {
  const { default: editor } = await import('./editorProgramme.js')
  return editor(container, values) // la fct qui prend un conteneur et les params comme arguments
}

const defaultProgramme = { titre: 'exemple', exos: [{ prog: [], fig: [{ type: 'point', nom: 'Hz1', cox: '0', coy: '400' }, { type: 'point', nom: 'Hz2', cox: '506', coy: '400' }], yaz: false, yar: false, yac: false, zoomin: 1.2, zoomax: 1.6, limcercle: 1 }] }

/**
 * Ajouter une description ici
 * @fileOverview
 * @author Tommy Barroy <t.barroy@laposte.net>
 * @since 2017-10
 */
export const params = {
  outils: ['mathquill'],
  /**
   * Ce qui est paramétrable dans l’éditeur de graphe
   * @type {LegacySectionParamsParametres}
   */
  parametres: [
    ['nbrepetitions', 3, 'entier', 'Nombre de répétitions de la section'],
    ['nbchances', 2, 'entier', 'Nombre d’essais possibles.'],
    // ['Programme', '', 'string', 'Le programme de construction'],
    ['Programme', JSON.stringify(defaultProgramme), 'multiEditor', 'Le programme de construction', initEditor],
    ['Compas', true, 'boolean', '<u>true</u>: Compas disponible'],
    ['Regle_grad', true, 'boolean', '<u>true</u>: Règle graduée disponible'],
    ['Regle', true, 'boolean', '<u>true</u>: Règle nongraduée disponible'],
    ['Equerre', true, 'boolean', '<u>true</u>: Equerre disponible'],
    ['Rapporteur', true, 'boolean', '<u>true</u>: Rapporteur disponible'],
    ['Precis_long', 2, 'number', 'Tolérance pour les longueurs.'],
    ['Precis_ang', 2, 'entier', 'Tolérance pour les angles.'],
    ['Rotation', true, 'boolean', '<u>true</u>: Possibilité de faire tourner la feuille.'],
    ['Agrandir', true, 'boolean', '<u>true</u>: Possibilité d’agrandir ou réduire.'],
    ['Sujet_ordonne', true, 'boolean', '<u>true</u>: Si le paramètre <b>Programme</b> contient plusieurs cas, ils sont demandés dans l’ordre.'],
    // le modèle gère la compatibilité ascendante avec l’ancien paramètre MiseEnPagePlusClaire
    ['theme', 'standard', 'liste', 'Modèle pour l’affichage', ['standard', 'zonesAvecImageDeFond']]

  ]
}

/**
 * section progconstruction
 * @this {Parcours}
 */
export default function main () {
  const me = this
  const stor = this.storage
  let progConst = stor.ProgConst

  let ds = me.donneesSection

  function getDonnees () {
    return {
      // Nombre de répétitions de l’exercice
      nbrepetitions: 3,

      dev: false,

      Sujet_ordonne: true,
      Programme: JSON.stringify(defaultProgramme),
      Compas: true,
      Regle_grad: true,
      Regle: true,
      Equerre: true,
      Rapporteur: true,
      Precis_long: 2,
      Precis_ang: 2,
      Rotation: true,
      Agrandir: true,

      // Si indication != "" alors un lien "Indication" s’affiche dans la zone Inférieure Gauche (this.zones.IG
      // une indication<br>de deux lignes..."
      indication: '',

      // pour le cas de presentation1bis
      // En effet pas d’indication possible pour cette présentation
      // surchargeindication: false,

      // Nombre de chances dont dispose l’él§ve pour répondre à la question
      nbchances: 1,

      //  || "presentation2" || "presentation3"  || "presentation1bis"
      structure: 'presentation3'
    }
  }
  function initSection () {
    me.donneesSection = getDonnees()
    // On surcharge avec les parametres passés par le graphe
    me.surcharge()
    me.validOnEnter = false // ex donneesSection.touche_entree
    // et on ré-affecte notre let
    ds = me.donneesSection
    progConst = new ProgConst(ds, me, 'Fig1')
    stor.ProgConst = progConst
    stor.ProgConst.apresCo = () => {}

    // Construction de la page
    me.construitStructurePage(ds.structure)
    // ce truc est dangereux car ça va planter du json valide (si y’a un ' dans une chaîne entre ") ou bien si y’a un " dans une chaîne entre '
    const progJson = ds.Programme.replace(/'/g, '"')
    let letitre = 'Suivre un programme de construction'
    // on tente de limiter la casse en ne remplaçant que les ' qui suivent ou précèdent une espace ou :{[,
    // const progJson = ds.Programme.replace(/([\s:{[,])'/g, '$1"').replace(/'([\s:}\]],)/g, '"$1')
    try {
      const deparse = JSON.parse(progJson)
      stor.ProgrammeLT = (deparse.exos) ? j3pClone(deparse.exos) : j3pClone(deparse)
      if (deparse.titre) letitre = deparse.titre
      if (letitre === '') letitre = ' '
    } catch (error) {
      console.error(error, 'avec le programme', progJson)
      throw Error('Programme mis en paramètre invalide')
    }

    ds.lesExos = []
    let lp = []
    for (let i = 0; i < stor.ProgrammeLT.length; i++) {
      lp.push(i)
    }
    if (!ds.Sujet_ordonne) lp = j3pShuffle(lp)
    for (let i = 0; i < ds.nbitems; i++) {
      ds.lesExos[i] = { num: lp[i % lp.length] }
    }

    ds.lesExos = ds.lesExos.reverse()
    me.typederreurs = [0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0]
    me.afficheTitre(letitre)

    stor.mepact = me.conteneur
    progConst.mepact = stor.mepact

    getMtgCore({ withMathJax: true, withApiPromise: true })
      .then(
        // success
        (mtgAppLecteur) => {
          progConst.mtgAppLecteur = mtgAppLecteur
          progConst.listePourCalc = mtgAppLecteur.createList('TWF0aEdyYXBoSmF2YTEuMAAAABI+TMzNAAJmcv###wEA#wEAAAAAAAAAAAQsAAACvQAAAQEAAAAAAAAAAQAAAAP#####AAAAAQAKQ0NhbGNDb25zdAD#####AAJwaQAWMy4xNDE1OTI2NTM1ODk3OTMyMzg0Nv####8AAAABAApDQ29uc3RhbnRlQAkh+1RELRj#####AAAAAQAHQ0NhbGN1bAD#####AAFlAAZleHAoMSn#####AAAAAgAJQ0ZvbmN0aW9uBwAAAAE#8AAAAAAAAAAAAAIA#####wABeAABMAAAAAEAAAAAAAAAAP###############w==')
          enonceMain()
        },
        // failure
        (error) => {
          j3pShowError(error, { message: 'mathgraph n’est pas correctement chargé', mustNotify: true })
        })
      // plantage dans le code de success
      .catch(j3pShowError)
  } // initSection

  function faisChoixExos () {
    const e = ds.lesExos.pop()
    const artebuf = []
    let mu, om
    progConst.Programme = j3pClone(stor.ProgrammeLT[e.num].prog)
    progConst.progfig = j3pClone(stor.ProgrammeLT[e.num].fig)
    progConst.yaaacentre = stor.ProgrammeLT[e.num].yac
    progConst.yaaapointill = stor.ProgrammeLT[e.num].yap
    progConst.yaaaarot = stor.ProgrammeLT[e.num].yar
    progConst.yaaaazom = stor.ProgrammeLT[e.num].yaz
    progConst.nbAt = stor.ProgrammeLT[e.num].nbAt
    progConst.zoomin = stor.ProgrammeLT[e.num].zoomin
    progConst.zoomax = stor.ProgrammeLT[e.num].zoomax
    progConst.limcercle = stor.ProgrammeLT[e.num].limcercle
    progConst.limseg = stor.ProgrammeLT[e.num].nbseg
    progConst.limdte = stor.ProgrammeLT[e.num].nbdte
    progConst.limddte = stor.ProgrammeLT[e.num].nbddte
    progConst.limarc = stor.ProgrammeLT[e.num].nbarc
    progConst.devmov = false

    progConst.ptCo = []
    progConst.segCo = []
    progConst.demiDco = []
    progConst.arcco = []
    progConst.dteco = []
    progConst.clignEff = []
    progConst.nomco = []
    progConst.okmouv = true
    progConst.onco = false
    progConst.nbseg = 0
    progConst.nbdte = -1
    progConst.nbddte = -1
    progConst.nbarc = -1
    progConst.nomprisProg = []
    progConst.nomspris = []
    stor.condVerif = []
    stor.existeVerif = []
    progConst.longcalculs = []
    progConst.nomDebSeg = []
    progConst.nomDebDte = []
    progConst.nomPosProg = ['A', 'B', 'C', 'D', 'E', 'F', 'G', 'H', 'I', 'J', 'K', 'L', 'M', 'N', 'O', 'P', 'Q', 'R', 'S', 'T', 'U', 'V', 'W', 'X', 'Y', 'Z']
    progConst.nomPosProg = j3pShuffle(progConst.nomPosProg)
    let a, b, text, bud, haz
    stor.hasard2 = []
    stor.MonMenu = []
    for (a = 0; a < progConst.progfig.length; a++) {
      if (progConst.progfig[a].type === 'point') {
        for (let i = 0; i < progConst.nomPosProg.length; i++) {
          if (progConst.nomPosProg[i] === progConst.progfig[a].nom) {
            progConst.nomPosProg.splice(i, 1)
            break
          }
        }
      }
    }
    for (a = 0; a < progConst.Programme.length; a++) {
      if (progConst.Programme[a].type === 'point') {
        if (progConst.Programme[a].nom.includes('Hz')) {
          for (let i = 0; i < progConst.nomPosProg.length; i++) {
            if (progConst.nomPosProg[i] === progConst.Programme[a].nom) {
              progConst.nomPosProg.splice(i, 1)
              break
            }
          }
        }
      }
      if (progConst.Programme[a].type === 'multi') {
        for (let j = 0; j < progConst.Programme[a].cont.length; j++) {
          if (progConst.Programme[a].cont[j].type === 'point') {
            if (progConst.Programme[a].cont[j].nom.includes('Hz')) {
              for (let i = 0; i < progConst.nomPosProg.length; i++) {
                if (progConst.nomPosProg[i] === progConst.Programme[a].cont[j].nom) {
                  progConst.nomPosProg.splice(i, 1)
                  break
                }
              }
            }
          }
        }
      }
    }
    const tb = []
    for (a = 0; a < progConst.progfig.length; a++) {
      if (progConst.progfig[a].type === 'point') {
        if (progConst.progfig[a].nom.includes('Hz')) {
          progConst.nomprisProg.push({ ki: progConst.progfig[a].nom, com: progConst.nomPosProg.pop() })
        }
        tb.push({ n: progConst.progfig[a].nom, x: parseFloat(progConst.progfig[a].cox), y: parseFloat(progConst.progfig[a].coy) })
      }
    }

    progConst.unite = 27.33576561109424
    for (let i = 0; i < tb.length - 1; i++) {
      for (let j = i + 1; j < tb.length; j++) {
        progConst.longcalculs.push({
          ki: tb[i].n + tb[j].n,
          form: progConst.dist({ x: tb[i].x, y: tb[i].y }, { x: tb[j].x, y: tb[j].y }) / progConst.unite
        })
        progConst.longcalculs.push({
          ki: tb[j].n + tb[i].n,
          form: progConst.dist({ x: tb[i].x, y: tb[i].y }, { x: tb[j].x, y: tb[j].y }) / progConst.unite
        })
      }
    }
    if (progConst.yaaaazom) {
      progConst.modiggg = (j3pGetRandomInt(progConst.zoomin * 100, progConst.zoomax * 100) / 100)
    } else {
      progConst.modiggg = 1
    }
    for (let i = 0; i < progConst.longcalculs.length; i++) {
      progConst.longcalculs[i].form = j3pArrondi(progConst.longcalculs[i].form * progConst.modiggg, 1)
    }

    let nombuf, laval
    for (a = 0; a < progConst.Programme.length; a++) {
      if (progConst.Programme[a].type !== 'multi') {
        if (progConst.Programme[a].type === 'point') {
          if (progConst.Programme[a].nom.includes('Hz')) {
            progConst.nomprisProg.push({ ki: progConst.Programme[a].nom, com: progConst.nomPosProg.pop() })
          }
        }
        if (progConst.Programme[a].type === 'segment') progConst.nbseg++
        text = progConst.Programme[a].text
        /// / ici gere hasard des cond
        bud = []
        nombuf = progConst.Programme[a].nom
        for (let i = 0; i < progConst.nomprisProg.length; i++) {
          const regex = new RegExp(progConst.nomprisProg[i].ki, 'g')
          text = text.replace(regex, progConst.nomprisProg[i].com)
          nombuf = nombuf.replace(regex, progConst.nomprisProg[i].com)
        }
        if (progConst.Programme[a].cond !== undefined) {
          let contlong = 0
          for (let i = 0; i < progConst.Programme[a].cond.length; i++) {
            stor.condVerif.push(progConst.Programme[a].cond[i])
            stor.condVerif[stor.condVerif.length - 1].etape = a
            stor.condVerif[stor.condVerif.length - 1].pour = nombuf
            if (progConst.Programme[a].cond[i].err) stor.condVerif[stor.condVerif.length - 1].err = progConst.Programme[a].cond[i].err
            if (progConst.Programme[a].cond[i].t === 'ap') {
              stor.condVerif[stor.condVerif.length - 1].pt = nombuf
            }
            if (progConst.Programme[a].cond[i].t === 'long') {
              stor.condVerif[stor.condVerif.length - 1].co1 = nombuf
              bud.push({ formule: progConst.recupe(progConst.Programme[a].cond[i].formule) })
              if (progConst.Programme[a].cond[i].formule2 !== undefined) {
                bud[bud.length - 1].formule2 = progConst.recupe(progConst.Programme[a].cond[i].formule2)
              }
              if (bud[bud.length - 1].formule.includes('haz')) {
                haz = j3pGetRandomInt(progConst.Programme[a].cond[i].hazmin, progConst.Programme[a].cond[i].hazmax)
                // eslint-disable-next-line prefer-regex-literals
                const regex = new RegExp('haz', 'g')
                bud[bud.length - 1].formule = bud[bud.length - 1].formule.replace(regex, haz)
              }
              if (bud[bud.length - 1].formule.includes('à')) {
                haz = j3pGetRandomInt(0, 100) / 100
                // eslint-disable-next-line prefer-regex-literals
                const regex = new RegExp('à', 'g')
                bud[bud.length - 1].formule = bud[bud.length - 1].formule.replace(regex, haz)
              }
              if (progConst.Programme[a].cond[i].affiche !== 'formule') {
                const mesx = []
                for (let j = 0; j < progConst.longcalculs.length; j++) {
                  mesx.push({ ki: progConst.longcalculs[j].ki, koi: progConst.longcalculs[j].form })
                }
                for (let j = 0; j < mesx.length; j++) {
                  const regex2 = new RegExp(mesx[j].ki, 'g')
                  bud[bud.length - 1].formule = bud[bud.length - 1].formule.replace(regex2, mesx[j].koi)
                }
                while (bud[bud.length - 1].formule.includes('\\times')) {
                  bud[bud.length - 1].formule = bud[bud.length - 1].formule.replace('\\times', '*')
                }
                if (bud[bud.length - 1].formule.includes('\\frac{')) {
                  bud[bud.length - 1].formule = '(' + bud[bud.length - 1].formule.substring(6, bud[bud.length - 1].formule.indexOf('}{')) + ')/(' + bud[bud.length - 1].formule.substring(bud[bud.length - 1].formule.indexOf('}{') + 2, bud[bud.length - 1].formule.length - 1) + ')'
                }
                bud[bud.length - 1].formule = progConst.calcule(bud[bud.length - 1].formule)
                if (bud[bud.length - 1].formule.includes('/')) {
                  bud[bud.length - 1].formule = String(j3pArrondi(parseInt(bud[bud.length - 1].formule.substring(0, bud[bud.length - 1].formule.indexOf('/'))) / parseInt(bud[bud.length - 1].formule.substring(bud[bud.length - 1].formule.indexOf('/') + 1)), 1))
                }
                bud[bud.length - 1].formule = String(j3pArrondi(parseFloat(bud[bud.length - 1].formule), 1))
              }

              let nombufco = progConst.Programme[a].cond[i].cop
              for (let j = 0; j < progConst.nomprisProg.length; j++) {
                const regex = new RegExp('£' + progConst.nomprisProg[j].ki, 'g')
                nombufco = nombufco.replace(regex, progConst.nomprisProg[j].com)
              }
              progConst.longcalculs.push({ ki: nombuf + nombufco, form: bud[bud.length - 1].formule })
              progConst.longcalculs.push({ ki: nombufco + nombuf, form: bud[bud.length - 1].formule })
              const regex2 = new RegExp('§' + contlong + '§', 'g')
              if (bud[bud.length - 1].formule2 !== undefined) {
                text = text.replace(regex2, progConst.calcule(bud[bud.length - 1].formule2.replace('f', bud[bud.length - 1].formule)).replace('.', ','))
              } else {
                text = text.replace(regex2, bud[bud.length - 1].formule.replace('.', ','))
              }
              if (i === 1) {
                // eslint-disable-next-line prefer-regex-literals
                const regex2 = new RegExp('§1§', 'g')
                if (bud[bud.length - 1].formule2 !== undefined) {
                  text = text.replace(regex2, progConst.calcule(bud[bud.length - 1].formule2.replace('f', bud[bud.length - 1].formule)).replace('.', ','))
                } else {
                  text = text.replace(regex2, bud[bud.length - 1].formule.replace('.', ','))
                }
              }
              stor.condVerif[stor.condVerif.length - 1].cb = bud[bud.length - 1].formule
              contlong++
            }
            if (progConst.Programme[a].cond[i].t === 'ang') {
              stor.condVerif[stor.condVerif.length - 1].co1 = nombuf
              bud.push({ formule: progConst.Programme[a].cond[i].formule })
              bud[bud.length - 1].formule = progConst.calculFormule(bud[bud.length - 1].formule, progConst.Programme[a].cond[i].affiche, progConst.Programme[a].cond[i].hazmin, progConst.Programme[a].cond[i].hazmax)
              const regex2 = new RegExp('§' + contlong + '§', 'g')
              text = text.replace(regex2, bud[bud.length - 1].formule.replace('.', ','))
              if (i === 1) {
                // eslint-disable-next-line prefer-regex-literals
                const regex2 = new RegExp('§1§', 'g')
                text = text.replace(regex2, bud[bud.length - 1].formule.replace('.', ','))
              }
              text = text.replace('µµ', '$\\widehat{' + progConst.recupe(progConst.Programme[a].nom + progConst.Programme[a].cond[i].cop + progConst.Programme[a].cond[i].cop2) + '}$')
              if (text.indexOf('µ0µ') !== -1) text = text.replace('µ0µ', '$\\widehat{' + progConst.recupe(progConst.Programme[a].nom + progConst.Programme[a].cond[0].cop + progConst.Programme[a].cond[0].cop2) + '}$')
              if (text.indexOf('µ1µ') !== -1) text = text.replace('µ1µ', '$\\widehat{' + progConst.recupe(progConst.Programme[a].nom + progConst.Programme[a].cond[1].cop + progConst.Programme[a].cond[1].cop2) + '}$')
              stor.condVerif[stor.condVerif.length - 1].cb = bud[bud.length - 1].formule
              contlong++
            }
          }
        }
        if (progConst.Programme[a].type === 'cercle') {
          if (progConst.Programme[a].mode === 'rayon') {
            if (progConst.Programme[a].affiche === 'valeur') {
              laval = progConst.calculFormule(progConst.Programme[a].rayon, true, progConst.Programme[a].hazmin, progConst.Programme[a].hazmax)
            } else {
              laval = progConst.recupe(progConst.Programme[a].rayon)
            }
            text = text.replace('£0£', laval)
          } else {
            laval = progConst.Programme[a].centre + progConst.Programme[a].par
            laval = progConst.recupe(laval)
          }
        }
        stor.existeVerif.push({ type: progConst.Programme[a].type, nom: nombuf, etape: a, errOub: progConst.Programme[a].errOub, exa: progConst.Programme[a].exa, pointille: progConst.Programme[a].pointille })
        if (progConst.Programme[a].type === 'cercle') {
          stor.condVerif.push({ t: 'cercle', nom: nombuf, centre: progConst.recupe(progConst.Programme[a].centre), rayon: laval })
          progConst.Programme[a].rayon = laval
        }
        if (progConst.Programme[a].type === 'droite' && !progConst.Programme[a].exa) {
          stor.condVerif.push({
            t: 'droite',
            nom: nombuf,
            condd: progConst.Programme[a].condd,
            pass: progConst.Programme[a].pass,
            pass2: progConst.Programme[a].pass2,
            odt: progConst.Programme[a].odt
          })
          if (progConst.Programme[a].pass2 === undefined) stor.condVerif[stor.condVerif.length - 1].pass2 = ''
          if (progConst.Programme[a].odt === undefined) stor.condVerif[stor.condVerif.length - 1].odt = ''
          stor.condVerif[stor.condVerif.length - 1].pass = progConst.recupe(stor.condVerif[stor.condVerif.length - 1].pass)
          stor.condVerif[stor.condVerif.length - 1].pass2 = progConst.recupe(stor.condVerif[stor.condVerif.length - 1].pass2)
          stor.condVerif[stor.condVerif.length - 1].odt = progConst.recupe(stor.condVerif[stor.condVerif.length - 1].odt)
        }
      } else {
        text = progConst.Programme[a].text
        let contlong = 0
        for (b = 0; b < progConst.Programme[a].cont.length; b++) {
          if (progConst.Programme[a].cont[b].type === 'point') {
            if (progConst.Programme[a].cont[b].nom.includes('Hz')) {
              progConst.nomprisProg.push({ ki: progConst.Programme[a].cont[b].nom, com: progConst.nomPosProg.pop() })
            }
          }
          if (progConst.Programme[a].cont[b].type === 'segment') progConst.nbseg++
          /// / ici gere hasard des cond
          bud = []
          if (progConst.Programme[a].cont[b].type !== 'af') {
            nombuf = progConst.recupe(progConst.Programme[a].cont[b].nom)
          }
          if (progConst.Programme[a].cont[b].cond !== undefined) {
            for (let i = 0; i < progConst.Programme[a].cont[b].cond.length; i++) {
              stor.condVerif.push(progConst.Programme[a].cont[b].cond[i])
              stor.condVerif[stor.condVerif.length - 1].etape = a
              stor.condVerif[stor.condVerif.length - 1].pour = nombuf
              if (progConst.Programme[a].cont[b].need === false) stor.condVerif[stor.condVerif.length - 1].noneed = true
              if (progConst.Programme[a].cont[b].cond[i].t === 'long') {
                stor.condVerif[stor.condVerif.length - 1].co1 = nombuf
                bud.push({ formule: progConst.recupe(progConst.Programme[a].cont[b].cond[i].formule) })
                if (progConst.Programme[a].cont[b].cond[i].formule2 !== undefined) {
                  bud[bud.length - 1].formule2 = progConst.recupe(progConst.Programme[a].cont[b].cond[i].formule2)
                }
                if (bud[bud.length - 1].formule.includes('haz')) {
                  haz = j3pGetRandomInt(progConst.Programme[a].cont[b].cond[i].hazmin, progConst.Programme[a].cont[b].cond[i].hazmax)
                  // eslint-disable-next-line prefer-regex-literals
                  const regex = new RegExp('haz', 'g')
                  bud[bud.length - 1].formule = bud[bud.length - 1].formule.replace(regex, haz)
                }
                if (bud[bud.length - 1].formule.includes('à')) {
                  haz = j3pGetRandomInt(0, 100) / 100
                  // eslint-disable-next-line prefer-regex-literals
                  const regex = new RegExp('à', 'g')
                  bud[bud.length - 1].formule = bud[bud.length - 1].formule.replace(regex, haz)
                }
                if (progConst.Programme[a].cont[b].cond[i].affiche !== 'formule') {
                  const mesx = []
                  for (let j = 0; j < progConst.longcalculs.length; j++) {
                    mesx.push({ ki: progConst.longcalculs[j].ki, koi: progConst.longcalculs[j].form, ki2: progConst.recupe(progConst.longcalculs[j].ki) })
                  }
                  for (let j = 0; j < mesx.length; j++) {
                    let regex2 = new RegExp(mesx[j].ki, 'g')
                    bud[bud.length - 1].formule = bud[bud.length - 1].formule.replace(regex2, mesx[j].koi)
                    regex2 = new RegExp(mesx[j].ki2, 'g')
                    bud[bud.length - 1].formule = bud[bud.length - 1].formule.replace(regex2, mesx[j].koi)
                    if (mesx[j].ki2.length > 1) {
                      regex2 = new RegExp(mesx[j].ki2[1] + mesx[j].ki2[0], 'g')
                      bud[bud.length - 1].formule = bud[bud.length - 1].formule.replace(regex2, mesx[j].koi)
                    }
                  }
                  while (bud[bud.length - 1].formule.includes('\\times')) {
                    bud[bud.length - 1].formule = bud[bud.length - 1].formule.replace('\\times', '*')
                  }
                  if (bud[bud.length - 1].formule.includes('\\frac{')) {
                    bud[bud.length - 1].formule = '(' + bud[bud.length - 1].formule.substring(6, bud[bud.length - 1].formule.indexOf('}{')) + ')/(' + bud[bud.length - 1].formule.substring(bud[bud.length - 1].formule.indexOf('}{') + 2, bud[bud.length - 1].formule.length - 1) + ')'
                  }
                  bud[bud.length - 1].formule = progConst.calcule(bud[bud.length - 1].formule)
                  if (bud[bud.length - 1].formule.includes('/')) {
                    bud[bud.length - 1].formule = String(j3pArrondi(parseInt(bud[bud.length - 1].formule.substring(0, bud[bud.length - 1].formule.indexOf('/'))) / parseInt(bud[bud.length - 1].formule.substring(bud[bud.length - 1].formule.indexOf('/') + 1)), 1))
                  }
                  bud[bud.length - 1].formule = String(j3pArrondi(parseFloat(bud[bud.length - 1].formule), 1))
                }
                let nombufco = progConst.Programme[a].cont[b].cond[i].cop
                for (let j = 0; j < progConst.nomprisProg.length; j++) {
                  const regex = new RegExp('£' + progConst.nomprisProg[j].ki, 'g')
                  nombufco = nombufco.replace(regex, progConst.nomprisProg[j].com)
                }
                progConst.longcalculs.push({ ki: nombuf + nombufco, form: bud[bud.length - 1].formule })
                progConst.longcalculs.push({ ki: nombufco + nombuf, form: bud[bud.length - 1].formule })
                // const regex2 = new RegExp('§' + i + '§', 'g')
                if (bud[bud.length - 1].formule2 !== undefined) {
                  text = text.replace('§' + contlong + '§', progConst.calcule(bud[bud.length - 1].formule2.replace('f', bud[bud.length - 1].formule)).replace('.', ','))
                } else {
                  if (progConst.Programme[a].cont[b].cond[i].paf !== true) text = text.replace('§' + contlong + '§', bud[bud.length - 1].formule.replace('.', ','))
                }
                stor.condVerif[stor.condVerif.length - 1].cb = bud[bud.length - 1].formule
                for (mu = 0; mu < artebuf.length; mu++) {
                  if (artebuf[mu].nom === stor.condVerif[stor.condVerif.length - 1].cb || artebuf[mu].nom2 === stor.condVerif[stor.condVerif.length - 1].cb) {
                    stor.condVerif[stor.condVerif.length - 1].cb = artebuf[mu].val
                  }
                }
                artebuf.push({
                  nom: progConst.recupe(stor.condVerif[stor.condVerif.length - 1].co1 + stor.condVerif[stor.condVerif.length - 1].cop),
                  nom2: progConst.recupe(stor.condVerif[stor.condVerif.length - 1].cop + stor.condVerif[stor.condVerif.length - 1].co1),
                  val: stor.condVerif[stor.condVerif.length - 1].cb
                })
                contlong++
              }
              if (progConst.Programme[a].cont[b].cond[i].t === 'ap') {
                stor.condVerif[stor.condVerif.length - 1].pt = nombuf
              }
              if (progConst.Programme[a].cont[b].cond[i].t === 'ang') {
                stor.condVerif[stor.condVerif.length - 1].co1 = nombuf
                bud.push({ formule: progConst.Programme[a].cont[b].cond[i].formule })
                if (bud[bud.length - 1].formule.includes('¤')) {
                  for (om = stor.condVerif.length - 1; om > -1; om--) {
                    bud[bud.length - 1].formule = bud[bud.length - 1].formule.replace('¤' + om, stor.condVerif[om].cb)
                  }
                }
                bud[bud.length - 1].formule = progConst.calculFormule(bud[bud.length - 1].formule, progConst.Programme[a].cont[b].cond[i].affiche, progConst.Programme[a].cont[b].cond[i].hazmin, progConst.Programme[a].cont[b].cond[i].hazmax)
                text = text.replace('§' + contlong + '§', bud[bud.length - 1].formule.replace('.', ','))
                text = text.replace('µµ', '$\\widehat{' + progConst.recupe(progConst.Programme[a].cont[b].nom + progConst.Programme[a].cont[b].cond[i].cop + progConst.Programme[a].cont[b].cond[i].cop2) + '}$')
                if (text.indexOf('µ0µ') !== -1) text = text.replace('µ0µ', '$\\widehat{' + progConst.recupe(progConst.Programme[a].cont[b].nom + progConst.Programme[a].cont[b].cond[0].cop + progConst.Programme[a].cont[b].cond[0].cop2) + '}$')
                if (text.indexOf('µ1µ') !== -1) text = text.replace('µ1µ', '$\\widehat{' + progConst.recupe(progConst.Programme[a].cont[b].nom + progConst.Programme[a].cont[b].cond[1].cop + progConst.Programme[a].cont[b].cond[1].cop2) + '}$')
                stor.condVerif[stor.condVerif.length - 1].cb = bud[bud.length - 1].formule
                contlong++
              }
            }
          }
          if (progConst.Programme[a].cont[b].type === 'cercle') {
            if (progConst.Programme[a].cont[b].mode === 'rayon') {
              if (progConst.Programme[a].cont[b].affiche === 'valeur') {
                laval = progConst.calculFormule(progConst.Programme[a].cont[b].rayon, true, progConst.Programme[a].cont[b].hazmin, progConst.Programme[a].cont[b].hazmax)
              } else {
                laval = progConst.Programme[a].cont[b].rayon
                for (let i = 0; i < progConst.nomprisProg.length; i++) {
                  const regex = new RegExp(progConst.nomprisProg[i].ki, 'g')
                  laval = laval.replace(regex, progConst.nomprisProg[i].com)
                }
              }
              text = text.replace('£0£', laval)
            } else {
              laval = progConst.Programme[a].cont[b].centre + progConst.Programme[a].cont[b].par
              for (let i = 0; i < progConst.nomprisProg.length; i++) {
                const regex = new RegExp(progConst.nomprisProg[i].ki, 'g')
                laval = laval.replace(regex, progConst.nomprisProg[i].com)
              }
            }
          }
          if (progConst.Programme[a].cont[b].need !== false) {
            stor.existeVerif.push({
              type: progConst.Programme[a].cont[b].type,
              nom: nombuf,
              etape: a,
              errOub: progConst.Programme[a].cont[b].errOub,
              pointille: progConst.Programme[a].cont[b].pointille
            })
          }
          if (progConst.Programme[a].cont[b].type === 'cercle') {
            stor.condVerif.push({ t: 'cercle', nom: nombuf, centre: progConst.recupe(progConst.Programme[a].cont[b].centre), rayon: laval })
            if (progConst.Programme[a].cont[b].need === false) stor.condVerif[stor.condVerif.length - 1].noneed = true
          }
          if (progConst.Programme[a].cont[b].type === 'droite' && progConst.Programme[a].cont[b].need !== false) {
            stor.condVerif.push({
              t: 'droite',
              nom: nombuf,
              condd: progConst.Programme[a].cont[b].condd,
              pass: progConst.Programme[a].cont[b].pass,
              pass2: progConst.Programme[a].cont[b].pass2,
              odt: progConst.Programme[a].cont[b].odt,
              err: progConst.Programme[a].cont[b].err
            })
            if (progConst.Programme[a].cont[b].pass2 === undefined) stor.condVerif[stor.condVerif.length - 1].pass2 = ''
            if (progConst.Programme[a].cont[b].odt === undefined) stor.condVerif[stor.condVerif.length - 1].odt = ''
            stor.condVerif[stor.condVerif.length - 1].pass = progConst.recupe(stor.condVerif[stor.condVerif.length - 1].pass)
            stor.condVerif[stor.condVerif.length - 1].pass2 = progConst.recupe(stor.condVerif[stor.condVerif.length - 1].pass2)
            stor.condVerif[stor.condVerif.length - 1].odt = progConst.recupe(stor.condVerif[stor.condVerif.length - 1].odt)
          }
        }
      }
      /// /
      text = progConst.recupe(text)
      stor.MonMenu.push(
        {
          name: 'men' + a,
          label: (a + 1) + '♦  ' + text,
          callback: function (menu/*, choice, event */) {
          }
        }
      )
    }
    if (stor.MonMenu.length > 1) {
      const decoupe = {
        callback: decoupeMenu,
        label: '<i><b>Clique ici pour détacher le programme</b></i>',
        name: 'men00'
      }
      stor.ledivdetavhe = j3pAddElt(stor.lesdiv.consigne, 'div')
      j3pAjouteBouton(stor.ledivdetavhe, cacheProg, { className: '', value: 'Cacher le Programme' })
      j3pAffiche(stor.ledivdetavhe, null, '\n')
      for (let i = 0; i < stor.MonMenu.length; i++) {
        j3pAffiche(stor.ledivdetavhe, null, stor.MonMenu[i].label.replace('AZERTYUIOPNN', '') + '\n')
      }
      stor.ledivdetavhe.style.display = 'none'
      stor.MonMenu.splice(0, 0, decoupe)
    }
    progConst.condVerif = j3pClone(stor.condVerif)
    progConst.listeSegPris = []
    progConst.listePointPris = []
    progConst.listeDtePris = []
    progConst.listeDemiDPris = []
    progConst.listeArcPris = []

    progConst.zoom = 1
    if (progConst.mondivBUlle !== undefined) {
      j3pDetruit(progConst.mondivBUlle)
      progConst.mondivBUlle = undefined
    }

    return e
  }
  function poseQuestion () {
    stor.divConsigneMenu = j3pAddElt(stor.lesdiv.consigne, 'div')
    if (stor.MonMenu.length === 1) {
      j3pAffiche(stor.divConsigneMenu, '', stor.MonMenu[0].label)
    } else {
      j3pAffiche(stor.divConsigneMenu, '', 'Clique <b>ici</b> pour voir le programme de construction à réaliser')
      stor.unmenu = new MenuContextuel(stor.divConsigneMenu, stor.MonMenu)
    }
    stor.lesdiv.travail.classList.add('travail')
    stor.lesdiv.consigne.classList.add('enonce')
    stor.lesdiv.explications.classList.remove('explique')
    stor.lesdiv.solution.classList.remove('correction')
    j3pAjouteBouton(stor.lesdiv.coboot, progConst.affaide.bind(progConst), { className: '', value: 'Aide' })
    if (progConst.nbAt) {
      j3pAffiche(stor.lesdiv.consigne, null, '<i>Dans cet exercice, il faut effacer tous les traits de construction.</i>', { style: { color: '#ee21ff' } })
    }
    stor.vpi = false
  }
  function cacheProg () {
    stor.divConsigneMenu.style.display = ''
    stor.ledivdetavhe.style.display = 'none'
  }
  function decoupeMenu () {
    stor.divConsigneMenu.style.display = 'none'
    stor.ledivdetavhe.style.display = ''
  }
  function creeLesDiv () {
    stor.lesdiv = {}
    stor.lesdiv.conteneur = j3pAddElt(me.zones.MG, 'div', {
      style: me.styles.etendre('toutpetit.enonce', { padding: '10px' })
    })
    stor.lesdiv.consignet = j3pAddElt(stor.lesdiv.conteneur, 'div')
    const tavg = addDefaultTable(stor.lesdiv.consignet, 1, 3)
    tavg[0][1].style.width = '40px'
    stor.lesdiv.consigne = tavg[0][0]
    progConst.consigne = stor.lesdiv.consigne
    stor.lesdiv.coboot = tavg[0][2]
    progConst.coboot = stor.lesdiv.coboot
    progConst.leDivBull = j3pAddElt(stor.lesdiv.conteneur, 'div')
    stor.lesdiv.travail1 = j3pAddElt(stor.lesdiv.conteneur, 'div')
    const tavgZ = addDefaultTable(stor.lesdiv.travail1, 1, 3)
    tavgZ[0][1].style.width = '40px'
    stor.lesdiv.travail = tavgZ[0][0]
    if (ds.dev) {
      const tavf = addDefaultTable(tavgZ[0][2], 2, 1)
      stor.lesdiv.correction = tavf[0][0]
      stor.lesdiv.dev = tavf[1][0]
      progConst.dev = stor.lesdiv.dev
    } else {
      stor.lesdiv.correction = tavgZ[0][2]
    }
    stor.lesdiv.zonefig = j3pAddElt(stor.lesdiv.travail, 'div')
    progConst.zonefig = stor.lesdiv.zonefig
    progConst.divparent = stor.lesdiv.conteneur
    stor.lesdiv.zonedrep = tavgZ[0][1]
    progConst.whereAfficheAide = j3pAddElt(stor.lesdiv.travail, 'div')
    progConst.whereAfficheAide.style.display = 'none'
    progConst.whereAfficheAide.style.border = '5px solid black'
    progConst.whereAfficheAide.style.padding = '10px'
    progConst.whereAfficheAide.style.borderRadius = '4px'
    progConst.whereAfficheAide.style.background = '#23887d'
    stor.lesdiv.explications = j3pAddElt(stor.lesdiv.travail, 'div')
    stor.lesdiv.solution = j3pAddElt(stor.lesdiv.travail, 'div')
    stor.lesdiv.explications.style.color = me.styles.cfaux
    stor.lesdiv.solution.style.color = me.styles.toutpetit.correction.color
    me.zonesElts.MG.classList.add('fond')
  }

  function yaReponse () {
    return true
  }

  function enonceMain () {
    progConst.raz = true
    if (((me.questionCourante % ds.nbetapes) === 1) || (ds.nbetapes === 1)) {
      // A chaque répétition, on nettoie la scène
      me.videLesZones()
      creeLesDiv()
      stor.Lexo = faisChoixExos()
      stor.lesdiv.travail.classList.add('travail')
      stor.lesdiv.consigne.classList.add('enonce')
      stor.lesdiv.solution.classList.remove('correction')
      stor.lesdiv.explications.classList.remove('explique')
    }

    poseQuestion()
    progConst.makefig()
    stor.lesdiv.travail.style.minWidth = '820px'
    me.finEnonce()
  }

  switch (this.etat) {
    case 'enonce':
      // code exécuté au lancement de la section
      if (me.debutDeLaSection) {
        initSection()
      } else {
        enonceMain()
      }

      break // case "enonce":

    case 'correction':
      // On teste si une réponse a été saisie
      if (progConst.unecaptOK()) return
      stor.yaexplik = false
      if (stor.lesdiv === undefined) return
      stor.lesdiv.explications.innerHTML = ''
      stor.lesdiv.correction.innerHTML = ''
      stor.yaco = false
      j3pEmpty(progConst.whereAfficheAide)
      progConst.whereAfficheAide.style.display = 'none'

      if (yaReponse()) {
        // Bonne réponse
        if (isRepOkConst(stor, progConst, ds)) {
          this._stopTimer()
          stor.lesdiv.correction.style.color = this.styles.cbien
          stor.lesdiv.correction.innerHTML = cBien

          progConst.desactivezone()
          progConst.faisFond(true)
          progConst.colorTout(true)
          this.cacheBoutonValider()
          this.score++

          this.typederreurs[0]++
          this.etat = 'navigation'
          this.sectionCourante()
        } else {
          // Pas de bonne réponse
          stor.lesdiv.correction.style.color = this.styles.cfaux
          stor.lesdiv.correction.innerHTML = cFaux
          if (this.isElapsed) {
            // A cause de la limite de temps :
            this._stopTimer()

            stor.lesdiv.correction.innerHTML += tempsDepasse + '<BR>'
            this.typederreurs[10]++
            this.cacheBoutonValider()

            affCorrectionConst(true, stor, progConst)

            this.etat = 'navigation'
            this.sectionCourante()
          } else {
            // Réponse fausse :
            if (this.essaiCourant < ds.nbchances) {
              stor.lesdiv.correction.innerHTML += '<br>' + essaieEncore

              affCorrectionConst(false, stor, progConst)

              this.typederreurs[1]++
            } else {
              // Erreur au nème essai
              this._stopTimer()
              this.cacheBoutonValider()

              affCorrectionConst(true, stor, progConst)

              if (stor.encours === 'p') {
                stor.pose = false
                stor.encours = 't'
              }

              this.typederreurs[2]++
              this.etat = 'navigation'
              this.sectionCourante()
            }
          }
          me.zonesElts.MG.scrollTop = 1000
        }
      } else {
        // pas de réponse
        stor.lesdiv.correction.style.color = this.styles.cfaux
        stor.lesdiv.correction.innerHTML = reponseIncomplete
        me.afficheBoutonValider()
      }

      if (ds.theme === 'zonesAvecImageDeFond') {
        if (stor.yaexplik) {
          stor.lesdiv.explications.classList.add('explique')
        }
        if (stor.yaco) {
          stor.lesdiv.solution.classList.add('correction')
        }
      }
      this.finCorrection()
      break // case "correction":

    case 'navigation':
      if (!this.sectionTerminee()) {
        this.etat = 'enonce'
        me.buttonsElts.suite.addEventListener('click', () => {
          progConst.raz = true
        })
      } else {
        me.buttonsElts.sectionSuivante.addEventListener('click', () => {
          progConst.raz = true
          j3pEmpty(stor.lesdiv.conteneur)
        })
      }
      this.finNavigation()
      break // case "navigation":
  }
}
