import { j3pAddElt, j3pAjouteBouton, j3pArrondi, j3pEmpty, j3pGetNewId, j3pGetRandomInt, j3pGetRandomLetters, j3pNombreBienEcrit, j3pShowError, j3pShuffle } from 'src/legacy/core/functions'
import { colorKo, colorOk } from 'src/legacy/core/StylesJ3p'
import ListeDeroulante from 'src/legacy/outils/listeDeroulante/ListeDeroulante'
import { j3pAffiche } from 'src/lib/mathquill/functions'
import { addDefaultTable } from 'src/legacy/themes/table'
import { j3pCreeSVG } from 'src/legacy/core/functionsSvg'
import { j3pAfficheCroixFenetres, j3pCreeFenetres, j3pToggleFenetres } from 'src/legacy/core/functionsJqDialog'
import textesGeneriques from 'src/lib/core/textes'
import { getMtgCore } from 'src/lib/outils/mathgraph'

const { cBien, essaieEncore, tempsDepasse } = textesGeneriques

/**
 * Ajouter une description ici
 * @fileOverview
 * @author Tommy Barroy <t.barroy@laposte.net>
 * @since 2017-10
*/

export const params = {
  outils: ['calculatrice', 'mathquill'],
  /**
   * Ce qui est paramétrable dans l’éditeur de graphe
   * @type {LegacySectionParamsParametres}
   */
  parametres: [
    ['nbrepetitions', 9, 'entier', 'Nombre de répétitions de la section'],
    ['limite', 0, 'entier', 'Temps disponible par répétition, en secondes'],
    ['nbchances', 1, 'entier', 'Nombre d’essais par répétition'],
    ['unites', 'non', 'liste', '<u>oui</u>: Changements d’unité possibles', ['oui', 'non', 'les deux']],
    ['schema', 'parfois', 'liste', '<u>oui</u>: un schéma est disponible', ['oui', 'non', 'parfois']],
    ['langage', 'français', 'liste', '<u>français</u> Pour une réponse en français <BR><br> <u>mathématique</u> pour une réponse en langage mathématique', ['français', 'mathématique', 'les deux']],
    ['diametre', 'non', 'liste', '<u>oui</u> Un cercle peut être défini à l’aide de son diamètre.', ['oui', 'non', 'les deux']],
    ['Calculatrice', false, 'boolean', '<u>true</u>:  Une calculatrice est disponible'],
    ['theme', 'standard', 'liste', 'Modèle pour l’affichage', ['standard', 'zonesAvecImageDeFond']]

  ]
}

const textes = {
  consigne1: ['(£a) est un cercle de centre £e et de £b £c £d .'],
  consigne2: ['Le point £a est à £b £c du point £d .', '£a est un point tel que £a£d = £b £c'],
  rayon: 'rayon',
  diametre: 'diamètre',
  liste1: ['Choisir', 'appartient', 'n’appartient pas'],
  liste2: ['Choisir', 'à l’intérieur', 'à l’extérieur'],
  repdeb: ['Le point £a ', '£a '],
  repfin: ['au cercle (£a) .', '(£a)'],
  rep2deb: 'Le point £a est ',
  rep2fin: ' du cercle (£a) .',
  unite: ['km', 'hm', 'dam', 'm', 'dm', 'cm', 'mm'],
  DiamCo1: '$ Rayon $',
  DiamCo2: '$ = \\frac{diametre}{2} $',
  DiamCo3: '$ = \\frac{£a}{2} $',
  DiamCo4: '$ = £b $ £c ',
  UnitCo1: 'Je convertis:',
  UnitCo2: '$£a£b = £c $ £d',
  UnitCo3: '$£a£b = £e $ £f',
  Co1: 'La distance du point £b au centre £a est £c rayon du cercle (£e) ,',
  Co2: 'donc le point £b £d cercle (£e).',
  mapos: ['est à l’intérieur du ', 'est à l’extérieur du ', 'appartient au '],
  macomp: ['plus petite que le ', 'plus grande que le ', 'égale au ']
}

/**
 * Pour la compatibilité ascendante des graphes, avant unites et diametre étaient des booléens
 * @param params
 */
export function upgradeParametres (params) {
  if (typeof params.unites === 'boolean') params.unites = params.unites ? 'oui' : 'non'
  if (typeof params.diametre === 'boolean') params.diametre = params.diametre ? 'oui' : 'non'
}

/**
 * section appartenancecercle
 * @this {Parcours}
 */
export default function main () {
  const me = this
  const ds = me.donneesSection
  const stor = this.storage
  let svgId = stor.svgId

  function gere () {
    if (stor.zoneliste0.reponse === stor.zoneliste0.givenChoices[1]) {
      me.afficheBoutonValider()
      if (stor.zoneliste1) {
        stor.zoneliste1.disable()
        stor.zoneliste1 = undefined
      }
      j3pEmpty(stor.lesdiv.travail2)
    } else {
      me.cacheBoutonValider()
      if (stor.zoneliste1) {
        stor.zoneliste1.disable()
        stor.zoneliste1 = undefined
      }
      j3pEmpty(stor.lesdiv.travail2)
      const tt = addDefaultTable(stor.lesdiv.travail2, 1, 3)
      j3pAffiche(tt[0][0], null, textes.rep2deb.replace('£a', stor.point) + '&nbsp;')
      j3pAffiche(tt[0][2], null, '&nbsp;' + textes.rep2fin, { a: '<i>' + stor.cercle + '</i>' }
      )
      stor.lesdiv.travail2.classList.add('travail')
      stor.zoneliste1 = ListeDeroulante.create(tt[0][1], textes.liste2, { onChange: () => me.afficheBoutonValider(), centre: true })
    }
  }

  function bonneReponse () {
    // cette fonction renvoie true ou false si du moins une réponse a été saisie
    let ok = true
    stor.erreur1 = false
    stor.erreur2 = false
    if (stor.zoneliste0.getReponseIndex() === 1) {
      if (stor.rayon !== stor.distance) {
        ok = false
        stor.erreur1 = true
      }
    } else {
      if (stor.rayon === stor.distance) {
        ok = false
        stor.erreur1 = true
      }
      if (stor.zoneliste1.getReponseIndex() === 1) {
        if (stor.rayon < stor.distance) {
          ok = false
          stor.erreur2 = true
        }
      } else {
        if (stor.rayon > stor.distance) {
          ok = false
          stor.erreur2 = true
        }
      }
    }
    return ok
  }

  function initSection () {
    me.validOnEnter = false // ex donneesSection.touche_entree
    me.construitStructurePage('presentation1bis')
    ds.lesExos = []
    let lp = []
    if (ds.langage === 'français' || ds.langage === 'les deux') lp.push(0)
    if (ds.langage === 'mathématique' || ds.langage === 'les deux') lp.push(1)
    if (lp.length === 0) lp = [0, 1]
    lp = j3pShuffle(lp)
    for (let i = 0; i < ds.nbitems; i++) {
      ds.lesExos.push({ lang: lp[i % lp.length] })
    }
    ds.lesExos = j3pShuffle(ds.lesExos)

    lp = ['ap', 'ap', 'in', 'out']
    lp = j3pShuffle(lp)
    for (let i = 0; i < ds.nbitems; i++) {
      ds.lesExos[i].rep = lp[i % lp.length]
    }
    ds.lesExos = j3pShuffle(ds.lesExos)

    lp = []
    if (ds.diametre === 'oui' || ds.diametre === 'les deux') lp.push(true)
    if (ds.diametre === 'non' || ds.diametre === 'les deux') lp.push(false)
    lp = j3pShuffle(lp)
    for (let i = 0; i < ds.nbitems; i++) {
      ds.lesExos[i].diam = lp[i % lp.length]
    }
    ds.lesExos = j3pShuffle(ds.lesExos)

    lp = []
    if (ds.unites === 'oui' || ds.unites === 'les deux') lp.push(true)
    if (ds.unites === 'non' || ds.unites === 'les deux') lp.push(false)
    lp = j3pShuffle(lp)
    for (let i = 0; i < ds.nbitems; i++) {
      ds.lesExos[i].unites = lp[i % lp.length]
    }
    ds.lesExos = j3pShuffle(ds.lesExos)

    for (let i = 0; i < ds.nbitems; i++) {
      ds.lesExos[i].schema = ds.schema === 'oui' || (ds.schema === 'parfois' && (i + 1) > (ds.nbitems - 1) / 2)
    }

    /*
             Par convention,`
            `   me.typederreurs[0] = nombre de bonnes réponses
                me.typederreurs[1] = nombre de fois où l’élève a utilisé une chance supplémentaire
                me.typederreurs[2] = nombre de mauvaises réponses
                me.typederreurs[10] = nombre de fois où l’élève a eu faux à cause d’un temps limite
                LES AUTRES peuvent être utilisées par la détermination de la pe dans le cas d’une section qualitative
             */
    me.typederreurs = [0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0]

    me.score = 0

    me.afficheTitre('Déterminer si un point appartient à un cercle')

    getMtgCore({ withMathJax: true })
      .then(
        // success
        (mtgAppLecteur) => {
          stor.mtgAppLecteur = mtgAppLecteur
          enonceMain()
        },
        // failure
        (error) => {
          j3pShowError(error, { message: 'mathgraph n’est pas correctement chargé', mustNotify: true })
        })
      // plantage dans enonceMain
      .catch(j3pShowError)
  }

  function faisChoixExo () {
    const e = ds.lesExos.pop()
    stor.langageencours = e.lang
    stor.rep = e.rep
    stor.diam = e.diam
    stor.unites = e.unites
    stor.schema = e.schema
  }

  function enonceMain () {
    me.videLesZones()
    faisChoixExo()

    let i = j3pGetRandomInt(11, 999)
    const jr = j3pGetRandomInt(0, 3)
    stor.rayon = j3pArrondi(i / Math.pow(10, jr), 3)
    stor.uniterayon = textes.unite[j3pGetRandomInt(0, 6)]
    stor.unitedistance = stor.uniterayon
    if (stor.rep === 'ap') { stor.distance = stor.rayon } else {
      i = j3pGetRandomInt(1, 3)
      if (stor.rep === 'out') {
        stor.distance = j3pArrondi(stor.rayon + i / Math.pow(10, jr), 3)
      } else {
        stor.distance = j3pArrondi(stor.rayon - i / Math.pow(10, jr), 3)
      }
    }
    ;[stor.centre, stor.point, stor.cercle] = j3pGetRandomLetters(3)
    stor.lesdiv = {}
    stor.lesdiv.conteneur = j3pAddElt(me.zonesElts.MG, 'div', {
      style: me.styles.etendre('toutpetit.enonce', { padding: '10px' })
    })
    const ty = addDefaultTable(stor.lesdiv.conteneur, 1, 3)
    const ty2 = addDefaultTable(ty[0][0], 3, 1)
    stor.lesdiv.consigne1 = ty2[0][0]
    ty[0][1].style.width = '20px'
    stor.divmatgra = ty[0][2]

    let monb, monc
    if (!stor.diam) {
      monb = textes.rayon
      monc = stor.rayon
    } else {
      monb = textes.diametre
      monc = j3pArrondi(stor.rayon * 2, 3)
    }
    j3pAffiche(stor.lesdiv.consigne1, null, textes.consigne1 + '\n',
      {
        a: '<i>' + stor.cercle + '</i>',
        b: monb,
        c: j3pNombreBienEcrit(monc),
        d: stor.uniterayon,
        e: stor.centre
      })
    stor.monctr = j3pNombreBienEcrit(monc) + ' ' + stor.uniterayon

    if (!stor.unites) {
      stor.distance2 = stor.distance
      stor.unitedistance2 = stor.unitedistance
    } else {
      stor.unitedistance2 = textes.unite[j3pGetRandomInt(0, 6)]
      stor.distance2 = j3pArrondi(stor.distance * Math.pow(10, textes.unite.indexOf(stor.unitedistance2) - textes.unite.indexOf(stor.unitedistance)), 10)
    }
    j3pAffiche(stor.lesdiv.consigne1, null, textes.consigne2[j3pGetRandomInt(0, 1)] + '\n<b>Complète la phrase suivante.</b>',
      {
        a: stor.point,
        b: j3pNombreBienEcrit(stor.distance2),
        c: stor.unitedistance2,
        d: stor.centre
      })

    stor.lesdiv.travail = ty2[1][0]

    const tyu = addDefaultTable(stor.lesdiv.travail, 1, 3)
    j3pAffiche(tyu[0][0], null, textes.repdeb[stor.langageencours].replace('£a', stor.point) + '&nbsp;')
    j3pAffiche(tyu[0][2], null, '&nbsp;' + textes.repfin[stor.langageencours], { a: '<i>' + stor.cercle + '</i>' })
    const MaListe = []
    MaListe.push(textes.liste1)

    MaListe.push(['...', '$\\in$', '$\\notin$'])// MMM
    stor.zoneliste0 = ListeDeroulante.create(tyu[0][1], MaListe[stor.langageencours], { onChange: gere, centre: true })
    stor.zoneliste1 = undefined
    stor.lesdiv.travail2 = ty2[2][0]

    stor.lesdiv.explications = j3pAddElt(stor.lesdiv.conteneur, 'div')
    stor.lesdiv.solution = j3pAddElt(stor.lesdiv.conteneur, 'div')
    stor.lesdiv.calculatrice = j3pAddElt(me.zones.MD, 'div', {
      style: me.styles.etendre('toutpetit.enonce', { padding: '10px' })
    })
    stor.lesdiv.correction = j3pAddElt(me.zones.MD, 'div', {
      style: me.styles.etendre('toutpetit.enonce', { padding: '10px' })
    })
    stor.lesdiv.explications.style.color = colorKo
    stor.lesdiv.solution.style.color = me.styles.toutpetit.correction.color

    me.cacheBoutonValider()
    stor.lesdiv.travail.classList.add('travail')
    stor.lesdiv.consigne1.classList.add('enonce')
    me.zonesElts.MG.classList.add('fond')

    if (stor.schema) {
      j3pAjouteBouton(stor.divmatgra, 'bob', '', 'Voir un schéma', makefig)
      stor.divmatgra.classList.add('enonce')
    }
    if (ds.Calculatrice) {
      me.fenetresjq = [{ name: 'Calculatrice', title: 'Une calculatrice', left: 600, top: 10 }]
      j3pCreeFenetres(me)
      j3pAfficheCroixFenetres('Calculatrice')
      j3pAjouteBouton(stor.lesdiv.calculatrice, 'Calculatrice', 'MepBoutons', 'Calculatrice', boutonfen)
    }
    me.finEnonce()
  }
  function boutonfen () {
    j3pToggleFenetres('Calculatrice')
  }
  function makefig () {
    svgId = j3pGetNewId('mtg')
    stor.svgId = svgId
    const txtFigure = 'TWF0aEdyYXBoSmF2YTEuMAAAABM+TMzNAAJmcv###wEA#wEAAAAAAAAAAAM7AAACdwAAAQEAAAAAAAAAAQAAABz#####AAAAAQAKQ0NhbGNDb25zdAD#####AAJwaQAWMy4xNDE1OTI2NTM1ODk3OTMyMzg0Nv####8AAAABAApDQ29uc3RhbnRlQAkh+1RELRj#####AAAAAQAKQ1BvaW50QmFzZQD#####AAAAAAAQAAAAAAAAAAAAAABACAAAAAAAAAAACQAAQGFgAAAAAABAYDwo9cKPXAAAAAIA#####wEAAAAAEAAAAAAAAAAAAAAAQAgAAAAAAAAAAAUAAUBhwAAAAAAAQC3Cj1wo9cT#####AAAAAQAJQ0NlcmNsZU9BAP####8BAAAAAAAAAQAAAAEAAAAC#####wAAAAEAD0NQb2ludExpZUNlcmNsZQD#####AQAAAAAQAAAAAAAAAAAAAABACAAAAAAAAAAACQABP+V8CLU5dXkAAAADAAAABAD#####AQAAAAAQAAAAAAAAAAAAAABACAAAAAAAAAAACQABQBaVIbytvKsAAAAD#####wAAAAEAEkNBcmNEZUNlcmNsZURpcmVjdAD#####AAAAAAAAAAEAAAABAAAABAAAAAX#####AAAAAQAUQ0Ryb2l0ZURpcmVjdGlvbkZpeGUA#####wEAAAABEAAAAQAAAAEAAAABAT#wAAAAAAAA#####wAAAAEAD0NQb2ludExpZURyb2l0ZQD#####AAAAAAAQAAAAAAAAAAAAAABACAAAAAAAAAAACQAAQFxAAAAAAAAAAAAH#####wAAAAEACENTZWdtZW50AP####8AAAD#ABAAAAEAAAABAAAAAQAAAAgAAAACAP####8BAAAAABAAAAAAAAAAAAAAAEAIAAAAAAAAAAAJAAFAcJAAAAAAAEBgfCj1wo9cAAAAAgD#####AQAAAAAQAAAAAAAAAAAAAABACAAAAAAAAAAACQABQGIAAAAAAABAXnhR64UeuAAAAAIA#####wEAAAAAEAAAAAAAAAAAAAAAQAgAAAAAAAAAAAkAAUBBgAAAAAABQEjwo9cKPXEAAAACAP####8BAAAAABAAAAAAAAAAAAAAAEAIAAAAAAAAAAAJAAFAaIAAAAAAAEBinCj1wo9c#####wAAAAIADENDb21tZW50YWlyZQD#####AAAAAAEABmNlcmNsZQAAAAwQAAAAAAACAAAAAgAAAAEAAAAAAAAAAAABYQAAAAkA#####wAAAAABAAVwb2ludAAAAAoQAAAAAAAAAAAAAQAAAAEAAAAAAAAAAAABYQAAAAkA#####wAAAP8BAAhkaXN0YW5jZQAAAA0QAAAAAAABAAAAAgAAAAEAAAAAAAAAAAAEYWFhYQAAAAkA#####wAAAAABAAZjZW50cmUAAAALEAAAAAAAAQAAAAIAAAABAAAAAAAAAAAAAUEAAAAEAP####8BAAAAABAAAAAAAAAAAAAAAEAIAAAAAAAAAAAJAAE#1xhNQZFU7wAAAAb#####AAAAAQARQ1N5bWV0cmllQ2VudHJhbGUA#####wAAAAH#####AAAAAQALQ1BvaW50SW1hZ2UA#####wEAAAAAEAAAAAAAAAAAAAAAQAgAAAAAAAAAAAkAAAAAEgAAABMAAAAIAP####8A#wD#ABAAAAEAAAABAAAAEgAAAAEAAAAIAP####8A#wD#ABAAAAEACHBvdXJkaWFtAAEAAAABAAAAFAAAAAIA#####wH#AP8AEAAAAAAAAAAAAAAAQAgAAAAAAAAAAAkAAUBZAAAAAAAAQFt4UeuFHrgAAAAJAP####8A#wD#AQAFcmF5b24AAAAXEAAAAAAAAgAAAAEAAAABAAAAAAAAAAAABWFhYWFhAAAABAD#####AQAA#wAQAAAAAAAAAAAAAABACAAAAAAAAAAACQABP9Ji0b#Fom8AAAADAAAABAD#####AQAA#wAQAAAAAAAAAAAAAABACAAAAAAAAAAACQABQBgGilpIgagAAAADAAAABQD#####AAAAAAAAAgEAAAABAAAAGQAAABr###############8='
    j3pEmpty(stor.divmatgra)
    j3pCreeSVG(stor.divmatgra, { id: svgId, width: 280, height: 270, style: { border: 'solid black 1px' } })
    stor.mtgAppLecteur.removeAllDoc()
    stor.mtgAppLecteur.addDoc(svgId, txtFigure, true)
    stor.mtgAppLecteur.calculateAndDisplayAll(true)
    stor.divmatgra.classList.add('enonce')
    stor.mtgAppLecteur.setText(svgId, '#centre', stor.centre, true)
    stor.mtgAppLecteur.setText(svgId, '#point', stor.point, true)
    stor.mtgAppLecteur.setText(svgId, '#cercle', '(' + stor.cercle + ')', true)
    if (!stor.diam) stor.mtgAppLecteur.setVisible(svgId, '#pourdiam', false, true)
    stor.mtgAppLecteur.setText(svgId, '#distance', j3pNombreBienEcrit(stor.distance2) + ' ' + stor.unitedistance2, true)
    stor.mtgAppLecteur.setText(svgId, '#rayon', stor.monctr, true)
  }

  function affCorrection () {
    // couleurs
    stor.zoneliste0.corrige(!stor.erreur1)
    if (stor.zoneliste1) {
      stor.zoneliste1.corrige(!(stor.erreur2 || stor.erreur1))
      stor.zoneliste1.disable(stor.erreur2 || stor.erreur1)
    }
    stor.zoneliste0.disable(stor.erreur1)

    // explik  MMM
    const untab = addDefaultTable(stor.lesdiv.explications, 1, 4)
    let diDist = untab[0][0]
    let diDist2 = untab[0][1]
    if ((stor.diam) && (stor.unitedistance2 !== stor.unitedistance)) {
      diDist = untab[0][2]
      diDist2 = untab[0][3]
      untab[0][1].style.width = '2px'
      untab[0][1].style.background = '#000000'
    }
    if (stor.diam) {
      j3pAffiche(untab[0][0], null, textes.DiamCo1 + textes.DiamCo2 + '\n')
      j3pAffiche(untab[0][0], null, textes.DiamCo1 + textes.DiamCo3 + '\n', { a: stor.rayon * 2 })
      j3pAffiche(untab[0][0], null, textes.DiamCo1 + textes.DiamCo4, {
        b: stor.rayon,
        c: stor.uniterayon
      })
    }

    if (stor.unitedistance2 !== stor.unitedistance) {
      diDist.style.paddingLeft = '5px'
      diDist.style.verticalAlign = 'top'
      diDist2.style.verticalAlign = 'top'
      j3pAffiche(diDist, null, '<u>' + textes.UnitCo1 + '</u> &nbsp;')
      j3pAffiche(diDist2, null, textes.UnitCo2 + '\n',
        {
          a: stor.point,
          b: stor.centre,
          c: stor.distance2,
          d: stor.unitedistance2
        })
      j3pAffiche(diDist2, null, textes.UnitCo3,
        {
          a: stor.point,
          b: stor.centre,
          e: stor.distance,
          f: stor.unitedistance
        })
    }
    let i = 0
    if (stor.distance === stor.rayon) i = 2
    if (stor.distance > stor.rayon) i = 1
    j3pAffiche(stor.lesdiv.solution, null, textes.Co1 + '\n',
      {
        a: stor.centre,
        b: stor.point,
        c: textes.macomp[i],
        e: '<i>' + stor.cercle + '</i>'
      })
    j3pAffiche(stor.lesdiv.solution, null, textes.Co2,
      {
        b: stor.point,
        d: textes.mapos[i],
        e: '<i>' + stor.cercle + '</i>'
      })
    if (ds.theme === 'zonesAvecImageDeFond') {
      stor.lesdiv.solution.classList.add('correction')
      stor.lesdiv.explications.classList.add('explique')
    }
  }

  switch (this.etat) {
    case 'enonce':

      if (this.debutDeLaSection) {
        initSection()
      } else {
        enonceMain()
      }

      break // case "enonce":

    case 'correction':
      // On teste si une réponse a été saisie
      if (![1, 2].includes(stor.zoneliste0.getReponseIndex())) return this.finCorrection()

      stor.lesdiv.solution.classList.remove('correction')
      stor.lesdiv.explications.classList.remove('explique')
      // Bonne réponse
      if (bonneReponse()) {
        this.score += j3pArrondi(1 / this.donneesSection.nbetapes, 1)
        stor.lesdiv.correction.style.color = colorOk
        stor.lesdiv.correction.innerHTML = cBien

        if (stor.zoneliste1) {
          stor.zoneliste1.corrige(true)
          stor.zoneliste1.disable()
        }
        stor.zoneliste0.corrige(true)
        stor.zoneliste0.disable()

        let i = 0
        if (stor.distance === stor.rayon) i = 2
        if (stor.distance > stor.rayon) i = 1
        j3pAffiche(stor.lesdiv.solution, 'exp21', textes.Co1 + '\n',
          {
            a: stor.centre,
            b: stor.point,
            c: textes.macomp[i],
            e: '<i>' + stor.cercle + '</i>'
          })
        j3pAffiche(stor.lesdiv.solution, 'exp22', textes.Co2,
          {
            b: stor.point,
            d: textes.mapos[i],
            e: '<i>' + stor.cercle + '</i>'
          })
        this.typederreurs[0]++
        return this.finCorrection('navigation', true)
      }

      // Pas de bonne réponse
      stor.lesdiv.correction.style.color = colorKo

      // A cause de la limite de temps :
      if (this.isElapsed) { // limite de temps
        stor.lesdiv.correction.innerHTML = tempsDepasse
        this.typederreurs[10]++
        affCorrection()
        return this.finCorrection('navigation', true)
      }

      // Réponse fausse :
      if (this.essaiCourant < this.donneesSection.nbchances) {
        stor.lesdiv.correction.innerHTML += '<br>' + essaieEncore
        this.typederreurs[1]++
        return this.finCorrection() // on reste en correction
      }

      // Erreur au dernier essai
      affCorrection()
      this.typederreurs[2]++
      this.finCorrection('navigation', true)
      break // case "correction":

    case 'navigation':
      if (!this.sectionTerminee()) {
        this.etat = 'enonce'
      }
      this.finNavigation()
      break // case "navigation":
  }
}
