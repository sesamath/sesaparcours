import { j3pAddElt, j3pArrondi, j3pGetRandomInt, j3pShuffle, j3pStyle, j3pShowError } from 'src/legacy/core/functions'
import ListeDeroulante from 'src/legacy/outils/listeDeroulante/ListeDeroulante'
import { j3pAffiche } from 'src/lib/mathquill/functions'
import ZoneStyleMathquill1 from 'src/legacy/outils/zoneStyleMathquill/ZoneStyleMathquill1'
import { addDefaultTable } from 'src/legacy/themes/table'
import { j3pBaseUrl } from 'src/lib/core/constantes'
import { getMtgCore } from 'src/lib/outils/mathgraph'
import textesGeneriques from 'src/lib/core/textes'

import FigNomme03 from './FigNomme03'

const { cBien, cFaux, essaieEncore, regardeCorrection, reponseIncomplete } = textesGeneriques

const pe1 = 'Maitrise'
const pe2 = 'Suffisant'
const pe3 = 'Suffisant - erreur nature'
const pe4 = 'Insuffisant'
const pe5 = 'Insuffisant - erreur nature'
const pe6 = 'Très insuffisant'
const pe7 = 'Très insuffisant - erreur nature'

/**
 * Ajouter une description ici
 * @fileOverview
 * @author Tommy Barroy <t.barroy@laposte.net>
 * @since 2017-10
 */
export const params = {
  outils: ['calculatrice', 'mathquill'],
  /**
   * Ce qui est paramétrable dans l’éditeur de graphe
   * @type {LegacySectionParamsParametres}
   */
  parametres: [
    ['nbrepetitions', 9, 'entier', 'Nombre de répétitions de la section'],
    ['limite', 0, 'entier', 'Temps disponible par répétition, en secondes'],
    ['nbchances', 1, 'entier', 'Nombre d’essais par répétition'],
    ['Type_Question', 'Nature_Nomme', 'liste', '<u>Nature</u>: L’élève doit donner la nature de l’élément. <BR><BR> <u>Nomme</u>: L’élève doit donner le nom de l’élément.', ['Nature_Nomme', 'Nature', 'Nomme']],
    ['Eclaire', true, 'boolean', 'true : Les éléments nommés sont colorés.'],
    ['SEGMENT', false, 'boolean', 'Le contenant peut être un segment.'],
    ['DEMI_DROITE', false, 'boolean', 'Le contenant peut être une demi-droite.'],
    ['QUADRILATERE', false, 'boolean', 'Le contenant peut être un quadrilatère quelconque.'],
    ['PARALLELOGRAMME', false, 'boolean', 'Le contenant peut être un parallèlogramme.'],
    ['LOSANGE', false, 'boolean', 'Le contenant peut être un losange.'],
    ['RECTANGLE', false, 'boolean', 'Le contenant peut être un rectangle.'],
    ['CARRE', false, 'boolean', 'Le contenant peut être un carré.'],
    ['CERCLE', false, 'boolean', 'Le contenant peut être un cercle.'],
    ['ARC', false, 'boolean', 'Le contenant peut être un arc de cercle.'],
    ['SECTEUR', false, 'boolean', 'Le contenant peut être un secteur circulaire.'],
    ['ANGLE', false, 'boolean', 'Le contenant peut être un angle.'],
    ['TRIANGLE', false, 'boolean', 'Le contenant peut être un triangle quelconque.'],
    ['TRIANGLE_ISOCELE', false, 'boolean', 'Le contenant peut être un triangle isocèle.'],
    ['TRIANGLE_EQUILATERAL', false, 'boolean', 'Le contenant peut être un triangle équilatéral.'],
    ['TRIANGLE_RECTANGLE', false, 'boolean', 'Le contenant peut être un triangle rectangle.'],
    ['CUBE', false, 'boolean', 'Le contenant peut être un cube.'],
    ['PAVEDROIT', false, 'boolean', 'Le contenant peut être un pavé droit.'],
    ['PRISME', false, 'boolean', 'Le contenant peut être un prisme droit.'],
    ['CYLINDRE', false, 'boolean', 'Le contenant peut être un cylindre.'],
    ['CYLINDRE_REVOLUTION', false, 'boolean', 'Le contenant peut être un cylindre de révolution.'],
    ['PYRAMIDE', false, 'boolean', 'Le contenant peut être une pyramide.'],
    ['CONE', false, 'boolean', 'Le contenant peut être un cône.'],
    ['CONE_REVOLUTION', false, 'boolean', 'Le contenant peut être un cône de révolution.'],
    ['SPHERE', false, 'boolean', 'Le contenant peut être une sphère.'],
    ['arete', false, 'boolean', 'L’élément peut être une arête. <BR><BR> Valable uniquement pour: <i> pavé droit, cube, prisme, pyramide </i>.'],
    ['arete_cachee', false, 'boolean', 'L’élément peut être une arête cachée ou visible. <BR><BR> Valable uniquement pour: <i> pavé droit, cube, prisme, pyramide </i>.'],
    ['arete_laterale', false, 'boolean', 'L’élément peut être une arête latérale. <BR><BR> Valable uniquement pour: <i>  prisme, pyramide </i>.'],
    ['sommet', false, 'boolean', 'L’élément peut être un sommet. <BR><BR> Valable uniquement pour: <i> pavé droit, cube, prisme, pyramide, triangles, quadrilatères </i>.'],
    ['sommet_oppose', false, 'boolean', 'L’élément peut être le sommet opposé à un sommet (ou côté) du contenant. <BR><BR> Valable uniquement pour: <i>  triangles, quadrilatères </i>.'],
    ['sommet_principal', false, 'boolean', 'L’élément peut être le sommet principal (ou LE sommet) du contenant. <BR><BR> Valable uniquement pour: <i>  triangle isocèle, cônes, pyramide </i>.'],
    ['sommet_angle_droit', false, 'boolean', 'L’élément peut être le sommet de l’angle droit du triangle rectangle. <BR><BR> Valable uniquement pour: <i>  triangle rectangle </i>.'],
    ['sommet_consecutif', false, 'boolean', 'L’élément peut être le sommet consécutif à un sommet donné du quadrilatère. <BR><BR> Valable uniquement pour: <i>  quadrilatères </i>.'],
    ['face', false, 'boolean', 'L’élément peut être une face du contenant. <BR><BR> Valable uniquement pour: <i>  cube, pavé droit, prisme, pyramide, cylindres, cônes </i>.'],
    ['face_cachee', false, 'boolean', 'L’élément peut être une face cachée ou visible du contenant. <BR><BR> Valable uniquement pour: <i>  cube, pavé droit, prisme, pyramide, cylindres, cônes  </i>.'],
    ['face_laterale', false, 'boolean', 'L’élément peut être une face latérale du contenant. <BR><BR> Valable uniquement pour: <i>   prisme, pyramide, cylindres, cônes  </i>.'],
    ['cote', false, 'boolean', 'L’élément peut être un côté du contenant. <BR><BR> Valable uniquement pour: <i>   triangles, quadrilatères, angle    </i>.'],
    ['cote_oppose', false, 'boolean', 'L’élément peut être le côté opposé à un sommet (ou côté) du contenant. <BR><BR> Valable uniquement pour: <i>  triangles, quadrilatères </i>.'],
    ['cote_consecutif', false, 'boolean', 'L’élément peut être un côté consécutif à un côté donné du quadrilatère. <BR><BR> Valable uniquement pour: <i>  quadrilatères </i>.'],
    ['cote_adjacent', false, 'boolean', 'L’élément peut être le côté adjacent à un angle donné du triangle rectangle. <BR><BR> Valable uniquement pour: <i>  triangle rectangle </i>.'],
    ['cote_oppose_angle', false, 'boolean', 'L’élément peut être le côté opposé à un angle donné du triangle rectangle. <BR><BR> Valable uniquement pour: <i>  triangle rectangle </i>.'],
    ['rayon', false, 'boolean', 'L’élément peut être un rayon. <BR><BR> Valable uniquement pour: <i>  cercle, cylindres, cônes, sphère </i>.'],
    ['diametre', false, 'boolean', 'L’élément peut être un diamètre. <BR><BR> Valable uniquement pour: <i>  cercle, cylindres, cônes, sphère </i>.'],
    ['corde', false, 'boolean', 'L’élément peut être une corde du cercle. <BR><BR> Valable uniquement pour: <i>  cercle  </i>.'],
    ['hypotenuse', false, 'boolean', 'L’élément peut être l’hypoténuse du triangle rectangle. <BR><BR> Valable uniquement pour: <i>  triangle rectangle</i>.'],
    ['diagonale', false, 'boolean', 'L’élément peut être une diagonale du quadrilatère. <BR><BR> Valable uniquement pour: <i>   quadrilatères </i>.'],
    ['generatrice', false, 'boolean', 'L’élément peut être une génératrice du cône de révolution. <BR><BR> Valable uniquement pour: <i>   cône de révolution </i>.'],
    ['angle', false, 'boolean', 'L’élément peut être un angle du contenant. <BR><BR> Valable uniquement pour: <i>   triangles, quadrilatères </i>.'],
    ['angle_base', false, 'boolean', 'L’élément peut être un angle à la base du triangle isocèle. <BR><BR> Valable uniquement pour: <i>   triangles isocèle </i>.'],
    ['angle_oppose', false, 'boolean', 'L’élément peut être l’angle opposé à un angle donné du quadrilatère. <BR><BR> Valable uniquement pour: <i>  quadrilatères </i>.'],
    ['angle_consecutif', false, 'boolean', 'L’élément peut être un angle consécutif à un angle donné du quadrilatère.  <BR><BR> Valable uniquement pour: <i>   quadrilatères </i>.'],
    ['equateur', false, 'boolean', 'L’élément peut être l’équateur de la sphère. <BR><BR> Valable uniquement pour: <i>   sphère </i>.'],
    ['meridien', false, 'boolean', 'L’élément peut être un méridien de la sphère. <BR><BR> Valable uniquement pour: <i>   sphère </i>.'],
    ['grand_cercle', false, 'boolean', 'L’élément peut être un grand cercle de la sphère. <BR><BR> Valable uniquement pour: <i>   sphère </i>.'],
    ['parallele', false, 'boolean', 'L’élément peut être un parallèle de la sphère. <BR><BR> Valable uniquement pour: <i>   sphère </i>.'],
    ['pole', false, 'boolean', 'L’élément peut être un pôle de la sphère. <BR><BR> Valable uniquement pour: <i>   sphère </i>.'],
    ['origine', false, 'boolean', 'L’élément peut être l’origine de la demi-droite. <BR><BR> Valable uniquement pour: <i>   demi-droite </i>.'],
    ['milieu', false, 'boolean', 'L’élément peut être le milieu du segment. <BR><BR> Valable uniquement pour: <i>   segment </i>.'],
    ['extremites', false, 'boolean', 'L’élément peut être une extrémité du contenant. <BR><BR> Valable uniquement pour: <i>   segment, arc de cercle </i>.'],
    ['centre', false, 'boolean', 'L’élément peut être le centre du contenant. <BR><BR> Valable uniquement pour: <i>   cercle, sphère, cônes </i>.'],
    ['bissectrice', false, 'boolean', 'L’élément peut être une (ou la) bissectrice du contenant. <BR><BR> Valable uniquement pour: <i>   angle </i>.'],
    ['mediatrice', false, 'boolean', 'L’élément peut être une (ou la) médiatrice du contenant. <BR><BR> Valable uniquement pour: <i>   segment, triangle quelconque, triangle rectangle </i>.'],
    ['hauteur', false, 'boolean', 'L’élément peut être une hauteur du contenant, ou la mesure de cette hauteur pour les solides. <BR><BR> Valable uniquement pour: <i>    triangle quelconque, triangle rectangle, cônes, pyramides, cylindre, prisme, parallèlogramme </i>.'],
    ['mediane', false, 'boolean', 'L’élément peut être une médiane du contenant. <BR><BR> Valable uniquement pour: <i>    triangle quelconque, triangle rectangle </i>.'],
    ['base', false, 'boolean', 'L’élément peut être une (ou la) base du contenant. <BR><BR> Valable uniquement pour: <i>    triangle isocèle, prisme, cylindres, pyramide, cônes  </i>.'],
    ['longueur', false, 'boolean', 'L’élément peut être la longueur d’un côté, d’une arête ou d’un segment donné. <BR><BR> Valable uniquement pour: <i>    segment, triangles, quadrilatères, cube, pavé droit, prisme, pyramide  </i>.'],
    ['le_diametre', false, 'boolean', 'L’élément peut être la longueur d’un diamètre du contenant. <BR><BR> Valable uniquement pour: <i>  cercle, cylindres, cônes, sphère </i>.'],
    ['le_rayon', false, 'boolean', 'L’élément peut être la longueur d’un rayon du contenant. <BR><BR> Valable uniquement pour: <i>  cercle, cylindres, cônes, sphère </i>.'],
    // le modèle gère la compatibilité ascendante avec l’ancien paramètre MiseEnPagePlusClaire
    ['theme', 'standard', 'liste', 'Modèle pour l’affichage', ['standard', 'zonesAvecImageDeFond']]

  ],
  // dans le cas d’une section QUANTITATIVE
  pe: [
    { pe_1: pe1 },
    { pe_2: pe2 },
    { pe_3: pe3 },
    { pe_4: pe4 },
    { pe_5: pe5 },
    { pe_6: pe6 },
    { pe_7: pe7 }
  ]
}

const peList = [pe1, pe2, pe3, pe4, pe5, pe6, pe7]

const textes = {
  couleurexplique: '#A9E2F3',
  couleurcorrec: '#A9F5A9',
  couleurenonce: '#D8D8D8',
  couleurenonceg: '#F3E2A9',
  couleuretiquettes: '#A9E2F3',

  couleurMiseEnVal: '#FF00BF',
  couleurMiseEnVal2: '#0000FF',
  Clic: 'Sélectionne ',
  NomContenant: ' £a ci-dessous.',
  ContDef: ['Pour ce segment', 'Pour cette demi-droite', 'Pour ce triangle quelconque', 'Pour ce triangle', 'Pour ce triangle équilatéral', 'Pour ce triangle', 'Pour ce quadrilatère quelconque', 'Pour ce rectangle', 'Pour ce carré', 'Pour ce losange', 'Pour ce  parallèlogramme', 'Pour ce cercle', 'Pour cet arc de cercle', 'Pour ce secteur circulaire', 'Pour cet angle', 'Pour ce cube', 'Pour ce pavé droit', 'Pour ce prisme droit', 'Pour ce cylindre', 'Pour ce cylindre de révolution', 'Pour cette pyramide', 'Pour ce cône', 'Pour ce cône de révolution', 'Pour cette sphère'],
  Nature: ' est ',
  NatureDeb: ' £b ',
  Nom: '£a £b se nomme ',
  Nature_Nom: '£a se nomme ',

  Listearete: ['une arête', 'un sommet', 'une face', 'un côté', 'une longueur'],
  COarete: ['', 'Un sommet est un point ! ', 'Une face est une surface !', 'On ne dit pas côté pour un solide !', 'Il ne faut pas confondre une arête et sa longueur !'],
  Listearete_cachee: ['une arête cachée', 'Une arête visible', 'un sommet', 'une face', 'un côté caché'],
  COarete_cachee: ['', 'Une arête en pointillée est cachée !', 'Un sommet est un point !', 'Une face est une surface !', 'On ne dit pas côté pour un solide !'],
  Listearete_cachee2: ['une arête visible', 'une arête cachée', 'un sommet', 'une face', 'un côté caché'],
  COarete_cachee2: ['', 'Une arête en pointillée est cachée !', 'Un sommet est un point !', 'Une face est une surface !', 'On ne dit pas côté pour un solide !'],
  Listearete_laterale: ['une arête latérale', 'un côté latérale', 'une arête littérale', 'une face latérale', 'une arête de poisson'],
  COarete_laterale: ['', 'On ne dit pas côté pour un solide !', 'Il ne faut pas confondre littérale et latérale !', 'Une face est une surface !', 'Il ne faut pas répondre n’importe quoi !'],
  Listesommet: ['un sommet', 'une arête', 'une face', 'une pointe', 'le sommet'],
  COsommet: ['', 'Une arête est un segment', 'Une face est une surface', 'On ne dit pas une pointe !', 'Il y a plusieurs sommets dans cette figure !'],
  ListesommetANGLE: ['le sommet', 'un côté', 'une face', 'une pointe', 'le sommet principal'],
  COsommetANGLE: ['', 'Un côté est un segment', 'Une face est une surface', 'On ne dit pas une pointe !', 'On l’appelle tout simplement <i> le sommet </i> !'],
  Listesommet_principal: ['le sommet principal', 'un sommet principal', 'un côté', 'la pointe', 'la hauteur'],
  Cosommet_principal: ['', 'Il n’y a qu’un seul sommet principal !', 'Un côté est un segment !', 'On ne dit pas une pointe !', 'Une hauteur est une droite !'],
  Listele_sommet: ['le sommet', 'l’arête', 'le centre', 'la face', 'une face'],
  COle_sommet: ['', 'Une arête est un segment !', 'Le centre est un point !', 'Une face est une surface !', 'Une face est une surface !'],
  Listesommet_oppose: ['le sommet opposé', 'un sommet consécutif', 'un côté', 'une diagonale', 'une longueur'],
  COsommet_oppose: ['', "Deux sommets consécutifs <i>'se suivent'</i> !", 'Un côté est un segment !', 'Une diagonale est un segment !', 'Une longueur est une mesure !'],
  Listesommet_consecutif: ['un sommet consécutif', 'le sommet opposé', 'un côté', 'une diagonale', 'une longueur'],
  COsommet_consecutif: ['', "Deux sommets opposés sont <i>'face à face'</i> !", 'Un côté est un segment !', 'Une diagonale est un segment !', 'Une longueur est une mesure !'],
  Listesommet_angle_droit: ['le sommet de l’angle droit', 'le côté de l’angle droit', 'un angle droit', 'l’hypoténuse', 'une hauteur'],
  COsommet_angle_droit: ['', 'Un côté est un segment !', 'Un point ne peut pas être un angle !', 'L’hypoténuse est un segment !', 'Une hauteur est une droite !'],
  Listeface: ['une face', 'un sommet', 'un point', 'une arête', 'une farce'],
  COface: ['', 'Un sommet est un point', 'Il ne s’agit pas d’un point !', 'Une arête est un segment !', 'Attention à bien lire correctement !'],
  Listeface_laterale2: ['la face latérale', 'une face de côté', 'un sommet', 'une arête latérale', 'une face latérale'],
  COface_laterale2: ['', "On ne dit pas <i>'face de côté'</i> !", 'Un sommet est un point !', 'Une arête est un segment !', 'Ce solide n’a qu’une seule face latérale !'],
  Listeface_laterale: ['une face latérale', 'une face de côté', 'un sommet', 'une arête latérale', 'une arête littérale'],
  COface_laterale: ['', "On ne dit pas <i>'face de côté'</i> !", 'Un sommet est un point !', 'Une arête est un segment !', 'Il ne faut pas confondre littérale et latérale !'],
  Listeface_cachee: ['une face cachée', 'une farce cachée', 'une arête cachée', 'une face visible', 'une arête visible'],
  COface_cachee: ['', 'Attention à bien lire correctement !', 'Une arête est un segment !', 'Si l’une des arête est cachée, la face l’est aussi !', 'Une arête est un segment !'],
  Listeface_cachee2: ['une face visible', 'une farce cachée', 'une arête cachée', 'une face cachée', 'une arête visible'],
  COface_cachee2: ['', 'Attention à bien lire correctement !', 'Une arête est un segment !', 'Quand il n’y a aucun pointillés, la face est visible !', 'Une arête est un segment !'],
  Listecote_consecutif: ['un côté consécutif', 'le côté opposé', 'une diagonale', 'un côté', 'le périmètre'],
  COcote_consecutif: ['', "Deux côtés opposés sont <i>'face à face'</i> !", 'Une diagonale relie deux sommets opposés !', 'Il y a deux côtés ici !', 'Le périmètre est une mesure !'],
  Listecote: ['un côté', 'un sommet', 'une hauteur', 'une diagonale', 'le côté'],
  COcote: ['', 'Un sommet est un point !', 'Une hauteur est une droite', 'Une diagonale relie deux sommets opposés !', 'Il y a plusieurs côtés dans cette figure !'],
  Listecote_oppose: ['le côté opposé', 'un côté consécutif', 'une diagonale', 'un côté', 'le périmètre'],
  COcote_oppose: ['', "Deux côtés consécutifs <i>'se suivent'</i> !", 'Une diagonale relie deux sommets opposés !', 'Il y a deux côtés ici !', 'Le périmètre est une mesure !'],
  Listecote_adjacent: ['le côté adjacent', 'un côté adjacent', 'le côté opposé', 'un côté opposé', 'l’hypoténuse'],
  COcote_adjacent: ['', 'Il n’a y qu’un seul côté adjacent !', "Le côté opposé est <i>'en face'</i> de l’angle !", "Le côté opposé est <i>'en face'</i> de l’angle !", "L’hypoténuse est <i>'en face'</i> de l’angle droit !"],
  Listecote_oppose_angle: ['le côté opposé', 'le côté adjacent', 'un côté adjacent', 'un côté opposé', 'l’hypoténuse'],
  COcote_oppose_angle: ['', 'Le côté adjacent touche l’angle !', 'Le côté adjacent touche l’angle !', 'Il n’y a qu’un seul côté opposé !', "L’hypoténuse est <i>'en face'</i> de l’angle droit !"],
  Listerayon: ['un rayon', 'le rayon', 'un diamètre', 'une corde', 'le diamètre'],
  COrayon: ['', "<i>'Le rayon'</i> est une longueur !", 'Un diamètre relie deux point du cercle en passant par le centre !', 'Une corde ne passe pas par le centre !', 'Un diamètre relie deux point du cercle en passant par le centre !'],
  Listediametre: ['un diamètre', 'un rayon', 'le rayon', 'une corde', 'le diamètre'],
  COdiametre: ['', 'Un rayon relie le centre à un point du cercle !', 'Le rayon est une longueur !', 'Ce segment passe par le centre, on peut dire mieux !', "<i>'le diamètre'</i> est une longueur !"],
  Listecorde: ['une corde', 'la corde', 'un rayon', 'un diamètre', 'le centre'],
  COcorde: ['', 'Il y a plusieurs cordes !', 'Un rayon relie le centre du cercle à un point du cercle !', 'Ce segment ne passe pas par le centre !', 'le centre est un point !'],
  Listehypotenuse: ['l’hypoténuse', 'une hypoténuse', 'le côté adjacent', 'le côté opposé', 'l’hippoténuze'],
  COhypotenuse: ['', 'Il n’a qu’une seule hypoténuse', 'le côté adjacent d’un angle touche cet angle !', 'le côté opposé d’un angle est <i>« en face de »</i> cet angle !', 'Attention à l’orthographe !'],
  Listediagonale: ['une diagonale', 'un côté', 'la diagonale', 'une arête', 'un sommet'],
  COdiagonale: ['', 'Un côté relie deux sommets consécutifs !', 'Il ya plusieurs diagonales dans ce quadrilatère !', 'Une arête est un mot du vocabulaire des solides !', 'Un sommet est un point !'],
  Listegeneratrice: ['une génératrice', 'la génératrice', 'un côté', 'une roue motrice', 'une hauteur'],
  COgeneratrice: ['', 'Il y a plusieurs génératrices dans ce cône !', 'Le mot côté fait partie du vocabulaire des figures planes !', 'Attention à bien lire !', 'Une hauteur est une droite !'],
  ListeangleSECTEUR: ['l’angle', 'un angle', 'un sommet', 'un côté', 'un triangle'],
  COangleSECTEUR: ['', 'un secteur circulaire n’a qu’un seul angle !', 'Un sommet est un point !', 'Un côté est un segment !', 'Un triangle est un polygone !'],
  Listeangle: ['un angle', 'un sommet', 'un côté', 'un triangle', 'une bissectrice'],
  COangle: ['', 'Un sommet est un point !', 'Un côté est un segment !', 'Un triangle est un polygone !', 'Une bissectrice est une droite !'],
  Listeangle_base: ['un angle à la base', 'un angle quelconque', 'un côté', 'un sommet', 'un angle opposé'],
  COangle_base: ['', 'Il n’est pas quelconque puisqu’il touche la base du triangle isocèle !', 'Un côté est un segment', 'Un sommet est un point !', "Un angle peut être <i>' opposé à un côté '</i>, mais pas <i>' opposé '</i> tout seul !"],
  Listeangle_oppose: ['l’angle opposé', 'un angle consécutif', 'un sommet opposé', 'un sommet consécutif', 'un côté consécutif'],
  COangle_oppose: ['', "deux angles consécutifs <i> ' se suivent '</i> !", 'Des sommets sont des points !', 'Des sommets sont des points !', 'Des côtés sont des segments !'],
  Listeangle_consecutif: ['un angle consécutif', 'l’angle opposé', 'un sommet opposé', 'un sommet consécutif', 'un côté consécutif'],
  COangle_consecutif: ['', "deux angles opposés sont <i> ' faca à face '</i> !", 'Des sommets sont des points !', 'Des sommets sont des points !', 'Des côtés sont des segments !'],
  Listemeridien: ['un méridien', 'un grand cercle', 'l’équateur', 'le centre', 'un parallèle'],
  COmeridien: ['', 'Il s’agit ici d’un demi-cercle !', 'L’équateur est le cercle situé à égale distance des pôles Nord et Sud !', 'Le centre est un point !', 'Un parallèle est un cercle parallèle à l’équateur !'],
  Listeequateur: ['l’équateur', 'un équateur', 'un méridien', 'un pôle', 'le diamètre'],
  COequateur: ['', 'Il n’y a qu’un seul équateur', 'Un méridien est un demi-cercle reliant les deux pôles', 'Un pôle est un point !', 'Le diamètre est une longueur !'],
  Listegrand_cercle: ['un grand cercle', 'un grand rond', 'le centre', 'un méridien', 'un cerceau'],
  COgrand_cercle: ['', "<i>' Rond '</i> n’est pas défini en mathématiqes !", 'Le centre est un point !', 'Un méridien est un demi-cercle reliant les deux pôles !', 'Et pourquoi pas un cerveau ?'],
  Listeparallele: ['un parallèle', 'une parallèle', 'un méridien', 'un grand cercle', 'un pôle'],
  COparallele: ['', 'On dit <u><i>un</i></u> parallèle pour ce cercle !', 'Un méridien est un demi-cercle reliant les deux pôles !', 'Un grand cercle a le même centre que la sphère !', 'Un pôle est un point !'],
  Listepole: ['un pôle', 'le pôle', 'le centre', 'un centre', 'un méridien'],
  COpole: ['', 'Il y a deux pôles', "Le centre est ... <i> ' au centre '</i> de la sphère ! ", "Le centre est ... <i> ' au centre '</i> de la sphère !", 'Un méridien est un demi-cercle reliant les deux pôles !'],
  Listeorigine: ['l’origine', 'une origine', 'une extrémité', 'le centre', 'le milieu'],
  COorigine: ['', 'Il n’y a qu’une seule origine !', "Elle s’appelle <i>' origine '</i> dans une demi-droite !", 'Une demi-droite n’a pas de centre !', 'Le milieu est le point d’un segment situé à égale distance des deux extrémités !'],
  Listemilieu: ['le milieu', 'un milieu', 'le centre', 'un centre', 'une extrémité'],
  COmilieu: ['', 'Il n’y a qu’un seul milieu !', 'Un segment n’a pas de centre !', 'Un segment n’a pas de centre !', 'Une extrémité est un point de départ ou d’arrêt du segment !'],
  Listeextremites: ['une extrémité', 'le milieu', 'l’extrémités', 'le centre', 'un centre'],
  COextremites: ['', 'Le milieu est le point d’un segment situé à égale distance des deux extrémités !', 'Il y a deux extrémités !', '', ''],
  ListecentreSECTEUR: ['le centre', 'un centre', 'le milieu', 'le rayon', 'un point'],
  COcentreSECTEUR: ['', 'Il n’y a qu’un seul centre !', 'Un secteur circulaire n’a pas de milieu !', 'Le rayon est une mesure !', 'Ce point a un nom particulier pour ce secteur !'],
  Listecentre: ['le centre', 'un centre', 'le milieu', 'le rayon', 'un point'],
  COcentre: ['', 'Il n’y a qu’un seul centre !', 'On ne parle pas de milieu dans ce cas !', 'Le rayon est une mesure !', 'Ce point est <i>à l’intérieur</i> !'],
  Listebissectrice_triangle: ['une bissectrice', 'une hauteur', 'une médiatrice', 'une médiane', 'une tangente'],
  CObissectrice_triangle: ['', 'Une hauteur est perpendiculaire à un côté !', 'Une médiatrice est perpendiculaire à un côté en passant par son milieu !', 'La médiane passe par un sommet du triangle et par le milieu du côté opposé !', 'Il n’y a pas de tangente à un triangle !'],
  Listemediatrice_triangle: ['une médiatrice', 'une bissectrice', 'une hauteur', 'une médiane', 'une tangente'],
  COmediatrice_triangle: ['', 'Une bissectrice sépare un angle en deux parties égales !', 'Une hauteur est perpendiculaire à un côté d’un triangle !', 'La médiane passe par un sommet d’un triangle et par le milieu du côté opposé !', 'On parle de tangente à un cercle !'],
  Listehauteur_triangle: ['une hauteur', 'une bissectrice', 'une médiatrice', 'une base', 'une tangente'],
  COhauteur_triangle: ['', 'Une bissectrice sépare un angle en deux parties égales !', 'Une médiatrice est perpendiculaire à un côté en passant par son milieu !', 'Une base est un côté du triangle !', 'On parle de tangente à un cercle !'],
  Listemediane: ['une médiane', 'une bissectrice', 'une médiatrice', 'une base', 'une hauteur'],
  COmediane: ['', 'Une bissectrice sépare un angle en deux parties égales !', 'Une médiatrice est perpendiculaire à un côté en passant par son milieu !', 'Une base est un côté du triangle !', 'Une hauteur est perpendiculaire à un côté d’un triangle !'],
  Listebissectrice_angle: ['la bissectrice', 'la médiatrice', 'un côté', 'la droite', 'la sécante'],
  CObissectrice_angle: ['', 'Un angle n’a pas de médiatrice !', 'Les côtés d’un angle sont sur ... <i> le côté </i> !', 'Cette droite a un nom !', 'Le mot <i>sécante</i> défini deux ligne qui se croisent !'],
  Listemediatrice_segment: ['la médiatrice', 'la hauteur', 'la perpendiculaire', 'la droite', 'le milieu'],
  COmediatrice_segment: ['', 'Un segment n’a pas de hauteur !', 'Cette perpendiculaire passe par le milieu !', 'Cette droite a un nom !', 'Le milieu d’un segment est un point !'],
  Listehauteur_parallelogramme: ['une hauteur', 'la hauteur', 'la perpendiculaire', 'une médiatrice', 'un côté'],
  COhauteur_parallelogramme: ['', 'Un parallèlogramme a plusieurs hauteur !', 'Il y a plusieurs perpendiculaires possibles !', 'Une médiatrice d’un côté passe au milieu du côté !', 'Les côtés sont sur ... <i> le côté </i> !'],
  Listehauteur_solide: ['la hauteur', 'une hauteur', 'un côté', 'le centre', 'une droite'],
  COhauteur_solide: ['', 'Il n’y a qu’une seule longueur égale à la hauteur du solide !', "Le mot <i>' côté '</i> est réservé aux figures planes !", 'Le centre est un point !', 'Une droite ne peuxpas appartenir à un solide, une droite est infinie !'],
  Listebase_isocele: ['la base', 'une base', 'le côté principal', 'l’hypoténuse', 'un sommet'],
  CObase_isocele: ['', 'Il n’y a qu’une base dans un triangle isocèle !', 'Cela n’existe pas !', 'L’hypoténuse est un côté des triangles rectangles !', 'Un sommet est un point !'],
  Listebase_prisme_cyl: ['une base', 'la base', 'une arête', 'un sol', 'un sommet'],
  CObase_prisme_cyl: ['', 'Ce solide a deux bases !', 'Une arête est un segment !', 'Cela ne se dit pas !', 'Un sommet est un point !'],
  Listebase_solide_pointe: ['la base', 'une base', 'une arête', 'un sol', 'un sommet'],
  CObase_solide_pointe: ['', 'Ce solide n’a qu’une seule base !', 'Une arête est un segment !', 'Cela ne se dit pas !', 'Un sommet est un point !'],
  Listelongueur: ['la longueur', 'une longueur', 'le nombre', 'la grandeur', 'la taille'],
  COlongueur: ['la longueur', 'Un segment n’a qu’une seule longueur', 'Un nombre avec une unité est une mesure !', "Pour un segment, on dit plutôt <i>' longueur '</i> !", "Pour un segment, on dit plutôt <i>' longueur '</i> !"],
  Listele_rayon: ['le rayon', 'le diamètre', 'un rayon', 'un diamètre', 'la corde'],
  COle_rayon: ['', 'Le diamètre est la longueur de tous les diamètres !', 'Il n’y qu’une seule longueur pour tous les rayons !', 'Le diamètre est la longueur de tous les diamètres !', 'Une corde est un segment qui relie deux points du cercle !'],
  Listele_diametre: ['le diamètre', 'le rayon', 'un rayon', 'un diamètre', 'la corde'],
  COle_diametre: ['', 'Le rayon est la longueur de tous les rayons !', 'Le rayon est la longueur de tous les rayons !', 'Il n’y qu’une seule longueur pour tous les diamètres !', 'Une corde est un segment qui relie deux points du cercle !'],

  point: '.',
  consigneNature: ' est ',
  consigneNom: ' £a se nomme03 ',
  Choisir: 'Choisir',
  PhrRepplusieurs: '£a en bleu peut se nommer £b',
  PhrRepun: '£a en bleu se nomme03 £b',
  PhrRepNplusieurs: 'L’élément en bleu est £a, <BR> £c peut se nommer £b',
  PhrRepNun: 'L’élément en bleu est £a, <BR> £c se nomme03 £b',
  genreinde: ['une', 'un'],
  genrede: ['la', 'le'],
  lespoly: ['un triangle', 'un quadrilatère', 'un pentagone', 'un hexagone', 'un heptagone', 'un octogone', 'un nonagone', 'un décagone'],
  phrase2: 'Attention à la place du numérateur et du dénominateur.',
  eletout: [['point', 1], ['segment', 1], ['arc de cercle', 1], ['droite', 0], ['demi-droite', 0], ['cercle', 1], ['angle', 1], ['point', 1], ['triangle', 1], ['quadrilatère', 1], ['pentagone', 1], ['hexagone', 1], ['heptagone', 1], ['octogone', 1], ['nonagone', 1], ['décagone', 1], ['longueur', 0]],
  eleligne: [['segment', 1], ['droite', 0], ['demi-droite', 0]],
  elelong: [['segment', 1], ['droite', 0], ['demi-droite', 0], ['longueur', 0]],
  elerond: [['cercle', 1], ['arc de cercle', 1]],
  eleangle: [['angle', 1], ['segment', 1], ['demi-droite', 0]],
  elepoly1: [['triangle', 1], ['quadrilatère', 1]],
  elepoly2: [['pentagone', 1], ['hexagone', 1], ['heptagone', 1], ['octogone', 1], ['nonagone', 1], ['décagone', 1]],
  ErSegLigne: 'Cette ligne n’est pas £a car elle s’arête en £b et en £c !',
  ErSommet: 'Le sommet d’un angle est toujours au milieu de son nom !',
  ErDteLigne: 'Cette ligne n’est pas £a car elle continue après £b et £c !',
  ErDte2Ligne: 'Cette ligne n’est pas £a car elle s’arête en £b et continue après £c !',
  ErPolycompte: 'Ce polygone n’est pas £a car il a £b côtés !',
  Erminus: "Les noms des points s'écrivent toujours en majuscule !",
  ErSegCrochets: "Le nom d’un segment s'écrit toujours entre crochets !",
  ErDteParent: "Le nom d’une droite s'écrit toujours entre parenthèses !",
  ErDte2Parent: "Le nom d’un demi-droite s'écrit entre un crochet et une parenthèse !",
  ErDte2Origine: 'L’origine de la demi-droite doit se trouver à côté du crochet !',
  ErEsp: "Il n’y a pas d’espace dans le nom d'£a !",
  ErPoint: "Il n’y a pas de point dans le nom d'£a !",
  ErVirg: "Il n’y a pas de virgule dans le nom d'£a !",
  ErPointVirg: "Il n’y a pas de point-virgule dans le nom d'£a !",
  ErParFo: "Il n’y a pas de parenthèse dans le nom d'£a !",
  ErCroFo: "Il n’y a pas de crochet dans le nom d'£a !",
  ErArcTrop: 'Le chapeau arrondi est réservé aux arcs de cercle !',
  ErAngleTrop: 'Le chapeau pointu est réservé aux angles !',
  ErArcOubli: 'Tu as oublié le chapeau arrondi sur le nom de l’arc de cercle !',
  ErAngleOubli: 'Tu as oublié le chapeau pointu sur le nom de l’angle !',
  ErDesordre: 'Pour nommer un polygone, il faut faire le tour du polygone !'
}

/**
 * section nomme03
 * @this {Parcours}
 */
export default function main () {
  const me = this
  const stor = this.storage
  const ds = this.donneesSection

  function yareponse () {
    if (me.isElapsed) { return true }
    if (stor.Nature) {
      if (!stor.Zonelist.changed) {
        stor.Zonelist.focus()
        return false
      }
    }

    if (stor.Nomme) {
      stor.LarepNom = stor.zonenom.reponsechap()
      if (stor.LarepNom[0] === '') {
        stor.zonenom.focus()
        return false
      }
    }
    return true
  }

  function bonneReponse () {
    stor.ErreurNat = false
    stor.AideNat = ''

    if (stor.Nature) {
      if (stor.Zonelist.reponse !== stor.listchoix[0]) {
        stor.ErreurNat = true
        const num = stor.listchoix.indexOf(stor.Zonelist.reponse)
        stor.AideNat = stor.COlistchoix[num]
      }
    }

    stor.ErreurNom = false
    stor.ErreurChap = false
    if (stor.Nomme) {
      let gtrouv = false
      for (let i = 0; i < stor.NomElem.length; i++) {
        if (stor.NomElem[i].texte === stor.zonenom.reponsechap()[0]) {
          gtrouv = true
          break
        }
      }
      stor.ErreurNom = !gtrouv
      stor.ErreurChap = (stor.zonenom.reponsechap()[1] !== stor.NomElem[0].chapo)
    }
    // affine les erreurs
    // erreurs de nat

    stor.AideNom = []

    if (stor.ErreurNom) {
      for (let i = 97; i < (97 + 25); i++) {
        if (stor.zonenom.reponsechap()[0].indexOf(String.fromCharCode(i)) !== -1) {
          stor.AideNom.push(textes.Erminus)
          break
        }
      }
      if (stor.zonenom.reponsechap()[0].indexOf(' ') !== -1) {
        stor.AideNom.push(textes.ErEsp.replace('£a', stor.elemType))
      }
      if (stor.zonenom.reponsechap()[0].indexOf(',') !== -1) {
        stor.AideNom.push(textes.ErVirg.replace('£a', stor.elemType))
      }
      if (stor.zonenom.reponsechap()[0].indexOf('.') !== -1) {
        stor.AideNom.push(textes.ErPoint.replace('£a', stor.elemType))
      }
      if (stor.zonenom.reponsechap()[0].indexOf(';') !== -1) {
        stor.AideNom.push(textes.ErPointVirg.replace('£a', stor.elemType))
      }
      if (stor.elemType === 'segment') {
        if ((stor.zonenom.reponsechap()[0].indexOf('[') === -1) || (stor.zonenom.reponsechap()[0].indexOf(']') === -1)) {
          stor.AideNom.push(textes.ErSegCrochets)
        }
      }
      if (stor.elemType === 'droite') {
        if ((stor.zonenom.reponsechap()[0].indexOf('(') === -1) || (stor.zonenom.reponsechap()[0].indexOf(')') === -1)) {
          stor.AideNom.push(textes.ErDteParent)
        }
      }
      if (stor.elemType === 'demi-droite') {
        if (((stor.zonenom.reponsechap()[0].indexOf('(') === -1) && (stor.zonenom.reponsechap()[0].indexOf(')') === -1)) || ((stor.zonenom.reponsechap()[0].indexOf('[') === -1) && (stor.zonenom.reponsechap()[0].indexOf(']') === -1))) {
          stor.AideNom.push(textes.ErDte2Parent)
        } else {
          let pl, OrEl
          if (stor.zonenom.reponsechap()[0].indexOf('[') === -1) {
            pl = stor.zonenom.reponsechap()[0].indexOf(']')
            if (pl !== 0) {
              OrEl = stor.zonenom.reponsechap()[0][pl - 1]
              if (OrEl !== stor.NomElem[0].texte[1]) {
                stor.AideNom.push(textes.ErDte2Origine)
              }
            }
          } else {
            pl = stor.zonenom.reponsechap()[0].indexOf('[')
            if (stor.zonenom.reponsechap()[0].length > pl + 1) {
              OrEl = stor.zonenom.reponsechap()[0][pl + 1]
              if (OrEl !== stor.NomElem[0].texte[1]) {
                stor.AideNom.push(textes.ErDte2Origine)
              }
            }
          }
        }
      }
      if ((stor.elemType !== 'demi-droite') && (stor.elemType !== 'cercle') && (stor.elemType !== 'droite')) {
        if ((stor.zonenom.reponsechap()[0].indexOf('(') !== -1) || (stor.zonenom.reponsechap()[0].indexOf(')') !== -1)) {
          stor.AideNom.push(textes.ErParFo.replace('£a', stor.elemTypeUnd))
        }
      }
      if ((stor.elemType !== 'segment') && (stor.elemType !== 'demi-droite')) {
        if ((stor.zonenom.reponsechap()[0].indexOf('[') !== -1) || (stor.zonenom.reponsechap()[0].indexOf(']') !== -1)) {
          stor.AideNom.push(textes.ErCroFo.replace('£a', stor.elemTypeUnd))
        }
      }
      if ((stor.elemType === 'face')) {
        let yatout = false
        if (stor.zonenom.reponsechap()[0].length === stor.NomElem[0].texte.length) {
          yatout = true
          for (const txt of stor.NomElem[0].texte) {
            yatout = yatout && (stor.zonenom.reponsechap()[0].indexOf(txt) !== -1)
          }
          if (yatout) { stor.AideNom.push(textes.ErDesordre) }
        }
      }
      if (stor.elemType === 'angle') {
        if (stor.zonenom.reponsechap()[0].length === 3) {
          const yasommet = (stor.zonenom.reponsechap()[0].indexOf(stor.NomElem[0].texte[1]) !== -1)
          if ((yasommet) && (stor.zonenom.reponsechap()[0].indexOf(stor.NomElem[0].texte[1]) !== 1)) { stor.AideNom.push(textes.ErSommet) }
        }
      }
    }
    if (stor.ErreurChap) {
      if (stor.NomElem[0].chapo === 1) {
        stor.AideNom.push(textes.ErAngleOubli)
      }
      if (stor.NomElem[0].chapo === 2) {
        stor.AideNom.push(textes.ErArcOubli)
      }
      if (stor.NomElem[0].chapo === 0) {
        if (stor.zonenom.reponsechap()[1] === 2) {
          stor.AideNom.push(textes.ErArcTrop)
        } else { stor.AideNom.push(textes.ErAngleTrop) }
      }
    }

    return ((!stor.ErreurNom) && (!stor.ErreurNat) && (!stor.ErreurChap))
  }

  function creeZoneNom () {
    stor.zonenom = new ZoneStyleMathquill1(stor.pourNom, {
      restric: 'ABCDEFGHIJKLMNOPQRSTUVWXYZabcdefghijklmnopqrstuvwxyz[](),;. ^$',
      limitenb: 9, // FIXME ce param n’existe pas dans ZoneStyleMathquill1
      limite: 5,
      enter: me.sectionCourante.bind(me),
      hasAutoKeyboard: true // FIXME ce param n’existe pas dans ZoneStyleMathquill1
    })
  }

  function initSection () {
    // Construction de la page
    me.construitStructurePage('presentation3')

    ds.ContenantPossible = []
    let figplanes = false
    let seg = false
    const dte = false
    let demidroite = false
    let triangles = false
    let quad = false
    let arc = false
    let cercle = false
    let angle = false
    let solid = false
    let PAVEDROIT = false
    let prisme = false
    let cylindre = false
    let pyra = false
    let cone = false
    let sphere = false
    let secteur = false

    ds.Nature = (ds.Type_Question.indexOf('Nature') !== -1)
    ds.Nomme = (ds.Type_Question.indexOf('Nomme') !== -1)

    ds.SEGMENT = ds.SEGMENT && (ds.milieu || ds.extremites || ds.mediatrice || ds.longueur)
    ds.DEMI_DROITE = ds.DEMI_DROITE && ds.origine
    ds.TRIANGLE = ds.TRIANGLE && (ds.sommet || ds.sommet_oppose || ds.cote || ds.cote_oppose || ds.angle || ds.mediane || ds.hauteur || ds.mediatrice || ds.longueur)
    ds.TRIANGLE_ISOCELE = ds.TRIANGLE_ISOCELE && (ds.sommet || ds.sommet_oppose || ds.cote || ds.cote_oppose || ds.angle || ds.angle_base || ds.sommet_principal || ds.longueur)
    ds.TRIANGLE_EQUILATERAL = ds.TRIANGLE_EQUILATERAL && (ds.sommet || ds.sommet_oppose || ds.cote || ds.cote_oppose || ds.angle || ds.longueur)
    ds.TRIANGLE_RECTANGLE = ds.TRIANGLE_RECTANGLE && (ds.sommet || ds.sommet_oppose || ds.cote || ds.cote_oppose || ds.angle || ds.hauteur || ds.mediatrice || ds.longueur || ds.cote_adjacent || ds.cote_oppose_angle || ds.sommet_angle_droit || ds.hypotenuse)
    ds.QUADRILATERE = ds.QUADRILATERE && (ds.diagonale || ds.sommet || ds.sommet_oppose || ds.sommet_consecutif || ds.cote || ds.cote_oppose || ds.cote_consecutif || ds.angle || ds.angle_oppose || ds.angle_consecutif || ds.longueur)
    ds.RECTANGLE = ds.RECTANGLE && (ds.centre || ds.diagonale || ds.sommet || ds.sommet_oppose || ds.sommet_consecutif || ds.cote || ds.cote_oppose || ds.cote_consecutif || ds.angle || ds.angle_oppose || ds.angle_consecutif || ds.longueur)
    ds.CARRE = ds.CARRE && (ds.centre || ds.diagonale || ds.sommet || ds.sommet_oppose || ds.sommet_consecutif || ds.cote || ds.cote_oppose || ds.cote_consecutif || ds.angle || ds.angle_oppose || ds.angle_consecutif || ds.longueur)
    ds.LOSANGE = ds.LOSANGE && (ds.centre || ds.diagonale || ds.sommet || ds.sommet_oppose || ds.sommet_consecutif || ds.cote || ds.cote_oppose || ds.cote_consecutif || ds.angle || ds.angle_oppose || ds.angle_consecutif || ds.longueur)
    ds.PARALLELOGRAMME = ds.PARALLELOGRAMME && (ds.centre || ds.diagonale || ds.sommet || ds.sommet_oppose || ds.sommet_consecutif || ds.cote || ds.cote_oppose || ds.cote_consecutif || ds.angle || ds.angle_oppose || ds.angle_consecutif || ds.longueur || ds.hauteur)
    ds.CERCLE = ds.CERCLE && (ds.rayon || ds.diametre || ds.corde || ds.le_rayon || ds.le_diametre || ds.centre)
    ds.ARC = ds.ARC && (ds.extremites || ds.centre)
    ds.SECTEUR = ds.SECTEUR && (ds.centre || ds.angle || ds.rayon || ds.le_rayon)
    ds.ANGLE = ds.ANGLE && (ds.cote || ds.sommet || ds.bissectrice)
    ds.CUBE = ds.CUBE && (ds.arete || ds.arete_cachee || ds.sommet || ds.face || ds.face_cachee || ds.longueur)
    ds.PAVEDROIT = ds.PAVEDROIT && (ds.arete || ds.arete_cachee || ds.sommet || ds.face || ds.face_cachee || ds.longueur)
    ds.PRISME = ds.PRISME && (ds.arete || ds.arete_cachee || ds.sommet || ds.face || ds.face_cachee || ds.longueur || ds.base || ds.face_laterale || ds.arete_laterale || ds.hauteur)
    ds.CYLINDRE = ds.CYLINDRE && (ds.rayon || ds.diametre || ds.le_rayon || ds.le_diametre || ds.face || ds.face_cachee || ds.base || ds.face_laterale || ds.hauteur)
    ds.CYLINDRE_REVOLUTION = ds.CYLINDRE_REVOLUTION && (ds.rayon || ds.diametre || ds.le_rayon || ds.le_diametre || ds.face || ds.face_cachee || ds.base || ds.face_laterale || ds.hauteur)
    ds.PYRAMIDE = ds.PYRAMIDE && (ds.arete || ds.arete_cachee || ds.sommet || ds.face || ds.face_cachee || ds.longueur || ds.base || ds.face_laterale || ds.arete_laterale || ds.hauteur || ds.sommet_principal)
    ds.CONE = ds.CONE && (ds.centre || ds.rayon || ds.diametre || ds.le_rayon || ds.le_diametre || ds.face || ds.base || ds.face_laterale || ds.hauteur || ds.sommet_principal)
    ds.CONE_REVOLUTION = ds.CONE_REVOLUTION && (ds.generatrice || ds.centre || ds.rayon || ds.diametre || ds.le_rayon || ds.le_diametre || ds.face || ds.base || ds.face_laterale || ds.hauteur || ds.sommet_principal)
    ds.SPHERE = ds.SPHERE && (ds.rayon || ds.diametre || ds.le_rayon || ds.le_diametre || ds.centre || ds.pole || ds.meridien || ds.grand_cercle || ds.equateur || ds.parallele)

    if (ds.SEGMENT) {
      figplanes = true
      seg = true
      ds.ContenantPossible.push('SEGMENT')
    }
    if (ds.DEMI_DROITE) {
      figplanes = true
      demidroite = true
      ds.ContenantPossible.push('DEMI_DROITE')
    }
    if (ds.TRIANGLE) {
      figplanes = true
      triangles = true
      ds.ContenantPossible.push('TRIANGLE')
    }
    if (ds.TRIANGLE_ISOCELE) {
      figplanes = true

      triangles = true
      ds.ContenantPossible.push('TRIANGLE_ISOCELE')
    }
    if (ds.TRIANGLE_EQUILATERAL) {
      figplanes = true

      triangles = true
      ds.ContenantPossible.push('TRIANGLE_EQUILATERAL')
    }
    if (ds.TRIANGLE_RECTANGLE) {
      figplanes = true

      triangles = true
      ds.ContenantPossible.push('TRIANGLE_RECTANGLE')
    }
    if (ds.QUADRILATERE) {
      figplanes = true
      quad = true
      ds.ContenantPossible.push('QUADRILATERE')
    }
    if (ds.RECTANGLE) {
      figplanes = true

      quad = true
      ds.ContenantPossible.push('RECTANGLE')
    }
    if (ds.CARRE) {
      figplanes = true

      quad = true
      ds.ContenantPossible.push('CARRE')
    }
    if (ds.LOSANGE) {
      figplanes = true

      quad = true
      ds.ContenantPossible.push('LOSANGE')
    }
    if (ds.PARALLELOGRAMME) {
      figplanes = true

      quad = true
      ds.ContenantPossible.push('PARALLELOGRAMME')
    }
    if (ds.CERCLE) {
      figplanes = true
      cercle = true
      ds.ContenantPossible.push('CERCLE')
    }
    if (ds.ARC) {
      figplanes = true
      arc = true
      ds.ContenantPossible.push('ARC')
    }
    if (ds.SECTEUR) {
      figplanes = true
      secteur = true
      ds.ContenantPossible.push('SECTEUR')
    }
    if (ds.ANGLE) {
      figplanes = true
      angle = true
      ds.ContenantPossible.push('ANGLE')
    }
    if (ds.CUBE) {
      solid = true
      PAVEDROIT = true
      prisme = true
      ds.ContenantPossible.push('CUBE')
    }
    if (ds.PAVEDROIT) {
      solid = true
      PAVEDROIT = true
      prisme = true
      ds.ContenantPossible.push('PAVEDROIT')
    }
    if (ds.PRISME) {
      solid = true
      prisme = true
      ds.ContenantPossible.push('PRISME')
    }
    if (ds.CYLINDRE) {
      solid = true
      cylindre = true
      ds.ContenantPossible.push('CYLINDRE')
    }
    if (ds.CYLINDRE_REVOLUTION) {
      solid = true
      cylindre = true
      ds.ContenantPossible.push('CYLINDRE_REVOLUTION')
    }
    if (ds.PYRAMIDE) {
      solid = true
      pyra = true
      ds.ContenantPossible.push('PYRAMIDE')
    }
    if (ds.CONE) {
      solid = true
      cone = true
      ds.ContenantPossible.push('CONE')
    }
    if (ds.CONE_REVOLUTION) {
      solid = true
      cone = true
      ds.ContenantPossible.push('CONE_REVOLUTION')
    }
    if (ds.SPHERE) {
      solid = true
      sphere = true
      ds.ContenantPossible.push('SPHERE')
    }

    ds.ElemPossible = []
    if (ds.arete) { if (prisme || pyra) { ds.ElemPossible.push('arete') } }
    if (ds.arete_cachee) { if (prisme || pyra) { ds.ElemPossible.push('arete_cachee') } }
    if (ds.arete_laterale) { if (ds.PRISME || pyra) { ds.ElemPossible.push('arete_laterale') } }
    if (ds.sommet) { if (pyra || prisme || ds.TRIANGLE || ds.TRIANGLE_RECTANGLE || ds.TRIANGLE_ISOCELE || ds.TRIANGLE_EQUILATERAL || ds.QUADRILATERE || ds.CARRE || ds.RECTANGLE || ds.LOSANGE || ds.PARALLELOGRAMME || ds.ANGLE) { ds.ElemPossible.push('sommet') } }
    if (ds.sommet_oppose) { if (ds.TRIANGLE || ds.TRIANGLE_RECTANGLE || ds.TRIANGLE_ISOCELE || ds.TRIANGLE_EQUILATERAL || ds.QUADRILATERE || ds.CARRE || ds.RECTANGLE || ds.LOSANGE || ds.PARALLELOGRAMME) { ds.ElemPossible.push('sommet_oppose') } }
    if (ds.sommet_principal) { if (ds.TRIANGLE_ISOCELE || pyra || cone) { ds.ElemPossible.push('sommet_principal') } }
    if (ds.sommet_consecutif) { if (ds.QUADRILATERE || ds.CARRE || ds.RECTANGLE || ds.LOSANGE || ds.PARALLELOGRAMME) { ds.ElemPossible.push('sommet_consecutif') } }
    if (ds.face_cachee) { if (pyra || prisme || (ds.Nature && (cylindre))) { ds.ElemPossible.push('face_cachee') } }
    if (ds.face_laterale) { if (ds.PRISME || pyra || (ds.Nature && (cylindre || cone))) { ds.ElemPossible.push('face_laterale') } }
    if (ds.face) { if (pyra || prisme || (ds.Nature && (cylindre || cone))) { ds.ElemPossible.push('face') } }
    if (ds.cote) { if (ds.TRIANGLE || ds.TRIANGLE_RECTANGLE || ds.TRIANGLE_ISOCELE || ds.TRIANGLE_EQUILATERAL || ds.QUADRILATERE || ds.CARRE || ds.RECTANGLE || ds.LOSANGE || ds.PARALLELOGRAMME || ds.ANGLE) { ds.ElemPossible.push('cote') } }
    if (ds.cote_oppose) { if (ds.TRIANGLE || ds.TRIANGLE_RECTANGLE || ds.TRIANGLE_ISOCELE || ds.TRIANGLE_EQUILATERAL || ds.QUADRILATERE || ds.CARRE || ds.RECTANGLE || ds.LOSANGE || ds.PARALLELOGRAMME || ds.ANGLE) { ds.ElemPossible.push('cote_oppose') } }
    if (ds.cote_oppose_angle) { if (ds.TRIANGLE_RECTANGLE) { ds.ElemPossible.push('cote_oppose_angle') } }
    if (ds.cote_adjacent) { if (ds.TRIANGLE_RECTANGLE) { ds.ElemPossible.push('cote_adjacent') } }
    if (ds.cote_consecutif) { if (ds.QUADRILATERE || ds.CARRE || ds.RECTANGLE || ds.LOSANGE || ds.PARALLELOGRAMME) { ds.ElemPossible.push('cote_consecutif') } }
    if (ds.rayon) { if (secteur || ds.CERCLE || cylindre || cone || sphere) { ds.ElemPossible.push('rayon') } }
    if (ds.diametre) { if (ds.CERCLE || cylindre || cone || sphere) { ds.ElemPossible.push('diametre') } }
    if (ds.corde) { if (ds.CERCLE) { ds.ElemPossible.push('corde') } }
    if (ds.hypotenuse) { if (ds.TRIANGLE_RECTANGLE) { ds.ElemPossible.push('hypotenuse') } }
    if (ds.diagonale) { if (ds.QUADRILATERE || ds.CARRE || ds.RECTANGLE || ds.LOSANGE || ds.PARALLELOGRAMME) { ds.ElemPossible.push('diagonale') } }
    if (ds.generatrice) { if (ds.CONE_REVOLUTION) { ds.ElemPossible.push('generatrice') } }
    if (ds.angle) { if (secteur || ds.TRIANGLE || ds.TRIANGLE_RECTANGLE || ds.TRIANGLE_ISOCELE || ds.TRIANGLE_EQUILATERAL || ds.QUADRILATERE || ds.CARRE || ds.RECTANGLE || ds.LOSANGE || ds.PARALLELOGRAMME) { ds.ElemPossible.push('angle') } }
    if (ds.angle_base) { if (ds.TRIANGLE_ISOCELE) { ds.ElemPossible.push('angle_base') } }
    if (ds.angle_oppose) { if (ds.QUADRILATERE || ds.CARRE || ds.RECTANGLE || ds.LOSANGE || ds.PARALLELOGRAMME) { ds.ElemPossible.push('angle_oppose') } }
    if (ds.angle_consecutif) { if (ds.QUADRILATERE || ds.CARRE || ds.RECTANGLE || ds.LOSANGE || ds.PARALLELOGRAMME) { ds.ElemPossible.push('angle_consecutif') } }
    if (ds.meridien) { if (sphere && ds.Nature) { ds.ElemPossible.push('meridien') } }
    if (ds.equateur) { if (sphere && ds.Nature) { ds.ElemPossible.push('equateur') } }
    if (ds.parallele) { if (sphere && ds.Nature) { ds.ElemPossible.push('parallele') } }
    if (ds.grand_cercle) { if (sphere && ds.Nature) { ds.ElemPossible.push('grand_cercle') } }
    if (ds.pole) { if (ds.SPHERE) { ds.ElemPossible.push('pole') } }
    if (ds.origine) { if (ds.DEMI_DROITE) { ds.ElemPossible.push('origine') } }
    if (ds.milieu) { if (ds.SEGMENT) { ds.ElemPossible.push('milieu') } }
    if (ds.extremites) { if (ds.SEGMENT || ds.ARC) { ds.ElemPossible.push('extremites') } }
    if (ds.centre) { if (secteur || cone || ds.CARRE || ds.RECTANGLE || ds.LOSANGE || ds.PARALLELOGRAMME || ds.CERCLE || ds.ARC || ds.SPHERE) { ds.ElemPossible.push('centre') } }
    if (ds.bissectrice) { if (ds.ANGLE) { ds.ElemPossible.push('bissectrice') } }
    if (ds.mediatrice) { if (ds.SEGMENT || ds.TRIANGLE || ds.TRIANGLE_RECTANGLE) { ds.ElemPossible.push('mediatrice') } }
    if (ds.hauteur) { if (cylindre || cone || pyra || ds.PRISME || ds.TRIANGLE || ds.TRIANGLE_RECTANGLE || ds.PARALLELOGRAMME) { ds.ElemPossible.push('hauteur') } }
    if (ds.mediane) { if (ds.TRIANGLE) { ds.ElemPossible.push('mediane') } }
    if (ds.base) { if (pyra || ds.PRISME || ds.TRIANGLE_ISOCELE || (ds.Nature && (cylindre || cone))) { ds.ElemPossible.push('base') } }
    if (ds.longueur) { if (seg || triangles || quad || PAVEDROIT || prisme || pyra) { ds.ElemPossible.push('longueur') } }
    if (ds.le_rayon) { if (secteur || ds.CERCLE || cylindre || cone || sphere) { ds.ElemPossible.push('le_rayon') } }
    if (ds.le_diametre) { if (ds.CERCLE || cylindre || cone || sphere) { ds.ElemPossible.push('le_diametre') } }

    if (ds.ElemPossible.length === 0) {
      return j3pShowError('Les paramètres de la section ne permettent aucun exercice !')
    }

    let letitrefin = 'd’une figure'
    if (!(solid && figplanes)) {
      if (!solid) {
        letitrefin = 'd’une figure plane'
        if ((!secteur) && seg && (!demidroite) && (!triangles) && (!quad) && (!cercle) && (!arc) && (!angle) && (!dte)) { letitrefin = 'd’un segment' }
        if (secteur && (!seg) && (!demidroite) && (!triangles) && (!quad) && (!cercle) && (!arc) && (!angle) && (!dte)) { letitrefin = 'd’un secteur circulaire' }
        if ((!secteur) && (!seg) && (demidroite) && (!triangles) && (!quad) && (!cercle) && (!arc) && (!angle) && (!dte)) { letitrefin = 'd’une demi-droite' }
        if ((!secteur) && (!seg) && (demidroite) && (!triangles) && (!quad) && (!cercle) && (!arc) && (!angle) && (dte)) { letitrefin = 'd’une droite' }
        if ((!secteur) && (!seg) && (!demidroite) && (!triangles) && (!quad) && (cercle) && (!arc) && (!angle) && (!dte)) { letitrefin = 'd’un cercle' }
        if ((!secteur) && (!seg) && (!demidroite) && (!triangles) && (!quad) && (!cercle) && (arc) && (!angle) && (!dte)) { letitrefin = 'd’un arc de cercle' }
        if ((!secteur) && (!seg) && (!demidroite) && (!triangles) && (!quad) && (!cercle) && (!arc) && (angle) && (!dte)) { letitrefin = 'd’un angle' }
        if ((!secteur) && (!seg) && (!demidroite) && (triangles) && (!quad) && (!cercle) && (!arc) && (!angle) && (!dte)) {
          letitrefin = 'd’un triangle'
          if (!ds.TRIANGLE) {
            letitrefin = 'd’un triangle particulier'
            if ((ds.TRIANGLE_ISOCELE) && (!ds.TRIANGLE_EQUILATERAL) && (!ds.TRIANGLE_RECTANGLE)) { letitrefin = 'd’un triangle isocèle' }

            if ((!ds.TRIANGLE_ISOCELE) && (ds.TRIANGLE_EQUILATERAL) && (!ds.TRIANGLE_RECTANGLE)) { letitrefin = 'd’un triangle équilatéral' }
            if ((!ds.TRIANGLE_ISOCELE) && (!ds.TRIANGLE_EQUILATERAL) && (ds.TRIANGLE_RECTANGLE)) { letitrefin = 'd’un triangle rectangle' }
          }
        }
        if ((!secteur) && (!seg) && (!demidroite) && (!triangles) && (quad) && (!cercle) && (!arc) && (!angle) && (!dte)) {
          letitrefin = 'd’un quadrilatère'
          if (!ds.QUADRILATERE) {
            letitrefin = 'd’un quadrilatère particulier'
            if ((ds.CARRE) && (!ds.RECTANGLE) && (!ds.LOSANGE) && (!ds.PARALLELOGRAMME)) { letitrefin = 'd’un carré' }

            if ((!ds.CARRE) && (ds.RECTANGLE) && (!ds.LOSANGE) && (!ds.PARALLELOGRAMME)) { letitrefin = 'd’un rectangle' }
            if ((!ds.CARRE) && (!ds.RECTANGLE) && (ds.LOSANGE) && (!ds.PARALLELOGRAMME)) { letitrefin = 'd’un losange' }
            if ((!ds.CARRE) && (!ds.RECTANGLE) && (!ds.LOSANGE) && (ds.PARALLELOGRAMME)) { letitrefin = 'd’un parallèlogramme' }
          }
        }
      } else {
        letitrefin = 'd’un solide'
        if (prisme && (!pyra) && (!cylindre) && (!cone) && (!sphere)) {
          letitrefin = 'd’un prisme'
          if ((ds.CUBE) && (!ds.PAVEDROIT) && (!ds.PRISME)) { letitrefin = 'd’un cube' }
          if ((ds.PAVEDROIT) && (!ds.PRISME)) { letitrefin = 'd’un pavé droit' }
        }
        if ((!prisme) && (pyra) && (!cylindre) && (!cone) && (!sphere)) { letitrefin = 'd’une pyramide' }
        if ((!prisme) && (!pyra) && (cylindre) && (!cone) && (!sphere)) {
          letitrefin = 'd’un cylindre'
          if (!ds.CYLINDRE) { letitrefin = 'd’un cylindre de révolution' }
        }
        if ((!prisme) && (!pyra) && (!cylindre) && (cone) && (!sphere)) {
          letitrefin = 'd’un cône'
          if (!ds.CONE) { letitrefin = 'd’un cône de révolution' }
        }
        if ((!prisme) && (!pyra) && (!cylindre) && (!cone) && (sphere)) { letitrefin = 'd’une sphère' }
      }
    }

    me.typederreurs = [0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0]
    me.score = 0
    me.afficheTitre('Element ' + letitrefin)

    if (me.donneesSection.indication) me.indication(me.zones.IG, me.donneesSection.indication)

    stor.contenantsuse = []
    stor.elementuse = []
    getMtgCore({ withMathJax: true })
      .then(
        // success
        (mtgAppLecteur) => {
          stor.mtgAppLecteur = mtgAppLecteur
          stor.listePourCalc = mtgAppLecteur.createList('TWF0aEdyYXBoSmF2YTEuMAAAABI+TMzNAAJmcv###wEA#wEAAAAAAAAAAAQsAAACvQAAAQEAAAAAAAAAAQAAAAP#####AAAAAQAKQ0NhbGNDb25zdAD#####AAJwaQAWMy4xNDE1OTI2NTM1ODk3OTMyMzg0Nv####8AAAABAApDQ29uc3RhbnRlQAkh+1RELRj#####AAAAAQAHQ0NhbGN1bAD#####AAFlAAZleHAoMSn#####AAAAAgAJQ0ZvbmN0aW9uBwAAAAE#8AAAAAAAAAAAAAIA#####wABeAABMAAAAAEAAAAAAAAAAP###############w==')
          enonceMain()
        },
        // failure
        (error) => {
          j3pShowError(error, { message: 'mathgraph n’est pas correctement chargé', mustNotify: true })
        })
      // plantage dans le code de success
      .catch(j3pShowError)
  } // initSection

  function modif (tab) {
    for (let i = 0; i < tab.length; i++) {
      for (let j = 0; j < tab[i].length; j++) {
        tab[i][j].style.padding = 0
        tab[i][j].style.verticalAlign = 'bottom'
      }
    }
  }

  function enonceMain () {
    me.videLesZones()
    stor.lesdiv = {}
    stor.lesdiv.conteneur = j3pAddElt(me.zonesElts.MG, 'div', {
      style: me.styles.etendre('toutpetit.enonce', { padding: '10px' })
    })
    me.zonesElts.MG.classList.add('fond')
    const tt = addDefaultTable(stor.lesdiv.conteneur, 1, 3)
    stor.lesdiv.figure = tt[0][0]
    tt[0][1].style.width = '5px'
    const tt2 = addDefaultTable(tt[0][2], 3, 1)
    stor.lesdiv.correction = tt2[0][0]
    j3pAffiche(tt2[1][0], null, '\n')
    stor.lesdiv.travail = tt2[2][0]
    stor.lesdiv.explications = j3pAddElt(me.zonesElts.MG, 'div')
    stor.lesdiv.explications2 = j3pAddElt(me.zonesElts.MG, 'div')

    let cptuse = 0
    let oksort = false
    do {
      stor.contenant = ds.ContenantPossible[j3pGetRandomInt(0, (ds.ContenantPossible.length - 1))]
      cptuse++
      oksort = (stor.contenantsuse.indexOf(stor.contenant) === -1)
    } while ((!oksort) && (cptuse < 50))
    stor.contenantsuse.push(stor.contenant)

    // selectionne element
    const elempo = []
    const listecontenant = ['SEGMENT', 'DEMI_DROITE', 'TRIANGLE', 'TRIANGLE_ISOCELE', 'TRIANGLE_EQUILATERAL', 'TRIANGLE_RECTANGLE', 'QUADRILATERE', 'RECTANGLE', 'CARRE', 'LOSANGE', 'PARALLELOGRAMME', 'CERCLE', 'ARC', 'SECTEUR', 'ANGLE', 'CUBE', 'PAVEDROIT', 'PRISME', 'CYLINDRE', 'CYLINDRE_REVOLUTION', 'PYRAMIDE', 'CONE', 'CONE_REVOLUTION', 'SPHERE']
    const listeelemepo = []
    // segment
    listeelemepo[0] = ['longueur', 'extremites', 'milieu', 'centre_symetrie', 'mediatrice']
    // demidroite
    listeelemepo[1] = ['origine']
    // triangle
    listeelemepo[2] = ['longueur', 'mediatrice', 'sommet', 'cote', 'angle', 'sommet_oppose', 'cote_oppose', 'hauteur', 'mediane']
    // triangle isocele
    listeelemepo[3] = ['longueur', 'sommet', 'cote', 'angle', 'sommet_oppose', 'cote_oppose', 'base', 'sommet_principal', 'angle_base']
    // triangle equilateral
    listeelemepo[4] = ['longueur', 'sommet', 'cote', 'angle', 'sommet_oppose', 'cote_oppose']
    // triangle rectangle
    listeelemepo[5] = ['longueur', 'mediatrice', 'sommet', 'cote', 'angle', 'sommet_oppose', 'cote_oppose', 'hauteur', 'hypotenuse', 'angle_droit', 'sommet_angle_droit', 'cote_oppose_angle', 'cote_adjacent']
    // quadrilatere
    listeelemepo[6] = ['longueur', 'sommet', 'cote', 'angle', 'sommet_oppose', 'cote_oppose', 'angle_oppose', 'angle_consecutif', 'sommet_consecutif', 'cote_consecutif', 'diagonale']
    // rectangle
    listeelemepo[7] = ['longueur', 'angle_egal', 'cote_parallele', 'cote_perpendiculaire', 'sommet', 'cote', 'angle', 'sommet_oppose', 'cote_oppose', 'angle_oppose', 'angle_consecutif', 'sommet_consecutif', 'cote_consecutif', 'diagonale', 'angle_droit', 'centre']
    // carré
    listeelemepo[8] = ['longueur', 'angle_egal', 'cote_parallele', 'cote_perpendiculaire', 'sommet', 'cote', 'angle', 'sommet_oppose', 'cote_oppose', 'angle_oppose', 'angle_consecutif', 'sommet_consecutif', 'cote_consecutif', 'diagonale', 'angle_droit', 'centre']
    // losange
    listeelemepo[9] = ['longueur', 'angle_egal', 'cote_parallele', 'sommet', 'cote', 'angle', 'sommet_oppose', 'cote_oppose', 'angle_oppose', 'angle_consecutif', 'sommet_consecutif', 'cote_consecutif', 'diagonale', 'angle_droit', 'centre']
    // parallèlogramme
    listeelemepo[10] = ['longueur', 'angle_egal', 'cote_parallele', 'sommet', 'cote', 'angle', 'sommet_oppose', 'cote_oppose', 'angle_oppose', 'angle_consecutif', 'sommet_consecutif', 'cote_consecutif', 'diagonale', 'angle_droit', 'centre', 'hauteur', 'hauteur_relative', 'base_relative']
    // cercle
    listeelemepo[11] = ['centre', 'rayon', 'corde', 'diametre', 'le_rayon', 'le_diametre']
    // arc
    listeelemepo[12] = ['extremites', 'centre']
    // secteur
    listeelemepo[13] = ['centre', 'angle', 'rayon', 'le_rayon']
    // angle
    listeelemepo[14] = ['sommet', 'cote', 'bissectrice']
    // cube
    listeelemepo[15] = ['longueur', 'face_cachee', 'arete_cachee', 'sommet', 'arete', 'face']
    // pave droit
    listeelemepo[16] = ['longueur', 'face_cachee', 'arete_cachee', 'sommet', 'arete', 'face']
    // prisme
    listeelemepo[17] = ['longueur', 'face_cachee', 'arete_cachee', 'face_parallele', 'face_perpendiculaire', 'arete_parallele', 'arete_perpendiculaire', 'arete_orthogonale', 'sommet', 'arete', 'face', 'face_laterale', 'hauteur', 'base']
    // cylindre
    listeelemepo[18] = ['face_cachee', 'arete', 'face', 'face_laterale', 'hauteur', 'base', 'rayon', 'diametre', 'le_rayon', 'le_diametre']
    // cylindre revo
    listeelemepo[19] = ['face_cachee', 'face', 'face_laterale', 'hauteur', 'base', 'rayon', 'diametre', 'le_rayon', 'le_diametre']
    // pyramide
    listeelemepo[20] = ['longueur', 'face_cachee', 'arete_cachee', 'arete', 'face', 'face_laterale', 'arete_laterale', 'hauteur', 'base', 'sommet', 'sommet_principal']
    // cone
    listeelemepo[21] = ['centre', 'face', 'face_laterale', 'hauteur', 'base', 'sommet_principal', 'rayon', 'diametre', 'le_rayon', 'le_diametre']
    // cone de revo
    listeelemepo[22] = ['generatrice', 'centre', 'face', 'face_laterale', 'hauteur', 'base', 'sommet_principal', 'rayon', 'diametre', 'le_rayon', 'le_diametre']
    // sphere
    listeelemepo[23] = ['pole', 'centre', 'rayon', 'diametre', 'meridien', 'parallele', 'grand_cercle', 'equateur', 'le_rayon', 'le_diametre']

    const contenantnum = listecontenant.indexOf(stor.contenant)
    for (let i = 0; i < ds.ElemPossible.length; i++) {
      if (listeelemepo[contenantnum].indexOf(ds.ElemPossible[i]) !== -1) {
        elempo.push(ds.ElemPossible[i])
      }
    }

    if (elempo.length === 0) {
      elempo.push(listeelemepo[contenantnum][0])
    }
    cptuse = 0
    oksort = false
    do {
      stor.element = elempo[j3pGetRandomInt(0, (elempo.length - 1))]
      cptuse++
      oksort = (stor.elementuse.indexOf(stor.element) === -1)
    } while ((!oksort) && (cptuse < 50))
    stor.elementuse.push(stor.element)

    stor.oknomme = true
    stor.oknomme = stor.oknomme && ((stor.element.indexOf('face') === -1) || (stor.contenant.indexOf('CONE') === -1))
    stor.oknomme = stor.oknomme && ((stor.element.indexOf('face') === -1) || (stor.contenant.indexOf('CYLINDRE') === -1))
    stor.oknomme = stor.oknomme && (stor.element !== 'meridien') && (stor.element !== 'grand_cercle') && (stor.element !== 'equateur') && (stor.element !== 'parallele')
    stor.oknomme = stor.oknomme && ((stor.element.indexOf('base') === -1) || ((stor.contenant.indexOf('CYLINDRE') === -1) && (stor.contenant.indexOf('CONE') === -1)))
    stor.Nomme = ds.Nomme && stor.oknomme
    stor.Nature = ds.Nature

    const elemetype = function (str) {
      if (str.indexOf('sommet') !== -1) { return 'point' }
      if (str.indexOf('face') !== -1) { return 'face' }
      if (str.indexOf('arete') !== -1) { return 'segment' }
      if (str.indexOf('cote') !== -1) {
        if (stor.contenant !== 'ANGLE') { return 'segment' } else { return 'demi-droite' }
      }
      if (str.indexOf('le_rayon') !== -1) { return 'mesure' }
      if (str.indexOf('le_diametre') !== -1) { return 'mesure' }
      if (str.indexOf('rayon') !== -1) { return 'segment' }
      if (str.indexOf('diametre') !== -1) { return 'segment' }
      if (str.indexOf('corde') !== -1) { return 'segment' }
      if (str.indexOf('hypotenuse') !== -1) { return 'segment' }
      if (str.indexOf('generatrice') !== -1) { return 'segment' }
      if (str.indexOf('diagonale') !== -1) { return 'segment' }
      if (str.indexOf('equateur') !== -1) { return 'cercle' }
      if (str.indexOf('meridien') !== -1) { return 'demi-cercle' }
      if (str.indexOf('grand_cercle') !== -1) { return 'cercle' }
      if (str.indexOf('parallele') !== -1) { return 'cercle' }
      if (str.indexOf('origine') !== -1) { return 'point' }
      if (str.indexOf('milieu') !== -1) { return 'point' }
      if (str.indexOf('extremites') !== -1) { return 'point' }
      if (str.indexOf('bissectrice') !== -1) { return 'droite' }
      if (str.indexOf('mediane') !== -1) { return 'droite' }
      if (str.indexOf('mediatrice') !== -1) { return 'droite' }
      if (str.indexOf('hauteur') !== -1) {
        if ((stor.contenant !== 'PRISME') && (stor.contenant !== 'CONE') && (stor.contenant !== 'CONE_REVOLUTION') && (stor.contenant !== 'CYLINDRE_REVOLUTION') && (stor.contenant !== 'CYLINDRE') && (stor.contenant !== 'PYRAMIDE')) { return 'droite' } else { return 'mesure' }
      }
      // spe
      if (str.indexOf('angle_base') !== -1) { return 'angle' }
      if (str.indexOf('base') !== -1) {
        if ((stor.contenant === 'TRIANGLE') || (stor.contenant === 'TRIANGLE_RECTANGLE') || (stor.contenant === 'TRIANGLE_EQUILATERAL') || (stor.contenant === 'TRIANGLE_ISOCELE')) { return 'segment' } else { return 'face' }
      }
      if (str.indexOf('longueur') !== -1) { return 'mesure' }
      if (str.indexOf('angle') !== -1) { return 'angle' }
      if (str.indexOf('centre') !== -1) { return 'point' }
      if (str.indexOf('pole') !== -1) { return 'point' }
    }
    const elemetypeUnd = function (str) {
      if (str.indexOf('sommet') !== -1) { return 'un point' }
      if (str.indexOf('face') !== -1) { return 'une face' }
      if (str.indexOf('arete') !== -1) { return 'un segment' }
      if (str.indexOf('cote') !== -1) {
        if (stor.contenant !== 'ANGLE') { return 'un segment' } else { return 'une demi-droite' }
      }
      if (str.indexOf('le_rayon') !== -1) { return 'une mesure' }
      if (str.indexOf('le_diametre') !== -1) { return 'une mesure' }
      if (str.indexOf('rayon') !== -1) { return 'un segment' }
      if (str.indexOf('diametre') !== -1) { return 'un segment' }
      if (str.indexOf('corde') !== -1) { return 'un segment' }
      if (str.indexOf('hypotenuse') !== -1) { return 'un segment' }
      if (str.indexOf('generatrice') !== -1) { return 'un segment' }
      if (str.indexOf('diagonale') !== -1) { return 'un segment' }
      if (str.indexOf('equateur') !== -1) { return 'un cercle' }
      if (str.indexOf('meridien') !== -1) { return 'une demi-cercle' }
      if (str.indexOf('grand_cercle') !== -1) { return 'un cercle' }
      if (str.indexOf('parallele') !== -1) { return 'un cercle' }
      if (str.indexOf('origine') !== -1) { return 'un point' }
      if (str.indexOf('milieu') !== -1) { return 'un point' }
      if (str.indexOf('extremites') !== -1) { return 'un point' }
      if (str.indexOf('bissectrice') !== -1) { return 'une droite' }
      if (str.indexOf('mediane') !== -1) { return 'une droite' }
      if (str.indexOf('mediatrice') !== -1) { return 'une droite' }
      if (str.indexOf('hauteur') !== -1) {
        if ((stor.contenant !== 'PRISME') && (stor.contenant !== 'CONE') && (stor.contenant !== 'CONE_REVOLUTION') && (stor.contenant !== 'CYLINDRE_REVOLUTION') && (stor.contenant !== 'CYLINDRE') && (stor.contenant !== 'PYRAMIDE')) { return 'une droite' } else { return 'une mesure' }
      }
      // spe
      if (str.indexOf('angle_base') !== -1) { return 'un angle' }
      if (str.indexOf('base') !== -1) {
        if ((stor.contenant === 'TRIANGLE') || (stor.contenant === 'TRIANGLE_RECTANGLE') || (stor.contenant === 'TRIANGLE_EQUILATERAL') || (stor.contenant === 'TRIANGLE_ISOCELE')) { return 'un segment' } else { return 'une face' }
      }
      if (str.indexOf('longueur') !== -1) { return 'une mesure' }
      if (str.indexOf('angle') !== -1) { return 'un angle' }
      if (str.indexOf('centre') !== -1) { return 'un point' }
      if (str.indexOf('pole') !== -1) { return 'un point' }
    }

    stor.elemType = elemetype(stor.element)
    stor.elemTypeUnd = elemetypeUnd(stor.element)

    if (stor.elemType === undefined) j3pShowError(Error('Erreur interne (cas non prévu)'), { mustNotify: true })

    if (stor.element === 'face_cachee') {
      if (j3pGetRandomInt(0, 1) === 0) { stor.element = 'face_visible' }
    }
    if (stor.element === 'arete_cachee') {
      if (j3pGetRandomInt(0, 1) === 0) { stor.element = 'arete_visible' }
    }

    stor.NeedEclair = false
    let needelem = ''
    switch (stor.element) {
      case 'sommet_oppose' :
        stor.lanatureUnd = 'un point'
        stor.lanatureDef = 'Le point'
        stor.lanature = 'le sommet opposé'
        if (stor.contenant.indexOf('TRIANGLE') !== -1) {
          needelem = 'du côté'
          stor.Natureprecise = 'Le sommet opposé au côté'
        } else {
          needelem = ' du sommet'
          stor.Natureprecise = 'Le sommet opposé au sommet'
        }

        break
      case 'sommet_consecutif' :
        stor.lanatureUnd = 'un point'
        stor.lanatureDef = 'Le point'
        stor.lanature = 'un sommet consécutif'
        stor.NeedEclair = true
        needelem = ' du sommet'
        stor.Natureprecise = 'Le sommet violet consécutif au sommet'
        break
      case 'cote_consecutif' :
        stor.lanatureUnd = 'un côté'
        stor.lanatureDef = 'Le côté'
        stor.lanature = 'un côté consécutif'
        needelem = ' du côté'
        stor.Natureprecise = 'Le côté violet consécutif au sommet'
        break
      case 'cote_oppose' :
        stor.lanatureUnd = 'un côté'
        stor.lanatureDef = 'Le côté'
        stor.lanature = 'le côté opposé'
        if (stor.contenant.indexOf('TRIANGLE') !== -1) {
          needelem = 'du sommet'
          stor.Natureprecise = 'Le coté violet opposé au sommet'
        } else {
          needelem = ' du côté'
          stor.Natureprecise = 'Le côté violet opposé au côté'
        }
        break
      case 'cote_adjacent' :
        stor.lanatureUnd = 'un côté'
        stor.lanatureDef = 'Le côté'
        stor.lanature = 'le côté adjacent'
        needelem = ' de l’angle aigu'
        stor.Natureprecise = 'Le côté adjacent à l’angle aigu'
        break
      case 'cote_oppose_angle' :
        stor.lanatureUnd = 'un côté'
        stor.lanatureDef = 'Le côté'
        stor.lanature = 'le côté opposé'
        needelem = ' de l’angle aigu'
        stor.Natureprecise = 'Le côté opposé à l’angle aigu'
        break
      case 'angle_oppose' :
        stor.lanatureUnd = 'un angle'
        stor.lanatureDef = 'L’angle'
        stor.lanature = 'l’angle opposé'
        needelem = ' de l’angle'
        stor.Natureprecise = 'L’angle opposé à l’angle'
        break
      case 'angle_consecutif' :
        stor.lanatureUnd = 'un angle'
        stor.lanatureDef = 'L’angle'
        stor.lanature = 'un angle consécitif'
        stor.NeedEclair = true
        needelem = ' de l’angle'
        stor.Natureprecise = 'L’angle violet consécutif à l’angle'
        break
      case 'longueur' :
        stor.lanatureUnd = 'une longueur'
        stor.lanatureDef = 'La longueur'
        stor.lanature = 'la longueur'
        if ((stor.contenant.indexOf('TRIANGLE') !== -1) || (stor.contenant === 'PARALLELOGRAMME') || (stor.contenant === 'QUADRILATERE') || (stor.contenant === 'LOSANGE') || (stor.contenant === 'RECTANGLE') || (stor.contenant === 'CARRE')) {
          needelem = ' du côté'
          stor.Natureprecise = 'La longueur du côté'
        } else if (stor.contenant !== 'SEGMENT') {
          needelem = ' de l’arête'
          stor.Natureprecise = 'La longueur de l’arête'
        } else { stor.Natureprecise = 'La longueur' }
        break
      case 'arete' :
        stor.lanatureUnd = 'une arête'
        stor.lanatureDef = 'l’arête'
        stor.lanature = 'un arête'
        stor.NeedEclair = true
        stor.Natureprecise = 'L’arête violette'
        break
      case 'arete_cachee' :
        stor.lanatureUnd = 'une arête'
        stor.lanatureDef = 'l’arête'
        stor.lanature = 'une arête cachée'
        stor.NeedEclair = true
        stor.Natureprecise = 'L’arête cachée violette'
        break
      case 'arete_visible' :
        stor.lanatureUnd = 'une arête'
        stor.lanatureDef = 'l’arête'
        stor.lanature = 'une arête visible'
        stor.NeedEclair = true
        stor.Natureprecise = 'L’arête visible violette'
        break
      case 'arete_laterale' :
        stor.lanatureUnd = 'une arête'
        stor.lanatureDef = 'l’arête'
        stor.lanature = 'une arête latérale'
        stor.NeedEclair = true
        stor.Natureprecise = 'L’arête latérale violette'
        break
      case 'sommet' :
        stor.lanatureUnd = 'un point'
        stor.lanatureDef = 'Le point'
        stor.lanature = 'un sommet'
        stor.NeedEclair = true
        stor.Natureprecise = 'Le sommet violet'
        break
      case 'sommet_principal' :
        stor.lanatureUnd = 'un point'
        stor.lanatureDef = 'Le point'
        stor.lanature = 'le sommet'
        stor.Natureprecise = 'Le sommet'
        if ((stor.contenant !== 'PYRAMIDE') && (stor.contenant !== 'CONE') && (stor.contenant !== 'CONE_REVOLUTION')) {
          stor.Natureprecise = 'Le sommet principal'
          stor.lanature = 'le sommet principal'
        }
        break
      case 'sommet_angle_droit' :
        stor.lanatureUnd = 'un point'
        stor.lanatureDef = 'Le point'
        stor.lanature = 'le sommet de l’angle droit'
        stor.Natureprecise = 'Le sommet de l’angle droit'
        break
      case 'face' :
        stor.lanatureUnd = 'une face'
        stor.lanatureDef = 'La face'
        stor.lanature = 'une face'
        stor.NeedEclair = true
        stor.Natureprecise = 'La face violette'
        break
      case 'face_laterale' :
        stor.lanatureUnd = 'une face'
        stor.lanatureDef = 'La face'
        stor.lanature = 'la face latérale'
        stor.Natureprecise = 'La face latérale violette'
        if ((stor.contenant !== 'CYLINDRE') && (stor.contenant !== 'CYLINDRE_REVOLUTION') && (stor.contenant !== 'CONE') && (stor.contenant !== 'CONE_REVOLUTION')) {
          stor.NeedEclair = true
          stor.Natureprecise = 'La face latérale'
        }
        break
      case 'face_cachee' :
        stor.lanatureUnd = 'une face'
        stor.lanatureDef = 'La face'
        stor.lanature = 'une face cachée'
        stor.NeedEclair = true
        stor.Natureprecise = 'La face cachée violette'
        break
      case 'face_visible' :
        stor.lanatureUnd = 'une face'
        stor.lanatureDef = 'La face'
        stor.lanature = 'une face visible'
        stor.NeedEclair = true
        stor.Natureprecise = 'La face visible violette'
        break
      case 'cote' :
        stor.lanatureUnd = 'un côté'
        stor.lanatureDef = 'Le côté'
        stor.lanature = 'un côté'
        stor.NeedEclair = true
        stor.Natureprecise = 'Le côté violet'
        break
      case 'rayon' :
        stor.lanatureUnd = 'un rayon'
        stor.lanatureDef = 'Le rayon'
        stor.lanature = 'un rayon'
        stor.NeedEclair = true
        stor.Natureprecise = 'Le rayon violet'
        break
      case 'diametre' :
        stor.lanatureUnd = 'un diamètre'
        stor.lanatureDef = 'Le diamètre'
        stor.lanature = 'un diamètre'
        stor.Natureprecise = 'Le diamètre tracé'
        break
      case 'corde' :
        stor.lanatureUnd = 'une corde'
        stor.lanatureDef = 'La corde'
        stor.lanature = 'une corde'
        stor.Natureprecise = 'La corde tracée'
        break
      case 'hypotenuse' :
        stor.lanatureUnd = 'une hypoténuse'
        stor.lanatureDef = 'L’hypoténuse'
        stor.lanature = 'l’hypoténuse'
        stor.Natureprecise = 'L’hypoténuse'
        break
      case 'diagonale' :
        stor.lanatureUnd = 'une diagonale'
        stor.lanatureDef = 'La diagonale'
        stor.lanature = 'une diagonale'
        stor.NeedEclair = true
        stor.Natureprecise = 'La diagonale violette'
        break
      case 'generatrice' :
        stor.lanatureUnd = 'une génératrice'
        stor.lanatureDef = 'La génératrice'
        stor.lanature = 'une génératrice'
        stor.NeedEclair = true
        stor.Natureprecise = 'La génératrice violette'
        break
      case 'angle' :
        stor.lanatureUnd = 'un angle'
        stor.lanatureDef = 'L’angle'
        stor.lanature = 'un angle'
        stor.NeedEclair = true
        stor.Natureprecise = 'L’angle violet'
        break
      case 'angle_base' :
        stor.lanatureUnd = 'un angle'
        stor.lanatureDef = 'L’angle'
        stor.lanature = 'un angle à la base'
        stor.NeedEclair = true
        stor.Natureprecise = 'L’angle à la base violet'
        break
      case 'pole' :
        stor.lanatureUnd = 'un point'
        stor.lanatureDef = 'Le point'
        stor.lanature = 'un pôle'
        stor.NeedEclair = true
        stor.Natureprecise = 'Le pôle violet'
        break
      case 'origine' :
        stor.lanatureUnd = 'un point'
        stor.lanatureDef = 'Le point'
        stor.lanature = 'l’origine'
        stor.Natureprecise = 'L’origine'
        break
      case 'milieu' :
        stor.lanatureUnd = 'un point'
        stor.lanatureDef = 'Le point'
        stor.lanature = 'le milieu'
        stor.Natureprecise = 'Le milieu'
        break
      case 'extremites' :
        stor.lanatureUnd = 'un point'
        stor.lanatureDef = 'Le point'
        stor.lanature = 'une extrémité'
        stor.NeedEclair = true
        stor.Natureprecise = 'L’extrémité violette'
        break
      case 'centre' :
        stor.lanatureUnd = 'un point'
        stor.lanatureDef = 'Le point'
        stor.lanature = 'le centre'
        stor.Natureprecise = 'Le centre'
        break
      case 'bissectrice' :
        stor.lanatureUnd = 'une droite'
        stor.lanatureDef = 'La droite'
        stor.lanature = 'une bissectrice'
        stor.Natureprecise = 'La bissectrice violette'
        break
      case 'mediatrice' :
        stor.lanatureUnd = 'une droite'
        stor.lanatureDef = 'La droite'
        stor.lanature = 'une médiatrice'
        stor.Natureprecise = 'La médiatrice'
        break
      case 'hauteur' :
        stor.lanatureUnd = 'une droite'
        stor.lanatureDef = 'La droite'
        stor.lanature = 'une hauteur'
        stor.Natureprecise = 'La hauteur violette'
        if ((stor.contenant === 'PRISME') || (stor.contenant === 'CYLINDRE') || (stor.contenant === 'CYLINDRE_REVOLUTION') || (stor.contenant === 'CONE') || (stor.contenant === 'CONE_REVOLUTION') || (stor.contenant === 'PYRAMIDE')) {
          stor.lanatureUnd = 'une longueur'
          stor.lanatureDef = 'La longueur'
          stor.lanature = 'la hauteur'
        }
        break
      case 'mediane' :
        stor.lanatureUnd = 'une droite'
        stor.lanatureDef = 'La droite'
        stor.lanature = 'une médiane'
        stor.Natureprecise = 'La médiane violette'
        break
      case 'base' :
        stor.Natureprecise = 'La base violette'
        stor.lanatureUnd = 'une face'
        stor.lanatureDef = 'La face'
        stor.lanature = 'une base'
        if (stor.contenant === 'TRIANGLE_ISOCELE') {
          stor.lanatureUnd = 'un segment'
          stor.lanatureDef = 'Le segment'
          stor.lanature = 'la base'
        }
        if ((stor.contenant === 'PYRAMIDE') || (stor.contenant === 'CONE') || (stor.contenant === 'CONE_REVOLUTION')) {
          stor.NeedEclair = true
          stor.Natureprecise = 'La base'
          stor.lanature = 'la base'
        }
        break
      case 'le_rayon' :
        stor.Natureprecise = 'Le rayon donné'
        stor.lanatureUnd = 'une longueur'
        stor.lanatureDef = 'La longueur'
        stor.lanature = 'le rayon'
        break
      case 'le_diametre' :
        stor.Natureprecise = 'Le diamètre'
        stor.lanatureUnd = 'une longueur'
        stor.lanatureDef = 'La longueur'
        stor.lanature = 'le diamètre'
        break
      case 'parallele' :
        stor.Natureprecise = 'Le parallèle'
        stor.lanatureUnd = 'un parallèle'
        stor.lanatureDef = 'le parallèle'
        stor.lanature = 'un parallèle'
        break
      case 'grand_cercle' :
        stor.Natureprecise = 'Le grand cercle'
        stor.lanatureUnd = 'un grand cercle'
        stor.lanatureDef = 'le grand cercle'
        stor.lanature = 'un grand cercle'
        break
      case 'meridien' :
        stor.Natureprecise = 'Le méridien'
        stor.lanatureUnd = 'un méridien'
        stor.lanatureDef = 'le méridien'
        stor.lanature = 'un méridien'
        break
      case 'equateur' :
        stor.Natureprecise = 'L’équateur'
        stor.lanatureUnd = 'l’équateur'
        stor.lanatureDef = 'l’équateur'
        stor.lanature = 'l’équateur'
        break
    }

    stor.fig = new FigNomme03(me)

    stor.lesdiv.figure.classList.add('enonce')
    stor.MonNeed = needelem
    if (stor.Nature) {
      switch (stor.elemType) {
        case 'segment' :
          stor.TypeDeb = 'le segment violet'
          break
        case 'point' :
          stor.TypeDeb = 'le point violet'
          break
        case 'face' :
          stor.TypeDeb = 'la surface violette'
          break
        case 'droite' :
          stor.TypeDeb = 'la droite violette'
          break
        case 'cercle' :
          stor.TypeDeb = 'le cercle violet'
          break
        case 'demi-droite' :
          stor.TypeDeb = 'la demi-droite violette'
          break
        case 'angle' :
          stor.TypeDeb = 'l’élément violet'
          break
        case 'mesure' :
          stor.TypeDeb = 'la mesure violette'
          break
        case 'demi-cercle' :
          stor.TypeDeb = 'le demi-cercle violet'
          break
      }

      stor.listchoix = []
      switch (stor.element) {
        case 'arete' :
          stor.listchoix = [...textes.Listearete]
          stor.COlistchoix = [...textes.COarete]
          break
        case 'arete_cachee' :
          stor.listchoix = [...textes.Listearete_cachee]
          stor.COlistchoix = [...textes.COarete_cachee]
          break
        case 'arete_visible' :
          stor.listchoix = [...textes.Listearete_cachee2]
          stor.COlistchoix = [...textes.COarete_cachee2]
          break
        case 'arete_laterale' :
          stor.listchoix = [...textes.Listearete_laterale]
          stor.COlistchoix = [...textes.COarete_laterale]
          break
        case 'sommet' :
          if (stor.contenant !== 'ANGLE') {
            stor.listchoix = [...textes.Listesommet]
            stor.COlistchoix = [...textes.COsommet]
          } else {
            stor.listchoix = [...textes.ListesommetANGLE]
            stor.COlistchoix = [...textes.COsommetANGLE]
          }
          break
        case 'sommet_principal' :
          if (stor.contenant === 'TRIANGLE_ISOCELE') {
            stor.listchoix = [...textes.Listesommet_principal]
            stor.COlistchoix = [...textes.Cosommet_principal]
          } else {
            stor.listchoix = [...textes.Listele_sommet]
            stor.COlistchoix = [...textes.COle_sommet]
          }
          break
        case 'sommet_oppose' :
          stor.listchoix = [...textes.Listesommet_oppose]
          stor.COlistchoix = [...textes.COsommet_oppose]
          break
        case 'sommet_consecutif' :
          stor.listchoix = [...textes.Listesommet_consecutif]
          stor.COlistchoix = [...textes.COsommet_consecutif]

          break
        case 'sommet_angle_droit' :
          stor.listchoix = [...textes.Listesommet_angle_droit]
          stor.COlistchoix = [...textes.COsommet_angle_droit]
          break
        case 'face' :
          stor.listchoix = [...textes.Listeface]
          stor.COlistchoix = [...textes.COface]
          break
        case 'face_laterale' :
          if ((stor.contenant === 'CYLINDRE') || (stor.contenant === 'CYLINDRE_REVOLUTION') || (stor.contenant === 'CONE_REVOLUTION') || (stor.contenant === 'CONE')) {
            stor.listchoix = [...textes.Listeface_laterale2]
            stor.COlistchoix = [...textes.COface_laterale2]
          } else {
            stor.listchoix = [...textes.Listeface_laterale]
            stor.COlistchoix = [...textes.COface_laterale]
          }
          break
        case 'face_cachee' :
          stor.listchoix = [...textes.Listeface_cachee]
          stor.COlistchoix = [...textes.COface_cachee]
          break
        case 'face_visible' :
          stor.listchoix = [...textes.Listeface_cachee2]
          stor.COlistchoix = [...textes.COface_cachee2]
          break
        case 'cote_consecutif' :
          stor.listchoix = [...textes.Listecote_consecutif]
          stor.COlistchoix = [...textes.COcote_consecutif]
          break
        case 'cote' :
          stor.listchoix = [...textes.Listecote]
          stor.COlistchoix = [...textes.COcote]
          break
        case 'cote_oppose' :
          stor.listchoix = [...textes.Listecote_oppose]
          stor.COlistchoix = [...textes.COcote_oppose]
          break
        case 'cote_adjacent' :
          stor.listchoix = [...textes.Listecote_adjacent]
          stor.COlistchoix = [...textes.COcote_adjacent]
          break
        case 'cote_oppose_angle' :
          stor.listchoix = [...textes.Listecote_oppose_angle]
          stor.COlistchoix = [...textes.COcote_oppose_angle]
          break
        case 'rayon' :
          stor.listchoix = [...textes.Listerayon]
          stor.COlistchoix = [...textes.COrayon]
          break
        case 'diametre' :
          stor.listchoix = [...textes.Listediametre]
          stor.COlistchoix = [...textes.COdiametre]
          break
        case 'corde' :
          stor.listchoix = [...textes.Listecorde]
          stor.COlistchoix = [...textes.COcorde]
          break
        case 'hypotenuse' :
          stor.listchoix = [...textes.Listehypotenuse]
          stor.COlistchoix = [...textes.COhypotenuse]
          break
        case 'diagonale' :
          stor.listchoix = [...textes.Listediagonale]
          stor.COlistchoix = [...textes.COdiagonale]
          break
        case 'generatrice' :
          stor.listchoix = [...textes.Listegeneratrice]
          stor.COlistchoix = [...textes.COgeneratrice]
          break
        case 'angle' :
          stor.listchoix = [...textes.Listeangle]
          stor.COlistchoix = [...textes.COangle]
          if (stor.contenant === 'SECTEUR') {
            stor.listchoix = [...textes.ListeangleSECTEUR]
            stor.COlistchoix = [...textes.COangleSECTEUR]
          }
          break
        case 'angle_base' :
          stor.listchoix = [...textes.Listeangle_base]
          stor.COlistchoix = [...textes.COangle_base]
          break
        case 'angle_oppose' :
          stor.listchoix = [...textes.Listeangle_oppose]
          stor.COlistchoix = [...textes.COangle_oppose]
          break
        case 'angle_consecutif' :
          stor.listchoix = [...textes.Listeangle_consecutif]
          stor.COlistchoix = [...textes.COangle_consecutif]
          break
        case 'meridien' :
          stor.listchoix = [...textes.Listemeridien]
          stor.COlistchoix = [...textes.COmeridien]
          break
        case 'equateur' :
          stor.listchoix = [...textes.Listeequateur]
          stor.COlistchoix = [...textes.COequateur]
          break
        case 'grand_cercle' :
          stor.listchoix = [...textes.Listegrand_cercle]
          stor.COlistchoix = [...textes.COgrand_cercle]
          break
        case 'parallele' :
          stor.listchoix = [...textes.Listeparallele]
          stor.COlistchoix = [...textes.COparallele]
          break
        case 'pole' :
          stor.listchoix = [...textes.Listepole]
          stor.COlistchoix = [...textes.COpole]
          break
        case 'origine' :
          stor.listchoix = [...textes.Listeorigine]
          stor.COlistchoix = [...textes.COorigine]
          break
        case 'milieu' :
          stor.listchoix = [...textes.Listemilieu]
          stor.COlistchoix = [...textes.COmilieu]
          break
        case 'extremites' :
          stor.listchoix = [...textes.Listeextremites]
          stor.COlistchoix = [...textes.COextremites]
          break
        case 'centre' :
          stor.listchoix = [...textes.Listecentre]
          stor.COlistchoix = [...textes.COcentre]
          if (stor.contenant === 'SECTEUR') {
            stor.listchoix = [...textes.ListecentreSECTEUR]
            stor.COlistchoix = [...textes.COcentreSECTEUR]
          }
          break
        case 'bissectrice' :
          if (stor.contenant === 'ANGLE') {
            stor.listchoix = [...textes.Listebissectrice_angle]
            stor.COlistchoix = [...textes.CObissectrice_angle]
          } else {
            stor.listchoix = [...textes.Listebissectrice_triangle]
            stor.COlistchoix = [...textes.CObissectrice_triangle]
          }

          break
        case 'mediatrice' :
          if (stor.contenant === 'SEGMENT') {
            stor.listchoix = [...textes.Listemediatrice_segment]
            stor.COlistchoix = [...textes.COmediatrice_segment]
          } else {
            stor.listchoix = [...textes.Listemediatrice_triangle]
            stor.COlistchoix = [...textes.COmediatrice_triangle]
          }
          break
        case 'hauteur' :
          if (stor.contenant === 'PARALLELOGRAMME') {
            stor.listchoix = [...textes.Listehauteur_parallelogramme]
            stor.COlistchoix = [...textes.COhauteur_parallelogramme]
          } else if (stor.contenant.indexOf('TRIANGLE') !== -1) {
            stor.listchoix = [...textes.Listehauteur_triangle]
            stor.COlistchoix = [...textes.COhauteur_triangle]
          } else {
            stor.listchoix = [...textes.Listehauteur_solide]
            stor.COlistchoix = [...textes.COhauteur_solide]
          }
          break
        case 'mediane' :
          stor.listchoix = [...textes.Listemediane]
          stor.COlistchoix = [...textes.COmediane]
          break
        case 'base' :
          if (stor.contenant.indexOf('TRIANGLE') !== -1) {
            stor.listchoix = [...textes.Listebase_isocele]
            stor.COlistchoix = [...textes.CObase_isocele]
          } else if ((stor.contenant.indexOf('CYLINDRE') !== -1) || (stor.contenant === 'PRISME')) {
            stor.listchoix = [...textes.Listebase_prisme_cyl]
            stor.COlistchoix = [...textes.CObase_prisme_cyl]
          } else {
            stor.listchoix = [...textes.Listebase_solide_pointe]
            stor.COlistchoix = [...textes.CObase_solide_pointe]
          }
          break
        case 'longueur' :
          stor.listchoix = [...textes.Listelongueur]
          stor.COlistchoix = [...textes.COlongueur]
          break
        case 'le_rayon' :
          stor.listchoix = [...textes.Listele_rayon]
          stor.COlistchoix = [...textes.COle_rayon]
          break
        case 'le_diametre' :
          stor.listchoix = [...textes.Listele_diametre]
          stor.COlistchoix = [...textes.COle_diametre]
          break
      }

      let maquest, maquestFin
      const listchoix2 = j3pShuffle(stor.listchoix)
      listchoix2.splice(0, 0, textes.Choisir)
      if (stor.Nomme) {
        stor.TabTra = addDefaultTable(stor.lesdiv.travail, 3, 1)
        modif(stor.TabTra)
        stor.tabTraPour = addDefaultTable(stor.TabTra[0][0], 1, 3)
        j3pAffiche(stor.tabTraPour[0][2], null, ',')
        modif(stor.tabTraPour)
        stor.tabTraNature = addDefaultTable(stor.TabTra[1][0], 1, 8)
        stor.cocoNat = stor.tabTraNature[0][7]
        j3pAffiche(stor.tabTraNature[0][0], null, stor.TypeDeb)
        stor.tabTraNature[0][0].style.color = '#FF00C0'
        j3pAffiche(stor.tabTraNature[0][2], null, '&nbsp;est&nbsp;')
        modif(stor.tabTraNature)
        stor.CoNature = addDefaultTable(stor.TabTra[1][0], 1, 1)[0][0]
        stor.CoNature.style.padding = 0
        stor.tabTraNom = addDefaultTable(stor.TabTra[2][0], 1, 2)
        j3pAffiche(stor.tabTraNom[0][0], null, 'Son nom est&nbsp;')
        stor.CoNom = addDefaultTable(stor.TabTra[2][0], 1, 1)[0][0]
        stor.CoNom.style.padding = 0
        stor.pourNom = stor.tabTraNom[0][1]
        modif(stor.tabTraNom)
        maquest = textes.NatureDeb.replace('£b', textes.ContDef[contenantnum])
        if (stor.NomContenant.chapo === 1) {
          j3pAffiche(stor.tabTraPour[0][1], null, '$\\widehat{' + stor.NomContenant.texte + '}$')
          stor.tabTraPour[0][0].style.verticalAlign = 'bottom'
        } else if (stor.NomContenant.chapo === 2) {
          const yy = j3pAffiche(stor.tabTraPour[0][1], null, stor.NomContenant.texte)
          j3pStyle(yy.parent, {
            background: 'url("' + j3pBaseUrl + 'sections/college/sixieme/Elementgeo/images/arc.png") no-repeat ',
            backgroundSize: '100% 30% '
          })
        } else {
          j3pAffiche(stor.tabTraPour[0][1], null, stor.NomContenant.texte)
        }
        j3pAffiche(stor.tabTraPour[0][0], null, maquest + '&nbsp;')
        if (stor.NomNeedElem.length > 0) {
          maquestFin = stor.NomNeedElem[0].texte
          if (stor.NomNeedElem[0].chapo === 1) {
            j3pAffiche(stor.tabTraNature[0][5], null, '&nbsp;$\\widehat{' + maquestFin + '}$')
          } else if (stor.NomNeedElem[0].chapo === 2) {
            const uu = j3pAffiche(stor.tabTraNature[0][5], null, '&nbsp;' + maquestFin)
            j3pStyle(uu.parent, {
              background: 'url("' + j3pBaseUrl + 'sections/college/sixieme/Elementgeo/images/arc.png") no-repeat ',
              backgroundSize: '100% 30% '
            })
          } else {
            j3pAffiche(stor.tabTraNature[0][5], null, '&nbsp;' + maquestFin)
          }
        }
        maquestFin = needelem
        j3pAffiche(stor.tabTraNature[0][4], null, '&nbsp;' + maquestFin)

        maquestFin = '.'
        j3pAffiche(stor.tabTraNature[0][6], null, maquestFin)
        stor.Zonelist = ListeDeroulante.create(stor.tabTraNature[0][3], listchoix2, {
          centre: true
        })

        creeZoneNom()
      } else {
        switch (stor.elemType) {
          case 'segment' :
            stor.TypeDeb = 'le segment '
            break
          case 'point' :
            stor.TypeDeb = 'le point '
            break
          case 'face' :
            stor.TypeDeb = 'la surface '
            break
          case 'droite' :
            stor.TypeDeb = 'la droite '
            break
          case 'cercle' :
            stor.TypeDeb = 'le cercle '
            break
          case 'demi-droite' :
            stor.TypeDeb = 'la demi-droite '
            break
          case 'angle' :
            stor.TypeDeb = 'l’élément '
            break
          case 'mesure' :
            stor.TypeDeb = 'la mesure'
            break
          case 'demi-cercle' :
            stor.TypeDeb = 'le demi-cercle '
            break
        }

        stor.TabTra = addDefaultTable(stor.lesdiv.travail, 3, 1)
        modif(stor.TabTra)
        stor.tabTraPour = addDefaultTable(stor.TabTra[0][0], 1, 3)
        j3pAffiche(stor.tabTraPour[0][2], null, ',')
        modif(stor.tabTraPour)
        stor.tabTraNature = addDefaultTable(stor.TabTra[1][0], 1, 9)
        stor.cocoNat = stor.tabTraNature[0][8]
        j3pAffiche(stor.tabTraNature[0][0], null, stor.TypeDeb + '&nbsp;')
        j3pAffiche(stor.tabTraNature[0][2], null, '&nbsp;est&nbsp;')
        modif(stor.tabTraNature)
        stor.CoNature = addDefaultTable(stor.TabTra[1][0], 1, 1)[0][0]
        stor.CoNature.style.padding = 0

        maquest = textes.NatureDeb.replace('£b', textes.ContDef[contenantnum])
        if (stor.NomContenant.chapo === 1) {
          j3pAffiche(stor.tabTraPour[0][1], null, '$\\widehat{' + stor.NomContenant.texte + '}$')
        } else if (stor.NomContenant.chapo === 2) {
          const yy = j3pAffiche(stor.tabTraPour[0][1], null, stor.NomContenant.texte)
          j3pStyle(yy.parent, {
            background: 'url("' + j3pBaseUrl + 'sections/college/sixieme/Elementgeo/images/arc.png") no-repeat ',
            backgroundSize: '100% 30% '
          })
        } else {
          j3pAffiche(stor.tabTraPour[0][1], null, stor.NomContenant.texte)
        }
        j3pAffiche(stor.tabTraPour[0][0], null, maquest)
        if (stor.NomNeedElem.length > 0) {
          maquestFin = needelem
          j3pAffiche(stor.tabTraNature[0][4], null, maquestFin)
          if (stor.NomNeedElem[0].chapo === 1) {
            j3pAffiche(stor.tabTraNature[0][5], null, '$\\widehat{' + stor.NomNeedElem[0].texte + '}$')
          } else if (stor.NomNeedElem[0].chapo === 2) {
            const kl = j3pAffiche(stor.tabTraNature[0][5], null, stor.NomNeedElem[0].texte)
            j3pStyle(kl.parent, {
              background: 'url("' + j3pBaseUrl + 'sections/college/sixieme/Elementgeo/images/arc.png") no-repeat ',
              backgroundSize: '100% 30% '
            })
          } else {
            j3pAffiche(stor.tabTraNature[0][5], null, stor.NomNeedElem[0].texte)
          }
        }
        if (stor.NomElem.length > 0) {
          maquestFin = stor.NomElem[0].texte
          if (stor.NomElem[0].chapo === 1) {
            j3pAffiche(stor.tabTraNature[0][1], null, '$\\widehat{' + maquestFin + '}$')
          } else if (stor.NomElem[0].chapo === 2) {
            const yy = j3pAffiche(stor.tabTraNature[0][1], null, maquestFin)
            j3pStyle(yy.parent, {
              background: 'url("' + j3pBaseUrl + 'sections/college/sixieme/Elementgeo/images/arc.png") no-repeat ',
              backgroundSize: '100% 30% '
            })
          } else {
            j3pAffiche(stor.tabTraNature[0][1], null, maquestFin)
          }
        }
        maquestFin = '.'
        j3pAffiche(stor.tabTraNature[0][6], null, maquestFin)
        stor.Zonelist = ListeDeroulante.create(stor.tabTraNature[0][3], listchoix2, {
          centre: true
        })
      }
    } else {
      switch (stor.element) {
        case 'segment' :
          stor.TypeDeb = 'le segment '
          break
        case 'point' :
          stor.TypeDeb = 'le point '
          break
        case 'face' :
          stor.TypeDeb = 'la surface '
          break
        case 'droite' :
          stor.TypeDeb = 'la droite '
          break
        case 'cercle' :
          stor.TypeDeb = 'le cercle '
          break
        case 'demi-droite' :
          stor.TypeDeb = 'la demi-droite '
          break
        case 'angle' :
          stor.TypeDeb = 'l’angle '
          break
        case 'longueur' :
          stor.TypeDeb = 'la '
          break
        case 'demi-cercle' :
          stor.TypeDeb = 'le demi-cercle '
          break
      }
      stor.TabTra = addDefaultTable(stor.lesdiv.travail, 3, 1)
      modif(stor.TabTra)
      stor.tabTraPour = addDefaultTable(stor.TabTra[0][0], 1, 3)
      j3pAffiche(stor.tabTraPour[0][2], null, ',')
      modif(stor.tabTraPour)
      stor.tabTraNom = addDefaultTable(stor.TabTra[2][0], 1, 4)
      j3pAffiche(stor.tabTraNom[0][2], null, '&nbsp;se nomme&nbsp;')
      stor.CoNom = addDefaultTable(stor.TabTra[2][0], 1, 1)[0][0]
      stor.CoNom.style.padding = 0
      stor.pourNom = stor.tabTraNom[0][3]
      modif(stor.tabTraNom)

      const maquest = textes.NatureDeb.replace('£b', textes.ContDef[contenantnum])
      if (stor.NomContenant.chapo === 1) {
        j3pAffiche(stor.tabTraPour[0][1], null, '$\\widehat{' + stor.NomContenant.texte + '}$')
      } else if (stor.NomContenant.chapo === 2) {
        const yy = j3pAffiche(stor.tabTraPour[0][1], null, stor.NomContenant.texte)
        j3pStyle(yy.parent, {
          background: 'url("' + j3pBaseUrl + 'sections/college/sixieme/Elementgeo/images/arc.png") no-repeat ',
          backgroundSize: '100% 30% '
        })
      } else {
        j3pAffiche(stor.tabTraPour[0][1], null, stor.NomContenant.texte)
      }
      j3pAffiche(stor.tabTraPour[0][0], null, maquest + '&nbsp;')

      let maquestFin = stor.Natureprecise
      j3pAffiche(stor.tabTraNom[0][0], null, maquestFin)
      if (stor.NomNeedElem.length > 0) {
        maquestFin = stor.NomNeedElem[0].texte
        if (stor.NomNeedElem[0].chapo === 1) {
          j3pAffiche(stor.tabTraNom[0][1], null, '$\\widehat{' + maquestFin + '}$')
        } else if (stor.NomNeedElem[0].chapo === 2) {
          const uu = j3pAffiche(stor.tabTraNom[0][1], null, maquestFin)
          j3pStyle(uu.parent, {
            background: 'url("' + j3pBaseUrl + 'sections/college/sixieme/Elementgeo/images/arc.png") no-repeat ',
            backgroundSize: '100% 30% '
          })
        } else {
          j3pAffiche(stor.tabTraNom[0][1], null, maquestFin)
        }
      }

      creeZoneNom()
    }

    if (stor.Nomme) {
      stor.TabTra[2][0].classList.add('travail')
    }
    if (stor.Nature) {
      stor.TabTra[1][0].classList.add('travail')
    }
    stor.TabTra[0][0].classList.add('enonce')

    if (stor.Nature) {
      stor.CoNature.style.color = me.styles.cfaux
    }
    if (stor.Nomme) {
      stor.CoNom.style.color = me.styles.cfaux
    }

    stor.lesdiv.solution = j3pAddElt(stor.lesdiv.conteneur, 'div')
    stor.lesdiv.solution.style.color = me.styles.petit.correction.color
    // Obligatoire
    me.finEnonce()
  }

  function afficheCorrectionFaux () {
    let yaaideNom = false
    let yaaideNature = false
    if (stor.Nature) {
      stor.Zonelist.corrige(!stor.ErreurNat)
      if (stor.ErreurNat) { stor.Zonelist.barre() }
      j3pAffiche(stor.cocoNat, null, stor.listchoix[0])
      stor.cocoNat.style.color = me.styles.petit.correction.color
    }
    if (stor.Nomme) {
      stor.zonenom.corrige((!stor.ErreurNom) && (!stor.ErreurChap))
      if (stor.ErreurNom || stor.ErreurChap) { stor.zonenom.barre() }
    }
    if (stor.Nature) {
      stor.Zonelist.disable()
    }
    if (stor.Nomme) {
      stor.zonenom.disable()
    }
    if (stor.AideNat !== '') {
      stor.CoNature.innerHTML += stor.AideNat
      yaaideNature = true
    }
    for (let i = 0; i < stor.AideNom.length; i++) {
      stor.CoNom.innerHTML += stor.AideNom[i]
      yaaideNom = true
    }

    if (stor.Nomme) {
      const tab = addDefaultTable(stor.lesdiv.solution, 1, 5)
      modif(tab)
      j3pAffiche(tab[0][0], null, stor.Natureprecise + '&nbsp;')
      j3pAffiche(tab[0][2], null, '&nbsp;se nomme&nbsp;')
      j3pAffiche(tab[0][4], null, '.')
      if (stor.NomNeedElem.length > 0) {
        if (stor.NomNeedElem[0].chapo === 1) {
          j3pAffiche(tab[0][1], null, '$\\widehat{' + stor.NomNeedElem[0].texte + '}$')
        } else if (stor.NomNeedElem[0].chapo === 2) {
          const yy = j3pAffiche(tab[0][1], null, stor.NomNeedElem[0].texte)
          j3pStyle(yy.parent, {
            background: 'url("' + j3pBaseUrl + 'sections/college/sixieme/Elementgeo/images/arc3.png") no-repeat ',
            backgroundSize: '100% 100% '
          })
        } else {
          j3pAffiche(tab[0][1], null, stor.NomNeedElem[0].texte)
        }
      }
      if (stor.NomElem[0].chapo === 1) {
        j3pAffiche(tab[0][3], null, '$\\widehat{' + stor.NomElem[0].texte + '}$')
      } else if (stor.NomElem[0].chapo === 2) {
        const yy = j3pAffiche(tab[0][3], null, stor.NomElem[0].texte)
        j3pStyle(yy.parent, {
          background: 'url("' + j3pBaseUrl + 'sections/college/sixieme/Elementgeo/images/arc3.png") no-repeat ',
          backgroundSize: '100% 100% '
        })
      } else {
        j3pAffiche(tab[0][3], null, stor.NomElem[0].texte)
      }
    } else {
      const tab = addDefaultTable(stor.lesdiv.solution, 1, 7)
      modif(tab)

      j3pAffiche(tab[0][2], null, '&nbsp;est&nbsp;')
      j3pAffiche(tab[0][6], null, '.')
      j3pAffiche(tab[0][0], null, stor.TypeDeb)
      if (stor.NomElem[0].chapo === 1) {
        j3pAffiche(tab[0][1], null, '$\\widehat{' + stor.NomElem[0].texte + '}$')
      } else if (stor.NomElem[0].chapo === 2) {
        const yy = j3pAffiche(tab[0][1], null, stor.NomElem[0].texte)
        j3pStyle(yy.parent, {
          background: 'url("' + j3pBaseUrl + 'sections/college/sixieme/Elementgeo/images/arc3.png") no-repeat ',
          backgroundSize: '100% 100% '
        })
      } else {
        j3pAffiche(tab[0][1], null, '&nbsp;' + stor.NomElem[0].texte)
      }
      j3pAffiche(tab[0][3], null, stor.lanature)
      j3pAffiche(tab[0][4], null, stor.MonNeed)

      if (stor.NomNeedElem.length > 0) {
        if (stor.NomNeedElem[0].chapo === 1) {
          j3pAffiche(tab[0][2], null, '$\\widehat{' + stor.NomNeedElem[0].texte + '}$')
        } else if (stor.NomNeedElem[0].chapo === 2) {
          const yy = j3pAffiche(tab[0][2], null, stor.NomNeedElem[0].texte)
          j3pStyle(yy.parent, {
            background: 'url("' + j3pBaseUrl + 'sections/college/sixieme/Elementgeo/images/arc3.png") no-repeat ',
            backgroundSize: '100% 100% '
          })
        } else {
          j3pAffiche(tab[0][2], null, stor.NomNeedElem[0].texte)
        }
      }
    }
    stor.lesdiv.solution.classList.add('correction')
    if (yaaideNom) {
      stor.CoNom.classList.add('explique')
    }
    if (yaaideNature) {
      stor.CoNature.classList.add('explique')
    }
  } // afficheCorrectionFaux

  switch (this.etat) {
    case 'enonce':
      if (this.debutDeLaSection) {
        initSection()
      } else {
        enonceMain()
      }
      break // case "enonce":

    case 'correction': {
      // On teste si une réponse a été saisie
      if (!yareponse()) {
        // ici on donne le message à afficher si une ou plusieurs zone(s) n’est pas complétée (tout d’abord on met en rouge : styles.cfaux)
        stor.lesdiv.correction.style.color = this.styles.cfaux
        stor.lesdiv.correction.innerHTML = reponseIncomplete
        return this.finCorrection()
      }
      // Une réponse a été saisie
      if (bonneReponse()) {
        // Bonne réponse
        this.score += j3pArrondi(1 / this.donneesSection.nbetapes, 1)
        stor.lesdiv.correction.style.color = this.styles.cbien
        stor.lesdiv.correction.innerHTML = cBien
        if (stor.Nature) {
          stor.Zonelist.corrige(true)
        }
        if (stor.Nomme) {
          stor.zonenom.corrige(true)
        }
        if (stor.Nature) {
          stor.Zonelist.disable()
        }
        if (stor.Nomme) {
          stor.zonenom.disable()
        }

        this.typederreurs[0]++
        return this.finCorrection('navigation', true)
      }

      // Pas de bonne réponse
      stor.lesdiv.correction.style.color = this.styles.cfaux

      // A cause de la limite de temps :
      if (this.isElapsed) { // limite de temps
        afficheCorrectionFaux()
        return this.finCorrection('navigation', true)
      }

      // Réponse fausse :
      stor.lesdiv.correction.innerHTML = cFaux
      if (this.essaiCourant < this.donneesSection.nbchances) {
        stor.lesdiv.correction.innerHTML += '<br>' + essaieEncore
        this.typederreurs[1]++
        // indication éventuelle ici
        return this.finCorrection() // on reste en correction
      }

      // Erreur au dernier essai
      stor.lesdiv.correction.innerHTML += '<br>' + regardeCorrection
      afficheCorrectionFaux()
      this.typederreurs[2]++
      this.finCorrection('navigation', true)
      break // case "correction":
    }

    case 'navigation':
      if (this.sectionTerminee()) {
        // On détermine la phrase d’état renvoyée par la section
        const sco = this.score / this.donneesSection.nbitems
        let peIndex
        if (sco >= 0.8) {
          peIndex = 0
        } else if (sco >= 0.55) {
          peIndex = 1
        } else if (sco >= 0.3) {
          peIndex = 3
        } else {
          peIndex = 5
        }
        let compt = this.typederreurs[2] / 4
        if (this.typederreurs[3] > compt) {
          peIndex++
          compt = this.typederreurs[3]
        }
        this.parcours.pe = peList[peIndex]
      } else {
        this.etat = 'enonce'
      }
      this.finNavigation()
      break // case "navigation":
  }
}
