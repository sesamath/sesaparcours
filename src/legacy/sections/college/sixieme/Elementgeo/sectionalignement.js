import { j3pAddElt, j3pAjouteBouton, j3pArrondi, j3pEmpty, j3pGetNewId, j3pGetRandomInt, j3pShowError } from 'src/legacy/core/functions'
import { j3pCreeSVG } from 'src/legacy/core/functionsSvg'
import ZoneStyleMathquill1 from 'src/legacy/outils/zoneStyleMathquill/ZoneStyleMathquill1'
import { addDefaultTable } from 'src/legacy/themes/table'
import textesGeneriques from 'src/lib/core/textes'
import { j3pAffiche } from 'src/lib/mathquill/functions'
import { getMtgCore } from 'src/lib/outils/mathgraph'
import { addMaj } from 'src/lib/utils/string'

const { cBien, cFaux, essaieEncore, regardeCorrection, reponseIncomplete } = textesGeneriques

/**
 * Ajouter une description ici
 * @fileOverview
 * @author Tommy Barroy <t.barroy@laposte.net>
 * @since 2017-10
 */
export const params = {
  outils: ['calculatrice', 'mathquill'],
  /**
   * Ce qui est paramétrable dans l’éditeur de graphe
   * @type {LegacySectionParamsParametres}
   */
  parametres: [
    ['nbrepetitions', 9, 'entier', 'Nombre de répétitions de la section'],
    ['indication', '', 'string', 'Indication pour chaque répétition'],
    ['limite', 0, 'entier', 'Temps disponible par répétition, en secondes'],
    ['Aide', true, 'boolean', '<u>true</u>: Un bouton permet à l’élève de visualiser clairement l’élèment de l’énoncé <BR><BR><u>false</u>: Non.'],
    // le modèle gère la compatibilité ascendante avec l’ancien paramètre MiseEnPagePlusClaire
    ['theme', 'standard', 'liste', 'Modèle pour l’affichage', ['standard', 'zonesAvecImageDeFond']]

  ]

}

/**
 * section alignement
 * @this {Parcours}
 */
export default function main () {
  const me = this
  const stor = me.storage
  let ds = me.donneesSection
  let svgId = stor.svgId

  function AfficheFig () {
    const nomspris = []
    const bob = [addMaj(nomspris), addMaj(nomspris), addMaj(nomspris), addMaj(nomspris)]
    svgId = j3pGetNewId('mtg32')
    stor.svgId = svgId
    const txtfigure = 'TWF0aEdyYXBoSmF2YTEuMAAAABM+TMzNAAJmcv###wEA#wEAAAAAAAAAAANLAAACfwAAAQEAAAAAAAAAAQAAAJP#####AAAAAQAKQ0NhbGNDb25zdAD#####AAJwaQAWMy4xNDE1OTI2NTM1ODk3OTMyMzg0Nv####8AAAABAApDQ29uc3RhbnRlQAkh+1RELRj#####AAAAAQAKQ1BvaW50QmFzZQD#####AQAAAAAOAAFVAMAkAAAAAAAAQBAAAAAAAAAAAAUAAEAv#nbItDlYQC#+dsi0OVj#####AAAAAQAUQ0Ryb2l0ZURpcmVjdGlvbkZpeGUA#####wEAAAAAEAAAAQAAAAEAAAABAT#wAAAAAAAA#####wAAAAEAD0NQb2ludExpZURyb2l0ZQD#####AQAAAAAOAAFWAMAAAAAAAAAAQBAAAAAAAAAAAAUAAUA##nbItDlYAAAAAv####8AAAABAAhDU2VnbWVudAD#####AQAAAAAQAAABAAAAAQAAAAEAAAAD#####wAAAAEAB0NNaWxpZXUA#####wEAAAAAEAAAAAAAAAAAAAAAQAgAAAAAAAAAAAUAAAAAAQAAAAP#####AAAAAgAMQ0NvbW1lbnRhaXJlAP####8BAAAAAAAAAAAAAAAAQBgAAAAAAAAAAAAAAAUMAAAAAAABAAAAAAAAAAEAAAAAAAAAAAABMf####8AAAABAAlDTG9uZ3VldXIA#####wAAAAEAAAADAAAAAgD#####AQAAAAAQAAAAAAAAAAAAAABACAAAAAAAAAAABQABQGHgAAAAAABAYlwo9cKPXAAAAAIA#####wEAAAAAEAAAAAAAAAAAAAAAQAgAAAAAAAAAAAUAAUBWwAAAAAAAQENwo9cKPXH#####AAAAAQAJQ0NlcmNsZU9BAP####8BAAAAAAAAAQAAAAgAAAAJAAAAAgD#####AAAAAAAQAAAAQAgAAAAAAAAAAAAAAAAAAAAABQAAQFMAAAAAAABAfd4UeuFHrv####8AAAABAAdDQ2FsY3VsAP####8ABW1pbmkxAAEwAAAAAQAAAAAAAAAAAAAACgD#####AAVtYXhpMQADMzYwAAAAAUB2gAAAAAAA#####wAAAAEAFENJbXBsZW1lbnRhdGlvblByb3RvAP####8AB0N1cnNldXIAAAAFAAAABQAAAAMAAAAMAAAADQAAAAsAAAADAAAAAA4BAAAAABAAAAEAAAABAAAACwE#8AAAAAAAAAAAAAQBAAAADgAAAAAAEAAAAMAIAAAAAAAAP#AAAAAAAAAAAAUAAEBdQAAAAAAAAAAAD#####8AAAABAAtDSG9tb3RoZXRpZQAAAAAOAAAAC#####8AAAABAApDT3BlcmF0aW9uA#####8AAAABAA9DUmVzdWx0YXRWYWxldXIAAAAMAAAADQEAAAAOAAAADAAAAA4AAAAN#####wAAAAEAC0NQb2ludEltYWdlAAAAAA4BAAAAAA0AAk8xAMAQAAAAAAAAQBAAAAAAAAAAAAUAAAAAEAAAABEAAAAMAAAAAA4AAAALAAAADQMAAAANAQAAAAE#8AAAAAAAAAAAAA4AAAAMAAAADQEAAAAOAAAADQAAAA4AAAAMAAAADwAAAAAOAQAAAAANAAJJMQDAAAAAAAAAAEAIAAAAAAAAAAAFAAAAABAAAAATAAAABQEAAAAOAAAAAAAQAAABAAAAAQAAAAsAAAAQAAAABAEAAAAOAAAAAAEQAAJrMQDAAAAAAAAAAEAAAAAAAAAAAAABAAE#2bWbWbWbWgAAABX#####AAAAAgAPQ01lc3VyZUFic2Npc3NlAQAAAA4AAmExAAAAEgAAABQAAAAW#####wAAAAEAD0NWYWxldXJBZmZpY2hlZQEAAAAOAAAAAAAAAAAAAAAAAMAYAAAAAAAAAAAAAAAWDwAB####AAAAAQAAAAIAAAABAAAAAAAAAAAAAAAAAgAAABcAAAAKAP####8AAkExAAJhMQAAAA4AAAAXAAAAAgD#####AAAAAAAQAAAAQAgAAAAAAAAAAAAAAAAAAAAABQAAQFKAAAAAAABAgF8KPXCj1wAAAAoA#####wAFbWluaTIAATAAAAABAAAAAAAAAAAAAAAKAP####8ABW1heGkyAAIxMAAAAAFAJAAAAAAAAAAAAAsA#####wAHQ3Vyc2V1cgAAAAUAAAAFAAAAAwAAABsAAAAcAAAAGgAAAAMAAAAAHQEAAAAAEAAAAQAAAAEAAAAaAT#wAAAAAAAAAAAABAEAAAAdAAAAAAAQAAAAwAgAAAAAAAA#8AAAAAAAAAAABQAAQF1AAAAAAAAAAAAeAAAADAAAAAAdAAAAGgAAAA0DAAAADgAAABsAAAANAQAAAA4AAAAbAAAADgAAABwAAAAPAAAAAB0BAAAAAA0AAk8xAMAQAAAAAAAAQBAAAAAAAAAAAAUAAAAAHwAAACAAAAAMAAAAAB0AAAAaAAAADQMAAAANAQAAAAE#8AAAAAAAAAAAAA4AAAAbAAAADQEAAAAOAAAAHAAAAA4AAAAbAAAADwAAAAAdAQAAAAANAAJJMQDAAAAAAAAAAEAIAAAAAAAAAAAFAAAAAB8AAAAiAAAABQEAAAAdAAAAAAAQAAABAAAAAQAAABoAAAAfAAAABAEAAAAdAAAAAAEQAAFrAMAAAAAAAAAAQAAAAAAAAAAAAAEAAT#jazazazazAAAAJAAAABABAAAAHQACYTIAAAAhAAAAIwAAACUAAAARAQAAAB0AAAAAAAAAAAAAAAAAwBgAAAAAAAAAAAAAACUPAAH###8AAAABAAAAAgAAAAEAAAAAAAAAAAAAAAACAAAAJgAAAAoA#####wACQTIAAmEyAAAADgAAACb#####AAAAAQAJQ1JvdGF0aW9uAP####8AAAAIAAAADgAAABkAAAAPAP####8BAAAAABAAAAAAAAAAAAAAAEAIAAAAAAAAAAAFAAAAAAkAAAAp#####wAAAAEAEUNTeW1ldHJpZUNlbnRyYWxlAP####8AAAAIAAAADwD#####AAAA#wAQAAAAAAAAAAAAAABACAAAAAAAAAACcEEJAAAAACoAAAArAAAABgD#####AQAAAAAQAAAAAAAAAAAAAABACAAAAAAAAAAABQAAAAAsAAAACAAAAAYA#####wEAAAAAEAAAAAAAAAAAAAAAQAgAAAAAAAAAAAUAAAAACAAAACoAAAAFAP####8BAAAAABAAAAEAAAABAAAALAAAACr#####AAAAAgAJQ0NlcmNsZU9SAP####8BAAAAAAAAAQAAAAgAAAABP+AAAAAAAAAAAAAAFAD#####AQAAAAAAAAEAAAAsAAAAAT#gAAAAAAAAAP####8AAAABABBDSW50RHJvaXRlQ2VyY2xlAP####8AAAAvAAAAMf####8AAAABABBDUG9pbnRMaWVCaXBvaW50AP####8BAAAAABAAAAAAAAAAAAAAAEAIAAAAAAAAAAAJAAEAAAAyAAAAFgD#####AAAAAAAQAAAAAAAAAAAAAABACAAAAAAAAAAACQACAAAAMgAAABUA#####wAAAC8AAAAwAAAAFgD#####AQAAAAAQAAAAAAAAAAAAAABACAAAAAAAAAAACQABAAAANQAAABYA#####wEAAAAAEAAAAAAAAAAAAAAAQAgAAAAAAAAAAAkAAgAAADUAAAAMAP####8AAAAzAAAADQMAAAAOAAAAKAAAAAFAJAAAAAAAAAAAAA8A#####wAAAP8AEAAAAAAAAAAAAAAAQAgAAAAAAAAAAnBCCQAAAAA3AAAAOAAAAAIA#####wAAAAAAEAAAAEAIAAAAAAAAAAAAAAAAAAAAAAkAAEBSAAAAAAAAQIGvCj1wo9cAAAAKAP####8ABW1pbmkzAAEwAAAAAQAAAAAAAAAAAAAACgD#####AAVtYXhpMwACMTAAAAABQCQAAAAAAAAAAAALAP####8AB0N1cnNldXIAAAAFAAAABQAAAAMAAAA7AAAAPAAAADoAAAADAAAAAD0BAAAAABAAAAEAAAABAAAAOgE#8AAAAAAAAAAAAAQBAAAAPQAAAP8AEAAAAMAIAAAAAAAAP#AAAAAAAAAAAAUAAEBdQAAAAAAAAAAAPgAAAAwAAAAAPQAAADoAAAANAwAAAA4AAAA7AAAADQEAAAAOAAAAOwAAAA4AAAA8AAAADwAAAAA9AQAAAAANAAJPMQDAEAAAAAAAAEAQAAAAAAAAAAAFAAAAAD8AAABAAAAADAAAAAA9AAAAOgAAAA0DAAAADQEAAAABP#AAAAAAAAAAAAAOAAAAOwAAAA0BAAAADgAAADwAAAAOAAAAOwAAAA8AAAAAPQEAAAAADQACSTEAwAAAAAAAAABACAAAAAAAAAAABQAAAAA#AAAAQgAAAAUBAAAAPQAAAP8AEAAAAQAAAAEAAAA6AAAAPwAAAAQBAAAAPQAAAP8BEAACazIAwAAAAAAAAABAAAAAAAAAAAAAAQABP+CvCvCvCvEAAABEAAAAEAEAAAA9AAJhMwAAAEEAAABDAAAARQAAABEBAAAAPQAAAP8AAAAAAAAAAADAGAAAAAAAAAAAAAAARQ8AAf###wAAAAEAAAACAAAAAQAAAAAAAAAAAAAAAAIAAABGAAAACgD#####AAJBMwACYTMAAAAOAAAARgAAAAwA#####wAAADYAAAANAwAAAA4AAABIAAAAAUAkAAAAAAAAAAAADwD#####AAAA#wAQAAAAAAAAAAAAAABACAAAAAAAAAACcEMJAAAAACoAAABJ#####wAAAAEAFkNEcm9pdGVQZXJwZW5kaWN1bGFpcmUA#####wEAAP8AEAAAAQAAAAEAAAAIAAAALwAAABUA#####wAAAEsAAAAKAAAAFgD#####AQAA#wAQAAAAAAAAAAAAAABACAAAAAAAAAAACQABAAAATAAAABYA#####wEAAP8AEAAAAAAAAAAAAAAAQAgAAAAAAAAAAAkAAgAAAEwAAAAVAP####8AAABLAAAAMAAAABYA#####wEAAP8AEAAAAAAAAAAAAAAAQAgAAAAAAAAAAAkAAQAAAE8AAAAWAP####8BAAD#ABAAAAAAAAAAAAAAAEAIAAAAAAAAAAAJAAIAAABP#####wAAAAEACUNEcm9pdGVBQgD#####AQAAAAAQAAABAAFkAAIAAAAsAAAASgAAABcA#####wEAAP8AEAAAAQAAAAIAAABKAAAAUgAAABcA#####wEAAP8AEAAAAQAAAAIAAAA5AAAAUgAAABcA#####wEAAP8AEAAAAQAAAAIAAAAsAAAAUgAAABQA#####wEAAP8AAAACAAAAOQAAAAE#4AAAAAAAAAAAAAAUAP####8BAAD#AAAAAgAAAEoAAAABP+AAAAAAAAAAAAAAFQD#####AAAAVQAAADEAAAAWAP####8BAAD#ABAAAAAAAAAAAAAAAEAIAAAAAAAAAAAJAAIAAABYAAAAFgD#####AQAA#wAQAAAAAAAAAAAAAABACAAAAAAAAAAACQABAAAAWAAAABUA#####wAAAFQAAABWAAAAFgD#####AQAA#wAQAAAAAAAAAAAAAABACAAAAAAAAAAACQACAAAAWwAAABYA#####wEAAP8AEAAAAAAAAAAAAAAAQAgAAAAAAAAAAAkAAQAAAFsAAAAVAP####8AAABTAAAAVwAAABYA#####wEAAP8AEAAAAAAAAAAAAAAAQAgAAAAAAAAAAAkAAQAAAF4AAAAWAP####8BAAD#ABAAAAAAAAAAAAAAAEAIAAAAAAAAAAAJAAIAAABeAAAABwD#####AAAA#wEAA25wQQAAAFoQAAAAAAABAAAAAQAAAAEAAAAAAAAAAAABQQAAAAcA#####wAAAP8BAANucEIAAABdEAAAAAAAAQAAAAEAAAABAAAAAAAAAAAAAUIAAAAHAP####8AAAD#AQADbnBDAAAAYBAAAAAAAAEAAAABAAAAAQAAAAAAAAAAAAFDAAAAAgD#####AAAAAAAQAAAAQAgAAAAAAAAAAAAAAAAAAAAACQAAQHKwAAAAAABAfj4UeuFHrgAAAAoA#####wAFbWluaTQAATAAAAABAAAAAAAAAAAAAAAKAP####8ABW1heGk0AAIxMAAAAAFAJAAAAAAAAAAAAAsA#####wAHQ3Vyc2V1cgAAAAUAAAAFAAAAAwAAAGUAAABmAAAAZAAAAAMAAAAAZwEAAAAAEAAAAQAAAAEAAABkAT#wAAAAAAAAAAAABAEAAABnAAAA#wAQAAAAwAgAAAAAAAA#8AAAAAAAAAAABQAAQF1AAAAAAAAAAABoAAAADAAAAABnAAAAZAAAAA0DAAAADgAAAGUAAAANAQAAAA4AAABlAAAADgAAAGYAAAAPAAAAAGcBAAAAAA0AAk8xAMAQAAAAAAAAQBAAAAAAAAAAAAUAAAAAaQAAAGoAAAAMAAAAAGcAAABkAAAADQMAAAANAQAAAAE#8AAAAAAAAAAAAA4AAABlAAAADQEAAAAOAAAAZgAAAA4AAABlAAAADwAAAABnAQAAAAANAAJJMQDAAAAAAAAAAEAIAAAAAAAAAAAFAAAAAGkAAABsAAAABQEAAABnAAAA#wAQAAABAAAAAgAAAGQAAABpAAAABAEAAABnAAAA#wEQAAJrMwDAAAAAAAAAAEAAAAAAAAAAAAABAAE#7ufufufufwAAAG4AAAAQAQAAAGcAAmE0AAAAawAAAG0AAABvAAAAEQEAAABnAAAA#wAAAAAAAAAAAMAYAAAAAAAAAAAAAABvDwAB####AAAAAQAAAAIAAAABAAAAAAAAAAAAAAAAAgAAAHAAAAACAP####8AAAAAABAAAABACAAAAAAAAAAAAAAAAAAAAAAJAABAcpAAAAAAAECAnwo9cKPXAAAACgD#####AAVtaW5pNQABMAAAAAEAAAAAAAAAAAAAAAoA#####wAFbWF4aTUAAjEwAAAAAUAkAAAAAAAAAAAACwD#####AAdDdXJzZXVyAAAABQAAAAUAAAADAAAAcwAAAHQAAAByAAAAAwAAAAB1AQAAAAAQAAABAAAAAQAAAHIBP#AAAAAAAAAAAAAEAQAAAHUAAAD#ABAAAADACAAAAAAAAD#wAAAAAAAAAAAFAABAXUAAAAAAAAAAAHYAAAAMAAAAAHUAAAByAAAADQMAAAAOAAAAcwAAAA0BAAAADgAAAHMAAAAOAAAAdAAAAA8AAAAAdQEAAAAADQACTzEAwBAAAAAAAABAEAAAAAAAAAAABQAAAAB3AAAAeAAAAAwAAAAAdQAAAHIAAAANAwAAAA0BAAAAAT#wAAAAAAAAAAAADgAAAHMAAAANAQAAAA4AAAB0AAAADgAAAHMAAAAPAAAAAHUBAAAAAA0AAkkxAMAAAAAAAAAAQAgAAAAAAAAAAAUAAAAAdwAAAHoAAAAFAQAAAHUAAAD#ABAAAAEAAAACAAAAcgAAAHcAAAAEAQAAAHUAAAD#ARAAAms0AMAAAAAAAAAAQAAAAAAAAAAAAAEAAT#gIwIwIwIwAAAAfAAAABABAAAAdQACYTUAAAB5AAAAewAAAH0AAAARAQAAAHUAAAD#AAAAAAAAAAAAwBgAAAAAAAAAAAAAAH0PAAH###8AAAABAAAAAgAAAAEAAAAAAAAAAAAAAAACAAAAfgAAAAoA#####wACQTQAAmE0AAAADgAAAHAAAAAMAP####8AAABRAAAADQMAAAAOAAAAgAAAAAFAJDMzMzMzMwAAAA8A#####wEAAP8AEAAAAAAAAAAAAAAAQAgAAAAAAAAAAAkAAAAATgAAAIEAAAAXAP####8BAAD#ABAAAAEAAAABAAAAggAAAEsAAAAVAP####8AAACDAAAACgAAABYA#####wEAAP8AEAAAAAAAAAAAAAAAQAgAAAAAAAAAAAkAAgAAAIQAAAAWAP####8BAAD#ABAAAAAAAAAAAAAAAEAIAAAAAAAAAAAJAAEAAACEAAAACgD#####AAJBNQACYTUAAAAOAAAAfgAAAAwA#####wAAAIYAAAANAwAAAA4AAACHAAAAAUAkAAAAAAAAAAAADwD#####AAAA#wAQAAAAAAAAAAAAAABACAAAAAAAAAACcEQJAAAAAIUAAACIAAAAGAD#####AQAAAAAQAAABAAJkQwACAAAASgAAAIkAAAAYAP####8BAAAAABAAAAEAAmRCAAIAAAA5AAAAiQAAABgA#####wEAAAAAEAAAAQACZEEAAgAAACwAAACJAAAAFAD#####AQAAAAAAAAIAAACJAAAAAT#gAAAAAAAAAAAAABcA#####wEAAP8AEAAAAQAAAAIAAACJAAAAUgAAABUA#####wAAAI4AAACNAAAAFgD#####AQAA#wAQAAAAAAAAAAAAAABACAAAAAAAAAAACQABAAAAjwAAABYA#####wEAAP8AEAAAAAAAAAAAAAAAQAgAAAAAAAAAAAkAAgAAAI8AAAAHAP####8AAAD#AQADbnBEAAAAkRAAAAAAAAEAAAABAAAAAQAAAAAAAAAAAAFEAAAAB###########'
    const yy = j3pCreeSVG(stor.lesdiv.figure, { id: svgId, width: 350, height: 300 })
    yy.style.border = 'solid black 1px'
    const A4 = j3pGetRandomInt(1, 10)
    const A5 = j3pGetRandomInt(0, 10)
    stor.mtgAppLecteur.removeAllDoc()
    stor.mtgAppLecteur.addDoc(svgId, txtfigure, true)
    stor.mtgAppLecteur.giveFormula2(svgId, 'A1', j3pGetRandomInt(0, 359))
    stor.mtgAppLecteur.giveFormula2(svgId, 'A2', j3pGetRandomInt(0, 10))
    stor.mtgAppLecteur.giveFormula2(svgId, 'A3', j3pGetRandomInt(0, 10))
    stor.mtgAppLecteur.giveFormula2(svgId, 'A4', A4)
    stor.mtgAppLecteur.giveFormula2(svgId, 'A5', A5)
    stor.mtgAppLecteur.setText(svgId, '#npA', bob[0], true)
    stor.mtgAppLecteur.setText(svgId, '#npB', bob[1], true)
    stor.mtgAppLecteur.setText(svgId, '#npC', bob[2], true)
    stor.mtgAppLecteur.setText(svgId, '#npD', bob[3], true)
    stor.mtgAppLecteur.calculateAndDisplayAll(true)
    return { bob }
  }

  function afcofo () {
    stor.z1.disable()
    stor.z2.disable()
    stor.z3.disable()
    stor.z3.corrige(false)
    stor.z2.corrige(false)
    stor.z1.corrige(false)
    stor.z1.barre()
    stor.z2.barre()
    stor.z3.barre()
    const ret = stor.ret.bob
    j3pEmpty(stor.lesdiv.Boutons)
    stor.mtgAppLecteur.setVisible(svgId, '#d', true, true)
    stor.mtgAppLecteur.setColor(svgId, '#d', 255, 100, 34, true)

    /// puis met en rouge et préviens
    if (stor.ERRminuscule) {
      j3pAffiche(stor.lesdiv.explications, null, "Les noms des lettres s'écrivent en majuscules !")
      stor.yaexplik = true
    }
    if (stor.ERRpasexiste) {
      const buf = stor.yaexplik ? '<BR>' : ''
      j3pAffiche(stor.lesdiv.explications, null, buf + 'Tu nommes un point qui n’est pas dans la figure !')
      stor.yaexplik = true
    }
    if (stor.ERR2m) {
      const buf = stor.yaexplik ? '<BR>' : ''
      j3pAffiche(stor.lesdiv.explications, null, buf + 'Tu as écrit deux fois le même point !')
      stor.yaexplik = true
    }
    if (stor.ERRbob && (!stor.ERRminuscule) && (!stor.ERRpasexiste) && (!stor.ERR2m)) {
      const buf = stor.yaexplik ? '<BR>' : ''
      j3pAffiche(stor.lesdiv.explications, null, buf + 'Ces trois points ne sont pas sur une même ligne droite !')
      stor.yaexplik = true
    }
    stor.yaco = true
    j3pAffiche(stor.lesdiv.solution, null, 'Les points ' + ret[0] + ' , ' + ret[1] + ' et ' + ret[2] + ' semblent alignés.')
  }

  function yareponse () {
    if (me.isElapsed) return true
    if (stor.z1.reponse() === '') {
      stor.z1.focus()
      return false
    }
    if (stor.z2.reponse() === '') {
      stor.z2.focus()
      return false
    }
    if (stor.z3.reponse() === '') {
      stor.z3.focus()
      return false
    }
    return true
  }

  function Bonnereponse () {
    stor.repEl = []
    stor.repEl[0] = stor.z1.reponse()
    stor.repEl[1] = stor.z2.reponse()
    stor.repEl[2] = stor.z3.reponse()

    const ret = stor.ret.bob
    stor.ERRminuscule = false
    stor.ERRpasexiste = false
    stor.ERR2m = false
    stor.ERRbob = false

    const minus = 'abcdefghijklmnopqrstuvwxyz'
    // verif points existent
    // si minuscules previent
    for (let i = 0; i < 3; i++) {
      if (minus.indexOf(stor.repEl[i]) !== -1) { stor.ERRminuscule = true }
      if (ret.indexOf(stor.repEl[i]) === -1) { stor.ERRpasexiste = true }
    }
    // verif pas deux fois le mm
    if (stor.repEl[0] === stor.repEl[1]) { stor.ERR2m = true }
    if (stor.repEl[0] === stor.repEl[2]) { stor.ERR2m = true }
    if (stor.repEl[1] === stor.repEl[2]) { stor.ERR2m = true }

    // verif ya pas bob[3]
    if (stor.repEl.indexOf(ret[3]) !== -1) { stor.ERRbob = true }

    return ((!stor.ERRminuscule) && (!stor.ERRpasexiste) && (!stor.ERR2m) && (!stor.ERRbob))
  }

  function voir (a, b) {
    j3pEmpty(stor.lesdiv.Boutons)
    let ki
    if (a !== 3 && b !== 3) {
      ki = '#d'
    } else {
      const c = a !== 3 ? a : b
      ki = c === 0 ? '#dA' : c === 1 ? '#dB' : '#dC'
    }
    stor.mtgAppLecteur.setVisible(svgId, ki, true, true)
  }

  function getDonnees () {
    return {

      // Nombre de répétitions de l’exercice
      nbrepetitions: 9,
      nbetapes: 1,

      Aide: true,
      segment: true,
      droite: true,
      demi_droite: true,
      cercle: true,
      ElementsInutiles: 'Oui',
      NotationMathematique: 'Oui',

      // une indication<br>de deux lignes...";
      indication: '',

      // pour le cas de presentation1bis
      // En effet pas d’indication possible pour cette présentation
      // ,surchargeindication: false;

      // Nombre de chances dont dispose l’él§ve pour répondre à la question
      nbchances: 1,

      structure: 'presentation1bis', //  || "presentation2" || "presentation3"  || "presentation1bis"

      // Paramètres de la section, avec leurs valeurs par défaut.

      /*
      Les textes présents dans la section
      Sont donc accessibles dans le code par ds.textes.phrase1
      Possibilité d’écrire dans le code :
      var lestextes: ds.textes;
      puis accès à l’aide de lestextes.phrase1
      */
      textes: {

        point: '.',
        Choisir: 'Choisir',
        Consigne1: 'Les points &nbsp;',
        Consigne2: '&nbsp; semblent alignés.',

        consigneG: 'Observe bien la figure puis complète la phrase ci-dessous.'

      },

      // Par défaut, pas de temps limite. Décommentez pour avoir un temps limite par défaut. En secondes.

      // ,limite: 10;
      /*
      Phrase d’état renvoyée par la section
      Si ,pe=0 alors la section est quantitative et renvoie un score entre 0 et 1.
      Dans le cas où la section est qualitative, lister les différents pe renvoyées :
          ,pe_1: "toto"
          ,pe_2: "tata"
          etc
      Dans la partie navigation, il faudra affecter la variable ,parcours.pe à l’une des précédentes.
      Par exemple,
      ,parcours.pe: ds.pe_2
      */
      pe: 0
    }
  }
  function initSection () {
    me.donneesSection = getDonnees()
    ds = me.donneesSection

    me.surcharge()

    // Construction de la page
    me.construitStructurePage(ds.structure)

    ds.nbitems = ds.nbetapes * ds.nbrepetitions

    stor.dejacas = []
    stor.dejacont = []
    stor.possibles = []
    if (me.donneesSection.segment) { stor.possibles.push('segment') }
    if (me.donneesSection.droite) { stor.possibles.push('droite') }
    if (me.donneesSection.demi_droite) { stor.possibles.push('demi_droite') }
    if (me.donneesSection.cercle) { stor.possibles.push('cercle') }
    if (stor.possibles.length === 0) { stor.possibles = ['segment'] }

    /*
     Par convention,`
    `   me.typederreurs[0] = nombre de bonnes réponses
        me.typederreurs[1] = nombre de fois où l’élève a utilisé une chance supplémentaire
        me.typederreurs[2] = nombre de mauvaises réponses
        me.typederreurs[10] = nombre de fois où l’élève a eu faux à cause d’un temps limite
        LES AUTRES peuvent être utilisées par la détermination de la pe dans le cas d’une section qualitative
     */
    me.typederreurs = [0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0]

    me.score = 0

    me.afficheTitre('Notion d’alignement')

    if (ds.indication) me.indication(me.zones.IG, ds.indication)

    getMtgCore({ withMathJax: true })
      .then(
        // success
        (mtgAppLecteur) => {
          stor.mtgAppLecteur = mtgAppLecteur
          enonceMain()
        },
        // failure
        (error) => {
          j3pShowError(error, { message: 'mathgraph n’est pas correctement chargé', mustNotify: true })
        })
      // plantage dans enonceMain
      .catch(j3pShowError)
  } // initSection

  function enonceMain () {
    me.videLesZones()

    stor.lesdiv = {}
    stor.lesdiv.conteneur = j3pAddElt(me.zonesElts.MG, 'div', {
      style: me.styles.etendre('toutpetit.enonce', { padding: '10px' })
    })
    stor.lesdiv.consigne = addDefaultTable(stor.lesdiv.conteneur, 1, 1)[0][0]
    stor.lesdiv.figure = addDefaultTable(stor.lesdiv.conteneur, 1, 1)[0][0]
    stor.lesdiv.travail = addDefaultTable(stor.lesdiv.conteneur, 1, 1)[0][0]
    stor.lesdiv.explications = j3pAddElt(stor.lesdiv.conteneur, 'div')
    stor.lesdiv.solution = j3pAddElt(stor.lesdiv.conteneur, 'div')
    stor.lesdiv.correction = j3pAddElt(me.zonesElts.MD, 'div', {
      style: me.styles.etendre('toutpetit.enonce', { padding: '10px' })
    })
    stor.lesdiv.Boutons = j3pAddElt(me.zonesElts.MD, 'div', {
      style: me.styles.etendre('toutpetit.enonce', { padding: '10px' })
    })
    stor.lesdiv.explications.style.color = me.styles.cfaux
    stor.lesdiv.solution.style.color = me.styles.toutpetit.correction.color
    me.zonesElts.MG.classList.add('fond')
    stor.lesdiv.travail.classList.add('travail')
    stor.lesdiv.consigne.classList.add('enonce')
    stor.lesdiv.explications.classList.remove('explique')
    stor.lesdiv.solution.classList.remove('correction')

    /// cree fig et recupe noms

    stor.ret = AfficheFig()
    const ret = stor.ret

    // pose question et phrase a complete
    if (me.donneesSection.Aide) {
      stor.boton1 = j3pAjouteBouton(stor.lesdiv.Boutons, voir.bind(null, 0, 1), { className: 'MepBoutons2', value: 'Tracer (' + ret.bob[0] + ret.bob[1] + ')' })
      j3pAddElt(stor.lesdiv.Boutons, 'br')
      j3pAddElt(stor.lesdiv.Boutons, 'br')
      stor.boton2 = j3pAjouteBouton(stor.lesdiv.Boutons, voir.bind(null, 0, 2), { className: 'MepBoutons2', value: 'Tracer (' + ret.bob[0] + ret.bob[2] + ')' })
      j3pAddElt(stor.lesdiv.Boutons, 'br')
      j3pAddElt(stor.lesdiv.Boutons, 'br')
      stor.boton3 = j3pAjouteBouton(stor.lesdiv.Boutons, voir.bind(null, 0, 3), { className: 'MepBoutons2', value: 'Tracer (' + ret.bob[0] + ret.bob[3] + ')' })
      j3pAddElt(stor.lesdiv.Boutons, 'br')
      j3pAddElt(stor.lesdiv.Boutons, 'br')
      stor.boton4 = j3pAjouteBouton(stor.lesdiv.Boutons, voir.bind(null, 1, 2), { className: 'MepBoutons2', value: 'Tracer (' + ret.bob[1] + ret.bob[2] + ')' })
      j3pAddElt(stor.lesdiv.Boutons, 'br')
      j3pAddElt(stor.lesdiv.Boutons, 'br')
      stor.boton5 = j3pAjouteBouton(stor.lesdiv.Boutons, voir.bind(null, 1, 3), { className: 'MepBoutons2', value: 'Tracer (' + ret.bob[1] + ret.bob[3] + ')' })
      j3pAddElt(stor.lesdiv.Boutons, 'br')
      j3pAddElt(stor.lesdiv.Boutons, 'br')
      stor.boton6 = j3pAjouteBouton(stor.lesdiv.Boutons, voir.bind(null, 2, 3), { className: 'MepBoutons2', value: 'Tracer (' + ret.bob[2] + ret.bob[3] + ')' })
    }

    j3pAffiche(stor.lesdiv.consigne, null, '<b>' + me.donneesSection.textes.consigneG + '</b>', {
      a: ret.lautre
    })

    const tt = addDefaultTable(stor.lesdiv.travail, 1, 7)
    j3pAffiche(tt[0][0], null, me.donneesSection.textes.Consigne1)
    stor.z1 = new ZoneStyleMathquill1(tt[0][1], {
      restric: 'ABCDEFGHIJKLMNOPQRSTUVWXYZabcdefghijklmnopqrstuvwxyz',
      limitenb: 1,
      limite: 0,
      enter: () => { me.sectionCourante() }
    })
    j3pAffiche(tt[0][2], null, '&nbsp;,&nbsp;')
    stor.z2 = new ZoneStyleMathquill1(tt[0][3], {
      restric: 'ABCDEFGHIJKLMNOPQRSTUVWXYZabcdefghijklmnopqrstuvwxyz',
      limitenb: 1,
      limite: 0,
      enter: () => { me.sectionCourante() }
    })
    j3pAffiche(tt[0][4], null, '&nbsp; et &nbsp;')
    stor.z3 = new ZoneStyleMathquill1(tt[0][5], {
      restric: 'ABCDEFGHIJKLMNOPQRSTUVWXYZabcdefghijklmnopqrstuvwxyz',
      limitenb: 1,
      limite: 0,
      enter: () => { me.sectionCourante() }
    })
    j3pAffiche(tt[0][6], null, me.donneesSection.textes.Consigne2)
    me.finEnonce()
  }

  switch (this.etat) {
    case 'enonce':
      // code exécuté au lancement de la section
      if (this.debutDeLaSection) {
        initSection()
      } else {
        enonceMain()
      }
      break // case "enonce":

    case 'correction':
      // On teste si une réponse a été saisie
      stor.lesdiv.explications.classList.remove('explique')
      stor.lesdiv.solution.classList.remove('correction')

      if (!yareponse()) {
        // ici on donne le message à afficher si une ou plusieurs zone(s) n’est pas complétée (tout d’abord on met en rouge : styles.cfaux)
        stor.lesdiv.correction.style.color = this.styles.cfaux
        stor.lesdiv.correction.innerHTML = reponseIncomplete
        this.afficheBoutonValider()
      } else { // Une réponse a été saisie
        stor.yaco = false
        stor.yaexplik = false

        // Bonne réponse
        if (Bonnereponse()) {
          this._stopTimer()
          this.score += j3pArrondi(1 / ds.nbetapes, 1)
          stor.lesdiv.correction.style.color = this.styles.cbien
          stor.lesdiv.correction.innerHTML = cBien

          stor.z1.disable()
          stor.z2.disable()
          stor.z3.disable()
          stor.z3.corrige(true)
          stor.z2.corrige(true)
          stor.z1.corrige(true)

          j3pEmpty(stor.lesdiv.Boutons)
          stor.mtgAppLecteur.setVisible(svgId, '#d', true, true)
          stor.mtgAppLecteur.setColor(svgId, '#d', 20, 255, 100, true)

          this.typederreurs[0]++
          this.cacheBoutonValider()
          this.etat = 'navigation'
          this.sectionCourante()
        } else {
          // Pas de bonne réponse
          stor.lesdiv.correction.style.color = this.styles.cfaux

          // A cause de la limite de temps :
          if (me.isElapsed) { // limite de temps
            this._stopTimer()
            this.cacheBoutonValider()
            stor.lesdiv.correction.innerHTML += 'Temps écoulé. <BR>' + '<br>' + regardeCorrection

            /// ////

            afcofo()

            /// /

            this.etat = 'navigation'
            this.sectionCourante()
          } else { // Réponse fausse :
            stor.lesdiv.correction.innerHTML = cFaux

            if (this.essaiCourant < ds.nbchances) {
              stor.lesdiv.correction.innerHTML += '<br>' + essaieEncore

              this.typederreurs[1]++
              // indication éventuelle ici
            } else { // Erreur au nème essai
              this._stopTimer()
              this.cacheBoutonValider()
              stor.lesdiv.correction.innerHTML += '<br>' + regardeCorrection

              afcofo()

              this.typederreurs[2]++
              this.etat = 'navigation'
              this.sectionCourante()
            }
          }
        }

        if (stor.yaexplik) stor.lesdiv.explications.classList.add('explique')
        if (stor.yaco) stor.lesdiv.solution.classList.add('correction')
      }
      // Obligatoire
      this.finCorrection()
      break // case "correction":

    case 'navigation':

      if (this.sectionTerminee()) {
        // On détermine la phrase d’état renvoyée par la section
        this.parcours.pe = (ds.nbetapes * this.score) / ds.nbitems
        this.afficheBoutonSectionSuivante()
        this.focus('sectioncontinuer')
      } else {
        this.etat = 'enonce'
        this.afficheBoutonSuite()
        this.focus('boutoncontinuer')
      }
      // Obligatoire
      this.finNavigation()

      break // case "navigation":
  }
}
