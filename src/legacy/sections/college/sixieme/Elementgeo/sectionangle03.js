import { j3pAddElt, j3pAjouteBouton, j3pArrondi, j3pClone, j3pDetruit, j3pEmpty, j3pGetNewId, j3pGetRandomBool, j3pGetRandomInt, j3pShowError, j3pShuffle } from 'src/legacy/core/functions'
import { getPe, initCptPe } from 'src/legacy/sections/college/helperTommy'
import { j3pAffiche } from 'src/lib/mathquill/functions'
import { addDefaultTable } from 'src/legacy/themes/table'
import { estMesureangle } from 'src/legacy/outils/zoneStyleMathquill/functions'
import ZoneStyleMathquill1 from 'src/legacy/outils/zoneStyleMathquill/ZoneStyleMathquill1'
import { j3pCreeSVG } from 'src/legacy/core/functionsSvg'
import textesGeneriques from 'src/lib/core/textes'
import { getMtgCore } from 'src/lib/outils/mathgraph'
import { addMaj } from 'src/lib/utils/string'

const { cBien, cFaux, essaieEncore, reponseIncomplete } = textesGeneriques

/**
 * Ajouter une description ici
 * @fileOverview
 * @author Tommy Barroy <t.barroy@laposte.net>
 * @since 2017-10
*/
export const params = {
  outils: ['mathquill'],
  /**
   * Ce qui est paramétrable dans l’éditeur de graphe
   * @type {LegacySectionParamsParametres}
   */
  parametres: [
    ['nbrepetitions', 3, 'entier', 'Nombre de répétitions de la section'],
    ['nbchances', 2, 'entier', 'Nombre d’essais possibles.'],
    ['Nul', false, 'boolean', '<u>true</u>: L’angle peut être nul.'],
    ['Plat', false, 'boolean', '<u>true</u>: L’angle peut être plat.'],
    ['Question', 'les trois', 'liste', '<u>Placement</u>: Il faut placer le rapporteur. <br><br> <u>Grad</u> Il faut sélectionner la graduation utilisée.  <br><br> <u>Mesure</u> Il faut donner la mesure du rapporteur.', ['Placement', 'Grad', 'Mesure', 'Grad_Mesure', 'Placement_Mesure', 'les trois']],
    // le modèle gère la compatibilité ascendante avec l’ancien paramètre MiseEnPagePlusClaire
    ['theme', 'standard', 'liste', 'Modèle pour l’affichage', ['standard', 'zonesAvecImageDeFond']]
  ],
  // dans le cas d’une section QUANTITATIVE
  pe: [
    { pe_1: 'placement' },
    { pe_2: 'graduation' },
    { pe_3: 'lecture' }
  ]
}

/**
 * section angle03
 * @this {Parcours}
 */
export default function main () {
  const me = this
  const ds = me.donneesSection
  const stor = me.storage
  let svgId = stor.svgId

  function initSection () {
    stor.tempo = 20
    stor.tempodeb = 1000
    stor.depCentreCo = 2
    initCptPe(ds, stor)

    // On surcharge avec les parametres passés par le graphe
    me.surcharge()
    me.validOnEnter = false // ex donneesSection.touche_entree

    // Construction de la page
    me.construitStructurePage('presentation3')

    me.donneesSection.nbetapes = 1
    me.donneesSection.nbitems = me.donneesSection.nbetapes * me.donneesSection.nbrepetitions

    stor.afaire = []
    if ((ds.Question === 'les trois') || (ds.Question.indexOf('Placement') !== -1)) stor.afaire.push('p')
    if ((ds.Question === 'les trois') || (ds.Question.indexOf('Grad') !== -1)) stor.afaire.push('g')
    if ((ds.Question === 'les trois') || (ds.Question.indexOf('Mesure') !== -1)) stor.afaire.push('m')

    ds.lesExos = []
    let lp = ['a', 'o', 'a', 'o']
    if (ds.Nul) lp.push('n')
    if (ds.Plat) lp.push('p')
    lp = j3pShuffle(lp)
    for (let i = 0; i < ds.nbitems; i++) {
      ds.lesExos[i] = { sit: lp[i % lp.length] }
    }
    ds.lesExos = j3pShuffle(ds.lesExos)

    me.typederreurs = [0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0]
    stor.liste = ['Choisir', 'nul.', 'aigu.', 'droit.', 'obtus.', 'plat.']

    me.score = 0
    let tt = 'Mesurer un angle'
    if (ds.Question === 'Placement') tt = 'Placer le rapporteur pour mesurer directement'
    if (ds.Question === 'Grad') tt = 'La bonne graduation'

    me.afficheTitre(tt)
    stor.restric = '0123456789°'
    getMtgCore({ withMathJax: true })
      .then(
        // success
        (mtgAppLecteur) => {
          stor.mtgAppLecteur = mtgAppLecteur
          enonceMain()
        },
        // failure
        (error) => {
          j3pShowError(error, { message: 'mathgraph n’est pas correctement chargé', mustNotify: true })
        })
      // plantage dans enonceMain
      .catch(j3pShowError)
  }

  function faisChoixExos () {
    const e = ds.lesExos.pop()
    const nomspris = []
    stor.A = addMaj(nomspris)
    stor.B = addMaj(nomspris)
    stor.C = addMaj(nomspris)
    stor.rotation = j3pGetRandomInt(1, 359)
    switch (e.sit) {
      case 'a': stor.deplacement = j3pGetRandomInt(10, 90)
        break
      case 'o': stor.deplacement = j3pGetRandomInt(90, 170)
        break
      case 'n': stor.deplacement = 0
        break
      case 'p': stor.deplacement = 180
        break
    }
    stor.encours = stor.afaire[0]
    stor.placeok = true
    stor.imfaite = false
    stor.zommfait = false
    stor.pose = false
    return e
  }
  function poseQuestion () {
    j3pEmpty(stor.lesdiv.consigne)
    j3pEmpty(stor.lesdiv.zonedrep)
    const ttt = addDefaultTable(stor.lesdiv.consigne, 1, 4)
    const nameA = '$\\widehat{' + stor.A + stor.B + stor.C + '}$'
    if (stor.encours === 'p') {
      j3pAffiche(ttt[0][0], null, '<b>Place correctement le rapporteur pour mesurer directement l’angle ' + nameA + '</b> ')
      me.afficheBoutonValider()
    }
    if (stor.encours === 'm') {
      ttt[0][2].style.width = '60px'
      j3pAffiche(ttt[0][0], null, '<b>Donne la mesure de l’angle  ' + nameA + '</b> ')
      const uipo = addDefaultTable(ttt[0][3], 1, 3)
      j3pAffiche(uipo[0][1], null, nameA + ' $\\approx$')
      stor.larep = new ZoneStyleMathquill1(uipo[0][2], {
        restric: stor.restric,
        limitenb: 1,
        limite: 5,
        hasAutoKeyboard: true,
        inverse: true,
        enter: me.sectionCourante.bind(me)
      })
      uipo[0][2].style.verticalAlign = 'bottom'
    }
    if (stor.encours === 'g') {
      // FIXME expliquer pourquoi il faut positionner l'élève d'office au dernier essai dès l'énoncé ? (si c'est justifié il faut plutôt fixer ds.nbchances à 1)
      j3pAffiche(ttt[0][0], null, '<b>Sélectionne les graduations utilisées pour lire directement la mesure de l’angle   ' + nameA + '</b> ')
    }

    j3pEmpty(stor.lesdiv.correction)
    j3pEmpty(stor.lesdiv.solution)
    j3pEmpty(stor.lesdiv.explications)
    if (stor.encours !== 'm') {
      stor.lesdiv.travail.classList.add('travail')
    } else {
      ttt[0][3].classList.add('travail')
    }
    stor.lesdiv.consigne.classList.add('enonce')
    stor.lesdiv.explications.classList.remove('explique')
    stor.lesdiv.solution.classList.remove('correction')
  }

  function creeLesDiv () {
    stor.lesdiv = {}
    stor.lesdiv.conteneur = j3pAddElt(me.zonesElts.MG, 'div', {
      style: me.styles.etendre('toutpetit.enonce', { padding: '10px' })
    })
    stor.lesdiv.consigne1 = addDefaultTable(stor.lesdiv.conteneur, 1, 1)[0][0]
    const ttt = addDefaultTable(stor.lesdiv.consigne1, 1, 3)
    stor.lesdiv.consigne = ttt[0][0]
    ttt[0][1].style.width = '40px'
    stor.lesdiv.solution = ttt[0][2]
    stor.lesdiv.travail1 = addDefaultTable(stor.lesdiv.conteneur, 1, 1)[0][0]
    const yyy = addDefaultTable(stor.lesdiv.travail1, 1, 3)
    yyy[0][1].style.width = '40px'
    stor.lesdiv.travail = yyy[0][0]
    stor.lesdiv.correction = yyy[0][2]
    stor.lesdiv.zonefig = j3pAddElt(stor.lesdiv.travail, 'div')
    stor.lesdiv.zonedrep = yyy[0][1]
    stor.lesdiv.explications = j3pAddElt(stor.lesdiv.conteneur, 'div')
    stor.lesdiv.explications.style.color = me.styles.cfaux
    me.zonesElts.MG.classList.add('fond')
    stor.lesdiv.solution.style.color = me.styles.toutpetit.correction.color
  }

  function makefig () {
    stor.consel = []
    stor.captured = false
    stor.captured2 = false
    stor.capInt = false
    stor.capExt = false
    stor.oublipa = false
    stor.x = stor.y = 0
    if (!stor.imfaite) {
      stor.svgId = j3pGetNewId('mtg32')
      svgId = stor.svgId
      stor.imfaite = true
      const txtFigure = 'TWF0aEdyYXBoSmF2YTEuMAAAABM+5mZmAAJmcv###wEA#wEAAAACAAB9EgAAgK4AAAAAAzkAAAJtAAABAQAAAAEAAAAGAApSYXBwb3J0ZXVyAc9Db25zdHJ1aXQgdW4gcmFwcG9ydGV1ciDDoCBwYXJ0aXIgZGUgZGV1eCBwb2ludHMgbGlicmVzLgpMZXMgb2JqZXRzIHNvdXJjZXMgbnVtw6lyaXF1ZXMgc29udCA6CiMxOmVwYWlzc2V1ciA6IGRpc3RhbmNlIGVuIHBpeGVscyBlbnRyZSBsZXMgZGV1eCBhcmNzIGR1IHJhcHBvcnRldXIKIzI6bG9uZ1BldGl0ZXNHcmFkIDogTG9uZ3VldXIgZW4gcGl4ZWwgZGVzIHBldGl0ZXMgZ3JhZHVhdGlvbnMKIzM6bG9uZ01veWVubmVzR3JhZCA6IExvbmd1ZXVyIGVuIHBpeGVsIGRlcyBtb3llbm5lcyBncmFkdWF0aW9ucwojNDpsb25nR3JhbmRlc0dyYWQgOiBMb25ndWV1ciBlbiBwaXhlbCBkZXMgZ3JhbmRlcyBncmFkdWF0aW9ucwpMZXMgb2JqZXRzIHNvdXJjZXMgZ3JhcGhpcXVlcyBzb250IDoKIzU6TGUgY2VudHJlIGR1IHJhcHBvcnRldXIKIzY6TGUgcG9pbnMgY29ycmVzcG9uZGFudCDDoCBsYSBncmFkdWF0aW9uIDAKAAAABgAAAA0BAAAAQP####8AAAABABFDRWxlbWVudEdlbmVyaXF1ZQAJZXBhaXNzZXVy#####wAAAAEAAAAAAA9sb25nUGV0aXRlc0dyYWT#####AAAAAQAAAAAAEGxvbmdNb3llbm5lc0dyYWT#####AAAAAQAAAAAAD2xvbmdHcmFuZGVzR3JhZP####8AAAABAAAAAAACTzEAAAAA#####wAAAAAAAkkxAAAAAP##########AAAAAQARQ1N5bWV0cmllQ2VudHJhbGUA#####wAAAAT#####AAAAAQALQ1BvaW50SW1hZ2UA#####wGkpKQAEAAAAAAAAAAAAAAAQAgAAAAAAAAAAAUAAAAABQAAAAb#####AAAAAQASQ0FyY0RlQ2VyY2xlRGlyZWN0Af####8ApKSkAAAAAQAAAAQAAAAFAAAAB#####8AAAACAAlDQ2VyY2xlT1IA#####wGkpKQAAAABAAAABf####8AAAABAA9DUmVzdWx0YXRWYWxldXIAAAAAAf####8AAAABAAhDU2VnbWVudAD#####AaSkpAAQAAABAAAAAQAAAAcAAAAF#####wAAAAEAEENJbnREcm9pdGVDZXJjbGUA#####wAAAAoAAAAJ#####wAAAAEAEENQb2ludExpZUJpcG9pbnQA#####wGkpKQBEAACVzEAAAAAAAAAAABACAAAAAAAAAAABQACAAAACwAAAAIA#####wGkpKQAEAAAAAAAAAAAAAAAQAgAAAAAAAAAAAUAAAAADAAAAAYAAAADAf####8ApKSkAAAAAQAAAAQAAAAMAAAADf####8AAAABAA9DUG9pbnRMaWVDZXJjbGUA#####wEAAAABEAACSjEAAAAAAAAAAABACAAAAAAAAAAABQABP8hHzOzEEzYAAAAI#####wAAAAEAFkNEcm9pdGVQZXJwZW5kaWN1bGFpcmUA#####wGkpKQAEAAAAQAAAAEAAAAFAAAACgAAAAQA#####wGkpKQAAAABAAAABf####8AAAABAApDT3BlcmF0aW9uAwAAAAUAAAAA#####wAAAAEACkNDb25zdGFudGVAAAAAAAAAAAEAAAAHAP####8AAAAQAAAAEQAAAAgA#####wGkpKQAEAAAAAAAAAAAAAAAQAgAAAAAAAAAAAUAAQAAABL#####AAAAAQAMQ1RyYW5zbGF0aW9uAP####8AAAAFAAAAEwAAAAIA#####wGkpKQAEAAAAAAAAAAAAAAAQAgAAAAAAAAAAAUAAAAABwAAABT#####AAAAAQAJQ1BvbHlnb25lAP####8BpKSkAAAAAQAAAAUAAAAFAAAAEwAAABUAAAAHAAAABf####8AAAABABBDU3VyZmFjZVBvbHlnb25lAf####8A2NjYAAAAAAAFAAAAFgAAAAYB#####wCkpKQAEAAAAQAAAAEAAAAHAAAAFQAAAAYB#####wCkpKQAEAAAAQAAAAEAAAAVAAAAEwAAAAYB#####wCkpKQAEAAAAQAAAAEAAAATAAAABQAAAAkA#####wEAAP8AEAAAAAAAAAAAAAAAQAgAAAAAAAAAAAUAAT#nyXJN8Z0XAAAACAAAAAkA#####wEAAP8AEAAAAAAAAAAAAAAAQAgAAAAAAAAAAAUAAT#nuF42fT8fAAAADv####8AAAACAA1DTGlldURlUG9pbnRzAP####8BAAD#AAAAAQAAABwAAAAyAAAAAAAcAAAAAAAAABAA#####wEAAP8AAAABAAAAGwAAADIAAAAAABsAAAAA#####wAAAAEAEUNTdXJmYWNlRGV1eExpZXV4Af####8A2NjYAAAAAAAFAAAAHQAAAB4AAAAGAP####8BpKSkABAAAAEAAAABAAAABAAAAA8AAAAEAP####8BpKSkAAAAAQAAAA8AAAAFAAAAAQEAAAAHAP####8AAAAgAAAAIQAAAAgA#####wGkpKQAEAAAAAAAAAAAAAAAQAgAAAAAAAAAAAUAAgAAACIAAAAGAP####8Bvb29ABAAAAEAAAABAAAADwAAACP#####AAAAAgASQ0xpZXVPYmpldFBhclB0TGllAf####8Avb29AAAAAAAkAAAADEBmoAAAAAAAAAAADwAAAAAAAAAEAP####8Bvb29AAAAAQAAAA8AAAAFAAAAAwEAAAAHAP####8AAAAgAAAAJgAAAAgA#####wG9vb0AEAAAAAAAAAAAAAAAQAgAAAAAAAAAAAUAAgAAACcAAAAGAP####8BhISEABAAAAEAAAABAAAAKAAAAA8AAAAEAP####8BhISEAAAAAQAAAA8AAAAFAAAAAgEAAAAHAP####8AAAAgAAAAKgAAAAgA#####wGEhIQAEAAAAAAAAAAAAAAAQAgAAAAAAAAAAAUAAgAAACsAAAAGAP####8BhISEABAAAAEAAAABAAAALAAAAA8AAAASAf####8AhISEAAAAAAAtAAAADEBCgAAAAAAAAAAADwAAAAAAAAASAf####8AhISEAAAAAAApAAAADEAzAAAAAAAAAAAADwAAAAD#####AAAAAgATQ01lc3VyZUFuZ2xlT3JpZW50ZQD#####AARhbmcxAAAABQAAAAQAAAAP#####wAAAAEAFENEcm9pdGVEaXJlY3Rpb25GaXhlAP####8BAAAAARAAAAEAAAABAAAABAE#8AAAAAAAAP####8AAAABAA9DUG9pbnRMaWVEcm9pdGUA#####wEAAAABEAACSzEAAAAAAAAAAABACAAAAAAAAAAABQABQG#AAAAAAAAAAAAxAAAAEwD#####AARhbmcyAAAAMgAAAAQAAAAPAAAABAD#####AQAAAAAAAAEAAAAoAAAADEAoAAAAAAAAAQAAAAcA#####wAAACAAAAA0AAAACAD#####AQAAAAAQAAAAAAAAAAAAAABACAAAAAAAAAAABQACAAAANf####8AAAABAA9DVmFsZXVyQWZmaWNoZWUA#####wEAAAABAAAAAAA2DAAAAAAAAQAAAAEAAAALAQAAAAUAAAAzAAAADEBWgAAAAAAAAAAAAAAAAAAwAAAAEgH#####AAAAAAAAAAAANwAAAAxAMwAAAAAAAAAAAA8AAAAA#####wAAAAEAB0NDYWxjdWwA#####wAHYW5nc3VwMQAIMTgwLWFuZzEAAAALAQAAAAxAZoAAAAAAAAAAAAUAAAAw#####wAAAAIAD0NNZXN1cmVBYnNjaXNzZQD#####AARhYnMxAAAABAAAAAUAAAAM#####wAAAAEAC0NIb21vdGhldGllAP####8AAAAEAAAABQAAADoAAAACAP####8BAAAAABAAAAAAAAAAAAAAAEAIAAAAAAAAAAAFAAAAAA8AAAA7AAAAFgD#####AQAAAAEAAAAAADwLAAAAAAABAAAAAgAAAAsBAAAABQAAADMAAAAMQFaAAAAAAAAAAAAAAAAAADkAAAASAf####8AAAAAAAAAAAA9AAAADEAzAAAAAAAAAAAADwAAAAAAAAAGAf####8ApKSkABAAAAEAAAABAAAADAAAAA0AAAABAAAAsP####8AAAABAApDQ2FsY0NvbnN0AP####8AAnBpABYzLjE0MTU5MjY1MzU4OTc5MzIzODQ2AAAADEAJIftURC0YAAAAFwD#####AAZkaXN0ZWUAAzAuNQAAAAw#4AAAAAAAAP####8AAAABAApDUG9pbnRCYXNlAP####8BAAAAAA4AAVUAwCQAAAAAAABAEAAAAAAAAAAABQAAQEaAAAAAAAFAef4UeuFHrgAAABQA#####wEAAAAAEAAAAQAAAAEAAAACAT#zMzMzMzMzAAAAFQD#####AQAAAAAOAAFWAMAAAAAAAAAAQBAAAAAAAAAAAAUAAUA#mBBiTdLyAAAAA#####8AAAABAAlDTG9uZ3VldXIA#####wAAAAIAAAAEAAAAGwD#####AAAA#wEQAAFCAAAAAAAAAAAAQAgAAAAAAAAAAnA2CQABQGlAAAAAAABAce4UeuFHrgAAAAQA#####wH#AAAAAAABAAAABgAAAAxAFAAAAAAAAAAAAAAJAP####8A####ARAAAUMAAAAAAAAAAABACAAAAAAAAAACcHQFAAE#+QqTiBH8AAAAAAf#####AAAAAQAIQ1ZlY3RldXIA#####wH#AAAAEAAAAQAAAAEAAAACAAAABAD#####AAAAAQANQ1RyYW5zUGFyVmVjdAD#####AAAACQAAAAIA#####wH#AAAAEAABQQAAAAAAAAAAAEAIAAAAAAAAAAAIAAAAAAYAAAAK#####wAAAAEADENCaXNzZWN0cmljZQD#####AQAAAAAQAAABAAAAAQAAAAgAAAAGAAAACwAAABUA#####wEAAAAAEAAAAAAAAAAAAAAAP+MzMzMzMzMAAAUAAUBpPkKWDt3JAAAADAAAABMA#####wAEYW5nMgAAAAsAAAAGAAAACAAAABYA#####wH#AAAAQAgAAAAAAAA#8AAAAAAAAAAAAAAADQ8AAAAAAAEAAAABAAAADAAAAAAAAAAAAAAAAsKwAAAAAA7#####AAAAAgATQ01hcnF1ZUFuZ2xlT3JpZW50ZQD#####Af8AAAAAAAEAAAABQEJbiQkrj78AAAALAAAABgAAAAgAAAAAAQD#####AAAABv####8AAAABAAlDRHJvaXRlQUIA#####wH#AAAAEAAAAQAAAAEAAAAGAAAACAAAAAQA#####wH#AAAAAAABAAAACAAAAAw#4zMzMzMzMwAAAAAHAP####8AAAASAAAAEwAAAAgA#####wH#AAAAEAAAAAAAAAAAAAAAQAgAAAAAAAAAAAgAAQAAABT#####AAAAAQAJQ1JvdGF0aW9uAP####8AAAAGAAAADEBWgAAAAAAAAAAAAgD#####Af8AAAAQAAAAAAAAAAAAAABACAAAAAAAAAAACAAAAAAVAAAAFgAAAAIA#####wH#AAAAEAAAAAAAAAAAAAAAQAgAAAAAAAAAAAgAAAAAFwAAABEAAAADAP####8A####AARhcmMyAAEAAAAGAAAAGAAAABf#####AAAAAQAZQ1N1cmZhY2VTZWN0ZXVyQ2lyY3VsYWlyZQD#####AP###wAAAAAABQAAABkAAAAGAP####8BAAAAABAAAAEAAAABAAAAAgAAAAT#####AAAAAQAHQ01pbGlldQD#####AQAAAAAQAAAAAAAAAAAAAABACAAAAAAAAAAABQAAAAACAAAABP####8AAAACAAxDQ29tbWVudGFpcmUA#####wEAAAAAAAAAAAAAAABAGAAAAAAAAAAAAAAAHAwAAAAAAAEAAAAAAAAADAAAAAAAAAAAAAExAAAAFwD#####AANhbmcABGFuZzIAAAAFAAAADgAAAAgA#####wH#AAAAEAAAAAAAAAAAAAAAQAgAAAAAAAAAAAgAAgAAABQAAAAbAP####8BAAAAARAAAUUAAAAAAAAAAABACAAAAAAAAAADcDMyCQABQH0IAAAAAABAaYZmZmZmZgAAABsA#####wEAAAAAEAAAAAAAAAAAAAAAQAgAAAAAAAAAA3AzMwkAAUCAlAAAAAAAQG6mZmZmZmb#####AAAAAQAJQ0NlcmNsZU9BAP####8BAAAAAAAAAQAAACAAAAAhAAAAFwD#####AAhyb3RhdGlvbgACOTAAAAAMQFaAAAAAAAAAAAAiAP####8AAAAgAAAABQAAACMAAAACAP####8AAAAAARAAAUYAAAAAAAAAAABACAAAAAAAAAAACQAAAAAhAAAAJP####8AAAABAA1DRGVtaURyb2l0ZU9BAP####8AAAAAAA0AAAEAAAABAAAAIAAAACUAAAAXAP####8ABGxhbmcAAzEyNQAAAAxAX0AAAAAAAAAAACIA#####wAAACAAAAAFAAAAJwAAAAIA#####wAAAAABEAABRwAAAAAAAAAAAEAIAAAAAAAAAAAJAAAAACUAAAAoAAAAJwD#####AAAAAAANAAABAAAAAQAAACAAAAApAAAAFQD#####AQAAAAAQAAAAAAAAAAAAAABACAAAAAAAAAAACQABP+OPj#74bbIAAAAmAAAAAwD#####AAAAAAAAAAEAAAAgAAAAKwAAACkAAAAjAP####8AAAAAAAAAAAAFAAAALAAAAAQA#####wEAAAAAAAABAAAAJQAAAAUAAAABAAAAAAoA#####wEAAAAAEAAAAQAAAAEAAAAlAAAAJgAAAAcA#####wAAAC8AAAAuAAAACAD#####AQAAAAAQAAAAAAAAAAAAAABACAAAAAAAAAAACQACAAAAMAAAAAgA#####wEAAAAAEAAAAAAAAAAAAAAAQAgAAAAAAAAAAAkAAQAAADAAAAAlAP####8AAAAAAMAUAAAAAAAAwCIAAAAAAAAAAUEAAAAyEAAAAAAAAAAAAAAAAAAMAAAAAAAAAAAAAWYAAAAEAP####8BAAAAAAAAAQAAACkAAAAFAAAAAQAAAAAKAP####8BAAAAABAAAAEAAAABAAAAKQAAACoAAAAHAP####8AAAA1AAAANAAAAAgA#####wEAAAAAEAAAAAAAAAAAAAAAQAgAAAAAAAAAAAkAAQAAADYAAAAIAP####8BAAAAABAAAAAAAAAAAAAAAEAIAAAAAAAAAAAJAAIAAAA2AAAAJQD#####AAAAAADACAAAAAAAAMAmAAAAAAAAAAFDAAAAOBAAAAAAAAAAAAAAAAAADAAAAAAAAAAAAAFnAAAAHwD#####AQAAAAAQAAABAAAAAQAAACUAAAAgAAAAKQAAAAQA#####wEAAAAAAAABAAAAIAAAAAUAAAABAAAAAAcA#####wAAADoAAAA7AAAACAD#####AQAAAAAQAAAAAAAAAAAAAABACAAAAAAAAAAACQABAAAAPAAAAAgA#####wEAAAAAEAAAAAAAAAAAAAAAQAgAAAAAAAAAAAkAAgAAADwAAAAlAP####8AAAAAAMAIAAAAAAAAwCIAAAAAAAAAAUIAAAA+EAAAAAAAAAAAAAAAAAAMAAAAAAAAAAAAAWgAAAAmAP####8BAAAAAAAAAQAAAAYAAAAfAAAABgD#####AQAAAAAQAAABAAAAAQAAABgAAAAXAAAAFwD#####AAllcGFpc3NldXIAAjYwAAAADEBOAAAAAAAAAAAAFwD#####AA9sb25nUGV0aXRlc0dyYWQAATgAAAAMQCAAAAAAAAAAAAAXAP####8AEGxvbmdNb3llbm5lc0dyYWQAAjE1AAAADEAuAAAAAAAAAAAAFwD#####AA9sb25nR3JhbmRlc0dyYWQAAjI0AAAADEA4AAAAAAAAAAAAAQD#####AAAABgAAABQA#####wEAAAABEAAAAQAAAAEAAAAGAT#zMzMzMzMzAAAAFQD#####AQAAAAEQAAJLMQAAAAAAAAAAAEAIAAAAAAAAAAAFAAFAb8AAAAAAAAAAAEcAAAAEAP####8BAAAAAAAAAQAAAAgAAAAMP9MzMzMzMzMAAAAAIQD#####AQAAAAAQAAABAAAAAQAAAB8AAAAIAAAABwD#####AAAASgAAAEkAAAAIAP####8BAAAAABAAAAAAAAAAAAAAAEAIAAAAAAAAAAAFAAIAAABLAAAACAD#####AQAAAAAQAAAAAAAAAAAAAABACAAAAAAAAAAABQABAAAASwAAACIA#####wAAAAgAAAAMQHLAAAAAAAAAAAACAP####8BAAAAABAAAAAAAAAAAAAAAEAIAAAAAAAAAAAFAAAAAE0AAABO#####wAAAAIAGkNNYXJxdWVBbmdsZU9yaWVudGVEaXJlY3RlAP####8A#wAAAAJtYQADAAAAAUAfO+5l0eZEAAAATQAAAAgAAABPAAAAAAcA#####wAAAEcAAABAAAAACAD#####AQAAAAAQAAAAAAAAAAAAAABACAAAAAAAAAAABQACAAAAUQAAAAgA#####wEAAAAAEAAAAAAAAAAAAAAAQAgAAAAAAAAAAAUAAQAAAFEAAAAiAP####8AAAAGAAAACwEAAAAFAAAAHgAAAAxAVoAAAAAAAAAAAAIA#####wEAAAAAEAABSQAAAAAAAAAAAEAIAAAAAAAAAAAFAAAAAFMAAABUAAAAFwD#####AAplcGFpc3NldXIxAAI1NQAAAAxAS4AAAAAAAAAAABcA#####wAQbG9uZ1BldGl0ZXNHcmFkMQABOAAAAAxAIAAAAAAAAAAAABcA#####wARbG9uZ01veWVubmVzR3JhZDEAAjEyAAAADEAoAAAAAAAAAAAAFwD#####ABBsb25nR3JhbmRlc0dyYWQxAAIxOAAAAAxAMgAAAAAAAAAAAAEA#####wAAAAYAAAACAP####8BpKSkABAAAAAAAAAAAAAAAEAIAAAAAAAAAAAFAAAAAFUAAABaAAAAAwD#####AKSkpAADYXJjAAEAAAAGAAAAVQAAAFsAAAAEAP####8BpKSkAAAAAQAAAFUAAAAFAAAAVgEAAAAGAP####8BpKSkABAAAAEAAAABAAAAWwAAAFUAAAAHAP####8AAABeAAAAXQAAAAgA#####wGkpKQAEAACVzEAAAAAAAAAAABACAAAAAAAAAAABQACAAAAXwAAAAIA#####wGkpKQAEAAAAAAAAAAAAAAAQAgAAAAAAAAAAAUAAAAAYAAAAFoAAAADAP####8ApKSkAAAAAQAAAAYAAABgAAAAYQAAAAkA#####wEAAAAAEAACSjEAAAAAAAAAAABACAAAAAAAAAAABQABP8hHzOzEEzYAAABcAAAACgD#####AaSkpAAQAAABAAAAAQAAAFUAAABeAAAABAD#####AaSkpAAAAAEAAABVAAAACwMAAAAFAAAAVgAAAAxAAAAAAAAAAAEAAAAHAP####8AAABkAAAAZQAAAAgA#####wGkpKQAEAAAAAAAAAAAAAAAQAgAAAAAAAAAAAUAAQAAAGYAAAANAP####8AAABVAAAAZwAAAAIA#####wGkpKQAEAAAAAAAAAAAAAAAQAgAAAAAAAAAAAUAAAAAWwAAAGgAAAAOAP####8ApKSkAARwb2x5AAEAAAAFAAAAVQAAAGcAAABpAAAAWwAAAFUAAAAPAP####8A2NjYAAAAAAAFAAAAagAAAAYA#####wCkpKQAEAAAAQAAAAEAAABbAAAAaQAAAAYA#####wCkpKQAEAAAAQAAAAEAAABpAAAAZwAAAAYA#####wCkpKQAEAAAAQAAAAEAAABnAAAAVQAAAAkA#####wEAAP8AEAAAAAAAAAAAAAAAQAgAAAAAAAAAAAUAAT#nyXJN8Z0XAAAAXAAAAAkA#####wEAAP8AEAAAAAAAAAAAAAAAQAgAAAAAAAAAAAUAAT#nuF42fT8fAAAAYgAAABAA#####wEAAP8AAAABAAAAcAAAADIAAAAAAHAAAAACAAAAcAAAAHAAAAAQAP####8BAAD#AAAAAQAAAG8AAAAyAAAAAABvAAAAAgAAAG8AAABvAAAAEQD#####ANjY2AAAAAAABQAAAHEAAAByAAAABgD#####AaSkpAAQAAABAAAAAQAAAAYAAABjAAAABAD#####AaSkpAAAAAEAAABjAAAABQAAAFcBAAAABwD#####AAAAdAAAAHUAAAAIAP####8BpKSkABAAAAAAAAAAAAAAAEAIAAAAAAAAAAAFAAIAAAB2AAAABgD#####Ab29vQAQAAABAAAAAQAAAGMAAAB3AAAAEgD#####AL29vQAAAAAAeAAAAAxAZqAAAAAAAAAAAGMAAAAGAAAAYwAAAHQAAAB1AAAAdgAAAHcAAAB4AAAABAD#####Ab29vQAAAAEAAABjAAAABQAAAFkBAAAABwD#####AAAAdAAAAHoAAAAIAP####8Bvb29ABAAAAAAAAAAAAAAAEAIAAAAAAAAAAAFAAIAAAB7AAAABgD#####AYSEhAAQAAABAAAAAQAAAHwAAABjAAAABAD#####AYSEhAAAAAEAAABjAAAABQAAAFgBAAAABwD#####AAAAdAAAAH4AAAAIAP####8BhISEABAAAAAAAAAAAAAAAEAIAAAAAAAAAAAFAAIAAAB#AAAABgD#####AYSEhAAQAAABAAAAAQAAAIAAAABjAAAAEgD#####AISEhAAAAAAAgQAAAAxAQoAAAAAAAAAAAGMAAAAGAAAAYwAAAHQAAAB+AAAAfwAAAIAAAACBAAAAEgD#####AISEhAAAAAAAfQAAAAxAMwAAAAAAAAAAAGMAAAAGAAAAYwAAAHQAAAB6AAAAewAAAHwAAAB9AAAAEwD#####AARhbmcxAAAAVQAAAAYAAABjAAAAFAD#####AQAAAAAQAAABAAAAAQAAAAYBP#AAAAAAAAAAAAAVAP####8BAAAAABAAAUsAAAAAAAAAAABACAAAAAAAAAAABQABQG#AAAAAAAAAAACFAAAAEwD#####AAVhbmcxMQAAAIYAAAAGAAAAYwAAAAQA#####wEAAAAAAAABAAAAfAAAAAxAKAAAAAAAAAEAAAAHAP####8AAAB0AAAAiAAAAAgA#####wEAAAAAEAAAAAAAAAAAAAAAQAgAAAAAAAAAAAUAAgAAAIkAAAAWAP####8BAAAAAQAAAAAAigwAAAAAAAEAAAABAAAACwEAAAAFAAAAhwAAAAxAVoAAAAAAAAAAAAAAAAAAhAAAABIA#####wAAAAAAAAAAAIsAAAAMQDMAAAAAAAAAAABjAAAACwAAAGMAAAB0AAAAegAAAHsAAAB8AAAAhAAAAIcAAACIAAAAiQAAAIoAAACLAAAAFwD#####AAdhbmdzdXAxAAgxODAtYW5nMQAAAAsBAAAADEBmgAAAAAAAAAAABQAAAIQAAAAYAP####8ABGFiczEAAAAGAAAAVQAAAGAAAAAZAP####8AAAAGAAAABQAAAI4AAAACAP####8BAAAAABAAAAAAAAAAAAAAAEAIAAAAAAAAAAAFAAAAAGMAAACPAAAAFgD#####AQAAAAEAAAAAAJALAAAAAAABAAAAAgAAAAsBAAAABQAAAIcAAAAMQFaAAAAAAAAAAAAAAAAAAI0AAAASAP####8AAAAAAAAAAACRAAAADEAzAAAAAAAAAAAAYwAAAAYAAABjAAAAhAAAAIcAAACNAAAAkAAAAJEAAAAGAP####8ApKSkABAAAAEAAAABAAAAYAAAAGEAAAAbAP####8BAAAAABAAAAAAAAAAAAAAAEAIAAAAAAAAAAAFAAFAYtAAAAAAAEB+4zMzMzMzAAAAAgD#####AQAAAAAQAAFEAAAAAAAAAAAAQAgAAAAAAAAAAAUAAAAAIAAAAAoAAAAfAP####8BAAAAABAAAAEAAAABAAAAJQAAACAAAACVAAAAFQD#####AQAAAAAQAAAAAAAAAAAAAAA#4zMzMzMzMwAABQABQGk+QpYO3ckAAACWAAAAEwD#####AARhbmdBAAAAlQAAACAAAAAlAAAAFgD#####AQAAAABACAAAAAAAAD#wAAAAAAAAAAAAAACXDwAAAAAAAQAAAAEAAAAMAAAAAAAAAAAAAAACwrAAAAAAmAAAACAA#####wEAAAAAAAABAAAAAUBCW4kJK4+#AAAAlQAAACAAAAAlAAAAAB8A#####wEAAAAAEAAAAQAAAAEAAAApAAAAIAAAAJUAAAAVAP####8BAAAAABAAAAAAAAAAAAAAAD#jMzMzMzMzAAAFAAFAaT5Clg7dyQAAAJsAAAATAP####8ABGFuZ0IAAACVAAAAIAAAACkAAAAWAP####8BAAAAAEAIAAAAAAAAP#AAAAAAAAAAAAAAAJwPAAAAAAABAAAAAQAAAAwAAAAAAAAAAAAAAALCsAAAAACdAAAAIAD#####AQAAAAAAAAEAAAABQEJbiQkrj78AAACVAAAAIAAAACkAAAAAFwD#####AAVsYW5nQQAEYW5nQQAAAAUAAACYAAAAFwD#####AAVsYW5nQgAEYW5nQgAAAAUAAACdAAAABAD#####Ab29vQAAAAIAAAAGAAAACwMAAAAFAAAAAQAAAAxAAAAAAAAAAAAAAAAHAP####8AAAAqAAAAQAAAAAgA#####wEAAP8AEAAAAAAAAAAAAAAAQAgAAAAAAAAAAAUAAQAAAKMAAAAIAP####8BAAD#ABAAAAAAAAAAAAAAAEAIAAAAAAAAAAAFAAIAAACjAAAABwD#####AAAAJgAAAEAAAAAIAP####8BAAD#ABAAAAAAAAAAAAAAAEAIAAAAAAAAAAAFAAEAAACmAAAACAD#####AQAA#wAQAAAAAAAAAAAAAABACAAAAAAAAAAABQACAAAApgAAABsA#####wEAAP8AEAAAAAAAAAAAAAAAQAgAAAAAAAAAAAUAAUB02AAAAAAAQHtjMzMzMzMAAAAbAP####8BAAD#ABAAAAAAAAAAAAAAAEAIAAAAAAAAAAAFAAFAdNgAAAAAAEB7YzMzMzMzAAAAHQD#####AAAA#wAQAAABAAAAAgAAAKkAAACqAAAAAB4A#####wAAAKsAAAACAP####8AAAAAABAAAAAAAAAAAAAAAEAIAAAAAAAAAAAJAAAAACAAAACsAAAAGwD#####AQAAAAAQAAAAAAAAAAAAAABACAAAAAAAAAAABQABQITYAAAAAABAPeFHrhR64v####8AAAACAAZDSW1hZ2UA#####wAAAAAAQDYAAAAAAABANv#######gAFaW1hZ2UAAACuDQAAAAAAAQAAAAIAAAAMAAAAAAAAAAAAAAAAOAAAADMAABG8iVBORw0KGgoAAAANSUhEUgAAADgAAAAzCAYAAADCQcvdAAAAAXNSR0IArs4c6QAAAARnQU1BAACxjwv8YQUAAAAJcEhZcwAADsQAAA7EAZUrDhsAABFRSURBVGhD5VoNdNX1eX7u#X#c7498ERJCEhJIgAABDAVca6kOplQ2pZurncfN1lnPqt3qOdZOWz2duuo6YWoPrfYDxSmylo2qPUyqguJBCQqKQIhAyBfk+yY39#t7z#u78Zyux4RE49nmXrzecG#u##97fu#7Ps#z#i4WuP05lNYDyQQ+VWHagL5WWFC7Mvfy229irgNIZcfe#D8ehhU4HQMuu2gVAVYsy3WcOYxKc+zdT0l0JoGq2uUgVmYuo177VMUHmFQGT79#GLUs0emMYDqJ3nAc+9sCODMURUcgCosF8NoMVBbYsXpOIRpneuAxTOj5fZ7WOMMSnVu3fPoB3nfgOF45HMCbrwbhCOootrsw07DDlrIiEskgjgyi1gwGLQkEcxGUL3fisyvcuHtdA+q8nrGrfPyYVoADZOAfvNKKzU+1YUVkJmpMF2wODeFoGqlcnrl00wKHXUMuk0MymYXLoSOXBqLRDLJaDvvRg9lLTTz+1UY0lRapz3ycmDaAz7Z24ZuPvAvfSS#m23wwmIQ0QSUSOTgcVpgEhpwFCYKKxbKqTLO5HJ8tMAwyHinP0K3IhYH+WBItrgA+f5UbT123Eg6LNnaXqccHAD9W8f#5U6#jpluOYfWpKizw+5AyWX7s7ng8C52LJw6kmbEwsySZyvAFYiUgC+ymVWVUs1oxNJRExJpGkV#HmlwZhv#DgOfmF9AcGB6700ePjwQwixyc334eXdsNrPXNQtqTRiKTgdutwe3U4PcbKC02MaPEhN2mI5vNweXSUEwAumZhVjXY7FaU8P3yMhNVsx3weXVuBskpk4SZ0XDluWqs#tpr2PV+19hdP1pMGWAgkYBx53NoOlyGYpsNKS2DVJqpYqvFElmkUjnYmB0LgYRCaWYzg8BwCv907zzseKqRgC2KbCKRNM60xdB6KoLevjiCQblOhllnOaezCKRTuHSkEtffcRTPHj07dvepx5QBLvzBK7gxWMddtyNjJSpikz6y6izHZA6hcBpd3XG0t8cQYVkahgUej4HhkZT6vMulo3aOAwWFJooLDdiZSYtmhcZ2s4wtR3jJauGF3Vmsy1Xi2m8fxav9A+q9qcaUAH7piddRe8iPkUAKFgJysBwdTinBLDLMopVZs9mscLl1RSLSiyPBNPr7EujpzXvdltYwzrRHEWDfxfh+mkyaokdMMvs59qjbZUVFhR2zyu0oZpnbCnP4eul8rP3eQZwYHlXXmEpMGuCTJzuxd2cEMwwXRmJp9A4kERxJswxT8BBQZYUDc2scmFfjxOJFLtTwZ+kr6Uth0u7zeYA9fLaxByWzI8EUQbLxmCyLlb3JzdFIQEFuygCv3z+QwCA3szcex9XJSvz1L5rVNaYSkwIoruSOH7+Hz0XKMZJMIUNmZLLUImWx6UwWA8yIZCnAUjx9Oore3qTqx1AoQ7nQ0NNDc8jIZAlsNIUR#p6VoDQ+RDs0XlAyGKeUxBPis8i0BMwCVj1ujVsQeM2Gh5pb1XUmG5MC+N09LagZKIKzAigq1FFaYmMfOdG42IPqShvKS+3w+3QFvJ87P0oASZZdiEQi2auqtMPtyd9qXq0T5TNNOFnakn0p7QxJJUWdlJ4VrZSMilbyiVnN62U8m8Ei04fvPNairjPZmJTQ6zc8hzVtFUiYbBhuuESWmRChk0VImBRrL0G6nLRk0Sz7Ebj165VoWu5hqRpqwVZR+bGQvjtwcASPbz3Hv+W4KWIOMtwQq2Je7pXKnmgn3R31k1Jj6DiVDuKme8txe1Nd#kLjxKSF#p79x7AyWArNK33CkuEarTr7iuXpJCMWkQ3LSQjl5fl5y+838czWRdi1fSn6SC5fvfkElqx8A1#c+A57K4WCin1Y0HQAX7ruKAU+jad#vhh3fGsOZswwsIQVIXjcHp19baisl8+0Yw5Zt4YV4y7Q8HnvTGzb3SGqNKm4IMA9BwMottjoLQmIrOnmjU0SgWQyxdIKswwHB9l3bVHVk1s21+OlvUNYv#EIntzeo0ijbq6TJGRDb38SDQtcaKh38eMW#MuWDlyx8TCzl8YP76tT8hKVHlRlytKlsgwFkiQakho3a3Awic7RKAbeAg6en5xsXABgDu++yanAECaU7ImHpJaxDItI4aV0IgUFBteSVVq38+lG3HrbSdzx3dNKyMVUS9bEl8ZZktKfUqqyOSUlOmbTwYizufnvWrDtmfPY8eQSvi8+iSXP3tWpr0JQKRnuaAB8dEJ2ysiMlAvPvzcNALe914WyrBdJZkp21kXKlywWFRmoUlplg51Zkyy9uGs5Hnq0A0dPhLFgvkuRiHI0#CNTRZhs2nIyws3Ja2SG7ZwWcuHznGonnvllL7b8tAu##fVF8FF26utcmFNlR4FPU20hGxtjb+e44rKMHU#v7Rxb5cQxIcAXWnpQp3lgc5NAPDLqACGWU3d3AscJpKU1gp6+JDZumIHW9yN45t966U5MlW3JgBgBnRsgTkdA9bOUKQYqu1Jy0oNCKMKeLmbq0R93Kg2srbXjud8MoPmtINo64sqwy2Ri4WqlKmy0PbFBkZILx4QAezpH4QgbGOWCwmHJYoYmWUMhpWLWLJEGA3He8Bay5cM#6URxkY7gaIaal9fDYYr0KK1bjmY7QDB7XhplVk34C3RVCX5mx0tCkSrw8Fnci1SBXE8AFdLKFfIeIkFSqrNZNTKJWCgbZsyBw0MjYysdPyYEGE#piLOMZIFcl7qJeMZQmHTORfm9Ghrmu5WZlp2vm+tGXZ1Dec26uS5FLNWVDoLwoGSGE#OXnEd5RRpeZ4FyOAWcOmTSEBcjOijtufe1gPKrM2eYzHCKWaeBIDn10UScY+VYFcHl4M8a6AxGxlY6fowLMMZxOxykBNGl8D8SBXWK3nGUvSSPM2ejeOudkGLO9o4Yjh2P4MjRUT6H0d4Vp5NJIEEn4#faMHdeGn#x#T#DNXddi43fuwxR7XV0dVnRdT6u#GqU1i#MHk#xRrJRNop703IvGpd48LmL#biI8nHl+mL85XVlWLbIg3r2+OJKL04NRMdWO37QMn94cF#lf3T6AjKHXvbPz7c0YClvJiEAMtx9KS35vfvvqVV#l97r6Izjt3sDGBhMoKPDiiWXvAh#+XFEAmVweAP4wjVbsf0ftrEKehTZlJXYcdUfl6jJfuF8pxqE7#5OrbrP78em8x1obh5CrxbDKsM99ur4MS5A8Zo63#WR#pM2OgtaqnPUqULKQmCYQymJ5OzZmNKwtZcV4c1DQZxlJqXkJKSHNCsJx0ij5#jl6Du1C57iM0iEyrF7622IJPuQ4mdlI8#3xvGTn3YrMpFrnjhyMW76RouSFSclqW6eC59p8mKImyzzYyZjQYyyI+u7UExo1VY+sA81h0oQNVPQ2Sc2sqGQxhB7TkLMsPTJ0YOrseGad0j7ubwB50Ngyugkz7m0xrbxQnd3wszNpJmmiOeiSgNlmpAeFPKQKOIG3v33Nbjp1hZlwEWSKmfZlBT1krHbuAGIA8f1ETy8qR5XVpepz#1+TMqqOWzSD1ZkubMy1#VyfImRSeVIQoZVYUKJru4YLr2kQL2nhl8uTOeC5KBJzmISKYp9egiJsJ8LTdDiJdRGSLJFIqS35bPD3LgvXlGCTlZKD7M6xEppORnG87sH8e+#7sfRY+G8TyUnhLjpNX6Xuv9EMWEGv7yjGbbX6AFpcuPJjAIpi0mqCUDYj9nhIms5+z3+yEJcfe27zHAKWSZY3pNjCyl12X11cqbyaUGa5WXQHVmtHHAp6jkOzAlOU7qew7NPNNK3HkaK6B12nSXONhGZsGnqtEA8qBnT8KK3HV0#ukKt88NiUhn88vIK7GnrQ1d7HH0DzAIzIm7Gw0dxsaF0ay4lQWa+l#cF8NgjC5QvFTaU2U42QZxKmmwqGppkaUa4SBlsZ7PsLvlsgRqdxKbIiPXYIw3Y#sselVm#V#qXVcDPiIc9SSMRjpDBpZKCCZiF+ZK+UEwI8Kp55ehFCHFapCi1L8nsCRgZZGXRMdL7CImikMK96UcdauDdt7uJwmySCGR2NDglmGojBOzX#moWMyy+EkoaRFKEnAbZx7t2LMVB#nzvg20qq5rGfPP3pCRDQfYp8US5jgTl5EwshBsvnTO2yoljQoASa#+kBEWlOgroXnQyp#SO9J6LDwEaoFuR0zH5+W9otPe8PIQXfrVUgRGSkMldyEfKrJqDbz0nC5ESyUYwlMbVG0qUj93JHvvhwx2UIa#qY9kgMRfiluayf2pYKXIMWchpZtATw2ULS8ZWOHFccOB98I0WPHH3ACp0d#4Ynlsph0xy1CAEJKUn4CQroplCFAvpbh76x3mornJg#4EgDjaPoJui#q1bqrB5SycW0kgvbnBj9Uq#yrpkX9hYznCirJJz5+LqaFHYNR#5ipFnM2QisKEfh+9cl39rnJjS0b33G8#jD05VQPdZSAyAkxonh7bDnNMMOVIQ1mRm5Y8sQ0jkHP2o#Lzu0kIsXeJVWV9BdyIGWsr07SNB7NjZi1QC8NDyyfmMWEIhJBmLZnHQddgtysxLSOUkwxY0x#pw#4P1+Ep9hXp9vJgSwO#va8G#PtCPRW4f70R9Y6kKk6ZJOk6XkA7Zjq8JeBF66SnRN6qDKmE5VxFT#ZtfLUPj6jfIxGRICribn9XUZ5h9PlsULUu#SeZkM#MbJnop93Dyl#+ztBMD#7xerWuimPSRhcRda+qRWBJBapDCzhKUsUcWHmIZ9ZPhus4llMN4j6Rxtp3uhkQUZpZk8XOqHVhGe7eGjClx+dpirFrlZTZ9NON2TgxCQjayJqXALhpKU0BgoqViEWWYlnk0PpLF#sgANt+wQF1nsjEpgPIF5S++uQK7C7sww2XnAvKORU6lpTTVJCC#SNqLjx3gyvwoDCojUZBM297JLWVcvNKnZj8x1EJC0lcKEEvTxtKXKUMckp3lKWUtE0yRjGUU9vo#suK6hsmx5wcxKYASf1hRghu+UsLUB+GzmygpMlXZyPHhwgUchTgeLVjg5sK4KAFuaiSLBDo5Wcjh7QCzLnGiJYJjJyTbIXUGM8r5UQ55JVtyJCIOSMrZSw8sG+jlPar9TrwzfwDbrl+prjGVmDRAiZ#96WdgW8u5bDiK891JdJPtjhwNofntEN49FsIJLlzYTxxPKEj#ysWq02pmqL4+b6tmUtjFiMvEYCEg+TpNvnBJJmXSzyBG6xYhM8f4s5zfZIMWbAqcwEu3X4xSh11dYyoxJYASr#ztGgTXjyJLn1ozy4kyLjj#sKFhoYsznBsrm3wEwlJm+aWFaNi3YtYlxNZJaaqpg6wkzClAs3Q8QjZS6tm8F0ChbmJv+Tm8cF8TlhX51eenGpNi0Q+LJQ#ugUafWq15ELHIsYQcPuUUQyaYDcEjpCiRoWyUlppYvsynzm5OtkbZXyIqEtwGyaQCl5chm5y#pqzYU9mJF+9ZhXVVpeo3pxJTkonx4rbdR7D5Z+dwvVaLNGfG4agcKslXZJqyWfTo+W+QmBGZFmSyMNibJl83KCsCSX0#QYMgxlq+PLUnNRwaGUJf#Qj23bUG1U6nutdUY0oyMV5sumIZXn10FQ7UtuN0NoiacgcJSKf+ZUka+eMNASeDrICR6d#jsqqDKwElJ2SildFRjksDWbR0hbAjfRarbnSh#f71Hxnc78bHyuDvxhNH2nD71uOwn3SjOu7JfzNEkmEbKUtn8JHkyBVnCQvp6CxDJ1lSpoPBXAKHPP3YcHkJHtjYgPnT8M9JpqVEPyyePdWJnc3deL2Z483JOMysHR6rCZ98Bczb6bxP1JrGUCyKeEkOjUtdWNPox51faADpJ3+RaYhPDOAHkQCnhXgSp4aiODMYRQelRcrUZzNQVeTAigo#XPzZp9H7fQLxiQP8n47#RjLG9FXG#5r4ANP#g38v+qn+F7+t+C+KNzkcQGVP3AAAAABJRU5ErkJgggAAAAX##########w=='
      j3pCreeSVG(stor.lesdiv.zonefig, { id: svgId, width: 720, height: 400 })
      stor.lesdiv.zonefig.style.border = 'solid black 1px'
      stor.mtgAppLecteur.removeAllDoc()
      stor.mtgAppLecteur.addDoc(svgId, txtFigure, true)
      stor.mtgAppLecteur.giveFormula2(svgId, 'rotation', stor.rotation)
      stor.mtgAppLecteur.giveFormula2(svgId, 'lang', stor.deplacement)
      stor.mtgAppLecteur.setText(svgId, '#A', stor.A, true)
      stor.mtgAppLecteur.setText(svgId, '#B', stor.B, true)
      stor.mtgAppLecteur.setText(svgId, '#C', stor.C, true)
      stor.mtgAppLecteur.calculateAndDisplayAll(true)
    }
    remet()
    if (stor.encours === 'g') {
      stor.mtgAppLecteur.setVisible(svgId, '#image', false, true)
      stor.mtgAppLecteur.setVisible(svgId, '#pt', false, true)
      stor.mtgAppLecteur.setVisible(svgId, '#ma', false, true)
      const ttt = addDefaultTable(stor.lesdiv.zonedrep, 3, 1)
      ttt[1][0].innerHTML = 'ou'
      stor.ClikInt = j3pAjouteBouton(ttt[0][0], gradInt, { className: 'ok', value: 'Intérieures' })
      stor.ClikExt = j3pAjouteBouton(ttt[2][0], gradExt, { className: 'ok', value: 'Extérieures' })
    }
    if (((stor.encours === 'm') || (stor.encours === 'g')) && !stor.pose) {
      stor.pose = true
      stor.ptDep = stor.mtgAppLecteur.getPointPosition(svgId, '#p32')
      stor.mtgAppLecteur.setPointPosition(svgId, '#p6', stor.ptDep.x, stor.ptDep.y, true)
      stor.mtgAppLecteur.calculate(svgId)
      if (stor.afaire[0] !== 'p') stor.choixA = j3pGetRandomBool()
      let ty
      if (stor.choixA) {
        ty = stor.mtgAppLecteur.valueOf(svgId, 'langA') + 90
      } else {
        ty = stor.mtgAppLecteur.valueOf(svgId, 'langB') - 90
      }
      stor.mtgAppLecteur.giveFormula2(svgId, 'ang', ty)

      stor.mtgAppLecteur.calculateAndDisplayAll(true)

      // prends coordonnées des 4 points
      const pt1 = stor.mtgAppLecteur.getPointPosition(svgId, 167)
      const pt2 = stor.mtgAppLecteur.getPointPosition(svgId, 164)
      stor.ptDep = stor.mtgAppLecteur.getPointPosition(svgId, 'B')
      stor.xmin = Math.min(pt1.x, pt2.x, stor.ptDep.x)
      stor.ymin = Math.min(pt1.y, pt2.y, stor.ptDep.y)
      stor.xmax = Math.max(pt1.x, pt2.x, stor.ptDep.x)
      stor.ymax = Math.max(pt1.y, pt2.y, stor.ptDep.y)
      /// calcul le ratio x , calcul le ratio y, prend le min
      stor.taillex = stor.xmax - stor.xmin + 30
      stor.tailley = stor.ymax - stor.ymin + 30
      const ratio1 = 370 / stor.tailley
      const ratio2 = 600 / stor.taillex
      stor.ratio = Math.min(ratio1, ratio2)
      /// calcul coordonnées du centre
      // calclu depx et depy pour que B arrive au centre
      stor.mil = { x: (stor.xmin + stor.xmax) / 2, y: (stor.ymin + stor.ymax) / 2 }
      stor.depx = 300 - stor.mil.x
      stor.depy = 195 - stor.mil.y
      stor.lanceEtape = 'A'
      RapCorrige('mm')
      LanceAnimeZoom()
    }
    if (stor.encours === 'm') {
      stor.mtgAppLecteur.setVisible(svgId, '#image', false, true)
      stor.mtgAppLecteur.setVisible(svgId, '#pt', false, true)
      stor.mtgAppLecteur.setVisible(svgId, '#ma', false, true)
      if (!stor.zommfait) { stor.oublipa = true } else { faisM() }
      stor.larep.focus()
    }
  }

  function recentre () {
    let t = stor.mtgAppLecteur.getPointPosition(svgId, '#p32')
    const deltax = 336.5 - t.x
    const deltay = 200 - t.y
    stor.mtgAppLecteur.translatePoint(svgId, '#p6', deltax, deltay, true)
    stor.mtgAppLecteur.translatePoint(svgId, '#p32', deltax, deltay, true)
    stor.mtgAppLecteur.translatePoint(svgId, '#p33', deltax, deltay, true)
    stor.mtgAppLecteur.updateFigure(svgId)
    t = stor.mtgAppLecteur.getPointPosition(svgId, '#p6')
    if ((t.x < 20) || (t.x > 600) || (t.y > 340) || (t.y < 20)) {
      stor.mtgAppLecteur.translatePoint(svgId, '#p6', 50 - t.x, 330 - t.y, true)
    }
    stor.mtgAppLecteur.updateFigure(svgId)
    stor.captured = stor.captured2 = false
    remet()
  }
  function remet () {
    if (stor.encours === 'p') {
      stor.mtgAppLecteur.addCallBackToSVGListener(svgId, 'mousemove', function (ev) {
        mousemove(ev)
      })
      stor.mtgAppLecteur.addCallBackToSVGListener(svgId, 'mouseup', function () {
        stor.captured = false
        stor.captured2 = false
      })
      stor.mtgAppLecteur.addCallBackToSVGListener(svgId, 'mousedown', function (ev) {
        // if (stor.encours !== 'p' && stor.encours !== '2') return
        stor.captured2 = true
        stor.x = ev.clientX
        stor.y = ev.clientY
        ev.preventDefault()
      })
      stor.mtgAppLecteur.addEventListener(svgId, '#arc', 'mousedown', function (ev) {
        // app.setLineStyle(svgId, 4, 1, 5, true)
        stor.captured = true
        stor.x = ev.clientX
        stor.y = ev.clientY
        ev.preventDefault()
      })
      stor.mtgAppLecteur.addEventListener(svgId, '#poly', 'mousedown', function (ev) {
        // app.setLineStyle(svgId, 4, 1, 5, true)
        stor.captured = true
        stor.x = ev.clientX
        stor.y = ev.clientY
        ev.preventDefault()
      })
      stor.mtgAppLecteur.addEventListener(svgId, '#arc', 'mouseover', function () {
        stor.mtgAppLecteur.getDoc(svgId).defaultCursor = 'pointer'
        stor.mtgAppLecteur.setVisible(svgId, '#pt', true, true)
        stor.mtgAppLecteur.setVisible(svgId, '#ma', true, true)
      })
      stor.mtgAppLecteur.addEventListener(svgId, '#poly', 'mouseover', function () {
        stor.mtgAppLecteur.getDoc(svgId).defaultCursor = 'pointer'
        stor.mtgAppLecteur.setVisible(svgId, '#pt', true, true)
        stor.mtgAppLecteur.setVisible(svgId, '#ma', true, true)
      })
      stor.mtgAppLecteur.addEventListener(svgId, '#arc', 'mouseout', function () {
        stor.mtgAppLecteur.getDoc(svgId).defaultCursor = null
        stor.mtgAppLecteur.setVisible(svgId, '#pt', false, true)
        stor.mtgAppLecteur.setVisible(svgId, '#ma', false, true)
      })
      stor.mtgAppLecteur.addEventListener(svgId, '#poly', 'mouseout', function () {
        stor.mtgAppLecteur.getDoc(svgId).defaultCursor = null
        stor.mtgAppLecteur.setVisible(svgId, '#pt', false, true)
        stor.mtgAppLecteur.setVisible(svgId, '#ma', false, true)
      })
      stor.mtgAppLecteur.addEventListener(svgId, '#arc2', 'mouseover', function () {
        stor.mtgAppLecteur.setVisible(svgId, '#pt', true, true)
        stor.mtgAppLecteur.setVisible(svgId, '#ma', true, true)
      })
      stor.mtgAppLecteur.addEventListener(svgId, '#arc2', 'mouseout', function () {
        stor.mtgAppLecteur.setVisible(svgId, '#pt', false, true)
        stor.mtgAppLecteur.setVisible(svgId, '#ma', false, true)
      })
      stor.mtgAppLecteur.addEventListener(svgId, '#image', 'click', function () {
        // app.setLineStyle(svgId, 4, 1, 5, true)
        recentre()
      })
      stor.mtgAppLecteur.addEventListener(svgId, '#image', 'mouseover', function () {
        stor.mtgAppLecteur.getDoc(svgId).defaultCursor = 'pointer'
      })
      stor.mtgAppLecteur.addEventListener(svgId, '#image', 'mouseout', function () {
        stor.mtgAppLecteur.getDoc(svgId).defaultCursor = null
      })
    }
  }
  function LanceAnimeZoom () {
    if (stor.encours === 'p') return
    let pt1, pt2, pt3, DEPX, DEPY
    if (stor.zommfait) {
      me.afficheBoutonValider()
      return
    }
    if (stor.afaire[0] !== 'p') {
      pt1 = stor.mtgAppLecteur.getPointPosition(svgId, '#p6')
      pt2 = stor.mtgAppLecteur.getPointPosition(svgId, '#p32')
      pt3 = stor.mtgAppLecteur.getPointPosition(svgId, '#p33')
      stor.mtgAppLecteur.setPointPosition(svgId, '#p6', pt1.x + stor.depx, pt1.y + stor.depy, true)
      stor.mtgAppLecteur.setPointPosition(svgId, '#p32', pt2.x + stor.depx, pt2.y + stor.depy, true)
      stor.mtgAppLecteur.setPointPosition(svgId, '#p33', pt3.x + stor.depx, pt3.y + stor.depy, true)
      stor.mtgAppLecteur.calculate(svgId)
      stor.mtgAppLecteur.zoom(svgId, 300, 195, stor.ratio, true)
      me.afficheBoutonValider()
      stor.mtgAppLecteur.giveFormula2(svgId, 'distee', 0.5 / stor.ratio)
      stor.zommfait = true
      if (stor.oublipa) faisM()
      return
    }
    switch (stor.lanceEtape) {
      case 'A':
        pt1 = stor.mtgAppLecteur.getPointPosition(svgId, '#p6')
        pt2 = stor.mtgAppLecteur.getPointPosition(svgId, '#p32')
        pt3 = stor.mtgAppLecteur.getPointPosition(svgId, '#p33')
        DEPX = 0
        DEPY = 0
        if ((Math.abs(pt1.x - (stor.ptDep.x + stor.depx)) < 2) && (Math.abs(pt1.y - (stor.ptDep.y + stor.depy)) < 2)) {
          stor.lanceEtape = 'Z1'
          setTimeout(LanceAnimeZoom, stor.tempodeb)
          return
        }
        if (Math.abs(pt1.x - (stor.ptDep.x + stor.depx)) > 2) {
          if (pt1.x < stor.ptDep.x + stor.depx) DEPX = 2
          if (pt1.x > stor.ptDep.x + stor.depx) DEPX = -2
        }
        if (Math.abs(pt1.y - (stor.ptDep.y + stor.depy)) > 2) {
          if (pt1.y < stor.ptDep.y + stor.depy) DEPY = 2
          if (pt1.y > stor.ptDep.y + stor.depy) DEPY = -2
        }
        stor.mtgAppLecteur.setPointPosition(svgId, '#p6', pt1.x + DEPX, pt1.y + DEPY, true)
        stor.mtgAppLecteur.setPointPosition(svgId, '#p32', pt2.x + DEPX, pt2.y + DEPY, true)
        stor.mtgAppLecteur.setPointPosition(svgId, '#p33', pt3.x + DEPX, pt3.y + DEPY, true)
        stor.mtgAppLecteur.calculate(svgId)
        setTimeout(LanceAnimeZoom, stor.tempo)
        break
      case 'Z1':
        stor.mtgAppLecteur.zoom(svgId, 300, 195, Math.cbrt(Math.cbrt(stor.ratio)), true)
        stor.lanceEtape = 'Z2'
        stor.mtgAppLecteur.calculate(svgId)
        setTimeout(LanceAnimeZoom, stor.tempo)
        return
      case 'Z2':
        stor.mtgAppLecteur.zoom(svgId, 300, 195, Math.cbrt(Math.cbrt(stor.ratio)), true)
        stor.lanceEtape = 'Z3'
        stor.mtgAppLecteur.calculate(svgId)
        setTimeout(LanceAnimeZoom, stor.tempo)
        return
      case 'Z3':
        stor.mtgAppLecteur.zoom(svgId, 300, 195, Math.cbrt(Math.cbrt(stor.ratio)), true)
        stor.lanceEtape = 'Z4'
        stor.mtgAppLecteur.calculate(svgId)
        setTimeout(LanceAnimeZoom, stor.tempo)
        return
      case 'Z4':
        stor.mtgAppLecteur.zoom(svgId, 300, 195, Math.cbrt(Math.cbrt(stor.ratio)), true)
        stor.lanceEtape = 'Z5'
        stor.mtgAppLecteur.calculate(svgId)
        setTimeout(LanceAnimeZoom, stor.tempo)
        return
      case 'Z5':
        stor.mtgAppLecteur.zoom(svgId, 300, 195, Math.cbrt(Math.cbrt(stor.ratio)), true)
        stor.lanceEtape = 'Z6'
        stor.mtgAppLecteur.calculate(svgId)
        setTimeout(LanceAnimeZoom, stor.tempo)
        return
      case 'Z6':
        stor.mtgAppLecteur.zoom(svgId, 300, 195, Math.cbrt(Math.cbrt(stor.ratio)), true)
        stor.lanceEtape = 'Z7'
        stor.mtgAppLecteur.calculate(svgId)
        setTimeout(LanceAnimeZoom, stor.tempo)
        return
      case 'Z7':
        stor.mtgAppLecteur.zoom(svgId, 300, 195, Math.cbrt(Math.cbrt(stor.ratio)), true)
        stor.lanceEtape = 'Z8'
        stor.mtgAppLecteur.calculate(svgId)
        setTimeout(LanceAnimeZoom, stor.tempo)
        return
      case 'Z8':
        stor.mtgAppLecteur.zoom(svgId, 300, 195, Math.cbrt(Math.cbrt(stor.ratio)), true)
        stor.lanceEtape = 'Z9'
        stor.mtgAppLecteur.calculate(svgId)
        setTimeout(LanceAnimeZoom, stor.tempo)
        return
      case 'Z9':
        stor.mtgAppLecteur.zoom(svgId, 300, 195, Math.cbrt(Math.cbrt(stor.ratio)), true)
        stor.lanceEtape = 'F'
        stor.mtgAppLecteur.calculate(svgId)
        setTimeout(LanceAnimeZoom, stor.tempo)
        break
      case 'F':
        stor.zommfait = true
        me.afficheBoutonValider()
    }
  }
  function faisM () {
    me.afficheBoutonValider()
  }
  function gradInt () {
    stor.capInt = true
    stor.capExt = false
    stor.mtgAppLecteur.setColor(svgId, 140, 0, 0, 0, true)
    stor.mtgAppLecteur.setColor(svgId, 146, 255, 0, 0, true)
    stor.mtgAppLecteur.updateFigure(svgId)
  }
  function gradExt () {
    stor.capExt = true
    stor.capInt = false
    stor.mtgAppLecteur.setColor(svgId, 140, 255, 0, 0, true)
    stor.mtgAppLecteur.setColor(svgId, 146, 0, 0, 0, true)
    stor.mtgAppLecteur.updateFigure(svgId)
  }

  function distangle (a1, a2) {
    const g = Math.max(a1, a2)
    const p = Math.min(a1, a2)
    return Math.min(g - p, 360 - g + p)
  }
  function mousemove (ev) {
    if (stor.mtgAppLecteur.getDoc(svgId).pointCapture) return
    if (me.etat !== 'correction') return
    if (stor.captured) {
      const newx = ev.clientX
      const newy = ev.clientY
      const deltax = newx - stor.x
      const deltay = newy - stor.y
      stor.x = newx
      stor.y = newy
      stor.mtgAppLecteur.translatePoint(svgId, '#p6', deltax, deltay, true)
      stor.mtgAppLecteur.updateFigure(svgId)
      RapCorrige()
      return
    }
    if (stor.captured2 && (me.etat !== 'correction') && stor.encours === 'p') {
      const newx = ev.clientX
      const newy = ev.clientY
      const deltax = newx - stor.x
      const deltay = newy - stor.y
      stor.x = newx
      stor.y = newy
      stor.mtgAppLecteur.translatePoint(svgId, '#p6', deltax, deltay, true)
      stor.mtgAppLecteur.translatePoint(svgId, '#p32', deltax, deltay, true)
      stor.mtgAppLecteur.translatePoint(svgId, '#p33', deltax, deltay, true)
    }
  }
  function dist (p1, p2) {
    return Math.sqrt((p1.x - p2.x) * (p1.x - p2.x) + (p1.y - p2.y) * (p1.y - p2.y))
  }
  function RapCorrige (cool) {
    let lacool = [200, 200, 200]
    if (cool === true) lacool = [0, 138, 115]
    if (cool === false) lacool = [214, 71, 0]
    if (cool === 'jk') lacool = [70, 70, 200]
    stor.mtgAppLecteur.setColor(svgId, 115, lacool[0], lacool[1], lacool[2], true)
    stor.mtgAppLecteur.setColor(svgId, 107, lacool[0], lacool[1], lacool[2], true)
    stor.mtgAppLecteur.setVisible(svgId, 162, (cool === false) || (cool === 'jk') || (cool === 'mm'), true)
    if (cool === 'mm') {
      stor.mtgAppLecteur.setVisible(svgId, '#p6', false, true)
    }
  }
  function yaReponse () {
    if (stor.encours === 'p') return true
    if (stor.encours === 'g') {
      if (!(stor.capExt || stor.capInt)) stor.ClikInt.focus()
      return (stor.capExt || stor.capInt)
    }
    if (stor.encours === 'm') {
      if (stor.larep.reponsechap()[0] === '') {
        stor.larep.focus()
        return false
      }
      return true
    }
  }
  function isRepOk () {
    stor.errCentre = false
    stor.errCentreP = false
    stor.err0 = false
    stor.err0P = false
    stor.err0I = false
    stor.errGraduations = false
    stor.errM = false
    stor.errMEcrit = false
    stor.errMEcritkoi = ''
    stor.errMG = false

    if (stor.encours === 'p') {
      const pt = stor.mtgAppLecteur.getPointPosition(svgId, 'B')
      const pt3 = stor.mtgAppLecteur.getPointPosition(svgId, '#p32')
      if (dist(pt, pt3) > 2) {
        if (dist(pt, pt3) < 4) {
          stor.errCentreP = true
          return false
        }
        stor.errCentre = true
        return false
      }
      const a1 = stor.mtgAppLecteur.valueOf(svgId, 'langA')
      const a2 = stor.mtgAppLecteur.valueOf(svgId, 'langB')
      let a3 = stor.mtgAppLecteur.valueOf(svgId, 'ang') - 90
      let a4 = a3 + 180
      if (a4 < -180) a4 += 360
      if (a4 > 180) a4 -= 360
      if (a3 < -180) a3 += 360
      if (a3 > 180) a3 -= 360
      if (Math.abs(a1 - a3) < 0.9) {
        stor.choixA = true
        return true
      }
      if (Math.abs(a2 - a4) < 0.9) {
        stor.choixA = false
        return true
      }
      if (Math.abs(a1 - a3) < 3) {
        stor.err0P = true
        return false
      }
      if (Math.abs(a2 - a4) < 3) {
        stor.err0P = true
        return false
      }
      if ((Math.abs(a1 - a4) < 1) || (Math.abs(a2 - a3) < 1)) {
        stor.err0I = true
        return false
      }
      stor.err0 = true
      return false
    }
    if (stor.encours === 'g') {
      return (stor.choixA === stor.capExt) || (stor.deplacement === 180)
    }
    if (stor.encours === 'm') {
      const kl = stor.larep.reponse()
      const ok = estMesureangle(kl)
      if (!ok.good) {
        stor.larep.corrige(false)
        stor.errMEcrit = true
        stor.errMEcritkoi = ok.adire
        return false
      }
      if (ok.val === stor.deplacement) return true
      stor.larep.corrige(false)
      if ((stor.deplacement < 90) !== (ok.val < 90)) {
        stor.errMG = true
        return false
      }
      stor.errM = true
      return false
    }

    stor.lesdiv.explications.classList.remove('explique')
    stor.lesdiv.solution.classList.remove('correction')

    return true
  }
  function desactivezone () {
    stor.mtgAppLecteur.setVisible(svgId, '#image', false, true)
    if (stor.encours === 'p') {
      stor.mtgAppLecteur.removeEventListener(svgId, '#arc', 'mousedown')
      stor.mtgAppLecteur.removeEventListener(svgId, '#poly', 'mousedown')
      stor.mtgAppLecteur.removeEventListener(svgId, '#arc', 'mouseover')
      stor.mtgAppLecteur.removeEventListener(svgId, '#poly', 'mouseover')
      stor.mtgAppLecteur.removeEventListener(svgId, '#arc', 'mouseout')
      stor.mtgAppLecteur.removeEventListener(svgId, '#poly', 'mouseout')
      stor.mtgAppLecteur.removeEventListener(svgId, '#arc2', 'mouseenter')
      stor.mtgAppLecteur.removeEventListener(svgId, '#arc2', 'mouseout')
      stor.mtgAppLecteur.setVisible(svgId, '#pt', false, true)
      stor.mtgAppLecteur.setVisible(svgId, 136, false, true)
      stor.mtgAppLecteur.setVisible(svgId, '#p6', false, true)
      stor.mtgAppLecteur.setVisible(svgId, '#ma', false, true)
      stor.mtgAppLecteur.getDoc(svgId).defaultCursor = null
    }
    if (stor.encours === 'g') j3pEmpty(stor.lesdiv.zonedrep)
    if (stor.encours === 'm') stor.larep.disable()
  }
  function passetoutvert () {
    if (stor.encours === 'p') {
      RapCorrige(true)
    }
    if (stor.encours === 'g') {
      if (stor.choixA) {
        stor.mtgAppLecteur.setColor(svgId, 140, 0, 138, 115, true)
        stor.mtgAppLecteur.setColor(svgId, 146, 0, 0, 0, true)
      } else {
        stor.mtgAppLecteur.setColor(svgId, 140, 0, 0, 0, true)
        stor.mtgAppLecteur.setColor(svgId, 146, 0, 138, 115, true)
      }
      stor.mtgAppLecteur.updateFigure(svgId)
    }
    if (stor.encours === 'm') stor.larep.corrige(true)
  }
  function Debannimerap () {
    stor.mtgAppLecteur.calculate(svgId)
    stor.ptBdep = stor.mtgAppLecteur.getPointPosition(svgId, 'B')
    stor.ptCdep = stor.mtgAppLecteur.getPointPosition(svgId, '#p32')
    stor.ptCEncours = j3pClone(stor.ptBdep)
    stor.anggg = stor.mtgAppLecteur.valueOf(svgId, 'ang')
    stor.anngDeb = stor.anggg
    stor.AngA = stor.mtgAppLecteur.valueOf(svgId, 'langA')
    stor.AngB = stor.mtgAppLecteur.valueOf(svgId, 'langB')
    stor.AngADeb = 90 + stor.anngDeb
    stor.AngBDeb = stor.anngDeb - 90
    if (stor.AngADeb > 180) stor.AngADeb -= 360
    if (stor.AngBDeb > 180) stor.AngBDeb -= 360
    if (stor.AngADeb < -180) stor.AngADeb += 360
    if (stor.AngBDeb < -180) stor.AngBDeb += 360
    stor.AngA += 90
    stor.AngB -= 90
    if (stor.AngA > 180) stor.AngA -= 360
    if (stor.AngB > 180) stor.AngB -= 360
    if (stor.AngA < -180) stor.AngA += 360
    if (stor.AngB < -180) stor.AngB += 360
    const depA = distangle(stor.AngADeb, stor.AngA)
    const depB = distangle(stor.AngBDeb, stor.AngB)

    stor.choixA = (depA > depB)
    setTimeout(annimerep, stor.tempodeb)
    stor.annimCpt = 'C'
  }
  function annimerep () {
    if (stor.raz) return
    if (!stor.lesdiv.consigne1) return
    switch (stor.annimCpt) {
      case 'C':
        if ((stor.ptCEncours.x === stor.ptCdep.x) && (stor.ptCEncours.y === stor.ptCdep.y)) {
          stor.annimCpt = 'A'
          setTimeout(annimerep, stor.tempo)
          return
        }
        if (stor.ptCEncours.x < stor.ptCdep.x) stor.ptCEncours.x += stor.depCentreCo
        if (stor.ptCEncours.y < stor.ptCdep.y) stor.ptCEncours.y += stor.depCentreCo
        if (stor.ptCEncours.x > stor.ptCdep.x) stor.ptCEncours.x -= stor.depCentreCo
        if (stor.ptCEncours.y > stor.ptCdep.y) stor.ptCEncours.y -= stor.depCentreCo
        if (Math.abs(stor.ptCEncours.x - stor.ptCdep.x) < stor.depCentreCo) stor.ptCEncours.x = stor.ptCdep.x
        if (Math.abs(stor.ptCEncours.y - stor.ptCdep.y) < stor.depCentreCo) stor.ptCEncours.y = stor.ptCdep.y
        stor.mtgAppLecteur.setPointPosition(svgId, '#p6', stor.ptCEncours.x, stor.ptCEncours.y, true)
        stor.mtgAppLecteur.calculate(svgId)
        setTimeout(annimerep, stor.tempo)
        break
      case 'A':
        if (stor.choixA) {
          if (stor.anggg === stor.AngA) {
            stor.annimCpt = 'F'
            setTimeout(annimerep, stor.tempo)
            return
          }
          if (stor.anggg > stor.AngA) stor.anggg--
          if (stor.anggg < stor.AngA) stor.anggg++
          if (Math.abs(stor.anggg - stor.AngA) < 1) stor.anggg = stor.AngA
          stor.mtgAppLecteur.giveFormula2(svgId, 'ang', stor.anggg)
          stor.mtgAppLecteur.calculateAndDisplayAll(true)
          setTimeout(annimerep, stor.tempo)
        } else {
          if (stor.anggg === stor.AngB) {
            stor.annimCpt = 'F'
            setTimeout(annimerep, stor.tempo)
            return
          }
          if (stor.anggg > stor.AngB) stor.anggg--
          if (stor.anggg < stor.AngB) stor.anggg++
          if (Math.abs(stor.anggg - stor.AngB) < 1) stor.anggg = stor.AngB
          stor.mtgAppLecteur.giveFormula2(svgId, 'ang', stor.anggg)
          stor.mtgAppLecteur.calculateAndDisplayAll(true)
          setTimeout(annimerep, stor.tempo)
        }
        break
      case 'F':
        RapCorrige('jk')
        stor.annimCpt = 'R'
        setTimeout(annimerep, stor.tempodeb)
        break
      case 'R':
        stor.ptCEncours = j3pClone(stor.ptBdep)
        stor.anggg = stor.anngDeb
        stor.mtgAppLecteur.setPointPosition(svgId, '#p6', stor.ptCEncours.x, stor.ptCEncours.y, true)
        stor.mtgAppLecteur.giveFormula2(svgId, 'ang', stor.anggg)
        stor.mtgAppLecteur.calculateAndDisplayAll(true)
        RapCorrige(false)
        stor.mtgAppLecteur.giveFormula2(svgId, 'ang', stor.anggg)
        stor.mtgAppLecteur.calculateAndDisplayAll(true)
        stor.annimCpt = 'C'
        setTimeout(annimerep, stor.tempodeb)
        break
    }
  }
  function faisSuite () {
    stor.bouSuit = j3pAjouteBouton(me.buttonsElts.container, finsuite, { className: 'big suite', value: 'Suite' })
    me.cacheBoutonValider()
  }
  function finsuite () {
    j3pDetruit(stor.bouSuit)
    stor.raz = true
    poseQuestion()
    makefig()
  }

  function affCorrection (bool) {
    let yaexplik = false
    let yaco = false
    if (stor.errCentre) {
      stor.compteurPe1++
      yaexplik = true
      j3pAffiche(stor.lesdiv.explications, null, 'Le centre du rapporteur est mal placé !')
    }
    if (stor.errCentreP) {
      stor.compteurPe1++
      yaexplik = true
      j3pAffiche(stor.lesdiv.explications, null, 'Sois plus précis pour le centre du rapporteur !')
    }
    if (stor.err0) {
      stor.compteurPe1++
      yaexplik = true
      j3pAffiche(stor.lesdiv.explications, null, 'Tu dois placer un $0$ sur l’un des deux côtés !')
    }
    if (stor.err0P) {
      stor.compteurPe1++
      yaexplik = true
      j3pAffiche(stor.lesdiv.explications, null, 'Le rapporteur n’est pas bien aligné avec un côté !')
    }
    if (stor.err0I) {
      stor.compteurPe1++
      yaexplik = true
      j3pAffiche(stor.lesdiv.explications, null, 'Il faut placer ce $0$ sur l’autre côté !')
    }
    if (stor.errMEcrit) {
      stor.compteurPe3++
      yaexplik = true
      j3pAffiche(stor.lesdiv.explications, null, stor.errMEcritkoi)
    }
    if (stor.errMG) {
      stor.compteurPe3++
      yaexplik = true
      j3pAffiche(stor.lesdiv.explications, null, 'Tu dois te tromper de graduation !')
    }
    if (stor.errM) stor.compteurPe3++

    if (stor.encours === 'p') RapCorrige(false)
    if (bool) {
      desactivezone()
      if (stor.encours === 'p') {
        Debannimerap()
        stor.raz = false
        stor.placeok = false
      }
      if (stor.encours === 'g') {
        stor.compteurPe2 += 2
        if (stor.choixA) {
          stor.mtgAppLecteur.setColor(svgId, 140, 0, 0, 255, true)
          stor.mtgAppLecteur.setColor(svgId, 146, 214, 71, 0, true)
        } else {
          stor.mtgAppLecteur.setColor(svgId, 140, 214, 71, 0, true)
          stor.mtgAppLecteur.setColor(svgId, 146, 0, 0, 255, true)
        }
        stor.mtgAppLecteur.updateFigure(svgId)
        j3pAffiche(stor.lesdiv.explications, null, 'Regarde bien le $0$ du rapporteur utilisé !')
      }
      if (stor.encours === 'm') {
        yaco = true
        stor.larep.barre()
        j3pAffiche(stor.lesdiv.solution, null, '$\\widehat{' + stor.A + stor.B + stor.C + '} \\approx' + stor.deplacement + '°$')
      }
    } else {
      me.afficheBoutonValider()
    }
    if (ds.theme === 'zonesAvecImageDeFond') {
      if (yaexplik) {
        stor.lesdiv.explications.classList.add('explique')
      }
      if (yaco) {
        stor.lesdiv.solution.classList.add('correction')
      }
    }
  } // affCorrection

  function enonceMain () {
    if (me.etapeCourante === 1) {
      // A chaque répétition, on nettoie la scène
      // A commenter si besoin
      me.videLesZones()
      creeLesDiv()
      stor.Lexo = faisChoixExos()
      stor.lesdiv.travail.classList.add('travail')
      stor.lesdiv.consigne.classList.add('enonce')
      stor.lesdiv.explications.classList.remove('explique')
      stor.lesdiv.solution.classList.remove('correction')
    }
    poseQuestion()
    makefig()
    me.finEnonce()
  }

  switch (this.etat) {
    case 'enonce':
      // code exécuté au lancement de la section
      if (me.debutDeLaSection) {
        initSection()
      } else {
        stor.raz = true
        enonceMain()
      }
      break // case "enonce":

    case 'correction':
      // On teste si une réponse a été saisie
      if (stor.lesdiv === undefined) return
      stor.lesdiv.explications.innerHTML = ''
      stor.lesdiv.correction.innerHTML = ''

      if (!yaReponse()) {
        // pas de réponse
        stor.lesdiv.correction.style.color = this.styles.cfaux
        stor.lesdiv.correction.innerHTML = reponseIncomplete
        return this.finCorrection()
      }

      // Bonne réponse
      if (isRepOk()) {
        stor.lesdiv.correction.style.color = this.styles.cbien
        stor.lesdiv.correction.innerHTML = cBien

        desactivezone()
        passetoutvert()
        this.cacheBoutonValider()

        if (stor.encours === 'm') {
          if (stor.afaire.length === 1) this.score++
          if (stor.afaire.length === 2) {
            if (stor.afaire[0] === 'p') {
              this.score = j3pArrondi(this.score + 0.5, 2)
            } else {
              this.score = j3pArrondi(this.score + 0.75, 2)
            }
          }
          if (stor.afaire.length === 3) this.score = j3pArrondi(this.score + 0.25, 2)
        }
        if (stor.encours === 'g') {
          if (stor.afaire.length !== 1) {
            this.score = j3pArrondi(this.score + 0.25, 2)
            stor.encours = 'm'
            faisSuite()
            // FIXME pourquoi ne pas appeler this.finCorrection() sans cacher le bouton
            me.cacheBoutonValider()
            return
          }
          this.score++
        }
        if (stor.encours === 'p') {
          if (stor.afaire.length !== 1) {
            this.score = j3pArrondi(this.score + 0.5, 2)
            if (stor.afaire.length === 3) {
              stor.encours = 'g'
            } else {
              stor.encours = 'm'
            }
            faisSuite()
            // FIXME pourquoi ne pas appeler this.finCorrection() sans cacher le bouton
            me.cacheBoutonSuite()
            return
          }
          this.score++
        }

        this.typederreurs[0]++
        return this.finCorrection('navigation', true)
      }

      // Pas de bonne réponse
      stor.lesdiv.correction.style.color = this.styles.cfaux
      stor.lesdiv.correction.innerHTML = cFaux
      if (me.essaiCourant < me.donneesSection.nbchances) {
        stor.lesdiv.correction.innerHTML += '<br>' + essaieEncore
        affCorrection(false)
        this.typederreurs[1]++
        return this.finCorrection()
      }

      // Erreur au dernier essai
      affCorrection(true)

      if (stor.encours === 'g') {
        if (stor.afaire.length !== 1) {
          stor.encours = 'm'
          faisSuite()
          // FIXME pourquoi ne pas appeler this.finCorrection() sans cacher le bouton
          me.cacheBoutonValider()
          return
        }
      }
      if (stor.encours === 'p') {
        if (stor.afaire.length !== 1) {
          if (stor.afaire.length === 3) {
            stor.encours = 'g'
          } else {
            stor.encours = 'm'
          }
          faisSuite()
          me.cacheBoutonValider()
          return
        }
      }

      this.typederreurs[2]++
      this.finCorrection('navigation', true)
      break // case "correction":

    case 'navigation':
      if (this.sectionTerminee()) {
        this.parcours.pe = getPe(ds, stor)
        this.buttonsElts.sectionSuivante.addEventListener('click', () => { stor.raz = true }, false)
      } else {
        this.etat = 'enonce'
      }
      this.finNavigation()
      break // case "navigation":
  }
}
