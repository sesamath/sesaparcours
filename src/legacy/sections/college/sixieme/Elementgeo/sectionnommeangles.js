import { j3pAddElt, j3pArrondi, j3pDetruit, j3pGetNewId, j3pGetRandomInt, j3pModale, j3pNotify, j3pShowError } from 'src/legacy/core/functions'
import { j3pCreeSVG } from 'src/legacy/core/functionsSvg'
import ZoneStyleMathquill1 from 'src/legacy/outils/zoneStyleMathquill/ZoneStyleMathquill1'
import { addDefaultTable } from 'src/legacy/themes/table'
import { j3pAffiche } from 'src/lib/mathquill/functions'
import { getMtgCore } from 'src/lib/outils/mathgraph'
import { addMaj } from 'src/lib/utils/string'
import textesGeneriques from 'src/lib/core/textes'

const { cBien, cFaux, essaieEncore, reponseIncomplete, regardeCorrection } = textesGeneriques

/**
 * Ajouter une description ici
 * @fileOverview
 * @author Tommy Barroy <t.barroy@laposte.net>
 * @since 2017-10
*/

export const params = {
  outils: ['calculatrice', 'mathquill'],
  /**
   * Ce qui est paramétrable dans l’éditeur de graphe
   * @type {LegacySectionParamsParametres}
   */
  parametres: [
    ['nbrepetitions', 9, 'entier', 'Nombre de répétitions de la section'],
    ['indication', '', 'string', 'Indication pour chaque répétition'],
    ['limite', 0, 'entier', 'Temps disponible par répétition, en secondes'],
    ['nbchances', 1, 'entier', 'Nombre d’essais par répétition'],
    ['mode', 'Click1', 'liste', '<u>Click 1</u>: Donne un angle et demande de cliquer sur son angle associé. \n\n <u>Click 2</u>: Demande de cliquer sur deux angles associés. \n\n <u>Nomme 1</u>: Donne un angle et demande un angle associé. \n\n <u>Nomme 2</u>: demande deux angles associés. \n\n <i>Des combinaisons de ces quatre méthodes peuvent aussi être choisies.</i>', ['Click1', 'Click2', 'Nomme1', 'Nomme2', 'Nomme1_Click1', 'Nomme2_Click2', 'Click 1 ou 2', 'Nomme 1 ou 2', 'Tout']],
    ['oppose', false, 'boolean', '<u>true</u>: L’exercice peut proposer des angles opposés par le sommet.'],
    ['alternes_internes', false, 'boolean', '<u>true</u>: L’exercice peut proposer des angles alternes_internes.'],
    ['correspondants', false, 'boolean', '<u>true</u>: L’exercice peut proposer des angles correspondants.'],
    ['alternes_externes', false, 'boolean', '<u>true</u>: L’exercice peut proposer des angles alternes-externes.'],
    ['supplementaires_et_adjacents', false, 'boolean', '<u>true</u>: L’exercice peut proposer des angles supplémentaires et adjacents.'],
    // le modèle gère la compatibilité ascendante avec l’ancien paramètre MiseEnPagePlusClaire
    ['theme', 'standard', 'liste', 'Modèle pour l’affichage', ['standard', 'zonesAvecImageDeFond']]

  ],
  // dans le cas d’une section QUANTITATIVE
  pe: [{ pe_1: 'Erreur Ecriture angle' }]
}

/**
 * section nommeangles
 * @this {Parcours}
 */
export default function main () {
  const me = this
  const ds = this.donneesSection
  const stor = me.storage
  let svgId = stor.svgId

  function affNomAngleExpr (expr) {
    return '$\\widehat{' + expr + '}$'
  }
  function opopop (num) {
    switch (num) {
      case 0: return [2]
      case 1: return [3]
      case 2: return [0]
      case 3: return [1]
      case 4: return [6]
      case 5: return [7]
      case 6: return [4]
      case 7: return [5]
    }
  }
  function corrige (num, bool) {
    const cool = bool ? [0, 255, 0] : (bool === false) ? [255, 127, 39] : [0, 0, 255]
    const cool2 = bool ? [0, 150, 0] : (bool === false) ? [200, 80, 10] : [0, 0, 150]
    stor.mtgAppLecteur.setColor(svgId, '#aa' + (num), cool2[0], cool2[1], cool2[2], true)
    stor.mtgAppLecteur.setLineStyle(svgId, '#aa' + (num), 0, 5, true)
    stor.mtgAppLecteur.setColor(svgId, '#s' + (num), cool[0], cool[1], cool[2], true)
  }
  function alalt (num) {
    switch (num) {
      case 1: return [7]
      case 2: return [4]
      case 4: return [2]
      case 7: return [1]
    }
  }
  function alalte (num) {
    switch (num) {
      case 0: return [6]
      case 3: return [5]
      case 5: return [3]
      case 6: return [0]
    }
  }
  function cococo (num) {
    switch (num) {
      case 0: return [4]
      case 1: return [5]
      case 2: return [6]
      case 3: return [7]
      case 4: return [0]
      case 5: return [1]
      case 6: return [2]
      case 7: return [3]
    }
  }
  function supsup (num) {
    switch (num) {
      case 0: return [1, 3]
      case 1: return [0, 2]
      case 2: return [3, 1]
      case 3: return [0, 2]
      case 4: return [5, 7]
      case 5: return [4, 6]
      case 6: return [5, 7]
      case 7: return [4, 6]
    }
  }
  function afficheFig () {
    const nomspris = []

    const txtFigure = 'TWF0aEdyYXBoSmF2YTEuMAAAABM+TMzNAAJmcv###wEA#wEAAAAAAAAAAANLAAACfwAAAQEAAAAAAAAAAQAAAOX#####AAAAAQAKQ0NhbGNDb25zdAD#####AAJwaQAWMy4xNDE1OTI2NTM1ODk3OTMyMzg0Nv####8AAAABAApDQ29uc3RhbnRlQAkh+1RELRj#####AAAAAQAKQ1BvaW50QmFzZQD#####AQAAAAAOAAFVAMAkAAAAAAAAQBAAAAAAAAAAAAUAAEAv#nbItDlYQC#+dsi0OVj#####AAAAAQAUQ0Ryb2l0ZURpcmVjdGlvbkZpeGUA#####wEAAAAAEAAAAQAAAAEAAAABAT#wAAAAAAAA#####wAAAAEAD0NQb2ludExpZURyb2l0ZQD#####AQAAAAAOAAFWAMAAAAAAAAAAQBAAAAAAAAAAAAUAAUA##nbItDlYAAAAAv####8AAAABAAhDU2VnbWVudAD#####AQAAAAAQAAABAAAAAQAAAAEAAAAD#####wAAAAEAB0NNaWxpZXUA#####wEAAAAAEAAAAAAAAAAAAAAAQAgAAAAAAAAAAAUAAAAAAQAAAAP#####AAAAAgAMQ0NvbW1lbnRhaXJlAP####8BAAAAAAAAAAAAAAAAQBgAAAAAAAAAAAAAAAUMAAAAAAABAAAAAAAAAAEAAAAAAAAAAAABMf####8AAAABAAlDTG9uZ3VldXIA#####wAAAAEAAAADAAAAAgD#####AQAAAAAQAAAAAAAAAAAAAABACAAAAAAAAAAABQABQHewAAAAAABAcd4UeuFHrgAAAAMA#####wEAAAABEAAAAQAAAAEAAAAIAD#wAAAAAAAAAAAAAwD#####AQAAAAEQAAABAAAAAQAAAAgBP#AAAAAAAAAAAAACAP####8BAAAAABAAAAAAAAAAAAAAAEAIAAAAAAAAAAAFAAFAZUAAAAAAAEBifCj1wo9cAAAAAgD#####AQAAAAAQAAAAAAAAAAAAAABACAAAAAAAAAAABQABQGUgAAAAAABAQXCj1wo9cf####8AAAABAAlDQ2VyY2xlT0EA#####wEAAAAAAAABAAAACwAAAAwAAAACAP####8AAAAAABAAAABACAAAAAAAAAAAAAAAAAAAAAAFAABAVAAAAAAAAEB93hR64Ueu#####wAAAAEAB0NDYWxjdWwA#####wAFbWluaTEAATEAAAABP#AAAAAAAAAAAAAKAP####8ABW1heGkxAAMzNjAAAAABQHaAAAAAAAD#####AAAAAQAUQ0ltcGxlbWVudGF0aW9uUHJvdG8A#####wAHQ3Vyc2V1cgAAAAUAAAAFAAAAAwAAAA8AAAAQAAAADgAAAAMAAAAAEQEAAAAAEAAAAQAAAAEAAAAOAT#wAAAAAAAAAAAABAEAAAARAAAAAAAQAAAAwAgAAAAAAAA#8AAAAAAAAAAABQAAQF1AAAAAAAAAAAAS#####wAAAAEAC0NIb21vdGhldGllAAAAABEAAAAO#####wAAAAEACkNPcGVyYXRpb24D#####wAAAAEAD0NSZXN1bHRhdFZhbGV1cgAAAA8AAAANAQAAAA4AAAAPAAAADgAAABD#####AAAAAQALQ1BvaW50SW1hZ2UAAAAAEQEAAAAADQACTzEAwBAAAAAAAABAEAAAAAAAAAAABQAAAAATAAAAFAAAAAwAAAAAEQAAAA4AAAANAwAAAA0BAAAAAT#wAAAAAAAAAAAADgAAAA8AAAANAQAAAA4AAAAQAAAADgAAAA8AAAAPAAAAABEBAAAAAA0AAkkxAMAAAAAAAAAAQAgAAAAAAAAAAAUAAAAAEwAAABYAAAAFAQAAABEAAAAAABAAAAEAAAABAAAADgAAABMAAAAEAQAAABEAAAAAARAAAmsxAMAAAAAAAAAAQAAAAAAAAAAAAAEAAT#eoeoeoeofAAAAGP####8AAAACAA9DTWVzdXJlQWJzY2lzc2UBAAAAEQACYTEAAAAVAAAAFwAAABn#####AAAAAQAPQ1ZhbGV1ckFmZmljaGVlAQAAABEAAAAAAEA9AAAAAAAAwBQAAAAAAAAAAAAAABkPAAH###8AAAABAAAAAgAAAAEAAAAAAAAAAAAAAAACAAAAGgAAAAIA#####wAAAAAAEAAAAEAIAAAAAAAAAAAAAAAAAAAAAAUAAEBUAAAAAAAAQICPCj1wo9cAAAAKAP####8ABW1pbmkyAAEwAAAAAQAAAAAAAAAAAAAACgD#####AAVtYXhpMgACMTAAAAABQCQAAAAAAAAAAAALAP####8AB0N1cnNldXIAAAAFAAAABQAAAAMAAAAdAAAAHgAAABwAAAADAAAAAB8BAAAAABAAAAEAAAABAAAAHAE#8AAAAAAAAAAAAAQBAAAAHwAAAAAAEAAAAMAIAAAAAAAAP#AAAAAAAAAAAAUAAEBdQAAAAAAAAAAAIAAAAAwAAAAAHwAAABwAAAANAwAAAA4AAAAdAAAADQEAAAAOAAAAHQAAAA4AAAAeAAAADwAAAAAfAQAAAAANAAJPMQDAEAAAAAAAAEAQAAAAAAAAAAAFAAAAACEAAAAiAAAADAAAAAAfAAAAHAAAAA0DAAAADQEAAAABP#AAAAAAAAAAAAAOAAAAHQAAAA0BAAAADgAAAB4AAAAOAAAAHQAAAA8AAAAAHwEAAAAADQACSTEAwAAAAAAAAABACAAAAAAAAAAABQAAAAAhAAAAJAAAAAUBAAAAHwAAAAAAEAAAAQAAAAEAAAAcAAAAIQAAAAQBAAAAHwAAAAABEAABawDAAAAAAAAAAEAAAAAAAAAAAAABAAE#4CMCMCMCMAAAACYAAAAQAQAAAB8AAmEyAAAAIwAAACUAAAAnAAAAEQEAAAAfAAAAAAAAAAAAAAAAAMAYAAAAAAAAAAAAAAAnDwAB####AAAAAQAAAAIAAAABAAAAAAAAAAAAAAAAAgAAACgAAAACAP####8AAAAAABAAAABACAAAAAAAAAAAAAAAAAAAAAAFAABAbsAAAAAAAEB93hR64UeuAAAACgD#####AAVtaW5pMwABMAAAAAEAAAAAAAAAAAAAAAoA#####wAFbWF4aTMAAjEwAAAAAUAkAAAAAAAAAAAACwD#####AAdDdXJzZXVyAAAABQAAAAUAAAADAAAAKwAAACwAAAAqAAAAAwAAAAAtAQAAAAAQAAABAAAAAQAAACoBP#AAAAAAAAAAAAAEAQAAAC0AAAAAABAAAADACAAAAAAAAD#wAAAAAAAAAAAFAABAXUAAAAAAAAAAAC4AAAAMAAAAAC0AAAAqAAAADQMAAAAOAAAAKwAAAA0BAAAADgAAACsAAAAOAAAALAAAAA8AAAAALQEAAAAADQACTzEAwBAAAAAAAABAEAAAAAAAAAAABQAAAAAvAAAAMAAAAAwAAAAALQAAACoAAAANAwAAAA0BAAAAAT#wAAAAAAAAAAAADgAAACsAAAANAQAAAA4AAAAsAAAADgAAACsAAAAPAAAAAC0BAAAAAA0AAkkxAMAAAAAAAAAAQAgAAAAAAAAAAAUAAAAALwAAADIAAAAFAQAAAC0AAAAAABAAAAEAAAABAAAAKgAAAC8AAAAEAQAAAC0AAAAAARAAAmsyAMAAAAAAAAAAQAAAAAAAAAAAAAEAAT#fufufufugAAAANAAAABABAAAALQACYTMAAAAxAAAAMwAAADUAAAARAQAAAC0AAAAAAAAAAAAAAAAAwBgAAAAAAAAAAAAAADUPAAH###8AAAABAAAAAgAAAAEAAAAAAAAAAAAAAAACAAAANgAAAAIA#####wAAAAAAEAAAAEAIAAAAAAAAAAAAAAAAAAAAAAUAAEBvIAAAAAAAQICfCj1wo9cAAAAKAP####8ABW1pbmk0AAEwAAAAAQAAAAAAAAAAAAAACgD#####AAVtYXhpNAACMTAAAAABQCQAAAAAAAAAAAALAP####8AB0N1cnNldXIAAAAFAAAABQAAAAMAAAA5AAAAOgAAADgAAAADAAAAADsBAAAAABAAAAEAAAABAAAAOAE#8AAAAAAAAAAAAAQBAAAAOwAAAAAAEAAAAMAIAAAAAAAAP#AAAAAAAAAAAAUAAEBdQAAAAAAAAAAAPAAAAAwAAAAAOwAAADgAAAANAwAAAA4AAAA5AAAADQEAAAAOAAAAOQAAAA4AAAA6AAAADwAAAAA7AQAAAAANAAJPMQDAEAAAAAAAAEAQAAAAAAAAAAAFAAAAAD0AAAA+AAAADAAAAAA7AAAAOAAAAA0DAAAADQEAAAABP#AAAAAAAAAAAAAOAAAAOQAAAA0BAAAADgAAADoAAAAOAAAAOQAAAA8AAAAAOwEAAAAADQACSTEAwAAAAAAAAABACAAAAAAAAAAABQAAAAA9AAAAQAAAAAUBAAAAOwAAAAAAEAAAAQAAAAEAAAA4AAAAPQAAAAQBAAAAOwAAAAABEAACazMAwAAAAAAAAABAAAAAAAAAAAAAAQABP9VVVVVVVVUAAABCAAAAEAEAAAA7AAJhNAAAAD8AAABBAAAAQwAAABEBAAAAOwAAAAAAAAAAAAAAAADAGAAAAAAAAAAAAAAAQw8AAf###wAAAAEAAAACAAAAAQAAAAAAAAAAAAAAAAIAAABEAAAACgD#####AAJBMQACYTEAAAAOAAAAGgAAAAoA#####wACQTIAAmEyAAAADgAAACgAAAAKAP####8AAkEzAAJhMwAAAA4AAAA2AAAACgD#####AAJBNAACYTQAAAAOAAAARP####8AAAABAAlDUm90YXRpb24A#####wAAAAsAAAAOAAAARgAAAA8A#####wAAAAAAEAAAAAAAAAAAAAAAQAgAAAAAAAAAAAkAAAAADAAAAEr#####AAAAAQARQ1N5bWV0cmllQ2VudHJhbGUA#####wAAAAsAAAAPAP####8AAAAAABAAAAAAAAAAAAAAAEAIAAAAAAAAAAAJAAAAAEsAAABMAAAABQD#####AQAAAAAQAAABAAAAAQAAAE0AAABL#####wAAAAIACUNDZXJjbGVPUgD#####AQAAAAAAAAEAAAALAAAAAT#wAAAAAAAAAP####8AAAABABBDSW50RHJvaXRlQ2VyY2xlAP####8AAABOAAAAT#####8AAAABABBDUG9pbnRMaWVCaXBvaW50AP####8BAAAAABAAAAAAAAAAAAAAAEAIAAAAAAAAAAAFAAEAAABQAAAAFgD#####AQAAAAAQAAAAAAAAAAAAAABACAAAAAAAAAAABQACAAAAUAAAAAwA#####wAAAFIAAAANAwAAAA4AAABHAAAAAUAkAAAAAAAAAAAADwD#####AAAAAAAQAAAAAAAAAAAAAABACAAAAAAAAAAABQAAAABNAAAAUwAAAAwA#####wAAAFEAAAANAwAAAA4AAABIAAAAAUAkAAAAAAAAAAAADwD#####AAAAAAAQAAAAAAAAAAAAAABACAAAAAAAAAAABQAAAABLAAAAVQAAABQA#####wEAAAAAAAABAAAATQAAAAFAAAAAAAAAAAD#####AAAAAQAQQ0ludENlcmNsZUNlcmNsZQD#####AAAADQAAAFcAAAAWAP####8BAAAAABAAAAAAAAAAAAAAAEAIAAAAAAAAAAAFAAEAAABYAAAAFgD#####AQAAAAAQAAAAAAAAAAAAAABACAAAAAAAAAAABQACAAAAWAAAABQA#####wEAAAAAAAABAAAASwAAAAFACAAAAAAAAAAAAAAXAP####8AAAANAAAAWwAAABYA#####wEAAAAAEAAAAAAAAAAAAAAAQAgAAAAAAAAAAAUAAgAAAFwAAAAWAP####8BAAAAABAAAAAAAAAAAAAAAEAIAAAAAAAAAAAFAAEAAABcAAAAEgD#####AAAACwAAAA0DAAAADQIAAAABQFhAAAAAAAAAAAAOAAAASQAAAAFAJAAAAAAAAAAAAA8A#####wAAAAAAEAAAAAAAAAAAAAAAQAgAAAAAAAAAAAkAAAAAWgAAAF######AAAAAQAJQ0Ryb2l0ZUFCAP####8AAAAAABAAAAEAAAACAAAAYAAAAFT#####AAAAAQAQQ0Ryb2l0ZVBhcmFsbGVsZQD#####AAAAAAAQAAABAAAAAgAAAFYAAABh#####wAAAAEAI0NBdXRyZVBvaW50SW50ZXJzZWN0aW9uRHJvaXRlQ2VyY2xlAP####8AAAAAABAAAAAAAAAAAAAAAEAIAAAAAAAAAAAJAAAAAA0AAABhAAAAYAAAABUA#####wAAAGIAAAANAAAAFgD#####AAAAAAAQAAAAAAAAAAAAAABACAAAAAAAAAAACQACAAAAZAAAABYA#####wAAAAAAEAAAAAAAAAAAAAAAQAgAAAAAAAAAAAkAAQAAAGQAAAAYAP####8AAAAAABAAAAEAAAACAAAATQAAAEv#####AAAAAQAWQ0Ryb2l0ZVBlcnBlbmRpY3VsYWlyZQD#####AQB#AAAQAAABAAAAAQAAAGAAAABhAAAAGwD#####AQB#AAAQAAABAAAAAQAAAGMAAABhAAAAGwD#####AQB#AAAQAAABAAAAAQAAAGUAAABiAAAAGwD#####AQB#AAAQAAABAAAAAQAAAGYAAABiAAAAGwD#####AQB#AAAQAAABAAAAAQAAAEsAAABnAAAAGwD#####AQB#AAAQAAABAAAAAQAAAE0AAABnAAAAFAD#####AQB#AAAAAAEAAABmAAAAAT#jMzMzMzMzAAAAABQA#####wEAfwAAAAABAAAAYwAAAAE#4zMzMzMzMwAAAAAUAP####8BAH8AAAAAAQAAAFQAAAABP+MzMzMzMzMAAAAAFAD#####AQB#AAAAAAEAAABNAAAAAT#jMzMzMzMzAAAAABQA#####wEAfwAAAAABAAAAVgAAAAE#4zMzMzMzMwAAAAAUAP####8BAH8AAAAAAQAAAEsAAAABP+MzMzMzMzMAAAAAFAD#####AQB#AAAAAAEAAABlAAAAAT#jMzMzMzMzAAAAABQA#####wEAfwAAAAABAAAAYAAAAAE#4zMzMzMzMwAAAAAVAP####8AAABoAAAAdQAAABYA#####wEAfwAAEAAAAAAAAAAAAAAAQAgAAAAAAAAAAAUAAQAAAHYAAAAWAP####8BAH8AABAAAAAAAAAAAAAAAEAIAAAAAAAAAAAFAAIAAAB2AAAAFQD#####AAAAagAAAHQAAAAWAP####8BAH8AABAAAAAAAAAAAAAAAEAIAAAAAAAAAAAFAAEAAAB5AAAAFgD#####AQB#AAAQAAAAAAAAAAAAAABACAAAAAAAAAAABQACAAAAeQAAABUA#####wAAAGsAAABuAAAAFgD#####AQB#AAAQAAAAAAAAAAAAAABACAAAAAAAAAAABQABAAAAfAAAABYA#####wEAfwAAEAAAAAAAAAAAAAAAQAgAAAAAAAAAAAUAAgAAAHwAAAAVAP####8AAABsAAAAcwAAABYA#####wEAfwAAEAAAAAAAAAAAAAAAQAgAAAAAAAAAAAUAAQAAAH8AAAAWAP####8BAH8AABAAAAAAAAAAAAAAAEAIAAAAAAAAAAAFAAIAAAB######wAAAAEADENCaXNzZWN0cmljZQD#####AQB#AAAQAAABAAAAAQAAAFQAAABWAAAAZQAAABUA#####wAAAIIAAAByAAAAFgD#####AAB#AAAQAAAAAAAAAAAAAABACAAAAAAAAAAABQACAAAAgwAAABYA#####wEAfwAAEAAAAAAAAAAAAAAAQAgAAAAAAAAAAAUAAQAAAIMAAAAcAP####8BAH8AABAAAAEAAAABAAAATQAAAFQAAABgAAAAFQD#####AAAAhgAAAHAAAAAWAP####8AAH8AABAAAAAAAAAAAAAAAEAIAAAAAAAAAAAFAAIAAACHAAAAFgD#####AQB#AAAQAAAAAAAAAAAAAABACAAAAAAAAAAABQABAAAAhwAAABUA#####wAAAG0AAABxAAAAFgD#####AQB#AAAQAAAAAAAAAAAAAABACAAAAAAAAAAABQABAAAAigAAABYA#####wEAfwAAEAAAAAAAAAAAAAAAQAgAAAAAAAAAAAUAAgAAAIoAAAAVAP####8AAABpAAAAbwAAABYA#####wEAfwAAEAAAAAAAAAAAAAAAQAgAAAAAAAAAAAUAAQAAAI0AAAAWAP####8BAH8AABAAAAAAAAAAAAAAAEAIAAAAAAAAAAAFAAIAAACNAAAAFAD#####AQB#AAAAAAEAAABUAAAAAT#wAAAAAAAAAAAAABQA#####wEAfwAAAAABAAAAVgAAAAE#8AAAAAAAAAAAAAAVAP####8AAABnAAAAkAAAABYA#####wEAfwAAEAAAAAAAAAAAAAAAQAgAAAAAAAAAAAUAAQAAAJIAAAAWAP####8BAH8AABAAAAAAAAAAAAAAAEAIAAAAAAAAAAAFAAIAAACSAAAAFQD#####AAAAYQAAAJAAAAAWAP####8BAH8AABAAAAAAAAAAAAAAAEAIAAAAAAAAAAAFAAIAAACVAAAAFgD#####AQB#AAAQAAAAAAAAAAAAAABACAAAAAAAAAAABQABAAAAlQAAABUA#####wAAAGIAAACRAAAAFgD#####AQB#AAAQAAAAAAAAAAAAAABACAAAAAAAAAAABQACAAAAmAAAABYA#####wEAfwAAEAAAAAAAAAAAAAAAQAgAAAAAAAAAAAUAAQAAAJgAAAAVAP####8AAABnAAAAkQAAABYA#####wEAfwAAEAAAAAAAAAAAAAAAQAgAAAAAAAAAAAUAAgAAAJsAAAAWAP####8BAH8AABAAAAAAAAAAAAAAAEAIAAAAAAAAAAAFAAEAAACb#####wAAAAEAEkNBcmNEZUNlcmNsZURpcmVjdAD#####AQAAAAAAAAMAAABUAAAAlwAAAJQAAAAdAP####8BAAAAAAAAAwAAAFQAAACUAAAAlgAAAB0A#####wEAAAAAAAADAAAAVAAAAJMAAACXAAAAHQD#####AQAAAAAAAAMAAABUAAAAlgAAAJMAAAAdAP####8BAAAAAAAAAwAAAFYAAACaAAAAnAAAAB0A#####wEAAAAAAAADAAAAVgAAAJwAAACZAAAAHQD#####AQAAAAAAAAMAAABWAAAAmQAAAJ0AAAAdAP####8BAAAAAAAAAwAAAFYAAACdAAAAmv####8AAAABABlDU3VyZmFjZVNlY3RldXJDaXJjdWxhaXJlAP####8A2NjYAAJzMAAAAAQAAAClAAAAHgD#####ANjY2AACczEAAAAEAAAAogAAAB4A#####wDY2NgAAnMyAAAABAAAAKMAAAAeAP####8A2NjYAAJzMwAAAAQAAACkAAAAHgD#####ANjY2AACczQAAAAEAAAAoAAAAB4A#####wDY2NgAAnM1AAAABAAAAJ4AAAAeAP####8A2NjYAAJzNgAAAAQAAACfAAAAHgD#####ANjY2AACczcAAAAEAAAAof####8AAAABAA5DT2JqZXREdXBsaXF1ZQD#####AAAAAAACZDEAAABhAAAAHwD#####AAAAAAACZDIAAABiAAAAHwD#####AAAAAAACZDMAAABnAAAABwD#####AAAAfwEAA25wQQAAAIUQAAAAAAABAAAAAQAAAAEAAAAAAAAAAAABQQAAAAcA#####wAAAH8BAANucEIAAACJEAAAAAAAAQAAAAEAAAABAAAAAAAAAAAAAUIAAAAHAP####8AAAB#AQADbnBDAAAAfhAAAAAAAAEAAAABAAAAAQAAAAAAAAAAAAFDAAAABwD#####AAAAfwEAA25wRAAAAI8QAAAAAAABAAAAAQAAAAEAAAAAAAAAAAABRAAAAAcA#####wAAAH8BAANucEUAAACMEAAAAAAAAQAAAAEAAAABAAAAAAAAAAAAAUUAAAAHAP####8AAAB#AQADbnBGAAAAeBAAAAAAAAEAAAABAAAAAQAAAAAAAAAAAAFGAAAABwD#####AAAAfwEAA25wRwAAAHsQAAAAAAABAAAAAQAAAAEAAAAAAAAAAAABRwAAAAcA#####wAAAH8BAANucEgAAACBEAAAAAAAAQAAAAEAAAABAAAAAAAAAAAAAUgAAAAUAP####8BAAAAAAAAAwAAAFQAAAABP+ZmZmZmZmYAAAAAFAD#####AQAAAAAAAAMAAABWAAAAAT#mZmZmZmZmAAAAABUA#####wAAAGEAAAC5AAAAFgD#####AQAAAAAQAAAAAAAAAAAAAABACAAAAAAAAAAACQACAAAAuwAAABYA#####wEAAAAAEAAAAAAAAAAAAAAAQAgAAAAAAAAAAAkAAQAAALsAAAAVAP####8AAABnAAAAuQAAABYA#####wEAAAAAEAAAAAAAAAAAAAAAQAgAAAAAAAAAAAkAAgAAAL4AAAAWAP####8BAAAAABAAAAAAAAAAAAAAAEAIAAAAAAAAAAAJAAEAAAC+AAAAFQD#####AAAAYgAAALoAAAAWAP####8BAAAAABAAAAAAAAAAAAAAAEAIAAAAAAAAAAAJAAIAAADBAAAAFgD#####AQAAAAAQAAAAAAAAAAAAAABACAAAAAAAAAAACQABAAAAwQAAABUA#####wAAAGcAAAC6AAAAFgD#####AQAAAAAQAAAAAAAAAAAAAABACAAAAAAAAAAACQACAAAAxAAAABYA#####wEAAAAAEAAAAAAAAAAAAAAAQAgAAAAAAAAAAAkAAQAAAMQAAAAdAP####8A#wAAAAJhMAAMAAAAVgAAAMYAAADDAAAAHQD#####AP8AAAACYTEADAAAAFYAAADDAAAAxQAAAB0A#####wD#AAAAAmEyAAwAAABWAAAAxQAAAMIAAAAdAP####8A#wAAAAJhMwAMAAAAVgAAAMIAAADGAAAAHQD#####AP8AAAACYTQADAAAAFQAAADAAAAAvQAAAB0A#####wD#AAAAAmE1AAwAAABUAAAAvQAAAL8AAAAdAP####8A#wAAAAJhNgAMAAAAVAAAAL8AAAC8AAAAHQD#####AP8AAAACYTcADAAAAFQAAAC8AAAAwAAAABQA#####wH#AAAAAAAMAAAAVAAAAAE#8AAAAAAAAAAAAAAUAP####8B#wAAAAAADAAAAFYAAAABP#AAAAAAAAAAAAAAFQD#####AAAAYQAAAM8AAAAWAP####8B#wAAABAAAAAAAAAAAAAAAEAIAAAAAAAAAAAJAAIAAADRAAAAFgD#####Af8AAAAQAAAAAAAAAAAAAABACAAAAAAAAAAACQABAAAA0QAAABUA#####wAAAGIAAADQAAAAFgD#####Af8AAAAQAAAAAAAAAAAAAABACAAAAAAAAAAACQACAAAA1AAAABYA#####wH#AAAAEAAAAAAAAAAAAAAAQAgAAAAAAAAAAAkAAQAAANQAAAAVAP####8AAABnAAAA0AAAABYA#####wH#AAAAEAAAAAAAAAAAAAAAQAgAAAAAAAAAAAkAAgAAANcAAAAWAP####8B#wAAABAAAAAAAAAAAAAAAEAIAAAAAAAAAAAJAAEAAADXAAAAFQD#####AAAAZwAAAM8AAAAWAP####8B#wAAABAAAAAAAAAAAAAAAEAIAAAAAAAAAAAJAAEAAADaAAAAFgD#####Af8AAAAQAAAAAAAAAAAAAABACAAAAAAAAAAACQACAAAA2gAAAB0A#####wAAAAAAA2FhMAADAAAAVgAAANkAAADWAAAAHQD#####AAAAAAADYWExAAMAAABWAAAA1gAAANgAAAAdAP####8AAAAAAANhYTIAAwAAAFYAAADYAAAA1QAAAB0A#####wAAAAAAA2FhMwADAAAAVgAAANUAAADZAAAAHQD#####AAAAAAADYWE0AAMAAABUAAAA2wAAANMAAAAdAP####8AAAAAAANhYTUAAwAAAFQAAADTAAAA3AAAAB0A#####wAAAAAAA2FhNgADAAAAVAAAANwAAADSAAAAHQD#####AAAAAAADYWE3AAMAAABUAAAA0gAAANsAAAAH##########8='
    const A = addMaj(nomspris)
    const B = addMaj(nomspris)
    const C = addMaj(nomspris)
    const D = addMaj(nomspris)
    const E = addMaj(nomspris)
    const F = addMaj(nomspris)
    const G = addMaj(nomspris)
    const H = addMaj(nomspris)
    stor.lettres = [A, B, C, D, E, F, G, H]
    stor.listangle = [H + A + C, C + A + B, B + A + G, G + A + H, A + B + D, D + B + E, E + B + F, F + B + A]
    svgId = j3pGetNewId()
    stor.svgId = svgId
    j3pCreeSVG(stor.lesdiv.figure, { id: svgId, width: 400, height: 350, style: { border: 'solid black 1px' } })
    stor.mtgAppLecteur.removeAllDoc()
    stor.mtgAppLecteur.addDoc(svgId, txtFigure, true)
    stor.mtgAppLecteur.giveFormula2(svgId, 'A1', String(j3pGetRandomInt(0, 359)))
    const A2 = j3pGetRandomInt(0, 5)
    let A3 = j3pGetRandomInt(0, 5)
    if (A2 === A3 && A2 === 0) A3 = 1
    stor.mtgAppLecteur.giveFormula2(svgId, 'A2', String(A2))
    stor.mtgAppLecteur.giveFormula2(svgId, 'A3', String(A3))
    stor.mtgAppLecteur.giveFormula2(svgId, 'A4', String(j3pGetRandomInt(0, 5)))
    stor.mtgAppLecteur.setText(svgId, '#npA', String(A))
    stor.mtgAppLecteur.setText(svgId, '#npB', String(B))
    stor.mtgAppLecteur.setText(svgId, '#npC', String(C))
    stor.mtgAppLecteur.setText(svgId, '#npD', String(D))
    stor.mtgAppLecteur.setText(svgId, '#npE', String(E))
    stor.mtgAppLecteur.setText(svgId, '#npF', String(F))
    stor.mtgAppLecteur.setText(svgId, '#npG', String(G))
    stor.mtgAppLecteur.setText(svgId, '#npH', String(H))
    stor.mtgAppLecteur.calculateAndDisplayAll(true)

    // lettres=['0','1','2','3','4','5','6','7','8','9','10','11','12']

    stor.angle1 = -1
    stor.listAnglSel = [false, false, false, false, false, false, false, false]
    if (stor.Selectionne1) {
      stor.angle1 = j3pGetRandomInt(0, 7)
      stor.angle2 = []
      switch (stor.pbencours) {
        case 'angles opposés par le sommet': stor.angle2 = opopop(stor.angle1)
          break
        case 'angles alternes-internes': // pas 0 //pas 1 //pas 4 //pas 6
          switch (stor.angle1) {
            case 0: stor.angle1 = 1
              break
            case 3 : stor.angle1 = 2
              break
            case 5: stor.angle1 = 4
              break
            case 6 : stor.angle1 = 7
              break
          }
          stor.angle2 = alalt(stor.angle1)
          break
        case 'angles correspondants': stor.angle2 = cococo(stor.angle1)
          break
        case 'angles alternes-externes': // pas 0 //pas 1 //pas 4 //pas 6
          switch (stor.angle1) {
            case 1: stor.angle1 = 0
              break
            case 2 : stor.angle1 = 3
              break
            case 4: stor.angle1 = 5
              break
            case 7 : stor.angle1 = 6
              break
          }
          stor.angle2 = alalte(stor.angle1)

          break
        case 'angles adjacents supplémentaires': stor.angle2 = supsup(stor.angle1)
          break
        default :
          j3pShowError(Error(`Cas non prévu (${stor.pbencours})`), { message: 'Erreur interne (cas non prévu)', mustNotify: true })
      }
      stor.mtgAppLecteur.setColor(svgId, '#aa' + (stor.angle1), 255, 0, 0, true)
      stor.mtgAppLecteur.setLineStyle(svgId, '#aa' + (stor.angle1), 0, 5, true)
      stor.mtgAppLecteur.setVisible(svgId, '#a' + (stor.angle1), false, true)
      stor.mtgAppLecteur.setColor(svgId, '#s' + (stor.angle1), 255, 100, 200, true)
    }

    for (let i = 0; i < 8; i++) {
      if (i !== stor.angle1) {
        stor.mtgAppLecteur.addEventListener(svgId, '#a' + i, 'mouseover', makFoncOver(i))
        stor.mtgAppLecteur.addEventListener(svgId, '#a' + i, 'mouseout', makFoncOut(i))
        stor.mtgAppLecteur.addEventListener(svgId, '#a' + i, 'click', makFoncClik(i))
        stor.mtgAppLecteur.setColor(svgId, '#a' + i, 0, 0, 0, true, 0)
      }
    }
  }
  function makFoncOver (n) {
    return function () {
      if (stor.raz) return
      stor.mtgAppLecteur.getDoc(svgId).defaultCursor = 'pointer'
      if (stor.listAnglSel[n]) {
        stor.mtgAppLecteur.setColor(svgId, '#aa' + n, 200, 50, 100, true)
        stor.mtgAppLecteur.setLineStyle(svgId, '#aa' + n, 0, 5, true)
        stor.mtgAppLecteur.setColor(svgId, '#s' + n, 150, 50, 100, true)
      } else {
        stor.mtgAppLecteur.setColor(svgId, '#aa' + n, 0, 0, 0, true)
        stor.mtgAppLecteur.setLineStyle(svgId, '#aa' + n, 0, 6, true)
        stor.mtgAppLecteur.setColor(svgId, '#s' + n, 100, 100, 100, true)
      }
    }
  }
  function makFoncOut (n) {
    return function () {
      if (stor.raz) return
      stor.mtgAppLecteur.getDoc(svgId).defaultCursor = ''
      if (stor.listAnglSel[n]) {
        // stor.mtgAppLecteur.setColor(svgId, '#a' + n, 255, 0, 0, true)
        // stor.mtgAppLecteur.setLineStyle(svgId, '#a' + n, 0, 5, true)
        stor.mtgAppLecteur.setColor(svgId, '#aa' + n, 255, 0, 0, true)
        stor.mtgAppLecteur.setLineStyle(svgId, '#aa' + n, 0, 5, true)
        stor.mtgAppLecteur.setColor(svgId, '#s' + n, 255, 100, 200, true)
      } else {
        stor.mtgAppLecteur.setColor(svgId, '#aa' + n, 0, 0, 0, true)
        stor.mtgAppLecteur.setLineStyle(svgId, '#aa' + n, 0, 3, true)
        stor.mtgAppLecteur.setColor(svgId, '#s' + n, 200, 200, 200, true)
      }
    }
  }
  function makFoncClik (n) {
    return function () {
      if (stor.raz) return
      stor.listAnglSel[n] = !stor.listAnglSel[n]
      if (stor.listAnglSel[n]) {
        stor.mtgAppLecteur.setColor(svgId, '#aa' + n, 255, 0, 0, true)
        // stor.mtgAppLecteur.setColor(svgId, '#a' + n, 255, 0, 0, true)
        stor.mtgAppLecteur.setLineStyle(svgId, '#aa' + n, 0, 5, true)
        // stor.mtgAppLecteur.setLineStyle(svgId, '#a' + n, 0, 5, true)
        stor.mtgAppLecteur.setColor(svgId, '#s' + n, 255, 100, 200, true)
      } else {
        stor.mtgAppLecteur.setColor(svgId, '#aa' + n, 0, 0, 0, true)
        stor.mtgAppLecteur.setLineStyle(svgId, '#aa' + n, 0, 3, true)
        // stor.mtgAppLecteur.setColor(svgId, '#a' + n, 0, 0, 0, true)
        // stor.mtgAppLecteur.setLineStyle(svgId, '#a' + n, 0, 3, true)
        stor.mtgAppLecteur.setColor(svgId, '#s' + n, 200, 200, 200, true)
      }
    }
  }
  function afcofo (boolfin) {
    stor.lesdiv.solution.innerHTML = ''
    stor.lesdiv.explications.innerHTML = ''
    let yaexplik = false
    let yaco = false

    let bufn = ''
    if (me.ErrEcritureAngle3) {
      j3pAffiche(stor.lesdiv.explications, null, 'Il faut trois lettres pour écrire le nom d’un angle !')
      bufn = '\n'
      yaexplik = true
      me.typederreurs[4]++
    }
    if (me.ErrAnglePrez) {
      j3pAffiche(stor.lesdiv.explications, null, bufn + 'Il faut nommer un angle tracé !')
      bufn = '\n'; yaexplik = true; me.typederreurs[4]++
    }
    if (me.ErrEcritureAngleMaj) {
      j3pAffiche(stor.lesdiv.explications, null, bufn + 'Il n’y a pas de minuscule dans cette figure !')
      bufn = '\n'; yaexplik = true; me.typederreurs[4]++
    }
    if (me.ErrEcritureAngleDouble) {
      j3pAffiche(stor.lesdiv.explications, null, bufn + 'Tu as nommé deux fois le même angle !')
      bufn = '\n'; yaexplik = true
    }
    if (me.ErrEcritureAngleIcon) {
      j3pAffiche(stor.lesdiv.explications, null, bufn + 'Toutes les lettres doivent être sur la figure !')
      bufn = '\n'; yaexplik = true
    }
    if (me.ErrEcritureAngleChap) {
      j3pAffiche(stor.lesdiv.explications, null, bufn + 'Il faut un chapeau pointu pour nommer un angle !')
      bufn = '\n'; yaexplik = true; me.typederreurs[4]++
    }
    if (me.ErrEcritureAngleSom) {
      j3pAffiche(stor.lesdiv.explications, null, bufn + "Le sommet d’un angle s'écrit au milieu !")
      bufn = '\n'; yaexplik = true; me.typederreurs[4]++
    }
    if (me.ErrPasPosInt) {
      j3pAffiche(stor.lesdiv.explications, null, bufn + 'Les angles alternes-internes sont entre les deux droites !')
      bufn = '\n'; yaexplik = true
    }
    if (me.ErrPasPosExt) {
      j3pAffiche(stor.lesdiv.explications, null, bufn + 'Les angles alternes-externes ne sont pas entre les deux droites !')
      yaexplik = true
    }

    // met rouge
    if (me.errAg1) {
      switch (stor.modeencours) {
        case 'c1':
        case 'c2': corrige(false)
          break
        default: stor.zone.corrige(false)
      }
    }

    if (me.errAg2) {
      switch (stor.modeencours) {
        case 'c1':
        case 'c2': corrige(false)
          break
        case 'n2': stor.zone2.corrige(false)
      }
    }

    if (me.err) {
      switch (stor.modeencours) {
        case 'n2': stor.zone2.corrige(false)
        // eslint-disable-next-line no-fallthrough
        case 'n1': stor.zone.corrige(false)
          break
        case 'c2': corrige(stor.num2, false)
        // eslint-disable-next-line no-fallthrough
        case 'c1': corrige(stor.num1, false)
          break
      }
    }

    if (boolfin) {
      switch (stor.modeencours) {
        case 'n2': stor.zone2.disable()
        // eslint-disable-next-line no-fallthrough
        case 'n1': stor.zone.disable()
          j3pDetruit('cccSSS')
          break
      }

      stor.raz = true

      if (stor.modeencours !== 'n2') {
        me.repAtt = stor.angle2[0]
      } else {
        stor.angle1 = j3pGetRandomInt(0, 7)
        if (stor.pbencours === 'angles alternes-internes') { stor.angle1 = 2 }
        if (stor.pbencours === 'angles alternes-externes') { stor.angle1 = 0 }
      }

      switch (stor.modeencours) {
        case 'c2': if (me.foa !== -1) {
          corrige(me.foa, false)
        }
          corrige(stor.num2, undefined)
        // eslint-disable-next-line no-fallthrough
        case 'c1':
          corrige(stor.angle2[0], undefined)
          break
        default: {
          let buf = 'Les angles ' + affNomAngleExpr(stor.listangle[stor.angle1]) + '  et  '
          switch (stor.pbencours) {
            case 'angles opposés par le sommet':
              stor.angle2 = opopop(stor.angle1)
              break
            case 'angles alternes-internes':
              stor.angle2 = alalt(stor.angle1)
              break
            case 'angles correspondants':
              stor.angle2 = cococo(stor.angle1)
              break
            case 'angles alternes-externes':
              stor.angle2 = alalte(stor.angle1)
              break
            case 'angles adjacents supplémentaires':
              stor.angle2 = supsup(stor.angle1)
              break
            default :
              j3pShowError(Error(`Cas non prévu (${stor.pbencours})`), {
                message: 'Erreur interne (cas non prévu)',
                mustNotify: true
              })
          }

          buf += affNomAngleExpr(stor.listangle[stor.angle2[0]]) + '  sont  ' + stor.advi2
          j3pAffiche(stor.lesdiv.solution, null, buf)
          yaco = true
        }
      }
      if (me.err) {
        switch (stor.modeencours) {
          case 'n2': stor.zone2.barre()
          // eslint-disable-next-line no-fallthrough
          case 'n1': stor.zone.barre()
            break
        }
      }
      if ((me.errAg1) && (stor.nomme)) { stor.zone.barre() }
      if ((me.errAg2) && (stor.modeencours === 'n2')) { stor.zone2.barre() }
    }
    if (yaexplik) {
      stor.lesdiv.explications.classList.add('explique')
    }
    if (yaco) {
      stor.lesdiv.solution.classList.add('correction')
    }
  }

  function listeselect () {
    const aret = { oui: [], non: [] }
    for (let i = 0; i < stor.listAnglSel.length; i++) {
      if (stor.listAnglSel[i]) {
        aret.oui.push([i])
      } else {
        aret.non.push([i])
      }
    }
    return aret
  }
  function yareponse () {
    switch (stor.modeencours) {
      case 'c1': me.rep = listeselect()
        if (me.rep.oui.length === 0) { return false }
        if (me.rep.oui.length > 1) { return 'ERR' }
        return true
      case 'c2' :me.rep = listeselect()
        if (me.rep.oui.length < 2) { return false }
        if (me.rep.oui.length > 2) { return 'ERR' }
        return true
      case 'n1' :me.rep = stor.zone.reponsechap()
        if (me.rep[0] === '') { stor.zone.focus(); return false }
        return true
      case 'n2' :me.rep = stor.zone.reponsechap()
        me.rep2 = stor.zone2.reponsechap()
        if (me.rep[0] === '') { stor.zone.focus(); return false }
        if (me.rep2[0] === '') { stor.zone2.focus(); return false }
        return true
    }
  }
  function bonneReponse () {
    me.ErrEcritureAngle3 = false
    me.ErrAnglePrez = false
    me.ErrEcritureAngleMaj = false
    me.ErrEcritureAngleDouble = false
    me.ErrEcritureAngleIcon = false
    me.ErrEcritureAngleChap = false
    me.ErrEcritureAngleSom = false

    me.err = false
    me.ErrPasPosInt = false
    me.ErrPasPosExt = false

    me.errAg1 = false
    me.errAg2 = false

    me.foa = -1

    function tetang (f) {
      // 3 lettres
      if (f[0].length < 3) {
        me.ErrEcritureAngle3 = true
        return false
      }
      // majuscules
      for (let i = 0; i < 3; i++) {
        if ('abcdefghijklmnopqrstuvwxyz'.indexOf(f[0][i]) !== -1) {
          me.ErrEcritureAngleMaj = true
          return false
        }
      }
      // pas deux fois la mm
      for (let i = 0; i < 2; i++) {
        for (let j = i + 1; j < 3; j++) {
          if (f[0][i] === f[0][j]) {
            me.ErrEcritureAngleDouble = true
            return false
          }
        }
      }
      // des lettres presentes
      for (let i = 0; i < 3; i++) {
        if (stor.lettres.indexOf(f[0][i]) === -1) {
          me.ErrEcritureAngleIcon = true
          return false
        }
      }
      if (f[1] !== 1) { me.ErrEcritureAngleChap = true; return false }
      return true
    }
    function existeangle (ki) {
      const k1 = ki[0]; const k2 = ki[1]; const k3 = ki[2]
      if ((k2 !== stor.lettres[0]) && (k2 !== stor.lettres[1])) {
        me.ErrEcritureAngleSom = true
        return false
      }
      switch (k1) {
        case stor.lettres[0] : if (k2 !== stor.lettres[1]) { return false }
          if ((k3 !== stor.lettres[3]) && (k3 !== stor.lettres[5])) { return false }
          break
        case stor.lettres[1] : if (k2 !== stor.lettres[0]) { return false }
          if ((k3 !== stor.lettres[2]) && (k3 !== stor.lettres[6])) { return false }
          break
        case stor.lettres[2] : if (k2 !== stor.lettres[0]) { return false }
          if ((k3 !== stor.lettres[1]) && (k3 !== stor.lettres[4]) && (k3 !== stor.lettres[7])) { return false }
          break
        case stor.lettres[3] : if (k2 !== stor.lettres[1]) { return false }
          if ((k3 !== stor.lettres[4]) && (k3 !== stor.lettres[0]) && (k3 !== stor.lettres[7])) { return false }
          break
        case stor.lettres[5] : if (k2 !== stor.lettres[1]) { return false }
          if ((k3 !== stor.lettres[4]) && (k3 !== stor.lettres[0]) && (k3 !== stor.lettres[7])) { return false }
          break
        case stor.lettres[6] : if (k2 !== stor.lettres[0]) { return false }
          if ((k3 !== stor.lettres[7]) && (k3 !== stor.lettres[1]) && (k3 !== stor.lettres[4])) { return false }
          break
        case stor.lettres[4] : if (k2 === stor.lettres[0]) {
          if ((k3 !== stor.lettres[2]) && (k3 !== stor.lettres[6])) { return false }
        }
          if (k2 === stor.lettres[1]) {
            if ((k3 !== stor.lettres[3]) && (k3 !== stor.lettres[5])) { return false }
          }
          break
        case stor.lettres[7] : if (k2 === stor.lettres[0]) {
          if ((k3 !== stor.lettres[2]) && (k3 !== stor.lettres[6])) { return false }
        }
          if (k2 === stor.lettres[1]) {
            if ((k3 !== stor.lettres[3]) && (k3 !== stor.lettres[5])) { return false }
          }
          break
      }
      return true
    }
    function tradangle (ki) {
      const k1 = ki[0]; const k3 = ki[2]
      switch (k1) {
        case stor.lettres[0] :
          if (k3 === stor.lettres[3]) return 4
          return 7
        case stor.lettres[1] :
          if (k3 === stor.lettres[2]) return 1
          return 2
        case stor.lettres[2] :
          if (k3 === stor.lettres[7]) return 0
          return 1
        case stor.lettres[3] :
          if (k3 === stor.lettres[4]) return 5
          return 4
        case stor.lettres[4] :
          if (k3 === stor.lettres[3]) return 5
          if (k3 === stor.lettres[2]) return 1
          if (k3 === stor.lettres[5]) return 6
          return 2
        case stor.lettres[5] :
          if (k3 === stor.lettres[4]) return 6
          return 7
        case stor.lettres[6] :
          if (k3 === stor.lettres[7]) return 3
          return 2
        case stor.lettres[7] :
          if (k3 === stor.lettres[3]) return 4
          if (k3 === stor.lettres[2]) return 0
          if (k3 === stor.lettres[5]) return 7
          return 3
      }
      return true
    }
    // si nomme test c’est bien des angles

    let num1
    let num2
    if (stor.nomme) {
      // si nomme test c’est bien des angles
      if (!tetang(me.rep)) { me.errAg1 = true }
      if (!stor.Selectionne1) {
        if (!tetang(me.rep2)) { me.errAg2 = true }
      }
      if (me.errAg1 || me.errAg2) { return false }
      // si nomme test c’est des angles de la figure
      if (!existeangle(me.rep[0])) { me.ErrAnglePrez = true; me.errAg1 = true }
      if (!stor.Selectionne1) {
        if (!existeangle(me.rep2[0])) { me.ErrAnglePrez = true; me.errAg2 = true }
      }
      if (me.errAg1 || me.errAg2) { return false }
      // trad les deux angles en chiffres
      num1 = tradangle(me.rep[0])
      if (stor.Selectionne1) { num2 = stor.angle1 } else { num2 = tradangle(me.rep2[0]) }
      // si nomme est on a pas 2 fois le mm
      if (num1 === num2) {
        me.errAg1 = true
        me.ErrEcritureAngleDouble = true; return false
      }
    }

    if (stor.modeencours === 'c2') {
      num1 = me.rep.oui[0][0]
      num2 = me.rep.oui[1][0]
    }
    if (stor.modeencours === 'c1') {
      num1 = me.rep.oui[0][0]
      num2 = stor.angle1
    }
    if (stor.pbencours === 'angles alternes-internes') {
      if ([0, 3, 5, 6].indexOf(num1) !== -1) { me.ErrPasPosInt = true; me.errAg1 = true }
      if ([0, 3, 5, 6].indexOf(num2) !== -1) { me.ErrPasPosInt = true; me.errAg2 = true }
    }
    let bb
    if (me.ErrPasPosInt) {
      if (([0, 3, 5, 6].indexOf(num1) !== -1) && ([0, 3, 5, 6].indexOf(num2) !== -1)) {
        me.foa = num2
        num2 = 2
      } else if ([0, 3, 5, 6].indexOf(num2) !== -1) {
        bb = num2
        num2 = num1
        num1 = bb
      }
    }

    if (stor.pbencours === 'angles alternes-externes') {
      if ([1, 2, 4, 7].indexOf(num1) !== -1) {
        me.ErrPasPosExt = true
        me.errAg1 = true
      }
      if ([1, 2, 4, 7].indexOf(num2) !== -1) {
        me.ErrPasPosExt = true
        me.errAg2 = true
      }
    }
    if (me.ErrPasPosExt) {
      if (([2, 3, 5, 7].indexOf(num1) !== -1) && ([2, 3, 5, 7].indexOf(num2) !== -1)) {
        me.foa = num2
        num2 = 0
      } else if ([2, 3, 5, 7].indexOf(num2) !== -1) {
        bb = num2; num2 = num1; num1 = bb
      }
    }

    switch (stor.pbencours) {
      case 'angles opposés par le sommet': stor.angle2 = opopop(num2)
        break
      case 'angles alternes-internes':stor.angle2 = alalt(num2)
        break
      case 'angles correspondants': stor.angle2 = cococo(num2)
        break
      case 'angles alternes-externes': stor.angle2 = alalte(num2)
        break
      case 'angles adjacents supplémentaires': stor.angle2 = supsup(num2)
        break
      default :
        j3pShowError(Error(`Cas non prévu (${stor.pbencours})`), { message: 'Erreur interne (cas non prévu)', mustNotify: true })
    }
    stor.num1 = num1
    stor.num2 = num2
    try {
      if (stor.angle2.indexOf(num1) === -1) {
        me.err = true
        return false
      }
    } catch (e) {
      j3pNotify('pour Tom', { pbencours: stor.pbencours, num2, modeencours: stor.modeencours })
    }
    return true
  } // bonneReponse

  function affimodale (texte) {
    const yy = j3pModale({ titre: 'Erreur', contenu: '' })
    j3pAffiche(yy, null, texte)
    yy.style.color = me.styles.toutpetit.correction.color
  }

  function initSection () {
    ds.pe = []
    // Construction de la page
    me.construitStructurePage('presentation3')
    stor.pbpos = []
    if (ds.oppose) { stor.pbpos.push('angles opposés par le sommet') }
    if (ds.alternes_internes) { stor.pbpos.push('angles alternes-internes') }
    if (ds.correspondants) { stor.pbpos.push('angles correspondants') }
    if (ds.alternes_externes) { stor.pbpos.push('angles alternes-externes') }
    if (ds.supplementaires_et_adjacents) { stor.pbpos.push('angles adjacents supplémentaires') }
    // FIXME, ds.adjacents n’existe pas
    if (ds.adjacents) { stor.pbpos.push('angles adjacents') }
    if (stor.pbpos.length === 0) {
      j3pShowError('Le paramétrage ne permet pas de construire l’exercice => le cas retenu sera celui des angles opposés par le sommet')
      stor.pbpos.push('angles opposés par le sommet')
    }

    stor.modepos = []
    switch (ds.mode) {
      case 'Click1' : stor.modepos = ['c1']
        break
      case 'Click2' : stor.modepos = ['c2']
        break
      case 'Nomme1' : stor.modepos = ['n1']
        break
      case 'Nomme2' : stor.modepos = ['n2']
        break
      case 'Nomme1_Click1' : stor.modepos = ['c1', 'n1']
        break
      case 'Nomme2_Click2' : stor.modepos = ['c2', 'n2']
        break
      case 'Click 1 ou 2' : stor.modepos = ['c1', 'c2']
        break
      case 'Nomme 1 ou 2' : stor.modepos = ['n1', 'n2']
        break
      default : stor.modepos = ['c1', 'c2', 'n1', 'n2']
    }

    me.typederreurs = [0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0]
    me.score = 0
    let letitredeb = 'Retrouver des angles associés '
    if (stor.pbpos.length === 1) { letitredeb = stor.pbpos[0] }

    me.afficheTitre(letitredeb)

    stor.pbfaits = []
    stor.modefaits = []

    // chargement mtg
    getMtgCore({ withMathJax: true })
      .then(
        // success
        (mtgAppLecteur) => {
          stor.mtgAppLecteur = mtgAppLecteur
          stor.listePourCalc = mtgAppLecteur.createList('TWF0aEdyYXBoSmF2YTEuMAAAABI+TMzNAAJmcv###wEA#wEAAAAAAAAAAAQsAAACvQAAAQEAAAAAAAAAAQAAAAP#####AAAAAQAKQ0NhbGNDb25zdAD#####AAJwaQAWMy4xNDE1OTI2NTM1ODk3OTMyMzg0Nv####8AAAABAApDQ29uc3RhbnRlQAkh+1RELRj#####AAAAAQAHQ0NhbGN1bAD#####AAFlAAZleHAoMSn#####AAAAAgAJQ0ZvbmN0aW9uBwAAAAE#8AAAAAAAAAAAAAIA#####wABeAABMAAAAAEAAAAAAAAAAP###############w==')
          enonceMain()
        },
        // failure
        (error) => {
          j3pShowError(error, { message: 'mathgraph n’est pas correctement chargé', mustNotify: true })
        })
      // plantage dans le code de success
      .catch(j3pShowError)
  } // initSection

  function enonceMain () {
    me.videLesZones()
    stor.raz = false
    stor.lesdiv = {}
    stor.lesdiv.conteneur = j3pAddElt(me.zonesElts.MG, 'div', {
      style: me.styles.etendre('toutpetit.enonce', { padding: '10px' })
    })
    stor.lesdiv.consigne = addDefaultTable(stor.lesdiv.conteneur, 1, 1)[0][0]
    const tab = addDefaultTable(stor.lesdiv.conteneur, 1, 3)
    const tab2 = addDefaultTable(tab[0][2], 4, 1)
    tab[0][1].style.width = '10px'
    stor.lesdiv.figure = tab[0][0]
    stor.lesdiv.travail = tab2[1][0]
    stor.lesdiv.explications = tab2[2][0]
    stor.lesdiv.solution = tab2[3][0]
    stor.lesdiv.correction = tab2[0][0]
    tab2[0][0].style.height = '50px'
    stor.lesdiv.explications.style.color = me.styles.cfaux
    stor.lesdiv.solution.style.color = me.styles.petit.correction.color
    me.zonesElts.MG.classList.add('fond')
    stor.lesdiv.consigne.classList.add('enonce')
    // selectionne pb et mode
    let cptuse = 0
    let oksort = false
    do {
      stor.pbencours = stor.pbpos[j3pGetRandomInt(0, (stor.pbpos.length - 1))]
      cptuse++
      oksort = (stor.pbfaits.indexOf(stor.pbencours) === -1)
    } while ((!oksort) && (cptuse < 50))
    stor.pbfaits.push(stor.pbencours)

    let advi = ''
    let advi2 = ''
    switch (stor.pbencours) {
      case 'angles opposés par le sommet':
        advi = 'l’angle opposé par le sommet  à l’angle '
        advi2 = 'opposés par le sommet.'
        break
      case 'angles alternes-internes':
        advi = 'l’angle alterne-interne à l’angle '
        advi2 = 'alternes-internes.'
        break
      case 'angles correspondants':
        advi = 'l’angle correspondant à l’angle '
        advi2 = 'correspondants.'
        break
      case 'angles alternes-externes':
        advi = 'l’angle alterne-externe à l’angle '
        advi2 = 'alternes-externes.'
        break
      case 'angles adjacents supplémentaires':
        advi = 'un angle adjacent et supplémentaire à l’angle '
        advi2 = 'adjacents et supplémentaires.'
        break
      default :
        j3pShowError(Error(`Cas non prévu (${stor.pbencours})`), { message: 'Erreur interne (cas non prévu)', mustNotify: true })
    }

    stor.advi2 = advi2

    cptuse = 0
    oksort = false
    do {
      stor.modeencours = stor.modepos[j3pGetRandomInt(0, (stor.modepos.length - 1))]
      cptuse++
      oksort = (stor.modefaits.indexOf(stor.modeencours) === -1)
    } while ((!oksort) && (cptuse < 50))

    stor.modefaits.push(stor.modeencours)

    let maquest1 = ''
    let maquest2 = ''
    stor.Selectionne1 = false
    stor.nomme = false

    switch (stor.modeencours) {
      case 'c1':
        stor.Selectionne1 = true
        maquest1 = `<b>Sélectionne ${advi}</b>`
        break
      case 'n1':
        stor.Selectionne1 = true
        stor.nomme = true
        break
      case 'n2':
        stor.nomme = true
        break
      case 'c2':
        maquest1 = '<b>Sélectionne deux angles </b>' + advi2
        break
    }

    if (stor.nomme) {
      stor.lesdiv.travail.classList.add('travail')
      stor.lesdiv.figure.classList.add('enonce')
    } else {
      stor.lesdiv.figure.classList.add('travail')
    }
    stor.lesdiv.solution.classList.remove('correction')
    stor.lesdiv.explications.classList.remove('explique')
    /// la fig

    afficheFig()
    switch (stor.modeencours) {
      case 'c1':
        maquest1 += affNomAngleExpr(stor.listangle[stor.angle1])
        j3pAffiche(stor.lesdiv.consigne, null, maquest1)
        break
      case 'n1': {
        maquest2 = 'Les angles ' + affNomAngleExpr(stor.listangle[stor.angle1]) + ' et&nbsp; '
        j3pAffiche(stor.lesdiv.consigne, null, '<b>Observe la figure puis complète la phrase ci-dessous.</b>')
        const tttab = addDefaultTable(stor.lesdiv.travail, 1, 3)
        j3pAffiche(tttab[0][0], null, maquest2)
        stor.zone = new ZoneStyleMathquill1(tttab[0][1], {
          restric: 'ABCDEFGHIJKLMNOPQRSTUVWXYZabcdefghijklmnopqrstuvwxyz[]().^$',
          limitenb: 9, // FIXME, cette option n’existe pas dans ZoneStyleMathquill1
          limite: 3,
          hasAutoKeyboard: true // FIXME, cette option n’existe pas dans ZoneStyleMathquill1
        })
        j3pAffiche(tttab[0][2], null, '&nbsp; sont ' + advi2)
        tttab[0][2].style.verticalAlign = 'bottom'
        tttab[0][1].style.verticalAlign = 'bottom'
      }
        break
      case 'n2': {
        j3pAffiche(stor.lesdiv.consigne, null, '<b>Observe la figure puis complète la phrase ci-dessous.</b>')
        const tttab = addDefaultTable(stor.lesdiv.travail, 1, 5)
        j3pAffiche(tttab[0][0], null, 'Les angles&nbsp;')
        stor.zone = new ZoneStyleMathquill1(tttab[0][1], {
          restric: 'ABCDEFGHIJKLMNOPQRSTUVWXYZabcdefghijklmnopqrstuvwxyz[]().^$',
          limitenb: 9,
          limite: 3,
          hasAutoKeyboard: true
        })
        j3pAffiche(tttab[0][2], null, '&nbsp; et &nbsp;')
        stor.zone2 = new ZoneStyleMathquill1(tttab[0][3], {
          restric: 'ABCDEFGHIJKLMNOPQRSTUVWXYZabcdefghijklmnopqrstuvwxyz[]().^$',
          limitenb: 9,
          limite: 3,
          hasAutoKeyboard: true
        })
        j3pAffiche(tttab[0][4], null, '&nbsp; sont ' + advi2)
      }
        break
      case 'c2':
        j3pAffiche(stor.lesdiv.consigne, null, maquest1)
        break
    }
    me.finEnonce()
  }

  switch (this.etat) {
    case 'enonce':
      // code exécuté au lancement de la section
      if (this.debutDeLaSection) {
        initSection()
      } else {
        enonceMain()
      }
      break // case "enonce":

    case 'correction': {
      // On teste si une réponse a été saisie
      stor.lesdiv.solution.classList.remove('correction')
      stor.lesdiv.explications.classList.remove('explique')
      const tt = yareponse()

      if (tt === 'ERR') {
        affimodale('Tu as sélectionné trop d’angles !!')
        return
      }

      if (!tt) {
        // ici on donne le message à afficher si une ou plusieurs zone(s) n’est pas complétée (tout d’abord on met en rouge : styles.cfaux)
        stor.lesdiv.correction.style.color = this.styles.cfaux
        stor.lesdiv.correction.innerHTML = reponseIncomplete
        return this.finCorrection() // on reste en correction
      }
      // Une réponse a été saisie
      // Bonne réponse
      if (bonneReponse()) {
        this.score += j3pArrondi(1 / ds.nbetapes, 1)
        stor.lesdiv.correction.style.color = this.styles.cbien
        stor.lesdiv.correction.innerHTML = cBien

        /// destout
        switch (stor.modeencours) {
          case 'c2':
            corrige(stor.num2, true)
            corrige(stor.num1, true)
            break
          case 'c1':
            corrige(stor.num1, true)
            break
          case 'n2':
            stor.zone2.corrige(true)
            stor.zone2.disable()
            stor.zone.corrige(true)
            stor.zone.disable()
            break
          case 'n1':
            stor.zone.corrige(true)
            stor.zone.disable()
            break
          default:
            console.error(Error(`modeencours ${stor.modeencours} inconnu`))
        }
        stor.raz = true
        /// mets toutvert
        this.typederreurs[0]++
        return this.finCorrection('navigation', true)
      }

      // Pas de bonne réponse
      stor.lesdiv.correction.style.color = this.styles.cfaux

      // A cause de la limite de temps :
      if (this.isElapsed) { // limite de temps
        afcofo(true)
        return this.finCorrection('navigation', true)
      }

      // Réponse fausse :
      stor.lesdiv.correction.innerHTML = cFaux
      if (me.essaiCourant < ds.nbchances) {
        stor.lesdiv.correction.innerHTML += '<br>' + essaieEncore
        afcofo(false)
        this.typederreurs[1]++
        return this.finCorrection() // on reste en correction
      }

      // Erreur au dernier essai
      stor.lesdiv.correction.innerHTML += '<br>' + regardeCorrection
      afcofo(true)
      this.typederreurs[2]++
      this.finCorrection('navigation', true)
      break // case "correction":
    }

    case 'navigation':
      if (this.sectionTerminee()) {
        // On détermine la phrase d’état renvoyée par la section
        if ((me.typederreurs[4] / ds.nbitems) > 0.3) {
          this.parcours.pe = ds.pe_1
        } else {
          this.parcours.pe = ''
        }
      } else {
        this.etat = 'enonce'
      }
      this.finNavigation()
      break // case "navigation":
  }
}
