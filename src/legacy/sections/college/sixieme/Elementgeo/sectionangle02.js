import { j3pAddElt, j3pAjouteBouton, j3pArrondi, j3pGetNewId, j3pGetRandomInt, j3pShowError, j3pShuffle } from 'src/legacy/core/functions'
import ListeDeroulante from 'src/legacy/outils/listeDeroulante/ListeDeroulante'
import { j3pAffiche } from 'src/lib/mathquill/functions'
import { addDefaultTable } from 'src/legacy/themes/table'
import { j3pCreeSVG } from 'src/legacy/core/functionsSvg'
import textesGeneriques from 'src/lib/core/textes'
import { getMtgCore } from 'src/lib/outils/mathgraph'
import { addMaj } from 'src/lib/utils/string'

const { cBien, cFaux, essaieEncore, reponseIncomplete, tempsDepasse } = textesGeneriques

/**
 * Ajouter une description ici
 * @fileOverview
 * @author Tommy Barroy <t.barroy@laposte.net>
 * @since 2017-10
*/
export const params = {
  outils: ['mathquill'],
  /**
   * Ce qui est paramétrable dans l’éditeur de graphe
   * @type {LegacySectionParamsParametres}
   */
  parametres: [
    ['nbrepetitions', 3, 'entier', 'Nombre de répétitions de la section'],
    ['nbchances', 1, 'entier', 'Nombre d’essais possibles.'],
    ['limite', 0, 'entier', 'Temps disponible par répétition, en secondes'],
    ['Nul', true, 'boolean', '<u>true</u>: L’angle peut être nul.'],
    ['Droit', true, 'boolean', '<u>true</u>: L’angle peut être droit.'],
    ['Plat', true, 'boolean', '<u>true</u>: L’angle peut être plat.'],
    ['Donnees', 'Figure', 'liste', '<u>Figure</u>: L’angle est tracé. <br><br> <u>Mesure</u> La mesure est donnée.', ['Figure', 'Mesure', 'les deux']],
    ['Equerre', true, 'boolean', '<u>true</u>: Une équerre est disponible.'],
    // le modèle gère la compatibilité ascendante avec l’ancien paramètre MiseEnPagePlusClaire
    ['theme', 'standard', 'liste', 'Modèle pour l’affichage', ['standard', 'zonesAvecImageDeFond']]
  ]
}

/**
 * section angle02
 * @this {Parcours}
 */
export default function main () {
  const me = this
  const stor = this.storage
  const ds = this.donneesSection
  let svgId = stor.svgId

  function initSection () {
    let i
    me.validOnEnter = false // ex donneesSection.touche_entree
    me.construitStructurePage('presentation1bis')

    ds.lesExos = []
    let lp = ['a', 'o']
    if (ds.Nul) lp.push('n')
    if (ds.Plat) lp.push('p')
    if (ds.Droit) lp.push('d')
    lp = j3pShuffle(lp)
    for (i = 0; i < ds.nbitems; i++) {
      ds.lesExos[i] = { sit: lp[i % lp.length] }
    }
    ds.lesExos = j3pShuffle(ds.lesExos)

    me.typederreurs = [0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0]
    stor.liste = ['Choisir', 'nul.', 'aigu.', 'droit.', 'obtus.', 'plat.']

    me.score = 0
    const tt = 'Nature d’un angle'
    lp = []
    if ((ds.Donnees === 'Figure') || (ds.Donnees === 'les deux')) lp.push('f')
    if ((ds.Donnees === 'Mesure') || (ds.Donnees === 'les deux')) lp.push('m')
    for (i = 0; i < ds.nbitems; i++) {
      ds.lesExos[i].donnees = lp[i % lp.length]
    }
    ds.lesExos = j3pShuffle(ds.lesExos)

    me.afficheTitre(tt)
    getMtgCore({ withMathJax: true })
      .then(
        // success
        (mtgAppLecteur) => {
          stor.mtgAppLecteur = mtgAppLecteur
          enonceMain()
        },
        // failure
        (error) => {
          j3pShowError(error, { message: 'mathgraph n’est pas correctement chargé', mustNotify: true })
        })
      // plantage dans enonceMain
      .catch(j3pShowError)
  }

  function faisChoixExos () {
    const e = ds.lesExos.pop()
    const nomspris = []
    stor.A = addMaj(nomspris)
    stor.B = addMaj(nomspris)
    stor.C = addMaj(nomspris)
    stor.rotation = j3pGetRandomInt(1, 359)
    const marge = 10
    switch (e.sit) {
      case 'a': stor.deplacement = j3pGetRandomInt(marge, 90 - marge)
        break
      case 'p': stor.deplacement = 180
        break
      case 'n':
        stor.deplacement = 0
        break
      case 'd':stor.deplacement = 90
        break
      case 'o': stor.deplacement = j3pGetRandomInt(90 + marge, 180 - marge)
        break
    }
    return e
  }
  function poseQuestion () {
    stor.ttt = addDefaultTable(stor.lesdiv.consigne, 2, 3)
    const yui = addDefaultTable(stor.lesdiv.zonedrep, 1, 2)
    j3pAffiche(stor.ttt[0][0], null, '<b>Complète la phrase ci-dessous.</b> ')
    let maf = 'L’angle ' + '$ \\widehat{' + stor.A + stor.B + stor.C + '}$'
    stor.ttt[0][1].style.width = '60px'
    if (stor.Lexo.donnees === 'f') {
      maf += ' semble &nbsp;'
    } else {
      maf += ' est &nbsp;'
      j3pAffiche(stor.ttt[1][0], null, '$ \\widehat{' + stor.A + stor.B + stor.C + '} = ' + stor.deplacement + '$°')
    }
    j3pAffiche(yui[0][0], null, maf)
    stor.laliste = ListeDeroulante.create(yui[0][1], stor.liste, {
      centre: true,
      sensHaut: stor.Lexo.donnees === 'f'
    })
    yui[0][1].style.verticalAlign = 'bottom'
    stor.eqviz = false
  }

  function creeLesDiv () {
    stor.lesdiv = {}
    stor.lesdiv.conteneur = j3pAddElt(me.zonesElts.MG, 'div', {
      style: me.styles.etendre('toutpetit.enonce', { padding: '10px' })
    })
    stor.lesdiv.consigne = addDefaultTable(stor.lesdiv.conteneur, 1, 1)[0][0]
    stor.lesdiv.travail = addDefaultTable(stor.lesdiv.conteneur, 1, 1)[0][0]
    stor.lesdiv.zonefig = j3pAddElt(stor.lesdiv.travail, 'div')
    const cells = addDefaultTable(stor.lesdiv.travail, 1, 3)
    cells[0][1].style.width = '40px'
    stor.lesdiv.zonedrep = cells[0][0]
    stor.lesdiv.explications = j3pAddElt(stor.lesdiv.conteneur, 'div', { style: { color: me.styles.cfaux } })
    stor.lesdiv.solution = cells[0][2]
    stor.lesdiv.solution.style.color = me.styles.toutpetit.correction.color
    stor.lesdiv.correction = j3pAddElt(me.zonesElts.MD, 'div', { style: me.styles.etendre('toutpetit.enonce', { padding: '10px' }) })
    me.zonesElts.MG.classList.add('fond')
  }
  function equerre () {
    stor.eqviz = !stor.eqviz
    stor.mtgAppLecteur.setVisible(svgId, 83, stor.eqviz, true)
    stor.mtgAppLecteur.setVisible(svgId, 89, stor.eqviz, true)
    stor.mtgAppLecteur.setVisible(svgId, 90, stor.eqviz, true)
    stor.mtgAppLecteur.setVisible(svgId, 91, stor.eqviz, true)
    stor.mtgAppLecteur.updateFigure(svgId)
  }

  function makefig () {
    stor.consel = []
    stor.captured = false
    stor.x = stor.y = 0
    stor.svgId = j3pGetNewId('mtg32')
    svgId = stor.svgId
    const txtFigure = 'TWF0aEdyYXBoSmF2YTEuMAAAABI+wo9cAAJmcv###wEA#wEAAAAAAAAAAANPAAACiAAAAQEAAAAAAAAAAQAAAF7#####AAAAAQAKQ0NhbGNDb25zdAD#####AAJwaQAWMy4xNDE1OTI2NTM1ODk3OTMyMzg0Nv####8AAAABAApDQ29uc3RhbnRlQAkh+1RELRj#####AAAAAQAKQ1BvaW50QmFzZQD#####AQAAAAEQAAFBAEAuAAAAAAAAQDQAAAAAAAAJAAFAceAAAAAAAEBjXCj1wo9cAAAAAgD#####AQAAAAAQAAAAAAAAAAAAAABACAAAAAAAAAUAAUB4SAAAAAAAQGc64UeuFHv#####AAAAAQAHQ0NhbGN1bAD#####AAhyb3RhdGlvbgACOTAAAAABQFaAAAAAAAD#####AAAAAQAJQ1JvdGF0aW9uAP####8AAAAB#####wAAAAEAD0NSZXN1bHRhdFZhbGV1cgAAAAP#####AAAAAQALQ1BvaW50SW1hZ2UA#####wEAAAAAEAAAAAAAAAAAAAAAQAgAAAAAAAAJAAAAAAIAAAAEAAAAAwD#####AAVhbmdsZQACNDUAAAABQEaAAAAAAAAAAAAEAP####8AAAABAAAABQAAAAYAAAAGAP####8BAAAAABAAAUIAAAAAAAAAAABACAAAAAAAAAkAAAAABQAAAAf#####AAAAAQAJQ0xvbmd1ZXVyAP####8AAAABAAAACAAAAAIA#####wEAAAAAEAAAAAAAAAAAAAAAQAgAAAAAAAAIAAFAXuAAAAAAAEBnuuFHrhR7#####wAAAAIACUNDZXJjbGVPUgD#####AQAAAAABAAAACgAAAAE#7MzMzMzMzQD#####AAAAAQAPQ1BvaW50TGllQ2VyY2xlAP####8B#wAAABAAAAAAAAAAAAAAAEAIAAAAAAAACAABP#AthqvhpVMAAAALAAAABAD#####AAAACgAAAAFARoAAAAAAAAAAAAYA#####wEAAAAAEAAAAAAAAAAAAAAAQAgAAAAAAAAIAAAAAAwAAAANAAAABAD#####AAAACv####8AAAABAAxDTW9pbnNVbmFpcmUAAAABQEaAAAAAAAAAAAAGAP####8BAAAAABAAAAAAAAAAAAAAAEAIAAAAAAAACAAAAAAMAAAAD#####8AAAABAA1DRGVtaURyb2l0ZU9BAP####8BAAAAAA0AAAEAAQAAAAoAAAAQAAAACwD#####AQAAAAANAAABAAEAAAAKAAAADgAAAAgA#####wEAAAAAAQAAAAoAAAABP+ZmZmZmZmYAAAAACAD#####AQAAAAABAAAACgAAAAE#+zMzMzMzMwD#####AAAAAQAQQ0ludERyb2l0ZUNlcmNsZQD#####AAAAEgAAABT#####AAAAAQAQQ1BvaW50TGllQmlwb2ludAD#####AQAAAAAQAAAAAAAAAAAAAABACAAAAAAAAAgAAQAAABUAAAAMAP####8AAAARAAAAEwAAAA0A#####wEAAAAAEAAAAAAAAAAAAAAAQAgAAAAAAAAIAAEAAAAX#####wAAAAEAEUNTeW1ldHJpZUNlbnRyYWxlAP####8AAAAQAAAABgD#####AQAAAAAQAAAAAAAAAAAAAABACAAAAAAAAAgAAAAAGAAAABn#####AAAAAQAJQ0Ryb2l0ZUFCAP####8BAAAAABAAAAEAAQAAABYAAAAY#####wAAAAEAEENEcm9pdGVQYXJhbGxlbGUA#####wEAAAAAEAAAAQABAAAAGgAAABsAAAAIAP####8BAAAAAAEAAAAKAAAAAT#DMzMzMzMzAAAAAA8A#####wEAAAAAEAAAAQABAAAAFgAAAAoAAAAPAP####8BAAAAABAAAAEAAQAAABAAAAAYAAAADAD#####AAAAHwAAAB0AAAANAP####8BAAAAABAAAAAAAAAAAAAAAEAIAAAAAAAACAABAAAAIAAAAAwA#####wAAAB4AAAAdAAAADQD#####AQAAAAAQAAAAAAAAAAAAAABACAAAAAAAAAgAAQAAACIAAAAQAP####8BAAAAABAAAAEAAQAAACMAAAAfAAAAEAD#####AQAAAAAQAAABAAEAAAAhAAAAHv####8AAAABABBDSW50RHJvaXRlRHJvaXRlAP####8BAAAAABAAAAAAAAAAAAAAAEAIAAAAAAAACAAAAAAlAAAAHAAAABEA#####wEAAAAAEAAAAAAAAAAAAAAAQAgAAAAAAAAIAAAAACQAAAAlAAAAEQD#####AQAAAAAQAAAAAAAAAAAAAABACAAAAAAAAAgAAAAAJAAAABz#####AAAAAQAJQ1BvbHlnb25lAP####8BAAAAAAEAAAAEAAAAJgAAACcAAAAoAAAAJv####8AAAABABBDU3VyZmFjZVBvbHlnb25lAP####8A####AAAABQAAACn#####AAAAAQAIQ1NlZ21lbnQA#####wAAAAAAEAAAAQABAAAAAQAAAAUAAAAUAP####8AAAAAABAAAAEAAQAAAAEAAAAIAAAACwD#####AAAAAAANAAABAAEAAAABAAAABQAAAAsA#####wAAAAAADQAAAQABAAAAAQAAAAj#####AAAAAQAWQ0Ryb2l0ZVBlcnBlbmRpY3VsYWlyZQD#####AQAAAAAQAAABAAEAAAAIAAAALgAAABUA#####wEAAAAAEAAAAQABAAAABQAAAC0AAAAIAP####8BAAAAAAEAAAAIAAAAAT#JmZmZmZmaAAAAAAgA#####wEAAAAAAQAAAAUAAAABP7mZmZmZmZoAAAAADAD#####AAAAMAAAADIAAAANAP####8BAAAAABAAAAAAAAAAAAAAAEAIAAAAAAAACQACAAAAMwAAAA0A#####wEAAAAAEAAAAAAAAAAAAAAAQAgAAAAAAAAJAAEAAAAzAAAAFAD#####AAAAAAAQAAABAAEAAAA0AAAANQAAABAA#####wEAAAAAEAAAAQABAAAANQAAACsAAAAIAP####8BAAAAAAEAAAA1AAAAAT#DMzMzMzMzAAAAAAwA#####wAAADcAAAA4AAAADQD#####AQAAAAAQAAAAAAAAAAAAAABACAAAAAAAAAkAAQAAADkAAAANAP####8BAAAAABAAAAAAAAAAAAAAAEAIAAAAAAAACQACAAAAOf####8AAAABAA9DUG9pbnRMaWVEcm9pdGUA#####wEAAAAAEAAAAAAAAAAAAAAAQAgAAAAAAAAJAAE#5etdhCkSqQAAACwAAAAVAP####8BAAAAABAAAAEAAQAAADwAAAAuAAAACAD#####AQAAAAABAAAAPAAAAAE#uZmZmZmZmgAAAAAMAP####8AAAA9AAAAPgAAAA0A#####wEAAAAAEAAAAAAAAAAAAAAAQAgAAAAAAAAJAAEAAAA#AAAADQD#####AQAAAAAQAAAAAAAAAAAAAABACAAAAAAAAAkAAgAAAD8AAAAUAP####8AAAAAABAAAAEAAQAAAEEAAABAAAAAEAD#####AQAAAAAQAAABAAEAAABBAAAALgAAAAgA#####wEAAAAAAQAAAEEAAAABP8MzMzMzMzMAAAAADAD#####AAAAQwAAAEQAAAANAP####8BAAAAABAAAAAAAAAAAAAAAEAIAAAAAAAACQACAAAARQAAAA0A#####wEAAAAAEAAAAAAAAAAAAAAAQAgAAAAAAAAJAAEAAABFAAAAFgD#####AQAAAAAQAAAAAAAAAAAAAABACAAAAAAAAAgAAT#Yp6JW8vM+AAAAK#####8AAAABABJDQXJjRGVDZXJjbGVEaXJlY3QA#####wAAAAAAAQAAAAEAAABIAAAACP####8AAAABAAxDQmlzc2VjdHJpY2UA#####wEAAAAAEAAAAQABAAAABQAAAAEAAAA8AAAACAD#####AQAAAAABAAAAAQAAAAE#wzMzMzMzMwAAAAAMAP####8AAABKAAAASwAAAA0A#####wEAAAAAEAAAAAAAAAAAAAAAQAgAAAAAAAAIAAEAAABMAAAADQD#####AQAAAAAQAAAAAAAAAAAAAABACAAAAAAAAAgAAgAAAEz#####AAAAAgAMQ0NvbW1lbnRhaXJlAP####8AAAAAAMAUAAAAAAAAwCQAAAAAAAAAAABOEAAAAAAAAAAAAAAAAAABAAAAAAAAAAAAAWT#####AAAAAQAZQ1N1cmZhY2VTZWN0ZXVyQ2lyY3VsYWlyZQD#####AAAAAAAAAAUAAABJAAAADQD#####AAAAAAAQAAAAAAAAAAAAAABACAAAAAAAAAgAAgAAABUAAAANAP####8AAAAAABAAAAAAAAAAAAAAAEAIAAAAAAAACAACAAAAFwAAABIA#####wEAAAAAAQAAAAQAAAAWAAAAGAAAAAoAAAAWAAAADQD#####AQAAAAAQAAAAAAAAAAAAAABACAAAAAAAAAgAAgAAACAAAAANAP####8BAAAAABAAAAAAAAAAAAAAAEAIAAAAAAAACAACAAAAIgAAABAA#####wEAAAAAEAAAAQABAAAAVQAAAB8AAAAQAP####8BAAAAABAAAAEAAQAAAFQAAAAeAAAAEQD#####AQAAAAAQAAAAAAAAAAAAAABACAAAAAAAAAgAAAAAVgAAAFcAAAATAP####8BAAD#AAAABQAAAFMAAAAUAP####8BAAAAABAAAAEAAQAAAFUAAABYAAAAFAD#####AQAAAAAQAAABAAEAAABYAAAAVAAAABkA#####wAAAAAAwAgAAAAAAADAJgAAAAAAAAAAAEYQAAAAAAAAAAAAAAAAAAEAAAAAAAAAAAABcwAAABkA#####wAAAAAAwAgAAAAAAADAJAAAAAAAAAAAADoQAAAAAAAAAAAAAAAAAAEAAAAAAAAAAAABZAAAAAn##########w=='
    j3pCreeSVG(stor.lesdiv.zonefig, { id: svgId, width: 500, height: 280 })
    stor.mtgAppLecteur.removeAllDoc()
    stor.mtgAppLecteur.addDoc(svgId, txtFigure, true)
    stor.mtgAppLecteur.giveFormula2(svgId, 'rotation', stor.rotation)
    stor.mtgAppLecteur.giveFormula2(svgId, 'angle', stor.deplacement)
    stor.mtgAppLecteur.setText(svgId, 92, stor.A, true)
    stor.mtgAppLecteur.setText(svgId, 79, stor.B, true)
    stor.mtgAppLecteur.setText(svgId, 93, stor.C, true)
    stor.mtgAppLecteur.calculateAndDisplayAll(true)
    stor.mtgAppLecteur.addCallBackToSVGListener(svgId, 'mousemove', function (ev) {
      mousemove(ev)
    })
    stor.mtgAppLecteur.addCallBackToSVGListener(svgId, 'mouseup', function () {
      stor.captured = false
    })
    stor.mtgAppLecteur.addEventListener(svgId, 89, 'mousedown', function (ev) {
      // app.setLineStyle(svgId, 4, 1, 5, true)
      stor.captured = true
      stor.x = ev.clientX
      stor.y = ev.clientY
    })
    stor.mtgAppLecteur.addEventListener(svgId, 89, 'mouseover', function (ev) {
      stor.mtgAppLecteur.getDoc(svgId).defaultCursor = 'pointer'
    })
    stor.mtgAppLecteur.addEventListener(svgId, 89, 'mouseout', function (ev) {
      stor.mtgAppLecteur.getDoc(svgId).defaultCursor = null
    })
    stor.mtgAppLecteur.addEventListener(svgId, 42, 'mouseenter', function () {
      if (stor.eqviz) stor.mtgAppLecteur.setVisible(svgId, 12, true, true)
    })
    stor.mtgAppLecteur.addEventListener(svgId, 42, 'mouseout', function () {
      stor.mtgAppLecteur.setVisible(svgId, 12, false, true)
    })
    if ((stor.Lexo.sit === 'p') || (stor.Lexo.sit === 'n')) {
      stor.mtgAppLecteur.setVisible(svgId, 1, true, true)
    }
  }
  function mousemove (ev) {
    if (stor.captured) {
      const newx = ev.clientX
      const newy = ev.clientY
      const deltax = newx - stor.x
      const deltay = newy - stor.y
      stor.x = newx
      stor.y = newy
      stor.mtgAppLecteur.translatePoint(svgId, 10, deltax, deltay, true)
      stor.mtgAppLecteur.updateFigure(svgId)
    }
  }

  function yaReponse () {
    if (me.isElapsed) return true
    if (!stor.laliste.changed) {
      stor.laliste.focus()
      return false
    }
    return true
  }
  function isRepOk () {
    switch (stor.laliste.reponse) {
      case 'obtus.': return (stor.Lexo.sit === 'o')
      case 'aigu.': return (stor.Lexo.sit === 'a')
      case 'plat.': return (stor.Lexo.sit === 'p')
      case 'droit.': return (stor.Lexo.sit === 'd')
      case 'nul.': return (stor.Lexo.sit === 'n')
    }
  }
  function desactivezone () {
    stor.laliste.disable()
  }
  function passetoutvert () {
    stor.laliste.corrige(true)
  }

  function AffCorrection (bool) {
    let dd
    stor.lesdiv.explications.classList.add('explique')
    switch (stor.laliste.reponse) {
      case 'obtus.': j3pAffiche(stor.lesdiv.explications, null, 'La mesure d’un angle obtus est strictement comprise entre $90$° et $180$° !')
        break
      case 'aigu.': j3pAffiche(stor.lesdiv.explications, null, 'La mesure d’un angle aigu est strictement comprise entre $0$° et $90$° !')
        break
      case 'plat.': j3pAffiche(stor.lesdiv.explications, null, 'La mesure d’un angle plat est égale $180$° !')
        break
      case 'droit.': j3pAffiche(stor.lesdiv.explications, null, 'La mesure d’un angle droit est égale 90° !')
        break
      case 'nul.': j3pAffiche(stor.lesdiv.explications, null, 'La mesure d’un angle nul est égale $0$° !')
        break
    }

    stor.laliste.corrige(false)

    if (bool) {
      // ce .correction est utilisé par testBrowser/sections/college/sixieme/Elementgeo/angle02.js (le modifier en conséquence si ça change)
      stor.lesdiv.solution.classList.add('correction')
      stor.laliste.barre()
      desactivezone()
      switch (stor.Lexo.sit) {
        case 'o': dd = 'obtus.'
          break
        case 'a': dd = 'aigu.'
          break
        case 'n': dd = 'nul.'
          break
        case 'd': dd = 'droit.'
          break
        case 'p': dd = 'plat.'
          break
      }
      if (stor.Lexo.donnees === 'f') {
        j3pAffiche(stor.lesdiv.solution, null, '$ \\widehat{' + stor.A + stor.B + stor.C + '}$ semble être un angle ' + dd)
      } else {
        j3pAffiche(stor.lesdiv.solution, null, '$ \\widehat{' + stor.A + stor.B + stor.C + '}$ est un angle ' + dd + '.')
      }
    }
  }

  function enonceMain () {
    // A chaque répétition, on nettoie la scène
    me.videLesZones()
    creeLesDiv()
    stor.Lexo = faisChoixExos()
    stor.lesdiv.travail.classList.add('travail')
    stor.lesdiv.consigne.classList.add('enonce')
    stor.lesdiv.explications.classList.remove('explique')
    stor.lesdiv.solution.classList.remove('correction')

    poseQuestion()
    if (stor.Lexo.donnees === 'f') {
      makefig()
      if (ds.Equerre) {
        j3pAjouteBouton(stor.ttt[0][2], 'Calculatrice', 'MepBoutons', 'Utiliser une équerre', equerre)
      }
    }
    me.finEnonce()
  }

  switch (this.etat) {
    case 'enonce':
      // code exécuté au lancement de la section
      if (me.debutDeLaSection) {
        initSection()
      } else {
        enonceMain()
      }
      break // case "enonce":

    case 'correction':
      // On teste si une réponse a été saisie
      if (stor.lesdiv === undefined) return
      stor.lesdiv.explications.innerHTML = ''
      stor.lesdiv.correction.innerHTML = ''

      if (yaReponse()) {
        // Bonne réponse
        if (isRepOk()) {
          this.score++
          stor.lesdiv.correction.style.color = this.styles.cbien
          stor.lesdiv.correction.innerHTML = cBien
          passetoutvert()
          desactivezone()
          if (stor.nedco) {
            if (ds.conseil) {
              let np, nm, b
              j3pAffiche(stor.lesdiv.solution, null, 'Le calcul serait plus rapide en regroupant les termes. \n\n')
              np = ''
              nm = ''
              for (b = 0; b < stor.lestermes.length; b++) {
                if (stor.lestermes[b].signe) { np = np + '+' + stor.lestermes[b].val } else { nm = nm + '-' + stor.lestermes[b].val }
              }
              np = np.substring(1) + nm + '$'
              j3pAffiche(stor.lesdiv.solution, null, '$A = ' + np + '\n')
              np = 0
              nm = 0
              for (b = 0; b < stor.lestermes.length; b++) {
                if (stor.lestermes[b].signe) { np += stor.lestermes[b].val } else { nm += stor.lestermes[b].val }
              }
              np = j3pArrondi(np, 1)
              nm = j3pArrondi(nm, 1)
              j3pAffiche(stor.lesdiv.solution, null, '$A = ' + np + '-' + nm + '$\n')
              np = j3pArrondi(np - nm, 1)
              j3pAffiche(stor.lesdiv.solution, null, '$A = ' + np + '$\n')
            }
          }

          this.typederreurs[0]++
          return this.finCorrection('navigation', true)
        }

        // Pas de bonne réponse
        stor.lesdiv.correction.style.color = this.styles.cfaux
        stor.lesdiv.correction.innerHTML = cFaux
        if (this.isElapsed) {
          stor.lesdiv.correction.innerHTML += tempsDepasse + '<BR>'
          this.typederreurs[10]++
          AffCorrection(true)
          return this.finCorrection('navigation', true)
        }

        // Réponse fausse :
        if (me.essaiCourant < me.donneesSection.nbchances) {
          stor.lesdiv.correction.innerHTML += '<br>' + essaieEncore
          AffCorrection(false)
          this.typederreurs[1]++
          // il reste des chances, on reste dans l’état correction
          return this.finCorrection()
        }

        // Erreur au dernier essai
        AffCorrection(true)
        this.typederreurs[2]++
        return this.finCorrection('navigation', true)
      }

      // pas de réponse
      stor.lesdiv.correction.style.color = this.styles.cfaux
      stor.lesdiv.correction.innerHTML = reponseIncomplete
      this.finCorrection()
      break // case "correction":

    case 'navigation':
      if (!this.sectionTerminee()) {
        this.etat = 'enonce'
      }
      this.finNavigation(true)
      break // case "navigation":
  }
}
