import { j3pAddElt, j3pArrondi, j3pGetNewId, j3pGetRandomInt, j3pModale, j3pShowError, j3pShuffle } from 'src/legacy/core/functions'
import { j3pAffiche } from 'src/lib/mathquill/functions'
import { addDefaultTable } from 'src/legacy/themes/table'
import ZoneStyleMathquill1 from 'src/legacy/outils/zoneStyleMathquill/ZoneStyleMathquill1'
import { j3pCreeSVG } from 'src/legacy/core/functionsSvg'
import textesGeneriques from 'src/lib/core/textes'
import { getMtgCore } from 'src/lib/outils/mathgraph'
import { addMaj } from 'src/lib/utils/string'

const { cBien, cFaux, essaieEncore, reponseIncomplete, tempsDepasse } = textesGeneriques

/**
 * Ajouter une description ici
 * @fileOverview
 * @author Tommy Barroy <t.barroy@laposte.net>
 * @since 2017-10
*/
export const params = {
  outils: ['mathquill'],
  /**
   * Ce qui est paramétrable dans l’éditeur de graphe
   * @type {LegacySectionParamsParametres}
   */
  parametres: [
    ['nbrepetitions', 3, 'entier', 'Nombre de répétitions de la section'],
    ['nbchances', 2, 'entier', 'Nombre d’essais possibles.'],
    ['limite', 0, 'entier', 'Temps disponible par répétition, en secondes'],
    ['Question', 'les deux', 'liste', '<u>Nommer</u>: Donner un nom d’un angle marqué. <br><br> <u>Retrouver</u> Sélectionner un angle à partir de son nom.', ['Nommer', 'Retrouver', 'les deux']],
    ['Trois_lettres', true, 'boolean', "<u>true</u>: Les angles donnés s'écrivent forcément avec trois lettres."],
    // le modèle gère la compatibilité ascendante avec l’ancien paramètre MiseEnPagePlusClaire
    ['theme', 'standard', 'liste', 'Modèle pour l’affichage', ['standard', 'zonesAvecImageDeFond']]
  ]
}

/**
 * section angle01
 * @this {Parcours}
 */
export default function main () {
  const me = this
  const stor = this.storage
  let ds = this.donneesSection
  let svgId = stor.svgId

  function getDonnees () {
    return {
      // Nombre de répétitions de l’exercice
      nbrepetitions: 3,

      Question: 'les deux',
      Trois_lettres: true,
      limite: 0,

      // Si indication != "" alors un lien "Indication" s’affiche dans la zone Inférieure Gauche (this.zones.IG
      // une indication<br>de deux lignes..."
      indication: '',

      // pour le cas de presentation1bis
      // En effet pas d’indication possible pour cette présentation
      // surchargeindication: false,

      // Nombre de chances dont dispose l’él§ve pour répondre à la question
      nbchances: 2,

      //  || "presentation2" || "presentation3"  || "presentation1bis"
      structure: 'presentation1bis'
    }
  }
  function initSection () {
    let i
    stor.lesbulle = []
    ds = getDonnees()
    me.donneesSection = ds
    // On surcharge avec les parametres passés par le graphe
    me.surcharge()
    me.validOnEnter = false // ex donneesSection.touche_entree

    // Construction de la page
    me.construitStructurePage(me.donneesSection.structure)
    me.donneesSection.nbetapes = 1
    me.donneesSection.nbitems = me.donneesSection.nbetapes * me.donneesSection.nbrepetitions

    ds.lesExos = []
    let lp = ['triangle1', 'adja', 'triangle2', 'tout']
    if (ds.Trois_lettres) lp = ['adja', 'triangle2', 'tout']
    for (i = 0; i < ds.nbitems; i++) {
      ds.lesExos[i] = { sit: lp[i % lp.length] }
    }
    ds.lesExos = j3pShuffle(ds.lesExos)
    stor.restric = 'ABCDEFGHIJKLMNOPQRSTUVWXYZabcdefghijklmnopqrstuvwxyz^$'

    me.typederreurs = [0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0]

    me.score = 0
    let tt = ''
    switch (ds.Question) {
      case 'les deux': tt = 'Nommer / retrouver un angle'
        lp = ['n', 't']
        break
      case 'Nommer': tt = 'Nommer un angle'
        lp = ['n']
        break
      case 'Retrouver': tt = 'Retrouver un angle avec son nom'
        lp = ['t']
        break
    }
    for (i = 0; i < ds.nbitems; i++) {
      ds.lesExos[i].quest = lp[i % lp.length]
    }
    ds.lesExos = j3pShuffle(ds.lesExos)

    me.afficheTitre(tt)

    getMtgCore({ withMathJax: true })
      .then(
        // success
        (mtgAppLecteur) => {
          stor.mtgAppLecteur = mtgAppLecteur
          enonceMain()
        },
        // failure
        (error) => {
          j3pShowError(error, { message: 'mathgraph n’est pas correctement chargé', mustNotify: true })
        })
      // plantage dans enonceMain
      .catch(j3pShowError)
  }

  function faisChoixExos () {
    const e = ds.lesExos.pop()
    const nomspris = []
    stor.A = addMaj(nomspris)
    stor.B = addMaj(nomspris)
    stor.C = addMaj(nomspris)
    stor.D = addMaj(nomspris)
    stor.E = addMaj(nomspris)
    stor.rotation = j3pGetRandomInt(1, 359)
    stor.deplacement = j3pGetRandomInt(1, 10) / 10
    stor.listanpos = []
    stor.foacche = []
    let nmax = 0
    switch (e.sit) {
      case 'triangle1':
        stor.listanpos.push({ num: 74, noms: [stor.B + stor.C + stor.A, stor.B + stor.C + stor.E, stor.E + stor.C + stor.B, stor.A + stor.C + stor.B, stor.C] })
        stor.listanpos.push({ num: 60, noms: [stor.B + stor.A + stor.C, stor.B + stor.A + stor.E, stor.E + stor.A + stor.B, stor.C + stor.A + stor.B, stor.A] })
        stor.listanpos.push({ num: 68, noms: [stor.A + stor.B + stor.C, stor.C + stor.B + stor.A, stor.B] })
        stor.foacche = [61, 43, 42, 121, 63, 64, 48, 47, 71, 70, 57, 56, 53, 52, 40]
        nmax = 2
        break
      case 'adja':
        stor.listanpos.push({ num: 74, noms: [stor.B + stor.C + stor.A, stor.B + stor.C + stor.E, stor.E + stor.C + stor.B, stor.A + stor.C + stor.B] })
        stor.listanpos.push({ num: 71, noms: [stor.A + stor.C + stor.D, stor.E + stor.C + stor.D, stor.D + stor.C + stor.A, stor.D + stor.C + stor.E] })
        stor.listanpos.push({ num: 53, noms: [stor.D + stor.C + stor.B, stor.B + stor.C + stor.D] })
        stor.foacche = [54, 61, 60, 59, 64, 63, 67, 68, 57, 56, 48, 47]
        nmax = 2
        break
      case 'triangle2':
        stor.listanpos.push({ num: 74, noms: [stor.B + stor.C + stor.A, stor.B + stor.C + stor.E, stor.E + stor.C + stor.B, stor.A + stor.C + stor.B] })
        stor.listanpos.push({ num: 71, noms: [stor.A + stor.C + stor.D, stor.E + stor.C + stor.D, stor.D + stor.C + stor.A, stor.D + stor.C + stor.E] })
        stor.listanpos.push({ num: 53, noms: [stor.D + stor.C + stor.B, stor.B + stor.C + stor.D] })
        stor.listanpos.push({ num: 60, noms: [stor.B + stor.A + stor.C, stor.B + stor.A + stor.E, stor.E + stor.A + stor.B, stor.C + stor.A + stor.B, stor.A] })
        stor.listanpos.push({ num: 68, noms: [stor.A + stor.B + stor.C, stor.C + stor.B + stor.A, stor.B] })
        stor.foacche = [61, 63, 64, 48, 47, 57, 56]
        nmax = 2
        if (!ds.Trois_lettres) {
          nmax = 4
        }
        break
      case 'tout':
        stor.listanpos.push({ num: 74, noms: [stor.B + stor.C + stor.A, stor.B + stor.C + stor.E, stor.E + stor.C + stor.B, stor.A + stor.C + stor.B] })
        stor.listanpos.push({ num: 60, noms: [stor.B + stor.A + stor.C, stor.B + stor.A + stor.E, stor.E + stor.A + stor.B, stor.C + stor.A + stor.B] })
        stor.listanpos.push({ num: 71, noms: [stor.A + stor.C + stor.D, stor.E + stor.C + stor.D, stor.D + stor.C + stor.A, stor.D + stor.C + stor.E] })
        stor.listanpos.push({ num: 53, noms: [stor.D + stor.C + stor.B, stor.B + stor.C + stor.D] })
        stor.listanpos.push({ num: 64, noms: [stor.C + stor.A + stor.D, stor.D + stor.A + stor.C, stor.D + stor.A + stor.E, stor.E + stor.A + stor.D] })
        stor.listanpos.push({ num: 57, noms: [stor.B + stor.A + stor.D, stor.D + stor.A + stor.B] })
        stor.listanpos.push({ num: 48, noms: [stor.C + stor.D + stor.A, stor.A + stor.D + stor.C, stor.D] })
        stor.listanpos.push({ num: 68, noms: [stor.A + stor.B + stor.C, stor.C + stor.B + stor.A, stor.B] })
        nmax = 5
        if (!ds.Trois_lettres) {
          nmax = 7
        }
        break
    }
    stor.langle = stor.listanpos[j3pGetRandomInt(0, nmax)]
    return e
  }
  function poseQuestion () {
    if (stor.Lexo.quest === 't') {
      j3pAffiche(stor.lesdiv.consigne, null, '<b>Tu dois sélectionner l’angle $ \\widehat{' + stor.langle.noms[j3pGetRandomInt(0, stor.langle.noms.length - 1)] + '}$</b>')
    } else {
      j3pAffiche(stor.lesdiv.consigne, null, '<b>Trouve un nom pour l’angle violet. </b>')
      const tt = addDefaultTable(stor.lesdiv.zonedrep, 1, 2)
      j3pAffiche(tt[0][0], null, 'L’angle violet se nomme &nbsp;')
      stor.reprep = new ZoneStyleMathquill1(tt[0][1], {
        restric: stor.restric,
        limitenb: 1,
        limite: 5,
        hasAutoKeyboard: true,
        enter: me.sectionCourante.bind(me)
      })
    }
  }

  function creeLesDiv () {
    stor.lesdiv = {}
    stor.lesdiv.conteneur = j3pAddElt(me.zonesElts.MG, 'div', {
      style: me.styles.etendre('toutpetit.enonce', { padding: '10px' })
    })
    stor.lesdiv.consigne = addDefaultTable(stor.lesdiv.conteneur, 1, 1)[0][0]
    stor.lesdiv.travail = addDefaultTable(stor.lesdiv.conteneur, 1, 1)[0][0]
    stor.lesdiv.zonefig = j3pAddElt(stor.lesdiv.travail, 'div')
    stor.lesdiv.zonedrep = j3pAddElt(stor.lesdiv.travail, 'div')
    stor.lesdiv.explications = j3pAddElt(stor.lesdiv.conteneur, 'div')
    stor.lesdiv.solution = j3pAddElt(stor.lesdiv.conteneur, 'div')
    stor.lesdiv.correction = j3pAddElt(me.zonesElts.MD, 'div', {
      style: me.styles.etendre('toutpetit.enonce', { padding: '10px' })
    })
    stor.lesdiv.explications.style.color = me.styles.cfaux
    stor.lesdiv.solution.style.color = me.styles.toutpetit.correction.color
    me.zonesElts.MG.classList.add('fond')
  }

  function affModale (texte) {
    const yy = j3pModale({ titre: 'Erreur', contenu: '' })
    j3pAffiche(yy, null, texte)
    yy.style.color = me.styles.toutpetit.correction.color
  }
  function makefig () {
    function rendsel (ki) {
      stor.mtgAppLecteur.addEventListener(svgId, ki.num, 'click', function () {
        let i
        for (i = 0; i < stor.consel.length; i++) {
          if (stor.consel[i].ki === ki.num) {
            stor.consel[i].sel = !stor.consel[i].sel
            break
          }
        }
        if (stor.consel[i] === undefined) return
        if (stor.consel[i].sel) {
          stor.mtgAppLecteur.setColor(svgId, ki.num, 255, 0, 0, true)
        } else {
          stor.mtgAppLecteur.setColor(svgId, ki.num, 120, 120, 120, true)
        }
      })
      stor.consel.push({ ki: ki.num, sel: false, koi: ki.noms })
      stor.mtgAppLecteur.addEventListener(svgId, ki.num, 'mouseout', function () {
        let i
        stor.mtgAppLecteur.getDoc(svgId).defaultCursor = null
        for (i = 0; i < stor.consel.length; i++) {
          if (stor.consel[i].ki === ki.num) {
            break
          }
        }
        if (stor.consel[i].sel) {
          stor.mtgAppLecteur.setColor(svgId, ki.num, 255, 0, 0, true)
        } else {
          stor.mtgAppLecteur.setColor(svgId, ki.num, 120, 120, 120, true)
        }
      })
      stor.mtgAppLecteur.addEventListener(svgId, ki.num, 'mouseover', function () {
        let i
        stor.mtgAppLecteur.getDoc(svgId).defaultCursor = 'pointer'
        for (i = 0; i < stor.consel.length; i++) {
          if (stor.consel[i].ki === ki.num) {
            break
          }
        }
        if (!stor.consel[i].sel) {
          stor.mtgAppLecteur.setColor(svgId, ki.num, 50, 50, 50, true)
        } else {
          stor.mtgAppLecteur.setColor(svgId, ki.num, 240, 100, 0, true)
        }
      })
    }
    let i
    stor.consel = []
    stor.svgId = j3pGetNewId('mtg32')
    svgId = stor.svgId
    const txtFigure = 'TWF0aEdyYXBoSmF2YTEuMAAAABI#Sj1xAAJmcv###wEA#wEAAAAAAAAAAANPAAACiAAAAQEAAAAAAAAAAQAAAIH#####AAAAAQAKQ0NhbGNDb25zdAD#####AAJwaQAWMy4xNDE1OTI2NTM1ODk3OTMyMzg0Nv####8AAAABAApDQ29uc3RhbnRlQAkh+1RELRj#####AAAAAQAKQ1BvaW50QmFzZQD#####AAAAAAAQAAAAQAgAAAAAAAAAAAAAAAAAAAcAAEBZQAAAAAAAQHtOFHrhR67#####AAAAAQAHQ0NhbGN1bAD#####AAVtaW5pMQABMAAAAAEAAAAAAAAAAAAAAAMA#####wAFbWF4aTEAAzM2MAAAAAFAdoAAAAAAAP####8AAAABABRDSW1wbGVtZW50YXRpb25Qcm90bwD#####AAdDdXJzZXVyAAAABQAAAAUAAAADAAAAAgAAAAMAAAAB#####wAAAAEAFENEcm9pdGVEaXJlY3Rpb25GaXhlAAAAAAQBAAAAABAAAAEAAQAAAAEBP#AAAAAAAAD#####AAAAAQAPQ1BvaW50TGllRHJvaXRlAQAAAAQAAAAAABAAAADACAAAAAAAAD#wAAAAAAAABQAAQF1AAAAAAAAAAAAF#####wAAAAEAC0NIb21vdGhldGllAAAAAAQAAAAB#####wAAAAEACkNPcGVyYXRpb24D#####wAAAAEAD0NSZXN1bHRhdFZhbGV1cgAAAAIAAAAIAQAAAAkAAAACAAAACQAAAAP#####AAAAAQALQ1BvaW50SW1hZ2UAAAAABAEAAAAADQACTzEAwBAAAAAAAABAEAAAAAAAAAUAAAAABgAAAAcAAAAHAAAAAAQAAAABAAAACAMAAAAIAQAAAAE#8AAAAAAAAAAAAAkAAAACAAAACAEAAAAJAAAAAwAAAAkAAAACAAAACgAAAAAEAQAAAAANAAJJMQDAAAAAAAAAAEAIAAAAAAAABQAAAAAGAAAACf####8AAAABAAhDU2VnbWVudAEAAAAEAAAAAAAQAAABAAIAAAABAAAABgAAAAYBAAAABAEAAAABEAACazEAwAAAAAAAAABAAAAAAAAAAAEAAT#QrwrwrwrxAAAAC#####8AAAACAA9DTWVzdXJlQWJzY2lzc2UBAAAABAAFZHNxZHMAAAAIAAAACgAAAAz#####AAAAAQAPQ1ZhbGV1ckFmZmljaGVlAQAAAAQAAAAAAAAAAAAAAAAAwBgAAAAAAAAAAAAMDwAB####AAAAAQAAAAIAAAABAAAAAAAAAAAAAAAAAgAAAA0AAAADAP####8ACXJvdGF0aW9uMgAFZHNxZHMAAAAJAAAADQAAAAIA#####wEAAP8AEAACYTQAAAAAAAAAAABACAAAAAAAAAkAAUBtYAAAAAAAQGC8KPXCj1z#####AAAAAQAJQ1JvdGF0aW9uAP####8AAAAQAAAACQAAAA8AAAACAP####8AAAAAABAAAABACAAAAAAAAAAAAAAAAAAABQAAQHE4AAAAAABAez1wo9cKPgAAAAMA#####wAFbWluaTIAATAAAAABAAAAAAAAAAAAAAADAP####8ABW1heGkyAAExAAAAAT#wAAAAAAAAAAAABAD#####AAdDdXJzZXVyAAAABQAAAAUAAAADAAAAEwAAABQAAAASAAAABQAAAAAVAQAAAAAQAAABAAEAAAASAT#wAAAAAAAAAAAABgEAAAAVAAAAAAAQAAAAwAgAAAAAAAA#8AAAAAAAAAUAAEBdQAAAAAAAAAAAFgAAAAcAAAAAFQAAABIAAAAIAwAAAAkAAAATAAAACAEAAAAJAAAAEwAAAAkAAAAUAAAACgAAAAAVAQAAAAANAAJPMQDAEAAAAAAAAEAQAAAAAAAABQAAAAAXAAAAGAAAAAcAAAAAFQAAABIAAAAIAwAAAAgBAAAAAT#wAAAAAAAAAAAACQAAABMAAAAIAQAAAAkAAAAUAAAACQAAABMAAAAKAAAAABUBAAAAAA0AAkkxAMAAAAAAAAAAQAgAAAAAAAAFAAAAABcAAAAaAAAACwEAAAAVAAAAAAAQAAABAAEAAAASAAAAFwAAAAYBAAAAFQAAAAABEAABawDAAAAAAAAAAEAAAAAAAAAAAQABP#AAAAAAAAAAAAAcAAAADAEAAAAVAAxkZXBsYWNlbWVudDIAAAAZAAAAGwAAAB0AAAANAQAAABUAAAAAAAAAAAAAAAAAwBgAAAAAAAAAAAAdDwAB####AAAAAQAAAAIAAAABAAAAAAAAAAAAAAAAAgAAAB4AAAACAP####8BAAD#ABAAAUIAAAAAAAAAAABACAAAAAAAAAgAAUB1YAAAAAAAQGg8KPXCj1wAAAACAP####8BAAD#ABAAAUEAQCQAAAAAAADAJAAAAAAAAAgAAUB0UAAAAAAAQFG4UeuFHrgAAAALAP####8BAAD#ABAAAAEABQAAACEAAAAg#####wAAAAEACUNMb25ndWV1cgD#####AAAAIQAAACAAAAADAP####8AC2RlcGxhY2VtZW50AAxkZXBsYWNlbWVudDIAAAAJAAAAHv####8AAAACAAlDQ2VyY2xlT1IA#####wEAAP8AAQAAACAAAAAJAAAAJAD#####AAAAAQAQQ0ludERyb2l0ZUNlcmNsZQD#####AAAAIgAAACX#####AAAAAQAQQ1BvaW50TGllQmlwb2ludAD#####AQAA#wAQAAAAAAAAAAAAAABACAAAAAAAAAkAAgAAACYAAAAKAP####8AAAAAARAAAWQAAAAAAAAAAABACAAAAAAAAAMAAAAAJwAAABEAAAACAP####8BAAD#ABAAAAAAAAAAAAAAAEAIAAAAAAAABQABQG1AAAAAAABAbrwo9cKPXAAAAAoA#####wAAAAABEAABYwAAAAAAAAAAAEAIAAAAAAAAAwAAAAApAAAAEQAAAAsA#####wAAAAAAEAAAAQACAAAAKgAAACgAAAACAP####8BAAD#ABAAAAAAAAAAAAAAAEAIAAAAAAAABQABQGsAAAAAAABAPeFHrhR64gAAAAoA#####wAAAAABEAABYQAAAAAAAAAAAEAIAAAAAAAAAwAAAAAsAAAAEQAAAAYA#####wEAAAAAEAAAAAAAAAAAAAAAQAgAAAAAAAAHAAE#6JcyhenpTAAAACv#####AAAAAQAUQ0FyY0RlQ2VyY2xlSW5kaXJlY3QA#####wAAAAAAAgAAACgAAAAuAAAALf####8AAAABABlDU3VyZmFjZVNlY3RldXJDaXJjdWxhaXJlAP####8AZmZmAAAABQAAAC8AAAACAP####8BAAAAABAAAAAAAAAAAAAAAEAIAAAAAAAABwABQFyAAAAAAABAYFwo9cKPXAAAAAoA#####wAAAAABEAABYgAAAAAAAAAAAEAIAAAAAAAAAwAAAAAxAAAAEQAAAAYA#####wEAAAAAEAAAAAAAAAAAAAAAQAgAAAAAAAAHAAE#36os9pVQmwAAACv#####AAAAAQASQ0FyY0RlQ2VyY2xlRGlyZWN0AP####8AAAAAAAEAAAAqAAAAMwAAADIAAAAUAP####8AZmZmAAAABQAAADQAAAALAP####8AAAAAABAAAAEAAgAAAC0AAAAyAAAABgD#####AQD#AAAQAAAAAAAAAAAAAABACAAAAAAAAAUAAT#fbwzoXkCaAAAANgAAABUA#####wAAAAAAAQAAAC0AAAA3AAAAKAAAABQA#####wBmZmYAAAAFAAAAOAAAAAYA#####wEAAAAAEAAAAAAAAAAAAAAAQAgAAAAAAAAHAAE#2CipSz5hOgAAADYAAAAVAP####8AAAAAAAEAAAAtAAAAOgAAACoAAAAUAP####8AZmZmAAAABQAAADsAAAALAP####8AAAAAABAAAAEAAgAAACgAAAAtAAAABgD#####AQAAAAAQAAAAAAAAAAAAAABACAAAAAAAAAcAAT#oZIKu5hH5AAAAPQAAABMA#####wAAAAAAAQAAAC0AAAA+AAAAKgAAABQA#####wBmZmYAAAAFAAAAPwAAAAsA#####wAAAAAAEAAAAQACAAAAMgAAACoAAAAGAP####8AAAAAABAAAAAAAAAAAAAAAEAIAAAAAAAABwABP9AhwCxkoDEAAABBAAAAFQD#####AAAAAAABAAAAMgAAAEIAAAAtAAAAFAD#####AGZmZgAAAAUAAABDAAAABgD#####AQAAAAAQAAAAAAAAAAAAAABACAAAAAAAAAcAAT#U2iq8vDSGAAAAKwAAABUA#####wAAAAAAAQAAACoAAABFAAAALQAAABQA#####wBmZmYAAAAFAAAARgAAAAYA#####wEAAAAAEAAAAAAAAAAAAAAAQAgAAAAAAAAHAAE#6TB5aZOGHAAAAEEAAAATAP####8AAAAAAAEAAAAqAAAASAAAAC0AAAAUAP####8AZmZmAAAABQAAAEkAAAACAP####8BAAAAABAAAAAAAAAAAAAAAEAIAAAAAAAABQABQBAAAAAAAABAcZ4UeuFHrgAAAAIA#####wEAAAAAEAAAAAAAAAAAAAAAQAgAAAAAAAAFAAFAflAAAAAAAEBxfhR64Ueu#####wAAAAEACUNEcm9pdGVBQgD#####Af8AAAAQAAABAAUAAABLAAAATP####8AAAABABZDRHJvaXRlUGVycGVuZGljdWxhaXJlAP####8B#wAAABAAAAEABQAAAEwAAABNAAAAFwD#####Af8AAAAQAAABAAUAAABLAAAATQAAAAYA#####wEAAAAAEAAAAAAAAAAAAAAAQAgAAAAAAAAFAAG#4oQ#TILuWgAAAE8AAAAXAP####8B#wAAABAAAAEABQAAAFAAAABP#####wAAAAEAEENJbnREcm9pdGVEcm9pdGUA#####wEAAAAAEAAAAAAAAAAAAAAAQAgAAAAAAAAFAAAAAE4AAABRAAAAAgD#####Af8AAAAQAAJiMAAAAAAAAAAAAEAIAAAAAAAACAABQGqwAAAAAABAL64UeuFHsAAAAAIA#####wH#AAAAEAACYjEAAAAAAAAAAABACAAAAAAAAAgAAUBcYAAAAAAAQFs1wo9cKPYAAAACAP####8B#wAAABAAAmIyAAAAAAAAAAAAQAgAAAAAAAAIAAFAbPAAAAAAAEBwPXCj1wo+AAAAEgD#####AAAA#wAQAAAAAAAAAAAAAABACAAAAAAAAAkAAQAAACb#####AAAAAQANQ0RlbWlEcm9pdGVPQQD#####AQAA#wANAAABAAEAAAAQAAAAJwAAABAA#####wEAAP8AAQAAACcAAAABP8mZmZmZmZoAAAAAEQD#####AAAAVwAAAFgAAAASAP####8BAAD#ABAAAAAAAAAAAAAAAEAIAAAAAAAACQACAAAAWQAAABIA#####wH#AAAAEAAAAAAAAAAAAAAAQAgAAAAAAAAJAAEAAABZAAAACwD#####AAAAAAAQAAABAAIAAAAtAAAAKgAAAAoA#####wEAAAAAEAACYTEAwCAAAAAAAADAKgAAAAAAAAcAAAAAUwAAABEAAAAKAP####8BAAAAABAAAmEyAMAcAAAAAAAAwCwAAAAAAAAHAAAAAFQAAAARAAAACgD#####AQAAAAAQAAJhMwDAHAAAAAAAAMAoAAAAAAAABwAAAABVAAAAEQAAAAoA#####wEAAAAAEAACYTAAwBgAAAAAAADAKgAAAAAAAAcAAAAAWwAAABEAAAAVAP####8BAAAAAAEAAAAtAAAANwAAACr#####AAAAAgAMQ0NvbW1lbnRhaXJlAP####8AAAAAAMAcAAAAAAAAwCYAAAAAAAAAAABdEAAAAAAAAAAAAAAAAAABAAAAAAAAAAAAAmExAAAAGgD#####AAAAAADAJgAAAAAAAMAkAAAAAAAAAAAAXhAAAAAAAAAAAAAAAAAAAQAAAAAAAAAAAARmZ2RzAAAAGgD#####AQAAAADAQgAAAAAAAEA3AAAAAAAAAAAAYBAAAAAAAAAAAAAAAAAAAQAAAAAAAAAAAANmZHMAAAAaAP####8BAAAAAMA0AAAAAAAAQDMAAAAAAAgAAABfEAAAAAAAAAAAAAAAAAABAAAAAAAAAAAABHZmZHcAAAAGAP####8BAAAAABAAAAAAAAAAAAAAAEAIAAAAAAAACQAAP+A9PuOsBZUAAABcAAAAFwD#####AQAAAAAQAAABAAEAAABmAAAAXAAAABAA#####wEAAAAAAQAAAGYAAAABP7mZmZmZmZoAAAAAEQD#####AAAAZwAAAGgAAAASAP####8BAAAAABAAAAAAAAAAAAAAAEAIAAAAAAAACQACAAAAaQAAABIA#####wEAAAAAEAAAAAAAAAAAAAAAQAgAAAAAAAAJAAEAAABpAAAAGgD#####AQAAAADAPQAAAAAAAD#wAAAAAAAAAAAAaxAAAAAAAAAAAAAAAAAAAQAAAAAAAAAAAAJyaQAAAAsA#####wAAAAAAEAAAAQABAAAAagAAAGsAAAAQAP####8BAAAAAAEAAABmAAAAAT#JmZmZmZmaAAAAABEA#####wAAAGcAAABuAAAAEgD#####AQAAAAAQAAAAAAAAAAAAAABACAAAAAAAAAkAAgAAAG8AAAASAP####8BAAAAABAAAAAAAAAAAAAAAEAIAAAAAAAACQABAAAAbwAAABoA#####wAAAAAAwBgAAAAAAADAKAAAAAAAAAAAAHEQAAAAAAAAAAAAAAAAAAEAAAAAAAAAAAACcmkAAAAZAP####8BAAAAAA0AAAEAAQAAADIAAAAoAAAAEAD#####AQAAAAABAAAAKAAAAAE#yZmZmZmZmgAAAAAXAP####8BAAAAABAAAAEAAQAAACgAAABzAAAAEQD#####AAAAdQAAAHQAAAASAP####8BAAAAABAAAAAAAAAAAAAAAEAIAAAAAAAAAwACAAAAdgAAABIA#####wEAAAAAEAAAAAAAAAAAAAAAQAgAAAAAAAADAAEAAAB2AAAAGgD#####AAAAAADAIAAAAAAAAMAiAAAAAAAAAAAAeBAAAAAAAAAAAAAAAAAAAQAAAAAAAAAAAAJmZAAAABkA#####wEAAAAADQAAAQABAAAALQAAACoAAAAQAP####8BAAAAAAEAAAAqAAAAAT#JmZmZmZmaAAAAABcA#####wEAAAAAEAAAAQABAAAAKgAAAHoAAAARAP####8AAAB8AAAAewAAABIA#####wEAAAAAEAAAAAAAAAAAAAAAQAgAAAAAAAADAAIAAAB9AAAAEgD#####AQAAAAAQAAAAAAAAAAAAAABACAAAAAAAAAMAAQAAAH0AAAAaAP####8AAAAAAMAgAAAAAAAAwCQAAAAAAAAAAAB#EAAAAAAAAAAAAAAAAAABAAAAAAAAAAAAAmdmAAAAI###########'
    j3pCreeSVG(stor.lesdiv.zonefig, { id: svgId, width: 500, height: 280 })
    stor.mtgAppLecteur.removeAllDoc()
    stor.mtgAppLecteur.addDoc(svgId, txtFigure, true)
    stor.mtgAppLecteur.giveFormula2(svgId, 'rotation2', stor.rotation)
    stor.mtgAppLecteur.giveFormula2(svgId, 'deplacement', stor.deplacement)
    stor.mtgAppLecteur.setText(svgId, 98, stor.A, true)
    stor.mtgAppLecteur.setText(svgId, 99, stor.B, true)
    stor.mtgAppLecteur.setText(svgId, 128, stor.C, true)
    stor.mtgAppLecteur.setText(svgId, 121, stor.D, true)
    stor.mtgAppLecteur.setText(svgId, 114, stor.E, true)
    stor.mtgAppLecteur.calculateAndDisplayAll(true)
    for (i = 0; i < stor.foacche.length; i++) {
      stor.mtgAppLecteur.setVisible(svgId, stor.foacche[i], false, true)
    }
    if (stor.Lexo.quest === 't') {
      for (i = 0; i < stor.listanpos.length; i++) {
        rendsel(stor.listanpos[i])
      }
    } else {
      stor.mtgAppLecteur.setColor(svgId, stor.langle.num, 167, 31, 139, true)
    }
  }
  function SelCorrige (num, cool) {
    let lacool = [51, 102, 255]
    if (cool === true) lacool = [0, 138, 115]
    if (cool === false) lacool = [214, 71, 0]
    stor.mtgAppLecteur.setColor(svgId, num, lacool[0], lacool[1], lacool[2], true)
  }
  function viresel (ki) {
    stor.mtgAppLecteur.removeEventListener(svgId, ki, 'click')
    stor.mtgAppLecteur.removeEventListener(svgId, ki, 'mouseout')
    stor.mtgAppLecteur.removeEventListener(svgId, ki, 'mouseover')
  }

  function yaReponse () {
    let i
    if (me.isElapsed) return true
    if (stor.Lexo.quest === 't') {
      let cpt = 0
      for (i = 0; i < stor.consel.length; i++) {
        if (stor.consel[i].sel) cpt++
      }
      if (cpt === 0) {
        affModale('Tu dois sélectionner un angle ! <br><i>(en cliquant dessus)</i>')
        return false
      }
      if (cpt > 1) {
        affModale('Tu as sélectionné trop d’angles !')
        return false
      }
      return true
    } else {
      if (stor.reprep.reponsechap()[0] === '') {
        stor.reprep.focus()
        return false
      }
      return true
    }
  }
  function isRepOk () {
    stor.errChap1 = false
    stor.errChap2 = false
    let larep

    if (stor.Lexo.quest === 't') {
      for (const consel of stor.consel) {
        if (consel.sel) larep = consel.koi[0]
      }
      return (stor.langle.noms.indexOf(larep) !== -1)
    }
    larep = stor.reprep.reponsechap()
    if (larep[1] === 0) {
      stor.errChap1 = true
      return false
    }
    if (larep[1] === 2) {
      stor.errChap2 = true
      return false
    }
    return (stor.langle.noms.indexOf(larep[0]) !== -1)
  }
  function desactivezone () {
    let i
    if (stor.Lexo.quest === 't') {
      for (i = 0; i < stor.consel.length; i++) {
        viresel(stor.consel[i].ki)
      }
    } else {
      stor.reprep.disable()
    }
  }
  function passetoutvert () {
    let i
    if (stor.Lexo.quest === 't') {
      for (i = 0; i < stor.consel.length; i++) {
        if (stor.consel[i].sel) SelCorrige(stor.consel[i].ki, true)
      }
    } else {
      stor.reprep.corrige(true)
    }
  }

  function AffCorrection (bool) {
    let i
    if (stor.errChap1) {
      stor.yaexplik = true
      j3pAffiche(stor.lesdiv.explications, null, 'Il faut mettre un chapeau pointu sur le nom d’un angle !')
    }
    if (stor.errChap2) {
      stor.yaexplik = true
      j3pAffiche(stor.lesdiv.explications, null, 'Le chapeau arrondi sert à nommer les arcs de cercle !')
    }
    if (stor.Lexo.quest !== 't') stor.reprep.corrige(false)

    if (bool) {
      desactivezone()
      if (stor.Lexo.quest === 't') {
        for (i = 0; i < stor.consel.length; i++) {
          if (stor.consel[i].sel) SelCorrige(stor.consel[i].ki, false)
        }
        SelCorrige(stor.langle.num)
      } else {
        stor.yaco = true
        j3pAffiche(stor.lesdiv.solution, null, '$ \\widehat{' + stor.langle.noms[0] + '}$ est un nom possible pour l’angle violet. \n<i>Le sommet </i>' + stor.langle.noms[0][1] + '<i> au milieu.</i>')
        stor.reprep.barre()
      }
    } else { me.afficheBoutonValider() }
  }

  function enonceMain () {
    if (me.etapeCourante === 1) {
      // A chaque répétition, on nettoie la scène
      // A commenter si besoin
      me.videLesZones()
      creeLesDiv()
      stor.Lexo = faisChoixExos()
      stor.lesdiv.travail.classList.add('travail')
      stor.lesdiv.consigne.classList.add('enonce')
      stor.lesdiv.explications.classList.remove('explique')
      stor.lesdiv.solution.classList.remove('correction')
    }
    poseQuestion()
    makefig()
    me.afficheBoutonValider()
    me.finEnonce()
  }

  switch (this.etat) {
    case 'enonce':
      // code exécuté au lancement de la section
      if (me.debutDeLaSection) {
        initSection()
      } else {
        enonceMain()
      }
      break // case "enonce":

    case 'correction':
      // On teste si une réponse a été saisie
      stor.yaexplik = false
      if (stor.lesdiv === undefined) return
      stor.lesdiv.explications.innerHTML = ''
      stor.lesdiv.correction.innerHTML = ''
      stor.yaco = false

      if (yaReponse()) {
        // Bonne réponse
        if (isRepOk()) {
          this._stopTimer()
          this.score++
          stor.lesdiv.correction.style.color = this.styles.cbien
          stor.lesdiv.correction.innerHTML = cBien

          desactivezone()
          passetoutvert()
          if (stor.nedco) {
            if (ds.conseil) {
              let np, nm, b
              j3pAffiche(stor.lesdiv.solution, null, 'Le calcul serait plus rapide en regroupant les termes. \n\n')
              np = ''
              nm = ''
              for (b = 0; b < stor.lestermes.length; b++) {
                if (stor.lestermes[b].signe) { np = np + '+' + stor.lestermes[b].val } else { nm = nm + '-' + stor.lestermes[b].val }
              }
              np = np.substring(1) + nm + '$'
              j3pAffiche(stor.lesdiv.solution, null, '$A = ' + np + '\n')
              np = 0
              nm = 0
              for (b = 0; b < stor.lestermes.length; b++) {
                if (stor.lestermes[b].signe) { np += stor.lestermes[b].val } else { nm += stor.lestermes[b].val }
              }
              np = j3pArrondi(np, 1)
              nm = j3pArrondi(nm, 1)
              j3pAffiche(stor.lesdiv.solution, null, '$A = ' + np + '-' + nm + '$\n')
              np = j3pArrondi(np - nm, 1)
              j3pAffiche(stor.lesdiv.solution, null, '$A = ' + np + '$\n')
            }
          }

          this.typederreurs[0]++
          this.cacheBoutonValider()
          this.etat = 'navigation'
          this.sectionCourante()
        } else {
          // Pas de bonne réponse
          stor.lesdiv.correction.style.color = this.styles.cfaux
          stor.lesdiv.correction.innerHTML = cFaux
          if (this.isElapsed) {
            // A cause de la limite de temps :
            this._stopTimer()

            stor.lesdiv.correction.innerHTML += tempsDepasse + '<BR>'
            this.typederreurs[10]++
            this.cacheBoutonValider()

            AffCorrection(true)

            this.etat = 'navigation'
            this.sectionCourante()
          } else {
            // Réponse fausse :
            if (me.essaiCourant < me.donneesSection.nbchances) {
              stor.lesdiv.correction.innerHTML += '<br>' + essaieEncore

              AffCorrection(false)

              this.typederreurs[1]++
            } else {
              // Erreur au nème essai
              this._stopTimer()
              this.cacheBoutonValider()

              AffCorrection(true)

              this.typederreurs[2]++
              this.etat = 'navigation'
              this.sectionCourante()
            }
          }
        }
      } else {
        // pas de réponse
        stor.lesdiv.correction.style.color = this.styles.cfaux
        stor.lesdiv.correction.innerHTML = reponseIncomplete
        me.afficheBoutonValider()
      }

      if (ds.theme === 'zonesAvecImageDeFond') {
        if (stor.yaexplik) {
          stor.lesdiv.explications.classList.add('explique')
        }
        if (stor.yaco) {
          stor.lesdiv.solution.classList.add('correction')
        }
      }

      // Obligatoire
      this.finCorrection()
      break // case "correction":

    case 'navigation':
      if (!this.sectionTerminee()) {
        this.etat = 'enonce'
      }
      this.finNavigation()
      break // case "navigation":
  }
}
