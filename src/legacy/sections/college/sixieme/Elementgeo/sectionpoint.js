import { j3pAddElt, j3pGetNewId, j3pGetRandomInt, j3pShowError } from 'src/legacy/core/functions'
import { j3pAffiche } from 'src/lib/mathquill/functions'
import { addDefaultTable } from 'src/legacy/themes/table'
import { j3pCreeSVG } from 'src/legacy/core/functionsSvg'
import { getMtgCore } from 'src/lib/outils/mathgraph'
import textesGeneriques from 'src/lib/core/textes'

const { cBien, cFaux, essaieEncore, regardeCorrection, reponseIncomplete, tempsDepasse } = textesGeneriques

/**
 * Ajouter une description ici
 * @fileOverview
 * @author Tommy Barroy <t.barroy@laposte.net>
 * @since 2017-10
*/

export const params = {
  outils: ['calculatrice', 'mathquill'],
  /**
   * Ce qui est paramétrable dans l’éditeur de graphe
   * @type {LegacySectionParamsParametres}
   */
  parametres: [
    ['indication', '', 'string', 'Indication pour chaque répétition'],
    ['limite', 0, 'entier', 'Temps disponible par répétition, en secondes'],
    // le modèle gère la compatibilité ascendante avec l’ancien paramètre MiseEnPagePlusClaire
    ['theme', 'standard', 'liste', 'Modèle pour l’affichage', ['standard', 'zonesAvecImageDeFond']]
  ]
}

/**
 * section point
 * @this {Parcours}
 */
export default function main () {
  const me = this
  let ds = me.donneesSection
  const stor = this.storage
  let svgId = stor.svgId

  function FigureSelPrecis (listCache) {
    const textFig = 'TWF0aEdyYXBoSmF2YTEuMAAAABM+TMzNAAJmcv###wEA#wEAAAAAAAAAAANLAAAChwAAAQEAAAAAAAAAAQAAAIj#####AAAAAQAKQ0NhbGNDb25zdAD#####AAJwaQAWMy4xNDE1OTI2NTM1ODk3OTMyMzg0Nv####8AAAABAApDQ29uc3RhbnRlQAkh+1RELRj#####AAAAAQAKQ1BvaW50QmFzZQD#####AQAAAAAOAAFVAMAkAAAAAAAAQBAAAAAAAAAAAAUAAEAwMm6XjU#fQDAybpeNT9######AAAAAQAUQ0Ryb2l0ZURpcmVjdGlvbkZpeGUA#####wEAAAAAEAAAAQAAAAEAAAABAT#wAAAAAAAA#####wAAAAEAD0NQb2ludExpZURyb2l0ZQD#####AQAAAAAOAAFWAMAAAAAAAAAAQBAAAAAAAAAAAAUAAUBAMm6XjU#fAAAAAv####8AAAABAAhDU2VnbWVudAD#####AQAAAAAQAAABAAAAAQAAAAEAAAAD#####wAAAAEAB0NNaWxpZXUA#####wEAAAAAEAAAAAAAAAAAAAAAQAgAAAAAAAAAAAUAAAAAAQAAAAP#####AAAAAgAMQ0NvbW1lbnRhaXJlAP####8BAAAAAAAAAAAAAAAAQBgAAAAAAAAAAAAAAAUMAAAAAAABAAAAAAAAAAEAAAAAAAAAAAABMf####8AAAABAAlDTG9uZ3VldXIA#####wAAAAEAAAADAAAAAgD#####AAAAfwAQAAAAAAAAAAAAAABACAAAAAAAAAAACQABQHzgAAAAAABAeP4UeuFHrgAAAAIA#####wAAAH8AEAAAAAAAAAAAAAAAQAgAAAAAAAAAAAkAAUBEAAAAAAABQHMeFHrhR64AAAACAP####8AAAB#ABAAAAAAAAAAAAAAAEAIAAAAAAAAAAAJAAFAe3AAAAAAAEBzLhR64UeuAAAAAgD#####AAAAfwAQAAAAAAAAAAAAAABACAAAAAAAAAAACQABQH3wAAAAAABAcP4UeuFHrgAAAAIA#####wAAAH8AEAAAAAAAAAAAAAAAQAgAAAAAAAAAAAkAAUB9MAAAAAAAQElwo9cKPXEAAAACAP####8AAAAAABAAAABACAAAAAAAAAAAAAAAAAAAAAAJAABARYAAAAAAAUB8HhR64Ueu#####wAAAAEAB0NDYWxjdWwA#####wAFbWluaTEAATAAAAABAAAAAAAAAAAAAAAJAP####8ABW1heGkxAAIxMAAAAAFAJAAAAAAAAP####8AAAABABRDSW1wbGVtZW50YXRpb25Qcm90bwD#####AAdDdXJzZXVyAAAABQAAAAUAAAADAAAADgAAAA8AAAANAAAAAwAAAAAQAQAAAAAQAAABAAAAAQAAAA0BP#AAAAAAAAAAAAAEAQAAABAAAAB#ABAAAADACAAAAAAAAD#wAAAAAAAAAAAFAABAXUAAAAAAAAAAABH#####AAAAAQALQ0hvbW90aGV0aWUAAAAAEAAAAA3#####AAAAAQAKQ09wZXJhdGlvbgP#####AAAAAQAPQ1Jlc3VsdGF0VmFsZXVyAAAADgAAAAwBAAAADQAAAA4AAAANAAAAD#####8AAAABAAtDUG9pbnRJbWFnZQAAAAAQAQAAAAANAAJPMQDAEAAAAAAAAEAQAAAAAAAAAAAFAAAAABIAAAATAAAACwAAAAAQAAAADQAAAAwDAAAADAEAAAABP#AAAAAAAAAAAAANAAAADgAAAAwBAAAADQAAAA8AAAANAAAADgAAAA4AAAAAEAEAAAAADQACSTEAwAAAAAAAAABACAAAAAAAAAAABQAAAAASAAAAFQAAAAUBAAAAEAAAAH8AEAAAAQAAAAEAAAANAAAAEgAAAAQBAAAAEAAAAH8BEAACazEAwAAAAAAAAABAAAAAAAAAAAAAAQABP9+5+5+5+58AAAAX#####wAAAAIAD0NNZXN1cmVBYnNjaXNzZQEAAAAQAAJhMQAAABQAAAAWAAAAGP####8AAAABAA9DVmFsZXVyQWZmaWNoZWUBAAAAEAAAAH8AAAAAAAAAAADAGAAAAAAAAAAAAAAAGA8AAf###wAAAAEAAAACAAAAAQAAAAAAAAAAAAAAAAIAAAAZAAAAAgD#####AAAAAAAQAAAAQAgAAAAAAAAAAAAAAAAAAAAACQAAQG3AAAAAAABAfH4UeuFHrgAAAAkA#####wAFbWluaTIAATAAAAABAAAAAAAAAAAAAAAJAP####8ABW1heGkyAAIxMAAAAAFAJAAAAAAAAAAAAAoA#####wAHQ3Vyc2V1cgAAAAUAAAAFAAAAAwAAABwAAAAdAAAAGwAAAAMAAAAAHgEAAAAAEAAAAQAAAAEAAAAbAT#wAAAAAAAAAAAABAEAAAAeAAAAfwAQAAAAwAgAAAAAAAA#8AAAAAAAAAAABQAAQF1AAAAAAAAAAAAfAAAACwAAAAAeAAAAGwAAAAwDAAAADQAAABwAAAAMAQAAAA0AAAAcAAAADQAAAB0AAAAOAAAAAB4BAAAAAA0AAk8xAMAQAAAAAAAAQBAAAAAAAAAAAAUAAAAAIAAAACEAAAALAAAAAB4AAAAbAAAADAMAAAAMAQAAAAE#8AAAAAAAAAAAAA0AAAAcAAAADAEAAAANAAAAHQAAAA0AAAAcAAAADgAAAAAeAQAAAAANAAJJMQDAAAAAAAAAAEAIAAAAAAAAAAAFAAAAACAAAAAjAAAABQEAAAAeAAAAfwAQAAABAAAAAQAAABsAAAAgAAAABAEAAAAeAAAAfwEQAAFrAMAAAAAAAAAAQAAAAAAAAAAAAAEAAT#cccccccccAAAAJQAAAA8BAAAAHgACYTIAAAAiAAAAJAAAACYAAAAQAQAAAB4AAAB#AAAAAAAAAAAAwBgAAAAAAAAAAAAAACYPAAH###8AAAABAAAAAgAAAAEAAAAAAAAAAAAAAAACAAAAJwAAAAkA#####wACQTEAAmExAAAADQAAABkAAAAJAP####8AAkEyAAJhMgAAAA0AAAAnAAAACwD#####AAAACQAAAAwDAAAADQAAACkAAAABQCQAAAAAAAAAAAAOAP####8AAAB#ABAAAAAAAAAAAAAAAEAIAAAAAAAAAAAJAAAAAAoAAAArAAAACwD#####AAAACwAAAAwDAAAADQAAACoAAAABQCQAAAAAAAAAAAAOAP####8AAAB#ABAAAAAAAAAAAAAAAEAIAAAAAAAAAAAJAAAAAAwAAAAtAAAAAwD#####AQAA#wEQAAABAAAAAQAAACwAP#AAAAAAAAAAAAADAP####8BAAD#ARAAAAEAAAABAAAALgE#8AAAAAAAAP####8AAAABABBDSW50RHJvaXRlRHJvaXRlAP####8BAAAAABAAAAAAAAAAAAAAAEAIAAAAAAAAAAAJAAAAADAAAAAv#####wAAAAIACUNDZXJjbGVPUgD#####AQAAfwAAAAEAAAAxAAAAAT#pmZmZmZmaAAAAABIA#####wAAAP8AAmJjAAcAAAAxAAAAAT#gAAAAAAAAAP####8AAAABAAlDUm90YXRpb24A#####wAAADEAAAABQEaAAAAAAAAAAAAOAP####8BAAB#ABAAAAAAAAAAAAAAAEAIAAAAAAAAAAAJAAAAAC4AAAA0#####wAAAAEACUNEcm9pdGVBQgD#####AQAAfwAQAAABAAAAAQAAADUAAAAx#####wAAAAEAFkNEcm9pdGVQZXJwZW5kaWN1bGFpcmUA#####wEAAH8AEAAAAQAAAAEAAAAxAAAANv####8AAAABABBDSW50RHJvaXRlQ2VyY2xlAP####8AAAA2AAAAM#####8AAAABABBDUG9pbnRMaWVCaXBvaW50AP####8BAAB#ABAAAAAAAAAAAAAAAEAIAAAAAAAAAAAJAAEAAAA4AAAAFwD#####AQAAfwAQAAAAAAAAAAAAAABACAAAAAAAAAAACQACAAAAOAAAABYA#####wAAADcAAAAzAAAAFwD#####AQAAfwAQAAAAAAAAAAAAAABACAAAAAAAAAAACQACAAAAOwAAABcA#####wEAAH8AEAAAAAAAAAAAAAAAQAgAAAAAAAAAAAkAAQAAADsAAAAGAP####8AAAAAABAAAAAAAAAAAAAAAEAIAAAAAAAAAAAJAAAAAAsAAAAMAAAABgD#####AAAAAAAQAAAAAAAAAAAAAABACAAAAAAAAAAACQAAAAAJAAAACv####8AAAABABFDU3ltZXRyaWVDZW50cmFsZQD#####AAAAPwAAAA4A#####wD#AAAAEAAAAAAAAAAAAAAAQAgAAAAAAAAAAAkAAAAALAAAAEAAAAAYAP####8AAAA+AAAADgD#####AP8AAAAQAAAAAAAAAAAAAABACAAAAAAAAAAACQAAAAAuAAAAQgAAABIA#####wH#AAAAAAAEAAAAMQAAAAFACAAAAAAAAAAAAAADAP####8BAAD#ARAAAAEAAAABAAAAQwE#8AAAAAAAAAAAAAMA#####wEAAP8BEAAAAQAAAAEAAABBAD#wAAAAAAAAAAAAEQD#####Af8AAAAQAAAAAAAAAAAAAABACAAAAAAAAAAACQAAAABFAAAARgAAABQA#####wH#AAAAEAAAAQAAAAQAAAAxAAAARwAAABYA#####wAAAEgAAABEAAAAFwD#####Af8AAAAQAAAAAAAAAAAAAABACAAAAAAAAAAACQACAAAASQAAABcA#####wH#AAAAEAAAAAAAAAAAAAAAQAgAAAAAAAAAAAkAAQAAAEkAAAASAP####8AAAD#AANmYzEABwAAAEsAAAABP+AAAAAAAAAAAAAAAwD#####AQAAAAEQAAABAAAABAAAAEsAP#AAAAAAAAAAAAASAP####8BAAAAAAAABAAAAEsAAAABP+mZmZmZmZoAAAAAFAD#####AAAAAAAQAAABAAJkMgAEAAAAMQAAAEsAAAATAP####8AAAAx#####wAAAAEADENNb2luc1VuYWlyZQAAAAFARoAAAAAAAAAAAA4A#####wEAAAAAEAAAAAAAAAAAAAAAQAgAAAAAAAAAAAkAAAAASwAAAFAAAAAUAP####8AAAAAABAAAAEAAmQxAAQAAABRAAAAMQAAAAUA#####wAAAAAAEAAAAQACczIABAAAADEAAABRAAAABQD#####AAAAAAAQAAABAAJzMwAEAAAAUQAAAEsAAAAYAP####8AAAAxAAAADgD#####AQAAAAAQAAAAAAAAAAAAAABACAAAAAAAAAAACQAAAABRAAAAVf####8AAAABAAxDQmlzc2VjdHJpY2UA#####wEAAAAAEAAAAQAAAAQAAABLAAAAMQAAAFYAAAAWAP####8AAABXAAAAMgAAABcA#####wEAAAAAEAAAAAAAAAAAAAAAQAgAAAAAAAAAAAkAAgAAAFgAAAAXAP####8BAAAAABAAAAAAAAAAAAAAAEAIAAAAAAAAAAAJAAEAAABYAAAAFQD#####AQAAAAAQAAABAAAABAAAAEsAAABPAAAAFQD#####AQAAAAAQAAABAAAABAAAAFEAAABSAAAAEgD#####AQAAAAAAAAQAAABRAAAAAT#pmZmZmZmaAAAAABYA#####wAAAFsAAABOAAAAFwD#####AQAAAAAQAAAAAAAAAAAAAABACAAAAAAAAAAACQABAAAAXgAAABcA#####wEAAAAAEAAAAAAAAAAAAAAAQAgAAAAAAAAAAAkAAgAAAF4AAAAWAP####8AAABcAAAAXQAAABcA#####wEAAAAAEAAAAAAAAAAAAAAAQAgAAAAAAAAAAAkAAQAAAGEAAAAXAP####8BAAAAABAAAAAAAAAAAAAAAEAIAAAAAAAAAAAJAAIAAABhAAAABwD#####AAAAAAEAA25wMQAAAFoUAAAAAAABAAAAAQAAAAEAAAAAAAAAAAABQQAAAAcA#####wAAAAABAANucDIAAABgFAAAAAAAAQAAAAEAAAABAAAAAAAAAAAAAUIAAAAHAP####8AAAAAAQADbnAzAAAAYxQAAAAAAAEAAAABAAAAAQAAAAAAAAAAAAFDAAAAEgD#####AAAA#wADZmNOAAcAAABaAAAAAT#TMzMzMzMzAAAAABIA#####wAAAP8AA2ZjMgAHAAAAUQAAAAE#4AAAAAAAAAAAAAAFAP####8AAAAAABAAAAEAAnMxAAQAAAAxAAAASwAAAAIA#####wEAAAAAEAAAAAAAAAAAAAAAQAgAAAAAAAAAAnBsCQABQITYAAAAAABAZpwo9cKPXP####8AAAABAAhDVmVjdGV1cgD#####AQAAAAAQAAABAAAABAAAADEAAABqAP####8AAAABAA1DVHJhbnNQYXJWZWN0AP####8AAABrAAAADgD#####AQAAAAAQAAAAAAAAAAAAAABACAAAAAAAAAAACQAAAAA5AAAAbAAAAA4A#####wEAAAAAEAAAAAAAAAAAAAAAQAgAAAAAAAAAAAkAAAAAOgAAAGwAAAAFAP####8AAAAAABAAAAEAA3BsMQAEAAAAbQAAAG4AAAAOAP####8BAAAAABAAAAAAAAAAAAAAAEAIAAAAAAAAAAAJAAAAADwAAABsAAAADgD#####AQAAAAAQAAAAAAAAAAAAAABACAAAAAAAAAAACQAAAAA9AAAAbAAAAAUA#####wAAAAAAEAAAAQADcGwyAAQAAABwAAAAcQAAABYA#####wAAAFsAAABMAAAAFwD#####AQAAAAAQAAAAAAAAAAAAAABACAAAAAAAAAAACQABAAAAcwAAABcA#####wEAAAAAEAAAAAAAAAAAAAAAQAgAAAAAAAAAAAkAAgAAAHMAAAAFAP####8AAAAAABAAAAEAA3NwMgAEAAAAdQAAAHQAAAAVAP####8BAAAAABAAAAEAAAAEAAAAMQAAAE8AAAAWAP####8AAAB3AAAAMwAAABcA#####wEAAAAAEAAAAAAAAAAAAAAAQAgAAAAAAAAAAAkAAgAAAHgAAAAXAP####8BAAAAABAAAAAAAAAAAAAAAEAIAAAAAAAAAAAJAAEAAAB4AAAABQD#####AAAAAAAQAAABAANzcDEABAAAAHoAAAB5AAAABQD#####AAAAAAAQAAABAAJ0MQAEAAAAOQAAADr#####AAAAAQAQQ0Ryb2l0ZVBhcmFsbGVsZQD#####AQAAAAAQAAABAAAABAAAAEsAAAB8AAAAFgD#####AAAAfQAAAEwAAAAXAP####8BAAAAABAAAAAAAAAAAAAAAEAIAAAAAAAAAAAJAAIAAAB+AAAAFwD#####AQAAAAAQAAAAAAAAAAAAAABACAAAAAAAAAAACQABAAAAfgAAAAUA#####wAAAAAAEAAAAQADc2IyAAQAAAB#AAAAgAAAAAUA#####wAAAAAAEAAAAQACdDIABAAAADwAAAA9AAAAHQD#####AQAAAAAQAAABAAAABAAAAEsAAACCAAAAFgD#####AAAAgwAAAEwAAAAXAP####8BAAAAABAAAAAAAAAAAAAAAEAIAAAAAAAAAAAJAAIAAACEAAAAFwD#####AQAAAAAQAAAAAAAAAAAAAABACAAAAAAAAAAACQABAAAAhAAAAAUA#####wAAAAAAEAAAAQADc2IxAAQAAACGAAAAhQAAAAf##########w=='
    svgId = j3pGetNewId()
    stor.svgId = svgId

    const A1 = j3pGetRandomInt(0, 10)
    const A2 = j3pGetRandomInt(0, 10)
    const listeNom = []
    const listZig = ['#bc', '#fc1', '#fc2', '#fcN']

    if (me.questionCourante % 7 === 1) {
      listeNom.push({ t: '#np2', nom: stor.lettre2 })
    }
    if (me.questionCourante % 7 === 2) {
      listeNom.push({ t: '#np2', nom: stor.lettre2 })
    }
    if (me.questionCourante % 7 !== 4) {
      listeNom.push({ t: '#np1', nom: stor.lettre })
    }
    if (me.questionCourante % 7 === 5) {
      listeNom.push({ t: '#np2', nom: stor.lettre2 })
    }
    if (me.questionCourante % 7 === 6) {
      listeNom.push({ t: '#np2', nom: stor.lettre2 })
      listeNom.push({ t: '#np3', nom: stor.lettre3 })
    }
    if (me.questionCourante % 7 === 0) {
      listeNom.push({
        t: '#np2',
        nom: stor.lettre2
      })
    }

    stor.Svg = j3pCreeSVG(stor.lesdiv.travail, { id: svgId, width: 465, height: 300, style: { border: 'solid black 1px' } })
    stor.mtgAppLecteur.removeAllDoc()
    stor.mtgAppLecteur.addDoc(svgId, textFig, true)
    stor.mtgAppLecteur.giveFormula2(svgId, 'A1', A1, true)
    stor.mtgAppLecteur.giveFormula2(svgId, 'A2', A2, true)
    stor.mtgAppLecteur.calculateAndDisplayAll(true)
    for (let ju = 0; ju < listeNom.length; ju++) {
      stor.mtgAppLecteur.setText(svgId, listeNom[ju].t, listeNom[ju].nom, true)
    }
    for (let ju = 0; ju < listCache.length; ju++) {
      stor.mtgAppLecteur.setVisible(svgId, listCache[ju], false, true)
    }
    for (let ju = 0; ju < listZig.length; ju++) {
      stor.mtgAppLecteur.setColor(svgId, listZig[ju], 0, 0, 0, true, 0)
    }
    stor.mtgAppLecteur.getDoc(svgId).defaultCursor = 'pointer'
    stor.mtgAppLecteur.addCallBackToSVGListener(svgId, 'touchstart', function (ev) {
      if (stor.blok) return
      const tt = stor.Svg.getBoundingClientRect()
      stor.fX = ev.changedTouches[0].clientX - tt.left
      stor.fY = ev.changedTouches[0].clientY - tt.top
      stor.blok = true
      stor.repEleve = 'faux'
      me.sectionCourante()
    })
    stor.mtgAppLecteur.addCallBackToSVGListener(svgId, 'mousedown', function (ev) {
      if (stor.blok) return
      const tt = stor.Svg.getBoundingClientRect()
      stor.fX = ev.clientX - tt.left
      stor.fY = ev.clientY - tt.top
      stor.blok = true
      stor.repEleve = 'faux'
      me.sectionCourante()
    })
    stor.mtgAppLecteur.addEventListener(svgId, '#bc', 'mousedown', function () {
      if (stor.blok) return
      stor.blok = true
      stor.repEleve = 'bien'
      me.sectionCourante()
    })
    stor.mtgAppLecteur.addEventListener(svgId, '#bc', 'touchstart', function () {
      if (stor.blok) return
      stor.blok = true
      stor.repEleve = 'bien'
      me.sectionCourante()
    })
    stor.mtgAppLecteur.addEventListener(svgId, '#fcN', 'mousedown', function () {
      if (stor.blok) return
      stor.blok = true
      stor.repEleve = 'nompoint'
      me.sectionCourante()
    })
    stor.mtgAppLecteur.addEventListener(svgId, '#fcN', 'touchstart', function () {
      if (stor.blok) return
      stor.blok = true
      stor.repEleve = 'nompoint'
      me.sectionCourante()
    })
  }
  function afficheco (bool) {
    let lacool
    if (bool === true) lacool = [0, 138, 115]
    if (bool === false) {
      lacool = [50, 50, 200]
    }
    if (stor.repEleve === 'faux') {
      stor.mtgAppLecteur.setPointPosition(svgId, '#pl', stor.fX, stor.fY, true)
      stor.mtgAppLecteur.setColor(svgId, '#pl1', 214, 71, 0, true)
      stor.mtgAppLecteur.setColor(svgId, '#pl2', 214, 71, 0, true)
    }
    if (stor.repEleve === 'nompoint') {
      stor.mtgAppLecteur.setText(svgId, '#np1', stor.lettre, true)
      stor.mtgAppLecteur.setColor(svgId, '#np1', 214, 71, 0, true)
    }
    stor.mtgAppLecteur.setVisible(svgId, '#t1', true, true)
    stor.mtgAppLecteur.setVisible(svgId, '#t2', true, true)
    stor.mtgAppLecteur.setColor(svgId, '#t1', lacool[0], lacool[1], lacool[2], true)
    stor.mtgAppLecteur.setColor(svgId, '#t2', lacool[0], lacool[1], lacool[2], true)
    stor.mtgAppLecteur.getDoc(svgId).defaultCursor = ''
    stor.mtgAppLecteur.updateFigure(svgId)
    if (me.questionCourante % 7 === 5) {
      j3pAffiche(stor.lesdiv.solution, null, ds.textes.rappel)
    }
  }
  function yareponse () {
    return true
  }
  function bonneReponse () {
    return (stor.repEleve === 'bien')
  }
  function getDonnees () {
    return {

      // Nombre de répétitions de l’exercice
      nbrepetitions: 7,
      nbetapes: 1,

      // Si indication !== "" alors un lien "Indication" s’affiche dans la zone Inférieure Gauche (zones.IG
      // une indication<br>de deux lignes...";
      indication: '',

      // pour le cas de presentation1bis
      // En effet pas d’indication possible pour cette présentation
      // surchargeindication: false;

      // Nombre de chances dont dispose l’él§ve pour répondre à la question
      nbchances: 1,

      structure: 'presentation1bis', //  || "presentation2" || "presentation3"  || "presentation1bis"

      // Paramètres de la section, avec leurs valeurs par défaut.

      /*
          Les textes présents dans la section
          Sont donc accessibles dans le code par donneesSection.textes.phrase1
          Possibilité d’écrire dans le code :
          var lestextes: donneesSection.textes;
          puis accès à l’aide de lestextes.phrase1
          */
      textes: {
        couleurexplique: '#A9E2F3',
        couleurcorrec: '#A9F5A9',
        couleurenonce: '#D8D8D8',
        couleurenonceg: '#F3E2A9',
        couleuretiquettes: '#A9E2F3',
        Consigne1: '<b>Clique précisément</b> sur le point £a .',
        Consigne2: 'Le point £a est le point d’intersection des deux droites.',
        ERRnompoint: 'Tu as cliqué sur <b>le nom</b> du point £a !<br>',
        regarde: 'Tu n’as pas cliqué sur le point £a . <BR> Regarde bien la correction en vert.',
        benouaicbien: 'Bravo !',
        rappel: "<u><b>rappel</b></u>: Un point s'écrit toujours avec une majuscule d’imprimerie. "
      },

      pe: 0
    }
  }
  function intiSection () {
    ds = getDonnees()
    me.donneesSection = ds

    me.surcharge()
    if (ds.Presentation) { ds.structure = 'presentation1' }

    // Construction de la page
    me.construitStructurePage(ds.structure)

    me.typederreurs = [0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0]
    ds.nbitems = ds.nbetapes * ds.nbrepetitions
    me.score = 0

    me.afficheTitre('Situer précisément un point')

    if (ds.indication) me.indication(me.zones.IG, ds.indication)

    // chargement mtg
    getMtgCore({ withMathJax: true })
      .then(
        // success
        (mtgAppLecteur) => {
          stor.mtgAppLecteur = mtgAppLecteur
          stor.listePourCalc = mtgAppLecteur.createList('TWF0aEdyYXBoSmF2YTEuMAAAABI+TMzNAAJmcv###wEA#wEAAAAAAAAAAAQsAAACvQAAAQEAAAAAAAAAAQAAAAP#####AAAAAQAKQ0NhbGNDb25zdAD#####AAJwaQAWMy4xNDE1OTI2NTM1ODk3OTMyMzg0Nv####8AAAABAApDQ29uc3RhbnRlQAkh+1RELRj#####AAAAAQAHQ0NhbGN1bAD#####AAFlAAZleHAoMSn#####AAAAAgAJQ0ZvbmN0aW9uBwAAAAE#8AAAAAAAAAAAAAIA#####wABeAABMAAAAAEAAAAAAAAAAP###############w==')
          enonceMain()
        },
        // failure
        (error) => {
          j3pShowError(error, { message: 'mathgraph n’est pas correctement chargé', mustNotify: true })
        })
      // plantage dans le code de success
      .catch(j3pShowError)
  } // initSection

  function enonceMain () {
    me.videLesZones()
    me.afficheBoutonValider()
    // ou dans le cas de presentation3
    // me.ajouteBoutons();

    me.cacheBoutonValider()
    stor.lesdiv = {}
    stor.lesdiv.conteneur = j3pAddElt(me.zonesElts.MG, 'div', {
      style: me.styles.etendre('toutpetit.enonce', { padding: '10px' })
    })
    stor.lesdiv.consigne = addDefaultTable(stor.lesdiv.conteneur, 1, 1)[0][0]
    stor.lesdiv.travail = addDefaultTable(stor.lesdiv.conteneur, 1, 1)[0][0]
    stor.lesdiv.explications = j3pAddElt(stor.lesdiv.conteneur, 'div')
    stor.lesdiv.solution = j3pAddElt(stor.lesdiv.conteneur, 'div')
    stor.lesdiv.correction = j3pAddElt(me.zonesElts.MD, 'div', {
      style: me.styles.etendre('toutpetit.enonce', { padding: '10px' })
    })
    stor.lesdiv.explications.style.color = me.styles.cfaux
    stor.lesdiv.solution.style.color = me.styles.toutpetit.correction.color
    me.zonesElts.MG.classList.add('fond')
    stor.lesdiv.travail.classList.add('travail')
    stor.lesdiv.consigne.classList.add('enonce')

    stor.lettre = String.fromCharCode(j3pGetRandomInt(65, 80))
    do {
      stor.lettre2 = String.fromCharCode(j3pGetRandomInt(65, 80))
    } while (stor.lettre === stor.lettre2)
    do {
      stor.lettre3 = String.fromCharCode(j3pGetRandomInt(65, 80))
    } while (stor.lettre3 === stor.lettre2 || stor.lettre3 === stor.lettre)

    j3pAffiche(stor.lesdiv.consigne, null, ds.textes.Consigne1,
      { a: stor.lettre })
    let fifig

    if (me.questionCourante % 7 === 1) {
      fifig = ['#s1', '#s2', '#s3', '#d1', '#d2', '#np3', '#sp2', '#sp1']
    }
    if (me.questionCourante % 7 === 2) {
      fifig = ['#s1', '#s2', '#s3', '#d1', '#np3', '#sb1', '#sb2', '#t1', '#t2']
    }

    if (me.questionCourante % 7 === 3) {
      fifig = ['#s1', '#s2', '#s3', '#np3', '#sb1', '#sb2', '#t1', '#t2', '#sp2', '#sp1', '#np2']
    }

    if (me.questionCourante % 7 === 4) {
      j3pAffiche(stor.lesdiv.consigne, null, '\n' + ds.textes.Consigne2,
        { a: stor.lettre })
      fifig = ['#s1', '#s2', '#s3', '#np3', '#sb1', '#sb2', '#t1', '#t2', '#sp2', '#sp1', '#np2', '#np1']
    }

    if (me.questionCourante % 7 === 5) {
      stor.lettre2 = stor.lettre.toLowerCase()
      fifig = ['#s1', '#s2', '#s3', '#d1', '#np3', '#sb1', '#sb2', '#t1', '#t2']
    }

    if (me.questionCourante % 7 === 6) {
      fifig = ['#d1', '#sb1', '#sb2', '#t1', '#t2', '#d2', '#sp1', '#sp2']
    }
    if (me.questionCourante % 7 === 0) {
      fifig = ['#s2', '#s3', '#d1', '#d2', '#np3', '#sb1', '#sb2', '#t1', '#t2']
    }

    stor.repere = new FigureSelPrecis(fifig)
    stor.blok = false
    // Obligatoire

    me.finEnonce()
    me.cacheBoutonValider()
  }

  switch (this.etat) {
    case 'enonce':
      // code exécuté au lancement de la section
      if (this.debutDeLaSection) {
        intiSection()
      } else {
        enonceMain()
      }
      break // case "enonce":

    case 'correction':
      // On teste si une réponse a été saisie
      // var fcts_valid = stor.fcts_valid;
      // var reponse = fcts_valid.validationGlobale();
      stor.yaexplik = false

      if (!yareponse()) {
        stor.lesdiv.correction.style.color = this.styles.cfaux
        stor.lesdiv.correction.innerHTML = reponseIncomplete
      } else { // Une réponse a été saisie
        // Bonne réponse
        if (bonneReponse()) {
          this._stopTimer()
          this.score++
          stor.lesdiv.correction.style.color = this.styles.cbien
          stor.lesdiv.correction.innerHTML = cBien

          afficheco(true)

          this.typederreurs[0]++
          this.cacheBoutonValider()
          this.etat = 'navigation'
          this.sectionCourante()
        } else {
          // Pas de bonne réponse
          stor.lesdiv.correction.style.color = this.styles.cfaux

          // A cause de la limite de temps :
          if (this.isElapsed) { // limite de temps
            this._stopTimer()

            stor.repEleve = ''
            afficheco(false)

            stor.lesdiv.correction.innerHTML = tempsDepasse
            this.typederreurs[10]++
            this.cacheBoutonValider()

            this.etat = 'navigation'
            this.sectionCourante()
          } else { // Réponse fausse :
            stor.lesdiv.correction.innerHTML = cFaux

            if (this.essaiCourant < ds.nbchances) {
              stor.lesdiv.correction.innerHTML += '<br>' + essaieEncore

              this.typederreurs[1]++
              // indication éventuelle ici
            } else { // Erreur au nème essai
              this._stopTimer()
              this.cacheBoutonValider()
              stor.lesdiv.correction.innerHTML += '<br>' + regardeCorrection

              if (stor.repEleve === 'nompoint') {
                stor.yaexplik = true
                j3pAffiche(stor.lesdiv.explications, null, ds.textes.ERRnompoint,
                  { a: stor.lettre })
              }

              afficheco(false)

              this.typederreurs[2]++
              this.etat = 'navigation'
              this.sectionCourante()
            }
          }
        }
      }

      if (ds.theme === 'zonesAvecImageDeFond') {
        if (stor.yaexplik) {
          stor.lesdiv.explications.classList.add('explique')
        }
      }

      // Obligatoire
      this.finCorrection()
      break // case "correction":

    case 'navigation':

      if (this.sectionTerminee()) {
        // On détermine la phrase d’état renvoyée par la section
        this.parcours.pe = (ds.nbetapes * this.score) / ds.nbitems
        this.afficheBoutonSectionSuivante()
        this.focus('sectioncontinuer')
      } else {
        this.etat = 'enonce'
        this.afficheBoutonSuite()
        this.focus('boutoncontinuer')
      }
      // Obligatoire
      this.finNavigation()

      break // case "navigation":
  }
}
