import { j3pAddElt, j3pArrondi, j3pGetRandomInt, j3pShowError } from 'src/legacy/core/functions'
import { getMtgCore } from 'src/lib/outils/mathgraph'
import { j3pAffiche } from 'src/lib/mathquill/functions'
import { addDefaultTable } from 'src/legacy/themes/table'
import textesGeneriques from 'src/lib/core/textes'

import FigNomme02 from './FigNomme02'

import './nomme02.scss'

const { cBien, cFaux, essaieEncore, regardeCorrection, reponseIncomplete } = textesGeneriques

export const params = {
  outils: ['calculatrice', 'mathquill'],
  /**
   * Ce qui est paramétrable dans l’éditeur de graphe
   * @type {LegacySectionParamsParametres}
   */
  parametres: [
    ['nbrepetitions', 9, 'entier', 'Nombre de répétitions de la section'],
    ['indication', '', 'string', 'Indication pour chaque répétition'],
    ['limite', 0, 'entier', 'Temps disponible par répétition, en secondes'],
    ['SEGMENT', false, 'boolean', 'Le contenant peut être un segment.'],
    ['DEMI_DROITE', false, 'boolean', 'Le contenant peut être une demi-droite.'],
    ['DROITE', false, 'boolean', 'Le contenant peut être une droite.'],
    ['QUADRILATERE', false, 'boolean', 'Le contenant peut être un quadrilatère quelconque.'],
    ['PARALLELOGRAMME', false, 'boolean', 'Le contenant peut être un parallélogramme.'],
    ['LOSANGE', false, 'boolean', 'Le contenant peut être un losange.'],
    ['RECTANGLE', false, 'boolean', 'Le contenant peut être un rectangle.'],
    ['CARRE', false, 'boolean', 'Le contenant peut être un carré.'],
    ['CERCLE', false, 'boolean', 'Le contenant peut être un cercle.'],
    ['ARC', false, 'boolean', 'Le contenant peut être un arc de cercle.'],
    ['SECTEUR', false, 'boolean', 'Le contenant peut être un secteur de disque.'],
    ['ANGLE', false, 'boolean', 'Le contenant peut être un angle.'],
    ['TRIANGLE', false, 'boolean', 'Le contenant peut être un triangle quelconque.'],
    ['TRIANGLE_ISOCELE', false, 'boolean', 'Le contenant peut être un triangle isocèle.'],
    ['TRIANGLE_EQUILATERAL', false, 'boolean', 'Le contenant peut être un triangle équilatéral.'],
    ['TRIANGLE_RECTANGLE', false, 'boolean', 'Le contenant peut être un triangle rectangle.'],
    ['CUBE', false, 'boolean', 'Le contenant peut être un cube.'],
    ['PAVE_DROIT', false, 'boolean', 'Le contenant peut être un pavé droit.'],
    ['PRISME', false, 'boolean', 'Le contenant peut être un prisme droit.'],
    ['CYLINDRE', false, 'boolean', 'Le contenant peut être un cylindre.'],
    ['CYLINDRE_REVOLUTION', false, 'boolean', 'Le contenant peut être un cylindre de révolution.'],
    ['PYRAMIDE', false, 'boolean', 'Le contenant peut être une pyramide.'],
    ['CONE', false, 'boolean', 'Le contenant peut être un cône.'],
    ['CONE_REVOLUTION', false, 'boolean', 'Le contenant peut être un cône de révolution.'],
    ['SPHERE', false, 'boolean', 'Le contenant peut être une sphère.'],
    ['arete', false, 'boolean', 'L’élève doit sélectionner toutes les arêtes du solide. <BR><BR> Valable uniquement pour: <i> pavé droit, cube, prisme, pyramide </i>.'],
    ['arete_laterale', false, 'boolean', 'L’élève doit sélectionner toutes les arêtes latérales du solide. <BR><BR> Valable uniquement pour: <i> prisme, pyramide </i>.'],
    ['arete_parallele', false, 'boolean', 'L’élève doit sélectionner toutes les arêtes parallèles à une arête donnée du solide. <BR><BR> Valable uniquement pour: <i> pavé droit, cube, prisme  </i>.'],
    ['arete_cachee', false, 'boolean', 'L’élève doit sélectionner toutes les arêtes cachées du solide. <BR><BR> Valable uniquement pour: <i> pavé droit, cube, prisme, pyramide </i>.'],
    ['arete_perpendiculaire', false, 'boolean', 'L’élève doit sélectionner toutes les arêtes perpendiculaires à une arête donnée du solide. <BR><BR> Valable uniquement pour: <i> pavé droit, cube, prisme </i>.'],
    ['arete_orthogonale', false, 'boolean', 'L’élève doit sélectionner toutes les arêtes orthogonales à une arête donnée du solide. <BR><BR> Valable uniquement pour: <i> pavé droit, cube, prisme, pyramide </i>.'],
    ['sommet', false, 'boolean', 'L’élève doit sélectionner tous les sommets du solide. <BR><BR> Valable uniquement pour: <i> pavé droit, cube, prisme, pyramide, triangles, quadrilatères </i>.'],
    ['sommet_oppose', false, 'boolean', 'L’élève doit sélectionner le sommet opposé à un sommet (ou côté) du contenant. <BR><BR> Valable uniquement pour: <i>  triangles, quadrilatères </i>.'],
    ['sommet_principal', false, 'boolean', 'L’élève doit sélectionner le sommet principal (ou LE sommet) du contenant. <BR><BR> Valable uniquement pour: <i>  triangle isocèle, cônes, pyramide </i>.'],
    ['sommet_angle_droit', false, 'boolean', 'L’élève doit sélectionner le sommet de l’angle droit du triangle rectangle. <BR><BR> Valable uniquement pour: <i>  triangle rectangle </i>.'],
    ['sommet_consecutif', false, 'boolean', 'L’élève doit sélectionner les sommets consécutifs à un sommet donné du quadrilatère. <BR><BR> Valable uniquement pour: <i>  quadrilatères </i>.'],
    ['face', false, 'boolean', 'L’élève doit sélectionner toutes les faces du contenant. <BR><BR> Valable uniquement pour: <i>  cube, pavé droit, prisme, pyramide, cylindres, cônes </i>.'],
    ['face_cachee', false, 'boolean', 'L’élève doit sélectionner toutes les faces cachées du contenant. <BR><BR> Valable uniquement pour: <i>  cube, pavé droit, prisme, pyramide  </i>.'],
    ['face_laterale', false, 'boolean', 'L’élève doit sélectionner toutes les faces latérales du contenant. <BR><BR> Valable uniquement pour: <i>   prisme, pyramide, cylindres, cônes  </i>.'],
    ['face_parallele', false, 'boolean', 'L’élève doit sélectionner toutes les faces paralléles à une face donnée du contenant. <BR><BR> Valable uniquement pour: <i>   cube, pavé droit, prisme, cylindres   </i>.'],
    ['face_perpendiculaire', false, 'boolean', 'L’élève doit sélectionner toutes les faces perpendiculaires à une face donnée du contenant. <BR><BR> Valable uniquement pour: <i>   cube, pavé droit, prisme    </i>.'],
    ['cote', false, 'boolean', 'L’élève doit sélectionner tous les côtés du contenant. <BR><BR> Valable uniquement pour: <i>   triangles, quadrilatères, angle    </i>.'],
    ['cote_oppose', false, 'boolean', 'L’élève doit sélectionner le côté opposé à un sommet (ou côté) du contenant. <BR><BR> Valable uniquement pour: <i>  triangles, quadrilatères </i>.'],
    ['cote_parallele', false, 'boolean', 'L’élève doit sélectionner tous les côtés parallèles à un côté donné du contenant. <BR><BR> Valable uniquement pour: <i>  parallélogramme, carré, rectangle, losange </i>.'],
    ['cote_perpendiculaire', false, 'boolean', 'L’élève doit sélectionner tous les côtés parallèles à un côté donné du contenant. <BR><BR> Valable uniquement pour: <i>  carré, rectangle,  triangle rectangle </i>.'],
    ['cote_oppose_angle', false, 'boolean', 'L’élève doit sélectionner le côté opposé d’un angle aigu donné du triangle rectangle. <BR><BR> Valable uniquement pour: <i>  triangle rectangle</i>.'],
    ['cote_adjacent', false, 'boolean', 'L’élève doit sélectionner le côté adjacent d’un angle aigu donné du triangle rectangle. <BR><BR> Valable uniquement pour: <i>  triangle rectangle</i>.'],
    ['cote_consecutif', false, 'boolean', 'L’élève doit sélectionner les côtés consécutifs à un côté donné du quadrilatère. <BR><BR> Valable uniquement pour: <i>  quadrilatères </i>.'],
    ['rayon', false, 'boolean', 'L’élève doit sélectionner tous les rayons du contenant. <BR><BR> Valable uniquement pour: <i>  cercle, cylindres, cônes, sphère </i>.'],
    ['diametre', false, 'boolean', 'L’élève doit sélectionner tous les diamètres du contenant. <BR><BR> Valable uniquement pour: <i>  cercle, cylindres, cônes, sphère </i>.'],
    ['corde', false, 'boolean', 'L’élève doit sélectionner toutes les cordes du cercle. <BR><BR> Valable uniquement pour: <i>  cercle  </i>.'],
    ['hypotenuse', false, 'boolean', 'L’élève doit sélectionner l’hypoténuse du triangle rectangle. <BR><BR> Valable uniquement pour: <i>  triangle rectangle</i>.'],
    ['diagonale', false, 'boolean', 'L’élève doit sélectionner les diagonales du quadrilatère. <BR><BR> Valable uniquement pour: <i>   quadrilatères </i>.'],
    ['generatrice', false, 'boolean', 'L’élève doit sélectionner les génératrices du cône de révolution. <BR><BR> Valable uniquement pour: <i>   cône de révolution </i>.'],
    ['angle', false, 'boolean', 'L’élève doit sélectionner tous les angles du contenant. <BR><BR> Valable uniquement pour: <i>   triangles, quadrilatères </i>.'],
    ['angle_droit', false, 'boolean', 'L’élève doit sélectionner tous les angles droits du contenant. <BR><BR> Valable uniquement pour: <i>   triangle rectangle, rectangle, carré </i>.'],
    ['angle_consecutif', false, 'boolean', 'L’élève doit sélectionner tous les angles consécutifs à un angle donné du contenant. <BR><BR> Valable uniquement pour: <i>   quadrilatères </i>.'],
    ['angle_oppose', false, 'boolean', 'L’élève doit sélectionner l’angle opposé à un angle donné du contenant. <BR><BR> Valable uniquement pour: <i>   quadrilatères </i>.'],
    ['angle_egal', false, 'boolean', 'L’élève doit sélectionner tous les angles de même mesure qu’un angle donné du contenant. <BR><BR> Valable uniquement pour: <i>   triangle équilatéral, triangle isocèle, parallélogrammes </i>.'],
    ['equateur', false, 'boolean', 'L’élève doit sélectionner l’équateur de la sphère. <BR><BR> Valable uniquement pour: <i>   sphère </i>.'],
    ['meridien', false, 'boolean', 'L’élève doit sélectionner tous les méridiens de la sphère. <BR><BR> Valable uniquement pour: <i>   sphère </i>.'],
    ['grand_cercle', false, 'boolean', 'L’élève doit sélectionner tous les grands cercles de la sphère. <BR><BR> Valable uniquement pour: <i>   sphère </i>.'],
    ['pole', false, 'boolean', 'L’élève doit sélectionner les deux pôles d’un sphère. <BR><BR> Valable uniquement pour: <i>   sphère </i>.'],
    ['origine', false, 'boolean', 'L’élève doit sélectionner l’origine de la demi-droite. <BR><BR> Valable uniquement pour: <i>   demi-droite </i>.'],
    ['milieu', false, 'boolean', 'L’élève doit sélectionner le milieu du segment. <BR><BR> Valable uniquement pour: <i>   segment </i>.'],
    ['extremites', false, 'boolean', 'L’élève doit sélectionner les extrémités du contenant. <BR><BR> Valable uniquement pour: <i>   segment, arc de cercle </i>.'],
    ['centre', false, 'boolean', 'L’élève doit sélectionner le centre du contenant. <BR><BR> Valable uniquement pour: <i>   cercle, sphère, cônes </i>.'],
    ['centre_symetrie', false, 'boolean', 'L’élève doit sélectionner le centre de symétrie du contenant. <BR><BR> Valable uniquement pour: <i>   cercle, parallélogrammes, segment, droites </i>.'],
    ['axe', false, 'boolean', 'L’élève doit sélectionner tous les axes de symétrie du contenant. <BR><BR> Valable uniquement pour: <i>   cercle, quadrilatères, triangles, segment, droites, demi-droites, arc de cercle, angle </i>.'],
    ['bissectrice', false, 'boolean', 'L’élève doit sélectionner la bissectrice de l’angle. <BR><BR> Valable uniquement pour: <i>   angle </i>.'],
    ['mediatrice', false, 'boolean', 'L’élève doit sélectionner la médiatrice d’un segment ou un côté donné. <BR><BR> Valable uniquement pour: <i>   segment, triangles, quadrilatères </i>.'],
    ['hauteur', false, 'boolean', 'L’élève doit sélectionner la (ou les) hauteur(s) du contenant. <BR><BR> Valable uniquement pour: <i>    triangles, cônes, pyramides, cylindre, prisme </i>.'],
    ['hauteur_relative', false, 'boolean', 'L’élève doit sélectionner la hauteur relative à un côté donné du contenant. <BR><BR> Valable uniquement pour: <i>    triangles, parallélogramme  </i>.'],
    ['base', false, 'boolean', 'L’élève doit sélectionner la (ou les) base(s) du contenant. <BR><BR> Valable uniquement pour: <i>    triangle isocèle, prisme, cylindres, pyramide, cônes  </i>.'],
    ['base_relative', false, 'boolean', 'L’élève doit sélectionner la (ou les) base(s) relative(s) à un côté donné du contenant. <BR><BR> Valable uniquement pour: <i>    triangles, parallélogramme  </i>.'],
    ['longueur_egale', false, 'boolean', 'L’élève doit sélectionner tous les segment de même longueur qu’un segment donné du contenant. <BR><BR> Valable uniquement pour: <i>    triangle équlatéral, triangle isocèle, parallélogramme, cube, pavé droit, prisme, cylindres, cônes, sphère, cercle  </i>.'],
    // le modèle gère la compatibilité ascendante avec l’ancien paramètre MiseEnPagePlusClaire
    ['theme', 'standard', 'liste', 'Modèle pour l’affichage', ['standard', 'zonesAvecImageDeFond']]

  ],
  // dans le cas d’une section QUANTITATIVE
  pe: [
    { pe_1: 'Maitrise' },
    { pe_2: 'Suffisant' },
    { pe_3: 'Suffisant - erreur nature' },
    { pe_4: 'Insuffisant' },
    { pe_5: 'Insuffisant - erreur nature' },
    { pe_6: 'Très insuffisant' },
    { pe_7: 'Très insuffisant - erreur nature' }
  ]

}

/**
 * section nomme02
 * @this {Parcours}
 */
export default function main () {
  const me = this
  const stor = this.storage
  let ds = me.donneesSection

  function elemetype (str) {
    if (str.indexOf('sommet') !== -1) { return 'point' }
    if (str.indexOf('face') !== -1) { return 'face' }
    if (str.indexOf('arete') !== -1) { return 'segment' }
    if (str.indexOf('cote') !== -1) {
      if (stor.contenant !== 'ANGLE') return 'segment'
      return 'demi-droite'
    }
    if (str.indexOf('rayon') !== -1) { return 'segment' }
    if (str.indexOf('diametre') !== -1) { return 'segment' }
    if (str.indexOf('corde') !== -1) { return 'segment' }
    if (str.indexOf('hypotenuse') !== -1) { return 'segment' }
    if (str.indexOf('generatrice') !== -1) { return 'segment' }
    if (str.indexOf('diagonale') !== -1) { return 'segment' }
    if (str.indexOf('equateur') !== -1) { return 'cercle' }
    if (str.indexOf('meridien') !== -1) { return 'demi-cercle' }
    if (str.indexOf('grand_cercle') !== -1) { return 'cercle' }
    if (str.indexOf('parallele') !== -1) { return 'cercle' }
    if (str.indexOf('origine') !== -1) { return 'point' }
    if (str.indexOf('milieu') !== -1) { return 'point' }
    if (str.indexOf('extremites') !== -1) { return 'point' }
    if (str.indexOf('axe') !== -1) { return 'droite' }
    if (str.indexOf('bissectrice') !== -1) { return 'droite' }
    if (str.indexOf('mediatrice') !== -1) { return 'droite' }
    if (str.indexOf('hauteur') !== -1) {
      if ((stor.contenant !== 'PRISME') && (stor.contenant !== 'CONE') && (stor.contenant !== 'CONE_REVOLUTION') && (stor.contenant !== 'CYLINDRE_REVOLUTION') && (stor.contenant !== 'CYLINDRE')) {
        return 'droite'
      }
      return 'segment'
    }
    // spe
    if (str.indexOf('base') !== -1) {
      if ((stor.contenant === 'TRIANGLE') || (stor.contenant === 'TRIANGLE_RECTANGLE') || (stor.contenant === 'TRIANGLE_EQUILATERAL') || (stor.contenant === 'TRIANGLE_ISOCELE')) {
        return 'segment'
      }
      return 'face'
    }
    if (str.indexOf('longueur_egale') !== -1) { return 'segment' }
    if (str.indexOf('angle') !== -1) { return 'angle' }
    if (str.indexOf('centre') !== -1) { return 'point' }
    if (str.indexOf('pole') !== -1) { return 'point' }
  }

  function yareponse () {
    stor.rep = stor.fig.listeselect()
    if ((stor.element === 'axe') || (stor.element === 'centre_symetrie')) {
      return true
    } else {
      if (me.isElapsed) { return true }
      return (stor.rep.oui.length > 0)
    }
  }

  function bonneReponse () {
    stor.ErrManque = false
    stor.ListeManque = []
    stor.ErrTrop = false
    stor.ListeTrop = []
    let goodrep = true

    // test manque
    for (let i = 0; i < stor.rep.non.length; i++) {
      for (let j = 0; j < stor.rep.non[i][0].length; j++) {
        if (stor.rep.non[i][0][j] === stor.element) {
          stor.ErrManque = true
          stor.ListeManque.push(stor.rep.non[i][0][j])
          stor.fig.corrige(stor.rep.non[i][1])
          goodrep = false
        }
      }
    }

    // test trop
    for (let i = 0; i < stor.rep.oui.length; i++) {
      let oklocal = false
      for (let j = 0; j < stor.rep.oui[i][0].length; j++) {
        if (stor.rep.oui[i][0][j] === stor.element) { oklocal = true }
      }
      if (!oklocal) {
        stor.ErrTrop = true
        goodrep = false
        stor.ListeTrop.push(stor.rep.oui[i][0])
        stor.fig.corrige(stor.rep.oui[i][1], false)
      } else {
        stor.fig.corrige(stor.rep.oui[i][1], true)
      }
    }

    // desactive

    stor.fig.blok = true

    // determine phrase co
    switch (stor.element) {
      case 'arete' :
        switch (stor.contenant) {
          case 'CUBE' :
            me.phraseco = 'Un cube a 12 arêtes.'
            break
          case 'PAVE_DROIT' :
            me.phraseco = 'Un pavé droit a 12 arêtes.'
            break
          case 'PRISME' :
          case 'PYRAMIDE':
            me.phraseco = 'Les arêtes sont les segment<br>qui relient les sommets.'
            break
        }
        break
      case 'extremites' :
        me.phraseco = 'Les extrémités sont les deux points <br>de début et de fin du segment.'
        break
      case 'arete_laterale' :
        if (stor.contenant === 'PRISME') { me.phraseco = 'Les arêtes latérales relient <br>les bases entre elles' } else { me.phraseco = 'Les arêtes latérales relient<br> les sommets de la base au sommet de la pyramide' }
        break
      case 'arete_parallele' :
        switch (stor.contenant) {
          case 'CUBE' :
            me.phraseco = 'Dans un cube, on trouve trois groupes<br> de quatre arêtes parallèles.'
            break
          case 'PAVE_DROIT' :
            me.phraseco = 'Dans un pavé droit, on trouve trois groupes<br> de quatre arêtes parallèles.'
            break
          case 'PRISME' :
            me.phraseco = 'Dans un prisme droit, <br>les arêtes latérales sont parallèles,<br> ainsi que les arêtes correspondantes des deux bases.'
            break
        }
        break
      case 'arete_cachee' :
        me.phraseco = 'Les arêtes cachées sont toujours<br> tracées en pointillées'
        break
      case 'arete_perpendiculaire' :
        switch (stor.contenant) {
          case 'CUBE' :
            me.phraseco = 'Dans un cube, toutes les faces<br> sont des carrés.'
            break
          case 'PAVE_DROIT' :
            me.phraseco = 'Dans un pavé droit, toutes les faces<br> sont des rectangles.'
            break
          case 'PRISME' :
            me.phraseco = 'Dans un prisme droit, les faces latérales<br> sont des rectangles.'
            break
        }
        break
      case 'arete_orthogonale' :
        switch (stor.contenant) {
          case 'CUBE' :
            me.phraseco = 'Dans un cube, <br>toutes les faces sont des carrés.'
            break
          case 'PAVE_DROIT' :
            me.phraseco = 'Dans un pavé droit, toutes les faces<br> sont des rectangles.'
            break
          case 'PRISME' :
            me.phraseco = 'Dans un prisme droit, les faces latérales<br> sont des rectangles.'
            break
        }
        break
      case 'sommet' :
        switch (stor.contenant) {
          case 'CUBE' :
          case 'PAVE_DROIT' :
          case 'PRISME' :
          case 'PYRAMIDE' :
            me.phraseco = 'Dans un solide, les sommets sont<br> les points d’intersection des arêtes.'
            break
          case 'ANGLE' :
            me.phraseco = 'Dans un angle, le sommet est<br> l’origine des deux côtés.'
            break
          default:
            me.phraseco = 'Dans une figure plane, les sommets<br> sont les points d’intersection des côtés.'
        }
        break
      case 'sommet_oppose' :
        me.phraseco = "Sommet opposé veut dire <i>'sommet en face de '</i> "
        break
      case 'sommet_principal' :
        switch (stor.contenant) {
          case 'CONE_REVOLUTION' :
          case 'CONE' :
          case 'PYRAMIDE' :
            me.phraseco = 'Le sommet est le point qui <br>ne se trouve pas dans le plan de la base.'
            break
          default:
            me.phraseco = 'Le sommet principal est le sommet qui<br> relie les deux côtés de même longueur.'
        }
        break
      case 'sommet_angle_droit' :
        me.phraseco = 'Le sommet de l’angle droit est<br> en face de l’hypoténuse.'
        break
      case 'sommet_consecutif' :
        me.phraseco = "Sommets consécutifs veut dire <i>'sommets qui se suivent'</i> "
        break
      case 'face' :
        switch (stor.contenant) {
          case 'CUBE' :
            me.phraseco = 'Un cube a 6 faces carrées.'
            break
          case 'PAVE_DROIT' :
            me.phraseco = 'Un pavé droit a 6 faces rectangulaires.'
            break
          case 'PRISME' :
            me.phraseco = 'Un prisme droit a deux faces parallèles et<br> superposables et<br>reliées par des faces rectangulaires.'
            break
          case 'CONE' :
          case 'CONE_REVOLUTION' :
            me.phraseco = 'Un cône est constitué<br> d’un disque et d’une face latérale'
            break
          case 'CYLINDRE' :
          case 'CYLINDRE_REVOLUTION' :
            me.phraseco = 'Un cylindre est constitué<br> de deux disques parallèles et<br> d’une face latérale'
            break
          case 'PYRAMIDE' :
            me.phraseco = 'Une pyramide est constituée <br>d’un polygone de base <br>relié au sommet par des triangles.'
            break
        }
        break
      case 'face_cachee' :
        me.phraseco = 'On reconnait une face cachée par<br> les arêtes cachées qui l’entourent. '
        break
      case 'face_laterale' :
        switch (stor.contenant) {
          case 'PRISME' :
            me.phraseco = 'Les faces latérales d’un prisme droit sont<br> les rectangles qui relient les bases. '
            break
          case 'CONE' :
            me.phraseco = 'La face latérale d’un cône est<br> une portion d’ellipse.'
            break
          case 'CONE_REVOLUTION' :
            me.phraseco = 'La face latérale d’un cône de révolution est<br> un secteur circulaire.'
            break
          case 'CYLINDRE' :
            me.phraseco = 'La face latérale d’un cylindre<br> est un parallélogramme.'
            break
          case 'CYLINDRE_REVOLUTION' :
            me.phraseco = 'La face latérale d’un cylindre de révolution est <br>un rectangle.'
            break
          case 'PYRAMIDE' :
            me.phraseco = 'Les faces latérales d’une pyramide sont<br> les triangles qui relient la base au<br> sommet de la pyramide.'
            break
        }
        break
      case 'face_parallele' :
        switch (stor.contenant) {
          case 'PRISME' :
            me.phraseco = 'Dans un prisme, les bases sont parallèles. '
            break
          case 'CYLINDRE' :
          case 'CYLINDRE_REVOLUTION' :
            me.phraseco = 'Dans un cylindre, les bases sont parallèles.'
            break
          case 'CUBE' :
            me.phraseco = 'Dans un cube, les faces opposées sont<br> parallèles deux à deux.'
            break
          case 'PAVE_DROIT' :
            me.phraseco = 'Dans un pavé droit, les faces opposées sont<br> parallèles deux à deux.'
            break
        }
        break
      case 'face_perpendiculaire' :
        switch (stor.contenant) {
          case 'PRISME' :
            me.phraseco = 'Dans un prisme, <br>les faces latérales sont<br> perpendiculaires aux bases. '
            break
          case 'CUBE' :
            me.phraseco = 'Dans un cube, <br>les faces consécutives sont<br> perpendiculaires.'
            break
          case 'PAVE_DROIT' :
            me.phraseco = 'Dans un pavé droit, <br>les faces consécutives sont perpendiculaires.'
            break
        }
        break
      case 'cote' :
        switch (stor.contenant) {
          case 'ANGLE' :
            me.phraseco = 'Dans un angle, <br>les côtés sont les demi-droites autour<br> de l’angle. '
            break
          default :
            me.phraseco = 'Les côtés sont les segments qui<br> relient les sommets entre eux.'
        }
        break
      case 'cote_oppose' :
        me.phraseco = "Côté opposé veut dire <i>'côté en face de'</i> "
        break
      case 'angle_oppose' :
        me.phraseco = "angle opposé veut dire <i>'angle en face de'</i> "
        break
      case 'angle_consecutif' :
        me.phraseco = "angles consécutifs veut dire <i>'angles qui se suivent'</i> "
        break
      case 'cote_parallele' :
        switch (stor.contenant) {
          case 'CARRE' :
            me.phraseco = 'Dans un carré,<br> les côtés opposés sont paralléles deux à deux.'
            break
          case 'RECTANGLE' :
            me.phraseco = 'Dans un rectangle,<br> les côtés opposés sont paralléles deux à deux.'
            break
          case 'LOSANGE' :
            me.phraseco = 'Dans un losange,<br> les côtés opposés sont paralléles deux à deux.'
            break
          case 'PARALLELOGRAMME' :
            me.phraseco = 'Dans un parallélogramme,<br> les côtés opposés sont paralléles deux à deux.'
            break
        }
        break
      case 'cote_perpendiculaire' :
        switch (stor.contenant) {
          case 'CARRE' :
            me.phraseco = 'Un carré a quatre angles droits. '
            break
          case 'RECTANGLE' :
            me.phraseco = 'Un rectangle a quatre angles droits.'
            break
          case 'TRIANGLE_RECTANGLE' :
            me.phraseco = 'Dans un triangle rectangle,<br> les deux côtés de l’angle droit sont perpendiculaires.'
            break
        }
        break
      case 'cote_oppose_angle' :
        me.phraseco = 'Dans un triangle,<br> le côté opposé d’un angle est<br> le côté « <i>en face</i> » de l’angle.'
        break
      case 'cote_adjacent' :
        me.phraseco = 'Dans un triangle rectangle,<br> le côté adjacent d’un angle est<br> le côté de l’angle qui n’est pas l’hypoténuse <br><i>(donc le côté qui relie <br>le sommet de cet angle à l’angle droit).</i>'
        break
      case 'cote_consecutif' :
        me.phraseco = 'Côtés consécutifs veut dire « <i>côtés qui se suivent</i> » '
        break
      case 'rayon' :
        switch (stor.contenant) {
          case 'CERCLE' :
            me.phraseco = 'Un rayon est un segment qui<br> relie le centre d’un cercle à<br> un point de ce cercle.'
            break
          default:
            me.phraseco = 'Un rayon est un segment qui<br> relie le centre à<br> un point de l’arc de cercle.'
            break
        }
        break
      case 'diametre' :
        me.phraseco = 'Un diamètre est une corde passant<br> par le centre du cercle.'
        break
      case 'corde' :
        me.phraseco = 'Un corde est un segment reliant<br> deux points d’un cercle.'
        break
      case 'hypotenuse' :
        me.phraseco = 'L’hypoténuse est le segment opposé au<br> sommet de l’angle droit.<br><i> (c’est également le plus grand côté)</i>.'
        break
      case 'diagonale' :
        me.phraseco = 'Une diagonale est un segment qui<br> relie deux sommets opposés.'
        break
      case 'generatrice' :
        me.phraseco = 'Une génératrice d’un cône de révolution est<br> un segment qui reliant le sommet à<br> un point du cercle de la base.'
        break
      case 'angle' :
        switch (stor.contenant) {
          case 'CARRE' :
          case 'RECTANGLE' :
          case 'LOSANGE' :
          case 'PARALLELOGRAMME' :
          case 'QUADRILATERE' :
            me.phraseco = 'Un quadrilatère a quatre angles.'
            break
          case 'SECTEUR' :
            me.phraseco = 'L’angle d’un secteur circulaire est<br> compris entre deux rayons.'
            break
          default:
            me.phraseco = 'Un triangle a trois angles.'
        }
        break
      case 'angle_droit' :
        switch (stor.contenant) {
          case 'CARRE' :
            me.phraseco = 'Un carré a quatre angles droits. '
            break
          case 'RECTANGLE' :
            me.phraseco = 'Un rectangle a quatre angles droits.'
            break
          case 'TRIANGLE_RECTANGLE' :
            me.phraseco = 'Un triangle rectangle a un angle droit.'
            break
        }
        break
      case 'angle_egal' :
        switch (stor.contenant) {
          case 'CARRE' :
            me.phraseco = 'Un carré a quatre angles droits.'
            break
          case 'RECTANGLE' :
            me.phraseco = 'Un rectangle a quatre angles droits.'
            break
          case 'LOSANGE' :
            me.phraseco = 'Dans un losange, <br>les angles opposés sont de même mesure.'
            break
          case 'PARALLELOGRAMME' :
            me.phraseco = 'Dans un parallélogramme, <br>les angles opposés sont de même mesure.'
            break
          case 'TRIANGLE_ISOCELE' :
            me.phraseco = 'Dans un triangle isocèle,<br> les angles à la base sont de même mesure.'
            break
          case 'TRIANGLE_EQUILATERAL' :
            me.phraseco = 'Dans un triangle équilatéral, <br>les trois angles mesurent 60°.'
            break
        }
        break
      case 'equateur' :
        me.phraseco = 'L’équateur est le grand cercle situé<br> à égale distance des deux pôles.'
        break
      case 'pole' :
        me.phraseco = 'Il y a deux pôles: <br>Le pôle Nord et le pôle Sud.'
        break
      case 'meridien' :
        me.phraseco = 'Un méridien est<br> un demi-cercle reliant les deux pôles.'
        break
      case 'grand_cercle' :
        me.phraseco = 'Un grand cercle d’une sphère est<br> un cercle de même centre et <br>de même rayon que la sphère.'
        break
      case 'origine' :
        me.phraseco = 'L’origine d’une demi-droite est<br> le point de départ de la demi-droite.'
        break
      case 'milieu' :
        me.phraseco = 'Le milieu d’un segment est<br> le point du segment à égale distance des<br> deux extrémités.'
        break
      case 'extremite' :
        me.phraseco = 'Les extrémités sont<br> les deux points de départ et<br> d’arrêt de la ligne.'
        break
      case 'centre' :
        switch (stor.contenant) {
          case 'CERCLE' :
            me.phraseco = 'Tous les points du cercle sont<br> à égale distance du centre.'
            break
          case 'CONE_REVOLUTION':
          case 'CONE' :
            me.phraseco = 'Le centre d’un cône est<br> le centre de sa base.'
            break
          case 'SECTEUR' :
            me.phraseco = 'Le centre d’un secteur circulaire est<br> le centre du disque dont il provient.'
            break
          case 'ARC' :
            me.phraseco = 'Le centre d’un arc circulaire est<br> le centre du cercle dont il provient.'
            break
          case 'SPHERE' :
            me.phraseco = 'Tous les points d’une sphère sont<br> à égale distance du centre.'
            break
        }
        break
      case 'centre_symetrie' :
        switch (stor.contenant) {
          case 'CARRE' :
            me.phraseco = 'Dans un carré,<br> le point d’intersection des diagonales est également<br> le centre de symétrie.'
            break
          case 'RECTANGLE' :
            me.phraseco = 'Dans un rectangle,<br> le point d’intersection des diagonales est également<br> le centre de symétrie.'
            break
          case 'LOSANGE' :
            me.phraseco = 'Dans un losange,<br>le point d’intersection des diagonales est également<br> le centre de symétrie.'
            break
          case 'PARALLELOGRAMME' :
            me.phraseco = 'Dans un parallélogramme, <br>le point d’intersection des diagonales est également<br> le centre de symétrie.'
            break
          case 'SEGMENT' :
            me.phraseco = 'Le milieu d’un segment est également<br> son centre de symétrie.'
            break
          case 'DROITE' :
            me.phraseco = 'Tous les points d’une droite sont également<br> centre de symétrie de cette droite.'
            break
          case 'CERCLE' :
            me.phraseco = 'Le centre d’un cercle est également<br> son centre de symétrie.'
            break
          case 'TRIANGLE' :
          case 'TRIANGLE_RECTANGLE' :
          case 'TRIANGLE_ISOCELE' :
          case 'TRIANGLE_EQUILATERAL' :
            me.phraseco = 'Un triangle n’a pas de centre de symétrie'
            break
          default :
            me.phraseco = 'Cette figure n’a pas de centre de symétrie'
            break
        }
        break
      case 'axe' :
        switch (stor.contenant) {
          case 'CARRE' :
            me.phraseco = 'Un carré a quatre axes de symétrie'
            break
          case 'RECTANGLE' :
            me.phraseco = 'Un rectangle a deux axes de symétrie'
            break
          case 'LOSANGE' :
            me.phraseco = 'Un losange a deux axes de symétrie.'
            break
          case 'PARALLELOGRAMME' :
            me.phraseco = 'Un parallélogramme quelconque n’a <br>pas d’axe de symétrie.'
            break
          case 'SEGMENT' :
            me.phraseco = 'Un segment a deux axes de symétrie.'
            break
          case 'DROITE' :
            me.phraseco = 'Toutes les droites perpendiculaires à <br>une droite donnée sont des axes de<br> symétrie de cette droite. <BR> La droite elle-même est un axe de symétrie.'
            break
          case 'CERCLE' :
            me.phraseco = 'Toutes les droites passant par<br> le centre d’un cercle sont<br> des axes de symétrie de ce cercle.'
            break
          case 'TRIANGLE_ISOCELE' :
            me.phraseco = 'Un triangle isocèle a<br> un axe de symétrie.'
            break
          case 'ANGLE' :
            me.phraseco = 'Un angle a un axe de symétrie.'
            break
          case 'ARC' :
            me.phraseco = 'Un arc de cercle a<br> un axe de symétrie.'
            break
          case 'SECTEUR' :
            me.phraseco = 'Un secteur circulaire a<br> un axe de symétrie.'
            break
          case 'DEMI_DROITE' :
            me.phraseco = 'Une demi-droite a<br> un axe de symétrie.'
            break
          case 'TRIANGLE_EQUILATERAL' :
            me.phraseco = 'Un triangle équilatéral a<br> trois axes de symétrie.'
            break
          default :
            me.phraseco = 'Cette figure n’a pas<br> d’axe de symétrie'
            break
        }
        break
      case 'bissectrice' :
        me.phraseco = 'La bissectrice d’un angle partage<br> cet angle en deux parties égales.'
        break
      case 'mediatrice' :
        me.phraseco = 'La médiatrice d’un segment est la droite <br>perpendiculaire à ce segment passant par<br> son milieu.'
        break
      case 'hauteur' :
        switch (stor.contenant) {
          case 'PRISME' :
            me.phraseco = 'Dans un prisme droit, <br>toutes les arêtes latérales ont<br> une mesure égale à la hauteur du prisme droit.'
            break
          case 'CYLINDRE' :
            me.phraseco = 'La hauteur est <br>la longueur d’un segment reliant perpendiculairement<br> les deux plans des bases.'
            break
          case 'CYLINDRE_REVOLUTION' :
            me.phraseco = 'Dans un cylindre de révolution,<br> chaque segment reliant deux points correspondants des<br> deux bases a une mesure égale à<br> la hauteur du cylindre.'
            break
          case 'CONE' :
            me.phraseco = 'La hauteur est <br>la longueur d’un segment reliant perpendiculairement<br> le sommet du cône au plan de la base.'
            break
          case 'CONE_REVOLUTION' :
            me.phraseco = 'La hauteur est égale à<br> la longueur reliant le sommet du cône de révolution au<br> centre de sa base.'
            break
          case 'PYRAMIDE' :
            me.phraseco = 'La hauteur est la longueur d’un<br> segment reliant perpendiculairement<br> le sommet de la pyramide au<br> plan de la base.'
            break
          default :
            me.phraseco = 'Une hauteur d’un triangle est<br> une droite passant par<br> un sommet perpendiculairement au côté opposé. '
            break
        }
        break
      case 'hauteur_relative' :
        switch (stor.contenant) {
          case 'PARALLELOGRAMME' :
            me.phraseco = 'La hauteur relative est <br>perpendiculaire au côté donné.'
            break
          default :
            me.phraseco = 'La hauteur relative est<br> la hauteur perpendiculaire au côté donné. '
            break
        }
        break
      case 'base' :
        switch (stor.contenant) {
          case 'PRISME' :
            me.phraseco = 'Dans un prisme droit,<br>les deux bases sont <br>parallèles et superposables.'
            break
          case 'CYLINDRE' :
          case 'CYLINDRE_REVOLUTION' :
            me.phraseco = 'Dans un cylindre, <br>les deux bases sont<br> deux disques parallèles et de même rayon.'
            break
          case 'CONE' :
          case 'CONE_REVOLUTION' :
            me.phraseco = 'La base est le disque relié au sommet.'
            break
          case 'PYRAMIDE' :
            me.phraseco = 'La base est le polygone relié au<br> sommet par des triangles.'
            break
          default :
            me.phraseco = 'La base est le côté opposé au<br> sommet principal. '
            break
        }
        break
      case 'base_relative' :
        me.phraseco = (stor.contenant === 'PARALLELOGRAMME')
          ? 'Les bases relatives sont <br>perpendiculaires à la hauteur donnée.'
          : 'La base relative est <br>le côté perpendiculaire à la hauteur donnée. '
        break
      case 'longueur_egale' :
        switch (stor.contenant) {
          case 'PARALLELOGRAMME' :
            me.phraseco = 'Deux côtés opposés sont de même longueurs, <br>les diagonales se coupent en leur milieu.'
            break
          case 'PRISME' :
            me.phraseco = 'Les arêtes latérales sont de même longueur<br>. Les deux bases sont superposables.'
            break
          case 'CARRE' :
            me.phraseco = 'Les côtés sont de même longueur, <br>les diagonales sont de même longueur et<br> se coupent en leur milieu.'
            break
          case 'RECTANGLE' :
            me.phraseco = 'Les côtés opposés sont de même longueur,<br> les diagonales sont de même longueur et<br> se coupent en leur milieu.'
            break
          case 'LOSANGE' :
            me.phraseco = 'Les côtés sont de même longueur, <br>et les diagonales se coupent en leur milieu.'
            break
          case 'CERCLE' :
            me.phraseco = 'Les rayons sont tous de la même longueur, <br> les diamètres sont tous de la même longueur.'
            break
          case 'TRIANGLE_ISOCELE' :
            me.phraseco = 'Un triangle isocèle a<br> deux côtés de même longueur.'
            break
          case 'TRIANGLE_EQUILATERAL' :
            me.phraseco = 'Un triangle équilatéral a<br> trois côtés de même longueur.'
            break
          case 'CUBE' :
            me.phraseco = 'Toutes les arêtes d’un cube ont <br>la même longueur.'
            break
          case 'PAVE_DROIT' :
            me.phraseco = 'Les arêtes parallèles d’un pavé droit ont<br> la même longueur.'
            break
          case 'CONE' :
            me.phraseco = 'Les rayons sont tous de la même longueur,<br> les diamètres sont tous de la même longueur.'
            break
          case 'CONE_REVOLUTION' :
            me.phraseco = 'Les rayons sont tous de la même longueur,<br> les diamètres sont tous de la même longueur. <BR> Les génératrices sont également de la même longueur.'
            break
          case 'CYLINDRE_REVOLUTION' :
            me.phraseco = 'Les rayons sont tous de la même longueur, <br>les diamètres sont tous de la même longueur. <BR> Les arêtes perpendiculaires aux bases également.'
            break
          case 'CYLINDRE' :
            me.phraseco = 'Les rayons sont tous de la même longueur,<br> les diamètres sont tous de la même longueur.'
            break
        }
        break
    }

    return goodrep
  } // bonneReponse

  function getDonneesDefault () {
    return {

      // Nombre de répétitions de l’exercice
      nbrepetitions: 9,
      nbetapes: 1,
      nbitems: me.nbetapes * me.nbrepetitions,

      SEGMENT: false,
      DEMI_DROITE: false,
      DROITE: false,
      QUADRILATERE: false,
      PARALLELOGRAMME: false,
      LOSANGE: false,
      RECTANGLE: false,
      CARRE: false,
      CERCLE: false,
      ARC: false,
      SECTEUR: false,
      ANGLE: false,
      TRIANGLE: false,
      TRIANGLE_ISOCELE: false,
      TRIANGLE_EQUILATERAL: false,
      TRIANGLE_RECTANGLE: false,
      CUBE: false,
      PAVE_DROIT: false,
      PRISME: false,
      CYLINDRE: false,
      CYLINDRE_REVOLUTION: false,
      PYRAMIDE: false,
      CONE: false,
      CONE_REVOLUTION: false,
      SPHERE: false,
      arete: false,
      face: false,
      arete_laterale: false,
      face_laterale: false,
      cote_parallele: false,
      cote_perpendiculaire: false,
      arete_parallele: false,
      face_parallele: false,
      arete_perpendiculaire: false,
      arete_orthogonale: false,
      face_perpendiculaire: false,
      bissectrice: false,
      meridien: false,
      equateur: false,
      parallele: false,
      grand_cercle: false,
      origine: false,
      milieu: false,
      extremites: false,
      sommet: false,
      cote: false,
      generatrice: false,
      angle: false,
      angle_oppose: false,
      angle_consecutif: false,
      sommet_oppose: false,
      cote_oppose: false,
      hauteur: false,
      base: false,
      base_relative: false,
      hauteur_relative: false,
      axe: false,
      longueur_egale: false,
      sommet_principal: false,
      angle_droit: false,
      angle_egal: false,
      cote_oppose_angle: false,
      sommet_consecutif: false,
      cote_consecutif: false,
      diagonale: false,
      rayon: false,
      diametre: false,
      corde: false,
      centre: false,
      centre_symetrie: false,
      mediatrice: false,
      arete_cachee: false,
      face_cachee: false,
      hypotenuse: false,
      sommet_angle_droit: false,
      cote_adjacent: false,
      pole: false,

      // Si indication !== "" alors un lien "Indication" s’affiche dans la zone Inférieure Gauche (me.zones.IG
      // une indication<br>de deux lignes...";
      indication: '',

      // pour le cas de presentation1bis
      // En effet pas d’indication possible pour cette présentation
      // surchargeindication: false,

      // Nombre de chances dont dispose l’él§ve pour répondre à la question
      nbchances: 1,

      structure: 'presentation3', //  || "presentation2" || "presentation3"  || "presentation1bis"

      // Paramètres de la section, avec leurs valeurs par défaut.
      pe_1: 'Maitrise',
      pe_2: 'Suffisant',
      pe_3: 'Suffisant - erreur nature',
      pe_4: 'Insuffisant',
      pe_5: 'Insuffisant - erreur nature',
      pe_6: 'Très insuffisant',
      pe_7: 'Très insuffisant - erreur nature',

      /*
        Les textes présents dans la section
        Sont donc accessibles dans le code par ds.textes.phrase1
        Possibilité d’écrire dans le code :
        var lestextes = ds.textes,
        puis accès à l’aide de lestextes.phrase1
        */
      textes: {
        couleurexplique: '#A9E2F3',
        couleurcorrec: '#A9F5A9',
        couleurenonce: '#D8D8D8',
        couleurenonceg: '#F3E2A9',
        couleuretiquettes: '#A9E2F3',

        couleurMiseEnVal: '#FF00BF',
        Clic: 'Sélectionne ',
        NomContenant: ' £a ci-dessous.',
        ContDef: ['du segment', 'de la demi-droite', 'du triangle quelconque', 'du triangle', 'du triangle équilatéral', 'du triangle ', 'du quadrilatère quelconque', 'du rectangle', 'du carré', 'du losange', 'du parallélogramme', 'du cercle', 'de l’arc de cercle', 'du secteur circulaire', 'de l’angle', 'du cube', 'du pavé droit', 'du prisme droit', 'du cylindre', 'du cylindre de révolution', 'de la pyramide', 'du cône', 'du cône de révolution', 'de la sphère', 'de la droite']
      },
      // Par défaut, pas de temps limite. Décommentez pour avoir un temps limite par défaut. En secondes.
      // limite: 10,
      /*
          Phrase d’état renvoyée par la section
          Si me.pe=0 alors la section est quantitative et renvoie un score entre 0 et 1.
          Dans le cas où la section est qualitative, lister les différents pe renvoyées :
              me.pe_1 = "toto"
              me.pe_2 = "tata"
              etc
          Dans la partie navigation, il faudra affecter la variable me.parcours.pe à l’une des précédentes.
          Par exemple,
          me.parcours.pe = ds.pe_2
          */
      pe: 0
    }
  } // getDonneesDefault
  function tradTout (str) {
    switch (str) {
      case 'extremites':
        return 'les extrémités'
      case 'pole':
        return 'les pôles'
      case 'milieu':
        return 'le milieu'
      case 'origine':
        return 'l’origine'
      case 'arete':
        return 'toutes les arêtes'
      case 'generatrice':
        return 'toutes les génératrices tracées'
      case 'diagonale':
        return 'toutes les diagonales'
      case 'centre_symetrie':
        return (stor.contenant === 'DROITE')
          ? 'tous les centres de symétrie nommés'
          : 'le centre de symétrie (si il existe)'
      case 'bissectrice':
        return 'la bissectrice'
      case 'angle_oppose':
        return 'l’angle opposé à l’angle' + ' <span class="rougeNn" > violet</span>'
      case 'angle_consecutif':
        return 'tous les angles consécutifs à l’angle ' + ' <span class="rougeNn" > violet</span>'
      case 'axe':
        switch (stor.contenant) {
          case 'CERCLE':
          case 'DROITE' :
            return 'tous les axes de symétrie tracés'
          default :
            return 'tous les axes de symétrie'
        }
      case 'hypotenuse':
        return 'l’hypoténuse'
      case 'mediatrice':
        switch (stor.contenant) {
          case 'SEGMENT' :
            return 'la médiatrice'
          default :
            return 'la médiatrice du côté <span class="rougeNn" > violet</span>'
        }
      case 'sommet_angle_droit':
        return 'le sommet de l’angle droit'
      case 'cote':
        return 'tous les côtés'
      case 'angle':
        switch (stor.contenant) {
          case 'SECTEUR':
            return 'l’angle'
          default :
            return 'tous les angles'
        }

      case 'angle_droit':
        switch (stor.contenant) {
          case 'RECTANGLE':
          case 'LOSANGE':
          case 'PARALLELOGRAMME':
          case 'CARRE':
            return 'tous les angles droits' //* aide clique face
          default:
            return 'l’angle droit' // la pour cône   //* aide clique face
        }
      case 'angle_egal':
        return 'tous les angles de même mesure que l’angle ' + ' <span class="rougeNn" > violet</span>'
      case 'cote_perpendiculaire':
        return 'tous les côtés perpendiculaires au côté <span class="rougeNn" > violet</span>'
      case 'cote_parallele':
        return 'tous les côtés paralléles au côté <span class="rougeNn" > violet</span>'
      case 'sommet_oppose':
        switch (stor.contenant) {
          case 'RECTANGLE':
          case 'LOSANGE':
          case 'PARALLELOGRAMME':
          case 'QUADRILATERE':
          case 'CARRE':
            return 'le sommet opposé au sommet <span class="rougeNn" > violet</span>'
          default:
            return 'le sommet opposé au côté <span class="rougeNn" > violet</span>'
        }
      case 'cote_oppose':
        switch (stor.contenant) {
          case 'RECTANGLE':
          case 'LOSANGE':
          case 'PARALLELOGRAMME':
          case 'QUADRILATERE':
          case 'CARRE':
            return 'le côté opposé au côté <span class="rougeNn" > violet</span>'
          default:
            return 'le côté opposé au sommet <span class="rougeNn" > violet</span>'
        }
      case 'cote_consecutif':
        return 'tous les côtés consécutifs au côté <span class="rougeNn" > violet</span>'
      case 'sommet_consecutif':
        return 'tous les sommets consécutifs au sommet <span class="rougeNn" > violet</span>'
      case 'cote_oppose_angle':
        return 'le côté opposé à l’angle ' + ' <span class="rougeNn" > violet</span>'
      case 'cote_adjacent':
        return 'le côté adjacent à l’angle ' + ' <span class="rougeNn" > violet</span>'
      case 'hauteur_relative':
        return 'la hauteur relative au côté <span class="rougeNn" > violet</span>'
      case 'base_relative':
        switch (stor.contenant) {
          case 'PARALLELOGRAMME':
            return 'toutes les bases relatives à la hauteur <span class="rougeNn" > violette</span>'
          default:
            return 'la base relative à la hauteur <span class="rougeNn" > violette</span>'
        }
      case 'arete_cachee':
        return 'toutes les arêtes cachées'
      case 'face_cachee':
        return 'toutes les faces cachées'
      case 'face_perpendiculaire':
        return 'toutes les faces perpendiculaires à la face <span class="rougeNn" > violette</span>'
      case 'arete_perpendiculaire':
        return 'toutes les arêtes perpendiculaires à l’arête ' + ' <span class="rougeNn" > violette</span>'
      case 'arete_orthogonale':
        return 'toutes les arêtes orthogonales à l’arête ' + ' <span class="rougeNn" > violette</span>'
      case 'arete_parallele':
        return 'toutes les arêtes parallèles à l’arête ' + ' <span class="rougeNn" > violette</span>'
      case 'face_parallele':
        return 'toutes les faces parallèles à la face <span class="rougeNn" > violette</span>'
      case 'arete_laterale':
        return 'toutes les arêtes latérale'
      case 'face':
        return 'toutes les faces' //* aide clique face
      case 'hauteur':
        switch (stor.contenant) {
          case 'TRIANGLE_RECTANGLE':
          case 'TRIANGLE':
          case 'TRIANGLE_ISOCELE':
          case 'TRIANGLE_EQUILATERAL':
          case 'PARALLELOGRAMME' :
            return 'toutes les hauteurs tracées' //* aide clique face
          case 'CYLINDRE_REVOLUTION':
          case 'CYLINDRE' :
          case 'PRISME':
            return 'tous les segments de longueur égale à la hauteur' //* aide clique face
          default:
            return 'la hauteur' // la pour cône   //* aide clique face
        }
      case 'face_laterale':
        switch (stor.contenant) {
          case 'PRISME':
          case 'PYRAMIDE' :
            return 'toutes les faces latérales' //* aide clique face
          default:
            return 'la face latérale' //* aide clique face
        }

      case 'sommet':
        switch (stor.contenant) {
          case 'ANGLE':
            return 'le sommet' //* aide clique face
          default:
            return 'tous les sommets'
        }
      case 'sommet_principal':
        switch (stor.contenant) {
          case 'TRIANGLE_ISOCELE':
            return 'le sommet principal' //* aide clique face
          default:
            return 'le sommet'
        }
      case 'base':
        switch (stor.contenant) {
          case 'PRISME':
          case 'CYLINDRE_REVOLUTION':
          case 'CYLINDRE' :
            return 'toutes les bases' //* aide clique face
          default:
            return 'la base' // la pour cône   //* aide clique face
        }

      case 'grand_cercle':
        return 'tous les grands cercles tracés'
      case 'diametre':
        return 'tous les diamètres tracés'
      case 'parallele':
        return 'tous les paralléles'
      case 'meridien':
        return 'tous les méridiens tracés'
      case 'rayon':
        return 'tous les rayons tracés'
      case 'centre':
        return 'le centre'
      case 'corde':
        return 'toutes les cordes tracées'
      case 'equateur':
        return 'l’équateur'
      case 'longueur_egale' :
        return 'tous les segments de même longueur que le segment <span class="rougeNn" > violet</span>'
      default :
        j3pShowError(Error(`Cas non prévu (${str})`), { message: 'Erreur interne (cas non prévu)', mustNotify: true })
    }
  }

  function initSection () {
    ds = getDonneesDefault()
    me.donneesSection = ds
    // on change le titre

    me.surcharge()

    ds.pe = [
      ds.pe_1,
      ds.pe_2,
      ds.pe_3,
      ds.pe_4,
      ds.pe_5,
      ds.pe_6,
      ds.pe_7
    ]
    // Construction de la page
    me.construitStructurePage(ds.structure)

    ds.ContenantPossible = []
    let figplanes = false
    let seg = false
    let dte = false
    let demidroite = false
    let triangles = false
    let quad = false
    let arc = false
    let cercle = false
    let angle = false
    let solid = false
    let prisme = false
    let cylindre = false
    let cylindrerevo = false
    let pyra = false
    let cone = false
    let sphere = false
    let secteur = false

    ds.SEGMENT = ds.SEGMENT && (ds.centre_symetrie || ds.milieu || ds.extremites || ds.mediatrice || ds.axe)
    ds.DEMI_DROITE = ds.DEMI_DROITE && (ds.centre_symetrie || ds.origine || ds.axe)
    ds.TRIANGLE = ds.TRIANGLE && (ds.centre_symetrie || ds.hauteur_relative || ds.base_relative || ds.sommet || ds.sommet_oppose || ds.cote || ds.cote_oppose || ds.angle || ds.bissectrice || ds.mediane || ds.hauteur || ds.mediatrice || ds.longueu_egaler || ds.axe)
    ds.TRIANGLE_ISOCELE = ds.TRIANGLE_ISOCELE && (ds.hauteur || ds.mediatrice || ds.hauteur_relative || ds.base_relative || ds.centre_symetrie || ds.angle_egal || ds.sommet || ds.sommet_oppose || ds.cote || ds.cote_oppose || ds.angle || ds.angle_base || ds.sommet_principal || ds.longueur || ds.axe)
    ds.TRIANGLE_EQUILATERAL = ds.TRIANGLE_EQUILATERAL && (ds.hauteur || ds.mediatrice || ds.hauteur_relative || ds.base_relative || ds.centre_symetrie || ds.angle_egal || ds.sommet || ds.sommet_oppose || ds.cote || ds.cote_oppose || ds.angle || ds.longueur_egale || ds.axe)
    ds.TRIANGLE_RECTANGLE = ds.TRIANGLE_RECTANGLE && (ds.centre_symetrie || ds.angle_droit || ds.hauteur_relative || ds.base_relative || ds.sommet || ds.sommet_oppose || ds.cote || ds.cote_oppose || ds.angle || ds.bissectrice || ds.mediane || ds.hauteur || ds.mediatrice || ds.longueur_egale || ds.cote_adjacent || ds.cote_oppose_angle || ds.sommet_angle_droit || ds.hypotenuse || ds.axe || ds.cote_perpendiculaire)

    ds.QUADRILATERE = ds.QUADRILATERE && (ds.centre_symetrie || ds.angle_consecutif || ds.angle_oppose || ds.sommet || ds.sommet_oppose || ds.sommet_consecutif || ds.cote || ds.cote_oppose || ds.cote_consecutif || ds.angle || ds.angle_oppose || ds.angle_consecutif || ds.longueur_egale || ds.axe || ds.diagonale)
    ds.RECTANGLE = ds.RECTANGLE && (ds.centre_symetrie || ds.angle_egal || ds.angle_droit || ds.angle_consecutif || ds.angle_oppose || ds.sommet || ds.sommet_oppose || ds.sommet_consecutif || ds.cote || ds.cote_oppose || ds.cote_consecutif || ds.angle || ds.angle_oppose || ds.angle_consecutif || ds.longueur_egale || ds.axe || ds.cote_parallele || ds.cote_perpendiculaire || ds.diagonale)
    ds.CARRE = ds.CARRE && (ds.centre_symetrie || ds.angle_egal || ds.angle_droit || ds.angle_consecutif || ds.angle_oppose || ds.sommet || ds.sommet_oppose || ds.sommet_consecutif || ds.cote || ds.cote_oppose || ds.cote_consecutif || ds.angle || ds.angle_oppose || ds.angle_consecutif || ds.longueur_egale || ds.axe || ds.cote_parallele || ds.cote_perpendiculaire || ds.diagonale)
    ds.LOSANGE = ds.LOSANGE && (ds.centre_symetrie || ds.angle_egal || ds.angle_consecutif || ds.angle_oppose || ds.sommet || ds.sommet_oppose || ds.sommet_consecutif || ds.cote || ds.cote_oppose || ds.cote_consecutif || ds.angle || ds.angle_oppose || ds.angle_consecutif || ds.longueur_egale || ds.axe || ds.cote_parallele || ds.diagonale)
    ds.PARALLELOGRAMME = ds.PARALLELOGRAMME && (ds.centre_symetrie || ds.angle_egal || ds.angle_consecutif || ds.angle_oppose || ds.sommet || ds.sommet_oppose || ds.sommet_consecutif || ds.cote || ds.cote_oppose || ds.cote_consecutif || ds.angle || ds.angle_oppose || ds.angle_consecutif || ds.longueur_egale || ds.hauteur || ds.axe || ds.cote_parallele || ds.diagonale)

    ds.CERCLE = ds.CERCLE && (ds.centre_symetrie || ds.rayon || ds.diametre || ds.corde || ds.le_diametre || ds.centre || ds.axe || ds.longueur_egale)
    ds.ARC = ds.ARC && (ds.centre_symetrie || ds.extremites || ds.centre || ds.axe || ds.rayon)
    ds.ANGLE = ds.ANGLE && (ds.centre_symetrie || ds.cote || ds.sommet || ds.bissectrice || ds.axe)
    ds.SECTEUR = ds.SECTEUR && (ds.centre_symetrie || ds.centre || ds.axe || ds.angle || ds.rayon)

    ds.CUBE = ds.CUBE && (ds.face_perpendiculaire || ds.face_parallele || ds.arete_orthogonale || ds.arete_perpendiculaire || ds.arete_parallele || ds.arete || ds.arete_cachee || ds.sommet || ds.face || ds.face_cachee || ds.longueur_egale)
    ds.PAVE_DROIT = ds.PAVE_DROIT && (ds.face_perpendiculaire || ds.face_parallele || ds.arete_orthogonale || ds.arete_perpendiculaire || ds.arete_parallele || ds.arete || ds.arete_cachee || ds.sommet || ds.face || ds.face_cachee || ds.longueur_egale)
    ds.PRISME = ds.PRISME && (ds.face_perpendiculaire || ds.face_parallele || ds.arete_orthogonale || ds.arete_perpendiculaire || ds.arete_parallele || ds.arete || ds.arete_cachee || ds.sommet || ds.face || ds.face_cachee || ds.longueur_egale || ds.base || ds.face_laterale || ds.arete_laterale || ds.hauteur)
    ds.CYLINDRE = ds.CYLINDRE && (ds.face_parallele || ds.rayon || ds.diametre || ds.le_diametre || ds.face || ds.face_cachee || ds.base || ds.face_laterale || ds.hauteur || ds.longueur_egale)
    ds.CYLINDRE_REVOLUTION = ds.CYLINDRE_REVOLUTION && (ds.face_parallele || ds.rayon || ds.diametre || ds.le_diametre || ds.face || ds.face_cachee || ds.base || ds.face_laterale || ds.hauteur || ds.longueur_egale)
    ds.PYRAMIDE = ds.PYRAMIDE && (ds.arete || ds.arete_cachee || ds.sommet || ds.face || ds.face_cachee || ds.longueur || ds.base || ds.face_laterale || ds.arete_laterale || ds.hauteur || ds.sommet_principal)
    ds.CONE = ds.CONE && (ds.rayon || ds.diametre || ds.le_diametre || ds.face || ds.face_cachee || ds.base || ds.face_laterale || ds.hauteur || ds.sommet_principal || ds.longueur_egale)
    ds.CONE_REVOLUTION = ds.CONE_REVOLUTION && (ds.rayon || ds.diametre || ds.le_diametre || ds.face || ds.face_cachee || ds.base || ds.face_laterale || ds.hauteur || ds.sommet_principal || ds.longueur_egale)
    ds.SPHERE = ds.SPHERE && (ds.rayon || ds.diametre || ds.le_diametre || ds.centre || ds.pole || ds.meridien || ds.grand_cercle || ds.equateur || ds.parallele || ds.longueur_egale)

    if (ds.SEGMENT) {
      figplanes = true
      seg = true
      ds.ContenantPossible.push('SEGMENT')
    }
    if (ds.DEMI_DROITE) {
      figplanes = true
      demidroite = true
      ds.ContenantPossible.push('DEMI_DROITE')
    }
    if (ds.DROITE) {
      figplanes = true
      dte = true
      ds.ContenantPossible.push('DROITE')
    }
    if (ds.TRIANGLE) {
      figplanes = true
      triangles = true
      ds.ContenantPossible.push('TRIANGLE')
    }
    if (ds.TRIANGLE_ISOCELE) {
      figplanes = true
      triangles = true
      ds.ContenantPossible.push('TRIANGLE_ISOCELE')
    }
    if (ds.TRIANGLE_EQUILATERAL) {
      figplanes = true
      triangles = true
      ds.ContenantPossible.push('TRIANGLE_EQUILATERAL')
    }
    if (ds.TRIANGLE_RECTANGLE) {
      figplanes = true
      triangles = true
      ds.ContenantPossible.push('TRIANGLE_RECTANGLE')
    }
    if (ds.QUADRILATERE) {
      figplanes = true
      quad = true
      ds.ContenantPossible.push('QUADRILATERE')
    }
    if (ds.RECTANGLE) {
      figplanes = true
      quad = true
      ds.ContenantPossible.push('RECTANGLE')
    }
    if (ds.CARRE) {
      figplanes = true
      quad = true
      ds.ContenantPossible.push('CARRE')
    }
    if (ds.LOSANGE) {
      figplanes = true
      quad = true
      ds.ContenantPossible.push('LOSANGE')
    }
    if (ds.PARALLELOGRAMME) {
      figplanes = true
      quad = true
      ds.ContenantPossible.push('PARALLELOGRAMME')
    }
    if (ds.CERCLE) {
      figplanes = true
      cercle = true
      ds.ContenantPossible.push('CERCLE')
    }
    if (ds.ARC) {
      figplanes = true
      arc = true
      ds.ContenantPossible.push('ARC')
    }
    if (ds.SECTEUR) {
      figplanes = true
      secteur = true
      ds.ContenantPossible.push('SECTEUR')
    }
    if (ds.ANGLE) {
      figplanes = true
      angle = true
      ds.ContenantPossible.push('ANGLE')
    }
    if (ds.CUBE) {
      solid = true
      prisme = true
      ds.ContenantPossible.push('CUBE')
    }
    if (ds.PAVE_DROIT) {
      solid = true
      prisme = true
      ds.ContenantPossible.push('PAVE_DROIT')
    }
    if (ds.PRISME) {
      solid = true
      prisme = true
      ds.ContenantPossible.push('PRISME')
    }
    if (ds.CYLINDRE) {
      solid = true
      cylindre = true
      ds.ContenantPossible.push('CYLINDRE')
    }
    if (ds.CYLINDRE_REVOLUTION) {
      solid = true
      cylindre = true
      cylindrerevo = true
      ds.ContenantPossible.push('CYLINDRE_REVOLUTION')
    }
    if (ds.PYRAMIDE) {
      solid = true
      pyra = true
      ds.ContenantPossible.push('PYRAMIDE')
    }
    if (ds.CONE) {
      solid = true
      cone = true
      ds.ContenantPossible.push('CONE')
    }
    if (ds.CONE_REVOLUTION) {
      solid = true
      cone = true
      ds.ContenantPossible.push('CONE_REVOLUTION')
    }
    if (ds.SPHERE) {
      solid = true
      sphere = true
      ds.ContenantPossible.push('SPHERE')
    }

    ds.ElemPossible = []
    if (ds.hypotenuse) { if (ds.TRIANGLE_RECTANGLE) { ds.ElemPossible.push('hypotenuse') } }
    if (ds.milieu) { if (ds.SEGMENT) { ds.ElemPossible.push('milieu') } }
    if (ds.extremites) { if (ds.SEGMENT || ds.ARC) { ds.ElemPossible.push('extremites') } }
    if (ds.origine) { if (ds.DEMI_DROITE) { ds.ElemPossible.push('origine') } }
    if (ds.sommet) { if (pyra || prisme || ds.TRIANGLE || ds.TRIANGLE_RECTANGLE || ds.TRIANGLE_ISOCELE || ds.TRIANGLE_EQUILATERAL || ds.QUADRILATERE || ds.CARRE || ds.RECTANGLE || ds.LOSANGE || ds.PARALLELOGRAMME || ds.ANGLE) { ds.ElemPossible.push('sommet') } }
    if (ds.cote) { if (ds.TRIANGLE || ds.TRIANGLE_RECTANGLE || ds.TRIANGLE_ISOCELE || ds.TRIANGLE_EQUILATERAL || ds.QUADRILATERE || ds.CARRE || ds.RECTANGLE || ds.LOSANGE || ds.PARALLELOGRAMME || ds.ANGLE) { ds.ElemPossible.push('cote') } }
    if (ds.cote_parallele) { if (ds.CARRE || ds.RECTANGLE || ds.LOSANGE || ds.PARALLELOGRAMME || prisme) { ds.ElemPossible.push('cote_parallele') } }
    if (ds.cote_perpendiculaire) { if (ds.TRIANGLE_RECTANGLE || ds.CARRE || ds.RECTANGLE || prisme) { ds.ElemPossible.push('cote_perpendiculaire') } }
    if (ds.arete_parallele) { if (prisme) { ds.ElemPossible.push('arete_parallele') } }
    if (ds.arete_perpendiculaire) { if (prisme) { ds.ElemPossible.push('arete_perpendiculaire') } }
    if (ds.arete_orthogonale) { if (prisme) { ds.ElemPossible.push('arete_orthogonale') } }
    if (ds.face_parallele) { if (prisme || cylindre) { ds.ElemPossible.push('face_parallele') } }
    if (ds.face_perpendiculaire) { if (prisme) { ds.ElemPossible.push('face_perpendiculaire') } }
    if (ds.angle) { if (secteur || ds.TRIANGLE || ds.TRIANGLE_RECTANGLE || ds.TRIANGLE_ISOCELE || ds.TRIANGLE_EQUILATERAL || ds.QUADRILATERE || ds.CARRE || ds.RECTANGLE || ds.LOSANGE || ds.PARALLELOGRAMME) { ds.ElemPossible.push('angle') } }
    if (ds.angles_base) { if (ds.TRIANGLE_ISOCELE) { ds.ElemPossible.push('angles_base') } }
    if (ds.sommet_oppose) { if (ds.TRIANGLE || ds.TRIANGLE_RECTANGLE || ds.TRIANGLE_ISOCELE || ds.TRIANGLE_EQUILATERAL || ds.QUADRILATERE || ds.CARRE || ds.RECTANGLE || ds.LOSANGE || ds.PARALLELOGRAMME) { ds.ElemPossible.push('sommet_oppose') } }
    if (ds.angle_oppose) { if (ds.QUADRILATERE || ds.CARRE || ds.RECTANGLE || ds.LOSANGE || ds.PARALLELOGRAMME) { ds.ElemPossible.push('angle_oppose') } }
    if (ds.angle_consecutif) { if (ds.QUADRILATERE || ds.CARRE || ds.RECTANGLE || ds.LOSANGE || ds.PARALLELOGRAMME) { ds.ElemPossible.push('angle_consecutif') } }
    if (ds.cote_oppose) { if (ds.TRIANGLE || ds.TRIANGLE_RECTANGLE || ds.TRIANGLE_ISOCELE || ds.TRIANGLE_EQUILATERAL || ds.QUADRILATERE || ds.CARRE || ds.RECTANGLE || ds.LOSANGE || ds.PARALLELOGRAMME || ds.ANGLE) { ds.ElemPossible.push('cote_oppose') } }
    if (ds.cote_oppose_angle) { if (ds.TRIANGLE_RECTANGLE) { ds.ElemPossible.push('cote_oppose_angle') } }
    if (ds.cote_adjacent) { if (ds.TRIANGLE_RECTANGLE) { ds.ElemPossible.push('cote_adjacent') } }
    if (ds.hauteur) { if (cylindre || cone || pyra || ds.PRISME || ds.TRIANGLE || ds.TRIANGLE_RECTANGLE || ds.TRIANGLE_ISOCELE || ds.TRIANGLE_EQUILATERAL || ds.PARALLELOGRAMME) { ds.ElemPossible.push('hauteur') } }
    if (ds.base) { if (cylindre || cone || pyra || ds.PRISME || ds.TRIANGLE_ISOCELE) { ds.ElemPossible.push('base') } }
    if (ds.base_relative) { if (triangles || ds.PARALLELOGRAMME) { ds.ElemPossible.push('base_relative') } }

    if (ds.hauteur_relative) { if (ds.TRIANGLE || ds.TRIANGLE_RECTANGLE || ds.TRIANGLE_ISOCELE || ds.TRIANGLE_EQUILATERAL || ds.LOSANGE || ds.PARALLELOGRAMME) { ds.ElemPossible.push('hauteur_relative') } }
    if (ds.axe) { if (figplanes) { ds.ElemPossible.push('axe') } }
    if (ds.longueur_egale) { if (prisme || cylindrerevo || ds.TRIANGLE_ISOCELE || ds.TRIANGLE_EQUILATERAL || ds.CARRE || ds.RECTANGLE || ds.LOSANGE || ds.PARALLELOGRAMME || cone) { ds.ElemPossible.push('longueur_egale') } }
    if (ds.sommet_principal) { if (cone || pyra || ds.TRIANGLE_ISOCELE) { ds.ElemPossible.push('sommet_principal') } }
    if (ds.angle_droit) { if (ds.TRIANGLE_RECTANGLE || ds.CARRE || ds.RECTANGLE) { ds.ElemPossible.push('angle_droit') } }
    if (ds.angle_egal) { if (ds.CARRE || ds.RECTANGLE || ds.PARALLELOGRAMME || ds.LOSANGE || ds.TRIANGLE_EQUILATERAL || ds.TRIANGLE_ISOCELE) { ds.ElemPossible.push('angle_egal') } }
    if (ds.sommet_angle_droit) { if (ds.TRIANGLE_RECTANGLE) { ds.ElemPossible.push('sommet_angle_droit') } }
    if (ds.sommet_consecutif) { if (ds.QUADRILATERE || ds.CARRE || ds.RECTANGLE || ds.LOSANGE || ds.PARALLELOGRAMME) { ds.ElemPossible.push('sommet_consecutif') } }
    if (ds.cote_consecutif) { if (ds.QUADRILATERE || ds.CARRE || ds.RECTANGLE || ds.LOSANGE || ds.PARALLELOGRAMME) { ds.ElemPossible.push('cote_consecutif') } }
    if (ds.diagonale) { if (ds.QUADRILATERE || ds.CARRE || ds.RECTANGLE || ds.LOSANGE || ds.PARALLELOGRAMME) { ds.ElemPossible.push('diagonale') } }
    if (ds.centre_symetrie) { if (figplanes) { ds.ElemPossible.push('centre_symetrie') } }
    if (ds.mediatrice) { if (ds.SEGMENT || triangles) { ds.ElemPossible.push('mediatrice') } }
    if (ds.centre) { if (secteur || cone || ds.CARRE || ds.RECTANGLE || ds.LOSANGE || ds.PARALLELOGRAMME || ds.CERCLE || ds.ARC || ds.SPHERE) { ds.ElemPossible.push('centre') } }
    if (ds.face) { if (solid) { ds.ElemPossible.push('face') } }
    if (ds.bissectrice) { if (ds.ANGLE) { ds.ElemPossible.push('bissectrice') } }
    if (ds.arete) { if (prisme || pyra) { ds.ElemPossible.push('arete') } }
    if (ds.face_cachee) { if (pyra || prisme) { ds.ElemPossible.push('face_cachee') } }
    if (ds.arete_cachee) { if (prisme || pyra) { ds.ElemPossible.push('arete_cachee') } }
    if (ds.arete_laterale) { if (prisme || cylindre || pyra || cone) { ds.ElemPossible.push('arete_laterale') } }
    if (ds.face_laterale) { if (prisme || cylindre || pyra || cone) { ds.ElemPossible.push('face_laterale') } }
    if (ds.rayon) { if (secteur || ds.CERCLE || cylindre || cone || sphere || ds.ARC) { ds.ElemPossible.push('rayon') } }
    if (ds.diametre) { if (ds.CERCLE || cylindre || cone || sphere) { ds.ElemPossible.push('diametre') } }
    if (ds.corde) { if (ds.CERCLE) { ds.ElemPossible.push('corde') } }
    if (ds.meridien) { if (sphere) { ds.ElemPossible.push('meridien') } }
    if (ds.equateur) { if (sphere) { ds.ElemPossible.push('equateur') } }
    if (ds.parallele) { if (sphere) { ds.ElemPossible.push('parallele') } }
    if (ds.grand_cercle) { if (sphere) { ds.ElemPossible.push('grand_cercle') } }
    if (ds.generatrice) { if (ds.CONE_REVOLUTION) { ds.ElemPossible.push('generatrice') } }
    if (ds.pole) { if (ds.SPHERE) { ds.ElemPossible.push('pole') } }

    if (ds.ElemPossible.length === 0) {
      j3pShowError('Les paramètres choisis ne permettent pas de construire l’exercice !')
      return
    }

    const letitredeb = 'Element '
    let letitrefin = 'd’une figure'
    if (!(solid && figplanes)) {
      if (!solid) {
        letitrefin = 'd’une figure plane'
        if ((!secteur) && seg && (!demidroite) && (!triangles) && (!quad) && (!cercle) && (!arc) && (!angle) && (!dte)) { letitrefin = 'd’un segment' }
        if (secteur && (!seg) && (!demidroite) && (!triangles) && (!quad) && (!cercle) && (!arc) && (!angle) && (!dte)) { letitrefin = 'd’un secteur circulaire' }
        if ((!secteur) && (!seg) && (demidroite) && (!triangles) && (!quad) && (!cercle) && (!arc) && (!angle) && (!dte)) { letitrefin = 'd’une demi-droite' }
        if ((!secteur) && (!seg) && (demidroite) && (!triangles) && (!quad) && (!cercle) && (!arc) && (!angle) && (!dte)) { letitrefin = 'd’une demi-droite' }
        if ((!secteur) && (!seg) && (demidroite) && (!triangles) && (!quad) && (!cercle) && (!arc) && (!angle) && (dte)) { letitrefin = 'd’une droite' }
        if ((!secteur) && (!seg) && (!demidroite) && (!triangles) && (!quad) && (cercle) && (!arc) && (!angle) && (!dte)) { letitrefin = 'd’un cercle' }
        if ((!secteur) && (!seg) && (!demidroite) && (!triangles) && (!quad) && (!cercle) && (arc) && (!angle) && (!dte)) { letitrefin = 'd’un arc de cercle' }
        if ((!secteur) && (!seg) && (!demidroite) && (!triangles) && (!quad) && (!cercle) && (!arc) && (angle) && (!dte)) { letitrefin = 'd’un angle' }
        if ((!secteur) && (!seg) && (!demidroite) && (triangles) && (!quad) && (!cercle) && (!arc) && (!angle) && (!dte)) {
          letitrefin = 'd’un triangle'
          if (!ds.TRIANGLE) {
            letitrefin = 'd’un triangle particulier'
            if ((ds.TRIANGLE_ISOCELE) && (!ds.TRIANGLE_EQUILATERAL) && (!ds.TRIANGLE_RECTANGLE)) { letitrefin = 'd’un triangle isocèle' }
            if ((!ds.TRIANGLE_ISOCELE) && (ds.TRIANGLE_EQUILATERAL) && (!ds.TRIANGLE_RECTANGLE)) { letitrefin = 'd’un triangle équilatéral' }
            if ((!ds.TRIANGLE_ISOCELE) && (!ds.TRIANGLE_EQUILATERAL) && (ds.TRIANGLE_RECTANGLE)) { letitrefin = 'd’un triangle rectangle' }
          }
        }
        if ((!secteur) && (!seg) && (!demidroite) && (!triangles) && (quad) && (!cercle) && (!arc) && (!angle) && (!dte)) {
          letitrefin = 'd’un quadrilatère'
          if (!ds.QUADRILATERE) {
            letitrefin = 'd’un quadrilatère particulier'
            if ((ds.CARRE) && (!ds.RECTANGLE) && (!ds.LOSANGE) && (!ds.PARALLELOGRAMME)) { letitrefin = 'd’un carré' }
            if ((!ds.CARRE) && (ds.RECTANGLE) && (!ds.LOSANGE) && (!ds.PARALLELOGRAMME)) { letitrefin = 'd’un rectangle' }
            if ((!ds.CARRE) && (!ds.RECTANGLE) && (ds.LOSANGE) && (!ds.PARALLELOGRAMME)) { letitrefin = 'd’un losange' }
            if ((!ds.CARRE) && (!ds.RECTANGLE) && (!ds.LOSANGE) && (ds.PARALLELOGRAMME)) { letitrefin = 'd’un parallélogramme' }
          }
        }
      } else {
        letitrefin = 'd’un solide'
        if (prisme && (!pyra) && (!cylindre) && (!cone) && (!sphere)) {
          letitrefin = 'd’un prisme'
          if ((ds.CUBE) && (!ds.PAVE_DROIT) && (!ds.PRISME)) { letitrefin = 'd’un cube' }
          if ((ds.PAVE_DROIT) && (!ds.PRISME)) { letitrefin = 'd’un pavé droit' }
        }
        if ((!prisme) && (pyra) && (!cylindre) && (!cone) && (!sphere)) { letitrefin = 'd’une pyramide' }
        if ((!prisme) && (!pyra) && (cylindre) && (!cone) && (!sphere)) {
          letitrefin = 'd’un cylindre'
          if (!ds.CYLINDRE) { letitrefin = 'd’un cylindre de révolution' }
        }
        if ((!prisme) && (!pyra) && (!cylindre) && (cone) && (!sphere)) {
          letitrefin = 'd’un cône'
          if (!ds.CONE) { letitrefin = 'd’un cône de révolution' }
        }
        if ((!prisme) && (!pyra) && (!cylindre) && (!cone) && (sphere)) { letitrefin = 'd’une sphère' }
      }
    }

    me.typederreurs = [0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0]
    me.score = 0

    me.afficheTitre(letitredeb + letitrefin)

    if (ds.indication !== '') me.indication(me.zones.IG, ds.indication)

    stor.contenantsuse = []
    stor.elementuse = []
    stor.elemuse = []
    getMtgCore({ withMathJax: true })
      .then(
        // success
        (mtgAppLecteur) => {
          stor.mtgAppLecteur = mtgAppLecteur
          stor.listePourCalc = mtgAppLecteur.createList('TWF0aEdyYXBoSmF2YTEuMAAAABI+TMzNAAJmcv###wEA#wEAAAAAAAAAAAQsAAACvQAAAQEAAAAAAAAAAQAAAAP#####AAAAAQAKQ0NhbGNDb25zdAD#####AAJwaQAWMy4xNDE1OTI2NTM1ODk3OTMyMzg0Nv####8AAAABAApDQ29uc3RhbnRlQAkh+1RELRj#####AAAAAQAHQ0NhbGN1bAD#####AAFlAAZleHAoMSn#####AAAAAgAJQ0ZvbmN0aW9uBwAAAAE#8AAAAAAAAAAAAAIA#####wABeAABMAAAAAEAAAAAAAAAAP###############w==')
          enonceMain()
        },
        // failure
        (error) => {
          j3pShowError(error, { message: 'mathgraph n’est pas correctement chargé', mustNotify: true })
        })
      // plantage dans le code de success
      .catch(j3pShowError)
  }

  function creelesdiv () {
    stor.lesdiv = {}
    stor.lesdiv.conteneur = j3pAddElt(me.zones.MG, 'div', { style: me.styles.etendre('toutpetit.enonce', { padding: '10px' }) })
    const tab1 = addDefaultTable(stor.lesdiv.conteneur, 4, 1)
    stor.lesdiv.consigne = tab1[0][0]
    const tab2 = addDefaultTable(tab1[1][0], 1, 3)
    tab2[0][1].style.width = '20px'
    stor.lesdiv.figure = tab2[0][0]
    const tab3 = addDefaultTable(tab2[0][2], 5, 1)
    tab3[1][0].style.height = '20px'
    tab3[3][0].style.height = '20px'
    stor.lesdiv.explication = tab3[2][0]
    stor.lesdiv.correction = tab3[0][0]
    stor.lesdiv.correctionbis = tab1[2][0]
    stor.lesdiv.solution = tab3[4][0]
    stor.lesdiv.consigne.classList.add('enonce')
    stor.lesdiv.figure.classList.add('travail')
    me.zonesElts.MG.classList.add('fond')
    stor.lesdiv.solution.style.color = me.styles.toutpetit.correction.color
    stor.lesdiv.explication.style.color = me.styles.cfaux
    stor.lesdiv.correctionbis.style.color = me.styles.cfaux
  }

  function enonceMain () {
    me.videLesZones()

    // ou dans le cas de presentation3
    me.ajouteBoutons()

    creelesdiv()
    // selectionne contenant
    let cptuse = 0
    let oksort = false
    do {
      stor.contenant = ds.ContenantPossible[j3pGetRandomInt(0, (ds.ContenantPossible.length - 1))]
      cptuse++
      oksort = (stor.contenantsuse.indexOf(stor.contenant) === -1)
    } while ((!oksort) && (cptuse < 50))

    stor.contenantsuse.push(stor.contenant)
    // selectionne element
    const elempo = []
    const listecontenant = ['SEGMENT', 'DEMI_DROITE', 'TRIANGLE', 'TRIANGLE_ISOCELE', 'TRIANGLE_EQUILATERAL', 'TRIANGLE_RECTANGLE', 'QUADRILATERE', 'RECTANGLE', 'CARRE', 'LOSANGE', 'PARALLELOGRAMME', 'CERCLE', 'ARC', 'SECTEUR', 'ANGLE', 'CUBE', 'PAVE_DROIT', 'PRISME', 'CYLINDRE', 'CYLINDRE_REVOLUTION', 'PYRAMIDE', 'CONE', 'CONE_REVOLUTION', 'SPHERE', 'DROITE']
    const listeelemepo = []
    // segment
    listeelemepo[0] = ['extremites', 'milieu', 'centre_symetrie', 'axe', 'mediatrice']
    // demidroite
    listeelemepo[1] = ['centre_symetrie', 'origine', 'axe']
    // triangle
    listeelemepo[2] = ['centre_symetrie', 'mediatrice', 'axe', 'sommet', 'cote', 'angle', 'sommet_oppose', 'cote_oppose', 'hauteur', 'hauteur_relative', 'base_relative']
    // triangle isocele
    listeelemepo[3] = ['centre_symetrie', 'mediatrice', 'angle_egal', 'sommet', 'cote', 'angle', 'sommet_oppose', 'cote_oppose', 'hauteur', 'base', 'hauteur_relative', 'base_relative', 'sommet_principal', 'longueur_egale', 'angles_base', 'axe']
    // triangle equilateral
    listeelemepo[4] = ['centre_symetrie', 'mediatrice', 'angle_egal', 'sommet', 'cote', 'angle', 'sommet_oppose', 'cote_oppose', 'hauteur', 'hauteur_relative', 'longueur_egale', 'base_relative', 'axe']
    // triangle rectangle
    listeelemepo[5] = ['centre_symetrie', 'mediatrice', 'axe', 'cote_perpendiculaire', 'sommet', 'cote', 'angle', 'sommet_oppose', 'cote_oppose', 'hauteur', 'hauteur_relative', 'base_relative', 'hypotenuse', 'angle_droit', 'sommet_angle_droit', 'cote_oppose_angle', 'cote_adjacent']
    // quadrilatere
    listeelemepo[6] = ['centre_symetrie', 'sommet', 'axe', 'cote', 'angle', 'sommet_oppose', 'cote_oppose', 'angle_oppose', 'angle_consecutif', 'sommet_consecutif', 'cote_consecutif', 'diagonale']
    // rectangle
    listeelemepo[7] = ['angle_egal', 'cote_parallele', 'cote_perpendiculaire', 'sommet', 'cote', 'angle', 'sommet_oppose', 'cote_oppose', 'angle_oppose', 'angle_consecutif', 'sommet_consecutif', 'cote_consecutif', 'diagonale', 'angle_droit', 'axe', 'longueur_egale', 'centre_symetrie']
    // carré
    listeelemepo[8] = ['angle_egal', 'cote_parallele', 'cote_perpendiculaire', 'sommet', 'cote', 'angle', 'sommet_oppose', 'cote_oppose', 'angle_oppose', 'angle_consecutif', 'sommet_consecutif', 'cote_consecutif', 'diagonale', 'angle_droit', 'axe', 'longueur_egale', 'centre_symetrie']
    // losange
    listeelemepo[9] = ['angle_egal', 'cote_parallele', 'sommet', 'cote', 'angle', 'sommet_oppose', 'cote_oppose', 'angle_oppose', 'angle_consecutif', 'sommet_consecutif', 'cote_consecutif', 'diagonale', 'axe', 'longueur_egale', 'centre_symetrie']
    // parallélogramme
    listeelemepo[10] = ['angle_egal', 'cote_parallele', 'sommet', 'cote', 'angle', 'sommet_oppose', 'cote_oppose', 'angle_oppose', 'angle_consecutif', 'sommet_consecutif', 'cote_consecutif', 'diagonale', 'axe', 'longueur_egale', 'centre_symetrie', 'hauteur', 'hauteur_relative', 'base_relative']
    // cercle
    listeelemepo[11] = ['longueur_egale', 'centre', 'rayon', 'corde', 'diametre', 'axe', 'centre_symetrie']
    // arc
    listeelemepo[12] = ['centre_symetrie', 'extremites', 'axe', 'centre']
    // secteur
    listeelemepo[13] = ['centre_symetrie', 'rayon', 'axe', 'angle', 'centre']
    // angle
    listeelemepo[14] = ['centre_symetrie', 'sommet', 'cote', 'axe', 'bissectrice']
    // cube
    listeelemepo[15] = ['longueur_egale', 'face_cachee', 'arete_cachee', 'face_parallele', 'face_perpendiculaire', 'arete_parallele', 'arete_perpendiculaire', 'arete_orthogonale', 'sommet', 'arete', 'face']
    // pave droit
    listeelemepo[16] = ['longueur_egale', 'face_cachee', 'arete_cachee', 'face_parallele', 'face_perpendiculaire', 'arete_parallele', 'arete_perpendiculaire', 'arete_orthogonale', 'sommet', 'arete', 'face']
    // prisme
    listeelemepo[17] = ['longueur_egale', 'face_cachee', 'arete_cachee', 'face_parallele', 'face_perpendiculaire', 'arete_parallele', 'arete_perpendiculaire', 'arete_orthogonale', 'sommet', 'arete', 'face', 'face_laterale', 'hauteur', 'base']
    // cylindre
    listeelemepo[18] = ['longueur_egale', 'face_parallele', 'face', 'face_laterale', 'hauteur', 'base', 'rayon', 'diametre']
    // cylindre revo
    listeelemepo[19] = ['longueur_egale', 'face_parallele', 'face', 'face_laterale', 'hauteur', 'base', 'rayon', 'diametre']
    // pyramide
    listeelemepo[20] = ['face_cachee', 'arete_cachee', 'arete', 'face', 'face_laterale', 'arete_laterale', 'hauteur', 'base', 'sommet', 'sommet_principal']
    // cone
    listeelemepo[21] = ['longueur_egale', 'centre', 'face', 'face_laterale', 'hauteur', 'base', 'sommet_principal', 'rayon', 'diametre']
    // cone de revo
    listeelemepo[22] = ['generatrice', 'longueur_egale', 'centre', 'face', 'face_laterale', 'hauteur', 'base', 'sommet_principal', 'rayon', 'diametre']
    // sphere
    listeelemepo[23] = ['pole', 'longueur_egale', 'centre', 'rayon', 'corde', 'diametre', 'meridien', 'parallele', 'grand_cercle', 'equateur']
    // droite
    listeelemepo[24] = ['axe', 'centre_symetrie']

    const contenantnum = listecontenant.indexOf(stor.contenant)
    for (let i = 0; i < ds.ElemPossible.length; i++) {
      if (listeelemepo[contenantnum].indexOf(ds.ElemPossible[i]) !== -1) {
        elempo.push(ds.ElemPossible[i])
      }
    }
    if (elempo.length === 0) {
      elempo.push(listeelemepo[contenantnum][0])
    }
    cptuse = 0
    oksort = false
    do {
      stor.element = elempo[j3pGetRandomInt(0, (elempo.length - 1))]
      cptuse++
      oksort = (stor.elementuse.indexOf(stor.element) === -1)
    } while ((!oksort) && (cptuse < 50))

    stor.elementuse.push(stor.element)

    // stor.element = 'base_relative'
    stor.elemType = elemetype(stor.element)
    if (stor.elemType === undefined) { console.error('undefined elem') }

    stor.fig = new FigNomme02(me)

    stor.elementDef = tradTout(stor.element)

    let dedans = ds.textes.ContDef[contenantnum]
    if (stor.elementDef === 'les pôles') dedans = 'de la sphère terrestre'
    if (stor.elementDef === 'l’équateur') dedans = 'de la sphère terrestre'
    if (stor.elementDef === 'tous les méridiens tracés') dedans = 'de la sphère terrestre'
    let maquest = ds.textes.Clic
    maquest += stor.elementDef
    maquest += ds.textes.NomContenant.replace('£a', dedans + ' ' + stor.LeNomDuTruc)

    j3pAffiche(stor.lesdiv.consigne, null, '<b>' + maquest + '</b>')
    me.finEnonce()
  }

  switch (me.etat) {
    case 'enonce':
      // code exécuté au lancement de la section
      if (me.debutDeLaSection) {
        initSection()
      } else {
        // A chaque répétition, on nettoie la scène
        // A commenter si besoin
        enonceMain()
      }
      break // case "enonce":

    case 'correction':
      // On teste si une réponse a été saisie

      if (!yareponse()) {
        // ici on donne le message à afficher si une ou plusieurs zone(s) n’est pas complétée (tout d’abord on met en rouge : styles.cfaux)
        stor.lesdiv.correction.style.color = me.styles.cfaux
        stor.lesdiv.correction.innerHTML = reponseIncomplete
        me.afficheBoutonValider()
      } else { // Une réponse a été saisie
        let yaaide = false
        // Bonne réponse
        if (bonneReponse()) {
          me._stopTimer()
          me.score += j3pArrondi(1 / ds.nbetapes, 1)
          stor.lesdiv.correction.style.color = me.styles.cbien
          stor.lesdiv.correction.innerHTML = cBien

          j3pAffiche(stor.lesdiv.solution, null, me.phraseco)

          me.typederreurs[0]++
          me.cacheBoutonValider()
          me.etat = 'navigation'
          me.sectionCourante()
        } else {
          // Pas de bonne réponse
          stor.lesdiv.correction.style.color = me.styles.cfaux

          // A cause de la limite de temps :
          if (me.isElapsed) { // limite de temps
            me._stopTimer()

            j3pAffiche(stor.lesdiv.solution, null, me.phraseco)

            /// /dit si elem manque
            if (stor.ErrManque) {
              yaaide = true
              j3pAffiche(stor.lesdiv.explication, null, 'Tu n’as pas tout sélectionné !<i> \n(le(s) élément(s) manquant(s) sont indiqués en bleu.)</i> \n\n')
            }

            // dit si trop
            if (stor.ErrTrop) {
              yaaide = true
              j3pAffiche(stor.lesdiv.explication, null, 'Tu as sélectionné trop d’éléments ! \n<i>(le(s) élément(s) en trop sont indiqués en marron.)</i> \n')
            }
            for (let i = 0; i < stor.ListeTrop.length; i++) {
              if (stor.ListeTrop[i].indexOf(stor.elemType) === -1) {
                me.typederreurs[3]++
              }
            }
            /// /

            me.etat = 'navigation'
            me.sectionCourante()
          } else { // Réponse fausse :
            stor.lesdiv.correction.innerHTML = cFaux

            if (me.essaiCourant < ds.nbchances) {
              stor.lesdiv.correction.innerHTML += '<br>' + essaieEncore
              me.afficheBoutonValider()
              me.typederreurs[1]++
              // indication éventuelle ici
            } else { // Erreur au nème essai
              me._stopTimer()
              me.cacheBoutonValider()
              stor.lesdiv.correction.innerHTML += '<br>' + regardeCorrection

              j3pAffiche(stor.lesdiv.solution, null, me.phraseco)

              /// /dit si elem manque
              if (stor.ErrManque) {
                yaaide = true
                j3pAffiche(stor.lesdiv.explication, null, 'Tu n’as pas tout sélectionné !<i> \n(le(s) élément(s) manquant(s) sont indiqués en bleu.)</i> \n\n')
              }

              // dit si trop
              if (stor.ErrTrop) {
                yaaide = true
                j3pAffiche(stor.lesdiv.explication, null, 'Tu as sélectionné trop d’éléments ! \n<i>(le(s) élément(s) en trop sont indiqués en marron.)</i> \n')
              }
              for (let i = 0; i < stor.ListeTrop.length; i++) {
                if (stor.ListeTrop[i].indexOf(stor.elemType) === -1) {
                  me.typederreurs[3]++
                }
              }

              me.typederreurs[2]++
              me.etat = 'navigation'
              me.sectionCourante()
            }
          }
        }

        if (ds.theme === 'zonesAvecImageDeFond') {
          stor.lesdiv.solution.classList.add('correction')
          if (yaaide) {
            stor.lesdiv.explication.classList.add('explique')
          }
        }
      }
      // Obligatoire
      me.finCorrection()
      break // case "correction":

    case 'navigation':

      if (me.sectionTerminee()) {
        // On détermine la phrase d’état renvoyée par la section
        const sco = me.score / ds.nbitems
        let peibase = 0
        if (sco >= 0.8) { peibase = 0 }
        if ((sco >= 0.55) && (sco < 0.8)) { peibase = 1 }
        if ((sco >= 0.3) && (sco < 0.55)) { peibase = 3 }
        if (sco < 0.3) { peibase = 5 }
        let peisup = 0
        let compt = me.typederreurs[2] / 4
        if (me.typederreurs[3] > compt) {
          peisup = 1
          compt = me.typederreurs[3]
        }
        me.parcours.pe = ds.pe[peibase + peisup]

        me.afficheBoutonSectionSuivante()
        me.focus('sectioncontinuer')
      } else {
        me.etat = 'enonce'
        me.afficheBoutonSuite()
        me.focus('boutoncontinuer')
      }
      // Obligatoire
      me.finNavigation()

      break // case "navigation":
  }
}
