import { j3pAddElt, j3pAjouteBouton, j3pClone, j3pEmpty, j3pGetNewId, j3pGetRandomBool, j3pGetRandomInt, j3pShowError, j3pShuffle } from 'src/legacy/core/functions'
import { j3pAffiche } from 'src/lib/mathquill/functions'
import { j3pCreeSVG } from 'src/legacy/core/functionsSvg'
import { getMtgCore } from 'src/lib/outils/mathgraph'
import { addDefaultTable } from 'src/legacy/themes/table'
import textesGeneriques from 'src/lib/core/textes'

const { cBien, cFaux, essaieEncore, regardeCorrection, reponseIncomplete, tempsDepasse } = textesGeneriques

export const params = {
  outils: ['mathquill'],
  /**
   * Ce qui est paramétrable dans l’éditeur de graphe
   * @type {LegacySectionParamsParametres}
   */
  parametres: [
    ['nbrepetitions', 8, 'entier', 'Nombre de répétitions de la section'],
    ['limite', 0, 'entier', 'Temps disponible par répétition, en secondes'],
    ['min', 2, 'entier', 'Nombre minimum de droites (2 min)'],
    ['max', 5, 'entier', 'Nombre maximum de droites (5 max)'],
    ['progressif', true, 'boolean', '<u>true</u>: Le nombre de droites augmente au cours de l’exercice. <BR><BR> <u>false</u>: Nombre de droites aléatoire (entre min et max).'],
    ['NomDesDroites', 'les deux', 'liste', '<u>(dx)</u>: Les droites s’appellent (d1) , (d2) , ... <BR><BR> <u>(AB)</u>: Les droites s’appellent (AB) , (BC) , ... <BR><BR> <u>les deux</u>: Les deux types de noms sont possibles.', ['(dx)', '(AB)', 'les deux']],
    ['Aide', false, 'boolean', '<u>true</u>: L’élève peut demander la colorisation des deux droites.'],
    // le modèle gère la compatibilité ascendante avec l’ancien paramètre MiseEnPagePlusClaire
    ['theme', 'standard', 'liste', 'Modèle pour l’affichage', ['standard', 'zonesAvecImageDeFond']]
  ]
}

/**
 * section point02
 * @this {Parcours}
 */
/**
 * section point02
 * @this {Parcours}
 */
export default function main () {
  const me = this
  const stor = me.storage
  let ds = me.donneesSection
  let svgId = stor.svgId

  function figureSelPrecis () {
    svgId = j3pGetNewId()
    stor.svgId = svgId

    const lll = j3pCreeSVG(stor.zoneTravail, { id: svgId, width: 480, height: 450 })
    const txtFigure = 'TWF0aEdyYXBoSmF2YTEuMAAAABM+TMzNAAJmcv###wEA#wEAAAAAAAAAAANLAAACfwAAAQEAAAAAAAAAAQAAAM######AAAAAQAKQ0NhbGNDb25zdAD#####AAJwaQAWMy4xNDE1OTI2NTM1ODk3OTMyMzg0Nv####8AAAABAApDQ29uc3RhbnRlQAkh+1RELRj#####AAAAAQAKQ1BvaW50QmFzZQD#####AQAAAAAOAAFVAMAkAAAAAAAAQBAAAAAAAAAAAAUAAEAv#nbItDlYQC#+dsi0OVj#####AAAAAQAUQ0Ryb2l0ZURpcmVjdGlvbkZpeGUA#####wEAAAAAEAAAAQAAAAEAAAABAT#wAAAAAAAA#####wAAAAEAD0NQb2ludExpZURyb2l0ZQD#####AQAAAAAOAAFWAMAAAAAAAAAAQBAAAAAAAAAAAAUAAUA##nbItDlYAAAAAv####8AAAABAAhDU2VnbWVudAD#####AQAAAAAQAAABAAAAAQAAAAEAAAAD#####wAAAAEAB0NNaWxpZXUA#####wEAAAAAEAAAAAAAAAAAAAAAQAgAAAAAAAAAAAUAAAAAAQAAAAP#####AAAAAgAMQ0NvbW1lbnRhaXJlAP####8BAAAAAAAAAAAAAAAAQBgAAAAAAAAAAAAAAAUMAAAAAAABAAAAAAAAAAEAAAAAAAAAAAABMf####8AAAABAAlDTG9uZ3VldXIA#####wAAAAEAAAADAAAAAgD#####Af8AAAAQAAAAAAAAAAAAAABACAAAAAAAAAAACAABQG0AAAAAAABAazwo9cKPXAAAAAIA#####wH#AAAAEAAAAAAAAAAAAAAAQAgAAAAAAAAAAAgAAUB4AAAAAAAAQHCeFHrhR67#####AAAAAQAJQ0NlcmNsZU9BAP####8BAAAAAAAAAQAAAAgAAAAJAAAAAgD#####Af8AAAAQAAAAAAAAAAAAAABACAAAAAAAAAAACAABQHrAAAAAAABAafwo9cKPXAAAAAIA#####wAAAAAAEAAAAEAIAAAAAAAAAAAAAAAAAAAAAAkAAEBSwAAAAAAAQH#eFHrhR67#####AAAAAQAHQ0NhbGN1bAD#####AAVtaW5pMQABMAAAAAEAAAAAAAAAAAAAAAoA#####wAFbWF4aTEAAzM1OQAAAAFAdnAAAAAAAP####8AAAABABRDSW1wbGVtZW50YXRpb25Qcm90bwD#####AAdDdXJzZXVyAAAABQAAAAUAAAADAAAADQAAAA4AAAAMAAAAAwAAAAAPAQAAAAAQAAABAAAAAQAAAAwBP#AAAAAAAAAAAAAEAQAAAA8AAAAAABAAAADACAAAAAAAAD#wAAAAAAAAAAAFAABAXUAAAAAAAAAAABD#####AAAAAQALQ0hvbW90aGV0aWUAAAAADwAAAAz#####AAAAAQAKQ09wZXJhdGlvbgP#####AAAAAQAPQ1Jlc3VsdGF0VmFsZXVyAAAADQAAAA0BAAAADgAAAA0AAAAOAAAADv####8AAAABAAtDUG9pbnRJbWFnZQAAAAAPAQAAAAANAAJPMQDAEAAAAAAAAEAQAAAAAAAAAAAFAAAAABEAAAASAAAADAAAAAAPAAAADAAAAA0DAAAADQEAAAABP#AAAAAAAAAAAAAOAAAADQAAAA0BAAAADgAAAA4AAAAOAAAADQAAAA8AAAAADwEAAAAADQACSTEAwAAAAAAAAABACAAAAAAAAAAABQAAAAARAAAAFAAAAAUBAAAADwAAAAAAEAAAAQAAAAEAAAAMAAAAEQAAAAQBAAAADwAAAAABEAACazEAwAAAAAAAAABAAAAAAAAAAAAAAQABP8tZtZtZtZsAAAAW#####wAAAAIAD0NNZXN1cmVBYnNjaXNzZQEAAAAPAAJhMQAAABMAAAAVAAAAF#####8AAAABAA9DVmFsZXVyQWZmaWNoZWUBAAAADwAAAAAAAAAAAAAAAADAGAAAAAAAAAAAAAAAFw8AAf###wAAAAEAAAACAAAAAQAAAAAAAAAAAAAAAAIAAAAYAAAACgD#####AAJBMQACYTEAAAAOAAAAGP####8AAAABAAlDUm90YXRpb24A#####wAAAAgAAAAOAAAAGgAAAA8A#####wEAAAAAEAAAAAAAAAAAAAAAQAgAAAAAAAAAAAkAAAAACQAAABsAAAASAP####8AAAAIAAAAAUBGgAAAAAAAAAAADwD#####AQAAAAAQAAAAAAAAAAAAAABACAAAAAAAAAACcEwJAAAAABwAAAAdAAAADwD#####AQAAAAAQAAAAAAAAAAAAAABACAAAAAAAAAAACQAAAAAeAAAAHQAAAA8A#####wEAAAAAEAAAAAAAAAAAAAAAQAgAAAAAAAAAAnBICQAAAAAfAAAAHQAAAA8A#####wEAAAAAEAAAAAAAAAAAAAAAQAgAAAAAAAAAAAkAAAAAIAAAAB0AAAAPAP####8BAAAAABAAAAAAAAAAAAAAAEAIAAAAAAAAAAJwQQkAAAAAIQAAAB0AAAAPAP####8BAAAAABAAAAAAAAAAAAAAAEAIAAAAAAAAAAJwRAkAAAAAIgAAAB0AAAAPAP####8BAAAAABAAAAAAAAAAAAAAAEAIAAAAAAAAAAJwRgkAAAAAIwAAAB3#####AAAAAQAJQ0Ryb2l0ZUFCAP####8BAAAAABAAAAEAAmQyAAIAAAAkAAAAHgAAABMA#####wEAAAAAEAAAAQACZDQAAgAAACQAAAAiAAAAEwD#####AQAAAAAQAAABAAJkNwACAAAAHgAAACIAAAATAP####8BAAAAABAAAAEAAmQxAAIAAAAjAAAAHwAAABMA#####wEAAAAAEAAAAQACZDYAAgAAACEAAAAjAAAAEwD#####AQAAAAAQAAABAAJkMwACAAAAIAAAABz#####AAAAAQAQQ0ludERyb2l0ZURyb2l0ZQD#####AQAAAAAQAAAAAAAAAAAAAABACAAAAAAAAAACcEUJAAAAACgAAAAmAAAAFAD#####AQAAAAAQAAAAAAAAAAAAAABACAAAAAAAAAACcEMJAAAAACYAAAApAAAAFAD#####AQAAAAAQAAAAAAAAAAAAAABACAAAAAAAAAACcEIJAAAAACcAAAApAAAAFAD#####AQAAAAAQAAAAAAAAAAAAAABACAAAAAAAAAACcEsJAAAAACUAAAAqAAAAFAD#####AQAAAAAQAAAAAAAAAAAAAABACAAAAAAAAAACcEoJAAAAACcAAAAqAAAAFAD#####AQAAAAAQAAAAAAAAAAAAAABACAAAAAAAAAACcEkJAAAAACgAAAAqAAAAFAD#####AQAAAAAQAAAAAAAAAAAAAABACAAAAAAAAAACcEcJAAAAACcAAAAoAAAACgD#####AAJBMgADMC41AAAAAT#gAAAAAAAA#####wAAAAIACUNDZXJjbGVPUgD#####AQAAAAACY0MAAgAAACwAAAAOAAAAMgAAAAAVAP####8BAAAAAAJjQgACAAAALQAAAA4AAAAyAAAAABUA#####wEAAAAAAmNBAAIAAAAiAAAADgAAADIAAAAAFQD#####AQAAAAACY0UAAgAAACsAAAAOAAAAMgAAAAAVAP####8BAAAAAAJjRAACAAAAIwAAAA4AAAAyAAAAABUA#####wEAAH8AAAABAAAAIQAAAA4AAAAyAAAAABUA#####wEAAAAAAmNIAAIAAAAgAAAADgAAADIAAAAAFQD#####AQAAAAACY0kAAgAAADAAAAAOAAAAMgAAAAAVAP####8BAAAAAAJjRwACAAAAMQAAAA4AAAAyAAAAABUA#####wEAAAAAAmNKAAIAAAAvAAAADgAAADIAAAAAFQD#####AQAAAAACY0sAAgAAAC4AAAAOAAAAMgAAAAAVAP####8BAAAAAAJjRgACAAAAJAAAAA4AAAAyAAAAABUA#####wEAAH8AAAABAAAAHAAAAA4AAAAyAAAAABUA#####wEAAAAAAmNMAAIAAAAeAAAADgAAADIAAAAAFQD#####AQAAfwAAAAEAAAAfAAAADgAAADIAAAAACQD#####AQB#AAAAAAEAAAAxAAAAC#####8AAAABABBDSW50RHJvaXRlQ2VyY2xlAP####8AAAAoAAAAQv####8AAAABABBDUG9pbnRMaWVCaXBvaW50AP####8BAH8AABAAAAAAAAAAAAAAAEAIAAAAAAAAAAAJAAIAAABDAAAAFwD#####AQB#AAAQAAAAAAAAAAAAAABACAAAAAAAAAAACQABAAAAQwAAABUA#####wEAfwAAAAABAAAARQAAAA4AAAAyAP####8AAAABABBDSW50Q2VyY2xlQ2VyY2xlAP####8AAABCAAAARgAAABcA#####wEAfwAAEAAAAAAAAAAAAAAAQAgAAAAAAAAAAAkAAQAAAEcAAAAXAP####8BAH8AABAAAAAAAAAAAAAAAEAIAAAAAAAAAAAJAAIAAABHAAAABwD#####AQAAAAEAA25kMQAAAEkQAAAAAAABAAAAAQAAAAEAAAAAAAAAAAAEKGQxKQAAABYA#####wAAACUAAABCAAAAFwD#####Af8AAAAQAAAAAAAAAAAAAABACAAAAAAAAAAACQACAAAASwAAABcA#####wH#AAAAEAAAAAAAAAAAAAAAQAgAAAAAAAAAAAkAAQAAAEsAAAAVAP####8B#wAAAAAAAQAAAE0AAAAOAAAAMgAAAAAYAP####8AAABCAAAATgAAABcA#####wH#AAAAEAAAAAAAAAAAAAAAQAgAAAAAAAAAAAkAAQAAAE8AAAAXAP####8B#wAAABAAAAAAAAAAAAAAAEAIAAAAAAAAAAAJAAIAAABPAAAABwD#####AQAAAAEAA25kMgAAAFEQAAAAAAABAAAAAQAAAAEAAAAAAAAAAAAEKGQyKQAAABYA#####wAAACoAAABCAAAAFwD#####AX8AfwAQAAAAAAAAAAAAAABACAAAAAAAAAAACQACAAAAUwAAABcA#####wF#AH8AEAAAAAAAAAAAAAAAQAgAAAAAAAAAAAkAAQAAAFMAAAAVAP####8BfwB#AAAAAQAAAFUAAAAOAAAAMgAAAAAYAP####8AAABCAAAAVgAAABcA#####wF#AH8AEAAAAAAAAAAAAAAAQAgAAAAAAAAAAAkAAQAAAFcAAAAXAP####8BfwB#ABAAAAAAAAAAAAAAAEAIAAAAAAAAAAAJAAIAAABXAAAABwD#####AQAAAAEAA25kMwAAAFgQAAAAAAABAAAAAQAAAAEAAAAAAAAAAAAEKGQzKQAAABYA#####wAAACYAAABCAAAAFwD#####Af8AAAAQAAAAAAAAAAAAAABACAAAAAAAAAAACQABAAAAWwAAABcA#####wH#AAAAEAAAAAAAAAAAAAAAQAgAAAAAAAAAAAkAAgAAAFsAAAAVAP####8B#wAAAAAABAAAAF0AAAAOAAAAMgAAAAAYAP####8AAABCAAAAXgAAABcA#####wH#AAAAEAAAAAAAAAAAAAAAQAgAAAAAAAAAAAkAAQAAAF8AAAAXAP####8B#wAAABAAAAAAAAAAAAAAAEAIAAAAAAAAAAAJAAIAAABfAAAABwD#####AQAAAAEAA25kNAAAAGEQAAAAAAABAAAAAQAAAAEAAAAAAAAAAAAEKGQ0KQAAABMA#####wEAAAAAEAAAAQACZDUAAgAAACAAAAAkAAAAFgD#####AAAAYwAAAEIAAAAXAP####8B#wAAABAAAAAAAAAAAAAAAEAIAAAAAAAAAAAJAAEAAABkAAAAFwD#####Af8AAAAQAAAAAAAAAAAAAABACAAAAAAAAAAACQACAAAAZAAAABUA#####wH#AAAAAAADAAAAZgAAAA4AAAAyAAAAABgA#####wAAAEIAAABnAAAAFwD#####Af8AAAAQAAAAAAAAAAAAAABACAAAAAAAAAAACQACAAAAaAAAABcA#####wH#AAAAEAAAAAAAAAAAAAAAQAgAAAAAAAAAAAkAAQAAAGgAAAAHAP####8BAAAAAQADbmQ1AAAAahAAAAAAAAEAAAABAAAAAQAAAAAAAAAAAAQoZDUpAAAAFgD#####AAAAKQAAAEIAAAAXAP####8B#wAAABAAAAAAAAAAAAAAAEAIAAAAAAAAAAAJAAEAAABsAAAAFwD#####Af8AAAAQAAAAAAAAAAAAAABACAAAAAAAAAAACQACAAAAbAAAABUA#####wH#AAAAAAADAAAAbgAAAA4AAAAyAAAAABgA#####wAAAEIAAABvAAAAFwD#####Af8AAAAQAAAAAAAAAAAAAABACAAAAAAAAAAACQACAAAAcAAAABcA#####wH#AAAAEAAAAAAAAAAAAAAAQAgAAAAAAAAAAAkAAQAAAHAAAAAHAP####8BAAAAAQADbmQ2AAAAchAAAAAAAAEAAAABAAAAAQAAAAAAAAAAAAQoZDYpAAAAFgD#####AAAAJwAAAEIAAAAXAP####8B#wAAABAAAAAAAAAAAAAAAEAIAAAAAAAAAAAJAAIAAAB0AAAAFwD#####Af8AAAAQAAAAAAAAAAAAAABACAAAAAAAAAAACQABAAAAdAAAABUA#####wH#AAAAAAADAAAAdgAAAA4AAAAyAAAAABgA#####wAAAEIAAAB3AAAAFwD#####Af8AAAAQAAAAAAAAAAAAAABACAAAAAAAAAAACQABAAAAeAAAABcA#####wH#AAAAEAAAAAAAAAAAAAAAQAgAAAAAAAAAAAkAAgAAAHgAAAAHAP####8BAAAAAQADbmQ3AAAAehAAAAAAAAEAAAABAAAAAQAAAAAAAAAAAAQoZDcpAAAAGAD#####AAAACgAAADkAAAAXAP####8BAH8AABAAAAAAAAAAAAAAAEAIAAAAAAAAAAAJAAEAAAB8AAAAFwD#####AQB#AAAQAAAAAAAAAAAAAABACAAAAAAAAAAACQACAAAAfP####8AAAABAAxDQmlzc2VjdHJpY2UA#####wEAfwAAEAAAAQAAAAIAAAAkAAAAMQAAAC8AAAAWAP####8AAAB#AAAAOwAAABcA#####wEAfwAAEAAAAAAAAAAAAAAAQAgAAAAAAAAAAAkAAgAAAIAAAAAXAP####8BAH8AABAAAAAAAAAAAAAAAEAIAAAAAAAAAAAJAAEAAACAAAAAGQD#####AQB#AAAQAAABAAAAAgAAADEAAAAwAAAAIAAAABYA#####wAAAIMAAAA6AAAAFwD#####AQB#AAAQAAAAAAAAAAAAAABACAAAAAAAAAAACQACAAAAhAAAABcA#####wEAfwAAEAAAAAAAAAAAAAAAQAgAAAAAAAAAAAkAAQAAAIQAAAAZAP####8BAH8AABAAAAEAAAACAAAAMAAAAC8AAAAeAAAAGQD#####AQB#AAAQAAABAAAAAgAAADEAAAAtAAAAIQAAABkA#####wEAfwAAEAAAAQAAAAIAAAAxAAAAKwAAACQAAAAWAP####8AAACJAAAANgAAABcA#####wEAfwAAEAAAAAAAAAAAAAAAQAgAAAAAAAAAAAkAAgAAAIoAAAAXAP####8BAH8AABAAAAAAAAAAAAAAAEAIAAAAAAAAAAAJAAEAAACKAAAAFgD#####AAAAiAAAADQAAAAXAP####8BAH8AABAAAAAAAAAAAAAAAEAIAAAAAAAAAAAJAAIAAACNAAAAFwD#####AQB#AAAQAAAAAAAAAAAAAABACAAAAAAAAAAACQABAAAAjQAAABYA#####wAAAIcAAAA8AAAAFwD#####AQB#AAAQAAAAAAAAAAAAAABACAAAAAAAAAAACQACAAAAkAAAABcA#####wEAfwAAEAAAAAAAAAAAAAAAQAgAAAAAAAAAAAkAAQAAAJD#####AAAAAQAWQ0Ryb2l0ZVBlcnBlbmRpY3VsYWlyZQD#####AQB#AAAQAAABAAAAAgAAAB8AAAAoAAAAGgD#####AQB#AAAQAAABAAAAAgAAACEAAAApAAAAGgD#####AQB#AAAQAAABAAAAAgAAACMAAAApAAAAGgD#####AQB#AAAQAAABAAAAAgAAACQAAABjAAAAFgD#####AAAAlgAAAD4AAAAXAP####8BAH8AABAAAAAAAAAAAAAAAEAIAAAAAAAAAAAJAAIAAACXAAAAFwD#####AQB#AAAQAAAAAAAAAAAAAABACAAAAAAAAAAACQABAAAAlwAAABYA#####wAAAJUAAAA3AAAAFwD#####AQB#AAAQAAAAAAAAAAAAAABACAAAAAAAAAAACQACAAAAmgAAABcA#####wEAfwAAEAAAAAAAAAAAAAAAQAgAAAAAAAAAAAkAAQAAAJoAAAAWAP####8AAACUAAAAOAAAABcA#####wEAfwAAEAAAAAAAAAAAAAAAQAgAAAAAAAAAAAkAAgAAAJ0AAAAXAP####8BAH8AABAAAAAAAAAAAAAAAEAIAAAAAAAAAAAJAAEAAACdAAAAFgD#####AAAAkwAAAEEAAAAXAP####8BAH8AABAAAAAAAAAAAAAAAEAIAAAAAAAAAAAJAAIAAACgAAAAFwD#####AQB#AAAQAAAAAAAAAAAAAABACAAAAAAAAAAACQABAAAAoAAAABkA#####wEAfwAAEAAAAQAAAAIAAABcAAAAIgAAAC0AAAAWAP####8AAACjAAAANQAAABcA#####wEAfwAAEAAAAAAAAAAAAAAAQAgAAAAAAAAAAAkAAgAAAKQAAAAXAP####8BAH8AABAAAAAAAAAAAAAAAEAIAAAAAAAAAAAJAAEAAACkAAAAGQD#####AQB#AAAQAAABAAAAAgAAACIAAAAsAAAAIwAAABYA#####wAAAKcAAAAzAAAAFwD#####AQB#AAAQAAAAAAAAAAAAAABACAAAAAAAAAAACQACAAAAqAAAABcA#####wEAfwAAEAAAAAAAAAAAAAAAQAgAAAAAAAAAAAkAAQAAAKgAAAAZAP####8BAH8AABAAAAEAAAACAAAAVQAAAC4AAAAeAAAAFgD#####AAAAqwAAAD0AAAAXAP####8BAH8AABAAAAAAAAAAAAAAAEAIAAAAAAAAAAAJAAIAAACsAAAAFwD#####AQB#AAAQAAAAAAAAAAAAAABACAAAAAAAAAAACQABAAAArAAAABkA#####wEAfwAAEAAAAQAAAAIAAABNAAAAHgAAAC8AAAAWAP####8AAACvAAAAQAAAABcA#####wEAfwAAEAAAAAAAAAAAAAAAQAgAAAAAAAAAAAkAAgAAALAAAAAXAP####8BAH8AABAAAAAAAAAAAAAAAEAIAAAAAAAAAAAJAAEAAACwAAAABwD#####AQAAAAEAAm5BAAAAphAAAAAAAAEAAAABAAAAAQAAAAAAAAAAAAFBAAAABwD#####AQAAAAEAAm5CAAAAjxAAAAAAAAEAAAABAAAAAQAAAAAAAAAAAAFCAAAABwD#####AQAAAAEAAm5DAAAAqhAAAAAAAAEAAAABAAAAAQAAAAAAAAAAAAFDAAAABwD#####AQAAAAEAAm5EAAAAnBAAAAAAAAEAAAABAAAAAQAAAAAAAAAAAAFEAAAABwD#####AQAAAAEAAm5FAAAAjBAAAAAAAAEAAAABAAAAAQAAAAAAAAAAAAFFAAAABwD#####AQAAAAEAAm5GAAAAmRAAAAAAAAEAAAABAAAAAQAAAAAAAAAAAAFGAAAABwD#####AQAAAAEAAm5HAAAAghAAAAAAAAEAAAABAAAAAQAAAAAAAAAAAAFHAAAABwD#####AQAAAAEAAm5IAAAAfhAAAAAAAAEAAAABAAAAAQAAAAAAAAAAAAFIAAAABwD#####AQAAAAEAAm5JAAAAhhAAAAAAAAEAAAABAAAAAQAAAAAAAAAAAAFJAAAABwD#####AQAAAAEAAm5KAAAAkRAAAAAAAAEAAAABAAAAAQAAAAAAAAAAAAFKAAAABwD#####AQAAAAEAAm5LAAAArhAAAAAAAAEAAAABAAAAAQAAAAAAAAAAAAFLAAAABwD#####AQAAAAEAAm5MAAAAshAAAAAAAAEAAAABAAAAAQAAAAAAAAAAAAFMAAAACgD#####AAJBMwADMC4zAAAAAT#TMzMzMzMzAAAAFQD#####AX8AAAADY2NBAAIAAAAiAAAADgAAAL8AAAAAFQD#####AX8AAAADY2NCAAIAAAAtAAAADgAAAL8AAAAAFQD#####AX8AAAADY2NDAAIAAAAsAAAADgAAAL8AAAAAFQD#####AX8AAAADY2NEAAIAAAAjAAAADgAAAL8AAAAAFQD#####AX8AAAADY2NFAAIAAAArAAAADgAAAL8AAAAAFQD#####AX8AAAADY2NGAAIAAAAkAAAADgAAAL8AAAAAFQD#####AX8AAAADY2NHAAIAAAAxAAAADgAAAL8AAAAAFQD#####AX8AAAADY2NIAAIAAAAgAAAADgAAAL8AAAAAFQD#####AX8AAAADY2NJAAIAAAAwAAAADgAAAL8AAAAAFQD#####AX8AAAADY2NKAAIAAAAvAAAADgAAAL8AAAAAFQD#####AX8AAAADY2NLAAIAAAAuAAAADgAAAL8AAAAAFQD#####AX8AAAADY2NMAAIAAAAeAAAADgAAAL8AAAAAAgD#####Af8AAAAQAAAAAAAAAAAAAABACAAAAAAAAAACcGwJAAFAdVAAAAAAAEBtnCj1wo9c#####wAAAAEADkNPYmpldER1cGxpcXVlAP####8BfwAAAAJmcAAAAMwAAAAVAP####8B#wAAAANmY3AAAgAAAMwAAAAOAAAAvwAAAAAH##########8='
    lll.style.border = 'solid black 1px'
    stor.lll = lll
    stor.mtgAppLecteur.removeAllDoc()
    stor.mtgAppLecteur.addDoc(svgId, txtFigure, true)
    stor.mtgAppLecteur.giveFormula2(svgId, 'A1', j3pGetRandomInt(0, 359))
    stor.mtgAppLecteur.calculateAndDisplayAll(true)

    for (let i = 0; i < stor.listPtAf.length; i++) {
      stor.mtgAppLecteur.setVisible(svgId, '#p' + stor.listPtAf[i].n, true)
      stor.mtgAppLecteur.setVisible(svgId, '#c' + stor.listPtAf[i].n, true)
      stor.mtgAppLecteur.setVisible(svgId, '#cc' + stor.listPtAf[i].n, true)
      stor.mtgAppLecteur.setVisible(svgId, '#n' + stor.listPtAf[i].n, true)
      stor.mtgAppLecteur.setText(svgId, '#n' + stor.listPtAf[i].n, stor.lettres[i])
      stor.mtgAppLecteur.setColor(svgId, '#c' + stor.listPtAf[i].n, 0, 0, 0, true, 0)
      stor.mtgAppLecteur.setColor(svgId, '#cc' + stor.listPtAf[i].n, 0, 0, 0, true, 0)
      stor.mtgAppLecteur.addEventListener(svgId, '#c' + stor.listPtAf[i].n, 'click', (ev) => { clique(stor.listPtAf[i].n, ev) })
      stor.mtgAppLecteur.addEventListener(svgId, '#cc' + stor.listPtAf[i].n, 'click', () => { cliqueP(stor.listPtAf[i].n) })
    }
    for (let i = 0; i < stor.listdAf.length; i++) {
      stor.mtgAppLecteur.setVisible(svgId, '#d' + stor.listdAf[i], true)
      if (stor.nomdroite === '(dx)') {
        stor.mtgAppLecteur.setVisible(svgId, '#nd' + stor.listdAf[i], true)
      }
    }

    lll.addEventListener('click', function (ev) {
      if (stor.raz) return
      const pos = lll.getBoundingClientRect()
      stor.mtgAppLecteur.setPointPosition(svgId, '#pl', ev.clientX - pos.left, ev.clientY - pos.top, true)
      stor.mtgAppLecteur.setVisible(svgId, '#fp', true, true)
      stor.mtgAppLecteur.setVisible(svgId, '#fcp', true, true)
      stor.errRien = true
      stor.errPrec = false
      stor.abon = false
      me.sectionCourante()
    })
  }

  function clique (k, ev) {
    if (stor.raz) return
    stor.raz = true
    const pos = stor.lll.getBoundingClientRect()
    stor.mtgAppLecteur.setPointPosition(svgId, '#pl', ev.clientX - pos.left, ev.clientY - pos.top, true)
    stor.mtgAppLecteur.setVisible(svgId, '#fp', true, true)
    stor.mtgAppLecteur.setVisible(svgId, '#fcp', true, true)
    stor.abon = false
    stor.errPrec = k === stor.Lepoint.n
    stor.errRien = false
    me.sectionCourante()
  }
  function cliqueP (k) {
    if (stor.raz) return
    stor.raz = true
    stor.errRien = false
    stor.errPrec = false
    stor.mtgAppLecteur.setColor(svgId, '#p' + k, 255, 0, 0, true)
    stor.mtgAppLecteur.setColor(svgId, '#cc' + k, 255, 0, 0, true)
    stor.abon = k === stor.Lepoint.n
    me.sectionCourante()
  }
  function lettre (nb) {
    const tablettres = []
    let il, ok
    for (let i = 0; i < nb; i++) {
      do {
        il = j3pGetRandomInt(65, 90)
        ok = true
        for (let j = 0; j < i; j++) {
          if (il === tablettres[j]) {
            ok = false
            break
          }
        }
      } while (!ok)
      tablettres.push(il)
    }
    stor.lettres = []
    for (let i = 0; i < tablettres.length; i++) {
      stor.lettres.push(String.fromCharCode(tablettres[i]))
    }
  }
  function colore (i) {
    if (i === 1) {
      j3pEmpty(stor.zoneBoot1)
      stor.mtgAppLecteur.setLineStyle(svgId, '#d' + stor.Lepoint.d1[0], 2, 4, true)
      stor.mtgAppLecteur.setColor(svgId, '#d' + stor.Lepoint.d1[0], 0, 0, 255, true)
    } else {
      j3pEmpty(stor.zoneBoot2)
      stor.mtgAppLecteur.setLineStyle(svgId, '#d' + stor.Lepoint.d1[1], 2, 4, true)
      stor.mtgAppLecteur.setColor(svgId, '#d' + stor.Lepoint.d1[1], 0, 0, 255, true)
    }
  }
  function isVisible (d) {
    return stor.listdAf.indexOf(d) !== -1
  }
  function yareponse () {
    colore(1)
    colore(2)
    stor.raz = true
    return true
  }
  function bonneReponse () {
    return stor.abon
  }
  function getDonnees () {
    return {

      // Nombre de répétitions de l’exercice
      nbrepetitions: 8,
      nbetapes: 1,

      min: 2,
      max: 10,
      progressif: true,
      Aide: false,
      NomDesDroites: 'les deux',

      // Si indication !== "" alors un lien "Indication" s’affiche dans la zone Inférieure Gauche
      indication: '',

      // pour le cas de presentation1bis
      // En effet pas d’indication possible pour cette présentation
      // ,surchargeindication: false

      // Nombre de chances dont dispose l’él§ve pour répondre à la question
      nbchances: 1,

      structure: 'presentation1bis', //  || "presentation2" || "presentation3"  || "presentation1bis"

      // Paramètres de la section, avec leurs valeurs par défaut.
      textes: {
        couleurexplique: '#A9E2F3',
        couleurcorrec: '#A9F5A9',
        couleurenonce: '#D8D8D8',
        couleurenonceg: '#F3E2A9',
        couleuretiquettes: '#A9E2F3',
        Consigne1: '<b>Clique précisément</b> sur le point d’intersection des droites £a et £b .',
        Consigne2: 'Le point02 £a est le point02 d’intersection des deux droites.',
        ERRnompoint02: 'Tu as cliqué sur <b>le nom</b> du point £a !',
        benouaicbien: 'Bravo !',
        colore: 'Voir £a '
      },

      pe: 0
    }
  }
  function initSection () {
    me.donneesSection = getDonnees()
    ds = me.donneesSection

    me.surcharge()
    if ((ds.NomDesDroites !== '(AB)') && (ds.NomDesDroites !== '(dx)')) {
      ds.NomDesDroites = 'les deux'
    }
    ds.nbitems = ds.nbetapes * ds.nbrepetitions
    // Construction de la page
    me.construitStructurePage(me.donneesSection.structure)

    if (me.donneesSection.min < 2) { me.donneesSection.min = 2 }
    if (me.donneesSection.max > 5) { me.donneesSection.max = 5 }
    if (me.donneesSection.min > me.donneesSection.max) { me.donneesSection.min = me.donneesSection.max }
    stor.nbdroites = me.donneesSection.min
    me.typederreurs = [0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0]

    me.score = 0
    stor.lisdtpo = [1, 2, 3, 4, 5, 6, 7]
    stor.listp = [
      { n: 'A', d1: [7, 4] },
      { n: 'B', d1: [7, 6] },
      { n: 'C', d1: [4, 6] },
      { n: 'D', d1: [6, 1] },
      { n: 'E', d1: [4, 1] },
      { n: 'F', d1: [5, 4, 2] },
      { n: 'G', d1: [5, 7, 1] },
      { n: 'H', d1: [5, 3] },
      { n: 'I', d1: [3, 1] },
      { n: 'J', d1: [7, 3] },
      { n: 'K', d1: [3, 2] },
      { n: 'L', d1: [7, 2] }
    ]

    me.afficheTitre('Retrouver le point d’intersection')

    if (me.donneesSection.indication) me.indication(me.zonesElts.IG, me.donneesSection.indication)
    // chargement mtg
    getMtgCore({ withMathJax: true })
      .then(
        // success
        (mtgAppLecteur) => {
          stor.mtgAppLecteur = mtgAppLecteur
          enonceMain()
        },
        // failure
        (error) => {
          j3pShowError(error, { message: 'mathgraph n’est pas correctement chargé', mustNotify: true })
        })
      // plantage dans enonceMain
      .catch(j3pShowError)
  }

  function affcofo () {
    let yaexplik = false
    if (stor.errPrec) {
      yaexplik = true
      j3pAffiche(stor.zoneExplication, '', 'Tu dois être plus précis !')
    }
    if (stor.errRien) {
      yaexplik = true
      j3pAffiche(stor.zoneExplication, '', 'Il faut cliquer sur un point !')
    }
    colore(1)
    colore(2)
    stor.mtgAppLecteur.setColor(svgId, '#p' + stor.Lepoint.n, 0, 255, 0, true)
    stor.mtgAppLecteur.setColor(svgId, '#cc' + stor.Lepoint.n, 0, 255, 0, true)
    if (yaexplik) stor.zoneExplication.classList.add('explique')
  }
  function trouveNom (n) {
    let lp = []
    for (let i = 0; i < stor.listPtAf.length; i++) {
      if (stor.listPtAf[i].d1.indexOf(n) !== -1) lp.push(i)
    }
    lp = j3pShuffle(lp)
    return '(' + stor.lettres[lp[0]] + stor.lettres[lp[1]] + ')'
  }
  function enonceMain () {
    me.videLesZones()
    me.afficheBoutonValider()
    stor.raz = false
    // ou dans le cas de presentation3
    // me.ajouteBoutons()
    // création du conteneur dans lequel se trouveront toutes les consignes
    const conteneur = j3pAddElt(me.zonesElts.MG, 'div', '', {
      style: me.styles.etendre('toutpetit.enonce', { padding: '10px' })
    })
    stor.zoneConsigne = addDefaultTable(conteneur, 1, 1)[0][0]
    const yui = addDefaultTable(conteneur, 1, 3)
    stor.zoneTravail = yui[0][0]
    stor.zoneExplication = yui[0][2]
    yui[0][1].style.width = '10px'
    stor.zoneExplication.style.color = me.styles.cfaux
    stor.zoneBoot1 = j3pAddElt(me.zonesElts.MD, 'div', '', { style: me.styles.etendre('toutpetit.correction', { padding: '10px' }) })
    stor.zoneBoot2 = j3pAddElt(me.zonesElts.MD, 'div', '', { style: me.styles.etendre('toutpetit.correction', { padding: '10px' }) })
    stor.zoneCorrection = j3pAddElt(me.zonesElts.MD, 'div', '', { style: me.styles.etendre('toutpetit.correction', { padding: '10px' }) })

    if (ds.progressif) {
      if (me.questionCourante < ds.nbrepetitions) {
        stor.nbdroites = stor.nbdroites + Math.round((ds.max - stor.nbdroites) / (ds.nbrepetitions - me.questionCourante))
        if (me.questionCourante === 1) { stor.nbdroites = ds.min }
      } else { stor.nbdroites = ds.max }
    } else { stor.nbdroites = j3pGetRandomInt(ds.min, ds.max) }
    stor.nomdroite = ds.NomDesDroites
    if (stor.nomdroite === 'les deux') {
      if (j3pGetRandomBool()) {
        stor.nomdroite = '(AB)'
      } else { stor.nomdroite = '(dx)' }
    }

    do {
      stor.listdAf = []
      stor.listPtAf = []
      let ldc = j3pClone(stor.lisdtpo)
      ldc = j3pShuffle(ldc)
      for (let i = 0; i < stor.nbdroites; i++) {
        stor.listdAf.push(ldc.pop())
      }
      for (let i = 0; i < stor.listp.length; i++) {
        let nbd = 0
        for (let j = 0; j < stor.listdAf.length; j++) {
          if (stor.listp[i].d1.indexOf(stor.listdAf[j]) !== -1) nbd++
        }
        if (nbd > 1) stor.listPtAf.push(stor.listp[i])
      }
      stor.listPtAf = j3pShuffle(stor.listPtAf)
      stor.Lepoint = j3pClone(stor.listPtAf[0])
    } while (stor.listPtAf.length < 1)
    if (stor.nomdroite !== '(dx)') {
      stor.listPtAf = []
      for (let i = 0; i < stor.listp.length; i++) {
        let nbd = false
        for (let j = 0; j < stor.listdAf.length; j++) {
          if (stor.listp[i].d1.indexOf(stor.listdAf[j]) !== -1) nbd = true
        }
        if (nbd) stor.listPtAf.push(stor.listp[i])
      }
    }
    lettre(stor.listPtAf.length)

    stor.Lepoint.d1 = j3pShuffle(stor.Lepoint.d1)
    for (let i = stor.Lepoint.d1.length - 1; i > -1; i--) {
      if (!isVisible(stor.Lepoint.d1[i])) stor.Lepoint.d1.splice(i, 1)
    }
    if (stor.nomdroite === '(dx)') {
      stor.nd1 = '(d' + stor.Lepoint.d1[0] + ')'
      stor.nd2 = '(d' + stor.Lepoint.d1[1] + ')'
    } else {
      stor.nd1 = trouveNom(stor.Lepoint.d1[0])
      stor.nd2 = trouveNom(stor.Lepoint.d1[1])
    }
    j3pAffiche(stor.zoneConsigne, '', ds.textes.Consigne1, { a: stor.nd1, b: stor.nd2 })
    figureSelPrecis()

    if (ds.Aide) {
      j3pAjouteBouton(stor.zoneBoot1, function () { colore(1) }, { className: 'MepBoutons', value: me.donneesSection.textes.colore.replace('£a', stor.nd1) })
      j3pAjouteBouton(stor.zoneBoot2, function () { colore(2) }, { className: 'MepBoutons', value: me.donneesSection.textes.colore.replace('£a', stor.nd2) })
    }
    me.zonesElts.MG.classList.add('fond')
    stor.zoneTravail.classList.add('travail')
    stor.zoneConsigne.classList.add('enonce')
    me.finEnonce()
    me.cacheBoutonValider()
  }

  switch (this.etat) {
    case 'enonce':
      // code exécuté au lancement de la section
      if (this.debutDeLaSection) {
        initSection()
      } else {
        enonceMain()
      }
      break // case "enonce":

    case 'correction':
      if (!yareponse()) {
        stor.zoneCorrection.style.color = this.styles.cfaux
        stor.zoneCorrection.innerHTML = reponseIncomplete
      } else {
        // Bonne réponse
        if (bonneReponse()) {
          this._stopTimer()
          this.score++
          stor.zoneCorrection.style.color = this.styles.cbien
          stor.zoneCorrection.innerHTML = cBien

          stor.raz = true
          stor.mtgAppLecteur.setColor(svgId, '#p' + stor.Lepoint.n, 0, 255, 0, true)
          stor.mtgAppLecteur.setColor(svgId, '#cc' + stor.Lepoint.n, 0, 255, 0, true)

          this.typederreurs[0]++
          this.cacheBoutonValider()
          this.etat = 'navigation'
          this.sectionCourante()
        } else {
          // Pas de bonne réponse
          stor.zoneCorrection.style.color = this.styles.cfaux

          // A cause de la limite de temps :
          if (this.isElapsed) { // limite de temps
            affcofo()

            stor.zoneCorrection.innerHTML = tempsDepasse
            this.typederreurs[10]++
            this.cacheBoutonValider()

            this.etat = 'navigation'
            this.sectionCourante()
          } else {
            stor.zoneCorrection.innerHTML = cFaux

            if (this.essaiCourant < this.donneesSection.nbchances) {
              stor.zoneCorrection.innerHTML += '<br>' + essaieEncore

              this.typederreurs[1]++
              // indication éventuelle ici
            } else {
              this._stopTimer()
              this.cacheBoutonValider()
              stor.zoneCorrection.innerHTML += '<br>' + regardeCorrection

              affcofo()

              this.typederreurs[2]++
              this.etat = 'navigation'
              this.sectionCourante()
            }
          }
        }
      }
      // Obligatoire
      this.finCorrection()
      break // case "correction":

    case 'navigation':

      if (this.sectionTerminee()) {
        // On détermine la phrase d’état renvoyée par la section
        this.parcours.pe = (this.donneesSection.nbetapes * this.score) / this.donneesSection.nbitems
        this.afficheBoutonSectionSuivante()
        this.focus('sectioncontinuer')
      } else {
        this.etat = 'enonce'
        this.afficheBoutonSuite()
        this.focus('boutoncontinuer')
      }
      // Obligatoire
      this.finNavigation()

      break // case "navigation":
  }
}
