import { j3pGetNewId, j3pGetRandomBool, j3pGetRandomInt, j3pShowError } from 'src/legacy/core/functions'
import { j3pCreeSVG } from 'src/legacy/core/functionsSvg'
import { addMaj } from 'src/lib/utils/string'

import getMtgFig from './getMtgFigNomme03'

const coMod = [255, 0, 191]
const coMod2 = [50, 50, 255]

class FigNomme03 {
  /**
   * @param {Parcours} parcours
   */
  constructor (parcours) {
    const stor = parcours.storage
    stor.svgId = j3pGetNewId()
    stor.LeNomDuTruc = ''
    stor.blok = false

    // les propriétés de storage qu’on utilise
    this.blok = stor.blok
    /** @type {MtgAppLecteur} */
    this.mtgAppLecteur = stor.mtgAppLecteur
    this.svgId = stor.svgId
    const svgId = this.svgId // raccourci
    const elt = stor.element

    stor.height = 300
    stor.lwidth = 380

    stor.listedeselemedessine = []
    const nomspris = []
    this.fifig = []
    stor.lisecss = []
    stor.NomElem = []
    stor.NomNeedElem = []
    stor.NomContenant = { texte: '', chapo: 0 }

    let listeNom = []
    stor.listeChange = []
    stor.listCache = []
    let pointhaut, A1, A2, A3, A4, lalettre7, lalettre5, lalettre6, lalettre3, lalettre4, lalettre1, lalettre2, lalettre8
    A1 = 0
    A2 = 0
    A3 = 0
    A4 = 0
    stor.lafig = getMtgFig(stor.contenant)

    switch (stor.contenant) {
      case 'SPHERE':
        stor.height = 340
        A1 = 306
        lalettre1 = addMaj(nomspris)
        lalettre2 = addMaj(nomspris)
        lalettre3 = addMaj(nomspris)
        lalettre4 = addMaj(nomspris)
        lalettre5 = addMaj(nomspris)
        lalettre6 = addMaj(nomspris)
        lalettre7 = addMaj(nomspris)
        if (['pole', 'equateur', 'meridien', 'parallele'].indexOf(elt) === -1) {
          A1 = j3pGetRandomInt(0, 360)
        } else {
          lalettre1 = 'N'
          lalettre6 = 'S'
        }

        if (elt === 'centre') {
          stor.NomElem = [{ texte: lalettre5, chapo: 0 }]
          stor.listeChange.push({ id: '#sO', co: coMod, ep: 2, st: 0 })
        } else if (elt === 'rayon') {
          const haz = j3pGetRandomInt(0, 4)
          if (haz === 0) {
            stor.listeChange.push({ id: '#r1', co: coMod, ep: 4, st: 2 })
            stor.NomElem = [{ texte: '[' + lalettre5 + lalettre4 + ']', chapo: 0 }, { texte: '[' + lalettre4 + lalettre5 + ']', chapo: 0 }]
          } else if (haz === 1) {
            stor.listeChange.push({ id: '#r2', co: coMod, ep: 4, st: 2 })
            stor.NomElem = [{ texte: '[' + lalettre5 + lalettre3 + ']', chapo: 0 }, { texte: '[' + lalettre3 + lalettre5 + ']', chapo: 0 }]
          } else if (haz === 2) {
            stor.listeChange.push({ id: '#r3', co: coMod, ep: 4, st: 2 })
            stor.NomElem = [{ texte: '[' + lalettre5 + lalettre2 + ']', chapo: 0 }, { texte: '[' + lalettre2 + lalettre5 + ']', chapo: 0 }]
          } else if (haz === 3) {
            stor.listeChange.push({ id: '#r4', co: coMod, ep: 4, st: 2 })
            stor.NomElem = [{ texte: '[' + lalettre5 + lalettre1 + ']', chapo: 0 }, { texte: '[' + lalettre1 + lalettre5 + ']', chapo: 0 }]
          } else {
            stor.listeChange.push({ id: '#r5', co: coMod, ep: 4, st: 2 })
            stor.NomElem = [{ texte: '[' + lalettre5 + lalettre6 + ']', chapo: 0 }, { texte: '[' + lalettre6 + lalettre5 + ']', chapo: 0 }]
          }
        } else if (elt === 'diametre') {
          if (j3pGetRandomBool()) {
            stor.listeChange.push({ id: '#r1', co: coMod, ep: 4, st: 2 })
            stor.NomElem = [{ texte: '[' + lalettre3 + lalettre4 + ']', chapo: 0 }, { texte: '[' + lalettre4 + lalettre3 + ']', chapo: 0 }]
            stor.listeChange.push({ id: '#r2', co: coMod, ep: 4, st: 2 })
          } else {
            stor.listeChange.push({ id: '#r5', co: coMod, ep: 4, st: 2 })
            stor.NomElem = [{ texte: '[' + lalettre1 + lalettre6 + ']', chapo: 0 }, { texte: '[' + lalettre6 + lalettre1 + ']', chapo: 0 }]
            stor.listeChange.push({ id: '#r4', co: coMod, ep: 4, st: 2 })
          }
        } else if (elt === 'grand_cercle') {
          stor.NomElem = [{ texte: 'violet', chapo: 0 }]
          const haz = j3pGetRandomInt(0, 2)
          if (haz === 0) {
            stor.listeChange.push({ id: '#meri1', co: coMod, ep: 4, st: 1 })
            stor.listeChange.push({ id: '#meri2', co: coMod, ep: 4, st: 1 })
            stor.listeChange.push({ id: '#meri3', co: coMod, ep: 4, st: 0 })
            stor.listeChange.push({ id: '#meri4', co: coMod, ep: 4, st: 0 })
          } else if (haz === 1) {
            stor.listeChange.push({ id: '#equa1', co: coMod, ep: 4, st: 1 })
            stor.listeChange.push({ id: '#equa2', co: coMod, ep: 4, st: 1 })
            stor.listeChange.push({ id: '#equa3', co: coMod, ep: 4, st: 0 })
            stor.listeChange.push({ id: '#equa4', co: coMod, ep: 4, st: 0 })
          } else {
            stor.listeChange.push({ id: '#C11', co: coMod, ep: 4, st: 0 })
            stor.listeChange.push({ id: '#C22', co: coMod, ep: 4, st: 0 })
            stor.listeChange.push({ id: '#C33', co: coMod, ep: 4, st: 0 })
            stor.listeChange.push({ id: '#C44', co: coMod, ep: 4, st: 0 })
          }
        } else if (elt === 'meridien') {
          stor.NomElem = [{ texte: 'violet', chapo: 0 }]
          if (j3pGetRandomBool()) {
            stor.listeChange.push({ id: '#meri1', co: coMod, ep: 4, st: 1 })
            stor.listeChange.push({ id: '#meri2', co: coMod, ep: 4, st: 1 })
          } else {
            stor.listeChange.push({ id: '#meri3', co: coMod, ep: 4, st: 0 })
            stor.listeChange.push({ id: '#meri4', co: coMod, ep: 4, st: 0 })
          }
        } else if (elt === 'parallele') {
          stor.NomElem = [{ texte: 'violet', chapo: 0 }]
          stor.listeChange.push({ id: '#para1', co: coMod, ep: 4, st: 0 })
          stor.listeChange.push({ id: '#para2', co: coMod, ep: 4, st: 1 })
          stor.listeChange.push({ id: '#para3', co: coMod, ep: 4, st: 0 })
          stor.listeChange.push({ id: '#para4', co: coMod, ep: 4, st: 1 })
        } else if (elt === 'equateur') {
          stor.NomElem = [{ texte: 'violet', chapo: 0 }]
          stor.listeChange.push({ id: '#equa1', co: coMod, ep: 4, st: 1 })
          stor.listeChange.push({ id: '#equa2', co: coMod, ep: 4, st: 1 })
          stor.listeChange.push({ id: '#equa3', co: coMod, ep: 4, st: 0 })
          stor.listeChange.push({ id: '#equa4', co: coMod, ep: 4, st: 0 })
        } else if (elt === 'pole') {
          if (j3pGetRandomBool()) {
            stor.NomElem = [{ texte: lalettre6, chapo: 0 }]
            stor.listeChange.push({ id: '#sS', co: coMod, ep: 2, st: 0 })
          } else {
            stor.NomElem = [{ texte: lalettre1, chapo: 0 }]
            stor.listeChange.push({ id: '#sM', co: coMod, ep: 2, st: 0 })
          }
        }
        // pas de else if ici car on traite rayon et tous les autres
        if (elt === 'le_rayon') {
          stor.NomElem = [{ texte: lalettre5 + lalettre7, chapo: 0 }, { texte: lalettre7 + lalettre5, chapo: 0 }]
        } else {
          stor.listCache.push('#aa1', '#aa2', '#aa3', '#aa4', '#aa5')
        }
        // idem ici avec diametre
        if (elt === 'le_diametre') {
          stor.NomElem = [{ texte: lalettre4 + lalettre3, chapo: 0 }, { texte: lalettre3 + lalettre4, chapo: 0 }]
        } else {
          stor.listCache.push('#bb1', '#bb2', '#bb3', '#bb4', '#bb5')
        }
        listeNom = [
          { t: '#npM', nom: lalettre1 },
          { t: '#npN', nom: lalettre2 },
          { t: '#npT', nom: lalettre3 },
          { t: '#npV', nom: lalettre4 },
          { t: '#npO', nom: lalettre5 },
          { t: '#npS', nom: lalettre6 },
          { t: '#npX', nom: lalettre7 }
        ]
        break

      case 'CONE_REVOLUTION':
        stor.height = 400
        stor.lwidth = 430
        lalettre1 = addMaj(nomspris)
        lalettre2 = addMaj(nomspris)
        lalettre3 = addMaj(nomspris)
        lalettre4 = addMaj(nomspris)
        lalettre5 = addMaj(nomspris)
        lalettre6 = addMaj(nomspris)
        lalettre7 = addMaj(nomspris)
        A1 = j3pGetRandomInt(0, 360)
        A2 = A3 = j3pGetRandomInt(65, 80)
        A4 = j3pGetRandomInt(7, 9)
        if (j3pGetRandomBool()) {
          stor.listeChange.push({ id: '#ray1', st: 2, ep: 3, co: [0, 0, 0] })
          stor.listeChange.push({ id: '#ray2', st: 2, ep: 3, co: [0, 0, 0] })
          stor.listeChange.push({ id: '#ray3', st: 2, ep: 3, co: [0, 0, 0] })
          stor.listeChange.push({ id: '#diam', st: 2, ep: 3, co: [0, 0, 0] })
          stor.listeChange.push({ id: '#arcc1', st: 1, ep: 4, co: [0, 0, 0] })
          stor.listeChange.push({ id: '#arcc2', st: 1, ep: 4, co: [0, 0, 0] })
          pointhaut = 2
        } else {
          stor.listeChange.push({ id: '#arcc1', st: 0, ep: 4, co: [0, 0, 0] })
          stor.listeChange.push({ id: '#arcc2', st: 0, ep: 4, co: [0, 0, 0] })
          pointhaut = 0
        }

        listeNom = [{ t: '#npO', nom: lalettre2 }, { t: '#npA', nom: lalettre3 }, { t: '#npY', nom: lalettre4 }, { t: '#npT', nom: lalettre5 }, { t: '#npK', nom: lalettre7 }]

        if (elt === 'generatrice') {
          stor.listeChange.push({ id: '#gen2', co: coMod, ep: 4, st: 0 })
          stor.NomElem = [{ texte: '[' + lalettre3 + lalettre6 + ']', chapo: 0 }, { texte: '[' + lalettre6 + lalettre3 + ']', chapo: 0 }]
          listeNom.push({ t: '#npL', nom: lalettre6 })
        } else {
          stor.listCache.push('#npL')
        }

        if (elt === 'centre') {
          stor.NomElem = [{ texte: lalettre2, chapo: 0 }]
          stor.listeChange.push({ id: '#sO', co: coMod, ep: 2, st: 0 })
        } else if (elt === 'sommet_principal') {
          stor.NomElem = [{ texte: lalettre3, chapo: 0 }]
          stor.listeChange.push({ id: '#sA', co: coMod, ep: 2, st: 0 })
        } else if (elt === 'rayon') {
          if (j3pGetRandomBool()) {
            stor.listeChange.push({ id: '#ray1', co: coMod, ep: 4, st: pointhaut })
            stor.NomElem = [{ texte: '[' + lalettre2 + lalettre4 + ']', chapo: 0 }, { texte: '[' + lalettre4 + lalettre2 + ']', chapo: 0 }]
          } else {
            stor.listeChange.push({ id: '#ray2', co: coMod, ep: 4, st: pointhaut })
            stor.NomElem = [{ texte: '[' + lalettre2 + lalettre5 + ']', chapo: 0 }, { texte: '[' + lalettre5 + lalettre2 + ']', chapo: 0 }]
          }
        } else if (elt === 'diametre') {
          stor.listeChange.push({ id: '#ray1', co: coMod, ep: 4, st: pointhaut })
          stor.NomElem = [{ texte: '[' + lalettre7 + lalettre4 + ']', chapo: 0 }, { texte: '[' + lalettre4 + lalettre7 + ']', chapo: 0 }]
          stor.listeChange.push({ id: '#ray3', co: coMod, ep: 4, st: pointhaut })
        }

        if (elt === 'le_rayon') {
          stor.NomElem = [{ texte: lalettre4 + lalettre2, chapo: 0 }, { texte: lalettre2 + lalettre4, chapo: 0 }]
        } else {
          stor.listCache.push('#cc1', '#cc2', '#cc3', '#cc4')
        }

        if (elt === 'le_diametre') {
          stor.NomElem = [{ texte: lalettre4 + lalettre7, chapo: 0 }, { texte: lalettre7 + lalettre4, chapo: 0 }]
        } else {
          stor.listCache.push('#bb1', '#bb2', '#bb5', '#bb4')
        }

        if (elt !== 'le_rayon' && elt !== 'le_diametre') {
          stor.listCache.push('#bb3')
        }

        if (elt === 'hauteur') {
          stor.NomElem = [{ texte: lalettre3 + lalettre2, chapo: 0 }, { texte: lalettre2 + lalettre3, chapo: 0 }]
        } else {
          stor.listCache.push('#aa1', '#aa2', '#aa3', '#aa4', '#aa5', '#npH', '#hauteur', '#co7', '#co8', '#co6', '#co5', '#co4', '#co3', '#co2', '#co1')
        }

        if (elt === 'base') {
          stor.NomElem = [{ texte: 'violette', chapo: 0 }]
          stor.listeChange.push({ id: '#surf3', co: coMod, ep: 4, st: 0 })
        } else if (elt === 'face') {
          stor.NomElem = [{ texte: 'violette', chapo: 0 }]
          if (j3pGetRandomBool()) {
            stor.listeChange.push({ id: '#surf3', co: coMod, ep: 4, st: 0 })
          } else {
            stor.listeChange.push({ id: '#surf1', co: coMod, ep: 4, st: 0 })
            stor.listeChange.push({ id: '#surf2', co: coMod, ep: 4, st: 0 })
          }
        } else if (elt === 'face_laterale') {
          stor.NomElem = [{ texte: 'violette', chapo: 0 }]

          stor.listeChange.push({ id: '#surf1', co: coMod, ep: 4, st: 0 })
          stor.listeChange.push({ id: '#surf2', co: coMod, ep: 4, st: 0 })
        }
        break

      case 'CONE':
        stor.height = 400
        stor.lwidth = 430
        A1 = j3pGetRandomInt(0, 360)
        A4 = j3pGetRandomInt(7, 9)
        if (j3pGetRandomBool()) {
          A2 = j3pGetRandomInt(92, 96)
          A3 = j3pGetRandomInt(50, 65)
        } else {
          do {
            A2 = j3pGetRandomInt(60, 85)
            A3 = j3pGetRandomInt(60, 85)
          }
          while (Math.abs(A2 - A3) < 15)
          stor.listeChange.push({ id: '#hauteur', st: 1, ep: 3, co: [0, 0, 0] })
        }
        if (j3pGetRandomBool()) {
          stor.listeChange.push({ id: '#ray1', st: 2, ep: 3, co: [0, 0, 0] })
          stor.listeChange.push({ id: '#ray2', st: 2, ep: 3, co: [0, 0, 0] })
          stor.listeChange.push({ id: '#ray3', st: 2, ep: 3, co: [0, 0, 0] })
          stor.listeChange.push({ id: '#diam', st: 2, ep: 3, co: [0, 0, 0] })
          stor.listeChange.push({ id: '#arcc1', st: 1, ep: 4, co: [0, 0, 0] })
          stor.listeChange.push({ id: '#arcc2', st: 1, ep: 4, co: [0, 0, 0] })
          pointhaut = 2
        } else {
          stor.listeChange.push({ id: '#arcc1', st: 0, ep: 4, co: [0, 0, 0] })
          stor.listeChange.push({ id: '#arcc2', st: 0, ep: 4, co: [0, 0, 0] })
          pointhaut = 0
        }
        lalettre1 = addMaj(nomspris)
        lalettre2 = addMaj(nomspris)
        lalettre3 = addMaj(nomspris)
        lalettre4 = addMaj(nomspris)
        lalettre5 = addMaj(nomspris)
        lalettre6 = addMaj(nomspris)
        lalettre7 = addMaj(nomspris)
        listeNom = [{ t: '#npO', nom: lalettre2 }, { t: '#npA', nom: lalettre3 }, { t: '#npY', nom: lalettre4 }, { t: '#npT', nom: lalettre5 }, { t: '#npK', nom: lalettre7 }]

        if (elt === 'centre') {
          stor.NomElem = [{ texte: lalettre2, chapo: 0 }]
          stor.listeChange.push({ id: '#sO', co: coMod, ep: 2, st: 0 })
        } else if (elt === 'sommet_principal') {
          stor.NomElem = [{ texte: lalettre3, chapo: 0 }]
          stor.listeChange.push({ id: '#sA', co: coMod, ep: 2, st: 0 })
        } else if (elt === 'rayon') {
          if (j3pGetRandomBool()) {
            stor.listeChange.push({ id: '#ray1', co: coMod, ep: 4, st: pointhaut })
            stor.NomElem = [{ texte: '[' + lalettre2 + lalettre4 + ']', chapo: 0 }, { texte: '[' + lalettre4 + lalettre2 + ']', chapo: 0 }]
          } else {
            stor.listeChange.push({ id: '#ray2', co: coMod, ep: 4, st: pointhaut })
            stor.NomElem = [{ texte: '[' + lalettre2 + lalettre5 + ']', chapo: 0 }, { texte: '[' + lalettre5 + lalettre2 + ']', chapo: 0 }]
          }
        } else if (elt === 'diametre') {
          stor.listeChange.push({ id: '#ray1', co: coMod, ep: 4, st: pointhaut })
          stor.NomElem = [{ texte: '[' + lalettre7 + lalettre4 + ']', chapo: 0 }, { texte: '[' + lalettre4 + lalettre7 + ']', chapo: 0 }]
          stor.listeChange.push({ id: '#ray3', co: coMod, ep: 4, st: pointhaut })
        }

        if (elt === 'le_rayon') {
          stor.NomElem = [{ texte: lalettre4 + lalettre2, chapo: 0 }, { texte: lalettre2 + lalettre4, chapo: 0 }]
        } else {
          stor.listCache.push('#cc1', '#cc2', '#cc3', '#cc4')
        }

        if (elt === 'le_diametre') {
          stor.NomElem = [{ texte: lalettre4 + lalettre7, chapo: 0 }, { texte: lalettre7 + lalettre4, chapo: 0 }]
        } else {
          stor.listCache.push('#bb1', '#bb2', '#bb5', '#bb4')
        }

        if (elt !== 'le_rayon' && elt !== 'le_diametre') {
          stor.listCache.push('#bb3')
        }

        if (elt === 'hauteur') {
          stor.NomElem = [{ texte: lalettre3 + lalettre6, chapo: 0 }, { texte: lalettre6 + lalettre3, chapo: 0 }]
          listeNom.push({ t: '#npH', nom: lalettre6 })
        } else {
          stor.listCache.push('#aa1', '#aa2', '#aa3', '#aa4', '#aa5', '#npH', '#hauteur', '#co7', '#co8', '#co6', '#co5', '#co4', '#co3', '#co2', '#co1')
        }

        if (elt === 'base') {
          stor.NomElem = [{ texte: 'violette', chapo: 0 }]

          stor.listeChange.push({ id: '#surf3', co: coMod, ep: 4, st: 0 })
        } else if (elt === 'face') {
          const haz = j3pGetRandomInt(0, 1)
          stor.NomElem = [{ texte: 'violette', chapo: 0 }]

          if (haz === 0) { stor.listeChange.push({ id: '#surf3', co: coMod, ep: 4, st: 0 }) } else {
            stor.listeChange.push({ id: '#surf1', co: coMod, ep: 4, st: 0 })
            stor.listeChange.push({ id: '#surf2', co: coMod, ep: 4, st: 0 })
          }
        } else if (elt === 'face_laterale') {
          stor.NomElem = [{ texte: 'violette', chapo: 0 }]

          stor.listeChange.push({ id: '#surf1', co: coMod, ep: 4, st: 0 })
          stor.listeChange.push({ id: '#surf2', co: coMod, ep: 4, st: 0 })
        }
        break

      case 'PYRAMIDE': {
        let ep1, ep2
        stor.height = 400
        A1 = j3pGetRandomInt(0, 360)
        A3 = j3pGetRandomInt(0, 180)
        A2 = j3pGetRandomInt(50, 179)
        A4 = j3pGetRandomInt(5, 11)
        if (j3pGetRandomBool()) {
          stor.listeChange.push({ id: '#b1', ep: 4, st: 1, co: [0, 0, 0] })
          ep1 = 1
          ep2 = 0
        } else {
          stor.listeChange.push({ id: '#s2', ep: 4, st: 1, co: [0, 0, 0] })
          stor.listeChange.push({ id: '#s3', ep: 4, st: 1, co: [0, 0, 0] })
          ep2 = 1
          ep1 = 0
        }
        lalettre1 = addMaj(nomspris)
        lalettre2 = addMaj(nomspris)
        lalettre3 = addMaj(nomspris)
        lalettre4 = addMaj(nomspris)
        lalettre5 = addMaj(nomspris)
        lalettre6 = addMaj(nomspris)
        listeNom = [{ t: '#npS', nom: lalettre1 }, { t: '#npA', nom: lalettre2 }, { t: '#npB', nom: lalettre3 }, { t: '#npC', nom: lalettre4 }, { t: '#npD', nom: lalettre5 }]

        if (elt === 'hauteur') {
          listeNom.push({ t: '#npE', nom: lalettre6 })
          stor.NomElem = [{ texte: lalettre1 + lalettre6, chapo: 0 }, { texte: lalettre6 + lalettre1, chapo: 0 }]
        } else {
          stor.listCache.push('#aa1', '#aa2', '#aa3', '#aa4', '#aa5', '#npE', '#aa6', '#aa7', '#aa8', '#aa9', '#aa10', '#aa11', '#aa12', '#aa13')
        }

        if (elt === 'sommet_principal') {
          stor.NomElem = [{ texte: lalettre1, chapo: 0 }]
          stor.listeChange.push({ id: '#sS', co: coMod, ep: 2, st: 0 })
        } else if (elt === 'arete') {
          const haz = j3pGetRandomInt(0, 7)
          if (haz === 0) {
            stor.listeChange.push({ id: '#s1', co: coMod, ep: 4, st: 0 })
            stor.NomElem = [{ texte: '[' + lalettre2 + lalettre1 + ']', chapo: 0 }, { texte: '[' + lalettre1 + lalettre2 + ']', chapo: 0 }]
          }
          if (haz === 1) {
            stor.listeChange.push({ id: '#s2', co: coMod, ep: 4, st: ep2 })
            stor.NomElem = [{ texte: '[' + lalettre3 + lalettre1 + ']', chapo: 0 }, { texte: '[' + lalettre1 + lalettre3 + ']', chapo: 0 }]
          }
          if (haz === 2) {
            stor.listeChange.push({ id: '#s3', co: coMod, ep: 4, st: ep2 })
            stor.NomElem = [{ texte: '[' + lalettre4 + lalettre1 + ']', chapo: 0 }, { texte: '[' + lalettre1 + lalettre4 + ']', chapo: 0 }]
          }
          if (haz === 3) {
            stor.listeChange.push({ id: '#s4', co: coMod, ep: 4, st: 0 })
            stor.NomElem = [{ texte: '[' + lalettre5 + lalettre1 + ']', chapo: 0 }, { texte: '[' + lalettre1 + lalettre5 + ']', chapo: 0 }]
          }
          if (haz === 4) {
            stor.listeChange.push({ id: '#b1', co: coMod, ep: 4, st: ep1 })
            stor.NomElem = [{ texte: '[' + lalettre5 + lalettre2 + ']', chapo: 0 }, { texte: '[' + lalettre2 + lalettre5 + ']', chapo: 0 }]
          }
          if (haz === 5) {
            stor.listeChange.push({ id: '#b2', co: coMod, ep: 4, st: 0 })
            stor.NomElem = [{ texte: '[' + lalettre3 + lalettre2 + ']', chapo: 0 }, { texte: '[' + lalettre2 + lalettre3 + ']', chapo: 0 }]
          }
          if (haz === 6) {
            stor.listeChange.push({ id: '#b3', co: coMod, ep: 4, st: 0 })
            stor.NomElem = [{ texte: '[' + lalettre3 + lalettre4 + ']', chapo: 0 }, { texte: '[' + lalettre4 + lalettre3 + ']', chapo: 0 }]
          }
          if (haz === 7) {
            stor.listeChange.push({ id: '#b4', co: coMod, ep: 4, st: 0 })
            stor.NomElem = [{ texte: '[' + lalettre5 + lalettre4 + ']', chapo: 0 }, { texte: '[' + lalettre4 + lalettre5 + ']', chapo: 0 }]
          }
        } else if (elt === 'sommet') {
          const haz = j3pGetRandomInt(0, 3)
          if (haz === 0) {
            stor.NomElem = [{ texte: lalettre2, chapo: 0 }]
            stor.listeChange.push({ id: '#sA', co: coMod, ep: 2, st: 0 })
          }
          if (haz === 1) {
            stor.NomElem = [{ texte: lalettre3, chapo: 0 }]
            stor.listeChange.push({ id: '#sB', co: coMod, ep: 2, st: 0 })
          }
          if (haz === 2) {
            stor.NomElem = [{ texte: lalettre4, chapo: 0 }]
            stor.listeChange.push({ id: '#sC', co: coMod, ep: 2, st: 0 })
          }
          if (haz === 3) {
            stor.NomElem = [{ texte: lalettre5, chapo: 0 }]
            stor.listeChange.push({ id: '#sD', co: coMod, ep: 2, st: 0 })
          }
        } else if (elt === 'arete_laterale') {
          const haz = j3pGetRandomInt(0, 3)
          if (haz === 0) {
            stor.listeChange.push({ id: '#s1', co: coMod, ep: 4, st: 0 })
            stor.NomElem = [{ texte: '[' + lalettre2 + lalettre1 + ']', chapo: 0 }, { texte: '[' + lalettre1 + lalettre2 + ']', chapo: 0 }]
          }
          if (haz === 1) {
            stor.listeChange.push({ id: '#s2', co: coMod, ep: 4, st: ep2 })
            stor.NomElem = [{ texte: '[' + lalettre3 + lalettre1 + ']', chapo: 0 }, { texte: '[' + lalettre1 + lalettre3 + ']', chapo: 0 }]
          }
          if (haz === 2) {
            stor.listeChange.push({ id: '#s3', co: coMod, ep: 4, st: ep2 })
            stor.NomElem = [{ texte: '[' + lalettre4 + lalettre1 + ']', chapo: 0 }, { texte: '[' + lalettre1 + lalettre4 + ']', chapo: 0 }]
          }
          if (haz === 3) {
            stor.listeChange.push({ id: '#s4', co: coMod, ep: 4, st: 0 })
            stor.NomElem = [{ texte: '[' + lalettre5 + lalettre1 + ']', chapo: 0 }, { texte: '[' + lalettre1 + lalettre5 + ']', chapo: 0 }]
          }
        } else if (elt === 'base') {
          stor.listeChange.push({ id: '#surf2', co: coMod, ep: 4, st: 0 })
          stor.NomElem = [{ texte: lalettre3 + lalettre2 + lalettre5 + lalettre4, chapo: 0 }]
          stor.NomElem.push({ texte: lalettre3 + lalettre4 + lalettre5 + lalettre2, chapo: 0 })
          stor.NomElem.push({ texte: lalettre4 + lalettre5 + lalettre2 + lalettre3, chapo: 0 })
          stor.NomElem.push({ texte: lalettre4 + lalettre3 + lalettre2 + lalettre5, chapo: 0 })
          stor.NomElem.push({ texte: lalettre5 + lalettre2 + lalettre3 + lalettre4, chapo: 0 })
          stor.NomElem.push({ texte: lalettre5 + lalettre4 + lalettre3 + lalettre2, chapo: 0 })
          stor.NomElem.push({ texte: lalettre2 + lalettre3 + lalettre4 + lalettre5, chapo: 0 })
          stor.NomElem.push({ texte: lalettre2 + lalettre5 + lalettre4 + lalettre3, chapo: 0 })
        } else if (elt === 'face') {
          const haz = j3pGetRandomInt(0, 4)
          if (haz === 0) {
            stor.listeChange.push({ id: '#surf2', co: coMod, ep: 4, st: 0 })
            stor.NomElem = [{ texte: lalettre3 + lalettre2 + lalettre5 + lalettre4, chapo: 0 }]
            stor.NomElem.push({ texte: lalettre3 + lalettre4 + lalettre5 + lalettre2, chapo: 0 })
            stor.NomElem.push({ texte: lalettre4 + lalettre5 + lalettre2 + lalettre3, chapo: 0 })
            stor.NomElem.push({ texte: lalettre4 + lalettre3 + lalettre2 + lalettre5, chapo: 0 })
            stor.NomElem.push({ texte: lalettre5 + lalettre2 + lalettre3 + lalettre4, chapo: 0 })
            stor.NomElem.push({ texte: lalettre5 + lalettre4 + lalettre3 + lalettre2, chapo: 0 })
            stor.NomElem.push({ texte: lalettre2 + lalettre3 + lalettre4 + lalettre5, chapo: 0 })
            stor.NomElem.push({ texte: lalettre2 + lalettre5 + lalettre4 + lalettre3, chapo: 0 })
          }
          if (haz === 1) {
            stor.listeChange.push({ id: '#surf1', co: coMod, ep: 4, st: 0 })
            stor.NomElem = [{ texte: lalettre1 + lalettre5 + lalettre2, chapo: 0 }]
            stor.NomElem.push({ texte: lalettre1 + lalettre2 + lalettre5, chapo: 0 })
            stor.NomElem.push({ texte: lalettre2 + lalettre1 + lalettre5, chapo: 0 })
            stor.NomElem.push({ texte: lalettre2 + lalettre5 + lalettre1, chapo: 0 })
            stor.NomElem.push({ texte: lalettre5 + lalettre2 + lalettre1, chapo: 0 })
            stor.NomElem.push({ texte: lalettre5 + lalettre1 + lalettre2, chapo: 0 })
          }
          if (haz === 2) {
            stor.listeChange.push({ id: '#surf3', co: coMod, ep: 4, st: 0 })
            stor.NomElem = [{ texte: lalettre1 + lalettre5 + lalettre4, chapo: 0 }]
            stor.NomElem.push({ texte: lalettre1 + lalettre4 + lalettre5, chapo: 0 })
            stor.NomElem.push({ texte: lalettre4 + lalettre1 + lalettre5, chapo: 0 })
            stor.NomElem.push({ texte: lalettre4 + lalettre5 + lalettre1, chapo: 0 })
            stor.NomElem.push({ texte: lalettre5 + lalettre4 + lalettre1, chapo: 0 })
            stor.NomElem.push({ texte: lalettre5 + lalettre1 + lalettre4, chapo: 0 })
          }
          if (haz === 3) {
            stor.listeChange.push({ id: '#surf4', co: coMod, ep: 4, st: 0 })
            stor.NomElem = [{ texte: lalettre1 + lalettre3 + lalettre4, chapo: 0 }]
            stor.NomElem.push({ texte: lalettre1 + lalettre4 + lalettre3, chapo: 0 })
            stor.NomElem.push({ texte: lalettre4 + lalettre1 + lalettre3, chapo: 0 })
            stor.NomElem.push({ texte: lalettre4 + lalettre3 + lalettre1, chapo: 0 })
            stor.NomElem.push({ texte: lalettre3 + lalettre4 + lalettre1, chapo: 0 })
            stor.NomElem.push({ texte: lalettre3 + lalettre1 + lalettre4, chapo: 0 })
          }
          if (haz === 4) {
            stor.listeChange.push({ id: '#surf5', co: coMod, ep: 4, st: 0 })
            stor.NomElem = [{ texte: lalettre1 + lalettre3 + lalettre2, chapo: 0 }]
            stor.NomElem.push({ texte: lalettre1 + lalettre2 + lalettre3, chapo: 0 })
            stor.NomElem.push({ texte: lalettre2 + lalettre1 + lalettre3, chapo: 0 })
            stor.NomElem.push({ texte: lalettre2 + lalettre3 + lalettre1, chapo: 0 })
            stor.NomElem.push({ texte: lalettre3 + lalettre2 + lalettre1, chapo: 0 })
            stor.NomElem.push({ texte: lalettre3 + lalettre1 + lalettre2, chapo: 0 })
          }
        } else if (elt === 'face_laterale') {
          const haz = j3pGetRandomInt(0, 3)
          if (haz === 1) {
            stor.listeChange.push({ id: '#surf1', co: coMod, ep: 4, st: 0 })
            stor.NomElem = [{ texte: lalettre1 + lalettre5 + lalettre2, chapo: 0 }]
            stor.NomElem.push({ texte: lalettre1 + lalettre2 + lalettre5, chapo: 0 })
            stor.NomElem.push({ texte: lalettre2 + lalettre1 + lalettre5, chapo: 0 })
            stor.NomElem.push({ texte: lalettre2 + lalettre5 + lalettre1, chapo: 0 })
            stor.NomElem.push({ texte: lalettre5 + lalettre2 + lalettre1, chapo: 0 })
            stor.NomElem.push({ texte: lalettre5 + lalettre1 + lalettre2, chapo: 0 })
          }
          if (haz === 2) {
            stor.listeChange.push({ id: '#surf3', co: coMod, ep: 4, st: 0 })
            stor.NomElem = [{ texte: lalettre1 + lalettre5 + lalettre4, chapo: 0 }]
            stor.NomElem.push({ texte: lalettre1 + lalettre4 + lalettre5, chapo: 0 })
            stor.NomElem.push({ texte: lalettre4 + lalettre1 + lalettre5, chapo: 0 })
            stor.NomElem.push({ texte: lalettre4 + lalettre5 + lalettre1, chapo: 0 })
            stor.NomElem.push({ texte: lalettre5 + lalettre4 + lalettre1, chapo: 0 })
            stor.NomElem.push({ texte: lalettre5 + lalettre1 + lalettre4, chapo: 0 })
          }
          if (haz === 3) {
            stor.listeChange.push({ id: '#surf4', co: coMod, ep: 4, st: 0 })
            stor.NomElem = [{ texte: lalettre1 + lalettre3 + lalettre4, chapo: 0 }]
            stor.NomElem.push({ texte: lalettre1 + lalettre4 + lalettre3, chapo: 0 })
            stor.NomElem.push({ texte: lalettre4 + lalettre1 + lalettre3, chapo: 0 })
            stor.NomElem.push({ texte: lalettre4 + lalettre3 + lalettre1, chapo: 0 })
            stor.NomElem.push({ texte: lalettre3 + lalettre4 + lalettre1, chapo: 0 })
            stor.NomElem.push({ texte: lalettre3 + lalettre1 + lalettre4, chapo: 0 })
          }
          if (haz === 0) {
            stor.listeChange.push({ id: '#surf5', co: coMod, ep: 4, st: 0 })
            stor.NomElem = [{ texte: lalettre1 + lalettre3 + lalettre2, chapo: 0 }]
            stor.NomElem.push({ texte: lalettre1 + lalettre2 + lalettre3, chapo: 0 })
            stor.NomElem.push({ texte: lalettre2 + lalettre1 + lalettre3, chapo: 0 })
            stor.NomElem.push({ texte: lalettre2 + lalettre3 + lalettre1, chapo: 0 })
            stor.NomElem.push({ texte: lalettre3 + lalettre2 + lalettre1, chapo: 0 })
            stor.NomElem.push({ texte: lalettre3 + lalettre1 + lalettre2, chapo: 0 })
          }
        } else if (elt === 'face_cachee') {
          if (ep1 === 0) {
            const haz = j3pGetRandomInt(0, 2)
            if (haz === 0) {
              stor.listeChange.push({ id: '#surf3', co: coMod, ep: 4, st: 0 })
              stor.NomElem = [{ texte: lalettre1 + lalettre5 + lalettre4, chapo: 0 }]
              stor.NomElem.push({ texte: lalettre1 + lalettre4 + lalettre5, chapo: 0 })
              stor.NomElem.push({ texte: lalettre4 + lalettre1 + lalettre5, chapo: 0 })
              stor.NomElem.push({ texte: lalettre4 + lalettre5 + lalettre1, chapo: 0 })
              stor.NomElem.push({ texte: lalettre5 + lalettre4 + lalettre1, chapo: 0 })
              stor.NomElem.push({ texte: lalettre5 + lalettre1 + lalettre4, chapo: 0 })
            }
            if (haz === 1) {
              stor.listeChange.push({ id: '#surf4', co: coMod, ep: 4, st: 0 })
              stor.NomElem = [{ texte: lalettre1 + lalettre3 + lalettre4, chapo: 0 }]
              stor.NomElem.push({ texte: lalettre1 + lalettre4 + lalettre3, chapo: 0 })
              stor.NomElem.push({ texte: lalettre4 + lalettre1 + lalettre3, chapo: 0 })
              stor.NomElem.push({ texte: lalettre4 + lalettre3 + lalettre1, chapo: 0 })
              stor.NomElem.push({ texte: lalettre3 + lalettre4 + lalettre1, chapo: 0 })
              stor.NomElem.push({ texte: lalettre3 + lalettre1 + lalettre4, chapo: 0 })
            }
            if (haz === 2) {
              stor.listeChange.push({ id: '#surf5', co: coMod, ep: 4, st: 0 })
              stor.NomElem = [{ texte: lalettre1 + lalettre3 + lalettre2, chapo: 0 }]
              stor.NomElem.push({ texte: lalettre1 + lalettre2 + lalettre3, chapo: 0 })
              stor.NomElem.push({ texte: lalettre2 + lalettre1 + lalettre3, chapo: 0 })
              stor.NomElem.push({ texte: lalettre2 + lalettre3 + lalettre1, chapo: 0 })
              stor.NomElem.push({ texte: lalettre3 + lalettre2 + lalettre1, chapo: 0 })
              stor.NomElem.push({ texte: lalettre3 + lalettre1 + lalettre2, chapo: 0 })
            }
          } else {
            if (j3pGetRandomBool()) {
              stor.listeChange.push({ id: '#surf2', co: coMod, ep: 4, st: 0 })
              stor.NomElem = [{ texte: lalettre3 + lalettre2 + lalettre5 + lalettre4, chapo: 0 }]
              stor.NomElem.push({ texte: lalettre3 + lalettre4 + lalettre5 + lalettre2, chapo: 0 })
              stor.NomElem.push({ texte: lalettre4 + lalettre5 + lalettre2 + lalettre3, chapo: 0 })
              stor.NomElem.push({ texte: lalettre4 + lalettre3 + lalettre2 + lalettre5, chapo: 0 })
              stor.NomElem.push({ texte: lalettre5 + lalettre2 + lalettre3 + lalettre4, chapo: 0 })
              stor.NomElem.push({ texte: lalettre5 + lalettre4 + lalettre3 + lalettre2, chapo: 0 })
              stor.NomElem.push({ texte: lalettre2 + lalettre3 + lalettre4 + lalettre5, chapo: 0 })
              stor.NomElem.push({ texte: lalettre2 + lalettre5 + lalettre4 + lalettre3, chapo: 0 })
            } else {
              stor.listeChange.push({ id: '#surf1', co: coMod, ep: 4, st: 0 })
              stor.NomElem = [{ texte: lalettre1 + lalettre5 + lalettre2, chapo: 0 }]
              stor.NomElem.push({ texte: lalettre1 + lalettre2 + lalettre5, chapo: 0 })
              stor.NomElem.push({ texte: lalettre2 + lalettre1 + lalettre5, chapo: 0 })
              stor.NomElem.push({ texte: lalettre2 + lalettre5 + lalettre1, chapo: 0 })
              stor.NomElem.push({ texte: lalettre5 + lalettre2 + lalettre1, chapo: 0 })
              stor.NomElem.push({ texte: lalettre5 + lalettre1 + lalettre2, chapo: 0 })
            }
          }
        } else if (elt === 'face_visible') {
          if (ep1 !== 0) {
            const haz = j3pGetRandomInt(0, 2)
            if (haz === 0) {
              stor.listeChange.push({ id: '#surf3', co: coMod, ep: 4, st: 0 })
              stor.NomElem = [{ texte: lalettre1 + lalettre5 + lalettre4, chapo: 0 }]
              stor.NomElem.push({ texte: lalettre1 + lalettre4 + lalettre5, chapo: 0 })
              stor.NomElem.push({ texte: lalettre4 + lalettre1 + lalettre5, chapo: 0 })
              stor.NomElem.push({ texte: lalettre4 + lalettre5 + lalettre1, chapo: 0 })
              stor.NomElem.push({ texte: lalettre5 + lalettre4 + lalettre1, chapo: 0 })
              stor.NomElem.push({ texte: lalettre5 + lalettre1 + lalettre4, chapo: 0 })
            }
            if (haz === 1) {
              stor.listeChange.push({ id: '#surf4', co: coMod, ep: 4, st: 0 })
              stor.NomElem = [{ texte: lalettre1 + lalettre3 + lalettre4, chapo: 0 }]
              stor.NomElem.push({ texte: lalettre1 + lalettre4 + lalettre3, chapo: 0 })
              stor.NomElem.push({ texte: lalettre4 + lalettre1 + lalettre3, chapo: 0 })
              stor.NomElem.push({ texte: lalettre4 + lalettre3 + lalettre1, chapo: 0 })
              stor.NomElem.push({ texte: lalettre3 + lalettre4 + lalettre1, chapo: 0 })
              stor.NomElem.push({ texte: lalettre3 + lalettre1 + lalettre4, chapo: 0 })
            }
            if (haz === 2) {
              stor.listeChange.push({ id: '#surf5', co: coMod, ep: 4, st: 0 })
              stor.NomElem = [{ texte: lalettre1 + lalettre3 + lalettre2, chapo: 0 }]
              stor.NomElem.push({ texte: lalettre1 + lalettre2 + lalettre3, chapo: 0 })
              stor.NomElem.push({ texte: lalettre2 + lalettre1 + lalettre3, chapo: 0 })
              stor.NomElem.push({ texte: lalettre2 + lalettre3 + lalettre1, chapo: 0 })
              stor.NomElem.push({ texte: lalettre3 + lalettre2 + lalettre1, chapo: 0 })
              stor.NomElem.push({ texte: lalettre3 + lalettre1 + lalettre2, chapo: 0 })
            }
          } else {
            if (j3pGetRandomBool()) {
              stor.listeChange.push({ id: '#surf2', co: coMod, ep: 4, st: 0 })
              stor.NomElem = [{ texte: lalettre3 + lalettre2 + lalettre5 + lalettre4, chapo: 0 }]
              stor.NomElem.push({ texte: lalettre3 + lalettre4 + lalettre5 + lalettre2, chapo: 0 })
              stor.NomElem.push({ texte: lalettre4 + lalettre5 + lalettre2 + lalettre3, chapo: 0 })
              stor.NomElem.push({ texte: lalettre4 + lalettre3 + lalettre2 + lalettre5, chapo: 0 })
              stor.NomElem.push({ texte: lalettre5 + lalettre2 + lalettre3 + lalettre4, chapo: 0 })
              stor.NomElem.push({ texte: lalettre5 + lalettre4 + lalettre3 + lalettre2, chapo: 0 })
              stor.NomElem.push({ texte: lalettre2 + lalettre3 + lalettre4 + lalettre5, chapo: 0 })
              stor.NomElem.push({ texte: lalettre2 + lalettre5 + lalettre4 + lalettre3, chapo: 0 })
            } else {
              stor.listeChange.push({ id: '#surf1', co: coMod, ep: 4, st: 0 })
              stor.NomElem = [{ texte: lalettre1 + lalettre5 + lalettre2, chapo: 0 }]
              stor.NomElem.push({ texte: lalettre1 + lalettre2 + lalettre5, chapo: 0 })
              stor.NomElem.push({ texte: lalettre2 + lalettre1 + lalettre5, chapo: 0 })
              stor.NomElem.push({ texte: lalettre2 + lalettre5 + lalettre1, chapo: 0 })
              stor.NomElem.push({ texte: lalettre5 + lalettre2 + lalettre1, chapo: 0 })
              stor.NomElem.push({ texte: lalettre5 + lalettre1 + lalettre2, chapo: 0 })
            }
          }
        } else if (elt === 'arete_cachee') {
          if (ep1 === 0) {
            if (j3pGetRandomBool()) {
              stor.listeChange.push({ id: '#s2', co: coMod, ep: 4, st: ep2 })
              stor.NomElem = [{ texte: '[' + lalettre3 + lalettre1 + ']', chapo: 0 }, { texte: '[' + lalettre1 + lalettre3 + ']', chapo: 0 }]
            } else {
              stor.listeChange.push({ id: '#s3', co: coMod, ep: 4, st: ep2 })
              stor.NomElem = [{ texte: '[' + lalettre4 + lalettre1 + ']', chapo: 0 }, { texte: '[' + lalettre1 + lalettre4 + ']', chapo: 0 }]
            }
          } else {
            stor.listeChange.push({ id: '#b1', co: coMod, ep: 4, st: ep1 })
            stor.NomElem = [{ texte: '[' + lalettre5 + lalettre2 + ']', chapo: 0 }, { texte: '[' + lalettre2 + lalettre5 + ']', chapo: 0 }]
          }
        } else if (elt === 'arete_visible') {
          if (ep1 !== 0) {
            if (j3pGetRandomBool()) {
              stor.listeChange.push({ id: '#s2', co: coMod, ep: 4, st: ep2 })
              stor.NomElem = [{ texte: '[' + lalettre3 + lalettre1 + ']', chapo: 0 }, { texte: '[' + lalettre1 + lalettre3 + ']', chapo: 0 }]
            } else {
              stor.listeChange.push({ id: '#s3', co: coMod, ep: 4, st: ep2 })
              stor.NomElem = [{ texte: '[' + lalettre4 + lalettre1 + ']', chapo: 0 }, { texte: '[' + lalettre1 + lalettre4 + ']', chapo: 0 }]
            }
          } else {
            stor.listeChange.push({ id: '#b1', co: coMod, ep: 4, st: ep1 })
            stor.NomElem = [{ texte: '[' + lalettre5 + lalettre2 + ']', chapo: 0 }, { texte: '[' + lalettre2 + lalettre5 + ']', chapo: 0 }]
          }
        }

        if (elt === 'longueur') {
          stor.NomElem = [{ texte: lalettre1 + lalettre2, chapo: 0 }, { texte: lalettre2 + lalettre1, chapo: 0 }]
          stor.NomNeedElem.push({ texte: '[' + lalettre1 + lalettre2 + ']', chapo: 0 })
        } else {
          stor.listCache.push('#bb1', '#bb2', '#bb3', '#bb4', '#bb5')
        }

        stor.NomContenant = {
          texte: lalettre1 + lalettre2 + lalettre3 + lalettre4 + lalettre5,
          chapo: 0
        }
        break
      } // PYRAMIDE

      case 'CYLINDRE_REVOLUTION':
        stor.height = 450
        stor.lwidth = 500
        lalettre1 = addMaj(nomspris)
        lalettre2 = addMaj(nomspris)
        lalettre3 = addMaj(nomspris)
        lalettre4 = addMaj(nomspris)
        lalettre5 = addMaj(nomspris)
        lalettre6 = addMaj(nomspris)
        A1 = j3pGetRandomInt(0, 360)
        A2 = j3pGetRandomInt(6, 11)
        A3 = A4 = 0
        listeNom = [{ t: '#np2', nom: lalettre2 }, { t: '#np3', nom: lalettre3 }, { t: '#np4', nom: lalettre4 }, { t: '#np5', nom: lalettre5 }, { t: '#np6', nom: lalettre6 }]

        if (elt === 'rayon') {
          if (j3pGetRandomBool()) {
            stor.NomElem = [{ texte: '[' + lalettre2 + lalettre6 + ']', chapo: 0 }, { texte: '[' + lalettre6 + lalettre2 + ']', chapo: 0 }]
            stor.listeChange.push({ id: '#ray3', co: coMod, ep: 4, st: 2 })
          } else {
            stor.NomElem = [{ texte: '[' + lalettre5 + lalettre4 + ']', chapo: 0 }, { texte: '[' + lalettre4 + lalettre5 + ']', chapo: 0 }]
            stor.listeChange.push({ id: '#ray4', co: coMod, ep: 4, st: 0 })
          }
        } else if (elt === 'diametre') {
          stor.NomElem = [{ texte: '[' + lalettre1 + lalettre3 + ']', chapo: 0 }, { texte: '[' + lalettre3 + lalettre1 + ']', chapo: 0 }]
          stor.listeChange.push({ id: '#ray1', co: coMod, ep: 4, st: 2 })
          stor.listeChange.push({ id: '#ray2', co: coMod, ep: 4, st: 2 })
        }

        if (elt === 'le_rayon') {
          stor.NomElem = [{ texte: lalettre5 + lalettre4, chapo: 0 }, { texte: lalettre4 + lalettre5, chapo: 0 }]
        } else {
          stor.listCache.push('#rr1', '#rr2', '#rr3', '#rr4', '#rr5')
        }

        if (elt === 'hauteur') {
          stor.listCache.push('#PP1', '#np1')
          stor.NomElem = [{ texte: lalettre6 + lalettre5, chapo: 0 }, { texte: lalettre5 + lalettre6, chapo: 0 }, { texte: lalettre3 + lalettre4, chapo: 0 }, { texte: lalettre4 + lalettre3, chapo: 0 }]
        } else {
          listeNom.push({ t: '#np1', nom: lalettre1 })
          stor.listCache.push('#aa1', '#aa2', '#aa3', '#aa4', '#aa5')
        }

        if (elt === 'le_diametre') {
          stor.NomElem = [{ texte: lalettre1 + lalettre3, chapo: 0 }, { texte: lalettre3 + lalettre1, chapo: 0 }]
        } else {
          stor.listCache.push('#dd1', '#dd2', '#dd3', '#dd4', '#dd5')
        }

        if (elt === 'base') {
          if (j3pGetRandomBool()) {
            stor.listeChange.push({ id: '#base2', co: coMod, ep: 4, st: 0 })
          } else {
            stor.listeChange.push({ id: '#base1', co: coMod, ep: 4, st: 0 })
          }
          stor.NomElem = [{ texte: 'violette', chapo: 0 }]
        } else if (elt === 'face') {
          const haz = j3pGetRandomInt(0, 2)
          if (haz === 0) {
            stor.listeChange.push({ id: '#base2', co: coMod, ep: 4, st: 0 })
          }
          if (haz === 1) {
            stor.listeChange.push({ id: '#base1', co: coMod, ep: 4, st: 0 })
          }
          if (haz === 2) {
            stor.listeChange.push({ id: '#lat1', co: coMod, ep: 4, st: 0 })
            stor.listeChange.push({ id: '#lat2', co: coMod, ep: 4, st: 0 })
          }
          stor.NomElem = [{ texte: 'violette', chapo: 0 }]
        } else if (elt === 'face_laterale') {
          stor.listeChange.push({ id: '#lat1', co: coMod, ep: 4, st: 0 })
          stor.listeChange.push({ id: '#lat2', co: coMod, ep: 4, st: 0 })
          stor.NomElem = [{ texte: 'violette', chapo: 0 }]
        } else if (elt === 'face_cachee') {
          stor.listeChange.push({ id: '#base2', co: coMod, ep: 4, st: 0 })
          stor.NomElem = [{ texte: 'violette', chapo: 0 }]
        } else if (elt === 'face_visible') {
          stor.listeChange.push({ id: '#base1', co: coMod, ep: 4, st: 0 })
          stor.NomElem = [{ texte: 'violette', chapo: 0 }]
        }
        break

      case 'SEGMENT': {
        const lalettre1 = addMaj(nomspris)
        const lalettre2 = addMaj(nomspris)
        const lalettre3 = addMaj(nomspris)
        const lalettreA = addMaj(nomspris)
        const lalettreB = addMaj(nomspris)
        const lalettreC = addMaj(nomspris)
        A1 = j3pGetRandomInt(0, 359)
        listeNom = [{ t: '#np1', nom: lalettre1 }, { t: '#np2', nom: lalettre2 }, { t: '#np3', nom: lalettre3 }, { t: '#npA', nom: lalettreA }, { t: '#npB', nom: lalettreB }, { t: '#npC', nom: lalettreC }]
        stor.NomElem = []
        if (elt === 'extremites') {
          if (j3pGetRandomBool()) {
            stor.listeChange.push({ id: '#s1', co: coMod, ep: 2, st: 0 })
            stor.NomElem = [{ texte: lalettre1, chapo: 0 }]
          } else {
            stor.listeChange.push({ id: '#s3', co: coMod, ep: 2, st: 0 })
            stor.NomElem = [{ texte: lalettre3, chapo: 0 }]
          }
        } else if (elt === 'milieu') {
          stor.listeChange.push({ id: '#s2', co: coMod, ep: 2, st: 0 })
          stor.NomElem = [{ texte: lalettre2, chapo: 0 }]
        } else if (elt === 'mediatrice') {
          stor.listeChange.push({ id: '#dmed', co: coMod, ep: 4, st: 0 })
          stor.NomElem = [{ texte: '(' + lalettre2 + lalettreB + ')', chapo: 0 }, { texte: '(' + lalettreB + lalettre2 + ')', chapo: 0 }]
        }

        if (elt === 'longueur') {
          stor.listCache.push('#dmed', '#d2', '#d3', '#pp1', '#pp2')
          listeNom.push({ t: '#npL', nom: '8 cm' })
          stor.NomElem.push({ texte: lalettre1 + lalettre3, chapo: 0 })
          stor.NomElem.push({ texte: lalettre3 + lalettre1, chapo: 0 })
        } else {
          stor.listCache.push('#npL', '#sl1', '#sl2', '#vl1', '#vl2')
        }

        stor.NomContenant = { texte: '[' + lalettre1 + lalettre3 + ']', chapo: 0 }
        break
      } // SEGMENT

      case 'DEMI_DROITE':
        lalettre1 = addMaj(nomspris)
        lalettre2 = addMaj(nomspris)
        A1 = j3pGetRandomInt(0, 359)
        listeNom = [{ t: '#np1', nom: lalettre1 }, { t: '#np3', nom: lalettre2 }]
        stor.NomElem.push({ texte: lalettre1, chapo: 0 })
        stor.NomContenant = { texte: '[' + lalettre1 + lalettre2 + ')', chapo: 0 }
        if (elt === 'origine') {
          stor.listeChange.push({ id: '#s1', co: coMod, ep: 2, st: 0 })
          stor.NomElem = [{ texte: lalettre1, chapo: 0 }]
        }
        break

      case 'QUADRILATERE': // det les 4 points
        A1 = j3pGetRandomInt(0, 359)
        A2 = j3pGetRandomInt(20, 30) / 10
        A3 = j3pGetRandomInt(35, 45) / 10
        lalettre1 = addMaj(nomspris)
        lalettre2 = addMaj(nomspris)
        lalettre3 = addMaj(nomspris)
        lalettre4 = addMaj(nomspris)
        lalettre5 = addMaj(nomspris)
        lalettre6 = addMaj(nomspris)
        listeNom = [{ t: '#np1', nom: lalettre1 }, { t: '#npF', nom: lalettre2 }, { t: '#npC', nom: lalettre3 }, { t: '#npE', nom: lalettre4 }, { t: '#npM', nom: lalettre5 }]

        if (elt === 'angle_oppose') {
          stor.NomElem = [{ texte: lalettre3 + lalettre2 + lalettre1, chapo: 1 }, { texte: lalettre1 + lalettre2 + lalettre3, chapo: 1 }]
          stor.listeChange.push({ id: '#as2', co: coMod, ep: 4, st: 0 })
          stor.listeChange.push({ id: '#a2', co: coMod, ep: 4, st: 0 })
          stor.listeChange.push({ id: '#as4', co: coMod2, ep: 4, st: 0 })
          stor.listeChange.push({ id: '#a4', co: coMod2, ep: 4, st: 0 })
          stor.NomNeedElem.push({ texte: lalettre1 + lalettre4 + lalettre3, chapo: 1 })
        } else if (elt === 'angle') {
          stor.NomElem = [{ texte: lalettre3 + lalettre2 + lalettre1, chapo: 1 }, { texte: lalettre1 + lalettre2 + lalettre3, chapo: 1 }]
          stor.listeChange.push({ id: '#as2', co: coMod, ep: 4, st: 0 })
          stor.listeChange.push({ id: '#a2', co: coMod, ep: 4, st: 0 })
        } else if (elt === 'diagonale') {
          stor.NomElem = [{ texte: '[' + lalettre3 + lalettre1 + ']', chapo: 0 }, { texte: '[' + lalettre1 + lalettre3 + ']', chapo: 0 }]
          stor.listeChange.push({ id: '#ss1', co: coMod, ep: 4, st: 0 })
          stor.listeChange.push({ id: '#ss2', co: coMod, ep: 4, st: 0 })
        } else if (elt === 'cote_oppose') {
          stor.NomElem = [{ texte: '[' + lalettre2 + lalettre1 + ']', chapo: 0 }, { texte: '[' + lalettre1 + lalettre2 + ']', chapo: 0 }]
          stor.listeChange.push({ id: '#seg1', co: coMod, ep: 4, st: 0 })
          stor.listeChange.push({ id: '#seg3', co: coMod2, ep: 4, st: 0 })
          stor.NomNeedElem.push({ texte: '[' + lalettre3 + lalettre4 + ']', chapo: 0 }, { texte: '[' + lalettre4 + lalettre3 + ']', chapo: 0 })
        } else if (elt === 'cote_consecutif') {
          stor.NomElem = [{ texte: '[' + lalettre2 + lalettre1 + ']', chapo: 0 }, { texte: '[' + lalettre1 + lalettre2 + ']', chapo: 0 }]
          stor.listeChange.push({ id: '#seg1', co: coMod, ep: 4, st: 0 })
          stor.listeChange.push({ id: '#seg2', co: coMod2, ep: 4, st: 0 })
          stor.NomNeedElem.push({ texte: '[' + lalettre3 + lalettre2 + ']', chapo: 0 }, { texte: '[' + lalettre2 + lalettre3 + ']', chapo: 0 })
        } else if (elt === 'angle_consecutif') {
          stor.NomElem = [{ texte: lalettre3 + lalettre2 + lalettre1, chapo: 1 }, { texte: lalettre1 + lalettre2 + lalettre3, chapo: 1 }]
          stor.listeChange.push({ id: '#as2', co: coMod, ep: 4, st: 0 })
          stor.listeChange.push({ id: '#a2', co: coMod, ep: 4, st: 0 })
          stor.listeChange.push({ id: '#as1', co: coMod2, ep: 4, st: 0 })
          stor.listeChange.push({ id: '#a1', co: coMod2, ep: 4, st: 0 })
          stor.NomNeedElem.push({ texte: lalettre2 + lalettre1 + lalettre4, chapo: 1 })
        } else if (elt === 'sommet') {
          stor.NomElem = [{ texte: lalettre4, chapo: 0 }]
          stor.listeChange.push({ id: '#sE', co: coMod, ep: 2, st: 0 })
        } else if (elt === 'sommet_oppose') {
          stor.NomElem = [{ texte: lalettre4, chapo: 0 }]
          stor.listeChange.push({ id: '#sE', co: coMod, ep: 2, st: 0 })
          stor.listeChange.push({ id: '#sF', co: coMod2, ep: 4, st: 0 })
          stor.NomNeedElem.push({ texte: lalettre2, chapo: 0 })
        } else if (elt === 'sommet_consecutif') {
          stor.NomElem = [{ texte: lalettre4, chapo: 0 }]
          stor.listeChange.push({ id: '#sE', co: coMod, ep: 2, st: 0 })
          stor.listeChange.push({ id: '#s1', co: coMod2, ep: 4, st: 0 })
          stor.NomNeedElem.push({ texte: lalettre1, chapo: 0 })
        } else if (elt === 'cote') {
          stor.NomElem = [{ texte: '[' + lalettre2 + lalettre1 + ']', chapo: 0 }, { texte: '[' + lalettre1 + lalettre2 + ']', chapo: 0 }]
          stor.listeChange.push({ id: '#seg1', co: coMod, ep: 4, st: 0 })
        }

        if (elt === 'longueur') {
          stor.NomElem = [{ texte: lalettre1 + lalettre4, chapo: 0 }, { texte: lalettre4 + lalettre1, chapo: 0 }]
          stor.NomNeedElem.push({ texte: '[' + lalettre1 + lalettre4 + ']', chapo: 0 })
        } else {
          stor.listCache.push('#aa1', '#aa2', '#aa3', '#aa4', '#aa5')
        }

        stor.NomContenant = {
          texte: lalettre1 + lalettre2 + lalettre3 + lalettre4,
          chapo: 0
        }
        break

      case 'PARALLELOGRAMME':
        lalettre1 = addMaj(nomspris)
        lalettre2 = addMaj(nomspris)
        lalettre3 = addMaj(nomspris)
        lalettre4 = addMaj(nomspris)
        lalettre5 = addMaj(nomspris)
        lalettre6 = addMaj(nomspris)
        A1 = j3pGetRandomInt(1, 360)
        A2 = j3pGetRandomInt(30, 75)
        A3 = j3pGetRandomInt(25, 30) / 10
        listeNom = [{ t: '#npD', nom: lalettre1 }, { t: '#npF', nom: lalettre2 }, { t: '#npC', nom: lalettre3 }, { t: '#npE', nom: lalettre4 }, { t: '#npO', nom: lalettre5 }]

        if (elt === 'angle_oppose') {
          stor.NomElem = [{ texte: lalettre3 + lalettre2 + lalettre1, chapo: 1 }, { texte: lalettre1 + lalettre2 + lalettre3, chapo: 1 }]
          stor.listeChange.push({ id: '#as2', co: coMod, ep: 4, st: 0 })
          stor.listeChange.push({ id: '#a2', co: coMod, ep: 4, st: 0 })
          stor.listeChange.push({ id: '#as4', co: coMod2, ep: 4, st: 0 })
          stor.listeChange.push({ id: '#a4', co: coMod2, ep: 4, st: 0 })
          stor.NomNeedElem.push({ texte: lalettre1 + lalettre4 + lalettre3, chapo: 1 })
        } else if (elt === 'angle') {
          stor.NomElem = [{ texte: lalettre3 + lalettre2 + lalettre1, chapo: 1 }, { texte: lalettre1 + lalettre2 + lalettre3, chapo: 1 }]
          stor.listeChange.push({ id: '#as2', co: coMod, ep: 4, st: 0 })
          stor.listeChange.push({ id: '#a2', co: coMod, ep: 4, st: 0 })
        } else if (elt === 'diagonale') {
          stor.NomElem = [{ texte: '[' + lalettre3 + lalettre1 + ']', chapo: 0 }, { texte: '[' + lalettre1 + lalettre3 + ']', chapo: 0 }]
          stor.listeChange.push({ id: '#mdiag2', co: coMod, ep: 4, st: 0 })
        } else if (elt === 'cote_oppose') {
          stor.NomElem = [{ texte: '[' + lalettre2 + lalettre1 + ']', chapo: 0 }, { texte: '[' + lalettre1 + lalettre2 + ']', chapo: 0 }]
          stor.listeChange.push({ id: '#seg1', co: coMod, ep: 4, st: 0 })
          stor.listeChange.push({ id: '#seg3', co: coMod2, ep: 4, st: 0 })
          stor.NomNeedElem.push({ texte: '[' + lalettre3 + lalettre4 + ']', chapo: 0 }, { texte: '[' + lalettre4 + lalettre3 + ']', chapo: 0 })
        } else if (elt === 'cote_consecutif') {
          stor.NomElem = [{ texte: '[' + lalettre2 + lalettre1 + ']', chapo: 0 }, { texte: '[' + lalettre1 + lalettre2 + ']', chapo: 0 }]
          stor.listeChange.push({ id: '#seg1', co: coMod, ep: 4, st: 0 })
          stor.listeChange.push({ id: '#seg2', co: coMod2, ep: 4, st: 0 })
          stor.NomNeedElem.push({ texte: '[' + lalettre3 + lalettre2 + ']', chapo: 0 }, { texte: '[' + lalettre2 + lalettre3 + ']', chapo: 0 })
        } else if (elt === 'angle_consecutif') {
          stor.NomElem = [{ texte: lalettre3 + lalettre2 + lalettre1, chapo: 1 }, { texte: lalettre1 + lalettre2 + lalettre3, chapo: 1 }]
          stor.listeChange.push({ id: '#as2', co: coMod, ep: 4, st: 0 })
          stor.listeChange.push({ id: '#a2', co: coMod, ep: 4, st: 0 })
          stor.listeChange.push({ id: '#as1', co: coMod2, ep: 4, st: 0 })
          stor.listeChange.push({ id: '#a1', co: coMod2, ep: 4, st: 0 })
          stor.NomNeedElem.push({ texte: lalettre2 + lalettre3 + lalettre4, chapo: 1 })
        } else if (elt === 'sommet') {
          stor.NomElem = [{ texte: lalettre4, chapo: 0 }]
          stor.listeChange.push({ id: '#sE', co: coMod, ep: 2, st: 0 })
        } else if (elt === 'sommet_oppose') {
          stor.NomElem = [{ texte: lalettre4, chapo: 0 }]
          stor.listeChange.push({ id: '#sE', co: coMod, ep: 2, st: 0 })
          stor.listeChange.push({ id: '#sF', co: coMod2, ep: 4, st: 0 })
          stor.NomNeedElem.push({ texte: lalettre2, chapo: 0 })
        } else if (elt === 'sommet_consecutif') {
          stor.NomElem = [{ texte: lalettre4, chapo: 0 }]
          stor.listeChange.push({ id: '#sE', co: coMod, ep: 2, st: 0 })
          stor.listeChange.push({ id: '#sD', co: coMod2, ep: 4, st: 0 })
          stor.NomNeedElem.push({ texte: lalettre1, chapo: 0 })
        } else if (elt === 'cote') {
          stor.NomElem = [{ texte: '[' + lalettre2 + lalettre1 + ']', chapo: 0 }, { texte: '[' + lalettre1 + lalettre2 + ']', chapo: 0 }]
          stor.listeChange.push({ id: '#seg1', co: coMod, ep: 4, st: 0 })
        }

        if (elt === 'longueur') {
          stor.NomElem = [{ texte: lalettre1 + lalettre4, chapo: 0 }, { texte: lalettre4 + lalettre1, chapo: 0 }]
          stor.NomNeedElem.push({ texte: '[' + lalettre1 + lalettre4 + ']', chapo: 0 })
        } else {
          stor.listCache.push('#aa1', '#aa2', '#aa3', '#aa4', '#aa5')
        }

        if (elt === 'centre') {
          stor.NomElem = [{ texte: lalettre5, chapo: 0 }]
          stor.listeChange.push({ id: '#sCentre', co: coMod, ep: 2, st: 0 })
        }

        if (elt === 'hauteur') {
          stor.NomElem = [{
            texte: '(' + lalettre1 + lalettre6 + ')',
            chapo: 0
          }, {
            texte: '(' + lalettre6 + lalettre1 + ')',
            chapo: 0
          }]
          listeNom.push({ t: '#hh4', nom: lalettre6 })
        } else {
          stor.listCache.push('#hh1', '#hh2', '#hh3', '#hh4')
        }

        stor.NomContenant = {
          texte: lalettre1 + lalettre2 + lalettre3 + lalettre4,
          chapo: 0
        }
        break // PARALLELOGRAMME

      case 'LOSANGE':
        A1 = j3pGetRandomInt(1, 360)
        A2 = 90
        A3 = j3pGetRandomInt(20, 30) / 10

        lalettre1 = addMaj(nomspris)
        lalettre2 = addMaj(nomspris)
        lalettre3 = addMaj(nomspris)
        lalettre4 = addMaj(nomspris)
        lalettre5 = addMaj(nomspris)
        listeNom = [{ t: '#npD', nom: lalettre1 }, { t: '#npF', nom: lalettre2 }, { t: '#npC', nom: lalettre3 }, { t: '#npE', nom: lalettre4 }, { t: '#npO', nom: lalettre5 }]

        if (elt === 'angle_oppose') {
          stor.NomElem = [{ texte: lalettre3 + lalettre2 + lalettre1, chapo: 1 }, { texte: lalettre1 + lalettre2 + lalettre3, chapo: 1 }]
          stor.listeChange.push({ id: '#as2', co: coMod, ep: 4, st: 0 })
          stor.listeChange.push({ id: '#a2', co: coMod, ep: 4, st: 0 })
          stor.listeChange.push({ id: '#as4', co: coMod2, ep: 4, st: 0 })
          stor.listeChange.push({ id: '#a4', co: coMod2, ep: 4, st: 0 })
          stor.NomNeedElem.push({ texte: lalettre1 + lalettre4 + lalettre3, chapo: 1 })
        } else if (elt === 'angle') {
          stor.NomElem = [{ texte: lalettre3 + lalettre2 + lalettre1, chapo: 1 }, { texte: lalettre1 + lalettre2 + lalettre3, chapo: 1 }]
          stor.listeChange.push({ id: '#as2', co: coMod, ep: 4, st: 0 })
          stor.listeChange.push({ id: '#a2', co: coMod, ep: 4, st: 0 })
        } else if (elt === 'diagonale') {
          stor.NomElem = [{ texte: '[' + lalettre3 + lalettre1 + ']', chapo: 0 }, { texte: '[' + lalettre1 + lalettre3 + ']', chapo: 0 }]
          stor.listeChange.push({ id: '#mdiag2', co: coMod, ep: 4, st: 0 })
        } else if (elt === 'cote_oppose') {
          stor.NomElem = [{ texte: '[' + lalettre2 + lalettre1 + ']', chapo: 0 }, { texte: '[' + lalettre1 + lalettre2 + ']', chapo: 0 }]
          stor.listeChange.push({ id: '#seg1', co: coMod, ep: 4, st: 0 })
          stor.listeChange.push({ id: '#seg3', co: coMod2, ep: 4, st: 0 })
          stor.NomNeedElem.push({ texte: '[' + lalettre3 + lalettre4 + ']', chapo: 0 }, { texte: '[' + lalettre4 + lalettre3 + ']', chapo: 0 })
        } else if (elt === 'cote_consecutif') {
          stor.NomElem = [{ texte: '[' + lalettre2 + lalettre1 + ']', chapo: 0 }, { texte: '[' + lalettre1 + lalettre2 + ']', chapo: 0 }]
          stor.listeChange.push({ id: '#seg1', co: coMod, ep: 4, st: 0 })
          stor.listeChange.push({ id: '#seg2', co: coMod2, ep: 4, st: 0 })
          stor.NomNeedElem.push({ texte: '[' + lalettre3 + lalettre2 + ']', chapo: 0 }, { texte: '[' + lalettre2 + lalettre3 + ']', chapo: 0 })
        } else if (elt === 'angle_consecutif') {
          stor.NomElem = [{ texte: lalettre3 + lalettre2 + lalettre1, chapo: 1 }, { texte: lalettre1 + lalettre2 + lalettre3, chapo: 1 }]
          stor.listeChange.push({ id: '#as2', co: coMod, ep: 4, st: 0 })
          stor.listeChange.push({ id: '#a2', co: coMod, ep: 4, st: 0 })
          stor.listeChange.push({ id: '#as1', co: coMod2, ep: 4, st: 0 })
          stor.listeChange.push({ id: '#a1', co: coMod2, ep: 4, st: 0 })
          stor.NomNeedElem.push({ texte: lalettre4 + lalettre3 + lalettre2, chapo: 1 })
        } else if (elt === 'sommet') {
          stor.NomElem = [{ texte: lalettre4, chapo: 0 }]
          stor.listeChange.push({ id: '#sE', co: coMod, ep: 2, st: 0 })
        } else if (elt === 'sommet_oppose') {
          stor.NomElem = [{ texte: lalettre4, chapo: 0 }]
          stor.listeChange.push({ id: '#sE', co: coMod, ep: 2, st: 0 })
          stor.listeChange.push({ id: '#sF', co: coMod2, ep: 4, st: 0 })
          stor.NomNeedElem.push({ texte: lalettre2, chapo: 0 })
        } else if (elt === 'sommet_consecutif') {
          stor.NomElem = [{ texte: lalettre4, chapo: 0 }]
          stor.listeChange.push({ id: '#sE', co: coMod, ep: 2, st: 0 })
          stor.listeChange.push({ id: '#sD', co: coMod2, ep: 4, st: 0 })
          stor.NomNeedElem.push({ texte: lalettre1, chapo: 0 })
        } else if (elt === 'cote') {
          stor.NomElem = [{ texte: '[' + lalettre2 + lalettre1 + ']', chapo: 0 }, { texte: '[' + lalettre1 + lalettre2 + ']', chapo: 0 }]
          stor.listeChange.push({ id: '#seg1', co: coMod, ep: 4, st: 0 })
        }

        if (elt === 'longueur') {
          stor.NomElem = [{ texte: lalettre1 + lalettre4, chapo: 0 }, { texte: lalettre4 + lalettre1, chapo: 0 }]
          stor.NomNeedElem.push({ texte: '[' + lalettre1 + lalettre4 + ']', chapo: 0 })
        } else {
          stor.listCache.push('#aa1', '#aa2', '#aa3', '#aa4', '#aa5')
        }

        if (elt === 'centre') {
          stor.NomElem = [{ texte: lalettre5, chapo: 0 }]
          stor.listeChange.push({ id: '#sCentre', co: coMod, ep: 2, st: 0 })
        }

        stor.NomContenant = {
          texte: lalettre1 + lalettre2 + lalettre3 + lalettre4,
          chapo: 0
        }
        break // LOSANGE

      case 'CARRE':
        A1 = j3pGetRandomInt(1, 260)
        A2 = 5.09
        A3 = A2
        lalettre1 = addMaj(nomspris)
        lalettre2 = addMaj(nomspris)
        lalettre3 = addMaj(nomspris)
        lalettre4 = addMaj(nomspris)
        lalettre5 = addMaj(nomspris)
        listeNom = [{ t: '#np1', nom: lalettre1 }, { t: '#npF', nom: lalettre2 }, { t: '#npC', nom: lalettre3 }, { t: '#npE', nom: lalettre4 }, { t: '#npM', nom: lalettre5 }]
        if (elt === 'angle_oppose') {
          stor.NomElem = [{ texte: lalettre3 + lalettre2 + lalettre1, chapo: 1 }, { texte: lalettre1 + lalettre2 + lalettre3, chapo: 1 }]
          stor.listeChange.push({ id: '#as2', co: coMod, ep: 4, st: 0 })
          stor.listeChange.push({ id: '#a2', co: coMod, ep: 4, st: 0 })
          stor.listeChange.push({ id: '#as4', co: coMod2, ep: 4, st: 0 })
          stor.listeChange.push({ id: '#a4', co: coMod2, ep: 4, st: 0 })
          stor.NomNeedElem.push({ texte: lalettre1 + lalettre4 + lalettre3, chapo: 1 })
        } else if (elt === 'angle') {
          stor.NomElem = [{ texte: lalettre3 + lalettre2 + lalettre1, chapo: 1 }, { texte: lalettre1 + lalettre2 + lalettre3, chapo: 1 }]
          stor.listeChange.push({ id: '#as2', co: coMod, ep: 4, st: 0 })
          stor.listeChange.push({ id: '#a2', co: coMod, ep: 4, st: 0 })
        } else if (elt === 'diagonale') {
          stor.NomElem = [{ texte: '[' + lalettre3 + lalettre1 + ']', chapo: 0 }, { texte: '[' + lalettre1 + lalettre3 + ']', chapo: 0 }]
          stor.listeChange.push({ id: '#dia1', co: coMod, ep: 4, st: 0 })
          stor.listeChange.push({ id: '#dia2', co: coMod, ep: 4, st: 0 })
        } else if (elt === 'cote_oppose') {
          stor.NomElem = [{ texte: '[' + lalettre2 + lalettre1 + ']', chapo: 0 }, { texte: '[' + lalettre1 + lalettre2 + ']', chapo: 0 }]
          stor.listeChange.push({ id: '#seg1', co: coMod, ep: 4, st: 0 })
          stor.listeChange.push({ id: '#seg3', co: coMod2, ep: 4, st: 0 })
          stor.NomNeedElem.push({ texte: '[' + lalettre3 + lalettre4 + ']', chapo: 0 }, { texte: '[' + lalettre4 + lalettre3 + ']', chapo: 0 })
        } else if (elt === 'cote_consecutif') {
          stor.NomElem = [{ texte: '[' + lalettre2 + lalettre1 + ']', chapo: 0 }, { texte: '[' + lalettre1 + lalettre2 + ']', chapo: 0 }]
          stor.listeChange.push({ id: '#seg1', co: coMod, ep: 4, st: 0 })
          stor.listeChange.push({ id: '#seg2', co: coMod2, ep: 4, st: 0 })
          stor.NomNeedElem.push({ texte: '[' + lalettre3 + lalettre2 + ']', chapo: 0 }, { texte: '[' + lalettre2 + lalettre3 + ']', chapo: 0 })
        } else if (elt === 'angle_consecutif') {
          stor.NomElem = [{ texte: lalettre3 + lalettre2 + lalettre1, chapo: 1 }, { texte: lalettre1 + lalettre2 + lalettre3, chapo: 1 }]
          stor.listeChange.push({ id: '#as2', co: coMod, ep: 4, st: 0 })
          stor.listeChange.push({ id: '#a2', co: coMod, ep: 4, st: 0 })
          stor.listeChange.push({ id: '#as1', co: coMod2, ep: 4, st: 0 })
          stor.listeChange.push({ id: '#a1', co: coMod2, ep: 4, st: 0 })
          stor.NomNeedElem.push({ texte: lalettre4 + lalettre1 + lalettre2, chapo: 1 })
        } else if (elt === 'sommet') {
          stor.NomElem = [{ texte: lalettre4, chapo: 0 }]
          stor.listeChange.push({ id: '#sE', co: coMod, ep: 2, st: 0 })
        } else if (elt === 'sommet_oppose') {
          stor.NomElem = [{ texte: lalettre4, chapo: 0 }]
          stor.listeChange.push({ id: '#sE', co: coMod, ep: 2, st: 0 })
          stor.listeChange.push({ id: '#sF', co: coMod2, ep: 4, st: 0 })
          stor.NomNeedElem.push({ texte: lalettre2, chapo: 0 })
        } else if (elt === 'sommet_consecutif') {
          stor.NomElem = [{ texte: lalettre4, chapo: 0 }]
          stor.listeChange.push({ id: '#sE', co: coMod, ep: 2, st: 0 })
          stor.listeChange.push({ id: '#s1', co: coMod2, ep: 4, st: 0 })
          stor.NomNeedElem.push({ texte: lalettre1, chapo: 0 })
        } else if (elt === 'cote') {
          stor.NomElem = [{ texte: '[' + lalettre2 + lalettre1 + ']', chapo: 0 }, { texte: '[' + lalettre1 + lalettre2 + ']', chapo: 0 }]
          stor.listeChange.push({ id: '#seg1', co: coMod, ep: 4, st: 0 })
        }

        if (elt === 'longueur') {
          stor.NomElem = [{ texte: lalettre3 + lalettre4, chapo: 0 }, { texte: lalettre4 + lalettre3, chapo: 0 }]
          stor.NomNeedElem.push({ texte: '[' + lalettre3 + lalettre4 + ']', chapo: 0 })
        } else {
          stor.listCache.push('#aa1', '#aa2', '#aa3', '#aa4', '#aa5')
        }

        if (elt === 'centre') {
          stor.NomElem = [{ texte: lalettre5, chapo: 0 }]
          stor.listeChange.push({ id: '#sM', co: coMod, ep: 2, st: 0 })
        }

        stor.NomContenant = {
          texte: lalettre1 + lalettre2 + lalettre3 + lalettre4,
          chapo: 0
        }
        break // CARRE

      case 'RECTANGLE':
        lalettre1 = addMaj(nomspris)
        lalettre2 = addMaj(nomspris)
        lalettre3 = addMaj(nomspris)
        lalettre4 = addMaj(nomspris)
        lalettre5 = addMaj(nomspris)
        A1 = j3pGetRandomInt(1, 260)
        A2 = j3pGetRandomInt(20, 40) / 10
        A3 = A2
        listeNom = [{ t: '#np1', nom: lalettre1 }, { t: '#npF', nom: lalettre2 }, { t: '#npC', nom: lalettre3 }, { t: '#npE', nom: lalettre4 }, { t: '#npO', nom: lalettre5 }]

        if (elt === 'angle_oppose') {
          stor.NomElem = [{ texte: lalettre3 + lalettre2 + lalettre1, chapo: 1 }, { texte: lalettre1 + lalettre2 + lalettre3, chapo: 1 }]
          stor.listeChange.push({ id: '#as2', co: coMod, ep: 4, st: 0 })
          stor.listeChange.push({ id: '#a2', co: coMod, ep: 4, st: 0 })
          stor.listeChange.push({ id: '#as4', co: coMod2, ep: 4, st: 0 })
          stor.listeChange.push({ id: '#a4', co: coMod2, ep: 4, st: 0 })
          stor.NomNeedElem.push({ texte: lalettre1 + lalettre4 + lalettre3, chapo: 1 })
        } else if (elt === 'angle') {
          stor.NomElem = [{ texte: lalettre3 + lalettre2 + lalettre1, chapo: 1 }, { texte: lalettre1 + lalettre2 + lalettre3, chapo: 1 }]
          stor.listeChange.push({ id: '#as2', co: coMod, ep: 4, st: 0 })
          stor.listeChange.push({ id: '#a2', co: coMod, ep: 4, st: 0 })
        } else if (elt === 'diagonale') {
          stor.NomElem = [{ texte: '[' + lalettre3 + lalettre1 + ']', chapo: 0 }, { texte: '[' + lalettre1 + lalettre3 + ']', chapo: 0 }]
          stor.listeChange.push({ id: '#diaad', co: coMod, ep: 4, st: 0 })
        } else if (elt === 'cote_oppose') {
          stor.NomElem = [{ texte: '[' + lalettre2 + lalettre1 + ']', chapo: 0 }, { texte: '[' + lalettre1 + lalettre2 + ']', chapo: 0 }]
          stor.listeChange.push({ id: '#seg1', co: coMod, ep: 4, st: 0 })
          stor.listeChange.push({ id: '#seg3', co: coMod2, ep: 4, st: 0 })
          stor.NomNeedElem.push({ texte: '[' + lalettre3 + lalettre4 + ']', chapo: 0 }, { texte: '[' + lalettre4 + lalettre3 + ']', chapo: 0 })
        } else if (elt === 'cote_consecutif') {
          stor.NomElem = [{ texte: '[' + lalettre2 + lalettre1 + ']', chapo: 0 }, { texte: '[' + lalettre1 + lalettre2 + ']', chapo: 0 }]
          stor.listeChange.push({ id: '#seg1', co: coMod, ep: 4, st: 0 })
          stor.listeChange.push({ id: '#seg2', co: coMod2, ep: 4, st: 0 })
          stor.NomNeedElem.push({ texte: '[' + lalettre3 + lalettre2 + ']', chapo: 0 }, { texte: '[' + lalettre2 + lalettre3 + ']', chapo: 0 })
        } else if (elt === 'angle_consecutif') {
          stor.NomElem = [{ texte: lalettre3 + lalettre2 + lalettre1, chapo: 1 }, { texte: lalettre1 + lalettre2 + lalettre3, chapo: 1 }]
          stor.listeChange.push({ id: '#as2', co: coMod, ep: 4, st: 0 })
          stor.listeChange.push({ id: '#a2', co: coMod, ep: 4, st: 0 })
          stor.listeChange.push({ id: '#as1', co: coMod2, ep: 4, st: 0 })
          stor.listeChange.push({ id: '#a1', co: coMod2, ep: 4, st: 0 })
          stor.NomNeedElem.push({ texte: lalettre4 + lalettre1 + lalettre2, chapo: 1 })
        } else if (elt === 'sommet') {
          stor.NomElem = [{ texte: lalettre4, chapo: 0 }]
          stor.listeChange.push({ id: '#sE', co: coMod, ep: 2, st: 0 })
        } else if (elt === 'sommet_oppose') {
          stor.NomElem = [{ texte: lalettre4, chapo: 0 }]
          stor.listeChange.push({ id: '#sE', co: coMod, ep: 2, st: 0 })
          stor.listeChange.push({ id: '#sF', co: coMod2, ep: 4, st: 0 })
          stor.NomNeedElem.push({ texte: lalettre2, chapo: 0 })
        } else if (elt === 'sommet_consecutif') {
          stor.NomElem = [{ texte: lalettre4, chapo: 0 }]
          stor.listeChange.push({ id: '#sE', co: coMod, ep: 2, st: 0 })
          stor.listeChange.push({ id: '#s1', co: coMod2, ep: 4, st: 0 })
          stor.NomNeedElem.push({ texte: lalettre1, chapo: 0 })
        } else if (elt === 'cote') {
          stor.NomElem = [{ texte: '[' + lalettre2 + lalettre1 + ']', chapo: 0 }, { texte: '[' + lalettre1 + lalettre2 + ']', chapo: 0 }]
          stor.listeChange.push({ id: '#seg1', co: coMod, ep: 4, st: 0 })
        }

        if (elt === 'longueur') {
          stor.NomElem = [{ texte: lalettre1 + lalettre4, chapo: 0 }, { texte: lalettre4 + lalettre1, chapo: 0 }]
          stor.NomNeedElem.push({ texte: '[' + lalettre1 + lalettre4 + ']', chapo: 0 })
        } else {
          stor.listCache.push('#aa1', '#aa2', '#aa3', '#aa4', '#aa5')
        }

        if (elt === 'centre') {
          stor.NomElem = [{ texte: lalettre5, chapo: 0 }]
          stor.listeChange.push({ id: '#sM', co: coMod, ep: 2, st: 0 })
        }

        stor.NomContenant = {
          texte: lalettre1 + lalettre2 + lalettre3 + lalettre4,
          chapo: 0
        }
        break

      case 'CERCLE':
        A2 = j3pGetRandomInt(40, 130)
        A1 = j3pGetRandomInt(0, 359)
        lalettre1 = addMaj(nomspris)
        A3 = A4 = 0
        lalettre2 = addMaj(nomspris)
        lalettre3 = addMaj(nomspris)
        lalettre4 = addMaj(nomspris)
        listeNom = [{ t: '#npD', nom: lalettre4 }, { t: '#npCentre', nom: lalettre1 }, { t: '#npE1', nom: lalettre2 }, { t: '#npE2', nom: lalettre3 }]

        if (elt === 'le_rayon') {
          stor.NomElem = [{ texte: lalettre1 + lalettre4, chapo: 0 }, { texte: lalettre4 + lalettre1, chapo: 0 }]
        } else {
          stor.listCache.push('#aa1', '#aa2', '#aa3', '#aa4')
        }

        if (elt === 'le_diametre') {
          stor.NomElem = [{ texte: lalettre3 + lalettre4, chapo: 0 }, { texte: lalettre4 + lalettre3, chapo: 0 }]
        } else {
          stor.listCache.push('#bb1', '#bb2', '#bb3', '#bb4')
        }

        if (elt !== 'le_diametre' && elt !== 'le_rayon') {
          stor.listCache.push('#aa5')
        }

        if (elt === 'diametre') {
          stor.NomElem = [{ texte: '[' + lalettre3 + lalettre4 + ']', chapo: 0 }, { texte: '[' + lalettre4 + lalettre3 + ']', chapo: 0 }]
          stor.listeChange.push({ id: '#c2', co: coMod, ep: 4, st: 0 })
          stor.listeChange.push({ id: '#c3', co: coMod, ep: 4, st: 0 })
        } else if (elt === 'rayon') {
          stor.NomElem = [{ texte: '[' + lalettre1 + lalettre2 + ']', chapo: 0 }, { texte: '[' + lalettre2 + lalettre1 + ']', chapo: 0 }]
          stor.listeChange.push({ id: '#c1', co: coMod, ep: 4, st: 0 })
        } else if (elt === 'corde') {
          stor.NomElem = [{ texte: '[' + lalettre4 + lalettre2 + ']', chapo: 0 }, { texte: '[' + lalettre2 + lalettre4 + ']', chapo: 0 }]
          stor.listeChange.push({ id: '#sCorde', co: coMod, ep: 4, st: 0 })
        } else if (elt === 'centre') {
          stor.NomElem = [{ texte: lalettre1, chapo: 0 }]
          stor.listeChange.push({ id: '#sCentre', co: coMod, ep: 2, st: 0 })
        }
        break

      case 'ARC':
        A1 = j3pGetRandomInt(0, 359)
        // on veut un nombre entre 100 et 160 ou entre 240 et 330
        A2 = j3pGetRandomBool()
          ? j3pGetRandomInt(100, 160)
          : j3pGetRandomInt(240, 330)
        lalettre1 = addMaj(nomspris)
        A3 = A4 = 0
        lalettre2 = addMaj(nomspris)
        lalettre3 = addMaj(nomspris)
        listeNom = [{ t: '#npCentre2', nom: lalettre1 }, { t: '#npE1', nom: lalettre2 }, { t: '#npE2', nom: lalettre3 }]

        if (elt === 'centre') {
          stor.NomElem = [{ texte: lalettre1, chapo: 0 }]
          stor.listeChange.push({ id: '#sCentre', co: coMod, ep: 2, st: 0 })
        } else if (elt === 'extremites') {
          stor.NomElem = [{ texte: lalettre2, chapo: 0 }]
          stor.listeChange.push({ id: '#sE1', co: coMod, ep: 2, st: 0 })
        }
        stor.NomContenant = { texte: lalettre2 + lalettre3, chapo: 2 }
        break

      case 'SECTEUR':// chope le centre
        A1 = j3pGetRandomInt(0, 359)
        // on veut un nombre entre 100 et 160 ou entre 240 et 330
        A2 = j3pGetRandomBool()
          ? j3pGetRandomInt(100, 160)
          : j3pGetRandomInt(240, 330)
        A3 = 0
        A4 = 0
        lalettre1 = addMaj(nomspris)
        lalettre2 = addMaj(nomspris)
        lalettre3 = addMaj(nomspris)
        if (A2 > 180) {
          stor.listCache.push('#a2', '#as2', '#c4')
        } else {
          stor.listCache.push('#a0', '#a1', '#as0', '#as1', '#c3')
        }
        listeNom = [{ t: '#npCentre1', nom: lalettre1 }, { t: '#npE1', nom: lalettre2 }, { t: '#npE2', nom: lalettre3 }]

        if (elt === 'centre') {
          stor.NomElem = [{ texte: lalettre1, chapo: 0 }]
          stor.listeChange.push({ id: '#sCentre', co: coMod, ep: 2, st: 0 })
        } else if (elt === 'angle') {
          stor.NomElem = [{ texte: lalettre3 + lalettre1 + lalettre2, chapo: 1 }, { texte: lalettre2 + lalettre1 + lalettre3, chapo: 1 }]
          stor.listeChange.push({ id: '#as2', co: coMod, ep: 4, st: 0 })
          stor.listeChange.push({ id: '#as1', co: coMod, ep: 4, st: 0 })
          stor.listeChange.push({ id: '#a2', co: coMod, ep: 4, st: 0 })
          stor.listeChange.push({ id: '#a1', co: coMod, ep: 4, st: 0 })
          stor.listeChange.push({ id: '#as0', co: coMod, ep: 4, st: 0 })
          stor.listeChange.push({ id: '#a0', co: coMod, ep: 4, st: 0 })
        } else if (elt === 'rayon') {
          stor.NomElem = [{ texte: '[' + lalettre2 + lalettre1 + ']', chapo: 0 }, { texte: '[' + lalettre1 + lalettre2 + ']', chapo: 0 }]
          stor.listeChange.push({ id: '#c1', co: coMod, ep: 4, st: 0 })
        }

        if (elt === 'le_rayon') {
          stor.NomElem = [{ texte: lalettre1 + lalettre2, chapo: 0 }, { texte: lalettre2 + lalettre1, chapo: 0 }]
        } else {
          stor.listCache.push('#aa1', '#aa2', '#aa3', '#aa4', '#aa5')
        }
        break

      case 'ANGLE':
        A2 = j3pGetRandomInt(100, 179)
        A3 = 0
        A1 = j3pGetRandomInt(0, 359)
        lalettre1 = addMaj(nomspris)
        lalettre2 = addMaj(nomspris)
        lalettre3 = addMaj(nomspris)
        lalettre4 = addMaj(nomspris)

        if (elt === 'sommet') {
          stor.listeChange.push({ id: '#sCentre', co: coMod, ep: 2, st: 0 })
          stor.NomElem = [{ texte: lalettre1, chapo: 0 }]
        } else if (elt === 'cote') {
          if (j3pGetRandomBool()) {
            stor.listeChange.push({ id: '#dd1', co: coMod, ep: 4, st: 0 })
            stor.NomElem = [{ texte: '[' + lalettre1 + lalettre2 + ')', chapo: 0 }, { texte: '(' + lalettre2 + lalettre1 + ']', chapo: 0 }]
          } else {
            stor.listeChange.push({ id: '#dd2', co: coMod, ep: 4, st: 0 })
            stor.NomElem = [{ texte: '[' + lalettre1 + lalettre3 + ')', chapo: 0 }, { texte: '(' + lalettre3 + lalettre1 + ']', chapo: 0 }]
          }
        } else if (elt === 'bissectrice') {
          stor.listeChange.push({ id: '#axe', co: coMod, ep: 4, st: 0 })
          stor.NomElem = [{ texte: '(' + lalettre1 + lalettre4 + ')', chapo: 0 }, { texte: '(' + lalettre4 + lalettre1 + ')', chapo: 0 }]
        }

        listeNom = [{ t: '#npB', nom: lalettre4 }, { t: '#npCentre', nom: lalettre1 }, { t: '#npE1', nom: lalettre2 }, { t: '#npE2', nom: lalettre3 }]

        stor.NomContenant = { texte: lalettre2 + lalettre1 + lalettre3, chapo: 1 }
        break

      case 'TRIANGLE':
        A1 = j3pGetRandomInt(1, 260)
        do {
          A2 = j3pGetRandomInt(30, 65) / 10
          A3 = j3pGetRandomInt(30, 65) / 10
        } while (Math.abs(A2 - A3) < 1 || Math.abs(Math.abs(55 - A2) - Math.abs(55 - A3)) < 1)
        stor.height = 400
        lalettre1 = addMaj(nomspris)
        lalettre2 = addMaj(nomspris)
        lalettre3 = addMaj(nomspris)
        lalettre4 = addMaj(nomspris)
        lalettre5 = addMaj(nomspris)
        lalettre6 = addMaj(nomspris)
        listeNom = [{ t: '#npC', nom: lalettre3 }, { t: '#npA', nom: lalettre1 }, { t: '#npB', nom: lalettre2 }]
        if (elt === 'sommet') {
          stor.NomElem = [{ texte: lalettre1, chapo: 0 }]
          stor.listeChange.push({ id: '#sA', co: coMod, ep: 2, st: 0 })
        } else if (elt === 'cote') {
          stor.NomElem = [{ texte: '[' + lalettre3 + lalettre1 + ']', chapo: 0 }, { texte: '[' + lalettre1 + lalettre3 + ']', chapo: 0 }]
          stor.listeChange.push({ id: '#seg1', co: coMod, ep: 4, st: 0 })
        } else if (elt === 'angle') {
          stor.NomElem = [{ texte: lalettre3 + lalettre1 + lalettre2, chapo: 1 }, { texte: lalettre2 + lalettre1 + lalettre3, chapo: 1 }]
          stor.listeChange.push({ id: '#as2', co: coMod, ep: 4, st: 0 })
          stor.listeChange.push({ id: '#a2', co: coMod, ep: 4, st: 0 })
        } else if (elt === 'sommet_oppose') {
          stor.NomElem = [{ texte: lalettre3, chapo: 0 }]
          stor.listeChange.push({ id: '#sC', co: coMod, ep: 4, st: 0 })
          stor.listeChange.push({ id: '#seg2', co: coMod2, ep: 4, st: 0 })
          stor.NomNeedElem.push({ texte: '[' + lalettre1 + lalettre2 + ']', chapo: 0 })
        } else if (elt === 'cote_oppose') {
          stor.NomElem = [{ texte: '[' + lalettre2 + lalettre1 + ']', chapo: 0 }, { texte: '[' + lalettre1 + lalettre2 + ']', chapo: 0 }]
          stor.listeChange.push({ id: '#sC', co: coMod2, ep: 4, st: 0 })
          stor.listeChange.push({ id: '#seg2', co: coMod, ep: 4, st: 0 })
          stor.NomNeedElem.push({ texte: lalettre3, chapo: 0 })
        }

        if (elt === 'hauteur') {
          stor.NomElem = [{ texte: '(' + lalettre4 + lalettre3 + ')', chapo: 0 }, { texte: '(' + lalettre3 + lalettre4 + ')', chapo: 0 }]
          stor.listeChange.push({ id: '#h1', co: coMod, ep: 4, st: 0 })
          listeNom.push({ t: '#npD', nom: lalettre4 })
        } else {
          stor.listCache.push('#gg1', '#gg2', '#h1', '#npD')
        }

        if (elt === 'mediatrice') {
          stor.NomElem = [{ texte: '(' + lalettre5 + lalettre6 + ')', chapo: 0 }, { texte: '(' + lalettre6 + lalettre5 + ')', chapo: 0 }]
          stor.listeChange.push({ id: '#med', co: coMod, ep: 4, st: 0 })
          listeNom.push({ t: '#npF', nom: lalettre6 })
        } else {
          stor.listCache.push('#uu1', '#uu2', '#med', '#npF')
        }

        if (elt === 'mediane') {
          stor.NomElem = [{ texte: '(' + lalettre5 + lalettre2 + ')', chapo: 0 }, { texte: '(' + lalettre2 + lalettre5 + ')', chapo: 0 }]
          stor.listeChange.push({ id: '#mediane', co: coMod, ep: 4, st: 0 })
        } else {
          stor.listCache.push('#mediane')
        }

        if (elt !== 'mediane' && elt !== 'mediatrice') {
          stor.listCache.push('#ll1', '#ll2', '#npE')
        } else {
          listeNom.push({ t: '#npE', nom: lalettre5 })
        }

        if (elt === 'longueur') {
          stor.NomElem = [{ texte: lalettre1 + lalettre3, chapo: 0 }, { texte: lalettre3 + lalettre1, chapo: 0 }]
          stor.NomNeedElem.push({ texte: '[' + lalettre1 + lalettre3 + ']', chapo: 0 })
        } else {
          stor.listCache.push('#aa1', '#aa2', '#aa3', '#aa4', '#aa5')
        }

        stor.NomContenant = {
          texte: lalettre1 + lalettre2 + lalettre3,
          chapo: 0
        }
        break

      case 'TRIANGLE_RECTANGLE': {
        stor.height = 400
        lalettre1 = addMaj(nomspris)
        lalettre2 = addMaj(nomspris)
        lalettre3 = addMaj(nomspris)
        lalettre4 = addMaj(nomspris)
        lalettre5 = addMaj(nomspris)
        lalettre6 = addMaj(nomspris)
        listeNom = [{ t: '#npF', nom: lalettre6 }, { t: '#npE', nom: lalettre5 }, { t: '#npC', nom: lalettre3 }, { t: '#npA', nom: lalettre1 }, { t: '#npB', nom: lalettre2 }]
        A1 = j3pGetRandomInt(1, 260)
        do {
          A3 = j3pGetRandomInt(35, 65) / 10
        } while (A3 < 5.9 && A3 > 4.7)
        A2 = Math.sqrt(7.2 * 7.2 - A3 * A3)

        if (elt === 'sommet') {
          stor.NomElem = [{ texte: lalettre1, chapo: 0 }]
          stor.listeChange.push({ id: '#sA', co: coMod, ep: 2, st: 0 })
        } else if (elt === 'cote') {
          stor.NomElem = [{ texte: '[' + lalettre3 + lalettre1 + ']', chapo: 0 }, { texte: '[' + lalettre1 + lalettre3 + ']', chapo: 0 }]
          stor.listeChange.push({ id: '#seg1', co: coMod, ep: 4, st: 0 })
        } else if (elt === 'angle') {
          stor.NomElem = [{ texte: lalettre3 + lalettre1 + lalettre2, chapo: 1 }, { texte: lalettre2 + lalettre1 + lalettre3, chapo: 1 }]
          stor.listeChange.push({ id: '#as2', co: coMod, ep: 4, st: 0 })
          stor.listeChange.push({ id: '#a2', co: coMod, ep: 4, st: 0 })
        } else if (elt === 'sommet_oppose') {
          if (j3pGetRandomBool()) {
            stor.NomElem = [{ texte: lalettre3, chapo: 0 }]
            stor.listeChange.push({ id: '#sC', co: coMod, ep: 4, st: 0 })
            stor.listeChange.push({ id: '#seg2', co: coMod2, ep: 4, st: 0 })
            stor.NomNeedElem.push({ texte: '[' + lalettre1 + lalettre2 + ']', chapo: 0 })
          } else {
            stor.NomElem = [{ texte: lalettre2, chapo: 0 }]
            stor.listeChange.push({ id: '#sB', co: coMod, ep: 4, st: 0 })
            stor.listeChange.push({ id: '#seg1', co: coMod2, ep: 4, st: 0 })
            stor.NomNeedElem.push({ texte: '[' + lalettre1 + lalettre3 + ']', chapo: 0 })
          }
        } else if (elt === 'cote_oppose') {
          if (j3pGetRandomBool()) {
            stor.NomElem = [{ texte: '[' + lalettre2 + lalettre1 + ']', chapo: 0 }, { texte: '[' + lalettre1 + lalettre2 + ']', chapo: 0 }]
            stor.listeChange.push({ id: '#sC', co: coMod2, ep: 4, st: 0 })
            stor.listeChange.push({ id: '#seg2', co: coMod, ep: 4, st: 0 })
            stor.NomNeedElem.push({ texte: lalettre3, chapo: 0 })
          } else {
            stor.NomElem = [{ texte: '[' + lalettre3 + lalettre1 + ']', chapo: 0 }, { texte: '[' + lalettre1 + lalettre3 + ']', chapo: 0 }]
            stor.listeChange.push({ id: '#sB', co: coMod2, ep: 4, st: 0 })
            stor.listeChange.push({ id: '#seg1', co: coMod, ep: 4, st: 0 })
            stor.NomNeedElem.push({ texte: lalettre2, chapo: 0 })
          }
        } else if (elt === 'hauteur') {
          if (j3pGetRandomBool()) {
            stor.NomElem = [{ texte: '(' + lalettre4 + lalettre3 + ')', chapo: 0 }, { texte: '(' + lalettre3 + lalettre4 + ')', chapo: 0 }]
            stor.listeChange.push({ id: '#h1', co: coMod, ep: 4, st: 0 })
          } else {
            stor.NomElem = [{ texte: '(' + lalettre1 + lalettre3 + ')', chapo: 0 }, { texte: '(' + lalettre3 + lalettre1 + ')', chapo: 0 }]
            stor.listeChange.push({ id: '#seg1', co: coMod, ep: 4, st: 0 })
            stor.listeChange.push({ id: '#d1', co: coMod, ep: 4, st: 0 })
          }
        }

        if (elt === 'mediatrice') {
          stor.NomElem = [{ texte: '(' + lalettre5 + lalettre6 + ')', chapo: 0 }, { texte: '(' + lalettre6 + lalettre5 + ')', chapo: 0 }]
          stor.listeChange.push({ id: '#med', co: coMod, ep: 4, st: 0 })
          stor.listCache.push('#npD')
        } else {
          listeNom.push({ t: '#npD', nom: lalettre4 })
        }

        if (elt === 'longueur') {
          stor.NomElem = [{ texte: lalettre1 + lalettre3, chapo: 0 }, { texte: lalettre3 + lalettre1, chapo: 0 }]
          stor.NomNeedElem.push({ texte: '[' + lalettre1 + lalettre3 + ']', chapo: 0 })
          stor.listCache.push('#med', '#zae', '#zae2')
        } else {
          stor.listCache.push('#aa1', '#aa2', '#aa3', '#aa4', '#aa5')
        }

        if (elt === 'cote_oppose_angle') {
          stor.NomElem = [{ texte: '[' + lalettre2 + lalettre3 + ']', chapo: 0 }, { texte: '[' + lalettre3 + lalettre2 + ']', chapo: 0 }]
          stor.listeChange.push({ id: '#a2', co: coMod2, ep: 4, st: 0 })
          stor.listeChange.push({ id: '#as2', co: coMod2, ep: 4, st: 0 })
          stor.listeChange.push({ id: '#seg3', co: coMod, ep: 4, st: 0 })
          stor.NomNeedElem.push({ texte: lalettre3 + lalettre1 + lalettre2, chapo: 1 })
        } else if (elt === 'cote_adjacent') {
          stor.NomElem = [{ texte: '[' + lalettre1 + lalettre3 + ']', chapo: 0 }, { texte: '[' + lalettre3 + lalettre1 + ']', chapo: 0 }]
          stor.listeChange.push({ id: '#a2', co: coMod2, ep: 4, st: 0 })
          stor.listeChange.push({ id: '#as2', co: coMod2, ep: 4, st: 0 })
          stor.listeChange.push({ id: '#seg1', co: coMod, ep: 4, st: 0 })
          stor.NomNeedElem.push({ texte: lalettre3 + lalettre1 + lalettre2, chapo: 1 })
        } else if (elt === 'hypotenuse') {
          stor.NomElem = [{ texte: '[' + lalettre1 + lalettre2 + ']', chapo: 0 }, { texte: '[' + lalettre2 + lalettre1 + ']', chapo: 0 }]
          stor.listeChange.push({ id: '#seg2', co: coMod, ep: 4, st: 0 })
        }

        let LeNomDuTruc
        if (j3pGetRandomInt(0, 1) === 0) {
          LeNomDuTruc = lalettre1 + lalettre2 + lalettre3 + ' rectangle en ' + lalettre3
        } else {
          LeNomDuTruc = lalettre2 + lalettre1 + lalettre3 + ' rectangle en ' + lalettre3
        }
        stor.NomContenant = {
          texte: LeNomDuTruc,
          chapo: 0
        }
      }
        break
      case 'TRIANGLE_ISOCELE': {
        lalettre1 = addMaj(nomspris)
        lalettre2 = addMaj(nomspris)
        lalettre3 = addMaj(nomspris)
        lalettre4 = addMaj(nomspris)
        lalettre5 = addMaj(nomspris)
        lalettre6 = addMaj(nomspris)
        lalettre7 = addMaj(nomspris)

        if (elt === 'sommet') {
          stor.NomElem = [{ texte: lalettre1, chapo: 0 }]
          stor.listeChange.push({ id: '#sA', co: coMod, ep: 2, st: 0 })
        } else if (elt === 'cote') {
          stor.NomElem = [{ texte: '[' + lalettre3 + lalettre1 + ']', chapo: 0 }, { texte: '[' + lalettre1 + lalettre3 + ']', chapo: 0 }]
          stor.listeChange.push({ id: '#seg1', co: coMod, ep: 4, st: 0 })
        } else if (elt === 'angle') {
          stor.NomElem = [{ texte: lalettre2 + lalettre3 + lalettre1, chapo: 1 }, { texte: lalettre1 + lalettre3 + lalettre2, chapo: 1 }]
          stor.listeChange.push({ id: '#as1', co: coMod, ep: 4, st: 0 })
          stor.listeChange.push({ id: '#a1', co: coMod, ep: 4, st: 0 })
        } else if (elt === 'sommet_oppose') {
          if (j3pGetRandomBool()) {
            stor.NomElem = [{ texte: lalettre3, chapo: 0 }]
            stor.listeChange.push({ id: '#sC', co: coMod, ep: 4, st: 0 })
            stor.listeChange.push({ id: '#seg2', co: coMod2, ep: 4, st: 0 })
            stor.NomNeedElem.push({ texte: '[' + lalettre1 + lalettre2 + ']', chapo: 0 })
          } else {
            stor.NomElem = [{ texte: lalettre2, chapo: 0 }]
            stor.listeChange.push({ id: '#sB', co: coMod, ep: 4, st: 0 })
            stor.listeChange.push({ id: '#seg1', co: coMod2, ep: 4, st: 0 })
            stor.NomNeedElem.push({ texte: '[' + lalettre1 + lalettre3 + ']', chapo: 0 })
          }
        } else if (elt === 'cote_oppose') {
          if (j3pGetRandomBool()) {
            stor.NomElem = [{ texte: '[' + lalettre2 + lalettre1 + ']', chapo: 0 }, { texte: '[' + lalettre1 + lalettre2 + ']', chapo: 0 }]
            stor.listeChange.push({ id: '#sC', co: coMod2, ep: 4, st: 0 })
            stor.listeChange.push({ id: '#seg2', co: coMod, ep: 4, st: 0 })
            stor.NomNeedElem.push({ texte: lalettre3, chapo: 0 })
          } else {
            stor.NomElem = [{ texte: '[' + lalettre3 + lalettre1 + ']', chapo: 0 }, { texte: '[' + lalettre1 + lalettre3 + ']', chapo: 0 }]
            stor.listeChange.push({ id: '#sB', co: coMod2, ep: 4, st: 0 })
            stor.listeChange.push({ id: '#seg1', co: coMod, ep: 4, st: 0 })
            stor.NomNeedElem.push({ texte: lalettre2, chapo: 0 })
          }
        } else if (elt === 'sommet_principal') {
          stor.NomElem = [{ texte: lalettre3, chapo: 0 }]
          stor.listeChange.push({ id: '#sC', co: coMod, ep: 2, st: 0 })
        } else if (elt === 'base') {
          stor.NomElem = [{ texte: '[' + lalettre2 + lalettre1 + ']', chapo: 0 }, { texte: '[' + lalettre1 + lalettre2 + ']', chapo: 0 }]
          stor.listeChange.push({ id: '#seg2', co: coMod, ep: 4, st: 0 })
        } else if (elt === 'angle_base') {
          stor.NomElem = [{ texte: lalettre2 + lalettre1 + lalettre3, chapo: 1 }, { texte: lalettre3 + lalettre1 + lalettre2, chapo: 1 }]
          stor.listeChange.push({ id: '#as2', co: coMod, ep: 4, st: 0 })
          stor.listeChange.push({ id: '#a2', co: coMod, ep: 4, st: 0 })
        }

        if (elt === 'longueur') {
          stor.NomElem = [{ texte: lalettre1 + lalettre2, chapo: 0 }, { texte: lalettre2 + lalettre1, chapo: 0 }]
          stor.NomNeedElem.push({ texte: '[' + lalettre1 + lalettre2 + ']', chapo: 0 })
        } else {
          stor.listCache.push('#aa1', '#aa2', '#aa3', '#aa4', '#aa5')
        }

        let LeNomDuTruc
        if (j3pGetRandomInt(0, 1) === 0) {
          LeNomDuTruc = lalettre1 + lalettre2 + lalettre3 + ' isocèle en ' + lalettre3
        } else {
          LeNomDuTruc = lalettre2 + lalettre1 + lalettre3 + ' isocèle en ' + lalettre3
        }
        stor.NomContenant = {
          texte: LeNomDuTruc,
          chapo: 0
        }
        A1 = j3pGetRandomInt(1, 260)
        do {
          A2 = j3pGetRandomInt(30, 70) / 10
        } while ((A2 > 4.7 && A2 < 5.5) || (A2 > 5.9 && A2 < 6.5))
        A3 = A2
        listeNom = [{ t: '#npC', nom: lalettre3 }, { t: '#npA', nom: lalettre1 }, { t: '#npB', nom: lalettre2 }]
        break
      } // TRIANGLE_ISOCELE

      case 'TRIANGLE_EQUILATERAL': // det les 4 points
        lalettre1 = addMaj(nomspris)
        lalettre2 = addMaj(nomspris)
        lalettre3 = addMaj(nomspris)
        lalettre4 = addMaj(nomspris)
        lalettre5 = addMaj(nomspris)
        lalettre6 = addMaj(nomspris)
        lalettre7 = addMaj(nomspris)
        A1 = j3pGetRandomInt(1, 260)
        A3 = A2 = 6.24
        listeNom = [{ t: '#npF', nom: lalettre6 }, { t: '#npG', nom: lalettre7 }, { t: '#npC', nom: lalettre3 }, { t: '#npA', nom: lalettre1 }, { t: '#npB', nom: lalettre2 }]

        if (elt === 'sommet') {
          stor.NomElem = [{ texte: lalettre3, chapo: 0 }]
          stor.listeChange.push({ id: '#sC', co: coMod, ep: 2, st: 0 })
        } else if (elt === 'cote') {
          stor.NomElem = [{ texte: '[' + lalettre1 + lalettre3 + ']', chapo: 0 }, { texte: '[' + lalettre3 + lalettre1 + ']', chapo: 0 }]
          stor.listeChange.push({ id: '#seg1', co: coMod, ep: 4, st: 0 })
        }

        if (elt === 'angle') {
          stor.NomElem = [{ texte: lalettre1 + lalettre3 + lalettre2, chapo: 1 }, { texte: lalettre2 + lalettre3 + lalettre1, chapo: 1 }]
          stor.listeChange.push({ id: '#as1', co: coMod, ep: 4, st: 0 })
          stor.listeChange.push({ id: '#a1', co: coMod, ep: 4, st: 0 })
          stor.listCache.push('#npD', '#npE')
        } else {
          listeNom.push({ t: '#npD', nom: lalettre4 }, { t: '#npE', nom: lalettre5 })
        }

        if (elt === 'sommet_oppose') {
          stor.NomElem = [{ texte: lalettre3, chapo: 0 }]
          stor.listeChange.push({ id: '#sC', co: coMod, ep: 4, st: 0 })
          stor.listeChange.push({ id: '#seg2', co: coMod2, ep: 4, st: 0 })
          stor.NomNeedElem.push({ texte: '[' + lalettre1 + lalettre2 + ']', chapo: 0 })
        } else if (elt === 'cote_oppose') {
          stor.NomElem = [{ texte: '[' + lalettre2 + lalettre1 + ']', chapo: 0 }, { texte: '[' + lalettre1 + lalettre2 + ']', chapo: 0 }]
          stor.listeChange.push({ id: '#sC', co: coMod2, ep: 4, st: 0 })
          stor.listeChange.push({ id: '#seg2', co: coMod, ep: 4, st: 0 })
          stor.NomNeedElem.push({ texte: lalettre3, chapo: 0 })
        }

        if (elt === 'longueur') {
          stor.NomElem = [{ texte: lalettre1 + lalettre3, chapo: 0 }, { texte: lalettre3 + lalettre1, chapo: 0 }]
          stor.NomNeedElem.push({ texte: '[' + lalettre1 + lalettre3 + ']', chapo: 0 })
        } else {
          stor.listCache.push('#aa1', '#aa2', '#aa3', '#aa4', '#aa5')
        }

        stor.NomContenant = {
          texte: lalettre2 + lalettre3 + lalettre1,
          chapo: 0
        }
        break

      case 'CUBE': {
        listeNom = []
        stor.height = 320
        stor.lwidth = 400
        A1 = j3pGetRandomInt(0, 359)

        lalettre1 = addMaj(nomspris); lalettre2 = addMaj(nomspris); lalettre3 = addMaj(nomspris); lalettre4 = addMaj(nomspris)
        lalettre5 = addMaj(nomspris); lalettre6 = addMaj(nomspris); lalettre7 = addMaj(nomspris)
        const lalettre8 = addMaj(nomspris)
        listeNom.push({ t: '#np1', nom: lalettre1 })
        listeNom.push({ t: '#np2', nom: lalettre2 })
        listeNom.push({ t: '#np3', nom: lalettre3 })
        listeNom.push({ t: '#np4', nom: lalettre4 })
        listeNom.push({ t: '#np5', nom: lalettre5 })
        listeNom.push({ t: '#np6', nom: lalettre6 })
        listeNom.push({ t: '#np7', nom: lalettre7 })
        listeNom.push({ t: '#np8', nom: lalettre8 })

        if (elt === 'face') {
          const haz = j3pGetRandomInt(0, 5)
          if (haz === 0) {
            stor.listeChange.push({ id: '#surf1', co: coMod, ep: 2, st: 0 })
            stor.NomElem = [{ texte: lalettre1 + lalettre4 + lalettre8 + lalettre6, chapo: 0 }]
            stor.NomElem.push({ texte: lalettre1 + lalettre6 + lalettre8 + lalettre4, chapo: 0 })
            stor.NomElem.push({ texte: lalettre6 + lalettre8 + lalettre4 + lalettre1, chapo: 0 })
            stor.NomElem.push({ texte: lalettre6 + lalettre1 + lalettre4 + lalettre8, chapo: 0 })
            stor.NomElem.push({ texte: lalettre8 + lalettre4 + lalettre1 + lalettre6, chapo: 0 })
            stor.NomElem.push({ texte: lalettre8 + lalettre6 + lalettre1 + lalettre4, chapo: 0 })
            stor.NomElem.push({ texte: lalettre4 + lalettre1 + lalettre6 + lalettre8, chapo: 0 })
            stor.NomElem.push({ texte: lalettre4 + lalettre8 + lalettre6 + lalettre1, chapo: 0 })
          }
          if (haz === 1) {
            stor.listeChange.push({ id: '#surf2', co: coMod, ep: 2, st: 0 })
            stor.NomElem = [{ texte: lalettre3 + lalettre4 + lalettre8 + lalettre7, chapo: 0 }]
            stor.NomElem.push({ texte: lalettre3 + lalettre7 + lalettre8 + lalettre4, chapo: 0 })
            stor.NomElem.push({ texte: lalettre7 + lalettre8 + lalettre4 + lalettre3, chapo: 0 })
            stor.NomElem.push({ texte: lalettre7 + lalettre3 + lalettre4 + lalettre8, chapo: 0 })
            stor.NomElem.push({ texte: lalettre8 + lalettre4 + lalettre3 + lalettre7, chapo: 0 })
            stor.NomElem.push({ texte: lalettre8 + lalettre7 + lalettre3 + lalettre4, chapo: 0 })
            stor.NomElem.push({ texte: lalettre4 + lalettre3 + lalettre7 + lalettre8, chapo: 0 })
            stor.NomElem.push({ texte: lalettre4 + lalettre8 + lalettre7 + lalettre3, chapo: 0 })
          }
          if (haz === 2) {
            stor.listeChange.push({ id: '#surf3', co: coMod, ep: 2, st: 0 })
            stor.NomElem = [{ texte: lalettre5 + lalettre6 + lalettre8 + lalettre7, chapo: 0 }]
            stor.NomElem.push({ texte: lalettre5 + lalettre7 + lalettre8 + lalettre6, chapo: 0 })
            stor.NomElem.push({ texte: lalettre7 + lalettre8 + lalettre6 + lalettre5, chapo: 0 })
            stor.NomElem.push({ texte: lalettre7 + lalettre5 + lalettre6 + lalettre8, chapo: 0 })
            stor.NomElem.push({ texte: lalettre8 + lalettre6 + lalettre5 + lalettre7, chapo: 0 })
            stor.NomElem.push({ texte: lalettre8 + lalettre7 + lalettre5 + lalettre6, chapo: 0 })
            stor.NomElem.push({ texte: lalettre6 + lalettre5 + lalettre7 + lalettre8, chapo: 0 })
            stor.NomElem.push({ texte: lalettre6 + lalettre8 + lalettre7 + lalettre5, chapo: 0 })
          }
          if (haz === 3) {
            stor.listeChange.push({ id: '#surf4', co: coMod, ep: 2, st: 0 })
            stor.NomElem = [{ texte: lalettre1 + lalettre2 + lalettre3 + lalettre4, chapo: 0 }]
            stor.NomElem.push({ texte: lalettre1 + lalettre4 + lalettre3 + lalettre2, chapo: 0 })
            stor.NomElem.push({ texte: lalettre4 + lalettre3 + lalettre2 + lalettre1, chapo: 0 })
            stor.NomElem.push({ texte: lalettre4 + lalettre1 + lalettre2 + lalettre3, chapo: 0 })
            stor.NomElem.push({ texte: lalettre3 + lalettre2 + lalettre1 + lalettre4, chapo: 0 })
            stor.NomElem.push({ texte: lalettre3 + lalettre4 + lalettre1 + lalettre2, chapo: 0 })
            stor.NomElem.push({ texte: lalettre2 + lalettre1 + lalettre4 + lalettre3, chapo: 0 })
            stor.NomElem.push({ texte: lalettre2 + lalettre3 + lalettre4 + lalettre1, chapo: 0 })
          }
          if (haz === 4) {
            stor.listeChange.push({ id: '#surf5', co: coMod, ep: 2, st: 0 })
            stor.NomElem = [{ texte: lalettre1 + lalettre2 + lalettre5 + lalettre6, chapo: 0 }]
            stor.NomElem.push({ texte: lalettre1 + lalettre6 + lalettre5 + lalettre2, chapo: 0 })
            stor.NomElem.push({ texte: lalettre6 + lalettre5 + lalettre2 + lalettre1, chapo: 0 })
            stor.NomElem.push({ texte: lalettre6 + lalettre1 + lalettre2 + lalettre5, chapo: 0 })
            stor.NomElem.push({ texte: lalettre5 + lalettre2 + lalettre1 + lalettre6, chapo: 0 })
            stor.NomElem.push({ texte: lalettre5 + lalettre6 + lalettre1 + lalettre2, chapo: 0 })
            stor.NomElem.push({ texte: lalettre2 + lalettre1 + lalettre6 + lalettre5, chapo: 0 })
            stor.NomElem.push({ texte: lalettre2 + lalettre5 + lalettre6 + lalettre1, chapo: 0 })
          }
          if (haz === 5) {
            stor.listeChange.push({ id: '#surf6', co: coMod, ep: 2, st: 0 })
            stor.NomElem = [{ texte: lalettre3 + lalettre2 + lalettre5 + lalettre7, chapo: 0 }]
            stor.NomElem.push({ texte: lalettre3 + lalettre7 + lalettre5 + lalettre2, chapo: 0 })
            stor.NomElem.push({ texte: lalettre7 + lalettre5 + lalettre2 + lalettre3, chapo: 0 })
            stor.NomElem.push({ texte: lalettre7 + lalettre3 + lalettre2 + lalettre5, chapo: 0 })
            stor.NomElem.push({ texte: lalettre5 + lalettre2 + lalettre3 + lalettre7, chapo: 0 })
            stor.NomElem.push({ texte: lalettre5 + lalettre7 + lalettre3 + lalettre2, chapo: 0 })
            stor.NomElem.push({ texte: lalettre2 + lalettre3 + lalettre7 + lalettre5, chapo: 0 })
            stor.NomElem.push({ texte: lalettre2 + lalettre5 + lalettre7 + lalettre3, chapo: 0 })
          }
        } else if (elt === 'face_cachee') {
          const haz = j3pGetRandomInt(0, 2)
          if (haz === 0) {
            stor.listeChange.push({ id: '#surf4', co: coMod, ep: 2, st: 0 })
            stor.NomElem = [{ texte: lalettre1 + lalettre2 + lalettre3 + lalettre4, chapo: 0 }]
            stor.NomElem.push({ texte: lalettre1 + lalettre4 + lalettre3 + lalettre2, chapo: 0 })
            stor.NomElem.push({ texte: lalettre4 + lalettre3 + lalettre2 + lalettre1, chapo: 0 })
            stor.NomElem.push({ texte: lalettre4 + lalettre1 + lalettre2 + lalettre3, chapo: 0 })
            stor.NomElem.push({ texte: lalettre3 + lalettre2 + lalettre1 + lalettre4, chapo: 0 })
            stor.NomElem.push({ texte: lalettre3 + lalettre4 + lalettre1 + lalettre2, chapo: 0 })
            stor.NomElem.push({ texte: lalettre2 + lalettre1 + lalettre4 + lalettre3, chapo: 0 })
            stor.NomElem.push({ texte: lalettre2 + lalettre3 + lalettre4 + lalettre1, chapo: 0 })
          }
          if (haz === 1) {
            stor.listeChange.push({ id: '#surf1', co: coMod, ep: 2, st: 0 })
            stor.NomElem = [{ texte: lalettre1 + lalettre4 + lalettre8 + lalettre6, chapo: 0 }]
            stor.NomElem.push({ texte: lalettre1 + lalettre6 + lalettre8 + lalettre4, chapo: 0 })
            stor.NomElem.push({ texte: lalettre6 + lalettre8 + lalettre4 + lalettre1, chapo: 0 })
            stor.NomElem.push({ texte: lalettre6 + lalettre1 + lalettre4 + lalettre8, chapo: 0 })
            stor.NomElem.push({ texte: lalettre8 + lalettre4 + lalettre1 + lalettre6, chapo: 0 })
            stor.NomElem.push({ texte: lalettre8 + lalettre6 + lalettre1 + lalettre4, chapo: 0 })
            stor.NomElem.push({ texte: lalettre4 + lalettre1 + lalettre6 + lalettre8, chapo: 0 })
            stor.NomElem.push({ texte: lalettre4 + lalettre8 + lalettre6 + lalettre1, chapo: 0 })
          }
          if (haz === 2) {
            stor.listeChange.push({ id: '#surf2', co: coMod, ep: 2, st: 0 })
            stor.NomElem = [{ texte: lalettre3 + lalettre4 + lalettre8 + lalettre7, chapo: 0 }]
            stor.NomElem.push({ texte: lalettre3 + lalettre7 + lalettre8 + lalettre4, chapo: 0 })
            stor.NomElem.push({ texte: lalettre7 + lalettre8 + lalettre4 + lalettre3, chapo: 0 })
            stor.NomElem.push({ texte: lalettre7 + lalettre3 + lalettre4 + lalettre8, chapo: 0 })
            stor.NomElem.push({ texte: lalettre8 + lalettre4 + lalettre3 + lalettre7, chapo: 0 })
            stor.NomElem.push({ texte: lalettre8 + lalettre7 + lalettre3 + lalettre4, chapo: 0 })
            stor.NomElem.push({ texte: lalettre4 + lalettre3 + lalettre7 + lalettre8, chapo: 0 })
            stor.NomElem.push({ texte: lalettre4 + lalettre8 + lalettre7 + lalettre3, chapo: 0 })
          }
        } else if (elt === 'face_visible') {
          const haz = j3pGetRandomInt(0, 2)
          if (haz === 0) {
            stor.listeChange.push({ id: '#surf3', co: coMod, ep: 2, st: 0 })
            stor.NomElem = [{ texte: lalettre5 + lalettre6 + lalettre8 + lalettre7, chapo: 0 }]
            stor.NomElem.push({ texte: lalettre5 + lalettre7 + lalettre8 + lalettre6, chapo: 0 })
            stor.NomElem.push({ texte: lalettre7 + lalettre8 + lalettre6 + lalettre5, chapo: 0 })
            stor.NomElem.push({ texte: lalettre7 + lalettre5 + lalettre6 + lalettre8, chapo: 0 })
            stor.NomElem.push({ texte: lalettre8 + lalettre6 + lalettre5 + lalettre7, chapo: 0 })
            stor.NomElem.push({ texte: lalettre8 + lalettre7 + lalettre5 + lalettre6, chapo: 0 })
            stor.NomElem.push({ texte: lalettre6 + lalettre5 + lalettre7 + lalettre8, chapo: 0 })
            stor.NomElem.push({ texte: lalettre6 + lalettre8 + lalettre7 + lalettre5, chapo: 0 })
          }
          if (haz === 1) {
            stor.listeChange.push({ id: '#surf6', co: coMod, ep: 2, st: 0 })
            stor.NomElem = [{ texte: lalettre3 + lalettre2 + lalettre5 + lalettre7, chapo: 0 }]
            stor.NomElem.push({ texte: lalettre3 + lalettre7 + lalettre5 + lalettre2, chapo: 0 })
            stor.NomElem.push({ texte: lalettre7 + lalettre5 + lalettre2 + lalettre3, chapo: 0 })
            stor.NomElem.push({ texte: lalettre7 + lalettre3 + lalettre2 + lalettre5, chapo: 0 })
            stor.NomElem.push({ texte: lalettre5 + lalettre2 + lalettre3 + lalettre7, chapo: 0 })
            stor.NomElem.push({ texte: lalettre5 + lalettre7 + lalettre3 + lalettre2, chapo: 0 })
            stor.NomElem.push({ texte: lalettre2 + lalettre3 + lalettre7 + lalettre5, chapo: 0 })
            stor.NomElem.push({ texte: lalettre2 + lalettre5 + lalettre7 + lalettre3, chapo: 0 })
          }
          if (haz === 2) {
            stor.listeChange.push({ id: '#surf5', co: coMod, ep: 2, st: 0 })
            stor.NomElem = [{ texte: lalettre1 + lalettre2 + lalettre5 + lalettre6, chapo: 0 }]
            stor.NomElem.push({ texte: lalettre1 + lalettre6 + lalettre5 + lalettre2, chapo: 0 })
            stor.NomElem.push({ texte: lalettre6 + lalettre5 + lalettre2 + lalettre1, chapo: 0 })
            stor.NomElem.push({ texte: lalettre6 + lalettre1 + lalettre2 + lalettre5, chapo: 0 })
            stor.NomElem.push({ texte: lalettre5 + lalettre2 + lalettre1 + lalettre6, chapo: 0 })
            stor.NomElem.push({ texte: lalettre5 + lalettre6 + lalettre1 + lalettre2, chapo: 0 })
            stor.NomElem.push({ texte: lalettre2 + lalettre1 + lalettre6 + lalettre5, chapo: 0 })
            stor.NomElem.push({ texte: lalettre2 + lalettre5 + lalettre6 + lalettre1, chapo: 0 })
          }
        } else if (elt === 'sommet') {
          const haz = j3pGetRandomInt(0, 2)
          if (haz === 0) {
            stor.NomElem = [{ texte: lalettre1, chapo: 0 }]
            stor.listeChange.push({ id: '#s1', co: coMod, ep: 2, st: 0 })
          }
          if (haz === 1) {
            stor.NomElem = [{ texte: lalettre5, chapo: 0 }]
            stor.listeChange.push({ id: '#s5', co: coMod, ep: 2, st: 0 })
          }
          if (haz === 2) {
            stor.NomElem = [{ texte: lalettre4, chapo: 0 }]
            stor.listeChange.push({ id: '#s4', co: coMod, ep: 2, st: 0 })
          }
        } else if (elt === 'arete') {
          const haz = j3pGetRandomInt(0, 2)
          if (haz === 0) {
            stor.NomElem = [{ texte: '[' + lalettre5 + lalettre2 + ']', chapo: 0 }, { texte: '[' + lalettre2 + lalettre5 + ']', chapo: 0 }]
            stor.listeChange.push({ id: '#seg1', co: coMod, ep: 4, st: 0 })
          }
          if (haz === 1) {
            stor.NomElem = [{ texte: '[' + lalettre3 + lalettre7 + ']', chapo: 0 }, { texte: '[' + lalettre7 + lalettre3 + ']', chapo: 0 }]
            stor.listeChange.push({ id: '#seg3', co: coMod, ep: 4, st: 0 })
          }
          if (haz === 2) {
            stor.NomElem = [{ texte: '[' + lalettre1 + lalettre4 + ']', chapo: 0 }, { texte: '[' + lalettre4 + lalettre1 + ']', chapo: 0 }]
            stor.listeChange.push({ id: '#seg10', co: coMod, ep: 4, st: 2 })
          }
        } else if (elt === 'arete_cachee') {
          stor.NomElem = [{ texte: '[' + lalettre1 + lalettre4 + ']', chapo: 0 }, { texte: '[' + lalettre4 + lalettre1 + ']', chapo: 0 }]
          stor.listeChange.push({ id: '#seg10', co: coMod, ep: 4, st: 2 })
        } else if (elt === 'arete_visible') {
          if (j3pGetRandomBool()) {
            stor.NomElem = [{ texte: '[' + lalettre5 + lalettre2 + ']', chapo: 0 }, { texte: '[' + lalettre2 + lalettre5 + ']', chapo: 0 }]
            stor.listeChange.push({ id: '#seg1', co: coMod, ep: 4, st: 0 })
          } else {
            stor.NomElem = [{ texte: '[' + lalettre3 + lalettre7 + ']', chapo: 0 }, { texte: '[' + lalettre7 + lalettre3 + ']', chapo: 0 }]
            stor.listeChange.push({ id: '#seg3', co: coMod, ep: 4, st: 0 })
          }
        }

        if (elt === 'longueur') {
          stor.NomElem = [{ texte: lalettre1 + lalettre6, chapo: 0 }, { texte: lalettre6 + lalettre1, chapo: 0 }]
          stor.NomNeedElem.push({ texte: '[' + lalettre1 + lalettre6 + ']', chapo: 0 })
        } else {
          stor.listCache.push('#aa1', '#aa2', '#aa3', '#aa4', '#aa5')
        }

        stor.NomContenant = {
          texte: lalettre1 + lalettre2 + lalettre3 + lalettre4 + lalettre6 + lalettre5 + lalettre7 + lalettre8,
          chapo: 0
        }
        break
      } // CUBE

      case 'PAVEDROIT': {
        stor.height = stor.lwidth = 400
        A1 = j3pGetRandomInt(1, 360)
        A2 = j3pGetRandomInt(0, 10)
        do {
          A3 = j3pGetRandomInt(0, 10)
        } while (A2 === A3)
        // 8 sommets
        lalettre1 = addMaj(nomspris); lalettre2 = addMaj(nomspris); lalettre3 = addMaj(nomspris); lalettre4 = addMaj(nomspris)
        lalettre5 = addMaj(nomspris); lalettre6 = addMaj(nomspris); lalettre7 = addMaj(nomspris)
        const lalettre8 = addMaj(nomspris)

        listeNom.push({ t: '#npA', nom: lalettre1 })
        listeNom.push({ t: '#npB', nom: lalettre2 })
        listeNom.push({ t: '#npC', nom: lalettre3 })
        listeNom.push({ t: '#npD', nom: lalettre4 })
        listeNom.push({ t: '#npE', nom: lalettre5 })
        listeNom.push({ t: '#npF', nom: lalettre6 })
        listeNom.push({ t: '#npG', nom: lalettre7 })
        listeNom.push({ t: '#npH', nom: lalettre8 })

        if (elt === 'face') {
          const haz = j3pGetRandomInt(0, 5)
          if (haz === 0) {
            stor.listeChange.push({ id: '#surfgauche', co: coMod, ep: 2, st: 0 })
            stor.NomElem = [{ texte: lalettre1 + lalettre2 + lalettre6 + lalettre5, chapo: 0 }]
            stor.NomElem.push({ texte: lalettre1 + lalettre5 + lalettre6 + lalettre2, chapo: 0 })
            stor.NomElem.push({ texte: lalettre5 + lalettre6 + lalettre2 + lalettre1, chapo: 0 })
            stor.NomElem.push({ texte: lalettre5 + lalettre1 + lalettre2 + lalettre6, chapo: 0 })
            stor.NomElem.push({ texte: lalettre6 + lalettre2 + lalettre1 + lalettre5, chapo: 0 })
            stor.NomElem.push({ texte: lalettre6 + lalettre5 + lalettre1 + lalettre2, chapo: 0 })
            stor.NomElem.push({ texte: lalettre2 + lalettre1 + lalettre5 + lalettre6, chapo: 0 })
            stor.NomElem.push({ texte: lalettre2 + lalettre6 + lalettre5 + lalettre1, chapo: 0 })
          }
          if (haz === 1) {
            stor.listeChange.push({ id: '#surfdroite', co: coMod, ep: 2, st: 0 })
            stor.NomElem = [{ texte: lalettre3 + lalettre4 + lalettre8 + lalettre7, chapo: 0 }]
            stor.NomElem.push({ texte: lalettre3 + lalettre7 + lalettre8 + lalettre4, chapo: 0 })
            stor.NomElem.push({ texte: lalettre7 + lalettre8 + lalettre4 + lalettre3, chapo: 0 })
            stor.NomElem.push({ texte: lalettre7 + lalettre3 + lalettre4 + lalettre8, chapo: 0 })
            stor.NomElem.push({ texte: lalettre8 + lalettre4 + lalettre3 + lalettre7, chapo: 0 })
            stor.NomElem.push({ texte: lalettre8 + lalettre7 + lalettre3 + lalettre4, chapo: 0 })
            stor.NomElem.push({ texte: lalettre4 + lalettre3 + lalettre7 + lalettre8, chapo: 0 })
            stor.NomElem.push({ texte: lalettre4 + lalettre8 + lalettre7 + lalettre3, chapo: 0 })
          }
          if (haz === 2) {
            stor.listeChange.push({ id: '#surfbas', co: coMod, ep: 2, st: 0 })
            stor.NomElem = [{ texte: lalettre5 + lalettre6 + lalettre7 + lalettre8, chapo: 0 }]
            stor.NomElem.push({ texte: lalettre5 + lalettre8 + lalettre7 + lalettre6, chapo: 0 })
            stor.NomElem.push({ texte: lalettre8 + lalettre7 + lalettre6 + lalettre5, chapo: 0 })
            stor.NomElem.push({ texte: lalettre8 + lalettre5 + lalettre6 + lalettre7, chapo: 0 })
            stor.NomElem.push({ texte: lalettre7 + lalettre6 + lalettre5 + lalettre8, chapo: 0 })
            stor.NomElem.push({ texte: lalettre7 + lalettre8 + lalettre5 + lalettre6, chapo: 0 })
            stor.NomElem.push({ texte: lalettre6 + lalettre5 + lalettre8 + lalettre7, chapo: 0 })
            stor.NomElem.push({ texte: lalettre6 + lalettre7 + lalettre8 + lalettre5, chapo: 0 })
          }
          if (haz === 3) {
            stor.listeChange.push({ id: '#surfhaut', co: coMod, ep: 2, st: 0 })
            stor.NomElem = [{ texte: lalettre1 + lalettre2 + lalettre3 + lalettre4, chapo: 0 }]
            stor.NomElem.push({ texte: lalettre1 + lalettre4 + lalettre3 + lalettre2, chapo: 0 })
            stor.NomElem.push({ texte: lalettre4 + lalettre3 + lalettre2 + lalettre1, chapo: 0 })
            stor.NomElem.push({ texte: lalettre4 + lalettre1 + lalettre2 + lalettre3, chapo: 0 })
            stor.NomElem.push({ texte: lalettre3 + lalettre2 + lalettre1 + lalettre4, chapo: 0 })
            stor.NomElem.push({ texte: lalettre3 + lalettre4 + lalettre1 + lalettre2, chapo: 0 })
            stor.NomElem.push({ texte: lalettre2 + lalettre1 + lalettre4 + lalettre3, chapo: 0 })
            stor.NomElem.push({ texte: lalettre2 + lalettre3 + lalettre4 + lalettre1, chapo: 0 })
          }
          if (haz === 4) {
            stor.listeChange.push({ id: '#surfavant', co: coMod, ep: 2, st: 0 })
            stor.NomElem = [{ texte: lalettre1 + lalettre5 + lalettre8 + lalettre4, chapo: 0 }]
            stor.NomElem.push({ texte: lalettre1 + lalettre4 + lalettre8 + lalettre5, chapo: 0 })
            stor.NomElem.push({ texte: lalettre4 + lalettre8 + lalettre5 + lalettre1, chapo: 0 })
            stor.NomElem.push({ texte: lalettre4 + lalettre1 + lalettre5 + lalettre8, chapo: 0 })
            stor.NomElem.push({ texte: lalettre8 + lalettre5 + lalettre1 + lalettre4, chapo: 0 })
            stor.NomElem.push({ texte: lalettre8 + lalettre4 + lalettre1 + lalettre5, chapo: 0 })
            stor.NomElem.push({ texte: lalettre5 + lalettre1 + lalettre4 + lalettre8, chapo: 0 })
            stor.NomElem.push({ texte: lalettre5 + lalettre8 + lalettre4 + lalettre1, chapo: 0 })
          }
          if (haz === 5) {
            stor.listeChange.push({ id: '#surfarriere', co: coMod, ep: 2, st: 0 })
            stor.NomElem = [{ texte: lalettre2 + lalettre3 + lalettre7 + lalettre6, chapo: 0 }]
            stor.NomElem.push({ texte: lalettre2 + lalettre6 + lalettre7 + lalettre3, chapo: 0 })
            stor.NomElem.push({ texte: lalettre6 + lalettre7 + lalettre3 + lalettre2, chapo: 0 })
            stor.NomElem.push({ texte: lalettre6 + lalettre2 + lalettre3 + lalettre7, chapo: 0 })
            stor.NomElem.push({ texte: lalettre7 + lalettre3 + lalettre2 + lalettre6, chapo: 0 })
            stor.NomElem.push({ texte: lalettre7 + lalettre6 + lalettre2 + lalettre3, chapo: 0 })
            stor.NomElem.push({ texte: lalettre3 + lalettre2 + lalettre6 + lalettre7, chapo: 0 })
            stor.NomElem.push({ texte: lalettre3 + lalettre7 + lalettre6 + lalettre2, chapo: 0 })
          }
        } else if (elt === 'face_cachee') {
          const haz = j3pGetRandomInt(0, 2)
          if (haz === 0) {
            stor.listeChange.push({ id: '#surfgauche', co: coMod, ep: 2, st: 0 })
            stor.NomElem = [{ texte: lalettre1 + lalettre2 + lalettre6 + lalettre5, chapo: 0 }]
            stor.NomElem.push({ texte: lalettre1 + lalettre5 + lalettre6 + lalettre2, chapo: 0 })
            stor.NomElem.push({ texte: lalettre5 + lalettre6 + lalettre2 + lalettre1, chapo: 0 })
            stor.NomElem.push({ texte: lalettre5 + lalettre1 + lalettre2 + lalettre6, chapo: 0 })
            stor.NomElem.push({ texte: lalettre6 + lalettre2 + lalettre1 + lalettre5, chapo: 0 })
            stor.NomElem.push({ texte: lalettre6 + lalettre5 + lalettre1 + lalettre2, chapo: 0 })
            stor.NomElem.push({ texte: lalettre2 + lalettre1 + lalettre5 + lalettre6, chapo: 0 })
            stor.NomElem.push({ texte: lalettre2 + lalettre6 + lalettre5 + lalettre1, chapo: 0 })
          }
          if (haz === 1) {
            stor.listeChange.push({ id: '#surfbas', co: coMod, ep: 2, st: 0 })
            stor.NomElem = [{ texte: lalettre5 + lalettre6 + lalettre7 + lalettre8, chapo: 0 }]
            stor.NomElem.push({ texte: lalettre5 + lalettre8 + lalettre7 + lalettre6, chapo: 0 })
            stor.NomElem.push({ texte: lalettre8 + lalettre7 + lalettre6 + lalettre5, chapo: 0 })
            stor.NomElem.push({ texte: lalettre8 + lalettre5 + lalettre6 + lalettre7, chapo: 0 })
            stor.NomElem.push({ texte: lalettre7 + lalettre6 + lalettre5 + lalettre8, chapo: 0 })
            stor.NomElem.push({ texte: lalettre7 + lalettre8 + lalettre5 + lalettre6, chapo: 0 })
            stor.NomElem.push({ texte: lalettre6 + lalettre5 + lalettre8 + lalettre7, chapo: 0 })
            stor.NomElem.push({ texte: lalettre6 + lalettre7 + lalettre8 + lalettre5, chapo: 0 })
          }
          if (haz === 2) {
            stor.listeChange.push({ id: '#surfarriere', co: coMod, ep: 2, st: 0 })
            stor.NomElem = [{ texte: lalettre2 + lalettre3 + lalettre7 + lalettre6, chapo: 0 }]
            stor.NomElem.push({ texte: lalettre2 + lalettre6 + lalettre7 + lalettre3, chapo: 0 })
            stor.NomElem.push({ texte: lalettre6 + lalettre7 + lalettre3 + lalettre2, chapo: 0 })
            stor.NomElem.push({ texte: lalettre6 + lalettre2 + lalettre3 + lalettre7, chapo: 0 })
            stor.NomElem.push({ texte: lalettre7 + lalettre3 + lalettre2 + lalettre6, chapo: 0 })
            stor.NomElem.push({ texte: lalettre7 + lalettre6 + lalettre2 + lalettre3, chapo: 0 })
            stor.NomElem.push({ texte: lalettre3 + lalettre2 + lalettre6 + lalettre7, chapo: 0 })
            stor.NomElem.push({ texte: lalettre3 + lalettre7 + lalettre6 + lalettre2, chapo: 0 })
          }
        } else if (elt === 'face_visible') {
          const haz = j3pGetRandomInt(0, 2)
          if (haz === 0) {
            stor.listeChange.push({ id: '#surfdroite', co: coMod, ep: 2, st: 0 })
            stor.NomElem = [{ texte: lalettre3 + lalettre4 + lalettre8 + lalettre7, chapo: 0 }]
            stor.NomElem.push({ texte: lalettre3 + lalettre7 + lalettre8 + lalettre4, chapo: 0 })
            stor.NomElem.push({ texte: lalettre7 + lalettre8 + lalettre4 + lalettre3, chapo: 0 })
            stor.NomElem.push({ texte: lalettre7 + lalettre3 + lalettre4 + lalettre8, chapo: 0 })
            stor.NomElem.push({ texte: lalettre8 + lalettre4 + lalettre3 + lalettre7, chapo: 0 })
            stor.NomElem.push({ texte: lalettre8 + lalettre7 + lalettre3 + lalettre4, chapo: 0 })
            stor.NomElem.push({ texte: lalettre4 + lalettre3 + lalettre7 + lalettre8, chapo: 0 })
            stor.NomElem.push({ texte: lalettre4 + lalettre8 + lalettre7 + lalettre3, chapo: 0 })
          }
          if (haz === 1) {
            stor.listeChange.push({ id: '#surfavant', co: coMod, ep: 2, st: 0 })
            stor.NomElem = [{ texte: lalettre1 + lalettre5 + lalettre8 + lalettre4, chapo: 0 }]
            stor.NomElem.push({ texte: lalettre1 + lalettre4 + lalettre8 + lalettre5, chapo: 0 })
            stor.NomElem.push({ texte: lalettre4 + lalettre8 + lalettre5 + lalettre1, chapo: 0 })
            stor.NomElem.push({ texte: lalettre4 + lalettre1 + lalettre5 + lalettre8, chapo: 0 })
            stor.NomElem.push({ texte: lalettre8 + lalettre5 + lalettre1 + lalettre4, chapo: 0 })
            stor.NomElem.push({ texte: lalettre8 + lalettre4 + lalettre1 + lalettre5, chapo: 0 })
            stor.NomElem.push({ texte: lalettre5 + lalettre1 + lalettre4 + lalettre8, chapo: 0 })
            stor.NomElem.push({ texte: lalettre5 + lalettre8 + lalettre4 + lalettre1, chapo: 0 })
          }
          if (haz === 2) {
            stor.listeChange.push({ id: '#surfhaut', co: coMod, ep: 2, st: 0 })
            stor.NomElem = [{ texte: lalettre1 + lalettre2 + lalettre3 + lalettre4, chapo: 0 }]
            stor.NomElem.push({ texte: lalettre1 + lalettre4 + lalettre3 + lalettre2, chapo: 0 })
            stor.NomElem.push({ texte: lalettre4 + lalettre3 + lalettre2 + lalettre1, chapo: 0 })
            stor.NomElem.push({ texte: lalettre4 + lalettre1 + lalettre2 + lalettre3, chapo: 0 })
            stor.NomElem.push({ texte: lalettre3 + lalettre2 + lalettre1 + lalettre4, chapo: 0 })
            stor.NomElem.push({ texte: lalettre3 + lalettre4 + lalettre1 + lalettre2, chapo: 0 })
            stor.NomElem.push({ texte: lalettre2 + lalettre1 + lalettre4 + lalettre3, chapo: 0 })
            stor.NomElem.push({ texte: lalettre2 + lalettre3 + lalettre4 + lalettre1, chapo: 0 })
          }
        } else if (elt === 'sommet') {
          const haz = j3pGetRandomInt(0, 7)
          if (haz === 0) {
            stor.NomElem = [{ texte: lalettre1, chapo: 0 }]
            stor.listeChange.push({ id: '#sA', co: coMod, ep: 2, st: 0 })
          }
          if (haz === 1) {
            stor.NomElem = [{ texte: lalettre2, chapo: 0 }]
            stor.listeChange.push({ id: '#sB', co: coMod, ep: 2, st: 0 })
          }
          if (haz === 2) {
            stor.NomElem = [{ texte: lalettre3, chapo: 0 }]
            stor.listeChange.push({ id: '#sC', co: coMod, ep: 2, st: 0 })
          }
          if (haz === 3) {
            stor.NomElem = [{ texte: lalettre4, chapo: 0 }]
            stor.listeChange.push({ id: '#sD', co: coMod, ep: 2, st: 0 })
          }
          if (haz === 4) {
            stor.NomElem = [{ texte: lalettre5, chapo: 0 }]
            stor.listeChange.push({ id: '#sE', co: coMod, ep: 2, st: 0 })
          }
          if (haz === 5) {
            stor.NomElem = [{ texte: lalettre6, chapo: 0 }]
            stor.listeChange.push({ id: '#sF', co: coMod, ep: 2, st: 0 })
          }
          if (haz === 6) {
            stor.NomElem = [{ texte: lalettre7, chapo: 0 }]
            stor.listeChange.push({ id: '#sG', co: coMod, ep: 2, st: 0 })
          }
          if (haz === 7) {
            stor.NomElem = [{ texte: lalettre8, chapo: 0 }]
            stor.listeChange.push({ id: '#sH', co: coMod, ep: 2, st: 0 })
          }
        } else if (elt === 'arete') {
          const haz = j3pGetRandomInt(0, 2)
          if (haz === 0) {
            stor.NomElem = [{ texte: '[' + lalettre1 + lalettre2 + ']', chapo: 0 }, { texte: '[' + lalettre2 + lalettre1 + ']', chapo: 0 }]
            stor.listeChange.push({ id: '#seg1', co: coMod, ep: 4, st: 0 })
          }
          if (haz === 1) {
            stor.NomElem = [{ texte: '[' + lalettre3 + lalettre4 + ']', chapo: 0 }, { texte: '[' + lalettre4 + lalettre3 + ']', chapo: 0 }]
            stor.listeChange.push({ id: '#seg3', co: coMod, ep: 4, st: 0 })
          }
          if (haz === 2) {
            stor.NomElem = [{ texte: '[' + lalettre5 + lalettre6 + ']', chapo: 0 }, { texte: '[' + lalettre6 + lalettre5 + ']', chapo: 0 }]
            stor.listeChange.push({ id: '#seg5', co: coMod, ep: 4, st: 1 })
          }
        } else if (elt === 'arete_cachee') {
          stor.NomElem = [{ texte: '[' + lalettre5 + lalettre6 + ']', chapo: 0 }, { texte: '[' + lalettre6 + lalettre5 + ']', chapo: 0 }]
          stor.listeChange.push({ id: '#seg5', co: coMod, ep: 4, st: 1 })
        } else if (elt === 'arete_visible') {
          if (j3pGetRandomBool()) {
            stor.NomElem = [{ texte: '[' + lalettre1 + lalettre2 + ']', chapo: 0 }, { texte: '[' + lalettre2 + lalettre1 + ']', chapo: 0 }]
            stor.listeChange.push({ id: '#seg1', co: coMod, ep: 4, st: 0 })
          } else {
            stor.NomElem = [{ texte: '[' + lalettre3 + lalettre4 + ']', chapo: 0 }, { texte: '[' + lalettre4 + lalettre3 + ']', chapo: 0 }]
            stor.listeChange.push({ id: '#seg3', co: coMod, ep: 4, st: 0 })
          }
        }

        if (elt === 'longueur') {
          stor.NomElem = [{ texte: lalettre3 + lalettre7, chapo: 0 }, { texte: lalettre7 + lalettre3, chapo: 0 }]
          stor.NomNeedElem.push({ texte: '[' + lalettre3 + lalettre7 + ']', chapo: 0 })
        } else {
          stor.listCache.push('#aa1', '#aa2', '#aa3', '#aa4', '#aa5')
        }

        stor.NomContenant = {
          texte: lalettre1 + lalettre2 + lalettre3 + lalettre4 + lalettre5 + lalettre6 + lalettre7 + lalettre8,
          chapo: 0
        }
        break
      } // PAVEDROIT

      case 'PRISME':
        stor.height = stor.lwidth = 400
        A1 = j3pGetRandomInt(1, 360)
        A2 = j3pGetRandomInt(0, 10)
        A3 = j3pGetRandomInt(0, 10)
        lalettre1 = addMaj(nomspris); lalettre2 = addMaj(nomspris)
        lalettre3 = addMaj(nomspris); lalettre4 = addMaj(nomspris); lalettre5 = addMaj(nomspris); lalettre6 = addMaj(nomspris)
        listeNom.push({ t: '#npA', nom: lalettre1 })
        listeNom.push({ t: '#npB', nom: lalettre2 })
        listeNom.push({ t: '#npE', nom: lalettre3 })
        listeNom.push({ t: '#npF', nom: lalettre4 })
        listeNom.push({ t: '#npG', nom: lalettre5 })
        listeNom.push({ t: '#npH', nom: lalettre6 })
        if (elt === 'face') {
          const haz = j3pGetRandomInt(0, 4)
          if (haz === 0) {
            stor.listeChange.push({ id: '#surfgauche', co: coMod, ep: 2, st: 0 })
            stor.NomElem = [{ texte: lalettre1 + lalettre2 + lalettre4 + lalettre3, chapo: 0 }]
            stor.NomElem.push({ texte: lalettre1 + lalettre3 + lalettre4 + lalettre2, chapo: 0 })
            stor.NomElem.push({ texte: lalettre3 + lalettre4 + lalettre2 + lalettre1, chapo: 0 })
            stor.NomElem.push({ texte: lalettre3 + lalettre1 + lalettre2 + lalettre4, chapo: 0 })
            stor.NomElem.push({ texte: lalettre4 + lalettre2 + lalettre1 + lalettre3, chapo: 0 })
            stor.NomElem.push({ texte: lalettre4 + lalettre3 + lalettre1 + lalettre2, chapo: 0 })
            stor.NomElem.push({ texte: lalettre2 + lalettre1 + lalettre3 + lalettre4, chapo: 0 })
            stor.NomElem.push({ texte: lalettre2 + lalettre4 + lalettre3 + lalettre1, chapo: 0 })
          }
          if (haz === 1) {
            stor.listeChange.push({ id: '#surfarriere', co: coMod, ep: 2, st: 0 })
            stor.NomElem = [{ texte: lalettre2 + lalettre4 + lalettre5, chapo: 0 }]
            stor.NomElem.push({ texte: lalettre2 + lalettre5 + lalettre4, chapo: 0 })
            stor.NomElem.push({ texte: lalettre5 + lalettre4 + lalettre2, chapo: 0 })
            stor.NomElem.push({ texte: lalettre5 + lalettre2 + lalettre4, chapo: 0 })
            stor.NomElem.push({ texte: lalettre4 + lalettre5 + lalettre2, chapo: 0 })
            stor.NomElem.push({ texte: lalettre4 + lalettre2 + lalettre5, chapo: 0 })
          }
          if (haz === 2) {
            stor.listeChange.push({ id: '#surfbas', co: coMod, ep: 2, st: 0 })
            stor.NomElem = [{ texte: lalettre4 + lalettre5 + lalettre6 + lalettre3, chapo: 0 }]
            stor.NomElem.push({ texte: lalettre4 + lalettre3 + lalettre6 + lalettre5, chapo: 0 })
            stor.NomElem.push({ texte: lalettre3 + lalettre4 + lalettre5 + lalettre6, chapo: 0 })
            stor.NomElem.push({ texte: lalettre3 + lalettre6 + lalettre5 + lalettre4, chapo: 0 })
            stor.NomElem.push({ texte: lalettre5 + lalettre6 + lalettre3 + lalettre4, chapo: 0 })
            stor.NomElem.push({ texte: lalettre5 + lalettre4 + lalettre3 + lalettre6, chapo: 0 })
            stor.NomElem.push({ texte: lalettre6 + lalettre5 + lalettre4 + lalettre3, chapo: 0 })
            stor.NomElem.push({ texte: lalettre6 + lalettre3 + lalettre4 + lalettre5, chapo: 0 })
          }
          if (haz === 3) {
            stor.listeChange.push({ id: '#surfavant', co: coMod, ep: 2, st: 0 })
            stor.NomElem = [{ texte: lalettre1 + lalettre3 + lalettre6, chapo: 0 }]
            stor.NomElem.push({ texte: lalettre1 + lalettre6 + lalettre3, chapo: 0 })
            stor.NomElem.push({ texte: lalettre3 + lalettre1 + lalettre6, chapo: 0 })
            stor.NomElem.push({ texte: lalettre3 + lalettre6 + lalettre1, chapo: 0 })
            stor.NomElem.push({ texte: lalettre6 + lalettre3 + lalettre1, chapo: 0 })
            stor.NomElem.push({ texte: lalettre6 + lalettre1 + lalettre3, chapo: 0 })
          }
          if (haz === 4) {
            stor.listeChange.push({ id: '#surfdroite', co: coMod, ep: 2, st: 0 })
            stor.NomElem = [{ texte: lalettre1 + lalettre2 + lalettre5 + lalettre6, chapo: 0 }]
            stor.NomElem.push({ texte: lalettre1 + lalettre6 + lalettre5 + lalettre2, chapo: 0 })
            stor.NomElem.push({ texte: lalettre2 + lalettre1 + lalettre6 + lalettre5, chapo: 0 })
            stor.NomElem.push({ texte: lalettre2 + lalettre5 + lalettre6 + lalettre1, chapo: 0 })
            stor.NomElem.push({ texte: lalettre5 + lalettre6 + lalettre1 + lalettre2, chapo: 0 })
            stor.NomElem.push({ texte: lalettre5 + lalettre2 + lalettre1 + lalettre6, chapo: 0 })
            stor.NomElem.push({ texte: lalettre6 + lalettre5 + lalettre2 + lalettre1, chapo: 0 })
            stor.NomElem.push({ texte: lalettre6 + lalettre1 + lalettre2 + lalettre5, chapo: 0 })
          }
        } else if (elt === 'face_laterale') {
          const haz = j3pGetRandomInt(0, 2)
          if (haz === 0) {
            stor.listeChange.push({ id: '#surfgauche', co: coMod, ep: 2, st: 0 })
            stor.NomElem = [{ texte: lalettre1 + lalettre2 + lalettre4 + lalettre3, chapo: 0 }]
            stor.NomElem.push({ texte: lalettre1 + lalettre3 + lalettre4 + lalettre2, chapo: 0 })
            stor.NomElem.push({ texte: lalettre3 + lalettre4 + lalettre2 + lalettre1, chapo: 0 })
            stor.NomElem.push({ texte: lalettre3 + lalettre1 + lalettre2 + lalettre4, chapo: 0 })
            stor.NomElem.push({ texte: lalettre4 + lalettre2 + lalettre1 + lalettre3, chapo: 0 })
            stor.NomElem.push({ texte: lalettre4 + lalettre3 + lalettre1 + lalettre2, chapo: 0 })
            stor.NomElem.push({ texte: lalettre2 + lalettre1 + lalettre3 + lalettre4, chapo: 0 })
            stor.NomElem.push({ texte: lalettre2 + lalettre4 + lalettre3 + lalettre1, chapo: 0 })
          }
          if (haz === 1) {
            stor.listeChange.push({ id: '#surfbas', co: coMod, ep: 2, st: 0 })
            stor.NomElem = [{ texte: lalettre4 + lalettre5 + lalettre6 + lalettre3, chapo: 0 }]
            stor.NomElem.push({ texte: lalettre4 + lalettre3 + lalettre6 + lalettre5, chapo: 0 })
            stor.NomElem.push({ texte: lalettre3 + lalettre4 + lalettre5 + lalettre6, chapo: 0 })
            stor.NomElem.push({ texte: lalettre3 + lalettre6 + lalettre5 + lalettre4, chapo: 0 })
            stor.NomElem.push({ texte: lalettre5 + lalettre6 + lalettre3 + lalettre4, chapo: 0 })
            stor.NomElem.push({ texte: lalettre5 + lalettre4 + lalettre3 + lalettre6, chapo: 0 })
            stor.NomElem.push({ texte: lalettre6 + lalettre5 + lalettre4 + lalettre3, chapo: 0 })
            stor.NomElem.push({ texte: lalettre6 + lalettre3 + lalettre4 + lalettre5, chapo: 0 })
          }
          if (haz === 2) {
            stor.listeChange.push({ id: '#surfdroite', co: coMod, ep: 2, st: 0 })
            stor.NomElem = [{ texte: lalettre1 + lalettre2 + lalettre5 + lalettre6, chapo: 0 }]
            stor.NomElem.push({ texte: lalettre1 + lalettre6 + lalettre5 + lalettre2, chapo: 0 })
            stor.NomElem.push({ texte: lalettre2 + lalettre1 + lalettre6 + lalettre5, chapo: 0 })
            stor.NomElem.push({ texte: lalettre2 + lalettre5 + lalettre6 + lalettre1, chapo: 0 })
            stor.NomElem.push({ texte: lalettre5 + lalettre6 + lalettre1 + lalettre2, chapo: 0 })
            stor.NomElem.push({ texte: lalettre5 + lalettre2 + lalettre1 + lalettre6, chapo: 0 })
            stor.NomElem.push({ texte: lalettre6 + lalettre5 + lalettre2 + lalettre1, chapo: 0 })
            stor.NomElem.push({ texte: lalettre6 + lalettre1 + lalettre2 + lalettre5, chapo: 0 })
          }
        } else if (elt === 'base') {
          if (j3pGetRandomBool()) {
            stor.listeChange.push({ id: '#surfavant', co: coMod, ep: 2, st: 0 })
            stor.NomElem = [{ texte: lalettre1 + lalettre3 + lalettre6, chapo: 0 }]
            stor.NomElem.push({ texte: lalettre1 + lalettre6 + lalettre3, chapo: 0 })
            stor.NomElem.push({ texte: lalettre3 + lalettre1 + lalettre6, chapo: 0 })
            stor.NomElem.push({ texte: lalettre3 + lalettre6 + lalettre1, chapo: 0 })
            stor.NomElem.push({ texte: lalettre6 + lalettre3 + lalettre1, chapo: 0 })
            stor.NomElem.push({ texte: lalettre6 + lalettre1 + lalettre3, chapo: 0 })
          } else {
            stor.listeChange.push({ id: '#surfarriere', co: coMod, ep: 2, st: 0 })
            stor.NomElem = [{ texte: lalettre2 + lalettre4 + lalettre5, chapo: 0 }]
            stor.NomElem.push({ texte: lalettre2 + lalettre5 + lalettre4, chapo: 0 })
            stor.NomElem.push({ texte: lalettre5 + lalettre4 + lalettre2, chapo: 0 })
            stor.NomElem.push({ texte: lalettre5 + lalettre2 + lalettre4, chapo: 0 })
            stor.NomElem.push({ texte: lalettre4 + lalettre5 + lalettre2, chapo: 0 })
            stor.NomElem.push({ texte: lalettre4 + lalettre2 + lalettre5, chapo: 0 })
          }
        } else if (elt === 'face_cachee') {
          const haz = j3pGetRandomInt(0, 2)
          if (haz === 0) {
            stor.listeChange.push({ id: '#surfarriere', co: coMod, ep: 2, st: 0 })
            stor.NomElem = [{ texte: lalettre2 + lalettre4 + lalettre5, chapo: 0 }]
            stor.NomElem.push({ texte: lalettre2 + lalettre5 + lalettre4, chapo: 0 })
            stor.NomElem.push({ texte: lalettre5 + lalettre4 + lalettre2, chapo: 0 })
            stor.NomElem.push({ texte: lalettre5 + lalettre2 + lalettre4, chapo: 0 })
            stor.NomElem.push({ texte: lalettre4 + lalettre5 + lalettre2, chapo: 0 })
            stor.NomElem.push({ texte: lalettre4 + lalettre2 + lalettre5, chapo: 0 })
          }
          if (haz === 1) {
            stor.listeChange.push({ id: '#surfbas', co: coMod, ep: 2, st: 0 })
            stor.NomElem = [{ texte: lalettre4 + lalettre5 + lalettre6 + lalettre3, chapo: 0 }]
            stor.NomElem.push({ texte: lalettre4 + lalettre3 + lalettre6 + lalettre5, chapo: 0 })
            stor.NomElem.push({ texte: lalettre3 + lalettre4 + lalettre5 + lalettre6, chapo: 0 })
            stor.NomElem.push({ texte: lalettre3 + lalettre6 + lalettre5 + lalettre4, chapo: 0 })
            stor.NomElem.push({ texte: lalettre5 + lalettre6 + lalettre3 + lalettre4, chapo: 0 })
            stor.NomElem.push({ texte: lalettre5 + lalettre4 + lalettre3 + lalettre6, chapo: 0 })
            stor.NomElem.push({ texte: lalettre6 + lalettre5 + lalettre4 + lalettre3, chapo: 0 })
            stor.NomElem.push({ texte: lalettre6 + lalettre3 + lalettre4 + lalettre5, chapo: 0 })
          }
          if (haz === 2) {
            stor.listeChange.push({ id: '#surfgauche', co: coMod, ep: 2, st: 0 })
            stor.NomElem = [{ texte: lalettre1 + lalettre2 + lalettre4 + lalettre3, chapo: 0 }]
            stor.NomElem.push({ texte: lalettre1 + lalettre3 + lalettre4 + lalettre2, chapo: 0 })
            stor.NomElem.push({ texte: lalettre3 + lalettre4 + lalettre2 + lalettre1, chapo: 0 })
            stor.NomElem.push({ texte: lalettre3 + lalettre1 + lalettre2 + lalettre4, chapo: 0 })
            stor.NomElem.push({ texte: lalettre4 + lalettre2 + lalettre1 + lalettre3, chapo: 0 })
            stor.NomElem.push({ texte: lalettre4 + lalettre3 + lalettre1 + lalettre2, chapo: 0 })
            stor.NomElem.push({ texte: lalettre2 + lalettre1 + lalettre3 + lalettre4, chapo: 0 })
            stor.NomElem.push({ texte: lalettre2 + lalettre4 + lalettre3 + lalettre1, chapo: 0 })
          }
        } else if (elt === 'face_visible') {
          if (j3pGetRandomBool()) {
            stor.listeChange.push({ id: '#surfavant', co: coMod, ep: 2, st: 0 })
            stor.NomElem = [{ texte: lalettre1 + lalettre3 + lalettre6, chapo: 0 }]
            stor.NomElem.push({ texte: lalettre1 + lalettre6 + lalettre3, chapo: 0 })
            stor.NomElem.push({ texte: lalettre3 + lalettre1 + lalettre6, chapo: 0 })
            stor.NomElem.push({ texte: lalettre3 + lalettre6 + lalettre1, chapo: 0 })
            stor.NomElem.push({ texte: lalettre6 + lalettre3 + lalettre1, chapo: 0 })
            stor.NomElem.push({ texte: lalettre6 + lalettre1 + lalettre3, chapo: 0 })
          } else {
            stor.listeChange.push({ id: '#surfdroite', co: coMod, ep: 2, st: 0 })
            stor.NomElem = [{ texte: lalettre1 + lalettre2 + lalettre5 + lalettre6, chapo: 0 }]
            stor.NomElem.push({ texte: lalettre1 + lalettre6 + lalettre5 + lalettre2, chapo: 0 })
            stor.NomElem.push({ texte: lalettre2 + lalettre1 + lalettre6 + lalettre5, chapo: 0 })
            stor.NomElem.push({ texte: lalettre2 + lalettre5 + lalettre6 + lalettre1, chapo: 0 })
            stor.NomElem.push({ texte: lalettre5 + lalettre6 + lalettre1 + lalettre2, chapo: 0 })
            stor.NomElem.push({ texte: lalettre5 + lalettre2 + lalettre1 + lalettre6, chapo: 0 })
            stor.NomElem.push({ texte: lalettre6 + lalettre5 + lalettre2 + lalettre1, chapo: 0 })
            stor.NomElem.push({ texte: lalettre6 + lalettre1 + lalettre2 + lalettre5, chapo: 0 })
          }
        } else if (elt === 'arete') {
          const haz = j3pGetRandomInt(0, 5)
          if (haz === 0) {
            stor.listeChange.push({ id: '#seg4', co: coMod, ep: 4, st: 1 })
            stor.NomElem = [{ texte: '[' + lalettre2 + lalettre4 + ']', chapo: 0 }, { texte: '[' + lalettre4 + lalettre2 + ']', chapo: 0 }]
          }
          if (haz === 1) {
            stor.listeChange.push({ id: '#seg6', co: coMod, ep: 4, st: 1 })
            stor.NomElem = [{ texte: '[' + lalettre5 + lalettre4 + ']', chapo: 0 }, { texte: '[' + lalettre4 + lalettre5 + ']', chapo: 0 }]
          }
          if (haz === 2) {
            stor.listeChange.push({ id: '#seg7', co: coMod, ep: 4, st: 1 })
            stor.NomElem = [{ texte: '[' + lalettre3 + lalettre4 + ']', chapo: 0 }, { texte: '[' + lalettre4 + lalettre3 + ']', chapo: 0 }]
          }
          if (haz === 3) {
            stor.listeChange.push({ id: '#seg1', co: coMod, ep: 4, st: 0 })
            stor.NomElem = [{ texte: '[' + lalettre1 + lalettre3 + ']', chapo: 0 }, { texte: '[' + lalettre3 + lalettre1 + ']', chapo: 0 }]
          }
          if (haz === 4) {
            stor.listeChange.push({ id: '#seg5', co: coMod, ep: 4, st: 0 })
            stor.NomElem = [{ texte: '[' + lalettre2 + lalettre5 + ']', chapo: 0 }, { texte: '[' + lalettre5 + lalettre2 + ']', chapo: 0 }]
          }
          if (haz === 5) {
            stor.listeChange.push({ id: '#seg8', co: coMod, ep: 4, st: 0 })
            stor.NomElem = [{ texte: '[' + lalettre2 + lalettre1 + ']', chapo: 0 }, { texte: '[' + lalettre1 + lalettre2 + ']', chapo: 0 }]
          }
        } else if (elt === 'arete_cachee') {
          const haz = j3pGetRandomInt(0, 2)
          if (haz === 0) {
            stor.listeChange.push({ id: '#seg4', co: coMod, ep: 4, st: 1 })
            stor.NomElem = [{ texte: '[' + lalettre2 + lalettre4 + ']', chapo: 0 }, { texte: '[' + lalettre4 + lalettre2 + ']', chapo: 0 }]
          }
          if (haz === 1) {
            stor.listeChange.push({ id: '#seg6', co: coMod, ep: 4, st: 1 })
            stor.NomElem = [{ texte: '[' + lalettre5 + lalettre4 + ']', chapo: 0 }, { texte: '[' + lalettre4 + lalettre5 + ']', chapo: 0 }]
          }
          if (haz === 2) {
            stor.listeChange.push({ id: '#seg7', co: coMod, ep: 4, st: 1 })
            stor.NomElem = [{ texte: '[' + lalettre3 + lalettre4 + ']', chapo: 0 }, { texte: '[' + lalettre4 + lalettre3 + ']', chapo: 0 }]
          }
        } else if (elt === 'arete_visible') {
          const haz = j3pGetRandomInt(0, 2)
          if (haz === 0) {
            stor.listeChange.push({ id: '#seg1', co: coMod, ep: 4, st: 0 })
            stor.NomElem = [{ texte: '[' + lalettre1 + lalettre3 + ']', chapo: 0 }, { texte: '[' + lalettre3 + lalettre1 + ']', chapo: 0 }]
          }
          if (haz === 1) {
            stor.listeChange.push({ id: '#seg5', co: coMod, ep: 4, st: 0 })
            stor.NomElem = [{ texte: '[' + lalettre2 + lalettre5 + ']', chapo: 0 }, { texte: '[' + lalettre5 + lalettre2 + ']', chapo: 0 }]
          }
          if (haz === 2) {
            stor.listeChange.push({ id: '#seg8', co: coMod, ep: 4, st: 0 })
            stor.NomElem = [{ texte: '[' + lalettre2 + lalettre1 + ']', chapo: 0 }, { texte: '[' + lalettre1 + lalettre2 + ']', chapo: 0 }]
          }
        }

        if (elt === 'longueur') {
          stor.NomElem = [{ texte: lalettre5 + lalettre2, chapo: 0 }, { texte: lalettre2 + lalettre5, chapo: 0 }]
          stor.NomNeedElem.push({ texte: '[' + lalettre5 + lalettre2 + ']', chapo: 0 })
        } else {
          stor.listCache.push('#aa1', '#aa2', '#aa3', '#aa4', '#aa5')
        }

        if (elt === 'hauteur') {
          stor.NomElem = [{ texte: lalettre5 + lalettre6, chapo: 0 }, { texte: lalettre6 + lalettre5, chapo: 0 }]
        } else {
          stor.listCache.push('#bb1', '#bb2', '#bb3', '#bb4', '#bb5')
        }

        if (elt === 'sommet') {
          const haz = j3pGetRandomInt(0, 5)
          if (haz === 0) {
            stor.NomElem = [{ texte: lalettre1, chapo: 0 }]
            stor.listeChange.push({ id: '#sA', co: coMod, ep: 2, st: 0 })
          }
          if (haz === 1) {
            stor.NomElem = [{ texte: lalettre2, chapo: 0 }]
            stor.listeChange.push({ id: '#sB', co: coMod, ep: 2, st: 0 })
          }
          if (haz === 2) {
            stor.NomElem = [{ texte: lalettre3, chapo: 0 }]
            stor.listeChange.push({ id: '#sE', co: coMod, ep: 2, st: 0 })
          }
          if (haz === 3) {
            stor.NomElem = [{ texte: lalettre4, chapo: 0 }]
            stor.listeChange.push({ id: '#sF', co: coMod, ep: 2, st: 0 })
          }
          if (haz === 4) {
            stor.NomElem = [{ texte: lalettre5, chapo: 0 }]
            stor.listeChange.push({ id: '#sG', co: coMod, ep: 2, st: 0 })
          }
          if (haz === 5) {
            stor.NomElem = [{ texte: lalettre6, chapo: 0 }]
            stor.listeChange.push({ id: '#sH', co: coMod, ep: 2, st: 0 })
          }
        }

        stor.NomContenant = {
          texte: lalettre1 + lalettre6 + lalettre3 + lalettre2 + lalettre5 + lalettre4,
          chapo: 0
        }
        break

      case 'CYLINDRE':// det les 4 points
        A1 = j3pGetRandomInt(0, 360)
        do {
          A2 = j3pGetRandomInt(85, 110)
        } while (Math.abs(A2 - 90) < 5)
        A3 = j3pGetRandomInt(90, 190)
        A4 = j3pGetRandomInt(6, 7)
        stor.height = 400
        stor.lwidth = 430

        lalettre2 = addMaj(nomspris)
        lalettre3 = addMaj(nomspris)
        lalettre4 = addMaj(nomspris)
        lalettre1 = addMaj(nomspris)
        lalettre5 = addMaj(nomspris)
        lalettre6 = addMaj(nomspris)
        lalettre7 = addMaj(nomspris)
        lalettre8 = addMaj(nomspris)
        listeNom = [{ t: '#np2', nom: lalettre7 }, { t: '#np3', nom: lalettre8 }, { t: '#npH', nom: lalettre3 }, { t: '#npB', nom: lalettre4 }, { t: '#npG', nom: lalettre5 }]
        if (elt === 'base') {
          if (j3pGetRandomBool()) {
            stor.listeChange.push({ id: '#base2', co: coMod, ep: 2, st: 0 })
            stor.NomElem = [{ texte: 'violette', chapo: 0 }]
          } else {
            stor.listeChange.push({ id: '#base1', co: coMod, ep: 2, st: 0 })
            stor.NomElem = [{ texte: 'violette', chapo: 0 }]
          }
        } else if (elt === 'face') {
          const haz = j3pGetRandomInt(0, 2)
          if (haz === 0) {
            stor.listeChange.push({ id: '#base2', co: coMod, ep: 2, st: 0 })
            stor.NomElem = [{ texte: 'violette', chapo: 0 }]
          }
          if (haz === 1) {
            stor.listeChange.push({ id: '#base1', co: coMod, ep: 2, st: 0 })
            stor.NomElem = [{ texte: 'violette', chapo: 0 }]
          }
          if (haz === 2) {
            stor.listeChange.push({ id: '#lat1', co: coMod, ep: 2, st: 0 })
            stor.listeChange.push({ id: '#lat2', co: coMod, ep: 2, st: 0 })
            stor.NomElem = [{ texte: 'violette', chapo: 0 }]
          }
        } else if (elt === 'rayon') {
          if (j3pGetRandomBool()) {
            stor.listeChange.push({ id: '#ray4', co: coMod, ep: 4, st: 2 })
            stor.NomElem = [{ texte: '[' + lalettre1 + lalettre3 + ']', chapo: 0 }, { texte: '[' + lalettre3 + lalettre1 + ']', chapo: 0 }]
          } else {
            stor.listeChange.push({ id: '#ray3', co: coMod, ep: 4, st: 0 })
            stor.NomElem = [{ texte: '[' + lalettre4 + lalettre6 + ']', chapo: 0 }, { texte: '[' + lalettre6 + lalettre4 + ']', chapo: 0 }]
          }
        }

        if (elt === 'le_rayon') {
          stor.NomElem = [{ texte: lalettre1 + lalettre3, chapo: 0 }, { texte: lalettre3 + lalettre1, chapo: 0 }]
        } else {
          stor.listCache.push('#ar1', '#ar2', '#ar3', '#ar4', '#ar5')
        }

        if (elt === 'le_diametre') {
          stor.listCache.push('#npX', '#ppx', '#np1', '#pp1')
          stor.NomElem = [{ texte: lalettre7 + lalettre8, chapo: 0 }, { texte: lalettre8 + lalettre7, chapo: 0 }]
        } else {
          stor.listCache.push('#ad1', '#ad2', '#ad3', '#ad4', '#ad5')
        }

        if (elt === 'diametre') {
          stor.listCache.push('#ray2', '#ray1')
          stor.listeChange.push({ id: '#diam', co: coMod, ep: 4, st: 0 })
          stor.NomElem = [{ texte: '[' + lalettre7 + lalettre8 + ']', chapo: 0 }, { texte: '[' + lalettre8 + lalettre7 + ']', chapo: 0 }]
        }
        if (elt !== 'diametre' && elt !== 'le_diametre') listeNom.push({ t: '#npX', nom: lalettre2 }, { t: '#np1', nom: lalettre6 })

        if (elt === 'hauteur') {
          stor.NomElem = [{ texte: lalettre4 + lalettre5, chapo: 0 }, { texte: lalettre5 + lalettre4, chapo: 0 }]
          stor.listCache.push('#npM')
        } else {
          stor.listCache.push('#ha1', '#ha2', '#ha3', '#ha4', '#ha5')
          listeNom.push({ t: '#npM', nom: lalettre1 })
        }

        if (elt === 'face_cachee') {
          stor.listeChange.push({ id: '#base2', co: coMod, ep: 2, st: 0 })
          stor.NomElem = [{ texte: 'violette', chapo: 0 }]
        } else if (elt === 'face_visible') {
          stor.listeChange.push({ id: '#base1', co: coMod, ep: 2, st: 0 })
          stor.NomElem = [{ texte: 'violette', chapo: 0 }]
        } else if (elt === 'face_laterale') {
          stor.listeChange.push({ id: '#lat1', co: coMod, ep: 2, st: 0 })
          stor.listeChange.push({ id: '#lat2', co: coMod, ep: 2, st: 0 })
          stor.NomElem = [{ texte: 'violette', chapo: 0 }]
        }

        break
      default :
        j3pShowError(Error(`Cas non prévu (${stor.contenant})`), { message: 'Erreur interne (cas non prévu)', mustNotify: true })
    }

    j3pCreeSVG(stor.lesdiv.figure, { id: svgId, width: stor.lwidth, height: stor.height, style: { border: 'solid black 1px' } })
    stor.mtgAppLecteur.removeAllDoc()
    stor.mtgAppLecteur.addDoc(svgId, stor.lafig, true)
    stor.mtgAppLecteur.giveFormula2(svgId, 'A1', A1, true)
    stor.mtgAppLecteur.giveFormula2(svgId, 'A2', A2, true)
    stor.mtgAppLecteur.giveFormula2(svgId, 'A3', A3, true)
    stor.mtgAppLecteur.giveFormula2(svgId, 'A4', A4, true)
    stor.mtgAppLecteur.calculateAndDisplayAll(true)
    for (let ju = 0; ju < listeNom.length; ju++) {
      stor.mtgAppLecteur.setText(svgId, listeNom[ju].t, listeNom[ju].nom, true)
    }
    for (let ju = 0; ju < stor.listeChange.length; ju++) {
      const col = stor.listeChange[ju].co
      const st = stor.listeChange[ju].st || 0
      stor.mtgAppLecteur.setColor(svgId, stor.listeChange[ju].id, col[0], col[1], col[2], true)
      stor.mtgAppLecteur.setLineStyle(svgId, stor.listeChange[ju].id, st, stor.listeChange[ju].ep, true)
    }
    for (let ju = 0; ju < stor.listCache.length; ju++) {
      stor.mtgAppLecteur.setVisible(svgId, stor.listCache[ju], false, true)
    }

    if (stor.contenant === 'CONE_REVOLUTION' || stor.contenant === 'CONE') {
      const p1 = stor.mtgAppLecteur.getPointPosition(svgId, '#POINT1')
      const p2 = stor.mtgAppLecteur.getPointPosition(svgId, '#POINT2')
      const p3 = stor.mtgAppLecteur.getPointPosition(svgId, '#POINT3')
      const p4 = stor.mtgAppLecteur.getPointPosition(svgId, '#POINT4')
      const pl = stor.mtgAppLecteur.getPointPosition(svgId, '#POINTL')
      let xmin = Math.min(Math.min(Math.min(p1.x, p3.x), p2.x), p4.x)
      let ymin = Math.min(Math.min(Math.min(p1.y, p3.y), p2.y), p4.y)
      let xmax = Math.max(Math.max(Math.max(p1.x, p3.x), p2.x), p4.x)
      let ymax = Math.max(Math.max(Math.max(p1.y, p3.y), p2.y), p4.y)
      if (stor.contenant === 'CONE') {
        const p5 = stor.mtgAppLecteur.getPointPosition(svgId, '#POINT5')
        xmin = Math.min(xmin, p5.x)
        xmax = Math.max(xmax, p5.x)
        ymin = Math.min(ymin, p5.y)
        ymax = Math.max(ymax, p5.y)
      }
      let depx = 0
      let depy = 0
      if (xmin < 20) depx = 20 - xmin
      if (xmax > 370) depx = 420 - xmax
      if (ymin < 20) depy = 20 - ymin
      if (ymax > 390) depy = 390 - ymax
      stor.mtgAppLecteur.setPointPosition(svgId, '#POINTL', pl.x + depx, pl.y + depy, true)
    } else if (stor.contenant === 'PYRAMIDE') {
      let cpt = 0
      while (!this.verifPyr() && cpt < 100) {
        cpt++
        A3 = j3pGetRandomInt(0, 180)
        stor.mtgAppLecteur.giveFormula2(svgId, 'A3', A3, true)
        stor.mtgAppLecteur.calculateAndDisplayAll(true)
      }
      const p1 = stor.mtgAppLecteur.getPointPosition(svgId, '#pv1')
      const p2 = stor.mtgAppLecteur.getPointPosition(svgId, '#pv2')
      const p3 = stor.mtgAppLecteur.getPointPosition(svgId, '#pv3')
      const p4 = stor.mtgAppLecteur.getPointPosition(svgId, '#pv4')
      const p5 = stor.mtgAppLecteur.getPointPosition(svgId, '#pv5')
      const bari = { x: (p1.x + p2.x + p3.x + p4.x + p5.x) / 5, y: (p1.y + p2.y + p3.y + p4.y + p5.y) / 5 }
      const pl1 = stor.mtgAppLecteur.getPointPosition(svgId, '#POINT1')
      const pl2 = stor.mtgAppLecteur.getPointPosition(svgId, '#POINT2')

      const pmil = stor.mtgAppLecteur.getPointPosition(svgId, '#PMIL')
      const depx = pmil.x - bari.x
      const depy = pmil.y - bari.y
      stor.mtgAppLecteur.setPointPosition(svgId, '#POINT1', pl1.x + depx, pl1.y + depy, true)
      stor.mtgAppLecteur.setPointPosition(svgId, '#POINT2', pl2.x + depx, pl2.y + depy, true)

      const ph1 = stor.mtgAppLecteur.valueOf(svgId, 'ph1')
      const ph2 = stor.mtgAppLecteur.valueOf(svgId, 'ph2')
      const ph3 = stor.mtgAppLecteur.valueOf(svgId, 'ph3')
      if (ph1 + ph2 < ph3 + 0.000001) {
        stor.mtgAppLecteur.setLineStyle(svgId, '#hauteur', 2, 3, true)
      }
    } else if (stor.contenant === 'CYLINDRE') {
      const p1 = stor.mtgAppLecteur.getPointPosition(svgId, '#PP1')
      const p2 = stor.mtgAppLecteur.getPointPosition(svgId, '#PP2')
      const p4 = stor.mtgAppLecteur.getPointPosition(svgId, '#POINT4')
      const p5 = stor.mtgAppLecteur.getPointPosition(svgId, '#POINT5')
      const bari = { x: (p1.x + p2.x + p4.x + p5.x) / 4, y: (p1.y + p2.y + p4.y + p5.y) / 4 }
      const pmil = stor.mtgAppLecteur.getPointPosition(svgId, '#POINTL')
      const depx = stor.lwidth / 2 - bari.x
      const depy = stor.height / 2 - bari.y
      stor.mtgAppLecteur.setPointPosition(svgId, '#POINTL', pmil.x + depx, pmil.y + depy, true)
    } else if (stor.contenant === 'CYLINDRE_REVOLUTION') {
      const p1 = stor.mtgAppLecteur.getPointPosition(svgId, '#POINT1')
      const p2 = stor.mtgAppLecteur.getPointPosition(svgId, '#POINT2')
      const p3 = stor.mtgAppLecteur.getPointPosition(svgId, '#POINT3')
      const p4 = stor.mtgAppLecteur.getPointPosition(svgId, '#POINT4')
      const bari = { x: (p1.x + p2.x + p3.x + p4.x) / 4, y: (p1.y + p2.y + p3.y + p4.y) / 4 }
      const pmil = stor.mtgAppLecteur.getPointPosition(svgId, '#POINTL1')
      const pmil2 = stor.mtgAppLecteur.getPointPosition(svgId, '#POINTL2')
      const depx = stor.lwidth / 2 - bari.x
      const depy = stor.height / 2 - bari.y
      stor.mtgAppLecteur.setPointPosition(svgId, '#POINTL1', pmil.x + depx, pmil.y + depy, true)
      stor.mtgAppLecteur.setPointPosition(svgId, '#POINTL2', pmil2.x + depx, pmil2.y + depy, true)
    } else if (stor.contenant === 'PAVEDROIT' || stor.contenant === 'PRISME') {
      const p1 = stor.mtgAppLecteur.getPointPosition(svgId, '#POINTMIL')
      const p2 = stor.mtgAppLecteur.getPointPosition(svgId, '#pointmil')
      const difx = stor.lwidth / 2 - p2.x
      const dify = stor.height / 2 - p2.y
      stor.mtgAppLecteur.setPointPosition(svgId, '#POINTMIL', p1.x + difx, p1.y + dify, true)
    }
  }

  verifPyr () {
    const { mtgAppLecteur, svgId } = this
    const ph1 = mtgAppLecteur.valueOf(svgId, 'VERIF1')
    const ph2 = mtgAppLecteur.valueOf(svgId, 'VERIF2')
    const ph3 = mtgAppLecteur.valueOf(svgId, 'VERIF3')
    const ph4 = mtgAppLecteur.valueOf(svgId, 'VERIF4')
    return (ph1 > 1.2) && (ph2 > 1.2) && (ph3 > 1.2) && (ph4 > 1.2)
  }
}

export default FigNomme03
