import { j3pAddElt, j3pAjouteBouton, j3pArrondi, j3pClone, j3pDetruit, j3pEmpty, j3pGetNewId, j3pGetRandomBool, j3pGetRandomInt, j3pShowError, j3pShuffle } from 'src/legacy/core/functions'
import { getPe, initCptPe } from 'src/legacy/sections/college/helperTommy'
import { j3pAffiche } from 'src/lib/mathquill/functions'
import { addDefaultTable } from 'src/legacy/themes/table'
import { j3pCreeSVG } from 'src/legacy/core/functionsSvg'
import textesGeneriques from 'src/lib/core/textes'
import { getMtgCore } from 'src/lib/outils/mathgraph'
import { addMaj } from 'src/lib/utils/string'

const { cBien, cFaux, essaieEncore, reponseIncomplete, tempsDepasse } = textesGeneriques

/**
 * Ajouter une description ici
 * @fileOverview
 * @author Tommy Barroy <t.barroy@laposte.net>
 * @since 2017-10
*/
export const params = {
  outils: ['mathquill'],
  /**
   * Ce qui est paramétrable dans l’éditeur de graphe
   * @type {LegacySectionParamsParametres}
   */
  parametres: [
    ['nbrepetitions', 3, 'entier', 'Nombre de répétitions de la section'],
    ['nbchances', 2, 'entier', 'Nombre d’essais possibles.'],
    ['Nul', false, 'boolean', '<u>true</u>: L’angle peut être nul.'],
    ['Plat', false, 'boolean', '<u>true</u>: L’angle peut être plat.'],
    ['Question', 'les trois', 'liste', '<u>Placement</u>: Il faut placer le rapporteur. <br><br> <u>Grad</u> Il faut sélectionner la graduation utilisée.  <br><br> <u>Construire</u> Il faut construire l’angle.', ['Placement', 'Grad', 'Construire', 'Grad_Construire', 'Placement_Construire', 'les trois']],
    // le modèle gère la compatibilité ascendante avec l’ancien paramètre MiseEnPagePlusClaire
    ['theme', 'standard', 'liste', 'Modèle pour l’affichage', ['standard', 'zonesAvecImageDeFond']]

  ],
  // dans le cas d’une section QUANTITATIVE
  pe: [
    { pe_1: 'placement' },
    { pe_2: 'graduation' },
    { pe_3: 'construction' }
  ]
}

/**
 * section angle04
 * @this {Parcours}
 */
export default function main () {
  const me = this
  const stor = me.storage
  const ds = me.donneesSection
  let svgId = stor.svgId

  function initSection () {
    let i
    stor.tempo = 11
    stor.tempodeb = 1000
    stor.depCentreCo = 2
    ds.limite = 0
    initCptPe(ds, stor)

    // On surcharge avec les parametres passés par le graphe
    me.validOnEnter = false // ex donneesSection.touche_entree

    // Construction de la page
    me.construitStructurePage('presentation3')

    stor.afaire = []
    if ((ds.Question === 'les trois') || (ds.Question.indexOf('Placement') !== -1)) stor.afaire.push('p')
    if ((ds.Question === 'les trois') || (ds.Question.indexOf('Grad') !== -1)) stor.afaire.push('g')
    if ((ds.Question === 'les trois') || (ds.Question.indexOf('Construire') !== -1)) stor.afaire.push('m')

    ds.lesExos = []
    let lp = ['a', 'o', 'a', 'o']
    if (ds.Nul) lp.push('n')
    if (ds.Plat) lp.push('p')
    lp = j3pShuffle(lp)
    for (i = 0; i < ds.nbitems; i++) {
      ds.lesExos[i] = { sit: lp[i % lp.length] }
    }
    ds.lesExos = j3pShuffle(ds.lesExos)

    for (i = 0; i < ds.nbitems; i++) {
      ds.lesExos[i].plus = i + 1 < ds.nbitems / 2
    }

    let tt = 'Construire un angle'
    if (ds.Question === 'Placement') tt = 'Placer le rapporteur pour construire directement'
    if (ds.Question === 'Grad') tt = 'La bonne graduation'

    me.afficheTitre(tt)
    stor.restric = '0123456789°'

    getMtgCore({ withMathJax: true })
      .then(
        // success
        (mtgAppLecteur) => {
          stor.mtgAppLecteur = mtgAppLecteur
          enonceMain()
        },
        // failure
        (error) => {
          j3pShowError(error, { message: 'mathgraph n’est pas correctement chargé', mustNotify: true })
        })
      // plantage dans enonceMain
      .catch(j3pShowError)
  }

  function faisChoixExos () {
    const e = ds.lesExos.pop()
    const nomspris = []
    stor.A = addMaj(nomspris)
    stor.B = addMaj(nomspris)
    stor.C = addMaj(nomspris)
    stor.rotation = j3pGetRandomInt(1, 359)
    switch (e.sit) {
      case 'a': stor.deplacement = j3pGetRandomInt(10, 90)
        break
      case 'o': stor.deplacement = j3pGetRandomInt(90, 170)
        break
      case 'n': stor.deplacement = 0
        break
      case 'p': stor.deplacement = 180
        break
    }
    stor.encours = stor.afaire[0]
    stor.placeok = true
    stor.imfaite = false
    stor.zommfait = false
    stor.pose = false
    return e
  }
  function poseQuestion () {
    j3pEmpty(stor.lesdiv.consigne)
    j3pEmpty(stor.lesdiv.zonedrep)
    if (stor.encours === 'p') {
      j3pAffiche(stor.lesdiv.consigne, null, '<b>Place correctement le rapporteur pour construire directement</b> un angle $\\widehat{' + stor.A + stor.B + stor.C + '}$ de mesure $' + stor.deplacement + '°$ .')
      me.afficheBoutonValider()
    }
    if (stor.encours === 'm') {
      j3pAffiche(stor.lesdiv.consigne, null, '<b>Construis un angle </b>$\\widehat{' + stor.A + stor.B + stor.C + '}$  de mesure $' + stor.deplacement + '°$ . <i>(en déplaçant le point ' + stor.C + ')</i>')
    }
    if (stor.encours === 'g') {
      // FIXME expliquer pourquoi il faut positionner l'élève d'office au dernier essai dès l'énoncé ? (si c'est justifié il faut plutôt fixer ds.nbchances à 1)
      me.essaiCourant = ds.nbchances - 1
      j3pAffiche(stor.lesdiv.consigne, null, '<b>Sélectionne les graduations utilisées pour construire directement l’angle  </b> ' + '$\\widehat{' + stor.A + stor.B + stor.C + '}$ de mesure $' + stor.deplacement + '°$ .')
    }

    j3pEmpty(stor.lesdiv.correction)
    j3pEmpty(stor.lesdiv.solution)
    j3pEmpty(stor.lesdiv.explications)
    stor.lesdiv.travail.classList.add('travail')
    stor.lesdiv.consigne.classList.add('enonce')
    stor.lesdiv.explications.classList.remove('explique')
    stor.lesdiv.solution.classList.remove('correction')
  }

  function creeLesDiv () {
    stor.lesdiv = {}
    stor.lesdiv.conteneur = j3pAddElt(me.zonesElts.MG, 'div', {
      style: me.styles.etendre('toutpetit.enonce', { padding: '10px' })
    })
    stor.lesdiv.consigne = addDefaultTable(stor.lesdiv.conteneur, 1, 1)[0][0]
    stor.lesdiv.travail1 = addDefaultTable(stor.lesdiv.conteneur, 1, 1)[0][0]
    const tt = addDefaultTable(stor.lesdiv.travail1, 1, 3)
    tt[0][1].style.width = '40px'
    stor.lesdiv.travail = tt[0][0]
    stor.lesdiv.correction = tt[0][2]
    stor.lesdiv.zonefig = j3pAddElt(stor.lesdiv.travail, 'div')
    stor.lesdiv.zonedrep = tt[0][1]
    stor.lesdiv.explications = j3pAddElt(stor.lesdiv.conteneur, 'div')
    stor.lesdiv.solution = j3pAddElt(stor.lesdiv.conteneur, 'div')
    stor.lesdiv.explications.style.color = me.styles.cfaux
    stor.lesdiv.solution.style.color = me.styles.toutpetit.correction.color
    me.zonesElts.MG.classList.add('fond')
    stor.lesdiv.travail.classList.add('travail')
    stor.lesdiv.consigne.classList.add('enonce')
  }

  function makefig () {
    stor.consel = []
    stor.captured = false
    stor.captured2 = false
    stor.oublipa = false
    stor.x = stor.y = 0
    if (!stor.imfaite) {
      svgId = j3pGetNewId('mtg32')
      stor.svgId = svgId
      stor.imfaite = true
      const txtfigure = 'TWF0aEdyYXBoSmF2YTEuMAAAABUAAmZy####AQD#AQAAAAIAAH0SAACArgAAAAADXAAAAoMAAAEBAAAAAQAAAAgAClJhcHBvcnRldXIBz0NvbnN0cnVpdCB1biByYXBwb3J0ZXVyIMOgIHBhcnRpciBkZSBkZXV4IHBvaW50cyBsaWJyZXMuCkxlcyBvYmpldHMgc291cmNlcyBudW3DqXJpcXVlcyBzb250IDoKIzE6ZXBhaXNzZXVyIDogZGlzdGFuY2UgZW4gcGl4ZWxzIGVudHJlIGxlcyBkZXV4IGFyY3MgZHUgcmFwcG9ydGV1cgojMjpsb25nUGV0aXRlc0dyYWQgOiBMb25ndWV1ciBlbiBwaXhlbCBkZXMgcGV0aXRlcyBncmFkdWF0aW9ucwojMzpsb25nTW95ZW5uZXNHcmFkIDogTG9uZ3VldXIgZW4gcGl4ZWwgZGVzIG1veWVubmVzIGdyYWR1YXRpb25zCiM0OmxvbmdHcmFuZGVzR3JhZCA6IExvbmd1ZXVyIGVuIHBpeGVsIGRlcyBncmFuZGVzIGdyYWR1YXRpb25zCkxlcyBvYmpldHMgc291cmNlcyBncmFwaGlxdWVzIHNvbnQgOgojNTpMZSBjZW50cmUgZHUgcmFwcG9ydGV1cgojNjpMZSBwb2lucyBjb3JyZXNwb25kYW50IMOgIGxhIGdyYWR1YXRpb24gMAoAAAAGAAAADQEAAABA#####wAAAAEAEUNFbGVtZW50R2VuZXJpcXVlAAllcGFpc3NldXL#####AAAAAQAAAAAAD2xvbmdQZXRpdGVzR3JhZP####8AAAABAAAAAAAQbG9uZ01veWVubmVzR3JhZP####8AAAABAAAAAAAPbG9uZ0dyYW5kZXNHcmFk#####wAAAAEAAAAAAAJPMQAAAAD#####AAAAAAACSTEAAAAA##########8AAAABABFDU3ltZXRyaWVDZW50cmFsZQD#####AAAABP####8AAAABAAtDUG9pbnRJbWFnZQD#####AaSkpD+AAAAAEAAAAAAAAAAAAAAAQAgAAAAAAAAAAAUAAAAABQAAAAb#####AAAAAQASQ0FyY0RlQ2VyY2xlRGlyZWN0Af####8ApKSkP4AAAAAAAAEAAAAEAAAABQAAAAf#####AAAAAgAJQ0NlcmNsZU9SAP####8BpKSkP4AAAAAAAAEAAAAF#####wAAAAEAD0NSZXN1bHRhdFZhbGV1cgAAAAAB#####wAAAAEACENTZWdtZW50AP####8BpKSkP4AAAAAQAAABAAAAAQAAAAcAAAAF#####wAAAAEAEENJbnREcm9pdGVDZXJjbGUA#####wAAAAoAAAAJ#####wAAAAEAEENQb2ludExpZUJpcG9pbnQA#####wGkpKQ#gAAAARAAAlcxAAAAAAAAAAAAQAgAAAAAAAAAAAUAAgAAAAsAAAACAP####8BpKSkP4AAAAAQAAAAAAAAAAAAAABACAAAAAAAAAAABQAAAAAMAAAABgAAAAMB#####wCkpKQ#gAAAAAAAAQAAAAQAAAAMAAAADf####8AAAABAA9DUG9pbnRMaWVDZXJjbGUA#####wEAAAA#gAAAARAAAkoxAAAAAAAAAAAAQAgAAAAAAAAAAAUAAT#IR8zsxBM2AAAACP####8AAAABABZDRHJvaXRlUGVycGVuZGljdWxhaXJlAP####8BpKSkP4AAAAAQAAABAAAAAQAAAAUAAAAKAAAABAD#####AaSkpD+AAAAAAAABAAAABf####8AAAABAApDT3BlcmF0aW9uAwAAAAUAAAAA#####wAAAAEACkNDb25zdGFudGVAAAAAAAAAAAEAAAAHAP####8AAAAQAAAAEQAAAAgA#####wGkpKQ#gAAAABAAAAAAAAAAAAAAAEAIAAAAAAAAAAAFAAEAAAAS#####wAAAAEADENUcmFuc2xhdGlvbgD#####AAAABQAAABMAAAACAP####8BpKSkP4AAAAAQAAAAAAAAAAAAAABACAAAAAAAAAAABQAAAAAHAAAAFP####8AAAABAAlDUG9seWdvbmUA#####wGkpKQ#gAAAAAAAAQAAAAUAAAAFAAAAEwAAABUAAAAHAAAABf####8AAAABABBDU3VyZmFjZVBvbHlnb25lAf####8A2NjYPkzMzQAAAAAABQAAABYAAAAGAf####8ApKSkP4AAAAAQAAABAAAAAQAAAAcAAAAVAAAABgH#####AKSkpD+AAAAAEAAAAQAAAAEAAAAVAAAAEwAAAAYB#####wCkpKQ#gAAAABAAAAEAAAABAAAAEwAAAAUAAAAJAP####8BAAD#P4AAAAAQAAAAAAAAAAAAAABACAAAAAAAAAAABQABP+fJck3xnRcAAAAIAAAACQD#####AQAA#z+AAAAAEAAAAAAAAAAAAAAAQAgAAAAAAAAAAAUAAT#nuF42fT8fAAAADv####8AAAACAA1DTGlldURlUG9pbnRzAP####8BAAD#P4AAAAAAAAEAAAAcAAAAMgAAAAAAHAAAAAAAAAAQAP####8BAAD#P4AAAAAAAAEAAAAbAAAAMgAAAAAAGwAAAAD#####AAAAAQARQ1N1cmZhY2VEZXV4TGlldXgB#####wDY2Ng+TMzNAAAAAAAFAAAAHQAAAB4AAAAGAP####8BpKSkP4AAAAAQAAABAAAAAQAAAAQAAAAPAAAABAD#####AaSkpD+AAAAAAAABAAAADwAAAAUAAAABAQAAAAcA#####wAAACAAAAAhAAAACAD#####AaSkpD+AAAAAEAAAAAAAAAAAAAAAQAgAAAAAAAAAAAUAAgAAACIAAAAGAP####8Bvb29P4AAAAAQAAABAAAAAQAAAA8AAAAj#####wAAAAIAEkNMaWV1T2JqZXRQYXJQdExpZQH#####AL29vT+AAAAAAAAAACQAAAAMQGagAAAAAAAAAAAPAAAAAAAAAAQA#####wG9vb0#gAAAAAAAAQAAAA8AAAAFAAAAAwEAAAAHAP####8AAAAgAAAAJgAAAAgA#####wG9vb0#gAAAABAAAAAAAAAAAAAAAEAIAAAAAAAAAAAFAAIAAAAnAAAABgD#####AYSEhD+AAAAAEAAAAQAAAAEAAAAoAAAADwAAAAQA#####wGEhIQ#gAAAAAAAAQAAAA8AAAAFAAAAAgEAAAAHAP####8AAAAgAAAAKgAAAAgA#####wGEhIQ#gAAAABAAAAAAAAAAAAAAAEAIAAAAAAAAAAAFAAIAAAArAAAABgD#####AYSEhD+AAAAAEAAAAQAAAAEAAAAsAAAADwAAABIB#####wCEhIQ#gAAAAAAAAAAtAAAADEBCgAAAAAAAAAAADwAAAAAAAAASAf####8AhISEP4AAAAAAAAAAKQAAAAxAMwAAAAAAAAAAAA8AAAAA#####wAAAAIAE0NNZXN1cmVBbmdsZU9yaWVudGUA#####wAEYW5nMQAAAAUAAAAEAAAAD#####8AAAABABRDRHJvaXRlRGlyZWN0aW9uRml4ZQD#####AQAAAD+AAAABEAAAAQAAAAEAAAAEAT#wAAAAAAAA#####wAAAAEAD0NQb2ludExpZURyb2l0ZQD#####AQAAAD+AAAABEAACSzEAAAAAAAAAAABACAAAAAAAAAAABQABQG#AAAAAAAAAAAAxAAAAEwD#####AARhbmcyAAAAMgAAAAQAAAAPAAAABAD#####AQAAAD+AAAAAAAABAAAAKAAAAAxAKAAAAAAAAAEAAAAHAP####8AAAAgAAAANAAAAAgA#####wEAAAA#gAAAABAAAAAAAAAAAAAAAEAIAAAAAAAAAAAFAAIAAAA1#####wAAAAEAD0NWYWxldXJBZmZpY2hlZQD#####AQAAAD+AAAABAAAAAAA2DAAAAAAAAQAAAAEAAAALAQAAAAUAAAAzAAAADEBWgAAAAAAAAAAAAAAAAAAAMAAAABIB#####wAAAAA#gAAAAAAAAAA3AAAADEAzAAAAAAAAAAAADwAAAAD#####AAAAAQAHQ0NhbGN1bAD#####AAdhbmdzdXAxAAgxODAtYW5nMQAAAAsBAAAADEBmgAAAAAAAAAAABQAAADD#####AAAAAgAPQ01lc3VyZUFic2Npc3NlAP####8ABGFiczEAAAAEAAAABQAAAAz#####AAAAAQALQ0hvbW90aGV0aWUA#####wAAAAQAAAAFAAAAOgAAAAIA#####wEAAAA#gAAAABAAAAAAAAAAAAAAAEAIAAAAAAAAAAAFAAAAAA8AAAA7AAAAFgD#####AQAAAD+AAAABAAAAAAA8CwAAAAAAAQAAAAIAAAALAQAAAAUAAAAzAAAADEBWgAAAAAAAAAAAAAAAAAAAOQAAABIB#####wAAAAA#gAAAAAAAAAA9AAAADEAzAAAAAAAAAAAADwAAAAAAAAAGAf####8ApKSkP4AAAAAQAAABAAAAAQAAAAwAAAANAAAAAQAAANX#####AAAAAQAKQ0NhbGNDb25zdAD#####AAJwaQAWMy4xNDE1OTI2NTM1ODk3OTMyMzg0NgAAAAxACSH7VEQtGAAAABcA#####wAGZGlzdGVlAAMwLjUAAAAMP+AAAAAAAAD#####AAAAAQAKQ1BvaW50QmFzZQD#####AQAAAD+AAAAADgABVQDAJAAAAAAAAEAQAAAAAAAAAAAFAABARoAAAAAAAUB5#hR64UeuAAAAFAD#####AQAAAD+AAAAAEAAAAQAAAAEAAAACAT#zMzMzMzMzAAAAFQD#####AQAAAD+AAAAADgABVgDAAAAAAAAAAEAQAAAAAAAAAAAFAAFAP5gQYk3S8gAAAAP#####AAAAAQAJQ0xvbmd1ZXVyAP####8AAAACAAAABAAAABsA#####wAAAP8#gAAAARAAAUIAAAAAAAAAAABACAAAAAAAAAAACQABQGigAAAAAABAc54UeuFHrgAAAAQA#####wH#AAA#gAAAAAAAAQAAAAYAAAAMQBQAAAAAAAAAAAAACQD#####AP###z+AAAABEAABQwAAAAAAAAAAAEAIAAAAAAAAAAJwdAUAAT#5CpOIEfwAAAAAB#####8AAAABAAhDVmVjdGV1cgD#####Af8AAD+AAAAAEAAAAQAAAAEAAAACAAAABAD#####AAAAAQANQ1RyYW5zUGFyVmVjdAD#####AAAACQAAAAIA#####wH#AAA#gAAAABAAAUEAAAAAAAAAAABACAAAAAAAAAAACAAAAAAGAAAACv####8AAAABAAxDQmlzc2VjdHJpY2UA#####wEAAAA#gAAAABAAAAEAAAABAAAACAAAAAYAAAALAAAAFQD#####AQAAAD+AAAAAEAAAAAAAAAAAAAAAP+MzMzMzMzMAAAUAAUBpPkKWDt3JAAAADAAAABMA#####wAEYW5nMgAAAAsAAAAGAAAACAAAABYA#####wH#AAA#gAAAAEAIAAAAAAAAP#AAAAAAAAAAAAAAAA0PAAAAAAABAAAAAQAAAAwAAAAAAAAAAAAAAAACwrAAAAAADv####8AAAACABNDTWFycXVlQW5nbGVPcmllbnRlAP####8B#wAAPuZmZgAAAAEAAAABQEJbiQkrj78AAAAACwAAAAYAAAAIAAAAAAEA#####wAAAAb#####AAAAAQAJQ0Ryb2l0ZUFCAP####8B#wAAP4AAAAAQAAABAAAAAQAAAAYAAAAIAAAABAD#####Af8AAD+AAAAAAAABAAAACAAAAAw#4zMzMzMzMwAAAAAHAP####8AAAASAAAAEwAAAAgA#####wH#AAA#gAAAABAAAAAAAAAAAAAAAEAIAAAAAAAAAAAIAAEAAAAU#####wAAAAEACUNSb3RhdGlvbgD#####AAAABgAAAAxAVoAAAAAAAAAAAAIA#####wH#AAA#gAAAABAAAAAAAAAAAAAAAEAIAAAAAAAAAAAIAAAAABUAAAAWAAAAAgD#####Af8AAD+AAAAAEAAAAAAAAAAAAAAAQAgAAAAAAAAAAAgAAAAAFwAAABEAAAADAP####8A####P4AAAAAEYXJjMgABAAAABgAAABgAAAAX#####wAAAAEAGUNTdXJmYWNlU2VjdGV1ckNpcmN1bGFpcmUA#####wH###8+5mZmAAAAAAAFAAAAGQAAAAYA#####wEAAAA#gAAAABAAAAEAAAABAAAAAgAAAAT#####AAAAAQAHQ01pbGlldQD#####AQAAAD+AAAAAEAAAAAAAAAAAAAAAQAgAAAAAAAAAAAUAAAAAAgAAAAT#####AAAAAgAMQ0NvbW1lbnRhaXJlAP####8BAAAAP4AAAAAAAAAAAAAAAEAYAAAAAAAAAAAAAAAcDAAAAAAAAQAAAAAAAAAMAAAAAAAAAAAAAAExAAAAFwD#####AANhbmcABGFuZzIAAAAFAAAADgAAAAgA#####wH#AAA#gAAAABAAAAAAAAAAAAAAAEAIAAAAAAAAAAAIAAIAAAAUAAAAGwD#####AQAAAD+AAAABEAABRQAAAAAAAAAAAEAIAAAAAAAAAAAJAAFAfQgAAAAAAEBphmZmZmZmAAAAGwD#####AQAAAD+AAAAAEAAAAAAAAAAAAAAAQAgAAAAAAAAAAAkAAUCAlAAAAAAAQG6mZmZmZmb#####AAAAAQAJQ0NlcmNsZU9BAP####8BAAAAP4AAAAAAAAEAAAAgAAAAIQAAABcA#####wAIcm90YXRpb24AAjkwAAAADEBWgAAAAAAAAAAAIgD#####AAAAIAAAAAUAAAAjAAAAAgD#####AAAAAD+AAAABEAABRgAAAAAAAAAAAEAIAAAAAAAAAAAJAAAAACEAAAAk#####wAAAAEADUNEZW1pRHJvaXRlT0EA#####wAAAAA#gAAAABAAAAEAAAABAAAAIAAAACUAAAAXAP####8ABGxhbmcAAzEyNQAAAAxAX0AAAAAAAAAAACIA#####wAAACAAAAAFAAAAJwAAAAIA#####wEAAAA#gAAAARAAAUcAAAAAAAAAAABACAAAAAAAAAAACQAAAAAlAAAAKAAAACcA#####wEAAAA#gAAAABAAAAEAAAABAAAAIAAAACkAAAAVAP####8BAAAAP4AAAAAQAAAAAAAAAAAAAABACAAAAAAAAAAACQABP+OPj#74bbIAAAAmAAAAAwD#####AQAAAD+AAAAAAAABAAAAIAAAACsAAAApAAAAIwD#####AQAAAD7mZmYAAAAAAAUAAAAsAAAABAD#####AQAAAD+AAAAAAAABAAAAJQAAAAUAAAABAAAAAAoA#####wEAAAA#gAAAABAAAAEAAAABAAAAJQAAACYAAAAHAP####8AAAAvAAAALgAAAAgA#####wEAAAA#gAAAABAAAAAAAAAAAAAAAEAIAAAAAAAAAAAJAAIAAAAwAAAACAD#####AQAAAD+AAAAAEAAAAAAAAAAAAAAAQAgAAAAAAAAAAAkAAQAAADAAAAAlAP####8AAAAAP4AAAADAFAAAAAAAAMAiAAAAAAAAAAFBAAAAMhAAAAAAAAAAAAAAAAAADAAAAAAAAAAAAAABZgAAAAQA#####wEAAAA#gAAAAAAAAQAAACkAAAAMP+AAAAAAAAAAAAAACgD#####AQAAAD+AAAAAEAAAAQAAAAEAAAApAAAAKgAAAAcA#####wAAADUAAAA0AAAACAD#####AQAAAD+AAAAAEAAAAAAAAAAAAAAAQAgAAAAAAAAAAAkAAQAAADYAAAAIAP####8BAAAAP4AAAAAQAAAAAAAAAAAAAABACAAAAAAAAAAACQACAAAANgAAACUA#####wEAAAA#gAAAAMAIAAAAAAAAwCYAAAAAAAAAAAAAADgQAAAAAAAAAAAAAAAAAAwAAAAAAAAAAAAAAWcAAAAfAP####8BAAAAP4AAAAAQAAABAAAAAQAAACUAAAAgAAAAKQAAAAQA#####wEAAAA#gAAAAAAAAQAAACAAAAAFAAAAAQAAAAAHAP####8AAAA6AAAAOwAAAAgA#####wEAAAA#gAAAABAAAAAAAAAAAAAAAEAIAAAAAAAAAAAJAAEAAAA8AAAACAD#####AQAAAD+AAAAAEAAAAAAAAAAAAAAAQAgAAAAAAAAAAAkAAgAAADwAAAAlAP####8AAAAAP4AAAADACAAAAAAAAMAiAAAAAAAAAAFCAAAAPhAAAAAAAAAAAAAAAAAADAAAAAAAAAAAAAABaAAAACYA#####wEAAAA#gAAAAAAAAQAAAAYAAAAfAAAABgD#####AQAAAD+AAAAAEAAAAQAAAAEAAAAYAAAAFwAAABcA#####wAJZXBhaXNzZXVyAAI2MAAAAAxATgAAAAAAAAAAABcA#####wAPbG9uZ1BldGl0ZXNHcmFkAAE4AAAADEAgAAAAAAAAAAAAFwD#####ABBsb25nTW95ZW5uZXNHcmFkAAIxNQAAAAxALgAAAAAAAAAAABcA#####wAPbG9uZ0dyYW5kZXNHcmFkAAIyNAAAAAxAOAAAAAAAAAAAAAEA#####wAAAAYAAAAUAP####8BAAAAP4AAAAEQAAABAAAAAQAAAAYBP#MzMzMzMzMAAAAVAP####8BAAAAP4AAAAEQAAJLMQAAAAAAAAAAAEAIAAAAAAAAAAAFAAFAb8AAAAAAAAAAAEcAAAAEAP####8BAAAAP4AAAAAAAAEAAAAIAAAADD#TMzMzMzMzAAAAACEA#####wEAAAA#gAAAABAAAAEAAAABAAAAHwAAAAgAAAAHAP####8AAABKAAAASQAAAAgA#####wEAAAA#gAAAABAAAAAAAAAAAAAAAEAIAAAAAAAAAAAFAAIAAABLAAAACAD#####AQAAAD+AAAAAEAAAAAAAAAAAAAAAQAgAAAAAAAAAAAUAAQAAAEsAAAAiAP####8AAAAIAAAADEBywAAAAAAAAAAAAgD#####AQAAAD+AAAAAEAAAAAAAAAAAAAAAQAgAAAAAAAAAAAUAAAAATQAAAE7#####AAAAAgAaQ01hcnF1ZUFuZ2xlT3JpZW50ZURpcmVjdGUA#####wD#AAA+5mZmAARtYXJjAAMAAAABQB877mXR5kQAAAAATQAAAAgAAABPAAAAAAcA#####wAAAEcAAABAAAAACAD#####AQAAAD+AAAAAEAAAAAAAAAAAAAAAQAgAAAAAAAAAAAUAAgAAAFEAAAAIAP####8BAAAAP4AAAAAQAAAAAAAAAAAAAABACAAAAAAAAAAABQABAAAAUQAAACIA#####wAAAAYAAAALAQAAAAUAAAAeAAAADEBWgAAAAAAAAAAAAgD#####AQAAAD+AAAAAEAABSQAAAAAAAAAAAEAIAAAAAAAAAAAFAAAAAFMAAABUAAAAFwD#####AAplcGFpc3NldXIxAAI1NQAAAAxAS4AAAAAAAAAAABcA#####wAQbG9uZ1BldGl0ZXNHcmFkMQABOAAAAAxAIAAAAAAAAAAAABcA#####wARbG9uZ01veWVubmVzR3JhZDEAAjEyAAAADEAoAAAAAAAAAAAAFwD#####ABBsb25nR3JhbmRlc0dyYWQxAAIxOAAAAAxAMgAAAAAAAAAAAAEA#####wAAAAYAAAACAP####8BpKSkP4AAAAAQAAAAAAAAAAAAAABACAAAAAAAAAAABQAAAABVAAAAWgAAAAMA#####wCkpKQ#gAAAAANhcmMAAQAAAAYAAABVAAAAWwAAAAQA#####wGkpKQ#gAAAAAAAAQAAAFUAAAAFAAAAVgEAAAAGAP####8BpKSkP4AAAAAQAAABAAAAAQAAAFsAAABVAAAABwD#####AAAAXgAAAF0AAAAIAP####8BpKSkP4AAAAAQAAJXMQAAAAAAAAAAAEAIAAAAAAAAAAAFAAIAAABfAAAAAgD#####AaSkpD+AAAAAEAAAAAAAAAAAAAAAQAgAAAAAAAAAAAUAAAAAYAAAAFoAAAADAP####8ApKSkP4AAAAAAAAEAAAAGAAAAYAAAAGEAAAAJAP####8BAAAAP4AAAAAQAAJKMQAAAAAAAAAAAEAIAAAAAAAAAAAFAAE#yEfM7MQTNgAAAFwAAAAKAP####8BpKSkP4AAAAAQAAABAAAAAQAAAFUAAABeAAAABAD#####AaSkpD+AAAAAAAABAAAAVQAAAAsDAAAABQAAAFYAAAAMQAAAAAAAAAABAAAABwD#####AAAAZAAAAGUAAAAIAP####8BpKSkP4AAAAAQAAAAAAAAAAAAAABACAAAAAAAAAAABQABAAAAZgAAAA0A#####wAAAFUAAABnAAAAAgD#####AaSkpD+AAAAAEAAAAAAAAAAAAAAAQAgAAAAAAAAAAAUAAAAAWwAAAGgAAAAOAP####8ApKSkP4AAAAAEcG9seQABAAAABQAAAFUAAABnAAAAaQAAAFsAAABVAAAADwD#####AAAAAD3MzM0AAAAAAAUAAABqAAAABgD#####AKSkpD+AAAAAEAAAAQAAAAEAAABbAAAAaQAAAAYA#####wCkpKQ#gAAAABAAAAEAAAABAAAAaQAAAGcAAAAGAP####8ApKSkP4AAAAAQAAABAAAAAQAAAGcAAABVAAAACQD#####AQAA#z+AAAAAEAAAAAAAAAAAAAAAQAgAAAAAAAAAAAUAAT#nyXJN8Z0XAAAAXAAAAAkA#####wEAAP8#gAAAABAAAAAAAAAAAAAAAEAIAAAAAAAAAAAFAAE#57heNn0#HwAAAGIAAAAQAP####8BAAD#P4AAAAAAAAEAAABwAAAAMgAAAAAAcAAAAAIAAABwAAAAcAAAABAA#####wEAAP8#gAAAAAAAAQAAAG8AAAAyAAAAAABvAAAAAgAAAG8AAABvAAAAEQD#####AAAAAD3MzM0AAAAAAAUAAABxAAAAcgAAAAYA#####wGkpKQ#gAAAABAAAAEAAAABAAAABgAAAGMAAAAEAP####8BpKSkP4AAAAAAAAEAAABjAAAABQAAAFcBAAAABwD#####AAAAdAAAAHUAAAAIAP####8BpKSkP4AAAAAQAAAAAAAAAAAAAABACAAAAAAAAAAABQACAAAAdgAAAAYA#####wG9vb0#gAAAABAAAAEAAAABAAAAYwAAAHcAAAASAP####8AZmZmP4AAAAAAAAAAeAAAAAxAZqAAAAAAAAAAAGMAAAAGAAAAYwAAAHQAAAB1AAAAdgAAAHcAAAB4AAAABAD#####Ab29vT+AAAAAAAABAAAAYwAAAAUAAABZAQAAAAcA#####wAAAHQAAAB6AAAACAD#####Ab29vT+AAAAAEAAAAAAAAAAAAAAAQAgAAAAAAAAAAAUAAgAAAHsAAAAGAP####8BhISEP4AAAAAQAAABAAAAAQAAAHwAAABjAAAABAD#####AYSEhD+AAAAAAAABAAAAYwAAAAUAAABYAQAAAAcA#####wAAAHQAAAB+AAAACAD#####AYSEhD+AAAAAEAAAAAAAAAAAAAAAQAgAAAAAAAAAAAUAAgAAAH8AAAAGAP####8BhISEP4AAAAAQAAABAAAAAQAAAIAAAABjAAAAEgD#####AISEhD+AAAAAAAAAAIEAAAAMQEKAAAAAAAAAAABjAAAABgAAAGMAAAB0AAAAfgAAAH8AAACAAAAAgQAAABIA#####wCEhIQ#gAAAAAAAAAB9AAAADEAzAAAAAAAAAAAAYwAAAAYAAABjAAAAdAAAAHoAAAB7AAAAfAAAAH0AAAATAP####8ABGFuZzEAAABVAAAABgAAAGMAAAAUAP####8BAAAAP4AAAAAQAAABAAAAAQAAAAYBP#AAAAAAAAAAAAAVAP####8BAAAAP4AAAAAQAAFLAAAAAAAAAAAAQAgAAAAAAAAAAAUAAUBvwAAAAAAAAAAAhQAAABMA#####wAFYW5nMTEAAACGAAAABgAAAGMAAAAEAP####8BAAAAP4AAAAAAAAEAAAB8AAAADEAoAAAAAAAAAQAAAAcA#####wAAAHQAAACIAAAACAD#####AQAAAD+AAAAAEAAAAAAAAAAAAAAAQAgAAAAAAAAAAAUAAgAAAIkAAAAWAP####8BAAAAP4AAAAEAAAAAAIoMAAAAAAABAAAAAQAAAAsBAAAABQAAAIcAAAAMQFaAAAAAAAAAAAAAAAAAAACEAAAAEgD#####AAAAAD+AAAAAAAAAAIsAAAAMQDMAAAAAAAAAAABjAAAACwAAAGMAAAB0AAAAegAAAHsAAAB8AAAAhAAAAIcAAACIAAAAiQAAAIoAAACLAAAAFwD#####AAdhbmdzdXAxAAgxODAtYW5nMQAAAAsBAAAADEBmgAAAAAAAAAAABQAAAIQAAAAYAP####8ABGFiczEAAAAGAAAAVQAAAGAAAAAZAP####8AAAAGAAAABQAAAI4AAAACAP####8BAAAAP4AAAAAQAAAAAAAAAAAAAABACAAAAAAAAAAABQAAAABjAAAAjwAAABYA#####wEAAAA#gAAAAQAAAAAAkAsAAAAAAAEAAAACAAAACwEAAAAFAAAAhwAAAAxAVoAAAAAAAAAAAAAAAAAAAI0AAAASAP####8AAAAAP4AAAAAAAAAAkQAAAAxAMwAAAAAAAAAAAGMAAAAGAAAAYwAAAIQAAACHAAAAjQAAAJAAAACRAAAABgD#####AKSkpD+AAAAAEAAAAQAAAAEAAABgAAAAYQAAABsA#####wEAAAA#gAAAABAAAAAAAAAAAAAAAEAIAAAAAAAAAAAFAAFAYtAAAAAAAEB+4zMzMzMzAAAAAgD#####AQAAAD+AAAAAEAABRAAAAAAAAAAAAEAIAAAAAAAAAAAFAAAAACAAAAAKAAAAHwD#####AQAAAD+AAAAAEAAAAQAAAAEAAAAlAAAAIAAAAJUAAAAVAP####8BAAAAP4AAAAAQAAAAAAAAAAAAAAA#4zMzMzMzMwAABQABQGk+QpYO3ckAAACWAAAAEwD#####AARhbmdBAAAAlQAAACAAAAAlAAAAFgD#####AQAAAD+AAAAAQAgAAAAAAAA#8AAAAAAAAAAAAAAAlw8AAAAAAAEAAAABAAAADAAAAAAAAAAAAAAAAALCsAAAAACYAAAAIAD#####AQAAAD7mZmYAAAABAAAAAUBCW4kJK4+#AAAAAJUAAAAgAAAAJQAAAAAfAP####8BAAAAP4AAAAAQAAABAAAAAQAAACkAAAAgAAAAlQAAABUA#####wEAAAA#gAAAABAAAAAAAAAAAAAAAD#jMzMzMzMzAAAFAAFAaT5Clg7dyQAAAJsAAAATAP####8ABGFuZ0IAAACVAAAAIAAAACkAAAAWAP####8BAAAAP4AAAABACAAAAAAAAD#wAAAAAAAAAAAAAACcDwAAAAAAAQAAAAEAAAAMAAAAAAAAAAAAAAAAAsKwAAAAAJ0AAAAgAP####8BAAAAPuZmZgAAAAEAAAABQEJbiQkrj78AAAAAlQAAACAAAAApAAAAABcA#####wAFbGFuZ0EABGFuZ0EAAAAFAAAAmAAAABcA#####wAFbGFuZ0IABGFuZ0IAAAAFAAAAnQAAAAQA#####wB#f38#gAAAAAAAAgAAAAYAAAALAwAAAAUAAAABAAAADEAAAAAAAAAAAAAAAAcA#####wAAACoAAABAAAAACAD#####AQAA#z+AAAAAEAAAAAAAAAAAAAAAQAgAAAAAAAAAAAUAAQAAAKMAAAAIAP####8BAAD#P4AAAAAQAAAAAAAAAAAAAABACAAAAAAAAAAABQACAAAAowAAAAcA#####wAAACYAAABAAAAACAD#####AQAA#z+AAAAAEAAAAAAAAAAAAAAAQAgAAAAAAAAAAAUAAQAAAKYAAAAIAP####8BAAD#P4AAAAAQAAAAAAAAAAAAAABACAAAAAAAAAAABQACAAAApgAAABsA#####wEAAP8#gAAAABAAAAAAAAAAAAAAAEAIAAAAAAAAAAAFAAFAdNgAAAAAAEB7YzMzMzMzAAAAGwD#####AQAA#z+AAAAAEAAAAAAAAAAAAAAAQAgAAAAAAAAAAAUAAUB02AAAAAAAQHtjMzMzMzMAAAAdAP####8AAAD#P4AAAAAQAAABAAAAAgAAAKkAAACqAAAAAB4A#####wAAAKsAAAACAP####8AAAAAP4AAAAEQAAFKAAAAAAAAAAAAQAgAAAAAAAAAAAkAAAAAIAAAAKwAAAAbAP####8BAAAAP4AAAAAQAAAAAAAAAAAAAABACAAAAAAAAAAABQABQINgAAAAAABAZRwo9cKPXAAAACcA#####wEAAAA#gAAAABAAAAEAAAABAAAArQAAAK4AAAAbAP####8BAAD#P4AAAAEQAAFIAAAAAAAAAAAAQAgAAAAAAAAAAAkAAUCBMAAAAAAAQHEOFHrhR64AAAAnAP####8BAAD#P4AAAAAQAAABAAAAAwAAAK0AAACwAAAAFQD#####Af8AAD+AAAAAEAAAAAAAAAAAAAAAQAgAAAAAAAAAAAkAAT#hANwLhC10AAAAsQAAAAoA#####wEAAAA#gAAAABAAAAEAAAACAAAAsgAAALEAAAAEAP####8BAAAAP4AAAAAAAAIAAACyAAAADD#gAAAAAAAAAAAAAAcA#####wAAALMAAAC0AAAACAD#####Af8AAD+AAAAAEAAAAAAAAAAAAAAAQAgAAAAAAAAAAAkAAgAAALUAAAAIAP####8B#wAAP4AAAAAQAAAAAAAAAAAAAABACAAAAAAAAAAACQABAAAAtQAAACUA#####wH#AAA#gAAAAMAQAAAAAAAAwCgAAAAAAAAAAAAAALcQAAAAAAAAAAAAAAAAAAwAAAAAAAAAAAAAAWf#####AAAAAQAUQ0ltcGxlbWVudGF0aW9uUHJvdG8A#####wAbTWVzdXJlIGQnYW5nbGUgbm9uIG9yaWVudMOpAAAAAgAAAAMAAAADAAAAsAAAAK0AAAAlAAAAHwAAAAC5AQAAAD+AAAAAEAAAAQAAAAEAAACwAAAArQAAACUAAAAVAAAAALkBAAAAP4AAAAAQAAABAAAFAAFAbyWoZaxxqQAAALr#####AAAAAQAXQ01lc3VyZUFuZ2xlR2VvbWV0cmlxdWUBAAAAuQAAALAAAACtAAAAJf####8AAAACABdDTWFycXVlQW5nbGVHZW9tZXRyaXF1ZQEAAAC5AQAAAD7mZmYAAAAFAAAAAUBFBhd#VJG7AAAAALAAAACtAAAAJQAAABYBAAAAuQEAAAA#gAAAAEAIAAAAAAAAAAAAAAAAAAAAAAAAALsPAAAAAAABAAAAAQAAAAwAAAAAAAAAAAAAAAACwrAAAAAAvAAAABcA#####wAHbGFuZ2ZpbgADSEpGAAAABQAAALwAAAAXAP####8ACGxhbmdmYXV4AAMtMTD#####AAAAAQAMQ01vaW5zVW5haXJlAAAADEAkAAAAAAAAAAAAIgD#####AAAArQAAAAUAAADAAAAAAgD#####AQAAAD+AAAAAEAAAAAAAAAAAAAAAQAgAAAAAAAAAAAkAAAAAJQAAAMEAAAAnAP####8B9IpEP4AAAAAQAAABAAAABQAAAK0AAADCAAAAFQD#####AQAAAD+AAAAAEAAAAAAAAAAAAAAAQAgAAAAAAAAAAAkAAT#zSWTbIGGgAAAAwwAAAAoA#####wEAAAA#gAAAABAAAAEAAAAFAAAAxAAAAMMAAAAEAP####8BAAAAP4AAAAAAAAUAAADEAAAABQAAAAEAAAAABwD#####AAAAxQAAAMYAAAAIAP####8BAAAAP4AAAAAQAAAAAAAAAAAAAABACAAAAAAAAAAAAwACAAAAxwAAAAgA#####wEAAAA#gAAAABAAAAAAAAAAAAAAAEAIAAAAAAAAAAADAAEAAADHAAAAJQD#####AQAAAD+AAAAAwBAAAAAAAADAJAAAAAAAAAAAAAAAyRAAAAAAAAAAAAAAAAAADAAAAAAAAAAAAAABawAAAAQA#####wFmZmY#gAAAAAAAAwAAALAAAAAFAAAAAQAAAAAKAP####8BZmZmP4AAAAAQAAABAAAAAwAAALAAAACxAAAABwD#####AAAAzAAAAMsAAAAIAP####8BAAAAP4AAAAAQAAAAAAAAAAAAAABACAAAAAAAAAAACQABAAAAzQAAAAgA#####wFmZmY#gAAAABAAAAAAAAAAAAAAAEAIAAAAAAAAAAAJAAIAAADNAAAAJQD#####AQAA#z+AAAAAwBAAAAAAAADAIAAAAAAAAAAAAAAAzhAAAAAAAAAAAAAAAAAADAAAAAAAAAAAAAABZv####8AAAABAA5DT2JqZXREdXBsaXF1ZQD#####AQAA#z+AAAAAAAAAALAAAAABAP####8AAACtAAAAAgD#####AQAAAD+AAAAAEAAAAAAAAAAAAAAAQAgAAAAAAAAAAAUAAAAAJQAAANIAAAAnAP####8BAAAAP4AAAAAQAAABAAVkZW1pZAABAAAArQAAANMAAAAF##########8='
      const yy = j3pCreeSVG(stor.lesdiv.zonefig, { id: svgId, width: 720, height: 400 })
      yy.style.border = 'solid black 1px'
      stor.mtgAppLecteur.removeAllDoc()
      stor.mtgAppLecteur.addDoc(svgId, txtfigure, true)
      stor.mtgAppLecteur.giveFormula2(svgId, 'rotation', stor.rotation)
      stor.mtgAppLecteur.giveFormula2(svgId, 'lang', stor.deplacement)
      stor.mtgAppLecteur.setText(svgId, '#A', stor.A, true)
      stor.mtgAppLecteur.setText(svgId, '#B', stor.B, true)
      stor.mtgAppLecteur.calculateAndDisplayAll(true)
      if (stor.Lexo.plus) stor.mtgAppLecteur.setVisible(svgId, '#demid', true, true)
    }
    if (stor.encours === 'p') {
      stor.mtgAppLecteur.addCallBackToSVGListener(svgId, 'mousemove', function (ev) {
        if (stor.encours !== 'p') return
        mousemove(ev)
      })
      stor.mtgAppLecteur.addCallBackToSVGListener(svgId, 'mouseup', function () {
        if (stor.encours !== 'p') return
        stor.captured = false
        stor.captured2 = false
      })

      stor.mtgAppLecteur.addEventListener(svgId, '#arc', 'mousedown', function (ev) {
        // app.setLineStyle(svgId, 4, 1, 5, true)
        stor.captured = true
        stor.x = ev.clientX
        stor.y = ev.clientY
        ev.preventDefault()
      })

      stor.mtgAppLecteur.addEventListener(svgId, '#poly', 'mousedown', function (ev) {
        // app.setLineStyle(svgId, 4, 1, 5, true)
        stor.captured = true
        stor.x = ev.clientX
        stor.y = ev.clientY
        ev.preventDefault()
      })

      stor.mtgAppLecteur.addEventListener(svgId, '#arc', 'mouseover', function () {
        stor.mtgAppLecteur.getDoc(svgId).defaultCursor = 'pointer'
        stor.mtgAppLecteur.setVisible(svgId, '#pt', true, true)
        stor.mtgAppLecteur.setVisible(svgId, '#marc', true, true)
      })

      stor.mtgAppLecteur.addEventListener(svgId, '#poly', 'mouseover', function () {
        stor.mtgAppLecteur.getDoc(svgId).defaultCursor = 'pointer'
        stor.mtgAppLecteur.setVisible(svgId, '#pt', true, true)
        stor.mtgAppLecteur.setVisible(svgId, '#marc', true, true)
      })

      stor.mtgAppLecteur.addEventListener(svgId, '#arc', 'mouseout', function () {
        stor.mtgAppLecteur.getDoc(svgId).defaultCursor = null
        stor.mtgAppLecteur.setVisible(svgId, '#pt', false, true)
        stor.mtgAppLecteur.setVisible(svgId, '#marc', false, true)
      })

      stor.mtgAppLecteur.addEventListener(svgId, '#poly', 'mouseout', function () {
        stor.mtgAppLecteur.getDoc(svgId).defaultCursor = null
        stor.mtgAppLecteur.setVisible(svgId, '#pt', false, true)
        stor.mtgAppLecteur.setVisible(svgId, '#marc', false, true)
      })
      stor.mtgAppLecteur.addEventListener(svgId, '#arc2', 'mouseover', function () {
        stor.mtgAppLecteur.setVisible(svgId, '#pt', true, true)
        stor.mtgAppLecteur.setVisible(svgId, '#marc', true, true)
      })
      stor.mtgAppLecteur.addEventListener(svgId, '#arc2', 'mouseout', function () {
        stor.mtgAppLecteur.setVisible(svgId, '#pt', false, true)
        stor.mtgAppLecteur.setVisible(svgId, '#marc', false, true)
      })
    }
    if (stor.encours === 'g') {
      stor.mtgAppLecteur.setVisible(svgId, '#pt', false, true)
      stor.mtgAppLecteur.setVisible(svgId, '#marc', false, true)
      const yui = addDefaultTable(stor.lesdiv.zonedrep, 3, 1)
      yui[1][0].innerHTML = 'ou'
      stor.ClikInt = j3pAjouteBouton(yui[0][0], gradInt, { className: 'ok', value: 'Intérieures' })
      stor.ClikExt = j3pAjouteBouton(yui[2][0], gradExt, { className: 'ok', value: 'Extérieures' })
    }
    if (((stor.encours === 'm') || (stor.encours === 'g')) && !stor.pose) {
      stor.pose = true
      stor.ptDep = stor.mtgAppLecteur.getPointPosition(svgId, 32)
      stor.mtgAppLecteur.setPointPosition(svgId, 6, stor.ptDep.x, stor.ptDep.y, true)

      if (stor.afaire[0] !== 'p') stor.choixA = j3pGetRandomBool()
      let ty
      if (stor.choixA) {
        ty = stor.mtgAppLecteur.valueOf(svgId, 'langA') + 90
        stor.mtgAppLecteur.giveFormula2(svgId, 'lang', stor.deplacement)
      } else {
        ty = stor.mtgAppLecteur.valueOf(svgId, 'langA') - 90
        stor.mtgAppLecteur.giveFormula2(svgId, 'lang', -stor.deplacement)
      }
      stor.mtgAppLecteur.giveFormula2(svgId, 'ang', ty)

      stor.mtgAppLecteur.calculateAndDisplayAll(true)

      // prends coordonnées des 4 points
      const pt1 = stor.mtgAppLecteur.getPointPosition(svgId, 167)
      const pt2 = stor.mtgAppLecteur.getPointPosition(svgId, 164)
      stor.ptDep = stor.mtgAppLecteur.getPointPosition(svgId, 'B')
      stor.xmin = Math.min(pt1.x, pt2.x, stor.ptDep.x)
      stor.ymin = Math.min(pt1.y, pt2.y, stor.ptDep.y)
      stor.xmax = Math.max(pt1.x, pt2.x, stor.ptDep.x)
      stor.ymax = Math.max(pt1.y, pt2.y, stor.ptDep.y)
      /// calcul le ratio x , calcul le ratio y, prend le min
      stor.taillex = stor.xmax - stor.xmin + 30
      stor.tailley = stor.ymax - stor.ymin + 30
      const ratio1 = 370 / stor.tailley
      const ratio2 = 600 / stor.taillex
      stor.ratio = Math.min(ratio1, ratio2)
      /// calcul coordonnées du centre
      // calclu depx et depy pour que B arrive au centre
      stor.mil = { x: (stor.xmin + stor.xmax) / 2, y: (stor.ymin + stor.ymax) / 2 }
      stor.depx = 300 - stor.mil.x
      stor.depy = 195 - stor.mil.y
      stor.lanceEtape = 'A'
      RapCorrige('mm')
      setTimeout(LanceAnimeZoom, 1)
    }
    if (stor.encours === 'm') {
      stor.mtgAppLecteur.setVisible(svgId, '#pt', false, true)
      stor.mtgAppLecteur.setVisible(svgId, '#marc', false, true)
      if (!stor.zommfait) { stor.oublipa = true } else { faisM() }
    }
    setTimeout(() => { stor.mtgAppLecteur.updateFigure(svgId) }, 10)
  }

  function faisM () {
    stor.mtgAppLecteur.setVisible(svgId, 176, true)
    stor.mtgAppLecteur.setVisible(svgId, 177, true)
    stor.mtgAppLecteur.setVisible(svgId, 208, true)
    stor.mtgAppLecteur.setText(svgId, 208, stor.C, true)
    stor.mtgAppLecteur.setVisible(svgId, '#pt', false, true)
    stor.mtgAppLecteur.setVisible(svgId, '#marc', false, true)
    stor.mtgAppLecteur.setPointPosition(svgId, 176, 300, 195, true)
    stor.oublipa = false
    me.afficheBoutonValider()
  }
  function LanceAnimeZoom () {
    let pt1, pt2, pt3, DEPX, DEPY
    if (stor.zommfait) {
      me.afficheBoutonValider()
      return
    }
    if (stor.afaire[0] !== 'p') {
      pt1 = stor.mtgAppLecteur.getPointPosition(svgId, 6)
      pt2 = stor.mtgAppLecteur.getPointPosition(svgId, 32)
      pt3 = stor.mtgAppLecteur.getPointPosition(svgId, 33)
      stor.mtgAppLecteur.setPointPosition(svgId, 6, pt1.x + stor.depx, pt1.y + stor.depy, true)
      stor.mtgAppLecteur.setPointPosition(svgId, 32, pt2.x + stor.depx, pt2.y + stor.depy, true)
      stor.mtgAppLecteur.setPointPosition(svgId, 33, pt3.x + stor.depx, pt3.y + stor.depy, true)
      stor.mtgAppLecteur.zoom(svgId, 300, 195, stor.ratio, true)
      stor.mtgAppLecteur.giveFormula2(svgId, 'distee', 0.5 / stor.ratio)
      stor.mtgAppLecteur.calculateAndDisplayAll(true)
      me.afficheBoutonValider()
      stor.zommfait = true
      if (stor.oublipa) faisM()
      return
    }
    switch (stor.lanceEtape) {
      case 'A':
        pt1 = stor.mtgAppLecteur.getPointPosition(svgId, 6)
        pt2 = stor.mtgAppLecteur.getPointPosition(svgId, 32)
        pt3 = stor.mtgAppLecteur.getPointPosition(svgId, 33)
        DEPX = 0
        DEPY = 0
        if ((Math.abs(pt1.x - (stor.ptDep.x + stor.depx)) < 2) && (Math.abs(pt1.y - (stor.ptDep.y + stor.depy)) < 2)) {
          stor.lanceEtape = 'Z1'
          setTimeout(LanceAnimeZoom, stor.tempodeb)
          return
        }
        if (Math.abs(pt1.x - (stor.ptDep.x + stor.depx)) > 2) {
          if (pt1.x < stor.ptDep.x + stor.depx) DEPX = 2
          if (pt1.x > stor.ptDep.x + stor.depx) DEPX = -2
        }
        if (Math.abs(pt1.y - (stor.ptDep.y + stor.depy)) > 2) {
          if (pt1.y < stor.ptDep.y + stor.depy) DEPY = 2
          if (pt1.y > stor.ptDep.y + stor.depy) DEPY = -2
        }
        stor.mtgAppLecteur.setPointPosition(svgId, 6, pt1.x + DEPX, pt1.y + DEPY, true)
        stor.mtgAppLecteur.setPointPosition(svgId, 32, pt2.x + DEPX, pt2.y + DEPY, true)
        stor.mtgAppLecteur.setPointPosition(svgId, 33, pt3.x + DEPX, pt3.y + DEPY, true)
        setTimeout(LanceAnimeZoom, stor.tempo - 10)
        break
      case 'Z1':
        stor.mtgAppLecteur.zoom(svgId, 300, 195, Math.cbrt(Math.cbrt(stor.ratio)), true)
        stor.lanceEtape = 'Z2'
        stor.mtgAppLecteur.giveFormula2(svgId, 'distee', 0.5 / Math.cbrt(Math.cbrt(stor.ratio)))
        setTimeout(LanceAnimeZoom, stor.tempo * 2)
        return
      case 'Z2':
        stor.mtgAppLecteur.zoom(svgId, 300, 195, Math.cbrt(Math.cbrt(stor.ratio)), true)
        stor.lanceEtape = 'Z3'
        stor.mtgAppLecteur.giveFormula2(svgId, 'distee', 0.5 / Math.cbrt(Math.cbrt(stor.ratio)))
        setTimeout(LanceAnimeZoom, stor.tempo * 2)
        return
      case 'Z3':
        stor.mtgAppLecteur.zoom(svgId, 300, 195, Math.cbrt(Math.cbrt(stor.ratio)), true)
        stor.mtgAppLecteur.giveFormula2(svgId, 'distee', 0.5 / Math.cbrt(Math.cbrt(stor.ratio)))
        stor.lanceEtape = 'Z4'
        setTimeout(LanceAnimeZoom, stor.tempo * 2)
        return
      case 'Z4':
        stor.mtgAppLecteur.zoom(svgId, 300, 195, Math.cbrt(Math.cbrt(stor.ratio)), true)
        stor.mtgAppLecteur.giveFormula2(svgId, 'distee', 0.5 / Math.cbrt(Math.cbrt(stor.ratio)))
        stor.lanceEtape = 'Z5'
        setTimeout(LanceAnimeZoom, stor.tempo * 2)
        return
      case 'Z5':
        stor.mtgAppLecteur.zoom(svgId, 300, 195, Math.cbrt(Math.cbrt(stor.ratio)), true)
        stor.mtgAppLecteur.giveFormula2(svgId, 'distee', 0.5 / Math.cbrt(Math.cbrt(stor.ratio)))
        stor.lanceEtape = 'Z6'
        setTimeout(LanceAnimeZoom, stor.tempo * 2)
        return
      case 'Z6':
        stor.mtgAppLecteur.zoom(svgId, 300, 195, Math.cbrt(Math.cbrt(stor.ratio)), true)
        stor.mtgAppLecteur.giveFormula2(svgId, 'distee', 0.5 / Math.cbrt(Math.cbrt(stor.ratio)))
        stor.lanceEtape = 'Z7'
        setTimeout(LanceAnimeZoom, stor.tempo * 2)
        return
      case 'Z7':
        stor.mtgAppLecteur.zoom(svgId, 300, 195, Math.cbrt(Math.cbrt(stor.ratio)), true)
        stor.mtgAppLecteur.giveFormula2(svgId, 'distee', 0.5 / Math.cbrt(Math.cbrt(stor.ratio)))
        stor.lanceEtape = 'Z8'
        setTimeout(LanceAnimeZoom, stor.tempo * 2)
        return
      case 'Z8':
        stor.mtgAppLecteur.zoom(svgId, 300, 195, Math.cbrt(Math.cbrt(stor.ratio)), true)
        stor.mtgAppLecteur.giveFormula2(svgId, 'distee', 0.5 / Math.cbrt(Math.cbrt(stor.ratio)))
        stor.lanceEtape = 'Z9'
        setTimeout(LanceAnimeZoom, stor.tempo * 2)
        return
      case 'Z9':
        stor.mtgAppLecteur.zoom(svgId, 300, 195, Math.cbrt(Math.cbrt(stor.ratio)), true)
        stor.lanceEtape = 'F'
        stor.mtgAppLecteur.giveFormula2(svgId, 'distee', 0.5 / stor.ratio)
        stor.mtgAppLecteur.calculateAndDisplayAll(true)
        setTimeout(LanceAnimeZoom, stor.tempo)
        break
      case 'F':
        stor.zommfait = true
        if (stor.oublipa) faisM()
        me.afficheBoutonValider()
    }
  }
  function gradInt () {
    stor.captured2 = true
    stor.captured = false
    stor.mtgAppLecteur.setColor(svgId, 140, 0, 0, 0, true)
    stor.mtgAppLecteur.setColor(svgId, 146, 255, 0, 0, true)
    stor.mtgAppLecteur.updateFigure(svgId)
  }
  function gradExt () {
    stor.captured = true
    stor.captured2 = false
    stor.mtgAppLecteur.setColor(svgId, 140, 255, 0, 0, true)
    stor.mtgAppLecteur.setColor(svgId, 146, 0, 0, 0, true)
    stor.mtgAppLecteur.updateFigure(svgId)
  }
  function distangle (a1, a2) {
    const g = Math.max(a1, a2)
    const p = Math.min(a1, a2)
    if ((g - p) < (360 - g + p)) return { r: g - p, s: true }
    return { r: 360 - g + p, s: false }
  }
  function mousemove (ev) {
    if (stor.encours !== 'p') return
    if (stor.captured) {
      const newx = ev.clientX
      const newy = ev.clientY
      const deltax = newx - stor.x
      const deltay = newy - stor.y
      stor.x = newx
      stor.y = newy
      stor.mtgAppLecteur.translatePoint(svgId, 6, deltax, deltay, true)
      stor.mtgAppLecteur.updateFigure(svgId)
      RapCorrige()
    }
    if (stor.captured2) RapCorrige()
  }

  function dist (p1, p2) {
    return Math.sqrt((p1.x - p2.x) * (p1.x - p2.x) + (p1.y - p2.y) * (p1.y - p2.y))
  }
  function RapCorrige (cool) {
    let lacool = [200, 200, 200]
    if (cool === true) lacool = [0, 138, 115]
    if (cool === false) lacool = [214, 71, 0]
    if (cool === 'jk') lacool = [70, 70, 200]
    stor.mtgAppLecteur.setColor(svgId, 115, lacool[0], lacool[1], lacool[2], true, 0.5)
    stor.mtgAppLecteur.setColor(svgId, 107, lacool[0], lacool[1], lacool[2], true, 0.5)
    stor.mtgAppLecteur.setVisible(svgId, 162, (cool === false) || (cool === 'jk') || (cool === 'mm'), true)
    if (cool === 'mm') {
      stor.mtgAppLecteur.setVisible(svgId, 6, false, true)
    }
    stor.mtgAppLecteur.updateFigure(svgId)
  }
  function yaReponse () {
    if (stor.encours === 'p') return true
    if (stor.encours === 'g') {
      if (!(stor.captured || stor.captured2)) stor.ClikInt.focus()
      return (stor.captured || stor.captured2)
    }
    if (stor.encours === 'm') {
      const ptt = stor.mtgAppLecteur.getPointPosition(svgId, 176)
      return ptt.x !== 300 || ptt.y !== 195
    }
  }
  function isRepOk () {
    stor.errCentre = false
    stor.errCentreP = false
    stor.err0 = false
    stor.err0P = false
    stor.err0I = false
    stor.errGraduations = false
    stor.errM = false
    stor.errMEcrit = false
    stor.errMEcritkoi = ''
    stor.errMG = false

    if (stor.encours === 'p') {
      const pt = stor.mtgAppLecteur.getPointPosition(svgId, 'B')
      const pt3 = stor.mtgAppLecteur.getPointPosition(svgId, 32)
      if (dist(pt, pt3) > 3) {
        if (dist(pt, pt3) < 5) {
          stor.errCentreP = true
          return false
        }
        stor.errCentre = true
        return false
      }
      const a1 = stor.mtgAppLecteur.valueOf(svgId, 'langA')
      // var a2 = stor.mtgAppLecteur.valueOf(svgId, 'langB')
      let a3 = stor.mtgAppLecteur.valueOf(svgId, 'ang') - 90
      let a4 = a3 + 180
      if (a4 < -180) a4 += 360
      if (a4 > 180) a4 -= 360
      if (a3 < -180) a3 += 360
      if (a3 > 180) a3 -= 360
      if (Math.abs(a1 - a3) < 1.5) {
        stor.choixA = true
        return true
      }
      if (Math.abs(a1 - a3) < 3) {
        stor.err0P = true
        return false
      }
      if (Math.abs(a1 - a4) < 1.5) {
        stor.choixA = false
        return true
      }
      if (Math.abs(a1 - a4) < 3) {
        stor.err0P = true
        return false
      }
      return false
    }
    if (stor.encours === 'g') {
      return (stor.choixA === stor.captured) || (stor.deplacement === 180)
    }
    if (stor.encours === 'm') {
      const kl = j3pArrondi(stor.mtgAppLecteur.valueOf(svgId, 'langfin'), 0)
      if (kl !== stor.deplacement) {
        const lacool = [214, 71, 0]
        stor.mtgAppLecteur.setColor(svgId, 176, lacool[0], lacool[1], lacool[2], true)
        stor.mtgAppLecteur.setColor(svgId, 177, lacool[0], lacool[1], lacool[2], true)
        stor.mtgAppLecteur.setColor(svgId, 183, lacool[0], lacool[1], lacool[2], true)
      }
      return kl === stor.deplacement
    }

    stor.lesdiv.explications.classList.remove('explique')
    stor.lesdiv.solution.classList.remove('correction')

    return true
  }
  function desactivezone () {
    if (stor.encours === 'p') {
      stor.mtgAppLecteur.removeEventListener(svgId, 92, 'mousedown')
      stor.mtgAppLecteur.removeEventListener(svgId, '#poly', 'mousedown')
      stor.mtgAppLecteur.removeEventListener(svgId, 92, 'mouseover')
      stor.mtgAppLecteur.removeEventListener(svgId, '#poly', 'mouseover')
      stor.mtgAppLecteur.removeEventListener(svgId, 92, 'mouseout')
      stor.mtgAppLecteur.removeEventListener(svgId, '#poly', 'mouseout')
      stor.mtgAppLecteur.removeEventListener(svgId, '#arc2', 'mouseenter')
      stor.mtgAppLecteur.removeEventListener(svgId, '#arc2', 'mouseout')
      stor.mtgAppLecteur.setVisible(svgId, '#pt', false, true)
      stor.mtgAppLecteur.setVisible(svgId, 136, false, true)
      stor.mtgAppLecteur.setVisible(svgId, 6, false, true)
      stor.mtgAppLecteur.setVisible(svgId, '#marc', false, true)
      stor.mtgAppLecteur.getDoc(svgId).defaultCursor = null
    }
    if (stor.encours === 'g') j3pEmpty(stor.lesdiv.zonedrep)
    if (stor.encours === 'm') {
      stor.mtgAppLecteur.removeEventListener(svgId, 120, 'mousemove')
      stor.mtgAppLecteur.getDoc(svgId).defaultCursor = null
      stor.mtgAppLecteur.removeEventListener(svgId, 'mousedown')
      stor.mtgAppLecteur.removeEventListener(svgId, 120, 'mouseout')
      stor.mtgAppLecteur.setVisible(svgId, 176, false, true)
      stor.mtgAppLecteur.setVisible(svgId, 209, true, true)
      stor.captured2 = false
    }
  }
  function passetoutvert () {
    if (stor.encours === 'p') {
      RapCorrige(true)
    }
    if (stor.encours === 'g') {
      if (stor.choixA) {
        stor.mtgAppLecteur.setColor(svgId, 140, 0, 138, 115, true)
        stor.mtgAppLecteur.setColor(svgId, 146, 0, 0, 0, true)
      } else {
        stor.mtgAppLecteur.setColor(svgId, 140, 0, 0, 0, true)
        stor.mtgAppLecteur.setColor(svgId, 146, 0, 138, 115, true)
      }
      stor.mtgAppLecteur.updateFigure(svgId)
    }
    if (stor.encours === 'm') {
      const lacool = [0, 138, 115]
      stor.mtgAppLecteur.setColor(svgId, 176, lacool[0], lacool[1], lacool[2], true)
      stor.mtgAppLecteur.setColor(svgId, 177, lacool[0], lacool[1], lacool[2], true)
      stor.mtgAppLecteur.setColor(svgId, 183, lacool[0], lacool[1], lacool[2], true)
    }
  }
  function Debannimerap () {
    stor.ptBdep = stor.mtgAppLecteur.getPointPosition(svgId, 'B')
    stor.ptCdep = stor.mtgAppLecteur.getPointPosition(svgId, 32)
    stor.ptCEncours = j3pClone(stor.ptBdep)
    stor.anggg = stor.mtgAppLecteur.valueOf(svgId, 'ang')
    stor.anngDeb = stor.anggg
    stor.AngA = stor.mtgAppLecteur.valueOf(svgId, 'langA')
    stor.AngA += 90
    if (stor.AngA > 180) stor.AngA -= 360
    if (stor.AngA < -180) stor.AngA += 360
    stor.AngB = stor.AngA + 180
    if (stor.AngB > 180) stor.AngB -= 360
    if (stor.AngB < -180) stor.AngB += 360
    const depA = distangle(stor.anngDeb, stor.AngA)
    const depB = distangle(stor.anngDeb, stor.AngB)
    stor.choixA = !(depA.r > depB.r)
    if (stor.choixA) {
      stor.s = depA.s
      stor.ANGAZ = stor.AngA
    } else {
      stor.s = depB.s
      stor.ANGAZ = stor.AngB
    }
    if (!stor.s && (stor.AngADeb < stor.ANGAZ)) stor.ANGAZ -= 360
    if (stor.s && (stor.AngADeb > stor.ANGAZ)) stor.ANGAZ += 360
    setTimeout(annimerep, stor.tempodeb)
    stor.annimCpt = 'C'
  }
  function annimerep () {
    if (stor.raz) return
    switch (stor.annimCpt) {
      case 'C':
        if ((stor.ptCEncours.x === stor.ptCdep.x) && (stor.ptCEncours.y === stor.ptCdep.y)) {
          stor.annimCpt = 'A'
          setTimeout(annimerep, stor.tempo)
          return
        }
        if (stor.ptCEncours.x < stor.ptCdep.x) stor.ptCEncours.x += stor.depCentreCo
        if (stor.ptCEncours.y < stor.ptCdep.y) stor.ptCEncours.y += stor.depCentreCo
        if (stor.ptCEncours.x > stor.ptCdep.x) stor.ptCEncours.x -= stor.depCentreCo
        if (stor.ptCEncours.y > stor.ptCdep.y) stor.ptCEncours.y -= stor.depCentreCo
        if (Math.abs(stor.ptCEncours.x - stor.ptCdep.x) < stor.depCentreCo) stor.ptCEncours.x = stor.ptCdep.x
        if (Math.abs(stor.ptCEncours.y - stor.ptCdep.y) < stor.depCentreCo) stor.ptCEncours.y = stor.ptCdep.y
        stor.mtgAppLecteur.setPointPosition(svgId, 6, stor.ptCEncours.x, stor.ptCEncours.y, true)
        setTimeout(annimerep, stor.tempo)
        break
      case 'A':
        if (stor.anggg === stor.ANGAZ) {
          stor.annimCpt = 'F'
          setTimeout(annimerep, stor.tempo)
          return
        }
        if (stor.anggg > stor.ANGAZ) stor.anggg--
        if (stor.anggg < stor.ANGAZ) stor.anggg++
        if (Math.abs(stor.anggg - stor.ANGAZ) < 1) stor.anggg = stor.ANGAZ
        stor.mtgAppLecteur.giveFormula2(svgId, 'ang', stor.anggg)
        stor.mtgAppLecteur.setPointPosition(svgId, 6, stor.ptCEncours.x, stor.ptCEncours.y, true)
        setTimeout(annimerep, stor.tempo)
        break
      case 'F':
        RapCorrige('jk')
        stor.annimCpt = 'R'
        setTimeout(annimerep, stor.tempodeb)
        break
      case 'R':
        stor.ptCEncours = j3pClone(stor.ptBdep)
        stor.anggg = stor.anngDeb
        stor.mtgAppLecteur.setPointPosition(svgId, 6, stor.ptCEncours.x, stor.ptCEncours.y, true)
        RapCorrige(false)
        stor.mtgAppLecteur.giveFormula2(svgId, 'ang', stor.anggg)
        stor.mtgAppLecteur.calculate(svgId)
        stor.mtgAppLecteur.setPointPosition(svgId, 6, stor.ptCEncours.x, stor.ptCEncours.y, true)
        stor.annimCpt = 'C'
        setTimeout(annimerep, stor.tempodeb)
        break
    }
  }
  function faisSuite () {
    stor.bouSuit = j3pAjouteBouton(me.buttonsElts.container, finsuite, { className: 'big suite', value: 'Suite' })
  }
  function finsuite () {
    j3pDetruit(stor.bouSuit)
    stor.raz = true
    poseQuestion()
    makefig()
  }

  function AffCorrection (bool) {
    if (stor.errCentre) {
      stor.compteurPe1++
      stor.yaexplik = true
      j3pAffiche(stor.lesdiv.explications, null, 'Le centre du rapporteur est mal placé !')
    }
    if (stor.errCentreP) {
      stor.compteurPe1++
      stor.yaexplik = true
      j3pAffiche(stor.lesdiv.explications, null, 'Sois plus précis pour le centre du rapporteur !')
    }
    if (stor.err0) {
      stor.compteurPe1++
      stor.yaexplik = true
      j3pAffiche(stor.lesdiv.explications, null, 'Tu dois placer un $0$ sur l’un des deux côtés !')
    }
    if (stor.err0P) {
      stor.compteurPe1++
      stor.yaexplik = true
      j3pAffiche(stor.lesdiv.explications, null, 'Le rapporteur n’est pas bien aligné avec un côté !')
    }
    if (stor.err0I) {
      stor.compteurPe1++
      stor.yaexplik = true
      j3pAffiche(stor.lesdiv.explications, null, 'Il faut placer ce $0$ sur l’autre côté !')
    }

    if (stor.encours === 'p') RapCorrige(false)
    if (stor.encours === 'm') stor.compteurPe3++
    if (bool) {
      desactivezone()
      if (stor.encours === 'p') {
        stor.raz = false
        Debannimerap()
        stor.placeok = false
      }
      if (stor.encours === 'g') {
        stor.compteurPe2 += 2
        if (stor.choixA) {
          stor.mtgAppLecteur.setColor(svgId, 140, 0, 0, 255, true)
          stor.mtgAppLecteur.setColor(svgId, 146, 214, 71, 0, true)
        } else {
          stor.mtgAppLecteur.setColor(svgId, 140, 214, 71, 0, true)
          stor.mtgAppLecteur.setColor(svgId, 146, 0, 0, 255, true)
        }
        stor.mtgAppLecteur.updateFigure(svgId)
        stor.yaexplik = true
        j3pAffiche(stor.lesdiv.explications, null, 'Regarde bien le $0$ du rapporteur utilisé !')
      }
      if (stor.encours === 'm') {
        stor.raz = false
        stor.angdeb = j3pArrondi(stor.mtgAppLecteur.valueOf(svgId, 'langfin'), 0)
        if (!stor.choixA) stor.angdeb = -stor.angdeb
        if (stor.angdeb < -180) stor.angdeb += 360
        if (stor.angdeb > 180) stor.angdeb -= 360
        if (stor.choixA) { stor.angfin = stor.deplacement } else { stor.angfin = -stor.deplacement }
        stor.angencours = 'D'
        setTimeout(lanceAnimeAngle, stor.tempodeb)
      }
    } else { me.afficheBoutonValider() }
  }
  function lanceAnimeAngle () {
    if (stor.raz) return
    switch (stor.angencours) {
      case 'D':
        stor.aencours = stor.angdeb
        stor.mtgAppLecteur.setVisible(svgId, 209, false, true)
        stor.mtgAppLecteur.giveFormula2(svgId, 'langfaux', stor.aencours)
        stor.mtgAppLecteur.setVisible(svgId, 195, true, true)
        stor.mtgAppLecteur.setColor(svgId, 201, 214, 71, 0, true)
        stor.mtgAppLecteur.setColor(svgId, 195, 214, 71, 0, true)
        stor.mtgAppLecteur.setColor(svgId, 194, 214, 71, 0, true)
        stor.mtgAppLecteur.setVisible(svgId, 176, false, true)
        stor.mtgAppLecteur.setVisible(svgId, 177, false, true)
        stor.mtgAppLecteur.setVisible(svgId, 183, false, true)
        stor.mtgAppLecteur.setVisible(svgId, 208, false, true)
        stor.mtgAppLecteur.setText(svgId, 201, stor.C, true)
        stor.mtgAppLecteur.calculateAndDisplayAll(true)
        stor.angencours = 'A'
        setTimeout(lanceAnimeAngle, stor.tempodeb)
        break
      case 'A':
        if (Math.abs(stor.angfin - stor.aencours) < 2) {
          stor.aencours = stor.angfin
          stor.mtgAppLecteur.giveFormula2(svgId, 'langfaux', stor.aencours)
          stor.mtgAppLecteur.calculateAndDisplayAll(true)
          stor.angencours = 'F'
          stor.mtgAppLecteur.setColor(svgId, 201, 70, 70, 200, true)
          stor.mtgAppLecteur.setColor(svgId, 195, 70, 70, 200, true)
          stor.mtgAppLecteur.setColor(svgId, 194, 70, 70, 200, true)
          setTimeout(lanceAnimeAngle, stor.tempodeb)
        } else {
          if (stor.aencours < stor.angfin) stor.aencours++
          if (stor.aencours > stor.angfin) stor.aencours--
          stor.mtgAppLecteur.giveFormula2(svgId, 'langfaux', stor.aencours)
          stor.mtgAppLecteur.calculateAndDisplayAll(true)
          setTimeout(lanceAnimeAngle, stor.tempo)
        }
        break
      case 'F':
        stor.angencours = 'D'
        setTimeout(lanceAnimeAngle, stor.tempodeb)
    }
  }

  function enonceMain () {
    if (me.etapeCourante === 1) {
      // A chaque répétition, on nettoie la scène
      me.videLesZones()
      creeLesDiv()
      stor.Lexo = faisChoixExos()
      stor.lesdiv.travail.classList.add('travail')
      stor.lesdiv.consigne.classList.add('enonce')
      stor.lesdiv.explications.classList.remove('explique')
      stor.lesdiv.solution.classList.remove('correction')
    }
    poseQuestion()
    makefig()
    me.finEnonce()
  }

  switch (this.etat) {
    case 'enonce':
      // code exécuté au lancement de la section
      if (me.debutDeLaSection) {
        initSection()
      } else {
        stor.raz = true
        enonceMain()
      }

      me.cacheBoutonSuite()
      break // case "enonce":

    case 'correction':
      // On teste si une réponse a été saisie
      stor.yaexplik = false
      if (stor.lesdiv === undefined) return
      stor.lesdiv.explications.innerHTML = ''
      stor.lesdiv.correction.innerHTML = ''
      stor.yaco = false

      if (stor.encours === 'g' && !stor.ClikInt) return
      if (yaReponse()) {
        // Bonne réponse
        if (isRepOk()) {
          this._stopTimer()
          stor.lesdiv.correction.style.color = this.styles.cbien
          stor.lesdiv.correction.innerHTML = cBien

          desactivezone()
          passetoutvert()
          this.cacheBoutonValider()

          if (stor.encours === 'm') {
            if (stor.afaire.length === 1) this.score++
            if (stor.afaire.length === 2) {
              if (stor.afaire[0] === 'p') {
                this.score = j3pArrondi(this.score + 0.5, 2)
              } else {
                this.score = j3pArrondi(this.score + 0.75, 2)
              }
            }
            if (stor.afaire.length === 3) this.score = j3pArrondi(this.score + 0.25, 2)
          }
          if (stor.encours === 'g') {
            if (stor.afaire.length !== 1) {
              this.score = j3pArrondi(this.score + 0.25, 2)
              stor.encours = 'm'
              faisSuite()
              me.cacheBoutonSuite()
              return
            } else { this.score++ }
          }
          if (stor.encours === 'p') {
            if (stor.afaire.length !== 1) {
              this.score = j3pArrondi(this.score + 0.5, 2)
              if (stor.afaire.length === 3) {
                stor.encours = 'g'
              } else {
                stor.encours = 'm'
              }
              faisSuite()
              me.cacheBoutonSuite()
              return
            } else { this.score++ }
          }

          this.typederreurs[0]++
          this.etat = 'navigation'
          this.sectionCourante()
        } else {
          // Pas de bonne réponse
          stor.lesdiv.correction.style.color = this.styles.cfaux
          stor.lesdiv.correction.innerHTML = cFaux
          if (this.isElapsed) {
            // A cause de la limite de temps :
            this._stopTimer()

            stor.lesdiv.correction.innerHTML += tempsDepasse + '<BR>'
            this.typederreurs[10]++
            this.cacheBoutonValider()

            AffCorrection(true)
            if (stor.encours === 'g') {
              if (stor.afaire.length !== 1) {
                stor.encours = 'm'
                faisSuite()
                me.cacheBoutonSuite()
                return
              }
            }
            if (stor.encours === 'p') {
              if (stor.afaire.length !== 1) {
                if (stor.afaire.length === 3) {
                  stor.encours = 'g'
                } else {
                  stor.encours = 'm'
                }
                faisSuite()
                me.cacheBoutonSuite()
                return
              }
            }

            this.etat = 'navigation'
            this.sectionCourante()
          } else {
            // Réponse fausse :
            if (me.essaiCourant < me.donneesSection.nbchances) {
              stor.lesdiv.correction.innerHTML += '<br>' + essaieEncore

              AffCorrection(false)

              this.typederreurs[1]++
            } else {
              // Erreur au nème essai
              this._stopTimer()
              this.cacheBoutonValider()

              AffCorrection(true)

              if (stor.encours === 'g') {
                if (stor.afaire.length !== 1) {
                  stor.encours = 'm'
                  faisSuite()
                  me.cacheBoutonSuite()
                  return
                }
              }
              if (stor.encours === 'p') {
                if (stor.afaire.length !== 1) {
                  if (stor.afaire.length === 3) {
                    stor.encours = 'g'
                  } else {
                    stor.encours = 'm'
                  }
                  faisSuite()
                  me.cacheBoutonSuite()
                  return
                }
              }

              this.typederreurs[2]++
              this.etat = 'navigation'
              this.sectionCourante()
            }
          }
        }
      } else {
        // pas de réponse
        stor.lesdiv.correction.style.color = this.styles.cfaux
        stor.lesdiv.correction.innerHTML = reponseIncomplete
        if (stor.encours === 'm') {
          stor.lesdiv.correction.innerHTML = 'Réponse incomplète ! <br> Tu dois déplacer le point ' + stor.C + ' .'
        }
        me.afficheBoutonValider()
      }

      if (stor.yaexplik) {
        stor.lesdiv.explications.classList.add('explique')
      }
      if (stor.yaco) {
        stor.lesdiv.solution.classList.add('correction')
      }

      // Obligatoire
      this.finCorrection()
      break // case "correction":

    case 'navigation':
      if (this.sectionTerminee()) {
        this.parcours.pe = getPe(ds, stor)
        me.buttonsElts.sectionSuivante.addEventListener('click', () => {
          stor.raz = true
        })
      } else {
        this.etat = 'enonce'
        me.buttonsElts.suite.addEventListener('click', () => {
          stor.raz = true
        })
      }
      this.finNavigation()
      break // case "navigation":
  }
}
