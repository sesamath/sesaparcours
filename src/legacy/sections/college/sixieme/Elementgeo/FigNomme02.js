import { j3pClone, j3pGetNewId, j3pGetRandomBool, j3pGetRandomInt, j3pShowError } from 'src/legacy/core/functions'
import { j3pCreeSVG } from 'src/legacy/core/functionsSvg'
import { legacyStyles } from 'src/legacy/core/StylesJ3p'
import { addMaj } from 'src/lib/utils/string'

import getMtgFig from './getMtgFigNomme02'

const { cbien, cfaux } = legacyStyles

const coMod = [255, 0, 191]

class FigNomme02 {
  /**
   * @param {Parcours} parcours
   */
  constructor (parcours) {
    const stor = parcours.storage
    stor.svgId = j3pGetNewId()
    stor.LeNomDuTruc = ''
    stor.blok = false

    // les propriétés de storage qu’on utilise
    this.blok = stor.blok
    /** @type {MtgAppLecteur} */
    this.mtgAppLecteur = stor.mtgAppLecteur
    this.svgId = stor.svgId
    const svgId = this.svgId // raccourci

    // autres propriétés spécifiques à AfficheFig
    this.aclick = []

    this.listedeselemedessine = []

    let nomspris = []
    // @todo définir ces variables plus près de leur usage, si possible avec des const
    let haz, ep1, d2, rel1, ok7, okp0, okp2, rel2, rel3, ok2, ok3, jh, point0, point1, point2, rela0, oks1, oks2, oks3,
      oks4, oks5, oks6, rels1, rels2, rels3, rels4, i, lettre1, lettre2, lettre3, lettre4, lettre5, lettre6, lettre8,
      ok4, ok5, ok6, typeface6, typeface2, lalettre, lalettre1, lalettre2, lalettre3, lalettre4, lalettre5, okd2, okd1,
      okd3, okd4, typeface5, typeface3, typeface4, typeface1, ok1, reld1, reld2, reld3, reld4, ok8, ok9, ok10, ok11,
      ok12, okpo1, okpo2, okpo3, okpo4, eppo1, eppo2, eppo3, eppo4, relpo1, relpo2, relpo3, relpo4, okdr1, reldrd1,
      reldrd2, rela, oka, leslettresz, fkl, ep2, rel4, rel5, rel7, rela1, rela2, oka0, oka1, oka2, okp1, okd5, okd7,
      lalettre6, lalettre7, lalettre8
    let A1 = 0
    let A2 = 0
    let A3 = 0
    let A4 = 0
    let rel6, rel8, rel9, rel10, rel11, rel12
    const specool = { over: [250, 100, 100], out: [200, 200, 200], in: [250, 150, 150] }
    let listeNom = []
    stor.listeChange = []
    stor.listCache = []
    stor.height = 300
    stor.lwidth = 380
    stor.listZig = []
    let pointhaut

    stor.lafig = getMtgFig(stor.contenant)

    switch (stor.contenant) {
      case 'SPHERE':
        stor.height = 340
        // le cercle
        stor.listZig = ['#cp1', '#cp2', '#cp3', '#cp4', '#cp5', '#cp6', '#cp7', '#cr1', '#cr2', '#cr3', '#cr4', '#cr6', '#cr5', '#cd1', '#cd2', '#cEl1', '#cEL2', '#cEL3', '#cCer', '#cEL4', '#cEL5']

        this.listedeselemedessine.push({
          type: 'ellipse3',
          nom: 'M1',
          val: ['demi-cercle', 'grand_cercle'],
          tag: '#cCer',
          tagAllume: [
            { tagA: '#C11', ep: 4, st: 0, couleur: [0, 0, 0] },
            { tagA: '#C22', ep: 4, st: 0, couleur: [0, 0, 0] },
            { tagA: '#C33', ep: 4, st: 0, couleur: [0, 0, 0] },
            { tagA: '#C44', ep: 4, st: 0, couleur: [0, 0, 0] }
          ],
          sm: true
        })

        // equateur
        // les deux ellipses
        this.listedeselemedessine.push({
          type: 'ellipse3',
          nom: 'M1',
          val: ['demi-cercle', 'equateur', 'grand_cercle'],
          tag: '#cEL2',
          tagAllume: [
            { tagA: '#equa1', ep: 3, st: 1, couleur: [0, 0, 0] },
            { tagA: '#equa2', ep: 3, st: 0, couleur: [0, 0, 0] },
            { tagA: '#equa3', ep: 3, st: 0, couleur: [0, 0, 0] },
            { tagA: '#equa4', ep: 3, st: 0, couleur: [0, 0, 0] }
          ],
          sm: true
        })

        // parallele
        // les deux ellipses
        this.listedeselemedessine.push({
          type: 'ellipse3',
          nom: 'M1',
          val: ['demi-cercle'],
          tag: '#cEl1',
          tagAllume: [
            { tagA: '#para2', ep: 2, st: 1, couleur: [0, 0, 0] },
            { tagA: '#para1', ep: 2, st: 0, couleur: [0, 0, 0] },
            { tagA: '#para4', ep: 2, st: 1, couleur: [0, 0, 0] },
            { tagA: '#para3', ep: 2, st: 0, couleur: [0, 0, 0] }
          ],
          sm: true
        })
        // 2 meridiens
        // les deux ellipses

        if (stor.element === 'grand_cercle') {
          stor.listCache.push('#cEL4', '#cEL5')
          this.listedeselemedessine.push({
            type: 'ellipse3',
            nom: 'M1',
            val: ['demi-cercle', 'meridien', 'grand_cercle'],
            tag: '#cEL3',
            tagAllume: [
              { tagA: '#meri3', ep: 2, st: 0, couleur: [0, 0, 0] },
              { tagA: '#meri4', ep: 2, st: 0, couleur: [0, 0, 0] },
              { tagA: '#meri1', ep: 2, st: 1, couleur: [0, 0, 0] },
              { tagA: '#meri2', ep: 2, st: 1, couleur: [0, 0, 0] }
            ],
            sm: true
          })
        } else {
          stor.listCache.push('#cEL3')
          this.listedeselemedessine.push({
            type: 'ellipse3',
            nom: 'M1',
            val: ['demi-cercle', 'meridien', 'grand_cercle'],
            tag: '#cEL4',
            tagAllume: [
              { tagA: '#meri1', ep: 2, st: 1, couleur: [0, 0, 0] },
              { tagA: '#meri2', ep: 2, st: 1, couleur: [0, 0, 0] }
            ],
            sm: true
          })
          this.listedeselemedessine.push({
            type: 'ellipse3',
            nom: 'M1',
            val: ['demi-cercle', 'meridien', 'grand_cercle'],
            tag: '#cEL5',
            tagAllume: [
              { tagA: '#meri3', ep: 2, st: 0, couleur: [0, 0, 0] },
              { tagA: '#meri4', ep: 2, st: 0, couleur: [0, 0, 0] }
            ],
            sm: true
          })
        }

        if (['pole', 'equateur', 'meridien', 'parallele'].indexOf(stor.element) !== -1) {
          lettre2 = 'A'
          lettre3 = 'B'
          lettre4 = 'C'
          lettre5 = 'D'
          lettre8 = 'E'
          lettre1 = 'N'
          lettre6 = 'S'
        } else {
          lettre2 = addMaj(nomspris)
          lettre3 = addMaj(nomspris)
          lettre4 = addMaj(nomspris)
          lettre5 = addMaj(nomspris)
          lettre8 = addMaj(nomspris)
          lettre1 = addMaj(nomspris)
          lettre6 = addMaj(nomspris)
        }

        this.listedeselemedessine.push({
          type: 'point',
          nom: lettre2 + 'r',
          val: ['point', 'pole'],
          tag: '#cp6',
          tagAllume: ['#sM'],
          couleur: [255, 255, 255],
          ep: false
        })
        this.listedeselemedessine.push({
          type: 'point',
          nom: lettre3,
          val: ['point'],
          tag: '#cp4',
          tagAllume: ['#sN'],
          couleur: [255, 255, 255],
          ep: false
        })
        this.listedeselemedessine.push({
          type: 'point',
          nom: lettre4,
          val: ['point', 'centre'],
          tag: '#cp3',
          tagAllume: ['#sO'],
          couleur: [255, 255, 255],
          ep: false
        })
        this.listedeselemedessine.push({
          type: 'point',
          nom: lettre5,
          val: ['point'],
          tag: '#cp7',
          tagAllume: ['#sT'],
          couleur: [255, 255, 255],
          ep: false
        })
        this.listedeselemedessine.push({
          type: 'point',
          nom: lettre8,
          val: ['point', 'pole'],
          tag: '#cp1',
          tagAllume: ['#sS'],
          couleur: [255, 255, 255],
          ep: false
        })
        this.listedeselemedessine.push({
          type: 'point',
          nom: lettre1,
          val: ['point'],
          tag: '#cp2',
          tagAllume: ['#sX'],
          couleur: [255, 255, 255],
          ep: false
        })
        this.listedeselemedessine.push({
          type: 'point',
          nom: lettre6,
          val: ['point'],
          tag: '#cp5',
          tagAllume: ['#sV'],
          couleur: [255, 255, 255],
          ep: false
        })

        oks1 = oks2 = oks3 = oks4 = oks5 = oks6 = true
        rels1 = rels2 = rels3 = rels4 = 'piege'

        if (stor.element === 'longueur_egale') {
          haz = j3pGetRandomInt(0, 1)
          if (haz === 0) {
            d2 = j3pGetRandomInt(0, 1)
            if (d2 === 0) {
              oks1 = false
              rel2 = 'longueur-egale'
            } else {
              oks2 = false
              rel1 = 'longueur_egale'
            }
          } else {
            d2 = j3pGetRandomInt(0, 5)
            rel1 = rel2 = rel3 = rel5 = rel4 = rel6 = 'longueur_egale'
            if (d2 === 0) {
              oks1 = false
            }
            if (d2 === 1) {
              oks2 = false
            }
            if (d2 === 2) {
              oks3 = false
            }
            if (d2 === 3) {
              oks4 = false
            }
            if (d2 === 4) {
              oks5 = false
            }
            if (d2 === 5) {
              oks6 = false
            }
          }
        }

        if (stor.element === 'diametre' || (stor.element === 'longueur_egale' && (haz === 0))) {
          if (oks1) {
            this.listedeselemedessine.push({
              type: 'segment',
              nom: lettre4 + lettre5,
              val: ['segment', 'diametre', rel1],
              tag: '#cd1',
              tagAllume: ['#diam1'],
              couleur: [0, 0, 0],
              ep: 2,
              st: 2
            })
          } else {
            stor.listeChange.push({ id: '#diam1', co: coMod, ep: 2, st: 2 })
          }
          if (oks2) {
            this.listedeselemedessine.push({
              type: 'segment',
              nom: lettre4 + lettre5,
              val: ['segment', 'diametre', rel2],
              tag: '#cd2',
              tagAllume: ['#diam2'],
              couleur: [0, 0, 0],
              ep: 2,
              st: 2
            })
          } else {
            stor.listeChange.push({ id: '#diam2', co: coMod, ep: 2, st: 2 })
          }
          stor.listCache.push('#r1', '#r2', '#r4', '#r5', '#cr1', '#cr2', '#cr5', '#cr6')
        } else {
          if (oks1) {
            this.listedeselemedessine.push({
              type: 'segment',
              nom: lettre4 + lettre5,
              val: ['segment', 'rayon', rel1],
              tag: '#cr2',
              tagAllume: ['#r1'],
              couleur: [0, 0, 0],
              ep: 2,
              st: 2
            })
          } else {
            stor.listeChange.push({ id: '#r1', co: coMod, ep: 2, st: 2 })
          }
          if (oks2) {
            this.listedeselemedessine.push({
              type: 'segment',
              nom: lettre4 + lettre5,
              val: ['segment', 'rayon', rel2],
              tag: '#cr1',
              tagAllume: ['#r2'],
              couleur: [0, 0, 0],
              ep: 2,
              st: 2
            })
          } else {
            stor.listeChange.push({ id: '#r2', co: coMod, ep: 2, st: 2 })
          }
          if (oks4) {
            this.listedeselemedessine.push({
              type: 'segment',
              nom: lettre4 + lettre5,
              val: ['segment', 'rayon', rel4],
              tag: '#cr6',
              tagAllume: ['#r4'],
              couleur: [0, 0, 0],
              ep: 2,
              st: 2
            })
          } else {
            stor.listeChange.push({ id: '#r4', co: coMod, ep: 2, st: 2 })
          }
          if (oks5) {
            this.listedeselemedessine.push({
              type: 'segment',
              nom: lettre4 + lettre5,
              val: ['segment', 'rayon', rel5],
              tag: '#cr5',
              tagAllume: ['#r5'],
              couleur: [0, 0, 0],
              ep: 2,
              st: 2
            })
          } else {
            stor.listeChange.push({ id: '#r5', co: coMod, ep: 2, st: 2 })
          }
          stor.listCache.push('#cd1', '#cd2', '#diam1', '#diam2')
        }

        if (oks3) {
          this.listedeselemedessine.push({
            type: 'segment',
            nom: lettre4 + lettre6,
            val: ['segment', 'rayon', rel3],
            tag: '#cr4',
            tagAllume: ['#r3'],
            couleur: [0, 0, 0],
            ep: 2,
            st: 2
          })
        } else {
          stor.listeChange.push({ id: '#r3', co: coMod, ep: 2, st: 2 })
        }
        if (oks6) {
          this.listedeselemedessine.push({
            type: 'segment',
            nom: lettre4 + lettre2,
            val: ['segment', 'rayon', rel6],
            tag: '#cr3',
            tagAllume: ['#r6'],
            couleur: [0, 0, 0],
            ep: 2,
            st: 2
          })
        } else {
          stor.listeChange.push({ id: '#r6', co: coMod, ep: 2, st: 2 })
        }

        A1 = 306
        if (['pole', 'equateur', 'meridien', 'parallele'].indexOf(stor.element) === -1) {
          A1 = j3pGetRandomInt(0, 360)
        }
        listeNom = [{ t: '#npM', nom: lettre1 }, { t: '#npN', nom: lettre2 }, { t: '#npT', nom: lettre3 }, {
          t: '#npV',
          nom: lettre4
        }, { t: '#npO', nom: lettre5 }, { t: '#npS', nom: lettre6 }, { t: '#npX', nom: lettre8 }]
        break
      case 'CONE_REVOLUTION':
        stor.listCache = ['#co1', '#co2', '#co3', '#co4', '#co5', '#co6', '#co7', '#co8']
        stor.height = 400
        stor.listeChange.push({ id: '#hauteur', st: 1, ep: 3, co: [0, 0, 0] })
        stor.listZig = ['#cpS', '#cpO', '#cpR', '#cpT', '#cpF', '#cr1', '#cr2', '#cr3', '#cd1', '#cg1', '#cg2', '#cb1', '#ch1']

        A1 = j3pGetRandomInt(0, 360)
        A2 = A3 = j3pGetRandomInt(65, 80)
        A4 = j3pGetRandomInt(7, 9)
        if (j3pGetRandomBool()) {
          stor.listeChange.push({ id: '#ray1', st: 1, ep: 3, co: [0, 0, 0] })
          stor.listeChange.push({ id: '#ray2', st: 1, ep: 3, co: [0, 0, 0] })
          stor.listeChange.push({ id: '#ray3', st: 1, ep: 3, co: [0, 0, 0] })
          stor.listeChange.push({ id: '#diam', st: 1, ep: 3, co: [0, 0, 0] })
          stor.listeChange.push({ id: '#arcc1', st: 1, ep: 4, co: [0, 0, 0] })
          stor.listeChange.push({ id: '#arcc2', st: 1, ep: 4, co: [0, 0, 0] })
          pointhaut = 1
        } else {
          stor.listeChange.push({ id: '#arcc1', st: 0, ep: 4, co: [0, 0, 0] })
          stor.listeChange.push({ id: '#arcc2', st: 0, ep: 4, co: [0, 0, 0] })
          pointhaut = 0
        }

        // la laterale
        this.listedeselemedessine.push({
          type: 'face',
          nom: 'pol1',
          val: ['face', 'face_laterale'],
          tag: '#pol1',
          tagAllume: ['#surf1', '#surf2'],
          tagspe: '#po5',
          couleur: [255, 255, 255],
          coolspe: specool,
          ep: 1
        })
        // la base
        this.listedeselemedessine.push({
          type: 'face',
          nom: 'pol1',
          val: ['face', 'base'],
          tag: '#pol2',
          tagAllume: ['#surf3'],
          tagspe: '#po6',
          couleur: [255, 255, 255],
          coolspe: specool,
          ep: 1
        })
        // l ellipse
        this.listedeselemedessine.push({
          type: 'ellipse3',
          nom: 'M1',
          val: ['demi-cercle', 'meridien', 'grand_cercle'],
          tag: '#cb1',
          tagAllume: [
            { tagA: '#arcc1', ep: 3, st: pointhaut, couleur: [0, 0, 0] },
            { tagA: '#arcc2', ep: 3, st: pointhaut, couleur: [0, 0, 0] },
            { tagA: '#arc1', ep: 4, st: 0, couleur: [0, 0, 0] },
            { tagA: '#arc3', ep: 4, st: 0, couleur: [0, 0, 0] },
            { tagA: '#arc4', ep: 4, st: 0, couleur: [0, 0, 0] },
            { tagA: '#arc2', ep: 4, st: 0, couleur: [0, 0, 0] }
          ],
          sm: true
        })

        lettre2 = addMaj(nomspris)
        lettre3 = addMaj(nomspris)
        lettre4 = addMaj(nomspris)
        lettre5 = addMaj(nomspris)
        lettre8 = addMaj(nomspris)
        listeNom = [{ t: '#npO', nom: lettre2 }, { t: '#npS', nom: lettre3 }, { t: '#npF', nom: lettre4 }, {
          t: '#npT',
          nom: lettre5
        }, { t: '#npR', nom: lettre8 }]
        this.listedeselemedessine.push({
          type: 'point',
          nom: lettre2,
          val: ['point', 'centre'],
          tag: '#cpO',
          tagAllume: ['#sO'],
          couleur: [255, 255, 255],
          ep: false
        })
        this.listedeselemedessine.push({
          type: 'point',
          nom: lettre3,
          val: ['point', 'sommet', 'sommet_principal'],
          tag: '#cpS',
          tagAllume: ['#sA'],
          couleur: [255, 255, 255],
          ep: false
        })
        this.listedeselemedessine.push({
          type: 'point',
          nom: lettre4,
          val: ['point'],
          tag: '#cpR',
          tagAllume: ['#sY'],
          couleur: [255, 255, 255],
          ep: false
        })
        this.listedeselemedessine.push({
          type: 'point',
          nom: lettre5,
          val: ['point'],
          tag: '#cpT',
          tagAllume: ['#sT'],
          couleur: [255, 255, 255],
          ep: false
        })
        this.listedeselemedessine.push({
          type: 'point',
          nom: lettre8,
          val: ['point'],
          tag: '#cpF',
          tagAllume: ['#sK'],
          couleur: [255, 255, 255],
          ep: false
        })
        // hauteur
        this.listedeselemedessine.push({
          type: 'segment',
          nom: lettre2 + lettre3,
          val: ['segment', 'hauteur'],
          tag: '#ch1',
          tagAllume: ['#hauteur'],
          couleur: [0, 0, 0],
          ep: 3,
          st: 1
        })
        oks1 = oks2 = oks3 = true
        if (stor.element === 'longueur_egale') {
          haz = j3pGetRandomInt(0, 2)
          if (haz === 0) {
            oks1 = false
          }
          if (haz === 1) {
            oks2 = false
          }
          if (haz === 2) {
            oks3 = false
          }
        }
        if (stor.element === 'diametre') {
          this.listedeselemedessine.push({
            type: 'segment',
            nom: lettre2 + lettre8,
            val: ['segment', 'diametre'],
            tag: '#cd1',
            tagAllume: ['#diam'],
            couleur: [0, 0, 0],
            ep: 3,
            st: pointhaut
          })
          stor.listCache.push('#ray1', '#cr3', '#cr2', '#ray3')
        } else {
          if (oks1) {
            this.listedeselemedessine.push({
              type: 'segment',
              nom: lettre2 + lettre8,
              val: ['segment', 'rayon', 'longueur_egale'],
              tag: '#cr2',
              tagAllume: ['#ray1'],
              couleur: [0, 0, 0],
              ep: 3,
              st: pointhaut
            })
          } else {
            stor.listeChange.push({ id: '#ray1', co: coMod, ep: 3, st: pointhaut })
          }
          if (oks2) {
            this.listedeselemedessine.push({
              type: 'segment',
              nom: lettre2 + lettre8,
              val: ['segment', 'rayon', 'longueur_egale'],
              tag: '#cr3',
              tagAllume: ['#ray3'],
              couleur: [0, 0, 0],
              ep: 3,
              st: pointhaut
            })
          } else {
            stor.listeChange.push({ id: '#ray3', co: coMod, ep: 3, st: pointhaut })
          }
          stor.listCache.push('#diam', '#cd1')
        }
        if (oks3) {
          this.listedeselemedessine.push({
            type: 'segment',
            nom: lettre2 + lettre5,
            val: ['segment', 'rayon', 'longueur_egale'],
            tag: '#cr1',
            tagAllume: ['#ray2'],
            couleur: [0, 0, 0],
            ep: 3,
            st: pointhaut
          })
        } else {
          stor.listeChange.push({ id: '#ray2', co: coMod, ep: 3, st: pointhaut })
        }
        this.listedeselemedessine.push({
          type: 'segment',
          nom: lettre5 + lettre3,
          val: ['segment', 'generatrice'],
          tag: '#cg1',
          tagAllume: ['#gen1'],
          couleur: [0, 0, 0],
          ep: 4
        })
        this.listedeselemedessine.push({
          type: 'segment',
          nom: lettre4 + lettre3,
          val: ['segment', 'generatrice'],
          tag: '#cg2',
          tagAllume: ['#gen2'],
          couleur: [0, 0, 0],
          ep: 4
        })
        break
      case 'CONE':
        stor.height = 400
        A1 = j3pGetRandomInt(0, 360)
        A4 = j3pGetRandomInt(7, 9)
        lalettre8 = 0
        if (j3pGetRandomBool()) {
          A2 = j3pGetRandomInt(92, 96)
          A3 = j3pGetRandomInt(50, 65)
        } else {
          do {
            A2 = j3pGetRandomInt(60, 85)
            A3 = j3pGetRandomInt(60, 85)
          }
          while (Math.abs(A2 - A3) < 15)
          lalettre8 = 1
          stor.listeChange.push({ id: '#hauteur', st: 1, ep: 3, co: [0, 0, 0] })
        }
        if (j3pGetRandomBool()) {
          stor.listeChange.push({ id: '#ray1', st: 1, ep: 3, co: [0, 0, 0] })
          stor.listeChange.push({ id: '#ray2', st: 1, ep: 3, co: [0, 0, 0] })
          stor.listeChange.push({ id: '#ray3', st: 1, ep: 3, co: [0, 0, 0] })
          stor.listeChange.push({ id: '#diam', st: 1, ep: 3, co: [0, 0, 0] })
          stor.listeChange.push({ id: '#arcc1', st: 1, ep: 4, co: [0, 0, 0] })
          stor.listeChange.push({ id: '#arcc2', st: 1, ep: 4, co: [0, 0, 0] })
          pointhaut = 1
        } else {
          stor.listeChange.push({ id: '#arcc1', st: 0, ep: 4, co: [0, 0, 0] })
          stor.listeChange.push({ id: '#arcc2', st: 0, ep: 4, co: [0, 0, 0] })
          pointhaut = 0
        }

        stor.listZig = ['#cp6', '#cp1', '#cp2', '#cp3', '#cp4', '#cp5', '#cr1', '#cr2', '#cr3', '#cd1', '#cg1', '#cg2', '#cEl', '#ch1']

        // la laterale
        this.listedeselemedessine.push({
          type: 'face',
          nom: 'pol1',
          val: ['face', 'face_laterale'],
          tag: '#pol1',
          tagAllume: ['#surf1', '#surf2'],
          tagspe: '#po5',
          couleur: [255, 255, 255],
          coolspe: specool,
          ep: 1
        })
        // la base
        this.listedeselemedessine.push({
          type: 'face',
          nom: 'pol1',
          val: ['face', 'base'],
          tag: '#pol2',
          tagAllume: ['#surf3'],
          tagspe: '#po6',
          couleur: [255, 255, 255],
          coolspe: specool,
          ep: 1
        })
        // l ellipse
        this.listedeselemedessine.push({
          type: 'ellipse3',
          nom: 'M1',
          val: ['demi-cercle', 'meridien', 'grand_cercle'],
          tag: '#cEl',
          tagAllume: [
            { tagA: '#arcc1', ep: 4, st: pointhaut, couleur: [0, 0, 0] },
            { tagA: '#arcc2', ep: 4, st: pointhaut, couleur: [0, 0, 0] },
            { tagA: '#arc1', ep: 4, st: 0, couleur: [0, 0, 0] },
            { tagA: '#arc3', ep: 4, st: 0, couleur: [0, 0, 0] },
            { tagA: '#arc4', ep: 4, st: 0, couleur: [0, 0, 0] },
            { tagA: '#arc2', ep: 4, st: 0, couleur: [0, 0, 0] }
          ],
          sm: true
        })

        lettre2 = addMaj(nomspris)
        lettre3 = addMaj(nomspris)
        lettre4 = addMaj(nomspris)
        lettre5 = addMaj(nomspris)
        lettre8 = addMaj(nomspris)
        lettre6 = addMaj(nomspris)
        listeNom = [{ t: '#npH', nom: lettre6 }, { t: '#npO', nom: lettre2 }, { t: '#npA', nom: lettre3 }, {
          t: '#npY',
          nom: lettre4
        }, { t: '#npT', nom: lettre5 }, { t: '#npK', nom: lettre8 }]
        this.listedeselemedessine.push({
          type: 'point',
          nom: lettre2,
          val: ['point', 'centre'],
          tag: '#cp6',
          tagAllume: ['#sO'],
          couleur: [255, 255, 255],
          ep: false
        })
        this.listedeselemedessine.push({
          type: 'point',
          nom: lettre3,
          val: ['point', 'sommet', 'sommet_principal'],
          tag: '#cp5',
          tagAllume: ['#sA'],
          couleur: [255, 255, 255],
          ep: false
        })
        this.listedeselemedessine.push({
          type: 'point',
          nom: lettre4,
          val: ['point'],
          tag: '#cp4',
          tagAllume: ['#sY'],
          couleur: [255, 255, 255],
          ep: false
        })
        this.listedeselemedessine.push({
          type: 'point',
          nom: lettre5,
          val: ['point'],
          tag: '#cp3',
          tagAllume: ['#sT'],
          couleur: [255, 255, 255],
          ep: false
        })
        this.listedeselemedessine.push({
          type: 'point',
          nom: lettre8,
          val: ['point'],
          tag: '#cp2',
          tagAllume: ['#sK'],
          couleur: [255, 255, 255],
          ep: false
        })
        this.listedeselemedessine.push({
          type: 'point',
          nom: lettre6,
          val: ['point'],
          tag: '#cp1',
          tagAllume: ['#sH'],
          couleur: [255, 255, 255],
          ep: false
        })
        // hauteur
        this.listedeselemedessine.push({
          type: 'segment',
          nom: lettre2 + lettre3,
          val: ['segment', 'hauteur'],
          tag: '#ch1',
          tagAllume: ['#hauteur'],
          couleur: [0, 0, 0],
          ep: 3,
          st: lalettre8
        })
        oks1 = oks2 = oks3 = true
        if (stor.element === 'longueur_egale') {
          haz = j3pGetRandomInt(0, 2)
          if (haz === 0) {
            oks1 = false
          }
          if (haz === 1) {
            oks2 = false
          }
          if (haz === 2) {
            oks3 = false
          }
        }
        if (stor.element === 'diametre') {
          this.listedeselemedessine.push({
            type: 'segment',
            nom: lettre2 + lettre8,
            val: ['segment', 'diametre'],
            tag: '#cd1',
            tagAllume: ['#diam'],
            couleur: [0, 0, 0],
            ep: 3,
            st: pointhaut
          })
          stor.listCache.push('#ray1', '#ray3', '#cr1', '#cr2')
        } else {
          if (oks1) {
            this.listedeselemedessine.push({
              type: 'segment',
              nom: lettre2 + lettre8,
              val: ['segment', 'rayon', 'longueur_egale'],
              tag: '#cr2',
              tagAllume: ['#ray1'],
              couleur: [0, 0, 0],
              ep: 3,
              st: pointhaut
            })
          } else {
            stor.listeChange.push({ id: '#ray1', co: coMod, ep: 3, st: pointhaut })
          }
          if (oks2) {
            this.listedeselemedessine.push({
              type: 'segment',
              nom: lettre2 + lettre8,
              val: ['segment', 'rayon', 'longueur_egale'],
              tag: '#cr1',
              tagAllume: ['#ray3'],
              couleur: [0, 0, 0],
              ep: 3,
              st: pointhaut
            })
          } else {
            stor.listeChange.push({ id: '#ray3', co: coMod, ep: 3, st: pointhaut })
          }
          stor.listCache.push('#diam', '#cd1')
        }
        if (oks3) {
          this.listedeselemedessine.push({
            type: 'segment',
            nom: lettre2 + lettre5,
            val: ['segment', 'rayon', 'longueur_egale'],
            tag: '#cr3',
            tagAllume: ['#ray2'],
            couleur: [0, 0, 0],
            ep: 3,
            st: pointhaut
          })
        } else {
          stor.listeChange.push({ id: '#ray2', co: coMod, ep: 3, st: pointhaut })
        }
        this.listedeselemedessine.push({
          type: 'segment',
          nom: lettre5 + lettre3,
          val: ['segment', 'generatrice'],
          tag: '#cg2',
          tagAllume: ['#gen1'],
          couleur: [0, 0, 0],
          ep: 4
        })
        this.listedeselemedessine.push({
          type: 'segment',
          nom: lettre4 + lettre3,
          val: ['segment', 'generatrice'],
          tag: '#cg1',
          tagAllume: ['#gen2'],
          couleur: [0, 0, 0],
          ep: 4
        })
        break
      case 'PYRAMIDE':
        stor.height = 400
        A1 = j3pGetRandomInt(0, 360)
        A3 = j3pGetRandomInt(0, 180)
        A2 = j3pGetRandomInt(50, 179)
        A4 = j3pGetRandomInt(5, 11)

        stor.listZig = ['#cc1', '#cc2', '#cc3', '#cc4', '#cc5', '#cs1', '#cs2', '#cs3', '#cs4', '#cs5', '#cs6', '#cs7', '#cs8', '#cs9']

        if (j3pGetRandomBool()) {
          rel1 = 'face_cachee'
          rel2 = 'face_visible'
          stor.listeChange.push({ id: '#b1', ep: 4, st: 1, co: [0, 0, 0] })
          rel3 = 'arete_visible'
          rel4 = 'arete_cachee'
          ep1 = 1
          ep2 = 0
        } else {
          rel2 = 'face_cachee'
          rel1 = 'face_visible'
          stor.listeChange.push({ id: '#s2', ep: 4, st: 1, co: [0, 0, 0] })
          stor.listeChange.push({ id: '#s3', ep: 4, st: 1, co: [0, 0, 0] })
          rel4 = 'arete_visible'
          rel3 = 'arete_cachee'
          ep2 = 1
          ep1 = 0
        }
        // les laterales
        this.listedeselemedessine.push({
          type: 'face',
          nom: 'pol1',
          val: ['face', 'face_laterale', rel1],
          tag: '#po1',
          tagAllume: ['#surf1'],
          tagspe: '#so1',
          couleur: [255, 255, 255],
          coolspe: specool,
          ep: 1
        })
        this.listedeselemedessine.push({
          type: 'face',
          nom: 'pol1',
          val: ['face', 'base', rel1],
          tag: '#pp4',
          tagAllume: ['#surf2'],
          tagspe: '#ss4',
          couleur: [255, 255, 255],
          coolspe: specool,
          ep: 1
        })
        this.listedeselemedessine.push({
          type: 'face',
          nom: 'pol1',
          val: ['face', 'face_laterale', rel2],
          tag: '#po3',
          tagAllume: ['#surf3'],
          tagspe: '#so3',
          couleur: [255, 255, 255],
          coolspe: specool,
          ep: 1
        })
        this.listedeselemedessine.push({
          type: 'face',
          nom: 'pol1',
          val: ['face', 'face_laterale', rel2],
          tag: '#po4',
          tagAllume: ['#surf4'],
          tagspe: '#so4',
          couleur: [255, 255, 255],
          coolspe: specool,
          ep: 1
        })
        this.listedeselemedessine.push({
          type: 'face',
          nom: 'pol1',
          val: ['face', 'face_laterale', rel2],
          tag: '#po5',
          tagAllume: ['#surf5'],
          tagspe: '#so5',
          couleur: [255, 255, 255],
          coolspe: specool,
          ep: 1
        })

        // les sommets
        lettre1 = addMaj(nomspris)
        lettre2 = addMaj(nomspris)
        lettre3 = addMaj(nomspris)
        lettre4 = addMaj(nomspris)
        lettre5 = addMaj(nomspris)
        listeNom = [{ t: '#npS', nom: lettre1 }, { t: '#npA', nom: lettre2 }, { t: '#npB', nom: lettre3 }, {
          t: '#npC',
          nom: lettre4
        }, { t: '#npD', nom: lettre5 }]
        this.listedeselemedessine.push({
          type: 'point',
          nom: lettre1,
          val: ['point', 'sommet', 'sommet_principal'],
          tag: '#cc1',
          tagAllume: ['#sS'],
          couleur: [255, 255, 255],
          ep: false
        })
        this.listedeselemedessine.push({
          type: 'point',
          nom: lettre2,
          val: ['point', 'sommet'],
          tag: '#cc5',
          tagAllume: ['#sA'],
          couleur: [255, 255, 255],
          ep: false
        })
        this.listedeselemedessine.push({
          type: 'point',
          nom: lettre3,
          val: ['point', 'sommet'],
          tag: '#cc4',
          tagAllume: ['#sB'],
          couleur: [255, 255, 255],
          ep: false
        })
        this.listedeselemedessine.push({
          type: 'point',
          nom: lettre4,
          val: ['point', 'sommet'],
          tag: '#cc3',
          tagAllume: ['#sC'],
          couleur: [255, 255, 255],
          ep: false
        })
        this.listedeselemedessine.push({
          type: 'point',
          nom: lettre5,
          val: ['point', 'sommet'],
          tag: '#cc2',
          tagAllume: ['#sD'],
          couleur: [255, 255, 255],
          ep: false
        })

        // les aretes
        this.listedeselemedessine.push({
          type: 'arete',
          nom: lalettre,
          val: ['arete', 'arete_laterale', 'arete_visible'],
          tag: '#cs9',
          tagAllume: ['#s1'],
          couleur: [0, 0, 0],
          ep: 4
        })
        this.listedeselemedessine.push({
          type: 'arete',
          nom: lalettre,
          val: ['arete', 'arete_laterale', rel3],
          tag: '#cs6',
          tagAllume: ['#s2'],
          couleur: [0, 0, 0],
          ep: 4,
          st: ep2
        })
        this.listedeselemedessine.push({
          type: 'arete',
          nom: lalettre,
          val: ['arete', 'arete_laterale', rel3],
          tag: '#cs4',
          tagAllume: ['#s3'],
          couleur: [0, 0, 0],
          ep: 4,
          st: ep2
        })
        this.listedeselemedessine.push({
          type: 'arete',
          nom: lalettre,
          val: ['arete', 'arete_laterale', 'arete_visible'],
          tag: '#cs3',
          tagAllume: ['#s4'],
          couleur: [0, 0, 0],
          ep: 4
        })

        this.listedeselemedessine.push({
          type: 'arete',
          nom: lalettre,
          val: ['arete', rel4],
          tag: '#cs7',
          tagAllume: ['#b1'],
          couleur: [0, 0, 0],
          ep: 4,
          st: ep1
        })
        this.listedeselemedessine.push({
          type: 'arete',
          nom: lalettre,
          val: ['arete', 'arete_visible'],
          tag: '#cs8',
          tagAllume: ['#b2'],
          couleur: [0, 0, 0],
          ep: 4
        })
        this.listedeselemedessine.push({
          type: 'arete',
          nom: lalettre,
          val: ['arete', 'arete_visible'],
          tag: '#cs1',
          tagAllume: ['#b3'],
          couleur: [0, 0, 0],
          ep: 4
        })
        this.listedeselemedessine.push({
          type: 'arete',
          nom: lalettre,
          val: ['arete', 'arete_visible'],
          tag: '#cs2',
          tagAllume: ['#b4'],
          couleur: [0, 0, 0],
          ep: 4
        })
        this.listedeselemedessine.push({
          type: 'arete',
          nom: lalettre,
          val: ['hauteur'],
          tag: '#cs5',
          tagAllume: ['#hauteur'],
          couleur: [0, 0, 0],
          ep: 3,
          st: 0
        })

        break
      case 'CYLINDRE_REVOLUTION':
        stor.height = 450
        stor.lwidth = 450
        A1 = j3pGetRandomInt(0, 360)
        A2 = j3pGetRandomInt(6, 11)
        stor.listZig = ['#cc1', '#cc2', '#cc3', '#cc4', '#cc5', '#cc6', '#cr1', '#cr2', '#cr3', '#cr4', '#cd1', '#cEl1', '#cEl2', '#cg1', '#cg2', '#ch1']

        ok1 = ok2 = true
        if (stor.element === 'face_parallele') {
          ok1 = (j3pGetRandomInt(0, 1) === 0)
          ok2 = !ok1
        }

        // face dessous
        if (ok1) {
          this.listedeselemedessine.push({
            type: 'face',
            nom: 'pol1',
            val: ['face', 'base', 'face_parallele'],
            tag: '#pob1',
            tagAllume: ['#base2'],
            tagspe: '#sob1',
            couleur: [255, 255, 255],
            coolspe: specool,
            ep: 1
          })
        } else {
          stor.listCache = ['#pob1', '#pob2', '#pob3', '#pob4', '#pob5', '#sob1', '#sob2', '#sob3', '#sob4', '#sob5']
          stor.listeChange.push({ id: '#base2', co: coMod })
        }
        if (ok2) {
          this.listedeselemedessine.push({
            type: 'face',
            nom: 'polh',
            val: ['face', 'base', 'face_parallele'],
            tag: '#poh1',
            tagAllume: ['#base1'],
            tagspe: '#soh1',
            couleur: [255, 255, 255],
            coolspe: specool,
            ep: 1
          })
        } else {
          stor.listCache = ['#poh1', '#poh2', '#poh3', '#poh4', '#poh5', '#soh1', '#soh2', '#soh3', '#soh4', '#soh5']
          stor.listeChange.push({ id: '#base1', co: coMod })
        }
        this.listedeselemedessine.push({
          type: 'face2',
          nom: ['face1', 'face2'],
          val: ['face_laterale', 'face'],
          tag: '#pol',
          tagAllume: ['#lat1', '#lat2'],
          tagspe: '#sol',
          couleur: [255, 255, 255],
          coolspe: specool,
          ep: 1
        })

        lettre1 = addMaj(nomspris)
        lettre2 = addMaj(nomspris)
        lettre3 = addMaj(nomspris)
        lettre4 = addMaj(nomspris)
        lettre5 = addMaj(nomspris)
        lettre6 = addMaj(nomspris)
        this.listedeselemedessine.push({
          type: 'point',
          nom: lettre1,
          val: ['point'],
          tag: '#cc1',
          tagAllume: ['#s1'],
          couleur: [255, 255, 255],
          ep: false
        })
        this.listedeselemedessine.push({
          type: 'point',
          nom: lettre2,
          val: ['point'],
          tag: '#cc2',
          tagAllume: ['#s2'],
          couleur: [255, 255, 255],
          ep: false
        })
        this.listedeselemedessine.push({
          type: 'point',
          nom: lettre3,
          val: ['point'],
          tag: '#cc4',
          tagAllume: ['#s3'],
          couleur: [255, 255, 255],
          ep: false
        })
        this.listedeselemedessine.push({
          type: 'point',
          nom: lettre4,
          val: ['point'],
          tag: '#cc3',
          tagAllume: ['#s4'],
          couleur: [255, 255, 255],
          ep: false
        })
        this.listedeselemedessine.push({
          type: 'point',
          nom: lettre5,
          val: ['point'],
          tag: '#cc5',
          tagAllume: ['#s5'],
          couleur: [255, 255, 255],
          ep: false
        })
        this.listedeselemedessine.push({
          type: 'point',
          nom: lettre6,
          val: ['point'],
          tag: '#cc6',
          tagAllume: ['#s6'],
          couleur: [255, 255, 255],
          ep: false
        })
        listeNom = [{ t: '#np1', nom: lettre1 }, { t: '#np2', nom: lettre2 }, { t: '#np3', nom: lettre3 }, {
          t: '#np4',
          nom: lettre4
        }, { t: '#np5', nom: lettre5 }, { t: '#np6', nom: lettre6 }]

        rel1 = rel2 = 'piege'
        ok1 = ok2 = ok3 = ok4 = ok5 = ok6 = ok7 = true

        if (stor.element === 'longueur_egale') {
          haz = j3pGetRandomInt(0, 6)
          if (haz === 0) {
            ok1 = false
            rel1 = 'longueur_egale'
          }
          if (haz === 1) {
            ok2 = false
            rel1 = 'longueur_egale'
          }
          if (haz === 2) {
            ok3 = false
            rel1 = 'longueur_egale'
          }
          if (haz === 3) {
            ok4 = false
            rel1 = 'longueur_egale'
          }
          if (haz === 4) {
            ok5 = false
            rel2 = 'longueur_egale'
          }
          if (haz === 5) {
            ok6 = false
            rel2 = 'longueur_egale'
          }
          if (haz === 6) {
            ok7 = false
            rel2 = 'longueur_egale'
          }
        }

        if (stor.element === 'diametre') {
          stor.listCache.push('#ray1', '#ray2', '#cr2', '#cr3')
          this.listedeselemedessine.push({
            type: 'segment',
            nom: lettre5 + 'm4',
            val: ['segment', 'diametre'],
            tag: '#cd1',
            tagAllume: ['#diam2'],
            couleur: [0, 0, 0],
            ep: 3,
            st: 2
          })
        } else {
          stor.listCache.push('#diam2', '#cd1')
          if (ok1) {
            this.listedeselemedessine.push({
              type: 'segment',
              nom: lettre5 + 'm4',
              val: ['segment', 'rayon', rel1],
              tag: '#cr3',
              tagAllume: ['#ray1'],
              couleur: [0, 0, 0],
              ep: 3,
              st: 2
            })
          } else { stor.listeChange.push({ id: '#ray1', ep: 4, co: coMod }) }
          if (ok2) {
            this.listedeselemedessine.push({
              type: 'segment',
              nom: lettre5 + 'm4',
              val: ['segment', 'rayon', rel1],
              tag: '#cr2',
              tagAllume: ['#ray2'],
              couleur: [0, 0, 0],
              ep: 3,
              st: 2
            })
          } else { stor.listeChange.push({ id: '#ray2', ep: 4, co: coMod }) }
        }
        if (ok3) {
          this.listedeselemedessine.push({
            type: 'segment',
            nom: lettre5 + 'm4',
            val: ['segment', 'rayon', rel1],
            tag: '#cr1',
            tagAllume: ['#ray3'],
            couleur: [0, 0, 0],
            ep: 3,
            st: 2
          })
        } else { stor.listeChange.push({ id: '#ray3', ep: 4, co: coMod }) }
        if (ok4) {
          this.listedeselemedessine.push({
            type: 'segment',
            nom: lettre5 + 'm4',
            val: ['segment', 'rayon', rel1],
            tag: '#cr4',
            tagAllume: ['#ray4'],
            couleur: [0, 0, 0],
            ep: 3
          })
        } else { stor.listeChange.push({ id: '#ray4', ep: 4, co: coMod, st: 2 }) }

        if (ok5) {
          this.listedeselemedessine.push({
            type: 'segment',
            nom: lettre5 + 'm4',
            val: ['segment', rel2, 'hauteur'],
            tag: '#ch1',
            tagAllume: ['#sl4'],
            couleur: [0, 0, 0],
            ep: 3,
            st: 2
          })
        } else { stor.listeChange.push({ id: '#sl4', ep: 4, co: coMod, st: 2 }) }
        if (ok6) {
          this.listedeselemedessine.push({
            type: 'segment',
            nom: lettre5 + 'm4',
            val: ['segment', rel2, 'hauteur'],
            tag: '#cg2',
            tagAllume: ['#sl1'],
            couleur: [0, 0, 0],
            ep: 3
          })
        } else { stor.listeChange.push({ id: '#sl1', ep: 4, co: coMod }) }
        if (ok7) {
          this.listedeselemedessine.push({
            type: 'segment',
            nom: lettre5 + 'm4',
            val: ['segment', rel2, 'hauteur'],
            tag: '#cg1',
            tagAllume: ['#sl2'],
            couleur: [0, 0, 0],
            ep: 3
          })
        } else { stor.listeChange.push({ id: '#sl2', ep: 4, co: coMod }) }

        break
      case 'SEGMENT':
        lalettre1 = addMaj(nomspris)
        this.listedeselemedessine.push({
          type: 'point',
          nom: lalettre1,
          val: ['point', 'extremites'],
          tag: '#cp1',
          tagAllume: ['#p1', '#s1'],
          couleur: [255, 255, 255],
          ep: false
        })
        stor.listZig = ['#cs1', '#cd1', '#cd2', '#cd3', '#cd4', '#cp1', '#cp2', '#cp3']

        lalettre2 = addMaj(nomspris)
        this.listedeselemedessine.push({
          type: 'point',
          nom: lalettre2,
          val: ['point', 'extremites'],
          tag: '#cp3',
          tagAllume: ['#p3', '#s3'],
          couleur: [255, 255, 255],
          ep: false
        })

        // milieu
        lalettre = addMaj(nomspris)
        this.listedeselemedessine.push({
          type: 'point',
          nom: lalettre,
          val: ['point', 'milieu', 'centre_symetrie'],
          tag: '#cp2',
          tagAllume: ['#p2', '#s2'],
          couleur: [255, 255, 255],
          ep: false
        })

        // mediatrice
        this.listedeselemedessine.push({
          type: 'droite',
          nom: lalettre + 'ù',
          val: ['droite', 'axe', 'mediatrice'],
          tag: '#cd3',
          tagAllume: ['#dmed'],
          couleur: [130, 130, 130],
          ep: 2
        })

        // dte qui porte le seg
        this.listedeselemedessine.push({
          type: 'droite',
          nom: lalettre1 + lalettre,
          val: ['droite', 'axe'],
          serie: 1,
          tag: '#cd1',
          tagAllume: ['#d1'],
          couleur: [130, 130, 130],
          ep: 2,
          contser: true
        })

        // une perp pour piege
        this.listedeselemedessine.push({
          type: 'droite',
          nom: lalettre1 + 'ç',
          val: ['droite'],
          tag: '#cd4',
          tagAllume: ['#d2'],
          couleur: [130, 130, 130],
          ep: 2
        })

        // une a 45° par milieu pour piege
        this.listedeselemedessine.push({
          type: 'droite',
          nom: lalettre + 'é',
          val: ['droite'],
          tag: '#cd2',
          tagAllume: ['#d3'],
          couleur: [130, 130, 130],
          ep: 2
        })

        // le seg quand mm
        this.listedeselemedessine.push({
          type: 'segment',
          nom: lalettre1 + lalettre2,
          val: ['segment', 'axe'],
          serie: 1,
          tag: '#cs1',
          tagAllume: ['#seg'],
          couleur: [0, 0, 0],
          ep: 2
        })
        stor.LeNomDuTruc = '[' + lalettre + lalettre1 + ']'

        A1 = j3pGetRandomInt(0, 359)
        listeNom = [{ t: '#np1', nom: lalettre1 }, { t: '#np2', nom: lalettre2 }, { t: '#np3', nom: lalettre }]
        break
      case 'DEMI_DROITE':
        stor.height = 350
        lalettre1 = addMaj(nomspris)
        this.listedeselemedessine.push({
          type: 'point',
          nom: lalettre1,
          val: ['point', 'origine'],
          tag: '#cp1',
          tagAllume: ['#p1', '#s1'],
          couleur: [255, 255, 255],
          ep: false
        })
        stor.listZig = ['#cs1', '#cd1', '#cd2', '#cd3', '#cd4', '#cp1', '#cp2', '#cp3']

        lalettre2 = addMaj(nomspris)
        this.listedeselemedessine.push({
          type: 'point',
          nom: lalettre2,
          val: ['point'],
          tag: '#cp3',
          tagAllume: ['#p3', '#s3'],
          couleur: [255, 255, 255],
          ep: false
        })

        lalettre = addMaj(nomspris)
        this.listedeselemedessine.push({
          type: 'point',
          nom: lalettre,
          val: ['point'],
          tag: '#cp2',
          tagAllume: ['#p2', '#s2'],
          couleur: [255, 255, 255],
          ep: false
        })

        this.listedeselemedessine.push({
          type: 'droite',
          nom: lalettre + 'ù',
          val: ['droite'],
          tag: '#cd2',
          tagAllume: ['#d2'],
          couleur: [130, 130, 130],
          ep: 2
        })

        this.listedeselemedessine.push({
          type: 'droite',
          nom: lalettre1 + lalettre,
          val: ['droite', 'axe'],
          serie: 1,
          tag: '#cd4',
          tagAllume: ['#d1'],
          couleur: [130, 130, 130],
          ep: 2,
          contser: true
        })

        this.listedeselemedessine.push({
          type: 'droite',
          nom: lalettre1 + 'ç',
          val: ['droite'],
          tag: '#cd1',
          tagAllume: ['#dmed'],
          couleur: [130, 130, 130],
          ep: 2
        })

        this.listedeselemedessine.push({
          type: 'droite',
          nom: lalettre + 'é',
          val: ['droite'],
          tag: '#cd3',
          tagAllume: ['#d3'],
          couleur: [130, 130, 130],
          ep: 2
        })

        this.listedeselemedessine.push({
          type: 'demidroite',
          nom: lalettre1 + lalettre2,
          val: ['droite', 'axe'],
          serie: 1,
          tag: '#cs1',
          tagAllume: ['#demid1'],
          couleur: [0, 0, 0],
          ep: 3
        })

        stor.LeNomDuTruc = '[' + lalettre1 + lalettre2 + ')'
        A1 = j3pGetRandomInt(0, 359)
        listeNom = [{ t: '#np1', nom: lalettre1 }, { t: '#np2', nom: lalettre2 }, { t: '#np3', nom: lalettre }]

        break
      case 'DROITE': { // deux points
        lalettre1 = addMaj(nomspris)
        this.listedeselemedessine.push({
          type: 'point',
          nom: lalettre1,
          val: ['point', 'centre_symetrie'],
          tag: '#cp1',
          tagAllume: ['#p1', '#s1'],
          couleur: [255, 255, 255],
          ep: false
        })
        stor.listZig = ['#cd0', '#cd1', '#cd2', '#cd3', '#cd4', '#cp1', '#cp2', '#cp3']

        lalettre2 = addMaj(nomspris)
        this.listedeselemedessine.push({
          type: 'point',
          nom: lalettre2,
          val: ['point', 'centre_symetrie'],
          tag: '#cp2',
          tagAllume: ['#p2', '#s2'],
          couleur: [255, 255, 255],
          ep: false
        })

        // un troisieme
        lalettre = addMaj(nomspris)
        this.listedeselemedessine.push({
          type: 'point',
          nom: lalettre,
          val: ['point', 'centre_symetrie'],
          tag: '#cp3',
          tagAllume: ['#p3', '#s3'],
          couleur: [255, 255, 255],
          ep: false
        })

        // un axe
        this.listedeselemedessine.push({
          type: 'droite',
          nom: lalettre + 'ù',
          val: ['droite', 'axe'],
          tag: '#cd3',
          tagAllume: ['#d3'],
          couleur: [130, 130, 130],
          ep: 2
        })
        // axe
        this.listedeselemedessine.push({
          type: 'droite',
          nom: 'qz',
          val: ['droite', 'axe'],
          tag: '#cd2',
          tagAllume: ['#d2'],
          couleur: [130, 130, 130],
          ep: 2
        })
        // avec son angle droit
        // un dernier axe dehors
        this.listedeselemedessine.push({
          type: 'droite',
          nom: 'extz2',
          val: ['droite', 'axe'],
          tag: '#cd4',
          tagAllume: ['#d4'],
          couleur: [130, 130, 130],
          ep: 2
        })

        // une a 45° par milieu pour piege
        this.listedeselemedessine.push({
          type: 'droite',
          nom: lalettre + 'é',
          val: ['droite'],
          tag: '#cd1',
          tagAllume: ['#d5'],
          couleur: [130, 130, 130],
          ep: 2
        })

        // le droite quand mm
        this.listedeselemedessine.push({
          type: 'droites',
          nom: lalettre1 + lalettre2,
          val: ['axe', 'droite'],
          tag: '#cd0',
          tagAllume: ['#d1'],
          couleur: [0, 0, 0],
          ep: 3
        })
        stor.LeNomDuTruc = '(' + lalettre1 + lalettre2 + ')'

        A1 = j3pGetRandomInt(0, 359)
        listeNom = [{ t: '#np1', nom: lalettre1 }, { t: '#np2', nom: lalettre2 }, { t: '#np3', nom: lalettre }]

        break
      }
      case 'QUADRILATERE': // det les 4 points
        {
          oks1 = oks2 = oks3 = oks4 = okd1 = okd2 = okd3 = okd4 = true
          rels1 = rels2 = rels3 = rels4 = reld1 = reld2 = reld3 = reld4 = 'piege'
          okpo1 = okpo2 = okpo3 = okpo4 = true
          relpo1 = relpo2 = relpo3 = relpo4 = 'piege'
          okdr1 = true
          reldrd1 = reldrd2 = 'piege'
          rela = ['piege', 'piege', 'piege', 'piege']
          oka = [true, true, true, true]
          if (stor.element === 'cote_oppose') {
            haz = j3pGetRandomInt(0, 3)
            if (haz === 0) {
              oks1 = false
              rels3 = 'cote_oppose'
            }
            if (haz === 1) {
              oks2 = false
              rels4 = 'cote_oppose'
            }
            if (haz === 2) {
              oks3 = false
              rels1 = 'cote_oppose'
            }
            if (haz === 3) {
              oks4 = false
              rels2 = 'cote_oppose'
            }
          }
          if (stor.element === 'cote_consecutif') {
            haz = j3pGetRandomInt(0, 3)
            if (haz === 0) {
              oks1 = false
              rels4 = 'cote_consecutif'
              rels2 = 'cote_consecutif'
            }
            if (haz === 1) {
              oks2 = false
              rels3 = 'cote_consecutif'
              rels1 = 'cote_consecutif'
            }
            if (haz === 2) {
              oks3 = false
              rels4 = 'cote_consecutif'
              rels2 = 'cote_consecutif'
            }
            if (haz === 3) {
              oks4 = false
              rels3 = 'cote_consecutif'
              rels1 = 'cote_consecutif'
            }
          }
          if (stor.element === 'angle_oppose') {
            haz = j3pGetRandomInt(0, 3)
            if (haz === 0) {
              oka[0] = false
              rela[2] = 'angle_oppose'
            }
            if (haz === 1) {
              oka[1] = false
              rela[3] = 'angle_oppose'
            }
            if (haz === 2) {
              oka[2] = false
              rela[0] = 'angle_oppose'
            }
            if (haz === 3) {
              oka[3] = false
              rela[1] = 'angle_oppose'
            }
          }
          if (stor.element === 'angle_consecutif') {
            haz = j3pGetRandomInt(0, 3)
            if (haz === 0) {
              oka[0] = false
              rela[1] = rela[3] = 'angle_consecutif'
            }
            if (haz === 1) {
              oka[1] = false
              rela[2] = rela[0] = 'angle_consecutif'
            }
            if (haz === 2) {
              oka[2] = false
              rela[1] = rela[3] = 'angle_consecutif'
            }
            if (haz === 3) {
              oka[3] = false
              rela[2] = rela[0] = 'angle_consecutif'
            }
          }
          if (stor.element === 'sommet_consecutif') {
            haz = j3pGetRandomInt(0, 3)
            if (haz === 0) {
              okpo1 = false
              relpo4 = 'sommet_consecutif'
              relpo2 = 'sommet_consecutif'
            }
            if (haz === 1) {
              okpo2 = false
              relpo1 = 'sommet_consecutif'
              relpo3 = 'sommet_consecutif'
            }
            if (haz === 2) {
              okpo3 = false
              relpo2 = 'sommet_consecutif'
              relpo4 = 'sommet_consecutif'
            }
            if (haz === 3) {
              okpo4 = false
              relpo3 = 'sommet_consecutif'
              relpo1 = 'sommet_consecutif'
            }
          }
          if (stor.element === 'sommet_oppose') {
            haz = j3pGetRandomInt(0, 3)
            if (haz === 0) {
              okpo1 = false
              relpo3 = 'sommet_oppose'
            }
            if (haz === 1) {
              okpo2 = false
              relpo4 = 'sommet_oppose'
            }
            if (haz === 2) {
              okpo3 = false
              relpo1 = 'sommet_oppose'
            }
            if (haz === 3) {
              okpo4 = false
              relpo2 = 'sommet_oppose'
            }
          }

          stor.listZig = ['#cp1', '#cp2', '#cp3', '#cp4', '#cp5', '#cs1', '#cs2', '#cs3', '#cs4', '#ca1', '#ca2', '#ca3', '#ca4', '#rd1', '#rd2', '#cd1', '#cd2', '#cd3', '#cd4', '#cd5', '#cd6', '#cd7', '#dc1', '#dc2', '#dc3', '#dc4']

          this.listedeselemedessine = []
          nomspris = []
          lalettre1 = addMaj(nomspris)
          lalettre2 = addMaj(nomspris)
          lalettre3 = addMaj(nomspris)
          lalettre4 = addMaj(nomspris)

          // det ordre points pour pas croiser
          leslettresz = []
          leslettresz = [lalettre1, lalettre2, lalettre3, lalettre4]

          fkl = function (num) {
            let varok = true
            let varep = 2
            let varrel = 'piege'
            let varco = [255, 255, 255]
            // unelettre = leslettresz[num];
            if (num === 0) {
              varok = okpo1
              varep = eppo1
              varrel = relpo1
            }
            if (num === 1) {
              varok = okpo2
              varep = eppo2
              varrel = relpo2
            }
            if (num === 2) {
              varok = okpo3
              varep = eppo3
              varrel = relpo3
            }
            if (num === 3) {
              varok = okpo4
              varep = eppo4
              varrel = relpo4
            }
            if (!varok) varco = coMod
            return {
              ok: varok,
              co: varco,
              ep: varep,
              rel: varrel
            }
          }
          let fff = fkl(0)
          if (fff.ok) {
            this.listedeselemedessine.push({
              type: 'point',
              nom: leslettresz[0],
              val: ['point', 'sommet', fff.rel],
              tag: '#cp3',
              tagAllume: ['#p1', '#s1'],
              couleur: [255, 255, 255],
              ep: false
            })
          }
          fff.id = '#s1'
          stor.listeChange.push(j3pClone(fff))

          fff = fkl(1)
          if (fff.ok) {
            this.listedeselemedessine.push({
              type: 'point',
              nom: leslettresz[1],
              val: ['point', 'sommet', fff.rel],
              tag: '#cp4',
              tagAllume: ['#pF', '#sF'],
              couleur: [255, 255, 255],
              ep: false
            })
          }
          fff.id = '#sF'
          stor.listeChange.push(j3pClone(fff))

          fff = fkl(2)
          if (fff.ok) {
            this.listedeselemedessine.push({
              type: 'point',
              nom: leslettresz[2],
              val: ['point', 'sommet', fff.rel],
              tag: '#cp1',
              tagAllume: ['#pC', '#sC'],
              couleur: [255, 255, 255],
              ep: false
            })
          }
          fff.id = '#sC'
          stor.listeChange.push(j3pClone(fff))

          fff = fkl(3)
          if (fff.ok) {
            this.listedeselemedessine.push({
              type: 'point',
              nom: leslettresz[3],
              val: ['point', 'sommet', fff.rel],
              tag: '#cp2',
              tagAllume: ['#pE', '#sE'],
              couleur: [255, 255, 255],
              ep: false
            })
          }
          fff.id = '#sE'
          stor.listeChange.push(j3pClone(fff))
          // les angles
          for (i = 0; i < 4; i++) {
            if (oka[i]) {
              this.listedeselemedessine.push({
                type: 'angle',
                nom: 'angle(' + leslettresz[i] + leslettresz[(i + 1) % 4] + leslettresz[(i + 2) % 4] + ')',
                val: ['angle', rela[i]],
                tag: '#ca' + (i % 4 + 1),
                tagAllume: ['#a' + (i % 4 + 1), '#as' + (i % 4 + 1)],
                couleur: [0, 0, 0],
                ep: false
              })
            } else {
              stor.listeChange.push(
                {
                  id: '#as' + (i % 4 + 1),
                  co: coMod
                }
              )
            }
          }

          // les 4 dtes derrieres les cotes
          this.listedeselemedessine.push({
            type: 'droite',
            nom: leslettresz[0] + leslettresz[1],
            val: ['droite'],
            tag: '#cd1',
            tagAllume: ['#dd1'],
            couleur: [130, 130, 130],
            ep: 2,
            serie: 1,
            contser: true
          })
          this.listedeselemedessine.push({
            type: 'droite',
            nom: leslettresz[1] + leslettresz[2],
            val: ['droite'],
            tag: '#cd5',
            tagAllume: ['#dd2'],
            couleur: [130, 130, 130],
            ep: 2,
            serie: 2,
            contser: true
          })
          this.listedeselemedessine.push({
            type: 'droite',
            nom: leslettresz[2] + leslettresz[3],
            val: ['droite'],
            tag: '#cd2',
            tagAllume: ['#dd3'],
            couleur: [130, 130, 130],
            ep: 2,
            serie: 3,
            contser: true
          })
          this.listedeselemedessine.push({
            type: 'droite',
            nom: leslettresz[3] + leslettresz[0],
            val: ['droite'],
            tag: '#cd6',
            tagAllume: ['#dd4'],
            couleur: [130, 130, 130],
            ep: 2,
            serie: 4,
            contser: true
          })
          // les 4 cotes
          if (oks1) {
            this.listedeselemedessine.push({
              type: 'segment',
              nom: leslettresz[0] + leslettresz[1],
              val: ['segment', 'cote', rels1],
              tag: '#cs2',
              tagAllume: ['#seg1'],
              couleur: [0, 0, 0],
              ep: 3,
              serie: 1
            })
          } else {
            stor.listeChange.push({ id: '#seg1', ep: 3, co: coMod })
          }
          if (oks2) {
            this.listedeselemedessine.push({
              type: 'segment',
              nom: leslettresz[1] + leslettresz[2],
              val: ['segment', 'cote', rels2],
              tag: '#cs1',
              tagAllume: ['#seg2'],
              couleur: [0, 0, 0],
              ep: 3,
              serie: 2
            })
          } else {
            stor.listeChange.push({ id: '#seg2', ep: 3, co: coMod })
          }
          if (oks3) {
            this.listedeselemedessine.push({
              type: 'segment',
              nom: leslettresz[2] + leslettresz[3],
              val: ['segment', 'cote', rels3],
              tag: '#cs4',
              tagAllume: ['#seg3'],
              couleur: [0, 0, 0],
              ep: 3,
              serie: 3
            })
          } else {
            stor.listeChange.push({ id: '#seg3', ep: 3, co: coMod })
          }
          if (oks4) {
            this.listedeselemedessine.push({
              type: 'segment',
              nom: leslettresz[3] + leslettresz[0],
              val: ['segment', 'cote', rels4],
              tag: '#cs3',
              tagAllume: ['#seg4'],
              couleur: [0, 0, 0],
              ep: 3,
              serie: 4
            })
          } else {
            stor.listeChange.push({ id: '#seg4', ep: 3, co: coMod })
          }

          // les  dtes derrieres les diag
          this.listedeselemedessine.push({
            type: 'droite',
            nom: leslettresz[0] + leslettresz[2],
            val: ['droite'],
            tag: '#cd4',
            tagAllume: ['#ddiag1'],
            couleur: [130, 130, 130],
            ep: 2,
            serie: 6,
            contser: true
          })
          this.listedeselemedessine.push({
            type: 'droite',
            nom: leslettresz[1] + leslettresz[3],
            val: ['droite'],
            tag: '#cd3',
            tagAllume: ['#ddiag2'],
            couleur: [130, 130, 130],
            ep: 2,
            serie: 5,
            contser: true
          })

          lalettre1 = addMaj(nomspris)
          this.listedeselemedessine.push({
            type: 'point',
            nom: lalettre1,
            val: ['point'],
            tag: '#cp5',
            tagAllume: ['#pM', '#sM'],
            couleur: [255, 255, 255],
            ep: false
          })

          if (stor.element === 'diagonale') {
            stor.listCache.push('#diag1p1', '#diag2p1', '#diag1p2', '#diag2p2', '#dc1', '#dc2', '#dc3', '#dc4')
            if (okd1) {
              this.listedeselemedessine.push({
                type: 'segment',
                nom: leslettresz[0] + leslettresz[2],
                val: ['segment', 'diagonale'],
                tag: '#rd1',
                tagAllume: ['#diag1'],
                couleur: [130, 130, 130],
                ep: 2,
                serie: 5
              })
            }
            if (okd2) {
              this.listedeselemedessine.push({
                type: 'segment',
                nom: leslettresz[1] + leslettresz[3],
                val: ['segment', 'diagonale'],
                tag: '#rd2',
                tagAllume: ['#diag2'],
                couleur: [130, 130, 130],
                ep: 2,
                serie: 6
              })
            }
          } else {
            stor.listCache.push('#diag1', '#diag2', '#rd1', '#rd2')
            // les diages
            if (okd1) {
              this.listedeselemedessine.push({
                type: 'segment',
                nom: lalettre1 + leslettresz[2],
                val: ['segment', 'diagonale', reld1],
                tag: '#dc2',
                tagAllume: ['#diag1p1'],
                couleur: [130, 130, 130],
                ep: 2,
                serie: 5
              })
            }
            if (okd2) {
              this.listedeselemedessine.push({
                type: 'segment',
                nom: lalettre1 + leslettresz[3],
                val: ['segment', 'diagonale', reld2],
                tag: '#dc4',
                tagAllume: ['#diag2p1'],
                couleur: [130, 130, 130],
                ep: 2,
                serie: 6
              })
            }
            if (okd3) {
              this.listedeselemedessine.push({
                type: 'segment',
                nom: leslettresz[0] + lalettre1,
                val: ['segment', 'diagonale', reld3],
                tag: '#dc1',
                tagAllume: ['#diag1p2'],
                couleur: [130, 130, 130],
                ep: 2,
                serie: 5
              })
            }
            if (okd4) {
              this.listedeselemedessine.push({
                type: 'segment',
                nom: leslettresz[1] + lalettre1,
                val: ['segment', 'diagonale', reld4],
                tag: '#dc3',
                tagAllume: ['#diag2p2'],
                couleur: [130, 130, 130],
                ep: 2,
                serie: 6
              })
            }
          }

          stor.LeNomDuTruc = leslettresz[0] + leslettresz[1] + leslettresz[2] + leslettresz[3]
          A1 = j3pGetRandomInt(0, 359)
          A2 = j3pGetRandomInt(20, 30) / 10
          A3 = j3pGetRandomInt(35, 45) / 10
          listeNom = [{ t: '#np1', nom: leslettresz[0] }, { t: '#npF', nom: leslettresz[1] }, {
            t: '#npC',
            nom: leslettresz[2]
          }, { t: '#npE', nom: leslettresz[3] }, { t: '#npM', nom: lalettre1 }]
        }
        break
      case 'PARALLELOGRAMME':
        oks1 = oks2 = oks3 = oks4 = okd1 = okd2 = okd3 = okd4 = true
        rels1 = rels2 = rels3 = rels4 = reld1 = reld2 = reld3 = reld4 = 'piege'
        okpo1 = okpo2 = okpo3 = okpo4 = true
        relpo1 = relpo2 = relpo3 = relpo4 = 'piege'
        okdr1 = true
        reldrd1 = reldrd2 = 'piege'
        rela = ['piege', 'piege', 'piege', 'piege']
        oka = [true, true, true, true]

        stor.listZig = ['#cp1', '#cp2', '#cp3', '#cp4', '#cp5', '#cs1', '#cs2', '#cs3', '#cs4', '#ca1', '#ca2', '#ca3', '#ca4', '#cdd1', '#cdd2', '#cd1', '#cd2', '#cd3', '#cd4', '#cd5', '#cd6', '#cd7', '#cd8', '#cd9', '#cr1', '#cr2', '#cr3', '#cr4']

        if (stor.element === 'cote_oppose') {
          haz = j3pGetRandomInt(0, 3)
          if (haz === 0) {
            oks1 = false
            rels3 = 'cote_oppose'
          }
          if (haz === 1) {
            oks2 = false
            rels4 = 'cote_oppose'
          }
          if (haz === 2) {
            oks3 = false
            rels1 = 'cote_oppose'
          }
          if (haz === 3) {
            oks4 = false
            rels2 = 'cote_oppose'
          }
        }
        if (stor.element === 'cote_consecutif') {
          haz = j3pGetRandomInt(0, 3)
          if (haz === 0) {
            oks1 = false
            rels4 = 'cote_consecutif'
            rels2 = 'cote_consecutif'
          }
          if (haz === 1) {
            oks2 = false
            rels3 = 'cote_consecutif'
            rels1 = 'cote_consecutif'
          }
          if (haz === 2) {
            oks3 = false
            rels4 = 'cote_consecutif'
            rels2 = 'cote_consecutif'
          }
          if (haz === 3) {
            oks4 = false
            rels3 = 'cote_consecutif'
            rels1 = 'cote_consecutif'
          }
        }
        if (stor.element === 'cote_perpendiculaire') {
          haz = j3pGetRandomInt(0, 3)
          if (haz === 0) {
            oks1 = false
            rels4 = 'cote_perpendiculaire'
            rels2 = 'cote_perpendiculaire'
          }
          if (haz === 1) {
            oks2 = false
            rels1 = 'cote_perpendiculaire'
            rels3 = 'cote_perpendiculaire'
          }
          if (haz === 2) {
            oks3 = false
            rels4 = 'cote_perpendiculaire'
            rels2 = 'cote_perpendiculaire'
          }
          if (haz === 3) {
            oks4 = false
            rels1 = 'cote_perpendiculaire'
            rels3 = 'cote_perpendiculaire'
          }
        }
        if (stor.element === 'cote_parallele') {
          haz = j3pGetRandomInt(0, 3)
          if (haz === 0) {
            oks1 = false
            rels3 = 'cote_parallele'
          }
          if (haz === 1) {
            oks2 = false
            rels4 = 'cote_parallele'
          }
          if (haz === 2) {
            oks3 = false
            rels1 = 'cote_parallele'
          }
          if (haz === 3) {
            oks4 = false
            rels2 = 'cote_parallele'
          }
        }
        if (stor.element === 'hauteur_relative') {
          haz = j3pGetRandomInt(0, 1)
          if (haz === 0) {
            oks3 = false
          }
          if (haz === 1) {
            oks1 = false
          }
        }
        if (stor.element === 'angle_oppose') {
          haz = j3pGetRandomInt(0, 3)
          if (haz === 0) {
            oka[0] = false
            rela[2] = 'angle_oppose'
          }
          if (haz === 1) {
            oka[1] = false
            rela[3] = 'angle_oppose'
          }
          if (haz === 2) {
            oka[2] = false
            rela[0] = 'angle_oppose'
          }
          if (haz === 3) {
            oka[3] = false
            rela[1] = 'angle_oppose'
          }
        }
        if (stor.element === 'angle_consecutif') {
          haz = j3pGetRandomInt(0, 3)
          if (haz === 0) {
            oka[0] = false
            rela[1] = rela[3] = 'angle_consecutif'
          }
          if (haz === 1) {
            oka[1] = false
            rela[2] = rela[0] = 'angle_consecutif'
          }
          if (haz === 2) {
            oka[2] = false
            rela[1] = rela[3] = 'angle_consecutif'
          }
          if (haz === 3) {
            oka[3] = false
            rela[2] = rela[0] = 'angle_consecutif'
          }
        }
        if (stor.element === 'base_relative') {
          okdr1 = false
        }
        if (stor.element === 'longueur_egale') {
          haz = j3pGetRandomInt(0, 7)
          // cote
          if (haz === 0) {
            oks1 = false
            rels3 = 'longueur_egale'
          }
          if (haz === 1) {
            oks2 = false
            rels4 = 'longueur_egale'
          }
          if (haz === 2) {
            oks3 = false
            rels1 = 'longueur_egale'
          }
          if (haz === 3) {
            oks4 = false
            rels2 = 'longueur_egale'
          }
          // demidiag
          if (haz === 4) {
            okd1 = false
            reld3 = 'longueur_egale'
          }
          if (haz === 5) {
            okd4 = false
            reld2 = 'longueur_egale'
          }
          if (haz === 6) {
            okd3 = false
            reld1 = 'longueur_egale'
          }
          if (haz === 7) {
            okd4 = false
            reld2 = 'longueur_egale'
          }
        }
        if (stor.element === 'angle_egal') {
          haz = j3pGetRandomInt(0, 3)
          if (haz === 0) {
            oka[0] = false
            rela[2] = 'angle_egal'
          }
          if (haz === 1) {
            oka[1] = false
            rela[3] = 'angle_egal'
          }
          if (haz === 2) {
            oka[2] = false
            rela[0] = 'angle_egal'
          }
          if (haz === 3) {
            oka[3] = false
            rela[1] = 'angle_egal'
          }
        }
        if (stor.element === 'sommet_consecutif') {
          haz = j3pGetRandomInt(0, 3)
          if (haz === 0) {
            okpo1 = false
            eppo1 = 5
            relpo4 = 'sommet_consecutif'
            relpo3 = 'sommet_consecutif'
          }
          if (haz === 1) {
            okpo2 = false
            eppo2 = 5
            relpo4 = 'sommet_consecutif'
            relpo3 = 'sommet_consecutif'
          }
          if (haz === 2) {
            okpo3 = false
            eppo3 = 5
            relpo1 = 'sommet_consecutif'
            relpo2 = 'sommet_consecutif'
          }
          if (haz === 3) {
            okpo4 = false
            eppo4 = 5
            relpo1 = 'sommet_consecutif'
            relpo2 = 'sommet_consecutif'
          }
        }
        if (stor.element === 'sommet_oppose') {
          haz = j3pGetRandomInt(0, 3)
          if (haz === 0) {
            okpo1 = false
            eppo1 = 5
            relpo2 = 'sommet_oppose'
          }
          if (haz === 1) {
            okpo2 = false
            eppo2 = 5
            relpo1 = 'sommet_oppose'
          }
          if (haz === 2) {
            okpo3 = false
            eppo3 = 5
            relpo4 = 'sommet_oppose'
          }
          if (haz === 3) {
            okpo4 = false
            eppo4 = 5
            relpo3 = 'sommet_oppose'
          }
        }

        this.listedeselemedessine = []
        nomspris = []

        lalettre1 = addMaj(nomspris)
        this.listedeselemedessine.push({
          type: 'point',
          nom: lalettre1,
          val: ['point', 'centre_symetrie'],
          tag: '#cp5',
          tagAllume: ['#pCentre', '#sCentre'],
          couleur: [255, 255, 255],
          ep: false
        })

        // les 4 sommets
        lalettre2 = addMaj(nomspris)
        if (okpo1) {
          this.listedeselemedessine.push({
            type: 'point',
            nom: lalettre2,
            val: ['point', 'sommet', relpo1],
            tag: '#cp2',
            tagAllume: ['#pD', '#sD'],
            couleur: [255, 255, 255],
            ep: false
          })
        } else {
          stor.listeChange.push({ id: '#sD', co: coMod })
        }
        lalettre3 = addMaj(nomspris)
        if (okpo2) {
          this.listedeselemedessine.push({
            type: 'point',
            nom: lalettre3,
            val: ['point', 'sommet', relpo2],
            tag: '#cp4',
            tagAllume: ['#pC', '#sC'],
            couleur: [255, 255, 255],
            ep: false
          })
        } else {
          stor.listeChange.push({ id: '#sC', co: coMod })
        }
        lalettre4 = addMaj(nomspris)
        if (okpo3) {
          this.listedeselemedessine.push({
            type: 'point',
            nom: lalettre4,
            val: ['point', 'sommet', relpo3],
            tag: '#cp3',
            tagAllume: ['#pF', '#sF'],
            couleur: [255, 255, 255],
            ep: false
          })
        } else {
          stor.listeChange.push({ id: '#sF', co: coMod })
        }
        lalettre5 = addMaj(nomspris)
        if (okpo4) {
          this.listedeselemedessine.push({
            type: 'point',
            nom: lalettre5,
            val: ['point', 'sommet', relpo4],
            tag: '#cp1',
            tagAllume: ['#pE', '#sE'],
            couleur: [255, 255, 255],
            ep: false
          })
        } else {
          stor.listeChange.push({ id: '#sE', co: coMod })
        }
        leslettresz = [lalettre2, lalettre4, lalettre3, lalettre5]
        // les angles
        for (i = 0; i < 4; i++) {
          if (oka[i]) {
            this.listedeselemedessine.push({
              type: 'angle',
              nom: 'angle(' + leslettresz[i] + leslettresz[(i + 1) % 4] + leslettresz[(i + 2) % 4] + ')',
              val: ['angle', rela[i]],
              tag: '#ca' + (i % 4 + 1),
              tagAllume: ['#a' + (i % 4 + 1), '#as' + (i % 4 + 1)],
              couleur: [0, 0, 0],
              ep: false
            })
          } else {
            stor.listeChange.push(
              {
                id: '#as' + (i % 4 + 1),
                co: coMod
              }
            )
          }
        }

        // une hauteur
        if (okdr1) {
          this.listedeselemedessine.push({
            type: 'droite',
            nom: lalettre5 + 'ô',
            val: ['droite', 'hauteur_relative', 'hauteur'],
            tag: '#cd8',
            tagAllume: ['#hauteur'],
            couleur: [130, 130, 130],
            ep: 2
          })
        } else {
          stor.listeChange.push({ id: '#hauteur', ep: 3, co: coMod })
        }

        // 2 axe piege
        this.listedeselemedessine.push({
          type: 'droite',
          nom: 'ùk',
          val: ['droite'],
          tag: '#cd7',
          tagAllume: ['#axe1'],
          couleur: [130, 130, 130],
          ep: 2
        })
        this.listedeselemedessine.push({
          type: 'droite',
          nom: 'ùl',
          val: ['droite'],
          tag: '#cd3',
          tagAllume: ['#axe2'],
          couleur: [130, 130, 130],
          ep: 2
        })

        // les 4 dtes derrieres les cotes
        this.listedeselemedessine.push({
          type: 'droite',
          nom: leslettresz[0] + leslettresz[1],
          val: ['droite'],
          tag: '#cd4',
          tagAllume: ['#dd1'],
          couleur: [130, 130, 130],
          ep: 2,
          serie: 1,
          contser: true
        })
        this.listedeselemedessine.push({
          type: 'droite',
          nom: leslettresz[1] + leslettresz[2],
          val: ['droite'],
          tag: '#cd6',
          tagAllume: ['#dd2'],
          couleur: [130, 130, 130],
          ep: 2,
          serie: 2,
          contser: true
        })
        this.listedeselemedessine.push({
          type: 'droite',
          nom: leslettresz[2] + leslettresz[3],
          val: ['droite'],
          tag: '#cd2',
          tagAllume: ['#dd3'],
          couleur: [130, 130, 130],
          ep: 2,
          serie: 3,
          contser: true
        })
        this.listedeselemedessine.push({
          type: 'droite',
          nom: leslettresz[3] + leslettresz[0],
          val: ['droite'],
          tag: '#cd9',
          tagAllume: ['#dd4'],
          couleur: [130, 130, 130],
          ep: 2,
          serie: 4,
          contser: true
        })
        // les 4 cotes
        if (oks1) {
          this.listedeselemedessine.push({
            type: 'segment',
            nom: leslettresz[0] + leslettresz[1],
            val: ['segment', 'base_relative', 'cote', rels1],
            tag: '#cs4',
            tagAllume: ['#seg1'],
            couleur: [0, 0, 0],
            ep: 3,
            serie: 1
          })
        } else {
          stor.listeChange.push({ id: '#seg1', ep: 3, co: coMod })
        }
        if (oks2) {
          this.listedeselemedessine.push({
            type: 'segment',
            nom: leslettresz[1] + leslettresz[2],
            val: ['segment', 'cote', rels2],
            tag: '#cs1',
            tagAllume: ['#seg2'],
            couleur: [0, 0, 0],
            ep: 3,
            serie: 2
          })
        } else {
          stor.listeChange.push({ id: '#seg2', ep: 3, co: coMod })
        }
        if (oks3) {
          this.listedeselemedessine.push({
            type: 'segment',
            nom: leslettresz[2] + leslettresz[3],
            val: ['segment', 'base_relative', 'cote', rels3],
            tag: '#cs2',
            tagAllume: ['#seg3'],
            couleur: [0, 0, 0],
            ep: 2,
            serie: 3
          })
        } else {
          stor.listeChange.push({ id: '#seg3', ep: 3, co: coMod })
        }
        if (oks4) {
          this.listedeselemedessine.push({
            type: 'segment',
            nom: leslettresz[3] + leslettresz[0],
            val: ['segment', 'cote', rels4],
            tag: '#cs3',
            tagAllume: ['#seg4'],
            couleur: [0, 0, 0],
            ep: 2,
            serie: 4
          })
        } else {
          stor.listeChange.push({ id: '#seg4', ep: 3, co: coMod })
        }

        // les  dtes derrieres les diag
        this.listedeselemedessine.push({
          type: 'droite',
          nom: leslettresz[0] + leslettresz[2],
          val: ['droite'],
          tag: '#cd1',
          tagAllume: ['#diag1bis'],
          couleur: [130, 130, 130],
          ep: 2,
          serie: 5,
          contser: true
        })
        this.listedeselemedessine.push({
          type: 'droite',
          nom: leslettresz[1] + leslettresz[3],
          val: ['droite'],
          tag: '#cd5',
          tagAllume: ['#diag3'],
          couleur: [130, 130, 130],
          ep: 2,
          serie: 6,
          contser: true
        })

        if (stor.element === 'diagonale') {
          stor.listCache.push('#diag1p1', '#diag2p1', '#diag1p2', '#diag2p2', '#cr1', '#cr2', '#cr3', '#cr4')
          if (okd1) {
            this.listedeselemedessine.push({
              type: 'segment',
              nom: leslettresz[0] + leslettresz[2],
              val: ['segment', 'diagonale', 'longueur_egale'],
              tag: '#cdd2',
              tagAllume: ['#mdiag1'],
              couleur: [130, 130, 130],
              ep: 2,
              serie: 5
            })
          } else {
            stor.listeChange.push({ id: '#mdiag1', ep: 3, co: coMod })
          }
          if (okd2) {
            this.listedeselemedessine.push({
              type: 'segment',
              nom: leslettresz[1] + leslettresz[3],
              val: ['segment', 'diagonale', 'longueur_egale'],
              tag: '#cdd1',
              tagAllume: ['#mdiag2'],
              couleur: [130, 130, 130],
              ep: 2,
              serie: 6
            })
          } else {
            stor.listeChange.push({ id: '#mdiag2', ep: 3, co: coMod })
          }
        } else {
          stor.listCache.push('#mdiag1', '#mdiag2', '#cdd1', '#cdd2')
          // les diages
          if (okd1) {
            this.listedeselemedessine.push({
              type: 'segment',
              nom: lalettre1 + leslettresz[2],
              val: ['segment', 'diagonale', reld1],
              tag: '#cr2',
              tagAllume: ['#diag1p1'],
              couleur: [130, 130, 130],
              ep: 2,
              serie: 5
            })
          } else {
            stor.listeChange.push({ id: '#diag1p1', ep: 3, co: coMod })
          }
          if (okd2) {
            this.listedeselemedessine.push({
              type: 'segment',
              nom: lalettre1 + leslettresz[3],
              val: ['segment', 'diagonale', reld2],
              tag: '#cr3',
              tagAllume: ['#diag2p1'],
              couleur: [130, 130, 130],
              ep: 2,
              serie: 6
            })
          } else {
            stor.listeChange.push({ id: '#diag2p2', ep: 3, co: coMod })
          }
          if (okd3) {
            this.listedeselemedessine.push({
              type: 'segment',
              nom: leslettresz[0] + lalettre1,
              val: ['segment', 'diagonale', reld3],
              tag: '#cr1',
              tagAllume: ['#diag1p2'],
              couleur: [130, 130, 130],
              ep: 2,
              serie: 5
            })
          } else {
            stor.listeChange.push({ id: '#diag1p2', ep: 3, co: coMod })
          }
          if (okd4) {
            this.listedeselemedessine.push({
              type: 'segment',
              nom: leslettresz[1] + lalettre1,
              val: ['segment', 'diagonale', reld4],
              tag: '#cr4',
              tagAllume: ['#diag2p2'],
              couleur: [130, 130, 130],
              ep: 2,
              serie: 6
            })
          } else {
            stor.listeChange.push({ id: '#diag2p2', ep: 3, co: coMod })
          }
        }

        stor.LeNomDuTruc = lalettre2 + lalettre4 + lalettre3 + lalettre5
        A1 = j3pGetRandomInt(1, 360)
        A2 = j3pGetRandomInt(30, 75)
        A3 = j3pGetRandomInt(25, 30) / 10
        listeNom = [{ t: '#npD', nom: leslettresz[0] }, { t: '#npF', nom: leslettresz[1] }, {
          t: '#npC',
          nom: leslettresz[2]
        }, { t: '#npE', nom: leslettresz[3] }, { t: '#npCentre', nom: lalettre1 }]

        break
      case 'LOSANGE':
        oks1 = oks2 = oks3 = oks4 = okd1 = okd2 = okd3 = okd4 = true
        rels1 = rels2 = rels3 = rels4 = reld1 = reld2 = reld3 = reld4 = 'piege'
        okpo1 = okpo2 = okpo3 = okpo4 = true
        eppo1 = eppo2 = eppo3 = eppo4 = 2
        relpo1 = relpo2 = relpo3 = relpo4 = 'piege'
        okdr1 = true
        reldrd1 = reldrd2 = 'piege'
        rela = ['piege', 'piege', 'piege', 'piege']
        oka = [true, true, true, true]
        stor.listZig = ['#cp1', '#cp2', '#cp3', '#cp4', '#cp5', '#cs1', '#cs2', '#cs3', '#cs4', '#ca1', '#ca2', '#ca3', '#ca4', '#cdd1', '#cdd2', '#cd1', '#cd2', '#cd3', '#cd4', '#cd5', '#cd6', '#cd7', '#cd8', '#cd9', '#cr1', '#cr2', '#cr3', '#cr4']

        if (stor.element === 'cote_oppose') {
          haz = j3pGetRandomInt(0, 3)
          if (haz === 0) {
            oks1 = false
            rels3 = 'cote_oppose'
          }
          if (haz === 1) {
            oks2 = false
            rels4 = 'cote_oppose'
          }
          if (haz === 2) {
            oks3 = false
            rels1 = 'cote_oppose'
          }
          if (haz === 3) {
            oks4 = false
            rels2 = 'cote_oppose'
          }
        }
        if (stor.element === 'cote_consecutif') {
          haz = j3pGetRandomInt(0, 3)
          if (haz === 0) {
            oks1 = false
            rels4 = 'cote_consecutif'
            rels2 = 'cote_consecutif'
          }
          if (haz === 1) {
            oks2 = false
            rels3 = 'cote_consecutif'
            rels1 = 'cote_consecutif'
          }
          if (haz === 2) {
            oks3 = false
            rels4 = 'cote_consecutif'
            rels2 = 'cote_consecutif'
          }
          if (haz === 3) {
            oks4 = false
            rels3 = 'cote_consecutif'
            rels1 = 'cote_consecutif'
          }
        }
        if (stor.element === 'cote_parallele') {
          haz = j3pGetRandomInt(0, 3)
          if (haz === 0) {
            oks1 = false
            rels3 = 'cote_parallele'
          }
          if (haz === 1) {
            oks2 = false
            rels4 = 'cote_parallele'
          }
          if (haz === 2) {
            oks3 = false
            rels1 = 'cote_parallele'
          }
          if (haz === 3) {
            oks4 = false
            rels2 = 'cote_parallele'
          }
        }
        if (stor.element === 'longueur_egale') {
          haz = j3pGetRandomInt(0, 7)
          // cote
          if (haz === 0) {
            oks1 = false
            rels3 = 'longueur_egale'
            rels2 = 'longueur_egale'
            rels4 = 'longueur_egale'
          }
          if (haz === 1) {
            oks2 = false
            rels1 = 'longueur_egale'
            rels3 = 'longueur_egale'
            rels4 = 'longueur_egale'
          }
          if (haz === 2) {
            oks3 = false
            rels1 = 'longueur_egale'
            rels2 = 'longueur_egale'
            rels4 = 'longueur_egale'
          }
          if (haz === 3) {
            oks4 = false
            rels2 = 'longueur_egale'
            rels3 = 'longueur_egale'
            rels1 = 'longueur_egale'
          }
          // demidiag
          if (haz === 4) {
            okd1 = false
            reld3 = 'longueur_egale'
          }
          if (haz === 5) {
            okd4 = false
            reld2 = 'longueur_egale'
          }
          if (haz === 6) {
            okd3 = false
            reld1 = 'longueur_egale'
          }
          if (haz === 7) {
            okd4 = false
            reld2 = 'longueur_egale'
          }
        }
        if (stor.element === 'angle_egal') {
          haz = j3pGetRandomInt(0, 3)
          if (haz === 0) {
            oka[0] = false
            rela[2] = 'angle_egal'
          }
          if (haz === 1) {
            oka[1] = false
            rela[3] = 'angle_egal'
          }
          if (haz === 2) {
            oka[2] = false
            rela[0] = 'angle_egal'
          }
          if (haz === 3) {
            oka[3] = false
            rela[1] = 'angle_egal'
          }
        }
        if (stor.element === 'angle_oppose') {
          haz = j3pGetRandomInt(0, 3)
          if (haz === 0) {
            oka[0] = false
            rela[2] = 'angle_oppose'
          }
          if (haz === 1) {
            oka[1] = false
            rela[3] = 'angle_oppose'
          }
          if (haz === 2) {
            oka[2] = false
            rela[0] = 'angle_oppose'
          }
          if (haz === 3) {
            oka[3] = false
            rela[1] = 'angle_oppose'
          }
        }
        if (stor.element === 'angle_consecutif') {
          haz = j3pGetRandomInt(0, 3)
          if (haz === 0) {
            oka[0] = false
            rela[1] = rela[3] = 'angle_consecutif'
          }
          if (haz === 1) {
            oka[1] = false
            rela[2] = rela[0] = 'angle_consecutif'
          }
          if (haz === 2) {
            oka[2] = false
            rela[1] = rela[3] = 'angle_consecutif'
          }
          if (haz === 3) {
            oka[3] = false
            rela[2] = rela[0] = 'angle_consecutif'
          }
        }
        if (stor.element === 'sommet_consecutif') {
          haz = j3pGetRandomInt(0, 3)
          if (haz === 0) {
            okpo1 = false
            relpo4 = 'sommet_consecutif'
            relpo3 = 'sommet_consecutif'
          }
          if (haz === 1) {
            okpo2 = false
            relpo4 = 'sommet_consecutif'
            relpo3 = 'sommet_consecutif'
          }
          if (haz === 2) {
            okpo3 = false
            relpo1 = 'sommet_consecutif'
            relpo2 = 'sommet_consecutif'
          }
          if (haz === 3) {
            okpo4 = false
            relpo1 = 'sommet_consecutif'
            relpo2 = 'sommet_consecutif'
          }
        }
        if (stor.element === 'sommet_oppose') {
          haz = j3pGetRandomInt(0, 3)
          if (haz === 0) {
            okpo1 = false
            relpo2 = 'sommet_oppose'
          }
          if (haz === 1) {
            okpo2 = false
            relpo1 = 'sommet_oppose'
          }
          if (haz === 2) {
            okpo3 = false
            relpo4 = 'sommet_oppose'
          }
          if (haz === 3) {
            okpo4 = false
            relpo3 = 'sommet_oppose'
          }
        }

        this.listedeselemedessine = []
        nomspris = []

        lalettre1 = addMaj(nomspris)
        this.listedeselemedessine.push({
          type: 'point',
          nom: lalettre1,
          val: ['point', 'centre_symetrie'],
          tag: '#cp5',
          tagAllume: ['#pCentre', '#sCentre'],
          couleur: [255, 255, 255],
          ep: false
        })

        // les 4 sommets
        lalettre2 = addMaj(nomspris)
        if (okpo1) {
          this.listedeselemedessine.push({
            type: 'point',
            nom: lalettre2,
            val: ['point', 'sommet', relpo1],
            tag: '#cp2',
            tagAllume: ['#pD', '#sD'],
            couleur: [255, 255, 255],
            ep: false
          })
        } else {
          stor.listeChange.push({ id: '#sD', co: coMod })
        }
        lalettre3 = addMaj(nomspris)
        if (okpo2) {
          this.listedeselemedessine.push({
            type: 'point',
            nom: lalettre3,
            val: ['point', 'sommet', relpo2],
            tag: '#cp4',
            tagAllume: ['#pC', '#sC'],
            couleur: [255, 255, 255],
            ep: false
          })
        } else {
          stor.listeChange.push({ id: '#sC', co: coMod })
        }
        lalettre4 = addMaj(nomspris)
        if (okpo3) {
          this.listedeselemedessine.push({
            type: 'point',
            nom: lalettre4,
            val: ['point', 'sommet', relpo3],
            tag: '#cp3',
            tagAllume: ['#pF', '#sF'],
            couleur: [255, 255, 255],
            ep: false
          })
        } else {
          stor.listeChange.push({ id: '#sF', co: coMod })
        }
        lalettre5 = addMaj(nomspris)
        if (okpo4) {
          this.listedeselemedessine.push({
            type: 'point',
            nom: lalettre5,
            val: ['point', 'sommet', relpo4],
            tag: '#cp1',
            tagAllume: ['#pE', '#sE'],
            couleur: [255, 255, 255],
            ep: false
          })
        } else {
          stor.listeChange.push({ id: '#sE', co: coMod })
        }

        leslettresz = [lalettre2, lalettre4, lalettre3, lalettre5]
        // les angles
        for (i = 0; i < 4; i++) {
          if (oka[i]) {
            this.listedeselemedessine.push({
              type: 'angle',
              nom: 'angle(' + leslettresz[i] + leslettresz[(i + 1) % 4] + leslettresz[(i + 2) % 4] + ')',
              val: ['angle', rela[i]],
              tag: '#ca' + (i % 4 + 1),
              tagAllume: ['#a' + (i % 4 + 1), '#as' + (i % 4 + 1)],
              couleur: [0, 0, 0],
              ep: false
            })
          } else {
            stor.listeChange.push(
              {
                id: '#as' + (i % 4 + 1),
                co: coMod
              }
            )
          }
        }

        // 2 axe piege
        this.listedeselemedessine.push({
          type: 'droite',
          nom: 'ùk',
          val: ['droite'],
          tag: '#cd7',
          tagAllume: ['#axe1'],
          couleur: [130, 130, 130],
          ep: 2
        })
        this.listedeselemedessine.push({
          type: 'droite',
          nom: 'ùl',
          val: ['droite'],
          tag: '#cd3',
          tagAllume: ['#axe2'],
          couleur: [130, 130, 130],
          ep: 2
        })

        // les 4 dtes derrieres les cotes
        this.listedeselemedessine.push({
          type: 'droite',
          nom: leslettresz[0] + leslettresz[1],
          val: ['droite'],
          tag: '#cd4',
          tagAllume: ['#dd1'],
          couleur: [130, 130, 130],
          ep: 2,
          serie: 1,
          contser: true
        })
        this.listedeselemedessine.push({
          type: 'droite',
          nom: leslettresz[1] + leslettresz[2],
          val: ['droite'],
          tag: '#cd6',
          tagAllume: ['#dd2'],
          couleur: [130, 130, 130],
          ep: 2,
          serie: 2,
          contser: true
        })
        this.listedeselemedessine.push({
          type: 'droite',
          nom: leslettresz[2] + leslettresz[3],
          val: ['droite'],
          tag: '#cd2',
          tagAllume: ['#dd3'],
          couleur: [130, 130, 130],
          ep: 2,
          serie: 3,
          contser: true
        })
        this.listedeselemedessine.push({
          type: 'droite',
          nom: leslettresz[3] + leslettresz[0],
          val: ['droite'],
          tag: '#cd9',
          tagAllume: ['#dd4'],
          couleur: [130, 130, 130],
          ep: 2,
          serie: 4,
          contser: true
        })
        // les 4 cotes
        if (oks1) {
          this.listedeselemedessine.push({
            type: 'segment',
            nom: leslettresz[0] + leslettresz[1],
            val: ['segment', 'base_relative', 'cote', rels1],
            tag: '#cs4',
            tagAllume: ['#seg1'],
            couleur: [0, 0, 0],
            ep: 3,
            serie: 1
          })
        } else {
          stor.listeChange.push({ id: '#seg1', ep: 4, co: coMod })
        }
        if (oks2) {
          this.listedeselemedessine.push({
            type: 'segment',
            nom: leslettresz[1] + leslettresz[2],
            val: ['segment', 'cote', rels2],
            tag: '#cs1',
            tagAllume: ['#seg2'],
            couleur: [0, 0, 0],
            ep: 3,
            serie: 2
          })
        } else {
          stor.listeChange.push({ id: '#seg2', ep: 4, co: coMod })
        }
        if (oks3) {
          this.listedeselemedessine.push({
            type: 'segment',
            nom: leslettresz[2] + leslettresz[3],
            val: ['segment', 'base_relative', 'cote', rels3],
            tag: '#cs2',
            tagAllume: ['#seg3'],
            couleur: [0, 0, 0],
            ep: 3,
            serie: 3
          })
        } else {
          stor.listeChange.push({ id: '#seg3', ep: 4, co: coMod })
        }
        if (oks4) {
          this.listedeselemedessine.push({
            type: 'segment',
            nom: leslettresz[3] + leslettresz[0],
            val: ['segment', 'cote', rels4],
            tag: '#cs3',
            tagAllume: ['#seg4'],
            couleur: [0, 0, 0],
            ep: 3,
            serie: 4
          })
        } else {
          stor.listeChange.push({ id: '#seg4', ep: 4, co: coMod })
        }

        // les  dtes derrieres les diag
        this.listedeselemedessine.push({
          type: 'droite',
          nom: leslettresz[0] + leslettresz[2],
          val: ['droite', 'axe'],
          tag: '#cd1',
          tagAllume: ['#diag1bis'],
          couleur: [130, 130, 130],
          ep: 2,
          serie: 5,
          contser: true
        })
        this.listedeselemedessine.push({
          type: 'droite',
          nom: leslettresz[1] + leslettresz[3],
          val: ['droite', 'axe'],
          tag: '#cd5',
          tagAllume: ['#diag3'],
          couleur: [130, 130, 130],
          ep: 2,
          serie: 6,
          contser: true
        })

        if (stor.element === 'diagonale') {
          stor.listCache.push('#diag1p1', '#diag2p1', '#diag1p2', '#diag2p2', '#cr1', '#cr2', '#cr3', '#cr4')
          if (okd1) {
            this.listedeselemedessine.push({
              type: 'segment',
              nom: leslettresz[0] + leslettresz[2],
              val: ['segment', 'diagonale', 'longueur_egale'],
              tag: '#cdd2',
              tagAllume: ['#mdiag1'],
              couleur: [130, 130, 130],
              ep: 2,
              serie: 5
            })
          } else {
            stor.listeChange.push({ id: '#mdiag1', ep: 3, co: coMod })
          }
          if (okd2) {
            this.listedeselemedessine.push({
              type: 'segment',
              nom: leslettresz[1] + leslettresz[3],
              val: ['segment', 'diagonale', 'longueur_egale'],
              tag: '#cdd1',
              tagAllume: ['#mdiag2'],
              couleur: [130, 130, 130],
              ep: 2,
              serie: 6
            })
          } else {
            stor.listeChange.push({ id: '#mdiag2', ep: 3, co: coMod })
          }
        } else {
          stor.listCache.push('#mdiag1', '#mdiag2', '#cdd1', '#cdd2')
          // les diages
          if (okd1) {
            this.listedeselemedessine.push({
              type: 'segment',
              nom: lalettre1 + leslettresz[2],
              val: ['segment', 'diagonale', reld1, 'axe'],
              tag: '#cr2',
              tagAllume: ['#diag1p1'],
              couleur: [130, 130, 130],
              ep: 2,
              serie: 5
            })
          } else {
            stor.listeChange.push({ id: '#diag1p1', ep: 3, co: coMod })
          }
          if (okd2) {
            this.listedeselemedessine.push({
              type: 'segment',
              nom: lalettre1 + leslettresz[3],
              val: ['segment', 'diagonale', reld2, 'axe'],
              tag: '#cr3',
              tagAllume: ['#diag2p1'],
              couleur: [130, 130, 130],
              ep: 2,
              serie: 6
            })
          } else {
            stor.listeChange.push({ id: '#diag2p2', ep: 3, co: coMod })
          }
          if (okd3) {
            this.listedeselemedessine.push({
              type: 'segment',
              nom: leslettresz[0] + lalettre1,
              val: ['segment', 'diagonale', reld3, 'axe'],
              tag: '#cr1',
              tagAllume: ['#diag1p2'],
              couleur: [130, 130, 130],
              ep: 2,
              serie: 5
            })
          } else {
            stor.listeChange.push({ id: '#diag1p2', ep: 3, co: coMod })
          }
          if (okd4) {
            this.listedeselemedessine.push({
              type: 'segment',
              nom: leslettresz[1] + lalettre1,
              val: ['segment', 'diagonale', reld4, 'axe'],
              tag: '#cr4',
              tagAllume: ['#diag2p2'],
              couleur: [130, 130, 130],
              ep: 2,
              serie: 6
            })
          } else {
            stor.listeChange.push({ id: '#diag2p2', ep: 3, co: coMod })
          }
        }

        stor.LeNomDuTruc = lalettre2 + lalettre4 + lalettre3 + lalettre5
        A1 = j3pGetRandomInt(1, 360)
        A2 = 90
        A3 = j3pGetRandomInt(20, 30) / 10
        listeNom = [{ t: '#npD', nom: leslettresz[0] }, { t: '#npF', nom: leslettresz[1] }, {
          t: '#npC',
          nom: leslettresz[2]
        }, { t: '#npE', nom: leslettresz[3] }, { t: '#npCentre', nom: lalettre1 }]

        stor.LeNomDuTruc = lalettre2 + lalettre4 + lalettre3 + lalettre5
        break
      case 'CARRE':
        oks1 = oks2 = oks3 = oks4 = okd1 = okd2 = okd3 = okd4 = true
        rels1 = rels2 = rels3 = rels4 = reld1 = reld2 = reld3 = reld4 = 'piege'
        okpo1 = okpo2 = okpo3 = okpo4 = true
        relpo1 = relpo2 = relpo3 = relpo4 = 'piege'
        rela = ['piege', 'piege', 'piege', 'piege']
        oka = [true, true, true, true]
        if (stor.element === 'cote_oppose') {
          haz = j3pGetRandomInt(0, 3)
          if (haz === 0) {
            oks1 = false
            rels3 = 'cote_oppose'
          }
          if (haz === 1) {
            oks2 = false
            rels4 = 'cote_oppose'
          }
          if (haz === 2) {
            oks3 = false
            rels1 = 'cote_oppose'
          }
          if (haz === 3) {
            oks4 = false
            rels2 = 'cote_oppose'
          }
        }
        if (stor.element === 'cote_consecutif') {
          haz = j3pGetRandomInt(0, 3)
          if (haz === 0) {
            oks1 = false
            rels4 = 'cote_consecutif'
            rels2 = 'cote_consecutif'
          }
          if (haz === 1) {
            oks2 = false
            rels3 = 'cote_consecutif'
            rels1 = 'cote_consecutif'
          }
          if (haz === 2) {
            oks3 = false
            rels4 = 'cote_consecutif'
            rels2 = 'cote_consecutif'
          }
          if (haz === 3) {
            oks4 = false
            rels3 = 'cote_consecutif'
            rels1 = 'cote_consecutif'
          }
        }
        if (stor.element === 'cote_perpendiculaire') {
          haz = j3pGetRandomInt(0, 3)
          if (haz === 0) {
            oks1 = false
            rels4 = 'cote_perpendiculaire'
            rels2 = 'cote_perpendiculaire'
          }
          if (haz === 1) {
            oks2 = false
            rels1 = 'cote_perpendiculaire'
            rels3 = 'cote_perpendiculaire'
          }
          if (haz === 2) {
            oks3 = false
            rels4 = 'cote_perpendiculaire'
            rels2 = 'cote_perpendiculaire'
          }
          if (haz === 3) {
            oks4 = false
            rels1 = 'cote_perpendiculaire'
            rels3 = 'cote_perpendiculaire'
          }
        }
        if (stor.element === 'cote_parallele') {
          haz = j3pGetRandomInt(0, 3)
          if (haz === 0) {
            oks1 = false
            rels3 = 'cote_parallele'
          }
          if (haz === 1) {
            oks2 = false
            rels4 = 'cote_parallele'
          }
          if (haz === 2) {
            oks3 = false
            rels1 = 'cote_parallele'
          }
          if (haz === 3) {
            oks4 = false
            rels2 = 'cote_parallele'
          }
        }
        if (stor.element === 'angle_oppose') {
          haz = j3pGetRandomInt(0, 3)
          if (haz === 0) {
            oka[0] = false
            rela[2] = 'angle_oppose'
          }
          if (haz === 1) {
            oka[1] = false
            rela[3] = 'angle_oppose'
          }
          if (haz === 2) {
            oka[2] = false
            rela[0] = 'angle_oppose'
          }
          if (haz === 3) {
            oka[3] = false
            rela[1] = 'angle_oppose'
          }
        }
        if (stor.element === 'angle_consecutif') {
          haz = j3pGetRandomInt(0, 3)
          if (haz === 0) {
            oka[0] = false
            rela[1] = rela[3] = 'angle_consecutif'
          }
          if (haz === 1) {
            oka[1] = false
            rela[2] = rela[0] = 'angle_consecutif'
          }
          if (haz === 2) {
            oka[2] = false
            rela[1] = rela[3] = 'angle_consecutif'
          }
          if (haz === 3) {
            oka[3] = false
            rela[2] = rela[0] = 'angle_consecutif'
          }
        }
        if (stor.element === 'longueur_egale') {
          haz = j3pGetRandomInt(0, 5)
          // cote
          if (haz === 0) {
            oks1 = false
            rels3 = 'longueur_egale'
            rels4 = 'longueur_egale'
            rels2 = 'longueur_egale'
          }
          if (haz === 1) {
            oks2 = false
            rels4 = 'longueur_egale'
            rels1 = 'longueur_egale'
            rels3 = 'longueur_egale'
          }
          // demidiag

          if (haz === 2) {
            okd3 = false
            reld2 = 'longueur_egale'
            reld1 = 'longueur_egale'
            reld4 = 'longueur_egale'
          }
          if (haz === 3) {
            okd4 = false
            reld2 = 'longueur_egale'
            reld3 = 'longueur_egale'
            reld1 = 'longueur_egale'
          }
          // diag
          if (haz === 4) {
            okd1 = false
            okd3 = false
            reld2 = 'longueur_egale'
            reld4 = 'longueur_egale'
          }
          if (haz === 5) {
            okd2 = false
            okd4 = false
            reld3 = 'longueur_egale'
            reld1 = 'longueur_egale'
          }
        }
        if (stor.element === 'angle_egal') {
          haz = j3pGetRandomInt(0, 3)
          if (haz === 0) {
            oka[0] = false
            rela[2] = 'angle_egal'
            rela[3] = 'angle_egal'
            rela[1] = 'angle_egal'
          }
          if (haz === 1) {
            oka[1] = false
            rela[2] = 'angle_egal'
            rela[3] = 'angle_egal'
            rela[0] = 'angle_egal'
          }
          if (haz === 2) {
            oka[2] = false
            rela[0] = 'angle_egal'
            rela[3] = 'angle_egal'
            rela[1] = 'angle_egal'
          }
          if (haz === 3) {
            oka[3] = false
            rela[2] = 'angle_egal'
            rela[0] = 'angle_egal'
            rela[1] = 'angle_egal'
          }
        }
        if (stor.element === 'sommet_consecutif') {
          haz = j3pGetRandomInt(0, 3)
          if (haz === 0) {
            okpo1 = false
            relpo2 = 'sommet_consecutif'
            relpo4 = 'sommet_consecutif'
          }
          if (haz === 1) {
            okpo2 = false
            relpo1 = 'sommet_consecutif'
            relpo3 = 'sommet_consecutif'
          }
          if (haz === 2) {
            okpo3 = false
            relpo4 = 'sommet_consecutif'
            relpo2 = 'sommet_consecutif'
          }
          if (haz === 3) {
            okpo4 = false
            relpo1 = 'sommet_consecutif'
            relpo3 = 'sommet_consecutif'
          }
        }
        if (stor.element === 'sommet_oppose') {
          haz = j3pGetRandomInt(0, 3)
          if (haz === 0) {
            okpo1 = false
            relpo3 = 'sommet_oppose'
          }
          if (haz === 1) {
            okpo2 = false
            relpo4 = 'sommet_oppose'
          }
          if (haz === 2) {
            okpo3 = false
            relpo1 = 'sommet_oppose'
          }
          if (haz === 3) {
            okpo4 = false
            relpo2 = 'sommet_oppose'
          }
        }
        stor.listZig = ['#cdd1', '#cdd2', '#cpE', '#cpF', '#cp1', '#cpC', '#cpM', '#cs1', '#cs2', '#cs3', '#cs4', '#ca1', '#ca2', '#ca3', '#ca4', '#cd1', '#cd2', '#cd3', '#cd4', '#cd5', '#cd6', '#cd7', '#cd8', '#cr1', '#cr2', '#cr3', '#cr4']

        this.listedeselemedessine = []
        nomspris = []

        // chope le centre
        lalettre1 = addMaj(nomspris)
        this.listedeselemedessine.push({
          type: 'point',
          nom: lalettre1,
          val: ['point', 'centre_symetrie'],
          tag: '#cpM',
          tagAllume: ['#pM', '#sM'],
          couleur: [255, 255, 255],
          ep: false
        })

        // les 4 sommets
        lalettre2 = addMaj(nomspris)
        if (okpo1) {
          this.listedeselemedessine.push({
            type: 'point',
            nom: lalettre2,
            val: ['point', 'sommet', relpo1],
            tag: '#cp1',
            tagAllume: ['#p1', '#s1'],
            couleur: [255, 255, 255],
            ep: false
          })
        } else {
          stor.listeChange.push({ id: '#s1', co: coMod })
        }
        lalettre3 = addMaj(nomspris)
        if (okpo2) {
          this.listedeselemedessine.push({
            type: 'point',
            nom: lalettre3,
            val: ['point', 'sommet', relpo2],
            tag: '#cpF',
            tagAllume: ['#pF', '#sF'],
            couleur: [255, 255, 255],
            ep: false
          })
        } else {
          stor.listeChange.push({ id: '#sF', co: coMod })
        }
        lalettre4 = addMaj(nomspris)
        if (okpo3) {
          this.listedeselemedessine.push({
            type: 'point',
            nom: lalettre4,
            val: ['point', 'sommet', relpo3],
            tag: '#cpC',
            tagAllume: ['#pC', '#sC'],
            couleur: [255, 255, 255],
            ep: false
          })
        } else {
          stor.listeChange.push({ id: '#sC', co: coMod })
        }
        lalettre5 = addMaj(nomspris)
        if (okpo4) {
          this.listedeselemedessine.push({
            type: 'point',
            nom: lalettre5,
            val: ['point', 'sommet', relpo4],
            tag: '#cpE',
            tagAllume: ['#pE', '#sE'],
            couleur: [255, 255, 255],
            ep: false
          })
        } else {
          stor.listeChange.push({ id: '#sE', co: coMod })
        }

        leslettresz = [lalettre2, lalettre4, lalettre3, lalettre5]
        // les angles
        for (i = 0; i < 4; i++) {
          if (oka[i]) {
            this.listedeselemedessine.push({
              type: 'angle',
              nom: 'angle(' + leslettresz[i] + leslettresz[(i + 1) % 4] + leslettresz[(i + 2) % 4] + ')',
              val: ['angle_droit', 'angle', rela[i]],
              tag: '#ca' + (i % 4 + 1),
              tagAllume: ['#a' + (i % 4 + 1), '#as' + (i % 4 + 1)],
              couleur: [0, 0, 0],
              ep: false
            })
          } else {
            stor.listeChange.push(
              {
                id: '#as' + (i % 4 + 1),
                co: coMod
              }
            )
          }
        }

        // 2 axes
        this.listedeselemedessine.push({
          type: 'droite',
          nom: 'ùk',
          val: ['droite', 'axe'],
          tag: '#cd4',
          tagAllume: ['#axe1'],
          couleur: [130, 130, 130],
          ep: 2
        })
        this.listedeselemedessine.push({
          type: 'droite',
          nom: 'ùl',
          val: ['droite', 'axe'],
          tag: '#cd7',
          tagAllume: ['#axe2'],
          couleur: [130, 130, 130],
          ep: 2
        })

        // les 4 dtes derrieres les cotes
        this.listedeselemedessine.push({
          type: 'droite',
          nom: leslettresz[0] + leslettresz[1],
          val: ['droite'],
          tag: '#cd1',
          tagAllume: ['#dd1'],
          couleur: [130, 130, 130],
          ep: 2,
          serie: 1,
          contser: true
        })
        this.listedeselemedessine.push({
          type: 'droite',
          nom: leslettresz[1] + leslettresz[2],
          val: ['droite'],
          tag: '#cd5',
          tagAllume: ['#dd2'],
          couleur: [130, 130, 130],
          ep: 2,
          serie: 2,
          contser: true
        })
        this.listedeselemedessine.push({
          type: 'droite',
          nom: leslettresz[2] + leslettresz[3],
          val: ['droite'],
          tag: '#cd8',
          tagAllume: ['#dd3'],
          couleur: [130, 130, 130],
          ep: 2,
          serie: 3,
          contser: true
        })
        this.listedeselemedessine.push({
          type: 'droite',
          nom: leslettresz[3] + leslettresz[0],
          val: ['droite'],
          tag: '#cd3',
          tagAllume: ['#dd4'],
          couleur: [130, 130, 130],
          ep: 2,
          serie: 4,
          contser: true
        })
        // les 4 cotes
        if (oks1) {
          this.listedeselemedessine.push({
            type: 'segment',
            nom: leslettresz[0] + leslettresz[1],
            val: ['segment', 'cote', rels1],
            tag: '#cs1',
            tagAllume: ['#seg1'],
            couleur: [0, 0, 0],
            ep: 3,
            serie: 1
          })
        } else {
          stor.listeChange.push({ id: '#seg1', ep: 3, co: coMod })
        }
        if (oks2) {
          this.listedeselemedessine.push({
            type: 'segment',
            nom: leslettresz[1] + leslettresz[2],
            val: ['segment', 'cote', rels2],
            tag: '#cs4',
            tagAllume: ['#seg2'],
            couleur: [0, 0, 0],
            ep: 3,
            serie: 2
          })
        } else {
          stor.listeChange.push({ id: '#seg2', ep: 3, co: coMod })
        }
        if (oks3) {
          this.listedeselemedessine.push({
            type: 'segment',
            nom: leslettresz[2] + leslettresz[3],
            val: ['segment', 'cote', rels3],
            tag: '#cs3',
            tagAllume: ['#seg3'],
            couleur: [0, 0, 0],
            ep: 3,
            serie: 3
          })
        } else {
          stor.listeChange.push({ id: '#seg3', ep: 3, co: coMod })
        }
        if (oks4) {
          this.listedeselemedessine.push({
            type: 'segment',
            nom: leslettresz[3] + leslettresz[0],
            val: ['segment', 'cote', rels4],
            tag: '#cs2',
            tagAllume: ['#seg4'],
            couleur: [0, 0, 0],
            ep: 3,
            serie: 4
          })
        } else {
          stor.listeChange.push({ id: '#seg4', ep: 3, co: coMod })
        }

        // les  dtes derrieres les diag
        this.listedeselemedessine.push({
          type: 'droite',
          nom: leslettresz[0] + leslettresz[2],
          val: ['droite', reldrd1, 'axe'],
          tag: '#cd2',
          tagAllume: ['#ddiag1'],
          couleur: [130, 130, 130],
          ep: 2,
          serie: 6,
          contser: true
        })
        this.listedeselemedessine.push({
          type: 'droite',
          nom: leslettresz[1] + leslettresz[3],
          val: ['droite', reldrd2, 'axe'],
          tag: '#cd6',
          tagAllume: ['#ddiag2'],
          couleur: [130, 130, 130],
          ep: 2,
          serie: 5,
          contser: true
        })

        if ((stor.element === 'diagonale') || ((stor.element === 'longueur_egale') && (haz > 3))) {
          stor.listCache.push('#diag1p1', '#diag2p1', '#diag1p2', '#diag2p2', '#cr1', '#cr2', '#cr3', '#cr4')
          if (okd1) {
            this.listedeselemedessine.push({
              type: 'segment',
              nom: leslettresz[0] + leslettresz[2],
              val: ['segment', 'diagonale', 'longueur_egale'],
              tag: '#cdd1',
              tagAllume: ['#diag1'],
              couleur: [130, 130, 130],
              ep: 2,
              serie: 5
            })
          } else {
            stor.listeChange.push({ id: '#diag1', ep: 3, co: coMod })
          }
          if (okd2) {
            this.listedeselemedessine.push({
              type: 'segment',
              nom: leslettresz[1] + leslettresz[3],
              val: ['segment', 'diagonale', 'longueur_egale'],
              tag: '#cdd2',
              tagAllume: ['#diag2'],
              couleur: [130, 130, 130],
              ep: 2,
              serie: 6
            })
          } else {
            stor.listeChange.push({ id: '#diag2', ep: 3, co: coMod })
          }
        } else {
          // les diages
          stor.listCache.push('#diag1', '#diag2', '#cdd1', '#cdd2')
          if (okd1) {
            this.listedeselemedessine.push({
              type: 'segment',
              nom: lalettre1 + leslettresz[2],
              val: ['segment', 'diagonale', reld1, 'axe'],
              tag: '#cr1',
              tagAllume: ['#diag1p1'],
              couleur: [130, 130, 130],
              ep: 2,
              serie: 5
            })
          } else {
            stor.listeChange.push({ id: '#diag1p1', ep: 3, co: coMod })
          }
          if (okd2) {
            this.listedeselemedessine.push({
              type: 'segment',
              nom: lalettre1 + leslettresz[3],
              val: ['segment', 'diagonale', reld2, 'axe'],
              tag: '#cr3',
              tagAllume: ['#diag2p1'],
              couleur: [130, 130, 130],
              ep: 2,
              serie: 6
            })
          } else {
            stor.listeChange.push({ id: '#diag2p1', ep: 3, co: coMod })
          }
          if (okd3) {
            this.listedeselemedessine.push({
              type: 'segment',
              nom: leslettresz[0] + lalettre1,
              val: ['segment', 'diagonale', reld3, 'axe'],
              tag: '#cr2',
              tagAllume: ['#diag1p2'],
              couleur: [130, 130, 130],
              ep: 2,
              serie: 5
            })
          } else {
            stor.listeChange.push({ id: '#diag1p2', ep: 3, co: coMod })
          }
          if (okd4) {
            this.listedeselemedessine.push({
              type: 'segment',
              nom: leslettresz[1] + lalettre1,
              val: ['segment', 'diagonale', reld4, 'axe'],
              tag: '#cr4',
              tagAllume: ['#diag2p2'],
              couleur: [130, 130, 130],
              ep: 2,
              serie: 6
            })
          } else {
            stor.listeChange.push({ id: '#diag2p2', ep: 3, co: coMod })
          }
        }

        stor.LeNomDuTruc = lalettre2 + lalettre4 + lalettre3 + lalettre5
        A1 = j3pGetRandomInt(1, 260)
        A2 = 5.09
        A3 = A2
        listeNom = [{ t: '#np1', nom: leslettresz[0] }, { t: '#npF', nom: leslettresz[1] }, {
          t: '#npC',
          nom: leslettresz[2]
        }, { t: '#npE', nom: leslettresz[3] }, { t: '#npM', nom: lalettre1 }]

        break
      case 'RECTANGLE':
        oks1 = oks2 = oks3 = oks4 = okd1 = okd2 = okd3 = okd4 = true
        rels1 = rels2 = rels3 = rels4 = reld1 = reld2 = reld3 = reld4 = 'piege'
        okpo1 = okpo2 = okpo3 = okpo4 = true
        relpo1 = relpo2 = relpo3 = relpo4 = 'piege'
        rela = ['piege', 'piege', 'piege', 'piege']
        oka = [true, true, true, true]
        if (stor.element === 'cote_oppose') {
          haz = j3pGetRandomInt(0, 3)
          if (haz === 0) {
            oks1 = false
            rels3 = 'cote_oppose'
          }
          if (haz === 1) {
            oks2 = false
            rels4 = 'cote_oppose'
          }
          if (haz === 2) {
            oks3 = false
            rels1 = 'cote_oppose'
          }
          if (haz === 3) {
            oks4 = false
            rels2 = 'cote_oppose'
          }
        }
        if (stor.element === 'cote_consecutif') {
          haz = j3pGetRandomInt(0, 3)
          if (haz === 0) {
            oks1 = false
            rels4 = 'cote_consecutif'
            rels2 = 'cote_consecutif'
          }
          if (haz === 1) {
            oks2 = false
            rels3 = 'cote_consecutif'
            rels1 = 'cote_consecutif'
          }
          if (haz === 2) {
            oks3 = false
            rels4 = 'cote_consecutif'
            rels2 = 'cote_consecutif'
          }
          if (haz === 3) {
            oks4 = false
            rels3 = 'cote_consecutif'
            rels1 = 'cote_consecutif'
          }
        }
        if (stor.element === 'cote_perpendiculaire') {
          haz = j3pGetRandomInt(0, 3)
          if (haz === 0) {
            oks1 = false
            rels4 = 'cote_perpendiculaire'
            rels2 = 'cote_perpendiculaire'
          }
          if (haz === 1) {
            oks2 = false
            rels1 = 'cote_perpendiculaire'
            rels3 = 'cote_perpendiculaire'
          }
          if (haz === 2) {
            oks3 = false
            rels4 = 'cote_perpendiculaire'
            rels2 = 'cote_perpendiculaire'
          }
          if (haz === 3) {
            oks4 = false
            rels1 = 'cote_perpendiculaire'
            rels3 = 'cote_perpendiculaire'
          }
        }
        if (stor.element === 'cote_parallele') {
          haz = j3pGetRandomInt(0, 3)
          if (haz === 0) {
            oks1 = false
            rels3 = 'cote_parallele'
          }
          if (haz === 1) {
            oks2 = false
            rels4 = 'cote_parallele'
          }
          if (haz === 2) {
            oks3 = false
            rels1 = 'cote_parallele'
          }
          if (haz === 3) {
            oks4 = false
            rels2 = 'cote_parallele'
          }
        }
        if (stor.element === 'angle_oppose') {
          haz = j3pGetRandomInt(0, 3)
          if (haz === 0) {
            oka[0] = false
            rela[2] = 'angle_oppose'
          }
          if (haz === 1) {
            oka[1] = false
            rela[3] = 'angle_oppose'
          }
          if (haz === 2) {
            oka[2] = false
            rela[0] = 'angle_oppose'
          }
          if (haz === 3) {
            oka[3] = false
            rela[1] = 'angle_oppose'
          }
        }
        if (stor.element === 'angle_consecutif') {
          haz = j3pGetRandomInt(0, 3)
          if (haz === 0) {
            oka[0] = false
            rela[1] = rela[3] = 'angle_consecutif'
          }
          if (haz === 1) {
            oka[1] = false
            rela[2] = rela[0] = 'angle_consecutif'
          }
          if (haz === 2) {
            oka[2] = false
            rela[1] = rela[3] = 'angle_consecutif'
          }
          if (haz === 3) {
            oka[3] = false
            rela[2] = rela[0] = 'angle_consecutif'
          }
        }
        if (stor.element === 'longueur_egale') {
          haz = j3pGetRandomInt(0, 5)
          // cote
          if (haz === 0) {
            oks1 = false
            rels3 = 'longueur_egale'
          }
          if (haz === 1) {
            oks2 = false
            rels4 = 'longueur_egale'
          }
          // demidiag
          if (haz === 2) {
            okd1 = false
            reld2 = 'longueur_egale'
            reld3 = 'longueur_egale'
            reld4 = 'longueur_egale'
          }
          if (haz === 3) {
            okd3 = false
            reld1 = 'longueur_egale'
            reld2 = 'longueur_egale'
            reld4 = 'longueur_egale'
          }
          // diag
          if (haz === 4) {
            okd1 = false
            okd3 = false
            reld2 = 'longueur_egale'
            reld4 = 'longueur_egale'
          }
          if (haz === 5) {
            okd2 = false
            okd4 = false
            reld3 = 'longueur_egale'
            reld1 = 'longueur_egale'
          }
        }
        if (stor.element === 'angle_egal') {
          haz = j3pGetRandomInt(0, 3)
          if (haz === 0) {
            oka[0] = false
            rela[2] = 'angle_egal'
            rela[3] = 'angle_egal'
            rela[1] = 'angle_egal'
          }
          if (haz === 1) {
            oka[1] = false
            rela[2] = 'angle_egal'
            rela[3] = 'angle_egal'
            rela[0] = 'angle_egal'
          }
          if (haz === 2) {
            oka[2] = false
            rela[0] = 'angle_egal'
            rela[3] = 'angle_egal'
            rela[1] = 'angle_egal'
          }
          if (haz === 3) {
            oka[3] = false
            rela[2] = 'angle_egal'
            rela[0] = 'angle_egal'
            rela[1] = 'angle_egal'
          }
        }
        if (stor.element === 'sommet_consecutif') {
          haz = j3pGetRandomInt(0, 3)
          if (haz === 0) {
            okpo1 = false
            eppo1 = 5
            relpo4 = 'sommet_consecutif'
            relpo2 = 'sommet_consecutif'
          }
          if (haz === 1) {
            okpo2 = false
            eppo2 = 5
            relpo1 = 'sommet_consecutif'
            relpo3 = 'sommet_consecutif'
          }
          if (haz === 2) {
            okpo3 = false
            eppo3 = 5
            relpo4 = 'sommet_consecutif'
            relpo2 = 'sommet_consecutif'
          }
          if (haz === 3) {
            okpo4 = false
            eppo4 = 5
            relpo1 = 'sommet_consecutif'
            relpo3 = 'sommet_consecutif'
          }
        }
        if (stor.element === 'sommet_oppose') {
          haz = j3pGetRandomInt(0, 3)
          if (haz === 0) {
            okpo1 = false
            eppo1 = 5
            relpo3 = 'sommet_oppose'
          }
          if (haz === 1) {
            okpo2 = false
            eppo2 = 5
            relpo4 = 'sommet_oppose'
          }
          if (haz === 2) {
            okpo3 = false
            eppo3 = 5
            relpo1 = 'sommet_oppose'
          }
          if (haz === 3) {
            okpo4 = false
            eppo4 = 5
            relpo2 = 'sommet_oppose'
          }
        }
        stor.listZig = ['#cdd1', '#cdd2', '#cpE', '#cpF', '#cp1', '#cpC', '#cpM', '#cs1', '#cs2', '#cs3', '#cs4', '#ca1', '#ca2', '#ca3', '#ca4', '#cd1', '#cd2', '#cd3', '#cd4', '#cd5', '#cd6', '#cd7', '#cd8', '#cr1', '#cr2', '#cr3', '#cr4']

        this.listedeselemedessine = []
        nomspris = []

        // chope le centre
        lalettre1 = addMaj(nomspris)
        this.listedeselemedessine.push({
          type: 'point',
          nom: lalettre1,
          val: ['point', 'centre_symetrie'],
          tag: '#cpM',
          tagAllume: ['#pM', '#sM'],
          couleur: [255, 255, 255],
          ep: false
        })

        // les 4 sommets
        lalettre2 = addMaj(nomspris)
        if (okpo1) {
          this.listedeselemedessine.push({
            type: 'point',
            nom: lalettre2,
            val: ['point', 'sommet', relpo1],
            tag: '#cp1',
            tagAllume: ['#p1', '#s1'],
            couleur: [255, 255, 255],
            ep: false
          })
        } else {
          stor.listeChange.push({ id: '#s1', co: coMod })
        }
        lalettre3 = addMaj(nomspris)
        if (okpo2) {
          this.listedeselemedessine.push({
            type: 'point',
            nom: lalettre3,
            val: ['point', 'sommet', relpo2],
            tag: '#cpF',
            tagAllume: ['#pF', '#sF'],
            couleur: [255, 255, 255],
            ep: false
          })
        } else {
          stor.listeChange.push({ id: '#sF', co: coMod })
        }
        lalettre4 = addMaj(nomspris)
        if (okpo3) {
          this.listedeselemedessine.push({
            type: 'point',
            nom: lalettre4,
            val: ['point', 'sommet', relpo3],
            tag: '#cpC',
            tagAllume: ['#pC', '#sC'],
            couleur: [255, 255, 255],
            ep: false
          })
        } else {
          stor.listeChange.push({ id: '#sC', co: coMod })
        }
        lalettre5 = addMaj(nomspris)
        if (okpo4) {
          this.listedeselemedessine.push({
            type: 'point',
            nom: lalettre5,
            val: ['point', 'sommet', relpo4],
            tag: '#cpE',
            tagAllume: ['#pE', '#sE'],
            couleur: [255, 255, 255],
            ep: false
          })
        } else {
          stor.listeChange.push({ id: '#sE', co: coMod })
        }

        leslettresz = [lalettre2, lalettre4, lalettre3, lalettre5]
        // les angles
        for (i = 0; i < 4; i++) {
          if (oka[i]) {
            this.listedeselemedessine.push({
              type: 'angle',
              nom: 'angle(' + leslettresz[i] + leslettresz[(i + 1) % 4] + leslettresz[(i + 2) % 4] + ')',
              val: ['angle_droit', 'angle', rela[i]],
              tag: '#ca' + (i % 4 + 1),
              tagAllume: ['#a' + (i % 4 + 1), '#as' + (i % 4 + 1)],
              couleur: [0, 0, 0],
              ep: false
            })
          } else {
            stor.listeChange.push(
              {
                id: '#as' + (i % 4 + 1),
                co: coMod
              }
            )
          }
        }

        // 2 axes
        this.listedeselemedessine.push({
          type: 'droite',
          nom: 'ùk',
          val: ['droite', 'axe'],
          tag: '#cd5',
          tagAllume: ['#axe1'],
          couleur: [130, 130, 130],
          ep: 2
        })
        this.listedeselemedessine.push({
          type: 'droite',
          nom: 'ùl',
          val: ['droite', 'axe'],
          tag: '#cd6',
          tagAllume: ['#axe2'],
          couleur: [130, 130, 130],
          ep: 2
        })

        // les 4 dtes derrieres les cotes
        this.listedeselemedessine.push({
          type: 'droite',
          nom: '(' + leslettresz[0] + leslettresz[1],
          val: ['droite'],
          tag: '#cd3',
          tagAllume: ['#dd1'],
          couleur: [130, 130, 130],
          ep: 2,
          serie: 1,
          contser: true
        })
        this.listedeselemedessine.push({
          type: 'droite',
          nom: '(' + leslettresz[1] + leslettresz[2],
          val: ['droite'],
          tag: '#cd1',
          tagAllume: ['#dd2'],
          couleur: [130, 130, 130],
          ep: 2,
          serie: 2,
          contser: true
        })
        this.listedeselemedessine.push({
          type: 'droite',
          nom: '(' + leslettresz[2] + leslettresz[3],
          val: ['droite'],
          tag: '#cd4',
          tagAllume: ['#dd3'],
          couleur: [130, 130, 130],
          ep: 2,
          serie: 3,
          contser: true
        })
        this.listedeselemedessine.push({
          type: 'droite',
          nom: '(' + leslettresz[3] + leslettresz[0],
          val: ['droite'],
          tag: '#cd2',
          tagAllume: ['#dd4'],
          couleur: [130, 130, 130],
          ep: 2,
          serie: 4,
          contser: true
        })
        // les 4 cotes
        if (oks1) {
          this.listedeselemedessine.push({
            type: 'segment',
            nom: leslettresz[0] + leslettresz[1],
            val: ['segment', 'cote', rels1],
            tag: '#cs4',
            tagAllume: ['#seg1'],
            couleur: [0, 0, 0],
            ep: 3,
            serie: 1
          })
        } else {
          stor.listeChange.push({ id: '#seg1', ep: 3, co: coMod })
        }
        if (oks2) {
          this.listedeselemedessine.push({
            type: 'segment',
            nom: leslettresz[1] + leslettresz[2],
            val: ['segment', 'cote', rels2],
            tag: '#cs1',
            tagAllume: ['#seg2'],
            couleur: [0, 0, 0],
            ep: 3,
            serie: 2
          })
        } else {
          stor.listeChange.push({ id: '#seg2', ep: 3, co: coMod })
        }
        if (oks3) {
          this.listedeselemedessine.push({
            type: 'segment',
            nom: leslettresz[2] + leslettresz[3],
            val: ['segment', 'cote', rels3],
            tag: '#cs2',
            tagAllume: ['#seg3'],
            couleur: [0, 0, 0],
            ep: 3,
            serie: 3
          })
        } else {
          stor.listeChange.push({ id: '#seg3', ep: 3, co: coMod })
        }
        if (oks4) {
          this.listedeselemedessine.push({
            type: 'segment',
            nom: leslettresz[3] + leslettresz[0],
            val: ['segment', 'cote', rels4],
            tag: '#cs3',
            tagAllume: ['#seg4'],
            couleur: [0, 0, 0],
            ep: 3,
            serie: 4
          })
        } else {
          stor.listeChange.push({ id: '#seg4', ep: 3, co: coMod })
        }

        // les  dtes derrieres les diag
        this.listedeselemedessine.push({
          type: 'droite',
          nom: leslettresz[0] + leslettresz[2],
          val: ['droite', reldrd1],
          tag: '#cd7',
          tagAllume: ['#ddiag1'],
          couleur: [130, 130, 130],
          ep: 2,
          serie: 6,
          contser: true
        })
        this.listedeselemedessine.push({
          type: 'droite',
          nom: leslettresz[1] + leslettresz[3],
          val: ['droite', reldrd2],
          tag: '#cd8',
          tagAllume: ['#ddiag2'],
          couleur: [130, 130, 130],
          ep: 2,
          serie: 5,
          contser: true
        })

        if ((stor.element === 'diagonale') || ((stor.element === 'longueur_egale') && (haz > 3))) {
          stor.listCache.push('#diag1p1', '#diag2p1', '#diag1p2', '#diag2p2', '#cr1', '#cr2', '#cr3', '#cr4')
          if (okd1) {
            this.listedeselemedessine.push({
              type: 'segment',
              nom: leslettresz[0] + leslettresz[2],
              val: ['segment', 'diagonale', 'longueur_egale'],
              tag: '#cdd1',
              tagAllume: ['#diag1'],
              couleur: [130, 130, 130],
              ep: 2,
              serie: 5
            })
          } else {
            stor.listeChange.push({ id: '#diag1', ep: 3, co: coMod })
          }
          if (okd2) {
            this.listedeselemedessine.push({
              type: 'segment',
              nom: leslettresz[1] + leslettresz[3],
              val: ['segment', 'diagonale', 'longueur_egale'],
              tag: '#cdd2',
              tagAllume: ['#diag2'],
              couleur: [130, 130, 130],
              ep: 2,
              serie: 6
            })
          } else {
            stor.listeChange.push({ id: '#diag2', ep: 3, co: coMod })
          }
        } else {
          // les diages
          stor.listCache.push('#diag1', '#diag2', '#cdd1', '#cdd2')
          if (okd1) {
            this.listedeselemedessine.push({
              type: 'segment',
              nom: lalettre1 + leslettresz[2],
              val: ['segment', 'diagonale', reld1],
              tag: '#cr4',
              tagAllume: ['#diag1p1'],
              couleur: [130, 130, 130],
              ep: 2,
              serie: 5
            })
          } else {
            stor.listeChange.push({ id: '#diag1p1', ep: 3, co: coMod })
          }
          if (okd2) {
            this.listedeselemedessine.push({
              type: 'segment',
              nom: lalettre1 + leslettresz[3],
              val: ['segment', 'diagonale', reld2],
              tag: '#cr2',
              tagAllume: ['#diag2p1'],
              couleur: [130, 130, 130],
              ep: 2,
              serie: 6
            })
          } else {
            stor.listeChange.push({ id: '#diag2p2', ep: 3, co: coMod })
          }
          if (okd3) {
            this.listedeselemedessine.push({
              type: 'segment',
              nom: leslettresz[0] + lalettre1,
              val: ['segment', 'diagonale', reld3],
              tag: '#cr3',
              tagAllume: ['#diag1p2'],
              couleur: [130, 130, 130],
              ep: 2,
              serie: 5
            })
          } else {
            stor.listeChange.push({ id: '#diag1p2', ep: 3, co: coMod })
          }
          if (okd4) {
            this.listedeselemedessine.push({
              type: 'segment',
              nom: leslettresz[1] + lalettre1,
              val: ['segment', 'diagonale', reld4],
              tag: '#cr1',
              tagAllume: ['#diag2p2'],
              couleur: [130, 130, 130],
              ep: 2,
              serie: 6
            })
          } else {
            stor.listeChange.push({ id: '#diag2p2', ep: 3, co: coMod })
          }
        }

        stor.LeNomDuTruc = lalettre2 + lalettre4 + lalettre3 + lalettre5
        A1 = j3pGetRandomInt(1, 260)
        A2 = j3pGetRandomInt(20, 40) / 10
        A3 = A2
        listeNom = [{ t: '#np1', nom: leslettresz[0] }, { t: '#npF', nom: leslettresz[1] }, {
          t: '#npC',
          nom: leslettresz[2]
        }, { t: '#npE', nom: leslettresz[3] }, { t: '#npM', nom: lalettre1 }]

        break
      case 'CERCLE': // chope le centre
        A2 = j3pGetRandomInt(40, 130)
        A1 = j3pGetRandomInt(0, 359)
        stor.listZig = ['#cs1', '#cdd1', '#cpE1', '#cpD', '#cpCentre', '#cpE2', '#cd1', '#cd2', '#cd3', '#cd4', '#cr1', '#cr2', '#cr3']

        lalettre1 = addMaj(nomspris)
        this.listedeselemedessine.push({
          type: 'point',
          nom: lalettre1,
          val: ['centre_symetrie', 'centre'],
          tag: '#cpCentre',
          tagAllume: ['#pcentre', '#sCentre'],
          couleur: [255, 255, 255],
          ep: false
        })

        lalettre2 = addMaj(nomspris)
        lalettre3 = addMaj(nomspris)
        lalettre4 = addMaj(nomspris)
        lalettre5 = addMaj(nomspris)

        // 4 points
        this.listedeselemedessine.push({
          type: 'point',
          nom: lalettre2,
          val: ['point'],
          tag: '#cpE1',
          tagAllume: ['#pE1', '#sE1'],
          couleur: [255, 255, 255],
          ep: false
        })
        this.listedeselemedessine.push({
          type: 'point',
          nom: lalettre3,
          val: ['point'],
          tag: '#cpE2',
          tagAllume: ['#pE2', '#sE2'],
          couleur: [255, 255, 255],
          ep: false
        })
        this.listedeselemedessine.push({
          type: 'point',
          nom: lalettre5,
          val: ['point'],
          tag: '#cpD',
          tagAllume: ['#pD', '#sD'],
          couleur: [255, 255, 255],
          ep: false
        })

        // 3 axes et un piege
        this.listedeselemedessine.push({
          type: 'droite',
          nom: lalettre3 + lalettre4,
          val: ['droite', 'axe'],
          tag: '#cd4',
          tagAllume: ['#d1'],
          couleur: [130, 130, 130],
          ep: 2,
          serie: 1,
          contser: true
        })
        this.listedeselemedessine.push({
          type: 'droite',
          nom: lalettre1 + lalettre2,
          val: ['droite', 'axe'],
          tag: '#cd3',
          tagAllume: ['#d2'],
          couleur: [130, 130, 130],
          ep: 2,
          serie: 2,
          contser: true
        })
        this.listedeselemedessine.push({
          type: 'droite',
          nom: lalettre1 + lalettre5,
          val: ['droite', 'axe'],
          tag: '#cd2',
          tagAllume: ['#axe'],
          couleur: [130, 130, 130],
          ep: 2
        })
        this.listedeselemedessine.push({
          type: 'droite',
          nom: lalettre2 + lalettre3,
          val: ['droite'],
          tag: '#cd1',
          tagAllume: ['#dCo'],
          couleur: [130, 130, 130],
          ep: 2,
          serie: 3,
          contser: true
        })

        this.listedeselemedessine.push({
          type: 'segment',
          nom: lalettre2 + lalettre3,
          val: ['segment', 'corde'],
          tag: '#cs1',
          tagAllume: ['#sCorde'],
          couleur: [0, 0, 0],
          ep: 2,
          serie: 3
        })
        this.listedeselemedessine.push({
          type: 'segment',
          nom: lalettre2 + lalettre3,
          val: ['segment', 'rayon', 'longueur_egale', 'axe'],
          tag: '#cr1',
          tagAllume: ['#c1'],
          couleur: [0, 0, 0],
          ep: 2,
          serie: 1
        })

        // rayon corde diametre
        if ((stor.element === 'diametre') || (stor.element === 'corde')) {
          this.listedeselemedessine.push({
            type: 'segment',
            nom: lalettre3 + lalettre4,
            val: ['segment', 'corde', 'diametre'],
            tag: '#cdd1',
            tagAllume: ['#diam'],
            couleur: [0, 0, 0],
            ep: 2,
            serie: 2
          })
          stor.listCache = ['#c2', '#c3', '#cr2', '#cr3']
        } else {
          stor.listCache = ['#diam', '#cdd1']
          this.listedeselemedessine.push({
            type: 'segment',
            nom: lalettre3 + lalettre4,
            val: ['segment', 'rayon', 'longueur_egale', 'axe'],
            tag: '#cr3',
            tagAllume: ['#c2'],
            couleur: [0, 0, 0],
            ep: 2,
            serie: 2
          })
          this.listedeselemedessine.push({
            type: 'segment',
            nom: lalettre3 + lalettre4,
            val: ['segment', 'rayon', 'longueur_egale', 'axe'],
            tag: '#cr2',
            tagAllume: ['#c3'],
            couleur: [0, 0, 0],
            ep: 2,
            serie: 2
          })
        }
        listeNom = [{ t: '#npCentre', nom: lalettre1 }, { t: '#npE1', nom: lalettre2 }, { t: '#npE2', nom: lalettre3 }]
        break
      case 'ARC': {
        do {
          A2 = j3pGetRandomInt(100, 330)
        } while (A2 > 160 && A2 < 240)
        A1 = j3pGetRandomInt(0, 359)
        stor.listZig = ['#cpF', '#cpE1', '#cpP', '#cpCentre', '#cpE2', '#cd1', '#cd2', '#cd3', '#cd4', '#cd5']

        lalettre1 = addMaj(nomspris)
        const npCentre = (A2 > 180) ? '#npCentre' : '#npCentre2'
        this.listedeselemedessine.push({
          type: 'point',
          nom: lalettre1,
          val: ['point', 'centre'],
          tag: '#cpCentre',
          tagAllume: ['#pcentre', '#sCentre'],
          couleur: [255, 255, 255],
          ep: false
        })

        lalettre2 = addMaj(nomspris)
        lalettre3 = addMaj(nomspris)
        lalettre4 = addMaj(nomspris)
        // 4 points
        this.listedeselemedessine.push({
          type: 'point',
          nom: lalettre2,
          val: ['point', 'extremites'],
          tag: '#cpE1',
          tagAllume: ['#pE1', '#sE1'],
          couleur: [255, 255, 255],
          ep: false
        })
        this.listedeselemedessine.push({
          type: 'point',
          nom: lalettre3,
          val: ['point', 'extremites'],
          tag: '#cpE2',
          tagAllume: ['#pE2', '#sE2'],
          couleur: [255, 255, 255],
          ep: false
        })
        oks2 = '#piege'
        oks3 = '#cd4'
        if (A2 > 180) {
          oks1 = '#npP'
          this.listedeselemedessine.push({
            type: 'point',
            nom: lalettre4,
            val: ['point'],
            tag: '#cpP',
            tagAllume: ['#pP', '#sP'],
            couleur: [255, 255, 255],
            ep: false
          })
        } else {
          oks1 = '#npF'
          this.listedeselemedessine.push({
            type: 'point',
            nom: lalettre4,
            val: ['point'],
            tag: '#cpF',
            tagAllume: ['#pF', '#sF'],
            couleur: [255, 255, 255],
            ep: false
          })
          oks2 += '2'
          oks3 = '#cd2'
        }

        // 3 axes et un piege
        this.listedeselemedessine.push({
          type: 'droite',
          nom: lalettre1 + lalettre4,
          val: ['droite'],
          tag: '#cd3',
          tagAllume: ['#d1'],
          couleur: [130, 130, 130],
          ep: 2
        })
        this.listedeselemedessine.push({
          type: 'droite',
          nom: lalettre1 + lalettre2,
          val: ['droite'],
          tag: '#cd5',
          tagAllume: ['#d2'],
          couleur: [130, 130, 130],
          ep: 2
        })
        this.listedeselemedessine.push({
          type: 'droite',
          nom: lalettre1 + 'g',
          val: ['droite', 'axe'],
          tag: '#cd1',
          tagAllume: ['#axe'],
          couleur: [130, 130, 130],
          ep: 2
        })
        this.listedeselemedessine.push({
          type: 'droite',
          nom: lalettre1 + lalettre3,
          val: ['droite'],
          tag: oks3,
          tagAllume: [oks2],
          couleur: [130, 130, 130],
          ep: 2
        })

        listeNom = [{ t: npCentre, nom: lalettre1 }, { t: oks1, nom: lalettre4 }, {
          t: '#npE1',
          nom: lalettre2
        }, { t: '#npE2', nom: lalettre3 }]
        A3 = 0
      }
        break
      case 'SECTEUR': // chope le centre
        lalettre1 = addMaj(nomspris)
        this.listedeselemedessine.push({
          type: 'point',
          nom: lalettre1,
          val: ['point', 'centre'],
          tag: '#cpCentre',
          tagAllume: ['#pcentre', '#sCentre'],
          couleur: [255, 255, 255],
          ep: false
        })
        A1 = j3pGetRandomInt(0, 359)
        do {
          A2 = j3pGetRandomInt(100, 330)
        } while (A2 > 160 && A2 < 240)
        stor.listZig = ['#cpE1', '#cpE2', '#cpCentre', '#cpF', '#cpP', '#cs1', '#cs2', '#cs3', '#cs4', '#cs5', '#cd1', '#cd2', '#cd3', '#cd4', '#cd5', '#ca1']

        // A2 = 140
        lalettre2 = addMaj(nomspris)
        lalettre3 = addMaj(nomspris)
        lalettre4 = addMaj(nomspris)
        // 4 points

        this.listedeselemedessine.push({
          type: 'point',
          nom: lalettre2,
          val: ['point', 'extremites'],
          tag: '#cpE1',
          tagAllume: ['#pE1', '#sE1'],
          couleur: [255, 255, 255],
          ep: false
        })
        this.listedeselemedessine.push({
          type: 'point',
          nom: lalettre3,
          val: ['point', 'extremites'],
          tag: '#cpE2',
          tagAllume: ['#pE2', '#sE2'],
          couleur: [255, 255, 255],
          ep: false
        })
        oks2 = '#piege'
        if (A2 > 180) {
          oks1 = '#npP'
          this.listedeselemedessine.push({
            type: 'point',
            nom: lalettre4,
            val: ['point'],
            tag: '#cpP',
            tagAllume: ['#pP', '#sP'],
            couleur: [255, 255, 255],
            ep: false
          })
          this.listedeselemedessine.push({
            type: 'angle',
            nom: 'angle(' + lalettre1 + lalettre2 + lalettre4 + ')',
            val: ['angle'],
            tag: '#ca1',
            tagAllume: ['#as0', '#a0'],
            couleur: [0, 0, 0],
            ep: false,
            serieColle: 1
          })
          this.listedeselemedessine.push({
            type: 'angle',
            nom: 'angle(' + lalettre1 + lalettre2 + lalettre4 + ')',
            val: ['angle'],
            tag: '#ca1',
            tagAllume: ['#as1', '#a1'],
            couleur: [0, 0, 0],
            ep: false,
            serieColle: 1
          })
          oks3 = '#c3'
          stor.listCache.push('#a2', '#as2', '#c4')
          oks4 = '#cd1'
          oks5 = '#cs1'
        } else {
          oks1 = '#npF'
          this.listedeselemedessine.push({
            type: 'point',
            nom: lalettre4,
            val: ['point'],
            tag: '#cpF',
            tagAllume: ['#pF', '#sF'],
            couleur: [255, 255, 255],
            ep: false
          })
          this.listedeselemedessine.push({
            type: 'angle',
            nom: 'angle(' + lalettre1 + lalettre2 + lalettre4 + ')',
            val: ['angle'],
            tag: '#ca1',
            tagAllume: ['#as2', '#a2'],
            couleur: [0, 0, 0],
            ep: false
          })
          oks2 += '2'
          stor.listCache.push('#a0', '#a1', '#as0', '#as1', '#c3')
          oks3 = '#c4'
          oks4 = '#cd5'
          oks5 = '#cs4'
        }

        // 3 axes et un piege
        this.listedeselemedessine.push({
          type: 'droite',
          nom: lalettre1 + lalettre4,
          val: ['droite'],
          tag: '#cd4',
          tagAllume: ['#d1'],
          couleur: [130, 130, 130],
          ep: 2,
          serie: 1,
          contser: true
        })
        this.listedeselemedessine.push({
          type: 'droite',
          nom: lalettre1 + lalettre2,
          val: ['droite'],
          tag: '#cd3',
          tagAllume: ['#d2'],
          couleur: [130, 130, 130],
          ep: 2,
          serie: 2,
          contser: true
        })
        this.listedeselemedessine.push({
          type: 'droite',
          nom: lalettre1 + 'g',
          val: ['droite', 'axe'],
          tag: '#cd2',
          tagAllume: ['#axe'],
          couleur: [130, 130, 130],
          ep: 2
        })
        this.listedeselemedessine.push({
          type: 'droite',
          nom: 'ttttt' + lalettre1 + lalettre3,
          val: ['droite'],
          tag: oks4,
          tagAllume: [oks2],
          couleur: [130, 130, 130],
          ep: 2,
          serie: 3,
          contser: true
        })

        // deux cote
        this.listedeselemedessine.push({
          type: 'segment',
          nom: lalettre4 + lalettre1,
          val: ['rayon'],
          tag: '#cs3',
          tagAllume: ['#c1'],
          couleur: [0, 0, 0],
          ep: 3,
          serie: 1
        })
        this.listedeselemedessine.push({
          type: 'segment',
          nom: lalettre2 + lalettre1,
          val: ['rayon'],
          tag: '#cs2',
          tagAllume: ['#c2'],
          couleur: [0, 0, 0],
          ep: 3,
          serie: 2
        })
        this.listedeselemedessine.push({
          type: 'segment',
          nom: 'rt' + lalettre3 + lalettre1,
          val: ['rayon'],
          tag: oks5,
          tagAllume: [oks3],
          couleur: [0, 0, 0],
          ep: 2,
          serie: 3
        })

        listeNom = [{ t: '#npCentre1', nom: lalettre1 }, { t: oks1, nom: lalettre4 }, {
          t: '#npE1',
          nom: lalettre2
        }, { t: '#npE2', nom: lalettre3 }]
        A3 = 0

        break
      case 'ANGLE':
        A2 = j3pGetRandomInt(100, 179)
        A3 = 0
        A1 = j3pGetRandomInt(0, 359)

        stor.listZig = ['#cpE1', '#cpE2', '#cpCentre', '#cs1', '#cs2', '#cm1', '#cm2', '#ca1', '#cd1', '#cd2']

        this.listedeselemedessine = []
        lalettre1 = addMaj(nomspris)
        this.listedeselemedessine.push({
          type: 'point',
          nom: lalettre1,
          val: ['point', 'sommet'],
          tag: '#cpCentre',
          tagAllume: ['#pcentre', '#sCentre'],
          couleur: [255, 255, 255],
          ep: false
        })
        lalettre2 = addMaj(nomspris)
        lalettre3 = addMaj(nomspris)
        this.listedeselemedessine.push({
          type: 'point',
          nom: lalettre2,
          val: ['point', 'extremites'],
          tag: '#cpE1',
          tagAllume: ['#pE1', '#sE1'],
          couleur: [255, 255, 255],
          ep: false
        })
        this.listedeselemedessine.push({
          type: 'point',
          nom: lalettre3,
          val: ['point', 'extremites'],
          tag: '#cpE2',
          tagAllume: ['#pE2', '#sE2'],
          couleur: [255, 255, 255],
          ep: false
        })

        this.listedeselemedessine.push({
          type: 'angle',
          nom: 'angle(' + lalettre1 + lalettre2 + lalettre4 + ')',
          val: ['angle'],
          tag: '#ca1',
          tagAllume: ['#as2', '#a2'],
          couleur: [0, 0, 0],
          ep: false
        })

        this.listedeselemedessine.push({
          type: 'droite',
          nom: '(' + lalettre1 + lalettre4,
          val: ['droite', 'axe', 'bissectrice'],
          tag: '#cd1',
          tagAllume: ['#axe'],
          couleur: [130, 130, 130],
          ep: 2
        })
        this.listedeselemedessine.push({
          type: 'droite',
          nom: lalettre1 + lalettre2,
          val: ['droite'],
          tag: '#cd2',
          tagAllume: ['#piege2'],
          couleur: [130, 130, 130],
          ep: 2
        })

        oks2 = [0, 0, 0]
        oks3 = [0, 0, 0]
        if (j3pGetRandomInt(0, 1) === 0) {
          stor.listeChange.push({ id: '#dd1', co: [130, 130, 130], ep: 2 })
          oks2 = [130, 130, 130]
        }
        if (j3pGetRandomInt(0, 1) === 0) {
          stor.listeChange.push({ id: '#dd2', co: [130, 130, 130], ep: 2 })
          oks3 = [130, 130, 130]
        }
        this.listedeselemedessine.push({
          type: 'demidroite',
          nom: lalettre3 + lalettre1,
          val: ['demi-droite', 'cote'],
          tag: '#cm2',
          tagAllume: ['#dd2'],
          couleur: oks3,
          ep: 2,
          serie: 2,
          contser: true
        })
        this.listedeselemedessine.push({
          type: 'demidroite',
          nom: lalettre3 + lalettre1,
          val: ['demi-droite', 'cote'],
          tag: '#cm1',
          tagAllume: ['#dd1'],
          couleur: oks2,
          ep: 2,
          serie: 1,
          contser: true
        })
        this.listedeselemedessine.push({
          type: 'segment',
          nom: lalettre1,
          val: ['segment', 'cote'],
          tag: '#cs1',
          tagAllume: ['#c1'],
          couleur: [0, 0, 0],
          ep: 2,
          serie: 1
        })
        this.listedeselemedessine.push({
          type: 'segment',
          nom: lalettre2 + lalettre1,
          val: ['segment', 'cote'],
          tag: '#cs2',
          tagAllume: ['#c2'],
          couleur: [0, 0, 0],
          ep: 3,
          serie: 2
        })

        listeNom = [{ t: '#npCentre', nom: lalettre1 }, { t: '#npE1', nom: lalettre2 }, { t: '#npE2', nom: lalettre3 }]

        break
      case 'TRIANGLE': // det les 4 points
        reld3 = reld4 = rel1 = rel2 = rel3 = rel4 = rel5 = rel7 = point0 = point1 = point2 = rela0 = rela1 = rela2 = 'piege'
        oka0 = oka1 = oka2 = true
        okp0 = okp1 = okp2 = true
        okd1 = okd5 = true
        oks4 = oks2 = oks3 = true
        stor.listZig = ['#cpA', '#cpB', '#cpC', '#cs1', '#cs2', '#cs3', '#cd1', '#cd2', '#cd3', '#cd4', '#cd5', '#cd6', '#ca1', '#ca2', '#ca3']

        if (stor.element === 'cote_oppose') {
          haz = j3pGetRandomInt(0, 2)
          if (haz === 0) {
            okp0 = false
            rel2 = 'cote_oppose'
          }
          if (haz === 1) {
            okp1 = false
            rel4 = 'cote_oppose'
          }
          if (haz === 2) {
            okp2 = false
            rel3 = 'cote_oppose'
          }
        }
        if (stor.element === 'hauteur_relative') {
          haz = j3pGetRandomInt(0, 1)
          if (haz === 0) {
            oks3 = false
            rel1 = 'hauteur_relative'
          }
          if (haz === 1) {
            oks2 = false
            rel5 = 'hauteur_relative'
          }
        }
        if (stor.element === 'base_relative') {
          haz = j3pGetRandomInt(0, 1)
          if (haz === 0) {
            okd1 = false
            rel3 = 'base_relative'
          }
          if (haz === 1) {
            okd5 = false
            rel2 = 'base_relative'
          }
        }
        if (stor.element === 'mediatrice') {
          oks4 = false
        }
        if (stor.element === 'sommet_oppose') {
          haz = j3pGetRandomInt(0, 2)
          if (haz === 0) {
            oks3 = false
            point2 = 'sommet_oppose'
          }
          if (haz === 1) {
            oks4 = false
            point1 = 'sommet_oppose'
          }
          if (haz === 2) {
            oks2 = false
            point0 = 'sommet_oppose'
          }
        }

        this.listedeselemedessine = []
        nomspris = []

        lalettre1 = addMaj(nomspris)
        if (okp0) {
          this.listedeselemedessine.push({
            type: 'point',
            nom: lalettre1,
            val: ['point', 'sommet', point0],
            tag: '#cpC',
            tagAllume: ['#pC', '#sC'],
            couleur: [255, 255, 255],
            ep: false
          })
        } else {
          stor.listeChange.push({ id: '#sC', co: coMod })
        }
        lalettre2 = addMaj(nomspris)
        if (okp1) {
          this.listedeselemedessine.push({
            type: 'point',
            nom: lalettre2,
            val: ['point', 'sommet', point1],
            tag: '#cpB',
            tagAllume: ['#pB', '#sB'],
            couleur: [255, 255, 255],
            ep: false
          })
        } else {
          stor.listeChange.push({ id: '#sB', co: coMod })
        }
        lalettre3 = addMaj(nomspris)
        if (okp2) {
          this.listedeselemedessine.push({
            type: 'point',
            nom: lalettre3,
            val: ['point', 'sommet', point2],
            tag: '#cpA',
            tagAllume: ['#pA', '#sA'],
            couleur: [255, 255, 255],
            ep: false
          })
        } else {
          stor.listeChange.push({ id: '#sA', co: coMod })
        }

        leslettresz = [lalettre1, lalettre2, lalettre3]
        rela = [rela0, rela1, rela2]
        oka = [oka0, oka1, oka2]
        // les angles
        for (i = 0; i < 3; i++) {
          if (oka[i]) {
            this.listedeselemedessine.push({
              type: 'angle',
              nom: 'angle(' + leslettresz[i] + leslettresz[(i + 1) % 3] + leslettresz[(i + 2) % 3] + ')',
              val: ['angle', rela[i]],
              tag: '#ca' + (i % 4 + 1),
              tagAllume: ['#a' + (i % 4 + 1), '#as' + (i % 4 + 1)],
              couleur: [0, 0, 0],
              ep: false
            })
          }
        }

        // les 4 dtes derrieres les cotes
        this.listedeselemedessine.push({
          type: 'droite',
          nom: '(' + leslettresz[0] + leslettresz[1] + ')',
          val: ['droite'],
          tag: '#cd1',
          tagAllume: ['#d3'],
          couleur: [130, 130, 130],
          ep: 2,
          serie: 3,
          contser: true
        })
        this.listedeselemedessine.push({
          type: 'droite',
          nom: '(' + leslettresz[1] + leslettresz[2] + ')',
          val: ['droite'],
          tag: '#cd2',
          tagAllume: ['#d2'],
          couleur: [130, 130, 130],
          ep: 2,
          serie: 2,
          contser: true
        })
        this.listedeselemedessine.push({
          type: 'droite',
          nom: '(' + leslettresz[2] + leslettresz[0] + ')',
          val: ['droite'],
          tag: '#cd3',
          tagAllume: ['#d1'],
          couleur: [130, 130, 130],
          ep: 2,
          serie: 1,
          contser: true
        })
        // les 4 cotes
        if (oks3) {
          this.listedeselemedessine.push({
            type: 'segment',
            nom: leslettresz[0] + leslettresz[1],
            val: ['segment', 'cote', rel3],
            tag: '#cs3',
            tagAllume: ['#seg3'],
            couleur: [0, 0, 0],
            ep: 3,
            serie: 3
          })
        } else {
          stor.listeChange.push({ id: '#seg3', ep: 3, co: coMod })
        }
        if (oks2) {
          this.listedeselemedessine.push({
            type: 'segment',
            nom: leslettresz[1] + leslettresz[2],
            val: ['segment', 'cote', 'base', rel2],
            tag: '#cs2',
            tagAllume: ['#seg2'],
            couleur: [0, 0, 0],
            ep: 3,
            serie: 2
          })
        } else {
          stor.listeChange.push({ id: '#seg2', ep: 3, co: coMod })
        }
        if (oks4) {
          this.listedeselemedessine.push({
            type: 'segment',
            nom: leslettresz[2] + leslettresz[0],
            val: ['segment', 'cote', rel4],
            tag: '#cs1',
            tagAllume: ['#seg1'],
            couleur: [0, 0, 0],
            ep: 3,
            serie: 1
          })
        } else {
          stor.listeChange.push({ id: '#seg1', ep: 3, co: coMod })
        }

        // les dtes derrieres 2 hauteurs
        // mediatrice
        if (okd1) {
          this.listedeselemedessine.push({
            type: 'droite',
            nom: lalettre3 + 'ô',
            val: [rel1, 'droite', 'hauteur'],
            tag: '#cd4',
            tagAllume: ['#h2'],
            couleur: [130, 130, 130],
            ep: 2
          })
        } else {
          stor.listeChange.push({ id: '#h2', ep: 3, co: coMod })
        }
        if (okd5) {
          this.listedeselemedessine.push({
            type: 'droite',
            nom: lalettre1 + 'ô2',
            val: [rel5, 'droite', 'hauteur'],
            tag: '#cd6',
            tagAllume: ['#h1'],
            couleur: [130, 130, 130],
            ep: 2
          })
        } else {
          stor.listeChange.push({ id: '#h1', ep: 3, co: coMod })
        }
        // les dtes derrieres 2 hauteurs
        // mediatrice

        // une mediatrice
        this.listedeselemedessine.push({
          type: 'droite',
          nom: 'kô3',
          val: [rel7, 'droite', 'mediatrice'],
          tag: '#cd5',
          tagAllume: ['#med'],
          couleur: [130, 130, 130],
          ep: 2
        })

        stor.LeNomDuTruc = lalettre1 + lalettre2 + lalettre3
        A1 = j3pGetRandomInt(1, 260)
        do {
          A2 = j3pGetRandomInt(30, 65) / 10
          A3 = j3pGetRandomInt(30, 65) / 10
        } while (Math.abs(A2 - A3) < 1 || Math.abs(Math.abs(55 - A2) - Math.abs(55 - A3)) < 1)

        listeNom = [{ t: '#npC', nom: lalettre3 }, { t: '#npA', nom: lalettre1 }, { t: '#npB', nom: lalettre2 }]

        break
      case 'TRIANGLE_RECTANGLE': // det les 4 points
        okp0 = okp1 = okp2 = true
        okd2 = okd3 = okd4 = okd7 = okd1 = true
        oks4 = oks2 = oks3 = true
        rela = ['angle_droit', 'piege', 'piege']
        oka = [true, true, true]
        if (stor.element === 'cote_oppose') {
          haz = j3pGetRandomInt(0, 2)
          if (haz === 0) {
            okp0 = false
            rel2 = 'cote_oppose'
          }
          if (haz === 1) {
            okp1 = false
            rel4 = 'cote_oppose'
          }
          if (haz === 2) {
            okp2 = false
            rel3 = 'cote_oppose'
          }
        }
        if (stor.element === 'cote_oppose_angle') {
          haz = j3pGetRandomInt(0, 1)
          if (haz === 0) {
            oka[2] = false
            rel4 = 'cote_oppose_angle'
          }
          if (haz === 1) {
            oka[1] = false
            rel3 = 'cote_oppose_angle'
          }
        }
        if (stor.element === 'cote_adjacent') {
          haz = j3pGetRandomInt(0, 1)
          if (haz === 0) {
            oka[2] = false
            rel3 = 'cote_adjacent'
          }
          if (haz === 1) {
            oka[1] = false
            rel4 = 'cote_adjacent'
          }
        }
        if (stor.element === 'cote_perpendiculaire') {
          haz = j3pGetRandomInt(0, 1)
          if (haz === 0) {
            oks3 = false
            rel4 = 'cote_perpendiculaire'
          }
          if (haz === 1) {
            oks4 = false
            rel3 = 'cote_perpendiculaire'
          }
        }
        if (stor.element === 'hauteur_relative') {
          haz = j3pGetRandomInt(0, 2)
          if (haz === 0) {
            oks3 = false
            rel4 = 'hauteur_relative'
            rel7 = 'hauteur_relative'
          }
          if (haz === 1) {
            oks4 = false
            rel5 = 'hauteur_relative'
            rel3 = 'hauteur_relative'
          }
          if (haz === 2) {
            oks2 = false
            rel1 = 'hauteur_relative'
          }
        }
        if (stor.element === 'base_relative') {
          haz = j3pGetRandomInt(0, 2)
          if (haz === 0) {
            oks4 = false
            rel3 = 'base_relative'
            okd7 = false
          }
          if (haz === 1) {
            oks3 = false
            rel4 = 'base_relative'
            okd1 = false
          }
          if (haz === 2) {
            okd3 = false
            rel2 = 'base_relative'
          }
        }
        if (stor.element === 'mediatrice') {
          oks4 = false
        }
        if (stor.element === 'sommet_oppose') {
          haz = j3pGetRandomInt(0, 2)
          if (haz === 0) {
            oks3 = false
            point2 = 'sommet_oppose'
          }
          if (haz === 1) {
            oks4 = false
            point1 = 'sommet_oppose'
          }
          if (haz === 2) {
            oks2 = false
            point0 = 'sommet_oppose'
          }
        }
        stor.listZig = ['#cpA', '#cpB', '#cpC', '#cs1', '#cs2', '#cs3', '#cd1', '#cd2', '#cd3', '#cd4', '#cd5', '#cd6', '#ca1', '#ca2', '#ca3']

        this.listedeselemedessine = []
        nomspris = []

        lalettre1 = addMaj(nomspris)
        if (okp0) {
          this.listedeselemedessine.push({
            type: 'point',
            nom: lalettre1,
            val: ['point', 'sommet', point0, 'sommet_angle_droit'],
            tag: '#cpC',
            tagAllume: ['#pC', '#sC'],
            couleur: [255, 255, 255],
            ep: false
          })
        } else {
          stor.listeChange.push({ id: '#sC', co: coMod })
        }
        lalettre2 = addMaj(nomspris)
        if (okp1) {
          this.listedeselemedessine.push({
            type: 'point',
            nom: lalettre2,
            val: ['point', 'sommet', point1],
            tag: '#cpB',
            tagAllume: ['#pB', '#sB'],
            couleur: [255, 255, 255],
            ep: false
          })
        } else {
          stor.listeChange.push({ id: '#sB', co: coMod })
        }
        lalettre3 = addMaj(nomspris)
        if (okp2) {
          this.listedeselemedessine.push({
            type: 'point',
            nom: lalettre3,
            val: ['point', 'sommet', point2],
            tag: '#cpA',
            tagAllume: ['#pA', '#sA'],
            couleur: [255, 255, 255],
            ep: false
          })
        } else {
          stor.listeChange.push({ id: '#sA', co: coMod })
        }

        leslettresz = [lalettre1, lalettre2, lalettre3]
        // les angles
        for (i = 0; i < 3; i++) {
          if (oka[i]) {
            this.listedeselemedessine.push({
              type: 'angle',
              nom: 'angle(' + leslettresz[i] + leslettresz[(i + 1) % 3] + leslettresz[(i + 2) % 3] + ')',
              val: ['angle', rela[i]],
              tag: '#ca' + (i % 4 + 1),
              tagAllume: ['#a' + (i % 3 + 1), '#as' + (i % 3 + 1)],
              couleur: [0, 0, 0],
              ep: false
            })
          } else {
            stor.listeChange.push(
              {
                id: '#as' + (i % 4 + 1),
                co: coMod
              })
          }
        }

        // les 3 dtes derrieres les cotes
        if (okd1) {
          this.listedeselemedessine.push({
            type: 'droite',
            nom: '(' + leslettresz[0] + leslettresz[1] + ')',
            val: ['droite', 'hauteur', rel5],
            tag: '#cd2',
            tagAllume: ['#d3'],
            couleur: [130, 130, 130],
            ep: 2,
            serie: 3,
            contser: true
          })
        } else {
          stor.listeChange.push({ id: '#d3', ep: 3, co: coMod })
        }
        this.listedeselemedessine.push({
          type: 'droite',
          nom: '(' + leslettresz[1] + leslettresz[2] + ')',
          val: ['droite'],
          tag: '#cd3',
          tagAllume: ['#d2'],
          couleur: [130, 130, 130],
          ep: 2,
          serie: 2,
          contser: true
        })
        if (okd7) {
          this.listedeselemedessine.push({
            type: 'droite',
            nom: '(' + leslettresz[2] + leslettresz[0] + ')',
            val: ['droite', 'hauteur', rel7],
            tag: '#cd1',
            tagAllume: ['#d1'],
            couleur: [130, 130, 130],
            ep: 2,
            serie: 1,
            contser: true
          })
        } else {
          stor.listeChange.push({ id: '#d1', ep: 3, co: coMod })
        }

        // les 3 cotes
        if (oks3) {
          this.listedeselemedessine.push({
            type: 'segment',
            nom: leslettresz[0] + leslettresz[1],
            val: ['segment', 'cote', rel3, 'hauteur'],
            tag: '#cs3',
            tagAllume: ['#seg3'],
            couleur: [0, 0, 0],
            ep: 3,
            serie: 3
          })
        } else {
          stor.listeChange.push({ id: '#seg3', ep: 3, co: coMod })
        }
        if (oks2) {
          this.listedeselemedessine.push({
            type: 'segment',
            nom: leslettresz[1] + leslettresz[2],
            val: ['segment', 'cote', 'hypotenuse', rel2],
            tag: '#cs2',
            tagAllume: ['#seg2'],
            couleur: [0, 0, 0],
            ep: 3,
            serie: 2
          })
        } else {
          stor.listeChange.push({ id: '#seg2', ep: 3, co: coMod })
        }
        if (oks4) {
          this.listedeselemedessine.push({
            type: 'segment',
            nom: leslettresz[2] + leslettresz[0],
            val: ['segment', 'cote', rel4, 'hauteur'],
            tag: '#cs1',
            tagAllume: ['#seg1'],
            couleur: [0, 0, 0],
            ep: 3,
            serie: 1
          })
        } else {
          stor.listeChange.push({ id: '#seg1', ep: 3, co: coMod })
        }

        // les dtes derrieres 2 hauteurs
        // mediatrice

        if (okd3) {
          this.listedeselemedessine.push({
            type: 'droite',
            nom: lalettre3 + 'ô',
            val: ['droite', 'hauteur', rel1],
            tag: '#cd4',
            tagAllume: ['#h1'],
            couleur: [130, 130, 130],
            ep: 2
          })
        } else {
          stor.listeChange.push({ id: '#h1', ep: 3, co: coMod })
        }
        if (okd2) {
          this.listedeselemedessine.push({
            type: 'droite',
            nom: lalettre1 + 'ô2',
            val: ['droite', reld2],
            tag: '#cd5',
            tagAllume: ['#piege'],
            couleur: [130, 130, 130],
            ep: 2
          })
        } else {
          stor.listeChange.push({ id: '#piege', ep: 3, co: coMod })
        }
        if (okd4) {
          this.listedeselemedessine.push({
            type: 'droite',
            nom: 'kô3',
            val: ['droite', 'mediatrice'],
            tag: '#cd6',
            tagAllume: ['#med'],
            couleur: [130, 130, 130],
            ep: 2
          })
        } else {
          stor.listeChange.push({ id: '#med', ep: 3, co: coMod })
        }

        A1 = j3pGetRandomInt(1, 260)
        A3 = j3pGetRandomInt(25, 65) / 10
        A2 = Math.sqrt(7.2 * 7.2 - A3 * A3)
        listeNom = [{ t: '#npC', nom: lalettre3 }, { t: '#npA', nom: lalettre1 }, { t: '#npB', nom: lalettre2 }]

        if (j3pGetRandomInt(0, 1) === 0) {
          stor.LeNomDuTruc = lalettre1 + lalettre2 + lalettre3 + ' rectangle en ' + lalettre3
        } else {
          stor.LeNomDuTruc = lalettre2 + lalettre1 + lalettre3 + ' rectangle en ' + lalettre3
        }
        break
      case 'TRIANGLE_ISOCELE': // det les 4 points
        reld2 = reld3 = reld4 = rel1 = rel2 = rel3 = rel4 = rel5 = rel7 = point0 = point1 = point2 = rela0 = rela1 = rela2 = 'piege'
        oka0 = oka1 = oka2 = true
        okp0 = okp1 = okp2 = true
        oks4 = oks2 = oks3 = true
        okd4 = okd2 = okd3 = true
        stor.listZig = ['#cpA', '#cpB', '#cpC', '#cs1', '#cs2', '#cs3', '#cd1', '#cd2', '#cd3', '#cd4', '#cd5', '#cd6', '#ca1', '#ca2', '#ca3']

        if (stor.element === 'cote_oppose') {
          haz = j3pGetRandomInt(0, 2)
          if (haz === 0) {
            okp0 = false
            rel2 = 'cote_oppose'
          }
          if (haz === 1) {
            okp1 = false
            rel4 = 'cote_oppose'
          }
          if (haz === 2) {
            okp2 = false
            rel3 = 'cote_oppose'
          }
        }
        if (stor.element === 'hauteur_relative') {
          haz = j3pGetRandomInt(0, 2)
          if (haz === 0) {
            oks3 = false
            reld3 = 'hauteur_relative'
          }
          if (haz === 1) {
            oks2 = false
            reld2 = 'hauteur_relative'
          }
          if (haz === 2) {
            oks4 = false
            reld4 = 'hauteur_relative'
          }
        }
        if (stor.element === 'mediatrice') {
          oks2 = false
          reld1 = 'mediatrice'
        }
        if (stor.element === 'base_relative') {
          haz = j3pGetRandomInt(0, 2)
          if (haz === 0) {
            okd3 = false
            rel3 = 'base_relative'
          }
          if (haz === 1) {
            okd2 = false
            rel2 = 'base_relative'
          }
          if (haz === 2) {
            okd4 = false
            rel4 = 'base_relative'
          }
        }
        if (stor.element === 'sommet_oppose') {
          haz = j3pGetRandomInt(0, 2)
          if (haz === 0) {
            oks3 = false
            point2 = 'sommet_oppose'
          }
          if (haz === 1) {
            oks4 = false
            point1 = 'sommet_oppose'
          }
          if (haz === 2) {
            oks2 = false
            point0 = 'sommet_oppose'
          }
        }
        if (stor.element === 'longueur_egale') {
          haz = j3pGetRandomInt(0, 1)
          if (haz === 0) {
            oks3 = false
            rel4 = 'longueur_egale'
          }
          if (haz === 1) {
            oks4 = false
            rel3 = 'longueur_egale'
          }
        }
        if (stor.element === 'angle_egal') {
          haz = j3pGetRandomInt(0, 1)
          haz = 0
          if (haz === 0) {
            oka2 = false
            rela1 = 'angle_egal'
          }
          if (haz === 1) {
            oka1 = false
            rela2 = 'angle_egal'
          }
        }

        this.listedeselemedessine = []
        nomspris = []

        lalettre1 = addMaj(nomspris)
        if (okp0) {
          this.listedeselemedessine.push({
            type: 'point',
            nom: lalettre1,
            val: ['point', 'sommet', point0, 'sommet_principal'],
            tag: '#cpC',
            tagAllume: ['#pC', '#sC'],
            couleur: [255, 255, 255],
            ep: false
          })
        } else {
          stor.listeChange.push({ id: '#sC', co: coMod })
        }
        lalettre2 = addMaj(nomspris)
        if (okp1) {
          this.listedeselemedessine.push({
            type: 'point',
            nom: lalettre2,
            val: ['point', 'sommet', point1],
            tag: '#cpB',
            tagAllume: ['#pB', '#sB'],
            couleur: [255, 255, 255],
            ep: false
          })
        } else {
          stor.listeChange.push({ id: '#sB', co: coMod })
        }
        lalettre3 = addMaj(nomspris)
        if (okp2) {
          this.listedeselemedessine.push({
            type: 'point',
            nom: lalettre3,
            val: ['point', 'sommet', point2],
            tag: '#cpA',
            tagAllume: ['#pA', '#sA'],
            couleur: [255, 255, 255],
            ep: false
          })
        } else {
          stor.listeChange.push({ id: '#sA', co: coMod })
        }

        leslettresz = [lalettre1, lalettre2, lalettre3]
        rela = [rela0, rela1, rela2]
        oka = [oka0, oka1, oka2]
        // les angles
        for (i = 0; i < 3; i++) {
          if (oka[i]) {
            this.listedeselemedessine.push({
              type: 'angle',
              nom: 'angle(' + leslettresz[i] + leslettresz[(i + 1) % 3] + leslettresz[(i + 2) % 3] + ')',
              val: ['angle', rela[i]],
              tag: '#ca' + (i % 4 + 1),
              tagAllume: ['#a' + (i % 3 + 1), '#as' + (i % 3 + 1)],
              couleur: [0, 0, 0],
              ep: false
            })
          } else {
            stor.listeChange.push(
              {
                id: '#as' + (i % 4 + 1),
                co: coMod
              })
          }
        }

        // les 3 dtes derrieres les cotes
        this.listedeselemedessine.push({
          type: 'droite',
          nom: '(' + leslettresz[0] + leslettresz[1] + ')',
          val: ['droite'],
          tag: '#cd1',
          tagAllume: ['#d3'],
          couleur: [130, 130, 130],
          ep: 2,
          serie: 3,
          contser: true
        })
        this.listedeselemedessine.push({
          type: 'droite',
          nom: '(' + leslettresz[1] + leslettresz[2] + ')',
          val: ['droite'],
          tag: '#cd2',
          tagAllume: ['#d2'],
          couleur: [130, 130, 130],
          ep: 2,
          serie: 2,
          contser: true
        })
        this.listedeselemedessine.push({
          type: 'droite',
          nom: '(' + leslettresz[2] + leslettresz[0] + ')',
          val: ['droite'],
          tag: '#cd3',
          tagAllume: ['#d1'],
          couleur: [130, 130, 130],
          ep: 2,
          serie: 1,
          contser: true
        })
        // les 3 cotes
        if (oks3) {
          this.listedeselemedessine.push({
            type: 'segment',
            nom: leslettresz[0] + leslettresz[1],
            val: ['segment', 'cote', rel3],
            tag: '#cs3',
            tagAllume: ['#seg3'],
            couleur: [0, 0, 0],
            ep: 3,
            serie: 3
          })
        } else {
          stor.listeChange.push({ id: '#seg3', ep: 3, co: coMod })
        }
        if (oks2) {
          this.listedeselemedessine.push({
            type: 'segment',
            nom: leslettresz[1] + leslettresz[2],
            val: ['segment', 'cote', 'base', rel2],
            tag: '#cs2',
            tagAllume: ['#seg2'],
            couleur: [0, 0, 0],
            ep: 3,
            serie: 2
          })
        } else {
          stor.listeChange.push({ id: '#seg2', ep: 3, co: coMod })
        }
        if (oks4) {
          this.listedeselemedessine.push({
            type: 'segment',
            nom: leslettresz[2] + leslettresz[0],
            val: ['segment', 'cote', rel4],
            tag: '#cs1',
            tagAllume: ['#seg1'],
            couleur: [0, 0, 0],
            ep: 3,
            serie: 1
          })
        } else {
          stor.listeChange.push({ id: '#seg1', ep: 3, co: coMod })
        }

        // les dtes derrieres 2 hauteurs
        // mediatrice

        if (okd3) {
          this.listedeselemedessine.push({
            type: 'droite',
            nom: lalettre3 + 'ô',
            val: ['droite', 'hauteur', reld3],
            tag: '#cd5',
            tagAllume: ['#h2'],
            couleur: [130, 130, 130],
            ep: 2
          })
        } else {
          stor.listeChange.push({ id: '#h2', ep: 3, co: coMod })
        }
        if (okd2) {
          this.listedeselemedessine.push({
            type: 'droite',
            nom: lalettre1 + 'ô2',
            val: ['droite', 'axe', 'mediatrice', reld2, 'hauteur'],
            tag: '#cd6',
            tagAllume: ['#h1'],
            couleur: [130, 130, 130],
            ep: 2
          })
        } else {
          stor.listeChange.push({ id: '#h1', ep: 3, co: coMod })
        }
        if (okd4) {
          this.listedeselemedessine.push({
            type: 'droite',
            nom: 'kô3',
            val: ['droite', reld4, 'hauteur'],
            tag: '#cd4',
            tagAllume: ['#med'],
            couleur: [130, 130, 130],
            ep: 2
          })
        } else {
          stor.listeChange.push({ id: '#med', ep: 3, co: coMod })
        }

        if (j3pGetRandomInt(0, 1) === 0) {
          stor.LeNomDuTruc = lalettre1 + lalettre2 + lalettre3 + ' isocèle en ' + lalettre3
        } else {
          stor.LeNomDuTruc = lalettre2 + lalettre1 + lalettre3 + ' isocèle en ' + lalettre3
        }
        A1 = j3pGetRandomInt(1, 260)
        do {
          A2 = j3pGetRandomInt(30, 70) / 10
        } while ((A2 > 4.7 && A2 < 5.5) || (A2 > 5.9 && A2 < 6.5))
        A3 = A2
        listeNom = [{ t: '#npC', nom: lalettre3 }, { t: '#npA', nom: lalettre1 }, { t: '#npB', nom: lalettre2 }]

        break
      case 'TRIANGLE_EQUILATERAL': // det les 4 points
        reld2 = reld3 = reld4 = rel1 = rel2 = rel3 = rel4 = rel5 = rel7 = point0 = point1 = point2 = rela0 = rela1 = rela2 = 'piege'
        oka0 = oka1 = oka2 = true
        okd3 = okd2 = okd4 = true
        okp0 = okp1 = okp2 = true
        oks4 = oks2 = oks3 = true
        okd4 = okd2 = okd3 = true
        stor.listZig = ['#cpA', '#cpB', '#cpC', '#cs1', '#cs2', '#cs3', '#cd1', '#cd2', '#cd3', '#cd4', '#cd5', '#cd6', '#ca1', '#ca2', '#ca3', '#cd7', '#cloG']

        if (stor.element === 'cote_oppose') {
          haz = j3pGetRandomInt(0, 2)
          if (haz === 0) {
            okp0 = false
            rel2 = 'cote_oppose'
          }
          if (haz === 1) {
            okp1 = false
            rel4 = 'cote_oppose'
          }
          if (haz === 2) {
            okp2 = false
            rel3 = 'cote_oppose'
          }
        }
        if (stor.element === 'hauteur_relative') {
          haz = j3pGetRandomInt(0, 2)
          if (haz === 0) {
            oks3 = false
            reld3 = 'hauteur_relative'
          }
          if (haz === 1) {
            oks4 = false
            reld4 = 'hauteur_relative'
          }
          if (haz === 2) {
            oks2 = false
            reld2 = 'hauteur_relative'
          }
        }
        if (stor.element === 'mediatrice') {
          oks3 = false
          reld3 = 'mediatrice'
        }
        if (stor.element === 'base_relative') {
          haz = j3pGetRandomInt(0, 2)
          if (haz === 0) {
            okd3 = false
            rel3 = 'base_relative'
          }
          if (haz === 1) {
            okd2 = false
            rel2 = 'base_relative'
          }
          if (haz === 2) {
            okd4 = false
            rel4 = 'base_relative'
          }
        }
        if (stor.element === 'sommet_oppose') {
          haz = j3pGetRandomInt(0, 2)
          if (haz === 0) {
            oks3 = false
            point2 = 'sommet_oppose'
          }
          if (haz === 1) {
            oks4 = false
            point1 = 'sommet_oppose'
          }
          if (haz === 2) {
            oks2 = false
            point0 = 'sommet_oppose'
          }
        }
        if (stor.element === 'longueur_egale') {
          haz = j3pGetRandomInt(0, 2)
          if (haz === 0) {
            oks3 = false
            rel2 = 'longueur_egale'
            rel4 = 'longueur_egale'
          }
          if (haz === 1) {
            oks4 = false
            rel2 = 'longueur_egale'
            rel3 = 'longueur_egale'
          }
          if (haz === 2) {
            oks2 = false
            rel3 = 'longueur_egale'
            rel4 = 'longueur_egale'
          }
        }
        if (stor.element === 'angle_egal') {
          haz = j3pGetRandomInt(0, 2)
          if (haz === 0) {
            oka0 = false
            rela2 = 'angle_egal'
            rela1 = 'angle_egal'
          }
          if (haz === 1) {
            oka1 = false
            rela2 = 'angle_egal'
            rela0 = 'angle_egal'
          }
          if (haz === 2) {
            oka2 = false
            rela1 = 'angle_egal'
            rela0 = 'angle_egal'
          }
        }

        this.listedeselemedessine = []
        nomspris = []

        lalettre1 = addMaj(nomspris)
        if (okp0) {
          this.listedeselemedessine.push({
            type: 'point',
            nom: lalettre1,
            val: ['point', 'sommet', point0, 'sommet_principal'],
            tag: '#cpC',
            tagAllume: ['#pC', '#sC'],
            couleur: [255, 255, 255],
            ep: false
          })
        } else {
          stor.listeChange.push({ id: '#sC', co: coMod })
        }
        lalettre2 = addMaj(nomspris)
        if (okp1) {
          this.listedeselemedessine.push({
            type: 'point',
            nom: lalettre2,
            val: ['point', 'sommet', point1],
            tag: '#cpB',
            tagAllume: ['#pB', '#sB'],
            couleur: [255, 255, 255],
            ep: false
          })
        } else {
          stor.listeChange.push({ id: '#sB', co: coMod })
        }
        lalettre3 = addMaj(nomspris)
        if (okp2) {
          this.listedeselemedessine.push({
            type: 'point',
            nom: lalettre3,
            val: ['point', 'sommet', point2],
            tag: '#cpA',
            tagAllume: ['#pA', '#sA'],
            couleur: [255, 255, 255],
            ep: false
          })
        } else {
          stor.listeChange.push({ id: '#sA', co: coMod })
        }
        this.listedeselemedessine.push({
          type: 'point',
          nom: 'G',
          val: ['point'],
          tag: '#cloG',
          tagAllume: ['#pG', '#sG'],
          couleur: [255, 255, 255],
          ep: false
        })

        leslettresz = [lalettre1, lalettre2, lalettre3]
        rela = [rela0, rela1, rela2]
        oka = [oka0, oka1, oka2]
        // les angles
        for (i = 0; i < 3; i++) {
          if (oka[i]) {
            this.listedeselemedessine.push({
              type: 'angle',
              nom: 'angle(' + leslettresz[i] + leslettresz[(i + 1) % 3] + leslettresz[(i + 2) % 3] + ')',
              val: ['angle', rela[i]],
              tag: '#ca' + (i % 4 + 1),
              tagAllume: ['#a' + (i % 3 + 1), '#as' + (i % 3 + 1)],
              couleur: [0, 0, 0],
              ep: false
            })
          } else {
            stor.listeChange.push(
              {
                id: '#as' + (i % 4 + 1),
                co: coMod
              })
          }
        }

        // les 3 dtes derrieres les cotes
        this.listedeselemedessine.push({
          type: 'droite',
          nom: '(' + leslettresz[0] + leslettresz[1] + ')',
          val: ['droite'],
          tag: '#cd1',
          tagAllume: ['#d3'],
          couleur: [130, 130, 130],
          ep: 2,
          serie: 3,
          contser: true
        })
        this.listedeselemedessine.push({
          type: 'droite',
          nom: '(' + leslettresz[1] + leslettresz[2] + ')',
          val: ['droite'],
          tag: '#cd2',
          tagAllume: ['#d2'],
          couleur: [130, 130, 130],
          ep: 2,
          serie: 2,
          contser: true
        })
        this.listedeselemedessine.push({
          type: 'droite',
          nom: '(' + leslettresz[2] + leslettresz[0] + ')',
          val: ['droite'],
          tag: '#cd3',
          tagAllume: ['#d1'],
          couleur: [130, 130, 130],
          ep: 2,
          serie: 1,
          contser: true
        })
        // les 3 cotes
        if (oks3) {
          this.listedeselemedessine.push({
            type: 'segment',
            nom: leslettresz[0] + leslettresz[1],
            val: ['segment', 'cote', rel3],
            tag: '#cs3',
            tagAllume: ['#seg3'],
            couleur: [0, 0, 0],
            ep: 3,
            serie: 3
          })
        } else {
          stor.listeChange.push({ id: '#seg3', ep: 3, co: coMod })
        }
        if (oks2) {
          this.listedeselemedessine.push({
            type: 'segment',
            nom: leslettresz[1] + leslettresz[2],
            val: ['segment', 'cote', 'base', rel2],
            tag: '#cs2',
            tagAllume: ['#seg2'],
            couleur: [0, 0, 0],
            ep: 3,
            serie: 2
          })
        } else {
          stor.listeChange.push({ id: '#seg2', ep: 3, co: coMod })
        }
        if (oks4) {
          this.listedeselemedessine.push({
            type: 'segment',
            nom: leslettresz[2] + leslettresz[0],
            val: ['segment', 'cote', rel4],
            tag: '#cs1',
            tagAllume: ['#seg1'],
            couleur: [0, 0, 0],
            ep: 3,
            serie: 1
          })
        } else {
          stor.listeChange.push({ id: '#seg1', ep: 3, co: coMod })
        }

        // les dtes derrieres 2 hauteurs
        // mediatrice

        if (okd3) {
          this.listedeselemedessine.push({
            type: 'droite',
            nom: lalettre3 + 'ô',
            val: ['droite', 'hauteur', reld3, 'axe'],
            tag: '#cd5',
            tagAllume: ['#h2'],
            couleur: [130, 130, 130],
            ep: 2
          })
        } else {
          stor.listeChange.push({ id: '#h2', ep: 3, co: coMod })
        }
        if (okd2) {
          this.listedeselemedessine.push({
            type: 'droite',
            nom: lalettre1 + 'ô2',
            val: ['droite', 'axe', reld2, 'hauteur'],
            tag: '#cd6',
            tagAllume: ['#h1'],
            couleur: [130, 130, 130],
            ep: 2
          })
        } else {
          stor.listeChange.push({ id: '#h1', ep: 3, co: coMod })
        }
        if (okd4) {
          this.listedeselemedessine.push({
            type: 'droite',
            nom: 'kô3',
            val: ['droite', reld4, 'hauteur', 'axe'],
            tag: '#cd4',
            tagAllume: ['#med'],
            couleur: [130, 130, 130],
            ep: 2
          })
        } else {
          stor.listeChange.push({ id: '#med', ep: 3, co: coMod })
        }
        this.listedeselemedessine.push({
          type: 'droite',
          nom: 'uuu',
          val: ['droite'],
          tag: '#cd7',
          tagAllume: ['#piege'],
          couleur: [130, 130, 130],
          ep: 2
        })
        A1 = j3pGetRandomInt(1, 260)
        A3 = A2 = 6.24
        listeNom = [{ t: '#npC', nom: lalettre3 }, { t: '#npA', nom: lalettre1 }, { t: '#npB', nom: lalettre2 }]

        stor.LeNomDuTruc = lalettre1 + lalettre2 + lalettre3

        break
      case 'CUBE': // det les 4 points
        listeNom = []
        stor.height = 320

        stor.listZig = ['#cseg1', '#cseg2', '#cseg3', '#cseg4', '#cseg5', '#cseg6', '#cseg7', '#cseg8', '#cseg9', '#cseg10', '#cseg11', '#cseg12', '#cc1', '#cc2', '#cc3', '#cc4', '#cc5', '#cc6', '#cc7', '#cc8']
        for (i = 0; i < 8; i++) {
          lalettre = addMaj(nomspris)
          this.listedeselemedessine.push({
            type: 'point',
            nom: lalettre,
            val: ['point', 'sommet'],
            tag: '#cc' + (i + 1),
            tagAllume: ['#p' + (i + 1), '#s' + (i + 1)],
            couleur: [255, 255, 255],
            ep: false
          })
          listeNom.push({ t: '#np' + (i + 1), nom: lalettre })
        }

        ok1 = ok2 = ok3 = ok4 = ok5 = ok6 = true
        typeface2 = typeface1 = typeface3 = typeface4 = typeface5 = typeface6 = 'piege'
        if (stor.element === 'face_parallele') {
          jh = j3pGetRandomInt(0, 5)
          if (jh === 0) {
            ok1 = false
            typeface5 = 'face_parallele'
          }
          if (jh === 1) {
            ok2 = false
            typeface3 = 'face_parallele'
          }
          if (jh === 2) {
            ok3 = false
            typeface2 = 'face_parallele'
          }
          if (jh === 3) {
            ok4 = false
            typeface6 = 'face_parallele'
          }
          if (jh === 4) {
            ok5 = false
            typeface1 = 'face_parallele'
          }
          if (jh === 5) {
            ok6 = false
            typeface4 = 'face_parallele'
          }
        }
        if (stor.element === 'face_perpendiculaire') {
          typeface1 = typeface2 = typeface3 = typeface4 = typeface5 = typeface6 = 'face_perpendiculaire'
          jh = j3pGetRandomInt(0, 5)
          if (jh === 0) {
            ok1 = false
            typeface5 = 'piege'
          }
          if (jh === 1) {
            ok2 = false
            typeface3 = 'piege'
          }
          if (jh === 2) {
            ok3 = false
            typeface2 = 'piege'
          }
          if (jh === 3) {
            ok4 = false
            typeface6 = 'piege'
          }
          if (jh === 4) {
            ok5 = false
            typeface1 = 'piege'
          }
          if (jh === 5) {
            ok6 = false
            typeface4 = 'piege'
          }
        }
        // face haut
        if (ok4) {
          this.listedeselemedessine.push({
            type: 'face',
            nom: 'pol1',
            val: ['face', 'face_laterale', typeface4],
            tag: '#polh5',
            tagAllume: ['#surf3'],
            tagspe: '#vv1',
            couleur: [255, 255, 255],
            coolspe: specool,
            ep: 1
          })
        } else {
          stor.listeChange.push({ id: '#surf3', co: coMod })
          stor.listCache.push('#polh1', '#polh2', '#polh3', '#polh4', '#polh5', '#vh0', '#vh1', '#vh2', '#vh3', '#vv1')
        }
        // face gauche
        if (ok2) {
          this.listedeselemedessine.push({
            type: 'face',
            nom: 'pol1',
            val: ['face', 'face_laterale', typeface2, 'face_cachee'],
            tag: '#pog5',
            tagAllume: ['#surf2'],
            tagspe: '#vv2',
            couleur: [255, 255, 255],
            coolspe: specool,
            ep: 1
          })
        } else {
          stor.listeChange.push({ id: '#surf2', co: coMod })
          stor.listCache.push('#pog1', '#pog2', '#pog3', '#pog4', '#pog5', '#vg0', '#vg1', '#vg2', '#vg3', '#vv2')
        }
        // face droite
        if (ok3) {
          this.listedeselemedessine.push({
            type: 'face',
            nom: 'pol1',
            val: ['face', 'face_laterale', typeface3],
            tag: '#pold5',
            tagAllume: ['#surf5'],
            tagspe: '#vv3',
            couleur: [255, 255, 255],
            coolspe: specool,
            ep: 1
          })
        } else {
          stor.listeChange.push({ id: '#surf5', co: coMod })
          stor.listCache.push('#pold1', '#pold2', '#pold3', '#pold4', '#pold5', '#vd0', '#vd1', '#vd2', '#vd3', '#vv3')
        }
        // face devant
        if (ok1) {
          this.listedeselemedessine.push({
            type: 'face',
            nom: 'pol1',
            val: ['face', 'face_laterale', typeface1],
            tag: '#polf5',
            tagAllume: ['#surf6'],
            tagspe: '#vv4',
            couleur: [255, 255, 255],
            coolspe: specool,
            ep: 1
          })
        } else {
          stor.listeChange.push({ id: '#surf6', co: coMod })
          stor.listCache.push('#polf1', '#polf2', '#polf3', '#polf4', '#polf5', '#vf0', '#vf1', '#vf2', '#vf3', '#vv4')
        }
        // face arriere
        if (ok5) {
          this.listedeselemedessine.push({
            type: 'face',
            nom: 'pol1',
            val: ['face', 'face_laterale', typeface5, 'face_cachee'],
            tag: '#polr5',
            tagAllume: ['#surf1'],
            tagspe: '#vv5',
            couleur: [255, 255, 255],
            coolspe: specool,
            ep: 1
          })
        } else {
          stor.listeChange.push({ id: '#surf1', co: coMod })
          stor.listCache.push('#polr1', '#polr2', '#polr3', '#polr4', '#polr5', '#vr0', '#vr1', '#vr2', '#vr3', '#vv5')
        }
        // face dessous
        if (ok6) {
          this.listedeselemedessine.push({
            type: 'face',
            nom: 'pol1',
            val: ['face_cachee', 'face', 'base', typeface6],
            tag: '#pols5',
            tagAllume: ['#surf4'],
            tagspe: '#vv6',
            couleur: [255, 255, 255],
            coolspe: specool,
            ep: 1
          })
        } else {
          stor.listeChange.push({ id: '#surf4', co: coMod })
          stor.listCache.push('#pols1', '#pols2', '#pols3', '#pols4', '#pols5', '#vs0', '#vs1', '#vs2', '#vs3', '#vv6')
        }

        ok1 = ok2 = ok3 = ok4 = ok5 = ok6 = ok7 = ok8 = ok9 = ok10 = ok11 = ok12 = true
        rel1 = rel2 = rel3 = rel4 = rel5 = rel6 = rel7 = rel8 = rel9 = rel10 = rel11 = rel12 = 'piege'
        if (stor.element === 'arete_parallele') {
          jh = j3pGetRandomInt(1, 12)
          if (jh === 1) {
            ok1 = false
            rel7 = rel11 = rel3 = 'arete_parallele'
          }
          if (jh === 2) {
            ok2 = false
            rel4 = rel10 = rel8 = 'arete_parallele'
          }
          if (jh === 3) {
            ok3 = false
            rel7 = rel11 = rel1 = 'arete_parallele'
          }
          if (jh === 4) {
            ok4 = false
            rel8 = rel10 = rel2 = 'arete_parallele'
          }
          if (jh === 5) {
            ok5 = false
            rel9 = rel12 = rel6 = 'arete_parallele'
          }
          if (jh === 6) {
            ok6 = false
            rel9 = rel12 = rel5 = 'arete_parallele'
          }
          if (jh === 7) {
            ok7 = false
            rel1 = rel11 = rel3 = 'arete_parallele'
          }
          if (jh === 8) {
            ok8 = false
            rel4 = rel10 = rel2 = 'arete_parallele'
          }
          if (jh === 9) {
            ok9 = false
            rel5 = rel12 = rel6 = 'arete_parallele'
          }
          if (jh === 10) {
            ok10 = false
            rel8 = rel4 = rel2 = 'arete_parallele'
          }
          if (jh === 11) {
            ok11 = false
            rel7 = rel1 = rel3 = 'arete_parallele'
          }
          if (jh === 12) {
            ok12 = false
            rel6 = rel5 = rel9 = 'arete_parallele'
          }
        }
        if (stor.element === 'longueur_egale') {
          jh = j3pGetRandomInt(1, 12)
          if (jh === 1) {
            ok1 = false
          }
          if (jh === 2) {
            ok2 = false
          }
          if (jh === 3) {
            ok3 = false
          }
          if (jh === 4) {
            ok4 = false
          }
          if (jh === 5) {
            ok5 = false
          }
          if (jh === 6) {
            ok6 = false
          }
          if (jh === 7) {
            ok7 = false
          }
          if (jh === 8) {
            ok8 = false
          }
          if (jh === 9) {
            ok9 = false
          }
          if (jh === 10) {
            ok10 = false
          }
          if (jh === 11) {
            ok11 = false
          }
          if (jh === 12) {
            ok12 = false
          }
        }
        if (stor.element === 'arete_orthogonale') {
          jh = j3pGetRandomInt(1, 12)
          if (jh === 1) {
            ok1 = false
            rel4 = rel9 = rel8 = rel5 = rel2 = rel12 = rel10 = rel6 = 'arete_orthogonale'
          }
          if (jh === 2) {
            ok2 = false
            rel1 = rel5 = rel7 = rel6 = rel3 = rel12 = rel11 = rel9 = 'arete_orthogonale'
          }
          if (jh === 3) {
            ok3 = false
            rel4 = rel9 = rel8 = rel5 = rel2 = rel12 = rel10 = rel6 = 'arete_orthogonale'
          }
          if (jh === 4) {
            ok4 = false
            rel1 = rel5 = rel7 = rel6 = rel3 = rel12 = rel11 = rel9 = 'arete_orthogonale'
          }
          if (jh === 5) {
            ok5 = false
            rel1 = rel2 = rel3 = rel4 = rel7 = rel8 = rel11 = rel10 = 'arete_orthogonale'
          }
          if (jh === 6) {
            ok6 = false
            rel1 = rel2 = rel3 = rel4 = rel7 = rel8 = rel11 = rel10 = 'arete_orthogonale'
          }
          if (jh === 7) {
            ok7 = false
            rel4 = rel9 = rel8 = rel5 = rel2 = rel12 = rel10 = rel6 = 'arete_orthogonale'
          }
          if (jh === 8) {
            ok8 = false
            rel1 = rel5 = rel7 = rel6 = rel3 = rel12 = rel11 = rel9 = 'arete_orthogonale'
          }
          if (jh === 9) {
            ok9 = false
            rel1 = rel2 = rel3 = rel4 = rel7 = rel8 = rel11 = rel10 = 'arete_orthogonale'
          }
          if (jh === 10) {
            ok10 = false
            rel1 = rel5 = rel7 = rel6 = rel3 = rel12 = rel11 = rel9 = 'arete_orthogonale'
          }
          if (jh === 11) {
            ok11 = false
            rel4 = rel9 = rel8 = rel5 = rel2 = rel12 = rel10 = rel6 = 'arete_orthogonale'
          }
          if (jh === 12) {
            ok12 = false
            rel1 = rel2 = rel3 = rel4 = rel7 = rel8 = rel11 = rel10 = 'arete_orthogonale'
          }
        }
        if (stor.element === 'arete_perpendiculaire') {
          jh = j3pGetRandomInt(1, 12)
          if (jh === 1) {
            ok1 = false
            rel4 = rel5 = rel2 = rel6 = 'arete_perpendiculaire'
          }
          if (jh === 2) {
            ok2 = false
            rel3 = rel12 = rel1 = rel6 = 'arete_perpendiculaire'
          }
          if (jh === 3) {
            ok3 = false
            rel4 = rel9 = rel2 = rel12 = 'arete_perpendiculaire'
          }
          if (jh === 4) {
            ok4 = false
            rel3 = rel5 = rel9 = rel1 = 'arete_perpendiculaire'
          }
          if (jh === 5) {
            ok5 = false
            rel4 = rel1 = rel7 = rel8 = 'arete_perpendiculaire'
          }
          if (jh === 6) {
            ok6 = false
            rel1 = rel7 = rel2 = rel10 = 'arete_perpendiculaire'
          }
          if (jh === 7) {
            ok7 = false
            rel8 = rel5 = rel10 = rel6 = 'arete_perpendiculaire'
          }
          if (jh === 8) {
            ok8 = false
            rel9 = rel5 = rel10 = rel7 = 'arete_perpendiculaire'
          }
          if (jh === 9) {
            ok9 = false
            rel4 = rel3 = rel11 = rel8 = 'arete_perpendiculaire'
          }
          if (jh === 10) {
            ok10 = false
            rel6 = rel7 = rel12 = rel11 = 'arete_perpendiculaire'
          }
          if (jh === 11) {
            ok11 = false
            rel8 = rel9 = rel10 = rel12 = 'arete_perpendiculaire'
          }
          if (jh === 12) {
            ok12 = false
            rel10 = rel11 = rel2 = rel3 = 'arete_perpendiculaire'
          }
        }

        if (ok1) {
          this.listedeselemedessine.push({
            type: 'arete',
            nom: lalettre,
            val: ['arete', 'longueur_egale', rel1],
            tag: '#cseg1',
            tagAllume: ['#seg1'],
            couleur: [0, 0, 0],
            ep: 3
          })
        } else { stor.listeChange.push({ id: '#seg1', co: coMod, ep: 4 }) }
        if (ok2) {
          this.listedeselemedessine.push({
            type: 'arete',
            nom: lalettre,
            val: ['arete', 'longueur_egale', rel2],
            tag: '#cseg2',
            tagAllume: ['#seg2'],
            couleur: [0, 0, 0],
            ep: 3
          })
        } else { stor.listeChange.push({ id: '#seg2', co: coMod, ep: 4 }) }
        if (ok3) {
          this.listedeselemedessine.push({
            type: 'arete',
            nom: lalettre,
            val: ['arete', 'longueur_egale', rel3],
            tag: '#cseg3',
            tagAllume: ['#seg3'],
            couleur: [0, 0, 0],
            ep: 3
          })
        } else { stor.listeChange.push({ id: '#seg3', co: coMod, ep: 4 }) }
        if (ok4) {
          this.listedeselemedessine.push({
            type: 'arete',
            nom: lalettre,
            val: ['arete', 'longueur_egale', rel4],
            tag: '#cseg4',
            tagAllume: ['#seg4'],
            couleur: [0, 0, 0],
            ep: 3
          })
        } else { stor.listeChange.push({ id: '#seg4', co: coMod, ep: 4 }) }
        if (ok5) {
          this.listedeselemedessine.push({
            type: 'arete',
            nom: lalettre,
            val: ['arete', 'longueur_egale', rel5],
            tag: '#cseg5',
            tagAllume: ['#seg5'],
            couleur: [0, 0, 0],
            ep: 3
          })
        } else { stor.listeChange.push({ id: '#seg5', co: coMod, ep: 4 }) }
        if (ok6) {
          this.listedeselemedessine.push({
            type: 'arete',
            nom: lalettre,
            val: ['arete', 'longueur_egale', rel6],
            tag: '#cseg6',
            tagAllume: ['#seg6'],
            couleur: [0, 0, 0],
            ep: 3
          })
        } else { stor.listeChange.push({ id: '#seg6', co: coMod, ep: 4 }) }
        if (ok7) {
          this.listedeselemedessine.push({
            type: 'arete',
            nom: lalettre,
            val: ['arete', 'longueur_egale', rel7],
            tag: '#cseg7',
            tagAllume: ['#seg7'],
            couleur: [0, 0, 0],
            ep: 3
          })
        } else { stor.listeChange.push({ id: '#seg7', co: coMod, ep: 4 }) }
        if (ok8) {
          this.listedeselemedessine.push({
            type: 'arete',
            nom: lalettre,
            val: ['arete', 'longueur_egale', rel8],
            tag: '#cseg8',
            tagAllume: ['#seg8'],
            couleur: [0, 0, 0],
            ep: 3
          })
        } else { stor.listeChange.push({ id: '#seg8', co: coMod, ep: 4 }) }
        if (ok9) {
          this.listedeselemedessine.push({
            type: 'arete',
            nom: lalettre,
            val: ['arete', 'longueur_egale', rel9],
            tag: '#cseg9',
            tagAllume: ['#seg9'],
            couleur: [0, 0, 0],
            ep: 3
          })
        } else { stor.listeChange.push({ id: '#seg9', co: coMod, ep: 4 }) }
        if (ok10) {
          this.listedeselemedessine.push({
            type: 'arete',
            nom: lalettre,
            val: ['arete', 'longueur_egale', rel10, 'arete_cachee'],
            tag: '#cseg10',
            tagAllume: ['#seg10'],
            couleur: [0, 0, 0],
            ep: 3,
            st: 2
          })
        } else { stor.listeChange.push({ id: '#seg10', co: coMod, ep: 4, st: 2 }) }
        if (ok11) {
          this.listedeselemedessine.push({
            type: 'arete',
            nom: lalettre,
            val: ['arete', 'longueur_egale', rel11, 'arete_cachee'],
            tag: '#cseg11',
            tagAllume: ['#seg11'],
            couleur: [0, 0, 0],
            ep: 3,
            st: 2
          })
        } else { stor.listeChange.push({ id: '#seg11', co: coMod, ep: 4, st: 2 }) }
        if (ok12) {
          this.listedeselemedessine.push({
            type: 'arete',
            nom: lalettre,
            val: ['arete', 'longueur_egale', rel12, 'arete_cachee'],
            tag: '#cseg12',
            tagAllume: ['#seg12'],
            couleur: [0, 0, 0],
            ep: 3,
            st: 2
          })
        } else { stor.listeChange.push({ id: '#seg12', co: coMod, ep: 4, st: 2 }) }

        A1 = j3pGetRandomInt(0, 359)
        break
      case 'PAVE_DROIT': // det les 4 points
        stor.height = stor.lwidth = 400
        A1 = j3pGetRandomInt(1, 360)
        A2 = j3pGetRandomInt(0, 10)
        do {
          A3 = j3pGetRandomInt(0, 10)
        } while (A2 === A3)
        // 8 sommets
        lalettre1 = addMaj(nomspris)
        lalettre2 = addMaj(nomspris)
        lalettre3 = addMaj(nomspris)
        lalettre4 = addMaj(nomspris)
        lalettre5 = addMaj(nomspris)
        lalettre6 = addMaj(nomspris)
        lalettre7 = addMaj(nomspris)
        lalettre8 = addMaj(nomspris)

        stor.listZig = ['#cseg1', '#cseg2', '#cseg3', '#cseg4', '#cseg5', '#cseg6', '#cseg7', '#cseg8', '#cseg9', '#cseg10', '#cseg11', '#cseg12', '#ccA', '#ccB', '#ccC', '#ccD', '#ccE', '#ccF', '#ccG', '#ccH']

        // 8 sommets
        this.listedeselemedessine.push({
          type: 'point',
          nom: lalettre1,
          val: ['point', 'sommet'],
          tag: '#ccA',
          tagAllume: ['#sA'],
          couleur: [255, 255, 255],
          ep: false
        })
        listeNom.push({ t: '#npA', nom: lalettre1 })
        this.listedeselemedessine.push({
          type: 'point',
          nom: lalettre2,
          val: ['point', 'sommet'],
          tag: '#ccB',
          tagAllume: ['#sB'],
          couleur: [255, 255, 255],
          ep: false
        })
        listeNom.push({ t: '#npB', nom: lalettre2 })
        this.listedeselemedessine.push({
          type: 'point',
          nom: lalettre3,
          val: ['point', 'sommet'],
          tag: '#ccC',
          tagAllume: ['#sC'],
          couleur: [255, 255, 255],
          ep: false
        })
        listeNom.push({ t: '#npC', nom: lalettre3 })
        this.listedeselemedessine.push({
          type: 'point',
          nom: lalettre4,
          val: ['point', 'sommet'],
          tag: '#ccD',
          tagAllume: ['#sD'],
          couleur: [255, 255, 255],
          ep: false
        })
        listeNom.push({ t: '#npD', nom: lalettre4 })
        this.listedeselemedessine.push({
          type: 'point',
          nom: lalettre5,
          val: ['point', 'sommet'],
          tag: '#ccE',
          tagAllume: ['#sE'],
          couleur: [255, 255, 255],
          ep: false
        })
        listeNom.push({ t: '#npE', nom: lalettre5 })
        this.listedeselemedessine.push({
          type: 'point',
          nom: lalettre6,
          val: ['point', 'sommet'],
          tag: '#ccF',
          tagAllume: ['#sF'],
          couleur: [255, 255, 255],
          ep: false
        })
        listeNom.push({ t: '#npF', nom: lalettre6 })
        this.listedeselemedessine.push({
          type: 'point',
          nom: lalettre7,
          val: ['point', 'sommet'],
          tag: '#ccG',
          tagAllume: ['#sG'],
          couleur: [255, 255, 255],
          ep: false
        })
        listeNom.push({ t: '#npG', nom: lalettre7 })
        this.listedeselemedessine.push({
          type: 'point',
          nom: lalettre8,
          val: ['point', 'sommet'],
          tag: '#ccH',
          tagAllume: ['#sH'],
          couleur: [255, 255, 255],
          ep: false
        })
        listeNom.push({ t: '#npH', nom: lalettre8 })

        ok1 = ok2 = ok3 = ok4 = ok5 = ok6 = true
        typeface2 = typeface1 = typeface3 = typeface4 = typeface5 = typeface6 = 'piege'
        if (stor.element === 'face_parallele') {
          jh = j3pGetRandomInt(0, 5)
          if (jh === 0) {
            ok1 = false
            typeface5 = 'face_parallele'
          }
          if (jh === 1) {
            ok2 = false
            typeface3 = 'face_parallele'
          }
          if (jh === 2) {
            ok3 = false
            typeface2 = 'face_parallele'
          }
          if (jh === 3) {
            ok4 = false
            typeface6 = 'face_parallele'
          }
          if (jh === 4) {
            ok5 = false
            typeface1 = 'face_parallele'
          }
          if (jh === 5) {
            ok6 = false
            typeface4 = 'face_parallele'
          }
        }
        if (stor.element === 'face_perpendiculaire') {
          typeface1 = typeface2 = typeface3 = typeface4 = typeface5 = typeface6 = 'face_perpendiculaire'
          jh = j3pGetRandomInt(0, 5)
          if (jh === 0) {
            ok1 = false
            typeface5 = 'piege'
          }
          if (jh === 1) {
            ok2 = false
            typeface3 = 'piege'
          }
          if (jh === 2) {
            ok3 = false
            typeface2 = 'piege'
          }
          if (jh === 3) {
            ok4 = false
            typeface6 = 'piege'
          }
          if (jh === 4) {
            ok5 = false
            typeface1 = 'piege'
          }
          if (jh === 5) {
            ok6 = false
            typeface4 = 'piege'
          }
        }

        // face gauche
        if (ok2) {
          this.listedeselemedessine.push({
            type: 'face',
            nom: 'pol1',
            val: ['face', 'face_cachee', typeface2],
            tag: '#pog5',
            tagAllume: ['#surfgauche'],
            tagspe: '#sog5',
            couleur: [255, 255, 255],
            coolspe: specool,
            ep: 1
          })
        } else {
          stor.listeChange.push({ id: '#surfgauche', co: coMod })
          stor.listCache.push('#pog1', '#pog2', '#pog3', '#pog4', '#pog5', '#sog1', '#sog2', '#sog3', '#sog4', '#sog5')
        }
        // face arriere
        if (ok5) {
          this.listedeselemedessine.push({
            type: 'face',
            nom: 'pof1',
            val: ['face', 'face_cachee', typeface5],
            tag: '#pof6',
            tagAllume: ['#surfarriere'],
            tagspe: '#sof5',
            couleur: [255, 255, 255],
            coolspe: specool,
            ep: 1
          })
        } else {
          stor.listeChange.push({ id: '#surfarriere', co: coMod })
          stor.listCache.push('#pof1', '#pof2', '#pof3', '#pof4', '#pof6', '#sof1', '#sof2', '#sof3', '#sof4', '#sof5', '#sof6')
        }
        // face dessous
        if (ok6) {
          this.listedeselemedessine.push({
            type: 'face',
            nom: 'pof1',
            val: ['face', 'face_cachee', typeface6],
            tag: '#poa5',
            tagAllume: ['#surfbas'],
            tagspe: '#soa5',
            couleur: [255, 255, 255],
            coolspe: specool,
            ep: 1
          })
        } else {
          stor.listeChange.push({ id: '#surfbas', co: coMod })
          stor.listCache.push('#poa1', '#poa2', '#poa3', '#poa4', '#poa5', '#soa1', '#soa2', '#soa3', '#soa4', '#soa5')
        }
        // face devant
        if (ok1) {
          this.listedeselemedessine.push({
            type: 'face',
            nom: 'pof1',
            val: ['face', 'face_laterale', typeface1],
            tag: '#pob5',
            tagAllume: ['#surfavant'],
            tagspe: '#sob5',
            couleur: [255, 255, 255],
            coolspe: specool,
            ep: 1
          })
        } else {
          stor.listeChange.push({ id: '#surfavant', co: coMod })
          stor.listCache.push('#pob1', '#pob2', '#pob3', '#pob4', '#pob5', '#sob1', '#sob2', '#sob3', '#sob4', '#sob5')
        }
        // face droite
        if (ok3) {
          this.listedeselemedessine.push({
            type: 'face',
            nom: 'pogsdfs1',
            val: ['face', 'face_laterale', typeface3],
            tag: '#pod5',
            tagAllume: ['#surfdroite'],
            tagspe: '#sod5',
            couleur: [255, 255, 255],
            coolspe: specool,
            ep: 1
          })
        } else {
          stor.listeChange.push({ id: '#surfdroite', co: coMod })
          stor.listCache.push('#pod1', '#pod2', '#pod3', '#pod4', '#pod5', '#sod1', '#sod2', '#sod3', '#sod4', '#sod5')
        }
        // face haut
        if (ok4) {
          this.listedeselemedessine.push({
            type: 'face',
            nom: 'pof1',
            val: ['face', 'face_laterale', typeface4],
            tag: '#poh5',
            tagAllume: ['#surfhaut'],
            tagspe: '#soh5',
            couleur: [255, 255, 255],
            coolspe: specool,
            ep: 1
          })
        } else {
          stor.listeChange.push({ id: '#surfhaut', co: coMod })
          stor.listCache.push('#poh1', '#poh2', '#poh3', '#poh4', '#poh5', '#soh1', '#soh2', '#soh3', '#soh4', '#soh5')
        }

        ok1 = ok2 = ok3 = ok4 = ok5 = ok6 = ok7 = ok8 = ok9 = ok10 = ok11 = ok12 = true
        rel1 = rel2 = rel3 = rel4 = rel5 = rel6 = rel7 = rel8 = rel9 = rel10 = rel11 = rel12 = 'piege'
        if (stor.element === 'arete_parallele') {
          jh = j3pGetRandomInt(1, 12)
          if (jh === 1) {
            ok1 = false
            rel3 = rel7 = rel5 = 'arete_parallele'
          }
          if (jh === 2) {
            ok2 = false
            rel4 = rel8 = rel6 = 'arete_parallele'
          }
          if (jh === 3) {
            ok3 = false
            rel1 = rel7 = rel5 = 'arete_parallele'
          }
          if (jh === 4) {
            ok4 = false
            rel2 = rel8 = rel6 = 'arete_parallele'
          }
          if (jh === 5) {
            ok5 = false
            rel3 = rel7 = rel1 = 'arete_parallele'
          }
          if (jh === 6) {
            ok6 = false
            rel2 = rel4 = rel8 = 'arete_parallele'
          }
          if (jh === 7) {
            ok7 = false
            rel3 = rel1 = rel5 = 'arete_parallele'
          }
          if (jh === 8) {
            ok8 = false
            rel4 = rel2 = rel6 = 'arete_parallele'
          }
          if (jh === 9) {
            ok9 = false
            rel12 = rel10 = rel11 = 'arete_parallele'
          }
          if (jh === 10) {
            ok10 = false
            rel9 = rel12 = rel11 = 'arete_parallele'
          }
          if (jh === 11) {
            ok11 = false
            rel9 = rel10 = rel12 = 'arete_parallele'
          }
          if (jh === 12) {
            ok12 = false
            rel9 = rel10 = rel11 = 'arete_parallele'
          }
        }
        if (stor.element === 'longueur_egale') {
          jh = j3pGetRandomInt(1, 12)
          if (jh === 1) {
            ok1 = false
            rel3 = rel7 = rel5 = 'longueur_egale'
          }
          if (jh === 2) {
            ok2 = false
            rel4 = rel8 = rel6 = 'longueur_egale'
          }
          if (jh === 3) {
            ok3 = false
            rel1 = rel7 = rel5 = 'longueur_egale'
          }
          if (jh === 4) {
            ok4 = false
            rel2 = rel8 = rel6 = 'longueur_egale'
          }
          if (jh === 5) {
            ok5 = false
            rel3 = rel7 = rel1 = 'longueur_egale'
          }
          if (jh === 6) {
            ok6 = false
            rel2 = rel4 = rel8 = 'longueur_egale'
          }
          if (jh === 7) {
            ok7 = false
            rel3 = rel1 = rel5 = 'longueur_egale'
          }
          if (jh === 8) {
            ok8 = false
            rel4 = rel2 = rel6 = 'longueur_egale'
          }
          if (jh === 9) {
            ok9 = false
            rel9 = rel10 = rel11 = 'longueur_egale'
          }
          if (jh === 10) {
            ok10 = false
            rel9 = rel12 = rel11 = 'longueur_egale'
          }
          if (jh === 11) {
            ok11 = false
            rel9 = rel10 = rel12 = 'longueur_egale'
          }
          if (jh === 12) {
            ok12 = false
            rel9 = rel10 = rel11 = 'longueur_egale'
          }
        }
        if (stor.element === 'arete_orthogonale') {
          jh = j3pGetRandomInt(1, 12)
          if (jh === 1) {
            ok1 = false
            rel2 = rel4 = rel6 = rel8 = 'arete_orthogonale'
            rel9 = rel10 = rel11 = rel12 = 'arete_orthogonale'
          }
          if (jh === 2) {
            ok2 = false
            rel9 = rel10 = rel11 = rel12 = 'arete_orthogonale'
            rel1 = rel3 = rel7 = rel5 = 'arete_orthogonale'
          }
          if (jh === 3) {
            ok3 = false
            rel2 = rel4 = rel6 = rel8 = 'arete_orthogonale'
            rel9 = rel10 = rel11 = rel12 = 'arete_orthogonale'
          }
          if (jh === 4) {
            ok4 = false
            rel9 = rel10 = rel11 = rel12 = 'arete_orthogonale'
            rel1 = rel3 = rel7 = rel5 = 'arete_orthogonale'
          }
          if (jh === 5) {
            ok5 = false
            rel2 = rel4 = rel6 = rel8 = 'arete_orthogonale'
            rel9 = rel10 = rel11 = rel12 = 'arete_orthogonale'
          }
          if (jh === 6) {
            ok6 = false
            rel9 = rel10 = rel11 = rel12 = 'arete_orthogonale'
            rel1 = rel3 = rel7 = rel5 = 'arete_orthogonale'
          }
          if (jh === 7) {
            ok7 = false
            rel2 = rel4 = rel6 = rel8 = 'arete_orthogonale'
            rel9 = rel10 = rel11 = rel12 = 'arete_orthogonale'
          }
          if (jh === 8) {
            ok8 = false
            rel9 = rel10 = rel11 = rel12 = 'arete_orthogonale'
            rel1 = rel3 = rel7 = rel5 = 'arete_orthogonale'
          }
          if (jh === 9) {
            ok9 = false
            rel2 = rel4 = rel6 = rel8 = 'arete_orthogonale'
            rel1 = rel3 = rel7 = rel5 = 'arete_orthogonale'
          }
          if (jh === 10) {
            ok10 = false
            rel2 = rel4 = rel6 = rel8 = 'arete_orthogonale'
            rel1 = rel3 = rel7 = rel5 = 'arete_orthogonale'
          }
          if (jh === 11) {
            ok11 = false
            rel2 = rel4 = rel6 = rel8 = 'arete_orthogonale'
            rel1 = rel3 = rel7 = rel5 = 'arete_orthogonale'
          }
          if (jh === 12) {
            ok12 = false
            rel2 = rel4 = rel6 = rel8 = 'arete_orthogonale'
            rel1 = rel3 = rel7 = rel5 = 'arete_orthogonale'
          }
        }
        if (stor.element === 'arete_perpendiculaire') {
          jh = j3pGetRandomInt(1, 12)
          if (jh === 1) {
            ok1 = false
            rel2 = rel4 = rel9 = rel10 = 'arete_perpendiculaire'
          }
          if (jh === 2) {
            ok2 = false
            rel1 = rel3 = rel11 = rel10 = 'arete_perpendiculaire'
          }
          if (jh === 3) {
            ok3 = false
            rel2 = rel4 = rel11 = rel12 = 'arete_perpendiculaire'
          }
          if (jh === 4) {
            ok4 = false
            rel1 = rel3 = rel9 = rel12 = 'arete_perpendiculaire'
          }
          if (jh === 5) {
            ok5 = false
            rel8 = rel6 = rel9 = rel10 = 'arete_perpendiculaire'
          }
          if (jh === 6) {
            ok6 = false
            rel5 = rel7 = rel11 = rel10 = 'arete_perpendiculaire'
          }
          if (jh === 7) {
            ok7 = false
            rel12 = rel11 = rel6 = rel8 = 'arete_perpendiculaire'
          }
          if (jh === 8) {
            ok8 = false
            rel5 = rel7 = rel9 = rel12 = 'arete_perpendiculaire'
          }
          if (jh === 9) {
            ok9 = false
            rel8 = rel4 = rel1 = rel5 = 'arete_perpendiculaire'
          }
          if (jh === 10) {
            ok10 = false
            rel2 = rel1 = rel5 = rel6 = 'arete_perpendiculaire'
          }
          if (jh === 11) {
            ok11 = false
            rel2 = rel3 = rel6 = rel7 = 'arete_perpendiculaire'
          }
          if (jh === 12) {
            ok12 = false
            rel3 = rel4 = rel7 = rel8 = 'arete_perpendiculaire'
          }
        }

        if (ok1) {
          this.listedeselemedessine.push({
            type: 'arete',
            nom: lalettre,
            val: ['arete', rel1],
            tag: '#cseg1',
            tagAllume: ['#seg1'],
            couleur: [0, 0, 0],
            ep: 3
          })
        } else { stor.listeChange.push({ id: '#seg1', co: coMod, ep: 4 }) }
        if (ok2) {
          this.listedeselemedessine.push({
            type: 'arete',
            nom: lalettre,
            val: ['arete', rel2],
            tag: '#cseg2',
            tagAllume: ['#seg2'],
            couleur: [0, 0, 0],
            ep: 3
          })
        } else { stor.listeChange.push({ id: '#seg2', co: coMod, ep: 4 }) }
        if (ok3) {
          this.listedeselemedessine.push({
            type: 'arete',
            nom: lalettre,
            val: ['arete', rel3],
            tag: '#cseg3',
            tagAllume: ['#seg3'],
            couleur: [0, 0, 0],
            ep: 3
          })
        } else { stor.listeChange.push({ id: '#seg3', co: coMod, ep: 4 }) }
        if (ok4) {
          this.listedeselemedessine.push({
            type: 'arete',
            nom: lalettre,
            val: ['arete', rel4],
            tag: '#cseg4',
            tagAllume: ['#seg4'],
            couleur: [0, 0, 0],
            ep: 3
          })
        } else { stor.listeChange.push({ id: '#seg4', co: coMod, ep: 4 }) }
        if (ok5) {
          this.listedeselemedessine.push({
            type: 'arete',
            nom: lalettre,
            val: ['arete', 'arete_cachee', rel5],
            tag: '#cseg5',
            tagAllume: ['#seg5'],
            couleur: [0, 0, 0],
            ep: 3,
            st: 1
          })
        } else { stor.listeChange.push({ id: '#seg5', co: coMod, ep: 4, st: 1 }) }
        if (ok6) {
          this.listedeselemedessine.push({
            type: 'arete',
            nom: lalettre,
            val: ['arete', 'arete_cachee', rel6],
            tag: '#cseg6',
            tagAllume: ['#seg6'],
            couleur: [0, 0, 0],
            ep: 3,
            st: 1
          })
        } else { stor.listeChange.push({ id: '#seg6', co: coMod, ep: 4, st: 1 }) }
        if (ok7) {
          this.listedeselemedessine.push({
            type: 'arete',
            nom: lalettre,
            val: ['arete', rel7],
            tag: '#cseg7',
            tagAllume: ['#seg7'],
            couleur: [0, 0, 0],
            ep: 3
          })
        } else { stor.listeChange.push({ id: '#seg7', co: coMod, ep: 4 }) }
        if (ok8) {
          this.listedeselemedessine.push({
            type: 'arete',
            nom: lalettre,
            val: ['arete', rel8],
            tag: '#cseg8',
            tagAllume: ['#seg8'],
            couleur: [0, 0, 0],
            ep: 3
          })
        } else { stor.listeChange.push({ id: '#seg8', co: coMod, ep: 4 }) }
        if (ok9) {
          this.listedeselemedessine.push({
            type: 'arete',
            nom: lalettre,
            val: ['arete', rel9],
            tag: '#cseg9',
            tagAllume: ['#seg9'],
            couleur: [0, 0, 0],
            ep: 3
          })
        } else { stor.listeChange.push({ id: '#seg9', co: coMod, ep: 4 }) }
        if (ok10) {
          this.listedeselemedessine.push({
            type: 'arete',
            nom: lalettre,
            val: ['arete', rel10, 'arete_cachee'],
            tag: '#cseg10',
            tagAllume: ['#seg10'],
            couleur: [0, 0, 0],
            ep: 3,
            st: 1
          })
        } else { stor.listeChange.push({ id: '#seg10', co: coMod, ep: 4, st: 1 }) }
        if (ok11) {
          this.listedeselemedessine.push({
            type: 'arete',
            nom: lalettre,
            val: ['arete', rel11],
            tag: '#cseg11',
            tagAllume: ['#seg11'],
            couleur: [0, 0, 0],
            ep: 3
          })
        } else { stor.listeChange.push({ id: '#seg11', co: coMod, ep: 4 }) }
        if (ok12) {
          this.listedeselemedessine.push({
            type: 'arete',
            nom: lalettre,
            val: ['arete', rel12],
            tag: '#cseg12',
            tagAllume: ['#seg12'],
            couleur: [0, 0, 0],
            ep: 3
          })
        } else { stor.listeChange.push({ id: '#seg12', co: coMod, ep: 4 }) }

        break
      case 'PRISME':
        stor.height = stor.lwidth = 400
        A1 = j3pGetRandomInt(1, 360)
        A2 = j3pGetRandomInt(0, 10)
        A3 = j3pGetRandomInt(0, 10)

        // 8 sommets
        lalettre1 = addMaj(nomspris)
        lalettre2 = addMaj(nomspris)
        lalettre5 = addMaj(nomspris)
        lalettre6 = addMaj(nomspris)
        lalettre7 = addMaj(nomspris)
        lalettre8 = addMaj(nomspris)

        stor.listZig = ['#cseg1', '#cseg2', '#cseg3', '#cseg4', '#cseg5', '#cseg6', '#cseg7', '#cseg8', '#cseg9', '#ccA', '#ccB', '#ccE', '#ccF', '#ccG', '#ccH']

        // 8 sommets
        this.listedeselemedessine.push({
          type: 'point',
          nom: lalettre1,
          val: ['point', 'sommet'],
          tag: '#ccA',
          tagAllume: ['#sA'],
          couleur: [255, 255, 255],
          ep: false
        })
        listeNom.push({ t: '#npA', nom: lalettre1 })
        this.listedeselemedessine.push({
          type: 'point',
          nom: lalettre2,
          val: ['point', 'sommet'],
          tag: '#ccB',
          tagAllume: ['#sB'],
          couleur: [255, 255, 255],
          ep: false
        })
        listeNom.push({ t: '#npB', nom: lalettre2 })
        this.listedeselemedessine.push({
          type: 'point',
          nom: lalettre5,
          val: ['point', 'sommet'],
          tag: '#ccE',
          tagAllume: ['#sE'],
          couleur: [255, 255, 255],
          ep: false
        })
        listeNom.push({ t: '#npE', nom: lalettre5 })
        this.listedeselemedessine.push({
          type: 'point',
          nom: lalettre6,
          val: ['point', 'sommet'],
          tag: '#ccF',
          tagAllume: ['#sF'],
          couleur: [255, 255, 255],
          ep: false
        })
        listeNom.push({ t: '#npF', nom: lalettre6 })
        this.listedeselemedessine.push({
          type: 'point',
          nom: lalettre7,
          val: ['point', 'sommet'],
          tag: '#ccG',
          tagAllume: ['#sG'],
          couleur: [255, 255, 255],
          ep: false
        })
        listeNom.push({ t: '#npG', nom: lalettre7 })
        this.listedeselemedessine.push({
          type: 'point',
          nom: lalettre8,
          val: ['point', 'sommet'],
          tag: '#ccH',
          tagAllume: ['#sH'],
          couleur: [255, 255, 255],
          ep: false
        })
        listeNom.push({ t: '#npH', nom: lalettre8 })

        ok1 = ok2 = ok3 = ok4 = ok5 = true
        typeface2 = typeface1 = 'piege'
        if (stor.element === 'face_parallele') {
          ok5 = (j3pGetRandomInt(0, 1) === 0)
          ok1 = !ok5
        }
        if (stor.element === 'face_perpendiculaire') {
          if (j3pGetRandomInt(0, 1) === 0) {
            typeface1 = 'face_perpendiculaire'
            jh = j3pGetRandomInt(0, 2)
            if (jh === 0) {
              ok4 = false
            }
            if (jh === 1) {
              ok2 = false
            }
            if (jh === 2) {
              ok3 = false
            }
          } else {
            typeface2 = 'face_perpendiculaire'
            if (j3pGetRandomInt(0, 1) === 0) {
              ok5 = false
            } else {
              ok1 = false
            }
          }
        }

        // face gauche
        if (ok2) {
          this.listedeselemedessine.push({
            type: 'face',
            nom: 'pol1',
            val: ['face', 'face_cachee', 'face_laterale', typeface2],
            tag: '#pog5',
            tagAllume: ['#surfgauche'],
            tagspe: '#sog5',
            couleur: [255, 255, 255],
            coolspe: specool,
            ep: 1
          })
        } else {
          stor.listeChange.push({ id: '#surfgauche', co: coMod })
          stor.listCache.push('#pog1', '#pog2', '#pog3', '#pog4', '#pog5', '#sog1', '#sog2', '#sog3', '#sog4', '#sog5')
        }
        // face arriere
        if (ok5) {
          this.listedeselemedessine.push({
            type: 'face',
            nom: 'pof1',
            val: ['face', 'face_cachee', 'base', 'face_parallele', typeface1],
            tag: '#pof6',
            tagAllume: ['#surfarriere'],
            tagspe: '#sof5',
            couleur: [255, 255, 255],
            coolspe: specool,
            ep: 1
          })
        } else {
          stor.listeChange.push({ id: '#surfarriere', co: coMod })
          stor.listCache.push('#pof1', '#pof2', '#pof3', '#pof4', '#pof6', '#sof1', '#sof2', '#sof3', '#sof4', '#sof5', '#sof6')
        }
        // face dessous
        if (ok4) {
          this.listedeselemedessine.push({
            type: 'face',
            nom: 'pof1',
            val: ['face', 'face_cachee', 'face_laterale', typeface2],
            tag: '#poa5',
            tagAllume: ['#surfbas'],
            tagspe: '#soa5',
            couleur: [255, 255, 255],
            coolspe: specool,
            ep: 1
          })
        } else {
          stor.listeChange.push({ id: '#surfbas', co: coMod })
          stor.listCache.push('#poa1', '#poa2', '#poa3', '#poa4', '#poa5', '#soa1', '#soa2', '#soa3', '#soa4', '#soa5')
        }
        // face devant
        if (ok1) {
          this.listedeselemedessine.push({
            type: 'face',
            nom: 'pof1',
            val: ['face', 'base', 'face_parallele', typeface1],
            tag: '#pob5',
            tagAllume: ['#surfavant'],
            tagspe: '#sob5',
            couleur: [255, 255, 255],
            coolspe: specool,
            ep: 1
          })
        } else {
          stor.listeChange.push({ id: '#surfavant', co: coMod })
          stor.listCache.push('#pob1', '#pob2', '#pob3', '#pob4', '#pob5', '#sob1', '#sob2', '#sob3', '#sob4', '#sob5')
        }
        // face droite
        if (ok3) {
          this.listedeselemedessine.push({
            type: 'face',
            nom: 'pogsdfs1',
            val: ['face', 'face_laterale', typeface2],
            tag: '#pod5',
            tagAllume: ['#surfdroite'],
            tagspe: '#sod5',
            couleur: [255, 255, 255],
            coolspe: specool,
            ep: 1
          })
        } else {
          stor.listeChange.push({ id: '#surfdroite', co: coMod })
          stor.listCache.push('#pod1', '#pod2', '#pod3', '#pod4', '#pod5', '#sod1', '#sod2', '#sod3', '#sod4', '#sod5')
        }

        ok1 = ok2 = ok3 = ok4 = ok5 = ok6 = ok7 = ok8 = ok9 = true

        if (stor.element === 'arete_parallele') {
          jh = j3pGetRandomInt(1, 9)
          if (jh === 1) {
            ok1 = false
            rel4 = 'arete_parallele'
          }
          if (jh === 2) {
            ok2 = false
            rel5 = 'arete_parallele'
          }
          if (jh === 3) {
            ok3 = false
            rel6 = 'arete_parallele'
          }
          if (jh === 4) {
            ok4 = false
            rel1 = 'arete_parallele'
          }
          if (jh === 5) {
            ok5 = false
            rel2 = 'arete_parallele'
          }
          if (jh === 6) {
            ok6 = false
            rel3 = 'arete_parallele'
          }
          if (jh === 7) {
            ok7 = false
            rel8 = rel9 = 'arete_parallele'
          }
          if (jh === 8) {
            ok8 = false
            rel7 = rel9 = 'arete_parallele'
          }
          if (jh === 9) {
            ok9 = false
            rel8 = rel7 = 'arete_parallele'
          }
        }
        if (stor.element === 'longueur_egale') {
          jh = j3pGetRandomInt(1, 9)
          if (jh === 1) {
            ok1 = false
            rel4 = 'longueur_egale'
          }
          if (jh === 2) {
            ok2 = false
            rel5 = 'longueur_egale'
          }
          if (jh === 3) {
            ok3 = false
            rel6 = 'longueur_egale'
          }
          if (jh === 4) {
            ok4 = false
            rel1 = 'longueur_egale'
          }
          if (jh === 5) {
            ok5 = false
            rel2 = 'longueur_egale'
          }
          if (jh === 6) {
            ok6 = false
            rel3 = 'longueur_egale'
          }
          if (jh === 7) {
            ok7 = false
            rel8 = rel9 = 'longueur_egale'
          }
          if (jh === 8) {
            ok8 = false
            rel7 = rel9 = 'longueur_egale'
          }
          if (jh === 9) {
            ok9 = false
            rel8 = rel7 = 'longueur_egale'
          }
        }
        if (stor.element === 'arete_orthogonale') {
          jh = j3pGetRandomInt(1, 9)
          if (jh === 1) {
            ok1 = false
            rel8 = rel7 = rel9 = 'arete_orthogonale'
          }
          if (jh === 2) {
            ok2 = false
            rel8 = rel7 = rel9 = 'arete_orthogonale'
          }
          if (jh === 3) {
            ok3 = false
            rel8 = rel7 = rel9 = 'arete_orthogonale'
          }
          if (jh === 4) {
            ok4 = false
            rel8 = rel7 = rel9 = 'arete_orthogonale'
          }
          if (jh === 5) {
            ok5 = false
            rel8 = rel7 = rel9 = 'arete_orthogonale'
          }
          if (jh === 6) {
            ok6 = false
            rel8 = rel7 = rel9 = 'arete_orthogonale'
          }
          if (jh === 7) {
            ok7 = false
            rel1 = rel2 = rel3 = rel4 = rel5 = rel6 = 'arete_orthogonale'
          }
          if (jh === 8) {
            ok8 = false
            rel1 = rel2 = rel3 = rel4 = rel5 = rel6 = 'arete_orthogonale'
          }
          if (jh === 9) {
            ok9 = false
            rel1 = rel2 = rel3 = rel4 = rel5 = rel6 = 'arete_orthogonale'
          }
        }
        if (stor.element === 'arete_perpendiculaire') {
          jh = j3pGetRandomInt(1, 9)
          if (jh === 1) {
            ok1 = false
            rel8 = rel7 = 'arete_perpendiculaire'
          }
          if (jh === 2) {
            ok2 = false
            rel8 = rel9 = 'arete_perpendiculaire'
          }
          if (jh === 3) {
            ok3 = false
            rel7 = rel9 = 'arete_perpendiculaire'
          }
          if (jh === 4) {
            ok4 = false
            rel8 = rel7 = 'arete_perpendiculaire'
          }
          if (jh === 5) {
            ok5 = false
            rel8 = rel9 = 'arete_perpendiculaire'
          }
          if (jh === 6) {
            ok6 = false
            rel7 = rel9 = 'arete_perpendiculaire'
          }
          if (jh === 7) {
            ok7 = false
            rel1 = rel3 = rel4 = rel6 = 'arete_perpendiculaire'
          }
          if (jh === 8) {
            ok8 = false
            rel1 = rel2 = rel4 = rel5 = 'arete_perpendiculaire'
          }
          if (jh === 9) {
            ok9 = false
            rel2 = rel3 = rel5 = rel6 = 'arete_perpendiculaire'
          }
        }

        if (ok1) {
          this.listedeselemedessine.push({
            type: 'arete',
            val: ['arete', rel1],
            tag: '#cseg1',
            tagAllume: ['#seg1'],
            couleur: [0, 0, 0],
            ep: 3
          })
        } else { stor.listeChange.push({ id: '#seg1', co: coMod, ep: 4 }) }
        if (ok2) {
          this.listedeselemedessine.push({
            type: 'arete',
            val: ['arete', rel2],
            tag: '#cseg2',
            tagAllume: ['#seg2'],
            couleur: [0, 0, 0],
            ep: 3
          })
        } else { stor.listeChange.push({ id: '#seg2', co: coMod, ep: 4 }) }
        if (ok3) {
          this.listedeselemedessine.push({
            type: 'arete',
            val: ['arete', rel3],
            tag: '#cseg3',
            tagAllume: ['#seg3'],
            couleur: [0, 0, 0],
            ep: 3
          })
        } else { stor.listeChange.push({ id: '#seg3', co: coMod, ep: 4 }) }
        if (ok4) {
          this.listedeselemedessine.push({
            type: 'arete',
            val: ['arete', 'arete_cachee', rel4],
            tag: '#cseg4',
            tagAllume: ['#seg4'],
            couleur: [0, 0, 0],
            ep: 3,
            st: 1
          })
        } else { stor.listeChange.push({ id: '#seg4', co: coMod, ep: 4, st: 1 }) }
        if (ok5) {
          this.listedeselemedessine.push({
            type: 'arete',
            val: ['arete', rel5],
            tag: '#cseg5',
            tagAllume: ['#seg5'],
            couleur: [0, 0, 0],
            ep: 3
          })
        } else { stor.listeChange.push({ id: '#seg5', co: coMod, ep: 4 }) }
        if (ok6) {
          this.listedeselemedessine.push({
            type: 'arete',
            val: ['arete', 'arete_cachee', rel6],
            tag: '#cseg6',
            tagAllume: ['#seg6'],
            couleur: [0, 0, 0],
            ep: 3,
            st: 1
          })
        } else { stor.listeChange.push({ id: '#seg6', co: coMod, ep: 4, st: 1 }) }
        if (ok7) {
          this.listedeselemedessine.push({
            type: 'arete',
            val: ['arete', 'arete_cachee', 'arete_laterale', 'hauteur', rel7],
            tag: '#cseg7',
            tagAllume: ['#seg7'],
            couleur: [0, 0, 0],
            ep: 3,
            st: 1
          })
        } else { stor.listeChange.push({ id: '#seg7', co: coMod, ep: 4, st: 1 }) }
        if (ok8) {
          this.listedeselemedessine.push({
            type: 'arete',
            val: ['arete', 'arete_laterale', 'hauteur', rel8],
            tag: '#cseg8',
            tagAllume: ['#seg8'],
            couleur: [0, 0, 0],
            ep: 3
          })
        } else { stor.listeChange.push({ id: '#seg8', co: coMod, ep: 4 }) }
        if (ok9) {
          this.listedeselemedessine.push({
            type: 'arete',
            nom: lalettre,
            val: ['arete', 'arete_laterale', 'hauteur', rel9],
            tag: '#cseg9',
            tagAllume: ['#seg9'],
            couleur: [0, 0, 0],
            ep: 3
          })
        } else { stor.listeChange.push({ id: '#seg9', co: coMod, ep: 4 }) }
        break
      case 'CYLINDRE': // det les 4 points
        A1 = j3pGetRandomInt(0, 360)
        do {
          A2 = j3pGetRandomInt(85, 110)
        } while (Math.abs(A2 - 90) < 5)
        A3 = j3pGetRandomInt(90, 190)
        A4 = j3pGetRandomInt(6, 7)
        stor.height = 400
        stor.lwidth = 430
        stor.listZig = ['#cseg5', '#cseg6', '#cseg7', '#cray1', '#cray2', '#cray3', '#cray4', '#cdiam', '#chauteur1', '#ccM', '#ccH', '#ccX', '#ccB', '#ccG']

        ok1 = ok2 = true
        if (stor.element === 'face_parallele') {
          ok1 = (j3pGetRandomInt(0, 1) === 0)
          ok2 = !ok1
        }
        // les deux ellipses
        if (ok1) {
          this.listedeselemedessine.push({
            type: 'face',
            nom: 'pol1',
            val: ['face', 'base', 'face_parallele'],
            tag: '#pob',
            tagAllume: ['#base2'],
            tagspe: '#sob',
            couleur: [255, 255, 255],
            coolspe: specool,
            ep: 1
          })
        } else {
          stor.listCache = ['#pob1', '#pob2', '#pob3', '#pob4', '#pob', '#sob1', '#sob2', '#sob3', '#sob4', '#sob']
          stor.listeChange.push({ id: '#base2', co: coMod })
        }
        if (ok2) {
          this.listedeselemedessine.push({
            type: 'face',
            nom: 'polh',
            val: ['face', 'base', 'face_parallele'],
            tag: '#poh',
            tagAllume: ['#base1'],
            tagspe: '#soh',
            couleur: [255, 255, 255],
            coolspe: specool,
            ep: 1
          })
        } else {
          stor.listCache = ['#poh1', '#poh2', '#poh3', '#poh4', '#poh', '#soh1', '#soh2', '#soh3', '#soh4', '#soh', '#soh5']
          stor.listeChange.push({ id: '#base1', co: coMod })
        }

        // la lat
        this.listedeselemedessine.push({
          type: 'face',
          nom: 'polh',
          val: ['face', 'face_laterale'],
          tag: '#pol',
          tagAllume: ['#lat1', '#lat2'],
          tagspe: '#sol',
          couleur: [255, 255, 255],
          coolspe: specool,
          ep: 1
        })

        // les deux 'cotes
        lettre2 = addMaj(nomspris)
        this.listedeselemedessine.push({
          type: 'point',
          nom: lettre2,
          val: ['piege'],
          tag: '#ccX',
          tagAllume: ['#sX'],
          couleur: [255, 255, 255],
          ep: false
        })
        lettre3 = addMaj(nomspris)
        this.listedeselemedessine.push({
          type: 'point',
          nom: lettre3,
          val: ['piege'],
          tag: '#ccH',
          tagAllume: ['#sH'],
          couleur: [255, 255, 255],
          ep: false
        })
        lettre4 = addMaj(nomspris)
        this.listedeselemedessine.push({
          type: 'point',
          nom: lettre4,
          val: ['point', 'centre'],
          tag: '#ccB',
          tagAllume: ['#sB'],
          couleur: [255, 255, 255],
          ep: false
        })
        lettre1 = addMaj(nomspris)
        this.listedeselemedessine.push({
          type: 'point',
          nom: lettre1,
          val: ['point', 'centre'],
          tag: '#ccM',
          tagAllume: ['#sM'],
          couleur: [255, 255, 255],
          ep: false
        })
        lettre5 = addMaj(nomspris)
        this.listedeselemedessine.push({
          type: 'point',
          nom: lettre5,
          val: ['point', 'piege'],
          tag: '#ccG',
          tagAllume: ['#sG'],
          couleur: [255, 255, 255],
          ep: false
        })
        listeNom = [{ t: '#npX', nom: lettre2 }, { t: '#npH', nom: lettre3 }, { t: '#npM', nom: lettre1 }, {
          t: '#npB',
          nom: lettre4
        }, { t: '#npG', nom: lettre5 }]

        ok1 = ok2 = ok3 = ok4 = ok5 = ok6 = ok7 = true
        rel1 = rel2 = 'piege'
        if (stor.element === 'longueur_egale') {
          haz = j3pGetRandomInt(0, 6)
          if (haz === 0) {
            ok1 = false
            rel1 = 'longueur-egale'
          }
          if (haz === 1) {
            ok2 = false
            rel1 = 'longueur-egale'
          }
          if (haz === 2) {
            ok3 = false
            rel1 = 'longueur-egale'
          }
          if (haz === 3) {
            ok4 = false
            rel1 = 'longueur-egale'
          }
          if (haz === 4) {
            ok5 = false
            rel2 = 'longueur-egale'
          }
          if (haz === 5) {
            ok6 = false
            rel2 = 'longueur-egale'
          }
          if (haz === 6) {
            ok7 = false
            rel2 = 'longueur-egale'
          }
        }
        // le diam du haut et un rayon du haut
        if (stor.element === 'diametre') {
          stor.listCache.push('#ray1')
          stor.listCache.push('#ray2')
          stor.listCache.push('#cray1')
          stor.listCache.push('#cray2')
          this.listedeselemedessine.push({
            type: 'segment',
            nom: lettre5 + 'm4',
            val: ['segment', 'diametre'],
            tag: '#cdiam',
            tagAllume: ['#diam'],
            couleur: [0, 0, 0],
            ep: 3
          })
        } else {
          stor.listCache.push('#diam')
          stor.listCache.push('#cdiam')
          if (ok1) {
            this.listedeselemedessine.push({
              type: 'segment',
              nom: lettre5 + 'm4',
              val: ['segment', 'rayon', rel1],
              tag: '#cray1',
              tagAllume: ['#ray1'],
              couleur: [0, 0, 0],
              ep: 3
            })
          } else { stor.listeChange.push({ id: '#ray1', ep: 4, co: coMod }) }
          if (ok2) {
            this.listedeselemedessine.push({
              type: 'segment',
              nom: lettre5 + 'm4',
              val: ['segment', 'rayon', rel1],
              tag: '#cray2',
              tagAllume: ['#ray2'],
              couleur: [0, 0, 0],
              ep: 3
            })
          } else { stor.listeChange.push({ id: '#ray2', ep: 4, co: coMod }) }
        }
        if (ok3) {
          this.listedeselemedessine.push({
            type: 'segment',
            nom: lettre5 + 'm4',
            val: ['segment', 'rayon', rel1],
            tag: '#cray3',
            tagAllume: ['#ray3'],
            couleur: [0, 0, 0],
            ep: 3
          })
        } else { stor.listeChange.push({ id: '#ray3', ep: 4, co: coMod }) }
        if (ok4) {
          this.listedeselemedessine.push({
            type: 'segment',
            nom: lettre5 + 'm4',
            val: ['segment', 'rayon', rel1],
            tag: '#cray4',
            tagAllume: ['#ray4'],
            couleur: [0, 0, 0],
            ep: 3,
            st: 2
          })
        } else { stor.listeChange.push({ id: '#ray4', ep: 4, co: coMod, st: 2 }) }
        if (ok5) {
          this.listedeselemedessine.push({
            type: 'segment',
            nom: lettre5 + 'm4',
            val: ['segment', rel2],
            tag: '#cseg5',
            tagAllume: ['#seg5'],
            couleur: [0, 0, 0],
            ep: 3,
            st: 2
          })
        } else { stor.listeChange.push({ id: '#seg5', ep: 4, co: coMod, st: 2 }) }
        if (ok6) {
          this.listedeselemedessine.push({
            type: 'segment',
            nom: lettre5 + 'm4',
            val: ['segment', rel2],
            tag: '#cseg6',
            tagAllume: ['#seg6'],
            couleur: [0, 0, 0],
            ep: 3
          })
        } else { stor.listeChange.push({ id: '#seg6', ep: 4, co: coMod }) }
        if (ok7) {
          this.listedeselemedessine.push({
            type: 'segment',
            nom: lettre5 + 'm4',
            val: ['segment', rel2],
            tag: '#cseg7',
            tagAllume: ['#seg7'],
            couleur: [0, 0, 0],
            ep: 3
          })
        } else { stor.listeChange.push({ id: '#seg7', ep: 4, co: coMod }) }
        // la hauteur
        this.listedeselemedessine.push({
          type: 'segment',
          nom: 'a' + lettre5,
          val: ['segment', 'hauteur'],
          tag: '#chauteur1',
          tagAllume: ['#hauteur1'],
          couleur: [0, 0, 0],
          ep: 3,
          st: 2,
          serieColle: 1
        })
        this.listedeselemedessine.push({
          type: 'segment',
          nom: 'a' + lettre5,
          val: ['segment', 'hauteur'],
          tag: '#chauteur2',
          tagAllume: ['#hauteur2'],
          couleur: [0, 0, 0],
          ep: 3,
          serieColle: 1
        })
        break
      default :
        j3pShowError(Error(`Cas pas encore géré (${stor.contenant})`), {
          message: 'Erreur interne (cas non prévu)',
          mustNotify: true
        })
    }

    j3pCreeSVG(stor.lesdiv.figure, {
      id: svgId,
      width: stor.lwidth,
      height: stor.height,
      style: { border: 'solid black 1px' }
    })
    this.mtgAppLecteur.removeAllDoc()
    this.mtgAppLecteur.addDoc(svgId, stor.lafig, true)
    this.mtgAppLecteur.giveFormula2(svgId, 'A1', String(A1))
    this.mtgAppLecteur.giveFormula2(svgId, 'A2', String(A2))
    this.mtgAppLecteur.giveFormula2(svgId, 'A3', String(A3))
    this.mtgAppLecteur.giveFormula2(svgId, 'A4', String(A4))
    this.mtgAppLecteur.calculateAndDisplayAll(true)
    for (let ju = 0; ju < listeNom.length; ju++) {
      this.mtgAppLecteur.setText(svgId, listeNom[ju].t, listeNom[ju].nom, true)
    }
    for (let ju = 0; ju < stor.listeChange.length; ju++) {
      const col = stor.listeChange[ju].co
      const st = stor.listeChange[ju].st || 0
      this.mtgAppLecteur.setColor(svgId, stor.listeChange[ju].id, col[0], col[1], col[2], true)
      this.mtgAppLecteur.setLineStyle(svgId, stor.listeChange[ju].id, st, stor.listeChange[ju].ep, true)
    }
    for (let ju = 0; ju < stor.listCache.length; ju++) {
      this.mtgAppLecteur.setVisible(svgId, stor.listCache[ju], false, true)
    }
    for (let ju = 0; ju < stor.listZig.length; ju++) {
      this.mtgAppLecteur.setColor(svgId, stor.listZig[ju], 0, 0, 0, true, 0)
    }

    this.mtgAppLecteur.updateFigure(svgId)
    if (stor.contenant === 'CONE_REVOLUTION' || stor.contenant === 'CONE') {
      const p1 = this.mtgAppLecteur.getPointPosition(svgId, '#POINT1')
      const p2 = this.mtgAppLecteur.getPointPosition(svgId, '#POINT2')
      const p3 = this.mtgAppLecteur.getPointPosition(svgId, '#POINT3')
      const p4 = this.mtgAppLecteur.getPointPosition(svgId, '#POINT4')
      const pl = this.mtgAppLecteur.getPointPosition(svgId, '#POINTL')
      let xmin = Math.min(Math.min(Math.min(p1.x, p3.x), p2.x), p4.x)
      let ymin = Math.min(Math.min(Math.min(p1.y, p3.y), p2.y), p4.y)
      let xmax = Math.max(Math.max(Math.max(p1.x, p3.x), p2.x), p4.x)
      let ymax = Math.max(Math.max(Math.max(p1.y, p3.y), p2.y), p4.y)
      if (stor.contenant === 'CONE') {
        const p5 = this.mtgAppLecteur.getPointPosition(svgId, '#POINT5')
        xmin = Math.min(xmin, p5.x)
        xmax = Math.max(xmax, p5.x)
        ymin = Math.min(ymin, p5.y)
        ymax = Math.max(ymax, p5.y)
      }
      let depx = 0
      let depy = 0
      if (xmin < 10) depx = 10 - xmin
      if (xmax > 370) depx = 370 - xmax
      if (ymin < 10) depy = 10 - ymin
      if (ymax > 390) depy = 390 - ymax
      this.mtgAppLecteur.setPointPosition(svgId, '#POINTL', pl.x + depx, pl.y + depy, true)
    }
    if (stor.contenant === 'PYRAMIDE') {
      let cpt = 0
      while (!this.verifPyr() && cpt < 100) {
        cpt++
        A3 = j3pGetRandomInt(0, 180)
        this.mtgAppLecteur.giveFormula2(svgId, 'A3', String(A3))
        this.mtgAppLecteur.calculateAndDisplayAll(true)
      }
      const p1 = this.mtgAppLecteur.getPointPosition(svgId, '#pv1')
      const p2 = this.mtgAppLecteur.getPointPosition(svgId, '#pv2')
      const p3 = this.mtgAppLecteur.getPointPosition(svgId, '#pv3')
      const p4 = this.mtgAppLecteur.getPointPosition(svgId, '#pv4')
      const p5 = this.mtgAppLecteur.getPointPosition(svgId, '#pv5')
      const bari = { x: (p1.x + p2.x + p3.x + p4.x + p5.x) / 5, y: (p1.y + p2.y + p3.y + p4.y + p5.y) / 5 }
      const pl1 = this.mtgAppLecteur.getPointPosition(svgId, '#POINT1')
      const pl2 = this.mtgAppLecteur.getPointPosition(svgId, '#POINT2')

      const pmil = this.mtgAppLecteur.getPointPosition(svgId, '#PMIL')
      const depx = pmil.x - bari.x
      const depy = pmil.y - bari.y
      this.mtgAppLecteur.setPointPosition(svgId, '#POINT1', pl1.x + depx, pl1.y + depy, true)
      this.mtgAppLecteur.setPointPosition(svgId, '#POINT2', pl2.x + depx, pl2.y + depy, true)

      const ph1 = this.mtgAppLecteur.valueOf(svgId, 'ph1')
      const ph2 = this.mtgAppLecteur.valueOf(svgId, 'ph2')
      const ph3 = this.mtgAppLecteur.valueOf(svgId, 'ph3')
      if (ph1 + ph2 < ph3 + 0.000001) {
        this.listedeselemedessine[18].st = 2
        this.mtgAppLecteur.setLineStyle(svgId, '#hauteur', 2, 3, true)
      }
    }
    if (stor.contenant === 'CYLINDRE') {
      const p1 = this.mtgAppLecteur.getPointPosition(svgId, '#POINT1')
      const p2 = this.mtgAppLecteur.getPointPosition(svgId, '#POINT2')
      const p3 = this.mtgAppLecteur.getPointPosition(svgId, '#POINT3')
      const p4 = this.mtgAppLecteur.getPointPosition(svgId, '#POINT4')
      const p5 = this.mtgAppLecteur.getPointPosition(svgId, '#POINT5')
      const bari = { x: (p1.x + p2.x + p3.x + p4.x + p5.x) / 5, y: (p1.y + p2.y + p3.y + p4.y + p5.y) / 5 }
      const pmil = this.mtgAppLecteur.getPointPosition(svgId, '#POINTL')
      const depx = stor.lwidth / 2 - bari.x
      const depy = stor.height / 2 - bari.y
      this.mtgAppLecteur.setPointPosition(svgId, '#POINTL', pmil.x + depx, pmil.y + depy, true)
    }
    if (stor.contenant === 'CYLINDRE_REVOLUTION') {
      const p1 = this.mtgAppLecteur.getPointPosition(svgId, '#POINT1')
      const p2 = this.mtgAppLecteur.getPointPosition(svgId, '#POINT2')
      const p3 = this.mtgAppLecteur.getPointPosition(svgId, '#POINT3')
      const p4 = this.mtgAppLecteur.getPointPosition(svgId, '#POINT4')
      const bari = { x: (p1.x + p2.x + p3.x + p4.x) / 4, y: (p1.y + p2.y + p3.y + p4.y) / 4 }
      const pmil = this.mtgAppLecteur.getPointPosition(svgId, '#POINTL1')
      const pmil2 = this.mtgAppLecteur.getPointPosition(svgId, '#POINTL2')
      const depx = stor.lwidth / 2 - bari.x
      const depy = stor.height / 2 - bari.y
      this.mtgAppLecteur.setPointPosition(svgId, '#POINTL1', pmil.x + depx, pmil.y + depy, true)
      this.mtgAppLecteur.setPointPosition(svgId, '#POINTL2', pmil2.x + depx, pmil2.y + depy, true)
    }
    if (stor.contenant === 'PAVE_DROIT' || stor.contenant === 'PRISME') {
      const p1 = this.mtgAppLecteur.getPointPosition(svgId, '#POINTMIL')
      const p2 = this.mtgAppLecteur.getPointPosition(svgId, '#pointmil')
      const difx = stor.lwidth / 2 - p2.x
      const dify = stor.height / 2 - p2.y
      this.mtgAppLecteur.setPointPosition(svgId, '#POINTMIL', p1.x + difx, p1.y + dify, true)
    }

    this.faisfoncoversel()
    stor.lesdiv.figure.classList.add('travail')
  } // constructor

  corrige (num, bool) {
    let cooleur
    if (bool === undefined) {
      cooleur = '#0000FF'
    }
    if (bool === true) {
      cooleur = cbien
    }
    if (bool === false) {
      cooleur = cfaux
    }
    const elem = this.aclick[num]
    const lalacoul = this.hexToRgb(cooleur.replace('#', ''))
    for (let j = 0; j < elem.tagAllume.length; j++) {
      const opa = (elem.tagspe) ? 0.4 : 1
      this.mtgAppLecteur.setColor(this.svgId, elem.tagAllume[j], lalacoul[0], lalacoul[1], lalacoul[2], true, opa)
    }
  }

  faisfoncoversel () {
    for (let i = 0; i < this.listedeselemedessine.length; i++) {
      const newaclik = j3pClone(this.listedeselemedessine[i])
      newaclik.clicked = false
      this.aclick.push(newaclik)
      this.mtgAppLecteur.addEventListener(this.svgId, newaclik.tag, 'mouseover', this.makeFoncOver(newaclik))
      this.mtgAppLecteur.addEventListener(this.svgId, newaclik.tag, 'mouseout', this.makeFoncOut(newaclik))
      this.mtgAppLecteur.addEventListener(this.svgId, newaclik.tag, 'click', this.makeFoncSelect(newaclik))
    }
  }

  faisOutSerieColle (elem) {
    const svgId = this.svgId
    const coul = elem.couleur
    for (let i = 0; i < this.aclick.length; i++) {
      if (this.aclick[i].serieColle === elem.serieColle) {
        const st = elem.st || 0
        const ep = elem.ep || 2
        const opa = (elem.tagspe) ? 0.4 : 1
        for (const tagAllume of this.aclick[i].tagAllume) {
          if (elem.clicked) {
            this.mtgAppLecteur.setColor(svgId, tagAllume, coul[0], coul[1], coul[2], true, opa)
            this.mtgAppLecteur.setLineStyle(svgId, tagAllume, st, ep, true)
          } else {
            this.mtgAppLecteur.setColor(svgId, tagAllume, 255, 0, 0, true, opa)
            this.mtgAppLecteur.setLineStyle(svgId, tagAllume, st, ep + 1, true)
          }
        }
      }
    }
  }

  faisOverSerieColle (elem) {
    const svgId = this.svgId
    for (let i = 0; i < this.aclick.length; i++) {
      if (this.aclick[i].serieColle === elem.serieColle) {
        const st = elem.st || 0
        const opa = (elem.tagspe) ? 0.4 : 1
        if (elem.clicked) {
          for (let j = 0; j < this.aclick[i].tagAllume.length; j++) {
            this.mtgAppLecteur.setColor(svgId, this.aclick[i].tagAllume[j], 200, 50, 50, true, opa)
            this.mtgAppLecteur.setLineStyle(svgId, this.aclick[i].tagAllume[j], st, 5, true)
          }
        } else {
          for (let j = 0; j < this.aclick[i].tagAllume.length; j++) {
            this.mtgAppLecteur.setColor(svgId, this.aclick[i].tagAllume[j], 250, 0, 0, true, opa)
            this.mtgAppLecteur.setLineStyle(svgId, this.aclick[i].tagAllume[j], st, 5, true)
          }
        }
      }
    }
  }

  hexToRgb (hex) {
    const bigint = parseInt(hex, 16)
    const r = (bigint >> 16) & 255
    const g = (bigint >> 8) & 255
    const b = bigint & 255
    return [r, g, b]
  }

  listeselect () {
    const repoui = []
    const repnon = []
    for (let i = 0; i < this.aclick.length; i++) {
      if (this.aclick[i].clicked) {
        repoui.push([this.aclick[i].val, i])
      } else {
        repnon.push([this.aclick[i].val, i])
      }
    }
    return {
      oui: repoui,
      non: repnon
    }
  }

  gereSerie (elem) {
    const svgId = this.svgId
    const opa = (elem.tagspe) ? 0.4 : 1
    if (elem.contser) {
      if (elem.clicked) {
        for (let i = 0; i < this.aclick.length; i++) {
          if (this.aclick[i].serie === elem.serie) {
            this.aclick[i].clicked = true
            for (let j = 0; j < this.aclick[i].tagAllume.length; j++) {
              this.mtgAppLecteur.setColor(svgId, this.aclick[i].tagAllume[j], 255, 0, 0, true, opa)
              this.mtgAppLecteur.setLineStyle(svgId, this.aclick[i].tagAllume[j], 0, 3, true)
            }
          }
        }
      } else {
        for (let i = 0; i < this.aclick.length; i++) {
          if (this.aclick[i].serie === elem.serie) {
            this.aclick[i].clicked = false
            const coul = this.aclick[i].couleur
            for (let j = 0; j < this.aclick[i].tagAllume.length; j++) {
              this.mtgAppLecteur.setColor(svgId, this.aclick[i].tagAllume[j], coul[0], coul[1], coul[2], true, opa)
              this.mtgAppLecteur.setLineStyle(svgId, this.aclick[i].tagAllume[j], 0, 2, true)
            }
          }
        }
      }
    } else {
      for (let i = 0; i < this.aclick.length; i++) {
        if (this.aclick[i].serie === elem.serie && elem.tag !== this.aclick[i].tag) {
          if (this.aclick[i].contser) {
            if (this.aclick[i].clicked) elem.clicked = true
            this.aclick[i].clicked = false
            const coul = this.aclick[i].couleur
            for (let j = 0; j < this.aclick[i].tagAllume.length; j++) {
              const st = this.aclick[i].st || 0
              this.mtgAppLecteur.setColor(svgId, this.aclick[i].tagAllume[j], coul[0], coul[1], coul[2], true, opa)
              this.mtgAppLecteur.setLineStyle(svgId, this.aclick[i].tagAllume[j], st, 2, true)
            }
          }
        }
      }
    }
  }

  makeFoncOut (elem) {
    return () => {
      const svgId = this.svgId
      if (this.blok) return
      const coul = elem.couleur
      this.mtgAppLecteur.getDoc(svgId).defaultCursor = null
      const st = elem.st || 0
      const ep = elem.ep || 1
      const opa = (elem.tagspe) ? 0.4 : 1
      if (elem.clicked) {
        for (let j = 0; j < elem.tagAllume.length; j++) {
          if (!elem.sm) {
            this.mtgAppLecteur.setColor(svgId, elem.tagAllume[j], 255, 0, 0, true, opa)
            this.mtgAppLecteur.setLineStyle(svgId, elem.tagAllume[j], st, ep + 1, true)
          } else {
            this.mtgAppLecteur.setColor(svgId, elem.tagAllume[j].tagA, 255, 0, 0, true, opa)
            this.mtgAppLecteur.setLineStyle(svgId, elem.tagAllume[j].tagA, elem.tagAllume[j].st, elem.tagAllume[j].ep + 1, true)
          }
        }
        if (elem.tagspe) {
          const scol = elem.coolspe
          this.mtgAppLecteur.setColor(svgId, elem.tagspe, scol.in[0], scol.in[1], scol.in[2], true, 0.5)
        }
      } else {
        for (let j = 0; j < elem.tagAllume.length; j++) {
          if (!elem.sm) {
            this.mtgAppLecteur.setColor(svgId, elem.tagAllume[j], coul[0], coul[1], coul[2], true, opa)
            this.mtgAppLecteur.setLineStyle(svgId, elem.tagAllume[j], st, ep, true)
          } else {
            this.mtgAppLecteur.setColor(svgId, elem.tagAllume[j].tagA, elem.tagAllume[j].couleur[0], elem.tagAllume[j].couleur[1], elem.tagAllume[j].couleur[2], true, opa)
            this.mtgAppLecteur.setLineStyle(svgId, elem.tagAllume[j].tagA, elem.tagAllume[j].st, elem.tagAllume[j].ep, true)
          }
        }
        if (elem.tagspe) {
          const scol = elem.coolspe
          this.mtgAppLecteur.setColor(svgId, elem.tagspe, scol.out[0], scol.out[1], scol.out[2], true, 0.5)
        }
      }
      if (elem.serieColle) {
        this.faisOutSerieColle(elem)
      }
    }
  } // makeFoncOut

  makeFoncOver (elem) {
    const svgId = this.svgId
    return () => {
      if (this.blok) return
      this.mtgAppLecteur.getDoc(svgId).defaultCursor = 'pointer'
      const st = elem.st || 0
      const opa = (elem.tagspe) ? 0.4 : 1
      if (elem.clicked) {
        for (let j = 0; j < elem.tagAllume.length; j++) {
          if (!elem.sm) {
            this.mtgAppLecteur.setColor(svgId, elem.tagAllume[j], 200, 50, 50, true, opa)
            this.mtgAppLecteur.setLineStyle(svgId, elem.tagAllume[j], st, 5, true)
          } else {
            this.mtgAppLecteur.setColor(svgId, elem.tagAllume[j].tagA, 200, 50, 50, true, opa)
            this.mtgAppLecteur.setLineStyle(svgId, elem.tagAllume[j].tagA, elem.tagAllume[j].st, 5, true)
          }
        }
        if (elem.tagspe) {
          const scol = elem.coolspe
          this.mtgAppLecteur.setColor(svgId, elem.tagspe, scol.over[0], scol.over[1], scol.over[2], true, 0.5)
        }
      } else {
        for (let j = 0; j < elem.tagAllume.length; j++) {
          if (!elem.sm) {
            const opa = (elem.tagspe) ? 0.4 : 1
            this.mtgAppLecteur.setColor(svgId, elem.tagAllume[j], 250, 0, 0, true, opa)
            this.mtgAppLecteur.setLineStyle(svgId, elem.tagAllume[j], st, 5, true)
          } else {
            this.mtgAppLecteur.setColor(svgId, elem.tagAllume[j].tagA, 250, 0, 0, true, opa)
            this.mtgAppLecteur.setLineStyle(svgId, elem.tagAllume[j].tagA, elem.tagAllume[j].st, 5, true)
          }
        }
        if (elem.tagspe) {
          const scol = elem.coolspe
          this.mtgAppLecteur.setColor(svgId, elem.tagspe, scol.over[0], scol.over[1], scol.over[2], true, 0.5)
        }
      }
      if (elem.serieColle) {
        this.faisOverSerieColle(elem)
      }
    }
  }

  makeFoncSelect (elem) {
    return () => {
      if (this.blok) return
      const svgId = this.svgId
      elem.clicked = !elem.clicked
      const st = elem.st || 0
      const opa = (elem.tagspe) ? 0.4 : 1
      if (!elem.clicked) {
        for (let j = 0; j < elem.tagAllume.length; j++) {
          this.mtgAppLecteur.setColor(svgId, elem.tagAllume[j], 200, 50, 50, true, opa)
          this.mtgAppLecteur.setLineStyle(svgId, elem.tagAllume[j], st, 7, true)
        }
        if (elem.tagspe) {
          const scol = elem.coolspe
          this.mtgAppLecteur.setColor(svgId, elem.tagspe, scol.over[0], scol.over[1], scol.over[2], true, 0.5)
        }
      } else {
        for (let j = 0; j < elem.tagAllume.length; j++) {
          this.mtgAppLecteur.setColor(svgId, elem.tagAllume[j], 250, 0, 0, true, opa)
          this.mtgAppLecteur.setLineStyle(svgId, elem.tagAllume[j], st, 7, true)
        }
        if (elem.tagspe) {
          const scol = elem.coolspe
          this.mtgAppLecteur.setColor(svgId, elem.tagspe, scol.over[0], scol.over[1], scol.over[2], true, 0.5)
        }
      }
      if (elem.serie !== undefined) this.gereSerie(elem)
      if (elem.serieColle) {
        for (let i = 0; i < this.aclick.length; i++) {
          if (this.aclick[i].serieColle === elem.serieColle) {
            this.aclick[i].clicked = elem.clicked
          }
        }
      }
    }
  }

  verifPyr () {
    const ph1 = this.mtgAppLecteur.valueOf(this.svgId, 'VERIF1')
    const ph2 = this.mtgAppLecteur.valueOf(this.svgId, 'VERIF2')
    const ph3 = this.mtgAppLecteur.valueOf(this.svgId, 'VERIF3')
    const ph4 = this.mtgAppLecteur.valueOf(this.svgId, 'VERIF4')
    return (ph1 > 1.2) && (ph2 > 1.2) && (ph3 > 1.2) && (ph4 > 1.2)
  }
}

export default FigNomme02
