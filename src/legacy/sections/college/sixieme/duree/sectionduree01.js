import $ from 'jquery'
import { j3pAddElt, j3pAjouteBouton, j3pClone, j3pDetruit, j3pEmpty, j3pGetNewId, j3pGetRandomBool, j3pGetRandomElt, j3pGetRandomInt, j3pModale, j3pShowError } from 'src/legacy/core/functions'
import { colorCorrection, colorKo, colorOk } from 'src/legacy/core/StylesJ3p'
import { addDefaultTable } from 'src/legacy/themes/table'
import BulleAide from 'src/legacy/outils/bulleAide/BulleAide'
import ZoneStyleMathquill1 from 'src/legacy/outils/zoneStyleMathquill/ZoneStyleMathquill1'
import { j3pAfficheCroixFenetres, j3pCreeFenetres, j3pToggleFenetres } from 'src/legacy/core/functionsJqDialog'
import { j3pCreeSVG } from 'src/legacy/core/functionsSvg'
import { getJ3pConteneur } from 'src/lib/core/domHelpers'
import { j3pAffiche } from 'src/lib/mathquill/functions'
import { getMtgCore } from 'src/lib/outils/mathgraph'
import textesGeneriques from 'src/lib/core/textes'

const { cBien, cFaux, essaieEncore, reponseIncomplete, tempsDepasse } = textesGeneriques

export const params = {
  outils: ['mathquill', 'calculatrice'],
  /**
   * Ce qui est paramétrable dans l’éditeur de graphe
   * @type {LegacySectionParamsParametres}
   */
  parametres: [
    ['nbrepetitions', 3, 'entier', 'Nombre de répétitions de la section'],
    ['nbchances', 2, 'entier', 'Nombre d’essais possibles'],
    ['Question', 'Les trois', 'liste', 'L’élève peut calculer le début, la durée ou la fin d’un événement.', ['Début', 'Durée', 'Fin', 'Début_Durée', 'Début_fin', 'Durée_Fin', 'Les trois']],
    ['Schema', 'Complété', 'liste', 'L’élève peut disposer d’un schéma pour s’aider', ['Oui', 'Complété', 'Non']],
    ['Jour', 'parfois', 'liste', '<b>true</b>: Les heures des début et fin de l’événement peut se situer sur deux journées différentes', ['toujours', 'parfois', 'jamais']],
    ['Calculatrice', true, 'boolean', '<u>true</u>: L’élève dispose d’une calculatrice.'],
    ['theme', 'standard', 'liste', 'Modèle pour l’affichage', ['standard', 'zonesAvecImageDeFond']]

  ]
}

/**
 * section duree01
 * @this {Parcours}
 */
export default function main () {
  const me = this
  const stor = this.storage
  const ds = this.donneesSection
  let svgId = stor.svgId

  function initSection () {
    me.validOnEnter = false // ex donneesSection.touche_entree
    // Construction de la page
    me.construitStructurePage('presentation1bis')

    stor.exos = []

    const questions = []
    if (ds.Question.indexOf('Début') !== -1 || ds.Question === 'Les trois') questions.push('Début')
    if (ds.Question.indexOf('Durée') !== -1 || ds.Question === 'Les trois') questions.push('Durée')
    if (ds.Question.indexOf('Fin') !== -1 || ds.Question === 'Les trois') questions.push('Fin')

    const sits = ['voiture', 'randonnée', 'avion', 'bateau', 'expérience', 'travail']
    const prenoms = ['Jean-Louis', 'Isabelle', 'Nathéo', 'Mathys', 'Llyn', 'Charlie', 'Félix', 'Daniel', 'Yves', 'Walid', 'Louane', 'Hortense', 'Jos']

    const getJour = () => {
      if (ds.Jour === 'toujours') return true
      if (ds.Jour === 'jamais') return false
      return j3pGetRandomBool()
    }
    for (let i = 0; i < me.donneesSection.nbitems; i++) {
      stor.exos.push({
        question: j3pGetRandomElt(questions),
        jour: getJour(),
        sit: j3pGetRandomElt(sits),
        prenom: j3pGetRandomElt(prenoms)
      })
    }

    me.typederreurs = [0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0]
    me.afficheTitre('Calculer avec les durées')
    stor.mepact = getJ3pConteneur(me.zonesElts.MG)

    // chargement mtg
    getMtgCore({ withMathJax: true })
      .then(
        // success
        (mtgAppLecteur) => {
          stor.mtgAppLecteur = mtgAppLecteur
          enonceMain()
        },
        // failure
        (error) => {
          j3pShowError(error, { message: 'mathgraph n’est pas correctement chargé', mustNotify: true })
        })
      // plantage dans enonceMain
      .catch(j3pShowError)
  }

  function faisChoixExos () {
    stor.aidetab = undefined
    const e = stor.exos.pop()
    stor.restric = '0123456789'
    stor.m = j3pGetRandomInt(0, 59)
    switch (e.sit) {
      case 'voiture':
        stor.h = j3pGetRandomInt(0, 9)
        stor.laf = 'OO a effectué un trajet en voiture '
        stor.laf1 = ' pendant OO.'
        stor.laf2 = ' Le trajet a débuté à OO.'
        stor.laf3 = ' Le trajet s’est terminé à OO.'
        stor.laf23 = 'Le trajet a débuté à OO1 est s’est terminé à OO2'
        stor.q1 = 'Tu dois trouver l’heure du départ.'
        stor.q2 = 'Tu dois trouver la durée du trajet.'
        stor.q3 = 'Tu dois trouver l’heure d’arrivée.'
        stor.dedur = 'durée = '
        stor.dedeb = 'heure de départ = '
        stor.defin = 'heure d’arrivée = '
        break
      case 'randonnée':
        stor.h = j3pGetRandomInt(0, 5)
        stor.laf = 'OO a effectué une randonnée '
        stor.laf1 = ' pendant OO.'
        stor.laf2 = ' La randonnée a débuté à OO.'
        stor.laf3 = ' La randonnée s’est terminée à OO.'
        stor.laf23 = 'La randonnée a débuté à OO1 est s’est terminée à OO2'
        stor.q1 = 'Tu dois trouver l’heure du départ.'
        stor.q2 = 'Tu dois trouver la durée de la randonnée.'
        stor.q3 = 'Tu dois trouver l’heure d’arrivée.'
        stor.dedur = 'durée = '
        stor.dedeb = 'heure de départ = '
        stor.defin = 'heure d’arrivée = '
        break
      case 'avion':
        stor.h = j3pGetRandomInt(4, 23)
        stor.laf = 'OO a effectué un voyage en avion '
        stor.laf1 = ' pendant OO.'
        stor.laf2 = ' Le vol a débuté à OO.'
        stor.laf3 = ' Le vol s’est terminé à OO.'
        stor.laf23 = 'Le vol a débuté à OO1 est s’est terminé à OO2'
        stor.q1 = 'Tu dois trouver l’heure du départ.'
        stor.q2 = 'Tu dois trouver la durée du vol.'
        stor.q3 = 'Tu dois trouver l’heure d’arrivée.'
        stor.dedur = 'durée = '
        stor.dedeb = 'heure de départ = '
        stor.defin = 'heure d’arrivée = '
        break
      case 'bateau':
        stor.h = j3pGetRandomInt(4, 23)
        stor.laf = 'OO a effectué une traversée en bateau '
        stor.laf1 = ' pendant OO.'
        stor.laf2 = ' La traversée a débuté à OO.'
        stor.laf3 = ' La traversée s’est terminée à OO.'
        stor.laf23 = 'La traversée a débuté à OO1 est s’est terminée à OO2'
        stor.q1 = 'Tu dois trouver l’heure du départ.'
        stor.q2 = 'Tu dois trouver la durée de la traversée.'
        stor.q3 = 'Tu dois trouver l’heure d’arrivée.'
        stor.dedur = 'durée = '
        stor.dedeb = 'heure de départ = '
        stor.defin = 'heure d’arrivée = '
        break
      case 'expérience':
        stor.h = j3pGetRandomInt(0, 23)
        stor.laf = 'OO a réalisé une expérience scientifique '
        stor.laf1 = ' pendant OO.'
        stor.laf2 = ' L’expérience a débuté à OO.'
        stor.laf3 = ' L’expérience s’est terminée à OO.'
        stor.laf23 = ' L’expérience a débuté à OO1 est s’est terminée à OO2'
        stor.q1 = 'Tu dois trouver l’heure du début de l’expérience.'
        stor.q2 = 'Tu dois trouver la durée de cette exprérience.'
        stor.q3 = 'Tu dois trouver l’heure de fin de cette exprérience.'
        stor.dedur = 'durée = '
        stor.dedeb = 'heure du début = '
        stor.defin = 'heure de fin = '
        break
      case 'travail':
        stor.h = j3pGetRandomInt(2, 17)
        stor.laf = 'OO a réalisé un travail de longue haleine '
        stor.laf1 = ' pendant OO.'
        stor.laf2 = ' Le travail a débuté à OO.'
        stor.laf3 = ' Le travail s’est terminé à OO.'
        stor.laf23 = ' Le travail a débuté à OO1 est s’est terminée à OO2'
        stor.q1 = 'Tu dois trouver l’heure du début de ce travail.'
        stor.q2 = 'Tu dois trouver la durée de ce travail.'
        stor.q3 = 'Tu dois trouver l’heure de fin de ce travail.'
        stor.dedur = 'durée = '
        stor.dedeb = 'heure du début = '
        stor.defin = 'heure de fin = '
        break
    }
    const dudur = stor.h * 60 + stor.m
    stor.dudur = dudur
    if (e.jour) {
      stor.mdep = j3pGetRandomInt(1441 - dudur, 1439)
    } else {
      stor.mdep = j3pGetRandomInt(0, 1440 - dudur)
    }
    stor.hdep = Math.floor(stor.mdep / 60)
    stor.mindep = stor.mdep % 60
    stor.mar = (stor.mdep + dudur) % 1440
    stor.har = Math.floor(stor.mar / 60)
    stor.minar = stor.mar % 60
    stor.yaco = false
    return e
  }
  function creeLesDiv () {
    const conteneur = j3pAddElt(me.zonesElts.MG, 'div', {
      style: me.styles.etendre('toutpetit.enonce', { padding: '10px' })
    })
    stor.lesdiv = {
      conteneur,
      situation: addDefaultTable(conteneur, 1, 1)[0][0],
      question: addDefaultTable(conteneur, 1, 1)[0][0],
      schema: addDefaultTable(conteneur, 1, 1)[0][0],
      travail: addDefaultTable(conteneur, 1, 1)[0][0],
      explications: j3pAddElt(conteneur, 'div', {
        style: me.styles.etendre('toutpetit.enonce', { padding: '10px' })
      }),
      solution: j3pAddElt(conteneur, 'div', {
        style: me.styles.etendre('toutpetit.enonce', { padding: '10px' })
      }),
      calculatrice: j3pAddElt(me.zonesElts.MD, 'div', {
        style: me.styles.etendre('toutpetit.enonce', { padding: '10px' })
      }),
      correction: j3pAddElt(me.zonesElts.MD, 'div', {
        style: me.styles.etendre('toutpetit.enonce', { padding: '10px' })
      })
    }
    stor.lesdiv.explications.style.color = colorKo
    stor.lesdiv.solution.style.color = colorCorrection
    stor.lesdiv.situation.classList.add('enonce')
    stor.lesdiv.question.classList.add('enonce')
    stor.lesdiv.travail.classList.add('travail')
    me.zonesElts.MG.classList.add('fond')
  }
  function poseQuestion () {
    let afdur, comb, affin, afdeb, quequest
    switch (stor.Lexo.question) {
      case 'Début':
        stor.pref = 'ligne'
        afdur = ''
        comb = ''
        if (stor.h !== 0) {
          comb = ' et '
          afdur = stor.h + ' heure'
          if (stor.h > 1) afdur += 's'
        }
        if (stor.m > 0) {
          afdur += comb + stor.m + ' minute'
          if (stor.m > 1) afdur += 's'
        }
        affin = stor.har + 'h'
        if (stor.har < 10) affin = '0' + affin
        if (stor.minar < 10) { affin += '0' + stor.minar } else { affin += stor.minar }
        j3pAffiche(stor.lesdiv.situation, null, '<i>' + stor.laf.replace('OO', stor.Lexo.prenom) + stor.laf1.replace('OO', afdur) + '</i> \n ')
        j3pAffiche(stor.lesdiv.situation, null, '<i>' + stor.laf3.replace('OO', affin) + '</i> \n')
        stor.tabLi = addDefaultTable(stor.lesdiv.question, 1, 3)
        stor.tabLi[0][1].style.width = '30px'
        j3pAffiche(stor.tabLi[0][0], null, '<b>' + stor.q1 + '</b>')
        quequest = stor.dedeb
        break
      case 'Durée':
        stor.pref = 'zed'
        afdeb = stor.hdep + 'h'
        if (stor.hdep < 10) afdeb = '0' + afdeb
        if (stor.mindep < 10) { afdeb += '0' + stor.mindep } else { afdeb += stor.mindep }
        affin = stor.har + 'h'
        if (stor.har < 10) affin = '0' + affin
        if (stor.minar < 10) { affin += '0' + stor.minar } else { affin += stor.minar }
        j3pAffiche(stor.lesdiv.situation, null, '<i>' + stor.laf.replace('OO', stor.Lexo.prenom) + '.</i> \n')
        j3pAffiche(stor.lesdiv.situation, null, '<i>' + stor.laf2.replace('OO', afdeb) + '</i> \n')
        j3pAffiche(stor.lesdiv.situation, null, '<i>' + stor.laf3.replace('OO', affin) + '</i> \n')
        stor.tabLi = addDefaultTable(stor.lesdiv.question, 1, 3)
        stor.tabLi[0][1].style.width = '30px'
        j3pAffiche(stor.tabLi[0][0], null, '<b>' + stor.q2 + '</b>')
        quequest = stor.dedur
        break
      case 'Fin':
        stor.pref = 'ligne'
        afdur = ''
        comb = ''
        if (stor.h !== 0) {
          comb = ' et '
          afdur = stor.h + ' heure'
          if (stor.h > 1) afdur += 's'
        }
        if (stor.m > 0) {
          afdur += comb + stor.m + ' minute'
          if (stor.m > 1) afdur += 's'
        }
        afdeb = stor.hdep + 'h'
        if (stor.hdep < 10) afdeb = '0' + afdeb
        if (stor.mindep < 10) { afdeb += '0' + stor.mindep } else { afdeb += stor.mindep }
        j3pAffiche(stor.lesdiv.situation, null, '<i>' + stor.laf.replace('OO', stor.Lexo.prenom) + stor.laf1.replace('OO', afdur) + '</i> \n')
        j3pAffiche(stor.lesdiv.situation, null, '<i>' + stor.laf2.replace('OO', afdeb) + '</i> \n')
        stor.tabLi = addDefaultTable(stor.lesdiv.question, 1, 3)
        stor.tabLi[0][1].style.width = '30px'
        quequest = stor.defin
        j3pAffiche(stor.tabLi[0][0], null, '<b>' + stor.q3 + '</b>')
        break
    }
    stor.afdur = afdur
    stor.afin = affin
    stor.afdeb = afdeb
    if (ds.Schema !== 'Non') {
      j3pAjouteBouton(stor.lesdiv.schema, shcemaz, { className: '', value: 'Utiliser un schéma' })
    }
    stor.tabfd = addDefaultTable(stor.lesdiv.travail, 1, 2)
    j3pAffiche(stor.tabfd[0][0], null, quequest + '&nbsp;')
    if (stor.Lexo.question === 'Durée') {
      stor.mez = 'hm'
      stor.zonem = stor.zoneh = undefined
      faisMez()
    } else {
      const ttt = addDefaultTable(stor.tabfd[0][1], 1, 3)
      j3pAffiche(ttt[0][1], null, '&nbsp; h &nbsp;')
      stor.zoneh = new ZoneStyleMathquill1(ttt[0][0], {
        limite: 4,
        limitenb: 1,
        restric: '0123456789',
        enter: me.sectionCourante.bind(me)
      })
      stor.zonem = new ZoneStyleMathquill1(ttt[0][2], {
        limite: 4,
        limitenb: 1,
        restric: '0123456789',
        enter: me.sectionCourante.bind(me)
      })
    }
    stor.lesdiv.explications.classList.remove('explique')
    stor.lesdiv.solution.classList.remove('correction')
    stor.kquest = quequest
  }

  function faisMez () {
    if (stor.zonem !== undefined) stor.zonem.disable()
    if (stor.zoneh !== undefined) stor.zoneh.disable()
    stor.zonem = stor.zoneh = undefined
    j3pEmpty(stor.tabfd[0][1])
    const ttt = addDefaultTable(stor.tabfd[0][1], 1, 6)
    stor.aef = ttt[0][5]
    const ttt2 = addDefaultTable(stor.aef, 2, 1)
    switch (stor.mez) {
      case 'hm':
        j3pAffiche(ttt[0][1], null, '&nbsp; h &nbsp;')
        j3pAffiche(ttt[0][3], null, '&nbsp; min &nbsp;')
        stor.zonem = new ZoneStyleMathquill1(ttt[0][2], {
          limite: 4,
          limitenb: 1,
          restric: '0123456789',
          enter: me.sectionCourante.bind(me)

        })
        stor.zoneh = new ZoneStyleMathquill1(ttt[0][0], {
          limite: 4,
          limitenb: 1,
          restric: '0123456789',
          enter: me.sectionCourante.bind(me)

        })
        ttt[0][4].style.width = '40px'
        j3pAjouteBouton(ttt2[0][0], faisMezh, { className: '', value: 'réponse en heures' })
        j3pAjouteBouton(ttt2[1][0], faisMezm, { className: '', value: 'réponse en minutes' })
        break
      case 'h':
        j3pAffiche(ttt[0][1], null, '&nbsp; h ')
        stor.zoneh = new ZoneStyleMathquill1(ttt[0][0], {
          limite: 4,
          limitenb: 1,
          restric: '0123456789.,',
          clavier: '0123456789.,',
          enter: me.sectionCourante.bind(me)

        })
        ttt[0][2].style.width = '40px'
        j3pAjouteBouton(ttt2[0][0], faisMezhm, { className: '', value: 'réponse en heures et minutes' })
        j3pAjouteBouton(ttt2[1][0], faisMezm, { className: '', value: 'réponse en minutes' })
        break
      case 'm':
        j3pAffiche(ttt[0][1], null, '&nbsp; min ')
        stor.zonem = new ZoneStyleMathquill1(ttt[0][0], {
          limite: 4,
          limitenb: 1,
          restric: '0123456789',
          clavier: '0123456789',
          enter: me.sectionCourante.bind(me)

        })
        ttt[0][2].style.width = '40px'
        j3pAjouteBouton(ttt2[0][0], faisMezhm, { className: '', value: 'réponse en heures et minutes' })
        j3pAjouteBouton(ttt2[1][0], faisMezh, { className: '', value: 'réponse en heures' })
        break
    }
  }
  function faisMezh () {
    stor.mez = 'h'
    faisMez()
  }
  function faisMezm () {
    stor.mez = 'm'
    faisMez()
  }
  function faisMezhm () {
    stor.mez = 'hm'
    faisMez()
  }

  function boutonfen () {
    j3pToggleFenetres('Calculatrice')
  }
  function shcemaz () {
    if (ds.Schema === 'Complété') {
      if (stor.pref === 'zed') {
        faisZed()
      } else { faisLigne() }
      return
    }
    const yy = j3pModale({ titre: 'Type de schéma', contenu: '' })
    const ttt = addDefaultTable(yy, 1, 5)
    ttt[0][1].style.width = '40px'
    ttt[0][3].style.width = '40px'
    j3pAjouteBouton(ttt[0][0], faisLigne, { className: '', value: 'ligne de temps' })
    j3pAjouteBouton(ttt[0][2], faisZed, { className: '', value: 'Z du temps' })
    j3pAjouteBouton(ttt[0][4], faisAnnule, { className: '', value: 'Annuler' })
  }
  function faisLigne () {
    stor.raz = false
    stor.okCoup = false
    if (stor.aidetab !== undefined) {
      stor.aidetab.disable()
      stor.aidetab = undefined
    }
    j3pEmpty(stor.tabLi[0][2])
    if (!stor.yaco) {
      stor.aidetab = new BulleAide(stor.tabLi[0][2], 'Clique sur la ligne bleu pour ajouter un repère. <br> Clique sur une valeur pour la modifier.', { place: 5 })
    }
    stor.LL = true
    j3pDetruit('modale')
    j3pDetruit('j3pmasque')
    j3pEmpty(stor.lesdiv.schema)
    svgId = j3pGetNewId()
    stor.svgId = svgId
    const jkl = addDefaultTable(stor.lesdiv.schema, 1, 2)
    stor.bos = j3pAjouteBouton(jkl[0][1], supSch, { value: 'Supprimer' })
    const hj = j3pCreeSVG(jkl[0][0], { id: svgId, width: 600, height: 200 })
    const txtFigure = 'TWF0aEdyYXBoSmF2YTEuMAAAABM+TMzNAAJmcv###wEA#wEAAAAAAAAAAANLAAACfwAAAQEAAAACAAAABgACZmgAAAAAAAEAAAAEAAAAAAj#####AAAAAQARQ0VsZW1lbnRHZW5lcmlxdWUAAAAAAAD##########wAAAAIABkNJbWFnZQH#####AP8AAAA#8AAAAAAAAEBNgAAAAAAAAAAAAAAADQAAAAAAAQAAAAL#####AAAAAQAKQ0NvbnN0YW50ZQAAAAAAAAAAAAAAABwAAAAcAAADGolQTkcNChoKAAAADUlIRFIAAAAcAAAAHAgGAAAAcg3flAAAAAFzUkdCAK7OHOkAAAAEZ0FNQQAAsY8L#GEFAAAACXBIWXMAABYlAAAWJQFJUiTwAAAABmJLR0QA#wD#AP+gvaeTAAACnUlEQVRIS92Wz0o6URTHj3#QhWaBulA0zRQUN4m4cBG0EIqC3JiLoJVv4Bv4AL2BCxc9gLQW3Api#kFUhIRwpRYGSn+l+c053RGnctJJN78PnJl7z#z5zrn33DkXOEYkEuEAYGE7PDxkTy6HAg#8CyAUCsHFxQWcnZ3B8#Mzun5ka2sLEokEZLNZCAQCcHNzw64sCMny8A9zV1dXrCdNPB7nTk5OOJvNxlmtVuZdDCXTJSaTCWtJ8#b2Bnq9HrrdLry8vIDD4WBXfkckuAyvr690fnh4IGGfz0f935AtOAuODIryQ8w885EtqFAoWIt#iVIJ4#GYks3r9TLvz8gS5OeeDMH5REPu7++h0+mA3++n#k#IEsSlgcsCo9RqtWTYxkg#Pj6g0WhAKpVid4uRJZhOpylp3t#fvxnOZzAYBJVKxe4WI0tQrVaDRqOh81cTEIb8K7KTRop5YshaBKX4PwQxoYTfZDgcpgxGa7fb6xE8OjqC09NTqiaFQoF5AY6Pj8XVIpPJsN7f2d3d5UwmE7X39vaohqKtJcLt7W0YjUYwGAyoXy6X6ZxMJlc#pHxkMBwOodfrMQ+A0WikanJ5eblaQbvdDo+PjxSdgNlsptqJvztkZYIulwuenp6oPgrodDowGAxwd3fHPCsSxDnr9#sisc3NTdoJ3N7eMs8nfxbEYcTIsB4KbGxsgMVimQ7jLH8S5NOdii7WQQEsVTs7O9BqtZhHjEgQK8CiXF9fQ7VahUqlwjyfc+bxeKBWqzHPd6b1BItnsVikrJq3L8UdWjQapQ+r1+vkczqd0Gw2KVq3200fIcV0I3x+fg65XI5exlzfwPVVKpVoTWH2zab#wcEB5PN51pvPVHBZ8GfMb4YhFovRYt#f32dXpAD4BwbM#Cj7fCliAAAAAElFTkSuQmCC#####wAAAAIADENDb21tZW50YWlyZQH#####AAAAAADACAAAAAAAAMBFgAAAAAAAAAAAAAAAEAAAAAAAAAAAAAAAAAACAAAAAAAAAAAAAWH#####AAAAAgAJQ0NlcmNsZU9SAP####8BAAAAAAAAAwAAAAAAAAACP9MzMzMzMzMA#####wAAAAEAFENEcm9pdGVEaXJlY3Rpb25GaXhlAP####8BAAAAARAAAAEAAAADAAAAAAA#8AAAAAAAAP####8AAAABABBDSW50RHJvaXRlQ2VyY2xlAP####8AAAAEAAAAA#####8AAAABABBDUG9pbnRMaWVCaXBvaW50Af####8AAAAAABAAAAAAAAAAAAAAAEAIAAAAAAAAAAAFAAIAAAAFAAAABwH#####AAAA#wAQAAAAAAAAAAAAAABACAAAAAAAAAAABQABAAAABQAAAAYAA2JiYgAAAAAAAwAAAAMAAAAAEQAAAAAAAAAAAAD#####AAAAAAAAAAAAAP####8AAAAAAAAAAAAA##########8AAAABAAdDTWlsaWV1AP####8B#wD#ABAAAAAAAAAAAAAAAEAIAAAAAAAAAAAFAAAAAAAAAAABAAAABQD#####AAB#AAEQAAABAAAAAwAAAAIBP#AAAAAAAAAAAAAFAP####8BAH8AARAAAAEAAAADAAAAAwA#8AAAAAAAAP####8AAAABABBDSW50RHJvaXRlRHJvaXRlAP####8AAH8AABAAAAAAAAAAAAAAAEAIAAAAAAAAAAAFAAAAAAUAAAAE#####wAAAAEAB0NDYWxjdWwA#####wACYWEAAjEwAAAAAkAkAAAAAAAA#####wAAAAEACUNSb3RhdGlvbgD#####AAAABv####8AAAABAA9DUmVzdWx0YXRWYWxldXIAAAAH#####wAAAAEAC0NQb2ludEltYWdlAP####8AAH8AABAAAAAAAAAAAAAAAEAIAAAAAAAAAAAFAAAAAAAAAAAIAAAACwD#####AAAABv####8AAAABAAxDTW9pbnNVbmFpcmUAAAAMAAAABwAAAA0A#####wAAfwAAEAAAAAAAAAAAAAAAQAgAAAAAAAAAAAUAAAAAAQAAAAr#####AAAAAQASQ0FyY0RlQ2VyY2xlRGlyZWN0Af####8AAAAAAAAAAwAAAAYAAAAJAAAACwAAAAYA#####wAAAAUAAAAMAAAABwD#####AAAAAAAQAAAAAAAAAAAAAABACAAAAAAAAAAABQABAAAADQAAAAMB#####wAAAAAAwBAAAAAAAABAFAAAAAAAAAAAAAAADhAAAAAAAAAAAAAAAAAAAgAAAAAAAAAAAAFhAAAAAQH#####AAAAAAC#8AAAAAAAAMAmAAAAAAAAAAAAAAAODQAAAAAAAQAAAAIAAAACAAAAAAAAAAAAAAAAHAAAABwAAAHmiVBORw0KGgoAAAANSUhEUgAAABwAAAAcCAMAAABF0y+mAAAAAXNSR0IArs4c6QAAAARnQU1BAACxjwv8YQUAAAB+UExURaNJpEdHR5GRkVBQUKSkpFZWVktLS6CgoFdXV1FRUU9PT19fX4CAgIODg4KCgn5+fjg4OHNzc8zMzDAwMCMjIx0dHXJyciIiIikpKR4eHhISEgsLCxMTEycnJy0tLaamprGxsSsrK3FxcbKysqqqqoWFhS4uLkBAQJ+fnwAAAJGrmG8AAAAqdFJOU#######################################################ADKo8FwAAAAJcEhZcwAAFiQAABYkAZsVxhQAAAC7SURBVDhPvdDZDoIwEAXQUdx3RQG3qFFj7v##oLNUutBnb8J0hpOmFAIoSA9hBPuuBwri2Udw4HpgOIpUcOx63jnhQ1zPSbDg0WsXA01wKrXVGGey02uMc3ddWugY4y+0tCWPK1v+imts+D553O5KvVEWsScIZ#HAr0FVFmu1Jnum#lo68tNFOkmppXSQzlIabVOki5Sr9Qma6T7Dm7WSO8+ghw2KFZ6aF#CWn1Y6U2xj#ceRYhga+Q8Avi3QZBhQD1WxAAAAAElFTkSuQmCCAAAAAQAAAJ3#####AAAAAQAKQ0NhbGNDb25zdAD#####AAJwaQAWMy4xNDE1OTI2NTM1ODk3OTMyMzg0NgAAAAJACSH7VEQtGP####8AAAABAApDUG9pbnRCYXNlAP####8BAAAAABAAAAAAAAAAAAAAAEAIAAAAAAAAAAVwdGRlYgUAAUA6gAAAAAAAQEvrhR64UfAAAAAFAP####8BAAAAARAAAAEAAAABAAAAAQE#8AAAAAAAAP####8AAAABAA9DUG9pbnRMaWVEcm9pdGUA#####wEAAAAAEAAAAAAAAAAAAAAAQAgAAAAAAAAABXB0ZmluBQABQHsgAAAAAAAAAAAC#####wAAAAEACENTZWdtZW50AP####8AAAD#ABAAAAEAAmwyAAMAAAABAAAAA#####8AAAABAA1DU2VnbWVudENsb25lAP####8A#i5kAA0AAAEAAkxMAAwAAAABAAAAAwAAAAQAAAARAP####8BAAAAABAAAUEAAAAAAAAAAABACAAAAAAAAAAABQABQE5AAAAAAABAda1wo9cKPgAAABEA#####wEAAAAAEAABQgAAAAAAAAAAAEAIAAAAAAAAAAAFAAFAVqAAAAAAAEB2DXCj1wo+#####wAAAAEACUNMb25ndWV1cgD#####AAAABgAAAAcAAAAEAP####8BAAAAAAAAAQAAAAMAAAACP#AAAAAAAAAAAAAABQD#####AQAAAAEQAAABAAAAAQAAAAMAP#AAAAAAAAAAAAAGAP####8AAAAKAAAACQAAAAcA#####wEAAAAAEAAAAAAAAAAAAAAAQAgAAAAAAAAAAAUAAgAAAAsAAAAHAP####8BAAAAABAAAAAAAAAAAAAAAEAIAAAAAAAAAAAFAAEAAAALAAAACwD#####AAAAAwAAAAJATgAAAAAAAAAAAA0A#####wEAAAAAEAAAAAAAAAAAAAAAQAgAAAAAAAAAAAUAAAAADAAAAA7#####AAAAAQAPQ1N5bWV0cmllQXhpYWxlAP####8AAAACAAAADQD#####AQAAAAAQAAAAAAAAAAAAAABACAAAAAAAAAAABQAAAAAPAAAAEAAAABMA#####wAAAP8AEAAAAQACbDEAAwAAAAMAAAAPAAAAEwD#####AAAA#wAQAAABAAJsMwADAAAAAwAAABEAAAARAP####8BAAD#ABAAAAAAAAAAAAAAAEAIAAAAAAAAAANwdDEFAAFAUGAAAAAAAEBH64UeuFHsAAAAAQD#####Af8AAAA#8AAAAAAAAEBNgAAAAAAAAANpbTEAAAAUDQAAAAAAAQAAAAIAAAACAAAAAAAAAAAAAAAAHAAAABwAAAMaiVBORw0KGgoAAAANSUhEUgAAABwAAAAcCAYAAAByDd+UAAAAAXNSR0IArs4c6QAAAARnQU1BAACxjwv8YQUAAAAJcEhZcwAAFiUAABYlAUlSJPAAAAAGYktHRAD#AP8A#6C9p5MAAAKdSURBVEhL3ZbPSjpRFMePf9CFZoG6UDTNFBQ3ibhwEbQQioLcmIuglW#gG#gAvYELFz2AtBbcCmL+QVSEhHClFgZKf6X5zTndEady0kk3vw+cmXvP#PnOuffcORc4RiQS4QBgYTs8PGRPLocCD#wLIBQKwcXFBZydncHz8zO6fmRrawsSiQRks1kIBAJwc3PDriwIyfLwD3NXV1esJ008HudOTk44m83GWa1W5l0MJdMlJpMJa0nz9vYGer0eut0uvLy8gMPhYFd+RyS4DK+vr3R+eHggYZ#PR#3fkC04C44MivJDzDzzkS2oUChYi3+JUgnj8ZiSzev1Mu#PyBLk554MwflEQ+7v76HT6YDf76f+T8gSxKWBywKj1Gq1ZNjGSD8+PqDRaEAqlWJ3i5ElmE6nKWne39+#Gc5nMBgElUrF7hYjS1CtVoNGo6HzVxMQhvwrspNGinliyFoEpfg#BDGhhN9kOBymDEZrt9vrETw6OoLT01OqJoVCgXkBjo+PxdUik8mw3t#Z3d3lTCYTtff29qiGoq0lwu3tbRiNRjAYDKhfLpfpnEwmVz+kfGQwHA6h1+sxD4DRaKRqcnl5uVpBu90Oj4+PFJ2A2Wym2om#O2Rlgi6XC56enqg+Cuh0OjAYDHB3d8c8KxLEOev3+yKxzc1N2gnc3t4yzyd#FsRhxMiwHgpsbGyAxWKZDuMsfxLk052KLtZBASxVOzs70Gq1mEeMSBArwKJcX19DtVqFSqXCPJ9z5vF4oFarMc93pvUEi2exWKSsmrcvxR1aNBqlD6vX6+RzOp3QbDYpWrfbTR8hxXQjfH5+Drlcjl7GXN#A9VUqlWhNYfbNpv#BwQHk83nWm89UcFnwZ8xvhiEWi9Fi39#fZ1ekAPgHBsz8KPt8KWIAAAAASUVORK5CYIIAAAADAP####8AAAAAAMAIAAAAAAAAwEWAAAAAAAAAA2FmMQAAABQQAAAAAAAAAAAAAAAAAAIAAAAAAAAAAAABYQAAAAQA#####wEAAAAAAAADAAAAFAAAAAI#0zMzMzMzMwAAAAAFAP####8BAAAAARAAAAEAAAADAAAAFAA#8AAAAAAAAAAAAAYA#####wAAABgAAAAXAAAABwD#####AQAAAAAQAAAAAAAAAAAAAABACAAAAAAAAAAABQACAAAAGQAAAAcA#####wEAAP8AEAAAAAAAAAAAAAAAQAgAAAAAAAAAAAUAAQAAABkAAAATAP####8AAAAAABAAAAEABHNlZzEAAwAAABoAAAAbAAAAEQD#####AQAAAAAQAAAAAAAAAAAAAABACAAAAAAAAAADcHQyBQABQGtQAAAAAABAR+uFHrhR7AAAABEA#####wEAAP8AEAAAAAAAAAAAAAAAQAgAAAAAAAAAA3B0MwUAAUB2mAAAAAAAQEbrhR64UewAAAARAP####8BAAAAABAAAAAAAAAAAAAAAEAIAAAAAAAAAANwdDQFAAFAf4gAAAAAAEBIa4UeuFHsAAAAEQD#####AQAAAAAQAAAAAAAAAAAAAABACAAAAAAAAAADcHQ1BQABQISMAAAAAABASGuFHrhR7AAAAAEA#####wH#AAAAP#AAAAAAAABATYAAAAAAAAADaW0yAAAAHQ0AAAAAAAEAAAACAAAAAgAAAAAAAAAAAAAAABwAAAAcAAADGolQTkcNChoKAAAADUlIRFIAAAAcAAAAHAgGAAAAcg3flAAAAAFzUkdCAK7OHOkAAAAEZ0FNQQAAsY8L#GEFAAAACXBIWXMAABYlAAAWJQFJUiTwAAAABmJLR0QA#wD#AP+gvaeTAAACnUlEQVRIS92Wz0o6URTHj3#QhWaBulA0zRQUN4m4cBG0EIqC3JiLoJVv4Bv4AL2BCxc9gLQW3Api#kFUhIRwpRYGSn+l+c053RGnctJJN78PnJl7z#z5zrn33DkXOEYkEuEAYGE7PDxkTy6HAg#8CyAUCsHFxQWcnZ3B8#Mzun5ka2sLEokEZLNZCAQCcHNzw64sCMny8A9zV1dXrCdNPB7nTk5OOJvNxlmtVuZdDCXTJSaTCWtJ8#b2Bnq9HrrdLry8vIDD4WBXfkckuAyvr690fnh4IGGfz0f935AtOAuODIryQ8w885EtqFAoWIt#iVIJ4#GYks3r9TLvz8gS5OeeDMH5REPu7++h0+mA3++n#k#IEsSlgcsCo9RqtWTYxkg#Pj6g0WhAKpVid4uRJZhOpylp3t#fvxnOZzAYBJVKxe4WI0tQrVaDRqOh81cTEIb8K7KTRop5YshaBKX4PwQxoYTfZDgcpgxGa7fb6xE8OjqC09NTqiaFQoF5AY6Pj8XVIpPJsN7f2d3d5UwmE7X39vaohqKtJcLt7W0YjUYwGAyoXy6X6ZxMJlc#pHxkMBwOodfrMQ+A0WikanJ5eblaQbvdDo+PjxSdgNlsptqJvztkZYIulwuenp6oPgrodDowGAxwd3fHPCsSxDnr9#sisc3NTdoJ3N7eMs8nfxbEYcTIsB4KbGxsgMVimQ7jLH8S5NOdii7WQQEsVTs7O9BqtZhHjEgQK8CiXF9fQ7VahUqlwjyfc+bxeKBWqzHPd6b1BItnsVikrJq3L8UdWjQapQ+r1+vkczqd0Gw2KVq3200fIcV0I3x+fg65XI5exlzfwPVVKpVoTWH2zab#wcEB5PN51pvPVHBZ8GfMb4YhFovRYt#f32dXpAD4BwbM#Cj7fCliAAAAAElFTkSuQmCCAAAAAwD#####AAAAAADACAAAAAAAAMBFgAAAAAAAAANhZjIAAAAdEAAAAAAAAAAAAAAAAAACAAAAAAAAAAAAAWEAAAAEAP####8BAAAAAAAAAwAAAB0AAAACP9MzMzMzMzMAAAAABQD#####AQAAAAAQAAABAAAAAwAAAB0AP#AAAAAAAAAAAAAGAP####8AAAAkAAAAIwAAAAcA#####wEAAAAAEAAAAAAAAAAAAAAAQAgAAAAAAAAAAAUAAgAAACUAAAAHAP####8BAAD#ABAAAAAAAAAAAAAAAEAIAAAAAAAAAAAFAAEAAAAlAAAAAQD#####Af8AAAA#8AAAAAAAAEBNgAAAAAAAAANpbTMAAAAeDQAAAAAAAQAAAAIAAAACAAAAAAAAAAAAAAAAHAAAABwAAAMaiVBORw0KGgoAAAANSUhEUgAAABwAAAAcCAYAAAByDd+UAAAAAXNSR0IArs4c6QAAAARnQU1BAACxjwv8YQUAAAAJcEhZcwAAFiUAABYlAUlSJPAAAAAGYktHRAD#AP8A#6C9p5MAAAKdSURBVEhL3ZbPSjpRFMePf9CFZoG6UDTNFBQ3ibhwEbQQioLcmIuglW#gG#gAvYELFz2AtBbcCmL+QVSEhHClFgZKf6X5zTndEady0kk3vw+cmXvP#PnOuffcORc4RiQS4QBgYTs8PGRPLocCD#wLIBQKwcXFBZydncHz8zO6fmRrawsSiQRks1kIBAJwc3PDriwIyfLwD3NXV1esJ008HudOTk44m83GWa1W5l0MJdMlJpMJa0nz9vYGer0eut0uvLy8gMPhYFd+RyS4DK+vr3R+eHggYZ#PR#3fkC04C44MivJDzDzzkS2oUChYi3+JUgnj8ZiSzev1Mu#PyBLk554MwflEQ+7v76HT6YDf76f+T8gSxKWBywKj1Gq1ZNjGSD8+PqDRaEAqlWJ3i5ElmE6nKWne39+#Gc5nMBgElUrF7hYjS1CtVoNGo6HzVxMQhvwrspNGinliyFoEpfg#BDGhhN9kOBymDEZrt9vrETw6OoLT01OqJoVCgXkBjo+PxdUik8mw3t#Z3d3lTCYTtff29qiGoq0lwu3tbRiNRjAYDKhfLpfpnEwmVz+kfGQwHA6h1+sxD4DRaKRqcnl5uVpBu90Oj4+PFJ2A2Wym2om#O2Rlgi6XC56enqg+Cuh0OjAYDHB3d8c8KxLEOev3+yKxzc1N2gnc3t4yzyd#FsRhxMiwHgpsbGyAxWKZDuMsfxLk052KLtZBASxVOzs70Gq1mEeMSBArwKJcX19DtVqFSqXCPJ9z5vF4oFarMc93pvUEi2exWKSsmrcvxR1aNBqlD6vX6+RzOp3QbDYpWrfbTR8hxXQjfH5+Drlcjl7GXN#A9VUqlWhNYfbNpv#BwQHk83nWm89UcFnwZ8xvhiEWi9Fi39#fZ1ekAPgHBsz8KPt8KWIAAAAASUVORK5CYIIAAAADAP####8AAAAAAMAIAAAAAAAAwEWAAAAAAAAAA2FmMwAAAB4QAAAAAAAAAAAAAAAAAAIAAAAAAAAAAAABYQAAAAQA#####wEAAAAAAAADAAAAHgAAAAI#0zMzMzMzMwAAAAAFAP####8BAAAAABAAAAEAAAADAAAAHgA#8AAAAAAAAAAAAAYA#####wAAACsAAAAqAAAABwD#####AQAAAAAQAAAAAAAAAAAAAABACAAAAAAAAAAABQACAAAALAAAAAcA#####wEAAP8AEAAAAAAAAAAAAAAAQAgAAAAAAAAAAAUAAQAAACwAAAABAP####8B#wAAAD#wAAAAAAAAQE2AAAAAAAAAA2ltNAAAAB8NAAAAAAABAAAAAgAAAAIAAAAAAAAAAAAAAAAcAAAAHAAAAxqJUE5HDQoaCgAAAA1JSERSAAAAHAAAABwIBgAAAHIN35QAAAABc1JHQgCuzhzpAAAABGdBTUEAALGPC#xhBQAAAAlwSFlzAAAWJQAAFiUBSVIk8AAAAAZiS0dEAP8A#wD#oL2nkwAAAp1JREFUSEvdls9KOlEUx49#0IVmgbpQNM0UFDeJuHARtBCKgtyYi6CVb+Ab+AC9gQsXPYC0FtwKYv5BVISEcKUWBkp#pfnNOd0Rp3LSSTe#D5yZe8#8+c6599w5FzhGJBLhAGBhOzw8ZE8uhwIP#AsgFArBxcUFnJ2dwfPzM7p+ZGtrCxKJBGSzWQgEAnBzc8OuLAjJ8vAPc1dXV6wnTTwe505OTjibzcZZrVbmXQwl0yUmkwlrSfP29gZ6vR663S68vLyAw+FgV35HJLgMr6+vdH54eCBhn89H#d+QLTgLjgyK8kPMPPORLahQKFiLf4lSCePxmJLN6#Uy78#IEuTnngzB+URD7u#vodPpgN#vp#5PyBLEpYHLAqPUarVk2MZIPz4+oNFoQCqVYneLkSWYTqcpad7f378ZzmcwGASVSsXuFiNLUK1Wg0ajofNXExCG#Cuyk0aKeWLIWgSl+D8EMaGE32Q4HKYMRmu32+sRPDo6gtPTU6omhUKBeQGOj4#F1SKTybDe39nd3eVMJhO19#b2qIairSXC7e1tGI1GMBgMqF8ul+mcTCZXP6R8ZDAcDqHX6zEPgNFopGpyeXm5WkG73Q6Pj48UnYDZbKbaib87ZGWCLpcLnp6eqD4K6HQ6MBgMcHd3xzwrEsQ56#f7IrHNzU3aCdze3jLPJ38WxGHEyLAeCmxsbIDFYpkO4yx#EuTTnYou1kEBLFU7OzvQarWYR4xIECvAolxfX0O1WoVKpcI8n3Pm8XigVqsxz3em9QSLZ7FYpKyaty#FHVo0GqUPq9fr5HM6ndBsNilat9tNHyHFdCN8fn4OuVyOXsZc38D1VSqVaE1h9s2m#8HBAeTzedabz1RwWfBnzG+GIRaL0WLf399nV6QA+AcGzPwo+3wpYgAAAABJRU5ErkJgggAAAAMA#####wAAAAAAwAgAAAAAAADARYAAAAAAAAADYWY0AAAAHxAAAAAAAAAAAAAAAAAAAgAAAAAAAAAAAAFhAAAABAD#####AQAAAAAAAAMAAAAfAAAAAj#TMzMzMzMzAAAAAAUA#####wEAAAAAEAAAAQAAAAMAAAAfAD#wAAAAAAAAAAAABgD#####AAAAMgAAADEAAAAHAP####8BAAAAABAAAAAAAAAAAAAAAEAIAAAAAAAAAAAFAAIAAAAzAAAABwD#####AQAA#wAQAAAAAAAAAAAAAABACAAAAAAAAAAABQABAAAAMwAAAAEA#####wH#AAAAP#AAAAAAAABATYAAAAAAAAADaW01AAAAIA0AAAAAAAEAAAACAAAAAgAAAAAAAAAAAAAAABwAAAAcAAADGolQTkcNChoKAAAADUlIRFIAAAAcAAAAHAgGAAAAcg3flAAAAAFzUkdCAK7OHOkAAAAEZ0FNQQAAsY8L#GEFAAAACXBIWXMAABYlAAAWJQFJUiTwAAAABmJLR0QA#wD#AP+gvaeTAAACnUlEQVRIS92Wz0o6URTHj3#QhWaBulA0zRQUN4m4cBG0EIqC3JiLoJVv4Bv4AL2BCxc9gLQW3Api#kFUhIRwpRYGSn+l+c053RGnctJJN78PnJl7z#z5zrn33DkXOEYkEuEAYGE7PDxkTy6HAg#8CyAUCsHFxQWcnZ3B8#Mzun5ka2sLEokEZLNZCAQCcHNzw64sCMny8A9zV1dXrCdNPB7nTk5OOJvNxlmtVuZdDCXTJSaTCWtJ8#b2Bnq9HrrdLry8vIDD4WBXfkckuAyvr690fnh4IGGfz0f935AtOAuODIryQ8w885EtqFAoWIt#iVIJ4#GYks3r9TLvz8gS5OeeDMH5REPu7++h0+mA3++n#k#IEsSlgcsCo9RqtWTYxkg#Pj6g0WhAKpVid4uRJZhOpylp3t#fvxnOZzAYBJVKxe4WI0tQrVaDRqOh81cTEIb8K7KTRop5YshaBKX4PwQxoYTfZDgcpgxGa7fb6xE8OjqC09NTqiaFQoF5AY6Pj8XVIpPJsN7f2d3d5UwmE7X39vaohqKtJcLt7W0YjUYwGAyoXy6X6ZxMJlc#pHxkMBwOodfrMQ+A0WikanJ5eblaQbvdDo+PjxSdgNlsptqJvztkZYIulwuenp6oPgrodDowGAxwd3fHPCsSxDnr9#sisc3NTdoJ3N7eMs8nfxbEYcTIsB4KbGxsgMVimQ7jLH8S5NOdii7WQQEsVTs7O9BqtZhHjEgQK8CiXF9fQ7VahUqlwjyfc+bxeKBWqzHPd6b1BItnsVikrJq3L8UdWjQapQ+r1+vkczqd0Gw2KVq3200fIcV0I3x+fg65XI5exlzfwPVVKpVoTWH2zab#wcEB5PN51pvPVHBZ8GfMb4YhFovRYt#f32dXpAD4BwbM#Cj7fCliAAAAAElFTkSuQmCCAAAAAwD#####AAAAAADACAAAAAAAAMBFgAAAAAAAAANhZjUAAAAgEAAAAAAAAAAAAAAAAAACAAAAAAAAAAAAAWEAAAAEAP####8BAAAAAAAAAwAAACAAAAACP9MzMzMzMzMAAAAABQD#####AQAAAAAQAAABAAAAAwAAACAAP#AAAAAAAAAAAAAGAP####8AAAA5AAAAOAAAAAcA#####wEAAAAAEAAAAAAAAAAAAAAAQAgAAAAAAAAAAAUAAgAAADoAAAAHAP####8BAAD#ABAAAAAAAAAAAAAAAEAIAAAAAAAAAAAFAAEAAAA6AAAAEwD#####AAAAAAAQAAABAARzZWcyAAMAAAAmAAAAJwAAABMA#####wAAAAAAEAAAAQAEc2VnMwADAAAALgAAAC0AAAATAP####8AAAAAABAAAAEABHNlZzQAAwAAADUAAAA0AAAAEwD#####AAAAAAAQAAABAARzZWc1AAMAAAA8AAAAOwAAAAgA#####wH#AP8AEAAAAAAAAAAAAAAAQAgAAAAAAAAAAAUAAAAAFAAAAB0AAAARAP####8BAH8AABAAAAAAAAAAAAAAAEAIAAAAAAAAAANwdDAFAAFAZfAAAAAAAEBWtcKPXCj2AAAABQD#####AQB#AAEQAAABAAAAAwAAAEIBP#AAAAAAAAAAAAAFAP####8BAH8AARAAAAEAAAADAAAAQQA#8AAAAAAAAAAAAAkA#####wEAfwAAEAAAAAAAAAAAAAAAQAgAAAAAAAAAAAUAAAAARAAAAEP#####AAAAAQAJQ0NlcmNsZU9BAP####8BAH8AAAAAAwAAAEUAAAAUAAAACgD#####AAJhYQACMTAAAAACQCQAAAAAAAAAAAALAP####8AAABFAAAADAAAAEcAAAANAP####8BAH8AABAAAAAAAAAAAAAAAEAIAAAAAAAAAAAFAAAAABQAAABIAAAACwD#####AAAARQAAAA4AAAAMAAAARwAAAA0A#####wEAfwAAEAAAAAAAAAAAAAAAQAgAAAAAAAAAAAUAAAAAHQAAAEoAAAAPAP####8A#wD#AARhcmMxAAIAAABFAAAASQAAAEsAAAAGAP####8AAABEAAAATAAAAAcA#####wEAAAAAEAAAAAAAAAAAAAAAQAgAAAAAAAAAAnIxBQABAAAATQAAAAcA#####wD#AP8AEAAAAAAAAAAAAAAAQAgAAAAAAAAAAAUAAgAAAE0AAAADAP####8A#wD#AMAQAAAAAAAAQBQAAAAAAAAABGFmZDEAAABOEAAAAAAAAAAAAAAAAAACAAAAAAAAAAAAAWEAAAABAP####8BAAAAAL#wAAAAAAAAwCYAAAAAAAAABGltZDEAAABODQAAAAAAAQAAAAIAAAACAAAAAAAAAAAAAAAAHAAAABwAAAHmiVBORw0KGgoAAAANSUhEUgAAABwAAAAcCAMAAABF0y+mAAAAAXNSR0IArs4c6QAAAARnQU1BAACxjwv8YQUAAAB+UExURaNJpEdHR5GRkVBQUKSkpFZWVktLS6CgoFdXV1FRUU9PT19fX4CAgIODg4KCgn5+fjg4OHNzc8zMzDAwMCMjIx0dHXJyciIiIikpKR4eHhISEgsLCxMTEycnJy0tLaamprGxsSsrK3FxcbKysqqqqoWFhS4uLkBAQJ+fnwAAAJGrmG8AAAAqdFJOU#######################################################ADKo8FwAAAAJcEhZcwAAFiQAABYkAZsVxhQAAAC7SURBVDhPvdDZDoIwEAXQUdx3RQG3qFFj7v##oLNUutBnb8J0hpOmFAIoSA9hBPuuBwri2Udw4HpgOIpUcOx63jnhQ1zPSbDg0WsXA01wKrXVGGey02uMc3ddWugY4y+0tCWPK1v+imts+D553O5KvVEWsScIZ#HAr0FVFmu1Jnum#lo68tNFOkmppXSQzlIabVOki5Sr9Qma6T7Dm7WSO8+ghw2KFZ6aF#CWn1Y6U2xj#ceRYhga+Q8Avi3QZBhQD1WxAAAAAElFTkSuQmCCAAAACAD#####Af8A#wAQAAAAAAAAAAAAAABACAAAAAAAAAAABQAAAAAdAAAAHgAAAAUA#####wEAfwAAEAAAAQAAAAMAAABCAT#wAAAAAAAAAAAABQD#####AQB#AAAQAAABAAAAAwAAAFIAP#AAAAAAAAAAAAAJAP####8BAH8AABAAAAAAAAAAAAAAAEAIAAAAAAAAAAAFAAAAAFQAAABTAAAACgD#####AANhYTEAAjEwAAAAAkAkAAAAAAAAAAAACwD#####AAAAVQAAAAwAAABWAAAADQD#####AQB#AAAQAAAAAAAAAAAAAABACAAAAAAAAAAABQAAAAAdAAAAVwAAAAsA#####wAAAFUAAAAOAAAADAAAAFYAAAANAP####8BAH8AABAAAAAAAAAAAAAAAEAIAAAAAAAAAAAFAAAAAB4AAABZAAAADwD#####AP8A#wAEYXJjMgACAAAAVQAAAFgAAABaAAAABgD#####AAAAVAAAAFsAAAAHAP####8BAAAAABAAAAAAAAAAAAAAAEAIAAAAAAAAAAJyMgUAAQAAAFwAAAADAP####8A#wD#AMAQAAAAAAAAQBQAAAAAAAAABGFmZDIAAABdEAAAAAAAAAAAAAAAAAACAAAAAAAAAAAAAWEAAAABAP####8BAAAAAL#wAAAAAAAAwCYAAAAAAAAABGltZDIAAABdDQAAAAAAAQAAAAIAAAACAAAAAAAAAAAAAAAAHAAAABwAAAHmiVBORw0KGgoAAAANSUhEUgAAABwAAAAcCAMAAABF0y+mAAAAAXNSR0IArs4c6QAAAARnQU1BAACxjwv8YQUAAAB+UExURaNJpEdHR5GRkVBQUKSkpFZWVktLS6CgoFdXV1FRUU9PT19fX4CAgIODg4KCgn5+fjg4OHNzc8zMzDAwMCMjIx0dHXJyciIiIikpKR4eHhISEgsLCxMTEycnJy0tLaamprGxsSsrK3FxcbKysqqqqoWFhS4uLkBAQJ+fnwAAAJGrmG8AAAAqdFJOU#######################################################ADKo8FwAAAAJcEhZcwAAFiQAABYkAZsVxhQAAAC7SURBVDhPvdDZDoIwEAXQUdx3RQG3qFFj7v##oLNUutBnb8J0hpOmFAIoSA9hBPuuBwri2Udw4HpgOIpUcOx63jnhQ1zPSbDg0WsXA01wKrXVGGey02uMc3ddWugY4y+0tCWPK1v+imts+D553O5KvVEWsScIZ#HAr0FVFmu1Jnum#lo68tNFOkmppXSQzlIabVOki5Sr9Qma6T7Dm7WSO8+ghw2KFZ6aF#CWn1Y6U2xj#ceRYhga+Q8Avi3QZBhQD1WxAAAAAElFTkSuQmCCAAAACAD#####Af8A#wAQAAAAAAAAAAAAAABACAAAAAAAAAAABQAAAAAeAAAAHwAAAAUA#####wEAfwAAEAAAAQAAAAMAAABCAT#wAAAAAAAAAAAABQD#####AQB#AAAQAAABAAAAAwAAAGAAP#AAAAAAAAAAAAAJAP####8BAH8AABAAAAAAAAAAAAAAAEAIAAAAAAAAAAAFAAAAAGIAAABhAAAACgD#####AARhYTExAAIxMAAAAAJAJAAAAAAAAAAAAAsA#####wAAAGMAAAAMAAAAZAAAAA0A#####wEAfwAAEAAAAAAAAAAAAAAAQAgAAAAAAAAAAAUAAAAAHgAAAGUAAAALAP####8AAABjAAAADgAAAAwAAABkAAAADQD#####AQB#AAAQAAAAAAAAAAAAAABACAAAAAAAAAAABQAAAAAfAAAAZwAAAA8A#####wD#AP8ABGFyYzMAAgAAAGMAAABmAAAAaAAAAAYA#####wAAAGIAAABpAAAABwD#####AQAAAAAQAAAAAAAAAAAAAABACAAAAAAAAAACcjMFAAEAAABqAAAAAwD#####AP8A#wDAEAAAAAAAAEAUAAAAAAAAAARhZmQzAAAAaxAAAAAAAAAAAAAAAAAAAgAAAAAAAAAAAAFhAAAAAQD#####AQAAAAC#8AAAAAAAAMAmAAAAAAAAAARpbWQzAAAAaw0AAAAAAAEAAAACAAAAAgAAAAAAAAAAAAAAABwAAAAcAAAB5olQTkcNChoKAAAADUlIRFIAAAAcAAAAHAgDAAAARdMvpgAAAAFzUkdCAK7OHOkAAAAEZ0FNQQAAsY8L#GEFAAAAflBMVEWjSaRHR0eRkZFQUFCkpKRWVlZLS0ugoKBXV1dRUVFPT09fX1+AgICDg4OCgoJ+fn44ODhzc3PMzMwwMDAjIyMdHR1ycnIiIiIpKSkeHh4SEhILCwsTExMnJyctLS2mpqaxsbErKytxcXGysrKqqqqFhYUuLi5AQECfn58AAACRq5hvAAAAKnRSTlP######################################################wAyqPBcAAAACXBIWXMAABYkAAAWJAGbFcYUAAAAu0lEQVQ4T73Q2Q6CMBAF0FHcd0UBt6hRY+7##6CzVLrQZ2#CdIaTphQCKEgPYQT7rgcK4tlHcOB6YDiKVHDset454UNcz0mw4NFrFwNNcCq11RhnstNrjHN3XVroGOMvtLQljytb#oprbPg+edzuSr1RFrEnCGfxwK9BVRZrtSZ7pv5aOvLTRTpJqaV0kM5SGm1TpIuUq#UJmuk+w5u1kjvPoIcNihWemhfwlp9WOlNsY#3HkWIYGvkPAL4t0GQYUA9VsQAAAABJRU5ErkJgggAAAAgA#####wH#AP8AEAAAAAAAAAAAAAAAQAgAAAAAAAAAAAUAAAAAHwAAACAAAAAFAP####8BAH8AABAAAAEAAAADAAAAQgE#8AAAAAAAAAAAAAUA#####wEAfwAAEAAAAQAAAAMAAABuAD#wAAAAAAAAAAAACQD#####AQB#AAAQAAAAAAAAAAAAAABACAAAAAAAAAAABQAAAABwAAAAbwAAAAoA#####wAEYWExMgACMTAAAAACQCQAAAAAAAAAAAALAP####8AAABxAAAADAAAAHIAAAANAP####8BAH8AABAAAAAAAAAAAAAAAEAIAAAAAAAAAAAFAAAAAB8AAABzAAAACwD#####AAAAcQAAAA4AAAAMAAAAcgAAAA0A#####wEAfwAAEAAAAAAAAAAAAAAAQAgAAAAAAAAAAAUAAAAAIAAAAHUAAAAPAP####8A#wD#AARhcmM0AAIAAABxAAAAdAAAAHYAAAAGAP####8AAABwAAAAdwAAAAcA#####wEAAAAAEAAAAAAAAAAAAAAAQAgAAAAAAAAAAnI0BQABAAAAeAAAAAMA#####wD#AP8AwBAAAAAAAABAFAAAAAAAAAAEYWZkNAAAAHkQAAAAAAAAAAAAAAAAAAIAAAAAAAAAAAABYQAAAAEA#####wEAAAAAv#AAAAAAAADAJgAAAAAAAAAEaW1kNAAAAHkNAAAAAAABAAAAAgAAAAIAAAAAAAAAAAAAAAAcAAAAHAAAAeaJUE5HDQoaCgAAAA1JSERSAAAAHAAAABwIAwAAAEXTL6YAAAABc1JHQgCuzhzpAAAABGdBTUEAALGPC#xhBQAAAH5QTFRFo0mkR0dHkZGRUFBQpKSkVlZWS0tLoKCgV1dXUVFRT09PX19fgICAg4ODgoKCfn5+ODg4c3NzzMzMMDAwIyMjHR0dcnJyIiIiKSkpHh4eEhISCwsLExMTJycnLS0tpqamsbGxKysrcXFxsrKyqqqqhYWFLi4uQEBAn5+fAAAAkauYbwAAACp0Uk5T######################################################8AMqjwXAAAAAlwSFlzAAAWJAAAFiQBmxXGFAAAALtJREFUOE+90NkOgjAQBdBR3HdFAbeoUWPu##+gs1S60GdvwnSGk6YUAihID2EE+64HCuLZR3DgemA4ilRw7HreOeFDXM9JsODRaxcDTXAqtdUYZ7LTa4xzd11a6BjjL7S0JY8rW#6Ka2z4Pnnc7kq9URaxJwhn8cCvQVUWa7Ume6b+Wjry00U6SamldJDOUhptU6SLlKv1CZrpPsObtZI7z6CHDYoVnpoX8JafVjpTbGP9x5FiGBr5DwC+LdBkGFAPVbEAAAAASUVORK5CYIIAAAARAP####8B#wAAABAAAAAAAAAAAAAAAEAIAAAAAAAAAAdwdGxpYnJlCAABQHSoAAAAAABAcP1wo9cKPgAAABEA#####wH#AAAAEAAAAAAAAAAAAAAAQAgAAAAAAAAAAAgAAUBz6AAAAAAAQHWdcKPXCj4AAAARAP####8B#wAAABAAAAAAAAAAAAAAAEAIAAAAAAAAAAAIAAFAgcwAAAAAAEA51wo9cKPYAAAABAD#####AQAAAAAAAAIAAAB+AAAAAj#jMzMzMzMzAAAAABEA#####wH#AAAAEAAAAAAAAAAAAAAAQAgAAAAAAAAAAAgAAUCBNAAAAAAAQEBrhR64UewAAAAXAP####8B#wAAAAAAAgAAAH4AAACA#####wAAAAEADkNTdXJmYWNlRGlzcXVlAP####8B#wAAAARzdXJmAAAABAAAAIEAAAAYAP####8B#i5kAAVzdXJmMgAAAAQAAAB#AAAAAQD#####Af8AAABAMgAAAAAAAEAyAAAAAAAAAARjb3VwAAAAgA0AAAAAAAEAAAACAAAAAgAAAAAAAAAAAAAAADAAAAAwAAAGN4lQTkcNChoKAAAADUlIRFIAAAAwAAAAMAgDAAAAYNwJtQAAAAFzUkdCAK7OHOkAAAAEZ0FNQQAAsY8L#GEFAAABPlBMVEWlpaV9fX2MjIyIiIiFhYWLi4uEhISHh4d+fn6Tk5OAgICPj4+fn59MTEwzMzNKSkpTU1NNTU1QUFBPT09OTk5RUVFLS0s1NTWQkJA+Pj5VVVVaWlpZWVlXV1dYWFhWVlZbW1s#Pz9BQUFUVFRSUlKNjY1AQEA2NjagoKBcXFyhoaFdXV2BgYFJSUlISEiCgoKvr69hYWGGhoZ5eXleXl6SkpJwcHCKiopHR0enp6eRkZFCQkJzc3NfX193d3dycnI8PDxGRkZra2uWlpY9PT05OTk4ODiJiYl1dXU6Ojqbm5tERER4eHisrKyjo6M7OztDQ0NkZGRoaGhjY2Ozs7Opqal#f38pKSmOjo5xcXF8fHykpKQtLS1paWlnZ2eZmZltbW2Xl5d7e3uDg4N2dnaUlJRFRUVvb280NDQAAACzuKGtAAAAanRSTlP###########################################################################################################################################8AvOTc4wAAAAlwSFlzAAAuIwAALiMBeKU#dgAABAxJREFUSEulVv9rGzcUH41xRrJDoEMQIz3pYoGzC5MJDsWxIWuztbluzihdu4wu60qgdaf##x#Y50ly7NoMVvb5RefT+7zv752#il+I#0GYnU+nva63v4Wv7#sHhwffFKE1oToJTgglhdwCBUXBhZNQBDPhaEDaWGoa7Sy5LbREzjoazZNoIhwOnYecJ7LObsNB3pFxNOweCCNL6Y7IGNoBXlvjDZnhiiC8g0cOyovIJpwmB#UNNLm6EK4hrFf35WS97EtSwmZBcieZ8G0wa7k1sjCcbRQ1rMnqMEuEWwUVCPhzIFCLxJF9uDVOnSfCUYCyXROcBGZp1zgDp#EmHCXCfmiyx5#DwlHIk#FSG9XCmq6midCvkf0itQZkWR6mRRjLVrEF2U+ETnLutpF8hJ4QzmJ8PUg6A0qXXEJTJKFNpBRZX#+BVAIDFKqp9x5icKbxqiEPS8hIQ603WjVoljbGx1Uvxrc1ihsyYSo8bPu2tR6tJiT3lYS49mSuZnE+cosXMV5Ya8J+tqBaakleSjLN4MN8#mRYoT0NwrXN0xiNM0qgfxpHMsfQDwjawa2WuFmAGY0F#EKgV2fxfmTFE7ZAts5ZelQTabiOvLzLBODHp9fKtBfiGll8Do#OEZFT2cJkrE2N#uK8#HT1c2HA8uLZzS#lefaSpDWlcF0N5dIH61CdcLV8VYQ28OuVrOBAnYOeCJFmKQ4u0#H62eLFG374Da4At#r3dHahRn45aFHncV2adLxdyFytu3zc2j#TOQ0uEzqFtqyrCuVVyMvFOvIH#LUgeExhkgg9wW2JMWldeL58X2SgaDl6twr67iX2gBU56EfK+9YZb0kh3wXvT4YBcyxQue4Yc3PDefSrwhluS+SZyra6q0PlLnm3LM7i44Fd#IDCYW3U3yXCROhEQL#ZwYePRzfLPORoRIvN4mRLCqtRe1sK1ymeP4fmwPA6UQvsB95qaGgzPo3zpV8gD2P0ZygTV8EldJq36GZ0KM9vy#2L5Fm095xb6FONCShZ6nim4RPAM5keIIoHkOT3OaxjjU1RKj3lbs5i28BrUWNEX2FE8aPE0Efh#gUwo91FuMAw8iTKg0TYqzAbO5y0m3GwnLGSd6vmngPhMPAF324CL0BB#Nq2aQUaTwI9B8K5xG3aKptAZtPuM3yLa2#xeJsIpyjcrgVWnq1ohRKxRCNOEyGe4G5nMSUNsJJUo0tQeF3WfcRMO428YVHseJZIPL9YOp5bjQnxOL2EzhYfwG3wOxhB1AuWTYTDEWcZgeFbwN+6TWhSaaF4P0InFUKcD5ASeNrig7YFalqkClt8eJ9EMwHd+3dQwcugyvd8jaDc7oedMZt0vd5hv#xjWGEPryZdfy22fvqP+EJCjP8AchrbEGxjLIQAAAAASUVORK5CYIIAAAARAP####8B#wAAABAAAAAAAAAAAAAAAEAIAAAAAAAAAAdwdGNyb2l4CAABQHL4AAAAAABAel1wo9cKPgAAAAQA#####wH#AAAAAAACAAAAhQAAAAI#4zMzMzMzMwAAAAAFAP####8B#wAAARAAAAEAAAACAAAAhQE#8AAAAAAAAAAAAAYA#####wAAAIcAAACGAAAABwD#####Af8AAAAQAAAAAAAAAAAAAABACAAAAAAAAAAACAACAAAAiAAAAAcA#####wH#AAAAEAAAAAAAAAAAAAAAQAgAAAAAAAAAAAgAAQAAAIgAAAALAP####8AAACFAAAAAkBGgAAAAAAAAAAADQD#####Af8AAAAQAAAAAAAAAAAAAABACAAAAAAAAAAACAAAAACKAAAAi#####8AAAABABFDU3ltZXRyaWVDZW50cmFsZQD#####AAAAhQAAAA0A#####wH#AAAAEAAAAAAAAAAAAAAAQAgAAAAAAAAAAAgAAAAAjAAAAI0AAAALAP####8AAACFAAAAAkBWgAAAAAAAAAAADQD#####Af8AAAAQAAAAAAAAAAAAAABACAAAAAAAAAAACAAAAACMAAAAjwAAAA0A#####wH#AAAAEAAAAAAAAAAAAAAAQAgAAAAAAAAAAAgAAAAAjgAAAI8AAAATAP####8A#wAAABAAAAEAAAAEAAAAkAAAAJEAAAATAP####8A#wAAABAAAAEAAAAEAAAAjgAAAIwAAAAEAP####8AAAAAAAJjMQACAAAAGwAAAAI#8AAAAAAAAAAAAAAEAP####8AAAAAAAJjMgACAAAAJwAAAAI#8AAAAAAAAAAAAAAEAP####8AAAAAAAJjMwACAAAALgAAAAI#8AAAAAAAAAAAAAAEAP####8AAAAAAAJjNAACAAAANQAAAAI#8AAAAAAAAAAAAAAEAP####8AAAAAAAJjNQACAAAAPAAAAAI#8AAAAAAAAAAAAAAEAP####8AAAAAAANjZDEAAgAAAE4AAAACP#AAAAAAAAAAAAAABAD#####AAAAAAADY2QyAAIAAABdAAAAAj#wAAAAAAAAAAAAAAQA#####wAAAAAAA2NkMwACAAAAawAAAAI#8AAAAAAAAAAAAAAEAP####8AAAAAAANjZDQAAgAAAHkAAAACP#AAAAAAAAAAAAAACP##########'
    hj.style.border = 'solid black 1px'
    stor.mtgAppLecteur.removeAllDoc()
    stor.mtgAppLecteur.addDoc(svgId, txtFigure, true)
    stor.mtgAppLecteur.calculateAndDisplayAll(true)
    stor.ptdeb = stor.mtgAppLecteur.getPointPosition(svgId, '#ptdeb')
    stor.ptfin = stor.mtgAppLecteur.getPointPosition(svgId, '#ptfin')
    stor.mtgAppLecteur.setColor(svgId, '#LL', 0, 0, 0, true, 0)
    faisLigneCoupe(2)
    if (ds.Schema === 'Complété') {
      switch (stor.Lexo.question) {
        case 'Début':
          stor.listCapt[1].val = stor.afin
          stor.listCaptR[0].val = stor.afdur
          stor.listCapt[1].blok = true
          stor.listCaptR[0].blok = true
          break
        case 'Fin':
          stor.listCapt[0].val = stor.afdeb
          stor.listCaptR[0].val = stor.afdur
          stor.listCapt[0].blok = true
          stor.listCaptR[0].blok = true
          break
      }
      completeLigne()
    }
  }
  function supSch () {
    j3pEmpty(stor.lesdiv.schema)
    if (stor.aidetab !== undefined) {
      stor.aidetab.disable()
      stor.aidetab = undefined
    }
    j3pEmpty(stor.tabLi[0][2])
    j3pAjouteBouton(stor.lesdiv.schema, shcemaz, { className: '', value: 'Utiliser un schéma' })
  }
  function completeLigne () {
    for (let i = 0; i < stor.listCapt.length; i++) {
      if (stor.listCapt[i].val !== '') {
        stor.mtgAppLecteur.setText(svgId, '#af' + (i + 1), stor.listCapt[i].val, true)
      }
      if (stor.listCapt[i].blok) {
        stor.mtgAppLecteur.setVisible(svgId, '#c' + (i + 1), false, true)
      }
    }
    for (let i = 0; i < stor.listCaptR.length; i++) {
      if (stor.listCaptR[i].val !== '') {
        stor.mtgAppLecteur.setText(svgId, '#afd' + (i + 1), stor.listCaptR[i].val, true)
        stor.mtgAppLecteur.setVisible(svgId, stor.listCaptR[i].im, false, true)
        if (stor.listCaptR[i].blok) {
          stor.mtgAppLecteur.setVisible(svgId, '#cd' + (i + 1), false, true)
        }
        if (stor.yaco && stor.Lexo.question === 'Durée') {
          stor.mtgAppLecteur.setColor(svgId, '#af' + (i + 1), 50, 50, 200, true)
        }
      }
    }
  }
  function trad (x) {
    return { x: x.clientX, y: x.clientY }
  }
  function trad2 (x) {
    return { x: x.touches[0].clientX, y: x.touches[0].clientY }
  }
  function faisCoup (w) {
    if (stor.raz) return
    if (stor.okCoup) return
    stor.raz = true
    setTimeout(() => { stor.raz = false }, 100)
    const sauCap = j3pClone(stor.listCapt)
    const sauCapE = j3pClone(stor.listCaptR)
    let WherSeg = -1
    for (let i = 0; i < stor.nbcoup - 1; i++) {
      if (w.x > sauCap[i].x) WherSeg = i + 1
    }
    if (stor.nbcoup === 5) return
    faisLigneCoupe(Math.min(stor.nbcoup + 1, 5))
    for (let i = 0; i < stor.listCapt.length; i++) {
      if (i < WherSeg) {
        stor.listCapt[i].val = sauCap[i].val
        stor.listCapt[i].blok = sauCap[i].blok
        if (i < WherSeg - 1) {
          stor.listCaptR[i].val = sauCapE[i].val
          stor.listCaptR[i].blok = sauCapE[i].blok
        }
      } else if (i > WherSeg) {
        stor.listCapt[i].val = sauCap[i - 1].val
        stor.listCapt[i].blok = sauCap[i - 1].blok
        if (i < stor.listCaptR.length) {
          stor.listCaptR[i].val = sauCapE[i - 1].val
          stor.listCaptR[i].blok = sauCapE[i - 1].blok
        }
      }
    }
    completeLigne()
  }
  function faisLigneCoupe (n) {
    stor.mtgAppLecteur.setPointPosition(svgId, '#pt0', 20, -200 + (n - 2) * 70, false)
    stor.listCapt = []
    for (let i = 1; i < 6; i++) {
      stor.mtgAppLecteur.setVisible(svgId, '#pt' + i, false, true)
      stor.mtgAppLecteur.setVisible(svgId, '#c' + i, i <= n, true)
      stor.mtgAppLecteur.setVisible(svgId, '#im' + i, false, true)
      stor.mtgAppLecteur.setVisible(svgId, '#af' + i, i <= n, true)
      stor.mtgAppLecteur.setVisible(svgId, '#seg' + i, i <= n, true)
      if (i <= n) stor.mtgAppLecteur.setText(svgId, '#af' + i, '?', true)
      stor.mtgAppLecteur.setVisible(svgId, '#arc' + i, i < n, true)
      stor.mtgAppLecteur.setVisible(svgId, '#afd' + i, i < n, true)
      stor.mtgAppLecteur.setVisible(svgId, '#cd' + i, i < n, true)
      stor.mtgAppLecteur.setVisible(svgId, '#imd' + i, false, true)
      if (i < n) { stor.mtgAppLecteur.setText(svgId, '#afd' + i, '?', true) } else { stor.mtgAppLecteur.setText(svgId, '#afd' + i, '', true) }

      if (i <= n) {
        stor.mtgAppLecteur.setPointPosition(svgId, '#pt' + i, stor.ptdeb.x + (i - 1) * (stor.ptfin.x - stor.ptdeb.x) / (n - 1), stor.ptdeb.y, false)
        stor.listCapt.push({ x: stor.ptdeb.x + (i - 1) * (stor.ptfin.x - stor.ptdeb.x) / (n - 1), im: '#im' + i, af: '#af' + i, val: '', seg: '#seg' + i })
      } else {
        stor.mtgAppLecteur.setPointPosition(svgId, '#pt' + i, 5000, stor.ptdeb.y, false)
      }
    }
    stor.listCaptR = []
    stor.mtgAppLecteur.updateFigure(svgId)
    for (let i = 1; i < n; i++) {
      stor.listCaptR.push({ n: '#r' + i, im: '#imd' + i, af: '#afd' + i, val: '' })
    }
    for (let i = 0; i < stor.listCapt.length; i++) {
      stor.mtgAppLecteur.setLineStyle(svgId, stor.listCapt[i].seg, 0, 2, true)
      stor.mtgAppLecteur.setColor(svgId, stor.listCapt[i].seg, 0, 0, 0, true)
    }
    stor.nbcoup = n
    for (let i = 1; i < n + 1; i++) {
      stor.mtgAppLecteur.setColor(svgId, '#c' + i, 0, 0, 0, true, 0)
      if (i !== n) {
        stor.mtgAppLecteur.setColor(svgId, '#cd' + i, 0, 0, 0, true, 0)
      }
    }
    stor.mtgAppLecteur.updateDisplay(svgId)
    if (!stor.yaco) {
      for (let i = 1; i < n + 1; i++) {
        if (!stor.listCapt[i - 1].blok) {
          stor.mtgAppLecteur.addEventListener(svgId, '#c' + i, 'mousedown', makeFoncDown(i, true))
          stor.mtgAppLecteur.addEventListener(svgId, '#c' + i, 'touchstart', makeFoncDown(i, true))
          stor.mtgAppLecteur.addEventListener(svgId, '#c' + i, 'mousemove', makeFoncMove(i, true))
          stor.mtgAppLecteur.addEventListener(svgId, '#c' + i, 'mouseout', makeFoncOut(i, true))
        }
        if (i !== n) {
          if (!stor.listCaptR[i - 1].blok) {
            stor.mtgAppLecteur.addEventListener(svgId, '#cd' + i, 'mousedown', makeFoncDown(i, false))
            stor.mtgAppLecteur.addEventListener(svgId, '#cd' + i, 'touchstart', makeFoncDown(i, false))
            stor.mtgAppLecteur.addEventListener(svgId, '#cd' + i, 'mousemove', makeFoncMove(i, false))
            stor.mtgAppLecteur.addEventListener(svgId, '#cd' + i, 'mouseout', makeFoncOut(i, false))
          }
        }
      }
      stor.mtgAppLecteur.addEventListener(svgId, '#LL', 'mouseout', () => { faisGros(false) })
      stor.mtgAppLecteur.addEventListener(svgId, '#LL', 'mouseover', () => { faisGros(true) })
      stor.mtgAppLecteur.addEventListener(svgId, '#LL', 'mousedown', (ev) => { faisCoup(trad(ev)) })
      stor.mtgAppLecteur.addEventListener(svgId, '#LL', 'touchstart', (ev) => { faisCoup(trad2(ev)) })
    }

    stor.mtgAppLecteur.setVisible(svgId, '#surf', false, true)
    stor.mtgAppLecteur.setVisible(svgId, '#surf2', false, true)
    stor.okCoup = false
    if (stor.nbcoup > 2) {
      stor.mtgAppLecteur.setVisible(svgId, '#coup', true, true)
      stor.mtgAppLecteur.addEventListener(svgId, '#coup', 'mouseover', () => {
        if (stor.raz) return
        stor.mtgAppLecteur.getDoc(svgId).defaultCursor = 'pointer'
        stor.mtgAppLecteur.setVisible(svgId, '#surf', true, true)
      })
      stor.mtgAppLecteur.addEventListener(svgId, '#coup', 'mouseout', () => {
        if (stor.raz) return
        stor.mtgAppLecteur.getDoc(svgId).defaultCursor = ''
        stor.mtgAppLecteur.setVisible(svgId, '#surf', false, true)
      })
      stor.mtgAppLecteur.addEventListener(svgId, '#coup', 'mousedown', () => {
        if (stor.raz) return
        stor.okCoup = !stor.okCoup
        if (stor.okCoup) {
          stor.mtgAppLecteur.setVisible(svgId, '#surf2', true, true)
          for (let i = 1; i < stor.listCapt.length - 1; i++) {
            stor.mtgAppLecteur.setLineStyle(svgId, '#seg' + (i + 1), 0, 12, true)
            stor.mtgAppLecteur.setColor(svgId, '#seg' + (i + 1), 255, 0, 0, true)
          }
        } else {
          stor.mtgAppLecteur.setVisible(svgId, '#surf2', false, true)
          for (let i = 1; i < stor.listCapt.length - 1; i++) {
            stor.mtgAppLecteur.setLineStyle(svgId, stor.listCapt[i].seg, 0, 2, true)
            stor.mtgAppLecteur.setColor(svgId, stor.listCapt[i].seg, 0, 0, 0, true)
          }
        }
      })
    } else {
      stor.mtgAppLecteur.setVisible(svgId, '#coup', false, true)
    }
  }
  function faisZed () {
    stor.LL = false
    stor.raz = false
    stor.okCoup = false
    j3pDetruit('modale')
    j3pDetruit('j3pmasque')
    j3pEmpty(stor.lesdiv.schema)
    svgId = j3pGetNewId()
    stor.svgId = svgId
    const jkl = addDefaultTable(stor.lesdiv.schema, 1, 2)
    stor.bos = j3pAjouteBouton(jkl[0][1], supSch, { value: 'Supprimer' })
    const hj = j3pCreeSVG(jkl[0][0], { id: svgId, width: 450, height: 220 })
    const txtFigure = 'TWF0aEdyYXBoSmF2YTEuMAAAABM+TMzNAAJmcv###wEA#wEAAAAAAAAAAANLAAACfwAAAQEAAAAAAAAAAQAAADX#####AAAAAQAKQ0NhbGNDb25zdAD#####AAJwaQAWMy4xNDE1OTI2NTM1ODk3OTMyMzg0Nv####8AAAABAApDQ29uc3RhbnRlQAkh+1RELRj#####AAAAAQAKQ1BvaW50QmFzZQD#####AQAAAAAOAAFVAMAkAAAAAAAAQBAAAAAAAAAAAAUAAEAwOJN0vGp#QDA4k3S8an######AAAAAQAUQ0Ryb2l0ZURpcmVjdGlvbkZpeGUA#####wEAAAAAEAAAAQAAAAEAAAABAT#wAAAAAAAA#####wAAAAEAD0NQb2ludExpZURyb2l0ZQD#####AQAAAAAOAAFWAMAAAAAAAAAAQBAAAAAAAAAAAAUAAUBAOJN0vGp#AAAAAv####8AAAABAAhDU2VnbWVudAD#####AQAAAAAQAAABAAAAAQAAAAEAAAAD#####wAAAAEAB0NNaWxpZXUA#####wEAAAAAEAAAAAAAAAAAAAAAQAgAAAAAAAAAAAUAAAAAAQAAAAP#####AAAAAgAMQ0NvbW1lbnRhaXJlAP####8BAAAAAAAAAAAAAAAAQBgAAAAAAAAAAAAAAAUMAAAAAAABAAAAAAAAAAEAAAAAAAAAAAABMf####8AAAABAAlDTG9uZ3VldXIA#####wAAAAEAAAADAAAAAgD#####Af8AAAAQAAAAAAAAAAAAAABACAAAAAAAAAADcHQxCAABQE3AAAAAAABAQ+uFHrhR6AAAAAMA#####wH#AAABEAAAAQAAAAQAAAAIAT#wAAAAAAAAAAAABAD#####Af8AAAAQAAAAAAAAAAAAAABACAAAAAAAAAADcHQyCAABQHIAAAAAAAAAAAAJAAAAAwD#####Af8AAAEQAAABAAAABAAAAAgAP#AAAAAAAAAAAAAEAP####8B#wAAABAAAAAAAAAAAAAAAEAIAAAAAAAAAANwdDMIAAFAXYKPXCj1xAAAAAsAAAAFAP####8AAAD#ABAAAAEAAAADAAAADAAAAAr#####AAAAAQAIQ1ZlY3RldXIA#####wH#AAAAEAAAAQAAAAQAAAAIAAAACgD#####AAAAAQANQ1RyYW5zUGFyVmVjdAD#####AAAADv####8AAAABAAtDUG9pbnRJbWFnZQD#####Af8AAAAQAAAAAAAAAAAAAABACAAAAAAAAAADcHQ0CAAAAAAMAAAADwAAAAUA#####wAAAP8AEAAAAQAAAAMAAAAMAAAAEAAAAAUA#####wAAAP8AEAAAAQAAAAMAAAAIAAAACv####8AAAACAAlDQ2VyY2xlT1IA#####wEAAP8AAAADAAAAEAAAAAE#8AAAAAAAAAD#####AAAAAQAQQ0ludERyb2l0ZUNlcmNsZQD#####AAAAEQAAABP#####AAAAAQAQQ1BvaW50TGllQmlwb2ludAD#####AAAA#wAQAAAAAAAAAAAAAABACAAAAAAAAAAACAABAAAAFAAAAA4A#####wEAAP8AEAAAAAAAAAAAAAAAQAgAAAAAAAAAAAgAAgAAABT#####AAAAAQAJQ1JvdGF0aW9uAP####8AAAAQAAAAAUA+AAAAAAAAAAAACwD#####AQAA#wAQAAAAAAAAAAAAAABACAAAAAAAAAAACAAAAAAWAAAAF#####8AAAABAA9DU3ltZXRyaWVBeGlhbGUA#####wAAABEAAAALAP####8BAAD#ABAAAAAAAAAAAAAAAEAIAAAAAAAAAAAIAAAAABgAAAAZAAAABQD#####AAAA#wAQAAABAAAAAwAAABoAAAAQAAAABQD#####AAAA#wAQAAABAAAAAwAAABgAAAAQ#####wAAAAIABkNJbWFnZQD#####AAAA#wDAMgAAAAAAAEBFgAAAAAAAAANpbTEAAAAIDQAAAAAAAQAAAAIAAAABAAAAAAAAAAAAAAAAHAAAABwAAAMaiVBORw0KGgoAAAANSUhEUgAAABwAAAAcCAYAAAByDd+UAAAAAXNSR0IArs4c6QAAAARnQU1BAACxjwv8YQUAAAAJcEhZcwAAFiUAABYlAUlSJPAAAAAGYktHRAD#AP8A#6C9p5MAAAKdSURBVEhL3ZbPSjpRFMePf9CFZoG6UDTNFBQ3ibhwEbQQioLcmIuglW#gG#gAvYELFz2AtBbcCmL+QVSEhHClFgZKf6X5zTndEady0kk3vw+cmXvP#PnOuffcORc4RiQS4QBgYTs8PGRPLocCD#wLIBQKwcXFBZydncHz8zO6fmRrawsSiQRks1kIBAJwc3PDriwIyfLwD3NXV1esJ008HudOTk44m83GWa1W5l0MJdMlJpMJa0nz9vYGer0eut0uvLy8gMPhYFd+RyS4DK+vr3R+eHggYZ#PR#3fkC04C44MivJDzDzzkS2oUChYi3+JUgnj8ZiSzev1Mu#PyBLk554MwflEQ+7v76HT6YDf76f+T8gSxKWBywKj1Gq1ZNjGSD8+PqDRaEAqlWJ3i5ElmE6nKWne39+#Gc5nMBgElUrF7hYjS1CtVoNGo6HzVxMQhvwrspNGinliyFoEpfg#BDGhhN9kOBymDEZrt9vrETw6OoLT01OqJoVCgXkBjo+PxdUik8mw3t#Z3d3lTCYTtff29qiGoq0lwu3tbRiNRjAYDKhfLpfpnEwmVz+kfGQwHA6h1+sxD4DRaKRqcnl5uVpBu90Oj4+PFJ2A2Wym2om#O2Rlgi6XC56enqg+Cuh0OjAYDHB3d8c8KxLEOev3+yKxzc1N2gnc3t4yzyd#FsRhxMiwHgpsbGyAxWKZDuMsfxLk052KLtZBASxVOzs70Gq1mEeMSBArwKJcX19DtVqFSqXCPJ9z5vF4oFarMc93pvUEi2exWKSsmrcvxR1aNBqlD6vX6+RzOp3QbDYpWrfbTR8hxXQjfH5+Drlcjl7GXN#A9VUqlWhNYfbNpv#BwQHk83nWm89UcFnwZ8xvhiEWi9Fi39#fZ1ekAPgHBsz8KPt8KWIAAAAASUVORK5CYIIAAAARAP####8AAAD#AEA8AAAAAAAAQEUAAAAAAAAAA2ltMgAAAAoNAAAAAAABAAAAAgAAAAEAAAAAAAAAAAAAAAAcAAAAHAAAAxqJUE5HDQoaCgAAAA1JSERSAAAAHAAAABwIBgAAAHIN35QAAAABc1JHQgCuzhzpAAAABGdBTUEAALGPC#xhBQAAAAlwSFlzAAAWJQAAFiUBSVIk8AAAAAZiS0dEAP8A#wD#oL2nkwAAAp1JREFUSEvdls9KOlEUx49#0IVmgbpQNM0UFDeJuHARtBCKgtyYi6CVb+Ab+AC9gQsXPYC0FtwKYv5BVISEcKUWBkp#pfnNOd0Rp3LSSTe#D5yZe8#8+c6599w5FzhGJBLhAGBhOzw8ZE8uhwIP#AsgFArBxcUFnJ2dwfPzM7p+ZGtrCxKJBGSzWQgEAnBzc8OuLAjJ8vAPc1dXV6wnTTwe505OTjibzcZZrVbmXQwl0yUmkwlrSfP29gZ6vR663S68vLyAw+FgV35HJLgMr6+vdH54eCBhn89H#d+QLTgLjgyK8kPMPPORLahQKFiLf4lSCePxmJLN6#Uy78#IEuTnngzB+URD7u#vodPpgN#vp#5PyBLEpYHLAqPUarVk2MZIPz4+oNFoQCqVYneLkSWYTqcpad7f378ZzmcwGASVSsXuFiNLUK1Wg0ajofNXExCG#Cuyk0aKeWLIWgSl+D8EMaGE32Q4HKYMRmu32+sRPDo6gtPTU6omhUKBeQGOj4#F1SKTybDe39nd3eVMJhO19#b2qIairSXC7e1tGI1GMBgMqF8ul+mcTCZXP6R8ZDAcDqHX6zEPgNFopGpyeXm5WkG73Q6Pj48UnYDZbKbaib87ZGWCLpcLnp6eqD4K6HQ6MBgMcHd3xzwrEsQ56#f7IrHNzU3aCdze3jLPJ38WxGHEyLAeCmxsbIDFYpkO4yx#EuTTnYou1kEBLFU7OzvQarWYR4xIECvAolxfX0O1WoVKpcI8n3Pm8XigVqsxz3em9QSLZ7FYpKyaty#FHVo0GqUPq9fr5HM6ndBsNilat9tNHyHFdCN8fn4OuVyOXsZc38D1VSqVaE1h9s2m#8HBAeTzedabz1RwWfBnzG+GIRaL0WLf399nV6QA+AcGzPwo+3wpYgAAAABJRU5ErkJgggAAABEA#####wAAAP8AQDkAAAAAAABARAAAAAAABAADaW00AAAAEA0AAAAAAAEAAAACAAAAAQAAAAAAAAAAAAAAABwAAAAcAAADGolQTkcNChoKAAAADUlIRFIAAAAcAAAAHAgGAAAAcg3flAAAAAFzUkdCAK7OHOkAAAAEZ0FNQQAAsY8L#GEFAAAACXBIWXMAABYlAAAWJQFJUiTwAAAABmJLR0QA#wD#AP+gvaeTAAACnUlEQVRIS92Wz0o6URTHj3#QhWaBulA0zRQUN4m4cBG0EIqC3JiLoJVv4Bv4AL2BCxc9gLQW3Api#kFUhIRwpRYGSn+l+c053RGnctJJN78PnJl7z#z5zrn33DkXOEYkEuEAYGE7PDxkTy6HAg#8CyAUCsHFxQWcnZ3B8#Mzun5ka2sLEokEZLNZCAQCcHNzw64sCMny8A9zV1dXrCdNPB7nTk5OOJvNxlmtVuZdDCXTJSaTCWtJ8#b2Bnq9HrrdLry8vIDD4WBXfkckuAyvr690fnh4IGGfz0f935AtOAuODIryQ8w885EtqFAoWIt#iVIJ4#GYks3r9TLvz8gS5OeeDMH5REPu7++h0+mA3++n#k#IEsSlgcsCo9RqtWTYxkg#Pj6g0WhAKpVid4uRJZhOpylp3t#fvxnOZzAYBJVKxe4WI0tQrVaDRqOh81cTEIb8K7KTRop5YshaBKX4PwQxoYTfZDgcpgxGa7fb6xE8OjqC09NTqiaFQoF5AY6Pj8XVIpPJsN7f2d3d5UwmE7X39vaohqKtJcLt7W0YjUYwGAyoXy6X6ZxMJlc#pHxkMBwOodfrMQ+A0WikanJ5eblaQbvdDo+PjxSdgNlsptqJvztkZYIulwuenp6oPgrodDowGAxwd3fHPCsSxDnr9#sisc3NTdoJ3N7eMs8nfxbEYcTIsB4KbGxsgMVimQ7jLH8S5NOdii7WQQEsVTs7O9BqtZhHjEgQK8CiXF9fQ7VahUqlwjyfc+bxeKBWqzHPd6b1BItnsVikrJq3L8UdWjQapQ+r1+vkczqd0Gw2KVq3200fIcV0I3x+fg65XI5exlzfwPVVKpVoTWH2zab#wcEB5PN51pvPVHBZ8GfMb4YhFovRYt#f32dXpAD4BwbM#Cj7fCliAAAAAElFTkSuQmCCAAAAEQD#####AAAA#wDAMf#######kBIAAAAAAAEAANpbTMAAAAMDQAAAAAAAQAAAAIAAAABAAAAAAAAAAAAAAAAHAAAABwAAAMaiVBORw0KGgoAAAANSUhEUgAAABwAAAAcCAYAAAByDd+UAAAAAXNSR0IArs4c6QAAAARnQU1BAACxjwv8YQUAAAAJcEhZcwAAFiUAABYlAUlSJPAAAAAGYktHRAD#AP8A#6C9p5MAAAKdSURBVEhL3ZbPSjpRFMePf9CFZoG6UDTNFBQ3ibhwEbQQioLcmIuglW#gG#gAvYELFz2AtBbcCmL+QVSEhHClFgZKf6X5zTndEady0kk3vw+cmXvP#PnOuffcORc4RiQS4QBgYTs8PGRPLocCD#wLIBQKwcXFBZydncHz8zO6fmRrawsSiQRks1kIBAJwc3PDriwIyfLwD3NXV1esJ008HudOTk44m83GWa1W5l0MJdMlJpMJa0nz9vYGer0eut0uvLy8gMPhYFd+RyS4DK+vr3R+eHggYZ#PR#3fkC04C44MivJDzDzzkS2oUChYi3+JUgnj8ZiSzev1Mu#PyBLk554MwflEQ+7v76HT6YDf76f+T8gSxKWBywKj1Gq1ZNjGSD8+PqDRaEAqlWJ3i5ElmE6nKWne39+#Gc5nMBgElUrF7hYjS1CtVoNGo6HzVxMQhvwrspNGinliyFoEpfg#BDGhhN9kOBymDEZrt9vrETw6OoLT01OqJoVCgXkBjo+PxdUik8mw3t#Z3d3lTCYTtff29qiGoq0lwu3tbRiNRjAYDKhfLpfpnEwmVz+kfGQwHA6h1+sxD4DRaKRqcnl5uVpBu90Oj4+PFJ2A2Wym2om#O2Rlgi6XC56enqg+Cuh0OjAYDHB3d8c8KxLEOev3+yKxzc1N2gnc3t4yzyd#FsRhxMiwHgpsbGyAxWKZDuMsfxLk052KLtZBASxVOzs70Gq1mEeMSBArwKJcX19DtVqFSqXCPJ9z5vF4oFarMc93pvUEi2exWKSsmrcvxR1aNBqlD6vX6+RzOp3QbDYpWrfbTR8hxXQjfH5+Drlcjl7GXN#A9VUqlWhNYfbNpv#BwQHk83nWm89UcFnwZ8xvhiEWi9Fi39#fZ1ekAPgHBsz8KPt8KWIAAAAASUVORK5CYIIAAAAGAP####8BAAD#ABAAAAAAAAAAAAAAAEAIAAAAAAAAAAJyMQgAAAAACAAAAAoAAAARAP####8AAAD#AAAAAAAAAAAAQEOAAAAAAAAABGltZDEAAAAhDQAAAAAAAQAAAAIAAAABAAAAAAAAAAAAAAAAHAAAABwAAAHmiVBORw0KGgoAAAANSUhEUgAAABwAAAAcCAMAAABF0y+mAAAAAXNSR0IArs4c6QAAAARnQU1BAACxjwv8YQUAAAB+UExURaNJpEdHR5GRkVBQUKSkpFZWVktLS6CgoFdXV1FRUU9PT19fX4CAgIODg4KCgn5+fjg4OHNzc8zMzDAwMCMjIx0dHXJyciIiIikpKR4eHhISEgsLCxMTEycnJy0tLaamprGxsSsrK3FxcbKysqqqqoWFhS4uLkBAQJ+fnwAAAJGrmG8AAAAqdFJOU#######################################################ADKo8FwAAAAJcEhZcwAAFiQAABYkAZsVxhQAAAC7SURBVDhPvdDZDoIwEAXQUdx3RQG3qFFj7v##oLNUutBnb8J0hpOmFAIoSA9hBPuuBwri2Udw4HpgOIpUcOx63jnhQ1zPSbDg0WsXA01wKrXVGGey02uMc3ddWugY4y+0tCWPK1v+imts+D553O5KvVEWsScIZ#HAr0FVFmu1Jnum#lo68tNFOkmppXSQzlIabVOki5Sr9Qma6T7Dm7WSO8+ghw2KFZ6aF#CWn1Y6U2xj#ceRYhga+Q8Avi3QZBhQD1WxAAAAAElFTkSuQmCCAAAABgD#####AQAA#wAQAAAAAAAAAAAAAABACAAAAAAAAAACcjIIAAAAAAwAAAAKAAAABgD#####AQAA#wAQAAAAAAAAAAAAAABACAAAAAAAAAACcjMIAAAAAAwAAAAQAAAAEQD#####AAAA#wBANAAAAAAAAEBHgAAAAAAAAARpbWQzAAAAJA0AAAAAAAEAAAACAAAAAQAAAAAAAAAAAAAAABwAAAAcAAAB5olQTkcNChoKAAAADUlIRFIAAAAcAAAAHAgDAAAARdMvpgAAAAFzUkdCAK7OHOkAAAAEZ0FNQQAAsY8L#GEFAAAAflBMVEWjSaRHR0eRkZFQUFCkpKRWVlZLS0ugoKBXV1dRUVFPT09fX1+AgICDg4OCgoJ+fn44ODhzc3PMzMwwMDAjIyMdHR1ycnIiIiIpKSkeHh4SEhILCwsTExMnJyctLS2mpqaxsbErKytxcXGysrKqqqqFhYUuLi5AQECfn58AAACRq5hvAAAAKnRSTlP######################################################wAyqPBcAAAACXBIWXMAABYkAAAWJAGbFcYUAAAAu0lEQVQ4T73Q2Q6CMBAF0FHcd0UBt6hRY+7##6CzVLrQZ2#CdIaTphQCKEgPYQT7rgcK4tlHcOB6YDiKVHDset454UNcz0mw4NFrFwNNcCq11RhnstNrjHN3XVroGOMvtLQljytb#oprbPg+edzuSr1RFrEnCGfxwK9BVRZrtSZ7pv5aOvLTRTpJqaV0kM5SGm1TpIuUq#UJmuk+w5u1kjvPoIcNihWemhfwlp9WOlNsY#3HkWIYGvkPAL4t0GQYUA9VsQAAAABJRU5ErkJgggAAABEA#####wAAAP8AQDEAAAAAAABAQgAAAAAAAAAEaW1kMgAAACMNAAAAAAABAAAAAgAAAAEAAAAAAAAAAAAAAAAcAAAAHAAAAeaJUE5HDQoaCgAAAA1JSERSAAAAHAAAABwIAwAAAEXTL6YAAAABc1JHQgCuzhzpAAAABGdBTUEAALGPC#xhBQAAAH5QTFRFo0mkR0dHkZGRUFBQpKSkVlZWS0tLoKCgV1dXUVFRT09PX19fgICAg4ODgoKCfn5+ODg4c3NzzMzMMDAwIyMjHR0dcnJyIiIiKSkpHh4eEhISCwsLExMTJycnLS0tpqamsbGxKysrcXFxsrKyqqqqhYWFLi4uQEBAn5+fAAAAkauYbwAAACp0Uk5T######################################################8AMqjwXAAAAAlwSFlzAAAWJAAAFiQBmxXGFAAAALtJREFUOE+90NkOgjAQBdBR3HdFAbeoUWPu##+gs1S60GdvwnSGk6YUAihID2EE+64HCuLZR3DgemA4ilRw7HreOeFDXM9JsODRaxcDTXAqtdUYZ7LTa4xzd11a6BjjL7S0JY8rW#6Ka2z4Pnnc7kq9URaxJwhn8cCvQVUWa7Ume6b+Wjry00U6SamldJDOUhptU6SLlKv1CZrpPsObtZI7z6CHDYoVnpoX8JafVjpTbGP9x5FiGBr5DwC+LdBkGFAPVbEAAAAASUVORK5CYIIAAAAHAP####8AAAAAAEA0AAAAAAAAwCgAAAAAAAAAA2FmMgAAAAoQAAAAAAAAAAAAAAAAAAEAAAAAAAAAAAABYQAAAAcA#####wAAAAAAwDAAAAAAAADAKAAAAAAAAAADYWYxAAAACBAAAAAAAAIAAAAAAAAAAQAAAAAAAAAAAAFhAAAABwD#####AAAAAADAMQAAAAAAAMAiAAAAAAAAAANhZjMAAAAMEAAAAAAAAgAAAAAAAAABAAAAAAAAAAAAAWEAAAAHAP####8AAAAAAEAxAAAAAAAAwCoAAAAAAAAAA2FmNAAAABAQAAAAAAAAAAAAAAAAAAEAAAAAAAAAAAABYQAAAAcA#####wD#AP8AAAAAAAAAAADAOwAAAAAAAAAEYWZkMQAAACEQAAAAAAABAAAAAAAAAAEAAAAAAAAAAAABYQAAAAcA#####wD#AP8AwDAAAAAAAAAAAAAAAAAAAAAEYWZkMgAAACMQAAAAAAACAAAAAgAAAAEAAAAAAAAAAAABYQAAAAcA#####wD#AP8AwAAAAAAAAABAEAAAAAAAAAAEYWZkMwAAACQQAAAAAAABAAAAAAAAAAEAAAAAAAAAAAABYQAAAAwA#####wAAAAAAAmMxAAIAAAAIAAAAAT#zMzMzMzMzAAAAAAwA#####wAAAAAAA2NkMQACAAAAIQAAAAE#7MzMzMzMzQAAAAAMAP####8AAAAAAAJjMgACAAAACgAAAAE#8zMzMzMzMwAAAAAMAP####8AAAAAAAJjMwACAAAADAAAAAE#8zMzMzMzMwAAAAAMAP####8AAAAAAANjZDMAAgAAACQAAAABP+zMzMzMzM0AAAAADAD#####AAAAAAADY2QyAAIAAAAjAAAAAT#szMzMzMzNAAAAAAwA#####wAAAAAAAmM0AAIAAAAQAAAAAT#zMzMzMzMzAAAAAAf##########w=='
    hj.style.border = 'solid black 1px'
    stor.mtgAppLecteur.removeAllDoc()
    stor.mtgAppLecteur.addDoc(svgId, txtFigure, true)
    stor.mtgAppLecteur.calculateAndDisplayAll(true)

    stor.nbcoup = 4
    stor.listCapt = []
    stor.listCaptR = []
    for (let i = 1; i < 5; i++) {
      stor.mtgAppLecteur.setColor(svgId, '#c' + i, 0, 0, 0, true, 0)
      stor.mtgAppLecteur.setVisible(svgId, '#im' + i, false, true)
      stor.mtgAppLecteur.setVisible(svgId, '#af' + i, true, true)
      stor.mtgAppLecteur.setText(svgId, '#af' + i, '?', true)
      stor.listCapt.push({ im: '#im' + i, af: '#af' + i, val: '' })
      if (i !== 4) {
        stor.mtgAppLecteur.setColor(svgId, '#cd' + i, 0, 0, 0, true, 0)
        stor.mtgAppLecteur.setVisible(svgId, '#afd' + i, true, true)
        stor.mtgAppLecteur.setVisible(svgId, '#imd' + i, false, true)
        stor.mtgAppLecteur.setText(svgId, '#afd' + i, '?', true)
        stor.listCaptR.push({ n: '#r' + i, im: '#imd' + i, af: '#afd' + i, val: '' })
      }
    }

    stor.mtgAppLecteur.updateFigure(svgId)

    if (ds.Schema === 'Complété' || stor.yaco === true) {
      stor.listCapt[3].val = stor.afin
      stor.listCapt[0].val = stor.afdeb
      stor.listCapt[0].blok = true
      stor.listCapt[3].blok = true
      stor.listCapt[2].val = stor.har + 'h00'
      let gg = stor.hdep
      if (stor.mindep > 0) gg++
      if (gg === 24) gg = 0
      stor.listCapt[1].val = gg + 'h00'
      stor.listCapt[2].blok = true
      stor.listCapt[1].blok = true
      if (stor.yaco) {
        if (stor.mindep === 0) { stor.listCaptR[0].val = '0 minute' }
        if (stor.mindep === 59) { stor.listCaptR[0].val = '1 minute' }
        if (stor.mindep !== 0 && stor.mindep !== 59) { stor.listCaptR[0].val = (60 - stor.mindep) + ' minutes' }
        let lh = stor.h
        if (stor.minar >= stor.mindep) lh--
        if (lh === 0 || lh === 1) { stor.listCaptR[1].val = lh + ' heure' } else { stor.listCaptR[1].val = lh + ' heures' }
        if (stor.minar === 0 || stor.minar === 1) { stor.listCaptR[2].val = stor.minar + ' minute' } else { stor.listCaptR[2].val = stor.minar + ' minutes' }
        stor.listCaptR[0].blok = true
        stor.listCaptR[1].blok = true
        stor.listCaptR[2].blok = true
      }
      completeLigne()
    }

    /// //////
    if (!stor.yaco) {
      for (let i = 1; i < 5; i++) {
        if (!stor.listCapt[0].blok) {
          stor.mtgAppLecteur.addEventListener(svgId, '#c' + i, 'mousedown', makeFoncDown(i, true))
          stor.mtgAppLecteur.addEventListener(svgId, '#c' + i, 'touchstart', makeFoncDown(i, true))
          stor.mtgAppLecteur.addEventListener(svgId, '#c' + i, 'mousemove', makeFoncMove(i, true))
          stor.mtgAppLecteur.addEventListener(svgId, '#c' + i, 'mouseout', makeFoncOut(i, true))
        }
        if (i !== 4) {
          if (!stor.listCaptR[0].blok) {
            stor.mtgAppLecteur.addEventListener(svgId, '#cd' + i, 'mousedown', makeFoncDown(i, false))
            stor.mtgAppLecteur.addEventListener(svgId, '#cd' + i, 'touchstart', makeFoncDown(i, false))
            stor.mtgAppLecteur.addEventListener(svgId, '#cd' + i, 'mousemove', makeFoncMove(i, false))
            stor.mtgAppLecteur.addEventListener(svgId, '#cd' + i, 'mouseout', makeFoncOut(i, false))
          }
        }
      }
    }
  }
  function makeFoncDown (i, b) {
    if (b) {
      return function () {
        if (stor.raz) return
        if (!stor.okCoup) {
          faisEdit(stor.listCapt[i - 1])
        } else {
          if (i < 2 || i === stor.nbcoup) return
          stor.raz = true
          setTimeout(() => { stor.raz = false }, 200)
          const sauv = j3pClone(stor.listCapt)
          const sauvR = j3pClone(stor.listCaptR)
          faisLigneCoupe(stor.nbcoup - 1)
          sauv.splice(i - 1, 1)
          sauvR.splice(i - 1, 1)
          stor.listCapt = j3pClone(sauv)
          stor.listCaptR = j3pClone(sauvR)
          stor.listCaptR[i - 2].val = ''
          completeLigne()
        }
      }
    } else {
      return function () {
        if (stor.raz) return
        if (stor.okCoup) return
        faisEdit(stor.listCaptR[i - 1])
      }
    }
  }
  function makeFoncMove (i, b) {
    if (b) {
      return function () {
        if (stor.raz) return
        stor.mtgAppLecteur.getDoc(svgId).defaultCursor = 'pointer'
        if (!stor.okCoup) {
          stor.mtgAppLecteur.setVisible(svgId, '#im' + i, true, true)
        }
      }
    } else {
      return function () {
        if (stor.raz) return
        if (stor.okCoup) return
        stor.mtgAppLecteur.setVisible(svgId, '#imd' + i, true, true)
        stor.mtgAppLecteur.getDoc(svgId).defaultCursor = 'pointer'
      }
    }
  }
  function makeFoncOut (i, b) {
    if (b) {
      return function () {
        if (stor.raz) return
        if (!stor.okCoup) {
          stor.mtgAppLecteur.setVisible(svgId, '#im' + i, false, true)
        }
        stor.mtgAppLecteur.getDoc(svgId).defaultCursor = ''
      }
    } else {
      return function () {
        if (stor.raz) return
        if (stor.okCoup) return
        stor.mtgAppLecteur.setVisible(svgId, '#imd' + i, false, true)
        stor.mtgAppLecteur.getDoc(svgId).defaultCursor = ''
      }
    }
  }
  function faisAnnule () {
    j3pDetruit('modale')
    j3pDetruit('j3pmasque')
  }
  function faisEdit (k) {
    stor.mtgAppLecteur.setText(svgId, k.af, '', true)
    const yy = j3pModale({ titre: 'Editer', contenu: '' })
    const ttt = addDefaultTable(yy, 2, 4)
    j3pAffiche(ttt[0][0], null, 'nouvelle valeur:')
    stor.newval = new ZoneStyleMathquill1(ttt[0][1], {
      limite: 6,
      limitenb: 1,
      restric: '0123456789hm',
      contenu: k.val,
      enter: () => sortEdit(k)
    })
    ttt[0][2].style.width = '40px'
    j3pAjouteBouton(ttt[0][3], function () { sortEdit(k) }, { className: '', value: 'OK' })
    j3pAjouteBouton(ttt[1][0], function () { sortEditQ(k) }, { className: '', value: 'Annuler' })
    setTimeout(function () { stor.newval.focus() }, 100)
    const elem = $('.croix')[0]
    j3pDetruit(elem)
  }
  function sortEdit (k) {
    stor.newval.disable()
    k.val = stor.newval.reponse()
    stor.mtgAppLecteur.setText(svgId, k.af, k.val !== '' ? k.val : '?', true)
    j3pDetruit('modale')
    j3pDetruit('j3pmasque')
  }
  function sortEditQ (k) {
    stor.newval.disable()
    stor.mtgAppLecteur.setText(svgId, k.af, k.val !== '' ? k.val : '?', true)
    j3pDetruit('modale')
    j3pDetruit('j3pmasque')
  }
  function faisGros (b) {
    if (stor.raz) return
    if (stor.okCoup) return
    if (stor.nbcoup === 5) b = false
    const ep = b ? 5 : 2
    const pp = b ? 'pointer' : ''
    stor.mtgAppLecteur.setLineStyle(svgId, '#l1', 0, ep, true)
    stor.mtgAppLecteur.setLineStyle(svgId, '#l2', 0, ep, true)
    stor.mtgAppLecteur.setLineStyle(svgId, '#l3', 0, ep, true)
    stor.fG = b
    stor.mtgAppLecteur.getDoc(svgId).defaultCursor = pp
  }

  function yaReponse () {
    if (me.isElapsed) return true
    if (stor.zonem && stor.zoneh) {
      if (stor.zonem.reponse() === '' && stor.zoneh.reponse() === '') {
        stor.zoneh.focus()
        return false
      }
    } else if (stor.zonem) {
      if (stor.zonem.reponse() === '') {
        stor.zonem.focus()
        return false
      }
    } else if (stor.zoneh) {
      if (stor.zoneh.reponse() === '') {
        stor.zoneh.focus()
        return false
      }
    } else {
      me.notif(Error('cas non géré, ni zonem ni zoneh'))
    }
    return true
  }

  function isRepOk () {
    let ok = true
    stor.errMinu = false
    stor.errHeure = false
    stor.errDur = false

    stor.rpM = 0
    if (stor.zonem !== undefined) {
      stor.rpM = stor.zonem.reponse()
    }
    stor.rpH = 0
    if (stor.zoneh !== undefined) {
      stor.rpH = stor.zoneh.reponse()
    }

    if (stor.Lexo.question !== 'Durée') {
      if (parseFloat(stor.rpH) > 23) {
        stor.zoneh.corrige(false)
        stor.errHeure = true
        ok = false
      }
      if (parseFloat(stor.rpM) > 59) {
        stor.zonem.corrige(false)
        stor.errMinu = true
        ok = false
      }
    }

    if (!ok) return false

    switch (stor.Lexo.question) {
      case 'Durée':
        if (stor.mez === 'h') stor.larep = parseFloat(stor.rpH) * 60
        if (stor.mez === 'm') stor.larep = parseFloat(stor.rpM)
        if (stor.mez === 'hm') stor.larep = parseFloat(stor.rpH) * 60 + parseFloat(stor.rpM)
        ok = (stor.larep === stor.dudur)
        if (!ok) {
          if (stor.mez === 'h') stor.zoneh.corrige(false)
          if (stor.mez === 'm') stor.zonem.corrige(false)
          if (stor.mez === 'hm') {
            if (parseFloat(stor.rpH) !== stor.h) { stor.zoneh.corrige(false) }
            if (parseFloat(stor.rpM) !== stor.m) { stor.zonem.corrige(false) }
          }
        }
        break
      case 'Début':
        ok = parseFloat(stor.rpH) === stor.hdep && parseFloat(stor.rpM) === stor.mindep
        if (!ok) {
          if (stor.hdep !== parseFloat(stor.rpH)) stor.zoneh.corrige(false)
          if (stor.mindep !== parseFloat(stor.rpM)) stor.zonem.corrige(false)
        }
        break
      case 'Fin':
        ok = parseFloat(stor.rpH) === stor.har && parseFloat(stor.rpM) === stor.minar
        if (!ok) {
          if (stor.har !== parseFloat(stor.rpH)) stor.zoneh.corrige(false)
          if (stor.minar !== parseFloat(stor.rpM)) stor.zonem.corrige(false)
        }
        break
    }

    return ok
  }

  function desactiveAll () {
    stor.raz = true
    if (stor.zonem !== undefined) stor.zonem.disable()
    if (stor.zoneh !== undefined) stor.zoneh.disable()
  }

  function barrelesfo () {
    // existe pas toujours
    stor.zonem?.barreIfKo()
    stor.zoneh?.barreIfKo()
  }

  function mettouvert () {
    if (stor.zonem !== undefined) stor.zonem.corrige(true)
    if (stor.zoneh !== undefined) stor.zoneh.corrige(true)
  }

  function afficheCorrection (bool) {
    j3pEmpty(stor.lesdiv.explications)
    let yaexplik = false
    if (stor.errMinu) {
      yaexplik = true
      j3pAffiche(stor.lesdiv.explications, null, 'Les minutes ne peuvent pas dépasser 60 !\n')
    }

    if (bool) {
      stor.yaco = true
      switch (stor.Lexo.question) {
        case 'Durée': {
          faisZed()
          j3pDetruit(stor.bos)
          let afdur = ''
          let comb = ''
          if (stor.h !== 0) {
            comb = ' et '
            afdur = stor.h + ' heure'
            if (stor.h > 1) afdur += 's'
          }
          if (stor.m > 0) {
            afdur += comb + stor.m + ' minute'
            if (stor.m > 1) afdur += 's'
          }
          j3pAffiche(stor.lesdiv.solution, null, 'Durée = ' + afdur)
          break
        }
        case 'Début': {
          let afdeb = stor.hdep + 'h'
          if (stor.hdep < 10) afdeb = '0' + afdeb
          if (stor.mindep < 10) { afdeb += '0' + stor.mindep } else { afdeb += stor.mindep }
          j3pAffiche(stor.lesdiv.solution, null, stor.kquest + '<i>' + afdeb + '</i>')
          faisLigne()
          j3pDetruit(stor.bos)
          if (stor.minar >= stor.mindep || stor.minar === 0) {
            faisLigneCoupe(3)
            stor.listCapt[2].val = stor.afin
            stor.listCapt[0].val = afdeb
            stor.listCapt[1].val = afficheHeure(stor.hdep, stor.minar)
            stor.listCapt[1].blok = true
            stor.listCapt[0].blok = true
            stor.listCapt[2].blok = true
            stor.listCaptR[0].val = (stor.minar - stor.mindep) + ' minute'
            if (stor.minar - stor.mindep > 1) stor.listCaptR[0].val += 's'
            stor.listCaptR[1].val = stor.h + ' heure'
            if (stor.h > 1) stor.listCaptR[1].val += 's'
            stor.listCaptR[1].blok = true
            stor.listCaptR[0].blok = true
            completeLigne()
            stor.mtgAppLecteur.setColor(svgId, stor.listCapt[0].af, 50, 50, 200, true)
            stor.mtgAppLecteur.setVisible(svgId, '#coup', false, true)
          } else {
            faisLigneCoupe(4)
            stor.listCapt[0].val = afdeb
            stor.listCapt[1].val = afficheHeure(stor.hdep + 1, 0)
            stor.listCapt[2].val = afficheHeure(stor.hdep + 1, stor.minar)
            stor.listCapt[3].val = stor.afin
            stor.listCapt[1].blok = true
            stor.listCapt[0].blok = true
            stor.listCapt[2].blok = true
            stor.listCapt[3].blok = true
            stor.listCaptR[0].val = (60 - stor.mindep) + ' minute'
            if (60 - stor.mindep > 1) stor.listCaptR[0].val += 's'
            stor.listCaptR[1].val = stor.minar + ' minute'
            if (stor.minar > 1) stor.listCaptR[1].val += 's'
            stor.listCaptR[2].val = stor.h + ' heure'
            if (stor.h > 1) stor.listCaptR[2].val += 's'
            stor.listCaptR[1].blok = true
            stor.listCaptR[0].blok = true
            stor.listCaptR[2].blok = true
            completeLigne()
            stor.mtgAppLecteur.setColor(svgId, stor.listCapt[0].af, 50, 50, 200, true)
            stor.mtgAppLecteur.setVisible(svgId, '#coup', false, true)
          }
          break
        }
        case 'Fin': {
          const affin = afficheHeure(stor.har, stor.minar)
          j3pAffiche(stor.lesdiv.solution, null, stor.kquest + '<i>' + affin + '</i>')
          faisLigne()
          j3pDetruit(stor.bos)
          if (stor.minar > stor.mindep || stor.minar === 0) {
            faisLigneCoupe(3)
            stor.listCapt[2].val = affin
            stor.listCapt[0].val = stor.afdeb
            stor.listCapt[1].val = afficheHeure(stor.hdep, stor.minar)
            stor.listCapt[1].blok = true
            stor.listCapt[0].blok = true
            stor.listCapt[2].blok = true
            stor.listCaptR[0].val = (stor.minar - stor.mindep) + ' minute'
            if (stor.minar - stor.mindep > 1) stor.listCaptR[0].val += 's'
            stor.listCaptR[1].val = stor.h + ' heure'
            if (stor.h > 1) stor.listCaptR[1].val += 's'
            stor.listCaptR[1].blok = true
            stor.listCaptR[0].blok = true
            completeLigne()
            stor.mtgAppLecteur.setColor(svgId, stor.listCapt[2].af, 50, 50, 200, true)
            stor.mtgAppLecteur.setVisible(svgId, '#coup', false, true)
          } else {
            faisLigneCoupe(4)
            stor.listCapt[0].val = stor.afdeb
            stor.listCapt[1].val = afficheHeure(stor.hdep + 1, 0)
            stor.listCapt[2].val = afficheHeure(stor.hdep + 1, stor.minar)
            stor.listCapt[3].val = affin
            stor.listCapt[1].blok = true
            stor.listCapt[0].blok = true
            stor.listCapt[2].blok = true
            stor.listCapt[3].blok = true
            stor.listCaptR[0].val = (60 - stor.mindep) + ' minute'
            if (60 - stor.mindep > 1) stor.listCaptR[0].val += 's'
            stor.listCaptR[1].val = stor.minar + ' minute'
            if (stor.minar > 1) stor.listCaptR[1].val += 's'
            stor.listCaptR[2].val = stor.h + ' heure'
            if (stor.h > 1) stor.listCaptR[2].val += 's'
            stor.listCaptR[1].blok = true
            stor.listCaptR[0].blok = true
            stor.listCaptR[2].blok = true
            completeLigne()
            stor.mtgAppLecteur.setColor(svgId, stor.listCapt[3].af, 50, 50, 200, true)
            stor.mtgAppLecteur.setVisible(svgId, '#coup', false, true)
          }
          break
        } // case Fin
      }
      j3pEmpty(stor.aef)
      barrelesfo()
      desactiveAll()
    }
    if (ds.theme === 'zonesAvecImageDeFond') {
      if (yaexplik) {
        stor.lesdiv.explications.classList.add('explique')
      }
      if (stor.yaco) {
        stor.lesdiv.solution.classList.add('correction')
      }
    }
  }

  function afficheHeure (a, b) {
    let afmoy = a + ''
    if (afmoy.length < 2) afmoy = '0' + afmoy
    afmoy += 'h'
    let afmid = b + ''
    if (afmid.length < 2) afmid = '0' + afmid
    return afmoy + afmid
  }

  function enonceMain () {
    me.videLesZones()
    creeLesDiv()
    stor.Lexo = faisChoixExos()
    poseQuestion()
    j3pEmpty(stor.lesdiv.calculatrice)
    if (ds.Calculatrice) {
      me.fenetresjq = [{ name: 'Calculatrice', title: 'Une calculatrice', left: 600, top: 10 }]
      j3pCreeFenetres(me)
      j3pAfficheCroixFenetres('Calculatrice')
      j3pAjouteBouton(stor.lesdiv.calculatrice, boutonfen, { className: 'MepBoutons', value: 'Calculatrice' })
    }
    me.finEnonce()
  }

  switch (this.etat) {
    case 'enonce':
      // code exécuté au lancement de la section
      if (me.debutDeLaSection) {
        initSection()
      } else {
        if (stor.aidetab !== undefined) {
          stor.aidetab.disable()
          stor.aidetab = undefined
        }
        enonceMain()
      }
      break // case "enonce":

    case 'correction':
      // On teste si une réponse a été saisie
      stor.lesdiv.explications.classList.remove('explique')
      stor.lesdiv.solution.classList.remove('correction')

      if (yaReponse()) {
        // Bonne réponse
        if (isRepOk()) {
          stor.lesdiv.correction.style.color = colorOk
          stor.lesdiv.correction.innerHTML = cBien

          desactiveAll()
          mettouvert()

          j3pEmpty(stor.lesdiv.explikjus)
          j3pEmpty(stor.lesdiv.explikenc)

          this.score++
          this.typederreurs[0]++
          return this.finCorrection('navigation', true)
        }

        // Pas de bonne réponse
        stor.lesdiv.correction.style.color = colorKo
        stor.lesdiv.correction.innerHTML = cFaux
        if (this.isElapsed) {
          // A cause de la limite de temps :
          stor.lesdiv.correction.innerHTML += tempsDepasse + '<BR>'
          this.typederreurs[10]++
          afficheCorrection(true)
          return this.finCorrection('navigation', true)
        }

        // Réponse fausse :
        if (me.essaiCourant < me.donneesSection.nbchances) {
          stor.lesdiv.correction.innerHTML += '<br>' + essaieEncore
          afficheCorrection(false)
          this.typederreurs[1]++
          return this.finCorrection()
        }

        // Erreur au dernier essai
        afficheCorrection(true)
        if (stor.encours === 'p') {
          stor.pose = false
          stor.encours = 't'
        }
        this.typederreurs[2]++
        return this.finCorrection('navigation', true)
      }

      // pas de réponse
      stor.lesdiv.correction.style.color = colorKo
      stor.lesdiv.correction.innerHTML = reponseIncomplete
      if (stor.encours === 'm') {
        stor.lesdiv.correction.innerHTML = 'Réponse incomplète ! <br> Tu dois déplacer le point ' + stor.C + ' .'
      }

      this.finCorrection()
      break // case "correction":

    case 'navigation':
      if (!this.sectionTerminee()) {
        this.etat = 'enonce'
      }
      this.finNavigation()
      break // case "navigation":
  }
}
