import { j3pAddElt, j3pAjouteBouton, j3pEmpty, j3pGetRandomInt, j3pShowError, j3pShuffle } from 'src/legacy/core/functions'
import ListeDeroulante from 'src/legacy/outils/listeDeroulante/ListeDeroulante'
import { j3pAffiche } from 'src/lib/mathquill/functions'
import { j3pCreeFenetres, j3pAfficheCroixFenetres, j3pToggleFenetres } from 'src/legacy/core/functionsJqDialog'
import { addDefaultTable } from 'src/legacy/themes/table'
import { ecrisBienMathquill } from 'src/legacy/outils/zoneStyleMathquill/functions'
import textesGeneriques from 'src/lib/core/textes'

const { cBien, cFaux, essaieEncore, reponseIncomplete, tempsDepasse } = textesGeneriques

/**
 * Ajouter une description ici
 * @fileOverview
 * @author Tommy Barroy <t.barroy@laposte.net>
 * @since 2017-10
*/

export const params = {
  outils: ['mathquill', 'calculatrice'],
  /**
   * Ce qui est paramétrable dans l’éditeur de graphe
   * @type {LegacySectionParamsParametres}
   */
  parametres: [
    ['nbrepetitions', 9, 'entier', 'Nombre de répétitions de la section'],
    ['Vocab_table', true, 'boolean', "<u>true</u>: Emploie de l’expression <i>'est dans la table de'</i> ."],
    ['Vocab_diviseur', true, 'boolean', "<u>true</u>: Emploie du terme <i>'diviseur'</i> ."],
    ['Vocab_multiple', true, 'boolean', "<u>true</u>: Emploie du terme <i>'multiple'</i> ."],
    ['Vocab_divisible', true, 'boolean', "<u>true</u>: Emploie de du terme <i>'divisible'</i> ."],
    ['par2', true, 'boolean', '<u>true</u>: Divisible par 2 demandé.'],
    ['par3', true, 'boolean', '<u>true</u>: Divisible par 3 demandé.'],
    ['par4', true, 'boolean', '<u>true</u>: Divisible par 4 demandé.'],
    ['par5', true, 'boolean', '<u>true</u>: Divisible par 5 demandé.'],
    ['par9', true, 'boolean', '<u>true</u>: Divisible par 9 demandé.'],
    ['par10', true, 'boolean', '<u>true</u>: Divisible par 10 demandé.'],
    ['parN', true, 'boolean', '<u>true</u>: Divisible par quelconque demandé.'],
    ['Justifie', true, 'boolean', '<i>Uniquement valable pour diviseur 2,3,4,5,9 et 10</i><br><br><u>true</u>: Justification demandée.'],
    ['Calcul_Mental', false, 'boolean', '<u>true</u>: le facteur est 2,3,4,5 ou 10 ou 100'],
    ['Calculatrice', true, 'boolean', '<u>true</u>: Calculatrice disponible.'],
    // le modèle gère la compatibilité ascendante avec l’ancien paramètre MiseEnPagePlusClaire
    ['theme', 'standard', 'liste', 'Modèle pour l’affichage', ['standard', 'zonesAvecImageDeFond']]
  ]
}

/**
 * section multiplevoc
 * @this {Parcours}
 */
export default function main () {
  const me = this
  const stor = me.storage
  const ds = me.donneesSection

  function creeLesDiv () {
    stor.lesdiv = {}
    stor.lesdiv.conteneur = j3pAddElt(me.zonesElts.MG, 'div', {
      style: me.styles.etendre('toutpetit.enonce', { padding: '10px' })
    })
    stor.lesdiv.consigneG = addDefaultTable(stor.lesdiv.conteneur, 1, 1)[0][0]
    stor.lesdiv.L1 = addDefaultTable(stor.lesdiv.conteneur, 1, 2)
    stor.lesdiv.figure = stor.lesdiv.L1[0][0]
    stor.lesdiv.donnees = stor.lesdiv.L1[0][1]
    stor.lesdiv.travail = addDefaultTable(stor.lesdiv.conteneur, 1, 1)[0][0]
    stor.lesdiv.explications = j3pAddElt(stor.lesdiv.conteneur, 'div')
    stor.lesdiv.solution = j3pAddElt(stor.lesdiv.conteneur, 'div')
    stor.lesdiv.calculatrice = j3pAddElt(me.zonesElts.MD, 'div', {
      style: me.styles.etendre('toutpetit.enonce', { padding: '10px' })
    })
    stor.lesdiv.correction = j3pAddElt(me.zonesElts.MD, 'div', {
      style: me.styles.etendre('toutpetit.enonce', { padding: '10px' })
    })
    stor.lesdiv.solution.style.color = me.styles.toutpetit.correction.color
    stor.lesdiv.explications.style.color = me.styles.cfaux
  }
  function initSection () {
    let i
    // Construction de la page
    me.construitStructurePage('presentation1bis')
    let lp = [true]
    lp = j3pShuffle(lp)
    ds.lesExos = []
    for (i = 0; i < ds.nbitems; i++) {
      ds.lesExos.push({ signe: lp[i % lp.length] })
    }
    ds.lesExos = j3pShuffle(ds.lesExos)
    me.typederreurs = [0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0]

    lp = ['vrai', 'faux', 'env']
    lp = j3pShuffle(lp)
    for (i = 0; i < ds.nbitems; i++) {
      ds.lesExos[i].cas = lp[i % lp.length]
    }
    ds.lesExos = j3pShuffle(ds.lesExos)

    lp = []
    if (ds.Vocab_table) lp.push('table')
    if (ds.Vocab_diviseur) lp.push('diviseur')
    if (ds.Vocab_multiple) lp.push('multiple')
    if (ds.Vocab_divisible) lp.push('divisible')
    if (lp.length === 0) {
      j3pShowError('Erreur paramétrage')
      lp = ['table']
    }
    lp = j3pShuffle(lp)
    for (i = 0; i < ds.nbitems; i++) {
      ds.lesExos[i].terme = lp[i % lp.length]
    }
    ds.lesExos = j3pShuffle(ds.lesExos)

    let tt = 'Diviseur et multiple'
    if (lp.length === 1) {
      tt = lp[0]
    }

    lp = []
    if (ds.par2) lp.push('2')
    if (ds.par3) lp.push('3')
    if (ds.par4) lp.push('4')
    if (ds.par5) lp.push('5')
    if (ds.par9) lp.push('9')
    if (ds.par10) lp.push('10')
    if (ds.parN) lp.push('N')
    if (lp.length === 0) {
      j3pShowError('Erreur paramétrage')
      lp = ['N']
    }
    lp = j3pShuffle(lp)
    for (i = 0; i < ds.nbitems; i++) {
      ds.lesExos[i].div = lp[i % lp.length]
    }
    ds.lesExos = j3pShuffle(ds.lesExos)

    if (lp.length === 1 && lp[0] !== 'N') {
      tt += ' ' + lp[0]
    }

    me.score = 0

    me.afficheTitre(tt)
    enonceMain()
  }
  function faisChoixExo () {
    let pass
    stor.restric = '0123456789-()'
    stor.Lexo = ds.lesExos.pop()
    stor.nb = []
    if (stor.Lexo.div !== 'N') {
      stor.Lexo.div = Number(stor.Lexo.div)
    } else {
      stor.Lexo.div = j3pGetRandomInt(11, 99)
    }
    stor.lemul = j3pGetRandomInt(101, 999)

    if (ds.Calcul_mental) {
      stor.lemul = j3pGetRandomInt(2, 14)
      if (stor.lemul === 6) stor.lemul = 21
      if (stor.lemul === 7) stor.lemul = 19
      if (stor.lemul === 8) stor.lemul = 101
      if (stor.lemul === 14) stor.lemul = 99
      if (stor.lemul === 12) stor.lemul = 1001
      if (stor.lemul === 13) stor.lemul = 999
    }
    stor.lenomb = stor.lemul * stor.Lexo.div
    if (stor.Lexo.cas === 'faux') {
      stor.lenomb += j3pGetRandomInt(1, stor.Lexo.div - 1)
    }
    if (stor.Lexo.cas === 'env') {
      pass = stor.lenomb
      stor.lenomb = stor.Lexo.div
      stor.Lexo.div = pass
    }
  }
  function boutonfen () {
    j3pToggleFenetres('Calculatrice')
  }
  function poseQuestion () {
    let buf, buf2
    stor.listeDeroulante = undefined
    j3pAffiche(stor.lesdiv.consigneG, null, '<b>Complète la phrase ci dessous</b>')
    switch (stor.Lexo.terme) {
      case 'table':
        buf = 'Le nombre $' + ecrisBienMathquill(stor.lenomb) + '$&nbsp;'
        buf2 = '&nbsp; dans la table de $' + ecrisBienMathquill(stor.Lexo.div) + '$.'
        break
      case 'multiple':
        buf = 'Le nombre $' + ecrisBienMathquill(stor.lenomb) + '$&nbsp;'
        buf2 = '&nbsp; un multiple de $' + ecrisBienMathquill(stor.Lexo.div) + '$.'
        break
      case 'diviseur':
        buf = 'Le nombre $' + ecrisBienMathquill(stor.Lexo.div) + '$&nbsp;'
        buf2 = '&nbsp; un diviseur de $' + ecrisBienMathquill(stor.lenomb) + '$.'
        break
      case 'divisible':
        buf = 'Le nombre $' + ecrisBienMathquill(stor.lenomb) + '$&nbsp;'
        buf2 = '&nbsp; divisible par $' + ecrisBienMathquill(stor.Lexo.div) + '$.'
        break
    }
    const ll = ['Choisir', ' est ', ' n’est pas ']
    stor.ltab1 = addDefaultTable(stor.lesdiv.travail, 2, 1)
    const ttab = addDefaultTable(stor.ltab1[0][0], 1, 3)
    j3pAffiche(ttab[0][0], null, buf)
    j3pAffiche(ttab[0][2], null, buf2)
    stor.liste0 = ListeDeroulante.create(ttab[0][1], ll, { onChange: faisbout, centre: true })
    if (ds.Calculatrice) {
      me.fenetresjq = [{ name: 'Calculatrice', title: 'Une calculatrice', left: 600, top: 10 }]
      j3pCreeFenetres(me)
      j3pAfficheCroixFenetres('Calculatrice')
      j3pAjouteBouton(stor.lesdiv.calculatrice, 'Calculatrice', 'MepBoutons', 'Calculatrice', boutonfen)
    }
  }
  function faisbout () {
    if (!ds.Justifie) return
    if (['2', '3', '4', '5', '9', '10'].indexOf(String(stor.Lexo.div)) === -1 && ['2', '3', '4', '5', '9', '10'].indexOf(String(stor.lenomb)) === -1) return
    if (stor.listeDeroulante !== undefined) stor.listeDeroulante.disable()
    const ll = ['Choisir']
    let fin, i, som
    j3pEmpty(stor.ltab1[1][0])
    const ftab = addDefaultTable(stor.ltab1[1][0], 2, 2)
    j3pAffiche(ftab[0][0], null, 'car&nbsp;')
    if (stor.liste0.getReponseIndex() === 1) {
      ll.push('le chiffre des unités est $' + String(stor.lenomb)[String(stor.lenomb).length - 1] + '$')
      if (stor.Lexo.div === 4) {
        fin = String(stor.lenomb)[String(stor.lenomb).length - 2] + String(stor.lenomb)[String(stor.lenomb).length - 1]
        ll.push('le nombre se termine par $' + fin + '$ , et $' + fin + '$ est dans la table de $' + ecrisBienMathquill(stor.Lexo.div) + '$')
      } else {
        fin = String(stor.lenomb)[0]
        ll.push('$' + ecrisBienMathquill(stor.lenomb) + '$ commence par $' + fin + '$ , et $' + fin + '$ est dans la table de $' + ecrisBienMathquill(stor.Lexo.div) + '$')
      }

      if (String(stor.lenomb).length > 1) {
        som = 0
        fin = ''
        for (i = 0; i < String(stor.lenomb).length; i++) {
          fin += String(stor.lenomb)[i] + ' +'
          som += Number(String(stor.lenomb)[i])
        }
        fin = fin.substring(0, fin.length - 1)
        ll.push('$' + fin + '$ = $' + som + '$ et $' + som + '$ est dans la table de $' + ecrisBienMathquill(stor.Lexo.div) + '$')
      } else {
        ll.push('$' + ecrisBienMathquill(stor.lenomb) + '=' + ecrisBienMathquill(stor.lenomb) + ' \\times 1$')
      }
      stor.listeDeroulante = ListeDeroulante.create(ftab[0][1], ll, { choix0: false })
    } else {
      if (stor.Lexo.div === 4) {
        ll.push('$' + ecrisBienMathquill(stor.lenomb) + '$ commence par $' + String(stor.lenomb)[0] + '$')
        fin = String(stor.lenomb)[String(stor.lenomb).length - 2] + String(stor.lenomb)[String(stor.lenomb).length - 1]
        ll.push('le nombre se termine par $' + fin + '$ , et $' + fin + '$ n’est pas dans la table de $' + ecrisBienMathquill(stor.Lexo.div) + '$')
      } else {
        ll.push('le chiffre des unités est $' + String(stor.lenomb)[String(stor.lenomb).length - 1] + '$')
        fin = String(stor.lenomb)[0]
        ll.push('$' + ecrisBienMathquill(stor.lenomb) + '$ commence par $' + fin + '$ , et $' + fin + '$ n’est pas dans la table de $' + ecrisBienMathquill(stor.Lexo.div) + '$')
      }
      if (String(stor.lenomb).length > 1) {
        som = 0
        fin = ''
        for (i = 0; i < String(stor.lenomb).length; i++) {
          fin += String(stor.lenomb)[i] + ' +'
          som += Number(String(stor.lenomb)[i])
        }
        fin = fin.substring(0, fin.length - 1)
        ll.push('$' + fin + '$ = $' + som + '$ et $' + som + '$  n’est pas dans la table de $' + ecrisBienMathquill(stor.Lexo.div) + '$')
      } else {
        ll.push('$' + ecrisBienMathquill(stor.lenomb) + '=' + ecrisBienMathquill(stor.lenomb) + ' \\times 1$')
      }
      ll.push('$' + ecrisBienMathquill(stor.Lexo.div) + ' > ' + ecrisBienMathquill(stor.lenomb) + '$')
      stor.listeDeroulante = ListeDeroulante.create(ftab[0][1], ll, { choix0: false })
    }
  }

  function yaReponse () {
    if (!stor.liste0.changed) return false
    if (stor.listeDeroulante !== undefined) {
      if (!stor.listeDeroulante.changed) {
        stor.listeDeroulante.focus()
        return false
      }
    }
    return true
  }
  function isRepOk () {
    stor.errPasPRod = false
    stor.errFact = false
    stor.errPar = false
    stor.errExp = false
    stor.repfo = false

    const tabrep = stor.liste0.getReponseIndex() === 1
    if (tabrep !== (stor.Lexo.cas === 'vrai')) {
      stor.liste0.corrige(false)
      return false
    }
    if (stor.listeDeroulante !== undefined) {
      switch (stor.Lexo.div) {
        case 2:
        case 5:
        case 10:
          if (stor.listeDeroulante.getReponseIndex() !== 1) {
            stor.listeDeroulante.corrige(false)
            return false
          }
          break
        case 3:
        case 9:
          if (stor.listeDeroulante.getReponseIndex() !== 3) {
            stor.listeDeroulante.corrige(false)
            return false
          }
          break
        case 4: if (stor.listeDeroulante.getReponseIndex() !== 2) {
          stor.listeDeroulante.corrige(false)
          return false
        }
          break
        default:
          if (stor.listeDeroulante.getReponseIndex() !== 4) {
            stor.listeDeroulante.corrige(false)
            return false
          }
      }
    }

    return true
  } // isRepOk

  function desactiveAll () {
    stor.liste0.disable()
    if (stor.listeDeroulante) stor.listeDeroulante.disable()
  } // desactiveAll
  function passeToutVert () {
    stor.liste0.corrige(true)
    if (stor.listeDeroulante) stor.listeDeroulante.corrige(true)
  }
  function barrelesfo () {
    stor.liste0.barreIfKo()
    if (stor.listeDeroulante) stor.listeDeroulante.barreIfKo()
  }
  function affCorrFaux (isFin) {
    /// //affiche indic
    let buf, i, buf2, buf3

    if (isFin) {
      // barre les faux
      barrelesfo()
      desactiveAll()
      stor.yaco = true
      buf2 = 'est'
      if (stor.Lexo.cas !== 'vrai') buf2 = 'n’est pas'
      switch (stor.Lexo.terme) {
        case 'table':
          buf = 'Le nombre $' + ecrisBienMathquill(stor.lenomb) + '$ ' + buf2 + ' dans la table de $' + ecrisBienMathquill(stor.Lexo.div) + '$'
          break
        case 'multiple':
          buf = 'Le nombre $' + ecrisBienMathquill(stor.lenomb) + '$ ' + buf2 + ' un multiple de $' + ecrisBienMathquill(stor.Lexo.div) + '$'
          break
        case 'diviseur':
          buf = 'Le nombre $' + ecrisBienMathquill(stor.Lexo.div) + '$ ' + buf2 + ' un diviseur de $' + ecrisBienMathquill(stor.lenomb) + '$'
          break
        case 'divisible':
          buf = 'Le nombre $' + ecrisBienMathquill(stor.lenomb) + '$ ' + buf2 + ' divisible par $' + ecrisBienMathquill(stor.Lexo.div) + '$'
          break
      }
      if (stor.Lexo.cas === 'env') {
        buf += '.<br> (la phrase est inversée)'
      }
      buf3 = '.'
      if (ds.Justifie && stor.Lexo.cas !== 'env' && [2, 3, 4, 5, 9, 10].indexOf(stor.Lexo.div) !== -1) {
        if (stor.Lexo.cas === 'vrai') {
          switch (stor.Lexo.div) {
            case 2:
            case 5:
            case 10:
              buf3 = ',<br>car le chiffre des unités est $' + String(stor.lenomb)[String(stor.lenomb).length - 1] + '$'
              break
            case 4: {
              const fin = String(stor.lenomb)[String(stor.lenomb).length - 2] + String(stor.lenomb)[String(stor.lenomb).length - 1]
              buf3 = ',<br>car $' + ecrisBienMathquill(stor.lenomb) + '\\text{ se termine par }' + fin + '\\text{ , et }' + fin + '\\text{ est dans la table de }' + ecrisBienMathquill(stor.Lexo.div) + '$'
              break
            }
            default: {
              let som = 0
              let fin = ''
              for (i = 0; i < String(stor.lenomb).length; i++) {
                fin += String(stor.lenomb)[i] + ' +'
                som += Number(String(stor.lenomb)[i])
              }
              fin = fin.substring(0, fin.length - 1)
              buf3 = ',<br>car $' + fin + '\\text{ = }' + som + '\\text{ et }' + som + '\\text{  est dans la table de }' + ecrisBienMathquill(stor.Lexo.div) + '$'
            }
          }
        } else {
          switch (stor.Lexo.div) {
            case 2:
              buf3 = ',<br> car le chiffre des unités est $' + String(stor.lenomb)[String(stor.lenomb).length - 1] + '$<br>'
              buf3 += '( Les multiples de $2$ se terminent par $0$, $2$, $4$, $6$, ou $8$ )'
              break
            case 5:
              buf3 = ',<br> car le chiffre des unités est $' + String(stor.lenomb)[String(stor.lenomb).length - 1] + '$<br>'
              buf3 += '( Les multiples de $5$ se terminent par $0$ ou $5$ )'
              break
            case 10:
              buf3 = ',<br> car le chiffre des unités est $' + String(stor.lenomb)[String(stor.lenomb).length - 1] + '$<br>'
              buf3 += '( les multiples de $10$ se terminent par $0$ )'
              break
            case 4: {
              const fin = String(stor.lenomb)[String(stor.lenomb).length - 2] + String(stor.lenomb)[String(stor.lenomb).length - 1]
              buf3 = ',<br>car $' + ecrisBienMathquill(stor.lenomb) + '\\text{ se termine par }' + fin + '\\text{ , et }' + fin + '\\text{ n’est pas dans la table de }' + ecrisBienMathquill(stor.Lexo.div) + '$'
              break
            }
            default: {
              let som = 0
              let fin = ''
              for (i = 0; i < String(stor.lenomb).length; i++) {
                fin += String(stor.lenomb)[i] + ' +'
                som += Number(String(stor.lenomb)[i])
              }
              fin = fin.substring(0, fin.length - 1)
              buf3 = ',<br>car $' + fin + '\\text{ = }' + som + '\\text{ et }' + som + '\\text{  n’est pas dans la table de }' + ecrisBienMathquill(stor.Lexo.div) + '$'
            }
          }
        }
        if (stor.Lexo.cas !== 'env') {
          buf += buf3
        }
      }
      j3pAffiche(stor.lesdiv.solution, null, buf)
      if (stor.Lexo.cas === 'vrai') {
        j3pAffiche(stor.lesdiv.solution, null, '\n$' + ecrisBienMathquill(stor.lenomb) + ' = ' + stor.lemul + ' \\times ' + stor.Lexo.div + '$')
      }
    }
  } // affCorrFaux
  function enonceMain () {
    me.videLesZones()
    creeLesDiv()
    stor.lesdiv.travail.classList.add('travail')
    stor.lesdiv.consigneG.classList.add('enonce')
    faisChoixExo()
    poseQuestion()
    me.afficheBoutonValider()
    me.finEnonce()
  }

  switch (this.etat) {
    case 'enonce':
      // code exécuté au lancement de la section
      if (me.debutDeLaSection) {
        initSection()
      } else {
        enonceMain()
      }
      break // case "enonce":

    case 'correction':
      // On teste si une réponse a été saisie
      stor.yaexplik = false
      stor.yaco = false
      stor.lesdiv.explications.innerHTML = ''
      stor.lesdiv.solution.innerHTML = ''
      stor.lesdiv.explications.classList.remove('explique')

      if (yaReponse()) {
        // Bonne réponse
        if (isRepOk()) {
          this._stopTimer()
          stor.lesdiv.correction.style.color = this.styles.cbien
          stor.lesdiv.correction.innerHTML = cBien

          desactiveAll()
          passeToutVert()

          this.score++
          this.typederreurs[0]++
          this.cacheBoutonValider()
          this.etat = 'navigation'
          this.sectionCourante()
        } else {
          // Pas de bonne réponse
          stor.lesdiv.correction.style.color = this.styles.cfaux
          stor.lesdiv.correction.innerHTML = cFaux
          if (this.isElapsed) {
            // A cause de la limite de temps :
            this._stopTimer()

            stor.lesdiv.correction.innerHTML = tempsDepasse + '<BR>'
            this.typederreurs[10]++
            this.cacheBoutonValider()

            affCorrFaux(true)
            stor.yaco = true

            this.etat = 'navigation'
            this.sectionCourante()
          } else {
            // Réponse fausse :
            if (this.essaiCourant < ds.nbchances) {
              stor.lesdiv.correction.innerHTML += '<br>' + essaieEncore

              affCorrFaux(false)

              // affCorrFaux.call(this, false)
              this.typederreurs[1]++
            } else {
              // Erreur au nème essai
              this._stopTimer()
              this.cacheBoutonValider()

              affCorrFaux(true)
              stor.yaco = true

              this.typederreurs[2]++
              this.etat = 'navigation'
              this.sectionCourante()
            }
          }
        }
      } else {
        // pas de réponse
        stor.lesdiv.correction.style.color = this.styles.cfaux
        stor.lesdiv.correction.innerHTML = reponseIncomplete
        this.afficheBoutonValider()
      }

      if (stor.yaexplik) stor.lesdiv.explications.classList.add('explique')
      if (stor.yaco) stor.lesdiv.solution.classList.add('correction')

      // Obligatoire
      me.finCorrection()
      break // case "correction":

    case 'navigation':
      if (!this.sectionTerminee()) {
        this.etat = 'enonce'
      }
      this.finNavigation()
      break // case "navigation":
  }
}
