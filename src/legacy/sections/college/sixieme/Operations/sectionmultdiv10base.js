import { j3pAddElt, j3pArrondi, j3pEmpty, j3pGetRandomInt, j3pShuffle } from 'src/legacy/core/functions'
import BoutonStyleMathquill from 'src/legacy/outils/boutonsStyleMatquill'
import ListeDeroulante from 'src/legacy/outils/listeDeroulante/ListeDeroulante'
import { verifNombreBienEcrit } from 'src/legacy/outils/fonctions/ecritureNombre'
import { j3pAffiche } from 'src/lib/mathquill/functions'
import { ecrisBienMathquill } from 'src/legacy/outils/zoneStyleMathquill/functions'
import ZoneStyleMathquill1 from 'src/legacy/outils/zoneStyleMathquill/ZoneStyleMathquill1'
import { addDefaultTable } from 'src/legacy/themes/table'
import textesGeneriques from 'src/lib/core/textes'

import './multiv10.css'

const { cBien, cFaux, essaieEncore, reponseIncomplete, tempsDepasse } = textesGeneriques

/**
 * Ajouter une description ici
 * @fileOverview
 * @author Tommy Barroy <t.barroy@laposte.net>
 * @since 2017-10
*/

export const params = {
  outils: ['mathquill'],
  /**
   * Ce qui est paramétrable dans l’éditeur de graphe
   * @type {LegacySectionParamsParametres}
   */
  parametres: [
    ['nbrepetitions', 9, 'entier', 'Nombre de répétitions de la section'],
    ['Trad1', true, 'boolean', '<u>true</u>: Traduction <i>le chiffre des unités devient les chiffre des ... </i> demandée.'],
    ['Trad2', true, 'boolean', '<u>true</u>: Traduction <i>... revient à déplacer le virgule de  ... </i> demandée.'],
    ['EtapesDetail', true, 'boolean', '<u>true</u>: Etapes pour repérer la place de la virgule.'],
    ['fois_10', true, 'boolean', '<u>true</u>:  L’exercice peut proposer des multiplications par 10 100 1000 ....   '],
    ['divise_10', true, 'boolean', '<u>true</u>:  L’exercice peut proposer des divisions par 10 100 1000 ....   '],
    ['fois_01', true, 'boolean', '<u>true</u>:  L’exercice peut proposer des multiplications par 0,1  0,01  0,001  ....   '],
    ['divise_01', true, 'boolean', '<u>true</u>:  L’exercice peut proposer des divisions par 0,1  0,01  0,001  ....   '],
    ['rang_max', 3, 'entier', 'Nombre de déplacement maximum (entre 1 et 6)'],
    ['Espace', true, 'boolean', '<u>true</u>: Les espaces entre chaque groupe de 3 sont exigés.'],
    // le modèle gère la compatibilité ascendante avec l’ancien paramètre MiseEnPagePlusClaire
    ['theme', 'standard', 'liste', 'Modèle pour l’affichage', ['standard', 'zonesAvecImageDeFond']]
  ]
}

/**
 * section multdiv10base
 * @this {Parcours}
 */
export default function main () {
  const me = this
  const stor = me.storage
  const ds = me.donneesSection

  function gererang (r) {
    const x = parseFloat(r[0])
    let bufr = '&nbsp; rang'
    if (x > 1) {
      bufr += 's'
    }
    bufr += ' '
    j3pEmpty(stor.tabPtiiZ[0][2])
    j3pAffiche(stor.tabPtiiZ[0][2], null, bufr)
  }
  function creeLesDiv () {
    stor.lesdiv = {}
    stor.lesdiv.conteneur = j3pAddElt(me.zonesElts.MG, 'div', { style: me.styles.etendre('toutpetit.enonce', { padding: '10px' }) })

    const l1 = addDefaultTable(stor.lesdiv.conteneur, 1, 3)
    modif(l1)
    stor.lesdiv.consigneG = l1[0][0]
    l1[0][1].style.width = '50px'
    stor.lesdiv.correction = l1[0][2]
    stor.lesdiv.travail = addDefaultTable(stor.lesdiv.conteneur, 1, 1)[0][0]
    stor.lesdiv.travail.style.padding = 0
    stor.lesdiv.etape1 = addDefaultTable(stor.lesdiv.conteneur, 1, 1)[0][0]
    stor.lesdiv.etape1.style.padding = 0
    stor.lesdiv.travail1 = addDefaultTable(stor.lesdiv.conteneur, 1, 1)[0][0]
    stor.lesdiv.travail1.style.padding = 0
    stor.lesdiv.etape2 = addDefaultTable(stor.lesdiv.conteneur, 1, 1)[0][0]
    stor.lesdiv.etape2.style.padding = 0
    stor.lesdiv.travail2 = addDefaultTable(stor.lesdiv.conteneur, 1, 1)[0][0]
    stor.lesdiv.travail2.style.padding = 0
    stor.lesdiv.etape3 = addDefaultTable(stor.lesdiv.conteneur, 1, 1)[0][0]
    stor.lesdiv.etape3.style.padding = 0
    stor.lesdiv.travail3 = addDefaultTable(stor.lesdiv.conteneur, 1, 1)[0][0]
    stor.lesdiv.travail3.style.padding = 0
    stor.lesdiv.travail4 = addDefaultTable(stor.lesdiv.conteneur, 1, 1)[0][0]
    stor.lesdiv.travail4.style.padding = 0
    stor.lesdiv.etape5 = addDefaultTable(stor.lesdiv.conteneur, 1, 1)[0][0]
    stor.lesdiv.etape5.style.padding = 0
    stor.lesdiv.travail5 = addDefaultTable(stor.lesdiv.conteneur, 1, 1)[0][0]
    stor.lesdiv.travail5.style.padding = 0
    stor.lesdiv.explications = j3pAddElt(stor.lesdiv.conteneur, 'div')
    stor.lesdiv.solution = j3pAddElt(stor.lesdiv.conteneur, 'div')
    stor.lesdiv.consigne = j3pAddElt(stor.lesdiv.consigneG, 'div')
    stor.lesdiv.old = j3pAddElt(stor.lesdiv.consigneG, 'div')
    stor.lesdiv.solution.style.color = me.styles.petit.correction.color
    stor.lesdiv.etape1.style.color = me.styles.petit.correction.color
    stor.lesdiv.etape2.style.color = me.styles.petit.correction.color
    stor.lesdiv.etape3.style.color = me.styles.petit.correction.color
    stor.lesdiv.etape5.style.color = me.styles.petit.correction.color
    stor.lesdiv.explications.style.color = me.styles.cfaux
    me.zonesElts.MG.classList.add('fond')
  }
  function modif (tab) {
    for (let i = 0; i < tab.length; i++) {
      for (let j = 0; j < tab[i].length; j++) {
        tab[i][j].style.padding = 0
      }
    }
  }

  function poseQuestion () {
    let s, lp
    let p = false
    let q = false
    let bufrang
    if (stor.remTrad1) {
      j3pEmpty(stor.tabPti[0][4])
      j3pAffiche(stor.tabPti[0][4], null, stor.repattt)
      stor.tabPti[0][4].style.color = me.styles.petit.correction.color
      stor.remTrad1 = false
    }
    if (stor.remTrad2) {
      if (stor.Lexo.rang > 1) { bufrang = ' rangs ' } else { bufrang = 'rang' }
      stor.buf1 += stor.Lexo.rang + ' ' + bufrang + ' vers la ' + stor.bufsens + ' .'
      const eeer = stor.tabPtiiZ[0][0].parentNode.parentNode
      j3pEmpty(eeer)
      j3pAffiche(eeer, null, stor.buf1)
      stor.remTrad2 = false
      eeer.style.color = me.styles.petit.correction.color
    }
    if (stor.remDet) {
      j3pEmpty(stor.lesdiv.travail3)
      j3pEmpty(stor.lesdiv.travail4)
      let pl = ('0000000' + stor.larepbonne).replace('.', ',')
      if (pl.indexOf(',') === -1) pl += ','
      pl += '0000000'
      j3pAffiche(stor.lesdiv.travail4, null, '$' + stor.monNombre2 + stor.bufdep + ' = ' + pl + '$')
      stor.lesdiv.travail4.style.color = me.styles.petit.correction.color
    }
    stor.etapou = me.questionCourante % ds.nbetapes - 1
    if (stor.etapou === -1) stor.etapou = stor.etapos.length - 1
    if (stor.etapou === 0) {
      if (stor.Lexo.type.indexOf('x') !== -1) {
        stor.bufdep = '\\times'
        stor.buf1 = 'Multiplier par '
        stor.buf1bis = 'Quand on multiplie par '
        p = true
      } else {
        stor.bufdep = '\\div'
        stor.buf1 = 'Diviser par '
        stor.buf1bis = 'Quand on divise par '
      }
      if (stor.Lexo.type.indexOf('10') !== -1) {
        s = 1
        q = true
      } else {
        s = -1
      }
      stor.buf1 += ' $' + ecrisBienMathquill(j3pArrondi(Math.pow(10, s * stor.Lexo.rang), stor.Lexo.rang)) + '$ revient à déplacer la virgule de &nbsp;'
      stor.buf1bis += ' $' + ecrisBienMathquill(j3pArrondi(Math.pow(10, s * stor.Lexo.rang), stor.Lexo.rang)) + '$ le chiffre des unités devient le chiffre des &nbsp;'
      if (p === q) { stor.bufsens = 'droite' } else { stor.bufsens = 'gauche' }
      stor.bufdep += ' ' + ecrisBienMathquill(j3pArrondi(Math.pow(10, s * stor.Lexo.rang), stor.Lexo.rang))

      j3pAffiche(stor.lesdiv.consigne, null, '<b>On veut calculer </b> $' + stor.monNombre2 + stor.bufdep + '$ \n')
    }
    if (stor.etapos[stor.etapou] === 'Trad1') {
      j3pAffiche(stor.lesdiv.etape1, null, '\n<b><u>Etape ' + (stor.etapou + 1) + '</u></b>: Traduire l’opération \n\n')
      stor.tabPti = addDefaultTable(stor.lesdiv.travail1, 1, 5)
      modif(stor.tabPti)
      j3pAffiche(stor.tabPti[0][0], null, stor.buf1bis)
      lp = ['Choisir', 'millionièmes', 'cent-millièmes', 'dix-millièmes', 'millièmes', 'centièmes', 'dixièmes', 'dizaines', 'centaines', 'milliers', 'dizaines de mille', 'centaines de mille', 'millions']
      if (ds.rang_max <= 3) lp = ['Choisir', 'millièmes', 'centièmes', 'dixièmes', 'dizaines', 'centaines', 'milliers']
      stor.liste1 = ListeDeroulante.create(stor.tabPti[0][4], lp, {
        centre: true
      })
      stor.lesdiv.travail1.classList.add('travail')
    }
    if (stor.etapos[stor.etapou] === 'Trad2') {
      vireEtape()
      j3pAffiche(stor.lesdiv.etape2, null, '\n<b><u>Etape ' + (stor.etapou + 1) + '</u></b>: Traduire l’opération \n\n')
      stor.tabPtiiZ = addDefaultTable(stor.lesdiv.travail2, 1, 5)
      modif(stor.tabPtiiZ)
      j3pAffiche(stor.tabPtiiZ[0][0], null, stor.buf1)
      stor.mazonem = new ZoneStyleMathquill1(stor.tabPtiiZ[0][1], { restric: '0123456789', limitenb: 1, limite: 1, afaire: gererang, enter: me.sectionCourante.bind(me) })
      j3pAffiche(stor.tabPtiiZ[0][2], null, '&nbsp; rang ')
      j3pAffiche(stor.tabPtiiZ[0][3], null, '&nbsp; vers la &nbsp;')
      stor.liste1 = ListeDeroulante.create(stor.tabPtiiZ[0][4], ['Choisir', 'droite.', 'gauche.'], {
        centre: true
      })
      stor.lesdiv.travail2.classList.add('travail')
    }
    if (stor.etapos[stor.etapou] === 'Repvirg') {
      vireEtape()
      const bufet = (!ds.Trad2 || ds.Trad1)
        ? 'Repérer le chiffre des unités. (<i>clique dessus</i>)'
        : 'Repérer la virgule. (<i>clique sur son emplacement</i>)'
      j3pAffiche(stor.lesdiv.etape3, null, '\n<b><u>Etape ' + (stor.etapou + 1) + '</u></b>: ' + bufet + ' \n\n')
      affichenbclik()
    }
    if (stor.etapos[stor.etapou] === 'Depvirg') {
      vireEtape()
      j3pEmpty(stor.lesdiv.travail3)
      const bufet = (!ds.Trad2 || ds.Trad1)
        ? 'Déplace la virgule pour que le chiffre des unités devienne le chiffres des ' + stor.repattt + ' .'
        : 'Déplacer la virgule.'
      j3pAffiche(stor.lesdiv.etape3, null, '\n<b><u>Etape ' + (stor.etapou + 1) + '</u></b>: ' + bufet + ' \n\n')
      affichenbclik2()
    }
    if (stor.etapos[stor.etapou] === 'Rep') {
      stor.lesdiv.travail3.classList.remove('travail')
      vireEtape()
      if (stor.etapos.length > 1) j3pAffiche(stor.lesdiv.etape5, null, '\n<b><u>Etape ' + (stor.etapou + 1) + '</u></b>: Ecrire la réponse \n')
      if (ds.Espace) j3pAffiche(stor.lesdiv.etape5, null, '<i>Un nombre mal écrit sera compté faux , attention aux espaces.</i>\n\n')

      stor.tabPtiiQ = addDefaultTable(stor.lesdiv.travail5, 1, 2)
      modif(stor.tabPtiiQ)
      j3pAffiche(stor.tabPtiiQ[0][0], null, '$' + stor.monNombre2 + stor.bufdep + ' = $')
      stor.mazonem = new ZoneStyleMathquill1(stor.tabPtiiQ[0][1], { restric: '0123456789,. ', limitenb: 10, limite: 20, enter: me.sectionCourante.bind(me) })
      // stor.larepbonne = j3pArrondi(parseFloat(stor.Nbb.replace(',', '.')), 20)
      stor.lesdiv.travail5.classList.add('travail')
    }
  }
  function affichenbclik2 () {
    let i
    stor.lesdiv.travail3.classList.remove('travail')
    stor.tabGrof = addDefaultTable(stor.lesdiv.travail4, 2, 2)
    modif(stor.tabGrof)
    j3pAffiche(stor.tabGrof[0][0], null, '$' + stor.monNombre2 + stor.bufdep + ' = $ ')
    stor.Nbb = '0000000' + stor.partieentiereEncours + stor.partiedecimaleEncours + '0000000'
    stor.ptiR = addDefaultTable(stor.tabGrof[0][1], 2, stor.Nbb.length * 2 + 1)
    modif(stor.ptiR)
    stor.iactu = 13 + 2 * stor.partieentiereEncours.length
    stor.igard = stor.iactu

    stor.tabAff = []
    for (i = 0; i < stor.Nbb.length * 2; i++) {
      if (i % 2 === 1) {
        stor.tabAff[i] = j3pAffiche(stor.ptiR[0][i], null, '&nbsp;<b>,</b>&nbsp;')
        if (i !== 13 + 2 * stor.partieentiereEncours.length) {
          stor.ptiR[0][i].style.visibility = 'hidden'
        }
      } else {
        j3pAffiche(stor.ptiR[0][i], null, '$' + stor.Nbb[i / 2] + '$')
        if (i + 1 === stor.igard) stor.ptiR[0][i].style.color = '#F00'
        if ((i / 2 < 7) || (i / 2 > stor.Nbb.length - 8)) { stor.ptiR[0][i].style.color = '#aaaaaa' }
      }
    }

    stor.tabGrof[1][1].style.align = 'center'
    const tt = addDefaultTable(stor.tabGrof[1][1], 1, 3)
    modif(tt)
    stor.bb1 = new BoutonStyleMathquill(tt[0][0], 'bout1', '←', bougegauche, { taille: '90%' })
    stor.bb3 = new BoutonStyleMathquill(tt[0][1], 'bout2', '∇', bougeraz, { taille: '90%' })
    stor.bb2 = new BoutonStyleMathquill(tt[0][2], 'bout3', '→', bougedroite, { taille: '90%' })
    stor.lesdiv.travail4.classList.add('travail')
  }
  function bougegauche () {
    if (stor.iactu > 1) {
      stor.ptiR[0][stor.iactu].style.visibility = 'hidden'
      stor.iactu = stor.iactu - 2
      stor.ptiR[0][stor.iactu].style.visibility = ''
    }
  }
  function bougedroite () {
    if (stor.iactu < stor.Nbb.length * 2 - 3) {
      stor.ptiR[0][stor.iactu].style.visibility = 'hidden'
      stor.iactu = stor.iactu + 2
      stor.ptiR[0][stor.iactu].style.visibility = ''
    }
  }
  function bougeraz () {
    stor.ptiR[0][stor.iactu].style.visibility = 'hidden'
    stor.iactu = stor.igard
    stor.ptiR[0][stor.iactu].style.visibility = ''
  }
  function suiv () {
    stor.cashj++
    if (stor.cashj === stor.hj.length) { stor.cashj = 0 }
    return stor.hj[stor.cashj]
  }
  function faisChoixExos () {
    const e = ds.lesExos.pop()
    let i
    let adroite = e.type.indexOf('x') !== -1
    if (e.type.indexOf('01') !== -1) adroite = !adroite
    stor.cas = suiv()
    switch (stor.cas) {
      case 'ent': // un entier normal qui dépasse le rang max
        stor.partieentiereDeb = String(j3pGetRandomInt(Math.pow(10, e.rang), Math.pow(10, e.rang + 2)))
        stor.yavirguleDeb = false
        stor.partiedecimaleDeb = ''
        break
      case 'ent1000': // un entier qui sera pas assez
        stor.partieentiereDeb = String(j3pGetRandomInt(1, Math.pow(10, e.rang) - 1))
        stor.yavirguleDeb = false
        stor.partiedecimaleDeb = ''
        break
      case 'dec': // un dec normal , qui depasse de chaque côté
        stor.partieentiereDeb = String(j3pGetRandomInt(Math.pow(10, e.rang), Math.pow(10, e.rang + 2)))
        if (adroite) stor.partieentiereDeb = String(j3pGetRandomInt(1, 100))
        stor.yavirguleDeb = true
        stor.partiedecimaleDeb = String(j3pGetRandomInt(Math.pow(10, e.rang - 1), Math.pow(10, e.rang + 1)))
        if (!adroite) stor.partiedecimaleDeb = String(j3pGetRandomInt(1, 100))
        stor.partiedecimaleDeb += String(j3pGetRandomInt(1, 9))
        break
      case 'decpur': // ent = 0
        stor.partieentiereDeb = '0'
        stor.yavirguleDeb = true
        stor.partiedecimaleDeb = String(j3pGetRandomInt(Math.pow(10, e.rang - 1), Math.pow(10, e.rang + 1)))
        stor.partiedecimaleDeb += String(j3pGetRandomInt(1, 9))
        break
      case 'ent0': // entier qui finit avec plein de 0
        stor.partieentiereDeb = String(j3pGetRandomInt(10, 1000))
        for (i = 0; i < e.rang; i++) { stor.partieentiereDeb += '0' }
        stor.yavirguleDeb = false
        stor.partiedecimaleDeb = ''
        break
      case 'decpeti': // un dec trop petit
        stor.partieentiereDeb = String(j3pGetRandomInt(1, Math.pow(10, e.rang) - 1))
        stor.yavirguleDeb = true
        stor.partiedecimaleDeb = String(j3pGetRandomInt(1, 9))
        break
    }
    let mu = 1
    if (!adroite) mu = -1
    stor.partieentiereEncours = stor.partieentiereDeb
    stor.yavirguleEncours = stor.yavirguleDeb
    stor.partiedecimaleEncours = stor.partiedecimaleDeb
    stor.monNombre = stor.partieentiereEncours
    let fOnombre = '00000000' + stor.partieentiereEncours + ','
    if (stor.yavirguleEncours) {
      stor.monNombre += ',' + stor.partiedecimaleEncours
      fOnombre += stor.partiedecimaleEncours
    }
    fOnombre += '00000000'
    const pv = fOnombre.indexOf(',')
    stor.monNombre2 = ecrisBienMathquill(parseFloat(stor.monNombre.replace(',', '.')))
    stor.remTrad1 = stor.remTrad2 = stor.remDet = false
    let lp, dv
    const adr = ((e.type === 'x10') || ((e.type === '/01')))
    if (!adr) {
      dv = -1
      lp = ['', 'dixièmes', 'centièmes', 'millièmes', 'dix-millièmes', 'cent-millièmes', 'millionièmes']
    } else {
      dv = +1
      lp = ['', 'dizaines', 'centaines', 'milliers', 'dizaines de mille', 'centaines de mille', 'millions']
    }
    fOnombre = fOnombre.replace(',', '')
    fOnombre = fOnombre.substring(0, pv + dv * e.rang) + ',' + fOnombre.substring(pv + dv * e.rang)
    fOnombre = vire0(fOnombre)
    stor.fOnombre = fOnombre
    stor.repattt = lp[e.rang]
    stor.larepbonne = j3pArrondi(parseFloat(stor.monNombre.replace(',', '.')) * Math.pow(10, mu * e.rang), 10)
    return e
  }
  function vire0 (s) {
    let agard1
    let aret
    for (let i = 0; i < s.length; i++) {
      agard1 = i
      if (s[i] !== '0' || s[i + 1] === ',') break
    }
    let agard2
    for (let i = s.length - 1; i > -1; i--) {
      agard2 = i
      if (s[i] !== '0' || s[i] === ',') break
    }
    aret = s.substring(agard1, agard2 + 1)
    if (aret[aret.length - 1] === ',') aret = aret.substring(0, aret.length - 1)
    return aret
  }

  function initSection () {
    let i

    stor.hj = ['ent1000', 'ent0', 'ent', 'decpur', 'dec', 'decpeti']
    stor.hj = j3pShuffle(stor.hj)
    stor.cashj = j3pGetRandomInt(0, stor.hj.length - 1)
    stor.cas = stor.hj[stor.cashj]

    me.donneesSection = ds

    // Construction de la page
    me.construitStructurePage('presentation3')

    stor.etapos = []
    if (ds.Trad1) stor.etapos.push('Trad1')
    if (ds.Trad2) stor.etapos.push('Trad2')
    if (ds.EtapesDetail) {
      stor.etapos.push('Repvirg')
      stor.etapos.push('Depvirg')
    }
    stor.etapos.push('Rep')

    me.donneesSection.nbetapes = stor.etapos.length

    me.donneesSection.nbitems = me.donneesSection.nbetapes * ds.nbrepetitions

    me.donneesSection.lesExos = []

    if (ds.rang_max > 6) { ds.rang_max = 6 }
    if (ds.rang_max < 1) { ds.rang_max = 1 }

    let lp = []
    if (ds.fois_10)lp.push('x10')
    if (ds.divise_10)lp.push('/10')
    if (ds.fois_01)lp.push('x01')
    if (ds.divise_01)lp.push('/01')
    if (lp.length === 0) {
      console.error('Paramétrage: aucun exercice possible !')
      lp = ['x10']
    }
    let titre1 = 'Multiplier ou diviser par '
    if (!lp.includes('x10') && !lp.includes('x01')) titre1 = 'Diviser par '
    if (!lp.includes('/10') && !lp.includes('/01')) titre1 = 'Multiplier par '
    let titre2 = '10 ou 100 ou ... ou 0,1 ou 0,01 ou ...'
    if (!lp.includes('/01') && !lp.includes('x01')) titre2 = '10 ou 100 ou ...'
    if (!lp.includes('/10') && !lp.includes('x10')) titre2 = '0,1 ou 0,01 ou ... '
    lp = j3pShuffle(lp)
    for (i = 0; i < ds.nbitems; i++) {
      ds.lesExos.push({ type: lp[i % (lp.length)] })
    }
    ds.lesExos = j3pShuffle(ds.lesExos)

    lp = []
    for (i = 1; i < ds.rang_max + 1; i++) { lp.push(i) }
    lp = j3pShuffle(lp)
    for (i = 0; i < ds.nbitems; i++) {
      ds.lesExos[i].rang = lp[i % (lp.length)]
    }
    ds.lesExos = j3pShuffle(ds.lesExos)

    me.typederreurs = [0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0]

    me.score = 0

    me.afficheTitre(titre1 + titre2)
    enonceMain()
  }
  function yaReponse () {
    if (stor.etapos[stor.etapou] === 'Trad2') {
      if (stor.mazonem.reponse() === '') {
        stor.mazonem.focus()
        return false
      }
      if (!stor.liste1.changed) {
        stor.liste1.focus()
        return false
      }
    }
    if (stor.etapos[stor.etapou] === 'Trad1') {
      if (!stor.liste1.changed) {
        stor.liste1.focus()
        return false
      }
    }
    if (stor.etapos[stor.etapou] === 'Rep') {
      if (stor.mazonem.reponse() === '') {
        stor.mazonem.focus()
        return false
      }
    }
    return true
  }
  function isRepOk () {
    stor.errRang = false // tromper nb rang
    stor.errSens = false // confond droite gauche

    stor.errvirgent = false // pas fin entier
    stor.errvirg = false // achete des lunettes

    stor.bougeSens = false
    stor.bougeNb = false

    stor.errEsp = false
    stor.fo = false

    let ok = true
    let repb
    let repat
    if (stor.etapos[stor.etapou] === 'Trad2') {
      stor.mazonem.disable()
      stor.liste1.disable()
      if (parseFloat(stor.mazonem.reponse()) !== stor.Lexo.rang) {
        stor.errRang = true
        ok = false
        stor.mazonem.corrige(false)
        stor.mazonem.barre()
      } else {
        stor.mazonem.corrige(true)
      }
      repb = (stor.liste1.reponse.indexOf('droite') !== -1)
      repat = (stor.bufsens.indexOf('droite') !== -1)
      if (repb !== repat) {
        stor.errSens = true
        ok = false
        stor.liste1.corrige(false)
        stor.liste1.barre()
      } else {
        stor.liste1.corrige(true)
      }
    }
    if (stor.etapos[stor.etapou] === 'Trad1') {
      stor.liste1.disable()
      if (stor.repattt === stor.liste1.reponse) {
        stor.liste1.corrige(true)
        return true
      } else {
        stor.liste1.corrige(false)
        stor.liste1.barre()
        return false
      }
    }
    if (stor.etapos[stor.etapou] === 'Repvirg') {
      ok = stor.bonnerep
      if (!ok) {
        if (!stor.yavirguleEncours) {
          stor.errvirgent = true
        } else {
          stor.errvirg = true
        }
      }
    }
    if (stor.etapos[stor.etapou] === 'Depvirg') {
      j3pEmpty(stor.tabGrof[1][1])
      repb = stor.bufsens.indexOf('gauche') !== -1
      repat = (stor.iactu - stor.igard) / 2
      if (repb !== (repat < 0)) {
        stor.bougeSens = true
        ok = false
      }
      if (stor.Lexo.rang !== Math.abs(repat)) {
        stor.bougeNb = true
        ok = false
      }
    }
    if (stor.etapos[stor.etapou] === 'Rep') {
      repb = stor.mazonem.reponse()
      if (!verifNombreBienEcrit(repb, ds.Espace, true).good) {
        stor.errEsp = true
        stor.remede = verifNombreBienEcrit(repb, ds.Espace, true).remede
        ok = false
      }
      // eslint-disable-next-line no-irregular-whitespace
      repb = repb.replace(/ /g, '')
      repb = repb.replace(/ /g, '')
      if (repb !== stor.fOnombre) {
        stor.fo = true
        ok = false
      }
    }
    return ok
  } // isRepOk
  function vireEtape () {
    j3pEmpty(stor.lesdiv.etape1)
    j3pEmpty(stor.lesdiv.etape2)
    j3pEmpty(stor.lesdiv.etape3)
    j3pEmpty(stor.lesdiv.etape5)
  }

  function affCorrFaux (isFin) {
    let bufrang
    if (stor.Lexo.rang > 1) { bufrang = ' rangs ' } else { bufrang = ' rang ' }
    /// //affiche indic
    if (stor.errRang) {
      j3pAffiche(stor.lesdiv.explications, null, 'Compte le nombre de <i>0</i> !\n')
      stor.yaexplik = true
    }
    if (stor.errSens) {
      if (stor.bufsens === 'gauche') {
        j3pAffiche(stor.lesdiv.explications, null, 'Le résultat doit être plus petit !\n')
      } else {
        j3pAffiche(stor.lesdiv.explications, null, 'Le résultat doit être plus grand !\n')
      }
      stor.yaexplik = true
    }
    if (stor.errvirgent) {
      if (ds.Trad2 && !ds.Trad1) {
        j3pAffiche(stor.lesdiv.explications, null, 'Dans un nombre entier, la virgule est <b><i>à la fin</i></b> !\n')
      } else {
        j3pAffiche(stor.lesdiv.explications, null, 'Dans un nombre entier, le chiffre des unités est à la fin !\n')
      }
      stor.yaexplik = true
    }
    if (stor.errvirg) {
      if (ds.Trad2 && !ds.Trad1) {
        j3pAffiche(stor.lesdiv.explications, null, 'La virgule se voit !\n')
      } else {
        j3pAffiche(stor.lesdiv.explications, null, 'Le chiffre des unités est juste avant la virgule !\n')
      }
    }
    if (stor.bougeSens) {
      j3pAffiche(stor.lesdiv.explications, null, 'Tu n’as pas déplacé la virgule dans le bon sens !\n')
      stor.yaexplik = true
    }
    if (stor.bougeNb) {
      if (ds.Trad2 && !ds.trad1) j3pAffiche(stor.lesdiv.explications, null, 'Tu n’as pas déplacé la virgule de ' + stor.Lexo.rang + bufrang + ' !\n')
      stor.yaexplik = true
    }
    if (stor.errEsp) {
      j3pAffiche(stor.lesdiv.explications, null, stor.remede + '\n')
      stor.yaexplik = true
    }

    if (isFin) {
      // barre les faux

      if (stor.etapos[stor.etapou] === 'Trad2') {
        j3pAffiche(stor.lesdiv.solution, null, stor.buf1 + stor.Lexo.rang + bufrang + 'vers la ' + stor.bufsens + ' .')
        stor.yaco = true
        stor.remTrad2 = true
      }
      if (stor.etapos[stor.etapou] === 'Trad1') {
        j3pAffiche(stor.lesdiv.solution, null, stor.buf1bis + stor.repattt)
        stor.yaco = true
        stor.remTrad1 = true
      }
      if (stor.etapos[stor.etapou] === 'Repvirg') {
        if (ds.Trad2 && !ds.Trad1) stor.goodiid.innerHTML = ' <b>,</b> '
        stor.goodiid.style.color = me.styles.petit.correction.color
      }
      if (stor.etapos[stor.etapou] === 'Depvirg') {
        stor.ptiR[0][stor.iactu].style.color = me.styles.cfaux
        if (stor.bufsens.indexOf('gauche') !== -1) {
          bufrang = -1
        } else {
          bufrang = 1
        }
        stor.ptiR[0][stor.igard + bufrang * stor.Lexo.rang * 2].style.color = me.styles.petit.correction.color
        stor.ptiR[0][stor.igard + bufrang * stor.Lexo.rang * 2].style.visibility = ''
        stor.iactu = stor.igard + bufrang * stor.Lexo.rang * 2
        stor.remDet = true
      }
      if (stor.etapos[stor.etapou] === 'Rep') {
        stor.mazonem.corrige(false)
        stor.mazonem.disable()
        stor.mazonem.barre()
        stor.yaco = true
        j3pAffiche(stor.lesdiv.solution, null, '$' + stor.monNombre2 + stor.bufdep + '=' + ecrisBienMathquill(stor.larepbonne) + '$')
      }
    }
  } // affCorrFaux
  function faisfaux () {
    if (ds.Trad2 && !ds.Trad1) j3pAffiche(this, null, ' <b>,</b> ')
    this.style.color = me.styles.cfaux
    stor.bonnerep = false
    dezaff()
    me.sectionCourante()
  }
  function faisbon () {
    this.style.color = me.styles.cbien
    if (ds.Trad2 && !ds.Trad1) {
      this.innerHTML = '<b>,</b>'
    }
    stor.bonnerep = true
    dezaff()
    me.sectionCourante()
  }
  function dezaff () {
    for (let i = 0; i < stor.tgh[0].length; i++) {
      stor.tgh[0][i].removeEventListener('click', faisfaux)
      stor.tgh[0][i].removeEventListener('click', faisbon)
      stor.tgh[0][i].classList.remove('Multdiv10baseC')
    }
  }
  function affichenbclik () {
    if (!ds.Trad2 || ds.Trad1) {
      stor.tgh = addDefaultTable(stor.lesdiv.travail3, 1, stor.monNombre.length * 2 - 1)
      modif(stor.tgh)
      const yaeuv = stor.monNombre.indexOf(',')
      for (let i = 0; i < stor.monNombre.length; i++) {
        const vk = j3pAffiche(stor.tgh[0][i * 2], null, '$' + stor.monNombre[i] + '$')
        if (i !== yaeuv) {
          stor.tgh[0][i * 2].classList.add('Multdiv10baseC')
          vk.mqList[0].style.cursor = 'pointer'
        }
        if (i !== stor.monNombre.length - 1) {
          j3pAffiche(stor.tgh[0][i * 2 + 1], null, '&nbsp;')
        }
        stor.tgh[0][i * 2].addEventListener('click', faisfaux, false)
      }
      const lb = yaeuv === -1 ? stor.monNombre.length - 1 : yaeuv - 1
      stor.tgh[0][lb * 2].removeEventListener('click', faisfaux)
      stor.tgh[0][lb * 2].addEventListener('click', faisbon, false)
      stor.goodiid = stor.tgh[0][lb * 2]
    } else {
      stor.tgh = addDefaultTable(stor.lesdiv.travail3, 1, stor.monNombre.length * 2 + 1)
      modif(stor.tgh)
      const yaeuv = stor.monNombre.indexOf(',')
      for (let i = 0; i < stor.monNombre.length; i++) {
        const vk = j3pAffiche(stor.tgh[0][i * 2 + 1], null, '$' + stor.monNombre[i] + '$')
        vk.mqList[0].style.cursor = 'pointer'
        stor.tgh[0][i * 2 + 1].classList.add('Multdiv10baseC')
        vk.mqList[0].style.cursor = 'pointer'
        stor.tgh[0][i * 2 + 1].addEventListener('click', faisfaux, false)

        j3pAffiche(stor.tgh[0][i * 2], null, '&nbsp;')
        stor.tgh[0][i * 2].classList.add('Multdiv10baseC')
        stor.tgh[0][i * 2].addEventListener('click', faisfaux, false)
      }
      j3pAffiche(stor.tgh[0][stor.monNombre.length * 2], null, '&nbsp;')
      stor.tgh[0][stor.monNombre.length * 2].classList.add('Multdiv10baseC')
      stor.tgh[0][stor.monNombre.length * 2].addEventListener('click', faisfaux, false)

      const lb = yaeuv === -1 ? 2 * stor.monNombre.length - 1 : 2 * yaeuv
      stor.tgh[0][lb + 1].removeEventListener('click', faisfaux)
      stor.tgh[0][lb + 1].addEventListener('click', faisbon, false)
      stor.goodiid = stor.tgh[0][lb + 1]
    }
    stor.lesdiv.travail3.classList.add('travail')
  }
  function enonceMain () {
    if (me.etapeCourante === 1) {
      me.videLesZones()
      creeLesDiv()
      stor.Lexo = faisChoixExos()
      stor.lesdiv.consigneG.classList.add('enonce')
      if (me.donneesSection.structure === 'presentation3') {
        me.ajouteBoutons()
        me.cacheBoutonSectionSuivante()
      }
    }
    poseQuestion()
    stor.lesdiv.solution.classList.remove('correction')
    stor.lesdiv.explications.classList.remove('explique')
    stor.lesdiv.correction.innerHTML = ''
    stor.lesdiv.solution.innerHTML = ''
    stor.lesdiv.explications.innerHTML = ''
    me.finEnonce()
    if (stor.etapos[stor.etapou] === 'Repvirg') me.cacheBoutonValider()
  }

  switch (this.etat) {
    case 'enonce':
      // code exécuté au lancement de la section
      if (me.debutDeLaSection) {
        initSection()
      } else {
        enonceMain()
      }
      break // case "enonce":

    case 'correction':
      // On teste si une réponse a été saisie
      stor.yaexplik = false
      stor.yaco = false
      stor.lesdiv.explications.innerHTML = ''
      stor.lesdiv.solution.innerHTML = ''
      stor.lesdiv.solution.classList.remove('correction')
      stor.lesdiv.explications.classList.remove('explique')

      if (yaReponse()) {
        // Bonne réponse
        if (isRepOk()) {
          this._stopTimer()
          this.score++
          stor.lesdiv.correction.style.color = this.styles.cbien
          stor.lesdiv.correction.innerHTML = cBien

          if ((me.questionCourante % ds.nbetapes) === 0) {
            stor.mazonem.corrige(true)
            stor.mazonem.disable()
          }
          this.typederreurs[0]++
          this.cacheBoutonValider()
          this.etat = 'navigation'
          this.sectionCourante()
        } else {
          // Pas de bonne réponse
          stor.lesdiv.correction.style.color = this.styles.cfaux
          stor.lesdiv.correction.innerHTML = cFaux
          if (this.isElapsed) {
            // A cause de la limite de temps :
            this._stopTimer()

            stor.lesdiv.correction.innerHTML += tempsDepasse + '<BR>'
            this.typederreurs[10]++
            this.cacheBoutonValider()

            affCorrFaux(true)

            this.etat = 'navigation'
            this.sectionCourante()
          } else {
            // Réponse fausse :
            if (me.essaiCourant < me.donneesSection.nbchances) {
              stor.lesdiv.correction.innerHTML += '<br>' + essaieEncore

              affCorrFaux(false)

              this.typederreurs[1]++
            } else {
              // Erreur au nème essai
              this._stopTimer()
              this.cacheBoutonValider()

              affCorrFaux(true)

              this.typederreurs[2]++
              this.etat = 'navigation'
              this.sectionCourante()
            }
          }
        }
      } else {
        // pas de réponse
        stor.lesdiv.correction.style.color = this.styles.cfaux
        stor.lesdiv.correction.innerHTML = reponseIncomplete
        this.afficheBoutonValider()
      }

      if (stor.yaexplik) {
        stor.lesdiv.explications.classList.add('explique')
      }
      if (stor.yaco) {
        stor.lesdiv.solution.classList.add('correction')
      }

      // Obligatoire
      this.finCorrection()
      break // case "correction":

    case 'navigation':
      if (!this.sectionTerminee()) {
        this.etat = 'enonce'
      }
      this.finNavigation()
      break // case "navigation":
  }
}
