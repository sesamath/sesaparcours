import { j3pAddElt, j3pArrondi, j3pClone, j3pGetRandomBool, j3pGetRandomInt, j3pNotify, j3pShuffle } from 'src/legacy/core/functions'
import { j3pAffiche } from 'src/lib/mathquill/functions'
import { addDefaultTable } from 'src/legacy/themes/table'
import textesGeneriques from 'src/lib/core/textes'

import '../../css/operations.scss'

const { cBien, cFaux, essaieEncore, reponseIncomplete, tempsDepasse } = textesGeneriques

/**
 * Ajouter une description ici
 * @fileOverview
 * @author Tommy Barroy <t.barroy@laposte.net>
 * @since 2017-10
*/

export const params = {
  outils: ['mathquill', 'calculatrice'],
  /**
   * Ce qui est paramétrable dans l’éditeur de graphe
   * @type {LegacySectionParamsParametres}
   */
  parametres: [
    ['nbrepetitions', 9, 'entier', 'Nombre de répétitions de la section'],
    ['limite', 0, 'entier', 'Temps disponible pour répondre'],
    // le modèle gère la compatibilité ascendante avec l’ancien paramètre MiseEnPagePlusClaire
    ['theme', 'standard', 'liste', 'Modèle pour l’affichage', ['standard', 'zonesAvecImageDeFond']]
  ],
  // dans le cas d’une section QUANTITATIVE
  pe: [
    { pe_1: 'Egalite' },
    { pe_2: 'Ecriture' },
    { pe_3: 'Produit_en_croix' },
    { pe_3: 'Simplification' }
  ]
}

const liste2 = [
  { n1: 'a+bxc', n2: 'a+r', num: 0 },
  { n1: 'axb+c', n2: 'r+c', num: 1 },
  { n1: '(a+b)xc', n2: 'rxc', num: 2 },
  { n1: 'cx(a+b)', n2: 'cxr', num: 3 },
  { n1: 'cx(a+b)xd', n2: 'cxr', num: 4 },
  { n1: 'c+a+bxd', n2: 'cxr', num: 5 },
  { n1: 'c+axb+d', n2: 'cxr', num: 6 }
]

/**
 * section priorite01
 * @this {Parcours}
 */
export default function main () {
  const me = this
  const stor = me.storage
  const ds = me.donneesSection

  function poseQuestion () {
    stor.listop = []
    j3pAffiche(stor.lesdiv.consigneG, null, '<b>Sélectionne l’opération prioritaire</b>')
    stor.tabGro = addDefaultTable(stor.lesdiv.travail, 3, 1)
    stor.tabGro2 = addDefaultTable(stor.tabGro[0][0], 1, 1 + stor.Lexo.formule.n1.length)
    j3pAffiche(stor.tabGro2[0][0], null, '$A=$')
    remp(stor.Lexo.formule.n1)
    me.cacheBoutonValider()
    stor.litchoix = []
  }

  function remp (quoi) {
    stor.encours = 'remp'
    let i
    let cmpt = 0
    for (i = 0; i < quoi.length; i++) {
      switch (quoi[i]) {
        case 'a': j3pAffiche(stor.tabGro2[0][i + 1], null, '$' + stor.a + '$')
          break
        case 'b': j3pAffiche(stor.tabGro2[0][i + 1], null, '$' + stor.b + '$')
          break
        case 'c': j3pAffiche(stor.tabGro2[0][i + 1], null, '$' + stor.c + '$')
          break
        case 'd': j3pAffiche(stor.tabGro2[0][i + 1], null, '$' + stor.d + '$')
          break
        case 'r': j3pAffiche(stor.tabGro2[0][i + 1], null, '$' + stor.r + '$')
          break
        case '(':j3pAffiche(stor.tabGro2[0][i + 1], null, '$($')
          break
        case ')':j3pAffiche(stor.tabGro2[0][i + 1], null, '$)$')
          break
        case '+':
          stor.listop.push({ num: cmpt, where: stor.tabGro2[0][i + 1] })
          if (stor.plus) {
            j3pAffiche(stor.tabGro2[0][i + 1], null, '$+$')
          } else {
            j3pAffiche(stor.tabGro2[0][i + 1], null, '$-$')
          }
          stor.tabGro2[0][i + 1].addEventListener('click', clicOp, false)
          stor.tabGro2[0][i + 1].style.cursor = 'pointer'
          stor.tabGro2[0][i + 1].num = cmpt
          stor.tabGro2[0][i + 1].setAttribute('class', 'opautre')
          cmpt++
          break
        case 'x':
          stor.listop.push({ num: cmpt, where: stor.tabGro2[0][i + 1] })
          if (stor.fois) {
            j3pAffiche(stor.tabGro2[0][i + 1], null, '$\\times$')
          } else {
            j3pAffiche(stor.tabGro2[0][i + 1], null, '$\\div$')
          }
          stor.tabGro2[0][i + 1].addEventListener('click', clicOp, false)
          stor.tabGro2[0][i + 1].style.cursor = 'pointer'
          stor.tabGro2[0][i + 1].num = cmpt
          stor.tabGro2[0][i + 1].setAttribute('class', 'opautre')
          cmpt++
          break
      }
    }
  }
  function clicOp () {
    stor.rep = this.num
    stor.litchoix.push(this)
    me.sectionCourante()
  }

  function creeLesDiv () {
    stor.lesdiv = {}
    stor.lesdiv.conteneur = j3pAddElt(me.zones.MG, 'div', {
      style: me.styles.etendre('toutpetit.enonce', { padding: '10px' })
    })
    stor.lesdiv.consigneG = addDefaultTable(stor.lesdiv.conteneur, 1, 1)[0][0]
    stor.lesdiv.etape = addDefaultTable(stor.lesdiv.conteneur, 1, 1)[0][0]
    stor.lesdiv.travail = addDefaultTable(stor.lesdiv.conteneur, 1, 1)[0][0]
    stor.lesdiv.explications = j3pAddElt(stor.lesdiv.conteneur, 'div')
    stor.lesdiv.solution = j3pAddElt(stor.lesdiv.conteneur, 'div')
    stor.lesdiv.calculatrice = j3pAddElt(me.zones.MD, 'div', {
      style: me.styles.etendre('toutpetit.enonce', { padding: '10px' })
    })
    stor.lesdiv.aide = j3pAddElt(me.zones.MD, 'div', {
      style: me.styles.etendre('toutpetit.enonce', { padding: '10px' })
    })
    stor.lesdiv.correction = j3pAddElt(me.zones.MD, 'div', {
      style: me.styles.etendre('toutpetit.enonce', { padding: '10px' })
    })
    stor.lesdiv.etape.style.color = me.styles.toutpetit.correction.color
    stor.lesdiv.solution.style.color = me.styles.toutpetit.correction.color
    stor.lesdiv.explications.style.color = me.styles.cfaux
  }
  function faisChoixExos () {
    const e = ds.lesExos.pop()
    stor.fois = j3pGetRandomBool()
    stor.plus = j3pGetRandomBool()
    switch (e.formule.num) {
      case 0:
        stor.lop = 1
        stor.c = j3pGetRandomInt(1, 10)
        if (stor.fois) {
          stor.b = j3pGetRandomInt(2, 4)
          stor.r = stor.b * stor.c
        } else {
          stor.b = j3pGetRandomInt(2, 5) * stor.c
          stor.r = Math.round(stor.b / stor.c)
        }
        if (stor.plus) {
          stor.a = j3pGetRandomInt(2, 10)
          stor.r2 = stor.r + stor.a
        } else {
          stor.a = stor.r + j3pGetRandomInt(1, 10)
          stor.r2 = stor.a - stor.r
        }
        break
      case 1:
        stor.lop = 0
        stor.b = j3pGetRandomInt(1, 10)
        if (stor.fois) {
          stor.a = j3pGetRandomInt(1, 10)
          stor.r = stor.a * stor.b
        } else {
          stor.r = j3pGetRandomInt(2, 5)
          stor.a = stor.r * stor.b
        }
        if (stor.plus) {
          stor.c = j3pGetRandomInt(1, 10)
          stor.r2 = stor.c + stor.r
        } else {
          stor.c = j3pGetRandomInt(1, stor.r - 1)
          stor.r2 = stor.r - stor.c
        }
        break
      case 2:
        stor.lop = 0
        stor.c = j3pGetRandomInt(1, 10)
        if (stor.fois) {
          stor.r = j3pGetRandomInt(3, 10)
          stor.r2 = stor.r * stor.c
        } else {
          stor.r2 = j3pGetRandomInt(2, 10)
          stor.r = stor.r2 * stor.c
        }
        stor.b = j3pGetRandomInt(1, stor.r - 1)
        if (stor.plus) {
          stor.a = stor.r - stor.b
        } else {
          stor.a = stor.r + stor.b
        }
        break
      case 3:
        stor.lop = 1
        stor.b = j3pGetRandomInt(1, 10)
        if (stor.plus) {
          stor.a = j3pGetRandomInt(1, 10)
          stor.r = stor.a + stor.b
        } else {
          stor.r = j3pGetRandomInt(1, 10)
          stor.a = stor.r + stor.b
        }
        if (stor.fois) {
          stor.c = j3pGetRandomInt(1, 10)
          stor.r2 = stor.r * stor.c
        } else {
          stor.r2 = j3pGetRandomInt(2, 10)
          stor.c = stor.r * stor.r2
        }
        break
      case 4:
        stor.lop = 1
        stor.a = j3pGetRandomInt(1, 10)
        stor.b = j3pGetRandomInt(1, 10)
        stor.c = j3pGetRandomInt(1, 10)
        stor.d = j3pGetRandomInt(1, 10)
        break
      case 5:
        stor.lop = 2
        stor.a = j3pGetRandomInt(1, 10)
        stor.b = j3pGetRandomInt(1, 10)
        stor.c = j3pGetRandomInt(1, 10)
        stor.d = j3pGetRandomInt(1, 10)
        break
      case 6:
        stor.lop = 1
        stor.a = j3pGetRandomInt(1, 10)
        stor.b = j3pGetRandomInt(1, 10)
        stor.c = j3pGetRandomInt(1, 10)
        stor.d = j3pGetRandomInt(1, 10)
        break
      default:
        stor.lop = 0
        stor.a = j3pGetRandomInt(1, 10)
        stor.b = j3pGetRandomInt(1, 10)
        stor.c = j3pGetRandomInt(1, 10)
        stor.d = j3pGetRandomInt(1, 10)
    }
    return e
  }
  function initSection () {
    // Construction de la page
    me.construitStructurePage('presentation1bis')

    ds.lesExos = []

    let lp = j3pClone(liste2)
    lp = j3pShuffle(lp)
    for (let i = 0; i < ds.nbitems; i++) {
      ds.lesExos.push({ formule: lp[i % (lp.length)] })
    }
    ds.lesExos = j3pShuffle(ds.lesExos)

    me.typederreurs = [0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0]

    me.score = 0

    me.afficheTitre('Opération prioritaire')
    enonceMain()
  }

  function yaReponse () {
    if (me.isElapsed) return true
    if (stor.encours === 'remp') {
      if (stor.rep === undefined || stor.rep > stor.listop.length - 1) return false
      return true
    }
  }
  function isRepOk () {
    const ok = true

    if (stor.encours === 'remp') {
      return (stor.rep === stor.lop)
    }

    return ok
  } // isRepOk
  function desactiveAll () {
    let i
    if (stor.encours === 'remp') {
      for (i = 0; i < stor.listop.length; i++) {
        stor.listop[i].where.removeEventListener('click', clicOp)
        stor.listop[i].where.setAttribute('class', '')
      }
    }
  } // desactiveAll
  function passeToutVert () {
    stor.listop[stor.lop].where.setAttribute('class', 'opautrevrai')
  } // passeToutVert
  function barrelesfo () {
    stor.listop[stor.lop].where.setAttribute('class', 'opautreco')
    if (stor.rep !== -1) {
      if (stor.listop[stor.rep] === undefined) {
        j3pNotify('Pour tom',
          { j3p: { leschoix: stor.litchoix, listop: stor.listop, lop: stor.lop, rep: stor.rep, formule: stor.Lexo.formule.num } })
      } else {
        stor.listop[stor.rep].where.setAttribute('class', 'opautrefo')
      }
    }
  }
  function affCorrFaux (isFin) {
    /// //affiche indic
    let tt = ''
    if (isFin) {
      barrelesfo()
      desactiveAll()
      // barre les faux
      switch (stor.Lexo.formule.num) {
        case 0:
        case 5:
        case 6:
        case 1: tt = 'La multiplication et la division sont prioritaires sur l’addition et la soustraction.'
          break
        case 4:
        case 2:
        case 3: tt = 'L’opération entre parenthèses est prioritaire.'
          break
      }
      j3pAffiche(stor.lesdiv.solution, null, tt)
    }
  } // affCorrFaux
  function enonceMain () {
    if (((me.questionCourante % ds.nbetapes) === 1) || (ds.nbetapes === 1)) {
      me.videLesZones()
    }
    creeLesDiv()
    stor.lesdiv.travail.classList.add('travail')
    stor.lesdiv.consigneG.classList.add('enonce')
    stor.Lexo = faisChoixExos()
    poseQuestion()

    me.finEnonce()
  }

  switch (this.etat) {
    case 'enonce':
      // code exécuté au lancement de la section
      if (me.debutDeLaSection) {
        initSection()
      } else {
        enonceMain()
      }
      break // case "enonce":

    case 'correction':
      // On teste si une réponse a été saisie
      stor.yaexplik = false
      stor.yaco = false
      stor.lesdiv.explications.innerHTML = ''
      stor.lesdiv.solution.innerHTML = ''

      if (yaReponse()) {
        // Bonne réponse
        if (isRepOk()) {
          this._stopTimer()
          stor.lesdiv.correction.style.color = this.styles.cbien
          stor.lesdiv.correction.innerHTML = cBien

          desactiveAll()
          passeToutVert()

          this.score++
          this.typederreurs[0]++
          this.cacheBoutonValider()
          this.etat = 'navigation'
          this.sectionCourante()
        } else {
          // Pas de bonne réponse
          stor.lesdiv.correction.style.color = this.styles.cfaux
          stor.lesdiv.correction.innerHTML = cFaux
          if (this.isElapsed) {
            // A cause de la limite de temps :
            this._stopTimer()

            stor.lesdiv.correction.innerHTML = tempsDepasse + '<BR>'
            this.typederreurs[10]++
            this.cacheBoutonValider()

            stor.rep = -1
            affCorrFaux(true)
            stor.yaco = true

            this.etat = 'navigation'
            this.sectionCourante()
          } else {
            // Réponse fausse :
            if (this.essaiCourant < ds.nbchances) {
              stor.lesdiv.correction.innerHTML += '<br>' + essaieEncore

              affCorrFaux(false)

              // affCorrFaux.call(this, false)
              this.typederreurs[1]++
            } else {
              // Erreur au nème essai
              this._stopTimer()
              this.cacheBoutonValider()

              affCorrFaux(true)
              stor.yaco = true

              if (stor.encours === 'compare') {
                this.score = j3pArrondi(this.score + 0.1, 1)
              }
              if (stor.encours === 'calcul') {
                this.score = j3pArrondi(this.score + 0.4, 1)
              }
              if (stor.encours === 'concl') {
                this.score = j3pArrondi(this.score + 0.7, 1)
              }

              this.typederreurs[2]++
              this.etat = 'navigation'
              this.sectionCourante()
            }
          }
        }
      } else {
        // pas de réponse
        stor.lesdiv.correction.style.color = this.styles.cfaux
        stor.lesdiv.correction.innerHTML = reponseIncomplete
        this.afficheBoutonValider()
      }

      // Obligatoire
      this.finCorrection()
      break // case "correction":

    case 'navigation':
      if (!this.sectionTerminee()) {
        this.etat = 'enonce'
      }
      this.finNavigation()
      break // case "navigation":
  }
}
