import { j3pAjouteBouton, j3pAddElt, j3pArrondi, j3pDetruit, j3pEmpty, j3pGetRandomInt, j3pModale, j3pShowError, j3pShuffle } from 'src/legacy/core/functions'
import { colorKo, colorOk } from 'src/legacy/core/StylesJ3p'
import BoutonStyleMathquill from 'src/legacy/outils/boutonsStyleMatquill'
import BulleAide from 'src/legacy/outils/bulleAide/BulleAide'
import ListeDeroulante from 'src/legacy/outils/listeDeroulante/ListeDeroulante'
import { verifNombreBienEcrit } from 'src/legacy/outils/fonctions/ecritureNombre'
import { getPe, initCptPe } from 'src/legacy/sections/college/helperTommy'
import { j3pAffiche } from 'src/lib/mathquill/functions'
import { ecrisBienMathquill } from 'src/legacy/outils/zoneStyleMathquill/functions'
import ZoneStyleMathquill1 from 'src/legacy/outils/zoneStyleMathquill/ZoneStyleMathquill1'
import { addDefaultTable } from 'src/legacy/themes/table'
import textesGeneriques from 'src/lib/core/textes'

import imgCase from './case.png'
import imgCaseH from './caseh.png'
import imgCaseB from './caseb.png'
import imgDroite from './droite.png'
import imgGauche from './gauche.png'
import imgEff from './eff.png'

const { cBien, cFaux, essaieEncore, reponseIncomplete, tempsDepasse } = textesGeneriques

// nos pe
const pe1 = 'Pose'
const pe2 = 'Calcul'
const pe3 = 'Egalite'
const pe4 = 'Virgule_multiplication'
const pe5 = 'Vocabulaire'
const pe6 = 'Ecriture'

/**
 * Ajouter une description ici
 * @fileOverview
 * @author Tommy Barroy <t.barroy@laposte.net>
 * @since 2017-10
 */
export const params = {
  outils: ['mathquill'],
  /**
   * Ce qui est paramétrable dans l’éditeur de graphe
   * @type {LegacySectionParamsParametres}
   */
  parametres: [
    ['nbrepetitions', 3, 'entier', 'Nombre de répétitions de la section'],
    ['nbchances', 2, 'entier', 'Nombre d’essais possibles.'],
    ['addition', true, 'boolean', '<u>true</u>: Opération proposée.'],
    ['soustraction', true, 'boolean', '<u>true</u>: Opération proposée.'],
    ['multiplication', true, 'boolean', '<u>true</u>: Opération proposée.'],
    ['division_entiere', true, 'boolean', '<u>true</u>: Opération proposée.'],
    ['division_decimale', true, 'boolean', '<u>true</u>: Opération proposée.'],
    ['Diviseur2', 'parfois', 'liste', '<i>Valable uniquement pour <b>division</b> </i> <br><br></b></i>Le diviseur est supérieur à 11.', ['parfois', 'toujours', 'jamais']],
    ['Virgule', true, 'boolean', '<u>true</u>: Décimaux possibles'],
    ['NbChiffresMin', 3, 'entier', 'Nombre de chiffres minimum (entre 2 et 5)'],
    ['NbChiffresMax', 3, 'entier', 'Nombre de chiffres maximum (entre 2 et 5)'],
    ['Progression', 'aletoire', 'liste', 'Evolution du nombre de chiffres entre min et max.', ['aletoire', 'lineaire']],
    ['Vocabulaire', true, 'boolean', '<i>Valable uniquement quand <b>Pose</b> est à true</i> <br><br></b></i><u>true</u>: L’élève doit déterminer la bonne opération.'],
    ['Pose', true, 'boolean', '<u>true</u>: L’élève doit poser l’opération.'],
    ['Calcul', true, 'boolean', '<u>true</u>: L’élève doit effectuer l’opération.'],
    ['Egalite', true, 'boolean', '<u>true</u>: Egalité finale à écrire.'],
    ['Parentheses_div', false, 'boolean', '<i>Uniquement pour <u>Egalité</u> de <u>division_entière</u> </i><br><br><u>true</u>: Parenthèses dans l’égalité.'],
    ['Faux_table', false, 'boolean', '<i>Uniquement pour <u>Calcul</u> de <u>division</u> </i><br><br><u>true</u>: L’opération est comptée fausse si la table n’est pas affichée pour un diviseur supérieur à 11.'],
    ['Mul_Virg_Seule', false, 'boolean', '<i>Uniquement pour <u>Calcul</u> de <u>multiplication</u> </i><br><br><u>true</u>: La multiplication est effectuée, l’élève doit seulement placer la virgule.'],
    // le modèle gère la compatibilité ascendante avec l’ancien paramètre MiseEnPagePlusClaire
    ['theme', 'standard', 'liste', 'Modèle pour l’affichage', ['standard', 'zonesAvecImageDeFond']]

  ],
  // dans le cas d’une section QUANTITATIVE
  pe: [
    { pe_1: pe1 },
    { pe_2: pe2 },
    { pe_3: pe3 },
    { pe_4: pe4 },
    { pe_5: pe5 },
    { pe_6: pe6 }
  ]
}

/**
 * section poseoperation
 * @this {Parcours} {
 */
export default function main () {
  const me = this
  const ds = me.donneesSection
  const stor = this.storage

  function initSection () {
    stor.tempo = 50
    stor.tempodeb = 1000
    stor.depCentreCo = 2
    initCptPe(ds, stor)
    stor.lisop = ['+', '-', 'x', '/']

    me.validOnEnter = false // ex donneesSection.touche_entree

    if (ds.Mul_Virg_Seule) ds.Virgule = true
    // Construction de la page
    me.construitStructurePage('presentation1bis')

    let titreDebut = ''
    stor.afaire = []
    if (ds.Pose) {
      stor.afaire.push('p')
      titreDebut = 'Poser '
    }
    if (ds.Calcul) {
      stor.afaire.push('c')
      titreDebut = 'Effectuer '
    }
    if (ds.Egalite) {
      stor.afaire.push('e')
      titreDebut = 'Ecrire l’égalité pour '
    }
    if (stor.afaire.length === 0) {
      j3pShowError('Aucune question dans les paramètres')
      stor.afaire.push('p')
    }
    if (stor.afaire.length !== 1) {
      titreDebut = 'Travailler sur '
    }

    // stor.nbetapes = stor.afaire.length
    // ds.nbitems = ds.nbrepetitions * stor.nbetapes
    me.surcharge({ nbetapes: stor.afaire.length })

    ds.NbChiffresMax = Math.max(2, ds.NbChiffresMax)
    ds.NbChiffresMin = Math.max(2, ds.NbChiffresMin)
    ds.NbChiffresMax = Math.min(5, ds.NbChiffresMax)
    ds.NbChiffresMin = Math.min(5, ds.NbChiffresMin)
    if (ds.NbChiffresMin > ds.NbChiffresMax) ds.NbChiffresMax = ds.NbChiffresMin

    // les variantes possibles pour remplir nos exos
    ds.exos = []
    // on pourrait faire une seule boucle de remplissage de ces exos à la fin, en utilisant j3pGetRandomElt pour chaque propriété
    // mais on veut que le max de cas distincts soient utilisés, donc on rempli les propriétés une par une,
    // en prenant pour chacune les cas possibles à tour de rôle
    // et on mélange les exos avant de passer à la propriété suivante

    // propriété nbCh (le nb de chiffres)
    const nbChiffres = []
    for (let i = ds.NbChiffresMin; i < ds.NbChiffresMax + 1; i++) nbChiffres.push(i)
    for (let i = 0; i < ds.nbitems; i++) {
      ds.exos.push({ nbCh: nbChiffres[i % nbChiffres.length] })
    }

    // op (opérateur à utiliser)
    const operateurs = []
    let titreFin = ''
    if (ds.addition) {
      operateurs.push('+')
      titreFin = ' une addition.'
    }
    if (ds.soustraction) {
      operateurs.push('-')
      titreFin = ' une soustraction.'
    }
    if (ds.multiplication) {
      operateurs.push('x')
      titreFin = ' une multiplication.'
    }
    if (ds.division_decimale) {
      operateurs.push('/')
      titreFin = ' une division décimale.'
    }
    if (ds.division_entiere) {
      operateurs.push(':')
      titreFin = ' une division entière.'
    }
    if (operateurs.length === 0) {
      j3pShowError('Aucun opération validée dans les paramètres')
      operateurs.push('+')
      titreFin = ' une addition.'
    }
    if (operateurs.length > 1) {
      titreFin = 'une opération.'
      ds.exos = j3pShuffle(ds.exos)
      ds.exos.forEach((exo, i) => { exo.op = operateurs[i % operateurs.length] })
    } else {
      // pas besoin de mélanger c’est le même pour tout le monde
      ds.exos.forEach(exo => { exo.op = operateurs[0] })
    }

    // divGrand : true si diviseur > 11
    if (ds.Diviseur2 === 'parfois') {
      // on alterne 1 ou 2 dans divGrand
      ds.exos = j3pShuffle(ds.exos)
      ds.exos.forEach((exo, i) => { exo.divGrand = i % 2 === 0 })
    } else {
      // toujours l’un ou l’autre
      ds.exos.forEach(exo => { exo.divGrand = ds.Diviseur2 === 'toujours' })
    }

    // eg : un booléen où false est deux fois plus fréquent, c’est quoi ?
    const egs = [true, false, false]
    ds.exos = j3pShuffle(ds.exos)
    ds.exos.forEach((exo, i) => { exo.eg = egs[i % egs.length] })

    if (ds.Progression === 'lineaire') {
      // on trie par nbCh croissant
      ds.exos.sort((a, b) => b.nbCh - a.nbCh)
    } else {
      ds.exos = j3pShuffle(ds.exos)
    }

    me.typederreurs = [0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0]
    me.afficheTitre(titreDebut + titreFin)
    stor.restric = '0123456789'
    if (ds.Virgule) stor.restric += ',.'
    stor.encours = stor.afaire[stor.afaire.length - 1]
    enonceMain()
  }

  function faisChoixExos () {
    stor.laifois = undefined
    stor.laitable = undefined
    stor.tabPo = undefined
    const e = ds.exos.pop()
    stor.nb1 = 0
    stor.ardiv = 1
    if (e.nbCh < 3 && e.op === ':' && e.divGrand) e.nbCh = 3
    if (e.nbCh < 2 && e.op === ':' && !e.divGrand) e.nbCh = 2
    let b1, b2
    for (let i = 0; i < e.nbCh; i++) {
      b1 = 0
      b2 = 9
      if ((i === 0) || (i === e.nbCh - 1)) b1 = 1
      if (e.nbCh === 1) b1 = 2
      stor.nb1 = j3pArrondi(stor.nb1 + j3pGetRandomInt(b1, b2) * Math.pow(10, i), 1)
      stor.nb1Cmons = stor.nb1
    }
    stor.nb2 = 0
    for (let i = 0; i < e.nbCh; i++) {
      b1 = 0
      b2 = 9
      if ((i === 0) || (i === e.nbCh - 1)) b1 = 1
      if (e.nbCh === 1) b1 = 2
      stor.nb2 = j3pArrondi(stor.nb2 + j3pGetRandomInt(b1, b2) * Math.pow(10, i), 1)
    }
    if ((e.op === 'x') && (e.eg)) {
      stor.nb1 = stor.nb1 - stor.nb1 % 2
      stor.nb2 = stor.nb2 - stor.nb2 % 5
      if (stor.nb2 % 10 === 0) stor.nb2 += 5
      if (stor.nb1 % 10 === 0) stor.nb1 += 2
    }
    if ((e.op === '/') || (e.op === ':')) {
      if (e.divGrand) {
        stor.nb2 = j3pGetRandomInt(12, 99)
      } else {
        stor.nb2 = j3pGetRandomInt(2, 11)
      }
      if (e.eg) {
        stor.nb1 = j3pArrondi(stor.nb1 - stor.nb1 % stor.nb2, 0)
      } else {
        if (stor.nb1 % stor.nb2 === 0) stor.nb1++
      }
    }
    if (ds.Virgule || e.op === '/') {
      switch (e.op) {
        case '+':
        case '-':
        case 'x': {
          const l1 = j3pGetRandomInt(0, e.nbCh)
          const l2 = j3pGetRandomInt(0, e.nbCh)
          stor.nb1 = j3pArrondi(stor.nb1 * Math.pow(10, -l1), e.nbCh)
          stor.nb2 = j3pArrondi(stor.nb2 * Math.pow(10, -l2), e.nbCh)
          if (e.eg && (e.op === '+')) {
            const l4 = e.nbCh - l1
            const l3 = e.nbCh - l2
            if (stor.nb1 > stor.nb2) {
              stor.nb2 = j3pArrondi(Math.pow(10, l4) - stor.nb1, e.nbCh)
            } else {
              stor.nb1 = j3pArrondi(Math.pow(10, l3) - stor.nb2, e.nbCh)
            }
          }
          if (e.eg && (e.op === '-')) {
            stor.nb2 = j3pGetRandomInt(1, stor.nb1Cmons - 1)
            stor.nb2 = stor.nb2 - stor.nb2 % 10
            stor.nb2 = stor.nb1Cmons - stor.nb2
            stor.nb2 = j3pArrondi(stor.nb2 * Math.pow(10, -l1), e.nbCh)
          }
          break
        }

        case '/': {
          const lim = Math.max(1, e.nbCh - 2)
          stor.ardiv = j3pGetRandomInt(lim, 3)
          stor.nb1 = j3pArrondi(stor.nb1 * Math.pow(10, -j3pGetRandomInt(lim, stor.ardiv)), stor.ardiv)
          stor.len1 = j3pArrondi(stor.nb1 * Math.pow(10, stor.ardiv), 0)
        }
      }
    }
    if (e.op === ':') stor.len1 = stor.nb1
    stor.mescase = []
    stor.occuped = []
    return e
  }

  function poseQuestion () {
    j3pEmpty(stor.lesdiv.explications)
    j3pEmpty(stor.lesdiv.solution)
    j3pEmpty(stor.lesdiv.correction)
    switch (stor.encours) {
      case 'p': posePose()
        break
      case 'c': poseCalcul()
        break
      case 'e': poseEgalite()
        break
    }
    stor.lesdiv.explications.classList.remove('explique')
    stor.lesdiv.solution.classList.remove('correction')
  }

  function creeLesDiv () {
    stor.lesdiv = {}
    stor.lesdiv.conteneur = j3pAddElt(me.zonesElts.MG, 'div', {
      style: me.styles.etendre('toutpetit.enonce', { padding: '10px' })
    })
    stor.lesdiv.consigne = addDefaultTable(stor.lesdiv.conteneur, 1, 1)[0][0]
    stor.lesdiv.travail = addDefaultTable(stor.lesdiv.conteneur, 1, 1)[0][0]
    stor.lesdiv.zonefig = addDefaultTable(stor.lesdiv.conteneur, 1, 1)[0][0]
    stor.lesdiv.zonedrep = addDefaultTable(stor.lesdiv.conteneur, 1, 1)[0][0]
    stor.lesdiv.explications = j3pAddElt(stor.lesdiv.conteneur, 'div')
    stor.lesdiv.solution = j3pAddElt(stor.lesdiv.conteneur, 'div')
    stor.lesdiv.correction = j3pAddElt(me.zonesElts.MD, 'div', {
      style: me.styles.etendre('toutpetit.enonce', { padding: '10px' })
    })
    stor.lesdiv.explications.style.color = me.styles.cfaux
    stor.lesdiv.solution.style.color = me.styles.toutpetit.correction.color
  }

  function poseEgalite () {
    j3pEmpty(stor.lesdiv.consigne)
    stor.lop = stor.Lexo.op
    switch (stor.Lexo.op) {
      case '+':
        stor.koi = 'la somme de $' + ecrisBienMathquill(stor.nb1) + '$ et $' + ecrisBienMathquill(stor.nb2) + '$'
        break
      case '-':
        stor.koi = 'la différence entre $' + ecrisBienMathquill(stor.nb1) + '$ et $' + ecrisBienMathquill(stor.nb2) + '$'
        break
      case 'x':
        stor.koi = 'le produit de $' + ecrisBienMathquill(stor.nb1) + '$ et $' + ecrisBienMathquill(stor.nb2) + '$'
        break
      case ':':
        stor.koi = 'le quotient entier de $' + ecrisBienMathquill(stor.nb1) + '$ par $' + ecrisBienMathquill(stor.nb2) + '$'
        break
      case '/':
        stor.koi = 'le quotient décimal de $' + ecrisBienMathquill(stor.nb1) + '$ par $' + ecrisBienMathquill(stor.nb2) + '$'
        break
    }
    stor.tabEg = addDefaultTable(stor.lesdiv.consigne, 3, 1)
    j3pAffiche(stor.tabEg[0][0], null, 'On a calculé ' + stor.koi + ' \n')
    let s = 'dixième'
    let sd = ' à $0,1$ près'
    if (stor.ardiv === 2) {
      s = 'centième'
      sd = ' à $0,01$ près'
    }
    if (stor.ardiv === 3) {
      s = 'millième'
      sd = ' à $0,001$ près'
    }
    if (stor.Lexo.op === '/') j3pAffiche(stor.tabEg[0][0], null, '(<i> approximation décimale par défaut au ' + s + ')</i> \n')

    if (stor.afaire.indexOf('c') === -1) {
      intiop()
    }
    stor.tabPo = addDefaultTable(stor.tabEg[2][0], 1, 3)
    afficheBonneOpe('ca')
    completeOpe('ca')

    j3pAffiche(stor.tabEg[2][0], null, '<b>Complète l’égalité suivante: </b> \n\n')
    j3pEmpty(stor.lesdiv.solution)
    j3pEmpty(stor.lesdiv.correction)
    j3pEmpty(stor.lesdiv.travail)
    stor.tabEgPo = addDefaultTable(stor.lesdiv.travail, 1, 4)
    let n1 = ecrisBienMathquill(stor.nb1)
    let n2 = ecrisBienMathquill(stor.nb2)
    switch (stor.lop) {
      case '+':
        j3pAffiche(stor.tabEgPo[0][0], null, '$' + n1 + ' + ' + n2 + ' = $')
        stor.lazone1 = new ZoneStyleMathquill1(stor.tabEgPo[0][1], { restric: stor.restric, limitenb: 1, limite: 10, hasAutoKeyboard: false, enter: me.sectionCourante.bind(me), tabauto: false })
        break
      case '-':
        if (stor.nb2 > stor.nb1) {
          s = n1
          n1 = n2
          n2 = s
        }
        j3pAffiche(stor.tabEgPo[0][0], null, '$' + n1 + ' - ' + n2 + ' = $')
        stor.lazone1 = new ZoneStyleMathquill1(stor.tabEgPo[0][1], { restric: stor.restric, limitenb: 1, limite: 10, hasAutoKeyboard: false, enter: me.sectionCourante.bind(me), tabauto: false })
        break
      case 'x':
        j3pAffiche(stor.tabEgPo[0][0], null, '$' + n1 + ' \\times ' + n2 + ' = $')
        stor.lazone1 = new ZoneStyleMathquill1(stor.tabEgPo[0][1], { restric: stor.restric, limitenb: 1, limite: 10, hasAutoKeyboard: false, enter: me.sectionCourante.bind(me), tabauto: false })
        break
      case '/':
        j3pAffiche(stor.tabEgPo[0][0], null, '$' + n1 + ' \\div ' + n2 + '$&nbsp;')
        stor.lazone1 = new ZoneStyleMathquill1(stor.tabEgPo[0][2], { restric: stor.restric + '.,', limitenb: 1, limite: 10, hasAutoKeyboard: false, enter: me.sectionCourante.bind(me), tabauto: false })
        stor.laliste1 = ListeDeroulante.create(stor.tabEgPo[0][1], ['Choisir', '$=$', '$\\approx$'], {
          centre: true
        })
        j3pAffiche(stor.tabEgPo[0][3], null, '&nbsp;' + sd)
        break
      case ':':
        if (ds.Parentheses_div) {
          j3pAffiche(stor.tabEgPo[0][0], null, '$' + n1 + ' = ( \\text{ }' + n2 + ' \\times $')
        } else {
          j3pAffiche(stor.tabEgPo[0][0], null, '$' + n1 + ' = ' + n2 + ' \\times $')
        }
        stor.lazone1 = new ZoneStyleMathquill1(stor.tabEgPo[0][1], { restric: stor.restric, limitenb: 1, limite: 10, hasAutoKeyboard: false, enter: me.sectionCourante.bind(me), tabauto: false })
        if (ds.Parentheses_div) {
          j3pAffiche(stor.tabEgPo[0][2], null, '&nbsp;$) + $')
        } else {
          j3pAffiche(stor.tabEgPo[0][2], null, '$ + $')
        }
        stor.lazone2 = new ZoneStyleMathquill1(stor.tabEgPo[0][3], { restric: stor.restric, limitenb: 1, limite: 10, hasAutoKeyboard: false, enter: me.sectionCourante.bind(me), tabauto: false })
        break
    }
  }

  /**
   * @todo ajouter une description et renommer fonction et arqument
   * @param sss
   */
  function intiop (sss) {
    let pv2, nbch1, nbch2, nb, n1, n2, d1, d2, cpt, k, su
    switch (stor.lop) {
      case '+':
        break
      case 'x':
        n1 = String(Math.max(stor.nb1, stor.nb2)).replace('.', ',')
        n2 = String(Math.min(stor.nb2, stor.nb1)).replace('.', ',')
        nbch1 = nbch2 = d1 = d2 = 0
        for (let i = 0; i < n1.length; i++) {
          if ((n1[i] !== '0') && (n1[i] !== ',')) nbch1++
          if (n1[i] !== ',') d1++
        }
        for (let i = 0; i < n2.length; i++) {
          if ((n2[i] !== '0') && (n2[i] !== ',')) nbch2++
          if (n2[i] !== ',') d2++
        }
        if (nbch1 < nbch2) {
          nb = n1
          n1 = n2
          n2 = nb
          d1 = d2
        }
        nb = n2
        nb = nb.replace(',', '').replace(/0/g, '')
        nb = nb.length
        while (n1.length !== n2.length) {
          if (n1.length < n2.length) n1 = ' ' + n1
          if (n2.length < n1.length) n2 = ' ' + n2
        }
        for (let i = 0; i < nb + 1; i++) {
          stor.occuped[i] = true
        }
        for (let i = 0; i < n1.length; i++) {
          stor.occuped[i + 1 + nb] = true
          if (sss !== true) {
            stor.bout = i + 1 + nb
          }
        }
        stor.nb = nb
        stor.nbch1 = d1
        stor.garde = n2.replace(',', '')
        stor.garde2 = n1.replace(',', '')
        pv2 = []
        nbch1 = 0
        nbch2 = -1
        for (let i = stor.garde.length - 1; i > -1; i--) {
          if (!isNaN(stor.garde[i]) && stor.garde[i] !== '0') {
            nbch2++
            su = parseInt(stor.garde[i]) * parseInt(stor.garde2) * Math.pow(10, nbch2)
            if (!isNaN(su)) {
              pv2.push(su)
              nbch1 += su
            }
          } else {
            if (stor.garde[i] === '0') {
              nbch2++
            }
          }
        }
        pv2.push(nbch1)
        stor.gardeTout = [...pv2]
        nbch1 = String(stor.nb1)
        if (nbch1.indexOf('.') === -1) { nbch1 = 0 } else {
          nbch1 = nbch1.length - nbch1.indexOf('.') - 1
        }
        nbch2 = String(stor.nb2)
        if (nbch2.indexOf('.') === -1) { nbch2 = 0 } else {
          nbch2 = nbch2.length - nbch2.indexOf('.') - 1
        }
        stor.gardeV = (nbch1 + nbch2)
        break
      case ':':
      case '/':
        n1 = String(stor.nb1).replace('.', ',')
        n2 = String(stor.nb2).replace('.', ',')
        stor.n1 = n1
        cpt = -1
        for (let i = 0; i < stor.n1.length + 1; i++) {
          k = parseFloat(stor.n1.replace(',', '').substring(0, i))
          if (k >= stor.nb2) break
        }
        stor.consNb = k
        k = String(k)
        cpt = String(stor.len1).length - k.length
        stor.cpt = cpt
        stor.cons2 = String(stor.len1).substring(k.length)
    }
  }
  function poseCalcul () {
    afficheDebutCorrection()
    if (stor.Lexo.op === 'x' && ds.Mul_Virg_Seule) {
      j3pAffiche(stor.lesdiv.consigne, null, '<b>Place la virgule dans le produit.</b> \n\n')
    } else {
      j3pAffiche(stor.lesdiv.consigne, null, '<b>Effectue l’opération.</b> \n\n')
    }
    j3pEmpty(stor.lesdiv.travail)
    stor.tabPo = addDefaultTable(stor.lesdiv.travail, 1, 3)
    modif(stor.tabPo, 1, 3)
    afficheBonneOpe()
    let cpt, k, z, la, mi, mj, prepre, first, gg, oldgg, last, prepre2, fifi
    stor.casesRep = []
    switch (stor.lop) {
      case '+':
        fifi = undefined
        stor.casesRep = []
        stor.casesRet = []
        for (let i = 0; i < 12; i++) {
          if (stor.occuped[i]) {
            stor.casesRep.push(new ZoneStyleMathquill1(stor.TabOp[3][i + 1], { id: 'moncalRep2c' + i + 'z', restric: stor.restric, limitenb: 1, limite: 0, hasAutoKeyboard: false, enter: me.sectionCourante.bind(me), tabauto: (fifi !== undefined), prevTab: fifi }))
            fifi = 'moncalRep2c' + i + 'z'
          }
          if (stor.occuped[i] && (i !== 0) && stor.occuped[i + 1] && (i !== stor.souvPLus[0] + 1)) {
            stor.casesRet.push(new ZoneStyleMathquill1(stor.TabOp[0][i + 1], { id: 'moncalRet2c' + i + 'z', restric: stor.restric, limitenb: 1, limite: 0, hasAutoKeyboard: false, enter: me.sectionCourante.bind(me), tabauto: (fifi !== undefined), prevTab: fifi }))
            fifi = 'moncalRet2c' + i + 'z'
          }
          stor.TabOp[0][0].parentNode.style.fontSize = '80%'
        }
        break
      case '-':
        fifi = undefined
        stor.casesRep = []
        for (let i = 0; i < 12; i++) {
          if (stor.occuped[i]) {
            stor.casesRep.push(new ZoneStyleMathquill1(stor.TabOp[3][i + 1], { id: 'moncalRep2c' + i + 'z', restric: stor.restric, limitenb: 1, limite: 0, hasAutoKeyboard: false, enter: me.sectionCourante.bind(me), tabauto: (fifi !== undefined), prevTab: fifi }))
            fifi = 'moncalRep2c' + i + 'z'
          }
        }
        break
      case 'x': {
        stor.tabVirg = []
        la = stor.garde.length
        z = -1
        mi = []
        mj = 0
        let g1 = 16
        let g2 = 0
        for (let i = 0; i < stor.nb + 1; i++) {
          stor.casesRep[i] = []
          la--
          z++
          first = false
          // FIXME gg est toujours undefined, il n’a jamais été affecté !
          if (gg !== undefined) {
            oldgg = [gg[0], gg[1]]
          }
          if (stor.garde[la] === '0' && la !== 0) { z++ }
          for (let j = 1; j < 16; j++) {
            if (i !== stor.nb) {
              if (j < stor.bout + 1) {
                g1 = Math.min(g1, j)
                g2 = Math.max(g2, j)
                stor.casesRep[i][stor.casesRep[i].length] = new ZoneStyleMathquill1(stor.TabOp[i + 4][j], {
                  id: 'moncalRep' + (i + 2) + 'c' + j + 'z',
                  restric: stor.restric,
                  limitenb: 1,
                  limite: 0,
                  hasAutoKeyboard: false,
                  enter: me.sectionCourante.bind(me),
                  tabauto: true,
                  // FIXME, au premier tour de boucle prepre est undefined
                  prevTab: prepre
                })
                prepre = 'moncalRep' + (i + 2) + 'c' + j + 'z'
                if (!first) {
                  first = true
                  // FIXME gg n’est jamais utilisé
                  gg = [i, stor.casesRep[i].length - 1]
                }
              }
            }
            if (i === stor.nb - 1) stor.TabOp[i + 4][j].style.borderBottom = '1px solid black'
            if (i === stor.nb) {
              if (j < stor.bout + 1) {
                mj = Math.max(mj, j)
                // FIXME pb here & quoi ser le tab tty ??
                const tty = addDefaultTable(stor.TabOp[i + 4][j], 1, 2)
                stor.casesRep[i][stor.casesRep[i].length] = new ZoneStyleMathquill1(tty[0][1], { id: 'moncalRep' + (i + 2) + 'c' + j + 'z', restric: stor.restric, limitenb: 1, limite: 0, hasAutoKeyboard: false, enter: me.sectionCourante.bind(me), tabauto: true, prevTab: prepre })
                mi.push(j)
                stor.tabVirg[j] = tty
                prepre = 'moncalRep' + (i + 2) + 'c' + j + 'z'
              }
            }
          }
          if (oldgg !== undefined) {
            stor.casesRep[oldgg[0]][oldgg[1]].prevTab = stor.casesRep[i][stor.casesRep[i].length - 1].id
          }
        }
        stor.tabVirg[mj + 1] = addDefaultTable(stor.TabOp[stor.nb + 4][mj + 2], 1, 2)
        z--
        stor.casesRet = []
        for (let j = g1; j < g2 + 1; j++) {
          if ((j > stor.bout - 1 - stor.nbch1 - z)) {
            stor.casesRet.push(new ZoneStyleMathquill1(stor.TabOp[3][j], {
              id: 'moncalRet' + j + 'z',
              restric: stor.restric,
              limitenb: 1,
              limite: 0,
              hasAutoKeyboard: false,
              enter: me.sectionCourante.bind(me),
              tabauto: false,
              prevTab: prepre
            }))
          }
        }
        j3pAffiche(stor.TabOp[stor.nb + 3][0], null, '$+$')
        stor.plmax = mi[mi.length - 1]
        stor.plmin = mi[0]
        stor.pl = stor.plmax
        stor.TabOp[3][0].parentNode.style.fontSize = '50%'
        stor.TabOp[3][0].parentNode.style.display = 'none'
        const tabJK = addDefaultTable(stor.tabPo[0][2], 5, 1)
        const tabFD = addDefaultTable(tabJK[0][0], 1, 3)
        stor.casesRet.push(new ZoneStyleMathquill1(tabFD[0][1], {
          id: 'moncalRet' + g2 + 'z',
          restric: stor.restric,
          limitenb: 1,
          limite: 0,
          hasAutoKeyboard: false,
          enter: me.sectionCourante.bind(me),
          tabauto: false,
          prevTab: prepre
        }))
        stor.tabPo[0][1].style.width = '50px'
        j3pAffiche(tabFD[0][0], null, '(<i>case retenue pour la multiplication:</i>&nbsp; ')
        j3pAffiche(tabFD[0][2], null, '&nbsp;) ')
        stor.tabFD = tabFD
        stor.retii = stor.casesRet.length - 1
        stor.rePdon = j3pAjouteBouton(tabJK[2][0], caseret, { className: 'MepBoutons', value: 'Cases retenue addition' })
        if (ds.Virgule) j3pAffiche(tabJK[3][0], null, 'virgule')
        if (ds.Virgule) stor.tabPo[0][2].style.verticalAlign = 'Bottom'
        // eslint-disable-next-line no-new
        if (ds.Virgule) new BoutonStyleMathquill(tabJK[3][0], 'bx', ['im2', 'im3'], [gau1, dro1], { image: [{ ch: 'im3', url: imgDroite }, { ch: 'im2', url: imgGauche }] })
        if (ds.Virgule) { dro1() } else {
          stor.plmax = 0
          stor.pl = 1
        }
        if (ds.Mul_Virg_Seule) {
          intiop(true)
          for (let i = 0; i < stor.casesRep.length; i++) {
            for (let j = 0; j < stor.casesRep[i].length; j++) {
              stor.casesRep[i][j].disable()
            }
          }
          for (let i = 0; i < stor.casesRet.length; i++) {
            stor.casesRet[i].disable()
          }
          j3pEmpty(tabJK[0][0])
          j3pEmpty(tabJK[1][0])
          j3pEmpty(tabJK[2][0])
          completeOpe('ca2')
        }
      }
        break
      case ':':
        cpt = -1
        for (let i = 0; i < stor.n1.length + 1; i++) {
          k = parseFloat(stor.n1.replace(',', '').substring(0, i))
          if (k >= stor.nb2) break
        }
        stor.consNb = k
        k = String(k)
        cpt = stor.n1.replace(',', '').replace(/ /g, '').length - k.length
        z = 1
        stor.cpt = cpt
        stor.cons2 = String(stor.len1).substring(k.length)
        for (let i = 0; i < cpt + 1; i++) {
          stor.casesRep[i * 2] = []
          stor.casesRep[i * 2 + 1] = []
          j3pAffiche(stor.TabOp[i * 2 + 1][4 - (k.length + cpt) + i + z], null, '$-$')
          stor.TabOp[i * 2 + 1][4 - (k.length + cpt) + i + z].style.borderBottom = '1px solid black'
          first = false
          for (let j = 0; j < 6; j++) {
            if ((j > 4 + z - (k.length + cpt) + i) && (j < 6 - cpt + i)) {
              la = j
              if (first === false) {
                first = true
                prepre2 = [i * 2 + 1, stor.casesRep[i * 2 + 1].length]
              }
              stor.casesRep[i * 2][stor.casesRep[i * 2].length] = new ZoneStyleMathquill1(stor.TabOp[i * 2 + 1][j], { id: 'moncalRep' + (i * 2 + 1) + 'c' + j + 'z', restric: stor.restric, limitenb: 1, limite: 0, hasAutoKeyboard: false, enter: me.sectionCourante.bind(me), tabauto: true })
              // FIXME au premier tour de boucle oldgg est undefined
              stor.casesRep[i * 2 + 1][stor.casesRep[i * 2 + 1].length] = new ZoneStyleMathquill1(stor.TabOp[i * 2 + 2][j], { id: 'moncalRep' + (i * 2 + 2) + 'c' + j + 'z', restric: stor.restric, limitenb: 1, limite: 0, hasAutoKeyboard: false, enter: me.sectionCourante.bind(me), tabauto: true, prevTab: oldgg })
              stor.TabOp[i * 2 + 1][j].style.borderBottom = '1px solid black'
              gg = 'moncalRep' + (i * 2 + 1) + 'c' + j + 'z'
              oldgg = 'moncalRep' + (i * 2 + 2) + 'c' + j + 'z'
              last = 'moncalRep' + (i * 2 + 2) + 'c' + j + 'z'
              if (stor.casesRep[i * 2].length !== 1) {
                stor.casesRep[i * 2][stor.casesRep[i * 2].length - 2].prevTab = 'moncalRep' + (i * 2 + 1) + 'c' + j + 'z'
              }
              prepre = [i * 2, stor.casesRep[i * 2].length - 1]
            }
          }
          // FIXME gérer le cas prepre & last undefined
          stor.casesRep[prepre[0]][prepre[1]].prevTab = last
          if ((i !== cpt) && (la < 6)) {
            // AAAAAA
            // bug sur la ?
            stor.casesRep[i * 2 + 1][stor.casesRep[i * 2 + 1].length] = new ZoneStyleMathquill1(stor.TabOp[i * 2 + 2][la + 1], { id: 'moncalRep' + (i * 2 + 2) + 'c' + (la + 1) + 'z', restric: stor.restric, limitenb: 1, limite: 0, hasAutoKeyboard: false, enter: me.sectionCourante.bind(me), tabauto: true, prevTab: 'moncalquoz' })
            stor.casesRep[prepre2[0]][prepre2[1]].prevTab = 'moncalRep' + (i * 2 + 2) + 'c' + (la + 1) + 'z'
          } else {
            stor.casesRep[prepre2[0]][prepre2[1]].tabauto = false
          }
          z = 0
        }
        stor.fautaff = false
        stor.mesCaseQuo = new ZoneStyleMathquill1(stor.TabOp[1][6], { id: 'moncalquoz', restric: '0123456789', limitenb: 1, limite: 10, hasAutoKeyboard: false, enter: me.sectionCourante.bind(me), tabauto: false })
        stor.rePdon = j3pAjouteBouton(stor.tabPo[0][2], afficheTableau, { className: 'MepBoutons', value: 'Il faut afficher la table' })
        break
      case '/':
        cpt = -1
        for (let i = 0; i < stor.n1.length + 1; i++) {
          k = parseFloat(stor.n1.replace(',', '').substring(0, i))
          if (k >= stor.nb2) break
        }
        stor.consNb = k
        k = String(k)
        cpt = stor.n1.replace(',', '').replace(/ /g, '').length - k.length
        stor.cpt = cpt
        z = 1
        stor.cons2 = String(stor.len1).substring(k.length)
        for (let i = 0; i < cpt + 1; i++) {
          stor.casesRep[i * 2] = []
          stor.casesRep[i * 2 + 1] = []
          j3pAffiche(stor.TabOp[i * 2 + 1][4 - (k.length + cpt) + i + z], null, '$-$')
          stor.TabOp[i * 2 + 1][4 - (k.length + cpt) + i + z].style.borderBottom = '1px solid black'
          first = false
          for (let j = 0; j < 6; j++) {
            if ((j > 4 + z - (k.length + cpt) + i) && (j < 6 - cpt + i)) {
              la = j
              if (first === false) {
                first = true
                prepre2 = [i * 2 + 1, stor.casesRep[i * 2 + 1].length]
              }
              stor.casesRep[i * 2][stor.casesRep[i * 2].length] = new ZoneStyleMathquill1(stor.TabOp[i * 2 + 1][j], { id: 'moncalRep' + (i * 2 + 1) + 'c' + j + 'z', restric: stor.restric, limitenb: 1, limite: 0, hasAutoKeyboard: false, enter: me.sectionCourante.bind(me), tabauto: true })
              stor.casesRep[i * 2 + 1][stor.casesRep[i * 2 + 1].length] = new ZoneStyleMathquill1(stor.TabOp[i * 2 + 2][j], { id: 'moncalRep' + (i * 2 + 2) + 'c' + j + 'z', restric: stor.restric, limitenb: 1, limite: 0, hasAutoKeyboard: false, enter: me.sectionCourante.bind(me), tabauto: true, prevTab: oldgg })
              stor.TabOp[i * 2 + 1][j].style.borderBottom = '1px solid black'
              gg = 'moncalRep' + (i * 2 + 1) + 'c' + j + 'z'
              oldgg = 'moncalRep' + (i * 2 + 2) + 'c' + j + 'z'
              last = 'moncalRep' + (i * 2 + 2) + 'c' + j + 'z'
              if (stor.casesRep[i * 2].length !== 1) {
                stor.casesRep[i * 2][stor.casesRep[i * 2].length - 2].prevTab = 'moncalRep' + (i * 2 + 1) + 'c' + j + 'z'
              }
              prepre = [i * 2, stor.casesRep[i * 2].length - 1]
            }
          }
          stor.casesRep[prepre[0]][prepre[1]].prevTab = last
          if ((i !== cpt) && (la < 6)) {
            stor.casesRep[i * 2 + 1][stor.casesRep[i * 2 + 1].length] = new ZoneStyleMathquill1(stor.TabOp[i * 2 + 2][la + 1], { id: 'moncalRep' + (i * 2 + 2) + 'c' + (la + 1) + 'z', restric: stor.restric, limitenb: 1, limite: 0, hasAutoKeyboard: false, enter: me.sectionCourante.bind(me), tabauto: true, prevTab: 'moncalquoz' })
            stor.casesRep[prepre2[0]][prepre2[1]].prevTab = 'moncalRep' + (i * 2 + 2) + 'c' + (la + 1) + 'z'
          } else {
            stor.casesRep[prepre2[0]][prepre2[1]].tabauto = false
          }
          z = 0
        }
        stor.fautaff = false
        stor.mesCaseQuo = new ZoneStyleMathquill1(stor.TabOp[1][6], { id: 'moncalquoz', restric: '0123456789.,', limitenb: 1, limite: 10, hasAutoKeyboard: false, enter: me.sectionCourante.bind(me), tabauto: false })
        stor.rePdon = j3pAjouteBouton(stor.tabPo[0][2], afficheTableau, { className: 'MepBoutons', value: 'Il faut afficher la table' })
        break
    }
  }
  // @todo mieux nommer cette fonction, ça fait quoi ???
  function caseret () {
    let ok
    for (let i = 0; i < stor.casesRep.length - 1; i++) {
      ok = false
      for (let j = 0; j < stor.casesRep[i].length; j++) {
        ok = ok || (stor.casesRep[i][j].reponse() !== '')
      }
      if (!ok) {
        afficheModale('Tu dois d’abord compléter <br>les termes de l’addition !')
        return
      }
    }
    j3pDetruit(stor.rePdon)
    stor.TabOp[3][0].parentNode.style.display = ''
    j3pEmpty(stor.tabFD[0][0])
    stor.casesRet[stor.retii].disable()
    j3pEmpty(stor.tabFD[0][1])
    j3pEmpty(stor.tabFD[0][2])
  }
  function afficheModale (texte) {
    const yy = j3pModale({ titre: 'Erreur', contenu: '' })
    const uu = j3pAffiche(yy, null, texte)
    uu.parent.style.color = me.styles.toutpetit.correction.color
  }
  function afficheTableau () {
    stor.fautaff = true
    j3pEmpty(stor.tabPo[0][2])
    stor.tabPo[0][1].style.width = '40px'
    const tabBref = addDefaultTable(stor.tabPo[0][2], 11, 1)
    tabBref[10][0].style.color = '#867373'
    for (let i = 0; i < 11; i++) {
      j3pAffiche(tabBref[i][0], null, '$' + i + ' \\times ' + stor.nb2 + ' = ' + (i * stor.nb2) + '$')
    }
  }
  function gau1 () {
    if (stor.pl === stor.plmin) return
    j3pEmpty(stor.tabVirg[stor.pl][0][0])
    j3pEmpty(stor.tabVirg[stor.pl][0][0])
    stor.pl--
    j3pAffiche(stor.tabVirg[stor.pl][0][0], null, '$,$')
    stor.tabVirg[stor.pl][0][0].style.color = ''
  }
  function dro1 () {
    if (stor.pl === stor.plmax + 1) return
    j3pEmpty(stor.tabVirg[stor.pl][0][0])
    stor.pl++
    j3pAffiche(stor.tabVirg[stor.pl][0][0], null, '$,$')
    stor.tabVirg[stor.pl][0][0].style.color = ''
  }

  function coloreVirgule (isOk) {
    stor.tabVirg[stor.pl][0][0].style.color = isOk ? colorOk : colorKo
  }

  function afficheBonneOpe (cache) {
    if (cache === 'co') {
      stor.lop = stor.Lexo.op
      changerT(true, 'co')
    } else if (cache === 'ca') {
      stor.lop = stor.Lexo.op
      changerT(true, 'ca')
    } else {
      changerT(true)
    }
    let n1, n2, nbch1, nbch2, nb, nb2, nb1, p1, p2, d1, d2, cptt, nx
    switch (stor.lop) {
      case '-':
        n1 = String(Math.max(stor.nb1, stor.nb2)).replace('.', ',')
        n2 = String(Math.min(stor.nb2, stor.nb1)).replace('.', ',')
        break
      case 'x':
        n1 = String(Math.max(stor.nb1, stor.nb2)).replace('.', ',')
        n2 = String(Math.min(stor.nb2, stor.nb1)).replace('.', ',')
        nbch1 = nbch2 = d1 = d2 = 0
        for (let i = 0; i < n1.length; i++) {
          if ((n1[i] !== '0') && (n1[i] !== ',')) nbch1++
          if (n1[i] !== ',') d1++
        }
        for (let i = 0; i < n2.length; i++) {
          if ((n2[i] !== '0') && (n2[i] !== ',')) nbch2++
          if (n2[i] !== ',') d2++
        }
        if (nbch1 < nbch2) {
          nb = n1
          n1 = n2
          n2 = nb
          d1 = d2
        }
        break
      case ':':
      case '/':
      case '+':
        n1 = String(stor.nb1).replace('.', ',')
        n2 = String(stor.nb2).replace('.', ',')
        break
    }
    switch (stor.lop) {
      case '+':
        nb1 = n1.indexOf(',')
        if (nb1 === -1) nb1 = n1.length
        nb2 = n2.indexOf(',')
        if (nb2 === -1) nb2 = n2.length
        nb = Math.max(nb1, nb2)
        p1 = n1.substring(0, nb1)
        p2 = n2.substring(0, nb2)
        while (p1.length !== p2.length) {
          if (p1.length < p2.length) p1 = '0' + p1
          if (p2.length < p1.length) p2 = '0' + p2
        }
        d1 = n1.substring(nb1)
        d2 = n2.substring(nb2)
        while (d1.length !== d2.length) {
          if (d1.length < d2.length) d1 += '0'
          if (d2.length < d1.length) d2 += '0'
        }
        stor.occuped[0] = true
        for (let i = 0; i < p1.length; i++) {
          j3pAffiche(stor.TabOp[1][i + 2], null, '$' + p1[i] + '$')
          j3pAffiche(stor.TabOp[2][i + 2], null, '$' + p2[i] + '$')
          stor.occuped[i + 1] = true
        }
        if (d1.length !== 0) {
          const nbc = p1.length
          j3pAffiche(stor.TabOp[1][nbc + 2], null, '$,$')
          j3pAffiche(stor.TabOp[2][nbc + 2], null, '$,$')
          stor.occuped[p1.length + 1] = true
          for (let i = 1; i < d1.length; i++) {
            j3pAffiche(stor.TabOp[1][nbc + i + 2], null, '$' + d1[i] + '$')
            j3pAffiche(stor.TabOp[2][nbc + i + 2], null, '$' + d2[i] + '$')
            stor.occuped[nbc + i + 1] = true
          }
        }
        stor.souvPLus = [p1.length, d1.length - 1]
        break
      case '-':
        stor.aZ = []
        stor.asO = []
        nb1 = n1.indexOf(',')
        if (nb1 === -1) nb1 = n1.length
        nb2 = n2.indexOf(',')
        if (nb2 === -1) nb2 = n2.length
        nb = Math.max(nb1, nb2)
        p1 = n1.substring(0, nb1)
        p2 = n2.substring(0, nb2)
        while (p1.length !== p2.length) {
          if (p1.length < p2.length) p1 = '0' + p1
          if (p2.length < p1.length) p2 = '0' + p2
        }
        d1 = n1.substring(nb1)
        d2 = n2.substring(nb2)
        while (d1.length !== d2.length) {
          if (d1.length < d2.length) d1 += '0'
          if (d2.length < d1.length) d2 += '0'
        }
        stor.occuped[0] = true
        cptt = 0
        for (let i = 0; i < p1.length; i++) {
          stor.aZ[cptt] = addDefaultTable(stor.TabOp[1][i + 2], 1, 2)
          stor.asO[cptt] = addDefaultTable(stor.TabOp[2][i + 2], 1, 2)
          j3pAffiche(stor.aZ[cptt][0][1], null, '$' + p1[i] + '$')
          j3pAffiche(stor.asO[cptt][0][1], null, '$' + p2[i] + '$')
          stor.aZ[cptt][0][0].style.width = '10px'
          stor.asO[cptt][0][0].style.width = '10px'
          if (i !== 0) {
            if ((cache !== 'co') && (cache !== 'ca')) {
              stor.aZ[cptt][0][0].bool = false
              stor.aZ[cptt][0][0].i = cptt
              stor.aZ[cptt][0][0].style.backgroundImage = `url('${imgCase}')`
              stor.aZ[cptt][0][0].style.cursor = 'pointer'
              stor.aZ[cptt][0][0].addEventListener('click', onClick, null)
            }
          }
          stor.occuped[i + 1] = true
          cptt++
        }
        if (d1.length !== 0) {
          const nbc = p1.length
          j3pAffiche(stor.TabOp[1][nbc + 2], null, '$,$')
          j3pAffiche(stor.TabOp[2][nbc + 2], null, '$,$')
          stor.occuped[nbc + 1] = true
          for (let i = 1; i < d1.length; i++) {
            stor.aZ[cptt] = addDefaultTable(stor.TabOp[1][nbc + i + 2], 1, 2)
            stor.asO[cptt] = addDefaultTable(stor.TabOp[2][nbc + i + 2], 1, 2)
            j3pAffiche(stor.aZ[cptt][0][1], null, '$' + d1[i] + '$')
            j3pAffiche(stor.asO[cptt][0][1], null, '$' + d2[i] + '$')
            stor.aZ[cptt][0][0].style.width = '10px'
            stor.asO[cptt][0][0].style.width = '10px'
            stor.occuped[nbc + i + 1] = true
            if ((cache !== 'co') && (cache !== 'ca')) {
              stor.aZ[cptt][0][0].bool = false
              stor.aZ[cptt][0][0].i = cptt
              stor.aZ[cptt][0][0].style.backgroundImage = `url('${imgCase}')`
              stor.aZ[cptt][0][0].style.cursor = 'pointer'
              stor.aZ[cptt][0][0].addEventListener('click', onClick, null)
            }
            cptt++
          }
        }
        stor.cptt = cptt
        stor.souvPLus = [p1.length, d1.length - 1]
        break
      case 'x':
        nx = n2
        nx = nx.replace(',', '')
        nb = nx.replace(/0/g, '')
        nx = nx.length
        nb = nb.length
        while (n1.length !== n2.length) {
          if (n1.length < n2.length) n1 = ' ' + n1
          if (n2.length < n1.length) n2 = ' ' + n2
        }
        for (let i = 0; i < nb + 1; i++) {
          stor.occuped[i] = true
        }
        for (let i = 0; i < n1.length; i++) {
          j3pAffiche(stor.TabOp[1][i + nx + 3], null, '$' + n1[i] + '$')
          j3pAffiche(stor.TabOp[2][i + nx + 3], null, '$' + n2[i] + '$')
          stor.occuped[i + 3 + nx] = true
          stor.bout = i + nx + 3
        }
        stor.nb = nb
        stor.nbch1 = d1
        stor.garde = n2.replace(',', '')
        stor.garde2 = n1.replace(',', '')
        break
      case ':':
        while (n1.length < 6) {
          n1 = ' ' + n1
        }
        while (n2.length < 6) {
          n2 += ' '
        }
        stor.n1 = n1
        for (let i = 0; i < 6; i++) {
          if (n1[i] !== ' ') stor.occuped[i] = true
          j3pAffiche(stor.TabOp[0][i], null, '$' + n1[i] + '$')
          j3pAffiche(stor.TabOp[0][i + 6], null, '$' + n2[i] + '$')
        }
        break
      case '/':
        nb1 = n1.indexOf(',')
        if (nb1 === -1) nb1 = n1.length
        p1 = n1.substring(0, nb1)
        d1 = n1.substring(nb1 + 1)
        while (d1.length < stor.ardiv) {
          d1 += '0'
        }
        nb2 = []
        for (let i = 0; i < p1.length; i++) {
          nb2[i] = p1[i]
        }
        nb2.push(',' + d1[0])
        for (let i = 1; i < d1.length; i++) {
          nb2.push(d1[i])
        }
        while (nb2.length < 6) {
          nb2.splice(0, 0, ' ')
        }
        while (n2.length < 6) {
          n2 += ' '
        }
        stor.n1 = String(stor.len1)

        for (let i = 0; i < 6; i++) {
          if (nb2[i] !== ' ') stor.occuped[i] = true
          j3pAffiche(stor.TabOp[0][i], null, '$' + nb2[i] + '$')
          j3pAffiche(stor.TabOp[0][i + 6], null, '$' + n2[i] + '$')
        }
    }
    for (let i = 1; i < stor.occuped.length; i++) {
      if (stor.occuped[i - 1] === true && stor.occuped[i + 1] === true) stor.occuped[i] = true
    }
  }
  function onClick () {
    this.bool = !this.bool
    if (this.bool) {
      stor.aZ[this.i][0][0].style.backgroundImage = `url('${imgCaseH}')`
      stor.asO[this.i - 1][0][0].style.backgroundImage = `url('${imgCaseB}')`
    } else {
      stor.aZ[this.i][0][0].style.backgroundImage = `url('${imgCase}')`
      stor.asO[this.i - 1][0][0].style.backgroundImage = ''
    }
  }

  function posePose () {
    afficheDebutCorrection()
    if (ds.Vocabulaire) {
      j3pAffiche(stor.lesdiv.consigne, null, '<b>Choisis la bonne opération, puis pose la.</b> \n\n')
    } else {
      j3pAffiche(stor.lesdiv.consigne, null, '<b>Pose l’opération.</b> \n\n')
    }
    stor.tabPo = addDefaultTable(stor.lesdiv.travail, 1, 3)
    modif(stor.tabPo, 1, 3)
    if (ds.Vocabulaire) {
      stor.tabPo[0][1].style.width = '40px'
      j3pAjouteBouton(stor.tabPo[0][2], changerT, { className: 'MepBoutons', value: 'Changer d’opération' })
      stor.lop = stor.lisop[j3pGetRandomInt(0, stor.lisop.length - 1)]
    }
    afficheOpeVide()
  }

  function modif (k, n, m) {
    for (let i = 0; i < n; i++) {
      for (let j = 0; j < m; j++) {
        k[i][j].style.padding = '2px'
        k[i][j].style.textAlign = 'center'
      }
    }
  }
  function afficheDebutCorrection () {
    j3pEmpty(stor.lesdiv.consigne)
    stor.lop = stor.Lexo.op
    switch (stor.Lexo.op) {
      case '+':
        stor.koi = 'la somme de $' + ecrisBienMathquill(stor.nb1) + '$ et $' + ecrisBienMathquill(stor.nb2) + '$'
        break
      case '-':
        stor.koi = 'la différence entre $' + ecrisBienMathquill(stor.nb1) + '$ et $' + ecrisBienMathquill(stor.nb2) + '$'
        break
      case 'x':
        stor.koi = 'le produit de $' + ecrisBienMathquill(stor.nb1) + '$ et $' + ecrisBienMathquill(stor.nb2) + '$'
        break
      case ':':
        stor.koi = 'le quotient entier de $' + ecrisBienMathquill(stor.nb1) + '$ par $' + ecrisBienMathquill(stor.nb2) + '$'
        break
      case '/':
        stor.koi = 'le quotient décimal de $' + ecrisBienMathquill(stor.nb1) + '$ par $' + ecrisBienMathquill(stor.nb2) + '$'
        break
    }
    j3pAffiche(stor.lesdiv.consigne, null, 'On veut calculer ' + stor.koi + ' \n')
    let s = ''
    if (stor.ardiv === 1) s = 'dixième'
    if (stor.ardiv === 2) s = 'centième'
    if (stor.ardiv === 3) s = 'millième'
    if (stor.Lexo.op === '/') j3pAffiche(stor.lesdiv.consigne, null, '(<i> approximation décimale par défaut au ' + s + ')</i> \n')
  }
  function afficheOpeVide (cache) {
    stor.mescase = []
    let bufco = stor.tabPo[0][0]
    let bufbla = 'black'
    if (cache === 'co') {
      bufco = stor.lesdiv.solution
      bufbla = 'blue'
    }
    if (cache === 'ca') {
      bufco = stor.tabEg[1][0]
      bufbla = 'black'
    }
    j3pEmpty(bufco)
    let ch = '$+$'
    switch (stor.lop) {
      case '+':
      case '-':
      case 'x':
        if (stor.lop === 'x') ch = '$\\times$'
        if (stor.lop === '-') ch = '$-$'
        stor.TabOp = addDefaultTable(bufco, 12, 20)
        modif(stor.TabOp, 12, 20)
        stor.TabOp[2][0].parentNode.style.borderBottom = '1px solid ' + bufbla
        j3pAffiche(stor.TabOp[2][0], null, ch)
        if ((cache !== true) && (cache !== 'co') && (cache !== 'ca')) {
          for (let i = 0; i < 2; i++) {
            stor.mescase[i] = []
            for (let j = 0; j < 11; j++) {
              stor.mescase[i][j] = new ZoneStyleMathquill1(stor.TabOp[i + 1][j + 1], { restric: stor.restric, limitenb: 1, limite: 0, hasAutoKeyboard: false, enter: me.sectionCourante.bind(me), tabauto: (j !== 10) })
            }
          }
          stor.d1 = new BoutonStyleMathquill(stor.TabOp[1][14], 'b1', ['im3', 'im2', 'im1'], [ga1, dr1, eff1], { image: [{ ch: 'im1', url: imgEff }, { ch: 'im2', url: imgDroite }, { ch: 'im3', url: imgGauche }] })
          stor.d2 = new BoutonStyleMathquill(stor.TabOp[2][14], 'b2', ['im3', 'im2', 'im1'], [ga2, dr2, eff2], { image: [{ ch: 'im1', url: imgEff }, { ch: 'im2', url: imgDroite }, { ch: 'im3', url: imgGauche }] })
        }
        break
      case '/':
      case ':':
        stor.TabOp = addDefaultTable(bufco, 20, 13)
        modif(stor.TabOp, 20, 13)
        stor.mescase = []
        stor.mescase[0] = []
        for (let j = 0; j < 12; j++) {
          if (j > 5) stor.TabOp[0][j].style.borderBottom = '1px solid ' + bufbla
        }
        for (let j = 0; j < 20; j++) {
          stor.TabOp[j][6].style.borderLeft = '1px solid ' + bufbla
          stor.TabOp[j][6].style.height = '5px'
        }
        if ((cache !== true) && (cache !== 'co') && (cache !== 'ca')) stor.mescase[0][0] = new ZoneStyleMathquill1(stor.TabOp[0][5], { restric: '0123456789.,', limitenb: 1, limite: 10, hasAutoKeyboard: false, enter: me.sectionCourante.bind(me), tabauto: false })
        if ((cache !== true) && (cache !== 'co') && (cache !== 'ca')) stor.mescase[0][1] = new ZoneStyleMathquill1(stor.TabOp[0][6], { restric: '0123456789', limitenb: 1, limite: 10, hasAutoKeyboard: false, enter: me.sectionCourante.bind(me), tabauto: false })
        break
    }
  }
  function changerT (bool, cache) {
    for (let i = stor.mescase.length - 1; i > -1; i--) {
      for (let j = stor.mescase[i].length - 1; j > -1; j--) {
        stor.mescase[i][j].disable()
        stor.mescase[i].splice(j, 1)
      }
      stor.mescase.splice(i, 1)
    }
    if (bool !== true) {
      let index = stor.lisop.indexOf(stor.lop)
      index++
      if (index === stor.lisop.length) index = 0
      stor.lop = stor.lisop[index]
    }
    if (cache === 'co') bool = 'co'
    if (cache === 'ca') bool = 'ca'
    afficheOpeVide(bool)
    if (cache !== 'co') j3pEmpty(stor.lesdiv.explications)
    stor.lesdiv.explications.classList.remove('explique')
    stor.lesdiv.solution.classList.remove('correction')
  }
  function eff1 () {
    for (let i = 0; i < 11; i++) stor.mescase[0][i].modif('')
  }
  function eff2 () {
    for (let i = 0; i < 11; i++) stor.mescase[1][i].modif('')
  }
  function ga1 () {
    const z = stor.mescase[0][0].reponse()
    for (let i = 0; i < 10; i++) {
      stor.mescase[0][i].modif(stor.mescase[0][i + 1].reponse())
    }
    stor.mescase[0][10].modif(z)
  }
  function ga2 () {
    const z = stor.mescase[1][0].reponse()
    for (let i = 0; i < 10; i++) stor.mescase[1][i].modif(stor.mescase[1][i + 1].reponse())
    stor.mescase[1][10].modif(z)
  }
  function dr1 () {
    const z = stor.mescase[0][10].reponse()
    for (let i = 10; i > 0; i--) stor.mescase[0][i].modif(stor.mescase[0][i - 1].reponse())
    stor.mescase[0][0].modif(z)
  }
  function dr2 () {
    const z = stor.mescase[1][10].reponse()
    for (let i = 10; i > 0; i--) stor.mescase[1][i].modif(stor.mescase[1][i - 1].reponse())
    stor.mescase[1][0].modif(z)
  }
  function yaReponse () {
    let ok
    if (stor.encours === 'p') {
      switch (stor.lop) {
        case '+':
        case '-':
        case 'x':
          ok = false
          for (let i = 0; i < 11; i++) {
            ok = ok || stor.mescase[0][i].reponse() !== ''
          }
          if (!ok) {
            stor.mescase[0][0].focus()
            return false
          }
          ok = false
          for (let i = 0; i < 11; i++) {
            ok = ok || stor.mescase[1][i].reponse() !== ''
          }
          if (!ok) {
            stor.mescase[1][0].focus()
            return false
          }
          return true
        case '/':
        case ':':
          if (stor.mescase[0][0].reponse() === '') {
            stor.mescase[0][0].focus()
            return false
          }
          if (stor.mescase[0][1].reponse() === '') {
            stor.mescase[0][1].focus()
            return false
          }
          return true
      }
    }
    if (stor.encours === 'c') {
      switch (stor.lop) {
        case '+':
          ok = false
          for (let i = 0; i < stor.casesRep.length; i++) {
            ok = ok || stor.casesRep[i].reponse() !== ''
          }
          if (!ok) {
            stor.casesRep[stor.casesRep.length - 1].focus()
            return false
          }
          break
        case '-':
          ok = false
          for (let i = 0; i < stor.casesRep.length; i++) {
            ok = ok || stor.casesRep[i].reponse() !== ''
          }
          if (!ok) {
            stor.casesRep[stor.casesRep.length - 1].focus()
            return false
          }
          break
        case 'x':
          if (ds.Mul_Virg_Seule) return true
          for (let i = 0; i < stor.casesRep.length; i++) {
            ok = false
            for (let j = 0; j < stor.casesRep[i].length; j++) {
              ok = ok || stor.casesRep[i][j].reponse() !== ''
            }
            if (!ok) {
              stor.casesRep[i][stor.casesRep[i].length - 1].focus()
              return false
            }
          }
          break
        case ':':
        case '/':
          for (let i = 0; i < stor.casesRep.length; i++) {
            if (i % 2 === 0) {
              // verif que dernier
              if (stor.casesRep[i][stor.casesRep[i].length - 1].reponse() === '') {
                stor.casesRep[i][stor.casesRep[i].length - 1].focus()
                return false
              }
            } else {
              // verif dernier
              if (stor.casesRep[i][stor.casesRep[i].length - 1].reponse() === '') {
                stor.casesRep[i][stor.casesRep[i].length - 1].focus()
                return false
              }
              if (i !== stor.casesRep.length - 1) {
                // verif avant dernier
                if (stor.casesRep[i][stor.casesRep[i].length - 2].reponse() === '') {
                  stor.casesRep[i][stor.casesRep[i].length - 2].focus()
                  return false
                }
              }
            }
          }
          if (stor.mesCaseQuo.reponse() === '') {
            stor.mesCaseQuo.focus()
            return false
          }
      }
      return true
    }
    if (stor.encours === 'e') {
      switch (stor.lop) {
        case '+':
        case '-':
        case 'x':
          if (stor.lazone1.reponse() === '') {
            stor.lazone1.focus()
            return false
          }
          return true
        case '/':
          if (stor.lazone1.reponse() === '') {
            stor.lazone1.focus()
            return false
          }
          if (!stor.laliste1.changed) {
            stor.laliste1.focus()
            return false
          }
          return true
        case ':':
          if (stor.lazone1.reponse() === '') {
            stor.lazone1.focus()
            return false
          }
          if (stor.lazone2.reponse() === '') {
            stor.lazone2.focus()
            return false
          }
          return true
      }
    }
  }
  function isRepOk () {
    let n1, n2, pv1, pv2, nbch1, nbch2, ok, ret, jkl
    stor.errLOP = false
    stor.errParetrou = false
    stor.errAligneV = false
    stor.errMoinsHaut = false
    stor.errInvD = false
    stor.errEcrit = false
    stor.LerrEcrit = ''
    stor.errCalcul = false
    stor.errVfois = false
    stor.errTableTropG = false
    stor.errTableTropP = false
    stor.errTable = false
    stor.LerrTable = ''
    stor.errSous = false
    stor.errDesc = false
    stor.errQuo = false
    stor.errSigne = false
    stor.errAffta = false
    stor.errVQ = false
    stor.errAproch = false
    stor.errInv1 = false

    if (stor.encours === 'p') {
      if (stor.lop !== stor.Lexo.op) {
        if ((stor.Lexo.op !== ':') || stor.lop !== '/') {
          stor.errLOP = true
          corrigeP(false)
          return false
        }
      }
      // recupe stor.nb1 stor.nb2
      n1 = n2 = ''
      switch (stor.lop) {
        case '+':
        case '-':
        case 'x':
          for (let i = 0; i < 11; i++) {
            n1 += stor.mescase[0][i].reponse()
            if (stor.mescase[0][i].reponse() === '') n1 += ' '
          }
          for (let i = 0; i < 11; i++) {
            n2 += stor.mescase[1][i].reponse()
            if (stor.mescase[1][i].reponse() === '') n2 += ' '
          }
          break
        default:
          n1 = stor.mescase[0][0].reponse()
          n2 = stor.mescase[0][1].reponse()
      }
      pv1 = n1.indexOf(',')
      pv2 = n2.indexOf(',')
      if (pv1 === -1) {
        for (let j = n1.length - 1; j > -1; j--) {
          if (n1[j] !== ' ') {
            pv1 = j + 1
            break
          }
        }
      }
      if (pv2 === -1) {
        for (let j = n2.length - 1; j > -1; j--) {
          if (n2[j] !== ' ') {
            pv2 = j + 1
            break
          }
        }
      }
      while (n1.indexOf(' ') === 0) {
        n1 = n1.substring(1)
      }
      while (n1.lastIndexOf(' ') === n1.length - 1) {
        n1 = n1.substring(0, n1.length - 1)
      }
      while (n2.indexOf(' ') === 0) {
        n2 = n2.substring(1)
      }
      while (n2.lastIndexOf(' ') === n2.length - 1) {
        n2 = n2.substring(0, n2.length - 1)
      }
      nbch1 = nbch2 = 0
      for (let i = 0; i < n1.length; i++) {
        if ((n1[i] !== ',') && (n1[i] !== '0')) nbch1++
      }
      for (let i = 0; i < n2.length; i++) {
        if ((n2[i] !== ',') && (n2[i] !== '0')) nbch2++
      }

      // verif nb bien ecrit
      ok = verifNombreBienEcrit(n1, false, false, (stor.Lexo.op !== ':' && stor.Lexo.op !== '/'))
      if (!ok.good) {
        stor.errEcrit = true
        corrigeFn(1)
        stor.LerrEcrit = ok.remede
      }
      ok = verifNombreBienEcrit(n2, false, false, (stor.Lexo.op !== ':' && stor.Lexo.op !== '/'))
      if (!ok.good) {
        stor.errEcrit = true
        corrigeFn(2)
        if (ok.remede !== stor.LerrEcrit) {
          stor.LerrEcrit += '<br>' + ok.remede
        }
      }

      ret = n1

      n1 = parseFloat(n1.replace(',', '.'))
      n2 = parseFloat(n2.replace(',', '.'))

      // verif ya nb1 et nb2
      if ((stor.nb1 !== n1) && (stor.nb2 !== n1)) {
        stor.errParetrou = true
        corrigeFn(1)
        return false
      }
      if (((stor.nb1 !== n2) && (stor.nb2 !== n2)) || ((n1 === n2) && (stor.n1 !== stor.n2))) {
        stor.errParetrou = true
        corrigeFn(2)
        return false
      }

      // verif pour + et - virgule alignées
      if ((stor.lop === '+') || (stor.lop === '-')) {
        if (pv1 !== pv2) {
          stor.errAligneV = true
          corrigeFn(2)
          return false
        }
      }

      // précise pour fois pas la peine aligne si ya
      if (stor.lop === 'x') {
        if ((pv1 === pv2) && (ds.Virgule)) {
          stor.laifois = new BulleAide(stor.tabPo[0][1], 'Inutile d’aligner les virgules <br> pour une multiplication.', { place: 0 })
        }
      }

      // verif plus grand en haut pour -
      if (stor.lop === '-') {
        if (n2 > n1) {
          stor.errMoinsHaut = true
          corrigeFn(2)
          corrigeFn(1)
          return false
        }
      }

      // précise moins de chiffres pour x en bas
      if (stor.lop === 'x') {
        if (nbch1 < nbch2) {
          stor.laifois = new BulleAide(stor.tabPo[0][1], 'L’opération sera moins longue en plaçant <br> la facteur avec le moins de chiffres non nuls en dessous.', { place: 0 })
        }
      }
      // verif pas inverse div et div
      if ((stor.lop === '/') || (stor.lop === ':')) {
        if (n1 === stor.nb2) {
          stor.errInvD = true
          corrigeFn(2)
          corrigeFn(1)
          return false
        }
      }

      // verif pas de virgule pour quotient entier
      if (stor.Lexo.op === ':') {
        if (ret.indexOf(',') !== -1) {
          stor.errVQ = true
          stor.mescase[0][0].corrige(false)
          return false
        }
      }

      // verif bon nb de chiffre pour div déci
      if (stor.Lexo.op === '/') {
        n1 = ret.indexOf(',')
        if (n1 === -1) n1 = ret.length
        n2 = ret.length
        pv1 = n2 - n1 - 1
        if (pv1 !== stor.ardiv) {
          stor.errAproch = true
          stor.mescase[0][0].corrige(false)
          return false
        }
      }
    }
    if (stor.encours === 'c') {
      n1 = n2 = ''
      switch (stor.lop) {
        case '+':
          for (let i = 0; i < stor.casesRep.length; i++) {
            n1 += stor.casesRep[i].reponse()
            if (stor.casesRep[i].reponse() === '') n1 += ' '
          }
          n2 = j3pArrondi(stor.nb1 + stor.nb2, 5)
          while (n1[0] === ' ') {
            n1 = n1.substring(1)
          }
          while (n1[n1.length - 1] === ' ') {
            n1 = n1.substring(0, n1.length - 1)
          }
          nbch1 = parseFloat(n1.replace(',', '.'))
          ok = verifNombreBienEcrit(n1, false, false)
          if (!ok.good) {
            corrigeDonc(stor.casesRep, false)
            stor.errEcrit = true
            stor.LerrEcrit = ok.remede
            return false
          }
          if (nbch1 === n2) return true
          n2 = String(n2).replace('.', ',')
          nbch2 = n2.indexOf(',')
          if (nbch2 === -1) nbch2 = n2.length
          pv1 = n2.substring(0, nbch2)
          pv2 = ''
          if (nbch2 !== n2.length) pv2 = n2.substring(nbch2 + 1)
          while (pv1.length < stor.souvPLus[0]) {
            pv1 = ' ' + pv1
          }
          while (pv2.length < stor.souvPLus[1]) {
            pv2 += ' '
          }
          if (nbch2 !== n2.length) {
            n2 = pv1 + ',' + pv2
          } else { n2 = pv1 }
          for (let i = 0; i < stor.casesRep.length; i++) {
            if (stor.casesRep[i].reponse() !== n2[i]) {
              if ((n2[i] !== undefined) || ((stor.casesRep[i].reponse() !== '0') && (stor.casesRep[i].reponse() !== '') && (n2[i] === undefined))) stor.casesRep[i].corrige(false)
            }
          }
          return false
        case '-':
          for (let i = 0; i < stor.casesRep.length; i++) {
            n1 += stor.casesRep[i].reponse()
            if (stor.casesRep[i].reponse() === '') n1 += ' '
          }
          n2 = j3pArrondi(Math.abs(stor.nb1 - stor.nb2), 5)
          while (n1[0] === ' ') {
            n1 = n1.substring(1)
          }
          while (n1[n1.length - 1] === ' ') {
            n1 = n1.substring(0, n1.length - 1)
          }
          nbch1 = parseFloat(n1.replace(',', '.'))
          ok = verifNombreBienEcrit(n1, false, false)
          if (!ok.good) {
            corrigeDonc(stor.casesRep, false)
            stor.errEcrit = true
            stor.LerrEcrit = ok.remede
            return false
          }
          if (nbch1 === n2) return true
          n2 = String(n2).replace('.', ',')
          nbch2 = n2.indexOf(',')
          if (nbch2 === -1) nbch2 = n2.length
          pv1 = n2.substring(0, nbch2)
          pv2 = ''
          if (nbch2 !== n2.length) pv2 = n2.substring(nbch2 + 1)
          while (pv1.length < stor.souvPLus[0]) {
            pv1 = ' ' + pv1
          }
          while (pv2.length < stor.souvPLus[1]) {
            pv2 += ' '
          }
          if (nbch2 !== n2.length) {
            n2 = pv1 + ',' + pv2
          } else { n2 = pv1 }
          for (let i = n2.length - 1; i > -2; i--) {
            if (stor.casesRep[stor.casesRep.length - n2.length + i].reponse() !== n2[i]) {
              if ((n2[i] !== undefined) || ((stor.casesRep[stor.casesRep.length - n2.length + i].reponse() !== '0') && (stor.casesRep[stor.casesRep.length - n2.length + i].reponse() !== '') && (n2[i] === undefined))) stor.casesRep[stor.casesRep.length - n2.length + i].corrige(false)
            }
          }
          return false
        case 'x':
          if (!ds.Mul_Virg_Seule) {
            n1 = []
            for (let i = 0; i < stor.casesRep.length; i++) {
              n1[i] = ''
              ok = false
              for (let j = 0; j < stor.casesRep[i].length; j++) {
                n2 = stor.casesRep[i][j].reponse()
                if (n2 === '') {
                  if (ok) { n2 = ' ' } else { n2 = '0' }
                } else {
                  ok = true
                }
                n1[i] += n2
              }
              while (n1[i][0] === ' ') {
                n1[i] = n1[i].substring(1)
              }
              if (i !== stor.casesRep.length - 1) {
                // remp les ' ' de la fin par des 0
                pv1 = 0
                for (let j = 0; j < n1[i].length; j++) {
                  if (n1[i][j] !== ' ') pv1 = j
                }
                for (let j = pv1; j < n1[i].length; j++) {
                  if (n1[i][j] === ' ') n1[i] = n1[i].substring(0, j) + '0' + n1[i].substring(j + 1)
                }
              }
            }
            pv2 = []
            nbch1 = 0
            nbch2 = -1
            for (let i = stor.garde.length - 1; i > -1; i--) {
              if (!isNaN(stor.garde[i]) && stor.garde[i] !== '0' && stor.garde[i] !== ' ') {
                nbch2++
                pv2.push(parseInt(stor.garde[i]) * parseInt(stor.garde2) * Math.pow(10, nbch2))
                nbch1 += parseInt(stor.garde[i]) * parseInt(stor.garde2 * Math.pow(10, nbch2))
              } else {
                if (stor.garde[i] === '0') {
                  nbch2++
                }
              }
            }
            pv2.push(nbch1)
            stor.gardeTout = [...pv2]
            ret = []
            for (let i = 0; i < n1.length; i++) {
              ret[i] = String(pv2[i])
              while (ret[i].length < n1[i].length) {
                ret[i] = '0' + ret[i]
              }
              while (n1[i].length < ret[i].length) {
                n1[i] = '0' + n1[i]
              }
            }
            // verif chaque ligne est un nb bien ecrit
            for (let i = 0; i < n1.length; i++) {
              nbch2 = verifNombreBienEcrit(n1[i], false, false)
              if (!nbch2.good) {
                stor.errEcrit = true
                stor.LerrEcrit += 'µ' + nbch2.remede
                corrigeDonc(stor.casesRep[i], false)
              }
            }
            if (stor.errEcrit) {
              stor.LerrEcrit = stor.LerrEcrit.substring(1).replace(/µ/g, '<br>')
              return false
            }
            ok = true
            // verif chaque produit ok
            for (let i = 0; i < n1.length; i++) {
              for (let j = 0; j < n1[i].length; j++) {
                if (ret[i][j] !== n1[i][j]) {
                  if (stor.casesRep[i][j] !== undefined) stor.casesRep[i][j].corrige(false)
                  ok = false
                }
              }
            }
            if (!ok) {
              stor.errCalcul = true
            }
          }

          // verif virgule then
          nbch1 = String(stor.nb1)
          if (nbch1.indexOf('.') === -1) { nbch1 = 0 } else {
            nbch1 = nbch1.length - nbch1.indexOf('.') - 1
          }
          nbch2 = String(stor.nb2)
          if (nbch2.indexOf('.') === -1) { nbch2 = 0 } else {
            nbch2 = nbch2.length - nbch2.indexOf('.') - 1
          }
          stor.gardeV = (nbch1 + nbch2)
          ret = stor.plmax - stor.pl + 1
          if (ret !== (nbch1 + nbch2)) {
            stor.errVfois = true
            coloreVirgule(false)
          }
          return !(stor.errVfois || stor.errCalcul)
        case ':':
          n1 = []
          jkl = String(stor.nb1).substring(String(stor.consNb).length)
          for (let i = 0; i < stor.casesRep.length; i++) {
            n1[i] = ''
            ok = false
            for (let j = 0; j < stor.casesRep[i].length; j++) {
              n2 = stor.casesRep[i][j].reponse()
              if (n2 === '') {
                if (ok) { n2 = ' ' } else { n2 = '0' }
              } else {
                ok = true
              }
              n1[i] += n2
            }
            while (n1[i][0] === ' ') {
              n1[i] = n1[i].substring(1)
            }
            if (i !== stor.casesRep.length - 1) {
              // remp les ' ' de la fin par des 0
              pv1 = 0
              for (let j = 0; j < n1[i].length; j++) {
                if (n1[i][j] !== ' ') pv1 = j
              }
              for (let j = pv1; j < n1[i].length; j++) {
                if (n1[i][j] === ' ') n1[i] = n1[i].substring(0, j) + '0' + n1[i].substring(j + 1)
              }
            }
          }

          // verfi c’est tous des nombres
          for (let i = 0; i < n1.length; i++) {
            nbch2 = verifNombreBienEcrit(n1[i], false, false)
            if (!nbch2.good) {
              stor.errEcrit = true
              stor.LerrEcrit += 'µ' + nbch2.remede
              corrigeDonc(stor.casesRep[i], false)
            }
          }
          if (stor.errEcrit) {
            stor.LerrEcrit = stor.LerrEcrit.substring(1).replace(/µ/g, '<br>')
            return false
          }

          n2 = stor.consNb
          for (let i = 0; i < stor.casesRep.length; i = i + 2) {
            // verif le nb i est dans table du diviseu
            nbch1 = parseInt(n1[i])
            if (nbch1 % stor.nb2 !== 0) {
              stor.errTable = true
              corrigeDonc(stor.casesRep[i], false)
              stor.LerrTable = String(parseInt(n1[i]))
              return false
            }
            // le plus grand en dessous de nb du dessus
            if (n2 - nbch1 >= stor.nb2) {
              stor.errTableTropP = true
              corrigeDonc(stor.casesRep[i], false)
              return false
            }
            if (nbch1 > n2) {
              stor.errTableTropG = true
              corrigeDonc(stor.casesRep[i], false)
              return false
            }
            // verif le reste est bon
            nbch2 = parseInt(n1[i + 1].substring(0, n1[i + 1].length - 1))
            if (i === n1.length - 2) nbch2 = parseInt(n1[i + 1])
            if (n2 - nbch1 !== nbch2) {
              stor.errSous = true
              ret = []
              for (let j = 0; j < stor.casesRep[i + 1].length - 1; j++) {
                ret[j] = stor.casesRep[i + 1][j]
              }
              corrigeDonc(ret, false)
              return false
            }
            // verif le chiffre suivant est bien descendu
            if (i !== n1.length - 2) {
              pv2 = jkl[0]
              if (jkl.length > 1) jkl = jkl.substring(1)
              if (n1[i + 1][n1[i + 1].length - 1] !== pv2) {
                stor.errDesc = true
                stor.casesRep[i + 1][stor.casesRep[i + 1].length - 1].corrige(false)
                return false
              }

              // mets le nouveau nb du dessus
              n2 = parseInt(n1[i + 1])
            }
          }
          n1 = stor.mesCaseQuo.reponse()
          if (parseInt(n1) !== Math.floor(stor.nb1 / stor.nb2)) {
            stor.errQuo = true
            stor.mesCaseQuo.corrige(false)
            return false
          }
          if ((stor.nb2 > 11) && (!stor.fautaff)) {
            stor.errAffta = true
            if (ds.Faux_table) {
              return false
            }
          }
          if ((stor.nb2 < 12) && (stor.fautaff)) {
            stor.errAffta2 = true
          }
          break
        default:
          n1 = []
          jkl = String(stor.len1).substring(String(stor.consNb).length)
          for (let i = 0; i < stor.casesRep.length; i++) {
            n1[i] = ''
            ok = false
            for (let j = 0; j < stor.casesRep[i].length; j++) {
              n2 = stor.casesRep[i][j].reponse()
              if (n2 === '') {
                if (ok) { n2 = ' ' } else { n2 = '0' }
              } else {
                ok = true
              }
              n1[i] += n2
            }
            while (n1[i][0] === ' ') {
              n1[i] = n1[i].substring(1)
            }
            if (i !== stor.casesRep.length - 1) {
              // remp les ' ' de la fin par des 0
              pv1 = 0
              for (let j = 0; j < n1[i].length; j++) {
                if (n1[i][j] !== ' ') pv1 = j
              }
              for (let j = pv1; j < n1[i].length; j++) {
                if (n1[i][j] === ' ') n1[i] = n1[i].substring(0, j) + '0' + n1[i].substring(j + 1)
              }
            }
          }

          // verfi c’est tous des nombres
          for (let i = 0; i < n1.length; i++) {
            nbch2 = verifNombreBienEcrit(n1[i], false, false)
            if (!nbch2.good) {
              stor.errEcrit = true
              stor.LerrEcrit += 'µ' + nbch2.remede
              corrigeDonc(stor.casesRep[i], false)
            }
          }
          if (stor.errEcrit) {
            stor.LerrEcrit = stor.LerrEcrit.substring(1).replace(/µ/g, '<br>')
            return false
          }

          n2 = stor.consNb
          for (let i = 0; i < stor.casesRep.length; i = i + 2) {
            // verif le nb i est dans table du diviseu
            nbch1 = parseInt(n1[i])
            if (nbch1 % stor.nb2 !== 0) {
              stor.errTable = true
              corrigeDonc(stor.casesRep[i], false)
              stor.LerrTable = String(parseInt(n1[i]))
              return false
            }
            // le plus grand en dessous de nb du dessus
            if (n2 - nbch1 >= stor.nb2) {
              stor.errTableTropP = true
              corrigeDonc(stor.casesRep[i], false)
              return false
            }
            if (nbch1 > n2) {
              stor.errTableTropG = true
              corrigeDonc(stor.casesRep[i], false)
              return false
            }
            // verif le reste est bon
            nbch2 = parseInt(n1[i + 1].substring(0, n1[i + 1].length - 1))
            if (i === n1.length - 2) nbch2 = parseInt(n1[i + 1])
            if (n2 - nbch1 !== nbch2) {
              stor.errSous = true
              ret = []
              for (let j = 0; j < stor.casesRep[i + 1].length - 1; j++) {
                ret[j] = stor.casesRep[i + 1][j]
              }
              corrigeDonc(ret, false)
              return false
            }
            // verif le chiffre suivant est bien descendu
            if (i !== n1.length - 2) {
              pv2 = jkl[0]
              if (jkl.length > 1) jkl = jkl.substring(1)
              if (n1[i + 1][n1[i + 1].length - 1] !== pv2) {
                stor.errDesc = true
                stor.casesRep[i + 1][stor.casesRep[i + 1].length - 1].corrige(false)
                return false
              }

              // mets le nouveau nb du dessus
              n2 = parseInt(n1[i + 1])
            }
          }
          n1 = stor.mesCaseQuo.reponse()
          if (parseFloat(n1.replace(',', '.')) !== approxDefaut(stor.nb1 / stor.nb2, stor.ardiv)) {
            stor.errQuo = true
            stor.mesCaseQuo.corrige(false)
            return false
          }
          if ((stor.nb2 > 11) && (!stor.fautaff)) {
            stor.errAffta = true
            if (ds.Faux_table) {
              return false
            }
          }
          if ((stor.nb2 < 12) && (stor.fautaff)) {
            stor.errAffta2 = true
          }
          break
      }
    }
    if (stor.encours === 'e') {
      n1 = stor.lazone1.reponse()
      ok = verifNombreBienEcrit(n1, false, true)
      if (!ok.good) {
        let saute = false
        if (ok.remede === 'Tu dois supprimer tous les zéros inutiles !' && stor.lop === '/') {
          // chope la precision obtenue et la precision demandée
          const prec = chopePrecision(n1)
          // si précision inférieure ou égale à demandée, ca passe
          if (prec <= stor.ardiv) saute = true
        }
        if (!saute) {
          stor.errEcrit = true
          stor.lazone1.corrige(false)
          stor.LerrEcrit = ok.remede
        }
      }
      if (stor.lop === ':') {
        n2 = stor.lazone2.reponse()
        ok = verifNombreBienEcrit(n2, false, true)
        if (!ok.good) {
          stor.errEcrit = true
          stor.lazone2.corrige(false)
          if (stor.LerrEcrit !== '') {
            stor.LerrEcrit = ok.remede
          } else {
            stor.LerrEcrit += '<br>' + ok.remede
          }
        }
      }
      if (stor.errEcrit) return false
      n1 = parseFloat(n1.replace(',', '.'))
      switch (stor.lop) {
        case '+':
          if (n1 !== j3pArrondi(stor.nb1 + stor.nb2, 6)) {
            stor.lazone1.corrige(false)
            return false
          }
          return true
        case '-':
          if (n1 !== j3pArrondi(Math.abs(stor.nb1 - stor.nb2), 6)) {
            stor.lazone1.corrige(false)
            return false
          }
          return true
        case 'x':
          if (n1 !== j3pArrondi(stor.nb1 * stor.nb2, 9)) {
            stor.lazone1.corrige(false)
            return false
          }
          return true
        case '/':
          ok = true
          pv1 = approxDefaut(stor.nb1 / stor.nb2, stor.ardiv)
          pv2 = approxDefaut(stor.nb1 / stor.nb2, stor.ardiv + 3)
          if (pv1 === pv2) { pv2 = '$=$' } else { pv2 = '$\\approx$' }
          if (stor.laliste1.reponse !== pv2) {
            stor.laliste1.corrige(false)
            stor.errSigne = true
            stor.ppkk = pv2
            ok = false
          }
          if (n1 !== pv1) {
            stor.lazone1.corrige(false)
            ok = false
          }
          return ok
        case ':':
          ok = true
          n2 = parseFloat(n2.replace(',', '.'))
          if (n1 !== Math.floor(stor.nb1 / stor.nb2)) {
            stor.lazone1.corrige(false)
            ok = false
          }
          if (n2 !== stor.nb1 % stor.nb2) {
            stor.lazone2.corrige(false)
            ok = false
          }
          if (n2 === Math.floor(stor.nb1 / stor.nb2) && n1 === stor.nb1 % stor.nb2) stor.errInv1 = true
          return ok
      }
    }

    stor.lesdiv.explications.classList.remove('explique')
    stor.lesdiv.solution.classList.remove('correction')

    return true
  }
  function chopePrecision (s) {
    if (s.indexOf(',') === -1) return 0
    return s.length - s.indexOf(',') - 1
  }
  function approxDefaut (n, c) {
    const n2 = Math.floor(j3pArrondi(n * Math.pow(10, c), 9))
    return j3pArrondi(n2 / Math.pow(10, c), c)
  }
  function corrigeDonc (tab, bool) {
    for (let i = 0; i < tab.length; i++) {
      tab[i].corrige(bool)
    }
  }
  function desactiveZone () {
    if (stor.encours === 'p') {
      for (let i = stor.mescase.length - 1; i > -1; i--) {
        for (let j = stor.mescase[i].length - 1; j > -1; j--) {
          stor.mescase[i][j].disable()
        }
      }
      j3pEmpty(stor.TabOp[1][14])
      j3pEmpty(stor.TabOp[2][14])
      j3pEmpty(stor.tabPo[0][2])
    }
    if (stor.encours === 'c') {
      switch (stor.lop) {
        case '+':
          for (let i = 0; i < stor.casesRet.length; i++) {
            stor.casesRet[i].disable()
          }
          for (let i = 0; i < stor.casesRep.length; i++) {
            stor.casesRep[i].disable()
          }
          break
        case '-':
          for (let i = 0; i < stor.casesRep.length; i++) {
            stor.casesRep[i].disable()
          }
          for (let i = 0; i < stor.cptt; i++) {
            stor.aZ[i][0][0].style.cursor = ''
            stor.aZ[i][0][0].removeEventListener('click', onClick)
          }
          break
        case ':':
        case '/':
          stor.mesCaseQuo.disable()
          for (let i = 0; i < stor.casesRep.length; i++) {
            for (let j = 0; j < stor.casesRep[i].length; j++) {
              stor.casesRep[i][j].disable()
            }
          }
          break

        case 'x':
          for (let i = 0; i < stor.casesRep.length; i++) {
            for (let j = 0; j < stor.casesRep[i].length; j++) {
              stor.casesRep[i][j].disable()
            }
          }
          for (let i = 0; i < stor.casesRet.length; i++) {
            stor.casesRet[i].disable()
          }
          j3pEmpty(stor.tabPo[0][2])
          break
      }
    }
    if (stor.encours === 'e') {
      switch (stor.lop) {
        case '+':
        case '-':
        case 'x':
          stor.lazone1.disable()
          return true
        case '/':
          stor.lazone1.disable()
          stor.laliste1.disable()
          return true
        case ':':
          stor.lazone1.disable()
          stor.lazone2.disable()
          return true
      }
    }
  }
  function coloreOk () {
    if (stor.encours === 'p') {
      for (let i = stor.mescase.length - 1; i > -1; i--) {
        for (let j = stor.mescase[i].length - 1; j > -1; j--) {
          stor.mescase[i][j].corrige(true)
        }
      }
    }
    if (stor.encours === 'c') {
      switch (stor.lop) {
        case '+':
          for (let i = 0; i < stor.casesRet.length; i++) {
            stor.casesRet[i].corrige(true)
          }
          for (let i = 0; i < stor.casesRep.length; i++) {
            stor.casesRep[i].corrige(true)
          }
          break
        case '-':
          for (let i = 0; i < stor.casesRep.length; i++) {
            stor.casesRep[i].corrige(true)
          }
          break
        case ':':
        case '/':
          stor.mesCaseQuo.corrige(true)
        // eslint-disable-next-line no-fallthrough
        case 'x':
          for (let i = 0; i < stor.casesRep.length; i++) {
            for (let j = 0; j < stor.casesRep[i].length; j++) {
              stor.casesRep[i][j].corrige(true)
            }
          }

          break
      }
    }
    if (stor.encours === 'e') {
      switch (stor.lop) {
        case '+':
        case '-':
        case 'x':
          stor.lazone1.corrige(true)
          return true
        case '/':
          stor.lazone1.corrige(true)
          stor.laliste1.corrige(true)
          return true
        case ':':
          stor.lazone1.corrige(true)
          stor.lazone2.corrige(true)
          return true
      }
    }
  }
  function corrigeP () {
    for (let i = stor.mescase.length - 1; i > -1; i--) {
      for (let j = stor.mescase[i].length - 1; j > -1; j--) {
        stor.mescase[i][j].corrige(false)
      }
    }
  }
  function corrigeFn (ki) {
    switch (stor.lop) {
      case '+':
      case '-':
      case 'x': {
        const i = ki - 1
        for (let j = 0; j < stor.mescase[i].length; j++) stor.mescase[i][j].corrige(false)
        break
      }
      default: {
        const i = ki - 1
        stor.mescase[0][i].corrige(false)
      }
    }
  }
  function barreLesFaux () {
    if (stor.encours === 'p') {
      for (let i = stor.mescase.length - 1; i > -1; i--) {
        for (let j = stor.mescase[i].length - 1; j > -1; j--) {
          stor.mescase[i][j].barreIfKo()
        }
      }
    }
    if (stor.encours === 'e') {
      switch (stor.lop) {
        case '+':
        case '-':
        case 'x':
          stor.lazone1.barre()
          return true
        case '/':
          stor.lazone1.barreIfKo()
          stor.laliste1.barreIfKo()
          return true
        case ':':
          stor.lazone1.barreIfKo()
          stor.lazone2.barreIfKo()
          return true
      }
    }
  }

  function afficheCorrection (bool) {
    let yaexplik = false
    let buf, buf2
    if (stor.errLOP) {
      stor.compteurPe5++
      yaexplik = true
      j3pAffiche(stor.lesdiv.explications, null, 'Ce n’est pas la bonne opération !')
    }
    if (stor.errParetrou) {
      yaexplik = true
      j3pAffiche(stor.lesdiv.explications, null, 'Recopie correctement les nombres !')
    }
    if (stor.errAligneV) {
      stor.compteurPe1++
      yaexplik = true
      buf = 'Tu dois aligner les chiffres des unités !<br> <i>(tu peux t’aider des virgules)</i><br>'
      if (!ds.Virgule) buf = 'Tu dois aligner les chiffres des unités !<br>'
      j3pAffiche(stor.lesdiv.explications, null, buf)
    }
    if (stor.errMoinsHaut) {
      stor.compteurPe1++
      yaexplik = true
      j3pAffiche(stor.lesdiv.explications, null, 'Tu dois placer le terme le plus grand en haut !\n')
    }
    if (stor.errInvD) {
      stor.compteurPe1++
      yaexplik = true
      j3pAffiche(stor.lesdiv.explications, null, 'Tu inverses dividende et diviseur !\n')
    }
    if (stor.errEcrit) {
      stor.compteurPe6++
      stor.yaexpli = true
      j3pAffiche(stor.lesdiv.explications, null, stor.LerrEcrit)
    }
    if (stor.errCalcul) {
      stor.compteurPe2++
      yaexplik = true
      j3pAffiche(stor.lesdiv.explications, null, 'Erreurs de calcul !\n')
    }
    if (stor.errVfois) {
      stor.compteurPe4++
      yaexplik = true
      j3pAffiche(stor.lesdiv.explications, null, 'Compte le nombre de décimales des deux facteurs !\n')
    }
    if (stor.errTableTropG) {
      stor.compteurPe2++
      yaexplik = true
      j3pAffiche(stor.lesdiv.explications, null, 'Tu as choisi un multiple de $' + String(stor.nb2) + '$ trop grand !\n')
    }
    if (stor.errTableTropP) {
      stor.compteurPe2++
      yaexplik = true
      j3pAffiche(stor.lesdiv.explications, null, 'Tu as choisi un multiple de $' + String(stor.nb2) + '$ trop petit !\n')
    }
    if (stor.errTable) {
      stor.compteurPe2++
      yaexplik = true
      j3pAffiche(stor.lesdiv.explications, null, '$' + stor.LerrTable + '$ n’est pas un multiple de $' + String(stor.nb2) + '$ !\n')
    }
    if (stor.errSous) {
      stor.compteurPe2++
      yaexplik = true
      j3pAffiche(stor.lesdiv.explications, null, 'Erreur dans la soustraction !\n')
    }
    if (stor.errDesc) {
      stor.compteurPe2++
      yaexplik = true
      j3pAffiche(stor.lesdiv.explications, null, 'Tu n’as pas descendu le bon chiffre !\n')
    }
    if (stor.errQuo) {
      stor.compteurPe2++
      yaexplik = true
      j3pAffiche(stor.lesdiv.explications, null, 'Erreur au quotient !\n')
    }
    if (stor.errAffta) {
      yaexplik = true
      j3pAffiche(stor.lesdiv.explications, null, 'Tout le monde ne connait pas la table de $' + stor.nb2 + '$ par coeur !\n')
    }
    if (stor.errAffta2) {
      stor.laitable = new BulleAide(stor.tabPo[0][1], 'Inutile d’afficher cette table. <br> <i>(elle est connue de tous)</i>', { place: 0 })
    }
    if (stor.errSigne) {
      stor.compteurPe3++
      yaexplik = true
      if (stor.ppkk === '=') {
        j3pAffiche(stor.lesdiv.explications, null, 'Il y a égalité quand le reste est nul !\n')
      } else {
        j3pAffiche(stor.lesdiv.explications, null, 'Il n’y a pas égalité quand le reste n’est pas nul !\n')
      }
    }
    if (stor.errVQ) {
      stor.compteurPe1++
      yaexplik = true
      j3pAffiche(stor.lesdiv.explications, null, 'Il n’y a pas de virgule dans une division euclidienne !\n')
    }
    if (stor.errAproch) {
      stor.compteurPe1++
      yaexplik = true
      j3pAffiche(stor.lesdiv.explications, null, 'Vérifie le nombre de chiffres après la virgule au dividende !\n')
    }
    if (stor.errInv1) {
      yaexplik = true
      j3pAffiche(stor.lesdiv.explications, null, 'Tu as inversé le reste et le quotient !\n')
    }

    if (bool) {
      stor.lop = stor.Lexo.op
      barreLesFaux()
      desactiveZone()
      if (stor.encours === 'p') {
        // affiche la co dans 3e case
        afficheBonneOpe('co')
      }
      if (stor.encours === 'c') {
        afficheBonneOpe('co')
        completeOpe()
      }
      if (stor.encours === 'e') {
        let n1 = ecrisBienMathquill(stor.nb1)
        let n2 = ecrisBienMathquill(stor.nb2)
        switch (stor.lop) {
          case '+':
            j3pAffiche(stor.lesdiv.solution, null, '$' + n1 + ' + ' + n2 + ' = ' + String(j3pArrondi(stor.nb1 + stor.nb2, 6)).replace('.', ',') + '$')
            break
          case '-':
            if (stor.nb2 > stor.nb1) {
              buf = n1
              n1 = n2
              n2 = buf
            }
            j3pAffiche(stor.lesdiv.solution, null, '$' + n1 + ' - ' + n2 + ' = ' + String(j3pArrondi(Math.abs(stor.nb1 - stor.nb2), 6)).replace('.', ',') + '$')
            break
          case 'x':
            j3pAffiche(stor.lesdiv.solution, null, '$' + n1 + ' \\times ' + n2 + ' = ' + String(j3pArrondi(stor.nb1 * stor.nb2, 9)).replace('.', ',') + '$')
            break
          case '/': {
            let sd = ' à $0,1$ près'
            if (stor.ardiv === 2) {
              sd = ' à $0,01$ près'
            }
            if (stor.ardiv === 3) {
              sd = ' à $0,001$ près'
            }
            buf = approxDefaut(stor.nb1 / stor.nb2, stor.ardiv)
            buf2 = approxDefaut(stor.nb1 / stor.nb2, stor.ardiv + 3)
            if (buf === buf2) { buf2 = ' = ' } else { buf2 = ' \\approx ' }
            j3pAffiche(stor.lesdiv.solution, null, '$' + n1 + ' \\div ' + n2 + ' ' + buf2 + String(buf).replace('.', ',') + '$ ' + sd)
            break
          }
          case ':':
            if (ds.Parentheses_div) {
              j3pAffiche(stor.lesdiv.solution, null, '$' + n1 + ' = (' + n2 + ' \\times ' + j3pArrondi(Math.floor(stor.nb1 / stor.nb2), 0) + ') + ' + (stor.nb1 % stor.nb2) + '$')
            } else {
              j3pAffiche(stor.lesdiv.solution, null, '$' + n1 + ' = ' + n2 + ' \\times ' + j3pArrondi(Math.floor(stor.nb1 / stor.nb2), 0) + ' + ' + (stor.nb1 % stor.nb2) + '$')
            }
            break
        }
      }
    }
    if (yaexplik) stor.lesdiv.explications.classList.add('explique')
  } // afficheCorrection

  function completeOpe (x) {
    let bufbla = 'blue'
    let dec = 0
    if (x === 'ca') {
      bufbla = 'black'
    }
    if (x === 'ca2') {
      bufbla = 'black'
      dec = 1
    }
    let n1, nv, nd, pe, pd, cpt
    switch (stor.lop) {
      case '+':
        n1 = j3pArrondi(stor.nb1 + stor.nb2, 5)
        n1 = String(n1)
        nv = n1.indexOf('.')
        if (nv === -1) nv = n1.length
        pe = n1.substring(0, nv)
        pd = n1.substring(nv + 1)
        while (pd.length < stor.souvPLus[1]) pd += '0'
        nd = 2
        if (pe.length > stor.souvPLus[0]) nd = 1
        for (let i = 0; i < pe.length; i++) {
          j3pAffiche(stor.TabOp[3][i + nd], null, '$' + pe[i] + '$')
        }
        cpt = pe.length + 1
        if (stor.souvPLus[1] > 0) {
          j3pAffiche(stor.TabOp[3][pe.length + nd], null, '$,$')
          for (let i = 0; i < pd.length; i++) {
            j3pAffiche(stor.TabOp[3][i + nd + cpt], null, '$' + pd[i] + '$')
          }
          cpt += pd.length
        }
        // gere retenues
        stor.TabOp[0][0].parentNode.style.fontSize = '60%'
        for (let i = nd; i < cpt + 1; i++) {
          n1 = parseInt(stor.TabOp[1][i].innerText.replace(/\$/g, ''))
          nv = parseInt(stor.TabOp[2][i].innerText.replace(/\$/g, ''))
          nd = String(parseInt(stor.TabOp[3][i].innerText.replace(/\$/g, '')))
          if (!isNaN(n1)) {
            n1 = String(n1 + nv)
            n1 = n1[n1.length - 1]
            if (n1 !== nd) {
              j3pAffiche(stor.TabOp[0][i], null, '$1$')
            }
          }
        }
        break
      case '-':
        n1 = j3pArrondi(Math.abs(stor.nb1 - stor.nb2), 5)
        n1 = String(n1)
        nv = n1.indexOf('.')
        if (nv === -1) nv = n1.length
        pe = n1.substring(0, nv)
        pd = n1.substring(nv + 1)
        while (pd.length < stor.souvPLus[1]) pd += '0'
        nd = 2
        if (pe.length < stor.souvPLus[0]) pe = '0' + pe
        for (let i = 0; i < pe.length; i++) {
          j3pAffiche(stor.TabOp[3][i + nd], null, '$' + pe[i] + '$')
        }
        cpt = pe.length + 1
        if (stor.souvPLus[1] > 0) {
          j3pAffiche(stor.TabOp[3][pe.length + nd], null, '$,$')
          for (let i = 0; i < pd.length; i++) {
            j3pAffiche(stor.TabOp[3][i + nd + cpt], null, '$' + pd[i] + '$')
          }
          cpt += pd.length
        }
        // gere retenues
        pe = 0
        stor.TabOp[0][0].parentNode.style.fontSize = '60%'
        for (let i = 2; i < cpt + 1; i++) {
          nd = String(parseInt(stor.aZ[i - 2 + pe][0][1].innerText.replace(/\$/g, '')))
          nv = parseInt(stor.aZ[i - 2 + pe][0][1].innerText.replace(/\$/g, ''))
          n1 = parseInt(stor.TabOp[3][i].innerText.replace(/\$/g, ''))
          if (!isNaN(n1)) {
            n1 = String(n1 + nv)
            n1 = n1[n1.length - 1]
            if (n1 !== nd) {
              // @todo nommer ça avec qqchose d’intelligible
              if (stor.aZ[i - 1 + pe]) {
                const asderf = stor.aZ[i - 1 + pe][0][0]
                j3pAffiche(asderf, null, '$1$')
                asderf.style.fontSize = '50%'
              }
              if (stor.asO[i - 2 + pe]) {
                const asdorf = stor.asO[i - 2 + pe][0][0]
                j3pAffiche(asdorf, null, '$1$')
                asdorf.style.fontSize = '50%'
              }
            }
          } else {
            pe = -1
          }
        }
        break
      case 'x':
        // afiche les +
        // affiche le rez
        for (let i = 0; i < stor.nb + 1; i++) {
          nv = String(stor.gardeTout[i])
          cpt = nv.length + 1
          for (let j = nv.length - 1; j > -1; j--) {
            pd = ''
            cpt--
            if ((i === stor.nb) && (cpt === stor.gardeV)) {
              if (x !== 'ca2') pd = ','
            }
            if (x === 'ca2' && i !== stor.nb) {
              j3pEmpty(stor.TabOp[3 + dec + i][stor.bout - j])
            }
            if (x !== 'ca2' || i !== stor.nb) {
              j3pAffiche(stor.TabOp[3 + dec + i][stor.bout - j], null, '$' + pd + nv[nv.length - 1 - j] + '$')
            }
            if (i === stor.nb - 1) {
              stor.TabOp[3 + dec + i][stor.bout - j + 1].style.borderBottom = 'solid 1px ' + bufbla
            }
          }
          if (i === stor.nb - 1) {
            stor.TabOp[3 + dec + i][stor.bout - nv.length + 1].style.borderBottom = 'solid 1px ' + bufbla
            if (x !== 'ca2') {
              j3pAffiche(stor.TabOp[3 + dec + i][0], null, '$+$')
            }
            stor.TabOp[3 + dec + i][stor.bout + 2].style.borderBottom = 'solid 1px ' + bufbla
          }
        }
        if (x === 'ca2') {
          nv = String(stor.gardeTout[stor.gardeTout.length - 1])
          for (let j = nv.length - 1; j > -1; j--) {
            j3pEmpty(stor.tabVirg[stor.plmax - j][0][1])
            j3pAffiche(stor.tabVirg[stor.plmax - j][0][1], null, '$' + pd + nv[nv.length - 1 - j] + '$')
          }
        }
        break
      case ':':
      case '/': {
        // stor.consNb (le nb avec arc au début)
        // stor.cpt (le nb de chiffres à descendre)
        pe = stor.consNb
        stor.cons2b = stor.cons2
        for (let i = 0; i < stor.cpt + 1; i++) {
          nv = j3pArrondi(Math.floor(pe / stor.nb2) * stor.nb2, 0)
          pe = pe - nv
          nv = String(nv)
          pd = String(pe)
          cpt = stor.cons2b[0]
          stor.cons2b = stor.cons2b.substring(1)
          for (let j = 0; j < nv.length; j++) {
            j3pAffiche(stor.TabOp[i * 2 + 1][5 - stor.cpt - j + i], null, '$' + nv[nv.length - 1 - j] + '$')
            stor.TabOp[i * 2 + 1][5 - stor.cpt - j + i].style.borderBottom = '1px solid ' + bufbla
          }
          const yIndex = 5 - stor.cpt - nv.length + i
          const ct = stor.TabOp[i * 2 + 1][yIndex]
          j3pAffiche(ct, null, '$-$')
          ct.style.borderBottom = '1px solid ' + bufbla
          for (let j = 0; j < pd.length; j++) {
            j3pAffiche(stor.TabOp[i * 2 + 2][5 - stor.cpt - j + i], null, '$' + pd[pd.length - 1 - j] + '$')
          }
          if (i !== stor.cpt) {
            j3pAffiche(stor.TabOp[i * 2 + 2][6 - stor.cpt + i], null, '$' + cpt + '$')
          }
          pe = parseInt(pd + cpt)
        }
        if (stor.Lexo.op === ':') stor.ardiv = 0
        pe = String(approxDefaut(stor.nb1 / stor.nb2, stor.ardiv)).replace('.', ',')
        pd = pe.indexOf(',')
        if (pd === -1) {
          cpt = 0
        } else {
          cpt = pe.length - 1 - pd
        }
        let fefe = false
        while (cpt < stor.ardiv) {
          if ((cpt === 0) && (!fefe)) {
            pe += ','
            fefe = true
          } else {
            pe += '0'
            cpt++
          }
        }
        j3pAffiche(stor.TabOp[1][6], null, '$' + pe + '$')
      }
    }
  }

  function suivEtape () {
    let i = stor.afaire.indexOf(stor.encours) + 1
    if (i > stor.afaire.length - 1) i = 0
    return stor.afaire[i]
  }
  function enonceMain () {
    stor.encours = suivEtape()
    if (stor.laifois !== undefined) {
      stor.laifois.disable()
      stor.laifois = undefined
    }
    if (stor.laitable !== undefined) {
      stor.laitable.disable()
      stor.laitable = undefined
    }
    if (stor.encours === stor.afaire[0]) {
      me.videLesZones()
      creeLesDiv()
      stor.Lexo = faisChoixExos()
      stor.lesdiv.travail.classList.add('travail')
      stor.lesdiv.consigne.classList.add('enonce')
      stor.lesdiv.explications.classList.remove('explique')
      stor.lesdiv.solution.classList.remove('correction')
      me.zonesElts.MG.classList.add('fond')
    }
    poseQuestion()
    me.finEnonce()
  }

  switch (this.etat) {
    case 'enonce':
      // code exécuté au lancement de la section
      if (me.debutDeLaSection) {
        initSection()
      } else {
        enonceMain()
      }
      break // case "enonce":

    case 'correction':
      // On teste si une réponse a été saisie
      stor.lesdiv.explications.innerHTML = ''
      stor.lesdiv.correction.innerHTML = ''

      if (!yaReponse()) {
        // pas de réponse
        stor.lesdiv.correction.style.color = this.styles.cfaux
        stor.lesdiv.correction.innerHTML = reponseIncomplete
        return this.finCorrection() // on reste dans l’état correction
      }

      if (isRepOk()) {
        // Bonne réponse
        stor.lesdiv.correction.style.color = this.styles.cbien
        stor.lesdiv.correction.innerHTML = cBien
        this.score++
        coloreOk()
        desactiveZone()

        this.typederreurs[0]++
        return this.finCorrection('navigation', true)
      }

      // Pas de bonne réponse
      stor.lesdiv.correction.style.color = this.styles.cfaux
      stor.lesdiv.correction.innerHTML = cFaux
      if (this.isElapsed) {
        // A cause de la limite de temps :
        stor.lesdiv.correction.innerHTML += tempsDepasse + '<BR>'
        this.typederreurs[10]++
        afficheCorrection(true)
        return this.finCorrection('navigation', true)
      }

      // Réponse fausse sans limite de temps atteinte
      if (me.essaiCourant < me.donneesSection.nbchances) {
        // il reste des essais
        stor.lesdiv.correction.innerHTML += '<br>' + essaieEncore
        afficheCorrection(false)
        this.typederreurs[1]++
        return this.finCorrection() // on reste dans l’état correction
      }

      // Erreur au dernier essai
      afficheCorrection(true)
      this.typederreurs[2]++
      this.finCorrection('navigation', true)
      break // case "correction":

    case 'navigation':
      if (this.sectionTerminee()) {
        this.parcours.pe = getPe(ds, stor)
      } else {
        this.etat = 'enonce'
      }
      this.finNavigation()
      break // case "navigation":
  }
}
