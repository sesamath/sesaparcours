import { j3pAddContent, j3pAddElt, j3pArrondi, j3pGetRandomInt } from 'src/legacy/core/functions'
import ListeDeroulante from 'src/legacy/outils/listeDeroulante/ListeDeroulante'
import { ecrisBienMathquill } from 'src/legacy/outils/zoneStyleMathquill/functions'
import ZoneStyleMathquill1 from 'src/legacy/outils/zoneStyleMathquill/ZoneStyleMathquill1'
import { addDefaultTable } from 'src/legacy/themes/table'
import { j3pAffiche } from 'src/lib/mathquill/functions'
import textesGeneriques from 'src/lib/core/textes'

const { cBien, cFaux, essaieEncore, regardeCorrection, tempsDepasse } = textesGeneriques

/**
 * Ajouter une description ici
 * @fileOverview
 * @author Tommy Barroy <t.barroy@laposte.net>
 * @since 2017-10
*/

export const params = {
  outils: ['calculatrice', 'mathquill'],
  /**
   * Ce qui est paramétrable dans l’éditeur de graphe
   * @type {LegacySectionParamsParametres}
   */
  parametres: [
    ['nbrepetitions', 9, 'entier', 'Nombre de répétitions de la section'],
    ['limite', 0, 'entier', 'Temps disponible par répétition, en secondes'],
    ['nbchances', 2, 'entier', 'Nombre d’essais par répétition'],
    ['Somme', true, 'boolean', '<u>true</u>: Calcul de l’ordre de grandeur d’une somme. '],
    ['Difference', true, 'boolean', '<u>true</u>: Calcul de l’ordre de grandeur d’une différence. '],
    ['Produit', true, 'boolean', '<u>true</u>: Calcul de l’ordre de grandeur d’un produit. '],
    ['Quotient', true, 'boolean', '<u>true</u>: Calcul de l’ordre de grandeur d’un quotient. '],
    ['Entier', false, 'boolean', '<u>true</u>: L’exercice ne propose que des nombres entiers '],
    ['Espace', false, 'boolean', '<u>true</u>: Les espaces entre chaque groupe de 3 sont exigés.'],
    // le modèle gère la compatibilité ascendante avec l’ancien paramètre MiseEnPagePlusClaire
    ['theme', 'standard', 'liste', 'Modèle pour l’affichage', ['standard', 'zonesAvecImageDeFond']]
  ]
}

const textes = {
  reponseIncomplete: 'Réponse incomplète !',
  consigne1: 'Donne un ordre de grandeur ',
  consigne2: ['', 'de la somme:', 'de la différence:', 'du produit:', 'du quotient:'],
  op: ['....', '$ + $', '$ - $', '$ \\times $', '$ ÷ $'],
  expliqueComplique1: 'Un ordre de grandeur doit être un nombre simple.',
  expliqueComplique2: '$£b$ a trop de chiffres différents de $0$.',
  expliqueApprox: 'Il y a d’autres nombres simples plus proches de $£b$ .',
  expliqueOp: ['L’ordre de grandeur d’une somme est égal à la somme des ordres de grandeur des termes.', 'L’ordre de grandeur d’une différence est égal à la différence des ordres de grandeur des termes.', 'L’ordre de grandeur d’un produit est égal au produit des ordres de grandeur des facteurs.', 'L’ordre de grandeur d’un quotient est égal au quotient des ordres de grandeur du dividende et du diviseur.'],
  expliqueOrdre: '$£o$ n’est pas égal à $£a$ £i $£b$ !',
  expliqueCorrige1: '$£a$ £b $£c$ est proche de $£d$ £b $£e$',
  expliqueCorrige2: 'donc $£a$ £b $£c$ est proche $£f$ .',
  reponseDebut: '$£a$ £b $£c$ est proche de &nbsp;',
  reponseFin: 'donc $£a$ £b $£c$ est proche de &nbsp;'
}

/**
 * section ordredegrandeur
 * @this {Parcours}
 */
export default function main () {
  const me = this
  const stor = me.storage
  const ds = me.donneesSection

  function mul (int) {
    switch (int) {
      case 9 :
        return 1
      case 8:
      case 7:
      case 6:
        return 2
      case 5:
        return 3
      case 4 :
        return 4
      case 3:
        return 5
      case 2 :
        return 8
      default :
        return 1
    }
  }
  function yareponse () {
    if (me.isElapsed) return true
    if (stor.zsm1.reponse() === '') {
      stor.zsm1.focus()
      return false
    }
    if (!stor.listesDeroulantes[0].changed) {
      stor.listesDeroulantes[0].focus()
      return false
    }
    if (stor.zsm2.reponse() === '') {
      stor.zsm2.focus()
      return false
    }
    if (stor.zsm3.reponse() === '') {
      stor.zsm3.focus()
      return false
    }

    return true
  }
  function ArrondSpe (nb, maxDecimales) {
    if (maxDecimales >= 0) {
      return Number(Number(nb).toFixed(maxDecimales))
    }
    return j3pArrondi(Math.round(nb / Math.pow(10, -maxDecimales)) * Math.pow(10, -maxDecimales), 0)
  }

  function bonneReponse () {
    // cette fonction renvoie true ou false si du moins une réponse a été saisie
    stor.ErrPlusieursVirgule = false
    stor.ErrVirguleSeule = false
    stor.ErrEspace = false
    stor.ErrDecimaleEspace = false
    stor.ErrZeroInutile = false
    stor.troploina = false
    stor.complika = false

    stor.troploinb = false
    stor.complikb = false
    stor.ok1 = true
    stor.ok2 = true
    stor.ok3 = true

    let ok = true
    if (!testEcr(stor.zsm1.reponse())) {
      ok = false
      stor.ok1 = false
    }
    if (!testEcr(stor.zsm2.reponse())) {
      ok = false
      stor.ok2 = false
    }
    if (!testEcr(stor.zsm3.reponse())) {
      ok = false
      stor.ok3 = false
    }
    if (!ok) return false

    // eslint-disable-next-line no-irregular-whitespace
    stor.apa = parseFloat(stor.zsm1.reponse().replace(',', '.').replace(/ /g, ''))
    // eslint-disable-next-line no-irregular-whitespace
    stor.apb = parseFloat(stor.zsm2.reponse().replace(',', '.').replace(/ /g, ''))
    // eslint-disable-next-line no-irregular-whitespace
    stor.ordre = parseFloat(stor.zsm3.reponse().replace(',', '.').replace(/ /g, ''))
    stor.opeleve = stor.listesDeroulantes[0].reponse

    // test ope
    stor.errop = (stor.op !== stor.listesDeroulantes[0].givenChoices.indexOf(stor.opeleve))

    // test apa
    if (stor.apa === stor.nombre1odg) { stor.ok1 = true } else {
      /// gere erreur apa
      stor.ok1 = false
      const bufa = '' + stor.apa
      let oka = true
      const bufas2 = bufa.replace('.', '')
      const bufas = bufas2.replace(/0/g, '')
      if (bufas.length > 2) { oka = false } else {
        if (bufas.length === 2) {
          const pos1 = bufas2.indexOf(bufas[0])
          const pos2 = bufas2.indexOf(bufas[1])
          if ((pos2 - pos1) > 1) { oka = false }
        }
      }
      if (!oka) { stor.complika = true }
    }

    // test apb
    if (stor.apb === stor.nombre2odg) { stor.ok2 = true } else {
      /// gere erreur apa
      stor.ok2 = false
      const bufa = '' + stor.apb
      let oka = true
      const bufas2 = bufa.replace('.', '')
      const bufas = bufas2.replace(/0/g, '')
      if (bufas.length > 2) { oka = false } else {
        if (bufas.length === 2) {
          const pos1 = bufas2.indexOf(bufas[0])
          const pos2 = bufas2.indexOf(bufas[1])
          if ((pos2 - pos1) > 1) oka = false
        }
      }
      if (!oka) stor.complikb = true
    }

    // test ordre
    switch (stor.listesDeroulantes[0].givenChoices.indexOf(stor.opeleve)) {
      case 1:
        stor.ordrebon = (stor.apa + stor.apb)
        break
      case 2:
        stor.ordrebon = (stor.apa - stor.apb)
        break
      case 3:
        stor.ordrebon = (stor.apa * stor.apb)
        break
      case 4:
        stor.ordrebon = (stor.apa / stor.apb)
        break
    }

    stor.errordre = !(Math.abs(stor.ordre - stor.ordrebon) < Math.pow(10, -12))
    stor.errordre2 = !(Math.abs(stor.ordre - stor.zordre) < Math.pow(10, -12))

    return (stor.ok1) && (stor.ok2) && (!stor.errordre2) && (!stor.errop)
  }
  function testEcr (ecr) {
    // test si y’a bien qu’une virgule
    if (ecr.indexOf(',') !== ecr.lastIndexOf(',')) {
      stor.ErrPlusieursVirgule = true
      return false
    }
    // test si les espaces sont pas strange
    // on tolere plusieurs espaces entre 2 grp
    // à partir de virgule, ou a partir de la droite
    // ca doit etre des groupes de 3 (sauf le dernier)
    // ou alors tout colle et espace a osef
    let partieentiereEncours, yavirguleEncours, partiedecimaleEncours
    if (ecr.indexOf(',') !== -1) {
      partieentiereEncours = ecr.substring(0, ecr.indexOf(','))
      yavirguleEncours = true
      partiedecimaleEncours = ecr.substring(ecr.indexOf(',') + 1)
    } else {
      partieentiereEncours = ecr
      yavirguleEncours = false
      partiedecimaleEncours = ''
    }
    // test si les espaces sont pas strange
    // à partir de virgule, ou a partir de la droite
    // ca doit etre des groupes de 3 (sauf le dernier)
    // ou alors tout colle et espace a osef
    if ((yavirguleEncours) && (partiedecimaleEncours.replace(/ /g, '').length === 0)) {
      stor.ErrVirguleSeule = true
      return false
    }

    let yaesp = true
    let comptgrp = 0
    let comptgrpold = 3
    let ing = false
    if (!yavirguleEncours) {
      while (partieentiereEncours[partieentiereEncours.length - 1] === ' ') {
        partieentiereEncours = partieentiereEncours.substring(0, partieentiereEncours.length - 1)
      }
    }
    for (let i = partieentiereEncours.length - 1; i > -1; i--) {
      if (partieentiereEncours[i] === ' ') {
        if (yaesp) {
          if (i === 0) {
            stor.ErrespVirg = true
            return false
          } else {
            stor.Err2esp = true
            return false
          }
        }
        yaesp = true
        if (ing) comptgrpold = comptgrp
        ing = false
        comptgrp = 0
      } else {
        yaesp = false
        comptgrp++
        ing = true
        if ((comptgrpold !== 3) || ((comptgrp > 3) && (ds.Espace))) {
          stor.ErrEspace = true
          return false
        }
      }
    }

    yaesp = true
    comptgrp = 0
    comptgrpold = 3
    ing = false
    for (let i = 0; i < partiedecimaleEncours.length; i++) {
      if (partiedecimaleEncours[i] === ' ') {
        if (yaesp) {
          if (i === 0) {
            stor.ErrespVirg = true
            return false
          } else {
            stor.Err2esp = true
            return false
          }
        }
        yaesp = true
        if (ing) comptgrpold = comptgrp
        ing = false
        comptgrp = 0
      } else {
        yaesp = false
        comptgrp++
        ing = true
        if ((comptgrpold !== 3) || ((comptgrp > 3) && (ds.Espace))) {
          stor.ErrDecimaleEspace = true
          return false
        }
      }
    }
    // test si ya oublié zero inut
    //
    for (let i = 0; i < partieentiereEncours.length; i++) {
      if (partieentiereEncours[i] !== ' ') {
        if (partieentiereEncours[i] === '0') {
          if (partieentiereEncours.length !== 1) {
            stor.ErrZeroInutile = true
            return false
          }
        } else { break }
      }
    }
    for (let i = partiedecimaleEncours.length - 1; i > -1; i--) {
      if (partiedecimaleEncours[i] !== ' ') {
        if (partiedecimaleEncours[i] === '0') {
          stor.ErrZeroInutile = true
          return false
        } else { break }
      }
    }
    return true
  }
  function initSection () {
    stor.opPossibles = []
    if (ds.Somme) stor.opPossibles.push(1)
    if (ds.Difference) stor.opPossibles.push(2)
    if (ds.Produit) stor.opPossibles.push(3)
    if (ds.Quotient) stor.opPossibles.push(4)
    if (stor.opPossibles.length === 0) stor.opPossibles.push(0)

    // Construction de la page
    me.construitStructurePage('presentation1bis')

    // DECLARATION DES VARIABLES
    // reponse attendue

    // contient un tab reponse [nb,bool=true si rep est diviseur,bool=true si vient d’un calcul]

    /*
     Par convention,`
    `   me.typederreurs[0] = nombre de bonnes réponses
        me.typederreurs[1] = nombre de fois où l’élève a utilisé une chance supplémentaire
        me.typederreurs[2] = nombre de mauvaises réponses
        me.typederreurs[10] = nombre de fois où l’élève a eu faux à cause d’un temps limite
        LES AUTRES peuvent être utilisées par la détermination de la pe dans le cas d’une section qualitative
     */
    me.typederreurs = [0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0]
    me.afficheTitre('Déterminer un ordre de grandeur')
    enonceMain()
  }
  function enonceMain () {
    me.videLesZones()
    ds.bloque = false
    /** @type {ListeDeroulante[]} */
    stor.listesDeroulantes = []

    stor.lesdiv = {}
    stor.lesdiv.conteneur = j3pAddElt(me.zonesElts.MG, 'div', {
      style: me.styles.etendre('toutpetit.enonce', { padding: '10px' })
    })
    stor.lesdiv.consigne1 = addDefaultTable(stor.lesdiv.conteneur, 1, 1)[0][0]
    stor.lesdiv.zoneRep = addDefaultTable(stor.lesdiv.conteneur, 1, 1)[0][0]
    const tt1 = addDefaultTable(stor.lesdiv.zoneRep, 1, 6)
    const tt2 = addDefaultTable(stor.lesdiv.zoneRep, 1, 2)
    stor.lesdiv.correction = j3pAddElt(me.zonesElts.MD, 'div', {
      style: me.styles.etendre('toutpetit.enonce', { padding: '10px' })
    })
    stor.lesdiv.explik2 = j3pAddElt(stor.lesdiv.conteneur, 'div')
    stor.lesdiv.explik1 = j3pAddElt(stor.lesdiv.conteneur, 'div')
    stor.lesdiv.explik2.style.color = me.styles.cfaux
    stor.lesdiv.explik1.style.color = me.styles.petit.correction.color
    me.zonesElts.MG.classList.add('fond')
    stor.lesdiv.zoneRep.classList.add('travail')
    stor.lesdiv.consigne1.classList.add('enonce')

    stor.op = stor.opPossibles[j3pGetRandomInt(0, (stor.opPossibles.length - 1))]

    // stor.op = 4;
    // .op 1 add 2 sou 3 fois 4 div
    stor.dec1 = j3pGetRandomInt(0, 4)
    stor.si1 = Math.pow(-1, j3pGetRandomInt(0, 1))
    if (ds.Entier) {
      stor.si1 = 1
      stor.dec1 = j3pGetRandomInt(2, 4)
    }
    stor.dec2 = stor.dec1
    stor.si2 = stor.si1
    if (stor.op > 2) {
      stor.si2 = 1
      stor.dec2 = 0
    }
    const si2 = Math.pow(-1, j3pGetRandomInt(0, 1))
    const deca = si2 * j3pGetRandomInt(11, 49) * Math.pow(10, stor.si1 * stor.dec1 - 3)
    const si3 = Math.pow(-1, j3pGetRandomInt(0, 1))
    const deca2 = si3 * j3pGetRandomInt(11, 49) * Math.pow(10, stor.si2 * stor.dec2 - 3)

    if (stor.op > 2) {
      stor.nombre2base = j3pGetRandomInt(2, 9)
    } else {
      stor.nombre2base = j3pGetRandomInt(2, 8) * 10
    }
    if (stor.op === 4) {
      stor.nombre1base = stor.nombre2base * 5 * mul(stor.nombre2base)
    } else {
      stor.nombre1base = j3pGetRandomInt(2, 8) * 10
    }
    if ((stor.op === 2) && (stor.nombre2base > stor.nombre1base)) {
      const u = stor.nombre2base
      stor.nombre2base = stor.nombre1base
      stor.nombre1base = u
    }
    // FIXME, maxDecimales est négatif !
    stor.nombre1 = ArrondSpe(stor.nombre1base * Math.pow(10, stor.si1 * stor.dec1) + deca, -stor.si1 * stor.dec1 + 3)
    stor.nombre2 = ArrondSpe(stor.nombre2base * Math.pow(10, stor.si2 * stor.dec2) + deca2, -stor.si2 * stor.dec2 + 3)
    stor.nombre1odg = ArrondSpe(stor.nombre1base * Math.pow(10, stor.si1 * stor.dec1), -stor.si1 * stor.dec1)
    stor.nombre2odg = ArrondSpe(stor.nombre2base * Math.pow(10, stor.si2 * stor.dec2), -stor.si2 * stor.dec2)

    if (ds.Entier) {
      stor.nombre2 = Math.round(stor.nombre2)
      stor.nombre1 = Math.round(stor.nombre1)
    }
    switch (stor.op) {
      case 1:
        stor.zordre = ArrondSpe(stor.nombre1odg + stor.nombre2odg, -stor.si1 * stor.dec1 + 3)
        break
      case 2:
        stor.zordre = ArrondSpe(stor.nombre1odg - stor.nombre2odg, -stor.si1 * stor.dec1 + 3)
        break
      case 3:
        stor.zordre = ArrondSpe(stor.nombre1odg * stor.nombre2odg, -stor.si1 * stor.dec1 + 3 + 1)
        break
      case 4:
        stor.zordre = ArrondSpe(stor.nombre1odg / stor.nombre2odg, -stor.si1 * stor.dec1 + 3 + 1)
        break
    }

    const maphrase = '<b><u>' + textes.consigne1 + textes.consigne2[stor.op] + '</u></b> ' + ' $£a $' + textes.op[stor.op] + '$ £b$'

    j3pAffiche(stor.lesdiv.consigne1, null, maphrase + '\n\n',
      {
        a: ecrisBienMathquill(stor.nombre1),
        b: ecrisBienMathquill(stor.nombre2)
      })

    j3pAffiche(tt1[0][0], null, textes.reponseDebut,
      {
        a: ecrisBienMathquill(stor.nombre1),
        c: ecrisBienMathquill(stor.nombre2),
        b: textes.op[stor.op]
      })
    stor.zsm1 = new ZoneStyleMathquill1(tt1[0][1], {
      restric: '0123456789,. ',
      limitenb: 9,
      limite: 15,
      enter: me.sectionCourante.bind(me)
    })
    j3pAddContent(tt1[0][2], '&nbsp;')
    stor.listesDeroulantes.push(ListeDeroulante.create(tt1[0][3], textes.op, {
      mathquill: 'oui',
      alignmathquill: true,
      centre: true,
      sansFleche: true
    }))
    j3pAddContent(tt1[0][4], '&nbsp;')
    stor.zsm2 = new ZoneStyleMathquill1(tt1[0][5], {
      restric: '0123456789,. ',
      limitenb: 9,
      limite: 15,
      enter: me.sectionCourante.bind(me)
    })

    j3pAffiche(tt2[0][0], null, textes.reponseFin,
      {
        a: ecrisBienMathquill(stor.nombre1),
        c: ecrisBienMathquill(stor.nombre2),
        b: textes.op[stor.op]
      })
    stor.zsm3 = new ZoneStyleMathquill1(tt2[0][1], {
      restric: '0123456789,. ',
      limitenb: 9,
      limite: 15,
      enter: me.sectionCourante.bind(me)
    })
    me.afficheBoutonValider()

    ///
    me.finEnonce()
  }

  switch (this.etat) {
    case 'enonce':

      if (this.debutDeLaSection) {
        initSection()
      } else {
        enonceMain()
      }
      break // case "enonce":

    case 'correction':
      stor.lesdiv.explik2.classList.remove('explique')
      // On teste si une réponse a été saisie
      if (!yareponse()) {
        stor.lesdiv.correction.style.color = this.styles.cfaux
        stor.lesdiv.correction.innerHTML = textes.reponseIncomplete
        this.afficheBoutonValider()
      } else {
        // Bonne réponse
        if (bonneReponse()) {
          this._stopTimer()
          this.score += ArrondSpe(1 / this.donneesSection.nbetapes, 1)
          stor.lesdiv.correction.style.color = this.styles.cbien
          stor.lesdiv.correction.innerHTML = cBien
          stor.lesdiv.explik1.innerHTML = ''
          stor.listesDeroulantes[0].corrige(true)
          stor.listesDeroulantes[0].disable(false)
          stor.listesDeroulantes.pop()
          stor.zsm1.corrige(true)
          stor.zsm1.disable()
          stor.zsm2.corrige(true)
          stor.zsm2.disable()
          stor.zsm3.corrige(true)
          stor.zsm3.disable()

          this.typederreurs[0]++
          this.cacheBoutonValider()
          this.etat = 'navigation'
          this.sectionCourante()
        } else {
          // Pas de bonne réponse
          let yakt = true
          stor.lesdiv.correction.style.color = this.styles.cfaux
          stor.lesdiv.correction.innerHTML = cFaux
          stor.lesdiv.explik2.innerHTML = ''
          if (stor.ErrPlusieursVirgule) {
            j3pAffiche(stor.lesdiv.explik2, null, 'Il ne peut y avoir plusieurs virgules dans un nombre !\n')
            yakt = false
          }
          if (stor.ErrVirguleSeule) {
            j3pAffiche(stor.lesdiv.explik2, null, 'Il ne peut pas y avoir de virgule sans partie décimale !\n')
            yakt = false
          }
          if (stor.ErrEspace) {
            yakt = false
            j3pAffiche(stor.lesdiv.explik2, null, 'Dans la partie entière, les chiffres doivent être regroupés par 3 en commençant par la droite !\n')
          }
          if (stor.ErrDecimaleEspace) {
            yakt = false
            j3pAffiche(stor.lesdiv.explik2, null, 'Dans la partie décimale, les chiffres doivent être regroupés par 3 en commençant par la gauche !\n')
          }
          if (stor.ErrZeroInutile) {
            yakt = false
            j3pAffiche(stor.lesdiv.explik2, null, 'Supprime les zéros inutiles !\n')
          }
          if (yakt) {
            stor.zsm1.corrige(true)
            stor.zsm2.corrige(true)
          }

          // A cause de la limite de temps :
          if (this.isElapsed) { // limite de temps
            this._stopTimer()

            stor.lesdiv.correction.innerHTML = tempsDepasse
            this.typederreurs[10]++
            this.cacheBoutonValider()

            this._stopTimer()
            this.cacheBoutonValider()

            stor.lesdiv.correction.innerHTML += '<br>' + regardeCorrection

            stor.listesDeroulantes[0].corrige(!stor.errop)
            stor.listesDeroulantes[0].disable(stor.errop)
            stor.listesDeroulantes.pop()
            stor.zsm3.disable()
            stor.zsm2.disable()
            stor.zsm1.disable()
            if (!stor.ok1) {
              stor.zsm1.corrige(false)
              stor.zsm1.barre()
            }
            if (!stor.ok2) {
              stor.zsm2.corrige(false)
              stor.zsm2.barre()
            }
            if (stor.errordre) {
              stor.zsm3.corrige(false)
              stor.zsm3.barre()
            }

            j3pAffiche(stor.lesidv.explik2, null, '<b>' + textes.expliqueCorrige1 + '\n' + textes.expliqueCorrige2 + '</b>',
              {
                a: ecrisBienMathquill(stor.nombre1),
                b: textes.op[stor.op],
                c: ecrisBienMathquill(stor.nombre2),
                d: ecrisBienMathquill(stor.nombre1odg),
                e: ecrisBienMathquill(stor.nombre2odg),
                f: ecrisBienMathquill(stor.zordre)
              })
            stor.lesdiv.explik2.classList.add('corretion')

            this.etat = 'navigation'
            this.sectionCourante()
          } else { // Réponse fausse :
            if (this.essaiCourant < this.donneesSection.nbchances) {
              stor.lesdiv.correction.innerHTML += '<br>' + essaieEncore

              if (!stor.ok1) {
                stor.zsm1.corrige(false)
                // indication pour a
                if (stor.complika) {
                  j3pAffiche(stor.lesdiv.explik2, null, textes.expliqueComplique1 + '\n' + textes.expliqueComplique2 + '\n',
                    {
                      b: stor.apa
                    })
                } else if (yakt) {
                  j3pAffiche(stor.lesdiv.explik2, null, textes.expliqueApprox + '\n',
                    {
                      b: stor.nombre1
                    })
                }
              }

              if (!stor.ok2) {
                stor.zsm2.corrige(false)
                // indication pour a
                if (stor.complikb) {
                  j3pAffiche(stor.lesdiv.explik2, null, textes.expliqueComplique1 + '\n' + textes.expliqueComplique2 + '\n',
                    {
                      b: stor.apb
                    })
                } else if (yakt) {
                  j3pAffiche(stor.lesdiv.explik2, null, textes.expliqueApprox + '\n',
                    {
                      b: stor.nombre2
                    })
                }
              }
              if (stor.errordre2) {
                stor.zsm3.corrige(false)
                if (stor.errordre) {
                  j3pAffiche(stor.lesdiv.explik2, null, textes.expliqueOrdre + '\n',
                    {
                      a: stor.apa,
                      b: stor.apb,
                      o: stor.ordre,
                      i: stor.opeleve
                    })
                }
              }

              stor.listesDeroulantes[0].corrige(!stor.errop)

              if (stor.errop) {
                j3pAffiche(stor.lesdiv.explik2, null, textes.expliqueOp[stor.op - 1] + '\n')
              }

              stor.lesdiv.explik2.classList.add('explique')
              me.afficheBoutonValider()
              this.typederreurs[1]++
              // indication éventuelle ici
            } else { // Erreur au nème essai
              this._stopTimer()
              this.cacheBoutonValider()

              stor.lesdiv.correction.innerHTML += '<br>' + regardeCorrection

              stor.listesDeroulantes[0].corrige(!stor.errop)
              stor.listesDeroulantes[0].disable(stor.errop)
              stor.listesDeroulantes.pop()
              stor.zsm3.disable()
              stor.zsm2.disable()
              stor.zsm1.disable()
              if (!stor.ok1) {
                stor.zsm1.corrige(false)
                stor.zsm1.barre()
              }
              if (!stor.ok2) {
                stor.zsm2.corrige(false)
                stor.zsm2.barre()
              }
              if (stor.errordre) {
                stor.zsm3.corrige(false)
                stor.zsm3.barre()
              }

              j3pAffiche(stor.lesdiv.explik1, null, '<b>' + textes.expliqueCorrige1 + '\n' + textes.expliqueCorrige2 + '</b>',
                {
                  a: ecrisBienMathquill(stor.nombre1),
                  b: textes.op[stor.op],
                  c: ecrisBienMathquill(stor.nombre2),
                  d: ecrisBienMathquill(stor.nombre1odg),
                  e: ecrisBienMathquill(stor.nombre2odg),
                  f: ecrisBienMathquill(stor.zordre)
                })

              stor.lesdiv.explik1.classList.add('correction')

              this.typederreurs[2]++
              this.etat = 'navigation'
              this.sectionCourante()
            }
          }
        }
      }
      // Obligatoire
      this.finCorrection()
      break // case "correction":

    case 'navigation':

      if (this.sectionTerminee()) {
        // On détermine la phrase d’état renvoyée par la section
        this.parcours.pe = (this.donneesSection.nbetapes * this.score) / this.donneesSection.nbitems
        this.afficheBoutonSectionSuivante()
        this.focus('sectioncontinuer')
      } else {
        this.etat = 'enonce'
        this.afficheBoutonSuite()
        this.focus('boutoncontinuer')
      }
      // Obligatoire
      this.finNavigation()

      break // case "navigation":
  }
}
