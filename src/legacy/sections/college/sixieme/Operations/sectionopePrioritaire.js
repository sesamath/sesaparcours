import { j3pAddContent, j3pAddElt, j3pBarre, j3pEmpty, j3pFocus, j3pGetRandomBool, j3pGetRandomInt, j3pStyle } from 'src/legacy/core/functions'
import { afficheBloc, phraseBloc } from 'src/legacy/core/functionsPhrase'
import ValidationZones from 'src/legacy/outils/calculatrice/ValidationZones'
import { j3pAffiche, mqRestriction } from 'src/lib/mathquill/functions'
import textesGeneriques from 'src/lib/core/textes'

const { essaieEncore, tempsDepasse } = textesGeneriques

/**
 * Mettre ici une description de la section
 * @author Rémi DENIAUD
 * @since octobre 2022
 * @fileOverview
 */

// nos constantes
const structure = 'presentation1'
// const ratioGauche = 0.75 C’est la valeur par défaut du ratio de la partie gauche dans presentation1
// N’utiliser cette variable que si ce ratio est différent
// (utilisé dans ce cas dans me.construitStructurePage(structure, ratioGauche) de initSection()

export const params = {
  // seuls les outils qui ne peuvent pas être importés doivent être déclarés ici (pour que le loader les charge),
  // sinon c’est à nous de les importer au début
  outils: ['calculatrice', 'mathquill'],
  // ce qui est paramétrable
  /**
   * Ce qui est paramétrable dans l’éditeur de graphe
   * @type {LegacySectionParamsParametres}
   */
  parametres: [
    ['nbrepetitions', 5, 'entier', 'Nombre de répétitions de la section'],
    ['indication', '', 'string', 'Indication pour chaque répétition'],
    ['limite', 0, 'entier', 'Temps disponible par répétition, en secondes'],
    ['nbchances', 2, 'entier', 'Nombre d’essais par répétition (valable uniquement lorsque avecCalcul vaut true)'],
    ['avecCalcul', false, 'boolean', 'Si elle vaut true, alors, après sélection de l’opération, on demande d’effectuer le calcul'],
    ['presenceParentheses', false, 'boolean', 'On peut imposer des parenthèses dans le calcul'],
    ['seuilReussite', 0.7, 'reel', 'score (sur 1) à partir duquel on considère la notion acquise.'],
    ['seuilEchec', 0.5, 'reel', 'Pour renvoyer la phrase d’état, on précise la proportion à partir de laquelle on estime identifier une erreur de l’élève']
    // les paramètres peuvent être du type 'entier', 'string', 'boolean', 'liste'
  ],
  // la liste des pe qui peuvent être retournés par une section quantitative
  pe: [
    { pe_1: 'Bravo' },
    { pe_2: 'Opération prioritaire non identifiée' },
    { pe_3: 'Problèmes dans les calculs' },
    { pe_4: 'Insuffisant' }
  ]
}

/**
 * La fonction principale, rappelée à chaque étape
 * Son this sera l’instance courante de Parcours
 * @this {Parcours}
 */
export default function maSection () {
  // suivant l’état courant on avise
  switch (this.etat) {
    case 'enonce':
      if (this.debutDeLaSection) {
        initSection(this)
        enonceStart(this)
      } else {
        enonceRestart(this)
      }
      // attention à bien appeler finEnonce() quand tout est fini (en général dans enonceEnd(me)), sinon on va rester coincé là
      // car le modèle l’attend avant de déploquer le passage à la suite
      break

    case 'correction':
      correction(this)
      break

    case 'navigation':
      navigation(this)
      break
  }
}

// ///////////////////////////////////////////////
// FONCTIONS Privées, déclarées une seule fois
// au chargement du fichier js
// ///////////////////////////////////////////////
/**
 * permet de mettre en évidence le signe lors du survol par la souris
 * @private
 * @param {Parcours} me
 * @param {HTMLElement} elt est l’élément HTML à mettre en surbrillance
 */
function surbrillanceSurvol (me, elt) {
  if (me.etat === 'correction' && elt.isActive) {
    const numDiv = elt.numero
    if (!me.storage.divselect.includes(numDiv)) {
      j3pStyle(elt, { backgroundColor: 'rgba(200,200,200,0.5)' })
    }
  }
}
/**
 * permet de mettre en évidence le signe lorsqu’on clique sur le signe
 * @private
 * @param {Parcours} me
 * @param {Array} tableauCalc contient les calculs à afficher suivant le signe choisi
 * @param {Array} tabResult est le tableau des réponses attendues
 * @param {HTMLElement} elt est l’élément HTML à mettre en surbrillance
 * @param {Array} elts tableau des éléments HTML qu’il faut remettre sans surbrillance
 * @param {HTMLElement} div est le div conteneur de la suite de la consigne (utile lorsqu’on a le calcul à faire)
 */
function surbrillanceSelect (me, tableauCalc, tabResult, elt, elts, div) {
  const ds = me.donneesSection
  const stor = me.storage
  if (me.etat === 'correction' && elt.isActive) {
    const numDiv = elt.numero
    if (!stor.divselect.includes(numDiv)) {
      // d’abord je remets 'sans fond' tous les div' dans le cas où j’ai un calcul à faire (car on ne peut dans ce cas sélectionner qu’un seul signe)
      if (ds.avecCalcul) {
        for (const span of elts) j3pStyle(span, { backgroundColor: 'rgba(200,200,200,0)' })
        stor.divselect = []
      }
      // Et je marque l’elt comme sélectionné
      j3pStyle(elt, { backgroundColor: 'rgba(200,200,200,1)' })
      stor.divselect.push(numDiv)
      if (ds.avecCalcul) {
        // Dans ce cas, j’affiche la zone de saisie
        j3pEmpty(div)
        phraseBloc(div, ds.textes.consigne3)
        const elt = afficheBloc(div, stor.lettre + tableauCalc[numDiv], {
          inputmq1: { dynamique: true, maxchars: 5 }
        })
        j3pStyle(elt.parent, { padding: '15px 0 0 20px' })
        stor.zoneInput = elt.inputmqList[0]
        stor.zoneInput.typeReponse = ['nombre', 'exact']
        stor.zoneInput.reponse = [tabResult[numDiv]]
        mqRestriction(stor.zoneInput, '\\d')
        j3pFocus(stor.zoneInput)
        stor.fctsValid = new ValidationZones({ zones: [stor.zoneInput], parcours: me })
      }
    } else {
      // Je désélectionne l’elt
      j3pStyle(elt, { backgroundColor: 'rgba(200,200,200,0)' })
      stor.divselect = stor.divselect.filter(value => { return value !== numDiv }) // On enlève numDiv dans le tableau stor.divselect
    }
  }
}
/**
 * permet d’enlever la mise en évidence du signe lorsqu’il n’est plus survolé
 * @param {Parcours} me
 * @param {HTMLElement} elt est l’élément HTML dont on enlève la surbrillance
 */
function surbrillanceSuppr (me, elt) {
  if (me.etat === 'correction' && elt.isActive) {
    const numDiv = elt.numero
    if (!me.storage.divselect.includes(numDiv)) {
      j3pStyle(elt, { backgroundColor: 'rgba(200,200,200,0)' })
    }
  }
}
/**
 * Retourne les données pour l’init
 * @private
 * @return {Object}
 */
function getDonnees () {
  return {

    // Nombre de répétitions de l’exercice
    nbrepetitions: 5,
    indication: '',
    nbchances: 2,
    presenceParentheses: false,
    avecCalcul: false,
    seuilReussite: 0.7,
    seuilEchec: 0.5,
    textes: {
      // le mieux est de mettre également le texte correspondant au titre de l’exo ici
      titreExo: 'Priorités opératoires',
      // on donne les phrases de la consigne
      consigne1: 'Sélectionne le (ou les) signe(s) correspondant à une opération prioritaire.',
      consigneComment1: 'Clique sur OK pour valider.',
      consigneComment2: 'De plus, tu n’as le droit qu’à un seul essai.',
      consigne2_1: 'On souhaite effectuer le calcul ci-dessous par étapes.',
      consigne2_2: 'Sélectionne tout d’abord le signe correspondant à l’opération prioritaire.',
      consigne3: 'Effectue alors ce calcul prioritaire.',
      consigne4: 'Termine alors le calcul.',
      consigne5_1: 'Poursuis alors le calcul.',
      consigne5_2: 'Poursuis le calcul en sélectionnant de nouveau le signe correspondant à l’opération prioritaire.',
      // les différents commentaires envisagés suivant les réponses fausses de l’élève
      comment1: 'Il faut sélectionner un signe&nbsp;!',
      comment2_1: 'Tu as une pénalité.',
      comment2_2: 'Tu as £n pénalités.',
      comment3: 'Tu n’as pas respecté les priorités opératoires&nbsp;!',
      comment4: 'C’est bien, continue.',
      // et les phrases utiles pour les explications de la réponse
      corr1: 'En l’absence de parenthèses, la multiplication et la division sont prioritaires sur l’addition et la soustraction.',
      corr2: 'Dans ce cas, la priorité opératoire est donnée au calcul entre parenthèses.',
      corr2_2: 'Ensuite, la multiplication et la division sont prioritaires sur l’addition et la soustraction.',
      corr3: 'Ensuite, en présence d’une ou plusieurs soustraction(s), on effectue les calculs de gauche à droite.',
      corr4: 'Ensuite, en l’absence de parenthèses, la multiplication et la division sont prioritaires sur l’addition et la soustraction.'
    },
    pe_1: 'Bravo',
    pe_2: 'Opération prioritaire non identifiée',
    pe_3: 'Problèmes dans les calculs',
    pe_4: 'Insuffisant'
  }
} // getDonnees

// on décide arbitrairement de nommer "me" la variable qui correspond à l’instance courante de Parcours (objet this de la fct principale)

/**
 * Appelé au début de la section
 * @private
 * @param {Parcours} me
 */
function initSection (me) {
  me.donneesSection = getDonnees()
  const stor = me.storage
  // On surcharge avec les parametres passés par le graphe
  me.surcharge()
  if (!me.donneesSection.avecCalcul) me.donneesSection.nbchances = 1
  // Construction de la page
  me.construitStructurePage(structure) // L’argument devient {structure:structure, ratioGauche:ratioGauche} si ratioGauche est défini comme variable globale
  me.afficheTitre(me.donneesSection.textes.titreExo)
  // on initialise un objet persistant pour garder une référence sur les éléments qu’on crée
  stor.elts = {}
  // et un autre pour nos listeners
  stor.listeners = {}
}

/**
 * Appelé au début de la section, après l’init
 * @private
 * @param {Parcours} me
 */
function enonceStart (me) {
  // on peut faire ici du chargement asynchrone

  // ou stocker des variables, par exemples celles qui serviraient pour la pe
  me.storage.pbOpePrio = 0
  me.storage.pbCalcul = 0
  // puis énoncé
  enonceEnd(me)
}

/**
 * Appelé au case enonce dans le cas d’une répétition
 * @private
 * @param {Parcours} me
 */
function enonceRestart (me) {
  // le nettoyage au restart
  me.videLesZones()
  enonceEnd(me)
}
/**
 * Retourne les données de l’exercice
 * @private
 * @param {Number} nbSignes est le nombre de signes dans l’expression
 * @param {Boolean} parentheses vaut true si on place des parentheses dans le calcul
 * @return {Object}
 */
function genereAlea (nbSignes, parentheses) {
  /**
   * données de l’exercice qui peuvent être les variables présentes dans les textes (consignes ou corrections)
   * ou autre données utiles à la validation
   * @private
   * @type {Object} obj
   */
  const obj = { avecParentheses: parentheses }
  let nb1, nb2
  obj.tabMultDiv = [] // array qui précise où se trouve le (ou les) signe(s) correspondant aux opérations prioritaires
  let resultInter, laRepInter
  obj.etapes = []
  obj.etapes.push([]) // je prévois un array d’array car il peut y avoir 2 opérations prioritaires donc je me prépare aux deux solutions possibles
  switch (nbSignes) {
    case 2:
      // calcul de la forme a signe1 b signe2 c
      nb1 = j3pGetRandomInt(2, 6)
      do {
        nb2 = j3pGetRandomInt(2, 6)
      } while (nb1 === nb2)
      if (j3pGetRandomBool()) {
        // Multiplication ou division au début
        obj.signe1 = j3pGetRandomBool() ? '\\times' : '\\div'
        obj.signe2 = j3pGetRandomBool() ? '+' : '-'
        if (obj.signe1 === '\\times') {
          obj.a = nb1
          obj.b = nb2
          obj.resultat = nb1 * nb2
          if (obj.signe2 === '+') {
            obj.c = j3pGetRandomInt(4, 12)
            obj.resultat += obj.c
            resultInter = obj.b + obj.c
          } else {
            obj.c = j3pGetRandomInt(1, nb1 * nb2 - 1)
            obj.resultat -= obj.c
            resultInter = obj.b - obj.c
          }
          laRepInter = obj.a * obj.b
          obj.resultInter = [nb1 * nb2, resultInter]
        } else {
          obj.a = nb1 * nb2
          obj.b = (j3pGetRandomBool()) ? nb1 : nb2
          obj.resultat = Math.round(obj.a / obj.b)
          if (obj.signe2 === '+') {
            obj.c = j3pGetRandomInt(4, 12)
            obj.resultat += obj.c
            resultInter = obj.b + obj.c
          } else {
            obj.c = j3pGetRandomInt(1, Math.round(obj.a / obj.b) - 1)
            obj.resultat -= obj.c
            resultInter = obj.b + obj.c
          }
          laRepInter = Math.round(obj.a / obj.b)
          obj.resultInter = [laRepInter, resultInter]
        }
        obj.etapes[0].push(laRepInter + obj.signe2 + obj.c)
        obj.tabMultDiv.push(0)
      } else {
        // Multiplication ou division à la fin
        obj.signe2 = j3pGetRandomBool() ? '\\times' : '\\div'
        obj.signe1 = j3pGetRandomBool() ? '+' : '-'
        if (obj.signe2 === '\\times') {
          obj.b = nb1
          obj.c = nb2
          obj.resultat = obj.b * obj.c
          if (obj.signe1 === '+') {
            obj.a = j3pGetRandomInt(4, 12)
            obj.resultat += obj.a
            resultInter = obj.a + obj.b
          } else {
            obj.a = j3pGetRandomInt(nb1 * nb2 + 2, nb1 * nb2 + 12)
            obj.resultat = obj.a - obj.resultat
            resultInter = obj.a - obj.b
          }
          laRepInter = Math.round(obj.b * obj.c)
        } else {
          obj.b = nb1 * nb2
          obj.c = (j3pGetRandomBool()) ? nb1 : nb2
          obj.resultat = Math.round(obj.b / obj.c)
          if (obj.signe1 === '+') {
            obj.a = j3pGetRandomInt(4, 12)
            obj.resultat += obj.a
            resultInter = obj.a + obj.b
          } else {
            obj.a = j3pGetRandomInt(Math.round(obj.b / obj.c) + 2, Math.round(obj.b / obj.c) + 13)
            obj.resultat = obj.a - obj.resultat
            resultInter = obj.a - obj.b
          }
          laRepInter = Math.round(obj.b / obj.c)
        }
        obj.resultInter = [resultInter, laRepInter]
        obj.tabMultDiv.push(1)
        obj.etapes[0].push(obj.a + obj.signe1 + laRepInter)
      }
      obj.etapes[0].push(obj.resultat)
      obj.tabCalc = ['&1& $' + obj.signe2 + obj.c + '$', '$' + obj.a + obj.signe1 + '$ &1&']
      obj.tabElts = [obj.a, obj.signe1, obj.b, obj.signe2, obj.c]
      obj.posSignes = [1, 3]
      break
    case 3 :
      // Je vais avoir plusieurs cas de figure
      {
        const casFigure = j3pGetRandomInt(1, 3)
        let calc, nbSomme
        if (obj.avecParentheses) {
          [obj.par1, obj.par2] = ['(', ')']
          switch (casFigure) {
            case 1:
              // (a+b)*c+d
              obj.tabMultDiv.push(0)
              obj.opePrio2 = [[0]]
              obj.signe2 = j3pGetRandomBool() ? '\\times' : '\\div'
              obj.signe1 = j3pGetRandomBool() ? '+' : '-'
              obj.signe3 = j3pGetRandomBool() ? '+' : '-'
              if (obj.signe2 === '\\times') {
                nbSomme = j3pGetRandomInt(5, 10)
                obj.c = j3pGetRandomInt(4, 9)
                calc = nbSomme * obj.c
              } else {
                obj.c = j3pGetRandomInt(4, 9)
                calc = j3pGetRandomInt(5, 9)
                nbSomme = obj.c * calc
              }
              if (obj.signe1 === '+') {
                obj.a = j3pGetRandomInt(2, nbSomme - 2)
                obj.b = nbSomme - obj.a
              } else {
                obj.b = j3pGetRandomInt(2, 7)
                obj.a = nbSomme + obj.b
              }
              if (obj.signe3 === '+') {
                obj.d = j3pGetRandomInt(5, 11)
                obj.resultat = calc + obj.d
              } else {
                obj.d = j3pGetRandomInt(2, calc - 2)
                obj.resultat = calc - obj.d
              }
              obj.tabCalc = ['&1& $' + obj.signe2 + obj.c + obj.signe3 + obj.d + '$', '$' + obj.a + obj.signe1 + '$ &1&' + '$' + obj.signe3 + obj.d + '$', '$(' + obj.a + obj.signe1 + obj.b + ')' + obj.signe2 + '$ &1&']
              obj.tabElts = [obj.par1, obj.a, obj.signe1, obj.b, obj.par2, obj.signe2, obj.c, obj.signe3, obj.d]
              resultInter = (obj.signe1 === '+') ? obj.a + obj.b : obj.a - obj.b
              obj.resultInter = [resultInter, (obj.signe2 === '\\times') ? obj.b * obj.c : Math.round(obj.b / obj.c), (obj.signe3 === '+') ? obj.c + obj.d : obj.c - obj.d]
              obj.posSignes = [2, 5, 7]
              obj.tabCalc2 = [['&1&$ ' + obj.signe3 + obj.d + '$', '$' + resultInter + obj.signe2 + '$ &1&']]
              obj.tabElts2 = [[resultInter, obj.signe2, obj.c, obj.signe3, obj.d]]
              obj.resultInter2 = [[(obj.signe2 === '\\times' ? resultInter * obj.c : Math.round(resultInter / obj.c)), (obj.signe3 === '+') ? obj.c + obj.d : obj.c - obj.d]]
              obj.etapes[0].push(nbSomme + obj.signe2 + obj.c + obj.signe3 + obj.d)
              obj.etapes[0].push(obj.resultInter2[0][0] + obj.signe3 + obj.d)
              // Les signes prioritaires dans la deuxième étape du calcul
              obj.tabMultDiv2 = [[0]]
              break
            case 2:
              // a+b*(c+d)
              obj.tabMultDiv.push(2)
              obj.opePrio2 = [[1]]
              obj.signe2 = j3pGetRandomBool() ? '\\times' : '\\div'
              obj.signe1 = j3pGetRandomBool() ? '+' : '-'
              obj.signe3 = j3pGetRandomBool() ? '+' : '-'
              if (obj.signe2 === '\\times') {
                nbSomme = j3pGetRandomInt(5, 10)
                obj.b = j3pGetRandomInt(4, 9)
                calc = nbSomme * obj.b
              } else {
                nbSomme = j3pGetRandomInt(5, 10)
                calc = j3pGetRandomInt(5, 9)
                obj.b = nbSomme * calc
              }
              if (obj.signe3 === '+') {
                do {
                  obj.c = j3pGetRandomInt(1, nbSomme - 1)
                  obj.d = nbSomme - obj.c
                } while (obj.c === obj.d)
              } else {
                obj.d = j3pGetRandomInt(2, 7)
                obj.c = nbSomme + obj.d
              }
              if (obj.signe1 === '+') {
                obj.a = j3pGetRandomInt(5, 11)
                obj.resultat = calc + obj.a
              } else {
                obj.a = j3pGetRandomInt(calc + 5, calc + 15)
                obj.resultat = obj.a - calc
              }
              obj.tabCalc = ['&1& $' + obj.signe2 + '(' + obj.c + obj.signe3 + obj.d + ')$', '$' + obj.a + obj.signe1 + '$ &1&' + '$' + obj.signe3 + obj.d + '$', '$' + obj.a + obj.signe1 + obj.b + obj.signe2 + '$ &1&']
              obj.tabElts = [obj.a, obj.signe1, obj.b, obj.signe2, obj.par1, obj.c, obj.signe3, obj.d, obj.par2]
              resultInter = (obj.signe3 === '+') ? obj.c + obj.d : obj.c - obj.d
              obj.resultInter = [(obj.signe1 === '+') ? obj.a + obj.b : obj.a - obj.b, (obj.signe2 === '\\times') ? obj.b * obj.c : Math.round(obj.b / obj.c), nbSomme]
              obj.posSignes = [1, 3, 6]
              obj.tabCalc2 = [['&1&$ ' + obj.signe2 + nbSomme + '$', '$' + obj.a + obj.signe1 + '$ &1&']]
              obj.tabElts2 = [[obj.a, obj.signe1, obj.b, obj.signe2, nbSomme]]
              obj.resultInter2 = [[(obj.signe1 === '+') ? obj.a + obj.b : obj.a - obj.b, calc]]
              obj.etapes[0].push(obj.a + obj.signe1 + obj.b + obj.signe2 + nbSomme)
              obj.etapes[0].push(obj.a + obj.signe1 + obj.resultInter2[0][1])
              // Les signes prioritaires dans la deuxième étape du calcul
              obj.tabMultDiv2 = [[1]]
              break
            case 3:
              // a+(b+c)*d
              obj.opePrio2 = [[1]]
              obj.tabMultDiv.push(1)
              obj.signe3 = j3pGetRandomBool() ? '\\times' : '\\div'
              obj.signe1 = j3pGetRandomBool() ? '+' : '-'
              obj.signe2 = j3pGetRandomBool() ? '+' : '-'
              if (obj.signe3 === '\\times') {
                nbSomme = j3pGetRandomInt(5, 10)
                obj.d = j3pGetRandomInt(4, 9)
                calc = nbSomme * obj.d
              } else {
                obj.d = j3pGetRandomInt(4, 9)
                calc = j3pGetRandomInt(5, 9)
                nbSomme = obj.d * calc
              }
              if (obj.signe2 === '+') {
                obj.b = j3pGetRandomInt(2, nbSomme - 2)
                obj.c = nbSomme - obj.b
              } else {
                obj.c = j3pGetRandomInt(2, 7)
                obj.b = nbSomme + obj.c
              }
              if (obj.signe1 === '+') {
                obj.a = j3pGetRandomInt(5, 11)
                obj.resultat = calc + obj.a
              } else {
                obj.a = j3pGetRandomInt(calc + 5, calc + 15)
                obj.resultat = obj.a - calc
              }
              obj.tabCalc = ['&1& $' + obj.signe2 + obj.c + obj.signe3 + obj.d + '$', '$' + obj.a + obj.signe1 + '$ &1&' + '$' + obj.signe3 + obj.d + '$', '$' + obj.a + obj.signe1 + obj.b + obj.signe2 + '$ &1&']
              obj.tabElts = [obj.a, obj.signe1, obj.par1, obj.b, obj.signe2, obj.c, obj.par2, obj.signe3, obj.d]
              obj.resultInter = [(obj.signe1 === '+') ? obj.a + obj.b : obj.a - obj.b, nbSomme, (obj.signe3 === '\\times') ? obj.c * obj.d : Math.round(obj.c / obj.d)]
              obj.posSignes = [1, 4, 7]
              obj.tabCalc2 = [['&1&$ ' + obj.signe3 + obj.d + '$', '$' + obj.a + obj.signe1 + '$ &1&']]
              obj.tabElts2 = [[obj.a, obj.signe1, nbSomme, obj.signe3, obj.d]]
              obj.resultInter2 = [[(obj.signe1 === '+') ? obj.a + nbSomme : obj.a - nbSomme, (obj.signe3 === '\\times') ? nbSomme * obj.d : Math.round(nbSomme / obj.d)]]
              obj.etapes[0].push(obj.a + obj.signe1 + nbSomme + obj.signe3 + obj.d)
              obj.etapes[0].push(obj.a + obj.signe1 + obj.resultInter2[0][1])
              // Les signes prioritaires dans la deuxième étape du calcul
              obj.tabMultDiv2 = [[1]]
              break
          }
        } else {
          let nbProduit, nbProduit2, resultInter2
          switch (casFigure) {
            case 1:
              // a+b*c+d
              obj.tabMultDiv.push(1)
              obj.opePrio2 = (obj.signe1 === '+') ? [[0, 1]] : [[0]]
              obj.signe2 = j3pGetRandomBool() ? '\\times' : '\\div'
              obj.signe1 = j3pGetRandomBool() ? '+' : '-'
              obj.signe3 = j3pGetRandomBool() ? '+' : '-'
              if (obj.signe2 === '\\times') {
                obj.b = j3pGetRandomInt(5, 9)
                obj.c = j3pGetRandomInt(4, 9)
                nbProduit = obj.b * obj.c
              } else {
                obj.c = j3pGetRandomInt(3, 7)
                nbProduit = j3pGetRandomInt(3, 7)
                obj.b = obj.c * nbProduit
              }
              if (obj.signe1 === '+') {
                obj.a = j3pGetRandomInt(5, 12)
                calc = obj.a + nbProduit
              } else {
                obj.a = nbProduit + j3pGetRandomInt(8, 15)
                calc = obj.a - nbProduit
              }
              if (obj.signe3 === '+') {
                obj.d = j3pGetRandomInt(5, 11)
              } else {
                obj.d = j3pGetRandomInt(2, calc - 2)
              }
              obj.resultat = (obj.signe3 === '+') ? calc + obj.d : calc - obj.d
              // Dans le cas où une bonne réponse a été donnée sur la première priorité, on enchaîne sur la 2nde
              obj.tabCalc2 = [['&1& $' + obj.signe3 + obj.d + '$', '$' + obj.a + obj.signe1 + '$ &1&']]
              obj.tabElts2 = [[obj.a, obj.signe1, nbProduit, obj.signe3, obj.d]]
              resultInter = (obj.signe2 === '\\times') ? obj.b * obj.c : Math.round(obj.b / obj.c)
              obj.resultInter = [(obj.signe1 === '+') ? obj.a + obj.b : obj.a - obj.b, resultInter, (obj.signe3 === '+') ? obj.c + obj.d : obj.c - obj.d]
              obj.resultInter2 = [[(obj.signe1 === '+') ? obj.a + resultInter : obj.a - resultInter, (obj.signe3 === '+') ? resultInter + obj.d : resultInter - obj.d]]
              obj.etapes[0].push(obj.a + obj.signe1 + nbProduit + obj.signe3 + obj.d)
              obj.etapes[0].push(obj.resultInter2[0][0] + obj.signe3 + obj.d)
              // Les signes prioritaires dans la deuxième étape du calcul
              obj.tabMultDiv2 = [[]]
              if (obj.signe1 === '+') obj.tabMultDiv2[0].push(0, 1)
              else obj.tabMultDiv2[0].push(0)
              break
            case 2:
              // a*b+c*d
              obj.opePrio2 = [[1], [0]]
              obj.tabMultDiv.push(0, 2)
              do {
                do {
                  obj.signe1 = j3pGetRandomBool() ? '\\times' : '\\div'
                  obj.signe3 = j3pGetRandomBool() ? '\\times' : '\\div'
                  obj.signe2 = j3pGetRandomBool() ? '+' : '-'
                } while (obj.signe1 === 'div' && obj.signe2 === '-' && obj.signe3 === '\\times')
                if (obj.signe1 === '\\times') {
                  obj.a = j3pGetRandomInt(5, 9)
                  obj.b = j3pGetRandomInt(4, 9)
                  nbProduit = obj.a * obj.b
                } else {
                  obj.b = j3pGetRandomInt(3, 7)
                  nbProduit = j3pGetRandomInt(3, 7)
                  obj.a = obj.b * nbProduit
                }
                if (obj.signe3 === '\\times') {
                  obj.c = j3pGetRandomInt(5, 9)
                  obj.d = j3pGetRandomInt(4, 9)
                  nbProduit2 = obj.c * obj.d
                } else {
                  obj.d = j3pGetRandomInt(3, 7)
                  nbProduit2 = j3pGetRandomInt(3, 7)
                  obj.c = obj.d * nbProduit2
                }
                // Je veux que le résultat final soit positif
                // Je me facilite la vie au niveau prog en recalcultant tout si ce n’est pas le cas
              } while (obj.signe2 === '-' && nbProduit <= nbProduit2)
              // Dans le cas où une bonne réponse a été donnée sur la première priorité, on enchaîne sur la 2nde
              obj.tabCalc2 = [
                ['&1& $' + obj.signe3 + obj.d + '$', '$' + nbProduit + obj.signe2 + '$ &1&'],
                ['&1& $' + obj.signe2 + nbProduit2 + '$', '$' + obj.a + obj.signe1 + '$ &1&']
              ]
              obj.tabElts2 = [
                [nbProduit, obj.signe2, obj.c, obj.signe3, obj.d],
                [obj.a, obj.signe1, obj.b, obj.signe2, nbProduit2]
              ]
              obj.resultat = (obj.signe2 === '+') ? nbProduit + nbProduit2 : nbProduit - nbProduit2
              resultInter = (obj.signe1 === '\\times') ? obj.a * obj.b : Math.round(obj.a / obj.b)
              resultInter2 = (obj.signe3 === '\\times') ? obj.c * obj.d : Math.round(obj.c / obj.d)
              obj.resultInter = [resultInter, (obj.signe1 === '+') ? obj.b + obj.c : obj.b - obj.c, resultInter2]
              obj.resultInter2 = [
                [(obj.signe2 === '+') ? resultInter + obj.c : resultInter - obj.c, (obj.signe3 === '\\times') ? obj.c * obj.d : Math.round(obj.c / obj.d)],
                [(obj.signe1 === '\\times') ? obj.a * obj.b : Math.round(obj.a / obj.b), (obj.signe2 === '+') ? obj.b + resultInter2 : obj.b - resultInter2]
              ]
              obj.etapes[0].push(nbProduit + obj.signe2 + obj.c + obj.signe3 + obj.d)
              obj.etapes[0].push(nbProduit + obj.signe2 + nbProduit2)
              obj.etapes.push([])
              obj.etapes[1].push(obj.a + obj.signe1 + obj.b + obj.signe2 + nbProduit2)
              obj.etapes[1].push(nbProduit + obj.signe2 + nbProduit2)
              obj.etapes[1].push(obj.resultat)
              // Les signes prioritaires dans la deuxième étape du calcul
              obj.tabMultDiv2 = [[1], [0]]
              break
            case 3:
              // a*b-c+d
              obj.opePrio2 = [[0]]
              do {
                obj.signe1 = j3pGetRandomBool() ? '\\times' : '\\div'
                obj.signe2 = '-'
                obj.signe3 = j3pGetRandomBool() ? '+' : '-'
                if (obj.signe1 === '\\times') {
                  obj.a = j3pGetRandomInt(2, 6)
                  obj.b = j3pGetRandomInt(2, 6)
                  nbProduit = obj.a * obj.b
                } else {
                  obj.b = j3pGetRandomInt(3, 7)
                  nbProduit = j3pGetRandomInt(5, 9)
                  obj.a = obj.b * nbProduit
                }
                obj.c = j3pGetRandomInt(3, nbProduit - 2)
                // nbSomme correspond à a+b
                if (obj.signe1 === '+') {
                  obj.d = j3pGetRandomInt(4, 13)
                } else {
                  obj.d = j3pGetRandomInt(2, nbProduit - obj.c)
                }
              } while (obj.d < 2)
              obj.resultat = (obj.signe3 === '+') ? nbProduit - obj.c + obj.d : nbProduit - obj.c - obj.d
              // Dans le cas où une bonne réponse a été donnée sur la première priorité, on enchaîne sur la 2nde
              resultInter = (obj.signe1 === '\\times') ? obj.a * obj.b : Math.round(obj.a / obj.b)
              obj.resultInter = [resultInter, obj.b - obj.c, (obj.signe3 === '+') ? obj.c + obj.d : obj.c - obj.d]
              obj.tabCalc2 = [['&1& $' + obj.signe3 + obj.d + '$', '$' + nbProduit + obj.signe2 + '$ &1&']]
              obj.tabElts2 = [[nbProduit, obj.signe2, obj.c, obj.signe3, obj.d]]
              obj.resultInter2 = [[resultInter - obj.c, (obj.signe3 === '+') ? obj.c + obj.d : obj.c - obj.d]]
              obj.tabMultDiv.push(0)
              obj.etapes[0].push(nbProduit + obj.signe2 + obj.c + obj.signe3 + obj.d)
              obj.etapes[0].push(obj.resultInter2[0][0] + obj.signe3 + obj.d)
              // Les signes prioritaires dans la deuxième étpe du calcul
              obj.tabMultDiv2 = [[0]]
              break
          }
          obj.tabCalc = ['&1& $' + obj.signe2 + obj.c + obj.signe3 + obj.d + '$', '$' + obj.a + obj.signe1 + '$ &1&' + '$' + obj.signe3 + obj.d + '$', '$' + obj.a + obj.signe1 + obj.b + obj.signe2 + '$ &1&']
          obj.tabElts = [obj.a, obj.signe1, obj.b, obj.signe2, obj.c, obj.signe3, obj.d]
          obj.posSignes = [1, 3, 5]
        }
      }
      obj.etapes[0].push(obj.resultat)
      obj.posSignes2 = [1, 3]
      break
    default:
      console.error('2 ou 3 signes seulement...')
      break
  }
  return obj
} // fin genereAlea

/**
 * Appelé après la validation pour terminer le calcul
 * @param {Parcours} me
 * @param {HTMLElement} conteneur est le div dans lequel écrire la suite du calcul
 */
function finCalc (me, conteneur) {
  const ds = me.donneesSection
  const stor = me.storage
  phraseBloc(conteneur, ds.textes.consigne4)
  const elt = afficheBloc(conteneur, stor.lettre + '&1&', {
    inputmq1: { dynamique: true, maxchars: 6 }
  })
  j3pStyle(elt.parent, { padding: '15px 0 0 20px' })
  stor.zoneInput = elt.inputmqList[0]
  mqRestriction(stor.zoneInput, '\\d')
  stor.zoneInput.typeReponse = ['nombre', 'exact']
  stor.zoneInput.reponse = [stor.objVariables.resultat]
  j3pFocus(stor.zoneInput)
  stor.spans = [] // cela va me servir à identifier que je suis à la fin
  stor.fctsValid = new ValidationZones({ zones: [stor.zoneInput], parcours: me })
}
/**
 * Appelé après la validation pour poursuivre le calcul
 * @param {Parcours} me
 * @param {HTMLElement} conteneur est le div dans lequel écrire la suite du calcul
 */
function poursuiteCalc (me, conteneur) {
  const ds = me.donneesSection
  const stor = me.storage
  phraseBloc(conteneur, ds.textes.consigne5_2)
  const eltCons2 = j3pAddElt(conteneur, 'div')
  j3pAffiche(eltCons2, '', stor.lettre)
  j3pStyle(eltCons2, { padding: '15px 0 0 20px' })
  stor.divselect = []
  stor.spans = construitCalcul(eltCons2, stor.objVariables.tabElts2[stor.k], stor.objVariables.posSignes2)
  stor.avantDerniereEtape = (stor.objVariables.posSignes2.length === 2)
  stor.divRepSaisie = j3pAddElt(stor.elts.divEnonce, 'div')
  stor.tabOpePrio = [...stor.objVariables.tabMultDiv2[stor.k]]
  for (const elt of stor.spans) {
    j3pStyle(elt, { borderRadius: '10px' })
    elt.addEventListener('mouseover', surbrillanceSurvol.bind(null, me, elt))
    elt.addEventListener('mouseout', surbrillanceSuppr.bind(null, me, elt))
    elt.addEventListener('click', surbrillanceSelect.bind(null, me, stor.objVariables.tabCalc2[stor.k], stor.objVariables.resultInter2[stor.k], elt, stor.spans, stor.divRepSaisie))
  }
}

/**
 * Permet de construire les spans contenant les éléments du calcul
 * @param {HTMLElement} conteneur est l’élément HTML contenant la suite de spans
 * @param {Array} tabElts est ce que contiendra chaque span
 * @param {Array} posSignes est le tableau qui précise la position des spans qui auront un signe
 * @return {Array} la liste des spans créés (éléments HTML)
 */
function construitCalcul (conteneur, tabElts, posSignes) {
  // PosSignes est utile si jamais le calcul possède des parenthèses
  const lesSpans = []
  let num = 0
  tabElts.forEach((elt, i) => {
    const span = j3pAddElt(conteneur, 'span')
    j3pStyle(span, { padding: '0 1px' })
    if (posSignes.includes(i)) {
      lesSpans.push(span)
      span.numero = num
      num++
      span.isActive = true
    }
    j3pAffiche(span, '', '$' + elt + '$')
  })
  return lesSpans
}
/**
 * Appelé après enonceStart ou enonceRestart
 * @private
 * @param me
 */
function enonceEnd (me) {
  const stor = me.storage
  const ds = me.donneesSection
  // on affiche l’énoncé
  stor.nbSignes = (ds.presenceParentheses)
    ? 3
    : (me.questionCourante <= Math.ceil(ds.nbrepetitions / 2)) ? 2 : 3
  stor.objVariables = genereAlea(stor.nbSignes, ds.presenceParentheses)
  stor.elts.divEnonce = j3pAddElt(me.zonesElts.MG, 'div', '', { style: me.styles.etendre('petit.enonce', { padding: '20px' }) })
  stor.divselect = []
  if (ds.avecCalcul) {
    phraseBloc(stor.elts.divEnonce, ds.textes.consigne2_1, stor.objVariables)
    phraseBloc(stor.elts.divEnonce, ds.textes.consigne2_2, stor.objVariables)
  } else {
    phraseBloc(stor.elts.divEnonce, ds.textes.consigne1, stor.objVariables)
    const phraseComment = phraseBloc(stor.elts.divEnonce, ds.textes.consigneComment1)
    j3pAddElt(phraseComment, 'br')
    j3pAddContent(phraseComment, ds.textes.consigneComment2)
    j3pStyle(phraseComment, { fontSize: '0.9em', fontStyle: 'italic' })
  }
  stor.lettre = '$' + String.fromCharCode(65 + me.questionCourante) + '=$'
  const eltCons2 = j3pAddElt(stor.elts.divEnonce, 'div')
  j3pAffiche(eltCons2, '', stor.lettre)
  j3pStyle(eltCons2, { padding: '15px 0 0 20px' })
  stor.spans = construitCalcul(eltCons2, stor.objVariables.tabElts, stor.objVariables.posSignes)
  stor.avantDerniereEtape = (stor.objVariables.posSignes.length === 2)
  stor.divRepSaisie = j3pAddElt(stor.elts.divEnonce, 'div')
  for (const elt of stor.spans) {
    j3pStyle(elt, { borderRadius: '10px' })
    elt.addEventListener('mouseover', surbrillanceSurvol.bind(null, me, elt))
    elt.addEventListener('mouseout', surbrillanceSuppr.bind(null, me, elt))
    elt.addEventListener('click', surbrillanceSelect.bind(null, me, stor.objVariables.tabCalc, stor.objVariables.resultInter, elt, stor.spans, stor.divRepSaisie))
  }
  stor.tabOpePrio = [...stor.objVariables.tabMultDiv]
  stor.finExo = false // Utilisé pour savoir si on est à la toute fin du calcul
  // On crée ici le div qui contiendra toutes les remarques sur ce qu’on pense de la réponse de l’élève (le plus souvent dans le cadre MD)
  // Il n’est ainsi construit qu’une seule fois et vidé à chaque passage dans correction()
  stor.zoneCorr = j3pAddElt(me.zonesElts.MD, 'div', '', { style: me.styles.moyen.correction })
  j3pStyle(stor.zoneCorr, { padding: '10px' })
  stor.nbPenalite = 0 // Sert quand on doit faire le calcul
  // divPenalite servira à indiquer le nombre de pénalités reçues
  stor.divPenalite = j3pAddElt(me.zonesElts.MD, 'div', '', { style: me.styles.moyen.correction })
  j3pStyle(stor.divPenalite, { color: me.styles.cfaux, padding: '10px' })
  // et on appelle finEnonce()
  me.finEnonce()
}

function afficheCorrection (me, bonneReponse = false) {
  const stor = me.storage
  const ds = me.donneesSection
  const zoneExpli = j3pAddElt(stor.elts.divEnonce, 'div', '', { style: me.styles.petit.correction })
  // La couleur de la correction sera fonction de la qualité de la réponse de l’élève
  j3pStyle(zoneExpli, { paddingTop: '10px', color: (bonneReponse) ? me.styles.cbien : me.styles.toutpetit.correction.color })
  if (stor.objVariables.avecParentheses) {
    phraseBloc(zoneExpli, ds.textes.corr2, stor.objVariables)
  } else {
    phraseBloc(zoneExpli, ds.textes.corr1, stor.objVariables)
  }
  if (ds.avecCalcul) {
    if (stor.objVariables.avecParentheses) phraseBloc(zoneExpli, ds.textes.corr4)
    else if (stor.objVariables.tabElts.includes('-')) phraseBloc(zoneExpli, ds.textes.corr3, stor.objVariables)
    for (const etape of stor.objVariables.etapes[0]) {
      const calc = afficheBloc(zoneExpli, stor.lettre + '$' + etape + '$', stor.objVariables)
      j3pStyle(calc.parent, { padding: '15px 0 0 20px' })
    }
  }
}

/**
 * Vérifie la réponse (retourne null si pas répondu)
 * @param {Parcours} me
 * @return {number|null}
 */
function getScore (me) {
  // check de la réponse, on retourne
  // - null si pas répondu
  // - 1 si ok
  // - un nombre entre 0 et 1 si ce n’est pas complètement bon
  const stor = me.storage
  const ds = me.donneesSection
  const reponse = (!ds.avecCalcul || stor.spans.length > 0)
    ? { aRepondu: stor.divselect.length > 0, bonneReponse: false }
    : { aRepondu: true, bonneReponse: false }
  if (reponse.aRepondu) {
    stor.signeSelectionne = true
    if (ds.avecCalcul) {
      const aRepondu = stor.fctsValid.valideReponses()
      if (aRepondu && stor.spans.length > 0) {
        // Dans ce cas, un seul signe a été sélectionné
        // Je vérifie si c’est une opération prioritaire
        stor.opePrio = stor.tabOpePrio.includes(stor.divselect[0])
        stor.k = stor.tabOpePrio.indexOf(stor.divselect[0]) // ceci me permet de savoir quel cas de figure a choisi l’élève
        if (stor.opePrio) {
          stor.reponseZone = stor.fctsValid.validationGlobale()
          reponse.aRepondu = stor.reponseZone.aRepondu
          reponse.bonneReponse = stor.reponseZone.bonneReponse
          if (stor.reponseZone.aRepondu && !stor.reponseZone.bonneReponse) stor.nbPenalite += 1
        } else {
          // Je lui laisse une autre chance s’il en a encore et je le pénalise
          stor.nbPenalite += 1
          stor.fctsValid.zones.bonneReponse[0] = false
          stor.fctsValid.coloreUneZone(stor.fctsValid.zones.inputs[0])
        }
      } else {
        stor.reponseZone = stor.fctsValid.validationGlobale()
        reponse.aRepondu = stor.reponseZone.aRepondu
        reponse.bonneReponse = stor.reponseZone.bonneReponse
        if (stor.reponseZone.aRepondu && !stor.reponseZone.bonneReponse) stor.nbPenalite += 1
      }
      if (reponse.aRepondu && !reponse.bonneReponse) {
        j3pEmpty(stor.divPenalite)
        if (stor.nbPenalite === 1) phraseBloc(stor.divPenalite, ds.textes.comment2_1)
        else if (stor.nbPenalite > 1) phraseBloc(stor.divPenalite, ds.textes.comment2_2, { n: stor.nbPenalite })
      }
    } else {
      // On vérifie si les signes sélectionnés sont les bons
      reponse.bonneReponse = (stor.tabOpePrio.length === stor.divselect.length)
      if (reponse.bonneReponse) {
        // Maitenant, je vérifie que ce sont les mêmes
        reponse.bonneReponse = (stor.divselect.every(elt => stor.tabOpePrio.includes(elt)))
      }
      for (const num of stor.divselect) {
        if (stor.tabOpePrio.includes(num)) {
          stor.spans[num].style.backgroundColor = me.styles.cbien
        } else {
          stor.spans[num].style.backgroundColor = me.styles.cfaux
          j3pBarre(stor.spans[num])
        }
        stor.spans[num].style.opacity = 0.4
      }
      for (const num of stor.tabOpePrio) {
        if (!stor.divselect.includes(num)) {
          stor.spans[num].style.backgroundColor = me.styles.toutpetit.correction.color
          stor.spans[num].style.opacity = 0.4
        }
      }
    }
  } else {
    stor.signeSelectionne = false
  }

  if (reponse.aRepondu) {
    return (reponse.bonneReponse) ? 1 : 0
  }
  return null
}

function correction (me) {
  const stor = me.storage
  const ds = me.donneesSection
  const score = getScore(me) // null si pas répondu
  if (ds.avecCalcul) {
    // Je ne passe à ce qui suit que si une erreur a été commise ou si j’ai bon à la toute fin du calcul
    if (score === 1 && stor.spans.length > 0) {
      // La question n’est pas terminée, c’est juste que j’ai eu bon jusqu'à présent
      for (const elt of stor.spans) elt.isActive = false
      if (stor.avantDerniereEtape) finCalc(me, j3pAddElt(stor.elts.divEnonce, 'div'))
      else poursuiteCalc(me, j3pAddElt(stor.elts.divEnonce, 'div'))
      // Il ne faut donc pas que je comptabilise une tentative
      me.essaiCourant -= 1
      me.reponseOk(stor.zoneCorr, ds.textes.comment4)
      return
    }
  }
  if ((score === 1) || (me.essaiCourant >= ds.nbchances)) me.score += Math.max(0, Math.round(100 * (score - stor.nbPenalite / ds.nbchances)) / 100)
  // pas de réponse sans limite de temps
  if (score === null && !me.isElapsed) {
    // on arrête là (on reste donc dans l’état correction avec le bouton valider actif)
    if (!stor.signeSelectionne) me.reponseManquante(stor.zoneCorr, ds.textes.comment1)
    else me.reponseManquante(stor.zoneCorr)
    return
  }
  if (score !== 1) {
    // On écrit c’est faux
    me.reponseKo(stor.zoneCorr)
    if (!stor.opePrio && me.essaiCourant === me.donneesSection.nbchances) {
      // Je lui annonce que le signe choisi est faux lorsqu’il n’a plus de tentative
      j3pAddElt(stor.zoneCorr, 'br')
      me.reponseKo(stor.zoneCorr, ds.textes.comment3, true)
    }
  }
  // le seul cas où on reste en correction, réponse fausse avec un autre essai
  if ((score !== 1) && me.essaiCourant < me.donneesSection.nbchances) {
    me.reponseKo(stor.zoneCorr, '<br>' + essaieEncore, true)
    return
  }

  // pour tout le reste on a terminé avec cette question, on affiche commentaire & correction, puis, au choix
  // 1) on choisit ici le prochain état, enonce ou navigation pour la fin de la section, et on met le bon bouton
  // 2) on affiche les commentaires et on passe d’office en navigation sans clic de l’élève, avec un sectionCourante('navigation'),
  //    et c’est là-bas que le choix se décide
  // 3) on a rien de spécial à afficher en fin de section, on fait comme pour 1) mais on active directement ici le bouton section suivante
  //    => on ne sera jamais appelé en méthode navigation
  // ci dessous l’option 2)
  if (score === 1) {
    // ça peut arriver aussi en temps limité (si le temps expire après la saisie mais avant que l’élève n’ait pu cliquer sur ok)
    // pour ça qu’on le teste en premier
    me.reponseOk(stor.zoneCorr)
    // Même dans le cas d’une bonne réponse, on peut afficher la correction qui aurait dans ce cas d’éventuels compléments d’information
    // afficheCorrection(me, true)
  } else {
    // ko, temps dépassé ou dernier essai
    if (me.isElapsed) {
      j3pAddElt(stor.zoneCorr, 'br')
      me.reponseKo(stor.zoneCorr, tempsDepasse, true)
    }
    afficheCorrection(me)
    if (!stor.opePrio) stor.pbOpePrio++
    else if (score !== 1) stor.pbCalcul++
  }
  // passe l’état en navigation et nous rappelle
  me.finCorrection('navigation', true)
}

function navigation (me) {
  if (me.sectionTerminee()) {
    // fin de section, on affiche un recap global (ou pas…)

    // on peut aussi calculer la pe retournée (seulement lorsqu’on demande un calcul)
    if (me.donneesSection.avecCalcul) {
      if (me.score / me.donneesSection.nbrepetitions > me.donneesSection.seuilReussite) me.parcours.pe = me.donneesSection.pe_1
      else if (me.storage.pbOpePrio / me.donneesSection.nbrepetitions > me.donneesSection.seuilEchec) me.parcours.pe = me.donneesSection.pe_2
      else if (me.storage.pbCalcul / me.donneesSection.nbrepetitions > me.donneesSection.seuilEchec) me.parcours.pe = me.donneesSection.pe_3
      else me.parcours.pe = me.donneesSection.pe_4
    }
    // et on affiche ce bout
    me.afficheBoutonSectionSuivante(true)
  } else {
    me.etat = 'enonce'
    // on laisse le bouton suite mis par le finCorrection() précédent
  }
  // Si on a rien à afficher de particulier après la dernière répétition, donc on passe true à finNavigation
  // pour que le modèle affiche directement le bouton "section suivante" (sans devoir cliquer sur 'suite' avant)
  me.finNavigation(true)
}
