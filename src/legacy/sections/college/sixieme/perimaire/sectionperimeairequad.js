import { j3pAddElt, j3pArrondi, j3pGetRandomInt, j3pShowError, j3pShuffle } from 'src/legacy/core/functions'
import ListeDeroulante from 'src/legacy/outils/listeDeroulante/ListeDeroulante'
import { verifNombreBienEcrit } from 'src/legacy/outils/fonctions/ecritureNombre'
import { j3pAffiche } from 'src/lib/mathquill/functions'
import ZoneStyleMathquill1 from 'src/legacy/outils/zoneStyleMathquill/ZoneStyleMathquill1'
import { addDefaultTable } from 'src/legacy/themes/table'
import { j3pCreeSVG } from 'src/legacy/core/functionsSvg'
import { getMtgCore } from 'src/lib/outils/mathgraph'
import textesGeneriques from 'src/lib/core/textes'

const { cBien, cFaux, essaieEncore, reponseIncomplete, tempsDepasse } = textesGeneriques

/**
 * Ajouter une description ici
 * @fileOverview
 * @author Tommy Barroy <t.barroy@laposte.net>
 * @since 2017-10
*/
export const params = {
  outils: ['mathquill'],
  /**
   * Ce qui est paramétrable dans l’éditeur de graphe
   * @type {LegacySectionParamsParametres}
   */
  parametres: [
    ['nbrepetitions', 3, 'entier', 'Nombre de répétitions de la section'],
    ['nbchances', 2, 'entier', 'Nombre d’essais possibles.'],
    ['Question', 'les deux', 'liste', '<u>Aire</u>: Calcule de l’aire. <br><br> <u>Périmètre</u>: calcul du périmètre.', ['les deux', 'Aire', 'Périmètre']],
    ['AireMax', 48, 'entier', 'Maximum de carreaux composant la figure (entre 1 et 48)'],
    ['AireMin', 1, 'entier', 'Minimum de carreaux composant la figure (entre 1 et 48)'],
    ['Ul', true, 'boolean', '<u>true</u>: L’unité de longueur varie de 1 à 2 carreaux'],
    ['Ua', true, 'boolean', '<u>true</u>: L’unité d’aire varie de 1/2 à 4 carreaux'],
    ['Unite2', true, 'boolean', '<u>true</u>: Affiche les deux unités.'],
    // le modèle gère la compatibilité ascendante avec l’ancien paramètre MiseEnPagePlusClaire
    ['theme', 'standard', 'liste', 'Modèle pour l’affichage', ['standard', 'zonesAvecImageDeFond']]
  ]
}

/**
 * section perimeairequad
 * @this {Parcours}
 */
export default function main () {
  const me = this
  let ds = me.donneesSection
  const stor = me.storage

  function getDonnees () {
    return {
      // Nombre de répétitions de l’exercice
      nbrepetitions: 3,

      Question: 'les deux',
      Ul: true,
      Ua: true,
      AireMax: 48,
      AireMin: 1,
      Unite2: true,

      // Si indication != "" alors un lien "Indication" s’affiche dans la zone Inférieure Gauche (this.zones.IG
      // une indication<br>de deux lignes..."
      indication: '',

      // pour le cas de presentation1bis
      // En effet pas d’indication possible pour cette présentation
      // surchargeindication: false,

      // Nombre de chances dont dispose l’él§ve pour répondre à la question
      nbchances: 2,

      //  || "presentation2" || "presentation3"  || "presentation1bis"
      structure: 'presentation1bis'
    }
  }

  function initSection () {
    let i

    stor.baseh = 148
    stor.basev = 204

    ds = getDonnees()
    me.donneesSection = ds
    // On surcharge avec les parametres passés par le graphe
    me.surcharge()
    me.validOnEnter = false // ex donneesSection.touche_entree

    // Construction de la page
    me.construitStructurePage(me.donneesSection.structure)

    me.donneesSection.nbetapes = 1
    me.donneesSection.nbitems = me.donneesSection.nbetapes * me.donneesSection.nbrepetitions

    ds.lesExos = []
    let lp = []
    if (ds.Question === 'les deux' || ds.Question === 'Aire') lp.push('aire')
    if (ds.Question === 'les deux' || ds.Question === 'Périmètre') lp.push('perim')
    for (i = 0; i < me.donneesSection.nbrepetitions; i++) {
      ds.lesExos.push({ question: lp[i % lp.length] })
    }
    ds.lesExos = j3pShuffle(ds.lesExos)

    lp = [1]
    if (ds.Ul) lp = [1, 2]
    lp = j3pShuffle(lp)
    for (i = 0; i < me.donneesSection.nbrepetitions; i++) {
      ds.lesExos[i].ul = lp[i % lp.length]
    }
    ds.lesExos = j3pShuffle(ds.lesExos)

    lp = [1]
    if (ds.Ua) lp = [0.5, 2, 1, 4]
    lp = j3pShuffle(lp)
    for (i = 0; i < me.donneesSection.nbrepetitions; i++) {
      ds.lesExos[i].ua = lp[i % lp.length]
    }
    ds.lesExos = j3pShuffle(ds.lesExos)

    if (isNaN(ds.AireMax)) ds.AireMax = 20
    ds.AireMax = Math.min(ds.AireMax, 48)
    ds.AireMax = Math.max(ds.AireMax, 1)
    if (isNaN(ds.AireMin)) ds.AireMin = 20
    ds.AireMin = Math.min(ds.AireMin, 48)
    ds.AireMin = Math.max(ds.AireMin, 1)
    if (ds.AireMin > ds.AireMax) ds.AireMin = ds.AireMAx
    lp = []
    for (i = ds.AireMin; i < ds.AireMax + 1; i++) {
      lp.push(i)
    }
    lp = j3pShuffle(lp)
    for (i = 0; i < me.donneesSection.nbrepetitions; i++) {
      ds.lesExos[i].nbcase = lp[i % lp.length]
    }
    ds.lesExos = j3pShuffle(ds.lesExos)

    me.typederreurs = [0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0]

    me.score = 0

    let tt
    switch (ds.Question) {
      case 'les deux':
        tt = 'Aire et périmètre dans un quadrillage'
        break
      case 'Aire':
        tt = 'Aire dans un quadrillage'
        break
      case 'Périmètre':
        tt = 'Périmètre dans un quadrillage'
    }

    me.afficheTitre(tt)

    // chargement mtg
    getMtgCore({ withMathJax: true })
      .then(
        // success
        (mtgAppLecteur) => {
          stor.mtgAppLecteur = mtgAppLecteur
          enonceMain()
        },
        // failure
        (error) => {
          j3pShowError(error, { message: 'mathgraph n’est pas correctement chargé', mustNotify: true })
        })
      // plantage dans enonceMain
      .catch(j3pShowError)
  }

  function faisChoixExos () {
    const e = ds.lesExos.pop()
    let i, j
    stor.lescase = []
    for (i = 0; i < 7; i++) {
      stor.lescase[i] = []
      for (j = 0; j < 7; j++) {
        stor.lescase[i][j] = false
      }
    }
    stor.lescase[4][4] = true
    stor.nbtraits = 0

    for (i = 1; i < e.nbcase; i++) {
      Ajoutcase()
    }

    /// /pour afficher les côté
    stor.lescotesHori = []
    stor.lescoteVerti = []
    for (i = 0; i < 8; i++) {
      stor.lescotesHori[i] = []
      stor.lescoteVerti[i] = []
      for (j = 0; j < 7; j++) {
        stor.lescotesHori[i][j] = 0
        stor.lescoteVerti[i][j] = 0
      }
    }

    for (i = 0; i < 7; i++) {
      for (j = 0; j < 7; j++) {
        if (stor.lescase[i][j]) faisCote(i, j)
      }
    }

    return e
  }

  function Ajoutcase () {
    let di = j3pGetRandomInt(0, 6)
    let dj = j3pGetRandomInt(0, 6)
    let cpt = 0
    let sol
    do {
      cpt++
      di++
      if (di === 7) {
        di = 0
        dj++
        if (dj === 7) dj = 0
      }
      if (stor.lescase[di][dj]) {
        sol = casepo(di, dj)
        if (sol.length > 0) {
          sol = j3pShuffle(sol)
          stor.lescase[sol[0].i][sol[0].j] = true
          return
        }
      }
    } while (cpt < 1000)
  }

  function casepo (li, lj) {
    const sol = []
    if (li > 0) {
      if (!stor.lescase[li - 1][lj]) sol.push({ i: li - 1, j: lj })
    }
    if (li < 6) {
      if (!stor.lescase[li + 1][lj]) sol.push({ i: li + 1, j: lj })
    }
    if (lj > 0) {
      if (!stor.lescase[li][lj - 1]) sol.push({ i: li, j: lj - 1 })
    }
    if (lj < 6) {
      if (!stor.lescase[li][lj + 1]) sol.push({ i: li, j: lj + 1 })
    }
    return sol
  }

  function faisCote (a, b) {
    stor.lescotesHori[b][a]++
    stor.lescotesHori[b + 1][a]++
    stor.lescoteVerti[a][b]++
    stor.lescoteVerti[a + 1][b]++
  }

  function metVisibleH (a, b) {
    stor.nbtraits++
    stor.mtgAppLecteur.setVisible('mtg32svg', stor.baseh + a * 7 + b, true, true)
  }

  function metVisibleV (a, b) {
    stor.nbtraits++
    stor.mtgAppLecteur.setVisible('mtg32svg', stor.basev + a * 7 + b, true, true)
  }

  function poseQuestion () {
    let buf = ''
    let tt, t2

    if (stor.Lexo.question === 'aire') {
      if (stor.Lexo.ua !== 1) buf = '<i> (avec la bonne unité d’aire) </i>'
      tt = '<b>Donne l’aire de la figure ci-dessous.' + buf + '</b>\n'
      t2 = 'Aire = &nbsp;'
    } else {
      if (stor.Lexo.ul !== 1) buf = '<i> (avec la bonne unité de longueur) </i>'
      tt = '<b>Donne le périmètre de la figure ci-dessous.' + buf + '</b>\n'
      t2 = 'Périmètre = &nbsp;'
    }
    j3pAffiche(stor.lesdiv.consigne, null, tt)

    const tab = addDefaultTable(stor.lesdiv.travail, 1, 3)
    j3pAffiche(tab[0][0], null, t2)
    stor.zone = new ZoneStyleMathquill1(tab[0][1], { restric: stor.restric, limitenb: 20, limite: 40, hasAutoKeyboard: stor.clvspe, enter: me.sectionCourante.bind(me), inverse: true })
    stor.liste = ListeDeroulante.create(tab[0][2], ['Choisir', 'u.l.', 'u.a.', 'cm', 'cm²'], { sensHaut: true })
  }

  function creeLesDiv () {
    stor.lesdiv = {}
    stor.lesdiv.conteneur = j3pAddElt(me.zones.MG, 'div', {
      style: me.styles.etendre('toutpetit.enonce', { padding: '10px' })
    })
    stor.lesdiv.consigneG = addDefaultTable(stor.lesdiv.conteneur, 1, 1)[0][0]
    const ttab = addDefaultTable(stor.lesdiv.consigneG, 2, 1)
    stor.lesdiv.consigne = ttab[0][0]
    stor.lesdiv.zonefig = ttab[1][0]
    stor.lesdiv.travail = addDefaultTable(stor.lesdiv.conteneur, 1, 1)[0][0]
    stor.lesdiv.explications = j3pAddElt(stor.lesdiv.conteneur, 'div', {
      style: me.styles.etendre('toutpetit.enonce', { padding: '10px' })
    })
    stor.lesdiv.solution = j3pAddElt(stor.lesdiv.conteneur, 'div', {
      style: me.styles.etendre('toutpetit.enonce', { padding: '10px' })
    })
    stor.lesdiv.correction = j3pAddElt(me.zones.MD, 'div', {
      style: me.styles.etendre('toutpetit.enonce', { padding: '10px' })
    })
    stor.lesdiv.explications.style.color = me.styles.cfaux
    stor.lesdiv.solution.style.color = me.styles.toutpetit.correction.color
  }

  function makefig () {
    let i, j

    const txtFigure = 'TWF0aEdyYXBoSmF2YTEuMAAAABM+TMzNAAJmcv###wEA#wEAAAAAAAAAAANRAAACSgAAAQEAAAAAAAAAAQAAAWr#####AAAAAQAKQ0NhbGNDb25zdAD#####AAJwaQAWMy4xNDE1OTI2NTM1ODk3OTMyMzg0Nv####8AAAABAApDQ29uc3RhbnRlQAkh+1RELRj#####AAAAAQAKQ1BvaW50QmFzZQD#####AAAAAAAOAAFPAMAoAAAAAAAAAAAAAAAAAAAAAAUAAUCEVij1wo9cQH1muFHrhR######AAAAAQAUQ0Ryb2l0ZURpcmVjdGlvbkZpeGUA#####wEAAAAAEAAAAQAAAAEAAAABAT#wAAAAAAAA#####wAAAAEAD0NQb2ludExpZURyb2l0ZQD#####AAAAAAEOAAFJAMAYAAAAAAAAAAAAAAAAAAAAAAUAAUA9V41P3ztlAAAAAv####8AAAABAAlDRHJvaXRlQUIA#####wAAAAAAEAAAAQAAAAEAAAABAAAAA#####8AAAABABZDRHJvaXRlUGVycGVuZGljdWxhaXJlAP####8AAAAAABAAAAEAAAABAAAAAQAAAAT#####AAAAAQAJQ0NlcmNsZU9BAP####8BAAAAAAAAAQAAAAEAAAAD#####wAAAAEAEENJbnREcm9pdGVDZXJjbGUA#####wAAAAUAAAAG#####wAAAAEAEENQb2ludExpZUJpcG9pbnQA#####wEAAAAAEAAAAQAABQABAAAABwAAAAkA#####wAAAAABDgABSgDAKAAAAAAAAMAQAAAAAAAAAAAFAAIAAAAH#####wAAAAIAB0NSZXBlcmUA#####wDm5uYAAAABAAAAAQAAAAMAAAAJAQEAAAAAAQAAAAAAAAAAAAAAAQAAAAAAAAAAAAAAAT#wAAAAAAAAAAAAAT#wAAAAAAAA#####wAAAAEACkNVbml0ZXhSZXAA#####wAEdW5pdAAAAAr#####AAAAAQALQ0hvbW90aGV0aWUA#####wAAAAH#####AAAAAQAKQ09wZXJhdGlvbgMAAAABP#AAAAAAAAD#####AAAAAQAPQ1Jlc3VsdGF0VmFsZXVyAAAAC#####8AAAABAAtDUG9pbnRJbWFnZQD#####AQAAAAAQAAJXIgEAAAEAAAAAAwAAAAz#####AAAAAQAJQ0xvbmd1ZXVyAP####8AAAABAAAADf####8AAAABAAdDQ2FsY3VsAP####8AB25iZ3JhZHgAAjIwAAAAAUA0AAAAAAAAAAAAEQD#####AAduYmdyYWR5AAIyMAAAAAFANAAAAAAAAP####8AAAABABRDSW1wbGVtZW50YXRpb25Qcm90bwD#####ABRHcmFkdWF0aW9uQXhlc1JlcGVyZQAAABsAAAAIAAAAAwAAAAoAAAAPAAAAEP####8AAAABABNDQWJzY2lzc2VPcmlnaW5lUmVwAAAAABEABWFic29yAAAACv####8AAAABABNDT3Jkb25uZWVPcmlnaW5lUmVwAAAAABEABW9yZG9yAAAACgAAAAsAAAAAEQAGdW5pdGV4AAAACv####8AAAABAApDVW5pdGV5UmVwAAAAABEABnVuaXRleQAAAAr#####AAAAAQAQQ1BvaW50RGFuc1JlcGVyZQAAAAARAAAAAAAQAAABAAAFAAAAAAoAAAAOAAAAEgAAAA4AAAATAAAAFgAAAAARAAAAAAAQAAABAAAFAAAAAAoAAAANAAAAAA4AAAASAAAADgAAABQAAAAOAAAAEwAAABYAAAAAEQAAAAAAEAAAAQAABQAAAAAKAAAADgAAABIAAAANAAAAAA4AAAATAAAADgAAABUAAAAMAAAAABEAAAAWAAAADgAAAA8AAAAPAAAAABEAAAAAABAAAAEAAAUAAAAAFwAAABkAAAAMAAAAABEAAAAWAAAADgAAABAAAAAPAAAAABEAAAAAABAAAAEAAAUAAAAAGAAAABv#####AAAAAQAIQ1NlZ21lbnQAAAAAEQEAAAAAEAAAAQAAAAEAAAAXAAAAGgAAABcAAAAAEQEAAAAAEAAAAQAAAAEAAAAYAAAAHAAAAAQAAAAAEQEAAAAACwABVwDAFAAAAAAAAMA0AAAAAAAAAAAFAAE#3FZ4mrzfDgAAAB3#####AAAAAgAIQ01lc3VyZVgAAAAAEQAGeENvb3JkAAAACgAAAB8AAAARAAAAABEABWFic3cxAAZ4Q29vcmQAAAAOAAAAIP####8AAAACABJDTGlldU9iamV0UGFyUHRMaWUBAAAAEQBmZmYAAAAAAB8AAAAOAAAADwAAAB8AAAACAAAAHwAAAB8AAAARAAAAABEABWFic3cyAA0yKmFic29yLWFic3cxAAAADQEAAAANAgAAAAFAAAAAAAAAAAAAAA4AAAASAAAADgAAACEAAAAWAAAAABEBAAAAABAAAAEAAAUAAAAACgAAAA4AAAAjAAAADgAAABMAAAAZAQAAABEAZmZmAAAAAAAkAAAADgAAAA8AAAAfAAAABQAAAB8AAAAgAAAAIQAAACMAAAAkAAAABAAAAAARAQAAAAALAAFSAEAgAAAAAAAAwCAAAAAAAAAAAAUAAT#RG06BtOgfAAAAHv####8AAAACAAhDTWVzdXJlWQAAAAARAAZ5Q29vcmQAAAAKAAAAJgAAABEAAAAAEQAFb3JkcjEABnlDb29yZAAAAA4AAAAnAAAAGQEAAAARAGZmZgAAAAAAJgAAAA4AAAAQAAAAJgAAAAIAAAAmAAAAJgAAABEAAAAAEQAFb3JkcjIADTIqb3Jkb3Itb3JkcjEAAAANAQAAAA0CAAAAAUAAAAAAAAAAAAAADgAAABMAAAAOAAAAKAAAABYAAAAAEQEAAAAAEAAAAQAABQAAAAAKAAAADgAAABIAAAAOAAAAKgAAABkBAAAAEQBmZmYAAAAAACsAAAAOAAAAEAAAACYAAAAFAAAAJgAAACcAAAAoAAAAKgAAACv#####AAAAAgAMQ0NvbW1lbnRhaXJlAAAAABEBZmZmAAAAAAAAAAAAQBgAAAAAAAAAAAAAAB8LAAH###8AAAABAAAAAAAAAAEAAAAAAAAAAAALI1ZhbChhYnN3MSkAAAAZAQAAABEAZmZmAAAAAAAtAAAADgAAAA8AAAAfAAAABAAAAB8AAAAgAAAAIQAAAC0AAAAbAAAAABEBZmZmAAAAAAAAAAAAQBgAAAAAAAAAAAAAACQLAAH###8AAAABAAAAAAAAAAEAAAAAAAAAAAALI1ZhbChhYnN3MikAAAAZAQAAABEAZmZmAAAAAAAvAAAADgAAAA8AAAAfAAAABgAAAB8AAAAgAAAAIQAAACMAAAAkAAAALwAAABsAAAAAEQFmZmYAwCAAAAAAAAA#8AAAAAAAAAAAAAAAJgsAAf###wAAAAIAAAABAAAAAQAAAAAAAAAAAAsjVmFsKG9yZHIxKQAAABkBAAAAEQBmZmYAAAAAADEAAAAOAAAAEAAAACYAAAAEAAAAJgAAACcAAAAoAAAAMQAAABsAAAAAEQFmZmYAwBwAAAAAAAAAAAAAAAAAAAAAAAAAKwsAAf###wAAAAIAAAABAAAAAQAAAAAAAAAAAAsjVmFsKG9yZHIyKQAAABkBAAAAEQBmZmYAAAAAADMAAAAOAAAAEAAAACYAAAAGAAAAJgAAACcAAAAoAAAAKgAAACsAAAAz#####wAAAAEADUNQb2ludEJhc2VFbnQA#####wEAAAAAEAAAAAAAAAAAAAAAQAgAAAAAAAAAAAgAAQAAAArANQAAAAAAAEAuAAAAAAAAAQEAAAAcAP####8BAAAAABAAAAAAAAAAAAAAAEAIAAAAAAAAAAAFAAEAAAAKwDQAAAAAAABALgAAAAAAAAEBAAAAHAD#####AQAAAAAQAAAAAAAAAAAAAABACAAAAAAAAAAABQABAAAACsAzAAAAAAAAQC4AAAAAAAABAQAAABwA#####wEAAAAAEAAAAAAAAAAAAAAAQAgAAAAAAAAAAAUAAQAAAArAMgAAAAAAAEAuAAAAAAAAAQEAAAAcAP####8BAAAAABAAAAAAAAAAAAAAAEAIAAAAAAAAAAAFAAEAAAAKwDEAAAAAAABALgAAAAAAAAEBAAAAHAD#####AQAAAAAQAAAAAAAAAAAAAABACAAAAAAAAAAABQABAAAACsAwAAAAAAAAQC4AAAAAAAABAQAAABwA#####wEAAAAAEAAAAAAAAAAAAAAAQAgAAAAAAAAAAAUAAQAAAArALgAAAAAAAEAuAAAAAAAAAQEAAAAcAP####8BAAAAABAAAAAAAAAAAAAAAEAIAAAAAAAAAAAFAAEAAAAKwCwAAAAAAABALgAAAAAAAAEBAAAAHAD#####AQAAAAAQAAAAAAAAAAAAAABACAAAAAAAAAAABQABAAAACsA1AAAAAAAAQCwAAAAAAAABAQAAABwA#####wEAAAAAEAAAAAAAAAAAAAAAQAgAAAAAAAAAAAUAAQAAAArANAAAAAAAAEAsAAAAAAAAAQEAAAAcAP####8BAAAAABAAAAAAAAAAAAAAAEAIAAAAAAAAAAAFAAEAAAAKwDMAAAAAAABALAAAAAAAAAEBAAAAHAD#####AQAAAAAQAAAAAAAAAAAAAABACAAAAAAAAAAABQABAAAACsAyAAAAAAAAQCwAAAAAAAABAQAAABwA#####wEAAAAAEAAAAAAAAAAAAAAAQAgAAAAAAAAAAAUAAQAAAArAMQAAAAAAAEAsAAAAAAAAAQEAAAAcAP####8BAAAAABAAAAAAAAAAAAAAAEAIAAAAAAAAAAAFAAEAAAAKwDAAAAAAAABALAAAAAAAAAEBAAAAHAD#####AQAAAAAQAAAAAAAAAAAAAABACAAAAAAAAAAABQABAAAACsAuAAAAAAAAQCwAAAAAAAABAQAAABwA#####wEAAAAAEAAAAAAAAAAAAAAAQAgAAAAAAAAAAAUAAQAAAArALAAAAAAAAEAsAAAAAAAAAQEAAAAcAP####8BAAAAABAAAAAAAAAAAAAAAEAIAAAAAAAAAAAFAAEAAAAKwDUAAAAAAABAKgAAAAAAAAEBAAAAHAD#####AQAAAAAQAAAAAAAAAAAAAABACAAAAAAAAAAABQABAAAACsA0AAAAAAAAQCoAAAAAAAABAQAAABwA#####wEAAAAAEAAAAAAAAAAAAAAAQAgAAAAAAAAAAAUAAQAAAArAMwAAAAAAAEAqAAAAAAAAAQEAAAAcAP####8BAAAAABAAAAAAAAAAAAAAAEAIAAAAAAAAAAAFAAEAAAAKwDIAAAAAAABAKgAAAAAAAAEBAAAAHAD#####AQAAAAAQAAAAAAAAAAAAAABACAAAAAAAAAAABQABAAAACsAxAAAAAAAAQCoAAAAAAAABAQAAABwA#####wEAAAAAEAAAAAAAAAAAAAAAQAgAAAAAAAAAAAUAAQAAAArAMAAAAAAAAEAqAAAAAAAAAQEAAAAcAP####8BAAAAABAAAAAAAAAAAAAAAEAIAAAAAAAAAAAFAAEAAAAKwC4AAAAAAABAKgAAAAAAAAEBAAAAHAD#####AQAAAAAQAAAAAAAAAAAAAABACAAAAAAAAAAABQABAAAACsAsAAAAAAAAQCoAAAAAAAABAQAAABwA#####wEAAAAAEAAAAAAAAAAAAAAAQAgAAAAAAAAAAAUAAQAAAArANQAAAAAAAEAoAAAAAAAAAQEAAAAcAP####8BAAAAABAAAAAAAAAAAAAAAEAIAAAAAAAAAAAFAAEAAAAKwDQAAAAAAABAKAAAAAAAAAEBAAAAHAD#####AQAAAAAQAAAAAAAAAAAAAABACAAAAAAAAAAABQABAAAACsAzAAAAAAAAQCgAAAAAAAABAQAAABwA#####wEAAAAAEAAAAAAAAAAAAAAAQAgAAAAAAAAAAAUAAQAAAArAMgAAAAAAAEAoAAAAAAAAAQEAAAAcAP####8BAAAAABAAAAAAAAAAAAAAAEAIAAAAAAAAAAAFAAEAAAAKwDEAAAAAAABAKAAAAAAAAAEBAAAAHAD#####AQAAAAAQAAAAAAAAAAAAAABACAAAAAAAAAAABQABAAAACsAwAAAAAAAAQCgAAAAAAAABAQAAABwA#####wEAAAAAEAAAAAAAAAAAAAAAQAgAAAAAAAAAAAUAAQAAAArALgAAAAAAAEAoAAAAAAAAAQEAAAAcAP####8BAAAAABAAAAAAAAAAAAAAAEAIAAAAAAAAAAAFAAEAAAAKwCwAAAAAAABAKAAAAAAAAAEBAAAAHAD#####AQAAAAAQAAAAAAAAAAAAAABACAAAAAAAAAAABQABAAAACsA1AAAAAAAAQCYAAAAAAAABAQAAABwA#####wEAAAAAEAAAAAAAAAAAAAAAQAgAAAAAAAAAAAUAAQAAAArANAAAAAAAAEAmAAAAAAAAAQEAAAAcAP####8BAAAAABAAAAAAAAAAAAAAAEAIAAAAAAAAAAAFAAEAAAAKwDMAAAAAAABAJgAAAAAAAAEBAAAAHAD#####AQAAAAAQAAAAAAAAAAAAAABACAAAAAAAAAAABQABAAAACsAyAAAAAAAAQCYAAAAAAAABAQAAABwA#####wEAAAAAEAAAAAAAAAAAAAAAQAgAAAAAAAAAAAUAAQAAAArAMQAAAAAAAEAmAAAAAAAAAQEAAAAcAP####8BAAAAABAAAAAAAAAAAAAAAEAIAAAAAAAAAAAFAAEAAAAKwDAAAAAAAABAJgAAAAAAAAEBAAAAHAD#####AQAAAAAQAAAAAAAAAAAAAABACAAAAAAAAAAABQABAAAACsAuAAAAAAAAQCYAAAAAAAABAQAAABwA#####wEAAAAAEAAAAAAAAAAAAAAAQAgAAAAAAAAAAAUAAQAAAArALAAAAAAAAEAmAAAAAAAAAQEAAAAcAP####8BAAAAABAAAAAAAAAAAAAAAEAIAAAAAAAAAAAFAAEAAAAKwDUAAAAAAABAJAAAAAAAAAEBAAAAHAD#####AQAAAAAQAAAAAAAAAAAAAABACAAAAAAAAAAABQABAAAACsA0AAAAAAAAQCQAAAAAAAABAQAAABwA#####wEAAAAAEAAAAAAAAAAAAAAAQAgAAAAAAAAAAAUAAQAAAArAMwAAAAAAAEAkAAAAAAAAAQEAAAAcAP####8BAAAAABAAAAAAAAAAAAAAAEAIAAAAAAAAAAAFAAEAAAAKwDIAAAAAAABAJAAAAAAAAAEBAAAAHAD#####AQAAAAAQAAAAAAAAAAAAAABACAAAAAAAAAAABQABAAAACsAxAAAAAAAAQCQAAAAAAAABAQAAABwA#####wEAAAAAEAAAAAAAAAAAAAAAQAgAAAAAAAAAAAUAAQAAAArAMAAAAAAAAEAkAAAAAAAAAQEAAAAcAP####8BAAAAABAAAAAAAAAAAAAAAEAIAAAAAAAAAAAFAAEAAAAKwC4AAAAAAABAJAAAAAAAAAEBAAAAHAD#####AQAAAAAQAAAAAAAAAAAAAABACAAAAAAAAAAABQABAAAACsAsAAAAAAAAQCQAAAAAAAABAQAAABwA#####wEAAAAAEAAAAAAAAAAAAAAAQAgAAAAAAAAAAAUAAQAAAArANQAAAAAAAEAiAAAAAAAAAQEAAAAcAP####8BAAAAABAAAAAAAAAAAAAAAEAIAAAAAAAAAAAFAAEAAAAKwDQAAAAAAABAIgAAAAAAAAEBAAAAHAD#####AQAAAAAQAAAAAAAAAAAAAABACAAAAAAAAAAABQABAAAACsAzAAAAAAAAQCIAAAAAAAABAQAAABwA#####wEAAAAAEAAAAAAAAAAAAAAAQAgAAAAAAAAAAAUAAQAAAArAMgAAAAAAAEAiAAAAAAAAAQEAAAAcAP####8BAAAAABAAAAAAAAAAAAAAAEAIAAAAAAAAAAAFAAEAAAAKwDEAAAAAAABAIgAAAAAAAAEBAAAAHAD#####AQAAAAAQAAAAAAAAAAAAAABACAAAAAAAAAAABQABAAAACsAwAAAAAAAAQCIAAAAAAAABAQAAABwA#####wEAAAAAEAAAAAAAAAAAAAAAQAgAAAAAAAAAAAUAAQAAAArALgAAAAAAAEAiAAAAAAAAAQEAAAAcAP####8BAAAAABAAAAAAAAAAAAAAAEAIAAAAAAAAAAAFAAEAAAAKwCwAAAAAAABAIgAAAAAAAAEBAAAAHAD#####AQAAAAAQAAAAAAAAAAAAAABACAAAAAAAAAAACAABAAAACsA1AAAAAAAAQCAAAAAAAAABAQAAABwA#####wEAAAAAEAAAAAAAAAAAAAAAQAgAAAAAAAAAAAUAAQAAAArANAAAAAAAAEAgAAAAAAAAAQEAAAAcAP####8BAAAAABAAAAAAAAAAAAAAAEAIAAAAAAAAAAAFAAEAAAAKwDMAAAAAAABAIAAAAAAAAAEBAAAAHAD#####AQAAAAAQAAAAAAAAAAAAAABACAAAAAAAAAAABQABAAAACsAyAAAAAAAAQCAAAAAAAAABAQAAABwA#####wEAAAAAEAAAAAAAAAAAAAAAQAgAAAAAAAAAAAUAAQAAAArAMQAAAAAAAEAgAAAAAAAAAQEAAAAcAP####8BAAAAABAAAAAAAAAAAAAAAEAIAAAAAAAAAAAFAAEAAAAKwDAAAAAAAABAIAAAAAAAAAEBAAAAHAD#####AQAAAAAQAAAAAAAAAAAAAABACAAAAAAAAAAABQABAAAACsAuAAAAAAAAQCAAAAAAAAABAQAAABwA#####wEAAAAAEAAAAAAAAAAAAAAAQAgAAAAAAAAAAAUAAQAAAArALAAAAAAAAEAgAAAAAAAAAQEAAAAcAP####8BAAAAABAAAAAAAAAAAAAAAEAIAAAAAAAAAAAHAAEAAAAKwCgAAAAAAABAKgAAAAAAAAEBAAAAHAD#####AQAAAAAQAAAAAAAAAAAAAABACAAAAAAAAAAABwABAAAACsAmAAAAAAAAQCoAAAAAAAABAf####8AAAABAAdDTWlsaWV1AP####8BAAAAABAAAAAAAAAAAAAAAEAIAAAAAAAAAAAFAAAAAHUAAAB2AAAAAwD#####AQAAAAEQAAABAAAAAQAAAHcAP#AAAAAAAAD#####AAAAAgAJQ0NlcmNsZU9SAP####8BAAAAAAAAAQAAAHcAAAABP+ZmZmZmZmYAAAAACAD#####AAAAeAAAAHkAAAAJAP####8BAAAAABAAAAAAAAAAAAAAAEAIAAAAAAAAAAAFAAEAAAB6AAAACQD#####AQAAAAAQAAAAAAAAAAAAAABACAAAAAAAAAAABQACAAAAegAAABsA#####wEAAAAAQEQAAAAAAABACAAAAAAAAAAAAAAAfBAAAAAAAAEAAAABAAAAAQAAAAAAAAAAABt1bml0w6kgZGUgbG9uZ3VldXIgKCB1LmwuICkAAAAXAP####8BAAAAABAAAAEAAAADAAAAdQAAAHYAAAAcAP####8BAAAAABAAAAAAAAAAAAAAAEAIAAAAAAAAAAAHAAEAAAAKwCgAAAAAAABAJgAAAAAAAAEBAAAAHAD#####AQAAAAAQAAAAAAAAAAAAAABACAAAAAAAAAAABwABAAAACsAmAAAAAAAAQCYAAAAAAAABAQAAABwA#####wEAAAAAEAAAAAAAAAAAAAAAQAgAAAAAAAAAAAcAAQAAAArAKAAAAAAAAEAkAAAAAAAAAQEAAAAcAP####8BAAAAABAAAAAAAAAAAAAAAEAIAAAAAAAAAAAHAAEAAAAKwCYAAAAAAABAJAAAAAAAAAEBAAAAHAD#####AQAAAAAQAAAAAAAAAAAAAABACAAAAAAAAAAABwABAAAACsAkAAAAAAAAQCYAAAAAAAABAQAAABwA#####wEAAAAAEAAAAAAAAAAAAAAAQAgAAAAAAAAAAAcAAQAAAArAKAAAAAAAAEAiAAAAAAAAAQEAAAAcAP####8BAAAAABAAAAAAAAAAAAAAAEAIAAAAAAAAAAAHAAEAAAAKwCQAAAAAAABAIgAAAAAAAAEBAAAAHAD#####AQAAAAAQAAAAAAAAAAAAAABACAAAAAAAAAAABwABAAAACsAkAAAAAAAAQCQAAAAAAAABAf####8AAAABAAlDUG9seWdvbmUA#####wEAAAAAAAADAAAABAAAAH8AAACAAAAAgQAAAH######AAAAAQAQQ1N1cmZhY2VQb2x5Z29uZQD#####AQAAAAAAAAAABQAAAIcAAAAfAP####8BAAAAAAAAAwAAAAUAAAB#AAAAgAAAAIIAAACBAAAAfwAAACAA#####wEAAAAAAAAAAAUAAACJAAAAHwD#####AQAAAAAAAAMAAAAFAAAAfwAAAIMAAACGAAAAgQAAAH8AAAAgAP####8BAAAAAAAAAAAFAAAAiwAAABwA#####wEAAAAAEAAAAAAAAAAAAAAAQAgAAAAAAAAAAAcAAQAAAArAJgAAAAAAAEAiAAAAAAAAAQEAAAAfAP####8BAAAAAAAAAwAAAAcAAACDAAAAhgAAAIIAAACNAAAAhAAAAH8AAACDAAAAIAD#####AQAAAAAAAAAABQAAAI4AAAAfAP####8BAAAAAAAAAwAAAAUAAAB#AAAAgwAAAIUAAACEAAAAfwAAACAA#####wEAAAAAAAAAAAUAAACQAAAAAgD#####AQAAAAAQAAAAAAAAAAAAAABACAAAAAAAAAAABwABQHOIAAAAAABAYFrhR64UewAAABsA#####wEAAAAAQDUAAAAAAAAAAAAAAAAAAAAAAAAAkhAAAAAAAAEAAAABAAAAAQAAAAAAAAAAABZ1bml0w6kgZCdhaXJlICggdS5hLiApAAAAFwD#####AQAAAAAQAAABAAAAAwAAADUAAAA2AAAAFwD#####AQAAAAAQAAABAAAAAwAAADYAAAA3AAAAFwD#####AQAAAAAQAAABAAAAAwAAADcAAAA4AAAAFwD#####AQAAAAAQAAABAAAAAwAAADgAAAA5AAAAFwD#####AQAAAAAQAAABAAAAAwAAADkAAAA6AAAAFwD#####AQAAAAAQAAABAAAAAwAAADoAAAA7AAAAFwD#####AQAAAAAQAAABAAAAAwAAADsAAAA8AAAAFwD#####AQAAAAAQAAABAAAAAwAAAD0AAAA+AAAAFwD#####AQAAAAAQAAABAAAAAwAAAD4AAAA#AAAAFwD#####AQAAAAAQAAABAAAAAwAAAD8AAABAAAAAFwD#####AQAAAAAQAAABAAAAAwAAAEAAAABBAAAAFwD#####AQAAAAAQAAABAAAAAwAAAEEAAABCAAAAFwD#####AQAAAAAQAAABAAAAAwAAAEIAAABDAAAAFwD#####AQAAAAAQAAABAAAAAwAAAEMAAABEAAAAFwD#####AQAAAAAQAAABAAAAAwAAAEUAAABGAAAAFwD#####AQAAAAAQAAABAAAAAwAAAEYAAABHAAAAFwD#####AQAAAAAQAAABAAAAAwAAAEcAAABIAAAAFwD#####AQAAAAAQAAABAAAAAwAAAEgAAABJAAAAFwD#####AQAAAAAQAAABAAAAAwAAAEkAAABKAAAAFwD#####AQAAAAAQAAABAAAAAwAAAEoAAABLAAAAFwD#####AQAAAAAQAAABAAAAAwAAAEsAAABMAAAAFwD#####AQAAAAAQAAABAAAAAwAAAE0AAABOAAAAFwD#####AQAAAAAQAAABAAAAAwAAAE4AAABPAAAAFwD#####AQAAAAAQAAABAAAAAwAAAE8AAABQAAAAFwD#####AQAAAAAQAAABAAAAAwAAAFAAAABRAAAAFwD#####AQAAAAAQAAABAAAAAwAAAFEAAABSAAAAFwD#####AQAAAAAQAAABAAAAAwAAAFIAAABTAAAAFwD#####AQAAAAAQAAABAAAAAwAAAFMAAABUAAAAFwD#####AQAAAAAQAAABAAAAAwAAAFUAAABWAAAAFwD#####AQAAAAAQAAABAAAAAwAAAFYAAABXAAAAFwD#####AQAAAAAQAAABAAAAAwAAAFcAAABYAAAAFwD#####AQAAAAAQAAABAAAAAwAAAFgAAABZAAAAFwD#####AQAAAAAQAAABAAAAAwAAAFkAAABaAAAAFwD#####AQAAAAAQAAABAAAAAwAAAFoAAABbAAAAFwD#####AQAAAAAQAAABAAAAAwAAAFsAAABcAAAAFwD#####AQAAAAAQAAABAAAAAwAAAF0AAABeAAAAFwD#####AQAAAAAQAAABAAAAAwAAAF4AAABfAAAAFwD#####AQAAAAAQAAABAAAAAwAAAF8AAABgAAAAFwD#####AQAAAAAQAAABAAAAAwAAAGAAAABhAAAAFwD#####AQAAAAAQAAABAAAAAwAAAGEAAABiAAAAFwD#####AQAAAAAQAAABAAAAAwAAAGIAAABjAAAAFwD#####AQAAAAAQAAABAAAAAwAAAGMAAABkAAAAFwD#####AQAAAAAQAAABAAAAAwAAAGUAAABmAAAAFwD#####AQAAAAAQAAABAAAAAwAAAGYAAABnAAAAFwD#####AQAAAAAQAAABAAAAAwAAAGcAAABoAAAAFwD#####AQAAAAAQAAABAAAAAwAAAGgAAABpAAAAFwD#####AQAAAAAQAAABAAAAAwAAAGkAAABqAAAAFwD#####AQAAAAAQAAABAAAAAwAAAGoAAABrAAAAFwD#####AQAAAAAQAAABAAAAAwAAAGsAAABsAAAAFwD#####AQAAAAAQAAABAAAAAwAAAG0AAABuAAAAFwD#####AQAAAAAQAAABAAAAAwAAAG4AAABvAAAAFwD#####AQAAAAAQAAABAAAAAwAAAG8AAABwAAAAFwD#####AQAAAAAQAAABAAAAAwAAAHAAAABxAAAAFwD#####AQAAAAAQAAABAAAAAwAAAHEAAAByAAAAFwD#####AQAAAAAQAAABAAAAAwAAAHIAAABzAAAAFwD#####AQAAAAAQAAABAAAAAwAAAHMAAAB0AAAAFwD#####AQAAAAAQAAABAAAAAwAAADUAAAA9AAAAFwD#####AQAAAAAQAAABAAAAAwAAAD0AAABFAAAAFwD#####AQAAAAAQAAABAAAAAwAAAEUAAABNAAAAFwD#####AQAAAAAQAAABAAAAAwAAAE0AAABVAAAAFwD#####AQAAAAAQAAABAAAAAwAAAFUAAABdAAAAFwD#####AQAAAAAQAAABAAAAAwAAAF0AAABlAAAAFwD#####AQAAAAAQAAABAAAAAwAAAGUAAABtAAAAFwD#####AQAAAAAQAAABAAAAAwAAADYAAAA+AAAAFwD#####AQAAAAAQAAABAAAAAwAAAD4AAABGAAAAFwD#####AQAAAAAQAAABAAAAAwAAAEYAAABOAAAAFwD#####AQAAAAAQAAABAAAAAwAAAE4AAABWAAAAFwD#####AQAAAAAQAAABAAAAAwAAAFYAAABeAAAAFwD#####AQAAAAAQAAABAAAAAwAAAF4AAABmAAAAFwD#####AQAAAAAQAAABAAAAAwAAAGYAAABuAAAAFwD#####AQAAAAAQAAABAAAAAwAAADcAAAA#AAAAFwD#####AQAAAAAQAAABAAAAAwAAAD8AAABHAAAAFwD#####AQAAAAAQAAABAAAAAwAAAEcAAABPAAAAFwD#####AQAAAAAQAAABAAAAAwAAAE8AAABXAAAAFwD#####AQAAAAAQAAABAAAAAwAAAFcAAABfAAAAFwD#####AQAAAAAQAAABAAAAAwAAAF8AAABnAAAAFwD#####AQAAAAAQAAABAAAAAwAAAGcAAABvAAAAFwD#####AQAAAAAQAAABAAAAAwAAADgAAABAAAAAFwD#####AQAAAAAQAAABAAAAAwAAAEAAAABIAAAAFwD#####AQAAAAAQAAABAAAAAwAAAEgAAABQAAAAFwD#####AQAAAAAQAAABAAAAAwAAAFAAAABYAAAAFwD#####AQAAAAAQAAABAAAAAwAAAFgAAABgAAAAFwD#####AQAAAAAQAAABAAAAAwAAAGAAAABoAAAAFwD#####AQAAAAAQAAABAAAAAwAAAGgAAABwAAAAFwD#####AQAAAAAQAAABAAAAAwAAADkAAABBAAAAFwD#####AQAAAAAQAAABAAAAAwAAAEEAAABJAAAAFwD#####AQAAAAAQAAABAAAAAwAAAEkAAABRAAAAFwD#####AQAAAAAQAAABAAAAAwAAAFEAAABZAAAAFwD#####AQAAAAAQAAABAAAAAwAAAFkAAABhAAAAFwD#####AQAAAAAQAAABAAAAAwAAAGEAAABpAAAAFwD#####AQAAAAAQAAABAAAAAwAAAGkAAABxAAAAFwD#####AQAAAAAQAAABAAAAAwAAADoAAABCAAAAFwD#####AQAAAAAQAAABAAAAAwAAAEIAAABKAAAAFwD#####AQAAAAAQAAABAAAAAwAAAEoAAABSAAAAFwD#####AQAAAAAQAAABAAAAAwAAAFIAAABaAAAAFwD#####AQAAAAAQAAABAAAAAwAAAFoAAABiAAAAFwD#####AQAAAAAQAAABAAAAAwAAAGIAAABqAAAAFwD#####AQAAAAAQAAABAAAAAwAAAGoAAAByAAAAFwD#####AQAAAAAQAAABAAAAAwAAADsAAABDAAAAFwD#####AQAAAAAQAAABAAAAAwAAAEMAAABLAAAAFwD#####AQAAAAAQAAABAAAAAwAAAEsAAABTAAAAFwD#####AQAAAAAQAAABAAAAAwAAAFMAAABbAAAAFwD#####AQAAAAAQAAABAAAAAwAAAFsAAABjAAAAFwD#####AQAAAAAQAAABAAAAAwAAAGMAAABrAAAAFwD#####AQAAAAAQAAABAAAAAwAAAGsAAABzAAAAFwD#####AQAAAAAQAAABAAAAAwAAADwAAABEAAAAFwD#####AQAAAAAQAAABAAAAAwAAAEQAAABMAAAAFwD#####AQAAAAAQAAABAAAAAwAAAEwAAABUAAAAFwD#####AQAAAAAQAAABAAAAAwAAAFQAAABcAAAAFwD#####AQAAAAAQAAABAAAAAwAAAFwAAABkAAAAFwD#####AQAAAAAQAAABAAAAAwAAAGQAAABsAAAAFwD#####AQAAAAAQAAABAAAAAwAAAGwAAAB0AAAAHAD#####AQAAAAAQAAAAAAAAAAAAAABACAAAAAAAAAAABwABAAAACsAkAAAAAAAAQCoAAAAAAAABAQAAABwA#####wEAAAAAEAAAAAAAAAAAAAAAQAgAAAAAAAAAAAgAAQAAAArAIgAAAAAAAEAqAAAAAAAAAQEAAAAXAP####8BAAAAABAAAAEAAAADAAAAdgAAAQQAAAAXAP####8BAAAAABAAAAEAAAADAAABBAAAAQUAAAAfAP####8BAAAAAAAAAQAAAAUAAAA1AAAANgAAAD4AAAA9AAAANQAAAB8A#####wEAAAAAAAABAAAABQAAADYAAAA3AAAAPwAAAD4AAAA2AAAAHwD#####AQAAAAAAAAEAAAAFAAAANwAAADgAAABAAAAAPwAAADcAAAAfAP####8BAAAAAAAAAQAAAAUAAAA4AAAAOQAAAEEAAABAAAAAOAAAAB8A#####wEAAAAAAAABAAAABQAAADkAAAA6AAAAQgAAAEEAAAA5AAAAHwD#####AQAAAAAAAAEAAAAFAAAAOgAAADsAAABDAAAAQgAAADoAAAAfAP####8BAAAAAAAAAQAAAAUAAAA7AAAAPAAAAEQAAABDAAAAOwAAAB8A#####wEAAAAAAAABAAAABQAAAD0AAAA+AAAARgAAAEUAAAA9AAAAHwD#####AQAAAAAAAAEAAAAFAAAAPgAAAD8AAABHAAAARgAAAD4AAAAfAP####8BAAAAAAAAAQAAAAUAAAA#AAAAQAAAAEgAAABHAAAAPwAAAB8A#####wEAAAAAAAABAAAABQAAAEAAAABBAAAASQAAAEgAAABAAAAAHwD#####AQAAAAAAAAEAAAAFAAAAQQAAAEIAAABKAAAASQAAAEEAAAAfAP####8BAAAAAAAAAQAAAAUAAABCAAAAQwAAAEsAAABKAAAAQgAAAB8A#####wEAAAAAAAABAAAABQAAAEMAAABEAAAATAAAAEsAAABDAAAAHwD#####AQAAAAAAAAEAAAAFAAAARQAAAEYAAABOAAAATQAAAEUAAAAfAP####8BAAAAAAAAAQAAAAUAAABGAAAARwAAAE8AAABOAAAARgAAAB8A#####wEAAAAAAAABAAAABQAAAEcAAABIAAAAUAAAAE8AAABHAAAAHwD#####AQAAAAAAAAEAAAAFAAAASAAAAEkAAABRAAAAUAAAAEgAAAAfAP####8BAAAAAAAAAQAAAAUAAABJAAAASgAAAFIAAABRAAAASQAAAB8A#####wEAAAAAAAABAAAABQAAAEoAAABLAAAAUwAAAFIAAABKAAAAHwD#####AQAAAAAAAAEAAAAFAAAASwAAAEwAAABUAAAAUwAAAEsAAAAfAP####8BAAAAAAAAAQAAAAUAAABNAAAATgAAAFYAAABVAAAATQAAAB8A#####wEAAAAAAAABAAAABQAAAE4AAABPAAAAVwAAAFYAAABOAAAAHwD#####AQAAAAAAAAEAAAAFAAAATwAAAFAAAABYAAAAVwAAAE8AAAAfAP####8BAAAAAAAAAQAAAAUAAABQAAAAUQAAAFkAAABYAAAAUAAAAB8A#####wEAAAAAAAABAAAABQAAAFEAAABSAAAAWgAAAFkAAABRAAAAHwD#####AQAAAAAAAAEAAAAFAAAAUgAAAFMAAABbAAAAWgAAAFIAAAAfAP####8BAAAAAAAAAQAAAAUAAABTAAAAVAAAAFwAAABbAAAAUwAAAB8A#####wEAAAAAAAABAAAABQAAAFUAAABWAAAAXgAAAF0AAABVAAAAHwD#####AQAAAAAAAAEAAAAFAAAAVgAAAFcAAABfAAAAXgAAAFYAAAAfAP####8BAAAAAAAAAQAAAAUAAABXAAAAWAAAAGAAAABfAAAAVwAAAB8A#####wEAAAAAAAABAAAABQAAAFgAAABZAAAAYQAAAGAAAABYAAAAHwD#####AQAAAAAAAAEAAAAFAAAAWQAAAFoAAABiAAAAYQAAAFkAAAAfAP####8BAAAAAAAAAQAAAAUAAABaAAAAWwAAAGMAAABiAAAAWgAAAB8A#####wEAAAAAAAABAAAABQAAAFsAAABcAAAAZAAAAGMAAABbAAAAHwD#####AQAAAAAAAAEAAAAFAAAAXQAAAF4AAABmAAAAZQAAAF0AAAAfAP####8BAAAAAAAAAQAAAAUAAABeAAAAXwAAAGcAAABmAAAAXgAAAB8A#####wEAAAAAAAABAAAABQAAAF8AAABgAAAAaAAAAGcAAABfAAAAHwD#####AQAAAAAAAAEAAAAFAAAAYAAAAGEAAABpAAAAaAAAAGAAAAAfAP####8BAAAAAAAAAQAAAAUAAABhAAAAYgAAAGoAAABpAAAAYQAAAB8A#####wEAAAAAAAABAAAABQAAAGIAAABjAAAAawAAAGoAAABiAAAAHwD#####AQAAAAAAAAEAAAAFAAAAYwAAAGQAAABsAAAAawAAAGMAAAAfAP####8BAAAAAAAAAQAAAAUAAABlAAAAZgAAAG4AAABtAAAAZQAAAB8A#####wEAAAAAAAABAAAABQAAAGYAAABnAAAAbwAAAG4AAABmAAAAHwD#####AQAAAAAAAAEAAAAFAAAAZwAAAGgAAABwAAAAbwAAAGcAAAAfAP####8BAAAAAAAAAQAAAAUAAABoAAAAaQAAAHEAAABwAAAAaAAAAB8A#####wEAAAAAAAABAAAABQAAAGkAAABqAAAAcgAAAHEAAABpAAAAHwD#####AQAAAAAAAAEAAAAFAAAAagAAAGsAAABzAAAAcgAAAGoAAAAfAP####8BAAAAAAAAAQAAAAUAAABrAAAAbAAAAHQAAABzAAAAawAAACAA#####wEAAAAABHN1MDAAAAAFAAABCAAAACAA#####wEAAAAABHN1MDEAAAAFAAABCQAAACAA#####wEAAAAABHN1MDIAAAAFAAABCgAAACAA#####wEAAAAABHN1MDMAAAAFAAABCwAAACAA#####wEAAAAABHN1MDQAAAAFAAABDAAAACAA#####wEAAAAABHN1MDUAAAAFAAABDQAAACAA#####wEAAAAABHN1MDYAAAAFAAABDgAAACAA#####wEAAAAABHN1MTAAAAAFAAABDwAAACAA#####wEAAAAABHN1MTEAAAAFAAABEAAAACAA#####wEAAAAABHN1MTIAAAAFAAABEQAAACAA#####wEAAAAABHN1MTMAAAAFAAABEgAAACAA#####wEAAAAABHN1MTQAAAAFAAABEwAAACAA#####wEAAAAABHN1MTUAAAAFAAABFAAAACAA#####wEAAAAABHN1MTYAAAAFAAABFQAAACAA#####wEAAAAABHN1MjAAAAAFAAABFgAAACAA#####wEAAAAABHN1MjEAAAAFAAABFwAAACAA#####wEAAAAABHN1MjIAAAAFAAABGAAAACAA#####wEAAAAABHN1MjMAAAAFAAABGQAAACAA#####wEAAAAABHN1MjQAAAAFAAABGgAAACAA#####wEAAAAABHN1MjUAAAAFAAABGwAAACAA#####wEAAAAABHN1MjYAAAAFAAABHAAAACAA#####wEAAAAABHN1MzAAAAAFAAABHQAAACAA#####wEAAAAABHN1MzEAAAAFAAABHgAAACAA#####wEAAAAABHN1MzIAAAAFAAABHwAAACAA#####wEAAAAABHN1MzMAAAAFAAABIAAAACAA#####wEAAAAABHN1MzQAAAAFAAABIQAAACAA#####wEAAAAABHN1MzUAAAAFAAABIgAAACAA#####wEAAAAABHN1MzYAAAAFAAABIwAAACAA#####wEAAAAABHN1NDAAAAAFAAABJAAAACAA#####wEAAAAABHN1NDEAAAAFAAABJQAAACAA#####wEAAAAABHN1NDIAAAAFAAABJgAAACAA#####wEAAAAABHN1NDMAAAAFAAABJwAAACAA#####wEAAAAABHN1NDQAAAAFAAABKAAAACAA#####wEAAAAABHN1NDUAAAAFAAABKQAAACAA#####wEAAAAABHN1NDYAAAAFAAABKgAAACAA#####wEAAAAABHN1NTAAAAAFAAABKwAAACAA#####wEAAAAABHN1NTEAAAAFAAABLAAAACAA#####wEAAAAABHN1NTIAAAAFAAABLQAAACAA#####wEAAAAABHN1NTMAAAAFAAABLgAAACAA#####wEAAAAABHN1NTQAAAAFAAABLwAAACAA#####wEAAAAABHN1NTUAAAAFAAABMAAAACAA#####wEAAAAABHN1NTYAAAAFAAABMQAAACAA#####wEAAAAABHN1NjAAAAAFAAABMgAAACAA#####wEAAAAABHN1NjEAAAAFAAABMwAAACAA#####wEAAAAABHN1NjIAAAAFAAABNAAAACAA#####wEAAAAABHN1NjMAAAAFAAABNQAAACAA#####wEAAAAABHN1NjQAAAAFAAABNgAAACAA#####wEAAAAABHN1NjUAAAAFAAABNwAAACAA#####wEAAAAABHN1NjYAAAAFAAABOAAAAA7##########w=='

    j3pCreeSVG(stor.lesdiv.zonefig, { id: 'mtg32svg', width: 500, height: 250 })
    stor.mtgAppLecteur.removeAllDoc()
    stor.mtgAppLecteur.addDoc('mtg32svg', txtFigure, true)
    stor.mtgAppLecteur.calculateAndDisplayAll(true)

    for (i = 0; i < 8; i++) {
      for (j = 0; j < 7; j++) {
        if (stor.lescotesHori[i][j] === 1) metVisibleH(i, j)
        if (stor.lescoteVerti[i][j] === 1) metVisibleV(i, j)
      }
    }

    for (i = 0; i < 7; i++) {
      for (j = 0; j < 7; j++) {
        if (stor.lescase[j][i]) stor.mtgAppLecteur.setVisible('mtg32svg', '#su' + i + j, true, true)
      }
    }

    if (stor.Lexo.question === 'perim' || ds.Unite2) {
      stor.mtgAppLecteur.setVisible('mtg32svg', 125, true, true)
      if (stor.Lexo.ul > 1) stor.mtgAppLecteur.setVisible('mtg32svg', 262, true, true)
      if (stor.Lexo.ul > 2) stor.mtgAppLecteur.setVisible('mtg32svg', 263, true, true)
      stor.mtgAppLecteur.setVisible('mtg32svg', 126, true, true)
    }
    if (stor.Lexo.question === 'aire' || ds.Unite2) {
      stor.mtgAppLecteur.setVisible('mtg32svg', 147, true, true)
      let nn
      switch (stor.Lexo.ua) {
        case 0.5:
          nn = 136
          break
        case 1:
          nn = 138
          break
        case 2:
          nn = 140
          break
        case 4:
          nn = 145
          break
      }
      stor.mtgAppLecteur.setVisible('mtg32svg', nn, true, true)
    }
  }

  function yaReponse () {
    if (stor.zone.reponse() === '') {
      stor.zone.focus()
      return false
    }
    if (!stor.liste.changed) {
      stor.liste.focus()
      return false
    }
    return true
  }

  function isRepOk () {
    stor.errEcrit = false
    stor.errRemdEcrit = []
    stor.errUnit = false
    stor.errCompt = false
    stor.errInv = false
    stor.errCUnit = false

    let repa, atta, attfoa, attu, attinv
    repa = stor.zone.reponse()
    const repu = stor.liste.reponse
    const tt = verifNombreBienEcrit(repa, false, true)
    if (!tt.good) {
      stor.zone.corrige(false)
      stor.errEcrit = true
      stor.errRemdEcrit.push(tt.remede)
      return false
    }
    atta = j3pArrondi(stor.Lexo.nbcase / stor.Lexo.ua, 2)
    attfoa = stor.Lexo.nbcase
    attu = 'u.a.'
    attinv = j3pArrondi(stor.nbtraits / stor.Lexo.ul, 2)
    if (stor.Lexo.question === 'perim') {
      attu = 'u.l.'
      atta = j3pArrondi(stor.nbtraits / stor.Lexo.ul, 2)
      attfoa = stor.nbtraits
      attinv = j3pArrondi(stor.Lexo.nbcase / stor.ua, 2)
    }
    repa = parseFloat(repa.replace(',', '.'))
    if (repa !== atta) {
      stor.zone.corrige(false)
      if (repa === attfoa) {
        stor.errUnit = true
      } else if (repa === attinv) {
        stor.errInv = true
      } else {
        stor.errCompt = true
      }
      return false
    }
    if (repu !== attu) {
      stor.errCUnit = true
      stor.liste.corrige(false)
      return false
    }
    return true
  }

  function desactivezone () {
    stor.zone.disable()
    stor.liste.disable()
  }

  function barrelesfaux () {
    stor.liste.barreIfKo()
    stor.zone.barreIfKo()
  }

  function mettoutvert () {
    stor.liste.corrige(true)
    stor.zone.corrige(true)
  }

  function AffCorrection (bool) {
    let buf

    if (stor.errEcrit) {
      stor.yaexplik = true
      j3pAffiche(stor.lesdiv.explications, null, stor.errRemdEcrit[0])
    }
    if (stor.errUnit) {
      buf = ' d’aire '
      stor.yaexplik = true
      if (stor.Lexo.question === 'perim') buf = ' de longueur '
      j3pAffiche(stor.lesdiv.explications, null, 'Observe bien l’unité ' + buf + ' donnée !')
    }
    if (stor.errCompt) {
      stor.yaexplik = true
      j3pAffiche(stor.lesdiv.explications, null, 'Tu n’as pas bien compté !')
    }
    if (stor.errInv) {
      stor.yaexplik = true
      j3pAffiche(stor.lesdiv.explications, null, 'Tu confonds <b>aire</b> et <b>périmètre</b>  !')
    }

    if (bool) {
      desactivezone()
      barrelesfaux()
      stor.yaco = true
      if (stor.Lexo.question === 'perim') {
        j3pAffiche(stor.lesdiv.solution, null, 'Périmètre = $' + j3pArrondi(stor.nbtraits / stor.Lexo.ul, 2) + '$ u.l.')
      } else {
        j3pAffiche(stor.lesdiv.solution, null, 'Aire = $' + j3pArrondi(stor.Lexo.nbcase / stor.Lexo.ua, 2) + '$ u.a.')
      }
    } else { me.afficheBoutonValider() }
  }

  function enonceMain () {
    if (me.etapeCourante === 1) {
      // A chaque répétition, on nettoie la scène
      // A commenter si besoin
      me.videLesZones()
      creeLesDiv()
      stor.Lexo = faisChoixExos()
      stor.lesdiv.travail.classList.add('travail')
      stor.lesdiv.consigneG.classList.add('enonce')
      stor.lesdiv.explications.classList.remove('explique')
      stor.lesdiv.solution.classList.remove('correction')
      me.zonesElts.MG.classList.add('fond')
    }
    poseQuestion()
    makefig()
    me.cacheBoutonSuite()
    me.finEnonce()
  }

  switch (this.etat) {
    case 'enonce':
      // code exécuté au lancement de la section
      if (me.debutDeLaSection) {
        initSection()
      } else {
        enonceMain()
      }
      break // case "enonce":

    case 'correction':
      // On teste si une réponse a été saisie
      stor.yaexplik = false
      if (stor.lesdiv === undefined) return
      stor.lesdiv.explications.innerHTML = ''
      stor.lesdiv.correction.innerHTML = ''
      stor.yaco = false

      if (yaReponse()) {
        // Bonne réponse
        if (isRepOk()) {
          this._stopTimer()
          stor.lesdiv.correction.style.color = this.styles.cbien
          stor.lesdiv.correction.innerHTML = cBien

          desactivezone()
          mettoutvert()

          this.cacheBoutonValider()
          this.score++

          this.typederreurs[0]++
          this.etat = 'navigation'
          this.sectionCourante()
        } else {
          // Pas de bonne réponse
          stor.lesdiv.correction.style.color = this.styles.cfaux
          stor.lesdiv.correction.innerHTML = cFaux
          if (this.isElapsed) {
            // A cause de la limite de temps :
            this._stopTimer()

            stor.lesdiv.correction.innerHTML += tempsDepasse + '<BR>'
            this.typederreurs[10]++
            this.cacheBoutonValider()

            AffCorrection(true)

            this.etat = 'navigation'
            this.sectionCourante()
          } else {
            // Réponse fausse :
            if (me.essaiCourant < me.donneesSection.nbchances) {
              stor.lesdiv.correction.innerHTML += '<br>' + essaieEncore

              AffCorrection(false)

              this.typederreurs[1]++
            } else {
              // Erreur au nème essai
              this._stopTimer()
              this.cacheBoutonValider()

              AffCorrection(true)

              this.typederreurs[2]++
              this.etat = 'navigation'
              this.sectionCourante()
            }
          }
        }
      } else {
        // pas de réponse
        stor.lesdiv.correction.style.color = this.styles.cfaux
        stor.lesdiv.correction.innerHTML = reponseIncomplete
        if (stor.encours === 'm') {
          stor.lesdiv.correction.innerHTML = 'Réponse incomplète ! <br> Tu dois déplacer le point ' + stor.C + ' .'
        }
        me.afficheBoutonValider()
      }

      if (ds.theme === 'zonesAvecImageDeFond') {
        if (stor.yaexplik) {
          stor.lesdiv.explications.classList.add('explique')
        }
        if (stor.yaco) {
          stor.lesdiv.solution.classList.add('correction')
        }
      }

      // Obligatoire
      this.finCorrection()
      break // case "correction":

    case 'navigation':
      if (!this.sectionTerminee()) {
        this.etat = 'enonce'
      }
      this.finNavigation()
      break // case "navigation":
  }
}
