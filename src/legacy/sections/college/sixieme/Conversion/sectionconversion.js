import loadJqueryDialog from 'src/lib/core/loadJqueryDialog'
import { j3pAddElt, j3pAjouteBouton, j3pArrondi, j3pEmpty, j3pEntierBienEcrit, j3pGetRandomBool, j3pGetRandomInt, j3pStyle } from 'src/legacy/core/functions'
import TableauConversionMobile from 'src/legacy/outils/tableauconversion/TableauConversionMobile'
import ZoneStyleMathquill1 from 'src/legacy/outils/zoneStyleMathquill/ZoneStyleMathquill1'
import { addDefaultTable } from 'src/legacy/themes/table'
import { getJ3pConteneur } from 'src/lib/core/domHelpers'
import { j3pAffiche } from 'src/lib/mathquill/functions'
import Dialog from 'src/lib/widgets/dialog/Dialog'
import textesGeneriques from 'src/lib/core/textes'

const { cBien, cFaux, essaieEncore, regardeCorrection, reponseIncomplete, tempsDepasse } = textesGeneriques

/**
 * Ajouter une description ici
 * @fileOverview
 * @author Tommy Barroy <t.barroy@laposte.net>
 * @since 2017-10
 */
export const params = {
  outils: ['mathquill'],
  /**
   * Ce qui est paramétrable dans l’éditeur de graphe
   * @type {LegacySectionParamsParametres}
   */
  parametres: [
    ['nbrepetitions', 9, 'entier', 'Nombre de répétitions de la section'],
    ['limite', 0, 'entier', 'Temps disponible par répétition, en secondes'],
    ['nbchances', 2, 'entier', 'Nombre d’essais possibles.'],
    ['m', true, 'boolean', '<u>true</u>: L’unité de départ peut être le mètre.'],
    ['L', true, 'boolean', '<u>true</u>: L’unité de départ peut être le litre.'],
    ['g', true, 'boolean', '<u>true</u>: L’unité de départ peut être le gramme.'],
    ['a', true, 'boolean', '<u>true</u>: L’unité de départ peut être l’are.'],
    ['mcarre', true, 'boolean', '<u>true</u>: L’unité de départ peut être le m²'],
    ['mcube', true, 'boolean', '<u>true</u>: L’unité de départ peut être le m³'],
    ['Changement', 'parfois', 'liste', '<u>toujours</u>: Si l’unité de départ est un multiple du m², l’unité d’arrivée est <i>a</i> ou <i>ha</i>. <BR> Si l’unité de départ est <i>a</i> ou <i>ha</i>, l’unité d’arrivée est un multiple du m². <BR> Si l’unité de départ est un multiple du m³, l’unité d’arrivée est un multiple du L. <BR> Si l’unité de départ est un multiple du L, l’unité d’arrivée est un multiple du m³. <BR><BR> <u>jamais</u>: Pas de changement d’unité. <BR><BR> <u>parfois</u>: Le cas peut se présenter. ', ['toujours', 'parfois', 'jamais']],
    ['Tableau', 'Virgule_Fleches_Choix', 'liste', '<u>Virgule</u>: Un clic sur une unité place automatiquement la virgule. <BR><BR> <u>Fleches</u>: L’élève déplace le nombre dans le tableau à l’aide de 2 fléches <BR><BR> <u>Clavier</u>: L’élève peut écrire des chiffres dans tableau <BR><BR> <u>Choix</u>: L’élève doit choisir le tableau de conversion qui convient. <BR><BR> <u>Visible</u>: Le tableau n’est pas intéractif <BR><BR> <u>Aucun</u>: Pas de tableau.', ['Aucun', 'visible', 'visible_Choix', 'Clavier', 'Clavier_Choix', 'Virgule_Clavier', 'Virgule_Clavier_Choix', 'Fleches', 'Fleches_Choix', 'Virgule_Fleches', 'Virgule_Fleches_Choix']],
    ['ZerosInutiles', 'Non_Acceptés', 'liste', '<u>Acceptés</u> ou <u>Non_Acceptés</u>', ['Acceptés', 'Non_Acceptés']],
    ['Espace', true, 'boolean', '<u>true</u>: Les espaces entre chaque groupe de 3 sont exigés.'],
    // le modèle gère la compatibilité ascendante avec l’ancien paramètre MiseEnPagePlusClaire
    ['theme', 'standard', 'liste', 'Modèle pour l’affichage', ['standard', 'zonesAvecImageDeFond']]
  ],
  // dans le cas d’une section QUANTITATIVE
  pe: [
    { pe_1: 'Maitrise' },
    { pe_2: 'Suffisant' },
    { pe_3: 'Suffisant - erreur dans l’écriture des nombres' },
    { pe_4: 'Insuffisant' },
    { pe_5: 'Insuffisant - erreur dans l’écriture des nombres' },
    { pe_6: 'Très insuffisant' },
    { pe_7: 'Très insuffisant - erreur dans l’écriture des nombres' }

  ]
}

/**
 * section conversion
 * @this {Parcours}
 */
export default function main () {
  const me = this
  let ds = me.donneesSection
  const stor = this.storage

  function yareponse () {
    if (me.isElapsed) {
      return true
    }
    return (stor.mazonem.reponse() !== '')
  }

  function estBonneReponse () {
    me.ErrPlusieursVirgule = false
    me.ErrVirguleSeule = false
    me.ErrEspace = false
    me.ErrDecimaleEspace = false
    me.ErrZeroInutile = false

    me.repEleve = stor.mazonem.reponse()
    if (me.repEleve === '') {
      stor.mazonem.majaffiche('0')
      me.repEleve = '0'
    }
    // test si y’a bien qu’une virgule
    if (me.repEleve.indexOf(',') !== me.repEleve.lastIndexOf(',')) {
      me.ErrPlusieursVirgule = true
      return false
    }
    // test si les espaces sont pas strange
    // on tolere plusieurs espaces entre 2 grp
    // à partir de virgule, ou a partir de la droite
    // ca doit etre des groupes de 3 (sauf le dernier)
    // ou alors tout colle et espace a osef

    if (me.repEleve.indexOf(',') !== -1) {
      me.partieentiereEncours = me.repEleve.substring(0, me.repEleve.indexOf(','))
      me.yavirguleEncours = true
      me.partiedecimaleEncours = me.repEleve.substring(me.repEleve.indexOf(',') + 1)
    } else {
      me.partieentiereEncours = me.repEleve
      me.yavirguleEncours = false
      me.partiedecimaleEncours = ''
    }

    // test si les espaces sont pas strange
    // à partir de virgule, ou a partir de la droite
    // ca doit etre des groupes de 3 (sauf le dernier)
    // ou alors tout colle et espace a osef
    if ((me.yavirguleEncours) && (me.partiedecimaleEncours.replace(/ /g, '').length === 0)) {
      me.ErrVirguleSeule = true
      return false
    }

    let yaesp = true
    let comptgrp = 0
    let comptgrpold = 3
    let ing = false
    for (let i = me.partieentiereEncours.length - 1; i > -1; i--) {
      if (me.partieentiereEncours[i] === ' ') {
        if (yaesp) {
          if (i === 0) {
            me.ErrespVirg = true
            return false
          } else {
            me.Err2esp = true
            return false
          }
        }
        yaesp = true
        if (ing) {
          comptgrpold = comptgrp
        }
        ing = false
        comptgrp = 0
      } else {
        yaesp = false
        comptgrp++
        ing = true
        if ((comptgrpold !== 3) || ((comptgrp > 3) && (ds.Espace))) {
          me.ErrEspace = true
          return false
        }
      }
    }

    yaesp = true
    comptgrp = 0
    comptgrpold = 3
    ing = false
    for (let i = 0; i < me.partiedecimaleEncours.length; i++) {
      if (me.partiedecimaleEncours[i] === ' ') {
        if (yaesp) {
          if (i === 0) {
            me.ErrespVirg = true
            return false
          } else {
            me.Err2esp = true
            return false
          }
        }
        yaesp = true
        if (ing) {
          comptgrpold = comptgrp
        }
        ing = false
        comptgrp = 0
      } else {
        yaesp = false
        comptgrp++
        ing = true
        if ((comptgrpold !== 3) || ((comptgrp > 3) && (ds.Espace))) {
          me.ErrDecimaleEspace = true
          return false
        }
      }
    }
    // test si ya oublié zero inut
    //
    for (let i = 0; i < me.partieentiereEncours.length; i++) {
      if (me.partieentiereEncours[i] !== ' ') {
        if (me.partieentiereEncours[i] === '0') {
          if (me.partieentiereEncours.length !== 1) {
            me.ErrZeroInutile = true
            return false
          }
        } else {
          break
        }
      }
    }
    for (let i = me.partiedecimaleEncours.length - 1; i > -1; i--) {
      if (me.partiedecimaleEncours[i] !== ' ') {
        if (me.partiedecimaleEncours[i] === '0') {
          me.ErrZeroInutile = true
          return false
        } else {
          break
        }
      }
    }
    // recolle tout
    // eslint-disable-next-line no-irregular-whitespace
    me.repEleve2 = me.repEleve.replace(/ /g, '')
    me.repEleve2 = me.repEleve2.replace(',', '.')
    me.repEleve2 = parseFloat(me.repEleve2)

    return (Math.abs(stor.repattendue - me.repEleve2) < Math.pow(10, -12))
  } // estBonneReponse

  function affAide () {
    if (!stor.dialog) return console.error(Error('Pas de dialog disponible pour l’afficher'))
    stor.dialog.toggle()
  }

  function changetab () {
    stor.numtab++
    if (stor.numtab === 7) {
      stor.numtab = 0
    }
    const monut = stor.lestab[stor.numtab][0]
    const fotrad = stor.lestab[stor.numtab][1]
    stor.tablo.disaClav()
    j3pEmpty(stor.zoneTableau[0][0])
    stor.tablo = new TableauConversionMobile(stor.zoneTableau[0][0], {
      unite: monut,
      are: fotrad,
      litre: fotrad,
      tonne: false,
      virgule: ds.Virgule,
      fleches: ds.fleches,
      nb: stor.nbdepart,
      clavier: ds.clavier,
      mepact: getJ3pConteneur(me.zonesElts.MG)
    })
  }

  function ecrisBienMathquill (nb) {
    let monc = j3pEntierBienEcrit(Math.trunc(nb)).replace(/ /g, '\\text{ }')
    if ((nb - Math.trunc(nb)) > 0) {
      let madec = (j3pArrondi((nb - Math.trunc(nb)), 10) + '').replace('0.', '')
      if (madec.length > 3) {
        madec = madec.substring(0, 3) + '\\text{ }' + madec.substring(3)
      }
      if (madec.length > 14) {
        madec = madec.substring(0, 14) + '\\text{ }' + madec.substring(14)
      }
      if (madec.length > 25) {
        madec = madec.substring(0, 25) + '\\text{ }' + madec.substring(25)
      }
      monc += (',' + madec)
    }
    return monc
  }

  function getDonnees () {
    return {
      typesection: 'college',

      // Nombre de répétitions de l’exercice
      nbrepetitions: 9,
      nbetapes: 1,
      nbchances: 2,

      m: true,
      L: true,
      g: true,
      a: true,
      mcarre: true,
      mcube: true,
      Tableau: 'VirguleMobile_Fleches',
      ZerosInutiles: 'Non_Acceptés',
      Espace: true,
      Changement: 'parfois',

      pe_1: 'Maitrise',
      pe_2: 'Suffisant',
      pe_3: 'Suffisant - erreur dans l’écriture des nombres',
      pe_4: 'Insuffisant',
      pe_5: 'Insuffisant - erreur dans l’écriture des nombres',
      pe_6: 'Très insuffisant',
      pe_7: 'Très insuffisant - erreur dans l’écriture des nombres',

      // Nombre de chances dont dispose l’él§ve pour répondre à la question

      structure: 'presentation1bis', //  || "presentation2" || "presentation3"  || "presentation1bis"

      // Paramètres de la section, avec leurs valeurs par défaut.

      /*
          Les textes présents dans la section
          Sont donc accessibles dans le code par donneesSection.textes.phrase1
          Possibilité d’écrire dans le code :
          var lestextes : donneesSection.textes;
          puis accès à l’aide de lestextes.phrase1
          */
      textes: {
        consigne: 'Complète la conversion ci-dessous',
        consigne2: '$£c$ £a $=$ ',
        ZeroInut: 'Ta réponse est juste, mais tu n’as pas supprimé les zéros inutiles. <BR> La réponse attendue est $£a$.',
        Tableaufleche: 'déplacer le nombre',
        TableauInserer: 'Copier le nombre dans le tableau',
        soluce: '$£c$ £e = $£a$ £d .',
        ZeroInut2: 'Tu dois supprimer les zéros inutiles !',
        ZeroInut3: 'Je te conseille de supprimer les zéros inutiles.',
        mult: ['k', 'h', 'da', '', 'd', 'c', 'm'],
        errtropvirgule: 'Il ne peut y avoir plusieurs virgules dans un nombre !',
        errvirguleseule: 'Il ne peut pas y avoir de virgule sans partie décimale !',
        errespace: 'Dans la partie entière, les chiffres doivent être regroupés par 3 en commençant par la droite !',
        errespacedec: 'Dans la partie décimale, les chiffres doivent être regroupés par 3 en commençant par la gauche !',
        errDeprang2: 'Supprime les zéros inutiles !'
      },
      pe: 0
    }
  }

  async function initSection () {
    // faut s’assurer que jQuery-ui est chargé
    await loadJqueryDialog()
    ds = getDonnees()
    me.donneesSection = ds

    me.surcharge()
    ds.pe = [ds.pe_1, ds.pe_2, ds.pe_3, ds.pe_4, ds.pe_5, ds.pe_6, ds.pe_7]
    ds.nbitems = ds.nbetapes * ds.nbrepetitions
    // Construction de la page
    me.construitStructurePage(ds.structure)

    stor.unitesPossibles = []
    if (ds.m) {
      stor.unitesPossibles.push('m')
    }
    if (ds.L) {
      stor.unitesPossibles.push('L')
    }
    if (ds.g) {
      stor.unitesPossibles.push('g')
    }
    if (ds.mcarre) {
      stor.unitesPossibles.push('m²')
    }
    if (ds.a) {
      stor.unitesPossibles.push('a')
    }
    if (ds.mcube) {
      stor.unitesPossibles.push('m³')
    }
    if (stor.unitesPossibles.length === 0) {
      stor.unitesPossibles.push('m')
    }

    me.typederreurs = [0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0]

    let titre = 'Conversion d’unités'
    if (stor.unitesPossibles.length === 1) {
      switch (stor.unitesPossibles[0]) {
        case 'm':
          titre += ' de longueur'
          break
        case 'g':
          titre += ' de masse'
          break
        case 'L':
          titre += ' de capacité'
          break
        case 'm²':
        case 'a' :
          titre += ' d’aire'
          break
        case 'm³':
          titre += ' de volume'
          break
      }
    }
    me.afficheTitre(titre)
    ds.Virgule = ds.Tableau.indexOf('irgule') !== -1
    ds.fleches = ds.Tableau.indexOf('eche') !== -1
    ds.clavier = ds.Tableau.indexOf('avier') !== -1
    enonceMain()
  }

  function enonceMain () {
    // A chaque répétition, on nettoie la scène
    // A commenter si besoin
    if (stor.dialog) {
      stor.dialog.destroy()
      stor.dialog = null
    }
    if (stor.tabloCo !== undefined) stor.tabloCo.out = true
    me.videLesZones()

    stor.tabloCo = undefined

    stor.lesdiv = {}
    stor.lesdiv.conteneur = j3pAddElt(me.zonesElts.MG, 'div', {
      style: me.styles.etendre('toutpetit.enonce', { padding: '10px' })
    })
    stor.lesdiv.consigne = addDefaultTable(stor.lesdiv.conteneur, 1, 1)[0][0]
    stor.lesdiv.travail = addDefaultTable(stor.lesdiv.conteneur, 1, 1)[0][0]
    const tta = addDefaultTable(stor.lesdiv.travail, 1, 3)
    stor.lesdiv.travail1 = tta[0][0]
    stor.lesdiv.travail2 = tta[0][1]
    stor.lesdiv.travail3 = tta[0][2]
    stor.lesdiv.explications = j3pAddElt(stor.lesdiv.conteneur, 'div')
    stor.lesdiv.solution = j3pAddElt(stor.lesdiv.conteneur, 'div')
    stor.lesdiv.bouton = j3pAddElt(me.zonesElts.MD, 'div', {
      style: me.styles.etendre('toutpetit.enonce', { padding: '10px' })
    })
    stor.lesdiv.correction = j3pAddElt(me.zonesElts.MD, 'div', {
      style: me.styles.etendre('toutpetit.enonce', { padding: '10px' })
    })
    stor.lesdiv.explications.style.color = me.styles.cfaux
    stor.lesdiv.solution.style.color = me.styles.toutpetit.correction.color

    let fotrad
    // on tire au sort une unité
    stor.monUnite = stor.unitesPossibles[j3pGetRandomInt(0, (stor.unitesPossibles.length - 1))]
    switch (stor.monUnite) {
      case 'm':
        stor.monUniteArrivee = 'm'
        stor.mult = 1
        fotrad = false
        break
      case 'g':
        stor.monUniteArrivee = 'g'
        stor.mult = 1
        fotrad = false
        break
      case 'L':
        stor.monUniteArrivee = 'L'
        stor.mult = 1
        fotrad = false
        if ((ds.Changement === 'toujours') || ((ds.Changement === 'parfois') && (j3pGetRandomBool()))) {
          stor.monUniteArrivee = 'm³'
          fotrad = true
          stor.mult = 3
        }
        break
      case 'm²':
        stor.monUniteArrivee = 'm²'
        stor.mult = 2
        fotrad = false
        if ((ds.Changement === 'toujours') || ((ds.Changement === 'parfois') && (j3pGetRandomBool()))) {
          stor.monUniteArrivee = 'a'
          fotrad = true
        }
        break
      case 'a' :
        stor.monUniteArrivee = 'a'
        fotrad = false
        stor.mult = 2
        if ((ds.Changement === 'toujours') || ((ds.Changement === 'parfois') && (j3pGetRandomBool()))) {
          stor.monUniteArrivee = 'm²'
          fotrad = true
        }
        break
      case 'm³':
        stor.monUniteArrivee = 'm³'
        fotrad = false
        stor.mult = 3
        if ((ds.Changement === 'toujours') || ((ds.Changement === 'parfois') && (j3pGetRandomBool()))) {
          stor.monUniteArrivee = 'L'
          fotrad = true
        }
        break
    }

    stor.RangUniteI = j3pGetRandomInt(0, 6)
    if (stor.monUnite === 'a') {
      if (j3pGetRandomInt(0, 2) === 0) {
        stor.RangUniteI = 1
      } else {
        if (j3pGetRandomBool()) {
          stor.RangUniteI = 3
        } else {
          stor.RangUniteI = 5
        }
      }
    }
    if (stor.monUnite === 'L') {
      stor.RangUniteI = j3pGetRandomInt(1, 6)
    }
    if ((stor.monUnite === 'm³') && (stor.monUniteArrivee === 'L')) {
      stor.RangUniteI = j3pGetRandomInt(3, 6)
    }
    if ((stor.monUnite === 'm²') && (stor.monUniteArrivee === 'a')) {
      stor.RangUniteI = j3pGetRandomInt(1, 4)
    }
    stor.RangUnite = ds.textes.mult[stor.RangUniteI]

    do {
      stor.RangUniteArriveeI = j3pGetRandomInt(0, 6)
      if (stor.monUniteArrivee === 'm²') {
        if (stor.monUnite === 'a') {
          if (stor.RangUniteI === 1) {
            stor.RangUniteArriveeI = j3pGetRandomInt(0, 4)
          }
          if (stor.RangUniteI === 3) {
            stor.RangUniteArriveeI = j3pGetRandomInt(0, 5)
          }
          if (stor.RangUniteI === 5) {
            stor.RangUniteArriveeI = j3pGetRandomInt(1, 6)
          }
        } else {
          stor.RangUniteArriveeI = j3pGetRandomInt(Math.max(0, stor.RangUniteI - 2), Math.min(6, stor.RangUniteI + 3))
        }
      }
      if (stor.monUniteArrivee === 'm³') {
        if (stor.monUnite === 'L') {
          stor.RangUniteArriveeI = j3pGetRandomInt(3, 6)
          stor.RangUniteArriveeI = 3
        } else {
          stor.RangUniteArriveeI = j3pGetRandomInt(Math.max(0, stor.RangUniteI - 2), Math.min(6, stor.RangUniteI + 2))
        }
      }
      if (stor.monUniteArrivee === 'a') {
        if (j3pGetRandomInt(0, 2) === 0) {
          stor.RangUniteArriveeI = 1
        } else {
          if (j3pGetRandomBool()) {
            stor.RangUniteArriveeI = 3
          } else {
            stor.RangUniteArriveeI = 5
          }
        }
      }
      if (stor.monUniteArrivee === 'L') {
        stor.RangUniteArriveeI = j3pGetRandomInt(1, 6)
        if (stor.monUnite === 'm³') {
          if (stor.RangUniteI === 6) {
            stor.RangUniteArriveeI = j3pGetRandomInt(3, 6)
          }
        }
      }
      stor.RangUniteArrivee = ds.textes.mult[stor.RangUniteArriveeI]
    }
    while ((stor.RangUniteArriveeI === stor.RangUniteI) && (stor.monUniteArrivee === stor.monUnite))

    let i1, i2
    i1 = stor.RangUniteArriveeI
    i2 = stor.RangUniteI
    if (stor.monUniteArrivee !== stor.monUnite) {
      switch (stor.monUniteArrivee) {
        case 'm²':
          i1 = (i1 - 1) * 2 + 1
          break
        case 'a':
          i2 = (i2 - 1) * 2 + 1
          break
        case 'm³':
          i1 = (i1 - 4) * 3 + 3
          break
        case 'L':
          i2 = (i2 - 4) * 3 + 3
          break
      }
    } else {
      if (stor.monUniteArrivee === 'm²') {
        i1 = 2 * i1
        i2 = 2 * i2
      }
      if (stor.monUniteArrivee === 'm³') {
        i1 = 3 * i1
        i2 = 3 * i2
      }
    }
    const dec = i2 - i1

    stor.nbdepart = j3pArrondi(j3pGetRandomInt(101, 999) * Math.pow(10, Math.max(j3pGetRandomInt(-4, 1), stor.RangUniteI - stor.RangUniteArriveeI - 8)), 5)
    if ((stor.monUniteArrivee === 'm³') && (stor.monUnite === 'm³')) {
      if (stor.RangUniteI - stor.RangUniteArriveeI > 1) {
        stor.nbdepart = j3pArrondi(j3pGetRandomInt(101, 999) * Math.pow(10, j3pGetRandomInt(-2, 1)), 5)
      }
    }
    if ((stor.monUniteArrivee === 'm³') && (stor.monUnite === 'L')) {
      if ((stor.RangUniteArriveeI === 3) && (stor.RangUniteI === 6)) {
        stor.nbdepart = j3pArrondi(j3pGetRandomInt(101, 999) * Math.pow(10, j3pGetRandomInt(-2, 1)), 5)
      }
      if ((stor.RangUniteArriveeI === 3) && (stor.RangUniteI === 5)) {
        stor.nbdepart = j3pArrondi(j3pGetRandomInt(101, 999) * Math.pow(10, j3pGetRandomInt(-3, 1)), 5)
      }
    }
    if ((stor.monUniteArrivee === 'L') && (stor.monUnite === 'm³')) {
      if ((stor.RangUniteI === 6) && (stor.RangUniteArriveeI === 3)) {
        stor.nbdepart = j3pArrondi(j3pGetRandomInt(101, 999) * Math.pow(10, j3pGetRandomInt(-2, 1)), 5)
      }
      if ((stor.RangUniteI === 6) && (stor.RangUniteArriveeI === 4)) {
        stor.nbdepart = j3pArrondi(j3pGetRandomInt(101, 999) * Math.pow(10, j3pGetRandomInt(-3, 1)), 5)
      }
      if ((stor.RangUniteI === 5) && (stor.RangUniteArriveeI === 1)) {
        stor.nbdepart = j3pArrondi(j3pGetRandomInt(101, 999) * Math.pow(10, j3pGetRandomInt(-3, 1)), 5)
      }
    }
    if ((stor.monUniteArrivee === 'a') && (stor.monUnite === 'm²')) {
      if ((stor.RangUniteI === 4) && (stor.RangUniteArriveeI === 1)) {
        stor.nbdepart = j3pArrondi(j3pGetRandomInt(101, 999) * Math.pow(10, j3pGetRandomInt(-2, 1)), 5)
      }
    }

    stor.monc = ecrisBienMathquill(stor.nbdepart)

    j3pAffiche(stor.lesdiv.consigne, null, '<b>' + ds.textes.consigne + '</b>')
    j3pAffiche(stor.lesdiv.travail1, null, ds.textes.consigne2,
      {
        c: stor.monc,
        a: stor.RangUnite + stor.monUnite
      })
    j3pAffiche(stor.lesdiv.travail3, null, '&nbsp;' + stor.RangUniteArrivee + stor.monUniteArrivee)
    stor.mazonem = new ZoneStyleMathquill1(stor.lesdiv.travail2, {
      restric: '0123456789,. ',
      limitenb: 24,
      limite: 1000,
      enter: me.sectionCourante.bind(me)
    })

    if (ds.Tableau !== 'Aucun') {
      let { width } = me.conteneur.getBoundingClientRect()
      width = Math.min(width * 0.9, 720)
      stor.dialog = new Dialog({ title: 'Tableau de Conversion', width })
    }

    if (ds.Tableau !== 'Aucun') {
      if (!stor?.dialog?.container) return console.error(Error('aucun dialog existant pour y mettre le tableau'))
      stor.zoneTableau = addDefaultTable(stor.dialog.container, 2, 1)
      j3pAjouteBouton(stor.lesdiv.bouton, 'TableauxBOUBOUT', 'MepBoutons', 'Tableaux', affAide)
      let monut = stor.monUnite
      if ((stor.monUnite === 'L') && (fotrad)) {
        monut = 'm³'
      }
      if (stor.monUnite === 'a') {
        monut = 'm²'
        fotrad = true
      }
      if (ds.Tableau.indexOf('Choix') !== -1) {
        stor.lestab = [['m', false], ['g', false], ['L', false], ['m²', false], ['m²', true], ['m³', false], ['m³', true]]
        stor.numtab = j3pGetRandomInt(0, 5)
        monut = stor.lestab[stor.numtab][0]
        fotrad = stor.lestab[stor.numtab][1]
        j3pAjouteBouton(stor.zoneTableau[1][0], 'unboutchange', 'MepBoutons', 'Changer de tableau', changetab)
      }

      stor.tablo = new TableauConversionMobile(stor.zoneTableau[0][0], {
        unite: monut,
        are: fotrad,
        litre: fotrad,
        tonne: false,
        virgule: ds.Virgule,
        fleches: ds.fleches,
        nb: stor.nbdepart,
        clavier: ds.clavier,
        mepact: getJ3pConteneur(me.zonesElts.MG)
      })
      j3pStyle(stor.zoneTableau[0][0], { fontSize: '120%' })
    }

    /// détermine stor.repattendue
    if (dec !== 0) {
      stor.repattendue = '0000000000000000000' + stor.nbdepart
      let pv = stor.repattendue.indexOf('.')
      if (pv === -1) pv = stor.repattendue.length
      stor.repattendue = stor.repattendue.replace('.', '')
      stor.repattendue += '0000000000000000'
      pv = pv - dec
      stor.repattendue = stor.repattendue.substring(0, pv) + '.' + stor.repattendue.substring(pv)
      stor.repattendue = parseFloat(stor.repattendue)
    } else {
      stor.repattendue = stor.nbdepart
    }

    /// fin determine repeattendue
    stor.lesdiv.travail.classList.add('travail')
    stor.lesdiv.consigne.classList.add('enonce')
    me.zonesElts.MG.classList.add('fond')
    if (ds.Tableau !== 'Aucun') {
      stor.zoneTableau[0][0].classList.add('enonce')
    }
    me.finEnonce()
  }

  function affcofo (bool) {
    stor.mazonem.corrige(false)
    stor.lesdiv.explications.innerHTML = ''
    if (me.ErrPlusieursVirgule) {
      me.typederreurs[6]++
      stor.yaexplik = true
      j3pAffiche(stor.lesdiv.explications, null, ds.textes.errtropvirgule)
    }
    if (me.ErrVirguleSeule) {
      me.typederreurs[6]++
      stor.yaexplik = true
      j3pAffiche(stor.lesdiv.explications, null, ds.textes.errvirguleseule)
    }
    if (me.ErrEspace) {
      me.typederreurs[6]++
      stor.yaexplik = true
      j3pAffiche(stor.lesdiv.explications, null, ds.textes.errespace)
    }
    if (me.ErrDecimaleEspace) {
      me.typederreurs[6]++
      stor.yaexplik = true
      j3pAffiche(stor.lesdiv.explications, null, ds.textes.errespacedec)
    }
    if (me.ErrZeroInutile) {
      me.typederreurs[6]++
      stor.yaexplik = true
      if (ds.ZerosInutiles === 'Non_Acceptés') {
        j3pAffiche(stor.lesdiv.explications, null, ds.textes.ZeroInut2)
      } else {
        j3pAffiche(stor.lesdiv.explications, null, ds.textes.ZeroInut3)
      }
    }

    if (bool) {
      stor.yaexplik = true
      const mona = ecrisBienMathquill(stor.repattendue)
      j3pAffiche(stor.lesdiv.solution, null, ' \n' + ds.textes.soluce,
        {
          a: mona,
          c: stor.monc,
          e: stor.RangUnite + stor.monUnite,
          d: stor.RangUniteArrivee + stor.monUniteArrivee
        })

      /// afiche tabco
      // fais le tab
      let monut = stor.monUnite
      let fotrad = (stor.monUniteArrivee !== stor.monUnite)
      if ((stor.monUnite === 'L') && (fotrad)) {
        monut = 'm³'
      }
      if (stor.monUnite === 'a') {
        monut = 'm²'
        fotrad = true
      }

      stor.tabloCo = new TableauConversionMobile(stor.lesdiv.explications, {
        unite: monut,
        are: fotrad,
        litre: fotrad,
        tonne: false,
        isco: true,
        mepact: getJ3pConteneur(me.zonesElts.MG),
        nb: stor.nbdepart,
        uniteDep: { unit: stor.monUnite, rang: stor.RangUnite },
        uniteFin: { unit: stor.monUniteArrivee, rang: stor.RangUniteArrivee }
      })
      stor.yasso = true
      if (ds.fleches || ds.clavier) {
        stor.tablo.disable()
      }
      if (ds.Tableau.indexOf('Choix') !== -1) {
        j3pEmpty(stor.zoneTableau[1][0])
      }
      stor.mazonem.barre()
      stor.mazonem.disable()
    }
  }

  switch (me.etat) {
    case 'enonce':
      // code exécuté au lancement de la section
      if (me.debutDeLaSection) {
        initSection()
      } else {
        enonceMain()
      }

      break // case "enonce":

    case 'correction':
      // On teste si une réponse a été saisie

      stor.yaexplik = false
      stor.yasso = false
      stor.lesdiv.explications.classList.remove('explication')

      if (!yareponse()) {
        // ici on donne le message à afficher si une ou plusieurs zone(s) n’est pas complétée (tout d’abord on met en rouge : styles.cfaux)
        stor.lesdiv.correction.style.color = this.styles.cfaux
        stor.lesdiv.correction.innerHTML = reponseIncomplete
        stor.mazonem.focus()
        return this.finCorrection()
      }

      // Bonne réponse
      if (estBonneReponse()) {
        this.score++
        stor.lesdiv.correction.style.color = this.styles.cbien
        stor.lesdiv.correction.innerHTML = cBien
        stor.lesdiv.explications.innerHTML = ''

        stor.mazonem.disable()
        stor.mazonem.corrige(true)

        if (ds.fleches || ds.clavier) {
          stor.tablo.disable()
        }
        if (ds.Tableau.indexOf('Choix') !== -1) {
          j3pEmpty(stor.zoneTableau[1][0])
        }

        const mona = ecrisBienMathquill(stor.repattendue)
        if (me.ErrZeroInutile) {
          stor.yaexplik = true
          j3pAffiche(stor.lesdiv.explications, null, ds.textes.ZeroInut,
            { a: mona })
        }
        this.typederreurs[0]++
        return this.finCorrection('navigation', true)
      }

      // Pas de bonne réponse
      stor.lesdiv.correction.style.color = this.styles.cfaux
      if (stor.yasso) stor.lesdiv.solution.classList.add('correction')
      if (stor.yaexplik) stor.lesdiv.explications.classList.add('explique')

      // A cause de la limite de temps :
      if (this.isElapsed) { // limite de temps
        stor.lesdiv.correction.innerHTML = tempsDepasse
        this.typederreurs[10]++
        affcofo(true)
        return this.finCorrection('navigation', true)
      }

      // KO sans limite de temps
      stor.lesdiv.correction.innerHTML = cFaux

      if (this.essaiCourant < this.donneesSection.nbchances) {
        // il reste des essais
        stor.lesdiv.correction.innerHTML += '<br>' + essaieEncore
        stor.lesdiv.correction.style.color = this.styles.cfaux
        stor.lesdiv.explications.innerHTML = ''
        this.typederreurs[1]++
        affcofo(false)
        return this.finCorrection()
      }

      // erreur au dernier essai
      affcofo(true)
      stor.lesdiv.correction.innerHTML += '<br>' + regardeCorrection
      this.typederreurs[2]++
      this.finCorrection('navigation', true)
      break // case "correction":

    case 'navigation':
      if (this.sectionTerminee()) {
        const sco = this.score / this.donneesSection.nbitems
        let peibase = 0
        if (sco >= 0.8) peibase = 0
        if ((sco >= 0.55) && (sco < 0.8)) peibase = 1
        if ((sco >= 0.3) && (sco < 0.55)) peibase = 3
        if (sco < 0.3) peibase = 5

        let peisup = 0
        const compt = this.typederreurs[2] / 4
        if (this.typederreurs[6] > compt) peisup = 1
        this.parcours.pe = this.donneesSection.pe[peibase + peisup]
      } else {
        this.etat = 'enonce'
      }
      this.finNavigation(true)
      break // case "navigation":
  }
}
