import { j3pAddContent, j3pAddElt, j3pAjouteBouton, j3pArrondi, j3pBarre, j3pDetruit, j3pEmpty, j3pGetRandomBool, j3pGetRandomInt } from 'src/legacy/core/functions'
import { j3pAffiche } from 'src/lib/mathquill/functions'
import { ecrisBienMathquill } from 'src/legacy/outils/zoneStyleMathquill/functions'
import ZoneStyleMathquill1 from 'src/legacy/outils/zoneStyleMathquill/ZoneStyleMathquill1'
import { addDefaultTable } from 'src/legacy/themes/table'
import TableauConversionMobile from 'src/legacy/outils/tableauconversion/TableauConversionMobile'
import textesGeneriques from 'src/lib/core/textes'

const { cBien, cFaux, essaieEncore, regardeCorrection, reponseIncomplete, tempsDepasse } = textesGeneriques

/**
 * Ajouter une description ici
 * @fileOverview
 * @author Tommy Barroy <t.barroy@laposte.net>
 * @since 2017-10
*/

export const params = {
  outils: ['mathquill'],
  /**
   * Ce qui est paramétrable dans l’éditeur de graphe
   * @type {LegacySectionParamsParametres}
   */
  parametres: [
    ['nbrepetitions', 9, 'entier', 'Nombre de répétitions de la section'],
    ['limite', 0, 'entier', 'Temps disponible par répétition, en secondes'],
    ['m', true, 'boolean', '<u>true</u>: L’unité peut être le mètre.'],
    ['L', true, 'boolean', '<u>true</u>: L’unité peut être le litre.'],
    ['g', true, 'boolean', '<u>true</u>: L’unité peut être le gramme.'],
    ['a', true, 'boolean', '<u>true</u>: L’unité peut être l’are.'],
    ['mcarre', true, 'boolean', '<u>true</u>: L’unité peut être le m²'],
    ['mcube', true, 'boolean', '<u>true</u>: L’unité peut être le m³'],
    ['Changement', 'parfois', 'liste', '<u>toujours</u>: Si l’unité de départ est un multiple du m², l’unité d’arrivée est <i>a</i> ou <i>ha</i>. <BR> Si l’unité de départ est <i>a</i> ou <i>ha</i> , l’unité d’arrivée est un multiple du m². <BR> Si l’unité de départ est un multiple du m³ , l’unité d’arrivée est un multiple du L. <BR> Si l’unité de départ est un multiple du L , l’unité d’arrivée est un multiple du m³. <BR><BR> <u>jamais</u>: Pas de changement d’unité. <BR><BR> <u>parfois</u>: Le cas peut se présenter. ', ['toujours', 'parfois', 'jamais']],
    ['ZerosInutiles', 'Non_Acceptés', 'liste', '<u>Accpetés</u> ou <u>Non_Acceptés</u>', ['Acceptés', 'Non_Acceptés']],
    ['Espace', true, 'boolean', '<u>true</u>: Les espaces entre chaque groupe sont exigés.'],
    // le modèle gère la compatibilité ascendante avec l’ancien paramètre MiseEnPagePlusClaire
    ['theme', 'standard', 'liste', 'Modèle pour l’affichage', ['standard', 'zonesAvecImageDeFond']]
  ],
  // dans le cas d’une section QUANTITATIVE
  pe: [
    { pe_1: 'Maitrise' },
    { pe_2: 'Suffisant' },
    { pe_3: 'Suffisant - erreur dans l’écriture des nombres' },
    { pe_4: 'Insuffisant' },
    { pe_5: 'Insuffisant - erreur dans l’écriture des nombres' },
    { pe_6: 'Très insuffisant' },
    { pe_7: 'Très insuffisant - erreur dans l’écriture des nombres' }

  ]

}

/**
 * section conversionbase
 * @this {Parcours}
 */
export default function main () {
  const me = this
  const stor = me.storage
  let ds = me.donneesSection

  function changetab () {
    stor.numtab++
    if (stor.numtab === 7) { stor.numtab = 0 };
    j3pEmpty(stor.lesdiv.tab)
    stor.tablo = new TableauConversionMobile(stor.lesdiv.tab, { unite: stor.lestab[stor.numtab][0], are: stor.lestab[stor.numtab][1], litre: stor.lestab[stor.numtab][1], tonne: false })
  }

  function cotab (bool) {
    const cool = bool ? me.styles.cbien : me.styles.cfaux
    stor.tablo.changeCool(cool)
  }

  function cotab2 (bool) {
    j3pEmpty(stor.tablo.tableCells[2][0])
    const cool = bool ? me.styles.cbien : me.styles.cfaux
    for (let i = 0; i < stor.tablo.listcases.length; i++) {
      stor.tablo.listcases[i].style.color = cool
      if (!bool) {
        j3pAffiche(stor.tablo.listcasesco[i], null, '$0$')
        stor.tablo.listcasesco[i].style.textAlign = 'center'
        if (i > 3 && i < stor.tablo.listcasesco.length - 4) {
          stor.tablo.listcasesco[i].style.background = '#fff'
          stor.tablo.listcasesco[i].style.border = '1px solid black'
        }
        stor.tablo.listcasesco[i].style.color = me.styles.petit.correction.color
      }
    }
    if (!bool) {
      stor.tablo.insere3()
    }
  }

  function yareponse () {
    if ((me.questionCourante % me.donneesSection.nbetapes) === 1) { return true };
    if ((me.questionCourante % me.donneesSection.nbetapes) === 2) { return true }
    if ((me.questionCourante % me.donneesSection.nbetapes) === 3) { return true };
    if ((me.questionCourante % me.donneesSection.nbetapes) === 4) { return true };
    if (me.isElapsed) { return true }
    return (stor.mazonem.reponse() !== '')
  }
  function chiffresdes (n, rang) {
    return j3pArrondi(j3pArrondi(Math.trunc(j3pArrondi(n / Math.pow(10, rang), 7)), 0) - j3pArrondi(Math.trunc(j3pArrondi(n / Math.pow(10, rang + 1), 7)) * 10, 0), 0)
  }
  function isRepOk () {
    if ((me.questionCourante % me.donneesSection.nbetapes) === 1) {
      const pos = []
      switch (stor.Monunite) {
        case 'm':pos.push(['m', false])
          break
        case 'g':pos.push(['g', false])
          break
        case 'L': pos.push(['m³', true])
          if (stor.MonuniteArrivee === 'L') {
            pos.push(['L', false])
          }
          break
        case 'a':pos.push(['m²', true])
          break
        case 'm²':pos.push(['m²', true])
          if (stor.MonuniteArrivee === 'm²') {
            pos.push(['m²', false])
          }
          break
        case 'm³':pos.push(['m³', true])
          if (stor.MonuniteArrivee === 'm³') {
            pos.push(['m³', false])
          }
          break
      }
      let ok = false
      for (var i = 0; i < pos.length; i++) {
        ok = ok || ((pos[i][0] === stor.lestab[stor.numtab][0]) && (pos[i][1] === stor.lestab[stor.numtab][1]))
      }
      return ok
    }

    if ((me.questionCourante % me.donneesSection.nbetapes) === 2) {
      if (stor.tablo.virgMem === undefined) return false
      const tAtt = (stor.Monunite !== 'a') && (stor.Monunite !== 'L' || ((stor.Monunite === 'L') && (stor.MonuniteArrivee === 'L')))
      return tAtt === stor.tablo.virgMem.t && stor.RangUniteI === stor.tablo.virgMem.num
    }

    if ((me.questionCourante % me.donneesSection.nbetapes) === 3) {
      stor.lavirgOld = stor.tablo.lavirg
      return (stor.tablo.plnb === stor.tablo.bnPLa())
    }

    if ((me.questionCourante % me.donneesSection.nbetapes) === 4) {
      if (stor.tablo.virgMem === undefined) return false
      const tAtt = (stor.MonuniteArrivee !== 'a') && (stor.MonuniteArrivee !== 'L' || ((stor.Monunite === 'L') && (stor.MonuniteArrivee === 'L')))
      return tAtt === stor.tablo.virgMem.t && stor.RangUniteArriveeI === stor.tablo.virgMem.num
    }

    stor.ErrPlusieursVirgule = false
    stor.ErrVirguleSeule = false
    stor.ErrEspace = false
    stor.ErrDecimaleEspace = false
    stor.ErrZeroInutile = false

    stor.repEleve = stor.mazonem.reponse()
    if (stor.repEleve === '') { stor.mazonem.majaffiche('0'); stor.repEleve = '0' }
    // test si y’a bien qu’une virgule
    if (stor.repEleve.indexOf(',') !== stor.repEleve.lastIndexOf(',')) {
      stor.ErrPlusieursVirgule = true
      return false
    }
    // test si les espaces sont pas strange
    // on tolere plusieurs espaces entre 2 grp
    // à partir de virgule, ou a partir de la droite
    // ca doit etre des groupes de 3 (sauf le dernier)
    // ou alors tout colle et espace a osef

    if (stor.repEleve.indexOf(',') !== -1) {
      stor.partieentiereEncours = stor.repEleve.substring(0, stor.repEleve.indexOf(','))
      stor.yavirguleEncours = true
      stor.partiedecimaleEncours = stor.repEleve.substring(stor.repEleve.indexOf(',') + 1)
    } else {
      stor.partieentiereEncours = stor.repEleve
      stor.yavirguleEncours = false
      stor.partiedecimaleEncours = ''
    }

    // test si les espaces sont pas strange
    // à partir de virgule, ou a partir de la droite
    // ca doit etre des groupes de 3 (sauf le dernier)
    // ou alors tout colle et espace a osef
    if ((stor.yavirguleEncours) && (stor.partiedecimaleEncours.replace(/ /g, '').length === 0)) {
      stor.ErrVirguleSeule = true
      return false
    }

    let yaesp = true
    let comptgrp = 0
    let comptgrpold = 3
    let ing = false
    for (i = stor.partieentiereEncours.length - 1; i > -1; i--) {
      if (stor.partieentiereEncours[i] === ' ') {
        if (yaesp) {
          if (i === 0) {
            stor.ErrespVirg = true
            return false
          } else {
            stor.Err2esp = true
            return false
          }
        }
        yaesp = true
        if (ing) { comptgrpold = comptgrp }
        ing = false
        comptgrp = 0
      } else {
        yaesp = false
        comptgrp++
        ing = true
        if ((comptgrpold !== 3) || ((comptgrp > 3) && (me.donneesSection.Espace))) {
          stor.ErrEspace = true
          return false
        }
      }
    }

    yaesp = true
    comptgrp = 0
    comptgrpold = 3
    ing = false
    for (i = 0; i < stor.partiedecimaleEncours.length; i++) {
      if (stor.partiedecimaleEncours[i] === ' ') {
        if (yaesp) {
          if (i === 0) {
            stor.ErrespVirg = true
            return false
          } else {
            stor.Err2esp = true
            return false
          }
        }
        yaesp = true
        if (ing) { comptgrpold = comptgrp }
        ing = false
        comptgrp = 0
      } else {
        yaesp = false
        comptgrp++
        ing = true
        if ((comptgrpold !== 3) || ((comptgrp > 3) && (me.donneesSection.Espace))) {
          stor.ErrDecimaleEspace = true
          return false
        }
      }
    }
    // test si ya oublié zero inut
    //
    for (i = 0; i < stor.partieentiereEncours.length; i++) {
      if (stor.partieentiereEncours[i] !== ' ') {
        if (stor.partieentiereEncours[i] === '0') {
          if (stor.partieentiereEncours.length !== 1) {
            stor.ErrZerosInutTrop = true
            return false
          }
        } else { break }
      }
    }
    for (i = stor.partiedecimaleEncours.length - 1; i > -1; i--) {
      if (stor.partiedecimaleEncours[i] !== ' ') {
        if (stor.partiedecimaleEncours[i] === '0') {
          stor.ErrZerosInutTrop = true
          return false
        } else { break }
      }
    }
    // recolle tout
    // eslint-disable-next-line no-irregular-whitespace
    stor.repEleve2 = stor.repEleve.replace(/ /g, '')
    stor.repEleve2 = stor.repEleve2.replace(',', '.')
    stor.repEleve2 = parseFloat(stor.repEleve2)

    return (Math.abs(stor.repattendue - stor.repEleve2) < Math.pow(10, -12))
  }

  function getDonnees () {
    return {

      // Nombre de répétitions de l’exercice
      nbrepetitions: 9,
      nbetapes: 5,
      nbchances: 1,

      m: true,
      L: true,
      g: true,
      a: true,
      mcarre: true,
      mcube: true,
      ZerosInutiles: 'Non_Acceptés',
      Espace: true,
      Changement: 'parfois',

      pe_1: 'Maitrise',
      pe_2: 'Suffisant',
      pe_3: 'Suffisant - erreur dans l’écriture des nombres',
      pe_4: 'Insuffisant',
      pe_5: 'Insuffisant - erreur dans l’écriture des nombres',
      pe_6: 'Très insuffisant',
      pe_7: 'Très insuffisant - erreur dans l’écriture des nombres',

      // Si indication !== "" alors un lien "Indication" s’affiche dans la zone Inférieure Gauche (,zones.IG
      // une indication<br>de deux lignes...";
      indication: '',

      // pour le cas de presentation1bis
      // En effet pas d’indication possible pour cette présentation
      // ,surchargeindication: false;

      // Nombre de chances dont dispose l’él§ve pour répondre à la question

      structure: 'presentation1bis', //  || "presentation2" || "presentation3"  || "presentation1bis"

      // Paramètres de la section, avec leurs valeurs par défaut.

      /*
          Les textes présents dans la section
          Sont donc accessibles dans le code par ds.textes.phrase1
          Possibilité d’écrire dans le code :
          var lestextes: ds.textes;
          puis accès à l’aide de lestextes.phrase1
          */
      textes: {
        couleurexplique: '#A9E2F3',
        couleurcorrec: '#A9F5A9',
        couleurenonce: '#D8D8D8',
        couleurenonceg: '#F3E2A9',
        couleuretiquettes: '#A9E2F3',
        consigne: 'Nous voulons convertir $£c$ £a <b> en </b> £b .',
        Etape1: 'Etape 1: Sélectionne le bon tableau de conversion',
        Etape1Co: 'Tu dois trouver un tableau dans lequel figure l’unité <b>£a</b> !',
        Etape1Co2: 'Il fallait choisir le tableau ci-dessous: <BR> <BR>',
        Etape2: 'Etape 2: Clique sur l’unité de départ dans le tableau',
        Etape3: 'Etape 3: Place correctement le nombre dans le tableau',
        Etape4: 'Etape 4: Clique sur l’unité d’arrivée dans le tableau',
        Etape5: 'Etape 5: Complète la conversion ci-dessous',
        consigne2: '$£c$ £a $=$ ',
        ZeroInut: 'Ta réponse est juste, mais tu n’as pas supprimé les zéros inutiles. <BR> La réponse attendue est $£a$.',
        Tableaufleche: 'déplacer le nombre',
        TableauInserer: 'Copier le nombre dans le tableau',
        AideVirgule: 'Clique sur une unité pour faire apparaître sa virgule',
        RepUnit: "Dans $£c$ , le chiffre des unités est $£a$. <BR> C’est ce chiffre que tu dois placer dans la case 'unité' des £b !",
        soluce: '$£c$ £e = $£a$ £d .',
        ZeroInut2: 'Tu dois supprimer les zéros inutiles !',
        ZeroInut3: 'Je te conseille de supprimer les zéros inutiles.',
        mult: ['k', 'h', 'da', '', 'd', 'c', 'm'],
        errtropvirgule: 'Il ne peut y avoir plusieurs virgules dans un nombre !',
        errvirguleseule: 'Il ne peut pas y avoir de virgule sans partie décimale !',
        errespace: 'Dans la partie entière, les chiffres doivent être regroupés par 3 en commençant par la droite !',
        errespacedec: 'Dans la partie décimale, les chiffres doivent être regroupés par 3 en commençant par la gauche !',
        errDeprang2: 'Supprime les zéros inutiles !'
      },

      // Par défaut, pas de temps limite. Décommentez pour avoir un temps limite par défaut. En secondes.

      // ,limite: 10;
      /*
          Phrase d’état renvoyée par la section
          Si ,pe=0 alors la section est quantitative et renvoie un score entre 0 et 1.
          Dans le cas où la section est qualitative, lister les différents pe renvoyées :
              ,pe_1: "toto"
              ,pe_2: "tata"
              etc
          Dans la partie navigation, il faudra affecter la variable ,parcours.pe à l’une des précédentes.
          Par exemple,
          ,parcours.pe: ds.pe_2
          */
      pe: 0
    }
  }

  function initSection () {
    me.donneesSection = getDonnees()
    ds = me.donneesSection
    stor.avir = []
    ds.nbitems = ds.nbetapes * ds.nbrepetitions

    me.surcharge()
    ds.pe = [ds.pe_1, ds.pe_2, ds.pe_3, ds.pe_4, ds.pe_5, ds.pe_6, ds.pe_7]

    // Construction de la page
    me.construitStructurePage(ds.structure)

    stor.Unitepos = []
    if (ds.m) { stor.Unitepos.push('m') };
    if (ds.L) { stor.Unitepos.push('L') };
    if (ds.g) { stor.Unitepos.push('g') };
    if (ds.mcarre) { stor.Unitepos.push('m²') }
    if (ds.a) { stor.Unitepos.push('a') };
    if (ds.mcube) { stor.Unitepos.push('m³') };
    if (stor.Unitepos.length === 0) { stor.Unitepos.push('m') };

    me.typederreurs = [0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0]

    me.score = 0

    let ajtitre = ''
    if (stor.Unitepos.length === 1) {
      switch (stor.Unitepos[0]) {
        case 'm':ajtitre = ' de longueur'
          break
        case 'g':ajtitre = ' de masse'
          break
        case 'L':ajtitre = ' de capacité'
          break
        case 'm²':
        case 'a' :ajtitre = ' d’aire'
          break
        case 'm³':ajtitre = ' de volume'
          break
      }
    }

    me.afficheTitre('conversionbase d’unités' + ajtitre)

    if (ds.indication) me.indication(me.zones.IG, ds.indication)
    enonceMain()
  }

  function enonceMain () {
    if (stor.avir.length > 0) {
      for (let i = stor.avir.length - 1; i > -1; i--) {
        j3pDetruit(stor.avir.pop())
      }
    }
    if ((me.questionCourante % ds.nbetapes) === 1) {
      me.videLesZones()
    }

    document.onselectstart = function () { return false }
    stor.ongoingTouches = []

    if ((me.questionCourante % ds.nbetapes) === 1) {
      stor.lesdiv = {}
      stor.lesdiv.conteneur = j3pAddElt(me.zonesElts.MG, 'div', {
        style: me.styles.etendre('toutpetit.enonce', { padding: '10px' })
      })
      let tt = addDefaultTable(stor.lesdiv.conteneur, 2, 1)
      stor.lesdiv.consigne = tt[0][0]
      j3pAddContent(tt[1][0], '<br>')
      stor.lesdiv.consigne2 = addDefaultTable(stor.lesdiv.conteneur, 1, 1)[0][0]
      stor.lesdiv.tableau = addDefaultTable(stor.lesdiv.conteneur, 1, 1)[0][0]
      stor.lesdiv.consigne3 = addDefaultTable(stor.lesdiv.conteneur, 1, 1)[0][0]
      stor.lesdiv.travail = addDefaultTable(stor.lesdiv.conteneur, 1, 1)[0][0]
      tt = addDefaultTable(stor.lesdiv.travail, 1, 3)
      stor.lesdiv.travail1 = tt[0][0]
      stor.lesdiv.travail2 = tt[0][1]
      stor.lesdiv.travail3 = tt[0][2]
      stor.lesdiv.explications = j3pAddElt(stor.lesdiv.conteneur, 'div')
      stor.lesdiv.solution = j3pAddElt(stor.lesdiv.conteneur, 'div')
      stor.lesdiv.correction = j3pAddElt(me.zonesElts.MD, 'div', {
        style: me.styles.etendre('toutpetit.enonce', { padding: '10px' })
      })
      stor.lesdiv.explications.style.color = me.styles.cfaux
      stor.lesdiv.solution.style.color = me.styles.toutpetit.correction.color

      stor.Monunite = stor.Unitepos[j3pGetRandomInt(0, (stor.Unitepos.length - 1))]
      let fotrad
      switch (stor.Monunite) {
        case 'm':stor.MonuniteArrivee = 'm'
          stor.mult = 1
          fotrad = false
          break
        case 'g':stor.MonuniteArrivee = 'g'
          stor.mult = 1
          fotrad = false
          break
        case 'L':stor.MonuniteArrivee = 'L'
          stor.mult = 1
          fotrad = false
          if ((me.donneesSection.Changement === 'toujours') || ((me.donneesSection.Changement === 'parfois') && (j3pGetRandomBool()))) {
            stor.MonuniteArrivee = 'm³'
            fotrad = true
            stor.mult = 3
          }
          break
        case 'm²':stor.MonuniteArrivee = 'm²'
          stor.mult = 2
          fotrad = false
          if ((me.donneesSection.Changement === 'toujours') || ((me.donneesSection.Changement === 'parfois') && (j3pGetRandomBool()))) {
            stor.MonuniteArrivee = 'a'
            fotrad = true
          }
          break
        case 'a' :stor.MonuniteArrivee = 'a'
          fotrad = false
          stor.mult = 2
          if ((me.donneesSection.Changement === 'toujours') || ((me.donneesSection.Changement === 'parfois') && (j3pGetRandomBool()))) {
            stor.MonuniteArrivee = 'm²'
            fotrad = true
          }
          break
        case 'm³':stor.MonuniteArrivee = 'm³'
          fotrad = false
          stor.mult = 3
          if ((me.donneesSection.Changement === 'toujours') || ((me.donneesSection.Changement === 'parfois') && (j3pGetRandomBool()))) {
            stor.MonuniteArrivee = 'L'
            fotrad = true
          }
          break
      }

      stor.RangUniteI = j3pGetRandomInt(0, 6)
      if (stor.Monunite === 'a') {
        if (j3pGetRandomInt(0, 2) === 0) { stor.RangUniteI = 1 } else { if (j3pGetRandomBool()) { stor.RangUniteI = 3 } else { stor.RangUniteI = 5 } }
      }
      if (stor.Monunite === 'L') { stor.RangUniteI = j3pGetRandomInt(1, 6) }
      if ((stor.Monunite === 'm³') && (stor.MonuniteArrivee === 'L')) { stor.RangUniteI = j3pGetRandomInt(3, 6) }
      if ((stor.Monunite === 'm²') && (stor.MonuniteArrivee === 'a')) { stor.RangUniteI = j3pGetRandomInt(1, 4) }
      stor.RangUnite = me.donneesSection.textes.mult[stor.RangUniteI]

      do {
        stor.RangUniteArriveeI = j3pGetRandomInt(0, 6)
        if (stor.MonuniteArrivee === 'm²') {
          if (stor.Monunite === 'a') {
            if (stor.RangUniteI === 1) { stor.RangUniteArriveeI = j3pGetRandomInt(0, 4) }
            if (stor.RangUniteI === 3) { stor.RangUniteArriveeI = j3pGetRandomInt(0, 5) }
            if (stor.RangUniteI === 5) { stor.RangUniteArriveeI = j3pGetRandomInt(1, 6) }
          } else {
            stor.RangUniteArriveeI = j3pGetRandomInt(Math.max(0, stor.RangUniteI - 2), Math.min(6, stor.RangUniteI + 3))
          }
        }
        if (stor.MonuniteArrivee === 'm³') {
          if (stor.Monunite === 'L') {
            stor.RangUniteArriveeI = j3pGetRandomInt(3, 6)
            stor.RangUniteArriveeI = 3
          } else {
            stor.RangUniteArriveeI = j3pGetRandomInt(Math.max(0, stor.RangUniteI - 2), Math.min(6, stor.RangUniteI + 2))
          }
        }
        if (stor.MonuniteArrivee === 'a') {
          if (j3pGetRandomInt(0, 2) === 0) { stor.RangUniteArriveeI = 1 } else { if (j3pGetRandomBool()) { stor.RangUniteArriveeI = 3 } else { stor.RangUniteArriveeI = 5 } }
        }
        if (stor.MonuniteArrivee === 'L') {
          stor.RangUniteArriveeI = j3pGetRandomInt(1, 6)
          if (stor.Monunite === 'm³') {
            if (stor.RangUniteI === 6) { stor.RangUniteArriveeI = j3pGetRandomInt(3, 6) }
          }
        }
        stor.RangUniteArrivee = me.donneesSection.textes.mult[stor.RangUniteArriveeI]
      }
      while ((stor.RangUniteArriveeI === stor.RangUniteI) && (stor.MonuniteArrivee === stor.Monunite))

      stor.nbdepart = j3pArrondi(j3pGetRandomInt(101, 999) * Math.pow(10, Math.max(j3pGetRandomInt(-4, 1), stor.RangUniteI - stor.RangUniteArriveeI - 8)), 5)
      if ((stor.MonuniteArrivee === 'm³') && (stor.Monunite === 'm³')) {
        if (stor.RangUniteI - stor.RangUniteArriveeI > 1) { stor.nbdepart = j3pArrondi(j3pGetRandomInt(101, 999) * Math.pow(10, j3pGetRandomInt(-2, 1)), 5) }
      }
      if ((stor.MonuniteArrivee === 'm³') && (stor.Monunite === 'L')) {
        if ((stor.RangUniteArriveeI === 3) && (stor.RangUniteI === 6)) { stor.nbdepart = j3pArrondi(j3pGetRandomInt(101, 999) * Math.pow(10, j3pGetRandomInt(-2, 1)), 5) }
        if ((stor.RangUniteArriveeI === 3) && (stor.RangUniteI === 5)) { stor.nbdepart = j3pArrondi(j3pGetRandomInt(101, 999) * Math.pow(10, j3pGetRandomInt(-3, 1)), 5) }
      }
      if ((stor.MonuniteArrivee === 'L') && (stor.Monunite === 'm³')) {
        if ((stor.RangUniteI === 6) && (stor.RangUniteArriveeI === 3)) { stor.nbdepart = j3pArrondi(j3pGetRandomInt(101, 999) * Math.pow(10, j3pGetRandomInt(-2, 1)), 5) }
        if ((stor.RangUniteI === 6) && (stor.RangUniteArriveeI === 4)) { stor.nbdepart = j3pArrondi(j3pGetRandomInt(101, 999) * Math.pow(10, j3pGetRandomInt(-3, 1)), 5) }
        if ((stor.RangUniteI === 5) && (stor.RangUniteArriveeI === 1)) { stor.nbdepart = j3pArrondi(j3pGetRandomInt(101, 999) * Math.pow(10, j3pGetRandomInt(-3, 1)), 5) }
      }

      stor.monc = ecrisBienMathquill(stor.nbdepart)

      j3pAffiche(stor.lesdiv.consigne, null, '<b>' + me.donneesSection.textes.consigne + '</b>',
        { c: stor.monc, a: stor.RangUnite + stor.Monunite, b: stor.RangUniteArrivee + stor.MonuniteArrivee })
      j3pAffiche(stor.lesdiv.consigne2, null, '<b><u>' + me.donneesSection.textes.Etape1 + '</u></b>',
        { c: stor.monc, a: stor.RangUnite + stor.Monunite, b: stor.RangUniteArrivee + stor.MonuniteArrivee })

      const tt2 = addDefaultTable(stor.lesdiv.tableau, 1, 3)
      stor.lesdiv.tab = tt2[0][0]
      tt2[0][1].style.width = '20px'
      stor.lesdiv.tabChange = tt2[0][2]

      stor.lestab = [['m', false], ['g', false], ['L', false], ['m²', false], ['m²', true], ['m³', false], ['m³', true]]
      stor.numtab = j3pGetRandomInt(0, 6)
      const monut = stor.lestab[stor.numtab][0]
      fotrad = stor.lestab[stor.numtab][1]
      j3pAjouteBouton(stor.lesdiv.tabChange, changetab, { id: 'unboutchange', className: 'MepBoutons', value: 'Changer de tableau' })

      me.donneesSection.Tableau = '1'
      stor.tablo = new TableauConversionMobile(stor.lesdiv.tab, { unite: monut, are: fotrad, litre: fotrad, tonne: false })

      /// détermine stor.repattendue
      stor.repattendue = j3pArrondi(stor.nbdepart * j3pArrondi(Math.pow(10, (stor.RangUniteArriveeI - stor.RangUniteI) * stor.mult), 10), 10)
      if (stor.Monunite === 'a') {
        if (stor.MonuniteArrivee === 'a') {
          stor.repattendue = j3pArrondi(stor.nbdepart * j3pArrondi(Math.pow(10, (stor.RangUniteArriveeI - stor.RangUniteI)), 10), 10)
        } else {
          var rangtrans = Math.trunc(0.5 * stor.RangUniteI + 0.5)
          stor.repattendue = j3pArrondi(stor.nbdepart * j3pArrondi(Math.pow(10, (stor.RangUniteArriveeI - rangtrans) * 2), 10), 10)
        }
      }
      if ((stor.Monunite === 'L') && (stor.MonuniteArrivee === 'm³')) {
        if (stor.RangUniteI > 3) { rangtrans = 5 } else { rangtrans = 4 };
        var mod = 0
        if ((stor.RangUniteI === 1) || (stor.RangUniteI === 4)) { mod = 2 };
        if ((stor.RangUniteI === 2) || (stor.RangUniteI === 5)) { mod = 1 };
        stor.repattendue = j3pArrondi(stor.nbdepart * j3pArrondi(Math.pow(10, (stor.RangUniteArriveeI - rangtrans) * 3 + mod), 10), 10)
      }
      if ((stor.Monunite === 'm²') && (stor.MonuniteArrivee === 'a')) {
        rangtrans = Math.trunc(0.5 * stor.RangUniteArriveeI + 0.5)
        stor.repattendue = j3pArrondi(stor.nbdepart * j3pArrondi(Math.pow(10, (rangtrans - stor.RangUniteI) * 2), 10), 10)
      }
      if ((stor.Monunite === 'm³') && (stor.MonuniteArrivee === 'L')) {
        if (stor.RangUniteArriveeI > 3) { rangtrans = 5 } else { rangtrans = 4 };
        mod = 0
        if ((stor.RangUniteArriveeI === 1) || (stor.RangUniteArriveeI === 4)) { mod = 2 };
        if ((stor.RangUniteArriveeI === 2) || (stor.RangUniteArriveeI === 5)) { mod = 1 };
        stor.repattendue = j3pArrondi(stor.nbdepart * j3pArrondi(Math.pow(10, (rangtrans - stor.RangUniteI) * 3 - mod), 10), 10)
      }

      me.zonesElts.MG.classList.add('fond')
      stor.lesdiv.consigne.classList.add('enonce')
      stor.lesdiv.consigne2.classList.add('enonce')
      stor.lesdiv.tableau.classList.add('travail')
    }

    if ((me.questionCourante % ds.nbetapes) === 2) {
      stor.lesdiv.explications.innerHTML = ''
      stor.lesdiv.solution.innerHTML = ''
      stor.lesdiv.consigne2.innerHTML = ''
      stor.lesdiv.correction.innerHTML = ''
      j3pAffiche(stor.lesdiv.consigne2, null, '<b><u>' + me.donneesSection.textes.Etape2 + '</u></b>',
        { c: stor.monc, a: stor.RangUnite + stor.Monunite, b: stor.RangUniteArrivee + stor.MonuniteArrivee })
      stor.lesdiv.tableau.innerHTML = ''
      me.donneesSection.Tableau = '2'
      stor.tablo = new TableauConversionMobile(stor.lesdiv.tableau, { unite: stor.unittab, are: stor.fotradtab, litre: stor.fotradtab, tonne: false, virgule: true, funcVirg: () => me.sectionCourante(), nb: stor.nbdepart })
    }

    if ((me.questionCourante % ds.nbetapes) === 3) {
      stor.lesdiv.explications.innerHTML = ''
      stor.lesdiv.solution.innerHTML = ''
      stor.lesdiv.consigne2.innerHTML = ''
      stor.lesdiv.correction.innerHTML = ''
      me.donneesSection.Tableau = '3'
      j3pAffiche(stor.lesdiv.consigne2, null, '<b><u>' + me.donneesSection.textes.Etape3 + '</u></b>',
        { c: stor.monc, a: stor.RangUnite + stor.Monunite, b: stor.RangUniteArrivee + stor.MonuniteArrivee })

      stor.tablo.vireFauxVirg()
      stor.tablo.directInsere()
    }

    if ((me.questionCourante % ds.nbetapes) === 4) {
      stor.lesdiv.explications.innerHTML = ''
      stor.lesdiv.solution.innerHTML = ''
      stor.lesdiv.consigne2.innerHTML = ''
      stor.lesdiv.correction.innerHTML = ''
      j3pAffiche(stor.lesdiv.consigne2, null, '<b><u>' + me.donneesSection.textes.Etape4 + '</u></b>',
        { c: stor.monc, a: stor.RangUnite + stor.Monunite, b: stor.RangUniteArrivee + stor.MonuniteArrivee })
      j3pEmpty(stor.lesdiv.tableau)
      stor.tablo = new TableauConversionMobile(stor.lesdiv.tableau, { unite: stor.unittab, are: stor.fotradtab, litre: stor.fotradtab, tonne: false, virgule: true, funcVirg: () => me.sectionCourante(), nb: stor.nbdepart, lavirgOld: stor.lavirgOld })
      stor.tablo.directInsere(true)
    }

    if ((me.questionCourante % ds.nbetapes) === 0) {
      stor.lesdiv.explications.innerHTML = ''
      stor.lesdiv.solution.innerHTML = ''
      stor.lesdiv.consigne2.innerHTML = ''
      stor.lesdiv.correction.innerHTML = ''
      stor.lesdiv.tableau.classList.remove('travail')
      stor.lesdiv.tableau.classList.add('enonce')
      stor.lesdiv.travail.classList.add('travail')
      stor.tablo.vireFauxVirg()
      j3pAffiche(stor.lesdiv.consigne2, null, '<b><u>' + me.donneesSection.textes.Etape5 + '</u></b>',
        { c: stor.monc, a: stor.RangUnite + stor.Monunite, b: stor.RangUniteArrivee + stor.MonuniteArrivee })
      j3pAffiche(stor.lesdiv.travail1, null, me.donneesSection.textes.consigne2,
        { c: stor.monc, a: stor.RangUnite + stor.Monunite })
      j3pAffiche(stor.lesdiv.travail3, null, '&nbsp;' + stor.RangUniteArrivee + stor.MonuniteArrivee)
      stor.mazonem = new ZoneStyleMathquill1(stor.lesdiv.travail2, { restric: '0123456789,. ', limitenb: 24, limite: 1000, enter: () => { me.sectionCourante() } })
    }

    me.finEnonce()
    if ((me.questionCourante % ds.nbetapes) === 2) me.cacheBoutonValider()
    if ((me.questionCourante % ds.nbetapes) === 4) me.cacheBoutonValider()
    stor.lesdiv.solution.classList.remove('correction')
    stor.lesdiv.explications.classList.remove('explique')
  }

  function affcofo () {
    stor.yasso = false
    stor.yaexplik = false
    if ((me.questionCourante % ds.nbetapes) === 1) {
      let unitoubli = ''
      stor.yasso = true
      stor.yaexplik = true
      if ((stor.Monunite === 'a') || (stor.Monunite === 'g') || ((stor.Monunite === 'm'))) { unitoubli = stor.Monunite } else if (stor.Monunite === 'm²') {
        if (stor.lestab[stor.numtab][0] !== 'm²') {
          unitoubli = 'm²'
        } else { unitoubli = 'a' }
      } else if (stor.Monunite === 'm³') {
        if (stor.lestab[stor.numtab][0] !== 'm³') {
          unitoubli = 'm³'
        } else { unitoubli = 'L' }
      } else if (stor.Monunite === 'L') {
        if (stor.MonuniteArrivee === 'L') { unitoubli = 'L' } else {
          if (stor.lestab[stor.numtab][0] !== 'm³') {
            unitoubli = 'm³'
          } else { unitoubli = 'L' }
        }
      }

      j3pAffiche(stor.lesdiv.explications, null, me.donneesSection.textes.Etape1Co,
        { a: unitoubli })
      stor.lesdiv.tabChange.innerHTML = ''
      cotab(false)
      j3pAffiche(stor.lesdiv.solution, null, me.donneesSection.textes.Etape1Co2)
      stor.unittab = stor.Monunite
      stor.fotradtab = (stor.MonuniteArrivee !== stor.Monunite)
      if ((stor.Monunite === 'L') && (stor.fotradtab)) { stor.unittab = 'm³' }
      if (stor.Monunite === 'a') { stor.unittab = 'm²'; stor.fotradtab = true }
      stor.tablo = new TableauConversionMobile(stor.lesdiv.solution, { unite: stor.unittab, are: stor.fotradtab, litre: stor.fotradtab, tonne: false })
      stor.avir.push(j3pBarre(stor.lesdiv.tab))
    }

    if ((me.questionCourante % ds.nbetapes) === 2) {
      if (stor.tablo.virgMem !== undefined) stor.tablo.colorVirg(false)
      const tAtt = (stor.Monunite !== 'a') && (stor.Monunite !== 'L' || ((stor.Monunite === 'L') && (stor.MonuniteArrivee === 'L')))
      if (tAtt) {
        stor.tablo.virgMem = { t: true, num: stor.RangUniteI }
      } else {
        if (stor.Monunite === 'a') {
          switch (stor.RangUniteI) {
            case 1: stor.tablo.virgMem = { t: false, num1: 1, num2: 1 }
              break
            case 3: stor.tablo.virgMem = { t: false, num1: 2, num2: 1 }
              break
            case 5: stor.tablo.virgMem = { t: false, num1: 3, num2: 1 }
              break
          }
        } else {
          switch (stor.RangUniteI) {
            case 1: stor.tablo.virgMem = { t: false, num1: 4, num2: 0 }
              break
            case 2: stor.tablo.virgMem = { t: false, num1: 4, num2: 1 }
              break
            case 3: stor.tablo.virgMem = { t: false, num1: 4, num2: 2 }
              break
            case 4: stor.tablo.virgMem = { t: false, num1: 5, num2: 0 }
              break
            case 5: stor.tablo.virgMem = { t: false, num1: 5, num2: 1 }
              break
            case 6: stor.tablo.virgMem = { t: false, num1: 5, num2: 2 }
              break
          }
        }
      }
      stor.tablo.colorVirg('co')
      stor.tablo.disable()
    }

    if ((me.questionCourante % ds.nbetapes) === 3) {
      cotab2()
      stor.yaexplik = true
      j3pAffiche(stor.lesdiv.explications, null, me.donneesSection.textes.RepUnit,
        { a: chiffresdes(stor.nbdepart, 0), c: stor.monc, b: stor.RangUnite + stor.Monunite })
    }

    if ((me.questionCourante % ds.nbetapes) === 4) {
      if (stor.tablo.virgMem !== undefined) stor.tablo.colorVirg(false)
      const tAtt = (stor.MonuniteArrivee !== 'a') && (stor.MonuniteArrivee !== 'L' || ((stor.Monunite === 'L') && (stor.MonuniteArrivee === 'L')))
      if (tAtt) {
        stor.tablo.virgMem = { t: true, num: stor.RangUniteArriveeI }
      } else {
        if (stor.Monunite === 'a') {
          switch (stor.RangUniteArriveeI) {
            case 1: stor.tablo.virgMem = { t: false, num1: 1, num2: 1 }
              break
            case 3: stor.tablo.virgMem = { t: false, num1: 2, num2: 1 }
              break
            case 5: stor.tablo.virgMem = { t: false, num1: 3, num2: 1 }
              break
          }
        } else {
          switch (stor.RangUniteArriveeI) {
            case 1: stor.tablo.virgMem = { t: false, num1: 4, num2: 0 }
              break
            case 2: stor.tablo.virgMem = { t: false, num1: 4, num2: 1 }
              break
            case 3: stor.tablo.virgMem = { t: false, num1: 4, num2: 2 }
              break
            case 4: stor.tablo.virgMem = { t: false, num1: 5, num2: 0 }
              break
            case 5: stor.tablo.virgMem = { t: false, num1: 5, num2: 1 }
              break
            case 6: stor.tablo.virgMem = { t: false, num1: 5, num2: 2 }
              break
          }
        }
      }
      stor.tablo.colorVirg('co')
      stor.tablo.disable()
    }

    if ((me.questionCourante % ds.nbetapes) === 0) {
      stor.mazonem.corrige(false)
      stor.mazonem.barre()
      stor.mazonem.disable()
      if (stor.ErrPlusieursVirgule) {
        me.typederreurs[6]++
        stor.yaexplik = true
        j3pAffiche(stor.lesdiv.explications, null, me.donneesSection.textes.errtropvirgule)
      }
      if (stor.ErrVirguleSeule) {
        me.typederreurs[6]++
        stor.yaexplik = true
        j3pAffiche(stor.lesdiv.explications, null, me.donneesSection.textes.errvirguleseule)
      }
      if (stor.ErrEspace) {
        me.typederreurs[6]++
        stor.yaexplik = true
        j3pAffiche(stor.lesdiv.explications, null, me.donneesSection.textes.errespace)
      }
      if (stor.ErrDecimaleEspace) {
        me.typederreurs[6]++
        stor.yaexplik = true
        j3pAffiche(stor.lesdiv.explications, null, me.donneesSection.textes.errespacedec)
      }
      if (stor.ErrZeroInutile) {
        me.typederreurs[6]++
        stor.yaexplik = true
        if (me.donneesSection.ZerosInutiles === 'Non_Acceptés') {
          j3pAffiche(stor.lesdiv.explications, null, me.donneesSection.textes.ZeroInut2)
        } else {
          j3pAffiche(stor.lesdiv.explications, null, me.donneesSection.textes.ZeroInut3)
        }
      }
      mona = ecrisBienMathquill(stor.repattendue)
      j3pAffiche(stor.lesdiv.solution, null, me.donneesSection.textes.soluce,
        { a: mona, c: stor.monc, e: stor.RangUnite + stor.Monunite, d: stor.RangUniteArrivee + stor.MonuniteArrivee })
      stor.yasso = true
    }
  }

  switch (this.etat) {
    case 'enonce':
      // code exécuté au lancement de la section
      if (this.debutDeLaSection) {
        initSection()
      } else {
        enonceMain()
      }
      break // case "enonce":

    case 'correction':
      // On teste si une réponse a été saisie

      if (!yareponse()) {
        // ici on donne le message à afficher si une ou plusieurs zone(s) n’est pas complétée (tout d’abord on met en rouge : styles.cfaux)
        stor.lesdiv.correction.style.color = this.styles.cfaux
        stor.lesdiv.correction.innerHTML = reponseIncomplete
        stor.mazonem.focus()
        this.afficheBoutonValider()
        return
      } else { // Une réponse a été saisie
        stor.yaexplik = false
        stor.yasso = false
        // Bonne réponse
        if (isRepOk()) {
          this._stopTimer()
          this.score++
          stor.lesdiv.correction.style.color = this.styles.cbien
          stor.lesdiv.correction.innerHTML = cBien

          if ((this.questionCourante % ds.nbetapes) === 1) {
            stor.lesdiv.tabChange.innerHTML = ''
            cotab(true)
            stor.unittab = stor.lestab[stor.numtab][0]
            stor.fotradtab = stor.lestab[stor.numtab][1]
          }

          if ((this.questionCourante % ds.nbetapes) === 2) {
            stor.tablo.colorVirg(true)
            stor.tablo.disable()
          }

          if ((this.questionCourante % ds.nbetapes) === 3) {
            cotab2(true)
          }

          if ((this.questionCourante % ds.nbetapes) === 4) {
            stor.tablo.colorVirg(true)
            stor.tablo.disable()
          }

          if ((this.questionCourante % ds.nbetapes) === 0) {
            stor.mazonem.disable()
            stor.mazonem.corrige(true)

            var mona = ecrisBienMathquill(stor.repattendue)
            if (stor.ErrZeroInutile) {
              stor.yaexplik = true
              j3pAffiche(stor.lesdiv.explications, null, me.donneesSection.textes.ZeroInut,
                { a: mona })
            }
          }

          this.typederreurs[0]++
          this.cacheBoutonValider()
          this.etat = 'navigation'
          this.sectionCourante()
        } else {
          // Pas de bonne réponse
          stor.lesdiv.correction.style.color = this.styles.cfaux

          // A cause de la limite de temps :
          if (this.isElapsed) { // limite de temps
            this._stopTimer()

            stor.lesdiv.correction.innerHTML = tempsDepasse
            this.typederreurs[10]++
            this.cacheBoutonValider()

            affcofo()

            this.etat = 'navigation'
            this.sectionCourante()
          } else { // Réponse fausse :
            stor.lesdiv.correction.innerHTML = cFaux

            if (this.essaiCourant < ds.nbchances) {
              stor.lesdiv.correction.innerHTML += '<br>' + essaieEncore
              stor.lesdiv.correction.style.color = this.styles.cfaux

              this.typederreurs[1]++
              // indication éventuelle ici
              /// /////
              /// //////

              /// ////
              /// ///
              /// ////
            } else { // Erreur au nème essai
              this._stopTimer()
              this.cacheBoutonValider()

              /// ////
              /// ////
              /// ////
              affcofo()

              stor.lesdiv.correction.innerHTML += '<br>' + regardeCorrection
              this.typederreurs[2]++
              this.etat = 'navigation'
              this.sectionCourante()
            }
          }
        }

        if (stor.yasso) {
          stor.lesdiv.solution.classList.add('correction')
        }
        if (stor.yaexplik) {
          stor.lesdiv.explications.classList.add('explique')
        }
      }

      // Obligatoire
      this.finCorrection()
      break // case "correction":

    case 'navigation':
      if (this.sectionTerminee()) {
        const sco = this.score / ds.nbitems
        let peibase = 0
        if (sco >= 0.8) { peibase = 0 }
        if ((sco >= 0.55) && (sco < 0.8)) { peibase = 1 }
        if ((sco >= 0.3) && (sco < 0.55)) { peibase = 3 }
        if (sco < 0.3) { peibase = 5 };
        let peisup = 0
        const compt = this.typederreurs[2] / 4
        if (this.typederreurs[6] > compt) {
          peisup = 1
        }
        this.parcours.pe = ds.pe[peibase + peisup]
      } else {
        this.etat = 'enonce'
      }
      this.finNavigation()
      break // case "navigation":
  }
}
