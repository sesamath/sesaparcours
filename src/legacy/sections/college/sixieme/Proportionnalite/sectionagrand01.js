import { j3pAddElt, j3pAjouteBouton, j3pArrondi, j3pEmpty, j3pGetRandomInt, j3pShuffle } from 'src/legacy/core/functions'
import { getPe, initCptPe } from 'src/legacy/sections/college/helperTommy'
import { j3pAffiche } from 'src/lib/mathquill/functions'
import ZoneStyleMathquill2 from 'src/legacy/outils/zoneStyleMathquill/ZoneStyleMathquill2'
import { addDefaultTable } from 'src/legacy/themes/table'
import { j3pCreeFenetres, j3pAfficheCroixFenetres, j3pToggleFenetres } from 'src/legacy/core/functionsJqDialog'
import { j3pBaseUrl } from 'src/lib/core/constantes'
import textesGeneriques from 'src/lib/core/textes'

const { cBien, cFaux, essaieEncore, reponseIncomplete, tempsDepasse } = textesGeneriques

/**
 * Ajouter une description ici
 * @fileOverview
 * @author Tommy Barroy <t.barroy@laposte.net>
 * @since 2017-10
*/
export const params = {
  outils: ['mathquill', 'calculatrice'],
  /**
   * Ce qui est paramétrable dans l’éditeur de graphe
   * @type {LegacySectionParamsParametres}
   */
  parametres: [
    ['nbrepetitions', 9, 'entier', 'Nombre de répétitions de la section'],
    ['nbchances', 1, 'entier', 'Nombre d’essais possibles.'],
    ['limite', 0, 'entier', 'Temps disponible par essai.'],
    ['Calcul', 'les trois', 'liste', '<u>Echelle</u>: Il faut calculer l’échelle. <br><br> <u>Depart</u>: Il faut calculer une longueur de départ. <br><br> <u>Arrivee</u>: Il fau calculer une longueur après transformation.', ['Echelle', 'Depart', 'Arrivee', 'Echelle_Depart', 'Echelle_Arrivee', 'Depart_Arrivee', 'les trois']],
    ['Rapport', false, 'boolean', "<u>true</u>: le terme <b>rapport</b> est utilisé à la place d'<b>échelle</b>."],
    ['Echelle', 'entier', 'liste', 'L’échelle peut être un entier, un décimal ou une fraction.', ['entier', 'decimal', 'fraction', 'entier_decimal', 'entier_fraction', 'decimal_fraction', 'les trois']],
    ['Schema', false, 'boolean', '<u>true</u>:  Un schéma de la situation est proposé.'],
    ['Calculatrice', false, 'boolean', '<u>true</u>:  Une calculatrice est disponible'],
    // le modèle gère la compatibilité ascendante avec l’ancien paramètre MiseEnPagePlusClaire
    ['theme', 'standard', 'liste', 'Modèle pour l’affichage', ['standard', 'zonesAvecImageDeFond']]

  ],
  // dans le cas d’une section QUANTITATIVE
  pe: [
    {
      pe_1: 'depart',
      pe_2: 'arrivee',
      pe_3: 'echelle',
      pe_4: 'agrandissement',
      pe_5: 'reduction'
    }
  ]
}

/**
 * section agrand01
 * @this {Parcours}
 */
export default function main () {
  const me = this
  const ds = me.donneesSection
  const stor = me.storage

  function boutonfen () {
    j3pToggleFenetres('Calculatrice')
  }
  function boutonCHem () {
    j3pEmpty(stor.lesdiv.schema)
    const tabContCo = addDefaultTable(stor.lesdiv.solution, 2, 1)
    modif(tabContCo)
    const tabFigco = addDefaultTable(tabContCo[1][0], 2, 3)
    modif(tabFigco)

    let nu = 'ffig.png'
    let nu2 = 'ffig2.png'
    if (stor.Lexo.place === '+') {
      nu = 'ffig2.png'
      nu2 = 'ffig.png'
    }
    tabFigco[0][0].innerHTML = '<img src="' + j3pBaseUrl + 'sections/college/sixieme/Proportionnalite/' + nu + '">'
    tabFigco[0][2].innerHTML = '<img src="' + j3pBaseUrl + 'sections/college/sixieme/Proportionnalite/' + nu2 + '">'
    j3pAffiche(tabFigco[0][1], null, '---------------->')
    if (stor.Lexo.type === 'dep') {
      j3pAffiche(tabFigco[1][0], null, '$?$')
    } else {
      j3pAffiche(tabFigco[1][0], null, '$ ' + stor.Lexo.dep + '$ cm.')
    }
    if (stor.Lexo.type === 'arr') {
      j3pAffiche(tabFigco[1][2], null, '$?$')
    } else {
      j3pAffiche(tabFigco[1][2], null, '$ ' + stor.Lexo.arr + '$ cm.')
    }
    if (stor.Lexo.type === 'ech') {
      j3pAffiche(tabFigco[1][1], null, '$\\times ?$')
    } else {
      let op = ' \\times '
      if (stor.Lexo.place === '-') op = ' \\div '
      let lut = stor.Lexo.ech.num
      if (stor.Lexo.place === '-') lut = stor.Lexo.ech.den
      if (stor.Lexo.lech === 'fraction') {
        op = ' \\times '
        lut = '\\frac{' + stor.Lexo.ech.num + '}{' + stor.Lexo.ech.den + '}'
      }
      j3pAffiche(tabFigco[1][1], null, '$ ' + op + lut + '$')
    }
    tabFigco[1][1].style.textAlign = 'center'
    tabFigco[1][0].style.textAlign = 'center'
    tabFigco[1][2].style.textAlign = 'center'
  }
  function creeLesDiv () {
    stor.lesdiv = {}
    stor.lesdiv.conteneur = j3pAddElt(me.zones.MG, 'div', {
      style: me.styles.etendre('toutpetit.enonce', { padding: '10px' })
    })
    stor.lesdiv.consigne = addDefaultTable(stor.lesdiv.conteneur, 1, 1)[0][0]
    stor.lesdiv.consigne.style.padding = 0
    stor.lesdiv.travail = addDefaultTable(stor.lesdiv.conteneur, 1, 1)[0][0]
    stor.lesdiv.travail.style.padding = 0
    stor.lesdiv.explications = j3pAddElt(stor.lesdiv.conteneur, 'div')
    stor.lesdiv.solution = j3pAddElt(stor.lesdiv.conteneur, 'div')
    stor.lesdiv.calculatrice = j3pAddElt(me.zones.MD, 'div', {
      style: me.styles.etendre('petit.correction', { padding: '10px' })
    })
    stor.lesdiv.schema = j3pAddElt(me.zones.MD, 'div', {
      style: me.styles.etendre('petit.correction', { padding: '10px' })
    })
    stor.lesdiv.correction = j3pAddElt(me.zones.MD, 'div', {
      style: me.styles.etendre('petit.correction', { padding: '10px' })
    })
    stor.lesdiv.solution.style.color = me.styles.petit.correction.color
    stor.lesdiv.explications.style.color = me.styles.cfaux
    me.zonesElts.MG.classList.add('fond')
  }
  function poseQuestion () {
    let bob
    let bub1 = 'L’échelle'
    let bub2 = 'égale'
    let bub3 = 'l’échelle'
    let bub4 = 'échelle'
    if (ds.Rapport) {
      bub1 = 'Le rapport'
      bub2 = 'égal'
      bub3 = 'le rapport'
      bub4 = 'rapport'
    }
    const tabpti = addDefaultTable(stor.lesdiv.travail, 1, 4)
    modif(tabpti)
    if (stor.Lexo.place === '+') {
      j3pAffiche(stor.lesdiv.consigne, null, 'On a effectué un agrandissement d’une figure.')
      bob = 'l’agrandissement'
      stor.bob2 = 'agrandie'
    } else {
      j3pAffiche(stor.lesdiv.consigne, null, 'On a effectué une réduction d’une figure.')
      bob = 'la réduction'
      stor.bob2 = 'réduite'
    }
    if ((stor.Lexo.type === 'arr') || (stor.Lexo.type === 'dep')) {
      j3pAffiche(stor.lesdiv.consigne, null, '\n' + bub1 + ' de ' + bob + ' est ' + bub2 + ' à  $' + affichemoi(stor.Lexo.ech) + '$ .')
      if (stor.Lexo.type === 'arr') {
        j3pAffiche(stor.lesdiv.consigne, null, '\n<b>Quelle est la longueur sur ' + bob + ' \nd’un segment mesurant $' + stor.Lexo.dep + '$ cm sur la figure ? </b> ')
        j3pAffiche(tabpti[0][0], null, ' longueur ' + stor.bob2)
        stor.att = { num: stor.Lexo.arr, den: 1 }
      } else {
        j3pAffiche(stor.lesdiv.consigne, null, '\nLa longueur d’un segment sur ' + bob + ' est égale à $' + stor.Lexo.arr + '$ cm, \n<b> quelle était sa longueur sur la figure initiale ? </b> ')
        j3pAffiche(tabpti[0][0], null, ' longueur de départ ')
        stor.att = { num: stor.Lexo.dep, den: 1 }
      }
      j3pAffiche(tabpti[0][3], null, '&nbsp;cm ')
    } else {
      j3pAffiche(stor.lesdiv.consigne, null, '\nUn segment mesurait $' + stor.Lexo.dep + '$ cm sur la figure, \nil mesure  $' + stor.Lexo.arr + '$ cm sur ' + bob + '\n<b>  Calcule ' + bub3 + '. </b> ')
      j3pAffiche(tabpti[0][0], null, bub4)
      stor.att = stor.Lexo.ech
    }
    j3pAffiche(tabpti[0][1], null, ' $ = $ ')
    j3pAffiche(stor.lesdiv.consigne, null, '\n\n')
    stor.zoneRaiz1 = new ZoneStyleMathquill2(tabpti[0][2], { id: 'RAIZ2l0c1fdsfq', restric: stor.restric, inverse: true, hasAutoKeyboard: true, enter: me.sectionCourante.bind(me) })
  }
  function affichemoi (j) {
    if (j.den === 1) return j.num + ''
    return '\\frac{' + j.num + '}{' + j.den + '}'
  }
  function faisChoixExos () {
    const e = ds.lesExos.pop()
    let num, den, numpos, i
    const echpos = [2, 3, 4, 5, 6, 7, 8, 9, 10, 11, 20, 100, 200, 1000]
    stor.restric = '0123456789/,.'
    switch (e.place) {
      case '+':
        switch (e.lech) {
          case 'entier':
            e.ech = { num: echpos[j3pGetRandomInt(0, echpos.length - 1)], den: 1 }
            e.dep = j3pGetRandomInt(2, 11)
            e.arr = e.dep * e.ech.num
            break
          case 'decimal':
            e.ech = { num: j3pArrondi(j3pGetRandomInt(0, 20) + j3pGetRandomInt(1, 9) / 10, 1), den: 1 }
            e.dep = j3pArrondi(j3pGetRandomInt(0, 20) + j3pGetRandomInt(1, 9) / 10, 1)
            e.arr = j3pArrondi(e.dep * e.ech.num, 2)
            break
          case 'fraction':
            numpos = [2, 3, 5, 7, 11, 13, 17, 19, 23, 29, 31]
            numpos = j3pShuffle(numpos)
            num = numpos.pop()
            den = numpos.pop()
            if (den > num) { i = num; num = den; den = i }
            e.ech = { num, den }
            e.dep = j3pGetRandomInt(2, 20) * den
            e.arr = j3pArrondi(e.dep * e.ech.num / e.ech.den, 0)
        }

        break
      case '-':
        switch (e.lech) {
          case 'entier':
            e.ech = { den: echpos[j3pGetRandomInt(0, echpos.length - 1)], num: 1 }
            e.arr = j3pGetRandomInt(2, 11)
            e.dep = e.arr * e.ech.den
            break
          case 'decimal':
            e.ech = { den: echpos[j3pGetRandomInt(0, echpos.length - 1)], num: 1 }
            e.arr = j3pArrondi(j3pGetRandomInt(2, 11) + j3pGetRandomInt(1, 9) / 10, 1)
            e.dep = j3pArrondi(e.arr * e.ech.den, 1)
            break
          case 'fraction':
            numpos = [2, 3, 5, 7, 11, 13, 17, 19, 23, 29, 31]
            numpos = j3pShuffle(numpos)
            num = numpos.pop()
            den = numpos.pop()
            if (den < num) { i = num; num = den; den = i }
            e.ech = { num, den }
            e.arr = j3pGetRandomInt(2, 20) * num
            e.dep = j3pArrondi(e.arr * e.ech.den / e.ech.num, 0)
        }
    }
    return e
  }

  function initSection () {
    initCptPe(ds, stor)

    // On surcharge avec les parametres passés par le graphe
    me.surcharge()
    me.validOnEnter = false

    // Construction de la page
    me.construitStructurePage('presentation1bis')

    me.donneesSection.lesExos = []

    let lp = []
    if ((ds.Calcul.indexOf('Ech') !== -1) || (ds.Calcul === 'les trois')) lp.push('ech')
    if ((ds.Calcul.indexOf('Arr') !== -1) || (ds.Calcul === 'les trois')) lp.push('arr')
    if ((ds.Calcul.indexOf('Dep') !== -1) || (ds.Calcul === 'les trois')) lp.push('dep')
    lp = j3pShuffle(lp)
    for (let i = 0; i < me.donneesSection.nbitems; i++) {
      me.donneesSection.lesExos.push({ type: lp[i % (lp.length)] })
    }
    me.donneesSection.lesExos = j3pShuffle(me.donneesSection.lesExos)

    lp = ['+', '-']
    lp = j3pShuffle(lp)
    for (let i = 0; i < me.donneesSection.nbitems; i++) {
      me.donneesSection.lesExos[i].place = lp[i % (lp.length)]
    }
    me.donneesSection.lesExos = j3pShuffle(me.donneesSection.lesExos)

    lp = []
    if ((ds.Echelle.indexOf('entier') !== -1) || (ds.Echelle === 'les trois')) lp.push('entier')
    if ((ds.Echelle.indexOf('decimal') !== -1) || (ds.Echelle === 'les trois')) lp.push('decimal')
    if ((ds.Echelle.indexOf('fraction') !== -1) || (ds.Echelle === 'les trois')) lp.push('fraction')
    lp = j3pShuffle(lp)
    for (let i = 0; i < me.donneesSection.nbitems; i++) {
      me.donneesSection.lesExos[i].lech = lp[i % lp.length]
    }
    me.donneesSection.lesExos = j3pShuffle(me.donneesSection.lesExos)
    me.typederreurs = [0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0]
    me.afficheTitre('Agrandissement et réduction')
    enonceMain()
  }

  function yaReponse () {
    if (stor.zoneRaiz1.reponse() === '') {
      stor.zoneRaiz1.focus()
      return false
    }
    return true
  }
  function isRepOk () {
    return egalite(stor.zoneRaiz1.reponsenb(), stor.att)
  }

  function egalite (a, b) {
    return j3pArrondi(a[0] * b.den, 6) === j3pArrondi(a[1] * b.num, 6)
  }
  function affCorrFaux (isFin) {
    /// //affiche indic
    stor.zoneRaiz1.corrige(false)

    if (isFin) {
      // barre les faux
      stor.zoneRaiz1.barre()
      stor.zoneRaiz1.disable()
      stor.yaco = true

      j3pEmpty(stor.lesdiv.solution)
      j3pEmpty(stor.lesdiv.schema)
      const tabContCo = addDefaultTable(stor.lesdiv.solution, 2, 1)
      modif(tabContCo)
      const tabCalco = addDefaultTable(tabContCo[0][0], 3, 3)
      modif(tabCalco)
      const tabFigco = addDefaultTable(tabContCo[1][0], 2, 3)
      modif(tabFigco)

      let bub1 = 'échelle'
      if (ds.Rapport) {
        bub1 = 'rapport'
      }

      let op, lut
      if (stor.Lexo.type === 'ech') {
        j3pAffiche(tabCalco[0][0], null, bub1 + ' = $\\frac{ \\text{longueur ' + stor.bob2 + '} }{ \\text{ longueur de départ}}$')
        j3pAffiche(tabCalco[1][0], null, bub1 + ' = $\\frac{ ' + stor.Lexo.arr + ' }{ ' + stor.Lexo.dep + '}$')
        j3pAffiche(tabCalco[2][0], null, bub1 + ' = $' + affichemoi(stor.Lexo.ech) + '$')
      } else {
        op = ' \\times '
        if (((stor.Lexo.type === 'arr') && (stor.Lexo.place === '-')) ||
          ((stor.Lexo.type === 'dep') && (stor.Lexo.place === '+'))) op = ' \\div '
        lut = stor.Lexo.ech.num
        if (stor.Lexo.place === '-') lut = stor.Lexo.ech.den
        let tan = stor.Lexo.dep
        if (stor.Lexo.type === 'dep') tan = stor.Lexo.arr
        if (stor.Lexo.lech === 'fraction') {
          op = ' \\times '
          lut = '\\frac{' + stor.Lexo.ech.num + '}{' + stor.Lexo.ech.den + '}'
          if (stor.Lexo.type === 'dep') lut = '\\frac{' + stor.Lexo.ech.den + '}{' + stor.Lexo.ech.num + '}'
        }
        let bob3 = 'de départ'
        if (stor.Lexo.type === 'arr') bob3 = stor.bob2
        j3pAffiche(tabCalco[0][0], null, 'longueur ' + bob3 + '$ = ' + tan + op + lut + '$')
        j3pAffiche(tabCalco[1][0], null, 'longueur ' + bob3 + '$ = ' + stor.att.num + '$ cm.')
      }
      let nu = 'ffig.png'
      let nu2 = 'ffig2.png'
      if (stor.Lexo.place === '+') {
        nu = 'ffig2.png'
        nu2 = 'ffig.png'
      }
      tabFigco[0][0].innerHTML = '<img src="' + j3pBaseUrl + 'sections/college/sixieme/Proportionnalite/' + nu + '">'
      tabFigco[0][2].innerHTML = '<img src="' + j3pBaseUrl + 'sections/college/sixieme/Proportionnalite/' + nu2 + '">'
      j3pAffiche(tabFigco[0][1], null, '---------------->')
      j3pAffiche(tabFigco[1][0], null, '$ ' + stor.Lexo.dep + '$ cm.')
      j3pAffiche(tabFigco[1][2], null, '$ ' + stor.Lexo.arr + '$ cm.')
      op = ' \\times '
      if (stor.Lexo.place === '-') op = ' \\div '
      lut = stor.Lexo.ech.num
      if (stor.Lexo.place === '-') lut = stor.Lexo.ech.den
      if (stor.Lexo.lech === 'fraction') {
        op = ' \\times '
        lut = '\\frac{' + stor.Lexo.ech.num + '}{' + stor.Lexo.ech.den + '}'
      }
      j3pAffiche(tabFigco[1][1], null, '$ ' + op + lut + '$')
      tabFigco[1][1].style.textAlign = 'center'
      tabFigco[1][0].style.textAlign = 'center'
      tabFigco[1][1].style.textAlign = 'center'
    }
  }
  function modif (ki) {
    for (let i = 0; i < ki.length; i++) {
      for (let j = 0; j < ki[i].length; j++) {
        ki[i][j].style.padding = 0
      }
    }
  }
  function enonceMain () {
    if (me.etapeCourante === 1) {
      // A chaque répétition, on nettoie la scène
      // A commenter si besoin
      me.videLesZones()
    }

    creeLesDiv()

    stor.Lexo = faisChoixExos()

    stor.lesdiv.travail.classList.add('travail')
    stor.lesdiv.consigne.classList.add('enonce')

    poseQuestion()
    if (ds.Calculatrice) {
      me.fenetresjq = [{ name: 'Calculatrice', title: 'Une calculatrice', left: 600, top: 10 }]
      j3pCreeFenetres(me)
      j3pAfficheCroixFenetres('Calculatrice')
      j3pAjouteBouton(stor.lesdiv.calculatrice, boutonfen, { className: 'MepBoutons', value: 'Calculatrice' })
    }

    if (ds.Schema) {
      j3pAjouteBouton(stor.lesdiv.schema, boutonCHem, { className: 'MepBoutons', value: 'Voir un schéma' })
    }

    me.afficheBoutonValider()
    me.finEnonce()
    stor.zoneRaiz1.focus()
  }

  switch (this.etat) {
    case 'enonce':
      // code exécuté au lancement de la section
      if (me.debutDeLaSection) {
        initSection()
      } else {
        enonceMain()
      }
      break // case "enonce":

    case 'correction':
      // On teste si une réponse a été saisie
      stor.yaexplik = false
      stor.yaco = false
      stor.lesdiv.explications.innerHTML = ''

      if (yaReponse()) {
        // Bonne réponse
        if (isRepOk()) {
          this.score++
          stor.lesdiv.correction.style.color = this.styles.cbien
          stor.lesdiv.correction.innerHTML = cBien

          j3pEmpty(stor.lesdiv.schema)
          stor.zoneRaiz1.corrige(true)
          stor.zoneRaiz1.disable()

          this.typederreurs[0]++
          this.etat = 'navigation'
          this.sectionCourante()
        } else {
          switch (stor.Lexo.type) {
            case 'dep':
              stor.compteurPe1++
              break
            case 'arr':
              stor.compteurPe2++
              break
            case 'ech':
              stor.compteurPe3++
              break
          }
          if (stor.Lexo.place === '+') {
            stor.compteurPe4++
          } else {
            stor.compteurPe5++
          }
          // Pas de bonne réponse
          stor.lesdiv.correction.style.color = this.styles.cfaux
          stor.lesdiv.correction.innerHTML = cFaux
          if (this.isElapsed) {
            stor.lesdiv.correction.innerHTML += tempsDepasse + '<BR>'
            this.typederreurs[10]++
            affCorrFaux(true)
            stor.yaco = true
            this.etat = 'navigation'
            this.sectionCourante()
          } else {
            // Réponse fausse :
            if (me.essaiCourant < me.donneesSection.nbchances) {
              stor.lesdiv.correction.innerHTML += '<br>' + essaieEncore
              affCorrFaux(false)
              // affCorrFaux.call(this, false)
              this.typederreurs[1]++
            } else {
              // Erreur au nème essai
              affCorrFaux(true)
              stor.yaco = true
              this.typederreurs[2]++
              this.etat = 'navigation'
              this.sectionCourante()
            }
          }
        }
      } else {
        // pas de réponse
        stor.lesdiv.correction.style.color = this.styles.cfaux
        stor.lesdiv.correction.innerHTML = reponseIncomplete
        this.afficheBoutonValider()
      }

      if (ds.theme === 'zonesAvecImageDeFond') {
        stor.lesdiv.correction.classList.add('feedback')

        if (stor.yaexplik) {
          stor.lesdiv.explications.classList.add('explique')
        }
        if (stor.yaco) {
          stor.lesdiv.solution.classList.add('correction')
        }
      }

      // Obligatoire
      this.finCorrection()
      break // case "correction":

    case 'navigation':
      if (this.sectionTerminee()) {
        this.parcours.pe = getPe(ds, stor)
      } else {
        this.etat = 'enonce'
      }
      this.finNavigation()
      break // case "navigation":
  }
}
