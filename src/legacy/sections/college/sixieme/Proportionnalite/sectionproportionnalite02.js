import $ from 'jquery'
import { j3pAddElt, j3pAjouteBouton, j3pArrondi, j3pClone, j3pDetruit, j3pDiv, j3pEmpty, j3pGetRandomBool, j3pGetRandomInt, j3pModale, j3pNotify, j3pPGCD, j3pShowError, j3pShuffle } from 'src/legacy/core/functions'
import BoutonStyleMathquill from 'src/legacy/outils/boutonsStyleMatquill'
import ListeDeroulante from 'src/legacy/outils/listeDeroulante/ListeDeroulante'
import MenuContextuel from 'src/legacy/outils/menuContextuel'
import { getJ3pConteneur } from 'src/lib/core/domHelpers'
import { j3pAffiche } from 'src/lib/mathquill/functions'
import { addDefaultTable } from 'src/legacy/themes/table'
import ZoneStyleMathquill2 from 'src/legacy/outils/zoneStyleMathquill/ZoneStyleMathquill2'
import { j3pAfficheCroixFenetres, j3pCreeFenetres, j3pMasqueFenetre, j3pToggleFenetres } from 'src/legacy/core/functionsJqDialog'
import { j3pExtraireNumDen } from 'src/legacy/core/functionsLatex'
import { affichefois, afficheFrac } from 'src/legacy/outils/zoneStyleMathquill/functions'
import { j3pBaseUrl } from 'src/lib/core/constantes'
import textesGeneriques from 'src/lib/core/textes'

import './animationPointIntersection.scss'

const { cBien, cFaux, essaieEncore, reponseIncomplete, tempsDepasse } = textesGeneriques

/**
 * @author Tommy Barroy <tommy.barroy@sesamath.net>
 * @since 10/2017
 * @fileOverview Tableau de proportionnalité à compléter progressivement
 */
export const params = {
  outils: ['calculatrice', 'mathquill'],
  /**
   * Ce qui est paramétrable dans l’éditeur de graphe
   * @type {LegacySectionParamsParametres}
   */
  parametres: [
    ['nbrepetitions', 9, 'entier', 'Nombre de répétitions de la section'],
    ['nbchances', 2, 'entier', 'Nombre d’essais possibles.'],
    ['limite', 0, 'entier', 'Temps disponible par répétition, en secondes'],
    ['Legende', true, 'boolean', '<u>true</u>: L’élève doit choisir les légendes du tableau avant de commencer <BR> <u>false</u>: Les légendes sont déjà notées.'],
    ['Remplir', true, 'boolean', '<u>true</u>: L’élève doit remplir le tableau avec les données de l’énoncé. <BR> <u>false</u>: Le tableau est pré-rempli.'],
    ['Completer', 'les deux', 'liste', '<u>Raisonnement</u>: L’élève doit justifier son résultat. <BR> <u>Reponse</u>: L’élève doit écrire son résultat.', ['les deux', 'Raisonnement', 'Reponse']],
    ['Conclusion', true, 'boolean', '<u>true</u>: L’élève doit compléter la conclusion. <BR> <u>false</u>: La conclusion est automatiquement remplie.'],
    ['Multiplication', true, 'boolean', '<i> uniquement avec le paramètre </i>Completer<i> à </i> Raisonnement <BR> <u>true</u>: L’élève peut multiplier une colonne par un nombre pour en obtenir une autre. <BR> <u>false</u>: non.'],
    ['Unite', true, 'boolean', '<i> uniquement avec le paramètre </i>Completer<i> à </i> Raisonnement <BR> <u>true</u>: L’élève peut utiliser le passage à l’unité. <BR> <u>false</u>: non.'],
    ['Addition', true, 'boolean', '<i> uniquement avec le paramètre </i>Completer<i> à </i> Raisonnement <BR> <u>true</u>: L’élève peut utiliser l’addition ou la sousraction de deux colonnes. <BR> <u>false</u>: non.'],
    ['Coefficient', true, 'boolean', '<i> uniquement avec le paramètre </i>Completer<i> à </i> Raisonnement <BR> <u>true</u>: L’élève peut utiliser le coefficient de proportionalité. <BR> <u>false</u>: non.'],
    ['ProduitCroix', true, 'boolean', '<i> uniquement avec le paramètre </i>Completer<i> à </i> Raisonnement <BR> <u>true</u>: L’élève peut utiliser le produit en croix. <BR> <u>false</u>: non.'],
    ['Entiers', false, 'boolean', ' <u>true</u>: Les nombres proposés et attendus sont forcément entiers. <BR> <u>false</u>: non.'],
    ['Ecriture', 'les deux', 'liste', " <u>fractionnaire</u>: L’écriture fractionnaire et préférée à l’écriture décimale. <BR> <u>décimale</u>: L’écriture décimale et préférée à l’écriture fractionnaire. <BR> <u>les deux</u>: Les deux écritures sont proposées aléatoirement. <BR> <BR> <i> valbale uniquement si le paramètre <b>'Entiers'</b> est à false </i>.", ['fractionnaire', 'décimale', 'les deux']],
    ['Positifs', true, 'boolean', '<u>true</u>:  Les nombres proposés et attendus sont forcément positifs. <BR> <u>false</u>: non.'],
    ['Piege', false, 'boolean', "<u>true</u>:  Les 4 opérations sont proposées dans le raisonnement 'opération sur 1 colonne'. <BR> <u>false</u>: non."],
    // le modèle gère la compatibilité ascendante avec l’ancien paramètre MiseEnPagePlusClaire
    ['theme', 'standard', 'liste', 'Modèle pour l’affichage', ['standard', 'zonesAvecImageDeFond']]
  ]
}

const textes = {
  couleurexplique: '#A9E2F3',
  couleurcorrec: '#A9F5A9',
  couleurenonce: '#D8D8D8',
  couleurenonceg: '#F3E2A9',
  couleuretiquettes: '#A9E2F3',
  etapeLegende: '<u>Etape £a</u>: Créer un tableau.',
  etapeRemplir: '<u>Etape £a</u>: Remplir le tableau avec les données de l’énoncé.',
  etapeCompleter: '<u>Etape £a</u>: Compléter le tableau en utilisant la proportionnalité.',
  etapeConclusion: '<u>Etape £a</u>: Conclure.',
  regardecp: 'Regarde la correction',
  prenomsf: ['Shahineze', 'Juliette', 'Lili', 'Yesmin', 'Sarah', 'Louise', 'Marta', 'Anaïs', 'Doriane', 'Karen', 'Solène', 'Lison'],
  prenomsg: ['Walid', 'Marc', 'Jean', 'Simon', 'Mohamed', 'Mathys', 'Gaël', 'Nathéo', 'Charlie', 'Jean-Paul', 'Abdel', 'Alexis', 'Rémi'],
  choisir: 'Choisir',
  pb: [
    /* {
    type: ['ent', 'pos'],
    Enoncedeb: '£e presse des oranges pour obtenir du jus. ',
    donnees: 'Avec $£a$ oranges, £f obtient $£b$ cL de jus.',
    Enoncefin: ' On suppose que la quantité de jus est proportionnelle au nombre d’oranges. <BR><b> Quelle quantité de jus obtient-£f avec $£c$ oranges ? <BR> Combien d’oranges faut-il pour obtenir $£d$ cL de jus ?</b>',
    legende1: 'nombre d’oranges',
    legende2: 'quantité de jus (cL)',
    unite1: 'oranges',
    unite1sing: 'orange',
    unite2: 'cL',
    unite2sing: 'cL',
    folegende11: 'nombre de jus d’orange',
    folegende12: 'nombre de peaux d’orange',
    folegende13: 'nombre orange',
    folegende21: 'masse des oranges (kg)',
    folegende22: 'quantité de jus (km)',
    folegende23: 'jus',
    concl1: 'Avec $£c$ oranges, £e obtient $£a$ cL de jus.',
    concl2: 'Pour obtenir $£d$ cL de jus, £f doit presser $£a$ oranges.',
    /// 10 cL pour une orange au min
    // 40 cL pour une orange au max
    limitebasse: 10,
    limitehaute: 20
  }, {
    type: ['ent', 'pos'],
    Enoncedeb: '£e repeint le mur de sa classe.',
    donnees: ' Avec $£a$ pots de peinture, £f peut couvrir $£b$ m² du mur. ',
    Enoncefin: ' On suppose que la surface recouverte est proportionnelle au nombre de pots de peinture. <BR> <b> Quelle surface peut-£f peindre avec $£c$ pots de peinture ? <BR> Combien de pots faut-il pour couvrir $£d$ m² ? </b>',
    legende1: 'nombre de pots de peinture',
    legende2: 'surface recouverte (m²)',
    unite1: 'pots',
    unite1sing: 'pot',
    unite2: 'm²',
    unite2sing: 'm²',
    folegende11: 'nombre de pots ouverts',
    folegende12: 'nombre de peaux',
    folegende13: 'nombre',
    folegende21: 'volume du mur (m³)',
    folegende22: 'surface',
    folegende23: 'aire recouverte (m)',
    concl1: 'Avec $£c$ pots de peinture, £e peut couvrir $£a$ m².',
    concl2: 'Pour couvrir $£d$ m² de mur, £f a besoin de  $£a$ pots',
    /// 5 m² pour un pot
    // 100 m² pour un pot
    limitebasse: 5,
    limitehaute: 20
  }, {
    type: ['ent', 'pos'],
    Enoncedeb: '£e prépare des crêpes.',
    donnees: ' Avec $£a$ œufs, £f peut cuisiner $£b$ crêpes. ',
    Enoncefin: " On suppose que le nombre de crêpes est proportionnel au nombre d'œufs. <BR> <b> Combien de crêpes peut-£f préparer avec $£c$ œufs ? <BR> Combien d'œufs faut-il pour faire $£d$ crêpes ? </b>",
    legende1: "nombre d'œufs",
    legende2: 'nombre de crêpes',
    unite1: 'œufs',
    unite1sing: 'œuf',
    unite2: 'crêpes',
    unite2sing: 'crêpe',
    folegende11: "nombre d'œufs cassés",
    folegende12: "jaune d'œufs",
    folegende13: 'euf',
    folegende21: 'nombre de crêpes mangées',
    folegende22: 'nombre de crêpes ratées',
    folegende23: 'crêpe',
    concl1: 'Avec $£c$ œufs, £e peut préparer $£a$ crêpes.',
    concl2: 'Pour faire $£d$ crêpes, £f a besoin de  $£a$ œufs',
    /// 5 crêpes  pour un oeufs
    // 20 crêpes  pour un oeufs
    limitebasse: 5,
    limitehaute: 10
  }, {
    type: ['pos'],
    Enoncedeb: '£e achète du liquide de nettoyage.',
    donnees: ' Avec $£a$ €, £f peut obtenir $£b$ litres. ',
    Enoncefin: ' On suppose que le volume de liquide est proportionnel au prix. <BR> <b> Quelle quantité de liquide peut-£f acheter avec $£c$ € ? <BR> Quelle somme faut-il pour obtenir $£d$ L ? </b>',
    legende1: 'prix (en €)',
    legende2: 'Quantité de liquide (en L)',
    unite1: '€',
    unite1sing: '€',
    unite2: 'L',
    unite2sing: 'L',
    folegende11: 'dollar',
    folegende12: 'argent',
    folegende13: 'prix',
    folegende21: 'volume de liquide (en km)',
    folegende22: 'propreté de l’eau',
    folegende23: 'surface liquide (en L)',
    concl1: 'Avec $£c$ €, £e peut acheter $£a$ L de liquide de nettoyage.',
    concl2: 'Pour obtenir $£d$ L de liquide, £f a besoin de  $£a$ €',
    /// 0,1 L pour 1 euro
    // 2 L pour 1 euro
    limitebasse: 0.3,
    limitehaute: 9
  },
    */
    {
      type: ['pos'],
      Enoncedeb: '£e conduit une ambulance.',
      donnees: ' Avec $£a$ L d’essence, £f peut parcourir $£b$ km. ',
      Enoncefin: ' On suppose que la distance parcourue est proportionnelle à la quantité d’essence. <BR> <b> Quelle distance peut-£f parcourir avec $£c$ L d’essence ? <BR> Quelle quantité d’essence faut-il pour parcourir $£d$ km ? </b>',
      legende2: 'distance (en km)',
      legende1: 'Quantité d’essence (en L)',
      unite2: 'km',
      unite2sing: 'km',
      unite1: 'L',
      unite1sing: 'L',
      folegende21: 'km',
      folegende22: 'distance',
      folegende23: 'distance (en kg)',
      folegende11: 'volume d’essence (en km)',
      folegende12: 'quantité d’énergie (en J)',
      folegende13: 'essence sans plomb',
      concl1: 'Avec $£c$ L, £e peut parcourir $£a$ km.',
      concl2: 'Pour parcourir $£d$ km, £f a besoin de  $£a$ L d’essence',
      /// 8 km par L
      // 22 km par L
      limitebasse: 8,
      limitehaute: 22
    }
  /*
  , {
    type: ['pos'],
    Enoncedeb: '£e utilise un plan de sa région.',
    donnees: '  $£a$ cm sur le plan représentent $£b$ km en réalité. ',
    Enoncefin: 'Les distances réelles et sur le plan sont proportionnelles. <BR> <b> Quelle distance réelle représentent $£c$ cm sur le plan ? <BR> Quelle longueur représente $£d$ km en réalité ? </b>',
    legende2: 'distance réelle (en km)',
    legende1: 'distance sur le plan (en cm)',
    unite2: 'km',
    unite2sing: 'km',
    unite1: 'cm',
    unite1sing: 'cm',
    folegende21: 'km',
    folegende22: 'distance',
    folegende23: 'distance réelle (en kg)',
    folegende11: 'plan',
    folegende12: 'distance (cm)',
    folegende13: 'distance dessinée',
    concl1: ' $£c$ cm sur le plan représentent $£a$ km.',
    concl2: 'Pour représenter $£d$ km, il faut une longueur de $£a$ cm sur le plan.',
    /// 8 km par L
    // 22 km par L
    limitebasse: 0.1,
    limitehaute: 20
  }, {
    type: ['pos'],
    Enoncedeb: '£e vend un métal précieux.',
    donnees: '  Pour $£a$ g de métal précieux , £f facture $£b$ €. ',
    Enoncefin: 'Le prix du métal précieux est proportionnel à sa masse. <BR> <b> Combien peut-£f vendre $£c$ g de métal précieux ? <BR> Quelle quantité de métal précieux doit-£f vendre pour obtenir $£d$ € ? </b>',
    legende2: 'prix du métal précieux (en €)',
    legende1: 'masse de métal précieux (en g)',
    unite2: '€',
    unite2sing: '€',
    unite1: 'g',
    unite1sing: 'g',
    folegende21: 'g',
    folegende22: 'Poids de métal précieux (g)',
    folegende23: 'masse d’or (en g)',
    folegende11: 'argent',
    folegende12: '$',
    folegende13: 'métal précieux',
    concl2: ' Pour obtenir $£d$ €, £f doit vendre $£a$ g de métal précieux.',
    concl1: '$£c$ g de métal précieux rapportent $£a$ €.',
    /// 8 km par L
    // 22 km par L
    limitebasse: 20,
    limitehaute: 100
  }, {
    type: ['pos'],
    Enoncedeb: '£e observe des bactéries au microscope.',
    donnees: '  $£a$ mm au microscope représentent $£b$ µm en réalité. ',
    Enoncefin: 'Les longueurs réelles et au microscope sont proportionnelles. <BR> <b> Quelle longueur réelle représentent $£c$ mm au microscope ? <BR> Quelle longueur au microscope représente $£d$ µm en réalité ? </b>',
    legende2: 'longueur réelle (en µm)',
    legende1: 'longueur au microscope (en mm)',
    unite2: 'µm',
    unite2sing: 'µm',
    unite1: 'mm',
    unite1sing: 'mm',
    folegende21: 'km',
    folegende22: 'µm',
    folegende23: 'longueur réelle (en km)',
    folegende11: 'microscope',
    folegende12: 'longueur (mm)',
    folegende13: 'longueur agrandie',
    concl1: ' $£c$ mm au microscope représentent $£a$ µm.',
    concl2: 'Pour représenter $£d$ µm, le microscope affiche une longueur de $£a$ mm.',
    /// 8 km par L
    // 22 km par L
    limitebasse: 0.1,
    limitehaute: 30
  }, {
    type: [''],
    Enoncedeb: '£e a inventé l’opération MachinMachine.',
    donnees: ' La MachinMachine de $£a$ est égale à $£b$ . ',
    Enoncefin: 'La MachinMachine d’un nombre est proportionnelle au nombre. <BR> <b> Quelle est la MachinMachine de  $£c$  ? <BR>  $£d$ est la MachinMachine de quel nombre  ? </b>',
    legende2: 'MachinMachine de <i>x</i>',
    legende1: 'nombre <i>x</i>',
    unite2: '',
    unite2sing: '',
    unite1: '',
    unite1sing: '',
    folegende21: 'km',
    folegende22: 'cL',
    folegende23: 'nombre entier',
    folegende11: 'MachinMachine',
    folegende12: 'MachinTruc',
    folegende13: 'MachinTruc de <i></i>',
    concl1: ' Le nombre $£c$ a pour MachinMachine $£a$.',
    concl2: ' $£d$ est la MachinMachine du nombre $£a$.',
    /// 8 km par L
    // 22 km par L
    limitebasse: 0.1,
    limitehaute: 100
  }
  */
  ] // fin liste pb
}

/**
 * section proportionnalite02
 * @this {Parcours}
 */
export default function main () {
  const me = this
  const stor = this.storage
  const ds = me.donneesSection

  function initSection () {
    // Construction de la page
    me.construitStructurePage('presentation1bis')
    stor.mepact = getJ3pConteneur(me.zonesElts.MG)

    // un objet pour pouvoir appeler ces fcts dynamiquement
    stor.fcts = {
      annule: {
        op1col: annuleop1col,
        sub2col: annulesub2col,
        aj2col: annuleaj2col,
        passageUnite: annulepassageUnite,
        prodCroix: annuleprodCroix
      }
    }
    stor.menus = []
    stor.resultats = []
    stor.boutonValOp = []
    stor.etapeAfaire = []
    stor.pbfait = []
    stor.genre = j3pGetRandomInt(0, 1)
    stor.gfait = []
    stor.ffait = []
    stor.methodesposfait = []
    stor.nb = 1
    stor.avireee = []

    ds.nbetapes = 1
    if (ds.Legende) {
      ds.nbetapes++
      stor.etapeAfaire.push('Legende')
    }
    if (ds.Remplir) {
      ds.nbetapes++
      stor.etapeAfaire.push('Remplir')
    }
    stor.etapeAfaire.push(ds.Completer)
    if (ds.Conclusion) {
      ds.nbetapes++
      stor.etapeAfaire.push('Conclusion')
    }
    ds.nbitems = ds.nbetapes * ds.nbrepetitions

    const rem = stor.etapeAfaire.pop()
    stor.etapeAfaire.splice(0, 0, rem)
    initChoices()
    // que l’on va mettre dans le menu suivant les cas
    stor.menuChoices = []
    if (ds.Multiplication || ds.Unite) stor.menuChoices.push(stor.choices.op1col)
    if (ds.Addition) stor.menuChoices.push(stor.choices.aj2col)
    if (ds.Addition) stor.menuChoices.push(stor.choices.sub2col)
    if (ds.Unite) stor.menuChoices.push(stor.choices.passageUnite)
    if (ds.Coefficient) stor.menuChoices.push(stor.choices.coef)
    if (ds.ProduitCroix) stor.menuChoices.push(stor.choices.prodCroix)
    // si on a rien de tout ce qui précède on met au moins ça
    if (!stor.menuChoices.length) stor.menuChoices.push(stor.choices.op1col)

    me.score = 0

    // titre
    let titre = 'Utiliser la proportionnalité - '
    if (stor.menuChoices.length === 1) {
      titre += stor.menuChoices[0].label
    } else if (stor.menuChoices.length === 2) {
      if (ds.Unite) titre += stor.choices.passageUnite.label
      else if (ds.Addition) titre += stor.choices.aj2col.label
    }
    me.afficheTitre(titre)
    enonceMain()
  } // initSection
  function enonceMain () {
    stor.blokRaiz = false
    if (((me.questionCourante % ds.nbetapes) === 1) || (ds.nbetapes === 1)) {
      stor.Etapefaite = []
      // A chaque répétition, on nettoie la scène
      me.videLesZones()
      stor.lesdiv = {}
    }

    stor.listeDeroulante = []
    stor.listunite = []
    stor.tablid = []
    stor.desatout = false
    stor.listepdt = []
    stor.yaeco = false
    stor.croixListe = []
    stor.yapdt = false

    let ok
    let cmpt

    if (stor.lesdiv.correction !== null) j3pDetruit(stor.lesdiv.correction)

    if (stor.etapeAfaire[me.questionCourante % ds.nbetapes] === 'Conclusion') {
      stor.lesdiv.explik.innerHTML = ''
      stor.lesdiv.explik2.innerHTML = ''
      stor.lesdiv.explik.classList.remove('correction')
      stor.lesdiv.explik2.classList.remove('explique')
      stor.isPaused = true

      stor.lesdiv.etape.innerHTML = ''
      j3pAffiche(stor.lesdiv.etape, null, textes.etapeConclusion, {
        styletext: {},
        a: ds.nbetapes
      })
      if (!stor.paspas) {
        if (!stor.isCorrVisible) {
          voirCorrection()
        }
        if (stor.lesdiv.blabliblo) j3pDetruit(stor.correcBout)
      } else {
        stor.repElco = j3pClone(stor.repEl)
      }
      j3pDetruit('Calculatrice')

      const tabconcl = addDefaultTable(stor.lesdiv.conclusion, 2, 1)
      const tabConcl1 = addDefaultTable(tabconcl[0][0], 1, 3)
      const tabConcl2 = addDefaultTable(tabconcl[1][0], 1, 3)
      let ll1 = stor.pbencours.concl1.split('$£a$')
      j3pAffiche(tabConcl1[0][0], null, ll1[0] + '&nbsp;', {
        styletext: {},
        c: stor.afnbxg1,
        e: stor.prenom
      })
      j3pAffiche(tabConcl1[0][2], null, '&nbsp;' + ll1[1], {
        styletext: {},
        c: stor.afnbxg1,
        e: stor.prenom
      })
      let conctab = addDefaultTable(tabConcl1[0][1], 1, 2)
      stor.lesdiv.concl1 = conctab[0][0]
      stor.lesdiv.concl1Co = conctab[0][1]
      ll1 = stor.pbencours.concl2.split('$£a$')
      j3pAffiche(tabConcl2[0][0], null, ll1[0] + '&nbsp;', {
        styletext: {},
        d: stor.afnbxg2,
        f: stor.ilelle
      })
      j3pAffiche(tabConcl2[0][2], null, '&nbsp;' + ll1[1], {
        styletext: {},
        d: stor.afnbxg2,
        f: stor.ilelle
      })
      conctab = addDefaultTable(tabConcl2[0][1], 1, 2)
      stor.lesdiv.concl2 = conctab[0][0]
      stor.lesdiv.concl2Co = conctab[0][1]
      stor.zoneConcl1 = new ZoneStyleMathquill2(stor.lesdiv.concl1, { id: 'Colkdo1', restric: stor.restric })
      stor.zoneConcl2 = new ZoneStyleMathquill2(stor.lesdiv.concl2, { id: 'Colkdo2', restric: stor.restric })
    } else {
      if (((me.questionCourante % ds.nbetapes) === 1) || (ds.nbetapes === 1)) {
        me.fenetresjq = [{ name: 'Calculatrice', title: 'Une calculatrice', left: 600, top: 10 }]
        j3pCreeFenetres(me)
        j3pAfficheCroixFenetres('Calculatrice')
        const ii = j3pAddElt(me.zonesElts.MD, 'div', {
          style: me.styles.etendre('toutpetit.enonce', { padding: '10px' })
        })
        j3pAjouteBouton(ii, j3pToggleFenetres.bind(null, 'Calculatrice'), { className: 'MepBoutons', value: 'Calculatrice' })

        stor.cent = false
        let frac = false
        const cpos = ds.Positifs || (j3pGetRandomInt(0, 1) === 1)
        if ((ds.Ecriture === 'fractionnaire') || ((ds.Ecriture === 'les deux') && (j3pGetRandomBool()))) frac = true
        if (ds.Entiers || (j3pGetRandomInt(0, 2) === 2)) stor.cent = true

        stor.decim = !(stor.cent || frac)

        stor.yaeulegende = false
        stor.yaeuremplir = false
        ok = false
        cmpt = 0
        const pbpos = []

        for (let i = 0; i < textes.pb.length; i++) {
          if ((stor.cent) || (textes.pb[i].type.indexOf('ent') === -1)) {
            if ((cpos) || (textes.pb[i].type.indexOf('pos') === -1)) {
              pbpos.push([textes.pb[i], i])
            }
          }
        }
        let indexg
        do {
          cmpt++
          const i = j3pGetRandomInt(0, (pbpos.length - 1))
          stor.pbencours = pbpos[i][0]
          ok = (stor.pbfait.indexOf(pbpos[i][1]) === -1)
          indexg = i
        } while ((cmpt < 100) && (!ok))
        stor.pbfait.push(pbpos[indexg][1])

        if ((me.questionCourante + me.genre) % 2 === 0) {
          ok = false
          cmpt = 0
          do {
            cmpt++
            const i = j3pGetRandomInt(0, (textes.prenomsf.length - 1))
            stor.prenom = textes.prenomsf[i]
            ok = (stor.ffait.indexOf(i) === -1)
            indexg = i
          } while ((cmpt < 100) && (!ok))
          stor.ilelle = 'elle'
          stor.ffait.push(indexg)
        } else {
          ok = false
          cmpt = 0
          do {
            cmpt++
            const i = j3pGetRandomInt(0, (textes.prenomsg.length - 1))
            stor.prenom = textes.prenomsg[i]
            ok = (stor.gfait.indexOf(i) === -1)
            indexg = i
          } while ((cmpt < 100) && (!ok))
          stor.ilelle = 'il'
          stor.gfait.push(indexg)
        }

        ok = false
        cmpt = 0
        do {
          cmpt++
          const i = j3pGetRandomInt(0, (stor.menuChoices.length - 1))
          stor.methodeVisee = stor.menuChoices[i].name
          ok = (stor.methodesposfait.indexOf(i) === -1)
          indexg = i
        } while ((cmpt < 100) && (!ok))
        stor.methodesposfait.push(indexg)
        if ((stor.menuChoices.length === 2) && (ds.Unite) && (!ds.Addition)) stor.methodeVisee = 'passageUnite'

        // det nbgrd1  A VOIR
        let monok
        let monpcpt = 0
        do {
          monpcpt++
          let x1, x2, base, base2, varmulpo, muls1, muls2, muls3, prems1, prems2, prems3, mul1, mul2, mul3, coef, den1, den2
          if (stor.methodeVisee === 'op1col') {
            if (stor.cent) {
              base = j3pGetRandomInt(2, 30)
              base2 = premsavec(base, stor.pbencours.limitebasse, stor.pbencours.limitehaute)

              varmulpo = [1, 2, 3, 4, 5, 6, 7, 8, 9, 10, 11, 100, 200]
              mul1 = varmulpo[j3pGetRandomInt(1, (varmulpo.length - 1))]
              do {
                mul2 = varmulpo[j3pGetRandomInt(1, (varmulpo.length - 1))]
              } while (mul1 === mul2)

              muls1 = muls2 = muls3 = 1
              prems1 = prems2 = prems3 = true
              const i1 = j3pGetRandomInt(0, 2)
              switch (i1) {
                case 0 :
                  muls1 = muls1 * mul2
                  prems1 = false
                  break
                case 1 :
                  muls2 = muls2 * mul2
                  prems2 = false
                  break
                case 2 :
                  muls3 = muls3 * mul2
                  prems3 = false
                  break
              }
              let i2
              do {
                i2 = j3pGetRandomInt(0, 2)
              } while (i2 === i1)
              switch (i2) {
                case 0 :
                  muls1 = muls1 * mul1
                  prems1 = false
                  break
                case 1 :
                  muls2 = muls2 * mul1
                  prems2 = false
                  break
                case 2 :
                  muls3 = muls3 * mul1
                  prems3 = false
                  break
              }
              stor.nb1g1 = { num: muls1 * base, den: 1 }
              stor.nb1g2 = { num: muls1 * base2, den: 1 }
              stor.nbxg1 = { num: muls2 * base, den: 1 }
              stor.nbxg2 = { num: muls3 * base2, den: 1 }
              stor.aideco = [prems1, prems2, prems3]
            } else if (frac) {
              base = j3pGetRandomInt(2, 30)
              base2 = premsavec(base)
              const base12 = premsavec(base)
              const base22 = premsavec(base2)

              varmulpo = [1, 2, 3, 4, 5, 6, 7, 8, 9, 10, 11, 100, 200]
              mul1 = varmulpo[j3pGetRandomInt(1, (varmulpo.length - 1))]
              do {
                mul2 = varmulpo[j3pGetRandomInt(1, (varmulpo.length - 1))]
              } while (mul1 === mul2)

              muls1 = muls2 = muls3 = 1
              prems1 = prems2 = prems3 = true
              const i1 = j3pGetRandomInt(0, 2)
              switch (i1) {
                case 0 :
                  muls1 = muls1 * mul2
                  prems1 = false
                  break
                case 1 :
                  muls2 = muls2 * mul2
                  prems2 = false
                  break
                case 2 :
                  muls3 = muls3 * mul2
                  prems3 = false
                  break
              }
              let i2
              do {
                i2 = j3pGetRandomInt(0, 2)
              } while (i2 === i1)
              switch (i2) {
                case 0 :
                  muls1 = muls1 * mul1
                  prems1 = false
                  break
                case 1 :
                  muls2 = muls2 * mul1
                  prems2 = false
                  break
                case 2 :
                  muls3 = muls3 * mul1
                  prems3 = false
                  break
              }
              stor.nb1g1 = { num: muls1 * base, den: base12 }
              stor.nb1g2 = { num: muls1 * base2, den: base22 }
              stor.nbxg1 = { num: muls2 * base, den: base12 }
              stor.nbxg2 = { num: muls3 * base2, den: base22 }
              stor.aideco = [prems1, prems2, prems3]
            } else {
              base = j3pGetRandomInt(2, 30)
              base2 = premsavec(base, stor.pbencours.limitebasse, stor.pbencours.limitehaute)

              varmulpo = [1, 2, 3, 4, 5, 6, 7, 8, 9, 10, 11, 20, 30, 40, 50, 100, 200]
              mul1 = varmulpo[j3pGetRandomInt(1, (varmulpo.length - 1))]
              do {
                mul2 = varmulpo[j3pGetRandomInt(1, (varmulpo.length - 1))]
              } while (mul1 === mul2)

              muls1 = muls2 = muls3 = 1
              prems1 = prems2 = prems3 = true
              const i1 = j3pGetRandomInt(0, 2)
              switch (i1) {
                case 0 :
                  muls1 = muls1 * mul2
                  prems1 = false
                  break
                case 1 :
                  muls2 = muls2 * mul2
                  prems2 = false
                  break
                case 2 :
                  muls3 = muls3 * mul2
                  prems3 = false
                  break
              }
              let i2
              do {
                i2 = j3pGetRandomInt(0, 2)
              } while (i2 === i1)
              switch (i2) {
                case 0 :
                  muls1 = muls1 * mul1
                  prems1 = false
                  break
                case 1 :
                  muls2 = muls2 * mul1
                  prems2 = false
                  break
                case 2 :
                  muls3 = muls3 * mul1
                  prems3 = false
                  break
              }
              stor.nb1g1 = { num: muls1 * base, den: 100 }
              stor.nb1g2 = { num: muls1 * base2, den: 100 }
              stor.nbxg1 = { num: muls2 * base, den: 100 }
              stor.nbxg2 = { num: muls3 * base2, den: 100 }
              stor.aideco = [prems1, prems2, prems3]
            }
            stor.nb2g1 = { num: 1, den: 1 }
            stor.nb2g2 = { num: 1, den: 1 }
            if (!cpos) {
              stor.nb1g1.num = stor.nb1g1.num * Math.pow(-1, j3pGetRandomInt(0, 1))
              stor.nb1g2.num = stor.nb1g2.num * Math.pow(-1, j3pGetRandomInt(0, 1))
              stor.nbxg1.num = stor.nbxg1.num * Math.pow(-1, j3pGetRandomInt(0, 1))
              stor.nbxg2.num = stor.nbxg2.num * Math.pow(-1, j3pGetRandomInt(0, 1))
            }
          }
          if (stor.methodeVisee === 'passageUnite') {
            if (stor.cent) {
              let valunit, vg1, vg2
              if (stor.pbencours.limitebasse > 1) {
                valunit = j3pGetRandomInt(stor.pbencours.limitebasse, stor.pbencours.limitehaute)
                vg1 = 1
                vg2 = valunit
                stor.aideco = true
              } else {
                if (j3pGetRandomBool()) {
                  vg1 = 1
                  vg2 = j3pGetRandomInt(2, stor.pbencours.limitehaute)
                  stor.aideco = true
                } else {
                  vg1 = j3pGetRandomInt(2, 1 / stor.pbencours.limitebasse)
                  vg2 = 1
                  stor.aideco = false
                }
              }
              const valxvec1 = j3pGetRandomInt(2, 9)
              const valxvec2 = premsavec(valxvec1)
              const valxvec3 = premsavec2(valxvec1, valxvec2)

              stor.nb1g1 = { num: valxvec1 * vg1, den: 1 }
              stor.nb1g2 = { num: valxvec1 * vg2, den: 1 }
              stor.nbxg1 = { num: valxvec2 * vg1, den: 1 }
              stor.nbxg2 = { num: valxvec3 * vg2, den: 1 }
            } else {
              if (frac) {
                const nb2 = j3pGetRandomInt(0, 3)
                const min5 = (nb2 === 0) ? 1 : 0
                const nb5 = j3pGetRandomInt(min5, (3 - nb2))
                const valunit = Math.pow(2, nb2) * +Math.pow(5, nb5)
                let vg1, vg2
                if (j3pGetRandomBool()) {
                  vg1 = 1
                  vg2 = valunit
                  stor.aideco = true
                } else {
                  vg1 = valunit
                  vg2 = 1
                  stor.aideco = false
                }

                const valxvec1 = j3pGetRandomInt(2, 9)
                const valxvec2 = premsavec(valxvec1)
                const valxvec3 = premsavec2(valxvec1, valxvec2)
                const valden = premsavec2(vg1, vg2)

                stor.nb1g1 = { num: valxvec1 * vg1, den: valden }
                stor.nb1g2 = { num: valxvec1 * vg2, den: valden }
                stor.nbxg1 = { num: valxvec2 * vg1, den: valden }
                stor.nbxg2 = { num: valxvec3 * vg2, den: valden }
              } else {
                const nb2 = j3pGetRandomInt(0, 3)
                const min5 = (nb2 === 0) ? 1 : 0
                const nb5 = j3pGetRandomInt(min5, (3 - nb2))
                const valunit = Math.pow(2, nb2) * +Math.pow(5, nb5)
                let vg1, vg2
                if (j3pGetRandomBool()) {
                  vg1 = 1
                  vg2 = valunit
                  stor.aideco = true
                } else {
                  vg1 = valunit
                  vg2 = 1
                  stor.aideco = false
                }

                const valxvec1 = j3pGetRandomInt(2, 9)
                const valxvec2 = premsavec(valxvec1)
                const valxvec3 = premsavec2(valxvec1, valxvec2)

                stor.nb1g1 = { num: valxvec1 * vg1, den: 100 }
                stor.nb1g2 = { num: valxvec1 * vg2, den: 100 }
                stor.nbxg1 = { num: valxvec2 * vg1, den: 100 }
                stor.nbxg2 = { num: valxvec3 * vg2, den: 100 }
              }
            }
            stor.nb2g1 = { num: 1, den: 1 }
            stor.nb2g2 = { num: 1, den: 1 }
            if (!cpos) {
              stor.nb1g1.num = stor.nb1g1.num * Math.pow(-1, j3pGetRandomInt(0, 1))
              stor.nb1g2.num = stor.nb1g2.num * Math.pow(-1, j3pGetRandomInt(0, 1))
              stor.nbxg1.num = stor.nbxg1.num * Math.pow(-1, j3pGetRandomInt(0, 1))
              stor.nbxg2.num = stor.nbxg2.num * Math.pow(-1, j3pGetRandomInt(0, 1))
            }
          }
          if ((stor.methodeVisee === 'aj2col') || (stor.methodeVisee === 'sub2col')) {
            stor.cent = true
            if (stor.cent) {
              base = j3pGetRandomInt(2, 30)
              base2 = premsavec(base, stor.pbencours.limitebasse, stor.pbencours.limitehaute)

              mul1 = j3pGetRandomInt(2, 9)
              mul2 = premsavec(mul1)
              if (!cpos) {
                mul1 = mul1 * Math.pow(-1, j3pGetRandomInt(0, 1))
                mul2 = mul2 * Math.pow(-1, j3pGetRandomInt(0, 1))
              }

              if (j3pGetRandomBool()) {
                x1 = mul1 * base + mul2 * base
                x2 = Math.max(mul1, mul2) * base2 - Math.min(mul2, mul1) * base2
                stor.aideco = true
              } else {
                x1 = Math.max(mul1, mul2) * base - Math.min(mul2, mul1) * base
                x2 = mul1 * base2 + mul2 * base2
                stor.aideco = false
              }

              stor.nb1g1 = { num: mul1 * base, den: 1 }
              stor.nb1g2 = { num: mul1 * base2, den: 1 }
              stor.nb2g1 = { num: mul2 * base, den: 1 }
              stor.nb2g2 = { num: mul2 * base2, den: 1 }
              stor.nbxg1 = { num: x1, den: 1 }
              stor.nbxg2 = { num: x2, den: 1 }
            } else {
              if (frac) {
                base = j3pGetRandomInt(2, 30)
                base2 = premsavec(base)

                mul1 = j3pGetRandomInt(2, 9)
                mul2 = premsavec(mul1)

                den1 = premsavec2(base, mul1)
                den2 = premsavec2(base2, mul1)

                if (!cpos) {
                  mul1 = mul1 * Math.pow(-1, j3pGetRandomInt(0, 1))
                  mul2 = mul2 * Math.pow(-1, j3pGetRandomInt(0, 1))
                }

                if (j3pGetRandomBool()) {
                  x1 = mul1 * base + mul2 * base
                  x2 = Math.max(mul1, mul2) * base2 - Math.min(mul2, mul1) * base2
                  stor.aideco = true
                } else {
                  x1 = Math.max(mul1, mul2) * base - Math.min(mul2, mul1) * base
                  x2 = mul1 * base2 + mul2 * base2
                  stor.aideco = false
                }

                stor.nb1g1 = { num: mul1 * base, den: den1 }
                stor.nb1g2 = { num: mul1 * base2, den: den2 }
                stor.nb2g1 = { num: mul2 * base, den: den1 }
                stor.nb2g2 = { num: mul2 * base2, den: den2 }
                stor.nbxg1 = { num: x1, den: den1 }
                stor.nbxg2 = { num: x2, den: den2 }
              } else {
                base = j3pGetRandomInt(2, 30)
                base2 = premsavec(base, stor.pbencours.limitebasse, stor.pbencours.limitehaute)

                mul1 = j3pGetRandomInt(2, 9)
                mul2 = premsavec(mul1)

                if (!cpos) {
                  mul1 = mul1 * Math.pow(-1, j3pGetRandomInt(0, 1))
                  mul2 = mul2 * Math.pow(-1, j3pGetRandomInt(0, 1))
                }

                if (j3pGetRandomBool()) {
                  x1 = mul1 * base + mul2 * base
                  x2 = Math.max(mul1, mul2) * base2 - Math.min(mul2, mul1) * base2
                  stor.aideco = true
                } else {
                  x1 = Math.max(mul1, mul2) * base - Math.min(mul2, mul1) * base
                  x2 = mul1 * base2 + mul2 * base2
                  stor.aideco = false
                }

                stor.nb1g1 = { num: mul1 * base, den: 100 }
                stor.nb1g2 = { num: mul1 * base2, den: 100 }
                stor.nb2g1 = { num: mul2 * base, den: 100 }
                stor.nb2g2 = { num: mul2 * base2, den: 100 }
                stor.nbxg1 = { num: x1, den: 100 }
                stor.nbxg2 = { num: x2, den: 100 }
              }
            }
          }
          if (stor.methodeVisee === 'coef') {
            if (stor.cent) {
              varmulpo = [1, 2, 3, 4, 5, 6, 7, 8, 9, 10, 11, 20, 30, 40, 50, 100, 200]
              for (let i = varmulpo.length; i > -1; i--) {
                if (varmulpo[i] < stor.pbencours.limitebasse || varmulpo[i] > stor.pbencours.limitehaute) {
                  varmulpo.splice(i, 1)
                }
              }
              coef = varmulpo[j3pGetRandomInt(1, (varmulpo.length - 1))]

              base = premsavec(coef)
              base2 = coef * base

              mul1 = premsavec2(base, coef)
              mul2 = premsavec2(mul1, base)
              mul3 = premsavec2(mul1, mul2)

              stor.nb1g1 = { num: (mul1 * base), den: 1 }
              stor.nb1g2 = { num: (mul1 * base2), den: 1 }
              stor.nbxg1 = { num: (mul2 * base), den: 1 }
              stor.nbxg2 = { num: (mul3 * base2), den: 1 }
            } else {
              if (frac) {
                varmulpo = [1, 2, 3, 4, 5, 6, 7, 8, 9, 10, 11, 100, 200]
                coef = varmulpo[j3pGetRandomInt(1, (varmulpo.length - 1))]

                base = premsavec(coef)
                base2 = coef * base

                mul1 = premsavec2(base, coef)
                mul2 = premsavec2(mul1, base)
                mul3 = premsavec(mul1, mul2)

                den1 = premsavec(base)
                den2 = premsavec(base2)

                stor.nb1g1 = {
                  num: (mul1 * base) / j3pPGCD(mul1, den1, { negativesAllowed: true, valueIfZero: 1 }),
                  den: den1 / (j3pPGCD(den1, mul1, { negativesAllowed: true, valueIfZero: 1 }))
                }
                stor.nb1g2 = {
                  num: (mul1 * base2) / j3pPGCD(mul1, den2, { negativesAllowed: true, valueIfZero: 1 }),
                  den: den2 / (j3pPGCD(den2, mul1, { negativesAllowed: true, valueIfZero: 1 }))
                }
                stor.nbxg1 = {
                  num: (mul2 * base) / j3pPGCD(mul2, den1, { negativesAllowed: true, valueIfZero: 1 }),
                  den: den1 / (j3pPGCD(mul2, den1, { negativesAllowed: true, valueIfZero: 1 }))
                }
                stor.nbxg2 = {
                  num: (mul3 * base2) / j3pPGCD(mul3, den2, { negativesAllowed: true, valueIfZero: 1 }),
                  den: den2 / j3pPGCD(mul3, den2, { negativesAllowed: true, valueIfZero: 1 })
                }
              } else {
                varmulpo = [1, 2, 3, 4, 5, 6, 7, 8, 9, 10, 11, 20, 30, 40, 50, 100, 200]
                for (let i = varmulpo.length; i > -1; i--) {
                  if (varmulpo[i] < stor.pbencours.limitebasse || varmulpo[i] > stor.pbencours.limitehaute) {
                    varmulpo.splice(i, 1)
                  }
                }
                coef = varmulpo[j3pGetRandomInt(1, (varmulpo.length - 1))]

                base = premsavec(coef)
                base2 = coef * base

                mul1 = premsavec2(base, coef)
                mul2 = premsavec2(mul1, base)
                mul3 = premsavec(mul1, mul2)

                stor.nb1g1 = { num: mul1 * base, den: 100 }
                stor.nb1g2 = { num: mul1 * base2, den: 100 }
                stor.nbxg1 = { num: mul2 * base, den: 100 }
                stor.nbxg2 = { num: mul3 * base2, den: 100 }
              }
            }
            stor.nb2g1 = { num: 1, den: 1 }
            stor.nb2g2 = { num: 1, den: 1 }
            if (!cpos) {
              stor.nb1g1.num = stor.nb1g1.num * Math.pow(-1, j3pGetRandomInt(0, 1))
              stor.nb1g2.num = stor.nb1g2.num * Math.pow(-1, j3pGetRandomInt(0, 1))
              stor.nbxg1.num = stor.nbxg1.num * Math.pow(-1, j3pGetRandomInt(0, 1))
              stor.nbxg2.num = stor.nbxg2.num * Math.pow(-1, j3pGetRandomInt(0, 1))
            }
          }
          if (stor.methodeVisee === 'prodCroix') {
            if (stor.cent) {
              varmulpo = [1, 2, 3, 4, 5, 6, 7, 8, 9, 10, 11, 20, 30, 40, 50, 100, 200]
              for (let i = varmulpo.length - 1; i > -1; i--) {
                if (varmulpo[i] < stor.pbencours.limitebasse || varmulpo[i] > stor.pbencours.limitehaute) {
                  varmulpo.splice(i, 1)
                }
              }
              base = varmulpo[j3pGetRandomInt(0, (varmulpo.length - 1))]
              if (base === undefined) {
                // 2
                j3pNotify('base undefined 2', { limitebasse: stor.pbencours.limitebasse, limitehaute: stor.pbencours.limitehaute, varmulpo })
                base = 2
              }
              base2 = premsavec(base)

              mul1 = premsavec2(base, base2)
              mul2 = premsavec2(mul1, base)
              mul3 = premsavec(mul1, mul2)

              stor.nb1g1 = { num: (mul1 * base), den: 1 }
              stor.nb1g2 = { num: (mul1 * base2), den: 1 }
              stor.nbxg1 = { num: (mul2 * base), den: 1 }
              stor.nbxg2 = { num: (mul3 * base2), den: 1 }
            } else {
              if (frac) {
                varmulpo = [1, 2, 3, 4, 5, 6, 7, 8, 9, 10, 11, 20, 30, 40, 50, 100, 200]
                base = varmulpo[j3pGetRandomInt(0, (varmulpo.length - 1))]
                base2 = premsavec(base)

                mul1 = premsavec2(base, base2)
                mul2 = premsavec2(mul1, base)
                mul3 = premsavec(mul1, mul2)

                den1 = premsavec(base)
                den2 = premsavec(base2)

                stor.nb1g1 = {
                  num: (mul1 * base) / j3pPGCD(mul1, den1, { negativesAllowed: true, valueIfZero: 1 }),
                  den: den1 / (j3pPGCD(den1, mul1, { negativesAllowed: true, valueIfZero: 1 }))
                }
                stor.nb1g2 = {
                  num: (mul1 * base2) / j3pPGCD(mul1, den2, { negativesAllowed: true, valueIfZero: 1 }),
                  den: den2 / (j3pPGCD(den2, mul1, { negativesAllowed: true, valueIfZero: 1 }))
                }
                stor.nbxg1 = {
                  num: (mul2 * base) / j3pPGCD(mul2, den1, { negativesAllowed: true, valueIfZero: 1 }),
                  den: den1 / (j3pPGCD(mul2, den1, { negativesAllowed: true, valueIfZero: 1 }))
                }
                stor.nbxg2 = {
                  num: (mul3 * base2) / j3pPGCD(mul3, den2, { negativesAllowed: true, valueIfZero: 1 }),
                  den: den2 / j3pPGCD(mul3, den2, { negativesAllowed: true, valueIfZero: 1 })
                }
              } else {
                varmulpo = [1, 2, 3, 4, 5, 6, 7, 8, 9, 10, 11, 20, 30, 40, 50, 100, 200]
                for (let i = varmulpo.length - 1; i > -1; i--) {
                  if (varmulpo[i] < stor.pbencours.limitebasse || varmulpo[i] > stor.pbencours.limitehaute) {
                    varmulpo.splice(i, 1)
                  }
                }
                base = varmulpo[j3pGetRandomInt(0, (varmulpo.length - 1))]
                if (base === undefined) {
                  // 1
                  j3pNotify('base undefined 1', { limitebasse: stor.pbencours.limitebasse, limitehaute: stor.pbencours.limitehaute, varmulpo })
                  base = 2
                }
                base2 = premsavec(base)
                mul1 = premsavec2(base, base2)
                mul2 = premsavec2(mul1, base)
                mul3 = premsavec(mul1, mul2)

                stor.nb1g1 = { num: mul1 * base, den: 100 }
                stor.nb1g2 = { num: mul1 * base2, den: 100 }
                stor.nbxg1 = { num: mul2 * base, den: 100 }
                stor.nbxg2 = { num: mul3 * base2, den: 100 }
              }
            }
            stor.nb2g1 = { num: 1, den: 1 }
            stor.nb2g2 = { num: 1, den: 1 }
            if (!cpos) {
              stor.nb1g1.num = stor.nb1g1.num * Math.pow(-1, j3pGetRandomInt(0, 1))
              stor.nb1g2.num = stor.nb1g2.num * Math.pow(-1, j3pGetRandomInt(0, 1))
              stor.nbxg1.num = stor.nbxg1.num * Math.pow(-1, j3pGetRandomInt(0, 1))
              stor.nbxg2.num = stor.nbxg2.num * Math.pow(-1, j3pGetRandomInt(0, 1))
            }
          }
          monok = true
          if (
            (stor.nb1g1.num === stor.nb1g2.num && stor.nb1g1.den === stor.nb1g2.den) ||
            (stor.nb1g1.num === stor.nbxg2.num && stor.nb1g1.den === stor.nbxg2.den) ||
            (stor.nb1g1.num === stor.nbxg1.num && stor.nb1g1.den === stor.nbxg1.den) ||
            (stor.nb1g2.num === stor.nbxg2.num && stor.nb1g2.den === stor.nbxg2.den) ||
            (stor.nb1g2.num === stor.nbxg1.num && stor.nb1g2.den === stor.nbxg1.den) ||
            (stor.nbxg1.num === stor.nbxg2.num && stor.nbxg1.den === stor.nbxg2.den)
          ) {
            monok = false
          } else if ((stor.methodeVisee === 'aj2col') || (stor.methodeVisee === 'sub2col')) {
            if (
              (stor.nb2g1.num === stor.nb1g2.num && stor.nb2g1.den === stor.nb1g2.den) ||
              (stor.nb2g2.num === stor.nb1g2.num && stor.nb2g2.den === stor.nb1g2.den) ||
              (stor.nb2g1.num === stor.nb1g1.num && stor.nb2g1.den === stor.nb1g1.den) ||
              (stor.nb2g2.num === stor.nb1g1.num && stor.nb2g2.den === stor.nb1g1.den) ||
              (stor.nb2g1.num === stor.nbxg1.num && stor.nb2g1.den === stor.nbxg1.den) ||
              (stor.nb2g1.num === stor.nbxg2.num && stor.nb2g1.den === stor.nbxg2.den) ||
              (stor.nb2g2.num === stor.nbxg1.num && stor.nb2g2.den === stor.nbxg1.den) ||
              (stor.nb2g2.num === stor.nbxg2.num && stor.nb2g2.den === stor.nbxg2.den)
            ) {
              monok = false
            }
          }
        } while (monpcpt < 100 && !monok)

        stor.restric = '0123456789'
        if (stor.cent) {
          stor.MaxL = 7
        } else {
          stor.restric += '.,/'
          if (frac) {
            stor.MaxL = 16
            stor.restric += '/'
          } else {
            stor.MaxL = 7
            stor.restric += '.,'
          }
        }
        if (!cpos) {
          stor.MaxL++
          stor.restric += '-'
          if ((frac) && (!stor.cent)) stor.MaxL++
        }

        stor.afnb1g1 = calcul(stor.decim, tabloise(stor.nb1g1), 1, 'i')
        stor.afnb1g2 = calcul(stor.decim, tabloise(stor.nb1g2), 1, 'i')
        stor.afnb2g1 = calcul(stor.decim, tabloise(stor.nb2g1), 1, 'i')
        stor.afnb2g2 = calcul(stor.decim, tabloise(stor.nb2g2), 1, 'i')
        stor.afnbxg1 = calcul(stor.decim, tabloise(stor.nbxg1), 1, 'i')
        stor.afnbxg2 = calcul(stor.decim, tabloise(stor.nbxg2), 1, 'i')

        // Dans ce conteneur, on crée un div par phrase (cela forcera le retour à la ligne)
        stor.lesdiv.conteneur = j3pAddElt(me.zonesElts.MG, 'div', {
          style: me.styles.etendre('toutpetit.enonce', { padding: '10px' })
        })
        stor.lesdiv.consigne = addDefaultTable(stor.lesdiv.conteneur, 1, 1)[0][0]
        stor.lesdiv.travail = j3pAddElt(stor.lesdiv.conteneur, 'div')
        stor.lesdiv.etape = j3pAddElt(stor.lesdiv.travail, 'div')
        stor.lesdiv.blabliblo = j3pAddElt(me.zonesElts.MD, 'div', {
          style: me.styles.etendre('toutpetit.enonce', { padding: '10px' })
        })
        stor.lesdiv.supCro = j3pAddElt(me.zonesElts.MD, 'div', {
          style: me.styles.etendre('toutpetit.enonce', { padding: '10px' })
        })

        const UUU = addDefaultTable(stor.lesdiv.travail, 6, 1)
        UUU[0][0].style.padding = 0
        UUU[1][0].style.padding = 0
        UUU[2][0].style.padding = 0
        UUU[3][0].style.padding = 0
        UUU[4][0].style.padding = 0
        UUU[5][0].style.padding = 0
        UUU[0][0].style.height = '10px'
        UUU[1][0].style.height = '30px'
        UUU[2][0].style.height = '25px'
        stor.lesdiv.vide1 = UUU[1][0]
        stor.lesdiv.vide2 = UUU[2][0]
        const mimim = addDefaultTable(UUU[3][0], 1, 4)
        mimim[0][0].style.padding = 0
        mimim[0][1].style.padding = 0
        mimim[0][2].style.padding = 0
        mimim[0][3].style.padding = 0
        stor.lesdiv.tableau = mimim[0][0]
        stor.lesdiv.ar0 = mimim[0][2]
        stor.lesdiv.ar1 = mimim[0][2]
        stor.lesdiv.ar2 = mimim[0][3]
        stor.ar1use = false
        stor.ar2use = false
        stor.lesdiv.vide3 = UUU[4][0]
        stor.lesdiv.vide3.style.paddingTop = '2px'
        stor.lesdiv.vide4 = UUU[5][0]
        stor.lesdiv.conclusion = j3pAddElt(stor.lesdiv.travail, 'div')
        stor.lesdiv.explik2 = j3pAddElt(stor.lesdiv.conteneur, 'div')
        stor.lesdiv.explik2.style.color = me.styles.cfaux
        stor.lesdiv.explik = j3pAddElt(stor.lesdiv.conteneur, 'div')
        stor.lesdiv.explik2.style.align = 'left'
        stor.lesdiv.explik.style.align = 'left'
        stor.lesdiv.explik.style.color = me.styles.petit.correction.color
        stor.nbRepAtt = 4
        let lacons = stor.pbencours.Enoncedeb.replace('£e', stor.prenom)
        lacons += '<BR>' + stor.pbencours.donnees.replace('£a', stor.afnb1g1).replace('£b', stor.afnb1g2).replace('£f', stor.ilelle)
        if ((stor.methodeVisee === 'aj2col') || (stor.methodeVisee === 'sub2col')) {
          lacons += '<BR>' + stor.pbencours.donnees.replace('£a', stor.afnb2g1).replace('£b', stor.afnb2g2).replace('£f', stor.ilelle)
          stor.nbRepAtt = 6
        }
        lacons += '<BR>' + stor.pbencours.Enoncefin.replace('£c', stor.afnbxg1).replace('£d', stor.afnbxg2).replace('£f', stor.ilelle).replace('£f', stor.ilelle)
        const nbBr = (ds.theme === 'zonesAvecImageDeFond') ? 3 : 2
        j3pAffiche(stor.lesdiv.consigne, null, lacons + '\n'.repeat(nbBr))
      } else {
        stor.lesdiv.explik.innerHTML = ''
        stor.lesdiv.explik2.innerHTML = ''
        stor.lesdiv.explik.classList.remove('correction')
        stor.lesdiv.explik2.classList.remove('explique')
      }

      let opts, container
      if (stor.etapeAfaire[me.questionCourante % ds.nbetapes] === 'Legende') {
        stor.yaeulegende = true
        stor.lesdiv.etape.innerHTML = ''
        j3pAffiche(stor.lesdiv.etape, null, textes.etapeLegende, {
          styletext: {},
          a: me.questionCourante % ds.nbetapes
        })
        // on ajoute le tableau
        opts = 4
        if (stor.nbRepAtt === 6) opts = 5
        container = stor.lesdiv.tableau

        stor.tabProp = makeDef(container, 4, opts)
        stor.liste1 = [stor.pbencours.legende1, stor.pbencours.folegende11, stor.pbencours.folegende12, stor.pbencours.folegende13]
        stor.liste1 = j3pShuffle(stor.liste1)
        stor.liste1.splice(0, 0, 'Choisir')
        stor.liste2 = [stor.pbencours.legende2, stor.pbencours.folegende21, stor.pbencours.folegende22, stor.pbencours.folegende23]
        stor.liste2 = j3pShuffle(stor.liste2)
        stor.liste2.splice(0, 0, 'Choisir')
        stor.ListeDeroulante1 = ListeDeroulante.create(stor.tabProp[1][0], stor.liste1, { centre: true, alignmathquill: true })
        stor.ListeDeroulante2 = ListeDeroulante.create(stor.tabProp[2][0], stor.liste2, { centre: true, alignmathquill: true })
      } else {
        if (!stor.yaeulegende) {
          // on ajoute le tableau
          opts = (stor.nbRepAtt === 6) ? 5 : 4
          j3pEmpty(stor.lesdiv.tableau)
          stor.tabProp = makeDef(stor.lesdiv.tableau, 4, opts)
          stor.Etapefaite.push('Legende')
        }

        if (stor.Etapefaite.indexOf('Legende') !== -1) {
          for (let i = stor.avireee.length - 1; i > -1; i++) {
            j3pDetruit(stor.avireee.pop())
          }
          stor.tabProp[1][0].innerHTML = stor.pbencours.legende1
          stor.tabProp[2][0].innerHTML = stor.pbencours.legende2
        }

        if (stor.etapeAfaire[me.questionCourante % ds.nbetapes] === 'Remplir') {
          stor.yaeuremplir = true
          stor.lesdiv.etape.innerHTML = ''
          j3pAffiche(stor.lesdiv.etape, null, textes.etapeRemplir, {
            styletext: {},
            a: me.questionCourante % ds.nbetapes
          })

          stor.listeInput = []
          for (let i = 0; i < 4; i++) {
            if (stor.tabProp[1][i + 1]) {
              j3pEmpty(stor.tabProp[1][i + 1])
              stor.listeInput.push(new ZoneStyleMathquill2(stor.tabProp[1][i + 1], { id: 'input1h' + i, restric: stor.restric, hasAutoKeyboard: (stor.restric.indexOf('/') !== -1), enter: me.sectionCourante.bind(me), limite: 9 }))
            }
            if (stor.tabProp[2][i + 1]) {
              j3pEmpty(stor.tabProp[2][i + 1])
              stor.listeInput.push(new ZoneStyleMathquill2(stor.tabProp[2][i + 1], { id: 'input2h' + i, restric: stor.restric, hasAutoKeyboard: (stor.restric.indexOf('/') !== -1), inverse: true, enter: me.sectionCourante.bind(me), limite: 9 }))
            }
          }
        } else {
          stor.repEl = []
          stor.placeNb = []
          stor.repEl.push(stor.afnb1g1)
          stor.placeNb.push('nb1g1')
          stor.repEl.push(stor.afnb1g2)
          stor.placeNb.push('nb1g2')
          if (stor.nbRepAtt === 6) {
            stor.repEl.push(stor.afnb2g1)
            stor.placeNb.push('nb2g1')
            stor.repEl.push(stor.afnb2g2)
            stor.placeNb.push('nb2g2')
          }
          stor.repEl.push(stor.afnbxg1)
          stor.placeNb.push('nbxg1')
          stor.repEl.push('?')
          stor.placeNb.push('rep1')
          stor.repEl.push('?')
          stor.placeNb.push('rep2')
          stor.repEl.push(stor.afnbxg2)
          stor.placeNb.push('nbxg2')

          for (let i = 0; i < stor.placeNb.length; i++) {
            if ((stor.placeNb[i] === 'rep1') || (stor.placeNb[i] === 'rep2')) {
              stor.repEl[i] = '?'
            }
          }
          for (let i = 0; i < stor.repEl.length; i++) {
            const ligne = (i % 2 === 1) ? 2 : 1
            let colonne
            if (i > 5) colonne = 4
            else if (i > 3) colonne = 3
            else if (i > 1) colonne = 2
            else colonne = 1
            j3pEmpty(stor.tabProp[ligne][colonne])
            stor.tabProp[ligne][colonne].style.color = ''
            j3pAffiche(stor.tabProp[ligne][colonne], null, '$' + stor.repEl[i] + '$')
          }
          if ((stor.etapeAfaire[me.questionCourante % ds.nbetapes] === 'les deux') ||
            (stor.etapeAfaire[me.questionCourante % ds.nbetapes] === 'Raisonnement') ||
            (stor.etapeAfaire[me.questionCourante % ds.nbetapes] === 'Reponse')) {
            stor.boutonValOp = []
            if (ds.nbetapes !== 1) {
              stor.lesdiv.etape.innerHTML = ''
              j3pAffiche(stor.lesdiv.etape, null, textes.etapeCompleter, {
                styletext: {},
                a: me.questionCourante % ds.nbetapes
              })
            }
            if (stor.repEl.length % 2 === 1) {
              stor.repEl.push('')
              stor.placeNb.push('rep1')
            }
            // si 'reponse' met juste des cases
            stor.resultats = []
            stor.listeInput = []

            if (stor.etapeAfaire[me.questionCourante % ds.nbetapes] === 'Reponse') {
              for (let i = 0; i < stor.repEl.length; i++) {
                const ligne = (i % 2 === 1) ? 2 : 1
                let colonne
                if (i > 5) colonne = 4
                else if (i > 3) colonne = 3
                else if (i > 1) colonne = 2
                else colonne = 1
                if (stor.repEl[i] === '?') {
                  j3pEmpty(stor.tabProp[ligne][colonne])
                  stor.listeInput.push(new ZoneStyleMathquill2(stor.tabProp[ligne][colonne], { id: 'input' + ligne + 'c' + colonne, restric: stor.restric, hasAutoKeyboard: (stor.restric.indexOf('/') !== -1), inverse: (ligne % 2 === 1), enter: me.sectionCourante.bind(me), limite: 9 }))
                  stor.listeInput[stor.listeInput.length - 1].onchange = adapteLarg
                  if (ligne === 1) { stor.inputrep2 = stor.listeInput[stor.listeInput.length - 1] } else { stor.inputrep1 = stor.listeInput[stor.listeInput.length - 1] }
                }
              }
            } else {
              me.zonesElts.MG.addEventListener('mousemove', gere5, false)

              stor.caseuprise = false

              stor.ajcolonne = 0
              stor.yacoef = false
              stor.yapdt = false

              let elt
              for (let ss = 0; ss < stor.repEl.length; ss++) {
                const ligne = (ss % 2 === 1) ? 2 : 1
                let colonne
                if (ss > 5) colonne = 4
                else if (ss > 3) colonne = 3
                else if (ss > 1) colonne = 2
                else colonne = 1
                if (stor.repEl[ss] === '?') {
                  j3pEmpty(stor.tabProp[ligne][colonne])
                  elt = j3pAddElt(stor.tabProp[ligne][colonne], 'p', '?', {
                    id: 'l' + ligne + 'c' + colonne,
                    style: {
                      textAlign: 'center',
                      fontWeight: 'bold'
                    }
                  })
                  elt.ligne = ligne
                  elt.colonne = colonne
                  addMenu(elt)
                }
              }
            }

            if (!ds.Conclusion) {
              const tabconcl = addDefaultTable(stor.lesdiv.conclusion, 2, 1)
              const tabConcl1 = addDefaultTable(tabconcl[0][0], 1, 3)
              const tabConcl2 = addDefaultTable(tabconcl[1][0], 1, 3)
              let ll1 = stor.pbencours.concl1.split('$£a$')
              j3pAffiche(tabConcl1[0][0], null, ll1[0] + '&nbsp;', {
                styletext: {},
                c: stor.afnbxg1,
                e: stor.prenom
              })
              j3pAffiche(tabConcl1[0][2], null, '&nbsp;' + ll1[1], {
                styletext: {},
                c: stor.afnbxg1,
                e: stor.prenom
              })
              let conctab = addDefaultTable(tabConcl1[0][1], 1, 2)
              stor.lesdiv.concl1 = conctab[0][0]
              stor.lesdiv.concl1Co = conctab[0][1]
              ll1 = stor.pbencours.concl2.split('$£a$')
              j3pAffiche(tabConcl2[0][0], null, ll1[0] + '&nbsp;', {
                styletext: {},
                d: stor.afnbxg2,
                f: stor.ilelle
              })
              j3pAffiche(tabConcl2[0][2], null, '&nbsp;' + ll1[1], {
                styletext: {},
                d: stor.afnbxg2,
                f: stor.ilelle
              })
              conctab = addDefaultTable(tabConcl2[0][1], 1, 2)
              stor.lesdiv.concl2 = conctab[0][0]
              stor.lesdiv.concl2Co = conctab[0][1]
              j3pAffiche(stor.lesdiv.concl1, null, '$?$')
              j3pAffiche(stor.lesdiv.concl2, null, '$?$')
            }

            if (stor.cent) {
              stor.lesdiv.indic = j3pAddElt(me.zonesElts.MG, 'div', {
                style: me.styles.etendre('toutpetit.correction', { padding: '10px' })
              })
              j3pAffiche(stor.lesdiv.indic, null, '<b><i>Tu ne dois utiliser que des nombres entiers.</i></b>')
            }
          }
        }
      }
    }
    stor.lesdiv.travail.classList.add('travail')
    stor.lesdiv.consigne.classList.add('enonce')

    stor.lesdiv.correction = j3pAddElt(me.zones.MD, 'div', {
      style: me.styles.etendre('toutpetit.correction', { padding: '10px' })
    })
    me.zonesElts.MG.classList.add('fond')
    me.finEnonce()
  }

  /**
   * init stor.choices
   * @private
   */
  function initChoices () {
    // la liste des choix possibles, dans stor car on a besoin de fct qui l’utilisent (ça peut pas être une constante)
    stor.choices = {
      op1col: {
        name: 'op1col',
        label: 'Opération sur une colonne',
        callback: function (menu/*, choice, event */) {
          affCadre('Choisis la colonne de départ', menu.trigger, 'op1col')
          foisRendSelColonnes(menu.trigger)
        }
      },
      aj2col: {
        name: 'aj2col',
        label: 'Ajouter 2 colonnes',
        callback: function (menu) {
          affCadre('Choisis les colonnes de départ', menu.trigger, 'aj2col')
          plusRendSel2Colonnes(menu.trigger)
        }
      },
      sub2col: {
        name: 'sub2col',
        label: 'Soustraire 2 colonnes',
        callback: function (menu) {
          affCadre('Choisis les colonnes de départ', menu.trigger, 'sub2col')
          moinsRendSel2Colonnes(menu.trigger)
        }
      },
      passageUnite: {
        name: 'passageUnite',
        label: 'Passage à l’unité',
        callback: function (menu) {
          addNewCol(menu.trigger)
        }
      },
      coef: {
        name: 'coef',
        label: 'Coefficient',
        callback: function (menu) {
          ajoutCoef(menu.trigger)
        }
      },
      prodCroix: {
        name: 'prodCroix',
        label: 'Produit en croix',
        callback: function (menu) {
          croixRendsel(menu.trigger)
          affCadre('Sélectionne les valeurs', menu.trigger, 'prodCroix')
        }
      }
    }
  }

  function affModale (texte) {
    const jk = j3pModale({ titre: 'Message', contenu: '' })
    j3pAffiche(jk, null, texte, { style: { color: me.styles.toutpetit.correction.color } })
  }

  function detruitMenu (menus, idDeclenche) {
    for (let i = 0; i < menus.length; i++) {
      if (menus[i].trigger === idDeclenche) {
        menus[i].destroy()
        menus.splice(i, 1)
        return
      }
    }
    error('Pas trouvé le menu ' + idDeclenche)
  }

  function pauseAll () {
    for (let i = 0; i < stor.menus.length; i++) {
      stor.menus[i].setActive(false)
    }
    stor.isPaused = true
  }

  function resumeAll () {
    for (let i = 0; i < stor.menus.length; i++) {
      stor.menus[i].setActive(true)
    }
    stor.isPaused = false
  }

  function addMenu (trigger) {
    stor.menus.push(new MenuContextuel(trigger, stor.menuChoices))
  }

  function recupRep (placeNb, repEl, num) {
    const ff = (num === 1) ? 'rep1' : 'rep2'
    for (let i = 0; i < placeNb.length; i++) {
      if (placeNb[i] === ff) {
        return trans(repEl[i])
      }
    }
  }

  function addNewCol (origine) {
    const nCol = stor.tabProp[1].length

    const elt1 = j3pAddElt(stor.tabProp[1][0].parentNode, 'td')
    elt1.ligne = 1
    elt1.colonne = nCol
    stor.tabProp[1].push(elt1)

    const elt2 = j3pAddElt(stor.tabProp[2][0].parentNode, 'td')
    elt2.ligne = 2
    elt2.colonne = nCol
    stor.tabProp[2].push(elt2)

    stor.ajcolonne++
    stor.tabProp[0][0].setAttribute('colspan', nCol + 1)
    stor.tabProp[3][0].setAttribute('colspan', nCol + 1)

    elt1.style.border = '1px solid black'
    elt2.style.border = '1px solid black'
    elt1.style.cursor = 'pointer'
    elt2.style.cursor = 'pointer'
    elt1.style.padding = 0
    elt2.style.padding = 0
    elt1.style.textAlign = 'center'
    elt2.style.textAlign = 'center'
    elt1.style.width = '50px'
    elt1.id2 = elt2
    elt2.id2 = elt1
    elt1.qui = origine
    elt2.qui = origine
    stor.listunite = { dou: origine, k: elt1, k2: elt2 }
    elt1.colSup = stor.tabProp[1].length - 1
    elt2.colSup = stor.tabProp[1].length - 1
    adapteLarg()
    const ell = elt1.ligne === origine.ligne ? elt2 : elt1
    uniteSelect(ell)
  }

  function error (msg) {
    j3pShowError(Error(msg))
  }

  function premsavec (nb, limb, limh) {
    // on cherche un nb premier avec nb
    let sol
    const nbBas = (limb) ? limb * nb : 2
    const nbHaut = Math.max((limh) ? limh * nb : nbBas * 2, 30)
    let i = 0 // protection contre les boucles infinies
    do {
      sol = j3pGetRandomInt(nbBas, nbHaut)
      i++
    } while (j3pPGCD(nb, sol, { negativesAllowed: true, valueIfZero: 1 }) !== 1 && i < 100)
    if (i === 100) throw Error('trouvé aucun nombre premier avec ' + nb)
    return sol
  }

  function sort (n1, n2, n3) {
    let temp
    const ret = [n1, n2, n3]
    if (ret[0] > ret[1]) {
      temp = ret[0]
      ret[0] = ret[1]
      ret[1] = temp
    }
    if (ret[1] > ret[2]) {
      temp = ret[1]
      ret[1] = ret[2]
      ret[2] = temp
    }
    if (ret[0] > ret[1]) {
      temp = ret[0]
      ret[0] = ret[1]
      ret[1] = temp
    }
    return ret
  }

  function premsavec2 (nb1, nb2) {
    let i = 0 // contre les boucles infinies
    let sol, tab
    do {
      sol = j3pGetRandomInt(2, 30)
      tab = sort(sol, nb1, nb2)
      i++
    } while (i < 100 && (j3pPGCD(nb1, sol, { negativesAllowed: true, valueIfZero: 1 }) !== 1 || j3pPGCD(nb2, sol, { negativesAllowed: true, valueIfZero: 1 }) !== 1 || tab[2] === tab[1] + tab[0]))
    if (i === 100) return console.error(Error('Pas trouvé de nombre premier avec ' + nb1 + ' et ' + nb2))
    return sol
  }

  function makeDef (a, b, c) {
    const uu = addDefaultTable(a, b, c)
    const ttab = uu[0][0].parentNode.parentNode
    ttab.style.backgroundColor = '#fff'
    ttab.style.marginBottom = '0'
    ttab.style.borderCollapse = 'collapse'
    for (let i = 0; i < b; i++) {
      for (let j = 0; j < c; j++) {
        if ((i !== 0) && (i !== 3)) {
          uu[i][j].style.padding = '2px'
          uu[i][j].style.border = '1px solid #000'
        } else {
          uu[i][j].style.padding = 0
        }
        uu[i][j].style.textAlign = 'center'
        uu[i][j].style.verticalAlign = 'middle'
        uu[i][j].style.whiteSpace = 'nowrap'
        uu[i][j].ligne = i
        uu[i][j].colonne = j
        uu[i][j].style.position = ''
        if (j > 0 && (i !== 0) && (i !== 3)) uu[i][j].innerHTML = '&nbsp;...&nbsp;'
      }
    }
    return uu
  }

  function tabloise (ob) {
    if (typeof ob !== 'object' || !ob.den) return console.error(Error('argument invalide'), ob)
    return [0, ob.num, ob.den]
  }

  function egalite (n1, n2) {
    return (Math.abs((n1[1] / n1[2]) - (n2[1] / n2[2])) < 1e-12)
  }

  function plusgrand (n1, n2) {
    return ((n1[1] / n1[2]) > (n2[1] / n2[2]))
  }

  function trans (kl) {
    if (Array.isArray(kl)) return kl
    kl = String(kl).replace(',', '.')
    if (kl.indexOf('frac') !== -1) {
      kl = j3pExtraireNumDen(kl)
    } else if (kl.indexOf('.') !== -1) {
      kl = [0, Math.round(parseFloat(kl * 1000000)), 1000000]
    } else {
      kl = [0, parseFloat(kl), 1]
    }
    return kl
  }

  function calcul (decim, buf1, buf2, op) {
    function getNumber (nb) {
      if (typeof nb === 'string') return parseFloat(nb.replace(',', '.'))
      if (typeof nb === 'number') return nb
    }

    if (!buf1 || (!buf2 && op !== 'i')) return '?'
    const buf11 = getNumber(buf1[1])
    const buf12 = getNumber(buf1[2])
    let buf21, buf22
    if (buf2) {
      buf21 = getNumber(buf2[1])
      buf22 = getNumber(buf2[2])
    }
    let val1, val2
    switch (op) {
      case '+':
        val1 = buf11 * buf22 + buf12 * buf21
        val2 = buf12 * buf22
        break
      case '-':
        val1 = -buf11 * buf22 + buf12 * buf21
        val2 = buf12 * buf22
        break
      case '×':
        val1 = buf11 * buf21
        val2 = buf12 * buf22
        break
      case '÷':
        val1 = buf12 * buf21
        val2 = buf11 * buf22
        break
      case 'i':
        if (decim) {
          val1 = j3pArrondi(buf11 / buf12, 5)
          val2 = 1
        } else {
          val1 = buf11
          val2 = buf12
        }
        break
    }

    if (isNaN(val1) || isNaN(val2) || !val2) return '?'
    let val, pg

    if (val2 !== 1) {
      pg = j3pPGCD(Math.abs(val1), Math.abs(val2, { negativesAllowed: true, valueIfZero: 1 }))
      val1 = Math.round(val1 / pg)
      val2 = Math.round(val2 / pg)
    }

    if (val2 !== 1) {
      val = '\\frac{' + val1 + '}{' + val2 + '}'
    } else {
      val = String(val1)
    }
    return val.replace('.', ',')
  }

  function valeurDe (cell) {
    const pla = (cell.ligne - 1) + 2 * (cell.colonne - 1)
    return trans(stor.repEl[pla])
  }

  function getNewIdHere () {
    stor.nb++
    return 'IdI' + stor.nb
  }

  function creeBoutonCorr () {
    stor.blokRaiz = true
    stor.correcBout = j3pAjouteBouton(stor.lesdiv.blabliblo, voirCorrection, { className: 'MepBoutons', value: 'Voir une correction' })
    stor.isCorrVisible = false
    stor.listeCorr = []
    stor.isPaused = true
    let casecel, i

    for (i = 0; i < stor.listeErr.length; i++) {
      switch (stor.listeErr[i][1]) {
        case 'one': {
          casecel = (stor.nbRepAtt === 4) ? stor.tabProp[2][2] : stor.tabProp[2][3]
          const nunu = j3pAddElt(casecel, 'div')
          j3pAffiche(nunu, null, '$' + calcul(stor.decim, stor.listeErr[i][0], '1', 'i') + '$')
          nunu.style.color = me.styles.toutpetit.correction.color

          stor.listeCorr.push(nunu)
          stor.listeCache.push(stor.inputrep1.texta)
          if (!ds.Conclusion) {
            j3pAffiche(stor.lesdiv.concl1Co, null, '$' + calcul(stor.decim, stor.listeErr[i][0], '1', 'i') + '$')
            stor.lesdiv.concl1Co.style.color = me.styles.toutpetit.correction.color
            stor.listeCorr.push(stor.lesdiv.concl1Co)
            stor.listeCache.push(stor.lesdiv.concl1)
          }
        }
          break
        case 'two': {
          casecel = (stor.nbRepAtt === 4) ? stor.tabProp[1][3] : stor.tabProp[1][4]
          const nunu = j3pAddElt(casecel, 'div')
          j3pAffiche(nunu, null, '$' + calcul(stor.decim, stor.listeErr[i][0], '1', 'i') + '$')
          nunu.style.color = me.styles.toutpetit.correction.color

          stor.listeCorr.push(nunu)
          stor.listeCache.push(stor.inputrep2.texta)
          if (!ds.Conclusion) {
            j3pAffiche(stor.lesdiv.concl2Co, null, '$' + calcul(stor.decim, stor.listeErr[i][0], '1', 'i') + '$')
            stor.lesdiv.concl2Co.style.color = me.styles.toutpetit.correction.color
            stor.listeCorr.push(stor.lesdiv.concl2Co)
            stor.listeCache.push(stor.lesdiv.concl2)
          }
        }
          break
      }
    }

    /*
    let resultat
    for (i = 0; i < stor.resultats.length; i++) {
      resultat = stor.resultats[i]
      switch (resultat.methode) {
        case 'ope1':
          stor.listeCache.push(resultat.svg)
          stor.listeCache.push(resultat.amoov)
          break
        case 'ope2':
          stor.listeCache.push('mySVG' + resultat.cellule)
          stor.listeCache.push('svg' + resultat.cellule)
          stor.listeCache.push('ope2' + resultat.cellule.colonne)
          break
        case 'ope2moins':
          stor.listeCache.push('mySVG' + resultat.cellule)
          stor.listeCache.push('svg' + resultat.cellule)
          stor.listeCache.push('ope2moins' + resultat.cellule.colonne)
          break
        case 'ope3':
          stor.listeCache.push('mySVG' + resultat.cellule)
          stor.listeCache.push('svg' + resultat.cellule)
          stor.listeCache.push('ope3' + resultat.cellule)
          stor.listeCache.push(resultat.caseu)
          stor.listeCache.push(resultat.caseu2)
          break
        case 'ope4':
          stor.listeCache.push('ope4')
          stor.listeCache.push('mySVGope4')
          stor.listeCache.push('ope42')
          break
        case 'ope5':
          stor.listeCache.push('ope5' + resultat.cellule)
          break
      }
    }
    */
    // cache faux
    for (i = 0; i < stor.listeCache.length; i++) {
      stor.listeCache[i].style.display = 'none'
    }

    stor.resultatsCorr = []
    // creation des methodes co
    let val1, val2
    const mv = stor.methodeVisee
    switch (mv) {
      case 'op1col': {
        if (stor.aideco[0]) {
          stor.resultatsCorr.push({
            methode: 'ope1',
            cellule: stor.tabProp[2][2],
            val1: stor.repElco[0],
            val2: stor.repElco[2],
            origine: stor.tabProp[2][1]
          })
          stor.resultatsCorr.push({
            methode: 'ope1',
            cellule: stor.tabProp[1][3],
            val1: stor.repElco[1],
            val2: stor.repElco[5],
            origine: stor.tabProp[1][1]
          })
        } else if (stor.aideco[1]) {
          stor.resultatsCorr.push({
            methode: 'ope1',
            cellule: stor.tabProp[2][2],
            val1: stor.repElco[0],
            val2: stor.repElco[2],
            origine: stor.tabProp[2][1]
          })
          stor.resultatsCorr.push({
            methode: 'ope1',
            cellule: stor.tabProp[1][3],
            val1: stor.repElco[3],
            val2: stor.repElco[5],
            origine: stor.tabProp[1][2]
          })
        } else {
          stor.resultatsCorr.push({
            methode: 'ope1',
            cellule: stor.tabProp[1][3],
            val1: stor.repElco[1],
            val2: stor.repElco[5],
            origine: stor.tabProp[1][1]
          })
          stor.resultatsCorr.push({
            methode: 'ope1',
            cellule: stor.tabProp[2][2],
            val1: stor.repElco[4],
            val2: stor.repElco[2],
            origine: stor.tabProp[2][2]
          })
        }
        break
      }
      case 'aj2col':
      case 'sub2col': {
        if (stor.aideco) {
          stor.resultatsCorr.push({
            methode: 'ope2',
            cellule: stor.tabProp[1][3],
            cell1: stor.tabProp[2][1],
            cell2: stor.tabProp[2][2],
            cell3: stor.tabProp[1][2],
            cell4: stor.tabProp[1][2],
            type: '+'
          })
          stor.resultatsCorr.push({
            methode: 'ope2',
            cellule: stor.tabProp[2][4],
            cell1: stor.tabProp[1][1],
            cell2: stor.tabProp[1][2],
            cell3: stor.tabProp[2][2],
            cell4: stor.tabProp[2][2],
            type: '-'
          })
        } else {
          stor.resultatsCorr.push({
            methode: 'ope2',
            cellule: stor.tabProp[1][3],
            cell1: stor.tabProp[2][1],
            cell2: stor.tabProp[2][2],
            cell3: stor.tabProp[1][2],
            cell4: stor.tabProp[1][2],
            type: '-'
          })
          stor.resultatsCorr.push({
            methode: 'ope2',
            cellule: stor.tabProp[2][4],
            cell1: stor.tabProp[1][1],
            cell2: stor.tabProp[1][2],
            cell3: stor.tabProp[2][2],
            cell4: stor.tabProp[2][2],
            type: '+'
          })
        }
        break
      }
      case 'passageUnite': {
        val1 = trans(stor.repElco[0])

        val2 = trans(stor.repElco[1])

        stor.repElco[6] = calcul(stor.decim, val2, val1, '÷')
        stor.repElco[7] = calcul(stor.decim, val2, val1, '÷')

        if (stor.aideco) {
          stor.resultatsCorr.push({
            methode: 'ope3',
            cellule: stor.tabProp[2][2],
            caseu: false,
            valflech: stor.repElco[2],
            valu: calcul(stor.decim, val1, val2, '÷')
          })
          stor.resultatsCorr.push({
            methode: 'ope1',
            cellule: stor.tabProp[1][3],
            val1: 1,
            val2: stor.repElco[4],
            origine: 'caseu2'
          })
        } else {
          stor.resultatsCorr.push({
            methode: 'ope3',
            cellule: stor.tabProp[1][3],
            caseu: true,
            valflech: stor.repElco[5],
            valu: calcul(stor.decim, val2, val1, '÷')
          })
          stor.resultatsCorr.push({
            methode: 'ope1',
            cellule: stor.tabProp[2][2],
            val1: 1,
            val2: stor.repElco[3],
            origine: 'caseu2'
          })
        }

        break
      }
      case 'coef': {
        val1 = trans(stor.repElco[0])
        val2 = trans(stor.repElco[1])
        const coefi = calcul(stor.decim, val1, val2, '÷')

        stor.resultatsCorr.push({ methode: 'ope4', coef: coefi })
        break
      }
      case 'prodCroix': {
        stor.resultatsCorr.push({
          methode: 'ope5',
          cellule: stor.tabProp[2][2],
          cel1: stor.tabProp[1][2],
          cel2: stor.tabProp[2][1],
          cel3: stor.tabProp[1][1]
        })
        stor.resultatsCorr.push({
          methode: 'ope5',
          cellule: stor.tabProp[1][3],
          cel1: stor.tabProp[2][3],
          cel2: stor.tabProp[1][1],
          cel3: stor.tabProp[2][1]
        })
        break
      }
    }

    adapteCorr()

    // cache listeCorr
    for (let i = 0; i < stor.listeCorr.length; i++) {
      stor.listeCorr[i].style.display = 'none'
    }

    // affiche fo
    for (let i = 0; i < stor.listeCache.length; i++) {
      stor.listeCache[i].style.display = ''
    }
    for (let i = 0; i < stor.listeDoudou.length; i++) {
      stor.listeDoudou[i].doudou.style.paddingLeft = stor.listeDoudou[i].nb
    }
  }

  function adapteCorr () {
    let yacoef = false
    let yapdt = false
    let num1
    let num2
    let flechdeb
    let posCellule
    let pos2
    let pos3
    let pos4
    let pos5
    let val1
    let val2
    let val
    let force
    let wwi
    let lop
    let si2
    let si
    let basex
    let mid
    let doudou
    let doudou2
    stor.divCo = []
    stor.svgCo = []
    stor.doudouCo = []

    for (let i = 0; i < stor.resultatsCorr.length; i++) {
      switch (stor.resultatsCorr[i].methode) {
        case 'ope1': {
          // prend pos deb et fin
          if (stor.resultatsCorr[i].cellule.ligne === 2) {
            doudou = stor.lesdiv.vide3
            doudou2 = stor.lesdiv.vide4
          } else {
            doudou = stor.lesdiv.vide2
            doudou2 = stor.lesdiv.vide1
          }
          stor.divCo[i] = j3pAddElt(doudou2, 'div', {
            style: me.styles.toutpetit.correction
          })
          val1 = stor.resultatsCorr[i].val1
          force = (val1 === 1)
          val2 = stor.resultatsCorr[i].val2
          if (!Array.isArray(val1)) val1 = trans(val1)
          if (!Array.isArray(val2)) val2 = trans(val2)

          if ((plusgrand(val1, val2) && (!force))) {
            lop = '÷'
            val = calcul(stor.decim, val2, val1, '÷')
          } else {
            lop = '×'
            val = calcul(stor.decim, val1, val2, '÷')
          }
          val = trans(val)
          val = calcul(stor.decim, val, 1, 'i')

          j3pAffiche(stor.divCo[i], null, '$' + lop + ' ' + val + '$', { styletext: { color: me.styles.toutpetit.correction.color } })

          let lorigine = stor.resultatsCorr[i].origine
          if (lorigine === 'caseu2') lorigine = stor.laCseU2
          posCellule = stor.resultatsCorr[i].cellule.getBoundingClientRect()
          pos2 = lorigine.getBoundingClientRect()
          pos3 = stor.lesdiv.tableau.getBoundingClientRect()
          let momod = 0
          if (posCellule.left > pos2.left) {
            wwi = posCellule.left - pos2.left + posCellule.width - 10
            flechdeb = wwi
            si = -1
          } else {
            wwi = pos2.left - posCellule.left + pos2.width - 10
            momod = 10
            flechdeb = 0
            si = 1
          }
          // change svg + blok case et liste
          doudou2.style.paddingLeft = (Math.min(posCellule.left, pos2.left) - pos3.left + momod) + 'px'
          if (stor.resultatsCorr[i].cellule.ligne === 2) {
            num2 = 20
            num1 = 0
            si2 = 1
          } else {
            num2 = 0
            num1 = 20
            si2 = -1
          }

          stor.svgCo[i] = j3pAddElt(doudou, 'div', {
            style: me.styles.toutpetit.correction
          })
          doudou.style.paddingLeft = (Math.min(posCellule.left, pos2.left) - pos3.left) + 'px'
          stor.svgCo[i].style.width = wwi + 'px'
          stor.svgCo[i].style.height = '25px'
          stor.svgCo[i].wwi = wwi

          const monSVG = document.createElementNS('http://www.w3.org/2000/svg', 'svg')
          monSVG.style.width = wwi + 'px'
          monSVG.style.height = '25px'
          monSVG.style.left = '0px'
          monSVG.style.top = '0px'

          stor.svgCo[i].appendChild(monSVG)
          // change rect et lign
          monSVG.innerHTML += '<path d="M0 ' + num1 + ' Q ' + (wwi / 2) + ' ' + num2 + ', ' + wwi + ' ' + num1 + '  M ' + flechdeb + ' ' + num1 + ' L ' + (flechdeb + si * wwi * 10 / 55) + ' ' + (num1 + si2 * 15) + '  M ' + flechdeb + ' ' + num1 + ' L ' + (flechdeb + si * wwi * 14 / 55) + ' ' + (num1 + si2) + '" stroke-width="3" stroke="' + me.styles.toutpetit.correction.color + '" fill="transparent" />'

          stor.listeCorr.push(stor.svgCo[i])
          stor.listeCorr.push(stor.divCo[i])
          stor.doudouCo.push({ doudou, nb: (Math.min(posCellule.left, pos2.left) - pos3.left + momod) + 'px' })
          stor.doudouCo.push({ doudou: doudou2, nb: (Math.min(posCellule.left, pos2.left) - pos3.left + momod) + 'px' })
        }
          break

        case 'ope2':
          {
          // prend pos deb et fin et flech
            posCellule = stor.resultatsCorr[i].cellule.getBoundingClientRect()
            pos2 = stor.resultatsCorr[i].cell1.getBoundingClientRect()
            pos3 = stor.lesdiv.tableau.getBoundingClientRect()
            pos4 = stor.resultatsCorr[i].cell2.getBoundingClientRect()
            pos5 = stor.resultatsCorr[i].cell3.getBoundingClientRect()
            basex = Math.min(posCellule.left, pos2.left, pos5.left)
            wwi = (Math.max(posCellule.left + posCellule.width, pos2.left + pos2.width, pos5.left + pos5.width) - basex)
            if (stor.resultatsCorr[i].cellule.ligne === 1) {
              doudou = stor.lesdiv.vide3
              doudou2 = stor.lesdiv.vide4
              num2 = 20
              num1 = 2
              si2 = 1
            } else {
              doudou = stor.lesdiv.vide2
              doudou2 = stor.lesdiv.vide1
              num2 = 2
              num1 = 20
              si2 = -1
            }
            if (pos2.left < pos5.left) { mid = Math.abs((pos5.left + pos5.width - pos2.left) / 2) } else { mid = Math.abs(pos2.left + pos2.width - pos5.left) / 2 }

            stor.svgCo[i] = j3pAddElt(doudou, 'div', {
              style: me.styles.toutpetit.correction
            })
            doudou.style.paddingLeft = (Math.min(posCellule.left, pos2.left) - pos3.left) + 'px'
            stor.svgCo[i].style.width = wwi + 'px'
            stor.svgCo[i].style.height = '25px'
            stor.svgCo[i].wwi = wwi
            stor.svgCo[i].background = '#ff0000'

            const monSVG = document.createElementNS('http://www.w3.org/2000/svg', 'svg')
            monSVG.style.width = wwi + 'px'
            monSVG.style.height = '25px'
            monSVG.style.left = '0px'
            monSVG.style.top = '0px'

            stor.svgCo[i].appendChild(monSVG)
            monSVG.innerHTML = '<path d="M' + (pos2.left - basex + pos2.width / 2) + ' ' + num1 + ' L ' + mid + ' ' + num2 + 'L ' + (pos5.left - basex + pos5.width / 2) + ' ' + num1 + ' M  ' + mid + ' ' + num2 + ' L ' + (posCellule.left + posCellule.width / 2 - basex) + ' 10 L ' + (posCellule.left + posCellule.width / 2 - basex) + ' ' + num1 + ' L ' + (posCellule.left + posCellule.width / 2 - basex + wwi / 10) + ' ' + (num1 + si2 * 7) + '  M ' + (posCellule.left + posCellule.width / 2 - basex) + ' ' + num1 + ' L ' + (posCellule.left + posCellule.width / 2 - basex - wwi / 10) + ' ' + (num1 + si2 * 7) + '" stroke-width="3" stroke="' + me.styles.toutpetit.correction.color + '"                  fill="transparent" />'

            stor.divCo[i] = j3pAddElt(doudou2, 'div', {
              style: me.styles.toutpetit.correction
            })
            j3pAffiche(stor.divCo[i], null, stor.resultatsCorr[i].type)
            doudou2.style.paddingLeft = (Math.min(posCellule.left, pos2.left) - pos3.left) + 'px'
            stor.listeCorr.push(stor.svgCo[i])
            stor.listeCorr.push(stor.divCo[i])
            stor.doudouCo.push({ doudou, nb: (basex - pos3.left) + 'px' })
            stor.doudouCo.push({ doudou: doudou2, nb: (Math.min(pos5.left, pos2.left) - pos3.left + mid) + 'px' })
          }
          break

        case 'ope3': {
          const elt1 = j3pAddElt(stor.tabProp[1][0].parentNode, 'td')
          elt1.ligne = 1
          elt1.colonne = 4
          stor.tabProp[1].push(elt1)

          const elt2 = j3pAddElt(stor.tabProp[2][0].parentNode, 'td')
          elt2.ligne = 2
          elt2.colonne = 4
          stor.tabProp[2].push(elt2)

          stor.ajcolonne++
          stor.tabProp[0][0].setAttribute('colspan', stor.tabProp[2].length)
          stor.tabProp[3][0].setAttribute('colspan', stor.tabProp[2].length)

          elt1.style.border = '1px solid black'
          elt2.style.border = '1px solid black'
          elt1.style.padding = 0
          elt2.style.padding = 0
          elt1.style.textAlign = 'center'
          elt2.style.textAlign = 'center'
          elt1.style.width = '40px'

          stor.listeCorr.push(elt1)
          stor.listeCorr.push(elt2)

          let caseu
          let caseu2
          let doudou
          let doudou2
          if (stor.resultatsCorr[i].caseu) {
            caseu = elt2
            caseu2 = elt1
            stor.laCseU2 = elt1
          } else {
            stor.laCseU2 = elt2
            caseu = elt1
            caseu2 = elt2
          }
          j3pAffiche(caseu, null, '  $1$  ')
          j3pAffiche(caseu2, null, '$' + stor.resultatsCorr[i].valu + '$')

          posCellule = stor.resultatsCorr[i].cellule.getBoundingClientRect()
          pos2 = caseu.getBoundingClientRect()
          pos3 = stor.lesdiv.tableau.getBoundingClientRect()
          pos4 = caseu2.getBoundingClientRect()
          // change svg
          wwi = (pos2.left + pos2.width - posCellule.left - 10)
          if (stor.resultatsCorr[i].cellule.ligne === 2) {
            doudou = stor.lesdiv.vide3
            doudou2 = stor.lesdiv.vide4
            num2 = 20
            num1 = 0
            si2 = 1
          } else {
            doudou = stor.lesdiv.vide2
            doudou2 = stor.lesdiv.vide1
            num2 = 0
            num1 = 20
            si2 = -1
          }
          if (posCellule.left < pos2.left) {
            flechdeb = 0
            si = 1
          } else {
            flechdeb = wwi
            si = -1
          }
          const sss = j3pAddElt(doudou, 'div')
          doudou.style.paddingLeft = (posCellule.left - pos3.left + 5) + 'px'

          sss.style.width = wwi + 'px'
          sss.style.height = '25px'
          sss.wwi = wwi

          const monSVG = document.createElementNS('http://www.w3.org/2000/svg', 'svg')
          monSVG.style.width = wwi + 'px'
          monSVG.style.height = '25px'
          monSVG.style.left = '0px'
          monSVG.style.top = '0px'

          sss.appendChild(monSVG)
          monSVG.innerHTML = '<path d="M0 ' + num1 + ' Q ' + (wwi / 2) + ' ' + num2 + ', ' + wwi + ' ' + num1 + '  M ' + flechdeb + ' ' + num1 + ' L ' + (flechdeb + si * wwi * 10 / 55) + ' ' + (num1 + si2 * 15) + '  M ' + flechdeb + ' ' + num1 + ' L ' + (flechdeb + si * wwi * 14 / 55) + ' ' + (num1 + si2) + '" stroke-width="3" stroke="' + me.styles.toutpetit.correction.color + '" fill="transparent" />'

          stor.listeCorr.push(sss)

          const yyy = j3pAddElt(doudou2, 'div', {
            style: me.styles.toutpetit.correction
          })
          doudou2.style.paddingLeft = (posCellule.left - pos3.left + 5) + 'px'

          j3pAffiche(yyy, null, '$ \\times ' + stor.resultatsCorr[i].valflech + '$')
          stor.listeCorr.push(yyy)

          stor.doudouCo.push({ doudou, nb: (posCellule.left - pos3.left + 5) + 'px' })
          stor.doudouCo.push({ doudou: doudou2, nb: (posCellule.left - pos3.left + 5) + 'px' })
        }
          break

        case 'ope4':
          if (!yacoef) {
            yacoef = true
            const co2 = j3pAddElt(stor.lesdiv.ar0, 'div')
            co2.style.width = '150px'
            co2.style.height = stor.lesdiv.ar0.offsetHeight + 'px'
            co2.style.backgroundImage = 'url("' + j3pBaseUrl + 'sections/college/sixieme/Proportionnalite/coefCO.png")'
            co2.style.backgroundRepeat = 'no-repeat'
            co2.style.backgroundSize = '100% 100%'

            const co1 = j3pAddElt(co2, 'div')
            co1.style.position = 'relative'
            co1.style.left = '38px'
            co1.style.top = (stor.lesdiv.ar0.offsetHeight / 2 - 18) + 'px'
            co1.style.color = me.styles.petit.correction.color
            j3pAffiche(co1, null, '$\\times ' + stor.resultatsCorr[i].coef + '$')
            stor.listeCorr.push(co2)
          }
          break
        case 'ope5': {
          if (!yapdt) { doudou = stor.lesdiv.ar1; yapdt = true } else { doudou = stor.lesdiv.ar2 }
          doudou.style.visibility = 'visible'
          const buf1 = valeurDe(stor.resultatsCorr[i].cel1)
          const buf2 = valeurDe(stor.resultatsCorr[i].cel2)
          const buf3 = valeurDe(stor.resultatsCorr[i].cel3)
          const doudou3 = j3pAddElt(doudou, 'div', {
            style: me.styles.toutpetit.correction
          })
          j3pAffiche(doudou3, null, '$ \\frac{' + calcul(stor.decim, buf1, null, 'i') + ' \\times ' + calcul(stor.decim, buf2, null, 'i') + ' }{ ' + calcul(stor.decim, buf3, null, 'i') + '} $')
          doudou3.style.background = '#aaaaff'
          stor.listeCorr.push(doudou3)
          /// ///
          posCellule = stor.resultatsCorr[i].cel1.getBoundingClientRect()
          pos2 = stor.resultatsCorr[i].cel2.getBoundingClientRect()
          pos3 = stor.resultatsCorr[i].cel3.getBoundingClientRect()
          pos4 = stor.resultatsCorr[i].cellule.getBoundingClientRect()
          pos5 = me.zonesElts.MG.getBoundingClientRect()

          let leight, y1, y2, y3, y4
          doudou2 = j3pAddElt(stor.lesdiv.tableau, 'div', {
            style: me.styles.toutpetit.enonce
          })
          doudou2.style.position = 'absolute'
          doudou2.style.pointerEvents = 'none'
          doudou2.style.left = (Math.min(posCellule.left, pos2.left, pos3.left, pos4.left) - pos5.left) + 'px'
          doudou2.style.top = (Math.min(posCellule.top, pos2.top, pos3.top, pos4.top) - pos5.top) + 'px'

          if (posCellule.top === pos2.top) {
            if (pos2.top === pos3.top) {
              if (pos3.top === pos4.top) {
                doudou2.style.height = posCellule.height + 'px'
                leight = posCellule.height
              } else {
                doudou2.style.height = (posCellule.height + pos4.height) + 'px'
                leight = posCellule.height + pos4.height
              }
            } else {
              doudou2.style.height = (posCellule.height + pos3.height) + 'px'
              leight = posCellule.height + pos3.height
            }
          } else {
            doudou2.style.height = (posCellule.height + pos2.height) + 'px'
            leight = posCellule.height + pos2.height
          }
          wwi = posCellule.left + posCellule.width - Math.min(posCellule.left, pos2.left, pos3.left, pos4.left)
          if (pos2.left > posCellule.left) {
            wwi = pos2.left + pos2.width - Math.min(posCellule.left, pos2.left, pos3.left, pos4.left)
          }
          if (pos3.left > pos2.left) {
            wwi = pos3.left + pos3.width - Math.min(posCellule.left, pos2.left, pos3.left, pos4.left)
          }
          if (pos4.left > pos3.left) {
            wwi = pos4.left + pos4.width - Math.min(posCellule.left, pos2.left, pos3.left, pos4.left)
          }
          if (stor.resultatsCorr[i].cel1.ligne === 1) { y1 = 5 } else { y1 = leight - 5 }
          if (stor.resultatsCorr[i].cel2.ligne === 1) { y2 = 5 } else { y2 = leight - 5 }
          if (stor.resultatsCorr[i].cel3.ligne === 1) { y3 = 5 } else { y3 = leight - 5 }
          if (stor.resultatsCorr[i].cellule.ligne === 1) { y4 = 5 } else { y4 = leight - 5 }
          const x1 = (posCellule.left + posCellule.width / 2) - Math.min(posCellule.left, pos2.left, pos3.left, pos4.left)
          const x2 = (pos2.left + pos2.width / 2) - Math.min(posCellule.left, pos2.left, pos3.left, pos4.left)
          const x3 = (pos3.left + pos3.width / 2) - Math.min(posCellule.left, pos2.left, pos3.left, pos4.left)
          let x4
          if (pos4.left > pos3.left) {
            x4 = (pos4.left + 5) - Math.min(posCellule.left, pos2.left, pos3.left, pos4.left)
          } else if (pos4.left < pos3.left) {
            x4 = (pos4.left + pos4.width - 5) - Math.min(posCellule.left, pos2.left, pos3.left, pos4.left)
          } else {
            x4 = x3
          }
          doudou2.style.width = wwi + 'px'
          doudou2.innerHTML = '<svg viewBox="0 0 ' + wwi + ' ' + leight + '" ><path d="M ' + x1 + ' ' + y1 + ' L ' + x2 + ' ' + y2 + ' L ' + x3 + ' ' + y3 + '  L ' + x4 + ' ' + y4 + '" stroke-width="3" stroke="' + me.styles.toutpetit.correction.color + '" fill="transparent"  /></svg>'
          /// ///

          doudou3.aff = doudou2
          doudou3.addEventListener('mouseover', croixMontreCo, false)
          doudou3.addEventListener('mouseout', croixCacheCo, false)
          doudou2.style.display = 'none'
        }
          break
      }
    }
  }

  function voirCorrection () {
    let bufco, buffo
    if (stor.isCorrVisible) {
      stor.isCorrVisible = false
      bufco = 'none'
      buffo = ''
      for (let i = 0; i < stor.listeDoudou.length; i++) {
        stor.listeDoudou[i].doudou.style.paddingLeft = stor.listeDoudou[i].nb
      }
      if (stor.lesdiv.blabliblo) stor.correcBout.value = 'Voir une correction'
    } else {
      bufco = ''
      buffo = 'none'
      stor.isCorrVisible = true
      for (let i = 0; i < stor.doudouCo.length; i++) {
        stor.doudouCo[i].doudou.style.paddingLeft = stor.doudouCo[i].nb
      }
      if (stor.lesdiv.blabliblo) stor.correcBout.value = 'Voir mes réponses'
    }
    // cache listeCorr
    for (let i = 0; i < stor.listeCorr.length; i++) {
      stor.listeCorr[i].style.display = bufco
    }
    // affiche fo
    for (let i = 0; i < stor.listeCache.length; i++) {
      stor.listeCache[i].style.display = buffo
    }
  }

  function croixMontreCo () {
    if (!stor.isCorrVisible) return
    this.aff.style.display = ''
  }

  function croixCacheCo () {
    this.aff.style.display = 'none'
  }

  function adapteLarg () {
    // @todo déclarer ces variables quand elles sont affectées
    let bub, resultat, buf1, buf2, buf3, jim, pla, listop, val, op, laval
    let posCellule, pos2, pos3, pos4, pos5, basex, mmmax, wwi, num1, num2, si2
    let flechdeb, si, mid, leight
    let y1, y2, y3, y4, x1, x2, x3, x4
    stor.listeCache = []
    stor.listeDoudou = []

    if (this !== undefined) {
      for (let i = 0; i < stor.resultats.length; i++) {
        if (stor.resultats[i].numres === this.numres) stor.resultats[i].color = 'black'
      }
    }

    if (ds.Completer === 'Raisonnement' && !stor.blokRaiz) {
      let dedep1 = false
      let dedep2 = false
      for (let ju = 0; ju < 2; ju++) {
        for (let i = 0; i < stor.resultats.length; i++) {
          resultat = stor.resultats[i]
          resultat.cellule.innerHTML = ''
          let dou
          if (resultat.cellule.ligne === 1) {
            stor.inputrep2 = {}
            stor.inputrep2.texta = j3pAddElt(resultat.cellule, 'div')
            dou = stor.inputrep2.texta
          } else {
            stor.inputrep1 = {}
            stor.inputrep1.texta = j3pAddElt(resultat.cellule, 'div')
            dou = stor.inputrep1.texta
          }
          switch (resultat.methode) {
            case 'ope1':
              // cellule:this.casedep,methode:'ope1',op:"ope1"+this.exclu,valeur:this.casedep+this.exclu,col1:this.id1,col12:this.id2})
              buf1 = trans(resultat.op.reponse().replace(/ /g, ''))

              if (resultat.cellule.ligne === resultat.col1.ligne) {
                jim = resultat.col1
              } else {
                jim = resultat.col12
              }
              pla = (jim.ligne - 1) + 2 * (jim.colonne - 1)
              if ((stor.placeNb[pla] !== 'rep1') && (stor.placeNb[pla] !== 'rep2')) {
                buf2 = trans(stor.repEl[pla])
              } else {
                dedep1 = (dedep1 || stor.placeNb[pla] === 'rep1')
                dedep2 = (dedep2 || stor.placeNb[pla] === 'rep2')
                if (dedep1 && dedep2) {
                  buf2 = ['?', '?']
                } else { buf2 = stor.repEl[pla] }
              }
              listop = resultat.liste
              val = trans(calcul(stor.decim, buf1, buf2, listop.reponse))
              val = calcul(stor.decim, val, 1, 'i')

              j3pAffiche(dou, null, '$' + val + '$')
              resultat.valeur = val
              jim = resultat.cellule
              pla = (jim.ligne - 1) + 2 * (jim.colonne - 1)
              stor.repEl[pla] = val
              break

            case 'ope2':
              // cellule:this.casedep,methode:'ope2',op:"ope2"+this.exclu,valeur:this.casedep+this.exclu,col1:this.id1,col12:this.id2,col2:this.mem,col22:this.mem2})
              if (resultat.cellule.ligne === resultat.col1.ligne) {
                jim = resultat.col1
              } else {
                jim = resultat.col12
              }
              pla = (jim.ligne - 1) + 2 * (jim.colonne - 1)
              if ((stor.placeNb[pla] !== 'rep1') && (stor.placeNb[pla] !== 'rep2')) {
                buf2 = trans(stor.repEl[pla])
              } else {
                dedep1 = (dedep1 || stor.placeNb[pla] === 'rep1')
                dedep2 = (dedep2 || stor.placeNb[pla] === 'rep2')
                if (dedep1 && dedep2) {
                  buf2 = ['?', '?']
                } else { buf2 = stor.repEl[pla] }
              }

              if (resultat.cellule.ligne === resultat.col2.ligne) {
                jim = resultat.col2
              } else {
                jim = resultat.col22
              }
              pla = (jim.ligne - 1) + 2 * (jim.colonne - 1)
              if ((stor.placeNb[pla] !== 'rep1') && (stor.placeNb[pla] !== 'rep2')) {
                buf1 = trans(stor.repEl[pla])
              } else {
                dedep1 = (dedep1 || stor.placeNb[pla] === 'rep1')
                dedep2 = (dedep2 || stor.placeNb[pla] === 'rep2')
                if (dedep1 && dedep2) {
                  buf1 = ['?', '?']
                } else { buf1 = stor.repEl[pla] }
              }
              val = trans(calcul(stor.decim, buf2, buf1, '+'))
              val = calcul(stor.decim, val, 1, 'i')
              j3pAffiche(dou, null, '$' + val + '$')
              resultat.valeur = val
              jim = resultat.cellule
              pla = (jim.ligne - 1) + 2 * (jim.colonne - 1)
              stor.repEl[pla] = val
              break

            case 'ope2moins':
              // cellule:this.casedep,methode:'ope2',op:"ope2"+this.exclu,valeur:this.casedep+this.exclu,col1:this.id1,col12:this.id2,col2:this.mem,col22:this.mem2})
              if (resultat.cellule.ligne === resultat.col1.ligne) {
                jim = resultat.col1
              } else {
                jim = resultat.col12
              }
              pla = (jim.ligne - 1) + 2 * (jim.colonne - 1)
              if ((stor.placeNb[pla] !== 'rep1') && (stor.placeNb[pla] !== 'rep2')) {
                buf2 = trans(stor.repEl[pla])
              } else {
                dedep1 = (dedep1 || stor.placeNb[pla] === 'rep1')
                dedep2 = (dedep2 || stor.placeNb[pla] === 'rep2')
                if (dedep1 && dedep2) {
                  buf2 = ['?', '?']
                } else { buf2 = stor.repEl[pla] }
              }

              if (resultat.cellule.ligne === resultat.col2.ligne) {
                jim = resultat.col2
              } else {
                jim = resultat.col22
              }
              pla = (jim.ligne - 1) + 2 * (jim.colonne - 1)
              if ((stor.placeNb[pla] !== 'rep1') && (stor.placeNb[pla] !== 'rep2')) {
                buf1 = trans(stor.repEl[pla])
              } else {
                dedep1 = (dedep1 || stor.placeNb[pla] === 'rep1')
                dedep2 = (dedep2 || stor.placeNb[pla] === 'rep2')
                if (dedep1 && dedep2) {
                  buf1 = ['?', '?']
                } else { buf1 = stor.repEl[pla] }
              }
              val = trans(calcul(stor.decim, buf2, buf1, '-'))
              val = calcul(stor.decim, val, 1, 'i')
              j3pAffiche(dou, null, '$' + val + '$')
              resultat.valeur = val
              jim = resultat.cellule
              pla = (jim.ligne - 1) + 2 * (jim.colonne - 1)
              stor.repEl[pla] = val

              break
            case 'ope3': // {cellule:this.qui,methode:'ope3',op:'ope3'+this.qui+'multinputmq1',valeurunit:"op32"+this.id2+"inpumq1",valeur:"op3"+this.qui+"res",caseu:this.id,caseu2:this.id2})
              buf1 = trans(resultat.valeurunit.reponse().replace(/ /g, ''))

              buf2 = trans(resultat.multi)

              val = trans(calcul(stor.decim, buf1, buf2, '×'))
              val = calcul(stor.decim, val, 1, 'i')
              j3pAffiche(dou, null, '$' + val + '$')
              resultat.valeur = val
              jim = resultat.cellule
              pla = (jim.ligne - 1) + 2 * (jim.colonne - 1)
              stor.repEl[pla] = val

              break
            case 'ope4':
              buf1 = trans(stor.zoneCoef.reponse().replace(/ /g, ''))

              jim = resultat.cellule
              pla = (jim.ligne - 1) + 2 * (jim.colonne - 1)
              if (stor.placeNb[pla] !== 'rep1') {
                buf2 = stor.nbxg2
              } else { buf2 = stor.nbxg1 }

              buf2 = [0, buf2.num, buf2.den]
              if (resultat.cellule.ligne !== 1) {
                op = '×'
              } else { op = '÷' }

              laval = trans(calcul(stor.decim, buf1, buf2, op))
              laval = calcul(stor.decim, laval, 1, 'i')
              j3pAffiche(dou, null, '$' + laval + '$')
              resultat.valeur = laval
              jim = resultat.cellule
              pla = (jim.ligne - 1) + 2 * (jim.colonne - 1)
              stor.repEl[pla] = laval

              break
            case 'ope5': // cellule:this.casedep,methode:'ope5',cel1:d.bufpdt[0],cel2:d.bufpdt[1],cel3:d.bufpdt[2],valeur:this.casedep+'ope5inputmq1',pla:this.pla})
              buf1 = valeurDe(resultat.cel1)
              buf2 = valeurDe(resultat.cel2)
              buf3 = valeurDe(resultat.cel3)
              val = trans(calcul(stor.decim, buf1, buf2, '×'))
              val = trans(calcul(stor.decim, buf3, val, '÷'))
              val = calcul(stor.decim, val, 1, 'i')
              jim = resultat.cellule
              j3pAffiche(dou, null, '$' + val + '$')
              resultat.valeur = val

              pla = (jim.ligne - 1) + 2 * (jim.colonne - 1)
              stor.repEl[pla] = val
              break
          }
        }
      }
    }
    if (ds.Completer !== 'Raisonnement') {
      // ligne = 2
      // lacase = 0
      for (let i = 0; i < stor.placeNb.length; i++) {
        // if (ligne === 1) { ligne = 2 } else {
        //  ligne = 1
        //  lacase++
        // }
        if (stor.placeNb[i] === 'rep1') {
          if (stor.inputrep1) {
            stor.repEl[i] = stor.inputrep1.reponse().replace(/ /g, '')
            // stor.listeCache.push(stor.inputrep1.texta)
          }
        }
        if (stor.placeNb[i] === 'rep2') {
          if (stor.inputrep2) {
            stor.repEl[i] = stor.inputrep2.reponse().replace(/ /g, '')
          }
        }
      }
    }

    for (let i = 0; i < stor.resultats.length; i++) {
      resultat = stor.resultats[i]
      switch (resultat.methode) {
        case 'ope1': // prend pos deb et fin
          // {cellule:this.casedep,methode:'ope1',op:"ope1"+this.exclu,valeur:this.casedep+this.exclu,col1:this.id1})
          {
            posCellule = resultat.cellule.getBoundingClientRect()
            pos2 = resultat.col1.getBoundingClientRect()
            pos3 = stor.lesdiv.tableau.getBoundingClientRect()
            if (posCellule.left > pos2.left) { mmmax = posCellule.width } else { mmmax = pos2.width }
            let doudou
            if (resultat.cellule.ligne === 2) { doudou = stor.lesdiv.vide3 } else { doudou = stor.lesdiv.vide2 }
            resultat.amoov.style.paddingLeft = (Math.min(posCellule.left, pos2.left) - pos3.left) + 'px'

            // change svg + blok case et liste
            wwi = (Math.max(posCellule.left, pos2.left) - Math.min(posCellule.left, pos2.left) + mmmax) - 20
            if (resultat.cellule.ligne === 2) {
              num2 = 20
              num1 = 0
              si2 = 1
            } else {
              num2 = 0
              num1 = 20
              si2 = -1
            }
            if (posCellule.left < pos2.left) {
              flechdeb = 0
              si = 1
            } else {
              flechdeb = wwi
              si = -1
            }

            j3pDetruit(resultat.svg)
            resultat.svg = j3pAddElt(doudou, 'div', {
              style: me.styles.toutpetit.enonce
            })
            stor.listeCache.push(resultat.svg)
            stor.listeCache.push(resultat.cont)
            stor.listeDoudou.push({ doudou: resultat.amoov, nb: (Math.min(posCellule.left, pos2.left) - pos3.left) + 'px' })
            stor.listeDoudou.push({ doudou, nb: (Math.min(posCellule.left, pos2.left) - pos3.left) + 'px' })
            doudou.paddingLeft = (Math.min(posCellule.left, pos2.left) - pos3.left) + 'px'
            resultat.svg.style.boxShadow = 'none'
            if (resultat.color === 'black') resultat.liste.corrige(null)

            // resultat.op.style.color = resultat.color
            resultat.svg.style.width = wwi + 'px'
            resultat.svg.style.height = '25px'
            resultat.svg.wwi = wwi

            const monSVG = document.createElementNS('http://www.w3.org/2000/svg', 'svg')
            monSVG.style.width = wwi + 'px'
            monSVG.style.height = '25px'
            monSVG.style.left = '0px'
            monSVG.style.top = '0px'

            resultat.svg.appendChild(monSVG)
            // change rect et lign
            monSVG.innerHTML += '<path d="M0 ' + num1 + ' Q ' + (wwi / 2) + ' ' + num2 + ', ' + wwi + ' ' + num1 + '  M ' + flechdeb + ' ' + num1 + ' L ' + (flechdeb + si * wwi * 10 / 55) + ' ' + (num1 + si2 * 15) + '  M ' + flechdeb + ' ' + num1 + ' L ' + (flechdeb + si * wwi * 14 / 55) + ' ' + (num1 + si2) + '" stroke-width="3" stroke="#000000" fill="transparent" />'
            monSVG.innerHTML += '<line x1="' + (wwi / 4) + '" y1="5" x2="' + (3 * wwi / 4) + '" y2="25" style="stroke:rgb(255,0,0);stroke-width:4" /><line x1="' + (wwi / 4) + '" y1="25" x2="' + (3 * wwi / 4) + '" y2="5" style="stroke:rgb(255,0,0);stroke-width:4" /><rect width="' + wwi + '" height="40" style="fill:rgb(0,0,255);fill-opacity:0;stroke-width:0;stroke:rgb(0,0,0)" />'
            monSVG.childNodes[1].style.visibility = 'hidden'
            monSVG.childNodes[2].style.visibility = 'hidden'
            monSVG.childNodes[3].qui = resultat.cellule
            monSVG.childNodes[3].qui1 = monSVG.childNodes[1]
            monSVG.childNodes[3].qui2 = monSVG.childNodes[2]
            monSVG.childNodes[3].coid = resultat.coid
            monSVG.childNodes[3].svg = resultat.svg
            monSVG.childNodes[3].ope1 = resultat.op
            monSVG.childNodes[3].amoov = resultat.amoov
            monSVG.childNodes[3].liste = resultat.liste
            if (!stor.desatout) {
              monSVG.childNodes[3].style.cursor = 'pointer'
              monSVG.childNodes[3].addEventListener('mouseover', FlecheClickSupprOver, false)
              monSVG.childNodes[3].addEventListener('mouseout', FlecheClickSupprOut, false)
              monSVG.childNodes[3].addEventListener('click', foisClickSuppr, false)
            }
          }
          break

        case 'ope2':
        case 'ope2moins': {
          // prend pos deb et fin et flech(stor.desatout)
          posCellule = resultat.cellule.getBoundingClientRect()
          pos2 = resultat.col1.getBoundingClientRect()
          pos3 = stor.lesdiv.tableau.getBoundingClientRect()
          pos4 = resultat.col12.getBoundingClientRect()
          pos5 = resultat.col2.getBoundingClientRect()
          basex = Math.min(posCellule.left, pos2.left, pos5.left)
          wwi = (Math.max(posCellule.left + posCellule.width, pos2.left + pos2.width, pos5.left + pos5.width) - basex)
          if (resultat.cellule.ligne === 2) {
            num2 = 20
            num1 = 2
            si2 = 1
          } else {
            num2 = 2
            num1 = 20
            si2 = -1
          }
          if (pos2.left < pos5.left) { mid = Math.abs((pos5.left + pos5.width - pos2.left) / 2) } else { mid = Math.abs(pos2.left + pos2.width - pos5.left) / 2 }

          j3pDetruit(resultat.svg)
          resultat.svg = j3pAddElt(resultat.doudou2, 'div', {
            style: me.styles.toutpetit.enonce
          })
          resultat.doudou2.style.paddingLeft = (basex - pos3.left) + 'px'
          resultat.svg.style.width = wwi + 'px'
          resultat.svg.style.height = '25px'
          resultat.svg.wwi = wwi

          const monSVG = document.createElementNS('http://www.w3.org/2000/svg', 'svg')
          monSVG.style.width = wwi + 'px'
          monSVG.style.height = '25px'
          monSVG.style.left = '0px'
          monSVG.style.top = '0px'
          monSVG.innerHTML = '<path d="M' + (pos2.left - basex + pos2.width / 2) + ' ' + num1 + ' L ' + mid + ' ' + num2 + 'L ' + (pos5.left - basex + pos5.width / 2) + ' ' + num1 + ' M  ' + mid + ' ' + num2 + ' L ' + (posCellule.left + posCellule.width / 2 - basex) + ' 10 L ' + (posCellule.left + posCellule.width / 2 - basex) + ' ' + num1 + ' L ' + (posCellule.left + posCellule.width / 2 - basex + wwi / 10) + ' ' + (num1 + si2 * 7) + '  M ' + (posCellule.left + posCellule.width / 2 - basex) + ' ' + num1 + ' L ' + (posCellule.left + posCellule.width / 2 - basex - wwi / 10) + ' ' + (num1 + si2 * 7) + '" stroke-width="3" stroke="' + resultat.color + '"                  fill="transparent" />'

          resultat.svg.appendChild(monSVG)
          // change svg + blok case et liste
          // change rect et lign
          monSVG.innerHTML += '<line x1="' + (wwi / 4) + '" y1="5" x2="' + (3 * wwi / 4) + '" y2="25" style="stroke:rgb(255,0,0);stroke-width:4" /><line x1="' + (wwi / 4) + '" y1="25" x2="' + (3 * wwi / 4) + '" y2="5" style="stroke:rgb(255,0,0);stroke-width:4" /><rect width="' + wwi + '" height="40" style="fill:rgb(0,0,255);fill-opacity:0;stroke-width:0;stroke:rgb(0,0,0)" />'
          monSVG.childNodes[1].style.visibility = 'hidden'
          monSVG.childNodes[2].style.visibility = 'hidden'
          monSVG.childNodes[3].qui = resultat.cellule
          if (!stor.desatout) {
            monSVG.childNodes[3].style.cursor = 'pointer'
            monSVG.childNodes[3].addEventListener('mouseover', FlecheClickSupprOver, false)
            monSVG.childNodes[3].addEventListener('mouseout', FlecheClickSupprOut, false)
            monSVG.childNodes[3].addEventListener('click', plusClick2Suppr, false)
            monSVG.childNodes[3].qui1 = monSVG.childNodes[1]
            monSVG.childNodes[3].qui2 = monSVG.childNodes[2]
            monSVG.childNodes[3].svg = resultat.svg
            monSVG.childNodes[3].op = resultat.op
            monSVG.childNodes[3].ope2 = resultat.valeur
          }
          resultat.op.style.paddingLeft = (Math.min(pos5.left, pos2.left) - pos3.left + mid) + 'px'
          resultat.wherop.style.color = resultat.color
          stor.listeCache.push(resultat.svg)
          stor.listeCache.push(resultat.wherop)
          stor.listeDoudou.push({ doudou: resultat.op, nb: (Math.min(pos5.left, pos2.left) - pos3.left + mid) + 'px' })
          stor.listeDoudou.push({ doudou: resultat.doudou2, nb: (basex - pos3.left) + 'px' })
        }
          break

        case 'ope3':
          {
            posCellule = resultat.cellule.getBoundingClientRect()
            pos2 = resultat.caseu.getBoundingClientRect()
            pos3 = stor.lesdiv.tableau.getBoundingClientRect()
            pos4 = resultat.caseu2.getBoundingClientRect()

            wwi = (pos2.left + pos2.width - posCellule.left) - 10
            if (resultat.cellule.ligne === 2) {
              num2 = 20
              num1 = 0
              si2 = 1
            } else {
              num2 = 0
              num1 = 20
              si2 = -1
            }
            if (posCellule.left < pos2.left) {
              flechdeb = 0
              si = 1
            } else {
              flechdeb = wwi
              si = -1
            }

            j3pDetruit(resultat.svg)
            resultat.svg = j3pAddElt(resultat.doudou2, 'div')
            resultat.doudou2.style.paddingLeft = (Math.min(posCellule.left, pos2.left) - pos3.left + 5) + 'px'
            resultat.svg.style.width = wwi + 'px'
            resultat.svg.style.height = '25px'
            resultat.svg.wwi = wwi

            const monSVG = document.createElementNS('http://www.w3.org/2000/svg', 'svg')
            monSVG.style.width = wwi + 'px'
            monSVG.style.height = '25px'
            monSVG.style.left = '0px'
            monSVG.style.top = '0px'
            monSVG.innerHTML = '<path d="M0 ' + num1 + ' Q ' + (wwi / 2) + ' ' + num2 + ', ' + wwi + ' ' + num1 + '  M ' + flechdeb + ' ' + num1 + ' L ' + (flechdeb + si * wwi * 10 / 55) + ' ' + (num1 + si2 * 15) + '  M ' + flechdeb + ' ' + num1 + ' L ' + (flechdeb + si * wwi * 14 / 55) + ' ' + (num1 + si2) + '" stroke-width="3" stroke="' + resultat.color + '" fill="transparent" />'
            resultat.svg.appendChild(monSVG)

            monSVG.innerHTML += '<line x1="' + (wwi / 4) + '" y1="5" x2="' + (3 * wwi / 4) + '" y2="25" style="stroke:rgb(255,0,0);stroke-width:4" /><line x1="' + (wwi / 4) + '" y1="25" x2="' + (3 * wwi / 4) + '" y2="5" style="stroke:rgb(255,0,0);stroke-width:4" /><rect width="' + wwi + '" height="40" style="fill:rgb(0,0,255);fill-opacity:0;stroke-width:0;stroke:rgb(0,0,0)" />'
            monSVG.childNodes[1].style.visibility = 'hidden'
            monSVG.childNodes[2].style.visibility = 'hidden'
            monSVG.childNodes[3].qui = resultat.cellule
            if (!stor.desatout) {
              monSVG.childNodes[3].style.cursor = 'pointer'
              monSVG.childNodes[3].addEventListener('mouseover', FlecheClickSupprOver, false)
              monSVG.childNodes[3].addEventListener('mouseout', FlecheClickSupprOut, false)
              monSVG.childNodes[3].addEventListener('click', uniteClickSuppr, false)
              monSVG.childNodes[3].qui1 = monSVG.childNodes[1]
              monSVG.childNodes[3].qui2 = monSVG.childNodes[2]
              monSVG.childNodes[3].svg = resultat.svg
              monSVG.childNodes[3].op = resultat.op
              monSVG.childNodes[3].ope2 = resultat.valeur
              monSVG.childNodes[3].valeurunit = resultat.valeurunit
              monSVG.childNodes[3].ddiv = resultat.ddiv
            }

            resultat.doudou1.style.paddingLeft = (Math.min(posCellule.left, pos2.left) - pos3.left + 5) + 'px'
            resultat.ddiv.style.color = resultat.color
            stor.listeCache.push(resultat.svg)
            stor.listeCache.push(resultat.ddiv)
            stor.listeCache.push(resultat.caseu)
            stor.listeCache.push(resultat.caseu2)
            stor.listeDoudou.push({ doudou: resultat.doudou1, nb: (Math.min(posCellule.left, pos2.left) - pos3.left + 5) + 'px' })
            stor.listeDoudou.push({ doudou: resultat.doudou2, nb: (Math.min(posCellule.left, pos2.left) - pos3.left + 5) + 'px' })
          }

          break

        case 'ope4': {
          if (stor.rectop4) {
            const lapos = stor.lesdiv.ar0.getBoundingClientRect()
            const lapos2 = me.zonesElts.MG.getBoundingClientRect()
            stor.rectop4.style.left = (lapos.left + 4) + 'px'
            stor.rectop4.style.height = (lapos.height - 2) + 'px'
            stor.rectop4.style.top = (lapos.top - lapos2.top) + 'px'
          }
          let gg = 'url("' + j3pBaseUrl + 'sections/college/sixieme/Proportionnalite/coef.png")'
          switch (stor.colorcoef) {
            case me.styles.cfaux: gg = 'url("' + j3pBaseUrl + 'sections/college/sixieme/Proportionnalite/coeffo.png")'
              break
            case me.styles.cbien: gg = 'url("' + j3pBaseUrl + 'sections/college/sixieme/Proportionnalite/coefbien.png")'
              break
          }
          stor.divCoef2.style.backgroundImage = gg
          stor.listeCache.push(stor.divCoef2)
        }
          break
        case 'ope5': /// prend pos fin
          {
            buf1 = valeurDe(resultat.cel1)
            buf2 = valeurDe(resultat.cel2)
            buf3 = valeurDe(resultat.cel3)
            j3pEmpty(resultat.c1)
            j3pAffiche(resultat.c1, null, '$' + calcul(stor.decim, buf1, null, 'i') + '$')
            j3pEmpty(resultat.c2)
            j3pAffiche(resultat.c2, null, '$' + calcul(stor.decim, buf2, null, 'i') + '$')
            j3pEmpty(resultat.c3)
            j3pAffiche(resultat.c3, null, '$' + calcul(stor.decim, buf3, null, 'i') + '$')

            posCellule = resultat.cel1.getBoundingClientRect()
            pos2 = resultat.cel2.getBoundingClientRect()
            pos3 = resultat.cel3.getBoundingClientRect()
            pos4 = resultat.cellule.getBoundingClientRect()
            pos5 = me.zonesElts.MG.getBoundingClientRect()

            if (resultat.svg) j3pDetruit(resultat.svg)
            resultat.svg = j3pAddElt(stor.lesdiv.tableau, 'div')
            resultat.svg.style.position = 'absolute'
            resultat.svg.style.pointerEvents = 'none'
            resultat.svg.style.left = (Math.min(posCellule.left, pos2.left, pos3.left, pos4.left) - pos5.left + me.zonesElts.MG.scrollLeft) + 'px'
            resultat.svg.style.top = (Math.min(posCellule.top, pos2.top, pos3.top, pos4.top) - pos5.top + me.zonesElts.MG.scrollTop) + 'px'
            if (posCellule.top === pos2.top) {
              if (pos2.top === pos3.top) {
                if (pos3.top === pos4.top) {
                  resultat.svg.style.height = posCellule.height + 'px'
                  leight = posCellule.height
                } else {
                  resultat.svg.style.height = (posCellule.height + pos4.height) + 'px'
                  leight = posCellule.height + pos4.height
                }
              } else {
                resultat.svg.style.height = (posCellule.height + pos3.height) + 'px'
                leight = posCellule.height + pos3.height
              }
            } else {
              resultat.svg.style.height = (posCellule.height + pos2.height) + 'px'
              leight = posCellule.height + pos2.height
            }
            wwi = posCellule.left + posCellule.width - Math.min(posCellule.left, pos2.left, pos3.left, pos4.left)
            if (pos2.left > posCellule.left) {
              wwi = pos2.left + pos2.width - Math.min(posCellule.left, pos2.left, pos3.left, pos4.left)
            }
            if (pos3.left > pos2.left) {
              wwi = pos3.left + pos3.width - Math.min(posCellule.left, pos2.left, pos3.left, pos4.left)
            }
            if (pos4.left > pos3.left) {
              wwi = pos4.left + pos4.width - Math.min(posCellule.left, pos2.left, pos3.left, pos4.left)
            }
            if (resultat.cel1.ligne === 1) { y1 = 5 } else { y1 = leight - 5 }
            if (resultat.cel2.ligne === 1) { y2 = 5 } else { y2 = leight - 5 }
            if (resultat.cel3.ligne === 1) { y3 = 5 } else { y3 = leight - 5 }
            if (resultat.cellule.ligne === 1) { y4 = 5 } else { y4 = leight - 5 }
            x1 = (posCellule.left + posCellule.width / 2) - Math.min(posCellule.left, pos2.left, pos3.left, pos4.left)
            x2 = (pos2.left + pos2.width / 2) - Math.min(posCellule.left, pos2.left, pos3.left, pos4.left)
            x3 = (pos3.left + pos3.width / 2) - Math.min(posCellule.left, pos2.left, pos3.left, pos4.left)
            if (pos4.left > pos3.left) { x4 = (pos4.left + 5) - Math.min(posCellule.left, pos2.left, pos3.left, pos4.left) } else if (pos4.left < pos3.left) { x4 = (pos4.left + pos4.width - 5) - Math.min(posCellule.left, pos2.left, pos3.left, pos4.left) } else { x4 = x3 }

            const monSVG = document.createElementNS('http://www.w3.org/2000/svg', 'svg')
            monSVG.style.width = wwi + 'px'
            monSVG.style.height = leight + 'px'
            monSVG.style.left = '0px'
            monSVG.style.top = '0px'
            monSVG.innerHTML = '<path d="M ' + x1 + ' ' + y1 + ' L ' + x2 + ' ' + y2 + ' L ' + x3 + ' ' + y3 + '  L ' + x4 + ' ' + y4 + '" stroke-width="3" stroke="' + resultat.color + '" fill="transparent"  />'
            resultat.svg.appendChild(monSVG)
            monSVG.style.width = wwi + 'px'
            monSVG.wwi = wwi
            monSVG.style.pointerEvents = 'none'
            monSVG.style.visibility = 'hidden'
            /// /mettre a jour les ??
            resultat.cellule.svg = monSVG
            resultat.div5.style.color = resultat.color
            stor.listeCache.push(resultat.div5)
            stor.listeCache.push(resultat.cellule.svg)
          }
          break
      }
    }

    for (let i = 0; i < stor.placeNb.length; i++) {
      if (stor.placeNb[i].substring(0, 4) === 'unit') {
        const kk = stor.placeNb[i].replace('unit', '')
        let j
        for (j = 0; j < stor.resultats.length; j++) {
          if (stor.resultats[j].numres === kk) {
            break
          }
        }
        laval = stor.resultats[j].valeurunit.reponse().replace(/ /g, '')
        stor.repEl[i] = laval
      }
    }

    if (!ds.Conclusion) {
      for (let i = 0; i < stor.placeNb.length; i++) {
        if (stor.placeNb[i] === 'rep1') {
          bub = stor.repEl[i]
          if (bub === undefined || bub === '') bub = '?'
          j3pEmpty(stor.lesdiv.concl1)
          j3pAffiche(stor.lesdiv.concl1, null, '$' + bub + '$')
        }
        if (stor.placeNb[i] === 'rep2') {
          bub = stor.repEl[i]
          if (bub === undefined || bub === '') bub = '?'
          j3pEmpty(stor.lesdiv.concl2)
          j3pAffiche(stor.lesdiv.concl2, null, '$' + bub + '$')
        }
      }
    }
    for (let i = 0; i < stor.listepdt; i++) {
      stor.listepdt[i].ou = stor.listepdt[i].this.getBoundingClientRect()
    }
  }

  function yaReponse () {
    let yaya = true
    const indiceEtape = me.questionCourante % ds.nbetapes
    if (stor.etapeAfaire[indiceEtape] === 'Legende') {
      if (!stor.ListeDeroulante2.changed) {
        yaya = false
        stor.ListeDeroulante2.focus()
      }
      if (!stor.ListeDeroulante1.changed) {
        yaya = false
        stor.ListeDeroulante1.focus()
      }
    }
    if (stor.etapeAfaire[indiceEtape] === 'Remplir') {
      let nbrep = 0
      for (let i = 0; i < stor.listeInput.length; i++) {
        if (stor.listeInput[i].reponse() !== '') nbrep++
      }
      if (nbrep < stor.nbRepAtt) {
        yaya = false
        affModale('Tu dois inscrire ' + stor.nbRepAtt + ' valeurs dans le tableau !')
      } else if (nbrep > stor.nbRepAtt) {
        yaya = false
        affModale('Tu ne peux pas écrire plus de ' + stor.nbRepAtt + ' valeurs dans le tableau !')
      }
    }
    if ((['les deux', 'Raisonnement', 'Reponse'].indexOf(stor.etapeAfaire[indiceEtape]) !== -1) || (ds.nbetapes === 1)) {
      if (ds.Completer === 'Reponse') {
        adapteLarg.call({}) // FIXMEok ici on lui passait me en this, alors que c’est un listener qui s’attend à recevoir un elt en this, en attendant on lui passe un objet vide pour que ça plante pas
        /// c’est une histoire de couleur, adaptelarg redimensionne le tableau et tout ce qu’il y a autour en fonction
        /// du contenu des cases (il recalcule parfois les valeurs du tableau en fonction du mode)
        /// quand on est au 2e essai , si on modifie un élément , il repasse en noir (il est plus marqué faux ou bon puisqu’il a changé)
        // mais comme avec une case y’a des fleche et des trucs en plus ,il e fallait l’id du truc modifié pour changr les couleurs des élèment
        // qui vont avec
        /// si adateptlarg est lancé avec rien, c’est qu’aucune couleur ne va changer, normalement la fonction teste si c’est vide , elle passe
        for (let i = 0; i < stor.listeInput.length; i++) {
          if (stor.listeInput[i].reponse() === '') {
            stor.listeInput[i].focus()
            return false
          }
        }
      } else {
        if (stor.menus.length) {
          affModale('Clique sur un point d’interrogation du tableau pour compléter !')
          return false
        }
        let resultat
        for (let i = 0; i < stor.resultats.length; i++) {
          resultat = stor.resultats[i]
          switch (resultat.methode) {
            case 'ope1':
              if (ds.Completer === 'les deux') {
                if (resultat.valeur.reponse() === '') {
                  yaya = false
                  resultat.valeur.focus()
                }
              }
              if (resultat.op.reponse() === '') {
                yaya = false
                resultat.op.focus()
              }
              break

            case 'ope2':
            case 'ope2moins':
              if (ds.Completer === 'les deux') {
                if (resultat.valeur.reponse() === '') {
                  yaya = false
                  resultat.valeur.focus()
                }
              }
              break
            case 'ope3':
              if (ds.Completer === 'les deux') {
                if (resultat.valeur.reponse() === '') {
                  yaya = false
                  resultat.valeur.focus()
                }
              }
              if (resultat.valeurunit.reponse() === '') {
                yaya = false
                resultat.valeurunit.focus()
              }
              break

            case 'ope4': // cellule:exclu,methode:'ope4',valeur:"ope4"+exclu+'inputmq1'})
              if (ds.Completer === 'les deux') {
                if (resultat.valeur.reponse() === '') {
                  yaya = false
                  resultat.valeur.focus()
                }
              }
              if (stor.zoneCoef.reponse() === '') {
                yaya = false
                stor.zoneCoef.focus()
              }
              break

            case 'ope5':
              if (ds.Completer === 'les deux') {
                if (resultat.valeur.reponse() === '') {
                  yaya = false
                  resultat.valeur.focus()
                }
              }
              break
          } // switch
        }
      }
    }
    if (stor.etapeAfaire[indiceEtape] === 'Conclusion') {
      if (stor.zoneConcl1.reponse() === '') {
        stor.zoneConcl1.focus()
        return false
      }
      if (stor.zoneConcl2.reponse() === '') {
        stor.zoneConcl2.focus()
        return false
      }
    }
    return yaya
  }

  function isRepOk () {
    const indiceEtape = me.questionCourante % ds.nbetapes
    let numden, nb, grand, jim, pla, buf1, buf2, buf3, val, lali
    if (stor.etapeAfaire[indiceEtape] === 'Legende') {
      return ((stor.ListeDeroulante2.reponse === stor.pbencours.legende2) && (stor.ListeDeroulante1.reponse === stor.pbencours.legende1))
    }
    if (stor.etapeAfaire[indiceEtape] === 'Remplir') {
      stor.repEl = []
      stor.ErrEcrit = false
      stor.ErrEcritlist = []
      stor.ErrInvent = false
      stor.ErrInventNum = []
      stor.ErrOubli = false
      stor.ErrOublitListe = []
      stor.ErrAsso1 = false
      stor.ErrAsso1list = []
      stor.ErrDouble = false
      stor.ErrDoublelist = []
      stor.ErrGrand = false
      stor.ErrGrandlist = []
      for (let i = 0; i < stor.listeInput.length; i++) {
        stor.repEl.push(stor.listeInput[i].reponse().replace(/ /g, ''))
      }
      // verifie que sont des nombres ou rien
      for (let i = 0; i < stor.repEl.length; i++) {
        if (stor.repEl[i]) {
          if (stor.repEl[i].indexOf('frac') === -1) {
            if (isNaN((stor.repEl[i]).replace(',', '.'))) {
              stor.ErrEcrit = true
              stor.ErrEcritlist.push(i)
            }
          } else {
            numden = j3pExtraireNumDen(stor.repEl[i])
            if (numden[2].indexOf('}') !== -1) {
              stor.ErrEcrit = true
              stor.ErrEcritlist.push(i)
            } else if ((isNaN(numden[1])) || (numden[1] === ' ')) {
              stor.ErrEcrit = true
              stor.ErrEcritlist.push(i)
            } else if ((isNaN(numden[2])) || (numden[2] === 0) || (numden[2] === ' ')) {
              stor.ErrEcrit = true
              stor.ErrEcritlist.push(i)
            }
          }
        }
      }
      if (stor.ErrEcrit) return false
      // verifie que les nombres rentrés sont présents dans l’énoncé
      for (let i = 0; i < stor.repEl.length; i++) {
        if (!stor.repEl[i]) continue
        if (stor.repEl[i].indexOf('frac') === -1) {
          nb = (stor.repEl[i] + '').replace(',', '.')
        } else {
          numden = j3pExtraireNumDen(stor.repEl[i])
          nb = numden[1] / numden[2]
        }
        let ok = false
        ok = ok || (Math.abs(nb - (stor.nb1g1.num / stor.nb1g1.den)) < Math.pow(10, -10))
        ok = ok || (Math.abs(nb - (stor.nb1g2.num / stor.nb1g2.den)) < Math.pow(10, -10))
        ok = ok || (Math.abs(nb - (stor.nbxg1.num / stor.nbxg1.den)) < Math.pow(10, -10))
        ok = ok || (Math.abs(nb - (stor.nbxg2.num / stor.nbxg2.den)) < Math.pow(10, -10))
        if (stor.nbRepAtt === 6) {
          ok = ok || (Math.abs(nb - (stor.nb2g1.num / stor.nb2g1.den)) < Math.pow(10, -10))
          ok = ok || (Math.abs(nb - (stor.nb2g2.num / stor.nb2g2.den)) < Math.pow(10, -10))
        }
        if (!ok) {
          stor.ErrInvent = true
          stor.ErrInventNum.push(i)
        } else { /// verif bonne grandeur
          if (stor.nbRepAtt === 6) {
            grand = ((Math.abs(nb - (stor.nb1g1.num / stor.nb1g1.den)) < Math.pow(10, -10)) ||
              (Math.abs(nb - (stor.nbxg1.num / stor.nbxg1.den)) < Math.pow(10, -10)) ||
              (Math.abs(nb - (stor.nb2g1.num / stor.nb2g1.den)) < Math.pow(10, -10)))
          } else {
            grand = ((Math.abs(nb - (stor.nb1g1.num / stor.nb1g1.den)) < Math.pow(10, -10)) ||
              (Math.abs(nb - (stor.nbxg1.num / stor.nbxg1.den)) < Math.pow(10, -10)))
          }
          if (grand) {
            if ((i % 2) === 1) {
              stor.ErrGrand = true
              stor.ErrGrandlist.push(i)
            }
          } else {
            if ((i % 2) === 0) {
              stor.ErrGrand = true
              stor.ErrGrandlist.push(i)
            }
          }
        }
      }

      // FIXMEok virer les affectations de stor.ErrOubli qui servent pas (ne garder que la dernière)
      // elles servent toutes
      /// stor.errOubli est vrai si y’a au moins un oubli de vrai
      const oublinb1g1 = verifOubli(stor.nb1g1)
      stor.ErrOubli = oublinb1g1.rep
      const oublinb1g2 = verifOubli(stor.nb1g2)
      stor.ErrOubli = oublinb1g2.rep || stor.ErrOubli
      const oublinbxg2 = verifOubli(stor.nbxg2)
      stor.ErrOubli = oublinbxg2.rep || stor.ErrOubli
      const oublinbxg1 = verifOubli(stor.nbxg1)
      let oublinb2g1 = { rep: false, place: -1 }
      let oublinb2g2 = { rep: false, place: -1 }
      stor.ErrOubli = oublinbxg1.rep || stor.ErrOubli
      if (stor.nbRepAtt === 6) {
        oublinb2g1 = verifOubli(stor.nb2g1)
        stor.ErrOubli = oublinb2g1.rep || stor.ErrOubli
        oublinb2g2 = verifOubli(stor.nb2g2)
        stor.ErrOubli = oublinb2g2.rep || stor.ErrOubli
      }

      // vérifie les asso
      if ((!stor.ErrOubli) && (!stor.ErrInvent) && (!stor.ErrGrand) && (!stor.ErrDouble)) {
        let imax = 3
        if (stor.nbRepAtt === 6) imax = 4
        for (let i = 0; i < imax; i++) {
          if (oublinb1g1.place === i * 2) {
            if (oublinb2g2.place === i * 2 + 1) {
              stor.ErrAsso1 = true
              stor.ErrAsso1list.push({ col: i, lequel: 'nb1', avec: 'nb2' })
            }
            if (oublinbxg2.place === i * 2 + 1) {
              stor.ErrAsso1 = true
              stor.ErrAsso1list.push({ col: i, lequel: 'nb1', avec: 'nbx' })
            }
          }
          if (oublinbxg1.place === i * 2) {
            if (oublinb2g2.place === i * 2 + 1) {
              stor.ErrAsso1 = true
              stor.ErrAsso1list.push({ col: i, lequel: 'nbx', avec: 'nb2' })
            }
            if (oublinbxg2.place === i * 2 + 1) {
              stor.ErrAsso1 = true
              stor.ErrAsso1list.push({ col: i, lequel: 'nbx', avec: 'nbx' })
            }
            if (oublinb1g2.place === i * 2 + 1) {
              stor.ErrAsso1 = true
              stor.ErrAsso1list.push({ col: i, lequel: 'nbx', avec: 'nb1' })
            }
          }
          if (oublinb2g1.place === i * 2) {
            if (oublinb1g2.place === i * 2 + 1) {
              stor.ErrAsso1 = true
              stor.ErrAsso1list.push({ col: i, lequel: 'nb2', avec: 'nb1' })
            }
            if (oublinbxg2.place === i * 2 + 1) {
              stor.ErrAsso1 = true
              stor.ErrAsso1list.push({ col: i, lequel: 'nb2', avec: 'nbx' })
            }
          }
        }
      }

      return (!stor.ErrOubli) && (!stor.ErrInvent) && (!stor.ErrGrand) && (!stor.ErrAsso1)
    }
    if ((['les deux', 'Raisonnement', 'Reponse'].indexOf(stor.etapeAfaire[indiceEtape]) !== -1) || (ds.nbetapes === 1)) {
      // test les deux rep
      j3pMasqueFenetre('Calculatrice')
      stor.ErrOpe1Op = false
      stor.ErrOpe1Op2 = false
      stor.err = false
      stor.ErrOpe2Op = false
      stor.ErrOpe2Opmoins = false
      stor.errUnit = false
      stor.errUnitRais = false
      stor.errCoef = false
      stor.ErrPdt1 = false
      stor.ErrPdt2 = false
      stor.listeErr = []

      stor.repElco = j3pClone(stor.repEl)
      let n1 = (stor.nbRepAtt === 4) ? 3 : 5
      const rep1 = recupRep(stor.placeNb, stor.repEl, 1)
      const rep2 = recupRep(stor.placeNb, stor.repEl, 2)
      const nb1g1 = tabloise(stor.nb1g1)
      const nb1g2 = tabloise(stor.nb1g2)
      const nbxg1 = tabloise(stor.nbxg1)
      let res1 = trans(calcul(stor.decim, nb1g2, nbxg1, '×'))
      let res2 = trans(calcul(stor.decim, nb1g1, res1, '÷'))
      stor.ok2 = egalite(res2, rep1)
      if (!stor.ok2) {
        stor.err = true
        stor.listeErr.push([res2, 'one'])
      }

      stor.repElco[n1] = res2
      const nbxg2 = tabloise(stor.nbxg2)
      res1 = trans(calcul(stor.decim, nb1g1, nbxg2, '×'))
      res2 = trans(calcul(stor.decim, nb1g2, res1, '÷'))
      stor.ok1 = egalite(res2, rep2)
      if (!stor.ok1) {
        stor.err = true
        stor.listeErr.push([res2, 'two'])
      }

      n1 = (stor.nbRepAtt === 4) ? 4 : 6
      stor.repElco[n1] = res2

      // si les  deux ou raisonnement
      // test les raisonnements
      if (ds.Completer !== 'Reponse') {
        let resultat
        for (let i = 0; i < stor.resultats.length; i++) {
          resultat = stor.resultats[i]
          switch (resultat.methode) {
            case 'ope1': {
              // verif op
              buf1 = trans(resultat.op.reponse().replace(/ /g, ''))
              if (resultat.cellule.ligne === resultat.col12.ligne) {
                jim = resultat.col1
              } else {
                jim = resultat.col12
              }
              pla = (jim.ligne - 1) + 2 * (jim.colonne - 1)
              buf2 = trans(stor.repEl[pla])
              const listop = resultat.liste
              jim = resultat.casa
              pla = (jim.ligne - 1) + 2 * (jim.colonne - 1)
              buf3 = trans(stor.repEl[pla])

              if ((listop.reponse === '+') || (listop.reponse === '-')) {
                stor.ErrOpe1Op = true
                stor.err = true
                stor.listeErr.push([i, 'op1op'])
              } else {
                val = trans(calcul(stor.decim, buf1, buf2, listop.reponse))
                if (!egalite(val, buf3)) {
                  stor.ErrOpe1Op2 = true
                  stor.err = true
                  stor.listeErr.push([i, 'op1op2', buf2, buf1, listop.reponse, buf3])
                }
              }
              // verif nb
              break
            }

            case 'ope2':
              // verif add fonctionne
              // cellule:d.casedep,methode:'ope2',op:"ope2"+d.exclu,valeur:d.casedep+'z',col1:d.id1,col12:d.id2,col2:d.mem,col22:d.mem2})

              lali = resultat.cellule.ligne
              if (lali === 1) { lali = 2 } else { lali = 1 }

              jim = stor.tabProp[lali][resultat.cellule.colonne]
              pla = (jim.ligne - 1) + 2 * (jim.colonne - 1)
              buf3 = trans(stor.repEl[pla])

              jim = stor.tabProp[lali][resultat.col1.colonne]
              pla = (jim.ligne - 1) + 2 * (jim.colonne - 1)
              buf1 = trans(stor.repEl[pla])

              jim = stor.tabProp[lali][resultat.col2.colonne]
              pla = (jim.ligne - 1) + 2 * (jim.colonne - 1)
              buf2 = trans(stor.repEl[pla])

              val = trans(calcul(stor.decim, buf1, buf2, '+'))
              if (!egalite(val, buf3)) {
                stor.ErrOpe2Op = true
                stor.err = true
                stor.listeErr.push([i, 'op2op', buf1, buf2, buf3])
              }
              break
            case 'ope2moins': {
              // verif sou fonctionne
              lali = resultat.cellule.ligne
              if (lali === 1) { lali = 2 } else { lali = 1 }

              jim = stor.tabProp[lali][resultat.cellule.colonne]
              pla = (jim.ligne - 1) + 2 * (jim.colonne - 1)
              buf3 = trans(stor.repEl[pla])

              jim = stor.tabProp[lali][resultat.col1.colonne]
              pla = (jim.ligne - 1) + 2 * (jim.colonne - 1)
              buf1 = trans(stor.repEl[pla])

              jim = stor.tabProp[lali][resultat.col2.colonne]
              pla = (jim.ligne - 1) + 2 * (jim.colonne - 1)
              buf2 = trans(stor.repEl[pla])

              val = trans(calcul(stor.decim, buf1, buf2, '-'))
              const valse = trans(calcul(stor.decim, buf2, buf1, '-'))
              if ((!egalite(val, buf3)) && (!egalite(valse, buf3))) {
                stor.ErrOpe2Opmoins = true
                stor.err = true
                stor.listeErr.push([i, 'op2moinsop', buf1, buf2, buf3])
              }
              break
            }
            case 'ope3': {
              // multi:bub,cellule:d.qui,methode:'ope3',op:'ope3'+d.qui+'multinputmq1',valeurunit:"op32"+d.id2+"inputmq1",valeur:d.qui+"z",caseu:d.id,caseu2:d.id2})
              // verif unite bonne
              buf2 = trans(stor.repEl[0])
              buf3 = trans(stor.repEl[1])
              let j
              for (j = 0; j < stor.placeNb.length; j++) {
                if (stor.placeNb[j] === 'unit' + resultat.numres) {
                  break
                }
              }
              buf1 = trans(stor.repEl[j])
              if ((j % 2) === 0) { val = trans(calcul(stor.decim, buf3, buf2, '÷')) } else {
                val = trans(calcul(stor.decim, buf2, buf3, '÷'))
              }

              if (!egalite(val, buf1)) {
                stor.errUnit = true
                stor.err = true
                stor.listeErr.push([i, 'op3unit', val])
              }

              if (ds.Completer !== 'Raisonnement') {
                if (resultat.multi === '?') {
                  stor.errUnitRais = true
                  stor.err = true
                  stor.listeErr.push([i, 'op3unitrais'])
                }
              }
              // verif ?
              break
            }

            case 'ope4':
              // verif coef bon
              buf2 = trans(stor.repEl[0])
              buf3 = trans(stor.repEl[1])
              buf1 = trans(stor.zoneCoef.reponse().replace(/ /g, ''))
              val = trans(calcul(stor.decim, buf2, buf3, '÷'))
              if (!egalite(val, buf1)) {
                stor.errCoef = true
                stor.err = true
                stor.listeErr.push([i, 'op4'])
              }
              break

            case 'ope5': {
              // verif produit en croix
              const cel0 = resultat.cellule
              const cel1 = resultat.cel1
              const cel2 = resultat.cel2
              const cel3 = resultat.cel3

              // verifie que 2 colonne
              const ligne0 = cel0.ligne
              const colonne0 = cel0.colonne
              const lignea = (ligne0 === 1) ? 2 : 1

              const listecolonne = [colonne0]
              if (listecolonne.indexOf(cel1.colonne) === -1) listecolonne.push(cel1.colonne)
              if (listecolonne.indexOf(cel2.colonne) === -1) listecolonne.push(cel2.colonne)
              if (listecolonne.indexOf(cel3.colonne) === -1) listecolonne.push(cel3.colonne)

              if (listecolonne.length > 2) {
                stor.ErrPdt2 = true
                stor.err = true
                stor.listeErr.push([i, 'op52'])
              } else {
                if (((cel1.ligne !== lignea) || (cel1.colonne !== colonne0)) && ((cel2.ligne !== lignea) || (cel2.colonne !== colonne0))) {
                  stor.ErrPdt1 = true
                  stor.err = true
                  stor.listeErr.push([i, 'op51', cel1, cel2, cel3])
                } else {
                  const lautre = ((cel1.ligne !== lignea) || (cel1.colonne !== colonne0)) ? cel1 : cel2
                  // verifie que l’autre (cel1 ou cel2) est mm ligne que cel0
                  if (lautre.ligne !== ligne0) {
                    stor.ErrPdt1 = true
                    stor.err = true
                    stor.listeErr.push([i, 'op51', cel1, cel2, cel3])
                  }
                }
              }
              break
            }
          }
        }
      }

      return !stor.err
    }
    if (stor.etapeAfaire[indiceEtape] === 'Conclusion') {
      const rep2 = trans(stor.zoneConcl2.reponse().replace(/ /g, ''))
      const rep1 = trans(stor.zoneConcl1.reponse().replace(/ /g, ''))
      let rep1att, rep2att
      if (stor.nbRepAtt === 6) {
        rep1att = trans(stor.repElco[5])
        rep2att = trans(stor.repElco[6])
      } else {
        rep1att = trans(stor.repElco[3])
        rep2att = trans(stor.repElco[4])
      }
      stor.rep1attaff = calcul(stor.decim, rep1att, 1, 'i')
      stor.rep2attaff = calcul(stor.decim, rep2att, 1, 'i')
      stor.ok1 = egalite(rep1, rep1att)
      stor.ok2 = egalite(rep2, rep2att)
      return (stor.ok1 && stor.ok2)
    }
  }

  function desactiveAll () {
    stor.desatout = true
    j3pEmpty(stor.lesdiv.supCro)
    let resultat
    for (let i = 0; i < stor.resultats.length; i++) {
      resultat = stor.resultats[i]
      switch (resultat.methode) {
        case 'ope1':
          resultat.liste.disable()
          resultat.op.disable()
          break
        case 'ope2':
          // j3pDesactive('ope2' + resultat.cellule.colonne)
          break
        case 'ope2moins':
          // j3pDesactive('ope2moins' + resultat.cellule.colonne)
          break
        case 'ope3':
          resultat.valeurunit.disable()
          break
        case 'ope4':
          if (stor.rectop4) {
            j3pDetruit(stor.rectop4)
            stor.rectop4 = undefined
          }
          stor.zoneCoef.disable()
          break
        case 'ope5':
          resultat.div5.style.visibility = 'visible'
          break
      }
    }
    if (ds.Completer !== 'Raisonnement') {
      stor.inputrep1.disable()
      stor.inputrep2.disable()
    }
    adapteLarg()
  }

  function passeToutVert () {
    let resultat
    for (let i = 0; i < stor.resultats.length; i++) {
      resultat = stor.resultats[i]
      resultat.color = me.styles.cbien
      switch (resultat.methode) {
        case 'ope1':
          resultat.liste.corrige(true)
          resultat.op.corrige(true)
          break
        case 'ope2':
          resultat.op.style.color = me.styles.cbien
          break
        case 'ope2moins':
          resultat.op.style.color = me.styles.cbien
          break
        case 'ope3':
          resultat.valeurunit.corrige(true)
          break
        case 'ope4':
          stor.zoneCoef.corrige(true)
          stor.colorcoef = me.styles.cbien
          break
        case 'ope5':
          resultat.div5.style.visibility = 'visible'
          resultat.color = me.styles.cbien
          break
      }
    }
    if (ds.Completer !== 'Raisonnement') {
      stor.inputrep1.corrige(true)
      stor.inputrep2.corrige(true)
    }
    if (!ds.Conclusion) {
      stor.lesdiv.concl1.style.color = me.styles.cbien
      stor.lesdiv.concl2.style.color = me.styles.cbien
    }
  }

  function affCorrFaux (isFin) {
    const indiceEtape = me.questionCourante % ds.nbetapes
    let opts, adire, numres

    if (stor.etapeAfaire[indiceEtape] === 'Legende') {
      stor.ListeDeroulante1.corrige(stor.ListeDeroulante1.reponse === stor.pbencours.legende1)
      stor.ListeDeroulante2.corrige(stor.ListeDeroulante2.reponse === stor.pbencours.legende2)
      if (isFin) {
        stor.lesdiv.explik.classList.add('correction')
        opts = (stor.nbRepAtt === 6) ? 5 : 4
        const ii = makeDef(stor.lesdiv.explik, 4, opts)
        j3pAffiche(ii[1][0], null, stor.pbencours.legende1)
        j3pAffiche(ii[2][0], null, stor.pbencours.legende2)
        if (stor.ListeDeroulante1.reponse !== stor.pbencours.legende1) {
          stor.ListeDeroulante1.barre()
        }
        if (stor.ListeDeroulante2.reponse !== stor.pbencours.legende2) {
          stor.ListeDeroulante2.barre()
        }
        stor.ListeDeroulante1.disable()
        stor.ListeDeroulante2.disable()
        stor.Etapefaite.push('Legende')
      }
    }
    if (stor.etapeAfaire[indiceEtape] === 'Remplir') {
      let br = ''
      if (!ds.theme === 'zonesAvecImageDeFond') br = '<BR>'

      if (stor.ErrEcrit) {
        adire = 'Cette écriture ne convient pas: '
        if (stor.ErrEcritlist.length > 1) {
          adire = 'Ces écritures ne conviennent pas: '
        }
        for (let i = 0; i < stor.ErrEcritlist.length; i++) {
          stor.listeInput[stor.ErrEcritlist[i]].corrige(false)
          if (isFin) stor.listeInput[stor.ErrEcritlist[i]].barre()
          adire += ' $' + stor.repEl[stor.ErrEcritlist[i]] + '$ ;'
        }
        adire = adire.substring(0, adire.length - 1) + '!'
        j3pAffiche(stor.lesdiv.explik2, null, br + adire)
        stor.yaeco = true
      } else {
        for (let i = 0; i < stor.listeInput.length; i++) {
          stor.listeInput[i].corrige(true)
        }

        if (stor.ErrOubli) {
          j3pAffiche(stor.lesdiv.explik2, null, br + ' Je ne retrouve pas toutes les données de l’énoncé !')
          stor.yaeco = true
          br = '<BR>'
        }
        if (stor.ErrInvent) {
          adire = 'Cette donnée ne figure pas dans l’énoncé: '
          if (stor.ErrInventNum.length > 1) {
            adire = 'Ces données ne figurent pas dans l’énoncé: '
          }
          for (let i = 0; i < stor.ErrInventNum.length; i++) {
            stor.listeInput[stor.ErrInventNum[i]].corrige(false)
            if (isFin) stor.listeInput[stor.ErrInventNum[i]].barre()
            adire += ' $' + stor.repEl[stor.ErrInventNum[i]] + '$ ;'
          }
          adire = adire.substring(0, adire.length - 1) + '!'
          j3pAffiche(stor.lesdiv.explik2, null, br + adire)
          stor.yaeco = true
          br = '<BR>'
        }

        if (stor.ErrDouble) {
          adire = 'Tu as écrit plusieur fois la même donnée ! '

          for (let i = 0; i < stor.ErrDoublelist.length; i++) {
            stor.listeInput[stor.ErrDoublelist[i]].corrige(false)
            if (isFin) stor.listeInput[stor.ErrDoublelist[i]].barre()
          }

          j3pAffiche(stor.lesdiv.explik2, null, br + adire)
          stor.yaeco = true
          br = '<BR>'
        }

        if (stor.ErrGrand) {
          adire = 'Cette donnée n’est pas dans la bonne ligne: '
          if (stor.ErrGrandlist.length > 1) {
            adire = 'Ces données ne sont pas dans la bonne ligne: '
          }
          for (let i = 0; i < stor.ErrGrandlist.length; i++) {
            stor.listeInput[stor.ErrGrandlist[i]].corrige(false)
            if (isFin) stor.listeInput[stor.ErrGrandlist[i]].barre()
            adire += ' $' + stor.repEl[stor.ErrGrandlist[i]] + '$ ;'
          }
          adire = adire.substring(0, adire.length - 1) + '!'
          j3pAffiche(stor.lesdiv.explik2, null, br + adire)
          stor.yaeco = true
          br = '<BR>'
        }
        if (stor.ErrAsso1) {
          for (let i = 0; i < stor.ErrAsso1list.length; i++) {
            stor.listeInput[stor.ErrAsso1list[i].col * 2].corrige(false)
            stor.listeInput[stor.ErrAsso1list[i].col * 2 + 1].corrige(false)
            if (isFin) {
              stor.listeInput[stor.ErrAsso1list[i].col * 2].barre()
              stor.listeInput[stor.ErrAsso1list[i].col * 2 + 1].barre()
            }
            let aff1
            switch (stor.ErrAsso1list[i].lequel) {
              case 'nb1':
                aff1 = stor.afnb1g1
                break
              case 'nb2':
                aff1 = stor.afnb2g1
                break
              case 'nbx':
                aff1 = stor.afnbxg1
                break
            }
            let aff2
            switch (stor.ErrAsso1list[i].avec) {
              case 'nb1':
                aff2 = stor.afnb1g2
                break
              case 'nb2':
                aff2 = stor.afnb2g2
                break
              case 'nbx':
                aff2 = stor.afnbxg2
                break
            }
            adire = "Il n’est pas écrit que ''$" + aff1 + '$ ' + stor.pbencours.unite1 + "'' corresponde à  ''$" + aff2 + '$ ' + stor.pbencours.unite2 + "'' !"
            j3pAffiche(stor.lesdiv.explik2, null, br + adire)
            stor.yaeco = true
          }
        }
      }

      if (isFin) {
        stor.lesdiv.explik.classList.add('correction')
        stor.repEl = []
        stor.placeNb = []
        stor.repEl.push(stor.afnb1g1)
        stor.placeNb.push('nb1g1')
        stor.repEl.push(stor.afnb1g2)
        stor.placeNb.push('nb1g2')
        if (stor.nbRepAtt === 6) {
          stor.repEl.push(stor.afnb2g1)
          stor.placeNb.push('nb2g1')
          stor.repEl.push(stor.afnb2g2)
          stor.placeNb.push('nb2g2')
        }

        // on ajoute le tableau
        opts = (stor.nbRepAtt === 6) ? 5 : 4
        const uu = makeDef(stor.lesdiv.explik, 4, opts)
        stor.repEl.push(stor.afnbxg1)
        stor.placeNb.push('nbxg1')
        stor.repEl.push('')
        stor.placeNb.push('rep1')
        stor.repEl.push('')
        stor.placeNb.push('rep2')
        stor.repEl.push(stor.afnbxg2)
        stor.placeNb.push('nbxg2')
        j3pAffiche(uu[1][0], null, stor.pbencours.legende1)
        j3pAffiche(uu[2][0], null, stor.pbencours.legende2)
        for (let i = 0; i < stor.repEl.length; i++) {
          let ligne = 1
          if (i % 2 === 1) ligne = 2
          let colonne = 1
          if (i > 1) colonne = 2
          if (i > 3) colonne = 3
          if (i > 5) colonne = 4
          j3pEmpty(uu[ligne][colonne])
          j3pAffiche(uu[ligne][colonne], null, '$' + stor.repEl[i] + '$')
        }
        stor.Etapefaite.push('Legende')
        for (let i = 0; i < stor.listeInput.length; i++) { stor.listeInput[i].disable() }
      }
    }
    if ((['les deux', 'Raisonnement', 'Reponse'].indexOf(stor.etapeAfaire[indiceEtape]) !== -1) || (ds.nbetapes === 1)) {
      stor.lesdiv.explik.innerHTML = ''

      let spa = ''
      stor.yaeco = false

      passeToutVert()

      let n1, n2, op, n3
      if (stor.ErrOpe1Op) {
        j3pAffiche(stor.lesdiv.explik2, null, spa + 'On ne peut pas ajouter ou soustraire un nombre à une colonne !')
        spa = '<BR>'
        stor.yaeco = true
        for (let i = 0; i < stor.listeErr.length; i++) {
          if (stor.listeErr[i][1] === 'op1op') {
            numres = stor.listeErr[i][0]
            stor.resultats[numres].color = me.styles.cfaux
            stor.resultats[numres].liste.corrige(false)
            stor.resultats[numres].op.corrige(false)
          }
        }
      }

      if (stor.ErrOpe1Op2) {
        for (let i = 0; i < stor.listeErr.length; i++) {
          if (stor.listeErr[i][1] === 'op1op2') {
            n1 = calcul(stor.decim, stor.listeErr[i][2], '', 'i')
            n2 = calcul(stor.decim, stor.listeErr[i][3], '', 'i')
            op = stor.listeErr[i][4]
            n3 = calcul(stor.decim, stor.listeErr[i][5], '', 'i')
            j3pAffiche(stor.lesdiv.explik2, null, spa + '$' + n1 + '$' + op + '$' + n2 + '$ n’est pas égal à $' + n3 + '$ !')
            spa = '<BR>'
            stor.yaeco = true
            numres = stor.listeErr[i][0]
            stor.resultats[numres].color = me.styles.cfaux
            stor.resultats[numres].liste.corrige(false)
            stor.resultats[numres].op.corrige(false)
          }
        }
      }

      if (stor.ErrOpe2Op) {
        for (let i = 0; i < stor.listeErr.length; i++) {
          if (stor.listeErr[i][1] === 'op2op') {
            n1 = calcul(stor.decim, stor.listeErr[i][2], '', 'i')
            n2 = calcul(stor.decim, stor.listeErr[i][3], '', 'i')
            op = '+'
            n3 = calcul(stor.decim, stor.listeErr[i][4], '', 'i')
            j3pAffiche(stor.lesdiv.explik2, null, spa + '$' + n1 + '$' + op + '$' + n2 + '$ n’est pas égal à $' + n3 + '$ !')
            spa = '<BR>'
            stor.yaeco = true
            numres = stor.listeErr[i][0]
            stor.resultats[numres].color = me.styles.cfaux
            stor.resultats[numres].op.style.color = me.styles.cfaux
          }
        }
      }

      if (stor.ErrOpe2Opmoins) {
        for (let i = 0; i < stor.listeErr.length; i++) {
          if (stor.listeErr[i][1] === 'op2moinsop') {
            n1 = calcul(stor.decim, stor.listeErr[i][2], '', 'i')
            n2 = calcul(stor.decim, stor.listeErr[i][3], '', 'i')
            op = '-'
            n3 = calcul(stor.decim, stor.listeErr[i][4], '', 'i')
            j3pAffiche(stor.lesdiv.explik2, null, spa + '$' + n1 + '$' + op + '$' + n2 + '$ n’est pas égal à $' + n3 + '$ !')
            spa = '<BR>'
            stor.yaeco = true
            numres = stor.listeErr[i][0]
            stor.resultats[numres].color = me.styles.cfaux
            stor.resultats[numres].op.style.color = me.styles.cfaux
          }
        }
      }

      if (stor.errUnitRais) {
        for (let i = 0; i < stor.listeErr.length; i++) {
          if (stor.listeErr[i][1] === 'op3unitrais') {
            numres = stor.listeErr[i][0]
            stor.resultats[numres].color = me.styles.cfaux
            stor.resultats[numres].ddiv.style.color = me.styles.cfaux
          }
        }
        j3pAffiche(stor.lesdiv.explik2, null, spa + 'Tu as placé une unité dans la mauvaise ligne !')
        spa = '<BR>'
        stor.yaeco = true
      }

      if (stor.errUnit) {
        for (let i = 0; i < stor.listeErr.length; i++) {
          if (stor.listeErr[i][1] === 'op3unit') {
            numres = stor.listeErr[i][0]
            stor.resultats[numres].valeurunit.corrige(false)
            const ll = stor.resultats[numres].caseu2.ligne
            const uns = (ll === 2) ? stor.pbencours.unite1sing : stor.pbencours.unite2sing
            j3pAffiche(stor.lesdiv.explik2, null, spa + 'La valeur pour 1 ' + uns + ' est fausse !')
            spa = '<BR>'
            stor.yaeco = true
          }
        }
      }

      if (stor.errCoef) {
        j3pAffiche(stor.lesdiv.explik2, null, spa + 'Le coefficient de proportionnalité est faux !')
        spa = '<BR>'
        stor.yaeco = true
        stor.zoneCoef.corrige(false)
        stor.colorcoef = me.styles.cfaux
      }

      if (stor.ErrPdt1) {
        j3pAffiche(stor.lesdiv.explik2, null, spa + 'Pour le produit en croix, on multiplie la diagonale, puis on divise par le troisième !')
        spa = '<BR>'
        stor.yaeco = true
        for (let i = 0; i < stor.listeErr.length; i++) {
          if (stor.listeErr[i][1] === 'op51') {
            numres = stor.listeErr[i][0]
            stor.resultats[numres].color = me.styles.cfaux
            stor.resultats[numres].div5.style.visibility = 'visible'
            stor.resultats[numres].div5.style.color = me.styles.cfaux
          }
        }
      }

      if (stor.ErrPdt2) {
        j3pAffiche(stor.lesdiv.explik2, null, spa + 'Dans un produit en croix, les 4 nombres doivent appartenir à 2 colonnes !')
        stor.yaeco = true
        for (let i = 0; i < stor.listeErr.length; i++) {
          if (stor.listeErr[i][1] === 'op52') {
            numres = stor.listeErr[i][0]
            stor.resultats[numres].color = me.styles.cfaux
            stor.resultats[numres].div5.style.visibility = 'visible'
            stor.resultats[numres].div5.style.color = me.styles.cfaux
          }
        }
      }

      if (ds.Completer !== 'Raisonnement') {
        if (!stor.ok1) {
          stor.inputrep2.corrige(false)
          if (!ds.Conclusion) {
            stor.lesdiv.concl2.style.color = me.styles.cfaux
          }
        }
        if (!stor.ok2) {
          stor.inputrep1.corrige(false)
          if (!ds.Conclusion) {
            stor.lesdiv.concl1.style.color = me.styles.cfaux
          }
        }
      }

      if (isFin) {
        desactiveAll()
        stor.paspas = false
      }
      adapteLarg()
      if (isFin) {
        creeBoutonCorr()
      }
    }
    if (stor.etapeAfaire[indiceEtape] === 'Conclusion') {
      stor.zoneConcl2.corrige(stor.ok2)
      stor.zoneConcl1.corrige(stor.ok1)
      stor.yaeco = false

      if (isFin) {
        if (!stor.ok1) {
          stor.zoneConcl1.barre()
          j3pAffiche(stor.lesdiv.concl1Co, null, '$' + stor.rep1attaff + '$')
          stor.lesdiv.concl1Co.style.color = me.styles.toutpetit.correction.color
        }
        if (!stor.ok2) {
          stor.zoneConcl2.barre()
          j3pAffiche(stor.lesdiv.concl2Co, null, '$' + stor.rep2attaff + '$')
          stor.lesdiv.concl2Co.style.color = me.styles.toutpetit.correction.color
        }
        stor.zoneConcl2.disable()
        stor.zoneConcl1.disable()
      }
    }
  }

  function foisRendSelColonnes (exclu) {
    const c = exclu.colonne
    const l = exclu.ligne
    let id1, id2
    if (l === 1) {
      id1 = stor.tabProp[1]
      id2 = stor.tabProp[2]
    } else {
      id1 = stor.tabProp[2]
      id2 = stor.tabProp[1]
    }
    const imax = (stor.nbRepAtt === 6 ? 5 : 4) + stor.ajcolonne
    // les cases de la colonne
    let l1c, l2c
    // on boucle sur les colonnes
    for (let i = 1; i < imax; i++) {
      if (i === c) continue
      l1c = stor.tabProp[1][i]
      l2c = stor.tabProp[2][i]

      l2c.style.cursor = 'pointer'
      l1c.id1 = id1[i]
      l1c.exclu = c
      l1c.casedep = exclu
      l1c.id2 = id2[i]
      l2c.id1 = id1[i]
      l2c.id2 = id2[i]
      l2c.exclu = c
      l2c.casedep = exclu
      l1c.addEventListener('mouseover', foisAllume, false)
      l2c.addEventListener('mouseover', foisAllume, false)
      l1c.addEventListener('mouseout', foisEteint, false)
      l2c.addEventListener('mouseout', foisEteint, false)
      l1c.addEventListener('click', foisSelect, false)
      l2c.addEventListener('click', foisSelect, false)
    }
  }

  function plusRendSel2Colonnes (exclu) {
    const c = exclu.colonne
    const l = exclu.ligne
    const idone = (l === 1) ? stor.tabProp[1] : stor.tabProp[2]
    const idtwo = (l === 1) ? stor.tabProp[2] : stor.tabProp[1]
    const imax = (stor.nbRepAtt === 6 ? 5 : 4) + stor.ajcolonne
    for (let i = 1; i < imax; i++) {
      if (i === c) continue
      const l1c = stor.tabProp[1][i]
      const l2c = stor.tabProp[2][i]
      l1c.style.cursor = 'pointer'
      l2c.style.cursor = 'pointer'
      l1c.id1 = idone[i]
      l1c.exclu = c
      l1c.Lexclu = l
      l1c.casedep = exclu
      l1c.id2 = idtwo[i]
      l2c.id1 = idone[i]
      l2c.id2 = idtwo[i]
      l2c.exclu = c
      l2c.Lexclu = l
      l2c.casedep = exclu
      l1c.addEventListener('mouseover', plusAllume1, false)
      l2c.addEventListener('mouseover', plusAllume1, false)
      l1c.addEventListener('mouseout', plusEteint1, false)
      l2c.addEventListener('mouseout', plusEteint1, false)
      l1c.addEventListener('click', plusSelect1, false)
      l2c.addEventListener('click', plusSelect1, false)
    }
  }

  function moinsRendSel2Colonnes (exclu) {
    const c = exclu.colonne
    const l = exclu.ligne
    const idone = (l === 1) ? stor.tabProp[1] : stor.tabProp[2]
    const idtwo = (l === 1) ? stor.tabProp[2] : stor.tabProp[1]
    const imax = (stor.nbRepAtt === 6 ? 5 : 4) + stor.ajcolonne
    let l1c, l2c
    for (let i = 1; i < imax; i++) {
      if (i === c) continue
      l1c = stor.tabProp[1][i]
      l2c = stor.tabProp[2][i]
      l1c.style.cursor = 'pointer'
      l2c.style.cursor = 'pointer'
      l1c.id1 = idone[i]
      l1c.exclu = c
      l1c.Lexclu = l
      l1c.casedep = exclu
      l1c.id2 = idtwo[i]
      l2c.id1 = idone[i]
      l2c.id2 = idtwo[i]
      l2c.exclu = c
      l2c.Lexclu = l
      l2c.casedep = exclu
      l1c.addEventListener('mouseover', moinsAllume1, false)
      l2c.addEventListener('mouseover', moinsAllume1, false)
      l1c.addEventListener('mouseout', moinsEteint1, false)
      l2c.addEventListener('mouseout', moinsEteint1, false)
      l1c.addEventListener('click', moinsSelect1, false)
      l2c.addEventListener('click', moinsSelect1, false)
    }
  }

  function croixRendsel (exclu) {
    stor.cell = exclu
    const c = exclu.colonne
    const l = exclu.ligne
    if (!stor.ar1use) {
      stor.ar1use = true
      stor.douk = stor.lesdiv.ar1
      stor.gardPla = true
    } else {
      stor.ar2use = true
      stor.douk = stor.lesdiv.ar2
      stor.gardPla = false
    }
    stor.douk.style.padding = '0px 5px 0px 5px'
    stor.douk.style.visibility = 'visible'
    stor.bigDiv5 = j3pAddElt(stor.douk, 'div')
    const tt = afficheFrac(stor.bigDiv5)
    const yy = affichefois(tt[0])
    stor.CroixN1 = yy[0]
    stor.CroixN2 = yy[2]
    stor.CroixN3 = tt[2]
    stor.CroixN1.innerHTML = '&nbsp;?&nbsp;'
    stor.CroixN2.innerHTML = '&nbsp;?&nbsp;'
    stor.CroixN3.innerHTML = '&nbsp;?&nbsp;'
    stor.CroixN1.classList.add('pointinter')
    stor.bigDiv5.style.background = '#aaaaff'
    stor.bufpdt = []
    for (let i = 1; i < stor.tabProp[1].length; i++) {
      if (c !== i || l !== 1) {
        stor.tabProp[1][i].addEventListener('mouseover', uniteAllume, false)
        stor.tabProp[1][i].addEventListener('mouseout', uniteEteint, false)
        stor.tabProp[1][i].addEventListener('click', croixSelect, false)
        stor.tabProp[1][i].style.cursor = 'pointer'
      }
      if (c !== i || l !== 2) {
        stor.tabProp[2][i].addEventListener('mouseover', uniteAllume, false)
        stor.tabProp[2][i].addEventListener('mouseout', uniteEteint, false)
        stor.tabProp[2][i].addEventListener('click', croixSelect, false)
        stor.tabProp[2][i].style.cursor = 'pointer'
      }
    }
  }

  function affCadre (text, trigger, op) {
    stor.cadreinfo = j3pAddElt(me.zonesElts.MG, 'div', {
      style: me.styles.etendre('toutpetit.enonce', { padding: '10px' })
    })
    stor.cadreinfo.style.position = 'absolute'
    stor.cadreinfo.style.left = '50px'
    stor.cadreinfo.style.top = '50px'
    stor.cadreinfohaut = j3pAddElt(stor.cadreinfo, 'div')
    const mil = j3pDiv(stor.cadreinfo, 'cadreinfomil', '')
    j3pAddElt(mil, 'br')
    stor.cadreinfobas = j3pAddElt(stor.cadreinfo, 'div')
    j3pAffiche(stor.cadreinfohaut, null, text)
    stor.cadreinfohaut.style.textAlign = 'center'
    stor.cadreinfobas.style.textAlign = 'center'
    stor.lacell = trigger
    const annuleCb = stor.fcts.annule[op]
    me.boutanull = new BoutonStyleMathquill(stor.cadreinfobas, 'bout', 'Annuler', annuleCb, {}, trigger.id)
    stor.cadreinfo.style.background = '#00FFFF'
    stor.cadreinfo.style.border = '1px solid black'
    pauseAll()
  } // affCadre

  function verifOubli (nbc) {
    let aret = {
      rep: true,
      place: -1
    }
    let nb, numden
    for (let i = 0; i < stor.repEl.length; i++) {
      if (stor.repEl[i].indexOf('frac') === -1) {
        nb = (stor.repEl[i] + '').replace(',', '.')
      } else {
        numden = j3pExtraireNumDen(stor.repEl[i])
        nb = numden[1] / numden[2]
      }
      if (Math.abs((nbc.num / nbc.den) - nb) < Math.pow(10, -10)) {
        if (!aret.rep) {
          stor.ErrDouble = true
          stor.ErrDoublelist.push(i)
        } else {
          aret = {
            rep: false,
            place: i
          }
        }
      }
    }
    if (aret.rep) stor.ErrOublitListe.push(nbc)
    return aret
  }

  function foisAllume () {
    this.id1.style.background = '#ff0'
    this.id2.style.background = '#ff0'
    this.id2.style.position = ''
    this.id1.style.position = ''
    /// ////////////
    /// ///////////
    const posCellule = this.casedep.getBoundingClientRect()
    const pos2 = this.id1.getBoundingClientRect()
    const pos3 = stor.lesdiv.tableau.getBoundingClientRect()

    let mmmax
    let num1
    let num2
    let si2
    let flechdeb
    let si

    if (posCellule.left > pos2.left) { mmmax = posCellule.width } else { mmmax = pos2.width }
    const wwi = (Math.max(posCellule.left, pos2.left) - Math.min(posCellule.left, pos2.left) + mmmax) - 10
    let doudou
    if (this.casedep.ligne === 2) {
      num2 = 20
      num1 = 0
      si2 = 1
      doudou = stor.lesdiv.vide3
    } else {
      num2 = 0
      num1 = 20
      si2 = -1
      doudou = stor.lesdiv.vide2
    }
    if (posCellule.left < pos2.left) {
      flechdeb = 0
      si = 1
    } else {
      flechdeb = wwi
      si = -1
    }

    if (stor.svgBuf) {
      j3pDetruit(stor.svgBuf)
    }
    stor.svgBuf = j3pAddElt(doudou, 'div', {
      style: me.styles.toutpetit.enonce
    })
    doudou.style.paddingLeft = (Math.min(posCellule.left, pos2.left) - pos3.left) + 'px'
    stor.svgBuf.style.width = wwi + 'px'
    stor.svgBuf.style.height = '25px'
    stor.svgBuf.wwi = wwi

    const monSVG = document.createElementNS('http://www.w3.org/2000/svg', 'svg')
    monSVG.style.width = wwi + 'px'
    monSVG.style.height = '25px'
    monSVG.style.left = '0px'
    monSVG.style.top = '0px'

    stor.svgBuf.appendChild(monSVG)

    monSVG.innerHTML += '<path d="M0 ' + num1 + ' Q ' + (wwi / 2) + ' ' + num2 + ', ' + wwi + ' ' + num1 + '  M ' + flechdeb + ' ' + num1 + ' L ' + (flechdeb + si * wwi * 10 / 55) + ' ' + (num1 + si2 * 15) + '  M ' + flechdeb + ' ' + num1 + ' L ' + (flechdeb + si * wwi * 14 / 55) + ' ' + (num1 + si2) + '" stroke-width="3" stroke="#000000" fill="transparent" />'
    stor.monSVG = monSVG
  } // allume
  function plusAllume1 () {
    this.id1.style.background = '#ff0'
    this.id2.style.background = '#ff0'
    this.id2.style.position = ''
    this.id1.style.position = ''
  }

  function plusAllume2 () {
    this.id1.style.background = '#ff0'
    this.id2.style.background = '#ff0'
    this.id2.style.position = ''
    this.id1.style.position = ''
    let num1
    let num2
    let si2
    let mid

    const posCellule = this.casedep.getBoundingClientRect()
    const pos2 = this.id1.getBoundingClientRect()
    const pos3 = stor.lesdiv.tableau.getBoundingClientRect()
    const pos5 = this.mem.getBoundingClientRect()

    if (stor.svgBuf) {
      j3pDetruit(stor.svgBuf)
    }
    const basex = Math.min(posCellule.left, pos2.left, pos5.left)
    const wwi = (Math.max(posCellule.left + posCellule.width, pos2.left + pos2.width, pos5.left + pos5.width) - basex)
    let doudou
    if (this.casedep.ligne === 2) {
      num2 = 20
      num1 = 2
      si2 = 1
      doudou = stor.lesdiv.vide3
    } else {
      num2 = 2
      num1 = 20
      si2 = -1
      doudou = stor.lesdiv.vide2
    }
    if (pos2.left < pos5.left) { mid = (pos5.left + pos5.width - pos2.left) / 2 } else { mid = (pos2.left + pos2.width - pos5.left) / 2 }

    // '<svg viewBox="0 0 '+wwi+' 25" id="mySVG"><path d="M0 20 Q '+(wwi/2)+' 0, '+wwi+' 20  L '+(wwi-wwi/55*10)+' 8 M '+wwi+' 20 L '+(wwi-wwi/55*15)+' 20" stroke-width="3" stroke="black" fill="transparent" /></svg>'
    stor.svgBuf = j3pAddElt(doudou, 'div', {
      style: me.styles.etendre('toutpetit.enonce')
    })
    doudou.style.paddingLeft = (basex - pos3.left) + 'px'

    stor.svgBuf.style.width = wwi + 'px'
    stor.svgBuf.style.height = '25px'
    stor.svgBuf.wwi = wwi

    const monSVG = document.createElementNS('http://www.w3.org/2000/svg', 'svg')
    monSVG.style.width = wwi + 'px'
    monSVG.style.height = '25px'
    monSVG.style.left = '0px'
    monSVG.style.top = '0px'

    stor.svgBuf.appendChild(monSVG)
    monSVG.innerHTML = '<path d="M' + (pos2.left - basex + pos2.width / 2) + ' ' + num1 + ' L ' + mid + ' ' + num2 + 'L ' + (pos5.left - basex + pos5.width / 2) + ' ' + num1 + ' M  ' + mid + ' ' + num2 + ' L ' + (posCellule.left + posCellule.width / 2 - basex) + ' 10 L ' + (posCellule.left + posCellule.width / 2 - basex) + ' ' + num1 + ' L ' + (posCellule.left + posCellule.width / 2 - basex + wwi / 10) + ' ' + (num1 + si2 * 7) + '  M ' + (posCellule.left + posCellule.width / 2 - basex) + ' ' + num1 + ' L ' + (posCellule.left + posCellule.width / 2 - basex - wwi / 10) + ' ' + (num1 + si2 * 7) + '" stroke-width="3" stroke="black"                  fill="transparent" />'
    stor.monSVG = monSVG
  }

  function moinsAllume1 () {
    this.id1.style.background = '#ff0'
    this.id2.style.background = '#ff0'
    this.id2.style.position = ''
    this.id1.style.position = ''
  }

  function moinsAllume2 () {
    this.id1.style.background = '#ff0'
    this.id2.style.background = '#ff0'
    this.id2.style.position = ''
    this.id1.style.position = ''
    let num1
    let num2
    let si2
    let mid

    const posCellule = this.casedep.getBoundingClientRect()
    const pos2 = this.id1.getBoundingClientRect()
    const pos3 = stor.lesdiv.tableau.getBoundingClientRect()
    const pos5 = this.mem.getBoundingClientRect()

    if (stor.svgBuf) {
      j3pDetruit(stor.svgBuf)
    }
    const basex = Math.min(posCellule.left, pos2.left, pos5.left)
    const wwi = (Math.max(posCellule.left + posCellule.width, pos2.left + pos2.width, pos5.left + pos5.width) - basex)
    let doudou
    if (this.casedep.ligne === 2) {
      num2 = 20
      num1 = 2
      si2 = 1
      doudou = stor.lesdiv.vide3
    } else {
      num2 = 2
      num1 = 20
      si2 = -1
      doudou = stor.lesdiv.vide2
    }
    if (pos2.left < pos5.left) { mid = (pos5.left + pos5.width - pos2.left) / 2 } else { mid = (pos2.left + pos2.width - pos5.left) / 2 }

    // '<svg viewBox="0 0 '+wwi+' 25" id="mySVG"><path d="M0 20 Q '+(wwi/2)+' 0, '+wwi+' 20  L '+(wwi-wwi/55*10)+' 8 M '+wwi+' 20 L '+(wwi-wwi/55*15)+' 20" stroke-width="3" stroke="black" fill="transparent" /></svg>'
    stor.svgBuf = j3pAddElt(doudou, 'div', {
      style: me.styles.etendre('toutpetit.enonce')
    })
    doudou.style.paddingLeft = (basex - pos3.left) + 'px'

    stor.svgBuf.style.width = wwi + 'px'
    stor.svgBuf.style.height = '25px'
    stor.svgBuf.wwi = wwi

    const monSVG = document.createElementNS('http://www.w3.org/2000/svg', 'svg')
    monSVG.style.width = wwi + 'px'
    monSVG.style.height = '25px'
    monSVG.style.left = '0px'
    monSVG.style.top = '0px'

    stor.svgBuf.appendChild(monSVG)
    monSVG.innerHTML = '<path d="M' + (pos2.left - basex + pos2.width / 2) + ' ' + num1 + ' L ' + mid + ' ' + num2 + 'L ' + (pos5.left - basex + pos5.width / 2) + ' ' + num1 + ' M  ' + mid + ' ' + num2 + ' L ' + (posCellule.left + posCellule.width / 2 - basex) + ' 10 L ' + (posCellule.left + posCellule.width / 2 - basex) + ' ' + num1 + ' L ' + (posCellule.left + posCellule.width / 2 - basex + wwi / 10) + ' ' + (num1 + si2 * 7) + '  M ' + (posCellule.left + posCellule.width / 2 - basex) + ' ' + num1 + ' L ' + (posCellule.left + posCellule.width / 2 - basex - wwi / 10) + ' ' + (num1 + si2 * 7) + '" stroke-width="3" stroke="black"                  fill="transparent" />'
    stor.monSVG = monSVG
  }

  function uniteAllume () {
    this.style.backgroundColor = '#ff0'
    this.style.position = ''
  }

  function foisEteint () {
    this.id1.style.background = '#fff'
    this.id2.style.background = '#fff'
    if (stor.svgBuf) {
      j3pDetruit(stor.svgBuf)
      stor.svgBuf = undefined
    }
  } // eteint
  function plusEteint1 () {
    this.id1.style.background = '#fff'
    this.id2.style.background = '#fff'
  }

  function plusEteint2 () {
    this.id1.style.background = '#fff'
    this.id2.style.background = '#fff'
    if (stor.svgBuf) {
      j3pDetruit(stor.svgBuf)
      stor.svgBuf = undefined
    }
  }

  function moinsEteint1 () {
    this.id1.style.background = '#fff'
    this.id2.style.background = '#fff'
  }

  function moinsEteint2 () {
    this.id1.style.background = '#fff'
    this.id2.style.background = '#fff'
    if (stor.svgBuf) {
      j3pDetruit(stor.svgBuf)
      stor.svgBuf = undefined
    }
  }

  function uniteEteint () {
    this.style.backgroundColor = '#fff'
  }

  function foisSelect () {
    this.id1.style.background = '#fff'
    this.id2.style.background = '#fff'
    const posCellule = this.casedep.getBoundingClientRect()
    const pos2 = this.id1.getBoundingClientRect()
    const pos3 = stor.mepact.getBoundingClientRect()

    const wwi = (Math.max(posCellule.left, pos2.left) - Math.min(posCellule.left, pos2.left) + posCellule.width)
    const doudou = (this.casedep.ligne === 2) ? stor.lesdiv.vide4 : stor.lesdiv.vide1
    const arriv = (this.casedep.ligne === 2) ? stor.tabProp[1][this.casedep.colonne] : stor.tabProp[2][this.casedep.colonne]
    const op1Div = j3pAddElt(doudou, 'div', {
      style: me.styles.etendre('toutpetit.enonce', { padding: '0px' })
    })
    doudou.style.paddingLeft = (Math.min(posCellule.left, pos2.left) - pos3.left) + 'px'

    const tabOp1 = addDefaultTable(op1Div, 1, 2)
    tabOp1[0][0].style.padding = 0
    tabOp1[0][1].style.padding = 0
    const c = this.exclu
    const imax = (stor.nbRepAtt === 6 ? 5 : 4) + stor.ajcolonne
    for (let i = 1; i < imax; i++) {
      if (i === Number(c)) continue
      const l1c = stor.tabProp[1][i]
      const l2c = stor.tabProp[2][i]
      l1c.style.cursor = ''
      l2c.style.cursor = ''
      l1c.removeEventListener('mouseover', foisAllume, false)
      l2c.removeEventListener('mouseover', foisAllume, false)
      l1c.removeEventListener('mouseout', foisEteint, false)
      l2c.removeEventListener('mouseout', foisEteint, false)
      l1c.removeEventListener('click', foisSelect, false)
      l2c.removeEventListener('click', foisSelect, false)
    }
    j3pDetruit(stor.cadreinfo)

    const coid = getNewIdHere()
    const hhh = new ZoneStyleMathquill2(tabOp1[0][1], { id: 'ope1c' + this.casedep.ligne + 'c' + this.casedep.colonne, restric: stor.restric, hasAutoKeyboard: (stor.restric.indexOf('/') !== -1), inverse: (this.casedep.ligne % 2 === 1), enter: me.sectionCourante.bind(me), limite: 9, onchange: adapteLarg })
    hhh.numres = coid
    let bu = ['+', '-', '×', '÷']
    if (!ds.Piege) bu = ['×', '÷']
    const ttl = ListeDeroulante.create(tabOp1[0][0], bu, {
      choix0: true,
      centre: true,
      boutonAgauche: true,
      onChange: adapteLarg,
      numres: coid
    })
    detruitMenu(stor.menus, this.casedep)
    let uuu
    this.casedep.innerHTML = ''
    if (ds.Completer !== 'Raisonnement') {
      uuu = new ZoneStyleMathquill2(this.casedep, { id: 'ZZope1c' + this.casedep.ligne + 'c' + this.casedep.colonne, restric: stor.restric, hasAutoKeyboard: (stor.restric.indexOf('/') !== -1), inverse: (this.casedep.ligne % 2 === 1), enter: me.sectionCourante.bind(me), limite: 9, onchange: adapteLarg, sansBord: true })
      if (this.casedep.ligne === 1) { stor.inputrep2 = uuu } else { stor.inputrep1 = uuu }
      this.casedep.addEventListener('click', () => {
        setTimeout(() => { uuu.focus() }, 10)
      })
    }

    const monSVG = stor.monSVG
    monSVG.innerHTML += '<line x1="' + (wwi / 4) + '" y1="5" x2="' + (3 * wwi / 4) + '" y2="25" style="stroke:rgb(255,0,0);stroke-width:4" /><line x1="' + (wwi / 4) + '" y1="25" x2="' + (3 * wwi / 4) + '" y2="5" style="stroke:rgb(255,0,0);stroke-width:4" /><rect width="' + wwi + '" height="40" style="fill:rgb(0,0,255);fill-opacity:0;stroke-width:0;stroke:rgb(0,0,0)" />'
    monSVG.childNodes[1].style.visibility = 'hidden'
    monSVG.childNodes[2].style.visibility = 'hidden'
    monSVG.childNodes[3].qui = this.casedep
    monSVG.childNodes[3].coid = coid
    monSVG.childNodes[3].style.cursor = 'pointer'
    monSVG.childNodes[3].addEventListener('mouseover', FlecheClickSupprOver, false)
    monSVG.childNodes[3].addEventListener('mouseout', FlecheClickSupprOut, false)
    monSVG.childNodes[3].addEventListener('click', foisClickSuppr, false)
    resumeAll()
    stor.resultats.push({
      numres: coid,
      color: 'black',
      cellule: this.casedep,
      methode: 'ope1',
      op: hhh,
      valeur: uuu,
      col1: this.id1,
      col12: this.id2,
      casa: arriv,
      liste: ttl,
      svg: stor.svgBuf,
      amoov: doudou,
      cont: op1Div
    })
    stor.svgBuf = undefined
    adapteLarg()
  } // select
  function plusSelect1 () {
    this.id1.style.background = '#fff'
    this.id2.style.background = '#fff'
    const c2 = this.id1.colonne
    const c = this.exclu
    let imax = (stor.nbRepAtt === 6 ? 5 : 4) + stor.ajcolonne
    for (let i = 1; i < imax; i++) {
      if (i === c) continue
      const l1c = stor.tabProp[1][i]
      const l2c = stor.tabProp[2][i]
      l1c.style.cursor = ''
      l2c.style.cursor = ''
      l1c.removeEventListener('mouseover', plusAllume1, false)
      l2c.removeEventListener('mouseover', plusAllume1, false)
      l1c.removeEventListener('mouseout', plusEteint1, false)
      l2c.removeEventListener('mouseout', plusEteint1, false)
      l1c.removeEventListener('click', plusSelect1, false)
      l2c.removeEventListener('click', plusSelect1, false)
    }
    stor.cadreinfohaut.innerHTML = ''
    j3pAffiche(stor.cadreinfohaut, null, 'Choisis la deuxième colonne')
    stor.cadreinfobas.innerHTML = ''
    stor.boutanull = new BoutonStyleMathquill(stor.cadreinfobas, 'bout', 'Annuler', annulePlus2, {}, this.casedep)

    const l = this.Lexclu
    const idone = (l === 1) ? stor.tabProp[1] : stor.tabProp[2]
    const idtwo = (l === 1) ? stor.tabProp[2] : stor.tabProp[1]
    imax = (stor.nbRepAtt === 6 ? 5 : 4) + stor.ajcolonne
    for (let i = 1; i < imax; i++) {
      if (i === c || i === c2) continue
      const l1c = stor.tabProp[1][i]
      const l2c = stor.tabProp[2][i]
      l1c.style.cursor = 'pointer'
      l2c.style.cursor = 'pointer'
      l1c.id1 = idone[i]
      l1c.mem = this.id1
      l1c.mem2 = this.id2
      l1c.id2 = idtwo[i]
      l2c.id1 = idone[i]
      l2c.id2 = idtwo[i]
      l2c.mem = this.id1
      l2c.mem2 = this.id2
      l1c.addEventListener('mouseover', plusAllume2, false)
      l2c.addEventListener('mouseover', plusAllume2, false)
      l1c.addEventListener('mouseout', plusEteint2, false)
      l2c.addEventListener('mouseout', plusEteint2, false)
      l1c.addEventListener('click', plusSelect2, false)
      l2c.addEventListener('click', plusSelect2, false)
    }
  }

  function moinsSelect1 () {
    this.id1.style.background = '#fff'
    this.id2.style.background = '#fff'
    const c2 = this.id1.colonne
    const c = this.exclu
    let imax = (stor.nbRepAtt === 6 ? 5 : 4) + stor.ajcolonne
    for (let i = 1; i < imax; i++) {
      if (i === c) continue
      const l1c = stor.tabProp[1][i]
      const l2c = stor.tabProp[2][i]
      l1c.style.cursor = ''
      l2c.style.cursor = ''
      l1c.removeEventListener('mouseover', moinsAllume1, false)
      l2c.removeEventListener('mouseover', moinsAllume1, false)
      l1c.removeEventListener('mouseout', moinsEteint1, false)
      l2c.removeEventListener('mouseout', moinsEteint1, false)
      l1c.removeEventListener('click', moinsSelect1, false)
      l2c.removeEventListener('click', moinsSelect1, false)
    }
    stor.cadreinfohaut.innerHTML = ''
    j3pAffiche(stor.cadreinfohaut, null, 'Choisis la deuxième colonne')
    stor.cadreinfobas.innerHTML = ''
    stor.boutanull = new BoutonStyleMathquill(stor.cadreinfobas, 'bout', 'Annuler', annuleMoins2, {}, this.casedep)

    const l = this.Lexclu
    const idone = (l === 1) ? stor.tabProp[1] : stor.tabProp[2]
    const idtwo = (l === 1) ? stor.tabProp[2] : stor.tabProp[1]
    imax = (stor.nbRepAtt === 6 ? 5 : 4) + stor.ajcolonne
    for (let i = 1; i < imax; i++) {
      if (i === c || i === c2) continue
      const l1c = stor.tabProp[1][i]
      const l2c = stor.tabProp[2][i]
      l1c.style.cursor = 'pointer'
      l2c.style.cursor = 'pointer'
      l1c.id1 = idone[i]
      l1c.mem = this.id1
      l1c.mem2 = this.id2
      l1c.id2 = idtwo[i]
      l2c.id1 = idone[i]
      l2c.id2 = idtwo[i]
      l2c.mem = this.id1
      l2c.mem2 = this.id2
      l1c.addEventListener('mouseover', moinsAllume2, false)
      l2c.addEventListener('mouseover', moinsAllume2, false)
      l1c.addEventListener('mouseout', moinsEteint2, false)
      l2c.addEventListener('mouseout', moinsEteint2, false)
      l1c.addEventListener('click', moinsSelect2, false)
      l2c.addEventListener('click', moinsSelect2, false)
    }
  }

  function plusSelect2 () {
    this.id1.style.background = '#fff'
    this.id2.style.background = '#fff'

    const posCellule = this.casedep.getBoundingClientRect()
    const pos2 = this.id1.getBoundingClientRect()
    const pos5 = this.mem.getBoundingClientRect()

    const basex = Math.min(posCellule.left, pos2.left, pos5.left)

    const wwi = (Math.max(posCellule.left + posCellule.width, pos2.left + pos2.width, pos5.left + pos5.width) - basex)

    const doudou = (this.casedep.ligne === 1) ? stor.lesdiv.vide1 : stor.lesdiv.vide4
    const doudou2 = (this.casedep.ligne === 1) ? stor.lesdiv.vide2 : stor.lesdiv.vide3
    const ope2S = j3pAddElt(doudou, 'div', {
      style: me.styles.etendre('toutpetit.enonce', { padding: '0px' })
    })
    ope2S.innerHTML = '+'
    const c2 = this.mem.colonne
    const c = this.exclu.colonne
    const imax = (stor.nbRepAtt === 6 ? 5 : 4) + stor.ajcolonne
    let l1c, l2c
    for (let i = 1; i < imax; i++) {
      if (i === c || i === c2) continue
      l1c = stor.tabProp[1][i]
      l2c = stor.tabProp[2][i]
      l1c.removeEventListener('mouseover', plusAllume2, false)
      l2c.removeEventListener('mouseover', plusAllume2, false)
      l1c.removeEventListener('mouseout', plusEteint2, false)
      l2c.removeEventListener('mouseout', plusEteint2, false)
      l1c.removeEventListener('click', plusSelect2, false)
      l2c.removeEventListener('click', plusSelect2, false)
    }

    if (!stor.cadreinfo) return
    j3pDetruit(stor.cadreinfo)
    detruitMenu(stor.menus, this.casedep)
    this.casedep.innerHTML = ''

    let uuu
    const coid = getNewIdHere()
    if (ds.Completer !== 'Raisonnement') {
      uuu = new ZoneStyleMathquill2(this.casedep, { id: 'ZZope2c' + this.casedep.ligne + 'c' + this.casedep.colonne, restric: stor.restric, hasAutoKeyboard: (stor.restric.indexOf('/') !== -1), inverse: (this.casedep.ligne % 2 === 1), enter: me.sectionCourante.bind(me), limite: 9, onchange: adapteLarg, sansBord: true })
      if (this.casedep.ligne === 1) { stor.inputrep2 = uuu } else { stor.inputrep1 = uuu }
      this.casedep.addEventListener('click', () => {
        setTimeout(() => { uuu.focus() }, 10)
      })
    }

    stor.monSVG.innerHTML += '<line x1="' + (wwi / 4) + '" y1="5" x2="' + (3 * wwi / 4) + '" y2="25" style="stroke:rgb(255,0,0);stroke-width:4" /><line x1="' + (wwi / 4) + '" y1="25" x2="' + (3 * wwi / 4) + '" y2="5" style="stroke:rgb(255,0,0);stroke-width:4" /><rect width="' + wwi + '" height="40" style="fill:rgb(0,0,255);fill-opacity:0;stroke-width:0;stroke:rgb(0,0,0)" />'
    stor.monSVG.childNodes[1].style.visibility = 'hidden'
    stor.monSVG.childNodes[2].style.visibility = 'hidden'

    stor.monSVG.childNodes[3].qui = this.casedep
    stor.monSVG.childNodes[3].style.cursor = 'pointer'
    stor.monSVG.childNodes[3].addEventListener('mouseover', FlecheClickSupprOver, false)
    stor.monSVG.childNodes[3].addEventListener('mouseout', FlecheClickSupprOut, false)
    stor.monSVG.childNodes[3].addEventListener('click', plusClick2Suppr, false)
    stor.resultats.push({
      numres: coid,
      color: 'black',
      cellule: this.casedep,
      methode: 'ope2',
      op: doudou,
      valeur: uuu,
      col1: this.id1,
      col12: this.id2,
      col2: this.mem,
      col22: this.mem2,
      svg: stor.svgBuf,
      doudou2,
      hum: '+',
      wherop: ope2S
    })
    stor.svgBuf = undefined
    resumeAll()
    adapteLarg()
  }

  function moinsSelect2 () {
    this.id1.style.background = '#fff'
    this.id2.style.background = '#fff'

    const posCellule = this.casedep.getBoundingClientRect()
    const pos2 = this.id1.getBoundingClientRect()
    const pos5 = this.mem.getBoundingClientRect()

    const basex = Math.min(posCellule.left, pos2.left, pos5.left)

    const wwi = (Math.max(posCellule.left + posCellule.width, pos2.left + pos2.width, pos5.left + pos5.width) - basex)

    let doudou
    let doudou2
    if (this.casedep.ligne === 1) {
      doudou = stor.lesdiv.vide1
      doudou2 = stor.lesdiv.vide2
    } else {
      doudou = stor.lesdiv.vide4
      doudou2 = stor.lesdiv.vide3
    }
    const ope2S = j3pAddElt(doudou, 'div', {
      style: me.styles.etendre('toutpetit.enonce', { padding: '0px' })
    })
    ope2S.innerHTML = '-'
    const c2 = this.mem.colonne
    const c = this.exclu.colonne
    const imax = (stor.nbRepAtt === 6 ? 5 : 4) + stor.ajcolonne
    let l1c, l2c
    for (let i = 1; i < imax; i++) {
      if (i === c || i === c2) continue
      l1c = stor.tabProp[1][i]
      l2c = stor.tabProp[2][i]
      l1c.removeEventListener('mouseover', moinsAllume2, false)
      l2c.removeEventListener('mouseover', moinsAllume2, false)
      l1c.removeEventListener('mouseout', moinsEteint2, false)
      l2c.removeEventListener('mouseout', moinsEteint2, false)
      l1c.removeEventListener('click', moinsSelect2, false)
      l2c.removeEventListener('click', moinsSelect2, false)
    }

    if (!stor.cadreinfo) return
    j3pDetruit(stor.cadreinfo)
    detruitMenu(stor.menus, this.casedep)
    this.casedep.innerHTML = ''

    let uuu
    const coid = getNewIdHere()
    if (ds.Completer !== 'Raisonnement') {
      uuu = new ZoneStyleMathquill2(this.casedep, { id: 'ZZope2c' + this.casedep.ligne + 'c' + this.casedep.colonne, restric: stor.restric, hasAutoKeyboard: (stor.restric.indexOf('/') !== -1), inverse: (this.casedep.ligne % 2 === 1), enter: me.sectionCourante.bind(me), limite: 9, onchange: adapteLarg, sansBord: true })
      if (this.casedep.ligne === 1) { stor.inputrep2 = uuu } else { stor.inputrep1 = uuu }
      this.casedep.addEventListener('click', () => {
        setTimeout(() => { uuu.focus() }, 10)
      })
    }

    stor.monSVG.innerHTML += '<line x1="' + (wwi / 4) + '" y1="5" x2="' + (3 * wwi / 4) + '" y2="25" style="stroke:rgb(255,0,0);stroke-width:4" /><line x1="' + (wwi / 4) + '" y1="25" x2="' + (3 * wwi / 4) + '" y2="5" style="stroke:rgb(255,0,0);stroke-width:4" /><rect width="' + wwi + '" height="40" style="fill:rgb(0,0,255);fill-opacity:0;stroke-width:0;stroke:rgb(0,0,0)" />'
    stor.monSVG.childNodes[1].style.visibility = 'hidden'
    stor.monSVG.childNodes[2].style.visibility = 'hidden'

    stor.monSVG.childNodes[3].qui = this.casedep
    stor.monSVG.childNodes[3].style.cursor = 'pointer'
    stor.monSVG.childNodes[3].addEventListener('mouseover', FlecheClickSupprOver, false)
    stor.monSVG.childNodes[3].addEventListener('mouseout', FlecheClickSupprOut, false)
    stor.monSVG.childNodes[3].addEventListener('click', plusClick2Suppr, false)
    stor.resultats.push({
      numres: coid,
      color: 'black',
      cellule: this.casedep,
      methode: 'ope2moins',
      op: doudou,
      valeur: uuu,
      col1: this.id1,
      col12: this.id2,
      col2: this.mem,
      col22: this.mem2,
      svg: stor.svgBuf,
      doudou2,
      hum: '-',
      wherop: ope2S
    })
    stor.svgBuf = undefined
    resumeAll()
    adapteLarg()
  }

  function croixSelect () {
    this.style.background = '#aaa'
    this.removeEventListener('mouseover', uniteAllume, false)
    this.removeEventListener('mouseout', uniteEteint, false)
    this.removeEventListener('click', croixSelect, false)
    stor.bufpdt.push(this)
    let bub
    switch (stor.bufpdt.length) {
      case 1:
        stor.CroixN1.classList.remove('pointinter')
        stor.CroixN2.classList.add('pointinter')
        // FIXMEok valeurDe veut un array et on lui passe une string
        // en fait ca travaille bien avec un string (c’est l’id d’une case)
        bub = valeurDe(this)
        j3pEmpty(stor.CroixN1)
        j3pAffiche(stor.CroixN1, null, '$' + calcul(stor.decim, bub, null, 'i') + '$')
        break
      case 2:
        stor.CroixN2.classList.remove('pointinter')
        stor.CroixN3.classList.add('pointinter')
        // FIXMEok valeurDe veut un array et on lui passe une string
        // en fait ca travaille bien avec un string (c’est l’id d’une case)
        bub = valeurDe(this)
        j3pEmpty(stor.CroixN2)
        j3pAffiche(stor.CroixN2, null, '$' + calcul(stor.decim, bub, null, 'i') + '$')
        break
      case 3: {
        j3pDetruit(stor.cadreinfo)
        stor.CroixN3.classList.remove('pointinter')
        // FIXMEok valeurDe veut un array et on lui passe une string
        // en fait ca travaille bien avec un string (c’est l’id d’une case)
        bub = valeurDe(this)
        j3pEmpty(stor.CroixN3)
        j3pAffiche(stor.CroixN3, null, '$' + calcul(stor.decim, bub, null, 'i') + '$')

        for (let i = 1; i < stor.tabProp[1].length; i++) {
          stor.tabProp[1][i].removeEventListener('mouseover', uniteAllume, false)
          stor.tabProp[1][i].removeEventListener('mouseout', uniteEteint, false)
          stor.tabProp[1][i].removeEventListener('click', croixSelect, false)
          stor.tabProp[1][i].style.cursor = ''
          stor.tabProp[2][i].removeEventListener('mouseover', uniteAllume, false)
          stor.tabProp[2][i].removeEventListener('mouseout', uniteEteint, false)
          stor.tabProp[2][i].removeEventListener('click', croixSelect, false)
          stor.tabProp[2][i].style.cursor = ''
        }

        let uuu
        if (ds.Completer !== 'Raisonnement') {
          j3pEmpty(stor.cell)
          uuu = new ZoneStyleMathquill2(stor.cell, { id: 'ZZgfope2c' + stor.cell.ligne + 'c' + stor.cell.colonne, restric: stor.restric, hasAutoKeyboard: (stor.restric.indexOf('/') !== -1), inverse: (stor.cell.ligne % 2 === 1), enter: me.sectionCourante.bind(me), limite: 9, onchange: adapteLarg, sansBord: true })
          if (stor.cell.ligne === 1) {
            stor.inputrep2 = uuu
          } else {
            stor.inputrep1 = uuu
          }
          stor.cell.addEventListener('click', () => {
            setTimeout(() => { uuu.focus() }, 10)
          })
        }
        stor.douk.style.visibility = 'hidden'
        for (let i = 0; i < stor.bufpdt.length; i++) {
          stor.bufpdt[i].style.background = '#fff'
        }

        stor.croixListe.push(stor.cell)
        stor.listepdt.push({
          this: stor.cell,
          ou: stor.cell.getBoundingClientRect()
        })
        stor.cell.tabope5 = []
        for (let i = 0; i < stor.bufpdt.length; i++) {
          stor.cell.tabope5[i] = stor.bufpdt[i]
        }
        const coid = getNewIdHere()
        stor.resultats.push({
          numres: coid,
          cellule: stor.cell,
          methode: 'ope5',
          cel1: stor.bufpdt[0],
          cel2: stor.bufpdt[1],
          cel3: stor.bufpdt[2],
          c1: stor.CroixN1,
          c2: stor.CroixN2,
          c3: stor.CroixN3,
          valeur: uuu,
          pla: this.pla,
          color: 'black',
          div5: stor.bigDiv5,
          ar1: stor.gardPla
        })
        stor.cell.laCase = stor.bigDiv5
        detruitMenu(stor.menus, stor.cell)
        resumeAll()
        if (!stor.yapdt) {
          j3pAjouteBouton(stor.lesdiv.supCro, croixSupopop, { className: 'MepBouton', value: 'Supprimer un produit en croix' })
        }
        stor.yapdt = true
      }
        break
    }
    adapteLarg()
  }

  function uniteSelect (h) {
    h.style.background = '#fff'
    h.innerHTML = ''
    h.id2.innerHTML = ''
    h.innerHTML = ''
    h.style.width = ''
    h.id2.style.width = ''
    detruitMenu(stor.menus, h.qui)

    resumeAll()

    const posCellule = h.qui.getBoundingClientRect()
    const pos2 = h.getBoundingClientRect()
    const pos3 = stor.lesdiv.tableau.getBoundingClientRect()

    let doudou
    let doudou2

    if (h.qui.ligne === 2) {
      doudou = stor.lesdiv.vide4
      doudou2 = stor.lesdiv.vide3
    } else {
      doudou = stor.lesdiv.vide1
      doudou2 = stor.lesdiv.vide2
    }

    const coid = getNewIdHere()
    if (h.ligne === 2) {
      stor.placeNb.push('unit' + coid)
      stor.repEl.push('?')
      stor.placeNb.push('uni' + coid)
      stor.repEl.push('1')
    } else {
      stor.placeNb.push('uni' + coid)
      stor.repEl.push('1')
      stor.placeNb.push('unit' + coid)
      stor.repEl.push('?')
    }
    const divu = j3pAddElt(doudou, 'div', {
      style: me.styles.toutpetit.enonce
    })
    doudou.style.paddingLeft = (Math.min(posCellule.left, pos2.left) - pos3.left) + 'px'
    const pla = (h.ligne - 1) + 2 * (h.qui.colonne - 1)

    const buf2 = trans(stor.repEl[pla])

    const bub = calcul(stor.decim, buf2, 0, 'i')
    j3pAffiche(divu, null, '$ \\times ' + bub + '$')
    let ttt
    if (ds.Completer !== 'Raisonnement') {
      j3pEmpty(h.qui)
      ttt = new ZoneStyleMathquill2(h.qui, { id: 'ZZopegf2c' + h.qui.ligne + 'c' + h.qui.colonne, restric: stor.restric, hasAutoKeyboard: (stor.restric.indexOf('/') !== -1), inverse: (h.qui.ligne % 2 === 1), enter: me.sectionCourante.bind(me), limite: 9, sansBord: true })
      if (h.qui.ligne === 1) { stor.inputrep2 = ttt } else { stor.inputrep1 = ttt }
      h.qui.addEventListener('click', () => {
        setTimeout(() => { ttt.focus() }, 10)
      })
    } else {
      h.qui.innerHTML = ''
      j3pAffiche(h.qui, null, '?')
    }

    j3pAffiche(h, null, ' $1$ ')
    const uuu = new ZoneStyleMathquill2(h.id2, { id: 'Colgfgfffkdo' + h.qui.colonne, restric: stor.restric, hasAutoKeyboard: (stor.restric.indexOf('/') !== -1), inverse: (h.qui.ligne % 2 === 1), enter: me.sectionCourante.bind(me), limite: 9, sansBord: true })
    h.id2.addEventListener('click', () => {
      setTimeout(() => { uuu.focus() }, 10)
    })
    stor.resultats.push({
      color: 'black',
      numres: coid,
      multi: bub,
      cellule: h.qui,
      methode: 'ope3',
      valeurunit: uuu,
      valeur: ttt,
      caseu: h,
      caseu2: h.id2,
      doudou1: doudou,
      doudou2,
      ddiv: divu
    })
    if (ttt) ttt.onchange = adapteLarg
    uuu.onchange = adapteLarg
    adapteLarg()
  } // uniteSelect

  function FlecheClickSupprOut () {
    if (stor.isPaused) return
    this.qui1.style.visibility = 'hidden'
    this.qui2.style.visibility = 'hidden'
  }

  function FlecheClickSupprOver () {
    if (stor.isPaused) return
    this.qui1.style.visibility = ''
    this.qui2.style.visibility = ''
  }

  function foisClickSuppr () {
    if (me.etat !== 'correction') return
    if (stor.isPaused) return
    j3pDetruit(this.svg)
    this.liste.disable()
    this.ope1.onchange = undefined
    this.ope1.disable()
    j3pEmpty(this.amoov)
    // on ajoute le menu
    addMenu(this.qui)

    let resultat
    for (let i = 0; i < stor.resultats.length; i++) {
      resultat = stor.resultats[i]
      if (resultat.cellule === this.qui) {
        if (resultat.valeur !== undefined && typeof resultat.valeur !== 'string') {
          resultat.valeur.onchange = undefined
          resultat.valeur.disable()
        }
        const jim = resultat.cellule
        const pla = (jim.ligne - 1) + 2 * (jim.colonne - 1)
        stor.repEl[pla] = '?'
        stor.resultats.splice(i, 1)
      }
    }
    j3pEmpty(this.qui)
    j3pAffiche(this.qui, null, '<b>?</b>')
    adapteLarg()
  }

  function plusClick2Suppr () {
    if (me.etat !== 'correction') return
    if (stor.isPaused) return
    j3pDetruit(this.svg)
    j3pEmpty(this.op)

    if (ds.Completer !== 'Raisonnement') {
      this.ope2.onchange = undefined
      this.ope2.disable()
    }
    this.qui.innerHTML = '<b>?</b>'

    addMenu(this.qui)
    let resultat
    for (let i = 0; i < stor.resultats.length; i++) {
      resultat = stor.resultats[i]
      if (resultat.cellule === this.qui) {
        const jim = resultat.cellule
        const pla = (jim.ligne - 1) + 2 * (jim.colonne - 1)
        stor.repEl[pla] = '?'
        stor.resultats.splice(i, 1)
      }
    }
    adapteLarg()
  }

  function uniteClickSuppr () {
    if (stor.isPaused) return
    let resultat, i
    // determine si ya une operation apres
    let ret1, ret2
    let foprevsort = false
    for (i = 0; i < stor.resultats.length; i++) {
      resultat = stor.resultats[i]
      if (resultat.cellule === this.qui) {
        ret1 = resultat.caseu
        ret2 = resultat.caseu2
        break
      }
    }
    if (i === 0 && stor.resultats.length === 2 && stor.resultats[1].methode === 'ope3') {
      foprevsort = true
    }
    for (i = 0; i < stor.resultats.length; i++) {
      const resultatb = stor.resultats[i]
      if (resultatb.cellule !== this.qui) {
        if ((resultatb.col1 === ret1) ||
          (resultatb.col1 === ret2) ||
          (resultatb.col2 === ret2) ||
          (resultatb.col2 === ret1) ||
          (resultatb.cel1 === ret1) ||
          (resultatb.cel2 === ret1) ||
          (resultatb.cel3 === ret1) ||
          (resultatb.cel1 === ret2) ||
          (resultatb.cel2 === ret2) ||
          (resultatb.cel3 === ret2)
        ) {
          foprevsort = true
        }
      }
    }
    if (foprevsort) {
      affModale('Je ne peux pas supprimer cette méthode ,<br> une autre méthode utilise la case... <br> <i>Supprime l’autre méthode d’abord. </i>')
      return
    }

    if (ds.Completer !== 'Raisonnement') {
      if (this.valeur) this.valeur.disable()
    }
    if (this.valeurunit) this.valeurunit.disable()

    try {
      stor.tabProp[1].splice(resultat.caseu.colSup, 1)
    } catch (error) {
      j3pNotify(error, { resultat, liste: stor.resultats })
    }
    stor.tabProp[2].splice(resultat.caseu.colSup, 1)
    stor.tabProp[0][0].setAttribute('colspan', stor.tabProp[1].length + 1)
    stor.tabProp[3][0].setAttribute('colspan', stor.tabProp[1].length + 1)

    j3pDetruit(resultat.caseu)
    j3pDetruit(resultat.caseu2)
    j3pDetruit(this.ddiv)
    j3pDetruit(this.svg)

    this.qui.innerHTML = '<b>?</b>'

    addMenu(this.qui)
    let coid
    for (let i = 0; i < stor.resultats.length; i++) {
      resultat = stor.resultats[i]
      if (resultat.cellule === this.qui) {
        const jim = resultat.cellule
        const pla = (jim.ligne - 1) + 2 * (jim.colonne - 1)
        stor.repEl[pla] = '?'
        stor.resultats.splice(i, 1)
        coid = resultat.numres
      }
    }
    for (let i = 0; i < stor.placeNb.length; i++) {
      if ((stor.placeNb[i] === 'uni' + coid) || (stor.placeNb[i] === 'unit' + coid)) {
        stor.placeNb.splice(i, 1)
        stor.repEl.splice(i, 1)
        i--
      }
    }
    stor.ajcolonne--
    adapteLarg()
  }

  function annuleCoef () {
    if (stor.isPaused) return
    let resultat
    stor.zoneCoef.disable()
    j3pDetruit(stor.divCoef2)
    j3pDetruit(stor.rectop4)
    stor.yacoef = false
    stor.lesdiv.ar0.style.width = '0px'
    for (let i = stor.resultats.length - 1; i > -1; i--) {
      resultat = stor.resultats[i]
      if (resultat.methode === 'ope4') {
        if (ds.Completer !== 'Raisonnement') {
          if (resultat.valeur) resultat.valeur.disable()
        }
        resultat.cellule.innerHTML = '<b>?</b>'
        const jim = resultat.cellule
        addMenu(jim)
        const pla = (jim.ligne - 1) + 2 * (jim.colonne - 1)
        stor.repEl[pla] = '?'
        stor.resultats.splice(i, 1)
      }
    }
    adapteLarg()
  }

  /*
   * Les fcts qui annulent l’opération proposée après clic sur un élément de menu
   * - ferme le cadre de affcadre
   * - remet les styles et les écouteurs qui vont bien
   */

  function annuleop1col () {
    const exclu = stor.lacell
    const c = exclu.colonne
    const imax = (stor.nbRepAtt === 6 ? 5 : 4) + stor.ajcolonne
    let l1c, l2c
    for (let i = 1; i < imax; i++) {
      if (i === c) continue
      l1c = stor.tabProp[1][i]
      l2c = stor.tabProp[2][i]
      l1c.style.cursor = ''
      l2c.style.cursor = ''
      l1c.removeEventListener('mouseover', foisAllume, false)
      l2c.removeEventListener('mouseover', foisAllume, false)
      l1c.removeEventListener('mouseout', foisEteint, false)
      l2c.removeEventListener('mouseout', foisEteint, false)
      l1c.removeEventListener('click', foisSelect, false)
      l2c.removeEventListener('click', foisSelect, false)
    }
    j3pDetruit(stor.cadreinfo)
    j3pDetruit(stor.svgBuf)
    stor.svgBuf = undefined
    resumeAll()
  }

  function annuleaj2col () {
    const exclu = stor.lacell
    const c = exclu.colonne
    const imax = (stor.nbRepAtt === 6 ? 5 : 4) + stor.ajcolonne
    let l1c, l2c
    for (let i = 1; i < imax; i++) {
      if (i === c) continue
      l1c = stor.tabProp[1][i]
      l2c = stor.tabProp[2][i]
      l1c.style.cursor = ''
      l2c.style.cursor = ''
      l1c.removeEventListener('mouseover', plusAllume1, false)
      l2c.removeEventListener('mouseover', plusAllume1, false)
      l1c.removeEventListener('mouseout', plusEteint1, false)
      l2c.removeEventListener('mouseout', plusEteint1, false)
      l1c.removeEventListener('click', plusSelect1, false)
      l2c.removeEventListener('click', plusSelect1, false)
    }
    j3pDetruit(stor.cadreinfo)
    resumeAll()
    if (stor.svgBuf) {
      j3pDetruit(stor.svgBuf)
      stor.svgBuf = undefined
    }
  }

  function annulepassageUnite () {
    j3pDetruit(stor.listunite.k)
    j3pDetruit(stor.listunite.k2)
    j3pDetruit(stor.cadreinfo)
    resumeAll()
    stor.ajcolonne--
  }

  function annuleprodCroix () {
    for (let j = 1; j < stor.tabProp[1].length; j++) {
      for (let i = 1; i < 3; i++) {
        stor.tabProp[i][j].style.cursor = ''
        stor.tabProp[i][j].style.background = '#fff'
        stor.tabProp[i][j].removeEventListener('mouseover', uniteAllume, false)
        stor.tabProp[i][j].removeEventListener('mouseout', uniteEteint, false)
        stor.tabProp[i][j].removeEventListener('click', croixSelect, false)
      }
    }
    if (stor.gardPla) { stor.ar1use = false } else { stor.ar2use = false }
    j3pDetruit(stor.bigDiv5)
    stor.bufpdt = []
    j3pDetruit(stor.cadreinfo)
    resumeAll()
  }

  function annulePlus2 () {
    const exclu = stor.lacell
    const c = exclu.colonne
    const imax = (stor.nbRepAtt === 6 ? 5 : 4) + stor.ajcolonne
    let l1c, l2c
    for (let i = 1; i < imax; i++) {
      if (i === c) continue
      l1c = stor.tabProp[1][i]
      l2c = stor.tabProp[2][i]
      l1c.style.cursor = ''
      l2c.style.cursor = ''
      l1c.removeEventListener('mouseover', plusAllume2, false)
      l2c.removeEventListener('mouseover', plusAllume2, false)
      l1c.removeEventListener('mouseout', plusEteint2, false)
      l2c.removeEventListener('mouseout', plusEteint2, false)
      l1c.removeEventListener('click', plusSelect2, false)
      l2c.removeEventListener('click', plusSelect2, false)
    }
    j3pDetruit(stor.cadreinfo)
    resumeAll()
    if (stor.svgBuf) {
      j3pDetruit(stor.svgBuf)
      stor.svgBuf = undefined
    }
  }

  function annulesub2col () {
    const exclu = stor.lacell
    const c = exclu.colonne
    const imax = (stor.nbRepAtt === 6 ? 5 : 4) + stor.ajcolonne
    let l1c, l2c
    for (let i = 1; i < imax; i++) {
      if (i === c) continue
      l1c = stor.tabProp[1][i]
      l2c = stor.tabProp[2][i]
      l1c.style.cursor = ''
      l2c.style.cursor = ''
      l1c.removeEventListener('mouseover', moinsAllume1, false)
      l2c.removeEventListener('mouseover', moinsAllume1, false)
      l1c.removeEventListener('mouseout', moinsEteint1, false)
      l2c.removeEventListener('mouseout', moinsEteint1, false)
      l1c.removeEventListener('click', moinsSelect1, false)
      l2c.removeEventListener('click', moinsSelect1, false)
    }
    j3pDetruit(stor.cadreinfo)
    resumeAll()
    if (stor.svgBuf) {
      j3pDetruit(stor.svgBuf)
      stor.svgBuf = undefined
    }
  }

  function annuleMoins2 () {
    const exclu = stor.lacell
    const c = exclu.colonne
    const imax = (stor.nbRepAtt === 6 ? 5 : 4) + stor.ajcolonne
    let l1c, l2c
    for (let i = 1; i < imax; i++) {
      if (i === c) continue
      l1c = stor.tabProp[1][i]
      l2c = stor.tabProp[2][i]
      l1c.style.cursor = ''
      l2c.style.cursor = ''
      l1c.removeEventListener('mouseover', moinsAllume2, false)
      l2c.removeEventListener('mouseover', moinsAllume2, false)
      l1c.removeEventListener('mouseout', moinsEteint2, false)
      l2c.removeEventListener('mouseout', moinsEteint2, false)
      l1c.removeEventListener('click', moinsSelect2, false)
      l2c.removeEventListener('click', moinsSelect2, false)
    }
    j3pDetruit(stor.cadreinfo)
    resumeAll()
    if (stor.svgBuf) {
      j3pDetruit(stor.svgBuf)
      stor.svgBuf = undefined
    }
  }

  function killcroixKill () {
    let resultat
    j3pDetruit(stor.lemasque)
    stor.lemasque = undefined
    for (let j = 0; j < stor.timasque.length; j++) {
      j3pDetruit(stor.timasque[j])
    }
    const listi = []
    for (let i = 0; i < stor.resultats.length; i++) {
      resultat = stor.resultats[i]
      if (resultat.methode === 'ope5') listi.push(i)
    }
    if (!listi.length) {
      j3pDetruit(stor.lesdiv.supCro)
      stor.yapdt = false
    }
  }

  function croixKill () {
    let resultat
    this.donnees.cellule.style.background = '#fff'
    this.donnees.cel1.style.background = '#fff'
    this.donnees.cel2.style.background = '#fff'
    this.donnees.cel3.style.background = '#fff'
    for (let i = stor.resultats.length - 1; i > -1; i--) {
      resultat = stor.resultats[i]
      if (resultat.methode === 'ope5') {
        if (resultat.cellule === this.donnees.cellule) {
          const jim = resultat.cellule
          const pla = (jim.ligne - 1) + 2 * (jim.colonne - 1)
          stor.repEl[pla] = '?'
          stor.resultats.splice(i, 1)
          break
        }
      }
    }
    if (resultat === undefined) {
      killcroixKill()
      return
    }
    j3pDetruit(resultat.div5)
    j3pDetruit(resultat.svg)
    if (ds.Completer !== 'Raisonnement') {
      if (resultat.valeur) resultat.valeur.disable()
    }
    for (let i = 0; i < stor.croixListe.length; i++) {
      if (stor.croixListe[i] === this.donnees.cellule) {
        stor.croixListe.splice(i, 1)
      }
    }

    // vire linput si ya
    // libere ar1 ou ar2
    if (resultat.ar1) { stor.ar1use = false } else { stor.ar2use = false }
    this.donnees.cellule.innerHTML = '<b>?</b>'
    addMenu(this.donnees.cellule) // ??? this.qui ou don.donnees.cellule ?
    killcroixKill()
  }

  function croixKillAllume () {
    this.qui1.style.visibility = 'visible'
    this.qui2.style.visibility = 'visible'
    this.donnees.cellule.style.background = '#aaa'
    this.donnees.cel1.style.background = '#aaa'
    this.donnees.cel2.style.background = '#aaa'
    this.donnees.cel3.style.background = '#aaa'
  }

  function croixKillEteint () {
    this.qui1.style.visibility = 'hidden'
    this.qui2.style.visibility = 'hidden'
    this.donnees.cellule.style.background = '#fff'
    this.donnees.cel1.style.background = '#fff'
    this.donnees.cel2.style.background = '#fff'
    this.donnees.cel3.style.background = '#fff'
  }

  function croixSupopop () {
    if (stor.isPaused) return
    const listi = []
    const posi = []
    const doni = []
    for (let i = 0; i < stor.resultats.length; i++) {
      const resultat = stor.resultats[i]
      if (resultat.methode === 'ope5') listi.push(i)
    }

    for (let i = 0; i < listi.length; i++) {
      stor.resultats[listi[i]].div5.style.visibility = 'visible'
      posi.push(stor.resultats[listi[i]].div5.getBoundingClientRect())
      doni.push(stor.resultats[listi[i]])
    }
    stor.timasque = []
    stor.lemasque = j3pAddElt(getJ3pConteneur(me.zonesElts.MG), 'div', '', { style: { display: 'none', position: 'fixed', left: 0, top: 0, zIndex: 80, width: '100%', height: '100%', opacity: 0.5, background: '#000' } })
    $(stor.lemasque).css({ filter: 'alpha(opacity=50)' }).fadeIn(500)
    stor.lemasque.addEventListener('click', killcroixKill, false)
    for (let i = 0; i < posi.length; i++) {
      const left = (posi[i].left - me.zonesElts.MG.scrollLeft) + 'px'
      const top = (posi[i].top - me.zonesElts.MG.scrollTop) + 'px'
      // dans une section, on peut jouer avec les zIndex entre 1 et 50 (boutons), mais ici c’est un masque, on fait comme pour la modale (masque à 80 et contenu à 90)
      const masquei = j3pAddElt(getJ3pConteneur(me.zonesElts.MG), 'div', { style: { display: 'none', position: 'fixed', left, top, width: posi[i].width + 'px', height: posi[i].height + 'px', zIndex: 82, cursor: 'pointer' } })
      $(masquei).css({ filter: 'alpha(opacity=0)' }).fadeIn(500)
      masquei.donnees = doni[i]
      masquei.addEventListener('click', croixKill, false)
      masquei.addEventListener('mouseover', croixKillAllume, false)
      masquei.addEventListener('mouseout', croixKillEteint, false)
      masquei.innerHTML += '<svg viewBox="0 0 ' + posi[i].width + ' ' + posi[i].height + '" ><line x1="0" y1="0" x2="' + posi[i].width + '" y2="' + posi[i].height + '" style="stroke:rgb(255,0,0);stroke-width:4" /><line  x1="' + posi[i].width + '" y1="0" x2="0" y2="' + posi[i].height + '" style="stroke:rgb(255,0,0);stroke-width:4" /></svg>'
      masquei.qui1 = masquei.childNodes[0].childNodes[0]
      masquei.qui2 = masquei.childNodes[0].childNodes[1]
      stor.timasque.push(masquei)
      masquei.qui2.style.visibility = 'hidden'
      masquei.qui1.style.visibility = 'hidden'
    }
  }

  function gere5 (event) {
    if (!stor.croixListe) return console.error(Error('Pas de propriété croixListe'))
    if (stor.croixListe.length === 0) return
    const X = event.clientX
    const Y = event.clientY

    for (let i = 0; i < stor.croixListe.length; i++) {
      // rustine je sais pas pk
      if (!stor.croixListe[i]) continue
      const pos = stor.croixListe[i].getBoundingClientRect()
      if (!((X > pos.left) && (X < (pos.left + pos.width)) && (Y > pos.top) && (Y < (pos.top + pos.height)))) {
        croixCache(stor.croixListe[i])
      } else {
        croixMontre(stor.croixListe[i])
      }
    }
  }

  function croixMontre (el) {
    if (stor.desatout) return
    if (stor.isPaused) return
    adapteLarg()
    if (!el.svg) return
    if (!el.laCase) return
    el.svg.style.visibility = 'visible'
    el.laCase.style.visibility = 'visible'
    for (let i = 0; i < el.tabope5.length; i++) {
      stor.tabProp[el.tabope5[i].ligne][el.tabope5[i].colonne].style.background = '#aaa'
    }
  }

  function croixCache (el) {
    if (stor.desatout) return
    if (stor.isPaused) return
    if (!el.svg) return
    if (!el.laCase) return
    el.svg.style.visibility = 'hidden'
    el.laCase.style.visibility = 'hidden'
    for (let i = 0; i < el.tabope5.length; i++) {
      stor.tabProp[el.tabope5[i].ligne][el.tabope5[i].colonne].style.background = '#fff'
    }
  }

  function ajoutCoef (exclu) {
    if (!stor.yacoef) {
      stor.yacoef = true
      stor.colorcoef = 'black'
      // dessine bulle

      stor.divCoef2 = j3pAddElt(stor.lesdiv.ar0, 'div')
      stor.divCoef2.style.width = '150px'
      stor.divCoef2.style.height = stor.lesdiv.ar0.offsetHeight + 'px'
      stor.divCoef2.style.backgroundImage = 'url("' + j3pBaseUrl + 'sections/college/sixieme/Proportionnalite/coef.png")'
      stor.divCoef2.style.backgroundRepeat = 'no-repeat'
      stor.divCoef2.style.backgroundSize = '100% 100%'

      stor.divCoef = j3pAddElt(stor.divCoef2, 'div')
      stor.divCoef.style.position = 'relative'
      stor.divCoef.style.left = '35px'
      stor.divCoef.style.top = (stor.divCoef2.offsetHeight / 2 - 20) + 'px'
      const tab = addDefaultTable(stor.divCoef, 1, 2)
      tab[0][0].style.padding = 0
      tab[0][1].style.padding = 0
      const lapos = stor.lesdiv.ar0.getBoundingClientRect()
      const lapos2 = me.zonesElts.MG.getBoundingClientRect()
      j3pAffiche(tab[0][0], null, '$\\times$&nbsp;')
      stor.zoneCoef = new ZoneStyleMathquill2(tab[0][1], { id: 'Colfdsfdkdo1', restric: stor.restric, limite: 3 })
      stor.rectop4 = j3pAddElt(stor.divCoef2, 'div')
      stor.rectop4.style.position = 'absolute'
      stor.rectop4.style.left = (lapos.left + 4) + 'px'
      stor.rectop4.style.height = (lapos.height - 2) + 'px'
      stor.rectop4.style.top = (lapos.top - lapos2.top) + 'px'
      stor.rectop4.style.width = '40px'
      stor.rectop4.style.cursor = 'pointer'
      stor.rectop4.qui = [exclu]
      stor.rectop4.addEventListener('click', annuleCoef, false)
      stor.rectop4.addEventListener('mouseover', coefOver, false)
      stor.rectop4.addEventListener('mouseout', coefQuit, false)
    } else {
      stor.rectop4.qui.push(exclu)
    }
    // push dans resultats
    exclu.innerHTML = ''
    setTimeout(function () { detruitMenu(stor.menus, exclu) }, 10)
    let ttt
    if (ds.Completer !== 'Raisonnement') {
      j3pEmpty(exclu)
      ttt = new ZoneStyleMathquill2(exclu, { id: 'ZZopegf2fdc' + exclu.ligne + 'c' + exclu.colonne, restric: stor.restric, hasAutoKeyboard: (stor.restric.indexOf('/') !== -1), inverse: (exclu.ligne % 2 === 1), enter: me.sectionCourante.bind(me), limite: 9, sansBord: true })
      if (exclu.ligne === 1) { stor.inputrep2 = ttt } else { stor.inputrep1 = ttt }
      exclu.addEventListener('click', () => {
        setTimeout(() => { ttt.focus() }, 10)
      })
    } else {
      exclu.innerHTML = ''
      j3pAffiche(exclu, null, '?')
    }
    stor.resultats.push({ cellule: exclu, methode: 'ope4', valeur: ttt })
    if (ttt) ttt.onchange = () => { adapteLarg(); colCoef() }
    stor.zoneCoef.onchange = () => { adapteLarg(); colCoef() }
    adapteLarg()
  } // ajoutCoef
  function colCoef () {
    stor.colorcoef = 'black'
    adapteLarg()
  }

  function coefOver () {
    if (stor.isPaused) return
    stor.rectop4.style.backgroundImage = 'url("' + j3pBaseUrl + 'sections/college/sixieme/Proportionnalite/croixCo.png")'
    stor.rectop4.style.backgroundRepeat = 'no-repeat'
    stor.rectop4.style.backgroundSize = '100% 100%'
  }

  function coefQuit () {
    stor.rectop4.style.backgroundImage = ''
  }

  switch (me.etat) {
    case 'enonce':
      if (me.debutDeLaSection) {
        initSection()
      } else {
        enonceMain()
      }
      break // case "enonce":

    case 'correction': {
      // On teste si une réponse a été saisie
      stor.lesdiv.explik.classList.remove('correction')
      stor.lesdiv.explik.innerHTML = ''
      stor.lesdiv.explik2.innerHTML = ''
      const indiceEtape = me.questionCourante % ds.nbetapes
      const etape = stor.etapeAfaire[indiceEtape]

      if (!yaReponse()) {
        // pas de réponse
        stor.lesdiv.correction.style.color = me.styles.cfaux
        stor.lesdiv.correction.innerHTML = reponseIncomplete
        return this.finCorrection()
      }

      // on a une réponse
      if (isRepOk()) {
        // Bonne réponse
        stor.croixListe = []
        me.score++
        stor.lesdiv.correction.style.color = me.styles.cbien
        stor.lesdiv.correction.innerHTML = cBien

        if (etape === 'Legende') {
          stor.ListeDeroulante1.corrige(true)
          stor.ListeDeroulante2.corrige(true)
          stor.ListeDeroulante1.disable()
          stor.ListeDeroulante2.disable()
          stor.Etapefaite.push('Legende')
        }
        if (etape === 'Remplir') {
          for (let i = 0; i < stor.listeInput.length; i++) {
            stor.listeInput[i].corrige(true)
            stor.listeInput[i].disable()
          }
          stor.Etapefaite.push('Remplr')
          stor.placeNb = []
          for (let i = 0; i < stor.repEl.length; i = i + 2) {
            const test = trans(stor.repEl[i])
            if (egalite(test, tabloise(stor.nb1g1))) {
              stor.placeNb.push('nb1g1')
              stor.placeNb.push('nb2g2')
            }
            if (egalite(test, tabloise(stor.nbxg1))) {
              stor.placeNb.push('nbxg1')
              stor.placeNb.push('rep1')
            }
            if (egalite(test, tabloise(stor.nb2g1))) {
              stor.placeNb.push('nb2g1')
              stor.placeNb.push('nb2g2')
            }
            if (!stor.repEl[i]) {
              stor.placeNb.push('rep2')
              stor.placeNb.push('nbxg2')
            }
          }
        }
        if ((['les deux', 'Raisonnement', 'Reponse'].indexOf(etape) !== -1) || (ds.nbetapes === 1)) {
          desactiveAll()
          passeToutVert()
          stor.paspas = true
          stor.isPaused = true
        }
        if (etape === 'Conclusion') {
          stor.zoneConcl1.corrige(true)
          stor.zoneConcl2.corrige(true)
          stor.zoneConcl1.disable()
          stor.zoneConcl2.disable()
        }

        this.typederreurs[0]++
        return this.finCorrection('navigation', true)
      }

      // Pas de bonne réponse
      stor.lesdiv.correction.style.color = me.styles.cfaux
      stor.lesdiv.correction.innerHTML = cFaux
      if (this.isElapsed) {
        // A cause de la limite de temps :
        stor.croixListe = []
        stor.lesdiv.correction.innerHTML += tempsDepasse + '<BR>'
        this.typederreurs[10]++
        affCorrFaux(true)
        return this.finCorrection('navigation', true)
      }

      // Réponse fausse sans limite de temps
      if (this.essaiCourant < ds.nbchances) {
        stor.lesdiv.correction.innerHTML += '<br>' + essaieEncore
        affCorrFaux(false)
        this.typederreurs[1]++
        return this.finCorrection() // on reste en correction
      }

      // Erreur au dernier essai
      if (!((etape === 'les deux') ||
                (etape === 'Raisonnement') ||
                (etape === 'Reponse'))) {
        stor.lesdiv.correction.innerHTML += '<BR>' + textes.regardecp
      }

      affCorrFaux(true)
      this.typederreurs[2]++
      this.finCorrection('navigation', true)
      break // case "correction":
    }

    case 'navigation':
      if (!this.sectionTerminee()) this.etat = 'enonce'
      this.finNavigation()
      break // case "navigation":
  }
}
