import { j3pAddElt, j3pArrondi, j3pGetRandomInt, j3pAddTxt, j3pEmpty, j3pAjouteBouton } from 'src/legacy/core/functions'
import { colorCorrection, colorKo, colorOk } from 'src/legacy/core/StylesJ3p'
import ListeDeroulante from 'src/legacy/outils/listeDeroulante/ListeDeroulante'
import { j3pAffiche } from 'src/lib/mathquill/functions'
import { addDefaultTable } from 'src/legacy/themes/table'
import ZoneStyleMathquill1 from 'src/legacy/outils/zoneStyleMathquill/ZoneStyleMathquill1'
import { j3pAfficheCroixFenetres, j3pCreeFenetres, j3pToggleFenetres } from 'src/legacy/core/functionsJqDialog'
import textesGeneriques from 'src/lib/core/textes'

const { cBien, essaieEncore, regardeCorrection, reponseIncomplete, tempsDepasse } = textesGeneriques

/**
 * Ajouter une description ici
 * @fileOverview
 * @author Tommy Barroy <t.barroy@laposte.net>
 * @since 2017-10
*/

export const params = {
  outils: ['calculatrice', 'mathquill'],
  /**
   * Ce qui est paramétrable dans l’éditeur de graphe
   * @type {LegacySectionParamsParametres}
   */
  parametres: [
    ['nbrepetitions', 9, 'entier', 'Nombre de répétitions de la section'],
    ['indication', '', 'string', 'Indication pour chaque répétition'],
    ['limite', 0, 'entier', 'Temps disponible par répétition, en secondes'],
    ['Presentation', true, 'boolean', "true: Les indications de réussite et les boutons 'ok' et 'suivant' sont dans un cadre séparé à droite. <BR> false: Un seul cadre pour tout"],
    ['Calculatrice', true, 'boolean', '<u>true</u>: L’élève dispose d’une calculatrice.'],
    ['theme', 'standard', 'liste', 'Modèle pour l’affichage', ['standard', 'zonesAvecImageDeFond']]

  ]
}

const textes = {
  couleurexplique: '#A9E2F3',
  couleurcorrec: '#A9F5A9',
  couleurenonce: '#D8D8D8',
  couleurenonceg: '#F3E2A9',
  couleuretiquettes: '#A9E2F3',
  grandeurs: [
    ['nombre d’oranges', 'oranges', 'quantité de jus d’orange', 'cL de jus d’orange', 1, 0, 'presser '],
    ['nombre de pots de peinture', 'pots de peinture', 'surface peinte', 'm²', 1, 0, 'peindre '],
    ['quantité d’essence', 'L d’essence', 'distance parcourue', 'km', 0, 0, 'parcourir '],
    ['prix du ruban', '€', 'longueur du ruban', 'cm de ruban', 1, 0, 'acheter '],
    ['quantité de noisettes', 'g de noisettes', 'nombre de desserts', 'desserts', 0, 1, 'cuisiner '],
    ['quantité de feuilles', 'feuilles', 'nombre de dessins', 'dessins', 0, 1, 'dessiner ']
  ],
  foisplus: '£a fois plus',
  foismoins: '£a fois moins',
  consigne1: 'Avec £a £b , je peux £e £c £d .',
  consigne2: 'S’agit-il d’une situation de proportionnalité ?',
  rep1: 'Quand ',
  grand: ' grand ',
  petit: ' petit ',
  operations: ['Choisir', 'je multiplie par', 'je divise par', 'j’ajoute', 'je soustrais'],
  operations2oui: ['je multiplie aussi par ', 'je divise aussi par ', 'j’ajoute aussi ', 'je soustrais aussi '],
  operations2non: ['je ne multiplie pas par ', 'je ne divise pas par ', 'je n’ajoute pas ', 'je ne soustrais pas '],
  liaison: [' la ', ' le ', ' à la ', ' au '],
  attente: 'je ...',
  choisir: 'Choisir',
  aussi: ' aussi',
  choix: ['Choisir', 'Il s’agit', 'Il ne s’agit pas'],
  finrep: ' d’une situation de proportionnalité.',
  raisonnementfaux: 'Raisonnement faux !',
  explikop: 'Deux grandeurs sont proportionnelles quand le produit (ou le quotient) par un nombre de l’une des deux implique le produit (ou le quotient) de l’autre par le même nombre. ',
  expliknb: '£a n’est pas £b fois plus £c que £d .',
  expliknbgood: '£a est aussi £b fois plus £c que £d .',
  faux: 'C’est faux',
  corrige2: ['je multiplie aussi ', 'je ne multiplie pas '],
  corrige3: ['il s’agit ', 'il ne s’agit pas'],
  corrige: 'Quand je mulptiplie £a par £b , £c £d par £e , donc £f d’une situation de proportionnalité.'
}

/**
 * section proportionnalite01
 * @this {Parcours}
 */
export default function main () {
  const me = this
  const ds = me.donneesSection
  const stor = this.storage

  function gere () {
    if (!stor.ListeDeroulante1.changed || stor.ListeDeroulante1.reponse === 'Choisir') return
    if (stor.ListeDeroulante2 !== undefined) {
      stor.ListeDeroulante2.disable()
      stor.ListeDeroulante2 = undefined
    }
    j3pEmpty(stor.tatabRep2)
    const i = textes.operations.indexOf(stor.ListeDeroulante1.reponse) - 1
    stor.ope = (i < 2) ? 0 : 2
    j3pEmpty(stor.tatabRep1[0][4])
    j3pAddTxt(stor.tatabRep1[0][4], '&nbsp;' + textes.liaison[stor.genre1 + stor.ope])
    stor.nb = parseInt(stor.zoneNb.reponse().replace(/ /g, ''))
    if (stor.zoneNb.reponse() === '') stor.nb = '??'
    const montab = []
    montab[0] = textes.choisir
    montab.push(textes.operations2oui[i] + stor.nb + ' ' + textes.liaison[stor.genre2 + stor.ope] + stor.grandeur2)
    montab.push(textes.operations2non[i] + stor.nb + ' ' + textes.liaison[stor.genre2 + stor.ope] + stor.grandeur2)
    stor.ListeDeroulante2 = ListeDeroulante.create(stor.tatabRep2, montab, { centre: true, alignmathquill: true })
  }

  function boutonfen () {
    j3pToggleFenetres('Calculatrice')
  }

  function yareponse () {
    if (me.isElapsed) { return true }
    if (!stor.ListeDeroulante0.changed) {
      stor.ListeDeroulante0.focus()
      return false
    }
    if (!stor.ListeDeroulante1.changed) {
      stor.ListeDeroulante1.focus()
      return false
    }
    if (stor.ListeDeroulante2 !== undefined) {
      if (!stor.ListeDeroulante2.changed) {
        stor.ListeDeroulante2.focus()
        return false
      }
    }
    if (stor.zoneNb.reponse() === '') {
      stor.zoneNb.focus()
      return false
    }
    return true
  }

  function bonneReponse () {
    // cette fonction renvoie true ou false si du moins une réponse a été saisie

    stor.erreurop = false
    stor.erreurmult = false
    stor.erreuraussi = false
    stor.erreurrep = false
    // test du choix d’operation
    let i = textes.operations.indexOf(stor.ListeDeroulante1.reponse) - 1
    if (i > 1) { // repondu ajoute ou soustrait -> erreur3
      stor.erreurop = true
      stor.ListeDeroulante1.corrige(false)
    } else {
      stor.ListeDeroulante1.corrige(true)
    }
    // les deux autres sont comptées bonnes, on ne sais pas dans quel sens l’éleve fait son raisonnement

    // test du multiplicateur (diviseur)
    if (parseFloat(stor.nb) !== stor.fois) {
      // multiplicateur deconne -> erreur4
      stor.erreurmult = true
      stor.zoneNb.corrige(false)
    } else {
      stor.zoneNb.corrige(true)
    }

    // test reponse aussi ou pas
    if ((!stor.erreurmult) && (!stor.erreurop)) {
      i = stor.ListeDeroulante2.givenChoices.indexOf(stor.ListeDeroulante2.reponse)
      if (((i === 1) && (!stor.vrai)) || ((i === 2) && (stor.vrai))) {
        // quand ya erreur sur le aussi ou pas -> erreur 5
        stor.erreuraussi = true
        stor.ListeDeroulante2.corrige(false)
      } else {
        stor.ListeDeroulante2.corrige(true)
      }
    }
    // tete reponse finale
    i = stor.ListeDeroulante0.givenChoices.indexOf(stor.ListeDeroulante0.reponse)
    if (((i === 1) && (!stor.vrai)) || ((i === 2) && (stor.vrai))) {
      // quand ya erreur  -> erreur 6
      stor.erreurrep = 6
      stor.ListeDeroulante0.corrige(false)
    } else {
      stor.ListeDeroulante0.corrige(true)
    }

    return ((!stor.erreurop) && (!stor.erreurmult) && (!stor.erreuraussi) && (!stor.erreurrep))
  }

  function initSection () {
    const structure = ds.Presentation ? 'presentation1' : 'presentation3'
    me.construitStructurePage(structure)

    // DECLARATION DES VARIABLES
    // reponse attendue
    stor.nombre1grandeur1 = 1
    stor.nombre2grandeur1 = 1
    stor.nombre1grandeur2 = 1
    stor.nombre2grandeur2 = 1
    stor.genre1 = 0
    stor.genre2 = 0
    stor.ope = 0
    stor.fois = 1
    stor.grandeur1 = ''
    stor.grandeur2 = ''
    stor.unitegrandeur1 = ''
    stor.unitegrandeur2 = ''
    stor.vrai = true
    stor.plusoumoins = true
    // contient un tab reponse [nb,bool=true si rep est diviseur,bool=true si vient d’un calcul]

    /*
             Par convention,`
            `   me.typederreurs[0] = nombre de bonnes réponses
                me.typederreurs[1] = nombre de fois où l’élève a utilisé une chance supplémentaire
                me.typederreurs[2] = nombre de mauvaises réponses
                me.typederreurs[10] = nombre de fois où l’élève a eu faux à cause d’un temps limite
                LES AUTRES peuvent être utilisées par la détermination de la pe dans le cas d’une section qualitative
             */
    me.typederreurs = [0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0]
    me.afficheTitre('Déterminer si une situation est de proportionnalité')
    if (ds.indication) me.indication(me.zones.IG, ds.indication)
    enonceMain()
  }

  function enonceMain () {
    me.videLesZones()
    let i = j3pGetRandomInt(0, 1)
    stor.vrai = (i === 0)
    stor.vraico = i
    i = j3pGetRandomInt(0, 1)
    stor.plusoumoins = (i === 0)
    i = j3pGetRandomInt(0, (textes.grandeurs.length - 1))
    stor.grandeur1 = textes.grandeurs[i][0]
    stor.grandeur2 = textes.grandeurs[i][2]
    stor.unitegrandeur1 = textes.grandeurs[i][1]
    stor.unitegrandeur2 = textes.grandeurs[i][3]
    stor.genre1 = textes.grandeurs[i][4]
    stor.genre2 = textes.grandeurs[i][5]
    stor.verbe = textes.grandeurs[i][6]
    stor.fois = j3pGetRandomInt(2, 11)
    if (stor.plusoumoins) {
      stor.nombre1grandeur1 = j3pGetRandomInt(2, 24)
      stor.nombre2grandeur1 = stor.fois * stor.nombre1grandeur1
      stor.nombre1grandeur2 = j3pGetRandomInt(2, 24)
      if (stor.vrai) { stor.nombre2grandeur2 = stor.fois * stor.nombre1grandeur2 } else { stor.nombre2grandeur2 = stor.fois * stor.nombre1grandeur2 + Math.pow(-1, j3pGetRandomInt(0, 1)) * j3pGetRandomInt(1, 4) }
    } else {
      stor.nombre2grandeur1 = j3pGetRandomInt(3, 24)
      stor.nombre1grandeur1 = stor.fois * stor.nombre2grandeur1
      stor.nombre2grandeur2 = j3pGetRandomInt(3, 24)
      if (stor.vrai) { stor.nombre1grandeur2 = stor.fois * stor.nombre2grandeur2 } else { stor.nombre1grandeur2 = stor.fois * stor.nombre2grandeur2 + Math.pow(-1, j3pGetRandomInt(0, 1)) * j3pGetRandomInt(1, 4) }
    }
    stor.ope = 0

    stor.lesdiv = {}
    stor.lesdiv.conteneur = j3pAddElt(me.zonesElts.MG, 'div', { style: me.styles.etendre('toutpetit.enonce', { padding: '10px' }) })
    if (ds.Presentation) {
      stor.lesdiv.consigne = addDefaultTable(stor.lesdiv.conteneur, 1, 1)[0][0]
      stor.lesdiv.calculatrice = j3pAddElt(me.zonesElts.MD, 'div', {
        style: me.styles.etendre('toutpetit.enonce', { padding: '10px' })
      })
      stor.lesdiv.correction = j3pAddElt(me.zonesElts.MD, 'div', { style: me.styles.etendre('toutpetit.enonce', { padding: '10px' }) })
    } else {
      const tab1 = addDefaultTable(stor.lesdiv.conteneur, 1, 3)
      stor.lesdiv.consigne = tab1[0][0]
      tab1[0][1].style.width = '40px'
      stor.lesdiv.calculatrice = j3pAddElt(tab1[0][2], 'div', {
        style: me.styles.etendre('toutpetit.enonce', { padding: '10px' })
      })
      stor.lesdiv.correction = j3pAddElt(tab1[0][2], 'div', { style: me.styles.etendre('toutpetit.enonce', { padding: '10px' }) })
    }
    j3pEmpty(stor.lesdiv.calculatrice)
    if (ds.Calculatrice) {
      me.fenetresjq = [{ name: 'Calculatrice', title: 'Une calculatrice', left: 600, top: 10 }]
      j3pCreeFenetres(me)
      j3pAfficheCroixFenetres('Calculatrice')
      j3pAjouteBouton(stor.lesdiv.calculatrice, 'Calculatrice', 'MepBoutons', 'Calculatrice', boutonfen)
    }
    stor.lesdiv.travail = addDefaultTable(stor.lesdiv.conteneur, 1, 1)[0][0]

    j3pAffiche(stor.lesdiv.consigne, null, textes.consigne1 + '\n',
      {
        a: stor.nombre1grandeur1,
        b: stor.unitegrandeur1,
        c: stor.nombre1grandeur2,
        d: stor.unitegrandeur2,
        e: stor.verbe
      })
    j3pAffiche(stor.lesdiv.consigne, null, textes.consigne1 + '\n',
      {
        a: stor.nombre2grandeur1,
        b: stor.unitegrandeur1,
        c: stor.nombre2grandeur2,
        d: stor.unitegrandeur2,
        e: stor.verbe
      })
    j3pAffiche(stor.lesdiv.consigne, null, '<b>' + textes.consigne2 + '</b>')
    const tatabRep = addDefaultTable(stor.lesdiv.travail, 3, 1)
    stor.tatabRep1 = addDefaultTable(tatabRep[0][0], 1, 7)
    stor.tatabRep2 = tatabRep[1][0]
    stor.tatabRep3 = addDefaultTable(tatabRep[2][0], 1, 2)
    j3pAddTxt(stor.tatabRep1[0][0], textes.rep1 + '&nbsp;')

    j3pAddTxt(stor.tatabRep1[0][2], '&nbsp;')

    stor.zoneNb = new ZoneStyleMathquill1(stor.tatabRep1[0][3], { restric: '0123456789' })
    stor.zoneNb.modifL = gere

    j3pAddTxt(stor.tatabRep1[0][4], '&nbsp;' + textes.liaison[stor.genre1 + stor.ope])
    j3pAddTxt(stor.tatabRep1[0][5], '&nbsp;' + stor.grandeur1)
    j3pAddTxt(stor.tatabRep1[0][6], ' , ')
    j3pAddTxt(stor.tatabRep2, textes.attente)
    j3pAddTxt(stor.tatabRep3[0][1], '&nbsp;' + textes.finrep)
    stor.ListeDeroulante0 = ListeDeroulante.create(stor.tatabRep3[0][0], textes.choix, { centre: true, alignmathquill: true })
    stor.ListeDeroulante1 = ListeDeroulante.create(stor.tatabRep1[0][1], textes.operations, { onChange: gere, centre: true, alignmathquill: true })
    stor.lesdiv.explik2 = j3pAddElt(stor.lesdiv.conteneur, 'div')
    stor.lesdiv.explik2.style.color = colorKo
    stor.lesdiv.explik = j3pAddElt(stor.lesdiv.conteneur, 'div')
    stor.lesdiv.explik.style.color = colorCorrection
    // Obligatoire
    stor.lesdiv.consigne.classList.add('enonce')
    stor.lesdiv.travail.classList.add('travail')
    me.zonesElts.MG.classList.add('fond')
    me.finEnonce()
  }

  switch (this.etat) {
    case 'enonce':
      if (this.debutDeLaSection) {
        initSection()
      } else {
        enonceMain()
      }
      break // case "enonce":

    case 'correction': {
      // On teste si une réponse a été saisie
      if (!yareponse()) {
        stor.lesdiv.correction.style.color = colorKo
        stor.lesdiv.correction.innerHTML = reponseIncomplete
        return this.finCorrection()
      }

      // Bonne réponse
      if (bonneReponse()) {
        this.score += j3pArrondi(1 / ds.nbetapes, 1)
        stor.lesdiv.correction.style.color = colorOk
        stor.lesdiv.correction.innerHTML = cBien

        stor.ListeDeroulante0.disable()
        stor.ListeDeroulante1.disable()
        stor.ListeDeroulante2.disable()
        stor.ListeDeroulante0 = undefined
        stor.ListeDeroulante1 = undefined
        stor.ListeDeroulante2 = undefined
        stor.zoneNb.disable()

        this.typederreurs[0]++
        return this.finCorrection('navigation', true)
      }

      // Pas de bonne réponse
      stor.lesdiv.correction.style.color = colorKo

      // A cause de la limite de temps :
      if (this.isElapsed) { // limite de temps
        stor.lesdiv.correction.innerHTML = tempsDepasse + '<BR>'
        this.typederreurs[10]++
        let afficheAide = false
        if ((stor.erreurop) || (stor.erreurmult) || (stor.erreuraussi)) {
          stor.lesdiv.correction.innerHTML += textes.raisonnementfaux
        }
        if (stor.erreurop) {
          if ((stor.nb !== '') && (stor.nb !== '??') && (stor.ListeDeroulante1.changed)) {
            afficheAide = true
            j3pAffiche(stor.lesdiv.explik2, null, textes.explikop + '\n')
          }
        }
        if (stor.erreurmult && (!stor.erreurop)) {
          const bu = (stor.nombre1grandeur1 > stor.nombre2grandeur1) ? textes.grand : textes.petit
          if ((stor.nb !== '') && (stor.nb !== '??') && (stor.ListeDeroulante1.changed)) {
            afficheAide = true
            j3pAffiche(stor.lesdiv.explik2, null, textes.expliknb + '\n',
              {
                a: stor.nombre1grandeur1 + ' ' + stor.unitegrandeur1,
                b: stor.nb,
                c: bu,
                d: stor.nombre2grandeur1 + ' ' + stor.unitegrandeur1
              })
          }
        }

        if (stor.erreuraussi && (!stor.erreurmult) && (!stor.erreurop)) {
          if (stor.vrai) {
            const bu = (stor.nombre1grandeur2 > stor.nombre2grandeur2) ? textes.grand : textes.petit
            afficheAide = true
            j3pAffiche(stor.lesdiv.explik2, null, textes.expliknbgood + '\n',
              {
                a: stor.nombre1grandeur2 + ' ' + stor.unitegrandeur2,
                b: stor.nb,
                c: bu,
                d: stor.nombre2grandeur2 + ' ' + stor.unitegrandeur2
              })
          } else {
            const bu = (stor.nombre1grandeur2 > stor.nombre2grandeur2) ? textes.grand : textes.petit
            afficheAide = true
            j3pAffiche(stor.lesdiv.explik2, null, textes.expliknb + '\n',
              {
                a: stor.nombre1grandeur2 + ' ' + stor.unitegrandeur2,
                b: stor.nb,
                c: bu,
                d: stor.nombre2grandeur2 + ' ' + stor.unitegrandeur2
              })
          }
        }

        if (stor.erreurrep) {
          stor.lesdiv.correction.innerHTML += '<BR>' + textes.faux
        }
        stor.lesdiv.correction.innerHTML += '<br>' + regardeCorrection

        j3pAffiche(stor.lesdiv.explik, null, textes.corrige,
          {
            a: textes.liaison[stor.genre1] + stor.grandeur1,
            b: stor.fois,
            c: textes.corrige2[stor.vraico],
            d: textes.liaison[stor.genre2] + stor.grandeur2,
            e: stor.fois,
            f: textes.corrige3[stor.vraico]
          })
        // "Quand je mulptiplie £a par £b , £c £d par £e , donc £f d’une situation de proportionnalité."

        stor.ListeDeroulante0.disable(stor.erreurrep)
        stor.ListeDeroulante1.disable(stor.erreurop)
        if (stor.ListeDeroulante2 !== undefined) {
          stor.ListeDeroulante2.disable(stor.erreuraussi)
        }
        stor.ListeDeroulante0 = undefined
        stor.ListeDeroulante1 = undefined
        stor.ListeDeroulante2 = undefined

        if (stor.erreurmult) stor.zoneNb.barre()
        stor.zoneNb.disable()
        if (ds.theme === 'zonesAvecImageDeFond') {
          stor.lesdiv.explik.classList.add('correction')
          if (afficheAide) {
            stor.lesdiv.explik2.classList.add('explique')
          }
        }
        return this.finCorrection('navigation', true)
      } // limite de temps

      // Réponse fausse
      if (this.essaiCourant < ds.nbchances) {
        stor.lesdiv.correction.innerHTML += '<br>' + essaieEncore
        this.typederreurs[1]++
        return this.finCorrection()
      }

      // Erreur au dernier essai
      let afficheAide = false
      if ((stor.erreurop) || (stor.erreurmult) || (stor.erreuraussi)) {
        stor.lesdiv.correction.innerHTML += textes.raisonnementfaux
      }
      if (stor.erreurop) {
        j3pAffiche(stor.lesdiv.explik2, null, textes.explikop + '\n')
        afficheAide = true
      }
      if (stor.erreurmult && (!stor.erreurop)) {
        const bu = (stor.nombre1grandeur1 > stor.nombre2grandeur1) ? textes.grand : textes.petit
        j3pAffiche(stor.lesdiv.explik2, null, textes.expliknb + '\n',
          {
            a: stor.nombre1grandeur1 + ' ' + stor.unitegrandeur1,
            b: stor.nb,
            c: bu,
            d: stor.nombre2grandeur1 + ' ' + stor.unitegrandeur1
          })
        afficheAide = true
      }

      if (stor.erreuraussi && (!stor.erreurmult) && (!stor.erreurop)) {
        if (stor.vrai) {
          const bu = (stor.nombre1grandeur2 > stor.nombre2grandeur2) ? textes.grand : textes.petit
          j3pAffiche(stor.lesdiv.explik2, null, textes.expliknbgood + '\n',
            {
              a: stor.nombre1grandeur2 + ' ' + stor.unitegrandeur2,
              b: stor.nb,
              c: bu,
              d: stor.nombre2grandeur2 + ' ' + stor.unitegrandeur2
            })
        } else {
          const bu = (stor.nombre1grandeur2 > stor.nombre2grandeur2) ? textes.grand : textes.petit
          j3pAffiche(stor.lesdiv.explik2, null, textes.expliknb + '\n',
            {
              a: stor.nombre1grandeur2 + ' ' + stor.unitegrandeur2,
              b: stor.nb,
              c: bu,
              d: stor.nombre2grandeur2 + ' ' + stor.unitegrandeur2
            })
        }
        afficheAide = true
      }

      if (stor.erreurrep) {
        stor.lesdiv.correction.innerHTML += '<BR>' + textes.faux
      }
      stor.lesdiv.correction.innerHTML += '<br>' + regardeCorrection

      j3pAffiche(stor.lesdiv.explik, null, textes.corrige,
        {
          a: textes.liaison[stor.genre1] + stor.grandeur1,
          b: stor.fois,
          c: textes.corrige2[stor.vraico],
          d: textes.liaison[stor.genre2] + stor.grandeur2,
          e: stor.fois,
          f: textes.corrige3[stor.vraico]
        })
      // "Quand je mulptiplie £a par £b , £c £d par £e , donc £f d’une situation de proportionnalité."

      stor.ListeDeroulante0.disable()
      stor.ListeDeroulante1.disable()
      stor.ListeDeroulante2.disable()
      stor.ListeDeroulante0 = undefined
      stor.ListeDeroulante1 = undefined
      stor.ListeDeroulante2 = undefined
      if (stor.erreurmult) { stor.zoneNb.barre() }
      stor.zoneNb.disable()
      if (ds.theme === 'zonesAvecImageDeFond') {
        stor.lesdiv.explik.classList.add('correction')
        if (afficheAide) {
          stor.lesdiv.explik2.classList.add('explique')
        }
      }

      this.typederreurs[2]++
      this.finCorrection('navigation', true)
      break // case "correction"
    }

    case 'navigation':
      if (!this.sectionTerminee()) {
        this.etat = 'enonce'
      }
      this.finNavigation()
      break // case "navigation":
  }
}
