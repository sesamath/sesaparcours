import { j3pAddElt, j3pAddTxt, j3pAjouteBouton, j3pArrondi, j3pBarre, j3pClone, j3pEmpty, j3pGetRandomBool, j3pGetRandomInt, j3pPGCD, j3pShuffle } from 'src/legacy/core/functions'
import ListeDeroulante from 'src/legacy/outils/listeDeroulante/ListeDeroulante'
import { j3pAffiche, j3pDesactive } from 'src/lib/mathquill/functions'
import { addDefaultTable } from 'src/legacy/themes/table'
import textesGeneriques from 'src/lib/core/textes'

const { cBien, cFaux, essaieEncore, reponseIncomplete, tempsDepasse } = textesGeneriques

/**
 * Ajouter une description ici
 * @fileOverview
 * @author Tommy Barroy <t.barroy@laposte.net>
 * @since 2017-10
*/

export const params = {
  outils: ['mathquill'],
  /**
   * Ce qui est paramétrable dans l’éditeur de graphe
   * @type {LegacySectionParamsParametres}
   */
  parametres: [
    ['nbrepetitions', 3, 'entier', 'Nombre de répétitions de la section'],
    ['nbchances', 2, 'entier', 'Nombre d’essais possibles.'],
    ['Positifs', 'parfois', 'liste', 'Données positives', ['toujours', 'jamais', 'parfois']],
    ['Quotients', 'entier', 'liste', 'Quotients attendus', ['entier', 'décimal', 'fraction', 'entier_décimal', 'entier_fraction', 'décimal_fraction', 'les trois']],
    ['Guide', true, 'boolean', '<u>true</u>: La démarche de résolution est explicite.'],
    // le modèle gère la compatibilité ascendante avec l’ancien paramètre MiseEnPagePlusClaire
    ['theme', 'standard', 'liste', 'Modèle pour l’affichage', ['standard', 'zonesAvecImageDeFond']]
  ]
}

/**
 * section propor03
 * @this {Parcours}
 */
export default function main () {
  const me = this
  const ds = me.donneesSection
  const stor = me.storage
  const grandeurs1 = [{ n: 'longueur', u: ['cm', 'm', 'km'] }, { n: 'durée', u: ['min', 'h', 's'] }, { n: 'prix', u: ['€'] }, { n: 'aire', u: ['cm²', 'm²', 'km²'] }]
  const grandeurs2 = [{ n: 'température', u: ['°C'] }, { n: 'altitude', u: ['m', 'cm', 'km'] }]

  function initSection () {
    // On surcharge avec les parametres passés par le graphe
    me.validOnEnter = false // ex donneesSection.touche_entree

    // Construction de la page
    me.construitStructurePage('presentation3')

    ds.lesExos = []

    let lp = [false]
    if (ds.Positifs === 'parfois') lp.push(true)
    if (ds.Positifs === 'toujours') lp = [true]
    lp = j3pShuffle(lp)
    for (let i = 0; i < ds.nbitems; i++) {
      ds.lesExos.push({ signe: lp[i % lp.length] })
    }
    ds.lesExos = j3pShuffle(ds.lesExos)

    lp = [true, false]
    for (let i = 0; i < ds.nbitems; i++) {
      ds.lesExos[i].cas = lp[i % lp.length]
    }
    ds.lesExos = j3pShuffle(ds.lesExos)

    lp = []
    if (ds.Quotients.indexOf('entier') !== -1 || ds.Quotients === 'les trois') lp.push('ent')
    if (ds.Quotients.indexOf('déci') !== -1 || ds.Quotients === 'les trois') lp.push('déci')
    if (ds.Quotients.indexOf('frac') !== -1 || ds.Quotients === 'les trois') lp.push('frac')
    for (let i = 0; i < ds.nbitems; i++) {
      ds.lesExos[i].type = lp[i % lp.length]
    }
    ds.lesExos = j3pShuffle(ds.lesExos)
    me.typederreurs = [0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0]
    me.afficheTitre('Proportionnalité ?')
    enonceMain()
  }

  function faisChoixExos () {
    const e = ds.lesExos.pop()
    stor.couples = []
    stor.bug = -1
    stor.egb = true
    let n1, n2, n3
    switch (e.type) {
      case 'ent':
        stor.coef = j3pGetRandomInt(2, 99)
        if (!e.signe && j3pGetRandomBool()) {
          stor.coef = -stor.coef
        }
        stor.couples[0] = { n: j3pGetRandomInt(2, 30) }
        stor.couples[0].n2 = stor.couples[0].n * stor.coef
        do {
          stor.couples[1] = { n: j3pGetRandomInt(2, 30) }
        } while (stor.couples[1].n === stor.couples[0].n)
        stor.couples[1].n2 = stor.couples[1].n * stor.coef
        do {
          stor.couples[2] = { n: j3pGetRandomInt(2, 30) }
        } while (stor.couples[1].n === stor.couples[2].n || stor.couples[0].n === stor.couples[2].n)
        stor.couples[2].n2 = stor.couples[2].n * stor.coef
        stor.afCoef = String(stor.coef)
        stor.afCoef = [stor.afCoef, stor.afCoef, stor.afCoef]
        if (!e.cas) {
          stor.bug = j3pGetRandomInt(0, 2)
          if (j3pGetRandomBool()) {
            stor.couples[stor.bug].n++
          } else {
            stor.couples[stor.bug].n2++
          }
          stor.afCoef[stor.bug] = j3pArrondi(stor.couples[stor.bug].n2 / stor.couples[stor.bug].n, 2)
          stor.egb = (Math.abs(stor.afCoef[stor.bug] - stor.couples[stor.bug].n2 / stor.couples[stor.bug].n) < 0.0000000001)
        }
        break
      case 'déci':
        stor.coef = j3pArrondi(j3pGetRandomInt(1, 9) + j3pGetRandomInt(1, 9) / 10, 1)
        if (!e.signe && j3pGetRandomBool()) {
          stor.coef = -stor.coef
        }
        stor.couples[0] = { n: j3pGetRandomInt(2, 30) }
        stor.couples[0].n2 = j3pArrondi(stor.couples[0].n * stor.coef, 1)
        do {
          stor.couples[1] = { n: j3pGetRandomInt(2, 30) }
        } while (stor.couples[1].n === stor.couples[0].n)
        stor.couples[1].n2 = j3pArrondi(stor.couples[1].n * stor.coef, 1)
        do {
          stor.couples[2] = { n: j3pGetRandomInt(2, 30) }
        } while (stor.couples[1].n === stor.couples[2].n || stor.couples[0].n === stor.couples[2].n)
        stor.couples[2].n2 = j3pArrondi(stor.couples[2].n * stor.coef, 1)
        stor.afCoef = String(stor.coef).replace('.', ',')
        stor.afCoef = [stor.afCoef, stor.afCoef, stor.afCoef]
        if (!e.cas) {
          stor.bug = j3pGetRandomInt(0, 2)
          if (j3pGetRandomBool()) {
            stor.couples[stor.bug].n++
          } else {
            stor.couples[stor.bug].n2++
          }
          stor.afCoef[stor.bug] = j3pArrondi(stor.couples[stor.bug].n2 / stor.couples[stor.bug].n, 3)
          stor.egb = (Math.abs(stor.afCoef[stor.bug] - stor.couples[stor.bug].n2 / stor.couples[stor.bug].n) < 0.0000000001)
        }
        break
      case 'frac':
        stor.coef = { num: j3pGetRandomInt(1, 27) }
        stor.coef.den = premsavec(stor.coef.num)
        if (!e.signe && j3pGetRandomBool()) {
          stor.coef.num = -stor.coef.num
        }
        do {
          n1 = j3pGetRandomInt(2, 30)
        } while (j3pPGCD(n1, stor.coef.den, { negativesAllowed: true, valueIfZero: 1 }) !== 1)
        do {
          n2 = j3pGetRandomInt(2, 30)
        } while (j3pPGCD(n2, stor.coef.den, { negativesAllowed: true, valueIfZero: 1 }) !== 1 || n2 === n1)
        do {
          n3 = j3pGetRandomInt(2, 30)
        } while (j3pPGCD(n3, stor.coef.den, { negativesAllowed: true, valueIfZero: 1 }) !== 1 || n3 === n1 || n3 === n2)
        stor.couples[0] = { n: n1 * stor.coef.den, n2: n1 * stor.coef.num }
        stor.couples[1] = { n: n2 * stor.coef.den, n2: n2 * stor.coef.num }
        stor.couples[2] = { n: n3 * stor.coef.den, n2: n3 * stor.coef.num }
        stor.afCoef = '\\frac{' + stor.coef.num + '}{' + stor.coef.den + '}'
        stor.afCoef = [stor.afCoef, stor.afCoef, stor.afCoef]
        if (!e.cas) {
          stor.bug = j3pGetRandomInt(0, 2)
          if (j3pGetRandomBool()) {
            stor.couples[stor.bug].n++
          } else {
            stor.couples[stor.bug].n2++
          }
          const peg = j3pPGCD(stor.couples[stor.bug].n2, stor.couples[stor.bug].n, { negativesAllowed: true, valueIfZero: 1 })
          stor.afCoef[stor.bug] = '\\frac{' + (stor.couples[stor.bug].n2 / peg) + '}{' + (stor.couples[stor.bug].n / peg) + '}'
          stor.egb = true
        }
        break
    }
    if (!e.signe && j3pGetRandomBool()) {
      stor.couples[0] = { n: -stor.couples[0].n, n2: -stor.couples[0].n2 }
    }
    if (!e.signe && j3pGetRandomBool()) {
      stor.couples[1] = { n: -stor.couples[1].n, n2: -stor.couples[1].n2 }
    }
    if (!e.signe && j3pGetRandomBool()) {
      stor.couples[2] = { n: -stor.couples[2].n, n2: -stor.couples[2].n2 }
    }
    const tt = e.signe
      ? j3pClone(grandeurs2).concat(grandeurs1)
      : j3pClone(grandeurs2)
    n1 = tt[j3pGetRandomInt(0, tt.length - 1)]
    stor.g1n = n1.n
    stor.g1u = n1.u[j3pGetRandomInt(0, n1.u.length - 1)]
    do {
      n2 = tt[j3pGetRandomInt(0, tt.length - 1)]
    } while (n2.n === stor.g1n)
    stor.g2n = n2.n
    stor.g2u = n2.u[j3pGetRandomInt(0, n2.u.length - 1)]
    return e
  }

  function poseQuestion () {
    let i
    if (ds.Guide) {
      j3pAffiche(stor.lesdiv.consigne, null, '<b>Effectue les calculs nécessaires pour \n</b> déterminer si le tableau suivant représente une situation de proportionnalité.\n\n')
    } else {
      j3pAffiche(stor.lesdiv.consigne, null, '<b>Le tableau suivant représente-t-il une situation de proportionnalité ?</b>\n\n')
    }
    const ltab = addDefaultTable(stor.lesdiv.consigne, 2, 4)
    j3pAffiche(ltab[0][0], null, '&nbsp;' + stor.g1n + ' (en ' + stor.g1u + ') &nbsp;')
    j3pAffiche(ltab[1][0], null, '&nbsp;' + stor.g2n + ' (en ' + stor.g2u + ') &nbsp;')
    ltab[0][0].style.border = '1px solid black'
    ltab[1][0].style.border = '1px solid black'
    ltab[0][0].style.background = '#ffffff'
    ltab[1][0].style.background = '#ffffff'
    for (i = 0; i < 3; i++) {
      j3pAffiche(ltab[0][i + 1], null, '&nbsp;$' + stor.couples[i].n + '$&nbsp;')
      j3pAffiche(ltab[1][i + 1], null, '&nbsp;$' + stor.couples[i].n2 + '$&nbsp;')
      ltab[0][i + 1].style.border = '1px solid black'
      ltab[1][i + 1].style.border = '1px solid black'
      ltab[0][i + 1].style.textAlign = 'center'
      ltab[1][i + 1].style.textAlign = 'center'
      ltab[0][i + 1].style.background = '#ffffff'
      ltab[1][i + 1].style.background = '#ffffff'
    }
    stor.tabAA = addDefaultTable(stor.lesdiv.travail, 3, 1)
    stor.tabfz = addDefaultTable(stor.tabAA[0][0], 1, 5)
    j3pAddTxt(stor.tabfz[0][1], '&nbsp;&nbsp;')
    j3pAddTxt(stor.tabfz[0][3], '&nbsp;&nbsp;')
    stor.cals = [false, false, false]
    faisbout1()
    stor.lesdiv.travail.classList.add('travail')
    stor.lesdiv.consigne.classList.add('enonce')
    stor.lesdiv.solution.classList.remove('correction')
    stor.lesdiv.explications.classList.remove('explique')
  }

  function faisbout1 () {
    j3pEmpty(stor.tabAA[2][0])
    j3pEmpty(stor.lesdiv.correction)
    j3pEmpty(stor.lesdiv.explications)
    const tgk = addDefaultTable(stor.tabAA[2][0], 1, 3)
    j3pAjouteBouton(tgk[0][0], 'bbb1df', '', 'Effectuer un calcul', effCal)
    j3pAffiche(tgk[0][1], null, '&nbsp;&nbsp;')
    if (!ds.Guide || stor.cals[1]) {
      j3pAjouteBouton(tgk[0][2], 'bbb1df2', '', 'Conclure', concl)
    }
  }
  function effCal () {
    const num = stor.cals.indexOf(false)
    if (num === -1 || (!stor.Lexo.cas && stor.bug < 2 && num === 2)) {
      stor.err = 'TropCalculs'
      me.sectionCourante()
      return
    }
    j3pEmpty(stor.tabAA[2][0])
    faisChoix(num)
  }
  function faisChoix (k) {
    let buf
    const tgk = addDefaultTable(stor.tabAA[2][0], 1, 3)
    j3pAffiche(tgk[0][0], null, 'Je calcule &nbsp;')
    j3pAjouteBouton(tgk[0][2], 'bbb1df', 'ok', 'ok', choixCal)
    let llli = []
    llli.push('\\frac{' + stor.couples[k].n2 + '}{' + stor.couples[k].n + '}')
    buf = stor.couples[k].n + ''
    if (buf[0] === '-') buf = '(' + buf + ')'
    llli.push(stor.couples[k].n2 + ' \\times ' + buf)
    llli.push(stor.couples[k].n2 + ' + ' + buf)
    llli.push(stor.couples[k].n2 + ' - ' + buf)
    llli = j3pShuffle(llli)
    llli.splice(0, 0, 'Choisir')
    llli = modif(llli)
    stor.listC = ListeDeroulante.create(tgk[0][1], llli, { centre: true })
    stor.listC.skl = k
  }
  function modif (tab) {
    const hh = j3pClone(tab)
    for (let i = 1; i < hh.length; i++) {
      hh[i] = '$' + hh[i] + '$'
    }
    return hh
  }
  function choixCal () {
    if (!stor.listC.changed) return
    if (stor.listC.reponse.indexOf('frac') === -1) {
      stor.err = 'MauvOp'
      me.sectionCourante()
      return
    }
    let buf2 = ' = '
    if (!stor.Lexo.cas && stor.bug === stor.listC.skl && !stor.egb) buf2 = ' \\approx '
    const buf = '$' + stor.listC.reponse.replace(/\$/g, '') + buf2 + stor.afCoef[stor.listC.skl] + '$'
    stor.listC.disable()
    j3pEmpty(stor.tabAA[2][0])
    j3pAffiche(stor.tabfz[0][stor.listC.skl * 2], null, buf)
    stor.cals[stor.listC.skl] = true
    faisbout1()
  }
  function concl () {
    const num = stor.cals.indexOf(false)
    if (num !== -1) {
      if (num !== 2 || stor.bug === 2 || stor.Lexo.cas) {
        stor.err = 'MankCal'
        me.sectionCourante()
        return
      }
    }
    j3pEmpty(stor.tabAA[2][0])
    faisConcl()
  }
  function faisConcl () {
    const tabgr = addDefaultTable(stor.tabAA[2][0], 2, 2)
    stor.tabhv = addDefaultTable(tabgr[1][0], 1, 4)
    let llli = ['Choisir', 'Tous les quotients sont égaux,', 'Les quotients ne sont pas tous égaux,']
    stor.liste = ListeDeroulante.create(tabgr[0][0], llli, { centre: true })
    j3pAffiche(stor.tabhv[0][0], null, 'donc ce tableau &nbsp;')
    j3pAffiche(stor.tabhv[0][2], null, ' &nbsp; une situation de proportionnalité.')
    llli = ['Choisir', 'représente', 'ne représente pas']
    stor.liste2 = ListeDeroulante.create(stor.tabhv[0][1], llli, { centre: true })
    j3pAjouteBouton(stor.tabhv[0][3], 'bbb1df', 'ok', 'ok', choixCgdfjkjklf)
  }
  function choixCgdfjkjklf () {
    if (!stor.liste.changed) {
      stor.liste.focus()
      return
    }
    if (!stor.liste2.changed) {
      stor.liste2.focus()
      return
    }
    let rep1 = stor.liste.getReponseIndex() === 1
    if (rep1 !== stor.Lexo.cas) {
      stor.err = 'Rep1'
      me.sectionCourante()
      return
    }
    rep1 = stor.liste2.getReponseIndex() === 1
    if (rep1 !== stor.Lexo.cas) {
      stor.err = 'Rep2'
      me.sectionCourante()
      return
    }
    stor.err = 'OK'
    me.sectionCourante()
  }
  function creeLesDiv () {
    stor.lesdiv = {}
    stor.lesdiv.conteneur = j3pAddElt(me.zones.MG, 'div', {
      style: me.styles.etendre('toutpetit.enonce', { padding: '10px' })
    })
    stor.lesdiv.consignet = addDefaultTable(stor.lesdiv.conteneur, 1, 1)[0][0]
    const ttab = addDefaultTable(stor.lesdiv.consignet, 1, 3)
    ttab[0][1].style.width = '40px'
    stor.lesdiv.consigne = ttab[0][0]
    stor.lesdiv.coboot = ttab[0][2]
    stor.lesdiv.travail1 = addDefaultTable(stor.lesdiv.conteneur, 1, 1)[0][0]
    const rtab = addDefaultTable(stor.lesdiv.travail1, 1, 3)
    rtab[0][1].style.width = '40px'
    stor.lesdiv.travail = rtab[0][0]
    stor.lesdiv.correction = rtab[0][2]
    stor.lesdiv.zonefig = j3pAddElt(stor.lesdiv.travail, 'div')
    stor.lesdiv.zonedrep = rtab[0][1]
    stor.lesdiv.explications = j3pAddElt(stor.lesdiv.conteneur, 'div')
    stor.lesdiv.solution = j3pAddElt(stor.lesdiv.conteneur, 'div')
    stor.lesdiv.explications.style.color = me.styles.cfaux
    stor.lesdiv.solution.style.color = me.styles.toutpetit.correction.color
  }

  function premsavec (k) {
    let ret
    do {
      ret = j3pGetRandomInt(2, 27)
    } while (j3pPGCD(ret, k, { negativesAllowed: true, valueIfZero: 1 }) !== 1)
    return ret
  }
  function yaReponse () {
    return true
  }

  function isRepOk () {
    return stor.err === 'OK'
  }

  function desactiveAll () {
    if (stor.err === 'TropCalculs' || stor.err === 'MankCal') {
      j3pDesactive('bbb1df')
      j3pDesactive('bbb1df2')
    }
    if (stor.err === 'MauvOp') {
      stor.listC.disable()
    }
    if (stor.err === 'Rep1' || stor.err === 'Rep2' || stor.err === 'OK') {
      stor.liste.disable()
      stor.liste2.disable()
      j3pEmpty(stor.tabhv[0][3])
    }
  }

  function barrelesfo () {
    if (stor.err === 'TropCalculs') {
      j3pBarre('bbb1df')
    }
    if (stor.err === 'MauvOp') {
      stor.listC.barre()
    }
    if (stor.err === 'MankCal') {
      j3pBarre('bbb1df2')
    }
    if (stor.err === 'Rep1') {
      stor.liste.barre()
    }
    if (stor.err === 'Rep2') {
      stor.liste2.barre()
    }
  }

  function mettouvert () {
    stor.liste.corrige(true)
    stor.liste2.corrige(true)
  }
  function AffCorrection (bool) {
    let i
    if (stor.err === 'TropCalculs') {
      stor.yaexplik = true
      j3pAffiche(stor.lesdiv.explications, null, 'Tu as déjà fait tous les calculs nécessaires pour conclure !')
    }
    if (stor.err === 'MauvOp') {
      stor.yaexplik = true
      j3pAffiche(stor.lesdiv.explications, null, 'Cette opération de t’aidera pas pour répondre à la question !')
      stor.listC.corrige(false)
    }
    if (stor.err === 'MankCal') {
      stor.yaexplik = true
      j3pAffiche(stor.lesdiv.explications, null, 'Tu n’as pas fait les calculs nécessaires pour justifier une réponse. !')
    }
    if (stor.err === 'Rep1') {
      stor.yaexplik = true
      j3pAffiche(stor.lesdiv.explications, null, 'Regarde bien les résultats des trois quotients !')
      stor.liste.corrige(false)
    }
    if (stor.err === 'Rep2') {
      stor.liste2.corrige(false)
    }

    if (bool) {
      barrelesfo()
      desactiveAll()

      for (i = 0; i < 3; i++) {
        if (!stor.Lexo.cas && stor.bug < 2 && i === 2) break
        let buf2 = ' = '
        if (!stor.Lexo.cas && stor.bug === i && !stor.egb) buf2 = ' \\approx '
        const buf = '$\\frac{' + stor.couples[i].n2 + '}{' + stor.couples[i].n + '} ' + buf2 + stor.afCoef[i] + '$ ;'
        j3pAffiche(stor.lesdiv.solution, null, buf)
      }
      let buf2 = '<br>Tous les quotients sont égaux, <br> donc ce tableau représente une situation de proportionnalité.'
      if (!stor.Lexo.cas) buf2 = '<br>Les quotients ne sont pas tous égaux, <br> donc ce tableau ne représente pas une situation de proportionnalité.'
      j3pAffiche(stor.lesdiv.solution, null, buf2)
    } else {
      me.cacheBoutonSuite()
    }
  }

  function enonceMain () {
    if (((me.questionCourante % ds.nbetapes) === 1) || (ds.nbetapes === 1)) {
      // A chaque répétition, on nettoie la scène
      // A commenter si besoin
      me.videLesZones()
      creeLesDiv()
      stor.Lexo = faisChoixExos()
      if (ds.theme === 'zonesAvecImageDeFond') {
        stor.lesdiv.travail.classList.add('travail')
        stor.lesdiv.consigne.classList.add('enonce')
        stor.lesdiv.solution.classList.remove('correction')
        stor.lesdiv.explications.classList.remove('explique')
      }
    }

    poseQuestion()
    me.cacheBoutonSuite()
    me.finEnonce()
    me.cacheBoutonValider()
  }

  switch (this.etat) {
    case 'enonce':
      // code exécuté au lancement de la section
      if (me.debutDeLaSection) {
        initSection()
      } else {
        enonceMain()
      }
      break // case "enonce":

    case 'correction':
      // On teste si une réponse a été saisie
      stor.yaexplik = false
      stor.lesdiv.explications.innerHTML = ''
      stor.lesdiv.correction.innerHTML = ''
      stor.yaco = false

      if (yaReponse()) {
        // Bonne réponse
        if (isRepOk()) {
          this._stopTimer()
          stor.lesdiv.correction.style.color = this.styles.cbien
          stor.lesdiv.correction.innerHTML = cBien

          desactiveAll()
          mettouvert()
          this.cacheBoutonValider()
          this.score++

          this.typederreurs[0]++
          this.etat = 'navigation'
          this.sectionCourante()
        } else {
          // Pas de bonne réponse
          stor.lesdiv.correction.style.color = this.styles.cfaux
          stor.lesdiv.correction.innerHTML = cFaux
          if (this.isElapsed) {
            // A cause de la limite de temps :
            this._stopTimer()

            stor.lesdiv.correction.innerHTML += tempsDepasse + '<BR>'
            this.typederreurs[10]++
            this.cacheBoutonValider()

            AffCorrection(true)

            this.etat = 'navigation'
            this.sectionCourante()
          } else {
            // Réponse fausse :
            if (this.essaiCourant < ds.nbchances) {
              stor.lesdiv.correction.innerHTML += '<br>' + essaieEncore

              AffCorrection(false)

              this.typederreurs[1]++
            } else {
              // Erreur au nème essai
              this._stopTimer()
              this.cacheBoutonValider()

              AffCorrection(true)

              if (stor.encours === 'p') {
                stor.pose = false
                stor.encours = 't'
              }

              this.typederreurs[2]++
              this.etat = 'navigation'
              this.sectionCourante()
            }
          }
        }
      } else {
        // pas de réponse
        stor.lesdiv.correction.style.color = this.styles.cfaux
        stor.lesdiv.correction.innerHTML = reponseIncomplete
        if (stor.encours === 'm') {
          stor.lesdiv.correction.innerHTML = 'Réponse incomplète ! <br> Tu dois déplacer le point ' + stor.C + ' .'
        }
        me.afficheBoutonValider()
      }

      if (stor.yaexplik) stor.lesdiv.explications.classList.add('explique')
      if (stor.yaco) stor.lesdiv.solution.classList.add('correction')

      // Obligatoire
      this.finCorrection()
      break // case "correction":

    case 'navigation':
      if (!this.sectionTerminee()) {
        this.etat = 'enonce'
      }
      this.finNavigation()
      break // case "navigation":
  }
}
