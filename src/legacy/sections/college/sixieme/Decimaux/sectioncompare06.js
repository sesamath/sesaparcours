import { j3pAddElt, j3pAjouteBouton, j3pArrondi, j3pEmpty, j3pGetNewId, j3pGetRandomBool, j3pGetRandomInt, j3pShuffle } from 'src/legacy/core/functions'
import Etiquettes from 'src/legacy/outils/etiquette/Etiquettes'
import PlaceEtiquettes from 'src/legacy/outils/etiquette/PlaceEtiquettes'
import { j3pAffiche } from 'src/lib/mathquill/functions'
import { addDefaultTable } from 'src/legacy/themes/table'
import { ecrisBienMathquill } from 'src/legacy/outils/zoneStyleMathquill/functions'
import textesGeneriques from 'src/lib/core/textes'

const { cBien, cFaux, essaieEncore, reponseIncomplete, tempsDepasse } = textesGeneriques

/**
 * Ajouter une description ici
 * @fileOverview
 * @author Tommy Barroy <t.barroy@laposte.net>
 * @since 2017-10
 */
export const params = {
  outils: ['mathquill'],
  /**
   * Ce qui est paramétrable dans l’éditeur de graphe
   * @type {LegacySectionParamsParametres}
   */
  parametres: [
    ['nbrepetitions', 9, 'entier', 'Nombre de répétitions de la section'],
    ['indication', '', 'string', 'Indication pour chaque répétition'],
    ['limite', 0, 'entier', 'Temps disponible par répétition, en secondes'],
    ['nbchances', 1, 'entier', 'Nombre d’essais par répétition'],
    ['RangMax', 'milliers', 'liste', 'approximation max', ['milliers', 'centaines', 'dizaines', 'unités', 'dixièmes', 'centièmes', 'millièmes']],
    ['RangMin', 'millièmes', 'liste', 'approximation min', ['milliers', 'centaines', 'dizaines', 'unités', 'dixièmes', 'centièmes', 'millièmes']],
    ['Nombre', 3, 'entier', 'Nombre de valeurs à ranger. (entre 3 et 6)'],
    ['Negatifs', 'parfois', 'liste', 'Nombres négatifs possibles ', ['toujours', 'parfois', 'jamais']],
    ['Ordre', 'les deux', 'liste', 'Ordre décroissant demandé', ['les deux', 'croissant', 'décroissant']],
    ['theme', 'standard', 'liste', 'Modèle pour l’affichage', ['standard', 'zonesAvecImageDeFond']]

  ],
  // dans le cas d’une section QUANTITATIVE
  pe: [
    { pe_1: '' }
  ]
}

/**
 * section compare06
 * @this {Parcours}
 */
export default function main () {
  const me = this
  let ds = me.donneesSection
  const stor = me.storage

  function getDonnees () {
    return {
      // Nombre de répétitions de l’exercice
      nbrepetitions: 3,

      RangMax: 'milliers',
      RangMin: 'millièmes',
      Nombre: 3,
      Negatifs: 'parfois',
      Ordre: 'les deux',

      // Si indication != "" alors un lien "Indication" s’affiche dans la zone Inférieure Gauche (this.zones.IG
      // une indication<br>de deux lignes..."
      indication: '',

      // pour le cas de presentation1bis
      // En effet pas d’indication possible pour cette présentation
      // surchargeindication: false,

      // Nombre de chances dont dispose l’él§ve pour répondre à la question
      nbchances: 1,

      //  || "presentation2" || "presentation3"  || "presentation1bis"
      structure: 'presentation3',

      // Paramètres de la section, avec leurs valeurs par défaut.

      /*
          Les textes présents dans la section
          Sont donc accessibles dans le code par me.donneesSection.textes.phrase1
          Possibilité d’écrire dans le code :
          var lestextes = me.donneesSection.textes
          puis accès à l’aide de lestextes.phrase1
          */
      textes: {
        etape: '<u>Étape £a</u>: £b.',
        regardecp: 'Regarde la correction',
        choisir: 'Choisir'
        // faux : "C’est faux"
      }, // fin textes

      // Par défaut, pas de temps limite. Décommentez pour avoir un temps limite par défaut. En secondes.

      // limite: 10,
      /*
          Phrase d’état renvoyée par la section
          Si this.pe=0 alors la section est quantitative et renvoie un score entre 0 et 1.
          Dans le cas où la section est qualitative, lister les différents pe renvoyées :
              pe_1: "toto"
              this.pe_2 = "tata"
              etc
          Dans la partie navigation, il faudra affecter la variable this.parcours.pe à l’une des précédentes.
          Par exemple,
          this.parcours.pe = me.donneesSection.pe_2
          */
      pe_1: ''
    }
  }
  function initSection () {
    let i
    ds = getDonnees()
    me.donneesSection = ds
    // On surcharge avec les parametres passés par le graphe
    me.surcharge()
    me.validOnEnter = false // ex donneesSection.touche_entree
    // Construction de la page
    me.construitStructurePage(me.donneesSection.structure)
    me.donneesSection.nbetapes = 1
    let lp = []
    if ((ds.Negatifs === 'parfois') || (ds.Negatifs === 'jamais')) {
      lp.push(false)
    }
    if ((ds.Negatifs === 'parfois') || (ds.Negatifs === 'toujours')) {
      lp.push(true)
    }
    me.donneesSection.nbitems = me.donneesSection.nbetapes * me.donneesSection.nbrepetitions
    ds.lesExos = []
    lp = j3pShuffle(lp)
    for (i = 0; i < ds.nbitems; i++) {
      ds.lesExos[i] = { neg: lp[i % lp.length] }
    }
    ds.lesExos = j3pShuffle(ds.lesExos)

    lp = []
    if ((ds.Ordre === 'les deux') || (ds.Ordre === 'croissant')) {
      lp.push(' croissant. ')
    }
    if ((ds.Ordre === 'les deux') || (ds.Ordre === 'décroissant')) {
      lp.push(' décroissant. ')
    }
    for (i = 0; i < ds.nbitems; i++) {
      ds.lesExos[i].ordre = lp[i % lp.length]
    }
    ds.lesExos = j3pShuffle(ds.lesExos)

    const listeRang = ['milliers', 'centaines', 'dizaines', 'unités', 'dixièmes', 'centièmes', 'millièmes']
    stor.Rmax = listeRang.indexOf(ds.RangMax)
    stor.Rmin = listeRang.indexOf(ds.RangMin)
    if (stor.Rmax === -1) stor.Rmax = 0
    if (stor.Rmin === -1) stor.Rmin = 6
    if (stor.Rmax > stor.Rmin) stor.Rmax = stor.Rmin

    if (isNaN(ds.Nombre)) ds.Nombre = 3
    ds.Nombre = parseInt(ds.Nombre)
    ds.Nombre = Math.max(3, Math.min(ds.Nombre, 6))

    me.afficheTitre('Ranger')
    enonceMain()
  }
  function faisChoixExos () {
    const e = ds.lesExos.pop()
    stor.lenbBAse = j3pArrondi(j3pGetRandomInt(1, 99000000) / 1000, stor.Rmin - 3)
    stor.listNombre = []
    const randDiff = stor.Rmin - stor.Rmax
    let lp = []
    for (let i = 0; i < randDiff + 1; i++) lp.push(i)
    lp = j3pShuffle(lp)
    let nb
    let cmpt
    for (let i = 0; i < ds.Nombre; i++) {
      const dev = lp[i % lp.length]
      cmpt = 0
      do {
        cmpt++
        let dec = j3pGetRandomInt(1, 9) * Math.pow(10, 3 - stor.Rmin + dev)
        if (j3pGetRandomBool()) dec = -dec
        nb = j3pArrondi(stor.lenbBAse + dec, stor.Rmin - 3)
        if (!e.neg) nb = Math.abs(nb)
      } while (stor.listNombre.indexOf(nb) !== -1 && cmpt < 100)
      stor.listNombre.push(nb)
    }
    if (e.neg) {
      for (let i = 0; i < stor.listNombre.length; i++) {
        if (j3pGetRandomBool()) stor.listNombre[i] = -stor.listNombre[i]
      }
    }
    stor.ordre = e.ordre
    stor.lesnb = []
    stor.listNombre = j3pShuffle(stor.listNombre)
    for (let i = 0; i < stor.listNombre.length; i++) {
      stor.lesnb.push('$' + ecrisBienMathquill(stor.listNombre[i]) + '$')
    }
  }
  function poseQuestion () {
    stor.taBG = addDefaultTable(stor.lesdiv.consigne, 2, 1)
    j3pAffiche(stor.taBG[0][0], null, '<b>Range les nombres suivants dans l’ordre ' + stor.ordre + '</b>\n<i>Change les signes si nécessaire</i>.\n')
    stor.taBG2 = addDefaultTable(stor.taBG[1][0], 2, 1)
    stor.taBG2[0][0].style.textAlign = 'center'
    j3pAffiche(stor.taBG2[0][0], null, '<i>Les nombres</i>')
    j3pAffiche(stor.lesdiv.consigne, null, '\n')
    stor.taBG2[1][0].style.border = '1px solid black'
    stor.taBG2[1][0].style.padding = '5px'
    stor.lesetik = new Etiquettes(stor.taBG2[1][0], 'fdshkjd', stor.lesnb, { centre: true, reutilisable: false, dispo: 'fixe', ligne: 1 })
    stor.tab2 = addDefaultTable(stor.lesdiv.travail, 3, 1)
    stor.tab2[1][0].style.height = '20px'
    stor.tabSymb = addDefaultTable(stor.tab2[2][0], 1, 2 * stor.lesnb.length - 1)
    stor.listPla = []
    for (let i = 0; i < stor.lesnb.length; i++) {
      stor.tabSymb[0][i * 2].id = j3pGetNewId()
      stor.listPla.push(new PlaceEtiquettes(stor.tabSymb[0][i * 2].id, 'kdldm' + i, stor.lesetik, {}))
    }
    stor.sens = false
    faisDec()
    stor.tab2[2][0].style.textAlign = 'center'
    j3pAjouteBouton(stor.tab2[1][0], faisDec, { value: 'Changer le sens' })
  }
  function faisDec () {
    stor.sens = !stor.sens
    const buf = stor.sens ? '>' : '<'
    for (let i = 0; i < stor.lesnb.length - 1; i++) {
      j3pEmpty(stor.tabSymb[0][1 + 2 * i])
      j3pAffiche(stor.tabSymb[0][1 + 2 * i], null, '&nbsp;$' + buf + '$&nbsp;')
    }
  }
  function creeLesDiv () {
    stor.lesdiv = {}
    stor.lesdiv.conteneur = j3pAddElt(me.zonesElts.MG, 'div', {
      style: me.styles.etendre('toutpetit.enonce', { padding: '10px' })
    })
    stor.lesdiv.conteneur1 = addDefaultTable(stor.lesdiv.conteneur, 1, 1)[0][0]
    stor.lesdiv.conteneur2 = addDefaultTable(stor.lesdiv.conteneur, 1, 1)[0][0]
    stor.lesdiv.conteneur3 = addDefaultTable(stor.lesdiv.conteneur1, 1, 3)
    stor.lesdiv.consigne = stor.lesdiv.conteneur3[0][0]
    stor.lesdiv.conteneur3[0][1].style.width = '40px'
    stor.lesdiv.travail = j3pAddElt(stor.lesdiv.conteneur2, 'div')
    stor.lesdiv.rep = j3pAddElt(stor.lesdiv.travail, 'div')
    stor.lesdiv.explications = j3pAddElt(stor.lesdiv.conteneur, 'div')
    stor.lesdiv.solution = j3pAddElt(stor.lesdiv.conteneur, 'div')
    stor.lesdiv.correction = stor.lesdiv.conteneur3[0][2]
    stor.lesdiv.explications.style.color = me.styles.cfaux
    stor.lesdiv.solution.style.color = me.styles.toutpetit.correction.color
    me.zonesElts.MG.classList.add('fond')
  }

  function yaReponse () {
    if (me.isElapsed) return true
    for (let i = 0; i < stor.listPla.length; i++) {
      if (stor.listPla[i].contenu === '') {
        return false
      }
    }
    return true
  }
  function isRepOk () {
    stor.errDen1 = false
    stor.errTout = false
    stor.errComp = []
    passetoutvert()
    stor.elRep = []
    let ok = true
    for (let i = 0; i < stor.listPla.length; i++) {
      let rep = stor.listPla[i].contenu
      while (rep.indexOf('\\text{ }') !== -1) {
        rep = rep.replace('\\text{ }', '')
      }
      while (rep.indexOf('$') !== -1) {
        rep = rep.replace('$', '')
      }
      rep = rep.replace(',', '.')
      if (rep === '') {
        stor.errTout = true
        ok = false
        stor.listPla[i].corrige(false)
      } else {
        rep = parseFloat(rep)
      }
      stor.elRep.push(rep)
    }
    stor.errOrdre = stor.ordre === ' croissant. ' ? stor.sens : !stor.sens
    for (let i = 0; i < stor.elRep.length - 1; i++) {
      if (stor.elRep[i] === '' || stor.elRep[i + 1] === '') continue
      if (((stor.elRep[i + 1] - stor.elRep[i]) < 0) !== stor.sens) {
        stor.errComp.push(i)
        ok = false
        stor.listPla[i].corrige(false)
        stor.listPla[i + 1].corrige(false)
      }
    }
    return ok && !stor.errOrdre
  }
  function desactivezone () {
    for (let i = 0; i < stor.listPla.length; i++) stor.listPla[i].disable()
    j3pEmpty(stor.taBG[1][0])
    j3pEmpty(stor.tab2[1][0])
  }
  function passetoutvert () {
    for (let i = 0; i < stor.listPla.length; i++) {
      stor.listPla[i].corrige(true)
    }
  }
  function barrelesfo () {
    for (let i = 0; i < stor.listPla.length; i++) {
      stor.listPla[i].barreIfKo()
    }
  }
  function AffCorrection (bool) {
    if (stor.errOrdre) {
      stor.yaexplik = true
      j3pAffiche(stor.lesdiv.explications, null, 'Tu ranges les nombres dans le mauvais ordre !\n')
    }
    if (stor.errTout) {
      stor.yaexplik = true
      j3pAffiche(stor.lesdiv.explications, null, 'Tous les nombres doivent être rangés ! \n')
    }
    if (stor.errComp.length > 0) {
      stor.yaexplik = true
      j3pAffiche(stor.lesdiv.explications, null, 'Des nombres sont mal rangés !\n')
    }

    if (bool) {
      stor.yaco = true
      desactivezone()
      barrelesfo()
      const func = (stor.ordre === ' croissant. ') ? function (a, b) { return a - b } : function (a, b) { return b - a }
      stor.listNombre = stor.listNombre.sort(func)
      let buf = ecrisBienMathquill(stor.listNombre[0])
      const comp = (stor.ordre === ' croissant. ') ? ' < ' : ' > '
      for (let i = 1; i < stor.listNombre.length; i++) {
        buf += comp + ecrisBienMathquill(stor.listNombre[i])
      }
      j3pAffiche(stor.lesdiv.solution, null, '$' + buf + '$')
    } else { me.afficheBoutonValider() }
  }
  function enonceMain () {
    if (me.etapeCourante === 1) {
      me.videLesZones()
      creeLesDiv()
      faisChoixExos()
      stor.lesdiv.travail.classList.add('travail')
      stor.lesdiv.consigne.classList.add('enonce')
    }
    poseQuestion()
    me.finEnonce()
  }

  switch (this.etat) {
    case 'enonce':
      // code exécuté au lancement de la section
      if (me.debutDeLaSection) {
        initSection()
      } else {
        enonceMain()
      }
      break // case "enonce":

    case 'correction':
      // On teste si une réponse a été saisie
      stor.yaexplik = false
      stor.lesdiv.explications.innerHTML = ''
      stor.lesdiv.correction.innerHTML = ''
      stor.yaco = false

      if (yaReponse()) {
        // Bonne réponse
        if (isRepOk()) {
          this._stopTimer()
          this.score++
          stor.lesdiv.correction.style.color = this.styles.cbien
          stor.lesdiv.correction.innerHTML = cBien

          desactivezone()
          passetoutvert()

          this.typederreurs[0]++
          this.cacheBoutonValider()
          this.etat = 'navigation'
          this.sectionCourante()
        } else {
          // Pas de bonne réponse
          stor.lesdiv.correction.style.color = this.styles.cfaux
          stor.lesdiv.correction.innerHTML = cFaux
          if (this.isElapsed) {
            // A cause de la limite de temps :
            this._stopTimer()

            stor.lesdiv.correction.innerHTML += tempsDepasse + '<BR>'
            this.typederreurs[10]++
            this.cacheBoutonValider()

            AffCorrection(true)

            this.etat = 'navigation'
            this.sectionCourante()
          } else {
            // Réponse fausse :
            if (me.essaiCourant < me.donneesSection.nbchances) {
              stor.lesdiv.correction.innerHTML += '<br>' + essaieEncore

              AffCorrection(false)

              this.typederreurs[1]++
            } else {
              // Erreur au nème essai
              this._stopTimer()
              this.cacheBoutonValider()

              AffCorrection(true)

              this.typederreurs[2]++
              this.etat = 'navigation'
              this.sectionCourante()
            }
          }
        }
      } else {
        // pas de réponse
        stor.lesdiv.correction.style.color = this.styles.cfaux
        stor.lesdiv.correction.innerHTML = reponseIncomplete
        me.afficheBoutonValider()
      }

      if (stor.yaexplik) stor.lesdiv.explications.classList.add('explique')
      if (stor.yaco) stor.lesdiv.solution.classList.add('correction')

      // Obligatoire
      this.finCorrection()
      break // case "correction":

    case 'navigation':

      if (this.sectionTerminee()) {
        // On détermine la phrase d’état renvoyée par la section
        this.parcours.pe = ''
        me.afficheBoutonSectionSuivante()
        this.focus('sectioncontinuer')
      } else {
        this.etat = 'enonce'
        me.afficheBoutonSuite()
        this.focus('boutoncontinuer')
      }
      // Obligatoire
      this.finNavigation()
      break // case "navigation":
  }
}
