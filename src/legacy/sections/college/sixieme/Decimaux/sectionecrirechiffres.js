import { j3pAddClass, j3pAddElt, j3pAjouteBouton, j3pDetruit, j3pGetRandomInt, j3pRemoveClass } from 'src/legacy/core/functions'

import { verifNombreBienEcrit } from 'src/legacy/outils/fonctions/ecritureNombre'
import ZoneStyleMathquill1 from 'src/legacy/outils/zoneStyleMathquill/ZoneStyleMathquill1'
import { addDefaultTable, addDefaultTableDetailed } from 'src/legacy/themes/table'
import { j3pAffiche } from 'src/lib/mathquill/functions'
import textesGeneriques from 'src/lib/core/textes'
import { nombreEnMots } from 'src/lib/outils/conversion/nombreEnMots'

import imgCentieme from './centieme.png'
import imgDixieme from './dixieme.png'
import imgDixMilieme from './dixmillieme.png'
import imgMilieme from './millieme.png'

import './ecrirechiffres.css'

const { cBien, cFaux, essaieEncore, regardeCorrection, reponseIncomplete, tempsDepasse } = textesGeneriques

/**
 * Ajouter une description ici
 * @fileOverview
 * @author Tommy Barroy <t.barroy@laposte.net>
 * @since 2017-10
 */
export const params = {
  outils: ['mathquill'],
  /**
   * Ce qui est paramétrable dans l’éditeur de graphe
   * @type {LegacySectionParamsParametres}
   */
  parametres: [
    ['nbrepetitions', 9, 'entier', 'Nombre de répétitions de la section'],
    ['nbchances', 2, 'entier', 'Nombre de tentatives possibles.'],
    ['indication', '', 'string', 'Indication pour chaque répétition'],
    ['limite', 0, 'entier', 'Temps disponible par répétition, en secondes'],
    ['Espace', true, 'boolean', '<u>true</u>: Les espaces entre chaque groupe sont exigés.'],
    ['Partie', 'Entière_Décimale', 'liste', '<u>Entière</u>: Le travail porte sur la partie entière. <BR><BR><u>Décimale</u>: Le travail porte sur la partie décimale. ', ['Entière', 'Décimale', 'Entière_Décimale']],
    ['Rang_max_Entier', 'milliard', 'liste', '<u>milliard</u>: Le nombre ne peut pas dépasser 999 999 999 999 <BR><BR><u>million</u>: Le nombre ne peut pas dépasser 999 999 999. <BR><BR><u>mille</u>: Le nombre ne peut pas dépasser 999 999. <BR><BR><u>unité</u>: Le nombre ne peut pas dépasser 999. ', ['milliard', 'million', 'mille', 'unité']],
    ['Rang_max_Decim', 'millième', 'liste', '<u>millième</u>: La précision est inférieure ou égale au millième <BR><BR><u>centième</u>: La précision est inférieure ou égale au centième. <BR><BR><u>dixième</u>: La précision est au dixième. <BR><BR><u>dix-millième</u>: La précision est inférieure ou égale au dix-millième. ', ['dix-millième', 'millième', 'centième', 'dixième']],
    ['Aide', true, 'boolean', '<u>true</u>: L’élève dispose d’un tableau pour s’aider.'],
    // le modèle gère la compatibilité ascendante avec l’ancien paramètre MiseEnPagePlusClaire
    ['theme', 'standard', 'liste', 'Modèle pour l’affichage', ['standard', 'zonesAvecImageDeFond']]

  ],
  // dans le cas d’une section QUANTITATIVE
  pe: [
    { pe_1: 'Erreur Espaces' },
    { pe_2: 'Erreur Virgule' },
    { pe_3: 'Erreur Rang' },
    { pe_4: 'Erreur Groupe de 3' },
    { pe_5: 'Erreur zéros inutiles' }
  ]
}

const textes = {
  aide: 'Attention !',
  aidezero: 'Clique sur un zéro pour le supprimer.',
  aideesp: 'Clique sur une espace par la faire disparaître. <BR> Clique sous deux chiffres pour insérer une espace.',
  soluce: 'Il fallait écrire $£a$ .',
  AttVirg: 'Tu ne peux pas supprimer la virgule, <BR> Il reste des chiffres dans la partie décimale !',
  consigne: '<b>Écris en chiffres ce nombre&nbsp;:</b> <BR> £a.',
  consigne3: ' $N: £a$ <BR><BR><BR>',
  consigne2bis: 'Ecris correctement ce nombre: $£a$'
}

/**
 * section ecrirechiffres
 * @this {Parcours}
 */
export default function main () {
  const me = this
  const stor = this.storage
  const ds = this.donneesSection

  function yareponse () {
    if (me.isElapsed) { return true }
    if (stor.mazone.reponse() === '') {
      stor.mazone.focus()
      return false
    }

    return true
  }

  function bonneReponse () {
    // FIXME faut pas de irregular-whitespace dans le code
    // eslint-disable-next-line no-irregular-whitespace
    stor.repEleve = stor.mazone.reponse().replace(/ /g, ' ')
    if (stor.repEleve === '') {
      stor.mazone.majaffiche('0')
      stor.repEleve = '0'
    }

    stor.test = verifNombreBienEcrit(stor.repEleve, ds.Espace, true)
    if (!stor.test.good) {
      return false
    }
    // le nb est correctement formaté mais pas forcément le bon
    return Math.abs(stor.test.nb - stor.nb) < 1e-5
  }

  function afcofo (boolfin) {
    if (boolfin) {
      if (document.getElementById('boton3') !== null) {
        j3pDetruit('boton3')
      }
      stor.mazone.barre()
      stor.mazone.disable()
      stor.lesdiv.trav2.style.display = ''
      desactivetab()
      corrigeTabGenerique()
    }

    stor.lesdiv.explications.innerHTML = ''
    stor.mazone.corrige(false)

    if (stor.test?.remede) {
      stor.lesdiv.explications.classList.add('explique')
      j3pAffiche(stor.lesdiv.explications, null, stor.test.remede)
      // typederreurs utilisé pour le calcul de la pe
      if (['espaceEnt', 'espaceDec', 'espaceDebDec', 'espaceFinEnt'].includes(stor.test.erreur)) {
        me.typederreurs[6]++
      } else if (['zeroInutile', 'espaceDouble'].includes(stor.test.erreur)) {
        // utilisé pour le calcul de la pe
        me.typederreurs[3]++
      }
    }

    if (boolfin) {
      stor.partieentiereDeb = stor.partieent + ''
      let sol1 = ''
      let ok = false
      for (let i = 0; i < stor.partieentiereDeb.length; i++) {
        if (stor.partieentiereDeb[i] !== ' ') {
          if (stor.partieentiereDeb[i] !== '0') ok = true
          if (ok) sol1 += stor.partieentiereDeb[i]
        }
      }
      if (sol1.length > 3) {
        sol1 = sol1.substring(0, sol1.length - 3) + ' ' + sol1.substring(sol1.length - 3)
      }
      if (sol1.length > 7) {
        sol1 = sol1.substring(0, sol1.length - 7) + ' ' + sol1.substring(sol1.length - 7)
      }
      if (sol1.length > 11) {
        sol1 = sol1.substring(0, sol1.length - 11) + ' ' + sol1.substring(sol1.length - 11)
      }
      if (sol1.length > 15) {
        sol1 = sol1.substring(0, sol1.length - 15) + ' ' + sol1.substring(sol1.length - 15)
      }

      let sol2 = ''
      ok = false
      stor.partiedecimaleDeb = stor.partiedecfin + ''
      for (let i = stor.partiedecimaleDeb.length - 1; i > -1; i--) {
        if (stor.partiedecimaleDeb[i] !== ' ') {
          if (stor.partiedecimaleDeb[i] !== '0') ok = true
          if (ok) sol2 = stor.partiedecimaleDeb[i] + sol2
        }
      }

      if (sol2 === 'undefined') { sol2 = '' }
      if (sol2.length > 3) {
        sol2 = sol2.substring(0, 3) + ' ' + sol2.substring(3)
      }
      if (sol2.length > 7) {
        sol2 = sol2.substring(0, 7) + ' ' + sol2.substring(7)
      }
      if (sol2.length > 11) {
        sol2 = sol2.substring(0, 11) + ' ' + sol2.substring(11)
      }
      if (sol2.length > 15) {
        sol2 = sol2.substring(0, 15) + ' ' + sol2.substring(15)
      }
      let sol3 = ''
      if (sol2.length > 0) { sol3 = ',' }
      if (sol1 === '') { sol1 = '0' }
      let rep = sol1 + sol3 + sol2
      rep = rep.replace(/ /g, '\\text{ }')
      stor.lesdiv.solution.classList.add('correction')
      stor.lesdiv.trav2.classList.add('explique')
      j3pAffiche(stor.lesdiv.solution, null, textes.soluce, { a: rep })
    }
  }

  function desactivetab () {
    if (ds.Partie.indexOf('Entière') !== -1) {
      if ((ds.Rang_max_Entier === 'milliard') && (ds.Partie.indexOf('Entière') !== -1)) {
        stor.aidemilliard.desactive()
      }
      if ((['milliard', 'million'].indexOf(ds.Rang_max_Entier) !== -1) && (ds.Partie.indexOf('Entière') !== -1)) {
        stor.aidemillion.desactive()
      }
      if ((ds.Rang_max_Entier !== 'unité') && (ds.Partie.indexOf('Entière') !== -1)) {
        stor.aidemille.desactive()
      }
    }
    stor.aideunite.desactive()
    if (ds.Partie.indexOf('Décimale') !== -1) {
      stor.aidedec.desactive()
    }
  }

  function corrigeTabGenerique () {
    if (ds.Partie.indexOf('Entière') !== -1) {
      if ((ds.Rang_max_Entier === 'milliard') && (ds.Partie.indexOf('Entière') !== -1)) {
        stor.aidemilliard.corrigetab()
      }
      if ((['milliard', 'million'].indexOf(ds.Rang_max_Entier) !== -1) && (ds.Partie.indexOf('Entière') !== -1)) {
        stor.aidemillion.corrigetab()
      }
      if ((ds.Rang_max_Entier !== 'unité') && (ds.Partie.indexOf('Entière') !== -1)) {
        stor.aidemille.corrigetab()
      }
    }
    stor.aideunite.corrigetab()
    if (ds.Partie.indexOf('Décimale') !== -1) {
      stor.aidedec.corrigetab()
    }
  }

  /**
   * à décrire
   * @param {string} b milliard|million|mille
   * @constructor
   */
  function Ref (b) {
    let dou
    switch (b) {
      case 'milliard':
        dou = stor.laide.miliard
        break
      case 'million':
        dou = stor.laide.millions
        break
      case 'mille':
        dou = stor.laide.mille
        break
      case 'unite':
        dou = stor.laide.unie
    }
    this.mescase = addDefaultTableDetailed(dou, 2, 7)
    for (let i = 0; i < 2; i++) {
      for (let j = 0; j < 7; j++) this.mescase.cells[i][j].style.padding = 0
    }
    j3pDetruit(this.mescase.cells[0][3])
    j3pDetruit(this.mescase.cells[0][4])
    j3pDetruit(this.mescase.cells[0][5])
    j3pDetruit(this.mescase.cells[0][6])
    this.mescase.cells[0][0].setAttribute('colspan', 2)
    this.mescase.cells[0][1].setAttribute('colspan', 3)
    this.mescase.cells[0][2].setAttribute('colspan', 2)
    this.mescase.cells[0][0].style.border = '1px solid black'
    this.mescase.cells[0][1].style.border = '1px solid black'
    this.mescase.cells[0][2].style.border = '1px solid black'
    j3pAffiche(this.mescase.cells[0][0], null, '&nbsp;C&nbsp;')
    j3pAffiche(this.mescase.cells[0][1], null, '&nbsp;D&nbsp;')
    j3pAffiche(this.mescase.cells[0][2], null, '&nbsp;U&nbsp;')
    j3pAffiche(this.mescase.cells[1][1], null, '&nbsp;')
    j3pAffiche(this.mescase.cells[1][3], null, '&nbsp;')
    j3pAffiche(this.mescase.cells[1][5], null, '&nbsp;')
    this.mescase.cells[1][0].classList.add('Aidegauche')
    this.mescase.cells[1][1].classList.add('Aidemilieu')
    this.mescase.cells[1][2].classList.add('Aidemilieu')
    this.mescase.cells[1][3].classList.add('Aidemilieu')
    this.mescase.cells[1][4].classList.add('Aidemilieu')
    this.mescase.cells[1][5].classList.add('Aidemilieu')
    this.mescase.cells[1][6].classList.add('Aidedroite')

    this.mescase.cells[1][0].style.textAlign = 'center'
    this.mescase.cells[1][1].style.textAlign = 'center'
    this.mescase.cells[1][2].style.textAlign = 'center'
    this.mescase.cells[1][3].style.textAlign = 'center'
    this.mescase.cells[1][4].style.textAlign = 'center'
    this.mescase.cells[1][5].style.textAlign = 'center'
    this.mescase.cells[1][6].style.textAlign = 'center'

    this.mescase.cells[1][0].setAttribute('contenteditable', true)
    this.mescase.cells[1][2].setAttribute('contenteditable', true)
    this.mescase.cells[1][4].setAttribute('contenteditable', true)
    this.mescase.cells[1][6].setAttribute('contenteditable', true)
    this.mescase.cells[1][0].style.width = '2px'
    this.mescase.cells[1][2].style.width = '2px'
    this.mescase.cells[1][4].style.width = '2px'
    this.mescase.cells[1][6].style.width = '2px'
    this.b = b
    this.cont = ['', '', '']
    const objet = this
    const gereListener = this.gere.bind(this)
    const gereVireListener = this.gereVire.bind(this)
    const gereClickListener = function () { objet.gereClick(this, objet) }
    this.mescase.cells[1][0].addEventListener('keydown', gereListener, false)
    this.mescase.cells[1][2].addEventListener('keydown', gereListener, false)
    this.mescase.cells[1][4].addEventListener('keydown', gereListener, false)
    this.mescase.cells[1][6].addEventListener('keydown', gereListener, false)

    this.mescase.cells[1][0].addEventListener('keypress', gereVireListener, false)
    this.mescase.cells[1][2].addEventListener('keypress', gereVireListener, false)
    this.mescase.cells[1][4].addEventListener('keypress', gereVireListener, false)
    this.mescase.cells[1][6].addEventListener('keypress', gereVireListener, false)

    this.mescase.cells[1][0].num = 0
    this.mescase.cells[1][2].num = 2
    this.mescase.cells[1][4].num = 4
    this.mescase.cells[1][6].num = 6

    this.mescase.cells[1][0].addEventListener('keyup', gereVireListener, false)
    this.mescase.cells[1][2].addEventListener('keyup', gereVireListener, false)
    this.mescase.cells[1][4].addEventListener('keyup', gereVireListener, false)
    this.mescase.cells[1][6].addEventListener('keyup', gereVireListener, false)

    this.mescase.cells[1][0].addEventListener('click', gereClickListener, false)
    this.mescase.cells[1][1].addEventListener('click', gereClickListener, false)
    this.mescase.cells[1][2].addEventListener('click', gereClickListener, false)
    this.mescase.cells[1][3].addEventListener('click', gereClickListener, false)
    this.mescase.cells[1][4].addEventListener('click', gereClickListener, false)
    this.mescase.cells[1][5].addEventListener('click', gereClickListener, false)
    this.mescase.cells[1][6].addEventListener('click', gereClickListener, false)

    this.mescase.cells[1][1].style.cursor = 'text'
    this.mescase.cells[1][3].style.cursor = 'text'
    this.mescase.cells[1][5].style.cursor = 'text'
    j3pAffiche(this.mescase.cells[1][1], null, '&nbsp;')
  } // Ref

  Ref.prototype.corrigetab = function corrigetab () {
    const pv = stor.lenombre.indexOf('.')
    let monnombre
    if (pv === -1) {
      monnombre = stor.lenombre
    } else {
      monnombre = stor.lenombre.substring(0, pv)
    }
    while (monnombre.length < 12) {
      monnombre = '0' + monnombre
    }
    let l = 9
    switch (this.b) {
      case 'milliard':
        l = 0
        break
      case 'million':
        l = 3
        break
      case 'mille':
        l = 6
        break
    }
    this.mescase.cells[1][1].innerHTML = monnombre[l]
    this.mescase.cells[1][3].innerHTML = monnombre[l + 1]
    this.mescase.cells[1][5].innerHTML = monnombre[l + 2]
    this.mescase.cells[1][1].style.color = me.styles.toutpetit.correction.color
    this.mescase.cells[1][3].style.color = me.styles.toutpetit.correction.color
    this.mescase.cells[1][5].style.color = me.styles.toutpetit.correction.color
  }
  Ref.prototype.metrouge = function metrouge () {
    this.color('#f00')
  }
  Ref.prototype.metblanc = function metblanc () {
    this.color('#fff')
  }
  Ref.prototype.color = function color (col) {
    this.mescase.cells[1].forEach(suf => { suf.style.background = col })
  }
  Ref.prototype.gereVire = function gereVire (event) {
    event.preventDefault()
    event.stopPropagation()
  }
  Ref.prototype.gereClick = function gereClick (t, objet) {
    let elem = t
    if (elem.getAttribute('contenteditable') === null) {
      elem = elem.nextSibling
    }
    let i = 0
    let child = elem
    while ((child = child.previousSibling) != null) i++
    // i vaut 0 , 2  , 4 ou 6
    if (objet.cont[2] === '') {
      i = Math.max(i, 2)
    }
    if (objet.cont[1] === '') {
      i = Math.min(i, 4)
    }
    if (objet.cont[0] === '') {
      i = 6
    }
    objet.mescase.cells[1][i].focus()
  }
  Ref.prototype.gere = function gere (event) {
    const suivgauche = () => {
      switch (this.b) {
        case 'milliard' :
          break
        case 'million':
          if (ds.Rang_max_Entier === 'milliard') { stor.aidemilliard.mescase.cells[1][6].focus() }
          break
        case 'mille':
          if (['milliard', 'million'].indexOf(ds.Rang_max_Entier) !== -1) { stor.aidemillion.mescase.cells[1][6].focus() }
          break
        case 'unite':
          if (ds.Rang_max_Entier !== 'unité' && ds.Partie !== 'Décimale') { stor.aidemille.mescase.cells[1][6].focus() }
      }
    }

    const suivdroite = () => {
      switch (this.b) {
        case 'milliard' :
          if (stor.aidemillion.cont[2] === '') {
            stor.aidemillion.mescase.cells[1][6].focus()
          } else if (stor.aidemillion.cont[1] === '') {
            stor.aidemillion.mescase.cells[1][4].focus()
          } else if (stor.aidemillion.cont[0] === '') {
            stor.aidemillion.mescase.cells[1][2].focus()
          } else {
            stor.aidemillion.mescase.cells[1][0].focus()
          }
          break
        case 'million':
          if (stor.aidemille.cont[2] === '') {
            stor.aidemille.mescase.cells[1][6].focus()
          } else if (stor.aidemille.cont[1] === '') {
            stor.aidemille.mescase.cells[1][4].focus()
          } else if (stor.aidemille.cont[0] === '') {
            stor.aidemille.mescase.cells[1][2].focus()
          } else {
            stor.aidemille.mescase.cells[1][0].focus()
          }
          break
        case 'mille':
          if (stor.aideunite.cont[2] === '') {
            stor.aideunite.mescase.cells[1][6].focus()
          } else if (stor.aideunite.cont[1] === '') {
            stor.aideunite.mescase.cells[1][4].focus()
          } else if (stor.aideunite.cont[0] === '') {
            stor.aideunite.mescase.cells[1][2].focus()
          } else {
            stor.aideunite.mescase.cells[1][0].focus()
          }
          break
        case 'unite' :
          if (ds.Partie.indexOf('Décimale') !== -1) {
            if (stor.aidedec.turn === 'dixieme') stor.aidedec.mescases.cells[1][2].focus()
            if (stor.aidedec.turn === 'centieme') stor.aidedec.mescases.cells[1][5].focus()
            if (stor.aidedec.turn === 'millieme') stor.aidedec.mescases.cells[1][8].focus()
            if (stor.aidedec.turn === 'dixmillieme') stor.aidedec.mescases.cells[1][11].focus()
          }
      }
    }

    // determine la touche
    // donne 0123456789 ou gauche droite suppr del tab
    let lak
    if ((event.key !== undefined) && (event.key !== 'undefined')) {
      lak = event.key
    }
    event.preventDefault()
    event.stopPropagation()
    // effet
    const metBlancLater = this.metblanc.bind(this)
    if ('0123456789'.includes(lak)) {
      switch (event.currentTarget.num) {
        case 0:
          this.metrouge()
          setTimeout(metBlancLater, 100)
          break
        case 2:
          if (this.cont[0] === '') {
            this.cont[0] = lak
            this.mescase.cells[1][1].innerHTML = lak
            this.mescase.cells[1][3].focus()
          } else {
            this.metrouge()
            setTimeout(metBlancLater, 100)
          }

          break
        case 4:
          if (this.cont[1] === '') {
            this.cont[1] = lak
            this.mescase.cells[1][3].innerHTML = lak
            this.mescase.cells[1][4].focus()
          } else if (this.cont[0] === '') {
            this.cont[0] = this.cont[1]
            this.cont[1] = lak
            this.mescase.cells[1][1].innerHTML = this.cont[0]
            this.mescase.cells[1][3].innerHTML = lak
            this.mescase.cells[1][4].focus()
          } else {
            this.metrouge()
            setTimeout(metBlancLater, 100)
          }

          break
        case 6:
          if (this.cont[2] === '') {
            this.cont[2] = lak
            this.mescase.cells[1][5].innerHTML = lak
            this.mescase.cells[1][6].focus()
          } else if (this.cont[1] === '') {
            this.cont[1] = this.cont[2]
            this.cont[2] = lak
            this.mescase.cells[1][3].innerHTML = this.cont[1]
            this.mescase.cells[1][5].innerHTML = lak
            this.mescase.cells[1][6].focus()
          } else if (this.cont[0] === '') {
            this.cont[0] = this.cont[1]
            this.cont[1] = this.cont[2]
            this.cont[2] = lak
            this.mescase.cells[1][1].innerHTML = this.cont[0]
            this.mescase.cells[1][3].innerHTML = this.cont[1]
            this.mescase.cells[1][5].innerHTML = lak
            this.mescase.cells[1][6].focus()
          } else {
            this.metrouge()
            setTimeout(metBlancLater, 100)
          }
      }
    } else if (lak === 'ArrowLeft') {
      switch (event.currentTarget.num) {
        case 0:
          suivgauche()
          break
        case 2:
          if (this.cont[0] !== '') { this.mescase.cells[1][0].focus() } else { suivgauche() }
          break
        case 4:
          if (this.cont[1] !== '') { this.mescase.cells[1][2].focus() } else { suivgauche() }
          break
        case 6:
          if (this.cont[2] !== '') { this.mescase.cells[1][4].focus() } else { suivgauche() }
      }
    } else if (lak === 'ArrowRight') {
      switch (event.currentTarget.num) {
        case 0:
          this.mescase.cells[1][2].focus()
          break
        case 2:
          this.mescase.cells[1][4].focus()
          break
        case 4:
          this.mescase.cells[1][6].focus()
          break
        case 6:
          suivdroite()
      }
    } else if (lak === 'Delete') {
      switch (event.currentTarget.num) {
        case 0:
          this.cont[0] = ''
          this.mescase.cells[1][1].innerHTML = this.cont[0]
          this.mescase.cells[1][2].focus()
          break
        case 2:
          this.cont[1] = this.cont[0]
          this.cont[0] = ''
          this.mescase.cells[1][1].innerHTML = this.cont[0]
          this.mescase.cells[1][3].innerHTML = this.cont[1]
          this.mescase.cells[1][4].focus()
          break
        case 4:
          this.cont[2] = this.cont[1]
          this.cont[1] = this.cont[0]
          this.cont[0] = ''
          this.mescase.cells[1][1].innerHTML = this.cont[0]
          this.mescase.cells[1][3].innerHTML = this.cont[1]
          this.mescase.cells[1][5].innerHTML = this.cont[2]
          this.mescase.cells[1][6].focus()
          break
      }
    } else if (lak === 'Backspace') {
      switch (event.currentTarget.num) {
        case 2:
          this.cont[0] = ''
          this.mescase.cells[1][1].innerHTML = this.cont[0]
          break
        case 4:
          this.cont[1] = this.cont[0]
          this.cont[0] = ''
          this.mescase.cells[1][1].innerHTML = this.cont[0]
          this.mescase.cells[1][3].innerHTML = this.cont[1]
          break
        case 6:
          this.cont[2] = this.cont[1]
          this.cont[1] = this.cont[0]
          this.cont[0] = ''
          this.mescase.cells[1][1].innerHTML = this.cont[0]
          this.mescase.cells[1][3].innerHTML = this.cont[1]
          this.mescase.cells[1][5].innerHTML = this.cont[2]
      }
    } else if (lak === 'Tab') {
      suivdroite()
    }

    if (this.mescase.cells[1][1].innerHTML === '') { this.mescase.cells[1][1].innerHTML = '&nbsp;' }
    if (this.mescase.cells[1][3].innerHTML === '') { this.mescase.cells[1][3].innerHTML = '&nbsp;' }
    if (this.mescase.cells[1][5].innerHTML === '') { this.mescase.cells[1][5].innerHTML = '&nbsp;' }
  }
  Ref.prototype.desactive = function desactive () {
    this.mescase.cells[1][0].setAttribute('contenteditable', 'false')
    this.mescase.cells[1][2].setAttribute('contenteditable', 'false')
    this.mescase.cells[1][4].setAttribute('contenteditable', 'false')
    this.mescase.cells[1][6].setAttribute('contenteditable', 'false')
    this.mescase.cells[1][1].style.cursor = ''
    this.mescase.cells[1][3].style.cursor = 'ext'
    this.mescase.cells[1][5].style.cursor = ''
  }

  function Ref2 (b) {
    // stor.laide.dec.innerHTML = '<table border="1" style="margin:0;" cellspacing=0 cellpadding=0 width="100px" ><TR align="center" ><TD   class="imptom" colspan=3 id="MmMmdixieme" height="140px" width="40px"><img src="' + j3pBaseUrl + 'sections/college/sixieme/Decimaux/dixieme.png" alt="dixième"  height="100%" width="100%" ></TD><TD class="imptom" colspan=3 id="MmMmcentieme" height="140px" width="40px"><img src="' + j3pBaseUrl + 'sections/college/sixieme/Decimaux/centieme.png" alt="centième"  height="100%" width="100%" ></TD><TD  class="imptom" colspan=3 id="MmMmmillieme" height="140px" width="40px"><img src="' + j3pBaseUrl + 'sections/college/sixieme/Decimaux/millieme.png" alt="millième"  height="100%" width="100%" ></TD><TD class="imptom" colspan=3 id="MmMmdixmillieme" height="140px" width="40px"><img src="' + j3pBaseUrl + 'sections/college/sixieme/Decimaux/dixmillieme.png" alt="dix-millième"  height="100%" width="100%" ></TD></TR><TR align="center" ><TD contenteditable="true" id="m101" class="Aidegauche"></TD><TD id="m102" class="Aidemilieu">&nbsp;</TD><TD contenteditable="true" id="m103" class="Aidedroite"></TD><TD contenteditable="true" id="m1001" class="Aidegauche"></TD><TD id="m1002" class="Aidemilieu">&nbsp;</TD><TD contenteditable="true" id="m1003" class="Aidedroite"></TD><TD contenteditable="true" id="m10001" class="Aidegauche"></TD><TD id="m10002" class="Aidemilieu">&nbsp;</TD><TD contenteditable="true" id="m10003" class="Aidedroite"></TD><TD contenteditable="true" id="m100001" class="Aidegauche"></TD><TD id="m100002" class="Aidemilieu">&nbsp;</TD><TD contenteditable="true" id="m100003" class="Aidedroite"></TD></TR></TABLE>'
    this.mescases = addDefaultTableDetailed(stor.laide.dec, 2, 12)
    this.mescases.table.style.border = '1px solid black'
    this.mescases.table.style.width = '100px'
    this.mescases.table.style.borderCollapse = 'collapse'
    this.mescases.table.style.textAlign = 'center'
    this.mescases.table.setAttribute('class', 'noMargin')
    for (let i = 0; i < 2; i++) {
      for (let j = 0; j < 12; j++) {
        if (i === 0) this.mescases.cells[i][j].style.border = '1px solid black'
        this.mescases.cells[i][j].style.padding = 0
        this.mescases.cells[i][j].num = j
      }
    }
    j3pDetruit(this.mescases.cells[0][4])
    j3pDetruit(this.mescases.cells[0][5])
    j3pDetruit(this.mescases.cells[0][6])
    j3pDetruit(this.mescases.cells[0][7])
    j3pDetruit(this.mescases.cells[0][8])
    j3pDetruit(this.mescases.cells[0][9])
    j3pDetruit(this.mescases.cells[0][10])
    j3pDetruit(this.mescases.cells[0][11])

    this.mescases.cells[0][0].classList.add('imptom')
    this.mescases.cells[0][0].style.height = '140px'
    this.mescases.cells[0][0].style.width = '40px'
    this.mescases.cells[0][0].style.border = '1px solid black'
    this.mescases.cells[0][0].setAttribute('colspan', 3)
    let img = j3pAddElt(this.mescases.cells[0][0], 'img')
    img.src = imgDixieme
    img.height = '100%'
    img.width = '100%'
    this.mescases.cells[1][0].classList.add('Aidegauche')
    this.mescases.cells[1][0].style.width = '2px'
    this.mescases.cells[1][0].setAttribute('contenteditable', true)
    this.mescases.cells[1][1].classList.add('Aidemilieu')
    this.mescases.cells[1][2].classList.add('Aidedroite')
    this.mescases.cells[1][2].style.width = '2px'
    this.mescases.cells[1][2].setAttribute('contenteditable', true)

    this.mescases.cells[0][1].classList.add('imptom')
    this.mescases.cells[0][1].style.height = '140px'
    this.mescases.cells[0][1].style.width = '40px'
    this.mescases.cells[0][1].style.border = '1px solid black'
    this.mescases.cells[0][1].setAttribute('colspan', 3)
    img = j3pAddElt(this.mescases.cells[0][1], 'img')
    img.src = imgCentieme
    img.height = '100%'
    img.width = '100%'
    this.mescases.cells[1][3].classList.add('Aidegauche')
    this.mescases.cells[1][3].style.width = '2px'
    this.mescases.cells[1][3].setAttribute('contenteditable', true)
    this.mescases.cells[1][4].setAttribute('class', 'Aidemilieu')
    this.mescases.cells[1][5].setAttribute('class', 'Aidedroite')
    this.mescases.cells[1][5].style.width = '3px'
    this.mescases.cells[1][5].setAttribute('contenteditable', true)

    this.mescases.cells[0][2].classList.add('imptom')
    this.mescases.cells[0][2].style.height = '140px'
    this.mescases.cells[0][2].style.width = '40px'
    this.mescases.cells[0][2].style.border = '1px solid black'
    this.mescases.cells[0][2].setAttribute('colspan', 3)
    img = j3pAddElt(this.mescases.cells[0][2], 'img')
    img.src = imgMilieme
    img.height = '100%'
    img.width = '100%'
    this.mescases.cells[1][6].classList.add('Aidegauche')
    this.mescases.cells[1][6].style.width = '2px'
    this.mescases.cells[1][6].setAttribute('contenteditable', true)
    this.mescases.cells[1][7].classList.add('Aidemilieu')
    this.mescases.cells[1][7].classList.add('Aidedroite')
    this.mescases.cells[1][8].style.width = '2px'
    this.mescases.cells[1][8].setAttribute('contenteditable', true)

    this.mescases.cells[0][3].classList.add('imptom')
    this.mescases.cells[0][3].setAttribute('colspan', 3)
    this.mescases.cells[0][3].style.height = '140px'
    this.mescases.cells[0][3].style.width = '40px'
    this.mescases.cells[0][3].style.border = '1px solid black'
    img = j3pAddElt(this.mescases.cells[0][3], 'img')
    img.src = imgDixMilieme
    img.height = '100%'
    img.width = '100%'
    this.mescases.cells[1][9].classList.add('Aidegauche')
    this.mescases.cells[1][9].style.width = '2px'
    this.mescases.cells[1][9].setAttribute('contenteditable', true)
    this.mescases.cells[1][10].classList.add('Aidemilieu')
    this.mescases.cells[1][11].classList.add('Aidedroite')
    this.mescases.cells[1][11].style.width = '2px'
    this.mescases.cells[1][11].setAttribute('contenteditable', true)

    this.b = b
    this.cont = ['', '', '', '']
    this.turn = 'dixieme'
    this.imageClicListener = this.imageclic.bind(this)
    this.mescases.cells[0][0].style.cursor = 'pointer'
    this.mescases.cells[0][0].addEventListener('click', this.imageClicListener, false)
    this.mescases.cells[0][1].style.cursor = 'pointer'
    this.mescases.cells[0][1].addEventListener('click', this.imageClicListener, false)
    this.mescases.cells[0][2].style.cursor = 'pointer'
    this.mescases.cells[0][2].addEventListener('click', this.imageClicListener, false)
    this.mescases.cells[0][3].style.cursor = 'pointer'
    this.mescases.cells[0][3].addEventListener('click', this.imageClicListener, false)
    const objet = this
    const gereListener = this.gere.bind(this)
    const gereClickListener = function () { objet.gereClick(objet, this) }
    const gereVireListener = this.gereVire.bind(this)
    this.mescases.cells[1][0].addEventListener('keydown', gereListener, false)
    this.mescases.cells[1][3].addEventListener('keydown', gereListener, false)
    this.mescases.cells[1][6].addEventListener('keydown', gereListener, false)
    this.mescases.cells[1][9].addEventListener('keydown', gereListener, false)

    this.mescases.cells[1][0].addEventListener('keypress', gereVireListener, false)
    this.mescases.cells[1][3].addEventListener('keypress', gereVireListener, false)
    this.mescases.cells[1][6].addEventListener('keypress', gereVireListener, false)
    this.mescases.cells[1][9].addEventListener('keypress', gereVireListener, false)

    this.mescases.cells[1][0].addEventListener('keyup', gereVireListener, false)
    this.mescases.cells[1][3].addEventListener('keyup', gereVireListener, false)
    this.mescases.cells[1][6].addEventListener('keyup', gereVireListener, false)
    this.mescases.cells[1][9].addEventListener('keyup', gereVireListener, false)

    this.mescases.cells[1][2].addEventListener('keydown', gereListener, false)
    this.mescases.cells[1][5].addEventListener('keydown', gereListener, false)
    this.mescases.cells[1][8].addEventListener('keydown', gereListener, false)
    this.mescases.cells[1][11].addEventListener('keydown', gereListener, false)

    this.mescases.cells[1][2].addEventListener('keypress', gereVireListener, false)
    this.mescases.cells[1][5].addEventListener('keypress', gereVireListener, false)
    this.mescases.cells[1][8].addEventListener('keypress', gereVireListener, false)
    this.mescases.cells[1][11].addEventListener('keypress', gereVireListener, false)

    this.mescases.cells[1][2].addEventListener('keyup', gereVireListener, false)
    this.mescases.cells[1][5].addEventListener('keyup', gereVireListener, false)
    this.mescases.cells[1][8].addEventListener('keyup', gereVireListener, false)
    this.mescases.cells[1][11].addEventListener('keyup', gereVireListener, false)

    this.mescases.cells[1][1].addEventListener('click', gereClickListener, false)
    this.mescases.cells[1][4].addEventListener('click', gereClickListener, false)
    this.mescases.cells[1][7].addEventListener('click', gereClickListener, false)
    this.mescases.cells[1][10].addEventListener('click', gereClickListener, false)
    this.mescases.cells[1][0].addEventListener('click', gereClickListener, false)
    this.mescases.cells[1][3].addEventListener('click', gereClickListener, false)
    this.mescases.cells[1][6].addEventListener('click', gereClickListener, false)
    this.mescases.cells[1][9].addEventListener('click', gereClickListener, false)
    this.mescases.cells[1][2].addEventListener('click', gereClickListener, false)
    this.mescases.cells[1][5].addEventListener('click', gereClickListener, false)
    this.mescases.cells[1][8].addEventListener('click', gereClickListener, false)
    this.mescases.cells[1][11].addEventListener('click', gereClickListener, false)

    this.mescases.cells[1][1].style.cursor = 'text'
    this.mescases.cells[1][4].style.cursor = 'text'
    this.mescases.cells[1][7].style.cursor = 'text'
    this.mescases.cells[1][10].style.cursor = 'text'
    this.mescases.cells[0][1].style.display = 'none'
    this.mescases.cells[1][3].style.display = 'none'
    this.mescases.cells[1][4].style.display = 'none'
    this.mescases.cells[1][5].style.display = 'none'
    this.mescases.cells[0][2].style.display = 'none'
    this.mescases.cells[1][6].style.display = 'none'
    this.mescases.cells[1][7].style.display = 'none'
    this.mescases.cells[1][8].style.display = 'none'
    this.mescases.cells[0][3].style.display = 'none'
    this.mescases.cells[1][9].style.display = 'none'
    this.mescases.cells[1][10].style.display = 'none'
    this.mescases.cells[1][11].style.display = 'none'
    if (ds.Rang_max_Decim === 'dix-millième') {
      this.mescases.cells[0][1].style.display = ''
      this.mescases.cells[1][3].style.display = ''
      this.mescases.cells[1][4].style.display = ''
      this.mescases.cells[1][5].style.display = ''
      this.mescases.cells[0][2].style.display = ''
      this.mescases.cells[1][6].style.display = ''
      this.mescases.cells[1][7].style.display = ''
      this.mescases.cells[1][8].style.display = ''
      this.mescases.cells[0][3].style.display = ''
      this.mescases.cells[1][9].style.display = ''
      this.mescases.cells[1][10].style.display = ''
      this.mescases.cells[1][11].style.display = ''
    }
    if (ds.Rang_max_Decim === 'millième') {
      this.mescases.cells[0][1].style.display = ''
      this.mescases.cells[1][3].style.display = ''
      this.mescases.cells[1][4].style.display = ''
      this.mescases.cells[1][5].style.display = ''
      this.mescases.cells[0][2].style.display = ''
      this.mescases.cells[1][6].style.display = ''
      this.mescases.cells[1][7].style.display = ''
      this.mescases.cells[1][8].style.display = ''
    }
    if (ds.Rang_max_Decim === 'centième') {
      this.mescases.cells[0][1].style.display = ''
      this.mescases.cells[1][3].style.display = ''
      this.mescases.cells[1][4].style.display = ''
      this.mescases.cells[1][5].style.display = ''
    }
    this.imageclic({ currentTarget: { num: 0 } })
  } // Ref2

  Ref2.prototype.corrigetab = function corrigetab () {
    const pv = stor.lenombre.indexOf('.')
    if (pv === -1) return
    const monnombre = stor.lenombre.substring(pv + 1)
    this.mescases.cells[1][1].innerHTML = this.mescases.cells[1][4].innerHTML = this.mescases.cells[1][7].innerHTML = this.mescases.cells[1][10].innerHTML = ''
    if (monnombre.length > 0) {
      this.mescases.cells[1][1].innerHTML = monnombre[0]
      this.mescases.cells[1][1].style.color = me.styles.toutpetit.correction.color
    }
    if (monnombre.length > 1) {
      this.mescases.cells[1][4].innerHTML = monnombre[1]
      this.mescases.cells[1][4].style.color = me.styles.toutpetit.correction.color
    }
    if (monnombre.length > 2) {
      this.mescases.cells[1][7].innerHTML = monnombre[2]
      this.mescases.cells[1][7].style.color = me.styles.toutpetit.correction.color
    }
    if (monnombre.length > 3) {
      this.mescases.cells[1][10].innerHTML = monnombre[3]
      this.mescases.cells[1][10].style.color = me.styles.toutpetit.correction.color
    }
  }
  Ref2.prototype.imageclic = function imageclic (event) {
    // on est appelé via un bind, donc notre this est toujours l’objet Ref2,
    // on utilise event.currentTarget.id pour récupérer l’id de l’élément sur lequel on a été ajouté comme listener
    switch (event.currentTarget.num) {
      case 0:
        this.mescases.cells[1][0].setAttribute('class', 'Aidegauche')
        this.mescases.cells[1][1].setAttribute('class', 'Aidemilieu')
        this.mescases.cells[1][2].setAttribute('class', 'Aidedroite')
        this.mescases.cells[1][3].setAttribute('class', 'Aidegauche')
        this.mescases.cells[1][4].setAttribute('class', 'Aidemilieu')
        this.mescases.cells[1][5].setAttribute('class', 'Aidedroite')
        this.mescases.cells[1][6].setAttribute('class', 'Aidegauche')
        this.mescases.cells[1][7].setAttribute('class', 'Aidemilieu')
        this.mescases.cells[1][8].setAttribute('class', 'Aidedroite')
        this.mescases.cells[1][9].setAttribute('class', 'Aidegauche')
        this.mescases.cells[1][10].setAttribute('class', 'Aidemilieu')
        this.mescases.cells[1][11].setAttribute('class', 'Aidedroite')
        this.mescases.cells[1][2].focus()
        this.turn = 'dixieme'
        break
      case 1:
        this.mescases.cells[1][0].setAttribute('class', 'Aidegauche')
        this.mescases.cells[1][1].setAttribute('class', 'Aidemilieu')
        this.mescases.cells[1][2].setAttribute('class', 'Aidemilieu')
        this.mescases.cells[1][3].setAttribute('class', 'Aidemilieu')
        this.mescases.cells[1][4].setAttribute('class', 'Aidemilieu')
        this.mescases.cells[1][5].setAttribute('class', 'Aidedroite')
        this.mescases.cells[1][6].setAttribute('class', 'Aidegauche')
        this.mescases.cells[1][7].setAttribute('class', 'Aidemilieu')
        this.mescases.cells[1][8].setAttribute('class', 'Aidedroite')
        this.mescases.cells[1][9].setAttribute('class', 'Aidegauche')
        this.mescases.cells[1][10].setAttribute('class', 'Aidemilieu')
        this.mescases.cells[1][11].setAttribute('class', 'Aidedroite')
        if (this.cont[1] === '') {
          this.cont[1] = this.cont[0]
          this.cont[0] = ''
        }
        this.mescases.cells[1][5].focus()
        this.turn = 'centieme'
        break
      case 2:
        this.mescases.cells[1][0].setAttribute('class', 'Aidegauche')
        this.mescases.cells[1][1].setAttribute('class', 'Aidemilieu')
        this.mescases.cells[1][2].setAttribute('class', 'Aidemilieu')
        this.mescases.cells[1][3].setAttribute('class', 'Aidemilieu')
        this.mescases.cells[1][4].setAttribute('class', 'Aidemilieu')
        this.mescases.cells[1][5].setAttribute('class', 'Aidemilieu')
        this.mescases.cells[1][6].setAttribute('class', 'Aidemilieu')
        this.mescases.cells[1][7].setAttribute('class', 'Aidemilieu')
        this.mescases.cells[1][8].setAttribute('class', 'Aidedroite')
        this.mescases.cells[1][9].setAttribute('class', 'Aidegauche')
        this.mescases.cells[1][10].setAttribute('class', 'Aidemilieu')
        this.mescases.cells[1][11].setAttribute('class', 'Aidedroite')
        if (this.cont[2] === '') {
          this.cont[2] = this.cont[1]
          this.cont[1] = this.cont[0]
          this.cont[0] = ''
        }
        if (this.cont[2] === '') {
          this.cont[2] = this.cont[1]
          this.cont[1] = this.cont[0]
          this.cont[0] = ''
        }
        this.mescases.cells[1][8].focus()
        this.turn = 'millieme'
        break
      case 3:
        this.mescases.cells[1][0].setAttribute('class', 'Aidegauche')
        this.mescases.cells[1][1].setAttribute('class', 'Aidemilieu')
        this.mescases.cells[1][2].setAttribute('class', 'Aidemilieu')
        this.mescases.cells[1][3].setAttribute('class', 'Aidemilieu')
        this.mescases.cells[1][4].setAttribute('class', 'Aidemilieu')
        this.mescases.cells[1][5].setAttribute('class', 'Aidemilieu')
        this.mescases.cells[1][6].setAttribute('class', 'Aidemilieu')
        this.mescases.cells[1][7].setAttribute('class', 'Aidemilieu')
        this.mescases.cells[1][8].setAttribute('class', 'Aidemilieu')
        this.mescases.cells[1][9].setAttribute('class', 'Aidemilieu')
        this.mescases.cells[1][10].setAttribute('class', 'Aidemilieu')
        this.mescases.cells[1][11].setAttribute('class', 'Aidedroite')
        if (this.cont[2] === '') {
          this.cont[3] = this.cont[2]
          this.cont[2] = this.cont[1]
          this.cont[1] = this.cont[0]
          this.cont[0] = ''
        }
        if (this.cont[2] === '') {
          this.cont[3] = this.cont[2]
          this.cont[2] = this.cont[1]
          this.cont[1] = this.cont[0]
          this.cont[0] = ''
        }
        if (this.cont[2] === '') {
          this.cont[3] = this.cont[2]
          this.cont[2] = this.cont[1]
          this.cont[1] = this.cont[0]
          this.cont[0] = ''
        }
        this.mescases.cells[1][11].focus()
        this.turn = 'dixmillieme'
        break
    }
    this.affcont()
  }
  Ref2.prototype.affcont = function affcont () {
    /// verif pas de blanc
    // si dixieme osef

    if (this.turn !== 'dixieme') {
      if (this.cont[1] === '') {
        this.cont[1] = this.cont[0]
        this.cont[0] = ''
      }
    }
    if ((this.turn !== 'dixieme') && (this.turn !== 'centieme')) {
      if (this.cont[2] === '') {
        this.cont[2] = this.cont[1]
        this.cont[1] = this.cont[0]
        this.cont[0] = ''
      }
    }
    if (this.turn === 'dixmillieme') {
      if (this.cont[3] === '') {
        this.cont[3] = this.cont[2]
        this.cont[2] = this.cont[1]
        this.cont[1] = this.cont[0]
        this.cont[0] = ''
      }
    }
    this.mescases.cells[1][1].innerHTML = this.cont[0]
    if (this.cont[0] === '') { this.mescases.cells[1][1].innerHTML = '&nbsp;' }
    this.mescases.cells[1][4].innerHTML = this.cont[1]
    if (this.cont[1] === '') { this.mescases.cells[1][4].innerHTML = '&nbsp;' }
    this.mescases.cells[1][7].innerHTML = this.cont[2]
    if (this.cont[2] === '') { this.mescases.cells[1][7].innerHTML = '&nbsp;' }
    this.mescases.cells[1][10].innerHTML = this.cont[3]
    if (this.cont[3] === '') { this.mescases.cells[1][10].innerHTML = '&nbsp;' }
  }
  Ref2.prototype.metrouge = function metrouge (q) {
    this.crouge = []
    switch (this.turn) {
      case 'dixieme':
        this.crouge.push(q)
        this.crouge.push(q + 1)
        this.crouge.push(q + 2)
        break
      case 'centieme':
        if ((q === '10') || (q === '100')) {
          this.crouge.push(0)
          this.crouge.push(1)
          this.crouge.push(2)
          this.crouge.push(3)
          this.crouge.push(4)
          this.crouge.push(5)
        } else {
          this.crouge.push(q)
          this.crouge.push(q + 1)
          this.crouge.push(q + 2)
        }
        break
      case 'millieme':
        if (q !== '10000') {
          this.crouge.push(0)
          this.crouge.push(1)
          this.crouge.push(2)
          this.crouge.push(3)
          this.crouge.push(4)
          this.crouge.push(5)
          this.crouge.push(6)
          this.crouge.push(7)
          this.crouge.push(8)
        } else {
          this.crouge.push(9)
          this.crouge.push(10)
          this.crouge.push(11)
        }
        break
      case 'dixmillieme':
        this.crouge.push(0)
        this.crouge.push(1)
        this.crouge.push(2)
        this.crouge.push(3)
        this.crouge.push(4)
        this.crouge.push(5)
        this.crouge.push(6)
        this.crouge.push(7)
        this.crouge.push(8)
        this.crouge.push(9)
        this.crouge.push(10)
        this.crouge.push(11)
    }
    this.color('#f00')
  }
  Ref2.prototype.metblanc = function metblanc () {
    this.color('#fff')
  }
  Ref2.prototype.color = function color (col) {
    for (let i = 0; i < this.crouge.length; i++) {
      this.mescases.cells[1][this.crouge[i]].style.background = col
    }
  }
  Ref2.prototype.gereVire = function gereVire (event) {
    event.preventDefault()
    event.stopPropagation()
  }
  Ref2.prototype.gereClick = function gereClick (o, t) {
    // on est appelé via un bind, donc notre t est toujours l’objet Ref2,
    // on utilise t.id pour récupérer l’id de l’élément sur lequel on a été ajouté comme listener
    let idToFocus
    switch (o.turn) {
      case 'dixieme':
        switch (t.num) {
          case 0:
            if (o.cont[0] !== '') idToFocus = 0
            else idToFocus = 2
            break
          case 1:
          case 2:
            idToFocus = 2
            break
          case 3:
            if (o.cont[1] !== '') idToFocus = 3
            else idToFocus = 5
            break
          case 4:
          case 5:
            idToFocus = 5
            break
          case 6:
            if (o.cont[2] !== '') idToFocus = 6
            else idToFocus = 8
            break
          case 7:
          case 8:
            idToFocus = 8
            break
          case 9:
            if (o.cont[3] !== '') idToFocus = 9
            else idToFocus = 11
            break
          case 10:
          case 11:
            idToFocus = 11
            break
        }
        break
      case 'centieme':
        switch (t.num) {
          case 0:
            if (o.cont[0] !== '') idToFocus = 0
            else if (o.cont[1] !== '') idToFocus = 3
            else idToFocus = 5
            break
          case 1:
          case 2:
            if (o.cont[1] !== '') idToFocus = 3
            else idToFocus = 5
            break
          case 3:
            if (o.cont[1] !== '') idToFocus = 3
            else idToFocus = 5
            break
          case 4:
          case 5:
            idToFocus = 5
            break
          case 6:
            if (o.cont[2] !== '') idToFocus = 6
            else idToFocus = 8
            break
          case 7:
          case 8:
            idToFocus = 8
            break
          case 9:
            if (o.cont[3] !== '') idToFocus = 9
            else idToFocus = 11
            break
          case 10:
          case 11:
            idToFocus = 11
            break
        }

        break
      case 'millieme':
        switch (t.num) {
          case 0:
            if (o.cont[0] !== '') idToFocus = 0
            else if (o.cont[1] !== '') idToFocus = 3
            else if (o.cont[2] !== '') idToFocus = 6
            else idToFocus = 8
            break
          case 1:
          case 2:
            if (o.cont[1] !== '') idToFocus = 3
            else if (o.cont[2] !== '') idToFocus = 6
            else idToFocus = 8
            break
          case 3:
            if (o.cont[1] !== '') idToFocus = 3
            else if (o.cont[2] !== '') idToFocus = 6
            else idToFocus = 8
            break
          case 4:
          case 5:
            if (o.cont[2] !== '') idToFocus = 6
            else idToFocus = 8
            break
          case 6:
            if (o.cont[2] !== '') idToFocus = 6
            else idToFocus = 8
            break
          case 7:
          case 8:
            idToFocus = 8
            break
          case 9:
            if (o.cont[3] !== '') idToFocus = 9
            else idToFocus = 11
            break
          case 10:
          case 11:
            idToFocus = 11
            break
        }
        break
      case 'dixmillieme':
        switch (t.num) {
          case 0:
            if (o.cont[0] !== '') idToFocus = 0
            else if (o.cont[1] !== '') idToFocus = 3
            else if (o.cont[2] !== '') idToFocus = 6
            else if (o.cont[3] !== '') idToFocus = 9
            else idToFocus = 11
            break
          case 1:
          case 2:
            if (o.cont[1] !== '') idToFocus = 3
            else if (o.cont[2] !== '') idToFocus = 6
            else if (o.cont[3] !== '') idToFocus = 9
            else idToFocus = 11
            break
          case 3:
            if (o.cont[1] !== '') idToFocus = 3
            else if (o.cont[2] !== '') idToFocus = 6
            else if (o.cont[3] !== '') idToFocus = 9
            else idToFocus = 11
            break
          case 4:
          case 5:
            if (o.cont[2] !== '') idToFocus = 6
            else if (o.cont[3] !== '') idToFocus = 9
            else idToFocus = 11
            break
          case 6:
            if (o.cont[2] !== '') idToFocus = 6
            else if (o.cont[3] !== '') idToFocus = 9
            else idToFocus = 11
            break
          case 7:
          case 8:
            if (o.cont[3] !== '') idToFocus = 9
            else idToFocus = 11
            break
          case 9:
            if (o.cont[3] !== '') idToFocus = 9
            else idToFocus = 11
            break
          case 10:
          case 11:
            idToFocus = 11
            break
        }
        break
    }
    if (o.mescases.cells[1][idToFocus]) o.mescases.cells[1][idToFocus].focus()
  }
  Ref2.prototype.gere = function gere (event) {
    // on est appelé via un bind, donc notre this est toujours l’objet Ref2,
    // on utilise event.currentTarget.num pour récupérer l’id de l’élément sur lequel on a été ajouté comme listener
    // determine la touche
    // donne 0123456789 ou gauche droite suppr del tab
    let lak
    if ((event.key !== undefined) && (event.key !== 'undefined')) {
      lak = event.key
    }
    event.preventDefault()
    event.stopPropagation()

    // effet
    const metBlancLater = this.metblanc.bind(this)
    if ('0123456789'.includes(lak)) {
      switch (event.currentTarget.num) {
        case 0 :
          this.metrouge(0)
          setTimeout(metBlancLater, 100)
          break
        case 2 :
          if (this.cont[0] !== '') {
            this.metrouge(0)
            setTimeout(metBlancLater, 100)
          } else { this.cont[0] = lak }
          break
        case 3 :
          if ((this.turn === 'dixieme') || (this.cont[0] !== '')) {
            this.metrouge(3)
            setTimeout(metBlancLater, 100)
          } else { this.cont[0] = lak }
          break
        case 5:
          if (this.cont[1] === '') {
            this.cont[1] = lak
          } else if ((this.cont[0] === '') && (this.turn !== 'dixieme')) {
            this.cont[0] = this.cont[1]
            this.cont[1] = lak
          } else {
            this.metrouge(3)
            setTimeout(metBlancLater, 100)
          }
          break
        case 6 :
          if ((this.turn === 'centieme') || (this.turn === 'dixieme') || (this.cont[0] !== '')) {
            this.metrouge(6)
            setTimeout(metBlancLater, 100)
          } else {
            this.cont[0] = this.cont[1]
            this.cont[1] = lak
          }
          break
        case 8:
          if (this.cont[2] === '') {
            this.cont[2] = lak
          } else if ((this.cont[0] === '') && ((this.turn === 'millieme') || (this.turn === 'dixmillieme'))) {
            this.cont[0] = this.cont[1]
            this.cont[1] = this.cont[2]
            this.cont[2] = lak
          } else {
            this.metrouge(6)
            setTimeout(metBlancLater, 100)
          }
          break
        case 9 :
          if ((this.turn !== 'dixmillieme') || (this.cont[0] !== '')) {
            this.metrouge(9)
            setTimeout(metBlancLater, 100)
          } else {
            this.cont[0] = this.cont[1]
            this.cont[1] = this.cont[2]
            this.cont[2] = lak
          }
          break
        case 11:
          if (this.cont[3] === '') { this.cont[3] = lak } else if ((this.cont[0] === '') && (this.turn === 'dixmillieme')) {
            this.cont[0] = this.cont[1]
            this.cont[1] = this.cont[2]
            this.cont[2] = this.cont[3]
            this.cont[3] = lak
          } else {
            this.metrouge(9)
            setTimeout(metBlancLater, 100)
          }
          break
      }
    } else if (lak === 'ArrowLeft') {
      switch (event.currentTarget.num) {
        case 0:
          stor.aideunite.mescase.cells[1][6].focus()
          break
        case 2 :
          if (this.cont[0] !== '') {
            this.mescases.cells[1][0].focus()
          } else {
            stor.aideunite.mescase.cells[1][6].focus()
          }
          break
        case 3 :
          if ((this.turn !== 'dixieme') && (this.cont[0] !== '')) {
            this.mescases.cells[1][0].focus()
          } else {
            if (this.turn === 'dixieme') this.mescases.cells[1][2].focus()
            else stor.aideunite.mescase.cells[1][6].focus()
          }
          break
        case 5:
          if (this.cont[1] !== '') { this.mescases.cells[1][3].focus() } else if (this.turn === 'dixieme') { this.mescases.cells[1][2].focus() } else { stor.aideunite.mescase.cells[1][6].focus() }
          break
        case 6 :
          if ((this.turn !== 'centieme') && (this.turn !== 'dixieme') && (this.cont[1] !== '')) {
            this.mescases.cells[1][3].focus()
          } else {
            if (this.turn === 'dixieme') this.mescases.cells[1][5].focus()
            else stor.aideunite.mescase.cells[1][6].focus()
          }
          break
        case 8:
          if (this.cont[2] !== '') {
            this.mescases.cells[1][6].focus()
          } else if ((this.turn === 'dixieme') || (this.turn === 'centieme')) {
            this.mescases.cells[1][5].focus()
          } else {
            stor.aideunite.mescase.cells[1][6].focus()
          }
          break
        case 9 :
          if ((this.turn === 'dixmillieme') && (this.cont[2] !== '')) {
            this.mescases.cells[1][6].focus()
          } else {
            if (this.turn !== 'dixmillieme') this.mescases.cells[1][8].focus()
            else stor.aideunite.mescase.cells[1][6].focus()
          }
          break
        case 11:
          if (this.cont[3] !== '') { this.mescases.cells[1][9].focus() } else if (this.turn !== 'dixmillieme') { this.mescases.cells[1][8].focus() } else { stor.aideunite.mescase.cells[1][6].focus() }
          break
      }
    } else if (lak === 'ArrowRight') {
      switch (event.currentTarget.num) {
        case 0 :
          if (this.cont[0] !== '') {
            this.mescases.cells[1][2].focus()
          }
          break
        case 2 :
          this.mescases.cells[1][5].focus()
          break
        case 3:
          this.mescases.cells[1][5].focus()
          break
        case 5 :
          this.mescases.cells[1][8].focus()
          break
        case 6:
          this.mescases.cells[1][8].focus()
          break
        case 8 :
          this.mescases.cells[1][11].focus()
          break
        case 9:
          this.mescases.cells[1][11].focus()
          break
      }
    } else if (lak === 'Delete') {
      switch (event.currentTarget.num) {
        case 0 :
          this.cont[0] = ''
          this.mescases.cells[1][2].focus()
          break
        case 2 :
          if (this.turn !== 'dixieme') {
            this.cont[1] = ''
            this.mescases.cells[1][5].focus()
          }
          break
        case 3:
          this.cont[1] = ''
          this.mescases.cells[1][5].focus()
          break
        case 5 :
          if ((this.turn !== 'dixieme') && (this.turn !== 'centieme')) {
            this.cont[2] = ''
            this.mescases.cells[1][8].focus()
          }
          break
        case 6:
          this.cont[2] = ''
          this.mescases.cells[1][8].focus()
          break
        case 8 :
          if (this.turn === 'dixmillieme') {
            this.cont[3] = ''
            this.mescases.cells[1][11].focus()
          }
          break
        case 9:
          this.cont[3] = ''
          this.mescases.cells[1][11].focus()
          break
      }
    } else if (lak === 'Backspace') {
      switch (event.currentTarget.num) {
        case 2 :
          this.cont[0] = ''
          break
        case 3:
          if (this.turn !== 'dixieme') this.cont[0] = ''
          break
        case 5 :
          this.cont[1] = ''
          break
        case 6:
          if ((this.turn !== 'dixieme') && (this.turn !== 'centieme')) this.cont[1] = ''
          break
        case 8 :
          this.cont[2] = ''
          break
        case 9:
          if (this.turn === 'dixmillieme') this.cont[2] = ''
          break
        case 11:
          this.cont[3] = ''
      }
    }
    this.affcont()
  } // gere
  Ref2.prototype.desactive = function desactive () {
    this.mescases.cells[1][0].setAttribute('contenteditable', 'false')
    this.mescases.cells[1][3].setAttribute('contenteditable', 'false')
    this.mescases.cells[1][6].setAttribute('contenteditable', 'false')
    this.mescases.cells[1][9].setAttribute('contenteditable', 'false')
    this.mescases.cells[1][2].setAttribute('contenteditable', 'false')
    this.mescases.cells[1][5].setAttribute('contenteditable', 'false')
    this.mescases.cells[1][8].setAttribute('contenteditable', 'false')
    this.mescases.cells[1][11].setAttribute('contenteditable', 'false')

    this.mescases.cells[1][1].style.cursor = ''
    this.mescases.cells[1][4].style.cursor = ''
    this.mescases.cells[1][7].style.cursor = ''
    this.mescases.cells[1][10].style.cursor = ''

    this.mescases.cells[0][0].style.cursor = ''
    this.mescases.cells[0][0].removeEventListener('click', this.imageClicListener, false)
    this.mescases.cells[0][1].style.cursor = ''
    this.mescases.cells[0][1].removeEventListener('click', this.imageClicListener, false)
    this.mescases.cells[0][2].style.cursor = ''
    this.mescases.cells[0][2].removeEventListener('click', this.imageClicListener, false)
    this.mescases.cells[0][3].style.cursor = ''
    this.mescases.cells[0][3].removeEventListener('click', this.imageClicListener, false)

    j3pRemoveClass('#MmMmdixieme', 'imptom')
    j3pRemoveClass('#MmMmcentieme', 'imptom')
    j3pRemoveClass('#MmMmmillieme', 'imptom')
    j3pRemoveClass('#MmMmdixmillieme', 'imptom')

    j3pAddClass('#MmMmdixieme', 'imptom2')
    j3pAddClass('#MmMmcentieme', 'imptom2')
    j3pAddClass('#MmMmmillieme', 'imptom2')
    j3pAddClass('#MmMmdixmillieme', 'imptom2')
  }

  function voir () {
    stor.lesdiv.trav2.style.display = ''
    j3pDetruit('boton3')
  }

  function retournetab (tab) {
    let ret = ''
    for (let i = 0; i < tab.length; i++) {
      ret = tab[i] + ret
    }
    return ret
  }

  function initSection () {
    me.construitStructurePage('presentation3')
    me.typederreurs = [0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0]
    const montd = 'Ecrire'
    let montf = ' un nombre décimal'
    if (ds.Partie.indexOf('Décimale') === -1) { montf = ' un nombre entier' }

    me.afficheTitre(montd + montf + ' en chiffres')

    stor.pbpos = ['nada']
    stor.pbfait = []
    if (ds.Partie.indexOf('Entière') !== -1) {
      if (ds.Rang_max_Entier === 'milliard') {
        stor.pbpos.push('-unite')
        stor.pbpos.push('-mille')
        stor.pbpos.push('-million')
        stor.pbpos.push('-million -mille')
        stor.pbpos.push('-million -unite')
        stor.pbpos.push('-mille -unite')
        stor.pbpos.push('-million -unite -mille')
        stor.pbpos.push('dizaine')
        stor.pbpos.push('unite')
      }
      if (ds.Rang_max_Entier === 'million') {
        stor.pbpos.push('-unite')
        stor.pbpos.push('-mille')
        stor.pbpos.push('-million')
        stor.pbpos.push('-mille -unite')
        stor.pbpos.push('dizaine')
        stor.pbpos.push('unite')
      }
      if (ds.Rang_max_Entier === 'mille') {
        stor.pbpos.push('-unite')
        stor.pbpos.push('-mille')
        stor.pbpos.push('dizaine')
        stor.pbpos.push('unite')
      }
    }
    if (ds.Partie.indexOf('Décimale') !== -1) {
      if (ds.Rang_max_Decim === 'dix-millième') {
        stor.pbpos.push('decim10')
        stor.pbpos.push('decim100')
        stor.pbpos.push('decim1000')
      }
      if (ds.Rang_max_Decim === 'millième') {
        stor.pbpos.push('decim10')
        stor.pbpos.push('decim100')
      }
      if (ds.Rang_max_Decim === 'centième') {
        stor.pbpos.push('decim10')
      }
    }

    if (ds.indication) me.indication(me.zones.IG, ds.indication)
    ds.pe = [ds.pe_1, ds.pe_2, ds.pe_3, ds.pe_4, ds.pe_5, ds.pe_6, ds.pe_7, ds.pe_8, ds.pe_9, ds.pe_10]
    enonceMain()
  }

  function enonceMain () {
    // A chaque répétition, on nettoie la scène
    me.videLesZones()
    stor.bull = []

    let cmptpos = 0
    let ok
    do {
      cmptpos++
      stor.pbencours = stor.pbpos[j3pGetRandomInt(0, (stor.pbpos.length - 1))]
      ok = (stor.pbfait.indexOf(stor.pbencours) === -1)
    } while ((cmptpos < 100) && (!ok))
    stor.pbfait.push(stor.pbencours)
    /// det nb

    // det si ya partie ent , dec ou les deux en fct des choix
    switch (ds.Partie) {
      case 'Entière':
        stor.yaent = true
        stor.yadec = false
        break
      case 'Décimale':
        stor.yaent = false
        stor.yadec = true
        break
      default :
        switch (j3pGetRandomInt(0, 5)) {
          case 0:
            stor.yaent = false
            stor.yadec = true
            break
          case 1:
            stor.yaent = true
            stor.yadec = false
            break
          default:
            stor.yaent = true
            stor.yadec = true
        }
    }
    if (stor.pbencours.indexOf('-') !== -1) { stor.yaent = true }
    if (stor.pbencours.indexOf('unite') !== -1) { stor.yaent = true }
    if (stor.pbencours.indexOf('dizaine') !== -1) { stor.yaent = true }
    if (stor.pbencours.indexOf('10') !== -1) { stor.yadec = true }

    // det partie ent en fonctin choix
    if (stor.yaent) {
      switch (ds.Rang_max_Entier) {
        case 'milliard':
          stor.partieent = j3pGetRandomInt(53, 998999999999)
          break
        case 'million':
          stor.partieent = j3pGetRandomInt(53, 998999999)
          break
        case 'mille':
          stor.partieent = j3pGetRandomInt(53, 998999)
          break
        default :
          stor.partieent = j3pGetRandomInt(53, 999)
      }
    } else {
      stor.partieent = 0
    }
    // det partie dec

    let decpos = ['dixiéme']
    if (ds.Rang_max_Decim === 'centième') { decpos = ['dixiéme', 'centième'] }
    if (ds.Rang_max_Decim === 'millième') { decpos = ['dixiéme', 'centième', 'millième'] }
    if (ds.Rang_max_Decim === 'dix-millième') { decpos = ['dixiéme', 'centième', 'millième', 'dix-millième'] }

    if (stor.pbencours === 'decim10') { decpos.splice(0, 1) }
    if (stor.pbencours === 'decim100') { decpos.splice(0, 2) }
    if (stor.pbencours === 'decim1000') { decpos.splice(0, 3) }

    ds.Rang_max_Decim2 = decpos[j3pGetRandomInt(0, decpos.length - 1)]
    let chifat
    if (stor.yadec) {
      switch (ds.Rang_max_Decim2) {
        case 'dixiéme':
          stor.partiedec = j3pGetRandomInt(1, 9)
          chifat = 1
          break
        case 'centième':
          stor.partiedec = j3pGetRandomInt(1, 99)
          chifat = 2
          break
        case 'millième':
          stor.partiedec = j3pGetRandomInt(1, 999)
          chifat = 3
          break
        default :
          stor.partiedec = j3pGetRandomInt(1, 9999)
          chifat = 4
      }
      const pc = stor.partiedec + ''
      if (pc[pc.length - 1] === '0') {
        stor.partiedec = stor.partiedec + j3pGetRandomInt(1, 9)
      }
    } else {
      stor.partiedec = 0
    }

    // determine un cas particulier
    let bub
    switch (stor.pbencours) {
      case '-million':
        if (ds.Rang_max_Entier === 'milliard') { stor.partieent = stor.partieent + 1000000000 }
        bub = retournetab(stor.partieent + '')
        bub = bub.substring(0, 6) + '000' + bub.substring(9, bub.length)
        stor.partieent = parseInt(retournetab(bub))
        break
      case '-mille':
        if (['milliard', 'million'].indexOf(ds.Rang_max_Entier) !== -1) { stor.partieent = stor.partieent + 1000000 }
        bub = retournetab(stor.partieent + '')
        bub = bub.substring(0, 3) + '000' + bub.substring(6, bub.length)
        stor.partieent = parseInt(retournetab(bub))
        break
      case '-unite':
        if (['milliard', 'million', 'mille'].indexOf(ds.Rang_max_Entier) !== -1) { stor.partieent = stor.partieent + 1000 }
        bub = retournetab(stor.partieent + '')
        bub = '000' + bub.substring(3, bub.length)
        stor.partieent = parseInt(retournetab(bub))
        break
      case '-million -mille':
        if (ds.Rang_max_Entier === 'milliard') { stor.partieent = stor.partieent + 1000000000 }
        bub = retournetab(stor.partieent + '')
        bub = bub.substring(0, 3) + '000000' + bub.substring(9, bub.length)
        stor.partieent = parseInt(retournetab(bub))
        break
      case '-million -unite':
        if (ds.Rang_max_Entier === 'milliard') { stor.partieent = stor.partieent + 1000000000 }
        bub = retournetab(stor.partieent + '')
        bub = '000' + bub.substring(3, 6) + '000' + bub.substring(9, bub.length)
        stor.partieent = parseInt(retournetab(bub))
        break
      case '-mille -unite':
        if (['milliard', 'million'].indexOf(ds.Rang_max_Entier) !== -1) { stor.partieent = stor.partieent + 1000000 }
        bub = retournetab(stor.partieent + '')
        bub = '000000' + bub.substring(6, bub.length)
        stor.partieent = parseInt(retournetab(bub))
        break
      case '-million -unite -mille':
        if (ds.Rang_max_Entier === 'milliard') { stor.partieent = stor.partieent + 1000000000 }
        bub = retournetab(stor.partieent + '')
        bub = '000000000' + bub.substring(9, bub.length)
        stor.partieent = parseInt(retournetab(bub))
        break
      case 'dizaine':
        stor.partieent = stor.partieent + 1000
        bub = retournetab(stor.partieent + '')
        bub = j3pGetRandomInt(0, 9) + j3pGetRandomInt(1, 9) + '0' + bub.substring(3, bub.length)
        stor.partieent = parseInt(retournetab(bub))

        break
      case 'unite':
        stor.partieent = stor.partieent + 1000
        bub = retournetab(stor.partieent + '')
        bub = j3pGetRandomInt(1, 9) + '00' + bub.substring(3, bub.length)
        stor.partieent = parseInt(retournetab(bub))

        break
      case 'decim10' :
        stor.partiedec = j3pGetRandomInt(1, 9)
        break
      case 'decim100' : {
        stor.partiedec = j3pGetRandomInt(11, 99)
        const pc = stor.partiedec + ''
        if (pc[pc.length - 1] === '0') {
          stor.partiedec = stor.partiedec + j3pGetRandomInt(1, 9)
        }
        break
      }
      case 'decim1000' : {
        stor.partiedec = j3pGetRandomInt(101, 999)
        const pc = stor.partiedec + ''
        if (pc[pc.length - 1] === '0') {
          stor.partiedec = stor.partiedec + j3pGetRandomInt(1, 9)
        }
        break
      }
    }

    // recompose puis determine lettres
    stor.lenombre = stor.partieent + ''
    if (stor.yadec) {
      const nbchif = (stor.partiedec + '').length
      stor.partiedecfin = stor.partiedec + ''
      stor.lenombre += '.'
      for (let i = 0; i < chifat - nbchif; i++) {
        stor.lenombre += '0'
        stor.partiedecfin = '0' + stor.partiedecfin
      }
      stor.lenombre += stor.partiedec
    }
    stor.nb = parseFloat(stor.lenombre)

    /// cree div
    stor.lesdiv = {}
    stor.lesdiv.conteneur = j3pAddElt(me.zones.MG, 'div', { style: me.styles.etendre('toutpetit.enonce', { padding: '10px' }) })
    stor.lesdiv.consigne = addDefaultTable(stor.lesdiv.conteneur, 1, 1)[0][0]
    const tt = addDefaultTable(stor.lesdiv.conteneur, 1, 3)
    tt[0][1].style.width = '20px'
    const tt2 = addDefaultTable(tt[0][0], 3, 1)
    stor.lesdiv.travail = tt2[0][0]
    stor.lesdiv.boot = tt2[1][0]
    stor.lesdiv.trav2 = tt2[2][0]
    stor.lesdiv.correction = tt[0][2]
    stor.lesdiv.explications = addDefaultTable(stor.lesdiv.conteneur, 1, 1)[0][0]
    stor.lesdiv.solution = addDefaultTable(stor.lesdiv.conteneur, 1, 1)[0][0]
    stor.lesdiv.explications.style.color = me.styles.cfaux
    stor.lesdiv.solution.style.color = me.styles.petit.correction.color

    tt[0][0].classList.add('travail')
    stor.lesdiv.consigne.classList.add('enonce')
    stor.lesdiv.explications.classList.remove('explique')
    stor.lesdiv.trav2.style.display = 'none'

    /// /pose question
    if (stor.partiedec === 0) {
      stor.nbquestion = nombreEnMots(stor.partieent)
    } else {
      const nb = stor.nb
      stor.nbquestion = nombreEnMots(nb)
    }

    if (stor.nbquestion[0] === '-') { stor.nbquestion = stor.nbquestion.substring(1) }
    if (stor.nbquestion[0] === '-') { stor.nbquestion = stor.nbquestion.substring(1) }

    j3pAffiche(stor.lesdiv.consigne, 'cons', textes.consigne, {
      a: stor.nbquestion
    })

    stor.mazone = new ZoneStyleMathquill1(stor.lesdiv.travail, {
      restric: '0123456789,. ',
      limite: 25,
      clavier: '0123456789, ',
      enter: me.sectionCourante.bind(me)
    })
    // on veut le rendre visible dès le départ
    stor.mazone.toggle()
    stor.mazone.focus()

    /// prep aide (si ya)
    if (ds.Aide) {
      j3pAjouteBouton(stor.lesdiv.boot, 'boton3', 'MepBoutons2', 'Aide', voir)
    }
    const untab = addDefaultTable(stor.lesdiv.trav2, 2, 1)
    j3pAffiche(untab[0][0], null, '<u>Tableau de lecture</u>')
    untab[0][0].style.textAlign = 'center'
    // tab = '<table border=3 style="margin:0;;background:#fff" cellspacing=0><TR align="center" style="margin:0;border:1px black solid=" ><TD id="MmMmmilliard">milliards</TD><TD id="MmMmmillion">millions</TD><TD id="MmMmmille">mille</TD><TD id="MmMmunite">unités</TD><TD id="MmMmvirgule"></TD><TD rowspan = 2 id="paritiedecimale"></TD></TR>'
    // tab += '<TR  ><TD style="vertical-align: bottom" id="MmMmmilliard2">        </TD><TD style="vertical-align: bottom" id="MmMmmillion2">       </TD><TD style="vertical-align: bottom" id="MmMmmille2">   </TD><TD style="vertical-align: bottom" id="MmMmunite2">    </TD><TD style="vertical-align: bottom" id="MmMmvirgule2"><b>&nbsp;,&nbsp;</b></TD></TR></TABLE>'
    const untab2 = addDefaultTableDetailed(untab[1][0], 2, 6)
    untab2.cells[0][5].style.padding = 0
    untab2.cells[0][4].style.padding = 0
    untab2.cells[0][3].style.padding = 0
    untab2.cells[0][2].style.padding = 0
    untab2.cells[0][1].style.padding = 0
    untab2.cells[0][0].style.padding = 0

    untab2.cells[1][3].style.verticalAlign = 'bottom'
    untab2.cells[1][2].style.verticalAlign = 'bottom'
    untab2.cells[1][1].style.verticalAlign = 'bottom'
    untab2.cells[1][0].style.verticalAlign = 'bottom'

    untab2.cells[1][3].style.border = '1px solid black'
    untab2.cells[1][2].style.border = '1px solid black'
    untab2.cells[1][1].style.border = '1px solid black'
    untab2.cells[1][0].style.border = '1px solid black'
    untab2.cells[0][3].style.border = '1px solid black'
    untab2.cells[0][2].style.border = '1px solid black'
    untab2.cells[0][1].style.border = '1px solid black'
    untab2.cells[0][0].style.border = '1px solid black'
    untab2.table.style.background = '#ffffff'
    untab2.table.style.border = '3px solid black'
    j3pDetruit(untab2.cells[1][4])
    untab2.cells[0][4].setAttribute('rowspan', 2)
    j3pDetruit(untab2.cells[1][5])
    untab2.cells[0][5].setAttribute('rowspan', 2)

    j3pAffiche(untab2.cells[0][0], null, 'milliards')
    j3pAffiche(untab2.cells[0][1], null, 'millions')
    j3pAffiche(untab2.cells[0][2], null, 'mille')
    j3pAffiche(untab2.cells[0][3], null, 'unités')
    j3pAffiche(untab2.cells[0][4], null, '&nbsp;,&nbsp;')
    untab2.cells[0][4].style.verticalAlign = 'bottom'
    untab2.cells[0][0].style.textAlign = 'center'
    untab2.cells[0][1].style.textAlign = 'center'
    untab2.cells[0][2].style.textAlign = 'center'
    untab2.cells[0][3].style.textAlign = 'center'
    untab2.cells[0][0].style.display = 'none'
    untab2.cells[1][0].style.display = 'none'
    untab2.cells[0][1].style.display = 'none'
    untab2.cells[1][1].style.display = 'none'
    untab2.cells[0][2].style.display = 'none'
    untab2.cells[1][2].style.display = 'none'
    untab2.cells[0][4].style.display = 'none'
    untab2.cells[0][5].style.display = 'none'
    stor.laide = {}
    stor.laide.miliard = untab2.cells[1][0]
    stor.laide.millions = untab2.cells[1][1]
    stor.laide.mille = untab2.cells[1][2]
    stor.laide.unie = untab2.cells[1][3]
    stor.laide.virg = untab2.cells[0][4]
    stor.laide.dec = untab2.cells[0][5]
    if (ds.Partie.indexOf('Entière') !== -1) {
      if ((ds.Rang_max_Entier === 'milliard') && (ds.Partie.indexOf('Entière') !== -1)) {
        stor.aidemilliard = new Ref('milliard')
        untab2.cells[0][0].style.display = ''
        untab2.cells[1][0].style.display = ''
      }
      if ((['milliard', 'million'].indexOf(ds.Rang_max_Entier) !== -1) && (ds.Partie.indexOf('Entière') !== -1)) {
        stor.aidemillion = new Ref('million')
        untab2.cells[0][1].style.display = ''
        untab2.cells[1][1].style.display = ''
      }
      if ((ds.Rang_max_Entier !== 'unité') && (ds.Partie.indexOf('Entière') !== -1)) {
        stor.aidemille = new Ref('mille')
        untab2.cells[0][2].style.display = ''
        untab2.cells[1][2].style.display = ''
      }
    }
    stor.aideunite = new Ref('unite')

    if (ds.Partie.indexOf('Décimale') !== -1) {
      stor.aidedec = new Ref2('paritiedecimale')
      untab2.cells[0][4].style.display = ''
      untab2.cells[0][5].style.display = ''
    }
    me.finEnonce()
  }

  switch (this.etat) {
    case 'enonce': {
      // code exécuté au lancement de la section
      if (this.debutDeLaSection) {
        initSection()
      } else {
        enonceMain()
      }
      break // case "enonce":
    }

    case 'correction':
      // On teste si une réponse a été saisie
      if (ds.theme === 'zonesAvecImageDeFond') {
        stor.lesdiv.explications.classList.remove('explique')
        stor.lesdiv.solution.classList.remove('correction')
      }

      if (!yareponse()) {
        // ici on donne le message à afficher si une ou plusieurs zone(s) n’est pas complétée (tout d’abord on met en rouge : styles.cfaux)
        stor.lesdiv.correction.style.color = this.styles.cfaux
        stor.lesdiv.correction.innerHTML = reponseIncomplete
        stor.mazone.focus()
        return this.finCorrection()
      }

      // Une réponse a été saisie
      if (bonneReponse()) {
        this.score++
        stor.lesdiv.correction.style.color = this.styles.cbien
        stor.lesdiv.correction.innerHTML = cBien

        if (document.getElementById('boton3') !== null) {
          j3pDetruit('boton3')
        }
        desactivetab()
        stor.lesdiv.explications.innerHTML = ''

        stor.mazone.corrige(true)
        stor.mazone.disable()
        return this.finCorrection('navigation', true)
      }

      // Pas de bonne réponse
      stor.lesdiv.correction.style.color = this.styles.cfaux
      // A cause de la limite de temps :
      if (this.isElapsed) { // limite de temps
        stor.lesdiv.correction.innerHTML = tempsDepasse
        afcofo(true)
        return this.finCorrection('navigation', true)
      }
      // Réponse fausse :
      stor.lesdiv.correction.innerHTML = cFaux
      if (this.essaiCourant < ds.nbchances) {
        stor.lesdiv.correction.innerHTML += '<br>' + essaieEncore
        afcofo(false)
        return this.finCorrection()
      }
      // Erreur au dernier essai
      afcofo(true)
      stor.lesdiv.correction.innerHTML += '<br>' + regardeCorrection
      this.finCorrection('navigation', true)
      break // case "correction":

    case 'navigation':
      if (this.sectionTerminee()) {
        // On détermine la phrase d’état renvoyée par la section
        const sco = this.score / ds.nbitems
        let peibase
        if (sco >= 0.8) {
          peibase = 0
        } else if (sco >= 0.55) {
          peibase = 1
        } else if (sco >= 0.3) {
          peibase = 4
        } else {
          peibase = 7
        }
        let peisup = 0
        let compt = this.typederreurs[2] / 4
        if (this.typederreurs[6] > compt) {
          peisup = 2
          compt = this.typederreurs[6]
        }
        if (this.typederreurs[3] > compt) {
          peisup = 1
        }
        this.parcours.pe = ds.pe[peibase + peisup]
      } else {
        this.etat = 'enonce'
      }
      this.finNavigation()
      break // case "navigation":
  }
}
