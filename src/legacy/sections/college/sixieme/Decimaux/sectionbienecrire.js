import { j3pAddElt, j3pAjouteBouton, j3pEmpty, j3pGetRandomInt, j3pModale } from 'src/legacy/core/functions'
import { j3pAffiche } from 'src/lib/mathquill/functions'
import ZoneStyleMathquill1 from 'src/legacy/outils/zoneStyleMathquill/ZoneStyleMathquill1'
import { addDefaultTable } from 'src/legacy/themes/table'
import BulleAide from 'src/legacy/outils/bulleAide/BulleAide'
import textesGeneriques from 'src/lib/core/textes'

import './bienecrire.scss'

const { cBien, cFaux, essaieEncore, regardeCorrection, reponseIncomplete, tempsDepasse } = textesGeneriques

/**
 * Ajouter une description ici
 * @fileOverview
 * @author Tommy Barroy <t.barroy@laposte.net>
 * @since 2017-10
*/

export const params = {
  outils: ['mathquill'],
  /**
   * Ce qui est paramétrable dans l’éditeur de graphe
   * @type {LegacySectionParamsParametres}
   */
  parametres: [
    ['nbrepetitions', 9, 'entier', 'Nombre de répétitions de la section'],
    ['indication', '', 'string', 'Indication pour chaque répétition'],
    ['limite', 0, 'entier', 'Temps disponible par répétition, en secondes'],
    ['Travail', 'Espaces_Zéros', 'liste', '<u>Espaces</u>: L’élève doit bien placer les espaces dans un nombre.  <BR><BR> <u>Zéros</u>: L’élève doit supprimer des zéros inutiles.', ['Espaces', 'Zéros', 'Espaces_Zéros']],
    ['Partie', 'Entière_Décimale', 'liste', '<u>Entière</u>: Le travail porte sur la partie entière. <BR><BR><u>Décimale</u>: Le travail porte sur la partie décimale. ', ['Entière', 'Décimale', 'Entière_Décimale']],
    ['Mode', 'Click', 'liste', '<u>Click</u>: Le travail se fait à la souris. <BR><BR> <u>Clavier</u>: Le travail se fait au clavier.', ['Clavier', 'Click']],
    ['MortSubite', true, 'boolean', "<u>true</u> : La réponse est comptée fausse dès que l’élève supprime un zéro utile <BR><BR> <u>false</u>: Un bouton 'Revenir en arrière' est utilisable."],
    // le modèle gère la compatibilité ascendante avec l’ancien paramètre MiseEnPagePlusClaire
    ['theme', 'standard', 'liste', 'Modèle pour l’affichage', ['standard', 'zonesAvecImageDeFond']]

  ],
  // dans le cas d’une section QUANTITATIVE
  pe: [
    { pe_1: 'Maitrise' },
    { pe_2: 'Suffisant' },
    { pe_3: 'Suffisant - erreur zéros' },
    { pe_4: 'Suffisant - erreur espaces' },
    { pe_5: 'Insuffisant' },
    { pe_6: 'Insuffisant - erreur zéros' },
    { pe_7: 'Insuffisant - erreur espaces' }
  ]
}

/**
 * section bienecrire
 * @this {Parcours}
 */
export default function main () {
  const me = this
  const stor = me.storage
  let ds = me.donneesSection

  function yareponse () {
    if (me.isElapsed) { return true }
    if (stor.Click) { return true }
    return (stor.mazonem.reponse() !== '')
  }
  function estBonneReponse () {
    me.ErrPlusieursVirgule = false
    me.ErrVirguleSeule = false
    me.ErrEspace = false
    me.ErrDecimaleEspace = false
    me.ErrZerosInutTrop = false
    me.ErrZerosEnMoins = false
    me.Err2esp = false
    me.ErrespVirg2 = false
    me.ErrespVirg = false
    me.ErrPasdent = false

    if (!stor.Click) {
      // eslint-disable-next-line no-irregular-whitespace
      me.repEleve = stor.mazonem.reponse().replace(/ /g, ' ')
      if (me.repEleve === '') {
        stor.mazonem.majaffiche('0')
        me.repEleve = '0'
      }
    } else {
      me.repEleve = stor.partieentiereEncours
      if (stor.yavirguleEncours) { me.repEleve += ',' }
      me.repEleve += stor.partiedecimaleEncours
    }

    // test si y’a bien qu’une virgule
    if (me.repEleve.indexOf(',') !== me.repEleve.lastIndexOf(',')) {
      me.ErrPlusieursVirgule = true
      return false
    }

    if (me.repEleve.indexOf(',') !== -1) {
      stor.partieentiereEncours = me.repEleve.substring(0, me.repEleve.indexOf(','))
      stor.yavirguleEncours = true
      stor.partiedecimaleEncours = me.repEleve.substring(me.repEleve.indexOf(',') + 1)
    } else {
      stor.partieentiereEncours = me.repEleve
      stor.yavirguleEncours = false
      stor.partiedecimaleEncours = ''
    }

    // test si ya viré un zero util
    // -> on compare les 2 nb
    let repatt = stor.partieentiereDeb
    if (stor.yavirguleDeb) { repatt += '.' }
    repatt += stor.partiedecimaleDeb
    repatt = parseFloat(repatt.replace(/ /g, ''))
    if (me.repEleve.indexOf(',') === 0) {
      me.ErrPasdent = true; return false
    }
    const repel = parseFloat(me.repEleve.replace(',', '.').replace(/ /g, ''))
    if (repatt !== repel) { me.ErrZerosEnMoins = true; return false }

    // test si ya oublié zero inut
    for (let i = 0; i < stor.partieentiereEncours.length; i++) {
      if (stor.partieentiereEncours[i] !== ' ') {
        if (stor.partieentiereEncours[i] === '0') {
          if (stor.partieentiereEncours.length !== 1) {
            me.ErrZerosInutTrop = true
            return false
          }
        } else {
          break
        }
      }
    }
    for (let i = stor.partiedecimaleEncours.length - 1; i > -1; i--) {
      if (stor.partiedecimaleEncours[i] !== ' ') {
        if (stor.partiedecimaleEncours[i] === '0') {
          me.ErrZerosInutTrop = true
          return false
        }
        break
      }
    }

    // test si les espaces sont pas strange
    // à partir de virgule, ou a partir de la droite
    // ca doit etre des groupes de 3 (sauf le dernier)
    // ou alors tout colle et espace a osef
    if ((stor.yavirguleEncours) && (stor.partiedecimaleEncours.replace(/ /g, '').length === 0)) {
      me.ErrVirguleSeule = true
      return false
    }

    let yaesp = true
    let comptgrp = 0
    let comptgrpold = 3
    let ing = false
    for (let i = stor.partieentiereEncours.length - 1; i > -1; i--) {
      if (stor.partieentiereEncours[i] === ' ') {
        if (yaesp) {
          if (i === (stor.partieentiereEncours.length - 1)) {
            me.ErrespVirg2 = true
            return false
          } else {
            me.Err2esp = true
            return false
          }
        }
        yaesp = true
        if (ing) { comptgrpold = comptgrp }
        ing = false
        comptgrp = 0
      } else {
        yaesp = false
        comptgrp++
        ing = true
        if ((comptgrpold !== 3) || ((comptgrp > 3) && (stor.Espaces))) {
          me.ErrEspace = true
          return false
        }
      }
    }

    yaesp = true
    comptgrp = 0
    comptgrpold = 3
    ing = false
    for (let i = 0; i < stor.partiedecimaleEncours.length; i++) {
      if (stor.partiedecimaleEncours[i] === ' ') {
        if (yaesp) {
          if (i === 0) {
            me.ErrespVirg = true
            return false
          } else {
            me.Err2esp = true
            return false
          }
        }
        yaesp = true
        if (ing) { comptgrpold = comptgrp }
        ing = false
        comptgrp = 0
      } else {
        yaesp = false
        comptgrp++
        ing = true
        if ((comptgrpold !== 3) || ((comptgrp > 3) && (stor.Espaces))) {
          me.ErrDecimaleEspace = true
          return false
        }
      }
    }
    // recolle tout
    return true
  }

  function affimodale (texte) {
    const yy = j3pModale({ titre: ds.textes.aide, contenu: '' })
    j3pAffiche(yy, null, texte)
    yy.style.color = me.styles.toutpetit.correction.color
  }

  function getDonnees () {
    return {

      // Nombre de répétitions de l’exercice
      nbrepetitions: 9,
      nbetapes: 1,
      limite: 0,
      Partie: 'Entière_Décimale',
      Travail: 'Espaces_Zéros',
      Mode: 'Click',
      MortSubite: true,
      coul1: '#F6FAFE',
      coul2: '#D8D8D8',
      pe_1: 'Maitrise',
      pe_2: 'Suffisant',
      pe_3: 'Suffisant - erreur zéros',
      pe_4: 'Suffisant - erreur espaces',
      pe_5: 'Insuffisant',
      pe_6: 'Insuffisant - erreur zéros',
      pe_7: 'Insuffisant - erreur espaces',
      // on précise la liste des pe supprimées et remplacées par d’autres (pour que les graphes construits avec ces anciennes pe continuent de fonctionner)
      replacedPe: {
        pe_8: 'pe_5',
        pe_9: 'pe_6',
        pe_10: 'pe_7'
      },

      // Si indication !== "" alors un lien "Indication" s’affiche dans la zone Inférieure Gauche (zones.IG
      // une indication<br>de deux lignes...";
      indication: '',

      // pour le cas de presentation1bis
      // En effet pas d’indication possible pour cette présentation
      // surchargeindication: false;

      // Nombre de chances dont dispose l’él§ve pour répondre à la question
      nbchances: 1,

      structure: 'presentation1', //  || "presentation2" || "presentation3"  || "presentation1bis"

      // Paramètres de la section, avec leurs valeurs par défaut.

      /*
          Les textes présents dans la section
          Sont donc accessibles dans le code par donneesSection.textes.phrase1
          Possibilité d’écrire dans le code :
          var lestextes: donneesSection.textes;
          puis accès à l’aide de lestextes.phrase1
          */
      textes: {
        couleurexplique: '#A9E2F3',
        couleurcorrec: '#A9F5A9',
        couleurenonce: '#D8D8D8',
        couleurenonceg: '#F3E2A9',
        couleuretiquettes: '#A9E2F3',
        aide: 'Attention !',
        aidezero: 'Clique sur un zéro pour le supprimer.',
        aideesp: 'Clique sur une espace par la faire disparaître. <BR> Clique sous deux chiffres pour insérer une espace.',
        soluce: 'Il fallait écrire $£a$ .',
        AttVirg: 'Tu ne peux pas supprimer la virgule, <BR> Il reste des chiffres dans la partie décimale !',
        consigne: 'Supprime les zéros inutiles dans le nombre $N$.',
        consigne2: 'Place correctement les espaces dans le nombre $N$.',
        consigne3: ' $N = £a$ <BR><BR><BR>',
        consigne2bis: 'Ecris correctement ce nombre: $£a$',
        errtropvirgule: 'Il ne peut y avoir plusieurs virgules dans un nombre !',
        errvirguleseule: 'Il ne peut pas y avoir de virgule sans partie décimale !',
        errespace: 'Dans la partie entière, les chiffres doivent être regroupés par 3 en commençant par la droite !',
        errespacedec: 'Dans la partie décimale, les chiffres doivent être regroupés par 3 en commençant par la gauche !',
        errDeprang2: 'Supprime les zéros inutiles !',
        errespVirg: 'Il ne doit pas y avoir d’espace à côté de la virgule !',
        errespVirg2: 'Il ne doit pas y avoir d’espace à la fin de la partie entière !',
        err2esp: 'Tu as mis 2 espaces côté à côté !',
        errZerosEnMoins: 'Ces deux nombres ne sont pas égaux !',
        errZerosInutTrop: 'Tu dois supprimer tous les zéros inutiles !'

      },

      // Par défaut, pas de temps limite. Décommentez pour avoir un temps limite par défaut. En secondes.

      // limite = 10;
      /*
          Phrase d’état renvoyée par la section
          Si pe=0 alors la section est quantitative et renvoie un score entre 0 et 1.
          Dans le cas où la section est qualitative, lister les différents pe renvoyées :
              pe_1 = "toto"
              pe_2 = "tata"
              etc
          Dans la partie navigation, il faudra affecter la variable parcours.pe à l’une des précédentes.
          Par exemple,
          parcours.pe = donneesSection.pe_2
          */
      pe: 0
    }
  }

  function supp (num, where) {
    if (where === 'Ent') {
      if ((stor.partieentiereEncours[num] === '0') && (ds.MortSubite)) {
        for (let i = 0; i < num; i++) {
          if (((stor.partieentiereEncours[i] !== '0') && (stor.partieentiereEncours[i] !== ' ')) || (stor.partieentiereEncours.length === 1)) {
            stor.partieentiereEncours = stor.partieentiereEncours.substring(0, num) + stor.partieentiereEncours.substring(num + 1, stor.partieentiereEncours.length)
            affichenbclik()
            me.sectionCourante()
            stor.yam = true
            return
          }
        }
      }
      stor.partieentiereEncours = stor.partieentiereEncours.substring(0, num) + stor.partieentiereEncours.substring(num + 1, stor.partieentiereEncours.length)
    }
    if (where === 'Dec') {
      if ((stor.partiedecimaleEncours[num] === '0') && (ds.MortSubite)) {
        for (let i = num + 1; i < stor.partiedecimaleEncours.length; i++) {
          if ((stor.partiedecimaleEncours[i] !== '0') && (stor.partiedecimaleEncours[i] !== ' ')) {
            stor.partiedecimaleEncours = stor.partiedecimaleEncours.substring(0, num) + stor.partiedecimaleEncours.substring(num + 1, stor.partiedecimaleEncours.length)
            affichenbclik()
            me.sectionCourante()
            return
          }
        }
      }
      stor.partiedecimaleEncours = stor.partiedecimaleEncours.substring(0, num) + stor.partiedecimaleEncours.substring(num + 1, stor.partiedecimaleEncours.length)
    }
    if (where === 'Virg') {
      if (stor.partiedecimaleEncours.length > 0) {
        affimodale(ds.textes.AttVirg)
        return
      }
      stor.yavirguleEncours = false
    }
    affichenbclik()
  }

  function ajou (num, where) {
    if (where === 'Ent') {
      stor.partieentiereEncours = stor.partieentiereEncours.substring(0, num) + ' ' + stor.partieentiereEncours.substring(num, stor.partieentiereEncours.length)
    }
    if (where === 'Dec') {
      stor.partiedecimaleEncours = stor.partiedecimaleEncours.substring(0, num) + ' ' + stor.partiedecimaleEncours.substring(num, stor.partiedecimaleEncours.length)
    }
    if (where === 'Virg') {
      stor.partieentiereEncours = stor.partieentiereEncours + ' '
    }
    affichenbclik()
  }

  function mafonczeroEnt () { supp(this.num, 'Ent') }
  function mafonczeroDec () { supp(this.num, 'Dec') }
  function mafoncVirg () { supp(0, 'Virg') }
  function mafoncEspVirg () { ajou(0, 'Virg') }
  function mafoncEspEnt () { ajou(this.num, 'Ent') }
  function mafoncEspDec () { ajou(this.num, 'Dec') }

  function affichenbclik (isDisa, isGood) {
    j3pEmpty(stor.lesdiv.travail)
    while (stor.partieentiereEncours[0] === ' ') {
      stor.partieentiereEncours = stor.partieentiereEncours.substring(1, stor.partieentiereEncours.length)
    }
    if (!stor.yavirguleEncours) {
      while (stor.partieentiereEncours[stor.partieentiereEncours.length - 1] === ' ') {
        stor.partieentiereEncours = stor.partieentiereEncours.substring(0, stor.partieentiereEncours.length - 1)
      }
    }
    while (stor.partiedecimaleEncours[stor.partiedecimaleEncours.length - 1] === ' ') {
      stor.partiedecimaleEncours = stor.partiedecimaleEncours.substring(0, stor.partiedecimaleEncours.length - 1)
    }
    let lacoul = ds.coul1
    if (ds.theme === 'zonesAvecImageDeFond') lacoul = ds.coul2

    let nbcase = stor.partieentiereEncours.length + stor.partiedecimaleEncours.length
    if (stor.yavirguleEncours) nbcase++
    const montatab = addDefaultTable(stor.lesdiv.travail, 2, nbcase + 1)
    let cmpt = -1
    for (let i = 0; i < stor.partieentiereEncours.length; i++) {
      montatab[0][i + 1].setAttribute('colspan', 2)
      montatab[0][i + 1].style.width = '10px'
      montatab[0][i + 1].num = i
      montatab[0][i + 1].style.borderCollapse = 'collapse'
      montatab[0][i + 1].style.borderLeft = '5px solid ' + lacoul
      montatab[0][i + 1].style.borderRight = '5px solid ' + lacoul
      montatab[0][i + 1].style.textAlign = 'right'
      montatab[0][i + 1].style.borderBottom = 0
      montatab[1][i].setAttribute('colspan', 2)
      montatab[1][i].style.height = '40px'
      cmpt = i
    }
    if (stor.yavirguleEncours) {
      const i = cmpt + 1
      montatab[0][i + 1].setAttribute('colspan', 2)
      montatab[0][i + 1].style.width = '10px'
      montatab[0][i + 1].num = i
      montatab[0][i + 1].style.borderCollapse = 'collapse'
      montatab[0][i + 1].style.borderLeft = '5px solid ' + lacoul
      montatab[0][i + 1].style.borderRight = '5px solid ' + lacoul
      montatab[0][i + 1].style.textAlign = 'right'
      montatab[0][i + 1].style.borderBottom = 0
      montatab[1][i].setAttribute('colspan', 2)
      montatab[1][i].style.height = '40px'
      cmpt++
    }
    stor.nbVirg = cmpt + 1
    for (let j = 0; j < stor.partiedecimaleEncours.length + 1; j++) {
      const i = cmpt + j
      montatab[0][i + 1].setAttribute('colspan', 2)
      montatab[0][i + 1].style.width = '10px'
      montatab[0][i + 1].num = i
      montatab[0][i + 1].style.borderCollapse = 'collapse'
      montatab[0][i + 1].style.borderLeft = '5px solid ' + lacoul
      montatab[0][i + 1].style.borderRight = '5px solid ' + lacoul
      montatab[0][i + 1].style.textAlign = 'right'
      montatab[0][i + 1].style.borderBottom = 0
      if (stor.partiedecimaleEncours.length === 0) break
      montatab[1][i].setAttribute('colspan', 2)
      montatab[1][i].style.height = '40px'
    }

    for (let i = 0; i < stor.partieentiereEncours.length; i++) {
      let buf = '$' + stor.partieentiereEncours[i] + '$'
      let ok = true
      if ((buf === '$0$') && (stor.Zeros)) {
        ok = false
        if (!isDisa) montatab[0][i + 1].setAttribute('class', 'klik')
        montatab[0][i + 1].num = i
        if (!isDisa) montatab[0][i + 1].addEventListener('click', mafonczeroEnt, false)
      }
      if (buf === '$ $') {
        ok = false
        buf = ' '
        if (stor.Espaces) {
          if (!isDisa) montatab[0][i + 1].setAttribute('class', 'klikesp')
          if (!isDisa) {
            buf = '_'
          } else {
            buf = '$\\text{ }$'
          }
          montatab[0][i + 1].num = i
          if (!isDisa) montatab[0][i + 1].addEventListener('click', mafonczeroEnt, false)
        }
      }
      j3pAffiche(montatab[0][i + 1], null, buf)
      if (ok) {
        if (!isDisa) montatab[0][i + 1].setAttribute('class', 'klikrien')
      }
      if (stor.Espaces) {
        if (i !== 0) {
          if (!isDisa) montatab[1][i].setAttribute('class', 'klikespsous')
          montatab[1][i].num = i
          if (!isDisa) montatab[1][i].addEventListener('click', mafoncEspEnt, false)
        }
      }
    }
    if (stor.yavirguleEncours) {
      if (!isDisa) montatab[0][stor.nbVirg].setAttribute('class', 'klik')
      if (!isDisa) montatab[0][stor.nbVirg].addEventListener('click', mafoncVirg, false)
      j3pAffiche(montatab[0][stor.nbVirg], null, '$,$')
      montatab[0][stor.nbVirg].style.color = 'black'
      if (stor.Espaces) {
        if (!isDisa) montatab[1][stor.nbVirg - 1].setAttribute('class', 'klikespsous')
        if (!isDisa) montatab[1][stor.nbVirg - 1].addEventListener('click', mafoncEspVirg, false)
      }
    }
    for (let j = 0; j < stor.partiedecimaleEncours.length; j++) {
      const i = j + 1
      let buf = '$' + stor.partiedecimaleEncours[j] + '$'
      let ok = true
      if ((buf === '$0$') && (stor.Zeros)) {
        ok = false
        if (!isDisa) montatab[0][stor.nbVirg + i].setAttribute('class', 'klik')
        montatab[0][stor.nbVirg + i].num = j
        if (!isDisa) montatab[0][stor.nbVirg + i].addEventListener('click', mafonczeroDec, false)
      }
      if (buf === '$ $') {
        ok = false
        buf = ' '
        if (stor.Espaces) {
          if (!isDisa) montatab[0][stor.nbVirg + i].setAttribute('class', 'klikesp')
          if (!isDisa) {
            buf = '_'
          } else {
            buf = '$\\text{ }$'
          }
          montatab[0][stor.nbVirg + i].num = j
          if (!isDisa) montatab[0][stor.nbVirg + i].addEventListener('click', mafonczeroDec, false)
        }
      }
      j3pAffiche(montatab[0][stor.nbVirg + i], null, buf)
      if (ok) {
        if (!isDisa) montatab[0][stor.nbVirg + i].setAttribute('class', 'klikrien')
      }
      if (stor.Espaces) {
        if (!isDisa) montatab[1][stor.nbVirg + i - 1].setAttribute('class', 'klikespsous')
        montatab[1][stor.nbVirg + i - 1].num = j
        if (!isDisa) montatab[1][stor.nbVirg + i - 1].addEventListener('click', mafoncEspDec, false)
      }
    }

    if ((stor.partieentiereEncours.length === 0) && (ds.MortSubite) && !isDisa) {
      me.sectionCourante()
    }
    j3pAffiche(montatab[0][0], null, '$N = $&nbsp;')
    if (isGood === true) {
      stor.lesdiv.travail.style.color = me.styles.cbien
    }
    if (isGood === false) stor.lesdiv.travail.style.color = me.styles.cfaux
    if (isDisa) vireb()
  }

  function vireb () {
    for (let i = 0; i < stor.bull.length; i++) {
      stor.bull[i].disable()
    }
    j3pEmpty(stor.ttCells[0][1])
    j3pEmpty(stor.ttCells[1][1])
  }

  function boutontannule () {
    stor.partieentiereEncours = stor.partieentiereDeb
    stor.yavirguleEncours = stor.yavirguleDeb
    stor.partiedecimaleEncours = stor.partiedecimaleDeb
    affichenbclik()
  }

  function initSection () {
    ds = getDonnees()
    me.donneesSection = ds

    me.surcharge()
    // Construction de la page
    me.construitStructurePage(ds.structure)
    ds.nbitems = ds.nbetapes * ds.nbrepetitions
    stor.Click = (ds.Mode === 'Click')
    stor.Espaces = (ds.Travail.indexOf('Espaces') !== -1)
    stor.Zeros = (ds.Travail.indexOf('Zéros') !== -1)
    stor.EspacesEnt = stor.Espaces && (ds.Partie.indexOf('Entière') !== -1)
    stor.EspacesDeci = stor.Espaces && (ds.Partie.indexOf('Décimale') !== -1)
    if (stor.Click) {
      // il suffit d’ajouter cette classe au parent pour que notre css importée s’applique
      me.zonesElts.MG.classList.add('bienEcrire')
    }
    me.typederreurs = [0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0]
    me.score = 0
    let montd = 'Bien écrire'
    let montf = ' un nombre décimal'
    if (ds.Partie.indexOf('Décimale') === -1) { montf = ' un nombre entier' }
    if (ds.Travail.indexOf('Espaces') === -1) { montd = 'Supprimer les zéros inutiles dans' }

    me.afficheTitre(montd + montf)

    if (ds.indication) me.indication(me.zones.IG, ds.indication)
    enonceMain()
  }

  function enonceMain () {
    me.videLesZones()

    stor.bull = []
    stor.lesdiv = {}
    stor.yam = false
    const divConteneur = j3pAddElt(me.zonesElts.MG, 'div', '', {
      style: me.styles.etendre('toutpetit.enonce', { padding: '10px' })
    })
    const cellsConsigne = addDefaultTable(divConteneur, 1, 1)
    stor.lesdiv.consigne = cellsConsigne[0][0]
    const cellsTravail = addDefaultTable(divConteneur, 1, 1)
    stor.lesdiv.travail = cellsTravail[0][0]
    stor.lesdiv.explications = j3pAddElt(divConteneur, 'div', '', {
      style: me.styles.toutpetit.correction
    })
    stor.lesdiv.explications.style.color = me.styles.cfaux
    stor.lesdiv.solution = j3pAddElt(divConteneur, 'div', '', {
      style: me.styles.toutpetit.correction
    })
    stor.lesdiv.annule = j3pAddElt(me.zonesElts.MD, 'div', '', {
      style: me.styles.etendre('toutpetit.enonce', { padding: '10px' })
    })
    stor.lesdiv.correction = j3pAddElt(me.zonesElts.MD, 'div', '', {
      style: me.styles.etendre('toutpetit.enonce', { padding: '10px' })
    })
    me.zonesElts.MG.classList.add('fond')

    // determination du nb
    // Si espacesEnt ya forcement une partie entiere
    // Si espacesDEC ya forcement une partie dec
    // Si zeros  partie decimale 2 chances sur 3
    // dans chaque partie , au moins un zero dedans
    stor.partieentiere = 0
    stor.partiedecimale = 0
    stor.partieentierenbchiffres = j3pGetRandomInt(4, 12)
    stor.partiedecimalenbchiffres = j3pGetRandomInt(4, 12)

    if (stor.Espaces) {
      if (!stor.EspacesDeci) {
        stor.partiedecimalenbchiffres = 0
      }
      if (!stor.EspacesEnt) {
        stor.partieentierenbchiffres = j3pGetRandomInt(1, 3)
      }
    }
    if (stor.Zeros) {
      if (ds.Partie.indexOf('Décimale') === -1) {
        stor.partiedecimalenbchiffres = 0
      }
      if (ds.Partie.indexOf('Entière') === -1) {
        stor.partieentierenbchiffres = j3pGetRandomInt(1, 3)
      }
    }

    if (!stor.Espaces) {
      if (ds.Partie.indexOf('Décimale') === -1) { stor.partiedecimalenbchiffres = 0 } else {
        const haz = j3pGetRandomInt(1, 3)
        if (haz === 1) { stor.partieentierenbchiffres = j3pGetRandomInt(1, 3) }
        if (haz === 2) { stor.partiedecimalenbchiffres = j3pGetRandomInt(1, 4) }
        if (haz === 3) {
          stor.partiedecimalenbchiffres = j3pGetRandomInt(1, 4)
          stor.partieentierenbchiffres = j3pGetRandomInt(1, 3)
        }
      }
    }

    stor.TabEnt = []
    for (let i = 0; i < stor.partieentierenbchiffres - 1; i++) {
      stor.TabEnt.push(j3pGetRandomInt(0, 9))
    }
    if (stor.EspacesEnt) { stor.TabEnt.push(j3pGetRandomInt(1, 9)) } else { stor.TabEnt.push(j3pGetRandomInt(0, 9)) }

    stor.TabDeci = []
    for (let i = 0; i < stor.partiedecimalenbchiffres - 1; i++) {
      stor.TabDeci.push(j3pGetRandomInt(0, 9))
    }
    if (stor.EspacesDeci) { stor.TabDeci.push(j3pGetRandomInt(1, 9)) }

    let ouzero
    if (stor.Zeros) {
      if (ds.Partie.indexOf('Entière') !== -1) {
        let cmptzeros = 0
        for (let i = 0; i < stor.TabEnt.length; i++) { if (stor.TabEnt[i] === 0) { cmptzeros++ } }
        let nbzeros = j3pGetRandomInt(0, (stor.partieentierenbchiffres - 1 - cmptzeros))
        let cmpt = 100
        do {
          cmpt--
          ouzero = j3pGetRandomInt(1, stor.partieentierenbchiffres)
          if (stor.TabEnt[ouzero] !== 0) {
            stor.TabEnt[ouzero] = 0
            nbzeros--
          }
        } while ((nbzeros > 0) && (cmpt > 0))
      }
      if (stor.partiedecimalenbchiffres > 1) {
        let cmptzeros = 0
        for (let i = 0; i < stor.TabDeci.length; i++) { if (stor.TabDeci[i] === 0) { cmptzeros++ } }
        let nbzeros = j3pGetRandomInt(0, (stor.partiedecimalenbchiffres - 1 - cmptzeros))
        let cmpt = 100
        do {
          cmpt--
          ouzero = j3pGetRandomInt(0, (stor.partiedecimalenbchiffres - 1))
          if (stor.TabDeci[ouzero] !== 0) {
            stor.TabDeci[ouzero] = 0
            nbzeros--
          }
        } while ((nbzeros > 0) && (cmpt > 0))
      }

      if (ds.Partie.indexOf('Décimale') === -1) {
        const ni = j3pGetRandomInt(1, 2)
        for (let i = 0; i < ni; i++) { stor.TabEnt.push(0) }
      } else if (ds.Partie.indexOf('Entière') === -1) {
        const ni = j3pGetRandomInt(1, 2)
        for (let i = 0; i < ni; i++) { stor.TabDeci.push(0) }
      } else {
        const haz = j3pGetRandomInt(1, 3)
        if (haz === 1) {
          const ni = j3pGetRandomInt(1, 2)
          for (let i = 0; i < ni; i++) { stor.TabEnt.push(0) }
        }
        if (haz === 2) {
          const ni = j3pGetRandomInt(1, 2)
          for (let i = 0; i < ni; i++) { stor.TabDeci.push(0) }
        }
        if (haz === 3) {
          const ni = j3pGetRandomInt(1, 2)
          for (let i = 0; i < ni; i++) { stor.TabDeci.push(0) }
          const ni2 = j3pGetRandomInt(1, 2)
          for (let i = 0; i < ni2; i++) { stor.TabEnt.push(0) }
        }
      }
    }

    stor.partieentiereDeb = ''
    for (let i = stor.TabEnt.length - 1; i > -1; i--) {
      if (stor.EspacesEnt) {
        if ((j3pGetRandomInt(0, 2) === 0) && (i !== stor.TabEnt.length - 1)) { stor.partieentiereDeb += ' ' }
      }
      stor.partieentiereDeb += stor.TabEnt[i]
    }
    stor.partiedecimaleDeb = ''
    for (let i = 0; i < stor.TabDeci.length; i++) {
      if (stor.EspacesDeci) {
        if (j3pGetRandomInt(0, 2) === 0) { stor.partiedecimaleDeb += ' ' }
      }
      stor.partiedecimaleDeb += stor.TabDeci[i]
    }
    if (stor.partiedecimaleDeb.length === 0) {
      stor.yavirguleDeb = (j3pGetRandomInt(0, 4) > 3)
      if (ds.Partie.indexOf('Décimale') === -1) { stor.yavirguleDeb = false }
    } else {
      stor.yavirguleDeb = true
    }

    if (!stor.Espaces) {
      if (stor.partieentiereDeb.length > 3) {
        stor.partieentiereDeb = stor.partieentiereDeb.substring(0, stor.partieentiereDeb.length - 3) + ' ' + stor.partieentiereDeb.substring(stor.partieentiereDeb.length - 3)
      }
      if (stor.partieentiereDeb.length > 7) {
        stor.partieentiereDeb = stor.partieentiereDeb.substring(0, stor.partieentiereDeb.length - 7) + ' ' + stor.partieentiereDeb.substring(stor.partieentiereDeb.length - 7)
      }
      if (stor.partieentiereDeb.length > 11) {
        stor.partieentiereDeb = stor.partieentiereDeb.substring(0, stor.partieentiereDeb.length - 11) + ' ' + stor.partieentiereDeb.substring(stor.partieentiereDeb.length - 11)
      }
      if (stor.partieentiereDeb.length > 15) {
        stor.partieentiereDeb = stor.partieentiereDeb.substring(0, stor.partieentiereDeb.length - 15) + ' ' + stor.partieentiereDeb.substring(stor.partieentiereDeb.length - 15)
      }

      if (stor.partiedecimaleDeb.length > 3) {
        stor.partiedecimaleDeb = stor.partiedecimaleDeb.substring(0, 3) + ' ' + stor.partiedecimaleDeb.substring(3)
      }
      if (stor.partiedecimaleDeb.length > 7) {
        stor.partiedecimaleDeb = stor.partiedecimaleDeb.substring(0, 7) + ' ' + stor.partiedecimaleDeb.substring(7)
      }
      if (stor.partiedecimaleDeb.length > 11) {
        stor.partiedecimaleDeb = stor.partiedecimaleDeb.substring(0, 11) + ' ' + stor.partiedecimaleDeb.substring(11)
      }
      if (stor.partiedecimaleDeb.length > 15) {
        stor.partiedecimaleDeb = stor.partiedecimaleDeb.substring(0, 15) + ' ' + stor.partiedecimaleDeb.substring(15)
      }
    }
    let aafiiche = ''
    for (let i = 0; i < stor.partieentiereDeb.length; i++) {
      if (stor.partieentiereDeb[i] === ' ') { aafiiche += '\\text{ }' } else {
        aafiiche += stor.partieentiereDeb[i]
      }
    }
    if (stor.yavirguleDeb) { aafiiche += ',' }
    for (let i = 0; i < stor.partiedecimaleDeb.length; i++) {
      if (stor.partiedecimaleDeb[i] === ' ') { aafiiche += '\\text{ }' } else {
        aafiiche += stor.partiedecimaleDeb[i]
      }
    }

    if (stor.Click) {
      stor.ttCells = addDefaultTable(stor.lesdiv.consigne, 3, 2)
      if (stor.Espaces) {
        j3pAffiche(stor.ttCells[0][0], null, '<b>' + ds.textes.consigne2 + '</b>')
        stor.bull.push(new BulleAide(stor.ttCells[0][1], ds.textes.aideesp, { place: 7 }))
      }
      if (stor.Zeros) {
        j3pAffiche(stor.ttCells[1][0], null, '<b>' + ds.textes.consigne + '</b>')
        stor.bull.push(new BulleAide(stor.ttCells[1][1], ds.textes.aidezero, { place: 7 }))
      }
      // j3pAffiche(stor.ttCells[2][0], null, '\n' + ds.textes.consigne3, { a: aafiiche })
      stor.partieentiereEncours = stor.partieentiereDeb
      stor.yavirguleEncours = stor.yavirguleDeb
      stor.partiedecimaleEncours = stor.partiedecimaleDeb

      affichenbclik()
      if ((!me.MortSubite) || (stor.Espaces)) {
        j3pAjouteBouton(stor.lesdiv.annule, boutontannule, { id: 'BoutAnnuler', className: 'MepBoutons', value: 'Annuler' })
      }
    } else {
      let uncont = stor.partieentiereDeb
      if (stor.yavirguleDeb) {
        uncont += ','
        uncont += stor.partiedecimaleDeb
      }

      j3pAffiche(stor.lesdiv.consigne, '', '<b>' + ds.textes.consigne2bis + '</b>\n\n', { a: aafiiche })
      const cellsTravail2 = addDefaultTable(stor.lesdiv.travail, 1, 2)
      // ZoneStyleMathquill1 sait pas gérer des éléments, seulement des ids, on en ajoute un ici
      stor.mazonem = new ZoneStyleMathquill1(cellsTravail2[0][1], {
        contenu: uncont,
        restric: '0123456789,. ', // FIXME faudrait virer cet espace fin insécable (ou le remplacer par une espace normale), si c’est indispensable mettre un commentaire pour expliquer pourquoi
        limitenb: 50,
        limite: 1000

      })
      j3pAffiche(cellsTravail2[0][0], null, '$N = $&nbsp;')
    }

    stor.lesdiv.travail.classList.add('travail')
    stor.lesdiv.consigne.classList.add('enonce')
    me.finEnonce()
  }

  switch (this.etat) {
    case 'enonce': {
      // code exécuté au lancement de la section
      if (this.debutDeLaSection) {
        initSection()
      } else {
        // A chaque répétition, on nettoie la scène
        // A commenter si besoin
        enonceMain()
      }
      break // case "enonce":
    }

    case 'correction': {
      // On teste si une réponse a été saisie
      if (stor.lesdiv.correction === undefined) return

      if (!yareponse()) {
        // ici on donne le message à afficher si une ou plusieurs zone(s) n’est pas complétée (tout d’abord on met en rouge : styles.cfaux)
        stor.lesdiv.correction.style.color = this.styles.cfaux
        stor.lesdiv.correction.innerHTML = reponseIncomplete
        stor.mazonem.focus()
        this.afficheBoutonValider()
        return
      }
      // Une réponse a été saisie
      let yaexplik = false
      let yasoluce = false
      // Bonne réponse
      if (estBonneReponse()) {
        this.score++
        stor.lesdiv.correction.style.color = this.styles.cbien
        stor.lesdiv.correction.innerHTML = cBien

        j3pEmpty(stor.lesdiv.annule)
        if (!stor.Click) {
          stor.mazonem.disable()
          stor.mazonem.corrige(true)
        } else {
          affichenbclik(true, true)
        }

        this.typederreurs[0]++
        this.cacheBoutonValider()
        this.etat = 'navigation'
        this.sectionCourante()
      } else {
        // Pas de bonne réponse
        stor.lesdiv.correction.style.color = this.styles.cfaux

        // A cause de la limite de temps :
        if (this.isElapsed) { // limite de temps
          stor.lesdiv.correction.innerHTML = tempsDepasse
          this.typederreurs[10]++
          this.cacheBoutonValider()
          j3pEmpty(stor.lesdiv.annule)
          if (!stor.Click) {
            stor.mazonem.disable()
            stor.mazonem.corrige(false)
            stor.mazonem.barre()
          } else {
            affichenbclik(true, false)
          }

          if (stor.yam) {
            yaexplik = true
            j3pAffiche(stor.lesdiv.explications, null, 'Tu as supprimé un <b>zéro utile</b> !\n')
          }
          if (me.ErrPlusieursVirgule) {
            yaexplik = true
            j3pAffiche(stor.lesdiv.explications, null, ds.textes.errtropvirgule)
          }
          if (me.ErrVirguleSeule) {
            yaexplik = true
            j3pAffiche(stor.lesdiv.explications, null, ds.textes.errvirguleseule)
          }
          if (me.ErrEspace) {
            this.typederreurs[6]++
            yaexplik = true
            j3pAffiche(stor.lesdiv.explications, null, ds.textes.errespace)
          }
          if (me.ErrDecimaleEspace) {
            this.typederreurs[6]++
            yaexplik = true
            j3pAffiche(stor.lesdiv.explications, null, ds.textes.errespacedec)
          }
          if (me.ErrZerosInutTrop) {
            yaexplik = true
            this.typederreurs[3]++
            j3pAffiche(stor.lesdiv.explications, null, ds.textes.errZerosInutTrop)
          }

          if (me.ErrZerosEnMoins) {
            this.typederreurs[3]++
            yaexplik = true
            j3pAffiche(stor.lesdiv.explications, null, ds.textes.errZerosEnMoins)
          }

          if (me.Err2esp) {
            this.typederreurs[6]++
            yaexplik = true
            j3pAffiche(stor.lesdiv.explications, null, ds.textes.err2esp)
          }
          if (me.ErrespVirg) {
            this.typederreurs[6]++
            yaexplik = true
            j3pAffiche(stor.lesdiv.explications, null, ds.textes.errespVirg)
          }
          if (me.ErrespVirg2) {
            this.typederreurs[6]++
            yaexplik = true
            j3pAffiche(stor.lesdiv.explications, null, ds.textes.errespVirg2)
          }

          let sol1 = ''
          let ok = false
          for (let i = 0; i < stor.partieentiereDeb.length; i++) {
            if (stor.partieentiereDeb[i] !== ' ') {
              if (stor.partieentiereDeb[i] !== '0') { ok = true }
              if (ok) { sol1 += stor.partieentiereDeb[i] }
            }
          }
          if (sol1.length > 3) {
            sol1 = sol1.substring(0, sol1.length - 3) + ' ' + sol1.substring(sol1.length - 3)
          }
          if (sol1.length > 7) {
            sol1 = sol1.substring(0, sol1.length - 7) + ' ' + sol1.substring(sol1.length - 7)
          }
          if (sol1.length > 11) {
            sol1 = sol1.substring(0, sol1.length - 11) + ' ' + sol1.substring(sol1.length - 11)
          }
          if (sol1.length > 15) {
            sol1 = sol1.substring(0, sol1.length - 15) + ' ' + sol1.substring(sol1.length - 15)
          }

          if (sol1 === '') sol1 = '0'
          let sol2 = ''
          ok = false
          for (let i = stor.partiedecimaleDeb.length - 1; i > -1; i--) {
            if (stor.partiedecimaleDeb[i] !== ' ') {
              if (stor.partiedecimaleDeb[i] !== '0') { ok = true }
              if (ok) { sol2 = stor.partiedecimaleDeb[i] + sol2 }
            }
          }
          if (sol2.length > 3) {
            sol2 = sol2.substring(0, 3) + ' ' + sol2.substring(3)
          }
          if (sol2.length > 7) {
            sol2 = sol2.substring(0, 7) + ' ' + sol2.substring(7)
          }
          if (sol2.length > 11) {
            sol2 = sol2.substring(0, 11) + ' ' + sol2.substring(11)
          }
          if (sol2.length > 15) {
            sol2 = sol2.substring(0, 15) + ' ' + sol2.substring(15)
          }
          let sol3 = ''
          if (sol2.length > 0) { sol3 = ',' }
          let rep = sol1 + sol3 + sol2
          rep = rep.replace(/ /g, '\\text{ }')
          yasoluce = true
          j3pAffiche(stor.lesdiv.solution, null, '\n' + ds.textes.soluce, { a: rep })
          this.etat = 'navigation'
          this.sectionCourante()
        } else { // Réponse fausse :
          stor.lesdiv.correction.innerHTML = cFaux

          if (this.essaiCourant < this.donneesSection.nbchances) {
            stor.lesdiv.correction.innerHTML += '<br>' + essaieEncore

            this.typederreurs[1]++
            // indication éventuelle ici
          } else { // Erreur au nème essai
            this.cacheBoutonValider()
            j3pEmpty(stor.lesdiv.annule)
            if (!stor.Click) {
              stor.mazonem.disable()
              stor.mazonem.corrige(false)
              stor.mazonem.barre()
            } else {
              affichenbclik(true, false)
            }
            if (stor.yam) {
              yaexplik = true
              j3pAffiche(stor.lesdiv.explications, null, 'Tu as supprimé un <b>zéro utile</b> !\n')
            }
            if (me.ErrPlusieursVirgule) {
              yaexplik = true
              j3pAffiche(stor.lesdiv.explications, null, ds.textes.errtropvirgule)
            }
            if (me.ErrVirguleSeule) {
              yaexplik = true
              j3pAffiche(stor.lesdiv.explications, null, ds.textes.errvirguleseule)
            }
            if (me.ErrEspace) {
              this.typederreurs[6]++
              yaexplik = true
              j3pAffiche(stor.lesdiv.explications, null, ds.textes.errespace)
            }
            if (me.ErrDecimaleEspace) {
              this.typederreurs[6]++
              yaexplik = true
              j3pAffiche(stor.lesdiv.explications, null, ds.textes.errespacedec)
            }
            if (me.ErrZerosInutTrop) {
              yaexplik = true
              this.typederreurs[3]++
              j3pAffiche(stor.lesdiv.explications, null, ds.textes.errZerosInutTrop)
            }

            if (me.ErrZerosEnMoins) {
              this.typederreurs[3]++
              yaexplik = true
              j3pAffiche(stor.lesdiv.explications, null, ds.textes.errZerosEnMoins)
            }

            if (me.Err2esp) {
              this.typederreurs[6]++
              yaexplik = true
              j3pAffiche(stor.lesdiv.explications, null, ds.textes.err2esp)
            }
            if (me.ErrespVirg) {
              this.typederreurs[6]++
              yaexplik = true
              j3pAffiche(stor.lesdiv.explications, null, ds.textes.errespVirg)
            }
            if (me.ErrespVirg2) {
              this.typederreurs[6]++
              yaexplik = true
              j3pAffiche(stor.lesdiv.explications, null, ds.textes.errespVirg2)
            }

            let sol1 = ''
            let ok = false
            for (let i = 0; i < stor.partieentiereDeb.length; i++) {
              if (stor.partieentiereDeb[i] !== ' ') {
                if (stor.partieentiereDeb[i] !== '0') { ok = true }
                if (ok) { sol1 += stor.partieentiereDeb[i] }
              }
            }
            if (sol1.length > 3) {
              sol1 = sol1.substring(0, sol1.length - 3) + ' ' + sol1.substring(sol1.length - 3)
            }
            if (sol1.length > 7) {
              sol1 = sol1.substring(0, sol1.length - 7) + ' ' + sol1.substring(sol1.length - 7)
            }
            if (sol1.length > 11) {
              sol1 = sol1.substring(0, sol1.length - 11) + ' ' + sol1.substring(sol1.length - 11)
            }
            if (sol1.length > 15) {
              sol1 = sol1.substring(0, sol1.length - 15) + ' ' + sol1.substring(sol1.length - 15)
            }
            if (sol1 === '') sol1 = '0'
            let sol2 = ''
            ok = false
            for (let i = stor.partiedecimaleDeb.length - 1; i > -1; i--) {
              if (stor.partiedecimaleDeb[i] !== ' ') {
                if (stor.partiedecimaleDeb[i] !== '0') { ok = true }
                if (ok) { sol2 = stor.partiedecimaleDeb[i] + sol2 }
              }
            }
            if (sol2.length > 3) {
              sol2 = sol2.substring(0, 3) + ' ' + sol2.substring(3)
            }
            if (sol2.length > 7) {
              sol2 = sol2.substring(0, 7) + ' ' + sol2.substring(7)
            }
            if (sol2.length > 11) {
              sol2 = sol2.substring(0, 11) + ' ' + sol2.substring(11)
            }
            if (sol2.length > 15) {
              sol2 = sol2.substring(0, 15) + ' ' + sol2.substring(15)
            }
            let sol3 = ''
            if (sol2.length > 0) { sol3 = ',' }
            let rep = sol1 + sol3 + sol2
            rep = rep.replace(/ /g, '\\text{ }')
            yasoluce = true
            j3pAffiche(stor.lesdiv.solution, null, '\n' + ds.textes.soluce, { a: rep })

            stor.lesdiv.correction.innerHTML += '<br>' + regardeCorrection
            this.typederreurs[2]++
            this.etat = 'navigation'
            this.sectionCourante()
          }
        }
      }
      if (ds.theme === 'zonesAvecImageDeFond') {
        if (yasoluce) {
          stor.lesdiv.explications.classList.add('explique')
        }
        if (yaexplik) {
          stor.lesdiv.solution.classList.add('correction')
        }
      }
      this.finCorrection()
      break // case "correction":
    }

    case 'navigation':
      if (this.sectionTerminee()) {
        // On détermine la phrase d’état renvoyée par la section
        const sco = this.score / this.donneesSection.nbitems
        let peNum
        if (sco >= 0.8) peNum = 1
        else if (sco >= 0.55) peNum = 2
        else if (sco >= 0.3) peNum = 3
        else peNum = 5
        let peNumAjout = 0
        let compt = this.typederreurs[2] / 4 // FIXME pourquoi diviser par 4 ??? Il faut tenir compte du nb de répétitions
        if (this.typederreurs[6] > compt) {
          peNumAjout = 2
          compt = this.typederreurs[6]
        }
        if (this.typederreurs[3] > compt) {
          peNumAjout = 1
        }
        this.parcours.pe = this.donneesSection[`pe_${peNum + peNumAjout}`]
      } else {
        this.etat = 'enonce'
      }
      this.finNavigation()
      break // case "navigation":
  }
}
