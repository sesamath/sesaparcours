import { j3pAddElt, j3pArrondi, j3pEmpty, j3pGetNewId, j3pGetRandomBool, j3pGetRandomInt, j3pShowError, j3pShuffle } from 'src/legacy/core/functions'
import { verifNombreBienEcrit } from 'src/legacy/outils/fonctions/ecritureNombre'
import { j3pAffiche } from 'src/lib/mathquill/functions'
import ZoneStyleMathquill1 from 'src/legacy/outils/zoneStyleMathquill/ZoneStyleMathquill1'
import { addDefaultTable } from 'src/legacy/themes/table'
import { ecrisBienMathquill } from 'src/legacy/outils/zoneStyleMathquill/functions'
import { j3pCreeSVG } from 'src/legacy/core/functionsSvg'
import { getMtgCore } from 'src/lib/outils/mathgraph'
import textesGeneriques from 'src/lib/core/textes'

const { cBien, cFaux, essaieEncore, regardeCorrection, reponseIncomplete, tempsDepasse } = textesGeneriques

/**
 * Ajouter une description ici
 * @fileOverview
 * @author Tommy Barroy <t.barroy@laposte.net>
 * @since 2017-10
 */
export const params = {
  outils: ['mathquill'],
  /**
   * Ce qui est paramétrable dans l’éditeur de graphe
   * @type {LegacySectionParamsParametres}
   */
  parametres: [
    ['nbrepetitions', 9, 'entier', 'Nombre de répétitions de la section'],
    ['nbchances', 2, 'entier', 'Nombre de tentatives possibles.'],
    ['limite', 0, 'entier', 'Temps disponible par répétition, en secondes'],
    ['Enonce', 'les deux', 'liste', 'Type d’énoncé', ['nombre', 'axe', 'les deux']],
    ['Espace', true, 'boolean', '<u>true</u>: Les espaces entre chaque groupe sont exigés.'],
    ['Millier', true, 'boolean', '<u>true</u>: Rang proposé.'],
    ['Centaine', true, 'boolean', '<u>true</u>: Rang proposé. '],
    ['Dizaine', true, 'boolean', '<u>true</u>: Rang proposé. '],
    ['Unite', true, 'boolean', '<u>true</u>: Rang proposé.'],
    ['Dixieme', true, 'boolean', '<u>true</u>: Rang proposé.'],
    ['Centieme', true, 'boolean', '<u>true</u>: Rang proposé..'],
    ['Millieme', true, 'boolean', '<u>true</u>: Rang proposé.'],
    ['theme', 'standard', 'liste', 'Modèle pour l’affichage', ['standard', 'zonesAvecImageDeFond']]

  ]
}

/**
 * section encadrer01
 * @this {Parcours}
 */
export default function main () {
  const me = this
  const stor = this.storage
  let ds = me.donneesSection

  function yareponse () {
    if (me.isElapsed) { return true }
    if (stor.zoneP.reponse() === '') {
      stor.zoneP.focus()
      return false
    }
    if (stor.zoneG.reponse() === '') {
      stor.zoneG.focus()
      return false
    }
    return true
  }
  function faisChoixExo () {
    const e = ds.lesExos.pop()
    const nbav = j3pGetRandomInt(1, 3)
    let nb = 0
    for (let i = 0; i < nbav; i++) {
      const lim = i !== nbav - 1 ? 0 : 1
      if (i === 0) {
        stor.lechip = j3pGetRandomInt(lim, 9)
        nb += stor.lechip * Math.pow(10, i)
      } else {
        nb += j3pGetRandomInt(lim, 9) * Math.pow(10, i)
      }
    }
    const nbap = j3pGetRandomInt(1, 3)
    let nba = 0
    for (let i = 0; i < nbap; i++) {
      let lim = i !== 0 ? 0 : 1
      let lim2 = 9
      if (e.quest === 'axe') {
        if (i === nbap - 1) {
          lim = 2
          lim2 = 8
        }
      }
      nba += j3pGetRandomInt(lim, lim2) * Math.pow(10, i)
    }
    stor.nb = j3pArrondi(nb * e.rang + nba * (e.rang * Math.pow(10, -nbap)), 6)
    stor.nbp = j3pArrondi(nb * e.rang, 3)
    stor.nbg = j3pArrondi((nb + 1) * e.rang, 3)
    stor.af1 = j3pArrondi(Math.trunc(nb / 10) * e.rang * 10, 3)
    stor.af2 = stor.af1 + e.rang * 10
    stor.moov = e.rang * 10
    stor.rang = e.rang
    stor.quest = e.quest
  }
  function isRepOk () {
    stor.errEcrit = []
    stor.repEleveP = stor.zoneP.reponse()
    stor.repEleveG = stor.zoneG.reponse()

    let okp = true
    let okg = true

    const testP = verifNombreBienEcrit(stor.repEleveP, ds.Espace, true)
    if (!testP.good) {
      stor.errEcrit.push(testP.remede)
      stor.zoneP.corrige(false)
      okp = false
    }
    const testG = verifNombreBienEcrit(stor.repEleveG, ds.Espace, true)
    if (!testG.good) {
      if (stor.errEcrit.indexOf(testG.remede) === -1) stor.errEcrit.push(testG.remede)
      stor.zoneG.corrige(false)
      okg = false
    }
    if (okp) {
      stor.repEleveP = testP.nb
      if (stor.repEleveP !== stor.nbp) {
        stor.errRep = true
        stor.zoneP.corrige(false)
        okp = false
      }
    }
    if (okg) {
      stor.repEleveG = testG.nb
      if (stor.repEleveG !== stor.nbg) {
        stor.errRep = true
        stor.zoneG.corrige(false)
        okg = false
      }
    }
    return okg && okp
  }
  function creeLesDiv () {
    stor.lesdiv = {}
    stor.lesdiv.conteneur = j3pAddElt(me.zones.MG, 'div', { style: me.styles.etendre('toutpetit.enonce', { padding: '10px' }) })
    stor.lesdiv.consigne = addDefaultTable(stor.lesdiv.conteneur, 1, 1)[0][0]
    stor.lesdiv.travail = addDefaultTable(stor.lesdiv.conteneur, 1, 1)[0][0]
    stor.lesdiv.explications = addDefaultTable(stor.lesdiv.conteneur, 1, 1)[0][0]
    stor.lesdiv.solution = addDefaultTable(stor.lesdiv.conteneur, 1, 1)[0][0]
    stor.lesdiv.correction = j3pAddElt(me.zones.MD, 'div', { style: me.styles.etendre('toutpetit.enonce', { padding: '10px' }) })
    stor.lesdiv.explications.style.color = me.styles.cfaux
    stor.lesdiv.solution.style.color = me.styles.petit.correction.color
    stor.lesdiv.travail.classList.add('travail')
    stor.lesdiv.consigne.classList.add('enonce')
    me.zonesElts.MG.classList.add('fond')
  }
  function getDonnees () {
    return {
      typesection: 'college', //

      // Nombre de répétitions de l’exercice
      nbrepetitions: 9,
      nbetapes: 1,

      limite: 0,
      Espace: true,
      Millier: true,
      Centaine: true,
      Dizaine: true,
      Unite: true,
      Dixieme: true,
      Centieme: true,
      Millieme: true,
      Enonce: 'les deux',

      // Si indication !== "" alors un lien "Indication" s’affiche dans la zone Inférieure Gauche (zones.IG
      // une indication<br>de deux lignes...";
      indication: '',

      // pour le cas de presentation1bis
      // En effet pas d’indication possible pour cette présentation
      // surchargeindication: false;

      // Nombre de chances dont dispose l’él§ve pour répondre à la question
      nbchances: 2,

      structure: 'presentation1bis', //  || "presentation2" || "presentation3"  || "presentation1bis"

      // Paramètres de la section, avec leurs valeurs par défaut.

      /*
          Les textes présents dans la section
          Sont donc accessibles dans le code par donneesSection.textes.phrase1
          Possibilité d’écrire dans le code :
          var lestextes: donneesSection.textes;
          puis accès à l’aide de lestextes.phrase1
          */
      textes: {

        aide: 'Attention !',
        aidezero: 'Clique sur un zéro pour le supprimer.',
        aideesp: 'Clique sur une espace par la faire disparaître. <BR> Clique sous deux chiffres pour insérer une espace.',
        soluce: 'Il fallait écrire $£a$ .',
        AttVirg: 'Tu ne peux pas supprimer la virgule, <BR> Il reste des chiffres dans la partie décimale !',
        consigne: '<b>Ecris en chiffres ce nombre:</b> <BR> £a.',
        consigne3: ' $N: £a$ <BR><BR><BR>',
        consigne2bis: 'Ecris correctement ce nombre: $£a$',
        errtropvirgule: 'Il ne peut y avoir plusieurs virgules dans un nombre !',
        errvirguleseule: 'Il ne peut pas y avoir de virgule sans partie décimale !',
        errespace: 'Dans la partie entière, les chiffres doivent être regroupés par 3 en commençant par la droite !',
        errespacedec: 'Dans la partie décimale, les chiffres doivent être regroupés par 3 en commençant par la gauche !',
        errDeprang2: 'Supprime les zéros inutiles !',
        errespVirg: 'Il ne doit pas y avoir d’espace à côté de la virgule !',
        errespVirg2: 'Il ne doit pas y avoir d’espace à la fin de la partie entière !',
        err2esp: 'Tu as mis 2 espaces côte à côte !',
        errZerosEnMoins: 'Ces deux nombres ne sont pas égaux !',
        errZerosInutTrop: 'Tu dois supprimer tous les zéros inutiles !'

      },

      // Par défaut, pas de temps limite. Décommentez pour avoir un temps limite par défaut. En secondes.

      // limite: 10;
      /*
          Phrase d’état renvoyée par la section
          Si pe=0 alors la section est quantitative et renvoie un score entre 0 et 1.
          Dans le cas où la section est qualitative, lister les différents pe renvoyées :
              pe_1: "toto"
              pe_2: "tata"
              etc
          Dans la partie navigation, il faudra affecter la variable parcours.pe à l’une des précédentes.
          Par exemple,
          parcours.pe: donneesSection.pe_2
          */
      pe: 0
    }
  }
  function PoseQuestion () {
    let buf
    if (stor.quest === 'axe') {
      buf = '\\text{abscisse de A}'
      const tta = addDefaultTable(stor.lesdiv.consigne, 2, 1)
      const svgId = j3pGetNewId()
      const txtFigure = 'TWF0aEdyYXBoSmF2YTEuMAAAABM+TMzNAAJmcv###wEA#wEAAAAAAAAAAANMAAACiAAAAQEAAAAAAAAAAQAAAHf#####AAAAAQAKQ0NhbGNDb25zdAD#####AAJwaQAWMy4xNDE1OTI2NTM1ODk3OTMyMzg0Nv####8AAAABAApDQ29uc3RhbnRlQAkh+1RELRj#####AAAAAQAKQ1BvaW50QmFzZQD#####AQAAAAAQAAAAAAAAAAAAAABACAAAAAAAAAAABQABQDqAAAAAAAJARhmZmZmZmgAAAAIA#####wAAAAAAEAAAAAAAAAAAAAAAQAgAAAAAAAAAAAUAAUByuAAAAAAAQHHTMzMzMzP#####AAAAAQAUQ0Ryb2l0ZURpcmVjdGlvbkZpeGUA#####wAAAAABEAAAAQAAAAIAAAABAT#wAAAAAAAA#####wAAAAEAD0NQb2ludExpZURyb2l0ZQD#####AQAAAAAQAAAAAAAAAAAAAABACAAAAAAAAAAABQABQEKAAAAAAAAAAAAD#####wAAAAEACENWZWN0ZXVyAP####8BAAAAABAAAAEAAAABAAAAAQAAAAQA#####wAAAAEADUNUcmFuc1BhclZlY3QA#####wAAAAX#####AAAAAQALQ1BvaW50SW1hZ2UA#####wEAAAAAEAAAAAAAAAAAAAAAQAgAAAAAAAAAAAUAAAAABAAAAAYAAAAHAP####8AAAAAABAAAAAAAAAAAAAAAEAIAAAAAAAAAAJwMQUAAAAABwAAAAYAAAAHAP####8AAAAAABAAAAAAAAAAAAAAAEAIAAAAAAAAAAAFAAAAAAgAAAAGAAAABwD#####AQAAAAAQAAAAAAAAAAAAAABACAAAAAAAAAAABQAAAAAJAAAABgAAAAcA#####wAAAAAAEAAAAAAAAAAAAAAAQAgAAAAAAAAAAAUAAAAACgAAAAYAAAAHAP####8AAAAAABAAAAAAAAAAAAAAAEAIAAAAAAAAAAAFAAAAAAsAAAAGAAAABwD#####AAAAAAAQAAAAAAAAAAAAAABACAAAAAAAAAAABQAAAAAMAAAABgAAAAcA#####wAAAAAAEAAAAAAAAAAAAAAAQAgAAAAAAAAAAAUAAAAADQAAAAYAAAAHAP####8AAAAAABAAAAAAAAAAAAAAAEAIAAAAAAAAAAAFAAAAAA4AAAAGAAAABwD#####AAAAAAAQAAAAAAAAAAAAAABACAAAAAAAAAAABQAAAAAPAAAABgAAAAcA#####wAAAAAAEAAAAAAAAAAAAAAAQAgAAAAAAAAAAAUAAAAAEAAAAAYAAAAHAP####8BfwAAABAAAAAAAAAAAAAAAEAIAAAAAAAAAAJwMgkAAAAAEQAAAAYAAAAHAP####8AAAAAABAAAAAAAAAAAAAAAEAIAAAAAAAAAAAFAAAAABIAAAAGAAAAAgD#####AAAAAAAQAAAAAAAAAAAAAABACAAAAAAAAAAABQABQHIoAAAAAABAckMzMzMzMwAAAAIA#####wAAAAAAEAAAAAAAAAAAAAAAQAgAAAAAAAAAAAUAAUB0GAAAAAAAQHGzMzMzMzMAAAAFAP####8AAAAAABAAAAEAAAABAAAAAgAAABQAAAAABQD#####AAAAAAAQAAABAAAAAQAAAAIAAAAVAAAAAAYA#####wAAABcAAAAHAP####8BAAAAABAAAAAAAAAAAAAAAEAIAAAAAAAAAAAFAAAAAAgAAAAYAAAABwD#####AQAAAAAQAAAAAAAAAAAAAABACAAAAAAAAAAABQAAAAASAAAAGP####8AAAABAAlDQ2VyY2xlT0EA#####wEAAAAAAAABAAAACAAAABkAAAAIAP####8BAAAAAAAAAQAAABIAAAAa#####wAAAAEAFkNEcm9pdGVQZXJwZW5kaWN1bGFpcmUA#####wEAAAAAEAAAAQAAAAEAAAAIAAAAAwAAAAkA#####wEAAAAAEAAAAQAAAAEAAAASAAAAA#####8AAAABABBDSW50RHJvaXRlQ2VyY2xlAP####8AAAAdAAAAG#####8AAAABABBDUG9pbnRMaWVCaXBvaW50AP####8BAAAAABAAAAAAAAAAAAAAAEAIAAAAAAAAAAAFAAIAAAAfAAAACwD#####AQAAAAAQAAAAAAAAAAAAAABACAAAAAAAAAAABQABAAAAHwAAAAoA#####wAAAB4AAAAcAAAACwD#####AQAAAAAQAAAAAAAAAAAAAABACAAAAAAAAAAABQACAAAAIgAAAAsA#####wEAAAAAEAAAAAAAAAAAAAAAQAgAAAAAAAAAAAUAAQAAACL#####AAAAAQAIQ1NlZ21lbnQA#####wB#AAAAEAAAAQAAAAcAAAAgAAAAIQAAAAwA#####wB#AAAAEAAAAQAAAAcAAAAjAAAAJAAAAAYA#####wAAABYAAAAHAP####8BfwAAABAAAAAAAAAAAAAAAEAIAAAAAAAAAAAFAAAAAAoAAAAnAAAACAD#####AX8AAAAAAAUAAAAKAAAAKAAAAAkA#####wF#AAAAEAAAAQAAAAUAAAAKAAAAAwAAAAoA#####wAAACoAAAApAAAACwD#####AX8AAAAQAAAAAAAAAAAAAABACAAAAAAAAAAABQACAAAAKwAAAAsA#####wF#AAAAEAAAAAAAAAAAAAAAQAgAAAAAAAAAAAUAAQAAACsAAAAMAP####8AAAAAABAAAAEAAAADAAAALAAAAC0AAAAHAP####8BAAAAABAAAAAAAAAAAAAAAEAIAAAAAAAAAAAFAAAAACwAAAAGAAAABwD#####AQAAAAAQAAAAAAAAAAAAAABACAAAAAAAAAAABQAAAAAtAAAABgAAAAwA#####wAAAAAAEAAAAQAAAAMAAAAvAAAAMAAAAAcA#####wEAAAAAEAAAAAAAAAAAAAAAQAgAAAAAAAAAAAUAAAAALwAAAAYAAAAHAP####8BAAAAABAAAAAAAAAAAAAAAEAIAAAAAAAAAAAFAAAAADAAAAAGAAAADAD#####AAAAAAAQAAABAAAAAwAAADIAAAAzAAAABwD#####AQAAAAAQAAAAAAAAAAAAAABACAAAAAAAAAAABQAAAAAyAAAABgAAAAcA#####wEAAAAAEAAAAAAAAAAAAAAAQAgAAAAAAAAAAAUAAAAAMwAAAAYAAAAMAP####8AAAAAABAAAAEAAAADAAAANQAAADYAAAAHAP####8BAAAAABAAAAAAAAAAAAAAAEAIAAAAAAAAAAAFAAAAADUAAAAGAAAABwD#####AQAAAAAQAAAAAAAAAAAAAABACAAAAAAAAAAABQAAAAA2AAAABgAAAAwA#####wAAAAAAEAAAAQAAAAMAAAA4AAAAOQAAAAcA#####wEAAAAAEAAAAAAAAAAAAAAAQAgAAAAAAAAAAAUAAAAAOAAAAAYAAAAHAP####8BAAAAABAAAAAAAAAAAAAAAEAIAAAAAAAAAAAFAAAAADkAAAAGAAAADAD#####AAAAAAAQAAABAAAAAwAAADsAAAA8AAAABwD#####AQAAAAAQAAAAAAAAAAAAAABACAAAAAAAAAAABQAAAAA7AAAABgAAAAcA#####wEAAAAAEAAAAAAAAAAAAAAAQAgAAAAAAAAAAAUAAAAAPAAAAAYAAAAMAP####8AAAAAABAAAAEAAAADAAAAPgAAAD8AAAAHAP####8BAAAAABAAAAAAAAAAAAAAAEAIAAAAAAAAAAAFAAAAAD4AAAAGAAAABwD#####AQAAAAAQAAAAAAAAAAAAAABACAAAAAAAAAAABQAAAAA#AAAABgAAAAwA#####wAAAAAAEAAAAQAAAAMAAABBAAAAQgAAAAcA#####wEAAAAAEAAAAAAAAAAAAAAAQAgAAAAAAAAAAAUAAAAAQQAAAAYAAAAHAP####8BAAAAABAAAAAAAAAAAAAAAEAIAAAAAAAAAAAFAAAAAEIAAAAGAAAADAD#####AQAAAAAQAAABAAAAAwAAAEQAAABFAAAABwD#####AQAAAAAQAAAAAAAAAAAAAABACAAAAAAAAAAABQAAAABEAAAABgAAAAcA#####wEAAAAAEAAAAAAAAAAAAAAAQAgAAAAAAAAAAAUAAAAARQAAAAYAAAAMAP####8AAAAAABAAAAEAAAADAAAARwAAAEgAAAAHAP####8BAAAAABAAAAAAAAAAAAAAAEAIAAAAAAAAAAAFAAAAAEcAAAAGAAAABwD#####AQAAAAAQAAAAAAAAAAAAAABACAAAAAAAAAAABQAAAABIAAAABgAAAAwA#####wAAAAAAEAAAAQAAAAMAAABKAAAASwAAAAcA#####wEAAAAAEAAAAAAAAAAAAAAAQAgAAAAAAAAAAAUAAAAASgAAAAYAAAAHAP####8BAAAAABAAAAAAAAAAAAAAAEAIAAAAAAAAAAAFAAAAAEsAAAAGAAAADAD#####AAAAAAAQAAABAAAAAwAAAE0AAABOAAAABQD#####AQAAAAAQAAABAAAAAwAAAAoAAAAJAAAAAAYA#####wAAAFAAAAAHAP####8BAAAAABAAAAAAAAAAAAAAAEAIAAAAAAAAAAAFAAAAACwAAABRAAAABwD#####AQAAAAAQAAAAAAAAAAAAAABACAAAAAAAAAAABQAAAAAtAAAAUQAAAAwA#####wAAAAAAEAAAAQAAAAMAAABSAAAAUwAAAAcA#####wEAAAAAEAAAAAAAAAAAAAAAQAgAAAAAAAAAAAUAAAAAUgAAAFEAAAAHAP####8BAAAAABAAAAAAAAAAAAAAAEAIAAAAAAAAAAAFAAAAAFMAAABRAAAADAD#####AQAAAAAQAAABAAAAAwAAAFUAAABWAAAABwD#####AQAAAAAQAAAAAAAAAAAAAABACAAAAAAAAAAABQAAAABVAAAAUQAAAAcA#####wEAAAAAEAAAAAAAAAAAAAAAQAgAAAAAAAAAAAUAAAAAVgAAAFEAAAAMAP####8AAAAAABAAAAEAAAADAAAAWAAAAFkAAAAHAP####8BAAAAABAAAAAAAAAAAAAAAEAIAAAAAAAAAAAFAAAAAFgAAABRAAAABwD#####AQAAAAAQAAAAAAAAAAAAAABACAAAAAAAAAAABQAAAABZAAAAUQAAAAwA#####wAAAAAAEAAAAQAAAAMAAABbAAAAXAAAAAcA#####wEAAAAAEAAAAAAAAAAAAAAAQAgAAAAAAAAAAAUAAAAAWwAAAFEAAAAHAP####8BAAAAABAAAAAAAAAAAAAAAEAIAAAAAAAAAAAFAAAAAFwAAABRAAAADAD#####AAAAAAAQAAABAAAAAwAAAF4AAABf#####wAAAAIADENDb21tZW50YWlyZQD#####AH8AAAEAAnQxAAAAIRQAAAAAAAEAAAAAAAAAAQAAAAAAAAAAAAUmPCYmJwAAAA0A#####wB#AAABAAJ0MgAAACQUAAAAAAABAAAAAAAAAAEAAAAAAAAAAAAGZGhyaHNoAAAAAgD#####AQAA#wAQAAAAAAAAAAAAAABACAAAAAAAAAACYWEJAAFAdWgAAAAAAEBlJmZmZmZmAAAAAwD#####AQAA#wEQAAABAAAAAwAAAGMBP#AAAAAAAAAAAAACAP####8AAAD#ABAAAAAAAAAAAAAAAEAIAAAAAAAAAAAJAAFAc4gAAAAAAEByIzMzMzMzAAAABQD#####AAAA#wAQAAABAAAAAwAAAAIAAABlAAAAAAYA#####wAAAGYAAAAHAP####8BAAD#ABAAAAAAAAAAAAAAAEAIAAAAAAAAAAAJAAAAAGMAAABnAAAACAD#####AQAA#wAAAAMAAABjAAAAaAAAAAoA#####wAAAGQAAABpAAAACwD#####AQAA#wAQAAAAAAAAAAAAAABACAAAAAAAAAAACQACAAAAagAAAAsA#####wEAAP8AEAAAAAAAAAAAAAAAQAgAAAAAAAAAAAkAAQAAAGr#####AAAAAQAJQ1JvdGF0aW9uAP####8AAABjAAAAAUBGgAAAAAAAAAAABwD#####AQAA#wAQAAAAAAAAAAAAAABACAAAAAAAAAAACQAAAABsAAAAbQAAAAcA#####wEAAP8AEAAAAAAAAAAAAAAAQAgAAAAAAAAAAAkAAAAAawAAAG0AAAAHAP####8BAAD#ABAAAAAAAAAAAAAAAEAIAAAAAAAAAAAJAAAAAG4AAABtAAAABwD#####AQAA#wAQAAAAAAAAAAAAAABACAAAAAAAAAAACQAAAABwAAAAbQAAAAcA#####wEAAP8AEAAAAAAAAAAAAAAAQAgAAAAAAAAAAAkAAAAAbwAAAG0AAAAHAP####8BAAD#ABAAAAAAAAAAAAAAAEAIAAAAAAAAAAAJAAAAAHIAAABtAAAADAD#####AAAA#wAQAAABAAAAAwAAAHEAAABzAAAADAD#####AAAA#wAQAAABAAAAAwAAAG4AAABvAAAADQD#####AAAA#wEAAAAAAHAUAAAAAAABAAAAAgAAAAEAAAAAAAAAAAABQf###############w=='
      j3pCreeSVG(tta[1][0], { id: svgId, width: 650, height: 100, style: { border: 'solid black 1px' } })
      stor.mtgAppLecteur.removeAllDoc()
      stor.mtgAppLecteur.addDoc(svgId, txtFigure, true)
      stor.mtgAppLecteur.calculateAndDisplayAll(true)
      let decale = 0
      switch (stor.lechip) {
        case 0:
        case 1:
        case 2:
          if (j3pGetRandomBool()) decale = -1
          break
        case 7:
        case 8:
        case 9:
          if (j3pGetRandomBool()) decale = +1
      }
      const N1 = stor.af1 + decale * stor.moov
      const N2 = stor.af2 + decale * stor.moov
      stor.mtgAppLecteur.setText(svgId, '#t1', '$' + ecrisBienMathquill(j3pArrondi(N1, 3)) + '$', true)
      stor.mtgAppLecteur.setText(svgId, '#t2', '$' + ecrisBienMathquill(j3pArrondi(N2, 3)) + '$', true)
      const p1 = stor.mtgAppLecteur.getPointPosition(svgId, '#p1')
      const p2 = stor.mtgAppLecteur.getPointPosition(svgId, '#p2')
      stor.mtgAppLecteur.setPointPosition(svgId, '#aa', (p2.x - p1.x) * (stor.nb - N1) / (N2 - N1) + p1.x, p1.y, true)

      if (stor.rang === 1) {
        j3pAffiche(tta[0][0], null, '<b>Donne un encadrement de l’abscisse de A entre deux entiers consécutifs.</b>')
      } else {
        j3pAffiche(tta[0][0], null, '<b>Donne un encadrement de l’abscisse de A entre les deux graduations les plus proches.</b>')
      }
    } else {
      buf = ecrisBienMathquill(stor.nb)
      if (stor.rang !== 1) {
        j3pAffiche(stor.lesdiv.consigne, null, '<b>Encadre le nombre $' + ecrisBienMathquill(stor.nb) + '$ entre deux approximations décimales ' + makeR(stor.rang) + '</b>.\n\n')
      } else {
        j3pAffiche(stor.lesdiv.consigne, null, '<b>Encadre le nombre $' + ecrisBienMathquill(stor.nb) + '$ entre deux entiers consécutifs.</b>\n\n')
      }
    }
    const tavg = addDefaultTable(stor.lesdiv.travail, 1, 3)
    j3pAffiche(tavg[0][1], null, '&nbsp; $ < ' + buf + ' < $ &nbsp;')
    stor.zoneP = new ZoneStyleMathquill1(tavg[0][0], { restric: '0123456789., ', enter: function () { me.sectionCourante() } })
    stor.zoneG = new ZoneStyleMathquill1(tavg[0][2], { restric: '0123456789., ', enter: function () { me.sectionCourante() } })
  }

  function makeR (x) {
    switch (x) {
      case 1000: return ' au millier '
      case 100: return ' à la centaine '
      case 10: return ' à la dizaine '
      case 0.1: return ' au dixième '
      case 0.01: return ' au centième '
      case 0.001: return ' au millième '
    }
    return ''
  }
  function afcofo (boolfin) {
    if (stor.errEcrit.length > 0) {
      stor.yaexplik = true
      for (let i = 0; i < stor.errEcrit.length; i++) {
        j3pAffiche(stor.lesdiv.explications, null, stor.errEcrit[i] + '\n')
      }
    }

    if (boolfin) {
      barrelesfo()
      desatout()
      stor.yaco = true
      const arep = (stor.quest === 'axe') ? '\\text{ abscisse de A } ' : ecrisBienMathquill(stor.nb)
      j3pAffiche(stor.lesdiv.solution, null, '$' + ecrisBienMathquill(stor.nbp) + ' < ' + arep + ' < ' + ecrisBienMathquill(stor.nbg) + '$')
    } else { me.afficheBoutonValider() }
  }

  function desatout () {
    stor.zoneP.disable()
    stor.zoneG.disable()
  }

  function barrelesfo () {
    stor.zoneG.barreIfKo()
    stor.zoneP.barreIfKo()
  }

  function initSection () {
    me.donneesSection = getDonnees()
    ds = me.donneesSection

    me.surcharge()
    // Construction de la page
    me.construitStructurePage(me.donneesSection.structure)

    ds.nbitems = ds.nbetapes * ds.nbrepetitions

    me.typederreurs = [0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0]

    me.score = 0

    me.afficheTitre('Encadrer un nombre')
    let lp = []
    if (ds.Millier) lp.push(1000)
    if (ds.Centaine) lp.push(100)
    if (ds.Dizaine) lp.push(10)
    if (ds.Unite) lp.push(1)
    if (ds.Dixieme) lp.push(0.1)
    if (ds.Centieme) lp.push(0.01)
    if (ds.Millieme) lp.push(0.001)
    if (lp.length === 0) lp.push(1)
    lp = j3pShuffle(lp)
    ds.lesExos = []
    for (let i = 0; i < ds.nbitems; i++) {
      ds.lesExos.push({ rang: lp[i % lp.length] })
    }
    ds.lesExos = j3pShuffle(ds.lesExos)

    lp = []
    if (ds.Enonce === 'les deux' || ds.Enonce === 'nombre') lp.push('nb')
    if (ds.Enonce === 'les deux' || ds.Enonce === 'axe') lp.push('axe')
    lp = j3pShuffle(lp)
    for (let i = 0; i < ds.nbitems; i++) {
      ds.lesExos[i].quest = lp[i % lp.length]
    }
    ds.lesExos = j3pShuffle(ds.lesExos)

    // chargement mtg
    getMtgCore({ withMathJax: true })
      .then(
        // success
        (mtgAppLecteur) => {
          stor.mtgAppLecteur = mtgAppLecteur
          stor.listePourCalc = mtgAppLecteur.createList('TWF0aEdyYXBoSmF2YTEuMAAAABI+TMzNAAJmcv###wEA#wEAAAAAAAAAAAQsAAACvQAAAQEAAAAAAAAAAQAAAAP#####AAAAAQAKQ0NhbGNDb25zdAD#####AAJwaQAWMy4xNDE1OTI2NTM1ODk3OTMyMzg0Nv####8AAAABAApDQ29uc3RhbnRlQAkh+1RELRj#####AAAAAQAHQ0NhbGN1bAD#####AAFlAAZleHAoMSn#####AAAAAgAJQ0ZvbmN0aW9uBwAAAAE#8AAAAAAAAAAAAAIA#####wABeAABMAAAAAEAAAAAAAAAAP###############w==')
          enonceMain()
        },
        // failure
        (error) => {
          j3pShowError(error, { message: 'mathgraph n’est pas correctement chargé', mustNotify: true })
        })
      // plantage dans le code de success
      .catch(j3pShowError)
  }

  function metoutvert () {
    stor.zoneP.corrige(true)
    stor.zoneG.corrige(true)
  }
  function enonceMain () {
    // A chaque répétition, on nettoie la scène
    // A commenter si besoin

    me.videLesZones()

    faisChoixExo()

    creeLesDiv()

    PoseQuestion()

    me.finEnonce()
  }

  switch (this.etat) {
    case 'enonce':
      // code exécuté au lancement de la section
      if (this.debutDeLaSection) {
        initSection()
      } else {
        enonceMain()
      }
      break // case "enonce":

    case 'correction':
      // On teste si une réponse a été saisie
      if (ds.theme === 'zonesAvecImageDeFond') {
        stor.lesdiv.explications.classList.remove('explique')
        stor.lesdiv.solution.classList.remove('correction')
        stor.lesdiv.correction.classList.remove('feedback')
      }
      j3pEmpty(stor.lesdiv.explications)

      if (!yareponse()) {
        // ici on donne le message à afficher si une ou plusieurs zone(s) n’est pas complétée (tout d’abord on met en rouge : styles.cfaux)
        stor.lesdiv.correction.style.color = this.styles.cfaux
        stor.lesdiv.correction.innerHTML = reponseIncomplete
        me.afficheBoutonValider()
        return
      } else { // Une réponse a été saisie
        stor.yaexplik = false
        stor.yaco = false
        // Bonne réponse
        if (isRepOk()) {
          this._stopTimer()
          this.score++
          stor.lesdiv.correction.style.color = this.styles.cbien
          stor.lesdiv.correction.innerHTML = cBien

          metoutvert()
          desatout()

          this.typederreurs[0]++
          this.cacheBoutonValider()
          this.etat = 'navigation'
          this.sectionCourante()
        } else {
          // Pas de bonne réponse
          stor.lesdiv.correction.style.color = this.styles.cfaux
          // A cause de la limite de temps :
          if (this.isElapsed) { // limite de temps
            stor.lesdiv.correction.innerHTML = tempsDepasse
            this.typederreurs[10]++
            this.cacheBoutonValider()
            afcofo(true)
            this.etat = 'navigation'
            this.sectionCourante()
          } else { // Réponse fausse :
            stor.lesdiv.correction.innerHTML = cFaux

            if (this.essaiCourant < this.donneesSection.nbchances) {
              stor.lesdiv.correction.innerHTML += '<br>' + essaieEncore
              afcofo(false)
              this.typederreurs[1]++
              // indication éventuelle ici
            } else { // Erreur au nème essai
              this._stopTimer()
              this.cacheBoutonValider()
              afcofo(true)
              stor.lesdiv.correction.innerHTML += '<br>' + regardeCorrection
              this.typederreurs[2]++
              this.etat = 'navigation'
              this.sectionCourante()
            }
          }
        }
      }

      if (stor.yaexplik) stor.lesdiv.explications.classList.add('explique')
      if (stor.yaco) stor.lesdiv.solution.classList.add('correction')
      // Obligatoire
      this.finCorrection()
      break // case "correction":

    case 'navigation':

      if (this.sectionTerminee()) {
        // On détermine la phrase d’état renvoyée par la section

        const sco = this.score / this.donneesSection.nbitems
        let peibase = 0
        if (sco >= 0.8) { peibase = 0 }
        if ((sco >= 0.55) && (sco < 0.8)) { peibase = 1 }
        if ((sco >= 0.3) && (sco < 0.55)) { peibase = 4 }
        if (sco < 0.3) { peibase = 7 }

        let peisup = 0
        let compt = this.typederreurs[2] / 4
        if (this.typederreurs[6] > compt) {
          peisup = 2
          compt = this.typederreurs[6]
        }
        if (this.typederreurs[3] > compt) {
          peisup = 1
        }
        this.parcours.pe = this.donneesSection.pe[peibase + peisup]

        this.afficheBoutonSectionSuivante()
        this.focus('sectioncontinuer')
      } else {
        this.etat = 'enonce'
        this.afficheBoutonSuite()
        this.focus('boutoncontinuer')
      }
      // Obligatoire
      this.finNavigation()

      break // case "navigation":
  }
}
