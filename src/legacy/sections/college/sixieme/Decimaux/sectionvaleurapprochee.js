import loadJqueryDialog from 'src/lib/core/loadJqueryDialog'
import { j3pAddElt, j3pAjouteBouton, j3pGetRandomBool, j3pGetRandomInt } from 'src/legacy/core/functions'
import { j3pAffiche } from 'src/lib/mathquill/functions'
import TableauConversionMobile from 'src/legacy/outils/tableauconversion/TableauConversionMobile'
import { ecrisBienMathquill } from 'src/legacy/outils/zoneStyleMathquill/functions'
import ZoneStyleMathquill1 from 'src/legacy/outils/zoneStyleMathquill/ZoneStyleMathquill1'
import { addDefaultTable } from 'src/legacy/themes/table'
import Dialog from 'src/lib/widgets/dialog/Dialog'
import textesGeneriques from 'src/lib/core/textes'

const { cBien, cFaux, essaieEncore, regardeCorrection, reponseIncomplete, tempsDepasse } = textesGeneriques

/**
 * Ajouter une description ici
 * @fileOverview
 * @author Tommy Barroy <t.barroy@laposte.net>
 * @since 2017-10
 */

export const params = {
  outils: ['mathquill'],
  /**
   * Ce qui est paramétrable dans l’éditeur de graphe
   * @type {LegacySectionParamsParametres}
   */
  parametres: [
    ['nbrepetitions', 9, 'entier', 'Nombre de répétitions de la section'],
    ['indication', '', 'string', 'Indication pour chaque répétition'],
    ['limite', 0, 'entier', 'Temps disponible par répétition, en secondes'],
    ['ApproximationDecimale', 'indéfini', 'liste', '<u>Exces_Defaut</u>: Approximation décimale par défaut ou par excès demandée. <BR><BR> <u>Exces_Defaut_Arrondi</u>: Approximation décimale par défaut , par excès ou arrondi demandée. <BR><BR> <u>Arrondi</u>: Arrondi demandé <BR><BR> <u>indéfini</u>: Approximation décimale simple demandée , les deux réponses (par défaut ou par excès) sont acceptées.', ['Exces', 'Defaut', 'Arrondi', 'Exces_Arrondi', 'Defaut_Arrondi', 'Exces_Defaut', 'Exces_Defaut_Arrondi', 'indéfini']],
    ['centaine', true, 'boolean', '<u>true</u>: L’exercice peut demander une approximation décimale à la centaine. <BR><BR> <u>false</u>: non.'],
    ['dizaine', true, 'boolean', '<u>true</u>: L’exercice peut demander une approximation décimale à la dizaine. <BR><BR> <u>false</u>: non.'],
    ['unite', true, 'boolean', '<u>true</u>: L’exercice peut demander une approximation décimale à l’unité. <BR><BR> <u>false</u>: non.'],
    ['dixieme', true, 'boolean', '<u>true</u>: L’exercice peut demander une approximation décimale au dixième. <BR><BR> <u>false</u>: non.'],
    ['centieme', true, 'boolean', '<u>true</u>: L’exercice peut demander une approximation décimale au centième. <BR><BR> <u>false</u>: non.'],
    ['millieme', true, 'boolean', '<u>true</u>: L’exercice peut demander une approximation décimale au millième. <BR><BR> <u>false</u>: non.'],
    ['dixmillieme', true, 'boolean', '<u>true</u>: L’exercice peut demander une approximation décimale au dix-millième. <BR><BR> <u>false</u>: non.'],
    ['Decimale', 'les deux', 'liste', "<u>oui</u>: Utilisation de l’expression '' à 0,1 près '' au lieu de '' au dixième '' <BR><BR> <u>non</u>: non. <BR><BR> <u>les deux</u>: Les deux formulations peuvent être proposées. ", ['oui', 'non', 'les deux']],
    ['Puissance', 'les deux', 'liste', "<u>oui</u>: Utilisation de l’expression '' à 10<sup>-1</sup> près '' au lieu de '' à 0,1 près '' (ne fonctionne que quand le paramètre Décimale est OUI). <BR><BR> <u>non</u>: non. <BR><BR> <u>les deux</u>: Les deux formulations peuvent être proposées. ", ['oui', 'non', 'les deux']],
    ['Unite', 'les deux', 'liste', '<u>oui</u>: le nombre proposé a une unité. <BR><BR> <u>non</u>: non. <BR><BR> <u>les deux</u>: Le nombre peut avoir une unité.', ['oui', 'non', 'les deux']],
    ['m', true, 'boolean', '<u>true</u>: dans le cas d’un nombre proposé avec unité, l’unité peut être le mètre'],
    ['L', true, 'boolean', '<u>true</u>: dans le cas d’un nombre proposé avec unité, l’unité peut être le litre'],
    ['g', true, 'boolean', '<u>true</u>: dans le cas d’un nombre proposé avec unité, l’unité peut être le gramme'],
    ['mcarre', true, 'boolean', '<u>true</u>: dans le cas d’un nombre proposé avec unité, l’unité peut être le m²'],
    ['mcube', true, 'boolean', '<u>true</u>: dans le cas d’un nombre proposé avec unité, l’unité peut être le m³'],
    ['ChangementUnitePossible', true, 'boolean', '<u>true</u>: dans le cas d’un nombre proposé avec unité, l’unité demandée peut être différente de celle donnée.'],
    ['Tableau', true, 'boolean', '<u>true</u>: dans le cas d’un changement d’unités, un tableau de conversion est disponible.'],
    ['Espace', true, 'boolean', '<u>true</u>: Les espaces entre chaque groupe sont exigés dans la partie entière.'],
    ['ValeurApprochee', false, 'boolean', "<u>true</u>: Pour les professeurs de collège qui préfèrent l’utilisation du terme 'valeur approchée' dont la définition diffère au lycée. "],
    // le modèle gère la compatibilité ascendante avec l’ancien paramètre MiseEnPagePlusClaire
    ['theme', 'standard', 'liste', 'Modèle pour l’affichage', ['standard', 'zonesAvecImageDeFond']]

  ],
  // dans le cas d’une section QUANTITATIVE
  pe: [
    { pe_1: 'Maitrise' },
    { pe_2: 'Suffisant' },
    { pe_3: 'Insuffisant' },
    { pe_4: 'Très insuffisant' },
    { pe_5: 'Suffisant - erreur de rang' },
    { pe_6: 'Suffisant - erreur d’arrondi' },
    { pe_7: "Suffisant - confusion entre 'par excès' et 'par défaut'" },
    { pe_8: 'Suffisant - erreur dans l’écriture des nombres' },
    { pe_9: 'Insuffisant - erreur de rang' },
    { pe_10: 'Insuffisant - erreur d’arrondi' },
    { pe_11: "Insuffisant - confusion entre 'par excès' et 'par défaut'" },
    { pe_12: 'Insuffisant - erreur dans l’écriture des nombres' },
    { pe_13: 'Très insuffisant - erreur de rang' },
    { pe_14: 'Très insuffisant - erreur d’arrondi' },
    { pe_15: "Très insuffisant - confusion entre 'par excès' et 'par défaut'" },
    { pe_16: 'Très insuffisant - erreur dans l’écriture des nombres' }
  ]
}

/**
 * section valeurapprochee
 * @this {Parcours}
 */
export default function main () {
  const me = this
  let ds = me.donneesSection
  const stor = this.storage

  function arrondi (nb, maxDecimales) {
    if (maxDecimales >= 0) { return Number(Number(nb).toFixed(maxDecimales)) } else { return arrondi(Math.round(nb / Math.pow(10, -maxDecimales)) * Math.pow(10, -maxDecimales), 0) }
  }

  function yareponse () {
    if (me.isElapsed) { return true }
    return (stor.mazonem.reponse() !== '')
  }

  function bonneReponse () {
    stor.ErrPlusieursVirgule = false
    stor.ErrVirguleSeule = false
    stor.ErrEspace = false
    stor.ErrDecimaleEspace = false
    stor.ErrDepRang1 = false
    stor.ErrDepRang2 = false
    stor.ErrRangRat = false
    stor.ErrZerosInutTrop = false

    stor.repEleve = stor.mazonem.reponse()
    if (stor.repEleve === '') {
      stor.mazonem.majaffiche('0')
      stor.repEleve = '0'
    }
    // test si y’a bien qu’une virgule
    if (stor.repEleve.indexOf(',') !== stor.repEleve.lastIndexOf(',')) {
      stor.ErrPlusieursVirgule = true
      return false
    }

    if (stor.repEleve.indexOf(',') !== -1) {
      stor.partieentiereEncours = stor.repEleve.substring(0, stor.repEleve.indexOf(','))
      stor.yavirguleEncours = true
      stor.partiedecimaleEncours = stor.repEleve.substring(stor.repEleve.indexOf(',') + 1)
    } else {
      stor.partieentiereEncours = stor.repEleve
      stor.yavirguleEncours = false
      stor.partiedecimaleEncours = ''
    }

    // test si les espaces sont pas strange
    // à partir de virgule, ou a partir de la droite
    // ca doit etre des groupes de 3 (sauf le dernier)
    // ou alors tout colle et espace a osef
    if ((stor.yavirguleEncours) && (stor.partiedecimaleEncours.replace(/ /g, '').length === 0)) {
      stor.ErrVirguleSeule = true
      return false
    }

    let yaesp = true
    let comptgrp = 0
    let comptgrpold = 3
    let ing = false
    for (let i = stor.partieentiereEncours.length - 1; i > -1; i--) {
      if (stor.partieentiereEncours[i] === ' ') {
        if (yaesp) {
          if (i === 0) {
            stor.ErrespVirg = true
            return false
          } else {
            stor.Err2esp = true
            return false
          }
        }
        yaesp = true
        if (ing) { comptgrpold = comptgrp }
        ing = false
        comptgrp = 0
      } else {
        yaesp = false
        comptgrp++
        ing = true
        if ((comptgrpold !== 3) || ((comptgrp > 3) && (ds.Espace))) {
          stor.ErrEspace = true
          return false
        }
      }
    }

    yaesp = true
    comptgrp = 0
    comptgrpold = 3
    ing = false
    for (let i = 0; i < stor.partiedecimaleEncours.length; i++) {
      if (stor.partiedecimaleEncours[i] === ' ') {
        if (yaesp) {
          if (i === 0) {
            stor.ErrespVirg = true
            return false
          } else {
            stor.Err2esp = true
            return false
          }
        }
        yaesp = true
        if (ing) { comptgrpold = comptgrp }
        ing = false
        comptgrp = 0
      } else {
        yaesp = false
        comptgrp++
        ing = true
        if ((comptgrpold !== 3) || ((comptgrp > 3) && (ds.Espace))) {
          stor.ErrDecimaleEspace = true
          return false
        }
      }
    }
    // test si ya oublié zero inut
    for (let i = 0; i < stor.partieentiereEncours.length; i++) {
      if (stor.partieentiereEncours[i] !== ' ') {
        if (stor.partieentiereEncours[i] === '0') {
          if (stor.partieentiereEncours.length !== 1) {
            stor.ErrZerosInutTrop = true
            return false
          }
        } else { break }
      }
    }
    for (let i = stor.partiedecimaleEncours.length - 1; i > -1; i--) {
      if (stor.partiedecimaleEncours[i] !== ' ') {
        if (stor.partiedecimaleEncours[i] === '0') {
          stor.ErrZerosInutTrop = true
        } else { break }
      }
    }
    // recolle tout
    // eslint-disable-next-line no-irregular-whitespace
    stor.repEleve2 = stor.repEleve.replace(/ /g, '') // FIXME pourquoi ce THSP (espace insécable fin) ?
    stor.repEleve2 = stor.repEleve2.replace(',', '.')
    stor.repEleve2 = parseFloat(stor.repEleve2)

    stor.egal1 = (Math.abs(stor.repattendue - stor.repEleve2) < Math.pow(10, -12))
    stor.egal2 = (Math.abs(stor.repattendue2 - stor.repEleve2) < Math.pow(10, -12))
    let ok = stor.indefini ? (stor.egal1 || stor.egal2) : stor.egal1
    if ((!ok) && (stor.egal2)) {
      if (stor.MonApprochetype === ds.textes.approchtype[2]) {
        me.typederreurs[4]++
      } else {
        me.typederreurs[5]++
      }
    }
    const nbreCaracteres = (lettre, mot) => mot.split(lettre).length - 1
    // det rang
    if (stor.yavirguleEncours) {
      stor.RangEleve = -stor.partiedecimaleEncours.length + nbreCaracteres(' ', stor.partiedecimaleEncours)
    } else {
      let cpt = 0
      for (let i = stor.partieentiereEncours.length - 1; i > -1; i--) {
        if (stor.partieentiereEncours[i] === ' ') continue
        if (stor.partieentiereEncours[i] === '0') {
          cpt++
          continue
        }
        break
      }
      stor.RangEleve = cpt
    }

    if (!(stor.repEleve === '0')) {
      if ((!ok) && (stor.RangEleve < (-stor.Rangi + 2))) {
        stor.ErrDepRang1 = true
      }
      if ((ok) && (stor.RangEleve < (-stor.Rangi + 2)) && (stor.RangEleve < 0)) {
        ok = false
        stor.ErrDepRang2 = true
      }
      if ((!ok) && (stor.RangEleve > (-stor.Rangi + 2))) {
        stor.ErrRangRat = true
      }
    }

    return ok
  }

  function toggleTableauConversion () {
    if (!stor.dialog) return console.error(Error('Pas de dialog disponible pour l’afficher'))
    stor.dialog.toggle()
  }

  function getDonnees () {
    return {

      // Nombre de répétitions de l’exercice
      nbrepetitions: 9,
      nbetapes: 1,
      ApproximationDecimale: 'Exces_Defaut_Arrondi',
      centaine: true,
      dizaine: true,
      unite: true,
      dixieme: true,
      centieme: true,
      millieme: true,
      dixmillieme: true,
      Decimale: true,
      Unite: 'les deux',
      m: true,
      L: true,
      g: true,
      mcarre: true,
      mcube: true,
      ChangementUnitePossible: true,
      Tableau: true,
      Puissance: true,
      Espace: true,
      ValeurApprochee: false,

      pe_1: 'Maitrise',
      pe_2: 'Suffisant',
      pe_3: 'Insuffisant',
      pe_4: 'Très insuffisant',
      pe_5: 'Suffisant - erreur de rang',
      pe_6: 'Suffisant - erreur d’arrondi',
      pe_7: "Suffisant - confusion entre 'par excès' et 'par défaut'",
      pe_8: 'Suffisant - erreur dans l’écriture des nombres',
      pe_9: 'Insuffisant - erreur de rang',
      pe_10: 'Insuffisant - erreur d’arrondi',
      pe_11: "Insuffisant - confusion entre 'par excès' et 'par défaut'",
      pe_12: 'Insuffisant - erreur dans l’écriture des nombres',
      pe_13: 'Très insuffisant - erreur de rang',
      pe_14: 'Très insuffisant - erreur d’arrondi',
      pe_15: "Très insuffisant - confusion entre 'par excès' et 'par défaut'",
      pe_16: 'Très insuffisant - erreur dans l’écriture des nombres',

      // Si indication != "" alors un lien "Indication" s’affiche dans la zone Inférieure Gauche (this.zones.IG
      // une indication<br>de deux lignes...";
      indication: '',
      // Nombre de chances dont dispose l’él§ve pour répondre à la question
      nbchances: 1,

      textes: {
        couleurexplique: '#A9E2F3',
        couleurcorrec: '#A9F5A9',
        couleurenonce: '#D8D8D8',
        couleurenonceg: '#F3E2A9',
        couleuretiquettes: '#A9E2F3',
        approchtype: ['par excès', 'par défaut', 'arrondie'],
        consigne: "Donne £gapproximation décimale £a £b de <span class='nowrap'>$£c$ £d.</span>",
        consigne2: "Donne £gapproximation décimale à $£e$ £f près £b de <span class='nowrap'>$£c$ £d.</span>",
        Aconsigne: "Donne £g valeur approchée £a £b de <span class='nowrap'>$£c$ £d.</span>",
        Aconsigne2: "Donne £g valeur approchée à $£e$ £f près £b de <span class='nowrap'>$£c$ £d.</span>",
        consignearrondi: "Donne l’arrondi £a de <span class='nowrap'>$£c$ £d.</span>",
        consigne2arrondi: "Donne l’arrondi à $£e$ £f près de <span class='nowrap'>$£c$ £d.</span>",
        approch: [['à la centaine', 'à la centaine de'], ['à la dizaine', 'à la dizaine de'], ['à l’unité', 'au'], ['au dixième', 'au dixième de'], ['au centième', 'au centième de'], ['au millième', 'au millième de'], ['au dix-millième', 'au dix-millième de']],
        pres: 'près',
        a: ' à',
        llala: "l'",
        une: 'une ',
        Ala: 'la',
        Aune: 'une',
        mult: ['k', 'h', 'da', '', 'd', 'c', 'm'],
        Eneffet: 'En effet, <BR> $£a$ £b $<$ $£c$ £e $<$ $£d$ £b .',
        errtropvirgule: 'Il ne peut y avoir plusieurs virgules dans un nombre !',
        errvirguleseule: 'Il ne peut pas y avoir de virgule sans partie décimale !',
        errespace: 'Dans la partie entière, les chiffres doivent être regroupés par 3 en commençant par la droite !',
        errespacedec: 'Dans la partie décimale, les chiffres doivent être regroupés par 3 en commençant par la gauche !',
        cofo: '$£a$ £b $<$ $£c$ £e $<$ $£d$ £b .',
        errDeprang: 'Le chiffre des £a est le £b £c la virgule !',
        errDeprang2: 'Supprime les zéros inutiles !',
        coarr: 'Le chiffre des £a de $£c$ est $£i$ (et $£i$ $£u$ $5$) ',
        cofo2: '$£c$ £e $\\approx$ $£m$ £b',
        cofo22: 'Donc $£c$ £e $\\approx$ $£m$ £b',
        listik: ['centaines', 'dizaines', 'unités', 'dixièmes', 'centièmes', 'millièmes', 'dix-millièmes', 'cent-millièmes'],
        listik2: ['troisième', 'deuxième', 'premier', 'premier', 'deuxième', 'troisième', 'quatrième'],
        listik3: ['avant', 'avant', 'avant', 'après', 'après', 'après', 'après']
      },
      pe: 0
    }
  }
  async function initSection () {
    // faut s’assurer que jQuery-ui est chargé
    await loadJqueryDialog()

    ds = getDonnees()
    me.donneesSection = ds
    const structure = 'presentation1'

    me.surcharge()
    ds.nbitems = ds.nbetapes * ds.nbrepetitions

    // Construction de la page
    me.construitStructurePage(structure)

    stor.indefini = ds.ApproximationDecimale.includes('indéfini')
    if (stor.indefini) {
      stor.ApprocheTypePossible = ['']
    } else {
      stor.ApprocheTypePossible = []
      if (ds.ApproximationDecimale.includes('Exces')) stor.ApprocheTypePossible.push(ds.textes.approchtype[0])
      if (ds.ApproximationDecimale.includes('Defaut')) stor.ApprocheTypePossible.push(ds.textes.approchtype[1])
      if (ds.ApproximationDecimale.includes('Arrondi')) stor.ApprocheTypePossible.push(ds.textes.approchtype[2])
      if (stor.ApprocheTypePossible.length === 0) stor.ApprocheTypePossible.push(ds.textes.approchtype[1]) // on impose défaut
    }
    stor.ApprocheRangPossible = []
    if (ds.centaine) stor.ApprocheRangPossible.push(0)
    if (ds.dizaine) stor.ApprocheRangPossible.push(1)
    if (ds.unite) stor.ApprocheRangPossible.push(2)
    if (ds.dixieme) stor.ApprocheRangPossible.push(3)
    if (ds.centieme) stor.ApprocheRangPossible.push(4)
    if (ds.millieme) stor.ApprocheRangPossible.push(5)
    if (ds.dixmillieme) stor.ApprocheRangPossible.push(6)
    if (stor.ApprocheRangPossible.length === 0) stor.ApprocheRangPossible.push(2)

    stor.UnitePossible = []
    if ((ds.Unite === 'oui') || (ds.Unite === 'les deux')) {
      if (ds.m) stor.UnitePossible.push('m')
      if (ds.L) stor.UnitePossible.push('L')
      if (ds.g) stor.UnitePossible.push('g')
      if (ds.mcarre) stor.UnitePossible.push('m²')
      if (ds.mcube) stor.UnitePossible.push('m³')
    }
    if (ds.Unite === 'les deux') stor.UnitePossible.push('')
    if (stor.UnitePossible.length === 0) stor.UnitePossible = ['']
    if ((ds.Decimale !== 'oui') && (ds.Decimale !== 'non')) ds.Decimale = 'les deux'
    if ((ds.Puissance !== 'oui') && (ds.Puissance !== 'non')) ds.Puissance = 'les deux'

    me.typederreurs = [0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0]

    me.score = 0

    if (ds.ValeurApprochee) {
      me.afficheTitre('Valeur approchée ', false)
    } else {
      me.afficheTitre('Approximation décimale ')
    }
    if (ds.indication !== '') me.indication(me.zones.IG, ds.indication)
    enonceMain()
  }
  function enonceMain () {
    me.videLesZones()

    stor.ongoingTouches = []
    if (stor.dialog) {
      stor.dialog.destroy()
      stor.dialog = null
    }
    const conteneur = j3pAddElt(me.zonesElts.MG, 'div', {
      style: me.styles.etendre('toutpetit.enonce', { padding: '10px' })
    })
    stor.elts = {}
    stor.elts.consigne = addDefaultTable(conteneur, 1, 1)[0][0]
    stor.elts.zoneTravail = addDefaultTable(conteneur, 1, 1)[0][0]
    const table = j3pAddElt(stor.elts.zoneTravail, 'table', '', { style: { verticalAlign: 'middle', margin: '0' } })
    const tr = j3pAddElt(table, 'tr')
    stor.elts.zoneT1 = j3pAddElt(tr, 'td', { nowrap: true })
    stor.elts.zoneT2 = j3pAddElt(tr, 'td')
    stor.elts.zoneT3 = j3pAddElt(tr, 'td')
    stor.elts.zoneExplications = j3pAddElt(conteneur, 'div')
    stor.elts.zoneExplications.style.color = me.styles.cfaux
    stor.elts.zoneSolution = j3pAddElt(conteneur, 'div')
    stor.elts.emplaTab = j3pAddElt(me.zonesElts.MD, 'div', {
      style: me.styles.etendre('toutpetit.enonce', { padding: '10px' })
    })
    stor.elts.correction = j3pAddElt(me.zonesElts.MD, 'div', {
      style: me.styles.etendre('toutpetit.enonce', { padding: '10px' })
    })
    stor.elts.zoneSolution.style.color = me.styles.petit.correction.color

    stor.elts.consigne.classList.add('enonce')
    stor.elts.zoneTravail.classList.add('travail')
    me.zonesElts.MG.classList.add('fond')

    let article = (ds.ValeurApprochee) ? ds.textes.Ala : ds.textes.llala
    stor.Monunite = stor.UnitePossible[j3pGetRandomInt(0, (stor.UnitePossible.length - 1))]
    stor.RangUniteI = j3pGetRandomInt(0, 6)
    stor.RangUnite = ds.textes.mult[stor.RangUniteI]
    if (stor.Monunite === '') stor.RangUnite = ''
    stor.Monunite2 = stor.RangUnite + stor.Monunite
    stor.MonApprochetype = stor.ApprocheTypePossible[j3pGetRandomInt(0, (stor.ApprocheTypePossible.length - 1))]
    stor.Rangi = stor.ApprocheRangPossible[j3pGetRandomInt(0, (stor.ApprocheRangPossible.length - 1))]
    let macons = ds.ValeurApprochee ? ds.textes.Aconsigne : ds.textes.consigne
    if (stor.MonApprochetype === ds.textes.approchtype[2]) { macons = ds.textes.consignearrondi }
    let mone = ''
    let monf = ''
    if (ds.Decimale === 'les deux') {
      stor.Maformule = 'oui'
      if (j3pGetRandomBool()) stor.Maformule = 'non'
    } else {
      stor.Maformule = ds.Decimale
    }
    if (stor.Maformule === 'oui') {
      macons = ds.textes.consigne2
      if (ds.ValeurApprochee) { macons = ds.textes.Aconsigne2 }
      if (stor.MonApprochetype === ds.textes.approchtype[2]) { macons = ds.textes.consigne2arrondi }
      if (ds.Puissance === 'les deux') {
        stor.Puissance = 'oui'
        if (j3pGetRandomBool()) stor.Puissance = 'non'
      } else {
        stor.Puissance = ds.Puissance
      }
      if (stor.Puissance === 'oui') {
        mone = '10^{' + (-stor.Rangi + 2) + '}'
        if ((-stor.Rangi + 2) === 1) { mone = 10 }
        if ((-stor.Rangi + 2) === 0) { mone = 1 }
      } else {
        mone = arrondi(Math.pow(10, -stor.Rangi + 2), 4) // +' '
      }
      stor.Rang = monf = stor.Monunite2
    } else {
      if (stor.Monunite2 !== '') {
        stor.Rang = ds.textes.approch[stor.Rangi][1] + ' ' + stor.Monunite2
      } else {
        stor.Rang = ds.textes.approch[stor.Rangi][0]
      }
      stor.Rang += ' ' + ds.textes.pres
    }

    let afficheTabConversion = false

    stor.uniterep = stor.Monunite2
    if ((ds.ChangementUnitePossible) && (j3pGetRandomInt(0, 1) === 1) && (stor.Monunite !== '')) {
      const decmax = (['m²', 'm³'].includes(stor.Monunite)) ? 2 : 3
      stor.RanguniterepI = stor.RangUniteI + j3pGetRandomInt(1, decmax)
      if (stor.RanguniterepI >= 7) stor.RanguniterepI = stor.RangUniteI - j3pGetRandomInt(1, decmax)
      stor.uniterep = ds.textes.mult[stor.RanguniterepI] + stor.Monunite
      if (ds.Tableau) {
        afficheTabConversion = true
        stor.dialog = new Dialog({ title: 'Tableau de Conversion' })
      }
    }
    if (afficheTabConversion) {
      stor.montata = new TableauConversionMobile(stor.dialog.container, {
        unite: stor.Monunite,
        are: true,
        litre: true,
        tonne: false
      })
      j3pAjouteBouton(stor.elts.emplaTab, toggleTableauConversion, { className: 'MepBoutons', value: 'Tableau de conversion' })
    }

    // determination du nb
    // entre 2 et 4 chiffres avant , entre 1 et 4 chiffres après
    do {
      stor.repattendue = j3pGetRandomInt(1, 9999)
      stor.repattendue = arrondi(stor.repattendue * Math.pow(10, (-stor.Rangi + 2)), -(-stor.Rangi + 2))
      const nbchiffenplus = j3pGetRandomInt(1, 4)
      let laj
      if (nbchiffenplus === 1) {
        laj = j3pGetRandomInt(1, 4)
      } else if (nbchiffenplus === 2) {
        laj = j3pGetRandomInt(11, 49)
      } else if (nbchiffenplus === 3) {
        laj = j3pGetRandomInt(101, 499)
      } else if (nbchiffenplus === 4) {
        laj = j3pGetRandomInt(1001, 4999)
      }
      laj = arrondi(laj * Math.pow(10, (-stor.Rangi + 2 - nbchiffenplus)), -(-stor.Rangi + 2 - nbchiffenplus))
      let mod = arrondi(Math.pow(10, (-stor.Rangi + 2)), -(-stor.Rangi + 2))
      if (stor.MonApprochetype === ds.textes.approchtype[0]) {
        laj = -laj
        mod = -mod
      }
      if (stor.MonApprochetype === ds.textes.approchtype[2]) {
        if (j3pGetRandomBool()) {
          laj = -laj
          mod = -mod
        }
      }

      stor.valenonce = arrondi(stor.repattendue + laj, -(-stor.Rangi + 2 - nbchiffenplus))
      stor.valenoncecons = stor.valenonce
      stor.repattendue2 = arrondi(stor.repattendue + mod, -(-stor.Rangi + 2))
      if (stor.indefini) {
        article = ds.textes.une
        if (ds.ValeurApprochee) { article = ds.textes.Aune }
      }

      if (stor.Monunite2 !== stor.uniterep) {
        let amul = 1
        if (stor.Monunite === 'm²') { amul = 2 }
        if (stor.Monunite === 'm³') { amul = 3 }
        stor.valenonce = arrondi(stor.valenonce * Math.pow(10, amul * (stor.RanguniterepI - stor.RangUniteI)), 11)
      }

      stor.monc = ecrisBienMathquill(stor.valenonce)
      stor.monc2 = ecrisBienMathquill(stor.valenoncecons)
    } while ((stor.monc.indexOf('e+') !== -1) || (stor.monc.indexOf('e-') !== -1))

    j3pAffiche(stor.elts.consigne, null, '<b>' + macons + '</b>',
      {
        g: article,
        e: mone,
        f: monf,
        c: stor.monc,
        d: stor.uniterep,
        b: stor.MonApprochetype,
        a: stor.Rang.replace('.', ',')
      })

    j3pAffiche(stor.elts.zoneT1, null, '$£c$ £d $\\approx$ ',
      { c: stor.monc, d: stor.uniterep })
    // ZoneStyleMathquill1 ne prend que des ids…
    stor.mazonem = new ZoneStyleMathquill1(stor.elts.zoneT2, {
      restric: '0123456789,. ',
      limitenb: 20,
      limite: 1000,
      enter: me.sectionCourante.bind(me)

    })
    j3pAffiche(stor.elts.zoneT3, null, ' £d ', { d: stor.Monunite2 })

    me.finEnonce()
  }

  switch (this.etat) {
    case 'enonce':
      // code exécuté au lancement de la section
      if (me.debutDeLaSection) {
        initSection()
      } else {
        enonceMain()
      }

      break // case "enonce":
    case 'correction': {
      // On teste si une réponse a été saisie
      if (!yareponse()) {
        // ici on donne le message à afficher si une ou plusieurs zone(s) n’est pas complétée (tout d’abord on met en rouge : styles.cfaux)
        stor.elts.correction.style.color = this.styles.cfaux
        stor.elts.correction.innerHTML = reponseIncomplete
        stor.mazonem.focus()
        this.afficheBoutonValider()
        return
      }
      // Une réponse a été saisie
      let yaexplik = false
      // Bonne réponse
      let mona, mona2, mond, mond2, marep, marep2
      if (bonneReponse()) {
        this._stopTimer()
        this.score++
        stor.elts.correction.style.color = this.styles.cbien
        stor.elts.correction.innerHTML = cBien

        stor.mazonem.corrige(true)
        stor.mazonem.disable()

        mona = Math.min(stor.repattendue, stor.repattendue2)
        mond = Math.max(stor.repattendue, stor.repattendue2)

        mond2 = ecrisBienMathquill(mond)
        mona2 = ecrisBienMathquill(mona)

        j3pAffiche(stor.elts.zoneSolution, null, ds.textes.Eneffet,
          { a: mona2, b: stor.Monunite2, c: stor.monc, e: stor.uniterep, d: mond2 })

        this.typederreurs[0]++
        this.cacheBoutonValider()
        this.etat = 'navigation'
        this.sectionCourante()
      } else {
        // Pas de bonne réponse
        stor.elts.correction.style.color = this.styles.cfaux

        // A cause de la limite de temps :
        if (this.isElapsed) { // limite de temps
          this._stopTimer()

          stor.elts.correction.innerHTML = tempsDepasse
          this.typederreurs[10]++
          this.cacheBoutonValider()

          stor.mazonem.corrige(false)
          stor.mazonem.barre()
          stor.mazonem.disable()

          if (stor.ErrPlusieursVirgule) {
            this.typederreurs[6]++
            yaexplik = true
            j3pAffiche(stor.elts.zoneExplications, null, ds.textes.errtropvirgule)
          }
          if (stor.ErrVirguleSeule) {
            this.typederreurs[6]++
            yaexplik = true
            j3pAffiche(stor.elts.zoneExplications, null, ds.textes.errvirguleseule)
          }
          if (stor.ErrEspace) {
            this.typederreurs[6]++
            yaexplik = true
            j3pAffiche(stor.elts.zoneExplications, null, ds.textes.errespace)
          }
          if (stor.ErrDecimaleEspace) {
            this.typederreurs[6]++
            yaexplik = true
            j3pAffiche(stor.elts.zoneExplications, null, ds.textes.errespacedec)
          }
          if ((stor.ErrDepRang1) || (stor.ErrRangRat)) {
            yaexplik = true
            this.typederreurs[3]++
            j3pAffiche(
              stor.elts.zoneExplications,
              null,
              ds.textes.errDeprang,
              { a: ds.textes.listik[stor.Rangi], b: ds.textes.listik2[stor.Rangi], c: ds.textes.listik3[stor.Rangi] }
            )
          }

          if (stor.ErrDepRang2) {
            this.typederreurs[3]++
            yaexplik = true
            j3pAffiche(stor.elts.zoneExplications, null, ds.textes.errDeprang2)
          }

          mona = Math.min(stor.repattendue, stor.repattendue2)
          mond = Math.max(stor.repattendue, stor.repattendue2)
          marep = stor.repattendue

          mond2 = ecrisBienMathquill(mond)
          mona2 = ecrisBienMathquill(mona)
          marep2 = ecrisBienMathquill(marep)

          if (stor.Monunite2 !== stor.uniterep) {
            j3pAffiche(stor.elts.zoneSolution, null, 'Je convertis $£a$ £b = $£c$ £d \n', {
              styletext: {},
              a: stor.monc,
              b: stor.uniterep,
              c: ecrisBienMathquill(stor.valenoncecons),
              d: stor.Monunite2
            })
          }

          j3pAffiche(stor.elts.zoneSolution, null, ds.textes.cofo,
            { a: mona2, b: stor.Monunite2, c: stor.monc, e: stor.uniterep, d: mond2 })
          if (stor.indefini) {
            j3pAffiche(stor.elts.zoneSolution, null, '\n' + ds.textes.cofo22,
              { m: mona2, b: stor.Monunite2, c: stor.monc, e: stor.uniterep })
            j3pAffiche(stor.elts.zoneSolution, null, '\nou ' + ds.textes.cofo2,
              { m: mond2, b: stor.Monunite2, c: stor.monc, e: stor.uniterep })
          } else {
            /// A CHANGE , pas valenonce quand change unite
            const moni1 = Math.trunc(stor.valenoncecons * Math.pow(10, stor.Rangi - 1))
            const moni2 = Math.trunc(stor.valenoncecons * Math.pow(10, stor.Rangi - 2)) * 10
            const moni = arrondi(moni1 - moni2, 0)
            const monu = (moni >= 5) ? '\\geq' : '<'
            if (stor.MonApprochetype === ds.textes.approchtype[2]) {
              j3pAffiche(stor.elts.zoneSolution, null, ' \n' + ds.textes.coarr,
                { a: ds.textes.listik[stor.Rangi + 1], i: moni, c: stor.monc2, u: monu })
            }
            j3pAffiche(stor.elts.zoneSolution, null, ' \n' + ds.textes.cofo22,
              { m: marep2, b: stor.Monunite2, c: stor.monc, e: stor.uniterep })
          }

          this.etat = 'navigation'
          this.sectionCourante()
        } else {
          // Réponse fausse :
          stor.elts.correction.innerHTML = cFaux

          if (this.essaiCourant < ds.nbchances) {
            stor.elts.correction.innerHTML += '<br>' + essaieEncore

            this.typederreurs[1]++
          } else { // Erreur au nème essai
            this._stopTimer()
            this.cacheBoutonValider()

            stor.mazonem.corrige(false)
            stor.mazonem.barre()
            stor.mazonem.disable()

            if (stor.ErrPlusieursVirgule) {
              this.typederreurs[6]++
              yaexplik = true
              j3pAffiche(stor.elts.zoneExplications, null, ds.textes.errtropvirgule)
            }
            if (stor.ErrVirguleSeule) {
              this.typederreurs[6]++
              yaexplik = true
              j3pAffiche(stor.elts.zoneExplications, null, ds.textes.errvirguleseule)
            }
            if (stor.ErrEspace) {
              this.typederreurs[6]++
              yaexplik = true
              j3pAffiche(stor.elts.zoneExplications, null, ds.textes.errespace)
            }
            if (stor.ErrDecimaleEspace) {
              this.typederreurs[6]++
              yaexplik = true
              j3pAffiche(stor.elts.zoneExplications, null, ds.textes.errespacedec)
            }
            if ((stor.ErrDepRang1) || (stor.ErrRangRat)) {
              yaexplik = true
              this.typederreurs[3]++
              j3pAffiche(stor.elts.zoneExplications, null, ds.textes.errDeprang,
                {
                  a: ds.textes.listik[stor.Rangi],
                  b: ds.textes.listik2[stor.Rangi],
                  c: ds.textes.listik3[stor.Rangi]
                })
            }

            if (stor.ErrDepRang2) {
              this.typederreurs[3]++
              yaexplik = true
              j3pAffiche(stor.elts.zoneExplications, null, ds.textes.errDeprang2)
            }

            mona = Math.min(stor.repattendue, stor.repattendue2)
            mond = Math.max(stor.repattendue, stor.repattendue2)
            marep = stor.repattendue
            mond2 = ecrisBienMathquill(mond)
            mona2 = ecrisBienMathquill(mona)
            marep2 = ecrisBienMathquill(marep)

            if (stor.Monunite2 !== stor.uniterep) {
              j3pAffiche(stor.elts.zoneSolution, null, 'Je convertis $£a$ £b = $£c$ £d \n', {
                styletext: {},
                a: stor.monc,
                b: stor.uniterep,
                c: ecrisBienMathquill(stor.valenoncecons),
                d: stor.Monunite2
              })
            }

            j3pAffiche(stor.elts.zoneSolution, null, ds.textes.cofo,
              { a: mona2, b: stor.Monunite2, c: stor.monc, e: stor.uniterep, d: mond2 })
            if (stor.indefini) {
              j3pAffiche(stor.elts.zoneSolution, null, '\n' + ds.textes.cofo22,
                { m: mona2, b: stor.Monunite2, c: stor.monc, e: stor.uniterep })
              j3pAffiche(stor.elts.zoneSolution, null, '\nou ' + ds.textes.cofo2,
                { m: mond2, b: stor.Monunite2, c: stor.monc, e: stor.uniterep })
            } else {
              /// A CHANGE , pas valenonce quand change unite
              const moni1 = Math.trunc(stor.valenoncecons * Math.pow(10, stor.Rangi - 1))
              const moni2 = Math.trunc(stor.valenoncecons * Math.pow(10, stor.Rangi - 2)) * 10
              const moni = arrondi(moni1 - moni2, 0)
              const monu = (moni >= 5) ? '\\geq' : '<'
              if (stor.MonApprochetype === ds.textes.approchtype[2]) {
                j3pAffiche(stor.elts.zoneSolution, null, ' \n' + ds.textes.coarr,
                  { a: ds.textes.listik[stor.Rangi + 1], i: moni, c: stor.monc2, u: monu })
              }
              j3pAffiche(stor.elts.zoneSolution, null, ' \n' + ds.textes.cofo22,
                { m: marep2, b: stor.Monunite2, c: stor.monc, e: stor.uniterep })
            }

            stor.elts.correction.innerHTML += '<br>' + regardeCorrection
            this.typederreurs[2]++
            this.etat = 'navigation'
            this.sectionCourante()
          }
        }
      }
      if (ds.theme === 'zonesAvecImageDeFond') {
        stor.elts.zoneSolution.classList.add('correction')
        if (yaexplik) {
          stor.elts.zoneExplications.classList.add('explique')
        }
      }
      // Obligatoire
      this.finCorrection()
      break // case "correction":
    }

    case 'navigation':
      if (this.sectionTerminee()) {
        let compt
        const sco = this.score / ds.nbitems
        if (sco >= 0.8) this.parcours.pe = ds.pe_1
        if ((sco >= 0.55) && (sco < 0.8)) {
          this.parcours.pe = ds.pe_2
          compt = this.typederreurs[2] / 4
          if (this.typederreurs[3] > compt) {
            compt = this.typederreurs[3]
            this.parcours.pe = ds.pe_5
          }
          if (this.typederreurs[4] > compt) {
            compt = this.typederreurs[4]
            this.parcours.pe = ds.pe_6
          }
          if (this.typederreurs[5] > compt) {
            compt = this.typederreurs[5]
            this.parcours.pe = ds.pe_7
          }
          if (this.typederreurs[6] > compt) {
            compt = this.typederreurs[6]
            this.parcours.pe = ds.pe_8
          }
        }
        if ((sco >= 0.3) && (sco < 0.55)) {
          this.parcours.pe = ds.pe_3
          compt = this.typederreurs[2] / 4
          if (this.typederreurs[3] > compt) {
            compt = this.typederreurs[3]
            this.parcours.pe = ds.pe_9
          }
          if (this.typederreurs[4] > compt) {
            compt = this.typederreurs[4]
            this.parcours.pe = ds.pe_10
          }
          if (this.typederreurs[5] > compt) {
            compt = this.typederreurs[5]
            this.parcours.pe = ds.pe_11
          }
          if (this.typederreurs[6] > compt) {
            compt = this.typederreurs[6]
            this.parcours.pe = ds.pe_12
          }
        }
        if (sco < 0.3) {
          this.parcours.pe = ds.pe_4
          compt = this.typederreurs[2] / 4
          if (this.typederreurs[3] > compt) {
            compt = this.typederreurs[3]
            this.parcours.pe = ds.pe_13
          }
          if (this.typederreurs[4] > compt) {
            compt = this.typederreurs[4]
            this.parcours.pe = ds.pe_14
          }
          if (this.typederreurs[5] > compt) {
            compt = this.typederreurs[5]
            this.parcours.pe = ds.pe_15
          }
          if (this.typederreurs[6] > compt) {
            compt = this.typederreurs[6]
            this.parcours.pe = ds.pe_16
          }
        }

        this.afficheBoutonSectionSuivante()
        this.focus('sectioncontinuer')
      } else {
        this.etat = 'enonce'
        this.afficheBoutonSuite()
        this.focus('boutoncontinuer')
      }
      // Obligatoire
      this.finNavigation()
      break // case "navigation":
  }
}
