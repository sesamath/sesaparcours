import { j3pAddElt, j3pAjouteBouton, j3pArrondi, j3pEmpty, j3pGetRandomBool, j3pGetRandomInt, j3pShuffle, j3pStyle, j3pGetNewId, j3pShowError } from 'src/legacy/core/functions'
import BulleAide from 'src/legacy/outils/bulleAide/BulleAide'
import Etiquettes from 'src/legacy/outils/etiquette/Etiquettes'
import PlaceEtiquettes from 'src/legacy/outils/etiquette/PlaceEtiquettes'
import { j3pAffiche } from 'src/lib/mathquill/functions'
import { addDefaultTable, addDefaultTableDetailed } from 'src/legacy/themes/table'
import { j3pCreeSVG } from 'src/legacy/core/functionsSvg'
import { getMtgCore } from 'src/lib/outils/mathgraph'
import textesGeneriques from 'src/lib/core/textes'

const { cBien, cFaux, essaieEncore, regardeCorrection, reponseIncomplete, tempsDepasse } = textesGeneriques

/**
 * Ajouter une description ici
 * @fileOverview
 * @author Tommy Barroy <t.barroy@laposte.net>
 * @since 2017-10
*/

export const params = {
  outils: ['mathquill'],
  /**
   * Ce qui est paramétrable dans l’éditeur de graphe
   * @type {LegacySectionParamsParametres}
   */
  parametres: [
    ['nbrepetitions', 9, 'entier', 'Nombre de répétitions de la section'],
    ['indication', '', 'string', 'Indication pour chaque répétition'],
    ['limite', 0, 'entier', 'Temps disponible par répétition, en secondes'],
    ['nbchances', 2, 'entier', 'Nombre d’essais par répétition'],
    ['TypeEnonce', 'Les deux', 'liste', '<u>Texte</u>:' + ' l’énoncé' + ' est un texte <BR><BR> <u>Figure</u>:' + ' L’énoncé' + ' est une figure. <BR><BR> <u>Les deux</u>:' + ' l’un ou l’autre aléatoirement.', ['Les deux', 'Texte', 'Figure']],
    ['Propriete1', true, 'boolean', '<u>true</u>: L’élève doit connaître la propriété ' + '"<i>Si deux droites sont perpendiculaires à une même troisième droite, alors elles sont parallèles entre elles.</i>" <BR><BR> <u>false</u>: cette propriété' + ' n’est pas utilisée.'],
    ['Propriete2', true, 'boolean', '<u>true</u>: L’élève doit connaître la propriété ' + '"<i>Si deux droites sont parallèles entre elles et si une troisième droite est perpendiculaire à l' + "' une, alors elle est aussi perpendiculaire à l’autre.</i>" + '" <BR><BR> <u>false</u>: cette propriété n' + "'est pas utilisée."],
    ['Propriete3', true, 'boolean', '<u>true</u>: L’élève doit connaître la propriété ' + '"<i>Si deux droites sont parallèles à une même troisième droite, alors elles sont parallèles entre elles.</i>" <BR>(formulation précise à choisir avec le paramètre EnonceP3) <BR><BR> <u>false</u>: cette propriété' + ' n’est pas utilisée.'],
    ['Enoncep3', 'énoncé 1', 'liste', '<u>énoncé 1</u>: formulation "<i>Si deux droites sont parallèles à une même troisième droite, alors elles sont parallèles entre elles.</i> " préférée. <BR><BR> <u>énoncé 2</u>: formulation  "<i>Si deux droites sont parallèles et qu' + "'" + 'une troisième droite est parallèle à l' + "'" + 'une, alors elle est aussi parallèle à l' + "'" + 'autre.</i>" préférée.', ['énoncé 1', 'énoncé 2']],
    ['NomDesDroites', 'les deux', 'liste', '<u>(dx)</u>: Les droites ' + 's’appellent (d1) , (d2) , ...' + '<BR><BR> <u>(AB)</u>: Les droites ' + 's’appellent (AB) , (BC) , ...' + '<BR><BR> <u>les deux</u>: Les deux types de noms sont possibles.', ['les deux', '(AB)', '(dx)']],
    ['ConclusionDonnee', 'oui', 'liste', '<u>oui</u>: La conclusion est donnée dans la question. <BR><BR> <u>droites</u>: les noms des deux droites dont on peut déterminer la position sont donnés. <BR><BR> <u>non</u>: rien n’est donné. <BR><BR><i>Dans la démonstration la conclusion est à compléter dans tous les cas.</i>', ['oui', 'droites', 'non']],
    ['FormulationImposee', false, 'boolean', '<u>true</u>: L’élève ne peut pas changer la phrase. <BR><BR> <u>false</u>: il peut (et parfois doit) la changer.'],
    ['QuestionApresDonnees', false, 'boolean', '<u>true</u>: La question est posée après les données. <BR><BR> <u>false</u>: La question est posée avant les données.'],
    ['Presentation', true, 'boolean', "<u>true</u>: Les indications de réussite et les boutons 'ok' et 'suivant' sont dans un cadre séparé à droite. <BR><BR> <u>false</u>: Un seul cadre pour tout"],
    // le modèle gère la compatibilité ascendante avec l’ancien paramètre MiseEnPagePlusClaire
    ['theme', 'standard', 'liste', 'Modèle pour l’affichage', ['standard', 'zonesAvecImageDeFond']]

  ]

}

/**
 * section dem01
 * @this {Parcours}
 */
export default function main () {
  const me = this
  let ds = me.donneesSection
  const stor = me.storage
  let svgId = stor.svgId

  function afficheFig () {
    stor.svgId = j3pGetNewId('mtg32')
    svgId = stor.svgId
    const txtFigure = 'TWF0aEdyYXBoSmF2YTEuMAAAABM+TMzNAAJmcv###wEA#wEAAAAAAAAAAANLAAACfwAAAQEAAAAAAAAAAQAAAMz#####AAAAAQAKQ0NhbGNDb25zdAD#####AAJwaQAWMy4xNDE1OTI2NTM1ODk3OTMyMzg0Nv####8AAAABAApDQ29uc3RhbnRlQAkh+1RELRj#####AAAAAQAKQ1BvaW50QmFzZQD#####AQAAAAAOAAFVAMAkAAAAAAAAQBAAAAAAAAAAAAUAAEAv#nbItDlYQC#+dsi0OVj#####AAAAAQAUQ0Ryb2l0ZURpcmVjdGlvbkZpeGUA#####wEAAAAAEAAAAQAAAAEAAAABAT#wAAAAAAAA#####wAAAAEAD0NQb2ludExpZURyb2l0ZQD#####AQAAAAAOAAFWAMAAAAAAAAAAQBAAAAAAAAAAAAUAAUA##nbItDlYAAAAAv####8AAAABAAhDU2VnbWVudAD#####AQAAAAAQAAABAAAAAQAAAAEAAAAD#####wAAAAEAB0NNaWxpZXUA#####wEAAAAAEAAAAAAAAAAAAAAAQAgAAAAAAAAAAAUAAAAAAQAAAAP#####AAAAAgAMQ0NvbW1lbnRhaXJlAP####8BAAAAAAAAAAAAAAAAQBgAAAAAAAAAAAAAAAUMAAAAAAABAAAAAAAAAAEAAAAAAAAAAAABMf####8AAAABAAlDTG9uZ3VldXIA#####wAAAAEAAAADAAAAAgD#####AQAAAAAQAAAAAAAAAAAAAABACAAAAAAAAAAABQABQGVgAAAAAABAYtwo9cKPXAAAAAIA#####wEAAAAAEAAAAAAAAAAAAAAAQAgAAAAAAAAAAAUAAUBdQAAAAAAAQDzhR64UeuL#####AAAAAQAJQ0NlcmNsZU9BAP####8BAAAAAAAAAQAAAAgAAAAJAAAAAgD#####AQAAAAAQAAAAAAAAAAAAAABACAAAAAAAAAAABQABQGYgAAAAAABAaNwo9cKPXAAAAAkA#####wEAAAAAAAABAAAACAAAAAsAAAACAP####8AAAAAABAAAABACAAAAAAAAAAAAAAAAAAAAAAFAABAWUAAAAAAAEB8ThR64Ueu#####wAAAAEAB0NDYWxjdWwA#####wAFbWluaTEAATAAAAABAAAAAAAAAAAAAAAKAP####8ABW1heGkxAAMzNTkAAAABQHZwAAAAAAD#####AAAAAQAUQ0ltcGxlbWVudGF0aW9uUHJvdG8A#####wAHQ3Vyc2V1cgAAAAUAAAAFAAAAAwAAAA4AAAAPAAAADQAAAAMAAAAAEAEAAAAAEAAAAQAAAAEAAAANAT#wAAAAAAAAAAAABAEAAAAQAAAAAAAQAAAAwAgAAAAAAAA#8AAAAAAAAAAABQAAQF1AAAAAAAAAAAAR#####wAAAAEAC0NIb21vdGhldGllAAAAABAAAAAN#####wAAAAEACkNPcGVyYXRpb24D#####wAAAAEAD0NSZXN1bHRhdFZhbGV1cgAAAA4AAAANAQAAAA4AAAAOAAAADgAAAA######AAAAAQALQ1BvaW50SW1hZ2UAAAAAEAEAAAAADQACTzEAwBAAAAAAAABAEAAAAAAAAAAABQAAAAASAAAAEwAAAAwAAAAAEAAAAA0AAAANAwAAAA0BAAAAAT#wAAAAAAAAAAAADgAAAA4AAAANAQAAAA4AAAAPAAAADgAAAA4AAAAPAAAAABABAAAAAA0AAkkxAMAAAAAAAAAAQAgAAAAAAAAAAAUAAAAAEgAAABUAAAAFAQAAABAAAAAAABAAAAEAAAABAAAADQAAABIAAAAEAQAAABAAAAAAARAAAmsxAMAAAAAAAAAAQAAAAAAAAAAAAAEAAT+uoeoeoeofAAAAF#####8AAAACAA9DTWVzdXJlQWJzY2lzc2UBAAAAEAACYTEAAAAUAAAAFgAAABj#####AAAAAQAPQ1ZhbGV1ckFmZmljaGVlAQAAABAAAAAAAAAAAAAAAAAAwBgAAAAAAAAAAAAAABgPAAH###8AAAABAAAAAgAAAAEAAAAAAAAAAAAAAAACAAAAGQAAAAoA#####wACQTEAAmExAAAADgAAABn#####AAAAAQAJQ1JvdGF0aW9uAP####8AAAAIAAAADgAAABsAAAAPAP####8BAAAAABAAAAAAAAAAAAAAAEAIAAAAAAAAAAAFAAAAAAkAAAAc#####wAAAAEAEUNTeW1ldHJpZUNlbnRyYWxlAP####8AAAAIAAAADwD#####AX8AAAAQAAAAAAAAAAAAAABACAAAAAAAAAAACAAAAAAdAAAAHgAAAAUA#####wEAAAAAEAAAAQAAAAEAAAAfAAAAHf####8AAAABABBDSW50RHJvaXRlQ2VyY2xlAP####8AAAAgAAAADP####8AAAABABBDUG9pbnRMaWVCaXBvaW50AP####8BAAAAABAAAAAAAAAAAAAAAEAIAAAAAAAAAAAFAAEAAAAhAAAAFQD#####AQAAAAAQAAAAAAAAAAAAAABACAAAAAAAAAAABQACAAAAIQAAAAIA#####wAAAAAAEAAAAEAIAAAAAAAAAAAAAAAAAAAAAAUAAEBYwAAAAAAAQH8eFHrhR64AAAAKAP####8ABW1pbmkyAAEwAAAAAQAAAAAAAAAAAAAACgD#####AAVtYXhpMgACMTAAAAABQCQAAAAAAAAAAAALAP####8AB0N1cnNldXIAAAAFAAAABQAAAAMAAAAlAAAAJgAAACQAAAADAAAAACcBAAAAABAAAAEAAAABAAAAJAE#8AAAAAAAAAAAAAQBAAAAJwAAAAAAEAAAAMAIAAAAAAAAP#AAAAAAAAAAAAUAAEBdQAAAAAAAAAAAKAAAAAwAAAAAJwAAACQAAAANAwAAAA4AAAAlAAAADQEAAAAOAAAAJQAAAA4AAAAmAAAADwAAAAAnAQAAAAANAAJPMQDAEAAAAAAAAEAQAAAAAAAAAAAFAAAAACkAAAAqAAAADAAAAAAnAAAAJAAAAA0DAAAADQEAAAABP#AAAAAAAAAAAAAOAAAAJQAAAA0BAAAADgAAACYAAAAOAAAAJQAAAA8AAAAAJwEAAAAADQACSTEAwAAAAAAAAABACAAAAAAAAAAABQAAAAApAAAALAAAAAUBAAAAJwAAAAAAEAAAAQAAAAEAAAAkAAAAKQAAAAQBAAAAJwAAAAABEAABawDAAAAAAAAAAEAAAAAAAAAAAAABAAE#0YEYEYEYEgAAAC4AAAAQAQAAACcAAmEyAAAAKwAAAC0AAAAvAAAAEQEAAAAnAAAAAAAAAAAAAAAAAMAYAAAAAAAAAAAAAAAvDwAB####AAAAAQAAAAIAAAABAAAAAAAAAAAAAAAAAgAAADAAAAACAP####8AAAAAABAAAABACAAAAAAAAAAAAAAAAAAAAAAFAABAWgAAAAAAAECAxwo9cKPXAAAACgD#####AAVtaW5pMwABMAAAAAEAAAAAAAAAAAAAAAoA#####wAFbWF4aTMAAjEwAAAAAUAkAAAAAAAAAAAACwD#####AAdDdXJzZXVyAAAABQAAAAUAAAADAAAAMwAAADQAAAAyAAAAAwAAAAA1AQAAAAAQAAABAAAAAQAAADIBP#AAAAAAAAAAAAAEAQAAADUAAAAAABAAAADACAAAAAAAAD#wAAAAAAAAAAAFAABAXUAAAAAAAAAAADYAAAAMAAAAADUAAAAyAAAADQMAAAAOAAAAMwAAAA0BAAAADgAAADMAAAAOAAAANAAAAA8AAAAANQEAAAAADQACTzEAwBAAAAAAAABAEAAAAAAAAAAABQAAAAA3AAAAOAAAAAwAAAAANQAAADIAAAANAwAAAA0BAAAAAT#wAAAAAAAAAAAADgAAADMAAAANAQAAAA4AAAA0AAAADgAAADMAAAAPAAAAADUBAAAAAA0AAkkxAMAAAAAAAAAAQAgAAAAAAAAAAAUAAAAANwAAADoAAAAFAQAAADUAAAAAABAAAAEAAAABAAAAMgAAADcAAAAEAQAAADUAAAAAARAAAmsyAMAAAAAAAAAAQAAAAAAAAAAAAAEAAT#PufufufugAAAAPAAAABABAAAANQACYTMAAAA5AAAAOwAAAD0AAAARAQAAADUAAAAAAAAAAAAAAAAAwBgAAAAAAAAAAAAAAD0PAAH###8AAAABAAAAAgAAAAEAAAAAAAAAAAAAAAACAAAAPgAAAAoA#####wACQTIAAmEyAAAADgAAADAAAAAKAP####8AAkEzAAJhMwAAAA4AAAA+AAAADAD#####AAAAIwAAAA0DAAAADgAAAEAAAAABQCQAAAAAAAAAAAAPAP####8BAAD#ABAAAAAAAAAAAAAAAEAIAAAAAAAAAAAIAAAAAB8AAABCAAAADAD#####AAAAIgAAAA0DAAAADgAAAEEAAAABQCQAAAAAAAAAAAAPAP####8BAAD#ABAAAAAAAAAAAAAAAEAIAAAAAAAAAAAIAAAAAB0AAABE#####wAAAAEACUNEcm9pdGVBQgD#####AQAAAAAQAAABAAJkNAADAAAAHwAAAB3#####AAAAAQAWQ0Ryb2l0ZVBlcnBlbmRpY3VsYWlyZQD#####AAAAAAAQAAABAAJkMQADAAAARQAAAEYAAAAXAP####8AAAAAABAAAAEAAmQzAAMAAABDAAAARgAAABcA#####wEAAAAAEAAAAQACZDIAAwAAAAgAAABGAAAAFAD#####AAAASAAAAAoAAAAVAP####8BfwAAABAAAAAAAAAAAAAAAEAIAAAAAAAAAAAIAAEAAABKAAAAFQD#####AX8AAAAQAAAAAAAAAAAAAABACAAAAAAAAAAACAACAAAASgAAABQA#####wAAAEkAAAAKAAAAFQD#####AX8AAAAQAAAAAAAAAAAAAABACAAAAAAAAAAACAABAAAATQAAABUA#####wF#AAAAEAAAAAAAAAAAAAAAQAgAAAAAAAAAAAgAAgAAAE0AAAAUAP####8AAABHAAAACgAAABUA#####wF#AAAAEAAAAAAAAAAAAAAAQAgAAAAAAAAAAAgAAQAAAFAAAAAVAP####8BfwAAABAAAAAAAAAAAAAAAEAIAAAAAAAAAAAIAAIAAABQAAAACgD#####AAFSAAMwLjYAAAABP+MzMzMzMzP#####AAAAAgAJQ0NlcmNsZU9SAP####8BfwAAAAAAAwAAAB8AAAAOAAAAUwAAAAAYAP####8BfwAAAAAAAwAAAEwAAAAOAAAAUwAAAAAYAP####8BfwAAAAAAAwAAAE8AAAAOAAAAUwAAAAAYAP####8BfwAAAAAAAwAAAFIAAAAOAAAAUwD#####AAAAAQAQQ0ludENlcmNsZUNlcmNsZQD#####AAAACgAAAFQAAAAVAP####8BAAB#ABAAAAAAAAAAAAAAAEAIAAAAAAAAAAAIAAEAAABYAAAAFQD#####AQAAfwAQAAAAAAAAAAAAAABACAAAAAAAAAAACAACAAAAWAAAABkA#####wAAAAoAAABVAAAAFQD#####AQAAfwAQAAAAAAAAAAAAAABACAAAAAAAAAAACAABAAAAWwAAABUA#####wEAAH8AEAAAAAAAAAAAAAAAQAgAAAAAAAAAAAgAAgAAAFsAAAAZAP####8AAAAKAAAAVgAAABUA#####wEAAH8AEAAAAAAAAAAAAAAAQAgAAAAAAAAAAAgAAQAAAF4AAAAVAP####8BAAB#ABAAAAAAAAAAAAAAAEAIAAAAAAAAAAAIAAIAAABeAAAAGQD#####AAAACgAAAFcAAAAVAP####8BAAB#ABAAAAAAAAAAAAAAAEAIAAAAAAAAAAAIAAEAAABhAAAAFQD#####AQAAfwAQAAAAAAAAAAAAAABACAAAAAAAAAAACAACAAAAYQAAAAoA#####wACcjIAAzAuMwAAAAE#0zMzMzMzMwAAABgA#####wEAAH8AAAADAAAAQwAAAA4AAABkAAAAABgA#####wEAAH8AAAADAAAACAAAAA4AAABkAAAAABgA#####wEAAH8AAAADAAAARQAAAA4AAABkAAAAABQA#####wAAAEYAAABlAAAAFQD#####AQB#AAAQAAAAAAAAAAAAAABACAAAAAAAAAAACAABAAAAaAAAABUA#####wEAfwAAEAAAAAAAAAAAAAAAQAgAAAAAAAAAAAgAAgAAAGgAAAAUAP####8AAABIAAAAZQAAABUA#####wEAfwAAEAAAAAAAAAAAAAAAQAgAAAAAAAAAAAgAAQAAAGsAAAAVAP####8BAH8AABAAAAAAAAAAAAAAAEAIAAAAAAAAAAAIAAIAAABrAAAAFAD#####AAAARgAAAGYAAAAVAP####8BAH8AABAAAAAAAAAAAAAAAEAIAAAAAAAAAAAIAAIAAABuAAAAFQD#####AQB#AAAQAAAAAAAAAAAAAABACAAAAAAAAAAACAABAAAAbgAAABQA#####wAAAEkAAABmAAAAFQD#####AQB#AAAQAAAAAAAAAAAAAABACAAAAAAAAAAACAABAAAAcQAAABUA#####wEAfwAAEAAAAAAAAAAAAAAAQAgAAAAAAAAAAAgAAgAAAHEAAAAUAP####8AAABHAAAAZwAAABUA#####wEAfwAAEAAAAAAAAAAAAAAAQAgAAAAAAAAAAAgAAQAAAHQAAAAVAP####8BAH8AABAAAAAAAAAAAAAAAEAIAAAAAAAAAAAIAAIAAAB0AAAAFAD#####AAAARgAAAGcAAAAVAP####8BAH8AABAAAAAAAAAAAAAAAEAIAAAAAAAAAAAIAAIAAAB3AAAAFQD#####AQB#AAAQAAAAAAAAAAAAAABACAAAAAAAAAAACAABAAAAdwAAABcA#####wEAfwAAEAAAAQAAAAMAAABzAAAASQAAABcA#####wEAfwAAEAAAAQAAAAMAAABqAAAARv####8AAAABABBDRHJvaXRlUGFyYWxsZWxlAP####8BAH8AABAAAAEAAAADAAAAbwAAAHsAAAAaAP####8BAH8AABAAAAEAAAADAAAAeAAAAHv#####AAAAAQAQQ0ludERyb2l0ZURyb2l0ZQD#####AQB#AAAQAAAAAAAAAAAAAABACAAAAAAAAAAACAAAAAB6AAAAewAAABsA#####wEAfwAAEAAAAAAAAAAAAAAAQAgAAAAAAAAAAAgAAAAAegAAAH0AAAAbAP####8BAH8AABAAAAAAAAAAAAAAAEAIAAAAAAAAAAAIAAAAAHoAAAB8AAAABQD#####AX9#AAAQAAABAANtMzEAAwAAAGoAAAB+AAAABQD#####AX9#AAAQAAABAANtMzIAAwAAAH4AAABtAAAABQD#####AX9#AAAQAAABAANtMjIAAwAAAG8AAACAAAAABQD#####AX9#AAAQAAABAANtMjEAAwAAAIAAAABzAAAABQD#####AX9#AAAQAAABAANtMTIAAwAAAHgAAAB#AAAABQD#####AX9#AAAQAAABAANtMTEAAwAAAH8AAAB2AAAABwD#####AQAAfwEAA25kMQAAAGMQAAAAAAABAAAAAQAAAAEAAAAAAAAAAAAEKGQxKQAAAAcA#####wEAAH8BAANuZDIAAABgEAAAAAAAAQAAAAEAAAABAAAAAAAAAAAABChkMikAAAAHAP####8BAAB#AQADbmQzAAAAXRAAAAAAAAEAAAABAAAAAQAAAAAAAAAAAAQoZDMpAAAABwD#####AQAAfwEAA25kNAAAAFoQAAAAAAABAAAAAQAAAAEAAAAAAAAAAAAEKGQ0KQAAAAoA#####wACcjMAATIAAAABQAAAAAAAAAAAAAAYAP####8BAAB#AAAAAwAAAAgAAAAOAAAAiwAAAAAYAP####8BAAB#AAAAAwAAAEUAAAAOAAAAiwAAAAAUAP####8AAABJAAAAjAAAABUA#####wEAAH8AEAAAAAAAAAAAAAAAQAgAAAAAAAAAAnBCCQACAAAAjgAAABUA#####wEAAH8AEAAAAAAAAAAAAAAAQAgAAAAAAAAAAnBICQABAAAAjgAAABQA#####wAAAEcAAACNAAAAFQD#####AQAAfwAQAAAAAAAAAAAAAABACAAAAAAAAAACcEMJAAIAAACRAAAAFQD#####AQAAfwAQAAAAAAAAAAAAAABACAAAAAAAAAACcEkJAAEAAACRAAAAGgD#####AQAAAAAQAAABAAABAQAAAJIAAABGAAAAGgD#####AQAAAAAQAAABAAABAQAAAJMAAABGAAAAGAD#####AQAAAAAAAQEAAABDAAAADgAAAIsAAAAAFAD#####AAAASAAAAJYAAAAVAP####8BAAB#ABAAAAAAAAAAAAAAAEAIAAAAAAAAAAJwQQkAAgAAAJcAAAAVAP####8BAAB#ABAAAAAAAAAAAAAAAEAIAAAAAAAAAAJwRwkAAQAAAJcAAAAYAP####8BAAB#AAABAQAAAJMAAAAOAAAAUwAAAAAYAP####8BAAB#AAABAQAAAJIAAAAOAAAAUwAAAAAYAP####8BAAB#AAABAQAAAI8AAAAOAAAAUwAAAAAYAP####8BAAB#AAABAQAAAJAAAAAOAAAAUwAAAAAYAP####8BAAB#AAABAQAAAJkAAAAOAAAAUwAAAAAYAP####8BAAB#AAABAQAAAJgAAAAOAAAAUwAAAAAUAP####8AAACVAAAAngAAABUA#####wH#AAAAEAAAAAAAAAAAAAAAQAgAAAAAAAAAAAgAAgAAAKAAAAAVAP####8B#wAAABAAAAAAAAAAAAAAAEAIAAAAAAAAAAAIAAEAAACgAAAAFAD#####AAAAlQAAAJ0AAAAVAP####8B#wAAABAAAAAAAAAAAAAAAEAIAAAAAAAAAAAIAAIAAACjAAAAFQD#####Af8AAAAQAAAAAAAAAAAAAABACAAAAAAAAAAACAABAAAAowAAABQA#####wAAAJUAAACaAAAAFQD#####Af8AAAAQAAAAAAAAAAAAAABACAAAAAAAAAAACAACAAAApgAAABUA#####wH#AAAAEAAAAAAAAAAAAAAAQAgAAAAAAAAAAAgAAQAAAKYAAAAUAP####8AAACUAAAAmwAAABUA#####wH#AAAAEAAAAAAAAAAAAAAAQAgAAAAAAAAAAAgAAgAAAKkAAAAVAP####8B#wAAABAAAAAAAAAAAAAAAEAIAAAAAAAAAAAIAAEAAACpAAAAFAD#####AAAAlAAAAJwAAAAVAP####8B#wAAABAAAAAAAAAAAAAAAEAIAAAAAAAAAAAIAAIAAACsAAAAFQD#####Af8AAAAQAAAAAAAAAAAAAABACAAAAAAAAAAACAABAAAArAAAABQA#####wAAAJQAAACfAAAAFQD#####Af8AAAAQAAAAAAAAAAAAAABACAAAAAAAAAAACAACAAAArwAAABUA#####wH#AAAAEAAAAAAAAAAAAAAAQAgAAAAAAAAAAAgAAQAAAK8AAAAYAP####8B#wAAAAABAQAAAEMAAAAOAAAAUwAAAAAYAP####8B#wAAAAABAQAAAAgAAAAOAAAAUwAAAAAYAP####8B#wAAAAABAQAAAEUAAAAOAAAAUwD#####AAAAAQAMQ0Jpc3NlY3RyaWNlAP####8B#wAAABAAAAEAAAEBAAAAHwAAAEMAAACZAAAAHAD#####Af8AAAAQAAABAAABAQAAAB8AAAAIAAAAkAAAABwA#####wH#AAAAEAAAAQAAAQEAAABDAAAARQAAAJMAAAAUAP####8AAAC3AAAAtAAAABUA#####wH#AAAAEAAAAAAAAAAAAAAAQAgAAAAAAAAAAAgAAQAAALgAAAAVAP####8B#wAAABAAAAAAAAAAAAAAAEAIAAAAAAAAAAAIAAIAAAC4AAAAFAD#####AAAAtgAAALMAAAAVAP####8B#wAAABAAAAAAAAAAAAAAAEAIAAAAAAAAAAAIAAEAAAC7AAAAFQD#####Af8AAAAQAAAAAAAAAAAAAABACAAAAAAAAAAACAACAAAAuwAAABQA#####wAAALUAAACyAAAAFQD#####Af8AAAAQAAAAAAAAAAAAAABACAAAAAAAAAAACAABAAAAvgAAABUA#####wH#AAAAEAAAAAAAAAAAAAAAQAgAAAAAAAAAAAgAAgAAAL4AAAAHAP####8BAAB#AQABQQAAALAQAAAAAAABAAAAAQAAAAEAAAAAAAAAAAABQQAAAAcA#####wEAAH8BAAFCAAAArRAAAAAAAAEAAAABAAAAAQAAAAAAAAAAAAFCAAAABwD#####AQAAfwEAAUMAAACqEAAAAAAAAQAAAAEAAAABAAAAAAAAAAAAAUMAAAAHAP####8BAAB#AQABRAAAAL8QAAAAAAABAAAAAQAAAAEAAAAAAAAAAAABRAAAAAcA#####wEAAH8BAAFFAAAAvBAAAAAAAAEAAAABAAAAAQAAAAAAAAAAAAFFAAAABwD#####AQAAfwEAAUYAAAC5EAAAAAAAAQAAAAEAAAABAAAAAAAAAAAAAUYAAAAHAP####8BAAB#AQABRwAAAKEQAAAAAAABAAAAAQAAAAEAAAAAAAAAAAABRwAAAAcA#####wEAAH8BAAFIAAAApBAAAAAAAAEAAAABAAAAAQAAAAAAAAAAAAFIAAAABwD#####AQAAfwEAAUkAAACnEAAAAAAAAQAAAAEAAAABAAAAAAAAAAAAAUkAAAAWAP####8BAAAAABAAAAEABWQzYmlzAgMAAABLAAAATAAAABYA#####wEAAAAAEAAAAQAFZDJiaXMCAwAAAE4AAABPAAAAB###########'
    const yy = j3pCreeSVG(stor.lesdiv.cons2, { id: svgId, width: 400, height: 350 })
    yy.style.border = '1px solid black'
    stor.mtgAppLecteur.removeAllDoc()
    stor.mtgAppLecteur.addDoc(svgId, txtFigure, true)
    stor.mtgAppLecteur.giveFormula2(svgId, 'A1', j3pGetRandomInt(1, 359))
    stor.mtgAppLecteur.giveFormula2(svgId, 'A2', j3pGetRandomInt(0, 7))
    stor.mtgAppLecteur.giveFormula2(svgId, 'A3', j3pGetRandomInt(0, 7))
    stor.mtgAppLecteur.calculateAndDisplayAll(true)

    if (stor.proprieteencours === 0) {
      stor.mtgAppLecteur.setVisible(svgId, '#d2', true, true)
      const haz = j3pGetRandomBool()
      const dx = haz ? 'd2' : 'd3'
      const adx = haz ? 'd3' : 'd2'
      stor.mtgAppLecteur.setColor(svgId, '#d1', 0, 0, 255, true)
      stor.mtgAppLecteur.setColor(svgId, '#' + adx, 0, 200, 0, true)
      stor.mtgAppLecteur.setColor(svgId, '#' + dx, 0, 0, 255, true)
      stor.mtgAppLecteur.setVisible(svgId, '#' + dx + 'bis', true, true)
      stor.mtgAppLecteur.setColor(svgId, '#' + dx + 'bis', 0, 200, 0, true)
      if (stor.NomDesDroites === '(dx)') {
        stor.mtgAppLecteur.setVisible(svgId, '#nd1', true, true)
        stor.mtgAppLecteur.setColor(svgId, '#nd1', 0, 0, 255, true)
        stor.mtgAppLecteur.setVisible(svgId, '#nd3', true, true)
        stor.mtgAppLecteur.setVisible(svgId, '#nd2', true, true)
        stor.mtgAppLecteur.setColor(svgId, '#n' + adx, 0, 200, 0, true)
        stor.mtgAppLecteur.setColor(svgId, '#n' + dx, 0, 0, 0, true)
        stor.mtgAppLecteur.setText(svgId, '#n' + adx, stor.droite3, true)
        stor.mtgAppLecteur.setText(svgId, '#n' + dx, stor.droite2, true)
        stor.mtgAppLecteur.setText(svgId, '#nd1', stor.droite1, true)
      } else {
        const dd1 = haz ? stor.droite2 : stor.droite3
        const dd2 = haz ? stor.droite3 : stor.droite2

        stor.mtgAppLecteur.setVisible(svgId, '#B', true, true)
        stor.mtgAppLecteur.setVisible(svgId, '#H', true, true)
        stor.mtgAppLecteur.setVisible(svgId, '#pB', true, true)
        stor.mtgAppLecteur.setVisible(svgId, '#pH', true, true)
        stor.mtgAppLecteur.setText(svgId, '#B', dd1[2], true)
        stor.mtgAppLecteur.setText(svgId, '#H', dd1[1], true)

        stor.mtgAppLecteur.setVisible(svgId, '#C', true, true)
        stor.mtgAppLecteur.setVisible(svgId, '#I', true, true)
        stor.mtgAppLecteur.setVisible(svgId, '#pC', true, true)
        stor.mtgAppLecteur.setVisible(svgId, '#pI', true, true)
        stor.mtgAppLecteur.setText(svgId, '#C', stor.droite1[2], true)
        stor.mtgAppLecteur.setText(svgId, '#I', stor.droite1[1], true)

        stor.mtgAppLecteur.setVisible(svgId, '#A', true, true)
        stor.mtgAppLecteur.setVisible(svgId, '#G', true, true)
        stor.mtgAppLecteur.setVisible(svgId, '#pA', true, true)
        stor.mtgAppLecteur.setVisible(svgId, '#pG', true, true)
        stor.mtgAppLecteur.setText(svgId, '#A', dd2[2], true)
        stor.mtgAppLecteur.setText(svgId, '#G', dd2[1], true)
      }
    }
    if (stor.proprieteencours === 2) {
      stor.mtgAppLecteur.setVisible(svgId, '#d4', true, true)
      stor.mtgAppLecteur.setVisible(svgId, '#m11', true, true)
      stor.mtgAppLecteur.setVisible(svgId, '#m12', true, true)
      stor.mtgAppLecteur.setColor(svgId, '#d1', 0, 0, 255, true)
      stor.mtgAppLecteur.setColor(svgId, '#d3', 0, 0, 255, true)
      if (stor.NomDesDroites === '(dx)') {
        stor.mtgAppLecteur.setVisible(svgId, '#nd1', true, true)
        stor.mtgAppLecteur.setVisible(svgId, '#nd3', true, true)
        stor.mtgAppLecteur.setVisible(svgId, '#nd4', true, true)
        stor.mtgAppLecteur.setText(svgId, '#nd3', stor.droite1, true)
        stor.mtgAppLecteur.setText(svgId, '#nd4', stor.droite3, true)
        stor.mtgAppLecteur.setText(svgId, '#nd1', stor.droite2, true)
        stor.mtgAppLecteur.setColor(svgId, '#nd4', 0, 0, 0, true)
      } else {
        stor.mtgAppLecteur.setVisible(svgId, '#F', true, true)
        stor.mtgAppLecteur.setVisible(svgId, '#D', true, true)
        stor.mtgAppLecteur.setText(svgId, '#F', stor.droite3[2], true)
        stor.mtgAppLecteur.setText(svgId, '#D', stor.droite3[1], true)
        stor.mtgAppLecteur.setVisible(svgId, '#C', true, true)
        stor.mtgAppLecteur.setVisible(svgId, '#pC', true, true)
        stor.mtgAppLecteur.setText(svgId, '#C', stor.droite2[2], true)
        stor.mtgAppLecteur.setVisible(svgId, '#G', true, true)
        stor.mtgAppLecteur.setVisible(svgId, '#pG', true, true)
        stor.mtgAppLecteur.setText(svgId, '#G', stor.droite1[2], true)
      }
    }
    if (stor.proprieteencours === 1) {
      stor.mtgAppLecteur.setVisible(svgId, '#d4', true, true)
      stor.mtgAppLecteur.setVisible(svgId, '#m11', true, true)
      stor.mtgAppLecteur.setVisible(svgId, '#m12', true, true)
      stor.mtgAppLecteur.setVisible(svgId, '#m31', true, true)
      stor.mtgAppLecteur.setVisible(svgId, '#m32', true, true)
      if (stor.NomDesDroites === '(dx)') {
        stor.mtgAppLecteur.setVisible(svgId, '#nd1', true, true)
        stor.mtgAppLecteur.setVisible(svgId, '#nd3', true, true)
        stor.mtgAppLecteur.setVisible(svgId, '#nd4', true, true)
        stor.mtgAppLecteur.setText(svgId, '#nd1', stor.droite1, true)
        stor.mtgAppLecteur.setText(svgId, '#nd3', stor.droite3, true)
        stor.mtgAppLecteur.setText(svgId, '#nd4', stor.droite2, true)
      } else {
        stor.mtgAppLecteur.setVisible(svgId, '#F', true, true)
        stor.mtgAppLecteur.setVisible(svgId, '#D', true, true)
        stor.mtgAppLecteur.setText(svgId, '#F', stor.droite2[2], true)
        stor.mtgAppLecteur.setText(svgId, '#D', stor.droite2[1], true)
        stor.mtgAppLecteur.setVisible(svgId, '#C', true, true)
        stor.mtgAppLecteur.setVisible(svgId, '#I', true, true)
        stor.mtgAppLecteur.setVisible(svgId, '#pC', true, true)
        stor.mtgAppLecteur.setVisible(svgId, '#pI', true, true)
        stor.mtgAppLecteur.setText(svgId, '#C', stor.droite1[2], true)
        stor.mtgAppLecteur.setText(svgId, '#I', stor.droite1[1], true)
        stor.mtgAppLecteur.setVisible(svgId, '#A', true, true)
        stor.mtgAppLecteur.setVisible(svgId, '#G', true, true)
        stor.mtgAppLecteur.setVisible(svgId, '#pA', true, true)
        stor.mtgAppLecteur.setVisible(svgId, '#pG', true, true)
        stor.mtgAppLecteur.setText(svgId, '#A', stor.droite3[2], true)
        stor.mtgAppLecteur.setText(svgId, '#G', stor.droite3[1], true)
      }
    }
  }
  function modif (ki) {
    for (let i = 0; i < ki.length; ki++) {
      for (let j = 0; j < ki[i].length; j++) {
        ki[i][j].style.padding = '0px'
      }
    }
  }
  function gere1 (a) {
    const num = a.manouvellediv
    switch (stor.phrase) {
      case 0 :
        switch (num[num.length - 1]) {
          case '0' :
            stor.placesetiquettes[0].annulecolor()
            stor.placesetiquettes[2].annulecolor()
            stor.placesetiquettes[5].annulecolor()
            break
          case '1' :
            stor.placesetiquettes[1].annulecolor()
            stor.placesetiquettes[2].annulecolor()
            stor.placesetiquettes[5].annulecolor()
            break
          case '2' :
            stor.placesetiquettes[1].annulecolor()
            stor.placesetiquettes[0].annulecolor()
            stor.placesetiquettes[2].annulecolor()
            stor.placesetiquettes[5].annulecolor()
            break
          case '3' :
          case '4' :
            stor.placesetiquettes[3].annulecolor()
            stor.placesetiquettes[4].annulecolor()
            stor.placesetiquettes[6].annulecolor()
            break
        }
        break
      case 1 :
        switch (num[num.length - 1]) {
          case '0' :
          case '1' :
            stor.placesetiquettes[0].annulecolor()
            stor.placesetiquettes[1].annulecolor()
            stor.placesetiquettes[6].annulecolor()
            break
          case '2':
          case '3' :
            stor.placesetiquettes[2].annulecolor()
            stor.placesetiquettes[3].annulecolor()
            stor.placesetiquettes[7].annulecolor()
            break
          case '4':
          case '5' :
            stor.placesetiquettes[4].annulecolor()
            stor.placesetiquettes[5].annulecolor()
            stor.placesetiquettes[8].annulecolor()
            break
        }
    }
  }

  function gere2 (a) {
    const num = a.manouvellediv
    switch (stor.phrase) {
      case 0 :
        switch (num[num.length - 1]) {
          case '0' :
            stor.placesetiquettes[0].annulecolor()
            stor.placesetiquettes[1].annulecolor()
            stor.placesetiquettes[2].annulecolor()
            stor.placesetiquettes[5].annulecolor()
            break
          case '1' :
            stor.placesetiquettes[3].annulecolor()
            stor.placesetiquettes[4].annulecolor()
            stor.placesetiquettes[6].annulecolor()
            break
        }
        break
      case 1 :
        switch (num[num.length - 2] + num[num.length - 1]) {
          case 'n0' :
            stor.placesetiquettes[0].annulecolor()
            stor.placesetiquettes[1].annulecolor()
            stor.placesetiquettes[6].annulecolor()
            break
          case 'z0' :
            stor.placesetiquettes[2].annulecolor()
            stor.placesetiquettes[3].annulecolor()
            stor.placesetiquettes[7].annulecolor()
            break
          case 'z1' :
            stor.placesetiquettes[4].annulecolor()
            stor.placesetiquettes[5].annulecolor()
            stor.placesetiquettes[8].annulecolor()
            break
        }
    }
  }

  function ChangePhrase () {
    for (let i = stor.placesetiquettes.length - 1; i > -1; i--) {
      stor.placesetiquettes[i].detruit()
      stor.placesetiquettes.pop()
    }
    j3pEmpty(stor.lesdiv.L1)
    j3pEmpty(stor.lesdiv.L2)
    j3pEmpty(stor.lesdiv.L3)

    stor.phrase = 1 - stor.phrase
    if (stor.phrase === 1) {
      const tt1 = addDefaultTable(stor.lesdiv.L1, 1, 7)
      modif(tt1)
      j3pAffiche(tt1[0][0], null, 'Les droites &nbsp;')
      stor.placesetiquettes.push(new PlaceEtiquettes(tt1[0][1], 'peti1', stor.etiquettes[0], {}))
      j3pAffiche(tt1[0][2], null, '&nbsp;et&nbsp;')
      stor.placesetiquettes.push(new PlaceEtiquettes(tt1[0][3], 'peti2', stor.etiquettes[0], {}))
      j3pAffiche(tt1[0][4], null, '&nbsp;sont&nbsp;')
      j3pAffiche(tt1[0][6], null, '&nbsp;.')

      const tt2 = addDefaultTable(stor.lesdiv.L2, 1, 7)
      modif(tt2)
      j3pAffiche(tt2[0][0], null, 'Comme la droite &nbsp;')
      stor.placesetiquettes.push(new PlaceEtiquettes(tt2[0][1], 'peti4', stor.etiquettes[0], {}))
      j3pAffiche(tt2[0][2], null, '&nbsp;est&nbsp;')
      j3pAffiche(tt2[0][4], null, '&nbsp;à la droite&nbsp;')
      stor.placesetiquettes.push(new PlaceEtiquettes(tt2[0][5], 'peti6', stor.etiquettes[0], {}))
      j3pAffiche(tt2[0][6], null, '&nbsp;,')

      const tt3 = addDefaultTable(stor.lesdiv.L3, 1, 7)
      modif(tt3)
      j3pAffiche(tt3[0][0], null, 'la droite &nbsp;')
      stor.placesetiquettes.push(new PlaceEtiquettes(tt3[0][1], 'peti7', stor.etiquettes[0], {}))
      j3pAffiche(tt3[0][2], null, '&nbsp;est aussi&nbsp;')
      j3pAffiche(tt3[0][4], null, '&nbsp;à la droite&nbsp;')
      stor.placesetiquettes.push(new PlaceEtiquettes(tt3[0][5], 'peti9', stor.etiquettes[0], {}))
      j3pAffiche(tt3[0][6], null, '&nbsp;.')
      stor.placesetiquettes.push(new PlaceEtiquettes(tt1[0][5], 'peti3', stor.etiquettes[1], {}))
      stor.placesetiquettes.push(new PlaceEtiquettes(tt2[0][3], 'peti5', stor.etiquettes[1], {}))
      stor.placesetiquettes.push(new PlaceEtiquettes(tt3[0][3], 'peti8', stor.etiquettes[1], {}))
    } else {
      const tt1 = addDefaultTable(stor.lesdiv.L1, 1, 9)
      modif(tt1)
      j3pAffiche(tt1[0][0], null, 'Les droites &nbsp;')
      stor.placesetiquettes.push(new PlaceEtiquettes(tt1[0][1], 'peti1', stor.etiquettes[0], {}))
      j3pAffiche(tt1[0][2], null, '&nbsp;et&nbsp;')
      stor.placesetiquettes.push(new PlaceEtiquettes(tt1[0][3], 'peti2', stor.etiquettes[0], {}))
      j3pAffiche(tt1[0][4], null, '&nbsp;sont&nbsp;')
      j3pAffiche(tt1[0][6], null, '&nbsp;à la même droite&nbsp;')
      stor.placesetiquettes.push(new PlaceEtiquettes(tt1[0][7], 'peti3', stor.etiquettes[0], {}))
      j3pAffiche(tt1[0][8], null, '&nbsp;,')

      const tt2 = addDefaultTable(stor.lesdiv.L2, 1, 7)
      modif(tt2)
      j3pAffiche(tt2[0][0], null, 'donc les droites &nbsp;')
      stor.placesetiquettes.push(new PlaceEtiquettes(tt2[0][1], 'peti4', stor.etiquettes[0], {}))
      j3pAffiche(tt2[0][2], null, '&nbsp;et&nbsp;')
      stor.placesetiquettes.push(new PlaceEtiquettes(tt2[0][3], 'peti5', stor.etiquettes[0], {}))
      j3pAffiche(tt2[0][4], null, '&nbsp;sont&nbsp;')
      j3pAffiche(tt2[0][6], null, '&nbsp;entre elles.')

      stor.placesetiquettes.push(new PlaceEtiquettes(tt1[0][5], 'peti6', stor.etiquettes[1], {}))
      stor.placesetiquettes.push(new PlaceEtiquettes(tt2[0][5], 'peti7', stor.etiquettes[1], {}))
    }
  }

  function yareponse () {
    let yaya = true
    for (let i = 0; i < stor.placesetiquettes.length; i++) {
      if (stor.placesetiquettes[i].etiq === -1) {
        yaya = false
        break
      }
    }
    if (me.isElapsed) { yaya = true }
    return yaya
  }

  function bonneReponse () {
    switch (stor.phrase) {
      case 0 :
        var d1 = stor.placesetiquettes[0].contenu
        var d2 = stor.placesetiquettes[1].contenu
        var d3 = stor.placesetiquettes[2].contenu
        var d4 = stor.placesetiquettes[3].contenu
        var d5 = stor.placesetiquettes[4].contenu
        var p1 = stor.placesetiquettes[5].contenu
        var p2 = stor.placesetiquettes[6].contenu
        stor.l1d1 = d1
        stor.l1d2 = d3
        stor.l1p = p1
        stor.l2d1 = d2
        stor.l2d2 = d3
        stor.l2p = p1
        stor.l3d1 = d4
        stor.l3d2 = d5
        stor.l3p = p2
        stor.l2td = stor.lesdiv.AL1
        stor.l3td = stor.lesdiv.AL2
        stor.okdonnees1 = (
          ((d1 === stor.droite1) && (d3 === stor.droite2) && (p1[0] === ds.textes.prop[stor.proprieteencours][0])) ||
          ((d1 === stor.droite2) && (d3 === stor.droite1) && (p1[0] === ds.textes.prop[stor.proprieteencours][0])) ||
          ((d2 === stor.droite1) && (d3 === stor.droite2) && (p1[0] === ds.textes.prop[stor.proprieteencours][0])) ||
          ((d2 === stor.droite2) && (d3 === stor.droite1) && (p1[0] === ds.textes.prop[stor.proprieteencours][0])))
        stor.okdonnees2 = (
          ((d1 === stor.droite3) && (d3 === stor.droite2) && (p1[0] === ds.textes.prop[stor.proprieteencours][1])) ||
          ((d1 === stor.droite2) && (d3 === stor.droite3) && (p1[0] === ds.textes.prop[stor.proprieteencours][1])) ||
          ((d2 === stor.droite3) && (d3 === stor.droite2) && (p1[0] === ds.textes.prop[stor.proprieteencours][1])) ||
          ((d2 === stor.droite2) && (d3 === stor.droite3) && (p1[0] === ds.textes.prop[stor.proprieteencours][1])))
        stor.okconcl = (
          ((d4 === stor.droite3) && (d5 === stor.droite1) && (p2[0] === ds.textes.prop[stor.proprieteencours][2])) ||
          ((d4 === stor.droite1) && (d5 === stor.droite3) && (p2[0] === ds.textes.prop[stor.proprieteencours][2])))
        stor.okcoherencedroite = true // mmm nomalement (d1,d2) === (d4,d5)
        stor.okcoherencepos = true
        return (stor.okdonnees1 && stor.okdonnees2 && stor.okconcl)

      case 1 :
        d1 = stor.placesetiquettes[0].contenu
        d2 = stor.placesetiquettes[1].contenu
        d3 = stor.placesetiquettes[2].contenu
        d4 = stor.placesetiquettes[3].contenu
        d5 = stor.placesetiquettes[4].contenu
        var d6 = stor.placesetiquettes[5].contenu
        p1 = stor.placesetiquettes[6].contenu
        p2 = stor.placesetiquettes[7].contenu
        var p3 = stor.placesetiquettes[8].contenu
        stor.l1d1 = d1
        stor.l1d2 = d2
        stor.l1p = p1
        stor.l2d1 = d4
        stor.l2d2 = d3
        stor.l2p = p2
        stor.l3d1 = d5
        stor.l3d2 = d6
        stor.l3p = p3
        stor.l2td = stor.lesdiv.AL2
        stor.l3td = stor.lesdiv.AL3
        stor.okdonnees1 = (
          ((d1 === stor.droite1) && (d2 === stor.droite2) && (p1[0] === ds.textes.prop[stor.proprieteencours][0])) ||
          ((d1 === stor.droite2) && (d2 === stor.droite1) && (p1[0] === ds.textes.prop[stor.proprieteencours][0])) ||
          ((d3 === stor.droite1) && (d4 === stor.droite2) && (p2[0] === ds.textes.prop[stor.proprieteencours][0])) ||
          ((d3 === stor.droite2) && (d4 === stor.droite1) && (p2[0] === ds.textes.prop[stor.proprieteencours][0])))
        stor.okdonnees2 = (
          ((d1 === stor.droite3) && (d2 === stor.droite2) && (p1[0] === ds.textes.prop[stor.proprieteencours][1])) ||
          ((d1 === stor.droite2) && (d2 === stor.droite3) && (p1[0] === ds.textes.prop[stor.proprieteencours][1])) ||
          ((d4 === stor.droite3) && (d3 === stor.droite2) && (p2[0] === ds.textes.prop[stor.proprieteencours][1])) ||
          ((d4 === stor.droite2) && (d3 === stor.droite3) && (p2[0] === ds.textes.prop[stor.proprieteencours][1])))
        stor.okconcl = (
          ((d6 === stor.droite3) && (d5 === stor.droite1) && (p3[0] === ds.textes.prop[stor.proprieteencours][2])) ||
          ((d6 === stor.droite1) && (d5 === stor.droite3) && (p3[0] === ds.textes.prop[stor.proprieteencours][2])))
        stor.okcoherencedroite = ((d3 === d5) || (d4 === d6))
        stor.okcoherencepos = (p2[0] === p3[0])
        return (stor.okdonnees1 && stor.okdonnees2 && stor.okconcl)
    }
  }

  function getDonnees () {
    return {

      // Nombre de répétitions de l’exercice
      nbrepetitions: 9,
      nbetapes: 1,

      Presentation: true,
      ConclusionDonnee: 'oui',
      Propriete3: true,
      Propriete2: true,
      Propriete1: true,
      Enoncep3: true,
      NomDesDroites: 'les deux',
      FormulationImposee: false,
      QuestionApresDonnees: false,

      // Si indication !== "" alors un lien "Indication" s’affiche dans la zone Inférieure Gauche (zones.IG
      // une indication<br>de deux lignes...";
      indication: '',

      // pour le cas de presentation1bis
      // En effet pas d’indication possible pour cette présentation
      // surchargeindication: false;

      // Nombre de chances dont dispose l’él§ve pour répondre à la question
      nbchances: 2,

      structure: 'presentation3', //  || "presentation2" || "presentation3"  || "presentation1bis"

      // Paramètres de la section, avec leurs valeurs par défaut.

      /*
          Les textes présents dans la section
          Sont donc accessibles dans le code par donneesSection.textes.phrase1
          Possibilité d’écrire dans le code :
          var lestextes: donneesSection.textes;
          puis accès à l’aide de lestextes.phrase1
          */
      textes: {
        couleurexplique: '#A9E2F3',
        couleurcorrec: '#A9F5A9',
        couleurenonce: '#D8D8D8',
        couleurenonceg: '#F3E2A9',
        couleuretiquettes: '#A9E2F3',
        Consigne1: 'Complète la démonstration à l’aide des étiquettes',
        consigne1vers2: [',', '.'],
        Consigne21: ' Les droites £a et £b sont £c . ',
        Consigne22: ' pour prouver que les droites £a et £b sont £c . ',
        Consigne23: ' pour prouver que les droites £a et £b sont .... ',
        prop: [['parallèles', 'parallèles', 'parallèles'], ['perpendiculaires', 'perpendiculaires', 'parallèles'], ['parallèles', 'perpendiculaires', 'perpendiculaires']],
        prop2: [['£a // £b ', '£a // £b '], ['£a ⊥ £b', '£a ⊥ £b'], ['£a // £b', '£a ⊥ £b ']],
        proprep: [['Les droites ETIQ1 et ETIQ1 sont ETIQ2 à la même droite ETIQ1,', 'donc les droites ETIQ1 et ETIQ1 sont ETIQ2 entre elles.'], ['Les droites ETIQ1 et ETIQ1 sont ETIQ2.', 'Comme la droite ETIQ1 est ETIQ3 à la droite ETIQ1,', 'la droite ETIQ1 est aussi ETIQ3 à la droite ETIQ1 .']],
        etiquette: 'Etiquettes',
        posrel: [['parallèles', 'parallèle'], ['perpendiculaires', 'perpendiculaire']],
        Change: 'Changer de phrase',
        aide: 'Aide',
        ersaitdeja: 'On connait déjà la position relative de £a et £b !',
        ersaispas: 'L’énoncé n’indique pas que les droites £a et £b sont £c !',
        erfaux: 'L’énoncé indique que les droites £a et £b ne sont pas £c !',
        erconcl: 'La conclusion est fausse !',
        mankconcl: 'La conclusion ne correspond pas à la question posée !',
        mankdonnees: 'Il manque des données de l’énoncé',
        ermeme: ['La droite £a ne peut pas être perpendiculaire à elle même !', 'Il est inutile d’écrire que la droite £a est parallèle à elle-même'],
        ercohe: "L’emploi du mot 'aussi' implique de retrouver des éléments identiques dans la dernière phrase.",
        rep: ['Les droites £a et £c sont £d à la droite £b , <BR> donc les droites £a et £c sont £e entre elles.', 'Les droites £a et £b sont £d. <BR> Comme la droite £c est £f à la droite £b,<BR> la droite £c est aussi £f à la droite £a .'],
        copro: [['Si deux droites sont parallèles à une même troisième droite, <BR> alors elles sont parallèles entre elles.', 'Si deux droites sont parallèles entre elles <BR> et si une troisième droite est parallèle à l’une, <BR> alors elle est aussi parallèle à l’autre.'], 'Si deux droites sont perpendiculaires à une même troisième droite, <BR> alors elles sont parallèles entre elles.', 'Si deux droites sont parallèles entre elles <BR> et si une troisième droite est perpendiculaire à l’une, <BR> alors elle est aussi perpendiculaire à l’autre.']
      },

      // Par défaut, pas de temps limite. Décommentez pour avoir un temps limite par défaut. En secondes.

      // limite: 10;
      /*
          Phrase d’état renvoyée par la section
          Si pe=0 alors la section est quantitative et renvoie un score entre 0 et 1.
          Dans le cas où la section est qualitative, lister les différents pe renvoyées :
              pe_1: "toto"
              pe_2: "tata"
              etc
          Dans la partie navigation, il faudra affecter la variable parcours.pe à l’une des précédentes.
          Par exemple,
          parcours.pe: donneesSection.pe_2
          */
      pe: 0
    }
  }

  function initSection () {
    me.donneesSection = getDonnees()
    ds = me.donneesSection

    me.surcharge()
    if (ds.Presentation) { ds.structure = 'presentation1' }

    // Construction de la page
    me.construitStructurePage({ structure: ds.structure, ratioGauche: 0.75 })

    // DECLARATION DES VARIABLES
    // reponse attendue
    stor.TypeEnonce = 'Les deux'
    if ((ds.TypeEnonce === 'Texte') || (ds.TypeEnonce === 'texte') || (ds.TypeEnonce === 'Textes') || (ds.TypeEnonce === 'textes')) { stor.TypeEnonce = 'Texte' }
    if ((ds.TypeEnonce === 'Figure') || (ds.TypeEnonce === 'figure') || (ds.TypeEnonce === 'figures') || (ds.TypeEnonce === 'Figures')) { stor.TypeEnonce = 'Figure' }
    stor.repprefer = 0
    if ((ds.Enoncep3 === 'e2') || (ds.Enoncep3 === 'E2')) { stor.repprefer = 1 }

    stor.proprietepossible = []
    if (ds.Propriete3) { stor.proprietepossible.push(0) }
    if (ds.Propriete1) { stor.proprietepossible.push(1) }
    if (ds.Propriete2) { stor.proprietepossible.push(2) }
    if (stor.proprietepossible.length === 0) { stor.proprietepossible = [0, 1, 2] }

    /*
             Par convention,`
            `   me.typederreurs[0] = nombre de bonnes réponses
                me.typederreurs[1] = nombre de fois où l’élève a utilisé une chance supplémentaire
                me.typederreurs[2] = nombre de mauvaises réponses
                me.typederreurs[10] = nombre de fois où l’élève a eu faux à cause d’un temps limite
                LES AUTRES peuvent être utilisées par la détermination de la pe dans le cas d’une section qualitative
             */
    me.typederreurs = [0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0]

    me.score = 0

    me.afficheTitre('Démontrer que deux droites sont parallèles ou perpendiculaires')

    if (ds.indication) me.indication(me.zones.IG, ds.indication)
    stor.bull = []
    getMtgCore({ withMathJax: true })
      .then(
        // success
        (mtgAppLecteur) => {
          stor.mtgAppLecteur = mtgAppLecteur
          enonceMain()
        },
        // failure
        (error) => {
          j3pShowError(error, { message: 'mathgraph n’est pas correctement chargé', mustNotify: true })
        })
      // plantage dans enonceMain
      .catch(j3pShowError)
  }
  function enonceMain () {
    // A chaque répétition, on nettoie la scène
    // A commenter si besoin
    let i
    for (i = stor.bull.length - 1; i > -1; i--) {
      stor.bull[i].disable()
      stor.bull.pop()
    }
    me.videLesZones()

    stor.etiquettes = []
    stor.bull = []
    stor.placesetiquettes = []
    let tablettres = []
    for (let i = 65; i < 91; i++) tablettres.push(i)
    tablettres = j3pShuffle(tablettres)
    stor.leslettres = []
    for (i = 0; i < 6; i++) {
      stor.leslettres.push(String.fromCharCode(tablettres[i]))
    }
    stor.NomDesDroites = ds.NomDesDroites
    if ((stor.NomDesDroites !== '(dx)') && (stor.NomDesDroites !== '(AB)')) {
      var nd = ['(dx)', '(AB)']
      stor.NomDesDroites = nd[j3pGetRandomInt(0, 1)]
    }
    stor.proprieteencours = stor.proprietepossible[j3pGetRandomInt(0, (stor.proprietepossible.length - 1))]
    stor.yachange = false
    if (stor.NomDesDroites === '(AB)') {
      if (stor.proprieteencours === 2) {
        stor.leslettres[4] = stor.leslettres[0]
        stor.leslettres[5] = stor.leslettres[2]
        stor.yachange = true
      }
      if (j3pGetRandomBool()) {
        if (stor.proprieteencours === 1) {
          stor.leslettres[3] = stor.leslettres[4]
          stor.leslettres[2] = stor.leslettres[0]
          stor.yachange = true
        }
      }
      stor.droite1 = '(' + stor.leslettres[0] + stor.leslettres[1] + ')'
      stor.droite2 = '(' + stor.leslettres[2] + stor.leslettres[3] + ')'
      stor.droite3 = '(' + stor.leslettres[4] + stor.leslettres[5] + ')'
    } else {
      nd = ['(d1)', '(d2)', '(d3)', '(d4)']
      nd = j3pShuffle(nd)
      stor.droite1 = nd[0]
      stor.droite2 = nd[1]
      stor.droite3 = nd[2]
      if (stor.proprieteencours === 2) {
        stor.yachange = true
      }
    }
    stor.TypeEnonceencours = stor.TypeEnonce
    if (stor.TypeEnonceencours === 'Les deux') {
      i = j3pGetRandomInt(0, 1)
      if (i === 0) { stor.TypeEnonceencours = 'Texte' } else { stor.TypeEnonceencours = 'Figure' }
    }

    stor.lesdiv = {}
    stor.lesdiv.conteneur = j3pAddElt(me.zonesElts.MG, 'div', {
      style: me.styles.etendre('toutpetit.enonce', { padding: '10px' })
    })
    const ttt = addDefaultTable(stor.lesdiv.conteneur, 1, 3)
    modif(ttt)
    stor.lesdiv.consigne = ttt[0][0]
    if (!ds.Presentation) {
      stor.lesdiv.correction = ttt[0][2]
      ttt[0][1].style.width = '20px'
    } else {
      stor.lesdiv.correction = j3pAddElt(me.zonesElts.MD, 'div', {
        style: me.styles.etendre('toutpetit.enonce', { padding: '10px' })
      })
    }

    let iop
    const tcons1 = addDefaultTable(stor.lesdiv.consigne, 2, 1)
    modif(tcons1)
    if (!ds.QuestionApresDonnees) {
      stor.lesdiv.zCons1 = tcons1[0][0]
      iop = addDefaultTable(tcons1[1][0], 1, 2)
      modif(iop)
    } else {
      stor.lesdiv.zCons1 = tcons1[1][0]
      iop = addDefaultTable(tcons1[0][0], 1, 2)
      modif(iop)
    }
    stor.lesdiv.cons2 = iop[0][0]
    stor.lesdiv.cons2bis = iop[0][1]
    i = j3pGetRandomInt(0, 1)
    if (i === 0) {
      var unedroite = stor.droite1
      var autredroite = stor.droite2
    } else {
      unedroite = stor.droite2
      autredroite = stor.droite1
    }
    i = j3pGetRandomInt(0, 1)
    if (i === 0) {
      var unedroite2 = stor.droite2
      var autredroite2 = stor.droite3
    } else {
      unedroite2 = stor.droite3
      autredroite2 = stor.droite2
    }
    let maquest = '<b>' + ds.textes.Consigne1 // </b> ajouté ci-dessous
    if ((ds.ConclusionDonnee === 'oui') || (ds.ConclusionDonnee === 'droites')) {
      maquest += ds.textes.consigne1vers2[0] + '\n'
      if (ds.ConclusionDonnee === 'oui') {
        maquest += ds.textes.Consigne22
      } else {
        maquest += ds.textes.Consigne23
      }
    } else { maquest += ds.textes.consigne1vers2[1] }
    maquest += '</b>'
    j3pAffiche(stor.lesdiv.zCons1, null, maquest,
      {
        a: stor.droite1,
        b: stor.droite3,
        c: ds.textes.prop[stor.proprieteencours][2]
      })
    switch (stor.TypeEnonceencours) {
      case 'Texte':
        j3pAffiche(stor.lesdiv.cons2, null, ds.textes.Consigne21 + '\n',
          {
            a: unedroite,
            b: autredroite,
            c: ds.textes.prop[stor.proprieteencours][0]
          })
        j3pAffiche(stor.lesdiv.cons2, null, ds.textes.Consigne21 + '\n',
          {
            a: unedroite2,
            b: autredroite2,
            c: ds.textes.prop[stor.proprieteencours][1]
          })
        break
      case 'Figure' :
        afficheFig(stor.proprieteencours)
        if (j3pGetRandomBool()) {
          j3pAffiche(stor.lesdiv.cons2bis, null, ds.textes.prop2[stor.proprieteencours][0] + '\n',
            {
              a: unedroite,
              b: autredroite
            })
          j3pAffiche(stor.lesdiv.cons2bis, null, ds.textes.prop2[stor.proprieteencours][1],
            {
              a: unedroite2,
              b: autredroite2
            })
        } else {
          j3pAffiche(stor.lesdiv.cons2bis, null, ds.textes.prop2[stor.proprieteencours][1] + '\n',
            {
              a: unedroite2,
              b: autredroite2
            })
          j3pAffiche(stor.lesdiv.cons2bis, null, ds.textes.prop2[stor.proprieteencours][0],
            {
              a: unedroite,
              b: autredroite
            })
        }
    }

    const ttra = addDefaultTableDetailed(stor.lesdiv.conteneur, 1, 3)
    ttra.cells[0][1].style.width = '20px'
    const ttra2 = addDefaultTable(ttra.cells[0][0], 4, 1)
    modif(ttra2)
    stor.lesdiv.Bouton = ttra2[0][0]
    const v1 = addDefaultTable(ttra2[1][0], 1, 2)
    modif(v1)
    stor.lesdiv.L1 = v1[0][0]
    stor.lesdiv.AL1 = v1[0][1]

    const v2 = addDefaultTable(ttra2[2][0], 1, 2)
    modif(v2)
    stor.lesdiv.L2 = v2[0][0]
    stor.lesdiv.AL2 = v2[0][1]
    const v3 = addDefaultTable(ttra2[3][0], 1, 2)
    modif(v3)
    stor.lesdiv.L3 = v3[0][0]
    stor.lesdiv.AL3 = v3[0][1]
    const attati = addDefaultTableDetailed(ttra.cells[0][2], 3, 1)
    modif(attati)
    stor.etikAef = attati.table
    stor.lesdiv.etiqTire = attati.cells[0][0]
    stor.lesdiv.etiqDte = attati.cells[1][0]
    stor.lesdiv.etiqPar = attati.cells[2][0]
    attati.table.style.border = '1px solid black'
    attati.cells[0][0].style.border = attati.cells[1][0].style.border = attati.cells[2][0].style.border = '1px solid black'
    j3pAffiche(stor.lesdiv.etiqTire, null, '<u>' + ds.textes.etiquette + '</u>')

    if (!ds.FormulationImposee) {
      stor.bouton1 = j3pAjouteBouton(stor.lesdiv.Bouton, ChangePhrase, { className: 'MepBoutons', value: ds.textes.Change })
    }

    let droite1 = stor.droite1
    let droite2 = stor.droite2
    let droite3 = stor.droite3

    ttra.table.classList.add('travail')
    stor.lesdiv.consigne.classList.add('enonce')
    stor.lesdiv.explications = j3pAddElt(stor.lesdiv.conteneur, 'div')
    stor.lesdiv.solution = j3pAddElt(stor.lesdiv.conteneur, 'div')
    stor.lesdiv.solution.style.color = me.styles.toutpetit.correction.color
    stor.lesdiv.explications.style.color = me.styles.cfaux

    i = j3pGetRandomInt(0, 1)
    if (i === 0) {
      var u = droite1
      droite1 = droite2
      droite2 = u
    }
    i = j3pGetRandomInt(0, 1)
    if (i === 0) {
      u = droite1
      droite1 = droite3
      droite3 = u
    }
    i = j3pGetRandomInt(0, 1)
    if (i === 0) {
      u = droite2
      droite2 = droite3
      droite3 = u
    }
    let listeetiq = [droite1, droite2, droite3]
    stor.etiquettes[stor.etiquettes.length] = new Etiquettes(stor.lesdiv.etiqDte, 'etiqdroites', listeetiq, {
      dispo: 'ligne',
      afaire: gere1
    })
    listeetiq = ds.textes.posrel
    stor.etiquettes[stor.etiquettes.length] = new Etiquettes(stor.lesdiv.etiqPar, 'etiqposrel', listeetiq, {
      dispo: 'colonne',
      afaire: gere2
    })
    i = j3pGetRandomInt(0, 1)
    stor.phrase = i
    if (ds.FormulationImposee) {
      if (stor.proprieteencours === 2) { stor.phrase = 0 }
      if (stor.proprieteencours === 1) { stor.phrase = 1 }
      if (stor.proprieteencours === 0) {
        if (ds.EnonceP3 === 'énoncé 1') { stor.phrase = 0 } else { stor.phrase = 1 }
      }
    }
    ChangePhrase()

    me.finEnonce()
  }

  switch (me.etat) {
    case 'enonce':
      // code exécuté au lancement de la section
      if (me.debutDeLaSection) {
        initSection()
      } else {
        enonceMain()
      }
      break // case "enonce":

    case 'correction':
      stor.lesdiv.explications.classList.remove('explications')

      if (!yareponse()) {
        stor.lesdiv.correction.style.color = me.styles.cfaux
        stor.lesdiv.correction.innerHTML = reponseIncomplete
      } else { // Une réponse a été saisie
        for (let i = stor.bull.length - 1; i > -1; i--) {
          stor.bull[i].disable()
          stor.bull.pop()
        }
        // Bonne réponse
        if (bonneReponse()) {
          me._stopTimer()
          me.score += j3pArrondi(1 / ds.nbetapes, 1)
          stor.lesdiv.correction.style.color = me.styles.cbien
          stor.lesdiv.correction.innerHTML = cBien

          stor.lesdiv.AL1.innerHTML = ''
          stor.lesdiv.AL2.innerHTML = ''
          stor.lesdiv.AL3.innerHTML = ''
          stor.lesdiv.explications.innerHTML = ''

          for (let i = stor.placesetiquettes.length - 1; i > -1; i--) {
            stor.placesetiquettes[i].corrige(true)
            stor.placesetiquettes[i].disable()
            stor.placesetiquettes.pop()
          }
          for (let i = stor.etiquettes.length - 1; i > -1; i--) {
            stor.etiquettes[i].disable()
            stor.etiquettes.pop()
          }
          if (!ds.FormulationImposee) {
            j3pEmpty(stor.lesdiv.Bouton)
          }
          if (!stor.okcoherencedroite || !stor.okcoherencepos) {
            j3pAffiche(stor.lesdiv.explications, null, 'Les données sont justes, mais la formulation est un peu compliquée.')
          }

          j3pEmpty(stor.etikAef)

          me.typederreurs[0]++
          me.cacheBoutonValider()
          me.etat = 'navigation'
          me.sectionCourante()
        } else {
          // Pas de bonne réponse
          stor.lesdiv.correction.style.color = me.styles.cfaux

          // A cause de la limite de temps :
          if (me.isElapsed) { // limite de temps
            me._stopTimer()

            stor.lesdiv.correction.innerHTML = tempsDepasse
            me.typederreurs[10]++
            me.cacheBoutonValider()

            /// /
            stor.lesdiv.AL1.innerHTML = ''
            stor.lesdiv.AL2.innerHTML = ''
            stor.lesdiv.AL3.innerHTML = ''

            stor.lesdiv.explications.innerHTML = ''
            if ((!stor.okdonnees1) || (!stor.okdonnees2)) {
              j3pAffiche(stor.lesdiv.explications, null, ds.textes.mankdonnees + '\n')
            }
            if (!stor.okconcl) {
              j3pAffiche(stor.lesdiv.explications, null, ds.textes.mankconcl)
            }
            stor.lesdiv.explications.classList.add('explique')
            stor.lesdiv.solution.classList.add('correction')

            var maco = ds.textes.rep[0]
            if (stor.proprieteencours === 0) { maco = ds.textes.rep[stor.repprefer] }
            if (stor.proprieteencours === 2) { maco = ds.textes.rep[1] }
            j3pAffiche(stor.lesdiv.solution, null, maco,
              {
                a: stor.droite1,
                b: stor.droite2,
                c: stor.droite3,
                d: ds.textes.prop[stor.proprieteencours][0],
                e: ds.textes.prop[stor.proprieteencours][2],
                f: ds.textes.prop[stor.proprieteencours][1].substring(0, ds.textes.prop[stor.proprieteencours][1].length - 1)
              })

            for (let i = stor.etiquettes.length - 1; i > -1; i--) {
              stor.etiquettes[i].disable()
              stor.etiquettes.pop()
            }
            j3pEmpty(stor.etikAef)
            j3pStyle(stor.etikAef, { border: '0px' })
            if (!ds.FormulationImposee) {
              j3pEmpty(stor.lesdiv.Bouton)
            }

            for (let i = stor.placesetiquettes.length - 1; i > -1; i--) {
              stor.placesetiquettes[i].corrige(true)
            }

            // test l1
            if ((stor.l1d1 !== '') && (stor.l1d2 !== '') && (stor.l1d3 !== '') && (stor.l1p[0] !== undefined)) {
              if (stor.l1d1 === stor.l1d2) {
                if (stor.l1p[1] === 'perpendiculaire') {
                  stor.bull[stor.bull.length] = new BulleAide(stor.lesdiv.AL1, ds.textes.ermeme[0].replace('£a', stor.l1d1), { place: 0 })
                } else {
                  stor.bull[stor.bull.length] = new BulleAide(stor.lesdiv.AL1, ds.textes.ermeme[1].replace('£a', stor.l1d1), { place: 0 })
                }
                if (stor.phrase === 0) {
                  stor.placesetiquettes[5].corrige(false, true)
                  stor.placesetiquettes[0].corrige(false, true)
                  stor.placesetiquettes[2].corrige(false, true)
                } else {
                  stor.placesetiquettes[6].corrige(false, true)
                  stor.placesetiquettes[0].corrige(false, true)
                  stor.placesetiquettes[1].corrige(false, true)
                }
              }
              if (
                ((stor.l1d1 === stor.droite1) && (stor.l1d2 === stor.droite2) && (stor.l1p[0] !== ds.textes.prop[stor.proprieteencours][0])) ||
                ((stor.l1d1 === stor.droite2) && (stor.l1d2 === stor.droite1) && (stor.l1p[0] !== ds.textes.prop[stor.proprieteencours][0])) ||
                ((stor.l1d1 === stor.droite2) && (stor.l1d2 === stor.droite3) && (stor.l1p[0] !== ds.textes.prop[stor.proprieteencours][1])) ||
                ((stor.l1d1 === stor.droite3) && (stor.l1d2 === stor.droite2) && (stor.l1p[0] !== ds.textes.prop[stor.proprieteencours][1]))
              ) {
                const content = ds.textes.erfaux
                  .replace('£a', stor.l1d1)
                  .replace('£b', stor.l1d2)
                  .replace('£c', stor.l1p[0])
                stor.bull[stor.bull.length] = new BulleAide(stor.lesdiv.AL1, content, { place: 0 })
                if (stor.phrase === 0) {
                  stor.placesetiquettes[5].corrige(false, true)
                  stor.placesetiquettes[0].corrige(false, true)
                  stor.placesetiquettes[2].corrige(false, true)
                } else {
                  stor.placesetiquettes[6].corrige(false)
                  stor.placesetiquettes[0].corrige(false, true)
                  stor.placesetiquettes[1].corrige(false, true)
                }
              }

              if (
                ((stor.l1d1 === stor.droite1) && (stor.l1d2 === stor.droite3)) ||
                ((stor.l1d1 === stor.droite3) && (stor.l1d2 === stor.droite1))
              ) {
                const content = ds.textes.ersaispas
                  .replace('£a', stor.l1d1)
                  .replace('£b', stor.l1d2)
                  .replace('£c', stor.l1p[0])
                stor.bull[stor.bull.length] = new BulleAide(stor.lesdiv.AL1, content, { place: 0 })
                if (stor.phrase === 0) {
                  stor.placesetiquettes[5].corrige(false, true)
                  stor.placesetiquettes[0].corrige(false, true)
                  stor.placesetiquettes[2].corrige(false, true)
                } else {
                  stor.placesetiquettes[6].corrige(false, true)
                  stor.placesetiquettes[0].corrige(false, true)
                  stor.placesetiquettes[1].corrige(false, true)
                }
              }
            }
            // test l2
            if ((stor.l2d1 !== '') && (stor.l2d2 !== '') && (stor.l2d3 !== '') && (stor.l2p[0] !== undefined)) {
              if (stor.l2d1 === stor.l2d2) {
                if (stor.l2p[1] === 'perpendiculaire') {
                  stor.bull[stor.bull.length] = new BulleAide(stor.l2td, ds.textes.ermeme[0].replace('£a', stor.l2d1), { place: 0 })
                } else {
                  stor.bull[stor.bull.length] = new BulleAide(stor.l2td, ds.textes.ermeme[1].replace('£a', stor.l2d1), { place: 0 })
                }
                if (stor.phrase === 0) {
                  stor.placesetiquettes[1].corrige(false, true)
                  stor.placesetiquettes[2].corrige(false, true)
                  stor.placesetiquettes[5].corrige(false, true)
                } else {
                  stor.placesetiquettes[2].corrige(false, true)
                  stor.placesetiquettes[3].corrige(false, true)
                  stor.placesetiquettes[7].corrige(false, true)
                }
              }
              if (
                ((stor.l2d1 === stor.droite1) && (stor.l2d2 === stor.droite2) && (stor.l2p[0] !== ds.textes.prop[stor.proprieteencours][0])) ||
                ((stor.l2d1 === stor.droite2) && (stor.l2d2 === stor.droite1) && (stor.l2p[0] !== ds.textes.prop[stor.proprieteencours][0])) ||
                ((stor.l2d1 === stor.droite2) && (stor.l2d2 === stor.droite3) && (stor.l2p[0] !== ds.textes.prop[stor.proprieteencours][1])) ||
                ((stor.l2d1 === stor.droite3) && (stor.l2d2 === stor.droite2) && (stor.l2p[0] !== ds.textes.prop[stor.proprieteencours][1]))) {
                stor.bull[stor.bull.length] = new BulleAide(stor.l2td, ds.textes.erfaux.replace('£a', stor.l2d1).replace('£b', stor.l2d2).replace('£c', stor.l2p[0]), { place: 0 })
                if (stor.phrase === 0) {
                  stor.placesetiquettes[1].corrige(false, true)
                  stor.placesetiquettes[2].corrige(false, true)
                  stor.placesetiquettes[5].corrige(false, true)
                } else {
                  stor.placesetiquettes[2].corrige(false, true)
                  stor.placesetiquettes[3].corrige(false, true)
                  stor.placesetiquettes[7].corrige(false, true)
                }
              }
              if (
                ((stor.l2d1 === stor.droite1) && (stor.l2d2 === stor.droite3)) ||
                ((stor.l2d1 === stor.droite3) && (stor.l2d2 === stor.droite1))) {
                stor.bull[stor.bull.length] = new BulleAide(stor.l2td, ds.textes.ersaispas.replace('£a', stor.l2d1).replace('£b', stor.l2d2).replace('£c', stor.l2p[0]), { place: 0 })
                if (stor.phrase === 0) {
                  stor.placesetiquettes[1].corrige(false, true)
                  stor.placesetiquettes[2].corrige(false, true)
                  stor.placesetiquettes[5].corrige(false, true)
                } else {
                  stor.placesetiquettes[2].corrige(false, true)
                  stor.placesetiquettes[3].corrige(false, true)
                  stor.placesetiquettes[7].corrige(false, true)
                }
              }
            }
            // test l3
            if ((stor.l3d1 !== '') && (stor.l3d2 !== '') && (stor.l3d3 !== '') && (stor.l3p[0] !== undefined)) {
              if (stor.l3d1 === stor.l3d2) {
                stor.bull[stor.bull.length] = new BulleAide(stor.l3td, 'Tu écris deux fois la droite ' + stor.l3d1, { place: 0 })
                if (stor.phrase === 0) {
                  stor.placesetiquettes[3].corrige(false, true)
                  stor.placesetiquettes[4].corrige(false, true)
                  stor.placesetiquettes[6].corrige(false, true)
                } else {
                  stor.placesetiquettes[4].corrige(false, true)
                  stor.placesetiquettes[5].corrige(false, true)
                  stor.placesetiquettes[8].corrige(false, true)
                }
              }
              if (
                ((stor.l3d1 === stor.droite1) && (stor.l3d2 === stor.droite2)) ||
                ((stor.l3d1 === stor.droite2) && (stor.l3d2 === stor.droite1)) ||
                ((stor.l3d1 === stor.droite2) && (stor.l3d2 === stor.droite3)) ||
                ((stor.l3d1 === stor.droite3) && (stor.l3d2 === stor.droite2))) {
                stor.bull[stor.bull.length] = new BulleAide(stor.l3td, ds.textes.ersaitdeja.replace('£a', stor.l3d1).replace('£b', stor.l3d2), { place: 0 })
                if (stor.phrase === 0) {
                  stor.placesetiquettes[3].corrige(false, true)
                  stor.placesetiquettes[4].corrige(false, true)
                  stor.placesetiquettes[6].corrige(false, true)
                } else {
                  stor.placesetiquettes[4].corrige(false, true)
                  stor.placesetiquettes[5].corrige(false, true)
                  stor.placesetiquettes[8].corrige(false, true)
                }
              }
              if (
                ((stor.l3d1 === stor.droite1) && (stor.l3d2 === stor.droite3) && (stor.l3p[0] !== ds.textes.prop[stor.proprieteencours][2])) ||
                ((stor.l3d1 === stor.droite3) && (stor.l3d2 === stor.droite1) && (stor.l3p[0] !== ds.textes.prop[stor.proprieteencours][2]))) {
                stor.bull[stor.bull.length] = new BulleAide(stor.l3td, ds.textes.erconcl, { place: 0 })
                if (stor.phrase === 0) {
                  stor.placesetiquettes[3].corrige(false, true)
                  stor.placesetiquettes[4].corrige(false, true)
                  stor.placesetiquettes[6].corrige(false, true)
                } else {
                  stor.placesetiquettes[4].corrige(false, true)
                  stor.placesetiquettes[5].corrige(false, true)
                  stor.placesetiquettes[8].corrige(false, true)
                }
              }
              if (stor.phrase === 1) {
                if ((!stor.okcoherencedroite) || (!stor.okcoherencepos)) {
                  stor.bull[stor.bull.length] = new BulleAide(stor.l3td, ds.textes.ercohe, { place: 0 })
                }
                if (!stor.okcoherencepos) {
                  stor.placesetiquettes[7].corrige(false, true)
                  stor.placesetiquettes[8].corrige(false, true)
                }
              }
            }

            for (let i = stor.placesetiquettes.length - 1; i > -1; i--) {
              stor.placesetiquettes[i].disable()
              stor.placesetiquettes.pop()
            }

            /// /

            me.etat = 'navigation'
            me.sectionCourante()
          } else { // Réponse fausse :
            stor.lesdiv.correction.innerHTML = cFaux

            if (me.essaiCourant < ds.nbchances) {
              stor.lesdiv.correction.innerHTML += '<br>' + essaieEncore

              stor.lesdiv.AL1.innerHTML = ''
              stor.lesdiv.AL2.innerHTML = ''
              stor.lesdiv.AL3.innerHTML = ''
              stor.lesdiv.explications.innerHTML = ''

              for (let i = stor.placesetiquettes.length - 1; i > -1; i--) {
                stor.placesetiquettes[i].corrige(true)
              }
              if (stor.l1d1 === stor.l1d2) {
                if (stor.l1p[1] === 'perpendiculaire') {
                  stor.bull[stor.bull.length] = new BulleAide(stor.lesdiv.AL1, ds.textes.ermeme[0].replace('£a', stor.l1d1), { place: 0 })
                } else {
                  stor.bull[stor.bull.length] = new BulleAide(stor.lesdiv.AL1, ds.textes.ermeme[1].replace('£a', stor.l1d1), { place: 0 })
                }
                if (stor.phrase === 0) {
                  stor.placesetiquettes[5].corrige(false)
                  stor.placesetiquettes[0].corrige(false)
                  stor.placesetiquettes[2].corrige(false)
                } else {
                  stor.placesetiquettes[6].corrige(false)
                  stor.placesetiquettes[0].corrige(false)
                  stor.placesetiquettes[1].corrige(false)
                }
              }
              if (
                ((stor.l1d1 === stor.droite1) && (stor.l1d2 === stor.droite2) && (stor.l1p[0] !== ds.textes.prop[stor.proprieteencours][0])) ||
                ((stor.l1d1 === stor.droite2) && (stor.l1d2 === stor.droite1) && (stor.l1p[0] !== ds.textes.prop[stor.proprieteencours][0])) ||
                ((stor.l1d1 === stor.droite2) && (stor.l1d2 === stor.droite3) && (stor.l1p[0] !== ds.textes.prop[stor.proprieteencours][1])) ||
                ((stor.l1d1 === stor.droite3) && (stor.l1d2 === stor.droite2) && (stor.l1p[0] !== ds.textes.prop[stor.proprieteencours][1]))) {
                const content = ds.textes.erfaux
                  .replace('£a', stor.l1d1)
                  .replace('£b', stor.l1d2)
                  .replace('£c', stor.l1p[0])
                stor.bull[stor.bull.length] = new BulleAide(stor.lesdiv.AL1, content, { place: 0 })
                if (stor.phrase === 0) {
                  stor.placesetiquettes[5].corrige(false)
                  stor.placesetiquettes[0].corrige(false)
                  stor.placesetiquettes[2].corrige(false)
                } else {
                  stor.placesetiquettes[6].corrige(false)
                  stor.placesetiquettes[0].corrige(false)
                  stor.placesetiquettes[1].corrige(false)
                }
              }

              if (
                ((stor.l1d1 === stor.droite1) && (stor.l1d2 === stor.droite3)) ||
                ((stor.l1d1 === stor.droite3) && (stor.l1d2 === stor.droite1))
              ) {
                const content = ds.textes.ersaispas
                  .replace('£a', stor.l1d1)
                  .replace('£b', stor.l1d2)
                  .replace('£c', stor.l1p[0])
                stor.bull[stor.bull.length] = new BulleAide(stor.lesdiv.AL1, content, { place: 0 })
                if (stor.phrase === 0) {
                  stor.placesetiquettes[5].corrige(false)
                  stor.placesetiquettes[0].corrige(false)
                  stor.placesetiquettes[2].corrige(false)
                } else {
                  stor.placesetiquettes[6].corrige(false)
                  stor.placesetiquettes[0].corrige(false)
                  stor.placesetiquettes[1].corrige(false)
                }
              }
              // test l2
              if (stor.l2d1 === stor.l2d2) {
                if (stor.l2p[1] === 'perpendiculaire') {
                  stor.bull[stor.bull.length] = new BulleAide(stor.l2td, ds.textes.ermeme[0].replace('£a', stor.l2d1), { place: 0 })
                } else {
                  stor.bull[stor.bull.length] = new BulleAide(stor.l2td, ds.textes.ermeme[1].replace('£a', stor.l2d1), { place: 0 })
                }
                if (stor.phrase === 0) {
                  stor.placesetiquettes[1].corrige(false)
                  stor.placesetiquettes[2].corrige(false)
                  stor.placesetiquettes[5].corrige(false)
                } else {
                  stor.placesetiquettes[2].corrige(false)
                  stor.placesetiquettes[3].corrige(false)
                  stor.placesetiquettes[7].corrige(false)
                }
              }
              if (
                ((stor.l2d1 === stor.droite1) && (stor.l2d2 === stor.droite2) && (stor.l2p[0] !== ds.textes.prop[stor.proprieteencours][0])) ||
                ((stor.l2d1 === stor.droite2) && (stor.l2d2 === stor.droite1) && (stor.l2p[0] !== ds.textes.prop[stor.proprieteencours][0])) ||
                ((stor.l2d1 === stor.droite2) && (stor.l2d2 === stor.droite3) && (stor.l2p[0] !== ds.textes.prop[stor.proprieteencours][1])) ||
                ((stor.l2d1 === stor.droite3) && (stor.l2d2 === stor.droite2) && (stor.l2p[0] !== ds.textes.prop[stor.proprieteencours][1]))) {
                stor.bull[stor.bull.length] = new BulleAide(stor.l2td, ds.textes.erfaux.replace('£a', stor.l2d1).replace('£b', stor.l2d2).replace('£c', stor.l2p[0]), { place: 0 })
                if (stor.phrase === 0) {
                  stor.placesetiquettes[1].corrige(false)
                  stor.placesetiquettes[2].corrige(false)
                  stor.placesetiquettes[5].corrige(false)
                } else {
                  stor.placesetiquettes[2].corrige(false)
                  stor.placesetiquettes[3].corrige(false)
                  stor.placesetiquettes[7].corrige(false)
                }
              }
              if (
                ((stor.l2d1 === stor.droite1) && (stor.l2d2 === stor.droite3)) ||
                ((stor.l2d1 === stor.droite3) && (stor.l2d2 === stor.droite1))) {
                stor.bull[stor.bull.length] = new BulleAide(stor.l2td, ds.textes.ersaispas.replace('£a', stor.l2d1).replace('£b', stor.l2d2).replace('£c', stor.l2p[0]), { place: 0 })
                if (stor.phrase === 0) {
                  stor.placesetiquettes[1].corrige(false)
                  stor.placesetiquettes[2].corrige(false)
                  stor.placesetiquettes[5].corrige(false)
                } else {
                  stor.placesetiquettes[2].corrige(false)
                  stor.placesetiquettes[3].corrige(false)
                  stor.placesetiquettes[7].corrige(false)
                }
              }
              // test l3
              if (stor.l3d1 === stor.l3d2) {
                stor.bull[stor.bull.length] = new BulleAide(stor.l3td, 'Tu écris deux fois la droite ' + stor.l3d1, { place: 0 })
                if (stor.phrase === 0) {
                  stor.placesetiquettes[3].corrige(false)
                  stor.placesetiquettes[4].corrige(false)
                  stor.placesetiquettes[6].corrige(false)
                } else {
                  stor.placesetiquettes[4].corrige(false)
                  stor.placesetiquettes[5].corrige(false)
                  stor.placesetiquettes[8].corrige(false)
                }
              }
              if (
                ((stor.l3d1 === stor.droite1) && (stor.l3d2 === stor.droite2)) ||
                ((stor.l3d1 === stor.droite2) && (stor.l3d2 === stor.droite1)) ||
                ((stor.l3d1 === stor.droite2) && (stor.l3d2 === stor.droite3)) ||
                ((stor.l3d1 === stor.droite3) && (stor.l3d2 === stor.droite2))) {
                stor.bull[stor.bull.length] = new BulleAide(stor.l3td, ds.textes.ersaitdeja.replace('£a', stor.l3d1).replace('£b', stor.l3d2), { place: 0 })
                if (stor.phrase === 0) {
                  stor.placesetiquettes[3].corrige(false)
                  stor.placesetiquettes[4].corrige(false)
                  stor.placesetiquettes[6].corrige(false)
                } else {
                  stor.placesetiquettes[4].corrige(false)
                  stor.placesetiquettes[5].corrige(false)
                  stor.placesetiquettes[8].corrige(false)
                }
              }
              if (
                ((stor.l3d1 === stor.droite1) && (stor.l3d2 === stor.droite3) && (stor.l3p[0] !== ds.textes.prop[stor.proprieteencours][2])) ||
                ((stor.l3d1 === stor.droite3) && (stor.l3d2 === stor.droite1) && (stor.l3p[0] !== ds.textes.prop[stor.proprieteencours][2]))) {
                stor.bull[stor.bull.length] = new BulleAide(stor.l3td, ds.textes.erconcl, { place: 0 })
                if (stor.phrase === 0) {
                  stor.placesetiquettes[3].corrige(false)
                  stor.placesetiquettes[4].corrige(false)
                  stor.placesetiquettes[6].corrige(false)
                } else {
                  stor.placesetiquettes[4].corrige(false)
                  stor.placesetiquettes[5].corrige(false)
                  stor.placesetiquettes[8].corrige(false)
                }
              }
              if (stor.phrase === 1) {
                if ((!stor.okcoherencedroite) || (!stor.okcoherencepos)) {
                  stor.bull[stor.bull.length] = new BulleAide(stor.l3td, ds.textes.ercohe, { place: 0 })
                }
                if (!stor.okcoherencepos) {
                  stor.placesetiquettes[7].corrige(false)
                  stor.placesetiquettes[8].corrige(false)
                }
              }

              stor.lesdiv.explications.innerHTML = ''
              if ((!stor.okdonnees1) || (!stor.okdonnees2)) {
                j3pAffiche(stor.lesdiv.explications, null, ds.textes.mankdonnees + '\n')
              }
              if (!stor.okconcl) {
                j3pAffiche(stor.lesdiv.explications, null, ds.textes.mankconcl)
              }
              stor.lesdiv.explications.classList.add('explique')
              me.typederreurs[1]++
              me.afficheBoutonValider()
              // indication éventuelle ici
            } else { // Erreur au nème essai
              me._stopTimer()
              me.cacheBoutonValider()
              stor.lesdiv.correction.innerHTML += '<br>' + regardeCorrection

              stor.lesdiv.AL1.innerHTML = ''
              stor.lesdiv.AL2.innerHTML = ''
              stor.lesdiv.AL3.innerHTML = ''

              stor.lesdiv.explications.innerHTML = ''
              if ((!stor.okdonnees1) || (!stor.okdonnees2)) {
                j3pAffiche(stor.lesdiv.explications, null, ds.textes.mankdonnees + '\n')
              }
              if (!stor.okconcl) {
                j3pAffiche(stor.lesdiv.explications, null, ds.textes.mankconcl)
              }

              stor.lesdiv.explications.classList.add('explique')
              stor.lesdiv.solution.classList.add('correction')

              maco = ds.textes.rep[0]
              if (stor.proprieteencours === 0) { maco = ds.textes.rep[stor.repprefer] }
              if (stor.proprieteencours === 2) { maco = ds.textes.rep[1] }
              j3pAffiche(stor.lesdiv.solution, null, maco,
                {
                  a: stor.droite1,
                  b: stor.droite2,
                  c: stor.droite3,
                  d: ds.textes.prop[stor.proprieteencours][0],
                  e: ds.textes.prop[stor.proprieteencours][2],
                  f: ds.textes.prop[stor.proprieteencours][1].substring(0, ds.textes.prop[stor.proprieteencours][1].length - 1)
                })

              for (let i = stor.etiquettes.length - 1; i > -1; i--) {
                stor.etiquettes[i].disable()
                stor.etiquettes.pop()
              }
              j3pEmpty(stor.etikAef)
              j3pStyle(stor.etikAef, { border: '0px' })
              if (!ds.FormulationImposee) {
                j3pEmpty(stor.lesdiv.Bouton)
              }

              for (let i = stor.placesetiquettes.length - 1; i > -1; i--) {
                stor.placesetiquettes[i].corrige(true)
              }

              // test l1
              if (stor.l1d1 === stor.l1d2) {
                if (stor.l1p[1] === 'perpendiculaire') {
                  stor.bull[stor.bull.length] = new BulleAide(stor.lesdiv.AL1, ds.textes.ermeme[0].replace('£a', stor.l1d1), { place: 0 })
                } else {
                  stor.bull[stor.bull.length] = new BulleAide(stor.lesdiv.AL1, ds.textes.ermeme[1].replace('£a', stor.l1d1), { place: 0 })
                }
                if (stor.phrase === 0) {
                  stor.placesetiquettes[5].corrige(false, true)
                  stor.placesetiquettes[0].corrige(false, true)
                  stor.placesetiquettes[2].corrige(false, true)
                } else {
                  stor.placesetiquettes[6].corrige(false, true)
                  stor.placesetiquettes[0].corrige(false, true)
                  stor.placesetiquettes[1].corrige(false, true)
                }
              }
              if (
                ((stor.l1d1 === stor.droite1) && (stor.l1d2 === stor.droite2) && (stor.l1p[0] !== ds.textes.prop[stor.proprieteencours][0])) ||
                ((stor.l1d1 === stor.droite2) && (stor.l1d2 === stor.droite1) && (stor.l1p[0] !== ds.textes.prop[stor.proprieteencours][0])) ||
                ((stor.l1d1 === stor.droite2) && (stor.l1d2 === stor.droite3) && (stor.l1p[0] !== ds.textes.prop[stor.proprieteencours][1])) ||
                ((stor.l1d1 === stor.droite3) && (stor.l1d2 === stor.droite2) && (stor.l1p[0] !== ds.textes.prop[stor.proprieteencours][1]))
              ) {
                const content = ds.textes.erfaux
                  .replace('£a', stor.l1d1)
                  .replace('£b', stor.l1d2)
                  .replace('£c', stor.l1p[0])
                stor.bull[stor.bull.length] = new BulleAide(stor.lesdiv.AL1, content, { place: 0 })
                if (stor.phrase === 0) {
                  stor.placesetiquettes[5].corrige(false, true)
                  stor.placesetiquettes[0].corrige(false, true)
                  stor.placesetiquettes[2].corrige(false, true)
                } else {
                  stor.placesetiquettes[6].corrige(false)
                  stor.placesetiquettes[0].corrige(false, true)
                  stor.placesetiquettes[1].corrige(false, true)
                }
              }

              if (
                ((stor.l1d1 === stor.droite1) && (stor.l1d2 === stor.droite3)) ||
                ((stor.l1d1 === stor.droite3) && (stor.l1d2 === stor.droite1))
              ) {
                const content = ds.textes.ersaispas
                  .replace('£a', stor.l1d1)
                  .replace('£b', stor.l1d2)
                  .replace('£c', stor.l1p[0])
                stor.bull[stor.bull.length] = new BulleAide(stor.lesdiv.AL1, content, { place: 0 })
                if (stor.phrase === 0) {
                  stor.placesetiquettes[5].corrige(false, true)
                  stor.placesetiquettes[0].corrige(false, true)
                  stor.placesetiquettes[2].corrige(false, true)
                } else {
                  stor.placesetiquettes[6].corrige(false, true)
                  stor.placesetiquettes[0].corrige(false, true)
                  stor.placesetiquettes[1].corrige(false, true)
                }
              }
              // test l2
              if (stor.l2d1 === stor.l2d2) {
                if (stor.l2p[1] === 'perpendiculaire') {
                  stor.bull[stor.bull.length] = new BulleAide(stor.l2td, ds.textes.ermeme[0].replace('£a', stor.l2d1), { place: 0 })
                } else {
                  stor.bull[stor.bull.length] = new BulleAide(stor.l2td, ds.textes.ermeme[1].replace('£a', stor.l2d1), { place: 0 })
                }
                if (stor.phrase === 0) {
                  stor.placesetiquettes[1].corrige(false, true)
                  stor.placesetiquettes[2].corrige(false, true)
                  stor.placesetiquettes[5].corrige(false, true)
                } else {
                  stor.placesetiquettes[2].corrige(false, true)
                  stor.placesetiquettes[3].corrige(false, true)
                  stor.placesetiquettes[7].corrige(false, true)
                }
              }
              if (
                ((stor.l2d1 === stor.droite1) && (stor.l2d2 === stor.droite2) && (stor.l2p[0] !== ds.textes.prop[stor.proprieteencours][0])) ||
                ((stor.l2d1 === stor.droite2) && (stor.l2d2 === stor.droite1) && (stor.l2p[0] !== ds.textes.prop[stor.proprieteencours][0])) ||
                ((stor.l2d1 === stor.droite2) && (stor.l2d2 === stor.droite3) && (stor.l2p[0] !== ds.textes.prop[stor.proprieteencours][1])) ||
                ((stor.l2d1 === stor.droite3) && (stor.l2d2 === stor.droite2) && (stor.l2p[0] !== ds.textes.prop[stor.proprieteencours][1]))) {
                stor.bull[stor.bull.length] = new BulleAide(stor.l2td, ds.textes.erfaux.replace('£a', stor.l2d1).replace('£b', stor.l2d2).replace('£c', stor.l2p[0]), { place: 0 })
                if (stor.phrase === 0) {
                  stor.placesetiquettes[1].corrige(false, true)
                  stor.placesetiquettes[2].corrige(false, true)
                  stor.placesetiquettes[5].corrige(false, true)
                } else {
                  stor.placesetiquettes[2].corrige(false, true)
                  stor.placesetiquettes[3].corrige(false, true)
                  stor.placesetiquettes[7].corrige(false, true)
                }
              }
              if (
                ((stor.l2d1 === stor.droite1) && (stor.l2d2 === stor.droite3)) ||
                ((stor.l2d1 === stor.droite3) && (stor.l2d2 === stor.droite1))) {
                stor.bull[stor.bull.length] = new BulleAide(stor.l2td, ds.textes.ersaispas.replace('£a', stor.l2d1).replace('£b', stor.l2d2).replace('£c', stor.l2p[0]), { place: 0 })
                if (stor.phrase === 0) {
                  stor.placesetiquettes[1].corrige(false, true)
                  stor.placesetiquettes[2].corrige(false, true)
                  stor.placesetiquettes[5].corrige(false, true)
                } else {
                  stor.placesetiquettes[2].corrige(false, true)
                  stor.placesetiquettes[3].corrige(false, true)
                  stor.placesetiquettes[7].corrige(false, true)
                }
              }
              // test l3
              if (stor.l3d1 === stor.l3d2) {
                stor.bull[stor.bull.length] = new BulleAide(stor.l3td, 'Tu écris deux fois la droite ' + stor.l3d1, { place: 0 })
                if (stor.phrase === 0) {
                  stor.placesetiquettes[3].corrige(false, true)
                  stor.placesetiquettes[4].corrige(false, true)
                  stor.placesetiquettes[6].corrige(false, true)
                } else {
                  stor.placesetiquettes[4].corrige(false, true)
                  stor.placesetiquettes[5].corrige(false, true)
                  stor.placesetiquettes[8].corrige(false, true)
                }
              }
              if (
                ((stor.l3d1 === stor.droite1) && (stor.l3d2 === stor.droite2)) ||
                ((stor.l3d1 === stor.droite2) && (stor.l3d2 === stor.droite1)) ||
                ((stor.l3d1 === stor.droite2) && (stor.l3d2 === stor.droite3)) ||
                ((stor.l3d1 === stor.droite3) && (stor.l3d2 === stor.droite2))) {
                stor.bull[stor.bull.length] = new BulleAide(stor.l3td, ds.textes.ersaitdeja.replace('£a', stor.l3d1).replace('£b', stor.l3d2), { place: 0 })
                if (stor.phrase === 0) {
                  stor.placesetiquettes[3].corrige(false, true)
                  stor.placesetiquettes[4].corrige(false, true)
                  stor.placesetiquettes[6].corrige(false, true)
                } else {
                  stor.placesetiquettes[4].corrige(false, true)
                  stor.placesetiquettes[5].corrige(false, true)
                  stor.placesetiquettes[8].corrige(false, true)
                }
              }
              if (
                ((stor.l3d1 === stor.droite1) && (stor.l3d2 === stor.droite3) && (stor.l3p[0] !== ds.textes.prop[stor.proprieteencours][2])) ||
                ((stor.l3d1 === stor.droite3) && (stor.l3d2 === stor.droite1) && (stor.l3p[0] !== ds.textes.prop[stor.proprieteencours][2]))) {
                stor.bull[stor.bull.length] = new BulleAide(stor.l3td, ds.textes.erconcl, { place: 0 })
                if (stor.phrase === 0) {
                  stor.placesetiquettes[3].corrige(false, true)
                  stor.placesetiquettes[4].corrige(false, true)
                  stor.placesetiquettes[6].corrige(false, true)
                } else {
                  stor.placesetiquettes[4].corrige(false, true)
                  stor.placesetiquettes[5].corrige(false, true)
                  stor.placesetiquettes[8].corrige(false, true)
                }
              }
              if (stor.phrase === 1) {
                if ((!stor.okcoherencedroite) || (!stor.okcoherencepos)) {
                  stor.bull[stor.bull.length] = new BulleAide(stor.l3td, ds.textes.ercohe, { place: 0 })
                }
                if (!stor.okcoherencepos) {
                  stor.placesetiquettes[7].corrige(false, true)
                  stor.placesetiquettes[8].corrige(false, true)
                }
              }

              for (let i = stor.placesetiquettes.length - 1; i > -1; i--) {
                stor.placesetiquettes[i].disable()
                stor.placesetiquettes.pop()
              }

              me.typederreurs[2]++
              me.etat = 'navigation'
              me.sectionCourante()
            }
          }
        }
      }
      // Obligatoire
      me.finCorrection()
      break // case "correction":

    case 'navigation':

      if (me.sectionTerminee()) {
        // On détermine la phrase d’état renvoyée par la section
        me.parcours.pe = (ds.nbetapes * me.score) / ds.nbitems
        me.afficheBoutonSectionSuivante()
        me.focus('sectioncontinuer')
      } else {
        me.etat = 'enonce'
        me.afficheBoutonSuite()
        me.focus('boutoncontinuer')
      }
      // Obligatoire
      me.finNavigation()

      break // case "navigation":
  }
}
