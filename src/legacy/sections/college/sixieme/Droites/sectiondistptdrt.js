import { j3pAddElt, j3pAjouteBouton, j3pArrondi, j3pGetNewId, j3pGetRandomBool, j3pGetRandomInt, j3pShowError, j3pShuffle } from 'src/legacy/core/functions'
import { j3pCreeFenetres, j3pAfficheCroixFenetres, j3pToggleFenetres } from 'src/legacy/core/functionsJqDialog'
import { j3pCreeSVG } from 'src/legacy/core/functionsSvg'
import ZoneStyleMathquill1 from 'src/legacy/outils/zoneStyleMathquill/ZoneStyleMathquill1'
import { addDefaultTable, addTable, getCells } from 'src/legacy/themes/table'
import { j3pAffiche } from 'src/lib/mathquill/functions'
import { getMtgCore } from 'src/lib/outils/mathgraph'
import { addMaj } from 'src/lib/utils/string'
import textesGeneriques from 'src/lib/core/textes'

const { cBien, cFaux, essaieEncore, reponseIncomplete, tempsDepasse } = textesGeneriques

/**
 * Ajouter une description ici
 * @fileOverview
 * @author Tommy Barroy <t.barroy@laposte.net>
 * @since 2017-10
 */

export const params = {
  outils: ['mathquill', 'calculatrice'],
  /**
   * Ce qui est paramétrable dans l’éditeur de graphe
   * @type {LegacySectionParamsParametres}
   */
  parametres: [
    ['nbrepetitions', 9, 'entier', 'Nombre de répétitions de la section'],
    ['nbchances', 2, 'entier', 'Nombre d’essais possibles.'],
    ['limite', 0, 'entier', 'Temps disponible pour répondre.'],
    ['NbDroite', 1, 'entier', 'Nombre de droites (entre 1 et 3).'],
    // le modèle gère la compatibilité ascendante avec l’ancien paramètre MiseEnPagePlusClaire
    ['theme', 'standard', 'liste', 'Modèle pour l’affichage', ['standard', 'zonesAvecImageDeFond']]
  ]
}

/**
 * section distptdrt
 * @this {Parcours}
 */
export default function main () {
  const me = this
  const stor = this.storage
  let ds = me.donneesSection
  let svgId = stor.svgId

  function creeLesDiv () {
    stor.lesdiv = {}
    stor.lesdiv.conteneur = j3pAddElt(me.zonesElts.MG, 'div', {
      style: me.styles.etendre('toutpetit.enonce', { padding: '10px' })
    })
    const tt = addDefaultTable(stor.lesdiv.conteneur, 1, 3)
    tt[0][1].style.width = '10px'
    const tbEnonce = addTable(tt[0][0], { className: 'tbDefault', nbLignes: 3, nbColonnes: 1 })
    tbEnonce.classList.add('enonce')
    const cells = getCells(tbEnonce)
    stor.lesdiv.consigneG = cells[0][0]
    stor.lesdiv.consigneG.style.textAlign = 'center'
    const cellsFigure = addDefaultTable(cells[1][0], 1, 3)
    stor.lesdiv.figure = cellsFigure[0][1]
    cellsFigure[0][0].style.width = '10px'
    cellsFigure[0][2].style.width = '10px'
    stor.lesdiv.donnees = cells[2][0]
    stor.lesdiv.donnees.style.textAlign = 'center'
    const tty = addDefaultTable(tt[0][2], 3, 1)
    stor.lesdiv.travail = tty[0][0]
    stor.lesdiv.explications = tty[1][0]
    stor.lesdiv.solution = tty[2][0]
    stor.lesdiv.calculatrice = j3pAddElt(me.zonesElts.MD, 'div', {
      style: me.styles.etendre('toutpetit.enonce', { padding: '10px' })
    })
    stor.lesdiv.correction = j3pAddElt(me.zonesElts.MD, 'div', {
      style: me.styles.etendre('toutpetit.enonce', { padding: '10px' })
    })
    stor.lesdiv.solution.style.color = me.styles.toutpetit.correction.color
    stor.lesdiv.explications.style.color = me.styles.cfaux
  }
  function getDonnees () {
    return {

      // Nombre de répétitions de l’exercice
      nbrepetitions: 9,

      NbDroite: 1,

      // Si indication != "" alors un lien "Indication" s’affiche dans la zone Inférieure Gauche (zones.IG
      // une indication<br>de deux lignes..."
      indication: '',

      // pour le cas de presentation1bis
      // En effet pas d’indication possible pour cette présentation
      // surchargeindication: false,

      // Nombre de chances dont dispose l’él§ve pour répondre à la question
      nbchances: 2,

      Choix_Phrase: true,
      //  || "presentation2" || "presentation3"  || "presentation1bis"
      structure: 'presentation1bis',

      // Paramètres de la section, avec leurs valeurs par défaut.

      // Par défaut, pas de temps limite. Décommentez pour avoir un temps limite par défaut. En secondes.

      // limite: 10,
      /*
        Phrase d’état renvoyée par la section
        Si pe=0 alors la section est quantitative et renvoie un score entre 0 et 1.
        Dans le cas où la section est qualitative, lister les différents pe renvoyées :
            pe_1: "toto"
            pe_2 = "tata"
            etc
        Dans la partie navigation, il faudra affecter la variable parcours.pe à l’une des précédentes.
        Par exemple,
        parcours.pe = ds.pe_2
        */
      pe_1: 'Egalite',
      pe_2: 'Ecriture',
      pe_3: 'Produit_en_croix',
      pe_4: 'Simplification'
    }
  }
  function initSection () {
    me.donneesSection = getDonnees()
    ds = me.donneesSection
    // On surcharge avec les parametres passés par le graphe
    me.surcharge()
    if (ds.NbDroite < 1 || ds.NbDroite > 3) ds.NbDroite = 1
    // Construction de la page
    me.construitStructurePage({ structure: ds.structure, ratioGauche: 0.75 })

    me.typederreurs = [0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0]

    me.score = 0

    me.afficheTitre('Distance d’un point à une droite')

    // chargement mtg
    getMtgCore({ withMathJax: true })
      .then(
        // success
        (mtgAppLecteur) => {
          stor.mtgAppLecteur = mtgAppLecteur
          enonceMain()
        },
        // failure
        (error) => {
          j3pShowError(error, { message: 'mathgraph n’est pas correctement chargé', mustNotify: true })
        })
      // plantage dans le code de success
      .catch(j3pShowError)
  }

  function faiFigure () {
    stor.lettres = []
    for (let i = 0; i < 8; i++) {
      addMaj(stor.lettres)
    }
    stor.nompoint = stor.lettres[7]

    stor.svgId = j3pGetNewId('mtg32')
    svgId = stor.svgId
    const txtFigure = 'TWF0aEdyYXBoSmF2YTEuMAAAABM+TMzNAAJmcv###wEA#wEAAAAAAAAAAANLAAACfwAAAQEAAAAAAAAAAQAAATH#####AAAAAQAKQ0NhbGNDb25zdAD#####AAJwaQAWMy4xNDE1OTI2NTM1ODk3OTMyMzg0Nv####8AAAABAApDQ29uc3RhbnRlQAkh+1RELRj#####AAAAAQAHQ0NhbGN1bAD#####AAJBNgADMC43AAAAAT#mZmZmZmZm#####wAAAAEACkNQb2ludEJhc2UA#####wEAAAAADgABVQDAJAAAAAAAAEAQAAAAAAAAAAAFAABAL#52yLQ5WEAv#nbItDlY#####wAAAAEAFENEcm9pdGVEaXJlY3Rpb25GaXhlAP####8BAAAAABAAAAEAAAABAAAAAgE#8AAAAAAAAP####8AAAABAA9DUG9pbnRMaWVEcm9pdGUA#####wEAAAAADgABVgDAAAAAAAAAAEAQAAAAAAAAAAAFAAFAP#52yLQ5WAAAAAP#####AAAAAQAIQ1NlZ21lbnQA#####wEAAAAAEAAAAQAAAAEAAAACAAAABP####8AAAABAAdDTWlsaWV1AP####8BAAAAABAAAAAAAAAAAAAAAEAIAAAAAAAAAAAFAAAAAAIAAAAE#####wAAAAIADENDb21tZW50YWlyZQD#####AQAAAAAAAAAAAAAAAEAYAAAAAAAAAAAAAAAGDAAAAAAAAQAAAAAAAAABAAAAAAAAAAAAATH#####AAAAAQAJQ0xvbmd1ZXVyAP####8AAAACAAAABAAAAAMA#####wEAAAAAEAAAAAAAAAAAAAAAQAgAAAAAAAAAAAUAAUB18AAAAAAAQHYeFHrhR64AAAAEAP####8BAAAAARAAAAEAAAABAAAACQA#8AAAAAAAAAAAAAQA#####wEAAAABEAAAAQAAAAEAAAAJAT#wAAAAAAAAAAAAAwD#####AX8AfwEQAAFBAAAAAAAAAAAAQAgAAAAAAAAAAAkAAEBkYAAAAAAAQGV8KPXCj1wAAAADAP####8BAAAAABAAAAAAAAAAAAAAAEAIAAAAAAAAAAAFAAFAYsAAAAAAAEBXeFHrhR64#####wAAAAEACUNDZXJjbGVPQQD#####AQAAAAAAAAEAAAAMAAAADQAAAAMA#####wAAAAAAEAAAAEAIAAAAAAAAAAAAAAAAAAAAAAUAAEBPAAAAAAABQH5OFHrhR64AAAACAP####8ABW1pbmkxAAExAAAAAT#wAAAAAAAAAAAAAgD#####AAVtYXhpMQADMzYwAAAAAUB2gAAAAAAA#####wAAAAEAFENJbXBsZW1lbnRhdGlvblByb3RvAP####8AB0N1cnNldXIAAAAFAAAABQAAAAMAAAAQAAAAEQAAAA8AAAAEAAAAABIBAAAAABAAAAEAAAABAAAADwE#8AAAAAAAAAAAAAUBAAAAEgAAAAAAEAAAAMAIAAAAAAAAP#AAAAAAAAAAAAUAAEBdQAAAAAAAAAAAE#####8AAAABAAtDSG9tb3RoZXRpZQAAAAASAAAAD#####8AAAABAApDT3BlcmF0aW9uA#####8AAAABAA9DUmVzdWx0YXRWYWxldXIAAAAQAAAADQEAAAAOAAAAEAAAAA4AAAAR#####wAAAAEAC0NQb2ludEltYWdlAAAAABIBAAAAAA0AAk8xAMAQAAAAAAAAQBAAAAAAAAAAAAUAAAAAFAAAABUAAAAMAAAAABIAAAAPAAAADQMAAAANAQAAAAE#8AAAAAAAAAAAAA4AAAAQAAAADQEAAAAOAAAAEQAAAA4AAAAQAAAADwAAAAASAQAAAAANAAJJMQDAAAAAAAAAAEAIAAAAAAAAAAAFAAAAABQAAAAXAAAABgEAAAASAAAAAAAQAAABAAAAAQAAAA8AAAAUAAAABQEAAAASAAAAAAEQAAJrMQDAAAAAAAAAAEAAAAAAAAAAAAABAAE#3hXhXhXhXQAAABn#####AAAAAgAPQ01lc3VyZUFic2Npc3NlAQAAABIAAmExAAAAFgAAABgAAAAa#####wAAAAEAD0NWYWxldXJBZmZpY2hlZQEAAAASAAAAAAAAAAAAAAAAAMAYAAAAAAAAAAAAAAAaDwAB####AAAAAQAAAAIAAAABAAAAAAAAAAAAAAAAAgAAABsAAAADAP####8AAAAAABAAAABACAAAAAAAAAAAAAAAAAAAAAAFAABATgAAAAAAAUCAlwo9cKPXAAAAAgD#####AAVtaW5pMgABMAAAAAEAAAAAAAAAAAAAAAIA#####wAFbWF4aTIAAjEwAAAAAUAkAAAAAAAAAAAACwD#####AAdDdXJzZXVyAAAABQAAAAUAAAADAAAAHgAAAB8AAAAdAAAABAAAAAAgAQAAAAAQAAABAAAAAQAAAB0BP#AAAAAAAAAAAAAFAQAAACAAAAAAABAAAADACAAAAAAAAD#wAAAAAAAAAAAFAABAXUAAAAAAAAAAACEAAAAMAAAAACAAAAAdAAAADQMAAAAOAAAAHgAAAA0BAAAADgAAAB4AAAAOAAAAHwAAAA8AAAAAIAEAAAAADQACTzEAwBAAAAAAAABAEAAAAAAAAAAABQAAAAAiAAAAIwAAAAwAAAAAIAAAAB0AAAANAwAAAA0BAAAAAT#wAAAAAAAAAAAADgAAAB4AAAANAQAAAA4AAAAfAAAADgAAAB4AAAAPAAAAACABAAAAAA0AAkkxAMAAAAAAAAAAQAgAAAAAAAAAAAUAAAAAIgAAACUAAAAGAQAAACAAAAAAABAAAAEAAAABAAAAHQAAACIAAAAFAQAAACAAAAAAARAAAWsAwAAAAAAAAABAAAAAAAAAAAAAAQABAAAAAAAAAAAAAAAnAAAAEAEAAAAgAAJhMgAAACQAAAAmAAAAKAAAABEBAAAAIAAAAAAAAAAAAAAAAADAGAAAAAAAAAAAAAAAKA8AAf###wAAAAEAAAACAAAAAQAAAAAAAAAAAAAAAAIAAAApAAAAAwD#####AAAAAAAQAAAAQAgAAAAAAAAAAAAAAAAAAAAABQAAQFAAAAAAAABAgfcKPXCj1wAAAAIA#####wAFbWluaTMAATAAAAABAAAAAAAAAAAAAAACAP####8ABW1heGkzAAIxMAAAAAFAJAAAAAAAAAAAAAsA#####wAHQ3Vyc2V1cgAAAAUAAAAFAAAAAwAAACwAAAAtAAAAKwAAAAQAAAAALgEAAAAAEAAAAQAAAAEAAAArAT#wAAAAAAAAAAAABQEAAAAuAAAAAAAQAAAAwAgAAAAAAAA#8AAAAAAAAAAABQAAQF1AAAAAAAAAAAAvAAAADAAAAAAuAAAAKwAAAA0DAAAADgAAACwAAAANAQAAAA4AAAAsAAAADgAAAC0AAAAPAAAAAC4BAAAAAA0AAk8xAMAQAAAAAAAAQBAAAAAAAAAAAAUAAAAAMAAAADEAAAAMAAAAAC4AAAArAAAADQMAAAANAQAAAAE#8AAAAAAAAAAAAA4AAAAsAAAADQEAAAAOAAAALQAAAA4AAAAsAAAADwAAAAAuAQAAAAANAAJJMQDAAAAAAAAAAEAIAAAAAAAAAAAFAAAAADAAAAAzAAAABgEAAAAuAAAAAAAQAAABAAAAAQAAACsAAAAwAAAABQEAAAAuAAAAAAEQAAJrMgDAAAAAAAAAAEAAAAAAAAAAAAABAAE#8AAAAAAAAAAAADUAAAAQAQAAAC4AAmEzAAAAMgAAADQAAAA2AAAAEQEAAAAuAAAAAAAAAAAAAAAAAMAYAAAAAAAAAAAAAAA2DwAB####AAAAAQAAAAIAAAABAAAAAAAAAAAAAAAAAgAAADcAAAADAP####8AAAAAABAAAABACAAAAAAAAAAAAAAAAAAAAAAFAABAdgAAAAAAAEB+XhR64UeuAAAAAgD#####AAVtaW5pNAABMAAAAAEAAAAAAAAAAAAAAAIA#####wAFbWF4aTQAAjEwAAAAAUAkAAAAAAAAAAAACwD#####AAdDdXJzZXVyAAAABQAAAAUAAAADAAAAOgAAADsAAAA5AAAABAAAAAA8AQAAAAAQAAABAAAAAQAAADkBP#AAAAAAAAAAAAAFAQAAADwAAAAAABAAAADACAAAAAAAAD#wAAAAAAAAAAAFAABAXUAAAAAAAAAAAD0AAAAMAAAAADwAAAA5AAAADQMAAAAOAAAAOgAAAA0BAAAADgAAADoAAAAOAAAAOwAAAA8AAAAAPAEAAAAADQACTzEAwBAAAAAAAABAEAAAAAAAAAAABQAAAAA+AAAAPwAAAAwAAAAAPAAAADkAAAANAwAAAA0BAAAAAT#wAAAAAAAAAAAADgAAADoAAAANAQAAAA4AAAA7AAAADgAAADoAAAAPAAAAADwBAAAAAA0AAkkxAMAAAAAAAAAAQAgAAAAAAAAAAAUAAAAAPgAAAEEAAAAGAQAAADwAAAAAABAAAAEAAAABAAAAOQAAAD4AAAAFAQAAADwAAAAAARAAAmszAMAAAAAAAAAAQAAAAAAAAAAAAAEAAT#UyUyUyUyVAAAAQwAAABABAAAAPAACYTQAAABAAAAAQgAAAEQAAAARAQAAADwAAAAAAAAAAAAAAAAAwBgAAAAAAAAAAAAAAEQPAAH###8AAAABAAAAAgAAAAEAAAAAAAAAAAAAAAACAAAARQAAAAIA#####wACQTEAAmExAAAADgAAABsAAAACAP####8AAkEyAAJhMgAAAA4AAAApAAAAAgD#####AAJBMwACYTMAAAAOAAAANwAAAAIA#####wACQTQAAmE0AAAADgAAAEX#####AAAAAQAJQ1JvdGF0aW9uAP####8AAAAMAAAADgAAAEcAAAAPAP####8BAAAAABAAAAAAAAAAAAAAAEAIAAAAAAAAAAAFAAAAAA0AAABLAAAAEgD#####AAAADAAAAAFAVoAAAAAAAAAAAA8A#####wEAAP8AEAAAAAAAAAAAAAAAQAgAAAAAAAAAAAUAAAAATAAAAE0AAAAPAP####8BAAD#ABAAAAAAAAAAAAAAAEAIAAAAAAAAAAAFAAAAAE4AAABN#####wAAAAIACUNDZXJjbGVPUgD#####AQAA#wAAAAEAAABOAAAAAT#gAAAAAAAAAAAAABMA#####wEAAP8AAAABAAAATwAAAA4AAAABAAAAABMA#####wEAAP8AAAABAAAATAAAAA4AAAABAP####8AAAABABBDSW50Q2VyY2xlQ2VyY2xlAP####8AAABRAAAADv####8AAAABABBDUG9pbnRMaWVCaXBvaW50AP####8BAAD#ABAAAAAAAAAAAAAAAEAIAAAAAAAAAAAFAAIAAABTAAAAFQD#####AQAA#wAQAAAAAAAAAAAAAABACAAAAAAAAAAABQABAAAAUwAAABQA#####wAAAFAAAAAOAAAAFQD#####AQAA#wAQAAAAAAAAAAAAAABACAAAAAAAAAAABQACAAAAVgAAABUA#####wEAAP8AEAAAAAAAAAAAAAAAQAgAAAAAAAAAAAUAAQAAAFYAAAAUAP####8AAABSAAAADgAAABUA#####wEAAP8AEAAAAAAAAAAAAAAAQAgAAAAAAAAAAAUAAgAAAFkAAAAVAP####8BAAD#ABAAAAAAAAAAAAAAAEAIAAAAAAAAAAAFAAEAAABZAAAADAD#####AAAAWgAAAA0DAAAADgAAAEgAAAABQCQAAAAAAAAAAAAPAP####8BAAD#ARAAAUMAAAAAAAAAAABACAAAAAAAAAABQgkAAAAAWwAAAFwAAAAMAP####8AAABXAAAADQMAAAAOAAAASQAAAAFAJAAAAAAAAAAAAA8A#####wEAAP8BEAABRQAAAAAAAAAAAEAIAAAAAAAAAAFECQAAAABYAAAAXgAAAAwA#####wAAAFQAAAANAwAAAA4AAABKAAAAAUAkAAAAAAAAAAAADwD#####AQAA#wEQAAFHAAAAAAAAAAAAQAgAAAAAAAAAAUYJAAAAAFUAAABgAAAABgD#####AQAAAAAQAAABAAJzRAEBAAAADAAAAF8AAAAGAP####8BAAAAABAAAAEAAnNCAQEAAAAMAAAAXQAAAAYA#####wEAAAAAEAAAAQACc0YBAQAAAAwAAABh#####wAAAAEAFkNEcm9pdGVQZXJwZW5kaWN1bGFpcmUA#####wEAAAAAEAAAAQACZDIAAgAAAGEAAABkAAAAFgD#####AQAAAAAQAAABAAJkMwACAAAAXwAAAGIAAAAWAP####8BAAAAABAAAAEAAmQxAAIAAABdAAAAY#####8AAAABABBDSW50RHJvaXRlRHJvaXRlAP####8BAAD#ARAAAUYAAAAAAAAAAABACAAAAAAAAAABRQkAAAAAZQAAAGYAAAAXAP####8BAAD#ARAAAUQAAAAAAAAAAABACAAAAAAAAAABQwkAAAAAZwAAAGYAAAATAP####8BAAD#AAAAAQAAAF0AAAABQAAAAAAAAAAAAAAAEwD#####AQAA#wAAAAEAAABhAAAAAUAAAAAAAAAAAP####8AAAABABBDSW50RHJvaXRlQ2VyY2xlAP####8AAABlAAAAawAAABUA#####wEAAP8AEAAAAAAAAAAAAAAAQAgAAAAAAAAAAAkAAQAAAGwAAAAVAP####8BAAD#ARAAAUgAAAAAAAAAAABACAAAAAAAAAABSAkAAgAAAGwAAAAYAP####8AAABnAAAAagAAABUA#####wEAAP8AEAAAAAAAAAAAAAAAQAgAAAAAAAAAAAkAAgAAAG8AAAAVAP####8BAAD#ARAAAUIAAAAAAAAAAABACAAAAAAAAAABQQkAAQAAAG8AAAADAP####8BfwB#ABAAAAAAAAAAAAAAAEAIAAAAAAAAAAAJAAFAY8AAAAAAAEA64UeuFHriAAAACgD#####AX8AfwAAAAEAAAAMAAAAcgAAABgA#####wAAAGUAAABzAAAAFQD#####AQAAfwAQAAAAAAAAAAAAAABACAAAAAAAAAAACQABAAAAdAAAABUA#####wEAAH8AEAAAAAAAAAAAAAAAQAgAAAAAAAAAAAkAAgAAAHQAAAAYAP####8AAABnAAAAcwAAABUA#####wEAAH8AEAAAAAAAAAAAAAAAQAgAAAAAAAAAAAkAAgAAAHcAAAAVAP####8BAAB#ABAAAAAAAAAAAAAAAEAIAAAAAAAAAAAJAAEAAAB3AAAAEwD#####AQAAfwAAAAEAAAB2AAAAAT#jMzMzMzMzAAAAABMA#####wEAAH8AAAABAAAAeQAAAAE#4zMzMzMzMwAAAAAUAP####8AAABzAAAAegAAABUA#####wEAAH8AEAAAAAAAAAAAAAAAQAgAAAAAAAAAAAkAAgAAAHwAAAAVAP####8BAAB#ABAAAAAAAAAAAAAAAEAIAAAAAAAAAAAJAAEAAAB8AAAAFAD#####AAAAcwAAAHsAAAAVAP####8BAAB#ABAAAAAAAAAAAAAAAEAIAAAAAAAAAAAJAAEAAAB#AAAAFQD#####AQAAfwAQAAAAAAAAAAAAAABACAAAAAAAAAAACQACAAAAfwAAABMA#####wEAAH8AAAABAAAAaQAAAAE#8AAAAAAAAAAAAAAYAP####8AAABmAAAAggAAABUA#####wEAAH8AEAAAAAAAAAAAAAAAQAgAAAAAAAAAAAkAAQAAAIMAAAAVAP####8BAAB#ABAAAAAAAAAAAAAAAEAIAAAAAAAAAAAJAAIAAACDAAAACAD#####AQAAAAEAA25kMQAAAIAQAAAAAAABAAAAAQAAAAEAAAAAAAAAAAAEKGQxKQAAAAgA#####wEAAAABAANuZDIAAAB9EAAAAAAAAQAAAAEAAAABAAAAAAAAAAAABChkMikAAAATAP####8BAAAAAAAAAgAAAIQAAAABP+MzMzMzMzMAAAAAFgD#####AQAAAAAQAAABAAAAAgAAAIQAAABmAAAAGAD#####AAAAiQAAAIgAAAAVAP####8BAAAAABAAAAAAAAAAAAAAAEAIAAAAAAAAAAAJAAIAAACKAAAAFQD#####AQAAAAAQAAAAAAAAAAAAAABACAAAAAAAAAAACQABAAAAigAAAAgA#####wEAAAABAANuZDMAAACMEAAAAAAAAQAAAAEAAAABAAAAAAAAAAAABChkMykAAAAGAP####8BAAAAABAAAAEAAnNHAQEAAAAMAAAAbgAAAAYA#####wEAAAAAEAAAAQACc0UBAQAAAAwAAABoAAAABgD#####AQAAAAAQAAABAAJzQwEBAAAADAAAAGkAAAAGAP####8BAAAAABAAAAEAAnNBAQEAAAAMAAAAcQAAABMA#####wEAAAAAAAIBAAAAYQAAAAE#0zMzMzMzMwAAAAATAP####8BAAAAAAACAQAAAF8AAAABP9MzMzMzMzMAAAAAEwD#####AQAAAAAAAgEAAABdAAAAAT#TMzMzMzMzAAAAABgA#####wAAAGMAAACUAAAAFQD#####AAAAAAAQAAAAAAAAAAAAAABACAAAAAAAAAAACQABAAAAlQAAABUA#####wEAAAAAEAAAAAAAAAAAAAAAQAgAAAAAAAAAAAkAAgAAAJUAAAAYAP####8AAABnAAAAlAAAABUA#####wEAAAAAEAAAAAAAAAAAAAAAQAgAAAAAAAAAAAkAAQAAAJgAAAAVAP####8BAAAAABAAAAAAAAAAAAAAAEAIAAAAAAAAAAAJAAIAAACY#####wAAAAEACENWZWN0ZXVyAP####8BAAAAABAAAAEAAAIBAAAAXQAAAJcA#####wAAAAEADUNUcmFuc1BhclZlY3QA#####wAAAJsAAAAPAP####8BAAAAABAAAAAAAAAAAAAAAEAIAAAAAAAAAAAJAAAAAJkAAACcAAAABgD#####AX9#AAAQAAABAANzQjIAAgAAAJkAAACdAAAABgD#####AX9#AAAQAAABAANzQjMAAgAAAJ0AAACXAAAAGAD#####AAAAZQAAAJIAAAAVAP####8BAAAAABAAAAAAAAAAAAAAAEAIAAAAAAAAAAAJAAEAAACgAAAAFQD#####AQAAAAAQAAAAAAAAAAAAAABACAAAAAAAAAAACQACAAAAoAAAABgA#####wAAAGQAAACSAAAAFQD#####AAAAAAAQAAAAAAAAAAAAAABACAAAAAAAAAAACQABAAAAowAAABUA#####wEAAAAAEAAAAAAAAAAAAAAAQAgAAAAAAAAAAAkAAgAAAKMAAAAZAP####8BAAAAABAAAAEAAAEBAAAAYQAAAKUAAAAAGgD#####AAAApgAAAA8A#####wEAAAAAEAAAAAAAAAAAAAAAQAgAAAAAAAAAAAkAAAAAogAAAKcAAAAGAP####8Bf38AABAAAAEAA3NGMgACAAAApQAAAKgAAAAGAP####8Bf38AABAAAAEAA3NGMwACAAAAqAAAAKIAAAAYAP####8AAABmAAAAkwAAABUA#####wF#fwAAEAAAAAAAAAAAAAAAQAgAAAAAAAAAAAkAAgAAAKsAAAAVAP####8Bf38AABAAAAAAAAAAAAAAAEAIAAAAAAAAAAAJAAEAAACrAAAAGAD#####AAAAYgAAAJMAAAAVAP####8Af38AABAAAAAAAAAAAAAAAEAIAAAAAAAAAAAJAAEAAACuAAAAFQD#####AX9#AAAQAAAAAAAAAAAAAABACAAAAAAAAAAACQACAAAArgAAABkA#####wF#fwAAEAAAAQAAAAIAAABfAAAAsAAAAAAaAP####8AAACxAAAADwD#####AX9#AAAQAAAAAAAAAAAAAABACAAAAAAAAAAACQAAAACtAAAAsgAAAAYA#####wF#fwAAEAAAAQADc0QyAAIAAACtAAAAswAAAAYA#####wF#fwAAEAAAAQADc0QzAAIAAACzAAAAsAAAABYA#####wH#AAAAEAAAAQAAAAEAAABuAAAAZQAAABYA#####wH#AAAAEAAAAQAAAAEAAABhAAAAZQAAABYA#####wH#AAAAEAAAAQAAAAEAAABxAAAAZwAAABYA#####wH#AAAAEAAAAQAAAAEAAABdAAAAZwAAABYA#####wH#AAAAEAAAAQAAAAEAAABfAAAAZgAAABMA#####wH#AAAAAAABAAAAbgAAAAE#4AAAAAAAAAAAAAATAP####8B#wAAAAAAAQAAAGEAAAABP+AAAAAAAAAAAAAAEwD#####Af8AAAAAAAEAAABdAAAAAT#gAAAAAAAAAAAAABMA#####wH#AAAAAAABAAAAcQAAAAE#4AAAAAAAAAAAAAATAP####8B#wAAAAAAAQAAAGgAAAABP+AAAAAAAAAAAAAAEwD#####Af8AAAAAAAEAAABpAAAAAT#gAAAAAAAAAAAAABgA#####wAAAGUAAAC#AAAAFQD#####Af8AAAAQAAAAAAAAAAAAAABACAAAAAAAAAAABQACAAAAwQAAABUA#####wH#AAAAEAAAAAAAAAAAAAAAQAgAAAAAAAAAAAUAAQAAAMEAAAAYAP####8AAABnAAAAwAAAABUA#####wH#AAAAEAAAAAAAAAAAAAAAQAgAAAAAAAAAAAUAAQAAAMQAAAAVAP####8B#wAAABAAAAAAAAAAAAAAAEAIAAAAAAAAAAAFAAIAAADEAAAAGAD#####AAAAZgAAAL8AAAAVAP####8B#wAAABAAAAAAAAAAAAAAAEAIAAAAAAAAAAAFAAIAAADHAAAAFQD#####Af8AAAAQAAAAAAAAAAAAAABACAAAAAAAAAAABQABAAAAxwAAABgA#####wAAAGYAAADAAAAAFQD#####Af8AAAAQAAAAAAAAAAAAAABACAAAAAAAAAAABQABAAAAygAAABUA#####wH#AAAAEAAAAAAAAAAAAAAAQAgAAAAAAAAAAAUAAgAAAMr#####AAAAAQAMQ0Jpc3NlY3RyaWNlAP####8B#wAAABAAAAEAAAABAAAAxgAAAGkAAADMAAAAGwD#####Af8AAAAQAAABAAAAAQAAAMMAAABoAAAAXwAAABgA#####wAAALgAAAC+AAAAFQD#####AQAAAAAQAAAAAAAAAAAAAABACAAAAAAAAAAABQABAAAAzwAAABUA#####wEAAAAAEAAAAAAAAAAAAAAAQAgAAAAAAAAAAAUAAgAAAM8AAAAYAP####8AAAC5AAAAvQAAABUA#####wEAAAAAEAAAAAAAAAAAAAAAQAgAAAAAAAAAAAUAAQAAANIAAAAVAP####8BAAAAABAAAAAAAAAAAAAAAEAIAAAAAAAAAAAFAAIAAADSAAAAGAD#####AAAAzQAAAMAAAAAVAP####8BAAAAABAAAAAAAAAAAAAAAEAIAAAAAAAAAAAFAAIAAADVAAAAFQD#####AQAAAAAQAAAAAAAAAAAAAABACAAAAAAAAAAABQABAAAA1QAAABgA#####wAAAM4AAAC#AAAAFQD#####AQAAAAAQAAAAAAAAAAAAAABACAAAAAAAAAAABQACAAAA2AAAABUA#####wEAAAAAEAAAAAAAAAAAAAAAQAgAAAAAAAAAAAUAAQAAANgAAAAYAP####8AAAC3AAAAvAAAABUA#####wEAAAAAEAAAAAAAAAAAAAAAQAgAAAAAAAAAAAUAAQAAANsAAAAVAP####8BAAAAABAAAAAAAAAAAAAAAEAIAAAAAAAAAAAFAAIAAADbAAAAGAD#####AAAAtgAAALsAAAAVAP####8BAAAAABAAAAAAAAAAAAAAAEAIAAAAAAAAAAAFAAEAAADeAAAAFQD#####AQAAAAAQAAAAAAAAAAAAAABACAAAAAAAAAAABQACAAAA3gAAABMA#####wEAAAAAAAABAAAAXwAAAAE#4AAAAAAAAAAAAAAYAP####8AAAC6AAAA4QAAABUA#####wEAAAAAEAAAAAAAAAAAAAAAQAgAAAAAAAAAAAUAAQAAAOIAAAAVAP####8BAAAAABAAAAAAAAAAAAAAAEAIAAAAAAAAAAAFAAIAAADiAAAACAD#####AQAA#wEAA25wQQAAANEQAAAAAAABAAAAAQAAAAEAAAAAAAAAAAABQQAAAAgA#####wEAAP8BAANucEIAAADUEAAAAAAAAQAAAAEAAAABAAAAAAAAAAAAAUIAAAAIAP####8BAAD#AQADbnBDAAAA1xAAAAAAAAEAAAABAAAAAQAAAAAAAAAAAAFDAAAACAD#####AQAA#wEAA25wRAAAAOQQAAAAAAABAAAAAQAAAAEAAAAAAAAAAAABRAAAAAgA#####wEAAP8BAANucEUAAADZEAAAAAAAAQAAAAEAAAABAAAAAAAAAAAAAUUAAAAIAP####8BAAD#AQADbnBGAAAA3RAAAAAAAAEAAAABAAAAAQAAAAAAAAAAAAFGAAAACAD#####AQAA#wEAA25wRwAAAOAQAAAAAAABAAAAAQAAAAEAAAAAAAAAAAABRwAAABsA#####wEAAP8AEAAAAQAAAAEAAABuAAAADAAAAHEAAAATAP####8BAAD#AAAAAQAAAAwAAAABP+AAAAAAAAAAAAAAGAD#####AAAA7AAAAO0AAAAVAP####8BAAD#ABAAAAAAAAAAAAAAAEAIAAAAAAAAAAAFAAIAAADuAAAAFQD#####AQAA#wAQAAAAAAAAAAAAAABACAAAAAAAAAAABQABAAAA7gAAAAgA#####wAAAP8BAANucEgAAADwEAAAAAAAAQAAAAEAAAABAAAAAAAAAAAAAUgAAAALAP####8AEk1lc3VyZSBkZSBsb25ndWV1cgAAAAUAAAACAAAAAgAAAAwAAABx#####wAAAAEAC0NNZWRpYXRyaWNlAAAAAPIBAAAAABAAAAEAAAABAAAADAAAAHEAAAAHAAAAAPIBAAAAABAAAAEAAAUAAAAADAAAAHEAAAATAAAAAPIBAAAAAAAAAQAAAPQAAAABQDAAAAAAAAABAAAAGAAAAADyAAAA8wAAAPUAAAAVAAAAAPIBAAAAABAAAAEAAAUAAQAAAPYAAAAJAQAAAPIAAAAMAAAAcQAAABEBAAAA8gEAAP8BAAAAAAD3EQAAAAAAAQAAAAEAAAABAAAAAAAAAAAAAAAAAQAAAPgAAAALAP####8AEk1lc3VyZSBkZSBsb25ndWV1cgAAAAUAAAACAAAAAgAAAAwAAABdAAAAHAAAAAD6AQAAAAAQAAABAAAAAQAAAAwAAABdAAAABwAAAAD6AQAAAAAQAAABAAAFAAAAAAwAAABdAAAAEwAAAAD6AQAAAAAAAAEAAAD8AAAAAUAwAAAAAAAAAQAAABgAAAAA+gAAAPsAAAD9AAAAFQAAAAD6AQAAAAAQAAABAAAFAAEAAAD+AAAACQEAAAD6AAAADAAAAF0AAAARAQAAAPoBAAD#AQAAAAAA#xEAAAAAAAEAAAABAAAAAQAAAAAAAAAAAAAAAAEAAAEAAAAACwD#####ABJNZXN1cmUgZGUgbG9uZ3VldXIAAAAFAAAAAgAAAAIAAAAMAAAAaQAAABwAAAABAgEAAAAAEAAAAQAAAAEAAAAMAAAAaQAAAAcAAAABAgEAAAAAEAAAAQAABQAAAAAMAAAAaQAAABMAAAABAgEAAAAAAAABAAABBAAAAAFAMAAAAAAAAAEAAAAYAAAAAQIAAAEDAAABBQAAABUAAAABAgEAAAAAEAAAAQAABQABAAABBgAAAAkBAAABAgAAAAwAAABpAAAAEQEAAAECAQAA#wEAAAAAAQcRAAAAAAABAAAAAQAAAAEAAAAAAAAAAAAAAAABAAABCAAAAAsA#####wASTWVzdXJlIGRlIGxvbmd1ZXVyAAAABQAAAAIAAAACAAAADAAAAF8AAAAcAAAAAQoBAAAAABAAAAEAAAABAAAADAAAAF8AAAAHAAAAAQoBAAAAABAAAAEAAAUAAAAADAAAAF8AAAATAAAAAQoBAAAAAAAAAQAAAQwAAAABQDAAAAAAAAABAAAAGAAAAAEKAAABCwAAAQ0AAAAVAAAAAQoBAAAAABAAAAEAAAUAAQAAAQ4AAAAJAQAAAQoAAAAMAAAAXwAAABEBAAABCgEAAP8BAAAAAAEPEQAAAAAAAQAAAAEAAAABAAAAAAAAAAAAAAAAAQAAARAAAAALAP####8AEk1lc3VyZSBkZSBsb25ndWV1cgAAAAUAAAACAAAAAgAAAAwAAABoAAAAHAAAAAESAQAAAAAQAAABAAAAAQAAAAwAAABoAAAABwAAAAESAQAAAAAQAAABAAAFAAAAAAwAAABoAAAAEwAAAAESAQAAAAAAAAEAAAEUAAAAAUAwAAAAAAAAAQAAABgAAAABEgAAARMAAAEVAAAAFQAAAAESAQAAAAAQAAABAAAFAAEAAAEWAAAACQEAAAESAAAADAAAAGgAAAARAQAAARIBAAD#AQAAAAABFxEAAAAAAAEAAAABAAAAAQAAAAAAAAAAAAAAAAEAAAEYAAAACwD#####ABJNZXN1cmUgZGUgbG9uZ3VldXIAAAAFAAAAAgAAAAIAAAAMAAAAYQAAABwAAAABGgEAAAAAEAAAAQAAAAEAAAAMAAAAYQAAAAcAAAABGgEAAAAAEAAAAQAABQAAAAAMAAAAYQAAABMAAAABGgEAAAAAAAABAAABHAAAAAFAMAAAAAAAAAEAAAAYAAAAARoAAAEbAAABHQAAABUAAAABGgEAAAAAEAAAAQAABQABAAABHgAAAAkBAAABGgAAAAwAAABhAAAAEQEAAAEaAQAA#wEAAAAAAR8RAAAAAAABAAAAAQAAAAEAAAAAAAAAAAAAAAABAAABIAAAAAsA#####wASTWVzdXJlIGRlIGxvbmd1ZXVyAAAABQAAAAIAAAACAAAADAAAAG4AAAAcAAAAASIBAAAAABAAAAEAAAABAAAADAAAAG4AAAAHAAAAASIBAAAAABAAAAEAAAUAAAAADAAAAG4AAAATAAAAASIBAAAAAAAAAQAAASQAAAABQDAAAAAAAAABAAAAGAAAAAEiAAABIwAAASUAAAAVAAAAASIBAAAAABAAAAEAAAUAAQAAASYAAAAJAQAAASIAAAAMAAAAbgAAABEBAAABIgEAAP8BAAAAAAEnEQAAAAAAAQAAAAEAAAABAAAAAAAAAAAAAAAAAQAAASgAAAACAP####8AAmRBAAJBQgAAAA4AAAD4AAAAAgD#####AAJkQgACQUMAAAAOAAABAAAAAAIA#####wACZEMAAkFEAAAADgAAAQgAAAACAP####8AAmREAAJBRQAAAA4AAAEQAAAAAgD#####AAJkRQACQUYAAAAOAAABGAAAAAIA#####wACZEYAAkFHAAAADgAAASAAAAACAP####8AAmRHAAJBSAAAAA4AAAEoAAAACP##########'
    const yy = j3pCreeSVG(stor.lesdiv.figure, { id: svgId, width: 353, height: 355 })
    yy.style.border = 'solid black 1px'
    stor.mtgAppLecteur.removeAllDoc()
    stor.mtgAppLecteur.addDoc(svgId, txtFigure, true)
    stor.mtgAppLecteur.giveFormula2(svgId, 'A1', j3pGetRandomInt(0, 359))
    stor.mtgAppLecteur.giveFormula2(svgId, 'A2', j3pGetRandomInt(0, 10))
    stor.mtgAppLecteur.giveFormula2(svgId, 'A3', j3pGetRandomInt(0, 10))
    stor.mtgAppLecteur.giveFormula2(svgId, 'A4', j3pGetRandomInt(0, 10))
    stor.mtgAppLecteur.setText(svgId, '#npH', stor.lettres[7], true)
    stor.mtgAppLecteur.calculateAndDisplayAll(true)

    const listed = j3pShuffle([1, 2, 3])
    let adire = []
    const lespos = [['A', 'B', 'C'], ['E', 'F', 'G'], ['C', 'D', 'E']]
    const aaf = []
    stor.lesrepos = []
    let ladist
    stor.nomdroite = '(d' + listed[2] + ')'
    for (let i = 0; i < ds.NbDroite; i++) {
      const li = listed.pop()
      stor.mtgAppLecteur.setVisible(svgId, '#d' + li, true, true)
      stor.mtgAppLecteur.setVisible(svgId, '#nd' + li, true, true)
      for (let j = 0; j < 3; j++) {
        if (aaf.indexOf(lespos[li - 1][j]) === -1) {
          aaf.push(lespos[li - 1][j])
        }
      }
    }

    for (let i = 0; i < aaf.length; i++) {
      let lang = false
      ladist = String(j3pArrondi(stor.mtgAppLecteur.valueOf(svgId, 'd' + aaf[i]), 1)).replace('.', ',')
      if (i === 1) {
        stor.larep = ladist
        stor.cons = '#s' + aaf[i]
      }
      stor.mtgAppLecteur.setVisible(svgId, '#p' + aaf[i], true, true)
      stor.mtgAppLecteur.setVisible(svgId, '#np' + aaf[i], true, true)
      stor.mtgAppLecteur.setVisible(svgId, '#s' + aaf[i], true, true)
      if (aaf[i] === 'B' || aaf[i] === 'D' || aaf[i] === 'F') {
        stor.mtgAppLecteur.setVisible(svgId, '#s' + aaf[i] + '2', true, true)
        stor.mtgAppLecteur.setVisible(svgId, '#s' + aaf[i] + '3', true, true)
        lang = true
        if (aaf[i] === 'B') ladist = j3pGetRandomBool() ? '2,3' : '2,2'
        if (aaf[i] === 'D') ladist = j3pGetRandomBool() ? '2,5' : '2,4'
        if (aaf[i] === 'F') ladist = j3pGetRandomBool() ? '2,7' : '2,6'
      }
      if (lang && i === 1) {
        stor.larep = ladist
        stor.cons = '#s' + aaf[i]
      }
      stor.mtgAppLecteur.setText(svgId, '#np' + aaf[i], stor.lettres[i], true)
      stor.lesrepos.push({ val: ladist, ang: lang, num: i })
      adire.push('$' + stor.lettres[7] + stor.lettres[i] + ' = ' + ladist + ' $ cm')
    }

    adire = j3pShuffle(adire)

    const wherd = addDefaultTable(stor.lesdiv.donnees, 1, 4)
    let dou = wherd[0][1]
    wherd[0][0].style.width = '10px'
    wherd[0][2].style.width = '20px'
    for (let i = 0; i < adire.length; i++) {
      j3pAffiche(dou, null, adire[i] + '\n')
      if (i > 2) dou = wherd[0][3]
    }
  }

  function poseQuestion () {
    j3pAffiche(stor.lesdiv.consigneG, null, '<b>Complète la phrase ci-contre.</b>')
    const tyu = addDefaultTable(stor.lesdiv.travail, 2, 1)
    const tyuer = addDefaultTable(tyu[1][0], 1, 3)
    j3pAffiche(tyu[0][0], null, 'La distance du point ' + stor.lettres[7] + ' à la droite ' + stor.nomdroite)
    j3pAffiche(tyuer[0][0], null, ' est égale à &nbsp;')
    j3pAffiche(tyuer[0][2], null, '&nbsp; cm .')
    stor.lazone = new ZoneStyleMathquill1(tyuer[0][1], { restric: '0123456789,.', limite: 6, enter: me.sectionCourante.bind(me) })
  }

  function boutonfen () {
    j3pToggleFenetres('Calculatrice')
  }

  function yaReponse () {
    if (me.isElapsed) return true
    if (stor.lazone.reponse() === '') {
      stor.lazone.focus()
      return false
    }
    return true
  }
  function isRepOk () {
    stor.errAucun = false
    stor.errBondrte = false
    stor.errAng = false

    let t2, i
    const valpo = []

    const t1 = stor.lazone.reponse()
    if (stor.lazone.reponse() === '') { return false }
    for (i = 0; i < stor.lesrepos.length; i++) {
      t2 = stor.lesrepos[i].val
      if (t1 === t2) valpo.push(stor.lesrepos[i])
    }

    if (valpo.length === 0) {
      stor.errAucun = true
      stor.lazone.corrige(false)
      return false
    }
    for (i = 0; i < valpo.length; i++) {
      if (valpo[i].ang && valpo[i].num === 1) return true
    }
    stor.lazone.corrige(false)
    if (!valpo[0].ang) {
      stor.errAng = true
      return false
    }
    stor.errBondrte = true
    return false
  } // isRepOk

  function desactiveAll () {
    stor.lazone.disable()
    stor.mtgAppLecteur.setColor(svgId, stor.cons, 0, 0, 255, true)
    stor.mtgAppLecteur.setLineStyle(svgId, stor.cons, 1, 4, true, true)
  } // desactiveAll
  function passeToutVert () {
    stor.lazone.corrige(true)
  } // passeToutVert
  function barrelesfo () {
    stor.lazone.barre()
  }
  function affCorrFaux (isFin) {
    /// //affiche indic

    if (stor.errAucun) {
      j3pAffiche(stor.lesdiv.explications, null, 'Cette longueur ne fait pas partie des données  !\n')
      stor.yaexplik = true
    }
    if (stor.errBondrte) {
      j3pAffiche(stor.lesdiv.explications, null, 'Regarde bien le nom de la droite !\n')
      stor.yaexplik = true
    }
    if (stor.errAng) {
      j3pAffiche(stor.lesdiv.explications, null, 'Il faut utiliser la perpendiculaire !\n')
      stor.yaexplik = true
    }

    if (isFin) {
      // barre les faux
      stor.yaco = true
      j3pAffiche(stor.lesdiv.solution, null, 'La distance du point ' + stor.lettres[7] + ' à la droite ' + stor.nomdroite + ' est égale à $' + stor.larep + '$ cm .')
      barrelesfo()
      desactiveAll()
    } else { me.afficheBoutonValider() }
  } // affCorrFaux
  function enonceMain () {
    if (((me.questionCourante % ds.nbetapes) === 1) || (ds.nbetapes === 1)) {
      // A chaque répétition, on nettoie la scène
      me.videLesZones()
      // ou dans le cas de presentation3
      // me.ajouteBoutons()
    }

    creeLesDiv()
    stor.lesdiv.travail.classList.add('travail')
    me.zonesElts.MG.classList.add('fond')
    faiFigure()
    poseQuestion()
    if (ds.Calculatrice) {
      me.fenetresjq = [{ name: 'Calculatrice', title: 'Une calculatrice', left: 600, top: 10 }]
      j3pCreeFenetres(me)
      j3pAfficheCroixFenetres('Calculatrice')
      j3pAjouteBouton(stor.lesdiv.calculatrice, 'Calculatrice', 'MepBoutons', 'Calculatrice', boutonfen)
    }
    me.afficheBoutonValider()
    me.finEnonce()
  }

  switch (me.etat) {
    case 'enonce':
      // code exécuté au lancement de la section
      if (me.debutDeLaSection) {
        initSection()
      } else {
        enonceMain()
      }
      break // case "enonce":

    case 'correction':
      // On teste si une réponse a été saisie
      stor.yaexplik = false
      stor.yaco = false
      stor.lesdiv.explications.innerHTML = ''
      stor.lesdiv.solution.innerHTML = ''
      if (stor.yaexplik) stor.lesdiv.explications.classList.add('explique')
      if (stor.yaco) stor.lesdiv.solution.classList.add('correction')

      if (yaReponse()) {
        // Bonne réponse
        if (isRepOk()) {
          me._stopTimer()
          stor.lesdiv.correction.style.color = me.styles.cbien
          stor.lesdiv.correction.innerHTML = cBien

          desactiveAll()
          passeToutVert()

          me.score++
          me.typederreurs[0]++
          me.cacheBoutonValider()
          me.etat = 'navigation'
          me.sectionCourante()
        } else {
          // Pas de bonne réponse
          stor.lesdiv.correction.style.color = me.styles.cfaux
          stor.lesdiv.correction.innerHTML = cFaux
          if (me.isElapsed) {
            // A cause de la limite de temps :
            me._stopTimer()

            stor.lesdiv.correction.innerHTML = tempsDepasse + '<BR>'
            me.typederreurs[10]++
            me.cacheBoutonValider()

            affCorrFaux(true)
            stor.yaco = true

            me.etat = 'navigation'
            me.sectionCourante()
          } else {
            // Réponse fausse :
            if (me.essaiCourant < ds.nbchances) {
              stor.lesdiv.correction.innerHTML += '<br>' + essaieEncore

              affCorrFaux(false)

              // affCorrFaux.call(this, false)
              me.typederreurs[1]++
            } else {
              // Erreur au nème essai
              me._stopTimer()
              me.cacheBoutonValider()

              affCorrFaux(true)
              stor.yaco = true

              me.typederreurs[2]++
              me.etat = 'navigation'
              me.sectionCourante()
            }
          }
        }
      } else {
        // pas de réponse
        stor.lesdiv.correction.style.color = me.styles.cfaux
        stor.lesdiv.correction.innerHTML = reponseIncomplete
        me.afficheBoutonValider()
      }

      if (ds.theme === 'zonesAvecImageDeFond') {
        if (stor.yaexplik) stor.lesdiv.explications.classList.add('explique')
        if (stor.yaco) stor.lesdiv.solution.classList.add('correction')
      }
      // Obligatoire
      me.finCorrection()
      break // case "correction":

    case 'navigation':
      if (!me.sectionTerminee()) {
        me.etat = 'enonce'
      }
      me.finNavigation()
      break
  }
}
