import { j3pAddElt, j3pClone, j3pGetNewId, j3pGetRandomInt, j3pShowError, j3pShuffle } from 'src/legacy/core/functions'
import { addDefaultTable } from 'src/legacy/themes/table'
import ListeDeroulante from 'src/legacy/outils/listeDeroulante/ListeDeroulante'
import { j3pAffiche } from 'src/lib/mathquill/functions'
import { j3pCreeSVG } from 'src/legacy/core/functionsSvg'
import { getMtgCore } from 'src/lib/outils/mathgraph'
import { addMaj } from 'src/lib/utils/string'
import textesGeneriques from 'src/lib/core/textes'

const { cBien, cFaux, essaieEncore, reponseIncomplete, tempsDepasse } = textesGeneriques

/**
 * Ajouter une description ici
 * @fileOverview
 * @author Tommy Barroy <t.barroy@laposte.net>
 * @since 2017-10
*/
export const params = {
  outils: ['mathquill'],
  /**
   * Ce qui est paramétrable dans l’éditeur de graphe
   * @type {LegacySectionParamsParametres}
   */
  parametres: [
    ['nbrepetitions', 3, 'entier', 'Nombre de répétitions de la section'],
    ['limite', 0, 'entier', 'Temps disponible par répétition, en secondes'],
    ['Question', 'est', 'liste', '<u>est</u>: avec code. <br><br> <u>semble</u>: sans code.', ['est', 'semble', 'les deux']],
    ['Elem_inutiles', 0, 'entier', 'entre 0 et 2'],
    ['Mode', 'clic', 'liste', '<u>clic</u>: avec code. <br><br> <u>complete</u>: sans code.', ['clic', 'complete']],
    ['theme', 'standard', 'liste', 'Modèle pour l’affichage', ['standard', 'zonesAvecImageDeFond']]
  ]
}

/**
 * section med01
 * @this {Parcours}
 */
/**
 * section med02
 * @this {Parcours}
 */
export default function main () {
  const me = this
  const stor = this.storage
  let ds = me.donneesSection
  let svgId = stor.svgId

  function getDonnees () {
    return {
      // Nombre de répétitions de l’exercice
      nbrepetitions: 3,

      Question: 'est',
      Elem_inutiles: 0,
      // Si indication != "" alors un lien "Indication" s’affiche dans la zone Inférieure Gauche (this.zones.IG
      // une indication<br>de deux lignes..."
      indication: '',
      Mode: 'clic',

      // pour le cas de presentation1bis
      // En effet pas d’indication possible pour cette présentation
      // surchargeindication: false,

      // Nombre de chances dont dispose l’él§ve pour répondre à la question
      nbchances: 1,

      //  || "presentation2" || "presentation3"  || "presentation1bis"
      structure: 'presentation1bis'
    }
  }

  function initSection () {
    let i
    ds = getDonnees()
    me.donneesSection = ds
    // On surcharge avec les parametres passés par le graphe
    me.surcharge()
    me.validOnEnter = false // ex donneesSection.touche_entree
    // Construction de la page
    me.construitStructurePage(me.donneesSection.structure)
    me.donneesSection.nbetapes = 1
    me.donneesSection.nbitems = me.donneesSection.nbetapes * me.donneesSection.nbrepetitions

    ds.lesExos = []
    let lp = []
    if (ds.Question === 'est' || ds.Question === 'les deux') lp.push('oui')
    if (ds.Question === 'semble' || ds.Question === 'les deux') lp.push('semble')
    lp.push('non')
    stor.tablp = j3pClone(lp)
    stor.tablp.splice(0, 0, '')
    lp = j3pShuffle(lp)
    for (i = 0; i < me.donneesSection.nbitems; i++) {
      ds.lesExos.push({ rand: lp[i % lp.length] })
    }
    ds.lesExos = j3pShuffle(ds.lesExos)

    lp = ['perp', 'ss']
    if (ds.Elem_inutiles > 0) lp.push('autre')
    lp = j3pShuffle(lp)
    for (i = 0; i < me.donneesSection.nbitems; i++) {
      ds.lesExos[i].cas = lp[i % lp.length]
    }
    ds.lesExos = j3pShuffle(ds.lesExos)

    lp = [true, false]
    lp = j3pShuffle(lp)
    for (i = 0; i < me.donneesSection.nbitems; i++) {
      ds.lesExos[i].haz = lp[i % lp.length]
    }
    ds.lesExos = j3pShuffle(ds.lesExos)

    lp = []
    if (ds.Question === 'est' || ds.Question === 'les deux') lp.push('est')
    if (ds.Question === 'semble' || ds.Question === 'les deux') lp.push('semb')
    lp = j3pShuffle(lp)
    for (i = 0; i < me.donneesSection.nbitems; i++) {
      ds.lesExos[i].dire = lp[i % lp.length]
    }
    ds.lesExos = j3pShuffle(ds.lesExos)

    me.typederreurs = [0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0]

    me.score = 0
    const tt = 'Reconnaitre une médiatrice'

    me.afficheTitre(tt)

    // chargement mtg
    getMtgCore({ withMathJax: true })
      .then(
        // success
        (mtgAppLecteur) => {
          stor.mtgAppLecteur = mtgAppLecteur
          enonceMain()
        },
        // failure
        (error) => {
          j3pShowError(error, { message: 'mathgraph n’est pas correctement chargé', mustNotify: true })
        })
      // plantage dans enonceMain
      .catch(j3pShowError)
  }

  function faisChoixExos () {
    const e = ds.lesExos.pop()
    const nomspris = []
    stor.nA = addMaj(nomspris)
    stor.nB = addMaj(nomspris)
    stor.nC = addMaj(nomspris)
    stor.nD = addMaj(nomspris)
    stor.blok = false
    return e
  }
  function modif (ki) {
    for (let i = 0; i < ki.length; i++) {
      for (let j = 0; j < ki[i].length; j++) {
        ki[i][j].style.padding = '0px'
      }
    }
  }

  function poseQuestion () {
    if (ds.Mode !== 'complete') {
      let lp = j3pShuffle(['piege1', 'piege2', 'piege3'])
      const gh = []
      if (stor.Lexo.cas === 'autre') {
        gh.push('med2')
        lp = j3pShuffle(['piege1', 'piege3'])
      }
      if (stor.Lexo.cas === 'perp') {
        gh.push('perp')
      }
      do {
        gh.push(lp.pop())
      } while (gh.length < 3)
      stor.acli = []
      gh.push('mediatrice')
      for (let i = 0; i < gh.length; i++) {
        stor.acli.push({ ki: gh[i], clicked: false })
        stor.mtgAppLecteur.setVisible(svgId, '#' + gh[i], true, true)
        stor.mtgAppLecteur.setVisible(svgId, '#c' + gh[i], true, true)
        stor.mtgAppLecteur.setColor(svgId, '#c' + gh[i], 0, 0, 0, true, 0)
        stor.mtgAppLecteur.addEventListener(svgId, '#c' + gh[i], 'mouseover', makeFoncOver(stor.acli[i]))
        stor.mtgAppLecteur.addEventListener(svgId, '#c' + gh[i], 'mouseout', makeFoncOut(stor.acli[i]))
        stor.mtgAppLecteur.addEventListener(svgId, '#c' + gh[i], 'click', makeFoncSelect(stor.acli[i]))
      }
      if (stor.Lexo.dire === 'est') {
        j3pAffiche(stor.lesdiv.consigne, null, '<b>Sélectionne la médiatrice du segment [' + stor.nA + stor.nB + '].</b>')
        stor.mtgAppLecteur.setVisible(svgId, '#a1', true, true)
        stor.mtgAppLecteur.setVisible(svgId, '#a2', true, true)
        if (stor.Lexo.haz) {
          stor.mtgAppLecteur.setVisible(svgId, '#m1', true, true)
          stor.mtgAppLecteur.setVisible(svgId, '#m2', true, true)
        } else {
          stor.mtgAppLecteur.setVisible(svgId, '#m7', true, true)
          stor.mtgAppLecteur.setVisible(svgId, '#m8', true, true)
        }
        if (gh.indexOf('perp') !== -1) {
          stor.mtgAppLecteur.setVisible(svgId, '#a3', true, true)
          stor.mtgAppLecteur.setVisible(svgId, '#a4', true, true)
        }
        if (gh.indexOf('med2') !== -1) {
          stor.mtgAppLecteur.setVisible(svgId, '#a5', true, true)
          stor.mtgAppLecteur.setVisible(svgId, '#a6', true, true)
          if (stor.Lexo.haz) {
            stor.mtgAppLecteur.setVisible(svgId, '#m3', true, true)
            stor.mtgAppLecteur.setVisible(svgId, '#m4', true, true)
          } else {
            stor.mtgAppLecteur.setVisible(svgId, '#m5', true, true)
            stor.mtgAppLecteur.setVisible(svgId, '#m6', true, true)
          }
        }
      } else {
        j3pAffiche(stor.lesdiv.consigne, null, '<b>Sélectionne la droite qui semble être </b>la médiatrice du segment [' + stor.nA + stor.nB + '].')
      }
      stor.lesdiv.zonefig.classList.add('travail')
    } else {
      stor.lesdiv.zonefig.classList.add('enonce')
      stor.lesdiv.travail.classList.add('travail')
      const yui = addDefaultTable(stor.lesdiv.travail, 1, 3)
      modif(yui)
      switch (stor.Lexo.rand) {
        case 'oui':
          stor.mtgAppLecteur.setVisible(svgId, '#mediatrice', true, true)
          stor.mtgAppLecteur.setColor(svgId, '#mediatrice', 200, 30, 200, true)
          stor.mtgAppLecteur.setLineStyle(svgId, '#mediatrice', 0, 6, true, true)
          stor.mtgAppLecteur.setVisible(svgId, '#a1', true, true)
          stor.mtgAppLecteur.setVisible(svgId, '#a2', true, true)
          if (stor.Lexo.haz) {
            stor.mtgAppLecteur.setVisible(svgId, '#m1', true, true)
            stor.mtgAppLecteur.setVisible(svgId, '#m2', true, true)
          } else {
            stor.mtgAppLecteur.setVisible(svgId, '#m7', true, true)
            stor.mtgAppLecteur.setVisible(svgId, '#m8', true, true)
          }
          break
        case 'semble': {
          stor.mtgAppLecteur.setVisible(svgId, '#mediatrice', true, true)
          stor.mtgAppLecteur.setColor(svgId, '#mediatrice', 200, 30, 200, true)
          stor.mtgAppLecteur.setLineStyle(svgId, '#mediatrice', 0, 6, true, true)
          const haz = j3pGetRandomInt(0, 2)
          if (haz === 0) {
            stor.mtgAppLecteur.setVisible(svgId, '#a1', true, true)
            stor.mtgAppLecteur.setVisible(svgId, '#a2', true, true)
          }
          if (haz === 1) {
            if (stor.Lexo.haz) {
              stor.mtgAppLecteur.setVisible(svgId, '#m1', true, true)
              stor.mtgAppLecteur.setVisible(svgId, '#m2', true, true)
            } else {
              stor.mtgAppLecteur.setVisible(svgId, '#m7', true, true)
              stor.mtgAppLecteur.setVisible(svgId, '#m8', true, true)
            }
          }
        }
          break
        default:
          switch (stor.Lexo.cas) {
            case 'perp':
              stor.mtgAppLecteur.setVisible(svgId, '#perp', true, true)
              stor.mtgAppLecteur.setColor(svgId, '#perp', 200, 30, 200, true)
              stor.mtgAppLecteur.setLineStyle(svgId, '#perp', 0, 6, true, true)
              stor.mtgAppLecteur.setVisible(svgId, '#a3', true, true)
              stor.mtgAppLecteur.setVisible(svgId, '#a4', true, true)
              break
            case 'autre':
              stor.mtgAppLecteur.setVisible(svgId, '#[DA]', true, true)
              stor.mtgAppLecteur.setVisible(svgId, '#med2', true, true)
              stor.mtgAppLecteur.setColor(svgId, '#med2', 200, 30, 200, true)
              stor.mtgAppLecteur.setLineStyle(svgId, '#med2', 0, 6, true, true)
              stor.mtgAppLecteur.setVisible(svgId, '#a5', true, true)
              stor.mtgAppLecteur.setVisible(svgId, '#a6', true, true)
              if (stor.Lexo.haz) {
                stor.mtgAppLecteur.setVisible(svgId, '#m3', true, true)
                stor.mtgAppLecteur.setVisible(svgId, '#m4', true, true)
              } else {
                stor.mtgAppLecteur.setVisible(svgId, '#m5', true, true)
                stor.mtgAppLecteur.setVisible(svgId, '#m6', true, true)
              }
              break
            default: {
              const p = ['#piege1', '#piege2', '#piege3'][j3pGetRandomInt(0, 2)]
              stor.mtgAppLecteur.setVisible(svgId, p, true, true)
              stor.mtgAppLecteur.setColor(svgId, p, 200, 30, 200, true)
              stor.mtgAppLecteur.setLineStyle(svgId, p, 0, 6, true, true)
            }
          }
      }
      j3pAffiche(yui[0][0], null, 'La droite violette&nbsp;')
      j3pAffiche(yui[0][2], null, '&nbsp;la médiatrice du segment [' + stor.nA + stor.nB + '].')
      const ll = ['Choisir']
      if (ds.Question === 'est' || ds.Question === 'les deux') ll.push('est')
      if (ds.Question === 'semble' || ds.Question === 'les deux') ll.push('semble être')
      ll.push('n’est pas')
      stor.listeDeroulante = ListeDeroulante.create(yui[0][1], ll, { centre: true })
    }
    stor.lesdiv.consigne.classList.add('enonce')
    stor.lesdiv.explications.classList.remove('explique')
    stor.lesdiv.solution.classList.remove('correction')
  }
  function makeFoncOver (elem) {
    return function () {
      if (stor.blok) return
      stor.mtgAppLecteur.getDoc(svgId).defaultCursor = 'pointer'
      if (elem.clicked) {
        stor.mtgAppLecteur.setColor(svgId, '#' + elem.ki, 200, 50, 50, true)
        stor.mtgAppLecteur.setLineStyle(svgId, '#' + elem.ki, 0, 5, true)
      } else {
        stor.mtgAppLecteur.setColor(svgId, '#' + elem.ki, 250, 0, 0, true)
        stor.mtgAppLecteur.setLineStyle(svgId, '#' + elem.ki, 0, 5, true)
      }
    }
  }
  function makeFoncOut (elem) {
    return function () {
      if (stor.blok) return
      stor.mtgAppLecteur.getDoc(svgId).defaultCursor = ''
      if (elem.clicked) {
        stor.mtgAppLecteur.setColor(svgId, '#' + elem.ki, 255, 0, 0, true)
        stor.mtgAppLecteur.setLineStyle(svgId, '#' + elem.ki, 0, 4, true)
      } else {
        stor.mtgAppLecteur.setColor(svgId, '#' + elem.ki, 100, 100, 100, true)
        stor.mtgAppLecteur.setLineStyle(svgId, '#' + elem.ki, 0, 2, true)
      }
    }
  }
  function makeFoncSelect (elem) {
    return function () {
      if (stor.blok) return
      elem.clicked = !elem.clicked
      stor.mtgAppLecteur.getDoc(svgId).defaultCursor = ''
      if (elem.clicked) {
        etientTout()
        elem.clicked = true
        stor.mtgAppLecteur.setColor(svgId, '#' + elem.ki, 255, 0, 0, true)
        stor.mtgAppLecteur.setLineStyle(svgId, '#' + elem.ki, 0, 4, true)
      } else {
        stor.mtgAppLecteur.setColor(svgId, '#' + elem.ki, 100, 100, 100, true)
        stor.mtgAppLecteur.setLineStyle(svgId, '#' + elem.ki, 0, 2, true)
      }
    }
  }

  function etientTout () {
    for (let i = 0; i < stor.acli.length; i++) {
      stor.acli[i].clicked = false
      stor.mtgAppLecteur.setColor(svgId, '#' + stor.acli[i].ki, 100, 100, 100, true)
      stor.mtgAppLecteur.setLineStyle(svgId, '#' + stor.acli[i].ki, 0, 2, true)
    }
  }
  function creeLesDiv () {
    me.zonesElts.MG.classList.add('fond')
    stor.lesdiv = {}
    stor.lesdiv.conteneur = j3pAddElt(me.zonesElts.MG, 'div', {
      style: me.styles.etendre('toutpetit.enonce', { padding: '10px' })
    })
    stor.lesdiv.consignet = addDefaultTable(stor.lesdiv.conteneur, 1, 1)[0][0]
    stor.lesdiv.consignet.style.padding = '0px'
    const tt = addDefaultTable(stor.lesdiv.consignet, 2, 1)
    modif(tt)
    stor.lesdiv.consigne = tt[0][0]
    stor.lesdiv.zonefig = tt[1][0]
    stor.lesdiv.travail = addDefaultTable(stor.lesdiv.conteneur, 1, 1)[0][0]
    stor.lesdiv.travail.style.padding = '0px'
    stor.lesdiv.correction = j3pAddElt(me.zonesElts.MD, 'div', {
      style: me.styles.etendre('toutpetit.enonce', { padding: '10px' })
    })
    stor.lesdiv.explications = j3pAddElt(stor.lesdiv.conteneur, 'div')
    stor.lesdiv.solution = j3pAddElt(stor.lesdiv.conteneur, 'div')
    stor.lesdiv.explications.style.color = me.styles.cfaux
    stor.lesdiv.solution.style.color = me.styles.toutpetit.correction.color
  }

  function makefig () {
    const txtFigure = 'TWF0aEdyYXBoSmF2YTEuMAAAABM+TMzNAAJmcv###wEA#wEAAAAAAAAAAAM5AAACbQAAAQEAAAAAAAAAAQAAAJz#####AAAAAQAKQ0NhbGNDb25zdAD#####AAJwaQAWMy4xNDE1OTI2NTM1ODk3OTMyMzg0Nv####8AAAABAApDQ29uc3RhbnRlQAkh+1RELRj#####AAAAAQAKQ1BvaW50QmFzZQD#####AQAAAAAQAAAAAAAAAAAAAABACAAAAAAAAAAABQABQGOAAAAAAABAY#wo9cKPXAAAAAIA#####wEAAAAAEAAAAAAAAAAAAAAAQAgAAAAAAAAAAAUAAUBjAAAAAAAAQEBwo9cKPXAAAAACAP####8AAAAAABAAAABACAAAAAAAAAAAAAAAAAAAAAAIAABAUQAAAAAAAEB#LhR64Ueu#####wAAAAEAB0NDYWxjdWwA#####wAFbWluaTEAATAAAAABAAAAAAAAAAAAAAADAP####8ABW1heGkxAAMzNjAAAAABQHaAAAAAAAD#####AAAAAQAUQ0ltcGxlbWVudGF0aW9uUHJvdG8A#####wAHQ3Vyc2V1cgAAAAUAAAAFAAAAAwAAAAQAAAAFAAAAA#####8AAAABABRDRHJvaXRlRGlyZWN0aW9uRml4ZQAAAAAGAQAAAAAQAAABAAAAAQAAAAMBP#AAAAAAAAD#####AAAAAQAPQ1BvaW50TGllRHJvaXRlAQAAAAYA#wAAABAAAADACAAAAAAAAD#wAAAAAAAAAAAFAABAXUAAAAAAAAAAAAf#####AAAAAQALQ0hvbW90aGV0aWUAAAAABgAAAAP#####AAAAAQAKQ09wZXJhdGlvbgP#####AAAAAQAPQ1Jlc3VsdGF0VmFsZXVyAAAABAAAAAgBAAAACQAAAAQAAAAJAAAABf####8AAAABAAtDUG9pbnRJbWFnZQAAAAAGAQAAAAANAAJPMQDAEAAAAAAAAEAQAAAAAAAAAAAFAAAAAAgAAAAJAAAABwAAAAAGAAAAAwAAAAgDAAAACAEAAAABP#AAAAAAAAAAAAAJAAAABAAAAAgBAAAACQAAAAUAAAAJAAAABAAAAAoAAAAABgEAAAAADQACSTEAwAAAAAAAAABACAAAAAAAAAAABQAAAAAIAAAAC#####8AAAABAAhDU2VnbWVudAEAAAAGAP8AAAAQAAABAAAAAQAAAAMAAAAIAAAABgEAAAAGAP8AAAEQAAJrMQDAAAAAAAAAAEAAAAAAAAAAAAABAAE#5ZtZtZtZtgAAAA3#####AAAAAgAPQ01lc3VyZUFic2Npc3NlAQAAAAYAAmExAAAACgAAAAwAAAAO#####wAAAAEAD0NWYWxldXJBZmZpY2hlZQEAAAAGAP8AAAAAAAAAAAAAAMAYAAAAAAAAAAAAAAAODwAB####AAAAAQAAAAIAAAABAAAAAAAAAAAAAAAAAgAAAA8AAAADAP####8AAkExAAJhMQAAAAkAAAAP#####wAAAAEACUNSb3RhdGlvbgD#####AAAAAQAAAAkAAAARAAAACgD#####AAAAAAAQAAAAAAAAAAAAAABACAAAAAAAAAACcEEJAAAAAAIAAAASAAAAAgD#####AAAAAAAQAAAAQAgAAAAAAAAAAAAAAAAAAAAACAAAQHCQAAAAAABAfr4UeuFHrgAAAAMA#####wAFbWluaTIAATAAAAABAAAAAAAAAAAAAAADAP####8ABW1heGkyAAIxMAAAAAFAJAAAAAAAAAAAAAQA#####wAHQ3Vyc2V1cgAAAAUAAAAFAAAAAwAAABUAAAAWAAAAFAAAAAUAAAAAFwEAAAAAEAAAAQAAAAEAAAAUAT#wAAAAAAAAAAAABgEAAAAXAP8AAAAQAAAAwAgAAAAAAAA#8AAAAAAAAAAABQAAQF1AAAAAAAAAAAAYAAAABwAAAAAXAAAAFAAAAAgDAAAACQAAABUAAAAIAQAAAAkAAAAVAAAACQAAABYAAAAKAAAAABcBAAAAAA0AAk8xAMAQAAAAAAAAQBAAAAAAAAAAAAUAAAAAGQAAABoAAAAHAAAAABcAAAAUAAAACAMAAAAIAQAAAAE#8AAAAAAAAAAAAAkAAAAVAAAACAEAAAAJAAAAFgAAAAkAAAAVAAAACgAAAAAXAQAAAAANAAJJMQDAAAAAAAAAAEAIAAAAAAAAAAAFAAAAABkAAAAcAAAACwEAAAAXAP8AAAAQAAABAAAAAQAAABQAAAAZAAAABgEAAAAXAP8AAAEQAAFrAMAAAAAAAAAAQAAAAAAAAAAAAAEAAT#lVVVVVVVVAAAAHgAAAAwBAAAAFwACYTIAAAAbAAAAHQAAAB8AAAANAQAAABcA#wAAAAAAAAAAAAAAwBgAAAAAAAAAAAAAAB8PAAH###8AAAABAAAAAgAAAAEAAAAAAAAAAAAAAAACAAAAIAAAAAIA#####wAAAAAAEAAAAEAIAAAAAAAAAAAAAAAAAAAAAAgAAEB7oAAAAAAAQH6eFHrhR64AAAADAP####8ABW1pbmkzAAEwAAAAAQAAAAAAAAAAAAAAAwD#####AAVtYXhpMwACMTAAAAABQCQAAAAAAAAAAAAEAP####8AB0N1cnNldXIAAAAFAAAABQAAAAMAAAAjAAAAJAAAACIAAAAFAAAAACUBAAAAABAAAAEAAAABAAAAIgE#8AAAAAAAAAAAAAYBAAAAJQD#AAAAEAAAAMAIAAAAAAAAP#AAAAAAAAAAAAUAAEBdQAAAAAAAAAAAJgAAAAcAAAAAJQAAACIAAAAIAwAAAAkAAAAjAAAACAEAAAAJAAAAIwAAAAkAAAAkAAAACgAAAAAlAQAAAAANAAJPMQDAEAAAAAAAAEAQAAAAAAAAAAAFAAAAACcAAAAoAAAABwAAAAAlAAAAIgAAAAgDAAAACAEAAAABP#AAAAAAAAAAAAAJAAAAIwAAAAgBAAAACQAAACQAAAAJAAAAIwAAAAoAAAAAJQEAAAAADQACSTEAwAAAAAAAAABACAAAAAAAAAAABQAAAAAnAAAAKgAAAAsBAAAAJQD#AAAAEAAAAQAAAAEAAAAiAAAAJwAAAAYBAAAAJQD#AAABEAACazIAwAAAAAAAAABAAAAAAAAAAAAAAQABP+Q9Q9Q9Q9QAAAAsAAAADAEAAAAlAAJhMwAAACkAAAArAAAALQAAAA0BAAAAJQD#AAAAAAAAAAAAAADAGAAAAAAAAAAAAAAALQ8AAf###wAAAAEAAAACAAAAAQAAAAAAAAAAAAAAAAIAAAAuAAAAAwD#####AAJBMgACYTIAAAAJAAAAIAAAAAMA#####wACQTMAAmEzAAAACQAAAC4AAAAOAP####8AAAABAAAACAAAAAABQE4AAAAAAAAAAAAIAgAAAAkAAAAwAAAAAUAgAAAAAAAAAAAACgD#####AAAAAAAQAAAAAAAAAAAAAABACAAAAAAAAAACcEIJAAAAABMAAAAyAAAADgD#####AAAAAQAAAAgAAAAAAUBZAAAAAAAAAAAACAIAAAAJAAAAMQAAAAE#8AAAAAAAAAAAAAoA#####wEAAAAAEAAAAAAAAAAAAAAAQAgAAAAAAAAAAnBDCQAAAAAzAAAANAAAAAsA#####wAAAAAAEAAAAQAEW0FCXQACAAAAEwAAADMAAAALAP####8BAAAAABAAAAEABFtCQ10AAgAAADMAAAA1#####wAAAAEAC0NNZWRpYXRyaWNlAP####8Bf39#ABAAAAEACm1lZGlhdHJpY2UAAgAAABMAAAAz#####wAAAAEAB0NNaWxpZXUA#####wF#AAAAEAAAAAAAAAAAAAAAQAgAAAAAAAAAAAgAAAAANQAAADMAAAAQAP####8BfwAAABAAAAAAAAAAAAAAAEAIAAAAAAAAAAAIAAAAADMAAAAT#####wAAAAEACUNEcm9pdGVBQgD#####AX9#fwAQAAABAAZwaWVnZTEAAgAAADoAAAA5AAAAEAD#####AX8AAAAQAAAAAAAAAAAAAABACAAAAAAAAAAACAAAAAATAAAAOgAAABAA#####wF#AAAAEAAAAAAAAAAAAAAAQAgAAAAAAAAAAAgAAAAAPAAAADr#####AAAAAQAWQ0Ryb2l0ZVBlcnBlbmRpY3VsYWlyZQD#####AX9#fwAQAAABAARwZXJwAAIAAAA9AAAANgAAAAIA#####wAAAP8AEAABQQAAAAAAAAAAAEAIAAAAAAAAAAAIAAHANP#######kCADwo9cKPXAAAAAgD#####AAAA#wAQAAFCAAAAAAAAAAAAQAgAAAAAAAAAAAgAAcAX#######4QIAHCj1wo9f#####AAAAAQAJQ0xvbmd1ZXVyAP####8AAAA#AAAAQAAAAAMA#####wABUgABMQAAAAE#8AAAAAAAAP####8AAAACAAlDQ2VyY2xlT1IA#####wEAAP8AAAACAAAAEwAAAAkAAABCAP####8AAAABAAxDQmlzc2VjdHJpY2UA#####wEAAP8AEAAAAQAAAAIAAAA1AAAAMwAAADwAAAAUAP####8BAAD#AAAAAgAAADMAAAAJAAAAQgD#####AAAAAQAQQ0ludERyb2l0ZUNlcmNsZQD#####AAAARAAAAEX#####AAAAAQAQQ1BvaW50TGllQmlwb2ludAD#####AQAA#wAQAAAAAAAAAAAAAABACAAAAAAAAAAACAABAAAARgAAABcA#####wEAAP8AEAAAAAAAAAAAAAAAQAgAAAAAAAAAAAgAAgAAAEYAAAASAP####8BAAD#ABAAAAEAAAACAAAANQAAADgAAAAUAP####8BAAD#AAAAAgAAADUAAAAJAAAAQgAAAAAWAP####8AAABJAAAASgAAABcA#####wEAAP8AEAAAAAAAAAAAAAAAQAgAAAAAAAAAAAgAAQAAAEsAAAAXAP####8BAAD#ABAAAAAAAAAAAAAAAEAIAAAAAAAAAAAIAAIAAABLAAAADgD#####AAAAAf####8AAAABAAxDTW9pbnNVbmFpcmUAAAABQE4AAAAAAAAAAAAKAP####8BAAAAABAAAAAAAAAAAAAAAEAIAAAAAAAAAAJwRAkAAAAAEwAAAE4AAAALAP####8BAAAAABAAAAEABFtDRF0AAgAAADUAAABPAAAACwD#####AQAAAAAQAAABAARbREFdAAIAAABPAAAAEwAAABAA#####wEAAAAAEAAAAAAAAAAAAAAAQAgAAAAAAAAAAAgAAAAATwAAABMAAAARAP####8Bf39#ABAAAAEABnBpZWdlMgACAAAAUgAAADkAAAAVAP####8BAAD#ABAAAAEAAAACAAAAEwAAAE8AAAA1AAAAFAD#####AQAA#wAAAAIAAABPAAAACQAAAEIAAAAAFgD#####AAAAVAAAAFUAAAAXAP####8BAAD#ABAAAAAAAAAAAAAAAEAIAAAAAAAAAAAIAAEAAABWAAAAFwD#####AQAA#wAQAAAAAAAAAAAAAABACAAAAAAAAAAACAACAAAAVgAAABUA#####wEAAP8AEAAAAQAAAAIAAABPAAAAEwAAADMAAAAWAP####8AAABZAAAAQwAAABcA#####wEAAP8AEAAAAAAAAAAAAAAAQAgAAAAAAAAAAAgAAQAAAFoAAAAXAP####8BAAD#ABAAAAAAAAAAAAAAAEAIAAAAAAAAAAAIAAIAAABaAAAAEAD#####AX8AAAAQAAAAAAAAAAAAAABACAAAAAAAAAAACAAAAAA1AAAATwAAABAA#####wF#AAAAEAAAAAAAAAAAAAAAQAgAAAAAAAAAAAgAAAAAXQAAAE8AAAARAP####8Bf39#ABAAAAEABnBpZWdlMwACAAAAXgAAADoAAAAUAP####8Bf38AAAJtMQACAAAAPAAAAAE#4AAAAAAAAAAAAAAQAP####8Bf38AABAAAAAAAAAAAAAAAEAIAAAAAAAAAAAIAAAAADoAAAAzAAAAFAD#####AX9#AAACbTIAAgAAAGEAAAABP+AAAAAAAAAAAAAAFAD#####AX9#AAAAAAIAAAA6AAAAAT#jMzMzMzMzAAAAABYA#####wAAADgAAABjAAAAFwD#####AX9#AAAQAAAAAAAAAAAAAABACAAAAAAAAAAACAABAAAAZAAAABcA#####wF#fwAAEAAAAAAAAAAAAAAAQAgAAAAAAAAAAAgAAgAAAGQAAAAWAP####8AAAA2AAAAYwAAABcA#####wF#fwAAEAAAAAAAAAAAAAAAQAgAAAAAAAAAAAgAAgAAAGcAAAAXAP####8Bf38AABAAAAAAAAAAAAAAAEAIAAAAAAAAAAAIAAEAAABn#####wAAAAEAEENEcm9pdGVQYXJhbGxlbGUA#####wF#fwAAEAAAAQAAAAIAAABpAAAAOAAAABkA#####wF#fwAAEAAAAQAAAAIAAABmAAAANv####8AAAABABBDSW50RHJvaXRlRHJvaXRlAP####8Bf38AABAAAAAAAAAAAAAAAEAIAAAAAAAAAAAIAAAAAGoAAABrAAAACwD#####AX9#AAAQAAABAAJhMQACAAAAZgAAAGwAAAALAP####8Bf38AABAAAAEAAmEyAAIAAABsAAAAaf####8AAAABAAhDVmVjdGV1cgD#####AX9#AAAQAAABAAAAAgAAADoAAAA9AP####8AAAABAA1DVHJhbnNQYXJWZWN0AP####8AAABvAAAACgD#####AX9#AAAQAAAAAAAAAAAAAABACAAAAAAAAAAACAAAAABmAAAAcAAAAAoA#####wF#fwAAEAAAAAAAAAAAAAAAQAgAAAAAAAAAAAgAAAAAbAAAAHAAAAAKAP####8Bf38AABAAAAAAAAAAAAAAAEAIAAAAAAAAAAAIAAAAAGkAAABwAAAACwD#####AX9#AAAQAAABAAJhMwACAAAAcQAAAHIAAAALAP####8Bf38AABAAAAEAAmE0AAIAAAByAAAAcwAAABIA#####wF#f38AEAAAAQAEbWVkMgACAAAAUgAAAFEAAAAUAP####8BfwAAAAAAAgAAAFIAAAABP+MzMzMzMzMAAAAAFgD#####AAAAUQAAAHcAAAAXAP####8BfwAAABAAAAAAAAAAAAAAAEAIAAAAAAAAAAAIAAIAAAB4AAAAFwD#####AX8AAAAQAAAAAAAAAAAAAABACAAAAAAAAAAACAABAAAAeAAAABYA#####wAAAHYAAAB3AAAAFwD#####AX8AAAAQAAAAAAAAAAAAAABACAAAAAAAAAAACAACAAAAewAAABcA#####wF#AAAAEAAAAAAAAAAAAAAAQAgAAAAAAAAAAAgAAQAAAHsAAAAZAP####8Bf38AABAAAAEAAAACAAAAeQAAAHYAAAAZAP####8Bf38AABAAAAEAAAACAAAAfAAAAFEAAAAaAP####8Bf38AABAAAAAAAAAAAAAAAEAIAAAAAAAAAAAIAAAAAH4AAAB#AAAACwD#####AX9#AAAQAAABAAJhNQACAAAAgAAAAHwAAAALAP####8Bf38AABAAAAEAAmE2AAIAAACAAAAAeQAAABAA#####wF#fwAAEAAAAAAAAAAAAAAAQAgAAAAAAAAAAAgAAAAATwAAAFIAAAAQAP####8Bf38AABAAAAAAAAAAAAAAAEAIAAAAAAAAAAAIAAAAAFIAAAATAAAAFAD#####AX9#AAACbTMACQAAAIMAAAABP9MzMzMzMzMAAAAAFAD#####AX9#AAACbTQACQAAAIQAAAABP9MzMzMzMzMA#####wAAAAIADENDb21tZW50YWlyZQD#####AQAA#wEAAUQAAABYEAAAAAAAAQAAAAEAAAABAAAAAAAAAAAAAUQAAAAdAP####8AAAD#AQABQQAAAFwQAAAAAAABAAAAAQAAAAEAAAAAAAAAAAABQQAAAB0A#####wAAAP8BAAFCAAAASBAAAAAAAAEAAAABAAAAAQAAAAAAAAAAAAFCAAAAHQD#####AQAA#wEAAUMAAABNEAAAAAAAAQAAAAEAAAABAAAAAAAAAAAAAUMAAAALAP####8BAAAAABAAAAEABFtCRF0AAgAAADMAAABPAAAAFAD#####AX9#AAACbTUAAgAAAIQAAAABP+AAAAAAAAAAAAAAFAD#####AX9#AAACbTYAAgAAAIMAAAABP+AAAAAAAAAAAAAAFAD#####AX9#AAACbTcACQAAAGEAAAABP9MzMzMzMzMAAAAAFAD#####AX9#AAACbTgACQAAADwAAAABP9MzMzMzMzMA#####wAAAAEADUNTZWdtZW50Q2xvbmUA#####wEAAP8ADQAAAQAFY1tBQl0ACwAAABMAAAAzAAAANgAAAB4A#####wEAAP8ADQAAAQAFY1tCQ10ACwAAADMAAAA1AAAANwAAAB4A#####wEAAP8ADQAAAQAFY1tDRF0ACwAAADUAAABPAAAAUAAAAB4A#####wEAAP8ADQAAAQAFY1tEQV0ACwAAAE8AAAATAAAAUQAAAB4A#####wEAAP8ADQAAAQAFY1tCRF0ACwAAADMAAABPAAAAi#####8AAAABAAxDRHJvaXRlQ2xvbmUA#####wEAAP8AEAAAAQALY21lZGlhdHJpY2UACwAAADgAAAAfAP####8BAAD#ABAAAAEABWNtZWQyAAsAAAB2AAAAHwD#####AQAA#wAQAAABAAVjcGVycAALAAAAPgAAAB8A#####wEAAP8AEAAAAQAHY3BpZWdlMQALAAAAOwAAAB8A#####wEAAP8AEAAAAQAHY3BpZWdlMwALAAAAXwAAAB8A#####wEAAP8AEAAAAQAHY3BpZWdlMgALAAAAU#####8AAAABAAlDQ2VyY2xlT0EA#####wEAAAAAAAABAAAAAQAAAAIAAABB##########8='
    svgId = j3pGetNewId()
    stor.svgId = svgId
    const yy = j3pCreeSVG(stor.lesdiv.zonefig, { id: svgId, width: 320, height: 320 })
    yy.style.border = 'solid black 1px'
    stor.mtgAppLecteur.removeAllDoc()
    stor.mtgAppLecteur.addDoc(svgId, txtFigure, true)
    stor.mtgAppLecteur.giveFormula2(svgId, 'A1', j3pGetRandomInt(0, 359), false)
    stor.mtgAppLecteur.giveFormula2(svgId, 'A2', j3pGetRandomInt(0, 10), false)
    stor.mtgAppLecteur.giveFormula2(svgId, 'A3', j3pGetRandomInt(0, 10), false)
    stor.mtgAppLecteur.calculateAndDisplayAll(true)
    stor.mtgAppLecteur.setText(svgId, '#A', stor.nA, true)
    stor.mtgAppLecteur.setText(svgId, '#B', stor.nB, true)

    let fa = false; let fb = false; let fc = false; let fd = false; let fBC = false; let fCD = false; let fDA = false
    let toviz = ['bc', 'cd', 'da']
    toviz = j3pShuffle(toviz)
    const am = []
    let u = ds.Elem_inutiles
    if (ds.Mode !== 'complete' && stor.Lexo.cas === 'autre') {
      am.push('da')
      toviz = j3pShuffle(['bc', 'cd'])
      u--
    }
    for (let i = 0; i < u; i++) {
      am.push(toviz.pop())
    }
    for (let i = 0; i < am.length; i++) {
      switch (am[i]) {
        case 'bc':
          fb = fc = fBC = true
          break
        case 'cd':
          fc = fd = fCD = true
          break
        case 'da':
          fd = fa = fDA = true
      }
    }
    if (fa) {
      stor.mtgAppLecteur.setVisible(svgId, '#pA', false, true)
    }
    if (fb) {
      stor.mtgAppLecteur.setVisible(svgId, '#pB', false, true)
    }
    if (fc) {
      stor.mtgAppLecteur.setText(svgId, '#C', stor.nC, true)
      if (!fCD || !fBC) {
        stor.mtgAppLecteur.setVisible(svgId, '#pC', true, true)
      }
    }
    if (fd) {
      stor.mtgAppLecteur.setText(svgId, '#D', stor.nD, true)
      if (!fCD || !fDA) {
        stor.mtgAppLecteur.setVisible(svgId, '#pD', true, true)
      }
    }
    if (fBC) {
      stor.mtgAppLecteur.setVisible(svgId, '#[BC]', true, true)
    }
    if (fCD) {
      stor.mtgAppLecteur.setVisible(svgId, '#[CD]', true, true)
    }
    if (fDA) {
      stor.mtgAppLecteur.setVisible(svgId, '#[DA]', true, true)
    }
    stor.fDA = fDA
    stor.mtgAppLecteur.updateFigure(svgId)
  }

  function yaReponse () {
    if (me.isElapsed) return true
    if (ds.Mode !== 'complete') {
      for (let i = 0; i < stor.acli.length; i++) {
        if (stor.acli[i].clicked) return true
      }
      return false
    } else {
      return stor.listeDeroulante.changed
    }
  }

  function isRepOk () {
    stor.errJus = false
    stor.errRep = false
    stor.errCoher = false

    // test existence tout elem

    stor.lesdiv.explications.classList.remove('explique')
    stor.lesdiv.solution.classList.remove('correction')

    stor.larep = 'rien'
    if (ds.Mode !== 'complete') {
      for (let i = 0; i < stor.acli.length; i++) {
        if (stor.acli[i].clicked) stor.larep = stor.acli[i].ki
      }
      return stor.larep === 'mediatrice'
    } else {
      return stor.listeDeroulante.getReponseIndex() === stor.tablp.indexOf(stor.Lexo.rand)
    }
  }

  function desactiveAll () {
    stor.blok = true
    stor.mtgAppLecteur.getDoc(svgId).defaultCursor = ''
    if (ds.Mode === 'complete') stor.listeDeroulante.disable()
  }
  function barrelesfo () {
    if (ds.Mode !== 'complete') {
      stor.mtgAppLecteur.setColor(svgId, '#' + stor.larep, 200, 20, 50, true)
    } else {
      stor.listeDeroulante.barre()
    }
  }
  function mettouvert () {
    if (ds.Mode !== 'complete') {
      stor.mtgAppLecteur.setColor(svgId, '#mediatrice', 50, 200, 50, true)
    } else {
      stor.listeDeroulante.corrige(true)
    }
  }
  function AffCorrection (bool) {
    if (ds.Mode !== 'complete') {
      switch (stor.larep) {
        case 'piege1':
        case 'piege3':
          j3pAffiche(stor.lesdiv.explications, null, 'La médiatrice doit être perpendiculaire au segment !')
          break
        case 'perp':
          j3pAffiche(stor.lesdiv.explications, null, 'La médiatrice doit passer par le milieu du segment !')
          break
        case 'med2':
          j3pAffiche(stor.lesdiv.explications, null, 'La droite que tu as choisie est la médiatrice du segment [' + stor.nD + stor.nA + '] !')
          break
        default:
          j3pAffiche(stor.lesdiv.explications, null, 'La médiatrice d’un segment est la droite perpendiculaire à ce segment, \net passant par son milieu !')
      }
    } else {
      stor.listeDeroulante.corrige(false)
      switch (stor.Lexo.rand) {
        case 'oui':
          if (stor.listeDeroulante.getReponseIndex() === 2) {
            j3pAffiche(stor.lesdiv.explications, null, "Les codes indiqués permettent d'être certain de la réponse !")
          }
          if (stor.listeDeroulante.getReponseIndex() === 3) {
            j3pAffiche(stor.lesdiv.explications, null, 'La médiatrice d’un segment passe par son milieu perpendiculairement !')
          }
          break
        case 'semble':
          if (stor.listeDeroulante.getReponseIndex() === 1) {
            j3pAffiche(stor.lesdiv.explications, null, "Les codes indiqués ne permettent pas d'être certain de la réponse !")
          }
          if (stor.listeDeroulante.getReponseIndex() === 3) {
            j3pAffiche(stor.lesdiv.explications, null, 'La médiatrice d’un segment passe par son milieu perpendiculairement !')
          }
          break
        default:
          switch (stor.Lexo.cas) {
            case 'perp':
              j3pAffiche(stor.lesdiv.explications, null, 'La droite violette ne passe pas par le milieu du segment !')
              break
            case 'autre':
              j3pAffiche(stor.lesdiv.explications, null, 'La droite violette est la médiatrice d’un autre segment !')
              break
            default:
              j3pAffiche(stor.lesdiv.explications, null, 'La médiatrice d’un segment passe par son milieu perpendiculairement !')
          }
      }
    }

    if (bool) {
      barrelesfo()
      desactiveAll()
      if (ds.Mode !== 'complete') {
        stor.mtgAppLecteur.setColor(svgId, '#mediatrice', 50, 50, 200, true)
        stor.mtgAppLecteur.setLineStyle(svgId, '#mediatrice', 0, 6, true, true)
      } else {
        const vi = ['est', 'semble être', 'n’est pas'][['oui', 'semble', 'non'].indexOf(stor.Lexo.rand)]
        j3pAffiche(stor.lesdiv.solution, null, 'La droite violette ' + vi + ' la médiatrice du segment [' + stor.nA + stor.nB + '].')
      }
    }
  }

  function enonceMain () {
    me.videLesZones()
    creeLesDiv()
    stor.Lexo = faisChoixExos()
    stor.lesdiv.travail.classList.add('travail')
    stor.lesdiv.consigne.classList.add('enonce')
    stor.lesdiv.explications.classList.remove('explique')
    stor.lesdiv.solution.classList.remove('correction')
    makefig()
    poseQuestion()
    me.finEnonce()
  }

  switch (this.etat) {
    case 'enonce':
      // code exécuté au lancement de la section
      if (me.debutDeLaSection) {
        initSection()
      } else {
        enonceMain()
      }
      break // case "enonce":

    case 'correction':
      // On teste si une réponse a été saisie
      stor.yaexplik = false
      if (stor.lesdiv === undefined) return
      stor.lesdiv.explications.innerHTML = ''
      stor.lesdiv.correction.innerHTML = ''
      stor.yaco = false

      if (yaReponse()) {
        // Bonne réponse
        if (isRepOk()) {
          this._stopTimer()
          stor.lesdiv.correction.style.color = this.styles.cbien
          stor.lesdiv.correction.innerHTML = cBien

          mettouvert()
          desactiveAll()
          this.cacheBoutonValider()
          this.score++

          this.typederreurs[0]++
          this.etat = 'navigation'
          this.sectionCourante()
        } else {
          // Pas de bonne réponse
          stor.lesdiv.correction.style.color = this.styles.cfaux
          stor.lesdiv.correction.innerHTML = cFaux
          if (this.isElapsed) {
            // A cause de la limite de temps :
            this._stopTimer()

            stor.lesdiv.correction.innerHTML += tempsDepasse + '<BR>'
            this.typederreurs[10]++
            this.cacheBoutonValider()

            AffCorrection(true)

            this.etat = 'navigation'
            this.sectionCourante()
          } else {
            // Réponse fausse :
            if (me.essaiCourant < me.donneesSection.nbchances) {
              stor.lesdiv.correction.innerHTML += '<br>' + essaieEncore

              AffCorrection(false)

              this.typederreurs[1]++
            } else {
              // Erreur au nème essai
              this._stopTimer()
              this.cacheBoutonValider()

              AffCorrection(true)

              if (stor.encours === 'p') {
                stor.pose = false
                stor.encours = 't'
              }

              this.typederreurs[2]++
              this.etat = 'navigation'
              this.sectionCourante()
            }
          }
        }
      } else {
        // pas de réponse
        stor.lesdiv.correction.style.color = this.styles.cfaux
        stor.lesdiv.correction.innerHTML = reponseIncomplete
        me.afficheBoutonValider()
      }

      if (stor.yaexplik) stor.lesdiv.explications.classList.add('explique')
      if (stor.yaco) stor.lesdiv.solution.classList.add('correction')

      // Obligatoire
      this.finCorrection()
      break // case "correction":

    case 'navigation':
      if (!this.sectionTerminee()) {
        this.etat = 'enonce'
      }
      this.finNavigation()
      break // case "navigation":
  }
}
