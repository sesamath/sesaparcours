import $ from 'jquery'
import { j3pArrondi, j3pDetruit, j3pGetRandomInt, j3pShowError, j3pShuffle, j3pEmpty, j3pAddElt, j3pGetNewId, j3pAddContent, j3pGetRandomElt, j3pNotify } from 'src/legacy/core/functions'
import { colorKo, colorOk, colorCorrection } from 'src/legacy/core/StylesJ3p'
import BoutonStyleMathquill from 'src/legacy/outils/boutonsStyleMatquill'
import BulleAide from 'src/legacy/outils/bulleAide/BulleAide'
import ListeDeroulante from 'src/legacy/outils/listeDeroulante/ListeDeroulante'
import { addDefaultTable, addTable, getCells } from 'src/legacy/themes/table'
import { j3pAffiche } from 'src/lib/mathquill/functions'
import { j3pCreeSVG } from 'src/legacy/core/functionsSvg'
import ZoneStyleMathquill1 from 'src/legacy/outils/zoneStyleMathquill/ZoneStyleMathquill1'
import { barreZone } from 'src/legacy/outils/zoneStyleMathquill/functions'
import textesGeneriques from 'src/lib/core/textes'

const { cBien, cFaux, essaieEncore, reponseIncomplete, tempsDepasse } = textesGeneriques

/**
 * Ajouter une description ici
 * @fileOverview
 * @author Tommy Barroy <t.barroy@laposte.net>
 * @since 2017-10
 * Section revue par Yves pour adaptation au clavier virtuel (10-2021)
 */

/**
 * Params exportés par la section, utilisés pour initialiser les valeurs par défaut de donneesSection et par editGraphe
 * @type {{outils: string[], parametres: ((string|number)[]|(string|string[])[]|(string|boolean)[])[]}}
 */
export const params = {
  outils: ['mathquill'],
  /**
   * Ce qui est paramétrable dans l’éditeur de graphe
   * @type {LegacySectionParamsParametres}
   */
  parametres: [
    ['nbrepetitions', 9, 'entier', 'Nombre de répétitions de la section'],
    ['nbchances', 2, 'entier', 'Nombre d’essais possibles.'],
    ['limite', 0, 'entier', 'Temps disponible par répétition, en secondes'],
    ['Type_valeur', 'les trois', 'liste', 'Le type des valeurs proposées<br><u>les trois</u>&nbsp;: tous (qualitatives, quantitatives discrètes ou continues)<br><u>Quantitative-discret-continu</u>&nbsp;: quantitatives (discrètes ou continues)<br><u>Quantitative-discret</u>&nbsp;: quantitatives et discrètes. <br><u>Quantitative-continu</u>&nbsp;: quantitatives et continues.<br><u>Qualitative</u>&nbsp;: qualitatives.', ['les trois', 'Quantitative-discret-continu', 'Quantitative-discret', 'Quantitative-continu', 'Qualitative', 'Qualitative et Quantitative-discret', 'Qualitative et Quantitative-continu']],
    ['DonneesListe', true, 'boolean', '<u>true</u>&nbsp;: Les données peuvent être présentées sous la forme d’une liste. <br><u>false</u>&nbsp;: non.'],
    ['DonneesTableau', true, 'boolean', '<u>true</u>&nbsp;: Les données peuvent être présentées sous la forme d’un tableau. <br><u>false</u>&nbsp;: non.'],
    ['DonneesGraphique', true, 'boolean', '<u>true</u>&nbsp;: Les données peuvent être présentées sous la forme d’un graphique cartésien. <br><u>false</u>&nbsp;: non.'],
    ['DonneesBatons', true, 'boolean', '<u>true</u>&nbsp;: Les données peuvent être présentées sous la forme d’un diagramme en bâtons. <br><u>false</u>&nbsp;: non.'],
    ['DonneesCirculaire', true, 'boolean', '<u>true</u>&nbsp;: Les données peuvent être présentées sous la forme d’un diagramme circulaire. <br><u>false</u>&nbsp;: non.'],
    ['ReponseTableau', true, 'boolean', '<u>true</u>&nbsp;: L’élève peut avoir à réaliser un tableau. <br><u>false</u>&nbsp;: non.'],
    ['ReponseGraphique', true, 'boolean', '<u>true</u>&nbsp;: L’élève peut avoir à réaliser un graphique cartésien. <br><u>false</u>&nbsp;: non.'],
    ['ReponseBatons', true, 'boolean', '<u>true</u>&nbsp;: L’élève peut avoir à réaliser un diagramme en bâtons. <br><u>false</u>&nbsp;: non.'],
    ['ReponseCirculaire', true, 'boolean', '<u>true</u>&nbsp;: L’élève peut avoir à réaliser un diagramme circulaire. <br><u>false</u>&nbsp;: non.'],
    ['LegendesFaites', false, 'boolean', '<u>true</u>&nbsp;: Les légendes sont complètes. <br><u>false</u>&nbsp;: L’élève doit compléter les légendes.<br><i> Ce paramètre ne concerne pas les diagrammes circulaires</i>'],
    ['GraduationsYFaites', false, 'boolean', '<i> Uniquement pour ReponseGraphique et ReponseDiagramme </i> <br><u>true</u>&nbsp;: L’axe des effectifs est déjà gradué. <br><u>false</u>&nbsp;: non.'],
    ['GraduationsXFaites', false, 'boolean', '<i> Ce paramètre ne concerne pas les tableaux </i> <br><u>true</u>&nbsp;: L’axe des valeurs est déjà complété. <br><u>false</u>&nbsp;: non.'],
    // le modèle gère la compatibilité ascendante avec l’ancien paramètre MiseEnPagePlusClaire
    ['theme', 'standard', 'liste', 'Modèle pour l’affichage', ['standard', 'zonesAvecImageDeFond']]
  ]
}

/**
 * @typedef Stat01ListItem
 * @param {string|number} valeur la valeur de cet item (string pour série quali et number pour série quanti)
 * @param {number} effectif Le nb d’élément ayant cette valeur dans la série
 */

/**
 * @typedef Stat01Serie
 * @property {number} id
 * @property {string} titre
 * @property {string} legendeX
 * @property {string} legendeY
 * @property {string[]} legendesKo
 * @property {Stat01TypesParamSerie} typeX le type du paramètre (qualitatif|discret|continu)
 * @property {string[]} [liste1] La liste des valeurs que peut prendre le paramètre dans le cas qualitatif
 * @property {Stat01LimiteX} [limiteX] pour les cas discret|continu
 * @property {Stat01LimiteY} limiteY
 */
/**
 * format de l’objet mis dans la propriété limiteY
 * @typedef Stat01LimiteY
 * @property {boolean} entier
 * @property {number} min
 * @property {number} max
 * @property {number} pas
 */

/**
 * format de l’objet mis dans la propriété limiteX (quanti)
 * @typedef Stat01LimiteX
 * @param {number} min
 * @param {number} max
 * @param {number} approx
 * @param {number} nbZerosInutiles
 * @param {number} amplitude
 * @param {string} lettre
 */
/**
 * Les types de représentation possibles pour une série
 * @typedef Stat01TypesRepresentation
 * @type {('tableau'|'graphique'|'batons'|'circulaire')}
 */
const typesRepresentation = ['tableau', 'graphique', 'batons', 'circulaire']
/**
 * Les types de séries
 * @typedef Stat01TypesParamSerie
 * @type {('qualitatif'|'discret'|'continu')}
 */
/**
 * La liste des séries statistiques pouvant être choisies
 * @type {Stat01Serie[]}
 */

const seriesPossibles = [
  // les quali
  {
    id: 'couleurYeux',
    titre: 'la couleur des yeux des élèves du collège de Sarah',
    legendeX: 'couleur des yeux',
    legendeY: 'nombre d’élèves',
    legendesKo: ['couleur des cheveux', 'marrons', 'bleus', 'peinture'],
    typeX: 'qualitatif',
    liste1: ['bleus', 'verts', 'marrons'],
    limiteY: { entier: true, min: 300, max: 400, pas: 40 }
  },
  {
    id: 'animalPrefere',
    titre: 'l’animal préféré des habitants de PetiteVille',
    legendeX: 'animal préféré',
    legendeY: 'nombre d’habitants',
    legendesKo: ['chien préféré', 'flipper', 'habitants préférés', 'Petiteville'],
    typeX: 'qualitatif',
    liste1: ['chat', 'chien', 'cheval', 'dauphin', 'poussin'],
    limiteY: { entier: true, min: 3000, max: 5000, pas: 500 }
  },
  {
    id: 'usageVelo',
    titre: 'si les personnes interrogées utilisent un vélo',
    legendeX: 'réponse',
    legendeY: 'nombre de personnes',
    legendesKo: ['nombre de vélos', 'roues', 'voiture', 'verts'],
    typeX: 'qualitatif',
    liste1: ['oui', 'non', 'sans réponse'],
    limiteY: { entier: true, min: 20, max: 30, pas: 3 }
  },
  {
    id: 'parfumGlace',
    titre: 'le parfum de glace préféré de plusieurs enfants',
    legendeX: 'parfum de glace',
    legendeY: 'nombre d’enfants',
    legendesKo: ['parfum d’enfant', 'rouge', 'glaces', 'frigo'],
    typeX: 'qualitatif',
    liste1: ['vanille', 'fraise', 'chocolat', 'menthe'],
    limiteY: { entier: true, min: 50, max: 60, pas: 5 }
  },
  {
    id: 'dernierFilm',
    titre: 'le dernier film vu par un groupe de personnes',
    legendeX: 'dernier film vu',
    legendeY: 'nombre de personnes',
    legendesKo: ['film préféré', 'pièce de théâtre', '1er film vu', 'série'],
    typeX: 'qualitatif',
    liste1: ['Star wars', 'Pirate des Caraïbes', 'Shrek', 'Harry Potter', 'Aladin'],
    limiteY: { entier: true, min: 2000, max: 3000, pas: 200 }
  },
  {
    id: 'natureTriangle',
    titre: 'la nature du dernier triangle tracé par les élèves d’une classe',
    legendeX: 'nature du triangle',
    legendeY: 'nombre d’élèves',
    legendesKo: ['nature d’élèves', 'losange', 'cercle', 'octogone'],
    typeX: 'qualitatif',
    liste1: ['équilatéral', 'isocèle', 'rectangle', 'quelconque'],
    limiteY: { entier: true, min: 20, max: 30, pas: 4 }
  },

  // quantitatif discret
  {
    id: 'nombreToupies',
    titre: 'le nombre de toupies des élèves interrogés',
    legendeX: 'nombre de toupies',
    legendeY: 'nombre d’élèves',
    legendesKo: ['nombre de jouets', 'nombre de cartes', 'force de la toupie'],
    typeX: 'discret',
    limiteX: { min: 1, max: 6 },
    limiteY: { entier: true, min: 20, max: 30, pas: 2 }
  },
  {
    id: 'nombreStylos',
    titre: 'le nombre de stylos des élèves interrogés',
    legendeX: 'nombre de stylos',
    legendeY: 'nombre d’élèves',
    legendesKo: ['nombre de jouets', 'nombre de cartes', 'couleur du stylo'],
    typeX: 'discret',
    limiteX: { min: 1, max: 5 },
    limiteY: { entier: true, min: 20, max: 30, pas: 2 }
  },
  {
    id: 'nombreCartes',
    titre: 'le nombre de carte Pokextra des élèves d’une école',
    legendeX: 'nombre de cartes',
    legendeY: 'nombre d’élèves',
    legendesKo: ['prix des cartes', 'Pikamon', 'nombre de maîtresses'],
    typeX: 'discret',
    limiteX: { min: 30, max: 35 },
    limiteY: { entier: true, min: 150, max: 260, pas: 20 }
  },
  // quantitatif continu
  {
    id: 'heureCoucher',
    titre: 'l’heure du coucher de plusieurs personnes',
    legendeX: 'heure h du coucher',
    legendeY: 'nombre de personnes',
    legendesKo: ['nombre d’heures', 'midi', 'oreiller'],
    typeX: 'continu',
    limiteX: { min: 19, max: 23, approx: 0.1, nbZerosInutiles: 0, amplitude: 1, lettre: 'h' },
    limiteY: { entier: true, min: 200, max: 300, pas: 20 }
  },
  {
    id: 'ageEleves',
    titre: 'l’âge des élèves de la classe de Charlie',
    legendeX: 'âge a (en années)',
    legendeY: 'nombre d’élèves',
    legendesKo: ['nombre de Charlie', 'âge de Charlie', 'âge de la maitresse'],
    typeX: 'continu',
    limiteX: { min: 9, max: 13, approx: 0.1, nbZerosInutiles: 0, amplitude: 1, lettre: 'a' },
    limiteY: { entier: true, min: 20, max: 30, pas: 2 }
  },

  {
    id: 'masseJouets',
    titre: 'la masse (en kg) des jouets fabriqués par une entreprise',
    legendeX: 'masse m (en kg)',
    legendeY: 'nombre de jouets',
    legendesKo: ['nombre de personnes', 'couleur des jouets', 'taille t en cm'],
    typeX: 'continu',
    limiteX: { min: 2.1, max: 2.5, approx: 0.01, nbZerosInutiles: 1, amplitude: 0.1, lettre: 'm' },
    limiteY: { entier: true, min: 1000, max: 1500, pas: 100 }
  },
  {
    id: 'taillePersonne',
    titre: 'la taille (en mètres) de personnes',
    legendeX: 'taille t (en m)',
    legendeY: 'nombre de personnes',
    legendesKo: ['âge des personnes', 'taille en km', 'nombre de géants'],
    typeX: 'continu',
    limiteX: { min: 1.3, max: 1.8, approx: 0.01, nbZerosInutiles: 2, amplitude: 0.1, lettre: 't' },
    limiteY: { entier: true, min: 20, max: 30, pas: 3 }
  },
  {
    id: 'dureeTrajet',
    titre: 'la durée (en minutes) du trajet maison-collège d’une classe',
    legendeX: 'durée d (en min)',
    legendeY: 'nombre d’élèves',
    legendesKo: ['âge des personnes', 'durée en mois'],
    typeX: 'continu',
    limiteX: { min: 5, max: 30, approx: 1, nbZerosInutiles: 0, amplitude: 5, lettre: 'd' },
    limiteY: { entier: true, min: 30, max: 50, pas: 4 }
  }
]

const textes = {
  regardecp: 'Regarde la correction',

  type: ['la liste', 'le diagramme en bâton', 'le graphique cartésien', 'le diagramme circulaire'],
  choisir: 'Choisir',
  grady: 'Les graduations verticales doivent être régulières !',
  gradypetit: 'L’unité choisie ne permet pas de placer le plus grand effectif !',
  manqueval: 'Je ne trouve pas la valeur £a !',
  manquevals: 'Je ne trouve pas les valeurs £a !',
  et: 'et',
  valtrop: '£a n’est pas une valeur de cette série !',
  valtrops: '£a ne sont pas des valeurs de cette série !',
  effec: 'Pas bon eff £a à valeur £b',
  valdouble: 'La valeur £a ne peut pas apparaitre plusieurs fois !',
  valdoubles: 'Les valeurs £a ne peuvent pas apparaitre plusieurs fois !',
  aidediag: 'Clique sur une barre rouge <br>puis monte ou descend la souris <br>en gardant le bouton appuyé. ',
  aidegraph: 'Clique sur un cercle rouge <br>puis monte ou descend la souris <br>en gardant le bouton appuyé. '
}

// Fonctions utilitaires

function addSvg (svg, type, params) {
  const svgns = 'http://www.w3.org/2000/svg'
  const el = document.createElementNS(svgns, type)
  for (const [prop, param] of Object.entries(params)) {
    if (prop === 'text') {
      const textNode = document.createTextNode(param)
      el.appendChild(textNode)
    } else {
      el.setAttributeNS(null, prop, param)
    }
  }
  svg.appendChild(el)
  return el
}

function getCoordFromMouseEvent (event) {
  return { x: event.clientX, y: event.clientY }
}

function getCoordFromTouchEvent (event) {
  event.preventDefault()
  event.stopPropagation()
  return { x: event.targetTouches[0].clientX, y: event.targetTouches[0].clientY }
}

class RepTab {
  /**
   *
   * @param {HTMLElement} cont
   * @param {Parcours} parcours
   */
  constructor (cont, parcours) {
    this.parcours = parcours
    this.ds = parcours.donneesSection
    this.stor = parcours.storage

    // on crée les listeners utilisés sur les boutons
    this.listenerAjoute = this.ajoute.bind(this)
    this.listenerSupprime = this.supprime.bind(this)

    this.contB = cont
    /** @type {ListeDeroulante[]} */
    this.listesDeroulantes = []
    this.colors = [['black', 'black'], ['black', 'black']]
    this.cont = j3pAddElt(this.contB, 'div')
    const tab = addDefaultTable(this.cont, 2, 1)
    const tabout = addDefaultTable(tab[1][0], 1, 3)
    this.divTab = tab[0][0]
    this.divBoutonA = tabout[0][0]
    tabout[0][1].style.width = '100%'
    this.divBoutons = tabout[0][2]

    let choixInitialX = textes.choisir
    let choixInitialY = textes.choisir
    let buf1 = { poss: 'null' }
    let buf2 = { poss: 'null' }
    if (this.ds.LegendesFaites) {
      choixInitialX = this.stor.serieEnCours.legendeX
      choixInitialY = this.stor.serieEnCours.legendeY
      buf1 = { poss: 'liste', laquelle: 'legendeX', choix: textes.choisir }
      buf2 = { poss: 'nb', prez: '' }
    }
    /**
     * @private
     * @type {{poss: string, laquelle: string, choix: number}[][]}
     */
    this.tableau = [[]]
    if (this.ds.GraduationsXFaites) {
      choixInitialX = this.stor.serieEnCours.legendeX
      this.tableau = [[
        { poss: 'liste', laquelle: 'legende', choix: choixInitialX },
        { poss: 'liste', laquelle: 'legende', choix: choixInitialY }
      ]]
      this.colors = []
      this.colors.push(['black', 'black'])
      for (let i = 0; i < this.stor.effectifs1.length; i++) {
        this.colors.push(['black', 'black'])
        this.tableau.push([
          { poss: 'liste', laquelle: 'legendeX', choix: this.stor.effectifs1[i].valeur },
          { poss: 'nb', prez: '' }
        ])
      }
    } else {
      // eslint-disable-next-line no-new
      new BoutonStyleMathquill(this.divBoutonA, 'boubout', 'Ajouter une colonne', this.listenerAjoute, { taille: '20px' })
      this.tableau = [
        [
          { poss: 'liste', laquelle: 'legende', choix: choixInitialX },
          { poss: 'liste', laquelle: 'legende', choix: choixInitialY }
        ],
        [buf1, buf2]]
    }
    this.majtab()
    this.parcours.finEnonce()
  }

  desactive () {
    if (this.listInput) {
      for (let i = 0; i < this.listInput.length; i++) {
        this.listInput[i].disable()
      }
    }
    for (let i = 0; i < this.listesDeroulantes.length; i++) {
      this.listesDeroulantes[i].disable()
    }
    j3pEmpty(this.divBoutonA)
    j3pEmpty(this.divBoutons)
  }

  touteffvert () {
    if (this.listInput) {
      for (let i = 0; i < this.listInput.length; i++) {
        this.listInput[i].corrige(true)
      }
    }
  }

  toutvalvert () {
    for (let i = 2; i < this.listesDeroulantes.length; i++) {
      this.listesDeroulantes[i].corrige(true)
      const tud = this.listesDeroulantes[i].tud
      const tod = this.listesDeroulantes[i].tod
      this.colors[tud][tod] = colorOk
    }
  }

  corrigeeffectifs (bool, tab, l, boolfin) {
    for (let i = 0; i < tab.length; i++) {
      for (let j = 0; j < this.listInput.length; j++) {
        if (this.listInput[j].num1 === tab[i] && this.listInput[j].num2 === l) {
          this.listInput[j].corrige(false)
          if (boolfin) this.listInput[j].barre()
        }
      }
    }
  }

  corrigeval (bool, tab, l, boolfin) {
    const col = bool ? colorOk : colorKo

    for (let i = 0; i < tab.length; i++) {
      for (let j = 2; j < this.listesDeroulantes.length; j++) {
        if (this.listesDeroulantes[j].reponse === tab[i]) {
          this.listesDeroulantes[j].corrige(bool)
          if (boolfin) this.listesDeroulantes[j].barre()
          const tud = this.listesDeroulantes[j].tud
          const tod = this.listesDeroulantes[j].tod
          this.colors[tud][tod] = col
        }
      }
    }
  }

  colorgrady () {
  }

  colorgradx () {
  }

  colorlegendey (bool, boolfin) {
    this.listesDeroulantes[1].corrige(bool)
    if (bool) {
      this.colors[0][1] = colorOk
    } else {
      this.colors[0][1] = colorKo
    }
    if (boolfin) this.listesDeroulantes[1].barre()
  }

  colorlegendex (bool, boolfin) {
    this.listesDeroulantes[0].corrige(bool)
    if (bool) {
      this.colors[0][0] = colorOk
    } else {
      this.colors[0][0] = colorKo
    }
    if (boolfin) this.listesDeroulantes[0].barre()
  }

  toutvert () {
    this.listesDeroulantes[0].corrige(true)
    this.listesDeroulantes[1].corrige(true)
    for (let i = 0; i < this.colors.length; i++) {
      this.colors[i] = [colorOk, colorOk]
    }
    this.toutvalvert()
    if (this.listInput) {
      for (let i = 0; i < this.listInput.length; i++) {
        this.listInput[i].corrige(true)
      }
    }
  }

  ajoute () {
    let ob1, ob2
    switch (this.tableau[0][0].choix) {
      case this.stor.serieEnCours.legendeY:
        ob1 = { poss: 'nb', prez: '' }
        break
      case this.stor.serieEnCours.legendeX:
        ob1 = { poss: 'liste', laquelle: 'legendeX', choix: textes.choisir }
        break
      default :
        ob1 = { poss: 'null' }
    }
    switch (this.tableau[0][1].choix) {
      case this.stor.serieEnCours.legendeY:
        ob2 = { poss: 'nb', prez: '' }
        break
      case this.stor.serieEnCours.legendeX:
        ob2 = { poss: 'liste', laquelle: 'legendeX', choix: textes.choisir }
        break
      default :
        ob2 = { poss: 'null' }
    }
    this.tableau.push([ob1, ob2])
    this.colors.push(['black', 'black'])
    this.majtab()
    j3pEmpty(this.divBoutonA)
    j3pEmpty(this.divBoutons)
    if (this.tableau.length < 8) {
      // eslint-disable-next-line no-new
      new BoutonStyleMathquill(this.divBoutons, 'boubout', 'Ajouter une colonne', this.listenerAjoute, { taille: '20px' })
    }
    if (this.tableau.length > 2) {
      this.bouts = new BoutonStyleMathquill(this.divBoutonA, 'boubout2', 'Supprimer une colonne', this.listenerSupprime, { taille: '20px' })
    }
  }

  supprime () {
    for (let i = this.listInput.length - 1; i > -1; i--) {
      this.listInput[i].disable()
      this.listInput.pop()
    }
    this.tableau.pop()
    this.majtab()
    this.colors.pop()
    j3pEmpty(this.divBoutonA)
    j3pEmpty(this.divBoutons)
    if (this.tableau.length < 8) {
      // eslint-disable-next-line no-new
      new BoutonStyleMathquill(this.divBoutons, 'boubout', 'Ajouter une colonne', this.listenerAjoute, { taille: '20px' })
    }
    if (this.tableau.length > 2) {
      this.bouts = new BoutonStyleMathquill(this.divBoutonA, 'boubout2', 'Supprimer une colonne', this.listenerSupprime, { taille: '20px' })
    }
  }

  yareponse () {
    let ok = true
    let foclist = false
    for (let i = 0; i < this.tableau.length; i++) {
      for (let j = 0; j < 2; j++) {
        switch (this.tableau[i][j].poss) {
          case 'liste':
            if (this.tableau[i][j].choix === textes.choisir) {
              ok = false
              foclist = true
            }
            break
          case 'nb':
            if (this.listInput) {
              for (let i = 0; i < this.listInput.length; i++) {
                if (this.listInput[i].reponse() === '') {
                  this.listInput[i].focus()
                  return false
                }
              }
            }
        }
      }
    }
    if (foclist) {
      for (let i = 0; i < this.listesDeroulantes.length; i++) {
        if (this.listesDeroulantes[i].reponse === textes.choisir) {
          this.listesDeroulantes[i].focus()
          break
        }
      }
    }
    return ok
  }

  reponse () {
    return { gradx: this.gradx, grady: this.grad, tablo: this.tableau, nbtraits: this.nbtraits }
  }

  gereliste (i, n, choix) {
    this.tableau[i][n].choix = choix
    let j
    if (i === 0) {
      switch (choix) {
        case this.stor.serieEnCours.legendeY:
          for (j = 1; j < this.tableau.length; j++) {
            this.tableau[j][n] = { poss: 'nb', prez: '' }
          }
          break
        case this.stor.serieEnCours.legendeX:
          for (j = 1; j < this.tableau.length; j++) {
            this.tableau[j][n] = { poss: 'liste', laquelle: 'legendeX', choix: textes.choisir }
          }
          break
        case this.stor.serieEnCours.legende2:
          for (j = 1; j < this.tableau.length; j++) {
            this.tableau[j][n] = { poss: 'liste', laquelle: 'legende2', choix: textes.choisir }
          }
          break
        default:
          for (j = 1; j < this.tableau.length; j++) {
            this.tableau[j][n] = { poss: 'null' }
          }
      }
    }

    this.majtab()
  }

  majtab () {
    for (let i = this.listesDeroulantes.length - 1; i > -1; i--) {
      this.listesDeroulantes[i].disable()
      this.listesDeroulantes.pop()
    }
    if (this.listInput) {
      for (let i = this.listInput.length - 1; i > -1; i--) {
        this.listInput[i].disable()
        this.listInput.pop()
      }
    }
    j3pEmpty(this.divTab)
    const untab2 = addTable(this.divTab, { nbLignes: 2, nbColonnes: this.tableau.length, className: 'tbStat01' })
    const untab = getCells(untab2)
    const willtab = []
    const willinput = []
    for (let i = 0; i < this.tableau.length; i++) {
      switch (this.tableau[i][0].poss) {
        case 'null':
          j3pAffiche(untab[0][i], null, '...')
          break
        case 'liste': {
          let montab = []
          let choix0
          if (this.tableau[i][0].laquelle === 'legende') {
            montab.push(this.stor.serieEnCours.legendeX)
            montab.push(this.stor.serieEnCours.legendeY)
            for (let mu = 0; mu < this.stor.serieEnCours.legendesKo.length; mu++) {
              montab.push(this.stor.serieEnCours.legendesKo[mu])
            }
          }
          if (this.tableau[i][0].laquelle === 'legendeX') {
            for (let mu = 0; mu < this.stor.effectifs1.length; mu++) {
              montab.push(this.stor.effectifs1[mu].valeur + '')
            }
          }
          montab = j3pShuffle(montab)
          if (this.tableau[i][0].choix === textes.choisir) {
            montab.splice(0, 0, textes.choisir)
            choix0 = false
          } else {
            choix0 = true
          }
          const bd = Boolean(this.ds.GraduationsXFaites)
          willtab.push({
            disabled: bd,
            col: this.colors[i][0],
            name: untab[0][i],
            tab: montab,
            param: {
              centre: true,
              choix: this.tableau[i][0].choix + '',
              choix0,
              onChange: this.gereliste.bind(this, i, 0) // choix sera le 3e arg
            },
            tud: i,
            tod: 0
          })
          break
        }
        case 'nb':
          willinput.push({
            name: untab[0][i],
            prez: this.tableau[i][0].prez,
            col: this.colors[i][0],
            num1: i,
            num2: 0
          })
          break
      }
      switch (this.tableau[i][1].poss) {
        case 'null':
          j3pAffiche(untab[1][i], null, '...')
          break
        case 'liste': {
          let montab = []
          let choix0
          if (this.tableau[i][1].laquelle === 'legende') {
            montab.push(this.stor.serieEnCours.legendeX)
            montab.push(this.stor.serieEnCours.legendeY)
            for (let mu = 0; mu < this.stor.serieEnCours.legendesKo.length; mu++) {
              montab.push(this.stor.serieEnCours.legendesKo[mu])
            }
          }
          if (this.tableau[i][1].laquelle === 'legendeX') {
            for (let mu = 0; mu < this.stor.effectifs1.length; mu++) {
              montab.push(this.stor.effectifs1[mu].valeur + '')
            }
          }
          montab = j3pShuffle(montab)
          if (this.tableau[i][1].choix === textes.choisir) {
            montab.splice(0, 0, textes.choisir)
            choix0 = false
          } else { choix0 = true }
          willtab.push({
            col: this.colors[i][1],
            name: untab[1][i],
            tab: montab,
            param: {
              centre: true,
              choix: this.tableau[i][1].choix + '',
              choix0,
              onChange: this.gereliste.bind(this, i, 1) // choix sera le 3e arg
            },
            tud: i,
            tod: 1
          })
          break
        }
        case 'nb':
          willinput.push({
            disabled: false,
            name: untab[1][i],
            prez: this.tableau[i][1].prez,
            col: this.colors[i][1],
            num1: i,
            num2: 1
          })
          break
      }
    }
    for (let i = 0; i < willtab.length; i++) {
      this.listesDeroulantes.push(ListeDeroulante.create(willtab[i].name, willtab[i].tab, willtab[i].param))
      this.listesDeroulantes[i].tud = willtab[i].tud
      this.listesDeroulantes[i].tod = willtab[i].tod
      if (willtab[i].col === colorOk) this.listesDeroulantes[this.listesDeroulantes.length - 1].corrige(true)
      if (willtab[i].col === colorKo) this.listesDeroulantes[this.listesDeroulantes.length - 1].corrige(false)
      if (willtab[i].disabled) this.listesDeroulantes[this.listesDeroulantes.length - 1].disable()
    }
    if (this.ds.LegendesFaites) {
      this.listesDeroulantes[0].disable()
      this.listesDeroulantes[1].disable()
    }
    this.listInput = []
    for (let i = 0; i < willinput.length; i++) {
      this.listInput.push(new ZoneStyleMathquill1(willinput[i].name, { limite: 5, contenu: willinput[i].prez }))
      willinput[i].name.style.color = willinput[i].col
      this.listInput[this.listInput.length - 1].num1 = willinput[i].num1
      this.listInput[this.listInput.length - 1].num2 = willinput[i].num2
      this.listInput[this.listInput.length - 1].modifL = this.getZsmModifListener()
      this.listInput[this.listInput.length - 1].li = this.listInput.length - 1
    }
  }

  getZsmModifListener () {
    const rep = this
    /** @this {ZoneStyleMathquill1} */
    return function zsmModifListener () {
      rep.tableau[this.num1][this.num2].prez = this.reponse().replace(/ /g, '')
    }
  }
} // RepTab

class RepCirc {
  /**
   *
   * @param {HTMLElement} cont
   * @param {Parcours} parcours
   */
  constructor (cont, parcours) {
    this.parcours = parcours
    this.ds = parcours.donneesSection
    this.stor = parcours.storage

    // on crée des propriétés pour nos listeners, pour pouvoir les ajouter / retirer
    // (fonctions fléchées pour éviter de faire du bind et conserver le this sur notre objet courant)
    this.listenerUp = () => {
      this.draggingElt = null
      this.linePrisDiag.style.strokeWidth = '0'
    }
    /**
     * @param {MouseEvent|TouchEvent} event
     */
    this.listenerDown = (event) => { this.draggingElt = event.currentTarget }
    this.listenerOut = (event) => { if (event.currentTarget) event.currentTarget.style.strokeWidth = '6px' }
    this.listenerOver = (event) => { if (event.currentTarget) event.currentTarget.style.strokeWidth = '10px' }
    // les listeners utilisés sur les boutons
    this.listenerAjoute = this.ajoute.bind(this)
    this.listenerSupprime = this.supprime.bind(this)

    this.listcoul = ['(116, 208, 241)', '(22, 184, 78)', '(152, 87, 23)', '(187, 11, 11)', '(218, 179, 10)', '(158, 158, 158)', '(255,255,200)', '(0,0,255)']
    this.hauteur = 250
    this.largeur = 610
    this.id = j3pGetNewId()
    let montab = []
    for (let mu = 0; mu < this.stor.effectifs1.length; mu++) {
      montab.push(this.stor.effectifs1[mu].valeur + '')
    }
    montab.push(this.stor.serieEnCours.legendeY)
    montab.push(this.stor.serieEnCours.legendeX)
    montab = j3pShuffle(montab)
    montab.splice(0, 0, textes.choisir)
    this.montab = montab
    this.first = true
    this.listesDeroulantes = []
    this.angle = 360 / (this.stor.effectifTotal)
    this.pasangle = 1
    if (this.angle < 10) {
      this.pasangle = this.stor.serieEnCours.limiteY.pas
    }

    this.cont = j3pAddElt(cont, 'div', '', { id: this.id + 'tabrep' })
    let tab = addDefaultTable(this.cont, 1, 2)
    this.eltTable = tab[0][0]
    this.eltReste = tab[0][1]
    tab = addDefaultTable(this.eltReste, 3, 1)
    this.eltZoneAide = tab[0][0]
    this.divbouta = tab[1][0]
    this.divBoutons = tab[2][0]

    /**
     * @private
     * @type {{poss: string, laquelle: string, choix: number}[][]}
     */
    this.tableau = [[]]

    if (!this.ds.GraduationsXFaites) {
      // eslint-disable-next-line no-new
      new BoutonStyleMathquill(this.divbouta, 'boubout', 'Ajouter un secteur', this.listenerAjoute, { taille: '20px' })
      this.tableau = [[
        { poss: 'liste', laquelle: 'legende', choix: this.stor.serieEnCours.legendeX },
        { poss: 'liste', laquelle: 'legende', choix: this.stor.serieEnCours.legendeY }
      ], [
        { poss: 'null', choix: textes.choisir },
        { poss: 'nb', prez: this.pasangle }
      ], [
        { poss: 'null', choix: textes.choisir },
        { poss: 'nb', prez: this.stor.effectifTotal - this.pasangle }
      ]]
    } else {
      this.tableau = [[
        { poss: 'liste', laquelle: 'legende', choix: this.stor.serieEnCours.legendeX },
        { poss: 'liste', laquelle: 'legende', choix: this.stor.serieEnCours.legendeY }
      ]]
      for (let i = 0; i < this.stor.effectifs1.length; i++) {
        this.tableau.push([
          { poss: 'null', choix: this.stor.effectifs1[i].valeur + '' },
          { poss: 'nb', prez: this.pasangle }
        ])
      }
      this.tableau[this.tableau.length - 1][1].prez = this.stor.effectifTotal - this.pasangle * (this.tableau.length - 2)
    }
    /** @type {HTMLElement|null} */
    this.draggingElt = null

    setTimeout(() => {
      this.majtab()
      this.bullaide = new BulleAide(this.eltZoneAide, textes.aidegraph)
      this.parcours.finEnonce()
    }, 100)
  }

  desactive () {
    this.bullaide.disable()
    this.eltReste.innerHTML = ''
    for (let i = 0; i < this.listesDeroulantes.length; i++) {
      this.listesDeroulantes[i].disable()
    }
    for (let i = 1; i < this.tableau.length - 1; i++) {
      this.priseDiag[i].style.visibility = 'hidden'
    }
    this.parcours.zonesElts.MG.removeEventListener('mouseup', this.listenerUp, false)
    this.parcours.zonesElts.MG.removeEventListener('touchend', this.listenerUp, false)
    this.parcours.zonesElts.MG.removeEventListener('touchcancel', this.listenerUp, false)
    this.replaceListe()
  }

  corrigeval (bool, tab, l, boolfin) {
    for (let i = 0; i < tab.length; i++) {
      for (let j = 0; j < this.listesDeroulantes.length; j++) {
        if (this.listesDeroulantes[j].reponse === tab[i]) {
          this.listesDeroulantes[j].corrige(bool)
          if (bool === false) {
            this.corrigeeffectifs(false, [j + 1])
            if (boolfin) this.listesDeroulantes[j].barre()
          }
        }
      }
    }
  }

  corrigeeffectifs (bool, tab) {
    const col = bool ? colorOk : colorKo

    for (let i = 0; i < tab.length; i++) {
      this.effDiag[tab[i]].style.stroke = col
      this.effDiag[tab[i]].style.fill = col
      this.colangle[tab[i]] = col
    }
  }

  touteffvert () {
    for (let i = 1; i < this.tableau.length; i++) {
      this.effDiag[i].style.stroke = colorOk
      this.effDiag[i].style.fill = colorOk
      this.colangle[i] = colorOk
    }
  }

  toutvalvert () {
    for (let i = 0; i < this.listesDeroulantes.length; i++) {
      this.listesDeroulantes[i].corrige(true)
    }
  }

  toutvert () {
    this.toutvalvert()
    this.touteffvert()
  }

  ajoute () {
    const ob1 = { poss: 'liste', laquelle: 'legendeX', choix: textes.choisir }
    const ob2 = { poss: 'nb', prez: this.pasangle }

    let ok = false
    for (let i = this.tableau.length - 1; i > 0; i--) {
      if (this.tableau[i][1].prez > this.pasangle) {
        this.tableau[i][1].prez -= this.pasangle
        this.colangle[i] = 'black'
        ok = true
        break
      }
    }
    if (!ok) {
      return j3pShowError('plus de place')
    }

    this.tableau.push([ob1, ob2])
    this.colangle.push('black')
    this.lalistederoul[this.tableau.length - 1] = j3pAddElt(this.eltTable, 'span', { style: { position: 'relative' } })
    this.listesDeroulantes.push(ListeDeroulante.create(this.lalistederoul[this.tableau.length - 1], this.montab, {
      centre: false,
      sensHaut: true,
      onChange: this.gereliste.bind(this, this.tableau.length - 1) // choix sera le 2e arg
    }))

    this.majtab()
    this.divbouta.innerHTML = ''
    this.divBoutons.innerHTML = ''
    if (this.tableau.length < 8) {
      // eslint-disable-next-line no-new
      new BoutonStyleMathquill(this.divbouta, 'boubout', 'Ajouter un secteur', this.listenerAjoute, { taille: '20px' })
    }
    if (this.tableau.length > 2) {
      this.bouts = new BoutonStyleMathquill(this.divBoutons, 'boubouts', 'Supprimer un secteur', this.listenerSupprime, { taille: '20px' })
    }
  }

  supprime () {
    this.listesDeroulantes[this.listesDeroulantes.length - 1].disable()
    this.listesDeroulantes.pop()

    this.tableau[this.tableau.length - 2][1].prez += this.tableau[this.tableau.length - 1][1].prez
    this.tableau.pop()
    this.colangle.pop()
    j3pDetruit(this.lalistederoul[this.tableau.length])

    this.majtab()
    this.divbouta.innerHTML = ''
    this.divBoutons.innerHTML = ''
    if (this.tableau.length < 8) {
      // eslint-disable-next-line no-new
      new BoutonStyleMathquill(this.divbouta, 'boubout', 'Ajouter un secteur', this.listenerAjoute, { taille: '20px' })
    }
    if (this.tableau.length > 3) {
      this.bouts = new BoutonStyleMathquill(this.divBoutons, 'boubouts', 'Supprimer un secteur', this.listenerSupprime, { taille: '20px' })
    }
  }

  yareponse () {
    for (let i = 1; i < this.tableau.length; i++) {
      if (this.tableau[i][0].choix === textes.choisir) {
        this.listesDeroulantes[i - 1].focus()
        return false
      }
    }
    return true
  }

  reponse () {
    return { gradx: this.gradx, grady: this.grad, tablo: this.tableau, nbtraits: this.nbtraits }
  }

  gereliste (index, choix) {
    this.tableau[index][0].choix = choix
    this.replaceListe()
  }

  majtab () {
    const hauteur = this.hauteur
    const largeur = this.largeur
    this.min = 25
    this.max = hauteur - 25
    if (!this.svg) {
      this.svg = j3pCreeSVG(this.eltTable, {
        width: largeur,
        height: (hauteur + 60),
        viewBox: '0 0 ' + largeur + ' ' + (hauteur + 60)
      })
    }
    addSvg(this.svg, 'rect', {
      x: 0,
      y: 0,
      width: largeur,
      height: hauteur + 60,
      style: 'fill:rgb(200,200,200);stroke-width:0;stroke:rgb(0,0,0)'
    })
    // det cb traits horizontaux
    let effmax = 0
    for (let i = 0; i < this.stor.effectifs1.length; i++) {
      if (effmax < this.stor.effectifs1[i].effectif) effmax = this.stor.effectifs1[i].effectif
    }
    // fleches et traits pointilles horizontaux
    addSvg(this.svg, 'circle', {
      cx: largeur / 2 - 100,
      cy: hauteur / 2 + 30,
      r: (hauteur - 10) / 2,
      style: 'fill:rgb(255, 255, 255);stroke:rgb(6,6,10);stroke-width:2;'
    })
    if (this.first) {
      this.colangle = ['']
      this.lalistederoul = []
      for (let i = 1; i < this.tableau.length; i++) {
        this.lalistederoul[i] = j3pAddElt(this.eltTable, 'span', { style: { position: 'relative' } })
        this.listesDeroulantes.push(ListeDeroulante.create(this.lalistederoul[i], this.montab, {
          centre: false,
          sensHaut: true,
          onChange: this.gereliste.bind(this, i), // choix sera le 2e arg
          choix: this.tableau[i][0].choix
        }))
        this.colangle.push('black')
        if (this.ds.GraduationsXFaites) {
          this.listesDeroulantes[this.listesDeroulantes.length - 1].disable()
        }
      }
      this.first = false
    }
    this.listangle = []
    let angle = 0
    this.setDiag = []
    this.effDiag = []
    this.priseDiag = []
    this.ptirec = []
    for (let i = 1; i < this.tableau.length; i++) {
      const angle2 = angle + (this.tableau[i][1].prez * this.angle)
      this.listangle[i] = { a1: angle, a2: angle2 }
      const lm = ((angle2 - angle) <= 180) ? '0,0,1' : '1,1,1'
      this.setDiag[i] = addSvg(this.svg, 'path', {
        d: 'M ' + ((largeur / 2 - 100) + Math.cos(angle * Math.PI / 180) * (hauteur - 10) / 2) + ' ' + ((hauteur / 2 + 30) + Math.sin(angle * Math.PI / 180) * (hauteur - 10) / 2) + '  A ' + (hauteur - 10) / 2 + ' ' + (hauteur - 10) / 2 + ', ' + lm + ', ' + ((largeur / 2 - 100) + Math.cos(angle2 * Math.PI / 180) * (hauteur - 10) / 2) + ' ' + (((hauteur / 2) + Math.sin(angle2 * Math.PI / 180) * (hauteur - 10) / 2) + 30) + ' L ' + (largeur / 2 - 100) + ' ' + (hauteur / 2 + 30) + ' Z',
        cy: hauteur / 2 + 30,
        r: (hauteur - 10) / 2,
        style: 'fill:rgb' + this.listcoul[i] + ';stroke:rgb(0,0,0);stroke-width:1;'
      })
      this.ptirec[i] = addSvg(this.svg, 'rect', {
        x: largeur / 2 + 40,
        y: 5 + i * 30,
        width: 50,
        height: 15,
        style: 'fill:rgb' + this.listcoul[i] + ';stroke-width:1;stroke:rgb(0,0,0)'
      })
      this.effDiag[i] = addSvg(this.svg, 'text', {
        x: 0,
        y: 0,
        style: 'font-size:15px;stroke:' + this.colangle[i] + ';fill:' + this.colangle[i],
        text: this.tableau[i][1].prez
      })
      if (i !== this.tableau.length - 1) {
        this.priseDiag[i] = addSvg(this.svg, 'circle', {
          cx: (largeur / 2 - 100) + Math.cos(angle2 * Math.PI / 180) * (hauteur - 10 + 14) / 2,
          cy: (hauteur / 2 + 30) + Math.sin(angle2 * Math.PI / 180) * (hauteur - 10 + 14) / 2,
          r: 7,
          style: 'fill:rgb(116, 208, 241);stroke:rgb(255,6,10);stroke-width:6;'
        })
        this.priseDiag[i].i = i
      }
      angle = angle2
    }
    this.linePrisDiag = addSvg(this.svg, 'line', {
      x1: 0,
      y1: 0,
      x2: largeur / 2 - 100,
      y2: hauteur / 2 + 30,
      style: 'stroke-width:0;stroke:red'
    })

    for (let i = 1; i < this.tableau.length; i++) {
      const el = this.effDiag[i]
      const { width, height } = el.getBoundingClientRect()
      const lestext = (this.listangle[i].a1 + this.listangle[i].a2) / 2
      this.effDiag[i].x.baseVal.getItem(0).value = (largeur / 2 - 100) + Math.cos(lestext * Math.PI / 180) * (hauteur - 10) / 3 - width / 2
      this.effDiag[i].y.baseVal.getItem(0).value = (hauteur / 2) + Math.sin(lestext * Math.PI / 180) * (hauteur - 10) / 3 + height / 2 - 2 + 27
    }
    for (let i = 1; i < this.tableau.length - 1; i++) {
      this.priseDiag[i].style.cursor = 'pointer'
      this.priseDiag[i].addEventListener('mouseout', this.listenerOut, false)
      this.priseDiag[i].addEventListener('mouseover', this.listenerOver, false)
      this.priseDiag[i].addEventListener('mousedown', this.listenerDown, false)
      this.priseDiag[i].addEventListener('touchstart', this.listenerDown, { capture: false, passive: true })
      this.priseDiag[i].addEventListener('mouseup', this.listenerUp, false)
      this.priseDiag[i].addEventListener('touchend', this.listenerUp, false)
      this.priseDiag[i].addEventListener('touchcancel', this.listenerUp, false)
    }

    this.svg.addEventListener('mousemove', (event) => { this.drag(event, getCoordFromMouseEvent(event)) }, { capture: false, passive: true })
    this.svg.addEventListener('touchmove', (event) => { this.drag(event, getCoordFromTouchEvent(event)) }, { capture: false, passive: false })
    this.svg.addEventListener('mouseup', this.listenerUp, false)
    this.svg.addEventListener('touchend', this.listenerUp, false)
    this.svg.addEventListener('touchcancel', this.listenerUp, false)
    this.parcours.zonesElts.MG.addEventListener('mouseup', this.listenerUp, false)
    this.parcours.zonesElts.MG.addEventListener('touchend', this.listenerUp, false)
    this.parcours.zonesElts.MG.addEventListener('touchcancel', this.listenerUp, false)
    this.replaceListe()
  }

  replaceListe () {
    const el = this.svg
    const { height } = el.getBoundingClientRect()
    let decalgauche = 0
    for (let i = 1; i < this.tableau.length; i++) {
      this.lalistederoul[i].style.left = (this.hauteur + 150 + decalgauche) + 'px'
      this.lalistederoul[i].style.top = (-height - 5 + i * 30) + 'px'
      const oubase = this.lalistederoul[i].getBoundingClientRect()
      decalgauche -= oubase.width
    }
  }

  /**
   * Déplace l’élément cliqué (on est appelé au mousemove|touchmove)
   * @param {MouseEvent|TouchEvent} event
   * @param {number} x position horizontale (de la souris|touch)
   * @param {number} y position verticale (de la souris|touch)
   */
  drag (event, { x, y }) {
    if (this.draggingElt === null) {
      this.linePrisDiag.style.strokeWidth = '0'
      return
    }
    this.linePrisDiag.style.strokeWidth = '6px'
    const { left, top } = this.svg.getBoundingClientRect(this.svg)

    const dy = y - top - (this.hauteur - 10) / 2
    const dx = x - left - (this.largeur / 2 - 100)

    if (dx === 0 || dy === 0) return
    let angle = Math.atan(dy / dx) * 180 / Math.PI
    if (dx === 0) angle = 90
    if (dx < 0) angle = angle + 180
    if (angle < 0) angle += 360

    let monnum = this.draggingElt.i

    const nb = Math.round(Math.round(angle) / Math.round(this.angle * this.pasangle))

    angle = Math.max(this.listangle[monnum].a1 + this.angle * this.pasangle, Math.min(this.listangle[monnum + 1].a2 - this.angle * this.pasangle, nb * this.angle * this.pasangle))

    // change les deux secteur
    // change la prise
    // change sa ligne
    try {
      this.draggingElt.cx.baseVal.value = ((this.largeur / 2 - 100) + Math.cos(angle * Math.PI / 180) * (this.hauteur - 10 + 14) / 2)
      this.draggingElt.cy.baseVal.value = ((this.hauteur / 2) + Math.sin(angle * Math.PI / 180) * (this.hauteur - 10 + 14) / 2) + 30
      this.linePrisDiag.x1.baseVal.value = ((this.largeur / 2 - 100) + Math.cos(angle * Math.PI / 180) * (this.hauteur - 10 + 14) / 2)
      this.linePrisDiag.y1.baseVal.value = ((this.hauteur / 2) + Math.sin(angle * Math.PI / 180) * (this.hauteur - 10 + 14) / 2) + 30
    } catch (e) {
      j3pNotify(e, { angle, dx, dy, nb, aat: Math.atan(dy / dx) * 180 / Math.PI })
      return
    }
    // this.draggingElt.y2.baseVal.value = ly;

    let lm = ((angle - this.listangle[monnum].a1) < 180) ? '0,0,1' : '1,1,1'
    this.setDiag[monnum].setAttributeNS(null, 'd', 'M ' + ((this.largeur / 2 - 100) + Math.cos(this.listangle[monnum].a1 * Math.PI / 180) * ((this.hauteur - 10) / 2)) + ' ' + ((((this.hauteur / 2) + Math.sin(this.listangle[monnum].a1 * Math.PI / 180) * ((this.hauteur - 10) / 2))) + 30) + '  A ' + ((this.hauteur - 10) / 2) + ' ' + ((this.hauteur - 10) / 2) + ', ' + lm + ', ' + ((this.largeur / 2 - 100) + Math.cos(angle * Math.PI / 180) * (this.hauteur - 10) / 2) + ' ' + (((this.hauteur / 2) + Math.sin(angle * Math.PI / 180) * (this.hauteur - 10) / 2) + 30) + ' L ' + ((this.largeur / 2 - 100)) + ' ' + ((this.hauteur / 2) + 30) + ' Z')
    lm = ((this.listangle[monnum + 1].a2 - angle) < 180) ? '0,0,1' : '1,1,1'
    this.setDiag[monnum + 1].setAttributeNS(null, 'd', 'M ' + ((this.largeur / 2 - 100) + Math.cos(angle * Math.PI / 180) * (this.hauteur - 10) / 2) + ' ' + (((this.hauteur / 2) + Math.sin(angle * Math.PI / 180) * (this.hauteur - 10) / 2) + 30) + '  A ' + ((this.hauteur - 10) / 2) + ' ' + ((this.hauteur - 10) / 2) + ', ' + lm + ', ' + ((this.largeur / 2 - 100) + Math.cos(this.listangle[monnum + 1].a2 * Math.PI / 180) * (this.hauteur - 10) / 2) + ' ' + (((this.hauteur / 2) + Math.sin(this.listangle[monnum + 1].a2 * Math.PI / 180) * (this.hauteur - 10) / 2) + 30) + ' L ' + ((this.largeur / 2 - 100)) + ' ' + ((this.hauteur / 2) + 30) + ' Z')

    let el = this.effDiag[monnum]
    el.innerHTML = Math.round((angle - this.listangle[monnum].a1) / this.angle)
    el.style.stroke = 'black'
    el.style.fill = 'black'
    this.colangle[monnum] = 'black'
    const { width, height } = el.getBoundingClientRect()
    let lestext = (this.listangle[monnum].a1 + angle) / 2
    this.effDiag[monnum].x.baseVal.getItem(0).value = (this.largeur / 2 - 100) + Math.cos(lestext * Math.PI / 180) * (this.hauteur - 10) / 3 - width / 2
    this.effDiag[monnum].y.baseVal.getItem(0).value = (this.hauteur / 2) + Math.sin(lestext * Math.PI / 180) * (this.hauteur - 10) / 3 + height / 2 - 2 + 27

    monnum++
    el = this.effDiag[monnum]
    el.innerHTML = Math.round((this.listangle[monnum].a2 - angle) / this.angle)
    el.style.stroke = 'black'
    el.style.fill = 'black'
    this.colangle[monnum] = 'black'
    const rect = el.getBoundingClientRect()
    lestext = (this.listangle[monnum].a2 + angle) / 2
    this.effDiag[monnum].x.baseVal.getItem(0).value = (this.largeur / 2 - 100) + Math.cos(lestext * Math.PI / 180) * (this.hauteur - 10) / 3 - rect.width / 2
    this.effDiag[monnum].y.baseVal.getItem(0).value = (this.hauteur / 2) + Math.sin(lestext * Math.PI / 180) * (this.hauteur - 10) / 3 + rect.height / 2 - 2 + 27

    this.tableau[monnum - 1][1].prez = Math.round((angle - this.listangle[monnum - 1].a1) / (this.angle))
    this.tableau[monnum][1].prez = Math.round((this.listangle[monnum].a2 - angle) / (this.angle))
    this.listangle[monnum - 1].a2 = angle
    this.listangle[monnum].a1 = angle
  }
}

class RepDiag {
  /**
   *
   * @param {HTMLElement} cont
   * @param {Parcours} parcours
   */
  constructor (cont, parcours) {
    this.parcours = parcours
    this.ds = parcours.donneesSection
    this.stor = parcours.storage

    this.bony = false
    this.bonx = false
    this.id = j3pGetNewId()
    this.disabled = false
    this.first = true
    this.listesDeroulantes = []
    this.gradx = []
    this.cont = j3pAddElt(cont, 'div', '', { id: this.id + 'tabrep' })
    let tab = addDefaultTable(this.cont, 1, 2)
    this.eltTable = tab[0][0]
    this.eltReste = tab[0][1]
    tab = addDefaultTable(this.eltReste, 5, 1)
    this.eltZoneAide = tab[0][0]
    this.eltZoneX = tab[1][0]
    this.eltZoneY = tab[2][0]
    this.divbouta = tab[3][0]
    this.divBoutons = tab[4][0]
    j3pAffiche(this.eltZoneX, null, 'Légende en abscisse&nbsp;: \n')
    j3pAffiche(this.eltZoneY, null, 'Légende en ordonnée&nbsp;: \n')

    let choixInitialX = textes.choisir
    let choixInitialY = textes.choisir
    if (this.ds.LegendesFaites) {
      choixInitialX = this.stor.serieEnCours.legendeX
      choixInitialY = this.stor.serieEnCours.legendeY
    }
    if (this.ds.GraduationsYFaites) {
      choixInitialY = this.stor.serieEnCours.legendeY
    }
    if (this.ds.GraduationsXFaites) {
      choixInitialX = this.stor.serieEnCours.legendeX
    }

    // on mélange…
    const montab = j3pShuffle([
      // les deux bonnes réponses x & y
      this.stor.serieEnCours.legendeX,
      this.stor.serieEnCours.legendeY,
      // et toutes les mauvaises
      ...this.stor.serieEnCours.legendesKo
    ])
    // et on ajoute choisir au début
    montab.unshift(textes.choisir)
    this.listex = ListeDeroulante.create(this.eltZoneX, montab, {
      choix: choixInitialX,
      onChange: this.gerelistex.bind(this)
    })
    this.listey = ListeDeroulante.create(this.eltZoneY, montab, {
      choix: choixInitialY,
      onChange: this.gerelistey.bind(this)
    })

    if (this.ds.LegendesFaites) {
      this.listex.disable()
      this.listey.disable()
    }
    if (this.ds.GraduationsYFaites) {
      this.listey.disable()
    }
    if (this.ds.GraduationsXFaites) {
      this.listex.disable()
    }

    this.colors = []
    this.colors.push('')

    /**
     * @private
     * @type {{poss: string, laquelle: string, choix: number}[][]}
     */
    this.tableau = [[]]

    if (this.stor.serieEnCours.typeX.indexOf('continu') === -1) {
      if (this.ds.GraduationsXFaites) {
        this.colors[1] = { fi: 'rgb(116, 208, 241)', st: 'rgb(6,6,10);', ba: 'rgb(200,0,0)' }
        this.tableau = [[
          { poss: 'liste', laquelle: 'legende', choix: choixInitialX },
          { poss: 'liste', laquelle: 'legende', choix: choixInitialY }
        ]]
        for (let i = 0; i < this.stor.effectifs1.length; i++) {
          this.colors.push({ fi: 'rgb(116, 208, 241)', st: 'rgb(6,6,10);', ba: 'rgb(200,0,0)' })
          this.tableau.push([
            { poss: 'liste', laquelle: 'legende', choix: this.stor.effectifs1[i].valeur },
            { poss: 'nb', prez: 0 }
          ])
        }
      } else {
        this.colors[1] = { fi: 'rgb(116, 208, 241)', st: 'rgb(6,6,10);', ba: 'rgb(200,0,0)' }
        this.tableau = [[
          { poss: 'liste', laquelle: 'legende', choix: choixInitialX },
          { poss: 'liste', laquelle: 'legende', choix: choixInitialY }
        ], [
          { poss: 'null' },
          { poss: 'nb', prez: 0 }
        ]]
      }
    } else {
      if (this.ds.GraduationsXFaites) {
        for (let i = 0; i < this.stor.effectifs1.length + 1; i++) {
          this.gradx.push(j3pArrondi(this.stor.serieEnCours.limiteX.min + i * this.stor.serieEnCours.limiteX.amplitude, 5))
        }
      } else {
        for (let i = 0; i < this.stor.effectifs1.length + 1; i++) {
          this.gradx.push('')
        }
      }
      this.tableau = [[
        { poss: 'liste', laquelle: 'legende', choix: choixInitialX },
        { poss: 'liste', laquelle: 'legende', choix: choixInitialY }
      ]]
      for (let i = 0; i < this.stor.effectifs1.length; i++) {
        this.tableau.push([
          { poss: 'nb', prez: '' },
          { poss: 'nb', prez: (i + 1) % 2 }
        ])
        this.colors.push({ fi: 'rgb(116, 208, 241)', st: 'stroke:rgb(6,6,10);' })
      }
    }
    this.draggingElt = null

    // on crée des propriétés pour nos listeners, pour pouvoir les ajouter / retirer
    // (fonctions fléchées pour éviter de faire du bind et conserver le this sur notre objet courant)
    this.listenerOut = (event) => {
      if (event.currentTarget) event.currentTarget.style.strokeWidth = '6px'
    }
    this.listenerOver = (event) => {
      if (!this.disabled && event.currentTarget) event.currentTarget.style.strokeWidth = '10px'
    }
    this.listenerDown = (event) => {
      this.draggingElt = event.currentTarget
    }
    this.listenerUp = () => {
      this.draggingElt = null
    }
    // on crée les listeners utilisés sur les boutons
    this.listenerAjoute = this.ajoute.bind(this)
    this.listenerSupprime = this.supprime.bind(this)

    // le listener de modifL (zsm) peut pas être une fct fléchée car on a besoin de son this
    // on en crée un ici plutôt que le recréer à chaque appel de gerelistex (et chaque tour de boucle dedans)
    const rep = this
    /** @this {ZoneStyleMathquill1} */
    this.onModifL = function onModifL () {
      // this est zsm
      const num = this.li
      rep.gradx[num - 1] = this.reponse().replace(/ /g, '')
      const { left } = rep.svg.getBoundingClientRect()
      const { width } = rep.lalistederoul[num].getBoundingClientRect()
      const { left: mgLeft } = rep.parcours.zonesElts.MG.getBoundingClientRect()
      rep.lalistederoul[num].style.left = (left + 35 + rep.intervalle + (num - 1) * this.intervalle - width / 2 - mgLeft) + 'px'
    }

    // @todo virer ce setTimeout et réagir à un rappel quand c’est prêt
    setTimeout(() => {
      this.majtab()
      if (this.ds.LegendesFaites) {
        this.gerelistex(choixInitialX)
        this.gerelistey(choixInitialY)
      }
      if (this.ds.GraduationsYFaites) {
        this.gerelistey(choixInitialY)
      }
      if (this.ds.GraduationsXFaites) {
        this.gerelistex(choixInitialX)
      }
      if ((this.stor.serieEnCours.typeX.indexOf('continu') === -1) && (!this.ds.GraduationsXFaites)) {
        // eslint-disable-next-line no-new
        new BoutonStyleMathquill(this.divbouta, 'boubout', 'Ajouter un bâton', this.listenerAjoute, { taille: '20px' })
      }
      this.bullaide = new BulleAide(this.eltZoneAide, textes.aidediag)

      this.parcours.finEnonce()
    }, 100)
  }

  desactive () {
    this.listex.disable()
    this.listey.disable()
    const svg = this.svg
    const fontZone = svg.style.fontFamily
    const tailleFontZone = svg.style.fontSize
    const ns = 'http://www.w3.org/2000/svg'
    if (this.bony) {
      if (!this.ds.GraduationsYFaites) {
        for (let i = 0; i < this.grad.length; i++) {
          const fg = this.zoneInputY[i].reponse().replace(/ /g, '')
          this.zoneInputY[i].disable()
          j3pDetruit(this.gradinput[i])
          const tt = document.createElementNS(ns, 'text')
          // FIXME, textWidth ajoute un span pour calculer une largeur, dans un svg ça doit marcher moyen, et probablement planter ou remonter n’importe quoi, au moins sur certains navigateurs
          // faire du $(svg.parentNode).textWidth(…) va donner la taille dans du html, mais ça peut être différent dans le svg (suivant son scale)
          // @todo il faudrait plutôt créer une nouvelle fct textWidthInSvg(svg, text) qui mesurerait la taille du texte dans ce svg, demander à Yves qui a déjà fait ça dans mathgraph
          const newTailleZone = Math.max($(this.svg).textWidth(fg, fontZone, tailleFontZone), 5)
          tt.setAttributeNS(null, 'x', 42 - newTailleZone)
          tt.setAttributeNS(null, 'y', (250 - 25 - (i + 1) * (200 / this.nbtraits)) + 6)
          tt.setAttributeNS(null, 'font-size', '15')
          this.gradinput[i] = tt
          const textNode = document.createTextNode(fg)
          tt.appendChild(textNode)
          svg.appendChild(tt)
        }
      }
    }
    if (this.bonx) {
      if (this.stor.serieEnCours.typeX.indexOf('continu') !== -1) {
        for (let i = 1; i < this.gradx.length + 1; i++) {
          const fg = this.gradinputX[i].reponse().replace(/ /g, '')
          this.gradinputX[i].disable()
          const ns = 'http://www.w3.org/2000/svg'
          const tt = document.createElementNS(ns, 'text')
          const newTailleZone = Math.max($(svg).textWidth(fg, fontZone, tailleFontZone), 5)
          tt.setAttributeNS(null, 'x', (37 + this.intervalle + (i - 1) * this.intervalle) - newTailleZone / 2)
          tt.setAttributeNS(null, 'y', this.hauteur - 10)
          tt.setAttributeNS(null, 'font-size', '15')
          this.gradinputX[i] = tt
          const textNode = document.createTextNode(fg)
          tt.appendChild(textNode)
          svg.appendChild(tt)
        }
      }
    }
    if (this.stor.serieEnCours.typeX.indexOf('continu') === -1) {
      for (let i = 0; i < this.listesDeroulantes.length; i++) {
        this.listesDeroulantes[i].disable()
        const fg = this.listesDeroulantes[i].reponse
        j3pEmpty(this.lalistederoul[i + 1])
        const tt = document.createElementNS(ns, 'text')
        const newTailleZone = Math.max($(svg).textWidth(fg, fontZone, tailleFontZone), 5)
        tt.setAttributeNS(null, 'x', (37 + 1.5 * this.intervalle + i * 2 * this.intervalle) - newTailleZone / 2)
        tt.setAttributeNS(null, 'y', this.hauteur - 1 - 10 * (i % 2))
        tt.setAttributeNS(null, 'font-size', '15')
        this.lalistederoul[i] = tt
        const textNode = document.createTextNode(fg)
        tt.appendChild(textNode)
        svg.appendChild(tt)
        this.listesDeroulantes[i].pourCo = this.lalistederoul[i]
      }
    }
    this.disabled = true
    this.bullaide.disable()
    j3pEmpty(this.eltReste)
  } // desactive

  corrigeval (bool, tab) {
    for (const [i, elt] of tab.entries()) {
      for (const [j, liste] of this.listesDeroulantes.entries()) {
        if (liste.reponse === elt) {
          if (!this.disabled) {
            liste.corrige(bool)
          } else {
            let zell = colorOk
            if (!bool) zell = colorKo
            this.listesDeroulantes[i].pourCo.style.fill = zell
          }
          if (bool === false) {
            this.corrigeeffectifs(false, [j + 1])
          }
        }
      }
    }
  }

  corrigeeffectifs (isOk, tab) {
    const col = isOk
      ? {
          fi: 'rgb(100, 210, 200)',
          st: colorOk,
          ba: colorOk
        }
      : {
          fi: 'rgb(255, 122, 41)',
          st: colorKo,
          ba: colorKo
        }

    for (const index of tab) {
      this.linediag[index].style.stroke = col.ba
      this.batondiag[index].style.stroke = col.st
      this.batondiag[index].style.fill = col.fi
      this.colors[index] = col
    }
  }

  toutvalvert () {
    for (let i = 0; i < this.listesDeroulantes.length; i++) {
      if (!this.disabled) {
        this.listesDeroulantes[i].corrige(true)
      } else {
        this.listesDeroulantes[i].pourCo.style.fill = colorOk
      }
    }
  }

  touteffvert () {
    for (let i = 1; i < this.tableau.length; i++) {
      this.linediag[i].style.stroke = colorOk
      this.linediag[i].style.fill = 'rgb(100, 210, 200)'
      this.linediag[i].style.stroke = colorOk
      this.linediag[i].style.fill = 'rgb(100, 210, 200)'
      this.colors[i] = { fi: 'rgb(100, 210, 200)', st: colorOk, ba: colorOk }
    }
  }

  colorgrady (bool, qui) {
    if (this.bony) {
      const col = bool ? colorOk : colorKo

      for (let i = 0; i < qui.length; i++) {
        this.gradinput[qui[i]].style.fill = col
        this.gradinput[qui[i]].style.color = col
      }
    }
  }

  colorgradx (bool, qui) {
    if (this.stor.serieEnCours.typeX.indexOf('continu') !== -1) {
      if (this.bonx) {
        const col = bool ? colorOk : colorKo

        for (let i = 0; i < qui.length; i++) {
          this.lalistederoul[qui[i]].style.fill = col
          this.lalistederoul[qui[i]].style.color = col
        }
      }
    }
  }

  colorlegendey (bool) {
    const col = bool ? colorOk : colorKo

    this.dtextly.style.stroke = col
    this.dtextly.style.fill = col
    this.listey.corrige(bool)
  }

  colorlegendex (bool) {
    const col = bool ? colorOk : colorKo

    this.dtextl1.style.stroke = col
    this.dtextl1.style.fill = col
    this.listex.corrige(bool)
  }

  toutvert () {
    this.colorlegendex(true)
    this.colorlegendey(true)
    if (this.bony) {
      const titab = []
      for (let i = 0; i < this.nbtraits - 1; i++) {
        titab[i] = i
      }
      this.colorgrady(true, titab)
    }
    if (this.bonx) {
      const titab = []
      for (let i = 0; i < this.tableau.length; i++) {
        titab[i] = i + 1
      }
      this.colorgradx(true, titab)
    }
    this.toutvalvert()
    const yy = []
    for (let i = 0; i < this.tableau.length - 1; i++) yy.push(i + 1)
    this.corrigeeffectifs(true, yy)
  }

  ajoute () {
    this.majtab()
    const ob1 = { poss: 'liste', laquelle: 'legendeX', choix: textes.choisir }
    const ob2 = { poss: 'nb', prez: 0 }
    this.tableau.push([ob1, ob2])
    this.colors.push({ fi: 'rgb(116, 208, 241)', st: 'rgb(6,6,10)', ba: 'rgb(200,0,0)' })
    if (this.bonx) {
      this.lalistederoul[this.tableau.length - 1] = j3pAddElt(this.eltTable, 'div', { style: { position: 'absolute' } })
      this.listesDeroulantes.push(ListeDeroulante.create(this.lalistederoul[this.tableau.length - 1], this.montab, {
        centre: true,
        sensHaut: true,
        onChange: this.gereliste.bind(this, this.tableau.length - 1) // choix sera le 2e argument
      }))
    } else {
      this.lalistederoul[this.tableau.length - 1] = j3pAddElt(this.eltTable, 'div', { style: { position: 'absolute' } })
      j3pAffiche(this.lalistederoul[this.tableau.length - 1], null, '?')
    }
    this.majtab()
    this.divbouta.innerHTML = ''
    this.divBoutons.innerHTML = ''
    if (this.tableau.length < 8) {
      // eslint-disable-next-line no-new
      new BoutonStyleMathquill(this.divbouta, 'boubout', 'Ajouter un bâton', this.listenerAjoute, { taille: '20px' })
    }
    if (this.tableau.length > 2) {
      this.bouts = new BoutonStyleMathquill(this.divBoutons, 'boubout', 'Supprimer un bâton', this.listenerSupprime, { taille: '20px' })
    }
  }

  gerelistex (choix) {
    this.tableau[0][0].choix = choix
    if (choix === this.stor.serieEnCours.legendeX) {
      if (this.bonx) {
        this.majtab()
        return
      }
      this.bonx = true
      if (this.stor.serieEnCours.typeX.indexOf('continu') === -1) {
        let montab = []
        for (let mu = 0; mu < this.stor.effectifs1.length; mu++) {
          montab.push(this.stor.effectifs1[mu].valeur + '')
        }
        montab = j3pShuffle(montab)
        montab.splice(0, 0, textes.choisir)
        for (let i = 1; i < this.tableau.length; i++) {
          j3pDetruit(this.lalistederoul[i])
          this.lalistederoul[i] = j3pAddElt(this.eltTable, 'div', { style: { position: 'absolute' } })
          this.listesDeroulantes.push(ListeDeroulante.create(this.lalistederoul[i], montab, {
            choix: (this.tableau[i][0].choix) ? (this.tableau[i][0].choix + '') : undefined,
            centre: true,
            sensHaut: true,
            onChange: this.gereliste.bind(this, i)
          }))
          if (this.ds.GraduationsXFaites) this.listesDeroulantes[this.listesDeroulantes.length - 1].disable()
        }
        this.montab = montab
      } else {
        if (this.gradinputX) {
          for (let u = 1; u < this.gradinputX.length; u++) {
            this.gradinputX[u].disable()
          }
        }
        this.gradinputX = []

        for (let i = 1; i < this.tableau.length + 1; i++) {
          j3pDetruit(this.lalistederoul[i])
          this.lalistederoul[i] = j3pAddElt(this.eltTable, 'div', { style: { position: 'absolute' } })
          this.gradinputX[i] = new ZoneStyleMathquill1(this.lalistederoul[i], { limite: 3, contenu: String(this.gradx[i - 1]) })
          this.gradinputX[i].modifL = this.onModifL
          this.gradinputX[i].li = i
          if (this.ds.GraduationsXFaites) this.gradinputX[i].disable()
        }
      }
    } else {
      if (!this.bonx) {
        this.majtab()
        return
      }
      this.bonx = false
      if (this.gradinputX) {
        for (let j = this.gradinputX.length - 1; j > 0; j--) {
          this.gradinputX[j].disable()
          this.gradinputX.pop()
        }
      }
      for (let j = this.listesDeroulantes.length - 1; j > -1; j--) {
        this.listesDeroulantes[j].disable()
        this.listesDeroulantes.pop()
      }
      for (let i = 1; i < this.tableau.length; i++) {
        j3pDetruit(this.lalistederoul[i])
        this.lalistederoul[i] = j3pAddElt(this.eltTable, 'div', { style: { position: 'absolute' } })
        j3pAffiche(this.lalistederoul[i], null, '?')
      }
      if (this.stor.serieEnCours.typeX.indexOf('continu') !== -1) {
        j3pDetruit(this.lalistederoul[this.tableau.length])
        this.lalistederoul[this.tableau.length] = j3pAddElt(this.eltTable, 'div', { style: { position: 'absolute' } })
        j3pAffiche(this.lalistederoul[this.tableau.length], null, '?')
      }
    }
    this.majtab()
  } // gerelistex

  gerelistey (a) {
    this.tableau[0][1].choix = a
    if (a === this.stor.serieEnCours.legendeY) {
      if (this.bony) {
        this.majtab()
        return
      }
      this.bony = true
      if (this.zoneInputY) {
        for (let u = this.zoneInputY.length - 1; u > -1; u--) {
          this.zoneInputY[u].disable()
          this.zoneInputY.pop()
        }
      }
      for (let u = 0; u < this.gradinput.length; u++) {
        j3pDetruit(this.gradinput[u])
      }
      this.gradinput = []
      for (let i = 0; i < this.nbtraits - 1; i++) {
        this.gradinput[i] = j3pAddElt(this.eltTable, 'div', { style: { position: 'absolute' } })
        if (this.ds.GraduationsYFaites) {
          j3pAffiche(this.gradinput[i], null, this.stor.serieEnCours.limiteY.pas * (i + 1) + '')
          this.grad[i] = this.stor.serieEnCours.limiteY.pas * (i + 1)
        } else {
          this.zoneInputY[i] = new ZoneStyleMathquill1(this.gradinput[i], { limite: 3, onmodif: this.getZsmModifListener() })
          this.zoneInputY[i].li = i
        }
        this.gradinput[i].style.fontSize = '13px'
      }
    } else {
      if (!this.bony) {
        this.majtab()
        return
      }
      this.bony = false
      if (this.zoneInputY) {
        for (let u = this.zoneInputY.length - 1; u > -1; u--) {
          this.zoneInputY[u].disable()
          this.zoneInputY.pop()
        }
      }

      for (let i = 0; i < this.nbtraits - 1; i++) {
        j3pDetruit(this.gradinput[i])
        this.gradinput[i] = j3pAddElt(this.eltTable, 'div', { style: { position: 'absolute' } })
        j3pAffiche(this.gradinput[i], null, '?')
      }
    }
    this.majtab()
  }

  supprime () {
    if (this.bonx) {
      this.listesDeroulantes[this.listesDeroulantes.length - 1].disable()
      this.listesDeroulantes.pop()
    }
    j3pDetruit(this.lalistederoul.pop())
    this.tableau.pop()
    this.colors.pop()
    this.majtab()
    this.divbouta.innerHTML = ''
    this.divBoutons.innerHTML = ''
    if (this.tableau.length < 8) {
      // eslint-disable-next-line no-new
      new BoutonStyleMathquill(this.divbouta, 'boubout', 'Ajouter un bâton', this.listenerAjoute, { taille: '20px' })
    }
    if (this.tableau.length > 2) {
      this.bouts = new BoutonStyleMathquill(this.divBoutons, 'boubout', 'Supprimer un bâton', this.listenerSupprime, { taille: '20px' })
    }
  }

  yareponse () {
    let ok = true
    if (this.listey.reponse === textes.choisir) {
      ok = false
      this.listey.focus()
    }
    if (this.listex.reponse === textes.choisir) {
      ok = false
      this.listex.focus()
    }
    if (this.bony) {
      for (let i = 0; i < this.grad.length; i++) {
        if (this.grad[i] === '') {
          ok = false
          this.gradinput[i].focus()
        }
      }
    }
    if (this.bonx) {
      if (this.stor.serieEnCours.typeX.indexOf('continu') !== -1) {
        for (let i = 0; i < this.gradx.length; i++) {
          if (this.gradx[i] === '') {
            ok = false
            this.gradinputX[i + 1].focus()
          }
        }
      } else {
        for (let i = 1; i < this.tableau.length; i++) {
          if (this.tableau[i][0].choix === textes.choisir) {
            ok = false
            this.listesDeroulantes[i - 1].focus()
          }
        }
      }
    }
    return ok
  }

  reponse () {
    const tabrep = []
    tabrep[0] = this.tableau[0]
    for (let i = 1; i < this.tableau.length; i++) {
      tabrep.push([{ choix: this.tableau[i][0].choix }, { prez: this.tableau[i][1].prez * parseInt(this.grad[0]) / 2 }])
    }
    return { gradx: this.gradx, grady: this.grad, tablo: tabrep, nbtraits: this.nbtraits }
  }

  gereliste (index, choix) {
    this.tableau[index][0].choix = choix
    this.majtab()
  }

  majtab () {
    const hauteur = 250
    const largeur = 600
    this.hauteur = hauteur
    this.min = 25
    this.max = hauteur - 25
    if (!this.svg) this.svg = j3pCreeSVG(this.eltTable, { width: largeur, height: hauteur, viewBox: '0 0 ' + largeur + ' ' + hauteur })
    j3pEmpty(this.svg)
    addSvg(this.svg, 'rect', { x: 0, y: 0, width: largeur, height: hauteur, style: 'fill:rgb(200,200,200);stroke-width:0;stroke:rgb(0,0,0)' })
    let effmax = 0
    for (let i = 0; i < this.stor.effectifs1.length; i++) {
      if (effmax < this.stor.effectifs1[i].effectif) effmax = this.stor.effectifs1[i].effectif
    }
    this.nbtraits = Math.trunc(effmax / this.stor.serieEnCours.limiteY.pas + 2)
    const intervallh = (hauteur - 50) / this.nbtraits
    const nbinterv = (this.stor.serieEnCours.typeX.includes('continu'))
      ? this.tableau.length + 1
      : 2 * (this.tableau.length - 1) + 1
    const intervalle = (largeur - 150) / nbinterv
    this.intervalle = intervalle
    // fleches et traits pointilles horizontaux
    addSvg(this.svg, 'line', { x1: 45, y1: hauteur - 25, x2: largeur - 5, y2: hauteur - 25, style: 'stroke:rgb(0,0,0);stroke-width:2' })
    if (this.stor.serieEnCours.typeX.indexOf('continu') !== -1) {
      addSvg(this.svg, 'line', { x1: largeur - 15, y1: hauteur - 35, x2: largeur - 5, y2: hauteur - 25, style: 'stroke:rgb(0,0,0);stroke-width:2' })
      addSvg(this.svg, 'line', { x1: largeur - 15, y1: hauteur - 15, x2: largeur - 5, y2: hauteur - 25, style: 'stroke:rgb(0,0,0);stroke-width:2' })
    }
    addSvg(this.svg, 'line', { x1: 45, y1: hauteur - 24, x2: 45, y2: 5, style: 'stroke:rgb(0,0,0);stroke-width:2' })
    addSvg(this.svg, 'line', { x1: 35, y1: 20, x2: 45, y2: 5, style: 'stroke:rgb(0,0,0);stroke-width:2' })
    addSvg(this.svg, 'line', { x1: 55, y1: 20, x2: 45, y2: 5, style: 'stroke:rgb(0,0,0);stroke-width:2' })
    const tabinput = []
    for (let i = 0; i < this.nbtraits - 1; i++) {
      const hj = addSvg(this.svg, 'line', { x1: 45, y1: hauteur - 25 - (i + 1) * intervallh, x2: largeur - 105, y2: hauteur - 25 - (i + 1) * intervallh, style: 'stroke:rgb(6,6,10);stroke-width:1;stroke-dasharray:4' })
      tabinput.push(hj)
    }
    // legendes
    this.dtextly = addSvg(this.svg, 'text', { x: 60, y: 15, style: 'font-size:15px;', text: this.tableau[0][1].choix })
    this.dtextl1 = addSvg(this.svg, 'text', { x: 0, y: hauteur - 40, style: 'font-size:15px;', text: this.tableau[0][0].choix })

    this.batondiag = []
    this.linediag = []
    if (this.stor.serieEnCours.typeX.indexOf('continu') === -1) {
      for (let i = 1; i < this.tableau.length; i++) {
        const s = (hauteur - 25 - intervallh * this.tableau[i][1].prez / 2)
        this.batondiag[i] = addSvg(this.svg, 'rect', { x: 35 + intervalle + 2 * (i - 1) * intervalle, y: s, width: intervalle, height: hauteur - 25 - s, style: 'fill:' + this.colors[i].fi + ';stroke:' + this.colors[i].st + ';stroke-width:1;' })
        this.linediag[i] = addSvg(this.svg, 'line', { x1: 35 + intervalle + 2 * (i - 1) * intervalle, y1: s, x2: 35 + intervalle + 2 * (i - 1) * intervalle + intervalle, y2: s, style: 'stroke:' + this.colors[i].ba + ';stroke-width:7;' })
        this.linediag[i].i = i
      }
    } else {
      for (let i = 1; i < this.tableau.length; i++) {
        const s = (hauteur - 25 - intervallh * this.tableau[i][1].prez / 2)
        this.batondiag[i] = addSvg(this.svg, 'rect', { x: 35 + intervalle + (i - 1) * intervalle, y: s, width: intervalle, height: hauteur - 25 - s, style: 'fill:rgb(116, 208, 241);stroke:rgb(6,6,10);stroke-width:1;' })
        this.linediag[i] = addSvg(this.svg, 'line', { x1: 35 + intervalle + (i - 1) * intervalle, y1: s, x2: 35 + intervalle + (i - 1) * intervalle + intervalle, y2: s, style: 'fill:rgb(116, 208, 241);stroke:rgb(255,6,10);stroke-width:7;' })
        this.linediag[i].i = i
      }
    }

    this.eltTable.style.width = largeur + 'px'
    this.eltTable.style.height = hauteur + 'px'
    const el = this.dtextl1
    const { width } = el.getBoundingClientRect()
    this.dtextl1.x.baseVal.getItem(0).value = largeur - width - 5
    this.dtextl1.x.baseVal.getItem(0).value = largeur - width - 5
    let mod, mod3
    if (this.stor.isWritingEnonce && this.ds.theme === 'zonesAvecImageDeFond') {
      mod = -14
      mod3 = 4
    } else {
      mod = -26
      mod3 = 0
    }
    this.eltTable.style.position = 'relative'
    if (!this.disabled) {
      if (!this.first) {
        if (this.zoneInputY) {
          for (let i = 0; i < this.zoneInputY.length; i++) {
            j3pDetruit(this.zoneInputY[i].disable())
          }
        }
        for (let i = 0; i < this.gradinput.length; i++) {
          j3pDetruit(this.gradinput[i])
        }
      } else {
        this.first = false
        this.grad = []
        for (let i = 0; i < this.nbtraits - 1; i++) {
          this.grad.push('')
        }
        this.lalistederoul = []
        for (let i = 1; i < this.tableau.length; i++) {
          this.lalistederoul[i] = j3pAddElt(this.eltTable, 'span', { style: { position: 'absolute' } })
          j3pAffiche(this.lalistederoul[i], null, '?')
        }
        if (this.stor.serieEnCours.typeX.indexOf('continu') !== -1) {
          this.lalistederoul[this.tableau.length] = j3pAddElt(this.eltTable, 'span', { style: { position: 'absolute' } })
          j3pAffiche(this.lalistederoul[this.tableau.length], null, '?')
        }
      }
      this.gradinput = []
      this.zoneInputY = []
      if (this.bony) {
        for (let i = 0; i < this.nbtraits - 1; i++) {
          if (this.ds.GraduationsYFaites) {
            this.gradinput[i] = j3pAddElt(this.eltTable, 'span', { style: { position: 'absolute' } })
            j3pAffiche(this.gradinput[i], null, this.stor.serieEnCours.limiteY.pas * (i + 1) + '')
          } else {
            this.gradinput[i] = j3pAddElt(this.eltTable, 'span', { style: { position: 'absolute' } })
            this.zoneInputY[i] = new ZoneStyleMathquill1(this.gradinput[i], { limite: 3, onmodif: this.getZsmModifListener(), contenu: this.grad[i] })
            this.zoneInputY[i].li = i
            this.zoneInputY[i].isblur = false
          }
          this.gradinput[i].style.fontSize = '13px'
        }
      } else {
        for (let i = 0; i < this.nbtraits - 1; i++) {
          this.gradinput[i] = j3pAddElt(this.eltTable, 'span', { style: { position: 'absolute' } })
          j3pAffiche(this.gradinput[i], null, '?')
        }
      }
      const oubase = this.eltTable.getBoundingClientRect()
      for (let i = 0; i < this.nbtraits - 1; i++) {
        const el = tabinput[i]
        const { top, left } = el.getBoundingClientRect()
        const lacase = this.gradinput[i].getBoundingClientRect()
        this.gradinput[i].style.top = (top - oubase.top - lacase.height / 2) + 'px'
        this.gradinput[i].style.left = (left - oubase.left - lacase.width - 5) + 'px'
        this.left = left - oubase.left
      }
      const rectbase = this.svg.getBoundingClientRect()
      for (let i = 1; i < this.tableau.length; i++) {
        const el = this.linediag[i]
        el.style.cursor = 'pointer'
        el.addEventListener('mouseout', this.listenerOut, false)
        el.addEventListener('mouseover', this.listenerOver, false)
        el.addEventListener('mousedown', this.listenerDown, false)
        el.addEventListener('touchstart', this.listenerDown, { capture: false, passive: true })
        el.addEventListener('mouseup', this.listenerUp, false)
        el.addEventListener('touchend', this.listenerUp, false)
        el.addEventListener('touchcancel', this.listenerUp, false)
        if (this.stor.serieEnCours.typeX.indexOf('continu') === -1) {
          this.lalistederoul[i].style.left = (rectbase.left + 35 + intervalle * 1.5 + 2 * (i - 1) * intervalle - oubase.left - this.lalistederoul[i].offsetWidth / 2 + mod3) + 'px'
          const dec = (this.lalistederoul[i].offsetWidth > (intervalle * 2)) ? 30 : 0
          this.lalistederoul[i].style.top = (((i + 1) % 2) * dec + rectbase.top + 10 + hauteur - oubase.top + mod) + 'px'
        } else {
          this.lalistederoul[i].style.left = (rectbase.left + 35 + intervalle + (i - 1) * intervalle - oubase.left - this.lalistederoul[i].offsetWidth / 2 + mod3) + 'px'
          this.lalistederoul[i].style.top = (rectbase.top + hauteur - oubase.top + mod + 5) + 'px'
        }
      }
      if (this.stor.serieEnCours.typeX.indexOf('continu') !== -1) {
        this.lalistederoul[this.tableau.length].style.left = (rectbase.left + 35 + intervalle + (this.tableau.length - 1) * intervalle - oubase.left - this.lalistederoul[this.tableau.length].offsetWidth / 2 + mod3) + 'px'
        this.lalistederoul[this.tableau.length].style.top = (rectbase.top + hauteur - oubase.top + mod + 5) + 'px'
      }
      this.intervh = intervallh

      this.svg.addEventListener('mousemove', (event) => { this.drag(event, getCoordFromMouseEvent(event)) }, { capture: false, passive: true })
      this.svg.addEventListener('touchmove', (event) => { this.drag(event, getCoordFromTouchEvent(event)) }, { capture: false, passive: false })
      this.svg.addEventListener('mouseup', this.listenerUp, false)
      this.svg.addEventListener('touchend', this.listenerUp, false)
      this.parcours.zonesElts.MG.addEventListener('mouseup', this.listenerUp, false)
      this.parcours.zonesElts.MG.addEventListener('touchend', this.listenerUp, false)
      this.parcours.zonesElts.MG.addEventListener('touchcancel', this.listenerUp, false)
    }
    this.tabinput = tabinput
  }

  getZsmModifListener () {
    const rep = this
    /** @this {ZoneStyleMathquill1} */
    return function zsmModifListener () {
      rep.grad[this.li] = this.reponse().replace(/ /g, '')
      const { width } = this.conteneur.getBoundingClientRect()
      this.conteneur.style.left = (rep.left - width - 5) + 'px'
    }
  }

  /**
   * Déplace l’élément cliqué (on est appelé au mousemove|touchmove)
   * @param {MouseEvent|TouchEvent} event
   * @param {number} y position verticale (de la souris|touch)
   */
  drag (event, { y }) {
    if (this.draggingElt === null || this.disabled) return
    if (!Number.isFinite(y)) return console.error(Error('drag récupère un y invalide'), y)
    const svg = this.svg
    const rectbase = svg.getBoundingClientRect(svg)
    let ly = y - rectbase.top
    const pourdiff = this.hauteur - 25 - ly
    const autre = Math.abs(Math.max(0, Math.round(Math.round(pourdiff) / Math.round(this.intervh / 2))))
    ly = (this.hauteur - 25 - this.intervh / 2 * autre)
    ly = Math.max(this.min, Math.min(this.max, ly))
    this.draggingElt.y1.baseVal.value = ly
    this.draggingElt.y2.baseVal.value = ly
    this.batondiag[this.draggingElt.i].y.baseVal.value = ly
    this.batondiag[this.draggingElt.i].height.baseVal.value = this.hauteur - 25 - ly
    this.draggingElt.style.stroke = 'rgb(200,0,0)'
    this.batondiag[this.draggingElt.i].style.stroke = 'rgb(6,6,10)'
    this.batondiag[this.draggingElt.i].style.fill = 'rgb(116, 208, 241)'
    this.colors[(parseInt(this.draggingElt[this.draggingElt.length - 1]))] = {
      fi: 'rgb(116, 208, 241)',
      st: 'rgb(6,6,10);',
      ba: 'rgb(200,0,0)'
    }
    this.tableau[this.draggingElt.i][1].prez = autre
  }
}

class RepGraph {
  /**
   *
   * @param {HTMLElement} cont
   * @param {Parcours} parcours
   */
  constructor (cont, parcours) {
    this.parcours = parcours
    this.ds = parcours.donneesSection
    this.stor = parcours.storage

    // on crée des propriétés pour nos listeners, pour pouvoir les ajouter / retirer
    // (fonctions fléchées pour éviter de faire du bind et conserver le this sur notre objet courant)
    /**
     * @param {MouseEvent|TouchEvent} event
     */
    this.listenerDown = (event) => { this.draggingElt = event.currentTarget }
    this.listenerOut = (event) => { if (event.currentTarget) event.currentTarget.style.strokeWidth = '6px' }
    this.listenerOver = (event) => { if (!this.disabled && event.currentTarget) event.currentTarget.style.strokeWidth = '10px' }
    this.listenerUp = () => { this.draggingElt = null }
    // les listeners utilisés sur les boutons
    this.listenerAjoute = this.ajoute.bind(this)
    this.listenerSupprime = this.supprime.bind(this)

    this.bony = false
    this.bonx = false
    this.id = j3pGetNewId()
    this.first = true
    this.listesDeroulantes = []
    this.gradx = []
    this.disabled = false
    this.cont = j3pAddElt(cont, 'div', '', { id: this.id + 'tabrep' })
    let tab = addDefaultTable(this.cont, 1, 2)
    this.eltTable = tab[0][0]
    this.eltReste = tab[0][1]
    tab = addDefaultTable(this.eltReste, 5, 1)
    this.eltZoneAide = tab[0][0]
    this.eltZoneX = tab[1][0]
    this.eltZoneY = tab[2][0]
    this.divbouta = tab[3][0]
    this.divBoutons = tab[4][0]
    j3pAffiche(this.eltZoneX, null, 'Légende en abscisse&nbsp;: \n')
    j3pAffiche(this.eltZoneY, null, 'Légende en ordonnée&nbsp;: \n')

    let montab = []
    montab.push(this.stor.serieEnCours.legendeX)
    montab.push(this.stor.serieEnCours.legendeY)
    for (let mu = 0; mu < this.stor.serieEnCours.legendesKo.length; mu++) {
      montab.push(this.stor.serieEnCours.legendesKo[mu])
    }
    montab = j3pShuffle(montab)
    montab.splice(0, 0, textes.choisir)
    let choixInitialX = textes.choisir
    let choixInitialY = textes.choisir
    if (this.ds.LegendesFaites) {
      choixInitialX = this.stor.serieEnCours.legendeX
      choixInitialY = this.stor.serieEnCours.legendeY
    }
    if (this.ds.GraduationsYFaites) {
      choixInitialY = this.stor.serieEnCours.legendeY
    }
    if (this.ds.GraduationsXFaites) {
      choixInitialX = this.stor.serieEnCours.legendeX
    }

    this.listex = ListeDeroulante.create(this.eltZoneX, montab, {
      choix: choixInitialX,
      onChange: this.gerelistex.bind(this),
      sensHaut: true
    })
    this.listey = ListeDeroulante.create(this.eltZoneY, montab, {
      choix: choixInitialY,
      onChange: this.gerelistey.bind(this),
      sensHaut: true
    })

    if (this.ds.LegendesFaites) {
      this.listex.disable()
      this.listey.disable()
    }
    if (this.ds.GraduationsYFaites) {
      this.listey.disable()
    }
    if (this.ds.GraduationsXFaites) {
      this.listex.disable()
    }

    this.colors = ['']

    /**
     * @private
     * @type {{poss: string, laquelle: string, choix: number}[][]}
     */
    this.tableau = [[]]
    if (this.stor.serieEnCours.typeX.indexOf('continu') === -1) {
      if (this.ds.GraduationsXFaites) {
        this.colors[1] = { fi: 'rgb(116, 208, 241)', st: 'rgb(255,6,10)' }
        this.tableau = [[
          { poss: 'liste', laquelle: 'legende', choix: choixInitialX },
          { poss: 'liste', laquelle: 'legende', choix: choixInitialY }
        ]]
        for (let i = 0; i < this.stor.effectifs1.length; i++) {
          this.colors.push({ fi: 'rgb(116, 208, 241)', st: 'rgb(255,6,10)' })
          this.tableau.push([
            { poss: 'liste', laquelle: 'legende', choix: this.stor.effectifs1[i].valeur },
            { poss: 'nb', prez: 0 }
          ])
        }
      } else {
        this.colors[1] = { fi: 'rgb(116, 208, 241)', st: 'rgb(255,6,10)' }
        this.colors[2] = { fi: 'rgb(116, 208, 241)', st: 'rgb(255,6,10)' }
        this.tableau = [[
          { poss: 'liste', laquelle: 'legende', choix: choixInitialX },
          { poss: 'liste', laquelle: 'legende', choix: choixInitialY }
        ], [
          { poss: 'null' },
          { poss: 'nb', prez: 0 }
        ], [
          { poss: 'null' },
          { poss: 'nb', prez: 0 }
        ]]
      }
    } else {
      if (this.ds.GraduationsXFaites) {
        for (let i = 0; i < this.stor.effectifs1.length + 1; i++) {
          this.gradx.push(j3pArrondi(this.stor.serieEnCours.limiteX.min + i * this.stor.serieEnCours.limiteX.amplitude, 5))
        }
      } else {
        for (let i = 0; i < this.stor.effectifs1.length + 1; i++) {
          this.gradx.push('')
        }
      }
      this.tableau = [[
        { poss: 'liste', laquelle: 'legende', choix: choixInitialX },
        { poss: 'liste', laquelle: 'legende', choix: choixInitialY }
      ]]
      for (let i = 0; i < this.stor.effectifs1.length; i++) {
        this.tableau.push([{ poss: 'nb', prez: '' }, { poss: 'nb', prez: (i + 1) % 2 }])
        this.colors.push({ fi: 'rgb(116, 208, 241)', st: 'rgb(255,6,10)' })
      }
    }
    /** @type {HTMLElement} */
    this.draggingElt = null

    // le listener de modifL (zsm) peut pas être une fct fléchée car on a besoin de son this
    // on en crée un ici plutôt que le recréer à chaque appel de gerelistex (et chaque tour de boucle dedans)
    const rep = this
    /** @this {ZoneStyleMathquill1} */
    this.onModifL = function onModifL () {
      // this est zsm
      const monnum = parseInt(this.li)
      rep.gradx[monnum - 1] = this.reponse().replace(/ /g, '')
      const { left } = rep.svg.getBoundingClientRect()
      const { width } = rep.lalistederoul[monnum].getBoundingClientRect()
      const { left: mgLeft } = rep.parcours.zonesElts.MG.getBoundingClientRect()
      rep.lalistederoul[monnum].style.left = (left + 35 + rep.intervalle + (monnum - 1) * rep.intervalle - width / 2 - mgLeft) + 'px'
    }

    setTimeout(() => {
      this.majtab()
      if (this.ds.LegendesFaites) {
        this.gerelistex(choixInitialX)
        this.gerelistey(choixInitialY)
      }
      if (this.ds.GraduationsYFaites) {
        this.gerelistey(choixInitialY)
      }
      if (this.ds.GraduationsXFaites) {
        this.gerelistex(choixInitialX)
      }
      if ((this.stor.serieEnCours.typeX.indexOf('continu') === -1) && (!this.ds.GraduationsXFaites)) {
        // eslint-disable-next-line no-new
        new BoutonStyleMathquill(this.divbouta, 'boubout', 'Ajouter un segment', this.listenerAjoute, { taille: '20px' })
      }

      this.bullaide = new BulleAide(this.eltZoneAide, textes.aidegraph)
      this.parcours.finEnonce()
    }, 100)
  }

  desactive () {
    this.listex.disable()
    this.listey.disable()
    const svg = this.svg
    const fontZone = svg.style.fontFamily
    const tailleFontZone = svg.style.fontSize
    const ns = 'http://www.w3.org/2000/svg'
    if (this.bony) {
      if (!this.ds.GraduationsYFaites) {
        for (let i = 0; i < this.grad.length; i++) {
          const fg = this.zoneInputY[i].reponse().replace(/ /g, '')
          this.zoneInputY[i].disable()
          j3pDetruit(this.gradinput[i])
          const tt = document.createElementNS(ns, 'text')
          // FIXME, textWidth ajoute un span pour calculer une largeur, dans un svg ça doit marcher moyen, et probablement planter ou remonter n’importe quoi, au moins sur certains navigateurs
          // faire du $(svg.parentNode).textWidth(…) va donner la taille dans du html, mais ça peut être différent dans le svg (suivant son scale)
          // @todo il faudrait plutôt créer une nouvelle fct textWidthInSvg(svg, text) qui mesurerait la taille du texte dans ce svg, demander à Yves qui a déjà fait ça dans mathgraph
          const newTailleZone = Math.max($(this.svg).textWidth(fg, fontZone, tailleFontZone), 5)
          tt.setAttributeNS(null, 'x', 42 - newTailleZone)
          tt.setAttributeNS(null, 'y', (250 - 25 - (i + 1) * (200 / this.nbtraits)) + 6)
          tt.setAttributeNS(null, 'font-size', '15')
          this.gradinput[i] = tt
          const textNode = document.createTextNode(fg)
          tt.appendChild(textNode)
          svg.appendChild(tt)
        }
      }
    }
    if (this.bonx) {
      if (this.stor.serieEnCours.typeX.indexOf('continu') !== -1) {
        for (let i = 1; i < this.gradx.length + 1; i++) {
          const fg = this.gradinputX[i].reponse().replace(/ /g, '')
          this.gradinputX[i].disable()
          const ns = 'http://www.w3.org/2000/svg'
          const tt = document.createElementNS(ns, 'text')
          const newTailleZone = Math.max($(svg).textWidth(fg, fontZone, tailleFontZone), 5)
          tt.setAttributeNS(null, 'x', (37 + this.intervalle + (i - 1) * this.intervalle) - newTailleZone / 2)
          tt.setAttributeNS(null, 'y', this.hauteur - 10)
          tt.setAttributeNS(null, 'font-size', '15')
          this.gradinputX[i] = tt
          const textNode = document.createTextNode(fg)
          tt.appendChild(textNode)
          svg.appendChild(tt)
        }
      }
    }
    if (this.stor.serieEnCours.typeX.indexOf('continu') === -1) {
      for (let i = 0; i < this.listesDeroulantes.length; i++) {
        this.listesDeroulantes[i].disable()
        const fg = this.listesDeroulantes[i].reponse
        j3pEmpty(this.lalistederoul[i + 1])
        const tt = document.createElementNS(ns, 'text')
        const newTailleZone = Math.max($(svg).textWidth(fg, fontZone, tailleFontZone), 5)
        tt.setAttributeNS(null, 'x', (37 + 1.5 * this.intervalle + i * 2 * this.intervalle) - newTailleZone / 2)
        tt.setAttributeNS(null, 'y', this.hauteur - 1 - 10 * (i % 2))
        tt.setAttributeNS(null, 'font-size', '15')
        this.lalistederoul[i] = tt
        const textNode = document.createTextNode(fg)
        tt.appendChild(textNode)
        svg.appendChild(tt)
        this.listesDeroulantes[i].pourCo = this.lalistederoul[i]
      }
    }
    this.disabled = true
    this.bullaide.disable()
    j3pEmpty(this.eltReste)
  } // desactive

  corrigeval (bool, tab) {
    for (let i = 0; i < tab.length; i++) {
      for (let j = 0; j < this.listesDeroulantes.length; j++) {
        if (this.listesDeroulantes[j].reponse === tab[i]) {
          if (!this.disabled) {
            this.listesDeroulantes[j].corrige(bool)
          } else {
            let zell = colorOk
            if (!bool) zell = colorKo
            this.listesDeroulantes[i].pourCo.style.fill = zell
          }
          if (bool === false) this.corrigeeffectifs(false, [j + 1])
        }
      }
    }
  }

  corrigeeffectifs (bool, tab) {
    const col = (bool)
      ? {
          fi: 'rgb(100, 210, 200)',
          st: colorOk,
          ba: colorOk
        }
      : {
          fi: 'rgb(255, 122, 41)',
          st: colorKo,
          ba: colorKo
        }

    for (let i = 0; i < tab.length; i++) {
      this.linediag[tab[i]].style.stroke = col.ba
      if (tab[i] !== this.tableau.length - 1) {
        this.batondiag[tab[i]].style.stroke = col.st
        this.batondiag[tab[i]].style.fill = col.fi
      }
      this.colors[tab[i]] = col
    }
  }

  toutvalvert () {
    for (let i = 0; i < this.listesDeroulantes.length; i++) {
      if (!this.disabled) {
        this.listesDeroulantes[i].corrige(true)
      } else {
        this.listesDeroulantes[i].pourCo.style.fill = colorOk
      }
    }
  }

  touteffvert () {
    for (let i = 1; i < this.tableau.length; i++) {
      this.linediag[i].style.stroke = colorOk
      this.linediag[i].style.fill = 'rgb(100, 210, 200)'
      this.linediag[i].style.stroke = colorOk
      this.linediag[i].style.fill = 'rgb(100, 210, 200)'
      this.colors[i] = { fi: 'rgb(100, 210, 200)', st: colorOk, ba: colorOk }
    }
  }

  colorgrady (bool, qui) {
    if (this.bony) {
      const col = bool ? colorOk : colorKo

      for (let i = 0; i < qui.length; i++) {
        this.gradinput[qui[i]].style.fill = col
        this.gradinput[qui[i]].style.color = col
      }
    }
  }

  colorgradx (bool, qui) {
    if (this.stor.serieEnCours.typeX.indexOf('continu') !== -1) {
      if (this.bonx) {
        const col = bool ? colorOk : colorKo

        for (let i = 0; i < qui.length; i++) {
          this.lalistederoul[qui[i]].style.fill = col
          this.lalistederoul[qui[i]].style.color = col
        }
      }
    }
  }

  colorlegendey (bool) {
    const col = bool ? colorOk : colorKo

    this.dtextly.style.stroke = col
    this.dtextly.style.fill = col
    this.listey.corrige(bool)
  }

  colorlegendex (bool) {
    const col = bool ? colorOk : colorKo

    this.dtextl1.style.stroke = col
    this.dtextl1.style.fill = col
    this.listex.corrige(bool)
  }

  toutvert () {
    this.colorlegendex(true)
    this.colorlegendey(true)
    if (this.bony) {
      const titab = []
      for (let i = 0; i < this.nbtraits - 1; i++) {
        titab[i] = i
      }
      this.colorgrady(true, titab)
    }
    if (this.bonx) {
      const titab = []
      for (let i = 0; i < this.tableau.length; i++) {
        titab[i] = i + 1
      }
      this.colorgradx(true, titab)
    }
    this.toutvalvert()
    const yy = []
    for (let i = 0; i < this.tableau.length - 1; i++) yy.push(i + 1)
    this.corrigeeffectifs(true, yy)
  }

  ajoute () {
    this.majtab()
    const ob1 = { poss: 'liste', laquelle: 'legendeX', choix: textes.choisir }
    const ob2 = { poss: 'nb', prez: 0 }
    this.tableau.push([ob1, ob2])
    this.colors.push({ fi: 'rgb(116, 208, 241)', st: 'rgb(255,6,10)' })
    if (this.bonx) {
      this.lalistederoul[this.tableau.length - 1] = j3pAddElt(this.eltTable, 'div', { style: { position: 'absolute' } })
      this.listesDeroulantes.push(ListeDeroulante.create(this.lalistederoul[this.tableau.length - 1], this.montab, {
        centre: true,
        sensHaut: true,
        onChange: this.gereliste.bind(this, this.tableau.length - 1)
      }))
    } else {
      this.lalistederoul[this.tableau.length - 1] = j3pAddElt(this.eltTable, 'div', { style: { position: 'absolute' } })
      j3pAffiche(this.lalistederoul[this.tableau.length - 1], null, '?')
    }
    this.majtab()
    this.divbouta.innerHTML = ''
    this.divBoutons.innerHTML = ''
    if (this.tableau.length < 8) {
      // eslint-disable-next-line no-new
      new BoutonStyleMathquill(this.divbouta, 'boubout', 'Ajouter un bâton', this.listenerAjoute, { taille: '20px' })
    }
    if (this.tableau.length > 2) {
      this.bouts = new BoutonStyleMathquill(this.divBoutons, 'boubout', 'Supprimer un bâton', this.listenerSupprime, { taille: '20px' })
    }
  }

  gerelistex (a) {
    this.tableau[0][0].choix = a
    if (a === this.stor.serieEnCours.legendeX) {
      if (this.bonx) {
        this.majtab()
        return
      }
      this.bonx = true
      if (this.stor.serieEnCours.typeX.indexOf('continu') === -1) {
        let montab = []
        for (let mu = 0; mu < this.stor.effectifs1.length; mu++) {
          montab.push(this.stor.effectifs1[mu].valeur + '')
        }
        montab = j3pShuffle(montab)
        montab.splice(0, 0, textes.choisir)
        for (let i = 1; i < this.tableau.length; i++) {
          j3pDetruit(this.lalistederoul[i])
          this.lalistederoul[i] = j3pAddElt(this.eltTable, 'div', { style: { position: 'absolute' } })
          this.listesDeroulantes.push(ListeDeroulante.create(this.lalistederoul[i], montab, {
            choix: this.tableau[i][0].choix + '',
            centre: true,
            sensHaut: true,
            onChange: this.gereliste.bind(this, i)
          }))
          if (this.ds.GraduationsXFaites) this.listesDeroulantes[this.listesDeroulantes.length - 1].disable()
        }
        this.montab = montab
      } else {
        if (this.gradinputX) {
          for (let u = 1; u < this.gradinputX.length; u++) {
            this.gradinputX[u].disable()
          }
        }
        this.gradinputX = []

        for (let i = 1; i < this.tableau.length + 1; i++) {
          j3pDetruit(this.lalistederoul[i])
          this.lalistederoul[i] = j3pAddElt(this.eltTable, 'div', { style: { position: 'absolute' } })
          this.gradinputX[i] = new ZoneStyleMathquill1(this.lalistederoul[i], { limite: 3, contenu: String(this.gradx[i - 1]) })
          this.gradinputX[i].modifL = this.onModifL
          this.gradinputX[i].li = i
          if (this.ds.GraduationsXFaites) this.gradinputX[i].disable()
        }
      }
    } else {
      if (!this.bonx) {
        this.majtab()
        return
      }
      this.bonx = false
      if (this.gradinputX) {
        for (let j = this.gradinputX.length - 1; j > 0; j--) {
          this.gradinputX[j].disable()
          this.gradinputX.pop()
        }
      }
      for (let j = this.listesDeroulantes.length - 1; j > -1; j--) {
        this.listesDeroulantes[j].disable()
        this.listesDeroulantes.pop()
      }
      for (let i = 1; i < this.tableau.length; i++) {
        j3pDetruit(this.lalistederoul[i])
        this.lalistederoul[i] = j3pAddElt(this.eltTable, 'div', { style: { position: 'absolute' } })
        j3pAffiche(this.lalistederoul[i], null, '?')
      }
      if (this.stor.serieEnCours.typeX.indexOf('continu') !== -1) {
        j3pDetruit(this.lalistederoul[this.tableau.length])
        this.lalistederoul[this.tableau.length] = j3pAddElt(this.eltTable, 'div', { style: { position: 'absolute' } })
        j3pAffiche(this.lalistederoul[this.tableau.length], null, '?')
      }
    }
    this.majtab()
  }

  gerelistey (a) {
    this.tableau[0][1].choix = a
    if (a === this.stor.serieEnCours.legendeY) {
      if (this.bony) {
        this.majtab()
        return
      }
      this.bony = true
      if (this.zoneInputY) {
        for (let u = 1; u < this.zoneInputY.length; u++) {
          this.zoneInputY[u].disable()
        }
      }
      for (let u = 0; u < this.gradinput.length; u++) {
        j3pDetruit(this.gradinput[u])
      }
      this.gradinput = []
      for (let i = 0; i < this.nbtraits - 1; i++) {
        this.gradinput[i] = j3pAddElt(this.eltTable, 'div', { style: { position: 'absolute' } })
        if (this.ds.GraduationsYFaites) {
          j3pAffiche(this.gradinput[i], null, this.stor.serieEnCours.limiteY.pas * (i + 1) + '')
          this.grad[i] = this.stor.serieEnCours.limiteY.pas * (i + 1)
        } else {
          this.zoneInputY[i] = new ZoneStyleMathquill1(this.gradinput[i], { limite: 3, onmodif: this.getZsmModifListener() })
          this.zoneInputY[i].li = i
        }
        this.gradinput[i].style.fontSize = '13px'
      }
    } else {
      if (!this.bony) {
        this.majtab()
        return
      }
      this.bony = false
      for (let i = 0; i < this.nbtraits - 1; i++) {
        j3pDetruit(this.gradinput[i])
        this.gradinput[i] = j3pAddElt(this.eltTable, 'div', { style: { position: 'absolute' } })
        j3pAffiche(this.gradinput[i], null, '?')
      }
    }

    this.majtab()
  }

  supprime () {
    if (this.bonx) {
      this.listesDeroulantes[this.listesDeroulantes.length - 1].disable()
      this.listesDeroulantes.pop()
    }
    j3pDetruit(this.lalistederoul.pop())
    this.tableau.pop()
    this.colors.pop()
    this.majtab()
    this.divbouta.innerHTML = ''
    this.divBoutons.innerHTML = ''
    if (this.tableau.length < 8) {
      // eslint-disable-next-line no-new
      new BoutonStyleMathquill(this.divbouta, 'boubout', 'Ajouter un segment', this.listenerAjoute, { taille: '20px' })
    }
    if (this.tableau.length > 3) {
      this.bouts = new BoutonStyleMathquill(this.divBoutons, 'boubout', 'Supprimer un segment', this.listenerSupprime, { taille: '20px' })
    }
  }

  yareponse () {
    let ok = true
    if (this.listey.reponse === textes.choisir) {
      ok = false
      this.listey.focus()
    }
    if (this.listex.reponse === textes.choisir) {
      ok = false
      this.listex.focus()
    }
    if (this.bony) {
      for (let i = 0; i < this.grad.length; i++) {
        if (this.grad[i] === '') {
          this.zoneInputY[i].focus()
          return false
        }
      }
    }
    if (!this.ds.GraduationsXFaites) {
      if (this.bonx) {
        if (this.stor.serieEnCours.typeX.indexOf('continu') !== -1) {
          for (const [i, gradX] of this.gradx.entries()) {
            if (gradX === '') {
              ok = false
              if (this.gradinputX[i]) this.gradinputX[i].focus()
              else console.error(Error(`pas de this.gradinputX[${i}]`))
              return false
            }
          }
        } else {
          for (let i = 1; i < this.tableau.length; i++) {
            if (this.tableau[i][0].choix === textes.choisir) {
              ok = false
              this.listesDeroulantes[i - 1].focus()
            }
          }
        }
      }
    }
    return ok
  }

  reponse () {
    const tabrep = []
    tabrep[0] = this.tableau[0]
    for (let i = 1; i < this.tableau.length; i++) {
      tabrep.push([{ choix: this.tableau[i][0].choix }, { prez: this.tableau[i][1].prez * parseInt(this.grad[0]) / 2 }])
    }
    return { gradx: this.gradx, grady: this.grad, tablo: tabrep, nbtraits: this.nbtraits }
  }

  gereliste (index, choix) {
    this.tableau[index][0].choix = choix
    this.majtab()
  }

  majtab () {
    const hauteur = 250
    const largeur = 600
    this.hauteur = hauteur
    this.min = 25
    this.max = hauteur - 25
    if (!this.svg) this.svg = j3pCreeSVG(this.eltTable, { width: largeur, height: hauteur, viewBox: '0 0 ' + largeur + ' ' + hauteur })
    j3pEmpty(this.svg)
    addSvg(this.svg, 'rect', { x: 0, y: 0, width: largeur, height: hauteur, style: 'fill:rgb(200,200,200);stroke-width:0;stroke:rgb(0,0,0)' })
    // det cb traits horizontaux
    let effmax = 0
    for (let i = 0; i < this.stor.effectifs1.length; i++) {
      if (effmax < this.stor.effectifs1[i].effectif) effmax = this.stor.effectifs1[i].effectif
    }
    this.nbtraits = Math.trunc(effmax / this.stor.serieEnCours.limiteY.pas + 2)
    const intervallh = (hauteur - 50) / this.nbtraits
    const nbinterv = (this.stor.serieEnCours.typeX.indexOf('continu') === -1)
      ? 2 * (this.tableau.length - 1) + 1
      : this.tableau.length + 1
    const intervalle = (largeur - 150) / nbinterv
    this.intervalle = intervalle
    // fleches et traits pointilles horizontaux
    addSvg(this.svg, 'line', { x1: 45, y1: hauteur - 25, x2: largeur - 5, y2: hauteur - 25, style: 'stroke:rgb(0,0,0);stroke-width:2' })
    if (this.stor.serieEnCours.typeX.indexOf('continu') !== -1) {
      addSvg(this.svg, 'line', { x1: largeur - 15, y1: hauteur - 35, x2: largeur - 5, y2: hauteur - 25, style: 'stroke:rgb(0,0,0);stroke-width:2' })
      addSvg(this.svg, 'line', { x1: largeur - 15, y1: hauteur - 15, x2: largeur - 5, y2: hauteur - 25, style: 'stroke:rgb(0,0,0);stroke-width:2' })
    }
    addSvg(this.svg, 'line', { x1: 45, y1: hauteur - 24, x2: 45, y2: 5, style: 'stroke:rgb(0,0,0);stroke-width:2' })
    addSvg(this.svg, 'line', { x1: 35, y1: 20, x2: 45, y2: 5, style: 'stroke:rgb(0,0,0);stroke-width:2' })
    addSvg(this.svg, 'line', { x1: 55, y1: 20, x2: 45, y2: 5, style: 'stroke:rgb(0,0,0);stroke-width:2' })
    const tabinput = []
    for (let i = 0; i < this.nbtraits - 1; i++) {
      const hj = addSvg(this.svg, 'line', { x1: 45, y1: hauteur - 25 - (i + 1) * intervallh, x2: largeur - 105, y2: hauteur - 25 - (i + 1) * intervallh, style: 'stroke:rgb(6,6,10);stroke-width:1;stroke-dasharray:4' })
      tabinput.push(hj)
    }
    // legendes
    this.dtextly = addSvg(this.svg, 'text', { x: 60, y: 15, style: 'font-size:15px;', text: this.tableau[0][1].choix })
    this.dtextl1 = addSvg(this.svg, 'text', { x: 0, y: hauteur - 40, style: 'font-size:15px;', text: this.tableau[0][0].choix })

    this.batondiag = []
    this.linediag = []
    // les batons

    if (this.stor.serieEnCours.typeX.indexOf('continu') === -1) {
      let i
      for (i = 1; i < this.tableau.length - 1; i++) {
        const s1 = (hauteur - 25 - intervallh * this.tableau[i][1].prez / 2)
        const s2 = (hauteur - 25 - intervallh * this.tableau[i + 1][1].prez / 2)
        this.batondiag[i] = addSvg(this.svg, 'line', { x1: 35 + intervalle * 1.5 + (i - 1) * 2 * intervalle, y1: s1, x2: 35 + intervalle * 1.5 + (i) * intervalle * 2, y2: s2, style: 'fill:rgb(116, 208, 241);stroke:rgb(6,6,10);stroke-width:1;' })
        this.linediag[i] = addSvg(this.svg, 'circle', { cx: 35 + intervalle * 1.5 + 2 * (i - 1) * intervalle, cy: s1, r: 6, style: 'fill:' + this.colors[i].fi + ';stroke:' + this.colors[i].st + ';stroke-width:6;' })
        this.linediag[i].i = i
      }
      const s1 = (hauteur - 25 - intervallh * this.tableau[i][1].prez / 2)
      this.linediag[i] = addSvg(this.svg, 'circle', { cx: 35 + intervalle * 1.5 + 2 * (i - 1) * intervalle, cy: s1, r: 6, style: 'fill:' + this.colors[i].fi + ';stroke:' + this.colors[i].st + ';stroke-width:6;' })
      this.linediag[i].i = i
    } else {
      let i
      for (i = 1; i < this.tableau.length - 1; i++) {
        const s1 = (hauteur - 25 - intervallh * this.tableau[i][1].prez / 2)
        const s2 = (hauteur - 25 - intervallh * this.tableau[i + 1][1].prez / 2)
        this.batondiag[i] = addSvg(this.svg, 'line', { x1: 35 + intervalle * 1.5 + (i - 1) * intervalle, y1: s1, x2: 35 + intervalle * 1.5 + (i) * intervalle, y2: s2, style: 'fill:rgb(116, 208, 241);stroke:rgb(6,6,10);stroke-width:1;' })
        this.linediag[i] = addSvg(this.svg, 'circle', { cx: 35 + intervalle * 1.5 + (i - 1) * intervalle, cy: s1, r: 6, style: 'fill:' + this.colors[i].fi + ';stroke:' + this.colors[i].st + ';stroke-width:6;' })
        this.linediag[i].i = i
      }
      const s1 = (hauteur - 25 - intervallh * this.tableau[i][1].prez / 2)
      this.linediag[i] = addSvg(this.svg, 'circle', { cx: 35 + intervalle * 1.5 + (i - 1) * intervalle, cy: s1, r: 6, style: 'fill:' + this.colors[i].fi + ';stroke:' + this.colors[i].st + ';stroke-width:6;' })
      this.linediag[i].i = i
    }

    const el = this.dtextl1
    const { width } = el.getBoundingClientRect()
    this.dtextl1.x.baseVal.getItem(0).value = largeur - width - 5

    let mod = -26
    let mod3 = 0
    if (this.stor.isWritingEnonce) {
      if (this.ds.theme === 'zonesAvecImageDeFond') {
        mod3 = 4
        mod = -14
      }
    }
    this.eltTable.style.position = 'relative'
    if (!this.disabled) {
      if (!this.first) {
        if (this.zoneInputY) {
          for (let i = 0; i < this.zoneInputY.length; i++) {
            j3pDetruit(this.zoneInputY[i].disable())
          }
        }
        for (let i = 0; i < this.gradinput.length; i++) {
          j3pDetruit(this.gradinput[i])
        }
      } else {
        this.first = false
        this.grad = []
        for (let i = 0; i < this.nbtraits - 1; i++) {
          this.grad.push('')
        }
        this.lalistederoul = []
        for (let i = 1; i < this.tableau.length; i++) {
          this.lalistederoul[i] = j3pAddElt(this.eltTable, 'span', { style: { position: 'absolute' } })
          j3pAffiche(this.lalistederoul[i], null, '?')
        }
        if (this.stor.serieEnCours.typeX.indexOf('continu') !== -1) {
          this.lalistederoul[this.tableau.length] = j3pAddElt(this.eltTable, 'span', { style: { position: 'absolute' } })
          j3pAffiche(this.lalistederoul[this.tableau.length], null, '?')
        }
      }
      this.gradinput = []
      this.zoneInputY = []
      if (this.bony) {
        for (let i = 0; i < this.nbtraits - 1; i++) {
          if (this.ds.GraduationsYFaites) {
            this.gradinput[i] = j3pAddElt(this.eltTable, 'span', { style: { position: 'absolute' } })
            j3pAffiche(this.gradinput[i], null, this.stor.serieEnCours.limiteY.pas * (i + 1) + '')
          } else {
            this.gradinput[i] = j3pAddElt(this.eltTable, 'span', { style: { position: 'absolute' } })
            this.zoneInputY[i] = new ZoneStyleMathquill1(this.gradinput[i], { limite: 3, onmodif: this.getZsmModifListener(), contenu: this.grad[i] })
            this.zoneInputY[i].li = i
            this.zoneInputY[i].isblur = false
          }
          this.gradinput[i].style.fontSize = '13px'
        }
      } else {
        for (let i = 0; i < this.nbtraits - 1; i++) {
          this.gradinput[i] = j3pAddElt(this.eltTable, 'span', { style: { position: 'absolute' } })
          j3pAffiche(this.gradinput[i], null, '?')
        }
      }
      const oubase = this.eltTable.getBoundingClientRect()
      for (let i = 0; i < this.nbtraits - 1; i++) {
        const el = tabinput[i]
        const { top, left } = el.getBoundingClientRect()
        const lacase = this.gradinput[i].getBoundingClientRect()
        this.gradinput[i].style.top = (top - oubase.top - lacase.height / 2) + 'px'
        this.gradinput[i].style.left = (left - oubase.left - lacase.width - 5) + 'px'
        this.left = left - oubase.left
      }

      const rectbase = this.svg.getBoundingClientRect()
      for (let i = 1; i < this.tableau.length; i++) {
        const el = this.linediag[i]
        el.style.cursor = 'pointer'
        el.addEventListener('mouseout', this.listenerOut, false)
        el.addEventListener('mouseover', this.listenerOver, false)
        el.addEventListener('mousedown', this.listenerDown, false)
        el.addEventListener('touchstart', this.listenerDown, { capture: false, passive: true })
        el.addEventListener('mouseup', this.listenerUp, false)
        el.addEventListener('touchend', this.listenerUp, false)
        el.addEventListener('touchcancel', this.listenerUp, false)
        if (this.stor.serieEnCours.typeX.indexOf('continu') === -1) {
          this.lalistederoul[i].style.left = (rectbase.left + 35 + intervalle * 1.5 + 2 * (i - 1) * intervalle - oubase.left - this.lalistederoul[i].offsetWidth / 2 + mod3) + 'px'
          const dec = (this.lalistederoul[i].offsetWidth > (intervalle * 2)) ? 30 : 0
          this.lalistederoul[i].style.top = (((i + 1) % 2) * dec + rectbase.top + 10 + hauteur - oubase.top + mod) + 'px'
        } else {
          this.lalistederoul[i].style.left = (rectbase.left + 35 + intervalle + (i - 1) * intervalle - oubase.left - this.lalistederoul[i].offsetWidth / 2 + mod3) + 'px'
          this.lalistederoul[i].style.top = (rectbase.top + hauteur - oubase.top + mod + 5) + 'px'
        }
      }
      if (this.stor.serieEnCours.typeX.indexOf('continu') !== -1) {
        this.lalistederoul[this.tableau.length].style.left = (rectbase.left + 35 + intervalle + (this.tableau.length - 1) * intervalle - oubase.left - this.lalistederoul[this.tableau.length].offsetWidth / 2 + mod3) + 'px'
        this.lalistederoul[this.tableau.length].style.top = (rectbase.top + hauteur - oubase.top + mod + 5) + 'px'
      }
      this.intervh = intervallh

      this.svg.addEventListener('mousemove', (event) => { this.drag(event, getCoordFromMouseEvent(event)) }, { capture: false, passive: true })
      this.svg.addEventListener('touchmove', (event) => { this.drag(event, getCoordFromTouchEvent(event)) }, { capture: false, passive: false }) // getCoordFromTouchEvent fait du event.preventDefault() => passive: false
      this.svg.addEventListener('mouseup', this.listenerUp, false)
      this.svg.addEventListener('touchend', this.listenerUp, false)
      this.parcours.zonesElts.MG.addEventListener('mouseup', this.listenerUp, false)
      this.parcours.zonesElts.MG.addEventListener('touchend', this.listenerUp, false)
      this.parcours.zonesElts.MG.addEventListener('touchcancel', this.listenerUp, false)
    }
    this.tabinput = tabinput
  }

  getZsmModifListener () {
    const rep = this
    /** @this {ZoneStyleMathquill1} */
    return function zsmModifListener () {
      // this est zsm
      rep.grad[this.li] = this.reponse().replace(/ /g, '')
      const { width } = this.conteneur.getBoundingClientRect()
      this.conteneur.style.left = (rep.left - width - 5) + 'px'
    }
  }

  /**
   * Déplace l’élément cliqué (on est appelé au mousemove|touchmove)
   * @param {MouseEvent|TouchEvent} event
   * @param {number} y position verticale (de la souris|touch)
   */
  drag (event, { y }) {
    if (this.disabled || this.draggingElt === null) return
    const el = this.svg
    const { top } = el.getBoundingClientRect(el)
    let ly = y - top
    const pourdiff = this.hauteur - 25 - ly
    const autre = Math.abs(Math.max(0, Math.round(Math.round(pourdiff) / Math.round(this.intervh / 2))))
    ly = (this.hauteur - 25 - this.intervh / 2 * autre)
    ly = Math.max(this.min, Math.min(this.max, ly))
    this.draggingElt.cy.baseVal.value = ly
    this.draggingElt.style.stroke = 'rgb(255,6,10)'
    this.draggingElt.style.fill = 'rgb(116, 208, 241)'
    // this.draggingElt.y2.baseVal.value = ly;
    const monnum = this.draggingElt.i
    if (monnum !== this.tableau.length - 1) {
      this.batondiag[this.draggingElt.i].y1.baseVal.value = ly
    }
    if (monnum !== 1) {
      this.batondiag[monnum - 1].y2.baseVal.value = ly
    }
    this.tableau[this.draggingElt.i][1].prez = autre
    this.colors[monnum] = { fi: 'rgb(116, 208, 241)', st: 'rgb(255,6,10)' }
  }
}

/**
 * section stat01
 * @this {Parcours}
 */
export default function main () {
  const me = this
  const stor = this.storage
  const ds = this.donneesSection

  /**
   * Retourne true si stor.nbeff contient un élément avec cet eff
   * @private
   * @param {number} eff
   * @return {boolean}
   */
  function estPresent (eff) {
    return stor.nbeff.some(n => eff === n.eff)
  }
  /**
   * Incrément la propriété nb de l’élément de stor.nbeff ayant cette valeur eff
   * @private
   * @param {number} eff
   * @return {boolean} false si on l’a pas trouvé
   */
  function augmente (eff) {
    for (const nbe of stor.nbeff) {
      if (nbe.eff === eff) {
        nbe.nb++
        return true
      }
    }
    return false
  }

  /**
   * Retourne la propriété nb de l’élément de stor.nbeff ayant eff à 0
   * @return {number}
   */
  function nbDeZero () {
    for (const nbe of stor.nbeff) {
      if (nbe.eff === 0) return nbe.nb
    }
    return 0
  }

  /**
   * Ajoute des zéros inutiles pour atteindre le nbDecimales voulu (on ajoute rien si on avait déjà ce nb de décimales ou plus)
   * @private
   * @param {number} nb
   * @param {number} nbDecimales
   * @return {string}
   */
  function ajouteZerosInutiles (nb, nbDecimales) {
    nb = String(nb).replace('.', ',')
    if (nbDecimales <= 0) {
      if (nbDecimales < 0) console.error(Error(`nb de décimales invalide : ${nbDecimales}`))
      return nb
    }
    if (!nb.includes(',')) nb += ','
    const longueurVoulue = nb.indexOf(',') + 1 + nbDecimales
    while (nb.length < longueurVoulue) nb += '0'
    return nb
  }

  /**
   * Barre l’élément dont on est le listener
   * @this {HTMLElement}
   */
  function barrele () {
    // on est un listener, this est l’élément sur lequel on écoute (un span)
    if (this.labarre) {
      j3pDetruit(this.labarre)
      this.labarre = null
    } else {
      this.labarre = barreZone(this)
    }
  }

  function yareponse () {
    return stor.represente.yareponse()
  }

  function bonneReponse () {
    stor.Erbonx = false
    stor.Erbony = false
    stor.ErInvTab = false
    stor.Ergrady = false
    stor.ErgradyPetit = false
    stor.Ergradx = false
    stor.ErManqval = false
    stor.ErValTrop = false
    stor.ErValdouble = false
    stor.ErEffectif = false
    stor.ErOrdDisc = false
    stor.listgrady = []
    stor.listgradx = []
    stor.listManqval = []
    stor.listtrop = []
    stor.listdouble = []
    stor.listeff = []

    stor.repEleve = stor.represente.reponse()
    /// test bony
    stor.Erbony = (stor.repEleve.tablo[0][1].choix !== stor.serieEnCours.legendeY)
    /// test bonx
    stor.Erbonx = (stor.repEleve.tablo[0][0].choix !== stor.serieEnCours.legendeX)

    stor.l = 1
    if (stor.reponseEnCours === 'tableau') {
      if ((stor.Erbony) && (stor.Erbonx)) {
        if ((stor.repEleve.tablo[0][1].choix === stor.serieEnCours.legendeX) && (stor.repEleve.tablo[0][0].choix === stor.serieEnCours.legendeY)) {
          stor.Erbony = stor.Erbonx = false
          stor.ErInvTab = true
          stor.l = 0
        }
      }
    }
    /// test bonne grady
    let grabase
    if (stor.repEleve.grady !== undefined) {
      grabase = parseInt(stor.repEleve.grady[0])
      for (let i = 1; i < stor.repEleve.grady.length; i++) {
        if (Math.abs(grabase * (i + 1) - stor.repEleve.grady[i]) > Math.pow(10, -10)) {
          stor.Ergrady = true
          stor.listgrady.push(i)
        }
      }
    }

    /// test si unite pas trop petite
    if (stor.repEleve.nbtraits !== undefined) {
      if (stor.repEleve.grady[0] !== '') {
        if (grabase !== undefined) {
          if (stor.effmax > stor.repEleve.nbtraits * grabase) {
            stor.ErgradyPetit = true
            stor.listgrady.push(0)
          }
        }
      }
    }

    // test bon gradx
    if (stor.serieEnCours.typeX.indexOf('continu') !== -1) {
      if (stor.repEleve.gradx !== undefined) {
        for (let i = 0; i < stor.repEleve.gradx.length; i++) {
          if (Math.abs(stor.serieEnCours.limiteX.min + i * stor.serieEnCours.limiteX.amplitude - Number(String(stor.repEleve.gradx[i]).replace(',', '.'))) > Math.pow(10, -10)) {
            stor.Ergradx = true
            stor.listgradx.push(i + 1)
          }
        }
      }
    }

    /// test presence toutes valeurs !== 0
    if (!stor.Erbonx) {
      if ((stor.serieEnCours.typeX.indexOf('continu') === -1) || (['batons', 'graphique'].indexOf(stor.reponseEnCours) === -1)) {
        let l = 0
        if (stor.ErInvTab) l = 1

        for (let i = 0; i < stor.effectifs1.length; i++) {
          let ok = false
          if (stor.effectifs1[i].effectif === 0) continue
          for (let j = 1; j < stor.repEleve.tablo.length; j++) {
            if (String(stor.repEleve.tablo[j][l].choix) === String(stor.effectifs1[i].valeur)) {
              ok = true
              break
            }
          }
          if (!ok) {
            stor.ErManqval = true
            stor.listManqval.push(stor.effectifs1[i].valeur)
          }
        }
      }
    }

    /// test pas de valeurs en trop
    if (stor.reponseEnCours === 'circulaire') {
      for (let i = 1; i < stor.repEleve.tablo.length; i++) {
        let ok = false
        for (let j = 0; j < stor.effectifs1.length; j++) {
          if (String(stor.repEleve.tablo[i][0].choix) === String(stor.effectifs1[j].valeur)) {
            ok = true
            break
          }
        }
        if (!ok) {
          stor.ErValTrop = true
          stor.listtrop.push(stor.repEleve.tablo[i][0].choix)
        }
      }
    }

    // test pas valeurs en double
    if (!stor.Erbonx) {
      if ((stor.serieEnCours.typeX.indexOf('continu') === -1) || (['batons', 'graphique'].indexOf(stor.reponseEnCours) === -1)) {
        const l = (stor.ErInvTab) ? 1 : 0
        for (let i = 1; i < stor.repEleve.tablo.length - 1; i++) {
          let ok = true
          for (let j = i + 1; j < stor.repEleve.tablo.length; j++) {
            if (String(stor.repEleve.tablo[i][l].choix) === stor.repEleve.tablo[j][l].choix) {
              ok = false
              break
            }
          }
          if (!ok) {
            stor.ErValdouble = true
            if (stor.listdouble.indexOf(stor.repEleve.tablo[i][l].choix) === -1) {
              stor.listdouble.push(stor.repEleve.tablo[i][l].choix)
            }
          }
        }
      }
    }

    if ((!stor.Erbony) && (!stor.Erbonx) && (!stor.Ergradx)) {
      /// test bons effectifs
      if ((stor.serieEnCours.typeX.indexOf('continu') === -1) || (['batons', 'graphique'].indexOf(stor.reponseEnCours) === -1)) {
        const l = stor.ErInvTab ? 0 : 1

        for (let i = 1; i < stor.repEleve.tablo.length; i++) {
          if (stor.listtrop.indexOf(stor.repEleve.tablo[i][1 - l].choix) !== -1) continue
          let attendu
          for (let j = 0; j < stor.effectifs1.length; j++) {
            if (String(stor.effectifs1[j].valeur) === String(stor.repEleve.tablo[i][1 - l].choix)) {
              attendu = stor.effectifs1[j].effectif
              break
            }
          }
          if (Math.abs(attendu - stor.repEleve.tablo[i][l].prez) > Math.pow(10, -10)) {
            stor.ErEffectif = true
            stor.listeff.push(i)
          }
        }
      } else {
        for (let i = 0; i < stor.effectifs1.length; i++) {
          if (Math.abs(stor.effectifs1[i].effectif - stor.repEleve.tablo[i + 1][1].prez) > Math.pow(10, -10)) {
            stor.ErEffectif = true
            stor.listeff.push(i + 1)
          }
        }
      }

      /// ++ ordre sur valeurs discret
    }
    return (!stor.ErgradyPetit) && (!stor.Erbonx) && (!stor.Erbony) && (!stor.Ergrady) && (!stor.Ergradx) && (!stor.ErManqval) && (!stor.ErValTrop) && (!stor.ErEffectif)
  }

  function dessineco (ou, koi, color) {
    const hauteur = 200
    const largeur = 500
    const color2 = color === '#000000' ? '#aaaaaa' : colorCorrection

    switch (koi) {
      case 'liste':
        stor.lesdiv.diag.style.lineHeight = '190%'
        j3pAddContent(stor.lesdiv.consigne1, 'La liste ci-dessous indique ' + stor.serieEnCours.titre + '&nbsp;: ')
        for (let i = 0; i < stor.liste.length; i++) {
          const buf = stor.liste[i]
          j3pAddContent(stor.lesdiv.diag, ' &nbsp;')
          const ell = j3pAddElt(stor.lesdiv.diag, 'span')
          j3pAddContent(stor.lesdiv.diag, ' &nbsp;')
          j3pAffiche(ell, null, '&nbsp;' + buf + '&nbsp;')
          ell.style.background = '#F7BE81'
          ell.style.cursor = 'pointer'
          ell.style.border = '1px solid black'
          ell.addEventListener('click', barrele, false)
        }
        break
      case 'tableau': {
        const tt = addDefaultTable(ou, 2, stor.effectifs1.length + 1)
        for (let i = 0; i < stor.effectifs1.length + 1; i++) {
          tt[0][i].style.padding = '0px 4px 0px 4px'
          tt[1][i].style.padding = '0px 4px 0px 4px'
          tt[0][i].style.border = '1px solid blue'
          tt[1][i].style.border = '1px solid blue'
          tt[0][i].style.textAlign = 'center'
          tt[1][i].style.textAlign = 'center'
          tt[0][i].style.color = color
          tt[1][i].style.color = color
        }
        j3pAffiche(tt[0][0], null, String(stor.serieEnCours.legendeX))
        j3pAffiche(tt[1][0], null, String(stor.serieEnCours.legendeY))
        for (let i = 0; i < stor.effectifs1.length; i++) {
          j3pAffiche(tt[0][i + 1], null, String(stor.effectifs1[i].valeur))
          j3pAffiche(tt[1][i + 1], null, String(stor.effectifs1[i].effectif))
        }
      }
        break
      case 'batons': {
        const svgco = j3pCreeSVG(ou, { width: largeur, height: hauteur, viewBox: '0 0 ' + largeur + ' ' + hauteur })
        addSvg(svgco, 'rect', { x: 0, y: 0, width: largeur, height: hauteur, style: 'fill:rgb(200,200,200);stroke-width:0;stroke:rgb(0,0,0)' })
        // det cb traits horizontaux
        let effmax = 0
        for (let i = 0; i < stor.effectifs1.length; i++) {
          if (effmax < stor.effectifs1[i].effectif) effmax = stor.effectifs1[i].effectif
        }
        const nbtraits = Math.trunc(effmax / stor.serieEnCours.limiteY.pas + 2)
        const intervallh = (hauteur - 50) / nbtraits
        const nbinterv = 2 * stor.effectifs1.length + 1
        const intervalle = (largeur - 150) / nbinterv
        // fleches et traits pointilles horizontaux
        addSvg(svgco, 'line', { x1: 45, y1: hauteur - 25, x2: largeur - 5, y2: hauteur - 25, style: 'stroke:' + color + ';fill:' + color + ';stroke-width:2' })
        addSvg(svgco, 'line', { x1: largeur - 15, y1: hauteur - 35, x2: largeur - 5, y2: hauteur - 25, style: 'stroke:' + color + ';fill:' + color + ';stroke-width:2' })
        addSvg(svgco, 'line', { x1: largeur - 15, y1: hauteur - 15, x2: largeur - 5, y2: hauteur - 25, style: 'stroke:' + color + ';fill:' + color + ';stroke-width:2' })
        addSvg(svgco, 'line', { x1: 45, y1: hauteur - 24, x2: 45, y2: 5, style: 'stroke:' + color + ';fill:' + color + ';stroke-width:2' })
        addSvg(svgco, 'line', { x1: 35, y1: 20, x2: 45, y2: 5, style: 'stroke:' + color + ';fill:' + color + ';stroke-width:2' })
        addSvg(svgco, 'line', { x1: 55, y1: 20, x2: 45, y2: 5, style: 'stroke:' + color + ';fill:' + color + ';stroke-width:2' })

        const ttom = []
        for (let i = 0; i < nbtraits - 1; i++) {
          addSvg(svgco, 'line', { x1: 45, y1: hauteur - 25 - (i + 1) * intervallh, x2: largeur - 105, y2: hauteur - 25 - (i + 1) * intervallh, style: 'stroke:' + color + ';fill:' + color + ';stroke-width:1;stroke-dasharray:4' })
          ttom[i] = addSvg(svgco, 'text', { x: 45, y: hauteur - 21 - (i + 1) * intervallh, style: 'font-size:15px;stroke:' + color + ';fill:' + color, text: String(stor.serieEnCours.limiteY.pas * (i + 1)) })
        }
        // legendes
        addSvg(svgco, 'text', { x: 60, y: 15, style: 'font-size:15px;stroke:' + color + ';fill:' + color, text: String(stor.serieEnCours.legendeY) })
        const textl1C = addSvg(svgco, 'text', { x: 0, y: hauteur - 40, style: 'font-size:15px;stroke:' + color + ';fill:' + color, text: String(stor.serieEnCours.legendeX) })

        // les batons
        const ttam = []
        const ttim = []
        if (stor.serieEnCours.typeX === 'continu') {
          for (let i = 0; i < stor.effectifs1.length; i++) {
            const s = (5 + (effmax - stor.effectifs1[i].effectif + 2 * stor.serieEnCours.limiteY.pas) / stor.serieEnCours.limiteY.pas * intervallh)
            addSvg(svgco, 'rect', { x: 35 + intervalle + 2 * i * intervalle, y: s + 20, width: 2 * intervalle, height: hauteur - 45 - s, style: 'stroke:black;fill:' + color2 + ';stroke-width:1;' })
            ttam[i] = addSvg(svgco, 'text', { x: 35 + intervalle + 2 * i * intervalle, y: hauteur - 5, style: 'font-size:15px;stroke:' + color + ';fill:' + color, text: ajouteZerosInutiles(j3pArrondi(stor.serieEnCours.limiteX.min + i * stor.serieEnCours.limiteX.amplitude, 5), stor.serieEnCours.limiteX.nbZerosInutiles) })
          }
          ttam[stor.effectifs1.length] = addSvg(svgco, 'text', { x: 35 + intervalle + 2 * stor.effectifs1.length * intervalle, y: hauteur - 5, style: 'font-size:15px;stroke:' + color + ';fill:' + color, text: ajouteZerosInutiles(j3pArrondi(stor.serieEnCours.limiteX.min + stor.effectifs1.length * stor.serieEnCours.limiteX.amplitude, 5), stor.serieEnCours.limiteX.nbZerosInutiles) })
        } else {
          for (let i = 0; i < stor.effectifs1.length; i++) {
            const s = (5 + (effmax - stor.effectifs1[i].effectif + 2 * stor.serieEnCours.limiteY.pas) / stor.serieEnCours.limiteY.pas * intervallh)
            addSvg(svgco, 'rect', { x: 35 + intervalle + 2 * i * intervalle, y: s + 20, width: intervalle, height: hauteur - 45 - s, style: 'stroke:black;fill:' + color2 + ';stroke-width:1;' })
            ttim[i] = addSvg(svgco, 'text', { x: 35 + intervalle + 2 * i * intervalle, y: hauteur - 5, style: 'font-size:15px;stroke:' + color + ';fill:' + color, text: String(stor.effectifs1[i].valeur) })
          }
        }

        // termine

        // replace les graduations
        for (let i = 0; i < nbtraits - 1; i++) {
          const el = ttom[i]
          const { width } = el.getBoundingClientRect()
          ttom[i].x.baseVal.getItem(0).value = ttom[i].x.baseVal.getItem(0).value - width - 2
        }
        let mmax = 0
        if (stor.serieEnCours.typeX === 'continu') {
          for (let i = 0; i < stor.effectifs1.length + 1; i++) {
            const el = ttam[i]
            const { width } = el.getBoundingClientRect()
            mmax = Math.max(mmax, width)
            el.x.baseVal.getItem(0).value -= width / 2
          }
          if (mmax > (2 * intervalle) - 2) {
            for (let i = 0; i < stor.effectifs1.length; i++) {
              ttam[i].y.baseVal.getItem(0).value += 10 * (i % 2) - 8
            }
          }
        } else {
          for (let i = 0; i < stor.effectifs1.length; i++) {
            const el = ttim[i]
            const { width } = el.getBoundingClientRect()
            mmax = Math.max(mmax, width)
            ttim[i].x.baseVal.getItem(0).value = (ttim[i].x.baseVal.getItem(0).value + (intervalle - width) / 2)
          }
          if (mmax > (2 * intervalle) - 2) {
            for (let i = 0; i < stor.effectifs1.length; i++) {
              ttim[i].y.baseVal.getItem(0).value += 11 * (i % 2) - 8
            }
          }
        }
        const el = textl1C
        const { width } = el.getBoundingClientRect()
        el.x.baseVal.getItem(0).value = largeur - (width + 5)
      }
        break
      case 'graphique': {
        const svgco = j3pCreeSVG(ou, { width: largeur, height: hauteur, viewBox: '0 0 ' + largeur + ' ' + hauteur })
        addSvg(svgco, 'rect', { x: 0, y: 0, width: largeur, height: hauteur, style: 'fill:rgb(200,200,200);stroke-width:0;stroke:rgb(0,0,0)' })
        // det cb traits horizontaux
        let effmax = 0
        for (let i = 0; i < stor.effectifs1.length; i++) {
          if (effmax < stor.effectifs1[i].effectif) {
            effmax = stor.effectifs1[i].effectif
          }
        }
        const nbtraits = Math.trunc(effmax / stor.serieEnCours.limiteY.pas + 2)
        const intervallh = (hauteur - 50) / nbtraits
        const nbinterv = 2 * stor.effectifs1.length + 1
        const intervalle = (largeur - 150) / nbinterv
        // fleches et traits pointilles horizontaux
        addSvg(svgco, 'line', { x1: 45, y1: hauteur - 25, x2: largeur - 5, y2: hauteur - 25, style: 'stroke:' + color + ';fill:' + color + ';stroke-width:2' })
        addSvg(svgco, 'line', { x1: largeur - 15, y1: hauteur - 35, x2: largeur - 5, y2: hauteur - 25, style: 'stroke:' + color + ';fill:' + color + ';stroke-width:2' })
        addSvg(svgco, 'line', { x1: largeur - 15, y1: hauteur - 15, x2: largeur - 5, y2: hauteur - 25, style: 'stroke:' + color + ';fill:' + color + ';stroke-width:2' })
        addSvg(svgco, 'line', { x1: 45, y1: hauteur - 24, x2: 45, y2: 5, style: 'stroke:' + color + ';fill:' + color + ';stroke-width:2' })
        addSvg(svgco, 'line', { x1: 35, y1: 20, x2: 45, y2: 5, style: 'stroke:' + color + ';fill:' + color + ';stroke-width:2' })
        addSvg(svgco, 'line', { x1: 55, y1: 20, x2: 45, y2: 5, style: 'stroke:' + color + ';fill:' + color + ';stroke-width:2' })

        const ttom = []
        for (let i = 0; i < nbtraits - 1; i++) {
          addSvg(svgco, 'line', { x1: 45, y1: hauteur - 25 - (i + 1) * intervallh, x2: largeur - 105, y2: hauteur - 25 - (i + 1) * intervallh, style: 'stroke:' + color + ';fill:' + color + ';stroke-width:1;stroke-dasharray:4' })
          ttom[i] = addSvg(svgco, 'text', { x: 45, y: hauteur - 21 - (i + 1) * intervallh, style: 'font-size:15px;stroke:' + color + ';fill:' + color, text: String(stor.serieEnCours.limiteY.pas * (i + 1)) })
        }
        // legendes
        addSvg(svgco, 'text', { x: 60, y: 15, style: 'font-size:15px;stroke:' + color + ';fill:' + color, text: String(stor.serieEnCours.legendeY) })
        const textl1C = addSvg(svgco, 'text', { x: 0, y: hauteur - 40, style: 'font-size:15px;stroke:' + color + ';fill:' + color, text: String(stor.serieEnCours.legendeX) })

        // les batons
        const ttam = []
        const ttim = []
        let dec
        if (stor.serieEnCours.typeX === 'continu') {
          for (let i = 0; i < stor.effectifs1.length; i++) {
            const s = (5 + (effmax - stor.effectifs1[i].effectif + 2 * stor.serieEnCours.limiteY.pas) / stor.serieEnCours.limiteY.pas * intervallh)
            addSvg(svgco, 'circle', { cx: 35 + 2 * intervalle + 2 * i * intervalle, cy: s + 20, r: 6, style: 'fill:rgb(255, 0, 0);stroke:rgb(6,6,10);stroke-width:0;' })
            ttam[i] = addSvg(svgco, 'text', { x: 35 + intervalle + 2 * i * intervalle, y: hauteur - 5, style: 'font-size:15px;stroke:' + color + ';fill:' + color, text: ajouteZerosInutiles(j3pArrondi(stor.serieEnCours.limiteX.min + i * stor.serieEnCours.limiteX.amplitude, 5), stor.serieEnCours.limiteX.nbZerosInutiles) })
          }
          ttam[stor.effectifs1.length] = addSvg(svgco, 'text', { x: 35 + intervalle + 2 * stor.effectifs1.length * intervalle, y: hauteur - 5, style: 'font-size:15px;stroke:' + color + ';fill:' + color, text: ajouteZerosInutiles(j3pArrondi(stor.serieEnCours.limiteX.min + stor.effectifs1.length * stor.serieEnCours.limiteX.amplitude, 5), stor.serieEnCours.limiteX.nbZerosInutiles) })
          dec = 0.5 * intervalle
        } else {
          dec = 0
          for (let i = 0; i < stor.effectifs1.length; i++) {
            const s = (5 + (effmax - stor.effectifs1[i].effectif + 2 * stor.serieEnCours.limiteY.pas) / stor.serieEnCours.limiteY.pas * intervallh)
            addSvg(svgco, 'circle', { cx: 35 + 1.5 * intervalle + 2 * i * intervalle, cy: s + 20, r: 5, style: 'fill:rgb(255, 0, 0);stroke:rgb(6,6,10);stroke-width:0;' })
            ttim[i] = addSvg(svgco, 'text', { x: 35 + intervalle + 2 * i * intervalle, y: hauteur - 5, style: 'font-size:15px;stroke:' + color + ';fill:' + color, text: String(stor.effectifs1[i].valeur) })
          }
        }
        for (let i = 0; i < stor.effectifs1.length - 1; i++) {
          const s1 = (5 + (effmax - stor.effectifs1[i].effectif + 2 * stor.serieEnCours.limiteY.pas) / stor.serieEnCours.limiteY.pas * intervallh)
          const s2 = (5 + (effmax - stor.effectifs1[i + 1].effectif + 2 * stor.serieEnCours.limiteY.pas) / stor.serieEnCours.limiteY.pas * intervallh)
          addSvg(svgco, 'line', { x1: dec + 35 + 1.5 * intervalle + 2 * i * intervalle, y1: s1 + 20, x2: dec + 35 + 1.5 * intervalle + 2 * (i + 1) * intervalle, y2: s2 + 20, style: 'fill:rgb(255, 0, 0);stroke:' + color + ';stroke-width:2;' })
        }

        // replace les graduations
        for (let i = 0; i < nbtraits - 1; i++) {
          const el = ttom[i]
          const { width } = el.getBoundingClientRect()
          ttom[i].x.baseVal.getItem(0).value = (ttom[i].x.baseVal.getItem(0).value - width - 2)
        }
        let mmax = 0
        if (stor.serieEnCours.typeX === 'continu') {
          for (let i = 0; i < stor.effectifs1.length + 1; i++) {
            const el = ttam[i]
            const { width } = el.getBoundingClientRect()
            mmax = Math.max(mmax, width)
            el.x.baseVal.getItem(0).value -= width / 2
          }
          if (mmax > (2 * intervalle) - 2) {
            for (let i = 0; i < stor.effectifs1.length; i++) {
              ttam[i].y.baseVal.getItem(0).value += 10 * (i % 2) - 8
            }
          }
        } else {
          for (let i = 0; i < stor.effectifs1.length; i++) {
            const el = ttim[i]
            const { width } = el.getBoundingClientRect()
            mmax = Math.max(mmax, width)
            ttim[i].x.baseVal.getItem(0).value = (ttim[i].x.baseVal.getItem(0).value + (intervalle - width) / 2)
          }
          if (mmax > (2 * intervalle) - 2) {
            for (let i = 0; i < stor.effectifs1.length; i++) {
              ttim[i].y.baseVal.getItem(0).value += 11 * (i % 2) - 8
            }
          }
        }
        const { width } = textl1C.getBoundingClientRect()
        textl1C.x.baseVal.getItem(0).value = largeur - (width + 5)
      }
        break
      case 'circulaire': {
        const listcoul = ['(116, 208, 241)', '(22, 184, 78)', '(152, 87, 23)', '(187, 11, 11)', '(218, 179, 10)', '(158, 158, 158)', '(255,255,200)', '(255,255,255)']
        const svgco = j3pCreeSVG(ou, { width: largeur, height: hauteur, viewBox: '0 0 ' + largeur + ' ' + hauteur })
        addSvg(svgco, 'rect', { x: 0, y: 0, width: largeur, height: hauteur, style: 'fill:rgb(200,200,200);stroke-width:0;stroke:rgb(0,0,0)' })
        addSvg(svgco, 'circle', { cx: largeur / 2 - 100, cy: hauteur / 2, r: (hauteur - 10) / 2, style: 'fill:rgb(255, 255, 255);stroke:rgb(6,6,10);stroke-width:2;' })
        let angle = 0
        const lestext = []
        const ttext = []
        for (let i = 0; i < stor.effectifs1.length; i++) {
          const angle2 = angle + (stor.effectifs1[i].effectif / stor.effectifTotal * 360)
          const lm = ((stor.effectifs1[i].effectif / stor.effectifTotal * 360) <= 180)
            ? '0,0,1'
            : '1,1,1'
          addSvg(svgco, 'path', { d: 'M ' + ((largeur / 2 - 100) + Math.cos(angle * Math.PI / 180) * (hauteur - 10) / 2) + ' ' + ((hauteur / 2) + Math.sin(angle * Math.PI / 180) * (hauteur - 10) / 2) + '  A ' + (hauteur - 10) / 2 + ' ' + (hauteur - 10) / 2 + ', ' + lm + ', ' + ((largeur / 2 - 100) + Math.cos(angle2 * Math.PI / 180) * (hauteur - 10) / 2) + ' ' + ((hauteur / 2) + Math.sin(angle2 * Math.PI / 180) * (hauteur - 10) / 2) + ' L ' + (largeur / 2 - 100) + ' ' + (hauteur / 2) + ' Z', style: 'stroke-width:1;stroke:rgb(0,0,0);fill:rgb' + listcoul[i] })
          addSvg(svgco, 'rect', { x: largeur / 2 + 20, y: 5 + i * 25, width: 50, height: 15, style: 'fill:rgb' + listcoul[i] + ';stroke-width:1;stroke:rgb(0,0,0)' })
          addSvg(svgco, 'text', { x: largeur / 2 + 75, y: 18 + i * 25, style: 'font-size:15px;stroke:' + color + ';fill:' + color, text: String(stor.effectifs1[i].valeur) })

          if (stor.effectifs1[i].effectif !== 0) {
            ttext[i] = addSvg(svgco, 'text', { x: 0, y: 0, r: (hauteur - 10) / 2, style: 'font-size:15px;stroke:' + color + ';fill:' + color, text: String(stor.effectifs1[i].effectif) })
          }
          lestext.push((angle + angle2) / 2)
          angle = angle2
        }

        // termine

        for (let i = 0; i < stor.effectifs1.length; i++) {
          if (stor.effectifs1[i].effectif !== 0) {
            const el = ttext[i]
            const { width, height } = el.getBoundingClientRect()
            ttext[i].x.baseVal.getItem(0).value = (largeur / 2 - 100) + Math.cos(lestext[i] * Math.PI / 180) * (hauteur - 10) / 3 - width / 2
            ttext[i].y.baseVal.getItem(0).value = (hauteur / 2) + Math.sin(lestext[i] * Math.PI / 180) * (hauteur - 10) / 3 + height / 2
          }
        }
      }
        break
      default :
        j3pShowError(Error(`Cas non prévu (${stor.donneeEnCours})`), { message: 'Erreur interne (seriesPossibles non prévu)', mustNotify: true })
    }
  }

  function affcofo (boolfin) {
    let yaexplik = false

    if (boolfin) stor.represente.desactive()

    stor.lesdiv.explik.innerHTML = ''
    stor.represente.toutvert()
    if ((!stor.Erbony) && (!stor.Erbonx) && (!stor.Ergradx)) {
      stor.represente.touteffvert(stor.l)
      stor.represente.toutvalvert()
    }

    if (stor.Erbony) {
      stor.represente.colorlegendey(false, boolfin)
    } else {
      if (stor.Ergrady) {
        j3pAffiche(stor.lesdiv.explik, null, textes.grady + '\n')
        yaexplik = true
      }
      if (stor.ErgradyPetit) {
        j3pAffiche(stor.lesdiv.explik, null, textes.gradypetit + '\n')
        yaexplik = true
      }
      if (stor.listgrady.length > 0) {
        stor.represente.colorgrady(false, stor.listgrady, boolfin)
      }
    }
    if (stor.Erbonx) {
      stor.represente.colorlegendex(false, boolfin)
    } else {
      if (stor.listgradx.length > 0) {
        stor.represente.colorgradx(false, stor.listgradx, boolfin)
      }
    }
    if (stor.ErManqval) {
      let buf = (stor.listManqval.length > 1) ? textes.manquevals : textes.manqueval

      let listval = '<b>"' + stor.listManqval[0] + '"</b>'
      for (let i = 1; i < stor.listManqval.length; i++) {
        listval += ' ; <b>"' + stor.listManqval[i] + '"</b>'
      }
      const li = listval.lastIndexOf(';')
      if (li !== -1) listval = listval.substring(0, li) + textes.et + listval.substring(li + 1)
      buf = buf.replace('£a', listval)
      j3pAffiche(stor.lesdiv.explik, null, buf + '\n')
      yaexplik = true
    }
    if (stor.ErValTrop) {
      stor.represente.corrigeval(false, stor.listtrop, stor.l, boolfin)
      let buf = (stor.listtrop.length > 1) ? textes.valtrops : textes.valtrop

      let listval = '<b>"' + stor.listtrop[0] + '"</b>'
      for (let i = 1; i < stor.listtrop.length; i++) {
        listval += ' ; <b>"' + stor.listtrop[i] + '"</b>'
      }
      const li = listval.lastIndexOf(';')
      if (li !== -1) {
        listval = listval.substring(0, li) + textes.et + listval.substring(li + 1)
      }
      buf = buf.replace('£a', listval)
      j3pAffiche(stor.lesdiv.explik, null, buf + '\n')
      yaexplik = true
    }

    if (stor.ErValdouble) {
      stor.represente.corrigeval(false, stor.listdouble, stor.l, boolfin)
      let buf = (stor.listdouble.length > 1) ? textes.valdoubles : textes.valdouble

      let listval = '<b>"' + stor.listdouble[0] + '"</b>'
      for (let i = 1; i < stor.listdouble.length; i++) {
        listval += ' ; <b>"' + stor.listdouble[i] + '"</b>'
      }
      const li = listval.lastIndexOf(';')
      if (li !== -1) listval = listval.substring(0, li) + textes.et + listval.substring(li + 1)
      buf = buf.replace('£a', listval)
      j3pAffiche(stor.lesdiv.explik, null, buf + '\n')
      yaexplik = true
    }

    if (stor.ErEffectif) {
      stor.represente.corrigeeffectifs(false, stor.listeff, stor.l, boolfin)
    }

    if (boolfin) {
      /// / puis dessine co
      dessineco(stor.lesdiv.explik2, stor.reponseEnCours, me.styles.cco)
    }

    if (yaexplik) stor.lesdiv.explik.classList.add('explique')
    if (boolfin) stor.lesdiv.explik2.classList.add('correction')
  }

  function initSection () {
    me.construitStructurePage('presentation3')
    stor.isWritingEnonce = true

    /**
     * les types de représentation possibles dans l’énoncé (données source, parmi liste, tableau, graphique, batons, circulaire)
     * @type {Stat01TypesRepresentation[]}
     */
    stor.donneesPossibles = []
    if (ds.DonneesListe) stor.donneesPossibles.push('liste')
    if (ds.DonneesTableau) stor.donneesPossibles.push('tableau')
    if (ds.DonneesGraphique) stor.donneesPossibles.push('graphique')
    if (ds.DonneesBatons) stor.donneesPossibles.push('batons')
    if (ds.DonneesCirculaire) stor.donneesPossibles.push('circulaire')
    // on en veut au moins une
    if (stor.donneesPossibles.length === 0) {
      console.error(Error('Paramétrage incohérent : aucun type de représentation possible pour les données => ils seront tous autorisés'))
      stor.donneesPossibles = typesRepresentation
    }

    /**
     * Les types de représentation possibles comme réponse demandée
     * @type {Stat01TypesRepresentation[]}
     */
    stor.reponsesPossibles = []
    if (ds.ReponseTableau) stor.reponsesPossibles.push('tableau')
    if (ds.ReponseGraphique) stor.reponsesPossibles.push('graphique')
    if (ds.ReponseBatons) stor.reponsesPossibles.push('batons')
    if (ds.ReponseCirculaire) stor.reponsesPossibles.push('circulaire')
    if (stor.reponsesPossibles.length === 0) {
      console.error('Paramétrage incohérent : aucun type de représentation possible pour les réponses => ils seront tous autorisés')
      stor.reponsesPossibles = typesRepresentation
    }

    // si y’a qu’un seul possible en réponse, alors il ne doit pas être dans donnees
    if (stor.reponsesPossibles.length === 1 && stor.donneesPossibles.includes(stor.reponsesPossibles[0])) {
      const typeRep = stor.reponsesPossibles[0]
      stor.donneesPossibles = stor.donneesPossibles.filter(type => type !== typeRep)
      if (stor.donneesPossibles.length === 0) {
        console.error(`Paramétrage incohérent : ${typeRep} est le seul type de réponse possible, mais aussi le seul type de données possible => on autorise tous les autres pour les données`)
        stor.donneesPossibles = typesRepresentation.filter(type => type !== typeRep)
      } else {
        console.error(`Paramétrage incohérent : ${typeRep} est le seul type de réponse possible, il ne doit pas figurer dans les données`)
      }
    }
    // et réciproquement
    if (stor.donneesPossibles.length === 1 && stor.reponsesPossibles.includes(stor.donneesPossibles[0])) {
      const typeDonnees = stor.donneesPossibles[0]
      stor.reponsesPossibles = stor.reponsesPossibles.filter(type => type !== typeDonnees)
      if (stor.reponsesPossibles.length === 0) {
        console.error(`Paramétrage incohérent : ${typeDonnees} est le seul type de données possible, mais aussi le seul type de réponse possible => on autorise tous les autres pour les réponses`)
        stor.reponsesPossibles = typesRepresentation.filter(type => type !== typeDonnees)
      } else {
        console.error(`Paramétrage incohérent : ${typeDonnees} est le seul type de données possible, il ne doit pas figurer dans les réponses`)
      }
    }
    if ((stor.donneesPossibles.includes('graphique') || stor.reponsesPossibles.includes('graphique')) && ds.Type_valeur !== 'les trois' && !ds.Type_valeur.includes('continu')) {
      console.error('Paramétrage incohérent : il faut des séries avec un paramètre continu pour les graphiques cartésiens, elles seront ajoutées automatiquement')
      ds.Type_valeur += ' et continu'
    }

    /**
     * Les séries dans lesquelles on peut piocher (construit d’après le paramétrage)
     * @type {Stat01Serie[]}
     */
    stor.seriesPossibles = []
    for (const cas of seriesPossibles) {
      if (ds.Type_valeur !== 'les trois') {
        if (!ds.Type_valeur.includes('Qualitative') && cas.typeX === 'qualitatif') continue
        if (!ds.Type_valeur.includes('continu') && cas.typeX === 'continu') continue
        if (!ds.Type_valeur.includes('discret') && cas.typeX === 'discret') continue
      }
      // si toujours là on prend
      stor.seriesPossibles.push(cas)
    }

    /**
     * Les types de données déjà obtenues aux répétitions précédentes
     * @type {Stat01TypesRepresentation}
     */
    stor.donneesFaites = []
    /**
     * Les type de réponses déjà demandées aux répétitions précédentes
     * @type {Stat01TypesRepresentation}
     */
    stor.reponsesFaites = []
    /**
     * Les ids des séries déjà obtenues
     * @type {string[]}
     */
    stor.seriesFaites = []

    /* Par convention,
        me.typederreurs[0] = nombre de bonnes réponses
        me.typederreurs[1] = nombre de fois où l’élève a utilisé une chance supplémentaire
        me.typederreurs[2] = nombre de mauvaises réponses
        me.typederreurs[10] = nombre de fois où l’élève a eu faux à cause d’un temps limite
        LES AUTRES peuvent être utilisées par la détermination de la pe dans le seriesPossibles d’une section qualitative
     */
    me.typederreurs = [0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0]
    me.afficheTitre('Représenter une série statistique ')
    if (ds.indication) me.indication(me.zones.IG, ds.indication)
    enonceMain()
  }

  function enonceMain () {
    me.videLesZones()
    let nbTirages = 0
    while (nbTirages < 3) {
      nbTirages++
      if (genereAlea()) break
    }
    if (nbTirages > 2) {
      return j3pShowError(Error('Désolé, impossible de construire une série statistique cohérente avec les contraintes imposées'), { notifyData: { donneesSection: ds, storage: stor, section: me.nomSection } })
    }

    // on ajoute ce tirage
    stor.reponsesFaites.push(stor.reponseEnCours)
    stor.donneesFaites.push(stor.donneeEnCours)
    stor.seriesFaites.push(stor.serieEnCours.id)

    // on mélange la liste des effectifs
    stor.liste = j3pShuffle(stor.liste)

    stor.effectifTotal = stor.effectifs1.reduce((total, { effectif }) => effectif + total, 0)

    // cree conteneurs
    stor.lesdiv = {}
    stor.lesdiv.conteneur = j3pAddElt(me.zones.MG, 'div', {
      style: me.styles.etendre('toutpetit.enonce', { padding: '10px' })
    })

    if (stor.donneeEnCours === 'liste' || stor.donneeEnCours === 'tableau') {
      if (stor.reponseEnCours === 'tableau') {
        stor.lesdiv.consigne = j3pAddElt(stor.lesdiv.conteneur, 'div')
        stor.lesdiv.consigne1 = j3pAddElt(stor.lesdiv.consigne, 'div')
        stor.lesdiv.diag = j3pAddElt(stor.lesdiv.consigne, 'div')
        stor.lesdiv.consigne2 = j3pAddElt(stor.lesdiv.consigne, 'div')
        stor.lesdiv.combine = j3pAddElt(stor.lesdiv.conteneur, 'div')
        const tab = addDefaultTable(stor.lesdiv.combine, 1, 3)
        stor.lesdiv.travail = tab[0][0]
        stor.lesdiv.correction = tab[0][2]
        tab[0][1].style.width = '20px'
        stor.lesdiv.explik = j3pAddElt(stor.lesdiv.conteneur, 'div')
        stor.lesdiv.explik.style.color = colorKo
        stor.lesdiv.explik.style.align = 'left'
        stor.lesdiv.explik2 = j3pAddElt(stor.lesdiv.conteneur, 'div')
      } else {
        stor.lesdiv.combine = j3pAddElt(stor.lesdiv.conteneur, 'div')
        let tab = addDefaultTable(stor.lesdiv.combine, 1, 3)
        stor.lesdiv.consigne = tab[0][0]
        stor.lesdiv.correction = tab[0][2]
        tab[0][1].style.width = '40px'
        stor.lesdiv.consigne1 = j3pAddElt(stor.lesdiv.consigne, 'div')
        stor.lesdiv.diag = j3pAddElt(stor.lesdiv.consigne, 'div')
        tab[0][0].style.whiteSpace = 'normal'
        stor.lesdiv.consigne2 = j3pAddElt(stor.lesdiv.consigne, 'div')
        stor.lesdiv.combine2 = j3pAddElt(stor.lesdiv.conteneur, 'div')
        tab = addDefaultTable(stor.lesdiv.combine2, 1, 3)
        stor.lesdiv.travail = tab[0][0]
        stor.lesdiv.explik2 = tab[0][2]
        tab[0][1].style.width = '40px'
        stor.lesdiv.explik = j3pAddElt(stor.lesdiv.conteneur, 'div')
        stor.lesdiv.explik.style.color = colorKo
        stor.lesdiv.explik.style.align = 'left'
      }
    } else {
      stor.lesdiv.consigne = j3pAddElt(stor.lesdiv.conteneur, 'div')
      stor.lesdiv.consigne1 = j3pAddElt(stor.lesdiv.consigne, 'div')
      let tab = addDefaultTable(stor.lesdiv.consigne, 1, 3)
      stor.lesdiv.diag = tab[0][0]
      stor.lesdiv.explik2 = tab[0][2]
      tab[0][1].style.width = '40px'
      stor.lesdiv.consigne2 = j3pAddElt(stor.lesdiv.consigne, 'div')
      stor.lesdiv.combine = j3pAddElt(stor.lesdiv.conteneur, 'div')
      tab = addDefaultTable(stor.lesdiv.combine, 1, 3)
      stor.lesdiv.travail = tab[0][0]
      stor.lesdiv.correction = tab[0][2]
      tab[0][1].style.width = '40px'
      stor.lesdiv.explik = j3pAddElt(stor.lesdiv.conteneur, 'div')
      stor.lesdiv.explik.style.color = colorKo
      stor.lesdiv.explik.style.align = 'left'
    }

    stor.lesdiv.travail.classList.add('travail')
    stor.lesdiv.consigne.classList.add('enonce')
    // faut virer le nowrap du tableau par défaut, sinon y’a 3km de scroll horizontal
    stor.lesdiv.consigne.style.whiteSpace = 'wrap'
    stor.lesdiv.explik.classList.remove('explique')
    stor.lesdiv.explik2.classList.remove('correction')
    me.zonesElts.MG.classList.add('fond')
    switch (stor.donneeEnCours) {
      case 'tableau':
        j3pAddContent(stor.lesdiv.consigne1, 'Le tableau ci-dessous indique ' + stor.serieEnCours.titre + '&nbsp;: ')
        break
      case 'batons': {
        const consigne = (stor.serieEnCours.typeX === 'continu' ? 'L’histogramme' : 'Le diagramme en bâtons') + ' ci-dessous indique ' + stor.serieEnCours.titre + ' &nbsp;:\n'
        j3pAddContent(stor.lesdiv.consigne1, consigne)
      }
        break
      case 'graphique':
        j3pAddContent(stor.lesdiv.consigne1, 'Le graphique cartésien ci-dessous indique ' + stor.serieEnCours.titre + '&nbsp;:\n')
        break
      case 'circulaire':
        j3pAddContent(stor.lesdiv.consigne1, 'Le diagramme circulaire ci-dessous indique ' + stor.serieEnCours.titre + '&nbsp;:\n')
        break
      case 'liste':
        // rien à ajouter
        break
      default :
        j3pShowError(Error(`Type de données « ${stor.donneeEnCours} » non géré`), { mustNotify: true })
    }
    // affiche données
    dessineco(stor.lesdiv.diag, stor.donneeEnCours, '#000000')
    j3pAddContent(stor.lesdiv.consigne1, '<br>')
    // pose question
    let consigne = '<b>Tu dois représenter cette série statistique à l’aide ' // </b> ajouté ci-dessous
    switch (stor.reponseEnCours) {
      case 'tableau':
        consigne += 'd’un tableau </b> '
        j3pAffiche(stor.lesdiv.consigne2, null, consigne)
        stor.represente = new RepTab(stor.lesdiv.travail, me)
        break
      case 'batons':
        consigne += 'd’un diagramme en bâtons </b> '
        if (stor.serieEnCours.typeX === 'continu') consigne = consigne.replace('diagramme en bâtons', 'histogramme')
        j3pAffiche(stor.lesdiv.consigne2, null, consigne)
        stor.represente = new RepDiag(stor.lesdiv.travail, me)

        break
      case 'graphique':
        consigne += 'd’un graphique cartésien </b> '
        j3pAffiche(stor.lesdiv.consigne2, null, consigne)
        stor.represente = new RepGraph(stor.lesdiv.travail, me)

        break
      case 'circulaire':
        consigne += 'd’un diagramme circulaire </b> '
        j3pAffiche(stor.lesdiv.consigne2, null, consigne)
        stor.represente = new RepCirc(stor.lesdiv.travail, me)
        break

      default :
        j3pShowError(Error(`Type de réponse « ${stor.reponseEnCours} » non géré`), { mustNotify: true })
    }
    stor.isWritingEnonce = false
    // finEnonce() sera appelé à la fin des constructeurs de RepTab|RepCirc|RepDiag (et dans les autres cas ?)
  }

  /**
   * Init les stor.xxxEnCours, les effectifs, et vérifie que le tirage est cohérent (retourne false sinon)
   * @return {boolean|void}
   */
  function genereAlea () {
    // init des  (et ajout à xxxFait)
    // tirage du type de réponse demandée (le premier non déjà fait parmi les possibles mélangés, ou bien un au hasard)
    stor.reponseEnCours = j3pShuffle(stor.reponsesPossibles).find(type => !stor.reponsesFaites.includes(type)) ?? j3pGetRandomElt(stor.reponsesPossibles)

    // idem pour donnees
    const pioche = j3pShuffle(stor.donneesPossibles).filter(type => type !== stor.reponseEnCours)
    if (!pioche) {
      // y’a un bug dans initSection car on s’est normalement arrangé pour que ça ne puisse pas arriver
      return j3pShowError(Error('Données incohérentes, impossible de continuer'), { notifyData: { donneesSection: ds, storage: stor } })
    }
    stor.donneeEnCours = pioche.find(type => !stor.donneesFaites.includes(type)) ?? j3pGetRandomElt(pioche)

    // si on a le cas liste en données, on vire les cas où limiteY > 30 (pourquoi ?)
    let piocheSeries = stor.seriesPossibles
    if (stor.donneeEnCours === 'liste') piocheSeries = piocheSeries.filter(serie => serie.limiteY.max <= 30)
    if (stor.donneeEnCours === 'graphique' || stor.reponseEnCours === 'graphique') piocheSeries = piocheSeries.filter(serie => serie.typeX === 'continu')
    if (piocheSeries.length === 0) {
      console.error(Error('aucune série possible avec les contraintes imposées'), stor)
      return false
    }

    // et là-dedans on en tire une qu’on a pas déjà eu (sauf si on a déjà tout eu…)
    /** @type {Stat01Serie} */
    stor.serieEnCours = j3pShuffle(piocheSeries).find(serie => !stor.seriesFaites.includes(serie.id)) ?? j3pGetRandomElt(piocheSeries)

    // tirage aléatoire de la liste des valeurs de la série
    let nbTirages = 0
    let tirageOk
    do {
      nbTirages++
      tirageOk = true
      /**
       * Liste de toutes les valeurs de la série statistique (string en quali et number en quanti)
       * @type {string[]|number[]}
       */
      stor.liste = []
      stor.effectifTotal = j3pGetRandomInt(stor.serieEnCours.limiteY.min, stor.serieEnCours.limiteY.max)
      if (stor.serieEnCours.typeX === 'qualitatif') {
        for (let i = 0; i < stor.effectifTotal; i += stor.serieEnCours.limiteY.pas) {
          const paramValue = j3pGetRandomElt(stor.serieEnCours.liste1)
          for (let j = 0; j < stor.serieEnCours.limiteY.pas; j++) {
            stor.liste.push(paramValue)
          }
        }
      } else if (stor.serieEnCours.typeX === 'discret') {
        for (let i = 0; i < stor.effectifTotal; i += stor.serieEnCours.limiteY.pas) {
          const val = j3pGetRandomInt(stor.serieEnCours.limiteX.min, stor.serieEnCours.limiteX.max)
          for (let j = 0; j < stor.serieEnCours.limiteY.pas; j++) {
            stor.liste.push(val)
          }
        }
      } else if (stor.serieEnCours.typeX === 'continu') {
        // determine avec limiteX approx
        const { min, max, approx } = stor.serieEnCours.limiteX
        const etendue = Math.trunc((max - min) / approx)
        const { pas } = stor.serieEnCours.limiteY
        for (let i = 0; i < stor.effectifTotal; i += pas) {
          const val = j3pArrondi(j3pGetRandomInt(0, etendue - 1) * approx + min, 8)
          for (let j = 0; j < stor.serieEnCours.limiteY.pas; j++) {
            stor.liste.push(val)
          }
        }
      } else {
        throw Error(`type de paramètre non géré : ${stor.serieEnCours.typeX}`)
      }
      stor.effectifTotal = stor.liste.length

      // calcul effectifs
      /**
       *
       * @type {Stat01ListItem[]}
       */
      stor.effectifs1 = []
      if (stor.serieEnCours.typeX === 'qualitatif') {
        for (const valeur of stor.serieEnCours.liste1) {
          // on compte les éléments de liste qui valent valeur
          const effectif = stor.liste.filter(v => v === valeur).length
          stor.effectifs1.push({ valeur, effectif })
        }
      } else if (stor.serieEnCours.typeX === 'discret') {
        for (let valeur = stor.serieEnCours.limiteX.min; valeur < stor.serieEnCours.limiteX.max + 1; valeur++) {
          const effectif = stor.liste.filter(v => v === valeur).length
          stor.effectifs1.push({ valeur, effectif })
        }
      } else if (stor.serieEnCours.typeX === 'continu') {
        // determine avec limiteX approx
        for (let i = stor.serieEnCours.limiteX.min; i < stor.serieEnCours.limiteX.max; i += stor.serieEnCours.limiteX.amplitude) {
          i = j3pArrondi(i, 5)
          const borneInfAffichee = ajouteZerosInutiles(i, stor.serieEnCours.limiteX.nbZerosInutiles)
          const borneSupAffichee = ajouteZerosInutiles(j3pArrondi(i + stor.serieEnCours.limiteX.amplitude, 5), stor.serieEnCours.limiteX.nbZerosInutiles)
          const valeur = borneInfAffichee + ' ≤ ' + stor.serieEnCours.limiteX.lettre + ' < ' + borneSupAffichee
          const effectif = stor.liste.filter(val => val >= i && val < j3pArrondi(i + stor.serieEnCours.limiteX.amplitude, 7)).length
          stor.effectifs1.push({ valeur, effectif })
        }
      } else {
        throw Error(`type de paramètre non géré : ${stor.serieEnCours.typeX}`)
      }

      // ??
      // ici faut empêcher 3 valeurs égales
      // plus que 2 valeurs egales
      // 1 seule valeur au max à 0, et aucune si diagramme circulaire
      // ??
      // Dsl, pas compris ce commentaire

      stor.nbeff = []

      for (let i = 0; i < stor.effectifs1.length; i++) {
        if (estPresent(stor.effectifs1[i].effectif)) {
          augmente(stor.effectifs1[i].effectif)
        } else {
          stor.nbeff.push({ eff: stor.effectifs1[i].effectif, nb: 1 })
        }
      }
      stor.nbeffmax = 0
      stor.effmax = 0
      for (let i = 0; i < stor.nbeff.length; i++) {
        stor.nbeffmax = Math.max(stor.nbeffmax, stor.nbeff[i].nb)
        stor.effmax = Math.max(stor.effmax, stor.nbeff[i].eff)
      }
      if (stor.nbeffmax > 2) tirageOk = false
      const nb0 = nbDeZero()
      if ((stor.reponseEnCours === 'circulaire') && (nb0 !== 0)) tirageOk = false
    } while (nbTirages < 200 && !tirageOk)

    return nbTirages < 200
  }

  switch (this.etat) {
    case 'enonce':
      if (this.debutDeLaSection) {
        initSection()
      } else {
        enonceMain()
      }
      break // case "enonce":

    case 'correction':
      // On teste si une réponse a été saisie
      stor.lesdiv.explik.classList.remove('correction')
      stor.lesdiv.explik2.classList.remove('explique')
      j3pEmpty(stor.lesdiv.explik2)
      j3pEmpty(stor.lesdiv.explik)

      if (!yareponse()) {
        stor.lesdiv.correction.style.color = colorKo
        stor.lesdiv.correction.innerHTML = reponseIncomplete
        return this.finCorrection()
      }
      // Bonne réponse
      if (bonneReponse()) {
        this.score += j3pArrondi(1 / this.donneesSection.nbetapes, 1)
        stor.lesdiv.correction.style.color = colorOk
        stor.lesdiv.correction.innerHTML = cBien
        stor.represente.toutvert()
        stor.represente.desactive()
        this.typederreurs[0]++
        return this.finCorrection('navigation', true)
      }

      // Pas de bonne réponse
      stor.lesdiv.correction.style.color = colorKo
      stor.lesdiv.correction.innerHTML = cFaux

      // A cause de la limite de temps :
      if (this.isElapsed) { // limite de temps
        stor.lesdiv.correction.innerHTML += tempsDepasse + '<br>'
        this.typederreurs[10]++
        affcofo(true)
        return this.finCorrection('navigation', true)
      }

      // Réponse fausse :
      if (this.essaiCourant < this.donneesSection.nbchances) {
        stor.lesdiv.correction.innerHTML += '<br>' + essaieEncore
        affcofo(false)
        this.typederreurs[1]++
        return this.finCorrection()
      }

      // Erreur au dernier essai
      stor.lesdiv.correction.innerHTML += '<br>' + textes.regardecp
      affcofo(true)
      this.typederreurs[2]++
      this.finCorrection('navigation', true)
      break // case "correction":

    case 'navigation':
      if (!this.sectionTerminee()) {
        this.etat = 'enonce'
      }
      this.finNavigation()
      break // case "navigation":
  }
}
