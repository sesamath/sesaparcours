import { j3pAddElt, j3pAjouteBouton, j3pArrondi, j3pClone, j3pDetruit, j3pEmpty, j3pGetNewId, j3pGetRandomBool, j3pGetRandomInt, j3pModale, j3pNotify, j3pShowError, j3pShuffle } from 'src/legacy/core/functions'
import Algebrite from 'algebrite'
import { j3pAffiche } from 'src/lib/mathquill/functions'
import ZoneStyleMathquill1 from 'src/legacy/outils/zoneStyleMathquill/ZoneStyleMathquill1'
import { addDefaultTable } from 'src/legacy/themes/table'
import textesGeneriques from 'src/lib/core/textes'

const { cBien, cFaux, essaieEncore, reponseIncomplete, tempsDepasse } = textesGeneriques

const dummyFn = () => undefined
/**
 * Ajouter une description ici
 * @fileOverview
 * @author Tommy Barroy <t.barroy@laposte.net>
 * @since 2017-10
 */
export const params = {
  outils: ['mathquill', 'calculatrice'],
  /**
   * Ce qui est paramétrable dans l’éditeur de graphe
   * @type {LegacySectionParamsParametres}
   */
  parametres: [
    ['nbrepetitions', 9, 'entier', 'Nombre de répétitions de la section'],
    ['nbchances', 2, 'entier', 'Nombre d’essais par répétition'],
    ['Positifs', true, 'boolean', '<i>uniquement quand <b>Formules</b> est à <b>donnée</b></i><br><br><u>true</u>: Les nombres sont positifs.'],
    ['puissances', true, 'boolean', '<i>uniquement quand <b>Formules</b> est à <b>donnée</b></i><br><br><u>true</u>: La formule peut inclure des puissances d’exposant supérieur à 3.</i> .'],
    ['cellule', 1, 'entier', 'nombre de cellules à utiliser (entre 0 et 3)'],
    ['difficulte', 1, 'entier', '<i>uniquement quand <b>Formules</b> est à <b>donnée</b></i><br><br>nombre d’opérations'],
    ['Formule', 'les deux', 'liste', 'type de la formule. <br><br><u>donnée</u>: La fomule est aléatoire. <br><br><u>context</u>: La formule attendue se déduit du context.', ['donnée', 'context', 'les deux']],
    ['Context_base', true, 'boolean', '<i>uniquement quand <b>Formules</b> est à <b>context</b></i><br><br> Les formules sont de type <b> suivant, précédent, double, tiers,...</b>'],
    ['Context_op', true, 'boolean', '<i>uniquement quand <b>Formules</b> est à <b>context</b></i><br><br> Les formules sont de type <b> somme, produit , ...</b>'],
    ['Context_stat', true, 'boolean', '<i>uniquement quand <b>Formules</b> est à <b>context</b></i><br><br> Les formules sont de type <b> fréquence , affectif total , moyenne ....</b>'],
    ['Donnee_fonction', false, 'boolean', '<i>uniquement quand <b>Formules</b> est à <b>donnée</b></i><br><br> La formule est donnée sous forme d’une fonction.'],
    ['aide', true, 'boolean', '<u>true</u>: Une aide est disponible.'],
    // le modèle gère la compatibilité ascendante avec l’ancien paramètre MiseEnPagePlusClaire
    ['theme', 'standard', 'liste', 'Modèle pour l’affichage', ['standard', 'zonesAvecImageDeFond']]
  ]
}

const situ = [
  { g1: 'âge (en années)', g2: 'effectif', leg: 'âge des élèves de la classe', v1: '10', v2: '11', v3: '12', v4: '13' },
  { g1: 'couleur', g2: 'effectif', leg: 'couleur des stylos de Maria', v1: 'bleu', v2: 'vert', v3: 'rouge', v4: 'noir' },
  { g1: 'moyen de déplacement', g2: 'effectif', leg: 'Sondage: "Quel moyen de déplacement utilisez vous ?', v1: 'vélo', v2: 'bus', v3: 'pieds', v4: 'train' }
]

/**
 * section tableur01
 * @this {Parcours}
 */
export default function main () {
  const me = this
  const stor = this.storage
  const ds = me.donneesSection

  function creeLesDiv () {
    stor.lesdiv = {}
    stor.lesdiv.conteneur = j3pAddElt(me.zonesElts.MG, 'div', {
      style: me.styles.etendre('toutpetit.enonce', { padding: '10px' })
    })
    stor.lesdiv.consigneG = addDefaultTable(stor.lesdiv.conteneur, 1, 1)[0][0]
    stor.lesdiv.travail = addDefaultTable(stor.lesdiv.conteneur, 1, 1)[0][0]
    stor.lesdiv.explications = j3pAddElt(stor.lesdiv.conteneur, 'div')
    stor.lesdiv.solution = j3pAddElt(stor.lesdiv.conteneur, 'div')
    stor.lesdiv.aide = j3pAddElt(me.zonesElts.MD, 'div', {
      style: me.styles.etendre('toutpetit.enonce', { padding: '10px' })
    })
    stor.lesdiv.correction = j3pAddElt(me.zonesElts.MD, 'div', {
      style: me.styles.etendre('toutpetit.enonce', { padding: '10px' })
    })
    stor.lesdiv.solution.style.color = me.styles.toutpetit.correction.color
    stor.lesdiv.explications.style.color = me.styles.cfaux
    me.zonesElts.MG.classList.add('fond')
  }

  function initSection () {
    me.construitStructurePage('presentation1bis')

    let lp = [true]
    if (!ds.Positifs) lp.push(false)
    lp = j3pShuffle(lp)
    stor.exos = []
    for (let i = 0; i < ds.nbitems; i++) {
      stor.exos.push({ signe: lp[i % lp.length] })
    }
    stor.exos = j3pShuffle(stor.exos)
    me.typederreurs = [0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0]

    lp = [true]
    if (!ds.Puissances) lp.push(false)
    lp = j3pShuffle(lp)
    for (let i = 0; i < ds.nbitems; i++) {
      stor.exos[i].puissance = lp[i % lp.length]
    }
    stor.exos = j3pShuffle(stor.exos)

    lp = []
    const lform = []
    if (ds.Context_base) {
      lform.push('suivant')
      lform.push('précédent')
      lform.push('double')
      lform.push('triple')
      lform.push('moitié')
      lform.push('tiers')
    }
    if (ds.Context_op) {
      lform.push('carré')
      lform.push('cube')
      lform.push('somme')
      lform.push('produit')
      lform.push('quotient')
      lform.push('différence')
    }
    if (ds.Context_stat) {
      lform.push('efftotal')
      lform.push('frequence')
      lform.push('frequencepourcent')
      lform.push('moyenne')
    }
    if (ds.Formule === 'context' || ds.Formule === 'les deux') lp = j3pClone(lform)
    if (ds.Formule === 'donnée' || ds.Formule === 'les deux') lp.push('donnée')
    if (lp.length === 0) {
      j3pShowError('Erreur paramétrage')
      lp = ['donnée']
    }
    lp = j3pShuffle(lp)
    for (let i = 0; i < ds.nbitems; i++) {
      stor.exos[i].formule = lp[i % lp.length]
    }
    stor.exos = j3pShuffle(stor.exos)

    lp = j3pClone(situ)
    lp = j3pShuffle(lp)
    for (let i = 0; i < ds.nbitems; i++) {
      stor.exos[i].situation = j3pClone(lp[i % lp.length])
    }
    stor.exos = j3pShuffle(stor.exos)

    const tt = 'Ecrire une formule dans un tableur'

    me.score = 0
    stor.laide = '<u>Addition</u>: +<br>'
    stor.laide += '<u>Soustraction</u>: -<br>'
    stor.laide += '<u>Multiplication</u>: *<br>'
    stor.laide += '<u>Division</u>: /<br>'
    if (ds.puissances) {
      stor.laide += '<u>Carré</u>: ^2<br>'
      stor.laide += '<u>Cube</u>: ^3<br>'
      stor.laide += '<u>Puissance n</u>: ^n<br>'
    }
    stor.laide += '<u>Lettre</u>: Le nom de la cellule contenant sa valeur<br>'

    me.afficheTitre(tt)
    enonceMain()
  }
  function faisChoixExo () {
    let buf, oppos, buf2, lopi, j, buff, nc, inc, prio, keplus, bufz, buffz, keplus2, priola
    oppos = [{ f: ['(n1)+(n2)', '(n2)+(n1)'], t: ['(n1)+(n2)', '(n2)+(n1)'], prio: 0 }, { f: ['(n1)-(n2)'], t: ['(n1)-(n2)'], prio: 0 }, { f: ['(n1)*(n2)', '(n2)*(n1)'], t: ['(n1) \\times (n2)', '(n2) \\times (n1)'], prio: 1 }, { f: ['((n1))/((n2))'], t: ['\\frac{n1}{n2}'], prio: 1 }]
    if (ds.puissance) oppos.push({ f: ['(n1)^(n2)'], t: ['(n1)^{(n2)}'], prio: 2 })
    stor.restric = '0123456789-()*/'
    stor.Lexo = stor.exos.pop()
    nc = 0
    inc = ['X', 'Y', 'Z', 'T', 'U', 'M']
    inc = j3pShuffle(inc)
    if (ds.Donnee_fonction && stor.Lexo.formule === 'donnée') {
      inc = ['<i>x</i>', '<i>y</i>']
    }
    stor.celVal = []
    stor.yatab = false
    switch (stor.Lexo.formule) {
      case 'donnée': {
        let dejpuis = false
        let dejent = false
        prio = 3
        keplus = true
        buf = j3pGetRandomInt(2, 9) + ',' + j3pGetRandomInt(1, 9)
        if (!ds.Positifs && j3pGetRandomBool()) {
          buf = '-' + buf
          keplus = false
        }
        if (nc < ds.cellule) {
          nc++
          buf = inc[nc - 1]
          keplus = true
        }
        buff = buf
        for (let i = 1; i < ds.difficulte + 1; i++) {
          keplus2 = true
          buf2 = j3pGetRandomInt(2, 9) + ',' + j3pGetRandomInt(1, 9)
          if (!ds.Positifs && j3pGetRandomBool()) {
            buf2 = '-' + buf2
            keplus2 = false
          }
          let ok
          do {
            lopi = j3pClone(oppos[j3pGetRandomInt(0, oppos.length - 1)])
            ok = (!dejpuis || lopi.f[0].indexOf('^') === -1) || (!dejent || lopi.f[0].indexOf('ent') === -1)
          } while (!ok)
          if (lopi.f[0].indexOf('^') !== -1) dejpuis = true
          if (lopi.f[0].indexOf('ent') !== -1) dejent = true
          if (lopi.f[0].indexOf('ent') === -1) {
            if (nc < ds.cellule && !ds.Donnee_fonction) {
              nc++
              buf2 = inc[nc - 1]
              keplus2 = true
            }
          }
          j = j3pGetRandomInt(0, lopi.t.length - 1)
          bufz = lopi.t[j]
          buffz = lopi.f[j]
          priola = lopi.prio
          if ((keplus && priola <= prio) || (lopi.f[0] === '(n1)/(n2)')) {
            bufz = bufz.replace('(n1)', buf)
            buffz = buffz.replace('(n1)', buff)
            bufz = bufz.replace('n1', buf)
            buffz = buffz.replace('n1', buff)
          } else {
            bufz = bufz.replace('n1', buf)
            buffz = buffz.replace('n1', buff)
          }
          prio = priola
          if (keplus2 || (lopi.f[0] === '(n1)/(n2)')) {
            buf = bufz.replace('(n2)', buf2)
            buff = buffz.replace('(n2)', buf2)
            buf = buf.replace('n2', buf2)
            buff = buff.replace('n2', buf2)
          } else {
            buf = bufz.replace('n2', buf2)
            buff = buffz.replace('n2', buf2)
          }
        }
        break
      }
      case 'tiers':
        buf = j3pGetRandomInt(2, 9) + ',' + j3pGetRandomInt(1, 9)
        if (!ds.Positifs && j3pGetRandomBool()) {
          buf = '-' + buf
        }
        if (nc < ds.cellule) {
          nc++
          buf = inc[nc - 1]
        }
        buff = buf + '/3'
        buf = '\\text{ le tiers de }' + buf
        break
      case 'moitié':
        buf = j3pGetRandomInt(2, 9) + ',' + j3pGetRandomInt(1, 9)
        if (!ds.Positifs && j3pGetRandomBool()) {
          buf = '-' + buf
        }
        if (nc < ds.cellule) {
          nc++
          buf = inc[nc - 1]
        }
        buff = buf + '/2'
        buf = '\\text{ la moitié de }' + buf
        break
      case 'triple':
        buf = j3pGetRandomInt(2, 9) + ',' + j3pGetRandomInt(1, 9)
        if (!ds.Positifs && j3pGetRandomBool()) {
          buf = '-' + buf
        }
        if (nc < ds.cellule) {
          nc++
          buf = inc[nc - 1]
        }
        buff = buf + '*3'
        buf = '\\text{ le triple de }' + buf
        break
      case 'double':
        buf = j3pGetRandomInt(2, 9) + ',' + j3pGetRandomInt(1, 9)
        if (!ds.Positifs && j3pGetRandomBool()) {
          buf = '-' + buf
        }
        if (nc < ds.cellule) {
          nc++
          buf = inc[nc - 1]
        }
        buff = buf + '*2'
        buf = '\\text{ le double de }' + buf
        break
      case 'carré':
        buf = j3pGetRandomInt(2, 9) + ',' + j3pGetRandomInt(1, 9)
        if (!ds.Positifs && j3pGetRandomBool()) {
          buf = '(-' + buf + ')'
        }
        if (nc < ds.cellule) {
          nc++
          buf = inc[nc - 1]
        }
        buf = buf + '^2'
        buff = buf
        break
      case 'cube':
        buf = j3pGetRandomInt(2, 9) + ',' + j3pGetRandomInt(1, 9)
        if (!ds.Positifs && j3pGetRandomBool()) {
          buf = '(-' + buf + ')'
        }
        if (nc < ds.cellule) {
          nc++
          buf = inc[nc - 1]
        }
        buf = buf + '^3'
        buff = buf
        break
      case 'suivant':
        buf = j3pGetRandomInt(2, 100)
        if (!ds.Positifs && j3pGetRandomBool()) {
          buf = '-' + buf
        }
        if (nc < ds.cellule) {
          nc++
          buf = inc[nc - 1]
        }
        buff = buf + '+1'
        buf = '\\text{ le suivant de }' + buf
        break
      case 'précédent':
        buf = j3pGetRandomInt(2, 100)
        if (!ds.Positifs && j3pGetRandomBool()) {
          buf = '-' + buf
        }
        if (nc < ds.cellule) {
          nc++
          buf = inc[nc - 1]
        }
        buff = buf + '-1'
        buf = '\\text{ le précédent de }' + buf
        break
      case 'différence':
        buf = j3pGetRandomInt(2, 9) + ',' + j3pGetRandomInt(1, 9)
        if (!ds.Positifs && j3pGetRandomBool()) {
          buf = '-' + buf
        }
        if (nc < ds.cellule) {
          nc++
          buf = inc[nc - 1]
        }
        buf2 = j3pGetRandomInt(2, 9) + ',' + j3pGetRandomInt(1, 9)
        if (!ds.Positifs && j3pGetRandomBool()) {
          buf2 = '(-' + buf2 + ')'
        }
        if (nc < ds.cellule) {
          nc++
          buf2 = inc[nc - 1]
        }
        buff = buf + '-' + buf2
        buf = buf + '-' + buf2
        break
      case 'somme':
        buf = j3pGetRandomInt(2, 9) + ',' + j3pGetRandomInt(1, 9)
        if (!ds.Positifs && j3pGetRandomBool()) {
          buf = '-' + buf
        }
        if (nc < ds.cellule) {
          nc++
          buf = inc[nc - 1]
        }
        buf2 = j3pGetRandomInt(2, 9) + ',' + j3pGetRandomInt(1, 9)
        if (!ds.Positifs && j3pGetRandomBool()) {
          buf2 = '(-' + buf2 + ')'
        }
        if (nc < ds.cellule) {
          nc++
          buf2 = inc[nc - 1]
        }
        buff = buf + '+' + buf2
        buf = buf + '+' + buf2
        break
      case 'produit':
        buf = j3pGetRandomInt(2, 9) + ',' + j3pGetRandomInt(1, 9)
        if (!ds.Positifs && j3pGetRandomBool()) {
          buf = '-' + buf
        }
        if (nc < ds.cellule) {
          nc++
          buf = inc[nc - 1]
        }
        buf2 = j3pGetRandomInt(2, 9) + ',' + j3pGetRandomInt(1, 9)
        if (!ds.Positifs && j3pGetRandomBool()) {
          buf2 = '(-' + buf2 + ')'
        }
        if (nc < ds.cellule) {
          nc++
          buf2 = inc[nc - 1]
        }
        buff = buf + '*' + buf2
        buf = buf + ' \\times ' + buf2
        break
      case 'quotient':
        buf = j3pGetRandomInt(2, 9) + ',' + j3pGetRandomInt(1, 9)
        if (!ds.Positifs && j3pGetRandomBool()) {
          buf = '-' + buf
        }
        if (nc < ds.cellule) {
          nc++
          buf = inc[nc - 1]
        }
        buf2 = j3pGetRandomInt(2, 9) + ',' + j3pGetRandomInt(1, 9)
        if (!ds.Positifs && j3pGetRandomBool()) {
          buf2 = '(-' + buf2 + ')'
        }
        if (nc < ds.cellule) {
          nc++
          buf2 = inc[nc - 1]
        }
        buff = buf + '/' + buf2
        buf = '\\frac{' + buf + '}{' + buf2.replace('(', '').replace(')', '') + '}'
        break
      case 'frequence':
        stor.yatab = true
        stor.ttab = j3pGetRandomInt(2, 4)
        stor.dek = j3pClone(stor.Lexo.situation)
        buf = buff = ''
        break
      case 'efftotal':
        stor.yatab = true
        stor.ttab = j3pGetRandomInt(2, 4)
        stor.dek = j3pClone(stor.Lexo.situation)
        buf = buff = ''
        break
      case 'moyenne':
        stor.yatab = true
        stor.ttab = j3pGetRandomInt(2, 4)
        stor.dek = j3pClone(situ[0])
        buf = buff = ''
        break
      case 'frequencepourcent':
        stor.yatab = true
        stor.ttab = j3pGetRandomInt(2, 4)
        stor.dek = j3pClone(stor.Lexo.situation)
        buf = buff = ''
        break
      default:
        j3pShowError(Error(`Cas non prévu (${stor.Lexo.formule})`), { message: 'Erreur interne (cas non prévu)', mustNotify: true })
    }
    const celpos = j3pShuffle(['B1', 'C1', 'D1', 'E1', 'F1', 'B2', 'C2', 'D2', 'E2', 'F2'])
    for (let i = 0; i < nc; i++) {
      oppos = celpos.pop()
      vireAvant(oppos, celpos)
      vireApres(oppos, celpos)
      prio = inc[i]
      priola = 1
      if (oppos.indexOf('1') === -1) priola = 2
      keplus = 1
      if (oppos.indexOf('B') !== -1) keplus = 2
      if (oppos.indexOf('C') !== -1) keplus = 3
      if (oppos.indexOf('D') !== -1) keplus = 4
      if (oppos.indexOf('E') !== -1) keplus = 5
      if (oppos.indexOf('F') !== -1) keplus = 6
      stor.celVal.push({ nom: oppos, varia: inc[i], ligne: priola, colonne: keplus, montre: true })
    }
    stor.buf = buf
    stor.bufz = buff.replace(/,/g, '.')
    stor.dddddeb = 0
  }
  function vireAvant (cas, tab) {
    if (cas === 'B1' || cas === 'B2') return
    let avire = lettreAv(cas[0])
    avire += cas[1]
    for (let i = tab.length - 1; i > -1; i--) {
      if (tab[i] === avire) {
        tab.splice(i, 1)
        return
      }
    }
  }
  function vireApres (cas, tab) {
    if (cas === 'F1' || cas === 'F2') return
    let avire = lettreAp(cas[0])
    avire += cas[1]
    for (let i = tab.length - 1; i > -1; i--) {
      if (tab[i] === avire) {
        tab.splice(i, 1)
        return
      }
    }
  }
  function lettreAv (l) {
    switch (l) {
      case 'C': return 'B'
      case 'D': return 'C'
      case 'E': return 'D'
      case 'F': return 'E'
    }
  }
  function lettreAp (l) {
    switch (l) {
      case 'B': return 'C'
      case 'C': return 'D'
      case 'D': return 'E'
      case 'E': return 'F'
    }
  }
  function poseQuestion () {
    stor.cache = false
    const Azrert = addDefaultTable(stor.lesdiv.travail, 2, 1)
    stor.tabTabl = addDefaultTable(Azrert[0][0], 7, 8)
    Azrert[1][0].style.textAlign = 'center'
    const tabcell = []
    for (let i = 0; i < 8; i++) {
      tabcell[i] = []
      for (let j = 0; j < 7; j++) {
        tabcell[i][j] = true
        stor.tabTabl[j][i].style.background = '#fff'
        stor.tabTabl[j][i].style.border = '1px solid grey'
        stor.tabTabl[j][i].style.textAlign = 'center'
        stor.tabTabl[j][i].style.paddingBottom = '5px'
      }
    }
    faisbout(stor.tabTabl[0][0], '', 20)
    faisbout(stor.tabTabl[0][1], 'A', 40)
    faisbout(stor.tabTabl[0][2], 'B', 40)
    faisbout(stor.tabTabl[0][3], 'C', 40)
    faisbout(stor.tabTabl[0][4], 'D', 40)
    faisbout(stor.tabTabl[0][5], 'E', 40)
    faisbout(stor.tabTabl[0][6], 'F', 40)

    faisbout(stor.tabTabl[1][0], '1', 30)
    faisbout(stor.tabTabl[2][0], '2', 20)
    faisbout(stor.tabTabl[3][0], '3', 20)
    faisbout(stor.tabTabl[4][0], '4', 20)
    faisbout(stor.tabTabl[5][0], '5', 20)
    faisbout(stor.tabTabl[6][0], '6', 20)

    stor.ligne = j3pGetRandomInt(3, 6)
    const ll = ['A', 'B', 'C', 'D', 'E', 'F']
    stor.colonne = j3pGetRandomInt(0, 4)

    let inc = ['X', 'Y', 'Z', 'T', 'U', 'M', 'R', 'P', 'Q', 'S', 'J', 'K']
    inc = j3pShuffle(inc)
    if (ds.Donnee_fonction && stor.Lexo.formule === 'donnée') {
      inc = ['<i>x</i>', '<i>y</i>']
    }
    if (stor.yatab) {
      stor.debtab = j3pGetRandomInt(1, 5 - stor.ttab)
      stor.lignetab = j3pGetRandomInt(2, 5)
      let ct = 0
      stor.celVal.push({
        nom: ll[stor.debtab] + stor.lignetab,
        varia: inc[ct],
        ligne: stor.lignetab,
        colonne: stor.debtab + 1,
        montre: false
      })
      ct++
      if (stor.Lexo.formule === 'moyenne') {
        stor.celVal.push({
          nom: ll[stor.debtab] + (stor.lignetab - 1),
          varia: inc[ct],
          ligne: stor.lignetab - 1,
          colonne: stor.debtab + 1,
          montre: false,
          val: stor.dek.v1
        })
        ct++
      }
      stor.celVal.push({
        nom: ll[stor.debtab + 1] + stor.lignetab,
        varia: inc[ct],
        ligne: stor.lignetab,
        colonne: stor.debtab + 2,
        montre: false
      })
      ct++
      if (stor.Lexo.formule === 'moyenne') {
        stor.celVal.push({
          nom: ll[stor.debtab + 1] + (stor.lignetab - 1),
          varia: inc[ct],
          ligne: stor.lignetab - 1,
          colonne: stor.debtab + 2,
          montre: false,
          val: stor.dek.v2
        })
        ct++
      }
      if (stor.ttab > 2) {
        stor.celVal.push({
          nom: ll[stor.debtab + 2] + stor.lignetab,
          varia: inc[ct],
          ligne: stor.lignetab,
          colonne: stor.debtab + 3,
          montre: false
        })
        ct++
        if (stor.Lexo.formule === 'moyenne') {
          stor.celVal.push({
            nom: ll[stor.debtab + 2] + (stor.lignetab - 1),
            varia: inc[ct],
            ligne: stor.lignetab - 1,
            colonne: stor.debtab + 3,
            montre: false,
            val: stor.dek.v3
          })
          ct++
        }
      }
      if (stor.ttab > 3) {
        stor.celVal.push({
          nom: ll[stor.debtab + 3] + stor.lignetab,
          varia: inc[ct],
          ligne: stor.lignetab,
          colonne: stor.debtab + 4,
          montre: false
        })
        ct++
        if (stor.Lexo.formule === 'moyenne') {
          stor.celVal.push({
            nom: ll[stor.debtab + 3] + (stor.lignetab - 1),
            varia: inc[ct],
            ligne: stor.lignetab - 1,
            colonne: stor.debtab + 4,
            montre: false,
            val: stor.dek.v4
          })
          ct++
        }
      }
    }
    if (stor.Lexo.formule === 'frequence') {
      stor.ligne = stor.lignetab + 1
      stor.colonne = j3pGetRandomInt(0, stor.ttab - 1)

      stor.bufz += stor.celVal[stor.colonne].nom + '/(' + stor.celVal[0].nom + '+' + stor.celVal[1].nom
      if (stor.ttab > 2) stor.bufz += '+' + stor.celVal[2].nom
      if (stor.ttab > 3) stor.bufz += '+' + stor.celVal[3].nom
      stor.bufz += ')'
      stor.colonne += stor.debtab - 1
    }
    if (stor.Lexo.formule === 'frequencepourcent') {
      stor.ligne = stor.lignetab + 1
      stor.colonne = j3pGetRandomInt(0, stor.ttab - 1)
      stor.bufz += stor.celVal[stor.colonne].nom + '/(' + stor.celVal[0].nom + '+' + stor.celVal[1].nom
      if (stor.ttab > 2) stor.bufz += '+' + stor.celVal[2].nom
      if (stor.ttab > 3) stor.bufz += '+' + stor.celVal[3].nom
      stor.bufz += ')*100'
      stor.colonne += stor.debtab - 1
    }
    if (stor.Lexo.formule === 'efftotal') {
      stor.ligne = stor.lignetab
      stor.colonne = stor.debtab + stor.ttab - 1
      stor.bufz += stor.celVal[0].nom + '+' + stor.celVal[1].nom
      if (stor.ttab > 2) stor.bufz += '+' + stor.celVal[2].nom
      if (stor.ttab > 3) stor.bufz += '+' + stor.celVal[3].nom
    }
    if (stor.Lexo.formule === 'moyenne') {
      stor.ligne = stor.lignetab + 1
      stor.colonne = stor.debtab + stor.ttab - 1
      let fin = '(' + stor.celVal[0].nom + '+' + stor.celVal[2].nom
      stor.bufz += '(' + stor.celVal[0].nom + '*' + stor.celVal[1].val + '+' + stor.celVal[2].nom + '*' + stor.celVal[3].val
      if (stor.ttab > 2) {
        stor.bufz += '+' + stor.celVal[4].nom + '*' + stor.celVal[5].val
        fin += '+' + stor.celVal[4].nom
      }
      if (stor.ttab > 3) {
        stor.bufz += '+' + stor.celVal[6].nom + '*' + stor.celVal[7].val
        fin += '+' + stor.celVal[6].nom
      }
      fin += ')'
      stor.bufz += ')/' + fin
    }

    let quest = ''
    if (stor.yatab) {
      quest = 'Cette page de tableur s’intitule: <i><u>' + stor.dek.leg + '</u></i><br>'
      let mmax = 2
      if (stor.Lexo.formule === 'frequence') {
        mmax = 3
      }
      if (stor.Lexo.formule === 'frequencepourcent') {
        mmax = 3
      }
      for (let i = 0; i < mmax; i++) {
        for (let j = 0; j < stor.ttab + 2; j++) {
          stor.tabTabl[stor.lignetab - 2 + i][stor.debtab + j].style.borderBottom = '2px solid black'
          stor.tabTabl[stor.lignetab - 1 + i][stor.debtab + j - 1].style.borderRight = '2px solid black'
          stor.tabTabl[stor.lignetab - 1 + i][stor.debtab + j].style.border = '2px solid black'
        }
      }
      j3pAffiche(stor.tabTabl[stor.lignetab - 1][stor.debtab], null, '&nbsp;' + stor.dek.g1 + '&nbsp;')
      j3pAffiche(stor.tabTabl[stor.lignetab][stor.debtab], null, '&nbsp;' + stor.dek.g2 + '&nbsp;')
      j3pAffiche(stor.tabTabl[stor.lignetab - 1][stor.debtab + 1], null, '&nbsp;' + stor.dek.v1 + '&nbsp;')
      j3pAffiche(stor.tabTabl[stor.lignetab - 1][stor.debtab + 2], null, '&nbsp;' + stor.dek.v2 + '&nbsp;')

      tabcell[stor.lignetab - 1][stor.debtab] = false
      tabcell[stor.lignetab][stor.debtab] = false
      tabcell[stor.lignetab - 1][stor.debtab + 1] = false
      tabcell[stor.lignetab - 1][stor.debtab + 2] = false

      if (stor.Lexo.formule === 'frequence') {
        j3pAffiche(stor.tabTabl[stor.lignetab + 1][stor.debtab], null, '&nbsp;fréquence&nbsp;')
        tabcell[stor.lignetab + 1][stor.debtab] = false
        j3pAffiche(stor.tabTabl[stor.lignetab + 1][stor.debtab + stor.ttab + 1], null, '&nbsp;$1$&nbsp;')
        tabcell[stor.lignetab + 1][stor.debtab + stor.ttab + 1] = false
      }
      if (stor.Lexo.formule === 'frequencepourcent') {
        j3pAffiche(stor.tabTabl[stor.lignetab + 1][stor.debtab], null, '&nbsp;fréquence en %&nbsp;')
        tabcell[stor.lignetab + 1][stor.debtab] = false
        j3pAffiche(stor.tabTabl[stor.lignetab + 1][stor.debtab + stor.ttab + 1], null, '&nbsp;$100$&nbsp;')
        tabcell[stor.lignetab + 1][stor.debtab + stor.ttab + 1] = false
      }
      if (stor.Lexo.formule === 'moyenne') {
        tabcell[stor.lignetab - 1][stor.debtab + 1] = stor.dek.v1
        tabcell[stor.lignetab - 1][stor.debtab + 2] = stor.dek.v2
        j3pAffiche(stor.tabTabl[stor.lignetab + 1][stor.debtab + stor.ttab], null, '&nbsp;moyenne&nbsp;')
        tabcell[stor.lignetab + 1][stor.debtab + stor.ttab] = false
        stor.tabTabl[stor.lignetab + 1][stor.debtab + stor.ttab + 1].style.border = '2px solid black'
        stor.tabTabl[stor.lignetab][stor.debtab + stor.ttab + 1].style.borderBottom = '2px solid black'
        stor.tabTabl[stor.lignetab + 1][stor.debtab + stor.ttab].style.borderRight = '2px solid black'
      }
      if (stor.ttab > 2) {
        j3pAffiche(stor.tabTabl[stor.lignetab - 1][stor.debtab + 3], null, '&nbsp;' + stor.dek.v3 + '&nbsp;')
        tabcell[stor.lignetab - 1][stor.debtab + 3] = false
        if (stor.Lexo.formule === 'moyenne') {
          tabcell[stor.lignetab - 1][stor.debtab + 3] = stor.dek.v3
        }
      }
      if (stor.ttab > 3) {
        j3pAffiche(stor.tabTabl[stor.lignetab - 1][stor.debtab + 4], null, '&nbsp;' + stor.dek.v4 + '&nbsp;')
        tabcell[stor.lignetab - 1][stor.debtab + 4] = false
      }
      j3pAffiche(stor.tabTabl[stor.lignetab - 1][stor.debtab + stor.ttab + 1], null, '&nbsp;TOTAL&nbsp;')
      tabcell[stor.lignetab - 1][stor.debtab + stor.ttab + 1] = false
      if (stor.Lexo.formule === 'moyenne') {
        tabcell[stor.lignetab - 1][stor.debtab + stor.ttab + 1] = stor.dek.v4
      }
    }

    if (stor.Lexo.formule === 'donnée') {
      const ttext = (!ds.Donnee_fonction) ? '&nbsp;$A \\text{ vaut : }$' : '&nbsp;$f(x)=$&nbsp;'
      j3pAffiche(stor.tabTabl[stor.ligne][stor.colonne + 1], null, ttext)
      tabcell[stor.ligne][stor.colonne + 1] = false
      stor.nomCel = ll[stor.colonne + 1] + stor.ligne
      quest = (!ds.Donnee_fonction) ? ('On considère l’expression $A = ' + stor.buf + '$ <br>') : 'On considère la fonction $f:\\text{ }x\\text{ }\\mapsto\\text{ }' + stor.buf + '$<br>'
      stor.nomCel = ll[stor.colonne + 1] + stor.ligne
      quest += '<b>Ecris une formule dans la cellule ' + ll[stor.colonne + 1] + stor.ligne + ' pour obtenir le calcul de l’expression $A$.</b><br><br>'
    } else {
      if (!stor.yatab) {
        j3pAffiche(stor.tabTabl[stor.ligne][stor.colonne + 1], null, '&nbsp;$' + stor.buf + '\\text{ vaut : }$')
        tabcell[stor.ligne][stor.colonne + 1] = false
      }
      quest += '<b>Ecris une formule dans la cellule ' + ll[stor.colonne + 1] + stor.ligne + '.</b><br>'
      if (stor.yatab && stor.Lexo.formule !== 'efftotal') {
        quest += '<i>Si tu veux utiliser la cellule contenant l’effectif total, il faut la compléter avant.</i>'
      }
    }
    for (let i = 0; i < stor.celVal.length; i++) {
      if (stor.celVal[i].montre) {
        j3pAffiche(stor.tabTabl[stor.celVal[i].ligne][stor.celVal[i].colonne - 1], null, '&nbsp;$\\text{ valeur de }' + stor.celVal[i].varia + ' : $&nbsp;')
        stor.tabTabl[stor.celVal[i].ligne - 1][stor.celVal[i].colonne].style.borderBottom = '2px solid black'
        stor.tabTabl[stor.celVal[i].ligne][stor.celVal[i].colonne - 1].style.borderRight = '2px solid black'
        stor.tabTabl[stor.celVal[i].ligne][stor.celVal[i].colonne].style.border = '2px solid black'
        tabcell[stor.celVal[i].ligne][stor.celVal[i].colonne - 1] = false
      }
    }
    /**
     * Tableau à 2 niveaux de ZoneStyleMathquill1
     * @private
     * @type {ZoneStyleMathquill1[][]}
     */
    stor.tabCelZsm = []
    for (let i = 1; i < 7; i++) {
      stor.tabCelZsm[i] = []
      for (let j = 1; j < 7; j++) {
        if (tabcell[i][j] === true) {
          stor.tabCelZsm[i][j] = new ZoneStyleMathquill1(stor.tabTabl[i][j], {
            restric: '0123456789/*+-()=ABCDEFabcdef,µ',
            limitenb: 9,
            limite: 30,
            enter: me.sectionCourante.bind(me),
            sansBord: true
          })
          stor.tabTabl[i][j].addEventListener('click', FaisClikHere(i, j))
        } else {
          stor.tabCelZsm[i][j] = {
            barreIfKo: dummyFn,
            corrige: dummyFn,
            disable: dummyFn,
            reponse: makeFonc(tabcell[i][j])
          }
        }
      }
    }
    j3pAffiche(stor.lesdiv.consigneG, null, quest)
    stor.ttabB = addDefaultTable(Azrert[0][0], 1, 3)
    if (stor.celVal.length > 0) {
      const newid = j3pGetNewId()
      j3pAjouteBouton(stor.ttabB[0][2], newid, '', 'Mettre des valeurs', function () {
        mettreVal()
      })
      stor.ttabB[0][1].style.width = '40px'
    }
    const newid = j3pGetNewId()
    j3pAjouteBouton(stor.ttabB[0][0], newid, '', 'Calculer', function () {
      calculer()
    })
  }
  function FaisClikHere (i, j) {
    return () => {
      setTimeout(() => { stor.tabCelZsm[i][j].focus() }, 10)
    }
  }
  function mettreVal () {
    let oz
    for (let i = 0; i < stor.celVal.length; i++) {
      if (stor.celVal[i].val === undefined) {
        if (['frequence', 'moyenne', 'frequencepourcent', 'efftotal'].indexOf(stor.Lexo.formule) === -1) {
          oz = String(j3pGetRandomInt(1, 5))
          if (!ds.Positifs && j3pGetRandomBool()) oz = '-' + oz
          stor.tabCelZsm[stor.celVal[i].ligne][stor.celVal[i].colonne].texta.elemnum = []
          stor.tabCelZsm[stor.celVal[i].ligne][stor.celVal[i].colonne].texta.poscurseur = 0
          stor.tabCelZsm[stor.celVal[i].ligne][stor.celVal[i].colonne].majaffiche(oz)
        } else {
          stor.tabCelZsm[stor.celVal[i].ligne][stor.celVal[i].colonne].texta.elemnum = []
          stor.tabCelZsm[stor.celVal[i].ligne][stor.celVal[i].colonne].texta.poscurseur = 0
          stor.tabCelZsm[stor.celVal[i].ligne][stor.celVal[i].colonne].majaffiche(String(j3pGetRandomInt(1, 5)))
        }
      }
    }
  }
  function makeFonc (g) {
    const ghj = g
    if (g === false) return function () { return '-erreur--' }
    return function () { return ghj }
  }
  function calculer () {
    stor.cache = true
    j3pEmpty(stor.ttabB[0][0])
    if (stor.celVal.length > 0) {
      j3pEmpty(stor.ttabB[0][2])
    }
    const newid = j3pGetNewId()
    j3pAjouteBouton(stor.ttabB[0][0], newid, '', 'Editer', function () { editer() })
    // on cache tous les editeurs
    for (let i = 1; i < 7; i++) {
      for (let j = 1; j < 7; j++) {
        if (stor.tabCelZsm[i][j].texta !== undefined) {
          stor.tabCelZsm[i][j].textaTab.style.display = 'none'
          stor.tabCelZsm[i][j].disabled = true
          stor.usedCel = []
          stor.dddddeb = 0
          j3pAffiche(stor.tabCelZsm[i][j].texta, 'AzeSDrf' + i + 'hfui' + j, calculeCell(i, j).replace(/\./g, ','))
        }
      }
    }
  }
  function editer () {
    stor.cache = false
    j3pEmpty(stor.ttabB[0][0])
    if (stor.celVal.length > 0) {
      j3pEmpty(stor.ttabB[0][2])
      const newid = j3pGetNewId()
      j3pAjouteBouton(stor.ttabB[0][2], newid, '', 'Mettre des valeurs', function () {
        mettreVal()
      })
    }
    const newid = j3pGetNewId()
    j3pAjouteBouton(stor.ttabB[0][0], newid, '', 'Calculer', function () { calculer() })
    for (let i = 1; i < 7; i++) {
      for (let j = 1; j < 7; j++) {
        if (stor.tabCelZsm[i][j].texta !== undefined) {
          j3pDetruit('AzeSDrf' + i + 'hfui' + j)
          stor.tabCelZsm[i][j].textaTab.style.display = ''
          stor.tabCelZsm[i][j].disabled = false
        }
      }
    }
  }
  function faisbout (id, letr, width) {
    const Sborder = '1px solid black'
    const Sbackground = '#aaaaFF'
    id.style.border = Sborder
    id.style.background = Sbackground
    j3pAffiche(id, null, '$' + letr + '$')
    id.style.width = width + 'px'
    id.style.height = '42px'
  }
  function calculeCell (l, c) {
    stor.dddddeb++
    if (stor.dddddeb > 200) {
      const data = { letab: stor.tabCelZsm, letout: stor }
      j3pNotify('pour tom much recurs', data)
      return '--erreur--'
    }
    let regex, bb, jj
    let cont = stor.tabCelZsm[l][c].reponse().toUpperCase()
    if (cont === '') return ''
    if (cont[0] !== '=') return cont.replace(/,/g, '.')
    cont = cont.substring(1)
    const tt = ['A', 'B', 'C', 'D', 'E', 'F']
    const nom = tt[c] + l
    stor.usedCel.push(nom)
    let rep2 = transforme(cont)
    if (rep2 === '--erreur--' || rep2.indexOf('RECURS') !== -1) return '--erreur--'
    for (let i = 0; i < stor.celVal.length; i++) {
      const nomVal = tt[stor.celVal[i].colonne] + stor.celVal[i].ligne
      if (stor.usedCel.indexOf(nomVal) !== -1) return 'recurs'
      stor.usedCel.push(nomVal)
      regex = new RegExp(stor.celVal[i].varia, 'g')
      bb = calculeCell(stor.celVal[i].ligne, stor.celVal[i].colonne)
      if (bb === '') bb = '0'
      if (bb === '--erreur--' && rep2.indexOf(stor.celVal[i].varia) !== -1) return bb
      rep2 = rep2.replace(regex, bb)
    }
    if (rep2 === '--erreur--' || rep2.indexOf('RECURS') !== -1) return '--erreur--'
    jj = Algebrite.run(rep2.replace(/,/g, '.'))
    if (jj.indexOf('/') !== -1) {
      jj = String(j3pArrondi(parseInt(jj.substring(0, jj.indexOf('/'))) / parseInt(jj.substring(jj.indexOf('/') + 1)), 5)).replace('.', ',')
    }
    if (jj.indexOf('...') !== -1) {
      jj = jj.replace('...', '')
      for (let i = jj.length - 1; i > 0; i--) {
        if (jj[i] === '0') {
          jj = jj.substring(0, i)
        } else { break }
      }
      if (jj[jj.length - 1] === '.') {
        jj = jj.substring(0, jj.length - 1)
      }
    }
    return jj
  }
  function affModale () {
    const yy = j3pModale({ titre: 'Aide', contenu: '' })
    j3pAffiche(yy, null, stor.laide)
    yy.style.color = me.styles.toutpetit.correction.color
  }

  function yaReponse () {
    if (stor.cache) editer()
    for (let i = 1; i < 7; i++) {
      for (let j = 1; j < 7; j++) {
        if (stor.tabCelZsm[i][j].reponse() !== '') return true
      }
    }
    return false
  }
  function isRepOk () {
    let tabrep, regex

    stor.errWrondCel = false
    stor.errRecurs = false
    stor.errEgal = false
    stor.errEgal2 = false

    stor.errExp = false
    stor.repfo = false

    tabrep = stor.tabCelZsm[stor.ligne][stor.colonne + 2].reponse().toUpperCase()
    if (tabrep === '') {
      stor.errWrondCel = true
      for (let i = 1; i < 7; i++) {
        for (let j = 1; j < 7; j++) {
          if (stor.tabCelZsm[i][j].reponse() !== '') stor.tabCelZsm[i][j].corrige(false)
        }
      }
      return false
    }

    if (tabrep[0] !== '=') {
      stor.errEgal = true
      stor.tabCelZsm[stor.ligne][stor.colonne + 2].corrige(false)
      return false
    }
    tabrep = tabrep.substring(1)
    if (tabrep.indexOf('=') !== -1) {
      stor.errEgal2 = true
      stor.tabCelZsm[stor.ligne][stor.colonne + 2].corrige(false)
      return false
    }
    stor.usedCel = [stor.nomCel]
    stor.dddddeb = 0
    const rep2 = transforme(tabrep).replace(/<I>X<\/I>/g, 'x')

    if (rep2 === 'recurs') {
      stor.errRecurs = true
      stor.tabCelZsm[stor.ligne][stor.colonne + 2].corrige(false)
      return false
    }

    tabrep = stor.bufz
    for (let i = 0; i < stor.celVal.length; i++) {
      regex = new RegExp(stor.celVal[i].nom, 'g')
      tabrep = tabrep.replace(regex, stor.celVal[i].varia)
    }
    tabrep = tabrep.replace(/<i>x<\/i>/g, 'x')
    const comp = Algebrite.run(rep2.replace(/,/g, '.') + '-(' + tabrep + ')')
    if (comp === '0') return true
    if (comp === '0.0') return true
    if (comp === '0.000000...') return true
    if (comp === '-0.000000...') return true
    stor.tabCelZsm[stor.ligne][stor.colonne + 2].corrige(false)
    return false
  } // isRepOk

  function transforme (k) {
    const tt = ['A', 'B', 'C', 'D', 'E', 'F']
    let ret = k
    let regex, buf
    ret = ret.toUpperCase()
    for (let i = 0; i < stor.celVal.length; i++) {
      regex = new RegExp(stor.celVal[i].nom, 'g')
      if (stor.celVal[i].val !== undefined) {
        ret = ret.replace(regex, '(' + stor.celVal[i].val + ')')
      } else {
        ret = ret.replace(regex, '(' + stor.celVal[i].varia + ')')
      }
    }
    // verif autre cels

    for (let i = 1; i < 7; i++) {
      for (let j = 0; j < tt.length; j++) {
        if (ret.indexOf(tt[j] + i) !== -1) {
          if (stor.usedCel.indexOf(tt[j] + i) !== -1) return 'recurs'
          stor.usedCel.push(tt[j] + i)
          buf = transformeDonc(tt[j] + i)
          if (buf === '') buf = '0'
          if (buf === '--erreur--') return '--erreur--'
          regex = new RegExp(tt[j] + i, 'g')
          ret = ret.replace(regex, '(' + buf + ')')
        }
      }
    }
    ret = ret.toUpperCase()
    for (let i = 0; i < stor.celVal.length; i++) {
      regex = new RegExp(stor.celVal[i].nom, 'g')
      if (stor.celVal[i].val !== undefined) {
        ret = ret.replace(regex, '(' + stor.celVal[i].val + ')')
      } else {
        ret = ret.replace(regex, '(' + stor.celVal[i].varia + ')')
      }
    }
    ret = ret.toUpperCase()
    return ret
  }
  function transformeDonc (cel) {
    let c
    if (cel.indexOf('A') !== -1) c = 1
    if (cel.indexOf('B') !== -1) c = 2
    if (cel.indexOf('C') !== -1) c = 3
    if (cel.indexOf('D') !== -1) c = 4
    if (cel.indexOf('E') !== -1) c = 5
    if (cel.indexOf('F') !== -1) c = 6
    cel = cel.replace(/[A-F]/g, '')
    const l = parseInt(cel)
    const cont = stor.tabCelZsm[l][c].reponse()
    if (cont[0] === '=') {
      return transforme(cont.substring(1))
    } else {
      if (estBienUnNombre(cont)) {
        return cont.replace(/,/g, '.')
      } else { return '--erreur--' }
    }
  }
  function estBienUnNombre (st) {
    // verif que chiffre et virgule
    const buf = st.replace(/0|1|2|3|4|5|6|7|8|9|,/g, '')
    if (buf.length > 0) return false
    if (st.indexOf(',') === -1) return true
    if (st.indexOf(',') === 0 || st.indexOf(',') === st.length - 1) return false
    if (st.indexOf(',') !== st.lastIndexOf(',')) return false
    return true
  }
  function desactiveAll () {
    for (let i = 1; i < 7; i++) {
      for (let j = 1; j < 7; j++) {
        stor.tabCelZsm[i][j].disable()
      }
    }
    j3pEmpty(stor.ttabB[0][0])
    j3pEmpty(stor.ttabB[0][2])
    stor.tabTabl[1][0].style.height = '10px'
    stor.tabTabl[2][0].style.height = '10px'
    stor.tabTabl[3][0].style.height = '10px'
    stor.tabTabl[4][0].style.height = '10px'
    stor.tabTabl[5][0].style.height = '10px'
    stor.tabTabl[6][0].style.height = '10px'
  } // desactiveAll
  function passeToutVert () {
    for (let i = 1; i < 7; i++) {
      for (let j = 1; j < 7; j++) {
        stor.tabCelZsm[i][j].corrige(true)
      }
    }
  }
  function barrelesfo () {
    for (let i = 1; i < 7; i++) {
      for (let j = 1; j < 7; j++) {
        stor.tabCelZsm[i][j].barreIfKo()
      }
    }
  }
  function affCorrFaux (isFin) {
    /// //affiche indic
    let buf, regex
    if (stor.errWrondCel) {
      stor.yaexplik = true
      j3pAffiche(stor.lesdiv.explications, null, 'Tu n’as pas rempli la bonne cellule !\n')
    }
    if (stor.errEgal) {
      stor.yaexplik = true
      j3pAffiche(stor.lesdiv.explications, null, 'Une formule commence par <b>=</b> !\n')
    }
    if (stor.errEgal2) {
      stor.yaexplik = true
      j3pAffiche(stor.lesdiv.explications, null, 'Il ne peut y avoir plusieurs symboles <b>=</b> !\n')
    }

    if (isFin) {
      // barre les faux
      barrelesfo()
      desactiveAll()
      buf = stor.bufz.replace(/\./g, ',')
      for (let i = 0; i < stor.celVal.length; i++) {
        regex = new RegExp(stor.celVal[i].varia, 'g')
        buf = buf.replace(regex, stor.celVal[i].nom)
      }
      j3pAffiche(stor.lesdiv.solution, null, 'Tu pouvais écrire la formule suivante <b>=' + buf + '</b>')
    } else {
      me.afficheBoutonValider()
    }
  } // affCorrFaux

  function enonceMain () {
    me.videLesZones()

    creeLesDiv()
    stor.lesdiv.travail.classList.add('travail')
    stor.lesdiv.consigneG.classList.add('enonce')
    faisChoixExo()
    poseQuestion()
    me.afficheBoutonValider()
    if (ds.aide) {
      j3pAjouteBouton(stor.lesdiv.aide, function () {
        affModale()
      }, { className: 'MepBoutons', value: 'Aide' })
    }
    me.finEnonce()
  }

  switch (this.etat) {
    case 'enonce':
      // code exécuté au lancement de la section
      if (me.debutDeLaSection) {
        initSection()
      } else {
        enonceMain()
      }

      break // case "enonce":

    case 'correction':
      // On teste si une réponse a été saisie
      stor.yaexplik = false
      stor.yaco = false
      stor.lesdiv.explications.innerHTML = ''
      stor.lesdiv.solution.innerHTML = ''
      stor.lesdiv.explications.classList.remove('explique')
      stor.lesdiv.solution.classList.remove('correction')

      if (yaReponse()) {
        // Bonne réponse
        if (isRepOk()) {
          this._stopTimer()
          stor.lesdiv.correction.style.color = this.styles.cbien
          stor.lesdiv.correction.innerHTML = cBien

          desactiveAll()
          passeToutVert()

          this.score++
          this.typederreurs[0]++
          this.cacheBoutonValider()
          this.etat = 'navigation'
          this.sectionCourante()
        } else {
          // Pas de bonne réponse
          stor.lesdiv.correction.style.color = this.styles.cfaux
          stor.lesdiv.correction.innerHTML = cFaux
          if (this.isElapsed) {
            // A cause de la limite de temps :
            this._stopTimer()

            stor.lesdiv.correction.innerHTML = tempsDepasse + '<BR>'
            this.typederreurs[10]++
            this.cacheBoutonValider()

            affCorrFaux(true)
            stor.yaco = true

            this.etat = 'navigation'
            this.sectionCourante()
          } else {
            // Réponse fausse :
            if (this.essaiCourant < ds.nbchances) {
              stor.lesdiv.correction.innerHTML += '<br>' + essaieEncore

              affCorrFaux(false)

              // affCorrFaux.call(this, false)
              this.typederreurs[1]++
            } else {
              // Erreur au nème essai
              this._stopTimer()
              this.cacheBoutonValider()

              affCorrFaux(true)
              stor.yaco = true

              this.typederreurs[2]++
              this.etat = 'navigation'
              this.sectionCourante()
            }
          }
        }
      } else {
        // pas de réponse
        stor.lesdiv.correction.style.color = this.styles.cfaux
        stor.lesdiv.correction.innerHTML = reponseIncomplete
        this.afficheBoutonValider()
      }

      if (ds.theme === 'zonesAvecImageDeFond') {
        if (stor.yaexplik) {
          stor.lesdiv.explications.classList.add('explique')
        }
        if (stor.yaco) {
          stor.lesdiv.solution.classList.add('correction')
        }
      }

      // Obligatoire
      this.finCorrection()
      break // case "correction":

    case 'navigation':
      if (!this.sectionTerminee()) {
        this.etat = 'enonce'
      }
      this.finNavigation()
      break // case "navigation":
  }
}
