import { j3pAddElt, j3pArrondi, j3pClone, j3pEmpty, j3pGetRandomInt, j3pShowError, j3pShuffle } from 'src/legacy/core/functions'
import ListeDeroulante from 'src/legacy/outils/listeDeroulante/ListeDeroulante'
import { j3pAffiche } from 'src/lib/mathquill/functions'
import { addDefaultTable, addTable, getCells } from 'src/legacy/themes/table'
import ZoneStyleMathquill1 from 'src/legacy/outils/zoneStyleMathquill/ZoneStyleMathquill1'
import textesGeneriques from 'src/lib/core/textes'

const { cBien, cFaux, essaieEncore, regardeCorrection, reponseIncomplete, tempsDepasse } = textesGeneriques

/**
 * Ajouter une description ici
 * @fileOverview
 * @author Tommy Barroy <t.barroy@laposte.net>
 * @since 2017-10
 */
export const params = {
  outils: ['calculatrice', 'mathquill'],
  /**
   * Ce qui est paramétrable dans l’éditeur de graphe
   * @type {LegacySectionParamsParametres}
   */
  parametres: [
    ['nbrepetitions', 8, 'entier', 'Nombre de répétitions de la section'],
    ['indication', '', 'string', 'Indication pour chaque répétition'],
    ['limite', 0, 'entier', 'Temps disponible par répétition, en secondes'],
    ['nbchances', 2, 'entier', 'Nombre d’essais par répétition'],
    ['calcul_et_conclusion', true, 'boolean', 'Affiche automatiquement le calcul et la phrase réponse'],
    ['Total', false, 'boolean', 'L’élève doit également compléter le calcul et la conclusion.'],
    ['Addition', true, 'boolean', 'L’élève doit également compléter le calcul et la conclusion.'],
    ['Soustraction', true, 'boolean', 'L’élève doit également compléter le calcul et la conclusion.'],
    ['Multiplication', true, 'boolean', 'L’élève doit également compléter le calcul et la conclusion.'],
    ['Division', true, 'boolean', 'L’élève doit également compléter le calcul et la conclusion.'],
    ['Presentation', true, 'boolean', "true : Les indications de réussite et les boutons 'ok' et 'suivant' sont dans un cadre séparé à droite. <BR> false : Un seul cadre pour tout"],
    // le modèle gère la compatibilité ascendante avec l’ancien paramètre MiseEnPagePlusClaire
    ['theme', 'standard', 'liste', 'Modèle pour l’affichage', ['standard', 'zonesAvecImageDeFond']]

  ]

}

/**
 * section probleme
 * @this {Parcours}
 */
export default function main () {
  const me = this
  let ds = me.donneesSection
  const stor = this.storage
  // rien

  function affichecalc () {
    let operable1 = false
    let operable4 = false
    let operable3 = false
    let o1 = 0
    let o2 = 0
    let op = 0
    let buf = ''
    j3pEmpty(stor.tatab[0][3]); j3pEmpty(stor.tatab[1][3])
    if (stor.repform[2] === ds.textes.ajouter) {
      operable1 = true; op = 0; buf = '$+$'
    }
    if (stor.repform[2] === ds.textes.soustraire) {
      buf = ' $ - $  '; operable1 = true; op = 1
    }
    if (stor.repform[2] === ds.textes.multiplier) {
      buf = ' $ \\times $  '; operable1 = true; op = 2
    }
    if (stor.repform[2] === ds.textes.diviser) {
      buf = ' $ \\div $  '; operable1 = true; op = 3
    }
    j3pAffiche(stor.tatab[0][3], null, buf)
    j3pAffiche(stor.tatab[1][3], null, buf)

    if (stor.repform[2] === ds.textes.soustraire) {
      stor.tatab[0][4].innerHTML = stor.repform[3]
      stor.tatab[1][4].innerHTML = ' ? '
      if (stor.repform[3] === stor.variable1) { stor.tatab[1][4].innerHTML = stor.nb1; operable3 = true; o1 = stor.nb1 }
      if (stor.repform[3] === stor.variable2) { stor.tatab[1][4].innerHTML = stor.nb2; operable3 = true; o1 = stor.nb2 }
    } else {
      stor.tatab[0][2].innerHTML = stor.repform[3]
      stor.tatab[1][2].innerHTML = ' ? '
      if (stor.repform[3] === stor.variable1) { stor.tatab[1][2].innerHTML = stor.nb1; operable3 = true; o1 = stor.nb1 }
      if (stor.repform[3] === stor.variable2) { stor.tatab[1][2].innerHTML = stor.nb2; operable3 = true; o1 = stor.nb2 }
    }

    if (stor.repform[2] === ds.textes.soustraire) {
      stor.tatab[0][2].innerHTML = stor.repform[4]
      stor.tatab[1][2].innerHTML = ' ? '
      if (stor.repform[4] === stor.variable1) { stor.tatab[1][2].innerHTML = stor.nb1; operable4 = true; o2 = stor.nb1 }
      if (stor.repform[4] === stor.variable2) { stor.tatab[1][2].innerHTML = stor.nb2; operable4 = true; o2 = stor.nb2 }
    } else {
      stor.tatab[0][4].innerHTML = stor.repform[4]
      stor.tatab[1][4].innerHTML = ' ? '
      if (stor.repform[4] === stor.variable1) { stor.tatab[1][4].innerHTML = stor.nb1; operable4 = true; o2 = stor.nb1 }
      if (stor.repform[4] === stor.variable2) { stor.tatab[1][4].innerHTML = stor.nb2; operable4 = true; o2 = stor.nb2 }
    }

    stor.tatab[2][2].innerHTML = ' ? '
    stor.tatab2[0][2].innerHTML = ' ? '
    if (operable4 && operable3 && operable1) {
      let rep
      switch (op) {
        case 0 : rep = o1 + o2; break
        case 1 : rep = o2 - o1; break
        case 2 : rep = o1 * o2; break
        case 3 : rep = o1 / o2; break
      }
      stor.tatab[2][2].innerHTML = rep
      stor.tatab2[0][2].innerHTML = rep + ' '
    }
  }
  function affichecalcT () {
    let buf
    j3pEmpty(stor.tatab[0][3]); j3pEmpty(stor.tatab[1][3])
    if (stor.repform[2] === ds.textes.ajouter) {
      buf = '$+$'
    }
    if (stor.repform[2] === ds.textes.soustraire) {
      buf = ' $ - $  '
    }
    if (stor.repform[2] === ds.textes.multiplier) {
      buf = ' $ \\times $  '
    }
    if (stor.repform[2] === ds.textes.diviser) {
      buf = ' $ \\div $  '
    }
    j3pAffiche(stor.tatab[0][3], null, buf)
    j3pAffiche(stor.tatab[1][3], null, buf)

    if (stor.repform[2] === ds.textes.soustraire) {
      stor.tatab[0][4].innerHTML = stor.repform[3]
    } else {
      stor.tatab[0][2].innerHTML = stor.repform[3]
    }

    if (stor.repform[2] === ds.textes.soustraire) {
      stor.tatab[0][2].innerHTML = stor.repform[4]
    } else {
      stor.tatab[0][4].innerHTML = stor.repform[4]
    }
  }

  function gerezone2 () {
    gerezone(2, stor.zoneliste[1].reponse)
  }
  function gerezone3 () {
    gerezone(3, stor.zoneliste[2].reponse)
  }
  function gerezone4 () {
    gerezone(4, stor.zoneliste[3].reponse)
  }
  function gerezone (nb, reponse) {
    stor.repform[nb] = reponse
    if (nb === 2) {
      if (stor.repform[nb] === ds.textes.ajouter) stor.zone33.innerHTML = ds.textes.lienadd
      if (stor.repform[nb] === ds.textes.soustraire) stor.zone33.innerHTML = ds.textes.liensou
      if (stor.repform[nb] === ds.textes.multiplier) stor.zone33.innerHTML = ds.textes.lienmul
      if (stor.repform[nb] === ds.textes.diviser) stor.zone33.innerHTML = ds.textes.liendiv
    }
    if (ds.Total) { affichecalcT(); return }
    if (ds.calcul_et_conclusion) { affichecalc() }
  }
  function bonneReponse () {
    me.errDouble = false
    me.errNB1 = false
    me.errNB2 = false
    me.errCal = false
    me.errConcl = false
    // cette fonction renvoie true ou false si du moins une réponse a été saisie
    // (dans le cas contraire, elle n’est pas appelée
    // elle est appelée dans la partie "correction" de la section
    let ok = true

    me.barre = [false, false, false, false]

    if (stor.repform[2] !== stor.operation) {
      me.barre[1] = true
      ok = false
      stor.zoneliste[1].corrige(false)
    }

    if ((stor.repform[2] !== ds.textes.ajouter) && (stor.repform[2] !== ds.textes.multiplier)) {
      if (stor.repform[3] !== stor.variable1) {
        me.barre[2] = true
        ok = false
        stor.zoneliste[2].corrige(false)
      }
      if (stor.repform[4] !== stor.variable2) {
        me.barre[3] = true
        ok = false
        stor.zoneliste[3].corrige(false)
      }
    } else {
      if (stor.repform[3] !== stor.variable1 && stor.repform[3] !== stor.variable2) {
        me.barre[2] = true
        ok = false
        stor.zoneliste[2].corrige(false)
      }
      if (stor.repform[4] !== stor.variable2 && stor.repform[4] !== stor.variable1) {
        me.barre[3] = true
        ok = false
        stor.zoneliste[3].corrige(false)
      }
      if (stor.repform[4] === stor.repform[3]) {
        me.barre[3] = true
        ok = false
        stor.zoneliste[3].corrige(false)
      }
    }

    if (ok) {
      stor.zoneliste[1].corrige(true)
      stor.zoneliste[2].corrige(true)
      stor.zoneliste[3].corrige(true)
    }
    if (!ds.Total) return ok
    if (!ok) return false

    let r1, r2, rt
    r1 = parseInt(stor.rep1.reponse())
    r2 = parseInt(stor.rep2.reponse())
    const r3 = parseInt(stor.rep3.reponse())
    const r4 = parseInt(stor.rep4.reponse())

    if (stor.operation === 'je soustrais...') {
      rt = r1; r1 = r2; r2 = rt
    }

    if ((stor.repform[2] !== ds.textes.ajouter) && (stor.repform[2] !== ds.textes.multiplier)) {
      if (r1 !== stor.nb1) {
        me.errNB1 = true
        ok = false
        stor.rep1.corrige(false)
      }
      if (r2 !== stor.nb2) {
        me.errNB2 = true
        ok = false
        stor.rep2.corrige(false)
      }
    } else {
      if (r1 !== stor.nb1 && r1 !== stor.nb2) {
        me.errNB1 = true
        ok = false
        stor.rep1.corrige(false)
      }
      if (r2 !== stor.nb1 && r2 !== stor.nb2) {
        me.errNB2 = true
        ok = false
        stor.rep2.corrige(false)
      }
      if (r1 === r2) {
        me.errDouble = true
        ok = false
        stor.rep2.corrige(false)
      }
    }

    if (!ok) return false

    switch (stor.operation) {
      case 'je soustrais...':
        rt = r2 - r1
        break
      case 'je divise...':
        rt = j3pArrondi(r1 / r2, 0)
        break
      case 'j’ajoute...':
        rt = r1 + r2
        break
      case 'je multiplie...':
        rt = r1 * r2
        break
    }

    if (r3 !== rt) {
      me.errCal = true
      stor.rep3.corrige(false)
      return false
    }

    if (r4 !== rt) {
      me.errConcl = true
      stor.rep4.corrige(false)
      return false
    }

    return true
  }
  function yareponse () {
    if (me.isElapsed) { return true }
    if (!stor.zoneliste[1].changed) { stor.zoneliste[1].focus(); return false }
    if (!stor.zoneliste[2].changed) { stor.zoneliste[2].focus(); return false }
    if (!stor.zoneliste[3].changed) { stor.zoneliste[3].focus(); return false }
    if (ds.Total) {
      if (stor.rep1.reponse() === '') {
        stor.rep1.focus()
        return false
      }
      if (stor.rep2.reponse() === '') {
        stor.rep2.focus()
        return false
      }
      if (stor.rep3.reponse() === '') {
        stor.rep3.focus()
        return false
      }
      if (stor.rep4.reponse() === '') {
        stor.rep4.focus()
        return false
      }
    }
    return true
  }
  function getDonnees () {
    return {

      // Nombre de répétitions de l’exercice
      nbrepetitions: 8,
      nbetapes: 1,
      calcul_et_conclusion: true,
      Presentation: true,

      Total: false,
      Addition: true,
      Soustraction: true,
      Multiplication: true,
      Division: true,

      // Si indication !== "" alors un lien "Indication" s’affiche dans la zone Inférieure Gauche (zones.IG
      // une indication<br>de deux lignes...";
      indication: '',

      // pour le cas de presentation1bis
      // En effet pas d’indication possible pour cette présentation
      // surchargeindication: false;

      // Nombre de chances dont dispose l’él§ve pour répondre à la question
      nbchances: 2,

      structure: 'presentation3', //  || "presentation2" || "presentation3"  || "presentation1bis"

      // Paramètres de la section, avec leurs valeurs par défaut.

      /*
          Les textes présents dans la section
          Sont donc accessibles dans le code par donneesSection.textes.phrase1
          Possibilité d’écrire dans le code :
          var lestextes: donneesSection.textes;
          puis accès à l’aide de lestextes.phrase1
          */
      textes: {
        couleurexplique: '#A9E2F3',
        couleurcorrec: '#A9F5A9',
        couleurenonce: '#D8D8D8',
        couleurenonceg: '#F3E2A9',
        couleuretiquettes: '#A9E2F3',
        consignegenerale: 'Complète le raisonnement pour répondre au problème suivant.',
        raisonnement: 'Raisonnement',
        raisonnementaddition: ' j’ajoute  ',
        additionsuite: ' #3# et #4#',
        multiplicationsuite: ' #3# par #4#',
        divisionsuite: ' #3# par #4#',
        soustractionsuite: ' #3# à #4#',
        raisonnementmultiplication: ' je multiplie  ',
        raisonnementdivision: ' je divise  ',
        raisonnementsoustraction: ' je soustrais  ',
        raisonnementdebut: 'Pour calculer  ',

        pb: [
          {
            consigne: ' Louis a £a billes dans la poche droite et £b billes dans la poche gauche. <BR> Combien a-t-il de billes en tout ?',
            consignevar1: 'le nombre de billes dans la poche droite',
            consignevar2: 'le nombre de billes dans la poche gauche',
            consignerep: 'le nombre total de billes',
            consigneEgal: ' est égal à ',
            consignepiege1: 'le nombre de poches',
            consignepiege2: 'le nombre de billes',
            consigneunite: ' bille(s)',
            consigneop: '+'
          },
          {
            consigne: ' Jean avait £b cartes Pokemon au début de la récréation. <BR> A la fin de la récréation, il lui en restait £a. <BR>  Combien a-t-il perdu de cartes ?',
            consignevar1: 'le nombre de cartes après la récréation',
            consignevar2: 'le nombre de cartes avant la récréation',
            consignerep: 'le nombre de cartes perdues',
            consigneEgal: ' est égal à ',
            consignepiege1: 'le nombre de cartes gagnées',
            consignepiege2: 'le nombre de cartes',
            consigneunite: ' carte(s)',
            consigneop: '-'
          },
          {
            consigne: ' Zakaria a £a copains. <BR> Chacun de ses copains a £b billes. <BR>  Combien de billes ont ses copains en tout ? ',
            consignevar1: 'le nombre de copains',
            consignevar2: 'le nombre de billes de chaque copain',
            consignerep: 'le nombre total de billes',
            consigneEgal: ' est égal à ',
            consignepiege1: 'le nombre de billes du professeur',
            consignepiege2: 'le nombre de billes',
            consigneunite: ' bille(s)',
            consigneop: '×'
          },
          {
            consigne: ' Le papa de Jason a £a bonbons qu’il veut partager équitablement entre ses £b enfants. <BR> Combien donnera-t-il de bonbons à chacun ?',
            consignevar1: 'le nombre de bonbons du papa',
            consignevar2: 'le nombre d’enfants',
            consignerep: 'le nombre de bonbons par enfant',
            consigneEgal: ' est égal à ',
            consignepiege1: 'le nombre de papa',
            consignepiege2: 'le nombre d’élèves',
            consigneunite: ' bonbon(s)',
            consigneop: '÷'
          },
          {
            consigne: ' Mélanie a marché £a km lundi et £b km mardi. <BR> Combien de kilomètres a-t-elle parcouru en tout ?',
            consignevar1: 'la distance parcourue le lundi',
            consignevar2: 'la distance parcourue le mardi',
            consignerep: 'la distance totale parcourue',
            consigneEgal: ' est égale à ',
            consignepiege1: 'la distance parcourue le dimanche',
            consignepiege2: 'la distance parcourue',
            consigneunite: ' km',
            consigneop: '+'
          },
          {
            consigne: ' Walid achète un jouet. Il donne un bon de £b € au marchand. Celui-ci lui rend £a €. <BR> Quel est le prix du jouet ?',
            consignevar1: 'la somme rendue',
            consignevar2: 'la somme donnée au marchand',
            consignerep: 'le prix du jouet',
            consigneEgal: ' est égal à ',
            consignepiege1: 'le prix du marchand',
            consignepiege2: 'la somme du produit',
            consigneunite: ' €',
            consigneop: '-'
          },
          {
            consigne: ' Castafiora adore écouter son morceau préféré qui dure £a minutes.<br> Aujourd’hui, elle l’a écouté £b fois. <BR> Combien de temps a-t-elle écouté son morceau préféré ?',
            consignevar1: 'la durée du morceau',
            consignevar2: 'le nombre d’écoutes',
            consignerep: 'la durée totale d’écoute',
            consigneEgal: ' est égale à ',
            consignepiege1: 'le nombre de durée',
            consignepiege2: 'le nombre de Catasphiora',
            consigneunite: ' min',
            consigneop: '×'
          },
          {
            consigne: 'Adrien a posé £b cubes les uns sur les autres. La pile de cubes mesure £a cm. <BR> Quelle est la hauteur d’un cube ?',
            consignevar1: 'la hauteur de la pile',
            consignevar2: 'le nombre de cubes',
            consignerep: 'la hauteur d’un cube ',
            consigneEgal: ' est égale à ',
            consignepiege1: 'le nombre de piles de cube',
            consignepiege2: 'la hauteur d’Adrien',
            consigneunite: ' cm',
            consigneop: '÷'
          },
          {
            consigne: 'Romane a £a voitures rouges et £b voitures vertes. <br> Combien a-t-elle de voitures en tout ?',
            consignevar1: 'le nombre de voitures rouges',
            consignevar2: 'le nombre de voitures vertes',
            consignerep: 'le nombre total de voitures',
            consigneEgal: ' est égal à ',
            consignepiege1: 'le nombre de voitures',
            consignepiege2: 'le nombre de voitures bleues',
            consigneunite: ' voiture(s)',
            consigneop: '+'
          },
          {
            consigne: 'Mika a effectué £a tours du lac. Le périmètre du lac est de £b km. <br> Quelle distance totale a-t-il parcourue ?',
            consignevar1: 'le nombre de tours',
            consignevar2: 'le périmètre du lac',
            consignerep: 'la distance parcourue',
            consigneEgal: ' est égale à ',
            consignepiege1: 'le nombre de lacs',
            consignepiege2: 'le périmètre du tours',
            consigneunite: ' km',
            consigneop: '×'
          },
          {
            consigne: ' Le papa de Jason a £a bonbons qu’il a partagé équitablement entre des enfants. <br> Il a donné £b bonbons à chaque enfant.<BR> Combien y a-t-il d’enfants ?',
            consignevar1: 'le nombre de bonbons du papa',
            consignevar2: 'le nombre de bonbons par enfant',
            consignerep: 'le nombre d’enfants',
            consigneEgal: ' est égal à ',
            consignepiege1: 'le nombre de papa',
            consignepiege2: 'le nombre d’élèves',
            consigneunite: ' enfant(s)',
            consigneop: '÷'
          }
        ],
        choisirdonnees: 'Données ?',
        choisiroperation: 'Opérations?',
        lienadd: ' et ',
        liensou: ' à ',
        lienmul: ' par ',
        liendiv: ' par ',
        lienvide: ' et ',
        liensep: ', ',
        ajouter: 'j’ajoute...',
        soustraire: 'je soustrais...',
        multiplier: 'je multiplie...',
        diviser: 'je divise...',
        calcul: 'Calcul',
        phrasecalcul: '£b £c £d = £a',
        conclusion: 'Conclusion'
      },

      // Par défaut, pas de temps limite. Décommentez pour avoir un temps limite par défaut. En secondes.

      // limite: 10;
      /*
          Phrase d’état renvoyée par la section
          Si pe=0 alors la section est quantitative et renvoie un score entre 0 et 1.
          Dans le cas où la section est qualitative, lister les différents pe renvoyées :
              pe_1: "toto"
              pe_2: "tata"
              etc
          Dans la partie navigation, il faudra affecter la variable parcours.pe à l’une des précédentes.
          Par exemple,
          parcours.pe: donneesSection.pe_2
          */
      pe: 0
    }
  }
  function disaAll () {
    stor.zoneliste[1].disable()
    stor.zoneliste[2].disable()
    stor.zoneliste[3].disable()
    if (ds.Total) {
      stor.rep1.disable()
      stor.rep2.disable()
      stor.rep3.disable()
      stor.rep4.disable()
    }
  }
  function metToutvert () {
    if (ds.Total) {
      stor.rep1.corrige(true)
      stor.rep2.corrige(true)
      stor.rep3.corrige(true)
      stor.rep4.corrige(true)
    }
  }
  function barrelesFo () {
    if (me.barre[1]) stor.zoneliste[1].barre()
    if (me.barre[2]) stor.zoneliste[2].barre()
    if (me.barre[3]) stor.zoneliste[3].barre()
    if (ds.Total) {
      stor.rep1.barreIfKo()
      stor.rep2.barreIfKo()
      stor.rep3.barreIfKo()
      stor.rep4.barreIfKo()
    }
  }

  function AffcorFo (bool) {
    if (me.errDouble) {
      stor.yaexplik = true
      j3pAffiche(stor.lesdiv.explRais, null, 'Tu écris deux fois la même valeur !\n')
    }
    if (me.errNB1 || me.errNB2) {
      stor.yaexplik = true
      j3pAffiche(stor.lesdiv.explRais, null, 'Je ne retrouve pas les valeurs attendues !\n')
    }
    if (me.errCal) {
      stor.yaexplik = true
      j3pAffiche(stor.lesdiv.explRais, null, 'Erreur de calcul !\n')
    }
    if (me.errConcl) {
      stor.yaexplik = true
      j3pAffiche(stor.lesdiv.explRais, null, 'Recopie correctement ton résultat !\n')
    }
    if (bool) {
      if (ds.calcul_et_conclusion && !ds.Total) {
        stor.repform[1] = stor.variablereponse
        stor.repform[2] = stor.operation
        stor.repform[3] = stor.variable1
        stor.repform[4] = stor.variable2
        affichecalc()
      }
      if (ds.Total) {
        stor.repform[1] = stor.variablereponse
        stor.repform[2] = stor.operation
        stor.repform[3] = stor.variable1
        stor.repform[4] = stor.variable2
        affichecalcT()
        if (stor.operation === 'je soustrais...') {
          const s = stor.nb1; stor.nb1 = stor.nb2; stor.nb2 = s
        }
        if (stor.rep1.reponse() !== String(stor.nb1)) j3pAffiche(stor.tab23eeel0c1, null, '$' + stor.nb1 + '$')
        if (stor.rep2.reponse() !== String(stor.nb2)) j3pAffiche(stor.tab25eeel0c1, null, '$' + stor.nb2 + '$')
        if (stor.rep3.reponse() !== String(stor.lalalfin)) j3pAffiche(stor.tab33eeel0c1, null, '$' + stor.lalalfin + '$')
        if (stor.rep4.reponse() !== String(stor.lalalfin)) j3pAffiche(stor.zoneM7l0c2eeel0c1, null, '$' + stor.lalalfin + '$')
        stor.tab23eeel0c1.style.color = me.styles.petit.correction.color
        stor.tab25eeel0c1.style.color = me.styles.petit.correction.color
        stor.tab33eeel0c1.style.color = me.styles.petit.correction.color
        stor.zoneM7l0c2eeel0c1.style.color = me.styles.petit.correction.color
        if (stor.rep1.reponse() !== String(stor.nb1) && stor.rep1.isOk() !== false) stor.rep1.barre()
        if (stor.rep2.reponse() !== String(stor.nb2) && stor.rep2.isOk() !== false) stor.rep2.barre()
        if (stor.rep3.reponse() !== String(stor.lalalfin) && stor.rep3.isOk() !== false) stor.rep3.barre()
        if (stor.rep4.reponse() !== String(stor.lalalfin) && stor.rep4.isOk() !== false) stor.rep4.barre()
        stor.tab23eeel0c1.style.background = ds.textes.couleurcorrec
        stor.tab25eeel0c1.style.background = ds.textes.couleurcorrec
        stor.tab33eeel0c1.style.background = ds.textes.couleurcorrec
        stor.zoneM7l0c2eeel0c1.style.background = ds.textes.couleurcorrec
      }
      if (me.barre[1] || me.barre[2] || me.barre[3]) {
        stor.yaco1 = true
        const phrase = ds.textes.raisonnementdebut + stor.variablereponse + ',<br>' + stor.operationco + ' ' + stor.variable1 + ' ' + stor.varlien + stor.variable2
        j3pAffiche(stor.lesdiv.explRais, null, phrase.replace(/à le/g, 'au'))
      }
      barrelesFo()
      disaAll()
    } else {
      me.afficheBoutonValider()
    }
  }
  function initSection () {
    let i, tu
    ds = getDonnees()
    me.donneesSection = ds

    me.surcharge()
    if (ds.Presentation) { ds.structure = 'presentation1' }
    ds.nbitems = ds.nbetapes * ds.nbrepetitions
    // Construction de la page
    me.construitStructurePage(ds.structure)

    stor.pbpos = j3pClone(ds.textes.pb)
    for (i = stor.pbpos.length - 1; i > -1; i--) {
      tu = stor.pbpos[i].consigneop
      if ((tu === '+' && !ds.Addition) || (tu === '-' && !ds.Soustraction) || (tu === '×' && !ds.Multiplication) || (tu === '÷' && !ds.Division)) stor.pbpos.splice(i, 1)
    }
    if (stor.pbpos.length === 0) {
      j3pShowError('pb paramétrage')
      stor.pbpos = j3pClone(ds.textes.pb)
    }
    stor.pbpos2 = []
    // DECLARATION DES VARIABLES
    // reponse attendue
    stor.variable1 = ''
    stor.nb1 = ''
    stor.variable2 = ''
    stor.nb2 = ''
    stor.operation = ''
    stor.variablereponse = ''
    stor.consigneEgal = ''
    stor.variablepiege1 = ''
    stor.variablepiege2 = ''
    stor.consigneaffichee = ''
    stor.inconnue1nom = ''
    stor.inconnue1choix = ['', '', '']
    stor.inconnue2nom = ''
    stor.inconnue2choix = ['', '', '']
    stor.unite = ''

    // contiendra soit consigne1 soit consigne2 (1 fois sur 2)
    stor.consigneaffichee = ''

    /*
             Par convention,`
            `   me.typederreurs[0] = nombre de bonnes réponses
                me.typederreurs[1] = nombre de fois où l’élève a utilisé une chance supplémentaire
                me.typederreurs[2] = nombre de mauvaises réponses
                me.typederreurs[10] = nombre de fois où l’élève a eu faux à cause d’un temps limite
                LES AUTRES peuvent être utilisées par la détermination de la pe dans le cas d’une section qualitative
             */
    me.typederreurs = [0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0]

    me.score = 0

    me.afficheTitre('Problèmes et raisonnement')

    if (ds.indication) me.indication(me.zones.IG, ds.indication)
    enonceMain()
  }
  function enonceMain () {
    me.videLesZones()
    if (ds.structure === 'presentation3') {
      me.ajouteBoutons()
      me.cacheBoutonSectionSuivante()
    }

    me.afficheBoutonValider()
    // ou dans le cas de presentation3
    // me.ajouteBoutons();

    stor.zoneliste = []

    if (stor.pbpos2.length === 0) {
      stor.pbpos2 = j3pClone(stor.pbpos)
      stor.pbpos2 = j3pShuffle(stor.pbpos2)
    }

    stor.Lexo = stor.pbpos2.pop()

    if (stor.Lexo.consigneop === '÷') { // pb division
      do {
        stor.nb2 = j3pGetRandomInt(2, 7)
        stor.nb1 = j3pGetRandomInt(15, 25) * stor.nb2
      } while (stor.nb1 === stor.nb2)
      stor.operation = ds.textes.diviser
      stor.varlien = ds.textes.liendiv
      stor.operationco = ds.textes.raisonnementdivision
      stor.lalalfin = j3pArrondi(stor.nb1 / stor.nb2, 0)
    }
    if (stor.Lexo.consigneop === '+') {
      do {
        stor.nb1 = j3pGetRandomInt(3, 15)
        stor.nb2 = j3pGetRandomInt(3, 15)
      } while (stor.nb1 === stor.nb2)
      stor.operation = ds.textes.ajouter
      stor.varlien = ds.textes.lienadd
      stor.operationco = ds.textes.raisonnementaddition
      stor.lalalfin = j3pArrondi(stor.nb1 + stor.nb2, 0)
    }
    if (stor.Lexo.consigneop === '-') {
      stor.nb1 = j3pGetRandomInt(3, 15)
      stor.nb2 = j3pGetRandomInt(3, 15) + stor.nb1
      stor.operation = ds.textes.soustraire
      stor.varlien = ds.textes.liensou
      stor.operationco = ds.textes.raisonnementsoustraction
      stor.lalalfin = j3pArrondi(stor.nb2 - stor.nb1, 0)
    }
    if (stor.Lexo.consigneop === '×') {
      do {
        stor.nb2 = j3pGetRandomInt(3, 15)
        stor.nb1 = j3pGetRandomInt(3, 15)
      } while (stor.nb1 === stor.nb2)
      stor.operation = ds.textes.multiplier
      stor.varlien = ds.textes.lienmul
      stor.operationco = ds.textes.raisonnementmultiplication
      stor.lalalfin = j3pArrondi(stor.nb1 * stor.nb2, 0)
    }

    stor.variable1 = stor.Lexo.consignevar1
    stor.variable2 = stor.Lexo.consignevar2
    stor.variablereponse = stor.Lexo.consignerep
    stor.consigneEgal = stor.Lexo.consigneEgal
    stor.consigneaffichee = stor.Lexo.consigne
    stor.variablepiege1 = stor.Lexo.consignepiege1
    stor.variablepiege2 = stor.Lexo.consignepiege2
    stor.unite = stor.Lexo.consigneunite

    const i = j3pGetRandomInt(0, 4)
    stor.inconnue1choix[0] = ds.textes.choisirdonnees
    stor.inconnue1choix[i + 1] = stor.variablereponse
    stor.inconnue1choix[(1 + i) % 5 + 1] = stor.variable1
    stor.inconnue1choix[(2 + i) % 5 + 1] = stor.variable2
    stor.inconnue1choix[(3 + i) % 5 + 1] = stor.variablepiege1
    stor.inconnue1choix[(4 + i) % 5 + 1] = stor.variablepiege2
    stor.inconnue2choix = [ds.textes.choisiroperation, ds.textes.ajouter, ds.textes.soustraire, ds.textes.multiplier, ds.textes.diviser]

    stor.repform = []
    stor.repform = [' ? ', ' ? ', ' ? ', ' ? ', ' ? ', ' ? ']

    stor.lesdiv = {}
    stor.lesdiv.conteneur = j3pAddElt(me.zones.MG, 'div', {
      style: me.styles.etendre('toutpetit.enonce', { padding: '6px' })
    })
    const tabG = addDefaultTable(stor.lesdiv.conteneur, 6, 1)
    if (!ds.Presentation) {
      const Harico = addDefaultTable(tabG[0][0], 1, 3)
      stor.lesdiv.correction = Harico[0][2]
      stor.lesdiv.enonce1 = Harico[0][0]
      Harico[0][1].style.width = '40px'
    } else {
      stor.lesdiv.correction = j3pAddElt(me.zones.MD, 'div', {
        style: me.styles.etendre('toutpetit.enonce', { padding: '6px' })
      })
      stor.lesdiv.enonce1 = tabG[0][0]
    }
    j3pAffiche(stor.lesdiv.enonce1, null, '<i>' + stor.consigneaffichee + '</i>',
      {
        a: stor.nb1,
        b: stor.nb2
      })
    if (ds.Total) {
      j3pAffiche(tabG[1][0], null, '<b> Complète le raisonnement, le calcul et la phrase réponse pour résoudre ce problème</b>.')
    } else {
      j3pAffiche(tabG[1][0], null, '<b> Complète le raisonnement pour résoudre ce problème</b>.')
    }

    tabG[1][0].style.color = me.styles.toutpetit.correction.color
    j3pAffiche(tabG[2][0], null, '<b><u>Raisonnement</u></b>:\n')
    j3pAffiche(tabG[2][0], null, ds.textes.raisonnementdebut + stor.variablereponse + ds.textes.liensep + '\n')
    stor.zoneliste[1] = ListeDeroulante.create(tabG[2][0], stor.inconnue2choix, {
      onChange: gerezone2,
      centre: true
    })
    j3pAffiche(tabG[2][0], null, ' ')
    stor.zoneliste[2] = ListeDeroulante.create(tabG[2][0], stor.inconnue1choix, {
      onChange: gerezone3,
      centre: true
    })
    stor.zone33 = j3pAddElt(tabG[2][0], 'span')
    j3pAffiche(stor.zone33, null, ds.textes.lienvide)
    stor.zoneliste[3] = ListeDeroulante.create(tabG[2][0], stor.inconnue1choix, {
      onChange: gerezone4,
      centre: true
    })
    stor.lesdiv.explRais = tabG[3][0]
    stor.lesdiv.explRais.style.color = me.styles.cfaux

    if (ds.calcul_et_conclusion || ds.Total) {
      const tabG2 = addDefaultTable(tabG[4][0], 3, 1)
      let squizz = ''
      if (!ds.Total) {
        tabG[4][0].style.backgroundColor = ds.textes.couleuretiquettes
        tabG[5][0].style.backgroundColor = ds.textes.couleuretiquettes
        squizz = '<i> (automatique)</i>'
      } else {
        tabG[4][0].classList.add('travail')
        tabG[5][0].classList.add('travail')
      }
      j3pAffiche(tabG2[0][0], null, '<b> <u>' + ds.textes.calcul + '</b></u>:' + squizz)
      stor.lesdiv.zoneCalcul = tabG2[1][0]
      stor.lesdiv.expliqCalul = tabG2[2][0]
      stor.lesdiv.expliqCalul.style.color = me.styles.toutpetit.correction.color

      stor.lesdiv.PartieConcl = tabG[5][0]
      const tabG3 = addDefaultTable(stor.lesdiv.PartieConcl, 3, 1)
      j3pAffiche(tabG3[0][0], null, '<b> <u>' + ds.textes.conclusion + '</b></u>:' + squizz)
      stor.lesdiv.zoneConcl = tabG3[1][0]
      stor.lesdiv.exliqconcl = tabG3[2][0]
      stor.lesdiv.exliqconcl.style.color = me.styles.toutpetit.correction.color

      const tatab = addTable(stor.lesdiv.zoneCalcul, { nbLignes: 3, nbColonnes: 5, className: 'probleme' })
      stor.tatab = getCells(tatab)
      j3pAffiche(stor.tatab[0][0], null, stor.variablereponse)
      j3pAffiche(stor.tatab[0][1], null, ' = ')
      j3pAffiche(stor.tatab[0][2], null, ' ? ')
      j3pAffiche(stor.tatab[0][3], null, ' ? ')
      j3pAffiche(stor.tatab[0][4], null, ' ? ')
      j3pAffiche(stor.tatab[1][0], null, stor.variablereponse)
      j3pAffiche(stor.tatab[1][1], null, ' = ')
      j3pAffiche(stor.tatab[1][2], null, ' ? ')
      j3pAffiche(stor.tatab[1][3], null, ' ? ')
      j3pAffiche(stor.tatab[1][4], null, ' ? ')
      j3pAffiche(stor.tatab[2][0], null, stor.variablereponse)
      j3pAffiche(stor.tatab[2][1], null, ' = ')
      j3pAffiche(stor.tatab[2][2], null, ' ? ')

      const tatab2 = addTable(stor.lesdiv.zoneConcl, { nbLignes: 1, nbColonnes: 4, className: 'probleme' })
      stor.tatab2 = getCells(tatab2)
      j3pAffiche(stor.tatab2[0][0], null, stor.variablereponse)
      j3pAffiche(stor.tatab2[0][1], null, stor.consigneEgal)
      j3pAffiche(stor.tatab2[0][2], null, ' ? ')
      j3pAffiche(stor.tatab2[0][3], null, stor.unite)
      if (ds.Total) {
        j3pEmpty(stor.tatab[1][2])
        const t1 = addDefaultTable(stor.tatab[1][2], 1, 2)
        stor.rep1 = new ZoneStyleMathquill1(t1[0][0], {
          restric: '0123456789',
          limite: 15,
          limitenb: 4,
          enter: me.sectionCourante.bind(me)
        })
        stor.tab23eeel0c1 = t1[0][1]
        j3pEmpty(stor.tatab[1][4])
        const t2 = addDefaultTable(stor.tatab[1][4], 1, 2)
        stor.rep2 = new ZoneStyleMathquill1(t2[0][0], {
          restric: '0123456789',
          limite: 15,
          limitenb: 4,
          enter: me.sectionCourante.bind(me)
        })
        stor.tab25eeel0c1 = t2[0][1]
        j3pEmpty(stor.tatab[2][2])
        const t3 = addDefaultTable(stor.tatab[2][2], 1, 2)
        stor.rep3 = new ZoneStyleMathquill1(t3[0][0], {
          restric: '0123456789',
          limite: 15,
          limitenb: 4,
          enter: me.sectionCourante.bind(me)
        })
        stor.tab33eeel0c1 = t3[0][1]
        j3pEmpty(stor.tatab2[0][2])
        const t4 = addDefaultTable(stor.tatab2[0][2], 1, 2)
        stor.rep4 = new ZoneStyleMathquill1(t4[0][0], {
          restric: '0123456789',
          limite: 15,
          limitenb: 4,
          enter: me.sectionCourante.bind(me)
        })
        stor.zoneM7l0c2eeel0c1 = t4[0][1]
      }
    }

    stor.lesdiv.enonce1.classList.add('enonce')
    tabG[1][0].classList.add('enonce')
    tabG[2][0].classList.add('travail')
    me.zonesElts.MG.classList.add('fond')

    // Obligatoire

    me.afficheBoutonValider()

    me.finEnonce()
  }

  switch (this.etat) {
    case 'enonce': {
      // code exécuté au lancement de la section

      if (this.debutDeLaSection) {
        initSection()
      } else {
        enonceMain()
      }
      break
    } // case "enonce":

    case 'correction':
      // On teste pas si une réponse a été saisie
      stor.yaco1 = false
      stor.yaexplik = false
      if (ds.Total) j3pEmpty(stor.lesdiv.explRais)
      if (!yareponse()) {
        stor.lesdiv.correction.style.color = this.styles.cfaux
        stor.lesdiv.correction.innerHTML = reponseIncomplete
        this.afficheBoutonValider()
      } else {
        if (bonneReponse()) {
          this.score += j3pArrondi(1 / this.donneesSection.nbetapes, 1)
          stor.lesdiv.correction.style.color = this.styles.cbien
          stor.lesdiv.correction.innerHTML = cBien

          disaAll()
          metToutvert()

          this.typederreurs[0]++
          this.cacheBoutonValider()
          this.etat = 'navigation'
          this.sectionCourante()
        } else {
          // Pas de bonne réponse
          stor.lesdiv.correction.style.color = this.styles.cfaux

          // A cause de la limite de temps :
          if (this.isElapsed) { // limite de temps
            stor.lesdiv.correction.innerHTML = tempsDepasse
            this.typederreurs[10]++
            this.cacheBoutonValider()
            AffcorFo(true)
            this.etat = 'navigation'
            this.sectionCourante()
          } else {
            stor.lesdiv.correction.innerHTML = cFaux
            if (this.essaiCourant < this.donneesSection.nbchances) {
              stor.lesdiv.correction.innerHTML += '<br>' + essaieEncore
              AffcorFo(false)
              this.typederreurs[1]++
            } else {
              this.cacheBoutonValider()
              stor.lesdiv.correction.innerHTML += '<br>' + regardeCorrection
              AffcorFo(true)
              this.typederreurs[2]++
              this.etat = 'navigation'
              this.sectionCourante()
            }
          }
        }
      }
      // Obligatoire

      if (ds.theme === 'zonesAvecImageDeFond') {
        if (stor.yaexplik) stor.lesdiv.explRais.classList.add('explique')
        if (stor.yaco1) stor.lesdiv.explRais.classList.add('correction')
      }
      this.finCorrection()
      break // case "correction":

    case 'navigation':

      if (this.sectionTerminee()) {
        // On détermine la phrase d’état renvoyée par la section
        this.parcours.pe = (this.donneesSection.nbetapes * this.score) / this.donneesSection.nbitems
        this.afficheBoutonSectionSuivante()
        this.focus('sectioncontinuer')
      } else {
        this.etat = 'enonce'
        this.afficheBoutonSuite()
        this.focus('boutoncontinuer')
      }
      // Obligatoire
      this.finNavigation()

      break // case "navigation":
  }
}
