import { j3pAddContent, j3pAddElt, j3pAjouteBouton, j3pArrondi, j3pClone, j3pDetruit, j3pEmpty, j3pGetNewId, j3pGetRandomBool, j3pGetRandomInt, j3pModale, j3pPGCD, j3pShowError, j3pShuffle } from 'src/legacy/core/functions'
import { j3pAffiche } from 'src/lib/mathquill/functions'
import { getMtgCore } from 'src/lib/outils/mathgraph'
import { addDefaultTable } from 'src/legacy/themes/table'
import { ecrisBienMathquill } from 'src/legacy/outils/zoneStyleMathquill/functions'
import { j3pCreeSVG } from 'src/legacy/core/functionsSvg'
import ZoneStyleMathquill2 from 'src/legacy/outils/zoneStyleMathquill/ZoneStyleMathquill2'
import ZoneStyleMathquill1 from 'src/legacy/outils/zoneStyleMathquill/ZoneStyleMathquill1'
import $ from 'jquery'
import ListeDeroulante from 'src/legacy/outils/listeDeroulante/ListeDeroulante'
import BoutonsStyleMatquill from 'src/legacy/outils/boutonsStyleMatquill'
import textesGeneriques from 'src/lib/core/textes'

const { cBien, cFaux, essaieEncore, regardeCorrection, reponseIncomplete, tempsDepasse } = textesGeneriques

/**
 * Ajouter une description ici
 * @fileOverview
 * @author Tommy Barroy <t.barroy@laposte.net>
 * @since 2017-10
 */
export const params = {
  outils: ['calculatrice', 'mathquill'],
  /**
   * Ce qui est paramétrable dans l’éditeur de graphe
   * @type {LegacySectionParamsParametres}
   */
  parametres: [
    ['nbrepetitions', 8, 'entier', 'Nombre de répétitions de la section'],
    ['limite', 0, 'entier', 'Temps disponible par répétition, en secondes'],
    ['nbchances', 2, 'entier', 'Nombre d’essais par répétition'],
    ['Question', 'Diagramme_nom', 'liste', 'blabla ', ['Diagramme_tout', 'Diagramme_nom', 'Diagramme_nombre']],
    ['Difficulte', '1', 'liste', '1 ou 2 ou les deux', ['1', '2', 'les deux']],
    ['Addition', true, 'boolean', 'Addition possible'],
    ['Soustraction', true, 'boolean', 'Soustraction possible'],
    ['Multiplication', true, 'boolean', 'Multiplication possible'],
    ['Division', true, 'boolean', 'Division possible'],
    ['Change', true, 'boolean', 'Changement possible'],
    ['theme', 'standard', 'liste', 'Modèle pour l’affichage', ['standard', 'zonesAvecImageDeFond']]
  ]
}

/**
 * section probleme02
 * @this {Parcours}
 */
export default function main () {
  const me = this
  let ds = me.donneesSection
  const stor = this.storage
  let svgId = stor.svgId

  function getDonnees () {
    return {

      // Nombre de répétitions de l’exercice
      nbrepetitions: 8,
      nbetapes: 2,
      Question: 'Diagramme_nom',
      limite: 0,
      Difficulte: '1',
      Addition: true,
      Soustraction: true,
      Change: true,
      Multiplication: true,
      Division: true,

      // Si indication !== "" alors un lien "Indication" s’affiche dans la zone Inférieure Gauche (zones.IG
      // une indication<br>de deux lignes...";

      // pour le cas de presentation1bis
      // En effet pas d’indication possible pour cette présentation
      // surchargeindication: false;

      // Nombre de chances dont dispose l’él§ve pour répondre à la question
      nbchances: 2,

      structure: 'presentation3', //  || "presentation2" || "presentation3"  || "presentation1bis"

      // Paramètres de la section, avec leurs valeurs par défaut.

      /*
          Les textes présents dans la section
          Sont donc accessibles dans le code par donneesSection.textes.phrase1
          Possibilité d’écrire dans le code :
          var lestextes: donneesSection.textes;
          puis accès à l’aide de lestextes.phrase1
          */
      textes: {
        pb: [
          {
            total: 'nombre total de billes',
            decoupes: ['nombre de billes dans la poche gauche', 'nombre de billes dans la poche droite'],
            contraintes: ['entier', 'positif'],
            op: '+',
            descrip1: '£a a £b billes en tout.',
            descripEl: ['£a a £b billes dans la poche gauche.', '£a a £b billes dans la poche droite.'],
            descripPLus: 'On cherche le nombre total de billes.',
            descripMoins: 'On cherche le '
          },
          {
            total: 'nombre cartes pokémon avant la récréation',
            decoupes: ['nombre cartes pokémon après la récréation', 'nombre de cartes perdues'],
            contraintes: ['entier', 'positif'],
            op: '+',
            descrip1: '£a avait £b cartes pokémon avant la récréation.',
            descripEl: ['£a a £b cartes pokémon après la récréation.', '£a a perdu £b cartes pokémon pendant la récréation.'],
            descripPLus: 'On cherche le nombre cartes pokémon avant la récréation.',
            descripMoins: 'On cherche le '
          },
          {
            decoupes: ['nombre de billes d’un copain', 'nombre de copains'],
            total: 'nombre de total de billes',
            nbpart: 'nombre de copains',
            tailepart: 'nombre de billes d’un copain',
            op: '*',
            contraintes: ['entier', 'positif'],
            descrip1: 'Les copains de £a ont £b billes en tout.',
            descripEl: ['£a a £b copains', 'Chaque copain de £a a £b billes.'],
            descripNbPart: 'On cherche le nombre de copains de £a.',
            descripPart: 'On cherche le nombre de billes de chaque copain.',
            descripTout: 'On cherche le nombre total de billes des copains.'
          },
          {
            decoupes: ['nombre de bonbons par enfant', 'nombre d’enfants'],
            total: 'nombre de bonbons de la maman',
            nbpart: 'nombre d’enfants',
            tailepart: 'nombre de bonbons par enfant',
            op: '*',
            contraintes: ['entier', 'positif'],
            descrip1: 'La maman de £a partage équitablement ses £b bonbons entre ses enfants.',
            descripEl: ['la maman de £a a £b enfants', 'Chaque enfant reçoit £b bonbons.'],
            descripNbPart: 'On cherche le nombre d’enfants.',
            descripPart: 'On cherche le nombre de bonbon de chaque enfant.',
            descripTout: 'On cherche le nombre de bonbons de la maman.'
          },
          {
            total: 'distance totale parcourue les 4 jours',
            decoupes: ['distance parcourue le lundi', 'distance parcourue le mardi', 'distance parcourue le mercredi', 'distance parcourue le jeudi'],
            contraintes: ['entier', 'positif'],
            op: '+',
            descrip1: '£a a parcouru £b km en 4 jours.',
            descripEl: ['£a a marché £b km dans le lundi.', '£a a roulé £b km le mardi.', '£a a nagé £b km le mercredi.', '£a a couru £b km le jeudi.'],
            descripPLus: 'On cherche la distance totale parcourue les 4 jours.',
            descripMoins: 'On cherche la '
          },
          {
            decoupes: ['longueur de la piste', 'nombre de tours de piste'],
            total: 'longueur totale de la course',
            nbpart: 'nombre de tours de piste',
            tailepart: 'longueur de la piste',
            op: '*',
            contraintes: ['entier', 'positif'],
            descrip1: '£a a effectué une course de £b km en tournant autour d’une piste',
            descripEl: ['£a a effectué £b tours.', 'la longueur de la piste est de £b km.'],
            descripNbPart: 'On cherche le nombre de tours de piste.',
            descripPart: 'On cherche la longueur de la piste.',
            descripTout: 'On cherche la longueur totale de la course.'
          }
        ]
      },

      // Par défaut, pas de temps limite. Décommentez pour avoir un temps limite par défaut. En secondes.

      // limite: 10;
      /*
          Phrase d’état renvoyée par la section
          Si pe=0 alors la section est quantitative et renvoie un score entre 0 et 1.
          Dans le cas où la section est qualitative, lister les différents pe renvoyées :
              pe_1: "toto"
              pe_2: "tata"
              etc
          Dans la partie navigation, il faudra affecter la variable parcours.pe à l’une des précédentes.
          Par exemple,
          parcours.pe: donneesSection.pe_2
          */
      pe: 0
    }
  }
  function initSection () {
    let i, tu
    ds = getDonnees()
    me.donneesSection = ds

    me.surcharge()
    ds.nbitems = ds.nbetapes * ds.nbrepetitions
    // Construction de la page
    me.construitStructurePage(ds.structure)
    stor.listPren = ['Charlie', 'Félix', 'Llyn', 'Mathys', 'Nathéo', 'Daniel', 'Tommy', 'Rémi', 'Nicolas', 'Mohamed', 'Walid', 'Sofian', 'Safa']
    stor.ListPrenGarde = j3pShuffle(stor.listPren)
    stor.typosPos = []
    if (ds.Addition || ds.Soustraction) stor.typosPos.push('+')
    if (ds.Multiplication || ds.Division) stor.typosPos.push('*')
    if (stor.typosPos.length === 0) {
      j3pShowError('problème de paramétrage: Aucune opération choisie')
      stor.typosPos.push('+')
      ds.Addition = true
    }
    const tabtyplus = []
    if (ds.Addition) tabtyplus.push('+')
    if (ds.Soustraction) tabtyplus.push('-')
    const tabtypfois = []
    if (ds.Multiplication) tabtypfois.push('*')
    if (ds.Division) { tabtypfois.push('/1'); tabtypfois.push('/2') }
    stor.pbpos = j3pClone(ds.textes.pb)
    for (i = stor.pbpos.length - 1; i > -1; i--) {
      tu = stor.pbpos[i].op
      if (stor.typosPos.indexOf(tu) === -1) stor.pbpos.splice(i, 1)
    }

    stor.lesExos = []
    let lp = [1, 2]
    if (ds.Difficulte === '1') lp = [1]
    if (ds.Difficulte === '2') lp = [2]
    for (let i = 0; i < ds.nbitems; i++) {
      stor.lesExos.push({ niv: lp[i % lp.length] })
    }
    stor.lesExos = j3pShuffle(stor.lesExos)

    lp = j3pClone(j3pShuffle(stor.pbpos))
    let lp2 = j3pClone(j3pShuffle(stor.pbpos))
    for (let i = 0; i < ds.nbitems; i++) {
      if (lp.length === 0) lp = j3pClone(j3pShuffle(stor.pbpos))
      stor.lesExos[i].prob = lp.pop()
      if (stor.lesExos[i].prob.op === '+') {
        stor.lesExos[i].op = tabtyplus[j3pGetRandomInt(0, tabtyplus.length - 1)]
      } else {
        stor.lesExos[i].op = tabtypfois[j3pGetRandomInt(0, tabtypfois.length - 1)]
      }
      if (stor.lesExos[i].niv === '2') {
        if (lp2.length === 0) lp2 = j3pClone(j3pShuffle(stor.pbpos))
        stor.lesExos[i].prob2 = lp2.pop()
        if (stor.lesExos[i].prob2.op === '+') {
          stor.lesExos[i].op2 = tabtyplus[j3pGetRandomInt(0, tabtyplus.length - 1)]
        } else {
          stor.lesExos[i].op2 = tabtypfois[j3pGetRandomInt(0, tabtypfois.length - 1)]
        }
      }
    }

    ds.Change = ds.Change && ds.Question === 'Diagramme_tout'
    me.typederreurs = [0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0]
    me.afficheTitre('Problèmes et schéma')

    getMtgCore({ withMathJax: true })
      .then(
        // success
        (mtgAppLecteur) => {
          stor.mtgAppLecteur = mtgAppLecteur
          stor.listePourCalc = mtgAppLecteur.createList('TWF0aEdyYXBoSmF2YTEuMAAAABI+TMzNAAJmcv###wEA#wEAAAAAAAAAAAQsAAACvQAAAQEAAAAAAAAAAQAAAAP#####AAAAAQAKQ0NhbGNDb25zdAD#####AAJwaQAWMy4xNDE1OTI2NTM1ODk3OTMyMzg0Nv####8AAAABAApDQ29uc3RhbnRlQAkh+1RELRj#####AAAAAQAHQ0NhbGN1bAD#####AAFlAAZleHAoMSn#####AAAAAgAJQ0ZvbmN0aW9uBwAAAAE#8AAAAAAAAAAAAAIA#####wABeAABMAAAAAEAAAAAAAAAAP###############w==')
          enonceMain()
        },
        // failure
        (error) => {
          j3pShowError(error, { message: 'mathgraph n’est pas correctement chargé', mustNotify: true })
        })
      // plantage dans le code de success
      .catch(j3pShowError)
  }

  function creeLesDiv () {
    stor.lesdiv = {}
    stor.lesdiv.conteneur = j3pAddElt(me.zonesElts.MG, 'div', {
      style: me.styles.etendre('toutpetit.enonce', { padding: '10px' })
    })
    stor.lesdiv.zone1 = addDefaultTable(stor.lesdiv.conteneur, 1, 3)
    j3pAddContent(stor.lesdiv.conteneur, '&nbsp;')
    stor.lesdiv.etape = addDefaultTable(stor.lesdiv.conteneur, 1, 1)[0][0]
    stor.lesdiv.etape.style.color = me.styles.toutpetit.correction.color
    j3pAddContent(stor.lesdiv.conteneur, '&nbsp;')
    stor.lesdiv.zone2 = addDefaultTable(stor.lesdiv.conteneur, 1, 3)
    stor.lesdiv.travail = stor.lesdiv.zone2[0][0]
    stor.lesdiv.figco =
    stor.lesdiv.correction = stor.lesdiv.zone1[0][2]
    stor.lesdiv.zone1[0][1].style.width = '40px'
    const ay2 = addDefaultTable(stor.lesdiv.travail, 2, 1)
    const ay = addDefaultTable(ay2[0][0], 1, 2)
    const ay3 = addDefaultTable(ay2[1][0], 1, 2)
    stor.lesdiv.fig = ay[0][0]
    stor.lesdiv.calc = ay3[0][0]
    stor.lesdiv.figco = ay[0][1]
    stor.lesdiv.consigne = stor.lesdiv.zone1[0][0]
    stor.lesdiv.zoneChange = addDefaultTable(stor.lesdiv.conteneur, 1, 1)[0][0]
    stor.lesdiv.explications = j3pAddElt(stor.lesdiv.conteneur, 'div')
    stor.lesdiv.solution = j3pAddElt(stor.lesdiv.conteneur, 'div')
    stor.lesdiv.explications.style.color = me.styles.cfaux
    stor.lesdiv.solution.style.color = me.styles.petit.correction.color
    stor.lesdiv.travail.classList.add('travail')
    stor.lesdiv.consigne.classList.add('enonce')
    me.zonesElts.MG.classList.add('fond')
  }
  function newPren () {
    if (stor.ListPrenGarde.length === 0) {
      stor.ListPrenGarde = j3pShuffle(stor.listPren)
    }
    return stor.ListPrenGarde.pop()
  }
  function faisChoixExos () {
    const e = stor.lesExos.pop()
    stor.pren = newPren()
    stor.yakoi = []
    stor.yakoi[1] = []
    switch (e.prob.op) {
      case '+': {
        stor.valDecoupe = []
        let cmpt = 0
        for (let i = 0; i < e.prob.decoupes.length; i++) {
          stor.valDecoupe[i] = j3pGetRandomInt(1, 10)
          if (e.prob.contraintes.indexOf('négatif') !== -1) {
            if (j3pGetRandomBool()) stor.valDecoupe[i] = -stor.valDecoupe[i]
          }
          cmpt += stor.valDecoupe[i]
        }
        stor.total = cmpt
        stor.den = 1
        stor.liPourMoins = j3pGetRandomInt(1, e.prob.decoupes.length)
        if (e.prob.contraintes.indexOf('fraction') !== -1) {
          stor.den = j3pShuffle([3, 7, 11, 13, 17, 5]).pop()
        }
        if (e.prob.contraintes.indexOf('décimal') !== -1) {
          for (let i = 0; i < e.prob.decoupes.length; i++) {
            stor.valDecoupe[i] = j3pArrondi(stor.valDecoupe[i] / 10, 1)
          }
          stor.total = j3pArrondi(stor.total / 10, 1)
        }
        stor.nbCases1 = e.prob.decoupes.length
        for (let i = 1; i < 8; i++) stor.yakoi[1][i] = { nom: e.prob.decoupes[i - 1] || ' ', val: affMaVal(stor.valDecoupe[i - 1]) }
        stor.yakoi[1][100] = { nom: ' ', val: ' ' }
      }
        break
      default: {
        stor.nbpart = j3pGetRandomInt(8, 20)
        stor.taillePart = j3pGetRandomInt(3, 9)
        stor.total = stor.nbpart * stor.taillePart
        stor.den = 1
        if (e.prob.contraintes.indexOf('fraction') !== -1) {
          stor.den = j3pShuffle([3, 7, 11, 13, 17, 5]).pop()
        }
        if (e.prob.contraintes.indexOf('décimal') !== -1) {
          stor.taillePart = j3pArrondi(stor.taillePart / 10, 1)
          stor.total = j3pArrondi(stor.total / 10, 1)
        }
        if (e.prob.contraintes.indexOf('négatif') !== -1) {
          if (j3pGetRandomBool()) {
            stor.taillePart = -stor.taillePart
            if (stor.nbpart % 2 === 1) stor.total = -stor.total
          }
        }
        stor.nbCases1 = 3
        stor.rechDiv = (e.op === '/1') ? 'part' : 'nbpart'
        for (let i = 1; i < 8; i++) stor.yakoi[1][i] = { nom: ' ', val: ' ' }
        stor.yakoi[1][1] = { nom: e.prob.tailepart, val: affMaVal(stor.taillePart) }
        stor.yakoi[1][100] = { nom: e.prob.nbpart, val: affMaVal(stor.nbpart) }
      }
    }
    stor.yakoi[0] = [{ nom: e.prob.total, val: affMaVal(stor.total) }]
    stor.yakoi[2] = []
    for (let i = 0; i < 12; i++) stor.yakoi[2][i] = { nom: '', val: '' }
    stor.yakoiGarde = j3pClone(stor.yakoi)
    stor.disab = false
    stor.foco = false
    return e
  }
  function poseQuestion () {
    j3pEmpty(stor.lesdiv.explications)
    stor.lesdiv.explications.classList.remove('explique')
    if (me.etapeCourante === 1) {
      stor.enc = true
      stor.nbCases2 = 0
      stor.lesdiv.zoneBout = stor.lesdiv.zone2[0][2]
      j3pAddContent(stor.lesdiv.zone2[0][2], '&nbsp;')
      if (stor.lexo.prob.op === '+') {
        j3pAddContent(stor.lesdiv.consigne, '<u>On considère le problème suivant</u>:<br>')
        if (stor.lexo.op === '-') j3pAffiche(stor.lesdiv.consigne, null, stor.lexo.prob.descrip1 + '<br>', { a: stor.pren, b: affMaVal(stor.total) })
        for (let i = 0; i < stor.lexo.prob.decoupes.length; i++) {
          if (stor.liPourMoins - 1 === i && stor.lexo.op === '-') continue
          j3pAffiche(stor.lesdiv.consigne, null, stor.lexo.prob.descripEl[i] + '<br>', { a: stor.pren, b: affMaVal(stor.valDecoupe[i]) })
        }
        if (stor.lexo.op === '+') {
          stor.asouv = stor.yakoi[0][0].val
          stor.yakoi[0][0].val = ''
          stor.ablok = { ligne: 0, ca: 0 }
          j3pAffiche(stor.lesdiv.consigne, null, stor.lexo.prob.descripPLus + '<br>', { a: stor.pren })
          stor.nomblok = stor.lexo.prob.total
        } else {
          stor.asouv = stor.yakoi[1][stor.liPourMoins].val
          stor.ablok = { ligne: 1, ca: stor.liPourMoins }
          stor.yakoi[1][stor.liPourMoins].val = ''
          j3pAffiche(stor.lesdiv.consigne, null, stor.lexo.prob.descripMoins + stor.lexo.prob.decoupes[stor.liPourMoins - 1] + '<br>', { a: stor.pren })
          stor.nomblok = stor.lexo.prob.decoupes[stor.liPourMoins - 1]
        }
      } else {
        if (stor.lexo.op === '*') {
          stor.asouv = stor.yakoi[0][0].val
          j3pAffiche(stor.lesdiv.consigne, null, stor.lexo.prob.descripEl[0] + '<br>', { a: stor.pren, b: stor.nbpart })
          j3pAffiche(stor.lesdiv.consigne, null, stor.lexo.prob.descripEl[1] + '<br>', { a: stor.pren, b: stor.taillePart })
          j3pAffiche(stor.lesdiv.consigne, null, stor.lexo.prob.descripTout + '<br>', { a: stor.pren })
          stor.yakoi[0][0].val = ''
          stor.ablok = { ligne: 0, ca: 0 }
          stor.nomblok = stor.lexo.prob.total
        } else {
          j3pAffiche(stor.lesdiv.consigne, null, stor.lexo.prob.descrip1 + '<br>', { a: stor.pren, b: stor.total })
          if (stor.rechDiv === 'part') {
            stor.asouv = stor.yakoi[1][1].val
            j3pAffiche(stor.lesdiv.consigne, null, stor.lexo.prob.descripEl[0] + '<br>', { a: stor.pren, b: stor.nbpart })
            j3pAffiche(stor.lesdiv.consigne, null, stor.lexo.prob.descripPart + '<br>', { a: stor.pren })
            stor.yakoi[1][1].val = ''
            stor.ablok = { ligne: 1, ca: 1 }
            stor.nomblok = stor.lexo.prob.decoupes[0]
          } else {
            stor.asouv = stor.yakoi[1][100].val
            j3pAffiche(stor.lesdiv.consigne, null, stor.lexo.prob.descripEl[1] + '<br>', { a: stor.pren, b: stor.taillePart })
            j3pAffiche(stor.lesdiv.consigne, null, stor.lexo.prob.descripNbPart + '<br>', { a: stor.pren })
            stor.yakoi[1][100].val = ''
            stor.ablok = { ligne: 1, ca: 100 }
            stor.nomblok = stor.lexo.prob.decoupes[1]
          }
        }
      }
      j3pAddContent(stor.lesdiv.etape, '<u>Etape 1</u>: Faire un schéma.')
      switch (ds.Question) {
        case 'Diagramme_tout':
          if (stor.yakoi[1][100]) {
            stor.yakoi[1][100].val = ''
            stor.yakoi[1][100].nom = ''
          }
          stor.yakoi[0][0].val = ''
          stor.yakoi[0][0].nom = ''
          for (let i = 1; i < 8; i++) {
            stor.yakoi[1][i].val = ''
            stor.yakoi[1][i].nom = ''
          }
          break
        case 'Diagramme_nom':
          if (stor.yakoi[1][100]) {
            stor.yakoi[1][100].nom = ''
          }
          stor.yakoi[0][0].nom = ''
          for (let i = 1; i < 8; i++) {
            stor.yakoi[1][i].nom = ''
          }
          break
        case 'Diagramme_nombre':
          if (stor.yakoi[1][100]) {
            stor.yakoi[1][100].val = ''
          }
          stor.yakoi[0][0].val = ''
          for (let i = 1; i < 8; i++) {
            stor.yakoi[1][i].val = ''
          }
          break
      }
      if (ds.Change) {
        j3pAjouteBouton(stor.lesdiv.zoneChange, faisChange, { value: 'Changer de représentation' })
      }
    } else {
      stor.enc = false
      if (stor.foco) {
        stor.lesdiv.figco.style.display = ''
        stor.lesdiv.fig.style.display = 'none'
      }
      j3pEmpty(stor.lesdiv.etape)
      j3pAddContent(stor.lesdiv.etape, '<u>Etape 2</u>: Donne le calcul.')
      const yy = addDefaultTable(stor.lesdiv.travail, 2, 2)
      j3pAffiche(yy[1][0], '', stor.nomblok + ' $ = $&nbsp;')
      stor.laz = new ZoneStyleMathquill2(yy[1][1], {
        restric: '',
        limite: 10,
        limitenb: 1
      })
      function fffunc (x) {
        return () => {
          stor.laz.majaffiche(String(x))
          setTimeout(() => { stor.laz.focus() }, 1)
        }
      }
      let ch = [
        { s: '$+$', func: fffunc('+') },
        { s: '$-$', func: fffunc('-') },
        { s: '$\\times$', func: fffunc('\\times') },
        { s: '$\\div$', func: fffunc('\\div') }
      ]
      switch (stor.lexo.op) {
        case '+':
          for (let i = 0; i < stor.lexo.prob.decoupes.length; i++) {
            ch.push({ s: '$' + stor.valDecoupe[i] + '$', func: fffunc('\\text{' + stor.valDecoupe[i] + '}') })
          }
          break
        case '-':
          for (let i = 0; i < stor.lexo.prob.decoupes.length; i++) {
            if (i === stor.liPourMoins - 1) continue
            ch.push({ s: '$' + stor.valDecoupe[i] + '$', func: fffunc('\\text{' + stor.valDecoupe[i] + '}') })
          }
          ch.push({ s: '$' + stor.total + '$', func: fffunc('\\text{' + stor.total + '}') })
          break
        case '*':
          ch.push({ s: '$' + stor.nbpart + '$', func: fffunc(stor.nbpart) })
          ch.push({ s: '$' + stor.taillePart + '$', func: fffunc(stor.taillePart) })
          break
        case '/1':
          ch.push({ s: '$' + stor.nbpart + '$', func: fffunc(stor.nbpart) })
          ch.push({ s: '$' + stor.total + '$', func: fffunc(stor.total) })
          break
        case '/2':
          ch.push({ s: '$' + stor.taillePart + '$', func: fffunc(stor.taillePart) })
          ch.push({ s: '$' + stor.total + '$', func: fffunc(stor.total) })
          break
      }
      ch = j3pShuffle(ch)
      const jj = []
      const kk = []
      for (let i = 0; i < ch.length; i++) {
        jj.push(ch[i].s)
        kk.push(ch[i].func)
      }
      stor.boty = new BoutonsStyleMatquill(yy[0][1], 'hjkfk', jj, kk, { mathquill: true })
      stor.aclear = yy[0][1]
    }
  }
  function faisChange () {
    stor.repFois = !stor.repFois
    afficheNcase(svgId, 1, '', stor.nbCases, stor.repFois)
  }
  function makefig (svgId, fig, co) {
    const txtFigure = 'TWF0aEdyYXBoSmF2YTEuMAAAABM+TMzNAAJmcv###wEA#wEAAAAAAAAAAAM4AAACbAAAAQEAAAAEAAAABgADY29tAAAAAAABAAAAAQAAAAAC#####wAAAAEAEUNFbGVtZW50R2VuZXJpcXVlAAAAAAAA##########8AAAACAAxDQ29tbWVudGFpcmUB#####wAAAP8BAAAAAAAAEAAAAAAAAQAAAAH#####AAAAAQAKQ0NvbnN0YW50ZQAAAAAAAAAAAAFBAAAABgACaW0AAAAAAAEAAAABAAAAAAIAAAAAAAAAAAAB##########8AAAACAAZDSW1hZ2UB#####wD###8BAAAAAAAADQAAAAAAAQAAAAEAAAACAAAAAAAAAAAAAAAAMgAAADIAAAamiVBORw0KGgoAAAANSUhEUgAAADIAAAAyCAIAAACRXR#mAAAAAXNSR0IArs4c6QAAAARnQU1BAACxjwv8YQUAAAAJcEhZcwAADsMAAA7DAcdvqGQAAAY7SURBVFhHxZlbSFVZHMaP3VORbkMYNYNZc6QMFJEeykKKUZCZ6NRLvvhSINgU49iDOjKHLpOlUEORRKQvYyCCPRr02ENEN9CSCrLLgwRdmC6Seszmt#Z3XJ6Oes7e56DzPWz++7++tda3v#Xf+6y9j+#rrGDMAcEXBwTXr1+vqKjIz88vLCysrKy8desWydHRUdFmQ1aUJuKDBw#OnTvXF4GFCxcePXoUTigUghNLloaLjTB1elgak8mMffv2ScqSJUvKyspKSkoWL16sTHV1NUyUJegWc3C0U04HS0AQICgvL5eCLVu2PHv2zGF97enpyc3NVb6+vp6MkRUaHdM0Jg6FPn369PHjR45TgqaRkRGYsQUBq8n6JE0sX3Fx8dDQEE0MpdEGBgY2btwoZdeuXZtwq6urKxAI+P3+76fHDw42bNhw48YNpuEaNPFkGEXjPumarU8pKSl5eXkvXrwgyQgcpez58+crVqyAgGjfv4Ojg4ODe#bsUR+XOHHiBAMNDw9PKctqivIJcG0KsrOz0QFH18ZQxNwKNC1dutQ3+mVs7969otJn##79DQ0Nf06DYDBI65kzZz58+MBYdukjYRSN+wQIrKatW7e+e#eOEXS6bt06lRfKkAX5+PHj5Lkrfe3t7SLt2rXr7du3ZmAvkAILq2myT9u3b+diRKOulbTKVGpwWGKMNHcEET5RzjSgGu2qxOkg2y3MPA7sqWPTNz5t27aNe4UMfQFBpLL+#n4yJ0+eVKampsa3bNkyosOHD9OgWrEg4x62y5Q+SZO0QuDaCOrq6kTYtGmTjbHq9evX5nblhBJmINj00eieYBRN41OUJtGsstra2nnz5okJMjMz+#r6yPuUPXbsGCdQ1c0THEmufBITiKy7b+fOnbIGTb29vWRY5WRl2WmMS#F8EhTLrdOnTy9atAgymh48eEBG+bAs7kyl1M0lnFnCl87cxJ40NTc3i2x9Uh5C4rLMJA7ZuOTdp6amJpGjfBIzQVnqTJC8T5M1ESQiy3Y2LiXnU9TaiQk8y7KdJ#tkn5kxNMWoJzEFb7JsZ+PStz650RS7nggsPMiynWeoniLhTRZH45J3Te59EtzKUh6fAHHCPsWop0h4cEtrB8dqYv#kySeXmoAHtzRcY2Mj#Dlz5rAjIiOtIgiRZE#1FAlXskg6q2eWr6CgAE30ysrKunfvHq3qJYisub3WUyTcuiVL2H3rl1Vgr3bnzh3ymg#QXbH1adWqVe7XzsKtW2rSDhtlBw4cYE9LvHz5cqtMNOJkfBJcyWL5tNOtqKiAzA6b+OLFi87UE8o+f#7M0Ws9KWlbFcSXRUZVxVsA+27IvK2ToamlpcURYJTdvn2bzKlTp5Rxr0lwtvjht06OrtxSYWEJxQ65ra0Nml5Jzp07p9WkhnghMIq811Nkq2JXbikvJ+bPn##o0aNwmwN2mJIreKonmmT8lStX2CFTBuLHkSWS3CotLYVZVFREzFj4ceHChUAgkJOTY78KocmTTyqPgYEBrpbuHR0dJOkb3y1dzZs3b#TqVlJScuTIkby8vAULFhgh40hPT9+xY8fDhw8h6#5wJMXSBHTBT58+TUtL49ouXbpEF1494rulSrx69SorFSWFgXJzcysrKzs7O1++fKkukQ99ZWJAbvFinZqayoCtra0k47vFqV6bgsGgo8SAit69e#fZs2fv378vYwTmkLVGkQtNQPxEZHH1HOnJL3R1dXV3d3fUpwpzZztfFo2WcYTb4iFBWcLkJFqRIsWRCDe7RuJuKUN#uQISM2ZKJO6WnXtykDySWsSZQxxZyXwaSQZ6mvT39+sb+ISslSszJQtB#5dbyIp0yzxOi3#6mXN93+Jc5ex0mXEwkaazbl2+fJlT41ZVsJXzQ4cOwUMW2VkDM3Jfs453797VNoTfRPJGFm1rs9evWbPm#fv3UqanwCyA6TkyaVVVFZr4tX7y5AmnCDWyfm#qJMsG4dWrV5zOMs6fP6#brqysjFM9osP#Yvz6x980sC1hW1xbW1s3w2CK+vp6Kmfz5s3MCzIyMh4#foygsKyhULjA8SxrrdkTzz78fv#NmzfRoOcFmPjPR#jtr39+CZT#6M#5buaxevXqwsJCnk367beaxsbG#gMW+o#eOeGtdgAAAABJRU5ErkJgggAAAAYABGFjZXIAAAAAAAEAAAABAAAAAAIAAAAAAAAAAAAB##########8AAAACAAlDQ2VyY2xlT1IB#####wAAAAAAAAABAAAAAAAAAAI#8AAAAAAAAAAAAAAGAAR0ZXh0AAAAAAACAAAAAgAAAAAQAAAAAAAAAAAAAP####8AAAAAAAAAAAAA##########8AAAABAAdDTWlsaWV1AP####8BAAAAABAAAAAAAAAAAAAAAEAIAAAAAAAAAAAFAAAAAAAAAAAB#####wAAAAEAFENEcm9pdGVEaXJlY3Rpb25GaXhlAP####8BAAAAARAAAAEAAAABAAAAAgE#8AAAAAAAAAAAAAYA#####wEAAAABEAAAAQAAAAEAAAAAAD#wAAAAAAAAAAAABgD#####AQAAAAEQAAABAAAAAQAAAAEAP#AAAAAAAAD#####AAAAAQAQQ0ludERyb2l0ZURyb2l0ZQD#####AQAAAAAQAAAAAAAAAAAAAABACAAAAAAAAAAABQAAAAADAAAABAAAAAcA#####wEAAAAAEAAAAAAAAAAAAAAAQAgAAAAAAAAAAAUAAAAAAwAAAAUAAAAEAP####8BAAAAAAAAAQAAAAYAAAACP8mZmZmZmZoAAAAABAD#####AQAAAAAAAAEAAAAHAAAAAj#JmZmZmZmaAP####8AAAABABBDSW50RHJvaXRlQ2VyY2xlAP####8AAAADAAAACP####8AAAABABBDUG9pbnRMaWVCaXBvaW50AP####8BAAAAABAAAAAAAAAAAAAAAEAIAAAAAAAAAAAFAAEAAAAKAAAACAD#####AAAAAwAAAAkAAAAJAP####8BAAAAABAAAAAAAAAAAAAAAEAIAAAAAAAAAAAFAAIAAAAMAAAAAQH#####AAAAfwEAAAAAAAsQAAAAAAAAAAAAAQAAAAIAAAAAAAAAAAADYWFhAAAAAQH#####AAB#AAEAAAAAAA0QAAAAAAACAAAAAQAAAAIAAAAAAAAAAAAEYmJiYgAAAAEAAALJ#####wAAAAEACkNDYWxjQ29uc3QA#####wACcGkAFjMuMTQxNTkyNjUzNTg5NzkzMjM4NDYAAAACQAkh+1RELRj#####AAAAAQAKQ1BvaW50QmFzZQD#####AX9#fwAQAAAAAAAAAAAAAABACAAAAAAAAAAEcDFjbwUAAUAUAAAAAAAIQDHhR64UeuIAAAALAP####8Bf39#ABAAAAAAAAAAAAAAAEAIAAAAAAAAAARwMmNvBQABQBwAAAAAAAhAeD4UeuFHrgAAAAsA#####wF#f38AEAAAAAAAAAAAAAAAQAgAAAAAAAAABHAzY28FAAFAiMAAAAAAAEB6rhR64UeuAAAACwD#####AX9#fwAQAAAAAAAAAAAAAABACAAAAAAAAAAEcDRjbwUAAUCICAAAAAAAQD3hR64UeuL#####AAAAAQAJQ1BvbHlnb25lAP####8Bf39#AAABAgAAAAUAAAABAAAABAAAAAMAAAACAAAAAf####8AAAABABBDU3VyZmFjZVBvbHlnb25lAP####8BAP8AAAZzdXJmY28AAAAFAAAABQAAAAsA#####wEAAP8AEAAAAAAAAAAAAAAAQAgAAAAAAAAAAm4xCAABQEZAAAAAAABATD1wo9cKPAAAAAYA#####wEAAP8BEAAAAQAAAAEAAAAHAD#wAAAAAAAA#####wAAAAEAD0NQb2ludExpZURyb2l0ZQD#####AQAA#wAQAAAAAAAAAAAAAABACAAAAAAAAAAACAABQE0AAAAAAAAAAAAI#####wAAAAEAEUNTeW1ldHJpZUNlbnRyYWxlAP####8AAAAJ#####wAAAAEAC0NQb2ludEltYWdlAP####8BAAD#ABAAAAAAAAAAAAAAAEAIAAAAAAAAAAAIAAAAAAcAAAAKAAAABgD#####AQAA#wEQAAABAAAAAQAAAAsBP#AAAAAAAAAAAAALAP####8B####ABAAAAAAAAAAAAAAAEAIAAAAAAAAAAhkaXJpZ2UxNAcAAUB7SAAAAAAAQFqeuFHrhR4AAAAGAP####8BAAAAARAAAAEAAAABAAAADQA#8AAAAAAAAAAAAAcA#####wH#AAAAEAAAAAAAAAAAAAAAQAgAAAAAAAAAAAgAAAAADAAAAA4AAAALAP####8BAAAAABAAAUEAAAAAAAAAAABACAAAAAAAAAAABQABQGxQAAAAAABAeHeuFHrhSAAAAAsA#####wEAAAAAEAABQgAAAAAAAAAAAEAIAAAAAAAAAAAFAAFAb3AAAAAAAEB4d64UeuFI#####wAAAAEACUNMb25ndWV1cgD#####AAAAEAAAABEAAAAEAP####8BAAAAAAAAAQAAAAsAAAACP#AAAAAAAAAAAAAABgD#####AQAAAAEQAAABAAAAAQAAAAsBP#AAAAAAAAAAAAAIAP####8AAAAUAAAAEwAAAAkA#####wEAAAAAEAAAAAAAAAAAAAAAQAgAAAAAAAAAAAUAAQAAABUAAAAEAP####8BAAAAAAAAAQAAABYAAAACP#AAAAAAAAAAAAAABgD#####AQAAAAEQAAABAAAAAQAAABYAP#AAAAAAAAAAAAAIAP####8AAAAYAAAAFwAAAAkA#####wEAAAAAEAAAAAAAAAAAAAAAQAgAAAAAAAAAAAUAAQAAABkAAAAEAP####8BAAAAAAABAgAAAA8AAAACP#AAAAAAAAAAAAAACAD#####AAAADAAAABsAAAAJAP####8BAAAAABAAAAAAAAAAAAAAAEAIAAAAAAAAAAAFAAIAAAAcAAAABAD#####AQAAAAAAAQIAAAAdAAAAAj#wAAAAAAAAAAAAAAYA#####wEAAAABEAAAAQAAAQIAAAAdAD#wAAAAAAAAAAAACAD#####AAAAHwAAAB4AAAAJAP####8BAAAAABAAAAAAAAAAAAAAAEAIAAAAAAAAAAAFAAEAAAAgAAAABQD#####AQAAAAAQAAAAAAAAAAAAAABACAAAAAAAAAAABQAAAAAaAAAAIQAAAAQA#####wEAAAAAAAECAAAAIgAAAAI#8AAAAAAAAAAAAAAGAP####8BAAAAARAAAAEAAAECAAAAIgA#8AAAAAAAAAAAAAgA#####wAAACQAAAAjAAAACQD#####AQAAAAAQAAAAAAAAAAAAAABACAAAAAAAAAAABQABAAAAJQAAAAYA#####wEAfwABEAAAAQAAAQIAAAAmAT#wAAAAAAAAAAAABwD#####AQB#AAAQAAAAAAAAAAAAAABACAAAAAAAAAAABQAAAAAYAAAAJwAAAAcA#####wEAfwAAEAAAAAAAAAAAAAAAQAgAAAAAAAAAAAUAAAAAHwAAACcAAAAEAP####8BAH8AAAABAgAAACgAAAACP+zMzMzMzM0AAAAABAD#####AQB#AAAAAQIAAAApAAAAAj#szMzMzMzNAAAAAAgA#####wAAABgAAAAqAAAACQD#####AQB#AAAQAAAAAAAAAAAAAABACAAAAAAAAAAABQACAAAALAAAAAkA#####wEAfwAAEAAAAAAAAAAAAAAAQAgAAAAAAAAAAAUAAQAAACwAAAAIAP####8AAAAfAAAAKwAAAAkA#####wEAfwAAEAAAAAAAAAAAAAAAQAgAAAAAAAAAAAUAAgAAAC8AAAAJAP####8BAH8AABAAAAAAAAAAAAAAAEAIAAAAAAAAAAAFAAEAAAAvAAAADAD#####Af###wAKcG9seU5iUGFydAECAAAABQAAAC0AAAAuAAAAMQAAADAAAAAtAAAADQD#####AX9#fwAKc3VyZk5iUGFydAAAAAUAAAAyAAAAAwD#####AX9#fwEACGltTnBQYXJ0AAAAJg0AAAAAAAEAAAABAAAAAgAAAAAAAAAAAAAAADIAAAAyAAAGpolQTkcNChoKAAAADUlIRFIAAAAyAAAAMggCAAAAkV0f5gAAAAFzUkdCAK7OHOkAAAAEZ0FNQQAAsY8L#GEFAAAACXBIWXMAAA7DAAAOwwHHb6hkAAAGO0lEQVRYR8WZW0hVWRzGj91TkW5DGDWDWXOkDBSRHspCilGQmejUS774UiDYFOPYgzoyhy6TpVBDkUSkL2Mggj0a9NhDRDfQkgqyy4MEXZguknrM5rf2d1yejnrO3ueg8z1s#vu#vrXWt7#13#usvY#v66xgzAHBFwcE169fr6ioyM#PLywsrKysvHXrFsnR0VHRZkNWlCbigwcPzp071xeBhQsXHj16FE4oFIITS5aGi40wdXpYGpPJjH379knKkiVLysrKSkpKFi9erEx1dTVMlCXoFnNwtFNOB0tAECAoLy+Xgi1btjx79sxhfe3p6cnNzVW+vr6ejJEVGh3TNCYOhT59+vTx40eOU4KmkZERmLEFAavJ+iRNLF9xcfHQ0BBNDKXRBgYGNm7cKGXXrl2bcKurqysQCPj9#u+nxw8ONmzYcOPGDabhGjTxZBhF4z7pmq1PKSkpeXl5L168IMkIHKXs+fPnK1asgIBo37+Do4ODg3v27FEflzhx4gQDDQ8PTynLaoryCXBtCrKzs9EBR9fGUMTcCjQtXbrUN#plbO#evaLSZ##+#Q0NDX9Og2AwSOuZM2c+fPjAWHbpI2EUjfsECKymrVu3vnv3jhF0um7dOpUXypAF+fjx4+S5K33t7e0i7dq16+3bt2ZgL5ACC6tpsk#bt2#nYkSjrpW0ylRqcFhijDR3BBE+Uc40oBrtqsTpINstzDwO7Klj0zc+bdu2jXuFDH0BQaSy#v5+MidPnlSmpqbGt2zZMqLDhw#ToFqxIOMetsuUPkmTtELg2gjq6upE2LRpk42x6vXr1+Z25YQSZiDY9NHonmAUTeNTlCbRrLLa2tp58+aJCTIzM#v6+sj7lD127BgnUNXNExxJrnwSE4isu2#nzp2yBk29vb1kWOVkZdlpjEvxfBIUy63Tp08vWrQIMpoePHhARvmwLO5MpdTNJZxZwpfO3MSeNDU3N4tsfVIeQuKyzCQO2bjk3aempiaRo3wSM0FZ6kyQvE+TNREkIst2Ni4l51PU2okJPMuynSf7ZJ+ZMTTFqCcxBW+ybGfj0rc+udEUu54ILDzIsp1nqJ4i4U0WR+OSd03ufRLcylIenwBxwj7FqKdIeHBLawfHamL#5Mknl5qAB7c0XGNjI#w5c+awIyIjrSIIkWRP9RQJV7JIOqtnlq+goABN9MrKyrp37x6t6iWIrLm91lMk3LolS9h965dVYK92584d8poP0F2x9WnVqlXu187CrVtq0g4bZQcOHGBPS7x8+XKrTDTiZHwSXMli+bTTraiogMwOm#jixYvO1BPKPn#+zNFrPSlpWxXEl0VGVcVbAPtuyLytk6GppaXFEWCU3b59m8ypU6eUca9JcLb44bdOjq7cUmFhCcUOua2tDZpeSc6dO6fVpIZ4ITCKvNdTZKtiV24pLyfmz5##6NGjcJsDdpiSK3iqJ5pk#JUrV9ghUwbix5ElktwqLS2FWVRURMxY+HHhwoVAIJCTk2O#CqHJk08qj4GBAa6W7h0dHSTpG98tXc2bN2#06lZSUnLkyJG8vLwFCxYYIeNIT0#fsWPHw4cPIev+cCTF0gR0wU+fPk1LS+PaLl26RBdePeK7pUq8evUqKxUlhYFyc3MrKys7OztfvnypLpEPfWViQG7xYp2amsqAra2tJOO7xalem4LBoKPEgIrevXv32bNn79+#L2ME5pC1RpELTUD8RGRx9RzpyS90dXV1d3d31KcKc2c7XxaNlnGE2+IhQVnC5CRakSLFkQg3u0bibilDf7kCEjNmSiTulp17cpA8klrEmUMcWcl8GkkGepr09#frG#iErJUrMyULQf+XW8iKdMs8Tot#+plzfd#iXOXsdJlxMJGms25dvnyZU+NWVbCV80OHDsFDFtlZAzNyX7OOd+#e1TaE30TyRhZta7PXr1mz5v3791Kmp8AsgOk5MmlVVRWa+LV+8uQJpwg1sn5v6iTLBuHVq1eczjLOnz+v266srIxTPaLD#2L8+sffNLAtYVtcW1tbN8Ngivr6eipn8+bNzAsyMjIeP36MoLCsoVC4wPEsa63ZE88+#H7#zZs30aDnBZj4z0f47a9#fgmU#+jP+W7msXr16sLCQp5N+u23msbGxv4DFvqP3jnhrXYAAAAASUVORK5CYIIAAAAGAP####8BAAD#ARAAAAEAAAABAAAACQE#8AAAAAAAAAAAAAYA#####wEAAP8BEAAAAQAAAAEAAAAHAT#wAAAAAAAAAAAACwD#####Af###wAQAAAAAAAAAAAAAABACAAAAAAAAAAGZGlyaWdlBwABQIk8AAAAAABAOHrhR64UeAAAAAYA#####wEAAP8BEAAAAQAAAAEAAAA3AD#wAAAAAAAAAAAABwD#####Af8AAAAQAAAAAAAAAAAAAABACAAAAAAAAAAACAAAAAA2AAAAOAAAAAcA#####wH#AAAAEAAAAAAAAAAAAAAAQAgAAAAAAAAAAAgAAAAANQAAADgAAAAMAP####8AAAAAAAVwb2x5MAACAAAABQAAAAcAAAA5AAAAOgAAAAkAAAAHAAAADQD#####AX9#fwAFc3VyZjAAAAAFAAAAOwAAAAsA#####wH###8AEAAAAAAAAAAAAAAAQAgAAAAAAAAACGRpcmlnZTExBwABQFBgAAAAAABAWF64UeuFHgAAAAYA#####wEAAAABEAAAAQAAAAEAAAA9AD#wAAAAAAAAAAAABwD#####Af8AAAAQAAAAAAAAAAAAAABACAAAAAAAAAAACAAAAAA1AAAAPgAAAAcA#####wH#AAAAEAAAAAAAAAAAAAAAQAgAAAAAAAAAAAgAAAAADAAAAD4AAAAMAP####8AAAAAAAZwb2x5MTEAAgAAAAUAAAAJAAAAPwAAAEAAAAALAAAACQAAAA0A#####wF#f38ABnN1cmYxMQAAAAUAAABBAAAACwD#####Af###wAQAAAAAAAAAAAAAABACAAAAAAAAAAIZGlyaWdlMTIHAAFAZdAAAAAAAEBYHrhR64UeAAAABgD#####AQAAAAEQAAABAAAAAQAAAEMAP#AAAAAAAAAAAAAHAP####8B#wAAABAAAAAAAAAAAAAAAEAIAAAAAAAAAAAIAAAAADUAAABEAAAABwD#####Af8AAAAQAAAAAAAAAAAAAABACAAAAAAAAAAACAAAAAAMAAAARAAAAAwA#####wAAAAAABnBvbHkxMgACAAAABQAAAD8AAABFAAAARgAAAEAAAAA#AAAADQD#####AX9#fwAGc3VyZjEyAAAABQAAAEcAAAALAP####8B####ABAAAAAAAAAAAAAAAEAIAAAAAAAAAAhkaXJpZ2UxMwcAAUByaAAAAAAAQFreuFHrhR4AAAAGAP####8BAAAAARAAAAEAAAABAAAASQA#8AAAAAAAAAAAAAcA#####wH#AAAAEAAAAAAAAAAAAAAAQAgAAAAAAAAAAAgAAAAANQAAAEoAAAAHAP####8B#wAAABAAAAAAAAAAAAAAAEAIAAAAAAAAAAAIAAAAAAwAAABKAAAADAD#####AAAAAAAGcG9seTEzAAIAAAAFAAAARQAAAEsAAABMAAAARgAAAEUAAAANAP####8Bf39#AAZzdXJmMTMAAAAFAAAATQAAAAcA#####wH#AAAAEAAAAAAAAAAAAAAAQAgAAAAAAAAAAAgAAAAANQAAAA4AAAAMAP####8AAAAAAAZwb2x5MTQAAgAAAAUAAABLAAAATwAAAA8AAABMAAAASwAAAA0A#####wF#f38ABnN1cmYxNAAAAAUAAABQAAAACwD#####Af###wAQAAAAAAAAAAAAAABACAAAAAAAAAAIZGlyaWdlMTUHAAFAgcQAAAAAAEBXnrhR64UeAAAABgD#####AQAAAAEQAAABAAAAAQAAAFIAP#AAAAAAAAAAAAAHAP####8B#wAAABAAAAAAAAAAAAAAAEAIAAAAAAAAAAAIAAAAADUAAABTAAAABwD#####Af8AAAAQAAAAAAAAAAAAAABACAAAAAAAAAAACAAAAAAMAAAAUwAAAAwA#####wAAAAAABnBvbHkxNQACAAAABQAAAE8AAABUAAAAVQAAAA8AAABPAAAADQD#####AX9#fwAGc3VyZjE1AAAABQAAAFYAAAALAP####8B####ABAAAAAAAAAAAAAAAEAIAAAAAAAAAAhkaXJpZ2UxNgcAAUCEKAAAAAAAQFq4UeuFHrgAAAAGAP####8BAAAAARAAAAEAAAABAAAAWAA#8AAAAAAAAAAAAAcA#####wH#AAAAEAAAAAAAAAAAAAAAQAgAAAAAAAAAAAgAAAAANQAAAFkAAAAHAP####8B#wAAABAAAAAAAAAAAAAAAEAIAAAAAAAAAAAIAAAAAAwAAABZAAAADAD#####AAAAAAAGcG9seTE2AAIAAAAFAAAAVAAAAFoAAABbAAAAVQAAAFQAAAANAP####8Bf39#AAZzdXJmMTYAAAAFAAAAXAAAAAsA#####wH###8AEAAAAAAAAAAAAAAAQAgAAAAAAAAACGRpcmlnZTE3BwABQIgkAAAAAABAWh64UeuFHgAAAAYA#####wEAAAABEAAAAQAAAAEAAABeAD#wAAAAAAAAAAAABwD#####Af8AAAAQAAAAAAAAAAAAAABACAAAAAAAAAAACAAAAAA1AAAAXwAAAAcA#####wH#AAAAEAAAAAAAAAAAAAAAQAgAAAAAAAAAAAgAAAAADAAAAF8AAAAMAP####8AAAAAAAZwb2x5MTcAAgAAAAUAAABaAAAAYAAAAGEAAABbAAAAWgAAAA0A#####wF#f38ABnN1cmYxNwAAAAUAAABiAAAADwD#####AAAACwAAABAA#####wEAAP8AEAAAAAAAAAAAAAAAQAgAAAAAAAAAAAgAAAAACQAAAGQAAAAGAP####8BAAD#ARAAAAEAAAABAAAAZQE#8AAAAAAAAAAAAAsA#####wH###8AEAAAAAAAAAAAAAAAQAgAAAAAAAAACGRpcmlnZTIxBwABQFkgAAAAAABAZC9cKPXCjwAAAAYA#####wEAAAABEAAAAQAAAAEAAABnAD#wAAAAAAAAAAAABwD#####Af8AAAAQAAAAAAAAAAAAAABACAAAAAAAAAAACAAAAAAMAAAAaAAAAAcA#####wH#AAAAEAAAAAAAAAAAAAAAQAgAAAAAAAAAAAgAAAAAZgAAAGgAAAAMAP####8AAAAAAAZwb2x5MjEAAgAAAAUAAAALAAAAaQAAAGoAAABlAAAACwAAAA0A#####wF#f38ABnN1cmYyMQAAAAUAAABrAAAACwD#####Af###wAQAAAAAAAAAAAAAABACAAAAAAAAAAIZGlyaWdlMjIHAAFAYvAAAAAAAEBkj1wo9cKPAAAABgD#####AQAAAAEQAAABAAAAAQAAAG0AP#AAAAAAAAAAAAAHAP####8B#wAAABAAAAAAAAAAAAAAAEAIAAAAAAAAAAAIAAAAAAwAAABuAAAABwD#####Af8AAAAQAAAAAAAAAAAAAABACAAAAAAAAAAACAAAAABmAAAAbgAAAAwA#####wAAAAAABnBvbHkyMgACAAAABQAAAGkAAABvAAAAcAAAAGoAAABpAAAADQD#####AX9#fwAGc3VyZjIyAAAABQAAAHEAAAALAP####8B####ABAAAAAAAAAAAAAAAEAIAAAAAAAAAAhkaXJpZ2UyMwcAAUBokAAAAAAAQGRvXCj1wo8AAAAGAP####8BAAAAARAAAAEAAAABAAAAcwA#8AAAAAAAAAAAAAcA#####wH#AAAAEAAAAAAAAAAAAAAAQAgAAAAAAAAAAAgAAAAADAAAAHQAAAAHAP####8B#wAAABAAAAAAAAAAAAAAAEAIAAAAAAAAAAAIAAAAAGYAAAB0AAAADAD#####AAAAAAAGcG9seTIzAAIAAAAFAAAAbwAAAHUAAAB2AAAAcAAAAG8AAAANAP####8Bf39#AAZzdXJmMjMAAAAFAAAAdwAAAAsA#####wH###8AEAAAAAAAAAAAAAAAQAgAAAAAAAAACGRpcmlnZTI0BwABQHAIAAAAAABAZG9cKPXCjwAAAAYA#####wEAAAABEAAAAQAAAAEAAAB5AD#wAAAAAAAAAAAABwD#####Af8AAAAQAAAAAAAAAAAAAABACAAAAAAAAAAACAAAAAAMAAAAegAAAAcA#####wH#AAAAEAAAAAAAAAAAAAAAQAgAAAAAAAAAAAgAAAAAZgAAAHoAAAAMAP####8AAAAAAAZwb2x5MjQAAgAAAAUAAAB1AAAAewAAAHwAAAB2AAAAdQAAAA0A#####wF#f38ABnN1cmYyNAAAAAUAAAB9AAAACwD#####Af###wAQAAAAAAAAAAAAAABACAAAAAAAAAAIZGlyaWdlMjUHAAFAc1gAAAAAAEBkr1wo9cKPAAAABgD#####AQAAAAEQAAABAAAAAQAAAH8AP#AAAAAAAAAAAAAHAP####8B#wAAABAAAAAAAAAAAAAAAEAIAAAAAAAAAAAIAAAAAAwAAACAAAAABwD#####Af8AAAAQAAAAAAAAAAAAAABACAAAAAAAAAAACAAAAABmAAAAgAAAAAwA#####wAAAAAABnBvbHkyNQACAAAABQAAAHsAAACBAAAAggAAAHwAAAB7AAAADQD#####AX9#fwAGc3VyZjI1AAAABQAAAIMAAAALAP####8B####ABAAAAAAAAAAAAAAAEAIAAAAAAAAAAhkaXJpZ2UyNgcAAUB2SAAAAAAAQGSPXCj1wo8AAAAGAP####8BAAAAARAAAAEAAAABAAAAhQA#8AAAAAAAAAAAAAcA#####wH#AAAAEAAAAAAAAAAAAAAAQAgAAAAAAAAAAAgAAAAADAAAAIYAAAAHAP####8B#wAAABAAAAAAAAAAAAAAAEAIAAAAAAAAAAAIAAAAAIYAAABmAAAADAD#####AAAAAAAGcG9seTI2AAIAAAAFAAAAgQAAAIcAAACIAAAAggAAAIEAAAANAP####8Bf39#AAZzdXJmMjYAAAAFAAAAiQAAAAsA#####wH###8AEAAAAAAAAAAAAAAAQAgAAAAAAAAACGRpcmlnZTI3BwABQHkIAAAAAABAZO9cKPXCjwAAAAYA#####wEAAAABEAAAAQAAAAEAAACLAD#wAAAAAAAAAAAABwD#####Af8AAAAQAAAAAAAAAAAAAABACAAAAAAAAAAACAAAAAAMAAAAjAAAAAcA#####wH#AAAAEAAAAAAAAAAAAAAAQAgAAAAAAAAAAAgAAAAAZgAAAIwAAAAMAP####8AAAAAAAZwb2x5MjcAAgAAAAUAAACHAAAAjQAAAI4AAACIAAAAhwAAAA0A#####wF#f38ABnN1cmYyNwAAAAUAAACPAAAACwD#####Af###wAQAAAAAAAAAAAAAABACAAAAAAAAAAIZGlyaWdlMjgHAAFAfSgAAAAAAEBkz1wo9cKPAAAABgD#####AQAAAAEQAAABAAAAAQAAAJEAP#AAAAAAAAAAAAAHAP####8B#wAAABAAAAAAAAAAAAAAAEAIAAAAAAAAAAAIAAAAAAwAAACSAAAABwD#####Af8AAAAQAAAAAAAAAAAAAABACAAAAAAAAAAACAAAAABmAAAAkgAAAAwA#####wAAAAAABnBvbHkyOAACAAAABQAAAI0AAACTAAAAlAAAAI4AAACNAAAADQD#####AX9#fwAGc3VyZjI4AAAABQAAAJUAAAALAP####8B####ABAAAAAAAAAAAAAAAEAIAAAAAAAAAAhkaXJpZ2UyOQcAAUCAxAAAAAAAQGKPXCj1wo8AAAAGAP####8BAAAAARAAAAEAAAABAAAAlwA#8AAAAAAAAAAAAAcA#####wH#AAAAEAAAAAAAAAAAAAAAQAgAAAAAAAAAAAgAAAAADAAAAJgAAAAHAP####8B#wAAABAAAAAAAAAAAAAAAEAIAAAAAAAAAAAIAAAAAGYAAACYAAAADAD#####AAAAAAAGcG9seTI5AAIAAAAFAAAAkwAAAJkAAACaAAAAlAAAAJMAAAANAP####8Bf39#AAZzdXJmMjkAAAAFAAAAmwAAAAsA#####wH###8AEAAAAAAAAAAAAAAAQAgAAAAAAAAACWRpcmlnZTIxMAcAAUCDBAAAAAAAQGQPXCj1wo8AAAAGAP####8BAAAAARAAAAEAAAABAAAAnQA#8AAAAAAAAAAAAAcA#####wH#AAAAEAAAAAAAAAAAAAAAQAgAAAAAAAAAAAgAAAAADAAAAJ4AAAAHAP####8B#wAAABAAAAAAAAAAAAAAAEAIAAAAAAAAAAAIAAAAAGYAAACeAAAADAD#####AAAAAAAHcG9seTIxMAACAAAABQAAAJkAAACfAAAAoAAAAJoAAACZAAAADQD#####AX9#fwAHc3VyZjIxMAAAAAUAAAChAAAACwD#####Af###wAQAAAAAAAAAAAAAABACAAAAAAAAAAJZGlyaWdlMjExBwABQIS8AAAAAABAZQ9cKPXCjwAAAAYA#####wEAAAABEAAAAQAAAAEAAACjAD#wAAAAAAAAAAAABwD#####Af8AAAAQAAAAAAAAAAAAAABACAAAAAAAAAAACAAAAAAMAAAApAAAAAcA#####wH#AAAAEAAAAAAAAAAAAAAAQAgAAAAAAAAAAAgAAAAAZgAAAKQAAAAMAP####8AAAAAAAdwb2x5MjExAAIAAAAFAAAAnwAAAKUAAACmAAAAoAAAAJ8AAAANAP####8Bf39#AAdzdXJmMjExAAAABQAAAKcAAAALAP####8B####ABAAAAAAAAAAAAAAAEAIAAAAAAAAAAlkaXJpZ2UyMTIHAAFAh1wAAAAAAEBlb1wo9cKPAAAABgD#####AQAAAAEQAAABAAAAAQAAAKkAP#AAAAAAAAAAAAAHAP####8B#wAAABAAAAAAAAAAAAAAAEAIAAAAAAAAAAAIAAAAAAwAAACqAAAABwD#####Af8AAAAQAAAAAAAAAAAAAABACAAAAAAAAAAACAAAAABmAAAAqgAAAAwA#####wAAAAAAB3BvbHkyMTIAAgAAAAUAAAClAAAAqwAAAKwAAACmAAAApQAAAA0A#####wF#f38AB3N1cmYyMTIAAAAFAAAArf####8AAAABAAhDU2VnbWVudAD#####AQAA#wAQAAABAAAAAQAAAAcAAAA6AAAAEgD#####AQAA#wAQAAABAAAAAQAAADkAAAAJAAAABwD#####AQAA#wAQAAAAAAAAAAAAAABACAAAAAAAAAAACAAAAACvAAAAsAAAAAMA#####wH###8BAANpbTAAAACxDQAAAAAAAQAAAAEAAAACAAAAAAAAAAAAAAAAMgAAADIAAAamiVBORw0KGgoAAAANSUhEUgAAADIAAAAyCAIAAACRXR#mAAAAAXNSR0IArs4c6QAAAARnQU1BAACxjwv8YQUAAAAJcEhZcwAADsMAAA7DAcdvqGQAAAY7SURBVFhHxZlbSFVZHMaP3VORbkMYNYNZc6QMFJEeykKKUZCZ6NRLvvhSINgU49iDOjKHLpOlUEORRKQvYyCCPRr02ENEN9CSCrLLgwRdmC6Seszmt#Z3XJ6Oes7e56DzPWz++7++tda3v#Xf+6y9j+#rrGDMAcEXBwTXr1+vqKjIz88vLCysrKy8desWydHRUdFmQ1aUJuKDBw#OnTvXF4GFCxcePXoUTigUghNLloaLjTB1elgak8mMffv2ScqSJUvKyspKSkoWL16sTHV1NUyUJegWc3C0U04HS0AQICgvL5eCLVu2PHv2zGF97enpyc3NVb6+vp6MkRUaHdM0Jg6FPn369PHjR45TgqaRkRGYsQUBq8n6JE0sX3Fx8dDQEE0MpdEGBgY2btwoZdeuXZtwq6urKxAI+P3+76fHDw42bNhw48YNpuEaNPFkGEXjPumarU8pKSl5eXkvXrwgyQgcpez58+crVqyAgGjfv4Ojg4ODe#bsUR+XOHHiBAMNDw9PKctqivIJcG0KsrOz0QFH18ZQxNwKNC1dutQ3+mVs7969otJn##79DQ0Nf06DYDBI65kzZz58+MBYdukjYRSN+wQIrKatW7e+e#eOEXS6bt06lRfKkAX5+PHj5Lkrfe3t7SLt2rXr7du3ZmAvkAILq2myT9u3b+diRKOulbTKVGpwWGKMNHcEET5RzjSgGu2qxOkg2y3MPA7sqWPTNz5t27aNe4UMfQFBpLL+#n4yJ0+eVKampsa3bNkyosOHD9OgWrEg4x62y5Q+SZO0QuDaCOrq6kTYtGmTjbHq9evX5nblhBJmINj00eieYBRN41OUJtGsstra2nnz5okJMjMz+#r6yPuUPXbsGCdQ1c0THEmufBITiKy7b+fOnbIGTb29vWRY5WRl2WmMS#F8EhTLrdOnTy9atAgymh48eEBG+bAs7kyl1M0lnFnCl87cxJ40NTc3i2x9Uh5C4rLMJA7ZuOTdp6amJpGjfBIzQVnqTJC8T5M1ESQiy3Y2LiXnU9TaiQk8y7KdJ#tkn5kxNMWoJzEFb7JsZ+PStz650RS7nggsPMiynWeoniLhTRZH45J3Te59EtzKUh6fAHHCPsWop0h4cEtrB8dqYv#kySeXmoAHtzRcY2Mj#Dlz5rAjIiOtIgiRZE#1FAlXskg6q2eWr6CgAE30ysrKunfvHq3qJYisub3WUyTcuiVL2H3rl1Vgr3bnzh3ymg#QXbH1adWqVe7XzsKtW2rSDhtlBw4cYE9LvHz5cqtMNOJkfBJcyWL5tNOtqKiAzA6b+OLFi87UE8o+f#7M0Ws9KWlbFcSXRUZVxVsA+27IvK2ToamlpcURYJTdvn2bzKlTp5Rxr0lwtvjht06OrtxSYWEJxQ65ra0Nml5Jzp07p9WkhnghMIq811Nkq2JXbikvJ+bPn##o0aNwmwN2mJIreKonmmT8lStX2CFTBuLHkSWS3CotLYVZVFREzFj4ceHChUAgkJOTY78KocmTTyqPgYEBrpbuHR0dJOkb3y1dzZs3b#TqVlJScuTIkby8vAULFhgh40hPT9+xY8fDhw8h6#5wJMXSBHTBT58+TUtL49ouXbpEF1494rulSrx69SorFSWFgXJzcysrKzs7O1++fKkukQ99ZWJAbvFinZqayoCtra0k47vFqV6bgsGgo8SAit69e#fZs2fv378vYwTmkLVGkQtNQPxEZHH1HOnJL3R1dXV3d3fUpwpzZztfFo2WcYTb4iFBWcLkJFqRIsWRCDe7RuJuKUN#uQISM2ZKJO6WnXtykDySWsSZQxxZyXwaSQZ6mvT39+sb+ISslSszJQtB#5dbyIp0yzxOi3#6mXN93+Jc5ex0mXEwkaazbl2+fJlT41ZVsJXzQ4cOwUMW2VkDM3Jfs453797VNoTfRPJGFm1rs9evWbPm#fv3UqanwCyA6TkyaVVVFZr4tX7y5AmnCDWyfm#qJMsG4dWrV5zOMs6fP6#brqysjFM9osP#Yvz6x980sC1hW1xbW1s3w2CK+vp6Kmfz5s3MCzIyMh4#foygsKyhULjA8SxrrdkTzz78fv#NmzfRoOcFmPjPR#jtr39+CZT#6M#5buaxevXqwsJCnk367beaxsbG#gMW+o#eOeGtdgAAAABJRU5ErkJgggAAABIA#####wEAAP8AEAAAAQAAAAEAAAAJAAAAQAAAABIA#####wEAAP8AEAAAAQAAAAEAAAALAAAAPwAAAAcA#####wEAAP8AEAAAAAAAAAAAAAAAQAgAAAAAAAAAAAgAAAAAswAAALT#####AAAAAQAUQ0ltcGxlbWVudGF0aW9uUHJvdG8A#####wACaW0AAAAAAAAAAQAAAAEAAAC1AAAAAwEAAAC2Af###wEABGltMTEAAAC1DQAAAAAAAQAAAAEAAAACAAAAAAAAAAAAAAAAMgAAADIAAAamiVBORw0KGgoAAAANSUhEUgAAADIAAAAyCAIAAACRXR#mAAAAAXNSR0IArs4c6QAAAARnQU1BAACxjwv8YQUAAAAJcEhZcwAADsMAAA7DAcdvqGQAAAY7SURBVFhHxZlbSFVZHMaP3VORbkMYNYNZc6QMFJEeykKKUZCZ6NRLvvhSINgU49iDOjKHLpOlUEORRKQvYyCCPRr02ENEN9CSCrLLgwRdmC6Seszmt#Z3XJ6Oes7e56DzPWz++7++tda3v#Xf+6y9j+#rrGDMAcEXBwTXr1+vqKjIz88vLCysrKy8desWydHRUdFmQ1aUJuKDBw#OnTvXF4GFCxcePXoUTigUghNLloaLjTB1elgak8mMffv2ScqSJUvKyspKSkoWL16sTHV1NUyUJegWc3C0U04HS0AQICgvL5eCLVu2PHv2zGF97enpyc3NVb6+vp6MkRUaHdM0Jg6FPn369PHjR45TgqaRkRGYsQUBq8n6JE0sX3Fx8dDQEE0MpdEGBgY2btwoZdeuXZtwq6urKxAI+P3+76fHDw42bNhw48YNpuEaNPFkGEXjPumarU8pKSl5eXkvXrwgyQgcpez58+crVqyAgGjfv4Ojg4ODe#bsUR+XOHHiBAMNDw9PKctqivIJcG0KsrOz0QFH18ZQxNwKNC1dutQ3+mVs7969otJn##79DQ0Nf06DYDBI65kzZz58+MBYdukjYRSN+wQIrKatW7e+e#eOEXS6bt06lRfKkAX5+PHj5Lkrfe3t7SLt2rXr7du3ZmAvkAILq2myT9u3b+diRKOulbTKVGpwWGKMNHcEET5RzjSgGu2qxOkg2y3MPA7sqWPTNz5t27aNe4UMfQFBpLL+#n4yJ0+eVKampsa3bNkyosOHD9OgWrEg4x62y5Q+SZO0QuDaCOrq6kTYtGmTjbHq9evX5nblhBJmINj00eieYBRN41OUJtGsstra2nnz5okJMjMz+#r6yPuUPXbsGCdQ1c0THEmufBITiKy7b+fOnbIGTb29vWRY5WRl2WmMS#F8EhTLrdOnTy9atAgymh48eEBG+bAs7kyl1M0lnFnCl87cxJ40NTc3i2x9Uh5C4rLMJA7ZuOTdp6amJpGjfBIzQVnqTJC8T5M1ESQiy3Y2LiXnU9TaiQk8y7KdJ#tkn5kxNMWoJzEFb7JsZ+PStz650RS7nggsPMiynWeoniLhTRZH45J3Te59EtzKUh6fAHHCPsWop0h4cEtrB8dqYv#kySeXmoAHtzRcY2Mj#Dlz5rAjIiOtIgiRZE#1FAlXskg6q2eWr6CgAE30ysrKunfvHq3qJYisub3WUyTcuiVL2H3rl1Vgr3bnzh3ymg#QXbH1adWqVe7XzsKtW2rSDhtlBw4cYE9LvHz5cqtMNOJkfBJcyWL5tNOtqKiAzA6b+OLFi87UE8o+f#7M0Ws9KWlbFcSXRUZVxVsA+27IvK2ToamlpcURYJTdvn2bzKlTp5Rxr0lwtvjht06OrtxSYWEJxQ65ra0Nml5Jzp07p9WkhnghMIq811Nkq2JXbikvJ+bPn##o0aNwmwN2mJIreKonmmT8lStX2CFTBuLHkSWS3CotLYVZVFREzFj4ceHChUAgkJOTY78KocmTTyqPgYEBrpbuHR0dJOkb3y1dzZs3b#TqVlJScuTIkby8vAULFhgh40hPT9+xY8fDhw8h6#5wJMXSBHTBT58+TUtL49ouXbpEF1494rulSrx69SorFSWFgXJzcysrKzs7O1++fKkukQ99ZWJAbvFinZqayoCtra0k47vFqV6bgsGgo8SAit69e#fZs2fv378vYwTmkLVGkQtNQPxEZHH1HOnJL3R1dXV3d3fUpwpzZztfFo2WcYTb4iFBWcLkJFqRIsWRCDe7RuJuKUN#uQISM2ZKJO6WnXtykDySWsSZQxxZyXwaSQZ6mvT39+sb+ISslSszJQtB#5dbyIp0yzxOi3#6mXN93+Jc5ex0mXEwkaazbl2+fJlT41ZVsJXzQ4cOwUMW2VkDM3Jfs453797VNoTfRPJGFm1rs9evWbPm#fv3UqanwCyA6TkyaVVVFZr4tX7y5AmnCDWyfm#qJMsG4dWrV5zOMs6fP6#brqysjFM9osP#Yvz6x980sC1hW1xbW1s3w2CK+vp6Kmfz5s3MCzIyMh4#foygsKyhULjA8SxrrdkTzz78fv#NmzfRoOcFmPjPR#jtr39+CZT#6M#5buaxevXqwsJCnk367beaxsbG#gMW+o#eOeGtdgAAAABJRU5ErkJgggAAABIA#####wEAAP8AEAAAAQAAAAEAAAA#AAAARgAAABIA#####wEAAP8AEAAAAQAAAAEAAABFAAAAQAAAAAcA#####wEAAP8AEAAAAAAAAAAAAAAAQAgAAAAAAAAAAAgAAAAAuAAAALkAAAATAP####8AAmltAAAAAAAAAAEAAAABAAAAugAAAAMBAAAAuwH###8BAARpbTEyAAAAug0AAAAAAAEAAAABAAAAAgAAAAAAAAAAAAAAADIAAAAyAAAGpolQTkcNChoKAAAADUlIRFIAAAAyAAAAMggCAAAAkV0f5gAAAAFzUkdCAK7OHOkAAAAEZ0FNQQAAsY8L#GEFAAAACXBIWXMAAA7DAAAOwwHHb6hkAAAGO0lEQVRYR8WZW0hVWRzGj91TkW5DGDWDWXOkDBSRHspCilGQmejUS774UiDYFOPYgzoyhy6TpVBDkUSkL2Mggj0a9NhDRDfQkgqyy4MEXZguknrM5rf2d1yejnrO3ueg8z1s#vu#vrXWt7#13#usvY#v66xgzAHBFwcE169fr6ioyM#PLywsrKysvHXrFsnR0VHRZkNWlCbigwcPzp071xeBhQsXHj16FE4oFIITS5aGi40wdXpYGpPJjH379knKkiVLysrKSkpKFi9erEx1dTVMlCXoFnNwtFNOB0tAECAoLy+Xgi1btjx79sxhfe3p6cnNzVW+vr6ejJEVGh3TNCYOhT59+vTx40eOU4KmkZERmLEFAavJ+iRNLF9xcfHQ0BBNDKXRBgYGNm7cKGXXrl2bcKurqysQCPj9#u+nxw8ONmzYcOPGDabhGjTxZBhF4z7pmq1PKSkpeXl5L168IMkIHKXs+fPnK1asgIBo37+Do4ODg3v27FEflzhx4gQDDQ8PTynLaoryCXBtCrKzs9EBR9fGUMTcCjQtXbrUN#plbO#evaLSZ##+#Q0NDX9Og2AwSOuZM2c+fPjAWHbpI2EUjfsECKymrVu3vnv3jhF0um7dOpUXypAF+fjx4+S5K33t7e0i7dq16+3bt2ZgL5ACC6tpsk#bt2#nYkSjrpW0ylRqcFhijDR3BBE+Uc40oBrtqsTpINstzDwO7Klj0zc+bdu2jXuFDH0BQaSy#v5+MidPnlSmpqbGt2zZMqLDhw#ToFqxIOMetsuUPkmTtELg2gjq6upE2LRpk42x6vXr1+Z25YQSZiDY9NHonmAUTeNTlCbRrLLa2tp58+aJCTIzM#v6+sj7lD127BgnUNXNExxJrnwSE4isu2#nzp2yBk29vb1kWOVkZdlpjEvxfBIUy63Tp08vWrQIMpoePHhARvmwLO5MpdTNJZxZwpfO3MSeNDU3N4tsfVIeQuKyzCQO2bjk3aempiaRo3wSM0FZ6kyQvE+TNREkIst2Ni4l51PU2okJPMuynSf7ZJ+ZMTTFqCcxBW+ybGfj0rc+udEUu54ILDzIsp1nqJ4i4U0WR+OSd03ufRLcylIenwBxwj7FqKdIeHBLawfHamL#5Mknl5qAB7c0XGNjI#w5c+awIyIjrSIIkWRP9RQJV7JIOqtnlq+goABN9MrKyrp37x6t6iWIrLm91lMk3LolS9h965dVYK92584d8poP0F2x9WnVqlXu187CrVtq0g4bZQcOHGBPS7x8+XKrTDTiZHwSXMli+bTTraiogMwOm#jixYvO1BPKPn#+zNFrPSlpWxXEl0VGVcVbAPtuyLytk6GppaXFEWCU3b59m8ypU6eUca9JcLb44bdOjq7cUmFhCcUOua2tDZpeSc6dO6fVpIZ4ITCKvNdTZKtiV24pLyfmz5##6NGjcJsDdpiSK3iqJ5pk#JUrV9ghUwbix5ElktwqLS2FWVRURMxY+HHhwoVAIJCTk2O#CqHJk08qj4GBAa6W7h0dHSTpG98tXc2bN2#06lZSUnLkyJG8vLwFCxYYIeNIT0#fsWPHw4cPIev+cCTF0gR0wU+fPk1LS+PaLl26RBdePeK7pUq8evUqKxUlhYFyc3MrKys7OztfvnypLpEPfWViQG7xYp2amsqAra2tJOO7xalem4LBoKPEgIrevXv32bNn79+#L2ME5pC1RpELTUD8RGRx9RzpyS90dXV1d3d31KcKc2c7XxaNlnGE2+IhQVnC5CRakSLFkQg3u0bibilDf7kCEjNmSiTulp17cpA8klrEmUMcWcl8GkkGepr09#frG#iErJUrMyULQf+XW8iKdMs8Tot#+plzfd#iXOXsdJlxMJGms25dvnyZU+NWVbCV80OHDsFDFtlZAzNyX7OOd+#e1TaE30TyRhZta7PXr1mz5v3791Kmp8AsgOk5MmlVVRWa+LV+8uQJpwg1sn5v6iTLBuHVq1eczjLOnz+v266srIxTPaLD#2L8+sffNLAtYVtcW1tbN8Ngivr6eipn8+bNzAsyMjIeP36MoLCsoVC4wPEsa63ZE88+#H7#zZs30aDnBZj4z0f47a9#fgmU#+jP+W7msXr16sLCQp5N+u23msbGxv4DFvqP3jnhrXYAAAAASUVORK5CYIIAAAASAP####8BAAD#ABAAAAEAAAABAAAARQAAAEwAAAASAP####8BAAD#ABAAAAEAAAABAAAASwAAAEYAAAAHAP####8BAAD#ABAAAAAAAAAAAAAAAEAIAAAAAAAAAAAIAAAAAL0AAAC+AAAAEwD#####AAJpbQAAAAAAAAABAAAAAQAAAL8AAAADAQAAAMAB####AQAEaW0xMwAAAL8NAAAAAAABAAAAAQAAAAIAAAAAAAAAAAAAAAAyAAAAMgAABqaJUE5HDQoaCgAAAA1JSERSAAAAMgAAADIIAgAAAJFdH+YAAAABc1JHQgCuzhzpAAAABGdBTUEAALGPC#xhBQAAAAlwSFlzAAAOwwAADsMBx2+oZAAABjtJREFUWEfFmVtIVVkcxo#dU5FuQxg1g1lzpAwUkR7KQopRkJno1Eu++FIg2BTj2IM6Mocuk6VQQ5FEpC9jIII9GvTYQ0Q30JIKssuDBF2YLpJ6zOa39ndcno56zt7noPM9bP77v7611re#9d#7rL2P7+usYMwBwRcHBNevX6+oqMjPzy8sLKysrLx16xbJ0dFR0WZDVpQm4oMHD86dO9cXgYULFx49ehROKBSCE0uWhouNMHV6WBqTyYx9+#ZJypIlS8rKykpKShYvXqxMdXU1TJQl6BZzcLRTTgdLQBAgKC8vl4ItW7Y8e#bMYX3t6enJzc1Vvr6+noyRFRod0zQmDoU+ffr08eNHjlOCppGREZixBQGryfokTSxfcXHx0NAQTQyl0QYGBjZu3Chl165dm3Crq6srEAj4#f7vp8cPDjZs2HDjxg2m4Ro08WQYReM+6ZqtTykpKXl5eS9evCDJCByl7Pnz5ytWrICAaN+#g6ODg4N79uxRH5c4ceIEAw0PD08py2qK8glwbQqys7PRAUfXxlDE3Ao0LV261Df6ZWzv3r2i0mf##v0NDQ1#ToNgMEjrmTNnPnz4wFh26SNhFI37BAispq1bt757944RdLpu3TqVF8qQBfn48ePkuSt97e3tIu3atevt27dmYC+QAgurabJP27dv52JEo66VtMpUanBYYow0dwQRPlHONKAa7arE6SDbLcw8DuypY9M3Pm3bto17hQx9AUGksv7+fjInT55Upqamxrds2TKiw4cP06BasSDjHrbLlD5Jk7RC4NoI6urqRNi0aZONser169fmduWEEmYg2PTR6J5gFE3jU5Qm0ayy2traefPmiQkyMzP7+vrI+5Q9duwYJ1DVzRMcSa58EhOIrLtv586dsgZNvb29ZFjlZGXZaYxL8XwSFMut06dPL1q0CDKaHjx4QEb5sCzuTKXUzSWcWcKXztzEnjQ1NzeLbH1SHkLisswkDtm45N2npqYmkaN8EjNBWepMkLxPkzURJCLLdjYuJedT1NqJCTzLsp0n+2SfmTE0xagnMQVvsmxn49K3PrnRFLueCCw8yLKdZ6ieIuFNFkfjkndN7n0S3MpSHp8AccI+xainSHhwS2sHx2pi#+TJJ5eagAe3NFxjYyP8OXPmsCMiI60iCJFkT#UUCVeySDqrZ5avoKAATfTKysq6d+8ereoliKy5vdZTJNy6JUvYfeuXVWCvdufOHfKaD9BdsfVp1apV7tfOwq1batIOG2UHDhxgT0u8fPlyq0w04mR8ElzJYvm0062oqIDMDpv44sWLztQTyj5##szRaz0paVsVxJdFRlXFWwD7bsi8rZOhqaWlxRFglN2+fZvMqVOnlHGvSXC2+OG3To6u3FJhYQnFDrmtrQ2aXknOnTun1aSGeCEwirzXU2SrYlduKS8n5s+f#+jRo3CbA3aYkit4qieaZPyVK1fYIVMG4seRJZLcKi0thVlUVETMWPhx4cKFQCCQk5NjvwqhyZNPKo+BgQGulu4dHR0k6RvfLV3Nmzdv9OpWUlJy5MiRvLy8BQsWGCHjSE9P37Fjx8OHDyHr#nAkxdIEdMFPnz5NS0vj2i5dukQXXj3iu6VKvHr1KisVJYWBcnNzKysrOzs7X758qS6RD31lYkBu8WKdmprKgK2trSTju8WpXpuCwaCjxICK3r1799mzZ+#fvy9jBOaQtUaRC01A#ERkcfUc6ckvdHV1dXd3d9SnCnNnO18WjZZxhNviIUFZwuQkWpEixZEIN7tG4m4pQ3+5AhIzZkok7pade3KQPJJaxJlDHFnJfBpJBnqa9Pf36xv4hKyVKzMlC0H#l1vIinTLPE6Lf#qZc33f4lzl7HSZcTCRprNuXb58mVPjVlWwlfNDhw7BQxbZWQMzcl+zjnfv3tU2hN9E8kYWbWuz169Zs+b9+#dSpqfALIDpOTJpVVUVmvi1fvLkCacINbJ+b+okywbh1atXnM4yzp8#r9uurKyMUz2iw#9i#PrH3zSwLWFbXFtbWzfDYIr6+noqZ#PmzcwLMjIyHj9+jKCwrKFQuMDxLGut2RPPPvx+#82bN9Gg5wWY+M9H+O2vf34JlP#oz#lu5rF69erCwkKeTfrtt5rGxsb+Axb6j9454a12AAAAAElFTkSuQmCCAAAAEgD#####AQAA#wAQAAABAAAAAQAAAEsAAAAPAAAAEgD#####AQAA#wAQAAABAAAAAQAAAE8AAABMAAAABwD#####AQAA#wAQAAAAAAAAAAAAAABACAAAAAAAAAAACAAAAADCAAAAwwAAABMA#####wACaW0AAAAAAAAAAQAAAAEAAADEAAAAAwEAAADFAf###wEABGltMTQAAADEDQAAAAAAAQAAAAEAAAACAAAAAAAAAAAAAAAAMgAAADIAAAamiVBORw0KGgoAAAANSUhEUgAAADIAAAAyCAIAAACRXR#mAAAAAXNSR0IArs4c6QAAAARnQU1BAACxjwv8YQUAAAAJcEhZcwAADsMAAA7DAcdvqGQAAAY7SURBVFhHxZlbSFVZHMaP3VORbkMYNYNZc6QMFJEeykKKUZCZ6NRLvvhSINgU49iDOjKHLpOlUEORRKQvYyCCPRr02ENEN9CSCrLLgwRdmC6Seszmt#Z3XJ6Oes7e56DzPWz++7++tda3v#Xf+6y9j+#rrGDMAcEXBwTXr1+vqKjIz88vLCysrKy8desWydHRUdFmQ1aUJuKDBw#OnTvXF4GFCxcePXoUTigUghNLloaLjTB1elgak8mMffv2ScqSJUvKyspKSkoWL16sTHV1NUyUJegWc3C0U04HS0AQICgvL5eCLVu2PHv2zGF97enpyc3NVb6+vp6MkRUaHdM0Jg6FPn369PHjR45TgqaRkRGYsQUBq8n6JE0sX3Fx8dDQEE0MpdEGBgY2btwoZdeuXZtwq6urKxAI+P3+76fHDw42bNhw48YNpuEaNPFkGEXjPumarU8pKSl5eXkvXrwgyQgcpez58+crVqyAgGjfv4Ojg4ODe#bsUR+XOHHiBAMNDw9PKctqivIJcG0KsrOz0QFH18ZQxNwKNC1dutQ3+mVs7969otJn##79DQ0Nf06DYDBI65kzZz58+MBYdukjYRSN+wQIrKatW7e+e#eOEXS6bt06lRfKkAX5+PHj5Lkrfe3t7SLt2rXr7du3ZmAvkAILq2myT9u3b+diRKOulbTKVGpwWGKMNHcEET5RzjSgGu2qxOkg2y3MPA7sqWPTNz5t27aNe4UMfQFBpLL+#n4yJ0+eVKampsa3bNkyosOHD9OgWrEg4x62y5Q+SZO0QuDaCOrq6kTYtGmTjbHq9evX5nblhBJmINj00eieYBRN41OUJtGsstra2nnz5okJMjMz+#r6yPuUPXbsGCdQ1c0THEmufBITiKy7b+fOnbIGTb29vWRY5WRl2WmMS#F8EhTLrdOnTy9atAgymh48eEBG+bAs7kyl1M0lnFnCl87cxJ40NTc3i2x9Uh5C4rLMJA7ZuOTdp6amJpGjfBIzQVnqTJC8T5M1ESQiy3Y2LiXnU9TaiQk8y7KdJ#tkn5kxNMWoJzEFb7JsZ+PStz650RS7nggsPMiynWeoniLhTRZH45J3Te59EtzKUh6fAHHCPsWop0h4cEtrB8dqYv#kySeXmoAHtzRcY2Mj#Dlz5rAjIiOtIgiRZE#1FAlXskg6q2eWr6CgAE30ysrKunfvHq3qJYisub3WUyTcuiVL2H3rl1Vgr3bnzh3ymg#QXbH1adWqVe7XzsKtW2rSDhtlBw4cYE9LvHz5cqtMNOJkfBJcyWL5tNOtqKiAzA6b+OLFi87UE8o+f#7M0Ws9KWlbFcSXRUZVxVsA+27IvK2ToamlpcURYJTdvn2bzKlTp5Rxr0lwtvjht06OrtxSYWEJxQ65ra0Nml5Jzp07p9WkhnghMIq811Nkq2JXbikvJ+bPn##o0aNwmwN2mJIreKonmmT8lStX2CFTBuLHkSWS3CotLYVZVFREzFj4ceHChUAgkJOTY78KocmTTyqPgYEBrpbuHR0dJOkb3y1dzZs3b#TqVlJScuTIkby8vAULFhgh40hPT9+xY8fDhw8h6#5wJMXSBHTBT58+TUtL49ouXbpEF1494rulSrx69SorFSWFgXJzcysrKzs7O1++fKkukQ99ZWJAbvFinZqayoCtra0k47vFqV6bgsGgo8SAit69e#fZs2fv378vYwTmkLVGkQtNQPxEZHH1HOnJL3R1dXV3d3fUpwpzZztfFo2WcYTb4iFBWcLkJFqRIsWRCDe7RuJuKUN#uQISM2ZKJO6WnXtykDySWsSZQxxZyXwaSQZ6mvT39+sb+ISslSszJQtB#5dbyIp0yzxOi3#6mXN93+Jc5ex0mXEwkaazbl2+fJlT41ZVsJXzQ4cOwUMW2VkDM3Jfs453797VNoTfRPJGFm1rs9evWbPm#fv3UqanwCyA6TkyaVVVFZr4tX7y5AmnCDWyfm#qJMsG4dWrV5zOMs6fP6#brqysjFM9osP#Yvz6x980sC1hW1xbW1s3w2CK+vp6Kmfz5s3MCzIyMh4#foygsKyhULjA8SxrrdkTzz78fv#NmzfRoOcFmPjPR#jtr39+CZT#6M#5buaxevXqwsJCnk367beaxsbG#gMW+o#eOeGtdgAAAABJRU5ErkJgggAAABIA#####wEAAP8AEAAAAQAAAAEAAABPAAAAVQAAABIA#####wEAAP8AEAAAAQAAAAEAAABUAAAADwAAAAcA#####wEAAP8AEAAAAAAAAAAAAAAAQAgAAAAAAAAAAAgAAAAAxwAAAMgAAAATAP####8AAmltAAAAAAAAAAEAAAABAAAAyQAAAAMBAAAAygH###8BAARpbTE1AAAAyQ0AAAAAAAEAAAABAAAAAgAAAAAAAAAAAAAAADIAAAAyAAAGpolQTkcNChoKAAAADUlIRFIAAAAyAAAAMggCAAAAkV0f5gAAAAFzUkdCAK7OHOkAAAAEZ0FNQQAAsY8L#GEFAAAACXBIWXMAAA7DAAAOwwHHb6hkAAAGO0lEQVRYR8WZW0hVWRzGj91TkW5DGDWDWXOkDBSRHspCilGQmejUS774UiDYFOPYgzoyhy6TpVBDkUSkL2Mggj0a9NhDRDfQkgqyy4MEXZguknrM5rf2d1yejnrO3ueg8z1s#vu#vrXWt7#13#usvY#v66xgzAHBFwcE169fr6ioyM#PLywsrKysvHXrFsnR0VHRZkNWlCbigwcPzp071xeBhQsXHj16FE4oFIITS5aGi40wdXpYGpPJjH379knKkiVLysrKSkpKFi9erEx1dTVMlCXoFnNwtFNOB0tAECAoLy+Xgi1btjx79sxhfe3p6cnNzVW+vr6ejJEVGh3TNCYOhT59+vTx40eOU4KmkZERmLEFAavJ+iRNLF9xcfHQ0BBNDKXRBgYGNm7cKGXXrl2bcKurqysQCPj9#u+nxw8ONmzYcOPGDabhGjTxZBhF4z7pmq1PKSkpeXl5L168IMkIHKXs+fPnK1asgIBo37+Do4ODg3v27FEflzhx4gQDDQ8PTynLaoryCXBtCrKzs9EBR9fGUMTcCjQtXbrUN#plbO#evaLSZ##+#Q0NDX9Og2AwSOuZM2c+fPjAWHbpI2EUjfsECKymrVu3vnv3jhF0um7dOpUXypAF+fjx4+S5K33t7e0i7dq16+3bt2ZgL5ACC6tpsk#bt2#nYkSjrpW0ylRqcFhijDR3BBE+Uc40oBrtqsTpINstzDwO7Klj0zc+bdu2jXuFDH0BQaSy#v5+MidPnlSmpqbGt2zZMqLDhw#ToFqxIOMetsuUPkmTtELg2gjq6upE2LRpk42x6vXr1+Z25YQSZiDY9NHonmAUTeNTlCbRrLLa2tp58+aJCTIzM#v6+sj7lD127BgnUNXNExxJrnwSE4isu2#nzp2yBk29vb1kWOVkZdlpjEvxfBIUy63Tp08vWrQIMpoePHhARvmwLO5MpdTNJZxZwpfO3MSeNDU3N4tsfVIeQuKyzCQO2bjk3aempiaRo3wSM0FZ6kyQvE+TNREkIst2Ni4l51PU2okJPMuynSf7ZJ+ZMTTFqCcxBW+ybGfj0rc+udEUu54ILDzIsp1nqJ4i4U0WR+OSd03ufRLcylIenwBxwj7FqKdIeHBLawfHamL#5Mknl5qAB7c0XGNjI#w5c+awIyIjrSIIkWRP9RQJV7JIOqtnlq+goABN9MrKyrp37x6t6iWIrLm91lMk3LolS9h965dVYK92584d8poP0F2x9WnVqlXu187CrVtq0g4bZQcOHGBPS7x8+XKrTDTiZHwSXMli+bTTraiogMwOm#jixYvO1BPKPn#+zNFrPSlpWxXEl0VGVcVbAPtuyLytk6GppaXFEWCU3b59m8ypU6eUca9JcLb44bdOjq7cUmFhCcUOua2tDZpeSc6dO6fVpIZ4ITCKvNdTZKtiV24pLyfmz5##6NGjcJsDdpiSK3iqJ5pk#JUrV9ghUwbix5ElktwqLS2FWVRURMxY+HHhwoVAIJCTk2O#CqHJk08qj4GBAa6W7h0dHSTpG98tXc2bN2#06lZSUnLkyJG8vLwFCxYYIeNIT0#fsWPHw4cPIev+cCTF0gR0wU+fPk1LS+PaLl26RBdePeK7pUq8evUqKxUlhYFyc3MrKys7OztfvnypLpEPfWViQG7xYp2amsqAra2tJOO7xalem4LBoKPEgIrevXv32bNn79+#L2ME5pC1RpELTUD8RGRx9RzpyS90dXV1d3d31KcKc2c7XxaNlnGE2+IhQVnC5CRakSLFkQg3u0bibilDf7kCEjNmSiTulp17cpA8klrEmUMcWcl8GkkGepr09#frG#iErJUrMyULQf+XW8iKdMs8Tot#+plzfd#iXOXsdJlxMJGms25dvnyZU+NWVbCV80OHDsFDFtlZAzNyX7OOd+#e1TaE30TyRhZta7PXr1mz5v3791Kmp8AsgOk5MmlVVRWa+LV+8uQJpwg1sn5v6iTLBuHVq1eczjLOnz+v266srIxTPaLD#2L8+sffNLAtYVtcW1tbN8Ngivr6eipn8+bNzAsyMjIeP36MoLCsoVC4wPEsa63ZE88+#H7#zZs30aDnBZj4z0f47a9#fgmU#+jP+W7msXr16sLCQp5N+u23msbGxv4DFvqP3jnhrXYAAAAASUVORK5CYIIAAAASAP####8BAAD#ABAAAAEAAAABAAAAVAAAAFsAAAASAP####8BAAD#ABAAAAEAAAABAAAAWgAAAFUAAAAHAP####8BAAD#ABAAAAAAAAAAAAAAAEAIAAAAAAAAAAAIAAAAAMwAAADNAAAAEwD#####AAJpbQAAAAAAAAABAAAAAQAAAM4AAAADAQAAAM8B####AQAEaW0xNgAAAM4NAAAAAAABAAAAAQAAAAIAAAAAAAAAAAAAAAAyAAAAMgAABqaJUE5HDQoaCgAAAA1JSERSAAAAMgAAADIIAgAAAJFdH+YAAAABc1JHQgCuzhzpAAAABGdBTUEAALGPC#xhBQAAAAlwSFlzAAAOwwAADsMBx2+oZAAABjtJREFUWEfFmVtIVVkcxo#dU5FuQxg1g1lzpAwUkR7KQopRkJno1Eu++FIg2BTj2IM6Mocuk6VQQ5FEpC9jIII9GvTYQ0Q30JIKssuDBF2YLpJ6zOa39ndcno56zt7noPM9bP77v7611re#9d#7rL2P7+usYMwBwRcHBNevX6+oqMjPzy8sLKysrLx16xbJ0dFR0WZDVpQm4oMHD86dO9cXgYULFx49ehROKBSCE0uWhouNMHV6WBqTyYx9+#ZJypIlS8rKykpKShYvXqxMdXU1TJQl6BZzcLRTTgdLQBAgKC8vl4ItW7Y8e#bMYX3t6enJzc1Vvr6+noyRFRod0zQmDoU+ffr08eNHjlOCppGREZixBQGryfokTSxfcXHx0NAQTQyl0QYGBjZu3Chl165dm3Crq6srEAj4#f7vp8cPDjZs2HDjxg2m4Ro08WQYReM+6ZqtTykpKXl5eS9evCDJCByl7Pnz5ytWrICAaN+#g6ODg4N79uxRH5c4ceIEAw0PD08py2qK8glwbQqys7PRAUfXxlDE3Ao0LV261Df6ZWzv3r2i0mf##v0NDQ1#ToNgMEjrmTNnPnz4wFh26SNhFI37BAispq1bt757944RdLpu3TqVF8qQBfn48ePkuSt97e3tIu3atevt27dmYC+QAgurabJP27dv52JEo66VtMpUanBYYow0dwQRPlHONKAa7arE6SDbLcw8DuypY9M3Pm3bto17hQx9AUGksv7+fjInT55Upqamxrds2TKiw4cP06BasSDjHrbLlD5Jk7RC4NoI6urqRNi0aZONser169fmduWEEmYg2PTR6J5gFE3jU5Qm0ayy2traefPmiQkyMzP7+vrI+5Q9duwYJ1DVzRMcSa58EhOIrLtv586dsgZNvb29ZFjlZGXZaYxL8XwSFMut06dPL1q0CDKaHjx4QEb5sCzuTKXUzSWcWcKXztzEnjQ1NzeLbH1SHkLisswkDtm45N2npqYmkaN8EjNBWepMkLxPkzURJCLLdjYuJedT1NqJCTzLsp0n+2SfmTE0xagnMQVvsmxn49K3PrnRFLueCCw8yLKdZ6ieIuFNFkfjkndN7n0S3MpSHp8AccI+xainSHhwS2sHx2pi#+TJJ5eagAe3NFxjYyP8OXPmsCMiI60iCJFkT#UUCVeySDqrZ5avoKAATfTKysq6d+8ereoliKy5vdZTJNy6JUvYfeuXVWCvdufOHfKaD9BdsfVp1apV7tfOwq1batIOG2UHDhxgT0u8fPlyq0w04mR8ElzJYvm0062oqIDMDpv44sWLztQTyj5##szRaz0paVsVxJdFRlXFWwD7bsi8rZOhqaWlxRFglN2+fZvMqVOnlHGvSXC2+OG3To6u3FJhYQnFDrmtrQ2aXknOnTun1aSGeCEwirzXU2SrYlduKS8n5s+f#+jRo3CbA3aYkit4qieaZPyVK1fYIVMG4seRJZLcKi0thVlUVETMWPhx4cKFQCCQk5NjvwqhyZNPKo+BgQGulu4dHR0k6RvfLV3Nmzdv9OpWUlJy5MiRvLy8BQsWGCHjSE9P37Fjx8OHDyHr#nAkxdIEdMFPnz5NS0vj2i5dukQXXj3iu6VKvHr1KisVJYWBcnNzKysrOzs7X758qS6RD31lYkBu8WKdmprKgK2trSTju8WpXpuCwaCjxICK3r1799mzZ+#fvy9jBOaQtUaRC01A#ERkcfUc6ckvdHV1dXd3d9SnCnNnO18WjZZxhNviIUFZwuQkWpEixZEIN7tG4m4pQ3+5AhIzZkok7pade3KQPJJaxJlDHFnJfBpJBnqa9Pf36xv4hKyVKzMlC0H#l1vIinTLPE6Lf#qZc33f4lzl7HSZcTCRprNuXb58mVPjVlWwlfNDhw7BQxbZWQMzcl+zjnfv3tU2hN9E8kYWbWuz169Zs+b9+#dSpqfALIDpOTJpVVUVmvi1fvLkCacINbJ+b+okywbh1atXnM4yzp8#r9uurKyMUz2iw#9i#PrH3zSwLWFbXFtbWzfDYIr6+noqZ#PmzcwLMjIyHj9+jKCwrKFQuMDxLGut2RPPPvx+#82bN9Gg5wWY+M9H+O2vf34JlP#oz#lu5rF69erCwkKeTfrtt5rGxsb+Axb6j9454a12AAAAAElFTkSuQmCCAAAAEgD#####AQAA#wAQAAABAAAAAQAAAFoAAABhAAAAEgD#####AQAA#wAQAAABAAAAAQAAAGAAAABbAAAABwD#####AQAA#wAQAAAAAAAAAAAAAABACAAAAAAAAAAACAAAAADRAAAA0gAAABMA#####wACaW0AAAAAAAAAAQAAAAEAAADTAAAAAwEAAADUAf###wEABGltMTcAAADTDQAAAAAAAQAAAAEAAAACAAAAAAAAAAAAAAAAMgAAADIAAAamiVBORw0KGgoAAAANSUhEUgAAADIAAAAyCAIAAACRXR#mAAAAAXNSR0IArs4c6QAAAARnQU1BAACxjwv8YQUAAAAJcEhZcwAADsMAAA7DAcdvqGQAAAY7SURBVFhHxZlbSFVZHMaP3VORbkMYNYNZc6QMFJEeykKKUZCZ6NRLvvhSINgU49iDOjKHLpOlUEORRKQvYyCCPRr02ENEN9CSCrLLgwRdmC6Seszmt#Z3XJ6Oes7e56DzPWz++7++tda3v#Xf+6y9j+#rrGDMAcEXBwTXr1+vqKjIz88vLCysrKy8desWydHRUdFmQ1aUJuKDBw#OnTvXF4GFCxcePXoUTigUghNLloaLjTB1elgak8mMffv2ScqSJUvKyspKSkoWL16sTHV1NUyUJegWc3C0U04HS0AQICgvL5eCLVu2PHv2zGF97enpyc3NVb6+vp6MkRUaHdM0Jg6FPn369PHjR45TgqaRkRGYsQUBq8n6JE0sX3Fx8dDQEE0MpdEGBgY2btwoZdeuXZtwq6urKxAI+P3+76fHDw42bNhw48YNpuEaNPFkGEXjPumarU8pKSl5eXkvXrwgyQgcpez58+crVqyAgGjfv4Ojg4ODe#bsUR+XOHHiBAMNDw9PKctqivIJcG0KsrOz0QFH18ZQxNwKNC1dutQ3+mVs7969otJn##79DQ0Nf06DYDBI65kzZz58+MBYdukjYRSN+wQIrKatW7e+e#eOEXS6bt06lRfKkAX5+PHj5Lkrfe3t7SLt2rXr7du3ZmAvkAILq2myT9u3b+diRKOulbTKVGpwWGKMNHcEET5RzjSgGu2qxOkg2y3MPA7sqWPTNz5t27aNe4UMfQFBpLL+#n4yJ0+eVKampsa3bNkyosOHD9OgWrEg4x62y5Q+SZO0QuDaCOrq6kTYtGmTjbHq9evX5nblhBJmINj00eieYBRN41OUJtGsstra2nnz5okJMjMz+#r6yPuUPXbsGCdQ1c0THEmufBITiKy7b+fOnbIGTb29vWRY5WRl2WmMS#F8EhTLrdOnTy9atAgymh48eEBG+bAs7kyl1M0lnFnCl87cxJ40NTc3i2x9Uh5C4rLMJA7ZuOTdp6amJpGjfBIzQVnqTJC8T5M1ESQiy3Y2LiXnU9TaiQk8y7KdJ#tkn5kxNMWoJzEFb7JsZ+PStz650RS7nggsPMiynWeoniLhTRZH45J3Te59EtzKUh6fAHHCPsWop0h4cEtrB8dqYv#kySeXmoAHtzRcY2Mj#Dlz5rAjIiOtIgiRZE#1FAlXskg6q2eWr6CgAE30ysrKunfvHq3qJYisub3WUyTcuiVL2H3rl1Vgr3bnzh3ymg#QXbH1adWqVe7XzsKtW2rSDhtlBw4cYE9LvHz5cqtMNOJkfBJcyWL5tNOtqKiAzA6b+OLFi87UE8o+f#7M0Ws9KWlbFcSXRUZVxVsA+27IvK2ToamlpcURYJTdvn2bzKlTp5Rxr0lwtvjht06OrtxSYWEJxQ65ra0Nml5Jzp07p9WkhnghMIq811Nkq2JXbikvJ+bPn##o0aNwmwN2mJIreKonmmT8lStX2CFTBuLHkSWS3CotLYVZVFREzFj4ceHChUAgkJOTY78KocmTTyqPgYEBrpbuHR0dJOkb3y1dzZs3b#TqVlJScuTIkby8vAULFhgh40hPT9+xY8fDhw8h6#5wJMXSBHTBT58+TUtL49ouXbpEF1494rulSrx69SorFSWFgXJzcysrKzs7O1++fKkukQ99ZWJAbvFinZqayoCtra0k47vFqV6bgsGgo8SAit69e#fZs2fv378vYwTmkLVGkQtNQPxEZHH1HOnJL3R1dXV3d3fUpwpzZztfFo2WcYTb4iFBWcLkJFqRIsWRCDe7RuJuKUN#uQISM2ZKJO6WnXtykDySWsSZQxxZyXwaSQZ6mvT39+sb+ISslSszJQtB#5dbyIp0yzxOi3#6mXN93+Jc5ex0mXEwkaazbl2+fJlT41ZVsJXzQ4cOwUMW2VkDM3Jfs453797VNoTfRPJGFm1rs9evWbPm#fv3UqanwCyA6TkyaVVVFZr4tX7y5AmnCDWyfm#qJMsG4dWrV5zOMs6fP6#brqysjFM9osP#Yvz6x980sC1hW1xbW1s3w2CK+vp6Kmfz5s3MCzIyMh4#foygsKyhULjA8SxrrdkTzz78fv#NmzfRoOcFmPjPR#jtr39+CZT#6M#5buaxevXqwsJCnk367beaxsbG#gMW+o#eOeGtdgAAAABJRU5ErkJgggAAABIA#####wEAAP8AEAAAAQAAAAEAAAALAAAAagAAABIA#####wEAAP8AEAAAAQAAAAEAAABlAAAAaQAAAAcA#####wEAAP8AEAAAAAAAAAAAAAAAQAgAAAAAAAAAAAgAAAAA1gAAANcAAAATAP####8AAmltAAAAAAAAAAEAAAABAAAA2AAAAAMBAAAA2QH###8BAARpbTIxAAAA2A0AAAAAAAEAAAABAAAAAgAAAAAAAAAAAAAAADIAAAAyAAAGpolQTkcNChoKAAAADUlIRFIAAAAyAAAAMggCAAAAkV0f5gAAAAFzUkdCAK7OHOkAAAAEZ0FNQQAAsY8L#GEFAAAACXBIWXMAAA7DAAAOwwHHb6hkAAAGO0lEQVRYR8WZW0hVWRzGj91TkW5DGDWDWXOkDBSRHspCilGQmejUS774UiDYFOPYgzoyhy6TpVBDkUSkL2Mggj0a9NhDRDfQkgqyy4MEXZguknrM5rf2d1yejnrO3ueg8z1s#vu#vrXWt7#13#usvY#v66xgzAHBFwcE169fr6ioyM#PLywsrKysvHXrFsnR0VHRZkNWlCbigwcPzp071xeBhQsXHj16FE4oFIITS5aGi40wdXpYGpPJjH379knKkiVLysrKSkpKFi9erEx1dTVMlCXoFnNwtFNOB0tAECAoLy+Xgi1btjx79sxhfe3p6cnNzVW+vr6ejJEVGh3TNCYOhT59+vTx40eOU4KmkZERmLEFAavJ+iRNLF9xcfHQ0BBNDKXRBgYGNm7cKGXXrl2bcKurqysQCPj9#u+nxw8ONmzYcOPGDabhGjTxZBhF4z7pmq1PKSkpeXl5L168IMkIHKXs+fPnK1asgIBo37+Do4ODg3v27FEflzhx4gQDDQ8PTynLaoryCXBtCrKzs9EBR9fGUMTcCjQtXbrUN#plbO#evaLSZ##+#Q0NDX9Og2AwSOuZM2c+fPjAWHbpI2EUjfsECKymrVu3vnv3jhF0um7dOpUXypAF+fjx4+S5K33t7e0i7dq16+3bt2ZgL5ACC6tpsk#bt2#nYkSjrpW0ylRqcFhijDR3BBE+Uc40oBrtqsTpINstzDwO7Klj0zc+bdu2jXuFDH0BQaSy#v5+MidPnlSmpqbGt2zZMqLDhw#ToFqxIOMetsuUPkmTtELg2gjq6upE2LRpk42x6vXr1+Z25YQSZiDY9NHonmAUTeNTlCbRrLLa2tp58+aJCTIzM#v6+sj7lD127BgnUNXNExxJrnwSE4isu2#nzp2yBk29vb1kWOVkZdlpjEvxfBIUy63Tp08vWrQIMpoePHhARvmwLO5MpdTNJZxZwpfO3MSeNDU3N4tsfVIeQuKyzCQO2bjk3aempiaRo3wSM0FZ6kyQvE+TNREkIst2Ni4l51PU2okJPMuynSf7ZJ+ZMTTFqCcxBW+ybGfj0rc+udEUu54ILDzIsp1nqJ4i4U0WR+OSd03ufRLcylIenwBxwj7FqKdIeHBLawfHamL#5Mknl5qAB7c0XGNjI#w5c+awIyIjrSIIkWRP9RQJV7JIOqtnlq+goABN9MrKyrp37x6t6iWIrLm91lMk3LolS9h965dVYK92584d8poP0F2x9WnVqlXu187CrVtq0g4bZQcOHGBPS7x8+XKrTDTiZHwSXMli+bTTraiogMwOm#jixYvO1BPKPn#+zNFrPSlpWxXEl0VGVcVbAPtuyLytk6GppaXFEWCU3b59m8ypU6eUca9JcLb44bdOjq7cUmFhCcUOua2tDZpeSc6dO6fVpIZ4ITCKvNdTZKtiV24pLyfmz5##6NGjcJsDdpiSK3iqJ5pk#JUrV9ghUwbix5ElktwqLS2FWVRURMxY+HHhwoVAIJCTk2O#CqHJk08qj4GBAa6W7h0dHSTpG98tXc2bN2#06lZSUnLkyJG8vLwFCxYYIeNIT0#fsWPHw4cPIev+cCTF0gR0wU+fPk1LS+PaLl26RBdePeK7pUq8evUqKxUlhYFyc3MrKys7OztfvnypLpEPfWViQG7xYp2amsqAra2tJOO7xalem4LBoKPEgIrevXv32bNn79+#L2ME5pC1RpELTUD8RGRx9RzpyS90dXV1d3d31KcKc2c7XxaNlnGE2+IhQVnC5CRakSLFkQg3u0bibilDf7kCEjNmSiTulp17cpA8klrEmUMcWcl8GkkGepr09#frG#iErJUrMyULQf+XW8iKdMs8Tot#+plzfd#iXOXsdJlxMJGms25dvnyZU+NWVbCV80OHDsFDFtlZAzNyX7OOd+#e1TaE30TyRhZta7PXr1mz5v3791Kmp8AsgOk5MmlVVRWa+LV+8uQJpwg1sn5v6iTLBuHVq1eczjLOnz+v266srIxTPaLD#2L8+sffNLAtYVtcW1tbN8Ngivr6eipn8+bNzAsyMjIeP36MoLCsoVC4wPEsa63ZE88+#H7#zZs30aDnBZj4z0f47a9#fgmU#+jP+W7msXr16sLCQp5N+u23msbGxv4DFvqP3jnhrXYAAAAASUVORK5CYIIAAAASAP####8BAAD#ABAAAAEAAAABAAAAaQAAAHAAAAASAP####8BAAD#ABAAAAEAAAABAAAAbwAAAGoAAAAHAP####8BAAD#ABAAAAAAAAAAAAAAAEAIAAAAAAAAAAAIAAAAANsAAADcAAAAEwD#####AAJpbQAAAAAAAAABAAAAAQAAAN0AAAADAQAAAN4B####AQAEaW0yMgAAAN0NAAAAAAABAAAAAQAAAAIAAAAAAAAAAAAAAAAyAAAAMgAABqaJUE5HDQoaCgAAAA1JSERSAAAAMgAAADIIAgAAAJFdH+YAAAABc1JHQgCuzhzpAAAABGdBTUEAALGPC#xhBQAAAAlwSFlzAAAOwwAADsMBx2+oZAAABjtJREFUWEfFmVtIVVkcxo#dU5FuQxg1g1lzpAwUkR7KQopRkJno1Eu++FIg2BTj2IM6Mocuk6VQQ5FEpC9jIII9GvTYQ0Q30JIKssuDBF2YLpJ6zOa39ndcno56zt7noPM9bP77v7611re#9d#7rL2P7+usYMwBwRcHBNevX6+oqMjPzy8sLKysrLx16xbJ0dFR0WZDVpQm4oMHD86dO9cXgYULFx49ehROKBSCE0uWhouNMHV6WBqTyYx9+#ZJypIlS8rKykpKShYvXqxMdXU1TJQl6BZzcLRTTgdLQBAgKC8vl4ItW7Y8e#bMYX3t6enJzc1Vvr6+noyRFRod0zQmDoU+ffr08eNHjlOCppGREZixBQGryfokTSxfcXHx0NAQTQyl0QYGBjZu3Chl165dm3Crq6srEAj4#f7vp8cPDjZs2HDjxg2m4Ro08WQYReM+6ZqtTykpKXl5eS9evCDJCByl7Pnz5ytWrICAaN+#g6ODg4N79uxRH5c4ceIEAw0PD08py2qK8glwbQqys7PRAUfXxlDE3Ao0LV261Df6ZWzv3r2i0mf##v0NDQ1#ToNgMEjrmTNnPnz4wFh26SNhFI37BAispq1bt757944RdLpu3TqVF8qQBfn48ePkuSt97e3tIu3atevt27dmYC+QAgurabJP27dv52JEo66VtMpUanBYYow0dwQRPlHONKAa7arE6SDbLcw8DuypY9M3Pm3bto17hQx9AUGksv7+fjInT55Upqamxrds2TKiw4cP06BasSDjHrbLlD5Jk7RC4NoI6urqRNi0aZONser169fmduWEEmYg2PTR6J5gFE3jU5Qm0ayy2traefPmiQkyMzP7+vrI+5Q9duwYJ1DVzRMcSa58EhOIrLtv586dsgZNvb29ZFjlZGXZaYxL8XwSFMut06dPL1q0CDKaHjx4QEb5sCzuTKXUzSWcWcKXztzEnjQ1NzeLbH1SHkLisswkDtm45N2npqYmkaN8EjNBWepMkLxPkzURJCLLdjYuJedT1NqJCTzLsp0n+2SfmTE0xagnMQVvsmxn49K3PrnRFLueCCw8yLKdZ6ieIuFNFkfjkndN7n0S3MpSHp8AccI+xainSHhwS2sHx2pi#+TJJ5eagAe3NFxjYyP8OXPmsCMiI60iCJFkT#UUCVeySDqrZ5avoKAATfTKysq6d+8ereoliKy5vdZTJNy6JUvYfeuXVWCvdufOHfKaD9BdsfVp1apV7tfOwq1batIOG2UHDhxgT0u8fPlyq0w04mR8ElzJYvm0062oqIDMDpv44sWLztQTyj5##szRaz0paVsVxJdFRlXFWwD7bsi8rZOhqaWlxRFglN2+fZvMqVOnlHGvSXC2+OG3To6u3FJhYQnFDrmtrQ2aXknOnTun1aSGeCEwirzXU2SrYlduKS8n5s+f#+jRo3CbA3aYkit4qieaZPyVK1fYIVMG4seRJZLcKi0thVlUVETMWPhx4cKFQCCQk5NjvwqhyZNPKo+BgQGulu4dHR0k6RvfLV3Nmzdv9OpWUlJy5MiRvLy8BQsWGCHjSE9P37Fjx8OHDyHr#nAkxdIEdMFPnz5NS0vj2i5dukQXXj3iu6VKvHr1KisVJYWBcnNzKysrOzs7X758qS6RD31lYkBu8WKdmprKgK2trSTju8WpXpuCwaCjxICK3r1799mzZ+#fvy9jBOaQtUaRC01A#ERkcfUc6ckvdHV1dXd3d9SnCnNnO18WjZZxhNviIUFZwuQkWpEixZEIN7tG4m4pQ3+5AhIzZkok7pade3KQPJJaxJlDHFnJfBpJBnqa9Pf36xv4hKyVKzMlC0H#l1vIinTLPE6Lf#qZc33f4lzl7HSZcTCRprNuXb58mVPjVlWwlfNDhw7BQxbZWQMzcl+zjnfv3tU2hN9E8kYWbWuz169Zs+b9+#dSpqfALIDpOTJpVVUVmvi1fvLkCacINbJ+b+okywbh1atXnM4yzp8#r9uurKyMUz2iw#9i#PrH3zSwLWFbXFtbWzfDYIr6+noqZ#PmzcwLMjIyHj9+jKCwrKFQuMDxLGut2RPPPvx+#82bN9Gg5wWY+M9H+O2vf34JlP#oz#lu5rF69erCwkKeTfrtt5rGxsb+Axb6j9454a12AAAAAElFTkSuQmCCAAAAEgD#####AQAA#wAQAAABAAAAAQAAAG8AAAB2AAAAEgD#####AQAA#wAQAAABAAAAAQAAAHUAAABwAAAABwD#####AQAA#wAQAAAAAAAAAAAAAABACAAAAAAAAAAACAAAAADgAAAA4QAAABMA#####wACaW0AAAAAAAAAAQAAAAEAAADiAAAAAwEAAADjAf###wEABGltMjMAAADiDQAAAAAAAQAAAAEAAAACAAAAAAAAAAAAAAAAMgAAADIAAAamiVBORw0KGgoAAAANSUhEUgAAADIAAAAyCAIAAACRXR#mAAAAAXNSR0IArs4c6QAAAARnQU1BAACxjwv8YQUAAAAJcEhZcwAADsMAAA7DAcdvqGQAAAY7SURBVFhHxZlbSFVZHMaP3VORbkMYNYNZc6QMFJEeykKKUZCZ6NRLvvhSINgU49iDOjKHLpOlUEORRKQvYyCCPRr02ENEN9CSCrLLgwRdmC6Seszmt#Z3XJ6Oes7e56DzPWz++7++tda3v#Xf+6y9j+#rrGDMAcEXBwTXr1+vqKjIz88vLCysrKy8desWydHRUdFmQ1aUJuKDBw#OnTvXF4GFCxcePXoUTigUghNLloaLjTB1elgak8mMffv2ScqSJUvKyspKSkoWL16sTHV1NUyUJegWc3C0U04HS0AQICgvL5eCLVu2PHv2zGF97enpyc3NVb6+vp6MkRUaHdM0Jg6FPn369PHjR45TgqaRkRGYsQUBq8n6JE0sX3Fx8dDQEE0MpdEGBgY2btwoZdeuXZtwq6urKxAI+P3+76fHDw42bNhw48YNpuEaNPFkGEXjPumarU8pKSl5eXkvXrwgyQgcpez58+crVqyAgGjfv4Ojg4ODe#bsUR+XOHHiBAMNDw9PKctqivIJcG0KsrOz0QFH18ZQxNwKNC1dutQ3+mVs7969otJn##79DQ0Nf06DYDBI65kzZz58+MBYdukjYRSN+wQIrKatW7e+e#eOEXS6bt06lRfKkAX5+PHj5Lkrfe3t7SLt2rXr7du3ZmAvkAILq2myT9u3b+diRKOulbTKVGpwWGKMNHcEET5RzjSgGu2qxOkg2y3MPA7sqWPTNz5t27aNe4UMfQFBpLL+#n4yJ0+eVKampsa3bNkyosOHD9OgWrEg4x62y5Q+SZO0QuDaCOrq6kTYtGmTjbHq9evX5nblhBJmINj00eieYBRN41OUJtGsstra2nnz5okJMjMz+#r6yPuUPXbsGCdQ1c0THEmufBITiKy7b+fOnbIGTb29vWRY5WRl2WmMS#F8EhTLrdOnTy9atAgymh48eEBG+bAs7kyl1M0lnFnCl87cxJ40NTc3i2x9Uh5C4rLMJA7ZuOTdp6amJpGjfBIzQVnqTJC8T5M1ESQiy3Y2LiXnU9TaiQk8y7KdJ#tkn5kxNMWoJzEFb7JsZ+PStz650RS7nggsPMiynWeoniLhTRZH45J3Te59EtzKUh6fAHHCPsWop0h4cEtrB8dqYv#kySeXmoAHtzRcY2Mj#Dlz5rAjIiOtIgiRZE#1FAlXskg6q2eWr6CgAE30ysrKunfvHq3qJYisub3WUyTcuiVL2H3rl1Vgr3bnzh3ymg#QXbH1adWqVe7XzsKtW2rSDhtlBw4cYE9LvHz5cqtMNOJkfBJcyWL5tNOtqKiAzA6b+OLFi87UE8o+f#7M0Ws9KWlbFcSXRUZVxVsA+27IvK2ToamlpcURYJTdvn2bzKlTp5Rxr0lwtvjht06OrtxSYWEJxQ65ra0Nml5Jzp07p9WkhnghMIq811Nkq2JXbikvJ+bPn##o0aNwmwN2mJIreKonmmT8lStX2CFTBuLHkSWS3CotLYVZVFREzFj4ceHChUAgkJOTY78KocmTTyqPgYEBrpbuHR0dJOkb3y1dzZs3b#TqVlJScuTIkby8vAULFhgh40hPT9+xY8fDhw8h6#5wJMXSBHTBT58+TUtL49ouXbpEF1494rulSrx69SorFSWFgXJzcysrKzs7O1++fKkukQ99ZWJAbvFinZqayoCtra0k47vFqV6bgsGgo8SAit69e#fZs2fv378vYwTmkLVGkQtNQPxEZHH1HOnJL3R1dXV3d3fUpwpzZztfFo2WcYTb4iFBWcLkJFqRIsWRCDe7RuJuKUN#uQISM2ZKJO6WnXtykDySWsSZQxxZyXwaSQZ6mvT39+sb+ISslSszJQtB#5dbyIp0yzxOi3#6mXN93+Jc5ex0mXEwkaazbl2+fJlT41ZVsJXzQ4cOwUMW2VkDM3Jfs453797VNoTfRPJGFm1rs9evWbPm#fv3UqanwCyA6TkyaVVVFZr4tX7y5AmnCDWyfm#qJMsG4dWrV5zOMs6fP6#brqysjFM9osP#Yvz6x980sC1hW1xbW1s3w2CK+vp6Kmfz5s3MCzIyMh4#foygsKyhULjA8SxrrdkTzz78fv#NmzfRoOcFmPjPR#jtr39+CZT#6M#5buaxevXqwsJCnk367beaxsbG#gMW+o#eOeGtdgAAAABJRU5ErkJgggAAABIA#####wEAAP8AEAAAAQAAAAEAAAB1AAAAfAAAABIA#####wEAAP8AEAAAAQAAAAEAAAB7AAAAdgAAAAcA#####wEAAP8AEAAAAAAAAAAAAAAAQAgAAAAAAAAAAAgAAAAA5QAAAOYAAAATAP####8AAmltAAAAAAAAAAEAAAABAAAA5wAAAAMBAAAA6AH###8BAARpbTI0AAAA5w0AAAAAAAEAAAABAAAAAgAAAAAAAAAAAAAAADIAAAAyAAAGpolQTkcNChoKAAAADUlIRFIAAAAyAAAAMggCAAAAkV0f5gAAAAFzUkdCAK7OHOkAAAAEZ0FNQQAAsY8L#GEFAAAACXBIWXMAAA7DAAAOwwHHb6hkAAAGO0lEQVRYR8WZW0hVWRzGj91TkW5DGDWDWXOkDBSRHspCilGQmejUS774UiDYFOPYgzoyhy6TpVBDkUSkL2Mggj0a9NhDRDfQkgqyy4MEXZguknrM5rf2d1yejnrO3ueg8z1s#vu#vrXWt7#13#usvY#v66xgzAHBFwcE169fr6ioyM#PLywsrKysvHXrFsnR0VHRZkNWlCbigwcPzp071xeBhQsXHj16FE4oFIITS5aGi40wdXpYGpPJjH379knKkiVLysrKSkpKFi9erEx1dTVMlCXoFnNwtFNOB0tAECAoLy+Xgi1btjx79sxhfe3p6cnNzVW+vr6ejJEVGh3TNCYOhT59+vTx40eOU4KmkZERmLEFAavJ+iRNLF9xcfHQ0BBNDKXRBgYGNm7cKGXXrl2bcKurqysQCPj9#u+nxw8ONmzYcOPGDabhGjTxZBhF4z7pmq1PKSkpeXl5L168IMkIHKXs+fPnK1asgIBo37+Do4ODg3v27FEflzhx4gQDDQ8PTynLaoryCXBtCrKzs9EBR9fGUMTcCjQtXbrUN#plbO#evaLSZ##+#Q0NDX9Og2AwSOuZM2c+fPjAWHbpI2EUjfsECKymrVu3vnv3jhF0um7dOpUXypAF+fjx4+S5K33t7e0i7dq16+3bt2ZgL5ACC6tpsk#bt2#nYkSjrpW0ylRqcFhijDR3BBE+Uc40oBrtqsTpINstzDwO7Klj0zc+bdu2jXuFDH0BQaSy#v5+MidPnlSmpqbGt2zZMqLDhw#ToFqxIOMetsuUPkmTtELg2gjq6upE2LRpk42x6vXr1+Z25YQSZiDY9NHonmAUTeNTlCbRrLLa2tp58+aJCTIzM#v6+sj7lD127BgnUNXNExxJrnwSE4isu2#nzp2yBk29vb1kWOVkZdlpjEvxfBIUy63Tp08vWrQIMpoePHhARvmwLO5MpdTNJZxZwpfO3MSeNDU3N4tsfVIeQuKyzCQO2bjk3aempiaRo3wSM0FZ6kyQvE+TNREkIst2Ni4l51PU2okJPMuynSf7ZJ+ZMTTFqCcxBW+ybGfj0rc+udEUu54ILDzIsp1nqJ4i4U0WR+OSd03ufRLcylIenwBxwj7FqKdIeHBLawfHamL#5Mknl5qAB7c0XGNjI#w5c+awIyIjrSIIkWRP9RQJV7JIOqtnlq+goABN9MrKyrp37x6t6iWIrLm91lMk3LolS9h965dVYK92584d8poP0F2x9WnVqlXu187CrVtq0g4bZQcOHGBPS7x8+XKrTDTiZHwSXMli+bTTraiogMwOm#jixYvO1BPKPn#+zNFrPSlpWxXEl0VGVcVbAPtuyLytk6GppaXFEWCU3b59m8ypU6eUca9JcLb44bdOjq7cUmFhCcUOua2tDZpeSc6dO6fVpIZ4ITCKvNdTZKtiV24pLyfmz5##6NGjcJsDdpiSK3iqJ5pk#JUrV9ghUwbix5ElktwqLS2FWVRURMxY+HHhwoVAIJCTk2O#CqHJk08qj4GBAa6W7h0dHSTpG98tXc2bN2#06lZSUnLkyJG8vLwFCxYYIeNIT0#fsWPHw4cPIev+cCTF0gR0wU+fPk1LS+PaLl26RBdePeK7pUq8evUqKxUlhYFyc3MrKys7OztfvnypLpEPfWViQG7xYp2amsqAra2tJOO7xalem4LBoKPEgIrevXv32bNn79+#L2ME5pC1RpELTUD8RGRx9RzpyS90dXV1d3d31KcKc2c7XxaNlnGE2+IhQVnC5CRakSLFkQg3u0bibilDf7kCEjNmSiTulp17cpA8klrEmUMcWcl8GkkGepr09#frG#iErJUrMyULQf+XW8iKdMs8Tot#+plzfd#iXOXsdJlxMJGms25dvnyZU+NWVbCV80OHDsFDFtlZAzNyX7OOd+#e1TaE30TyRhZta7PXr1mz5v3791Kmp8AsgOk5MmlVVRWa+LV+8uQJpwg1sn5v6iTLBuHVq1eczjLOnz+v266srIxTPaLD#2L8+sffNLAtYVtcW1tbN8Ngivr6eipn8+bNzAsyMjIeP36MoLCsoVC4wPEsa63ZE88+#H7#zZs30aDnBZj4z0f47a9#fgmU#+jP+W7msXr16sLCQp5N+u23msbGxv4DFvqP3jnhrXYAAAAASUVORK5CYIIAAAASAP####8BAAD#ABAAAAEAAAABAAAAewAAAIIAAAASAP####8BAAD#ABAAAAEAAAABAAAAgQAAAHwAAAAHAP####8BAAD#ABAAAAAAAAAAAAAAAEAIAAAAAAAAAAAIAAAAAOoAAADrAAAAEwD#####AAJpbQAAAAAAAAABAAAAAQAAAOwAAAADAQAAAO0B####AQAEaW0yNQAAAOwNAAAAAAABAAAAAQAAAAIAAAAAAAAAAAAAAAAyAAAAMgAABqaJUE5HDQoaCgAAAA1JSERSAAAAMgAAADIIAgAAAJFdH+YAAAABc1JHQgCuzhzpAAAABGdBTUEAALGPC#xhBQAAAAlwSFlzAAAOwwAADsMBx2+oZAAABjtJREFUWEfFmVtIVVkcxo#dU5FuQxg1g1lzpAwUkR7KQopRkJno1Eu++FIg2BTj2IM6Mocuk6VQQ5FEpC9jIII9GvTYQ0Q30JIKssuDBF2YLpJ6zOa39ndcno56zt7noPM9bP77v7611re#9d#7rL2P7+usYMwBwRcHBNevX6+oqMjPzy8sLKysrLx16xbJ0dFR0WZDVpQm4oMHD86dO9cXgYULFx49ehROKBSCE0uWhouNMHV6WBqTyYx9+#ZJypIlS8rKykpKShYvXqxMdXU1TJQl6BZzcLRTTgdLQBAgKC8vl4ItW7Y8e#bMYX3t6enJzc1Vvr6+noyRFRod0zQmDoU+ffr08eNHjlOCppGREZixBQGryfokTSxfcXHx0NAQTQyl0QYGBjZu3Chl165dm3Crq6srEAj4#f7vp8cPDjZs2HDjxg2m4Ro08WQYReM+6ZqtTykpKXl5eS9evCDJCByl7Pnz5ytWrICAaN+#g6ODg4N79uxRH5c4ceIEAw0PD08py2qK8glwbQqys7PRAUfXxlDE3Ao0LV261Df6ZWzv3r2i0mf##v0NDQ1#ToNgMEjrmTNnPnz4wFh26SNhFI37BAispq1bt757944RdLpu3TqVF8qQBfn48ePkuSt97e3tIu3atevt27dmYC+QAgurabJP27dv52JEo66VtMpUanBYYow0dwQRPlHONKAa7arE6SDbLcw8DuypY9M3Pm3bto17hQx9AUGksv7+fjInT55Upqamxrds2TKiw4cP06BasSDjHrbLlD5Jk7RC4NoI6urqRNi0aZONser169fmduWEEmYg2PTR6J5gFE3jU5Qm0ayy2traefPmiQkyMzP7+vrI+5Q9duwYJ1DVzRMcSa58EhOIrLtv586dsgZNvb29ZFjlZGXZaYxL8XwSFMut06dPL1q0CDKaHjx4QEb5sCzuTKXUzSWcWcKXztzEnjQ1NzeLbH1SHkLisswkDtm45N2npqYmkaN8EjNBWepMkLxPkzURJCLLdjYuJedT1NqJCTzLsp0n+2SfmTE0xagnMQVvsmxn49K3PrnRFLueCCw8yLKdZ6ieIuFNFkfjkndN7n0S3MpSHp8AccI+xainSHhwS2sHx2pi#+TJJ5eagAe3NFxjYyP8OXPmsCMiI60iCJFkT#UUCVeySDqrZ5avoKAATfTKysq6d+8ereoliKy5vdZTJNy6JUvYfeuXVWCvdufOHfKaD9BdsfVp1apV7tfOwq1batIOG2UHDhxgT0u8fPlyq0w04mR8ElzJYvm0062oqIDMDpv44sWLztQTyj5##szRaz0paVsVxJdFRlXFWwD7bsi8rZOhqaWlxRFglN2+fZvMqVOnlHGvSXC2+OG3To6u3FJhYQnFDrmtrQ2aXknOnTun1aSGeCEwirzXU2SrYlduKS8n5s+f#+jRo3CbA3aYkit4qieaZPyVK1fYIVMG4seRJZLcKi0thVlUVETMWPhx4cKFQCCQk5NjvwqhyZNPKo+BgQGulu4dHR0k6RvfLV3Nmzdv9OpWUlJy5MiRvLy8BQsWGCHjSE9P37Fjx8OHDyHr#nAkxdIEdMFPnz5NS0vj2i5dukQXXj3iu6VKvHr1KisVJYWBcnNzKysrOzs7X758qS6RD31lYkBu8WKdmprKgK2trSTju8WpXpuCwaCjxICK3r1799mzZ+#fvy9jBOaQtUaRC01A#ERkcfUc6ckvdHV1dXd3d9SnCnNnO18WjZZxhNviIUFZwuQkWpEixZEIN7tG4m4pQ3+5AhIzZkok7pade3KQPJJaxJlDHFnJfBpJBnqa9Pf36xv4hKyVKzMlC0H#l1vIinTLPE6Lf#qZc33f4lzl7HSZcTCRprNuXb58mVPjVlWwlfNDhw7BQxbZWQMzcl+zjnfv3tU2hN9E8kYWbWuz169Zs+b9+#dSpqfALIDpOTJpVVUVmvi1fvLkCacINbJ+b+okywbh1atXnM4yzp8#r9uurKyMUz2iw#9i#PrH3zSwLWFbXFtbWzfDYIr6+noqZ#PmzcwLMjIyHj9+jKCwrKFQuMDxLGut2RPPPvx+#82bN9Gg5wWY+M9H+O2vf34JlP#oz#lu5rF69erCwkKeTfrtt5rGxsb+Axb6j9454a12AAAAAElFTkSuQmCCAAAAEgD#####AQAA#wAQAAABAAAAAQAAAIEAAACIAAAAEgD#####AQAA#wAQAAABAAAAAQAAAIcAAACCAAAABwD#####AQAA#wAQAAAAAAAAAAAAAABACAAAAAAAAAAACAAAAADvAAAA8AAAABMA#####wACaW0AAAAAAAAAAQAAAAEAAADxAAAAAwEAAADyAf###wEABGltMjYAAADxDQAAAAAAAQAAAAEAAAACAAAAAAAAAAAAAAAAMgAAADIAAAamiVBORw0KGgoAAAANSUhEUgAAADIAAAAyCAIAAACRXR#mAAAAAXNSR0IArs4c6QAAAARnQU1BAACxjwv8YQUAAAAJcEhZcwAADsMAAA7DAcdvqGQAAAY7SURBVFhHxZlbSFVZHMaP3VORbkMYNYNZc6QMFJEeykKKUZCZ6NRLvvhSINgU49iDOjKHLpOlUEORRKQvYyCCPRr02ENEN9CSCrLLgwRdmC6Seszmt#Z3XJ6Oes7e56DzPWz++7++tda3v#Xf+6y9j+#rrGDMAcEXBwTXr1+vqKjIz88vLCysrKy8desWydHRUdFmQ1aUJuKDBw#OnTvXF4GFCxcePXoUTigUghNLloaLjTB1elgak8mMffv2ScqSJUvKyspKSkoWL16sTHV1NUyUJegWc3C0U04HS0AQICgvL5eCLVu2PHv2zGF97enpyc3NVb6+vp6MkRUaHdM0Jg6FPn369PHjR45TgqaRkRGYsQUBq8n6JE0sX3Fx8dDQEE0MpdEGBgY2btwoZdeuXZtwq6urKxAI+P3+76fHDw42bNhw48YNpuEaNPFkGEXjPumarU8pKSl5eXkvXrwgyQgcpez58+crVqyAgGjfv4Ojg4ODe#bsUR+XOHHiBAMNDw9PKctqivIJcG0KsrOz0QFH18ZQxNwKNC1dutQ3+mVs7969otJn##79DQ0Nf06DYDBI65kzZz58+MBYdukjYRSN+wQIrKatW7e+e#eOEXS6bt06lRfKkAX5+PHj5Lkrfe3t7SLt2rXr7du3ZmAvkAILq2myT9u3b+diRKOulbTKVGpwWGKMNHcEET5RzjSgGu2qxOkg2y3MPA7sqWPTNz5t27aNe4UMfQFBpLL+#n4yJ0+eVKampsa3bNkyosOHD9OgWrEg4x62y5Q+SZO0QuDaCOrq6kTYtGmTjbHq9evX5nblhBJmINj00eieYBRN41OUJtGsstra2nnz5okJMjMz+#r6yPuUPXbsGCdQ1c0THEmufBITiKy7b+fOnbIGTb29vWRY5WRl2WmMS#F8EhTLrdOnTy9atAgymh48eEBG+bAs7kyl1M0lnFnCl87cxJ40NTc3i2x9Uh5C4rLMJA7ZuOTdp6amJpGjfBIzQVnqTJC8T5M1ESQiy3Y2LiXnU9TaiQk8y7KdJ#tkn5kxNMWoJzEFb7JsZ+PStz650RS7nggsPMiynWeoniLhTRZH45J3Te59EtzKUh6fAHHCPsWop0h4cEtrB8dqYv#kySeXmoAHtzRcY2Mj#Dlz5rAjIiOtIgiRZE#1FAlXskg6q2eWr6CgAE30ysrKunfvHq3qJYisub3WUyTcuiVL2H3rl1Vgr3bnzh3ymg#QXbH1adWqVe7XzsKtW2rSDhtlBw4cYE9LvHz5cqtMNOJkfBJcyWL5tNOtqKiAzA6b+OLFi87UE8o+f#7M0Ws9KWlbFcSXRUZVxVsA+27IvK2ToamlpcURYJTdvn2bzKlTp5Rxr0lwtvjht06OrtxSYWEJxQ65ra0Nml5Jzp07p9WkhnghMIq811Nkq2JXbikvJ+bPn##o0aNwmwN2mJIreKonmmT8lStX2CFTBuLHkSWS3CotLYVZVFREzFj4ceHChUAgkJOTY78KocmTTyqPgYEBrpbuHR0dJOkb3y1dzZs3b#TqVlJScuTIkby8vAULFhgh40hPT9+xY8fDhw8h6#5wJMXSBHTBT58+TUtL49ouXbpEF1494rulSrx69SorFSWFgXJzcysrKzs7O1++fKkukQ99ZWJAbvFinZqayoCtra0k47vFqV6bgsGgo8SAit69e#fZs2fv378vYwTmkLVGkQtNQPxEZHH1HOnJL3R1dXV3d3fUpwpzZztfFo2WcYTb4iFBWcLkJFqRIsWRCDe7RuJuKUN#uQISM2ZKJO6WnXtykDySWsSZQxxZyXwaSQZ6mvT39+sb+ISslSszJQtB#5dbyIp0yzxOi3#6mXN93+Jc5ex0mXEwkaazbl2+fJlT41ZVsJXzQ4cOwUMW2VkDM3Jfs453797VNoTfRPJGFm1rs9evWbPm#fv3UqanwCyA6TkyaVVVFZr4tX7y5AmnCDWyfm#qJMsG4dWrV5zOMs6fP6#brqysjFM9osP#Yvz6x980sC1hW1xbW1s3w2CK+vp6Kmfz5s3MCzIyMh4#foygsKyhULjA8SxrrdkTzz78fv#NmzfRoOcFmPjPR#jtr39+CZT#6M#5buaxevXqwsJCnk367beaxsbG#gMW+o#eOeGtdgAAAABJRU5ErkJgggAAABIA#####wEAAP8AEAAAAQAAAAEAAACHAAAAjgAAABIA#####wEAAP8AEAAAAQAAAAEAAACNAAAAiAAAAAcA#####wEAAP8AEAAAAAAAAAAAAAAAQAgAAAAAAAAAAAgAAAAA9AAAAPUAAAATAP####8AAmltAAAAAAAAAAEAAAABAAAA9gAAAAMBAAAA9wH###8BAARpbTI3AAAA9g0AAAAAAAEAAAABAAAAAgAAAAAAAAAAAAAAADIAAAAyAAAGpolQTkcNChoKAAAADUlIRFIAAAAyAAAAMggCAAAAkV0f5gAAAAFzUkdCAK7OHOkAAAAEZ0FNQQAAsY8L#GEFAAAACXBIWXMAAA7DAAAOwwHHb6hkAAAGO0lEQVRYR8WZW0hVWRzGj91TkW5DGDWDWXOkDBSRHspCilGQmejUS774UiDYFOPYgzoyhy6TpVBDkUSkL2Mggj0a9NhDRDfQkgqyy4MEXZguknrM5rf2d1yejnrO3ueg8z1s#vu#vrXWt7#13#usvY#v66xgzAHBFwcE169fr6ioyM#PLywsrKysvHXrFsnR0VHRZkNWlCbigwcPzp071xeBhQsXHj16FE4oFIITS5aGi40wdXpYGpPJjH379knKkiVLysrKSkpKFi9erEx1dTVMlCXoFnNwtFNOB0tAECAoLy+Xgi1btjx79sxhfe3p6cnNzVW+vr6ejJEVGh3TNCYOhT59+vTx40eOU4KmkZERmLEFAavJ+iRNLF9xcfHQ0BBNDKXRBgYGNm7cKGXXrl2bcKurqysQCPj9#u+nxw8ONmzYcOPGDabhGjTxZBhF4z7pmq1PKSkpeXl5L168IMkIHKXs+fPnK1asgIBo37+Do4ODg3v27FEflzhx4gQDDQ8PTynLaoryCXBtCrKzs9EBR9fGUMTcCjQtXbrUN#plbO#evaLSZ##+#Q0NDX9Og2AwSOuZM2c+fPjAWHbpI2EUjfsECKymrVu3vnv3jhF0um7dOpUXypAF+fjx4+S5K33t7e0i7dq16+3bt2ZgL5ACC6tpsk#bt2#nYkSjrpW0ylRqcFhijDR3BBE+Uc40oBrtqsTpINstzDwO7Klj0zc+bdu2jXuFDH0BQaSy#v5+MidPnlSmpqbGt2zZMqLDhw#ToFqxIOMetsuUPkmTtELg2gjq6upE2LRpk42x6vXr1+Z25YQSZiDY9NHonmAUTeNTlCbRrLLa2tp58+aJCTIzM#v6+sj7lD127BgnUNXNExxJrnwSE4isu2#nzp2yBk29vb1kWOVkZdlpjEvxfBIUy63Tp08vWrQIMpoePHhARvmwLO5MpdTNJZxZwpfO3MSeNDU3N4tsfVIeQuKyzCQO2bjk3aempiaRo3wSM0FZ6kyQvE+TNREkIst2Ni4l51PU2okJPMuynSf7ZJ+ZMTTFqCcxBW+ybGfj0rc+udEUu54ILDzIsp1nqJ4i4U0WR+OSd03ufRLcylIenwBxwj7FqKdIeHBLawfHamL#5Mknl5qAB7c0XGNjI#w5c+awIyIjrSIIkWRP9RQJV7JIOqtnlq+goABN9MrKyrp37x6t6iWIrLm91lMk3LolS9h965dVYK92584d8poP0F2x9WnVqlXu187CrVtq0g4bZQcOHGBPS7x8+XKrTDTiZHwSXMli+bTTraiogMwOm#jixYvO1BPKPn#+zNFrPSlpWxXEl0VGVcVbAPtuyLytk6GppaXFEWCU3b59m8ypU6eUca9JcLb44bdOjq7cUmFhCcUOua2tDZpeSc6dO6fVpIZ4ITCKvNdTZKtiV24pLyfmz5##6NGjcJsDdpiSK3iqJ5pk#JUrV9ghUwbix5ElktwqLS2FWVRURMxY+HHhwoVAIJCTk2O#CqHJk08qj4GBAa6W7h0dHSTpG98tXc2bN2#06lZSUnLkyJG8vLwFCxYYIeNIT0#fsWPHw4cPIev+cCTF0gR0wU+fPk1LS+PaLl26RBdePeK7pUq8evUqKxUlhYFyc3MrKys7OztfvnypLpEPfWViQG7xYp2amsqAra2tJOO7xalem4LBoKPEgIrevXv32bNn79+#L2ME5pC1RpELTUD8RGRx9RzpyS90dXV1d3d31KcKc2c7XxaNlnGE2+IhQVnC5CRakSLFkQg3u0bibilDf7kCEjNmSiTulp17cpA8klrEmUMcWcl8GkkGepr09#frG#iErJUrMyULQf+XW8iKdMs8Tot#+plzfd#iXOXsdJlxMJGms25dvnyZU+NWVbCV80OHDsFDFtlZAzNyX7OOd+#e1TaE30TyRhZta7PXr1mz5v3791Kmp8AsgOk5MmlVVRWa+LV+8uQJpwg1sn5v6iTLBuHVq1eczjLOnz+v266srIxTPaLD#2L8+sffNLAtYVtcW1tbN8Ngivr6eipn8+bNzAsyMjIeP36MoLCsoVC4wPEsa63ZE88+#H7#zZs30aDnBZj4z0f47a9#fgmU#+jP+W7msXr16sLCQp5N+u23msbGxv4DFvqP3jnhrXYAAAAASUVORK5CYIIAAAASAP####8BAAD#ABAAAAEAAAABAAAAjQAAAJQAAAASAP####8BAAD#ABAAAAEAAAABAAAAkwAAAI4AAAAHAP####8BAAD#ABAAAAAAAAAAAAAAAEAIAAAAAAAAAAAIAAAAAPkAAAD6AAAAEwD#####AAJpbQAAAAAAAAABAAAAAQAAAPsAAAADAQAAAPwB####AQAEaW0yOAAAAPsNAAAAAAABAAAAAQAAAAIAAAAAAAAAAAAAAAAyAAAAMgAABqaJUE5HDQoaCgAAAA1JSERSAAAAMgAAADIIAgAAAJFdH+YAAAABc1JHQgCuzhzpAAAABGdBTUEAALGPC#xhBQAAAAlwSFlzAAAOwwAADsMBx2+oZAAABjtJREFUWEfFmVtIVVkcxo#dU5FuQxg1g1lzpAwUkR7KQopRkJno1Eu++FIg2BTj2IM6Mocuk6VQQ5FEpC9jIII9GvTYQ0Q30JIKssuDBF2YLpJ6zOa39ndcno56zt7noPM9bP77v7611re#9d#7rL2P7+usYMwBwRcHBNevX6+oqMjPzy8sLKysrLx16xbJ0dFR0WZDVpQm4oMHD86dO9cXgYULFx49ehROKBSCE0uWhouNMHV6WBqTyYx9+#ZJypIlS8rKykpKShYvXqxMdXU1TJQl6BZzcLRTTgdLQBAgKC8vl4ItW7Y8e#bMYX3t6enJzc1Vvr6+noyRFRod0zQmDoU+ffr08eNHjlOCppGREZixBQGryfokTSxfcXHx0NAQTQyl0QYGBjZu3Chl165dm3Crq6srEAj4#f7vp8cPDjZs2HDjxg2m4Ro08WQYReM+6ZqtTykpKXl5eS9evCDJCByl7Pnz5ytWrICAaN+#g6ODg4N79uxRH5c4ceIEAw0PD08py2qK8glwbQqys7PRAUfXxlDE3Ao0LV261Df6ZWzv3r2i0mf##v0NDQ1#ToNgMEjrmTNnPnz4wFh26SNhFI37BAispq1bt757944RdLpu3TqVF8qQBfn48ePkuSt97e3tIu3atevt27dmYC+QAgurabJP27dv52JEo66VtMpUanBYYow0dwQRPlHONKAa7arE6SDbLcw8DuypY9M3Pm3bto17hQx9AUGksv7+fjInT55Upqamxrds2TKiw4cP06BasSDjHrbLlD5Jk7RC4NoI6urqRNi0aZONser169fmduWEEmYg2PTR6J5gFE3jU5Qm0ayy2traefPmiQkyMzP7+vrI+5Q9duwYJ1DVzRMcSa58EhOIrLtv586dsgZNvb29ZFjlZGXZaYxL8XwSFMut06dPL1q0CDKaHjx4QEb5sCzuTKXUzSWcWcKXztzEnjQ1NzeLbH1SHkLisswkDtm45N2npqYmkaN8EjNBWepMkLxPkzURJCLLdjYuJedT1NqJCTzLsp0n+2SfmTE0xagnMQVvsmxn49K3PrnRFLueCCw8yLKdZ6ieIuFNFkfjkndN7n0S3MpSHp8AccI+xainSHhwS2sHx2pi#+TJJ5eagAe3NFxjYyP8OXPmsCMiI60iCJFkT#UUCVeySDqrZ5avoKAATfTKysq6d+8ereoliKy5vdZTJNy6JUvYfeuXVWCvdufOHfKaD9BdsfVp1apV7tfOwq1batIOG2UHDhxgT0u8fPlyq0w04mR8ElzJYvm0062oqIDMDpv44sWLztQTyj5##szRaz0paVsVxJdFRlXFWwD7bsi8rZOhqaWlxRFglN2+fZvMqVOnlHGvSXC2+OG3To6u3FJhYQnFDrmtrQ2aXknOnTun1aSGeCEwirzXU2SrYlduKS8n5s+f#+jRo3CbA3aYkit4qieaZPyVK1fYIVMG4seRJZLcKi0thVlUVETMWPhx4cKFQCCQk5NjvwqhyZNPKo+BgQGulu4dHR0k6RvfLV3Nmzdv9OpWUlJy5MiRvLy8BQsWGCHjSE9P37Fjx8OHDyHr#nAkxdIEdMFPnz5NS0vj2i5dukQXXj3iu6VKvHr1KisVJYWBcnNzKysrOzs7X758qS6RD31lYkBu8WKdmprKgK2trSTju8WpXpuCwaCjxICK3r1799mzZ+#fvy9jBOaQtUaRC01A#ERkcfUc6ckvdHV1dXd3d9SnCnNnO18WjZZxhNviIUFZwuQkWpEixZEIN7tG4m4pQ3+5AhIzZkok7pade3KQPJJaxJlDHFnJfBpJBnqa9Pf36xv4hKyVKzMlC0H#l1vIinTLPE6Lf#qZc33f4lzl7HSZcTCRprNuXb58mVPjVlWwlfNDhw7BQxbZWQMzcl+zjnfv3tU2hN9E8kYWbWuz169Zs+b9+#dSpqfALIDpOTJpVVUVmvi1fvLkCacINbJ+b+okywbh1atXnM4yzp8#r9uurKyMUz2iw#9i#PrH3zSwLWFbXFtbWzfDYIr6+noqZ#PmzcwLMjIyHj9+jKCwrKFQuMDxLGut2RPPPvx+#82bN9Gg5wWY+M9H+O2vf34JlP#oz#lu5rF69erCwkKeTfrtt5rGxsb+Axb6j9454a12AAAAAElFTkSuQmCCAAAAEgD#####AQAA#wAQAAABAAAAAQAAAJMAAACaAAAAEgD#####AQAA#wAQAAABAAAAAQAAAJkAAACUAAAABwD#####AQAA#wAQAAAAAAAAAAAAAABACAAAAAAAAAAACAAAAAD+AAAA#wAAABMA#####wACaW0AAAAAAAAAAQAAAAEAAAEAAAAAAwEAAAEBAf###wEABGltMjkAAAEADQAAAAAAAQAAAAEAAAACAAAAAAAAAAAAAAAAMgAAADIAAAamiVBORw0KGgoAAAANSUhEUgAAADIAAAAyCAIAAACRXR#mAAAAAXNSR0IArs4c6QAAAARnQU1BAACxjwv8YQUAAAAJcEhZcwAADsMAAA7DAcdvqGQAAAY7SURBVFhHxZlbSFVZHMaP3VORbkMYNYNZc6QMFJEeykKKUZCZ6NRLvvhSINgU49iDOjKHLpOlUEORRKQvYyCCPRr02ENEN9CSCrLLgwRdmC6Seszmt#Z3XJ6Oes7e56DzPWz++7++tda3v#Xf+6y9j+#rrGDMAcEXBwTXr1+vqKjIz88vLCysrKy8desWydHRUdFmQ1aUJuKDBw#OnTvXF4GFCxcePXoUTigUghNLloaLjTB1elgak8mMffv2ScqSJUvKyspKSkoWL16sTHV1NUyUJegWc3C0U04HS0AQICgvL5eCLVu2PHv2zGF97enpyc3NVb6+vp6MkRUaHdM0Jg6FPn369PHjR45TgqaRkRGYsQUBq8n6JE0sX3Fx8dDQEE0MpdEGBgY2btwoZdeuXZtwq6urKxAI+P3+76fHDw42bNhw48YNpuEaNPFkGEXjPumarU8pKSl5eXkvXrwgyQgcpez58+crVqyAgGjfv4Ojg4ODe#bsUR+XOHHiBAMNDw9PKctqivIJcG0KsrOz0QFH18ZQxNwKNC1dutQ3+mVs7969otJn##79DQ0Nf06DYDBI65kzZz58+MBYdukjYRSN+wQIrKatW7e+e#eOEXS6bt06lRfKkAX5+PHj5Lkrfe3t7SLt2rXr7du3ZmAvkAILq2myT9u3b+diRKOulbTKVGpwWGKMNHcEET5RzjSgGu2qxOkg2y3MPA7sqWPTNz5t27aNe4UMfQFBpLL+#n4yJ0+eVKampsa3bNkyosOHD9OgWrEg4x62y5Q+SZO0QuDaCOrq6kTYtGmTjbHq9evX5nblhBJmINj00eieYBRN41OUJtGsstra2nnz5okJMjMz+#r6yPuUPXbsGCdQ1c0THEmufBITiKy7b+fOnbIGTb29vWRY5WRl2WmMS#F8EhTLrdOnTy9atAgymh48eEBG+bAs7kyl1M0lnFnCl87cxJ40NTc3i2x9Uh5C4rLMJA7ZuOTdp6amJpGjfBIzQVnqTJC8T5M1ESQiy3Y2LiXnU9TaiQk8y7KdJ#tkn5kxNMWoJzEFb7JsZ+PStz650RS7nggsPMiynWeoniLhTRZH45J3Te59EtzKUh6fAHHCPsWop0h4cEtrB8dqYv#kySeXmoAHtzRcY2Mj#Dlz5rAjIiOtIgiRZE#1FAlXskg6q2eWr6CgAE30ysrKunfvHq3qJYisub3WUyTcuiVL2H3rl1Vgr3bnzh3ymg#QXbH1adWqVe7XzsKtW2rSDhtlBw4cYE9LvHz5cqtMNOJkfBJcyWL5tNOtqKiAzA6b+OLFi87UE8o+f#7M0Ws9KWlbFcSXRUZVxVsA+27IvK2ToamlpcURYJTdvn2bzKlTp5Rxr0lwtvjht06OrtxSYWEJxQ65ra0Nml5Jzp07p9WkhnghMIq811Nkq2JXbikvJ+bPn##o0aNwmwN2mJIreKonmmT8lStX2CFTBuLHkSWS3CotLYVZVFREzFj4ceHChUAgkJOTY78KocmTTyqPgYEBrpbuHR0dJOkb3y1dzZs3b#TqVlJScuTIkby8vAULFhgh40hPT9+xY8fDhw8h6#5wJMXSBHTBT58+TUtL49ouXbpEF1494rulSrx69SorFSWFgXJzcysrKzs7O1++fKkukQ99ZWJAbvFinZqayoCtra0k47vFqV6bgsGgo8SAit69e#fZs2fv378vYwTmkLVGkQtNQPxEZHH1HOnJL3R1dXV3d3fUpwpzZztfFo2WcYTb4iFBWcLkJFqRIsWRCDe7RuJuKUN#uQISM2ZKJO6WnXtykDySWsSZQxxZyXwaSQZ6mvT39+sb+ISslSszJQtB#5dbyIp0yzxOi3#6mXN93+Jc5ex0mXEwkaazbl2+fJlT41ZVsJXzQ4cOwUMW2VkDM3Jfs453797VNoTfRPJGFm1rs9evWbPm#fv3UqanwCyA6TkyaVVVFZr4tX7y5AmnCDWyfm#qJMsG4dWrV5zOMs6fP6#brqysjFM9osP#Yvz6x980sC1hW1xbW1s3w2CK+vp6Kmfz5s3MCzIyMh4#foygsKyhULjA8SxrrdkTzz78fv#NmzfRoOcFmPjPR#jtr39+CZT#6M#5buaxevXqwsJCnk367beaxsbG#gMW+o#eOeGtdgAAAABJRU5ErkJgggAAABIA#####wEAAP8AEAAAAQAAAAEAAACZAAAAoAAAABIA#####wEAAP8AEAAAAQAAAAEAAACfAAAAmgAAAAcA#####wEAAP8AEAAAAAAAAAAAAAAAQAgAAAAAAAAAAAgAAAABAwAAAQQAAAATAP####8AAmltAAAAAAAAAAEAAAABAAABBQAAAAMBAAABBgH###8BAAVpbTIxMAAAAQUNAAAAAAABAAAAAQAAAAIAAAAAAAAAAAAAAAAyAAAAMgAABqaJUE5HDQoaCgAAAA1JSERSAAAAMgAAADIIAgAAAJFdH+YAAAABc1JHQgCuzhzpAAAABGdBTUEAALGPC#xhBQAAAAlwSFlzAAAOwwAADsMBx2+oZAAABjtJREFUWEfFmVtIVVkcxo#dU5FuQxg1g1lzpAwUkR7KQopRkJno1Eu++FIg2BTj2IM6Mocuk6VQQ5FEpC9jIII9GvTYQ0Q30JIKssuDBF2YLpJ6zOa39ndcno56zt7noPM9bP77v7611re#9d#7rL2P7+usYMwBwRcHBNevX6+oqMjPzy8sLKysrLx16xbJ0dFR0WZDVpQm4oMHD86dO9cXgYULFx49ehROKBSCE0uWhouNMHV6WBqTyYx9+#ZJypIlS8rKykpKShYvXqxMdXU1TJQl6BZzcLRTTgdLQBAgKC8vl4ItW7Y8e#bMYX3t6enJzc1Vvr6+noyRFRod0zQmDoU+ffr08eNHjlOCppGREZixBQGryfokTSxfcXHx0NAQTQyl0QYGBjZu3Chl165dm3Crq6srEAj4#f7vp8cPDjZs2HDjxg2m4Ro08WQYReM+6ZqtTykpKXl5eS9evCDJCByl7Pnz5ytWrICAaN+#g6ODg4N79uxRH5c4ceIEAw0PD08py2qK8glwbQqys7PRAUfXxlDE3Ao0LV261Df6ZWzv3r2i0mf##v0NDQ1#ToNgMEjrmTNnPnz4wFh26SNhFI37BAispq1bt757944RdLpu3TqVF8qQBfn48ePkuSt97e3tIu3atevt27dmYC+QAgurabJP27dv52JEo66VtMpUanBYYow0dwQRPlHONKAa7arE6SDbLcw8DuypY9M3Pm3bto17hQx9AUGksv7+fjInT55Upqamxrds2TKiw4cP06BasSDjHrbLlD5Jk7RC4NoI6urqRNi0aZONser169fmduWEEmYg2PTR6J5gFE3jU5Qm0ayy2traefPmiQkyMzP7+vrI+5Q9duwYJ1DVzRMcSa58EhOIrLtv586dsgZNvb29ZFjlZGXZaYxL8XwSFMut06dPL1q0CDKaHjx4QEb5sCzuTKXUzSWcWcKXztzEnjQ1NzeLbH1SHkLisswkDtm45N2npqYmkaN8EjNBWepMkLxPkzURJCLLdjYuJedT1NqJCTzLsp0n+2SfmTE0xagnMQVvsmxn49K3PrnRFLueCCw8yLKdZ6ieIuFNFkfjkndN7n0S3MpSHp8AccI+xainSHhwS2sHx2pi#+TJJ5eagAe3NFxjYyP8OXPmsCMiI60iCJFkT#UUCVeySDqrZ5avoKAATfTKysq6d+8ereoliKy5vdZTJNy6JUvYfeuXVWCvdufOHfKaD9BdsfVp1apV7tfOwq1batIOG2UHDhxgT0u8fPlyq0w04mR8ElzJYvm0062oqIDMDpv44sWLztQTyj5##szRaz0paVsVxJdFRlXFWwD7bsi8rZOhqaWlxRFglN2+fZvMqVOnlHGvSXC2+OG3To6u3FJhYQnFDrmtrQ2aXknOnTun1aSGeCEwirzXU2SrYlduKS8n5s+f#+jRo3CbA3aYkit4qieaZPyVK1fYIVMG4seRJZLcKi0thVlUVETMWPhx4cKFQCCQk5NjvwqhyZNPKo+BgQGulu4dHR0k6RvfLV3Nmzdv9OpWUlJy5MiRvLy8BQsWGCHjSE9P37Fjx8OHDyHr#nAkxdIEdMFPnz5NS0vj2i5dukQXXj3iu6VKvHr1KisVJYWBcnNzKysrOzs7X758qS6RD31lYkBu8WKdmprKgK2trSTju8WpXpuCwaCjxICK3r1799mzZ+#fvy9jBOaQtUaRC01A#ERkcfUc6ckvdHV1dXd3d9SnCnNnO18WjZZxhNviIUFZwuQkWpEixZEIN7tG4m4pQ3+5AhIzZkok7pade3KQPJJaxJlDHFnJfBpJBnqa9Pf36xv4hKyVKzMlC0H#l1vIinTLPE6Lf#qZc33f4lzl7HSZcTCRprNuXb58mVPjVlWwlfNDhw7BQxbZWQMzcl+zjnfv3tU2hN9E8kYWbWuz169Zs+b9+#dSpqfALIDpOTJpVVUVmvi1fvLkCacINbJ+b+okywbh1atXnM4yzp8#r9uurKyMUz2iw#9i#PrH3zSwLWFbXFtbWzfDYIr6+noqZ#PmzcwLMjIyHj9+jKCwrKFQuMDxLGut2RPPPvx+#82bN9Gg5wWY+M9H+O2vf34JlP#oz#lu5rF69erCwkKeTfrtt5rGxsb+Axb6j9454a12AAAAAElFTkSuQmCCAAAAEgD#####AQAA#wAQAAABAAAAAQAAAJ8AAACmAAAAEgD#####AQAA#wAQAAABAAAAAQAAAKUAAACgAAAABwD#####AQAA#wAQAAAAAAAAAAAAAABACAAAAAAAAAAACAAAAAEIAAABCQAAABMA#####wACaW0AAAAAAAAAAQAAAAEAAAEKAAAAAwEAAAELAf###wEABWltMjExAAABCg0AAAAAAAEAAAABAAAAAgAAAAAAAAAAAAAAADIAAAAyAAAGpolQTkcNChoKAAAADUlIRFIAAAAyAAAAMggCAAAAkV0f5gAAAAFzUkdCAK7OHOkAAAAEZ0FNQQAAsY8L#GEFAAAACXBIWXMAAA7DAAAOwwHHb6hkAAAGO0lEQVRYR8WZW0hVWRzGj91TkW5DGDWDWXOkDBSRHspCilGQmejUS774UiDYFOPYgzoyhy6TpVBDkUSkL2Mggj0a9NhDRDfQkgqyy4MEXZguknrM5rf2d1yejnrO3ueg8z1s#vu#vrXWt7#13#usvY#v66xgzAHBFwcE169fr6ioyM#PLywsrKysvHXrFsnR0VHRZkNWlCbigwcPzp071xeBhQsXHj16FE4oFIITS5aGi40wdXpYGpPJjH379knKkiVLysrKSkpKFi9erEx1dTVMlCXoFnNwtFNOB0tAECAoLy+Xgi1btjx79sxhfe3p6cnNzVW+vr6ejJEVGh3TNCYOhT59+vTx40eOU4KmkZERmLEFAavJ+iRNLF9xcfHQ0BBNDKXRBgYGNm7cKGXXrl2bcKurqysQCPj9#u+nxw8ONmzYcOPGDabhGjTxZBhF4z7pmq1PKSkpeXl5L168IMkIHKXs+fPnK1asgIBo37+Do4ODg3v27FEflzhx4gQDDQ8PTynLaoryCXBtCrKzs9EBR9fGUMTcCjQtXbrUN#plbO#evaLSZ##+#Q0NDX9Og2AwSOuZM2c+fPjAWHbpI2EUjfsECKymrVu3vnv3jhF0um7dOpUXypAF+fjx4+S5K33t7e0i7dq16+3bt2ZgL5ACC6tpsk#bt2#nYkSjrpW0ylRqcFhijDR3BBE+Uc40oBrtqsTpINstzDwO7Klj0zc+bdu2jXuFDH0BQaSy#v5+MidPnlSmpqbGt2zZMqLDhw#ToFqxIOMetsuUPkmTtELg2gjq6upE2LRpk42x6vXr1+Z25YQSZiDY9NHonmAUTeNTlCbRrLLa2tp58+aJCTIzM#v6+sj7lD127BgnUNXNExxJrnwSE4isu2#nzp2yBk29vb1kWOVkZdlpjEvxfBIUy63Tp08vWrQIMpoePHhARvmwLO5MpdTNJZxZwpfO3MSeNDU3N4tsfVIeQuKyzCQO2bjk3aempiaRo3wSM0FZ6kyQvE+TNREkIst2Ni4l51PU2okJPMuynSf7ZJ+ZMTTFqCcxBW+ybGfj0rc+udEUu54ILDzIsp1nqJ4i4U0WR+OSd03ufRLcylIenwBxwj7FqKdIeHBLawfHamL#5Mknl5qAB7c0XGNjI#w5c+awIyIjrSIIkWRP9RQJV7JIOqtnlq+goABN9MrKyrp37x6t6iWIrLm91lMk3LolS9h965dVYK92584d8poP0F2x9WnVqlXu187CrVtq0g4bZQcOHGBPS7x8+XKrTDTiZHwSXMli+bTTraiogMwOm#jixYvO1BPKPn#+zNFrPSlpWxXEl0VGVcVbAPtuyLytk6GppaXFEWCU3b59m8ypU6eUca9JcLb44bdOjq7cUmFhCcUOua2tDZpeSc6dO6fVpIZ4ITCKvNdTZKtiV24pLyfmz5##6NGjcJsDdpiSK3iqJ5pk#JUrV9ghUwbix5ElktwqLS2FWVRURMxY+HHhwoVAIJCTk2O#CqHJk08qj4GBAa6W7h0dHSTpG98tXc2bN2#06lZSUnLkyJG8vLwFCxYYIeNIT0#fsWPHw4cPIev+cCTF0gR0wU+fPk1LS+PaLl26RBdePeK7pUq8evUqKxUlhYFyc3MrKys7OztfvnypLpEPfWViQG7xYp2amsqAra2tJOO7xalem4LBoKPEgIrevXv32bNn79+#L2ME5pC1RpELTUD8RGRx9RzpyS90dXV1d3d31KcKc2c7XxaNlnGE2+IhQVnC5CRakSLFkQg3u0bibilDf7kCEjNmSiTulp17cpA8klrEmUMcWcl8GkkGepr09#frG#iErJUrMyULQf+XW8iKdMs8Tot#+plzfd#iXOXsdJlxMJGms25dvnyZU+NWVbCV80OHDsFDFtlZAzNyX7OOd+#e1TaE30TyRhZta7PXr1mz5v3791Kmp8AsgOk5MmlVVRWa+LV+8uQJpwg1sn5v6iTLBuHVq1eczjLOnz+v266srIxTPaLD#2L8+sffNLAtYVtcW1tbN8Ngivr6eipn8+bNzAsyMjIeP36MoLCsoVC4wPEsa63ZE88+#H7#zZs30aDnBZj4z0f47a9#fgmU#+jP+W7msXr16sLCQp5N+u23msbGxv4DFvqP3jnhrXYAAAAASUVORK5CYIIAAAASAP####8BAAD#ABAAAAEAAAABAAAApQAAAKwAAAASAP####8BAAD#ABAAAAEAAAABAAAAqwAAAKYAAAAHAP####8BAAD#ABAAAAAAAAAAAAAAAEAIAAAAAAAAAAAIAAAAAQ0AAAEOAAAAEwD#####AAJpbQAAAAAAAAABAAAAAQAAAQ8AAAADAQAAARAB####AQAFaW0yMTIAAAEPDQAAAAAAAQAAAAEAAAACAAAAAAAAAAAAAAAAMgAAADIAAAamiVBORw0KGgoAAAANSUhEUgAAADIAAAAyCAIAAACRXR#mAAAAAXNSR0IArs4c6QAAAARnQU1BAACxjwv8YQUAAAAJcEhZcwAADsMAAA7DAcdvqGQAAAY7SURBVFhHxZlbSFVZHMaP3VORbkMYNYNZc6QMFJEeykKKUZCZ6NRLvvhSINgU49iDOjKHLpOlUEORRKQvYyCCPRr02ENEN9CSCrLLgwRdmC6Seszmt#Z3XJ6Oes7e56DzPWz++7++tda3v#Xf+6y9j+#rrGDMAcEXBwTXr1+vqKjIz88vLCysrKy8desWydHRUdFmQ1aUJuKDBw#OnTvXF4GFCxcePXoUTigUghNLloaLjTB1elgak8mMffv2ScqSJUvKyspKSkoWL16sTHV1NUyUJegWc3C0U04HS0AQICgvL5eCLVu2PHv2zGF97enpyc3NVb6+vp6MkRUaHdM0Jg6FPn369PHjR45TgqaRkRGYsQUBq8n6JE0sX3Fx8dDQEE0MpdEGBgY2btwoZdeuXZtwq6urKxAI+P3+76fHDw42bNhw48YNpuEaNPFkGEXjPumarU8pKSl5eXkvXrwgyQgcpez58+crVqyAgGjfv4Ojg4ODe#bsUR+XOHHiBAMNDw9PKctqivIJcG0KsrOz0QFH18ZQxNwKNC1dutQ3+mVs7969otJn##79DQ0Nf06DYDBI65kzZz58+MBYdukjYRSN+wQIrKatW7e+e#eOEXS6bt06lRfKkAX5+PHj5Lkrfe3t7SLt2rXr7du3ZmAvkAILq2myT9u3b+diRKOulbTKVGpwWGKMNHcEET5RzjSgGu2qxOkg2y3MPA7sqWPTNz5t27aNe4UMfQFBpLL+#n4yJ0+eVKampsa3bNkyosOHD9OgWrEg4x62y5Q+SZO0QuDaCOrq6kTYtGmTjbHq9evX5nblhBJmINj00eieYBRN41OUJtGsstra2nnz5okJMjMz+#r6yPuUPXbsGCdQ1c0THEmufBITiKy7b+fOnbIGTb29vWRY5WRl2WmMS#F8EhTLrdOnTy9atAgymh48eEBG+bAs7kyl1M0lnFnCl87cxJ40NTc3i2x9Uh5C4rLMJA7ZuOTdp6amJpGjfBIzQVnqTJC8T5M1ESQiy3Y2LiXnU9TaiQk8y7KdJ#tkn5kxNMWoJzEFb7JsZ+PStz650RS7nggsPMiynWeoniLhTRZH45J3Te59EtzKUh6fAHHCPsWop0h4cEtrB8dqYv#kySeXmoAHtzRcY2Mj#Dlz5rAjIiOtIgiRZE#1FAlXskg6q2eWr6CgAE30ysrKunfvHq3qJYisub3WUyTcuiVL2H3rl1Vgr3bnzh3ymg#QXbH1adWqVe7XzsKtW2rSDhtlBw4cYE9LvHz5cqtMNOJkfBJcyWL5tNOtqKiAzA6b+OLFi87UE8o+f#7M0Ws9KWlbFcSXRUZVxVsA+27IvK2ToamlpcURYJTdvn2bzKlTp5Rxr0lwtvjht06OrtxSYWEJxQ65ra0Nml5Jzp07p9WkhnghMIq811Nkq2JXbikvJ+bPn##o0aNwmwN2mJIreKonmmT8lStX2CFTBuLHkSWS3CotLYVZVFREzFj4ceHChUAgkJOTY78KocmTTyqPgYEBrpbuHR0dJOkb3y1dzZs3b#TqVlJScuTIkby8vAULFhgh40hPT9+xY8fDhw8h6#5wJMXSBHTBT58+TUtL49ouXbpEF1494rulSrx69SorFSWFgXJzcysrKzs7O1++fKkukQ99ZWJAbvFinZqayoCtra0k47vFqV6bgsGgo8SAit69e#fZs2fv378vYwTmkLVGkQtNQPxEZHH1HOnJL3R1dXV3d3fUpwpzZztfFo2WcYTb4iFBWcLkJFqRIsWRCDe7RuJuKUN#uQISM2ZKJO6WnXtykDySWsSZQxxZyXwaSQZ6mvT39+sb+ISslSszJQtB#5dbyIp0yzxOi3#6mXN93+Jc5ex0mXEwkaazbl2+fJlT41ZVsJXzQ4cOwUMW2VkDM3Jfs453797VNoTfRPJGFm1rs9evWbPm#fv3UqanwCyA6TkyaVVVFZr4tX7y5AmnCDWyfm#qJMsG4dWrV5zOMs6fP6#brqysjFM9osP#Yvz6x980sC1hW1xbW1s3w2CK+vp6Kmfz5s3MCzIyMh4#foygsKyhULjA8SxrrdkTzz78fv#NmzfRoOcFmPjPR#jtr39+CZT#6M#5buaxevXqwsJCnk367beaxsbG#gMW+o#eOeGtdgAAAABJRU5ErkJgggAAAAsA#####wEAAP8AEAAAAAAAAAAAAAAAQAgAAAAAAAAAAAgAAUBiUAAAAAAAQHlnrhR64UgAAAABAP####8BAAD#AQAAAAABEhAAAAAAAAEAAAABAAAAAgAAAAAAAAAAAAFBAAAAEwD#####AANjb20AAAAAAAAAAQAAAAEAAADdAAAAAQEAAAEUAAAA#wEABWFmZjIyAAAA3RAAAAAAAAEAAAABAAAAAgAAAAAAAAAAAAFBAAAAEwD#####AANjb20AAAAAAAAAAQAAAAEAAADiAAAAAQEAAAEWAAAA#wEABWFmZjIzAAAA4hAAAAAAAAEAAAABAAAAAgAAAAAAAAAAAAFBAAAAEwD#####AANjb20AAAAAAAAAAQAAAAEAAADYAAAAAQEAAAEYAAAA#wEABWFmZjIxAAAA2BAAAAAAAAEAAAABAAAAAgAAAAAAAAAAAAFBAAAAEwD#####AANjb20AAAAAAAAAAQAAAAEAAADnAAAAAQEAAAEaAAAA#wEABWFmZjI0AAAA5xAAAAAAAAEAAAABAAAAAgAAAAAAAAAAAAFBAAAAEwD#####AANjb20AAAAAAAAAAQAAAAEAAADsAAAAAQEAAAEcAAAA#wEABWFmZjI1AAAA7BAAAAAAAAEAAAABAAAAAgAAAAAAAAAAAAFBAAAAEwD#####AANjb20AAAAAAAAAAQAAAAEAAADxAAAAAQEAAAEeAAAA#wEABWFmZjI2AAAA8RAAAAAAAAEAAAABAAAAAgAAAAAAAAAAAAFBAAAAEwD#####AANjb20AAAAAAAAAAQAAAAEAAAD2AAAAAQEAAAEgAAAA#wEABWFmZjI3AAAA9hAAAAAAAAEAAAABAAAAAgAAAAAAAAAAAAFBAAAAEwD#####AANjb20AAAAAAAAAAQAAAAEAAAD7AAAAAQEAAAEiAAAA#wEABWFmZjI4AAAA+xAAAAAAAAEAAAABAAAAAgAAAAAAAAAAAAFBAAAAEwD#####AANjb20AAAAAAAAAAQAAAAEAAAEAAAAAAQEAAAEkAAAA#wEABWFmZjI5AAABABAAAAAAAAEAAAABAAAAAgAAAAAAAAAAAAFBAAAAEwD#####AANjb20AAAAAAAAAAQAAAAEAAAEFAAAAAQEAAAEmAAAA#wEABmFmZjIxMAAAAQUQAAAAAAABAAAAAQAAAAIAAAAAAAAAAAABQQAAABMA#####wADY29tAAAAAAAAAAEAAAABAAABCgAAAAEBAAABKAAAAP8BAAZhZmYyMTEAAAEKEAAAAAAAAQAAAAEAAAACAAAAAAAAAAAAAUEAAAATAP####8AA2NvbQAAAAAAAAABAAAAAQAAAQ8AAAABAQAAASoAAAD#AQAGYWZmMjEyAAABDxAAAAAAAAEAAAABAAAAAgAAAAAAAAAAAAFBAAAAEwD#####AANjb20AAAAAAAAAAQAAAAEAAAC1AAAAAQEAAAEsAAAA#wEABWFmZjExAAAAtRAAAAAAAAEAAAABAAAAAgAAAAAAAAAAAAFBAAAAEwD#####AANjb20AAAAAAAAAAQAAAAEAAAC6AAAAAQEAAAEuAAAAAAEABWFmZjEyAAAAuhAAAAAAAAEAAAABAAAAAgAAAAAAAAAAAAFBAAAAEwD#####AANjb20AAAAAAAAAAQAAAAEAAAC#AAAAAQEAAAEwAAAAAAEABWFmZjEzAAAAvxAAAAAAAAEAAAABAAAAAgAAAAAAAAAAAAFBAAAAEwD#####AANjb20AAAAAAAAAAQAAAAEAAADEAAAAAQEAAAEyAAAA#wEABWFmZjE0AAAAxBAAAAAAAAEAAAABAAAAAgAAAAAAAAAAAAFBAAAAEwD#####AANjb20AAAAAAAAAAQAAAAEAAADJAAAAAQEAAAE0AAAA#wEABWFmZjE1AAAAyRAAAAAAAAEAAAABAAAAAgAAAAAAAAAAAAFBAAAAEwD#####AANjb20AAAAAAAAAAQAAAAEAAADOAAAAAQEAAAE2AAAA#wEABWFmZjE2AAAAzhAAAAAAAAEAAAABAAAAAgAAAAAAAAAAAAFBAAAAEwD#####AANjb20AAAAAAAAAAQAAAAEAAADTAAAAAQEAAAE4AAAA#wEABWFmZjE3AAAA0xAAAAAAAAEAAAABAAAAAgAAAAAAAAAAAAFBAAAAEwD#####AANjb20AAAAAAAAAAQAAAAEAAACxAAAAAQEAAAE6AAAA#wEABGFmZjAAAACxEAAAAAAAAQAAAAEAAAACAAAAAAAAAAAAAUEAAAAEAP####8BAAAAAARjZXIwAAEAAACxAAAAAj#wAAAAAAAAAAAAABMA#####wAEYWNlcgAAAAAAAAABAAAAAQAAALUAAAAEAQAAAT0BAAAAAAVjZXIxMQABAAAAtQAAAAI#8AAAAAAAAAAAAAATAP####8ABGFjZXIAAAAAAAAAAQAAAAEAAAC6AAAABAEAAAE#AQAAAAAFY2VyMTIAAQAAALoAAAACP#AAAAAAAAAAAAAAEwD#####AARhY2VyAAAAAAAAAAEAAAABAAAAvwAAAAQBAAABQQEAAAAABWNlcjEzAAEAAAC#AAAAAj#wAAAAAAAAAAAAABMA#####wAEYWNlcgAAAAAAAAABAAAAAQAAAMQAAAAEAQAAAUMBAAAAAAVjZXIxNAABAAAAxAAAAAI#8AAAAAAAAAAAAAATAP####8ABGFjZXIAAAAAAAAAAQAAAAEAAADJAAAABAEAAAFFAQAAAAAFY2VyMTUAAQAAAMkAAAACP#AAAAAAAAAAAAAAEwD#####AARhY2VyAAAAAAAAAAEAAAABAAAAzgAAAAQBAAABRwEAAAAABWNlcjE2AAEAAADOAAAAAj#wAAAAAAAAAAAAABMA#####wAEYWNlcgAAAAAAAAABAAAAAQAAANMAAAAEAQAAAUkBAAAAAAVjZXIxNwABAAAA0wAAAAI#8AAAAAAAAAAAAAATAP####8ABGFjZXIAAAAAAAAAAQAAAAEAAADYAAAABAEAAAFLAQAAAAAFY2VyMjEAAQAAANgAAAACP#AAAAAAAAAAAAAAEwD#####AARhY2VyAAAAAAAAAAEAAAABAAAA3QAAAAQBAAABTQEAAAAABWNlcjIyAAEAAADdAAAAAj#wAAAAAAAAAAAAABMA#####wAEYWNlcgAAAAAAAAABAAAAAQAAAOIAAAAEAQAAAU8BAAAAAAVjZXIyMwABAAAA4gAAAAI#8AAAAAAAAAAAAAATAP####8ABGFjZXIAAAAAAAAAAQAAAAEAAADnAAAABAEAAAFRAQAAAAAFY2VyMjQAAQAAAOcAAAACP#AAAAAAAAAAAAAAEwD#####AARhY2VyAAAAAAAAAAEAAAABAAAA7AAAAAQBAAABUwEAAAAABWNlcjI1AAEAAADsAAAAAj#wAAAAAAAAAAAAABMA#####wAEYWNlcgAAAAAAAAABAAAAAQAAAPEAAAAEAQAAAVUBAAAAAAVjZXIyNgABAAAA8QAAAAI#8AAAAAAAAAAAAAATAP####8ABGFjZXIAAAAAAAAAAQAAAAEAAAD2AAAABAEAAAFXAQAAAAAFY2VyMjcAAQAAAPYAAAACP#AAAAAAAAAAAAAAEwD#####AARhY2VyAAAAAAAAAAEAAAABAAAA+wAAAAQBAAABWQEAAAAABWNlcjI4AAEAAAD7AAAAAj#wAAAAAAAAAAAAABMA#####wAEYWNlcgAAAAAAAAABAAAAAQAAAQAAAAAEAQAAAVsBAAAAAAVjZXIyOQABAAABAAAAAAI#8AAAAAAAAAAAAAATAP####8ABGFjZXIAAAAAAAAAAQAAAAEAAAEFAAAABAEAAAFdAQAAAAAGY2VyMjEwAAEAAAEFAAAAAj#wAAAAAAAAAAAAABMA#####wAEYWNlcgAAAAAAAAABAAAAAQAAAQoAAAAEAQAAAV8BAAAAAAZjZXIyMTEAAQAAAQoAAAACP#AAAAAAAAAAAAAAEwD#####AARhY2VyAAAAAAAAAAEAAAABAAABDwAAAAQBAAABYQEAAAAABmNlcjIxMgABAAABDwAAAAI#8AAAAAAAAAAAAAALAP####8BAAAAABAAAAAAAAAAAAAAAEAIAAAAAAAAAAAFAAFAa2AAAAAAAEB5iFHrhR64AAAACwD#####AQAAAAAQAAAAAAAAAAAAAABACAAAAAAAAAAABQABQHWAAAAAAABAfdhR64UeuAAAAAUA#####wEAAAAAEAAAAAAAAAAAAAAAQAgAAAAAAAAAAAUAAAABYwAAAWQAAAAGAP####8BAAAAARAAAAEAAAABAAABZQE#8AAAAAAAAAAAAAYA#####wEAAAABEAAAAQAAAAEAAAFjAD#wAAAAAAAAAAAABgD#####AQAAAAEQAAABAAAAAQAAAWQAP#AAAAAAAAAAAAAHAP####8BAAAAABAAAAAAAAAAAAAAAEAIAAAAAAAAAAAFAAAAAWYAAAFnAAAABwD#####AQAAAAAQAAAAAAAAAAAAAABACAAAAAAAAAAABQAAAAFmAAABaAAAAAQA#####wEAAAAAAAABAAABaQAAAAI#yZmZmZmZmgAAAAAEAP####8BAAAAAAAAAQAAAWoAAAACP8mZmZmZmZoAAAAACAD#####AAABZgAAAWsAAAAJAP####8BAAAAABAAAAAAAAAAAAAAAEAIAAAAAAAAAAAFAAIAAAFtAAAACQD#####AQAAAAAQAAAAAAAAAAAAAABACAAAAAAAAAAABQABAAABbQAAAAgA#####wAAAWYAAAFsAAAACQD#####AQAAAAAQAAAAAAAAAAAAAABACAAAAAAAAAAABQABAAABcAAAAAkA#####wEAAAAAEAAAAAAAAAAAAAAAQAgAAAAAAAAAAAUAAgAAAXAAAAABAP####8BAAB#AQAAAAABbxAAAAAAAAAAAAABAAAAAgAAAAAAAAAAAANhYWEAAAABAP####8BAH8AAQAAAAABchAAAAAAAAIAAAABAAAAAgAAAAAAAAAAAARiYmJiAAAAEwD#####AAR0ZXh0AAAADAAAAAIAAAACAAAABwAAADoAAAAFAAAAAXUBAAAAABAAAAAAAAAAAAAAAEAIAAAAAAAAAAAFAAAAAAcAAAA6AAAABgAAAAF1AQAAAAAQAAABAAAAAQAAAXYBP#AAAAAAAAAAAAAGAAAAAXUBAAAAABAAAAEAAAABAAAABwA#8AAAAAAAAAAAAAYAAAABdQEAAAAAEAAAAQAAAAEAAAA6AD#wAAAAAAAAAAAABwAAAAF1AQAAAAAQAAAAAAAAAAAAAABACAAAAAAAAAAABQAAAAF3AAABeAAAAAcAAAABdQEAAAAAEAAAAAAAAAAAAAAAQAgAAAAAAAAAAAUAAAABdwAAAXkAAAAEAAAAAXUBAAAAAAAAAQAAAXoAAAACP8mZmZmZmZoAAAAABAAAAAF1AQAAAAAAAAEAAAF7AAAAAj#JmZmZmZmaAAAAAAgAAAABdQAAAXcAAAF8AAAACQAAAAF1AQAAAAAQAAAAAAAAAAAAAABACAAAAAAAAAAABQABAAABfgAAAAgAAAABdQAAAXcAAAF9AAAACQAAAAF1AQAAAAAQAAAAAAAAAAAAAABACAAAAAAAAAAABQACAAABgAAAAAEBAAABdQAAAH8BAAd0ZXh0MDAxAAABfxAAAAAAAAAAAAABAAAAAgAAAAAAAAAAAANhYWEAAAABAQAAAXUAAH8AAQAHdGV4dDAwMgAAAYEQAAAAAAACAAAAAQAAAAIAAAAAAAAAAAAEYmJiYgAAABMA#####wAEdGV4dAAAAAwAAAACAAAAAgAAAAkAAABAAAAABQAAAAGEAQAAAAAQAAAAAAAAAAAAAABACAAAAAAAAAAABQAAAAAJAAAAQAAAAAYAAAABhAEAAAAAEAAAAQAAAAEAAAGFAT#wAAAAAAAAAAAABgAAAAGEAQAAAAAQAAABAAAAAQAAAAkAP#AAAAAAAAAAAAAGAAAAAYQBAAAAABAAAAEAAAABAAAAQAA#8AAAAAAAAAAAAAcAAAABhAEAAAAAEAAAAAAAAAAAAAAAQAgAAAAAAAAAAAUAAAABhgAAAYcAAAAHAAAAAYQBAAAAABAAAAAAAAAAAAAAAEAIAAAAAAAAAAAFAAAAAYYAAAGIAAAABAAAAAGEAQAAAAAAAAEAAAGJAAAAAj#JmZmZmZmaAAAAAAQAAAABhAEAAAAAAAABAAABigAAAAI#yZmZmZmZmgAAAAAIAAAAAYQAAAGGAAABiwAAAAkAAAABhAEAAAAAEAAAAAAAAAAAAAAAQAgAAAAAAAAAAAUAAQAAAY0AAAAIAAAAAYQAAAGGAAABjAAAAAkAAAABhAEAAAAAEAAAAAAAAAAAAAAAQAgAAAAAAAAAAAUAAgAAAY8AAAABAQAAAYQAAAB#AQAHdGV4dDExMQAAAY4QAAAAAAAAAAAAAQAAAAIAAAAAAAAAAAADYWFhAAAAAQEAAAGEAAB#AAEAB3RleHQxMTIAAAGQEAAAAAAAAgAAAAEAAAACAAAAAAAAAAAABGJiYmIAAAATAP####8ABHRleHQAAAAMAAAAAgAAAAIAAAA#AAAARgAAAAUAAAABkwEAAAAAEAAAAAAAAAAAAAAAQAgAAAAAAAAAAAUAAAAAPwAAAEYAAAAGAAAAAZMBAAAAABAAAAEAAAABAAABlAE#8AAAAAAAAAAAAAYAAAABkwEAAAAAEAAAAQAAAAEAAAA#AD#wAAAAAAAAAAAABgAAAAGTAQAAAAAQAAABAAAAAQAAAEYAP#AAAAAAAAAAAAAHAAAAAZMBAAAAABAAAAAAAAAAAAAAAEAIAAAAAAAAAAAFAAAAAZUAAAGWAAAABwAAAAGTAQAAAAAQAAAAAAAAAAAAAABACAAAAAAAAAAABQAAAAGVAAABlwAAAAQAAAABkwEAAAAAAAABAAABmAAAAAI#yZmZmZmZmgAAAAAEAAAAAZMBAAAAAAAAAQAAAZkAAAACP8mZmZmZmZoAAAAACAAAAAGTAAABlQAAAZoAAAAJAAAAAZMBAAAAABAAAAAAAAAAAAAAAEAIAAAAAAAAAAAFAAEAAAGcAAAACAAAAAGTAAABlQAAAZsAAAAJAAAAAZMBAAAAABAAAAAAAAAAAAAAAEAIAAAAAAAAAAAFAAIAAAGeAAAAAQEAAAGTAAAAfwEAB3RleHQxMjEAAAGdEAAAAAAAAAAAAAEAAAACAAAAAAAAAAAAA2FhYQAAAAEBAAABkwAAfwABAAd0ZXh0MTIyAAABnxAAAAAAAAIAAAABAAAAAgAAAAAAAAAAAARiYmJiAAAAEwD#####AAR0ZXh0AAAADAAAAAIAAAACAAAARQAAAEwAAAAFAAAAAaIBAAAAABAAAAAAAAAAAAAAAEAIAAAAAAAAAAAFAAAAAEUAAABMAAAABgAAAAGiAQAAAAAQAAABAAAAAQAAAaMBP#AAAAAAAAAAAAAGAAAAAaIBAAAAABAAAAEAAAABAAAARQA#8AAAAAAAAAAAAAYAAAABogEAAAAAEAAAAQAAAAEAAABMAD#wAAAAAAAAAAAABwAAAAGiAQAAAAAQAAAAAAAAAAAAAABACAAAAAAAAAAABQAAAAGkAAABpQAAAAcAAAABogEAAAAAEAAAAAAAAAAAAAAAQAgAAAAAAAAAAAUAAAABpAAAAaYAAAAEAAAAAaIBAAAAAAAAAQAAAacAAAACP8mZmZmZmZoAAAAABAAAAAGiAQAAAAAAAAEAAAGoAAAAAj#JmZmZmZmaAAAAAAgAAAABogAAAaQAAAGpAAAACQAAAAGiAQAAAAAQAAAAAAAAAAAAAABACAAAAAAAAAAABQABAAABqwAAAAgAAAABogAAAaQAAAGqAAAACQAAAAGiAQAAAAAQAAAAAAAAAAAAAABACAAAAAAAAAAABQACAAABrQAAAAEBAAABogAAAH8BAAd0ZXh0MTMxAAABrBAAAAAAAAAAAAABAAAAAgAAAAAAAAAAAANhYWEAAAABAQAAAaIAAH8AAQAHdGV4dDEzMgAAAa4QAAAAAAACAAAAAQAAAAIAAAAAAAAAAAAEYmJiYgAAABMA#####wAEdGV4dAAAAAwAAAACAAAAAgAAAEsAAAAPAAAABQAAAAGxAQAAAAAQAAAAAAAAAAAAAABACAAAAAAAAAAABQAAAABLAAAADwAAAAYAAAABsQEAAAAAEAAAAQAAAAEAAAGyAT#wAAAAAAAAAAAABgAAAAGxAQAAAAAQAAABAAAAAQAAAEsAP#AAAAAAAAAAAAAGAAAAAbEBAAAAABAAAAEAAAABAAAADwA#8AAAAAAAAAAAAAcAAAABsQEAAAAAEAAAAAAAAAAAAAAAQAgAAAAAAAAAAAUAAAABswAAAbQAAAAHAAAAAbEBAAAAABAAAAAAAAAAAAAAAEAIAAAAAAAAAAAFAAAAAbMAAAG1AAAABAAAAAGxAQAAAAAAAAEAAAG2AAAAAj#JmZmZmZmaAAAAAAQAAAABsQEAAAAAAAABAAABtwAAAAI#yZmZmZmZmgAAAAAIAAAAAbEAAAGzAAABuAAAAAkAAAABsQEAAAAAEAAAAAAAAAAAAAAAQAgAAAAAAAAAAAUAAQAAAboAAAAIAAAAAbEAAAGzAAABuQAAAAkAAAABsQEAAAAAEAAAAAAAAAAAAAAAQAgAAAAAAAAAAAUAAgAAAbwAAAABAQAAAbEAAAB#AQAHdGV4dDE0MQAAAbsQAAAAAAAAAAAAAQAAAAIAAAAAAAAAAAADYWFhAAAAAQEAAAGxAAB#AAEAB3RleHQxNDIAAAG9EAAAAAAAAgAAAAEAAAACAAAAAAAAAAAABGJiYmIAAAATAP####8ABHRleHQAAAAMAAAAAgAAAAIAAABPAAAAVQAAAAUAAAABwAEAAAAAEAAAAAAAAAAAAAAAQAgAAAAAAAAAAAUAAAAATwAAAFUAAAAGAAAAAcABAAAAABAAAAEAAAABAAABwQE#8AAAAAAAAAAAAAYAAAABwAEAAAAAEAAAAQAAAAEAAABPAD#wAAAAAAAAAAAABgAAAAHAAQAAAAAQAAABAAAAAQAAAFUAP#AAAAAAAAAAAAAHAAAAAcABAAAAABAAAAAAAAAAAAAAAEAIAAAAAAAAAAAFAAAAAcIAAAHDAAAABwAAAAHAAQAAAAAQAAAAAAAAAAAAAABACAAAAAAAAAAABQAAAAHCAAABxAAAAAQAAAABwAEAAAAAAAABAAABxQAAAAI#yZmZmZmZmgAAAAAEAAAAAcABAAAAAAAAAQAAAcYAAAACP8mZmZmZmZoAAAAACAAAAAHAAAABwgAAAccAAAAJAAAAAcABAAAAABAAAAAAAAAAAAAAAEAIAAAAAAAAAAAFAAEAAAHJAAAACAAAAAHAAAABwgAAAcgAAAAJAAAAAcABAAAAABAAAAAAAAAAAAAAAEAIAAAAAAAAAAAFAAIAAAHLAAAAAQEAAAHAAAAAfwEAB3RleHQxNTEAAAHKEAAAAAAAAAAAAAEAAAACAAAAAAAAAAAAA2FhYQAAAAEBAAABwAAAfwABAAd0ZXh0MTUyAAABzBAAAAAAAAIAAAABAAAAAgAAAAAAAAAAAARiYmJiAAAAEwD#####AAR0ZXh0AAAADAAAAAIAAAACAAAAVAAAAFsAAAAFAAAAAc8BAAAAABAAAAAAAAAAAAAAAEAIAAAAAAAAAAAFAAAAAFQAAABbAAAABgAAAAHPAQAAAAAQAAABAAAAAQAAAdABP#AAAAAAAAAAAAAGAAAAAc8BAAAAABAAAAEAAAABAAAAVAA#8AAAAAAAAAAAAAYAAAABzwEAAAAAEAAAAQAAAAEAAABbAD#wAAAAAAAAAAAABwAAAAHPAQAAAAAQAAAAAAAAAAAAAABACAAAAAAAAAAABQAAAAHRAAAB0gAAAAcAAAABzwEAAAAAEAAAAAAAAAAAAAAAQAgAAAAAAAAAAAUAAAAB0QAAAdMAAAAEAAAAAc8BAAAAAAAAAQAAAdQAAAACP8mZmZmZmZoAAAAABAAAAAHPAQAAAAAAAAEAAAHVAAAAAj#JmZmZmZmaAAAAAAgAAAABzwAAAdEAAAHWAAAACQAAAAHPAQAAAAAQAAAAAAAAAAAAAABACAAAAAAAAAAABQABAAAB2AAAAAgAAAABzwAAAdEAAAHXAAAACQAAAAHPAQAAAAAQAAAAAAAAAAAAAABACAAAAAAAAAAABQACAAAB2gAAAAEBAAABzwAAAH8BAAd0ZXh0MTYxAAAB2RAAAAAAAAAAAAABAAAAAgAAAAAAAAAAAANhYWEAAAABAQAAAc8AAH8AAQAHdGV4dDE2MgAAAdsQAAAAAAACAAAAAQAAAAIAAAAAAAAAAAAEYmJiYgAAABMA#####wAEdGV4dAAAAAwAAAACAAAAAgAAAFoAAABhAAAABQAAAAHeAQAAAAAQAAAAAAAAAAAAAABACAAAAAAAAAAABQAAAABaAAAAYQAAAAYAAAAB3gEAAAAAEAAAAQAAAAEAAAHfAT#wAAAAAAAAAAAABgAAAAHeAQAAAAAQAAABAAAAAQAAAFoAP#AAAAAAAAAAAAAGAAAAAd4BAAAAABAAAAEAAAABAAAAYQA#8AAAAAAAAAAAAAcAAAAB3gEAAAAAEAAAAAAAAAAAAAAAQAgAAAAAAAAAAAUAAAAB4AAAAeEAAAAHAAAAAd4BAAAAABAAAAAAAAAAAAAAAEAIAAAAAAAAAAAFAAAAAeAAAAHiAAAABAAAAAHeAQAAAAAAAAEAAAHjAAAAAj#JmZmZmZmaAAAAAAQAAAAB3gEAAAAAAAABAAAB5AAAAAI#yZmZmZmZmgAAAAAIAAAAAd4AAAHgAAAB5QAAAAkAAAAB3gEAAAAAEAAAAAAAAAAAAAAAQAgAAAAAAAAAAAUAAQAAAecAAAAIAAAAAd4AAAHgAAAB5gAAAAkAAAAB3gEAAAAAEAAAAAAAAAAAAAAAQAgAAAAAAAAAAAUAAgAAAekAAAABAQAAAd4AAAB#AQAHdGV4dDE3MQAAAegQAAAAAAAAAAAAAQAAAAIAAAAAAAAAAAADYWFhAAAAAQEAAAHeAAB#AAEAB3RleHQxNzIAAAHqEAAAAAAAAgAAAAEAAAACAAAAAAAAAAAABGJiYmIAAAATAP####8ABHRleHQAAAAMAAAAAgAAAAIAAAALAAAAagAAAAUAAAAB7QEAAAAAEAAAAAAAAAAAAAAAQAgAAAAAAAAAAAUAAAAACwAAAGoAAAAGAAAAAe0BAAAAABAAAAEAAAABAAAB7gE#8AAAAAAAAAAAAAYAAAAB7QEAAAAAEAAAAQAAAAEAAAALAD#wAAAAAAAAAAAABgAAAAHtAQAAAAAQAAABAAAAAQAAAGoAP#AAAAAAAAAAAAAHAAAAAe0BAAAAABAAAAAAAAAAAAAAAEAIAAAAAAAAAAAFAAAAAe8AAAHwAAAABwAAAAHtAQAAAAAQAAAAAAAAAAAAAABACAAAAAAAAAAABQAAAAHvAAAB8QAAAAQAAAAB7QEAAAAAAAABAAAB8gAAAAI#yZmZmZmZmgAAAAAEAAAAAe0BAAAAAAAAAQAAAfMAAAACP8mZmZmZmZoAAAAACAAAAAHtAAAB7wAAAfQAAAAJAAAAAe0BAAAAABAAAAAAAAAAAAAAAEAIAAAAAAAAAAAFAAEAAAH2AAAACAAAAAHtAAAB7wAAAfUAAAAJAAAAAe0BAAAAABAAAAAAAAAAAAAAAEAIAAAAAAAAAAAFAAIAAAH4AAAAAQEAAAHtAAAAfwEAB3RleHQyMTEAAAH3EAAAAAAAAAAAAAEAAAACAAAAAAAAAAAAA2FhYQAAAAEBAAAB7QAAfwABAAd0ZXh0MjEyAAAB+RAAAAAAAAIAAAABAAAAAgAAAAAAAAAAAARiYmJiAAAAEwD#####AAR0ZXh0AAAADAAAAAIAAAACAAAAaQAAAHAAAAAFAAAAAfwBAAAAABAAAAAAAAAAAAAAAEAIAAAAAAAAAAAFAAAAAGkAAABwAAAABgAAAAH8AQAAAAAQAAABAAAAAQAAAf0BP#AAAAAAAAAAAAAGAAAAAfwBAAAAABAAAAEAAAABAAAAaQA#8AAAAAAAAAAAAAYAAAAB#AEAAAAAEAAAAQAAAAEAAABwAD#wAAAAAAAAAAAABwAAAAH8AQAAAAAQAAAAAAAAAAAAAABACAAAAAAAAAAABQAAAAH+AAAB#wAAAAcAAAAB#AEAAAAAEAAAAAAAAAAAAAAAQAgAAAAAAAAAAAUAAAAB#gAAAgAAAAAEAAAAAfwBAAAAAAAAAQAAAgEAAAACP8mZmZmZmZoAAAAABAAAAAH8AQAAAAAAAAEAAAICAAAAAj#JmZmZmZmaAAAAAAgAAAAB#AAAAf4AAAIDAAAACQAAAAH8AQAAAAAQAAAAAAAAAAAAAABACAAAAAAAAAAABQABAAACBQAAAAgAAAAB#AAAAf4AAAIEAAAACQAAAAH8AQAAAAAQAAAAAAAAAAAAAABACAAAAAAAAAAABQACAAACBwAAAAEBAAAB#AAAAH8BAAd0ZXh0MjIxAAACBhAAAAAAAAAAAAABAAAAAgAAAAAAAAAAAANhYWEAAAABAQAAAfwAAH8AAQAHdGV4dDIyMgAAAggQAAAAAAACAAAAAQAAAAIAAAAAAAAAAAAEYmJiYgAAABMA#####wAEdGV4dAAAAAwAAAACAAAAAgAAAG8AAAB2AAAABQAAAAILAQAAAAAQAAAAAAAAAAAAAABACAAAAAAAAAAABQAAAABvAAAAdgAAAAYAAAACCwEAAAAAEAAAAQAAAAEAAAIMAT#wAAAAAAAAAAAABgAAAAILAQAAAAAQAAABAAAAAQAAAG8AP#AAAAAAAAAAAAAGAAAAAgsBAAAAABAAAAEAAAABAAAAdgA#8AAAAAAAAAAAAAcAAAACCwEAAAAAEAAAAAAAAAAAAAAAQAgAAAAAAAAAAAUAAAACDQAAAg4AAAAHAAAAAgsBAAAAABAAAAAAAAAAAAAAAEAIAAAAAAAAAAAFAAAAAg0AAAIPAAAABAAAAAILAQAAAAAAAAEAAAIQAAAAAj#JmZmZmZmaAAAAAAQAAAACCwEAAAAAAAABAAACEQAAAAI#yZmZmZmZmgAAAAAIAAAAAgsAAAINAAACEgAAAAkAAAACCwEAAAAAEAAAAAAAAAAAAAAAQAgAAAAAAAAAAAUAAQAAAhQAAAAIAAAAAgsAAAINAAACEwAAAAkAAAACCwEAAAAAEAAAAAAAAAAAAAAAQAgAAAAAAAAAAAUAAgAAAhYAAAABAQAAAgsAAAB#AQAHdGV4dDIzMQAAAhUQAAAAAAAAAAAAAQAAAAIAAAAAAAAAAAADYWFhAAAAAQEAAAILAAB#AAEAB3RleHQyMzIAAAIXEAAAAAAAAgAAAAEAAAACAAAAAAAAAAAABGJiYmIAAAATAP####8ABHRleHQAAAAMAAAAAgAAAAIAAAB1AAAAfAAAAAUAAAACGgEAAAAAEAAAAAAAAAAAAAAAQAgAAAAAAAAAAAUAAAAAdQAAAHwAAAAGAAAAAhoBAAAAABAAAAEAAAABAAACGwE#8AAAAAAAAAAAAAYAAAACGgEAAAAAEAAAAQAAAAEAAAB1AD#wAAAAAAAAAAAABgAAAAIaAQAAAAAQAAABAAAAAQAAAHwAP#AAAAAAAAAAAAAHAAAAAhoBAAAAABAAAAAAAAAAAAAAAEAIAAAAAAAAAAAFAAAAAhwAAAIdAAAABwAAAAIaAQAAAAAQAAAAAAAAAAAAAABACAAAAAAAAAAABQAAAAIcAAACHgAAAAQAAAACGgEAAAAAAAABAAACHwAAAAI#yZmZmZmZmgAAAAAEAAAAAhoBAAAAAAAAAQAAAiAAAAACP8mZmZmZmZoAAAAACAAAAAIaAAACHAAAAiEAAAAJAAAAAhoBAAAAABAAAAAAAAAAAAAAAEAIAAAAAAAAAAAFAAEAAAIjAAAACAAAAAIaAAACHAAAAiIAAAAJAAAAAhoBAAAAABAAAAAAAAAAAAAAAEAIAAAAAAAAAAAFAAIAAAIlAAAAAQEAAAIaAAAAfwEAB3RleHQyNDEAAAIkEAAAAAAAAAAAAAEAAAACAAAAAAAAAAAAA2FhYQAAAAEBAAACGgAAfwABAAd0ZXh0MjQyAAACJhAAAAAAAAIAAAABAAAAAgAAAAAAAAAAAARiYmJiAAAAEwD#####AAR0ZXh0AAAADAAAAAIAAAACAAAAewAAAIIAAAAFAAAAAikBAAAAABAAAAAAAAAAAAAAAEAIAAAAAAAAAAAFAAAAAHsAAACCAAAABgAAAAIpAQAAAAAQAAABAAAAAQAAAioBP#AAAAAAAAAAAAAGAAAAAikBAAAAABAAAAEAAAABAAAAewA#8AAAAAAAAAAAAAYAAAACKQEAAAAAEAAAAQAAAAEAAACCAD#wAAAAAAAAAAAABwAAAAIpAQAAAAAQAAAAAAAAAAAAAABACAAAAAAAAAAABQAAAAIrAAACLAAAAAcAAAACKQEAAAAAEAAAAAAAAAAAAAAAQAgAAAAAAAAAAAUAAAACKwAAAi0AAAAEAAAAAikBAAAAAAAAAQAAAi4AAAACP8mZmZmZmZoAAAAABAAAAAIpAQAAAAAAAAEAAAIvAAAAAj#JmZmZmZmaAAAAAAgAAAACKQAAAisAAAIwAAAACQAAAAIpAQAAAAAQAAAAAAAAAAAAAABACAAAAAAAAAAABQABAAACMgAAAAgAAAACKQAAAisAAAIxAAAACQAAAAIpAQAAAAAQAAAAAAAAAAAAAABACAAAAAAAAAAABQACAAACNAAAAAEBAAACKQAAAH8BAAd0ZXh0MjUxAAACMxAAAAAAAAAAAAABAAAAAgAAAAAAAAAAAANhYWEAAAABAQAAAikAAH8AAQAHdGV4dDI1MgAAAjUQAAAAAAACAAAAAQAAAAIAAAAAAAAAAAAEYmJiYgAAABMA#####wAEdGV4dAAAAAwAAAACAAAAAgAAAIEAAACIAAAABQAAAAI4AQAAAAAQAAAAAAAAAAAAAABACAAAAAAAAAAABQAAAACBAAAAiAAAAAYAAAACOAEAAAAAEAAAAQAAAAEAAAI5AT#wAAAAAAAAAAAABgAAAAI4AQAAAAAQAAABAAAAAQAAAIEAP#AAAAAAAAAAAAAGAAAAAjgBAAAAABAAAAEAAAABAAAAiAA#8AAAAAAAAAAAAAcAAAACOAEAAAAAEAAAAAAAAAAAAAAAQAgAAAAAAAAAAAUAAAACOgAAAjsAAAAHAAAAAjgBAAAAABAAAAAAAAAAAAAAAEAIAAAAAAAAAAAFAAAAAjoAAAI8AAAABAAAAAI4AQAAAAAAAAEAAAI9AAAAAj#JmZmZmZmaAAAAAAQAAAACOAEAAAAAAAABAAACPgAAAAI#yZmZmZmZmgAAAAAIAAAAAjgAAAI6AAACPwAAAAkAAAACOAEAAAAAEAAAAAAAAAAAAAAAQAgAAAAAAAAAAAUAAQAAAkEAAAAIAAAAAjgAAAI6AAACQAAAAAkAAAACOAEAAAAAEAAAAAAAAAAAAAAAQAgAAAAAAAAAAAUAAgAAAkMAAAABAQAAAjgAAAB#AQAHdGV4dDI2MQAAAkIQAAAAAAAAAAAAAQAAAAIAAAAAAAAAAAADYWFhAAAAAQEAAAI4AAB#AAEAB3RleHQyNjIAAAJEEAAAAAAAAgAAAAEAAAACAAAAAAAAAAAABGJiYmIAAAATAP####8ABHRleHQAAAAMAAAAAgAAAAIAAACHAAAAjgAAAAUAAAACRwEAAAAAEAAAAAAAAAAAAAAAQAgAAAAAAAAAAAUAAAAAhwAAAI4AAAAGAAAAAkcBAAAAABAAAAEAAAABAAACSAE#8AAAAAAAAAAAAAYAAAACRwEAAAAAEAAAAQAAAAEAAACHAD#wAAAAAAAAAAAABgAAAAJHAQAAAAAQAAABAAAAAQAAAI4AP#AAAAAAAAAAAAAHAAAAAkcBAAAAABAAAAAAAAAAAAAAAEAIAAAAAAAAAAAFAAAAAkkAAAJKAAAABwAAAAJHAQAAAAAQAAAAAAAAAAAAAABACAAAAAAAAAAABQAAAAJJAAACSwAAAAQAAAACRwEAAAAAAAABAAACTAAAAAI#yZmZmZmZmgAAAAAEAAAAAkcBAAAAAAAAAQAAAk0AAAACP8mZmZmZmZoAAAAACAAAAAJHAAACSQAAAk4AAAAJAAAAAkcBAAAAABAAAAAAAAAAAAAAAEAIAAAAAAAAAAAFAAEAAAJQAAAACAAAAAJHAAACSQAAAk8AAAAJAAAAAkcBAAAAABAAAAAAAAAAAAAAAEAIAAAAAAAAAAAFAAIAAAJSAAAAAQEAAAJHAAAAfwEAB3RleHQyNzEAAAJREAAAAAAAAAAAAAEAAAACAAAAAAAAAAAAA2FhYQAAAAEBAAACRwAAfwABAAd0ZXh0MjcyAAACUxAAAAAAAAIAAAABAAAAAgAAAAAAAAAAAARiYmJiAAAAEwD#####AAR0ZXh0AAAADAAAAAIAAAACAAAAjQAAAJQAAAAFAAAAAlYBAAAAABAAAAAAAAAAAAAAAEAIAAAAAAAAAAAFAAAAAI0AAACUAAAABgAAAAJWAQAAAAAQAAABAAAAAQAAAlcBP#AAAAAAAAAAAAAGAAAAAlYBAAAAABAAAAEAAAABAAAAjQA#8AAAAAAAAAAAAAYAAAACVgEAAAAAEAAAAQAAAAEAAACUAD#wAAAAAAAAAAAABwAAAAJWAQAAAAAQAAAAAAAAAAAAAABACAAAAAAAAAAABQAAAAJYAAACWQAAAAcAAAACVgEAAAAAEAAAAAAAAAAAAAAAQAgAAAAAAAAAAAUAAAACWAAAAloAAAAEAAAAAlYBAAAAAAAAAQAAAlsAAAACP8mZmZmZmZoAAAAABAAAAAJWAQAAAAAAAAEAAAJcAAAAAj#JmZmZmZmaAAAAAAgAAAACVgAAAlgAAAJdAAAACQAAAAJWAQAAAAAQAAAAAAAAAAAAAABACAAAAAAAAAAABQABAAACXwAAAAgAAAACVgAAAlgAAAJeAAAACQAAAAJWAQAAAAAQAAAAAAAAAAAAAABACAAAAAAAAAAABQACAAACYQAAAAEBAAACVgAAAH8BAAd0ZXh0MjgxAAACYBAAAAAAAAAAAAABAAAAAgAAAAAAAAAAAANhYWEAAAABAQAAAlYAAH8AAQAHdGV4dDI4MgAAAmIQAAAAAAACAAAAAQAAAAIAAAAAAAAAAAAEYmJiYgAAABMA#####wAEdGV4dAAAAAwAAAACAAAAAgAAAJMAAACaAAAABQAAAAJlAQAAAAAQAAAAAAAAAAAAAABACAAAAAAAAAAABQAAAACTAAAAmgAAAAYAAAACZQEAAAAAEAAAAQAAAAEAAAJmAT#wAAAAAAAAAAAABgAAAAJlAQAAAAAQAAABAAAAAQAAAJMAP#AAAAAAAAAAAAAGAAAAAmUBAAAAABAAAAEAAAABAAAAmgA#8AAAAAAAAAAAAAcAAAACZQEAAAAAEAAAAAAAAAAAAAAAQAgAAAAAAAAAAAUAAAACZwAAAmgAAAAHAAAAAmUBAAAAABAAAAAAAAAAAAAAAEAIAAAAAAAAAAAFAAAAAmcAAAJpAAAABAAAAAJlAQAAAAAAAAEAAAJqAAAAAj#JmZmZmZmaAAAAAAQAAAACZQEAAAAAAAABAAACawAAAAI#yZmZmZmZmgAAAAAIAAAAAmUAAAJnAAACbAAAAAkAAAACZQEAAAAAEAAAAAAAAAAAAAAAQAgAAAAAAAAAAAUAAQAAAm4AAAAIAAAAAmUAAAJnAAACbQAAAAkAAAACZQEAAAAAEAAAAAAAAAAAAAAAQAgAAAAAAAAAAAUAAgAAAnAAAAABAQAAAmUAAAB#AQAHdGV4dDI5MQAAAm8QAAAAAAAAAAAAAQAAAAIAAAAAAAAAAAADYWFhAAAAAQEAAAJlAAB#AAEAB3RleHQyOTIAAAJxEAAAAAAAAgAAAAEAAAACAAAAAAAAAAAABGJiYmIAAAATAP####8ABHRleHQAAAAMAAAAAgAAAAIAAACZAAAAoAAAAAUAAAACdAEAAAAAEAAAAAAAAAAAAAAAQAgAAAAAAAAAAAUAAAAAmQAAAKAAAAAGAAAAAnQBAAAAABAAAAEAAAABAAACdQE#8AAAAAAAAAAAAAYAAAACdAEAAAAAEAAAAQAAAAEAAACZAD#wAAAAAAAAAAAABgAAAAJ0AQAAAAAQAAABAAAAAQAAAKAAP#AAAAAAAAAAAAAHAAAAAnQBAAAAABAAAAAAAAAAAAAAAEAIAAAAAAAAAAAFAAAAAnYAAAJ3AAAABwAAAAJ0AQAAAAAQAAAAAAAAAAAAAABACAAAAAAAAAAABQAAAAJ2AAACeAAAAAQAAAACdAEAAAAAAAABAAACeQAAAAI#yZmZmZmZmgAAAAAEAAAAAnQBAAAAAAAAAQAAAnoAAAACP8mZmZmZmZoAAAAACAAAAAJ0AAACdgAAAnsAAAAJAAAAAnQBAAAAABAAAAAAAAAAAAAAAEAIAAAAAAAAAAAFAAEAAAJ9AAAACAAAAAJ0AAACdgAAAnwAAAAJAAAAAnQBAAAAABAAAAAAAAAAAAAAAEAIAAAAAAAAAAAFAAIAAAJ#AAAAAQEAAAJ0AAAAfwEACHRleHQyMTAxAAACfhAAAAAAAAAAAAABAAAAAgAAAAAAAAAAAANhYWEAAAABAQAAAnQAAH8AAQAIdGV4dDIxMDIAAAKAEAAAAAAAAgAAAAEAAAACAAAAAAAAAAAABGJiYmIAAAATAP####8ABHRleHQAAAAMAAAAAgAAAAIAAACfAAAApgAAAAUAAAACgwEAAAAAEAAAAAAAAAAAAAAAQAgAAAAAAAAAAAUAAAAAnwAAAKYAAAAGAAAAAoMBAAAAABAAAAEAAAABAAAChAE#8AAAAAAAAAAAAAYAAAACgwEAAAAAEAAAAQAAAAEAAACfAD#wAAAAAAAAAAAABgAAAAKDAQAAAAAQAAABAAAAAQAAAKYAP#AAAAAAAAAAAAAHAAAAAoMBAAAAABAAAAAAAAAAAAAAAEAIAAAAAAAAAAAFAAAAAoUAAAKGAAAABwAAAAKDAQAAAAAQAAAAAAAAAAAAAABACAAAAAAAAAAABQAAAAKFAAAChwAAAAQAAAACgwEAAAAAAAABAAACiAAAAAI#yZmZmZmZmgAAAAAEAAAAAoMBAAAAAAAAAQAAAokAAAACP8mZmZmZmZoAAAAACAAAAAKDAAAChQAAAooAAAAJAAAAAoMBAAAAABAAAAAAAAAAAAAAAEAIAAAAAAAAAAAFAAEAAAKMAAAACAAAAAKDAAAChQAAAosAAAAJAAAAAoMBAAAAABAAAAAAAAAAAAAAAEAIAAAAAAAAAAAFAAIAAAKOAAAAAQEAAAKDAAAAfwEACHRleHQyMTExAAACjRAAAAAAAAAAAAABAAAAAgAAAAAAAAAAAANhYWEAAAABAQAAAoMAAH8AAQAIdGV4dDIxMTIAAAKPEAAAAAAAAgAAAAEAAAACAAAAAAAAAAAABGJiYmIAAAATAP####8ABHRleHQAAAAMAAAAAgAAAAIAAAClAAAArAAAAAUAAAACkgEAAAAAEAAAAAAAAAAAAAAAQAgAAAAAAAAAAAUAAAAApQAAAKwAAAAGAAAAApIBAAAAABAAAAEAAAABAAACkwE#8AAAAAAAAAAAAAYAAAACkgEAAAAAEAAAAQAAAAEAAAClAD#wAAAAAAAAAAAABgAAAAKSAQAAAAAQAAABAAAAAQAAAKwAP#AAAAAAAAAAAAAHAAAAApIBAAAAABAAAAAAAAAAAAAAAEAIAAAAAAAAAAAFAAAAApQAAAKVAAAABwAAAAKSAQAAAAAQAAAAAAAAAAAAAABACAAAAAAAAAAABQAAAAKUAAAClgAAAAQAAAACkgEAAAAAAAABAAAClwAAAAI#yZmZmZmZmgAAAAAEAAAAApIBAAAAAAAAAQAAApgAAAACP8mZmZmZmZoAAAAACAAAAAKSAAAClAAAApkAAAAJAAAAApIBAAAAABAAAAAAAAAAAAAAAEAIAAAAAAAAAAAFAAEAAAKbAAAACAAAAAKSAAAClAAAApoAAAAJAAAAApIBAAAAABAAAAAAAAAAAAAAAEAIAAAAAAAAAAAFAAIAAAKdAAAAAQEAAAKSAAAAfwEACHRleHQyMTIxAAACnBAAAAAAAAAAAAABAAAAAgAAAAAAAAAAAANhYWEAAAABAQAAApIAAH8AAQAIdGV4dDIxMjIAAAKeEAAAAAAAAgAAAAEAAAACAAAAAAAAAAAABGJiYmIAAAALAP####8BAAAAABAAAAAAAAAAAAAAAEAIAAAAAAAAAAlwb2ludFBsdXMFAAFAaAAAAAAAAEB6jhR64UeuAAAAAwD#####AAAAAAEABmltUGx1cwAAAqENAAAAAAAAAAAAAAAAAAIAAAAAAAAAAAAAAAAVAAAAFQAAAp+JUE5HDQoaCgAAAA1JSERSAAAAFQAAABUIBgAAAKkXpZYAAAABc1JHQgCuzhzpAAAABGdBTUEAALGPC#xhBQAAAAlwSFlzAAAOxAAADsQBlSsOGwAAAjRJREFUOE+1k01r1FAUht#EEUb8bJnW4tAilFm50Cpap4q4caFYhW4rLvwDgroQ#4Gdzazd9QcUoSKuBHHTLoqLTqeCta1jJyBt00luJpPvxJOb0BIT6SZ9wiU55+a899xzzxUABDRyhYsSkZUDgiBAjL9z5UhEE9v#9qONdUmGtKNy+zCGB89htFzClcqF2BNtPyH6#msDPcvB+b7T3D6MP3sMZ08WMXnrUuzJEH03v0iCp#Do9sFP4Vy73YYoiiiXy7E3JMDclwaYYePZg+ux7z8HFYRP4O8P0zRQq82gXq#v+zzPhW1b8H0#jkqSFvUDuK5zMBwHrVYLW1u#ue2QmGXq0DUFjmPHUUnSopSJY9v4tbmJ5soKms0mGNOgKiq3lxvLWP+5RqIqPDdbtBC#E+i6jrczNSwsLNIiASRJ4rWafvKUZgNUb97Am9evop8zSIn6lKl4TEC1Oo7BgRKv3fyHjzheKGDy4X1ez4sjw#DpzWua0elpUd9DgUSnph5ToIceZb26+h3FE0W8fPEctmXQMHltwwWEjL2m1gkPpqdpYB2Zxi401okOjOqssT10mQJDZ7AMnWebRVqUgvWuAk3ZhdrZgc5k3Jm4honxy1DlbRKV0euqvAPCTLNINP#spyV4popKSeBBLrVMWI5#EQSRX4YNVsSZ#iFM37saz2TcqM9La9iQtiGrGvWrx08+nkpCUWHgQH8fKiNDuDs2GvmJlGgeZF7TPDgSUb796DMvgL#wqELm#4Nd5AAAAABJRU5ErkJgggAAAAsA#####wEAAAAAEAAAAAAAAAAAAAAAQAgAAAAAAAAACnBvaW50TW9pbnMFAAFAdjAAAAAAAEB+ThR64UeuAAAAAwD#####AAAAAAEAB2ltTW9pbnMAAAKjDQAAAAAAAAAAAAAAAAACAAAAAAAAAAAAAAAAFQAAABUAAAJRiVBORw0KGgoAAAANSUhEUgAAABUAAAAVCAYAAACpF6WWAAAAAXNSR0IArs4c6QAAAARnQU1BAACxjwv8YQUAAAAJcEhZcwAADsQAAA7EAZUrDhsAAAHmSURBVDhP1ZQ9TxRRFIbfBRQX2IQFlVBYKBUWhtJYECoSKaz8Axb8BKQgUQv8Gxr9BTYUJtgYExO0ml3WxAZCYoCF3dn53Dsf9y7n7pwMTmbYxqXwSd7MnXNy3pl77kcJQI80VPqmRPI2BEqlEkZ4PFSuxTQ3#W#GAQ6O22hZHkeuplqZwP35KpaXFjiSTD9n+n5nD5XJW5ieKnPkamxP9D++#uwxRxJTjTZN2f6w2#u+f0gjNVBKqd7PX0e91+8+0#sl2q+4p5SjooGKoxCxjLkgS6GpUhIyjgoVxyHCsAvfsxEInyuyFJtKBdu20Wg0UK#XM6oZNRiGgbPmCULR5YosY#zMoQ02Xm7CdfO7YHR0BK+2NlG9t8iRLIWmtAyYnZ3B2tpTCPobWhXO6NbQWOdnphHJiKNZclvq7ccvePJwDksP7lBCQUpJHpKzoL7GCANB6uL3sY8fRwJvXqxydsAxDYMAntNBp9WEY57DscxUrm32c6LrIooCrshSbEpT1sVO5xyW2YTVPk1lU8xzTAjf7W+rInKm5fEx+CLAadsh2ThpaVmpdOzM8tFyAgSRQvnmDa68JNfTT19r+NNsw#H8ZFHouv0rnaJPY2VyAvN3b+P5yiOOJj3Nmf4r#+F9mgyHBXABZlp1ppoTheUAAAAASUVORK5CYIIAAAAGAP####8BAAAAARAAAAEAAAABAAACoQE#8AAAAAAAAAAAAA4A#####wEAAAAAEAAAAAAAAAAAAAAAQAgAAAAAAAAAAAUAAUA1AAAAAAAAAAACpQAAAAYA#####wEAAAABEAAAAQAAAAEAAAKmAD#wAAAAAAAAAAAADgD#####AQAAAAAQAAAAAAAAAAAAAABACAAAAAAAAAAABQABQDUAAAAAAAAAAAKnAAAABgD#####AQAAAAEQAAABAAAAAQAAAqEAP#AAAAAAAAAAAAAGAP####8BAAAAARAAAAEAAAABAAACqAE#8AAAAAAAAAAAAAcA#####wEAAAAAEAAAAAAAAAAAAAAAQAgAAAAAAAAAAAUAAAACqQAAAqoAAAAMAP####8AAAAAAAhwb2x5UGx1cwABAAAABQAAAqEAAAKmAAACqAAAAqsAAAKh#####wAAAAEACENWZWN0ZXVyAP####8BAAAAABAAAAEAAAABAAACoQAAAqMA#####wAAAAEADUNUcmFuc1BhclZlY3QA#####wAAAq0AAAAQAP####8BAAAAABAAAAAAAAAAAAAAAEAIAAAAAAAAAAAFAAAAAqEAAAKuAAAAEAD#####AQAAAAAQAAAAAAAAAAAAAABACAAAAAAAAAAABQAAAAKmAAACrgAAABAA#####wEAAAAAEAAAAAAAAAAAAAAAQAgAAAAAAAAAAAUAAAACqAAAAq4AAAAQAP####8BAAAAABAAAAAAAAAAAAAAAEAIAAAAAAAAAAAFAAAAAqsAAAKuAAAADAD#####AAAAAAAJcG9seU1vaW5zAAEAAAAFAAACrwAAArAAAAKxAAACsgAAAq8AAAAJAP####8BAAAAABAAAAAAAAAAAAAAAEAIAAAAAAAAAAAFAAIAAAAVAAAACQD#####AQAAAAAQAAAAAAAAAAAAAABACAAAAAAAAAAABQACAAAAGf####8AAAABAAxDQXJjRGVDZXJjbGUA#####wAAAAAABnBhcmFiNQECAAAAFgAAAAsAAAAaAAAACQD#####AQAAAAAQAAAAAAAAAAAAAABACAAAAAAAAAAABQABAAAAHAAAAAkA#####wEAAAAAEAAAAAAAAAAAAAAAQAgAAAAAAAAAAAUAAgAAACAAAAAWAP####8AAAAAAAZwYXJhYjYBAgAAAB0AAAAPAAAAIQAAAAYA#####wEAAAABEAAAAQAAAQIAAAAaAT#wAAAAAAAAAAAACAD#####AAACugAAACMAAAAJAP####8BAAAAABAAAAAAAAAAAAAAAEAIAAAAAAAAAAAFAAEAAAK7AAAACQD#####AQAAAAAQAAAAAAAAAAAAAABACAAAAAAAAAAABQACAAACuwAAAAkA#####wEAAAAAEAAAAAAAAAAAAAAAQAgAAAAAAAAAAAUAAgAAACX#####AAAAAQAJQ1JvdGF0aW9uAP####8AAAAmAAAAAkBWgAAAAAAAAAAAEAD#####AQAAAAAQAAAAAAAAAAAAAABACAAAAAAAAAAABQAAAAAiAAACvwAAABAA#####wEAAAAAEAAAAAAAAAAAAAAAQAgAAAAAAAAAAAUAAAACwAAAAr8AAAAQAP####8BAAAAABAAAAAAAAAAAAAAAEAIAAAAAAAAAAAFAAAAAsEAAAK#AAAAFgD#####AAAAAAAGcGFyYWI0AQIAAALCAAACvAAAACYAAAAWAP####8AAAAAAAZwYXJhYjMBAgAAAsAAAAK9AAAAJgAAABIA#####wAAAAAAEAAAAQAGcGFyYWIyAQIAAAAaAAACvQAAABIA#####wAAAAAAEAAAAQAGcGFyYWIxAQIAAAK8AAAAIQAAAAEA#####wAAAH8BAAl0ZXh0MTEwMDEAAALAEAAAAAAAAgAAAAEAAAACAAAAAAAAAAAAA2tvaQAAAAEA#####wAAfwABAAl0ZXh0MTEwMDIAAALCEAAAAAAAAAAAAAEAAAACAAAAAAAAAAAABW5ia29pAAAAEv##########'
    j3pEmpty(fig)
    if ((ds.Change && ds.Question === 'Diagramme_tout') && !co) {
      stor.repFois = j3pGetRandomBool()
      stor.nbCases = 2
    } else {
      stor.repFois = stor.lexo.prob.op !== '+'
      stor.nbCases = stor.nbCases1
    }
    const height = (stor.lexo.niv !== 1) ? 270 : ((ds.Change && ds.Question === 'Diagramme_tout') || stor.repFois) ? 220 : 170
    j3pCreeSVG(fig, { id: svgId, width: 980, height, style: { border: 'none' } })
    stor.mtgAppLecteur.addDoc(svgId, txtFigure, true)
    stor.mtgAppLecteur.calculateAndDisplayAll(true)
    stor.mtgAppLecteur.setPointPosition(svgId, '#n1', 20, 20, true)
    stor.mtgAppLecteur.setPointPosition(svgId, '#p1co', -10, -10, true)
    stor.mtgAppLecteur.setPointPosition(svgId, '#p2co', -10, 600, true)
    stor.mtgAppLecteur.setPointPosition(svgId, '#p3co', 1000, 600, true)
    stor.mtgAppLecteur.setPointPosition(svgId, '#p4co', 1000, -10, true)
    stor.mtgAppLecteur.setPointPosition(svgId, '#dirige', 960, 10, true)
    stor.mtgAppLecteur.addEventListener(svgId, '#poly0', 'mouseover', function () {
      if (stor.disab) return
      stor.mtgAppLecteur.getDoc(svgId).defaultCursor = 'pointer'
      stor.mtgAppLecteur.setVisible(svgId, '#im0', true)
      stor.mtgAppLecteur.setVisible(svgId, '#surf0', true)
    })
    stor.mtgAppLecteur.addEventListener(svgId, '#poly0', 'mouseout', function () {
      if (stor.disab) return
      stor.mtgAppLecteur.getDoc(svgId).defaultCursor = ''
      stor.mtgAppLecteur.setVisible(svgId, '#im0', false)
      stor.mtgAppLecteur.setVisible(svgId, '#surf0', false)
    })
    stor.mtgAppLecteur.addEventListener(svgId, '#poly0', 'mousedown', function () {
      if (stor.disab) return
      const k = {}
      k.i = 0
      k.n = 0
      faitEdit(k)
    })
    // montre la prems et cache tous les niv 2
    for (let i = stor.nbCases2 + 1; i < 13; i++) {
      stor.mtgAppLecteur.setVisible(svgId, '#poly2' + i, false)
      stor.mtgAppLecteur.setVisible(svgId, '#aff2' + i, false)
      stor.mtgAppLecteur.setVisible(svgId, '#text2' + i + '1', false)
      stor.mtgAppLecteur.setVisible(svgId, '#text2' + i + '2', false)
    }
    stor.mtgAppLecteur.setVisible(svgId, '#parab1', false)
    stor.mtgAppLecteur.setVisible(svgId, '#parab2', false)
    stor.mtgAppLecteur.setVisible(svgId, '#parab3', false)
    stor.mtgAppLecteur.setVisible(svgId, '#parab4', false)
    stor.mtgAppLecteur.setVisible(svgId, '#parab5', false)
    stor.mtgAppLecteur.setVisible(svgId, '#parab6', false)
    stor.mtgAppLecteur.setVisible(svgId, '#partKoi', false)
    stor.mtgAppLecteur.setVisible(svgId, '#partVal', false)
    stor.mtgAppLecteur.setVisible(svgId, '#aff0', false)
    stor.mtgAppLecteur.setText(svgId, '#text002', (stor.yakoi[0][0].val === '') ? '?' : stor.yakoi[0][0].val)
    stor.mtgAppLecteur.setText(svgId, '#text001', (stor.yakoi[0][0].nom === '') ? '?' : stor.yakoi[0][0].nom)
    // si ya change on part sur addition avec 2 , avec + et - et change representation
    afficheNcase(svgId, 1, '', stor.nbCases, stor.repFois)
    stor.mtgAppLecteur.updateFigure(svgId)
  }

  function faitEdit (k) {
    const yy = j3pModale({ titre: 'Modifier', contenu: '' })
    let placeok, placeannule
    switch (ds.Question) {
      case 'Diagramme_tout': {
        let montab = j3pClone(stor.lexo.prob.decoupes)
        montab.push(stor.lexo.prob.total)
        montab = j3pShuffle(montab)
        montab.splice(0, 0, 'Choisir')
        const ttt2 = addDefaultTable(yy, 2, 1)
        const ttt = addDefaultTable(ttt2[0][0], 1, 3)
        const ttto = addDefaultTable(ttt[0][0], 2, 1)
        const tttval = addDefaultTable(ttto[0][0], 1, 2)
        const tttcont = addDefaultTable(ttto[0][0], 1, 2)
        j3pAffiche(tttval[0][0], null, 'valeur:&nbsp;')
        const laval = (stor.yakoi[k.i][k.n].val !== '') ? stor.yakoi[k.i][k.n].val : undefined
        stor.newval = new ZoneStyleMathquill1(tttval[0][1], {
          limite: 3,
          limitenb: 1,
          contenu: laval,
          enter: () => sortEdit(k)
        })
        j3pAffiche(tttcont[0][0], null, 'contenu:&nbsp;')
        const choix = (stor.yakoi[k.i][k.n].nom !== '') ? stor.yakoi[k.i][k.n].nom : undefined
        stor.newCont = ListeDeroulante.create(tttcont[0][1], montab, { dansModale: true, choix })
        placeannule = ttt2[0][0]
        placeok = ttt[0][2]
        ttt[0][1].style.width = '40px'
      }
        break
      case 'Diagramme_nom': {
        let montab = j3pClone(stor.lexo.prob.decoupes)
        montab.push(stor.lexo.prob.total)
        montab = j3pShuffle(montab)
        montab.splice(0, 0, 'Choisir')
        const ttt = addDefaultTable(yy, 2, 4)
        j3pAffiche(ttt[0][0], null, 'contenu:&nbsp;')
        stor.newCont = ListeDeroulante.create(ttt[0][1], montab, { dansModale: true })
        ttt[0][2].style.width = '40px'
        placeok = ttt[0][3]
        placeannule = ttt[1][0]
      }
        break
      case 'Diagramme_nombre': {
        if (k.i === stor.ablok.ligne && k.n === stor.ablok.ca) {
          const jkl = addDefaultTable(yy, 2, 1)
          j3pAddContent(jkl[0][0], 'Tu ne pourras modifier cette valeur <br> qu’après la validation du schéma.')
          const laff = () => {
            j3pDetruit('modale')
            j3pDetruit('j3pmasque')
          }
          j3pAjouteBouton(jkl[1][0], laff, { className: '', value: 'OK' })
          return
        }
        const ttt = addDefaultTable(yy, 2, 4)
        j3pAffiche(ttt[0][0], null, 'nouvelle valeur:')
        const laval = (stor.yakoi[k.i][k.n].val !== '') ? stor.yakoi[k.i][k.n].val : undefined
        stor.newval = new ZoneStyleMathquill1(ttt[0][1], {
          limite: 3,
          limitenb: 1,
          contenu: laval,
          enter: () => sortEdit(k)
        })
        setTimeout(function () { stor.newval.focus() }, 100)
        ttt[0][2].style.width = '40px'
        placeok = ttt[0][3]
        placeannule = ttt[1][0]
        break
      }
    }
    j3pAjouteBouton(placeok, function () { sortEdit(k) }, { className: '', value: 'OK' })
    j3pAjouteBouton(placeannule, function () { sortEditQ(k) }, { className: '', value: 'Annuler' })
    const elem = $('.croix')[0]
    j3pDetruit(elem)
  }
  function afficheNcase (svgId, ligne, dou, nb, mul) {
    stor.nbCases = (mul) ? 4 : nb
    stor.repFois = mul
    const baseX = (mul || ds.Change === false) ? 960 : 930
    stor.mtgAppLecteur.setPointPosition(svgId, '#dirige', baseX, 10, true)
    for (let i = 1; i < 8; i++) {
      stor.mtgAppLecteur.setVisible(svgId, '#poly1' + i, false)
      stor.mtgAppLecteur.setVisible(svgId, '#aff1' + i, false)
      stor.mtgAppLecteur.setVisible(svgId, '#text1' + i + '1', false)
      stor.mtgAppLecteur.setVisible(svgId, '#text1' + i + '2', false)
      stor.mtgAppLecteur.setText(svgId, '#aff1' + i, ' ')
    }
    stor.mtgAppLecteur.setVisible(svgId, '#imPlus', false)
    stor.mtgAppLecteur.setVisible(svgId, '#imMoins', false)
    stor.mtgAppLecteur.setVisible(svgId, '#polyPlus', false)
    stor.mtgAppLecteur.setVisible(svgId, '#polyMoins', false)
    stor.mtgAppLecteur.setVisible(svgId, '#text11002', false)
    stor.mtgAppLecteur.setVisible(svgId, '#text11001', false)
    stor.mtgAppLecteur.setText(svgId, '#text11002', '')
    stor.mtgAppLecteur.setText(svgId, '#text11001', '')
    stor.mtgAppLecteur.setVisible(svgId, '#parab1', false)
    stor.mtgAppLecteur.setVisible(svgId, '#parab2', false)
    stor.mtgAppLecteur.setVisible(svgId, '#parab3', false)
    stor.mtgAppLecteur.setVisible(svgId, '#parab4', false)
    stor.mtgAppLecteur.setVisible(svgId, '#parab5', false)
    stor.mtgAppLecteur.setVisible(svgId, '#parab6', false)
    stor.mtgAppLecteur.setVisible(svgId, '#partKoi', false)
    stor.mtgAppLecteur.setVisible(svgId, '#partVal', false)
    stor.mtgAppLecteur.setText(svgId, '#partVal', '')
    stor.mtgAppLecteur.setText(svgId, '#partKoi', '')
    stor.mtgAppLecteur.setVisible(svgId, '#polyNbPart', false)
    stor.mtgAppLecteur.setVisible(svgId, '#surfNbPart', false)
    stor.mtgAppLecteur.setVisible(svgId, '#imNpPart', false)
    if (ligne === 1) {
      if (!mul) {
        const adec = (nb < 4) ? 1 : (nb < 6) ? 2 : 3
        for (let i = 1; i < nb + 1; i++) {
          stor.mtgAppLecteur.setPointPosition(svgId, '#dirige1' + i, 20 + (baseX - 20) / nb * i, 100, true)
          stor.mtgAppLecteur.setVisible(svgId, '#poly1' + i, true)
          stor.mtgAppLecteur.addEventListener(svgId, '#poly1' + i, 'mouseover', function () {
            if (stor.disab) return
            stor.mtgAppLecteur.getDoc(svgId).defaultCursor = 'pointer'
            stor.mtgAppLecteur.setVisible(svgId, '#im1' + i, true)
            stor.mtgAppLecteur.setVisible(svgId, '#surf1' + i, true)
          })
          stor.mtgAppLecteur.addEventListener(svgId, '#poly1' + i, 'mouseout', function () {
            if (stor.disab) return
            stor.mtgAppLecteur.getDoc(svgId).defaultCursor = ''
            stor.mtgAppLecteur.setVisible(svgId, '#im1' + i, false)
            stor.mtgAppLecteur.setVisible(svgId, '#surf1' + i, false)
          })
          stor.mtgAppLecteur.addEventListener(svgId, '#poly1' + i, 'mousedown', function () {
            if (stor.disab) return
            const k = {}
            k.i = 1
            k.n = i
            faitEdit(k)
          })
          stor.mtgAppLecteur.setVisible(svgId, '#aff1' + i, false)
          stor.mtgAppLecteur.setVisible(svgId, '#text1' + i + '1', true)
          stor.mtgAppLecteur.setVisible(svgId, '#text1' + i + '2', true)
          stor.mtgAppLecteur.setText(svgId, '#text1' + i + '2', (stor.yakoi[1][i].val === '') ? '?' : stor.yakoi[1][i].val, adec)
          stor.mtgAppLecteur.setText(svgId, '#text1' + i + '1', (stor.yakoi[1][i].nom === '') ? '?' : decoupe(stor.yakoi[1][i].nom, adec))
        }
        if (ds.Change) {
          stor.mtgAppLecteur.setPointPosition(svgId, '#pointPlus', 945, 82, true)
          stor.mtgAppLecteur.setPointPosition(svgId, '#pointMoins', 945, 114, true)
          if (nb < 7) {
            stor.mtgAppLecteur.setVisible(svgId, '#polyPlus', true)
            stor.mtgAppLecteur.setVisible(svgId, '#imPlus', true)
            stor.mtgAppLecteur.setColor(svgId, '#polyPlus', 0, 0, 0, true, 1)
            stor.mtgAppLecteur.setLineStyle(svgId, '#polyPlus', 0, 2, true)
            stor.mtgAppLecteur.addEventListener(svgId, '#polyPlus', 'mouseover', function () {
              if (stor.disab) return
              stor.mtgAppLecteur.getDoc(svgId).defaultCursor = 'pointer'
              stor.mtgAppLecteur.setLineStyle(svgId, '#polyPlus', 0, 4, true)
              stor.mtgAppLecteur.setColor(svgId, '#polyPlus', 255, 0, 0, true, 1)
            })
            stor.mtgAppLecteur.addEventListener(svgId, '#polyPlus', 'mouseout', function () {
              if (stor.disab) return
              stor.mtgAppLecteur.getDoc(svgId).defaultCursor = ''
              stor.mtgAppLecteur.setLineStyle(svgId, '#polyPlus', 0, 2, true)
              stor.mtgAppLecteur.setColor(svgId, '#polyPlus', 0, 0, 0, true, 1)
            })
            stor.mtgAppLecteur.addEventListener(svgId, '#polyPlus', 'mousedown', function () {
              if (stor.disab) return
              if (nb === 6) { stor.mtgAppLecteur.getDoc(svgId).defaultCursor = '' } else {
                setTimeout(() => {
                  stor.mtgAppLecteur.setLineStyle(svgId, '#polyPlus', 0, 4, true)
                  stor.mtgAppLecteur.setColor(svgId, '#polyPlus', 255, 0, 0, true, 1)
                }, 1)
              }
              afficheNcase(svgId, ligne, dou, nb + 1, mul)
            })
          }
          if (nb > 2) {
            stor.mtgAppLecteur.setVisible(svgId, '#imMoins', true)
            stor.mtgAppLecteur.setVisible(svgId, '#polyMoins', true)
            stor.mtgAppLecteur.setColor(svgId, '#polyMoins', 0, 0, 0, true, 1)
            stor.mtgAppLecteur.setLineStyle(svgId, '#polyMoins', 0, 2, true)
            stor.mtgAppLecteur.addEventListener(svgId, '#polyMoins', 'mouseover', function () {
              if (stor.disab) return
              stor.mtgAppLecteur.getDoc(svgId).defaultCursor = 'pointer'
              stor.mtgAppLecteur.setLineStyle(svgId, '#polyMoins', 0, 4, true)
              stor.mtgAppLecteur.setColor(svgId, '#polyMoins', 255, 0, 0, true, 1)
            })
            stor.mtgAppLecteur.addEventListener(svgId, '#polyMoins', 'mouseout', function () {
              if (stor.disab) return
              stor.mtgAppLecteur.getDoc(svgId).defaultCursor = ''
              stor.mtgAppLecteur.setLineStyle(svgId, '#polyMoins', 0, 2, true)
              stor.mtgAppLecteur.setColor(svgId, '#polyMoins', 0, 0, 0, true, 1)
            })
            stor.mtgAppLecteur.addEventListener(svgId, '#polyMoins', 'mousedown', function () {
              if (stor.disab) return
              if (nb === 3) { stor.mtgAppLecteur.getDoc(svgId).defaultCursor = '' } else {
                setTimeout(() => {
                  stor.mtgAppLecteur.setLineStyle(svgId, '#polyMoins', 0, 4, true)
                  stor.mtgAppLecteur.setColor(svgId, '#polyMoins', 255, 0, 0, true, 1)
                }, 1)
              }
              afficheNcase(svgId, ligne, dou, nb - 1, mul)
            })
          }
        }
      } else {
        stor.mtgAppLecteur.setVisible(svgId, '#polyNbPart', true)
        stor.mtgAppLecteur.setPointPosition(svgId, '#dirige11', 20 + (baseX - 20) / 4, 100, true)
        stor.mtgAppLecteur.setPointPosition(svgId, '#dirige12', 20 + 2 * (baseX - 20) / 4, 100, true)
        stor.mtgAppLecteur.setPointPosition(svgId, '#dirige13', 20 + 3 * (baseX - 20) / 4, 100, true)
        stor.mtgAppLecteur.setPointPosition(svgId, '#dirige14', 20 + (baseX - 20), 100, true)
        stor.mtgAppLecteur.calculate(svgId)
        stor.mtgAppLecteur.setVisible(svgId, '#poly11', true)
        stor.mtgAppLecteur.setVisible(svgId, '#text111', true)
        stor.mtgAppLecteur.setVisible(svgId, '#text112', true)
        stor.mtgAppLecteur.setText(svgId, '#text111', (stor.yakoi[1][1].nom === '') ? '?' : decoupe(stor.yakoi[1][1].nom, 2))
        stor.mtgAppLecteur.setText(svgId, '#text112', (stor.yakoi[1][1].val === '') ? '?' : stor.yakoi[1][1].val)
        stor.mtgAppLecteur.setVisible(svgId, '#poly14', true)
        stor.mtgAppLecteur.setVisible(svgId, '#text141', true)
        stor.mtgAppLecteur.setVisible(svgId, '#text142', true)
        stor.mtgAppLecteur.setText(svgId, '#text141', (stor.yakoi[1][1].nom === '') ? '?' : decoupe(stor.yakoi[1][1].nom, 2))
        stor.mtgAppLecteur.setText(svgId, '#text142', (stor.yakoi[1][1].val === '') ? '?' : stor.yakoi[1][1].val)
        stor.mtgAppLecteur.setVisible(svgId, '#poly12', true)
        stor.mtgAppLecteur.setVisible(svgId, '#text121', true)
        stor.mtgAppLecteur.setVisible(svgId, '#text122', true)
        stor.mtgAppLecteur.setText(svgId, '#text121', (stor.yakoi[1][1].nom === '') ? '?' : decoupe(stor.yakoi[1][1].nom, 2))
        stor.mtgAppLecteur.setText(svgId, '#text122', (stor.yakoi[1][1].val === '') ? '?' : stor.yakoi[1][1].val)
        stor.mtgAppLecteur.setVisible(svgId, '#poly13', false)
        stor.mtgAppLecteur.setText(svgId, '#aff13', '••••••••••••')
        stor.mtgAppLecteur.addEventListener(svgId, '#poly11', 'mouseover', function () {
          if (stor.disab) return
          stor.mtgAppLecteur.getDoc(svgId).defaultCursor = 'pointer'
          stor.mtgAppLecteur.setVisible(svgId, '#im11', true)
          stor.mtgAppLecteur.setVisible(svgId, '#surf11', true)
          stor.mtgAppLecteur.setVisible(svgId, '#im12', true)
          stor.mtgAppLecteur.setVisible(svgId, '#surf12', true)
          stor.mtgAppLecteur.setVisible(svgId, '#im14', true)
          stor.mtgAppLecteur.setVisible(svgId, '#surf14', true)
        })
        stor.mtgAppLecteur.addEventListener(svgId, '#poly11', 'mouseout', function () {
          if (stor.disab) return
          stor.mtgAppLecteur.getDoc(svgId).defaultCursor = ''
          stor.mtgAppLecteur.setVisible(svgId, '#im11', false)
          stor.mtgAppLecteur.setVisible(svgId, '#surf11', false)
          stor.mtgAppLecteur.setVisible(svgId, '#im12', false)
          stor.mtgAppLecteur.setVisible(svgId, '#surf12', false)
          stor.mtgAppLecteur.setVisible(svgId, '#im14', false)
          stor.mtgAppLecteur.setVisible(svgId, '#surf14', false)
        })
        stor.mtgAppLecteur.addEventListener(svgId, '#poly11', 'mousedown', function () {
          if (stor.disab) return
          const k = {}
          k.i = 1
          k.n = 1
          faitEdit(k)
        })
        stor.mtgAppLecteur.addEventListener(svgId, '#poly14', 'mouseover', function () {
          if (stor.disab) return
          stor.mtgAppLecteur.getDoc(svgId).defaultCursor = 'pointer'
          stor.mtgAppLecteur.setVisible(svgId, '#im11', true)
          stor.mtgAppLecteur.setVisible(svgId, '#surf11', true)
          stor.mtgAppLecteur.setVisible(svgId, '#im12', true)
          stor.mtgAppLecteur.setVisible(svgId, '#surf12', true)
          stor.mtgAppLecteur.setVisible(svgId, '#im14', true)
          stor.mtgAppLecteur.setVisible(svgId, '#surf14', true)
        })
        stor.mtgAppLecteur.addEventListener(svgId, '#poly14', 'mouseout', function () {
          if (stor.disab) return
          stor.mtgAppLecteur.getDoc(svgId).defaultCursor = ''
          stor.mtgAppLecteur.setVisible(svgId, '#im11', false)
          stor.mtgAppLecteur.setVisible(svgId, '#surf11', false)
          stor.mtgAppLecteur.setVisible(svgId, '#im12', false)
          stor.mtgAppLecteur.setVisible(svgId, '#surf12', false)
          stor.mtgAppLecteur.setVisible(svgId, '#im14', false)
          stor.mtgAppLecteur.setVisible(svgId, '#surf14', false)
        })
        stor.mtgAppLecteur.addEventListener(svgId, '#poly14', 'mousedown', function () {
          if (stor.disab) return
          const k = {}
          k.i = 1
          k.n = 1
          faitEdit(k)
        })
        stor.mtgAppLecteur.addEventListener(svgId, '#poly12', 'mouseover', function () {
          if (stor.disab) return
          stor.mtgAppLecteur.getDoc(svgId).defaultCursor = 'pointer'
          stor.mtgAppLecteur.setVisible(svgId, '#im11', true)
          stor.mtgAppLecteur.setVisible(svgId, '#surf11', true)
          stor.mtgAppLecteur.setVisible(svgId, '#im12', true)
          stor.mtgAppLecteur.setVisible(svgId, '#surf12', true)
          stor.mtgAppLecteur.setVisible(svgId, '#im14', true)
          stor.mtgAppLecteur.setVisible(svgId, '#surf14', true)
        })
        stor.mtgAppLecteur.addEventListener(svgId, '#poly12', 'mouseout', function () {
          if (stor.disab) return
          stor.mtgAppLecteur.getDoc(svgId).defaultCursor = ''
          stor.mtgAppLecteur.setVisible(svgId, '#im11', false)
          stor.mtgAppLecteur.setVisible(svgId, '#surf11', false)
          stor.mtgAppLecteur.setVisible(svgId, '#im12', false)
          stor.mtgAppLecteur.setVisible(svgId, '#surf12', false)
          stor.mtgAppLecteur.setVisible(svgId, '#im14', false)
          stor.mtgAppLecteur.setVisible(svgId, '#surf14', false)
        })
        stor.mtgAppLecteur.addEventListener(svgId, '#poly12', 'mousedown', function () {
          if (stor.disab) return
          const k = {}
          k.i = 1
          k.n = 1
          faitEdit(k)
        })

        stor.mtgAppLecteur.addEventListener(svgId, '#polyNbPart', 'mouseover', function () {
          if (stor.disab) return
          stor.mtgAppLecteur.getDoc(svgId).defaultCursor = 'pointer'
          stor.mtgAppLecteur.setVisible(svgId, '#imNpPart', true)
          stor.mtgAppLecteur.setVisible(svgId, '#surfNbPart', true)
        })
        stor.mtgAppLecteur.addEventListener(svgId, '#polyNbPart', 'mouseout', function () {
          if (stor.disab) return
          stor.mtgAppLecteur.getDoc(svgId).defaultCursor = ''
          stor.mtgAppLecteur.setVisible(svgId, '#imNpPart', false)
          stor.mtgAppLecteur.setVisible(svgId, '#surfNbPart', false)
        })
        stor.mtgAppLecteur.addEventListener(svgId, '#polyNbPart', 'mousedown', function () {
          if (stor.disab) return
          const k = {}
          k.i = 1
          k.n = 100
          faitEdit(k)
        })

        stor.mtgAppLecteur.setVisible(svgId, '#parab1', true)
        stor.mtgAppLecteur.setVisible(svgId, '#parab2', true)
        stor.mtgAppLecteur.setVisible(svgId, '#parab3', true)
        stor.mtgAppLecteur.setVisible(svgId, '#parab4', true)
        stor.mtgAppLecteur.setVisible(svgId, '#parab5', true)
        stor.mtgAppLecteur.setVisible(svgId, '#parab6', true)
        stor.mtgAppLecteur.setVisible(svgId, '#partKoi', true)
        stor.mtgAppLecteur.setVisible(svgId, '#partVal', true)
        stor.mtgAppLecteur.setVisible(svgId, '#text11002', true)
        stor.mtgAppLecteur.setVisible(svgId, '#text11001', true)
        stor.mtgAppLecteur.setText(svgId, '#text11002', (stor.yakoi[1][100].val === '') ? '?' : stor.yakoi[1][100].val)
        stor.mtgAppLecteur.setText(svgId, '#text11001', (stor.yakoi[1][100].nom === '') ? '?' : stor.yakoi[1][100].nom)
      }
    }
  }
  function sortEdit (k) {
    const adec = (k.i === 0) ? 1 : (stor.repFois) ? 2 : (stor.nbCases < 4) ? 1 : (stor.nbCases < 6) ? 2 : 3
    switch (ds.Question) {
      case 'Diagramme_tout':
        stor.newCont.disable()
        stor.yakoi[k.i][k.n].nom = (stor.newCont.reponse !== 'Choisir') ? stor.newCont.reponse : stor.yakoi[k.i][k.n].nom
        stor.mtgAppLecteur.setText(svgId, '#text' + k.i + k.n + '1', stor.yakoi[k.i][k.n].nom !== '' ? decoupe(stor.yakoi[k.i][k.n].nom, adec) : '?', true)
        stor.mtgAppLecteur.setColor(svgId, '#text' + k.i + k.n + '1', 0, 0, 255)
        if (k.i === 1 && k.n === 1 && stor.lexo.prob.op === '*') {
          stor.mtgAppLecteur.setText(svgId, '#text' + k.i + '21', stor.yakoi[k.i][k.n].nom !== '' ? stor.yakoi[k.i][k.n].nom : '?', true)
          stor.mtgAppLecteur.setText(svgId, '#text' + k.i + '41', stor.yakoi[k.i][k.n].nom !== '' ? stor.yakoi[k.i][k.n].nom : '?', true)
        }
        stor.newval.disable()
        stor.yakoi[k.i][k.n].val = (stor.newval.reponse().replace(/ /g, '') !== '') ? '$' + stor.newval.reponse().replace(/ /g, '') + '$' : ''
        stor.mtgAppLecteur.setText(svgId, '#text' + k.i + k.n + '2', stor.yakoi[k.i][k.n].val !== '' ? stor.yakoi[k.i][k.n].val : '?', true)
        stor.mtgAppLecteur.setColor(svgId, '#text' + k.i + k.n + '2', 0, 255, 0)
        if (stor.yakoi[k.i][k.n].nom === stor.nomblok) {
          const foav = stor.yakoi[k.i][k.n].val !== ''
          stor.yakoi[k.i][k.n].val = ''
          stor.mtgAppLecteur.setText(svgId, '#text' + k.i + k.n + '2', '?', true)
          stor.mtgAppLecteur.setColor(svgId, '#text' + k.i + k.n + '2', 0, 255, 0)
          if (foav) {
            j3pDetruit('modale')
            j3pDetruit('j3pmasque')
            const yy = j3pModale({ titre: 'Message', contenu: '' })
            const jkl = addDefaultTable(yy, 2, 1)
            j3pAddContent(jkl[0][0], 'Tu ne pourras modifier cette valeur <br> qu’après la validation du schéma.')
            const laff = () => {
              j3pDetruit('modale')
              j3pDetruit('j3pmasque')
            }
            j3pAjouteBouton(jkl[1][0], laff, { className: '', value: 'OK' })
            return
          }
        }
        if (k.i === 1 && k.n === 1 && stor.repFois) {
          stor.mtgAppLecteur.setText(svgId, '#text' + k.i + '41', stor.yakoi[k.i][k.n].nom !== '' ? decoupe(stor.yakoi[k.i][k.n].nom, adec) : '?', true)
          stor.mtgAppLecteur.setText(svgId, '#text' + k.i + '21', stor.yakoi[k.i][k.n].nom !== '' ? decoupe(stor.yakoi[k.i][k.n].nom, adec) : '?', true)
          stor.mtgAppLecteur.setText(svgId, '#text' + k.i + '22', stor.yakoi[k.i][k.n].val !== '' ? stor.yakoi[k.i][k.n].val : '?', true)
          stor.mtgAppLecteur.setText(svgId, '#text' + k.i + '42', stor.yakoi[k.i][k.n].val !== '' ? stor.yakoi[k.i][k.n].val : '?', true)
          stor.mtgAppLecteur.setColor(svgId, '#text' + k.i + '42', 0, 255, 0)
          stor.mtgAppLecteur.setColor(svgId, '#text' + k.i + '22', 0, 255, 0)
          stor.mtgAppLecteur.setColor(svgId, '#text' + k.i + '41', 0, 0, 255)
          stor.mtgAppLecteur.setColor(svgId, '#text' + k.i + '21', 0, 0, 255)
        }
        break
      case 'Diagramme_nom':
        stor.newCont.disable()
        stor.yakoi[k.i][k.n].nom = (stor.newCont.reponse !== 'Choisir') ? stor.newCont.reponse : stor.yakoi[k.i][k.n].nom
        stor.mtgAppLecteur.setText(svgId, '#text' + k.i + k.n + '1', stor.yakoi[k.i][k.n].nom !== '' ? decoupe(stor.yakoi[k.i][k.n].nom, adec) : '?', true)
        stor.mtgAppLecteur.setColor(svgId, '#text' + k.i + k.n + '1', 0, 0, 255)
        if (k.i === 1 && k.n === 1 && stor.repFois) {
          stor.mtgAppLecteur.setText(svgId, '#text' + k.i + '21', stor.yakoi[k.i][k.n].nom !== '' ? decoupe(stor.yakoi[k.i][k.n].nom, adec) : '?', true)
          stor.mtgAppLecteur.setText(svgId, '#text' + k.i + '41', stor.yakoi[k.i][k.n].nom !== '' ? decoupe(stor.yakoi[k.i][k.n].nom, adec) : '?', true)
          stor.mtgAppLecteur.setColor(svgId, '#text' + k.i + '21', 0, 0, 255)
          stor.mtgAppLecteur.setColor(svgId, '#text' + k.i + '41', 0, 0, 255)
        }
        break
      case 'Diagramme_nombre':
        stor.newval.disable()
        stor.yakoi[k.i][k.n].val = (stor.newval.reponse().replace(/ /g, '') !== '') ? '$' + stor.newval.reponse().replace(/ /g, '') + '$' : ''
        stor.mtgAppLecteur.setText(svgId, '#text' + k.i + k.n + '2', stor.yakoi[k.i][k.n].val !== '' ? stor.yakoi[k.i][k.n].val : '?', true)
        stor.mtgAppLecteur.setColor(svgId, '#text' + k.i + k.n + '2', 0, 255, 0)
        if (k.i === 1 && k.n === 1 && stor.repFois) {
          stor.mtgAppLecteur.setText(svgId, '#text' + k.i + '22', stor.yakoi[k.i][k.n].val !== '' ? stor.yakoi[k.i][k.n].val : '?', true)
          stor.mtgAppLecteur.setText(svgId, '#text' + k.i + '42', stor.yakoi[k.i][k.n].val !== '' ? stor.yakoi[k.i][k.n].val : '?', true)
          stor.mtgAppLecteur.setColor(svgId, '#text' + k.i + '42', 0, 255, 0)
          stor.mtgAppLecteur.setColor(svgId, '#text' + k.i + '22', 0, 255, 0)
        }
        break
    }
    j3pDetruit('modale')
    j3pDetruit('j3pmasque')
  }
  function sortEditQ (k) {
    switch (ds.Question) {
      case 'Diagramme_tout':
        break
      case 'Diagramme_nom':
        stor.newCont.disable()
        break
      case 'Diagramme_nombre':
        stor.newval.disable()
        break
    }
    stor.mtgAppLecteur.setText(svgId, k.af, k.val || '?', true)
    j3pDetruit('modale')
    j3pDetruit('j3pmasque')
  }
  function decoupe (st, nb) {
    if (nb === 1) return st
    const leng = st.length
    if (nb === 2) {
      const cp = st.substring(leng / 2)
      const dou = cp.indexOf(' ')
      return st.substring(0, dou + leng / 2) + '\n' + st.substring(dou + leng / 2)
    }
    const cp = st.substring(leng / 3)
    const dou = cp.indexOf(' ')
    return st.substring(0, dou + leng / 3) + '\n' + decoupe(st.substring(dou + leng / 3), 2)
  }

  function affMaVal (val) {
    if (!val) return ''
    if (stor.den !== 1) {
      const pgc = j3pPGCD(val, stor.den)
      if (j3pArrondi(stor.den / pgc, 0) !== 1) return '$\\frac{' + ecrisBienMathquill(j3pArrondi(val / pgc, 0)) + '}{' + j3pArrondi(stor.den / pgc, 0) + '}'
      return ecrisBienMathquill(j3pArrondi(val / pgc, 0))
    } else {
      return '$' + ecrisBienMathquill(val) + '$'
    }
  }
  function bonneReponse () {
    stor.errTypeOp = false
    stor.errTotNom = false
    stor.errTotVal = false
    stor.err2foislememe = false
    stor.errMankGrandList = []
    stor.errpetitokVal = false
    stor.errNomPart = false
    stor.errValPart = false
    stor.errPlusNbPart = false
    stor.errPlusNbPartInc = false
    stor.errPartNom = false
    stor.errPartVal = false
    stor.errNbPartNom = false
    stor.errNbPartVal = false
    if (stor.enc) {
      let ok = true
      if (stor.lexo.prob.op === '+') {
        if (stor.repFois) {
          stor.errTypeOp = true
          return false
        }
        if (stor.yakoi[0][0].nom !== stor.lexo.prob.total) {
          stor.errTotNom = true
          stor.mtgAppLecteur.setColor(svgId, '#text001', 255, 0, 0, true)
          stor.mtgAppLecteur.setText(svgId, '#text001', stor.yakoi[0][0].nom)
          ok = false
        }
        if (stor.yakoi[0][0].val !== '' && Number(stor.yakoi[0][0].val.replace(/\$/g, '')) !== stor.total) {
          stor.errTotVal = true
          stor.mtgAppLecteur.setColor(svgId, '#text002', 255, 0, 0)
          stor.mtgAppLecteur.setText(svgId, '#text002', stor.yakoi[0][0].val)
          ok = false
        }
        // verif yatous les petit+
        // avec leur bonnes valeurs
        // verif yen a pas deux fois
        // verif le découpage est bon ?
        for (let i = 0; i < stor.lexo.prob.decoupes.length; i++) {
          let petitokTrouve = false
          let petitokVal = true
          for (let j = 1; j < stor.nbCases + 1; j++) {
            if (stor.yakoi[1][j].nom === stor.lexo.prob.decoupes[i]) {
              if (petitokTrouve) {
                stor.err2foislememe = true
                stor.mtgAppLecteur.setColor(svgId, '#text1' + j + '1', 255, 0, 0)
                stor.mtgAppLecteur.setText(svgId, '#text1' + j + '1', stor.yakoi[1][j].nom)
                break
              }
              petitokTrouve = true
              if (stor.yakoi[1][j].val !== '' && Number(stor.yakoi[1][j].val.replace(/\$/g, '')) !== stor.valDecoupe[i]) {
                stor.errpetitokVal = true
                stor.mtgAppLecteur.setColor(svgId, '#text1' + j + '2', 255, 0, 0)
                stor.mtgAppLecteur.setText(svgId, '#text1' + j + '2', stor.yakoi[1][j].val)
                petitokVal = false
              }
            }
          }
          if (!petitokTrouve) {
            stor.errMankGrandList.push(stor.lexo.prob.decoupes[i])
            ok = false
          }
          if (!petitokVal) ok = false
        }
      } else {
        if (stor.yakoi[0][0].nom !== stor.lexo.prob.total) {
          stor.errTotNom = true
          stor.mtgAppLecteur.setColor(svgId, '#text001', 255, 0, 0)
          stor.mtgAppLecteur.setText(svgId, '#text001', stor.yakoi[0][0].nom)
          ok = false
        }
        if (stor.yakoi[0][0].val !== '' && Number(stor.yakoi[0][0].val.replace(/\$/g, '')) !== stor.total) {
          stor.errTotVal = true
          stor.mtgAppLecteur.setColor(svgId, '#text002', 255, 0, 0)
          stor.mtgAppLecteur.setText(svgId, '#text002', stor.yakoi[0][0].val)
          ok = false
        }
        if (!stor.repFois) {
          for (let i = 1; i < stor.nbCases + 1; i++) {
            if (stor.yakoi[1][1].nom !== stor.lexo.prob.tailepart) {
              stor.errNomPart = true
              stor.mtgAppLecteur.setColor(svgId, '#text1' + i + '1', 255, 0, 0)
              stor.mtgAppLecteur.setText(svgId, '#text1' + i + '1', stor.yakoi[1][1].nom)
              ok = false
            }
            if (stor.yakoi[1][i].val !== '' && Number(stor.yakoi[1][i].val.replace(/\$/g, '')) !== stor.taillePart) {
              stor.errValPart = true
              stor.mtgAppLecteur.setColor(svgId, '#text1' + i + '2', 255, 0, 0)
              stor.mtgAppLecteur.setText(svgId, '#text1' + i + '2', stor.yakoi[1][i].val)
              ok = false
            }
          }
          if (stor.rechDiv === 'nbpart') {
            stor.errPlusNbPartInc = true
            ok = false
          } else {
            if (stor.nbCases !== stor.nbpart) {
              stor.errPlusNbPart = true
              ok = false
            }
          }
        } else {
          // la part
          if (stor.yakoi[1][1].nom !== stor.lexo.prob.tailepart) {
            stor.errPartNom = true
            stor.mtgAppLecteur.setColor(svgId, '#text111', 255, 0, 0)
            stor.mtgAppLecteur.setColor(svgId, '#text121', 255, 0, 0)
            stor.mtgAppLecteur.setColor(svgId, '#text141', 255, 0, 0)
            stor.mtgAppLecteur.setText(svgId, '#text111', stor.yakoi[1][1].nom)
            stor.mtgAppLecteur.setText(svgId, '#text121', stor.yakoi[1][1].nom)
            stor.mtgAppLecteur.setText(svgId, '#text141', stor.yakoi[1][1].nom)
            ok = false
          }
          if (stor.yakoi[1][1].val !== '' && Number(stor.yakoi[1][1].val.replace(/\$/g, '')) !== stor.taillePart) {
            stor.errPartVal = true
            stor.mtgAppLecteur.setColor(svgId, '#text112', 255, 0, 0)
            stor.mtgAppLecteur.setColor(svgId, '#text122', 255, 0, 0)
            stor.mtgAppLecteur.setColor(svgId, '#text142', 255, 0, 0)
            stor.mtgAppLecteur.setText(svgId, '#text112', stor.yakoi[1][1].val)
            stor.mtgAppLecteur.setText(svgId, '#text122', stor.yakoi[1][1].val)
            stor.mtgAppLecteur.setText(svgId, '#text142', stor.yakoi[1][1].val)
            ok = false
          }
          // nbpart
          if (stor.yakoi[1][100].nom !== stor.lexo.prob.nbpart) {
            stor.errNbPartNom = true
            stor.mtgAppLecteur.setColor(svgId, '#text11001', 255, 0, 0)
            stor.mtgAppLecteur.setText(svgId, '#text11001', stor.yakoi[1][100].nom)
            ok = false
          }
          if (stor.yakoi[1][100].val !== '' && Number(stor.yakoi[1][100].val.replace(/\$/g, '')) !== stor.nbpart) {
            stor.errNbPartVal = true
            stor.mtgAppLecteur.setColor(svgId, '#text11002', 255, 0, 0)
            stor.mtgAppLecteur.setText(svgId, '#text11002', stor.yakoi[1][100].val)
            ok = false
          }
        }
      }
      return ok
    } else {
      const larp = nettoie(stor.laz.reponse())
      stor.listePourCalc.giveFormula2('x', larp)
      stor.listePourCalc.calculateNG()
      const obt = stor.listePourCalc.valueOf('x')
      return obt === Number(stor.asouv.replace(/\$/g, '').replace(/,/g, '.'))
    }
  }
  function nettoie (s) {
    s = s.replace(/ /g, '')
    while (s.indexOf('\\times') !== -1) s = s.replace('\\times', '*')
    while (s.indexOf('\\div') !== -1) s = s.replace('\\div', '/')
    while (s.indexOf('\\text{') !== -1) s = s.replace('\\text{', '')
    while (s.indexOf('}') !== -1) s = s.replace('}', '')
    return s
  }
  function yareponse () {
    if (me.isElapsed) { return true }
    if (stor.enc) {
      // on fait les liste des reponse !== de '', il doit en manquer qu’une
      let cpt = 0
      let cpttout = 2
      if (stor.yakoi[0][0].nom !== '') cpt++
      if (stor.yakoi[0][0].val !== '') cpt++
      if (stor.repFois) {
        cpttout += 4
        if (stor.yakoi[1][100].nom !== '') cpt++
        if (stor.yakoi[1][100].val !== '') cpt++
        if (stor.yakoi[1][1].nom !== '') cpt++
        if (stor.yakoi[1][1].val !== '') cpt++
      } else {
        cpttout += 2 * stor.nbCases
        for (let i = 1; i < stor.nbCases + 1; i++) {
          if (stor.yakoi[1][i].nom !== '') cpt++
          if (stor.yakoi[1][i].val !== '') cpt++
        }
      }
      return cpt + 1 === cpttout
    }
    return true
  }
  function disaAll () {
    if (stor.enc) {
      stor.disab = true
      stor.mtgAppLecteur.setVisible(svgId, '#imPlus', false)
      stor.mtgAppLecteur.setVisible(svgId, '#imMoins', false)
      stor.mtgAppLecteur.setVisible(svgId, '#polyPlus', false)
      stor.mtgAppLecteur.setVisible(svgId, '#polyMoins', false)
      stor.mtgAppLecteur.setVisible(svgId, '#polyNbPart', false)
      j3pEmpty(stor.lesdiv.zoneChange)
    } else {
      j3pEmpty(stor.aclear)
      stor.laz.disable()
    }
  }
  function metToutvert () {
    if (stor.enc) {
      stor.mtgAppLecteur.setVisible(svgId, '#surfco', true)
      stor.mtgAppLecteur.setColor(svgId, '#surfco', 130, 255, 100, true)
    } else { stor.laz.corrige(true) }
  }
  function barrelesFo () {
    if (stor.enc) {
      stor.mtgAppLecteur.setVisible(svgId, '#surfco', true)
      stor.mtgAppLecteur.setColor(svgId, '#surfco', 233, 80, 20, true)
    } else { stor.laz.barre() }
  }
  function AffcorFo (bool) {
    if (stor.errTypeOp) {
      stor.yaexplik = true
      j3pAddContent(stor.lesdiv.explications, 'Ce n’est pas la bonne représentation !<br>')
    }
    if (stor.errTotNom) {
      stor.yaexplik = true
      j3pAddContent(stor.lesdiv.explications, 'La grandeur correspondant au tout n’est pas correcte pas !<br>')
    }
    if (stor.errTotVal) {
      stor.yaexplik = true
      j3pAddContent(stor.lesdiv.explications, 'La valeur correspondant au tout n’est pas correcte pas !<br>')
    }
    if (stor.err2foislememe) {
      stor.yaexplik = true
      j3pAddContent(stor.lesdiv.explications, 'La même grandeur apparaît deux fois !<br>')
    }
    if (stor.errMankGrandList.length > 1) {
      stor.yaexplik = true
      for (let i = 0; i < stor.errMankGrandList.length; i++) {
        j3pAddContent(stor.lesdiv.explications, 'Manque la grandeur ' + stor.errMankGrandList[i] + '!<br>')
      }
    }
    if (stor.errpetitokVal) {
      stor.yaexplik = true
      j3pAddContent(stor.lesdiv.explications, 'Les valeurs ne correspondent pas toutes à leur grandeur (revoir l’énoncé) !<br>')
    }
    if (stor.errNomPart) {
      stor.yaexplik = true
      j3pAddContent(stor.lesdiv.explications, 'Une grandeur indiquée ne correspond pas !<br>')
    }
    if (stor.errValPart) {
      stor.yaexplik = true
      j3pAddContent(stor.lesdiv.explications, 'Les valeurs ne correspondent pas toutes à leur grandeur (revoir l’énoncé) !<br>')
    }
    if (stor.errPlusNbPart) {
      stor.yaexplik = true
      j3pAddContent(stor.lesdiv.explications, 'Le nombre de parties ne correspond pas aux données !<br>')
    }
    if (stor.errPlusNbPartInc) {
      stor.yaexplik = true
      j3pAddContent(stor.lesdiv.explications, 'Le nombre de parties n’est pas donné dans l’énoncé !<br>')
    }
    if (stor.errPartNom) {
      stor.yaexplik = true
      j3pAddContent(stor.lesdiv.explications, 'Une grandeur indiquée ne correspond pas !<br>')
    }
    if (stor.errPartVal) {
      stor.yaexplik = true
      j3pAddContent(stor.lesdiv.explications, 'Les valeurs ne correspondent pas toutes à leur grandeur (revoir l’énoncé) !<br>')
    }
    if (stor.errNbPartNom) {
      stor.yaexplik = true
      j3pAddContent(stor.lesdiv.explications, 'Une grandeur indiquée ne correspond pas !<br>')
    }
    if (stor.errNbPartVal) {
      stor.yaexplik = true
      j3pAddContent(stor.lesdiv.explications, 'Les valeurs ne correspondent pas toutes à leur grandeur (revoir l’énoncé) !<br>')
    }

    if (bool) {
      barrelesFo()
      disaAll()
      if (stor.enc) {
        stor.foco = true
        const svgId2 = j3pGetNewId('mt')
        stor.yakoi = j3pClone(stor.yakoiGarde)
        stor.yakoi[stor.ablok.ligne][stor.ablok.ca].val = ''
        makefig(svgId2, stor.lesdiv.figco, true)
        stor.mtgAppLecteur.setVisible(svgId2, '#polyNbPart', false)
        stor.mtgAppLecteur.setVisible(svgId2, '#surfco', true)
        stor.mtgAppLecteur.setColor(svgId2, '#surfco', 150, 150, 255, true)
        stor.leboutco = j3pAjouteBouton(stor.lesdiv.etape, faisvoirCo, { value: 'Voir une correction' })
        stor.voviz = false
        stor.lesdiv.figco.style.display = 'none'
      } else {
        let toAf = stor.nomblok + ' $ = '
        let deb = true
        switch (stor.lexo.op) {
          case '+':
            for (let i = 0; i < stor.lexo.prob.decoupes.length; i++) {
              const toAd = (!deb) ? stor.valDecoupe[i] : ' + ' + stor.valDecoupe[i]
              deb = false
              toAf += toAd
            }
            break
          case '-':
            toAf += stor.total
            for (let i = 0; i < stor.lexo.prob.decoupes.length; i++) {
              if (i === stor.liPourMoins - 1) continue
              toAf += ' - ' + stor.valDecoupe[i]
            }
            break
          case '*':
            toAf += stor.nbpart + ' \\times ' + stor.taillePart
            break
          case '/1':
            toAf += stor.total + ' \\div ' + stor.nbpart
            break
          case '/2':
            toAf += stor.total + ' \\div ' + stor.taillePart
            break
        }
        j3pAffiche(stor.lesdiv.solution, null, toAf + '$')
      }
    }
  }
  function faisvoirCo () {
    stor.voviz = !stor.voviz
    stor.leboutco.value = (stor.voviz) ? 'Voir ma réponse' : 'voir une correction'
    stor.lesdiv.figco.style.display = (stor.voviz) ? '' : 'none'
    stor.lesdiv.fig.style.display = (stor.voviz) ? 'none' : ''
  }
  function enonceMain () {
    if (me.etapeCourante === 1) {
      me.videLesZones()
      creeLesDiv()
      stor.lexo = faisChoixExos()
      poseQuestion()
      svgId = j3pGetNewId('mtg')
      stor.svgId = svgId
      stor.mtgAppLecteur.removeAllDoc()
      makefig(svgId, stor.lesdiv.fig)
    } else {
      poseQuestion()
    }
    me.ajouteBoutons()
    me.cacheBoutonSectionSuivante()

    me.finEnonce()
  }

  switch (this.etat) {
    case 'enonce': {
      // code exécuté au lancement de la section

      if (this.debutDeLaSection) {
        initSection()
      } else {
        enonceMain()
      }
      break
    } // case "enonce":

    case 'correction':
      // On teste pas si une réponse a été saisie
      stor.yaco1 = false
      stor.yaexplik = false
      if (ds.Total) j3pEmpty(stor.lesdiv.explRais)
      if (!yareponse()) {
        stor.lesdiv.correction.style.color = this.styles.cfaux
        stor.lesdiv.correction.innerHTML = reponseIncomplete
        this.afficheBoutonValider()
      } else {
        if (bonneReponse()) {
          this.score++
          stor.lesdiv.correction.style.color = this.styles.cbien
          stor.lesdiv.correction.innerHTML = cBien

          disaAll()
          metToutvert()

          this.typederreurs[0]++
          this.cacheBoutonValider()
          this.etat = 'navigation'
          this.sectionCourante()
        } else {
          // Pas de bonne réponse
          stor.lesdiv.correction.style.color = this.styles.cfaux

          // A cause de la limite de temps :
          if (this.isElapsed) { // limite de temps
            stor.lesdiv.correction.innerHTML = tempsDepasse
            this.typederreurs[10]++
            this.cacheBoutonValider()
            AffcorFo(true)
            this.etat = 'navigation'
            this.sectionCourante()
          } else {
            stor.lesdiv.correction.innerHTML = cFaux
            if (this.essaiCourant < this.donneesSection.nbchances) {
              stor.lesdiv.correction.innerHTML += '<br>' + essaieEncore
              AffcorFo(false)
              this.typederreurs[1]++
            } else {
              this.cacheBoutonValider()
              stor.lesdiv.correction.innerHTML += '<br>' + regardeCorrection
              AffcorFo(true)
              this.typederreurs[2]++
              this.etat = 'navigation'
              this.sectionCourante()
            }
          }
        }
      }
      // Obligatoire

      if (ds.theme === 'zonesAvecImageDeFond') {
        if (stor.yaexplik) stor.lesdiv.explications.classList.add('explique')
        if (stor.yaco1) stor.lesdiv.solution.classList.add('correction')
      }
      this.finCorrection()
      break // case "correction":

    case 'navigation':

      if (this.sectionTerminee()) {
        // On détermine la phrase d’état renvoyée par la section
        this.parcours.pe = (this.donneesSection.nbetapes * this.score) / this.donneesSection.nbitems
        this.afficheBoutonSectionSuivante()
        this.focus('sectioncontinuer')
      } else {
        this.etat = 'enonce'
        this.afficheBoutonSuite()
        this.focus('boutoncontinuer')
      }
      // Obligatoire
      this.finNavigation()

      break // case "navigation":
  }
}
