import { j3pDiv, j3pEmpty, j3pGetNewId, j3pGetRandomBool, j3pGetRandomInt, j3pShowError, j3pShuffle } from 'src/legacy/core/functions'
import BulleAide from 'src/legacy/outils/bulleAide/BulleAide'
import { j3pAffiche } from 'src/lib/mathquill/functions'
import { addDefaultTable } from 'src/legacy/themes/table'
import { j3pCreeSVG } from 'src/legacy/core/functionsSvg'
import { getMtgCore } from 'src/lib/outils/mathgraph'
import { addMaj } from 'src/lib/utils/string'
import textesGeneriques from 'src/lib/core/textes'

const { cBien, cFaux, essaieEncore, reponseIncomplete, tempsDepasse } = textesGeneriques

/**
 * Ajouter une description ici
 * @fileOverview
 * @author Tommy Barroy <t.barroy@laposte.net>
 * @since 2017-10
*/

export const params = {
  outils: ['mathquill'],
  /**
   * Ce qui est paramétrable dans l’éditeur de graphe
   * @type {LegacySectionParamsParametres}
   */
  parametres: [
    ['nbrepetitions', 3, 'entier', 'Nombre de répétitions de la section'],
    ['nbchances', 2, 'entier', 'Nombre d’essais possibles.'],
    ['Axiale', true, 'boolean', '<u>true</u>: Symétrique axiale'],
    ['Centrale', true, 'boolean', '<u>true</u>: Symétrique centrale'],
    ['Translation', true, 'boolean', '<u>true</u>: Translation'],
    ['Rotation', true, 'boolean', '<u>true</u>: Translation'],
    ['Homothetie', true, 'boolean', '<u>true</u>: homothétie'],
    ['Point', true, 'boolean', '<u>true</u>: image d’un point'],
    ['Segment', true, 'boolean', '<u>true</u>: image d’un segment'],
    ['Triangle', true, 'boolean', '<u>true</u>: image d’un triangle'],
    ['Cercle', true, 'boolean', '<u>true</u>: image d’un cercle'],
    ['horizontale', true, 'boolean', '<u>true</u>: axe horizontal'],
    ['verticale', true, 'boolean', '<u>true</u>: axe vertical'],
    ['oblique', true, 'boolean', '<u>true</u>: axe oblique'],
    ['difficulte', 'linéaire', 'liste', '<u>linéaire</u>: il faut d’abord placer des points, puis les segments, puis les triangles, puis cercle <br><br> <u>aléatoire</u>: pas d’ordre', ['linéaire', 'aléatoire']],
    // le modèle gère la compatibilité ascendante avec l’ancien paramètre MiseEnPagePlusClaire
    ['theme', 'standard', 'liste', 'Modèle pour l’affichage', ['standard', 'zonesAvecImageDeFond']]

  ]
}
/**
 * section quad
 * @this {Parcours}
 */
/**
 * section symetriequad
 * @this {Parcours}
 */
export default function main () {
  const me = this
  const stor = me.storage
  let ds = me.donneesSection
  let svgId = stor.svgId

  function getDonnees () {
    return {
      // Nombre de répétitions de l’exercice
      nbrepetitions: 3,

      Axiale: true,
      Centrale: true,
      Translation: true,
      Rotation: true,
      Homothetie: true,
      Point: true,
      Segment: true,
      Cercle: true,
      Triangle: true,
      horizontale: true,
      verticale: true,
      oblique: true,
      difficulte: 'linéaire',

      // Si indication != "" alors un lien "Indication" s’affiche dans la zone Inférieure Gauche (this.zones.IG
      // une indication<br>de deux lignes..."
      indication: '',

      // pour le cas de presentation1bis
      // En effet pas d’indication possible pour cette présentation
      // surchargeindication: false,

      // Nombre de chances dont dispose l’él§ve pour répondre à la question
      nbchances: 2,

      //  || "presentation2" || "presentation3"  || "presentation1bis"
      structure: 'presentation3'
    }
  }

  function initSection () {
    let i, buf1, buf2

    ds = getDonnees()
    me.donneesSection = ds
    // On surcharge avec les parametres passés par le graphe
    me.surcharge()
    me.validOnEnter = false // ex donneesSection.touche_entree
    // Construction de la page
    me.construitStructurePage(ds.structure)
    ds.lesExos = []
    buf1 = ''
    let lp = []
    if (ds.Cercle) lp.push('cercle')
    if (ds.Point) lp.push('point')
    if (ds.Segment) lp.push('segment')
    if (ds.Triangle) lp.push('triangle')
    if (lp.length === 0) {
      j3pShowError('Erreur paramétrage')
      lp = ['point']
    }
    if (lp.length === 1) {
      buf1 = ' d’un ' + lp[0] + ' '
    }
    lp = j3pShuffle(lp)
    for (i = 0; i < ds.nbitems; i++) {
      ds.lesExos.push({ koi: lp[i % lp.length] })
    }
    ds.lesExos = j3pShuffle(ds.lesExos)

    lp = []
    if (ds.horizontale) lp.push('horizontale')
    if (ds.verticale) lp.push('verticale')
    if (ds.oblique) lp.push('oblique')
    if (lp.length === 0 && ds.Axiale) {
      j3pShowError('Erreur paramétrage')
      lp = ['verticale']
    }
    lp = j3pShuffle(lp)
    for (i = 0; i < ds.nbitems; i++) {
      ds.lesExos[i].dte = lp[i % lp.length]
    }
    ds.lesExos = j3pShuffle(ds.lesExos)

    lp = []
    buf2 = 'Transformation '
    if (ds.Axiale) lp.push('Axiale')
    if (ds.Centrale) lp.push('Centrale')
    if (ds.Translation) lp.push('Translation')
    if (ds.Rotation) lp.push('Rotation')
    if (ds.Homothetie) lp.push('Homothetie')
    if (lp.length === 0) {
      j3pShowError('Erreur paramétrage')
      lp = ['verticale']
    }
    if (lp.length === 2 && lp[0] === 'Axiale' && lp[1] === 'Centrale') buf2 = 'Symétries '
    if (lp.length === 1) {
      buf2 = lp[0]
      if (buf2 === 'Axiale') buf2 = 'Symétrie axiale '
      if (buf2 === 'Centrale') buf2 = 'Symétrie centrale '
    }
    lp = j3pShuffle(lp)
    for (i = 0; i < ds.nbitems; i++) {
      ds.lesExos[i].trans = lp[i % lp.length]
    }
    ds.lesExos = j3pShuffle(ds.lesExos)

    if (ds.difficulte === 'linéaire') {
      ds.lesExos = ds.lesExos.sort(function (a, b) {
        const ord = ['point', 'segment', 'triangle', 'cercle']
        return ord.indexOf(b.koi) - ord.indexOf(a.koi)
      })
    }
    me.typederreurs = [0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0]

    const tt = buf2 + buf1 + ' dans un quadrillage'

    me.afficheTitre(tt)

    getMtgCore({ withMathJax: true })
      .then(
        // success
        (mtgAppLecteur) => {
          stor.mtgAppLecteur = mtgAppLecteur
          enonceMain()
        },
        // failure
        (error) => {
          j3pShowError(error, { message: 'mathgraph n’est pas correctement chargé', mustNotify: true })
        })
      // plantage dans enonceMain
      .catch(j3pShowError)
  }

  function faisChoixExos () {
    const e = ds.lesExos.pop()
    stor.mA = false
    stor.mB = false
    stor.mDtAB = false
    stor.limmin = { x: 0, y: 0 }
    stor.limmax = { x: 0, y: 0 }
    stor.B = { x: 0, y: 0 }
    stor.nomspris = []

    switch (e.trans) {
      case 'Axiale': {
        const sdf = ['a', 'b', 'c', 'd', 'm', 't']
        stor.nomAB = '(' + sdf[j3pGetRandomInt(0, sdf.length - 1)] + ')'
        switch (e.dte) {
          case 'horizontale':
            stor.A = { y: j3pGetRandomInt(4, 5) }
            stor.A.x = 0
            stor.B = { x: 10, y: stor.A.y }
            stor.mDtAB = true
            stor.limmin.x = -10
            stor.limmax.x = 4
            stor.limmin.y = stor.A.y - 3
            stor.limmax.y = stor.A.y + 3
            stor.funcrep = function (pt) { return { x: pt.x, y: 2 * stor.A.y - pt.y } }
            break
          case 'verticale':
            stor.A = { x: j3pGetRandomInt(-5, -1) }
            stor.A.y = 0
            stor.B = { y: 10, x: stor.A.x }
            stor.mDtAB = true
            stor.limmin.x = stor.A.x - 5
            stor.limmax.x = stor.A.x + 5
            stor.limmin.y = 1
            stor.limmax.y = 6
            stor.funcrep = function (pt) { return { x: 2 * stor.A.x - pt.x, y: pt.y } }
            break
          case 'oblique':
            stor.A = { x: j3pGetRandomInt(-5, -1) }
            stor.A.y = 4
            stor.B = { y: 3, x: stor.A.x + Math.pow(-1, j3pGetRandomInt(0, 1)) }
            stor.mDtAB = true
            stor.limmin.x = -10
            stor.limmax.x = 4
            stor.limmin.y = 1
            stor.limmax.y = 6
            if (stor.A.x < stor.B.x) {
              stor.funcrep = function (pt) { return { x: stor.A.x + (pt.y - stor.A.y), y: stor.A.y - (stor.A.x - pt.x) } }
            } else {
              stor.funcrep = function (pt) { return { x: stor.A.x - (pt.y - stor.A.y), y: stor.A.y + (stor.A.x - pt.x) } }
            }
            break
        }
        break
      } // case Axiale

      case 'Centrale':
        stor.A = { y: j3pGetRandomInt(2, 4), x: j3pGetRandomInt(-6, 1) }
        stor.Anom = addMaj(stor.nomspris)
        stor.mA = true
        stor.limmin.x = -10
        stor.limmax.x = 4
        stor.limmin.y = 1
        stor.limmax.y = 6
        stor.funcrep = function (pt) { return { x: 2 * stor.A.x - pt.x, y: 2 * stor.A.y - pt.y } }
        break
      case 'Translation':
        stor.A = { y: j3pGetRandomInt(2, 5), x: j3pGetRandomInt(-6, 1) }
        stor.Anom = addMaj(stor.nomspris)
        stor.mA = true
        do {
          stor.B = { y: j3pGetRandomInt(2, 5), x: j3pGetRandomInt(-9, 5) }
        } while ((stor.B.x === stor.A.x && stor.B.y === stor.A.y) || (dist(stor.A, stor.B) > 6))
        stor.Bnom = addMaj(stor.nomspris)
        stor.mB = true
        stor.limmin.x = -10
        stor.limmax.x = 4
        stor.limmin.y = 1
        stor.limmax.y = 7
        stor.funcrep = function (pt) { return { x: pt.x + stor.B.x - stor.A.x, y: pt.y + stor.B.y - stor.A.y } }
        break
      case 'Rotation':
        stor.A = { y: 4, x: -1 }
        stor.Anom = addMaj(stor.nomspris)
        stor.mA = true
        stor.limmin.x = -10
        stor.limmax.x = 4
        stor.limmin.y = 1
        stor.limmax.y = 7
        stor.sens = j3pGetRandomBool()
        if (stor.sens) {
          stor.funcrep = function (pt) { return { x: stor.A.x - (pt.y - stor.A.y), y: stor.A.y + (pt.x - stor.A.x) } }
        } else {
          stor.funcrep = function (pt) { return { x: stor.A.x + (pt.y - stor.A.y), y: stor.A.y - (pt.x - stor.A.x) } }
        }
        break
      case 'Homothetie':
        stor.Anom = addMaj(stor.nomspris)
        stor.mA = true
        stor.rapbase = j3pGetRandomInt(2, 3)
        stor.yaf = j3pGetRandomBool()
        stor.yan = j3pGetRandomBool()
        stor.rap = stor.rapbase
        stor.rapAf = String(stor.rapbase)
        if (stor.yaf) {
          stor.rap = 1 / stor.rap
          stor.rapAf = '\\frac{1}{' + stor.rapAf + '}'
        }
        if (stor.yan) {
          stor.rap = -stor.rap
          stor.rapAf = '-' + stor.rapAf
        }
        if (stor.rapbase < 3) {
          stor.A = { y: 4, x: -1 }
        } else {
          if (j3pGetRandomBool()) {
            stor.A = { x: j3pGetRandomInt(-1, 2), y: 4 }
          } else {
            stor.A = { x: j3pGetRandomInt(-7, -4), y: 4 }
          }
        }
        stor.limmin.x = -10
        stor.limmax.x = 4
        stor.limmin.y = 1
        stor.limmax.y = 7
        stor.funcrep = function (pt) { return { x: stor.A.x + stor.rap * (pt.x - stor.A.x), y: stor.A.y + stor.rap * (pt.y - stor.A.y) } }
        break
    }

    stor.mP1 = stor.mP2 = stor.mP3 = stor.mP4 = stor.mP5 = stor.mP6 = false
    stor.mSeg1 = stor.mSeg2 = stor.mSeg3 = stor.mSeg4 = stor.mSeg5 = false
    stor.P1 = { x: 0, y: 0 }
    stor.P2 = { x: 0, y: 0 }
    stor.P3 = { x: 0, y: 0 }
    stor.P4 = { x: 0, y: 0 }
    stor.P5 = { x: 0, y: 0 }
    stor.P6 = { x: 0, y: 0 }
    stor.P1nom = addMaj(stor.nomspris)
    stor.P2nom = addMaj(stor.nomspris)
    stor.P3nom = addMaj(stor.nomspris)
    stor.P4nom = addMaj(stor.nomspris)
    stor.P5nom = addMaj(stor.nomspris)
    stor.mCercle = false
    let cc
    switch (e.koi) {
      case 'point':
        stor.mP1 = true
        cc = 0
        do {
          do {
            cc++
            stor.P1.x = j3pGetRandomInt(stor.limmin.x, stor.limmax.x)
            stor.P1.y = j3pGetRandomInt(stor.limmin.y, stor.limmax.y)
          } while ((cc < 500) && ((e.trans === 'Homothetie') && (stor.yaf) && (((stor.P1.x - stor.A.x) % stor.rapbase !== 0) || ((stor.P1.y - stor.A.y) % stor.rapbase !== 0))))
          cc++
        } while ((cc < 500) && (!dedansok(stor.funcrep(stor.P1)) || (stor.A.x === stor.P1.x && stor.A.y === stor.P1.y)))
        break
      case 'segment':
        stor.mP1 = true
        do {
          cc = 0
          do {
            cc++
            stor.P1.x = j3pGetRandomInt(stor.limmin.x, stor.limmax.x)
            stor.P1.y = j3pGetRandomInt(stor.limmin.y, stor.limmax.y)
          } while ((cc < 500) && ((e.trans === 'Homothetie') && (stor.yaf) && (((stor.P1.x - stor.A.x) % stor.rapbase !== 0) || ((stor.P1.y - stor.A.y) % stor.rapbase !== 0))))
          cc++
        } while ((cc < 500) && (!dedansok(stor.funcrep(stor.P1)) || (stor.A.x === stor.P1.x && stor.A.y === stor.P1.y)))
        stor.mP2 = true
        do {
          cc = 0
          do {
            cc++
            stor.P2.x = j3pGetRandomInt(stor.limmin.x, stor.limmax.x)
            stor.P2.y = j3pGetRandomInt(stor.limmin.y, stor.limmax.y)
          } while ((cc < 500) && ((e.trans === 'Homothetie') && (stor.yaf) && (((stor.P2.x - stor.A.x) % stor.rapbase !== 0) || ((stor.P2.y - stor.A.y) % stor.rapbase !== 0))))
          cc++
        } while ((cc < 500) && (!dedansok(stor.funcrep(stor.P2)) || (stor.P2.x === stor.P1.x && stor.P2.y === stor.P1.y)))
        stor.mSeg1 = true
        break
      case 'triangle':
        stor.mP1 = true
        do {
          cc = 0
          do {
            cc++
            stor.P1.x = j3pGetRandomInt(stor.limmin.x, stor.limmax.x)
            stor.P1.y = j3pGetRandomInt(stor.limmin.y, stor.limmax.y)
          } while ((cc < 500) && ((e.trans === 'Homothetie') && (stor.yaf) && (((stor.P1.x - stor.A.x) % stor.rapbase !== 0) || ((stor.P1.y - stor.A.y) % stor.rapbase !== 0))))
          cc++
        } while ((cc < 500) && (!dedansok(stor.funcrep(stor.P1)) || (stor.A.x === stor.P1.x && stor.A.y === stor.P1.y)))
        stor.mP2 = true
        do {
          cc = 0
          do {
            cc++
            stor.P2.x = j3pGetRandomInt(stor.limmin.x, stor.limmax.x)
            stor.P2.y = j3pGetRandomInt(stor.limmin.y, stor.limmax.y)
          } while ((cc < 500) && ((e.trans === 'Homothetie') && (stor.yaf) && (((stor.P2.x - stor.A.x) % stor.rapbase !== 0) || ((stor.P2.y - stor.A.y) % stor.rapbase !== 0))))
          cc++
        } while ((cc < 500) && (!dedansok(stor.funcrep(stor.P2)) || (stor.P2.x === stor.P1.x && stor.P2.y === stor.P1.y)))
        stor.mSeg1 = true
        stor.mSeg2 = true
        stor.mSeg5 = true
        stor.mP3 = true
        do {
          cc = 0
          do {
            cc++
            stor.P3.x = j3pGetRandomInt(stor.limmin.x, stor.limmax.x)
            stor.P3.y = j3pGetRandomInt(stor.limmin.y, stor.limmax.y)
          } while ((cc < 500) && ((e.trans === 'Homothetie') && (stor.yaf) && (((stor.P3.x - stor.A.x) % stor.rapbase !== 0) || ((stor.P3.y - stor.A.y) % stor.rapbase !== 0))))
          cc++
        } while ((cc < 500) && (!dedansok(stor.funcrep(stor.P3)) || (stor.P3.x === stor.P1.x && stor.P3.y === stor.P1.y) || (stor.P2.x === stor.P3.x && stor.P2.y === stor.P3.y)))
        stor.mSeg1 = true
        break
      case 'cercle':
        stor.mP5 = true
        do {
          cc = 0
          do {
            cc++
            stor.P5.x = j3pGetRandomInt(stor.limmin.x, stor.limmax.x)
            stor.P5.y = j3pGetRandomInt(stor.limmin.y, stor.limmax.y)
          } while ((cc < 500) && ((e.trans === 'Homothetie') && (stor.yaf) && (((stor.P5.x - stor.A.x) % stor.rapbase !== 0) || ((stor.P5.y - stor.A.y) % stor.rapbase !== 0))))
          cc++
        } while ((cc < 500) && !dedansok(stor.funcrep(stor.P5)))
        stor.mP6 = true
        do {
          cc = 0
          do {
            cc++
            stor.P6.x = j3pGetRandomInt(stor.limmin.x, stor.limmax.x)
            stor.P6.y = j3pGetRandomInt(stor.limmin.y, stor.limmax.y)
          } while ((cc < 500) && ((e.trans === 'Homothetie') && (stor.yaf) && (((stor.P6.x - stor.A.x) % stor.rapbase !== 0) || ((stor.P6.y - stor.A.y) % stor.rapbase !== 0))))
          cc++
        } while ((cc < 500) && (!dedansok(stor.funcrep(stor.P6)) || (stor.P6.x === stor.P5.x && stor.P6.y === stor.P5.y)))
        stor.mCercle = true
    }

    return e
  }

  function dedansok (pt) {
    return (
      pt.x >= stor.limmin.x &&
      pt.x <= stor.limmax.x &&
      pt.y >= stor.limmin.y &&
      pt.y <= stor.limmax.y
    )
  }

  function poseQuestion () {
    let tt = 'Place '
    let buf = ' l’image '
    if (stor.Lexo.trans === 'Axiale' || stor.Lexo.trans === 'Centrale') buf = ' le symétrique '
    tt += buf
    switch (stor.Lexo.koi) {
      case 'point':
        tt += stor.P1nom + "' du point " + stor.P1nom + ' '
        break
      case 'segment':
        tt += '[' + stor.P1nom + "'" + stor.P2nom + "'] du segment [" + stor.P1nom + stor.P2nom + '] '
        break
      case 'triangle':
        tt += stor.RP1nom + stor.RP2nom + stor.RP3nom + ' du triangle ' + stor.P1nom + stor.P2nom + stor.P3nom + ' '
        break
      case 'cercle':
        tt += ' du cercle de centre ' + stor.P5nom + ' '
        break
    }
    switch (stor.Lexo.trans) {
      case 'Axiale':
        tt += ' par rapport à la droite ' + stor.nomAB + ' .'
        break
      case 'Centrale':
        tt += ' par rapport au point ' + stor.Anom + ' .'
        break
      case 'Translation':
        tt += ' par la translation qui envoie ' + stor.Anom + ' en ' + stor.Bnom + ' .'
        break
      case 'Rotation':
        if (stor.sens) {
          tt += ' par la rotation de centre ' + stor.Anom + ' et  d’angle  $90$°.<br> (dans le sens des aiguilles d’une montre)'
        } else {
          tt += ' par la rotation de centre ' + stor.Anom + ' et  d’angle  $90$°.<br> (dans le sens inverse des aiguilles d’une montre)'
        }
        break
      case 'Homothetie':
        tt += ' par l’homothétie de centre ' + stor.Anom + ' et de rapport $' + stor.rapAf + '$ .'
    }
    stor.untab = addDefaultTable(stor.lesdiv.consigne, 1, 2)
    j3pAffiche(stor.untab[0][0], null, '<b>' + tt + '</b>')
    stor.laide = new BulleAide(stor.untab[0][1], 'Clique sur un point violet pour le déplacer')
    stor.lesdiv.travail.classList.add('travail')
    stor.lesdiv.consigne.classList.add('enonce')
    me.zonesElts.MG.classList.add('fond')
    stor.lesdiv.solution.classList.remove('correction')
    stor.lesdiv.explications.classList.remove('explique')
  }

  function creeLesDiv () {
    stor.lesdiv = {}
    stor.lesdiv.conteneur = j3pDiv(me.zones.MG, {
      style: me.styles.etendre('toutpetit.enonce', { padding: '10px' })
    })
    stor.lesdiv.consignet = j3pDiv(stor.lesdiv.conteneur, {
      style: me.styles.toutpetit.enonce
    })
    let untab = addDefaultTable(stor.lesdiv.consignet, 1, 3)
    untab[0][1].style.width = '40px'
    stor.lesdiv.consigne = untab[0][0]
    stor.lesdiv.coboot = untab[0][2]
    stor.lesdiv.travail1 = j3pDiv(stor.lesdiv.conteneur, {
      style: me.styles.toutpetit.enonce
    })
    untab = addDefaultTable(stor.lesdiv.travail1, 1, 3)
    untab[0][1].style.width = '40px'
    stor.lesdiv.travail = untab[0][0]
    stor.lesdiv.correction = untab[0][2]
    stor.lesdiv.zonefig = j3pDiv(stor.lesdiv.travail, {
      style: me.styles.toutpetit.enonce
    })
    stor.lesdiv.zonedrep = untab[0][1]
    stor.lesdiv.explications = j3pDiv(stor.lesdiv.conteneur, {
      style: me.styles.toutpetit.enonce
    })
    stor.lesdiv.solution = j3pDiv(stor.lesdiv.conteneur, {
      style: me.styles.toutpetit.enonce
    })
    stor.lesdiv.explications.style.color = me.styles.cfaux
    stor.lesdiv.solution.style.color = me.styles.toutpetit.correction.color
  }

  function makefig () {
    const txtfigure = 'TWF0aEdyYXBoSmF2YTEuMAAAABM9TMzNAAJmcv###wEA#wEAAAAAAAAAAAM7AAACdwAAAQEAAAAAAAAAAQAAAIr#####AAAAAQAKQ0NhbGNDb25zdAD#####AAJwaQAWMy4xNDE1OTI2NTM1ODk3OTMyMzg0Nv####8AAAABAApDQ29uc3RhbnRlQAkh+1RELRj#####AAAAAQAKQ1BvaW50QmFzZQD#####AQAAAAAOAAFPAMAoAAAAAAAAAAAAAAAAAAAABE9PT08FAAFAeOo9cKPXCkB0Lwo9cKPX#####wAAAAEAFENEcm9pdGVEaXJlY3Rpb25GaXhlAP####8BAAAAABAAAAEAAAABAAAAAQE#8zMzMzMzM#####8AAAABAA9DUG9pbnRMaWVEcm9pdGUA#####wEAAAABDgABSQDAGAAAAAAAAAAAAAAAAAAAAAJPSQUAAUA#mBBiTdLyAAAAAv####8AAAABAAlDRHJvaXRlQUIA#####wEAAAAAEAAAAQAAAAEAAAABAAAAA#####8AAAABABZDRHJvaXRlUGVycGVuZGljdWxhaXJlAP####8A2NjYABAAAAEAAAABAAAAAQAAAAT#####AAAAAQAJQ0NlcmNsZU9BAP####8BAAAAAAAAAQAAAAEAAAAD#####wAAAAEAEENJbnREcm9pdGVDZXJjbGUA#####wAAAAUAAAAG#####wAAAAEAEENQb2ludExpZUJpcG9pbnQA#####wEAAAAAEAAAAQAABQABAAAABwAAAAkA#####wEAAAABDgABSgDAKAAAAAAAAMAQAAAAAAAAAAJPSgUAAgAAAAf#####AAAAAgAHQ1JlcGVyZQD#####ANjY2AAAAAEAAAABAAAAAwAAAAkBAQAAAAABAAAAAAAAAAAAAAABAAAAAAAAAAAAAAABP#AAAAAAAAAAAAABP#AAAAAAAAD#####AAAAAQAKQ1VuaXRleFJlcAD#####AAR1bml0AAAACv####8AAAABAAtDSG9tb3RoZXRpZQD#####AAAAAf####8AAAABAApDT3BlcmF0aW9uAwAAAAE#8AAAAAAAAP####8AAAABAA9DUmVzdWx0YXRWYWxldXIAAAAL#####wAAAAEAC0NQb2ludEltYWdlAP####8BAAAAABAAAlciAQAAAQAAAAADAAAADP####8AAAABAAlDTG9uZ3VldXIA#####wAAAAEAAAAN#####wAAAAEAB0NDYWxjdWwA#####wAHbmJncmFkeAACMjAAAAABQDQAAAAAAAAAAAARAP####8AB25iZ3JhZHkAAjIwAAAAAUA0AAAAAAAAAAAAEQD#####AANyYXkAATEAAAABP#AAAAAAAAD#####AAAAAQANQ1BvaW50QmFzZUVudAD#####AH8AfwEQAAAAAAAAAAAAAABACAAAAAAAAAAGcmVwcHQxCQABAAAACkAIAAAAAAAAQBwAAAAAAAABAQAAABIA#####wB#AH8BEAAAAAAAAAAAAAAAQAgAAAAAAAAABnJlcHB0MgkAAQAAAApAFAAAAAAAAEAcAAAAAAAAAQEAAAASAP####8AfwB#ARAAAAAAAAAAAAAAAEAIAAAAAAAAAAZyZXBwdDMJAAEAAAAKQBQAAAAAAABAFAAAAAAAAAEBAAAAEgD#####AH8AfwEQAAAAAAAAAAAAAABACAAAAAAAAAAGcmVwcHQ0CQABAAAACkAIAAAAAAAAQBQAAAAAAAABAQAAABIA#####wB#AH8BEAAAAAAAAAAAAAAAQAgAAAAAAAAABnJlcHB0NQkAAQAAAApAEAAAAAAAAEAIAAAAAAAAAQEAAAASAP####8AfwB#ARAAAAAAAAAAAAAAAEAIAAAAAAAAAAZyZXBwdDYIAAEAAAAKQBAAAAAAAAA#8AAAAAAAAAEB#####wAAAAEACENTZWdtZW50AP####8AfwB#ABAAAAEAB3JlcHNlZzEAAwAAABIAAAATAAAAEwD#####AH8AfwAQAAABAAdyZXBzZWcyAAMAAAATAAAAFAAAABMA#####wB#AH8AEAAAAQAHcmVwc2VnMwADAAAAFAAAABUAAAATAP####8AfwB#ABAAAAEAB3JlcHNlZzQAAwAAABUAAAASAAAAEwD#####AH8AfwAQAAABAAdyZXBzZWc1AAMAAAASAAAAFP####8AAAACAAlDQ2VyY2xlT1IA#####wF#AH8AAAADAAAAEgAAAA4AAAARAAAAABQA#####wF#AH8AAAADAAAAEwAAAA4AAAARAAAAABQA#####wF#AH8AAAADAAAAFAAAAA4AAAARAAAAABQA#####wF#AH8AB2NlcmNjbGUAAwAAABUAAAAOAAAAEQD#####AAAAAQAPQ1BvaW50TGllQ2VyY2xlAP####8A####ABAAAAAAAAAAAAAAAEAIAAAAAAAAAAAHAAFABzXEcnAcKgAAAB0AAAAVAP####8A####ABAAAAAAAAAAAAAAAEAIAAAAAAAAAAAHAAE#1LOH6qKWLwAAAB4AAAAVAP####8A####ABAAAAAAAAAAAAAAAEAIAAAAAAAAAAAHAAFAGMNRsbYa2AAAAB8AAAAVAP####8A####ABAAAAAAAAAAAAAAAEAIAAAAAAAAAAAHAAFACre9WVnszQAAACAAAAAVAP####8A####ABAAAAAAAAAAAAAAAEAIAAAAAAAAAAAHAAFACaQO4SKn5wAAAB7#####AAAAAgAMQ0NvbW1lbnRhaXJlAP####8AfwB#AQAKcmVwcGV0MnR4dAAAACIQAAAAAAABAAAAAQAAAAEAAAAAAAAAAAABZgAAABYA#####wB#AH8BAAlyZXBwdDF0eHQAAAAhEAAAAAAAAQAAAAEAAAABAAAAAAAAAAAAAWYAAAAWAP####8AfwB#AQAJcmVwcHQzdHh0AAAAIxAAAAAAAAEAAAABAAAAAQAAAAAAAAAAAAFkAAAAFgD#####AH8AfwEACXJlcHB0NHR4dAAAACQQAAAAAAABAAAAAQAAAAEAAAAAAAAAAAABZgAAAAcA#####wB#AH8ACXJlcGNlcmNsZQADAAAAFgAAABcAAAACAP####8A#wAAABAAAAAAAAAAAAAAAEAIAAAAAAAAAANBQUEJAAFATQAAAAAAAEBX+FHrhR64AAAAAgD#####AP8AAAAQAAAAAAAAAAAAAABACAAAAAAAAAADQkJCCQABQE+AAAAAAAFAa1wo9cKPXAAAAAUA#####wD#AAAAEAAAAQAGZHRlc3ltAAMAAAArAAAALAAAAAIA#####wAAAAAAEAAAAAAAAAAAAAAAQAgAAAAAAAAAA3B0MQkAAUBmoAAAAAAAQF04UeuFHrgAAAACAP####8AAAAAABAAAAAAAAAAAAAAAEAIAAAAAAAAAANwdDIJAAFAcUAAAAAAAEBZuFHrhR64AAAAAgD#####AAAAAAAQAAAAAAAAAAAAAABACAAAAAAAAAADcHQzCQABQHIAAAAAAABAapwo9cKPXAAAAAIA#####wAAAAAAEAAAAAAAAAAAAAAAQAgAAAAAAAAAA3B0NAkAAUBkAAAAAAAAQGjcKPXCj1wAAAACAP####8AAAAAABAAAAAAAAAAAAAAAEAIAAAAAAAAAANwdDUJAAFAa2AAAAAAAECANwo9cKPXAAAAAgD#####AAAAAAAQAAAAAAAAAAAAAABACAAAAAAAAAADcHQ2CQABQHUQAAAAAABAgKcKPXCj1wAAABQA#####wEAAAAAAAADAAAALgAAAA4AAAARAAAAABQA#####wEAAAAAAAADAAAALwAAAA4AAAARAAAAABQA#####wEAAAAAAAADAAAAMAAAAA4AAAARAAAAABQA#####wEAAAAAAAADAAAAMQAAAA4AAAARAAAAABQA#####wEAAAAAAAADAAAAMgAAAA4AAAARAAAAABUA#####wD###8AEAAAAAAAAAAAAAAAQAgAAAAAAAAAAAcAAT#5IftURC0YAAAANAAAABUA#####wD###8AEAAAAAAAAAAAAAAAQAgAAAAAAAAAAAcAAT#4OC3KaYlDAAAANQAAABUA#####wD###8AEAAAAAAAAAAAAAAAQAgAAAAAAAAAAAcAAT#5IftURC0YAAAANgAAABUA#####wD###8AEAAAAAAAAAAAAAAAQAgAAAAAAAAAAAcAAT#3aOJQrsb8AAAANwAAABUA#####wD###8AEAAAAAAAAAAAAAAAQAgAAAAAAAAAAAcAAT#4rPwpv9SWAAAAOAAAABYA#####wAAAAABAAZwdDF0eHQAAAA5EAAAAAAAAQAAAAEAAAABAAAAAAAAAAAAAWEAAAAWAP####8AAAAAAQAGcHQydHh0AAAAOhAAAAAAAAEAAAABAAAAAQAAAAAAAAAAAAFhAAAAFgD#####AAAAAAEABnB0M3R4dAAAADsQAAAAAAABAAAAAQAAAAEAAAAAAAAAAAABYQAAABYA#####wAAAAABAAZwdDR0eHQAAAA8EAAAAAAAAQAAAAEAAAABAAAAAAAAAAAAAWEAAAAWAP####8AAAAAAQAGcHQ1dHh0AAAAPRAAAAAAAAEAAAABAAAAAQAAAAAAAAAAAAFhAAAAEwD#####AAAAAAAQAAABAARzZWcxAAMAAAAuAAAALwAAABMA#####wAAAAAAEAAAAQAEc2VnMgADAAAALwAAADAAAAATAP####8AAAAAABAAAAEABHNlZzMAAwAAADAAAAAxAAAAEwD#####AAAAAAAQAAABAARzZWc0AAMAAAAxAAAALgAAABMA#####wAAAAAAEAAAAQAEc2VnNQADAAAALgAAADAAAAAHAP####8AAAAAAAZjZXJjbGUAAwAAADIAAAAzAAAAEgD#####AAAAAAAQAAAAAAAAAAAAAABACAAAAAAAAAAABwABAAAACkAYAAAAAAAAwBQAAAAAAAABAQAAABIA#####wAAAAAAEAAAAAAAAAAAAAAAQAgAAAAAAAAAAAcAAQAAAApAGAAAAAAAAMAUAAAAAAAAAQH#####AAAAAQAIQ1ZlY3RldXIA#####wAAAAAAEAAAAQAAAAMAAABJAAAASgD#####AAAAAQANQ1RyYW5zUGFyVmVjdAD#####AAAASwAAAA8A#####wAAAAAAEAAAAAAAAAAAAAAAQAgAAAAAAAAABHB0MTIJAAAAAC4AAABMAAAADwD#####AAAAAAAQAAAAAAAAAAAAAABACAAAAAAAAAAEcHQyMgkAAAAALwAAAEwAAAAPAP####8AAAAAABAAAAAAAAAAAAAAAEAIAAAAAAAAAARwdDMyCQAAAAAwAAAATAAAAA8A#####wAAAAAAEAAAAAAAAAAAAAAAQAgAAAAAAAAABHB0NDIJAAAAADEAAABMAAAADwD#####AAAAAAAQAAAAAAAAAAAAAABACAAAAAAAAAAEcHQ1MgkAAAAAMgAAAEwAAAAPAP####8AAAAAABAAAAAAAAAAAAAAAEAIAAAAAAAAAARwdDYyCQAAAAAzAAAATAAAABQA#####wEAAAAAAAADAAAAKwAAAA4AAAARAAAAABUA#####wD###8AEAAAAAAAAAAAAAAAQAgAAAAAAAAAAAcAAT+bqz+mVINIAAAAUwAAABYA#####wD#AAABAARub21BAAAAVBAAAAAAAAEAAAABAAAAAQAAAAAAAAAAAAFhAAAAFAD#####Af8AAAAAAAMAAAAsAAAADgAAABEAAAAABgD#####Af8AAAAQAAABAAAAAwAAACwAAAAtAAAACAD#####AAAAVwAAAFYAAAAJAP####8B#wAAABAAAAAAAAAAAAAAAEAIAAAAAAAAAAAHAAEAAABYAAAACQD#####Af8AAAAQAAAAAAAAAAAAAABACAAAAAAAAAAABwACAAAAWP####8AAAABABBDRHJvaXRlUGFyYWxsZWxlAP####8B#wAAABAAAAEAAAADAAAAWgAAAC0AAAAEAP####8A####ABAAAAAAAAAAAAAAAEAIAAAAAAAAAAAHAAG#5GYI#LkDwgAAAFsAAAAWAP####8A#wAAAQAFbm9tQUIAAABcEAAAAAAAAQAAAAEAAAABAAAAAAAAAAAAAXMAAAAPAP####8A#wAAABAAAAAAAAAAAAAAAEAIAAAAAAAAAAhkdHN5bXB0MQkAAAAAKwAAAEwAAAAPAP####8A#wAAABAAAAAAAAAAAAAAAEAIAAAAAAAAAAlkdGVzeW1wdDIJAAAAACwAAABMAAAAFQD#####AP###wAQAAAAAAAAAAAAAABACAAAAAAAAAAABwABP7uRkDlWYcQAAABWAAAAFgD#####AP8AAAEABG5vbUIAAABgEAAAAAAAAQAAAAEAAAABAAAAAAAAAAAAAWQAAAACAP####8BAAD#ABAAAAAAAAAAAAAAAEAIAAAAAAAAAARjb3AxCQABQHkwAAAAAABAbFwo9cKPXAAAAAIA#####wEAAP8AEAAAAAAAAAAAAAAAQAgAAAAAAAAABGNvcDIJAAFAgLAAAAAAAEBr#Cj1wo9cAAAAAgD#####AQAA#wAQAAAAAAAAAAAAAABACAAAAAAAAAAEY29wMwkAAUCEiAAAAAAAQGvcKPXCj1wAAAACAP####8BAAD#ABAAAAAAAAAAAAAAAEAIAAAAAAAAAARjb3A0CQABQIgoAAAAAABAaZwo9cKPXAAAAAIA#####wEAAP8AEAAAAAAAAAAAAAAAQAgAAAAAAAAABGNvcDUJAAFAh+AAAAAAAEBxnhR64UeuAAAAAgD#####AQAA#wAQAAAAAAAAAAAAAABACAAAAAAAAAAEY29wNgkAAUCHeAAAAAAAQHeOFHrhR64AAAAUAP####8BAH8AAAAAAQAAAGIAAAAOAAAAEQAAAAAUAP####8BAH8AAAAAAQAAAGMAAAAOAAAAEQAAAAAUAP####8BAH8AAAAAAQAAAGQAAAAOAAAAEQAAAAAUAP####8BAH8AAAAAAQAAAGUAAAAOAAAAEQAAAAAUAP####8BAH8AAAAAAQAAAGYAAAAOAAAAEQAAAAAVAP####8B####ABAAAAAAAAAAAAAAAEAIAAAAAAAAAARjb2IxBwABP#maaulX34gAAABoAAAAFQD#####Af###wAQAAAAAAAAAAAAAABACAAAAAAAAAAEY29iMgcAAT#4OC3KaYlDAAAAaQAAABUA#####wH###8AEAAAAAAAAAAAAAAAQAgAAAAAAAAABGNvYjMHAAE#+gvI3h7Q7QAAAGoAAAAVAP####8B####ABAAAAAAAAAAAAAAAEAIAAAAAAAAAARjb2I0BwABP#e5gOjScKsAAABrAAAAFQD#####Af###wAQAAAAAAAAAAAAAABACAAAAAAAAAAEY29iNQcAAT#88Chf6BLLAAAAbAAAABYA#####wEAAP8BAAVjb2FmMQAAAG0QAAAAAAABAAAAAQAAAAEAAAAAAAAAAAABYQAAABYA#####wEAAP8BAAVjb2FmMgAAAG4QAAAAAAABAAAAAQAAAAEAAAAAAAAAAAABZQAAABYA#####wEAAP8BAAVjb2FmMwAAAG8QAAAAAAABAAAAAQAAAAEAAAAAAAAAAAABZAAAABYA#####wEAAP8BAAVjb2FmNAAAAHAQAAAAAAABAAAAAQAAAAEAAAAAAAAAAAABZAAAABYA#####wEAAP8BAAVjb2FmNQAAAHEQAAAAAAABAAAAAQAAAAEAAAAAAAAAAAABegAAABMA#####wEAAP8AEAAAAQAGY29zZWcxAAQAAABiAAAAYwAAABMA#####wEAAP8AEAAAAQAGY29zZWcyAAQAAABjAAAAZAAAABMA#####wEAAP8AEAAAAQAGY29zZWczAAQAAABkAAAAZQAAABMA#####wEAAP8AEAAAAQAGY29zZWc0AAQAAABlAAAAYgAAABMA#####wEAAP8AEAAAAQAGY29zZWc1AAQAAABkAAAAYgAAAAcA#####wEAAP8ACGNvY2VyY2xlAAQAAABmAAAAZwAAAAIA#####wEAAP8AEAAAAAAAAAAAAAAAQAgAAAAAAAAAA2FwMQcAAUB+UAAAAAAAQIK#Cj1wo9cAAAACAP####8BAAD#ABAAAAAAAAAAAAAAAEAIAAAAAAAAAANhcDIHAAFAgbAAAAAAAECCvwo9cKPXAAAAAgD#####AQAA#wAQAAAAAAAAAAAAAABACAAAAAAAAAADYXAzBwABQIJ4AAAAAABAg7cKPXCj1wAAAAIA#####wEAAP8AEAAAAAAAAAAAAAAAQAgAAAAAAAAAA2FwNAcAAUCDAAAAAAAAQIK3Cj1wo9cAAAACAP####8BAAD#ABAAAAAAAAAAAAAAAEAIAAAAAAAAAANhcDUHAAFAhBgAAAAAAECDtwo9cKPXAAAAAgD#####AQAA#wAQAAAAAAAAAAAAAABACAAAAAAAAAADYXA2BwABQIWoAAAAAABAgpcKPXCj1wAAABMA#####wCBgfcAEAAAAQAAAgMAAAB9AAAAfgAAABMA#####wCBgfcAEAAAAQAAAgMAAAB+AAAAfwAAABMA#####wCBgfcAEAAAAQAAAgMAAACAAAAAgQAAABMA#####wCBgfcAEAAAAQAAAgMAAACBAAAAggAAABQA#####wF#AH8AAAABAAAAFgAAAA4AAAARAAAAABUA#####wD###8AEAAAAAAAAAAAAAAAQAgAAAAAAAAAAAcAAT#5RcaucTuqAAAAhwAAABYA#####wB#AH8BAAlyZXBwdDV0eHQAAACIEAAAAAAAAQAAAAEAAAABAAAAAAAAAAAAAWEAAAAO##########8='
    svgId = j3pGetNewId()
    stor.svgId = svgId
    const uu = j3pCreeSVG(stor.lesdiv.zonefig, { id: svgId, width: 620, height: 300 })
    uu.style.border = 'solid black 1px'
    stor.mtgAppLecteur.removeAllDoc()
    stor.mtgAppLecteur.addDoc(svgId, txtfigure, true)
    stor.mtgAppLecteur.calculateAndDisplayAll(true)
    stor.OO = stor.mtgAppLecteur.getPointPosition(svgId, '#OOOO')
    stor.OI = stor.mtgAppLecteur.getPointPosition(svgId, '#OI')
    stor.OJ = stor.mtgAppLecteur.getPointPosition(svgId, '#OJ')
    stor.A = transco(stor.A)
    stor.B = transco(stor.B)
    stor.P1 = transco(stor.P1)
    stor.P2 = transco(stor.P2)
    stor.P3 = transco(stor.P3)
    stor.P4 = transco(stor.P4)
    stor.P5 = transco(stor.P5)
    stor.P6 = transco(stor.P6)
    stor.mtgAppLecteur.giveFormula2(svgId, 'ray', 0.5)
    stor.mtgAppLecteur.setPointPosition(svgId, '#AAA', stor.A.x, stor.A.y, false)
    stor.mtgAppLecteur.setPointPosition(svgId, '#BBB', stor.B.x, stor.B.y, false)
    stor.mtgAppLecteur.setPointPosition(svgId, '#pt1', stor.P1.x, stor.P1.y, false)
    stor.mtgAppLecteur.setPointPosition(svgId, '#pt2', stor.P2.x, stor.P2.y, false)
    stor.mtgAppLecteur.setPointPosition(svgId, '#pt3', stor.P3.x, stor.P3.y, false)
    stor.mtgAppLecteur.setPointPosition(svgId, '#pt4', stor.P4.x, stor.P4.y, false)
    stor.mtgAppLecteur.setPointPosition(svgId, '#pt5', stor.P5.x, stor.P5.y, false)
    stor.mtgAppLecteur.setPointPosition(svgId, '#pt6', stor.P6.x, stor.P6.y, false)
    stor.mtgAppLecteur.setVisible(svgId, '#AAA', false, false)
    stor.mtgAppLecteur.setVisible(svgId, '#BBB', false, false)
    stor.mtgAppLecteur.setVisible(svgId, '#dtsympt1', stor.mA, false)
    stor.mtgAppLecteur.setVisible(svgId, '#nomA', stor.mA, false)
    stor.mtgAppLecteur.setVisible(svgId, '#dtesympt2', stor.mB, false)
    stor.mtgAppLecteur.setVisible(svgId, '#nomB', stor.mB, false)
    stor.mtgAppLecteur.setVisible(svgId, '#dtesym', stor.mDtAB, false)
    stor.mtgAppLecteur.setVisible(svgId, '#nomAB', stor.mDtAB, false)
    if (stor.mA) {
      stor.mtgAppLecteur.setText(svgId, '#nomA', stor.Anom, false)
    }
    if (stor.mB) {
      stor.mtgAppLecteur.setText(svgId, '#nomB', stor.Bnom, false)
    }
    if (stor.mDtAB) {
      stor.mtgAppLecteur.setText(svgId, '#nomAB', stor.nomAB, false)
    }
    stor.mtgAppLecteur.setVisible(svgId, '#reppt1', stor.mP1, false)
    stor.mtgAppLecteur.setVisible(svgId, '#reppt2', stor.mP2, false)
    stor.mtgAppLecteur.setVisible(svgId, '#reppt3', stor.mP3, false)
    stor.mtgAppLecteur.setVisible(svgId, '#reppt4', stor.mP4, false)
    stor.mtgAppLecteur.setVisible(svgId, '#reppt5', stor.mP5, false)
    stor.mtgAppLecteur.setVisible(svgId, '#reppt6', stor.mP6, false)
    stor.mtgAppLecteur.setVisible(svgId, '#reppt1txt', stor.mP1, false)
    stor.mtgAppLecteur.setVisible(svgId, '#reppet2txt', stor.mP2, false)
    stor.mtgAppLecteur.setVisible(svgId, '#reppt3txt', stor.mP3, false)
    stor.mtgAppLecteur.setVisible(svgId, '#reppt4txt', stor.mP4, false)
    stor.mtgAppLecteur.setVisible(svgId, '#reppt5txt', stor.mP5, false)
    stor.mP1s = stor.mP1
    stor.mP2s = stor.mP2
    stor.mP3s = stor.mP3
    stor.mP4s = stor.mP4
    stor.mP5s = stor.mP5
    if (stor.Lexo.trans !== 'Axiale') {
      if (stor.P1.x === stor.A.x && stor.P1.y === stor.A.y) {
        stor.mP1 = false
        stor.P1nom = stor.Anom
      }
      if (stor.P2.x === stor.A.x && stor.P2.y === stor.A.y) {
        stor.mP2 = false
        stor.P2nom = stor.Anom
      }
      if (stor.P3.x === stor.A.x && stor.P3.y === stor.A.y) {
        stor.mP3 = false
        stor.P3nom = stor.Anom
      }
      if (stor.P5.x === stor.A.x && stor.P5.y === stor.A.y) {
        stor.mP5 = false
        stor.P5nom = stor.Anom
      }
    }
    if (stor.Lexo.trans === 'Translation') {
      if (stor.P1.x === stor.B.x && stor.P1.y === stor.B.y) {
        stor.mP1 = false
        stor.P1nom = stor.Bnom
      }
      if (stor.P2.x === stor.B.x && stor.P2.y === stor.B.y) {
        stor.mP2 = false
        stor.P2nom = stor.Bnom
      }
      if (stor.P3.x === stor.B.x && stor.P3.y === stor.B.y) {
        stor.mP3 = false
        stor.P3nom = stor.Bnom
      }
      if (stor.P5.x === stor.B.x && stor.P5.y === stor.B.y) {
        stor.mP5 = false
        stor.P5nom = stor.Bnom
      }
    }
    stor.RP1nom = stor.P1nom + "'"
    stor.RP2nom = stor.P2nom + "'"
    stor.RP3nom = stor.P3nom + "'"
    stor.RP4nom = stor.P4nom + "'"
    stor.RP5nom = stor.P5nom + "'"
    stor.mtgAppLecteur.setVisible(svgId, '#pt1', false, false)
    stor.mtgAppLecteur.setVisible(svgId, '#pt2', false, false)
    stor.mtgAppLecteur.setVisible(svgId, '#pt3', false, false)
    stor.mtgAppLecteur.setVisible(svgId, '#pt4', false, false)
    stor.mtgAppLecteur.setVisible(svgId, '#pt5', false, false)
    stor.mtgAppLecteur.setVisible(svgId, '#pt6', false, false)
    stor.mtgAppLecteur.setVisible(svgId, '#pt12', stor.mP1, false)
    stor.mtgAppLecteur.setVisible(svgId, '#pt22', stor.mP2, false)
    stor.mtgAppLecteur.setVisible(svgId, '#pt32', stor.mP3, false)
    stor.mtgAppLecteur.setVisible(svgId, '#pt42', stor.mP4, false)
    stor.mtgAppLecteur.setVisible(svgId, '#pt52', stor.mP5, false)
    stor.mtgAppLecteur.setVisible(svgId, '#pt62', stor.mP6, false)
    stor.mtgAppLecteur.setVisible(svgId, '#pt1txt', stor.mP1, false)
    stor.mtgAppLecteur.setVisible(svgId, '#pt2txt', stor.mP2, false)
    stor.mtgAppLecteur.setVisible(svgId, '#pt3txt', stor.mP3, false)
    stor.mtgAppLecteur.setVisible(svgId, '#pt4txt', stor.mP4, false)
    stor.mtgAppLecteur.setVisible(svgId, '#pt5txt', stor.mP5, false)
    stor.mtgAppLecteur.setVisible(svgId, '#seg1', stor.mSeg1, false)
    stor.mtgAppLecteur.setVisible(svgId, '#seg2', stor.mSeg2, false)
    stor.mtgAppLecteur.setVisible(svgId, '#seg3', stor.mSeg3, false)
    stor.mtgAppLecteur.setVisible(svgId, '#seg4', stor.mSeg4, false)
    stor.mtgAppLecteur.setVisible(svgId, '#seg5', stor.mSeg5, false)
    stor.mtgAppLecteur.setVisible(svgId, '#cercle', stor.mCercle, false)
    stor.mtgAppLecteur.setVisible(svgId, '#repseg1', stor.mSeg1, false)
    stor.mtgAppLecteur.setVisible(svgId, '#repseg2', stor.mSeg2, false)
    stor.mtgAppLecteur.setVisible(svgId, '#repseg3', stor.mSeg3, false)
    stor.mtgAppLecteur.setVisible(svgId, '#repseg4', stor.mSeg4, false)
    stor.mtgAppLecteur.setVisible(svgId, '#repseg5', stor.mSeg5, false)
    stor.mtgAppLecteur.setVisible(svgId, '#repcercle', stor.mCercle, false)
    if (stor.mP1s) {
      stor.mtgAppLecteur.setText(svgId, '#pt1txt', stor.P1nom, false)
      stor.mtgAppLecteur.setText(svgId, '#reppt1txt', stor.RP1nom, false)
    }
    if (stor.mP2s) {
      stor.mtgAppLecteur.setText(svgId, '#pt2txt', stor.P2nom, false)
      stor.mtgAppLecteur.setText(svgId, '#reppet2txt', stor.RP2nom, false)
    }
    if (stor.mP3s) {
      stor.mtgAppLecteur.setText(svgId, '#pt3txt', stor.P3nom, false)
      stor.mtgAppLecteur.setText(svgId, '#reppt3txt', stor.RP3nom, false)
    }
    if (stor.mP4s) {
      stor.mtgAppLecteur.setText(svgId, '#pt4txt', stor.P4nom, false)
      stor.mtgAppLecteur.setText(svgId, '#reppt4txt', stor.RP4nom, false)
    }
    if (stor.mP5s) {
      stor.mtgAppLecteur.setText(svgId, '#pt5txt', stor.P5nom, false)
      stor.mtgAppLecteur.setText(svgId, '#reppt5txt', stor.RP5nom, false)
    }
    stor.mtgAppLecteur.updateFigure(svgId)
  }

  function transco (pt) {
    return { x: stor.OO.x + pt.x * (stor.OI.x - stor.OO.x), y: stor.OO.y + pt.y * (stor.OJ.y - stor.OO.y) }
  }

  function dist (p1, p2) {
    return Math.sqrt((p1.x - p2.x) * (p1.x - p2.x) + (p1.y - p2.y) * (p1.y - p2.y))
  }

  function mettouvert () {
    switch (stor.Lexo.koi) {
      case 'point':
        stor.mtgAppLecteur.setColor(svgId, '#reppt1', 0, 150, 100, true)
        stor.mtgAppLecteur.setText(svgId, '#reppt1txt', stor.P1nom + "'", true)
        stor.mtgAppLecteur.setColor(svgId, '#reppt1txt', 0, 150, 100, true)
        break
      case 'segment':
        stor.mtgAppLecteur.setColor(svgId, '#reppt1', 0, 150, 100, true)
        stor.mtgAppLecteur.setText(svgId, '#reppt1txt', stor.RP1nom, true)
        stor.mtgAppLecteur.setColor(svgId, '#reppt1txt', 0, 150, 100, true)
        stor.mtgAppLecteur.setColor(svgId, '#reppt2', 0, 150, 100, true)
        stor.mtgAppLecteur.setColor(svgId, '#repseg1', 0, 150, 100, true)
        stor.mtgAppLecteur.setText(svgId, '#reppet2txt', stor.RP2nom, true)
        stor.mtgAppLecteur.setColor(svgId, '#reppet2txt', 0, 150, 100, true)
        break
      case 'triangle':
        stor.mtgAppLecteur.setColor(svgId, '#reppt1', 0, 150, 100, true)
        stor.mtgAppLecteur.setText(svgId, '#reppt1txt', stor.RP1nom, true)
        stor.mtgAppLecteur.setColor(svgId, '#reppt1txt', 0, 150, 100, true)
        stor.mtgAppLecteur.setColor(svgId, '#reppt2', 0, 150, 100, true)
        stor.mtgAppLecteur.setText(svgId, '#reppet2txt', stor.RP2nom, true)
        stor.mtgAppLecteur.setColor(svgId, '#reppet2txt', 0, 150, 100, true)
        stor.mtgAppLecteur.setColor(svgId, '#reppt3', 0, 150, 100, true)
        stor.mtgAppLecteur.setColor(svgId, '#repseg1', 0, 150, 100, true)
        stor.mtgAppLecteur.setColor(svgId, '#repseg2', 0, 150, 100, true)
        stor.mtgAppLecteur.setColor(svgId, '#repseg5', 0, 150, 100, true)
        stor.mtgAppLecteur.setText(svgId, '#reppt3txt', stor.RP3nom, true)
        stor.mtgAppLecteur.setColor(svgId, '#reppt3txt', 0, 150, 100, true)
        break
      case 'cercle':
        stor.mtgAppLecteur.setColor(svgId, '#reppt5', 0, 150, 100, true)
        stor.mtgAppLecteur.setText(svgId, '#reppt5txt', stor.RP5nom, true)
        stor.mtgAppLecteur.setColor(svgId, '#reppt5txt', 0, 150, 100, true)
        stor.mtgAppLecteur.setColor(svgId, '#reppt6', 0, 150, 100, true)
        stor.mtgAppLecteur.setColor(svgId, '#repcercle', 0, 150, 100, true)
        break
    }
  }

  function yaReponse () {
    return true
  }

  function isRepOk () {
    let pt1, pt1rep, pt2, pt2rep, pt3, pt3rep
    // test existence tout elem
    switch (stor.Lexo.koi) {
      case 'point':
        pt1 = stor.mtgAppLecteur.getPointPosition(svgId, '#reppt1')
        pt2 = stor.funcrep(stor.P1)
        return dist(pt1, pt2) < 1
      case 'segment':
        pt1 = stor.mtgAppLecteur.getPointPosition(svgId, '#reppt1')
        pt1rep = stor.funcrep(stor.P1)
        pt2 = stor.mtgAppLecteur.getPointPosition(svgId, '#reppt2')
        pt2rep = stor.funcrep(stor.P2)
        return (dist(pt1, pt1rep) < 1) && (dist(pt2, pt2rep) < 1)
      case 'triangle':
        pt1 = stor.mtgAppLecteur.getPointPosition(svgId, '#reppt1')
        pt1rep = stor.funcrep(stor.P1)
        pt2 = stor.mtgAppLecteur.getPointPosition(svgId, '#reppt2')
        pt2rep = stor.funcrep(stor.P2)
        pt3 = stor.mtgAppLecteur.getPointPosition(svgId, '#reppt3')
        pt3rep = stor.funcrep(stor.P3)
        return (dist(pt1, pt1rep) < 1) && (dist(pt2, pt2rep) < 1) && (dist(pt3, pt3rep) < 1)
      case 'cercle':
        pt1 = stor.mtgAppLecteur.getPointPosition(svgId, '#reppt5')
        pt1rep = stor.funcrep(stor.P5)
        pt2 = stor.mtgAppLecteur.getPointPosition(svgId, '#reppt6')
        pt2rep = stor.funcrep(stor.P6)
        return (dist(pt1, pt1rep) < 1) && (Math.abs(dist(pt1, pt2) - dist(pt1rep, pt2rep)) < 5)
    }

    if (ds.theme === 'zonesAvecImageDeFond') {
      stor.lesdiv.solution.classList.remove('correction')
      stor.lesdiv.explications.classList.remove('explique')
    }

    return true
  }

  function AffCorrection (bool) {
    let buf, elem
    if (bool) {
      stor.laide.disable()
      j3pEmpty(stor.untab[0][1])
      stor.lesdiv.correction.innerHTML += '<br> Regarde la correction. <br> <i>(en bleu)</i>'
      switch (stor.Lexo.koi) {
        case 'point':
          stor.mtgAppLecteur.setColor(svgId, '#reppt1', 243, 143, 12, true)
          stor.mtgAppLecteur.setText(svgId, '#reppt1txt', stor.P1nom + "'", true)
          stor.mtgAppLecteur.setColor(svgId, '#reppt1txt', 243, 143, 12, true)
          // met un point vert à la bonne place
          // met son nom
          // met le trajet
          stor.mtgAppLecteur.setVisible(svgId, '#cop1', true, true)
          stor.mtgAppLecteur.setVisible(svgId, '#coaf1', true, true)
          stor.mtgAppLecteur.setText(svgId, '#coaf1', stor.RP1nom, true)
          buf = stor.funcrep(stor.P1)
          stor.mtgAppLecteur.setPointPosition(svgId, '#cop1', buf.x, buf.y, true)
          break
        case 'segment':
          stor.mtgAppLecteur.setColor(svgId, '#reppt1', 243, 143, 12, true)
          stor.mtgAppLecteur.setText(svgId, '#reppt1txt', stor.RP1nom, true)
          stor.mtgAppLecteur.setColor(svgId, '#reppt1txt', 243, 143, 12, true)
          // met un point vert à la bonne place
          // met son nom
          // met le trajet
          stor.mtgAppLecteur.setVisible(svgId, '#cop1', true, true)
          stor.mtgAppLecteur.setVisible(svgId, '#coaf1', true, true)
          stor.mtgAppLecteur.setText(svgId, '#coaf1', stor.RP1nom, true)
          buf = stor.funcrep(stor.P1)
          stor.mtgAppLecteur.setPointPosition(svgId, '#cop1', buf.x, buf.y, true)

          stor.mtgAppLecteur.setColor(svgId, '#reppt2', 243, 143, 12, true)
          stor.mtgAppLecteur.setColor(svgId, '#repseg1', 243, 143, 12, true)
          stor.mtgAppLecteur.setText(svgId, '#reppet2txt', stor.RP2nom, true)
          stor.mtgAppLecteur.setColor(svgId, '#reppet2txt', 243, 143, 12, true)
          // met un point vert à la bonne place
          // met son nom
          // met le trajet
          stor.mtgAppLecteur.setVisible(svgId, '#cop2', true, true)
          stor.mtgAppLecteur.setVisible(svgId, '#coaf2', true, true)
          stor.mtgAppLecteur.setVisible(svgId, '#coseg1', true, true)
          stor.mtgAppLecteur.setText(svgId, '#coaf2', stor.RP2nom, true)
          elem = stor.funcrep(stor.P2)
          stor.mtgAppLecteur.setPointPosition(svgId, '#cop2', elem.x, elem.y, true)
          break
        case 'triangle':
          stor.mtgAppLecteur.setColor(svgId, '#reppt1', 243, 143, 12, true)
          stor.mtgAppLecteur.setText(svgId, '#reppt1txt', stor.RP1nom, true)
          stor.mtgAppLecteur.setColor(svgId, '#reppt1txt', 243, 143, 12, true)
          // met un point vert à la bonne place
          // met son nom
          // met le trajet
          stor.mtgAppLecteur.setVisible(svgId, '#cop1', true, true)
          stor.mtgAppLecteur.setVisible(svgId, '#coaf1', true, true)
          stor.mtgAppLecteur.setText(svgId, '#coaf1', stor.RP1nom, true)
          buf = stor.funcrep(stor.P1)
          stor.mtgAppLecteur.setPointPosition(svgId, '#cop1', buf.x, buf.y, true)

          stor.mtgAppLecteur.setColor(svgId, '#reppt2', 243, 143, 12, true)
          stor.mtgAppLecteur.setText(svgId, '#reppet2txt', stor.RP2nom, true)
          stor.mtgAppLecteur.setColor(svgId, '#reppet2txt', 243, 143, 12, true)
          // met un point vert à la bonne place
          // met son nom
          // met le trajet
          stor.mtgAppLecteur.setVisible(svgId, '#cop2', true, true)
          stor.mtgAppLecteur.setVisible(svgId, '#coaf2', true, true)
          stor.mtgAppLecteur.setText(svgId, '#coaf2', stor.RP2nom, true)
          elem = stor.funcrep(stor.P2)
          stor.mtgAppLecteur.setPointPosition(svgId, '#cop2', elem.x, elem.y, true)

          stor.mtgAppLecteur.setColor(svgId, '#reppt3', 243, 143, 12, true)
          stor.mtgAppLecteur.setColor(svgId, '#repseg1', 243, 143, 12, true)
          stor.mtgAppLecteur.setColor(svgId, '#repseg2', 243, 143, 12, true)
          stor.mtgAppLecteur.setColor(svgId, '#repseg5', 243, 143, 12, true)
          stor.mtgAppLecteur.setText(svgId, '#reppt3txt', stor.RP3nom, true)
          stor.mtgAppLecteur.setColor(svgId, '#reppt3txt', 243, 143, 12, true)
          // met un point vert à la bonne place
          // met son nom
          // met le trajet
          stor.mtgAppLecteur.setVisible(svgId, '#cop3', true, true)
          stor.mtgAppLecteur.setVisible(svgId, '#coaf3', true, true)
          stor.mtgAppLecteur.setVisible(svgId, '#coseg1', true, true)
          stor.mtgAppLecteur.setVisible(svgId, '#coseg2', true, true)
          stor.mtgAppLecteur.setVisible(svgId, '#coseg5', true, true)
          stor.mtgAppLecteur.setText(svgId, '#coaf3', stor.RP3nom, true)
          elem = stor.funcrep(stor.P3)
          stor.mtgAppLecteur.setPointPosition(svgId, '#cop3', elem.x, elem.y, true)
          break
        case 'cercle':
          stor.mtgAppLecteur.setColor(svgId, '#reppt5', 243, 143, 12, true)
          stor.mtgAppLecteur.setText(svgId, '#reppt5txt', stor.RP5nom, true)
          stor.mtgAppLecteur.setColor(svgId, '#reppt5txt', 243, 143, 12, true)
          // met un point vert à la bonne place
          // met son nom
          // met le trajet
          stor.mtgAppLecteur.setVisible(svgId, '#cop5', true, true)
          stor.mtgAppLecteur.setVisible(svgId, '#coaf5', true, true)
          stor.mtgAppLecteur.setText(svgId, '#coaf5', stor.RP5nom, true)
          buf = stor.funcrep(stor.P5)
          stor.mtgAppLecteur.setPointPosition(svgId, '#cop5', buf.x, buf.y, true)

          stor.mtgAppLecteur.setColor(svgId, '#reppt6', 243, 143, 12, true)
          stor.mtgAppLecteur.setColor(svgId, '#repcercle', 243, 143, 12, true)
          // met un point vert à la bonne place
          // met son nom
          // met le trajet
          stor.mtgAppLecteur.setVisible(svgId, '#cop6', true, true)
          stor.mtgAppLecteur.setVisible(svgId, '#cocercle', true, true)
          elem = stor.funcrep(stor.P6)
          stor.mtgAppLecteur.setPointPosition(svgId, '#cop6', elem.x, elem.y, true)
          break
      }
      // stor.mtgAppLecteur.updateFigure(svgId)
      stor.mtgAppLecteur.setActive(svgId, false)
    } else {
      me.afficheBoutonValider()
    }
  }

  function enonceMain () {
    // A chaque répétition, on nettoie la scène
    me.videLesZones()
    creeLesDiv()
    stor.Lexo = faisChoixExos()
    stor.lesdiv.travail.classList.add('travail')
    stor.lesdiv.consigne.classList.add('enonce')
    stor.lesdiv.solution.classList.remove('correction')
    stor.lesdiv.explications.classList.remove('explique')

    makefig()
    poseQuestion()
    me.afficheBoutonValider()
    me.cacheBoutonSuite()
    me.finEnonce()
  }

  switch (this.etat) {
    case 'enonce':
      // code exécuté au lancement de la section
      if (me.debutDeLaSection) {
        initSection()
      } else {
        enonceMain()
      }
      break // case "enonce":

    case 'correction':
      // On teste si une réponse a été saisie
      stor.yaexplik = false
      j3pEmpty(stor.lesdiv.explications)
      j3pEmpty(stor.lesdiv.correction)
      stor.yaco = false

      if (yaReponse()) {
        // Bonne réponse
        if (isRepOk()) {
          this._stopTimer()
          stor.lesdiv.correction.style.color = this.styles.cbien
          stor.lesdiv.correction.innerHTML = cBien

          mettouvert()
          stor.mtgAppLecteur.setActive(svgId, false)
          stor.laide.disable()
          j3pEmpty(stor.untab[0][1])
          this.cacheBoutonValider()
          this.score++

          this.typederreurs[0]++
          this.etat = 'navigation'
          this.sectionCourante()
        } else {
          // Pas de bonne réponse
          stor.lesdiv.correction.style.color = this.styles.cfaux
          stor.lesdiv.correction.innerHTML = cFaux
          if (this.isElapsed) {
            // A cause de la limite de temps :
            stor.lesdiv.correction.innerHTML += tempsDepasse + '<BR>'
            this.typederreurs[10]++
            this.cacheBoutonValider()

            AffCorrection(true)

            this.etat = 'navigation'
            this.sectionCourante()
          } else {
            // Réponse fausse :
            if (this.essaiCourant < ds.nbchances) {
              stor.lesdiv.correction.innerHTML += '<br>' + essaieEncore

              AffCorrection(false)

              this.typederreurs[1]++
            } else {
              // Erreur au nème essai
              this._stopTimer()
              this.cacheBoutonValider()

              AffCorrection(true)

              this.typederreurs[2]++
              this.etat = 'navigation'
              this.sectionCourante()
            }
          }
        }
      } else {
        // pas de réponse
        stor.lesdiv.correction.style.color = this.styles.cfaux
        stor.lesdiv.correction.innerHTML = reponseIncomplete
        me.afficheBoutonValider()
      }

      if (ds.theme === 'zonesAvecImageDeFond') {
        if (stor.yaexplik) {
          stor.lesdiv.explications.classList.add('explique')
        }
        if (stor.yaco) {
          stor.lesdiv.solution.classList.add('correction')
        }
      }

      // Obligatoire
      this.finCorrection()
      break // case "correction":

    case 'navigation':
      if (!this.sectionTerminee()) {
        this.etat = 'enonce'
      }
      this.finNavigation()
      break // case "navigation":
  }
}
