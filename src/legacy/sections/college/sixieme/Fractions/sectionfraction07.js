import { j3pAddElt, j3pArrondi, j3pGetRandomInt, j3pShuffle } from 'src/legacy/core/functions'
import { verifNombreBienEcrit } from 'src/legacy/outils/fonctions/ecritureNombre'
import { j3pAffiche } from 'src/lib/mathquill/functions'
import { ecrisBienMathquill, afficheFrac } from 'src/legacy/outils/zoneStyleMathquill/functions'
import ZoneStyleMathquill1 from 'src/legacy/outils/zoneStyleMathquill/ZoneStyleMathquill1'
import { addDefaultTable } from 'src/legacy/themes/table'
import textesGeneriques from 'src/lib/core/textes'

const { cBien, cFaux, essaieEncore, reponseIncomplete, tempsDepasse } = textesGeneriques

/**
 * Ajouter une description ici
 * @fileOverview
 * @author Tommy Barroy <t.barroy@laposte.net>
 * @since 2017-10
 */

export const params = {
  outils: ['mathquill'],
  /**
   * Ce qui est paramétrable dans l’éditeur de graphe
   * @type {LegacySectionParamsParametres}
   */
  parametres: [
    ['nbrepetitions', 9, 'entier', 'Nombre de répétitions de la section'],
    ['indication', '', 'string', 'Indication pour chaque répétition'],
    ['limite', 0, 'entier', 'Temps disponible par répétition, en secondes'],
    ['nbchances', 2, 'entier', 'Nombre d’essais par répétition'],
    ['question', 'les deux', 'liste', '<u>Fraction</u>: L’élève doit donner l’écriture fractionnaire. <br><br> <u>Décimal</u>: L’élève doit donner l’écriture décimale.', ['Fraction', 'Décimal', 'les deux']],
    ['Sup1', 'parfois', 'liste', 'Le nombre peut être supérieure à 1.', ['toujours', 'parfois', 'jamais']],
    ['limite2', 'millième', 'liste', 'Rang maximum', ['dixième', 'centième', 'millième', 'dix_millième', 'cent_millième', 'millionième']],
    ['Simple', true, 'boolean', '<i> Uniquement pour la question <u>Fraction</u></i><br><br><u>true</u>: Le dénominateur doit être le plus petit possible.'],
    ['Espace', false, 'boolean', '<u>true</u>: Les espaces séparant les groupes de 3 sont exigés.'],
    // le modèle gère la compatibilité ascendante avec l’ancien paramètre MiseEnPagePlusClaire
    ['theme', 'standard', 'liste', 'Modèle pour l’affichage', ['standard', 'zonesAvecImageDeFond']]
  ]
}

/**
 * section fraction07
 * @this {Parcours}
 */
export default function main () {
  const maxDecimales = 6
  const me = this
  const stor = me.storage
  let ds = me.donneesSection

  function getDonnees () {
    return {
      // Nombre de répétitions de l’exercice
      nbrepetitions: 3,

      question: 'les deux',
      Sup1: 'parfois',
      limite2: 'millième',
      Simple: true,
      Espace: false,

      // Si indication != "" alors un lien "Indication" s’affiche dans la zone Inférieure Gauche (me.zones.IG
      // une indication<br>de deux lignes..."
      indication: '',

      // pour le cas de presentation1bis
      // En effet pas d’indication possible pour cette présentation
      // surchargeindication: false,

      // Nombre de chances dont dispose l’él§ve pour répondre à la question
      nbchances: 2,

      //  || "presentation2" || "presentation3"  || "presentation1bis"
      structure: 'presentation1bis'
    }
  }
  function initSection () {
    ds = getDonnees()
    me.donneesSection = ds
    // On surcharge avec les parametres passés par le graphe
    me.surcharge()
    me.validOnEnter = false // ex donneesSection.touche_entree

    // Construction de la page
    me.construitStructurePage(me.donneesSection.structure)

    me.donneesSection.nbetapes = 1
    let lp = []
    if ((ds.question === 'les deux') || (ds.question === 'Fraction')) {
      lp.push('F')
    }
    if ((ds.question === 'les deux') || (ds.question === 'Décimal')) {
      lp.push('D')
    }

    me.donneesSection.nbitems = me.donneesSection.nbetapes * me.donneesSection.nbrepetitions

    ds.lesExos = []
    lp = j3pShuffle(lp)
    for (let i = 0; i < ds.nbitems; i++) {
      ds.lesExos[i] = { quest: lp[i % lp.length] }
    }
    ds.lesExos = j3pShuffle(ds.lesExos)

    if (ds.Sup1 === 'parfois') lp = ['+', '-']
    if (ds.Sup1 === 'toujours') lp = ['+']
    if (ds.Sup1 === 'jamais') lp = ['-']
    lp = j3pShuffle(lp)
    for (let i = 0; i < ds.nbitems; i++) {
      ds.lesExos[i].type = lp[i % lp.length]
    }
    ds.lesExos = j3pShuffle(ds.lesExos)

    const lp2 = ['dixième', 'centième', 'millième', 'dix_millième', 'cent_millième', 'millionième']
    const nb = lp2.indexOf(ds.limite2)
    lp = []
    for (let i = 0; i < nb + 1; i++) {
      lp.push(lp2[i])
    }
    lp = j3pShuffle(lp)
    for (let i = 0; i < ds.nbitems; i++) {
      ds.lesExos[i].limite2 = lp[i % lp.length]
    }
    ds.lesExos = j3pShuffle(ds.lesExos)

    stor.restric = '0123456789 '

    me.typederreurs = [0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0]

    me.score = 0
    const tt = 'Fraction décimale'

    me.afficheTitre(tt)
    enonceMain()
  }
  function faisChoixExos () {
    const e = ds.lesExos.pop()
    switch (e.limite2) {
      case 'dixième': stor.den = 10
        break
      case 'centième': stor.den = 100
        break
      case 'millième': stor.den = 1000
        break
      case 'dix_millième': stor.den = 10000
        break
      case 'cent_millième': stor.den = 100000
        break
      case 'millionième': stor.den = 1000000
        break
    }
    if (e.type === '+') {
      stor.num = j3pGetRandomInt(stor.den + 1, 2 * stor.den - 1)
    } else {
      stor.num = j3pGetRandomInt(1, stor.den - 1)
    }
    if (stor.num % 10 === 0) stor.num++
    stor.deci = j3pArrondi(stor.num / stor.den, maxDecimales)
    return e
  }
  function poseQuestion () {
    if (stor.Lexo.quest === 'F') {
      poseFraction()
    } else {
      poseDecimal()
    }
  }
  function poseFraction () {
  // précise unite
    const a = ecrisBienMathquill(stor.deci)
    if (ds.Simple) {
      j3pAffiche(stor.lesdiv.consigne, null, '<b>Ecris le nombre $' + a + '$ sous la forme d’une fraction décimale.</b>\n<i>(la plus simple possible)</i>\n\n')
    } else {
      j3pAffiche(stor.lesdiv.consigne, null, '<b>Ecris le nombre $' + a + '$ sous la forme d’une fraction décimale.</b>\n\n')
    }
    const tmon = addDefaultTable(stor.lesdiv.travail, 1, 2)
    j3pAffiche(tmon[0][0], null, '$' + a + '=$')
    const t = afficheFrac(tmon[0][1])
    stor.numRep = new ZoneStyleMathquill1(t[0], { restric: stor.restric, limite: 8, limitenb: 1, enter: me.sectionCourante.bind(me) })
    stor.denRep = new ZoneStyleMathquill1(t[2], { restric: stor.restric, limite: 8, limitenb: 1, enter: me.sectionCourante.bind(me) })
    me.afficheBoutonValider()
  }
  function poseDecimal () {
    const a = ecrisBienMathquill(stor.num)
    const b = ecrisBienMathquill(stor.den)
    j3pAffiche(stor.lesdiv.consigne, null, '<b>Donne l’écriture décimale de la fraction $\\frac{' + a + '}{' + b + '}$</b>\n\n')
    const tty = addDefaultTable(stor.lesdiv.travail, 1, 2)
    j3pAffiche(tty[0][0], null, '$\\frac{' + a + '}{' + b + '} =$')
    stor.rep = new ZoneStyleMathquill1(tty[0][1], { restric: stor.restric + ',.', limite: 8, limitenb: 1, enter: me.sectionCourante.bind(me) })
    me.afficheBoutonValider()
  }
  function creeLesDiv () {
    stor.lesdiv = {}
    stor.lesdiv.conteneur = j3pAddElt(me.zonesElts.MG, 'div', {
      style: me.styles.etendre('toutpetit.enonce', { padding: '10px' })
    })
    stor.lesdiv.conteneur1 = addDefaultTable(stor.lesdiv.conteneur, 1, 1)[0][0]
    stor.lesdiv.conteneur2 = addDefaultTable(stor.lesdiv.conteneur, 1, 1)[0][0]
    stor.lesdiv.consigne = j3pAddElt(stor.lesdiv.conteneur1, 'div')
    stor.lesdiv.figure1 = j3pAddElt(stor.lesdiv.conteneur1, 'div')
    stor.lesdiv.travail = j3pAddElt(stor.lesdiv.conteneur2, 'div')
    stor.lesdiv.figure2 = j3pAddElt(stor.lesdiv.travail, 'div')
    stor.lesdiv.rep = j3pAddElt(stor.lesdiv.travail, 'div')
    stor.lesdiv.explications = j3pAddElt(stor.lesdiv.conteneur, 'div')
    stor.lesdiv.solution = j3pAddElt(stor.lesdiv.conteneur, 'div')
    stor.lesdiv.correction = j3pAddElt(me.zonesElts.MD, 'div', {
      style: me.styles.etendre('toutpetit.enonce', { padding: '10px' })
    })
    stor.lesdiv.explications.style.color = me.styles.cfaux
    stor.lesdiv.solution.style.color = me.styles.toutpetit.correction.color
    me.zonesElts.MG.classList.add('fond')
  }

  function yaReponse () {
    if (me.isElapsed) return true
    if (stor.Lexo.quest === 'F') {
      if (stor.numRep.reponse() === '') {
        stor.numRep.focus()
        return false
      }
      if (stor.denRep.reponse() === '') {
        stor.denRep.focus()
        return false
      }
    }
    if (stor.Lexo.quest === 'D') {
      if (stor.rep.reponse() === '') {
        stor.rep.focus()
        return false
      }
    }
    return true
  }
  function isRepOk () {
    let ok, vir, i
    stor.errDef = false
    stor.errEcrit = false
    stor.errEcritNum = ''
    stor.errEcritDen = ''
    stor.errSimpl1 = false
    stor.errSimpl2 = false
    stor.errF = false
    stor.errEcritDec = ''

    if (stor.Lexo.quest === 'F') {
      vir = false
      let lnum = stor.numRep.reponse()
      let lden = stor.denRep.reponse()
      ok = verifNombreBienEcrit(lnum, ds.Espace, true)
      if (!ok.good) {
        vir = true
        stor.errEcrit = true
        stor.numRep.corrige(false)
        stor.errEcritNum = ok.remede
      }
      lnum = ok.nb
      ok = verifNombreBienEcrit(lden, ds.Espace, true)
      if (!ok.good) {
        vir = true
        stor.errEcrit = true
        stor.denRep.corrige(false)
        stor.errEcritDen = ok.remede
      }
      lden = ok.nb
      if (vir) return false
      for (i = 0; i < 20; i++) {
        vir = vir || (lden === Math.pow(10, i))
      }
      if (!vir) {
        stor.errDef = true
        stor.denRep.corrige(false)
        return false
      }
      if (ds.Simple) {
        if (lden !== stor.den) {
          if (lden > stor.den) {
            stor.errSimpl1 = true
          } else {
            stor.errSimpl2 = true
          }
          stor.denRep.corrige(false)
          return false
        }
      }
      if (j3pArrondi(lnum / lden, maxDecimales) !== stor.deci) {
        if (stor.num !== lnum) stor.numRep.corrige(false)
        if (stor.den !== lden) stor.denRep.corrige(false)
        stor.errF = true
        return false
      }
      return true
    }
    if (stor.Lexo.quest === 'D') {
      const rep = stor.rep.reponse()
      ok = verifNombreBienEcrit(rep, ds.Espace, true)
      if (!ok.good) {
        stor.rep.corrige(false)
        stor.errEcrit = true
        stor.errEcritDec = ok.remede
        return false
      }
      if (ok.nb !== stor.deci) {
        stor.rep.corrige(false)
        return false
      }
      return true
    }
  }
  function desactivezone () {
    if (stor.Lexo.quest === 'F') {
      stor.numRep.disable()
      stor.denRep.disable()
    }
    if (stor.Lexo.quest === 'D') {
      stor.rep.disable()
    }
  }
  function passetoutvert () {
    if (stor.Lexo.quest === 'F') {
      stor.numRep.corrige(true)
      stor.denRep.corrige(true)
    }
    if (stor.Lexo.quest === 'D') {
      stor.rep.corrige(true)
    }
  }
  function barrelesfo () {
    if (stor.Lexo.quest === 'F') {
      stor.numRep.barreIfKo()
      stor.denRep.barreIfKo()
    }
    if (stor.Lexo.quest === 'D') {
      stor.rep.barreIfKo()
    }
  }
  function AffCorrection (bool) {
    if (stor.errDef) {
      stor.yaexplik = true
      j3pAffiche(stor.lesdiv.explications, null, 'Le dénominateur d’une fraction décimale doit être $10$ , ou $100$ , ou $1000$ , ou ... !')
    }
    if (stor.errEcrit) {
      stor.yaexplik = true
      j3pAffiche(stor.lesdiv.explications, null, '<u>Erreur d’écriture</u>:\n')
      if (stor.errEcritNum !== '') {
        j3pAffiche(stor.lesdiv.explications, null, 'au numérateur: ' + stor.errEcritNum + '\n')
      }
      if (stor.errEcritDen !== '') {
        j3pAffiche(stor.lesdiv.explications, null, 'au dénominateur: ' + stor.errEcritDen + '\n')
      }
      if (stor.errEcritDec !== '') {
        j3pAffiche(stor.lesdiv.explications, null, stor.errEcritDec + '\n')
      }
    }
    if (stor.errSimpl1) {
      stor.yaexplik = true
      j3pAffiche(stor.lesdiv.explications, null, 'Tu peux choisir un dénominateur plus petit !')
    }
    if (stor.errSimpl2) {
      stor.yaexplik = true
      j3pAffiche(stor.lesdiv.explications, null, 'Tu dois choisir un dénominateur plus grand !')
    }

    if (bool) {
      stor.yaco = true
      let a, b, c
      desactivezone()
      barrelesfo()
      if (stor.Lexo.quest === 'F') {
        a = ecrisBienMathquill(stor.deci)
        b = ecrisBienMathquill(stor.num)
        c = ecrisBienMathquill(stor.den)
        j3pAffiche(stor.lesdiv.solution, null, '$' + a + '=\\frac{' + b + '}{' + c + '}$ ')
      }
      if (stor.Lexo.quest === 'D') {
        a = ecrisBienMathquill(stor.deci)
        b = ecrisBienMathquill(stor.num)
        c = ecrisBienMathquill(stor.den)
        j3pAffiche(stor.lesdiv.solution, null, '$\\frac{' + b + '}{' + c + '} = ' + a + '$ ')
      }
    } else { me.afficheBoutonValider() }
  }

  function enonceMain () {
    if (me.etapeCourante === 1) {
      // A chaque répétition, on nettoie la scène
      me.videLesZones()
      creeLesDiv()
      stor.Lexo = faisChoixExos()
      stor.lesdiv.travail.classList.add('travail')
      stor.lesdiv.conteneur1.classList.add('enonce')
    }

    poseQuestion()
    me.finEnonce()
  }

  switch (me.etat) {
    case 'enonce':
      // code exécuté au lancement de la section
      if (me.debutDeLaSection) {
        initSection()
      } else {
        enonceMain()
      }
      break // case "enonce":

    case 'correction':
      // On teste si une réponse a été saisie
      stor.yaexplik = false
      stor.lesdiv.explications.innerHTML = ''
      stor.lesdiv.correction.innerHTML = ''
      stor.yaco = false

      if (yaReponse()) {
        // Bonne réponse
        if (isRepOk()) {
          me.score++
          stor.lesdiv.correction.style.color = me.styles.cbien
          stor.lesdiv.correction.innerHTML = cBien
          desactivezone()
          passetoutvert()
          me.typederreurs[0]++
          me.finCorrection('navigation', true)
        } else {
          // Pas de bonne réponse
          stor.lesdiv.correction.style.color = me.styles.cfaux
          stor.lesdiv.correction.innerHTML = cFaux
          if (me.isElapsed) {
            // A cause de la limite de temps :
            stor.lesdiv.correction.innerHTML += tempsDepasse + '<BR>'
            me.typederreurs[10]++
            AffCorrection(true)
            me.finCorrection('navigation', true)
          } else {
            // Réponse fausse :
            if (me.essaiCourant < me.donneesSection.nbchances) {
              // il reste des essais, on reste en etat correction
              stor.lesdiv.correction.innerHTML += '<br>' + essaieEncore
              AffCorrection(false)
              me.typederreurs[1]++
              me.finCorrection()
            } else {
              // Erreur au nème essai
              AffCorrection(true)
              me.typederreurs[2]++
              me.finCorrection('navigation', true)
            }
          }
        }
      } else {
        // pas de réponse
        stor.lesdiv.correction.style.color = me.styles.cfaux
        stor.lesdiv.correction.innerHTML = reponseIncomplete
        me.finCorrection()
      }

      if (ds.theme === 'zonesAvecImageDeFond') {
        if (stor.yaexplik) {
          stor.lesdiv.explications.classList.add('explique')
        }
        if (stor.yaco) {
          stor.lesdiv.solution.classList.add('correction')
        }
      }
      break // case "correction":

    case 'navigation':

      if (me.sectionTerminee()) {
        // On détermine la phrase d’état renvoyée par la section
        me.parcours.pe = ''
        me.afficheBoutonSectionSuivante()
        me.focus('sectioncontinuer')
      } else {
        me.etat = 'enonce'
        me.afficheBoutonSuite()
        me.focus('boutoncontinuer')
      }
      // Obligatoire
      me.finNavigation()
      break // case "navigation":
  }
}
