import { j3pAddElt, j3pAjouteBouton, j3pDetruit, j3pEmpty, j3pGetRandomInt, j3pPGCD, j3pShuffle } from 'src/legacy/core/functions'
import { j3pAffiche } from 'src/lib/mathquill/functions'
import ZoneStyleMathquill1 from 'src/legacy/outils/zoneStyleMathquill/ZoneStyleMathquill1'
import { j3pCreeFenetres, j3pAfficheCroixFenetres, j3pToggleFenetres } from 'src/legacy/core/functionsJqDialog'
import { addDefaultTable } from 'src/legacy/themes/table'
import { afficheFrac } from 'src/legacy/outils/zoneStyleMathquill/functions'
import textesGeneriques from 'src/lib/core/textes'

const { cBien, cFaux, essaieEncore, reponseIncomplete, tempsDepasse } = textesGeneriques

/**
 * Ajouter une description ici
 * @fileOverview
 * @author Tommy Barroy <t.barroy@laposte.net>
 * @since 2017-10
*/

export const params = {
  outils: ['mathquill', 'calculatrice'],
  /**
   * Ce qui est paramétrable dans l’éditeur de graphe
   * @type {LegacySectionParamsParametres}
   */
  parametres: [
    ['nbrepetitions', 3, 'entier', 'Nombre de répétitions de la section'],
    ['nbchances', 2, 'entier', 'Nombre d’essais possibles'],
    ['Justifie', true, 'boolean', '<u>true</u>: L’élève doit d’abord décomposer en somme de deux fractions.'],
    ['Calcul_mental', true, 'boolean', '<u>true</u>: L’exercice peut se faire de tête.'],
    ['Calculatrice', true, 'boolean', '<u>true</u>: L’élève dispose d’une calculatrice.'],
    // le modèle gère la compatibilité ascendante avec l’ancien paramètre MiseEnPagePlusClaire
    ['theme', 'standard', 'liste', 'Modèle pour l’affichage', ['standard', 'zonesAvecImageDeFond']]
  ]
}

/**
 * section fraction12
 * @this {Parcours}
 */
export default function main () {
  const me = this
  const stor = me.storage
  let ds = me.donneesSection

  function getDonnees () {
    return {
      // Nombre de répétitions de l’exercice
      nbrepetitions: 3,

      Justifie: true,
      Calculatrice: true,
      Calcul_mental: true,
      // Si indication != "" alors un lien "Indication" s’affiche dans la zone Inférieure Gauche (me.zones.IG
      // une indication<br>de deux lignes..."
      indication: '',

      // pour le cas de presentation1bis
      // En effet pas d’indication possible pour cette présentation
      // surchargeindication: false,

      // Nombre de chances dont dispose l’él§ve pour répondre à la question
      nbchances: 2,

      //  || "presentation2" || "presentation3"  || "presentation1bis"
      structure: 'presentation1bis'
    }
  }

  function initSection () {
    let i, max

    ds = getDonnees()
    me.donneesSection = ds
    // On surcharge avec les parametres passés par le graphe
    me.surcharge()
    me.validOnEnter = false // ex donneesSection.touche_entree

    // Construction de la page
    me.construitStructurePage(me.donneesSection.structure)

    me.donneesSection.nbetapes = 1
    stor.afaire = ['enc']
    if (ds.Justifie) {
      me.donneesSection.nbetapes = 2
      stor.afaire = ['jus', 'enc']
    }
    stor.encours = stor.afaire[stor.afaire.length - 1]
    me.donneesSection.nbitems = me.donneesSection.nbetapes * me.donneesSection.nbrepetitions

    ds.lesExos = []

    max = 100
    let lp = []
    if (ds.Calcul_mental) max = 11
    for (i = 1; i < max + 1; i++) lp.push(i)
    lp = j3pShuffle(lp)
    for (i = 0; i < me.donneesSection.nbitems; i++) {
      ds.lesExos.push({ nb: lp[i % lp.length] })
    }
    ds.lesExos = j3pShuffle(ds.lesExos)

    me.typederreurs = [0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0]

    me.score = 0
    const tt = 'Décomposer une fraction'

    me.afficheTitre(tt)

    enonceMain()
  }

  function faisChoixExos () {
    const e = ds.lesExos.pop()
    stor.restric = '0123456789'
    stor.nm = e.nb
    stor.den = j3pGetRandomInt(2, 11)
    do {
      stor.num = stor.nm * stor.den + j3pGetRandomInt(1, stor.den - 1)
    } while (j3pPGCD(Math.abs(stor.num), Math.abs(stor.den, { negativesAllowed: true, valueIfZero: 1 })) !== 1)
    stor.dev = true
    stor.laf = '\\frac{' + stor.num + '}{' + stor.den + '}'
    stor.foco = false
    stor.lbar1 = stor.lbar2 = undefined
    return e
  }

  function poseQuestion () {
    if (stor.foco) {
      j3pEmpty(stor.lesdiv.travailjus)
      j3pEmpty(stor.lesdiv.explikjus)
    }
    let t
    if (stor.afaire.length === 1 || stor.encours === 'jus') {
      j3pAffiche(stor.lesdiv.consigne, null, 'On désire décomposer la fraction $' + stor.laf + '$ comme la somme \nd’un entier et d’une autre fraction (plus petite que $1$).\n\n')
      stor.lesdiv.consigne.classList.add('enonce')
    }
    if (stor.encours === 'enc') {
      if (stor.lbar1 !== undefined) j3pDetruit(stor.lbar1)
      if (stor.lbar2 !== undefined) j3pDetruit(stor.lbar2)
      stor.lbar1 = stor.lbar2 = undefined
      j3pEmpty(stor.lesdiv.enoncejus)
      j3pAffiche(stor.lesdiv.enonceenc, null, '<b>Donne la décomposition de $' + stor.laf + '$.</b>')
      const tyu = addDefaultTable(stor.lesdiv.travailenc, 1, 5)
      t = afficheFrac(tyu[0][0])
      j3pAffiche(t[2], null, '$' + stor.den + '$')
      j3pAffiche(t[0], null, '$' + stor.num + '$')
      j3pAffiche(tyu[0][1], null, '&nbsp;$=$&nbsp;')
      j3pAffiche(tyu[0][3], null, '&nbsp;$+$&nbsp;')
      let where1, where2
      if (stor.dev) {
        where1 = tyu[0][2]; where2 = tyu[0][4]
      } else {
        where2 = tyu[0][2]; where1 = tyu[0][4]
      }
      stor.zoneEnc1 = new ZoneStyleMathquill1(where1, { restric: stor.restric, hasAutoKeyboard: false, limite: 6, enter: me.sectionCourante.bind(me) })
      t = afficheFrac(where2)
      stor.zoneEnc2 = new ZoneStyleMathquill1(t[0], { restric: stor.restric, hasAutoKeyboard: false, limite: 6, enter: me.sectionCourante.bind(me) })
      stor.zoneEnc3 = new ZoneStyleMathquill1(t[2], { restric: stor.restric, hasAutoKeyboard: false, limite: 6, enter: me.sectionCourante.bind(me) })
      stor.lesdiv.travailenc.classList.add('travail')
      stor.lesdiv.enonceenc.classList.add('enonce')
      stor.lesdiv.travailjus.classList.remove('travail')
      stor.lesdiv.enoncejus.classList.remove('enonce')
      stor.lesdiv.solutionjus.classList.add('enonce')
    } else {
      j3pAffiche(stor.lesdiv.enoncejus, null, '<b>Complète l’égalité. </b>')
      const yui = addDefaultTable(stor.lesdiv.travailjus, 1, 5)
      t = afficheFrac(yui[0][0])
      j3pAffiche(t[2], null, '$' + stor.den + '$')
      j3pAffiche(t[0], null, '$' + stor.num + '$')
      j3pAffiche(yui[0][1], null, '&nbsp;$=$&nbsp;')
      t = afficheFrac(yui[0][2])
      j3pAffiche(t[2], null, '$' + stor.den + '$')
      stor.zoneJus1 = new ZoneStyleMathquill1(t[0], { restric: stor.restric, hasAutoKeyboard: false, limite: 6, enter: me.sectionCourante.bind(me) })
      j3pAffiche(yui[0][3], null, '&nbsp;$+$&nbsp;')
      t = afficheFrac(yui[0][4])
      j3pAffiche(t[2], null, '$' + stor.den + '$')
      stor.zoneJus2 = new ZoneStyleMathquill1(t[0], { restric: stor.restric, hasAutoKeyboard: false, limite: 6, enter: me.sectionCourante.bind(me) })
      stor.lesdiv.travailjus.classList.add('travail')
      stor.lesdiv.enoncejus.classList.add('enonce')
    }
    stor.lesdiv.solutionenc.classList.remove('correction')
    stor.lesdiv.solutionjus.classList.remove('correction')
    stor.lesdiv.explikjus.classList.remove('explique')
    stor.lesdiv.explikenc.classList.remove('explique')
  }

  function creeLesDiv () {
    stor.lesdiv = {}
    stor.lesdiv.conteneur = stor.lesdiv.conteneur = j3pAddElt(me.zonesElts.MG, 'div', {
      style: me.styles.etendre('toutpetit.enonce', { padding: '10px' })
    })
    stor.lesdiv.consigne = addDefaultTable(stor.lesdiv.conteneur, 1, 1)[0][0]
    const cont = addDefaultTable(stor.lesdiv.conteneur, 8, 1)
    stor.lesdiv.enoncejus = cont[0][0]
    stor.lesdiv.travailjus = cont[1][0]
    stor.lesdiv.explikjus = cont[2][0]
    stor.lesdiv.solutionjus = cont[3][0]
    stor.lesdiv.enonceenc = cont[4][0]
    stor.lesdiv.travailenc = cont[5][0]
    stor.lesdiv.explikenc = cont[6][0]
    stor.lesdiv.solutionenc = cont[7][0]
    stor.lesdiv.calculatrice = j3pAddElt(me.zonesElts.MD, 'div', {
      style: me.styles.etendre('toutpetit.enonce', { padding: '10px' })
    })
    stor.lesdiv.correction = j3pAddElt(me.zonesElts.MD, 'div', {
      style: me.styles.etendre('toutpetit.enonce', { padding: '10px' })
    })
    stor.lesdiv.explikjus.style.color = me.styles.cfaux
    stor.lesdiv.explikenc.style.color = me.styles.cfaux
    stor.lesdiv.solutionjus.style.color = me.styles.toutpetit.correction.color
    stor.lesdiv.solutionenc.style.color = me.styles.toutpetit.correction.color
    me.zonesElts.MG.classList.add('fond')
  }

  function boutonfen () {
    j3pToggleFenetres('Calculatrice')
  }

  function yaReponse () {
    if (stor.encours === 'jus') {
      if (stor.zoneJus1.reponse() === '') {
        stor.zoneJus1.focus()
        return false
      }
      if (stor.zoneJus2.reponse() === '') {
        stor.zoneJus2.focus()
        return false
      }
    }
    if (stor.encours === 'enc') {
      if (stor.zoneEnc1.reponse() === '') {
        stor.zoneEnc1.focus()
        return false
      }
      if (stor.zoneEnc2.reponse() === '') {
        stor.zoneEnc2.focus()
        return false
      }
      if (stor.zoneEnc3.reponse() === '') {
        stor.zoneEnc3.focus()
        return false
      }
    }
    return true
  }

  function isRepOk () {
    let r1, r2, ok, r3
    stor.errMult = false
    stor.errAdd = false
    stor.errMax = false

    stor.errTG = false
    stor.errTP = false
    stor.errDen = false
    stor.errReste = false

    if (stor.encours === 'jus') {
      ok = true
      r1 = parseInt(stor.zoneJus1.reponse())
      r2 = parseInt(stor.zoneJus2.reponse())
      if (r1 + r2 !== stor.num) {
        stor.errAdd = true
        if (r1 !== stor.num % stor.den && r1 !== Math.floor(stor.num / stor.den) * stor.den)stor.zoneJus1.corrige(false)
        if (r2 !== stor.num % stor.den && r2 !== Math.floor(stor.num / stor.den) * stor.den)stor.zoneJus2.corrige(false)
        return false
      }
      if (r1 % stor.den !== 0 && r2 % stor.den !== 0) {
        stor.errMult = true
        if (r1 !== stor.num % stor.den)stor.zoneJus1.corrige(false)
        if (r2 !== stor.num % stor.den)stor.zoneJus2.corrige(false)
        return false
      }
      if (r1 !== Math.floor(stor.num / stor.den) * stor.den && r2 !== Math.floor(stor.num / stor.den) * stor.den) {
        stor.errMax = true
        if (r1 % stor.den === 0) { stor.garde = r1 } else { stor.garde = r2 }
        stor.zoneJus1.corrige(false)
        stor.zoneJus2.corrige(false)
        return false
      }
      stor.dev = (r1 % stor.den === 0)
      return true
    }
    if (stor.encours === 'enc') {
      ok = true
      r1 = parseInt(stor.zoneEnc1.reponse())
      r2 = parseInt(stor.zoneEnc2.reponse())
      r3 = parseInt(stor.zoneEnc3.reponse())
      if (r1 > Math.floor(stor.num / stor.den)) {
        stor.errTG = true
        stor.zoneEnc1.corrige(false)
        ok = false
      }
      if (r1 < Math.floor(stor.num / stor.den)) {
        stor.errTP = true
        stor.zoneEnc1.corrige(false)
        ok = false
      }
      if (r3 !== stor.den) {
        stor.errDen = true
        ok = false
        stor.zoneEnc3.corrige(false)
      }
      if (ok) {
        if (r2 !== stor.num % stor.den) {
          ok = false
          stor.errReste = true
          stor.zoneEnc2.corrige(false)
        }
      }
      return ok
    }
  }

  function desactiveAll () {
    if (stor.encours === 'jus') {
      stor.zoneJus1.disable()
      stor.zoneJus2.disable()
    }
    if (stor.encours === 'enc') {
      stor.zoneEnc1.disable()
      stor.zoneEnc2.disable()
      stor.zoneEnc3.disable()
    }
  }

  function barrelesfo () {
    if (stor.encours === 'jus') {
      if (stor.zoneJus1.bon === false) stor.lbar1 = stor.zoneJus1.barre()
      if (stor.zoneJus2.bon === false) stor.lbar2 = stor.zoneJus2.barre()
    }
    if (stor.encours === 'enc') {
      stor.zoneEnc1.barreIfKo()
      stor.zoneEnc2.barreIfKo()
      stor.zoneEnc3.barreIfKo()
    }
  }

  function mettouvert () {
    if (stor.encours === 'jus') {
      stor.zoneJus1.corrige(true)
      stor.zoneJus2.corrige(true)
    }
    if (stor.encours === 'enc') {
      stor.zoneEnc1.corrige(true)
      stor.zoneEnc2.corrige(true)
      stor.zoneEnc3.corrige(true)
    }
  }
  function AffCorrection (bool) {
    j3pEmpty(stor.lesdiv.explikjus)
    j3pEmpty(stor.lesdiv.explikenc)
    if (stor.errMult) {
      stor.yaexplik = true
      j3pAffiche(stor.lesdiv.explikjus, null, 'Tu dois choisir un multiple de $' + stor.den + '$ pour obtenir ensuite un entier !\n')
    }
    if (stor.errAdd) {
      stor.yaexplik = true
      j3pAffiche(stor.lesdiv.explikjus, null, '$' + stor.num + '$ n’est pas égal à $' + stor.zoneJus1.reponse() + ' + ' + stor.zoneJus2.reponse() + '$ !\n')
    }
    if (stor.errMax) {
      stor.yaexplik = true
      j3pAffiche(stor.lesdiv.explikjus, null, 'Tu peux un choisir un multiple de $' + stor.den + '$ plus grand que $' + stor.garde + '$ !\n')
    }

    if (stor.errTG) {
      stor.yaexplik = true
      j3pAffiche(stor.lesdiv.explikenc, null, '$' + stor.zoneEnc1.reponse() + '$  est plus grand que $' + stor.laf + '$ ! \n')
    }
    if (stor.errTP) {
      stor.yaexplik = true
      j3pAffiche(stor.lesdiv.explikenc, null, 'Tu peux choisir un entier plus grand que $' + stor.zoneEnc1.reponse() + '$ !\n')
    }
    if (stor.errDen) {
      stor.yaexplik = true
      j3pAffiche(stor.lesdiv.explikenc, null, 'Ne change pas le dénominateur pour la fraction restante !\n')
    }
    if (stor.errReste) {
      stor.yaexplik = true
      j3pAffiche(stor.lesdiv.explikenc, null, 'Le numérateur de la fraction restante est faux !\n')
    }

    if (bool) {
      barrelesfo()
      desactiveAll()
      stor.yaco = true
      stor.foco = true
      if (stor.encours === 'jus') {
        stor.dev = true
        j3pAffiche(stor.lesdiv.solutionjus, null, '$' + stor.laf + ' =  \\frac{' + (stor.num - stor.num % stor.den) + '}{' + stor.den + '} + \\frac{' + (stor.num % stor.den) + '}{' + stor.den + '}$')
      }
      if (stor.encours === 'enc') {
        if (stor.dev) {
          if (stor.afaire.length === 1) j3pAffiche(stor.lesdiv.solutionenc, null, '$' + stor.laf + ' = \\frac{' + (stor.num - stor.num % stor.den) + '}{' + stor.den + '} + \\frac{' + stor.num % stor.den + '}{' + stor.den + '}$ \n')
          j3pAffiche(stor.lesdiv.solutionenc, null, '$' + stor.laf + ' = ' + Math.floor(stor.num / stor.den) + ' + \\frac{' + stor.num % stor.den + '}{' + stor.den + '}$')
        } else {
          j3pAffiche(stor.lesdiv.solutionenc, null, '$' + stor.laf + ' =  \\frac{' + stor.num % stor.den + '}{' + stor.den + '} + ' + Math.floor(stor.num / stor.den) + '$')
        }
      }
    } else { me.afficheBoutonValider() }
  }

  function enonceMain () {
    if (me.etapeCourante === 1) {
      // A chaque répétition, on nettoie la scène
      me.videLesZones()
      creeLesDiv()
      stor.Lexo = faisChoixExos()
    }

    /// //////////////////////////////////
    /* LE CODE PRINCIPAL DE LA SECTION */
    /// //////////////////////////////////
    let ss = stor.afaire.indexOf(stor.encours) + 1
    if (ss === stor.afaire.length) ss = 0
    stor.encours = stor.afaire[ss]
    poseQuestion()
    j3pEmpty(stor.lesdiv.correction)
    me.afficheBoutonValider()
    me.cacheBoutonSuite()
    j3pEmpty(stor.lesdiv.calculatrice)
    if (ds.Calculatrice) {
      me.fenetresjq = [{ name: 'Calculatrice', title: 'Une calculatrice', left: 600, top: 10 }]
      j3pCreeFenetres(me)
      j3pAfficheCroixFenetres('Calculatrice')
      j3pAjouteBouton(stor.lesdiv.calculatrice, 'Calculatrice', 'MepBoutons', 'Calculatrice', boutonfen)
    }
    me.finEnonce()
  }

  switch (me.etat) {
    case 'enonce':
      // code exécuté au lancement de la section
      if (me.debutDeLaSection) {
        initSection()
      } else {
        enonceMain()
      }
      break // case "enonce":

    case 'correction':
      // On teste si une réponse a été saisie
      stor.yaexplik = false
      stor.yaco = false
      stor.lesdiv.solutionenc.classList.remove('correction')
      stor.lesdiv.solutionjus.classList.remove('correction')
      stor.lesdiv.explikjus.classList.remove('explique')
      stor.lesdiv.explikenc.classList.remove('explique')

      if (yaReponse()) {
        // Bonne réponse
        if (isRepOk()) {
          me._stopTimer()
          stor.lesdiv.correction.style.color = me.styles.cbien
          stor.lesdiv.correction.innerHTML = cBien

          desactiveAll()
          mettouvert()

          j3pEmpty(stor.lesdiv.explikjus)
          j3pEmpty(stor.lesdiv.explikenc)

          me.cacheBoutonValider()
          me.score++

          me.typederreurs[0]++
          me.etat = 'navigation'
          me.sectionCourante()
        } else {
          // Pas de bonne réponse
          stor.lesdiv.correction.style.color = me.styles.cfaux
          stor.lesdiv.correction.innerHTML = cFaux
          if (me.isElapsed) {
            // A cause de la limite de temps :
            me._stopTimer()

            stor.lesdiv.correction.innerHTML += tempsDepasse + '<BR>'
            me.typederreurs[10]++
            me.cacheBoutonValider()

            AffCorrection(true)

            me.etat = 'navigation'
            me.sectionCourante()
          } else {
            // Réponse fausse :
            if (me.essaiCourant < me.donneesSection.nbchances) {
              stor.lesdiv.correction.innerHTML += '<br>' + essaieEncore

              AffCorrection(false)

              me.typederreurs[1]++
            } else {
              // Erreur au nème essai
              me._stopTimer()
              me.cacheBoutonValider()

              AffCorrection(true)

              if (stor.encours === 'p') {
                stor.pose = false
                stor.encours = 't'
              }

              me.typederreurs[2]++
              me.etat = 'navigation'
              me.sectionCourante()
            }
          }
        }
      } else {
        // pas de réponse
        stor.lesdiv.correction.style.color = me.styles.cfaux
        stor.lesdiv.correction.innerHTML = reponseIncomplete
        if (stor.encours === 'm') {
          stor.lesdiv.correction.innerHTML = 'Réponse incomplète ! <br> Tu dois déplacer le point ' + stor.C + ' .'
        }
        me.afficheBoutonValider()
      }

      if (stor.yaexplik) {
        if (stor.encours === 'jus') stor.lesdiv.explikjus.classList.add('explique')
        if (stor.encours === 'enc') stor.lesdiv.explikenc.classList.add('explique')
      }
      if (stor.yaco) {
        if (stor.encours === 'jus') stor.lesdiv.solutionjus.classList.add('correction')
        if (stor.encours === 'enc') stor.lesdiv.solutionenc.classList.add('correction')
      }

      // Obligatoire
      me.finCorrection()
      break // case "correction":

    case 'navigation':
      if (!this.sectionTerminee()) {
        this.etat = 'enonce'
      }
      this.finNavigation()
      break // case "navigation":
  }
}
