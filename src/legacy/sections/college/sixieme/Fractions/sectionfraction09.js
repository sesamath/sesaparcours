import { j3pAddElt, j3pArrondi, j3pGetNewId, j3pGetRandomBool, j3pGetRandomInt, j3pModale, j3pPGCD, j3pShowError, j3pShuffle } from 'src/legacy/core/functions'
import { verifNombreBienEcrit } from 'src/legacy/outils/fonctions/ecritureNombre'
import { j3pAffiche } from 'src/lib/mathquill/functions'
import { ecrisBienMathquill, afficheFrac } from 'src/legacy/outils/zoneStyleMathquill/functions'
import ZoneStyleMathquill1 from 'src/legacy/outils/zoneStyleMathquill/ZoneStyleMathquill1'
import { addDefaultTable } from 'src/legacy/themes/table'
import { j3pCreeSVG } from 'src/legacy/core/functionsSvg'
import textesGeneriques from 'src/lib/core/textes'
import { getMtgCore } from 'src/lib/outils/mathgraph'

const { cBien, cFaux, essaieEncore, reponseIncomplete, tempsDepasse } = textesGeneriques

/**
 * Ajouter une description ici
 * @fileOverview
 * @author Tommy Barroy <t.barroy@laposte.net>
 * @since 2017-10
*/

export const params = {
  outils: ['mathquill'],
  /**
   * Ce qui est paramétrable dans l’éditeur de graphe
   * @type {LegacySectionParamsParametres}
   */
  parametres: [
    ['nbrepetitions', 3, 'entier', 'Nombre de répétitions de la section'],
    ['nbchances', 2, 'entier', 'Nombre d’essais possibles.'],
    ['Question', 'les deux', 'liste', '<u>Placer</u>: L’élève doit placer un point d’abscisse donnée <br><br> <u>Lire</u>: L’élève doit lire une abscisse.', ['Placer', 'Lire', 'les deux']],
    ['Positifs', 'parfois', 'liste', 'Les abscisses sont positives', ['toujours', 'jamais', 'parfois']],
    ['Multiple', 'parfois', 'liste', 'Le dénominateur est un multiple des graduations proposées', ['toujours', 'jamais', 'parfois']],
    ['Origine', 'parfois', 'liste', 'Origine de la droite/demi-droite visible.', ['toujours', 'jamais', 'parfois']],
    ['Unite', 'parfois', 'liste', 'L’abscisse est comprise entre 0 et 1 (ou -1).', ['toujours', 'jamais', 'parfois']],
    ['Nature', 'fraction', 'liste', 'Nature de l’abscisse', ['fraction', 'décimal', 'les deux']],
    ['Rangmin', 'millième', 'liste', '<i>Uniquement quand Nature est à <b>décimal</b></i>', ['millième', 'centième', 'dixième', 'unité', 'dizaine', 'centaine', 'millier']],
    ['Rangmax', 'millier', 'liste', '<i>Uniquement quand Nature est à <b>décimal</b></i>', ['millième', 'centième', 'dixième', 'unité', 'dizaine', 'centaine', 'millier']],
    ['Espace', true, 'boolean', '<i>Uniquement quand Nature est à <b>décimal</b></i><br><br><u>true</u>: IL faut mettre des espaces pour séparer les groupes.'],
    ['Simplifie', false, 'boolean', '<u>true</u>: Le réponse doit être simplifiée.'],
    // le modèle gère la compatibilité ascendante avec l’ancien paramètre MiseEnPagePlusClaire
    ['theme', 'standard', 'liste', 'Modèle pour l’affichage', ['standard', 'zonesAvecImageDeFond']]
  ]
}

/**
 * section fraction09
 * @this {Parcours}
 */
export default function main () {
  const me = this
  let ds = me.donneesSection
  const stor = me.storage
  let idSvg = stor.idSvg

  function getDonnees () {
    return {
      // Nombre de répétitions de l’exercice
      nbrepetitions: 3,

      Positifs: 'parfois',
      Multiple: 'parfois',
      Origine: 'parfois',
      Question: 'les deux',
      Simplifie: false,
      Unite: 'parfois',
      Rangmin: 'millième',
      Rangmax: 'millier',
      Nature: 'fraction',
      Espace: true,

      // Si indication != "" alors un lien "Indication" s’affiche dans la zone Inférieure Gauche (this.zones.IG
      // une indication<br>de deux lignes..."
      indication: '',

      // pour le cas de presentation1bis
      // En effet pas d’indication possible pour cette présentation
      // surchargeindication: false,

      // Nombre de chances dont dispose l’él§ve pour répondre à la question
      nbchances: 2,

      //  || "presentation2" || "presentation3"  || "presentation1bis"
      structure: 'presentation3'
    }
  }

  function initSection () {
    let i
    ds = getDonnees()
    me.donneesSection = ds
    // On surcharge avec les parametres passés par le graphe
    me.surcharge()
    me.validOnEnter = false // ex donneesSection.touche_entree
    // Construction de la page
    me.construitStructurePage(me.donneesSection.structure, 0.75)
    me.donneesSection.nbetapes = 1
    me.donneesSection.nbitems = me.donneesSection.nbetapes * me.donneesSection.nbrepetitions

    ds.lesExos = []
    let lp = [2, 3, 4, 5, 6, 7, 8, 9, 10, 11, 12]
    lp = j3pShuffle(lp)
    for (i = 0; i < me.donneesSection.nbitems; i++) {
      ds.lesExos.push({ grad: lp[i % lp.length] })
    }
    ds.lesExos = j3pShuffle(ds.lesExos)

    lp = [false]
    if (ds.Multiple === 'parfois') lp.push(true)
    if (ds.Multiple === 'toujours') lp = [true]
    lp = j3pShuffle(lp)
    for (i = 0; i < me.donneesSection.nbitems; i++) {
      ds.lesExos[i].multiple = lp[i % lp.length]
    }
    ds.lesExos = j3pShuffle(ds.lesExos)

    lp = []
    if (ds.Nature === 'fraction' || ds.Nature === 'les deux') lp.push('frac')
    if (ds.Nature === 'décimal' || ds.Nature === 'les deux') lp.push('dec')
    lp = j3pShuffle(lp)
    for (i = 0; i < me.donneesSection.nbitems; i++) {
      ds.lesExos[i].nat = lp[i % lp.length]
    }
    ds.lesExos = j3pShuffle(ds.lesExos)

    lp = ['lire']
    if (ds.Question === 'Placer') lp = ['placer']
    if (ds.Question === 'les deux') lp.push('placer')
    lp = j3pShuffle(lp)
    for (i = 0; i < me.donneesSection.nbitems; i++) {
      ds.lesExos[i].question = lp[i % lp.length]
    }
    ds.lesExos = j3pShuffle(ds.lesExos)

    if (ds.Origine === 'jamais') ds.Unite = 'jamais'
    lp = [false]
    if (ds.Origine === 'parfois') lp.push(true)
    if (ds.Origine === 'toujours') lp = [true]
    lp = j3pShuffle(lp)
    for (i = 0; i < me.donneesSection.nbitems; i++) {
      ds.lesExos[i].origine = lp[i % lp.length]
    }
    ds.lesExos = j3pShuffle(ds.lesExos)

    lp = [false]
    if (ds.Positifs === 'parfois') lp.push(true)
    if (ds.Positifs === 'toujours') lp = [true]
    lp = j3pShuffle(lp)
    for (i = 0; i < me.donneesSection.nbitems; i++) {
      ds.lesExos[i].signe = lp[i % lp.length]
    }
    ds.lesExos = j3pShuffle(ds.lesExos)

    lp = [false]
    if (ds.Unite === 'parfois') lp.push(true)
    if (ds.Unite === 'toujours') lp = [true]
    lp = j3pShuffle(lp)
    for (i = 0; i < me.donneesSection.nbitems; i++) {
      ds.lesExos[i].unite = lp[i % lp.length]
    }
    ds.lesExos = j3pShuffle(ds.lesExos)

    const ttu = ['millième', 'centième', 'dixième', 'unité', 'dizaine', 'centaine', 'millier']
    ds.Rangmax = ttu.indexOf(ds.Rangmax) - 3
    ds.Rangmin = ttu.indexOf(ds.Rangmin) - 3
    ds.Rangmax = Math.min(3, Math.max(-3, ds.Rangmax))
    ds.Rangmin = Math.min(3, Math.max(-3, ds.Rangmin))
    ds.Rangmax = Math.max(ds.Rangmax, ds.Rangmin)

    lp = []
    for (i = ds.Rangmin; i < ds.Rangmax + 1; i++) {
      lp.push(i)
    }
    lp = j3pShuffle(lp)
    for (i = 0; i < me.donneesSection.nbitems; i++) {
      ds.lesExos[i].rang = lp[i % lp.length]
    }
    ds.lesExos = j3pShuffle(ds.lesExos)

    me.typederreurs = [0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0]

    me.score = 0
    let tt = ''
    if (ds.Question === 'Placer') tt = 'Placer un point d’abscisse donnée.'
    if (ds.Question === 'Lire') tt = 'Retrouver l’abscisse d’un point.'
    if (ds.Question === 'les deux') tt = 'Axe gradué et abscisse.'

    me.afficheTitre(tt)

    // chargement mtg
    getMtgCore({ withMathJax: true })
      .then(
        // success
        (mtgAppLecteur) => {
          stor.mtgAppLecteur = mtgAppLecteur
          enonceMain()
        },
        // failure
        (error) => {
          j3pShowError(error, { message: 'mathgraph n’est pas correctement chargé', mustNotify: true })
        })
      // plantage dans enonceMain
      .catch(j3pShowError)
  } // initSection

  function faisChoixExos () {
    const e = ds.lesExos.pop()

    stor.centre = j3pGetRandomInt(3, 10)
    if (e.origine || e.unite) stor.centre = 0
    if (!e.signe) stor.centre = -stor.centre

    if (e.nat === 'frac') {
      stor.compt = e.grad
      if (e.multiple) {
        if ([2, 3, 5, 7, 11].indexOf(e.grad) !== -1) {
          const tb = [4, 6, 8, 9, 10, 12]
          e.grad = tb[j3pGetRandomInt(0, 5)]
        }
        switch (e.grad) {
          case 4:
            stor.compt = 2
            break
          case 6:
            if (j3pGetRandomBool()) {
              stor.compt = 2
            } else {
              stor.compt = 3
            }
            break
          case 8:
            if (j3pGetRandomBool()) {
              stor.compt = 2
            } else {
              stor.compt = 4
            }
            break
          case 9:
            stor.compt = 3
            break
          case 10:
            if (j3pGetRandomBool()) {
              stor.compt = 2
            } else {
              stor.compt = 5
            }
            break
          case 12: {
            const nb = j3pGetRandomInt(0, 3)
            stor.compt = nb === 3 ? 6 : nb + 2
          }
        }
      }
      do {
        if (ds.Positifs === 'toujours' && stor.centre === 0) {
          do {
            stor.num = j3pGetRandomInt(1, stor.compt - 1)
          } while (j3pPGCD(stor.num, stor.compt, { negativesAllowed: true, valueIfZero: 1 }) !== 1)
          stor.num += stor.compt * j3pGetRandomInt(0, 2)
        }
        if (e.unite) {
          do {
            stor.num = j3pGetRandomInt(1, stor.compt - 1)
          } while (j3pPGCD(stor.num, stor.compt, { negativesAllowed: true, valueIfZero: 1 }) !== 1)
        }
        if (ds.Positifs !== 'toujours' || stor.centre !== 0) {
          do {
            stor.num = j3pGetRandomInt(1, stor.compt - 1)
          } while (j3pPGCD(stor.num, stor.compt, { negativesAllowed: true, valueIfZero: 1 }) !== 1)
          stor.num += stor.compt * j3pGetRandomInt(stor.centre - 1, stor.centre + 1)
        }
      } while ((e.signe && stor.num < 0) || (!e.signe && stor.num > 0))
    } else {
      e.grad = 10
      stor.compt = 10
      if (ds.Positifs === 'toujours' && stor.centre === 0) {
        stor.num = j3pGetRandomInt(1, 9)
        stor.num += stor.compt * j3pGetRandomInt(0, 2)
      }
      if (e.unite) {
        stor.num = j3pGetRandomInt(1, 9)
      }
      if (ds.Positifs !== 'toujours' || stor.centre !== 0) {
        stor.num = j3pGetRandomInt(1, 9)
        stor.num += stor.compt * j3pGetRandomInt(stor.centre - 1, stor.centre + 1)
      }
      stor.centrebis = j3pArrondi(stor.centre * Math.pow(10, e.rang), 5)
      stor.numbis = j3pArrondi(stor.num * Math.pow(10, e.rang), 5)
    }
    return e
  }

  function poseQuestion () {
    let k, mu, yy, ko
    if (stor.Lexo.nat === 'frac') {
      stor.laf = '\\frac{' + stor.num + '}{' + stor.compt + '}'
      if (ds.Positifs === 'toujours' && stor.centre === 0) {
        mu = stor.grad / stor.compt
        k = stor.num * mu
        // stor.mtgAppLecteur.setVisible(stor.idSvg,'#afff1',true,true)
        stor.mtgAppLecteur.setText(stor.idSvg, '#afff1', 'A')
        stor.mtgAppLecteur.setText(stor.idSvg, '#afff2', '$0$')
        stor.mtgAppLecteur.setText(stor.idSvg, '#afff3', '$1$')
        stor.mtgAppLecteur.setText(stor.idSvg, '#afff4', '$2$')
      } else {
        mu = stor.grad / stor.compt
        k = stor.num * mu - stor.centre * stor.grad + stor.grad - 1
        yy = stor.mtgAppLecteur.getPointPosition(stor.idSvg, '#pt' + (k + 1))
        // stor.mtgAppLecteur.setVisible(stor.idSvg,'#afff1',true,true)
        stor.mtgAppLecteur.setText(stor.idSvg, '#afff1', 'A')
        stor.mtgAppLecteur.setText(stor.idSvg, '#afff2', '$' + stor.centre + '$')
        stor.mtgAppLecteur.setText(stor.idSvg, '#afff3', '$' + (stor.centre + 1) + '$')
      }
      stor.restric = '0123456789'
    } else {
      stor.laf = ecrisBienMathquill(stor.numbis)
      if (ds.Positifs === 'toujours' && stor.centre === 0) {
        mu = stor.grad / stor.compt
        k = stor.num * mu
        // stor.mtgAppLecteur.setVisible(stor.idSvg,'#afff1',true,true)
        stor.mtgAppLecteur.setText(stor.idSvg, '#afff1', 'A')
        stor.mtgAppLecteur.setText(stor.idSvg, '#afff2', '$0$')
        ko = ecrisBienMathquill(j3pArrondi(Math.pow(10, stor.Lexo.rang + 1), 5))
        stor.mtgAppLecteur.setText(stor.idSvg, '#afff3', '$' + ko + '$')
        ko = ecrisBienMathquill(j3pArrondi(2 * Math.pow(10, stor.Lexo.rang + 1), 5))
        stor.mtgAppLecteur.setText(stor.idSvg, '#afff4', '$' + ko + '$')
      } else {
        mu = stor.grad / stor.compt
        k = stor.num * mu - stor.centre * stor.grad + stor.grad - 1
        yy = stor.mtgAppLecteur.getPointPosition(stor.idSvg, '#pt' + (k + 1))
        // stor.mtgAppLecteur.setVisible(stor.idSvg,'#afff1',true,true)
        stor.mtgAppLecteur.setText(stor.idSvg, '#afff1', 'A')
        ko = ecrisBienMathquill(j3pArrondi(stor.centre * Math.pow(10, stor.Lexo.rang + 1), 5))
        stor.mtgAppLecteur.setText(stor.idSvg, '#afff2', '$' + ko + '$')
        ko = ecrisBienMathquill(j3pArrondi((stor.centre + 1) * Math.pow(10, stor.Lexo.rang + 1), 5))
        stor.mtgAppLecteur.setText(stor.idSvg, '#afff3', '$' + ko + '$')
      }
      stor.restric = '0123456789., '
    }

    if (ds.Positifs !== 'toujours') stor.restric += '-'

    stor.ptrep = '#pt' + (k + 1)
    if (stor.Lexo.question === 'lire') {
      stor.mtgAppLecteur.setPointPosition(stor.idSvg, '#ptdep', -100, -100, true)
      j3pAffiche(stor.lesdiv.consigne, null, '<b>Donne l’abscisse du point $A$</b>.')
      const tty = addDefaultTable(stor.lesdiv.travail, 1, 2)
      j3pAffiche(tty[0][0], null, '$x_{A} = $')
      if (stor.Lexo.nat === 'frac') {
        k = afficheFrac(tty[0][1])
        stor.repnum = new ZoneStyleMathquill1(k[0], { restric: stor.restric, limite: 8, limitenb: 1, enter: me.sectionCourante.bind(me) })
        stor.repden = new ZoneStyleMathquill1(k[2], { restric: stor.restric.replace('-', ''), limite: 8, limitenb: 1, enter: me.sectionCourante.bind(me) })
      } else {
        stor.repnum = new ZoneStyleMathquill1(tty[0][1], { restric: stor.restric, limite: 8, limitenb: 1, enter: me.sectionCourante.bind(me) })
        stor.repden = { corrige: function () { console.error('bug corrige') } }
      }
      if (ds.Positifs === 'toujours' && stor.centre === 0) {
        k = stor.num * mu
        stor.mtgAppLecteur.setVisible(stor.idSvg, '#pt' + (k + 1), true, true)
        stor.mtgAppLecteur.setPointPosition(stor.idSvg, '#ppp1', 50 + k * stor.dg, 50, true)
        stor.mtgAppLecteur.setVisible(stor.idSvg, '#pt' + (k + 1), true, true)
        stor.mtgAppLecteur.setColor(stor.idSvg, '#afff1', 30, 30, 200, true)
        stor.mtgAppLecteur.updateFigure(stor.idSvg)
      } else {
        k = stor.num * mu - stor.centre * stor.grad + stor.grad - 1
        stor.mtgAppLecteur.setVisible(stor.idSvg, '#pt' + (k + 1), true, true)
        yy = stor.mtgAppLecteur.getPointPosition(stor.idSvg, '#pt' + (k + 1))
        stor.mtgAppLecteur.setPointPosition(stor.idSvg, '#ppp1', yy.x, 50, true)
        stor.mtgAppLecteur.setColor(stor.idSvg, '#afff1', 30, 30, 200, true)
        stor.mtgAppLecteur.updateFigure(stor.idSvg)
      }
      stor.lesdiv.travail.classList.add('travail')
      stor.lesdiv.conteneur2.classList.add('enonce')
    } else {
      stor.xrep = stor.mtgAppLecteur.getPointPosition(stor.idSvg, stor.ptrep).x
      stor.mu = ' droite '
      if (ds.Positifs === 'toujours') stor.mu = ' demi-droite '
      j3pAffiche(stor.lesdiv.consigne, null, '<b>Place le point A sur la ' + stor.mu + ' pour que son abscisse soit $' + stor.laf + '$</b>')
      stor.ptdeb = stor.mtgAppLecteur.getPointPosition(stor.idSvg, '#ptdep')
      stor.mtgAppLecteur.updateFigure(stor.idSvg)
      stor.lesdiv.conteneur2.classList.add('travail')
    }

    stor.lesdiv.consigne.classList.add('enonce')
    stor.lesdiv.explications.classList.remove('explique')
    stor.lesdiv.solution.classList.remove('correction')
  }

  function creeLesDiv () {
    stor.lesdiv = {}
    stor.lesdiv.conteneur = j3pAddElt(me.zonesElts.MG, 'div', {
      style: me.styles.etendre('toutpetit.enonce', { padding: '10px' })
    })
    stor.lesdiv.consignet = j3pAddElt(stor.lesdiv.conteneur, 'div')
    const tt = addDefaultTable(stor.lesdiv.consignet, 1, 3)
    tt[0][1].style.width = '40px'
    stor.lesdiv.consigne = tt[0][0]
    stor.lesdiv.coboot = tt[0][2]
    stor.lesdiv.conteneur2 = addDefaultTable(stor.lesdiv.conteneur, 1, 1)[0][0]
    stor.lesdiv.zonefig = j3pAddElt(stor.lesdiv.conteneur2, 'div')
    stor.lesdiv.travail1 = addDefaultTable(stor.lesdiv.conteneur, 1, 1)[0][0]
    const tt2 = addDefaultTable(stor.lesdiv.travail1, 1, 3)
    tt2[0][1].style.width = '40px'
    stor.lesdiv.travail = tt2[0][0]
    stor.lesdiv.correction = tt2[0][2]
    stor.lesdiv.zonedrep = tt2[0][1]
    stor.lesdiv.explications = j3pAddElt(stor.lesdiv.conteneur, 'div')
    stor.lesdiv.solution = j3pAddElt(stor.lesdiv.conteneur, 'div')
    stor.lesdiv.explications.style.color = me.styles.cfaux
    stor.lesdiv.solution.style.color = me.styles.toutpetit.correction.color
    me.zonesElts.MG.classList.add('fond')
  }

  function makefig () {
    let i
    const txtFigure = 'TWF0aEdyYXBoSmF2YTEuMAAAABM+TMzNAAJmcv###wEA#wEAAAAAAAAAAANLAAAChwAAAQEAAAAAAAAAAQAAAT######AAAAAQAKQ0NhbGNDb25zdAD#####AAJwaQAWMy4xNDE1OTI2NTM1ODk3OTMyMzg0Nv####8AAAABAApDQ29uc3RhbnRlQAkh+1RELRj#####AAAAAQAKQ1BvaW50QmFzZQD#####AQAAAAAOAAFVAMAkAAAAAAAAQBAAAAAAAAAAAAUAAEAwMm6XjU#fQDAybpeNT9######AAAAAQAUQ0Ryb2l0ZURpcmVjdGlvbkZpeGUA#####wEAAAAAEAAAAQAAAAEAAAABAT#wAAAAAAAA#####wAAAAEAD0NQb2ludExpZURyb2l0ZQD#####AQAAAAAOAAFWAMAAAAAAAAAAQBAAAAAAAAAAAAUAAUBAMm6XjU#fAAAAAv####8AAAABAAhDU2VnbWVudAD#####AQAAAAAQAAABAAAAAQAAAAEAAAAD#####wAAAAEAB0NNaWxpZXUA#####wEAAAAAEAAAAAAAAAAAAAAAQAgAAAAAAAAAAAUAAAAAAQAAAAP#####AAAAAgAMQ0NvbW1lbnRhaXJlAP####8BAAAAAAAAAAAAAAAAQBgAAAAAAAAAAAAAAAUMAAAAAAABAAAAAAAAAAEAAAAAAAAAAAABMf####8AAAABAAlDTG9uZ3VldXIA#####wAAAAEAAAADAAAAAgD#####AQAAAAAQAAAAAAAAAAAAAABACAAAAAAAAAAGcG9pbnQwBQABQDgAAAAAAABAYnwo9cKPXAAAAAMA#####wEAAAABEAAAAQAGZHJvaXRlAAMAAAAIAT#wAAAAAAAAAAAABAD#####AQAAAAAQAAAAAAAAAAAAAABACAAAAAAAAAAABQABQHwwAAAAAAAAAAAJ#####wAAAAEADUNEZW1pRHJvaXRlT0EA#####wEAAAAADQAAAQAKZGVtaWRyb2l0ZQADAAAACAAAAAoAAAACAP####8BAAAAABAAAAAAAAAAAAAAAEAIAAAAAAAAAAZwb2ludDEFAAFARoAAAAAAAUBifCj1wo9c#####wAAAAEAFkNEcm9pdGVQZXJwZW5kaWN1bGFpcmUA#####wEAAAAAEAAAAQAAAAEAAAAIAAAACf####8AAAABAAdDQ2FsY3VsAP####8ABHJheTEAATEAAAABP#AAAAAAAAAAAAALAP####8ABHJheTIAATEAAAABP#AAAAAAAAAAAAALAP####8ABHJheTMAATEAAAABP#AAAAAAAAAAAAALAP####8ABHJheTQAATEAAAABP#AAAAAAAAAAAAALAP####8ABHJheTUAATEAAAABP#AAAAAAAAAAAAALAP####8ABHJheTYAATEAAAABP#AAAAAAAAAAAAALAP####8ABHJheTcAATEAAAABP#AAAAAAAAAAAAALAP####8ABHJheTgAATEAAAABP#AAAAAAAAAAAAALAP####8ABHJheTkAATEAAAABP#AAAAAAAAAAAAALAP####8ABXJheTEwAAExAAAAAT#wAAAAAAAAAAAACwD#####AAVyYXkxMQABMQAAAAE#8AAAAAAAAAAAAAsA#####wAFcmF5MTIAATEAAAABP#AAAAAAAAAAAAALAP####8ABXJheTEzAAExAAAAAT#wAAAAAAAAAAAACwD#####AAVyYXkxNAABMQAAAAE#8AAAAAAAAAAAAAsA#####wAFcmF5MTUAATEAAAABP#AAAAAAAAAAAAALAP####8ABXJheTE2AAExAAAAAT#wAAAAAAAAAAAACwD#####AAVyYXkxNwABMQAAAAE#8AAAAAAAAAAAAAsA#####wAFcmF5MTgAATEAAAABP#AAAAAAAAAAAAALAP####8ABXJheTE5AAExAAAAAT#wAAAAAAAAAAAACwD#####AAVyYXkyMAABMQAAAAE#8AAAAAAAAAAAAAsA#####wAFcmF5MjEAATEAAAABP#AAAAAAAAAAAAALAP####8ABXJheTIyAAExAAAAAT#wAAAAAAAAAAAACwD#####AAVyYXkyMwABMQAAAAE#8AAAAAAAAAAAAAsA#####wAFcmF5MjQAATEAAAABP#AAAAAAAAAAAAALAP####8ABXJheTI1AAExAAAAAT#wAAAAAAAAAAAACwD#####AAVyYXkyNgABMQAAAAE#8AAAAAAAAAAAAAsA#####wAFcmF5MjcAATEAAAABP#AAAAAAAAAAAAALAP####8ABXJheTI4AAExAAAAAT#wAAAAAAAAAAAACwD#####AAVyYXkyOQABMQAAAAE#8AAAAAAAAAAAAAsA#####wAFcmF5MzAAATEAAAABP#AAAAAAAAD#####AAAAAQAIQ1ZlY3RldXIA#####wEAAAAAEAAAAQAAAAEAAAAIAAAADAD#####AAAAAQANQ1RyYW5zUGFyVmVjdAD#####AAAALP####8AAAABAAtDUG9pbnRJbWFnZQD#####AQAA#wAQAAAAAAAAAAAAAABACAAAAAAAAAADcHQzCQAAAAAMAAAALQAAAA4A#####wEAAP8AEAAAAAAAAAAAAAAAQAgAAAAAAAAAA3B0NAkAAAAALgAAAC0AAAAOAP####8BAAD#ABAAAAAAAAAAAAAAAEAIAAAAAAAAAANwdDUJAAAAAC8AAAAtAAAADgD#####AQAA#wAQAAAAAAAAAAAAAABACAAAAAAAAAADcHQ2CQAAAAAwAAAALQAAAA4A#####wEAAP8AEAAAAAAAAAAAAAAAQAgAAAAAAAAAA3B0NwkAAAAAMQAAAC0AAAAOAP####8BAAD#ABAAAAAAAAAAAAAAAEAIAAAAAAAAAANwdDgJAAAAADIAAAAtAAAADgD#####AQAA#wAQAAAAAAAAAAAAAABACAAAAAAAAAADcHQ5CQAAAAAzAAAALQAAAA4A#####wEAAP8AEAAAAAAAAAAAAAAAQAgAAAAAAAAABHB0MTAJAAAAADQAAAAtAAAADgD#####AQAA#wAQAAAAAAAAAAAAAABACAAAAAAAAAAEcHQxMQkAAAAANQAAAC0AAAAOAP####8BAAD#ABAAAAAAAAAAAAAAAEAIAAAAAAAAAARwdDEyCQAAAAA2AAAALQAAAA4A#####wEAAP8AEAAAAAAAAAAAAAAAQAgAAAAAAAAABHB0MTMJAAAAADcAAAAtAAAADgD#####AQAA#wAQAAAAAAAAAAAAAABACAAAAAAAAAAEcHQxNAkAAAAAOAAAAC0AAAAOAP####8BAAD#ABAAAAAAAAAAAAAAAEAIAAAAAAAAAARwdDE1CQAAAAA5AAAALQAAAA4A#####wEAAP8AEAAAAAAAAAAAAAAAQAgAAAAAAAAABHB0MTYJAAAAADoAAAAtAAAADgD#####AQAA#wAQAAAAAAAAAAAAAABACAAAAAAAAAAEcHQxNwkAAAAAOwAAAC0AAAAOAP####8BAAD#ABAAAAAAAAAAAAAAAEAIAAAAAAAAAARwdDE4CQAAAAA8AAAALQAAAA4A#####wEAAP8AEAAAAAAAAAAAAAAAQAgAAAAAAAAABHB0MTkJAAAAAD0AAAAtAAAADgD#####AQAA#wAQAAAAAAAAAAAAAABACAAAAAAAAAAEcHQyMAkAAAAAPgAAAC0AAAAOAP####8BAAD#ABAAAAAAAAAAAAAAAEAIAAAAAAAAAARwdDIxCQAAAAA#AAAALQAAAA4A#####wEAAP8AEAAAAAAAAAAAAAAAQAgAAAAAAAAABHB0MjIJAAAAAEAAAAAtAAAADgD#####AQAA#wAQAAAAAAAAAAAAAABACAAAAAAAAAAEcHQyMwkAAAAAQQAAAC0AAAAOAP####8BAAD#ABAAAAAAAAAAAAAAAEAIAAAAAAAAAARwdDI0CQAAAABCAAAALQAAAA4A#####wEAAP8AEAAAAAAAAAAAAAAAQAgAAAAAAAAABHB0MjUJAAAAAEMAAAAtAAAADgD#####AQAA#wAQAAAAAAAAAAAAAABACAAAAAAAAAAEcHQyNgkAAAAARAAAAC0AAAAOAP####8BAAD#ABAAAAAAAAAAAAAAAEAIAAAAAAAAAARwdDI3CQAAAABFAAAALQAAAA4A#####wEAAP8AEAAAAAAAAAAAAAAAQAgAAAAAAAAABHB0MjgJAAAAAEYAAAAtAAAADgD#####AQAA#wAQAAAAAAAAAAAAAABACAAAAAAAAAAEcHQyOQkAAAAARwAAAC0AAAAOAP####8BAAD#ABAAAAAAAAAAAAAAAEAIAAAAAAAAAARwdDMwCQAAAABIAAAALf####8AAAABAAxDRHJvaXRlSW1hZ2UA#####wEAAAAAEAAAAQAAAAEAAAANAAAALQAAAA8A#####wEAAAAAEAAAAQAAAAEAAABKAAAALQAAAA8A#####wEAAAAAEAAAAQAAAAEAAABLAAAALQAAAA8A#####wEAAAAAEAAAAQAAAAEAAABMAAAALQAAAA8A#####wEAAAAAEAAAAQAAAAEAAABNAAAALQAAAA8A#####wEAAAAAEAAAAQAAAAEAAABOAAAALQAAAA8A#####wEAAAAAEAAAAQAAAAEAAABPAAAALQAAAA8A#####wEAAAAAEAAAAQAAAAEAAABQAAAALQAAAA8A#####wEAAAAAEAAAAQAAAAEAAABRAAAALQAAAA8A#####wEAAAAAEAAAAQAAAAEAAABSAAAALQAAAA8A#####wEAAAAAEAAAAQAAAAEAAABTAAAALQAAAA8A#####wEAAAAAEAAAAQAAAAEAAABUAAAALQAAAA8A#####wEAAAAAEAAAAQAAAAEAAABVAAAALQAAAA8A#####wEAAAAAEAAAAQAAAAEAAABWAAAALQAAAA8A#####wEAAAAAEAAAAQAAAAEAAABXAAAALQAAAA8A#####wEAAAAAEAAAAQAAAAEAAABYAAAALQAAAA8A#####wEAAAAAEAAAAQAAAAEAAABZAAAALQAAAA8A#####wEAAAAAEAAAAQAAAAEAAABaAAAALQAAAA8A#####wEAAAAAEAAAAQAAAAEAAABbAAAALQAAAA8A#####wEAAAAAEAAAAQAAAAEAAABcAAAALQAAAA8A#####wEAAAAAEAAAAQAAAAEAAABdAAAALQAAAA8A#####wEAAAAAEAAAAQAAAAEAAABeAAAALQAAAA8A#####wEAAAAAEAAAAQAAAAEAAABfAAAALQAAAA8A#####wEAAAAAEAAAAQAAAAEAAABgAAAALQAAAA8A#####wEAAAAAEAAAAQAAAAEAAABhAAAALQAAAA8A#####wEAAAAAEAAAAQAAAAEAAABiAAAALQAAAA8A#####wEAAAAAEAAAAQAAAAEAAABjAAAALQAAAA8A#####wEAAAAAEAAAAQAAAAEAAABkAAAALQAAAA8A#####wEAAAAAEAAAAQAAAAEAAABlAAAALf####8AAAACAAlDQ2VyY2xlT1IA#####wEAAAAAAAABAAAACP####8AAAABAA9DUmVzdWx0YXRWYWxldXIAAAAOAAAAABAA#####wEAAAAAAAABAAAADAAAABEAAAAPAAAAABAA#####wEAAAAAAAABAAAALgAAABEAAAAQAAAAABAA#####wEAAAAAAAABAAAALwAAABEAAAARAAAAABAA#####wEAAAAAAAABAAAAMAAAABEAAAASAAAAABAA#####wEAAAAAAAABAAAAMQAAABEAAAATAAAAABAA#####wEAAAAAAAABAAAAMgAAABEAAAAUAAAAABAA#####wEAAAAAAAABAAAAMwAAABEAAAAVAAAAABAA#####wEAAAAAAAABAAAANAAAABEAAAAWAAAAABAA#####wEAAAAAAAABAAAANQAAABEAAAAXAAAAABAA#####wEAAAAAAAABAAAANgAAABEAAAAYAAAAABAA#####wEAAAAAAAABAAAANwAAABEAAAAZAAAAABAA#####wEAAAAAAAABAAAAOAAAABEAAAAaAAAAABAA#####wEAAAAAAAABAAAAOQAAABEAAAAbAAAAABAA#####wEAAAAAAAABAAAAOgAAABEAAAAcAAAAABAA#####wEAAAAAAAABAAAAOwAAABEAAAAdAAAAABAA#####wEAAAAAAAABAAAAPAAAABEAAAAeAAAAABAA#####wEAAAAAAAABAAAAPQAAABEAAAAfAAAAABAA#####wEAAAAAAAABAAAAPgAAABEAAAAgAAAAABAA#####wEAAAAAAAABAAAAPwAAABEAAAAhAAAAABAA#####wEAAAAAAAABAAAAQAAAABEAAAAiAAAAABAA#####wEAAAAAAAABAAAAQQAAABEAAAAjAAAAABAA#####wEAAAAAAAABAAAAQgAAABEAAAAkAAAAABAA#####wEAAAAAAAABAAAAQwAAABEAAAAlAAAAABAA#####wEAAAAAAAABAAAARAAAABEAAAAmAAAAABAA#####wEAAAAAAAABAAAARQAAABEAAAAnAAAAABAA#####wEAAAAAAAABAAAARgAAABEAAAAoAAAAABAA#####wEAAAAAAAABAAAARwAAABEAAAApAAAAABAA#####wEAAAAAAAABAAAASAAAABEAAAAqAAAAABAA#####wEAAAAAAAABAAAASQAAABEAAAArAP####8AAAABABBDSW50RHJvaXRlQ2VyY2xlAP####8AAABmAAAAhP####8AAAABABBDUG9pbnRMaWVCaXBvaW50AP####8BAAAAABAAAAAAAAAAAAAAAEAIAAAAAAAAAAAFAAEAAACFAAAAEwD#####AQAAAAAQAAAAAAAAAAAAAABACAAAAAAAAAAABQACAAAAhQAAABIA#####wAAAGUAAACDAAAAEwD#####AQAAAAAQAAAAAAAAAAAAAABACAAAAAAAAAAABQABAAAAiAAAABMA#####wEAAAAAEAAAAAAAAAAAAAAAQAgAAAAAAAAAAAUAAgAAAIgAAAASAP####8AAABkAAAAggAAABMA#####wEAAAAAEAAAAAAAAAAAAAAAQAgAAAAAAAAAAAUAAQAAAIsAAAATAP####8BAAAAABAAAAAAAAAAAAAAAEAIAAAAAAAAAAAFAAIAAACLAAAAEgD#####AAAAYwAAAIEAAAATAP####8BAAAAABAAAAAAAAAAAAAAAEAIAAAAAAAAAAAFAAEAAACOAAAAEwD#####AQAAAAAQAAAAAAAAAAAAAABACAAAAAAAAAAABQACAAAAjgAAABIA#####wAAAGIAAACAAAAAEwD#####AQAAAAAQAAAAAAAAAAAAAABACAAAAAAAAAAABQABAAAAkQAAABMA#####wEAAAAAEAAAAAAAAAAAAAAAQAgAAAAAAAAAAAUAAgAAAJEAAAASAP####8AAABhAAAAfwAAABMA#####wEAAAAAEAAAAAAAAAAAAAAAQAgAAAAAAAAAAAUAAQAAAJQAAAATAP####8BAAAAABAAAAAAAAAAAAAAAEAIAAAAAAAAAAAFAAIAAACUAAAAEgD#####AAAAYAAAAH4AAAATAP####8BAAAAABAAAAAAAAAAAAAAAEAIAAAAAAAAAAAFAAEAAACXAAAAEwD#####AQAAAAAQAAAAAAAAAAAAAABACAAAAAAAAAAABQACAAAAlwAAABIA#####wAAAF8AAAB9AAAAEwD#####AQAAAAAQAAAAAAAAAAAAAABACAAAAAAAAAAABQABAAAAmgAAABMA#####wEAAAAAEAAAAAAAAAAAAAAAQAgAAAAAAAAAAAUAAgAAAJoAAAASAP####8AAABeAAAAfAAAABMA#####wEAAAAAEAAAAAAAAAAAAAAAQAgAAAAAAAAAAAUAAQAAAJ0AAAATAP####8BAAAAABAAAAAAAAAAAAAAAEAIAAAAAAAAAAAFAAIAAACdAAAAEgD#####AAAAXQAAAHsAAAATAP####8BAAAAABAAAAAAAAAAAAAAAEAIAAAAAAAAAAAFAAEAAACgAAAAEwD#####AQAAAAAQAAAAAAAAAAAAAABACAAAAAAAAAAABQACAAAAoAAAABIA#####wAAAFwAAAB6AAAAEwD#####AQAAAAAQAAAAAAAAAAAAAABACAAAAAAAAAAABQABAAAAowAAABMA#####wEAAAAAEAAAAAAAAAAAAAAAQAgAAAAAAAAAAAUAAgAAAKMAAAASAP####8AAABbAAAAeQAAABMA#####wEAAAAAEAAAAAAAAAAAAAAAQAgAAAAAAAAAAAUAAQAAAKYAAAATAP####8BAAAAABAAAAAAAAAAAAAAAEAIAAAAAAAAAAAFAAIAAACmAAAAEgD#####AAAAWgAAAHgAAAATAP####8BAAAAABAAAAAAAAAAAAAAAEAIAAAAAAAAAAAFAAEAAACpAAAAEwD#####AQAAAAAQAAAAAAAAAAAAAABACAAAAAAAAAAABQACAAAAqQAAABIA#####wAAAFkAAAB3AAAAEwD#####AQAAAAAQAAAAAAAAAAAAAABACAAAAAAAAAAABQABAAAArAAAABMA#####wEAAAAAEAAAAAAAAAAAAAAAQAgAAAAAAAAAAAUAAgAAAKwAAAASAP####8AAABYAAAAdgAAABMA#####wEAAAAAEAAAAAAAAAAAAAAAQAgAAAAAAAAAAAUAAQAAAK8AAAATAP####8BAAAAABAAAAAAAAAAAAAAAEAIAAAAAAAAAAAFAAIAAACvAAAAEgD#####AAAAVwAAAHUAAAATAP####8BAAAAABAAAAAAAAAAAAAAAEAIAAAAAAAAAAAFAAEAAACyAAAAEwD#####AQAAAAAQAAAAAAAAAAAAAABACAAAAAAAAAAABQACAAAAsgAAABIA#####wAAAFYAAAB0AAAAEwD#####AQAAAAAQAAAAAAAAAAAAAABACAAAAAAAAAAABQABAAAAtQAAABMA#####wEAAAAAEAAAAAAAAAAAAAAAQAgAAAAAAAAAAAUAAgAAALUAAAASAP####8AAABVAAAAcwAAABMA#####wEAAAAAEAAAAAAAAAAAAAAAQAgAAAAAAAAAAAUAAQAAALgAAAATAP####8BAAAAABAAAAAAAAAAAAAAAEAIAAAAAAAAAAAFAAIAAAC4AAAAEgD#####AAAAVAAAAHIAAAATAP####8BAAAAABAAAAAAAAAAAAAAAEAIAAAAAAAAAAAFAAEAAAC7AAAAEwD#####AQAAAAAQAAAAAAAAAAAAAABACAAAAAAAAAAABQACAAAAuwAAABIA#####wAAAFMAAABxAAAAEwD#####AQAAAAAQAAAAAAAAAAAAAABACAAAAAAAAAAABQABAAAAvgAAABMA#####wEAAAAAEAAAAAAAAAAAAAAAQAgAAAAAAAAAAAUAAgAAAL4AAAASAP####8AAABSAAAAcAAAABMA#####wEAAAAAEAAAAAAAAAAAAAAAQAgAAAAAAAAAAAUAAQAAAMEAAAATAP####8BAAAAABAAAAAAAAAAAAAAAEAIAAAAAAAAAAAFAAIAAADBAAAAEgD#####AAAAUQAAAG8AAAATAP####8BAAAAABAAAAAAAAAAAAAAAEAIAAAAAAAAAAAFAAEAAADEAAAAEwD#####AQAAAAAQAAAAAAAAAAAAAABACAAAAAAAAAAABQACAAAAxAAAABIA#####wAAAFAAAABuAAAAEwD#####AQAAAAAQAAAAAAAAAAAAAABACAAAAAAAAAAABQABAAAAxwAAABMA#####wEAAAAAEAAAAAAAAAAAAAAAQAgAAAAAAAAAAAUAAgAAAMcAAAASAP####8AAABPAAAAbQAAABMA#####wEAAAAAEAAAAAAAAAAAAAAAQAgAAAAAAAAAAAUAAQAAAMoAAAATAP####8BAAAAABAAAAAAAAAAAAAAAEAIAAAAAAAAAAAFAAIAAADKAAAAEgD#####AAAATgAAAGwAAAATAP####8BAAAAABAAAAAAAAAAAAAAAEAIAAAAAAAAAAAFAAEAAADNAAAAEwD#####AQAAAAAQAAAAAAAAAAAAAABACAAAAAAAAAAABQACAAAAzQAAABIA#####wAAAE0AAABrAAAAEwD#####AQAAAAAQAAAAAAAAAAAAAABACAAAAAAAAAAABQABAAAA0AAAABMA#####wEAAAAAEAAAAAAAAAAAAAAAQAgAAAAAAAAAAAUAAgAAANAAAAASAP####8AAABMAAAAagAAABMA#####wEAAAAAEAAAAAAAAAAAAAAAQAgAAAAAAAAAAAUAAQAAANMAAAATAP####8BAAAAABAAAAAAAAAAAAAAAEAIAAAAAAAAAAAFAAIAAADTAAAAEgD#####AAAASwAAAGkAAAATAP####8BAAAAABAAAAAAAAAAAAAAAEAIAAAAAAAAAAAFAAEAAADWAAAAEwD#####AQAAAAAQAAAAAAAAAAAAAABACAAAAAAAAAAABQACAAAA1gAAABIA#####wAAAEoAAABoAAAAEwD#####AQAAAAAQAAAAAAAAAAAAAABACAAAAAAAAAAABQABAAAA2QAAABMA#####wEAAAAAEAAAAAAAAAAAAAAAQAgAAAAAAAAAAAUAAgAAANkAAAASAP####8AAAANAAAAZwAAABMA#####wEAAAAAEAAAAAAAAAAAAAAAQAgAAAAAAAAAAAUAAgAAANwAAAATAP####8BAAAAABAAAAAAAAAAAAAAAEAIAAAAAAAAAAAFAAEAAADcAAAABQD#####AAAAAAAQAAABAARzZWcwAAEAAADdAAAA3gAAAAUA#####wAAAAAAEAAAAQAEc2VnMQABAAAA2wAAANoAAAAFAP####8AAAAAABAAAAEABHNlZzIAAQAAANgAAADXAAAABQD#####AAAAAAAQAAABAARzZWczAAEAAADVAAAA1AAAAAUA#####wAAAAAAEAAAAQAEc2VnNAABAAAA0gAAANEAAAAFAP####8AAAAAABAAAAEABHNlZzUAAQAAAM8AAADOAAAABQD#####AAAAAAAQAAABAARzZWc2AAEAAADMAAAAywAAAAUA#####wAAAAAAEAAAAQAEc2VnNwABAAAAyQAAAMgAAAAFAP####8AAAAAABAAAAEABHNlZzgAAQAAAMYAAADFAAAABQD#####AAAAAAAQAAABAARzZWc5AAEAAADDAAAAwgAAAAUA#####wAAAAAAEAAAAQAFc2VnMTAAAQAAAMAAAAC#AAAABQD#####AAAAAAAQAAABAAVzZWcxMQABAAAAvQAAALwAAAAFAP####8AAAAAABAAAAEABXNlZzEyAAEAAAC6AAAAuQAAAAUA#####wAAAAAAEAAAAQAFc2VnMTMAAQAAALcAAAC2AAAABQD#####AAAAAAAQAAABAAVzZWcxNAABAAAAtAAAALMAAAAFAP####8AAAAAABAAAAEABXNlZzE1AAEAAACxAAAAsAAAAAUA#####wAAAAAAEAAAAQAFc2VnMTYAAQAAAK4AAACtAAAABQD#####AAAAAAAQAAABAAVzZWcxNwABAAAAqwAAAKoAAAAFAP####8AAAAAABAAAAEABXNlZzE4AAEAAACoAAAApwAAAAUA#####wAAAAAAEAAAAQAFc2VnMTkAAQAAAKUAAACkAAAABQD#####AAAAAAAQAAABAAVzZWcyMAABAAAAogAAAKEAAAAFAP####8AAAAAABAAAAEABXNlZzIxAAEAAACfAAAAngAAAAUA#####wAAAAAAEAAAAQAFc2VnMjIAAQAAAJwAAACbAAAABQD#####AAAAAAAQAAABAAVzZWcyMwABAAAAmQAAAJgAAAAFAP####8AAAAAABAAAAEABXNlZzI0AAEAAACWAAAAlQAAAAUA#####wAAAAAAEAAAAQAFc2VnMjUAAQAAAJMAAACSAAAABQD#####AAAAAAAQAAABAAVzZWcyNgABAAAAkAAAAI8AAAAFAP####8AAAAAABAAAAEABXNlZzI3AAEAAACNAAAAjAAAAAUA#####wAAAAAAEAAAAQAFc2VnMjgAAQAAAIoAAACJAAAABQD#####AAAAAAAQAAABAAVzZWcyOQABAAAAhwAAAIYAAAALAP####8ABXJheTMxAAExAAAAAT#wAAAAAAAAAAAACwD#####AAVyYXkzMgABMQAAAAE#8AAAAAAAAAAAAAsA#####wAFcmF5MzMAATEAAAABP#AAAAAAAAAAAAALAP####8ABXJheTM0AAExAAAAAT#wAAAAAAAAAAAACwD#####AAVyYXkzNQABMQAAAAE#8AAAAAAAAAAAAAsA#####wAFcmF5MzYAATEAAAABP#AAAAAAAAAAAAAPAP####8BAAAAABAAAAEAAAABAAAAZgAAAC0AAAAPAP####8BAAAAABAAAAEAAAABAAABAwAAAC0AAAAPAP####8BAAAAABAAAAEAAAABAAABBAAAAC0AAAAPAP####8BAAAAABAAAAEAAAABAAABBQAAAC0AAAAPAP####8BAAAAABAAAAEAAAABAAABBgAAAC0AAAAPAP####8BAAAAABAAAAEAAAABAAABBwAAAC0AAAAOAP####8BAAD#ABAAAAAAAAAAAAAAAEAIAAAAAAAAAARwdDMxCQAAAABJAAAALQAAAA4A#####wEAAP8AEAAAAAAAAAAAAAAAQAgAAAAAAAAABHB0MzIJAAAAAQkAAAAtAAAADgD#####AQAA#wAQAAAAAAAAAAAAAABACAAAAAAAAAAEcHQzMwkAAAABCgAAAC0AAAAOAP####8BAAD#ABAAAAAAAAAAAAAAAEAIAAAAAAAAAARwdDM0CQAAAAELAAAALQAAAA4A#####wEAAP8AEAAAAAAAAAAAAAAAQAgAAAAAAAAABHB0MzUJAAAAAQwAAAAtAAAADgD#####AQAA#wAQAAAAAAAAAAAAAABACAAAAAAAAAAEcHQzNgkAAAABDQAAAC0AAAAQAP####8BAAAAAAAAAQAAAQkAAAARAAAA#QAAAAAQAP####8BAAAAAAAAAQAAAQoAAAARAAAA#gAAAAAQAP####8BAAAAAAAAAQAAAQsAAAARAAAA#wAAAAAQAP####8BAAAAAAAAAQAAAQwAAAARAAABAAAAAAAQAP####8BAAAAAAAAAQAAAQ0AAAARAAABAQAAAAAQAP####8BAAAAAAAAAQAAAQ4AAAARAAABAgAAAAASAP####8AAAEIAAABFAAAABMA#####wEAAAAAEAAAAAAAAAAAAAAAQAgAAAAAAAAAAAUAAQAAARUAAAATAP####8BAAAAABAAAAAAAAAAAAAAAEAIAAAAAAAAAAAFAAIAAAEVAAAAEgD#####AAABBwAAARMAAAATAP####8BAAAAABAAAAAAAAAAAAAAAEAIAAAAAAAAAAAFAAEAAAEYAAAAEwD#####AQAAAAAQAAAAAAAAAAAAAABACAAAAAAAAAAABQACAAABGAAAABIA#####wAAAQYAAAESAAAAEwD#####AQAAAAAQAAAAAAAAAAAAAABACAAAAAAAAAAABQABAAABGwAAABMA#####wEAAAAAEAAAAAAAAAAAAAAAQAgAAAAAAAAAAAUAAgAAARsAAAASAP####8AAAEFAAABEQAAABMA#####wEAAAAAEAAAAAAAAAAAAAAAQAgAAAAAAAAAAAUAAQAAAR4AAAATAP####8BAAAAABAAAAAAAAAAAAAAAEAIAAAAAAAAAAAFAAIAAAEeAAAAEgD#####AAABBAAAARAAAAATAP####8BAAAAABAAAAAAAAAAAAAAAEAIAAAAAAAAAAAFAAEAAAEhAAAAEwD#####AQAAAAAQAAAAAAAAAAAAAABACAAAAAAAAAAABQACAAABIQAAABIA#####wAAAQMAAAEPAAAAEwD#####AQAAAAAQAAAAAAAAAAAAAABACAAAAAAAAAAABQABAAABJAAAABMA#####wEAAAAAEAAAAAAAAAAAAAAAQAgAAAAAAAAAAAUAAgAAASQAAAAFAP####8AAAAAABAAAAEABXNlZzMxAAEAAAEmAAABJQAAAAUA#####wAAAAAAEAAAAQAFc2VnMzIAAQAAASMAAAEiAAAABQD#####AAAAAAAQAAABAAVzZWczMwABAAABIAAAAR8AAAAFAP####8AAAAAABAAAAEABXNlZzM0AAEAAAEdAAABHAAAAAUA#####wAAAAAAEAAAAQAFc2VnMzUAAQAAARoAAAEZAAAABQD#####AAAAAAAQAAABAAVzZWczNgABAAABFwAAARYAAAACAP####8BAAAAABAAAAAAAAAAAAAAAEAIAAAAAAAAAARwcHAxBQABQHAgAAAAAABAdq4UeuFHrgAAAAIA#####wEAAAAAEAAAAAAAAAAAAAAAQAgAAAAAAAAABHBwcDIFAAFAeJAAAAAAAEB2rhR64UeuAAAABwD#####AAAAAAEABWFmZmYxAAABLRQAAAAAAAEAAAABAAAAAQAAAAAAAAAAAAFhAAAABwD#####AAAAAAEABWFmZmYyAAABLhQAAAAAAAEAAAABAAAAAQAAAAAAAAAAAAFhAAAAAgD#####AAAA#wAQAAAAAAAAAAAAAABACAAAAAAAAAAACQABQHDAAAAAAABAfs4UeuFHrgAAAAIA#####wAAAP8AEAAAAAAAAAAAAAAAQAgAAAAAAAAAAAkAAUBwwAAAAAAAQH7OFHrhR64AAAAMAP####8AAAD#ABAAAAEAAAABAAABMQAAATIAAAAADQD#####AAABMwAAAA4A#####wEAAP8AEAAAAAAAAAAAAAAAQAgAAAAAAAAAA3B0MgkAAAAADAAAATQAAAAOAP####8BAAD#ABAAAAAAAAAAAAAAAEAIAAAAAAAAAANwdDEJAAAAAAgAAAE0AAAAAgD#####AH8AfwEQAAFBAAAAAAAAAAAAQAgAAAAAAAAABXB0ZGVwCQABQEIAAAAAAAFARHCj1wo9cQAAAAIA#####wEAAAAAEAAAAAAAAAAAAAAAQAgAAAAAAAAABHBwcDMJAAFAYGAAAAAAAEB2zhR64UeuAAAABwD#####AAAAAAEABWFmZmYzAAABOBQAAAAAAAEAAAABAAAAAQAAAAAAAAAAAAFhAAAAAgD#####AQAAAAAQAAAAAAAAAAAAAABACAAAAAAAAAAEcHBwNAkAAUBxgAAAAAAAQHp+FHrhR64AAAAHAP####8AAAAAAQAFYWZmZjQAAAE6FAAAAAAAAQAAAAEAAAABAAAAAAAAAAAAAWEAAAAQAP####8BAAAAAAAAAwAAATcAAAABP#AAAAAAAAAA#####wAAAAEAD0NQb2ludExpZUNlcmNsZQD#####AP###wAQAAAAAAAAAAAAAABACAAAAAAAAAAABwABP#kh+1RELRgAAAE8AAAABwD#####AH8AfwEABWFmbm9tAAABPRQAAAAAAAEAAAABAAAAAQAAAAAAAAAAAAFBAAAAB###########'
    idSvg = j3pGetNewId('mtg32')
    stor.idSvg = idSvg
    const yy = j3pCreeSVG(stor.lesdiv.zonefig, { id: stor.idSvg, width: 800, height: 180 })
    yy.style.border = 'solid black 1px'
    stor.mtgAppLecteur.removeAllDoc()
    stor.mtgAppLecteur.addDoc(stor.idSvg, txtFigure, true)
    stor.mtgAppLecteur.calculateAndDisplayAll(true)
    stor.OO = stor.mtgAppLecteur.setText(stor.idSvg, '#af1', '$\\frac{1}{2}$')
    stor.mtgAppLecteur.updateFigure(stor.idSvg)
    stor.grad = stor.Lexo.grad

    stor.mtgAppLecteur.setPointPosition(stor.idSvg, '#point0', 50, 90, true)
    for (i = 0; i < 37; i++) stor.mtgAppLecteur.giveFormula2(stor.idSvg, 'ray' + i, 0.5, false)
    if (ds.Positifs === 'toujours' && stor.centre === 0) {
      stor.mtgAppLecteur.setPointPosition(stor.idSvg, '#point1', 50 + (754 / (stor.grad * 3)), 90, true)
      stor.dg = 754 / (stor.grad * 3)
      stor.mtgAppLecteur.setVisible(stor.idSvg, '#demidroite', true, true)
      stor.mtgAppLecteur.setLineStyle(stor.idSvg, '#seg0', 0, 4, true)
      stor.mtgAppLecteur.setColor(stor.idSvg, '#seg0', 255, 0, 0, true)
      stor.mtgAppLecteur.giveFormula2(stor.idSvg, 'ray1', 1, false)
      stor.mtgAppLecteur.setPointPosition(stor.idSvg, '#ppp2', 50, 140, true)
      stor.mtgAppLecteur.setLineStyle(stor.idSvg, '#seg' + (stor.grad), 0, 4, true)
      stor.mtgAppLecteur.setColor(stor.idSvg, '#seg' + (stor.grad), 255, 0, 0, true)
      stor.mtgAppLecteur.setPointPosition(stor.idSvg, '#ppp3', 50 + stor.dg * stor.grad, 140, true)
      stor.mtgAppLecteur.giveFormula2(stor.idSvg, 'ray' + (stor.grad + 1), 1, false)
      stor.mtgAppLecteur.setLineStyle(stor.idSvg, '#seg' + (stor.grad * 2), 0, 4, true)
      stor.mtgAppLecteur.setColor(stor.idSvg, '#seg' + (stor.grad * 2), 255, 0, 0, true)
      stor.mtgAppLecteur.setPointPosition(stor.idSvg, '#ppp4', 50 + stor.dg * stor.grad * 2, 140, true)
      stor.mtgAppLecteur.giveFormula2(stor.idSvg, 'ray' + (stor.grad * 2 + 1), 1, false)
    } else {
      stor.dg = 782 / (stor.grad * 3 - 1)
      stor.mtgAppLecteur.setPointPosition(stor.idSvg, '#point1', 20 + stor.dg, 90, true)
      stor.mtgAppLecteur.setPointPosition(stor.idSvg, '#point0', 20, 90, true)
      stor.mtgAppLecteur.setVisible(stor.idSvg, '#droite', true, true)

      stor.mtgAppLecteur.setLineStyle(stor.idSvg, '#seg' + (stor.grad - 1), 0, 4, true)
      stor.mtgAppLecteur.setColor(stor.idSvg, '#seg' + (stor.grad - 1), 255, 0, 0, true)
      stor.mtgAppLecteur.giveFormula2(stor.idSvg, 'ray' + stor.grad, 1, false)
      stor.mtgAppLecteur.setPointPosition(stor.idSvg, '#ppp2', 20 + stor.dg * (stor.grad - 1), 140, true)

      stor.mtgAppLecteur.setLineStyle(stor.idSvg, '#seg' + (2 * stor.grad - 1), 0, 4, true)
      stor.mtgAppLecteur.setColor(stor.idSvg, '#seg' + (2 * stor.grad - 1), 255, 0, 0, true)
      stor.mtgAppLecteur.giveFormula2(stor.idSvg, 'ray' + 2 * stor.grad, 1, false)
      stor.mtgAppLecteur.setPointPosition(stor.idSvg, '#ppp3', 20 + stor.dg * (2 * stor.grad - 1), 140, true)
    }
    stor.mtgAppLecteur.updateFigure(stor.idSvg)
  }

  function yaReponse () {
    if (stor.Lexo.question === 'lire') {
      if (stor.repnum.reponse() === '') {
        stor.repnum.focus()
        return false
      }
      if (stor.Lexo.nat === 'frac') {
        if (stor.repden.reponse() === '') {
          stor.repden.focus()
          return false
        }
      }
      return true
    }
    // placer
    const pt = stor.mtgAppLecteur.getPointPosition(stor.idSvg, '#ptdep')
    if (pt.x === stor.ptdeb.x && pt.y === stor.ptdeb.y) {
      affModale('Clique sur le point A pour le déplacer !', 'Déplacement de A')
      return false
    }
    return true
  }

  function affModale (texte, titre) {
    if (titre === undefined) titre = 'Erreur'
    const yy = j3pModale({ titre, contenu: '' })
    j3pAffiche(yy, null, texte)
    yy.style.color = me.styles.toutpetit.correction
  }

  function isRepOk () {
    stor.errSimpl = false
    stor.errDiv = false
    stor.errNum = false
    stor.errUnit = false
    stor.errDte = false
    stor.errPLace = false
    stor.errEcrit = false
    stor.errPrecis = false
    stor.errEcritList = ''
    let repn, repd, kailo
    // test existence tout elem
    if (stor.Lexo.question === 'lire') {
      if (stor.Lexo.nat === 'frac') {
        repn = stor.repnum.reponse()
        repd = stor.repden.reponse()
      } else {
        repn = stor.repnum.reponse()
        repd = 10
      }
      if (repn.indexOf('-') !== repn.lastIndexOf('-')) {
        stor.errEcrit = true
        stor.repnum.corrige(false)
        return false
      }
      if (repn.indexOf('-') !== 0 && repn.indexOf('-') !== -1) {
        stor.errEcrit = true
        stor.repnum.corrige(false)
        return false
      }
      if (stor.Lexo.nat !== 'frac') {
        kailo = verifNombreBienEcrit(repn, ds.Espace, true)
        if (!kailo.good) {
          stor.errEcrit = true
          stor.errEcritList = kailo.remede
          stor.repnum.corrige(false)
          return false
        } else {
          repn = j3pArrondi(kailo.nb / Math.pow(10, stor.Lexo.rang), 5)
        }
      } else {
        repd = parseInt(repd)
      }
      if (egalfrac(repn, repd, stor.num, stor.compt)) {
        if (j3pPGCD(repn, repd, { negativesAllowed: true, valueIfZero: 1 }) !== 1 && ds.Simplifie) {
          stor.errSimpl = true
          stor.repnum.corrige(false)
          stor.repden.corrige(false)
          return false
        }
        return true
      }

      if (stor.Lexo.nat === 'frac') {
        if (repd !== stor.grad && repd !== stor.compt) {
          stor.errDiv = true
          stor.repden.corrige(false)
          return false
        }
      }

      stor.repnum.corrige(false)
      if (stor.num % stor.grad === repn % stor.grad) {
        stor.errUnit = true
        return false
      }
      stor.errNum = true
      return false
    } else {
      // test appartient à demi-droite
      const prep = stor.mtgAppLecteur.getPointPosition(stor.idSvg, '#ptdep')
      if (prep.y > 93 || prep.y < 83) {
        stor.errDte = true
        return false
      }

      if (prep.x > stor.xrep + 3 || prep.x < stor.xrep - 3) {
        if (prep.x < stor.xrep + 8 && prep.x > stor.xrep - 8) {
          stor.errPrecis = true
        }
        stor.errPLace = true
        return false
      }
    }

    stor.lesdiv.explications.classList.remove('explique')
    stor.lesdiv.solution.classList.remove('correction')

    return true
  } // isRepOk

  function egalfrac (a, b, c, d) {
    if (b === 0 || d === 0) return false
    return (j3pArrondi(a * d, 0) === j3pArrondi(b * c, 0))
  }
  function desactiveAll () {
    if (stor.Lexo.question === 'lire') {
      stor.repnum.disable()
      if (stor.Lexo.nat === 'frac') {
        stor.repden.disable()
      }
    }
  }

  function barrelesfo () {
    if (stor.Lexo.question === 'lire') {
      stor.repnum.barreIfKo()
      if (stor.Lexo.nat === 'frac') {
        stor.repden.barreIfKo()
      }
    }
  }

  function mettouvert () {
    if (stor.Lexo.question === 'lire') {
      stor.repnum.corrige(true)
      if (stor.Lexo.nat === 'frac') {
        stor.repden.corrige(true)
      }
    }
  }
  function afficheCorrection (bool) {
    let buf
    if (stor.errSimpl) {
      stor.yaexplik = true
      j3pAffiche(stor.lesdiv.explications, null, 'Tu dois simplifier ta réponse !\n')
    }
    if (stor.errDiv) {
      stor.yaexplik = true
      j3pAffiche(stor.lesdiv.explications, null, 'Compte bien le nombre de divisions d’une unité !\n')
    }
    if (stor.errNum) {
      stor.yaexplik = true
      if (stor.Lexo.nat === 'frac') {
        j3pAffiche(stor.lesdiv.explications, null, 'Erreur au numérateur !\n')
      }
    }
    if (stor.errUnit) {
      stor.yaexplik = true
      j3pAffiche(stor.lesdiv.explications, null, 'Tu dois compter depuis l’origine !\n')
    }
    if (stor.errDte) {
      stor.yaexplik = true
      j3pAffiche(stor.lesdiv.explications, null, 'Tu n’as pas placé le point sur la ' + stor.mu + ' !\n')
    }
    if (stor.errEcrit) {
      stor.yaexplik = true
      if (stor.errEcritList !== '') {
        j3pAffiche(stor.lesdiv.explications, null, stor.errEcritList + '\n')
      } else {
        j3pAffiche(stor.lesdiv.explications, null, 'Erreur d’écriture au numérateur !\n')
      }
    }
    if (stor.errPrecis) {
      stor.yaexplik = true
      j3pAffiche(stor.lesdiv.explications, null, 'Sois plus précis !\n')
    }

    if (bool) {
      barrelesfo()
      desactiveAll()
      if (stor.Lexo.question === 'lire') {
        stor.yaco = true
        if (stor.Lexo.nat === 'frac') {
          buf = stor.grad / stor.compt
          j3pAffiche(stor.lesdiv.solution, null, 'L’unité est divisée en $' + stor.grad + '$ parties, \n')

          let tu, mmmmp, acomp, tt
          if (stor.centre === 0 && ds.Positifs === 'toujours') {
            tu = '+'
            mmmmp = (((buf * stor.num) % stor.grad) === 1) ? 'partie' : 'parties'
            j3pAffiche(stor.lesdiv.solution, null, 'Je compte $' + ((buf * stor.num) % stor.grad) + '$ ' + mmmmp + ' à partir de $' + Math.trunc((buf * stor.num) / stor.grad) + '$ \n')
            acomp = 0
          } else {
            acomp = stor.centre
            if (Math.abs(stor.num / stor.compt - (stor.centre + 1)) < Math.abs(stor.num / stor.compt - stor.centre)) acomp = stor.centre + 1

            if (stor.num / stor.compt > acomp) {
              tt = 'après'
              tu = '+'
            } else {
              tt = 'avant'
              tu = '-'
            }
            mmmmp = 'parties'
            if (Math.abs(Math.abs(acomp * stor.grad) - Math.abs(stor.num * buf)) === 1) mmmmp = 'partie'
            j3pAffiche(stor.lesdiv.solution, null, 'Je compte $' + Math.abs(Math.abs(acomp * stor.grad) - Math.abs(stor.num * buf)) + '$ ' + mmmmp + ' ' + tt + ' la graduation $' + acomp + '$ \n')
          }
          if (Math.trunc((buf * stor.num) / stor.grad) !== 0) {
            j3pAffiche(stor.lesdiv.solution, null, 'Donc $x_{A} = \\frac{' + acomp * stor.grad + '}{' + stor.grad + '} ' + tu + ' \\frac{' + Math.abs(Math.abs(acomp * stor.grad) - Math.abs(stor.num * buf)) + '}{' + stor.grad + '} = \\frac{' + (buf * stor.num) + '}{' + stor.grad + '}$')
          } else {
            j3pAffiche(stor.lesdiv.solution, null, 'Donc $x_{A} = \\frac{' + (buf * stor.num) + '}{' + stor.grad + '}$ ')
          }
          if (stor.compt !== stor.grad) {
            j3pAffiche(stor.lesdiv.solution, null, '$ = ' + stor.laf + '$')
          }
        } else {
          j3pAffiche(stor.lesdiv.solution, null, 'Une petite graduation vaut $' + String(j3pArrondi(Math.pow(10, stor.Lexo.rang), 4)).replace('.', ',') + '$ donc \n')
          j3pAffiche(stor.lesdiv.solution, null, '$x_{A} = ' + stor.laf + '$')
        }
      } else {
        stor.mtgAppLecteur.setPointPosition(stor.idSvg, '#ppp1', stor.xrep, 50, true)
        stor.mtgAppLecteur.setVisible(stor.idSvg, '#afff1', true, true)
        stor.mtgAppLecteur.setVisible(stor.idSvg, '#afnom', false, true)
        stor.mtgAppLecteur.setText(stor.idSvg, '#afff1', '$A$', true)
        stor.mtgAppLecteur.setColor(stor.idSvg, '#afff1', 30, 30, 200, true)
        stor.mtgAppLecteur.setColor(stor.idSvg, '#ptdep', 200, 30, 30, true)
        stor.mtgAppLecteur.setVisible(stor.idSvg, stor.ptrep, true, true)
        stor.mtgAppLecteur.setColor(stor.idSvg, stor.ptrep, 30, 30, 200, true)
        if (stor.compt !== stor.grad) {
          let i
          for (i = 0; i < 36; i++) {
            if ((i + 1) % (stor.grad / stor.compt) === 0 && (i + 1) % stor.grad !== 0) {
              stor.mtgAppLecteur.setColor(stor.idSvg, '#seg' + i, 30, 150, 150, true)
              stor.mtgAppLecteur.setLineStyle(stor.idSvg, '#seg' + i, 0, 3, true)
            }
          }
        }
        stor.mtgAppLecteur.updateFigure(stor.idSvg)
        stor.mtgAppLecteur.setActive(stor.idSvg, false)
      }
    }
    if (ds.theme === 'zonesAvecImageDeFond') {
      if (stor.yaexplik) {
        stor.lesdiv.explications.classList.add('explique')
      }
      if (stor.yaco) {
        stor.lesdiv.solution.classList.add('correction')
      }
    }
  } // afficheCorrection

  function enonceMain () {
    if (me.etapeCourante === 1) {
      // A chaque répétition, on nettoie la scène
      me.videLesZones()
      creeLesDiv()
      stor.Lexo = faisChoixExos()
      stor.lesdiv.explications.classList.remove('explique')
      stor.lesdiv.solution.classList.remove('correction')
    }

    /// //////////////////////////////////
    /* LE CODE PRINCIPAL DE LA SECTION */
    /// //////////////////////////////////

    makefig()
    poseQuestion()
    me.cacheBoutonSuite()
    me.finEnonce()
  }

  switch (this.etat) {
    case 'enonce':
      // code exécuté au lancement de la section
      if (me.debutDeLaSection) {
        initSection()
      } else {
        enonceMain()
      }
      break // case "enonce":

    case 'correction':
      // On teste si une réponse a été saisie
      stor.yaexplik = false
      if (stor.lesdiv === undefined) return
      stor.lesdiv.explications.innerHTML = ''
      stor.lesdiv.correction.innerHTML = ''
      stor.yaco = false

      if (yaReponse()) {
        // Bonne réponse
        if (isRepOk()) {
          stor.lesdiv.correction.style.color = this.styles.cbien
          stor.lesdiv.correction.innerHTML = cBien

          desactiveAll()
          mettouvert()
          this.score++
          this.typederreurs[0]++
          return this.finCorrection('navigation', true)
        }

        // Pas de bonne réponse
        stor.lesdiv.correction.style.color = this.styles.cfaux
        stor.lesdiv.correction.innerHTML = cFaux
        if (me.isElapsed) {
          // A cause de la limite de temps :
          stor.lesdiv.correction.innerHTML += tempsDepasse + '<BR> Regarde la correction.'
          this.typederreurs[10]++
          afficheCorrection(true)
          return this.finCorrection('navigation', true)
        }

        // Réponse fausse :
        if (me.essaiCourant < me.donneesSection.nbchances) {
          // il reste des essais
          stor.lesdiv.correction.innerHTML += '<br>' + essaieEncore
          afficheCorrection(false)
          this.typederreurs[1]++
          return this.finCorrection() // on reste dans l’état correction
        }

        // Erreur au dernier essai
        stor.lesdiv.correction.innerHTML += '<BR> Regarde la correction.'
        afficheCorrection(true)
        if (stor.encours === 'p') {
          stor.pose = false
          stor.encours = 't'
        }
        this.typederreurs[2]++
        return this.finCorrection('navigation', true)
      }

      // pas de réponse
      stor.lesdiv.correction.style.color = this.styles.cfaux
      stor.lesdiv.correction.innerHTML = reponseIncomplete
      if (stor.encours === 'm') {
        stor.lesdiv.correction.innerHTML = 'Réponse incomplète ! <br> Tu dois déplacer le point ' + stor.C + ' .'
      }
      this.finCorrection()
      break // case "correction":

    case 'navigation':
      if (!this.sectionTerminee()) {
        this.etat = 'enonce'
      }
      this.finNavigation()
      break // case "navigation":
  }
}
