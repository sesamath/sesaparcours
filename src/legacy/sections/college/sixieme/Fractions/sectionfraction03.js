import { j3pAddElt, j3pAjouteBouton, j3pArrondi, j3pClone, j3pEmpty, j3pGetRandomInt, j3pPGCD, j3pShuffle } from 'src/legacy/core/functions'
import { j3pCreeFenetres, j3pAfficheCroixFenetres, j3pToggleFenetres } from 'src/legacy/core/functionsJqDialog'
import BrouillonCalculs from 'src/legacy/outils/brouillon/BrouillonCalculs'
import ListeDeroulante from 'src/legacy/outils/listeDeroulante/ListeDeroulante'
import { affichefois, afficheFrac } from 'src/legacy/outils/zoneStyleMathquill/functions'
import ZoneStyleMathquill3 from 'src/legacy/outils/zoneStyleMathquill/ZoneStyleMathquill3'
import { addDefaultTable } from 'src/legacy/themes/table'
import { j3pAffiche } from 'src/lib/mathquill/functions'
import textesGeneriques from 'src/lib/core/textes'

const { cBien, cFaux, essaieEncore, reponseIncomplete, tempsDepasse } = textesGeneriques

/**
 * Ajouter une description ici
 * @fileOverview
 * @author Tommy Barroy <t.barroy@laposte.net>
 * @since 2017-10
*/

export const params = {
  outils: ['mathquill', 'calculatrice'],
  /**
   * Ce qui est paramétrable dans l’éditeur de graphe
   * @type {LegacySectionParamsParametres}
   */
  parametres: [
    ['nbrepetitions', 9, 'entier', 'Nombre de répétitions de la section'],
    ['nbchances', 2, 'entier', 'Nombre d’essais possibles.'],
    ['produit', true, 'boolean', '<u>true</u>: L’élève doit écrire le produit.'],
    ['detail', true, 'boolean', '<u>true</u>: L’élève doit écrire le calcul.'],
    ['pourcentage', 'oui', 'liste', '<u>oui</u>:  présence de %.', ['oui', 'non', 'les deux']],
    ['Calculatrice', true, 'boolean', '<u>true</u>:  Une calculatrice est disponible'],
    // le modèle gère la compatibilité ascendante avec l’ancien paramètre MiseEnPagePlusClaire
    ['theme', 'standard', 'liste', 'Modèle pour l’affichage', ['standard', 'zonesAvecImageDeFond']]
  ],
  // dans le cas d’une section QUANTITATIVE
  pe: [
    { pe_1: 'Egalite' },
    { pe_2: 'Ecriture' },
    { pe_3: 'Produit_en_croix' },
    { pe_4: 'Simplification' }
  ]
}

const problemes = [
  { enonce: 'Myriam a mangé £a de son paquet de chips de $£b$ g. <br>  Quelle quantité de chips a-t-elle mangée ?', unite: 'g', limite1h: 800, limite1b: 200, cal: 'quantité' },
  { enonce: 'Dans le collège de $£b$ élèves de Claudio,<br> £a des enfants ont un animal de compagnie. <br>  Combien d’élèves ont un animal de compagnie ?', unite: 'élèves', limite1h: 800, limite1b: 80, cal: 'nombre d’élèves' },
  { enonce: 'George a préparé $£b$ cL de potion magique. <br>L’eau représente £a de la potion. <br>  Quelle quantité d’eau contient la potion ?', unite: 'cL', limite1h: 500, limite1b: 10, cal: 'quantité' },
  { enonce: 'Frédérique achète un vétement à $£b$ €. <br>Il y a une réduction de £a <br>  Quel est le montant de la réduction ?', unite: '€', limite1h: 200, limite1b: 10, cal: 'réduction' }
]

/**
 * section fraction03
 * @this {Parcours}
 */
export default function main () {
  const me = this
  const stor = me.storage
  const ds = me.donneesSection

  function creeLesDiv () {
    stor.lesdiv = {}
    stor.lesdiv.conteneur = j3pAddElt(me.zonesElts.MG, 'div', {
      style: me.styles.etendre('toutpetit.enonce', { padding: '10px' })
    })
    stor.lesdiv.consigneG = addDefaultTable(stor.lesdiv.conteneur, 1, 1)[0][0]
    stor.lesdiv.etape = addDefaultTable(stor.lesdiv.conteneur, 1, 1)[0][0]
    stor.lesdiv.travail = addDefaultTable(stor.lesdiv.conteneur, 1, 1)[0][0]
    stor.lesdiv.explications = j3pAddElt(stor.lesdiv.conteneur, 'div')
    stor.lesdiv.solution = j3pAddElt(stor.lesdiv.conteneur, 'div')
    stor.lesdiv.calculatrice = j3pAddElt(me.zonesElts.MD, 'div', {
      style: me.styles.etendre('toutpetit.enonce', { padding: '10px' })
    })
    stor.lesdiv.aide = j3pAddElt(me.zonesElts.MD, 'div', {
      style: me.styles.etendre('toutpetit.enonce', { padding: '10px' })
    })
    stor.lesdiv.correction = j3pAddElt(me.zonesElts.MD, 'div', {
      style: me.styles.etendre('toutpetit.enonce', { padding: '10px' })
    })
    stor.lesdiv.etape.style.color = me.styles.toutpetit.correction.color
    stor.lesdiv.solution.style.color = me.styles.toutpetit.correction.color
    stor.lesdiv.explications.style.color = me.styles.cfaux
    me.zonesElts.MG.classList.add('fond')
  }
  function poseQuestion () {
    stor.foBrouill = false
    j3pAffiche(stor.lesdiv.consigneG, '', stor.Lexo.pb.enonce.replace('£a', stor.affenonce).replace('£b', stor.nombre).replace('de  les', 'de'))
    stor.tacCont = addDefaultTable(stor.lesdiv.travail, 3, 1)
    if (ds.produit) {
      faisProd()
      return
    }
    stor.foBrouill = true
    if (ds.detail) {
      faisDetail()
      return
    }
    faisResult()
  }

  function faisDetail () {
    j3pEmpty(stor.lesdiv.etape)
    stor.etapeencours = 'detail'
    j3pAffiche(stor.lesdiv.etape, '', '<b><u>Étape</u>:Détaille le calcul.</b>')
    if (stor.foBrouill) {
      stor.tabZpro = addDefaultTable(stor.tacCont[0][0], 1, 3)
      j3pAffiche(stor.tabZpro[0][0], '', stor.Lexo.pb.cal)
      j3pAffiche(stor.tabZpro[0][1], '', '$=$')
      j3pAffiche(stor.tabZpro[0][2], '', stor.aff + '$ \\times ' + stor.nombre + '$')
      stor.foBrouill = false
    }
    const tabZ = addDefaultTable(stor.tacCont[1][0], 1, 3)
    j3pAffiche(tabZ[0][0], '', stor.Lexo.pb.cal)
    j3pAffiche(tabZ[0][1], '', '$=$')
    if (stor.foco1) {
      j3pEmpty(stor.tacCont[0][2])
      stor.tacCont[0][2].style.color = me.styles.petit.correction.color
      j3pAffiche(stor.tacCont[0][2], '', stor.aff + '$ \\times ' + stor.nombre + '$')
    }
    const t = afficheFrac(tabZ[0][2])
    const t2 = affichefois(t[0])
    stor.zoneDet3 = new ZoneStyleMathquill3(t[2], { restric: '0123456789', hasAutoKeyboard: false, limite: 6 })
    stor.zoneDet2 = new ZoneStyleMathquill3(t2[0], { restric: '0123456789', hasAutoKeyboard: false, limite: 6 })
    stor.zoneDet1 = new ZoneStyleMathquill3(t2[2], { restric: '0123456789', hasAutoKeyboard: false, limite: 6 })
  }
  function faisResult () {
    stor.etapeencours = 'reponse'
    if ((!ds.produit) && (!ds.detail)) {
      stor.tabZpro = addDefaultTable(stor.tacCont[0][0], 1, 3)
      j3pAffiche(stor.tabZpro[0][0], '', stor.Lexo.pb.cal)
      j3pAffiche(stor.tabZpro[0][1], '', '$=$')
      j3pAffiche(stor.tabZpro[0][2], '', stor.aff + '$ \\times ' + stor.nombre + '$')
      stor.foBrouill = false
    } else {
      j3pEmpty(stor.lesdiv.etape)
      j3pAffiche(stor.lesdiv.etape, '', '<b><u>Étape</u>:Donner la réponse.</b>')
    }
    const tabFin = addDefaultTable(stor.tacCont[2][0], 2, 4)
    stor.brouillon1 = new BrouillonCalculs(
      {
        caseBoutons: tabFin[0][0],
        caseEgal: tabFin[0][1],
        caseCalculs: tabFin[0][2],
        caseApres: tabFin[0][3]
      }, {
        limite: 30,
        limitenb: 20,
        restric: '()0123456789+-*/,.',
        hasAutoKeyboard: true,
        lmax: 2
      }
    )
    j3pAffiche(tabFin[1][0], '', stor.Lexo.pb.cal)
    j3pAffiche(tabFin[1][1], '', '$=$')
    j3pAffiche(tabFin[1][3], '', stor.Lexo.pb.unite)
    stor.zoneRep = new ZoneStyleMathquill3(tabFin[1][2], { restric: '0123456789', hasAutoKeyboard: false, limite: 6 })
  }
  function faisProd () {
    stor.etapeencours = 'prod'
    j3pAffiche(stor.lesdiv.etape, '', '<b><u>Étape</u>: traduire par une opération.</b>')
    stor.tabZpro = addDefaultTable(stor.tacCont[0][0], 1, 3)
    j3pAffiche(stor.tabZpro[0][0], '', stor.Lexo.pb.cal)
    j3pAffiche(stor.tabZpro[0][1], '', '$=$')
    let ll = []
    ll.push(stor.aff + '$  + ' + stor.nombre + '$')
    ll.push(stor.aff + '$  - ' + stor.nombre + '$')
    ll.push(stor.aff + '$  \\times ' + stor.nombre + '$')
    ll.push(stor.aff + '$  \\div ' + stor.nombre + '$')
    ll = j3pShuffle(ll)
    ll.splice(0, 0, 'Choisir')
    stor.listProd = ListeDeroulante.create(stor.tabZpro[0][1], ll)
  }
  function boutonfen () {
    j3pToggleFenetres('Calculatrice')
  }
  function faisChoixExos () {
    const e = ds.lesExos.pop()
    let t3, i
    stor.num = j3pGetRandomInt(2, 98)
    stor.den = 100
    if (e.formule !== '%') {
      t3 = j3pPGCD(stor.num, stor.den, { negativesAllowed: true, valueIfZero: 1 })
      stor.num = Math.round(stor.num / t3)
      stor.den = Math.round(stor.den / t3)
      if (stor.num === 1) {
        stor.affenonce = '$\\frac{' + stor.num + '}{' + stor.den + '}$'
      } else {
        stor.affenonce = ' les $\\frac{' + stor.num + '}{' + stor.den + '}$'
      }
      stor.aff = '$\\frac{' + stor.num + '}{' + stor.den + '}$'
    } else {
      stor.affenonce = '$' + stor.num + '$ %'
      stor.aff = '$\\frac{' + stor.num + '}{' + stor.den + '}$'
    }
    stor.repos = []
    for (i = Math.ceil(e.pb.limite1b * stor.num / stor.den); i < Math.floor(e.pb.limite1h * stor.num / stor.den) + 1; i++) {
      if ((i * stor.den) % stor.num === 0) stor.repos.push(i)
    }
    stor.rep = stor.repos[j3pGetRandomInt(0, stor.repos.length - 1)]
    stor.nombre = stor.rep * stor.den / stor.num
    stor.foco1 = false
    return e
  }

  function initSection () {
    let i
    // Construction de la page
    me.construitStructurePage('presentation1bis')
    me.donneesSection.lesExos = []

    let lp = []
    if ((ds.pourcentage === 'oui') || (ds.pourcentage === 'les deux')) lp.push('%')
    if ((ds.pourcentage === 'non') || (ds.pourcentage === 'les deux')) lp.push('frac')
    lp = j3pShuffle(lp)
    for (i = 0; i < me.donneesSection.nbitems; i++) {
      me.donneesSection.lesExos.push({ formule: lp[i % (lp.length)] })
    }
    me.donneesSection.lesExos = j3pShuffle(me.donneesSection.lesExos)

    lp = j3pClone(problemes)
    lp = j3pShuffle(lp)

    for (i = 0; i < me.donneesSection.nbitems; i++) {
      me.donneesSection.lesExos[i].pb = lp[i % (lp.length)]
    }
    me.donneesSection.lesExos = j3pShuffle(me.donneesSection.lesExos)

    me.typederreurs = [0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0]
    me.afficheTitre('Calculer une fraction d’une quantité')
    enonceMain()
  }

  function yaReponse () {
    if (stor.etapeencours === 'prod') {
      if (!stor.listProd.changed) {
        stor.listProd.focus()
        return false
      }
      return true
    }
    if (stor.etapeencours === 'detail') {
      if (stor.zoneDet1.reponse() === '') {
        stor.zoneDet1.focus()
        return false
      }
      if (stor.zoneDet2.reponse() === '') {
        stor.zoneDet2.focus()
        return false
      }
      if (stor.zoneDet3.reponse() === '') {
        stor.zoneDet3.focus()
        return false
      }
      return true
    }
    if (stor.etapeencours === 'reponse') {
      if (stor.zoneRep.reponse() === '') {
        stor.zoneRep.focus()
        return false
      }
      return true
    }
  }

  function isRepOk () {
    stor.errProd = false
    stor.errDen = false
    stor.errNum = false
    stor.errNum2 = false

    let ok = true
    let t1, t2, t3

    if (stor.etapeencours === 'prod') {
      if (stor.listProd.reponse.indexOf('times') === -1) {
        stor.errProd = true
        stor.listProd.corrige(false)
        return false
      }
      return true
    }
    if (stor.etapeencours === 'detail') {
      t1 = parseFloat(stor.zoneDet1.reponse())
      t2 = parseFloat(stor.zoneDet2.reponse())
      t3 = parseFloat(stor.zoneDet3.reponse())

      if (t3 !== stor.den) {
        stor.errDen = true
        stor.zoneDet3.corrige(false)
        ok = false
      }
      if ((t1 !== stor.num) && (t1 !== stor.nombre)) {
        stor.errNum = true
        stor.zoneDet1.corrige(false)
        ok = false
      }
      if ((t2 !== stor.num) && (t2 !== stor.nombre)) {
        stor.errNum = true
        stor.zoneDet2.corrige(false)
        ok = false
      }
      if (t1 === t2) {
        stor.errNum2 = true
        stor.zoneDet2.corrige(false)
        ok = false
      }
      return ok
    }
    if (stor.etapeencours === 'reponse') {
      t1 = parseFloat(stor.zoneRep.reponse())
      if (t1 !== stor.rep) {
        stor.errFin = true
        stor.zoneRep.corrige(false)
        return false
      }
      return true
    }

    return ok
  }

  function desactiveAll () {
    if (stor.etapeencours === 'prod') {
      stor.listProd.disable()
    }
    if (stor.etapeencours === 'detail') {
      stor.zoneDet1.disable()
      stor.zoneDet2.disable()
      stor.zoneDet3.disable()
    }
    if (stor.etapeencours === 'reponse') {
      stor.zoneRep.disable()
      stor.brouillon1.disable()
    }
  }

  function passeToutVert () {
    if (stor.etapeencours === 'prod') {
      stor.listProd.corrige(true)
    }
    if (stor.etapeencours === 'detail') {
      stor.zoneDet1.corrige(true)
      stor.zoneDet2.corrige(true)
      stor.zoneDet3.corrige(true)
    }
    if (stor.etapeencours === 'reponse') {
      stor.zoneRep.corrige(true)
    }
  }

  function barrelesfo () {
    if (stor.etapeencours === 'prod') {
      stor.listProd.barre()
    }
    if (stor.etapeencours === 'detail') {
      stor.zoneDet1.barreIfKo()
      stor.zoneDet2.barreIfKo()
      stor.zoneDet3.barreIfKo()
    }
    if (stor.etapeencours === 'reponse') {
      stor.zoneRep.barre()
    }
  }

  function affCorrFaux (isFin) {
    /// //affiche indic

    if (stor.errProd) {
      j3pAffiche(stor.lesdiv.explications, '', 'Ce n’est pas la bonne opération !\n')
      stor.yaexplik = true
    }
    if (stor.errNum) {
      j3pAffiche(stor.lesdiv.explications, '', 'Erreur au numérateur !\n')
      stor.yaexplik = true
    }
    if (stor.errNum2) {
      j3pAffiche(stor.lesdiv.explications, '', 'Tu utilises deux fois le même nombre !\n')
      stor.yaexplik = true
    }
    if (stor.errDen) {
      j3pAffiche(stor.lesdiv.explications, '', 'Erreur au dénominateur !\n')
      stor.yaexplik = true
    }
    if (stor.errFin) {
      j3pAffiche(stor.lesdiv.explications, '', 'Erreur de calcul !\n')
      stor.yaexplik = true
    }

    if (isFin) {
      // barre les faux
      barrelesfo()
      desactiveAll()
      stor.lesdiv.solution.classList.add('correction')
      // afficheco
      if (stor.etapeencours === 'prod') {
        stor.foco1 = true
        j3pAffiche(stor.lesdiv.solution, '', stor.Lexo.pb.cal + ' $ = $' + stor.aff + '$ \\times ' + stor.nombre + '$')
      }
      if (stor.etapeencours === 'detail') {
        j3pAffiche(stor.lesdiv.solution, '', stor.Lexo.pb.cal + ' $ = \\frac{' + stor.num + ' \\times ' + stor.nombre + '}{' + stor.den + '}$')
      }
      if (stor.etapeencours === 'reponse') {
        j3pAffiche(stor.lesdiv.solution, '', stor.Lexo.pb.cal + ' $ = ' + stor.rep + '$ ' + stor.Lexo.pb.unite)
      }
    }
  } // affCorrFaux

  function enonceMain () {
    if (me.etapeCourante === 1) {
      // A chaque répétition, on nettoie la scène
      // A commenter si besoin
      me.videLesZones()
    }

    creeLesDiv()
    stor.lesdiv.travail.classList.add('travail')
    stor.lesdiv.consigneG.classList.add('enonce')
    stor.Lexo = faisChoixExos()
    poseQuestion()
    if (ds.Calculatrice) {
      me.fenetresjq = [{ name: 'Calculatrice', title: 'Une calculatrice', left: 600, top: 10 }]
      j3pCreeFenetres(me)
      j3pAfficheCroixFenetres('Calculatrice')
      j3pAjouteBouton(stor.lesdiv.calculatrice, 'Calculatrice', 'MepBoutons', 'Calculatrice', boutonfen)
    }
    me.finEnonce()
  }

  switch (this.etat) {
    case 'enonce':
      // code exécuté au lancement de la section
      if (me.debutDeLaSection) {
        initSection()
      } else {
        enonceMain()
      }
      break // case "enonce":

    case 'correction':
      // On teste si une réponse a été saisie
      stor.yaexplik = false
      stor.lesdiv.explications.innerHTML = ''
      stor.lesdiv.solution.innerHTML = ''
      stor.lesdiv.explications.classList.remove('explique')
      stor.lesdiv.solution.classList.remove('correction')

      if (yaReponse()) {
        // Bonne réponse
        if (isRepOk()) {
          stor.lesdiv.correction.style.color = this.styles.cbien
          stor.lesdiv.correction.innerHTML = cBien

          desactiveAll()
          passeToutVert()
          if (stor.etapeencours === 'prod') {
            stor.niv++
            stor.divencours++
            if (ds.detail) {
              faisDetail()
              stor.niv++
              return
            }
            stor.foBrouill = true
            faisResult()
            return
          }
          if (stor.etapeencours === 'detail') {
            stor.niv++
            faisResult()
            return
          }

          stor.etapeencours = ''
          this.score++
          this.typederreurs[0]++
          return this.finCorrection('navigation', true)
        }

        // Pas de bonne réponse
        stor.lesdiv.correction.style.color = this.styles.cfaux
        stor.lesdiv.correction.innerHTML = cFaux
        if (this.isElapsed) {
          // A cause de la limite de temps :
          stor.lesdiv.correction.innerHTML += tempsDepasse + '<BR>'
          this.typederreurs[10]++

          affCorrFaux(true)
          stor.lesdiv.solution.classList.add('correction')

          return this.finCorrection('navigation', true)
        }

        // Réponse fausse :
        if (me.essaiCourant < me.donneesSection.nbchances) {
          stor.lesdiv.correction.innerHTML += '<br>' + essaieEncore
          affCorrFaux(false)
          this.typederreurs[1]++
          return this.finCorrection()
        }

        // Erreur au dernier essai
        affCorrFaux(true)
        stor.lesdiv.solution.classList.add('correction')

        if (stor.etapeencours === 'compare') {
          this.score = j3pArrondi(this.score + 0.1, 1)
        }
        if (stor.etapeencours === 'calcul') {
          this.score = j3pArrondi(this.score + 0.4, 1)
        }
        if (stor.etapeencours === 'concl') {
          this.score = j3pArrondi(this.score + 0.7, 1)
        }

        this.typederreurs[2]++
        return this.finCorrection('navigation', true)
      }

      // pas de réponse
      stor.lesdiv.correction.style.color = this.styles.cfaux
      stor.lesdiv.correction.innerHTML = reponseIncomplete
      this.finCorrection()
      break // case "correction":

    case 'navigation':
      if (!this.sectionTerminee()) {
        this.etat = 'enonce'
      }
      this.finNavigation()
      break // case "navigation":
  }
}
