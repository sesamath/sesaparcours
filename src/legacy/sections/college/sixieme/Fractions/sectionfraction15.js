import { j3pAddContent, j3pAddElt, j3pAjouteBouton, j3pArrondi, j3pGetRandomInt, j3pPGCD, j3pShuffle } from 'src/legacy/core/functions'
import { j3pCreeFenetres, j3pAfficheCroixFenetres, j3pToggleFenetres } from 'src/legacy/core/functionsJqDialog'
import { addDefaultTable } from 'src/legacy/themes/table'
import ZoneColoriage from 'src/legacy/outils/zoneColoriage/ZoneColoriage'
import textesGeneriques from 'src/lib/core/textes'

const { cBien, cFaux, essaieEncore, reponseIncomplete, tempsDepasse } = textesGeneriques

/**
 * Ajouter une description ici
 * @fileOverview
 * @author Tommy Barroy <t.barroy@laposte.net>
 * @since 2017-10
*/

export const params = {
  outils: ['mathquill', 'calculatrice'],
  /**
   * Ce qui est paramétrable dans l’éditeur de graphe
   * @type {LegacySectionParamsParametres}
   */
  parametres: [
    ['nbrepetitions', 3, 'entier', 'Nombre de répétitions de la section'],
    ['nbchances', 1, 'entier', 'Nombre d’essais possibles'],
    ['limite', 0, 'entier', 'Temps disponible par répétition, en secondes'],
    ['difficulteMin', 3, 'entier', 'De 3 a 10'],
    ['difficulteMax', 10, 'entier', 'De 3 a 10'],
    ['difficulte', 'progressive', 'liste', 'progressive ou aléatoire', ['progressive', 'aléatoire']],
    ['Calculatrice', true, 'boolean', '<u>true</u>: L’élève dispose d’une calculatrice.'],
    ['theme', 'standard', 'liste', 'Modèle pour l’affichage', ['standard', 'zonesAvecImageDeFond']]
  ]
}

/**
 * section fraction15
 * @this {Parcours}
 */
export default function main () {
  const me = this
  const stor = me.storage
  let ds = me.donneesSection

  function getDonnees () {
    return {
      // Nombre de répétitions de l’exercice
      nbrepetitions: 3,
      Justifie: true,
      Situation: true,
      Calculatrice: true,
      Donnees: 'les deux',
      Pourcentage: true,
      // Si indication != "" alors un lien "Indication" s’affiche dans la zone Inférieure Gauche (me.zones.IG
      // une indication<br>de deux lignes..."
      indication: '',
      limite: 0,
      difficulte: 'progressive',
      difficulteMin: 3,
      difficulteMax: 4,

      // pour le cas de presentation1bis
      // En effet pas d’indication possible pour cette présentation
      // surchargeindication: false,

      // Nombre de chances dont dispose l’él§ve pour répondre à la question
      nbchances: 1,

      //  || "presentation2" || "presentation3"  || "presentation1bis"
      structure: 'presentation1bis'
    }
  }

  function initSection () {
    ds = getDonnees()
    me.donneesSection = ds
    // On surcharge avec les parametres passés par le graphe
    me.surcharge()
    me.validOnEnter = false // ex donneesSection.touche_entree

    // Construction de la page
    me.construitStructurePage(me.donneesSection.structure)

    me.donneesSection.nbetapes = 1
    me.donneesSection.nbitems = me.donneesSection.nbrepetitions

    stor.lesExos = []
    ds.difficulteMin = Math.max(3, ds.difficulteMin)
    ds.difficulteMax = Math.min(10, ds.difficulteMax)
    if (ds.difficulteMax < ds.difficulteMin) ds.difficulteMin = ds.difficulteMax
    let lp = []
    for (let i = ds.difficulteMin; i <= ds.difficulteMax; i++) {
      lp.push(i)
    }
    lp = j3pShuffle(lp)
    for (let i = 0; i < me.donneesSection.nbitems; i++) {
      stor.lesExos.push(lp[i % lp.length])
    }
    if (ds.difficulte === 'progressive') stor.lesExos = stor.lesExos.sort((a, b) => { return b - a })
    me.typederreurs = [0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0]

    me.score = 0
    const tt = 'Fractions égales - pixel art'

    me.afficheTitre(tt)

    enonceMain()
  }

  function faisChoixExos () {
    stor.frac1 = newFrac()
    stor.diff = stor.lesExos.pop()
    do {
      stor.frac2 = newFrac()
    } while (stor.frac1.num * stor.frac2.den === stor.frac2.num * stor.frac1.den)
    do {
      stor.frac3 = newFrac()
    } while (stor.frac1.num * stor.frac3.den === stor.frac3.num * stor.frac1.den || stor.frac3.num * stor.frac2.den === stor.frac2.num * stor.frac3.den)
  }

  function newFrac () {
    let ff = {}
    do {
      ff = { num: j3pGetRandomInt(1, 9), den: j3pGetRandomInt(1, 9) }
      const pg = j3pPGCD(ff.num, ff.den)
      ff = { num: j3pArrondi(ff.num / pg, 0), den: j3pArrondi(ff.den / pg, 0) }
    } while (ff.den === 1)
    return ff
  }
  function poseQuestion () {
    j3pAddContent(stor.lesdiv.enonce, '<b>Colorie ce carré en fonction de son contenu</b')
    stor.pixel = new ZoneColoriage({
      conteneur: stor.lesdiv.travail,
      classes: [{ mod: 'Une fraction égale à' + affFrac(stor.frac1), func: faisUndemi(stor.frac1) }, { mod: 'Une fraction égale à' + affFrac(stor.frac2), func: faisUndemi(stor.frac2) }, { mod: 'Une fraction égale à' + affFrac(stor.frac3), func: faisUndemi(stor.frac3) }],
      dim: stor.diff
    })
    stor.lesdiv.travail.classList.add('travail')
    stor.lesdiv.enonce.classList.add('enonce')
  }
  function affFrac (f) {
    return '$\\frac{' + f.num + '}{' + f.den + '}$'
  }
  function faisUndemi (ff) {
    return () => {
      const a = j3pGetRandomInt(1, 10)
      return affFrac({ num: a * ff.num, den: a * ff.den })
    }
  }
  function creeLesDiv () {
    stor.lesdiv = {}
    stor.lesdiv.conteneur = stor.lesdiv.conteneur = j3pAddElt(me.zonesElts.MG, 'div', {
      style: me.styles.etendre('toutpetit.enonce', { padding: '10px' })
    })
    stor.lesdiv.enonce = addDefaultTable(stor.lesdiv.conteneur, 1, 1)[0][0]
    stor.lesdiv.travail = addDefaultTable(stor.lesdiv.conteneur, 1, 1)[0][0]
    stor.lesdiv.calculatrice = j3pAddElt(me.zonesElts.MD, 'div', {
      style: me.styles.etendre('toutpetit.enonce', { padding: '10px' })
    })
    stor.lesdiv.correction = j3pAddElt(me.zonesElts.MD, 'div', {
      style: me.styles.etendre('toutpetit.enonce', { padding: '10px' })
    })
    me.zonesElts.MG.classList.add('fond')
  }

  function boutonfen () {
    j3pToggleFenetres('Calculatrice')
  }

  function yaReponse () {
    if (me.isElapsed) return true
    return stor.pixel.yareponse()
  }

  function isRepOk () {
    return stor.pixel.isOk()
  }

  function enonceMain () {
    me.videLesZones()
    creeLesDiv()
    faisChoixExos()
    poseQuestion()
    if (ds.Calculatrice) {
      me.fenetresjq = [{ name: 'Calculatrice', title: 'Une calculatrice', left: 600, top: 10 }]
      j3pCreeFenetres(me)
      j3pAfficheCroixFenetres('Calculatrice')
      j3pAjouteBouton(stor.lesdiv.calculatrice, 'Calculatrice', 'MepBoutons', 'Calculatrice', boutonfen)
    }
    me.finEnonce()
  }

  switch (me.etat) {
    case 'enonce':
      // code exécuté au lancement de la section
      if (me.debutDeLaSection) {
        initSection()
      } else {
        enonceMain()
      }
      break // case "enonce":

    case 'correction':
      // On teste si une réponse a été saisie
      stor.yaexplik = false
      stor.yaco = false

      if (yaReponse()) {
        // Bonne réponse
        if (isRepOk()) {
          me._stopTimer()
          stor.lesdiv.correction.style.color = me.styles.cbien
          stor.lesdiv.correction.innerHTML = cBien

          stor.pixel.disable()
          stor.pixel.corrige()
          me.score = j3pArrondi(me.score + 1, 1)

          me.typederreurs[0]++
          me.etat = 'navigation'
          me.sectionCourante()
        } else {
          // Pas de bonne réponse
          stor.lesdiv.correction.style.color = me.styles.cfaux
          stor.lesdiv.correction.innerHTML = cFaux
          if (me.isElapsed) {
            // A cause de la limite de temps :
            me._stopTimer()

            stor.lesdiv.correction.innerHTML += '<br>' + tempsDepasse + '<BR>'
            me.typederreurs[10]++
            stor.pixel.corrige(false)
            stor.pixel.disable()
            me.score = j3pArrondi(me.score + stor.pixel.getScore(), 1)

            me.etat = 'navigation'
            me.sectionCourante()
          } else {
            // Réponse fausse :
            if (me.essaiCourant < me.donneesSection.nbchances) {
              stor.lesdiv.correction.innerHTML += '<br>' + essaieEncore
              stor.pixel.corrige()
              me.typederreurs[1]++
            } else {
              // Erreur au nème essai
              me._stopTimer()
              me.cacheBoutonValider()

              stor.pixel.corrige(false)
              stor.pixel.disable()
              me.score = j3pArrondi(me.score + stor.pixel.getScore(), 1)

              me.typederreurs[2]++
              me.etat = 'navigation'
              me.sectionCourante()
            }
          }
        }
      } else {
        // pas de réponse
        stor.lesdiv.correction.style.color = me.styles.cfaux
        stor.lesdiv.correction.innerHTML = reponseIncomplete
        me.afficheBoutonValider()
      }

      // Obligatoire
      me.finCorrection()
      break // case "correction":

    case 'navigation':

      if (me.sectionTerminee()) {
        // On détermine la phrase d’état renvoyée par la section
        me.afficheBoutonSectionSuivante()
        me.focus('sectioncontinuer')
      } else {
        me.etat = 'enonce'
        me.afficheBoutonSuite()
        me.focus('boutoncontinuer')
      }
      // Obligatoire
      me.finNavigation()
      break // case "navigation":
  }
}
