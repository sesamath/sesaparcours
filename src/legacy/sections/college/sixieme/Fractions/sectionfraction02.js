import { j3pAddElt, j3pAddTxt, j3pEmpty, j3pGetRandomInt } from 'src/legacy/core/functions'
import { j3pAffiche } from 'src/lib/mathquill/functions'
import ZoneStyleMathquill1 from 'src/legacy/outils/zoneStyleMathquill/ZoneStyleMathquill1'
import { addDefaultTable } from 'src/legacy/themes/table'
import { afficheFrac } from 'src/legacy/outils/zoneStyleMathquill/functions'
import textesGeneriques from 'src/lib/core/textes'

const { cBien, cFaux, essaieEncore, regardeCorrection, reponseIncomplete, tempsDepasse } = textesGeneriques

/**
 * Ajouter une description ici
 * @fileOverview
 * @author Tommy Barroy <t.barroy@laposte.net>
 * @since 2017-10
*/

export const params = {
  outils: ['calculatrice', 'mathquill'],
  /**
   * Ce qui est paramétrable dans l’éditeur de graphe
   * @type {LegacySectionParamsParametres}
   */
  parametres: [
    ['nbrepetitions', 8, 'entier', 'Nombre de répétitions de la section'],
    ['indication', '', 'string', 'Indication pour chaque répétition'],
    ['limite', 0, 'entier', 'Temps disponible par répétition, en secondes'],
    ['nbchances', 1, 'entier', 'Nombre d’essais par répétition'],
    ['theme', 'standard', 'liste', 'Modèle pour l’affichage', ['standard', 'zonesAvecImageDeFond']]
  ]
}

/**
 * section fraction02
 * @this {Parcours}
 */
export default function main () {
  const me = this
  const stor = me.storage
  let ds = me.donneesSection

  function affcofo (boo) {
    j3pAffiche(stor.lesdiv.explications, null, ds.textes.phrase5 + '\n',
      { a: stor.denominateur, b: stor.numerateur })
    const n1 = parseFloat(stor.zonenum.reponse())
    const n2 = parseFloat(stor.zoneden.reponse())
    if ((n1 === stor.denominateur) && (n2 === stor.numerateur)) {
      j3pAffiche(stor.lesdiv.explications, null, ds.textes.phrase2)
    }
    stor.zonenum.corrige(false)
    stor.zoneden.corrige(false)
    stor.yaexplik = true
    if (boo) {
      stor.zonenum.barre()
      stor.zoneden.barre()
      stor.yaco = true
      stor.zonenum.disable()
      stor.zoneden.disable()
      j3pAffiche(stor.lesdiv.solution, null, ds.textes.phrase4)
      j3pAffiche(stor.lesdiv.solution, null, ' $ \\frac{' + stor.numerateur + '}{' + stor.denominateur + '}$')
      j3pAffiche(stor.lesdiv.solution, null, ds.textes.phrase3)
    }
  }
  function getDonnees () {
    return {

      // Nombre de répétitions de l’exercice
      nbrepetitions: 8,
      nbetapes: 1,

      // Si indication !== "" alors un lien "Indication" s’affiche dans la zone Inférieure Gauche (zones.IG
      // une indication<br>de deux lignes...";
      indication: '',

      // pour le cas de presentation1bis
      // En effet pas d’indication possible pour cette présentation
      // surchargeindication: false;

      // Nombre de chances dont dispose l’él§ve pour répondre à la question
      nbchances: 1,

      structure: 'presentation1', //  || "presentation2" || "presentation3"  || "presentation1bis"

      // Paramètres de la section, avec leurs valeurs par défaut.

      /*
          Les textes présents dans la section
          Sont donc accessibles dans le code par donneesSection.textes.phrase1
          Possibilité d’écrire dans le code :
          var lestextes: donneesSection.textes;
          puis accès à l’aide de lestextes.phrase1
          */
      textes: {
        couleurexplique: '#A9E2F3',
        couleurcorrec: '#A9F5A9',
        couleurenonce: '#D8D8D8',
        couleurenonceg: '#F3E2A9',
        couleuretiquettes: '#A9E2F3',
        consigne1: 'Complète avec une fraction.',
        enoncea1: '  $£b  \\times $',
        enoncea2: '  $ \\times  £b$ ',
        enonceb1: ' $= £a$  ',
        enonceb2: ' $£a =$ ',
        phrase2: 'Attention à la place du numérateur et du dénominateur.',
        phrase5: 'On cherche quel nombre multiplié par $£a$ donne $£b$ !',
        phrase4: 'La bonne réponse est ',
        phrase3: '.'

      },

      // Par défaut, pas de temps limite. Décommentez pour avoir un temps limite par défaut. En secondes.

      // limite: 10;
      /*
          Phrase d’état renvoyée par la section
          Si pe=0 alors la section est quantitative et renvoie un score entre 0 et 1.
          Dans le cas où la section est qualitative, lister les différents pe renvoyées :
              pe_1: "toto"
              pe_2: "tata"
              etc
          Dans la partie navigation, il faudra affecter la variable parcours.pe à l’une des précédentes.
          Par exemple,
          parcours.pe: donneesSection.pe_2
          */
      pe: 0
    }
  }
  function yaReponseOuTropTard () {
    if (me.isElapsed) return true
    if (stor.zonenum.reponse() === '') {
      stor.zonenum.focus()
      return false
    }
    if (stor.zoneden.reponse() === '') {
      stor.zoneden.focus()
      return false
    }
    return true
  }
  function isRepOk () {
    const n1 = parseFloat(stor.zonenum.reponse())
    const n2 = parseFloat(stor.zoneden.reponse())
    return ((n1 * stor.denominateur === n2 * stor.numerateur) && (n2 !== 0))
  }
  function initSection () {
    ds = getDonnees()
    me.donneesSection = ds

    me.surcharge()
    ds.nbitems = ds.nbetapes * ds.nbrepetitions
    // Construction de la page
    me.construitStructurePage(ds.structure)

    // DECLARATION DES VARIABLES
    // reponse attendue
    stor.numerateur = 1
    stor.denominateur = 1

    // contiendra soit consigne1 soit consigne2 (1 fois sur 2)
    stor.Consigneegal = ''
    stor.Consignefois = ''
    /*
             Par convention,`
            `   me.typederreurs[0] = nombre de bonnes réponses
                me.typederreurs[1] = nombre de fois où l’élève a utilisé une chance supplémentaire
                me.typederreurs[2] = nombre de mauvaises réponses
                me.typederreurs[10] = nombre de fois où l’élève a eu faux à cause d’un temps limite
                LES AUTRES peuvent être utilisées par la détermination de la pe dans le cas d’une section qualitative
             */
    me.typederreurs = [0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0]

    me.score = 0

    me.afficheTitre('Ecriture fractionnaire dans un produit')

    if (ds.indication) me.indication(me.zones.IG, ds.indication)
    enonceMain()
  }
  function enonceMain () {
    me.videLesZones()

    stor.lesdiv = {}
    stor.lesdiv.conteneur = j3pAddElt(me.zonesElts.MG, 'div', {
      style: me.styles.etendre('toutpetit.enonce', { padding: '10px' })
    })
    stor.lesdiv.consigneG = addDefaultTable(stor.lesdiv.conteneur, 1, 1)[0][0]
    stor.lesdiv.etape = addDefaultTable(stor.lesdiv.conteneur, 1, 1)[0][0]
    stor.lesdiv.travail = addDefaultTable(stor.lesdiv.conteneur, 1, 1)[0][0]
    j3pAddTxt(stor.lesdiv.etape, '&nbsp;')
    stor.lesdiv.correction = j3pAddElt(me.zonesElts.MD, 'div', {
      style: me.styles.etendre('toutpetit.enonce', { padding: '10px' })
    })
    stor.lesdiv.explications = j3pAddElt(stor.lesdiv.conteneur, 'div')
    stor.lesdiv.solution = j3pAddElt(stor.lesdiv.conteneur, 'div')
    stor.lesdiv.solution.style.color = me.styles.toutpetit.correction.color
    stor.lesdiv.explications.style.color = me.styles.cfaux
    me.zonesElts.MG.classList.add('fond')
    stor.lesdiv.travail.classList.add('travail')
    stor.lesdiv.consigneG.classList.add('enonce')

    j3pAffiche(stor.lesdiv.consigneG, null, '<b> <u>' + ds.textes.consigne1 + '</b> </u>')

    const tab = []
    tab[4] = " <TABLE BORDER = 0 style='margin-bottom:0'> <TR><TD ROWSPAN=3 id = 'zoneegal'></TD><TD id = 'zonenumerateur'></TD><TD ROWSPAN=3 id = 'zonefois'></TD></TR><TR><TD BGCOLOR='black' height='2'></TD></TR><TR><TD id = 'zonedenominateur'></TD></TR></TABLE>"
    stor.numerateur = j3pGetRandomInt(101, 200)
    stor.denominateur = j3pGetRandomInt(101, 200)
    if (me.questionCourante % 4 === 1) {
      stor.Consignefois = ds.textes.enoncea1
      stor.Consigneegal = ds.textes.enonceb1
      const ttab = addDefaultTable(stor.lesdiv.travail, 1, 3)
      const tt = afficheFrac(ttab[0][1])
      stor.lesdiv.zoneFois = ttab[0][0]
      stor.lesdiv.zoneNum = tt[0]
      stor.lesdiv.zoneDen = tt[2]
      stor.lesdiv.zoneEgal = ttab[0][2]
    }
    if (me.questionCourante % 4 === 2) {
      stor.Consigneegal = ds.textes.enonceb1
      stor.Consignefois = ds.textes.enoncea2
      const ttab = addDefaultTable(stor.lesdiv.travail, 1, 3)
      const tt = afficheFrac(ttab[0][0])
      stor.lesdiv.zoneFois = ttab[0][1]
      stor.lesdiv.zoneNum = tt[0]
      stor.lesdiv.zoneDen = tt[2]
      stor.lesdiv.zoneEgal = ttab[0][2]
    }
    if (me.questionCourante % 4 === 3) {
      stor.Consigneegal = ds.textes.enonceb2
      stor.Consignefois = ds.textes.enoncea1
      const ttab = addDefaultTable(stor.lesdiv.travail, 1, 3)
      const tt = afficheFrac(ttab[0][2])
      stor.lesdiv.zoneFois = ttab[0][1]
      stor.lesdiv.zoneNum = tt[0]
      stor.lesdiv.zoneDen = tt[2]
      stor.lesdiv.zoneEgal = ttab[0][0]
    }
    if (me.questionCourante % 4 === 0) {
      stor.Consigneegal = ds.textes.enonceb2
      stor.Consignefois = ds.textes.enoncea2
      const ttab = addDefaultTable(stor.lesdiv.travail, 1, 3)
      const tt = afficheFrac(ttab[0][1])
      stor.lesdiv.zoneFois = ttab[0][2]
      stor.lesdiv.zoneNum = tt[0]
      stor.lesdiv.zoneDen = tt[2]
      stor.lesdiv.zoneEgal = ttab[0][0]
    }

    stor.zonenum = new ZoneStyleMathquill1(stor.lesdiv.zoneNum, {
      restric: '0123456789',
      limitenb: 9,
      limite: 5,
      enter: me.sectionCourante.bind(me)

    })
    stor.zoneden = new ZoneStyleMathquill1(stor.lesdiv.zoneDen, {
      restric: '0123456789',
      limitenb: 9,
      limite: 5,
      enter: me.sectionCourante.bind(me)

    })
    stor.zonenum.focus()

    j3pAffiche(stor.lesdiv.zoneFois, null, stor.Consignefois,
      {
        a: stor.numerateur,
        b: stor.denominateur
      })
    j3pAffiche(stor.lesdiv.zoneEgal, null, stor.Consigneegal,
      {
        a: stor.numerateur,
        b: stor.denominateur
      })

    me.finEnonce()
  }

  switch (this.etat) {
    case 'enonce':
      // code exécuté au lancement de la section
      if (this.debutDeLaSection) {
        initSection()
      } else {
        // A chaque répétition, on nettoie la scène
        // A commenter si besoin
        enonceMain()
      }
      break // case "enonce":

    case 'correction': {
      // On teste si une réponse a été saisie
      stor.yaexplik = stor.yaco = false
      j3pEmpty(stor.lesdiv.explications)
      stor.lesdiv.explications.classList.remove('explique')
      stor.lesdiv.solution.classList.remove('correction')
      if (!yaReponseOuTropTard()) {
        // ici on donne le message à afficher si une ou plusieurs zone(s) n’est pas complétée (tout d’abord on met en rouge : styles.cfaux)
        stor.lesdiv.correction.style.color = this.styles.cfaux
        stor.lesdiv.correction.innerHTML = reponseIncomplete
        this.afficheBoutonValider()
      } else {
        // Bonne réponse
        if (isRepOk()) {
          this._stopTimer()
          this.score++
          stor.lesdiv.correction.style.color = this.styles.cbien
          stor.lesdiv.correction.innerHTML = cBien

          stor.zonenum.corrige(true)
          stor.zoneden.corrige(true)
          stor.zonenum.disable()
          stor.zoneden.disable()

          this.typederreurs[0]++
          this.cacheBoutonValider()
          this.etat = 'navigation'
          this.sectionCourante()
        } else {
          // Pas de bonne réponse
          stor.lesdiv.correction.style.color = this.styles.cfaux

          // A cause de la limite de temps :
          if (me.isElapsed) { // limite de temps
            stor.lesdiv.correction.innerHTML = tempsDepasse
            stor.lesdiv.correction.innerHTML += '<br>' + regardeCorrection
            this.cacheBoutonValider()
            this._stopTimer()
            this.typederreurs[10]++
            affcofo(true)

            this.etat = 'navigation'
            this.sectionCourante()
          } else {
            stor.lesdiv.correction.innerHTML = cFaux

            if (this.essaiCourant < this.donneesSection.nbchances) {
              stor.lesdiv.correction.innerHTML += '<br>' + essaieEncore

              affcofo(false)

              this.typederreurs[1]++
            } else {
              this._stopTimer()
              this.cacheBoutonValider()
              affcofo(true)
              this.typederreurs[2]++
              this.etat = 'navigation'
              this.sectionCourante()
            }
          }
        }
      }
      // Obligatoire
      if (stor.yaexplik) stor.lesdiv.explications.classList.add('explique')
      if (stor.yaco) stor.lesdiv.solution.classList.add('correction')

      this.finCorrection()
      break // case "correction":
    }
    case 'navigation':

      if (this.sectionTerminee()) {
        // On détermine la phrase d’état renvoyée par la section
        this.parcours.pe = (this.donneesSection.nbetapes * this.score) / this.donneesSection.nbitems
        this.afficheBoutonSectionSuivante()
        this.focus('sectioncontinuer')
      } else {
        this.etat = 'enonce'
        this.afficheBoutonSuite()
        this.focus('boutoncontinuer')
      }
      // Obligatoire
      this.finNavigation()

      break // case "navigation":
  }
}
