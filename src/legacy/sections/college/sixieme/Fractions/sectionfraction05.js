import { j3pAddElt, j3pAjouteBouton, j3pArrondi, j3pClone, j3pEmpty, j3pGetRandomInt, j3pPGCD, j3pShuffle } from 'src/legacy/core/functions'
import { j3pCreeFenetres, j3pAfficheCroixFenetres, j3pToggleFenetres } from 'src/legacy/core/functionsJqDialog'
import BrouillonCalculs from 'src/legacy/outils/brouillon/BrouillonCalculs'
import ListeDeroulante from 'src/legacy/outils/listeDeroulante/ListeDeroulante'
import { affichefois, afficheFrac } from 'src/legacy/outils/zoneStyleMathquill/functions'
import ZoneStyleMathquill3 from 'src/legacy/outils/zoneStyleMathquill/ZoneStyleMathquill3'
import { addDefaultTable } from 'src/legacy/themes/table'
import { j3pAffiche } from 'src/lib/mathquill/functions'
import textesGeneriques from 'src/lib/core/textes'

const { cBien, cFaux, essaieEncore, reponseIncomplete, tempsDepasse } = textesGeneriques

// une fct qui fait rien
const dummy = () => {}

/**
 * Ajouter une description ici
 * @fileOverview
 * @author Tommy Barroy <t.barroy@laposte.net>
 * @since 2017-10
*/

export const params = {
  outils: ['mathquill', 'calculatrice'],
  /**
   * Ce qui est paramétrable dans l’éditeur de graphe
   * @type {LegacySectionParamsParametres}
   */
  parametres: [
    ['nbrepetitions', 9, 'entier', 'Nombre de répétitions de la section'],
    ['nbchances', 2, 'entier', 'Nombre d’essais possibles.'],
    ['justifier', true, 'boolean', '<u>true</u>: L’élève doit justifier le résultat.'],
    ['Aide', true, 'boolean', '<i>Uniquement quand <b>justifier</b> est à false</i><br><br><u>true</u>: La justification est affichée.'],
    ['detail', true, 'boolean', '<u>true</u>: L’élève doit écrire le calcul.'],
    ['pourcentage', 'oui', 'liste', '<u>oui</u>:  présence de %.', ['oui', 'non', 'les deux']],
    ['Calculatrice', true, 'boolean', '<u>true</u>:  Une calculatrice est disponible'],
    // le modèle gère la compatibilité ascendante avec l’ancien paramètre MiseEnPagePlusClaire
    ['theme', 'standard', 'liste', 'Modèle pour l’affichage', ['standard', 'zonesAvecImageDeFond']]
  ]
}

/**
 * section fraction05
 * @this {Parcours}
 */
export default function main () {
  const me = this
  const stor = me.storage
  let ds = me.donneesSection

  function creeLesDiv () {
    stor.lesdiv = {}
    stor.lesdiv.conteneur = j3pAddElt(me.zonesElts.MG, 'div', {
      style: me.styles.etendre('toutpetit.enonce', { padding: '10px' })
    })
    stor.lesdiv.consigneG = addDefaultTable(stor.lesdiv.conteneur, 1, 1)[0][0]
    stor.lesdiv.etape = addDefaultTable(stor.lesdiv.conteneur, 1, 1)[0][0]
    stor.lesdiv.travail = addDefaultTable(stor.lesdiv.conteneur, 1, 1)[0][0]
    stor.lesdiv.explications = j3pAddElt(stor.lesdiv.conteneur, 'div')
    stor.lesdiv.solution = j3pAddElt(stor.lesdiv.conteneur, 'div')
    stor.lesdiv.calculatrice = j3pAddElt(me.zonesElts.MD, 'div', {
      style: me.styles.etendre('toutpetit.enonce', { padding: '10px' })
    })
    stor.lesdiv.aide = j3pAddElt(me.zonesElts.MD, 'div', {
      style: me.styles.etendre('toutpetit.enonce', { padding: '10px' })
    })
    stor.lesdiv.correction = j3pAddElt(me.zonesElts.MD, 'div', {
      style: me.styles.etendre('toutpetit.enonce', { padding: '10px' })
    })
    stor.lesdiv.etape.style.color = me.styles.toutpetit.correction.color
    stor.lesdiv.solution.style.color = me.styles.toutpetit.correction.color
    stor.lesdiv.explications.style.color = me.styles.cfaux
    me.zonesElts.MG.classList.add('fond')
  }
  function faisChoixExos () {
    const e = ds.lesExos.pop()
    let t3, i
    stor.num = j3pGetRandomInt(2, 98)
    stor.den = 100
    if (e.formule !== '%') {
      t3 = j3pPGCD(stor.num, stor.den, { negativesAllowed: true, valueIfZero: 1 })
      stor.num = Math.round(stor.num / t3)
      stor.den = Math.round(stor.den / t3)
      if (stor.num === 1) {
        stor.affenonce = '$\\frac{' + stor.num + '}{' + stor.den + '}$'
      } else {
        stor.affenonce = ' les $\\frac{' + stor.num + '}{' + stor.den + '}$'
      }
      stor.aff = '$\\frac{' + stor.num + '}{' + stor.den + '}$'
    } else {
      stor.affenonce = '$' + stor.num + '$ %'
      stor.aff = '$\\frac{' + stor.num + '}{' + stor.den + '}$'
    }
    stor.repos = []
    for (i = Math.ceil(e.pb.limite1b * stor.num / stor.den); i < Math.floor(e.pb.limite1h * stor.num / stor.den) + 1; i++) {
      if ((i * stor.den) % stor.num === 0) stor.repos.push(i)
    }
    stor.rep = stor.repos[j3pGetRandomInt(0, stor.repos.length - 1)]
    stor.nombre = stor.rep * stor.den / stor.num
    return e
  }
  function getDonnees () {
    return {

      // Nombre de répétitions de l’exercice
      nbrepetitions: 9,

      justifier: true,
      detail: true,
      pourcentage: 'oui',
      Calculatrice: true,

      // Si indication != "" alors un lien "Indication" s’affiche dans la zone Inférieure Gauche (me.zones.IG
      // une indication<br>de deux lignes..."
      indication: '',
      Aide: true,

      // pour le cas de presentation1bis
      // En effet pas d’indication possible pour cette présentation
      // surchargeindication: false,

      // Nombre de chances dont dispose l’él§ve pour répondre à la question
      nbchances: 2,

      Choix_Phrase: true,
      //  || "presentation2" || "presentation3"  || "presentation1bis"
      structure: 'presentation1bis',

      mespb: [
        { enonce: 'Myriam a mangé $£a$g de son paquet de chips de $£b$ g. <br>  <b>£x du paquet de chips a-t-elle mangé ?</b>', unite: 'g', limite1h: 800, limite1b: 200, cal: 'quantité', repaf: 'Quantité de chips mangée', nombreaf: 'masse du paquet de chips plein' },
        { enonce: 'Dans le collège de Claudio il y a $£b$ élèves,<br> $£a$ élèves ont un chat. <br>  <b>£x d’élèves du collège ont un chat?</b>', unite: 'élèves', limite1h: 800, limite1b: 80, cal: 'nombre d’élèves', repaf: 'Nombre d’élèves ayant un chat', nombreaf: 'Nombre d’élèves du collège' },
        { enonce: 'George a préparé $£b$ cL de potion magique. <br>Il a mis $£a$ cL d’eau dans la potion. <br> <b> £x d’eau contient la potion ?</b>', unite: 'cL', limite1h: 500, limite1b: 10, cal: 'quantité', repaf: 'Quantité d’eau', nombreaf: 'Quantité de potion' },
        { enonce: 'Frédérique achète un vétement à $£b$ €. <br>Il y a une réduction de $£a$€ <br>  <b>£x du prix total représente la réduction ?</b>', unite: '€', limite1h: 300, limite1b: 10, cal: 'réduction', repaf: 'montant de la réduction', nombreaf: 'coût du vétement avant réduction' }
      ]
    }
  }
  function initSection () {
    let i

    ds = getDonnees()
    me.donneesSection = ds
    // On surcharge avec les parametres passés par le graphe
    me.surcharge()

    // Construction de la page
    me.construitStructurePage(ds.structure)

    ds.lesExos = []

    let lp = []
    if ((ds.pourcentage === 'oui') || (ds.pourcentage === 'les deux')) lp.push('%')
    if ((ds.pourcentage === 'non') || (ds.pourcentage === 'les deux')) lp.push('frac')
    lp = j3pShuffle(lp)
    for (i = 0; i < ds.nbitems; i++) {
      ds.lesExos.push({ formule: lp[i % (lp.length)] })
    }
    ds.lesExos = j3pShuffle(ds.lesExos)

    lp = j3pClone(ds.mespb)
    lp = j3pShuffle(lp)

    for (i = 0; i < ds.nbitems; i++) {
      ds.lesExos[i].pb = lp[i % (lp.length)]
    }
    ds.lesExos = j3pShuffle(ds.lesExos)

    me.typederreurs = [0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0]

    me.score = 0

    me.afficheTitre('fraction d’une quantité')

    enonceMain()
  }

  function poseQuestion () {
    me.afficheBoutonValider()
    stor.foBrouill = false
    stor.repAF = '\\text{' + stor.Lexo.pb.repaf + '}'
    stor.nombreAF = '\\text{' + stor.Lexo.pb.nombreaf + '}'
    let tutu = 'Quelle fraction '
    if (stor.Lexo.formule === '%') tutu = 'Quel pourcentage '
    j3pAffiche(stor.lesdiv.consigneG, null, stor.Lexo.pb.enonce.replace('£a', stor.rep).replace('£b', stor.nombre).replace('de  les', 'de').replace('£x', tutu))
    stor.conCont = addDefaultTable(stor.lesdiv.travail, 3, 3)
    if (ds.justifier) {
      faisProd()
      return
    }
    stor.foBrouill = true
    if (ds.detail || stor.Lexo.formule !== '%') {
      faisDetail()
      return
    }
    faisResult()
  }
  function faisDetail () {
    me.afficheBoutonValider()
    j3pEmpty(stor.lesdiv.etape)
    stor.etapeencours = 'detail'
    let bub, bub2, bub3
    if (stor.Lexo.formule === '%') {
      bub2 = 'pourcentage'
      bub = ' \\times 100 '
      bub3 = '<b><u>Étape</u>: détaille le calcul.</b>'
    } else {
      bub2 = 'fraction'
      bub = ''
      bub3 = '<b><u>Étape</u>: écris la fraction.</b>'
    }
    j3pAffiche(stor.lesdiv.etape, null, bub3)
    if (stor.foBrouill) {
      if (ds.Aide) {
        const tabZ = addDefaultTable(stor.conCont[0][0], 1, 7)
        j3pAffiche(tabZ[0][0], null, bub2)
        j3pAffiche(tabZ[0][1], null, '$=$')
        j3pAffiche(tabZ[0][2], null, '$ \\frac{' + stor.repAF + '}{' + stor.nombreAF + '} ' + bub + '$')
      }
      stor.foBrouill = false
    }
    // javais mis 7 colonnes ?
    const tte = addDefaultTable(stor.conCont[1][0], 1, 3)
    j3pAffiche(tte[0][0], null, bub2)
    j3pAffiche(tte[0][1], null, '$=$')
    const t = afficheFrac(tte[0][2])
    stor.zoneDet3 = new ZoneStyleMathquill3(t[2], { restric: '0123456789', hasAutoKeyboard: false, limite: 6, enter: me.sectionCourante.bind(me) })
    if (stor.Lexo.formule === '%') {
      const t2 = affichefois(t[0])
      stor.zoneDet2 = new ZoneStyleMathquill3(t2[0], { restric: '0123456789', hasAutoKeyboard: false, limite: 6, enter: me.sectionCourante.bind(me) })
      stor.zoneDet1 = new ZoneStyleMathquill3(t2[2], { restric: '0123456789', hasAutoKeyboard: false, limite: 6, enter: me.sectionCourante.bind(me) })
    } else {
      stor.zoneDet1 = new ZoneStyleMathquill3(t[0], { restric: '0123456789', hasAutoKeyboard: false, limite: 6, enter: me.sectionCourante.bind(me) })
      stor.zoneDet2 = { disable: dummy, corrige: dummy }
    }
  }
  function faisResult () {
    me.afficheBoutonValider()
    stor.etapeencours = 'reponse'
    let bub2 = 'fraction'
    let bub = ''
    if (stor.Lexo.formule === '%') {
      bub2 = 'pourcentage'
      bub = ' \\times 100 '
    }
    if ((!ds.justifier) && (!ds.detail)) {
      const tty = addDefaultTable(stor.conCont[0][0], 1, 7)
      j3pAffiche(tty[0][0], null, bub2)
      j3pAffiche(tty[0][1], null, '$=$')
      if (ds.Aide) {
        j3pAffiche(tty[0][2], null, '$ \\frac{' + stor.repAF + '}{' + stor.nombreAF + '} ' + bub + '$')
      } else {
        j3pAffiche(tty[0][2], null, '$???$')
      }
      stor.foBrouill = false
    } else {
      j3pEmpty(stor.lesdiv.etape)
      j3pAffiche(stor.lesdiv.etape, null, '<b><u>Étape</u>: donner la réponse</b>.')
    }
    const tabF = addDefaultTable(stor.conCont[2][0], 2, 4)
    stor.brouillon1 = new BrouillonCalculs(
      {
        caseBoutons: tabF[0][0],
        caseEgal: tabF[0][1],
        caseCalculs: tabF[0][2],
        caseApres: tabF[0][3]
      }, {
        limite: 30,
        limitenb: 20,
        restric: '()0123456789+-*/,.',
        hasAutoKeyboard: true,
        lmax: 4
      }
    )
    j3pAffiche(tabF[1][0], null, bub2)
    j3pAffiche(tabF[1][1], null, '$=$')
    if (stor.Lexo.formule === '%') j3pAffiche(tabF[1][3], null, ' %')
    stor.zoneRep = new ZoneStyleMathquill3(tabF[1][2], { restric: '0123456789', hasAutoKeyboard: false, limite: 6, enter: me.sectionCourante.bind(me) })
  }
  function faisProd () {
    me.afficheBoutonValider()
    stor.etapeencours = 'prod'
    j3pAffiche(stor.lesdiv.etape, null, '<b><u>Étape</u>: justifier le calcul</b>.')
    const ttabZ = addDefaultTable(stor.conCont[0][0], 1, 7)
    let bub2 = 'fraction'
    if (stor.Lexo.formule === '%') bub2 = 'pourcentage'
    j3pAffiche(ttabZ[0][0], null, bub2)
    j3pAffiche(ttabZ[0][1], null, '$=$')
    let ll = []
    let bub = ''
    if (stor.Lexo.formule === '%') bub = ' \\times 100 '
    ll.push('$ \\frac{' + stor.repAF + '}{' + stor.nombreAF + '} ' + bub + '$')
    stor.repat = '$ \\frac{' + stor.repAF + '}{' + stor.nombreAF + '} ' + bub + '$'
    ll.push('$ \\frac{' + stor.nombreAF + '}{' + stor.repAF + '} ' + bub + '$')
    ll.push('$' + stor.repAF + '  \\times ' + stor.nombreAF + bub + '$')
    ll.push('$' + stor.nombreAF + '  - ' + stor.repAF + bub + '$')
    ll = j3pShuffle(ll)
    ll.splice(0, 0, 'Choisir')
    stor.listProd = ListeDeroulante.create(ttabZ[0][1], ll)
  }

  function boutonfen () {
    j3pToggleFenetres('Calculatrice')
  }

  function yaReponse () {
    if (stor.etapeencours === 'prod') {
      if (!stor.listProd.changed) {
        stor.listProd.focus()
        return false
      }
      return true
    }
    if (stor.etapeencours === 'detail') {
      if (stor.zoneDet1.reponse() === '') {
        stor.zoneDet1.focus()
        return false
      }
      if (stor.zoneDet3.reponse() === '') {
        stor.zoneDet3.focus()
        return false
      }
      if (stor.Lexo.formule === '%') {
        if (stor.zoneDet2.reponse() === '') {
          stor.zoneDet2.focus()
          return false
        }
      }
      return true
    }
    if (stor.etapeencours === 'reponse') {
      if (stor.zoneRep.reponse() === '') {
        stor.zoneRep.focus()
        return false
      }
      return true
    }
  }
  function isRepOk () {
    stor.errProd = false
    stor.errDen = false
    stor.errNum = false
    stor.errNum2 = false
    stor.errCAL = false

    let ok = true
    let t1, t2, t3

    if (stor.etapeencours === 'prod') {
      if (stor.listProd.reponse !== stor.repat) {
        stor.errProd = true
        return false
      }
      return true
    }
    if (stor.etapeencours === 'detail') {
      t1 = parseFloat(stor.zoneDet1.reponse())
      t3 = parseFloat(stor.zoneDet3.reponse())

      if (stor.Lexo.formule !== '%') {
        if ((t1 / t3 - stor.rep / stor.nombre) < Math.pow(10, -9)) return true
        if (t1 !== stor.rep) {
          stor.errNum = true
          stor.zoneDet1.corrige(false)
          ok = false
        }
        if (t3 !== stor.nombre) {
          stor.errDen = true
          stor.zoneDet3.corrige(false)
          ok = false
        }
        return ok
      }
      if (t3 !== stor.nombre) {
        stor.errDen = true
        stor.zoneDet3.corrige(false)
        ok = false
      }
      t2 = parseFloat(stor.zoneDet2.reponse())
      if ((t2 !== stor.rep) && (t2 !== 100)) {
        stor.errNum = true
        stor.zoneDet2.corrige(false)
        ok = false
      }
      if ((t1 !== stor.rep) && (t1 !== 100)) {
        stor.errNum = true
        stor.zoneDet1.corrige(false)
        ok = false
      }
      if (t1 === t2) {
        stor.errNum2 = true
        stor.zoneDet2.corrige(false)
        ok = false
      }
      return ok
    }
    if (stor.etapeencours === 'reponse') {
      if (stor.Lexo.formule === '%') {
        t1 = parseFloat(stor.zoneRep.reponse())
        if (t1 !== Math.round(stor.num / stor.den * 100)) {
          stor.zoneRep.corrige(false)
          stor.errCAL = true
          return false
        }
      }
      return true
    }

    return ok
  }
  function desactiveAll () {
    if (stor.etapeencours === 'prod') {
      stor.listProd.disable()
    }
    if (stor.etapeencours === 'detail') {
      stor.zoneDet1.disable()
      stor.zoneDet2.disable()
      stor.zoneDet3.disable()
    }
    if (stor.etapeencours === 'reponse') {
      stor.zoneRep.disable()
      stor.brouillon1.disable()
    }
  }
  function passeToutVert () {
    if (stor.etapeencours === 'prod') {
      stor.listProd.corrige(true)
    }
    if (stor.etapeencours === 'detail') {
      stor.zoneDet1.corrige(true)
      stor.zoneDet2.corrige(true)
      stor.zoneDet3.corrige(true)
    }
    if (stor.etapeencours === 'reponse') {
      stor.zoneRep.corrige(true)
    }
  }
  function barrelesfo () {
    if (stor.etapeencours === 'prod') {
      stor.listProd.barre()
    }
    if (stor.etapeencours === 'detail') {
      stor.zoneDet1.barreIfKo()
      stor.zoneDet2.barreIfKo()
      stor.zoneDet3.barreIfKo()
    }
    if (stor.etapeencours === 'reponse') {
      stor.zoneRep.barre()
    }
  }
  function affCorrFaux (isFin) {
    /// //affiche indic

    if (stor.errProd) {
      j3pAffiche(stor.lesdiv.explications, null, 'Ce n’est pas la bonne opération !\n')
      stor.yaexplik = true
    }
    if (stor.errNum) {
      j3pAffiche(stor.lesdiv.explications, null, 'Erreur au numérateur !\n')
      stor.yaexplik = true
    }
    if (stor.errNum2) {
      j3pAffiche(stor.lesdiv.explications, null, 'Tu utilises deux fois le même nombre !\n')
      stor.yaexplik = true
    }
    if (stor.errDen) {
      j3pAffiche(stor.lesdiv.explications, null, 'Erreur au dénominateur !\n')
      stor.yaexplik = true
    }
    if (stor.errCAL) {
      j3pAffiche(stor.lesdiv.explications, null, 'Erreur de calcul !\n')
      stor.yaexplik = true
    }

    if (isFin) {
      let bub = ''
      let bub2 = 'fraction'
      if (stor.Lexo.formule === '%') {
        bub = ' \\times 100'
        bub2 = 'pourcentage'
      }
      // barre les faux
      barrelesfo()
      desactiveAll()
      stor.yaco = true
      // afficheco
      if (stor.etapeencours === 'prod') {
        j3pAffiche(stor.lesdiv.solution, null, bub2 + stor.repat)
      }
      if (stor.etapeencours === 'detail') {
        j3pAffiche(stor.lesdiv.solution, null, bub2 + ' $ = \\frac{' + stor.rep + bub + '}{' + stor.nombre + '}$')
      }
      if (stor.etapeencours === 'reponse') {
        if (stor.Lexo.formule === '%') {
          j3pAffiche(stor.lesdiv.solution, null, bub2 + ' $ = ' + Math.round(stor.num / stor.den * 100) + '$ %')
        }
      }
    } else { me.afficheBoutonValider() }
  }

  function enonceMain () {
    if (((me.questionCourante % ds.nbetapes) === 1) || (ds.nbetapes === 1)) {
      me.videLesZones()
    }

    creeLesDiv()
    stor.lesdiv.travail.classList.add('travail')
    stor.lesdiv.consigneG.classList.add('enonce')
    stor.Lexo = faisChoixExos()
    poseQuestion()
    if (ds.Calculatrice) {
      me.fenetresjq = [{ name: 'Calculatrice', title: 'Une calculatrice', left: 600, top: 10 }]
      j3pCreeFenetres(me)
      j3pAfficheCroixFenetres('Calculatrice')
      j3pAjouteBouton(stor.lesdiv.calculatrice, 'Calculatrice', 'MepBoutons', 'Calculatrice', boutonfen)
    }
    me.afficheBoutonValider()
    me.finEnonce()
  }

  switch (me.etat) {
    case 'enonce':
      // code exécuté au lancement de la section
      if (me.debutDeLaSection) {
        initSection()
      } else {
        enonceMain()
      }
      break // case "enonce":

    case 'correction':
      // On teste si une réponse a été saisie
      stor.yaexplik = false
      stor.yaco = false
      stor.lesdiv.explications.innerHTML = ''
      stor.lesdiv.solution.innerHTML = ''
      stor.lesdiv.explications.classList.remove('explique')
      stor.lesdiv.solution.classList.remove('correction')
      if (yaReponse()) {
        // Bonne réponse
        if (isRepOk()) {
          me._stopTimer()
          stor.lesdiv.correction.style.color = me.styles.cbien
          stor.lesdiv.correction.innerHTML = cBien

          desactiveAll()
          passeToutVert()
          if (stor.etapeencours === 'prod') {
            stor.niv++
            stor.divencours++
            if (ds.detail) {
              faisDetail()
              stor.niv++
              return
            }
            stor.foBrouill = true
            faisResult()
            return
          }
          if (stor.etapeencours === 'detail') {
            if (stor.Lexo.formule === '%') {
              stor.niv++
              faisResult()
              return
            }
          }
          stor.etapeencours = ''

          me.score++
          me.typederreurs[0]++
          me.cacheBoutonValider()
          me.etat = 'navigation'
          me.sectionCourante()
        } else {
          // Pas de bonne réponse
          stor.lesdiv.correction.style.color = me.styles.cfaux
          stor.lesdiv.correction.innerHTML = cFaux
          if (me.isElapsed) {
            // A cause de la limite de temps :
            me._stopTimer()

            stor.lesdiv.correction.innerHTML += tempsDepasse + '<BR> Regarde la correction.'
            me.typederreurs[10]++
            me.cacheBoutonValider()

            affCorrFaux(true)
            stor.yaco = true

            me.etat = 'navigation'
            me.sectionCourante()
          } else {
            // Réponse fausse :
            if (me.essaiCourant < ds.nbchances) {
              stor.lesdiv.correction.innerHTML += '<br>' + essaieEncore

              affCorrFaux(false)

              // affCorrFaux.call(this, false)
              me.typederreurs[1]++
            } else {
              // Erreur au nème essai
              stor.lesdiv.correction.innerHTML += '<BR> Regarde la correction.'

              me._stopTimer()
              me.cacheBoutonValider()

              affCorrFaux(true)
              stor.yaco = true

              if (stor.etapeencours === 'compare') {
                me.score = j3pArrondi(me.score + 0.1, 1)
              }
              if (stor.etapeencours === 'calcul') {
                me.score = j3pArrondi(me.score + 0.4, 1)
              }
              if (stor.etapeencours === 'concl') {
                me.score = j3pArrondi(me.score + 0.7, 1)
              }

              me.typederreurs[2]++
              me.etat = 'navigation'
              me.sectionCourante()
            }
          }
        }
      } else {
        // pas de réponse
        stor.lesdiv.correction.style.color = me.styles.cfaux
        stor.lesdiv.correction.innerHTML = reponseIncomplete
        me.afficheBoutonValider()
      }

      if (stor.yaexplik) stor.lesdiv.explications.classList.add('explique')
      if (stor.yaco) stor.lesdiv.solution.classList.add('correction')
      // Obligatoire
      me.finCorrection()
      break // case "correction":

    case 'navigation':
      if (!me.sectionTerminee()) {
        me.etat = 'enonce'
      }
      me.finNavigation()
      break // case "navigation":
  }
}
