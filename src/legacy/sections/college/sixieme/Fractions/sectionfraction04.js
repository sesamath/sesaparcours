import { j3pAddElt, j3pAjouteBouton, j3pArrondi, j3pEmpty, j3pGetRandomInt, j3pPGCD, j3pShuffle } from 'src/legacy/core/functions'
import { j3pAffiche } from 'src/lib/mathquill/functions'
import ZoneStyleMathquill3 from 'src/legacy/outils/zoneStyleMathquill/ZoneStyleMathquill3'
import { j3pCreeFenetres, j3pAfficheCroixFenetres, j3pToggleFenetres } from 'src/legacy/core/functionsJqDialog'
import { addDefaultTable } from 'src/legacy/themes/table'
import { affichefois, afficheFrac } from 'src/legacy/outils/zoneStyleMathquill/functions'
import textesGeneriques from 'src/lib/core/textes'

const { cBien, cFaux, essaieEncore, reponseIncomplete, tempsDepasse } = textesGeneriques

/**
 * Ajouter une description ici
 * @fileOverview
 * @author Tommy Barroy <t.barroy@laposte.net>
 * @since 2017-10
*/

export const params = {
  outils: ['mathquill', 'calculatrice'],
  /**
   * Ce qui est paramétrable dans l’éditeur de graphe
   * @type {LegacySectionParamsParametres}
   */
  parametres: [
    ['nbrepetitions', 9, 'entier', 'Nombre de répétitions de la section'],
    ['nbchances', 2, 'entier', 'Nombre d’essais possibles.'],
    ['pluspetit', 'les deux', 'liste', '<u>true</u>: Le dénominateur d’arrivée est inférieur au dénominateur de départ.', ['oui', 'non', 'les deux']],
    ['detail', true, 'boolean', '<u>true</u>: L’élève doit écrire le calcul.'],
    ['positifs', true, 'boolean', '<u>true</u>: Les nombres sont forcément positifs.'],
    ['Calculatrice', true, 'boolean', '<u>true</u>:  Une calculatrice est disponible'],
    // le modèle gère la compatibilité ascendante avec l’ancien paramètre MiseEnPagePlusClaire
    ['theme', 'standard', 'liste', 'Modèle pour l’affichage', ['standard', 'zonesAvecImageDeFond']]

  ]
}

/**
 * section fraction04
 * @this {Parcours}
 */
export default function main () {
  const me = this
  let ds = me.donneesSection
  const stor = me.storage

  function creeLesDiv () {
    stor.lesdiv = {}
    stor.lesdiv.conteneur = j3pAddElt(me.zonesElts.MG, 'div', {
      style: me.styles.etendre('toutpetit.enonce', { padding: '10px' })
    })
    stor.lesdiv.consigneG = addDefaultTable(stor.lesdiv.conteneur, 1, 1)[0][0]
    stor.lesdiv.etape = addDefaultTable(stor.lesdiv.conteneur, 1, 1)[0][0]
    stor.lesdiv.travail = addDefaultTable(stor.lesdiv.conteneur, 1, 1)[0][0]
    stor.lesdiv.explications = j3pAddElt(stor.lesdiv.conteneur, 'div')
    stor.lesdiv.solution = j3pAddElt(stor.lesdiv.conteneur, 'div')
    stor.lesdiv.calculatrice = j3pAddElt(me.zonesElts.MD, 'div', {
      style: me.styles.etendre('toutpetit.enonce', { padding: '10px' })
    })
    stor.lesdiv.aide = j3pAddElt(me.zonesElts.MD, 'div', {
      style: me.styles.etendre('toutpetit.enonce', { padding: '10px' })
    })
    stor.lesdiv.correction = j3pAddElt(me.zonesElts.MD, 'div', {
      style: me.styles.etendre('toutpetit.enonce', { padding: '10px' })
    })
    stor.lesdiv.etape.style.color = me.styles.toutpetit.correction.color
    stor.lesdiv.solution.style.color = me.styles.toutpetit.correction.color
    stor.lesdiv.explications.style.color = me.styles.cfaux
    me.zonesElts.MG.classList.add('fond')
  }
  function poseQuestion () {
    me.afficheBoutonValider()
    stor.aff = '$\\frac{' + stor.num + '}{' + stor.den + '}$'
    j3pAffiche(stor.lesdiv.consigneG, null, 'Trouve une fraction égale à $\\frac{' + stor.num + '}{' + stor.den + '}$ avec $' + stor.den2 + '$ au dénominateur.')
    stor.tabCont = addDefaultTable(stor.lesdiv.travail, 2, 1)
    if (ds.detail) {
      faisDetail()
      return
    }
    faisResult()
  }
  function faisDetail () {
    me.afficheBoutonValider()
    j3pEmpty(stor.lesdiv.etape)
    stor.etapeencours = 'detail'
    j3pAffiche(stor.lesdiv.etape, null, '<b><u>Étape</u>:Détaille le calcul.</b>')
    const tabZ = addDefaultTable(stor.tabCont[0][0], 1, 3)
    j3pAffiche(tabZ[0][0], null, stor.aff)
    j3pAffiche(tabZ[0][1], null, '$=$')
    const t = afficheFrac(tabZ[0][2])
    const t2 = affichefois(t[0])
    const t3 = affichefois(t[2])
    stor.zoneDet3 = new ZoneStyleMathquill3(t3[2], { restric: stor.restric, hasAutoKeyboard: false, limite: 6, enter: me.sectionCourante.bind(me) })
    stor.zoneDet2 = new ZoneStyleMathquill3(t2[0], { restric: stor.restric, hasAutoKeyboard: false, limite: 6, enter: me.sectionCourante.bind(me) })
    stor.zoneDet1 = new ZoneStyleMathquill3(t2[2], { restric: stor.restric, hasAutoKeyboard: false, limite: 6, enter: me.sectionCourante.bind(me) })
    stor.zoneDet4 = new ZoneStyleMathquill3(t3[0], { restric: stor.restric, hasAutoKeyboard: false, limite: 6, enter: me.sectionCourante.bind(me) })
  }
  function faisResult () {
    me.afficheBoutonValider()
    stor.etapeencours = 'reponse'
    if (ds.detail) {
      j3pEmpty(stor.lesdiv.etape)
      j3pAffiche(stor.lesdiv.etape, null, '<b><u>Étape</u>:Termine le calcul.</b>')
    }
    const TabR = addDefaultTable(stor.tabCont[1][0], 1, 3)
    j3pAffiche(TabR[0][0], null, stor.aff)
    j3pAffiche(TabR[0][1], null, '$=$')
    const t = afficheFrac(TabR[0][2])
    stor.zoneRepNum = new ZoneStyleMathquill3(t[0], { restric: stor.restric, hasAutoKeyboard: false, limite: 6, enter: me.sectionCourante.bind(me) })
    stor.zoneRepDen = new ZoneStyleMathquill3(t[2], { restric: stor.restric, hasAutoKeyboard: false, limite: 6, enter: me.sectionCourante.bind(me) })
  }

  function boutonfen () {
    j3pToggleFenetres('Calculatrice')
  }
  function premsavec (n) {
    let p
    do {
      p = j3pGetRandomInt(2, 20)
    } while (j3pPGCD(n, p, { negativesAllowed: true, valueIfZero: 1 }) !== 1)
    return p
  }
  function faisChoixExos () {
    const e = ds.lesExos.pop()
    stor.restric = '0123456789'
    if (e.signe === -1) stor.restric += '-'
    let num, den
    stor.num = j3pGetRandomInt(1, 20)
    stor.num = e.signe * stor.num
    stor.den = premsavec(stor.num)
    stor.mul = j3pGetRandomInt(2, 12)
    stor.num2 = stor.num * stor.mul
    stor.den2 = stor.den * stor.mul
    if (e.formule !== '+') {
      num = stor.num
      den = stor.den
      stor.num = stor.num2
      stor.den = stor.den2
      stor.num2 = num
      stor.den2 = den
    }
    return e
  }
  function getDonnees () {
    return {

      // Nombre de répétitions de l’exercice
      nbrepetitions: 9,

      pluspetit: 'les deux',
      detail: true,
      positifs: true,
      Calculatrice: true,

      // Si indication != "" alors un lien "Indication" s’affiche dans la zone Inférieure Gauche (this.zones.IG
      // une indication<br>de deux lignes..."
      indication: '',

      // pour le cas de presentation1bis
      // En effet pas d’indication possible pour cette présentation
      // surchargeindication: false,

      // Nombre de chances dont dispose l’él§ve pour répondre à la question
      nbchances: 2,

      Choix_Phrase: true,
      //  || "presentation2" || "presentation3"  || "presentation1bis"
      structure: 'presentation1bis',

      mespb: [
        { enonce: 'Myriam a mangé £a de son paquet de chips de $£b$ g. <br>  Quelle quantité de chips a-t-elle mangée ?', unite: 'g', limite1h: 800, limite1b: 200, cal: 'quantité' },
        { enonce: 'Dans le collège de $£b$ élèves de Claudio,<br> £a des enfants ont un animal de compagnie. <br>  Combien d’élèves ont un animal de compagnie ?', unite: 'élèves', limite1h: 800, limite1b: 80, cal: 'nombre d’élèves' },
        { enonce: 'George a préparé $£b$ cL de potion magique. <br>L’eau représente £a de la potion. <br>  Quelle quantité d’eau contient la potion ?', unite: 'cL', limite1h: 100, limite1b: 10, cal: 'quantité' },
        { enonce: 'Frédérique achète un vétement à $£b$ €. <br>Il y a une réduction de £a <br>  Quel est le montant de la réduction ?', unite: '€', limite1h: 100, limite1b: 10, cal: 'réduction' }
      ]
    }
  }
  function initSection () {
    let i
    ds = getDonnees()
    me.donneesSection = ds
    // On surcharge avec les parametres passés par le graphe
    me.surcharge()

    // Construction de la page
    me.construitStructurePage(me.donneesSection.structure)

    me.donneesSection.nbetapes = 1
    me.donneesSection.nbitems = me.donneesSection.nbetapes * me.donneesSection.nbrepetitions
    me.donneesSection.lesExos = []

    let lp = []
    if ((ds.pluspetit === 'oui') || (ds.pluspetit === 'les deux')) lp.push('-')
    if ((ds.pluspetit === 'non') || (ds.pluspetit === 'les deux')) lp.push('+')
    lp = j3pShuffle(lp)
    for (i = 0; i < me.donneesSection.nbitems; i++) {
      me.donneesSection.lesExos.push({ formule: lp[i % (lp.length)] })
    }
    me.donneesSection.lesExos = j3pShuffle(me.donneesSection.lesExos)

    lp = [1]
    if (!ds.positifs) { lp.push(-1) }
    lp = j3pShuffle(lp)
    for (i = 0; i < me.donneesSection.nbitems; i++) {
      me.donneesSection.lesExos[i].signe = lp[i % lp.length]
    }
    me.donneesSection.lesExos = j3pShuffle(me.donneesSection.lesExos)

    me.typederreurs = [0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0]

    me.score = 0

    me.afficheTitre('Changer de dénominateur')
    enonceMain()
  }

  function yaReponse () {
    if (stor.etapeencours === 'detail') {
      if (stor.zoneDet1.reponse() === '') {
        stor.zoneDet1.focus()
        return false
      }
      if (stor.zoneDet2.reponse() === '') {
        stor.zoneDet2.focus()
        return false
      }
      if (stor.zoneDet3.reponse() === '') {
        stor.zoneDet3.focus()
        return false
      }
      if (stor.zoneDet4.reponse() === '') {
        stor.zoneDet4.focus()
        return false
      }
      return true
    }
    if (stor.etapeencours === 'reponse') {
      if (stor.zoneRepNum.reponse() === '') {
        stor.zoneRepNum.focus()
        return false
      }
      if (stor.zoneRepDen.reponse() === '') {
        stor.zoneRepDen.focus()
        return false
      }
      return true
    }
  }
  function isRepOk () {
    stor.errHautBas = false
    stor.errFracDep = false
    stor.errPourArriv = false
    stor.errFracAtt = false
    stor.errCalDen = false
    stor.errCalc = false
    stor.errDen = false

    let ok = true
    let t1, t2, t3, t4, t5, t6
    let n1, n2

    if (stor.etapeencours === 'detail') {
      t1 = parseFloat(stor.zoneDet1.reponse())
      t2 = parseFloat(stor.zoneDet2.reponse())
      t3 = parseFloat(stor.zoneDet3.reponse())
      t4 = parseFloat(stor.zoneDet4.reponse())

      // 1 2 en haut
      // si mode + grand
      if (stor.Lexo.formule === '+') {
        if ((t3 !== stor.den) && (t4 !== stor.den)) {
          stor.errFracDep = true
          stor.zoneDet3.corrige(false)
          stor.zoneDet4.corrige(false)
          ok = false
        }
        if ((t1 !== stor.num) && (t2 !== stor.num)) {
          stor.errFracDep = true
          stor.zoneDet1.corrige(false)
          stor.zoneDet2.corrige(false)
          ok = false
        }
        if (!ok) return false
        if (t1 === stor.num) { t5 = t2; n1 = stor.zoneDet2 } else { t5 = t1; n1 = stor.zoneDet1 }
        if (t3 === stor.den) { t6 = t4; n2 = stor.zoneDet4 } else { t6 = t3; n2 = stor.zoneDet3 }
        if (t5 !== t6) {
          stor.errHautBas = true
          n1.corrige(false)
          n2.corrige(false)
          ok = false
        }
        if (t3 * t4 !== stor.den2) {
          stor.errPourArriv = true
          n2.corrige(false)
          ok = false
        }
        return ok
      } else {
        if ((t3 !== stor.den2) && (t4 !== stor.den2)) {
          stor.errFracAtt = true
          stor.zoneDet3.corrige(false)
          stor.zoneDet4.corrige(false)
          ok = false
        }
        if (!ok) return false
        if (t3 * t4 !== stor.den) {
          stor.errCalDen = true
          stor.zoneDet3.corrige(false)
          stor.zoneDet4.corrige(false)
        }
        if (t3 === stor.den2) { t6 = t4; n2 = stor.zoneDet4 } else { t6 = t3; n2 = stor.zoneDet3 }
        if ((t1 !== t6) && (t2 !== t6)) {
          stor.errHautBas = true
          stor.zoneDet1.corrige(false)
          stor.zoneDet2.corrige(false)
          ok = false
        }
        if (!ok) return false
        if ((t1 !== stor.num2) && (t2 !== stor.num2)) {
          stor.errCalc = true
          if (t1 !== t6) stor.zoneDet1.corrige(false)
          if ((t2 !== t6) || (t1 === t6)) stor.zoneDet2.corrige(false)
          ok = false
        }
        return ok
      }
    }
    if (stor.etapeencours === 'reponse') {
      t1 = parseFloat(stor.zoneRepDen.reponse())
      t2 = parseFloat(stor.zoneRepNum.reponse())

      if (t1 !== stor.den2) {
        stor.errDen = true
        stor.zoneRepDen.corrige(false)
        ok = false
      }
      if (!ok) return false
      if (t2 !== stor.num2) {
        stor.errCalc = true
        stor.zoneRepNum.corrige(false)
        ok = false
      }
      return ok
    }
  } // isRepOk
  // utilise this
  function desactiveAll () {
    if (stor.etapeencours === 'detail') {
      stor.zoneDet1.disable()
      stor.zoneDet2.disable()
      stor.zoneDet3.disable()
      stor.zoneDet4.disable()
    }
    if (stor.etapeencours === 'reponse') {
      stor.zoneRepNum.disable()
      stor.zoneRepDen.disable()
    }
  } // desactiveAll
  function passeToutVert () {
    if (stor.etapeencours === 'detail') {
      stor.zoneDet1.corrige(true)
      stor.zoneDet2.corrige(true)
      stor.zoneDet3.corrige(true)
      stor.zoneDet4.corrige(true)
    }
    if (stor.etapeencours === 'reponse') {
      stor.zoneRepNum.corrige(true)
      stor.zoneRepDen.corrige(true)
    }
  } // passeToutVert
  function barrelesfo () {
    if (stor.etapeencours === 'detail') {
      stor.zoneDet1.barreIfKo()
      stor.zoneDet2.barreIfKo()
      stor.zoneDet3.barreIfKo()
      stor.zoneDet4.barreIfKo()
    }
    if (stor.etapeencours === 'reponse') {
      stor.zoneRepNum.barreIfKo()
      stor.zoneRepDen.barreIfKo()
    }
  }

  function affCorrFaux (isFin) {
    /// //affiche indic

    if (stor.errHautBas) {
      j3pAffiche(stor.lesdiv.explications, null, 'On devrait trouver le même facteur en haut et en bas !\n')
      stor.yaexplik = true
    }
    if (stor.errFracDep) {
      j3pAffiche(stor.lesdiv.explications, null, 'Tu dois utiliser la fraction de départ !\n')
      stor.yaexplik = true
    }
    if (stor.errPourArriv) {
      j3pAffiche(stor.lesdiv.explications, null, "Ce calcul ne donnera pas $'+d.den2+'$ au dénominateur !\n")
      stor.yaexplik = true
    }
    if (stor.errFracAtt) {
      j3pAffiche(stor.lesdiv.explications, null, 'Ce calcul ne permettra pas de faire apparaitre $' + stor.den2 + '$ au dénominateur!\n')
      stor.yaexplik = true
    }
    if (stor.errCalDen) {
      j3pAffiche(stor.lesdiv.explications, null, 'Je ne comprends pas le produit au dénominateur !\n')
      stor.yaexplik = true
    }
    if (stor.errCalc) {
      j3pAffiche(stor.lesdiv.explications, null, 'Il y a une erreur de calcul !\n')
      stor.yaexplik = true
    }
    if (stor.errDen) {
      j3pAffiche(stor.lesdiv.explications, null, 'Ce n’est pas le dénominateur demandé !\n')
      stor.yaexplik = true
    }

    if (isFin) {
      // barre les faux
      barrelesfo()
      desactiveAll()
      stor.yaco = true
      // afficheco
      if (stor.etapeencours === 'detail') {
        if (stor.Lexo.formule === '+') {
          j3pAffiche(stor.lesdiv.solution, null, stor.aff + '$ = \\frac{' + stor.num + ' \\times ' + stor.mul + '}{' + stor.den + ' \\times ' + stor.mul + '}$')
        } else {
          j3pAffiche(stor.lesdiv.solution, null, stor.aff + '$ = \\frac{' + stor.num2 + ' \\times ' + stor.mul + '}{' + stor.den2 + ' \\times ' + stor.mul + '}$')
        }
      }
      if (stor.etapeencours === 'reponse') {
        j3pAffiche(stor.lesdiv.solution, null, stor.aff + '$ = \\frac{' + stor.num2 + '}{' + stor.den2 + '}$')
      }
    } else { me.afficheBoutonValider() }
  } // affCorrFaux

  function enonceMain () {
    if (me.etapeCourante === 1) {
      me.videLesZones()
    }
    creeLesDiv()
    stor.lesdiv.travail.classList.add('travail')
    stor.lesdiv.consigneG.classList.add('enonce')
    stor.Lexo = faisChoixExos()
    poseQuestion()
    if (ds.Calculatrice) {
      me.fenetresjq = [{ name: 'Calculatrice', title: 'Une calculatrice', left: 600, top: 10 }]
      j3pCreeFenetres(me)
      j3pAfficheCroixFenetres('Calculatrice')
      j3pAjouteBouton(stor.lesdiv.calculatrice, 'Calculatrice', 'MepBoutons', 'Calculatrice', boutonfen)
    }
    me.afficheBoutonValider()
    me.finEnonce()
  }

  switch (this.etat) {
    case 'enonce':
      // code exécuté au lancement de la section
      if (me.debutDeLaSection) {
        initSection()
      } else {
        enonceMain()
      }
      break // case "enonce":

    case 'correction':
      // On teste si une réponse a été saisie
      stor.yaexplik = false
      stor.yaco = false
      stor.lesdiv.explications.innerHTML = ''
      stor.lesdiv.solution.innerHTML = ''
      stor.lesdiv.explications.classList.remove('explique')
      stor.lesdiv.solution.classList.remove('correction')

      if (yaReponse()) {
        // Bonne réponse
        if (isRepOk()) {
          this._stopTimer()
          stor.lesdiv.correction.style.color = this.styles.cbien
          stor.lesdiv.correction.innerHTML = cBien

          desactiveAll()
          passeToutVert()
          if (stor.etapeencours === 'detail') {
            stor.niv++
            faisResult()
            return
          }
          stor.etapeencours = ''

          this.score++
          this.typederreurs[0]++
          this.cacheBoutonValider()
          this.etat = 'navigation'
          this.sectionCourante()
        } else {
          // Pas de bonne réponse
          stor.lesdiv.correction.style.color = this.styles.cfaux
          stor.lesdiv.correction.innerHTML = cFaux
          if (this.isElapsed) {
            // A cause de la limite de temps :
            this._stopTimer()

            stor.lesdiv.correction.innerHTML += tempsDepasse + '<BR>'
            this.typederreurs[10]++
            this.cacheBoutonValider()

            affCorrFaux(true)
            stor.yaco = true

            this.etat = 'navigation'
            this.sectionCourante()
          } else {
            // Réponse fausse :
            if (me.essaiCourant < me.donneesSection.nbchances) {
              stor.lesdiv.correction.innerHTML += '<br>' + essaieEncore

              affCorrFaux(false)

              // affCorrFaux.call(this, false)
              this.typederreurs[1]++
            } else {
              // Erreur au nème essai
              this._stopTimer()
              this.cacheBoutonValider()

              affCorrFaux(true)
              stor.yaco = true

              if (stor.etapeencours === 'compare') {
                this.score = j3pArrondi(this.score + 0.1, 1)
              }
              if (stor.etapeencours === 'calcul') {
                this.score = j3pArrondi(this.score + 0.4, 1)
              }
              if (stor.etapeencours === 'concl') {
                this.score = j3pArrondi(this.score + 0.7, 1)
              }

              this.typederreurs[2]++
              this.etat = 'navigation'
              this.sectionCourante()
            }
          }
        }
      } else {
        // pas de réponse
        stor.lesdiv.correction.style.color = this.styles.cfaux
        stor.lesdiv.correction.innerHTML = reponseIncomplete
        this.afficheBoutonValider()
      }

      if (stor.yaexplik) stor.lesdiv.explications.classList.add('explique')
      if (stor.yaco) stor.lesdiv.solution.classList.add('correction')

      // Obligatoire
      this.finCorrection()
      break // case "correction":

    case 'navigation':
      if (!this.sectionTerminee()) {
        this.etat = 'enonce'
      }
      this.finNavigation()
      break // case "navigation":
  }
}
