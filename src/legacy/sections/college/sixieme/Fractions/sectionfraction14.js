import { j3pAddElt, j3pAjouteBouton, j3pArrondi, j3pClone, j3pEmpty, j3pGetRandomInt, j3pPGCD, j3pShuffle, j3pGetNewId, j3pGetRandomBool, j3pDetruit } from 'src/legacy/core/functions'
import { j3pAffiche } from 'src/lib/mathquill/functions'
import { j3pCreeFenetres, j3pAfficheCroixFenetres, j3pToggleFenetres } from 'src/legacy/core/functionsJqDialog'
import { addDefaultTable } from 'src/legacy/themes/table'
import ListeDeroulante from 'src/legacy/outils/listeDeroulante/ListeDeroulante'
import ZoneStyleMathquill2 from 'src/legacy/outils/zoneStyleMathquill/ZoneStyleMathquill2'
import BrouillonCalculs from 'src/legacy/outils/brouillon/BrouillonCalculs'
import { ecrisBienMathquill } from 'src/legacy/outils/zoneStyleMathquill/functions'
import textesGeneriques from 'src/lib/core/textes'

const { cBien, cFaux, essaieEncore, reponseIncomplete, tempsDepasse } = textesGeneriques

/**
 * Ajouter une description ici
 * @fileOverview
 * @author Tommy Barroy <t.barroy@laposte.net>
 * @since 2017-10
*/

export const params = {
  outils: ['mathquill', 'calculatrice'],
  /**
   * Ce qui est paramétrable dans l’éditeur de graphe
   * @type {LegacySectionParamsParametres}
   */
  parametres: [
    ['nbrepetitions', 3, 'entier', 'Nombre de répétitions de la section'],
    ['nbchances', 1, 'entier', 'Nombre d’essais possibles'],
    ['Donnees', 'fractions', 'liste', 'La proportion est donnée avec une fraction ou un pourcentage ou les deux.', ['fractions', 'poucentages', 'les deux']],
    ['Justifie', true, 'boolean', '<u>true</u>: L’élève dispose d’une calculatrice.'],
    ['Calculatrice', true, 'boolean', '<u>true</u>: L’élève dispose d’une calculatrice.'],
    ['theme', 'standard', 'liste', 'Modèle pour l’affichage', ['standard', 'zonesAvecImageDeFond']]
  ]
}

export function upgradeParametres (parametres) {
  // Donnees était un booléen avant de devenir une liste
  if (typeof parametres.Donnees === 'boolean') parametres.Donnees = parametres.Donnees ? 'fractions' : 'poucentages'
}

/**
 * section fraction14
 * @this {Parcours}
 */
export default function main () {
  const me = this
  const stor = me.storage
  let ds = me.donneesSection

  function getDonnees () {
    return {
      // Nombre de répétitions de l’exercice
      nbrepetitions: 3,
      Justifie: true,
      Situation: true,
      Calculatrice: true,
      Donnees: 'les deux',
      Pourcentage: true,
      // Si indication != "" alors un lien "Indication" s’affiche dans la zone Inférieure Gauche (me.zones.IG
      // une indication<br>de deux lignes..."
      indication: '',

      // pour le cas de presentation1bis
      // En effet pas d’indication possible pour cette présentation
      // surchargeindication: false,

      // Nombre de chances dont dispose l’él§ve pour répondre à la question
      nbchances: 1,

      //  || "presentation2" || "presentation3"  || "presentation1bis"
      structure: 'presentation1bis',
      pb: [
        {
          ncompt: 'nombre de filles',
          ntot: 'nombre d’élèves',
          min: 16,
          max: 37,
          prop: 'Un propriétaire terrien a vendu £a $£p$ de sa propriété en 2001, <br> et £b $£q$ du reste en 2002.<br> La partie invendue au bout des deux années représente $£c$ hectares.',
          prop2: 'la fraction de la propriété vendue en 2002',
          prop3: 'Fraction de la propriété vendue en 2002',
          prop4: 'la fraction de la propriété invendue en 2002',
          prop5: 'Fraction de la propriété invendue en 2002',
          prop6: 'la superficie intiale de la propriété',
          prop7: 'Superficie intiale de la propriété',
          prop8: 'la fraction de la propriété vendue en 2001',
          unite: 'ha'
        }
        // { ncompt: 'nombre de filles', ntot: "nombre d’élèves", min: 16, max: 37, sit: 'Dans la classe de £b, il y a $£a$ garçons et $£d$ filles.', prop: 'Dans la classe de £b, $£p$ des élèves sont des garçons.', cal: 'la proportion de garçons dans la classe de £b' },
        // { ncompt: 'nombre de poules rousses', ntot: 'nombre total de poules', min: 16, max: 37, sit: 'Dans la ferme de £b, il y a $£c$ poules, dont $£a$ sont rousses.', prop: 'Dans la ferme de £b, $£p$ des poules sont rousses.', cal: 'la proportion de poules rousses dans la ferme de £b' },
        // { ncompt: 'nombre de poules rousses', ntot: 'nombre total de poules', min: 16, max: 37, sit: 'Dans la ferme de £b, il y a $£a$ poules rousses et $£d$ poules brunes.', prop: 'Dans la ferme de £b, $£p$ des poules sont rousses.', cal: 'la proportion de poules rousses dans la ferme de £b' },
        // { ncompt: 'nombre de figurines en métal', ntot: 'nombre total de figurines', min: 5, max: 50, sit: 'Dans la chambre de £b, il y a $£a$ figurines en métal sur $£c$ figurines.', prop: 'Dans la chambre de £b, $£p$ des figurines sont en métal.', cal: 'la proportion de figurines en métal dans la chambre de £b' }
      ],
      // Par défaut, pas de temps limite. Décommentez pour avoir un temps limite par défaut. En secondes.
      pren: ['Moussa', 'Chloée', 'Llyn', 'Mathys', 'Rémi', 'Nathéo', 'Charlie', 'Félix', 'Daniel', 'Cathy', 'Murielle', 'Stéphanie', 'Célita']
    }
  }

  function initSection () {
    ds = getDonnees()
    me.donneesSection = ds
    // On surcharge avec les parametres passés par le graphe
    me.surcharge()
    me.validOnEnter = false // ex donneesSection.touche_entree

    // Construction de la page
    me.construitStructurePage(me.donneesSection.structure)

    me.donneesSection.nbetapes = 3
    stor.afaire = ['reste', 'invendu', 'superficie']
    if (ds.Justifie) {
      stor.afaire = ['Jusreste', 'reste', 'Jusinvendu', 'invendu', 'Jussuperficie', 'superficie']
      me.donneesSection.nbetapes = 6
    }
    stor.encours = stor.afaire[stor.afaire.length - 1]
    me.donneesSection.nbitems = 3 * me.donneesSection.nbrepetitions

    ds.lesExos = []

    let lp = [0, 1, 2]
    lp = j3pShuffle(lp)
    for (let i = 0; i < me.donneesSection.nbitems; i++) {
      ds.lesExos.push({ cas: lp[i % lp.length] })
    }
    ds.lesExos = j3pShuffle(ds.lesExos)

    lp = []
    if (ds.Donnees === 'fractions' || ds.Donnees === 'les deux') lp.push('f')
    if (ds.Donnees === 'pourcentages' || ds.Donnees === 'les deux') lp.push('p')
    let casPos = []
    for (let i = 0; i < lp.length; i++) {
      for (let j = 0; j < lp.length; j++) {
        casPos.push(lp[i] + lp[j])
      }
    }
    casPos = j3pShuffle(casPos)
    for (let i = 0; i < me.donneesSection.nbitems; i++) {
      ds.lesExos[i].don = casPos[i % casPos.length]
    }
    ds.lesExos = j3pShuffle(ds.lesExos)

    lp = j3pShuffle(j3pClone(ds.pb))
    for (let i = 0; i < me.donneesSection.nbitems; i++) {
      ds.lesExos[i].pb = j3pClone(lp[i % lp.length])
    }
    ds.lesExos = j3pShuffle(ds.lesExos)

    lp = j3pShuffle(j3pClone(ds.pren))
    for (let i = 0; i < me.donneesSection.nbitems; i++) {
      ds.lesExos[i].pren1 = lp[i % lp.length]
      do {
        ds.lesExos[i].pren2 = lp[j3pGetRandomInt(0, lp.length - 1)]
      } while (ds.lesExos[i].pren1 === ds.lesExos[i].pren2)
    }
    ds.lesExos = j3pShuffle(ds.lesExos)

    me.typederreurs = [0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0]

    me.score = 0
    const tt = 'Partie d’une partie'

    me.afficheTitre(tt)

    enonceMain()
  }

  function faisChoixExos () {
    const e = ds.lesExos.pop()
    stor.b1 = e.pren1
    stor.b2 = e.pren2
    switch (e.don) {
      case 'pp':
        stor.don1t = stor.don2t = 'p'
        stor.don1 = e.pb.sit
        stor.don2 = e.pb.sit
        stor.c1 = stor.c2 = 100
        stor.a1 = j3pGetRandomInt(2, stor.c1 - 2)
        stor.a2 = j3pGetRandomInt(2, stor.c1 - 2)
        stor.d1 = stor.c1 - stor.a1
        stor.d2 = stor.c2 - stor.a2
        stor.p1 = stor.a1 + ' %'
        stor.p2 = stor.a2 + ' %'
        break
      case 'ff':
        stor.don1t = stor.don2t = 'f'
        stor.don1 = e.pb.prop
        stor.don2 = e.pb.prop
        stor.c1 = j3pGetRandomInt(e.pb.min, e.pb.max)
        stor.c2 = j3pGetRandomInt(e.pb.min, e.pb.max)
        stor.a1 = j3pGetRandomInt(2, stor.c1 - 2)
        stor.a2 = j3pGetRandomInt(2, stor.c2 - 2)
        stor.d1 = stor.c1 - stor.a1
        stor.d2 = stor.c2 - stor.a2
        stor.p1 = '\\frac{' + j3pArrondi(stor.a1 / j3pPGCD(stor.a1, stor.c1), 0) + '}{' + j3pArrondi(stor.c1 / j3pPGCD(stor.a1, stor.c1), 0) + '}'
        stor.p2 = '\\frac{' + j3pArrondi(stor.a2 / j3pPGCD(stor.a2, stor.c2), 0) + '}{' + j3pArrondi(stor.c2 / j3pPGCD(stor.a2, stor.c2), 0) + '}'
        break
      case 'pf':
      case 'fp':
        stor.don1 = e.pb.prop
        stor.don2 = e.pb.prop
        stor.don1t = stor.don2t = 'f'
        if (j3pGetRandomBool()) {
          stor.don1t = 'p'
        } else {
          stor.don2t = 'p'
        }
        if (stor.don1t === 'p') { stor.c1 = 100 } else {
          stor.c1 = j3pGetRandomInt(e.pb.min, e.pb.max)
        }
        if (stor.don2t === 'p') { stor.c2 = 100 } else {
          stor.c2 = j3pGetRandomInt(e.pb.min, e.pb.max)
        }
        stor.a1 = j3pGetRandomInt(2, stor.c1 - 2)
        stor.a2 = j3pGetRandomInt(2, stor.c2 - 2)
        stor.d1 = stor.c1 - stor.a1
        stor.d2 = stor.c2 - stor.a2
        stor.p2 = '\\frac{' + j3pArrondi(stor.a2 / j3pPGCD(stor.a2, stor.c2), 0) + '}{' + j3pArrondi(stor.c2 / j3pPGCD(stor.a2, stor.c2), 0) + '}'
        stor.p1 = '\\frac{' + j3pArrondi(stor.a1 / j3pPGCD(stor.a1, stor.c1), 0) + '}{' + j3pArrondi(stor.c1 / j3pPGCD(stor.a1, stor.c1), 0) + '}'
        if (stor.don1t === 'p') stor.p1 = stor.a1 + ' %'
        if (stor.don2t === 'p') stor.p2 = stor.a2 + ' %'
        break
    }
    stor.foco1 = stor.foco2 = stor.foco3 = stor.foco4 = stor.foco5 = false
    stor.fracreste = [stor.c1 * stor.a2 - stor.a1 * stor.a2, stor.c1 * stor.c2]
    stor.fracreste2 = [stor.c1 * stor.c2 - stor.a1 * stor.c2 + stor.a1 * stor.a2 - stor.c1 * stor.a2, stor.c1 * stor.c2]
    const hum = j3pPGCD(stor.fracreste2[0], stor.fracreste2[1])
    stor.focadiv = j3pArrondi(stor.fracreste2[0] / hum, 0)
    stor.c = j3pGetRandomInt(2, 10) * stor.focadiv
    stor.finfin = j3pArrondi(stor.c * stor.fracreste2[1] / stor.fracreste2[0], 0)
    return e
  }

  function poseQuestion () {
    if (stor.foco1) {
      j3pEmpty(stor.douko)
      j3pAffiche(stor.douko, null, stor.repat)
      stor.douko.style.color = me.styles.petit.correction.color
      stor.foco1 = false
    }
    if (stor.foco2) {
      j3pEmpty(stor.douko)
      j3pAffiche(stor.douko, null, stor.kekoi)
      stor.douko.style.color = me.styles.petit.correction.color
      stor.foco2 = false
    }
    if (stor.foco3) {
      j3pEmpty(stor.douko)
      j3pAffiche(stor.douko, null, stor.repat)
      stor.douko.style.color = me.styles.petit.correction.color
      stor.foco3 = false
    }
    if (stor.foco4) {
      j3pEmpty(stor.douko)
      j3pAffiche(stor.douko, null, stor.kekoi)
      stor.douko.style.color = me.styles.petit.correction.color
      stor.foco4 = false
    }
    if (stor.foco5) {
      j3pEmpty(stor.douko)
      j3pAffiche(stor.douko, null, stor.repat)
      stor.douko.style.color = me.styles.petit.correction.color
      stor.foco5 = false
    }
    if (!stor.yaeudeb) {
      stor.yaeudeb = true
      const deva = (stor.don1t === 'f') ? 'les' : ''
      const devb = (stor.don2t === 'f') ? 'les' : ''
      j3pAffiche(stor.lesdiv.consigne1, j3pGetNewId(), stor.Lexo.pb.prop + '\n', { p: stor.p1, q: stor.p2, a: deva, b: devb, c: ecrisBienMathquill(stor.c) })
    }
    j3pEmpty(stor.lesdiv.consigne2)
    if (stor.encours === 'Jusreste') {
      const tt = '<b>Détermine le calcul pour obtenir ' + stor.Lexo.pb.prop2 + '.</b>'
      j3pAffiche(stor.lesdiv.consigne2, null, tt)
      stor.lesdiv.consigne2.style.color = me.styles.petit.correction.color
      const tty = addDefaultTable(stor.lesdiv.travail, 1, 3)
      j3pAffiche(tty[0][0], null, stor.Lexo.pb.prop3 + '$=$&nbsp;')
      let p1, p2
      if (stor.don1t === 'p') { p1 = '\\frac{' + stor.a1 + '}{100}' } else { p1 = '\\frac{' + j3pArrondi(stor.a1 / j3pPGCD(stor.a1, stor.c1), 0) + '}{' + j3pArrondi(stor.c1 / j3pPGCD(stor.a1, stor.c1), 0) + '}' }
      if (stor.don2t === 'p') { p2 = '\\frac{' + stor.a2 + '}{100}' } else { p2 = '\\frac{' + j3pArrondi(stor.a2 / j3pPGCD(stor.a2, stor.c2), 0) + '}{' + j3pArrondi(stor.c2 / j3pPGCD(stor.a2, stor.c2), 0) + '}' }
      const rep1 = '$(1 - ' + p1 + ')\\times' + p2 + '$'
      stor.rep2 = '$(1 - ' + p1 + ')+' + p2 + '$'
      stor.rep3 = '$(1 + ' + p1 + ') \\times ' + p2 + '$'
      const rep4 = '$(1 + ' + p1 + ')\\div' + p2 + '$'
      const ll = j3pShuffle([rep1, stor.rep2, stor.rep3, rep4])
      ll.splice(0, 0, 'Choisir')
      stor.liste1 = ListeDeroulante.create(tty[0][1], ll)
      stor.repat = rep1
      stor.douko = tty[0][1]
    }
    if (stor.encours === 'reste') {
      const tt = '<b>Calcule ' + stor.Lexo.pb.prop2 + '</b>'
      j3pAffiche(stor.lesdiv.consigne2, null, tt)
      stor.lesdiv.consigne2.style.color = me.styles.petit.correction.color
      const tty = addDefaultTable(stor.lesdiv.travail, 2, 1)
      const tty3 = addDefaultTable(tty[1][0], 1, 2)
      j3pAffiche(tty3[0][0], null, stor.Lexo.pb.prop3 + '$=$&nbsp;')
      const tty2 = addDefaultTable(tty[0][0], 1, 4)
      stor.asuuup = tty[0][0].parentNode
      stor.brouillon1 = new BrouillonCalculs(
        {
          caseBoutons: tty2[0][0],
          caseEgal: tty2[0][1],
          caseCalculs: tty2[0][2],
          caseApres: tty2[0][3]
        }, {
          limite: 30,
          limitenb: 20,
          restric: '()0123456789+-*/,.',
          hasAutoKeyboard: true,
          lmax: 3,
          justeg: true,
          titre: stor.Lexo.pb.prop3 + '$=$&nbsp;'
        }
      )
      stor.zone1 = new ZoneStyleMathquill2(tty3[0][1], { enter: () => { me.sectionCourante() }, restric: '0123456789/', limite: 10 })
      stor.douko = tty3[0][1]
    }
    if (stor.encours === 'Jusinvendu') {
      j3pDetruit(stor.asuuup)
      const tt = '<b>Détermine le calcul pour obtenir ' + stor.Lexo.pb.prop4 + '.</b>'
      j3pAffiche(stor.lesdiv.consigne2, null, tt)
      stor.lesdiv.consigne2.style.color = me.styles.petit.correction.color
      const tty = addDefaultTable(stor.lesdiv.travail, 1, 3)
      j3pAffiche(tty[0][0], null, stor.Lexo.pb.prop5 + '$=$&nbsp;')
      let p1
      if (stor.don1t === 'p') { p1 = '\\frac{' + stor.a1 + '}{100}' } else { p1 = '\\frac{' + j3pArrondi(stor.a1 / j3pPGCD(stor.a1, stor.c1), 0) + '}{' + j3pArrondi(stor.c1 / j3pPGCD(stor.a1, stor.c1), 0) + '}' }
      const p2 = '\\frac{' + stor.FaGArde[0] + '}{' + stor.FaGArde[1] + '}'
      const rep1 = '$1 - ' + p1 + ' - ' + p2 + '$'
      const rep2 = '$1 + ' + p1 + ' + ' + p2 + '$'
      const rep3 = '$' + p1 + ' + ' + p2 + ' - 1$'
      const rep4 = '$' + p1 + ' + ' + p2 + ' + 1$'
      const ll = j3pShuffle([rep1, rep2, rep3, rep4])
      ll.splice(0, 0, 'Choisir')
      stor.liste1 = ListeDeroulante.create(tty[0][1], ll)
      stor.repat = rep1
      stor.douko = tty[0][1]
    }
    if (stor.encours === 'invendu') {
      j3pEmpty(stor.lesdiv.consigne2)
      const tt = '<b>Calcule ' + stor.Lexo.pb.prop4 + '</b>'
      j3pAffiche(stor.lesdiv.consigne2, null, tt)
      stor.lesdiv.consigne2.style.color = me.styles.petit.correction.color
      const tty = addDefaultTable(stor.lesdiv.travail, 2, 1)
      const tty3 = addDefaultTable(tty[1][0], 1, 2)
      j3pAffiche(tty3[0][0], null, stor.Lexo.pb.prop5 + '$=$&nbsp;')
      const tty2 = addDefaultTable(tty[0][0], 1, 4)
      stor.asuuup = tty[0][0].parentNode
      stor.brouillon1 = new BrouillonCalculs(
        {
          caseBoutons: tty2[0][0],
          caseEgal: tty2[0][1],
          caseCalculs: tty2[0][2],
          caseApres: tty2[0][3]
        }, {
          limite: 30,
          limitenb: 20,
          restric: '()0123456789+-*/,.',
          hasAutoKeyboard: true,
          lmax: 3,
          justeg: true,
          titre: stor.Lexo.pb.prop5 + '$=$&nbsp;'
        }
      )
      stor.zone1 = new ZoneStyleMathquill2(tty3[0][1], { enter: () => { me.sectionCourante() }, restric: '0123456789/', limite: 10 })
      stor.douko = tty3[0][1]
    }
    if (stor.encours === 'Jussuperficie') {
      j3pDetruit(stor.asuuup)
      const tt = '<b>Détermine le calcul pour obtenir ' + stor.Lexo.pb.prop6 + '</b>'
      j3pAffiche(stor.lesdiv.consigne2, null, tt)
      stor.lesdiv.consigne2.style.color = me.styles.petit.correction.color
      const tty = addDefaultTable(stor.lesdiv.travail, 1, 3)
      j3pAffiche(tty[0][0], null, stor.Lexo.pb.prop7 + '$=$&nbsp;')
      const p2 = '\\frac{' + stor.FaGArde[0] + '}{' + stor.FaGArde[1] + '}'
      const p3 = '\\frac{' + stor.FaGArde[1] + '}{' + stor.FaGArde[0] + '}'
      const rep1 = '$' + stor.c + '\\times' + p3 + '$'
      const rep2 = '$' + stor.c + '\\times' + p2 + '$'
      const rep3 = '$' + stor.c + '+' + p3 + '$'
      const rep4 = '$' + stor.c + '+' + p2 + '$'
      const ll = j3pShuffle([rep1, rep2, rep3, rep4])
      ll.splice(0, 0, 'Choisir')
      stor.liste1 = ListeDeroulante.create(tty[0][1], ll)
      stor.repat = rep1
      stor.douko = tty[0][1]
    }
    if (stor.encours === 'superficie') {
      j3pEmpty(stor.lesdiv.consigne2)
      const tt = '<b>Calcule ' + stor.Lexo.pb.prop7 + '</b>'
      j3pAffiche(stor.lesdiv.consigne2, null, tt)
      stor.lesdiv.consigne2.style.color = me.styles.petit.correction.color
      const tty = addDefaultTable(stor.lesdiv.travail, 2, 1)
      const tty3 = addDefaultTable(tty[1][0], 1, 2)
      j3pAffiche(tty3[0][0], null, stor.Lexo.pb.prop7 + '$=$&nbsp;')
      const tty2 = addDefaultTable(tty[0][0], 1, 4)
      stor.asuuup = tty[0][0].parentNode
      stor.brouillon1 = new BrouillonCalculs(
        {
          caseBoutons: tty2[0][0],
          caseEgal: tty2[0][1],
          caseCalculs: tty2[0][2],
          caseApres: tty2[0][3]
        }, {
          limite: 30,
          limitenb: 20,
          restric: '()0123456789+-*/,.',
          hasAutoKeyboard: true,
          lmax: 3,
          justeg: true,
          titre: stor.Lexo.pb.prop7 + '$=$&nbsp;'
        }
      )
      stor.zone1 = new ZoneStyleMathquill2(tty3[0][1], { enter: () => { me.sectionCourante() }, restric: '0123456789', limite: 10 })
      stor.douko = tty3[0][1]
    }
    j3pEmpty(stor.lesdiv.solution)
    j3pEmpty(stor.lesdiv.explik)
    stor.lesdiv.solution.classList.remove('correction')
    stor.lesdiv.explik.classList.remove('explique')
    stor.lesdiv.consigne.classList.add('enonce')
  }

  function creeLesDiv () {
    stor.lesdiv = {}
    stor.lesdiv.conteneur = stor.lesdiv.conteneur = j3pAddElt(me.zonesElts.MG, 'div', {
      style: me.styles.etendre('toutpetit.enonce', { padding: '10px' })
    })
    stor.lesdiv.consigne = addDefaultTable(stor.lesdiv.conteneur, 1, 1)[0][0]
    stor.lesdiv.consigne1 = addDefaultTable(stor.lesdiv.consigne, 1, 1)[0][0]
    stor.lesdiv.consigne2 = addDefaultTable(stor.lesdiv.consigne, 1, 1)[0][0]
    stor.lesdiv.travail = addDefaultTable(stor.lesdiv.conteneur, 1, 1)[0][0]
    stor.lesdiv.explik = j3pAddElt(stor.lesdiv.conteneur, 'div', {
      style: me.styles.etendre('toutpetit.enonce', { padding: '10px' })
    })
    stor.lesdiv.solution = j3pAddElt(stor.lesdiv.conteneur, 'div', {
      style: me.styles.etendre('toutpetit.enonce', { padding: '10px' })
    })
    stor.lesdiv.calculatrice = j3pAddElt(me.zonesElts.MD, 'div', {
      style: me.styles.etendre('toutpetit.enonce', { padding: '10px' })
    })
    stor.lesdiv.correction = j3pAddElt(me.zonesElts.MD, 'div', {
      style: me.styles.etendre('toutpetit.enonce', { padding: '10px' })
    })
    stor.lesdiv.explik.style.color = me.styles.cfaux
    stor.lesdiv.solution.style.color = me.styles.toutpetit.correction.color
    me.zonesElts.MG.classList.add('fond')
  }

  function boutonfen () {
    j3pToggleFenetres('Calculatrice')
  }

  function yaReponse () {
    if (stor.encours === 'Jusreste' || stor.encours === 'Jusinvendu' || stor.encours === 'Jussuperficie') {
      if (!stor.liste1.changed) {
        stor.liste1.focus()
        return false
      }
    }
    if (stor.encours === 'reste') {
      if (!stor.zone1.reponse() === '') {
        stor.zone1.focus()
        return false
      }
    }
    return true
  }

  function isRepOk () {
    stor.errJusReste1 = false
    stor.errJusReste2 = false
    stor.errJusReste3 = false
    stor.errJusInvendu = false
    stor.errEcri = false
    stor.errJusSuperficie = false
    stor.errEcriL = []

    if (stor.encours === 'Jusreste') {
      if (stor.repat !== stor.liste1.reponse) {
        stor.liste1.corrige(false)
        stor.errJusReste1 = (stor.liste1.reponse === stor.rep2)
        stor.errJusReste2 = (stor.liste1.reponse === stor.rep3)
        stor.errJusReste3 = !stor.errJusReste1 && !stor.errJusReste2
        return false
      }
      return true
    }
    if (stor.encours === 'reste') {
      const repEl = stor.zone1.reponsenb()
      if (isNaN(repEl[0]) || isNaN(repEl[1])) {
        stor.errEcri = true
        stor.errEcriL.push('La fraction est mal écrite !')
        stor.zone1.corrige(false)
        return false
      }
      if (stor.fracreste[0] * repEl[1] === stor.fracreste[1] * repEl[0]) stor.FaGArde = repEl
      return stor.fracreste[0] * repEl[1] === stor.fracreste[1] * repEl[0]
    }
    if (stor.encours === 'Jusinvendu') {
      if (stor.repat !== stor.liste1.reponse) {
        stor.liste1.corrige(false)
        stor.errJusInvendu = true
        return false
      }
      return true
    }
    if (stor.encours === 'invendu') {
      const repEl = stor.zone1.reponsenb()
      if (isNaN(repEl[0]) || isNaN(repEl[1])) {
        stor.errEcri = true
        stor.errEcriL.push('La fraction est mal écrite !')
        stor.zone1.corrige(false)
        return false
      }
      if (stor.fracreste2[0] * repEl[1] === stor.fracreste2[1] * repEl[0]) stor.FaGArde = repEl
      return stor.fracreste2[0] * repEl[1] === stor.fracreste2[1] * repEl[0]
    }
    if (stor.encours === 'Jussuperficie') {
      if (stor.repat !== stor.liste1.reponse) {
        stor.liste1.corrige(false)
        stor.errJusSuperficie = true
        return false
      }
      return true
    }
    if (stor.encours === 'superficie') {
      const repEl = Number(stor.zone1.reponse().replace(/ /g, ''))
      return stor.finfin === repEl
    }
  }

  function desactiveAll () {
    if (stor.encours === 'Jusreste' || stor.encours === 'Jusinvendu' || stor.encours === 'Jussuperficie') {
      stor.liste1.disable()
    }
    if (stor.encours === 'reste' || stor.encours === 'invendu' || stor.encours === 'superficie') {
      stor.zone1.disable()
      stor.brouillon1.disable()
    }
  }

  function barrelesfo () {
    if (stor.encours === 'Jusreste' || stor.encours === 'Jusinvendu' || stor.encours === 'Jussuperficie') {
      stor.liste1.barre()
    }
    if (stor.encours === 'reste' || stor.encours === 'invendu' || stor.encours === 'superficie') {
      stor.zone1.barre()
    }
  }

  function mettouvert () {
    if (stor.encours === 'Jusreste' || stor.encours === 'Jusinvendu' || stor.encours === 'Jussuperficie') {
      stor.liste1.corrige(true)
    }
    if (stor.encours === 'reste' || stor.encours === 'invendu' || stor.encours === 'superficie') {
      stor.zone1.corrige(true)
    }
  }

  function AffCorrection (bool) {
    j3pEmpty(stor.lesdiv.explik)
    if (stor.errJusReste1) {
      j3pAffiche(stor.lesdiv.explik, null, 'Pour calculer une fraction d’une quantité, \nil faut multiplier la quantité par la fraction !')
    }
    if (stor.errJusReste2) {
      const devb = (stor.don2t === 'f') ? 'les' : ''
      j3pAffiche(stor.lesdiv.explik, null, 'Il faut calculer ' + devb + stor.p2 + ' du reste !')
    }
    if (stor.errJusReste3) {
      j3pAffiche(stor.lesdiv.explik, null, 'Calcul à revoir !')
    }
    if (stor.errEcri) {
      stor.yaexplik = true
      for (let i = 0; i < stor.errEcriL.length; i++) {
        j3pAffiche(stor.lesdiv.explik, null, stor.errEcriL[i] + '\n')
      }
    }
    if (stor.errJusInvendu) {
      j3pAffiche(stor.lesdiv.explik, null, 'Il fallait retirer £a \net £b \nà la totalité !', { a: stor.Lexo.pb.prop8, b: stor.Lexo.pb.prop2 })
    }
    if (stor.errJusSuperficie) {
      stor.kekoi = '$\\frac{' + stor.FaGArde[0] + '}{' + stor.FaGArde[1] + '}$'
      j3pAffiche(stor.lesdiv.explik, null, 'On sait que $£a$ £b représentent ' + stor.kekoi + ' de £c .', { c: stor.Lexo.pb.prop6, b: stor.Lexo.pb.unite, a: ecrisBienMathquill(stor.c) })
    }
    if (bool) {
      barrelesfo()
      desactiveAll()
      stor.yaco = true
      if (stor.encours === 'Jusreste') {
        stor.foco1 = true
        j3pAffiche(stor.lesdiv.solution, j3pGetNewId(), stor.Lexo.pb.prop3 + '$=$' + stor.repat)
      }
      if (stor.encours === 'reste') {
        if (stor.afaire.indexOf('Jusreste') === -1) {
          let p1, p2
          if (stor.don1t === 'p') { p1 = '\\frac{' + stor.a1 + '}{100}' } else { p1 = '\\frac{' + j3pArrondi(stor.a1 / j3pPGCD(stor.a1, stor.c1), 0) + '}{' + j3pArrondi(stor.c1 / j3pPGCD(stor.a1, stor.c1), 0) + '}' }
          if (stor.don2t === 'p') { p2 = '\\frac{' + stor.a2 + '}{100}' } else { p2 = '\\frac{' + j3pArrondi(stor.a2 / j3pPGCD(stor.a2, stor.c2), 0) + '}{' + j3pArrondi(stor.c2 / j3pPGCD(stor.a2, stor.c2), 0) + '}' }
          j3pAffiche(stor.lesdiv.solution, j3pGetNewId(), stor.Lexo.pb.prop3 + '$ =(1 - ' + p1 + ') \\times' + p2 + '$\n')
        }
        stor.foco2 = true
        const jj = j3pPGCD(stor.fracreste[0], stor.fracreste[1])
        stor.FaGArde = [j3pArrondi(stor.fracreste[0] / jj, 0), j3pArrondi(stor.fracreste[1] / jj, 0)]
        stor.kekoi = '$\\frac{' + stor.FaGArde[0] + '}{' + stor.FaGArde[1] + '}$'
        j3pAffiche(stor.lesdiv.solution, j3pGetNewId(), stor.Lexo.pb.prop3 + '$=$' + stor.kekoi)
      }
      if (stor.encours === 'Jusinvendu') {
        stor.foco3 = true
        j3pAffiche(stor.lesdiv.solution, j3pGetNewId(), stor.Lexo.pb.prop5 + '$=$' + stor.repat)
      }
      if (stor.encours === 'invendu') {
        if (stor.afaire.indexOf('Jusinvendu') === -1) {
          let p1
          if (stor.don1t === 'p') { p1 = '\\frac{' + stor.a1 + '}{100}' } else { p1 = '\\frac{' + j3pArrondi(stor.a1 / j3pPGCD(stor.a1, stor.c1), 0) + '}{' + j3pArrondi(stor.c1 / j3pPGCD(stor.a1, stor.c1), 0) + '}' }
          const p2 = '\\frac{' + stor.FaGArde[0] + '}{' + stor.FaGArde[1] + '}'
          const rep1 = '$1 - ' + p1 + ' - ' + p2 + '$'
          j3pAffiche(stor.lesdiv.solution, j3pGetNewId(), stor.Lexo.pb.prop5 + '$=$' + rep1)
        }
        stor.foco4 = true
        const jj = j3pPGCD(stor.fracreste2[0], stor.fracreste2[1])
        stor.FaGArde = [j3pArrondi(stor.fracreste2[0] / jj, 0), j3pArrondi(stor.fracreste2[1] / jj, 0)]
        stor.kekoi = '$\\frac{' + stor.FaGArde[0] + '}{' + stor.FaGArde[1] + '}$'
        j3pAffiche(stor.lesdiv.solution, j3pGetNewId(), stor.Lexo.pb.prop5 + '$=$' + stor.kekoi)
      }
      if (stor.encours === 'Jussuperficie') {
        stor.foco5 = true
        j3pAffiche(stor.lesdiv.solution, j3pGetNewId(), stor.Lexo.pb.prop7 + '$=$' + stor.repat)
      }
      if (stor.encours === 'superficie') {
        if (stor.afaire.indexOf('Jussuperficie') === -1) {
          const rep1 = '$' + ecrisBienMathquill(stor.c) + '\\times' + '\\frac{' + stor.FaGArde[1] + '}{' + stor.FaGArde[0] + '}' + '$'
          j3pAffiche(stor.lesdiv.solution, j3pGetNewId(), stor.Lexo.pb.prop5 + '$=$' + rep1)
        }
        j3pAffiche(stor.lesdiv.solution, j3pGetNewId(), stor.Lexo.pb.prop7 + '$=$' + ecrisBienMathquill(stor.finfin))
      }
    }
  }

  function enonceMain () {
    if (me.etapeCourante === 1) {
      // A chaque répétition, on nettoie la scène
      // A commenter si besoin
      me.videLesZones()
      creeLesDiv()
      stor.Lexo = faisChoixExos()
    }

    /// //////////////////////////////////
    /* LE CODE PRINCIPAL DE LA SECTION */
    /// //////////////////////////////////
    let ss = stor.afaire.indexOf(stor.encours) + 1
    if (ss === stor.afaire.length) ss = 0
    stor.encours = stor.afaire[ss]
    poseQuestion()
    j3pEmpty(stor.lesdiv.correction)
    me.afficheBoutonValider()
    me.cacheBoutonSuite()
    j3pEmpty(stor.lesdiv.calculatrice)
    if (ds.Calculatrice) {
      me.fenetresjq = [{ name: 'Calculatrice', title: 'Une calculatrice', left: 600, top: 10 }]
      j3pCreeFenetres(me)
      j3pAfficheCroixFenetres('Calculatrice')
      j3pAjouteBouton(stor.lesdiv.calculatrice, 'Calculatrice', 'MepBoutons', 'Calculatrice', boutonfen)
    }
    me.finEnonce()
  }

  switch (me.etat) {
    case 'enonce':
      // code exécuté au lancement de la section
      if (me.debutDeLaSection) {
        initSection()
      } else {
        enonceMain()
      }
      break // case "enonce":

    case 'correction':
      // On teste si une réponse a été saisie
      stor.yaexplik = false
      stor.yaco = false
      stor.lesdiv.solution.classList.remove('correction')
      stor.lesdiv.explik.classList.remove('explique')

      if (yaReponse()) {
        // Bonne réponse
        if (isRepOk()) {
          me._stopTimer()
          stor.lesdiv.correction.style.color = me.styles.cbien
          stor.lesdiv.correction.innerHTML = cBien

          mettouvert()
          desactiveAll()

          j3pEmpty(stor.lesdiv.explikjus)

          me.cacheBoutonValider()
          me.score++

          me.typederreurs[0]++
          me.etat = 'navigation'
          me.sectionCourante()
        } else {
          // Pas de bonne réponse
          stor.lesdiv.correction.style.color = me.styles.cfaux
          stor.lesdiv.correction.innerHTML = cFaux
          if (me.isElapsed) {
            // A cause de la limite de temps :
            me._stopTimer()

            stor.lesdiv.correction.innerHTML += tempsDepasse + '<BR>'
            me.typederreurs[10]++
            me.cacheBoutonValider()

            AffCorrection(true)

            me.etat = 'navigation'
            me.sectionCourante()
          } else {
            // Réponse fausse :
            if (me.essaiCourant < me.donneesSection.nbchances) {
              stor.lesdiv.correction.innerHTML += '<br>' + essaieEncore

              AffCorrection(false)

              me.typederreurs[1]++
            } else {
              // Erreur au nème essai
              me._stopTimer()
              me.cacheBoutonValider()

              AffCorrection(true)

              if (stor.encours === 'p') {
                stor.pose = false
                stor.encours = 't'
              }

              me.typederreurs[2]++
              me.etat = 'navigation'
              me.sectionCourante()
            }
          }
        }
      } else {
        // pas de réponse
        stor.lesdiv.correction.style.color = me.styles.cfaux
        stor.lesdiv.correction.innerHTML = reponseIncomplete
        me.afficheBoutonValider()
      }

      if (stor.yaexplik) {
        stor.lesdiv.explik.classList.add('explique')
      }
      if (stor.yaco) {
        stor.lesdiv.solution.classList.add('correction')
      }

      // Obligatoire
      me.finCorrection()
      break // case "correction":

    case 'navigation':

      if (me.sectionTerminee()) {
        // On détermine la phrase d’état renvoyée par la section
        me.afficheBoutonSectionSuivante()
        me.focus('sectioncontinuer')
      } else {
        me.etat = 'enonce'
        me.afficheBoutonSuite()
        me.focus('boutoncontinuer')
      }
      // Obligatoire
      me.finNavigation()
      break // case "navigation":
  }
}
