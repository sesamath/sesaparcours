import { j3pAddElt, j3pGetRandomBool, j3pGetRandomInt, j3pShuffle } from 'src/legacy/core/functions'
import ListeDeroulante from 'src/legacy/outils/listeDeroulante/ListeDeroulante'
import { j3pAffiche } from 'src/lib/mathquill/functions'
import { addDefaultTable } from 'src/legacy/themes/table'
import textesGeneriques from 'src/lib/core/textes'

const { cBien, cFaux, essaieEncore, reponseIncomplete, tempsDepasse } = textesGeneriques

/**
 * Ajouter une description ici
 * @fileOverview
 * @author Tommy Barroy <t.barroy@laposte.net>
 * @since 2017-10
*/

export const params = {
  outils: ['mathquill'],
  /**
   * Ce qui est paramétrable dans l’éditeur de graphe
   * @type {LegacySectionParamsParametres}
   */
  parametres: [
    ['nbrepetitions', 3, 'entier', 'Nombre de répétitions de la section'],
    ['Positifs', 'parfois', 'liste', 'Les abscisses sont positives', ['toujours', 'jamais', 'parfois']],
    // le modèle gère la compatibilité ascendante avec l’ancien paramètre MiseEnPagePlusClaire
    ['theme', 'standard', 'liste', 'Modèle pour l’affichage', ['standard', 'zonesAvecImageDeFond']]
  ]
}

/**
 * section fraction10
 * @this {Parcours}
 */
export default function main () {
  const me = this
  const stor = me.storage
  let ds = me.donneesSection

  function getDonnees () {
    return {
      // Nombre de répétitions de l’exercice
      nbrepetitions: 3,

      Positifs: 'parfois',

      // Si indication != "" alors un lien "Indication" s’affiche dans la zone Inférieure Gauche (this.zones.IG
      // une indication<br>de deux lignes..."
      indication: '',

      // pour le cas de presentation1bis
      // En effet pas d’indication possible pour cette présentation
      // surchargeindication: false,

      // Nombre de chances dont dispose l’él§ve pour répondre à la question
      nbchances: 1,

      //  || "presentation2" || "presentation3"  || "presentation1bis"
      structure: 'presentation3'
    }
  }

  function initSection () {
    let i
    ds = getDonnees()
    me.donneesSection = ds
    // On surcharge avec les parametres passés par le graphe
    me.surcharge()
    me.validOnEnter = false // ex donneesSection.touche_entree

    // Construction de la page
    me.construitStructurePage(ds.structure)

    ds.lesExos = []

    let lp = [false]
    if (ds.Positifs === 'parfois') lp.push(true)
    if (ds.Positifs === 'toujours') lp = [true]
    lp = j3pShuffle(lp)
    for (i = 0; i < ds.nbitems; i++) {
      ds.lesExos.push({ signe: lp[i % lp.length] })
    }
    ds.lesExos = j3pShuffle(ds.lesExos)

    lp = ['>', '<', '=']
    for (i = 0; i < ds.nbitems; i++) {
      ds.lesExos[i].cas = lp[i % lp.length]
    }
    ds.lesExos = j3pShuffle(ds.lesExos)

    lp = [true, false]
    for (i = 0; i < ds.nbitems; i++) {
      ds.lesExos[i].sens = lp[i % lp.length]
    }
    ds.lesExos = j3pShuffle(ds.lesExos)

    me.typederreurs = [0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0]

    me.score = 0
    const tt = 'Comparer à l’unité'

    me.afficheTitre(tt)

    enonceMain()
  }

  function faisChoixExos () {
    const e = ds.lesExos.pop()

    e.num = j3pGetRandomInt(3, 900)
    stor.fn = stor.fd = ''
    stor.bob = '1'
    e.at = '$' + e.cas + '$'
    switch (e.cas) {
      case '>':
        if (e.signe) {
          e.den = j3pGetRandomInt(1, e.num - 1)
        } else {
          e.den = j3pGetRandomInt(e.num + 1, 999)
        }
        break
      case '=':
        e.den = e.num
        if (j3pGetRandomBool()) {
          if (j3pGetRandomBool()) {
            stor.fn = '0'
          } else {
            stor.fd = '0'
          }
        }
        break
      case '<':
        if (e.signe) {
          e.den = j3pGetRandomInt(e.num + 1, 999)
        } else {
          e.den = j3pGetRandomInt(1, e.num - 1)
        }
    }
    stor.laf = '\\frac{' + stor.fn + e.num + '}{' + stor.fd + e.den + '}'
    if (!e.signe) {
      stor.laf = '-' + stor.laf
      stor.bob = '-1'
    }
    return e
  }

  function poseQuestion () {
    j3pAffiche(stor.lesdiv.consigne, null, '<b>Tu dois comparer la fraction $' + stor.laf + '$ à $' + stor.bob + '$</b>.\n\n')
    const yui = addDefaultTable(stor.lesdiv.travail, 1, 3)
    if (stor.Lexo.sens) {
      j3pAffiche(yui[0][0], null, '$' + stor.laf + '$')
      j3pAffiche(yui[0][2], null, '$' + stor.bob + '$')
    } else {
      j3pAffiche(yui[0][2], null, '$' + stor.laf + '$')
      j3pAffiche(yui[0][0], null, '$' + stor.bob + '$')
      if (stor.Lexo.cas === '>') stor.Lexo.at = '$<$'
      if (stor.Lexo.cas === '<') stor.Lexo.at = '$>$'
    }
    const liste = ['Choisir', '$>$', '$=$', '$<$']
    stor.laliste = ListeDeroulante.create(yui[0][1], liste, {
      centre: true
    })

    stor.lesdiv.travail.classList.add('travail')
    stor.lesdiv.consigne.classList.add('enonce')
    stor.lesdiv.solution.classList.remove('correction')
    stor.lesdiv.explications.classList.remove('explique')
  }

  function creeLesDiv () {
    stor.lesdiv = {}
    stor.lesdiv.conteneur = j3pAddElt(me.zonesElts.MG, 'div', {
      style: me.styles.etendre('toutpetit.enonce', { padding: '10px' })
    })
    stor.lesdiv.consignet = j3pAddElt(stor.lesdiv.conteneur, 'div')
    const tt = addDefaultTable(stor.lesdiv.consignet, 1, 3)
    tt[0][1].style.width = '40px'
    stor.lesdiv.consigne = tt[0][0]
    stor.lesdiv.coboot = tt[0][2]
    stor.lesdiv.travail1 = addDefaultTable(stor.lesdiv.conteneur, 1, 1)[0][0]
    const tt2 = addDefaultTable(stor.lesdiv.travail1, 1, 3)
    tt2[0][1].style.width = '40px'
    stor.lesdiv.travail = tt2[0][0]
    stor.lesdiv.correction = tt2[0][2]
    stor.lesdiv.zonefig = j3pAddElt(stor.lesdiv.conteneur, 'div')
    stor.lesdiv.zonedrep = tt2[0][1]
    stor.lesdiv.explications = j3pAddElt(stor.lesdiv.travail, 'div')
    stor.lesdiv.solution = j3pAddElt(stor.lesdiv.conteneur, 'div')
    stor.lesdiv.explications.style.color = me.styles.cfaux
    stor.lesdiv.solution.style.color = me.styles.toutpetit.correction.color
    me.zonesElts.MG.classList.add('fond')
  }

  function yaReponse () {
    if (!stor.laliste.changed) {
      stor.laliste.focus()
      return false
    }
    return true
  }

  function isRepOk () {
    return (stor.Lexo.at === stor.laliste.reponse)
  }

  function desactiveAll () {
    stor.laliste.disable()
  }

  function barrelesfo () {
    stor.laliste.corrige(false)
    stor.laliste.barre()
  }

  function mettouvert () {
    stor.laliste.corrige(true)
  }
  function AffCorrection (bool) {
    let buf

    if (bool) {
      stor.yaco = true
      barrelesfo()
      desactiveAll()
      buf = stor.Lexo.cas
      if (!stor.Lexo.signe) {
        if (buf === '>') { buf = '<' } else if (buf === '<') { buf = '>' }
      }
      j3pAffiche(stor.lesdiv.solution, null, 'En effet, $' + stor.fn + stor.Lexo.num + ' ' + buf + ' ' + stor.fd + stor.Lexo.den + '$ , donc $' + stor.laf.replace('-', '') + ' ' + buf + ' 1$.\n')
      if (!stor.Lexo.signe) {
        if (stor.Lexo.cas !== '=') {
          j3pAffiche(stor.lesdiv.solution, null, 'Les nombres négatifs étant rangés dans le sens contraire des nombres positifs, on a $' + stor.laf + ' ' + stor.Lexo.cas + ' -1$')
        } else {
          j3pAffiche(stor.lesdiv.solution, null, 'Donc $' + stor.laf + ' ' + stor.Lexo.cas + ' -1$')
        }
      }
    }
  }

  function enonceMain () {
    // A chaque répétition, on nettoie la scène
    // A commenter si besoin
    me.videLesZones()
    creeLesDiv()
    stor.Lexo = faisChoixExos()
    stor.lesdiv.travail.classList.add('travail')
    stor.lesdiv.consigne.classList.add('enonce')
    stor.lesdiv.explications.classList.remove('explique')
    stor.lesdiv.solution.classList.remove('correction')

    poseQuestion()
    me.cacheBoutonSuite()
    me.finEnonce()
  }
  switch (this.etat) {
    case 'enonce':
      // code exécuté au lancement de la section
      if (me.debutDeLaSection) {
        initSection()
      } else {
        enonceMain()
      }
      break // case "enonce":

    case 'correction':
      // On teste si une réponse a été saisie
      stor.yaexplik = false
      stor.lesdiv.explications.innerHTML = ''
      stor.lesdiv.correction.innerHTML = ''
      stor.yaco = false

      if (yaReponse()) {
        // Bonne réponse
        if (isRepOk()) {
          this._stopTimer()
          stor.lesdiv.correction.style.color = this.styles.cbien
          stor.lesdiv.correction.innerHTML = cBien

          desactiveAll()
          mettouvert()
          this.cacheBoutonValider()
          this.score++

          this.typederreurs[0]++
          this.etat = 'navigation'
          this.sectionCourante()
        } else {
          // Pas de bonne réponse
          stor.lesdiv.correction.style.color = this.styles.cfaux
          stor.lesdiv.correction.innerHTML = cFaux
          if (this.isElapsed) {
            // A cause de la limite de temps :
            this._stopTimer()

            stor.lesdiv.correction.innerHTML += tempsDepasse + '<BR>'
            this.typederreurs[10]++
            this.cacheBoutonValider()

            AffCorrection(true)

            this.etat = 'navigation'
            this.sectionCourante()
          } else {
            // Réponse fausse :
            if (this.essaiCourant < ds.nbchances) {
              stor.lesdiv.correction.innerHTML += '<br>' + essaieEncore

              AffCorrection(false)

              this.typederreurs[1]++
            } else {
              // Erreur au nème essai
              this._stopTimer()
              this.cacheBoutonValider()

              AffCorrection(true)

              if (stor.encours === 'p') {
                stor.pose = false
                stor.encours = 't'
              }

              this.typederreurs[2]++
              this.etat = 'navigation'
              this.sectionCourante()
            }
          }
        }
      } else {
        // pas de réponse
        stor.lesdiv.correction.style.color = this.styles.cfaux
        stor.lesdiv.correction.innerHTML = reponseIncomplete
        if (stor.encours === 'm') {
          stor.lesdiv.correction.innerHTML = 'Réponse incomplète ! <br> Tu dois déplacer le point ' + stor.C + ' .'
        }
        me.afficheBoutonValider()
      }

      if (stor.yaexplik) stor.lesdiv.explications.classList.add('explique')
      if (stor.yaco) stor.lesdiv.solution.classList.add('correction')

      // Obligatoire
      this.finCorrection()
      break // case "correction":

    case 'navigation':
      if (!this.sectionTerminee()) {
        this.etat = 'enonce'
      }
      this.finNavigation()
      break // case "navigation":
  }
}
