import { j3pAddElt, j3pAjouteBouton, j3pArrondi, j3pClone, j3pEmpty, j3pGetRandomInt, j3pShowError, j3pPGCD, j3pShuffle, j3pGetNewId, j3pGetRandomBool } from 'src/legacy/core/functions'
import { verifNombreBienEcrit } from 'src/legacy/outils/fonctions/ecritureNombre'
import { j3pAffiche } from 'src/lib/mathquill/functions'
import ZoneStyleMathquill1 from 'src/legacy/outils/zoneStyleMathquill/ZoneStyleMathquill1'
import { j3pCreeFenetres, j3pAfficheCroixFenetres, j3pToggleFenetres } from 'src/legacy/core/functionsJqDialog'
import { addDefaultTable } from 'src/legacy/themes/table'
import { ecrisBienMathquill } from 'src/legacy/outils/zoneStyleMathquill/functions'
import ListeDeroulante from 'src/legacy/outils/listeDeroulante/ListeDeroulante'
import textesGeneriques from 'src/lib/core/textes'

const { cBien, cFaux, essaieEncore, reponseIncomplete, tempsDepasse } = textesGeneriques

/**
 * Ajouter une description ici
 * @fileOverview
 * @author Tommy Barroy <t.barroy@laposte.net>
 * @since 2017-10
*/

export const params = {
  outils: ['mathquill', 'calculatrice'],
  /**
   * Ce qui est paramétrable dans l’éditeur de graphe
   * @type {LegacySectionParamsParametres}
   */
  parametres: [
    ['nbrepetitions', 3, 'entier', 'Nombre de répétitions de la section'],
    ['nbchances', 1, 'entier', 'Nombre d’essais possibles'],
    ['Situation', true, 'boolean', '<u>true</u>: La situation est présentée avec les effectifs réels.'],
    ['Fraction', true, 'boolean', '<u>true</u>: la proportion est donnée avec une fraction.'],
    ['Pourcentage', true, 'boolean', '<u>true</u>: La proportion est donnée avec un pourcentage.'],
    ['Calculatrice', true, 'boolean', '<u>true</u>: L’élève dispose d’une calculatrice.'],
    ['theme', 'standard', 'liste', 'Modèle pour l’affichage', ['standard', 'zonesAvecImageDeFond']]
  ]
}

/**
 * section fraction13
 * @this {Parcours}
 */
export default function main () {
  const me = this
  const stor = me.storage
  let ds = me.donneesSection

  function getDonnees () {
    return {
      // Nombre de répétitions de l’exercice
      nbrepetitions: 3,

      Situation: true,
      Calculatrice: true,
      Fraction: true,
      Pourcentage: true,
      // Si indication != "" alors un lien "Indication" s’affiche dans la zone Inférieure Gauche (me.zones.IG
      // une indication<br>de deux lignes..."
      indication: '',

      // pour le cas de presentation1bis
      // En effet pas d’indication possible pour cette présentation
      // surchargeindication: false,

      // Nombre de chances dont dispose l’él§ve pour répondre à la question
      nbchances: 1,

      //  || "presentation2" || "presentation3"  || "presentation1bis"
      structure: 'presentation1bis',
      pb: [
        { ncompt: 'nombre de filles', ntot: 'nombre d’élèves', min: 16, max: 37, sit: 'Dans la classe de £b, il y a $£a$ filles sur $£c$ élèves.', prop: 'Dans la classe de £b, $£p$ des élèves sont des filles.', cal: 'la proportion de filles dans la classe de £b' },
        { ncompt: 'nombre de filles', ntot: 'nombre d’élèves', min: 16, max: 37, sit: 'Dans la classe de £b, il y a $£a$ garçons et $£d$ filles.', prop: 'Dans la classe de £b, $£p$ des élèves sont des garçons.', cal: 'la proportion de garçons dans la classe de £b' },
        { ncompt: 'nombre de poules rousses', ntot: 'nombre total de poules', min: 16, max: 37, sit: 'Dans la ferme de £b, il y a $£c$ poules, dont $£a$ sont rousses.', prop: 'Dans la ferme de £b, $£p$ des poules sont rousses.', cal: 'la proportion de poules rousses dans la ferme de £b' },
        { ncompt: 'nombre de poules rousses', ntot: 'nombre total de poules', min: 16, max: 37, sit: 'Dans la ferme de £b, il y a $£a$ poules rousses et $£d$ poules brunes.', prop: 'Dans la ferme de £b, $£p$ des poules sont rousses.', cal: 'la proportion de poules rousses dans la ferme de £b' },
        { ncompt: 'nombre de figurines en métal', ntot: 'nombre total de figurines', min: 5, max: 50, sit: 'Dans la chambre de £b, il y a $£a$ figurines en métal sur $£c$ figurines.', prop: 'Dans la chambre de £b, $£p$ des figurines sont en métal.', cal: 'la proportion de figurines en métal dans la chambre de £b' }
      ],
      // Par défaut, pas de temps limite. Décommentez pour avoir un temps limite par défaut. En secondes.
      pren: ['Moussa', 'Chloée', 'Llyn', 'Mathys', 'Rémi', 'Nathéo', 'Charlie', 'Félix', 'Daniel', 'Cathy', 'Murielle', 'Stéphanie', 'Célita']
    }
  }

  function initSection () {
    ds = getDonnees()
    me.donneesSection = ds
    // On surcharge avec les parametres passés par le graphe
    me.surcharge()
    me.validOnEnter = false // ex donneesSection.touche_entree

    // Construction de la page
    me.construitStructurePage(me.donneesSection.structure)

    me.donneesSection.nbetapes = 2
    stor.afaire = ['prop', 'comp']
    stor.encours = stor.afaire[stor.afaire.length - 1]
    me.donneesSection.nbitems = me.donneesSection.nbetapes * me.donneesSection.nbrepetitions

    ds.lesExos = []

    let lp = [0, 1, 2]
    lp = j3pShuffle(lp)
    for (let i = 0; i < me.donneesSection.nbitems; i++) {
      ds.lesExos.push({ cas: lp[i % lp.length] })
    }
    ds.lesExos = j3pShuffle(ds.lesExos)

    lp = []
    if (ds.Situation) lp.push('Sit')
    if (ds.Fraction) lp.push('Frac')
    if (ds.Pourcentage) lp.push('Pour')
    if (lp.length === 0) {
      j3pShowError('Erreur de paramètrage')
      lp = ['Sit']
    }
    let casPos = []
    if (lp.indexOf('Sit') !== -1) {
      casPos.push('SitSit')
    }
    if (lp.indexOf('Frac') !== -1) {
      casPos.push('FracFrac')
      if (lp.indexOf('Sit') !== -1) {
        casPos.push('FracSit')
      }
    }
    if (lp.indexOf('Pour') !== -1) {
      if (lp.indexOf('Frac') === -1 || lp.indexOf('Sit') !== -1) casPos.push('PourSit')
      if (lp.indexOf('Frac') !== -1) {
        casPos.push('PourFrac')
      }
    }
    casPos = j3pShuffle(casPos)
    for (let i = 0; i < me.donneesSection.nbitems; i++) {
      ds.lesExos[i].don = casPos[i % casPos.length]
    }
    ds.lesExos = j3pShuffle(ds.lesExos)

    lp = j3pShuffle(j3pClone(ds.pb))
    for (let i = 0; i < me.donneesSection.nbitems; i++) {
      ds.lesExos[i].pb = j3pClone(lp[i % lp.length])
    }
    ds.lesExos = j3pShuffle(ds.lesExos)

    lp = j3pShuffle(j3pClone(ds.pren))
    for (let i = 0; i < me.donneesSection.nbitems; i++) {
      ds.lesExos[i].pren1 = lp[i % lp.length]
      do {
        ds.lesExos[i].pren2 = lp[j3pGetRandomInt(0, lp.length - 1)]
      } while (ds.lesExos[i].pren1 === ds.lesExos[i].pren2)
    }
    ds.lesExos = j3pShuffle(ds.lesExos)

    me.typederreurs = [0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0]

    me.score = 0
    const tt = 'Comparer des proportions'

    me.afficheTitre(tt)

    enonceMain()
  }

  function faisChoixExos () {
    const e = ds.lesExos.pop()
    stor.b1 = e.pren1
    stor.b2 = e.pren2
    switch (e.don) {
      case 'SitSit':
        stor.don1t = stor.don2t = 's'
        stor.don1 = e.pb.sit
        stor.don2 = e.pb.sit
        stor.c1 = j3pGetRandomInt(e.pb.min, e.pb.max)
        do {
          stor.c2 = j3pGetRandomInt(e.pb.min, e.pb.max)
        } while (stor.c1 === stor.c2)
        stor.a1 = j3pGetRandomInt(2, stor.c1 - 2)
        do {
          stor.a2 = j3pGetRandomInt(2, stor.c2 - 2)
        } while (Math.abs(stor.a1 / stor.c1 - stor.a2 / stor.c2) < 0.02)
        stor.d1 = stor.c1 - stor.a1
        stor.d2 = stor.c2 - stor.a2
        break
      case 'FracFrac':
        stor.don1t = stor.don2t = 'f'
        stor.don1 = e.pb.prop
        stor.don2 = e.pb.prop
        stor.c1 = j3pGetRandomInt(e.pb.min, e.pb.max)
        do {
          stor.c2 = j3pGetRandomInt(e.pb.min, e.pb.max)
        } while (j3pPGCD(stor.c1, stor.c2) !== 1)
        stor.a1 = j3pGetRandomInt(2, stor.c1 - 2)
        do {
          stor.a2 = j3pGetRandomInt(2, stor.c2 - 2)
        } while (Math.abs(stor.a1 / stor.c1 - stor.a2 / stor.c2) < 0.02)
        stor.d1 = stor.c1 - stor.a1
        stor.d2 = stor.c2 - stor.a2
        stor.p1 = '\\frac{' + j3pArrondi(stor.a1 / j3pPGCD(stor.a1, stor.c1), 0) + '}{' + j3pArrondi(stor.c1 / j3pPGCD(stor.a1, stor.c1), 0) + '}'
        stor.p2 = '\\frac{' + j3pArrondi(stor.a2 / j3pPGCD(stor.a2, stor.c2), 0) + '}{' + j3pArrondi(stor.c2 / j3pPGCD(stor.a2, stor.c2), 0) + '}'
        break
      case 'FracSit':
        stor.don1 = e.pb.prop
        stor.don2 = e.pb.prop
        if (j3pGetRandomBool()) { stor.don1 = e.pb.sit; stor.don1t = 's'; stor.don2t = 'f' } else { stor.don2 = e.pb.sit; stor.don2t = 's'; stor.don1t = 'f' }
        stor.c1 = j3pGetRandomInt(e.pb.min, e.pb.max)
        do {
          stor.c2 = j3pGetRandomInt(e.pb.min, e.pb.max)
        } while (j3pPGCD(stor.c1, stor.c2) !== 1)
        stor.a1 = j3pGetRandomInt(2, stor.c1 - 2)
        do {
          stor.a2 = j3pGetRandomInt(2, stor.c2 - 2)
        } while (Math.abs(stor.a1 / stor.c1 - stor.a2 / stor.c2) < 0.02)
        stor.d1 = stor.c1 - stor.a1
        stor.d2 = stor.c2 - stor.a2
        stor.p1 = '\\frac{' + j3pArrondi(stor.a1 / j3pPGCD(stor.a1, stor.c1), 0) + '}{' + j3pArrondi(stor.c1 / j3pPGCD(stor.a1, stor.c1), 0) + '}'
        stor.p2 = '\\frac{' + j3pArrondi(stor.a2 / j3pPGCD(stor.a2, stor.c2), 0) + '}{' + j3pArrondi(stor.c2 / j3pPGCD(stor.a2, stor.c2), 0) + '}'
        break
      case 'PourSit':
        stor.don1 = e.pb.prop
        stor.don2 = e.pb.prop
        stor.don1t = stor.don2t = 's'
        if (j3pGetRandomBool()) { stor.don1 = e.pb.sit; stor.don2t = 'p' } else { stor.don2 = e.pb.sit; stor.don1t = 'p' }
        if (stor.don1t === 'p') { stor.c1 = 100 } else {
          do {
            stor.c1 = j3pGetRandomInt(e.pb.min, e.pb.max)
          } while (j3pPGCD(stor.c1, 100) !== 1)
        }
        if (stor.don2t === 'p') { stor.c2 = 100 } else {
          do {
            stor.c2 = j3pGetRandomInt(e.pb.min, e.pb.max)
          } while (j3pPGCD(stor.c2, 100) !== 1)
        }
        stor.a1 = j3pGetRandomInt(2, stor.c1 - 2)
        do {
          stor.a2 = j3pGetRandomInt(2, stor.c2 - 2)
        } while (Math.abs(stor.a1 / stor.c1 - stor.a2 / stor.c2) < 0.02)
        stor.d1 = stor.c1 - stor.a1
        stor.d2 = stor.c2 - stor.a2
        stor.p1 = j3pArrondi(stor.a1 / stor.c1 * 100, 0) + ' %'
        stor.p2 = j3pArrondi(stor.a2 / stor.c2 * 100, 0) + ' %'
        break
      case 'PourFrac':
        stor.don1 = e.pb.prop
        stor.don2 = e.pb.prop
        stor.don1t = stor.don2t = 'f'
        if (j3pGetRandomBool()) {
          stor.don1t = 'p'
        } else {
          stor.don2t = 'p'
        }
        if (stor.don1t === 'p') { stor.c1 = 100 } else {
          do {
            stor.c1 = j3pGetRandomInt(e.pb.min, e.pb.max)
          } while (j3pPGCD(stor.c1, 100) !== 1)
        }
        if (stor.don2t === 'p') { stor.c2 = 100 } else {
          do {
            stor.c2 = j3pGetRandomInt(e.pb.min, e.pb.max)
          } while (j3pPGCD(stor.c2, 100) !== 1)
        }
        stor.a1 = j3pGetRandomInt(2, stor.c1 - 2)
        do {
          stor.a2 = j3pGetRandomInt(2, stor.c2 - 2)
        } while (Math.abs(stor.a1 / stor.c1 - stor.a2 / stor.c2) < 0.02)
        stor.d1 = stor.c1 - stor.a1
        stor.d2 = stor.c2 - stor.a2
        stor.p2 = '\\frac{' + j3pArrondi(stor.a2 / j3pPGCD(stor.a2, stor.c2), 0) + '}{' + j3pArrondi(stor.c2 / j3pPGCD(stor.a2, stor.c2), 0) + '}'
        stor.p1 = '\\frac{' + j3pArrondi(stor.a1 / j3pPGCD(stor.a1, stor.c1), 0) + '}{' + j3pArrondi(stor.c1 / j3pPGCD(stor.a1, stor.c1), 0) + '}'
        if (stor.don1t === 'p') stor.p1 = stor.a1 + ' %'
        if (stor.don2t === 'p') stor.p2 = stor.a2 + ' %'
        break
    }
    stor.foco1 = stor.foco2 = false
    return e
  }

  function poseQuestion () {
    if (stor.foco1) {
      j3pEmpty(stor.asup1)
      j3pAffiche(stor.asup1, null, '$' + ecrisBienMathquill(j3pArrondi(stor.a1 / stor.c1, 2)) + '$')
      stor.asup1.style.color = me.styles.petit.correction.color
    }
    if (stor.foco2) {
      j3pEmpty(stor.asup2)
      j3pAffiche(stor.asup2, null, '$' + ecrisBienMathquill(j3pArrondi(stor.a2 / stor.c2, 2)) + '$')
      stor.asup2.style.color = me.styles.petit.correction.color
    }
    if (stor.encours === 'prop') {
      j3pAffiche(stor.lesdiv.consigne1, j3pGetNewId(), stor.don1 + '\n', { a: stor.a1, b: stor.b1, c: stor.c1, d: stor.d1, p: stor.p1 })
      j3pAffiche(stor.lesdiv.consigne1, j3pGetNewId(), stor.don2, { a: stor.a2, b: stor.b2, c: stor.c2, d: stor.d2, p: stor.p2 })
      const tt = '<b>Calcule </b>' + stor.Lexo.pb.cal.replace('£b', stor.b1) + '<b> puis <br> calcule </b>' + stor.Lexo.pb.cal.replace('£b', stor.b2) + '.'
      j3pAffiche(stor.lesdiv.consigne2, null, tt + '\n<i>(donner les réponses sous forme décimale)</i>')
      stor.lesdiv.consigne2.style.color = me.styles.petit.correction.color
      const tty = addDefaultTable(stor.lesdiv.travailjus, 1, 3)
      let teg = '='
      let tplus = ''
      if (Math.abs(j3pArrondi(stor.a1 / stor.c1, 2) - stor.a1 / stor.c1) > 1e-12) {
        teg = '$\\approx$'
        tplus = '&nbsp;(au centième près)'
      }
      j3pAffiche(tty[0][0], null, stor.Lexo.pb.cal.replace('£b', stor.b1).replace('la p', 'P') + ' ' + teg + '&nbsp;')
      j3pAffiche(tty[0][2], null, tplus)
      stor.zone1 = new ZoneStyleMathquill1(tty[0][1], { enter: () => { me.sectionCourante() }, restric: '0123456789.,', limite: 5 })
      stor.asup1 = tty[0][1]
      const tty2 = addDefaultTable(stor.lesdiv.travailjus, 1, 3)
      teg = '='
      tplus = ''
      if (Math.abs(j3pArrondi(stor.a2 / stor.c2, 2) - stor.a2 / stor.c2) > 1e-12) {
        teg = '$\\approx$'
        tplus = '&nbsp;(au centième près)'
      }
      j3pAffiche(tty2[0][0], null, stor.Lexo.pb.cal.replace('£b', stor.b2).replace('la p', 'P') + ' ' + teg + '&nbsp;')
      j3pAffiche(tty2[0][2], null, tplus)
      stor.zone2 = new ZoneStyleMathquill1(tty2[0][1], { enter: () => { me.sectionCourante() }, restric: '0123456789.,', limite: 5 })
      stor.lesdiv.jus.classList.add('travail')
      stor.asup2 = tty2[0][1]
    } else {
      stor.lesdiv.jus.classList.remove('travail')
      stor.lesdiv.travailenc.classList.add('travail')
      stor.lesdiv.jus.classList.add('enonce')
      j3pEmpty(stor.lesdiv.consigne2)
      j3pAffiche(stor.lesdiv.consigne2, null, '<b>Compare les deux proportions.</b>')
      const tt = addDefaultTable(stor.lesdiv.travailenc, 3, 1)
      j3pAffiche(tt[0][0], null, stor.Lexo.pb.cal.replace('£b', stor.b1).replace('la p', 'La p'))
      stor.liste1 = ListeDeroulante.create(tt[1][0], ['Choisir', 'est supèrieure à', 'est inférieure à'], { center: true })
      tt[1][0].style.textAlign = 'center'
      j3pAffiche(tt[2][0], null, stor.Lexo.pb.cal.replace('£b', stor.b2))
    }
    j3pEmpty(stor.lesdiv.solutionjus)
    stor.lesdiv.solutionenc.classList.remove('correction')
    stor.lesdiv.solutionjus.classList.remove('correction')
    stor.lesdiv.explikjus.classList.remove('explique')
    stor.lesdiv.consigne.classList.add('enonce')
  }

  function creeLesDiv () {
    stor.lesdiv = {}
    stor.lesdiv.conteneur = stor.lesdiv.conteneur = j3pAddElt(me.zonesElts.MG, 'div', {
      style: me.styles.etendre('toutpetit.enonce', { padding: '10px' })
    })
    stor.lesdiv.consigne = addDefaultTable(stor.lesdiv.conteneur, 1, 1)[0][0]
    stor.lesdiv.consigne1 = addDefaultTable(stor.lesdiv.consigne, 1, 1)[0][0]
    stor.lesdiv.consigne2 = addDefaultTable(stor.lesdiv.consigne, 1, 1)[0][0]
    const cont = addDefaultTable(stor.lesdiv.conteneur, 4, 1)
    const cont2 = addDefaultTable(cont[0][0], 2, 1)
    stor.lesdiv.jus = cont[0][0]
    stor.lesdiv.travailjus = cont2[0][0]
    stor.lesdiv.explikjus = cont[1][0]
    stor.lesdiv.solutionjus = cont2[1][0]
    stor.lesdiv.travailenc = cont[2][0]
    stor.lesdiv.solutionenc = cont[3][0]
    stor.lesdiv.calculatrice = j3pAddElt(me.zonesElts.MD, 'div', {
      style: me.styles.etendre('toutpetit.enonce', { padding: '10px' })
    })
    stor.lesdiv.correction = j3pAddElt(me.zonesElts.MD, 'div', {
      style: me.styles.etendre('toutpetit.enonce', { padding: '10px' })
    })
    stor.lesdiv.explikjus.style.color = me.styles.cfaux
    stor.lesdiv.solutionjus.style.color = me.styles.toutpetit.correction.color
    stor.lesdiv.solutionenc.style.color = me.styles.toutpetit.correction.color
    me.zonesElts.MG.classList.add('fond')
  }

  function boutonfen () {
    j3pToggleFenetres('Calculatrice')
  }

  function yaReponse () {
    if (stor.encours === 'prop') {
      if (stor.zone1.reponse() === '') {
        stor.zone1.focus()
        return false
      }
      if (stor.zone2.reponse() === '') {
        stor.zone2.focus()
        return false
      }
    }
    return true
  }

  function isRepOk () {
    stor.errEcri = false
    stor.errEcriL = []

    if (stor.encours === 'prop') {
      let ok = true
      const rep1 = stor.zone1.reponse().replace(/ /g, '')
      const rep2 = stor.zone2.reponse().replace(/ /g, '')
      const j1 = verifNombreBienEcrit(rep1, false, true)
      const j2 = verifNombreBienEcrit(rep2, false, true)
      if (!j1.good) {
        stor.zone1.corrige(false)
        stor.errEcri = true
        stor.errEcriL.push(j1.remede)
        ok = false
      }
      if (!j2.good) {
        stor.zone2.corrige(false)
        stor.errEcri = true
        if (stor.errEcriL.indexOf(j2.remede) === -1) stor.errEcriL.push(j2.remede)
        ok = false
      }
      if (!ok) return false
      if (Math.abs(stor.a1 / stor.c1 - j1.nb) > 0.01) {
        stor.zone1.corrige(false)
        ok = false
      } else stor.zone1.corrige(true)
      if (Math.abs(stor.a2 / stor.c2 - j2.nb) > 0.01) {
        stor.zone2.corrige(false)
        ok = false
      } else stor.zone2.corrige(true)
      return ok
    } else {
      if (!stor.liste1.changed) return false
      const csup = stor.a1 / stor.c1 > stor.a2 / stor.c2
      const repsup = stor.liste1.getReponseIndex() === 1
      return csup === repsup
    }
  }

  function desactiveAll () {
    if (stor.encours === 'prop') {
      stor.zone1.disable()
      stor.zone2.disable()
    } else stor.liste1.disable()
  }

  function barrelesfo () {
    if (stor.encours === 'prop') {
      stor.zone1.barreIfKo()
      stor.zone2.barreIfKo()
    } else {
      stor.liste1.barre()
    }
  }

  function mettouvert () {
    if (stor.encours === 'prop') {
      stor.zone1.corrige(true)
      stor.zone2.corrige(true)
    } else stor.liste1.corrige(true)
  }
  function AffCorrection (bool) {
    j3pEmpty(stor.lesdiv.explikjus)
    if (stor.errEcri) {
      stor.yaexplik = true
      for (let i = 0; i < stor.errEcriL.length; i++) {
        j3pAffiche(stor.lesdiv.explikjus, null, stor.errEcriL[i] + '\n')
      }
    }
    if (stor.encours !== 'prop') stor.liste1.corrige(false)

    if (bool) {
      barrelesfo()
      desactiveAll()
      stor.yaco = true
      if (stor.encours === 'prop') {
        if (stor.zone1.bon === false) {
          stor.foco1 = true
          let teg = '='
          let tplus = ''
          switch (stor.don1t) {
            case 's':
              j3pAffiche(stor.lesdiv.solutionjus, j3pGetNewId(), stor.Lexo.pb.cal.replace('£b', stor.b1).replace('la p', 'P') + ' ' + teg + ' $\\frac{\\text{' + stor.Lexo.pb.ncompt + '}}{\\text{' + stor.Lexo.pb.ntot + '}}$ ' + '\n')
              j3pAffiche(stor.lesdiv.solutionjus, j3pGetNewId(), stor.Lexo.pb.cal.replace('£b', stor.b1).replace('la p', 'P') + ' ' + teg + ' $\\frac{' + stor.a1 + '}{' + stor.c1 + '}$ ' + '\n')
              break
            case 'f':
              j3pAffiche(stor.lesdiv.solutionjus, j3pGetNewId(), stor.Lexo.pb.cal.replace('£b', stor.b1).replace('la p', 'P') + ' ' + teg + ' $' + j3pArrondi(stor.a1 / j3pPGCD(stor.a1, stor.c1), 0) + '\\div' + j3pArrondi(stor.c1 / j3pPGCD(stor.a1, stor.c1), 0) + '$ ' + '\n')
              break
            case 'p':
              j3pAffiche(stor.lesdiv.solutionjus, j3pGetNewId(), stor.Lexo.pb.cal.replace('£b', stor.b1).replace('la p', 'P') + ' ' + teg + ' $' + stor.a1 + '\\div 100$ ' + '\n')
              break
          }
          if (Math.abs(j3pArrondi(stor.a1 / stor.c1, 2) - stor.a1 / stor.c1) > 1e-12) {
            teg = '$\\approx$'
            tplus = ' (au centième près)'
          }
          j3pAffiche(stor.lesdiv.solutionjus, j3pGetNewId(), stor.Lexo.pb.cal.replace('£b', stor.b1).replace('la p', 'P') + ' ' + teg + ' $' + ecrisBienMathquill(j3pArrondi(stor.a1 / stor.c1, 2)) + '$ ' + tplus + '\n\n')
        }
        if (stor.zone2.isOk() === false) {
          stor.foco2 = true
          let teg = '='
          let tplus = ''
          switch (stor.don2t) {
            case 's':
              j3pAffiche(stor.lesdiv.solutionjus, j3pGetNewId(), stor.Lexo.pb.cal.replace('£b', stor.b2).replace('la p', 'P') + ' ' + teg + ' $\\frac{\\text{' + stor.Lexo.pb.ncompt + '}}{\\text{' + stor.Lexo.pb.ntot + '}}$ ' + '\n')
              j3pAffiche(stor.lesdiv.solutionjus, j3pGetNewId(), stor.Lexo.pb.cal.replace('£b', stor.b2).replace('la p', 'P') + ' ' + teg + ' $\\frac{' + stor.a2 + '}{' + stor.c2 + '}$ ' + '\n')
              break
            case 'f':
              j3pAffiche(stor.lesdiv.solutionjus, j3pGetNewId(), stor.Lexo.pb.cal.replace('£b', stor.b2).replace('la p', 'P') + ' ' + teg + ' $' + j3pArrondi(stor.a2 / j3pPGCD(stor.a2, stor.c2), 0) + '\\div' + j3pArrondi(stor.c2 / j3pPGCD(stor.a2, stor.c2), 0) + '$ ' + '\n')
              break
            case 'p':
              j3pAffiche(stor.lesdiv.solutionjus, j3pGetNewId(), stor.Lexo.pb.cal.replace('£b', stor.b2).replace('la p', 'P') + ' ' + teg + ' $' + stor.a2 + '\\div 100$ ' + '\n')
              break
          }
          if (Math.abs(j3pArrondi(stor.a2 / stor.c2, 2) - stor.a2 / stor.c2) > 1e-12) {
            teg = '$\\approx$'
            tplus = ' (au centième près)'
          }
          j3pAffiche(stor.lesdiv.solutionjus, j3pGetNewId(), stor.Lexo.pb.cal.replace('£b', stor.b2).replace('la p', 'P') + ' ' + teg + ' $' + ecrisBienMathquill(j3pArrondi(stor.a2 / stor.c2, 2)) + '$ ' + tplus)
        }
        if (stor.zone1.isOk() === false && stor.zone2.isOk() === false) j3pEmpty(stor.lesdiv.consigne2)
      } else {
        const symb = (stor.a1 / stor.c1 > stor.a2 / stor.c2) ? ' est supérieure.' : ' est inférieure. '
        j3pAffiche(stor.lesdiv.solutionenc, null, stor.Lexo.pb.cal.replace('£b', stor.b1).replace('la p', 'La p') + symb)
      }
    } else { me.afficheBoutonValider() }
  }

  function enonceMain () {
    if (me.etapeCourante === 1) {
      // A chaque répétition, on nettoie la scène
      me.videLesZones()
      creeLesDiv()
      stor.Lexo = faisChoixExos()
    }

    /// //////////////////////////////////
    /* LE CODE PRINCIPAL DE LA SECTION */
    /// //////////////////////////////////
    let ss = stor.afaire.indexOf(stor.encours) + 1
    if (ss === stor.afaire.length) ss = 0
    stor.encours = stor.afaire[ss]
    poseQuestion()
    j3pEmpty(stor.lesdiv.correction)
    me.afficheBoutonValider()
    me.cacheBoutonSuite()
    j3pEmpty(stor.lesdiv.calculatrice)
    if (ds.Calculatrice) {
      me.fenetresjq = [{ name: 'Calculatrice', title: 'Une calculatrice', left: 600, top: 10 }]
      j3pCreeFenetres(me)
      j3pAfficheCroixFenetres('Calculatrice')
      j3pAjouteBouton(stor.lesdiv.calculatrice, 'Calculatrice', 'MepBoutons', 'Calculatrice', boutonfen)
    }
    me.finEnonce()
  }

  switch (me.etat) {
    case 'enonce':
      // code exécuté au lancement de la section
      if (me.debutDeLaSection) {
        initSection()
      } else {
        enonceMain()
      }
      break // case "enonce":

    case 'correction':
      // On teste si une réponse a été saisie
      stor.yaexplik = false
      stor.yaco = false
      stor.lesdiv.solutionenc.classList.remove('correction')
      stor.lesdiv.solutionjus.classList.remove('correction')
      stor.lesdiv.explikjus.classList.remove('explique')

      if (yaReponse()) {
        // Bonne réponse
        if (isRepOk()) {
          me._stopTimer()
          stor.lesdiv.correction.style.color = me.styles.cbien
          stor.lesdiv.correction.innerHTML = cBien

          mettouvert()
          desactiveAll()

          j3pEmpty(stor.lesdiv.explikjus)

          me.cacheBoutonValider()
          me.score++

          me.typederreurs[0]++
          me.etat = 'navigation'
          me.sectionCourante()
        } else {
          // Pas de bonne réponse
          stor.lesdiv.correction.style.color = me.styles.cfaux
          stor.lesdiv.correction.innerHTML = cFaux
          if (me.isElapsed) {
            // A cause de la limite de temps :
            me._stopTimer()

            stor.lesdiv.correction.innerHTML += tempsDepasse + '<BR>'
            me.typederreurs[10]++
            me.cacheBoutonValider()

            AffCorrection(true)

            me.etat = 'navigation'
            me.sectionCourante()
          } else {
            // Réponse fausse :
            if (me.essaiCourant < me.donneesSection.nbchances) {
              stor.lesdiv.correction.innerHTML += '<br>' + essaieEncore

              AffCorrection(false)

              me.typederreurs[1]++
            } else {
              // Erreur au nème essai
              me._stopTimer()
              me.cacheBoutonValider()

              AffCorrection(true)

              if (stor.encours === 'p') {
                stor.pose = false
                stor.encours = 't'
              }

              me.typederreurs[2]++
              me.etat = 'navigation'
              me.sectionCourante()
            }
          }
        }
      } else {
        // pas de réponse
        stor.lesdiv.correction.style.color = me.styles.cfaux
        stor.lesdiv.correction.innerHTML = reponseIncomplete
        if (stor.encours === 'm') {
          stor.lesdiv.correction.innerHTML = 'Réponse incomplète ! <br> Tu dois déplacer le point ' + stor.C + ' .'
        }
        me.afficheBoutonValider()
      }

      if (stor.yaexplik) {
        if (stor.encours === 'prop') stor.lesdiv.explikjus.classList.add('explique')
      }
      if (stor.yaco) {
        if (stor.encours === 'prop') stor.lesdiv.solutionjus.classList.add('correction')
        if (stor.encours === 'comp') stor.lesdiv.solutionenc.classList.add('correction')
      }

      // Obligatoire
      me.finCorrection()
      break // case "correction":

    case 'navigation':
      if (!this.sectionTerminee()) {
        this.etat = 'enonce'
      }
      this.finNavigation()
      break // case "navigation":
  }
}
