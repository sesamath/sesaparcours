import { j3pAddElt, j3pAddTxt, j3pGetRandomInt } from 'src/legacy/core/functions'
import { j3pAffiche } from 'src/lib/mathquill/functions'
import ZoneStyleMathquill1 from 'src/legacy/outils/zoneStyleMathquill/ZoneStyleMathquill1'
import { addDefaultTable } from 'src/legacy/themes/table'
import { afficheFrac } from 'src/legacy/outils/zoneStyleMathquill/functions'
import textesGeneriques from 'src/lib/core/textes'

const { cBien, cFaux, essaieEncore, reponseIncomplete, tempsDepasse } = textesGeneriques

/**
 * Ajouter une description ici
 * @fileOverview
 * @author Tommy Barroy <t.barroy@laposte.net>
 * @since 2017-10
 */

export const params = {
  outils: ['calculatrice', 'mathquill'],
  /**
   * Ce qui est paramétrable dans l’éditeur de graphe
   * @type {LegacySectionParamsParametres}
   */
  parametres: [
    ['nbrepetitions', 9, 'entier', 'Nombre de répétitions de la section'],
    ['indication', '', 'string', 'Indication pour chaque répétition'],
    ['limite', 0, 'entier', 'Temps disponible par répétition, en secondes'],
    ['nbchances', 1, 'entier', 'Nombre d’essais par répétition'],
    // le modèle gère la compatibilité ascendante avec l’ancien paramètre MiseEnPagePlusClaire
    ['theme', 'standard', 'liste', 'Modèle pour l’affichage', ['standard', 'zonesAvecImageDeFond']]
  ]
}

/**
 * section fraction01
 * @this {Parcours}
 */
export default function main () {
  const me = this
  const stor = me.storage
  const ds = me.donneesSection
  const textes = {
    consigne1: 'Complète avec une fraction.',
    consigne2: ' $£a \\div £b$ = ',
    consigne3: " Le quotient de $£a$ par $£b$ s'écrit ",
    phrase2: 'Attention à la place du numérateur et du dénominateur.'
  }

  function yareponse () {
    if (me.isElapsed) return true
    if (stor.zonenum.reponse() === '') {
      stor.zonenum.focus()
      return false
    }
    if (stor.zoneden.reponse() === '') {
      stor.zoneden.focus()
      return false
    }
    return true
  }
  function isRepOk () {
    const n1 = parseFloat(stor.zonenum.reponse())
    const n2 = parseFloat(stor.zoneden.reponse())
    return ((n1 * stor.denominateur === n2 * stor.numerateur) && (n2 !== 0))
  }
  function initSection () {
    // Construction de la page
    me.construitStructurePage('presentation1')

    // DECLARATION DES VARIABLES
    // reponse attendue
    ds.nbitems = ds.nbetapes * ds.nbrepetitions
    stor.numerateur = 1
    stor.denominateur = 1

    // contiendra soit consigne1 soit consigne2 (1 fois sur 2)
    stor.consigneaffichee = ''
    /*
             Par convention,`
            `   me.typederreurs[0] = nombre de bonnes réponses
                me.typederreurs[1] = nombre de fois où l’élève a utilisé une chance supplémentaire
                me.typederreurs[2] = nombre de mauvaises réponses
                me.typederreurs[10] = nombre de fois où l’élève a eu faux à cause d’un temps limite
                LES AUTRES peuvent être utilisées par la détermination de la pe dans le cas d’une section qualitative
             */
    me.typederreurs = [0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0]

    me.score = 0

    me.afficheTitre('Ecriture fractionnaire')

    if (me.donneesSection.indication) me.indication(me.zones.IG, me.donneesSection.indication)
    enonceMain()
  }
  function enonceMain () {
    me.videLesZones()
    stor.lesdiv = {}
    stor.lesdiv.conteneur = j3pAddElt(me.zonesElts.MG, 'div', {
      style: me.styles.etendre('toutpetit.enonce', { padding: '10px' })
    })
    // Dans ce conteneur, on crée un div par phrase (cela forcera le retour à la ligne)
    stor.lesdiv.consigneG = addDefaultTable(stor.lesdiv.conteneur, 1, 1)[0][0]
    stor.lesdiv.etape = addDefaultTable(stor.lesdiv.conteneur, 1, 1)[0][0]
    stor.lesdiv.travail = addDefaultTable(stor.lesdiv.conteneur, 1, 1)[0][0]
    j3pAddTxt(stor.lesdiv.etape, '&nbsp;')
    stor.lesdiv.correction = j3pAddElt(me.zonesElts.MD, 'div', {
      style: me.styles.etendre('toutpetit.enonce', { padding: '10px' })
    })
    stor.lesdiv.explications = j3pAddElt(stor.lesdiv.conteneur, 'div')
    stor.lesdiv.explications.style.color = me.styles.cfaux
    me.zonesElts.MG.classList.add('fond')
    stor.lesdiv.travail.classList.add('travail')
    stor.lesdiv.consigneG.classList.add('enonce')

    const ttab = addDefaultTable(stor.lesdiv.travail, 1, 3)
    const tt = afficheFrac(ttab[0][1])
    stor.lesdiv.zoneCons = ttab[0][0]
    stor.lesdiv.zoneNum = tt[0]
    stor.lesdiv.zoneDen = tt[2]
    stor.lesdiv.zoneRep = ttab[0][2]
    stor.lesdiv.zoneRep.style.color = me.styles.toutpetit.correction.color

    stor.numerateur = j3pGetRandomInt(101, 200)
    stor.denominateur = j3pGetRandomInt(101, 200)

    if (me.questionCourante % 2 === 0) {
      stor.consigneaffichee = textes.consigne2
    } else {
      stor.consigneaffichee = textes.consigne3
    }

    j3pAffiche(stor.lesdiv.consigneG, null, '<b> <u>' + textes.consigne1 + '</b> </u>')

    j3pAffiche(stor.lesdiv.zoneCons, null, stor.consigneaffichee + '&nbsp;',
      {
        a: stor.numerateur,
        b: stor.denominateur
      })
    stor.zonenum = new ZoneStyleMathquill1(stor.lesdiv.zoneNum, {
      restric: '0123456789',
      limitenb: 9,
      limite: 5,
      enter: me.sectionCourante.bind(me)
    })
    stor.zoneden = new ZoneStyleMathquill1(stor.lesdiv.zoneDen, {
      restric: '0123456789',
      limitenb: 9,
      limite: 5,
      enter: me.sectionCourante.bind(me)
    })
    stor.zonenum.focus()
    me.finEnonce()
  }
  function affcofo () {
    stor.zonenum.corrige(false)
    stor.zoneden.corrige(false)
    stor.zonenum.barre()
    stor.zoneden.barre()
    stor.zonenum.disable()
    stor.zoneden.disable()

    stor.lesdiv.explications.classList.add('explique')
    stor.lesdiv.zoneRep.classList.add('correction')
    j3pAffiche(stor.lesdiv.zoneRep, null, ' $ \\frac{' + stor.numerateur + '}{' + stor.denominateur + '}$')
    stor.lesdiv.explications.innerHTML = textes.phrase2
  }

  switch (this.etat) {
    case 'enonce':
      // code exécuté au lancement de la section
      if (this.debutDeLaSection) {
        initSection()
      } else {
        enonceMain()
      }
      break // case "enonce":

    case 'correction':
      // On teste si une réponse a été saisie
      if (!yareponse()) {
        // ici on donne le message à afficher si une ou plusieurs zone(s) n’est pas complétée (tout d’abord on met en rouge : styles.cfaux)
        stor.lesdiv.correction.style.color = this.styles.cfaux
        stor.lesdiv.correction.innerHTML = reponseIncomplete
        this.afficheBoutonValider()
      } else {
        // Bonne réponse
        if (isRepOk()) {
          this.score++
          stor.lesdiv.correction.style.color = this.styles.cbien
          stor.lesdiv.correction.innerHTML = cBien

          stor.zonenum.corrige(true)
          stor.zoneden.corrige(true)
          stor.zonenum.disable()
          stor.zoneden.disable()

          this.typederreurs[0]++
          this.cacheBoutonValider()
          this.etat = 'navigation'
          this.sectionCourante()
        } else {
          // Pas de bonne réponse
          stor.lesdiv.correction.style.color = this.styles.cfaux

          // A cause de la limite de temps :
          if (this.isElapsed) { // limite de temps
            this._stopTimer()

            affcofo()

            stor.lesdiv.correction.innerHTML = tempsDepasse
            this.typederreurs[10]++
            this.cacheBoutonValider()
            /*
                         *   RECOPIER LA CORRECTION ICI !
                         */
            this.etat = 'navigation'
            this.sectionCourante()
          } else {
            stor.lesdiv.correction.innerHTML = cFaux

            if (this.essaiCourant < this.donneesSection.nbchances) {
              stor.lesdiv.correction.innerHTML += '<br>' + essaieEncore

              stor.zonenum.corrige(false)
              stor.zoneden.corrige(false)

              this.typederreurs[1]++
              // indication éventuelle ici
            } else {
              this._stopTimer()
              this.cacheBoutonValider()

              affcofo()

              this.typederreurs[2]++
              this.etat = 'navigation'
              this.sectionCourante()
            }
          }
        }
      }
      // Obligatoire
      this.finCorrection()
      break // case "correction":

    case 'navigation':

      if (this.sectionTerminee()) {
        // On détermine la phrase d’état renvoyée par la section
        this.parcours.pe = (this.donneesSection.nbetapes * this.score) / this.donneesSection.nbitems
        this.afficheBoutonSectionSuivante()
        this.focus('sectioncontinuer')
      } else {
        this.etat = 'enonce'
        this.afficheBoutonSuite()
        this.focus('boutoncontinuer')
      }
      // Obligatoire
      this.finNavigation()

      break // case "navigation":
  }
}
