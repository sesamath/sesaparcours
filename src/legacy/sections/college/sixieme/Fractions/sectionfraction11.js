import { j3pAddElt, j3pAjouteBouton, j3pEmpty, j3pGetRandomInt, j3pPGCD, j3pShuffle } from 'src/legacy/core/functions'
import { j3pAffiche } from 'src/lib/mathquill/functions'
import ZoneStyleMathquill1 from 'src/legacy/outils/zoneStyleMathquill/ZoneStyleMathquill1'
import { j3pCreeFenetres, j3pAfficheCroixFenetres, j3pToggleFenetres } from 'src/legacy/core/functionsJqDialog'
import { addDefaultTable } from 'src/legacy/themes/table'
import { afficheFrac } from 'src/legacy/outils/zoneStyleMathquill/functions'
import textesGeneriques from 'src/lib/core/textes'

const { cBien, cFaux, essaieEncore, reponseIncomplete, tempsDepasse } = textesGeneriques

/**
 * Ajouter une description ici
 * @fileOverview
 * @author Tommy Barroy <t.barroy@laposte.net>
 * @since 2017-10
*/

export const params = {
  outils: ['mathquill', 'calculatrice'],
  /**
   * Ce qui est paramétrable dans l’éditeur de graphe
   * @type {LegacySectionParamsParametres}
   */
  parametres: [
    ['nbrepetitions', 3, 'entier', 'Nombre de répétitions de la section'],
    ['nbchances', 2, 'entier', 'Nombre d’essais possibles'],
    ['Positifs', 'parfois', 'liste', 'Les fractions sont positives', ['toujours', 'jamais', 'parfois']],
    ['Justifie', true, 'boolean', '<u>true</u>: L’élève doit d’abord encadrer le numérateur.'],
    ['Calcul_mental', true, 'boolean', '<u>true</u>: L’exercice peut se faire de tête.'],
    ['Calculatrice', true, 'boolean', '<u>true</u>: L’élève dispose d’une calculatrice.'],
    // le modèle gère la compatibilité ascendante avec l’ancien paramètre MiseEnPagePlusClaire
    ['theme', 'standard', 'liste', 'Modèle pour l’affichage', ['standard', 'zonesAvecImageDeFond']]
  ]
}

/**
 * section fraction11
 * @this {Parcours}
 */
export default function main () {
  const me = this
  const stor = me.storage
  let ds = me.donneesSection

  function getDonnees () {
    return {
      // Nombre de répétitions de l’exercice
      nbrepetitions: 3,

      Justifie: true,
      Positifs: 'parfois',
      Calculatrice: true,
      Calcul_mental: true,
      // Si indication != "" alors un lien "Indication" s’affiche dans la zone Inférieure Gauche (this.zones.IG
      // une indication<br>de deux lignes..."
      indication: '',

      // pour le cas de presentation1bis
      // En effet pas d’indication possible pour cette présentation
      // surchargeindication: false,

      // Nombre de chances dont dispose l’él§ve pour répondre à la question
      nbchances: 2,

      //  || "presentation2" || "presentation3"  || "presentation1bis"
      structure: 'presentation1bis'
    }
  }

  function initSection () {
    let i, max
    ///

    ds = getDonnees()
    me.donneesSection = ds
    // On surcharge avec les parametres passés par le graphe
    me.surcharge()
    me.validOnEnter = false // ex donneesSection.touche_entree

    // Construction de la page
    me.construitStructurePage(me.donneesSection.structure)

    me.donneesSection.nbetapes = 1
    stor.afaire = ['enc']
    if (ds.Justifie) {
      me.donneesSection.nbetapes = 2
      stor.afaire = ['jus', 'enc']
    }
    stor.encours = stor.afaire[stor.afaire.length - 1]
    me.donneesSection.nbitems = me.donneesSection.nbetapes * me.donneesSection.nbrepetitions

    ds.lesExos = []

    let lp = [false]
    if (ds.Positifs === 'parfois') lp.push(true)
    if (ds.Positifs === 'toujours') lp = [true]
    lp = j3pShuffle(lp)
    for (i = 0; i < me.donneesSection.nbitems; i++) {
      ds.lesExos.push({ signe: lp[i % lp.length] })
    }
    ds.lesExos = j3pShuffle(ds.lesExos)

    max = 100
    lp = []
    if (ds.Calcul_mental) max = 11
    for (i = 0; i < max + 1; i++) lp.push(i)
    lp = j3pShuffle(lp)
    for (i = 0; i < me.donneesSection.nbitems; i++) {
      ds.lesExos[i].nb = lp[i % lp.length]
    }
    ds.lesExos = j3pShuffle(ds.lesExos)

    me.typederreurs = [0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0]

    me.score = 0
    const tt = 'Encadrer une fraction'

    me.afficheTitre(tt)

    enonceMain()
  }

  function faisChoixExos () {
    const e = ds.lesExos.pop()
    stor.restric = '0123456789'
    stor.nm = e.nb
    if (!e.signe) {
      stor.nm = -stor.nm
      stor.restric += '-'
    }
    stor.np = stor.nm + 1
    stor.den = j3pGetRandomInt(2, 11)
    do {
      stor.num = stor.nm * stor.den + j3pGetRandomInt(1, stor.den - 1)
    } while (j3pPGCD(Math.abs(stor.num), Math.abs(stor.den, { negativesAllowed: true, valueIfZero: 1 })) !== 1)

    stor.laf = '\\frac{' + stor.num + '}{' + stor.den + '}'
    stor.foco = false
    return e
  }

  function poseQuestion () {
    if (stor.foco) {
      j3pEmpty(stor.lesdiv.travailjus)
      j3pEmpty(stor.lesdiv.explikjus)
    }
    let t
    if (stor.afaire.length === 1 || stor.encours === 'jus') {
      j3pAffiche(stor.lesdiv.consigne, null, 'On désire encadrer la fraction $' + stor.laf + '$ entre deux entiers consécutifs.\n\n')
      stor.lesdiv.consigne.classList.add('enonce')
    }
    if (stor.encours === 'enc') {
      j3pEmpty(stor.lesdiv.enoncejus)
      j3pAffiche(stor.lesdiv.enonceenc, null, '<b>Donne l’encadrement de $' + stor.laf + '$.</b>')
      const hjk = addDefaultTable(stor.lesdiv.travailenc, 1, 3)
      j3pAffiche(hjk[0][1], null, '&nbsp;$<$&nbsp;$' + stor.laf + '$&nbsp;$<$&nbsp;')
      stor.zoneEnc1 = new ZoneStyleMathquill1(hjk[0][0], { restric: stor.restric, hasAutoKeyboard: false, limite: 6, enter: me.sectionCourante.bind(me) })
      stor.zoneEnc2 = new ZoneStyleMathquill1(hjk[0][2], { restric: stor.restric, hasAutoKeyboard: false, limite: 6, enter: me.sectionCourante.bind(me) })
      stor.lesdiv.travailenc.classList.add('travail')
      stor.lesdiv.enonceenc.classList.add('enonce')
      stor.lesdiv.travailjus.classList.remove('travail')
      stor.lesdiv.enoncejus.classList.remove('enonce')
      stor.lesdiv.solutionjus.classList.add('enonce')
    } else {
      j3pAffiche(stor.lesdiv.enoncejus, null, '<b>Donne les deux multiples du dénominateur qui encadrent le numérateur au plus proche. </b>')
      const tyu = addDefaultTable(stor.lesdiv.travailjus, 1, 5)
      t = afficheFrac(tyu[0][0])
      j3pAffiche(t[2], null, '$' + stor.den + '$')
      stor.zoneJus1 = new ZoneStyleMathquill1(t[0], { restric: stor.restric, hasAutoKeyboard: false, limite: 6, enter: me.sectionCourante.bind(me) })
      j3pAffiche(tyu[0][1], null, '&nbsp;$<$&nbsp;')
      j3pAffiche(tyu[0][3], null, '&nbsp;$<$&nbsp;')
      t = afficheFrac(tyu[0][2])
      j3pAffiche(t[2], null, '$' + stor.den + '$')
      j3pAffiche(t[0], null, '$' + stor.num + '$')
      t = afficheFrac(tyu[0][4])
      j3pAffiche(t[2], null, '$' + stor.den + '$')
      stor.zoneJus2 = new ZoneStyleMathquill1(t[0], { restric: stor.restric, hasAutoKeyboard: false, limite: 6, enter: me.sectionCourante.bind(me) })
      stor.lesdiv.travailjus.classList.add('travail')
      stor.lesdiv.enoncejus.classList.add('enonce')
    }
    stor.lesdiv.solutionjus.classList.remove('correction')
    stor.lesdiv.explikjus.classList.remove('explique')
    stor.lesdiv.solutionenc.classList.remove('correction')
    stor.lesdiv.explikenc.classList.remove('explique')
  }

  function creeLesDiv () {
    stor.lesdiv = {}
    stor.lesdiv.conteneur = j3pAddElt(me.zonesElts.MG, 'div', {
      style: me.styles.etendre('toutpetit.enonce', { padding: '10px' })
    })
    stor.lesdiv.consigne = addDefaultTable(stor.lesdiv.conteneur, 1, 1)[0][0]
    const cont = addDefaultTable(stor.lesdiv.conteneur, 8, 1)
    stor.lesdiv.enoncejus = cont[0][0]
    stor.lesdiv.travailjus = cont[1][0]
    stor.lesdiv.explikjus = cont[2][0]
    stor.lesdiv.solutionjus = cont[3][0]
    stor.lesdiv.enonceenc = cont[4][0]
    stor.lesdiv.travailenc = cont[5][0]
    stor.lesdiv.explikenc = cont[6][0]
    stor.lesdiv.solutionenc = cont[7][0]
    stor.lesdiv.calculatrice = j3pAddElt(me.zonesElts.MD, 'div', {
      style: me.styles.etendre('toutpetit.enonce', { padding: '10px' })
    })
    stor.lesdiv.correction = j3pAddElt(me.zonesElts.MD, 'div', {
      style: me.styles.etendre('toutpetit.enonce', { padding: '10px' })
    })
    stor.lesdiv.explikjus.style.color = me.styles.cfaux
    stor.lesdiv.explikenc.style.color = me.styles.cfaux
    stor.lesdiv.solutionjus.style.color = me.styles.toutpetit.correction.color
    stor.lesdiv.solutionenc.style.color = me.styles.toutpetit.correction.color
    me.zonesElts.MG.classList.add('fond')
  }

  function boutonfen () {
    j3pToggleFenetres('Calculatrice')
  }

  function yaReponse () {
    if (stor.encours === 'jus') {
      if (stor.zoneJus1.reponse() === '') {
        stor.zoneJus1.focus()
        return false
      }
      if (stor.zoneJus2.reponse() === '') {
        stor.zoneJus2.focus()
        return false
      }
    }
    if (stor.encours === 'enc') {
      if (stor.zoneEnc1.reponse() === '') {
        stor.zoneEnc1.focus()
        return false
      }
      if (stor.zoneEnc2.reponse() === '') {
        stor.zoneEnc2.focus()
        return false
      }
    }
    return true
  }

  function isRepOk () {
    let r1, r2, ok
    stor.errMult = false
    stor.errPetiProch = false
    stor.errGrandProch = false
    stor.errPetiSens = false
    stor.errGrandSens = false
    stor.errCons = false
    stor.errPG = false
    stor.errGP = false
    stor.errPtP = false
    stor.errGtG = false
    stor.errS1 = false
    stor.errS2 = false
    if (stor.encours === 'jus') {
      ok = true
      r1 = parseInt(stor.zoneJus1.reponse())
      r2 = parseInt(stor.zoneJus2.reponse())
      if (stor.zoneJus1.reponse().indexOf('-') !== stor.zoneJus1.reponse().lastIndexOf('-') || (stor.zoneJus1.reponse().indexOf('-') !== 0 && stor.zoneJus1.reponse().indexOf('-') !== -1) || stor.zoneJus1.reponse() === '-') {
        stor.errS1 = true
        stor.zoneJus1.corrige(false)
        ok = false
      } else if (r1 % stor.den !== 0) {
        stor.errMult = true
        stor.zoneJus1.corrige(false)
        ok = false
      } else if (r1 > stor.num) {
        stor.errPetiSens = true
        stor.zoneJus1.corrige(false)
        ok = false
      } else if (stor.num - r1 > stor.den) {
        stor.errPetiProch = true
        stor.zoneJus1.corrige(false)
        ok = false
      }
      if (stor.zoneJus2.reponse().indexOf('-') !== stor.zoneJus2.reponse().lastIndexOf('-') || (stor.zoneJus2.reponse().indexOf('-') !== 0 && stor.zoneJus2.reponse().indexOf('-') !== -1) || stor.zoneJus2.reponse() === '-') {
        stor.errS1 = true
        stor.zoneJus2.corrige(false)
        ok = false
      } else if (r2 % stor.den !== 0) {
        stor.errMult = true
        stor.zoneJus2.corrige(false)
        ok = false
      } else if (r2 < stor.num) {
        stor.errGrandSens = true
        stor.zoneJus2.corrige(false)
        ok = false
      } else if (r2 - stor.num > stor.den) {
        stor.errGrandProch = true
        stor.zoneJus2.corrige(false)
        ok = false
      }
      return ok
    }
    if (stor.encours === 'enc') {
      ok = true
      r1 = parseInt(stor.zoneEnc1.reponse())
      r2 = parseInt(stor.zoneEnc2.reponse())
      if (stor.zoneEnc1.reponse().indexOf('-') !== stor.zoneEnc1.reponse().lastIndexOf('-') || (stor.zoneEnc1.reponse().indexOf('-') !== 0 && stor.zoneEnc1.reponse().indexOf('-') !== -1) || stor.zoneEnc1.reponse() === '-') {
        stor.errS2 = true
        stor.zoneEnc1.corrige(false)
        ok = false
      }
      if (stor.zoneEnc2.reponse().indexOf('-') !== stor.zoneEnc2.reponse().lastIndexOf('-') || (stor.zoneEnc2.reponse().indexOf('-') !== 0 && stor.zoneEnc2.reponse().indexOf('-') !== -1) || stor.zoneEnc2.reponse() === '-') {
        stor.errS2 = true
        stor.zoneEnc2.corrige(false)
        ok = false
      }
      if (!ok) return false
      if (Math.abs(r2 - r1) !== 1) {
        stor.errCons = true
        ok = false
      }
      if (r1 > stor.num / stor.den) {
        stor.errPG = true
        ok = false
        stor.zoneEnc1.corrige(false)
      } else if (r1 !== Math.floor(stor.num / stor.den)) {
        stor.errPtP = true
        ok = false
        stor.zoneEnc1.corrige(false)
      }
      if (r2 < stor.num / stor.den) {
        stor.errGP = true
        ok = false
        stor.zoneEnc2.corrige(false)
      } else if (r2 !== Math.ceil(stor.num / stor.den)) {
        stor.errGtG = true
        ok = false
        stor.zoneEnc2.corrige(false)
      }
      return ok
    }
  }

  function desactiveAll () {
    if (stor.encours === 'jus') {
      stor.zoneJus1.disable()
      stor.zoneJus2.disable()
    }
    if (stor.encours === 'enc') {
      stor.zoneEnc1.disable()
      stor.zoneEnc2.disable()
    }
  }

  function barrelesfo () {
    if (stor.encours === 'jus') {
      stor.zoneJus1.barreIfKo()
      stor.zoneJus2.barreIfKo()
    }
    if (stor.encours === 'enc') {
      stor.zoneEnc1.barreIfKo()
      stor.zoneEnc2.barreIfKo()
    }
  }

  function mettouvert () {
    if (stor.encours === 'jus') {
      stor.zoneJus1.corrige(true)
      stor.zoneJus2.corrige(true)
    }
    if (stor.encours === 'enc') {
      stor.zoneEnc1.corrige(true)
      stor.zoneEnc2.corrige(true)
    }
  }
  function AffCorrection (bool) {
    j3pEmpty(stor.lesdiv.explikjus)
    j3pEmpty(stor.lesdiv.explikenc)
    if (stor.errMult) {
      stor.yaexplik = true
      j3pAffiche(stor.lesdiv.explikjus, null, 'Il faut choisir des multiples de $' + stor.den + '$ !\n')
    }
    if (stor.errS1) {
      stor.yaexplik = true
      j3pAffiche(stor.lesdiv.explikjus, null, 'Attention à l’écriture des nombres  !\n')
    }
    if (stor.errPetiProch) {
      stor.yaexplik = true
      j3pAffiche(stor.lesdiv.explikjus, null, '$' + stor.zoneJus1.reponse() + '$ n’est pas le multiple de $' + stor.den + '$ le plus proche de $' + stor.num + '$ !\n')
    }
    if (stor.errPetiSens) {
      stor.yaexplik = true
      j3pAffiche(stor.lesdiv.explikjus, null, '$' + stor.zoneJus1.reponse() + '$ est plus grand que $' + stor.num + '$ !\n')
    }
    if (stor.errGrandProch) {
      stor.yaexplik = true
      j3pAffiche(stor.lesdiv.explikjus, null, '$' + stor.zoneJus2.reponse() + '$ n’est pas le multiple de $' + stor.den + '$ le plus proche de $' + stor.num + '$ !\n')
    }
    if (stor.errGrandSens) {
      stor.yaexplik = true
      j3pAffiche(stor.lesdiv.explikjus, null, '$' + stor.zoneJus2.reponse() + '$ est plus petit que $' + stor.num + '$ !\n')
    }

    if (stor.errCons) {
      stor.yaexplik = true
      j3pAffiche(stor.lesdiv.explikenc, null, '$' + stor.zoneEnc1.reponse() + '$  et $' + stor.zoneEnc2.reponse() + '$ ne sont pas consécutifs !\n')
    }
    if (stor.errPG) {
      stor.yaexplik = true
      j3pAffiche(stor.lesdiv.explikenc, null, '$' + stor.zoneEnc1.reponse() + '$  est plus grand que $' + stor.laf + '$ !\n')
    }
    if (stor.errPtP) {
      stor.yaexplik = true
      j3pAffiche(stor.lesdiv.explikenc, null, '$' + stor.zoneEnc1.reponse() + '$  n’est pas assez proche de $' + stor.laf + '$ !\n')
    }
    if (stor.errGP) {
      stor.yaexplik = true
      j3pAffiche(stor.lesdiv.explikenc, null, '$' + stor.zoneEnc2.reponse() + '$  est plus petit que $' + stor.laf + '$ !\n')
    }
    if (stor.errGtG) {
      stor.yaexplik = true
      j3pAffiche(stor.lesdiv.explikenc, null, '$' + stor.zoneEnc2.reponse() + '$  n’est pas assez proche de $' + stor.laf + '$ !\n')
    }
    if (stor.errS2) {
      stor.yaexplik = true
      j3pAffiche(stor.lesdiv.explikenc, null, 'Attention à l’écriture des nombres  !\n')
    }

    if (bool) {
      barrelesfo()
      desactiveAll()
      stor.yaco = true
      stor.foco = true
      if (stor.encours === 'jus') {
        if (stor.num > 0) {
          j3pAffiche(stor.lesdiv.solutionjus, null, '$\\frac{' + (stor.num - stor.num % stor.den) + '}{' + stor.den + '} < \\frac{' + stor.num + '}{' + stor.den + '} < \\frac{' + ((stor.num - stor.num % stor.den) + stor.den) + '}{' + stor.den + '}$')
        } else {
          j3pAffiche(stor.lesdiv.solutionjus, null, '$\\frac{' + ((stor.num - stor.num % stor.den) - stor.den) + '}{' + stor.den + '} < \\frac{' + stor.num + '}{' + stor.den + '} < \\frac{' + (stor.num - stor.num % stor.den) + '}{' + stor.den + '}$')
        }
      }
      if (stor.encours === 'enc') {
        j3pAffiche(stor.lesdiv.solutionenc, null, '$' + Math.floor(stor.num / stor.den) + ' < ' + stor.laf + ' < ' + Math.ceil(stor.num / stor.den) + '$')
      }
    } else { me.afficheBoutonValider() }
  }
  function enonceMain () {
    if (me.etapeCourante === 1) {
      // A chaque répétition, on nettoie la scène
      // A commenter si besoin
      me.videLesZones()
      creeLesDiv()
      stor.Lexo = faisChoixExos()
    }

    let ss = stor.afaire.indexOf(stor.encours) + 1
    if (ss === stor.afaire.length) ss = 0
    stor.encours = stor.afaire[ss]
    poseQuestion()
    j3pEmpty(stor.lesdiv.correction)
    me.afficheBoutonValider()
    me.cacheBoutonSuite()
    j3pEmpty(stor.lesdiv.calculatrice)
    if (ds.Calculatrice) {
      me.fenetresjq = [{ name: 'Calculatrice', title: 'Une calculatrice', left: 600, top: 10 }]
      j3pCreeFenetres(me)
      j3pAfficheCroixFenetres('Calculatrice')
      j3pAjouteBouton(stor.lesdiv.calculatrice, 'Calculatrice', 'MepBoutons', 'Calculatrice', boutonfen)
    }
    me.finEnonce()
  }

  switch (this.etat) {
    case 'enonce':
      // code exécuté au lancement de la section
      if (me.debutDeLaSection) {
        initSection()
      } else {
        enonceMain()
      }

      break // case "enonce":

    case 'correction':
      // On teste si une réponse a été saisie
      stor.yaexplik = false
      stor.yaco = false
      stor.lesdiv.solutionjus.classList.remove('correction')
      stor.lesdiv.explikjus.classList.remove('explique')
      stor.lesdiv.solutionenc.classList.remove('correction')
      stor.lesdiv.explikenc.classList.remove('explique')

      if (yaReponse()) {
        // Bonne réponse
        if (isRepOk()) {
          this._stopTimer()
          stor.lesdiv.correction.style.color = this.styles.cbien
          stor.lesdiv.correction.innerHTML = cBien

          desactiveAll()
          mettouvert()

          j3pEmpty(stor.lesdiv.explikjus)
          j3pEmpty(stor.lesdiv.explikenc)

          this.cacheBoutonValider()
          this.score++

          this.typederreurs[0]++
          this.etat = 'navigation'
          this.sectionCourante()
        } else {
          // Pas de bonne réponse
          stor.lesdiv.correction.style.color = this.styles.cfaux
          stor.lesdiv.correction.innerHTML = cFaux
          if (this.isElapsed) {
            // A cause de la limite de temps :
            this._stopTimer()

            stor.lesdiv.correction.innerHTML += tempsDepasse + '<BR>'
            this.typederreurs[10]++
            this.cacheBoutonValider()

            AffCorrection(true)

            this.etat = 'navigation'
            this.sectionCourante()
          } else {
            // Réponse fausse :
            if (me.essaiCourant < me.donneesSection.nbchances) {
              stor.lesdiv.correction.innerHTML += '<br>' + essaieEncore

              AffCorrection(false)

              this.typederreurs[1]++
            } else {
              // Erreur au nème essai
              this._stopTimer()
              this.cacheBoutonValider()

              AffCorrection(true)

              if (stor.encours === 'p') {
                stor.pose = false
                stor.encours = 't'
              }

              this.typederreurs[2]++
              this.etat = 'navigation'
              this.sectionCourante()
            }
          }
        }
      } else {
        // pas de réponse
        stor.lesdiv.correction.style.color = this.styles.cfaux
        stor.lesdiv.correction.innerHTML = reponseIncomplete
        if (stor.encours === 'm') {
          stor.lesdiv.correction.innerHTML = 'Réponse incomplète ! <br> Tu dois déplacer le point ' + stor.C + ' .'
        }
        me.afficheBoutonValider()
      }

      if (stor.yaexplik) {
        if (stor.encours === 'jus') stor.lesdiv.explikjus.classList.add('explique')
        if (stor.encours === 'enc') stor.lesdiv.explikenc.classList.add('explique')
      }
      if (stor.yaco) {
        if (stor.encours === 'jus') stor.lesdiv.solutionjus.classList.add('correction')
        if (stor.encours === 'enc') stor.lesdiv.solutionenc.classList.add('correction')
      }
      // Obligatoire
      this.finCorrection()
      break // case "correction":

    case 'navigation':
      if (!this.sectionTerminee()) {
        this.etat = 'enonce'
      }
      this.finNavigation()
      break // case "navigation":
  }
}
