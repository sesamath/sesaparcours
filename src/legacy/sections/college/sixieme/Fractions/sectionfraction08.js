import { j3pAddElt, j3pAjouteBouton, j3pArrondi, j3pBarre, j3pEmpty, j3pGetRandomInt, j3pPGCD, j3pShuffle } from 'src/legacy/core/functions'
import { verifNombreBienEcrit } from 'src/legacy/outils/fonctions/ecritureNombre'
import { j3pAffiche } from 'src/lib/mathquill/functions'
import { ecrisBienMathquill, afficheFrac, affichefois } from 'src/legacy/outils/zoneStyleMathquill/functions'
import ZoneStyleMathquill1 from 'src/legacy/outils/zoneStyleMathquill/ZoneStyleMathquill1'
import { j3pCreeFenetres, j3pAfficheCroixFenetres, j3pToggleFenetres } from 'src/legacy/core/functionsJqDialog'
import { addDefaultTable } from 'src/legacy/themes/table'
import textesGeneriques from 'src/lib/core/textes'

const { cBien, cFaux, essaieEncore, reponseIncomplete, tempsDepasse } = textesGeneriques

/**
 * Ajouter une description ici
 * @fileOverview
 * @author Tommy Barroy <t.barroy@laposte.net>
 * @since 2017-10
 */
export const params = {
  outils: ['mathquill', 'calculatrice'],
  /**
   * Ce qui est paramétrable dans l’éditeur de graphe
   * @type {LegacySectionParamsParametres}
   */
  parametres: [
    ['nbrepetitions', 9, 'entier', 'Nombre de répétitions de la section'],
    ['indication', '', 'string', 'Indication pour chaque répétition'],
    ['limite', 0, 'entier', 'Temps disponible par répétition, en secondes'],
    ['nbchances', 2, 'entier', 'Nombre d’essais par répétition'],
    ['Simple', true, 'boolean', '<u>true</u>: Le dénominateur doit être le plus petit possible .'],
    ['negatif', true, 'boolean', '<u>true</u>: Il peut y avoir des numérateurs négatifs .'],
    ['Detail_Reduction', true, 'boolean', '<u>true</u>: Justification demandée.'],
    ['Espace', false, 'boolean', '<u>true</u>: Les espaces entre les groupe de 3 sont exigés'],
    ['Calculatrice', false, 'boolean', '<u>true</u>: Une calculatrice est disponible'],
    // le modèle gère la compatibilité ascendante avec l’ancien paramètre MiseEnPagePlusClaire
    ['theme', 'standard', 'liste', 'Modèle pour l’affichage', ['standard', 'zonesAvecImageDeFond']]
  ]
}

/**
 * section fraction08
 * @this {Parcours}
 */
export default function main () {
  const me = this
  let ds = me.donneesSection
  const stor = me.storage

  function getDonnees () {
    return {
      // Nombre de répétitions de l’exercice
      nbrepetitions: 3,

      Simple: true,
      negatif: true,
      Detail_Reduction: true,
      Espace: false,
      Calculatrice: false,

      // Si indication != "" alors un lien "Indication" s’affiche dans la zone Inférieure Gauche (this.zones.IG
      // une indication<br>de deux lignes..."
      indication: '',

      // pour le cas de presentation1bis
      // En effet pas d’indication possible pour cette présentation
      // surchargeindication: false,

      // Nombre de chances dont dispose l’él§ve pour répondre à la question
      nbchances: 2,

      //  || "presentation2" || "presentation3"  || "presentation1bis"
      structure: 'presentation1bis'
    }
  }
  function initSection () {
    let i
    ds = getDonnees()
    me.donneesSection = ds
    // On surcharge avec les parametres passés par le graphe
    me.surcharge()
    me.validOnEnter = false // ex donneesSection.touche_entree

    // Construction de la page
    me.construitStructurePage(me.donneesSection.structure)
    me.donneesSection.nbetapes = 1
    me.donneesSection.nbitems = me.donneesSection.nbetapes * me.donneesSection.nbrepetitions

    ds.lesExos = []
    let lp = [1, 2, 3, 4]
    for (i = 0; i < ds.nbitems; i++) {
      ds.lesExos[i] = { quest: lp[i % lp.length] }
    }
    ds.lesExos = j3pShuffle(ds.lesExos)

    lp = ['++']
    if (ds.negatif) lp = ['++', '--', '+-', '-+']
    lp = j3pShuffle(lp)
    for (i = 0; i < ds.nbitems; i++) {
      ds.lesExos[i].signe = lp[i % lp.length]
    }
    ds.lesExos = j3pShuffle(ds.lesExos)

    stor.restric = '0123456789 '
    if (ds.negatif) stor.restric += '-'

    me.typederreurs = [0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0]

    me.score = 0
    let tt = 'Réduire au même dénominateur'
    if (ds.Comparer) tt += ' puis comparer'

    me.afficheTitre(tt)
    enonceMain()
  }
  function faisChoixExos () {
    const e = ds.lesExos.pop()
    let mulfac = [2, 3, 4, 5, 10, 100]
    mulfac = j3pShuffle(mulfac)
    mulfac = mulfac.pop()
    switch (e.quest) {
      case 1:
        stor.den1 = j3pGetRandomInt(2, 13)
        stor.den2 = premsavec(stor.den1, 2, 13)
        break
      case 2:
        stor.den1 = j3pGetRandomInt(2, 13)
        stor.den2 = j3pArrondi(mulfac * stor.den1, 0)
        break
      case 3:
        stor.den2 = j3pGetRandomInt(2, 13)
        stor.den1 = j3pArrondi(mulfac * stor.den2, 0)
        break
      case 4:
        stor.den1 = premsavec(mulfac, 2, 13)
        stor.den2 = premsavec(stor.den1, 2, 13)
        stor.den1 = j3pArrondi(mulfac * stor.den1, 0)
        stor.den2 = j3pArrondi(mulfac * stor.den2, 0)
        break
    }
    stor.num1 = premsavec(stor.den1, 1, 13)
    stor.num2 = premsavec(stor.den2, 1, 13)
    if (e.signe[0] === '-') stor.num1 = -stor.num1
    if (e.signe[1] === '-') stor.num2 = -stor.num2
    return e
  }
  function poseQuestion () {
    stor.FOA = stor.FOB = undefined
    stor.repnum = stor.repnumB = stor.repden = stor.repdenB = stor.repnumD2 = stor.repdenD2 = stor.repnumD2B = stor.repdenD2B = undefined
    const z1 = ecrisBienMathquill(stor.den1)
    const z2 = ecrisBienMathquill(stor.den2)
    j3pAffiche(stor.lesdiv.consigne2, null, '<b>Réduis $A$ et $B$ au même dénominateur.</b>\n')
    if (ds.Simple) j3pAffiche(stor.lesdiv.consigne2, null, '<i>Le dénominateur doit être le plus petit possible.</i>\n')
    j3pAffiche(stor.lesdiv.consigne2, null, '\n')
    stor.monDiv = addDefaultTable(stor.lesdiv.travail, 1, 3)
    stor.monDivA = addDefaultTable(stor.monDiv[0][0], 4, 2)
    stor.monDivB = addDefaultTable(stor.monDiv[0][2], 4, 2)
    stor.monDiv[0][1].style.width = '50px'
    stor.monDiv[0][0].style.verticalAlign = 'top'
    stor.monDiv[0][2].style.verticalAlign = 'top'
    j3pAffiche(stor.monDivA[0][0], null, '$A=$')
    j3pAffiche(stor.monDivB[0][0], null, '$B=$')
    j3pAffiche(stor.monDivA[0][1], null, '$\\frac{' + stor.num1 + '}{' + z1 + '}$')
    j3pAffiche(stor.monDivB[0][1], null, '$\\frac{' + stor.num2 + '}{' + z2 + '}$')
    j3pAjouteBouton(stor.monDivA[1][1], 'ModifA', '', 'Modifier A', modifA)
    j3pAjouteBouton(stor.monDivB[1][1], 'ModifB', '', 'Modifier B', modifB)
    me.afficheBoutonValider()
  }

  function creeLesDiv () {
    stor.lesdiv = {}
    stor.lesdiv.conteneur = j3pAddElt(me.zonesElts.MG, 'div', {
      style: me.styles.etendre('toutpetit.enonce', { padding: '10px' })
    })
    stor.lesdiv.consigne1 = addDefaultTable(stor.lesdiv.conteneur, 1, 1)[0][0]
    stor.lesdiv.calculatrice = j3pAddElt(me.zonesElts.MD, 'div', {
      style: me.styles.etendre('toutpetit.enonce', { padding: '10px' })
    })
    stor.lesdiv.consigne = j3pAddElt(stor.lesdiv.consigne1, 'div')
    stor.lesdiv.consigne2 = j3pAddElt(stor.lesdiv.consigne1, 'div')
    const tt = addDefaultTable(stor.lesdiv.conteneur, 1, 3)
    stor.lesdiv.travail = tt[0][0]
    tt[0][1].style.width = '20px'
    stor.lesdiv.explications = j3pAddElt(stor.lesdiv.conteneur, 'div')
    stor.lesdiv.solution = tt[0][2]
    stor.lesdiv.correction = j3pAddElt(me.zonesElts.MD, 'div', {
      style: me.styles.etendre('toutpetit.enonce', { padding: '10px' })
    })
    stor.lesdiv.explications.style.color = me.styles.cfaux
    stor.lesdiv.solution.style.color = me.styles.toutpetit.correction.color
    me.zonesElts.MG.classList.add('fond')
  }
  function modifA () {
    let t
    faisFOA()
    faisFOB()
    j3pEmpty(stor.lesdiv.explications)
    j3pEmpty(stor.monDivA[1][1])
    j3pAffiche(stor.monDivA[2][0], null, '$A=$')
    t = afficheFrac(stor.monDivA[2][1])
    stor.repnum = new ZoneStyleMathquill1(t[0], { restric: stor.restric, limite: 8, limitenb: 1, enter: me.sectionCourante.bind(me), onmodif: () => faisFOA() })
    stor.repden = new ZoneStyleMathquill1(t[2], { restric: stor.restric.replace('-', ''), limite: 8, limitenb: 1, enter: me.sectionCourante.bind(me), onmodif: () => faisFOA() })
    if (ds.Detail_Reduction) {
      j3pAffiche(stor.monDivA[1][0], null, '$A=$')
      t = afficheFrac(stor.monDivA[1][1])
      let t2 = affichefois(t[0])
      j3pAffiche(t2[0], null, '$' + stor.num1 + '$')
      stor.repnumD2 = new ZoneStyleMathquill1(t2[2], { restric: stor.restric, limite: 8, limitenb: 1, enter: me.sectionCourante.bind(me), onmodif: () => faisFOA() })
      t2 = affichefois(t[2])
      j3pAffiche(t2[0], null, '$' + stor.den1 + '$')
      stor.repdenD2 = new ZoneStyleMathquill1(t2[2], { restric: stor.restric.replace('-', ''), limite: 8, limitenb: 1, enter: me.sectionCourante.bind(me), onmodif: () => faisFOA() })
    }
    j3pAjouteBouton(stor.monDivA[3][1], 'ModifA', '', 'Annuler', supA)
  }
  function modifB () {
    let t
    j3pEmpty(stor.lesdiv.explications)
    faisFOA()
    faisFOB()
    j3pEmpty(stor.monDivB[1][1])
    j3pAffiche(stor.monDivB[2][0], null, '$B=$')
    t = afficheFrac(stor.monDivB[2][1])
    stor.repnumB = new ZoneStyleMathquill1(t[0], { restric: stor.restric, limite: 8, limitenb: 1, enter: me.sectionCourante.bind(me) })
    stor.repdenB = new ZoneStyleMathquill1(t[2], { restric: stor.restric.replace('-', ''), limite: 8, limitenb: 1, enter: me.sectionCourante.bind(me) })
    if (ds.Detail_Reduction) {
      j3pAffiche(stor.monDivB[1][0], null, '$B=$')
      t = afficheFrac(stor.monDivB[1][1])
      let t2 = affichefois(t[0])
      j3pAffiche(t2[0], null, '$' + stor.num2 + '$')
      stor.repnumD2B = new ZoneStyleMathquill1(t2[2], { restric: stor.restric, limite: 8, limitenb: 1, enter: me.sectionCourante.bind(me) })
      t2 = affichefois(t[2])
      j3pAffiche(t2[0], null, '$' + stor.den2 + '$')
      stor.repdenD2B = new ZoneStyleMathquill1(t2[2], { restric: stor.restric.replace('-', ''), limite: 8, limitenb: 1, enter: me.sectionCourante.bind(me) })
    }
    j3pAjouteBouton(stor.monDivB[3][1], 'ModifB', '', 'Annuler', supB)
  }
  function supA () {
    stor.repnum.disable()
    stor.repden.disable()
    stor.repnum = stor.repden = undefined
    if (stor.repnumD2 !== undefined) {
      stor.repnumD2.disable()
      stor.repdenD2.disable()
      stor.repnumD2 = stor.repdenD2 = undefined
    }
    j3pEmpty(stor.lesdiv.explications)
    faisFOA()
    j3pEmpty(stor.monDivA[1][1])
    j3pEmpty(stor.monDivA[1][0])
    j3pEmpty(stor.monDivA[2][0])
    j3pEmpty(stor.monDivA[2][1])
    j3pEmpty(stor.monDivA[3][1])
    j3pAjouteBouton(stor.monDivA[1][1], 'ModifA', '', 'Modifier A', modifA)
  }
  function supB () {
    stor.repnumB.disable()
    stor.repdenB.disable()
    stor.repnumB = stor.repdenB = undefined
    if (stor.repnumD2B !== undefined) {
      stor.repnumD2B.disable()
      stor.repdenD2B.disable()
      stor.repnumD2B = stor.repdenD2B = undefined
    }
    j3pEmpty(stor.lesdiv.explications)
    faisFOB()
    j3pEmpty(stor.monDivB[1][1])
    j3pEmpty(stor.monDivB[1][0])
    j3pEmpty(stor.monDivB[2][0])
    j3pEmpty(stor.monDivB[2][1])
    j3pEmpty(stor.monDivB[3][1])
    j3pAjouteBouton(stor.monDivB[1][1], 'ModifB', '', 'Modifier B', modifB)
  }
  function faisFOA (bool) {
    let col = ''
    if (bool === false) col = me.styles.cfaux
    if (bool === true) col = me.styles.cbien
    stor.FOA = bool
    stor.monDiv[0][0].style.color = col
  }
  function faisFOB (bool) {
    let col = ''
    stor.FOB = bool
    if (bool === false) col = me.styles.cfaux
    if (bool === true) col = me.styles.cbien
    stor.monDiv[0][2].style.color = col
  }
  function premsavec (nb, lim1, lim2) {
    // on cherche un nb premier avec nb
    let sol
    let i = 0 // protection contre les boucles infinies
    do {
      sol = j3pGetRandomInt(lim1, lim2)
      i++
    } while (j3pPGCD(nb, sol, { negativesAllowed: true, valueIfZero: 1 }) !== 1 && i < 100)
    if (i === 100) throw Error('trouvé aucun nombre premier avec ' + nb)
    return sol
  }
  function boutonfen () {
    j3pToggleFenetres('Calculatrice')
  }

  function yaReponse () {
    if (me.isElapsed) return true
    if (stor.repnum !== undefined) {
      if (stor.repnum.reponse() === '') {
        stor.repnum.focus()
        return false
      }
      if (stor.repden.reponse() === '') {
        stor.repden.focus()
        return false
      }
    }
    if (stor.repnumB !== undefined) {
      if (stor.repnumB.reponse() === '') {
        stor.repnumB.focus()
        return false
      }
      if (stor.repdenB.reponse() === '') {
        stor.repdenB.focus()
        return false
      }
    }
    if (stor.repnumD2 !== undefined) {
      if (stor.repnumD2.reponse() === '') {
        stor.repnumD2.focus()
        return false
      }
      if (stor.repdenD2.reponse() === '') {
        stor.repdenD2.focus()
        return false
      }
    }
    if (stor.repnumD2B !== undefined) {
      if (stor.repnumD2B.reponse() === '') {
        stor.repnumD2B.focus()
        return false
      }
      if (stor.repdenD2B.reponse() === '') {
        stor.repdenD2B.focus()
        return false
      }
    }
    return true
  }
  function isRepOk () {
    let ok, nA, dA, nB, dB, DnA, DnB, DdA, DdB, okbuf
    stor.errPasM = false
    stor.errtabA = false
    stor.errtabB = false
    stor.errA = false
    stor.errB = false
    stor.errC = false
    stor.errMulM = false
    stor.errEcrit = false
    stor.lerrEcrit = ''
    stor.errSimple = false

    ok = true
    let fovir = false
    if ((stor.repnum === undefined) && (stor.repnumB === undefined)) {
      stor.errPasM = true
      faisFOA(false)
      faisFOB(false)
      return false
    }
    if (stor.repnum !== undefined) {
      nA = stor.repnum.reponse()
      dA = stor.repden.reponse()
      okbuf = verifNombreBienEcrit(nA, ds.Espace, true)
      if (!okbuf.good) {
        stor.repnum.corrige(false)
        stor.errEcrit = true
        stor.lerrEcrit += okbuf.remede + '<br>'
        fovir = true
      }
      nA = okbuf.nb
      okbuf = verifNombreBienEcrit(dA, ds.Espace, true)
      if (!okbuf.good) {
        stor.repden.corrige(false)
        stor.errEcrit = true
        stor.lerrEcrit += okbuf.remede + '<br>'
        fovir = true
      }
      dA = okbuf.nb
    } else {
      nA = stor.num1
      dA = stor.den1
    }
    if (stor.repnumB !== undefined) {
      nB = stor.repnumB.reponse()
      dB = stor.repdenB.reponse()
      okbuf = verifNombreBienEcrit(nB, ds.Espace, true)
      if (!okbuf.good) {
        stor.repnumB.corrige(false)
        stor.errEcrit = true
        stor.lerrEcrit += okbuf.remede + '<br>'
        fovir = true
      }
      nB = okbuf.nb
      okbuf = verifNombreBienEcrit(dB, ds.Espace, true)
      if (!okbuf.good) {
        stor.repdenB.corrige(false)
        stor.errEcrit = true
        stor.lerrEcrit += okbuf.remede + '<br>'
        fovir = true
      }
      dB = okbuf.nb
    } else {
      nB = stor.num2
      dB = stor.den2
    }
    if (ds.Detail_Reduction) {
      if (stor.repnumD2 !== undefined) {
        DnA = stor.repnumD2.reponse()
        DdA = stor.repdenD2.reponse()

        okbuf = verifNombreBienEcrit(DnA, ds.Espace, true)
        if (!okbuf.good) {
          stor.repnumD2.corrige(false)
          stor.errEcrit = true
          stor.lerrEcrit += okbuf.remede + '<br>'
          fovir = true
        }
        DnA = okbuf.nb
        okbuf = verifNombreBienEcrit(DdA, ds.Espace, true)
        if (!okbuf.good) {
          stor.repdenD2.corrige(false)
          stor.errEcrit = true
          stor.lerrEcrit += okbuf.remede + '<br>'
          fovir = true
        }
        DdA = okbuf.nb

        if (DnA !== DdA) {
          stor.errMulM = true
          faisFOA(false)
          ok = false
        }
      }

      if (stor.repnumD2B !== undefined) {
        DnB = stor.repnumD2B.reponse()
        DdB = stor.repdenD2B.reponse()

        okbuf = verifNombreBienEcrit(DnB, ds.Espace, true)
        if (!okbuf.good) {
          stor.repnumD2B.corrige(false)
          stor.errEcrit = true
          stor.lerrEcrit += okbuf.remede + '<br>'
          fovir = true
        }
        DnB = okbuf.nb
        okbuf = verifNombreBienEcrit(DdB, ds.Espace, true)
        if (!okbuf.good) {
          stor.repdenD2B.corrige(false)
          stor.errEcrit = true
          stor.lerrEcrit += okbuf.remede + '<br>'
          fovir = true
        }
        DdB = okbuf.nb

        // verif mult par mm
        if (DnB !== DdB) {
          stor.errMulM = true
          ok = false
          faisFOB(false)
        }
        // verif ca donne bien la fin
      }
    }
    if (fovir) return false

    // verif den dans 2 tables
    if (dA % stor.den1 !== 0) {
      stor.errtabA = true
      faisFOA(false)
      ok = false
    }
    if (dB % stor.den2 !== 0) {
      stor.errtabB = true
      faisFOB(false)
      ok = false
    }
    // verif egal
    if (!egalfrac(nA, dA, stor.num1, stor.den1)) {
      stor.errA = true
      faisFOA(false)
      ok = false
    }
    if (!egalfrac(nB, dB, stor.num2, stor.den2)) {
      stor.errB = true
      faisFOB(false)
      ok = false
    }
    // verif mm deno
    if (dA !== dB) {
      stor.errPasM = true
      ok = false
    }
    // autres verifs
    if (ds.Detail_Reduction) {
      if (stor.repnumD2 !== undefined) {
        if ((stor.num1 * DnA !== nA) || (stor.den1 * DnA !== dA)) {
          stor.errC = true
          faisFOA(false)
          ok = false
        }
      }
      if (stor.repnumD2B !== undefined) {
        if ((stor.num2 * DnB !== nB) || (stor.den2 * DnB !== dB)) {
          stor.errC = true
          faisFOB(false)
          ok = false
        }
      }
    }
    if (ok && ds.Simple) {
      if (dA !== (stor.den1 * stor.den2 / j3pPGCD(stor.den1, stor.den2, { negativesAllowed: true, valueIfZero: 1 }))) {
        ok = false
        faisFOA(false)
        faisFOB(false)
        stor.errSimple = true
      }
    }
    return ok
  }
  function egalfrac (a, b, c, d) {
    if (b === 0 || d === 0) return false
    return (j3pArrondi(a * d, 0) === j3pArrondi(b * c, 0))
  }
  function desactivezone () {
    if (stor.repnum !== undefined) {
      stor.repnum.disable()
      stor.repden.disable()
    } else {
      j3pEmpty(stor.monDivA[1][1])
    }
    if (stor.repnumB !== undefined) {
      stor.repnumB.disable()
      stor.repdenB.disable()
    } else {
      j3pEmpty(stor.monDivB[1][1])
    }
    if (stor.repnumD2 !== undefined) {
      stor.repnumD2.disable()
      stor.repdenD2.disable()
    }
    if (stor.repnumD2B !== undefined) {
      stor.repnumD2B.disable()
      stor.repdenD2B.disable()
    }
    j3pEmpty(stor.monDivA[3][1])
    j3pEmpty(stor.monDivB[3][1])
  }
  function passetoutvert () {
    faisFOA(true)
    faisFOB(true)
  }
  function barrelesfo () {
    if (stor.FOA === false) j3pBarre(stor.monDiv[0][0])
    if (stor.FOB === false) j3pBarre(stor.monDiv[0][2])
  }
  function AffCorrection (bool) {
    if (stor.errPasM) {
      stor.yaexplik = true
      j3pAffiche(stor.lesdiv.explications, null, 'Les fractions $A$ et $B$ n’ont pas le même dénominateur !\n')
    }
    if (stor.errtabA) {
      stor.yaexplik = true
      j3pAffiche(stor.lesdiv.explications, null, 'Le nouveau dénominateur de $A$ n’est pas dans la table de $' + stor.den1 + '$ !\n')
    }
    if (stor.errtabB) {
      stor.yaexplik = true
      j3pAffiche(stor.lesdiv.explications, null, 'Le nouveau dénominateur de $B$ n’est pas dans la table de $' + stor.den2 + '$ !\n')
    }
    if (stor.errSimple) {
      stor.yaexplik = true
      j3pAffiche(stor.lesdiv.explications, null, 'Tu peux trouver un dénominateur plus petit !\n')
    }
    if (stor.errA) {
      stor.yaexplik = true
      j3pAffiche(stor.lesdiv.explications, null, 'La nouvelle écriture de $A$ n’est pas égale à celle de l’énoncé !\n')
    }
    if (stor.errB) {
      stor.yaexplik = true
      j3pAffiche(stor.lesdiv.explications, null, 'La nouvelle écriture de $B$ n’est pas égale à celle de l’énoncé !\n')
    }
    if (stor.errC) {
      stor.yaexplik = true
      j3pAffiche(stor.lesdiv.explications, null, 'Erreur de calcul !\n')
    }
    if (stor.errMulM) {
      stor.yaexplik = true
      j3pAffiche(stor.lesdiv.explications, null, 'Tu dois multiplier le numérateur et le dénominateur par le même nombre (non nul) !\n')
    }
    if (stor.errEcrit) {
      stor.yaexplik = true
      j3pAffiche(stor.lesdiv.explications, null, stor.lerrEcrit)
    }

    if (bool) {
      stor.yaco = true
      let a, b, c, e
      desactivezone()
      barrelesfo()
      const tabHJK = addDefaultTable(stor.lesdiv.solution, 3, 5)
      tabHJK[0][2].style.width = '50px'
      j3pAffiche(tabHJK[0][0], null, '$A=$')
      j3pAffiche(tabHJK[0][3], null, '$B=$')
      const z1 = ecrisBienMathquill(stor.den1)
      const z2 = ecrisBienMathquill(stor.den2)
      j3pAffiche(tabHJK[0][1], null, '$\\frac{' + stor.num1 + '}{' + z1 + '}$')
      j3pAffiche(tabHJK[0][4], null, '$\\frac{' + stor.num2 + '}{' + z2 + '}$')
      if (stor.Lexo.quest !== 3) {
        j3pAffiche(tabHJK[2][0], null, '$A=$')
        a = (stor.den1 * stor.den2 / j3pPGCD(stor.den1, stor.den2, { negativesAllowed: true, valueIfZero: 1 }))
        b = a / stor.den1
        c = ecrisBienMathquill(a)
        e = ecrisBienMathquill(b * stor.num1)
        j3pAffiche(tabHJK[2][1], null, '$\\frac{' + e + '}{' + c + '}$')
        j3pAffiche(tabHJK[1][0], null, '$A=$')
        j3pAffiche(tabHJK[1][1], null, '$\\frac{' + stor.num1 + ' \\times ' + b + ' }{' + z1 + ' \\times ' + b + ' }$')
      }
      if (stor.Lexo.quest !== 2) {
        j3pAffiche(tabHJK[2][3], null, '$B=$')
        a = (stor.den1 * stor.den2 / j3pPGCD(stor.den1, stor.den2, { negativesAllowed: true, valueIfZero: 1 }))
        b = a / stor.den2
        c = ecrisBienMathquill(a)
        e = ecrisBienMathquill(b * stor.num2)
        j3pAffiche(tabHJK[2][4], null, '$\\frac{' + e + '}{' + c + '}$')
        j3pAffiche(tabHJK[1][3], null, '$B=$')
        j3pAffiche(tabHJK[1][4], null, '$\\frac{' + stor.num2 + ' \\times ' + b + ' }{' + z2 + ' \\times ' + b + ' }$')
      }
    } else { me.afficheBoutonValider() }
  }
  function enonceMain () {
    if (me.etapeCourante === 1) {
      // A chaque répétition, on nettoie la scène
      // A commenter si besoin
      me.videLesZones()
      creeLesDiv()
      stor.Lexo = faisChoixExos()
      stor.lesdiv.travail.classList.add('travail')
      stor.lesdiv.consigne1.classList.add('enonce')
    }
    poseQuestion()
    if (ds.Calculatrice) {
      me.fenetresjq = [{ name: 'Calculatrice', title: 'Une calculatrice', left: 600, top: 10 }]
      j3pCreeFenetres(me)
      j3pAfficheCroixFenetres('Calculatrice')
      j3pAjouteBouton(stor.lesdiv.calculatrice, 'Calculatrice', 'MepBoutons', 'Calculatrice', boutonfen)
    }
    me.finEnonce()
  }

  switch (this.etat) {
    case 'enonce':
      // code exécuté au lancement de la section
      if (me.debutDeLaSection) {
        initSection()
      } else {
        enonceMain()
      }
      break // case "enonce":

    case 'correction':
      // On teste si une réponse a été saisie
      stor.yaexplik = false
      stor.lesdiv.explications.innerHTML = ''
      stor.lesdiv.correction.innerHTML = ''
      stor.yaco = false

      if (yaReponse()) {
        // Bonne réponse
        if (isRepOk()) {
          this._stopTimer()
          this.score++
          stor.lesdiv.correction.style.color = this.styles.cbien
          stor.lesdiv.correction.innerHTML = cBien

          desactivezone()
          passetoutvert()

          this.typederreurs[0]++
          this.cacheBoutonValider()
          this.etat = 'navigation'
          this.sectionCourante()
        } else {
          // Pas de bonne réponse
          stor.lesdiv.correction.style.color = this.styles.cfaux
          stor.lesdiv.correction.innerHTML = cFaux
          if (me.isElapsed) {
            // A cause de la limite de temps :
            this._stopTimer()

            stor.lesdiv.correction.innerHTML += tempsDepasse + '<BR> Regarde la correction.'
            this.typederreurs[10]++
            this.cacheBoutonValider()

            AffCorrection(true)

            this.etat = 'navigation'
            this.sectionCourante()
          } else {
            // Réponse fausse :
            if (me.essaiCourant < me.donneesSection.nbchances) {
              stor.lesdiv.correction.innerHTML += '<br>' + essaieEncore

              AffCorrection(false)

              this.typederreurs[1]++
            } else {
              // Erreur au nème essai
              stor.lesdiv.correction.innerHTML += '<BR> Regarde la correction.'

              this._stopTimer()
              this.cacheBoutonValider()

              AffCorrection(true)

              this.typederreurs[2]++
              this.etat = 'navigation'
              this.sectionCourante()
            }
          }
        }
      } else {
        // pas de réponse
        stor.lesdiv.correction.style.color = this.styles.cfaux
        stor.lesdiv.correction.innerHTML = reponseIncomplete
        me.afficheBoutonValider()
      }

      if (ds.theme === 'zonesAvecImageDeFond') {
        if (stor.yaexplik) {
          stor.lesdiv.explications.classList.add('explique')
        }
        if (stor.yaco) {
          stor.lesdiv.solution.classList.add('correction')
        }
      }

      // Obligatoire
      this.finCorrection()
      break // case "correction":

    case 'navigation':
      if (!this.sectionTerminee()) {
        this.etat = 'enonce'
      }
      this.finNavigation()
      break // case "navigation":
  }
}
