import { j3pAddElt, j3pArrondi, j3pGetRandomInt, j3pShowError, j3pShuffle } from 'src/legacy/core/functions'
import ListeDeroulante from 'src/legacy/outils/listeDeroulante/ListeDeroulante'
import { verifNombreBienEcrit } from 'src/legacy/outils/fonctions/ecritureNombre'
import { j3pAffiche } from 'src/lib/mathquill/functions'
import ZoneStyleMathquill1 from 'src/legacy/outils/zoneStyleMathquill/ZoneStyleMathquill1'
import { addDefaultTable } from 'src/legacy/themes/table'
import textesGeneriques from 'src/lib/core/textes'

const { cBien, cFaux, reponseIncomplete, tempsDepasse } = textesGeneriques

/**
 * Ajouter une description ici
 * @fileOverview
 * @author Tommy Barroy <t.barroy@laposte.net>
 * @since 2017-10
*/
/* global THREE */

export const params = {
  outils: ['mathquill', 'three'],
  /**
   * Ce qui est paramétrable dans l’éditeur de graphe
   * @type {LegacySectionParamsParametres}
   */
  parametres: [
    ['nbrepetitions', 3, 'entier', 'Nombre de répétitions de la section'],
    ['nbchances', 2, 'entier', 'Nombre d’essais possibles.'],
    ['limite', 0, 'entier', 'Temps disponible par répétition, en secondes'],
    ['Max', 48, 'entier', 'Maximum de cubes composant le solide (entre 1 et 27)'],
    ['Min', 1, 'entier', 'Minimum de cubes composant le solide (entre 1 et 27)'],
    ['Uv', true, 'boolean', '<u>true</u>: L’unité varie de 1 à 4 cubes'],
    ['Entier', true, 'boolean', '<u>true</u>: Le volume est forcément un entier'],
    // le modèle gère la compatibilité ascendante avec l’ancien paramètre MiseEnPagePlusClaire
    ['theme', 'standard', 'liste', 'Modèle pour l’affichage', ['standard', 'zonesAvecImageDeFond']]
  ]
}

/**
 * section volumecompt
 * @this {Parcours}
 */
export default function main () {
  const me = this
  const ds = me.donneesSection
  const stor = me.storage

  function initSection () {
    let i
    me.validOnEnter = false // ex donneesSection.touche_entree

    // Construction de la page
    me.construitStructurePage('presentation3')

    if (isNaN(ds.Max)) ds.Max = 27
    if (isNaN(ds.Min)) ds.Min = 1
    ds.Max = Math.min(27, ds.Max)
    ds.Min = Math.max(1, ds.Min)
    if (ds.Max < ds.Min) ds.Max = ds.Min

    ds.lesExos = []
    let lp = [1]
    if (ds.Uv) lp = [1, 2, 4]

    lp = j3pShuffle(lp)
    for (i = 0; i < ds.nbitems; i++) {
      ds.lesExos[i] = { unite: lp[i % lp.length] }
    }
    ds.lesExos = j3pShuffle(ds.lesExos)
    me.afficheTitre('Volume par comptage')
    enonceMain()
  }
  function creeLesDiv () {
    stor.lesdiv = {}
    stor.lesdiv.conteneur = j3pAddElt(me.zones.MG, 'div', '', { style: me.styles.etendre('toutpetit.enonce', { padding: '10px' }) })

    stor.lesdiv.consigne = addDefaultTable(stor.lesdiv.conteneur, 1, 1)[0][0]
    const tab1 = addDefaultTable(stor.lesdiv.conteneur, 2, 1)
    const tab2 = addDefaultTable(tab1[0][0], 1, 5)
    stor.lesdiv.zonefig = tab2[0][0]
    tab2[0][1].style.width = '20px'
    stor.lesdiv.zoneunite = tab2[0][2]
    tab2[0][3].style.width = '20px'
    const tab4 = addDefaultTable(tab2[0][4], 4, 1)
    stor.lesdiv.travail = tab4[1][0]
    stor.lesdiv.explications = tab4[2][0]
    stor.lesdiv.solution = tab4[3][0]
    stor.lesdiv.correction = tab4[0][0]
    stor.lesdiv.explications.style.color = me.styles.cfaux
    stor.lesdiv.solution.style.color = me.styles.toutpetit.correction.color
  }
  function faisChoixExos () {
    const e = ds.lesExos.pop()
    let cpt = 0
    do {
      if (ds.Entier) {
        stor.nbcube = e.unite * j3pGetRandomInt(ds.Min, 27)
        stor.restric = '0123456789'
      } else {
        stor.nbcube = j3pGetRandomInt(ds.Min, ds.Max)
        stor.restric = '0123456789.,'
      }
      cpt++
    } while (((stor.nbcube < ds.Min) || (stor.nbcube > ds.Max)) && (cpt < 1000))
    if (cpt === 1000) stor.nbcube = e.unite
    return e
  }

  function poseQuestion () {
    let buf = ''

    if (stor.Lexo.ua !== 1) buf = '<i>(avec la bonne unité de volume)</i> '
    const tt = `<b>Donne le volume du solide ci-dessous ${buf}:</b>\n`
    const t2 = 'Volume = &nbsp;'
    j3pAffiche(stor.lesdiv.consigne, null, tt)

    const tatab = addDefaultTable(stor.lesdiv.travail, 1, 3)
    j3pAffiche(tatab[0][0], null, t2)
    j3pAffiche(stor.lesdiv.zonefig, null, '&nbsp;&nbsp;<b><u>Solide</u></b>')
    j3pAffiche(stor.lesdiv.zoneunite, null, '<b><u>Unité de volume (u.v.)</u></b>')
    stor.zone = new ZoneStyleMathquill1(tatab[0][1], { restric: stor.restric, limitenb: 20, limite: 40, hasAutoKeyboard: stor.clvspe, enter: function () { me.sectionCourante() }, inverse: true })
    stor.liste = ListeDeroulante.create(tatab[0][2], ['Choisir', 'u.v.', 'u.a.', 'cm²', 'cm³'], { sensHaut: true })
    makefig()
    me.afficheBoutonValider()
  }

  function makefig () {
    stor.lesdiv.zonefig.style.border = 'solid black 1px'
    let i, x, y, z
    stor.x = stor.y = stor.z = 0
    stor.captu = false
    try {
      stor.renderer = new THREE.WebGLRenderer({ alpha: true })
    } catch (error) {
      console.error(error)
      j3pShowError('Votre navigateur ne permet pas d’affichage 3D, impossible de continuer')
      return
    }
    stor.renderer.setClearColor(0x000000, 0)
    stor.renderer.setSize(200, 300)
    stor.lesdiv.zonefig.appendChild(stor.renderer.domElement)
    stor.scene = new THREE.Scene()
    stor.camera = new THREE.PerspectiveCamera(50, 0.5, 1, 1000)
    stor.camera.position.set(0, 0, 200)
    stor.scene.add(stor.camera)
    stor.groupe = new THREE.Group()
    let line, are
    const mterare = new THREE.MeshBasicMaterial({ color: 0x000000, transparent: false })
    const materlb = [
      new THREE.MeshBasicMaterial({ color: 0xff0000, transparent: false, opacity: 1, side: THREE.DoubleSide }),
      new THREE.MeshBasicMaterial({ color: 0xff0000, transparent: false, opacity: 1, side: THREE.DoubleSide }),
      new THREE.MeshBasicMaterial({ color: 0x0000ff, transparent: false, opacity: 1, side: THREE.DoubleSide }),
      new THREE.MeshBasicMaterial({ color: 0x0000ff, transparent: false, opacity: 1, side: THREE.DoubleSide }),
      new THREE.MeshBasicMaterial({ color: 0xff00ff, transparent: false, opacity: 1, side: THREE.DoubleSide }),
      new THREE.MeshBasicMaterial({ color: 0xff00ff, transparent: false, opacity: 1, side: THREE.DoubleSide })
    ]
    const materlr = new THREE.MeshFaceMaterial(materlb)

    let sol = []
    sol.push({
      x: 1,
      y: 1,
      z: 1
    })
    let cpt = 0
    if (stor.nbcube > 1) {
      do {
        cpt++
        x = j3pGetRandomInt(0, 2)
        y = j3pGetRandomInt(0, 2)
        z = j3pGetRandomInt(0, 2)
        if (libre({
          x,
          y,
          z
        }, sol)) {
          sol.push({
            x,
            y,
            z
          })
        }
      } while (sol.length < stor.nbcube && cpt < 1000)
      if (cpt === 1000) j3pShowError('pas réussi')
    }

    stor.listCube = []
    stor.listAre = []
    for (i = 0; i < sol.length; i++) {
      line = new THREE.CubeGeometry(20, 20, 20)
      // for (var j = 0; j < line.faces.length; j ++) {
      //  line.faces[ j ].color.setHex(0xff00000);
      // }
      line.faces[0].color.setHex(0xff00000)
      line.faces[1].color.setHex(0xff00000)
      stor.listCube.push(new THREE.Mesh(line, materlr))
      stor.listCube[stor.listCube.length - 1].position.set(sol[i].x * 20 - 20, sol[i].y * 20 - 20, sol[i].z * 20 - 20)
      stor.listCube[stor.listCube.length - 1].geometry.faces[0].color.setHex(0xff00000)
      are = new THREE.BoxGeometry(20, 1, 1, 1, 1, 1)
      stor.listAre.push(new THREE.Mesh(are, mterare))
      stor.listAre[stor.listAre.length - 1].position.set(sol[i].x * 20 - 20, sol[i].y * 20 - 10, sol[i].z * 20 - 10)
      stor.listAre[stor.listAre.length - 1].rotation.x = 0
      stor.listAre[stor.listAre.length - 1].rotation.y = 0
      stor.listAre[stor.listAre.length - 1].rotation.z = 0
      stor.listAre.push(new THREE.Mesh(are, mterare))
      stor.listAre[stor.listAre.length - 1].position.set(sol[i].x * 20 - 20, sol[i].y * 20 - 30, sol[i].z * 20 - 30)
      stor.listAre[stor.listAre.length - 1].rotation.x = 0
      stor.listAre[stor.listAre.length - 1].rotation.y = 0
      stor.listAre[stor.listAre.length - 1].rotation.z = 0
      stor.listAre.push(new THREE.Mesh(are, mterare))
      stor.listAre[stor.listAre.length - 1].position.set(sol[i].x * 20 - 20, sol[i].y * 20 - 10, sol[i].z * 20 - 30)
      stor.listAre[stor.listAre.length - 1].rotation.x = 0
      stor.listAre[stor.listAre.length - 1].rotation.y = 0
      stor.listAre[stor.listAre.length - 1].rotation.z = 0
      stor.listAre.push(new THREE.Mesh(are, mterare))
      stor.listAre[stor.listAre.length - 1].position.set(sol[i].x * 20 - 20, sol[i].y * 20 - 30, sol[i].z * 20 - 10)
      stor.listAre[stor.listAre.length - 1].rotation.x = 0
      stor.listAre[stor.listAre.length - 1].rotation.y = 0
      stor.listAre[stor.listAre.length - 1].rotation.z = 0
      /// //
      stor.listAre.push(new THREE.Mesh(are, mterare))
      stor.listAre[stor.listAre.length - 1].position.set(sol[i].x * 20 - 10, sol[i].y * 20 - 10, sol[i].z * 20 - 20)
      stor.listAre[stor.listAre.length - 1].rotation.x = 0
      stor.listAre[stor.listAre.length - 1].rotation.y = Math.PI / 2
      stor.listAre[stor.listAre.length - 1].rotation.z = 0
      stor.listAre.push(new THREE.Mesh(are, mterare))
      stor.listAre[stor.listAre.length - 1].position.set(sol[i].x * 20 - 30, sol[i].y * 20 - 30, sol[i].z * 20 - 20)
      stor.listAre[stor.listAre.length - 1].rotation.x = 0
      stor.listAre[stor.listAre.length - 1].rotation.y = Math.PI / 2
      stor.listAre[stor.listAre.length - 1].rotation.z = 0
      stor.listAre.push(new THREE.Mesh(are, mterare))
      stor.listAre[stor.listAre.length - 1].position.set(sol[i].x * 20 - 10, sol[i].y * 20 - 30, sol[i].z * 20 - 20)
      stor.listAre[stor.listAre.length - 1].rotation.x = 0
      stor.listAre[stor.listAre.length - 1].rotation.y = Math.PI / 2
      stor.listAre[stor.listAre.length - 1].rotation.z = 0
      stor.listAre.push(new THREE.Mesh(are, mterare))
      stor.listAre[stor.listAre.length - 1].position.set(sol[i].x * 20 - 30, sol[i].y * 20 - 10, sol[i].z * 20 - 20)
      stor.listAre[stor.listAre.length - 1].rotation.x = 0
      stor.listAre[stor.listAre.length - 1].rotation.y = Math.PI / 2
      stor.listAre[stor.listAre.length - 1].rotation.z = 0
      /// //
      stor.listAre.push(new THREE.Mesh(are, mterare))
      stor.listAre[stor.listAre.length - 1].position.set(sol[i].x * 20 - 10, sol[i].y * 20 - 20, sol[i].z * 20 - 10)
      stor.listAre[stor.listAre.length - 1].rotation.x = 0
      stor.listAre[stor.listAre.length - 1].rotation.z = Math.PI / 2
      stor.listAre[stor.listAre.length - 1].rotation.y = 0
      stor.listAre.push(new THREE.Mesh(are, mterare))
      stor.listAre[stor.listAre.length - 1].position.set(sol[i].x * 20 - 30, sol[i].y * 20 - 20, sol[i].z * 20 - 30)
      stor.listAre[stor.listAre.length - 1].rotation.x = 0
      stor.listAre[stor.listAre.length - 1].rotation.z = Math.PI / 2
      stor.listAre[stor.listAre.length - 1].rotation.y = 0
      stor.listAre.push(new THREE.Mesh(are, mterare))
      stor.listAre[stor.listAre.length - 1].position.set(sol[i].x * 20 - 10, sol[i].y * 20 - 20, sol[i].z * 20 - 30)
      stor.listAre[stor.listAre.length - 1].rotation.x = 0
      stor.listAre[stor.listAre.length - 1].rotation.z = Math.PI / 2
      stor.listAre[stor.listAre.length - 1].rotation.y = 0
      stor.listAre.push(new THREE.Mesh(are, mterare))
      stor.listAre[stor.listAre.length - 1].position.set(sol[i].x * 20 - 30, sol[i].y * 20 - 20, sol[i].z * 20 - 10)
      stor.listAre[stor.listAre.length - 1].rotation.x = 0
      stor.listAre[stor.listAre.length - 1].rotation.z = Math.PI / 2
      stor.listAre[stor.listAre.length - 1].rotation.y = 0
    }

    for (i = 0; i < stor.listCube.length; i++) {
      stor.groupe.add(stor.listCube[i])
    }
    for (i = 0; i < stor.listAre.length; i++) {
      stor.groupe.add(stor.listAre[i])
    }

    stor.scene.add(stor.groupe)
    // on effectue le rendu de la scène
    stor.renderer.render(stor.scene, stor.camera)
    stor.renderer.domElement.addEventListener('mousedown', animateDown, false)
    stor.renderer.domElement.addEventListener('mouseup', animateStop, false)
    stor.renderer.domElement.addEventListener('mousemove', animateMov, false)
    stor.renderer.domElement.style.cursor = 'pointer'
    stor.limt = j3pGetRandomInt(10, 90) / 10

    stor.lesdiv.zoneunite.style.border = 'solid black 1px'
    stor.x2 = stor.y2 = stor.z2 = 0
    stor.captu2 = false
    stor.renderer2 = new THREE.WebGLRenderer({ alpha: true })
    stor.renderer2.setClearColor(0x000000, 0)
    stor.renderer2.setSize(200, 300)
    stor.lesdiv.zoneunite.appendChild(stor.renderer2.domElement)
    stor.scene2 = new THREE.Scene()
    stor.camera2 = new THREE.PerspectiveCamera(50, 0.5, 1, 1000)
    stor.camera2.position.set(0, 0, 200)
    stor.scene2.add(stor.camera2)
    stor.groupe2 = new THREE.Group()

    sol = []
    sol.push({
      x: 1,
      y: 1,
      z: 1
    })
    if (stor.Lexo.unite > 1) {
      sol.push({
        x: 2,
        y: 1,
        z: 1
      })
    }
    if (stor.Lexo.unite > 2) {
      sol.push({
        x: 1,
        y: 2,
        z: 1
      })
      sol.push({
        x: 2,
        y: 2,
        z: 1
      })
    }
    if (stor.Lexo.unite > 4) {
      sol.push({
        x: 1,
        y: 1,
        z: 2
      })
      sol.push({
        x: 1,
        y: 2,
        z: 2
      })
      sol.push({
        x: 2,
        y: 1,
        z: 2
      })
      sol.push({
        x: 2,
        y: 2,
        z: 2
      })
    }

    stor.listCube = []
    stor.listAre = []
    for (i = 0; i < sol.length; i++) {
      line = new THREE.CubeGeometry(20, 20, 20)
      // for (var j = 0; j < line.faces.length; j ++) {
      //  line.faces[ j ].color.setHex(0xff00000);
      // }
      line.faces[0].color.setHex(0xff00000)
      line.faces[1].color.setHex(0xff00000)
      stor.listCube.push(new THREE.Mesh(line, materlr))
      stor.listCube[stor.listCube.length - 1].position.set(sol[i].x * 20 - 20, sol[i].y * 20 - 20, sol[i].z * 20 - 20)
      stor.listCube[stor.listCube.length - 1].geometry.faces[0].color.setHex(0xff00000)
      are = new THREE.BoxGeometry(20, 1, 1, 1, 1, 1)
      stor.listAre.push(new THREE.Mesh(are, mterare))
      stor.listAre[stor.listAre.length - 1].position.set(sol[i].x * 20 - 20, sol[i].y * 20 - 10, sol[i].z * 20 - 10)
      stor.listAre[stor.listAre.length - 1].rotation.x = 0
      stor.listAre[stor.listAre.length - 1].rotation.y = 0
      stor.listAre[stor.listAre.length - 1].rotation.z = 0
      stor.listAre.push(new THREE.Mesh(are, mterare))
      stor.listAre[stor.listAre.length - 1].position.set(sol[i].x * 20 - 20, sol[i].y * 20 - 30, sol[i].z * 20 - 30)
      stor.listAre[stor.listAre.length - 1].rotation.x = 0
      stor.listAre[stor.listAre.length - 1].rotation.y = 0
      stor.listAre[stor.listAre.length - 1].rotation.z = 0
      stor.listAre.push(new THREE.Mesh(are, mterare))
      stor.listAre[stor.listAre.length - 1].position.set(sol[i].x * 20 - 20, sol[i].y * 20 - 10, sol[i].z * 20 - 30)
      stor.listAre[stor.listAre.length - 1].rotation.x = 0
      stor.listAre[stor.listAre.length - 1].rotation.y = 0
      stor.listAre[stor.listAre.length - 1].rotation.z = 0
      stor.listAre.push(new THREE.Mesh(are, mterare))
      stor.listAre[stor.listAre.length - 1].position.set(sol[i].x * 20 - 20, sol[i].y * 20 - 30, sol[i].z * 20 - 10)
      stor.listAre[stor.listAre.length - 1].rotation.x = 0
      stor.listAre[stor.listAre.length - 1].rotation.y = 0
      stor.listAre[stor.listAre.length - 1].rotation.z = 0
      /// //
      stor.listAre.push(new THREE.Mesh(are, mterare))
      stor.listAre[stor.listAre.length - 1].position.set(sol[i].x * 20 - 10, sol[i].y * 20 - 10, sol[i].z * 20 - 20)
      stor.listAre[stor.listAre.length - 1].rotation.x = 0
      stor.listAre[stor.listAre.length - 1].rotation.y = Math.PI / 2
      stor.listAre[stor.listAre.length - 1].rotation.z = 0
      stor.listAre.push(new THREE.Mesh(are, mterare))
      stor.listAre[stor.listAre.length - 1].position.set(sol[i].x * 20 - 30, sol[i].y * 20 - 30, sol[i].z * 20 - 20)
      stor.listAre[stor.listAre.length - 1].rotation.x = 0
      stor.listAre[stor.listAre.length - 1].rotation.y = Math.PI / 2
      stor.listAre[stor.listAre.length - 1].rotation.z = 0
      stor.listAre.push(new THREE.Mesh(are, mterare))
      stor.listAre[stor.listAre.length - 1].position.set(sol[i].x * 20 - 10, sol[i].y * 20 - 30, sol[i].z * 20 - 20)
      stor.listAre[stor.listAre.length - 1].rotation.x = 0
      stor.listAre[stor.listAre.length - 1].rotation.y = Math.PI / 2
      stor.listAre[stor.listAre.length - 1].rotation.z = 0
      stor.listAre.push(new THREE.Mesh(are, mterare))
      stor.listAre[stor.listAre.length - 1].position.set(sol[i].x * 20 - 30, sol[i].y * 20 - 10, sol[i].z * 20 - 20)
      stor.listAre[stor.listAre.length - 1].rotation.x = 0
      stor.listAre[stor.listAre.length - 1].rotation.y = Math.PI / 2
      stor.listAre[stor.listAre.length - 1].rotation.z = 0
      stor.listAre.push(new THREE.Mesh(are, mterare))
      stor.listAre[stor.listAre.length - 1].position.set(sol[i].x * 20 - 10, sol[i].y * 20 - 20, sol[i].z * 20 - 10)
      stor.listAre[stor.listAre.length - 1].rotation.x = 0
      stor.listAre[stor.listAre.length - 1].rotation.z = Math.PI / 2
      stor.listAre[stor.listAre.length - 1].rotation.y = 0
      stor.listAre.push(new THREE.Mesh(are, mterare))
      stor.listAre[stor.listAre.length - 1].position.set(sol[i].x * 20 - 30, sol[i].y * 20 - 20, sol[i].z * 20 - 30)
      stor.listAre[stor.listAre.length - 1].rotation.x = 0
      stor.listAre[stor.listAre.length - 1].rotation.z = Math.PI / 2
      stor.listAre[stor.listAre.length - 1].rotation.y = 0
      stor.listAre.push(new THREE.Mesh(are, mterare))
      stor.listAre[stor.listAre.length - 1].position.set(sol[i].x * 20 - 10, sol[i].y * 20 - 20, sol[i].z * 20 - 30)
      stor.listAre[stor.listAre.length - 1].rotation.x = 0
      stor.listAre[stor.listAre.length - 1].rotation.z = Math.PI / 2
      stor.listAre[stor.listAre.length - 1].rotation.y = 0
      stor.listAre.push(new THREE.Mesh(are, mterare))
      stor.listAre[stor.listAre.length - 1].position.set(sol[i].x * 20 - 30, sol[i].y * 20 - 20, sol[i].z * 20 - 10)
      stor.listAre[stor.listAre.length - 1].rotation.x = 0
      stor.listAre[stor.listAre.length - 1].rotation.z = Math.PI / 2
      stor.listAre[stor.listAre.length - 1].rotation.y = 0
    }

    for (i = 0; i < stor.listCube.length; i++) {
      stor.groupe2.add(stor.listCube[i])
    }
    for (i = 0; i < stor.listAre.length; i++) {
      stor.groupe2.add(stor.listAre[i])
    }

    stor.scene2.add(stor.groupe2)
    // on effectue le rendu de la scène
    stor.renderer2.render(stor.scene2, stor.camera2)
    stor.renderer2.domElement.addEventListener('mousedown', animateDown2, false)
    stor.renderer2.domElement.addEventListener('mouseup', animateStop2, false)
    stor.renderer2.domElement.addEventListener('mousemove', animateMov2, false)
    stor.renderer2.domElement.style.cursor = 'pointer'

    animate()
  }

  function animateDown (ev) {
    stor.captu = true
    stor.x = ev.clientX
    stor.y = ev.clientY
  }
  function animateStop (ev) {
    stor.captu = false
  }
  function animateMov (ev) {
    if (!stor.captu) return
    const X = ev.clientX
    const Y = ev.clientY
    if ((Math.abs(stor.groupe.rotation.x) % (2 * Math.PI) < 3 * Math.PI / 2) && (Math.abs(stor.groupe.rotation.x) % (2 * Math.PI) > Math.PI / 2)) {
      stor.groupe.rotation.y -= Math.atan((X - stor.x) / 100)
    } else {
      stor.groupe.rotation.y += Math.atan((X - stor.x) / 100)
    }

    stor.groupe.rotation.x += Math.atan((Y - stor.y) / 100)
    stor.x = X
    stor.y = Y
    stor.renderer.render(stor.scene, stor.camera)
  }
  function animateDown2 (ev) {
    stor.captu2 = true
    stor.x2 = ev.clientX
    stor.y2 = ev.clientY
  }
  function animateStop2 (ev) {
    stor.captu2 = false
  }
  function animateMov2 (ev) {
    if (!stor.captu2) return
    const X = ev.clientX
    const Y = ev.clientY
    if ((Math.abs(stor.groupe2.rotation.x) % (2 * Math.PI) < 3 * Math.PI / 2) && (Math.abs(stor.groupe2.rotation.x) % (2 * Math.PI) > Math.PI / 2)) {
      stor.groupe2.rotation.y -= Math.atan((X - stor.x) / 100)
    } else {
      stor.groupe2.rotation.y += Math.atan((X - stor.x) / 100)
    }

    stor.groupe2.rotation.x += Math.atan((Y - stor.y) / 100)
    stor.x = X
    stor.y = Y
    stor.renderer2.render(stor.scene2, stor.camera2)
  }

  function animate () {
    if (stor.groupe.rotation.x > stor.limt) return
    if (stor.groupe2.rotation.x > stor.limt) return
    if (stor.captu) return
    if (stor.captu2) return
    requestAnimationFrame(animate)
    stor.groupe.rotation.y += 0.02
    stor.groupe.rotation.x += 0.01
    stor.renderer.render(stor.scene, stor.camera)
    stor.groupe2.rotation.y += 0.02
    stor.groupe2.rotation.x += 0.01
    stor.renderer2.render(stor.scene2, stor.camera2)
  }

  function libre (ki, koi, b) {
    let u
    for (u = 0; u < koi.length; u++) {
      if ((koi[u].x === ki.x) && (koi[u].y === ki.y) && (koi[u].z === ki.z)) return false
    }
    const colle = !libre2({ x: ki.x + 1, y: ki.y, z: ki.z }, koi) || !libre2({ x: ki.x - 1, y: ki.y, z: ki.z }, koi) || !libre2({ x: ki.x, y: ki.y + 1, z: ki.z }, koi) || !libre2({ x: ki.x, y: ki.y - 1, z: ki.z }, koi) || !libre2({ x: ki.x, y: ki.y, z: ki.z + 1 }, koi) || !libre2({ x: ki.x, y: ki.y, z: ki.z - 1 }, koi)
    return colle
  }

  function libre2 (ki, koi) {
    let u
    for (u = 0; u < koi.length; u++) {
      if ((koi[u].x === ki.x) && (koi[u].y === ki.y) && (koi[u].z === ki.z)) return false
    }
    return true
  }
  function yaReponse () {
    if (me.isElapsed) return true
    if (stor.zone.reponse() === '') {
      stor.zone.focus()
      return false
    }
    if (!stor.liste.changed) {
      stor.liste.focus()
      return false
    }
    return true
  }

  function isRepOk () {
    stor.errEcrit = false
    stor.errRemdEcrit = []
    stor.errUnit = false
    stor.errCompt = false
    stor.errInv = false
    stor.errCUnit = false

    let repa = stor.zone.reponse()
    const repu = stor.liste.reponse
    const tt = verifNombreBienEcrit(repa, false, true)
    if (!tt.good) {
      stor.zone.corrige(false)
      stor.errEcrit = true
      stor.errRemdEcrit.push(tt.remede)
      return false
    }
    const atta = j3pArrondi(stor.nbcube / stor.Lexo.unite, 2)
    const attfoa = stor.nbcube
    const attu = 'u.v.'
    repa = parseFloat(repa.replace(',', '.'))
    if (repa !== atta) {
      stor.zone.corrige(false)
      if (repa === attfoa) {
        stor.errUnit = true
      } else {
        stor.errCompt = true
      }
      return false
    }
    if (repu !== attu) {
      stor.errCUnit = true
      stor.liste.corrige(false)
      return false
    }
    return true
  }

  function desactivezone () {
    stor.zone.disable()
    stor.liste.disable()
  }

  function barrelesfaux () {
    stor.liste.barreIfKo()
    stor.zone.barreIfKo()
  }

  function passetoutvert () {
    stor.liste.corrige(true)
    stor.zone.corrige(true)
  }

  function AffCorrection (bool) {
    let buf
    if (stor.errEcrit) {
      stor.yaexplik = true
      j3pAffiche(stor.lesdiv.explications, null, stor.errRemdEcrit[0])
    }
    if (stor.errUnit) {
      buf = ' d’aire '
      stor.yaexplik = true
      if (stor.Lexo.question === 'perim') buf = ' de longueur '
      j3pAffiche(stor.lesdiv.explications, null, 'Observe bien l’unité ' + buf + ' donnée !')
    }
    if (stor.errCompt) {
      stor.yaexplik = true
      j3pAffiche(stor.lesdiv.explications, null, 'Tu n’as pas bien compté !')
    }

    if (bool) {
      desactivezone()
      barrelesfaux()
      stor.yaco = true

      j3pAffiche(stor.lesdiv.solution, null, 'Volume = $' + j3pArrondi(stor.nbcube / stor.Lexo.unite, 2) + '$ u.v.')
    } else { me.afficheBoutonValider() }
  }

  function enonceMain () {
    stor.raz = true
    if (((me.questionCourante % ds.nbetapes) === 1) || (ds.nbetapes === 1)) {
      // A chaque répétition, on nettoie la scène
      // A commenter si besoin
      me.videLesZones()
      if (ds.structure === 'presentation3') {
        me.ajouteBoutons()
        me.cacheBoutonSectionSuivante()
      }
      // ou dans le cas de presentation3
      // this.ajouteBoutons()
      creeLesDiv()
      stor.Lexo = faisChoixExos()
      stor.lesdiv.travail.classList.add('travail')
      stor.lesdiv.consigne.classList.add('enonce')
      stor.lesdiv.zonefig.classList.add('enonce')
      stor.lesdiv.zoneunite.classList.add('enonce')
      stor.lesdiv.solution.classList.remove('correction')
      stor.lesdiv.explications.classList.remove('explique')
      me.zonesElts.MG.classList.add('fond')
    }
    poseQuestion()
    me.cacheBoutonSuite()
    me.finEnonce()
  }

  switch (this.etat) {
    case 'enonce':
      // code exécuté au lancement de la section
      if (me.debutDeLaSection) {
        initSection()
      } else {
        stor.raz = true
        enonceMain()
      }
      break // case "enonce":

    case 'correction':
      // On teste si une réponse a été saisie
      stor.yaexplik = false
      stor.lesdiv.explications.innerHTML = ''
      stor.lesdiv.correction.innerHTML = ''
      stor.yaco = false

      if (yaReponse()) {
        // Bonne réponse
        if (isRepOk()) {
          this._stopTimer()
          stor.lesdiv.correction.style.color = this.styles.cbien
          stor.lesdiv.correction.innerHTML = cBien
          this.score++

          desactivezone()
          passetoutvert()
          this.cacheBoutonValider()

          this.typederreurs[0]++
          this.etat = 'navigation'
          this.sectionCourante()
        } else {
          // Pas de bonne réponse
          stor.lesdiv.correction.style.color = this.styles.cfaux
          stor.lesdiv.correction.innerHTML = cFaux
          if (this.isElapsed) {
            // A cause de la limite de temps :
            this._stopTimer()

            stor.lesdiv.correction.innerHTML += tempsDepasse + '<BR>'
            this.typederreurs[10]++
            this.cacheBoutonValider()

            AffCorrection(true)

            this.etat = 'navigation'
            this.sectionCourante()
          } else {
            // Réponse fausse :
            if (this.essaiCourant < ds.nbchances) {
              stor.lesdiv.correction.innerHTML += '<br> Essaie encore.'

              AffCorrection(false)

              this.typederreurs[1]++
            } else {
              // Erreur au nème essai
              this._stopTimer()
              this.cacheBoutonValider()

              AffCorrection(true)

              this.typederreurs[2]++
              this.etat = 'navigation'
              this.sectionCourante()
            }
          }
        }
      } else {
        // pas de réponse
        stor.lesdiv.correction.style.color = this.styles.cfaux
        stor.lesdiv.correction.innerHTML = reponseIncomplete
        me.afficheBoutonValider()
      }

      if (ds.theme === 'zonesAvecImageDeFond') {
        if (stor.yaexplik) {
          stor.lesdiv.explications.classList.add('explique')
        }
        if (stor.yaco) {
          stor.lesdiv.solution.classList.add('correction')
        }
      }

      // Obligatoire
      this.finCorrection()
      break // case "correction":

    case 'navigation':
      if (!this.sectionTerminee()) {
        this.etat = 'enonce'
      }
      this.finNavigation()
      break // case "navigation":
  }
}
