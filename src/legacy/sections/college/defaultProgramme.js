export default function getDefaultProgramme () {
  return {
    titre: 'Connaitre les carrés',
    consigne: '',
    nbCouples: 3,

    // 'expl' les cartes sont notées en dur
    // 'gen' les cartes sont générés par une fonction ( au moins face B )
    carteChoix: 'expl',

    // les cartes en dur
    CartesExpl: [['le carré de 1', '1'], ['2 au carré', '4'], ['$3 \\times 3$', '9'], ['$4^2$', '16'], ['5 fois 5', '25']],

    // la methode de génération
    CartesGen: {
      // 'liste': liste contient un tableau de string ou de { type mathgraph, content: { dimx, dimy, txtFigure } }
      // 'genAC': genA contient une fig mg qui renvoie une chaine
      // 'genAMG' : genAMG contient une fig mathraph et dimx dimy
      modeA: 'liste',
      liste: ['le carré de 1', '2 au carré', '$3 \\times 3$', '$4^2$', '5 fois 5'],
      genA: undefined,
      genAMG: undefined,

      // 'genC': genB contient une fig mg qui renvoie une chaine
      // 'genM' : genBMG contient une fig mathraph et dimx dimy
      modeB: 'genC',
      genBMG: undefined,

      minA: 1,
      maxA: 12
    }
  }
}
