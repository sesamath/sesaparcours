import { j3pAddContent, j3pGetRandomBool, j3pGetRandomInt } from 'src/legacy/core/functions'
import { getNewScratch } from 'src/legacy/outils/scratch/loaders'
import { addDefaultTable } from 'src/legacy/themes/table'

export const params = {
  outils: ['mathquill'],
  /**
   * Ce qui est paramétrable dans l’éditeur de graphe
   * @type {LegacySectionParamsParametres}
   */
  parametres: [
    ['nbrepetitions', 4, 'entier', 'Nombre de répétitions de la section'],
    ['nbchances', 0, 'entier', 'Nombre d’essais possibles (illimité si 0)'],
    ['PasAPas', true, 'boolean', "<u>true</u>: L’élève peut lancer son programme en mode 'Pas à pas'."],
    ['Sequence', true, 'boolean', '<u>true</u>: Chaque bloc a une durée d’exécution d’environ 1 seconde.'],
    ['difficultemin', 1, 'entier', 'Définis la longueur du programme attendu (minimum 1)'],
    ['difficultemax', 8, 'entier', 'Définis la longueur du programme attendu (maximum 8)'],
    ['difficulte', 'Progressive', 'liste', 'Détermination approximative du nombre de blocs (entre min et max)', ['Aléatoire', 'Progressive']],
    ['tolerance', 0, 'entier', 'Nombre de blocs supplementaires acceptés'],
    ['CacheTest', false, 'boolean', '<u>true</u>: L’élève ne voit pas l’action de son programme lors d la correction.']
  ]
}

/**
 * section blokbrasobjectif
 * @this {Parcours}
 */
export default function main () {
  const me = this
  const stor = me.storage
  const ds = me.donneesSection
  let scratch = stor.scratch

  function ajdep (quoi) {
    stor.nbblocattendu++
    if (quoi === 'T') {
      scratch.params.scratchProgDonnees.lesordres.splice(0, 0, { type: 'tourner_bras' })
      stor.sensbrasbase = 1 - stor.sensbrasbase
    } else {
      if (stor.hauteurbrasbase === 0) {
        scratch.params.scratchProgDonnees.lesordres.splice(0, 0, { type: 'descendre_bras' })
      } else {
        scratch.params.scratchProgDonnees.lesordres.splice(0, 0, { type: 'monter_bras' })
      }
      stor.hauteurbrasbase = 1 - stor.hauteurbrasbase
    }
  }

  async function initSection () {
    ds.theme = 'zonesAvecImageDeFond'
    scratch = await getNewScratch()
    stor.scratch = scratch
    me.validOnEnter = false // ex donneesSection.touche_entree
    scratch.initParams(me)
    scratch.testFlag = function () {
    }
    scratch.testFlagFin = function () {
      let ok
      let mess = ''
      switch (stor.letap) {
        case 1 :
          ok = (scratch.params.scratchProgDonnees.donneesbras.tapis1 !== 0)
          break
        case 2 :
          ok = (scratch.params.scratchProgDonnees.donneesbras.tapis2 !== 0)

          break
        case 3 :
          ok = (scratch.params.scratchProgDonnees.donneesbras.tapis3 !== 0)

          break
        case 4 :
          ok = (scratch.params.scratchProgDonnees.donneesbras.tapis4 !== 0)

          break
      }
      if (!ok) { mess = 'Le bras doit déposer l’objet sur le tapis ' + stor.letap + ' !' }
      return { ok, mess }
    }
    scratch.params.AutoriseSupprimeVariable = false
    scratch.params.AutoriseRenomeVariable = false

    stor.dejafait2 = []
    stor.dejafait = []
    stor.dejafait1 = []
    stor.dejafait3 = []

    ds.difficultemin = Math.max(1, ds.difficultemin)
    ds.difficultemax = Math.min(8, ds.difficultemax)
    if (ds.difficultemax < ds.difficultemin) {
      ds.difficultemin = ds.difficultemax
    }

    me.construitStructurePage({ structure: 'presentation3', ratioGauche: 0.6 })
    me.stockage = [0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0]

    me.score = 0
    me.afficheTitre('Réaliser un objectif - bras mécanique')

    // if (ds.indication !="") me.indication(me.zones.IG,ds.indication);

    scratch.params.scratchProgDonnees.menu = []
    scratch.params.scratchProgDonnees.progdeb = []
    scratch.params.scratchProgDonnees.progdeb.push({ type: 'Drapo', editable: 'false', deletable: 'false', movable: 'false', x: '20', y: '20' })

    scratch.params.scratchProgDonnees.menu.push({ type: 'catégorie', nom: 'Déplacements', coul1: '#4C97FF', coul2: '#3373CC', contenu: [] })
    scratch.params.scratchProgDonnees.menu[0].contenu.push({ type: 'monter_bras' })
    scratch.params.scratchProgDonnees.menu[0].contenu.push({ type: 'descendre_bras' })
    scratch.params.scratchProgDonnees.menu[0].contenu.push({ type: 'tourner_bras' })
    scratch.params.scratchProgDonnees.menu.push({ type: 'catégorie', nom: 'Bras', coul1: '#D65CD6', coul2: '#BD42BD', contenu: [] })
    scratch.params.scratchProgDonnees.menu[1].contenu.push({ type: 'reduire_bras' })
    scratch.params.scratchProgDonnees.menu[1].contenu.push({ type: 'agrandir_bras' })
    scratch.params.scratchProgDonnees.menu.push({ type: 'catégorie', nom: 'Pince', coul1: '#40BF4A', coul2: '#389438', contenu: [] })
    scratch.params.scratchProgDonnees.menu[2].contenu.push({ type: 'prendre_bras' })
    scratch.params.scratchProgDonnees.menu[2].contenu.push({ type: 'deposer_bras' })
    enonceMain()
  }

  function enonceMain () {
    me.videLesZones()

    scratch.params.scratchProgDonnees.lesordres = []

    let elem = []

    let petitcompte = 0
    do {
      petitcompte++
      var u = j3pGetRandomInt(1, 4)
    } while ((petitcompte < 100) && (stor.dejafait.indexOf(u) !== -1))
    stor.dejafait.push(u)
    stor.letap = u
    switch (u) {
      case 1:
        stor.hauteurbrasbase = 0
        stor.sensbrasbase = 0
        break
      case 2:
        stor.hauteurbrasbase = 1
        stor.sensbrasbase = 0
        break
      case 3:
        stor.hauteurbrasbase = 0
        stor.sensbrasbase = 1
        break
      case 4:
        stor.hauteurbrasbase = 1
        stor.sensbrasbase = 1
        break
    }

    stor.longueurbrasbase = 1
    elem.push({ obj: j3pGetRandomInt(1, 2), ou: 'bras' })

    stor.nbblocdif = j3pGetRandomInt(ds.difficultemin, ds.difficultemax)
    if (ds.difficulte === 'Progressive') {
      stor.nbblocdif = ds.difficultemin + Math.round((me.questionCourante - 1) * (ds.difficultemax - ds.difficultemin) / (ds.nbrepetitions - 1))
      if (ds.nbrepetitions === 1) { stor.nbblocdif = ds.difficultemin }
    }

    scratch.params.scratchProgDonnees.lesordres.push({ type: 'deposer_bras' })
    stor.nbblocattendu = 1

    if (stor.nbblocdif > 1) {
      stor.longueurbrasbase = 0
      scratch.params.scratchProgDonnees.lesordres.splice(0, 0, { type: 'agrandir_bras' })
      stor.nbblocattendu++
    }

    if (stor.nbblocdif > 2) {
      if (j3pGetRandomBool()) { var dep = ['T', 'M'] } else { dep = ['M', 'T'] }
      ;
      for (var i = 0; i < j3pGetRandomInt(1, 2); i++) {
        ajdep(dep[i])
      }
      var double = (stor.nbblocattendu === 4)
    }
    if (stor.nbblocdif > 3) {
      stor.longueurbrasbase = 1
      scratch.params.scratchProgDonnees.lesordres.splice(0, 0, { type: 'reduire_bras' })
      stor.nbblocattendu++
    }
    if (stor.nbblocdif > 4) {
      elem = []
      if (stor.hauteurbrasbase === 0) {
        if (stor.sensbrasbase === 0) {
          elem.push({ obj: j3pGetRandomInt(1, 2), ou: 'tapis1' })
        } else {
          elem.push({ obj: j3pGetRandomInt(1, 2), ou: 'tapis3' })
        }
      } else {
        if (stor.sensbrasbase === 0) {
          elem.push({ obj: j3pGetRandomInt(1, 2), ou: 'tapis2' })
        } else {
          elem.push({ obj: j3pGetRandomInt(1, 2), ou: 'tapis4' })
        }
      }
      scratch.params.scratchProgDonnees.lesordres.splice(0, 0, { type: 'prendre_bras' })
      stor.nbblocattendu++
    }
    if (stor.nbblocdif > 5) {
      stor.longueurbrasbase = 0
      scratch.params.scratchProgDonnees.lesordres.splice(0, 0, { type: 'agrandir_bras' })
      stor.nbblocattendu++
    }
    if (stor.nbblocdif > 6) {
      if (j3pGetRandomBool()) { dep = ['T', 'M'] } else { dep = ['M', 'T'] }
      ;
      let k = 2
      if (double) { k = 1 }
      ;
      for (i = 0; i < k; i++) {
        ajdep(dep[i])
      }
    }
    if (stor.nbblocdif > 7) {
      stor.longueurbrasbase = 1
      scratch.params.scratchProgDonnees.lesordres.splice(0, 0, { type: 'reduire_bras' })
      stor.nbblocattendu++
    }

    scratch.initBras(stor.hauteurbrasbase, stor.sensbrasbase, stor.longueurbrasbase, elem)
    scratch.enonce([], {
      pourInject: { StartScale: 0.75 },
      modeCorrection: {
        type: 'SansComparaison',
        aVerif: [{ quoi: 'NombreMaxBloc', nombre: scratch.params.scratchProgDonnees.lesordres.length + ds.tolerance + 1 },
          { quoi: 'QuandAvertir', quand: 'apres' }]
      },
      dirigeProg: { sequence: ds.Sequence, pasApas: ds.PasAPas, cacheTest: ds.CacheTest }
    })
    // Scratchbras();
    const yy = addDefaultTable(scratch.lesdivs.enonceG, 1, 1)[0][0]
    j3pAddContent(yy, '<b> Programme le bras </b> pour qu’il dépose l’objet sur le tapis n° ' + stor.letap + ', <BR> puis clique sur le drapeau.')

    me.finEnonce()
    me.cacheBoutonValider()
  }

  switch (this.etat) {
    case 'enonce':

      // code exécuté au lancement de la section
      if (this.debutDeLaSection) {
        initSection()
      } else {
        enonceMain()
      }
      break // case "enonce":

    case 'correction':
      try {
        const ret = scratch.prepCorrec()
        const xmlDoc2 = ret.xmlDoc2
        const xmlText = ret.xmlText

        if (scratch.params.scratchProgDonnees.repEleve) {
          // Bonne réponse
          this.score++
          scratch.corrigeGood(xmlDoc2, xmlText)
        } else {
          // Pas de bonne réponse
          scratch.corrige(xmlDoc2, xmlText)
        }
        // on veut être rappelé dans l’état navigation
        this.finCorrection('navigation', true)
      } catch (error) {
        console.error(error)
      }

      // Obligatoire
      this.finCorrection()
      break // case "correction":

    case 'navigation':

      if (this.sectionTerminee()) {
        scratch.lesdivs.BoutonsSuite.appendChild(this.buttonsElts.sectionSuivante)
        this.buttonsElts.sectionSuivante.addEventListener('click', () => {
          scratch.params.grosStop = true
        })
      } else {
        this.etat = 'enonce'
        scratch.lesdivs.BoutonsSuite.appendChild(this.buttonsElts.suite)
        this.buttonsElts.suite.addEventListener('click', () => {
          scratch.params.grosStop = true
        })
      }
      // Obligatoire
      this.finNavigation()

      break // case "navigation":
  }
}
