import { j3pGetRandomInt, j3pNotify } from 'src/legacy/core/functions'
import BulleAide from 'src/legacy/outils/bulleAide/BulleAide'
import { addDefaultTable } from 'src/legacy/themes/table'
import { getNewScratch } from 'src/legacy/outils/scratch/loaders'

export const params = {
  outils: ['mathquill'],
  /**
   * Ce qui est paramétrable dans l’éditeur de graphe
   * @type {LegacySectionParamsParametres}
   */
  parametres: [
    ['nbrepetitions', 4, 'entier', 'Nombre de répétitions de la section'],
    ['nbchances', 0, 'entier', 'Nombre d’essais possibles (illimité si 0)'],
    ['PasAPas', true, 'boolean', "<u>true</u>: L’élève peut lancer son programme en mode 'Pas à pas'."],
    ['Sequence', true, 'boolean', '<u>true</u>: Chaque bloc a une durée d’exécution d’environ 1 seconde.'],
    ['CacheTest', false, 'boolean', '<u>true</u>: L’élève ne voit pas l’action de son programme lors d la correction.']
  ]
}

/**
 * section blokbrasordre
 * @this {Parcours}
 */
export default function main () {
  const me = this
  const stor = me.storage
  const ds = me.donneesSection
  let scratch = stor.scratch

  function tradordre (el) {
    switch (el.type) {
      case 'monter_bras': return 'monter'
      case 'descendre_bras': return 'descendre'
      case 'tourner_bras': return 'tourner'
      case 'reduire_bras': return 'réduire'
      case 'agrandir_bras': return 'agrandir'
      case 'prendre_bras': return 'prendre'
      case 'deposer_bras': return 'déposer'
    }
  }
  async function initSection () {
    ds.theme = 'zonesAvecImageDeFond'
    scratch = await getNewScratch()
    stor.scratch = scratch
    stor.dejafait2 = []
    stor.dejafait = []
    stor.dejafait1 = []
    stor.dejafait3 = []
    me.validOnEnter = false // ex donneesSection.touche_entree
    scratch.initParams(me)
    scratch.testFlag = function () {
    }
    scratch.testFlagFin = function () {
      let ok = true
      let mess = ''
      const madein = []
      for (let i = 0; i < scratch.params.scratchProgDonnees.lesordres.length; i++) {
        madein.push(scratch.params.scratchProgDonnees.lesordres[i].type)
      }
      const tabout = scratch.params.scratchProgDonnees.tabcodeX[0]
      const made = []
      if (!tabout) {
        j3pNotify('pour tom tabout vide', { tabcodeX: scratch.params.scratchProgDonnees.tabcodeX, worspace: scratch.params.workspaceEl })
        return { ok: false, mess: 'Tu dois placer trois blocs <br> sous le bloc "<b>Quand Drapeau pressé</b>"!' }
      }
      for (let i = 1; i < tabout.length - 1; i++) {
        made.push(tabout[i].nom)
      }
      if (madein.length !== made.length) { ok = false; mess = 'Tu dois placer trois ordres <br> sous le bloc "<b>Quand Drapeau pressé</b>"!' } else {
        for (let i = 0; i < madein.length; i++) {
          if (madein[i] !== made[i]) { ok = false; mess = 'Tu dois donner les instructions dans l’ordre demandé !' }
        }
      }
      return { ok, mess }
    }
    me.construitStructurePage({ structure: 'presentation3', ratioGauche: 0.6 })
    me.stockage = [0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0]

    me.score = 0
    me.afficheTitre('Programmer par bloc - Bras mécanique')
    ds.nbitems = ds.nbetapes * ds.nbrepetitions

    // if (ds.indication !="") me.indication(me.zones.IG,ds.indication);
    scratch.params.scratchProgDonnees.menu = []
    scratch.params.scratchProgDonnees.progdeb = []
    scratch.params.scratchProgDonnees.progdeb.push({ type: 'Drapo', editable: 'false', deletable: 'false', movable: 'false', x: '20', y: '20' })

    scratch.params.scratchProgDonnees.menu.push({ type: 'catégorie', nom: 'Déplacements', coul1: '#4C97FF', coul2: '#3373CC', contenu: [] })
    scratch.params.scratchProgDonnees.menu[0].contenu.push({ type: 'monter_bras' })
    scratch.params.scratchProgDonnees.menu[0].contenu.push({ type: 'descendre_bras' })
    scratch.params.scratchProgDonnees.menu[0].contenu.push({ type: 'tourner_bras' })
    scratch.params.scratchProgDonnees.menu.push({ type: 'catégorie', nom: 'Bras', coul1: '#D65CD6', coul2: '#BD42BD', contenu: [] })
    scratch.params.scratchProgDonnees.menu[1].contenu.push({ type: 'reduire_bras' })
    scratch.params.scratchProgDonnees.menu[1].contenu.push({ type: 'agrandir_bras' })
    scratch.params.scratchProgDonnees.menu.push({ type: 'catégorie', nom: 'Pince', coul1: '#40BF4A', coul2: '#389438', contenu: [] })
    scratch.params.scratchProgDonnees.menu[2].contenu.push({ type: 'prendre_bras' })
    scratch.params.scratchProgDonnees.menu[2].contenu.push({ type: 'deposer_bras' })
    enonceMain()
  }

  function enonceMain () {
    me.videLesZones()
    scratch.params.scratchProgDonnees.lesordres = []
    const elem = []

    let petitcompte = 0
    let u
    do {
      petitcompte++
      u = j3pGetRandomInt(0, 2)
    } while ((petitcompte < 100) && (stor.dejafait.indexOf(u) !== -1))
    stor.dejafait.push(u)
    let u2
    if (u === 0) { // aller prendre
      petitcompte = 0
      do {
        petitcompte++
        u2 = j3pGetRandomInt(0, 1)
      } while ((petitcompte < 100) && (stor.dejafait1.indexOf(u2) !== -1))
      stor.dejafait1.push(u2)

      if (u2 === 0) {
        stor.sensbrasbase = j3pGetRandomInt(0, 1)
        stor.longueurbrasbase = 0
        stor.hauteurbrasbase = j3pGetRandomInt(0, 1)
        if (stor.sensbrasbase === 0) {
          if (stor.hauteurbrasbase === 0) {
            scratch.params.scratchProgDonnees.lesordres.push({ type: 'monter_bras' })
            elem.push({ obj: j3pGetRandomInt(1, 2), ou: 'tapis2' })
          } else {
            elem.push({ obj: j3pGetRandomInt(1, 2), ou: 'tapis1' })
            scratch.params.scratchProgDonnees.lesordres.push({ type: 'descendre_bras' })
          }
        } else {
          if (stor.hauteurbrasbase === 0) {
            scratch.params.scratchProgDonnees.lesordres.push({ type: 'monter_bras' })
            elem.push({ obj: j3pGetRandomInt(1, 2), ou: 'tapis4' })
          } else {
            elem.push({ obj: j3pGetRandomInt(1, 2), ou: 'tapis3' })
            scratch.params.scratchProgDonnees.lesordres.push({ type: 'descendre_bras' })
          }
        }
        scratch.params.scratchProgDonnees.lesordres.push({ type: 'agrandir_bras' })
        scratch.params.scratchProgDonnees.lesordres.push({ type: 'prendre_bras' })
      } else {
        stor.sensbrasbase = j3pGetRandomInt(0, 1)
        stor.longueurbrasbase = 0
        stor.hauteurbrasbase = j3pGetRandomInt(0, 1)
        if (stor.sensbrasbase === 0) {
          if (stor.hauteurbrasbase === 0) {
            elem.push({ obj: j3pGetRandomInt(1, 2), ou: 'tapis3' })
          } else {
            elem.push({ obj: j3pGetRandomInt(1, 2), ou: 'tapis4' })
          }
        } else {
          if (stor.hauteurbrasbase === 0) {
            elem.push({ obj: j3pGetRandomInt(1, 2), ou: 'tapis1' })
          } else {
            elem.push({ obj: j3pGetRandomInt(1, 2), ou: 'tapis2' })
          }
        }
        scratch.params.scratchProgDonnees.lesordres.push({ type: 'tourner_bras' })
        scratch.params.scratchProgDonnees.lesordres.push({ type: 'agrandir_bras' })
        scratch.params.scratchProgDonnees.lesordres.push({ type: 'prendre_bras' })
      }
    }
    if (u === 1) { // aller déposer
      petitcompte = 0
      do {
        petitcompte++
        u2 = j3pGetRandomInt(0, 1)
      } while ((petitcompte < 100) && (stor.dejafait2.indexOf(u2) !== -1))
      stor.dejafait2.push(u2)

      if (u2 === 0) {
        stor.sensbrasbase = j3pGetRandomInt(0, 1)
        stor.longueurbrasbase = 0
        stor.hauteurbrasbase = j3pGetRandomInt(0, 1)
        elem.push({ obj: j3pGetRandomInt(1, 2), ou: 'bras' })
        if (stor.hauteurbrasbase === 0) {
          scratch.params.scratchProgDonnees.lesordres.push({ type: 'monter_bras' })
        } else {
          scratch.params.scratchProgDonnees.lesordres.push({ type: 'descendre_bras' })
        }

        scratch.params.scratchProgDonnees.lesordres.push({ type: 'agrandir_bras' })
        scratch.params.scratchProgDonnees.lesordres.push({ type: 'deposer_bras' })
      } else {
        stor.sensbrasbase = j3pGetRandomInt(0, 1)
        stor.longueurbrasbase = 0
        stor.hauteurbrasbase = j3pGetRandomInt(0, 1)
        elem.push({ obj: j3pGetRandomInt(1, 2), ou: 'bras' })

        scratch.params.scratchProgDonnees.lesordres.push({ type: 'tourner_bras' })
        scratch.params.scratchProgDonnees.lesordres.push({ type: 'agrandir_bras' })
        scratch.params.scratchProgDonnees.lesordres.push({ type: 'deposer_bras' })
      }
    }
    if (u === 2) { // déplacer
      petitcompte = 0
      do {
        petitcompte++
        u2 = j3pGetRandomInt(0, 1)
      } while ((petitcompte < 100) && (stor.dejafait3.indexOf(u2) !== -1))
      stor.dejafait3.push(u2)

      if (u2 === 0) {
        stor.sensbrasbase = j3pGetRandomInt(0, 1)
        stor.longueurbrasbase = 1
        stor.hauteurbrasbase = j3pGetRandomInt(0, 1)
        elem.push({ obj: j3pGetRandomInt(1, 2), ou: 'bras' })
        scratch.params.scratchProgDonnees.lesordres.push({ type: 'reduire_bras' })
        if (stor.hauteurbrasbase === 0) {
          scratch.params.scratchProgDonnees.lesordres.push({ type: 'monter_bras' })
        } else {
          scratch.params.scratchProgDonnees.lesordres.push({ type: 'descendre_bras' })
        }
        scratch.params.scratchProgDonnees.lesordres.push({ type: 'tourner_bras' })
      } else {
        stor.sensbrasbase = j3pGetRandomInt(0, 1)
        stor.longueurbrasbase = 0
        stor.hauteurbrasbase = j3pGetRandomInt(0, 1)
        elem.push({ obj: j3pGetRandomInt(1, 2), ou: 'bras' })

        if (stor.hauteurbrasbase === 0) {
          scratch.params.scratchProgDonnees.lesordres.push({ type: 'monter_bras' })
        } else {
          scratch.params.scratchProgDonnees.lesordres.push({ type: 'descendre_bras' })
        }
        scratch.params.scratchProgDonnees.lesordres.push({ type: 'tourner_bras' })
        scratch.params.scratchProgDonnees.lesordres.push({ type: 'agrandir_bras' })
      }
    }

    scratch.initBras(stor.hauteurbrasbase, stor.sensbrasbase, stor.longueurbrasbase, elem)
    scratch.enonce([], {
      pourInject: { StartScale: 0.8 },
      modeCorrection: {
        type: 'SansComparaison',
        aVerif: [{ quoi: 'NombreDeBlocs', combien: 4 },
          { quoi: 'BesoinBloc', nom: scratch.params.scratchProgDonnees.lesordres[0].type },
          { quoi: 'BesoinBloc', nom: scratch.params.scratchProgDonnees.lesordres[1].type },
          { quoi: 'BesoinBloc', nom: scratch.params.scratchProgDonnees.lesordres[2].type },
          { quoi: 'BesoinniemeBloc', nom: scratch.params.scratchProgDonnees.lesordres[0].type, place: 1 },
          { quoi: 'QuandAvertir', quand: 'apres' }]
      },
      dirigeProg: { sequence: ds.Sequence, pasApas: ds.PasAPas, cacheTest: ds.CacheTest }
    })
    // Scratchbras();

    const tt = addDefaultTable(scratch.lesdivs.enonceG, 2, 1)
    tt[0][0].style.padding = 0
    tt[1][0].style.padding = 0
    tt[0][0].innerHTML = '<b>Attache les blocs <i><u>' + tradordre(scratch.params.scratchProgDonnees.lesordres[0]) + '</u></i> , <i><u>' + tradordre(scratch.params.scratchProgDonnees.lesordres[1]) + ' </u></i> puis  <i><u>' + tradordre(scratch.params.scratchProgDonnees.lesordres[2]) + ' </u></i> au programme</b>,'
    const tt2 = addDefaultTable(tt[1][0], 1, 2)
    tt2[0][0].style.padding = 0
    tt2[0][1].style.padding = 0
    tt2[0][0].innerHTML = 'puis clique sur le drapeau.'
    stor.aise = new BulleAide(tt2[0][1], 'Fais glisser les blocs utiles sous le bloc jaune.', { place: 5 })

    me.finEnonce()
    me.cacheBoutonValider()
  }

  switch (this.etat) {
    case 'enonce':

      // code exécuté au lancement de la section
      if (this.debutDeLaSection) {
        initSection()
      } else {
        enonceMain()
      }
      break // case "enonce":

    case 'correction':
      try {
        const ret = scratch.prepCorrec()
        const xmlDoc2 = ret.xmlDoc2
        const xmlText = ret.xmlText

        if (scratch.params.scratchProgDonnees.repEleve) {
          // Bonne réponse
          this.score++
          scratch.corrigeGood(xmlDoc2, xmlText)
        } else {
          // Pas de bonne réponse
          scratch.corrige(xmlDoc2, xmlText)
        }
        // on veut être rappelé dans l’état navigation
        this.finCorrection('navigation', true)
      } catch (error) {
        console.error(error)
      }

      // Obligatoire
      this.finCorrection()
      break // case "correction":

    case 'navigation':

      if (this.sectionTerminee()) {
        scratch.lesdivs.BoutonsSuite.appendChild(this.buttonsElts.sectionSuivante)
        this.buttonsElts.sectionSuivante.addEventListener('click', () => {
          scratch.params.grosStop = true
        })
      } else {
        this.etat = 'enonce'
        scratch.lesdivs.BoutonsSuite.appendChild(this.buttonsElts.suite)
        this.buttonsElts.suite.addEventListener('click', () => {
          scratch.params.grosStop = true
        })
      }
      // Obligatoire
      this.finNavigation()

      break // case "navigation":
  }
}
