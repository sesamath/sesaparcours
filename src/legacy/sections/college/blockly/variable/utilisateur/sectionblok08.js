import { j3pGetRandomInt } from 'src/legacy/core/functions'
import { j3pAffiche } from 'src/lib/mathquill/functions'
import { addDefaultTable } from 'src/legacy/themes/table'
import { getNewScratch } from 'src/legacy/outils/scratch/loaders'

export const params = {
  outils: ['mathquill'],
  /**
   * Ce qui est paramétrable dans l’éditeur de graphe
   * @type {LegacySectionParamsParametres}
   */
  parametres: [
    ['nbrepetitions', 4, 'entier', 'Nombre de répétitions de la section'],
    ['nbchances', 0, 'entier', 'Nombre d’essais possibles (illimité si 0)'],
    ['PasAPas', true, 'boolean', "<u>true</u>: L’élève peut lancer son programme en mode 'Pas à pas'."],
    ['Sequence', true, 'boolean', '<u>true</u>: Chaque bloc a une durée d’exécution d’environ 1 seconde.'],
    ['Completer', true, 'boolean', '<u>true</u>: Une partie du programme est déjà en place.'],
    ['CacheTest', false, 'boolean', '<u>true</u>: L’élève ne voit pas l’action de son programme lors d la correction.']
  ]
}

const consigne = '<b> Tu dois créer la variable </b> <i>£a</i> ; <br> puis <b> écrire un programme </b> qui injecte un nombre donné par un utilisateur dans la variable <i>£a</i> .'

/**
 * section blok08
 * @this {Parcours}
 */
export default function main () {
  const me = this
  const stor = me.storage
  const ds = me.donneesSection
  let scratch = stor.scratch

  async function initSection () {
    scratch = await getNewScratch()
    stor.scratch = scratch
    stor.tabcodeX = []
    stor.Averif = []
    stor.afficheEssai = true
    me.validOnEnter = false // ex donneesSection.touche_entree
    scratch.initParams(me)
    /// plein de fonction prog
    scratch.testFlag = function () {

    }
    scratch.testFlagFin = function () {
      let ok = true
      let mess = ''
      if (this.params.scratchProgDonnees.reponseDonnesAGetUS.length === 0) return { ok: false, mess: 'Il faut demander une valeur à l\'utilisateur !' }
      if (scratch.params.scratchProgDonnees.variables.length === 0) return { ok: false, mess: 'Il faut créer une variable <i>' + stor.nomVar + '</i> !' }
      if (scratch.params.scratchProgDonnees.variables.length > 1) return { ok: false, mess: 'Tu as créé des variables inutiles !' }
      if (scratch.params.scratchProgDonnees.variables[0].name !== stor.nomVar) return { ok: false, mess: 'Le nom de la variable ne correspond pas !' }
      if (scratch.params.scratchProgDonnees.variables.length > 0) {
        if (scratch.params.scratchProgDonnees.variables[0].val !== scratch.vireGuillemets(scratch.params.scratchProgDonnees.reponse)) { ok = false; mess = 'La valeur de la variable <i>' + scratch.params.scratchProgDonnees.variables[0].name + '</i> n’est pas correcte !' }
      } else {
        ok = false
      }
      return { ok, mess }
    }
    scratch.params.scratchProgDonnees.squizzVar = true

    me.construitStructurePage('presentation3')
    me.stockage = [0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0]

    stor.dejafait = []
    me.afficheTitre('Injecter une réponse dans une variable')
    scratch.params.verifDeb = true

    scratch.params.scratchProgDonnees.progdeb = []
    scratch.params.scratchProgDonnees.progdeb.push({ type: 'Drapo', editable: 'false', deletable: 'false', movable: 'false', x: '20', y: '20' })

    scratch.params.scratchProgDonnees.menu = []
    scratch.params.scratchProgDonnees.menu.push({ type: 'catégorie', nom: 'VARIABLES', coul1: '#FF8C1A', coul2: '#DB6E00', custom: 'VARIABLE', contenu: [] })
    scratch.params.scratchProgDonnees.menu.push({ type: 'catégorie', nom: 'UTILISATEUR', coul1: '#4CBFE6', coul2: '#2E8EB8', contenu: [] })
    scratch.params.scratchProgDonnees.menu[1].contenu.push({ type: 'reponse' })

    if (ds.Completer) {
      scratch.params.scratchProgDonnees.progdeb.push({ type: 'Get_us', value: [{ nom: 'STRING', contenu: { valeur: 'Ecris un nombre.', editable: 'false', deletable: 'false', movable: 'false' } }], editable: 'false', deletable: 'false', movable: 'false' })
    } else {
      scratch.params.scratchProgDonnees.menu[1].contenu.push({ type: 'Get_us', value: [{ nom: 'STRING', contenu: { valeur: 'Ecris un nombre.', editable: 'false', deletable: 'false', movable: 'false' } }] })
    }
    scratch.params.blokmenulimite.push({ type: 'Get_us', nb: 1 })
    enonceMain()
  }

  function enonceMain () {
    me.videLesZones()

    scratch.params.NomsVariableAutorise = ['a', 'b', 'c', 'd', 'e', 'f', 'g', 'h', 'i', 'j', 'k', 'l', 'm', 'n', 'o', 'p', 'q', 'r', 's', 't', 'u', 'v', 'w', 'x', 'y', 'z']
    scratch.params.scratchProgDonnees.Lutins[0].cox = 10
    scratch.params.scratchProgDonnees.Lutins[0].coxbase = 10
    scratch.params.scratchProgDonnees.lesvarbonnes = []
    scratch.params.scratchProgDonnees.lesordres = []

    stor.nomVar = scratch.params.NomsVariableAutorise[j3pGetRandomInt(0, (scratch.params.NomsVariableAutorise.length - 1))]

    scratch.params.scratchProgDonnees.lesordres.push({ type: 'Get_us', value: [{ nom: 'STRING', contenu: { valeur: 'Ecris un nombre.', editable: 'false', deletable: 'false', movable: 'false' } }], editable: 'false', deletable: 'false', movable: 'false' })
    // scratch.params.scratchProgDonnees.lesordres.push({type:'ordre',quoi:'demande',message:{variable:'x'}})
    scratch.params.scratchProgDonnees.lesordres.push({ type: 'data_setvariableto', editable: 'false', deletable: 'false', movable: 'false', variable: [{ nom: 'VARIABLE', nomvar: stor.nomVar, id: 'aaaaaa' }], value: [{ nom: 'VALUE', editable: 'false', deletable: 'false', movable: 'false', contenu: { reponse: '' } }] })

    scratch.params.scratchProgDonnees.lesflag = []
    scratch.params.scratchProgDonnees.lesflag.push({ ok: false })
    const nomdesvar = []
    for (let i = 0; i < scratch.params.scratchProgDonnees.lesvarbonnes.length; i++) {
      nomdesvar.push(stor.nomVar)
    }
    scratch.enonce([], {
      pourInject: { },
      modeCorrection: {
        type: 'SansComparaison',
        aVerif: [{ quoi: 'Flag', nombre: 0 },
          { quoi: 'NombreMaxBloc', nombre: 1 + 3 },
          { quoi: 'ExisteVar', nom: nomdesvar },
          { quoi: 'QuandAvertir', quand: 'apres' },
          { quoi: 'AnnuleSiAvert', quand: false }]
      },
      dirigeProg: { sequence: ds.Sequence, pasApas: ds.PasAPas, cacheTest: ds.CacheTest }
    })
    const yy = addDefaultTable(scratch.lesdivs.enonceG, 1, 1)[0][0]
    j3pAffiche(yy, null, consigne, { a: stor.nomVar })
    scratch.params.scratchProgDonnees.listValPos = [['1'], ['2'], ['3'], ['4'], ['5'], ['6'], ['7'], ['8'], ['9'], ['10']]

    me.finEnonce()
    // on veut pas du bouton, c'est le drapeau vert qui valide
    me.cacheBoutonValider()
  }

  switch (this.etat) {
    case 'enonce':
      if (this.debutDeLaSection) {
        initSection()
      } else {
        enonceMain()
      }
      break // case "enonce":

    case 'correction':
      try {
        const ret = scratch.prepCorrec()
        const xmlDoc2 = ret.xmlDoc2
        const xmlText = ret.xmlText

        if (scratch.params.scratchProgDonnees.repEleve) {
          // Bonne réponse
          this.score++
          scratch.corrigeGood(xmlDoc2, xmlText)
        } else {
          // Pas de bonne réponse
          scratch.corrige(xmlDoc2, xmlText)
        }
        // on veut être rappelé dans l’état navigation
        this.finCorrection('navigation', true)
      } catch (error) {
        console.error(error)
      }
      break // case "correction":

    case 'navigation':

      if (this.sectionTerminee()) {
        scratch.lesdivs.BoutonsSuite.appendChild(this.buttonsElts.sectionSuivante)
        this.buttonsElts.sectionSuivante.addEventListener('click', () => {
          scratch.params.grosStop = true
        })
      } else {
        this.etat = 'enonce'
        scratch.lesdivs.BoutonsSuite.appendChild(this.buttonsElts.suite)
        this.buttonsElts.suite.addEventListener('click', () => {
          scratch.params.grosStop = true
        })
      }
      // Obligatoire
      this.finNavigation()

      break // case "navigation":
  }
}
