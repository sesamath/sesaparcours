import { j3pGetRandomInt, j3pShuffle } from 'src/legacy/core/functions'
import { j3pAffiche } from 'src/lib/mathquill/functions'
import { getNewScratch } from 'src/legacy/outils/scratch/loaders'
import { addDefaultTable } from 'src/legacy/themes/table'

import fondvarImg from 'src/legacy/outils/scratch/images/fondvar.jpg'

export const params = {
  outils: ['mathquill'],
  /**
   * Ce qui est paramétrable dans l’éditeur de graphe
   * @type {LegacySectionParamsParametres}
   */
  parametres: [
    ['nbrepetitions', 4, 'entier', 'Nombre de répétitions de la section'],
    ['nbchances', 0, 'entier', 'Nombre d’essais possibles (illimité si 0)'],
    ['PasAPas', true, 'boolean', "<u>true</u>: L’élève peut lancer son programme en mode 'Pas à pas'."],
    ['Sequence', true, 'boolean', '<u>true</u>: Chaque bloc a une durée d’exécution d’environ 1 seconde.'],
    ['operation', true, 'boolean', '<u>true</u>: Le lutin doit transmettre le résultat d’une opération.'],
    ['CacheTest', false, 'boolean', '<u>true</u>: L’élève ne voit pas l’action de son programme lors de la correction.']
  ]
}

/**
 * section blokvar
 * @this {Parcours}
 */
export default function main () {
  const me = this
  const stor = me.storage
  const ds = me.donneesSection
  let scratch = stor.scratch

  async function initSection () {
    scratch = await getNewScratch()
    stor.scratch = scratch
    stor.tabcodeX = []
    stor.Averif = []
    me.validOnEnter = false // ex donneesSection.touche_entree
    scratch.initParams(me)
    scratch.testFlag = function () {

    }
    scratch.testFlagFin = function () {
      let reg, verif
      switch (stor.toki) {
        case 'Charlie':
          reg = scratch.params.scratchProgDonnees.mem[2]
          break
        case 'Félix':
          reg = scratch.params.scratchProgDonnees.mem[1]
          break
        default:
          reg = scratch.params.scratchProgDonnees.mem[0]
          break
      }
      switch (stor.deki) {
        case 'Charlie':
          verif = scratch.params.scratchProgDonnees.regMem[2]
          break
        case 'Félix':
          verif = scratch.params.scratchProgDonnees.regMem[1]
          break
        default:
          verif = scratch.params.scratchProgDonnees.regMem[0]
          break
      }
      if (!ds.operation) {
        if (String(verif) !== reg) {
          return { ok: false, mess: 'Dans cette situation, <br>' + stor.toki + ' aurait du avoir le nombre ' + verif + '.' }
        } else {
          return { ok: true, mess: '' }
        }
      } else {
        let verif2
        let ar
        switch (stor.deki2) {
          case 'Charlie':
            verif2 = scratch.params.scratchProgDonnees.regMem[2]
            break
          case 'Félix':
            verif2 = scratch.params.scratchProgDonnees.regMem[1]
            break
          default:
            verif2 = scratch.params.scratchProgDonnees.regMem[0]
            break
        }
        switch (stor.op) {
          case '+':
            ar = verif + verif2
            break
          case '-':
            ar = verif - verif2
            break
          case '*':
            ar = verif * verif2
            break
          case ':':
            ar = verif / verif2
            break
        }
        if (Math.abs(ar - reg) > 0.00000001) {
          return { ok: false, mess: 'Dans cette situation,<br> ' + stor.toki + ' aurait du avoir le nombre ' + ar + '.' }
        } else {
          return { ok: true, mess: '' }
        }
      }
    }
    scratch.params.scratchProgDonnees.squizzVar = true
    ds.nbitems = ds.nbetapes * ds.nbrepetitions
    scratch.params.yainit = true
    scratch.params.AutoriseSupprimeVariable = false
    scratch.params.AutoriseRenomeVariable = false

    me.construitStructurePage('presentation3')
    me.stockage = [0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0]

    stor.dejafait = []
    me.afficheTitre('Utiliser une variable')

    scratch.params.scratchProgDonnees.lesvarbonnes = [{ nom: 'variable', val: 'vide', id: 'bbbbbb' }]
    if (ds.operation) {
      scratch.params.scratchProgDonnees.lesvarbonnes[0].nom = 'variable1'
      scratch.params.scratchProgDonnees.lesvarbonnes.push({ nom: 'variable2', val: 'vide', id: 'aaaaaa' })
    }

    scratch.params.figureAfaire = [
      {
        type: 'image',
        x: 0,
        y: -150,
        width: 450,
        height: 600,
        url: fondvarImg
      }
    ]
    scratch.params.scratchProgDonnees.Lutins[0].larg = 40
    scratch.params.scratchProgDonnees.Lutins[0].hauteurbase = 100
    scratch.params.scratchProgDonnees.Lutins[0].haut = 50
    scratch.params.scratchProgDonnees.Lutins[0].coxbase = 70
    scratch.params.scratchProgDonnees.Lutins[0].coybase = 135

    scratch.params.scratchProgDonnees.menu.push({ type: 'catégorie', nom: 'Déplacement', coul1: '#4C97FF', coul2: '#3373CC', contenu: [] })
    scratch.params.scratchProgDonnees.menu.push({ type: 'catégorie', nom: 'Variables', coul1: '#FFA500', coul2: '#3373CC', contenu: [] })
    scratch.params.scratchProgDonnees.menu[1].contenu.push({ type: 'Giv_pomme', variable: [] })
    scratch.params.scratchProgDonnees.menu[0].contenu.push({ type: 'vaCharlie' })
    scratch.params.scratchProgDonnees.menu[0].contenu.push({ type: 'vaLlyn' })
    scratch.params.scratchProgDonnees.menu[0].contenu.push({ type: 'vaFelix' })
    if (ds.operation) {
      scratch.params.scratchProgDonnees.menu[1].contenu.push({ type: 'Get_pomme', variable: [{ nom: 'VARIABLE', nomvar: 'variable1', id: 'bbbbbb' }] })
      scratch.params.scratchProgDonnees.menu.push({ type: 'catégorie', nom: 'Opérateurs', coul1: '#22dd22', coul2: '#22dd22', contenu: [] })
      scratch.params.scratchProgDonnees.menu[2].contenu.push({ type: 'operator_add', value: [{ nom: 'NUM1', contenu: { valeur: ' ' } }, { nom: 'NUM2', contenu: { valeur: ' ' } }] })
      scratch.params.scratchProgDonnees.menu[2].contenu.push({ type: 'operator_subtract', value: [{ nom: 'NUM1', contenu: { valeur: ' ' } }, { nom: 'NUM2', contenu: { valeur: ' ' } }] })
      scratch.params.scratchProgDonnees.menu[2].contenu.push({ type: 'operator_multiply', value: [{ nom: 'NUM1', contenu: { valeur: ' ' } }, { nom: 'NUM2', contenu: { valeur: ' ' } }] })
      scratch.params.scratchProgDonnees.menu[2].contenu.push({ type: 'operator_divide', value: [{ nom: 'NUM1', contenu: { valeur: ' ' } }, { nom: 'NUM2', contenu: { valeur: ' ' } }] })
      scratch.params.scratchProgDonnees.menu[1].contenu.push({ type: 'data_variable', variable: [{ nom: 'VARIABLE', nomvar: 'variable1', id: 'bbbbbb' }] })
      scratch.params.scratchProgDonnees.menu[1].contenu.push({ type: 'data_variable', variable: [{ nom: 'VARIABLE', nomvar: 'variable2', id: 'aaaaaa' }] })
      scratch.params.scratchProgDonnees.menu[1].contenu[0].variable.nomvar = 'variable1'
    } else {
      scratch.params.scratchProgDonnees.menu[1].contenu.push({ type: 'Get_pomme', variable: [{ nom: 'VARIABLE', nomvar: 'variable', id: 'bbbbbb' }] })
      scratch.params.scratchProgDonnees.menu[1].contenu.push({ type: 'data_variable', variable: [{ nom: 'VARIABLE', nomvar: 'variable', id: 'bbbbbb' }] })
    }
    enonceMain()
  }
  function enonceMain () {
    me.videLesZones()

    scratch.params.scratchProgDonnees.mem = [j3pGetRandomInt(2, 12)]
    scratch.params.scratchProgDonnees.mem.push(scratch.params.scratchProgDonnees.mem[0] + j3pGetRandomInt(2, 12))
    scratch.params.scratchProgDonnees.mem.push(scratch.params.scratchProgDonnees.mem[1] + j3pGetRandomInt(2, 12))
    scratch.params.NomsVariableAutorise = []
    scratch.params.scratchProgDonnees.lesordres = []
    scratch.params.scratchProgDonnees.progdeb = []
    scratch.params.scratchProgDonnees.progdeb.push({ type: 'Drapo', editable: 'false', deletable: 'false', movable: 'false', x: '20', y: '20' })
    scratch.params.scratchProgDonnees.Lutins[0].cox = scratch.params.scratchProgDonnees.Lutins[0].coxbase
    scratch.params.scratchProgDonnees.Lutins[0].coy = scratch.params.scratchProgDonnees.Lutins[0].coybase

    // [0] c’est y’a toutes les variables qu’il faut
    // [1] les variables sont bien à la bonne valeur
    scratch.params.scratchProgDonnees.lesflag = []

    const listto = j3pShuffle(['Charlie', 'Félix', 'Llyn'])
    stor.deki = listto.pop()
    stor.toki = listto.pop()
    let cons
    let lim = 7
    if (!ds.operation) {
      switch (stor.deki) {
        case 'Charlie':
          scratch.params.scratchProgDonnees.lesordres.push({ type: 'vaCharlie' })
          break
        case 'Félix':
          scratch.params.scratchProgDonnees.lesordres.push({ type: 'vaFelix' })
          break
        default:
          scratch.params.scratchProgDonnees.lesordres.push({ type: 'vaLlyn' })
      }
      scratch.params.scratchProgDonnees.lesordres.push({ type: 'Get_pomme', variable: [{ nom: 'VARIABLE', nomvar: 'variable', id: 'bbbbbb' }] })
      switch (stor.toki) {
        case 'Charlie':
          scratch.params.scratchProgDonnees.lesordres.push({ type: 'vaCharlie' })
          break
        case 'Félix':
          scratch.params.scratchProgDonnees.lesordres.push({ type: 'vaFelix' })
          break
        default:
          scratch.params.scratchProgDonnees.lesordres.push({ type: 'vaLlyn' })
      }
      scratch.params.scratchProgDonnees.lesordres.push({ type: 'Giv_pomme', value: [{ nom: 'VARIABLE', contenu: { type: 'data_variable', variable: [{ nom: 'VARIABLE', nomvar: 'variable', id: 'bbbbbb' }] } }] })
      cons = '<b>Programme Scratch</b> pour qu’il transmette le nombre de ' + stor.deki + ' à ' + stor.toki + '.'
    } else {
      stor.deki2 = listto.pop()
      stor.op = ['+', '-', '*', ':'][j3pGetRandomInt(0, 3)]
      lim += 3
      switch (stor.deki) {
        case 'Charlie':
          scratch.params.scratchProgDonnees.lesordres.push({ type: 'vaCharlie' })
          break
        case 'Félix':
          scratch.params.scratchProgDonnees.lesordres.push({ type: 'vaFelix' })
          break
        default:
          scratch.params.scratchProgDonnees.lesordres.push({ type: 'vaLlyn' })
      }
      scratch.params.scratchProgDonnees.lesordres.push({ type: 'Get_pomme', variable: [{ nom: 'VARIABLE', nomvar: 'variable1', id: 'bbbbbb' }] })
      switch (stor.deki2) {
        case 'Charlie':
          scratch.params.scratchProgDonnees.lesordres.push({ type: 'vaCharlie' })
          break
        case 'Félix':
          scratch.params.scratchProgDonnees.lesordres.push({ type: 'vaFelix' })
          break
        default:
          scratch.params.scratchProgDonnees.lesordres.push({ type: 'vaLlyn' })
      }
      scratch.params.scratchProgDonnees.lesordres.push({ type: 'Get_pomme', variable: [{ nom: 'VARIABLE', nomvar: 'variable2', id: 'aaaaaa' }] })
      let dedans, direop, direop2
      switch (stor.op) {
        case '+':
          dedans = 'plus'
          direop = ' la somme du nombre de '
          direop2 = ' et du nombre de '
          break
        case '-':
          dedans = 'moins'
          direop = ' la différence du nombre de '
          direop2 = ' moins le nombre de '
          break
        case '*':
          dedans = 'fois'
          direop = ' le produit du nombre de '
          direop2 = ' par le nombre de '
          break
        case ':':
          dedans = 'div'
          direop = ' le quotient du nombre de '
          direop2 = ' par le nombre de '
          break
      }
      switch (stor.toki) {
        case 'Charlie':
          scratch.params.scratchProgDonnees.lesordres.push({ type: 'vaCharlie' })
          break
        case 'Félix':
          scratch.params.scratchProgDonnees.lesordres.push({ type: 'vaFelix' })
          break
        default:
          scratch.params.scratchProgDonnees.lesordres.push({ type: 'vaLlyn' })
      }
      scratch.params.scratchProgDonnees.lesordres.push({ type: 'Giv_pomme', value: [{ nom: 'VARIABLE', contenu: { operateur: dedans, value: [{ nom: 'NUM1', contenu: { type: 'data_variable', variable: [{ nom: 'VARIABLE', nomvar: 'variable1', id: 'bbbbbb' }] } }, { nom: 'NUM2', contenu: { type: 'data_variable', variable: [{ nom: 'VARIABLE', nomvar: 'variable2', id: 'aaaaaa' }] } }] } }] })
      cons = '<b>Programme Scratch</b> pour qu’il transmette à ' + stor.toki + '<br>' + direop + stor.deki + direop2 + stor.deki2 + '.'
    }

    scratch.enonce([], {
      pourInject: { },
      modeCorrection: {
        type: 'SansComparaison',
        aVerif: [
          { quoi: 'NombreMaxBloc', nombre: lim },
          { quoi: 'QuandAvertir', quand: 'apres' },
          { quoi: 'AnnuleSiAvert', quand: false }]
      },
      dirigeProg: { sequence: ds.Sequence, pasApas: ds.PasAPas, cacheTest: ds.CacheTest }
    })
    const yy = addDefaultTable(scratch.lesdivs.enonceG, 1, 1)[0][0]
    j3pAffiche(yy, null, cons)

    me.finEnonce()
    me.cacheBoutonValider()
  }

  switch (this.etat) {
    case 'enonce':
      if (this.debutDeLaSection) {
        initSection()
      } else {
        enonceMain()
      }
      break // case "enonce":

    case 'correction':
      try {
        const ret = scratch.prepCorrec()
        const xmlDoc2 = ret.xmlDoc2
        const xmlText = ret.xmlText

        if (scratch.params.scratchProgDonnees.repEleve) {
          // Bonne réponse
          this.score++
          scratch.corrigeGood(xmlDoc2, xmlText)
        } else {
          // Pas de bonne réponse
          scratch.corrige(xmlDoc2, xmlText)
        }
        // on veut être rappelé dans l’état navigation
        this.finCorrection('navigation', true)
      } catch (error) {
        console.error(error)
      }
      break // case "correction":

    case 'navigation':

      if (this.sectionTerminee()) {
        scratch.lesdivs.BoutonsSuite.appendChild(this.buttonsElts.sectionSuivante)
        this.buttonsElts.sectionSuivante.addEventListener('click', () => {
          scratch.params.grosStop = true
        })
      } else {
        this.etat = 'enonce'
        scratch.lesdivs.BoutonsSuite.appendChild(this.buttonsElts.suite)
        this.buttonsElts.suite.addEventListener('click', () => {
          scratch.params.grosStop = true
        })
      }
      // Obligatoire
      this.finNavigation()

      break // case "navigation":
  }
}
