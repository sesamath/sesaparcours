import { j3pGetRandomInt } from 'src/legacy/core/functions'
import { j3pAffiche } from 'src/lib/mathquill/functions'
import { getNewScratch } from 'src/legacy/outils/scratch/loaders'
import { addDefaultTable } from 'src/legacy/themes/table'

export const params = {
  outils: ['mathquill'],
  /**
   * Ce qui est paramétrable dans l’éditeur de graphe
   * @type {LegacySectionParamsParametres}
   */
  parametres: [
    ['nbrepetitions', 4, 'entier', 'Nombre de répétitions de la section'],
    ['nbchances', 0, 'entier', 'Nombre d’essais possibles (illimité si 0)'],
    ['PasAPas', true, 'boolean', "<u>true</u>: L’élève peut lancer son programme en mode 'Pas à pas'."],
    ['Sequence', true, 'boolean', '<u>true</u>: Chaque bloc a une durée d’exécution d’environ 1 seconde.'],
    ['nombre_de_variables', 1, 'entier', 'Nombre de variables (maximum 3)'],
    ['CacheTest', false, 'boolean', '<u>true</u>: L’élève ne voit pas l’action de son programme lors de la correction.']
  ]
}

const textes = {
  phrase1: 'Ecris ',
  phrase2: 'La solution était ',
  phrase3: 'On te demande d’écrire ',
  resat: ['le nombre suivant', 'le nombre précédent', 'le double de', 'le triple de', 'la moitié de', 'le tiers de'],
  consigne: [
    '<b> Tu dois créer la variable </b> <i>£a</i> ;<br>',
    '<b> Tu dois créer les variables </b> <i>£a</i> <b> et </b> <i>£b</i> ;<br>',
    '<b> Tu dois créer les variables </b> <i>£a</i> <b> , </b> <i>£b</i> <b> et </b> <i>£c</i> ;<br>'
  ],
  consigne2: [
    '<b> écrire un programme </b> qui injecte le nombre $£d$ dans la variable <i>£a</i> .',
    '<b> écrire un programme </b> qui injecte le nombre $£d$ dans la variable <i>£a</i> <br>et le nombre $£e$ dans la variable <i> £b</i> .',
    '<b> écrire un programme </b> qui injecte les nombres $£d$  , $£e$ et $£f$ <br> dans les variables respectives <i>£a</i> , <i>£b</i> et <i>£c</i> .'
  ],
  puis: ' puis '
}

/**
 * section blok07
 * @this {Parcours}
 */
export default function main () {
  const me = this
  const stor = me.storage
  const ds = me.donneesSection
  let scratch = stor.scratch

  async function initSection () {
    scratch = await getNewScratch()
    stor.scratch = scratch
    me.donneesSection = ds
    stor.tabcodeX = []
    stor.Averif = []
    scratch.initParams(me)
    scratch.testFlag = function () {
    }
    scratch.testFlagFin = function () {
      for (let i = 0; i < stor.agarde.length; i++) {
        let j
        for (j = 0; j < scratch.params.scratchProgDonnees.variables.length; j++) {
          if (scratch.params.scratchProgDonnees.variables[j].name === stor.agarde[i].nom) break
        }
        if (j === scratch.params.scratchProgDonnees.variables.length) {
          return { ok: false, mess: 'Je ne trouve pas de variable nommée <i>' + stor.agarde[i].nom + '</i> !' }
        } else {
          if (scratch.params.scratchProgDonnees.variables[j].val !== (stor.agarde[i].val + '')) {
            return { ok: false, mess: 'La valeur de la variable <i>' + scratch.params.scratchProgDonnees.variables[j].name + '</i> n’est pas correcte !' }
          }
        }
      }
      return { ok: true, mess: '' }
    }
    scratch.params.scratchProgDonnees.squizzVar = true
    ds.nbitems = ds.nbetapes * ds.nbrepetitions

    if (!Number.isInteger(ds.nombre_de_variables)) {
      console.error(Error(`nombre_de_variables invalide (${ds.nombre_de_variables}) => 1`))
      ds.nombre_de_variables = 1
    } else if (ds.nombre_de_variables > 3) {
      ds.nombre_de_variables = 3
    } else if (ds.nombre_de_variables < 1) {
      ds.nombre_de_variables = 1
    }

    me.construitStructurePage({ structure: 'presentation3', theme: 'zonesAvecImageDeFond' })
    stor.dejafait = []
    me.afficheTitre('Injecter une valeur dans une variable')

    scratch.params.scratchProgDonnees.menu = []
    scratch.params.scratchProgDonnees.progdeb = []
    scratch.params.scratchProgDonnees.menu.push({ type: 'catégorie', nom: 'VARIABLES', coul1: '#FF8C1A', coul2: '#DB6E00', custom: 'VARIABLE', contenu: [] })

    enonceMain()
  }

  function enonceMain () {
    me.videLesZones()
    scratch.params.NomsVariableAutorise = ['a', 'b', 'c', 'd', 'e', 'f', 'g', 'h', 'i', 'j', 'k', 'l', 'm', 'n', 'o', 'p', 'q', 'r', 's', 't', 'u', 'v', 'w', 'x', 'y', 'z']
    scratch.params.scratchProgDonnees.lesordres = []
    scratch.params.scratchProgDonnees.lesvarbonnes = []
    const varuse = []
    const varval = []
    scratch.params.scratchProgDonnees.progdeb = []
    scratch.params.scratchProgDonnees.progdeb.push({ type: 'Drapo', editable: 'false', deletable: 'false', movable: 'false', x: '20', y: '20' })

    for (let i = 0; i < ds.nombre_de_variables; i++) {
      let ok, lettre
      do {
        lettre = scratch.params.NomsVariableAutorise[j3pGetRandomInt(0, (scratch.params.NomsVariableAutorise.length - 1))]
        ok = (varuse.indexOf(lettre) === -1)
      } while (!ok)
      varuse.push(lettre)
      const val = j3pGetRandomInt(7, 1000)
      varval.push(val)
      // scratch.params.scratchProgDonnees.lesvarbonnes.push({ nom: lettre, val, id: 'aaa' + varuse.length })
      scratch.params.scratchProgDonnees.lesordres.push({ type: 'data_setvariableto', variable: [{ nom: 'VARIABLE', nomvar: lettre, id: 'aaa' + varuse.length }], value: [{ nom: 'VALUE', contenu: { valeur: val } }] })
    }

    scratch.params.scratchProgDonnees.lesflag = []
    const cons = textes.consigne[ds.nombre_de_variables - 1] + textes.puis + textes.consigne2[ds.nombre_de_variables - 1]
    const mesv = ['', '', '']
    const mesvr = [0, 0, 0]
    stor.agarde = []
    for (let i = 0; i < ds.nombre_de_variables; i++) {
      mesv[i] = varuse[i]
      mesvr[i] = varval[i]
      stor.agarde.push({ nom: varuse[i], val: varval[i] })
    }

    const nomdesvar = []

    for (let i = 0; i < varuse.length; i++) {
      nomdesvar.push(varuse[i])
    }

    scratch.enonce([], {
      pourInject: { },
      modeCorrection: {
        type: 'SansComparaison',
        aVerif: [
          { quoi: 'NombreMaxBloc', nombre: 1 + ds.nombre_de_variables },
          { quoi: 'ExisteVar', nom: nomdesvar },
          { quoi: 'QuandAvertir', quand: 'apres' },
          { quoi: 'AnnuleSiAvert', quand: false }]
      },
      dirigeProg: { sequence: ds.Sequence, pasApas: ds.PasAPas, cacheTest: ds.CacheTest }
    })
    const yy = addDefaultTable(scratch.lesdivs.enonceG, 1, 1)[0][0]
    j3pAffiche(yy, null, cons, {
      a: mesv[0],
      b: mesv[1],
      c: mesv[2],
      d: mesvr[0],
      e: mesvr[1],
      f: mesvr[2]
    })

    me.finEnonce()
    // on ne veut pas du bouton valider, remplacé par notre bouton run (le drapeau vert)
    me.cacheBoutonValider()
  }

  switch (this.etat) {
    case 'enonce':
      if (this.debutDeLaSection) {
        initSection()
      } else {
        enonceMain()
      }
      break // case "enonce":

    case 'correction':
      try {
        const ret = scratch.prepCorrec()
        const xmlDoc2 = ret.xmlDoc2
        const xmlText = ret.xmlText

        if (scratch.params.scratchProgDonnees.repEleve) {
          // Bonne réponse
          this.score++
          scratch.corrigeGood(xmlDoc2, xmlText)
        } else {
          // Pas de bonne réponse
          scratch.corrige(xmlDoc2, xmlText)
        }
        // on veut être rappelé dans l’état navigation
        this.finCorrection('navigation', true)
      } catch (error) {
        console.error(error)
      }
      break // case "correction":

    case 'navigation':
      if (this.sectionTerminee()) {
        scratch.lesdivs.BoutonsSuite.appendChild(this.buttonsElts.sectionSuivante)
        this.buttonsElts.sectionSuivante.addEventListener('click', () => {
          scratch.params.grosStop = true
        })
      } else {
        this.etat = 'enonce'
        scratch.lesdivs.BoutonsSuite.appendChild(this.buttonsElts.suite)
        this.buttonsElts.suite.addEventListener('click', () => {
          scratch.params.grosStop = true
        })
      }
      // Obligatoire
      this.finNavigation()

      break // case "navigation":
  }
}
