import { j3pAddContent, j3pGetRandomInt } from 'src/legacy/core/functions'
import { getNewScratch } from 'src/legacy/outils/scratch/loaders'
import { addDefaultTable } from 'src/legacy/themes/table'

export const params = {
  outils: ['mathquill'],
  /**
   * Ce qui est paramétrable dans l’éditeur de graphe
   * @type {LegacySectionParamsParametres}
   */
  parametres: [
    ['nbrepetitions', 4, 'entier', 'Nombre de répétitions de la section'],
    ['nbchances', 3, 'entier', 'Nombre d’essais possibles (illimité si 0)'],
    ['PasAPas', true, 'boolean', "<u>true</u>: L’élève peut lancer son programme en mode 'Pas à pas'."],
    ['Sequence', true, 'boolean', '<u>true</u>: Chaque bloc a une durée d’exécution d’environ 1 seconde.'],
    ['difficultemin', 1, 'entier', 'Difficulté du programme (minimum 1)'],
    ['difficultemax', 9, 'entier', 'Difficulté du programme (maximum 9)'],
    ['difficulte', 'Progressive', 'liste', 'Gestion de la progression (entre min et max)', ['Aléatoire', 'Progressive']],
    ['tolerance', 0, 'entier', 'Nombre de blocs supplementaires acceptés'],
    ['CacheTest', false, 'boolean', '<u>true</u>: L’élève ne voit pas l’action de son programme lors d la correction.']
  ]

}

/**
 * section blokfuseeboucle
 * @this {Parcours}
 */
export default function main () {
  const me = this
  const stor = me.storage
  const ds = me.donneesSection
  let scratch = stor.scratch

  async function initSection () {
    ds.theme = 'zonesAvecImageDeFond'
    scratch = await getNewScratch()
    stor.scratch = scratch
    stor.dejafait2 = []
    stor.dejafait = []
    me.validOnEnter = false // ex donneesSection.touche_entree

    scratch.initParams(me)
    scratch.testFlag = function (b) {
      if (scratch.params.scratchProgDonnees.resetflag) {
        for (var i = 0; i < stor.lesflag.length; i++) {
          stor.lesflag[i].val = false
        }
        scratch.params.scratchProgDonnees.resetflag = false
      }
      for (i = 0; i < stor.lesflag.length; i++) {
        if ((scratch.params.scratchProgDonnees.Lutins[0].cox === stor.lesflag[i].x) &&
          (scratch.params.scratchProgDonnees.Lutins[0].coy === stor.lesflag[i].y) &&
          (scratch.params.scratchProgDonnees.Lutins[0].hauteur === stor.lesflag[i].h) &&
          (stor.lesflag[i].need || b)) {
          stor.lesflag[i].val = true
        }
      }
    }
    scratch.testFlagFin = function () {
      scratch.testFlag(true)
      let ok = true
      for (let i = 0; i < stor.lesflag.length; i++) {
        if (!stor.lesflag[i].val) { ok = false }
      }
      const mess = (ok) ? 'Objetif atteint' : 'Objetif non atteint'
      return { ok, mess }
    }
    ds.nbitems = ds.nbetapes * ds.nbrepetitions
    ds.difficultemin = Math.max(1, ds.difficultemin)
    ds.difficultemax = Math.min(9, ds.difficultemax)
    if (ds.difficultemax < ds.difficultemin) {
      ds.difficultemin = ds.difficultemax
    }

    me.construitStructurePage({ structure: 'presentation3' })
    me.stockage = [0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0]

    me.score = 0
    me.afficheTitre('Réaliser un objectif - Fusée')

    scratch.params.scratchProgDonnees.menu = []
    scratch.params.scratchProgDonnees.progdeb = []
    scratch.params.scratchProgDonnees.progdeb.push({ type: 'Drapo', editable: 'false', deletable: 'false', movable: 'false', x: '20', y: '20' })

    scratch.params.scratchProgDonnees.menu.push({ type: 'catégorie', nom: 'CONTROLES', coul1: '#FFAB19', coul2: '#FFAB19', contenu: [] })
    scratch.params.scratchProgDonnees.menu[0].contenu.push({ type: 'control_repeat', value: [{ nom: 'TIMES', contenu: { valeur: 10 } }] })
    scratch.params.scratchProgDonnees.menu.push({ type: 'catégorie', nom: 'DEPLACEMENTS', coul1: '#4C97FF', coul2: '#3373CC', contenu: [] })
    scratch.params.scratchProgDonnees.menu[1].contenu.push({ type: 'avance_fusee' })
    scratch.params.scratchProgDonnees.menu[1].contenu.push({ type: 'droite_fusee' })
    scratch.params.scratchProgDonnees.menu[1].contenu.push({ type: 'gauche_fusee' })
    scratch.params.scratchProgDonnees.menu.push({ type: 'catégorie', nom: 'ACTIONS', coul1: '#D65CD6', coul2: '#BD42BD', contenu: [] })
    scratch.params.scratchProgDonnees.menu[2].contenu.push({ type: 'decolle_fusee' })
    scratch.params.scratchProgDonnees.menu[2].contenu.push({ type: 'atteri_fusee' })
    enonceMain()
  }

  function enonceMain () {
    me.videLesZones()

    scratch.params.scratchProgDonnees.lesordres = []
    const elem = []
    stor.diff = j3pGetRandomInt(ds.difficultemin, ds.difficultemax)
    if (ds.difficulte === 'Progressive') {
      stor.diff = ds.difficultemin + Math.round((me.questionCourante - 1) * (ds.difficultemax - ds.difficultemin) / (ds.nbrepetitions - 1))
      if (ds.nbrepetitions === 1) { stor.diff = ds.difficultemin }
    }

    stor.yaA = false
    stor.yaD = false
    stor.yaT = false

    const X = 0
    let Y = 9
    const A = 0
    let nbcase = 10
    let H = 1

    scratch.params.scratchProgDonnees.lesordres = []
    let decx, decy
    decx = decy = 0
    stor.lesflag = []
    let lenbdebloc = 0

    if (stor.diff === 1) {
      decy = j3pGetRandomInt(3, 9)
      stor.lesflag.push({ x: 0, y: 9 - decy, h: 1, val: false, need: false })
      scratch.params.scratchProgDonnees.lesordres.push({ type: 'control_repeat', value: [{ nom: 'TIMES', contenu: { valeur: decy } }], statement: [{ nom: 'SUBSTACK', contenu: [{ type: 'avance_fusee' }] }] })
      var cons = '<b>Amène la fusée sur le spationaute en utilisant 3 blocs.</b><br>(En évitant les trous noirs)'
      for (var i = 9; i > 9 - decy - 1; i--) {
        elem.push({ quoi: 'trou', x: 1, y: i })
      }
      lenbdebloc = 3
      elem.push({ quoi: 'cosmo', x: decx, y: 9 - decy })
    }
    if (stor.diff === 2) {
      decy = j3pGetRandomInt(4, 9)
      elem.push({ quoi: 'terre', x: 0, y: 9 })
      H = 0
      stor.lesflag.push({ x: 0, y: 9 - decy, h: 1, val: false, need: false })
      scratch.params.scratchProgDonnees.lesordres.push({ type: 'decolle_fusee' })
      scratch.params.scratchProgDonnees.lesordres.push({ type: 'control_repeat', value: [{ nom: 'TIMES', contenu: { valeur: decy } }], statement: [{ nom: 'SUBSTACK', contenu: [{ type: 'avance_fusee' }] }] })
      cons = '<b>Amène la fusée sur le spationaute en utilisant 4 blocs.</b> <BR>  (En évitant les trous noirs)'
      for (i = 9; i > 9 - decy - 1; i--) {
        elem.push({ quoi: 'trou', x: 1, y: i })
      }
      lenbdebloc = 4
      elem.push({ quoi: 'cosmo', x: decx, y: 9 - decy })
    }
    if (stor.diff === 3) {
      decy = j3pGetRandomInt(4, 9)
      elem.push({ quoi: 'terre', x: 0, y: 9 })
      H = 0
      stor.lesflag.push({ x: 0, y: 9 - decy, h: 0, val: false, need: true })
      scratch.params.scratchProgDonnees.lesordres.push({ type: 'decolle_fusee' })
      scratch.params.scratchProgDonnees.lesordres.push({ type: 'control_repeat', value: [{ nom: 'TIMES', contenu: { valeur: decy } }], statement: [{ nom: 'SUBSTACK', contenu: [{ type: 'avance_fusee' }] }] })
      scratch.params.scratchProgDonnees.lesordres.push({ type: 'atteri_fusee' })
      cons = '<b>Fais atterrir la fusée sur la planète rouge en utilisant 5 blocs.</b> <BR>  (En évitant les trous noirs)'
      for (i = 9; i > 9 - decy - 1; i--) {
        elem.push({ quoi: 'trou', x: 1, y: i })
      }
      lenbdebloc = 5
      elem.push({ quoi: 'mars', x: decx, y: 9 - decy })
    }

    if (stor.diff === 4) {
      decy = j3pGetRandomInt(3, 9)
      elem.push({ quoi: 'terre', x: 0, y: 9 })
      H = 0
      stor.lesflag.push({ x: 1, y: 9 - decy, h: 0, val: false, need: true })
      scratch.params.scratchProgDonnees.lesordres.push({ type: 'decolle_fusee' })
      scratch.params.scratchProgDonnees.lesordres.push({ type: 'control_repeat', value: [{ nom: 'TIMES', contenu: { valeur: decy } }], statement: [{ nom: 'SUBSTACK', contenu: [{ type: 'avance_fusee' }] }] })
      scratch.params.scratchProgDonnees.lesordres.push({ type: 'droite_fusee' })
      scratch.params.scratchProgDonnees.lesordres.push({ type: 'avance_fusee' })
      scratch.params.scratchProgDonnees.lesordres.push({ type: 'atteri_fusee' })
      cons = '<b>Fais atterrir la fusée sur la planète rouge en utilisant 7 blocs.</b> <BR>  (En évitant les trous noirs)'
      for (i = 9; i > 9 - decy - 1; i--) {
        elem.push({ quoi: 'trou', x: 1, y: i })
      }
      decx = 1
      lenbdebloc = 7
      elem.push({ quoi: 'mars', x: decx, y: 9 - decy })
    }
    if (stor.diff === 5) {
      decy = j3pGetRandomInt(3, 9)
      decx = j3pGetRandomInt(3, 9)
      elem.push({ quoi: 'terre', x: 0, y: 9 })
      H = 0
      stor.lesflag.push({ x: decx, y: 9 - decy, h: 0, val: false, need: true })
      scratch.params.scratchProgDonnees.lesordres.push({ type: 'decolle_fusee' })
      scratch.params.scratchProgDonnees.lesordres.push({ type: 'control_repeat', value: [{ nom: 'TIMES', contenu: { valeur: decy } }], statement: [{ nom: 'SUBSTACK', contenu: [{ type: 'avance_fusee' }] }] })
      scratch.params.scratchProgDonnees.lesordres.push({ type: 'droite_fusee' })
      scratch.params.scratchProgDonnees.lesordres.push({ type: 'control_repeat', value: [{ nom: 'TIMES', contenu: { valeur: decx } }], statement: [{ nom: 'SUBSTACK', contenu: [{ type: 'avance_fusee' }] }] })
      scratch.params.scratchProgDonnees.lesordres.push({ type: 'atteri_fusee' })
      cons = '<b>Fais atterrir la fusée sur la planète rouge en utilisant 8 blocs.</b> <BR>  (En évitant les trous noirs)'
      for (i = 9; i > 9 - decy + 1; i--) {
        elem.push({ quoi: 'trou', x: 1, y: i })
      }
      for (i = 1; i < decx + 1; i++) {
        elem.push({ quoi: 'trou', x: i, y: 9 - decy + 1 })
      }
      lenbdebloc = 8
      elem.push({ quoi: 'mars', x: decx, y: 9 - decy })
    }

    if (stor.diff === 6) {
      decy = j3pGetRandomInt(3, 8)
      decx = j3pGetRandomInt(3, 9)
      elem.push({ quoi: 'terre', x: 0, y: 9 })
      H = 0
      stor.lesflag.push({ x: decx, y: 9 - decy - 1, h: 0, val: false, need: true })
      scratch.params.scratchProgDonnees.lesordres.push({ type: 'decolle_fusee' })
      scratch.params.scratchProgDonnees.lesordres.push({ type: 'control_repeat', value: [{ nom: 'TIMES', contenu: { valeur: decy } }], statement: [{ nom: 'SUBSTACK', contenu: [{ type: 'avance_fusee' }] }] })
      scratch.params.scratchProgDonnees.lesordres.push({ type: 'droite_fusee' })
      scratch.params.scratchProgDonnees.lesordres.push({ type: 'control_repeat', value: [{ nom: 'TIMES', contenu: { valeur: decx } }], statement: [{ nom: 'SUBSTACK', contenu: [{ type: 'avance_fusee' }] }] })
      scratch.params.scratchProgDonnees.lesordres.push({ type: 'gauche_fusee' })
      scratch.params.scratchProgDonnees.lesordres.push({ type: 'avance_fusee' })
      scratch.params.scratchProgDonnees.lesordres.push({ type: 'atteri_fusee' })
      cons = '<b>Fais atterrir la fusée sur la planète rouge en utilisant 10 blocs.</b> <BR>  (En évitant les trous noirs)'
      for (i = 9; i > 9 - decy + 1; i--) {
        elem.push({ quoi: 'trou', x: 1, y: i })
      }
      for (i = 1; i < decx + 1; i++) {
        elem.push({ quoi: 'trou', x: i, y: 9 - decy + 1 })
      }
      for (i = 0; i < decx; i++) {
        elem.push({ quoi: 'trou', x: i, y: 9 - decy - 1 })
      }
      decy++
      lenbdebloc = 10
      elem.push({ quoi: 'mars', x: decx, y: 9 - decy })
    }

    let decy2
    if (stor.diff === 7) {
      decy = j3pGetRandomInt(3, 5)
      decx = j3pGetRandomInt(3, 9)
      decy2 = j3pGetRandomInt(3, (9 - decy))
      elem.push({ quoi: 'terre', x: 0, y: 9 })
      H = 0
      stor.lesflag.push({ x: decx, y: 9 - decy - decy2, h: 0, val: false, need: true })
      scratch.params.scratchProgDonnees.lesordres.push({ type: 'decolle_fusee' })
      scratch.params.scratchProgDonnees.lesordres.push({ type: 'control_repeat', value: [{ nom: 'TIMES', contenu: { valeur: decy } }], statement: [{ nom: 'SUBSTACK', contenu: [{ type: 'avance_fusee' }] }] })
      scratch.params.scratchProgDonnees.lesordres.push({ type: 'droite_fusee' })
      scratch.params.scratchProgDonnees.lesordres.push({ type: 'control_repeat', value: [{ nom: 'TIMES', contenu: { valeur: decx } }], statement: [{ nom: 'SUBSTACK', contenu: [{ type: 'avance_fusee' }] }] })
      scratch.params.scratchProgDonnees.lesordres.push({ type: 'gauche_fusee' })
      scratch.params.scratchProgDonnees.lesordres.push({ type: 'control_repeat', value: [{ nom: 'TIMES', contenu: { valeur: decy2 } }], statement: [{ nom: 'SUBSTACK', contenu: [{ type: 'avance_fusee' }] }] })
      scratch.params.scratchProgDonnees.lesordres.push({ type: 'atteri_fusee' })
      cons = '<b>Fais atterrir la fusée sur la planète rouge en utilisant 11 blocs.</b> <BR>  (En évitant les trous noirs)'
      for (i = 9; i > 9 - decy + 1; i--) {
        elem.push({ quoi: 'trou', x: 1, y: i })
      }
      for (i = 1; i < decx + 1; i++) {
        elem.push({ quoi: 'trou', x: i, y: 9 - decy + 1 })
      }
      for (i = 0; i < decx; i++) {
        elem.push({ quoi: 'trou', x: i, y: 9 - decy - 1 })
      }
      if (decx !== 9) {
        for (i = 9 - decy + 1; i > 9 - decy2 - decy; i--) {
          elem.push({ quoi: 'trou', x: decx + 1, y: i })
        }
      }
      for (i = 9 - decy - 1; i > 9 - decy2 - decy; i--) {
        elem.push({ quoi: 'trou', x: decx - 1, y: i })
      }
      decy += decy2
      lenbdebloc = 11
      elem.push({ quoi: 'mars', x: decx, y: 9 - decy })
    }
    if (stor.diff === 8) {
      decy = j3pGetRandomInt(3, 9)

      scratch.params.scratchProgDonnees.lesordres.push({ type: 'control_repeat', value: [{ nom: 'TIMES', contenu: { valeur: decy } }], statement: [{ nom: 'SUBSTACK', contenu: [{ type: 'decolle_fusee' }, { type: 'avance_fusee' }, { type: 'atteri_fusee' }] }] })

      cons = '<b>La fusée doit atterrir sur toutes les planètes puis atterrir sur la planète rouge.</b> <BR>  (En évitant les trous noirs)'
      for (i = 8; i > 9 - decy - 1; i--) {
        elem.push({ quoi: 'trou', x: 1, y: i })
      }
      for (i = 9; i > 9 - decy; i--) {
        elem.push({ quoi: 'terre', x: 0, y: i })
        stor.lesflag.push({ x: 0, y: i, h: 0, val: false, need: true })
      }

      lenbdebloc = 5
      elem.push({ quoi: 'mars', x: decx, y: 9 - decy })
      H = 0
    }

    if (stor.diff === 9) {
      H = 0
      decy = j3pGetRandomInt(2, 4)
      decy2 = j3pGetRandomInt(3, 4)
      nbcase = decy * decy2 + 1

      for (i = 0; i < decy; i++) {
        // stor.plan[0][stor.taillex-1-(i*decy2+decy2)] = 2;
        let buf = 'terre'
        if (i === decy - 1) { buf = 'mars' }
        elem.push({ quoi: buf, x: 0, y: nbcase - 1 - (i * decy2 + decy2) })
        stor.lesflag.push({ x: 0, y: nbcase - 1 - (i * decy2 + decy2), h: 0, val: false, need: true })
      }
      Y = nbcase - 1
      elem.push({ quoi: 'terre', x: 0, y: nbcase - 1 })

      scratch.params.scratchProgDonnees.lesordres.push({ type: 'control_repeat', value: [{ nom: 'TIMES', contenu: { valeur: decy } }], statement: [{ nom: 'SUBSTACK', contenu: [{ type: 'decolle_fusee' }, { type: 'control_repeat', value: [{ nom: 'TIMES', contenu: { valeur: decy2 } }], statement: [{ nom: 'SUBSTACK', contenu: [{ type: 'avance_fusee' }] }] }, { type: 'atteri_fusee' }] }] })

      cons = '<b>La fusée doit atterrir sur toutes les planètes puis atterrir sur la planète rouge.</b> <BR>  (En évitant les trous noirs)'
      for (i = 0; i < nbcase; i++) {
        elem.push({ quoi: 'trou', x: 1, y: i })
      }

      lenbdebloc = 6
    }

    scratch.initFusee(nbcase, X, Y, H, A, elem)
    scratch.enonce([], {
      pourInject: { },
      modeCorrection: {
        type: 'SansComparaison',
        aVerif: [{ quoi: 'NombreMaxBlocs', combien: lenbdebloc + ds.tolerance },
          { quoi: 'QuandAvertir', quand: 'apres' }]
      },
      dirigeProg: { sequence: ds.Sequence, pasApas: ds.PasAPas, cacheTest: ds.CacheTest }
    })
    const yy = addDefaultTable(scratch.lesdivs.enonceG, 1, 1)[0][0]

    j3pAddContent(yy, cons)

    me.finEnonce()
    me.cacheBoutonValider()
  }

  switch (this.etat) {
    case 'enonce':
      if (this.debutDeLaSection) {
        initSection()
      } else {
        enonceMain()
      }
      break // case "enonce":

    case 'correction':
      try {
        const ret = scratch.prepCorrec()
        const xmlDoc2 = ret.xmlDoc2
        const xmlText = ret.xmlText

        if (scratch.params.scratchProgDonnees.repEleve) {
          // Bonne réponse
          this.score++
          scratch.corrigeGood(xmlDoc2, xmlText)
        } else {
          // Pas de bonne réponse
          scratch.corrige(xmlDoc2, xmlText)
        }
        // on veut être rappelé dans l’état navigation
        this.finCorrection('navigation', true)
      } catch (error) {
        console.error(error)
      }
      break // case "correction":

    case 'navigation':

      if (this.sectionTerminee()) {
        scratch.lesdivs.BoutonsSuite.appendChild(this.buttonsElts.sectionSuivante)
        this.buttonsElts.sectionSuivante.addEventListener('click', () => {
          scratch.params.grosStop = true
        })
      } else {
        this.etat = 'enonce'
        scratch.lesdivs.BoutonsSuite.appendChild(this.buttonsElts.suite)
        this.buttonsElts.suite.addEventListener('click', () => {
          scratch.params.grosStop = true
        })
      }
      // Obligatoire
      this.finNavigation()

      break // case "navigation":
  }
}
