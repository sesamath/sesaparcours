import { j3pAddContent, j3pGetRandomBool, j3pGetRandomInt, j3pShuffle } from 'src/legacy/core/functions'
import { getNewScratch } from 'src/legacy/outils/scratch/loaders'
import { addDefaultTable } from 'src/legacy/themes/table'

export const params = {
  outils: ['mathquill'],
  /**
   * Ce qui est paramétrable dans l’éditeur de graphe
   * @type {LegacySectionParamsParametres}
   */
  parametres: [
    ['nbrepetitions', 4, 'entier', 'Nombre de répétitions de la section'],
    ['nbchances', 3, 'entier', 'Nombre d’essais possibles (illimité si 0)'],
    ['PasAPas', true, 'boolean', "<u>true</u>: L’élève peut lancer son programme en mode 'Pas à pas'."],
    ['Sequence', true, 'boolean', '<u>true</u>: Chaque bloc a une durée d’exécution d’environ 1 seconde.'],
    ['difficultemin', 1, 'entier', 'Difficulté du programme (minimum 1)'],
    ['difficultemax', 4, 'entier', 'Difficulté du programme (maximum 4)'],
    ['difficulte', 'Progressive', 'liste', 'Gestion de la progression (entre min et max)', ['Aléatoire', 'Progressive']],
    ['tolerance', 0, 'entier', 'Nombre de blocs supplementaires acceptés'],
    ['CacheTest', false, 'boolean', '<u>true</u>: L’élève ne voit pas l’action de son programme lors d la correction.']
  ]

}

/**
 * section blokfuseesi
 * @this {Parcours}
 */
export default function main () {
  const me = this
  const stor = me.storage
  const ds = me.donneesSection
  let scratch = stor.scratch

  async function initSection () {
    ds.theme = 'zonesAvecImageDeFond'
    scratch = await getNewScratch()
    stor.scratch = scratch
    me.validOnEnter = false // ex donneesSection.touche_entree

    scratch.initParams(me)
    scratch.testFlag = function () {
    }
    scratch.testFlagFin = function () {
      let ok = true
      let mess = ''
      if (scratch.params.scratchProgDonnees.Lutins[0].cox !== 3) { ok = false; mess = 'La fusée n’a pas besoin de tourner !' } else if (scratch.params.scratchProgDonnees.Lutins[0].coy !== 6 - stor.leY) { ok = false; mess = 'La fusée doit aller jusqu’au radar !' } else if (scratch.params.scratchProgDonnees.cartefusee[3][6 - stor.leY] === 3) {
        if (scratch.params.scratchProgDonnees.Lutins[0].hauteur !== 0) { ok = false; mess = 'La fusée doit atterrir sur la planète rouge !!' }
      } else if (scratch.params.scratchProgDonnees.cartefusee[3][6 - stor.leY] === 1) {
        if (scratch.params.scratchProgDonnees.Lutins[0].hauteur !== 0) { ok = false; mess = 'La fusée doit atterrir !!' }
        if (scratch.params.scratchProgDonnees.Lutins[0].angle !== 180) { ok = false; mess = 'La fusée doit faire demi-tour !!' }
      } else if (scratch.params.scratchProgDonnees.cartefusee[3][6 - stor.leY] === 4) { ok = false; mess = 'La fusée doit récupérer le spationaute !!' }

      return { ok, mess }
    }
    scratch.remiseAzero = function () {
    }
    scratch.scratchResetFlag = function () {
    }
    ds.nbitems = ds.nbetapes * ds.nbrepetitions

    scratch.params.forcealeatoire = true
    ds.difficultemin = Math.min(4, Math.max(1, ds.difficultemin))
    ds.difficultemax = Math.max(1, Math.min(4, ds.difficultemax))
    if (ds.difficultemax < ds.difficultemin) {
      ds.difficultemin = ds.difficultemax
    }

    me.construitStructurePage({ structure: 'presentation3' })
    me.stockage = [0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0]

    me.score = 0
    me.afficheTitre('Réaliser un objectif - Fusée')

    scratch.params.scratchProgDonnees.menu = []
    scratch.params.scratchProgDonnees.progdeb = []
    scratch.params.scratchProgDonnees.progdeb.push({ type: 'Drapo', editable: 'false', deletable: 'false', movable: 'false', x: '20', y: '20' })

    scratch.params.scratchProgDonnees.menu.push({ type: 'catégorie', nom: 'CONDITIONS', coul1: '#40BF4A', coul2: '#389438', contenu: [] })
    scratch.params.scratchProgDonnees.menu[0].contenu.push({ type: 'operator_fusee_planet' })
    scratch.params.scratchProgDonnees.menu[0].contenu.push({ type: 'operator_fusee_cosmo' })
    scratch.params.scratchProgDonnees.menu.push({ type: 'catégorie', nom: 'CONTROLES', coul1: '#FFAB19', coul2: '#FFAB19', contenu: [] })
    scratch.params.scratchProgDonnees.menu[1].contenu.push({ type: 'control_if' })
    scratch.params.scratchProgDonnees.menu[1].contenu.push({ type: 'control_if_else' })
    scratch.params.scratchProgDonnees.menu[1].contenu.push({ type: 'control_repeat', value: [{ nom: 'TIMES', contenu: { valeur: 10 } }] })
    scratch.params.scratchProgDonnees.menu.push({ type: 'catégorie', nom: 'DEPLACEMENTS', coul1: '#4C97FF', coul2: '#3373CC', contenu: [] })
    scratch.params.scratchProgDonnees.menu[2].contenu.push({ type: 'avance_fusee' })
    scratch.params.scratchProgDonnees.menu[2].contenu.push({ type: 'droite_fusee' })
    scratch.params.scratchProgDonnees.menu[2].contenu.push({ type: 'gauche_fusee' })
    scratch.params.scratchProgDonnees.menu.push({ type: 'catégorie', nom: 'ACTIONS', coul1: '#D65CD6', coul2: '#BD42BD', contenu: [] })
    scratch.params.scratchProgDonnees.menu[3].contenu.push({ type: 'decolle_fusee' })
    scratch.params.scratchProgDonnees.menu[3].contenu.push({ type: 'atteri_fusee' })
    scratch.params.scratchProgDonnees.menu[3].contenu.push({ type: 'recuperer_fusee' })
    enonceMain()
  }

  function enonceMain () {
    me.videLesZones()
    scratch.params.scratchProgDonnees.lesordres = []
    stor.listefaite = []
    const elem = []
    stor.diff = j3pGetRandomInt(ds.difficultemin, ds.difficultemax)
    if (ds.difficulte === 'Progressive') {
      stor.diff = ds.difficultemin + Math.round((me.questionCourante - 1) * (ds.difficultemax - ds.difficultemin) / (ds.nbrepetitions - 1))
      if (ds.nbrepetitions === 1) { stor.diff = ds.difficultemin }
    }

    stor.yadec = j3pGetRandomBool()
    let lenbdebloc = 0
    let H = 1
    if (stor.yadec) {
      lenbdebloc++
      H = 0
      scratch.params.scratchProgDonnees.lesordres.push({ type: 'decolle_fusee' })
      elem.push({ quoi: 'terre', x: 3, y: 6 })
    }

    stor.leY = j3pGetRandomInt(3, 6)

    scratch.params.scratchProgDonnees.lesordres.push({ type: 'control_repeat', value: [{ nom: 'TIMES', contenu: { valeur: stor.leY } }], statement: [{ nom: 'SUBSTACK', contenu: [{ type: 'avance_fusee' }] }] })

    const nbcase = 7
    const X = 3
    const Y = 6
    const A = 0

    stor.radarpos = []
    let cons
    switch (stor.diff) {
      case 1: cons = 'La fusée doit se rendre sur le signal radar pour savoir ce qu’il s’y trouve. <BR> <b> Si il s’agit d’une nouvelle planète, elle doit y atterrir. </b>'
        stor.radarpos.push('')
        stor.radarpos.push('mars')
        scratch.params.scratchProgDonnees.lesordres.push({ type: 'control_if', value: [{ nom: 'CONDITION', contenu: { type: 'operator_fusee_planet' } }], statement: [{ nom: 'SUBSTACK', contenu: [{ type: 'atteri_fusee' }] }] })

        lenbdebloc += 6
        scratch.params.validation = [{ nbvalid: 2 }]
        break
      case 2: cons = 'La fusée doit se rendre sur le signal radar pour savoir ce qu’il s’y trouve. <BR> <b> Si il s’agit d’un spationaute, elle doit le récupérer. </b>'
        stor.radarpos.push('')
        stor.radarpos.push('cosmo')
        scratch.params.scratchProgDonnees.lesordres.push({ type: 'control_if', value: [{ nom: 'CONDITION', contenu: { type: 'operator_fusee_cosmo' } }], statement: [{ nom: 'SUBSTACK', contenu: [{ type: 'recuperer_fusee' }] }] })

        lenbdebloc += 6
        scratch.params.validation = [{ nbvalid: 2 }]
        break
      case 3:cons = 'La fusée doit se rendre sur le signal radar <i>(il y a forcément quelque chose sous ce radar). <br></i> <b> Si il s’agit d’une nouvelle planète, elle doit y atterrir ;  Si il s’agit d’un spationaute, elle doit le récupérer. </b>'
        stor.radarpos.push('cosmo')
        stor.radarpos.push('mars')
        lenbdebloc += 7
        scratch.params.validation = [{ nbvalid: 2 }]
        scratch.params.scratchProgDonnees.lesordres.push({ type: 'control_if_else', value: [{ nom: 'CONDITION', contenu: { type: 'operator_fusee_cosmo' } }], statement: [{ nom: 'SUBSTACK', contenu: [{ type: 'recuperer_fusee' }] }, { nom: 'SUBSTACK2', contenu: [{ type: 'atteri_fusee' }] }] })

        break
      case 4:cons = 'La fusée doit se rendre sur le signal radar <i>(il y a forcément quelque chose sous ce radar). <br></i> <b> Si il s’agit d’une planète rouge, elle doit y atterrir ; <br> si il s’agit d’un spationaute, elle doit le récupérer ; <br>si il s’agit d’une planète terre, elle doit faire un demi-tour avant de se poser. </b>'
        stor.radarpos.push('cosmo')
        stor.radarpos.push('mars')
        stor.radarpos.push('terre')
        scratch.params.validation = [{ nbvalid: 3 }]
        scratch.params.scratchProgDonnees.lesordres.push({ type: 'control_if_else', value: [{ nom: 'CONDITION', contenu: { type: 'operator_fusee_cosmo' } }], statement: [{ nom: 'SUBSTACK', contenu: [{ type: 'recuperer_fusee' }] }, { nom: 'SUBSTACK2', contenu: [{ type: 'control_if_else', value: [{ nom: 'CONDITION', contenu: { type: 'operator_fusee_planet' } }], statement: [{ nom: 'SUBSTACK', contenu: [{ type: 'atteri_fusee' }] }, { nom: 'SUBSTACK2', contenu: [{ type: 'droite_fusee' }, { type: 'droite_fusee' }, { type: 'atteri_fusee' }] }] }] }] })
        lenbdebloc += 12
        break
    }
    scratch.params.radarPos = j3pShuffle(stor.radarpos)
    elem.push({ quoi: 'radar', x: 3, y: 6 - stor.leY, cache: stor.radarpos[j3pGetRandomInt(0, stor.radarpos.length - 1)] })

    scratch.initFusee(nbcase, X, Y, H, A, elem)
    scratch.enonce([], {
      pourInject: { },
      modeCorrection: {
        type: 'SansComparaison',
        aVerif: [{ quoi: 'NombreMaxBloc', nombre: lenbdebloc },
          { quoi: 'QuandAvertir', quand: 'apres' }]
      },
      dirigeProg: { sequence: ds.Sequence, pasApas: ds.PasAPas, cacheTest: ds.CacheTest }
    })
    const yy = addDefaultTable(scratch.lesdivs.enonceG, 1, 1)[0][0]
    j3pAddContent(yy, cons)

    me.finEnonce()
    me.cacheBoutonValider()
  }

  switch (this.etat) {
    case 'enonce':
      if (this.debutDeLaSection) {
        initSection()
      } else {
        enonceMain()
      }
      break // case "enonce":

    case 'correction':
      try {
        const ret = scratch.prepCorrec()
        const xmlDoc2 = ret.xmlDoc2
        const xmlText = ret.xmlText

        if (scratch.params.scratchProgDonnees.repEleve) {
          // Bonne réponse
          this.score++
          scratch.corrigeGood(xmlDoc2, xmlText)
        } else {
          // Pas de bonne réponse
          scratch.corrige(xmlDoc2, xmlText)
        }
        // on veut être rappelé dans l’état navigation
        this.finCorrection('navigation', true)
      } catch (error) {
        console.error(error)
      }
      break // case "correction":

    case 'navigation':

      if (this.sectionTerminee()) {
        scratch.lesdivs.BoutonsSuite.appendChild(this.buttonsElts.sectionSuivante)
        this.buttonsElts.sectionSuivante.addEventListener('click', () => {
          scratch.params.grosStop = true
        })
      } else {
        this.etat = 'enonce'
        scratch.lesdivs.BoutonsSuite.appendChild(this.buttonsElts.suite)
        this.buttonsElts.suite.addEventListener('click', () => {
          scratch.params.grosStop = true
        })
      }
      // Obligatoire
      this.finNavigation()

      break // case "navigation":
  }
}
