import { j3pGetRandomInt, j3pNotify } from 'src/legacy/core/functions'
import BulleAide from 'src/legacy/outils/bulleAide/BulleAide'
import { addDefaultTable } from 'src/legacy/themes/table'
import { getNewScratch } from 'src/legacy/outils/scratch/loaders'

export const params = {
  outils: ['mathquill'],
  /**
   * Ce qui est paramétrable dans l’éditeur de graphe
   * @type {LegacySectionParamsParametres}
   */
  parametres: [
    ['nbrepetitions', 4, 'entier', 'Nombre de répétitions de la section'],
    ['nbchances', 2, 'entier', 'Nombre d’essais possibles (illimité si 0)'],
    ['PasAPas', true, 'boolean', "<u>true</u>: L’élève peut lancer son programme en mode 'Pas à pas'."],
    ['Sequence', true, 'boolean', '<u>true</u>: Chaque bloc a une durée d’exécution d’environ 1 seconde.'],
    ['CacheTest', false, 'boolean', '<u>true</u>: L’élève ne voit pas l’action de son programme lors d la correction.']
  ]

}

/**
 * section blokfuseeordre
 * @this {Parcours}
 */
export default function main () {
  const me = this
  const stor = me.storage
  const ds = me.donneesSection
  let scratch = stor.scratch

  function tradordre (el) {
    switch (el.type) {
      case 'avance_fusee': return 'avancer'
      case 'droite_fusee': return 'tourner à droite'
      case 'gauche_fusee': return 'tourner à gauche'
      case 'decolle_fusee': return 'décoller'
      case 'atteri_fusee': return 'atterrir'
    }
  }
  async function initSection () {
    ds.theme = 'zonesAvecImageDeFond'
    scratch = await getNewScratch()
    stor.scratch = scratch
    stor.dejafait2 = []
    stor.dejafait = []
    me.validOnEnter = false // ex donneesSection.touche_entree

    scratch.initParams(me)
    scratch.testFlag = function () {
    }
    scratch.testFlagFin = function () {
      let ok = true
      let mess = ''
      const madein = []
      for (let i = 0; i < scratch.params.scratchProgDonnees.lesordres.length; i++) {
        madein.push(scratch.params.scratchProgDonnees.lesordres[i].type)
      }
      const tabout = scratch.params.scratchProgDonnees.tabcodeX[0]
      if (!tabout) {
        j3pNotify('pour tom tabout vide', { tabcodeX: scratch.params.scratchProgDonnees.tabcodeX, worspace: scratch.params.workspaceEl })
        return { ok: false, mess: 'Tu dois placer deux blocs <br> sous le bloc "<b>Quand Drapeau pressé</b>"!' }
      }
      const made = []
      for (let i = 1; i < tabout.length - 1; i++) {
        made.push(tabout[i].nom)
      }
      if (madein.length !== made.length) return { ok: false, mess: 'Tu dois placer deux blocs <br> sous le bloc "<b>Quand Drapeau pressé</b>"!' }
      for (let i = 0; i < madein.length; i++) {
        if (madein[i] !== made[i]) { ok = false; mess = 'L’ordre des blocs est important !' }
      }
      return { ok, mess }
    }
    me.construitStructurePage({ structure: 'presentation3' })
    me.stockage = [0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0]
    ds.nbitems = ds.nbetapes * ds.nbrepetitions

    me.score = 0
    stor.dejafait = []
    me.afficheTitre('Programmer par bloc - Fusée')

    // if (ds.indication !="") me.indication(me.zones.IG,ds.indication);

    scratch.params.scratchProgDonnees.menu = []
    scratch.params.scratchProgDonnees.progdeb = []
    scratch.params.scratchProgDonnees.progdeb.push({ type: 'Drapo', editable: 'false', deletable: 'false', movable: 'false', x: '20', y: '20' })

    scratch.params.scratchProgDonnees.menu.push({ type: 'catégorie', nom: 'DEPLACEMENTS', coul1: '#4C97FF', coul2: '#3373CC', contenu: [] })
    scratch.params.scratchProgDonnees.menu[0].contenu.push({ type: 'avance_fusee' })
    scratch.params.scratchProgDonnees.menu[0].contenu.push({ type: 'droite_fusee' })
    scratch.params.scratchProgDonnees.menu[0].contenu.push({ type: 'gauche_fusee' })
    scratch.params.scratchProgDonnees.menu.push({ type: 'catégorie', nom: 'ACTIONS', coul1: '#D65CD6', coul2: '#BD42BD', contenu: [] })
    scratch.params.scratchProgDonnees.menu[1].contenu.push({ type: 'decolle_fusee' })
    scratch.params.scratchProgDonnees.menu[1].contenu.push({ type: 'atteri_fusee' })
    enonceMain()
  }

  function enonceMain () {
    me.videLesZones()

    scratch.params.scratchProgDonnees.lesordres = []
    const elem = []

    /// /////////////

    const X = 1
    const Y = 1
    let Hauteurbase = 1
    const Anglebase = 90
    const nbcase = 4

    let u
    if (me.questionCourante % 2 === 1) {
      let petitcompte = 0
      do {
        petitcompte++
        u = j3pGetRandomInt(0, 1)
      } while ((petitcompte < 100) && (stor.dejafait2.indexOf(u) !== -1))
      stor.dejafait2.push(u)

      if (u === 0) {
        scratch.params.scratchProgDonnees.lesordres.push({ type: 'decolle_fusee' })
        scratch.params.scratchProgDonnees.lesordres.push({ type: 'avance_fusee' })
        elem.push({ quoi: 'terre', x: 1, y: 1 })
        Hauteurbase = 0
      } else {
        scratch.params.scratchProgDonnees.lesordres.push({ type: 'avance_fusee' })
        scratch.params.scratchProgDonnees.lesordres.push({ type: 'atteri_fusee' })
        elem.push({ quoi: 'terre', x: 2, y: 1 })
      }
    } else {
      const lesordrespos = [{ type: 'avance_fusee' }, { type: 'droite_fusee' }, { type: 'gauche_fusee' }]

      let petitcompte = 0
      do {
        petitcompte++
        u = j3pGetRandomInt(0, 2)
      } while ((petitcompte < 100) && (stor.dejafait.indexOf(u) !== -1))
      stor.dejafait.push(u)
      scratch.params.scratchProgDonnees.lesordres.push(lesordrespos[u])
      lesordrespos.splice(u, 1)
      scratch.params.scratchProgDonnees.lesordres.push(lesordrespos[j3pGetRandomInt(0, 1)])
    }

    scratch.initFusee(nbcase, X, Y, Hauteurbase, Anglebase, elem)
    scratch.enonce([], {
      pourInject: { },
      modeCorrection: {
        type: 'SansComparaison',
        aVerif: [{ quoi: 'NombreDeBlocs', combien: 3 },
          { quoi: 'BesoinBloc', nom: scratch.params.scratchProgDonnees.lesordres[0].type },
          { quoi: 'BesoinBloc', nom: scratch.params.scratchProgDonnees.lesordres[1].type },
          { quoi: 'BesoinniemeBloc', nom: scratch.params.scratchProgDonnees.lesordres[0].type, place: 1 },
          { quoi: 'QuandAvertir', quand: 'apres' }]
      },
      dirigeProg: { sequence: ds.Sequence, pasApas: ds.PasAPas, cacheTest: ds.CacheTest }
    })

    const bufd = '<b> Attache les blocs <i><u>' + tradordre(scratch.params.scratchProgDonnees.lesordres[0]) + '</u></i> puis  <i><u>' + tradordre(scratch.params.scratchProgDonnees.lesordres[1]) + ' </u></i>  au programme. </b>'
    const uu = addDefaultTable(scratch.lesdivs.enonceG, 1, 2)
    uu[0][0].innerHTML = bufd + '&nbsp;'
    stor.aise = new BulleAide(uu[0][1], 'Fais glisser les blocs utiles sous le bloc jaune.', { place: 5 })

    me.finEnonce()
    me.cacheBoutonValider()
  }

  switch (this.etat) {
    case 'enonce':
      if (this.debutDeLaSection) {
        initSection()
      } else {
        enonceMain()
      }
      break // case "enonce":

    case 'correction':
      try {
        const ret = scratch.prepCorrec()
        const xmlDoc2 = ret.xmlDoc2
        const xmlText = ret.xmlText

        if (scratch.params.scratchProgDonnees.repEleve) {
          // Bonne réponse
          this.score++
          scratch.corrigeGood(xmlDoc2, xmlText)
        } else {
          // Pas de bonne réponse
          scratch.corrige(xmlDoc2, xmlText)
        }
        // on veut être rappelé dans l’état navigation
        this.finCorrection('navigation', true)
      } catch (error) {
        console.error(error)
      }
      break

    case 'navigation':

      if (this.sectionTerminee()) {
        scratch.lesdivs.BoutonsSuite.appendChild(this.buttonsElts.sectionSuivante)
        this.buttonsElts.sectionSuivante.addEventListener('click', () => {
          scratch.params.grosStop = true
        })
      } else {
        this.etat = 'enonce'
        scratch.lesdivs.BoutonsSuite.appendChild(this.buttonsElts.suite)
        this.buttonsElts.suite.addEventListener('click', () => {
          scratch.params.grosStop = true
        })
      }
      // Obligatoire
      this.finNavigation()

      break // case "navigation":
  }
}
