import { j3pGetRandomBool, j3pGetRandomInt } from 'src/legacy/core/functions'
import BulleAide from 'src/legacy/outils/bulleAide/BulleAide'
import { addDefaultTable } from 'src/legacy/themes/table'
import { getNewScratch } from 'src/legacy/outils/scratch/loaders'

export const params = {
  outils: ['mathquill'],
  /**
   * Ce qui est paramétrable dans l’éditeur de graphe
   * @type {LegacySectionParamsParametres}
   */
  parametres: [
    ['nbrepetitions', 4, 'entier', 'Nombre de répétitions de la section'],
    ['nbchances', 3, 'entier', 'Nombre d’essais possibles (illimité si 0)'],
    ['PasAPas', true, 'boolean', "<u>true</u>: L’élève peut lancer son programme en mode 'Pas à pas'."],
    ['Sequence', true, 'boolean', '<u>true</u>: Chaque bloc a une durée d’exécution d’environ 1 seconde.'],
    ['difficultemin', 3, 'entier', 'Difficulté du programme (minimum 3)'],
    ['difficultemax', 12, 'entier', 'Difficulté du programme (maximum 12)'],
    ['difficulte', 'Progressive', 'liste', 'Gestion de la progression (entre min et max)', ['Aléatoire', 'Progressive']],
    ['tolerance', 0, 'entier', 'Nombre de blocs supplementaires acceptés'],
    ['CacheTest', false, 'boolean', '<u>true</u>: L’élève ne voit pas l’action de son programme lors d la correction.']
  ]

}

/**
 * section blokfuseeobjectif
 * @this {Parcours}
 */
export default function main () {
  const me = this
  const stor = me.storage
  const ds = me.donneesSection
  let scratch = stor.scratch

  function ajb (lequel) {
    switch (lequel) {
      case 'A':
        if (((stor.nbblocattendurestant > 9) || (j3pGetRandomBool())) && (stor.nbblocattendurestant > 2)) {
          stor.yaA = true
          stor.nbblocattendurestant--
          return
        } else {
          return
        }
      case 'D':
        if (((stor.nbblocattendurestant > 9) || (j3pGetRandomBool())) && (stor.nbblocattendurestant > 2)) {
          stor.yaD = true
          stor.nbblocattendurestant--
          return
        } else {
          return
        }
      case 'T':
        if (((stor.nbblocattendurestant > 6) || (j3pGetRandomBool())) && (stor.nbblocattendurestant > 3)) {
          stor.yaT = true
          stor.nbblocattendurestant--
        }
    }
  }

  async function initSection () {
    ds.theme = 'zonesAvecImageDeFond'
    scratch = await getNewScratch()
    stor.scratch = scratch

    stor.dejafait2 = []
    stor.dejafait = []
    me.surcharge()
    me.validOnEnter = false // ex donneesSection.touche_entree

    scratch.initParams(me)
    scratch.testFlag = function () {
    }
    scratch.testFlagFin = function () {
      let ok = true
      let mess = 'Objectif atteint'
      if ((stor.yaA) && (scratch.params.scratchProgDonnees.Lutins[0].hauteur === 1)) { ok = false; mess = 'La fusée doit atterrir sur une planète !' }
      if ((!stor.yaA) && (scratch.params.scratchProgDonnees.Lutins[0].hauteur === 0)) { ok = false; mess = 'La fusée ne doit pas attérir sur une planète !' }
      if (stor.Yatt !== scratch.params.scratchProgDonnees.Lutins[0].coy) { ok = false; mess = 'La fusée ne se trouve pas au bon endroit !' }
      if (stor.Xatt !== scratch.params.scratchProgDonnees.Lutins[0].cox) { ok = false; mess = 'La fusée ne se trouve pas au bon endroit !' }
      return { ok, mess }
    }
    ds.nbitems = ds.nbetapes * ds.nbrepetitions

    ds.difficultemin = Math.min(12, Math.max(3, ds.difficultemin))
    ds.difficultemax = Math.max(3, Math.min(12, ds.difficultemax))
    if (ds.difficultemax < ds.difficultemin) {
      ds.difficultemin = ds.difficultemax
    }

    me.construitStructurePage({ structure: 'presentation3' })
    me.stockage = [0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0]

    me.score = 0
    me.afficheTitre('Réaliser un objectif - Fusée')

    scratch.params.scratchProgDonnees.menu = []
    scratch.params.scratchProgDonnees.progdeb = []
    scratch.params.scratchProgDonnees.progdeb.push({ type: 'Drapo', editable: 'false', deletable: 'false', movable: 'false', x: '20', y: '20' })

    scratch.params.scratchProgDonnees.menu.push({ type: 'catégorie', nom: 'DEPLACEMENTS', coul1: '#4C97FF', coul2: '#3373CC', contenu: [] })
    scratch.params.scratchProgDonnees.menu[0].contenu.push({ type: 'avance_fusee' })
    scratch.params.scratchProgDonnees.menu[0].contenu.push({ type: 'droite_fusee' })
    scratch.params.scratchProgDonnees.menu[0].contenu.push({ type: 'gauche_fusee' })
    scratch.params.scratchProgDonnees.menu.push({ type: 'catégorie', nom: 'ACTIONS', coul1: '#D65CD6', coul2: '#BD42BD', contenu: [] })
    scratch.params.scratchProgDonnees.menu[1].contenu.push({ type: 'decolle_fusee' })
    scratch.params.scratchProgDonnees.menu[1].contenu.push({ type: 'atteri_fusee' })
    enonceMain()
  }

  function enonceMain () {
    me.videLesZones()

    scratch.params.scratchProgDonnees.lesordres = []
    const elem = []

    stor.nbblocattendu = j3pGetRandomInt(ds.difficultemin, ds.difficultemax)
    if (ds.difficulte === 'Progressive') {
      stor.nbblocattendu = ds.difficultemin + Math.round((me.questionCourante - 1) * (ds.difficultemax - ds.difficultemin) / (ds.nbrepetitions - 1))
      if (ds.nbrepetitions === 1) { stor.nbblocattendu = ds.difficultemin }
    }

    stor.yaA = false
    stor.yaD = false
    stor.yaT = false

    stor.nbblocattendurestant = stor.nbblocattendu

    const needT = ['A', 'T', 'D']
    const testb = []
    for (var i = 0; i < 3; i++) {
      const haz = j3pGetRandomInt(0, needT.length - 1)
      testb.push(needT[haz])
      needT.splice(haz, 1)
    }
    for (i = 0; i < testb.length; i++) {
      ajb(testb[i])
    }

    scratch.params.scratchProgDonnees.lesordres = []
    for (i = 0; i < stor.nbblocattendurestant; i++) {
      scratch.params.scratchProgDonnees.lesordres.push({ type: 'avance_fusee' })
    }

    if (stor.yaT) {
      const cote = j3pGetRandomInt(0, 1)
      if (cote === 0) { var buf = 'droite_fusee' } else { buf = 'gauche_fusee' }
      const pl = Math.max((scratch.params.scratchProgDonnees.lesordres.length - 3), 1)
      const pl2 = Math.min(6, scratch.params.scratchProgDonnees.lesordres.length - 1)
      scratch.params.scratchProgDonnees.lesordres.splice((j3pGetRandomInt(pl, pl2)), 0, { type: buf })
    }
    if (stor.yaD) {
      scratch.params.scratchProgDonnees.lesordres.splice(0, 0, { type: 'decolle_fusee' })
    }
    if (stor.yaA) {
      scratch.params.scratchProgDonnees.lesordres.push({ type: 'atteri_fusee' })
    }

    const X = 3
    const Y = 6
    const A = 0
    const nbcase = 7
    let H = 1

    if (stor.yaD) {
      H = 0
      elem.push({ quoi: 'terre', x: 3, y: 6 })
    };
    let xpr = 3
    let ypr = 6
    let kx = 0
    let ky = -1
    for (i = 0; i < scratch.params.scratchProgDonnees.lesordres.length; i++) {
      if (scratch.params.scratchProgDonnees.lesordres[i].type === 'avance_fusee') {
        xpr = xpr + kx
        ypr = ypr + ky
      }
      if (scratch.params.scratchProgDonnees.lesordres[i].type === 'droite_fusee') {
        kx = 1; ky = 0
      }
      if (scratch.params.scratchProgDonnees.lesordres[i].type === 'gauche_fusee') {
        kx = -1; ky = 0
      }
    }
    let unecons = 'pour qu’elle atterrisse sur la planète rouge. '
    if (stor.yaA) {
      elem.push({ quoi: 'mars', x: xpr, y: ypr })
    } else {
      elem.push({ quoi: 'cosmo', x: xpr, y: ypr })
      unecons = 'pour qu’elle s’arrête sur le spationaute. '
    }
    stor.Xatt = xpr
    stor.Yatt = ypr

    scratch.initFusee(nbcase, X, Y, H, A, elem)
    scratch.enonce([], {
      pourInject: { },
      modeCorrection: {
        type: 'SansComparaison',
        aVerif: [{ quoi: 'NombreMaxBlocs', combien: stor.nbblocattendu + ds.tolerance + 1 },
          { quoi: 'QuandAvertir', quand: 'apres' }]
      },
      dirigeProg: { sequence: ds.Sequence, pasApas: ds.PasAPas, cacheTest: ds.CacheTest }
    })

    const uu = addDefaultTable(scratch.lesdivs.enonceG, 2, 1)
    uu[0][0].innerHTML = '<b> Programme la fusée </b> ' + unecons
    stor.aise = new BulleAide(uu[1][0], 'Fais glisser les blocs utiles sous le bloc jaune.', { place: 5 })

    me.finEnonce()
    me.cacheBoutonValider()
  }

  switch (this.etat) {
    case 'enonce':
      if (this.debutDeLaSection) {
        initSection()
      } else {
        enonceMain()
      }
      break // case "enonce":

    case 'correction':
      try {
        const ret = scratch.prepCorrec()
        const xmlDoc2 = ret.xmlDoc2
        const xmlText = ret.xmlText

        if (scratch.params.scratchProgDonnees.repEleve) {
          // Bonne réponse
          this.score++
          scratch.corrigeGood(xmlDoc2, xmlText)
        } else {
          // Pas de bonne réponse
          scratch.corrige(xmlDoc2, xmlText)
        }
        // on veut être rappelé dans l’état navigation
        this.finCorrection('navigation', true)
      } catch (error) {
        console.error(error)
      }
      break

    case 'navigation':

      if (this.sectionTerminee()) {
        scratch.lesdivs.BoutonsSuite.appendChild(this.buttonsElts.sectionSuivante)
        this.buttonsElts.sectionSuivante.addEventListener('click', () => {
          scratch.params.grosStop = true
        })
      } else {
        this.etat = 'enonce'
        scratch.lesdivs.BoutonsSuite.appendChild(this.buttonsElts.suite)
        this.buttonsElts.suite.addEventListener('click', () => {
          scratch.params.grosStop = true
        })
      }
      // Obligatoire
      this.finNavigation()

      break // case "navigation":
  }
}
