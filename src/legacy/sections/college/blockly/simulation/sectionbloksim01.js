import loadJqueryDialog from 'src/lib/core/loadJqueryDialog'
import { j3pAjouteBouton, j3pClone, j3pGetRandomInt, j3pShuffle } from 'src/legacy/core/functions'
import { j3pAffiche } from 'src/lib/mathquill/functions'
import { getNewScratch } from 'src/legacy/outils/scratch/loaders'
import { addDefaultTable } from 'src/legacy/themes/table'
import Dialog from 'src/lib/widgets/dialog/Dialog'

export const params = {
  outils: ['mathquill'],
  /**
   * Ce qui est paramétrable dans l’éditeur de graphe
   * @type {LegacySectionParamsParametres}
   */
  parametres: [
    ['nbrepetitions', 4, 'entier', 'Nombre de répétitions de la section'],
    ['nbchances', 3, 'entier', 'Nombre d’essais possibles (illimité si 0)'],
    ['Difficulte', 1, 'entier', 'Difficulté <b>1</b> ou <b></b>2</b>.'],
    ['PasAPas', false, 'boolean', "<u>true</u>: L’élève peut lancer son programme en mode 'Pas à pas'."],
    ['Sequence', false, 'boolean', '<u>true</u>: Chaque bloc a une durée d’exécution d’environ 1 seconde.'],
    ['tolerance', 0, 'entier', 'Nombre de blocs supplementaires acceptés'],
    ['CacheTest', false, 'boolean', '<u>true</u>: L’élève ne voit pas l’action de son programme lors de la correction.']
  ]
}

const sitPos = [
  {
    titre: 'Pile ou face',
    evenements: [
      {
        nom: 'Pile',
        freq: { num: 1, den: 2 }
      },
      {
        nom: 'Face',
        freq: { num: 1, den: 2 }
      }
    ],
    descrip: 'Je jette une pièce de 1€ et <br>je regarde la face vers le haut.',
    hazFix: 2,
    nbBloc: 8,
    diff: 1
  },
  {
    titre: 'Dé à £n faces',
    evenementsG: {
      nom: '£i',
      freq: { num: 1, den: '£n' }
    },
    generateurN: { min: 3, max: 12 },
    descrip: 'On jette un dé à £n faces.',
    hazN: true,
    nbBloc: 6,
    diff: 1
  },
  {
    titre: 'Urne contenant £n boules rouges et £p boules bleues',
    evenements: [
      {
        nom: 'boule rouge',
        freq: { num: '£n', den: '£n + £p' }
      }, {
        nom: 'boule bleue',
        freq: { num: '£p', den: '£n + £p' }
      }
    ],
    generateurN: { min: 3, max: 12 },
    generateurP: { min: 3, max: 12 },
    descrip: 'Une personne tire au hasard une boule d’une urne<br> contenant £n boules rouges et £p boules bleues<br> indiscernables au toucher.',
    hazP1: true,
    nbBloc: 8,
    diff: 1
  },
  {
    titre: 'Urne contenant £n boules rouges, £p boules bleues et £q boules vertes',
    evenements: [
      {
        nom: 'boule rouge',
        freq: { num: '£n', den: '£n + £p + £q' }
      }, {
        nom: 'boule bleue',
        freq: { num: '£p', den: '£n + £p + £q' }
      }, {
        nom: 'boule verte',
        freq: { num: '£q', den: '£n + £p + £q' }
      }
    ],
    generateurN: { min: 3, max: 10 },
    generateurP: { min: 3, max: 10 },
    generateurQ: { min: 3, max: 10 },
    descrip: 'Une personne tire au hasard une boule d’une urne <br>contenant £n boules rouges, £p boules bleues et<br> £q boules vertes indiscernables au toucher.',
    hazP2: true,
    nbBloc: 12,
    diff: 2
  }
]

/**
 * section blok10
 * @this {Parcours} {
 */
export default function main () {
  const me = this
  const stor = me.storage
  const ds = me.donneesSection
  let scratch = stor.scratch

  async function initSection () {
    // faut s’assurer que jQuery-ui est chargé
    await loadJqueryDialog()
    ds.theme = 'zonesAvecImageDeFond'
    scratch = await getNewScratch()
    stor.scratch = scratch
    stor.pbfait = []
    me.validOnEnter = false // ex donneesSection.touche_entree
    scratch.initParams(me)
    scratch.params.verifTousLesCas = true
    scratch.testFlag = function () {
    }
    scratch.testFlagFin = function () {
      if (!this.params.scratchProgDonnees.AleaResult) {
        console.error('pas de AleaResult')
        return { ok: true, mess: '' }
      }
      if (!scratch.params.scratchProgDonnees.valeursaleatoiressorties) {
        return { ok: true, mess: '' }
      }
      if (scratch.params.scratchProgDonnees.valeursaleatoiressorties.length < 1) {
        return { ok: true, mess: '' }
      }
      let sai = scratch.said()
      const val = scratch.params.scratchProgDonnees.valeursaleatoiressorties[0].valeur
      if (sai.length === 0) return { ok: false, mess: 'Quand le nombre aléatoire vaut ' + val + ',<br> le programme ne donne pas de résultat !' }
      if (sai.length > 1) return { ok: false, mess: 'Quand le nombre aléatoire vaut ' + val + ',<br> le programme donne plusieurs résultats !' }
      sai = sai[0]

      this.params.scratchProgDonnees.AleaResult.rez.push(sai)

      if (this.params.scratchProgDonnees.AleaResult.rez.length < this.params.scratchProgDonnees.AleaResult.att) return { ok: true, mess: '' }

      const err = []
      const tabRep = {}
      const listRep = []
      const tabRez = j3pClone(this.params.scratchProgDonnees.AleaResult.rez)

      // verif y’a exactement 1 rep pour chaque i
      for (let i = 0; i < tabRez.length; i++) {
        if (listRep.indexOf(tabRez[i]) === -1) {
          listRep.push(tabRez[i])
          tabRep[tabRez[i]] = 1
        } else {
          tabRep[tabRez[i]]++
        }
      }
      // verif y’a toutes les rep
      for (let i = 0; i < stor.evAt.length; i++) {
        if (listRep.indexOf(stor.evAt[i]) === -1) {
          err.push({ err: 'Le programme ne donne jamais le résultat <i>' + stor.evAt[i] + '</i> !' })
        }
      }
      // verif y’en a pas en +
      for (let i = 0; i < listRep.length; i++) {
        if (stor.evAt.indexOf(listRep[i]) === -1) {
          err.push({ err: 'Le programme donne un résultat innattendu: <i>' + listRep[i] + '</i> !' })
        }
      }
      if (err.length > 0) return { ok: false, mess: err[0].err }
      // verif les freq sont correctes
      for (let i = 0; i < listRep.length; i++) {
        if (!fracEgale({ num: tabRep[listRep[i]], den: tabRez.length }, stor.freqAt[listRep[i]])) {
          if (tabRep[listRep[i]] / tabRez.length > stor.freqAt[listRep[i]].num / stor.freqAt[listRep[i]].den) {
            return { ok: false, mess: 'La probabilité $\\frac{' + tabRep[listRep[i]] + '}{' + tabRez.length + '}$ du résultat <b><i>' + listRep[i] + '</i></b> est trop grande !' }
          } else {
            return { ok: false, mess: 'La probabilité $\\frac{' + tabRep[listRep[i]] + '}{' + tabRez.length + '}$ du résultat <b><i>' + listRep[i] + '</i></b> est trop petite !' }
          }
        }
      }
      return { ok: true, mess: 'Objectif atteint' }
    }
    scratch.params.AutoriseSupprimeVariable = false
    scratch.params.AutoriseRenomeVariable = false
    scratch.params.verifDeb = true

    stor.exoPos = []
    const sitPos2 = j3pClone(sitPos)
    if (ds.Difficulte === 1) sitPos2.pop()
    let sitPosbuf = []
    for (let i = 0; i < ds.nbitems; i++) {
      if (sitPosbuf.length === 0) sitPosbuf = j3pShuffle(sitPos2)
      stor.exoPos.push(sitPosbuf.pop())
    }
    me.construitStructurePage({ structure: 'presentation3', ratioGauche: 0.6 })
    me.stockage = [0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0]
    me.score = 0
    stor.dejafait = []
    me.afficheTitre('Injecter un nombre aléatoire dans une variable')
    enonceMain()
  }
  function modif (tab) {
    for (let i = 0; i < tab.length; i++) {
      for (let j = 0; j < tab[i].length; j++) {
        tab[i][j].style.padding = '0px'
      }
    }
  }
  function affDial () {
    if (!stor.dialog) return console.error(Error('Pas de dialog disponible pour l’afficher'))
    stor.dialog.toggle()
  }
  function fracEgale (a, b) {
    return a.num * b.den === a.den * b.num
  }
  function enonceMain () {
    me.videLesZones()
    scratch.params.scratchProgDonnees.Lutins[0].cox = 10
    scratch.params.scratchProgDonnees.lesordres = []
    scratch.params.scratchProgDonnees.progdeb = []
    scratch.params.scratchProgDonnees.menu = []

    scratch.params.scratchProgDonnees.menu.push({ type: 'catégorie', nom: 'CONDITIONS', coul1: '#40BF4A', coul2: '#389438', contenu: [] })
    scratch.params.scratchProgDonnees.menu[0].contenu.push({ type: 'operator_equals', value: [{ nom: 'OPERAND1', contenu: { valeur: ' ' } }, { nom: 'OPERAND2', contenu: { valeur: ' ' } }] })
    scratch.params.scratchProgDonnees.menu[0].contenu.push({ type: 'operator_gt', value: [{ nom: 'OPERAND1', contenu: { valeur: ' ' } }, { nom: 'OPERAND2', contenu: { valeur: ' ' } }] })
    scratch.params.scratchProgDonnees.menu[0].contenu.push({ type: 'operator_lt', value: [{ nom: 'OPERAND1', contenu: { valeur: ' ' } }, { nom: 'OPERAND2', contenu: { valeur: ' ' } }] })
    scratch.params.scratchProgDonnees.menu.push({ type: 'catégorie', nom: 'VARIABLES', coul1: '#FF8C1A', coul2: '#DB6E00', contenu: [] })
    scratch.params.scratchProgDonnees.menu[1].contenu.push({ type: 'data_variable', variable: [{ nom: 'VARIABLE', nomvar: 'x', id: 'aaaaaaa' }] })
    scratch.params.scratchProgDonnees.menu.push({ type: 'catégorie', nom: 'CONTROLES', coul1: '#FFAB19', coul2: '#FFAB19', contenu: [] })
    scratch.params.scratchProgDonnees.menu[2].contenu.push({ type: 'control_if' })
    scratch.params.scratchProgDonnees.menu[2].contenu.push({ type: 'control_if_else' })
    scratch.params.scratchProgDonnees.menu.push({ type: 'catégorie', nom: 'DIRE', coul1: '#9966FF', coul2: '#774DCB', contenu: [] })

    stor.exo = stor.exoPos.pop()
    stor.n = (stor.exo.generateurN) ? j3pGetRandomInt(stor.exo.generateurN.min, stor.exo.generateurN.max) : 0
    stor.p = (stor.exo.generateurP) ? j3pGetRandomInt(stor.exo.generateurP.min, stor.exo.generateurP.max) : 0
    stor.q = (stor.exo.generateurQ) ? j3pGetRandomInt(stor.exo.generateurQ.min, stor.exo.generateurQ.max) : 0
    if (stor.dialog) {
      stor.dialog.destroy()
      stor.dialog = null
    }

    if (stor.exo.hazFix) stor.maxAff = stor.exo.hazFix
    if (stor.exo.hazN) stor.maxAff = stor.n
    if (stor.exo.hazP1) stor.maxAff = stor.n + stor.p
    if (stor.exo.hazP2) stor.maxAff = stor.n + stor.p + stor.q
    scratch.params.scratchProgDonnees.lesordres.push({ type: 'data_setvariableto', editable: 'false', deletable: 'false', movable: 'false', variable: [{ nom: 'VARIABLE', nomvar: 'x' }], value: [{ nom: 'VALUE', editable: 'false', deletable: 'false', movable: 'false', contenu: { type: 'operator_random', value: [{ nom: 'NUM1', contenu: { valeur: '1' } }, { nom: 'NUM2', contenu: { valeur: String(stor.maxAff) } }] } }] })

    scratch.params.scratchProgDonnees.progdeb.push({ type: 'Drapo', editable: 'false', deletable: 'false', movable: 'false', x: '20', y: '20' })
    scratch.params.scratchProgDonnees.progdeb.push({ type: 'data_setvariableto', editable: 'false', deletable: 'false', movable: 'false', variable: [{ nom: 'VARIABLE', nomvar: 'x' }], value: [{ nom: 'VALUE', editable: 'false', deletable: 'false', movable: 'false', contenu: { type: 'operator_random', value: [{ nom: 'NUM1', contenu: { valeur: '1' } }, { nom: 'NUM2', contenu: { valeur: '10' } }] } }] })

    stor.dialog = new Dialog({
      title: 'Description de l’expérience',
      showText: true,
      width: '430px',
      height: 500,
      position: { my: 'right bottom', at: 'right bottom', of: window }
    })
    const tabDiag = addDefaultTable(stor.dialog.container, 2, 1)
    j3pAffiche(tabDiag[0][0], null, stor.exo.descrip + '<br><br><u><b>Résultats possibles:</b></u><br>', { n: stor.n, p: stor.p, q: stor.q })
    stor.evAt = []
    stor.freqAt = {}
    if (stor.exo.evenements) {
      const tabDiagRes = addDefaultTable(tabDiag[1][0], stor.exo.evenements.length, 1)
      for (let i = 0; i < stor.exo.evenements.length; i++) {
        stor.evAt.push(stor.exo.evenements[i].nom.replace('£i', String(i + 1)))
        j3pAffiche(tabDiagRes[i][0], null, '&nbsp;-&nbsp;' + stor.exo.evenements[i].nom, { i: i + 1 })
        scratch.params.scratchProgDonnees.menu[3].contenu.push({ type: 'dire', value: [{ nom: 'DELTA', contenu: { valeur: stor.exo.evenements[i].nom, editable: 'false', movable: 'false', deletable: 'false' } }, { nom: 'TEMPS', contenu: { valeur: 2, editable: 'false', deletable: 'false', movable: 'false' } }] })
      }
      if (stor.exo.evenements.length === 2) {
        const lacond = { nom: 'CONDITION', contenu: { type: 'operator_lt', value: [{ nom: 'OPERAND1', contenu: { type: 'data_variable', variable: [{ nom: 'VARIABLE', nomvar: 'x', id: 'aaaaaaa' }] } }, { nom: 'OPERAND2', contenu: {} }] } }
        if (stor.exo.titre === 'Pile ou face') {
          lacond.contenu.type = 'operator_equals'
          lacond.contenu.value[1].contenu = { valeur: 1 }
          stor.freqAt.Pile = { num: 1, den: 2 }
          stor.freqAt.Face = { num: 1, den: 2 }
        } else {
          stor.freqAt['boule rouge'] = { num: stor.n, den: stor.n + stor.p }
          stor.freqAt['boule bleue'] = { num: stor.p, den: stor.n + stor.p }
          lacond.contenu.value[1].contenu = { valeur: stor.n + 1 }
        }
        scratch.params.scratchProgDonnees.lesordres.push({ type: 'control_if_else', value: [lacond], statement: [{ nom: 'SUBSTACK', contenu: [{ type: 'dire', value: [{ nom: 'DELTA', contenu: { valeur: stor.exo.evenements[0].nom } }, { nom: 'TEMPS', contenu: { valeur: 2 } }] }] }, { nom: 'SUBSTACK2', contenu: [{ type: 'dire', value: [{ nom: 'DELTA', contenu: { valeur: stor.exo.evenements[1].nom } }, { nom: 'TEMPS', contenu: { valeur: 2 } }] }] }] })
      } else {
        stor.freqAt['boule rouge'] = { num: stor.n, den: stor.n + stor.p + stor.q }
        stor.freqAt['boule bleue'] = { num: stor.p, den: stor.n + stor.p + stor.q }
        stor.freqAt['boule verte'] = { num: stor.q, den: stor.n + stor.p + stor.q }
        scratch.params.scratchProgDonnees.lesordres.push({ type: 'control_if_else', value: [{ nom: 'CONDITION', contenu: { type: 'operator_lt', value: [{ nom: 'OPERAND1', contenu: { type: 'data_variable', variable: [{ nom: 'VARIABLE', nomvar: 'x', id: 'aaaaaaa' }] } }, { nom: 'OPERAND2', contenu: { valeur: stor.n + 1 } }] } }], statement: [{ nom: 'SUBSTACK', contenu: [{ type: 'dire', value: [{ nom: 'DELTA', contenu: { valeur: stor.exo.evenements[0].nom } }, { nom: 'TEMPS', contenu: { valeur: 2 } }] }] }, { nom: 'SUBSTACK2', contenu: [{ type: 'control_if_else', value: [{ nom: 'CONDITION', contenu: { type: 'operator_lt', value: [{ nom: 'OPERAND1', contenu: { type: 'data_variable', variable: [{ nom: 'VARIABLE', nomvar: 'x', id: 'aaaaaaa' }] } }, { nom: 'OPERAND2', contenu: { valeur: stor.n + stor.p + 1 } }] } }], statement: [{ nom: 'SUBSTACK', contenu: [{ type: 'dire', value: [{ nom: 'DELTA', contenu: { valeur: stor.exo.evenements[1].nom } }, { nom: 'TEMPS', contenu: { valeur: 2 } }] }] }, { nom: 'SUBSTACK2', contenu: [{ type: 'dire', value: [{ nom: 'DELTA', contenu: { valeur: stor.exo.evenements[2].nom } }, { nom: 'TEMPS', contenu: { valeur: 2 } }] }] }] }] }] })
      }
    } else {
      const tabDiagRes = addDefaultTable(tabDiag[1][0], stor.n, 1)
      for (let i = 0; i < stor.n; i++) {
        stor.freqAt[String(i + 1)] = { num: 1, den: stor.n }
        stor.evAt.push(stor.exo.evenementsG.nom.replace('£i', String(i + 1)))
        j3pAffiche(tabDiagRes[i][0], null, '&nbsp;-&nbsp;' + stor.exo.evenementsG.nom, { i: i + 1 })
      }
      scratch.params.scratchProgDonnees.menu[3].contenu.push({ type: 'dire', value: [{ nom: 'DELTA', contenu: { valeur: ' ' } }, { nom: 'TEMPS', contenu: { valeur: 2, editable: 'false', deletable: 'false', movable: 'false' } }] })
      scratch.params.scratchProgDonnees.lesordres.push({ type: 'dire', value: [{ nom: 'DELTA', contenu: { type: 'data_variable', variable: [{ nom: 'VARIABLE', nomvar: 'x', id: 'aaaaaaa' }] } }, { nom: 'TEMPS', contenu: { valeur: 2, editable: 'false', deletable: 'false', movable: 'false' } }] })
    }

    // préparer la correction et verif si faut mettre des sorties spéciales
    // genre boule rouge ou pile

    scratch.enonce([], {
      pourInject: { StartScale: 0.6 },
      modeCorrection: {
        type: 'SansComparaison',
        aVerif: [
          { quoi: 'NombreMaxBloc', nombre: stor.exo.nbBloc + ds.tolerance },
          { quoi: 'QuandAvertir', quand: 'apres' },
          { quoi: 'AnnuleSiAvert', quand: false }]
      },
      dirigeProg: { sequence: ds.Sequence, pasApas: ds.PasAPas, cacheTest: ds.CacheTest }
    })
    const yy = addDefaultTable(scratch.lesdivs.enonceG, 3, 1)
    modif(yy)

    j3pAffiche(yy[0][0], null, '<b>Complète ce programme pour qu’il simule l’expérience aléatoire</b>')
    j3pAffiche(yy[1][0], null, '&nbsp;&nbsp;&nbsp;&nbsp;<i>"' + stor.exo.titre + '"</i>.', { n: stor.n, p: stor.p, q: stor.q })
    j3pAffiche(yy[2][0], null, '&nbsp;&nbsp;&nbsp;&nbsp;')
    j3pAjouteBouton(yy[2][0], affSit, { value: 'Description de cette expérience' })
    scratch.params.scratchProgDonnees.fautChekAllAlea = true

    me.finEnonce()
    // on ne veut pas du bouton ok car c’est le clic sur le drapeau qui valide (faut donc mettre ça après finEnonce)
    me.cacheBoutonValider()
  }

  function affSit () {
    affDial()
  }

  switch (this.etat) {
    case 'enonce':
      // code exécuté au lancement de la section
      if (this.debutDeLaSection) {
        initSection()
      } else {
        enonceMain()
      }
      break // case "enonce":

    case 'correction':
      try {
        const ret = scratch.prepCorrec()
        const xmlDoc2 = ret.xmlDoc2
        const xmlText = ret.xmlText

        if (scratch.params.scratchProgDonnees.repEleve) {
          // Bonne réponse
          this.score++
          scratch.corrigeGood(xmlDoc2, xmlText)
        } else {
          // Pas de bonne réponse
          scratch.corrige(xmlDoc2, xmlText)
        }
        // on veut être rappelé dans l’état navigation
        this.finCorrection('navigation', true)
      } catch (error) {
        console.error(error)
      }
      break // case "correction":

    case 'navigation':
      if (this.sectionTerminee()) {
        scratch.lesdivs.BoutonsSuite.appendChild(this.buttonsElts.sectionSuivante)
        this.buttonsElts.sectionSuivante.addEventListener('click', () => {
          scratch.params.grosStop = true
        })
      } else {
        this.etat = 'enonce'
        scratch.lesdivs.BoutonsSuite.appendChild(this.buttonsElts.suite)
        this.buttonsElts.suite.addEventListener('click', () => {
          scratch.params.grosStop = true
        })
      }
      // Obligatoire
      this.finNavigation()
      break // case "navigation":
  }
}
