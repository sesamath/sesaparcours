import { j3pGetRandomBool, j3pGetRandomInt } from 'src/legacy/core/functions'
import { getNewScratch } from 'src/legacy/outils/scratch/loaders'

export const params = {
  outils: ['mathquill'],
  /**
   * Ce qui est paramétrable dans l’éditeur de graphe
   * @type {LegacySectionParamsParametres}
   */
  parametres: [
    ['nbrepetitions', 4, 'entier', 'Nombre de répétitions de la section'],
    ['nbchances', 0, 'entier', 'Nombre d’essais possibles (illimité si 0)'],
    ['PasAPas', true, 'boolean', "<u>true</u>: L’élève peut lancer son programme en mode 'Pas à pas'."],
    ['Sequence', true, 'boolean', '<u>true</u>: Chaque bloc a une durée d’exécution d’environ 1 seconde.'],
    ['angle', 'Lutin', 'liste', '<u>Lutin</u>: Les angles indiqués sont les angles à programmer. <BR> <u>Rendu</u>: Les angles donnés sont les angles géométriques de la figure (complémentaires). <BR> <u>Les deux</u>: Les deux cas sont possibles.', ['Lutin', 'Rendu', 'Les deux']],
    ['difficultemin', 1, 'entier', 'Difficulté du programme (minimum 1)'],
    ['difficultemax', 3, 'entier', 'Difficulté du programme (maximum 3)'],
    ['difficulte', 'Progressive', 'liste', 'Gestion de la progression (entre min et max)', ['Aléatoire', 'Progressive']],
    ['tolerance', 0, 'entier', 'Nombre de blocs supplementaires acceptés'],
    ['CacheTest', false, 'boolean', '<u>true</u>: L’élève ne voit pas l’action de son programme lors d la correction.']
  ]

}

/**
 * section blokligne
 * @this {Parcours}
 */
export default function blokligne () {
  const me = this
  const stor = me.storage
  const ds = me.donneesSection
  let scratch = stor.scratch

  function psuiv (x0, y0, l, a, na) {
    const ret = {}
    ret.a = a + na
    if (ret.a > 0) { ret.a += 360 }
    if (ret.a >= 360) { ret.a -= 360 }
    ret.x = x0 + l * Math.cos((ret.a) * Math.PI / 180)
    ret.y = y0 + l * Math.sin((ret.a) * Math.PI / 180)
    return ret
  }
  async function initSection () {
    ds.theme = 'zonesAvecImageDeFond'
    scratch = await getNewScratch()
    stor.scratch = scratch
    me.validOnEnter = false // ex donneesSection.touche_entree

    scratch.initParams(me, true)
    scratch.testFlag = function () {}
    ds.nbitems = ds.nbetapes * ds.nbrepetitions

    me.construitStructurePage({ structure: 'presentation3' }) // ratioGauche ne sert à rien avec presentation3
    me.stockage = [0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0]

    me.score = 0
    ds.difficultemin = Math.max(1, ds.difficultemin)
    ds.difficultemax = Math.min(3, ds.difficultemax)
    if (ds.difficultemax < ds.difficultemin) {
      ds.difficultemin = ds.difficultemax
    }
    me.afficheTitre('Programmer un tracé')

    scratch.params.scratchProgDonnees.progdeb = []
    scratch.params.scratchProgDonnees.progdeb.push({ type: 'Drapo', editable: 'false', deletable: 'false', movable: 'false', x: '20', y: '20' })

    scratch.params.scratchProgDonnees.menu = []
    scratch.params.scratchProgDonnees.menu.push({ type: 'catégorie', nom: 'STYLO', coul1: '#40BF4A', coul2: '#389438', contenu: [] })
    scratch.params.scratchProgDonnees.menu[0].contenu.push({ type: 'poserstylo' })
    scratch.params.scratchProgDonnees.menu[0].contenu.push({ type: 'levestylo' })
    scratch.params.scratchProgDonnees.menu.push({ type: 'catégorie', nom: 'DEPLACEMENTS', coul1: '#4C97FF', coul2: '#3373CC', contenu: [] })
    scratch.params.scratchProgDonnees.menu[1].contenu.push({ type: 'motion_movesteps', value: [{ nom: 'STEPS', contenu: { valeur: 50 } }] })
    scratch.params.scratchProgDonnees.menu[1].contenu.push({ type: 'motion_turnright', value: [{ nom: 'DEGREES', contenu: { valeur: 90 } }] })
    scratch.params.scratchProgDonnees.menu[1].contenu.push({ type: 'motion_turnleft', value: [{ nom: 'DEGREES', contenu: { valeur: 90 } }] })

    enonceMain()
  }

  function enonceMain () {
    me.videLesZones()

    stor.diff = j3pGetRandomInt(ds.difficultemin, ds.difficultemax)
    if (ds.difficulte === 'Progressive') {
      stor.diff = ds.difficultemin + Math.round((me.questionCourante - 1) * (ds.difficultemax - ds.difficultemin) / (ds.nbrepetitions - 1))
    }

    let nbmaxblock
    scratch.params.scratchProgDonnees.lesordres = []
    stor.angle = j3pGetRandomBool()
    if (ds.angle === 'Lutin') { stor.angle = false }
    if (ds.angle === 'Rendu') { stor.angle = true }

    let npoint
    let nang
    let tut
    switch (stor.diff) {
      case 1: {
        const long = j3pGetRandomInt(100, 200)
        scratch.params.figureAfaire = [{ type: 'pointdepart', x1: 50, y1: 100 },
          { type: 'line', x1: 50, y1: 100, x2: 50 + long, y2: 100, affichelong: true }]
        scratch.params.scratchProgDonnees.lesordres.push({ type: 'poserstylo' })
        scratch.params.scratchProgDonnees.lesordres.push({ type: 'motion_movesteps', value: [{ nom: 'STEPS', contenu: { valeur: long } }] })
        nbmaxblock = 3
        break
      }
      case 2: {
        const long1 = j3pGetRandomInt(60, 100)
        const ang = j3pGetRandomInt(50, 130)

        const long2 = j3pGetRandomInt(50, 90)
        const sens = j3pGetRandomInt(0, 1)

        const angrad = ang * Math.PI / 180
        let fturn, yd
        if (sens === 0) {
          fturn = 'turnleft'
          yd = 120
          nang = ang - 180
        } else {
          fturn = 'turnright'
          yd = 50
          nang = 180 - ang
        }
        if (ang > 90) {
          tut = -1
        } else {
          if (sens === 1) {
            tut = -1
          } else {
            tut = -1
          }
        }
        scratch.params.figureAfaire = [
          { type: 'pointdepart', x1: 50, y1: yd },
          { type: 'line', x1: 50, y1: yd, x2: 50 + long1, y2: yd, affichelong: true }
        ]
        npoint = psuiv(50 + long1, yd, long2, nang, 0)
        scratch.params.figureAfaire.push({
          type: 'line',
          x1: 50 + long1,
          y1: yd,
          x2: npoint.x,
          y2: npoint.y,
          affichelong: true
        })
        if (stor.angle) {
          scratch.params.figureAfaire.push({
            type: 'angle',
            x1: 50,
            y1: yd,
            x2: 50 + long1,
            y2: yd,
            x3: (50 + long1) + tut * Math.cos(angrad) * long2,
            y3: yd - Math.pow(-1, sens) * Math.sin(angrad) * long2
          })
        } else {
          scratch.params.figureAfaire.push({
            type: 'angledirect',
            x1: 50,
            y1: yd,
            x2: 50 + long1,
            y2: yd,
            x3: (50 + long1) + tut * Math.cos(angrad) * long2,
            y3: yd - Math.pow(-1, sens) * Math.sin(angrad) * long2
          })
        }

        scratch.params.scratchProgDonnees.lesordres.push({ type: 'poserstylo' })
        scratch.params.scratchProgDonnees.lesordres.push({ type: 'motion_movesteps', value: [{ nom: 'STEPS', contenu: { valeur: long1 } }] })
        scratch.params.scratchProgDonnees.lesordres.push({
          type: 'motion_' + fturn,
          value: [{ nom: 'DEGREES', contenu: { valeur: 180 - ang } }]
        })
        scratch.params.scratchProgDonnees.lesordres.push({ type: 'motion_movesteps', value: [{ nom: 'STEPS', contenu: { valeur: long2 } }] })
        break
      }
      case 3: {
        const long1 = j3pGetRandomInt(60, 100)
        const ang = j3pGetRandomInt(50, 130)

        const long2 = j3pGetRandomInt(80, 100)
        const sens = j3pGetRandomInt(0, 1)

        const long3 = j3pGetRandomInt(50, 90)

        let fturn = 'turnleft'
        let fturn2 = 'turnright'
        let yd = 120
        let nang = ang - 180
        const angrad = ang * Math.PI / 180
        if (sens !== 0) {
          fturn = 'turnright'
          fturn2 = 'turnleft'
          yd = 30
          nang = 180 - ang
        }

        if (ang > 90) {
          tut = -1
        } else {
          if (sens === 1) {
            tut = -1
          } else {
            tut = -1
          }
        }
        scratch.params.figureAfaire = [{ type: 'pointdepart', x1: 50, y1: yd },
          { type: 'line', x1: 50, y1: yd, x2: 50 + long1, y2: yd, affichelong: true }]

        npoint = psuiv(50 + long1, yd, long2, nang, 0)
        scratch.params.figureAfaire.push({
          type: 'line',
          x1: 50 + long1,
          y1: yd,
          x2: npoint.x,
          y2: npoint.y,
          affichelong: true
        })

        scratch.params.figureAfaire.push({
          type: 'line',
          x1: npoint.x,
          y1: npoint.y,
          x2: npoint.x + long3,
          y2: npoint.y,
          affichelong: true
        })

        if (stor.angle) {
          scratch.params.figureAfaire.push({
            type: 'angle',
            x1: 50,
            y1: yd,
            x2: 50 + long1,
            y2: yd,
            x3: (50 + long1) + tut * Math.cos(angrad) * long2,
            y3: yd - Math.pow(-1, sens) * Math.sin(angrad) * long2
          })
          scratch.params.figureAfaire.push({
            type: 'angle',
            x1: 50 + long1,
            y1: yd,
            x2: (50 + long1) + tut * Math.cos(angrad) * long2,
            y2: yd - Math.pow(-1, sens) * Math.sin(angrad) * long2,
            x3: (50 + long1) + tut * Math.cos(angrad) * long2 + long3,
            y3: yd - Math.pow(-1, sens) * Math.sin(angrad) * long2
          })
        } else {
          scratch.params.figureAfaire.push({
            type: 'angledirect',
            x1: 50,
            y1: yd,
            x2: 50 + long1,
            y2: yd,
            x3: (50 + long1) + tut * Math.cos(angrad) * long2,
            y3: yd - Math.pow(-1, sens) * Math.sin(angrad) * long2
          })
          scratch.params.figureAfaire.push({
            type: 'angledirect',
            x1: 50 + long1,
            y1: yd,
            x2: (50 + long1) + tut * Math.cos(angrad) * long2,
            y2: yd - Math.pow(-1, sens) * Math.sin(angrad) * long2,
            x3: (50 + long1) + tut * Math.cos(angrad) * long2 + long3,
            y3: yd - Math.pow(-1, sens) * Math.sin(angrad) * long2
          })
        }

        scratch.params.scratchProgDonnees.lesordres.push({ type: 'poserstylo' })
        scratch.params.scratchProgDonnees.lesordres.push({ type: 'motion_movesteps', value: [{ nom: 'STEPS', contenu: { valeur: long1 } }] })
        scratch.params.scratchProgDonnees.lesordres.push({
          type: 'motion_' + fturn,
          value: [{ nom: 'DEGREES', contenu: { valeur: 180 - ang } }]
        })
        scratch.params.scratchProgDonnees.lesordres.push({ type: 'motion_movesteps', value: [{ nom: 'STEPS', contenu: { valeur: long2 } }] })
        scratch.params.scratchProgDonnees.lesordres.push({
          type: 'motion_' + fturn2,
          value: [{ nom: 'DEGREES', contenu: { valeur: 180 - ang } }]
        })
        scratch.params.scratchProgDonnees.lesordres.push({ type: 'motion_movesteps', value: [{ nom: 'STEPS', contenu: { valeur: long3 } }] })

        break
      }
      default: console.error('difficulté hors limite')
    }

    scratch.enonceDessin(null, {
      pourInject: { StartScale: 0.75 },
      modeCorrection: {
        type: 'SansComparaison',
        aVerif: [{ quoi: 'NombreMaxBloc', nombre: nbmaxblock + ds.tolerance },
          { quoi: 'QuandAvertir', quand: 'apres' },
          { quoi: 'AnnuleSiAvert', quand: false }]
      },
      dirigeProg: { sequence: ds.Sequence, pasApas: ds.PasAPas, cacheTest: ds.CacheTest }
    })

    me.finEnonce()
    me.cacheBoutonValider()
  }

  switch (this.etat) {
    case 'enonce':
      // code exécuté au lancement de la section
      if (this.debutDeLaSection) {
        initSection()
      } else {
        enonceMain()
      }
      break // case "enonce":

    case 'correction':
      try {
        const ret = scratch.prepCorrec()
        const xmlDoc2 = ret.xmlDoc2
        const xmlText = ret.xmlText

        if (scratch.params.scratchProgDonnees.repEleve) {
          // Bonne réponse
          this.score++
          scratch.corrigeGood(xmlDoc2, xmlText)
        } else {
          // Pas de bonne réponse
          scratch.corrige(xmlDoc2, xmlText)
        }
        // on veut être rappelé dans l’état navigation
        this.finCorrection('navigation', true)
      } catch (error) {
        console.error(error)
      }
      break // case "correction":

    case 'navigation':
      if (this.sectionTerminee()) {
        scratch.lesdivs.BoutonsSuite.appendChild(this.buttonsElts.sectionSuivante)
        this.buttonsElts.sectionSuivante.addEventListener('click', () => {
          scratch.params.grosStop = true
        })
      } else {
        this.etat = 'enonce'
        scratch.lesdivs.BoutonsSuite.appendChild(this.buttonsElts.suite)
        this.buttonsElts.suite.addEventListener('click', () => {
          scratch.params.grosStop = true
        })
      }
      // Obligatoire
      this.finNavigation()

      break // case "navigation":
  }
}
