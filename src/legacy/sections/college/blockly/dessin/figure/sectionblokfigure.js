import { j3pGetRandomBool, j3pGetRandomElt, j3pGetRandomInt, j3pShowError } from 'src/legacy/core/functions'
import { getNewScratch } from 'src/legacy/outils/scratch/loaders'

export const params = {
  outils: ['mathquill'],
  /**
   * Ce qui est paramétrable dans l’éditeur de graphe
   * @type {LegacySectionParamsParametres}
   */
  parametres: [
    ['nbrepetitions', 4, 'entier', 'Nombre de répétitions de la section'],
    ['nbchances', 0, 'entier', 'Nombre d’essais possibles (illimité si 0)'],
    ['PasAPas', true, 'boolean', "<u>true</u>: L’élève peut lancer son programme en mode 'Pas à pas'."],
    ['Sequence', true, 'boolean', '<u>true</u>: Chaque bloc a une durée d’exécution d’environ 1 seconde.'],
    ['Carre', false, 'boolean', '<u>true</u>: La figure peut être un carré.'],
    ['TriangleEquilateral', false, 'boolean', '<u>true</u>: La figure peut être un triangle équilatéral.'],
    ['Rectangle', false, 'boolean', '<u>true</u>: La figure peut être un rectangle.'],
    ['Parallelogramme', false, 'boolean', '<u>true</u>: La figure peut être un parallélogramme.'],
    ['Hexagone', false, 'boolean', '<u>true</u>: La figure peut être un hexagone.'],
    ['Boucle', false, 'boolean', '<u>true</u>: L’élève peut (et doit) utiliser une boucle.'],
    ['AnglesAffiches', false, 'boolean', '<u>true</u>: Les angles de la figure sont affichés'],
    ['tolerance', 0, 'entier', 'Nombre de blocs supplementaires acceptés'],
    ['CacheTest', false, 'boolean', '<u>true</u>: L’élève ne voit pas l’action de son programme lors d la correction.']
  ]
}

/**
 * section blokfigure
 * @this {Parcours}
 */
export default function main () {
  const me = this
  const stor = me.storage
  const ds = me.donneesSection

  function psuiv (x0, y0, l, a, na) {
    const ret = {}
    ret.a = a + na
    if (ret.a > 0) ret.a += 360
    if (ret.a >= 360) ret.a -= 360
    ret.x = x0 + l * Math.cos((ret.a) * Math.PI / 180)
    ret.y = y0 + l * Math.sin((ret.a) * Math.PI / 180)
    return ret
  }

  async function initSection () {
    const scratch = await getNewScratch()
    stor.scratch = scratch
    me.validOnEnter = false // ex donneesSection.touche_entree
    scratch.initParams(me, true)
    scratch.testFlag = function () {}

    me.construitStructurePage({ structure: 'presentation3', ratioGauche: 0.6, theme: 'zonesAvecImageDeFond' })
    me.stockage = [0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0]

    stor.dejafait = []
    /**
     * la liste des possibles parmi carre/triangleequi/rectangle/parallelo/hexagone
     * @type {string[]}
     */
    stor.figpossible = []
    if (ds.Carre) stor.figpossible.push('carre')
    if (ds.TriangleEquilateral) stor.figpossible.push('triangleequi')
    if (ds.Rectangle) stor.figpossible.push('rectangle')
    if (ds.Parallelogramme) stor.figpossible.push('parallelo')
    if (ds.Hexagone) stor.figpossible.push('hexagone')
    if (stor.figpossible.length === 0) {
      j3pShowError('Paramétrage: Aucune figure à tracer !')
      stor.figpossible.push('carre')
    }

    me.afficheTitre('Programmer le tracé d’une figure.')

    scratch.params.scratchProgDonnees.progdeb = []
    scratch.params.scratchProgDonnees.progdeb.push({ type: 'Drapo', editable: 'false', deletable: 'false', movable: 'false', x: '20', y: '20' })

    scratch.params.scratchProgDonnees.menu = []
    scratch.params.scratchProgDonnees.menu.push({ type: 'catégorie', nom: 'STYLO', coul1: '#40BF4A', coul2: '#389438', contenu: [] })
    scratch.params.scratchProgDonnees.menu[0].contenu.push({ type: 'poserstylo' })
    scratch.params.scratchProgDonnees.menu[0].contenu.push({ type: 'levestylo' })
    scratch.params.scratchProgDonnees.menu.push({ type: 'catégorie', nom: 'DEPLACEMENTS', coul1: '#4C97FF', coul2: '#3373CC', contenu: [] })
    scratch.params.scratchProgDonnees.menu[1].contenu.push({ type: 'motion_movesteps', value: [{ nom: 'STEPS', contenu: { valeur: 50 } }] })
    scratch.params.scratchProgDonnees.menu[1].contenu.push({ type: 'motion_turnright', value: [{ nom: 'DEGREES', contenu: { valeur: 90 } }] })
    scratch.params.scratchProgDonnees.menu[1].contenu.push({ type: 'motion_turnleft', value: [{ nom: 'DEGREES', contenu: { valeur: 90 } }] })
    if (ds.Boucle) {
      scratch.params.scratchProgDonnees.menu.push({ type: 'catégorie', nom: 'CONTROLES', coul1: '#FFAB19', coul2: '#CF8B17', contenu: [] })
      scratch.params.scratchProgDonnees.menu[2].contenu.push({ type: 'control_repeat', value: [{ nom: 'TIMES', contenu: { valeur: 10 } }] })
    }

    // on peut lancer la 1re question
    enonceMain()
  }

  function enonceMain () {
    me.videLesZones()
    const scratch = stor.scratch
    scratch.params.scratchProgDonnees.lesordres = []
    if (stor.figpossible.length === 1) {
      // pas de hasard, toujours la même
      stor.figencours = stor.figpossible[0]
    } else {
      // on regarde parmi les figures jamais tirées
      const restePossibles = stor.figpossible.filter(fig => !stor.dejafait.includes(fig))
      if (restePossibles.length) {
        stor.figencours = j3pGetRandomElt(restePossibles)
        stor.dejafait.push(stor.figencours)
      } else {
        // on a déjà parcouru tous les possibles
        stor.figencours = j3pGetRandomElt(stor.figpossible)
        stor.dejafait = [stor.figencours]
      }
    }

    // variables utilisées après le switch
    let nomfig, nbmaxbloc

    switch (stor.figencours) {
      case 'carre': {
        const long1 = j3pGetRandomInt(60, 90)
        const sensTrigo = j3pGetRandomBool()
        nomfig = 'ce carré'
        const xdeb = 70
        let fturn, ttt, yd
        if (sensTrigo) {
          fturn = 'turnleft'
          ttt = -1
          yd = 120
        } else {
          fturn = 'turnright'
          yd = 40
          ttt = +1
        }

        // le carré de base
        scratch.params.figureAfaire = [
          { type: 'pointdepart', x1: xdeb, y1: yd },
          { type: 'line', x1: xdeb, y1: yd, x2: xdeb + long1, y2: yd, affichelong: true },
          { type: 'line', x1: xdeb + long1, y1: yd, x2: xdeb + long1, y2: yd + ttt * long1, affichelong: false },
          { type: 'line', x1: xdeb + long1, y1: yd + ttt * long1, x2: xdeb, y2: yd + ttt * long1, affichelong: false },
          { type: 'line', x1: xdeb, y1: yd + ttt * long1, x2: xdeb, y2: yd, affichelong: false }
        ]
        if (ds.AnglesAffiches) {
          // on affiche les angles sur le modèle
          scratch.params.figureAfaire.push({
            type: 'angle',
            x1: xdeb,
            y1: yd,
            x2: xdeb + long1,
            y2: yd,
            x3: xdeb + long1,
            y3: yd + ttt * long1
          })
          scratch.params.figureAfaire.push({
            type: 'angle',
            x1: xdeb + long1,
            y1: yd,
            x2: xdeb + long1,
            y2: yd + ttt * long1,
            x3: xdeb,
            y3: yd + ttt * long1
          })
          scratch.params.figureAfaire.push({
            type: 'angle',
            x1: xdeb + long1,
            y1: yd + ttt * long1,
            x2: xdeb,
            y2: yd + ttt * long1,
            x3: xdeb,
            y3: yd
          })
          scratch.params.figureAfaire.push({
            type: 'angle',
            x1: xdeb,
            y1: yd + ttt * long1,
            x2: xdeb,
            y2: yd,
            x3: xdeb + long1,
            y3: yd
          })
        }

        // ce que l’élève doit faire
        scratch.params.scratchProgDonnees.lesordres.push({ type: 'poserstylo' })
        if (!ds.Boucle) {
          nbmaxbloc = 9
          scratch.params.scratchProgDonnees.lesordres.push({ type: 'motion_movesteps', value: [{ nom: 'STEPS', contenu: { valeur: long1 } }] })
          scratch.params.scratchProgDonnees.lesordres.push({ type: 'motion_' + fturn, value: [{ nom: 'DEGREES', contenu: { valeur: 90 } }] })
          scratch.params.scratchProgDonnees.lesordres.push({ type: 'motion_movesteps', value: [{ nom: 'STEPS', contenu: { valeur: long1 } }] })
          scratch.params.scratchProgDonnees.lesordres.push({ type: 'motion_' + fturn, value: [{ nom: 'DEGREES', contenu: { valeur: 90 } }] })
          scratch.params.scratchProgDonnees.lesordres.push({ type: 'motion_movesteps', value: [{ nom: 'STEPS', contenu: { valeur: long1 } }] })
          scratch.params.scratchProgDonnees.lesordres.push({ type: 'motion_' + fturn, value: [{ nom: 'DEGREES', contenu: { valeur: 90 } }] })
          scratch.params.scratchProgDonnees.lesordres.push({ type: 'motion_movesteps', value: [{ nom: 'STEPS', contenu: { valeur: long1 } }] })
        } else {
          const laboucle = []
          nbmaxbloc = 5
          laboucle.push({ type: 'motion_movesteps', value: [{ nom: 'STEPS', contenu: { valeur: long1 } }] })
          laboucle.push({ type: 'motion_' + fturn, value: [{ nom: 'DEGREES', contenu: { valeur: 90 } }] })
          scratch.params.scratchProgDonnees.lesordres.push({
            type: 'control_repeat',
            value: [{ nom: 'TIMES', contenu: { valeur: 4 } }],
            statement: [{ nom: 'SUBSTACK', contenu: laboucle }]
          })
        }
        break
      }

      case 'triangleequi': {
        const long1 = j3pGetRandomInt(70, 100)
        const sensTrigo = j3pGetRandomBool()
        nomfig = 'ce triangle équilatéral'
        const xdeb = 70
        let fturn, ttt, yd, tt
        if (sensTrigo) {
          fturn = 'turnleft'
          ttt = -120
          yd = 120
          tt = -1
        } else {
          fturn = 'turnright'
          yd = 40
          ttt = +120
          tt = 1
        }
        scratch.params.figureAfaire = [{ type: 'pointdepart', x1: xdeb, y1: yd }]
        const point = { x: xdeb, y: yd, a: 0 }
        let npoint, an
        for (let i = 0; i < 3; i++) {
          an = ttt
          if (i === 0) an = 0
          npoint = psuiv(point.x, point.y, long1, point.a, an)
          const affichelong = (i === 0)
          scratch.params.figureAfaire.push({ type: 'line', x1: point.x, y1: point.y, x2: npoint.x, y2: npoint.y, affichelong })
          point.x = npoint.x
          point.y = npoint.y
          point.a = npoint.a
        }

        if (ds.AnglesAffiches) {
          scratch.params.figureAfaire.push({ type: 'angle', x1: xdeb, y1: yd, x2: xdeb + long1, y2: yd, x3: xdeb + long1 * Math.cos(60 * Math.PI / 180), y3: yd + long1 * Math.sin(60 * Math.PI / 180) * tt })
          scratch.params.figureAfaire.push({ type: 'angle', x2: xdeb, y2: yd, x3: xdeb + long1, y3: yd, x1: xdeb + long1 * Math.cos(60 * Math.PI / 180), y1: yd + long1 * Math.sin(60 * Math.PI / 180) * tt })
          scratch.params.figureAfaire.push({ type: 'angle', x3: xdeb, y3: yd, x1: xdeb + long1, y1: yd, x2: xdeb + long1 * Math.cos(60 * Math.PI / 180), y2: yd + long1 * Math.sin(60 * Math.PI / 180) * tt })
        }

        scratch.params.scratchProgDonnees.lesordres.push({ type: 'poserstylo' })
        if (!ds.Boucle) {
          nbmaxbloc = 7
          scratch.params.scratchProgDonnees.lesordres.push({ type: 'motion_movesteps', value: [{ nom: 'STEPS', contenu: { valeur: long1 } }] })
          scratch.params.scratchProgDonnees.lesordres.push({ type: 'motion_' + fturn, value: [{ nom: 'DEGREES', contenu: { valeur: 120 } }] })
          scratch.params.scratchProgDonnees.lesordres.push({ type: 'motion_movesteps', value: [{ nom: 'STEPS', contenu: { valeur: long1 } }] })
          scratch.params.scratchProgDonnees.lesordres.push({ type: 'motion_' + fturn, value: [{ nom: 'DEGREES', contenu: { valeur: 120 } }] })
          scratch.params.scratchProgDonnees.lesordres.push({ type: 'motion_movesteps', value: [{ nom: 'STEPS', contenu: { valeur: long1 } }] })
        } else {
          const laboucle = []
          nbmaxbloc = 5
          laboucle.push({ type: 'motion_movesteps', value: [{ nom: 'STEPS', contenu: { valeur: long1 } }] })
          laboucle.push({ type: 'motion_' + fturn, value: [{ nom: 'DEGREES', contenu: { valeur: 120 } }] })
          scratch.params.scratchProgDonnees.lesordres.push({ type: 'control_repeat', value: [{ nom: 'TIMES', contenu: { valeur: 3 } }], statement: [{ nom: 'SUBSTACK', contenu: laboucle }] })
        }

        break
      }
      case 'rectangle': {
        let long1 = j3pGetRandomInt(60, 120)
        const long2 = j3pGetRandomInt(60, 90)
        if (long1 === long2) long1 += 3
        const sensTrigo = j3pGetRandomBool()
        nomfig = 'ce rectangle'
        const xdeb = 70
        let fturn, ttt, yd
        if (sensTrigo) {
          fturn = 'turnleft'
          ttt = -1
          yd = 120
        } else {
          fturn = 'turnright'
          yd = 40
          ttt = +1
        }

        scratch.params.figureAfaire = [
          { type: 'pointdepart', x1: xdeb, y1: yd },
          { type: 'line', x1: xdeb, y1: yd, x2: xdeb + long1, y2: yd, affichelong: true },
          { type: 'line', x1: xdeb + long1, y1: yd, x2: xdeb + long1, y2: yd + ttt * long2, affichelong: true },
          { type: 'line', x1: xdeb + long1, y1: yd + ttt * long2, x2: xdeb, y2: yd + ttt * long2, affichelong: false },
          { type: 'line', x1: xdeb, y1: yd + ttt * long2, x2: xdeb, y2: yd, affichelong: false }
        ]

        if (ds.AnglesAffiches) {
          scratch.params.figureAfaire.push({ type: 'angle', x1: xdeb, y1: yd, x2: xdeb + long1, y2: yd, x3: xdeb + long1, y3: yd + ttt * long2 })
          scratch.params.figureAfaire.push({ type: 'angle', x1: xdeb + long1, y1: yd, x2: xdeb + long1, y2: yd + ttt * long2, x3: xdeb, y3: yd + ttt * long2 })
          scratch.params.figureAfaire.push({ type: 'angle', x1: xdeb + long1, y1: yd + ttt * long2, x2: xdeb, y2: yd + ttt * long2, x3: xdeb, y3: yd })
          scratch.params.figureAfaire.push({ type: 'angle', x1: xdeb, y1: yd + ttt * long2, x2: xdeb, y2: yd, x3: xdeb + long1, y3: yd })
        }

        scratch.params.scratchProgDonnees.lesordres.push({ type: 'poserstylo' })
        if (!ds.Boucle) {
          nbmaxbloc = 9
          scratch.params.scratchProgDonnees.lesordres.push({ type: 'motion_movesteps', value: [{ nom: 'STEPS', contenu: { valeur: long1 } }] })
          scratch.params.scratchProgDonnees.lesordres.push({ type: 'motion_' + fturn, value: [{ nom: 'DEGREES', contenu: { valeur: 90 } }] })
          scratch.params.scratchProgDonnees.lesordres.push({ type: 'motion_movesteps', value: [{ nom: 'STEPS', contenu: { valeur: long2 } }] })
          scratch.params.scratchProgDonnees.lesordres.push({ type: 'motion_' + fturn, value: [{ nom: 'DEGREES', contenu: { valeur: 90 } }] })
          scratch.params.scratchProgDonnees.lesordres.push({ type: 'motion_movesteps', value: [{ nom: 'STEPS', contenu: { valeur: long1 } }] })
          scratch.params.scratchProgDonnees.lesordres.push({ type: 'motion_' + fturn, value: [{ nom: 'DEGREES', contenu: { valeur: 90 } }] })
          scratch.params.scratchProgDonnees.lesordres.push({ type: 'motion_movesteps', value: [{ nom: 'STEPS', contenu: { valeur: long2 } }] })
        } else {
          const laboucle = []
          nbmaxbloc = 7
          laboucle.push({ type: 'motion_movesteps', value: [{ nom: 'STEPS', contenu: { valeur: long1 } }] })
          laboucle.push({ type: 'motion_' + fturn, value: [{ nom: 'DEGREES', contenu: { valeur: 90 } }] })
          laboucle.push({ type: 'motion_movesteps', value: [{ nom: 'STEPS', contenu: { valeur: long2 } }] })
          laboucle.push({ type: 'motion_' + fturn, value: [{ nom: 'DEGREES', contenu: { valeur: 90 } }] })
          scratch.params.scratchProgDonnees.lesordres.push({ type: 'control_repeat', value: [{ nom: 'TIMES', contenu: { valeur: 2 } }], statement: [{ nom: 'SUBSTACK', contenu: laboucle }] })
        }

        break
      }
      case 'parallelo': {
        let long1 = j3pGetRandomInt(70, 130)
        const long2 = j3pGetRandomInt(60, 90)
        let ang = j3pGetRandomInt(50, 130)
        if (long1 === long2) long1 += 3
        if ((ang > 85) && (ang < 95)) ang += 10
        const sensTrigo = j3pGetRandomInt(0, 1)
        nomfig = ' ce parallélogramme'
        const xdeb = 70
        let fturn, ttt, yd
        if (sensTrigo) {
          fturn = 'turnleft'
          ttt = -1
          yd = 120
        } else {
          fturn = 'turnright'
          yd = 40
          ttt = +1
        }
        const pt1x = xdeb + long1
        const pt1y = yd
        const pt2x = pt1x + long2 * Math.cos(ang * Math.PI / 180)
        const pt2y = yd + long2 * ttt * Math.sin(ang * Math.PI / 180)
        const pt3x = pt2x - long1
        const pt3y = pt2y

        let ab
        if (sensTrigo) {
          fturn = 'turnleft'
          ttt = -ang
          yd = 120
          ab = -180
        } else {
          fturn = 'turnright'
          yd = 40
          ttt = +ang
          ab = 180
        }
        scratch.params.figureAfaire = [{ type: 'pointdepart', x1: xdeb, y1: yd }]
        const point = { x: xdeb, y: yd, a: 0 }
        let long
        for (let i = 0; i < 4; i++) {
          let an = ttt
          long = long2
          if (i % 2 === 0) {
            an = ab - an
            long = long1
          }
          if (i === 0) an = 0
          const npoint = psuiv(point.x, point.y, long, point.a, an)
          const affichelong = (i < 2)
          scratch.params.figureAfaire.push({
            type: 'line',
            x1: point.x,
            y1: point.y,
            x2: npoint.x,
            y2: npoint.y,
            affichelong
          })
          point.x = npoint.x
          point.y = npoint.y
          point.a = npoint.a
        }

        scratch.params.figureAfaire.push({ type: 'angle', x1: xdeb, y1: yd, x2: pt1x, y2: pt1y, x3: pt2x, y3: pt2y })
        scratch.params.figureAfaire.push({
          type: 'angle',
          x1: pt1x,
          y1: pt1y,
          x2: pt2x,
          y2: pt2y,
          x3: pt3x,
          y3: pt3y
        })

        if (ds.AnglesAffiches) {
          scratch.params.figureAfaire.push({
            type: 'angle',
            x1: pt2x,
            y1: pt2y,
            x2: pt3x,
            y2: pt3y,
            x3: xdeb,
            y3: yd
          })
          scratch.params.figureAfaire.push({
            type: 'angle',
            x1: pt3x,
            y1: pt3y,
            x2: xdeb,
            y2: yd,
            x3: pt1x,
            y3: pt1y
          })
        }

        scratch.params.scratchProgDonnees.lesordres.push({ type: 'poserstylo' })
        if (!ds.Boucle) {
          nbmaxbloc = 9
          scratch.params.scratchProgDonnees.lesordres.push({ type: 'motion_movesteps', value: [{ nom: 'STEPS', contenu: { valeur: long1 } }] })
          scratch.params.scratchProgDonnees.lesordres.push({ type: 'motion_' + fturn, value: [{ nom: 'DEGREES', contenu: { valeur: ang } }] })
          scratch.params.scratchProgDonnees.lesordres.push({ type: 'motion_movesteps', value: [{ nom: 'STEPS', contenu: { valeur: long2 } }] })
          scratch.params.scratchProgDonnees.lesordres.push({ type: 'motion_' + fturn, value: [{ nom: 'DEGREES', contenu: { valeur: 180 - ang } }] })
          scratch.params.scratchProgDonnees.lesordres.push({ type: 'motion_movesteps', value: [{ nom: 'STEPS', contenu: { valeur: long1 } }] })
          scratch.params.scratchProgDonnees.lesordres.push({ type: 'motion_' + fturn, value: [{ nom: 'DEGREES', contenu: { valeur: ang } }] })
          scratch.params.scratchProgDonnees.lesordres.push({ type: 'motion_movesteps', value: [{ nom: 'STEPS', contenu: { valeur: long2 } }] })
        } else {
          const laboucle = []
          nbmaxbloc = 7
          laboucle.push({ type: 'motion_movesteps', value: [{ nom: 'STEPS', contenu: { valeur: long1 } }] })
          laboucle.push({ type: 'motion_' + fturn, value: [{ nom: 'DEGREES', contenu: { valeur: ang } }] })
          laboucle.push({ type: 'motion_movesteps', value: [{ nom: 'STEPS', contenu: { valeur: long2 } }] })
          laboucle.push({ type: 'motion_' + fturn, value: [{ nom: 'DEGREES', contenu: { valeur: 180 - ang } }] })
          scratch.params.scratchProgDonnees.lesordres.push({
            type: 'control_repeat',
            value: [{ nom: 'TIMES', contenu: { valeur: 2 } }],
            statement: [{ nom: 'SUBSTACK', contenu: laboucle }]
          })
        }
        break
      }

      case 'hexagone': {
        const long1 = j3pGetRandomInt(60, 70)
        const sensTrigo = j3pGetRandomBool()
        nomfig = 'cet hexagone régulier'
        const xdeb = 70
        let fturn, ttt, yd
        if (sensTrigo) {
          fturn = 'turnleft'
          ttt = -1
          yd = 130
        } else {
          fturn = 'turnright'
          yd = 25
          ttt = +1
        }
        const pt1x = xdeb + long1
        const pt1y = yd
        const pt2x = pt1x + long1 * Math.cos(60 * Math.PI / 180)
        const pt2y = yd + long1 * ttt * Math.sin(60 * Math.PI / 180)
        const pt3x = pt1x
        const pt3y = pt2y + long1 * ttt * Math.sin(60 * Math.PI / 180)
        const pt4x = xdeb
        const pt4y = pt3y
        const pt5x = xdeb - long1 * Math.cos(60 * Math.PI / 180)
        const pt5y = pt2y

        if (sensTrigo) {
          fturn = 'turnleft'
          ttt = -60
        } else {
          fturn = 'turnright'
          ttt = +60
        }
        scratch.params.figureAfaire = [{ type: 'pointdepart', x1: xdeb, y1: yd }]
        const point = { x: xdeb, y: yd, a: 0 }
        for (let i = 0; i < 6; i++) {
          let an = ttt
          if (i === 0) an = 0
          const npoint = psuiv(point.x, point.y, long1, point.a, an)
          const affichelong = (i < 2)
          scratch.params.figureAfaire.push({
            type: 'line',
            x1: point.x,
            y1: point.y,
            x2: npoint.x,
            y2: npoint.y,
            affichelong
          })
          point.x = npoint.x
          point.y = npoint.y
          point.a = npoint.a
        }

        scratch.params.figureAfaire.push({ type: 'angle', x1: xdeb, y1: yd, x2: pt1x, y2: pt1y, x3: pt2x, y3: pt2y })

        if (ds.AnglesAffiches) {
          scratch.params.figureAfaire.push({
            type: 'angle',
            x1: pt1x,
            y1: pt1y,
            x2: pt2x,
            y2: pt2y,
            x3: pt3x,
            y3: pt3y
          })
          scratch.params.figureAfaire.push({
            type: 'angle',
            x1: pt2x,
            y1: pt2y,
            x2: pt3x,
            y2: pt3y,
            x3: pt4x,
            y3: pt4y
          })
          scratch.params.figureAfaire.push({
            type: 'angle',
            x1: pt3x,
            y1: pt3y,
            x2: pt4x,
            y2: pt4y,
            x3: pt5x,
            y3: pt5y
          })
          scratch.params.figureAfaire.push({
            type: 'angle',
            x1: pt4x,
            y1: pt4y,
            x2: pt5x,
            y2: pt5y,
            x3: xdeb,
            y3: yd
          })
          scratch.params.figureAfaire.push({
            type: 'angle',
            x1: pt5x,
            y1: pt5y,
            x2: xdeb,
            y2: yd,
            x3: pt1x,
            y3: pt1y
          })
        }

        scratch.params.scratchProgDonnees.lesordres.push({ type: 'poserstylo' })
        if (!ds.Boucle) {
          nbmaxbloc = 13
          scratch.params.scratchProgDonnees.lesordres.push({ type: 'motion_movesteps', value: [{ nom: 'STEPS', contenu: { valeur: long1 } }] })
          scratch.params.scratchProgDonnees.lesordres.push({ type: 'motion_' + fturn, value: [{ nom: 'DEGREES', contenu: { valeur: 60 } }] })
          scratch.params.scratchProgDonnees.lesordres.push({ type: 'motion_movesteps', value: [{ nom: 'STEPS', contenu: { valeur: long1 } }] })
          scratch.params.scratchProgDonnees.lesordres.push({ type: 'motion_' + fturn, value: [{ nom: 'DEGREES', contenu: { valeur: 60 } }] })
          scratch.params.scratchProgDonnees.lesordres.push({ type: 'motion_movesteps', value: [{ nom: 'STEPS', contenu: { valeur: long1 } }] })
          scratch.params.scratchProgDonnees.lesordres.push({ type: 'motion_' + fturn, value: [{ nom: 'DEGREES', contenu: { valeur: 60 } }] })
          scratch.params.scratchProgDonnees.lesordres.push({ type: 'motion_movesteps', value: [{ nom: 'STEPS', contenu: { valeur: long1 } }] })
          scratch.params.scratchProgDonnees.lesordres.push({ type: 'motion_' + fturn, value: [{ nom: 'DEGREES', contenu: { valeur: 60 } }] })
          scratch.params.scratchProgDonnees.lesordres.push({ type: 'motion_movesteps', value: [{ nom: 'STEPS', contenu: { valeur: long1 } }] })
          scratch.params.scratchProgDonnees.lesordres.push({ type: 'motion_' + fturn, value: [{ nom: 'DEGREES', contenu: { valeur: 60 } }] })
          scratch.params.scratchProgDonnees.lesordres.push({ type: 'motion_movesteps', value: [{ nom: 'STEPS', contenu: { valeur: long1 } }] })
        } else {
          const laboucle = []
          nbmaxbloc = 5
          laboucle.push({ type: 'motion_movesteps', value: [{ nom: 'STEPS', contenu: { valeur: long1 } }] })
          laboucle.push({ type: 'motion_' + fturn, value: [{ nom: 'DEGREES', contenu: { valeur: 60 } }] })
          scratch.params.scratchProgDonnees.lesordres.push({
            type: 'control_repeat',
            value: [{ nom: 'TIMES', contenu: { valeur: 6 } }],
            statement: [{ nom: 'SUBSTACK', contenu: laboucle }]
          })
        }
        break
      } // case hexagone
    } // switch

    scratch.enonceDessin(`<b>Écris le programme de ce lutin</b> pour qu’il reproduise ${nomfig}.`, {
      pourInject: { },
      modeCorrection: {
        type: 'SansComparaison',
        aVerif: [
          { quoi: 'NombreMaxBloc', nombre: nbmaxbloc + ds.tolerance },
          { quoi: 'QuandAvertir', quand: 'apres' },
          { quoi: 'AnnuleSiAvert', quand: false }
        ]
      },
      dirigeProg: { sequence: ds.Sequence, pasApas: ds.PasAPas, cacheTest: ds.CacheTest }
    })

    me.finEnonce()
    // c’est le drapeau vert qui valide
    me.cacheBoutonValider()
  } // enonceMain

  switch (this.etat) {
    case 'enonce':
      // code exécuté au lancement de la section
      if (this.debutDeLaSection) initSection()
      else enonceMain()
      break // case "enonce":

    case 'correction':
      try {
        const scratch = stor.scratch
        const ret = scratch.prepCorrec()
        const xmlDoc2 = ret.xmlDoc2
        const xmlText = ret.xmlText

        if (scratch.params.scratchProgDonnees.repEleve) {
          // Bonne réponse
          this.score++
          scratch.corrigeGood(xmlDoc2, xmlText)
        } else {
          // Pas de bonne réponse
          scratch.corrige(xmlDoc2, xmlText)
        }
        // on veut être rappelé dans l’état navigation
        this.finCorrection('navigation', true)
      } catch (error) {
        console.error(error)
      }
      break // case "correction":

    case 'navigation':
      if (this.sectionTerminee()) {
        stor.scratch.lesdivs.BoutonsSuite.appendChild(this.buttonsElts.sectionSuivante)
        this.buttonsElts.sectionSuivante.addEventListener('click', () => {
          stor.scratch.params.grosStop = true
        })
      } else {
        this.etat = 'enonce'
        stor.scratch.lesdivs.BoutonsSuite.appendChild(this.buttonsElts.suite)
        this.buttonsElts.suite.addEventListener('click', () => {
          stor.scratch.params.grosStop = true
        })
      }
      // Obligatoire
      this.finNavigation()
      break // case "navigation":
  }
}
