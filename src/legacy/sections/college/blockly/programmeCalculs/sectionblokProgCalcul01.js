import { j3pAjouteBouton, j3pArrondi, j3pGetRandomBool, j3pGetRandomInt } from 'src/legacy/core/functions'
import Algebrite from 'algebrite'
import BulleAide from 'src/legacy/outils/bulleAide/BulleAide'
import MenuContextuel from 'src/legacy/outils/menuContextuel'
import { j3pAffiche } from 'src/lib/mathquill/functions'
import { addDefaultTable } from 'src/legacy/themes/table'
import { getNewScratch } from 'src/legacy/outils/scratch/loaders'

export const params = {
  outils: ['mathquill'],
  /**
   * Ce qui est paramétrable dans l’éditeur de graphe
   * @type {LegacySectionParamsParametres}
   */
  parametres: [
    ['nbrepetitions', 4, 'entier', 'Nombre de répétitions de la section'],
    ['nbchances', 0, 'entier', 'Nombre d’essais possibles (illimité si 0)'],
    ['PasAPas', false, 'boolean', "<u>true</u>: L’élève peut lancer son programme en mode 'Pas à pas'."],
    ['Sequence', true, 'boolean', '<u>true</u>: Chaque bloc a une durée d’exécution d’environ 1 seconde.'],
    ['Negatifs', true, 'boolean', '<u>true</u>: Le programme de calcul peut utiliser des négatifs.'],
    ['Decimaux', true, 'boolean', '<u>true</u>: Le programme de calcul peut utiliser des décimaux.'],
    ['difficultemin', 1, 'entier', 'Difficulté du programme (minimum 1)'],
    ['difficultemax', 4, 'entier', 'Difficulté du programme (maximum 4)'],
    ['difficulte', 'Progressive', 'liste', 'Gestion de la progression (entre min et max)', ['Aléatoire', 'Progressive']],
    ['CacheTest', false, 'boolean', '<u>true</u>: L’élève ne voit pas l’action de son programme lors d la correction.']
  ]
}

/**
 * section blokProgCalcul01
 * @this {Parcours}
 */
export default function main () {
  const me = this
  const stor = me.storage
  const ds = me.donneesSection

  function decoupeMenu () {
    stor.divCalPro.style.display = 'none'
    stor.ledivdetavhe.style.display = ''
  }
  function cacheProg () {
    stor.divCalPro.style.display = ''
    stor.ledivdetavhe.style.display = 'none'
  }

  async function initSection () {
    const scratch = await getNewScratch()
    stor.scratch = scratch
    stor.tabcodeX = []
    stor.Averif = []
    stor.afficheEssai = true
    // pas de validation sur entrée, c’est le drapeau vert qui valide
    me.validOnEnter = false // ex donneesSection.touche_entree

    scratch.initParams(me)
    scratch.testFlag = function () { }
    scratch.testFlagFin = function () {
      const t = scratch.params.scratchProgDonnees.reponseDonnesAGetUS[scratch.params.scratchProgDonnees.reponseDonnesAGetUS.length - 1]
      const lastDit = scratch.params.scratchProgDonnees.said.pop()
      const gg = ('(' + stor.larep + ')-(' + lastDit + ')').replace(/x@ù/g, 'x')
      const XX = Algebrite.run(gg)
      if ((XX === '0') || (XX === '0.0')) { return { ok: true, mess: '' } } else {
        let arrr = Algebrite.run(stor.larep.replace('x@ù', t))
        if (arrr.indexOf('/') !== -1) arrr = parseInt(arrr.substring(0, arrr.indexOf('/'))) / parseInt(arrr.substring(arrr.indexOf('/') + 1))
        return { ok: false, mess: 'Pour ' + t + ' Scratch aurait du dire: ' + arrr }
      }
    }
    scratch.params.yaProgCalcul = true
    ds.nbitems = ds.nbetapes * ds.nbrepetitions

    stor.opfait = []
    stor.opfait2 = []

    ds.difficultemin = Math.max(1, ds.difficultemin)
    ds.difficultemax = Math.min(4, ds.difficultemax)
    if (ds.difficultemax < ds.difficultemin) {
      ds.difficultemin = ds.difficultemax
    }

    me.construitStructurePage({ structure: 'presentation3', theme: 'zonesAvecImageDeFond', ratioGauche: 0.6 })
    me.stockage = [0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0]
    stor.dejafait = []
    me.afficheTitre('Faire des calculs')
    scratch.lemenu = { destroy: () => {} }
    enonceMain()
  }

  function enonceMain () {
    const scratch = stor.scratch
    scratch.lemenu.destroy()
    me.videLesZones()

    scratch.params.scratchProgDonnees.lesvarbonnes = [{ nom: 'résultat', val: 'vide', id: 'bbbbbb' }]
    scratch.params.scratchProgDonnees.menu = []
    scratch.params.scratchProgDonnees.progdeb = []
    scratch.params.scratchProgDonnees.lesordres = []
    scratch.params.scratchProgDonnees.progdeb.push({ type: 'Drapo', editable: 'false', deletable: 'false', movable: 'false', x: '20', y: '20' })
    scratch.params.scratchProgDonnees.progdeb.push({ type: 'Get_us', value: [{ nom: 'STRING', contenu: { valeur: 'Donne un nombre', editable: 'false', deletable: 'false', movable: 'false' } }], editable: 'false', deletable: 'false', movable: 'false' })
    scratch.params.scratchProgDonnees.progdeb.push({ type: 'data_setvariableto', editable: 'false', deletable: 'false', movable: 'false', variable: [{ nom: 'VARIABLE', nomvar: 'résultat', id: 'bbbbbb' }], value: [{ nom: 'VALUE', editable: 'false', deletable: 'false', movable: 'false', contenu: { type: 'fauxx', editable: 'false', deletable: 'false', movable: 'false' } }] })
    scratch.params.scratchProgDonnees.lesordres.push({ type: 'Get_us', value: [{ nom: 'STRING', contenu: { valeur: 'Donne un nombre', editable: 'false', deletable: 'false', movable: 'false' } }], editable: 'false', deletable: 'false', movable: 'false' })
    scratch.params.scratchProgDonnees.lesordres.push({ type: 'data_setvariableto', editable: 'false', deletable: 'false', movable: 'false', variable: [{ nom: 'VARIABLE', nomvar: 'résultat', id: 'bbbbbb' }], value: [{ nom: 'VALUE', editable: 'false', deletable: 'false', movable: 'false', contenu: { type: 'fauxx', editable: 'false', deletable: 'false', movable: 'false' } }] })

    scratch.params.scratchProgDonnees.menu.push({ type: 'catégorie', nom: 'ordres', coul1: '#4C97FF', coul2: '#3373CC', contenu: [] })
    scratch.params.scratchProgDonnees.menu[0].contenu.push({ type: 'operator_add2', value: [{ nom: 'NUM1', contenu: { type: 'data_variable', editable: 'false', deletable: 'false', movable: 'false', variable: [{ nom: 'VARIABLE', nomvar: 'résultat', id: 'bbbbbb' }] } }] })
    scratch.params.scratchProgDonnees.menu[0].contenu.push({ type: 'operator_subtract2', value: [{ nom: 'NUM1', contenu: { type: 'data_variable', editable: 'false', deletable: 'false', movable: 'false', variable: [{ nom: 'VARIABLE', nomvar: 'résultat', id: 'bbbbbb' }] } }] })
    scratch.params.scratchProgDonnees.menu[0].contenu.push({ type: 'operator_subtract3', value: [{ nom: 'NUM2', contenu: { type: 'data_variable', editable: 'false', deletable: 'false', movable: 'false', variable: [{ nom: 'VARIABLE', nomvar: 'résultat', id: 'bbbbbb' }] } }] })
    scratch.params.scratchProgDonnees.menu[0].contenu.push({ type: 'operator_multiply2', value: [{ nom: 'NUM1', contenu: { type: 'data_variable', editable: 'false', deletable: 'false', movable: 'false', variable: [{ nom: 'VARIABLE', nomvar: 'résultat', id: 'bbbbbb' }] } }] })
    scratch.params.scratchProgDonnees.menu[0].contenu.push({ type: 'operator_divide2', value: [{ nom: 'NUM1', contenu: { type: 'data_variable', editable: 'false', deletable: 'false', movable: 'false', variable: [{ nom: 'VARIABLE', nomvar: 'résultat', id: 'bbbbbb' }] } }] })
    scratch.params.scratchProgDonnees.menu[0].contenu.push({ type: 'operator_divide3', value: [{ nom: 'NUM2', contenu: { type: 'data_variable', editable: 'false', deletable: 'false', movable: 'false', variable: [{ nom: 'VARIABLE', nomvar: 'résultat', id: 'bbbbbb' }] } }] })

    stor.listeid = []
    stor.valeurauto = []
    scratch.params.scratchProgDonnees.Lutins[0].cox = 10
    scratch.params.scratchProgDonnees.Lutins[0].coxbase = 10
    scratch.params.scratchProgDonnees.Lutins[0].coy = 150
    scratch.params.scratchProgDonnees.Lutins[0].coybase = 150
    stor.diff = j3pGetRandomInt(ds.difficultemin, ds.difficultemax)

    if (ds.difficulte === 'Progressive') {
      stor.diff = ds.difficultemin + Math.round((me.questionCourante - 1) * (ds.difficultemax - ds.difficultemin) / (ds.nbrepetitions - 1))
    }
    stor.tabop = []
    stor.ProgCAl = []
    stor.ProgCAl.push({
      name: 'men' + stor.ProgCAl.length,
      label: '1♦ Choisir un nombre.',
      callback: function (menu/*, choice, event */) {
      }
    })
    stor.larep = 'x@ù'
    const tabpos = [['Ajouter £a £b .', 'operator_add', '(£x)+(£n)', 'au', false], ['Soustraire £a £b .', 'operator_subtract', '(£x)-(£n)', 'au', false], ['Multiplier £b par £a .', 'operator_multiply', '(£x)*(£n)', 'le', false], ['Diviser £b par £a .', 'operator_divide', '(£x)/(£n)', 'le', false], ['Diviser £a par £b .', 'operator_divide', '(£n)/(£x)', 'le', true], ['Soustraire £b à £a .', 'operator_subtract', '(£n)-(£x)', 'le', true]]
    for (let i = 0; i < stor.diff; i++) {
      scratch.params.scratchProgDonnees.progdeb.push({ type: 'data_setvariableto', editable: 'false', deletable: 'false', movable: 'false', variable: [{ nom: 'VARIABLE', nomvar: 'résultat', id: 'bbbbbb' }] })
      stor.tabop.push({ op: tabpos[j3pGetRandomInt(0, 5)], nb: j3pGetRandomInt(11, 100) })
      if (j3pGetRandomBool() && ds.Decimaux) {
        stor.tabop[stor.tabop.length - 1].nb = j3pArrondi(stor.tabop[stor.tabop.length - 1].nb / 10, 1)
      }
      if (j3pGetRandomBool() && ds.Negatifs) {
        stor.tabop[stor.tabop.length - 1].nb = -stor.tabop[stor.tabop.length - 1].nb
      }
      let bubuf = stor.tabop[stor.tabop.length - 1].op[0].replace('£a', (stor.tabop[stor.tabop.length - 1].nb + '').replace('.', ','))
      if (i !== 0) {
        bubuf = bubuf.replace('£b', stor.tabop[stor.tabop.length - 1].op[3] + ' résultat ')
      } else {
        if (stor.tabop[stor.tabop.length - 1].op[4]) {
          bubuf = bubuf.replace('£b', ' le nombre ')
        } else {
          bubuf = bubuf.replace('£b', '')
        }
      }
      stor.ProgCAl.push({
        name: 'men' + i,
        label: (i + 2) + '♦ ' + bubuf,
        callback: function (menu/*, choice, event */) {
        }
      })
      stor.larep = stor.tabop[stor.tabop.length - 1].op[2].replace('£x', stor.larep).replace('£n', stor.tabop[stor.tabop.length - 1].nb)
      if (stor.tabop[stor.tabop.length - 1].op[4]) {
        scratch.params.scratchProgDonnees.lesordres.push({ type: 'data_setvariableto', editable: 'false', deletable: 'false', movable: 'false', variable: [{ nom: 'VARIABLE', nomvar: 'résultat', id: 'bbbbbb' }], value: [{ nom: 'VALUE', editable: 'false', deletable: 'false', movable: 'false', contenu: { type: stor.tabop[stor.tabop.length - 1].op[1], editable: 'false', deletable: 'false', movable: 'false', value: [{ nom: 'NUM2', contenu: { type: 'data_variable', editable: 'false', deletable: 'false', movable: 'false', variable: [{ nom: 'VARIABLE', nomvar: 'résultat', id: 'bbbbbb' }] } }, { nom: 'NUM1', contenu: { valeur: stor.tabop[stor.tabop.length - 1].nb } }] } }] })
      } else {
        scratch.params.scratchProgDonnees.lesordres.push({ type: 'data_setvariableto', editable: 'false', deletable: 'false', movable: 'false', variable: [{ nom: 'VARIABLE', nomvar: 'résultat', id: 'bbbbbb' }], value: [{ nom: 'VALUE', editable: 'false', deletable: 'false', movable: 'false', contenu: { type: stor.tabop[stor.tabop.length - 1].op[1], editable: 'false', deletable: 'false', movable: 'false', value: [{ nom: 'NUM1', contenu: { type: 'data_variable', editable: 'false', deletable: 'false', movable: 'false', variable: [{ nom: 'VARIABLE', nomvar: 'résultat', id: 'bbbbbb' }] } }, { nom: 'NUM2', contenu: { valeur: stor.tabop[stor.tabop.length - 1].nb } }] } }] })
      }
    }
    stor.ProgCAl.push(
      {
        name: 'men' + stor.ProgCAl.length,
        label: '<u><b>rappel</b></u>: Dans scratch, le point remplace la virgule.',
        callback: function () {}
      }
    )
    scratch.params.scratchProgDonnees.progdeb.push({ type: 'dire', editable: 'false', deletable: 'false', movable: 'false', value: [{ nom: 'DELTA', contenu: { type: 'data_variable', editable: 'false', deletable: 'false', movable: 'false', variable: [{ nom: 'VARIABLE', nomvar: 'résultat', id: 'bbbbbb' }] } }, { nom: 'TEMPS', contenu: { valeur: 2, editable: 'false', deletable: 'false', movable: 'false' } }] })
    scratch.params.scratchProgDonnees.lesordres.push({ type: 'dire', editable: 'false', deletable: 'false', movable: 'false', value: [{ nom: 'DELTA', contenu: { type: 'data_variable', editable: 'false', deletable: 'false', movable: 'false', variable: [{ nom: 'VARIABLE', nomvar: 'résultat', id: 'bbbbbb' }] } }, { nom: 'TEMPS', contenu: { valeur: 2, editable: 'false', deletable: 'false', movable: 'false' } }] })

    scratch.enonce([], {
      pourInject: { StartScale: 0.70 },
      modeCorrection: {
        type: 'SansComparaison',
        aVerif: [
          { quoi: 'QuandAvertir', quand: 'après' }]
      },
      dirigeProg: { sequence: ds.Sequence, pasApas: ds.PasAPas, cacheTest: ds.CacheTest }
    })

    scratch.params.scratchProgDonnees.listValPos = [['1'], ['2'], ['3'], ['4'], ['5'], ['6'], ['7'], ['8'], ['9'], ['10']]

    // les bonnes questions
    const cons = '<b> Programme le chat pour qu’il effectue le programme de calcul ci-dessous. </b> '
    const tt1 = addDefaultTable(scratch.lesdivs.enonceG, 2, 1)
    tt1[0][0].style.padding = 0
    const tt2 = addDefaultTable(tt1[0][0], 1, 2)
    tt2[0][0].style.padding = 0
    tt2[0][1].style.padding = 0
    j3pAffiche(tt2[0][0], null, cons + '&nbsp;')
    const tt3 = addDefaultTable(tt1[1][0], 1, 2)
    tt3[0][0].style.padding = 0
    tt3[0][1].style.padding = 0
    stor.divCalPro = tt3[0][0]
    j3pAffiche(stor.divCalPro, null, '&nbsp;Programme de calcul&nbsp;')
    stor.divCalPro.style.color = '#F22'
    stor.divCalPro.style.cursor = 'pointer'
    stor.divCalPro.style.border = '1px solid black'
    stor.divCalPro.style.background = '#aaaaff'
    stor.aise = new BulleAide(tt2[0][1], 'Clique sur <i><b>programme de calcul</b></i> pour le voir', { place: 5 })

    const decoupe = {
      callback: decoupeMenu,
      label: '<span style="background: #aaaaff; color: #f22; font-size: 70%"><i><b>Clique ici pour détacher le programme</b></i></span>',
      name: 'men00'
    }
    stor.ledivdetavhe = tt3[0][1]
    for (let i = 0; i < stor.ProgCAl.length; i++) {
      j3pAffiche(stor.ledivdetavhe, null, stor.ProgCAl[i].label.replace('AZERTYUIOPNN', '') + '\n')
    }
    const rty = j3pAjouteBouton(stor.ledivdetavhe, cacheProg, { value: 'Cacher le Programme' })
    rty.style.background = '#aaaaff'
    rty.style.color = '#F22'
    stor.ledivdetavhe.style.display = 'none'
    stor.ledivdetavhe.style.background = '#eee'
    stor.ledivdetavhe.style.border = '1px solid black'
    stor.ProgCAl.push(decoupe)

    scratch.lemenu = new MenuContextuel(stor.divCalPro, stor.ProgCAl)

    scratch.params.scratchProgDonnees.nbessai = ds.nbchances
    me.finEnonce()
    // on veut pas du bouton ok, c’est le drapeau vert qui valide
    me.cacheBoutonValider()
  }

  switch (this.etat) {
    case 'enonce':
      if (this.debutDeLaSection) {
        initSection()
      } else {
        enonceMain()
      }
      break // case "enonce":

    case 'correction':
      try {
        const scratch = stor.scratch
        const ret = scratch.prepCorrec()
        const xmlDoc2 = ret.xmlDoc2
        const xmlText = ret.xmlText

        if (scratch.params.scratchProgDonnees.repEleve) {
          // Bonne réponse
          this.score++
          scratch.corrigeGood(xmlDoc2, xmlText)
        } else {
          // Pas de bonne réponse
          scratch.corrige(xmlDoc2, xmlText)
        }
        // on veut être rappelé dans l’état navigation
        this.finCorrection('navigation', true)
      } catch (error) {
        console.error(error)
      }
      break // case "correction":

    case 'navigation':
      if (this.sectionTerminee()) {
        stor.scratch.lesdivs.BoutonsSuite.appendChild(this.buttonsElts.sectionSuivante)
        this.buttonsElts.sectionSuivante.addEventListener('click', () => {
          stor.scratch.params.grosStop = true
        })
      } else {
        this.etat = 'enonce'
        stor.scratch.lesdivs.BoutonsSuite.appendChild(this.buttonsElts.suite)
        this.buttonsElts.suite.addEventListener('click', () => {
          stor.scratch.params.grosStop = true
        })
      }
      // Obligatoire
      this.finNavigation()
      break // case "navigation":
  }
}
