import { j3pGetRandomInt } from 'src/legacy/core/functions'
import { j3pAffiche } from 'src/lib/mathquill/functions'
import { getNewScratch } from 'src/legacy/outils/scratch/loaders'
import { addDefaultTable } from 'src/legacy/themes/table'

export const params = {
  outils: ['mathquill'],
  /**
   * Ce qui est paramétrable dans l’éditeur de graphe
   * @type {LegacySectionParamsParametres}
   */
  parametres: [
    ['nbrepetitions', 4, 'entier', 'Nombre de répétitions de la section'],
    ['nbchances', 0, 'entier', 'Nombre d’essais possibles (illimité si 0)'],
    ['difficultemin', 1, 'entier', 'Difficulté du programme (minimum 1)'],
    ['difficultemax', 4, 'entier', 'Difficulté du programme (maximum 4)'],
    ['difficulte', 'Progressive', 'liste', 'Gestion de la progression (entre min et max)', ['Aléatoire', 'Progressive']],
    ['PasAPas', true, 'boolean', "<u>true</u>: L’élève peut lancer son programme en mode 'Pas à pas'."],
    ['Sequence', true, 'boolean', '<u>true</u>: Chaque bloc a une durée d’exécution d’environ 1 seconde.'],
    ['CacheTest', false, 'boolean', '<u>true</u>: L’élève ne voit pas l’action de son programme lors de la correction.']
  ]
}

/**
 * section blokblok01
 * @this {Parcours}
 */
export default function main () {
  const me = this
  const stor = me.storage
  const ds = me.donneesSection
  let scratch = stor.scratch

  async function initSection () {
    ds.theme = 'zonesAvecImageDeFond'
    scratch = await getNewScratch()
    stor.scratch = scratch
    stor.tabcodeX = []
    stor.Averif = []
    me.validOnEnter = false // ex donneesSection.touche_entree
    scratch.initParams(me)
    scratch.testFlag = function () {
      if (scratch.params.scratchProgDonnees.Lutins[0].coy === 3 && scratch.params.scratchProgDonnees.Lutins[0].hauteur === 0) scratch.params.scratchProgDonnees.lesflag[0].ok = true
      if (scratch.params.scratchProgDonnees.Lutins[0].coy === 1 && scratch.params.scratchProgDonnees.Lutins[0].hauteur === 0) scratch.params.scratchProgDonnees.lesflag[1].ok = true
      if (scratch.params.scratchProgDonnees.Lutins[0].coy === 0 && scratch.params.scratchProgDonnees.Lutins[0].hauteur === 0) scratch.params.scratchProgDonnees.lesflag[2].ok = true

      if (scratch.params.scratchProgDonnees.Lutins[0].coy === 2 && scratch.params.scratchProgDonnees.Lutins[0].hauteur === 0) scratch.params.scratchProgDonnees.lesflag[0].ok = true
      if (scratch.params.scratchProgDonnees.Lutins[0].coy === 0 && scratch.params.scratchProgDonnees.Lutins[0].cox === 2 && scratch.params.scratchProgDonnees.Lutins[0].hauteur === 0) scratch.params.scratchProgDonnees.lesflag[1].ok = true
      if (scratch.params.scratchProgDonnees.Lutins[0].coy === 0 && scratch.params.scratchProgDonnees.Lutins[0].cox === 4 && scratch.params.scratchProgDonnees.Lutins[0].hauteur === 0) scratch.params.scratchProgDonnees.lesflag[2].ok = true
    }
    scratch.testFlagFin = function () {
      switch (stor.nbblocattendu) {
        case 1:
          if (scratch.params.scratchProgDonnees.cartefusee[2][0] + scratch.params.scratchProgDonnees.cartefusee[2][1] + scratch.params.scratchProgDonnees.cartefusee[2][3] > 0) return { ok: false, mess: 'La fusée doit récupérer tous les spationautes !' }
          return { ok: true, mess: '' }
        case 2:
        case 3:
          if (!(scratch.params.scratchProgDonnees.lesflag[0].ok && scratch.params.scratchProgDonnees.lesflag[1].ok && scratch.params.scratchProgDonnees.lesflag[2].ok)) return { ok: false, mess: 'La fusée doit attérir toutes les planètes !' }
          return { ok: true, mess: '' }
        case 4:
          if (!(scratch.params.scratchProgDonnees.lesflag[0].ok && scratch.params.scratchProgDonnees.lesflag[1].ok && scratch.params.scratchProgDonnees.lesflag[2].ok)) return { ok: false, mess: 'La fusée doit attérir toutes les planètes !' }
          if (scratch.params.scratchProgDonnees.cartefusee[2][3] + scratch.params.scratchProgDonnees.cartefusee[2][1] + scratch.params.scratchProgDonnees.cartefusee[3][0] > 0) return { ok: false, mess: 'La fusée doit récupérer tous les spationautes !' }
          return { ok: true, mess: '' }
      }
    }
    scratch.params.scratchProgDonnees.squizzVar = true
    ds.nbitems = ds.nbetapes * ds.nbrepetitions

    me.construitStructurePage({ structure: 'presentation3', ratioGauche: 0.6 })
    me.stockage = [0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0]
    scratch.params.verifDeb = true

    me.score = 0
    stor.dejafait = []
    me.afficheTitre('FUSEE - Utiliser un bloc personnalisé')

    ds.difficultemin = Math.min(4, Math.max(1, ds.difficultemin))
    ds.difficultemax = Math.max(1, Math.min(4, ds.difficultemax))
    if (ds.difficultemax < ds.difficultemin) {
      ds.difficultemin = ds.difficultemax
    }

    scratch.params.scratchProgDonnees.Lutins[0].larg = 40
    scratch.params.scratchProgDonnees.Lutins[0].hauteurbase = 100
    scratch.params.scratchProgDonnees.Lutins[0].haut = 50
    scratch.params.scratchProgDonnees.Lutins[0].coxbase = 70
    scratch.params.scratchProgDonnees.Lutins[0].coybase = 135

    scratch.params.scratchProgDonnees.menu.push({ type: 'catégorie', nom: 'DEPLACEMENTS', coul1: '#4C97FF', coul2: '#3373CC', contenu: [] })
    scratch.params.scratchProgDonnees.menu[0].contenu.push({ type: 'avance_fusee' })
    scratch.params.scratchProgDonnees.menu[0].contenu.push({ type: 'droite_fusee' })
    scratch.params.scratchProgDonnees.menu[0].contenu.push({ type: 'gauche_fusee' })
    scratch.params.scratchProgDonnees.menu.push({ type: 'catégorie', nom: 'ACTIONS', coul1: '#D65CD6', coul2: '#BD42BD', contenu: [] })
    scratch.params.scratchProgDonnees.menu[1].contenu.push({ type: 'decolle_fusee' })
    scratch.params.scratchProgDonnees.menu[1].contenu.push({ type: 'atteri_fusee' })
    scratch.params.scratchProgDonnees.menu[1].contenu.push({ type: 'recuperer_fusee' })
    enonceMain()
  }

  function enonceMain () {
    me.videLesZones()
    scratch.params.scratchProgDonnees.lesordres = []

    stor.nbblocattendu = j3pGetRandomInt(ds.difficultemin, ds.difficultemax)
    if (ds.difficulte === 'Progressive') {
      stor.nbblocattendu = ds.difficultemin + Math.round((me.questionCourante - 1) * (ds.difficultemax - ds.difficultemin) / (ds.nbrepetitions - 1))
      if (ds.nbrepetitions === 1) { stor.nbblocattendu = ds.difficultemin }
    }
    const elem = []
    let cons, lim
    switch (stor.nbblocattendu) {
      case 1:
        scratch.params.scratchProgDonnees.progdeb = []
        scratch.params.scratchProgDonnees.progdeb.push([{ type: 'MAkePerso', editable: 'false', deletable: 'false', movable: 'false', x: '20', y: '20' }])
        scratch.params.scratchProgDonnees.progdeb.push([{ type: 'Drapo', editable: 'false', deletable: 'false', movable: 'false', x: '200', y: '20' }])
        scratch.params.scratchProgDonnees.progdeb[1].push({ type: 'avance_fusee', editable: 'false', deletable: 'false', movable: 'false' })
        scratch.params.scratchProgDonnees.progdeb[1].push({ type: 'perso', editable: 'false', deletable: 'false', movable: 'false' })
        scratch.params.scratchProgDonnees.progdeb[1].push({ type: 'avance_fusee', editable: 'false', deletable: 'false', movable: 'false' })
        scratch.params.scratchProgDonnees.progdeb[1].push({ type: 'avance_fusee', editable: 'false', deletable: 'false', movable: 'false' })
        scratch.params.scratchProgDonnees.progdeb[1].push({ type: 'perso', editable: 'false', deletable: 'false', movable: 'false' })
        scratch.params.scratchProgDonnees.progdeb[1].push({ type: 'avance_fusee', editable: 'false', deletable: 'false', movable: 'false' })
        scratch.params.scratchProgDonnees.progdeb[1].push({ type: 'perso', editable: 'false', deletable: 'false', movable: 'false' })

        elem.push({ quoi: 'cosmo', x: 2, y: 3 })
        elem.push({ quoi: 'cosmo', x: 2, y: 1 })
        elem.push({ quoi: 'cosmo', x: 2, y: 0 })
        scratch.params.scratchProgDonnees.lesordres = []
        lim = 10
        scratch.params.scratchProgDonnees.lesordres.push([{ type: 'MAkePerso', editable: 'false', deletable: 'false', movable: 'false', x: '20', y: '20' }])
        scratch.params.scratchProgDonnees.lesordres.push([])
        scratch.params.scratchProgDonnees.lesordres[1].push({ type: 'avance_fusee', editable: 'false', deletable: 'false', movable: 'false' })
        scratch.params.scratchProgDonnees.lesordres[1].push({ type: 'perso', editable: 'false', deletable: 'false', movable: 'false' })
        scratch.params.scratchProgDonnees.lesordres[1].push({ type: 'avance_fusee', editable: 'false', deletable: 'false', movable: 'false' })
        scratch.params.scratchProgDonnees.lesordres[1].push({ type: 'avance_fusee', editable: 'false', deletable: 'false', movable: 'false' })
        scratch.params.scratchProgDonnees.lesordres[1].push({ type: 'perso', editable: 'false', deletable: 'false', movable: 'false' })
        scratch.params.scratchProgDonnees.lesordres[1].push({ type: 'avance_fusee', editable: 'false', deletable: 'false', movable: 'false' })
        scratch.params.scratchProgDonnees.lesordres[1].push({ type: 'perso', editable: 'false', deletable: 'false', movable: 'false' })
        scratch.params.scratchProgDonnees.lesordres[0].push({ type: 'recuperer_fusee', editable: 'false', deletable: 'false', movable: 'false' })
        cons = '<b>Programme la définition de  <i>Mon bloc</i> </b> pour que la fusée récupère les 3 spationautes.'
        break
      case 2:
        scratch.params.scratchProgDonnees.progdeb = []
        scratch.params.scratchProgDonnees.progdeb.push([{ type: 'MAkePerso', editable: 'false', deletable: 'false', movable: 'false', x: '20', y: '20' }])
        scratch.params.scratchProgDonnees.progdeb.push([{ type: 'Drapo', editable: 'false', deletable: 'false', movable: 'false', x: '200', y: '20' }])
        scratch.params.scratchProgDonnees.progdeb[1].push({ type: 'avance_fusee', editable: 'false', deletable: 'false', movable: 'false' })
        scratch.params.scratchProgDonnees.progdeb[1].push({ type: 'perso', editable: 'false', deletable: 'false', movable: 'false' })
        scratch.params.scratchProgDonnees.progdeb[1].push({ type: 'avance_fusee', editable: 'false', deletable: 'false', movable: 'false' })
        scratch.params.scratchProgDonnees.progdeb[1].push({ type: 'avance_fusee', editable: 'false', deletable: 'false', movable: 'false' })
        scratch.params.scratchProgDonnees.progdeb[1].push({ type: 'perso', editable: 'false', deletable: 'false', movable: 'false' })
        scratch.params.scratchProgDonnees.progdeb[1].push({ type: 'avance_fusee', editable: 'false', deletable: 'false', movable: 'false' })
        scratch.params.scratchProgDonnees.progdeb[1].push({ type: 'perso', editable: 'false', deletable: 'false', movable: 'false' })

        elem.push({ quoi: 'mars', x: 2, y: 3 })
        elem.push({ quoi: 'mars', x: 2, y: 1 })
        elem.push({ quoi: 'mars', x: 2, y: 0 })
        scratch.params.scratchProgDonnees.lesordres = []
        lim = 11
        scratch.params.scratchProgDonnees.lesordres.push([{ type: 'MAkePerso', editable: 'false', deletable: 'false', movable: 'false', x: '20', y: '20' }])
        scratch.params.scratchProgDonnees.lesordres.push([])
        scratch.params.scratchProgDonnees.lesordres[1].push({ type: 'avance_fusee', editable: 'false', deletable: 'false', movable: 'false' })
        scratch.params.scratchProgDonnees.lesordres[1].push({ type: 'perso', editable: 'false', deletable: 'false', movable: 'false' })
        scratch.params.scratchProgDonnees.lesordres[1].push({ type: 'avance_fusee', editable: 'false', deletable: 'false', movable: 'false' })
        scratch.params.scratchProgDonnees.lesordres[1].push({ type: 'avance_fusee', editable: 'false', deletable: 'false', movable: 'false' })
        scratch.params.scratchProgDonnees.lesordres[1].push({ type: 'perso', editable: 'false', deletable: 'false', movable: 'false' })
        scratch.params.scratchProgDonnees.lesordres[1].push({ type: 'avance_fusee', editable: 'false', deletable: 'false', movable: 'false' })
        scratch.params.scratchProgDonnees.lesordres[1].push({ type: 'perso', editable: 'false', deletable: 'false', movable: 'false' })
        scratch.params.scratchProgDonnees.lesordres[0].push({ type: 'atteri_fusee', editable: 'false', deletable: 'false', movable: 'false' })
        scratch.params.scratchProgDonnees.lesordres[0].push({ type: 'decolle_fusee', editable: 'false', deletable: 'false', movable: 'false' })
        cons = '<b>Programme la définition de  <i>Mon bloc</i> </b> pour que la fusée attérisse sur chaque planète.'
        break
      case 3:
        scratch.params.scratchProgDonnees.progdeb = []
        scratch.params.scratchProgDonnees.progdeb.push([{ type: 'MAkePerso', editable: 'false', deletable: 'false', movable: 'false', x: '20', y: '20' }])
        scratch.params.scratchProgDonnees.progdeb.push([{ type: 'Drapo', editable: 'false', deletable: 'false', movable: 'false', x: '200', y: '20' }])
        scratch.params.scratchProgDonnees.progdeb[1].push({ type: 'perso', editable: 'false', deletable: 'false', movable: 'false' })
        scratch.params.scratchProgDonnees.progdeb[1].push({ type: 'avance_fusee', editable: 'false', deletable: 'false', movable: 'false' })
        scratch.params.scratchProgDonnees.progdeb[1].push({ type: 'perso', editable: 'false', deletable: 'false', movable: 'false' })
        scratch.params.scratchProgDonnees.progdeb[1].push({ type: 'perso', editable: 'false', deletable: 'false', movable: 'false' })

        elem.push({ quoi: 'mars', x: 2, y: 3 })
        elem.push({ quoi: 'mars', x: 2, y: 1 })
        elem.push({ quoi: 'mars', x: 2, y: 0 })
        scratch.params.scratchProgDonnees.lesordres = []
        lim = 9
        scratch.params.scratchProgDonnees.lesordres.push([{ type: 'MAkePerso', editable: 'false', deletable: 'false', movable: 'false', x: '20', y: '20' }])
        scratch.params.scratchProgDonnees.lesordres.push([])
        scratch.params.scratchProgDonnees.lesordres[1].push({ type: 'perso', editable: 'false', deletable: 'false', movable: 'false' })
        scratch.params.scratchProgDonnees.lesordres[1].push({ type: 'avance_fusee', editable: 'false', deletable: 'false', movable: 'false' })
        scratch.params.scratchProgDonnees.lesordres[1].push({ type: 'perso', editable: 'false', deletable: 'false', movable: 'false' })
        scratch.params.scratchProgDonnees.lesordres[1].push({ type: 'perso', editable: 'false', deletable: 'false', movable: 'false' })
        scratch.params.scratchProgDonnees.lesordres[0].push({ type: 'avance_fusee', editable: 'false', deletable: 'false', movable: 'false' })
        scratch.params.scratchProgDonnees.lesordres[0].push({ type: 'atteri_fusee', editable: 'false', deletable: 'false', movable: 'false' })
        scratch.params.scratchProgDonnees.lesordres[0].push({ type: 'decolle_fusee', editable: 'false', deletable: 'false', movable: 'false' })
        cons = '<b>Programme la définition de  <i>Mon bloc</i> </b> pour que la fusée attérisse sur chaque planète.'
        break
      case 4:
        scratch.params.scratchProgDonnees.progdeb = []
        scratch.params.scratchProgDonnees.progdeb.push([{ type: 'MAkePerso', editable: 'false', deletable: 'false', movable: 'false', x: '20', y: '20' }])
        scratch.params.scratchProgDonnees.progdeb.push([{ type: 'Drapo', editable: 'false', deletable: 'false', movable: 'false', x: '200', y: '20' }])
        scratch.params.scratchProgDonnees.progdeb[1].push({ type: 'perso', editable: 'false', deletable: 'false', movable: 'false' })
        scratch.params.scratchProgDonnees.progdeb[1].push({ type: 'perso', editable: 'false', deletable: 'false', movable: 'false' })
        scratch.params.scratchProgDonnees.progdeb[1].push({ type: 'droite_fusee', editable: 'false', deletable: 'false', movable: 'false' })
        scratch.params.scratchProgDonnees.progdeb[1].push({ type: 'perso', editable: 'false', deletable: 'false', movable: 'false' })

        elem.push({ quoi: 'mars', x: 2, y: 2 })
        elem.push({ quoi: 'cosmo', x: 2, y: 3 })
        elem.push({ quoi: 'cosmo', x: 2, y: 1 })
        elem.push({ quoi: 'mars', x: 2, y: 0 })
        elem.push({ quoi: 'cosmo', x: 3, y: 0 })
        elem.push({ quoi: 'mars', x: 4, y: 0 })
        scratch.params.scratchProgDonnees.lesordres = []
        lim = 12
        scratch.params.scratchProgDonnees.lesordres.push([{ type: 'MAkePerso', editable: 'false', deletable: 'false', movable: 'false', x: '20', y: '20' }])
        scratch.params.scratchProgDonnees.lesordres.push([])
        scratch.params.scratchProgDonnees.lesordres[1].push({ type: 'perso', editable: 'false', deletable: 'false', movable: 'false' })
        scratch.params.scratchProgDonnees.lesordres[1].push({ type: 'perso', editable: 'false', deletable: 'false', movable: 'false' })
        scratch.params.scratchProgDonnees.lesordres[1].push({ type: 'droite_fusee', editable: 'false', deletable: 'false', movable: 'false' })
        scratch.params.scratchProgDonnees.lesordres[1].push({ type: 'perso', editable: 'false', deletable: 'false', movable: 'false' })
        scratch.params.scratchProgDonnees.lesordres[0].push({ type: 'avance_fusee', editable: 'false', deletable: 'false', movable: 'false' })
        scratch.params.scratchProgDonnees.lesordres[0].push({ type: 'recuperer_fusee', editable: 'false', deletable: 'false', movable: 'false' })
        scratch.params.scratchProgDonnees.lesordres[0].push({ type: 'avance_fusee', editable: 'false', deletable: 'false', movable: 'false' })
        scratch.params.scratchProgDonnees.lesordres[0].push({ type: 'atteri_fusee', editable: 'false', deletable: 'false', movable: 'false' })
        scratch.params.scratchProgDonnees.lesordres[0].push({ type: 'decolle_fusee', editable: 'false', deletable: 'false', movable: 'false' })
        cons = '<b>Programme la définition de  <i>Mon bloc</i> </b> pour que la fusée récupère chaque spationaute et attérisse sur chaque planète.'
        break
    }

    scratch.initFusee(5, 2, 4, 1, 0, elem)
    scratch.remiseAzero = () => {
      scratch.initFusee(5, 2, 4, 1, 0, elem)
    }
    scratch.enonce([], {
      pourInject: { StartScale: 0.75 },
      modeCorrection: {
        type: 'SansComparaison',
        aVerif: [
          { quoi: 'NombreMaxBloc', nombre: lim },
          { quoi: 'QuandAvertir', quand: 'apres' },
          { quoi: 'AnnuleSiAvert', quand: false }]
      },
      dirigeProg: { sequence: ds.Sequence, pasApas: ds.PasAPas, cacheTest: ds.CacheTest }
    })

    // [0] c’est y’a toutes les variables qu’il faut
    // [1] les variables sont bien à la bonne valeur
    scratch.params.scratchProgDonnees.lesflag = [{ ok: false }, { ok: false }, { ok: false }]
    cons += '<br> <i>(tu ne peux utiliser plus de ' + lim + ' blocs au total)</i>'
    const yy = addDefaultTable(scratch.lesdivs.enonceG, 1, 1)[0][0]
    j3pAffiche(yy, null, cons)

    me.finEnonce()
    me.cacheBoutonValider()
  }

  switch (this.etat) {
    case 'enonce':
      if (this.debutDeLaSection) {
        initSection()
      } else {
        enonceMain()
      }
      break // case "enonce":

    case 'correction':
      try {
        const ret = scratch.prepCorrec()
        const xmlDoc2 = ret.xmlDoc2
        const xmlText = ret.xmlText

        if (scratch.params.scratchProgDonnees.repEleve) {
          // Bonne réponse
          this.score++
          scratch.corrigeGood(xmlDoc2, xmlText)
        } else {
          // Pas de bonne réponse
          scratch.corrige(xmlDoc2, xmlText)
        }
        // on veut être rappelé dans l’état navigation
        this.finCorrection('navigation', true)
      } catch (error) {
        console.error(error)
      }
      break // case "correction":

    case 'navigation':

      if (this.sectionTerminee()) {
        scratch.lesdivs.BoutonsSuite.appendChild(this.buttonsElts.sectionSuivante)
        this.buttonsElts.sectionSuivante.addEventListener('click', () => {
          scratch.params.grosStop = true
        })
      } else {
        this.etat = 'enonce'
        scratch.lesdivs.BoutonsSuite.appendChild(this.buttonsElts.suite)
        this.buttonsElts.suite.addEventListener('click', () => {
          scratch.params.grosStop = true
        })
      }
      // Obligatoire
      this.finNavigation()

      break // case "navigation":
  }
}
