import { j3pGetRandomBool, j3pGetRandomInt, j3pShowError, j3pShuffle } from 'src/legacy/core/functions'
import { getNewScratch } from 'src/legacy/outils/scratch/loaders'

export const params = {
  outils: ['mathquill'],
  /**
   * Ce qui est paramétrable dans l’éditeur de graphe
   * @type {LegacySectionParamsParametres}
   */
  parametres: [
    ['nbrepetitions', 4, 'entier', 'Nombre de répétitions de la section'],
    ['nbchances', 0, 'entier', 'Nombre d’essais possibles (illimité si 0)'],
    ['PasAPas', true, 'boolean', "<u>true</u>: L’élève peut lancer son programme en mode 'Pas à pas'."],
    ['Sequence', true, 'boolean', '<u>true</u>: Chaque bloc a une durée d’exécution d’environ 1 seconde.'],
    ['Ligne1', false, 'boolean', '<u>true</u>: La figure peut être une ligne.'],
    ['Ligne2', false, 'boolean', '<u>true</u>: La figure peut être une ligne.'],
    ['Ligne3', false, 'boolean', '<u>true</u>: La figure peut être une ligne.'],
    ['Carre', false, 'boolean', '<u>true</u>: La figure peut être un carré.'],
    ['TriangleEquilateral', false, 'boolean', '<u>true</u>: La figure peut être un triangle équilatéral.'],
    ['Boucle', false, 'boolean', '<u>true</u>: L’élève peut (et doit) utiliser une boucle.'],
    ['AnglesAffiches', false, 'boolean', '<u>true</u>: Les angles de la figure sont affichés'],
    ['CacheTest', false, 'boolean', '<u>true</u>: L’élève ne voit pas l’action de son programme lors de la correction.']
  ]
}

/**
 * section blokblok03
 * @this {Parcours}
 */
export default function main () {
  const me = this
  const stor = me.storage
  const ds = me.donneesSection
  let scratch = stor.scratch

  /// plein de fonction prog
  function psuiv (x0, y0, l, a, na) {
    const ret = {}
    ret.a = a + na
    if (ret.a > 0) ret.a += 360
    if (ret.a >= 360) ret.a -= 360
    ret.x = x0 + l * Math.cos((ret.a) * Math.PI / 180)
    ret.y = y0 + l * Math.sin((ret.a) * Math.PI / 180)
    return ret
  }

  async function initSection () {
    ds.theme = 'zonesAvecImageDeFond'
    scratch = await getNewScratch()
    stor.scratch = scratch
    stor.tabcodeX = []
    stor.Averif = []
    me.validOnEnter = false // ex donneesSection.touche_entree
    scratch.initParams(me, true)
    scratch.testFlag = function () {
    }
    scratch.params.scratchProgDonnees.squizzVar = true
    ds.nbitems = ds.nbetapes * ds.nbrepetitions
    scratch.params.AutoriseSupprimeVariable = false
    scratch.params.AutoriseRenomeVariable = false
    scratch.params.verifDeb = true

    me.construitStructurePage({ structure: 'presentation3' })
    me.stockage = [0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0]

    me.score = 0
    me.afficheTitre('DESSIN - Utiliser une variable')

    let lp = []
    if (ds.Carre) lp.push('carre')
    if (ds.Ligne1) lp.push('ligne1')
    if (ds.Ligne2) lp.push('ligne2')
    if (ds.Ligne3) lp.push('ligne3')
    if (ds.TriangleEquilateral) lp.push('triangleequi')
    if (ds.Rectangle) lp.push('rectangle')
    if (ds.Parallelogramme) lp.push('parallelo')
    if (ds.Hexagone) lp.push('hexagone')
    if (lp.length === 0) {
      j3pShowError('Paramétrage: Aucune figure à tracer !')
      lp.push('carre')
    }
    const quel1 = (lp.length === 1) && (lp[0] === 'ligne1')
    lp = j3pShuffle(lp)
    stor.lesExos = []
    let yaeul1 = false
    let fozi = 0
    for (let i = 0; i < ds.nbitems; i++) {
      let apush = lp[(i + fozi) % lp.length]
      if (apush === 'ligne1') {
        if (yaeul1 && !quel1) {
          fozi++
          apush = lp[(i + fozi) % lp.length]
        }
        yaeul1 = true
      }
      stor.lesExos.push({ fig: apush })
    }
    //
    const aordre = ['ligne1', 'ligne2', 'ligne3']
    const cocop = []
    for (let i = 0; i < 3; i++) {
      for (let j = stor.lesExos.length - 1; j > -1; j--) {
        if (stor.lesExos[j].fig === aordre[i]) {
          cocop.push(aordre[i])
          stor.lesExos.splice(j, 1)
        }
      }
    }
    for (let i = cocop.length - 1; i > -1; i--) {
      stor.lesExos.push({ fig: cocop[i] })
    }

    enonceMain()
  }

  function enonceMain () {
    me.videLesZones()
    scratch.params.scratchProgDonnees.progdeb = []
    scratch.params.scratchProgDonnees.lesordres = []
    scratch.params.scratchProgDonnees.lesvarbonnes = [{
      nom: 'x',
      val: 0,
      id: 'aaaaaa'
    }]
    scratch.params.scratchProgDonnees.progdeb.push({ type: 'Drapo', editable: 'false', deletable: 'false', movable: 'false', x: '20', y: '20' })
    scratch.params.scratchProgDonnees.progdeb.push({
      type: 'data_setvariableto',
      variable: [{ nom: 'VARIABLE', nomvar: 'x', id: 'aaaaaa' }],
      value: [{
        nom: 'VALUE',
        contenu: {
          operateur: 'operator_random',
          value: [{
            nom: 'NUM1',
            contenu: {
              valeur: 50,
              editable: 'false',
              deletable: 'false',
              movable: 'false'
            },
            editable: 'false',
            deletable: 'false',
            movable: 'false'
          },
          {
            nom: 'NUM2',
            contenu: {
              valeur: 90,
              editable: 'false',
              deletable: 'false',
              movable: 'false'
            },
            editable: 'false',
            deletable: 'false',
            movable: 'false'
          }],
          editable: 'false',
          deletable: 'false',
          movable: 'false'
        },
        editable: 'false',
        deletable: 'false',
        movable: 'false'
      }],
      editable: 'false',
      deletable: 'false',
      movable: 'false'
    })
    scratch.params.scratchProgDonnees.lesordres.push({
      type: 'data_setvariableto',
      variable: [{ nom: 'VARIABLE', nomvar: 'x', id: 'aaaaaa' }],
      value: [{
        nom: 'VALUE',
        contenu: {
          operateur: 'operator_random',
          value: [{ nom: 'NUM1', contenu: { valeur: 50 } },
            { nom: 'NUM2', contenu: { valeur: 90 } }]
        }
      }],
      editable: 'false',
      deletable: 'false',
      movable: 'false'
    })
    stor.figencours = stor.lesExos.pop().fig
    scratch.params.scratchProgDonnees.menu = []
    scratch.params.scratchProgDonnees.menu.push({ type: 'catégorie', nom: 'STYLO', coul1: '#40BF4A', coul2: '#389438', contenu: [] })
    scratch.params.scratchProgDonnees.menu[0].contenu.push({ type: 'poserstylo' })
    scratch.params.scratchProgDonnees.menu[0].contenu.push({ type: 'levestylo' })
    scratch.params.scratchProgDonnees.menu.push({ type: 'catégorie', nom: 'DEPLACEMENTS', coul1: '#4C97FF', coul2: '#3373CC', contenu: [] })
    scratch.params.scratchProgDonnees.menu[1].contenu.push({ type: 'motion_movesteps', value: [{ nom: 'STEPS', contenu: { shadow: 'math_number', val: 50 } }] })
    scratch.params.scratchProgDonnees.menu[1].contenu.push({ type: 'motion_turnright', value: [{ nom: 'DEGREES', contenu: { valeur: 90 } }] })
    scratch.params.scratchProgDonnees.menu[1].contenu.push({ type: 'motion_turnleft', value: [{ nom: 'DEGREES', contenu: { valeur: 90 } }] })
    if (ds.Boucle) {
      scratch.params.scratchProgDonnees.menu.push({ type: 'catégorie', nom: 'CONTROLES', coul1: '#FFAB19', coul2: '#CF8B17', contenu: [] })
      scratch.params.scratchProgDonnees.menu[2].contenu.push({ type: 'control_repeat', value: [{ nom: 'TIMES', contenu: { valeur: 10 } }] })
    }
    scratch.params.scratchProgDonnees.menu.push({ type: 'catégorie', nom: 'VARIABLE', coul1: '#FFA500', coul2: '#3373CC', contenu: [] })
    scratch.params.scratchProgDonnees.menu[scratch.params.scratchProgDonnees.menu.length - 1].contenu.push({ type: 'data_variable', variable: [{ nom: 'VARIABLE', nomvar: 'x', id: 'aaaaaa' }] })
    let cons
    let lim
    switch (stor.figencours) {
      case 'ligne3': {
        lim = 16
        cons = ' pour que le lutin reproduise la ligne brisée <br> en utilisant les longueurs x , y et z choisies par un utilisateur.'
        scratch.params.scratchProgDonnees.lesvarbonnes.push({
          nom: 'y',
          val: 0,
          id: 'bbbbbb'
        })
        scratch.params.scratchProgDonnees.lesvarbonnes.push({
          nom: 'z',
          val: 0,
          id: 'cccccc'
        })
        scratch.params.scratchProgDonnees.progdeb.push({
          type: 'data_setvariableto',
          variable: [{ nom: 'VARIABLE', nomvar: 'y', id: 'bbbbbb' }],
          value: [{
            nom: 'VALUE',
            contenu: {
              operateur: 'operator_random',
              value: [{
                nom: 'NUM1',
                contenu: {
                  valeur: 50,
                  editable: 'false',
                  deletable: 'false',
                  movable: 'false'
                },
                editable: 'false',
                deletable: 'false',
                movable: 'false'
              },
              {
                nom: 'NUM2',
                contenu: {
                  valeur: 90,
                  editable: 'false',
                  deletable: 'false',
                  movable: 'false'
                },
                editable: 'false',
                deletable: 'false',
                movable: 'false'
              }],
              editable: 'false',
              deletable: 'false',
              movable: 'false'
            },
            editable: 'false',
            deletable: 'false',
            movable: 'false'
          }],
          editable: 'false',
          deletable: 'false',
          movable: 'false'
        })
        scratch.params.scratchProgDonnees.lesordres.push({
          type: 'data_setvariableto',
          variable: [{ nom: 'VARIABLE', nomvar: 'y', id: 'bbbbbb' }],
          value: [{
            nom: 'VALUE',
            contenu: {
              operateur: 'operator_random',
              value: [{ nom: 'NUM1', contenu: { valeur: 50 } },
                { nom: 'NUM2', contenu: { valeur: 90 } }]
            }
          }]
        })
        scratch.params.scratchProgDonnees.progdeb.push({
          type: 'data_setvariableto',
          variable: [{ nom: 'VARIABLE', nomvar: 'z', id: 'cccccc' }],
          value: [{
            nom: 'VALUE',
            contenu: {
              operateur: 'operator_random',
              value: [{
                nom: 'NUM1',
                contenu: {
                  valeur: 50,
                  editable: 'false',
                  deletable: 'false',
                  movable: 'false'
                },
                editable: 'false',
                deletable: 'false',
                movable: 'false'
              },
              {
                nom: 'NUM2',
                contenu: {
                  valeur: 90,
                  editable: 'false',
                  deletable: 'false',
                  movable: 'false'
                },
                editable: 'false',
                deletable: 'false',
                movable: 'false'
              }],
              editable: 'false',
              deletable: 'false',
              movable: 'false'
            },
            editable: 'false',
            deletable: 'false',
            movable: 'false'
          }],
          editable: 'false',
          deletable: 'false',
          movable: 'false'
        })
        scratch.params.scratchProgDonnees.lesordres.push({
          type: 'data_setvariableto',
          variable: [{ nom: 'VARIABLE', nomvar: 'z', id: 'cccccc' }],
          value: [{
            nom: 'VALUE',
            contenu: {
              operateur: 'operator_random',
              value: [{ nom: 'NUM1', contenu: { valeur: 50 } },
                { nom: 'NUM2', contenu: { valeur: 90 } }]
            }
          }]
        })
        scratch.params.scratchProgDonnees.menu[scratch.params.scratchProgDonnees.menu.length - 1].contenu.push({ type: 'data_variable', variable: [{ nom: 'VARIABLE', nomvar: 'y', id: 'bbbbbb' }] })
        scratch.params.scratchProgDonnees.menu[scratch.params.scratchProgDonnees.menu.length - 1].contenu.push({ type: 'data_variable', variable: [{ nom: 'VARIABLE', nomvar: 'z', id: 'cccccc' }] })
        const ang = j3pGetRandomInt(70, 110)
        const sens = 0
        const fturn = 'turnleft'
        const fturn2 = 'turnright'
        const yd = 120
        const nang = ang - 180

        const angrad = ang * Math.PI / 180
        let tut
        if (ang > 90) { tut = -1 } else { if (sens === 1) { tut = -1 } else { tut = -1 } }
        scratch.params.figureAfaire = [{ type: 'pointdepart', x1: 50, y1: yd },
          { type: 'line', x1: 50, y1: yd, x2: 73, y2: yd, affichelong: false, spe: 'x' },
          { type: 'line', x1: 76, y1: yd, x2: 79, y2: yd, affichelong: false, spe: 'x' },
          { type: 'line', x1: 82, y1: yd, x2: 85, y2: yd, affichelong: false, spe: 'x' },
          { type: 'line', x1: 100, y1: yd, x2: 100, y2: yd, affichelong: true, spe: 'x' },
          { type: 'line', x1: 115, y1: yd, x2: 118, y2: yd, affichelong: false, spe: 'x' },
          { type: 'line', x1: 121, y1: yd, x2: 124, y2: yd, affichelong: false, spe: 'x' },
          { type: 'line', x1: 127, y1: yd, x2: 150, y2: yd, affichelong: false, spe: 'x' }
        ]
        scratch.params.figureAfaire2 = [{
          x1: () => { return 50 },
          y1: () => { return yd },
          x2: (xx) => { return 50 + xx },
          y2: () => { return yd }
        }, {
          x1: (xx) => { return 50 + xx },
          y1: () => { return yd },
          x2: (xx, yy) => { return psuiv(xx + 50, yd, yy, nang, 0).x },
          y2: (xx, yy) => { return psuiv(xx + 50, yd, yy, nang, 0).y }
        }, {
          x1: (xx, yy) => { return psuiv(xx + 50, yd, yy, nang, 0).x },
          y1: (xx, yy) => { return psuiv(xx + 50, yd, yy, nang, 0).y },
          x2: (xx, yy, zz) => { return psuiv(xx + 50, yd, yy, nang, 0).x + zz },
          y2: (xx, yy) => { return psuiv(xx + 50, yd, yy, nang, 0).y }
        }
        ]
        const npoint = psuiv(150, yd, 80, nang, 0)
        const dec = { x: npoint.x - 150, y: npoint.y - yd }
        scratch.params.figureAfaire.push({ type: 'line', x1: 150, y1: yd, x2: 150 + dec.x / 4, y2: yd + dec.y / 4, affichelong: false, spe: 'y' })
        scratch.params.figureAfaire.push({ type: 'line', x1: 150 + 3 * dec.x / 8, y1: yd + 3 * dec.y / 8, x2: 150 + 5 * dec.x / 16, y2: yd + 5 * dec.y / 16, affichelong: false, spe: 'y' })
        scratch.params.figureAfaire.push({ type: 'line', x1: 150 + 3 * dec.x / 4, y1: yd + 3 * dec.y / 4, x2: npoint.x, y2: npoint.y, affichelong: false, spe: 'y' })
        scratch.params.figureAfaire.push({ type: 'line', x1: npoint.x - 3 * dec.x / 8, y1: npoint.y - 3 * dec.y / 8, x2: npoint.x - 5 * dec.x / 16, y2: npoint.y - 5 * dec.y / 16, affichelong: false, spe: 'y' })
        scratch.params.figureAfaire.push({ type: 'line', x1: npoint.x - dec.x / 2, y1: npoint.y - dec.y / 2, x2: npoint.x - dec.x / 2, y2: npoint.y - dec.y / 2, affichelong: true, spe: 'y' })

        scratch.params.figureAfaire.push({ type: 'line', x1: npoint.x, y1: npoint.y, x2: npoint.x + 23, y2: npoint.y, affichelong: false, spe: 'z' })
        scratch.params.figureAfaire.push({ type: 'line', x1: npoint.x + 26, y1: npoint.y, x2: npoint.x + 29, y2: npoint.y, affichelong: false, spe: 'z' })
        scratch.params.figureAfaire.push({ type: 'line', x1: npoint.x + 32, y1: npoint.y, x2: npoint.x + 35, y2: npoint.y, affichelong: false, spe: 'z' })
        scratch.params.figureAfaire.push({ type: 'line', x1: npoint.x + 50, y1: npoint.y, x2: npoint.x + 50, y2: npoint.y, affichelong: true, spe: 'z' })
        scratch.params.figureAfaire.push({ type: 'line', x1: npoint.x + 65, y1: npoint.y, x2: npoint.x + 68, y2: npoint.y, affichelong: false, spe: 'z' })
        scratch.params.figureAfaire.push({ type: 'line', x1: npoint.x + 71, y1: npoint.y, x2: npoint.x + 73, y2: npoint.y, affichelong: false, spe: 'z' })
        scratch.params.figureAfaire.push({ type: 'line', x1: npoint.x + 77, y1: npoint.y, x2: npoint.x + 100, y2: npoint.y, affichelong: false, spe: 'z' })

        if (ds.AnglesAffiches) {
          scratch.params.figureAfaire.push({ type: 'angle', x1: 50, y1: yd, x2: 150, y2: yd, x3: 150 + tut * Math.cos(angrad) * 80, y3: yd - Math.pow(-1, sens) * Math.sin(angrad) * 80 })
          scratch.params.figureAfaire.push({ type: 'angle', x1: 150, y1: yd, x2: 150 + tut * Math.cos(angrad) * 80, y2: yd - Math.pow(-1, sens) * Math.sin(angrad) * 80, x3: 150 + tut * Math.cos(angrad) * 80 + 100, y3: yd - Math.pow(-1, sens) * Math.sin(angrad) * 80 })
        } else {
          scratch.params.figureAfaire.push({ type: 'angledirect', x1: 50, y1: yd, x2: 150, y2: yd, x3: 150 + tut * Math.cos(angrad) * 80, y3: yd - Math.pow(-1, sens) * Math.sin(angrad) * 80 })
          scratch.params.figureAfaire.push({ type: 'angledirect', x1: 150, y1: yd, x2: 150 + tut * Math.cos(angrad) * 80, y2: yd - Math.pow(-1, sens) * Math.sin(angrad) * 80, x3: 150 + tut * Math.cos(angrad) * 80 + 100, y3: yd - Math.pow(-1, sens) * Math.sin(angrad) * 80 })
        }
        scratch.params.scratchProgDonnees.lesordres.push({ type: 'poserstylo' })
        scratch.params.scratchProgDonnees.lesordres.push({ type: 'motion_movesteps', value: [{ nom: 'STEPS', contenu: { type: 'data_variable', variable: [{ nom: 'VARIABLE', nomvar: 'x', id: 'aaaaaa' }] } }] })
        scratch.params.scratchProgDonnees.lesordres.push({ type: 'motion_' + fturn, value: [{ nom: 'DEGREES', contenu: { valeur: 180 - ang } }] })
        scratch.params.scratchProgDonnees.lesordres.push({ type: 'motion_movesteps', value: [{ nom: 'STEPS', contenu: { type: 'data_variable', variable: [{ nom: 'VARIABLE', nomvar: 'y', id: 'bbbbbb' }] } }] })
        scratch.params.scratchProgDonnees.lesordres.push({ type: 'motion_' + fturn2, value: [{ nom: 'DEGREES', contenu: { valeur: 180 - ang } }] })
        scratch.params.scratchProgDonnees.lesordres.push({ type: 'motion_movesteps', value: [{ nom: 'STEPS', contenu: { type: 'data_variable', variable: [{ nom: 'VARIABLE', nomvar: 'z', id: 'cccccc' }] } }] })
      }
        break
      case 'ligne2': {
        cons = ' pour que le lutin reproduise la ligne brisée, <br> en utilisant les longueurs des variables x et y.'
        lim = 11
        scratch.params.scratchProgDonnees.menu[scratch.params.scratchProgDonnees.menu.length - 1].contenu.push({ type: 'data_variable', variable: [{ nom: 'VARIABLE', nomvar: 'y', id: 'bbbbbb' }] })
        scratch.params.scratchProgDonnees.lesvarbonnes.push({
          nom: 'y',
          val: 0,
          id: 'bbbbbb'
        })
        scratch.params.scratchProgDonnees.progdeb.push({
          type: 'data_setvariableto',
          variable: [{ nom: 'VARIABLE', nomvar: 'y', id: 'bbbbbb' }],
          value: [{
            nom: 'VALUE',
            contenu: {
              operateur: 'operator_random',
              value: [{
                nom: 'NUM1',
                contenu: {
                  valeur: 50,
                  editable: 'false',
                  deletable: 'false',
                  movable: 'false'
                },
                editable: 'false',
                deletable: 'false',
                movable: 'false'
              },
              {
                nom: 'NUM2',
                contenu: {
                  valeur: 90,
                  editable: 'false',
                  deletable: 'false',
                  movable: 'false'
                },
                editable: 'false',
                deletable: 'false',
                movable: 'false'
              }],
              editable: 'false',
              deletable: 'false',
              movable: 'false'
            },
            editable: 'false',
            deletable: 'false',
            movable: 'false'
          }],
          editable: 'false',
          deletable: 'false',
          movable: 'false'
        })
        scratch.params.scratchProgDonnees.lesordres.push({
          type: 'data_setvariableto',
          variable: [{ nom: 'VARIABLE', nomvar: 'y', id: 'bbbbbb' }],
          value: [{
            nom: 'VALUE',
            contenu: {
              operateur: 'operator_random',
              value: [{ nom: 'NUM1', contenu: { valeur: 50 } },
                { nom: 'NUM2', contenu: { valeur: 90 } }]
            }
          }],
          editable: 'false',
          deletable: 'false',
          movable: 'false'
        })
        const ang = j3pGetRandomInt(70, 110)
        const sens = 0
        const fturn = 'turnleft'
        const yd = 120
        const nang = ang - 180

        const angrad = ang * Math.PI / 180
        let tut
        if (ang > 90) { tut = -1 } else { if (sens === 1) { tut = -1 } else { tut = -1 } }
        scratch.params.figureAfaire = [{ type: 'pointdepart', x1: 50, y1: yd },
          { type: 'line', x1: 50, y1: yd, x2: 73, y2: yd, affichelong: false, spe: 'x' },
          { type: 'line', x1: 76, y1: yd, x2: 79, y2: yd, affichelong: false, spe: 'x' },
          { type: 'line', x1: 82, y1: yd, x2: 85, y2: yd, affichelong: false, spe: 'x' },
          { type: 'line', x1: 100, y1: yd, x2: 100, y2: yd, affichelong: true, spe: 'x' },
          { type: 'line', x1: 115, y1: yd, x2: 118, y2: yd, affichelong: false, spe: 'x' },
          { type: 'line', x1: 121, y1: yd, x2: 124, y2: yd, affichelong: false, spe: 'x' },
          { type: 'line', x1: 127, y1: yd, x2: 150, y2: yd, affichelong: false, spe: 'x' }
        ]
        scratch.params.figureAfaire2 = [{
          x1: () => { return 50 },
          y1: () => { return yd },
          x2: (xx) => { return 50 + xx },
          y2: () => { return yd }
        }, {
          x1: (xx) => { return 50 + xx },
          y1: () => { return yd },
          x2: (xx, yy) => { return psuiv(xx + 50, yd, yy, nang, 0).x },
          y2: (xx, yy) => { return psuiv(xx + 50, yd, yy, nang, 0).y }
        }
        ]
        const npoint = psuiv(150, yd, 80, nang, 0)
        const dec = { x: npoint.x - 150, y: npoint.y - yd }
        scratch.params.figureAfaire.push({ type: 'line', x1: 150, y1: yd, x2: 150 + dec.x / 4, y2: yd + dec.y / 4, affichelong: false, spe: 'y' })
        scratch.params.figureAfaire.push({ type: 'line', x1: 150 + 3 * dec.x / 8, y1: yd + 3 * dec.y / 8, x2: 150 + 5 * dec.x / 16, y2: yd + 5 * dec.y / 16, affichelong: false, spe: 'y' })
        scratch.params.figureAfaire.push({ type: 'line', x1: 150 + 3 * dec.x / 4, y1: yd + 3 * dec.y / 4, x2: npoint.x, y2: npoint.y, affichelong: false, spe: 'y' })
        scratch.params.figureAfaire.push({ type: 'line', x1: npoint.x - 3 * dec.x / 8, y1: npoint.y - 3 * dec.y / 8, x2: npoint.x - 5 * dec.x / 16, y2: npoint.y - 5 * dec.y / 16, affichelong: false, spe: 'y' })
        scratch.params.figureAfaire.push({ type: 'line', x1: npoint.x - dec.x / 2, y1: npoint.y - dec.y / 2, x2: npoint.x - dec.x / 2, y2: npoint.y - dec.y / 2, affichelong: true, spe: 'y' })

        if (ds.AnglesAffiches) {
          scratch.params.figureAfaire.push({ type: 'angle', x1: 50, y1: yd, x2: 150, y2: yd, x3: 150 + tut * Math.cos(angrad) * 80, y3: yd - Math.pow(-1, sens) * Math.sin(angrad) * 80 })
        } else {
          scratch.params.figureAfaire.push({ type: 'angledirect', x1: 50, y1: yd, x2: 150, y2: yd, x3: 150 + tut * Math.cos(angrad) * 80, y3: yd - Math.pow(-1, sens) * Math.sin(angrad) * 80 })
        }
        scratch.params.scratchProgDonnees.lesordres.push({ type: 'poserstylo' })
        scratch.params.scratchProgDonnees.lesordres.push({ type: 'motion_movesteps', value: [{ nom: 'STEPS', contenu: { type: 'data_variable', variable: [{ nom: 'VARIABLE', nomvar: 'x', id: 'aaaaaa' }] } }] })
        scratch.params.scratchProgDonnees.lesordres.push({ type: 'motion_' + fturn, value: [{ nom: 'DEGREES', contenu: { valeur: 180 - ang } }] })
        scratch.params.scratchProgDonnees.lesordres.push({ type: 'motion_movesteps', value: [{ nom: 'STEPS', contenu: { type: 'data_variable', variable: [{ nom: 'VARIABLE', nomvar: 'y', id: 'bbbbbb' }] } }] })
      }
        break
      case 'ligne1': {
        lim = 6
        const sens = j3pGetRandomInt(0, 1)
        let yd = 120

        if (sens !== 0) {
          yd = 30
        }
        scratch.params.figureAfaire = [{ type: 'pointdepart', x1: 50, y1: yd },
          { type: 'line', x1: 50, y1: yd, x2: 73, y2: yd, affichelong: false, spe: 'x' },
          { type: 'line', x1: 76, y1: yd, x2: 79, y2: yd, affichelong: false, spe: 'x' },
          { type: 'line', x1: 82, y1: yd, x2: 85, y2: yd, affichelong: false, spe: 'x' },
          { type: 'line', x1: 100, y1: yd, x2: 100, y2: yd, affichelong: true, spe: 'x' },
          { type: 'line', x1: 115, y1: yd, x2: 118, y2: yd, affichelong: false, spe: 'x' },
          { type: 'line', x1: 121, y1: yd, x2: 124, y2: yd, affichelong: false, spe: 'x' },
          { type: 'line', x1: 127, y1: yd, x2: 150, y2: yd, affichelong: false, spe: 'x' }
        ]
        scratch.params.figureAfaire2 = [{
          x1: () => { return 50 },
          y1: () => { return yd },
          x2: (xx) => { return 50 + xx },
          y2: () => { return yd }
        }]
        cons = ' pour que le lutin trace une ligne <br> dont la longeur est donnée par la variable x.'
        scratch.params.scratchProgDonnees.lesordres.push({ type: 'poserstylo' })
        scratch.params.scratchProgDonnees.lesordres.push({ type: 'motion_movesteps', value: [{ nom: 'STEPS', contenu: { type: 'data_variable', variable: [{ nom: 'VARIABLE', nomvar: 'x', id: 'aaaaaa' }] } }] })
      }
        break
      case 'carre': {
        cons = ' pour que le lutin construise un carré <br> dont la longueur est donnée par la variable x.'
        const sensTrigo = j3pGetRandomBool()
        const xdeb = 100
        let fturn, ttt, yd
        if (sensTrigo) {
          fturn = 'turnleft'
          ttt = -1
          yd = 120
        } else {
          fturn = 'turnright'
          yd = 40
          ttt = +1
        }

        // le carré de base
        scratch.params.figureAfaire = [
          { type: 'pointdepart', x1: xdeb, y1: yd },
          { type: 'line', x1: 100, y1: yd, x2: 123, y2: yd, affichelong: false, spe: 'x' },
          { type: 'line', x1: 126, y1: yd, x2: 129, y2: yd, affichelong: false, spe: 'x' },
          { type: 'line', x1: 132, y1: yd, x2: 135, y2: yd, affichelong: false, spe: 'x' },
          { type: 'line', x1: 150, y1: yd, x2: 150, y2: yd, affichelong: true, spe: 'x' },
          { type: 'line', x1: 165, y1: yd, x2: 168, y2: yd, affichelong: false, spe: 'x' },
          { type: 'line', x1: 171, y1: yd, x2: 174, y2: yd, affichelong: false, spe: 'x' },
          { type: 'line', x1: 177, y1: yd, x2: 200, y2: yd, affichelong: false, spe: 'x' },
          { type: 'line', x1: xdeb + 100, y1: yd, x2: xdeb + 100, y2: yd + ttt * 100, affichelong: false },
          { type: 'line', x1: xdeb + 100, y1: yd + ttt * 100, x2: xdeb, y2: yd + ttt * 100, affichelong: false },
          { type: 'line', x1: xdeb, y1: yd + ttt * 100, x2: xdeb, y2: yd, affichelong: false }

        ]
        scratch.params.figureAfaire2 = [{
          x1: () => { return 100 },
          y1: () => { return yd },
          x2: (xx) => { return 100 + xx },
          y2: () => { return yd }
        }, {
          x1: (xx) => { return 100 + xx },
          y1: () => { return yd },
          x2: (xx) => { return 100 + xx },
          y2: (xx) => { return yd + ttt * xx }
        }, {
          x1: (xx) => { return 100 + xx },
          y1: (xx) => { return yd + ttt * xx },
          x2: () => { return 100 },
          y2: (xx) => { return yd + ttt * xx }
        }, {
          x1: () => { return 100 },
          y1: (xx) => { return yd + ttt * xx },
          x2: () => { return 100 },
          y2: () => { return yd }
        }
        ]
        if (ds.AnglesAffiches) {
          // on affiche les angles sur le modèle
          scratch.params.figureAfaire.push({
            type: 'angle',
            x1: xdeb,
            y1: yd,
            x2: xdeb + 100,
            y2: yd,
            x3: xdeb + 100,
            y3: yd + ttt * 100
          })
          scratch.params.figureAfaire.push({
            type: 'angle',
            x1: xdeb + 100,
            y1: yd,
            x2: xdeb + 100,
            y2: yd + ttt * 100,
            x3: xdeb,
            y3: yd + ttt * 100
          })
          scratch.params.figureAfaire.push({
            type: 'angle',
            x1: xdeb + 100,
            y1: yd + ttt * 100,
            x2: xdeb,
            y2: yd + ttt * 100,
            x3: xdeb,
            y3: yd
          })
          scratch.params.figureAfaire.push({
            type: 'angle',
            x1: xdeb,
            y1: yd + ttt * 100,
            x2: xdeb,
            y2: yd,
            x3: xdeb + 100,
            y3: yd
          })
        }
        scratch.params.scratchProgDonnees.lesordres.push({ type: 'poserstylo' })
        if (!ds.Boucle) {
          lim = 15
          scratch.params.scratchProgDonnees.lesordres.push({ type: 'motion_movesteps', value: [{ nom: 'STEPS', contenu: { type: 'data_variable', variable: [{ nom: 'VARIABLE', nomvar: 'x', id: 'aaaaaa' }] } }] })
          scratch.params.scratchProgDonnees.lesordres.push({ type: 'motion_' + fturn, value: [{ nom: 'DEGREES', contenu: { valeur: 90 } }] })
          scratch.params.scratchProgDonnees.lesordres.push({ type: 'motion_movesteps', value: [{ nom: 'STEPS', contenu: { type: 'data_variable', variable: [{ nom: 'VARIABLE', nomvar: 'x', id: 'aaaaaa' }] } }] })
          scratch.params.scratchProgDonnees.lesordres.push({ type: 'motion_' + fturn, value: [{ nom: 'DEGREES', contenu: { valeur: 90 } }] })
          scratch.params.scratchProgDonnees.lesordres.push({ type: 'motion_movesteps', value: [{ nom: 'STEPS', contenu: { type: 'data_variable', variable: [{ nom: 'VARIABLE', nomvar: 'x', id: 'aaaaaa' }] } }] })
          scratch.params.scratchProgDonnees.lesordres.push({ type: 'motion_' + fturn, value: [{ nom: 'DEGREES', contenu: { valeur: 90 } }] })
          scratch.params.scratchProgDonnees.lesordres.push({ type: 'motion_movesteps', value: [{ nom: 'STEPS', contenu: { type: 'data_variable', variable: [{ nom: 'VARIABLE', nomvar: 'x', id: 'aaaaaa' }] } }] })
        } else {
          lim = 8
          const laboucle = []
          laboucle.push({ type: 'motion_movesteps', value: [{ nom: 'STEPS', contenu: { type: 'data_variable', variable: [{ nom: 'VARIABLE', nomvar: 'x', id: 'aaaaaa' }] } }] })
          laboucle.push({ type: 'motion_' + fturn, value: [{ nom: 'DEGREES', contenu: { valeur: 90 } }] })
          scratch.params.scratchProgDonnees.lesordres.push({
            type: 'control_repeat',
            value: [{ nom: 'TIMES', contenu: { valeur: 4 } }],
            statement: [{ nom: 'SUBSTACK', contenu: laboucle }]
          })
        }
      }
        break
      case 'triangleequi': {
        cons = ' pour que le lutin construise un triangle équilatéral <br> dont la longueur est donnée par la variable x.'
        const sensTrigo = j3pGetRandomBool()
        const xdeb = 100
        let fturn, ttt, yd, na
        if (sensTrigo) {
          fturn = 'turnleft'
          ttt = -1
          yd = 120
          na = -120
        } else {
          fturn = 'turnright'
          yd = 40
          ttt = +1
          na = 120
        }

        // le carré de base
        scratch.params.figureAfaire = [
          { type: 'pointdepart', x1: xdeb, y1: yd },
          { type: 'line', x1: 100, y1: yd, x2: 123, y2: yd, affichelong: false, spe: 'x' },
          { type: 'line', x1: 126, y1: yd, x2: 129, y2: yd, affichelong: false, spe: 'x' },
          { type: 'line', x1: 132, y1: yd, x2: 135, y2: yd, affichelong: false, spe: 'x' },
          { type: 'line', x1: 150, y1: yd, x2: 150, y2: yd, affichelong: true, spe: 'x' },
          { type: 'line', x1: 165, y1: yd, x2: 168, y2: yd, affichelong: false, spe: 'x' },
          { type: 'line', x1: 171, y1: yd, x2: 174, y2: yd, affichelong: false, spe: 'x' },
          { type: 'line', x1: 177, y1: yd, x2: 200, y2: yd, affichelong: false, spe: 'x' },
          { type: 'line', x1: 200, y1: yd, x2: psuiv(200, yd, 100, na, 0).x, y2: psuiv(200, yd, 100, na, 0).y, affichelong: false },
          { type: 'line', x1: 100, y1: yd, x2: psuiv(200, yd, 100, na, 0).x, y2: psuiv(200, yd, 100, na, 0).y, affichelong: false }
        ]

        scratch.params.figureAfaire2 = [{
          x1: () => { return 100 },
          y1: () => { return yd },
          x2: (xx) => { return 100 + xx },
          y2: () => { return yd }
        }, {
          x1: (xx) => { return 100 + xx },
          y1: () => { return yd },
          x2: (xx) => { return psuiv(100 + xx, yd, xx, na, 0).x },
          y2: (xx) => { return psuiv(100 + xx, yd, xx, na, 0).y }
        }, {
          x1: (xx) => { return psuiv(100 + xx, yd, xx, na, 0).x },
          y1: (xx) => { return psuiv(100 + xx, yd, xx, na, 0).y },
          x2: () => { return 100 },
          y2: () => { return yd }
        }
        ]
        if (ds.AnglesAffiches) {
          scratch.params.figureAfaire.push({ type: 'angle', x1: 100, y1: yd, x2: 100 + 100, y2: yd, x3: 100 + 100 * Math.cos(60 * Math.PI / 180), y3: yd + 100 * Math.sin(60 * Math.PI / 180) * ttt })
          scratch.params.figureAfaire.push({ type: 'angle', x2: 100, y2: yd, x3: 100 + 100, y3: yd, x1: 100 + 100 * Math.cos(60 * Math.PI / 180), y1: yd + 100 * Math.sin(60 * Math.PI / 180) * ttt })
          scratch.params.figureAfaire.push({ type: 'angle', x3: 100, y3: yd, x1: 100 + 100, y1: yd, x2: 100 + 100 * Math.cos(60 * Math.PI / 180), y2: yd + 100 * Math.sin(60 * Math.PI / 180) * ttt })
        }
        scratch.params.scratchProgDonnees.lesordres.push({ type: 'poserstylo' })
        if (!ds.Boucle) {
          lim = 12
          scratch.params.scratchProgDonnees.lesordres.push({ type: 'motion_movesteps', value: [{ nom: 'STEPS', contenu: { type: 'data_variable', variable: [{ nom: 'VARIABLE', nomvar: 'x', id: 'aaaaaa' }] } }] })
          scratch.params.scratchProgDonnees.lesordres.push({ type: 'motion_' + fturn, value: [{ nom: 'DEGREES', contenu: { valeur: 120 } }] })
          scratch.params.scratchProgDonnees.lesordres.push({ type: 'motion_movesteps', value: [{ nom: 'STEPS', contenu: { type: 'data_variable', variable: [{ nom: 'VARIABLE', nomvar: 'x', id: 'aaaaaa' }] } }] })
          scratch.params.scratchProgDonnees.lesordres.push({ type: 'motion_' + fturn, value: [{ nom: 'DEGREES', contenu: { valeur: 120 } }] })
          scratch.params.scratchProgDonnees.lesordres.push({ type: 'motion_movesteps', value: [{ nom: 'STEPS', contenu: { type: 'data_variable', variable: [{ nom: 'VARIABLE', nomvar: 'x', id: 'aaaaaa' }] } }] })
        } else {
          lim = 8
          const laboucle = []
          laboucle.push({ type: 'motion_movesteps', value: [{ nom: 'STEPS', contenu: { type: 'data_variable', variable: [{ nom: 'VARIABLE', nomvar: 'x', id: 'aaaaaa' }] } }] })
          laboucle.push({ type: 'motion_' + fturn, value: [{ nom: 'DEGREES', contenu: { valeur: 120 } }] })
          scratch.params.scratchProgDonnees.lesordres.push({
            type: 'control_repeat',
            value: [{ nom: 'TIMES', contenu: { valeur: 3 } }],
            statement: [{ nom: 'SUBSTACK', contenu: laboucle }]
          })
        }
      }
        break
    }
    const maxblok = ' <i>(' + lim + ' blocs max)</i>'
    scratch.enonceDessin('<b>Complète le programme</b> ' + cons + maxblok, {
      pourInject: { StartScale: 0.55 },
      modeCorrection: {
        type: 'SansComparaison',
        aVerif: []
      },
      dirigeProg: { sequence: ds.Sequence, pasApas: ds.PasAPas, cacheTest: ds.CacheTest }
    })
    // [0] c’est y’a toutes les variables qu’il faut
    // [1] les variables sont bien à la bonne valeur
    scratch.params.scratchProgDonnees.lesflag = []
    scratch.params.scratchProgDonnees.Averif = [
      { quoi: 'NombreMaxBloc', nombre: lim },
      { quoi: 'QuandAvertir', quand: 'apres' },
      { quoi: 'AnnuleSiAvert', quand: false }]
    me.finEnonce()
    me.cacheBoutonValider()
  }

  switch (this.etat) {
    case 'enonce':
      if (this.debutDeLaSection) {
        initSection()
      } else {
        enonceMain()
      }
      break // case "enonce":

    case 'correction':
      try {
        const ret = scratch.prepCorrec()
        const xmlDoc2 = ret.xmlDoc2
        const xmlText = ret.xmlText

        if (scratch.params.scratchProgDonnees.repEleve) {
          // Bonne réponse
          this.score++
          scratch.corrigeGood(xmlDoc2, xmlText)
        } else {
          // Pas de bonne réponse
          scratch.corrige(xmlDoc2, xmlText)
        }
        // on veut être rappelé dans l’état navigation
        this.finCorrection('navigation', true)
      } catch (error) {
        console.error(error)
      }
      break // case "correction":

    case 'navigation':

      if (this.sectionTerminee()) {
        scratch.lesdivs.BoutonsSuite.appendChild(this.buttonsElts.sectionSuivante)
        this.buttonsElts.sectionSuivante.addEventListener('click', () => {
          scratch.params.grosStop = true
        })
      } else {
        this.etat = 'enonce'
        scratch.lesdivs.BoutonsSuite.appendChild(this.buttonsElts.suite)
        this.buttonsElts.suite.addEventListener('click', () => {
          scratch.params.grosStop = true
        })
      }
      // Obligatoire
      this.finNavigation()

      break // case "navigation":
  }
}
