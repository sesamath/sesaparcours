import { j3pGetRandomBool, j3pGetRandomInt, j3pShowError, j3pShuffle } from 'src/legacy/core/functions'
import { getNewScratch } from 'src/legacy/outils/scratch/loaders'

export const params = {
  outils: ['mathquill'],
  /**
   * Ce qui est paramétrable dans l’éditeur de graphe
   * @type {LegacySectionParamsParametres}
   */
  parametres: [
    ['nbrepetitions', 4, 'entier', 'Nombre de répétitions de la section'],
    ['nbchances', 0, 'entier', 'Nombre d’essais possibles (illimité si 0)'],
    ['PasAPas', true, 'boolean', "<u>true</u>: L’élève peut lancer son programme en mode 'Pas à pas'."],
    ['Sequence', true, 'boolean', '<u>true</u>: Chaque bloc a une durée d’exécution d’environ 1 seconde.'],
    ['Ligne', false, 'boolean', '<u>true</u>: La figure peut être une ligne.'],
    ['Carre', false, 'boolean', '<u>true</u>: La figure peut être un carré.'],
    ['TriangleEquilateral', false, 'boolean', '<u>true</u>: La figure peut être un triangle équilatéral.'],
    ['Boucle', false, 'boolean', '<u>true</u>: L’élève peut (et doit) utiliser une boucle.'],
    ['variable', false, 'boolean', '<u>true</u>: L’élève peut (et doit) utiliser une variable.'],
    ['AnglesAffiches', false, 'boolean', '<u>true</u>: Les angles de la figure sont affichés'],
    ['CacheTest', false, 'boolean', '<u>true</u>: L’élève ne voit pas l’action de son programme lors de la correction.']
  ]
}

/**
 * section blokblok02
 * @this {Parcours}
 */
export default function main () {
  const me = this
  const stor = me.storage
  const ds = me.donneesSection
  let scratch = stor.scratch

  /// plein de fonction prog
  function psuiv (x0, y0, l, a, na) {
    const ret = {}
    ret.a = a + na
    if (ret.a > 0) ret.a += 360
    if (ret.a >= 360) ret.a -= 360
    ret.x = x0 + l * Math.cos((ret.a) * Math.PI / 180)
    ret.y = y0 + l * Math.sin((ret.a) * Math.PI / 180)
    return ret
  }

  async function initSection () {
    ds.theme = 'zonesAvecImageDeFond'
    scratch = await getNewScratch()
    stor.scratch = scratch
    stor.tabcodeX = []
    stor.Averif = []
    me.validOnEnter = false // ex donneesSection.touche_entree
    scratch.initParams(me, true)
    scratch.testFlag = function () {
    }
    scratch.params.scratchProgDonnees.squizzVar = true
    ds.nbitems = ds.nbetapes * ds.nbrepetitions
    scratch.params.AutoriseSupprimeVariable = false
    scratch.params.AutoriseRenomeVariable = false
    scratch.params.verifDeb = true

    me.construitStructurePage({ structure: 'presentation3', ratioGauche: 0.6 })
    me.stockage = [0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0]

    me.score = 0
    me.afficheTitre('DESSIN - Utiliser un bloc personnalisé')
    if (ds.variable) {
      scratch.params.scratchProgDonnees.lesvarbonnes = [{ nom: 'variable', val: 'vide', id: 'bbbbbb' }]
    } else {
      scratch.params.scratchProgDonnees.lesvarbonnes = []
    }

    let lp = []
    if (ds.Carre) lp.push('carre')
    if (ds.Ligne) lp.push('ligne')
    if (ds.TriangleEquilateral) lp.push('triangleequi')
    if (ds.Rectangle) lp.push('rectangle')
    if (ds.Parallelogramme) lp.push('parallelo')
    if (ds.Hexagone) lp.push('hexagone')
    if (lp.length === 0) {
      j3pShowError('Paramétrage: Aucune figure à tracer !')
      lp.push('carre')
    }
    lp = j3pShuffle(lp)
    stor.lesExos = []
    for (let i = 0; i < ds.nbitems; i++) {
      stor.lesExos.push({ fig: lp[i % lp.length] })
    }

    scratch.params.scratchProgDonnees.menu = []
    scratch.params.scratchProgDonnees.menu.push({ type: 'catégorie', nom: 'STYLO', coul1: '#40BF4A', coul2: '#389438', contenu: [] })
    scratch.params.scratchProgDonnees.menu[0].contenu.push({ type: 'poserstylo' })
    scratch.params.scratchProgDonnees.menu[0].contenu.push({ type: 'levestylo' })
    scratch.params.scratchProgDonnees.menu.push({ type: 'catégorie', nom: 'DEPLACEMENTS', coul1: '#4C97FF', coul2: '#3373CC', contenu: [] })
    scratch.params.scratchProgDonnees.menu[1].contenu.push({ type: 'motion_movesteps', value: [{ nom: 'STEPS', contenu: { valeur: 50 } }] })
    scratch.params.scratchProgDonnees.menu[1].contenu.push({ type: 'motion_turnright', value: [{ nom: 'DEGREES', contenu: { valeur: 90 } }] })
    scratch.params.scratchProgDonnees.menu[1].contenu.push({ type: 'motion_turnleft', value: [{ nom: 'DEGREES', contenu: { valeur: 90 } }] })
    if (ds.Boucle) {
      scratch.params.scratchProgDonnees.menu.push({ type: 'catégorie', nom: 'CONTROLES', coul1: '#FFAB19', coul2: '#CF8B17', contenu: [] })
      scratch.params.scratchProgDonnees.menu[2].contenu.push({ type: 'control_repeat', value: [{ nom: 'TIMES', contenu: { valeur: 10 } }] })
    }
    if (ds.variable) {
      scratch.params.scratchProgDonnees.menu.push({ type: 'catégorie', nom: 'VARIABLE', coul1: '#FFA500', coul2: '#3373CC', contenu: [] })
      scratch.params.scratchProgDonnees.menu[scratch.params.scratchProgDonnees.menu.length - 1].contenu.push({ type: 'data_variable', variable: [{ nom: 'VARIABLE', nomvar: 'variable', id: 'bbbbbb' }] })
    }
    enonceMain()
  }

  function enonceMain () {
    me.videLesZones()
    scratch.params.scratchProgDonnees.progdeb = []
    scratch.params.scratchProgDonnees.lesordres = []
    let dou = 200
    if (ds.variable) dou = 280
    scratch.params.scratchProgDonnees.progdeb.push([{ type: 'MAkePerso', editable: 'false', deletable: 'false', movable: 'false', x: dou, y: '20' }])
    scratch.params.scratchProgDonnees.lesordres.push([{ type: 'MAkePerso', editable: 'false', deletable: 'false', movable: 'false', x: dou, y: '20' }])
    scratch.params.scratchProgDonnees.progdeb.push([{ type: 'Drapo', editable: 'false', deletable: 'false', movable: 'false', x: '20', y: '20' }])
    scratch.params.scratchProgDonnees.lesordres.push([])
    scratch.params.scratchProgDonnees.empDec = false

    stor.figencours = stor.lesExos.pop().fig

    let lim, long1, long2, long3

    const Sc = ds.variable ? 0.6 : 0.75
    switch (stor.figencours) {
      case 'ligne': {
        const limbas = ds.AnglesAffiches ? 50 : 31
        long1 = j3pGetRandomInt(limbas, 60)
        do {
          long2 = j3pGetRandomInt(limbas, 60)
        } while (Math.abs(long2 - long1) < 5)
        do {
          long3 = j3pGetRandomInt(limbas, 60)
        } while (Math.abs(long2 - long3) < 5)
        if (!ds.variable) {
          long2 = long1; long3 = long1
        }
        const xdeb = 20
        const yd = 100

        // le carré de base
        scratch.params.figureAfaire = [
          { type: 'pointdepart', x1: xdeb, y1: yd },
          { type: 'pointdepart2', x1: xdeb + 100, y1: yd },
          { type: 'pointdepart3', x1: xdeb + 220, y1: yd },
          { type: 'line', x1: xdeb, y1: yd, x2: xdeb + long1, y2: yd, affichelong: true },
          { type: 'line', x1: (xdeb + 100), y1: yd, x2: (xdeb + 100) + long2, y2: yd, affichelong: true },
          { type: 'line', x1: (xdeb + 220), y1: yd, x2: (xdeb + 220) + long3, y2: yd, affichelong: true }
        ]
        scratch.params.scratchProgDonnees.lesordres[0].push({ type: 'poserstylo' })
        const v1 = ds.variable ? { type: 'data_variable', variable: [{ nom: 'VARIABLE', nomvar: 'variable', id: 'bbbbbb' }] } : { valeur: long1 }
        if (!ds.variable) {
          lim = 10
        } else {
          lim = 14
        }
        scratch.params.scratchProgDonnees.lesordres[0].push({ type: 'motion_movesteps', value: [{ nom: 'STEPS', contenu: v1 }] })
        scratch.params.scratchProgDonnees.lesordres[0].push({ type: 'levestylo' })
      }
        break
      case 'carre': {
        const limbas = ds.AnglesAffiches ? 50 : 31
        long1 = j3pGetRandomInt(limbas, 60)
        do {
          long2 = j3pGetRandomInt(limbas, 60)
        } while (Math.abs(long2 - long1) < 5)
        do {
          long3 = j3pGetRandomInt(limbas, 60)
        } while (Math.abs(long2 - long3) < 5)
        if (!ds.variable) {
          long2 = long1; long3 = long1
        }
        const sensTrigo = j3pGetRandomBool()
        const xdeb = 20
        let fturn, ttt, yd
        if (sensTrigo) {
          fturn = 'turnleft'
          ttt = -1
          yd = 120
        } else {
          fturn = 'turnright'
          yd = 40
          ttt = +1
        }

        // le carré de base
        scratch.params.figureAfaire = [
          { type: 'pointdepart', x1: xdeb, y1: yd },
          { type: 'pointdepart2', x1: xdeb + 100, y1: yd },
          { type: 'pointdepart3', x1: xdeb + 220, y1: yd },
          { type: 'line', x1: xdeb, y1: yd, x2: xdeb + long1, y2: yd, affichelong: true },
          { type: 'line', x1: xdeb + long1, y1: yd, x2: xdeb + long1, y2: yd + ttt * long1, affichelong: false },
          { type: 'line', x1: xdeb + long1, y1: yd + ttt * long1, x2: xdeb, y2: yd + ttt * long1, affichelong: false },
          { type: 'line', x1: xdeb, y1: yd + ttt * long1, x2: xdeb, y2: yd, affichelong: false },

          { type: 'line', x1: (xdeb + 100), y1: yd, x2: (xdeb + 100) + long2, y2: yd, affichelong: true },
          { type: 'line', x1: (xdeb + 100) + long2, y1: yd, x2: (xdeb + 100) + long2, y2: yd + ttt * long2, affichelong: false },
          { type: 'line', x1: (xdeb + 100) + long2, y1: yd + ttt * long2, x2: (xdeb + 100), y2: yd + ttt * long2, affichelong: false },
          { type: 'line', x1: (xdeb + 100), y1: yd + ttt * long2, x2: (xdeb + 100), y2: yd, affichelong: false },

          { type: 'line', x1: (xdeb + 220), y1: yd, x2: (xdeb + 220) + long3, y2: yd, affichelong: true },
          { type: 'line', x1: (xdeb + 220) + long3, y1: yd, x2: (xdeb + 220) + long3, y2: yd + ttt * long3, affichelong: false },
          { type: 'line', x1: (xdeb + 220) + long3, y1: yd + ttt * long3, x2: (xdeb + 220), y2: yd + ttt * long3, affichelong: false },
          { type: 'line', x1: (xdeb + 220), y1: yd + ttt * long3, x2: (xdeb + 220), y2: yd, affichelong: false }
        ]
        if (ds.AnglesAffiches) {
          // on affiche les angles sur le modèle
          scratch.params.figureAfaire.push({
            type: 'angle',
            x1: xdeb,
            y1: yd,
            x2: xdeb + long1,
            y2: yd,
            x3: xdeb + long1,
            y3: yd + ttt * long1
          })
          scratch.params.figureAfaire.push({
            type: 'angle',
            x1: xdeb + long1,
            y1: yd,
            x2: xdeb + long1,
            y2: yd + ttt * long1,
            x3: xdeb,
            y3: yd + ttt * long1
          })
          scratch.params.figureAfaire.push({
            type: 'angle',
            x1: xdeb + long1,
            y1: yd + ttt * long1,
            x2: xdeb,
            y2: yd + ttt * long1,
            x3: xdeb,
            y3: yd
          })
          scratch.params.figureAfaire.push({
            type: 'angle',
            x1: xdeb,
            y1: yd + ttt * long1,
            x2: xdeb,
            y2: yd,
            x3: xdeb + long1,
            y3: yd
          })

          scratch.params.figureAfaire.push({
            type: 'angle',
            x1: (xdeb + 100),
            y1: yd,
            x2: (xdeb + 100) + long2,
            y2: yd,
            x3: (xdeb + 100) + long2,
            y3: yd + ttt * long2
          })
          scratch.params.figureAfaire.push({
            type: 'angle',
            x1: (xdeb + 100) + long2,
            y1: yd,
            x2: (xdeb + 100) + long2,
            y2: yd + ttt * long2,
            x3: (xdeb + 100),
            y3: yd + ttt * long2
          })
          scratch.params.figureAfaire.push({
            type: 'angle',
            x1: (xdeb + 100) + long2,
            y1: yd + ttt * long2,
            x2: (xdeb + 100),
            y2: yd + ttt * long2,
            x3: (xdeb + 100),
            y3: yd
          })
          scratch.params.figureAfaire.push({
            type: 'angle',
            x1: (xdeb + 100),
            y1: yd + ttt * long2,
            x2: (xdeb + 100),
            y2: yd,
            x3: (xdeb + 100) + long2,
            y3: yd
          })

          scratch.params.figureAfaire.push({
            type: 'angle',
            x1: (xdeb + 220),
            y1: yd,
            x2: (xdeb + 220) + long3,
            y2: yd,
            x3: (xdeb + 220) + long3,
            y3: yd + ttt * long3
          })
          scratch.params.figureAfaire.push({
            type: 'angle',
            x1: (xdeb + 220) + long3,
            y1: yd,
            x2: (xdeb + 220) + long3,
            y2: yd + ttt * long3,
            x3: (xdeb + 220),
            y3: yd + ttt * long3
          })
          scratch.params.figureAfaire.push({
            type: 'angle',
            x1: (xdeb + 220) + long3,
            y1: yd + ttt * long3,
            x2: (xdeb + 220),
            y2: yd + ttt * long3,
            x3: (xdeb + 220),
            y3: yd
          })
          scratch.params.figureAfaire.push({
            type: 'angle',
            x1: (xdeb + 220),
            y1: yd + ttt * long3,
            x2: (xdeb + 220),
            y2: yd,
            x3: (xdeb + 220) + long3,
            y3: yd
          })
        }

        scratch.params.scratchProgDonnees.lesordres[0].push({ type: 'poserstylo' })
        const v1 = ds.variable ? { type: 'data_variable', variable: [{ nom: 'VARIABLE', nomvar: 'variable', id: 'bbbbbb' }] } : { valeur: long1 }
        if (!ds.Boucle) {
          if (!ds.variable) {
            lim = 17
          } else {
            lim = 24
          }
          scratch.params.scratchProgDonnees.lesordres[0].push({ type: 'motion_movesteps', value: [{ nom: 'STEPS', contenu: v1 }] })
          scratch.params.scratchProgDonnees.lesordres[0].push({ type: 'motion_' + fturn, value: [{ nom: 'DEGREES', contenu: { valeur: 90 } }] })
          scratch.params.scratchProgDonnees.lesordres[0].push({ type: 'motion_movesteps', value: [{ nom: 'STEPS', contenu: v1 }] })
          scratch.params.scratchProgDonnees.lesordres[0].push({ type: 'motion_' + fturn, value: [{ nom: 'DEGREES', contenu: { valeur: 90 } }] })
          scratch.params.scratchProgDonnees.lesordres[0].push({ type: 'motion_movesteps', value: [{ nom: 'STEPS', contenu: v1 }] })
          scratch.params.scratchProgDonnees.lesordres[0].push({ type: 'motion_' + fturn, value: [{ nom: 'DEGREES', contenu: { valeur: 90 } }] })
          scratch.params.scratchProgDonnees.lesordres[0].push({ type: 'motion_movesteps', value: [{ nom: 'STEPS', contenu: v1 }] })
          scratch.params.scratchProgDonnees.lesordres[0].push({ type: 'motion_' + fturn, value: [{ nom: 'DEGREES', contenu: { valeur: 90 } }] })
        } else {
          if (!ds.variable) {
            lim = 12
          } else {
            lim = 16
          }
          const laboucle = []
          laboucle.push({ type: 'motion_movesteps', value: [{ nom: 'STEPS', contenu: v1 }] })
          laboucle.push({ type: 'motion_' + fturn, value: [{ nom: 'DEGREES', contenu: { valeur: 90 } }] })
          scratch.params.scratchProgDonnees.lesordres[0].push({
            type: 'control_repeat',
            value: [{ nom: 'TIMES', contenu: { valeur: 4 } }],
            statement: [{ nom: 'SUBSTACK', contenu: laboucle }]
          })
        }
        scratch.params.scratchProgDonnees.lesordres[0].push({ type: 'levestylo' })
        break
      }
      case 'triangleequi': {
        const limbas = ds.AnglesAffiches ? 56 : 40
        long1 = j3pGetRandomInt(limbas, 60)
        do {
          long2 = j3pGetRandomInt(limbas, 60)
        } while (Math.abs(long2 - long1) < 1)
        do {
          long3 = j3pGetRandomInt(limbas, 57)
        } while (Math.abs(long2 - long3) < 1)
        if (!ds.variable) {
          long2 = long1; long3 = long1
        }
        const sensTrigo = j3pGetRandomBool()
        const xdeb = 20
        let fturn, ttt, yd, tt
        if (sensTrigo) {
          fturn = 'turnleft'
          ttt = -120
          yd = 120
          tt = -1
        } else {
          fturn = 'turnright'
          yd = 40
          ttt = +120
          tt = 1
        }
        scratch.params.figureAfaire = [
          { type: 'pointdepart', x1: xdeb, y1: yd },
          { type: 'pointdepart2', x1: xdeb + 100, y1: yd },
          { type: 'pointdepart3', x1: xdeb + 220, y1: yd }
        ]
        let point = { x: xdeb, y: yd, a: 0 }
        let npoint, an
        for (let i = 0; i < 3; i++) {
          an = ttt
          if (i === 0) an = 0
          npoint = psuiv(point.x, point.y, long1, point.a, an)
          const affichelong = (i === 0)
          scratch.params.figureAfaire.push({ type: 'line', x1: point.x, y1: point.y, x2: npoint.x, y2: npoint.y, affichelong })
          point.x = npoint.x
          point.y = npoint.y
          point.a = npoint.a
        }

        point = { x: (xdeb + 100), y: yd, a: 0 }
        for (let i = 0; i < 3; i++) {
          an = ttt
          if (i === 0) an = 0
          npoint = psuiv(point.x, point.y, long2, point.a, an)
          const affichelong = (i === 0)
          scratch.params.figureAfaire.push({ type: 'line', x1: point.x, y1: point.y, x2: npoint.x, y2: npoint.y, affichelong })
          point.x = npoint.x
          point.y = npoint.y
          point.a = npoint.a
        }

        point = { x: (xdeb + 220), y: yd, a: 0 }
        for (let i = 0; i < 3; i++) {
          an = ttt
          if (i === 0) an = 0
          npoint = psuiv(point.x, point.y, long3, point.a, an)
          const affichelong = (i === 0)
          scratch.params.figureAfaire.push({ type: 'line', x1: point.x, y1: point.y, x2: npoint.x, y2: npoint.y, affichelong })
          point.x = npoint.x
          point.y = npoint.y
          point.a = npoint.a
        }

        if (ds.AnglesAffiches) {
          scratch.params.figureAfaire.push({ type: 'angle', x1: xdeb, y1: yd, x2: xdeb + long1, y2: yd, x3: xdeb + long1 * Math.cos(60 * Math.PI / 180), y3: yd + long1 * Math.sin(60 * Math.PI / 180) * tt })
          scratch.params.figureAfaire.push({ type: 'angle', x2: xdeb, y2: yd, x3: xdeb + long1, y3: yd, x1: xdeb + long1 * Math.cos(60 * Math.PI / 180), y1: yd + long1 * Math.sin(60 * Math.PI / 180) * tt })
          scratch.params.figureAfaire.push({ type: 'angle', x3: xdeb, y3: yd, x1: xdeb + long1, y1: yd, x2: xdeb + long1 * Math.cos(60 * Math.PI / 180), y2: yd + long1 * Math.sin(60 * Math.PI / 180) * tt })

          scratch.params.figureAfaire.push({ type: 'angle', x1: (xdeb + 100), y1: yd, x2: (xdeb + 100) + long2, y2: yd, x3: (xdeb + 100) + long2 * Math.cos(60 * Math.PI / 180), y3: yd + long2 * Math.sin(60 * Math.PI / 180) * tt })
          scratch.params.figureAfaire.push({ type: 'angle', x2: (xdeb + 100), y2: yd, x3: (xdeb + 100) + long2, y3: yd, x1: (xdeb + 100) + long2 * Math.cos(60 * Math.PI / 180), y1: yd + long2 * Math.sin(60 * Math.PI / 180) * tt })
          scratch.params.figureAfaire.push({ type: 'angle', x3: (xdeb + 100), y3: yd, x1: (xdeb + 100) + long2, y1: yd, x2: (xdeb + 100) + long2 * Math.cos(60 * Math.PI / 180), y2: yd + long2 * Math.sin(60 * Math.PI / 180) * tt })

          scratch.params.figureAfaire.push({ type: 'angle', x1: (xdeb + 220), y1: yd, x2: (xdeb + 220) + long3, y2: yd, x3: (xdeb + 220) + long3 * Math.cos(60 * Math.PI / 180), y3: yd + long3 * Math.sin(60 * Math.PI / 180) * tt })
          scratch.params.figureAfaire.push({ type: 'angle', x2: (xdeb + 220), y2: yd, x3: (xdeb + 220) + long3, y3: yd, x1: (xdeb + 220) + long3 * Math.cos(60 * Math.PI / 180), y1: yd + long3 * Math.sin(60 * Math.PI / 180) * tt })
          scratch.params.figureAfaire.push({ type: 'angle', x3: (xdeb + 220), y3: yd, x1: (xdeb + 220) + long3, y1: yd, x2: (xdeb + 220) + long3 * Math.cos(60 * Math.PI / 180), y2: yd + long3 * Math.sin(60 * Math.PI / 180) * tt })
        }

        scratch.params.scratchProgDonnees.lesordres[0].push({ type: 'poserstylo' })
        const v1 = ds.variable ? { type: 'data_variable', variable: [{ nom: 'VARIABLE', nomvar: 'variable', id: 'bbbbbb' }] } : { valeur: long1 }
        if (!ds.Boucle) {
          if (!ds.variable) {
            lim = 15
          } else {
            lim = 21
          }
          scratch.params.scratchProgDonnees.lesordres[0].push({ type: 'motion_movesteps', value: [{ nom: 'STEPS', contenu: v1 }] })
          scratch.params.scratchProgDonnees.lesordres[0].push({ type: 'motion_' + fturn, value: [{ nom: 'DEGREES', contenu: { valeur: 120 } }] })
          scratch.params.scratchProgDonnees.lesordres[0].push({ type: 'motion_movesteps', value: [{ nom: 'STEPS', contenu: v1 }] })
          scratch.params.scratchProgDonnees.lesordres[0].push({ type: 'motion_' + fturn, value: [{ nom: 'DEGREES', contenu: { valeur: 120 } }] })
          scratch.params.scratchProgDonnees.lesordres[0].push({ type: 'motion_movesteps', value: [{ nom: 'STEPS', contenu: v1 }] })
          scratch.params.scratchProgDonnees.lesordres[0].push({ type: 'motion_' + fturn, value: [{ nom: 'DEGREES', contenu: { valeur: 120 } }] })
        } else {
          if (!ds.variable) {
            lim = 12
          } else {
            lim = 16
          }
          const laboucle = []
          laboucle.push({ type: 'motion_movesteps', value: [{ nom: 'STEPS', contenu: v1 }] })
          laboucle.push({ type: 'motion_' + fturn, value: [{ nom: 'DEGREES', contenu: { valeur: 120 } }] })
          scratch.params.scratchProgDonnees.lesordres[0].push({
            type: 'control_repeat',
            value: [{ nom: 'TIMES', contenu: { valeur: 3 } }],
            statement: [{ nom: 'SUBSTACK', contenu: laboucle }]
          })
        }
        scratch.params.scratchProgDonnees.lesordres[0].push({ type: 'levestylo' })
        break
      }
    } // switch

    if (ds.variable) {
      scratch.params.scratchProgDonnees.progdeb[1].push({ type: 'data_setvariableto', editable: 'false', deletable: 'false', movable: 'false', variable: [{ nom: 'VARIABLE', nomvar: 'variable' }], value: [{ nom: 'VALUE', editable: 'false', deletable: 'false', movable: 'false', contenu: { valeur: '20' } }] })
      scratch.params.scratchProgDonnees.lesordres[1].push({ type: 'data_setvariableto', editable: 'false', deletable: 'false', movable: 'false', variable: [{ nom: 'VARIABLE', nomvar: 'variable' }], value: [{ nom: 'VALUE', editable: 'false', deletable: 'false', movable: 'false', contenu: { valeur: long1 } }] })
    }
    scratch.params.scratchProgDonnees.progdeb[1].push({ type: 'perso', editable: 'false', deletable: 'false', movable: 'false' })
    scratch.params.scratchProgDonnees.progdeb[1].push({ type: 'pointV', editable: 'false', deletable: 'false', movable: 'false' })
    scratch.params.scratchProgDonnees.lesordres[1].push({ type: 'perso', editable: 'false', deletable: 'false', movable: 'false' })
    scratch.params.scratchProgDonnees.lesordres[1].push({ type: 'pointV', editable: 'false', deletable: 'false', movable: 'false' })
    if (ds.variable) {
      scratch.params.scratchProgDonnees.progdeb[1].push({ type: 'data_setvariableto', editable: 'false', deletable: 'false', movable: 'false', variable: [{ nom: 'VARIABLE', nomvar: 'variable' }], value: [{ nom: 'VALUE', editable: 'false', deletable: 'false', movable: 'false', contenu: { valeur: '20' } }] })
      scratch.params.scratchProgDonnees.lesordres[1].push({ type: 'data_setvariableto', editable: 'false', deletable: 'false', movable: 'false', variable: [{ nom: 'VARIABLE', nomvar: 'variable' }], value: [{ nom: 'VALUE', editable: 'false', deletable: 'false', movable: 'false', contenu: { valeur: long2 } }] })
    }
    scratch.params.scratchProgDonnees.progdeb[1].push({ type: 'perso', editable: 'false', deletable: 'false', movable: 'false' })
    scratch.params.scratchProgDonnees.progdeb[1].push({ type: 'pointB', editable: 'false', deletable: 'false', movable: 'false' })
    scratch.params.scratchProgDonnees.lesordres[1].push({ type: 'perso', editable: 'false', deletable: 'false', movable: 'false' })
    scratch.params.scratchProgDonnees.lesordres[1].push({ type: 'pointB', editable: 'false', deletable: 'false', movable: 'false' })
    if (ds.variable) {
      scratch.params.scratchProgDonnees.progdeb[1].push({ type: 'data_setvariableto', editable: 'false', deletable: 'false', movable: 'false', variable: [{ nom: 'VARIABLE', nomvar: 'variable' }], value: [{ nom: 'VALUE', editable: 'false', deletable: 'false', movable: 'false', contenu: { valeur: '20' } }] })
      scratch.params.scratchProgDonnees.lesordres[1].push({ type: 'data_setvariableto', editable: 'false', deletable: 'false', movable: 'false', variable: [{ nom: 'VARIABLE', nomvar: 'variable' }], value: [{ nom: 'VALUE', editable: 'false', deletable: 'false', movable: 'false', contenu: { valeur: long3 } }] })
    }
    scratch.params.scratchProgDonnees.progdeb[1].push({ type: 'perso', editable: 'false', deletable: 'false', movable: 'false' })
    scratch.params.scratchProgDonnees.lesordres[1].push({ type: 'perso', editable: 'false', deletable: 'false', movable: 'false' })
    const cons = '<br><i>(tu ne peux utiliser plus de ' + lim + ' blocs au total)</i>'
    const cons2 = ds.variable ? ' et modifie les blocs orange <br>' : ''
    scratch.enonceDessin('<b>Donne la définition du bloc personnalisé ' + cons2 + ' pour que le lutin repoduise le modèle</b>.' + cons, {
      pourInject: { StartScale: Sc },
      modeCorrection: {
        type: 'SansComparaison',
        aVerif: []
      },
      dirigeProg: { sequence: ds.Sequence, pasApas: ds.PasAPas, cacheTest: ds.CacheTest }
    })

    // [0] c’est y’a toutes les variables qu’il faut
    // [1] les variables sont bien à la bonne valeur
    scratch.params.scratchProgDonnees.lesflag = []
    scratch.params.scratchProgDonnees.Averif = [
      { quoi: 'NombreMaxBloc', nombre: lim },
      { quoi: 'QuandAvertir', quand: 'apres' },
      { quoi: 'AnnuleSiAvert', quand: false }]

    me.finEnonce()
    me.cacheBoutonValider()
  }

  switch (this.etat) {
    case 'enonce':
      if (this.debutDeLaSection) {
        initSection()
      } else {
        enonceMain()
      }
      break // case "enonce":

    case 'correction':
      try {
        const ret = scratch.prepCorrec()
        const xmlDoc2 = ret.xmlDoc2
        const xmlText = ret.xmlText

        if (scratch.params.scratchProgDonnees.repEleve) {
          // Bonne réponse
          this.score++
          scratch.corrigeGood(xmlDoc2, xmlText)
        } else {
          // Pas de bonne réponse
          scratch.corrige(xmlDoc2, xmlText)
        }
        // on veut être rappelé dans l’état navigation
        this.finCorrection('navigation', true)
      } catch (error) {
        console.error(error)
      }
      break // case "correction":

    case 'navigation':

      if (this.sectionTerminee()) {
        scratch.lesdivs.BoutonsSuite.appendChild(this.buttonsElts.sectionSuivante)
        this.buttonsElts.sectionSuivante.addEventListener('click', () => {
          scratch.params.grosStop = true
        })
      } else {
        this.etat = 'enonce'
        scratch.lesdivs.BoutonsSuite.appendChild(this.buttonsElts.suite)
        this.buttonsElts.suite.addEventListener('click', () => {
          scratch.params.grosStop = true
        })
      }
      // Obligatoire
      this.finNavigation()

      break // case "navigation":
  }
}
