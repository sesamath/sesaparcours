import { j3pGetRandomInt } from 'src/legacy/core/functions'
import BulleAide from 'src/legacy/outils/bulleAide/BulleAide'
import { j3pAffiche } from 'src/lib/mathquill/functions'
import { addDefaultTable } from 'src/legacy/themes/table'
import { getNewScratch } from 'src/legacy/outils/scratch/loaders'

export const params = {
  outils: ['mathquill'],
  /**
   * Ce qui est paramétrable dans l’éditeur de graphe
   * @type {LegacySectionParamsParametres}
   */
  parametres: [
    ['nbrepetitions', 4, 'entier', 'Nombre de répétitions de la section'],
    ['nbchances', 0, 'entier', 'Nombre d’essais possibles (illimité si 0)'],
    ['PasAPas', false, 'boolean', "<u>true</u>: L’élève peut lancer son programme en mode 'Pas à pas'."],
    ['Sequence', true, 'boolean', '<u>true</u>: Chaque bloc a une durée d’exécution d’environ 1 seconde.'],
    ['Completer', true, 'boolean', '<u>true</u>: Une partie du programme est déjà en place.'],
    ['difficultemin', 1, 'entier', 'Difficulté du programme (minimum 1)'],
    ['difficultemax', 2, 'entier', 'Difficulté du programme (maximum 2)'],
    ['difficulte', 'Progressive', 'liste', 'Gestion de la progression (entre min et max)', ['Aléatoire', 'Progressive']],
    ['CacheTest', false, 'boolean', '<u>true</u>: L’élève ne voit pas l’action de son programme lors d la correction.']
  ]

}
/**
 * section blok11
 * @this {Parcours}
 */
export default function main () {
  const me = this
  const stor = me.storage
  const ds = me.donneesSection
  let scratch = stor.scratch

  async function initSection () {
    ds.theme = 'zonesAvecImageDeFond'
    scratch = await getNewScratch()
    stor.scratch = scratch
    stor.tabcodeX = []
    stor.Averif = []
    stor.afficheEssai = true
    me.validOnEnter = false // ex donneesSection.touche_entree
    scratch.initParams(me)
    scratch.testFlag = function () {

    }
    scratch.testFlagFin = function () {
      let mess = ''
      let buf = ''
      // test utilise que nb ok
      let ok = true
      if (scratch.params.scratchProgDonnees.Valeursutilisees.length === 0) {
        ok = false
        mess = 'Tu ne peux utiliser que des nombres de l’énoncé !'
        buf = '<BR>'
      } else {
        for (let i = 0; i < scratch.params.scratchProgDonnees.Valeursutilisees.length; i++) {
          if (scratch.params.scratchProgDonnees.Valeursautorisees.indexOf(String(scratch.params.scratchProgDonnees.Valeursutilisees[i]).replace(/ /g, '')) === -1) {
            ok = false
            mess = 'Tu ne peux utiliser que des nombres de l’énoncé !'
            buf = '<BR>'
          }
        }
      }

      if (scratch.params.scratchProgDonnees.said[0] !== undefined) {
        if ((scratch.params.scratchProgDonnees.said[0] + '') !== (stor.resultat + '')) {
          mess += buf + 'Ce n’est pas le résultat attendu !'
          ok = false
        }
      } else { ok = false }
      return { ok, mess }
    }
    ds.nbitems = ds.nbetapes * ds.nbrepetitions
    stor.opfait = []
    stor.opfait2 = []

    ds.difficultemin = Math.max(1, ds.difficultemin)
    ds.difficultemax = Math.min(2, ds.difficultemax)
    if (ds.difficultemax < ds.difficultemin) {
      ds.difficultemin = ds.difficultemax
    }

    me.construitStructurePage({ structure: 'presentation3', ratioGauche: 0.6 })
    me.stockage = [0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0]

    me.score = 0
    stor.dejafait = []
    me.afficheTitre('Faire des calculs')

    scratch.params.scratchProgDonnees.menu = []
    scratch.params.scratchProgDonnees.progdeb = []
    scratch.params.scratchProgDonnees.progdeb.push({ type: 'Drapo', editable: 'false', deletable: 'false', movable: 'false', x: '20', y: '20' })

    scratch.params.scratchProgDonnees.menu.push({ type: 'catégorie', nom: 'Opérateurs', coul1: '#22dd22', coul2: '#22dd22', contenu: [] })
    scratch.params.scratchProgDonnees.menu[0].contenu.push({ type: 'operator_add', value: [{ nom: 'NUM1', contenu: { valeur: ' ' } }, { nom: 'NUM2', contenu: { valeur: ' ' } }] })
    scratch.params.scratchProgDonnees.menu[0].contenu.push({ type: 'operator_subtract', value: [{ nom: 'NUM1', contenu: { valeur: ' ' } }, { nom: 'NUM2', contenu: { valeur: ' ' } }] })
    scratch.params.scratchProgDonnees.menu[0].contenu.push({ type: 'operator_multiply', value: [{ nom: 'NUM1', contenu: { valeur: ' ' } }, { nom: 'NUM2', contenu: { valeur: ' ' } }] })
    scratch.params.scratchProgDonnees.menu[0].contenu.push({ type: 'operator_divide', value: [{ nom: 'NUM1', contenu: { valeur: ' ' } }, { nom: 'NUM2', contenu: { valeur: ' ' } }] })
    if (!ds.Completer) {
      scratch.params.scratchProgDonnees.menu[0].contenu.push({ type: 'dire', value: [{ nom: 'DELTA', contenu: { valeur: 'à compléter' } }, { nom: 'TEMPS', contenu: { valeur: 2, editable: 'false', deletable: 'false', movable: 'false' } }] })
    } else {
      scratch.params.scratchProgDonnees.progdeb.push({ type: 'dire', editable: 'false', deletable: 'false', movable: 'false', value: [{ nom: 'DELTA', contenu: { valeur: 'PARTIE A COMPLETER' } }, { nom: 'TEMPS', contenu: { valeur: 2, editable: 'false', deletable: 'false', movable: 'false' } }] })
    }
    scratch.params.blokmenulimite.push({ type: 'dire', nb: 1 })
    enonceMain()
  }

  function enonceMain () {
    me.videLesZones()

    stor.listeid = []
    stor.valeurauto = []
    scratch.params.scratchProgDonnees.Lutins[0].cox = 10
    scratch.params.scratchProgDonnees.Lutins[0].coxbase = 10
    let nbmax = 3

    // les bonnes questions

    stor.diff = j3pGetRandomInt(me.donneesSection.difficultemin, me.donneesSection.difficultemax)
    if (me.donneesSection.difficulte === 'Progressive') {
      stor.diff = me.donneesSection.difficultemin + Math.round((me.questionCourante - 1) * (me.donneesSection.difficultemax - me.donneesSection.difficultemin) / (me.donneesSection.nbrepetitions - 1))
    }

    stor.reponse = 0
    scratch.params.scratchProgDonnees.lesordres = []

    let mess
    stor.nb1 = j3pGetRandomInt(59, 99)
    stor.nb2 = j3pGetRandomInt(24, 49)

    scratch.params.scratchProgDonnees.Valeursdebaseautorisees = [stor.nb1 + '', stor.nb2 + '']

    let ok = false
    let cmpt = 0
    do {
      cmpt++
      stor.opi = j3pGetRandomInt(0, 3)
      ok = (stor.opfait.indexOf(stor.opi) === -1)
    } while ((cmpt < 100) && (!ok))
    stor.opfait.push(stor.opi)

    const oposs = ['+', '-', '×', '÷']
    const oposs2 = ['operator_add', 'operator_subtract', 'operator_multiply', 'operator_divide']
    stor.op = oposs[stor.opi]
    stor.optype = oposs2[stor.opi]
    stor.operation = stor.nb1 + ' ' + stor.op + ' ' + stor.nb2

    switch (stor.op) {
      case '+':stor.resultat = stor.nb1 + stor.nb2
        break
      case '-':stor.resultat = stor.nb1 - stor.nb2
        break
      case '×':stor.resultat = stor.nb1 * stor.nb2
        break
      case '÷':stor.resultat = stor.nb1 / stor.nb2
        break
    }
    mess = { operateur: stor.optype, value: [{ nom: 'NUM1', contenu: { valeur: stor.nb1 } }, { nom: 'NUM2', contenu: { valeur: stor.nb2 } }] }

    if (stor.diff === 2) {
      stor.nb3 = j3pGetRandomInt(2, 9)

      nbmax++
      ok = false
      cmpt = 0
      do {
        cmpt++
        stor.opi2 = j3pGetRandomInt(0, 3)
        ok = (stor.opfait2.indexOf(stor.opi2) === -1)
      } while ((cmpt < 100) && (!ok))

      stor.opfait2.push(stor.opi2)
      stor.op2 = oposs[stor.opi2]
      stor.optype2 = oposs2[stor.opi2]
      stor.sens = j3pGetRandomInt(0, 1)

      if (stor.sens === 0) {
        switch (stor.op2) {
          case '+':stor.resultat = stor.resultat + stor.nb3
            break
          case '-':stor.resultat = stor.resultat - stor.nb3
            break
          case '×':stor.resultat = stor.resultat * stor.nb3
            break
          case '÷':stor.resultat = stor.resultat / stor.nb3
            break
        }
        var buf2 = ''
        var buf1 = ''
        if ((stor.opi < 2) && (stor.opi2 > 1)) {
          buf1 = ') '; buf2 = '('
        }
        stor.operation = buf2 + stor.operation + ' ' + buf1 + stor.op2 + ' ' + stor.nb3

        mess = { operateur: stor.optype2, value: [{ nom: 'NUM1', contenu: { operateur: stor.optype, value: [{ nom: 'NUM1', contenu: { valeur: stor.nb1 } }, { nom: 'NUM2', contenu: { valeur: stor.nb2 } }] } }, { nom: 'NUM2', contenu: { valeur: stor.nb3 } }] }
      } else {
        stor.nb3 = j3pGetRandomInt(5000, 5999)
        switch (stor.op2) {
          case '+':stor.resultat = stor.nb3 + stor.resultat
            break
          case '-':stor.resultat = stor.nb3 - stor.resultat
            break
          case '×':stor.resultat = stor.nb3 * stor.resultat
            break
          case '÷':stor.resultat = stor.nb3 / stor.resultat
            break
        }
        buf2 = ''
        buf1 = ''
        if ((stor.opi < 2) && (stor.opi2 > 1)) {
          buf1 = ')'; buf2 = '('
        }
        if (stor.opi2 === 1) {
          buf1 = ')'; buf2 = '('
        }
        if (stor.opi2 === 3) {
          buf1 = ')'; buf2 = '('
        }
        stor.operation = stor.nb3 + ' ' + stor.op2 + ' ' + buf2 + stor.operation + buf1
        mess = { operateur: stor.optype2, value: [{ nom: 'NUM2', contenu: { operateur: stor.optype, value: [{ nom: 'NUM1', contenu: { valeur: stor.nb1 } }, { nom: 'NUM2', contenu: { valeur: stor.nb2 } }] } }, { nom: 'NUM1', contenu: { valeur: stor.nb3 } }] }
      }

      scratch.params.scratchProgDonnees.Valeursdebaseautorisees.push(stor.nb3 + '')
    }
    // [0] c’est affiche le good
    // [1] c’est pas d’autre nb que mes vals
    scratch.params.scratchProgDonnees.lesflag.push({ val: false })
    scratch.params.scratchProgDonnees.lesflag.push({ val: false })
    scratch.params.scratchProgDonnees.lesordres.push({ type: 'dire', value: [{ nom: 'DELTA', contenu: mess }, { nom: 'TEMPS', contenu: { valeur: 2, editable: 'false', deletable: 'false', movable: 'false' } }] })

    scratch.enonce([], {
      pourInject: { StartScale: 0.6 },
      modeCorrection: {
        type: 'SansComparaison',
        aVerif: [{ quoi: 'Flag', nombre: 0 },
          { quoi: 'NombreMaxBloc', nombre: nbmax },
          { quoi: 'BesoinBloc', nom: 'dire' },
          { quoi: 'QuandAvertir', quand: 'après' }]
      },
      dirigeProg: { sequence: ds.Sequence, pasApas: ds.PasAPas, cacheTest: ds.CacheTest }
    })

    const yy = addDefaultTable(scratch.lesdivs.enonceG, 1, 2)
    yy[0][0].style.padding = 0
    yy[0][1].style.padding = 0
    const cons = '<b> Programme le chat pour qu’il dise </b> <BR> le résultat de l’opération ' + stor.operation
    j3pAffiche(yy[0][0], null, cons)
    stor.aise = new BulleAide(yy[0][1], "Le chat doit faire lui même le calcul. <i> (utilise les blocs <b>'OPERATEURS'</b>) </i>", { place: 5 })

    me.finEnonce()
    me.cacheBoutonValider()
  }

  switch (this.etat) {
    case 'enonce':
      // code exécuté au lancement de la section
      if (this.debutDeLaSection) {
        initSection()
      } else {
        enonceMain()
      }
      break // case "enonce":

    case 'correction':
      try {
        const ret = scratch.prepCorrec()
        const xmlDoc2 = ret.xmlDoc2
        const xmlText = ret.xmlText

        if (scratch.params.scratchProgDonnees.repEleve) {
          // Bonne réponse
          this.score++
          scratch.corrigeGood(xmlDoc2, xmlText)
        } else {
          // Pas de bonne réponse
          scratch.corrige(xmlDoc2, xmlText)
        }
        // on veut être rappelé dans l’état navigation
        this.finCorrection('navigation', true)
      } catch (error) {
        console.error(error)
      }
      break // case "correction":

    case 'navigation':
      if (this.sectionTerminee()) {
        scratch.lesdivs.BoutonsSuite.appendChild(this.buttonsElts.sectionSuivante)
        this.buttonsElts.sectionSuivante.addEventListener('click', () => {
          scratch.params.grosStop = true
        })
      } else {
        this.etat = 'enonce'
        scratch.lesdivs.BoutonsSuite.appendChild(this.buttonsElts.suite)
        this.buttonsElts.suite.addEventListener('click', () => {
          scratch.params.grosStop = true
        })
      }
      // Obligatoire
      this.finNavigation()

      break // case "navigation":
  }
}
