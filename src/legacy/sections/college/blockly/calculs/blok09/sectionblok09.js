import { j3pGetRandomInt } from 'src/legacy/core/functions'
import { j3pAffiche } from 'src/lib/mathquill/functions'
import { getNewScratch } from 'src/legacy/outils/scratch/loaders'
import { addDefaultTable } from 'src/legacy/themes/table'

export const params = {
  outils: ['mathquill'],
  /**
   * Ce qui est paramétrable dans l’éditeur de graphe
   * @type {LegacySectionParamsParametres}
   */
  parametres: [
    ['nbrepetitions', 4, 'entier', 'Nombre de répétitions de la section'],
    ['nbchances', 0, 'entier', 'Nombre d’essais possibles (illimité si 0)'],
    ['PasAPas', false, 'boolean', "<u>true</u>: L’élève peut lancer son programme en mode 'Pas à pas'."],
    ['Sequence', true, 'boolean', '<u>true</u>: Chaque bloc a une durée d’exécution d’environ 1 seconde.'],
    ['Completer', true, 'boolean', '<u>true</u>: Une partie du programme est déjà en place.'],
    ['CacheTest', false, 'boolean', '<u>true</u>: L’élève ne voit pas l’action de son programme lors d la correction.']
  ]

}

/**
 * section blok09
 * @this {Parcours}
 */
export default function main () {
  const me = this
  const stor = me.storage
  const ds = me.donneesSection
  let scratch = stor.scratch
  const textes = {
    resat: ['le nombre suivant du', 'le nombre précédent du', 'le double du', 'le triple du', 'la moitié du', 'le tiers du', 'le carré du'],
    consigne: ' <b> Ecris un programme </b> qui fait dire au chat <b>£a</b> nombre donné par un utilisateur .'
  }

  async function initSection () {
    ds.theme = 'zonesAvecImageDeFond'
    scratch = await getNewScratch()
    stor.scratch = scratch
    stor.pbfait = []
    me.validOnEnter = false // ex donneesSection.touche_entree
    scratch.initParams(me)
    scratch.testFlag = function () {

    }
    scratch.testFlagFin = function () {
      let ok = true
      let mess = ''
      const dictKat = scratch.vireGuillemets(scratch.said())
      const t = parseFloat(scratch.params.scratchProgDonnees.reponseDonnesAGetUS[scratch.params.scratchProgDonnees.reponseDonnesAGetUS.length - 1])
      if (isNaN(dictKat) || dictKat === '') { ok = false; mess = 'Le chat doit dire un nombre !' } else {
        switch (stor.pbencours) {
          case 0 : var acomp = t + 1
            break
          case 1 : acomp = t - 1
            break
          case 2 : acomp = t * 2
            break
          case 3 : acomp = t * 3
            break
          case 4 : acomp = t / 2
            break
          case 5 : acomp = t / 3
            break
          case 6 : acomp = t * t
            break
        }
        ok = (Math.abs(acomp - dictKat) < Math.pow(10, -5))
        if (!ok) { mess = 'Le chat ne dit pas le résultat attendu !' }
      }
      return { ok, mess }
    }
    ds.nbitems = ds.nbetapes * ds.nbrepetitions
    me.construitStructurePage({ structure: 'presentation3', ratioGauche: 0.6 })
    me.stockage = [0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0]

    me.score = 0
    stor.dejafait = []
    me.afficheTitre('Calculs avec une réponse')

    scratch.params.scratchProgDonnees.menu = []
    scratch.params.scratchProgDonnees.progdeb = []
    scratch.params.scratchProgDonnees.progdeb.push({ type: 'Drapo', editable: 'false', deletable: 'false', movable: 'false', x: '20', y: '20' })

    scratch.params.scratchProgDonnees.menu.push({ type: 'catégorie', nom: 'Opérateurs', coul1: '#22dd22', coul2: '#22dd22', contenu: [] })
    scratch.params.scratchProgDonnees.menu[0].contenu.push({ type: 'operator_add', value: [{ nom: 'NUM1', contenu: { valeur: ' ' } }, { nom: 'NUM2', contenu: { valeur: ' ' } }] })
    scratch.params.scratchProgDonnees.menu[0].contenu.push({ type: 'operator_subtract', value: [{ nom: 'NUM1', contenu: { valeur: ' ' } }, { nom: 'NUM2', contenu: { valeur: ' ' } }] })
    scratch.params.scratchProgDonnees.menu[0].contenu.push({ type: 'operator_multiply', value: [{ nom: 'NUM1', contenu: { valeur: ' ' } }, { nom: 'NUM2', contenu: { valeur: ' ' } }] })
    scratch.params.scratchProgDonnees.menu[0].contenu.push({ type: 'operator_divide', value: [{ nom: 'NUM1', contenu: { valeur: ' ' } }, { nom: 'NUM2', contenu: { valeur: ' ' } }] })
    scratch.params.scratchProgDonnees.menu.push({ type: 'catégorie', nom: 'Utilisateur', coul1: '#4C97FF', coul2: '#3373CC', contenu: [] })
    scratch.params.scratchProgDonnees.menu[1].contenu.push({ type: 'reponse' })
    if (!ds.Completer) {
      scratch.params.scratchProgDonnees.menu[1].contenu.push({ type: 'Get_us', value: [{ nom: 'STRING', contenu: { valeur: 'Donne un nombre.', editable: 'false', deletable: 'false', movable: 'false' } }] })
      scratch.params.scratchProgDonnees.menu[1].contenu.push({ type: 'dire', value: [{ nom: 'DELTA', contenu: { valeur: 'PARTIE A COMPLETER' } }, { nom: 'TEMPS', contenu: { valeur: 2, editable: 'false', deletable: 'false', movable: 'false' } }] })
    } else {
      scratch.params.scratchProgDonnees.progdeb.push({ type: 'Get_us', value: [{ nom: 'STRING', contenu: { valeur: 'Donne un nombre.', editable: 'false', deletable: 'false', movable: 'false' } }], editable: 'false', deletable: 'false', movable: 'false' })
      scratch.params.scratchProgDonnees.progdeb.push({ type: 'dire', editable: 'false', deletable: 'false', movable: 'false', value: [{ nom: 'DELTA', contenu: { valeur: 'PARTIE A COMPLETER' } }, { nom: 'TEMPS', contenu: { valeur: 2, editable: 'false', deletable: 'false', movable: 'false' } }] })
    }
    scratch.params.blokmenulimite.push({ type: 'dire', nb: 1 })

    enonceMain()
  }

  function enonceMain () {
    me.videLesZones()

    scratch.params.scratchProgDonnees.Lutins[0].cox = 10
    scratch.params.scratchProgDonnees.Lutins[0].coxbase = 10

    let cmpt = 0
    do {
      cmpt++
      stor.pbencours = j3pGetRandomInt(0, (textes.resat.length - 1))
    } while ((stor.pbfait.indexOf(stor.pbencours) !== -1) && (cmpt < 100))
    stor.pbfait.push(stor.pbencours)

    scratch.params.scratchProgDonnees.lesordres = []

    let nbblocks = 5
    const num2 = {}
    switch (stor.pbencours) {
      case 0 : var op = 'plus'
        num2.valeur = '1'
        scratch.params.scratchProgDonnees.listValPos = [['1'], ['2'], ['3'], ['4'], ['5'], ['6'], ['7'], ['8'], ['9'], ['10']]
        break
      case 1 : op = 'moins'
        num2.valeur = '1'
        scratch.params.scratchProgDonnees.listValPos = [['2'], ['3'], ['4'], ['5'], ['6'], ['7'], ['8'], ['9'], ['10']]
        break
      case 2 : op = 'fois'
        num2.valeur = '2'
        scratch.params.scratchProgDonnees.listValPos = [['3'], ['4'], ['5'], ['6'], ['7'], ['8'], ['9'], ['10'], ['11']]
        break
      case 3 : op = 'fois'
        num2.valeur = '3'
        scratch.params.scratchProgDonnees.listValPos = [['4'], ['5'], ['6'], ['7'], ['8'], ['9'], ['10'], ['11']]
        break
      case 4 : op = 'div'
        num2.valeur = '2'
        scratch.params.scratchProgDonnees.listValPos = [['6'], ['8'], ['12'], ['10'], ['14']]
        break
      case 5 : op = 'div'
        num2.valeur = '3'
        scratch.params.scratchProgDonnees.listValPos = [['6'], ['15'], ['12'], ['18'], ['21']]
        break
      case 6 : op = 'fois'
        num2.reponse = true
        nbblocks++
        scratch.params.scratchProgDonnees.listValPos = [['1'], ['2'], ['3'], ['4'], ['5'], ['6'], ['7'], ['8'], ['9'], ['10']]
        break
    }

    scratch.params.scratchProgDonnees.lesordres.push({ type: 'Get_us', value: [{ nom: 'STRING', contenu: { valeur: 'Donne un nombre.', editable: 'false', deletable: 'false', movable: 'false' } }] })
    scratch.params.scratchProgDonnees.lesordres.push({ type: 'dire', value: [{ nom: 'DELTA', contenu: { operateur: op, value: [{ nom: 'NUM1', contenu: { reponse: '' } }, { nom: 'NUM2', contenu: num2 }] } }, { nom: 'TEMPS', contenu: { valeur: 2, editable: 'false', deletable: 'false', movable: 'false' } }] })

    scratch.enonce([], {
      pourInject: { StartScale: 0.75 },
      modeCorrection: {
        type: 'SansComparaison',
        aVerif: [
          { quoi: 'NombreMaxBloc', nombre: nbblocks },
          { quoi: 'BesoinBloc', nom: 'Getus' },
          { quoi: 'BesoinBloc', nom: 'dire' },
          { quoi: 'QuandAvertir', quand: 'apres' },
          { quoi: 'AnnuleSiAvert', quand: false }]
      },
      dirigeProg: { sequence: ds.Sequence, pasApas: ds.PasAPas, cacheTest: ds.CacheTest }
    })

    const cons = textes.consigne
    const yy = addDefaultTable(scratch.lesdivs.enonceG, 1, 1)[0][0]

    j3pAffiche(yy, null, cons, { a: textes.resat[stor.pbencours] })

    me.finEnonce()
    me.cacheBoutonValider()
  }

  switch (this.etat) {
    case 'enonce':
      // code exécuté au lancement de la section
      if (this.debutDeLaSection) {
        initSection()
      } else {
        enonceMain()
      }
      break // case "enonce":

    case 'correction':
      try {
        const ret = scratch.prepCorrec()
        const xmlDoc2 = ret.xmlDoc2
        const xmlText = ret.xmlText

        if (scratch.params.scratchProgDonnees.repEleve) {
          // Bonne réponse
          this.score++
          scratch.corrigeGood(xmlDoc2, xmlText)
        } else {
          // Pas de bonne réponse
          scratch.corrige(xmlDoc2, xmlText)
        }
        // on veut être rappelé dans l’état navigation
        this.finCorrection('navigation', true)
      } catch (error) {
        console.error(error)
      }
      break // case "correction":

    case 'navigation':

      if (this.sectionTerminee()) {
        scratch.lesdivs.BoutonsSuite.appendChild(this.buttonsElts.sectionSuivante)
        this.buttonsElts.sectionSuivante.addEventListener('click', () => {
          scratch.params.grosStop = true
        })
      } else {
        this.etat = 'enonce'
        scratch.lesdivs.BoutonsSuite.appendChild(this.buttonsElts.suite)
        this.buttonsElts.suite.addEventListener('click', () => {
          scratch.params.grosStop = true
        })
      }
      // Obligatoire
      this.finNavigation()

      break // case "navigation":
  }
}
