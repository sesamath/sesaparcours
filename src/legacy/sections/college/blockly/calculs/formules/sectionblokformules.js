import { j3pGetRandomInt, j3pShowError } from 'src/legacy/core/functions'
import BulleAide from 'src/legacy/outils/bulleAide/BulleAide'
import { j3pAffiche } from 'src/lib/mathquill/functions'
import { addDefaultTable } from 'src/legacy/themes/table'
import { getNewScratch } from 'src/legacy/outils/scratch/loaders'

export const params = {
  outils: ['mathquill'],
  /**
   * Ce qui est paramétrable dans l’éditeur de graphe
   * @type {LegacySectionParamsParametres}
   */
  parametres: [
    ['nbrepetitions', 4, 'entier', 'Nombre de répétitions de la section'],
    ['nbchances', 3, 'entier', 'Nombre d’essais possibles (illimité si 0)'],
    ['PasAPas', false, 'boolean', "<u>true</u>: L’élève peut lancer son programme en mode 'Pas à pas'."],
    ['Sequence', true, 'boolean', '<u>true</u>: Chaque bloc a une durée d’exécution d’environ 1 seconde.'],
    ['A_Triangle_Quelconque', false, 'boolean', 'true : Formule aire d’un triangle quelconque proposée <BR> false : non.'],
    ['P_Triangle_Equilateral', false, 'boolean', 'true : Formule périmètre d’un triangle équilatéral proposée <BR> false : non.'],
    ['P_Carre', false, 'boolean', 'true : Formule périmètre d’un carré proposée <BR> false : non.'],
    ['A_Carre', false, 'boolean', 'true : Formule aire d’un carré proposée <BR> false : non.'],
    ['P_Rectangle', false, 'boolean', 'true : Formule périmètre d’un rectangle proposée <BR> false : non.'],
    ['A_Rectangle', false, 'boolean', 'true : Formule aire d’un rectangle proposée <BR> false : non.'],
    ['P_Losange', false, 'boolean', 'true : Formule périmètre d’un losange proposée <BR> false : non.'],
    ['P_Parallelogramme', false, 'boolean', 'true : Formule périmètre d’un parallélogramme proposée <BR> false : non.'],
    ['A_Parallelogramme', false, 'boolean', 'true : Formule aire d’un parallélogramme proposée <BR> false : non.'],
    ['P_Cercle', false, 'boolean', 'true : Formule périmètre d’un cercle proposée <BR> false : non.'],
    ['A_Cercle', false, 'boolean', 'true : Formule aire d’un cercle proposée <BR> false : non.'],
    ['P_Cube', false, 'boolean', 'true : Formule périmètre d’un cube proposée <BR> false : non.'],
    ['A_Cube', false, 'boolean', 'true : Formule aire d’un cube proposée <BR> false : non.'],
    ['V_Cube', false, 'boolean', 'true : Formule volume d’un cube proposée <BR> false : non.'],
    ['A_Sphere', false, 'boolean', 'true : Formule aire d’une sphère proposée <BR> false : non.'],
    ['V_Sphere', false, 'boolean', 'true : Formule volume d’une sphère proposée <BR> false : non.'],
    ['CacheTest', false, 'boolean', '<u>true</u>: L’élève ne voit pas l’action de son programme lors d la correction.']
  ]
}

/**
 * section blokformules
 * @this {Parcours}
 */
export default function main () {
  const me = this
  const stor = me.storage
  const ds = me.donneesSection
  let scratch = stor.scratch

  function last (tab) {
    if (tab.length === 0) { return 'NaN' }
    return tab[tab.length - 1]
  }
  function avantlast (tab) {
    if (tab.length < 2) { return 'NaN' }
    return tab[tab.length - 2]
  }

  stor.Pcarre = function (tav) { return 4 * last(tav) }
  stor.Acarre = function (tav) { return last(tav) * last(tav) }
  stor.Atriq = function (tav) { return last(tav) * avantlast(tav) / 2 }
  stor.Ptrieq = function (tav) { return last(tav) * 3 }
  stor.Prectangle = function (tav) { return last(tav) * 2 + avantlast(tav) * 2 }
  stor.Arectangle = function (tav) { return last(tav) * avantlast(tav) }
  stor.Plosange = function (tav) { return last(tav) * 4 }
  stor.Pparallelo = function (tav) { return last(tav) * 2 + avantlast(tav) * 2 }
  stor.Aparallelo = function (tav) { return last(tav) * avantlast(tav) }
  stor.Pcercle = function (tav) { return last(tav) * Math.PI * 2 }
  stor.Acercle = function (tav) { return last(tav) * last(tav) * Math.PI }
  stor.Pcube = function (tav) { return last(tav) * 12 }
  stor.Acube = function (tav) { return 6 * last(tav) * last(tav) }
  stor.Vcube = function (tav) { return last(tav) * last(tav) * last(tav) }
  stor.ALprisme = function (tav) { return last(tav) * avantlast(tav) }
  stor.Vprisme = function (tav) { return last(tav) * avantlast(tav) }
  stor.Pcylindre = function (tav) { return 4 * Math.PI * last(tav) }
  stor.Acylindre = function (tav) { return 2 * Math.PI * last(tav) * last(tav) + 2 * Math.PI * last(tav) * avantlast(tav) }
  stor.ALcylindre = function (tav) { return 2 * Math.PI * last(tav) * avantlast(tav) }
  stor.Vcylindre = function (tav) { return Math.PI * last(tav) * last(tav) * avantlast(tav) }
  stor.Vpyramide = function (tav) { return last(tav) * avantlast(tav) / 3 }
  stor.Pcone = function (tav) { return 2 * Math.PI * last(tav) }
  stor.Vcone = function (tav) { return Math.PI * last(tav) * last(tav) * avantlast(tav) / 3 }
  stor.Asphere = function (tav) { return 4 * Math.PI * last(tav) * last(tav) }
  stor.Vsphere = function (tav) { return 4 / 3 * Math.PI * last(tav) * last(tav) * last(tav) }

  async function initSection () {
    ds.theme = 'zonesAvecImageDeFond'
    scratch = await getNewScratch()
    stor.scratch = scratch
    me.validOnEnter = false // ex donneesSection.touche_entree

    scratch.initParams(me)
    scratch.testFlag = function () {}
    scratch.testFlagFin = function () {
      let mess = ''
      const ok = (scratch.params.scratchProgDonnees.said[scratch.params.scratchProgDonnees.said.length - 1] === (stor.foncverif(scratch.params.scratchProgDonnees.reponseDonnesAGetUS) + ''))
      if (!ok)(mess = 'Le chat de donne pas le bon résultat !')
      return { ok, mess }
    }
    scratch.params.AutoriseSupprimeVariable = false
    scratch.params.AutoriseRenomeVariable = false
    ds.nbitems = ds.nbetapes * ds.nbrepetitions
    stor.formpos = []

    if (ds.A_Triangle_Quelconque) { stor.formpos.push('atriq') }
    if (ds.P_Triangle_Equilateral) { stor.formpos.push('ptrieq') }
    if (ds.P_Carre) { stor.formpos.push('pcarre') }
    if (ds.A_Carre) { stor.formpos.push('acarre') }
    if (ds.P_Rectangle) { stor.formpos.push('prectangle') }
    if (ds.A_Rectangle) { stor.formpos.push('arectangle') }
    if (ds.P_Losange) { stor.formpos.push('plosange') }
    if (ds.P_Parallelogramme) { stor.formpos.push('pparallelo') }
    if (ds.A_Parallelogramme) { stor.formpos.push('aparallelo') }
    if (ds.P_Cercle) { stor.formpos.push('pcercle') }
    if (ds.A_Cercle) { stor.formpos.push('acercle') }
    if (ds.P_Cube) { stor.formpos.push('pcube') }
    if (ds.A_Cube) { stor.formpos.push('acube') }
    if (ds.V_Cube) { stor.formpos.push('vcube') }
    if (ds.A_Sphere) { stor.formpos.push('asphere') }
    if (ds.V_Sphere) { stor.formpos.push('vsphere') }
    if (stor.formpos.length === 0) { j3pShowError('Paramètrage: Aucune formule disponible'); stor.formpos.push('pcarre') }

    me.construitStructurePage({ structure: 'presentation3', ratioGauche: 0.6 })
    me.stockage = [0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0]

    stor.dejafait = []
    me.afficheTitre('Faire des calculs')
    enonceMain()
  }

  function enonceMain () {
    me.videLesZones()

    scratch.params.scratchProgDonnees.lesordres = []
    scratch.params.scratchProgDonnees.lesvarbonnes = []
    scratch.params.scratchProgDonnees.Lutins[0].cox = -30
    scratch.params.scratchProgDonnees.Lutins[0].coxbase = -30

    scratch.params.scratchProgDonnees.menu = []
    scratch.params.scratchProgDonnees.progdeb = []
    scratch.params.scratchProgDonnees.progdeb.push({ type: 'Drapo', editable: 'false', deletable: 'false', movable: 'false', x: '20', y: '20' })

    scratch.params.scratchProgDonnees.menu.push({ type: 'catégorie', nom: 'Opérateurs', coul1: '#22dd22', coul2: '#22dd22', contenu: [] })
    scratch.params.scratchProgDonnees.menu[0].contenu.push({ type: 'operator_add', value: [{ nom: 'NUM1', contenu: { valeur: ' ' } }, { nom: 'NUM2', contenu: { valeur: ' ' } }] })
    scratch.params.scratchProgDonnees.menu[0].contenu.push({ type: 'operator_subtract', value: [{ nom: 'NUM1', contenu: { valeur: ' ' } }, { nom: 'NUM2', contenu: { valeur: ' ' } }] })
    scratch.params.scratchProgDonnees.menu[0].contenu.push({ type: 'operator_multiply', value: [{ nom: 'NUM1', contenu: { valeur: ' ' } }, { nom: 'NUM2', contenu: { valeur: ' ' } }] })
    scratch.params.scratchProgDonnees.menu[0].contenu.push({ type: 'operator_divide', value: [{ nom: 'NUM1', contenu: { valeur: ' ' } }, { nom: 'NUM2', contenu: { valeur: ' ' } }] })
    scratch.params.scratchProgDonnees.menu.push({ type: 'catégorie', nom: 'Variables', coul1: '#FF8C1A', coul2: '#DB6E00', contenu: [] })

    let cm = 0
    let ok
    do {
      cm++
      stor.fencours = stor.formpos[j3pGetRandomInt(0, stor.formpos.length - 1)]
      ok = (stor.dejafait.indexOf(stor.fencours) === -1)
    } while ((cm < 100) && (!ok))
    stor.dejafait.push(stor.fencours)

    let bufmen = ''
    let larep

    switch (stor.fencours) {
      case 'pcarre': scratch.params.scratchProgDonnees.menu[1].contenu.push({ type: 'data_variable', variable: [{ nom: 'VARIABLE', nomvar: 'cote', id: 'aaaaaaa' }] })
        scratch.params.scratchProgDonnees.progdeb.push({ type: 'Get_us', value: [{ nom: 'STRING', contenu: { valeur: 'Donne la longueur du côté d’un carré (en cm).', editable: 'false', deletable: 'false', movable: 'false' } }], editable: 'false', deletable: 'false', movable: 'false' })
        scratch.params.scratchProgDonnees.progdeb.push({ type: 'data_setvariableto', editable: 'false', deletable: 'false', movable: 'false', variable: [{ nom: 'VARIABLE', nomvar: 'cote', id: 'aaaaaaa' }], value: [{ nom: 'VALUE', editable: 'false', deletable: 'false', movable: 'false', contenu: { reponse: '', editable: 'false', deletable: 'false', movable: 'false' } }] })
        scratch.params.scratchProgDonnees.progdeb.push({ type: 'dire', editable: 'false', deletable: 'false', movable: 'false', value: [{ nom: 'DELTA', editable: 'false', deletable: 'false', movable: 'false', contenu: { valeur: 'Voici son périmètre (en cm).', editable: 'false', deletable: 'false', movable: 'false' } }, { nom: 'TEMPS', contenu: { valeur: 2, editable: 'false', deletable: 'false', movable: 'false' } }] })
        scratch.params.scratchProgDonnees.progdeb.push({ type: 'dire', editable: 'false', deletable: 'false', movable: 'false', value: [{ nom: 'DELTA', contenu: { valeur: 'PARTIE A COMPLETER' } }, { nom: 'TEMPS', contenu: { valeur: 2, editable: 'false', deletable: 'false', movable: 'false' } }] })

        scratch.params.scratchProgDonnees.lesvarbonnes.push({ nom: 'cote', val: 0, id: 'aaaaaaa' })
        stor.foncverif = stor.Pcarre
        larep = { operateur: '*', value: [{ nom: 'NUM1', contenu: { valeur: 4 } }, { nom: 'NUM2', contenu: { type: 'data_variable', variable: [{ nom: 'VARIABLE', nomvar: 'cote', id: 'aaaaaaa' }] } }] }
        bufmen = 'le périmètre d’un carré'

        scratch.params.scratchProgDonnees.lesordres.push({ type: 'Get_us', value: [{ nom: 'STRING', contenu: { valeur: 'Donne la longueur du côté d’un carré (en cm).', editable: 'false', deletable: 'false', movable: 'false' } }], editable: 'false', deletable: 'false', movable: 'false' })
        scratch.params.scratchProgDonnees.lesordres.push({ type: 'data_setvariableto', editable: 'false', deletable: 'false', movable: 'false', variable: [{ nom: 'VARIABLE', nomvar: 'cote', id: 'aaaaaaa' }], value: [{ nom: 'VALUE', editable: 'false', deletable: 'false', movable: 'false', contenu: { reponse: '', editable: 'false', deletable: 'false', movable: 'false' } }] })
        scratch.params.scratchProgDonnees.lesordres.push({ type: 'dire', editable: 'false', deletable: 'false', movable: 'false', value: [{ nom: 'DELTA', editable: 'false', deletable: 'false', movable: 'false', contenu: { valeur: 'Voici son périmètre (en cm).', editable: 'false', deletable: 'false', movable: 'false' } }, { nom: 'TEMPS', contenu: { valeur: 2, editable: 'false', deletable: 'false', movable: 'false' } }] })
        scratch.params.scratchProgDonnees.listValPos = [['1'], ['2'], ['3'], ['4'], ['5'], ['6'], ['7'], ['8'], ['9'], ['10']]
        break
      case 'acarre': scratch.params.scratchProgDonnees.menu[1].contenu.push({ type: 'data_variable', variable: [{ nom: 'VARIABLE', nomvar: 'cote', id: 'aaaaaaa' }] })
        scratch.params.scratchProgDonnees.progdeb.push({ type: 'Get_us', value: [{ nom: 'STRING', contenu: { valeur: 'Donne la longueur du côté d’un carré (en cm).', editable: 'false', deletable: 'false', movable: 'false' } }], editable: 'false', deletable: 'false', movable: 'false' })
        scratch.params.scratchProgDonnees.progdeb.push({ type: 'data_setvariableto', editable: 'false', deletable: 'false', movable: 'false', variable: [{ nom: 'VARIABLE', nomvar: 'cote', id: 'aaaaaaa' }], value: [{ nom: 'VALUE', editable: 'false', deletable: 'false', movable: 'false', contenu: { reponse: '', editable: 'false', deletable: 'false', movable: 'false' } }] })
        scratch.params.scratchProgDonnees.progdeb.push({ type: 'dire', editable: 'false', deletable: 'false', movable: 'false', value: [{ nom: 'DELTA', editable: 'false', deletable: 'false', movable: 'false', contenu: { valeur: 'Voici son aire (en cm²).', editable: 'false', deletable: 'false', movable: 'false' } }, { nom: 'TEMPS', contenu: { valeur: 2, editable: 'false', deletable: 'false', movable: 'false' } }] })
        scratch.params.scratchProgDonnees.progdeb.push({ type: 'dire', editable: 'false', deletable: 'false', movable: 'false', value: [{ nom: 'DELTA', contenu: { valeur: 'PARTIE A COMPLETER' } }, { nom: 'TEMPS', contenu: { valeur: 2, editable: 'false', deletable: 'false', movable: 'false' } }] })

        scratch.params.scratchProgDonnees.lesvarbonnes.push({ nom: 'cote', val: 0, id: 'aaaaaaa' })
        stor.foncverif = stor.Acarre
        larep = { operateur: '*', value: [{ nom: 'NUM1', contenu: { type: 'data_variable', variable: [{ nom: 'VARIABLE', nomvar: 'cote', id: 'aaaaaaa' }] } }, { nom: 'NUM2', contenu: { type: 'data_variable', variable: [{ nom: 'VARIABLE', nomvar: 'cote', id: 'aaaaaaa' }] } }] }
        bufmen = 'l’aire d’un carré'

        scratch.params.scratchProgDonnees.lesordres.push({ type: 'Get_us', value: [{ nom: 'STRING', contenu: { valeur: 'Donne la longueur du côté d’un carré (en cm).', editable: 'false', deletable: 'false', movable: 'false' } }], editable: 'false', deletable: 'false', movable: 'false' })
        scratch.params.scratchProgDonnees.lesordres.push({ type: 'data_setvariableto', editable: 'false', deletable: 'false', movable: 'false', variable: [{ nom: 'VARIABLE', nomvar: 'cote', id: 'aaaaaaa' }], value: [{ nom: 'VALUE', editable: 'false', deletable: 'false', movable: 'false', contenu: { reponse: '', editable: 'false', deletable: 'false', movable: 'false' } }] })
        scratch.params.scratchProgDonnees.lesordres.push({ type: 'dire', editable: 'false', deletable: 'false', movable: 'false', value: [{ nom: 'DELTA', editable: 'false', deletable: 'false', movable: 'false', contenu: { valeur: 'Voici son aire (en cm²).', editable: 'false', deletable: 'false', movable: 'false' } }, { nom: 'TEMPS', contenu: { valeur: 2, editable: 'false', deletable: 'false', movable: 'false' } }] })
        scratch.params.scratchProgDonnees.listValPos = [['1'], ['2'], ['3'], ['4'], ['5'], ['6'], ['7'], ['8'], ['9'], ['10']]

        break
      case 'atriq': scratch.params.scratchProgDonnees.menu[1].contenu.push({ type: 'data_variable', variable: [{ nom: 'VARIABLE', nomvar: 'base', id: 'aaaaaaa' }] })
        scratch.params.scratchProgDonnees.menu[1].contenu.push({ type: 'data_variable', variable: [{ nom: 'VARIABLE', nomvar: 'hauteur', id: 'bbbbbbb' }] })
        scratch.params.scratchProgDonnees.progdeb.push({ type: 'Get_us', value: [{ nom: 'STRING', contenu: { valeur: 'Donne la longueur de la base d’un triangle (en cm).', editable: 'false', deletable: 'false', movable: 'false' } }], editable: 'false', deletable: 'false', movable: 'false' })
        scratch.params.scratchProgDonnees.progdeb.push({ type: 'data_setvariableto', editable: 'false', deletable: 'false', movable: 'false', variable: [{ nom: 'VARIABLE', nomvar: 'base', id: 'aaaaaaa' }], value: [{ nom: 'VALUE', editable: 'false', deletable: 'false', movable: 'false', contenu: { reponse: '', editable: 'false', deletable: 'false', movable: 'false' } }] })
        scratch.params.scratchProgDonnees.progdeb.push({ type: 'Get_us', value: [{ nom: 'STRING', contenu: { valeur: 'Donne la longueur de la hauteur associée (en cm).', editable: 'false', deletable: 'false', movable: 'false' } }], editable: 'false', deletable: 'false', movable: 'false' })
        scratch.params.scratchProgDonnees.progdeb.push({ type: 'data_setvariableto', editable: 'false', deletable: 'false', movable: 'false', variable: [{ nom: 'VARIABLE', nomvar: 'hauteur', id: 'bbbbbbb' }], value: [{ nom: 'VALUE', editable: 'false', deletable: 'false', movable: 'false', contenu: { reponse: '', editable: 'false', deletable: 'false', movable: 'false' } }] })
        scratch.params.scratchProgDonnees.progdeb.push({ type: 'dire', editable: 'false', deletable: 'false', movable: 'false', value: [{ nom: 'DELTA', editable: 'false', deletable: 'false', movable: 'false', contenu: { valeur: 'Voici son aire (en cm²).', editable: 'false', deletable: 'false', movable: 'false' } }, { nom: 'TEMPS', contenu: { valeur: 2, editable: 'false', deletable: 'false', movable: 'false' } }] })
        scratch.params.scratchProgDonnees.progdeb.push({ type: 'dire', editable: 'false', deletable: 'false', movable: 'false', value: [{ nom: 'DELTA', contenu: { valeur: 'PARTIE A COMPLETER' } }, { nom: 'TEMPS', contenu: { valeur: 2, editable: 'false', deletable: 'false', movable: 'false' } }] })

        scratch.params.scratchProgDonnees.lesvarbonnes.push({ nom: 'base', val: 0, id: 'aaaaaaa' })
        scratch.params.scratchProgDonnees.lesvarbonnes.push({ nom: 'hauteur', val: 0, id: 'bbbbbbb' })
        stor.foncverif = stor.Atriq
        larep = { operateur: '/', value: [{ nom: 'NUM1', contenu: { operateur: '*', value: [{ nom: 'NUM1', contenu: { type: 'data_variable', variable: [{ nom: 'VARIABLE', nomvar: 'base', id: 'aaaaaaa' }] } }, { nom: 'NUM2', contenu: { type: 'data_variable', variable: [{ nom: 'VARIABLE', nomvar: 'hauteur', id: 'bbbbbbb' }] } }] } }, { nom: 'NUM2', contenu: { valeur: 2 } }] }
        bufmen = 'l’aire d’un triangle quelconque'

        scratch.params.scratchProgDonnees.lesordres.push({ type: 'Get_us', value: [{ nom: 'STRING', contenu: { valeur: 'Donne la longueur de la base d’un triangle (en cm).', editable: 'false', deletable: 'false', movable: 'false' } }], editable: 'false', deletable: 'false', movable: 'false' })
        scratch.params.scratchProgDonnees.lesordres.push({ type: 'data_setvariableto', editable: 'false', deletable: 'false', movable: 'false', variable: [{ nom: 'VARIABLE', nomvar: 'base', id: 'aaaaaaa' }], value: [{ nom: 'VALUE', editable: 'false', deletable: 'false', movable: 'false', contenu: { reponse: '', editable: 'false', deletable: 'false', movable: 'false' } }] })
        scratch.params.scratchProgDonnees.lesordres.push({ type: 'Get_us', value: [{ nom: 'STRING', contenu: { valeur: 'Donne la longueur de la hauteur associée (en cm).', editable: 'false', deletable: 'false', movable: 'false' } }], editable: 'false', deletable: 'false', movable: 'false' })
        scratch.params.scratchProgDonnees.lesordres.push({ type: 'data_setvariableto', editable: 'false', deletable: 'false', movable: 'false', variable: [{ nom: 'VARIABLE', nomvar: 'hauteur', id: 'bbbbbbb' }], value: [{ nom: 'VALUE', editable: 'false', deletable: 'false', movable: 'false', contenu: { reponse: '', editable: 'false', deletable: 'false', movable: 'false' } }] })
        scratch.params.scratchProgDonnees.lesordres.push({ type: 'dire', editable: 'false', deletable: 'false', movable: 'false', value: [{ nom: 'DELTA', editable: 'false', deletable: 'false', movable: 'false', contenu: { valeur: 'Voici son aire (en cm²).', editable: 'false', deletable: 'false', movable: 'false' } }, { nom: 'TEMPS', contenu: { valeur: 2, editable: 'false', deletable: 'false', movable: 'false' } }] })
        scratch.params.scratchProgDonnees.listValPos = [['2', '3'], ['2', '4'], ['3', '4'], ['4', '7'], ['5', '6'], ['6', '4'], ['7', '10'], ['8', '5'], ['9', '4'], ['10', '12']]

        break
      case 'ptrieq': scratch.params.scratchProgDonnees.menu[1].contenu.push({ type: 'data_variable', variable: [{ nom: 'VARIABLE', nomvar: 'cote', id: 'aaaaaaa' }] })
        scratch.params.scratchProgDonnees.progdeb.push({ type: 'Get_us', value: [{ nom: 'STRING', contenu: { valeur: 'Donne la longueur du côté d’un triangle équilaéral (en cm).', editable: 'false', deletable: 'false', movable: 'false' } }], editable: 'false', deletable: 'false', movable: 'false' })
        scratch.params.scratchProgDonnees.progdeb.push({ type: 'data_setvariableto', editable: 'false', deletable: 'false', movable: 'false', variable: [{ nom: 'VARIABLE', nomvar: 'cote', id: 'aaaaaaa' }], value: [{ nom: 'VALUE', editable: 'false', deletable: 'false', movable: 'false', contenu: { reponse: '', editable: 'false', deletable: 'false', movable: 'false' } }] })
        scratch.params.scratchProgDonnees.progdeb.push({ type: 'dire', editable: 'false', deletable: 'false', movable: 'false', value: [{ nom: 'DELTA', editable: 'false', deletable: 'false', movable: 'false', contenu: { valeur: 'Voici son périmètre (en cm).', editable: 'false', deletable: 'false', movable: 'false' } }, { nom: 'TEMPS', contenu: { valeur: 2, editable: 'false', deletable: 'false', movable: 'false' } }] })
        scratch.params.scratchProgDonnees.progdeb.push({ type: 'dire', editable: 'false', deletable: 'false', movable: 'false', value: [{ nom: 'DELTA', contenu: { valeur: 'PARTIE A COMPLETER' } }, { nom: 'TEMPS', contenu: { valeur: 2, editable: 'false', deletable: 'false', movable: 'false' } }] })

        scratch.params.scratchProgDonnees.lesvarbonnes.push({ nom: 'cote', val: 0, id: 'aaaaaaa' })
        stor.foncverif = stor.Ptrieq
        larep = { operateur: '*', value: [{ nom: 'NUM1', contenu: { valeur: 3 } }, { nom: 'NUM2', contenu: { type: 'data_variable', variable: [{ nom: 'VARIABLE', nomvar: 'cote', id: 'aaaaaaa' }] } }] }
        bufmen = 'le périmètre d’un triangle équilatéral'

        scratch.params.scratchProgDonnees.lesordres.push({ type: 'Get_us', value: [{ nom: 'STRING', contenu: { valeur: 'Donne la longueur du côté d’un triangle équilatéral (en cm).', editable: 'false', deletable: 'false', movable: 'false' } }], editable: 'false', deletable: 'false', movable: 'false' })
        scratch.params.scratchProgDonnees.lesordres.push({ type: 'data_setvariableto', editable: 'false', deletable: 'false', movable: 'false', variable: [{ nom: 'VARIABLE', nomvar: 'cote', id: 'aaaaaaa' }], value: [{ nom: 'VALUE', editable: 'false', deletable: 'false', movable: 'false', contenu: { reponse: '', editable: 'false', deletable: 'false', movable: 'false' } }] })
        scratch.params.scratchProgDonnees.lesordres.push({ type: 'dire', editable: 'false', deletable: 'false', movable: 'false', value: [{ nom: 'DELTA', editable: 'false', deletable: 'false', movable: 'false', contenu: { valeur: 'Voici son périmètre (en cm).', editable: 'false', deletable: 'false', movable: 'false' } }, { nom: 'TEMPS', contenu: { valeur: 2, editable: 'false', deletable: 'false', movable: 'false' } }] })
        scratch.params.scratchProgDonnees.listValPos = [['1'], ['2'], ['3'], ['4'], ['5'], ['6'], ['7'], ['8'], ['9'], ['10']]

        break
      case 'prectangle': scratch.params.scratchProgDonnees.menu[1].contenu.push({ type: 'data_variable', variable: [{ nom: 'VARIABLE', nomvar: 'longueur', id: 'aaaaaaa' }] })
        scratch.params.scratchProgDonnees.menu[1].contenu.push({ type: 'data_variable', variable: [{ nom: 'VARIABLE', nomvar: 'largeur', id: 'bbbbbbb' }] })
        scratch.params.scratchProgDonnees.progdeb.push({ type: 'Get_us', value: [{ nom: 'STRING', contenu: { valeur: 'Donne la longueur d’un rectangle (en cm).', editable: 'false', deletable: 'false', movable: 'false' } }], editable: 'false', deletable: 'false', movable: 'false' })
        scratch.params.scratchProgDonnees.progdeb.push({ type: 'data_setvariableto', editable: 'false', deletable: 'false', movable: 'false', variable: [{ nom: 'VARIABLE', nomvar: 'longueur', id: 'aaaaaaa' }], value: [{ nom: 'VALUE', editable: 'false', deletable: 'false', movable: 'false', contenu: { reponse: '', editable: 'false', deletable: 'false', movable: 'false' } }] })
        scratch.params.scratchProgDonnees.progdeb.push({ type: 'Get_us', value: [{ nom: 'STRING', contenu: { valeur: 'Donne la largeur du rectangle (en cm).', editable: 'false', deletable: 'false', movable: 'false' } }], editable: 'false', deletable: 'false', movable: 'false' })
        scratch.params.scratchProgDonnees.progdeb.push({ type: 'data_setvariableto', editable: 'false', deletable: 'false', movable: 'false', variable: [{ nom: 'VARIABLE', nomvar: 'largeur', id: 'bbbbbbb' }], value: [{ nom: 'VALUE', editable: 'false', deletable: 'false', movable: 'false', contenu: { reponse: '', editable: 'false', deletable: 'false', movable: 'false' } }] })
        scratch.params.scratchProgDonnees.progdeb.push({ type: 'dire', editable: 'false', deletable: 'false', movable: 'false', value: [{ nom: 'DELTA', editable: 'false', deletable: 'false', movable: 'false', contenu: { valeur: 'Voici son périmètre (en cm).', editable: 'false', deletable: 'false', movable: 'false' } }, { nom: 'TEMPS', contenu: { valeur: 2, editable: 'false', deletable: 'false', movable: 'false' } }] })
        scratch.params.scratchProgDonnees.progdeb.push({ type: 'dire', editable: 'false', deletable: 'false', movable: 'false', value: [{ nom: 'DELTA', contenu: { valeur: 'PARTIE A COMPLETER' } }, { nom: 'TEMPS', contenu: { valeur: 2, editable: 'false', deletable: 'false', movable: 'false' } }] })

        scratch.params.scratchProgDonnees.lesvarbonnes.push({ nom: 'longueur', val: 0, id: 'aaaaaaa' })
        scratch.params.scratchProgDonnees.lesvarbonnes.push({ nom: 'largeur', val: 0, id: 'bbbbbbb' })
        stor.foncverif = stor.Prectangle
        larep = { operateur: '*', value: [{ nom: 'NUM1', contenu: { valeur: 2 } }, { nom: 'NUM2', contenu: { operateur: '+', value: [{ nom: 'NUM1', contenu: { type: 'data_variable', variable: [{ nom: 'VARIABLE', nomvar: 'longueur', id: 'aaaaaaa' }] } }, { nom: 'NUM2', contenu: { type: 'data_variable', variable: [{ nom: 'VARIABLE', nomvar: 'largeur', id: 'bbbbbbb' }] } }] } }] }
        bufmen = 'le périmètre d’un rectangle'

        scratch.params.scratchProgDonnees.lesordres.push({ type: 'Get_us', value: [{ nom: 'STRING', contenu: { valeur: 'Donne la longueur d’un rectangle (en cm).', editable: 'false', deletable: 'false', movable: 'false' } }], editable: 'false', deletable: 'false', movable: 'false' })
        scratch.params.scratchProgDonnees.lesordres.push({ type: 'data_setvariableto', editable: 'false', deletable: 'false', movable: 'false', variable: [{ nom: 'VARIABLE', nomvar: 'longueur', id: 'aaaaaaa' }], value: [{ nom: 'VALUE', editable: 'false', deletable: 'false', movable: 'false', contenu: { reponse: '', editable: 'false', deletable: 'false', movable: 'false' } }] })
        scratch.params.scratchProgDonnees.lesordres.push({ type: 'Get_us', value: [{ nom: 'STRING', contenu: { valeur: 'Donne la largeur du rectangle (en cm).', editable: 'false', deletable: 'false', movable: 'false' } }], editable: 'false', deletable: 'false', movable: 'false' })
        scratch.params.scratchProgDonnees.lesordres.push({ type: 'data_setvariableto', editable: 'false', deletable: 'false', movable: 'false', variable: [{ nom: 'VARIABLE', nomvar: 'largeur', id: 'bbbbbbb' }], value: [{ nom: 'VALUE', editable: 'false', deletable: 'false', movable: 'false', contenu: { reponse: '', editable: 'false', deletable: 'false', movable: 'false' } }] })
        scratch.params.scratchProgDonnees.lesordres.push({ type: 'dire', editable: 'false', deletable: 'false', movable: 'false', value: [{ nom: 'DELTA', editable: 'false', deletable: 'false', movable: 'false', contenu: { valeur: 'Voici son périmètre (en cm).', editable: 'false', deletable: 'false', movable: 'false' } }, { nom: 'TEMPS', contenu: { valeur: 2, editable: 'false', deletable: 'false', movable: 'false' } }] })
        scratch.params.scratchProgDonnees.listValPos = [['2', '3'], ['2', '4'], ['3', '4'], ['4', '7'], ['5', '6'], ['6', '4'], ['7', '10'], ['8', '5'], ['9', '4'], ['10', '12']]

        break
      case 'arectangle': scratch.params.scratchProgDonnees.menu[1].contenu.push({ type: 'data_variable', variable: [{ nom: 'VARIABLE', nomvar: 'longueur', id: 'aaaaaaa' }] })
        scratch.params.scratchProgDonnees.menu[1].contenu.push({ type: 'data_variable', variable: [{ nom: 'VARIABLE', nomvar: 'largeur', id: 'bbbbbbb' }] })
        scratch.params.scratchProgDonnees.progdeb.push({ type: 'Get_us', value: [{ nom: 'STRING', contenu: { valeur: 'Donne la longueur d’un rectangle (en cm).', editable: 'false', deletable: 'false', movable: 'false' } }], editable: 'false', deletable: 'false', movable: 'false' })
        scratch.params.scratchProgDonnees.progdeb.push({ type: 'data_setvariableto', editable: 'false', deletable: 'false', movable: 'false', variable: [{ nom: 'VARIABLE', nomvar: 'longueur', id: 'aaaaaaa' }], value: [{ nom: 'VALUE', editable: 'false', deletable: 'false', movable: 'false', contenu: { reponse: '', editable: 'false', deletable: 'false', movable: 'false' } }] })
        scratch.params.scratchProgDonnees.progdeb.push({ type: 'Get_us', value: [{ nom: 'STRING', contenu: { valeur: 'Donne la largeur du rectangle (en cm).', editable: 'false', deletable: 'false', movable: 'false' } }], editable: 'false', deletable: 'false', movable: 'false' })
        scratch.params.scratchProgDonnees.progdeb.push({ type: 'data_setvariableto', editable: 'false', deletable: 'false', movable: 'false', variable: [{ nom: 'VARIABLE', nomvar: 'largeur', id: 'bbbbbbb' }], value: [{ nom: 'VALUE', editable: 'false', deletable: 'false', movable: 'false', contenu: { reponse: '', editable: 'false', deletable: 'false', movable: 'false' } }] })
        scratch.params.scratchProgDonnees.progdeb.push({ type: 'dire', editable: 'false', deletable: 'false', movable: 'false', value: [{ nom: 'DELTA', editable: 'false', deletable: 'false', movable: 'false', contenu: { valeur: 'Voici son aire (en cm²).', editable: 'false', deletable: 'false', movable: 'false' } }, { nom: 'TEMPS', contenu: { valeur: 2, editable: 'false', deletable: 'false', movable: 'false' } }] })
        scratch.params.scratchProgDonnees.progdeb.push({ type: 'dire', editable: 'false', deletable: 'false', movable: 'false', value: [{ nom: 'DELTA', contenu: { valeur: 'PARTIE A COMPLETER' } }, { nom: 'TEMPS', contenu: { valeur: 2, editable: 'false', deletable: 'false', movable: 'false' } }] })

        scratch.params.scratchProgDonnees.lesvarbonnes.push({ nom: 'longueur', val: 0, id: 'aaaaaaa' })
        scratch.params.scratchProgDonnees.lesvarbonnes.push({ nom: 'largeur', val: 0, id: 'bbbbbbb' })
        stor.foncverif = stor.Arectangle
        larep = { operateur: '*', value: [{ nom: 'NUM1', contenu: { type: 'data_variable', variable: [{ nom: 'VARIABLE', nomvar: 'longueur', id: 'aaaaaaa' }] } }, { nom: 'NUM2', contenu: { type: 'data_variable', variable: [{ nom: 'VARIABLE', nomvar: 'largeur', id: 'bbbbbbb' }] } }] }
        bufmen = 'l’aire d’un rectangle'

        scratch.params.scratchProgDonnees.lesordres.push({ type: 'Get_us', value: [{ nom: 'STRING', contenu: { valeur: 'Donne la longueur d’un rectangle (en cm).', editable: 'false', deletable: 'false', movable: 'false' } }], editable: 'false', deletable: 'false', movable: 'false' })
        scratch.params.scratchProgDonnees.lesordres.push({ type: 'data_setvariableto', editable: 'false', deletable: 'false', movable: 'false', variable: [{ nom: 'VARIABLE', nomvar: 'longueur', id: 'aaaaaaa' }], value: [{ nom: 'VALUE', editable: 'false', deletable: 'false', movable: 'false', contenu: { reponse: '', editable: 'false', deletable: 'false', movable: 'false' } }] })
        scratch.params.scratchProgDonnees.lesordres.push({ type: 'Get_us', value: [{ nom: 'STRING', contenu: { valeur: 'Donne la largeur du rectangle (en cm).', editable: 'false', deletable: 'false', movable: 'false' } }], editable: 'false', deletable: 'false', movable: 'false' })
        scratch.params.scratchProgDonnees.lesordres.push({ type: 'data_setvariableto', editable: 'false', deletable: 'false', movable: 'false', variable: [{ nom: 'VARIABLE', nomvar: 'largeur', id: 'bbbbbbb' }], value: [{ nom: 'VALUE', editable: 'false', deletable: 'false', movable: 'false', contenu: { reponse: '', editable: 'false', deletable: 'false', movable: 'false' } }] })
        scratch.params.scratchProgDonnees.lesordres.push({ type: 'dire', editable: 'false', deletable: 'false', movable: 'false', value: [{ nom: 'DELTA', editable: 'false', deletable: 'false', movable: 'false', contenu: { valeur: 'Voici son aire (en cm²).', editable: 'false', deletable: 'false', movable: 'false' } }, { nom: 'TEMPS', contenu: { valeur: 2, editable: 'false', deletable: 'false', movable: 'false' } }] })
        scratch.params.scratchProgDonnees.listValPos = [['2', '3'], ['2', '4'], ['3', '4'], ['4', '7'], ['5', '6'], ['6', '4'], ['7', '10'], ['8', '5'], ['9', '4'], ['10', '12']]

        break
      case 'plosange': scratch.params.scratchProgDonnees.menu[1].contenu.push({ type: 'data_variable', variable: [{ nom: 'VARIABLE', nomvar: 'cote', id: 'aaaaaaa' }] })
        scratch.params.scratchProgDonnees.progdeb.push({ type: 'Get_us', value: [{ nom: 'STRING', contenu: { valeur: 'Donne la longueur du côté d’un losange (en cm).', editable: 'false', deletable: 'false', movable: 'false' } }], editable: 'false', deletable: 'false', movable: 'false' })
        scratch.params.scratchProgDonnees.progdeb.push({ type: 'data_setvariableto', editable: 'false', deletable: 'false', movable: 'false', variable: [{ nom: 'VARIABLE', nomvar: 'cote', id: 'aaaaaaa' }], value: [{ nom: 'VALUE', editable: 'false', deletable: 'false', movable: 'false', contenu: { reponse: '', editable: 'false', deletable: 'false', movable: 'false' } }] })
        scratch.params.scratchProgDonnees.progdeb.push({ type: 'dire', editable: 'false', deletable: 'false', movable: 'false', value: [{ nom: 'DELTA', editable: 'false', deletable: 'false', movable: 'false', contenu: { valeur: 'Voici son périmètre (en cm).', editable: 'false', deletable: 'false', movable: 'false' } }, { nom: 'TEMPS', contenu: { valeur: 2, editable: 'false', deletable: 'false', movable: 'false' } }] })
        scratch.params.scratchProgDonnees.progdeb.push({ type: 'dire', editable: 'false', deletable: 'false', movable: 'false', value: [{ nom: 'DELTA', contenu: { valeur: 'PARTIE A COMPLETER' } }, { nom: 'TEMPS', contenu: { valeur: 2, editable: 'false', deletable: 'false', movable: 'false' } }] })

        scratch.params.scratchProgDonnees.lesvarbonnes.push({ nom: 'cote', val: 0, id: 'aaaaaaa' })
        stor.foncverif = stor.Plosange
        larep = { operateur: '*', value: [{ nom: 'NUM1', contenu: { valeur: 4 } }, { nom: 'NUM2', contenu: { type: 'data_variable', variable: [{ nom: 'VARIABLE', nomvar: 'cote', id: 'aaaaaaa' }] } }] }
        bufmen = 'le périmètre d’un losange'

        scratch.params.scratchProgDonnees.lesordres.push({ type: 'Get_us', value: [{ nom: 'STRING', contenu: { valeur: 'Donne la longueur du côté d’un losange (en cm).', editable: 'false', deletable: 'false', movable: 'false' } }], editable: 'false', deletable: 'false', movable: 'false' })
        scratch.params.scratchProgDonnees.lesordres.push({ type: 'data_setvariableto', editable: 'false', deletable: 'false', movable: 'false', variable: [{ nom: 'VARIABLE', nomvar: 'cote', id: 'aaaaaaa' }], value: [{ nom: 'VALUE', editable: 'false', deletable: 'false', movable: 'false', contenu: { reponse: '', editable: 'false', deletable: 'false', movable: 'false' } }] })
        scratch.params.scratchProgDonnees.lesordres.push({ type: 'dire', editable: 'false', deletable: 'false', movable: 'false', value: [{ nom: 'DELTA', editable: 'false', deletable: 'false', movable: 'false', contenu: { valeur: 'Voici son périmètre (en cm).', editable: 'false', deletable: 'false', movable: 'false' } }, { nom: 'TEMPS', contenu: { valeur: 2, editable: 'false', deletable: 'false', movable: 'false' } }] })
        scratch.params.scratchProgDonnees.listValPos = [['1'], ['2'], ['3'], ['4'], ['5'], ['6'], ['7'], ['8'], ['9'], ['10']]

        break
      case 'pparallelo': scratch.params.scratchProgDonnees.menu[1].contenu.push({ type: 'data_variable', variable: [{ nom: 'VARIABLE', nomvar: 'longueur', id: 'aaaaaaa' }] })
        scratch.params.scratchProgDonnees.menu[1].contenu.push({ type: 'data_variable', variable: [{ nom: 'VARIABLE', nomvar: 'largeur', id: 'bbbbbbb' }] })
        scratch.params.scratchProgDonnees.progdeb.push({ type: 'Get_us', value: [{ nom: 'STRING', contenu: { valeur: 'Donne la longueur d’un parallélogramme (en cm).', editable: 'false', deletable: 'false', movable: 'false' } }], editable: 'false', deletable: 'false', movable: 'false' })
        scratch.params.scratchProgDonnees.progdeb.push({ type: 'data_setvariableto', editable: 'false', deletable: 'false', movable: 'false', variable: [{ nom: 'VARIABLE', nomvar: 'longueur', id: 'aaaaaaa' }], value: [{ nom: 'VALUE', editable: 'false', deletable: 'false', movable: 'false', contenu: { reponse: '', editable: 'false', deletable: 'false', movable: 'false' } }] })
        scratch.params.scratchProgDonnees.progdeb.push({ type: 'Get_us', value: [{ nom: 'STRING', contenu: { valeur: 'Donne la largeur du parallélogramme (en cm).', editable: 'false', deletable: 'false', movable: 'false' } }], editable: 'false', deletable: 'false', movable: 'false' })
        scratch.params.scratchProgDonnees.progdeb.push({ type: 'data_setvariableto', editable: 'false', deletable: 'false', movable: 'false', variable: [{ nom: 'VARIABLE', nomvar: 'largeur', id: 'bbbbbbb' }], value: [{ nom: 'VALUE', editable: 'false', deletable: 'false', movable: 'false', contenu: { reponse: '', editable: 'false', deletable: 'false', movable: 'false' } }] })
        scratch.params.scratchProgDonnees.progdeb.push({ type: 'dire', editable: 'false', deletable: 'false', movable: 'false', value: [{ nom: 'DELTA', editable: 'false', deletable: 'false', movable: 'false', contenu: { valeur: 'Voici son périmètre (en cm).', editable: 'false', deletable: 'false', movable: 'false' } }, { nom: 'TEMPS', contenu: { valeur: 2, editable: 'false', deletable: 'false', movable: 'false' } }] })
        scratch.params.scratchProgDonnees.progdeb.push({ type: 'dire', editable: 'false', deletable: 'false', movable: 'false', value: [{ nom: 'DELTA', contenu: { valeur: 'PARTIE A COMPLETER' } }, { nom: 'TEMPS', contenu: { valeur: 2, editable: 'false', deletable: 'false', movable: 'false' } }] })

        scratch.params.scratchProgDonnees.lesvarbonnes.push({ nom: 'longueur', val: 0, id: 'aaaaaaa' })
        scratch.params.scratchProgDonnees.lesvarbonnes.push({ nom: 'largeur', val: 0, id: 'bbbbbbb' })
        stor.foncverif = stor.Pparallelo
        larep = { operateur: '*', value: [{ nom: 'NUM1', contenu: { valeur: 2 } }, { nom: 'NUM2', contenu: { operateur: '+', value: [{ nom: 'NUM1', contenu: { type: 'data_variable', variable: [{ nom: 'VARIABLE', nomvar: 'longueur', id: 'aaaaaaa' }] } }, { nom: 'NUM2', contenu: { type: 'data_variable', variable: [{ nom: 'VARIABLE', nomvar: 'largeur', id: 'bbbbbbb' }] } }] } }] }
        bufmen = 'le périmètre d’un parallélogramme'

        scratch.params.scratchProgDonnees.lesordres.push({ type: 'Get_us', value: [{ nom: 'STRING', contenu: { valeur: 'Donne la longueur d’un parallélogramme (en cm).', editable: 'false', deletable: 'false', movable: 'false' } }], editable: 'false', deletable: 'false', movable: 'false' })
        scratch.params.scratchProgDonnees.lesordres.push({ type: 'data_setvariableto', editable: 'false', deletable: 'false', movable: 'false', variable: [{ nom: 'VARIABLE', nomvar: 'longueur', id: 'aaaaaaa' }], value: [{ nom: 'VALUE', editable: 'false', deletable: 'false', movable: 'false', contenu: { reponse: '', editable: 'false', deletable: 'false', movable: 'false' } }] })
        scratch.params.scratchProgDonnees.lesordres.push({ type: 'Get_us', value: [{ nom: 'STRING', contenu: { valeur: 'Donne la largeur du parallélogramme (en cm).', editable: 'false', deletable: 'false', movable: 'false' } }], editable: 'false', deletable: 'false', movable: 'false' })
        scratch.params.scratchProgDonnees.lesordres.push({ type: 'data_setvariableto', editable: 'false', deletable: 'false', movable: 'false', variable: [{ nom: 'VARIABLE', nomvar: 'largeur', id: 'bbbbbbb' }], value: [{ nom: 'VALUE', editable: 'false', deletable: 'false', movable: 'false', contenu: { reponse: '', editable: 'false', deletable: 'false', movable: 'false' } }] })
        scratch.params.scratchProgDonnees.lesordres.push({ type: 'dire', editable: 'false', deletable: 'false', movable: 'false', value: [{ nom: 'DELTA', editable: 'false', deletable: 'false', movable: 'false', contenu: { valeur: 'Voici son périmètre (en cm).', editable: 'false', deletable: 'false', movable: 'false' } }, { nom: 'TEMPS', contenu: { valeur: 2, editable: 'false', deletable: 'false', movable: 'false' } }] })
        scratch.params.scratchProgDonnees.listValPos = [['2', '3'], ['2', '4'], ['3', '4'], ['4', '7'], ['5', '6'], ['6', '4'], ['7', '10'], ['8', '5'], ['9', '4'], ['10', '12']]
        break
      case 'aparallelo':scratch.params.scratchProgDonnees.menu[1].contenu.push({ type: 'data_variable', variable: [{ nom: 'VARIABLE', nomvar: 'base', id: 'aaaaaaa' }] })
        scratch.params.scratchProgDonnees.menu[1].contenu.push({ type: 'data_variable', variable: [{ nom: 'VARIABLE', nomvar: 'hauteur', id: 'bbbbbbb' }] })
        scratch.params.scratchProgDonnees.progdeb.push({ type: 'Get_us', value: [{ nom: 'STRING', contenu: { valeur: 'Donne la longueur de la base d’un parallélogramme (en cm).', editable: 'false', deletable: 'false', movable: 'false' } }], editable: 'false', deletable: 'false', movable: 'false' })
        scratch.params.scratchProgDonnees.progdeb.push({ type: 'data_setvariableto', editable: 'false', deletable: 'false', movable: 'false', variable: [{ nom: 'VARIABLE', nomvar: 'base', id: 'aaaaaaa' }], value: [{ nom: 'VALUE', editable: 'false', deletable: 'false', movable: 'false', contenu: { reponse: '', editable: 'false', deletable: 'false', movable: 'false' } }] })
        scratch.params.scratchProgDonnees.progdeb.push({ type: 'Get_us', value: [{ nom: 'STRING', contenu: { valeur: 'Donne la longueur de la hauteur associée (en cm).', editable: 'false', deletable: 'false', movable: 'false' } }], editable: 'false', deletable: 'false', movable: 'false' })
        scratch.params.scratchProgDonnees.progdeb.push({ type: 'data_setvariableto', editable: 'false', deletable: 'false', movable: 'false', variable: [{ nom: 'VARIABLE', nomvar: 'hauteur', id: 'bbbbbbb' }], value: [{ nom: 'VALUE', editable: 'false', deletable: 'false', movable: 'false', contenu: { reponse: '', editable: 'false', deletable: 'false', movable: 'false' } }] })
        scratch.params.scratchProgDonnees.progdeb.push({ type: 'dire', editable: 'false', deletable: 'false', movable: 'false', value: [{ nom: 'DELTA', editable: 'false', deletable: 'false', movable: 'false', contenu: { valeur: 'Voici son aire (en cm²).', editable: 'false', deletable: 'false', movable: 'false' } }, { nom: 'TEMPS', contenu: { valeur: 2, editable: 'false', deletable: 'false', movable: 'false' } }] })
        scratch.params.scratchProgDonnees.progdeb.push({ type: 'dire', editable: 'false', deletable: 'false', movable: 'false', value: [{ nom: 'DELTA', contenu: { valeur: 'PARTIE A COMPLETER' } }, { nom: 'TEMPS', contenu: { valeur: 2, editable: 'false', deletable: 'false', movable: 'false' } }] })

        scratch.params.scratchProgDonnees.lesvarbonnes.push({ nom: 'base', val: 0, id: 'aaaaaaa' })
        scratch.params.scratchProgDonnees.lesvarbonnes.push({ nom: 'hauteur', val: 0, id: 'bbbbbbb' })
        stor.foncverif = stor.Aparallelo
        larep = { operateur: '*', value: [{ nom: 'NUM1', contenu: { type: 'data_variable', variable: [{ nom: 'VARIABLE', nomvar: 'base', id: 'aaaaaaa' }] } }, { nom: 'NUM2', contenu: { type: 'data_variable', variable: [{ nom: 'VARIABLE', nomvar: 'hauteur', id: 'bbbbbbb' }] } }] }
        bufmen = 'l’aire d’un parallélogramme'

        scratch.params.scratchProgDonnees.lesordres.push({ type: 'Get_us', value: [{ nom: 'STRING', contenu: { valeur: 'Donne la longueur de la base d’un parallélogramme (en cm).', editable: 'false', deletable: 'false', movable: 'false' } }], editable: 'false', deletable: 'false', movable: 'false' })
        scratch.params.scratchProgDonnees.lesordres.push({ type: 'data_setvariableto', editable: 'false', deletable: 'false', movable: 'false', variable: [{ nom: 'VARIABLE', nomvar: 'base', id: 'aaaaaaa' }], value: [{ nom: 'VALUE', editable: 'false', deletable: 'false', movable: 'false', contenu: { reponse: '', editable: 'false', deletable: 'false', movable: 'false' } }] })
        scratch.params.scratchProgDonnees.lesordres.push({ type: 'Get_us', value: [{ nom: 'STRING', contenu: { valeur: 'Donne la longueur de la hauteur associée (en cm).', editable: 'false', deletable: 'false', movable: 'false' } }], editable: 'false', deletable: 'false', movable: 'false' })
        scratch.params.scratchProgDonnees.lesordres.push({ type: 'data_setvariableto', editable: 'false', deletable: 'false', movable: 'false', variable: [{ nom: 'VARIABLE', nomvar: 'hauteur', id: 'bbbbbbb' }], value: [{ nom: 'VALUE', editable: 'false', deletable: 'false', movable: 'false', contenu: { reponse: '', editable: 'false', deletable: 'false', movable: 'false' } }] })
        scratch.params.scratchProgDonnees.lesordres.push({ type: 'dire', editable: 'false', deletable: 'false', movable: 'false', value: [{ nom: 'DELTA', editable: 'false', deletable: 'false', movable: 'false', contenu: { valeur: 'Voici son aire (en cm²).', editable: 'false', deletable: 'false', movable: 'false' } }, { nom: 'TEMPS', contenu: { valeur: 2, editable: 'false', deletable: 'false', movable: 'false' } }] })
        scratch.params.scratchProgDonnees.listValPos = [['2', '3'], ['2', '4'], ['3', '4'], ['4', '7'], ['5', '6'], ['6', '4'], ['7', '10'], ['8', '5'], ['9', '4'], ['10', '12']]

        break
      case 'pcube': scratch.params.scratchProgDonnees.menu[1].contenu.push({ type: 'data_variable', variable: [{ nom: 'VARIABLE', nomvar: 'cote', id: 'aaaaaaa' }] })
        scratch.params.scratchProgDonnees.progdeb.push({ type: 'Get_us', value: [{ nom: 'STRING', contenu: { valeur: 'Donne la longueur du côté d’un cube (en cm).', editable: 'false', deletable: 'false', movable: 'false' } }], editable: 'false', deletable: 'false', movable: 'false' })
        scratch.params.scratchProgDonnees.progdeb.push({ type: 'data_setvariableto', editable: 'false', deletable: 'false', movable: 'false', variable: [{ nom: 'VARIABLE', nomvar: 'cote', id: 'aaaaaaa' }], value: [{ nom: 'VALUE', editable: 'false', deletable: 'false', movable: 'false', contenu: { reponse: '', editable: 'false', deletable: 'false', movable: 'false' } }] })
        scratch.params.scratchProgDonnees.progdeb.push({ type: 'dire', editable: 'false', deletable: 'false', movable: 'false', value: [{ nom: 'DELTA', editable: 'false', deletable: 'false', movable: 'false', contenu: { valeur: 'Voici son périmètre (en cm).', editable: 'false', deletable: 'false', movable: 'false' } }, { nom: 'TEMPS', contenu: { valeur: 2, editable: 'false', deletable: 'false', movable: 'false' } }] })
        scratch.params.scratchProgDonnees.progdeb.push({ type: 'dire', editable: 'false', deletable: 'false', movable: 'false', value: [{ nom: 'DELTA', contenu: { valeur: 'PARTIE A COMPLETER' } }, { nom: 'TEMPS', contenu: { valeur: 2, editable: 'false', deletable: 'false', movable: 'false' } }] })

        scratch.params.scratchProgDonnees.lesvarbonnes.push({ nom: 'cote', val: 0, id: 'aaaaaaa' })
        stor.foncverif = stor.Pcube
        larep = { operateur: '*', value: [{ nom: 'NUM1', contenu: { valeur: 12 } }, { nom: 'NUM2', contenu: { type: 'data_variable', variable: [{ nom: 'VARIABLE', nomvar: 'cote', id: 'aaaaaaa' }] } }] }
        bufmen = 'le périmètre d’un cube'

        scratch.params.scratchProgDonnees.lesordres.push({ type: 'Get_us', value: [{ nom: 'STRING', contenu: { valeur: 'Donne la longueur du côté d’un cube (en cm).', editable: 'false', deletable: 'false', movable: 'false' } }], editable: 'false', deletable: 'false', movable: 'false' })
        scratch.params.scratchProgDonnees.lesordres.push({ type: 'data_setvariableto', editable: 'false', deletable: 'false', movable: 'false', variable: [{ nom: 'VARIABLE', nomvar: 'cote', id: 'aaaaaaa' }], value: [{ nom: 'VALUE', editable: 'false', deletable: 'false', movable: 'false', contenu: { reponse: '', editable: 'false', deletable: 'false', movable: 'false' } }] })
        scratch.params.scratchProgDonnees.lesordres.push({ type: 'dire', editable: 'false', deletable: 'false', movable: 'false', value: [{ nom: 'DELTA', editable: 'false', deletable: 'false', movable: 'false', contenu: { valeur: 'Voici son périmètre (en cm).', editable: 'false', deletable: 'false', movable: 'false' } }, { nom: 'TEMPS', contenu: { valeur: 2, editable: 'false', deletable: 'false', movable: 'false' } }] })
        scratch.params.scratchProgDonnees.listValPos = [['1'], ['2'], ['3'], ['4'], ['5'], ['6'], ['7'], ['8'], ['9'], ['10']]
        break
      case 'acube': scratch.params.scratchProgDonnees.menu[1].contenu.push({ type: 'data_variable', variable: [{ nom: 'VARIABLE', nomvar: 'cote', id: 'aaaaaaa' }] })
        scratch.params.scratchProgDonnees.progdeb.push({ type: 'Get_us', value: [{ nom: 'STRING', contenu: { valeur: 'Donne la longueur du côté d’un cube (en cm).', editable: 'false', deletable: 'false', movable: 'false' } }], editable: 'false', deletable: 'false', movable: 'false' })
        scratch.params.scratchProgDonnees.progdeb.push({ type: 'data_setvariableto', editable: 'false', deletable: 'false', movable: 'false', variable: [{ nom: 'VARIABLE', nomvar: 'cote', id: 'aaaaaaa' }], value: [{ nom: 'VALUE', editable: 'false', deletable: 'false', movable: 'false', contenu: { reponse: '', editable: 'false', deletable: 'false', movable: 'false' } }] })
        scratch.params.scratchProgDonnees.progdeb.push({ type: 'dire', editable: 'false', deletable: 'false', movable: 'false', value: [{ nom: 'DELTA', editable: 'false', deletable: 'false', movable: 'false', contenu: { valeur: 'Voici son aire (en cm²).', editable: 'false', deletable: 'false', movable: 'false' } }, { nom: 'TEMPS', contenu: { valeur: 2, editable: 'false', deletable: 'false', movable: 'false' } }] })
        scratch.params.scratchProgDonnees.progdeb.push({ type: 'dire', editable: 'false', deletable: 'false', movable: 'false', value: [{ nom: 'DELTA', contenu: { valeur: 'PARTIE A COMPLETER' } }, { nom: 'TEMPS', contenu: { valeur: 2, editable: 'false', deletable: 'false', movable: 'false' } }] })

        scratch.params.scratchProgDonnees.lesvarbonnes.push({ nom: 'cote', val: 0, id: 'aaaaaaa' })
        stor.foncverif = stor.Acube
        larep = { operateur: '*', value: [{ nom: 'NUM1', contenu: { operateur: '*', value: [{ nom: 'NUM1', contenu: { type: 'data_variable', variable: [{ nom: 'VARIABLE', nomvar: 'cote', id: 'aaaaaaa' }] } }, { nom: 'NUM2', contenu: { type: 'data_variable', variable: [{ nom: 'VARIABLE', nomvar: 'cote', id: 'aaaaaaa' }] } }] } }, { nom: 'NUM2', contenu: { valeur: 12 } }] }
        bufmen = 'l’aire d’un cube'

        scratch.params.scratchProgDonnees.lesordres.push({ type: 'Get_us', value: [{ nom: 'STRING', contenu: { valeur: 'Donne la longueur du côté d’un cube (en cm).', editable: 'false', deletable: 'false', movable: 'false' } }], editable: 'false', deletable: 'false', movable: 'false' })
        scratch.params.scratchProgDonnees.lesordres.push({ type: 'data_setvariableto', editable: 'false', deletable: 'false', movable: 'false', variable: [{ nom: 'VARIABLE', nomvar: 'cote', id: 'aaaaaaa' }], value: [{ nom: 'VALUE', editable: 'false', deletable: 'false', movable: 'false', contenu: { reponse: '', editable: 'false', deletable: 'false', movable: 'false' } }] })
        scratch.params.scratchProgDonnees.lesordres.push({ type: 'dire', editable: 'false', deletable: 'false', movable: 'false', value: [{ nom: 'DELTA', editable: 'false', deletable: 'false', movable: 'false', contenu: { valeur: 'Voici son aire (en cm²).', editable: 'false', deletable: 'false', movable: 'false' } }, { nom: 'TEMPS', contenu: { valeur: 2, editable: 'false', deletable: 'false', movable: 'false' } }] })
        scratch.params.scratchProgDonnees.listValPos = [['1'], ['2'], ['3'], ['4'], ['5'], ['6'], ['7'], ['8'], ['9'], ['10']]
        break
      case 'vcube': scratch.params.scratchProgDonnees.menu[1].contenu.push({ type: 'data_variable', variable: [{ nom: 'VARIABLE', nomvar: 'cote', id: 'aaaaaaa' }] })
        scratch.params.scratchProgDonnees.progdeb.push({ type: 'Get_us', value: [{ nom: 'STRING', contenu: { valeur: 'Donne la longueur du côté d’un cube (en cm).', editable: 'false', deletable: 'false', movable: 'false' } }], editable: 'false', deletable: 'false', movable: 'false' })
        scratch.params.scratchProgDonnees.progdeb.push({ type: 'data_setvariableto', editable: 'false', deletable: 'false', movable: 'false', variable: [{ nom: 'VARIABLE', nomvar: 'cote', id: 'aaaaaaa' }], value: [{ nom: 'VALUE', editable: 'false', deletable: 'false', movable: 'false', contenu: { reponse: '', editable: 'false', deletable: 'false', movable: 'false' } }] })
        scratch.params.scratchProgDonnees.progdeb.push({ type: 'dire', editable: 'false', deletable: 'false', movable: 'false', value: [{ nom: 'DELTA', editable: 'false', deletable: 'false', movable: 'false', contenu: { valeur: 'Voici son volume (en cm³).', editable: 'false', deletable: 'false', movable: 'false' } }, { nom: 'TEMPS', contenu: { valeur: 2, editable: 'false', deletable: 'false', movable: 'false' } }] })
        scratch.params.scratchProgDonnees.progdeb.push({ type: 'dire', editable: 'false', deletable: 'false', movable: 'false', value: [{ nom: 'DELTA', contenu: { valeur: 'PARTIE A COMPLETER' } }, { nom: 'TEMPS', contenu: { valeur: 2, editable: 'false', deletable: 'false', movable: 'false' } }] })

        scratch.params.scratchProgDonnees.lesvarbonnes.push({ nom: 'cote', val: 0, id: 'aaaaaaa' })
        stor.foncverif = stor.Vcube
        larep = { operateur: '*', value: [{ nom: 'NUM1', contenu: { operateur: '*', value: [{ nom: 'NUM1', contenu: { type: 'data_variable', variable: [{ nom: 'VARIABLE', nomvar: 'cote', id: 'aaaaaaa' }] } }, { nom: 'NUM2', contenu: { type: 'data_variable', variable: [{ nom: 'VARIABLE', nomvar: 'cote', id: 'aaaaaaa' }] } }] } }, { nom: 'NUM2', contenu: { type: 'data_variable', variable: [{ nom: 'VARIABLE', nomvar: 'cote', id: 'aaaaaaa' }] } }] }
        bufmen = 'le volume d’un cube'

        scratch.params.scratchProgDonnees.lesordres.push({ type: 'Get_us', value: [{ nom: 'STRING', contenu: { valeur: 'Donne la longueur du côté d’un cube (en cm).', editable: 'false', deletable: 'false', movable: 'false' } }], editable: 'false', deletable: 'false', movable: 'false' })
        scratch.params.scratchProgDonnees.lesordres.push({ type: 'data_setvariableto', editable: 'false', deletable: 'false', movable: 'false', variable: [{ nom: 'VARIABLE', nomvar: 'cote', id: 'aaaaaaa' }], value: [{ nom: 'VALUE', editable: 'false', deletable: 'false', movable: 'false', contenu: { reponse: '', editable: 'false', deletable: 'false', movable: 'false' } }] })
        scratch.params.scratchProgDonnees.lesordres.push({ type: 'dire', editable: 'false', deletable: 'false', movable: 'false', value: [{ nom: 'DELTA', editable: 'false', deletable: 'false', movable: 'false', contenu: { valeur: 'Voici son volume (en cm³).', editable: 'false', deletable: 'false', movable: 'false' } }, { nom: 'TEMPS', contenu: { valeur: 2, editable: 'false', deletable: 'false', movable: 'false' } }] })
        scratch.params.scratchProgDonnees.listValPos = [['1'], ['2'], ['3'], ['4'], ['5'], ['6'], ['7'], ['8'], ['9'], ['10']]
        break
      case 'pcercle': scratch.params.scratchProgDonnees.menu[1].contenu.push({ type: 'data_variable', variable: [{ nom: 'VARIABLE', nomvar: 'rayon', id: 'aaaaaaa' }] })
        scratch.params.scratchProgDonnees.menu[1].contenu.push({ type: 'pi' })
        scratch.params.scratchProgDonnees.progdeb.push({ type: 'Get_us', value: [{ nom: 'STRING', contenu: { valeur: 'Donne le rayon d’un cercle (en cm).', editable: 'false', deletable: 'false', movable: 'false' } }], editable: 'false', deletable: 'false', movable: 'false' })
        scratch.params.scratchProgDonnees.progdeb.push({ type: 'data_setvariableto', editable: 'false', deletable: 'false', movable: 'false', variable: [{ nom: 'VARIABLE', nomvar: 'rayon', id: 'aaaaaaa' }], value: [{ nom: 'VALUE', editable: 'false', deletable: 'false', movable: 'false', contenu: { reponse: '', editable: 'false', deletable: 'false', movable: 'false' } }] })
        scratch.params.scratchProgDonnees.progdeb.push({ type: 'dire', editable: 'false', deletable: 'false', movable: 'false', value: [{ nom: 'DELTA', editable: 'false', deletable: 'false', movable: 'false', contenu: { valeur: 'Voici son périmètre (en cm).', editable: 'false', deletable: 'false', movable: 'false' } }, { nom: 'TEMPS', contenu: { valeur: 2, editable: 'false', deletable: 'false', movable: 'false' } }] })
        scratch.params.scratchProgDonnees.progdeb.push({ type: 'dire', editable: 'false', deletable: 'false', movable: 'false', value: [{ nom: 'DELTA', contenu: { valeur: 'PARTIE A COMPLETER' } }, { nom: 'TEMPS', contenu: { valeur: 2, editable: 'false', deletable: 'false', movable: 'false' } }] })

        scratch.params.scratchProgDonnees.lesvarbonnes.push({ nom: 'rayon', val: 0, id: 'aaaaaaa' })
        stor.foncverif = stor.Pcercle
        larep = { operateur: '*', value: [{ nom: 'NUM1', contenu: { valeur: 2 } }, { nom: 'NUM2', contenu: { operateur: '*', value: [{ nom: 'NUM1', contenu: { type: 'pi' } }, { nom: 'NUM2', contenu: { type: 'data_variable', variable: [{ nom: 'VARIABLE', nomvar: 'rayon', id: 'aaaaaaa' }] } }] } }] }
        bufmen = 'le périmètre d’un cercle'

        scratch.params.scratchProgDonnees.lesordres.push({ type: 'Get_us', value: [{ nom: 'STRING', contenu: { valeur: 'Donne le rayon d’un cercle (en cm).', editable: 'false', deletable: 'false', movable: 'false' } }], editable: 'false', deletable: 'false', movable: 'false' })
        scratch.params.scratchProgDonnees.lesordres.push({ type: 'data_setvariableto', editable: 'false', deletable: 'false', movable: 'false', variable: [{ nom: 'VARIABLE', nomvar: 'rayon', id: 'aaaaaaa' }], value: [{ nom: 'VALUE', editable: 'false', deletable: 'false', movable: 'false', contenu: { reponse: '', editable: 'false', deletable: 'false', movable: 'false' } }] })
        scratch.params.scratchProgDonnees.lesordres.push({ type: 'dire', editable: 'false', deletable: 'false', movable: 'false', value: [{ nom: 'DELTA', editable: 'false', deletable: 'false', movable: 'false', contenu: { valeur: 'Voici son périmètre (en cm).', editable: 'false', deletable: 'false', movable: 'false' } }, { nom: 'TEMPS', contenu: { valeur: 2, editable: 'false', deletable: 'false', movable: 'false' } }] })
        scratch.params.scratchProgDonnees.listValPos = [['1'], ['2'], ['3'], ['4'], ['5'], ['6'], ['7'], ['8'], ['9'], ['10']]
        break
      case 'acercle': scratch.params.scratchProgDonnees.menu[1].contenu.push({ type: 'data_variable', variable: [{ nom: 'VARIABLE', nomvar: 'rayon', id: 'aaaaaaa' }] })
        scratch.params.scratchProgDonnees.menu[1].contenu.push({ type: 'pi' })
        scratch.params.scratchProgDonnees.progdeb.push({ type: 'Get_us', value: [{ nom: 'STRING', contenu: { valeur: 'Donne le rayon d’un cercle (en cm).', editable: 'false', deletable: 'false', movable: 'false' } }], editable: 'false', deletable: 'false', movable: 'false' })
        scratch.params.scratchProgDonnees.progdeb.push({ type: 'data_setvariableto', editable: 'false', deletable: 'false', movable: 'false', variable: [{ nom: 'VARIABLE', nomvar: 'rayon', id: 'aaaaaaa' }], value: [{ nom: 'VALUE', editable: 'false', deletable: 'false', movable: 'false', contenu: { reponse: '', editable: 'false', deletable: 'false', movable: 'false' } }] })
        scratch.params.scratchProgDonnees.progdeb.push({ type: 'dire', editable: 'false', deletable: 'false', movable: 'false', value: [{ nom: 'DELTA', editable: 'false', deletable: 'false', movable: 'false', contenu: { valeur: 'Voici son aire (en cm²).', editable: 'false', deletable: 'false', movable: 'false' } }, { nom: 'TEMPS', contenu: { valeur: 2, editable: 'false', deletable: 'false', movable: 'false' } }] })
        scratch.params.scratchProgDonnees.progdeb.push({ type: 'dire', editable: 'false', deletable: 'false', movable: 'false', value: [{ nom: 'DELTA', contenu: { valeur: 'PARTIE A COMPLETER' } }, { nom: 'TEMPS', contenu: { valeur: 2, editable: 'false', deletable: 'false', movable: 'false' } }] })

        scratch.params.scratchProgDonnees.lesvarbonnes.push({ nom: 'rayon', val: 0, id: 'aaaaaaa' })
        stor.foncverif = stor.Acercle
        larep = { operateur: '*', value: [{ nom: 'NUM1', contenu: { type: 'pi' } }, { nom: 'NUM2', contenu: { operateur: '*', value: [{ nom: 'NUM1', contenu: { type: 'data_variable', variable: [{ nom: 'VARIABLE', nomvar: 'rayon', id: 'aaaaaaa' }] } }, { nom: 'NUM2', contenu: { type: 'data_variable', variable: [{ nom: 'VARIABLE', nomvar: 'rayon', id: 'aaaaaaa' }] } }] } }] }
        bufmen = 'l’aire d’un cercle'

        scratch.params.scratchProgDonnees.lesordres.push({ type: 'Get_us', value: [{ nom: 'STRING', contenu: { valeur: 'Donne le rayon d’un cercle (en cm).', editable: 'false', deletable: 'false', movable: 'false' } }], editable: 'false', deletable: 'false', movable: 'false' })
        scratch.params.scratchProgDonnees.lesordres.push({ type: 'data_setvariableto', editable: 'false', deletable: 'false', movable: 'false', variable: [{ nom: 'VARIABLE', nomvar: 'rayon', id: 'aaaaaaa' }], value: [{ nom: 'VALUE', editable: 'false', deletable: 'false', movable: 'false', contenu: { reponse: '', editable: 'false', deletable: 'false', movable: 'false' } }] })
        scratch.params.scratchProgDonnees.lesordres.push({ type: 'dire', editable: 'false', deletable: 'false', movable: 'false', value: [{ nom: 'DELTA', editable: 'false', deletable: 'false', movable: 'false', contenu: { valeur: 'Voici son aire (en cm²).', editable: 'false', deletable: 'false', movable: 'false' } }, { nom: 'TEMPS', contenu: { valeur: 2, editable: 'false', deletable: 'false', movable: 'false' } }] })
        scratch.params.scratchProgDonnees.listValPos = [['1'], ['2'], ['3'], ['4'], ['5'], ['6'], ['7'], ['8'], ['9'], ['10']]
        break
      case 'asphere': scratch.params.scratchProgDonnees.menu[1].contenu.push({ type: 'data_variable', variable: [{ nom: 'VARIABLE', nomvar: 'rayon', id: 'aaaaaaa' }] })
        scratch.params.scratchProgDonnees.menu[1].contenu.push({ type: 'pi' })
        scratch.params.scratchProgDonnees.progdeb.push({ type: 'Get_us', value: [{ nom: 'STRING', contenu: { valeur: 'Donne le rayon d’une sphère (en cm).', editable: 'false', deletable: 'false', movable: 'false' } }], editable: 'false', deletable: 'false', movable: 'false' })
        scratch.params.scratchProgDonnees.progdeb.push({ type: 'data_setvariableto', editable: 'false', deletable: 'false', movable: 'false', variable: [{ nom: 'VARIABLE', nomvar: 'rayon', id: 'aaaaaaa' }], value: [{ nom: 'VALUE', editable: 'false', deletable: 'false', movable: 'false', contenu: { reponse: '', editable: 'false', deletable: 'false', movable: 'false' } }] })
        scratch.params.scratchProgDonnees.progdeb.push({ type: 'dire', editable: 'false', deletable: 'false', movable: 'false', value: [{ nom: 'DELTA', editable: 'false', deletable: 'false', movable: 'false', contenu: { valeur: 'Voici son aire (en cm²).', editable: 'false', deletable: 'false', movable: 'false' } }, { nom: 'TEMPS', contenu: { valeur: 2, editable: 'false', deletable: 'false', movable: 'false' } }] })
        scratch.params.scratchProgDonnees.progdeb.push({ type: 'dire', editable: 'false', deletable: 'false', movable: 'false', value: [{ nom: 'DELTA', contenu: { valeur: 'PARTIE A COMPLETER' } }, { nom: 'TEMPS', contenu: { valeur: 2, editable: 'false', deletable: 'false', movable: 'false' } }] })

        scratch.params.scratchProgDonnees.lesvarbonnes.push({ nom: 'rayon', val: 0, id: 'aaaaaaa' })
        stor.foncverif = stor.Asphere
        larep = { operateur: '*', value: [{ nom: 'NUM1', contenu: { valeur: 4 } }, { nom: 'NUM2', contenu: { operateur: '*', value: [{ nom: 'NUM1', contenu: { type: 'pi' } }, { nom: 'NUM2', contenu: { operateur: '*', value: [{ nom: 'NUM1', contenu: { type: 'data_variable', variable: [{ nom: 'VARIABLE', nomvar: 'rayon', id: 'aaaaaaa' }] } }, { nom: 'NUM2', contenu: { type: 'data_variable', variable: [{ nom: 'VARIABLE', nomvar: 'rayon', id: 'aaaaaaa' }] } }] } }] } }] }
        bufmen = 'l’aire d’une sphère'

        scratch.params.scratchProgDonnees.lesordres.push({ type: 'Get_us', value: [{ nom: 'STRING', contenu: { valeur: 'Donne le rayon d’une sphère (en cm).', editable: 'false', deletable: 'false', movable: 'false' } }], editable: 'false', deletable: 'false', movable: 'false' })
        scratch.params.scratchProgDonnees.lesordres.push({ type: 'data_setvariableto', editable: 'false', deletable: 'false', movable: 'false', variable: [{ nom: 'VARIABLE', nomvar: 'rayon', id: 'aaaaaaa' }], value: [{ nom: 'VALUE', editable: 'false', deletable: 'false', movable: 'false', contenu: { reponse: '', editable: 'false', deletable: 'false', movable: 'false' } }] })
        scratch.params.scratchProgDonnees.lesordres.push({ type: 'dire', editable: 'false', deletable: 'false', movable: 'false', value: [{ nom: 'DELTA', editable: 'false', deletable: 'false', movable: 'false', contenu: { valeur: 'Voici son aire (en cm²).', editable: 'false', deletable: 'false', movable: 'false' } }, { nom: 'TEMPS', contenu: { valeur: 2, editable: 'false', deletable: 'false', movable: 'false' } }] })
        scratch.params.scratchProgDonnees.listValPos = [['1'], ['2'], ['3'], ['4'], ['5'], ['6'], ['7'], ['8'], ['9'], ['10']]
        break
      case 'vsphere': scratch.params.scratchProgDonnees.menu[1].contenu.push({ type: 'data_variable', variable: [{ nom: 'VARIABLE', nomvar: 'rayon', id: 'aaaaaaa' }] })
        scratch.params.scratchProgDonnees.menu[1].contenu.push({ type: 'pi' })
        scratch.params.scratchProgDonnees.progdeb.push({ type: 'Get_us', value: [{ nom: 'STRING', contenu: { valeur: 'Donne le rayon d’une sphère (en cm).', editable: 'false', deletable: 'false', movable: 'false' } }], editable: 'false', deletable: 'false', movable: 'false' })
        scratch.params.scratchProgDonnees.progdeb.push({ type: 'data_setvariableto', editable: 'false', deletable: 'false', movable: 'false', variable: [{ nom: 'VARIABLE', nomvar: 'rayon', id: 'aaaaaaa' }], value: [{ nom: 'VALUE', editable: 'false', deletable: 'false', movable: 'false', contenu: { reponse: '', editable: 'false', deletable: 'false', movable: 'false' } }] })
        scratch.params.scratchProgDonnees.progdeb.push({ type: 'dire', editable: 'false', deletable: 'false', movable: 'false', value: [{ nom: 'DELTA', editable: 'false', deletable: 'false', movable: 'false', contenu: { valeur: 'Voici son volume (en cm³).', editable: 'false', deletable: 'false', movable: 'false' } }, { nom: 'TEMPS', contenu: { valeur: 2, editable: 'false', deletable: 'false', movable: 'false' } }] })
        scratch.params.scratchProgDonnees.progdeb.push({ type: 'dire', editable: 'false', deletable: 'false', movable: 'false', value: [{ nom: 'DELTA', contenu: { valeur: 'PARTIE A COMPLETER' } }, { nom: 'TEMPS', contenu: { valeur: 2, editable: 'false', deletable: 'false', movable: 'false' } }] })

        scratch.params.scratchProgDonnees.lesvarbonnes.push({ nom: 'rayon', val: 0, id: 'aaaaaaa' })
        stor.foncverif = stor.Vsphere
        larep = { operateur: '/', value: [{ nom: 'NUM1', contenu: { operateur: '*', value: [{ nom: 'NUM1', contenu: { valeur: 4 } }, { nom: 'NUM2', contenu: { operateur: '*', value: [{ nom: 'NUM1', contenu: { type: 'pi' } }, { nom: 'NUM2', contenu: { operateur: '*', value: [{ nom: 'NUM1', contenu: { type: 'data_variable', variable: [{ nom: 'VARIABLE', nomvar: 'rayon', id: 'aaaaaaa' }] } }, { nom: 'NUM2', contenu: { operateur: '*', value: [{ nom: 'NUM1', contenu: { type: 'data_variable', variable: [{ nom: 'VARIABLE', nomvar: 'rayon', id: 'aaaaaaa' }] } }, { nom: 'NUM2', contenu: { type: 'data_variable', variable: [{ nom: 'VARIABLE', nomvar: 'rayon', id: 'aaaaaaa' }] } }] } }] } }] } }] } }, { nom: 'NUM2', contenu: { valeur: 3 } }] }
        bufmen = 'le volume d’une sphère'

        scratch.params.scratchProgDonnees.lesordres.push({ type: 'Get_us', value: [{ nom: 'STRING', contenu: { valeur: 'Donne le rayon d’une sphère (en cm).', editable: 'false', deletable: 'false', movable: 'false' } }], editable: 'false', deletable: 'false', movable: 'false' })
        scratch.params.scratchProgDonnees.lesordres.push({ type: 'data_setvariableto', editable: 'false', deletable: 'false', movable: 'false', variable: [{ nom: 'VARIABLE', nomvar: 'rayon', id: 'aaaaaaa' }], value: [{ nom: 'VALUE', editable: 'false', deletable: 'false', movable: 'false', contenu: { reponse: '', editable: 'false', deletable: 'false', movable: 'false' } }] })
        scratch.params.scratchProgDonnees.lesordres.push({ type: 'dire', editable: 'false', deletable: 'false', movable: 'false', value: [{ nom: 'DELTA', editable: 'false', deletable: 'false', movable: 'false', contenu: { valeur: 'Voici son volume (en cm³).', editable: 'false', deletable: 'false', movable: 'false' } }, { nom: 'TEMPS', contenu: { valeur: 2, editable: 'false', deletable: 'false', movable: 'false' } }] })
        scratch.params.scratchProgDonnees.listValPos = [['1'], ['2'], ['3'], ['4'], ['5'], ['6'], ['7'], ['8'], ['9'], ['10']]
        break

      default : j3pShowError(Error(stor.fencours + ' n’est pas géré !'))
    }

    scratch.params.scratchProgDonnees.lesordres.push({ type: 'dire', editable: 'false', deletable: 'false', movable: 'false', value: [{ nom: 'DELTA', contenu: larep }, { nom: 'TEMPS', contenu: { valeur: 2, editable: 'false', deletable: 'false', movable: 'false' } }] })

    scratch.enonce([], {
      pourInject: { StartScale: 0.6 },
      modeCorrection: {
        type: 'SansComparaison',
        aVerif: [{ quoi: 'QuandAvertir', quand: 'après' }]
      },
      dirigeProg: { sequence: ds.Sequence, pasApas: ds.PasAPas, cacheTest: ds.CacheTest }
    })

    const cons = ('<b> Programme le chat pour qu’il donne la valeur</b> de ' + bufmen).replace('de le', 'du')
    const yy = addDefaultTable(scratch.lesdivs.enonceG, 1, 2)
    yy[0][0].style.padding = 0
    yy[0][1].style.padding = 0
    j3pAffiche(yy[0][0], null, cons + '&nbsp;')
    stor.aise = new BulleAide(yy[0][1], 'Complète la partie <b><i>"PARTIE A COMPLETER"</i></b>', { place: 3 })

    me.finEnonce()
    // on ne veut pas du bouton valider, c'est le drapeau scratch qui valide
    me.cacheBoutonValider()
  }

  switch (this.etat) {
    case 'enonce':
      if (this.debutDeLaSection) {
        initSection()
      } else {
        enonceMain()
      }
      break // case "enonce":

    case 'correction':
      try {
        const ret = scratch.prepCorrec()
        const xmlDoc2 = ret.xmlDoc2
        const xmlText = ret.xmlText

        if (scratch.params.scratchProgDonnees.repEleve) {
          // Bonne réponse
          this.score++
          scratch.corrigeGood(xmlDoc2, xmlText)
        } else {
          // Pas de bonne réponse
          scratch.corrige(xmlDoc2, xmlText)
        }
        // on veut être rappelé dans l’état navigation
        this.finCorrection('navigation', true)
      } catch (error) {
        console.error(error)
      }
      break // case "correction":

    case 'navigation':
      if (this.sectionTerminee()) {
        scratch.lesdivs.BoutonsSuite.appendChild(this.buttonsElts.sectionSuivante)
        this.buttonsElts.sectionSuivante.addEventListener('click', () => {
          scratch.params.grosStop = true
        })
      } else {
        this.etat = 'enonce'
        scratch.lesdivs.BoutonsSuite.appendChild(this.buttonsElts.suite)
        this.buttonsElts.suite.addEventListener('click', () => {
          scratch.params.grosStop = true
        })
      }
      // Obligatoire
      this.finNavigation()
      break // case "navigation":
  }
}
