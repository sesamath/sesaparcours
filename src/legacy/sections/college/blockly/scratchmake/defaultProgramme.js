export function getListeBloc () {
  return [
    {
      type: 'catégorie',
      nom: 'DEPLACEMENTS',
      coul1: '#4C97FF',
      coul2: '#3373CC',
      contenu: [
        { type: 'avancer', value: [{ nom: 'STEPS', contenu: { valeur: '20' } }], aff: false },
        { type: 'tourne_droite', value: [{ nom: 'DEGREES', contenu: { valeur: '15' } }], aff: false },
        { type: 'tourne_gauche', value: [{ nom: 'DEGREES', contenu: { valeur: '15' } }], aff: false },
        { type: 'aller_a', value: [{ nom: 'X', contenu: { valeur: '0' } }, { nom: 'Y', contenu: { valeur: '0' } }], aff: false },
        { type: 'ajouter_x', value: [{ nom: 'STEPS', contenu: { valeur: '20' } }], aff: false },
        { type: 'mettre_x', value: [{ nom: 'STEPS', contenu: { valeur: '20' } }], aff: false },
        { type: 'ajouter_y', value: [{ nom: 'STEPS', contenu: { valeur: '20' } }], aff: false },
        { type: 'mettre_y', value: [{ nom: 'STEPS', contenu: { valeur: '20' } }], aff: false },
        { type: 'motion_pointindirection', value: [{ nom: 'STEPS', contenu: { valeur: '90' } }], aff: false },
        { type: 'abscisse_x', aff: false },
        { type: 'ordonnee_y', aff: false },
        { type: 'direction', aff: false }
      ]
    },
    {
      type: 'catégorie',
      nom: 'APPARENCE',
      coul1: '#88224e',
      coul2: '#540429',
      contenu: [
        { type: 'dire', value: [{ nom: 'DELTA', contenu: { valeur: ' ' } }, { nom: 'TEMPS', contenu: { valeur: '2' } }], aff: false }
      ]
    },
    {
      type: 'catégorie',
      nom: 'EVENEMENT',
      coul1: '#fdc130',
      coul2: '#a19a14',
      contenu: [
        { type: 'Drapo', aff: false },
        { type: 'FLECHEG', aff: false },
        { type: 'FLECHED', aff: false },
        { type: 'FLECHEH', aff: false },
        { type: 'FLECHEB', aff: false },
        { type: 'FLECHEA', aff: false },
        { type: 'FLECHEESP', aff: false },
        { type: 'EVENTSou', aff: false }
      ]
    },
    {
      type: 'catégorie',
      nom: 'CONTROLES',
      coul1: '#fd6e03',
      coul2: '#bb7002',
      contenu: [
        { type: 'control_repeat', value: [{ nom: 'TIMES', contenu: { valeur: ' ' } }], aff: false },
        { type: 'control_forever', value: [], aff: false },
        { type: 'control_repeat_until', value: [], aff: false },
        { type: 'control_if', aff: false },
        { type: 'control_if_else', aff: false },
        { type: 'control_stop', aff: false }
      ]
    },
    {
      type: 'catégorie',
      nom: 'OPERATEURS',
      coul1: '#2cbd0a',
      coul2: '#085d03',
      contenu: [
        { type: 'operator_add', value: [{ nom: 'NUM1', contenu: { valeur: ' ' } }, { nom: 'NUM2', contenu: { valeur: ' ' } }], aff: false },
        { type: 'operator_subtract', value: [{ nom: 'NUM1', contenu: { valeur: ' ' } }, { nom: 'NUM2', contenu: { valeur: ' ' } }], aff: false },
        { type: 'operator_multiply', value: [{ nom: 'NUM1', contenu: { valeur: ' ' } }, { nom: 'NUM2', contenu: { valeur: ' ' } }], aff: false },
        { type: 'operator_divide', value: [{ nom: 'NUM1', contenu: { valeur: ' ' } }, { nom: 'NUM2', contenu: { valeur: ' ' } }], aff: false },
        { type: 'operator_random', value: [{ nom: 'NUM1', contenu: { valeur: ' ' } }, { nom: 'NUM2', contenu: { valeur: ' ' } }], aff: false },
        { type: 'operator_equals', value: [{ nom: 'OPERAND1', contenu: { valeur: ' ' } }, { nom: 'OPERAND2', contenu: { valeur: ' ' } }], aff: false },
        { type: 'operator_gt', value: [{ nom: 'OPERAND1', contenu: { valeur: ' ' } }, { nom: 'OPERAND2', contenu: { valeur: ' ' } }], aff: false },
        { type: 'operator_lt', value: [{ nom: 'OPERAND1', contenu: { valeur: ' ' } }, { nom: 'OPERAND2', contenu: { valeur: ' ' } }], aff: false },
        { type: 'operator_mathop', value: [{ nom: 'NUM', contenu: { valeur: ' ' } }], aff: false },
        { type: 'operator_reste', value: [{ nom: 'NUM1', contenu: { valeur: ' ' } }, { nom: 'NUM2', contenu: { valeur: ' ' } }], aff: false },
        { type: 'operator_quotient', value: [{ nom: 'NUM1', contenu: { valeur: ' ' } }, { nom: 'NUM2', contenu: { valeur: ' ' } }], aff: false }
      ]
    },
    {
      type: 'catégorie',
      nom: 'STYLO',
      coul1: '#258f06',
      coul2: '#085d03',
      contenu: [
        { type: 'lever_stylo', aff: false },
        { type: 'poser_stylo', aff: false }
      ]
    },
    {
      type: 'catégorie',
      nom: 'CAPTEUR',
      coul1: '#0f4c83',
      coul2: '#041454',
      contenu: [
        { type: 'Get_us', value: [{ nom: 'STRING', contenu: { valeur: ' ' } }], aff: false },
        { type: 'reponse', aff: false }
      ]
    },
    {
      type: 'catégorie',
      nom: 'BLOC PERSO',
      coul1: '#820da1',
      coul2: '#540429',
      contenu: [
        { type: 'perso', aff: false }
      ]
    },
    {
      type: 'catégorie',
      nom: 'DEPLACEMENTS PAR CASE',
      coul1: '#4C97FF',
      coul2: '#3373CC',
      contenu: [
        { type: 'caseAvancer', value: [], aff: false },
        { type: 'caseReculer', value: [], aff: false },
        { type: 'caseTournerDroite', value: [], aff: false },
        { type: 'caseTournerGauche', value: [], aff: false }
      ]
    },
    {
      type: 'catégorie',
      nom: 'OPERATEURS PAR CASE',
      coul1: '#2cbd0a',
      coul2: '#085d03',
      contenu: [
        { type: 'caseIsJaune', value: [], aff: false },
        { type: 'caseIsBleue', value: [], aff: false },
        { type: 'caseIsRouge', value: [], aff: false },
        { type: 'caseIsVerte', value: [], aff: false },
        { type: 'caseIsOrange', value: [], aff: false },
        { type: 'caseIsRose', value: [], aff: false },
        { type: 'caseIsViolette', value: [], aff: false },
        { type: 'caseIsColore', value: [], aff: false },
        { type: 'caseIsNotColore', value: [], aff: false },
        { type: 'caseDevantNoire', value: [], aff: false }
      ]
    },
    {
      type: 'catégorie',
      nom: 'ACTION PAR CASE',
      coul1: '#e524ff',
      coul2: '#6700b4',
      contenu: [
        { type: 'caseColoreJaune', value: [], aff: false },
        { type: 'caseColoreBleue', value: [], aff: false },
        { type: 'caseColoreRouge', value: [], aff: false },
        { type: 'caseColoreVerte', value: [], aff: false },
        { type: 'caseColoreOrange', value: [], aff: false },
        { type: 'caseColoreRose', value: [], aff: false },
        { type: 'caseColoreViolette', value: [], aff: false }
      ]
    },
    { type: 'catégorie', nom: 'VARIABLES', coul1: '#FF8C1A', coul2: '#DB6E00', custom: 'VARIABLE', contenu: [], aff: false }
  ]
}

export default function getDefaultProgramme () {
  return {
    Difficulte: 'Progressive',
    Sequence: true,
    PasAPas: true,
    Programme: [
      getDefaultProgrammeElement()
    ]
  }
}

/**
 * Retourne un programme scratch avec ses propriétés par défaut
 * @returns {Object}
 */
function getDefaultProgrammeElement () {
  return {
    ProgDeb: '<xml xmlns="http://www.w3.org/1999/xhtml"><variables></variables><block type="Drapo" id="0WG*|pxD_W52YSIj~7*s" x="20" y="20"></block></xml>',
    ProgrammeAt: '<xml xmlns="http://www.w3.org/1999/xhtml"><variables></variables><block type="Drapo" id="0WG*|pxD_W52YSIj~7*s"  x="20" y="20"></block></xml>',
    listeBloc: getListeBloc(),
    enonce: 'La question..',
    crea: false,
    niv: 0,
    nom: 'Nouvel exercice',
    blocPerso: [],
    comptedire: true,
    comptedirfin: false,
    comptepla: true,
    comptetrace: true,
    comptevar: [],
    comptebloc: 0,
    Rendu1: true,
    Rendu2: true,
    lutinScratch: false,
    imageFond: { type: 'aucun' },
    listValPos: []
  }
}
