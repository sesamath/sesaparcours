import $ from 'jquery'

import loadJqueryDialog from 'src/lib/core/loadJqueryDialog'
import { j3pAddContent, j3pAddElt, j3pAjouteBouton, j3pClone, j3pEmpty, j3pShuffle, j3pStyle } from 'src/legacy/core/functions'
import { getNewScratch } from 'src/legacy/outils/scratch/loaders'
import { addDefaultTable } from 'src/legacy/themes/table'
import { j3pAffiche } from 'src/lib/mathquill/functions'
import loadMq from 'src/lib/mathquill/loadMathquill'
import Dialog from 'src/lib/widgets/dialog/Dialog'
import drapeauImg from 'src/legacy/outils/scratch/images/drapeau.gif'
import retourImg from 'src/legacy/outils/scratch/images/retour.jpg'
import boutonImg from 'src/legacy/outils/scratch/images/stop.gif'
import okImg from 'src/legacy/outils/scratch/images/drapeauok.gif'

import getDefaultProgramme from './defaultProgramme'
import chatImg from 'src/legacy/outils/paramEditors/chat.png'
import dessinleveImg from 'src/legacy/outils/paramEditors/dessinleve.png'

// chargé dynamiquement plus bas
let Blockly

async function initEditor (container, values) {
  const { default: editor } = await import('./editorProgramme.js') // ce sera un import natif (cf vite.config)
  return editor(container, values)
}

export const params = {
  outils: ['mathquill'],
  /**
   * Ce qui est paramétrable dans l’éditeur de graphe
   * @type {LegacySectionParamsParametres}
   */
  parametres: [
    ['Parametres', JSON.stringify(getDefaultProgramme()), 'multiEditor', 'Json figure de base et figure attendue.', initEditor],
    ['nbrepetitions', 3, 'entier', 'Nombre de répétitions de la section'],
    ['nbchances', 10, 'entier', 'Nombre d’essais possibles. (0 signifie illimité)'],
    ['limite', 0, 'entier', 'Temps disponible par répétition, en secondes (0  signifie illimité)']
  ]
}

const sortOnNivProp = (a, b) => b.niv - a.niv

/**
 * section blokmohascratch
 * @this {Parcours}
 */
export default function main () {
  const me = this
  const stor = me.storage
  const ds = me.donneesSection

  function rendpaseditable (n) {
    n = n.replace(/<block/g, '<block editable = "false" movable = "false" deletable = "false" ')
    const deg = n.split('shadow')
    for (let i = 1; i < deg.length; i += 2) {
      if (deg[i].indexOf('A COMPLETER') === -1) deg[i] = ' editable = "false" movable = "false" deletable = "false" ' + deg[i]
    }
    let aret = deg[0]
    for (let i = 1; i < deg.length; i++) {
      aret += 'shadow' + deg[i]
    }
    return aret
  }

  function voirRendu () {
    if (stor.dialog) stor.dialog.show()
  }
  function refaisProg (xmlDoc, ou) {
    let xml = xmlDoc.firstChild
    while (xml.nodeType !== 1) {
      xml = xml.nextSibling
    }
    ou.clear()
    Blockly.Xml.domToWorkspace(xml, ou)
  }

  async function initSection () {
    // faut s’assurer que jQuery-ui est chargé, Mq aussi
    await Promise.all([loadJqueryDialog(), loadMq()])
    const scratch = await getNewScratch()
    Blockly = scratch.Blockly
    stor.scratch = scratch
    stor.dejafait2 = []
    stor.dejafait = []
    me.validOnEnter = false // ex donneesSection.touche_entree

    ds.ProgrammeLT2 = ds.Parametres
    try {
      let ProgrammeLT
      if (typeof ds.Parametres === 'object' && ds.Parametres !== null) {
        ProgrammeLT = ds.Parametres
      } else if (typeof ds.Parametres === 'string') {
        try {
          ProgrammeLT = JSON.parse(ds.Parametres)
        } catch (error) {
          console.error(error)
        }
      }
      if (!ProgrammeLT) {
        console.error(Error(`type de Programme invalide ${typeof ds.Parametres}`))
        ProgrammeLT = getDefaultProgramme()
      }
      ds.ProgrammeLT2 = ProgrammeLT
    } catch (error) {
      console.error(error, 'avec le programme', ds.Parametres)
      throw Error('Programme mis en paramètre invalide')
    }

    // Scratch utilise ds.Sequence, ds.PasAPas, etc
    // Ce ne sont pas des paramètres mais faut les ajouter à ds
    ds.Sequence = ds.ProgrammeLT2.Sequence
    ds.PasAPas = ds.ProgrammeLT2.PasAPas
    ds.Difficulte = ds.ProgrammeLT2.Difficulte
    ds.ProgrammeLT = ds.ProgrammeLT2.Programme

    stor.Difficulte = ds.ProgrammeLT2.Difficulte
    stor.Programme = ds.ProgrammeLT2.Programme

    scratch.initParams(me)
    scratch.params.MohaMode = true
    scratch.testFlag = function () {
    }
    scratch.testFlagFin = function () {
      if (!scratch.params.Aconserve) return { ok: true, mess: 'ras' }
      const el = scratch.params.Aconserve.el
      const moha = scratch.params.Aconserve.moha
      // test place lutin
      if (stor.ProgEnCours.comptepla) {
        let ok = true
        ok = ok && (el.lutin[0].cox === moha.lutin[0].cox)
        ok = ok && (el.lutin[0].coy === moha.lutin[0].coy)
        ok = ok && (metBienAngle(el.lutin[0].angle) === metBienAngle(moha.lutin[0].angle))
        if (!ok) return { ok: false, mess: 'Le lutin n’est pas dans la position attendue.' }
      }
      // test dit lutin
      if (stor.ProgEnCours.comptedire) {
        if (stor.ProgEnCours.comptedirfin) {
          const diel = el.said.pop()
          const dimoha = moha.said.pop()
          if (diel !== dimoha && diel.trim() !== dimoha) return { ok: false, mess: 'Le lutin aurait du dire ' + dimoha + ' dans cette situation.' }
        } else {
          if (el.said.length !== moha.said.length) {
            return { ok: false, mess: 'Le lutin n’a pas dit ce qu’il fallait.' }
          } else {
            let ok = true
            for (let i = 0; i < el.said.length; i++) {
              ok = ok && (el.said[i] === moha.said[i] || el.said[i].trim() === moha.said[i])
            }
            if (!ok) return { ok: false, mess: 'Le lutin n’a pas dit ce qu’il fallait.' }
          }
        }
      }
      // test trace lutin
      if (stor.ProgEnCours.comptetrace) {
        // cree deux canvas , dessine dedans puis trans en base 64
        const cnv1 = j3pAddElt(scratch.params.me.zonesElts.MG, 'div')
        const canvas1 = j3pAddElt(cnv1, 'canvas', { style: { display: 'none', width: '300px', height: '300px' } })
        const cnv2 = j3pAddElt(scratch.params.me.zonesElts.MG, 'div')
        const canvas2 = j3pAddElt(cnv2, 'canvas', { style: { display: 'none', width: '300px', height: '300px' } })

        const ctx1 = canvas1.getContext('2d')
        const ctx2 = canvas2.getContext('2d')
        // dans model
        for (let i = 0; i < el.lignes.length; i++) {
          const obi = el.lignes[i]
          scratch.scratchDrawLine(obi.x1, obi.y1 / 2 + 15, obi.x2, obi.y2 / 2 + 15, 1, 'black', ctx2)
        }
        for (let i = 0; i < moha.lignes.length; i++) {
          const obi = moha.lignes[i]
          scratch.scratchDrawLine(obi.x1, obi.y1 / 2 + 15, obi.x2, obi.y2 / 2 + 15, 1, 'black', ctx1)
        }
        // autre tests
        // 1 on colle les segments
        const coolEl = recolle(el.lignes)
        const cooMoha = recolle(moha.lignes)
        if (coolEl.length !== cooMoha.length) return { ok: false, mess: 'Les tracés obtenus ne sont pas corrects !' }
        for (let i = 0; i < coolEl.length; i++) {
          if (!aSonClone(coolEl[i], cooMoha)) return { ok: false, mess: 'Les tracés obtenus ne sont pas corrects !' }
        }
      }
      // test var
      if (stor.ProgEnCours.comptevar.length > 0) {
        function RetrouveVal (ki, ou) {
          for (let i = 0; i < ou.length; i++) {
            if (ou[i].name === ki) return ou[i].val
          }
          return ki + ' non trouvée'
        }
        for (let i = 0; i < stor.ProgEnCours.comptevar.length; i++) {
          const val1 = RetrouveVal(stor.ProgEnCours.comptevar[i], el.variables)
          const val2 = RetrouveVal(stor.ProgEnCours.comptevar[i], moha.variables)
          if (val1 !== val2) return { ok: false, mess: 'La variable <b>' + stor.ProgEnCours.comptevar[i] + '</b> devrait contenir ' + val2 + ' dans cette situation !' }
        }
      }
      if (stor.ProgEnCours.comptebloc > 0) {
        if (scratch.params.scratchProgDonnees.nbdeblocs > stor.ProgEnCours.comptebloc) return { ok: false, mess: 'Tu peux utiliser moins de blocs !' }
      }
      if (stor.ProgEnCours.corCaseCoulToutes) {
        let ok = true
        for (let i = 0; i < el.tabCase.length; i++) {
          for (let j = 0; j < el.tabCase[i].length; j++) {
            ok = ok && el.tabCase[i][j] === moha.tabCase[i][j]
          }
        }
        if (!ok) {
          return { ok: false, mess: 'Les couleurs des cases ne correspondent pas !' }
        }
      }
      if (stor.ProgEnCours.corCaseCoul) {
        const coulCaseEl = chopeCoul(el.tabCase, el.lutin[0].cox, el.lutin[0].coy)
        const coulCaseMoha = chopeCoul(moha.tabCase, moha.lutin[0].cox, moha.lutin[0].coy)
        if (coulCaseEl !== coulCaseMoha) {
          return { ok: false, mess: 'Scratch ne s\'est pas arrêté sur la bonne case !' }
        }
      }
      return { ok: true, mess: 'ras' }
    }

    me.construitStructurePage({ structure: 'presentation3', ratioGauche: 0.6, theme: 'zonesAvecImageDeFond' })
    me.stockage = [0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0]

    me.score = 0
    me.afficheTitre('Construire un programme scratch')

    ds.ProgrammeLT2Choice = j3pShuffle(stor.Programme)
    ds.ProgrammeLT2FF = []
    let nivVu = []
    for (let i = 0; i < ds.ProgrammeLT2Choice.length; i++) {
      const niniv = ds.ProgrammeLT2Choice[i].niv
      if (nivVu.indexOf(niniv) === -1) {
        nivVu.push(niniv)
      }
    }
    if (stor.Difficulte === 'Progressive' && ds.nbitems <= nivVu.length) {
      nivVu = []
      for (let i = ds.ProgrammeLT2Choice.length - 1; i > -1; i--) {
        const niniv = ds.ProgrammeLT2Choice[i].niv
        if (nivVu.indexOf(niniv) === -1) {
          nivVu.push(niniv)
          if (ds.ProgrammeLT2FF.length < ds.nbitems) ds.ProgrammeLT2FF.push(j3pClone(ds.ProgrammeLT2Choice[i]))
        }
      }
    } else {
      for (let i = 0; i < ds.nbitems; i++) {
        if (ds.ProgrammeLT2Choice.length === 0) ds.ProgrammeLT2Choice = j3pShuffle(stor.Programme)
        ds.ProgrammeLT2FF.push(ds.ProgrammeLT2Choice.pop())
      }
    }
    if (stor.Difficulte === 'Progressive') ds.ProgrammeLT2FF.sort(sortOnNivProp)
    enonceMain()
  }
  function chopeCoul (tab, x, y) {
    const lx = Math.floor(x / 50)
    const ly = Math.floor(y / 50)
    return tab[lx][ly]
  }
  function metBienAngle (angle) {
    while (angle <= -180) angle += 360
    while (angle > 180) angle -= 360
    return angle
  }
  function aSonClone (el, tab) {
    const X = { x: el.x1, y: el.y1 }
    const Y = { x: el.x2, y: el.y2 }
    for (let i = 0; i < tab.length; i++) {
      const A = { x: tab[i].x1, y: tab[i].y1 }
      const B = { x: tab[i].x2, y: tab[i].y2 }
      if ((pointProche(X, A) && pointProche(Y, B)) ||
        (pointProche(X, A) && pointProche(Y, B)) ||
        (pointProche(X, A) && pointProche(Y, B)) ||
        (pointProche(X, A) && pointProche(Y, B))) return true
    }
    return false
  }
  function pointProche (A, B) {
    return stor.scratch.dist(A, B) < 0.5
  }
  function recolle (tab) {
    const copieTab = j3pClone(tab)
    for (let i = 0; i < copieTab.length; i++) {
      const B = { x: copieTab[i].x2, y: copieTab[i].y2 }
      const A = { x: copieTab[i].x1, y: copieTab[i].y1 }
      for (let j = copieTab.length - 1; j > i; j--) {
        const D = { x: copieTab[j].x2, y: copieTab[j].y2 }
        const C = { x: copieTab[j].x1, y: copieTab[j].y1 }
        if (pointApSeg(C, A, B) || pointApSeg(D, A, B) || pointApSeg(B, C, D) || pointApSeg(A, C, D)) {
          const distAB = stor.scratch.dist(A, B)
          const distAC = stor.scratch.dist(A, C)
          const distAD = stor.scratch.dist(A, D)
          const distBC = stor.scratch.dist(B, C)
          const distBD = stor.scratch.dist(B, D)
          const distCD = stor.scratch.dist(C, D)
          const lemax = Math.max(distAB, distAC, distAD, distBC, distBD, distCD)
          copieTab.splice(j, 1)
          if (lemax === distAB) {
            copieTab[i].x1 = A.x
            copieTab[i].y1 = A.y
            copieTab[i].x2 = B.x
            copieTab[i].y2 = B.y
          } else if (lemax === distAC) {
            copieTab[i].x1 = A.x
            copieTab[i].y1 = A.y
            copieTab[i].x2 = C.x
            copieTab[i].y2 = C.y
          } else if (lemax === distAD) {
            copieTab[i].x1 = A.x
            copieTab[i].y1 = A.y
            copieTab[i].x2 = D.x
            copieTab[i].y2 = D.y
          } else if (lemax === distBC) {
            copieTab[i].x1 = B.x
            copieTab[i].y1 = B.y
            copieTab[i].x2 = C.x
            copieTab[i].y2 = C.y
          } else if (lemax === distBD) {
            copieTab[i].x1 = B.x
            copieTab[i].y1 = D.y
            copieTab[i].x2 = D.x
            copieTab[i].y2 = D.y
          } else {
            copieTab[i].x1 = C.x
            copieTab[i].y1 = C.y
            copieTab[i].x2 = D.x
            copieTab[i].y2 = D.y
          }
        }

        // si oui
        // test si C1C2 = k AB

        // si oui prendle plus grand, vire C1C2 du tab, et remplace AB par le plus grand
      }
    }
    return copieTab
  }
  function pointApSeg (C, A, B) {
    const AB = { x: B.x - A.x, y: B.y - A.y }
    const AC = { x: C.x - A.x, y: C.y - A.y }
    const vecABC = AB.x * AC.y - AB.y * AC.x
    const CA = { x: -AC.x, y: -AC.y }
    const CB = { x: B.x - C.x, y: B.y - C.y }
    const scalCACB = CA.x * CB.x + CA.y * CB.y
    return (Math.abs(vecABC) < 0.000001 && scalCACB <= 0)
  }
  function decoupeAffiche (ou, text, tabCodeImage) {
    const enoceCoupe = text.split(tabCodeImage[0].code)
    const newTab = j3pClone(tabCodeImage)
    newTab.splice(0, 1)
    if (newTab.length === 0) {
      j3pAffiche(ou, null, enoceCoupe[0], { a: stor.valmin, b: stor.valmax })
      for (let i = 1; i < enoceCoupe.length; i++) {
        j3pAddElt(ou, 'img', { src: tabCodeImage[0].image })
        j3pAffiche(ou, null, enoceCoupe[i], { a: stor.valmin, b: stor.valmax })
      }
    } else {
      decoupeAffiche(ou, enoceCoupe[0], newTab)
      for (let i = 1; i < enoceCoupe.length; i++) {
        j3pAddElt(ou, 'img', { src: tabCodeImage[0].image })
        decoupeAffiche(ou, enoceCoupe[i], newTab)
      }
    }
  }

  function enonceMain () {
    if (stor.dialog) {
      stor.dialog.destroy()
      stor.dialog = null
    }
    const scratch = stor.scratch
    scratch.params.yaTestEvent = false
    scratch.params.NomsVariableAutorise = []
    scratch.params.blokmenulimite = [
      { type: 'Drapo', nb: 1 },
      { type: 'FLECHEG', nb: 1 },
      { type: 'FLECHED', nb: 1 },
      { type: 'FLECHEB', nb: 1 },
      { type: 'FLECHEH', nb: 1 },
      { type: 'FLECHEESP', nb: 1 },
      { type: 'FLECHEA', nb: 1 },
      { type: 'EVENTSou', nb: 1 }
    ]
    stor.ProgEnCours = ds.ProgrammeLT2FF.pop()
    scratch.params.MohaMode = true
    scratch.params.MohaModeCo = true
    stor.listeBloc = j3pClone(stor.ProgEnCours.listeBloc)
    const montabMenu = []
    for (let i = 0; i < stor.listeBloc.length; i++) {
      let cmpt = 0
      for (let j = 0; j < stor.listeBloc[i].contenu.length; j++) {
        if (stor.listeBloc[i].contenu[j].aff) cmpt++
      }
      if (cmpt !== 0) {
        montabMenu.push(j3pClone(stor.listeBloc[i]))
        for (let j = montabMenu[montabMenu.length - 1].contenu.length - 1; j > -1; j--) {
          if (!stor.listeBloc[i].contenu[j].aff) montabMenu[montabMenu.length - 1].contenu.splice(j, 1)
        }
      }
      if (i === stor.listeBloc.length - 1) {
        if (stor.listeBloc[i].aff) montabMenu.push(j3pClone(stor.listeBloc[i]))
      }
    }
    if (montabMenu.length === 0) {
      montabMenu.push({
        type: 'catégorie',
        nom: 'DEPLACEMENTS',
        coul1: '#4C97FF',
        coul2: '#3373CC',
        contenu: [
          { type: 'avancer', value: [{ nom: 'STEPS', contenu: { valeur: '20' } }], aff: false }
        ]
      })
    }
    scratch.params.scratchProgDonnees.menu = j3pClone(montabMenu)

    let okClav = false
    for (let i = 0; i < montabMenu.length; i++) {
      if (montabMenu[i].nom === 'EVENEMENT') {
        for (let j = 0; j < montabMenu[i].contenu.length; j++) {
          okClav = okClav || (montabMenu[i].contenu[j].type !== 'Drapo')
        }
      }
    }
    const listClav = ['◄', '►', '▲', '▼', 'ª', '▂']
    for (let i = 0; i < listClav.length; i++) {
      okClav = okClav || stor.ProgEnCours.ProgDeb.indexOf(listClav[i]) !== -1
    }
    scratch.params.yaFauxClavier = okClav
    if (scratch.params.yaFauxClavier) scratch.params.NbEssaiIllimite = true
    me.videLesZones()
    scratch.params.scratchProgDonnees.listValPos = stor.ProgEnCours.listValPos
    scratch.params.scratchProgDonnees.Lutins[0].cox = 100
    scratch.params.scratchProgDonnees.Lutins[0].coxbase = 100
    scratch.params.yaFond = stor.ProgEnCours.imageFond.type !== 'aucun'
    scratch.params.lefond = stor.ProgEnCours.imageFond.txtFigure
    scratch.params.figureAfaire = []
    if (stor.ProgEnCours.imageFond.type === 'axe') {
      for (let i = 0; i < stor.ProgEnCours.imageFond.points.length; i++) {
        scratch.params.figureAfaire.push({ type: 'croix', x: stor.ProgEnCours.imageFond.points[i].x, y: stor.ProgEnCours.imageFond.points[i].y })
      }
    }
    scratch.params.yaCaseAlea = false
    if (stor.ProgEnCours.imageFond.type === 'cases') {
      for (let i = 0; i < stor.ProgEnCours.imageFond.cases.length; i++) {
        const Kool = []
        for (let j = 0; j < stor.ProgEnCours.imageFond.cases[i].coul.length; j++) {
          Kool.push(scratch.fileCouleur(stor.ProgEnCours.imageFond.cases[i].coul[j]))
        }
        let cCaseAlea = stor.ProgEnCours.imageFond.cases[i].cox.length > 1
        cCaseAlea = cCaseAlea || stor.ProgEnCours.imageFond.cases[i].coy.length > 1
        cCaseAlea = cCaseAlea || stor.ProgEnCours.imageFond.cases[i].coul.length > 1
        scratch.params.yaCaseAlea = scratch.params.yaCaseAlea || cCaseAlea
        if (!cCaseAlea) {
          scratch.params.figureAfaire.push({
            type: 'Cases',
            x: stor.ProgEnCours.imageFond.cases[i].cox,
            y: stor.ProgEnCours.imageFond.cases[i].coy,
            width: 50,
            height: 50,
            coul: Kool
          })
        }
      }
    }
    if (scratch.params.yaCaseAlea) {
      scratch.params.scratchProgDonnees.cases = scratch.makeAleaCases(stor.ProgEnCours.imageFond.cases)
      scratch.params.scratchProgDonnees.randomCases = j3pShuffle(scratch.params.scratchProgDonnees.cases).pop()
    }
    if (!stor.ProgEnCours.lutinScratch) {
      scratch.params.scratchProgDonnees.Lutins[0].img = chatImg
      scratch.params.scratchProgDonnees.Lutins[0].larg = 120
      scratch.params.scratchProgDonnees.Lutins[0].haut = 120
      if (stor.ProgEnCours.coxLut && stor.ProgEnCours.coyLut) {
        scratch.params.scratchProgDonnees.Lutins[0].coxbase = (stor.ProgEnCours.coxLut - 1) * 40
        scratch.params.scratchProgDonnees.Lutins[0].coybase = (stor.ProgEnCours.coyLut - 1) * 20 + 100
      }
    } else {
      if (stor.ProgEnCours.lutinScratch === 'Petit Scratch') {
        scratch.params.scratchProgDonnees.Lutins[0].img = chatImg
        scratch.params.scratchProgDonnees.Lutins[0].larg = 50
        scratch.params.scratchProgDonnees.Lutins[0].haut = 50
        scratch.params.scratchProgDonnees.Lutins[0].coxbase = (stor.ProgEnCours.coxLut - 1) * 50
        scratch.params.scratchProgDonnees.Lutins[0].coybase = (stor.ProgEnCours.coyLut - 1) * 50
      } else {
        scratch.params.scratchProgDonnees.Lutins[0].img = dessinleveImg
        scratch.params.scratchProgDonnees.Lutins[0].larg = 100
        scratch.params.scratchProgDonnees.Lutins[0].haut = 100
      }
    }
    scratch.enonce([], {
      pourInject: { },
      modeCorrection: {
        type: 'Comparaison',
        aVerif: [],
        lesvarbonnes: []
      },
      dirigeProg: { sequence: ds.Sequence, pasApas: ds.PasAPas, cacheTest: false }
    })
    const side = 'start'
    scratch.params.workspaceDemo = Blockly.inject(scratch.lesdivs.sortieMoha, {
      comments: true,
      disable: true,
      collapse: false,
      media: '/externals/blockly/media/',
      readOnly: true,
      rtl: null,
      scrollbars: true,
      toolbox: null,
      toolboxPosition: side === 'top' || side === 'start' ? 'start' : 'end',
      horizontalLayout: side === 'top' || side === 'bottom',
      sounds: false,
      zoom: {
        controls: true,
        wheel: true,
        startScale: 0.6,
        maxScale: 2,
        minScale: 0.25,
        scaleSpeed: 1.1
      },
      colours: {
        fieldShadow: 'rgba(100, 100, 100, 0.3)',
        dragShadowOpacity: 0.6
      }
    })
    scratch.lesdivs.sortieMoha.style.height = '0px'
    scratch.lesdivs.sortieMoha.style.width = '0px'
    // scratch.lesdivs.sortieMoha.style.display = ''
    Blockly.svgResize(scratch.params.workspaceDemo)
    refaisProg($.parseXML(stor.ProgEnCours.ProgrammeAt), scratch.params.workspaceDemo)
    scratch.params.CoMOha = stor.ProgEnCours.ProgrammeAt
    scratch.params.ProgCompare = []
    scratch.params.ProgCompare = Blockly.JavaScript.workspaceToCode(scratch.params.workspaceDemo)
    // TODO, ajouter lesvarbonnes

    scratch.dansBlokMoha = true
    scratch.ref = () => {
      if (stor.ProgEnCours.ProgrammeMod) {
        scratch.params.verifDeb = false
        refaisProg($.parseXML(stor.ProgEnCours.ProgDeb), scratch.params.workspaceEl)
      } else {
        scratch.params.verifDeb = true
        refaisProg($.parseXML(rendpaseditable(stor.ProgEnCours.ProgDeb)), scratch.params.workspaceEl)
        scratch.params.acomp = scratch.scratchRunBlockly(scratch.params.workspaceEl)
      }
    }
    scratch.ref()
    const robert = addDefaultTable(scratch.lesdivs.enonceG, 2, 1)
    stor.aViVide = robert[1][0]
    decoupeAffiche(robert[0][0], stor.ProgEnCours.enonce, [{ code: '££1', image: retourImg }, { code: '££2', image: drapeauImg }, { code: '££3', image: boutonImg }, { code: '££4', image: okImg }])
    if (okClav && stor.ProgEnCours.Rendu1) {
      stor.ProgEnCours.Rendu2 = false
    }
    let ajj2 = 'Résultat du programme attendu'
    if (!stor.ProgEnCours.Rendu2) {
      ajj2 = 'Tester le programme attendu'
    }
    if (stor.ProgEnCours.Rendu1 && stor.ProgEnCours.ProgrammeAt.indexOf('Get_us') === -1) {
      if (okClav) {
        stor.dialog = new Dialog({
          title: ajj2,
          showText: true,
          width: '335px',
          height: 530
        })
      } else {
        stor.dialog = new Dialog({
          title: ajj2,
          showText: true,
          width: '335px',
          height: 480
        })
      }
      const kkjjj = addDefaultTable(stor.dialog.container, 4, 1)
      stor.dialog.container.style.background = '#000'
      kkjjj[1][0].style.textAlign = 'center'
      kkjjj[0][0].style.textAlign = 'center'
      kkjjj[0][0].style.height = '40px'
      kkjjj[0][0].style.padding = '0px'
      j3pAjouteBouton(kkjjj[0][0], function () {
        stor.dialog.toggle()
      }, { value: 'Fermer' })
      scratch.params.scratchProgDonnees.mypreDemo = kkjjj[1][0]
      let tabEventDrap
      if (okClav) {
        const tabEvent = addDefaultTable(kkjjj[2][0], 2, 1)
        tabEventDrap = tabEvent[0][0]
        tabEvent[0][0].style.padding = '10px'
        tabEvent[1][0].style.border = '1px solid black'
        tabEvent[1][0].style.borderRadius = '5px'
        tabEvent[1][0].style.background = '#79d3c4'
        tabEvent[1][0].style.width = '300px'
        const divClav = j3pAddElt(tabEvent[1][0], 'div')
        divClav.style.padding = '10px'
        divClav.style.display = 'grid'
        divClav.style.gridTemplateRows = 'auto'
        divClav.style.gridTemplateColumns = 'auto auto auto auto auto auto auto auto auto auto auto auto auto'
        const listClav = ['◄', '►', '▲', '▼', 'ª', '▂']
        for (let i = 0; i < 6; i++) {
          const td1 = j3pAddElt(divClav, 'div')
          td1.style.gridColumn = String(i * 2 + 2)
          td1.style.gridRow = '1'
          td1.style.display = 'flex'
          td1.style.alignItems = 'center'
          td1.style.justifyContent = 'center'
          td1.style.textAlign = 'center'
          td1.style.border = '1px solid black'
          td1.style.borderRadius = '3px'
          j3pAddContent(td1, (listClav[i] === 'ª') ? 'a' : listClav[i])
          td1.classList.add('cKlav')
          td1.addEventListener('click', () => { scratch.lanceEvent(listClav[i] + 'Demo') })
        }
      } else {
        tabEventDrap = kkjjj[2][0]
      }
      const lanceursrsDemo = kkjjj[2][0]
      const sp2 = j3pAddElt(tabEventDrap, 'span')
      scratch.params.drapeauDemo = j3pAddElt(sp2, 'img', { src: drapeauImg })
      scratch.params.drapeauDemo.addEventListener('click', () => { scratch.lanceEvent('drapeauDemo') }, false)
      j3pStyle(scratch.params.drapeauDemo, { cursor: 'pointer' })
      j3pAjouteBouton(kkjjj[3][0], scratch.reinitDemo.bind(scratch), { value: 'Réinitialiser' })
      scratch.traceSortieBaseDemo()
      let ajj = 'Tester le programme attendu'
      if (stor.ProgEnCours.Rendu2) {
        scratch.lanceEvent('drapeauDemoVite')
        ajj = 'Résultat du programme attendu'
        scratch.cacheDrapeauxDemo = () => {
          lanceursrsDemo.style.display = 'none'
        }
        scratch.montreBoutonReinitDemo = () => {
          kkjjj[3][0].style.display = 'none'
        }
        scratch.montreDrapeauxDemo = () => {
          lanceursrsDemo.style.display = 'none'
        }
        scratch.cahceBoutonReinitDemo = () => {
          kkjjj[3][0].style.display = 'none'
        }
      } else {
        scratch.cacheDrapeauxDemo = () => {
          lanceursrsDemo.style.display = 'none'
        }
        scratch.montreBoutonReinitDemo = () => {
          kkjjj[3][0].style.display = ''
        }
        scratch.montreDrapeauxDemo = () => {
          lanceursrsDemo.style.display = ''
        }
        scratch.cahceBoutonReinitDemo = () => {
          kkjjj[3][0].style.display = 'none'
        }
      }
      scratch.cahceBoutonReinitDemo()
      j3pAjouteBouton(robert[1][0], voirRendu, { value: ajj })
      setTimeout(voirRendu, 2000)
    }
    scratch.fautRinitDemo = false

    if (stor.scratch.defineRandom()) {
      const bornes = stor.scratch.retrouveBorne()
      const verifAle = []
      for (let j = 0; j < bornes.length; j++) {
        verifAle[j] = []
        for (let i = bornes[j].borneInf; i < bornes[j].borneSup + 1; i++) {
          verifAle[j].push({ val: i, idBlok: bornes[j].idBlok })
        }
      }
      stor.scratch.params.valideAlea = verifAle
    } else {
      stor.scratch.params.valideAlea = undefined
    }
    me.finEnonce()
    me.cacheBoutonValider()
  }

  switch (this.etat) {
    case 'enonce':
      if (this.debutDeLaSection) {
        initSection()
      } else {
        enonceMain()
      }
      break // case "enonce":

    case 'correction': {
      const scratch = stor.scratch
      try {
        if (stor.dialog) {
          stor.dialog.destroy()
          stor.dialog = null
        }
        j3pEmpty(stor.aViVide)
        const ret = scratch.prepCorrec()
        const xmlDoc2 = ret.xmlDoc2
        const xmlText = ret.xmlText

        if (scratch.params.scratchProgDonnees.repEleve) {
          // Bonne réponse
          this.score++
          scratch.corrigeGood(xmlDoc2, xmlText)
        } else {
          // Pas de bonne réponse
          scratch.corrige(xmlDoc2, xmlText)
        }
        // on veut être rappelé dans l’état navigation
        this.finCorrection('navigation', true)
      } catch (error) {
        console.error(error)
      }
      scratch.params.MohaMode = false
      break // case "correction":
    }

    case 'navigation':
      if (this.sectionTerminee()) {
        stor.scratch.lesdivs.BoutonsSuite.appendChild(this.buttonsElts.sectionSuivante)
        this.buttonsElts.sectionSuivante.addEventListener('click', () => {
          stor.scratch.params.grosStop = true
        })
      } else {
        this.etat = 'enonce'
        stor.scratch.lesdivs.BoutonsSuite.appendChild(this.buttonsElts.suite)
        this.buttonsElts.suite.addEventListener('click', () => {
          stor.scratch.params.grosStop = true
        })
      }
      // Obligatoire
      this.finNavigation()
      break // case "navigation":
  }
}
