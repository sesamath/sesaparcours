import { j3pAddContent, j3pAddElt, j3pAjouteBouton, j3pDetruit, j3pEmpty } from 'src/legacy/core/functions'
import EditorProgrammeScratchMake from 'src/legacy/outils/paramEditors/EditorProgrammeScratchMake'
import { getNewScratch } from 'src/legacy/outils/scratch/loaders'
import { j3pModale2 } from 'src/legacy/outils/zoneStyleMathquill/functions'
import loadMq from 'src/lib/mathquill/loadMathquill'
import { getMtgCore } from 'src/lib/outils/mathgraph'
import getDefaultProgramme from './defaultProgramme'

export default function editorProgramme (container, values) {
  // on ajoute notre éditeur dans container
  const btnsContainer = j3pAddElt(container, 'div', 'Chargement en cours…', {
    style: {
      margin: '1rem auto',
      textAlign: 'center'
    }
  })

  return new Promise((resolve, reject) => {
    let editor

    function verifYaPasExo () {
      if (editor.yaBlok || editor.stor.yaBlok) {
        const yy = j3pModale2({ titre: 'Avertissement ', contenu: 'Un exercice est en cours\n de modification.', divparent: container })
        j3pAddContent(yy, '\n')
        j3pAjouteBouton(yy, () => {
          j3pDetruit(yy)
          j3pDetruit(yy.masque)
        }, { value: 'Annuler' })
        j3pAddContent(yy, '\n')
        j3pAddContent(yy, '\n')
        j3pAjouteBouton(yy, () => {
          j3pDetruit(yy)
          j3pDetruit(yy.masque)
          onSaveParams()
        }, { value: 'Tant pis' })
        return
      }
      onSaveParams()
    }

    function onSaveParams () {
      if (document.fullscreenElement) document.exitFullscreen()
      clearInterval(editor.intervalId)
      // on résoud la promesse avec nos nouvelles valeurs
      resolve({
        Parametres: JSON.stringify({
          // faut mettre du json valide pour ce param
          Programme: editor.stor.Programme,
          // on peut aussi affecter d’autres paramètres (ça fonctionne pas encore pour les boolean ni les listes)
          // ca c’est trop cool :)
          Sequence: editor.stor.ParamSequence.reponse === 'Oui',
          PasAPas: editor.stor.ParamPasAPas.reponse === 'Oui',
          Difficulte: editor.stor.ParamDifficul.reponse
        }),
        nbchances: (editor.stor.listeparamChance.reponse === 'Illimité') ? 0 : Number(editor.stor.inputNbChances.value),
        nbrepetitions: Number(editor.stor.ParamnbExos.value),
        limite: (editor.stor.listeparamLimite.reponse === 'Non') ? 0 : Number(editor.stor.inputLimite.value)
      })
    }

    getNewScratch()
      .then(scratch => {
        container.parentNode.style.opacity = 1
        editor = new EditorProgrammeScratchMake({ container, getDefaultProgramme, values, scratch })
      })
      .then(loadMq)
      .then(() => getMtgCore({ withMathJax: true }))
      .then(mtgAppLecteur => {
        // on vire le message de chargement
        j3pEmpty(btnsContainer)
        editor.scratch.params.mtgApplecteur = mtgAppLecteur
        editor.stor.listePourCalc = mtgAppLecteur.createList('TWF0aEdyYXBoSmF2YTEuMAAAABI+TMzNAAJmcv###wEA#wEAAAAAAAAAAAQsAAACvQAAAQEAAAAAAAAAAQAAAAP#####AAAAAQAKQ0NhbGNDb25zdAD#####AAJwaQAWMy4xNDE1OTI2NTM1ODk3OTMyMzg0Nv####8AAAABAApDQ29uc3RhbnRlQAkh+1RELRj#####AAAAAQAHQ0NhbGN1bAD#####AAFlAAZleHAoMSn#####AAAAAgAJQ0ZvbmN0aW9uBwAAAAE#8AAAAAAAAAAAAAIA#####wABeAABMAAAAAEAAAAAAAAAAP###############w==')
        editor.creeLesDiv()
        editor.stor.Programme = []
        editor.menuTotal()

        // Important, un bouton annuler
        const btnProps = { style: { margin: '0.2rem', fontSize: '120%' } }
        const btnAnnuler = j3pAddElt(btnsContainer, 'button', 'Annuler', btnProps)
        // et le bouton valider
        const btnValider = j3pAddElt(btnsContainer, 'button', 'Valider', btnProps)
        // et les comportements au clic
        btnAnnuler.addEventListener('click', () => {
          clearInterval(editor.intervalId)
          if (document.fullscreenElement) document.exitFullscreen()
          resolve()
        })
        btnValider.addEventListener('click', verifYaPasExo)
      })
      .catch(reject)
  }) // promesse retournée
}
