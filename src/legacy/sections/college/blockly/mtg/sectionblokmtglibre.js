import { j3pClone, j3pDetruit, j3pEmpty } from 'src/legacy/core/functions'
import { addDefaultTable } from 'src/legacy/themes/table'
import $ from 'jquery'
import { getMtgCore } from 'src/lib/outils/mathgraph'
import { getNewScratchMathgraph } from 'src/legacy/outils/scratch/loaders'
import { j3pBaseUrl } from 'src/lib/core/constantes'

const defaultProgramme = [
  {
    figdep: [],
    suite: [],
    placeDep: true,
    listeBloc: {
      pointLibreAvecNom: true,
      pointIntersection: true,
      pointSur: true,
      pointMilieu: true,
      pointLong: true,
      segmentAB: true,
      segmentlongA: true,
      droitept: true,
      droitepara: true,
      droiteperp: true,
      demidroitept: true,
      cercleCentrePoint: true,
      cercleCentreRayon: true,
      effacer: true,
      arc1PointPasse: true,
      arccentreplus: true,
      arccentremoins: true,
      angleplus: true,
      anglemoins: true,
      pointille: true
    },
    crea: true,
    niv: 2,
    nom: 'im1',
    blocPerso: []
  }
]

export const params = {
  outils: ['mathquill'],
  /**
   * Ce qui est paramétrable dans l’éditeur de graphe
   * @type {LegacySectionParamsParametres}
   */
  parametres: [
    ['nbrepetitions', 4, 'entier', 'Nombre de répétitions de la section'],
    ['PasAPas', true, 'boolean', "<u>true</u>: L’élève peut lancer son programme en mode 'Pas à pas'."],
    ['Sequence', true, 'boolean', '<u>true</u>: Chaque bloc a une durée d’exécution d’environ 1 seconde.'],
    // FIXME rectifier l’explication (y’a pas d’adresse)
    ['Programme', JSON.stringify(defaultProgramme), 'string', 'Json figure de base et figure attendue\nvoir à cette adresse pour le construire'],
    ['Difficulte', 'Progressive', 'liste', 'aléatoire ou linéaire', ['Aléatoire', 'Progressive']]
  ]
}

/**
 * section blokmtglibre
 * @this {Parcours}
 */
export default function main () {
  const me = this
  const stor = me.storage
  const ds = me.donneesSection

  function verifYa (ob) {
    if (ob.need === false) return { ok: true, mess: '' }
    const scratch = stor.scratch
    const newob = scratch.scratchMtgReplaceLettres(ob)
    let okbuf = false
    let iagarde = -1
    for (let j = 0; j < newob.noms.length; j++) {
      for (let i = 0; i < stor.result.length; i++) {
        if (stor.result[i].noms.indexOf(newob.noms[j]) !== -1) {
          iagarde = i
          okbuf = true
          break
        }
        if (okbuf) break
      }
    }
    if (!okbuf) return { ok: false, mess: 'Je ne trouve pas ' + lela(newob.type) + ' ' + newob.type.replace('pt', '') + ' ' + newob.noms[0] + '<br>(<i>regarde la figure modèle</i>)' }
    if (stor.result[iagarde].type !== newob.type.replace('pt', '')) return { ok: false, mess: newob.noms[0] + ' est ' + unune(newob.type) + ' ' + newob.type.replace('pt', '') + ' !' }
    // ya le nom
    if (newob.sur) {
      for (let i = 0; i < ob.sur.length; i++) {
        okbuf = false
        const tabNoms = scratch.scratchMtgRetrouveFin({ nom: newob.sur[i] }, 'noms')
        for (let j = 0; j < tabNoms.length; j++) {
          for (let k = 0; k < stor.result[iagarde].sur.length; k++) {
            if (stor.result[iagarde].sur[k] === tabNoms[j]) {
              okbuf = true
            }
          }
        }
        if (!okbuf) {
          return { ok: false, mess: lela(newob.type).replace('pt', '') + ' ' + newob.type + ' ' + newob.noms[0] + ' doit appartenir à ' + newob.sur[i] }
        }
      }
    }
    return { ok: true, mess: '' }
  }
  function lela (s) {
    switch (s) {
      case 'point':
      case 'segment':
      case 'cercle': return 'le'
      case 'droitept':
      case 'demidroite': return 'la'
    }
  }
  function unune (s) {
    switch (s) {
      case 'point':
      case 'segment':
      case 'cercle': return 'un'
      case 'droitept':
      case 'demidroite': return 'une'
    }
  }

  async function initSection () {
    me.validOnEnter = false // ex donneesSection.touche_entree
    ds.pe = 'lu' // pour dire au modèle qu’il n’y a pas de score à afficher

    const scratch = await getNewScratchMathgraph()
    const { default: figure } = await import('src/legacy/outils/scratch/defaultFigure.js')
    scratch.setFigure(figure, true)

    const Blockly = scratch.Blockly
    stor.scratch = scratch
    stor.dejafait2 = []
    stor.dejafait = []
    scratch.initParams(me)
    scratch.testFlag = function () {

    }
    scratch.testFlagFin = function () {
      const aret = scratch.params.MtgProg.figdep.length
      const att = j3pClone(scratch.params.MtgProg.suite)
      stor.result = j3pClone(scratch.params.donnesMtg)
      stor.result.splice(0, aret)
      let ok = true
      let mess = ''
      for (let i = 0; i < att.length; i++) {
        const retour = verifYa(att[i])
        if (!retour.ok) {
          mess = retour.mess
          ok = false
          break
        }
      }
      return { ok, mess }
    }
    scratch.refaisMenu = function () {
      if (scratch.params.BlocsAAj) {
        const x2 = scratch.scratchCreeMenu([], true)
        const xmlDoc2 = $.parseXML(x2)
        let xml2 = xmlDoc2.firstChild
        while (xml2.nodeType !== 1) {
          xml2 = xml2.nextSibling
        }
        const aVire = addDefaultTable(me.zonesElts.MG, 1, 1)[0][0]
        for (const property in scratch.params.BlocsAAj) {
          const iDej = scratch.params.lesBlocPerso.indexOf(property)
          const nwBlo = scratch.params.BlocsAAj[property]
          const obdep = {
            message0: nwBlo.txt,
            args0: [],
            category: Blockly.Categories.motion,
            colour: nwBlo.colour,
            extensions: ['shape_statement']
          }
          const value = []
          let funX = () => ''
          let funY = () => ''
          let funZ = () => ''
          if (nwBlo.nbVar > 0) {
            obdep.args0.push({ type: 'input_value', name: 'X', default: '' })
            funX = (block) => scratch.Blockly.JavaScript.valueToCode(block, 'X', Blockly.JavaScript.ORDER_ATOMIC).replace(/'/g, '')
            value.push({ nom: 'X', contenu: { valeur: ' ' } })
          }
          if (nwBlo.nbVar > 1) {
            obdep.args0.push({ type: 'input_value', name: 'Y', default: '' })
            funY = (block) => { return Blockly.JavaScript.valueToCode(block, 'Y', Blockly.JavaScript.ORDER_ATOMIC).replace(/'/g, '') }
            value.push({ nom: 'Y', contenu: { valeur: ' ' } })
          }
          if (nwBlo.nbVar > 2) {
            obdep.args0.push({ type: 'input_value', name: 'Z', default: '' })
            funZ = (block) => { return Blockly.JavaScript.valueToCode(block, 'Z', Blockly.JavaScript.ORDER_ATOMIC).replace(/'/g, '') }
            value.push({ nom: 'Z', contenu: { valeur: ' ' } })
          }
          obdep.message0 = nwBlo.txt
          if (nwBlo.yaicone) {
            obdep.message0 = '%1 %2' + obdep.message0
            obdep.args0.splice(0, 0, { type: 'field_vertical_separator' })
            obdep.args0.splice(0, 0, {
              type: 'field_image',
              src: nwBlo.icone,
              width: 30,
              height: 30
            })
          }
          const xml = $.parseXML(nwBlo.xmlText)
          if (iDej === -1) {
            scratch.params.lesBlocPerso.push(nwBlo.nom)
          }
          scratch.params.listeDef[nwBlo.nom] = { nom: nwBlo.nom, nbVar: nwBlo.nbVar, xml, txt: nwBlo.txt, yaicone: nwBlo.yaicone, xmlText: nwBlo.xmlText }
          if (nwBlo.yaicone) scratch.params.listeDef[nwBlo.nom].icone = nwBlo.icone

          Blockly.Blocks[nwBlo.nom] = {
            init: function () {
              this.jsonInit({
                type: 'make',
                message0: 'Définir ' + nwBlo.nom,
                inputsInline: false,
                nextStatement: null,
                colour: 330,
                tooltip: '',
                helpUrl: ''
              })
            }
          }
          Blockly.JavaScript[nwBlo.nom] = function () {
            return { type: 'make', eventtype: 'make', nom: nwBlo.nom }
          }
          Blockly.Blocks[nwBlo.nom + 'b'] = {
            init: function () {
              this.jsonInit(obdep)
            }
          }
          Blockly.JavaScript[nwBlo.nom + 'b'] = function (block) {
            const retValx = function () {
              return funX(block)
            }
            const retvaly = function () {
              return funY(block)
            }
            const retvalz = function () {
              return funZ(block)
            }
            return { type: 'Mb', nom: nwBlo.nom + 'b', valx: retValx(), valy: retvaly(), valz: retvalz() }
          }

          const side = 'start'
          scratch.params.scratchProgDonnees.workspaceDef = Blockly.inject(aVire, {
            comments: true,
            disable: false,
            collapse: false,
            media: j3pBaseUrl + 'externals/blockly/media/',
            readOnly: false,
            rtl: null,
            scrollbars: true,
            toolbox: xml2,
            toolboxPosition: side === 'top' || side === 'start' ? 'start' : 'end',
            horizontalLayout: side === 'top' || side === 'bottom',
            sounds: false,
            zoom: {
              controls: true,
              wheel: true,
              startScale: 0.6,
              maxScale: 2,
              minScale: 0.25,
              scaleSpeed: 1.1
            },
            colours: {
              fieldShadow: 'rgba(100, 100, 100, 0.3)',
              dragShadowOpacity: 0.6
            }
          })
          Blockly.svgResize(scratch.params.scratchProgDonnees.workspaceDef)
          // scratch.params.scratchProgDonnees.workspaceDef.clear()
          let x = xml.firstChild
          while (x.nodeType !== 1) {
            x = x.nextSibling
          }
          Blockly.Xml.domToWorkspace(x, scratch.params.scratchProgDonnees.workspaceDef)
          const ladef = Blockly.JavaScript.workspaceToCode(scratch.params.scratchProgDonnees.workspaceDef)
          ladef[0].splice(0, 1)
          scratch.params.listeDef[nwBlo.nom].def = ladef[0]
          scratch.params.listeDef[nwBlo.nom].value = value
          j3pEmpty(aVire)
        }
        j3pDetruit(aVire)
      }

      scratch.params.scratchProgDonnees.menu = []
      const lib = scratch.params.MtgProg.listeBloc
      if (lib.pointLibreAvecNom || lib.pointIntersection || lib.pointSur || lib.pointMilieu || lib.pointLong) {
        scratch.params.scratchProgDonnees.menu.push({ type: 'catégorie', nom: 'POINTS', coul1: '#3230B8', coul2: '#3373CC', contenu: [] })
        if (lib.pointLibreAvecNom) scratch.params.scratchProgDonnees.menu[scratch.params.scratchProgDonnees.menu.length - 1].contenu.push({ type: 'pointLibreAvecNom', value: [{ nom: 'NOM', contenu: { valeur: 'A' } }] })
        if (lib.pointIntersection) scratch.params.scratchProgDonnees.menu[scratch.params.scratchProgDonnees.menu.length - 1].contenu.push({ type: 'pointIntersection', value: [{ nom: 'NOM', contenu: { valeur: 'A' } }, { nom: 'CONT1', contenu: { valeur: ' ' } }, { nom: 'CONT2', contenu: { valeur: ' ' } }] })
        if (lib.pointSur) scratch.params.scratchProgDonnees.menu[scratch.params.scratchProgDonnees.menu.length - 1].contenu.push({ type: 'pointSur', value: [{ nom: 'NOM', contenu: { valeur: 'A' } }, { nom: 'CONT', contenu: { valeur: ' ' } }] })
        if (lib.pointMilieu) scratch.params.scratchProgDonnees.menu[scratch.params.scratchProgDonnees.menu.length - 1].contenu.push({ type: 'pointMilieu', value: [{ nom: 'NOM', contenu: { valeur: 'I' } }, { nom: 'CONT', contenu: { valeur: 'AB' } }] })
        if (lib.pointLong) scratch.params.scratchProgDonnees.menu[scratch.params.scratchProgDonnees.menu.length - 1].contenu.push({ type: 'pointLong', value: [{ nom: 'EXT1', contenu: { valeur: 'A' } }, { nom: 'LONG', contenu: { valeur: ' ' } }, { nom: 'P1', contenu: { valeur: ' ' } }] })
      }

      if (lib.segmentAB || lib.segmentlongA) {
        scratch.params.scratchProgDonnees.menu.push({ type: 'catégorie', nom: 'SEGMENTS', coul1: '#3230B8', coul2: '#3373CC', contenu: [] })
        if (lib.segmentAB) scratch.params.scratchProgDonnees.menu[scratch.params.scratchProgDonnees.menu.length - 1].contenu.push({ type: 'segmentAB', value: [{ nom: 'EXT1', contenu: { valeur: 'AB' } }] })
        if (lib.segmentlongA) scratch.params.scratchProgDonnees.menu[scratch.params.scratchProgDonnees.menu.length - 1].contenu.push({ type: 'segmentlongA', value: [{ nom: 'EXT1', contenu: { valeur: 'AB' } }, { nom: 'LONG', contenu: { valeur: '3' } }] })
      }

      if (lib.droitept || lib.droitepara || lib.droiteperp) {
        scratch.params.scratchProgDonnees.menu.push({ type: 'catégorie', nom: 'DROITES', coul1: '#3230B8', coul2: '#3373CC', contenu: [] })
        if (lib.droitept) scratch.params.scratchProgDonnees.menu[scratch.params.scratchProgDonnees.menu.length - 1].contenu.push({ type: 'droitept', value: [{ nom: 'POINT1', contenu: { valeur: 'AB' } }] })
        if (lib.droitepara) scratch.params.scratchProgDonnees.menu[scratch.params.scratchProgDonnees.menu.length - 1].contenu.push({ type: 'droitepara', value: [{ nom: 'NOM', contenu: { valeur: '(d1)' } }, { nom: 'PARA', contenu: { valeur: '(AB)' } }, { nom: 'POINT', contenu: { valeur: 'C' } }] })
        if (lib.droiteperp) scratch.params.scratchProgDonnees.menu[scratch.params.scratchProgDonnees.menu.length - 1].contenu.push({ type: 'droiteperp', value: [{ nom: 'NOM', contenu: { valeur: '(d1)' } }, { nom: 'PERP', contenu: { valeur: '(AB)' } }, { nom: 'POINT', contenu: { valeur: 'C' } }] })
      }

      if (lib.demidroitept) {
        scratch.params.scratchProgDonnees.menu.push({ type: 'catégorie', nom: 'DEMI-DROITES', coul1: '#3230B8', coul2: '#3373CC', contenu: [] })
        scratch.params.scratchProgDonnees.menu[scratch.params.scratchProgDonnees.menu.length - 1].contenu.push({ type: 'demidroitept', value: [{ nom: 'ORIGINE', contenu: { valeur: 'AB' } }] })
      }

      if (lib.cercleCentrePoint || lib.cercleCentreRayon) {
        scratch.params.scratchProgDonnees.menu.push({ type: 'catégorie', nom: 'CERCLES', coul1: '#3230B8', coul2: '#3373CC', contenu: [] })
        if (lib.cercleCentrePoint) scratch.params.scratchProgDonnees.menu[scratch.params.scratchProgDonnees.menu.length - 1].contenu.push({ type: 'cercleCentrePoint', value: [{ nom: 'CENTRE', contenu: { valeur: 'O' } }, { nom: 'POINT', contenu: { valeur: 'A' } }, { nom: 'NOM', contenu: { valeur: 'C1' } }] })
        if (lib.cercleCentreRayon)scratch.params.scratchProgDonnees.menu[scratch.params.scratchProgDonnees.menu.length - 1].contenu.push({ type: 'cercleCentreRayon', value: [{ nom: 'CENTRE', contenu: { valeur: 'O' } }, { nom: 'RAYON', contenu: { valeur: '3' } }, { nom: 'NOM', contenu: { valeur: 'C1' } }] })
      }

      if (lib.arc1PointPasse || lib.arccentreplus || lib.arccentremoins) {
        scratch.params.scratchProgDonnees.menu.push({ type: 'catégorie', nom: 'ARC DE CERCLES', coul1: '#3230B8', coul2: '#3373CC', contenu: [] })
        if (lib.arc1PointPasse) scratch.params.scratchProgDonnees.menu[scratch.params.scratchProgDonnees.menu.length - 1].contenu.push({ type: 'arc1PointPasse', value: [{ nom: 'POINT', contenu: { valeur: 'M' } }, { nom: 'NOM', contenu: { valeur: 'BC' } }] })
        if (lib.arccentreplus) scratch.params.scratchProgDonnees.menu[scratch.params.scratchProgDonnees.menu.length - 1].contenu.push({ type: 'arccentreplus', value: [{ nom: 'CENTRE', contenu: { valeur: 'O' } }, { nom: 'NOM', contenu: { valeur: 'AB' } }] })
        if (lib.arccentremoins) scratch.params.scratchProgDonnees.menu[scratch.params.scratchProgDonnees.menu.length - 1].contenu.push({ type: 'arccentremoins', value: [{ nom: 'CENTRE', contenu: { valeur: 'O' } }, { nom: 'NOM', contenu: { valeur: 'AB' } }] })
      }

      if (lib.effacer || lib.renommer || lib.regroupe || lib.ecriSeg || lib.ecriDroite || lib.ecriDDroite || lib.nomAleatoire) {
        scratch.params.scratchProgDonnees.menu.push({ type: 'catégorie', nom: 'UTILITAIRES', coul1: '#B5E61D', coul2: '#3373CC', contenu: [] })
        if (lib.effacer) scratch.params.scratchProgDonnees.menu[scratch.params.scratchProgDonnees.menu.length - 1].contenu.push({ type: 'mtgEffacer', value: [{ nom: 'NOM', contenu: { valeur: ' ' } }] })
        if (lib.renommer) scratch.params.scratchProgDonnees.menu[scratch.params.scratchProgDonnees.menu.length - 1].contenu.push({ type: 'Renommer', value: [{ nom: 'NOM', contenu: { valeur: ' ' } }, { nom: 'NOM2', contenu: { valeur: ' ' } }] })
        if (lib.regroupe) scratch.params.scratchProgDonnees.menu[scratch.params.scratchProgDonnees.menu.length - 1].contenu.push({ type: 'Regroupe', value: [{ nom: 'A', contenu: { valeur: ' ' } }, { nom: 'B', contenu: { valeur: ' ' } }] })
        if (lib.ecriSeg) scratch.params.scratchProgDonnees.menu[scratch.params.scratchProgDonnees.menu.length - 1].contenu.push({ type: 'ecriSeg', value: [{ nom: 'A', contenu: { valeur: ' ' } }] })
        if (lib.ecriDroite) scratch.params.scratchProgDonnees.menu[scratch.params.scratchProgDonnees.menu.length - 1].contenu.push({ type: 'ecriDroite', value: [{ nom: 'A', contenu: { valeur: ' ' } }, { nom: 'B', contenu: { valeur: ' ' } }] })
        if (lib.ecriDDroite) scratch.params.scratchProgDonnees.menu[scratch.params.scratchProgDonnees.menu.length - 1].contenu.push({ type: 'ecriDDroite', value: [{ nom: 'A', contenu: { valeur: ' ' } }, { nom: 'B', contenu: { valeur: ' ' } }] })
        if (lib.nomAleatoire) scratch.params.scratchProgDonnees.menu[scratch.params.scratchProgDonnees.menu.length - 1].contenu.push({ type: 'NomAleatoire' })
      }
      if (scratch.params.lesBlocPerso.length > 0) {
        scratch.params.scratchProgDonnees.menu.push({ type: 'catégorie', nom: 'MES BLOCS', coul1: '#D14C7D', coul2: '#B8436E', contenu: [] })
        for (let i = 0; i < scratch.params.lesBlocPerso.length; i++) {
          scratch.params.scratchProgDonnees.menu[scratch.params.scratchProgDonnees.menu.length - 1].contenu.push({ type: scratch.params.lesBlocPerso[i] + 'b', value: scratch.params.listeDef[scratch.params.lesBlocPerso[i]].value })
        }
      }

      if (scratch.params.MtgProg.crea) scratch.params.scratchProgDonnees.menu.push({ type: 'catégorie', nom: 'NOUVEAU BLOC', coul1: '#ffffff', coul2: '#030203', custom: 'PROCEDURE', contenu: [] })

      const x = scratch.scratchCreeMenu(scratch.params.scratchProgDonnees.menu, true)
      const xmlDoc = $.parseXML(x)
      let xml = xmlDoc.firstChild
      while (xml.nodeType !== 1) {
        xml = xml.nextSibling
      }
      scratch.params.scratchProgDonnees.workspace.updateToolbox(xml)

      scratch.scratchMenuPerso()
    }

    me.construitStructurePage({ structure: 'presentation3', ratioGauche: 0.6, theme: 'zonesAvecImageDeFond' })
    j3pDetruit(me.zonesElts.HG)
    j3pDetruit(me.zonesElts.HD)

    scratch.cmtglibre = true

    stor.remetHeigth = setInterval(() => {
      if (me.zonesElts.MG.offsetHeight !== 1300) me.zonesElts.MG.style.height = '1300px'
    }, 1000)
    me.stockage = [0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0]

    const progJson = ds.Programme.replace(/'/g, '"')
    // on tente de limiter la casse en ne remplaçant que les ' qui suivent ou précèdent une espace ou :{[,
    // const progJson = ds.Programme.replace(/([\s:{[,])'/g, '$1"').replace(/'([\s:}\]],)/g, '"$1')
    try {
      stor.ProgrammeLT = JSON.parse(progJson)
    } catch (error) {
      console.error(error, 'avec le programme', progJson)
      throw Error('Programme mis en paramètre invalide')
    }

    stor.mtgApp = await getMtgCore({ withApiPromise: true })
    scratch.setMtg(stor.mtgApp)
    enonceMain()
  }

  function enonceMain () {
    const scratch = stor.scratch
    const Blockly = scratch.Blockly
    scratch.params.NomsVariableAutorise = []

    me.videLesZones()
    scratch.params.scratchProgDonnees.lesordres = []

    scratch.params.MtgProg = stor.ProgrammeLT[0]

    // genere list bloc
    scratch.params.scratchProgDonnees.menu = []
    scratch.params.scratchProgDonnees.progdeb = []
    scratch.params.scratchProgDonnees.progdeb.push({ type: 'Drapo', editable: 'false', deletable: 'false', movable: 'false', x: '20', y: '20' })

    const lib = scratch.params.MtgProg.listeBloc
    if (lib.pointLibreAvecNom || lib.pointIntersection || lib.pointSur || lib.pointMilieu || lib.pointLong) {
      scratch.params.scratchProgDonnees.menu.push({ type: 'catégorie', nom: 'POINTS', coul1: '#3230B8', coul2: '#000000', contenu: [] })
      if (lib.pointLibreAvecNom) scratch.params.scratchProgDonnees.menu[scratch.params.scratchProgDonnees.menu.length - 1].contenu.push({ type: 'pointLibreAvecNom', value: [{ nom: 'NOM', contenu: { valeur: 'A' } }] })
      if (lib.pointIntersection) scratch.params.scratchProgDonnees.menu[scratch.params.scratchProgDonnees.menu.length - 1].contenu.push({ type: 'pointIntersection', value: [{ nom: 'NOM', contenu: { valeur: 'A' } }, { nom: 'CONT1', contenu: { valeur: ' ' } }, { nom: 'CONT2', contenu: { valeur: ' ' } }] })
      if (lib.pointSur) scratch.params.scratchProgDonnees.menu[scratch.params.scratchProgDonnees.menu.length - 1].contenu.push({ type: 'pointSur', value: [{ nom: 'NOM', contenu: { valeur: 'A' } }, { nom: 'CONT', contenu: { valeur: ' ' } }] })
      if (lib.pointMilieu) scratch.params.scratchProgDonnees.menu[scratch.params.scratchProgDonnees.menu.length - 1].contenu.push({ type: 'pointMilieu', value: [{ nom: 'NOM', contenu: { valeur: 'I' } }, { nom: 'CONT', contenu: { valeur: 'AB' } }] })
      if (lib.pointLong) scratch.params.scratchProgDonnees.menu[scratch.params.scratchProgDonnees.menu.length - 1].contenu.push({ type: 'pointLong', value: [{ nom: 'EXT1', contenu: { valeur: 'A' } }, { nom: 'P1', contenu: { valeur: ' ' } }, { nom: 'LONG', contenu: { valeur: ' ' } }] })
    }

    if (lib.segmentAB || lib.segmentlongA) {
      scratch.params.scratchProgDonnees.menu.push({ type: 'catégorie', nom: 'SEGMENTS', coul1: '#ff0000', coul2: '#000000', contenu: [] })
      if (lib.segmentAB) scratch.params.scratchProgDonnees.menu[scratch.params.scratchProgDonnees.menu.length - 1].contenu.push({ type: 'segmentAB', value: [{ nom: 'EXT1', contenu: { valeur: 'AB' } }] })
      if (lib.segmentlongA) scratch.params.scratchProgDonnees.menu[scratch.params.scratchProgDonnees.menu.length - 1].contenu.push({ type: 'segmentlongA', value: [{ nom: 'EXT1', contenu: { valeur: 'AB' } }, { nom: 'LONG', contenu: { valeur: '3' } }] })
    }

    if (lib.demidroitept || lib.angleplus || lib.anglemoins) {
      scratch.params.scratchProgDonnees.menu.push({ type: 'catégorie', nom: 'DEMI-DROITES', coul1: '#e11ac3', coul2: '#000000', contenu: [] })
      scratch.params.scratchProgDonnees.menu[scratch.params.scratchProgDonnees.menu.length - 1].contenu.push({ type: 'demidroitept', value: [{ nom: 'ORIGINE', contenu: { valeur: 'AB' } }] })
      if (lib.angleplus) scratch.params.scratchProgDonnees.menu[scratch.params.scratchProgDonnees.menu.length - 1].contenu.push({ type: 'angleplus', value: [{ nom: 'NOM', contenu: { valeur: 'ABC' } }, { nom: 'MESURE', contenu: { valeur: '45' } }] })
      if (lib.anglemoins) scratch.params.scratchProgDonnees.menu[scratch.params.scratchProgDonnees.menu.length - 1].contenu.push({ type: 'anglemoins', value: [{ nom: 'NOM', contenu: { valeur: 'ABC' } }, { nom: 'MESURE', contenu: { valeur: '45' } }] })
    }

    if (lib.droitept || lib.droitepara || lib.droiteperp) {
      scratch.params.scratchProgDonnees.menu.push({ type: 'catégorie', nom: 'DROITES', coul1: '#7900c5', coul2: '#000000', contenu: [] })
      if (lib.droitept) scratch.params.scratchProgDonnees.menu[scratch.params.scratchProgDonnees.menu.length - 1].contenu.push({ type: 'droitept', value: [{ nom: 'POINT1', contenu: { valeur: 'AB' } }] })
      if (lib.droitepara) scratch.params.scratchProgDonnees.menu[scratch.params.scratchProgDonnees.menu.length - 1].contenu.push({ type: 'droitepara', value: [{ nom: 'NOM', contenu: { valeur: '(d1)' } }, { nom: 'PARA', contenu: { valeur: '(AB)' } }, { nom: 'POINT', contenu: { valeur: 'C' } }] })
      if (lib.droiteperp) scratch.params.scratchProgDonnees.menu[scratch.params.scratchProgDonnees.menu.length - 1].contenu.push({ type: 'droiteperp', value: [{ nom: 'NOM', contenu: { valeur: '(d1)' } }, { nom: 'PERP', contenu: { valeur: '(AB)' } }, { nom: 'POINT', contenu: { valeur: 'C' } }] })
    }

    if (lib.cercleCentrePoint || lib.cercleCentreRayon) {
      scratch.params.scratchProgDonnees.menu.push({ type: 'catégorie', nom: 'CERCLES', coul1: '#E55B16', coul2: '#090505', contenu: [] })
      if (lib.cercleCentrePoint) scratch.params.scratchProgDonnees.menu[scratch.params.scratchProgDonnees.menu.length - 1].contenu.push({ type: 'cercleCentrePoint', value: [{ nom: 'CENTRE', contenu: { valeur: 'O' } }, { nom: 'POINT', contenu: { valeur: 'A' } }, { nom: 'NOM', contenu: { valeur: 'C1' } }] })
      if (lib.cercleCentreRayon)scratch.params.scratchProgDonnees.menu[scratch.params.scratchProgDonnees.menu.length - 1].contenu.push({ type: 'cercleCentreRayon', value: [{ nom: 'CENTRE', contenu: { valeur: 'O' } }, { nom: 'RAYON', contenu: { valeur: '3' } }, { nom: 'NOM', contenu: { valeur: 'C1' } }] })
    }

    if (lib.arc1PointPasse || lib.arccentreplus || lib.arccentremoins) {
      scratch.params.scratchProgDonnees.menu.push({ type: 'catégorie', nom: 'ARC DE CERCLES', coul1: '#E57E16', coul2: '#010202', contenu: [] })
      if (lib.arc1PointPasse) scratch.params.scratchProgDonnees.menu[scratch.params.scratchProgDonnees.menu.length - 1].contenu.push({ type: 'arc1PointPasse', value: [{ nom: 'POINT', contenu: { valeur: 'M' } }, { nom: 'NOM', contenu: { valeur: 'BC' } }] })
      if (lib.arccentreplus) scratch.params.scratchProgDonnees.menu[scratch.params.scratchProgDonnees.menu.length - 1].contenu.push({ type: 'arccentreplus', value: [{ nom: 'CENTRE', contenu: { valeur: 'O' } }, { nom: 'NOM', contenu: { valeur: 'AB' } }] })
      if (lib.arccentremoins) scratch.params.scratchProgDonnees.menu[scratch.params.scratchProgDonnees.menu.length - 1].contenu.push({ type: 'arccentremoins', value: [{ nom: 'CENTRE', contenu: { valeur: 'O' } }, { nom: 'NOM', contenu: { valeur: 'AB' } }] })
    }

    if (lib.effacer || lib.renommer || lib.regroupe || lib.ecriSeg || lib.ecriDroite || lib.ecriDDroite || lib.nomAleatoire) {
      scratch.params.scratchProgDonnees.menu.push({ type: 'catégorie', nom: 'UTILITAIRES', coul1: '#B5E61D', coul2: '#3373CC', contenu: [] })
      if (lib.effacer) scratch.params.scratchProgDonnees.menu[scratch.params.scratchProgDonnees.menu.length - 1].contenu.push({ type: 'mtgEffacer', value: [{ nom: 'NOM', contenu: { valeur: ' ' } }] })
      if (lib.pointille) scratch.params.scratchProgDonnees.menu[scratch.params.scratchProgDonnees.menu.length - 1].contenu.push({ type: 'mtgPointille', value: [{ nom: 'NOM', contenu: { valeur: '[AB]' } }] })
      if (lib.renommer) scratch.params.scratchProgDonnees.menu[scratch.params.scratchProgDonnees.menu.length - 1].contenu.push({ type: 'Renommer', value: [{ nom: 'NOM', contenu: { valeur: ' ' } }, { nom: 'NOM2', contenu: { valeur: ' ' } }] })
      if (lib.regroupe) scratch.params.scratchProgDonnees.menu[scratch.params.scratchProgDonnees.menu.length - 1].contenu.push({ type: 'Regroupe', value: [{ nom: 'A', contenu: { valeur: ' ' } }, { nom: 'B', contenu: { valeur: ' ' } }] })
      if (lib.ecriSeg) scratch.params.scratchProgDonnees.menu[scratch.params.scratchProgDonnees.menu.length - 1].contenu.push({ type: 'ecriSeg', value: [{ nom: 'A', contenu: { valeur: ' ' } }] })
      if (lib.ecriDroite) scratch.params.scratchProgDonnees.menu[scratch.params.scratchProgDonnees.menu.length - 1].contenu.push({ type: 'ecriDroite', value: [{ nom: 'A', contenu: { valeur: ' ' } }, { nom: 'B', contenu: { valeur: ' ' } }] })
      if (lib.ecriDDroite) scratch.params.scratchProgDonnees.menu[scratch.params.scratchProgDonnees.menu.length - 1].contenu.push({ type: 'ecriDDroite', value: [{ nom: 'A', contenu: { valeur: ' ' } }, { nom: 'B', contenu: { valeur: ' ' } }] })
      if (lib.nomAleatoire) scratch.params.scratchProgDonnees.menu[scratch.params.scratchProgDonnees.menu.length - 1].contenu.push({ type: 'NomAleatoire' })
    }

    if (scratch.params.MtgProg.blocPerso) {
      const x2 = scratch.scratchCreeMenu([], true)
      const xmlDoc2 = $.parseXML(x2)
      let xml2 = xmlDoc2.firstChild
      while (xml2.nodeType !== 1) {
        xml2 = xml2.nextSibling
      }
      const aVire = addDefaultTable(me.zonesElts.MG, 1, 1)[0][0]
      for (let i = 0; i < scratch.params.MtgProg.blocPerso.length; i++) {
        if (i === 0) {
          scratch.params.scratchProgDonnees.menu.push({ type: 'catégorie', nom: 'MES BLOCS', coul1: '#D14C7D', coul2: '#B8436E', contenu: [] })
        }
        function urlImg (img, onlyRel) {
          if (onlyRel) return '/outils/scratch/' + img
          return j3pBaseUrl + 'outils/scratch/' + img
        }
        const nwBlo = scratch.params.MtgProg.blocPerso[i]
        const obdep = {
          message0: nwBlo.txt,
          args0: [],
          category: Blockly.Categories.motion,
          colour: nwBlo.colour,
          extensions: ['shape_statement']
        }
        const value = []
        let funX = () => ''
        let funY = () => ''
        let funZ = () => ''
        if (nwBlo.nbVar > 0) {
          obdep.args0.push({ type: 'input_value', name: 'X', default: '' })
          funX = (block) => { return Blockly.JavaScript.valueToCode(block, 'X', Blockly.JavaScript.ORDER_ATOMIC).replace(/'/g, '') }
          value.push({ nom: 'X', contenu: { valeur: ' ' } })
        }
        if (nwBlo.nbVar > 1) {
          obdep.args0.push({ type: 'input_value', name: 'Y', default: '' })
          funY = (block) => { return Blockly.JavaScript.valueToCode(block, 'Y', Blockly.JavaScript.ORDER_ATOMIC).replace(/'/g, '') }
          value.push({ nom: 'Y', contenu: { valeur: ' ' } })
        }
        if (nwBlo.nbVar > 2) {
          obdep.args0.push({ type: 'input_value', name: 'Z', default: '' })
          funZ = (block) => { return Blockly.JavaScript.valueToCode(block, 'Z', Blockly.JavaScript.ORDER_ATOMIC).replace(/'/g, '') }
          value.push({ nom: 'Z', contenu: { valeur: ' ' } })
        }
        obdep.message0 = nwBlo.txt
        let yaicone = false
        if (scratch.params.creaIconeChoice) {
          obdep.args0.splice(0, 0, { type: 'field_vertical_separator' })
          obdep.args0.splice(0, 0, {
            type: 'field_image',
            src: urlImg(scratch.params.creaIconeChoice),
            width: 30,
            height: 30
          })
          yaicone = true
        }
        const xml = $.parseXML(nwBlo.xmlText)
        scratch.params.lesBlocPerso.push(nwBlo.nom)
        scratch.params.listeDef[nwBlo.nom] = { nom: nwBlo.nom, nbVar: nwBlo.nbVar, xml, txt: nwBlo.txt, yaicone }
        Blockly.Blocks[nwBlo.nom] = {
          init: function () {
            this.jsonInit({
              type: 'make',
              message0: 'Définir ' + nwBlo.nom,
              inputsInline: false,
              nextStatement: null,
              colour: 330,
              tooltip: '',
              helpUrl: ''
            })
          }
        }
        Blockly.JavaScript[nwBlo.nom] = function () {
          return { type: 'make', eventtype: 'make', nom: nwBlo.nom }
        }
        Blockly.Blocks[nwBlo.nom + 'b'] = {
          init: function () {
            this.jsonInit(obdep)
          }
        }
        Blockly.JavaScript[nwBlo.nom + 'b'] = function (block) {
          const retValx = function () {
            return funX(block)
          }
          const retvaly = function () {
            return funY(block)
          }
          const retvalz = function () {
            return funZ(block)
          }
          return { type: 'Mb', nom: nwBlo.nom + 'b', valx: retValx(), valy: retvaly(), valz: retvalz() }
        }
        scratch.params.scratchProgDonnees.menu[scratch.params.scratchProgDonnees.menu.length - 1].contenu.push({ type: nwBlo.nom + 'b', value })

        const side = 'start'
        scratch.params.scratchProgDonnees.workspaceDef = Blockly.inject(aVire, {
          comments: true,
          disable: false,
          collapse: false,
          media: j3pBaseUrl + 'externals/blockly/media/',
          readOnly: false,
          rtl: null,
          scrollbars: true,
          toolbox: xml2,
          toolboxPosition: side === 'top' || side === 'start' ? 'start' : 'end',
          horizontalLayout: side === 'top' || side === 'bottom',
          sounds: false,
          zoom: {
            controls: true,
            wheel: true,
            startScale: 0.6,
            maxScale: 2,
            minScale: 0.25,
            scaleSpeed: 1.1
          },
          colours: {
            fieldShadow: 'rgba(100, 100, 100, 0.3)',
            dragShadowOpacity: 0.6
          }
        })
        Blockly.svgResize(scratch.params.scratchProgDonnees.workspaceDef)
        // scratch.params.scratchProgDonnees.workspaceDef.clear()
        let x = xml.firstChild
        while (x.nodeType !== 1) {
          x = x.nextSibling
        }
        Blockly.Xml.domToWorkspace(x, scratch.params.scratchProgDonnees.workspaceDef)
        const ladef = Blockly.JavaScript.workspaceToCode(scratch.params.scratchProgDonnees.workspaceDef)
        ladef[0].splice(0, 1)
        scratch.params.listeDef[nwBlo.nom].def = ladef[0]
        j3pEmpty(aVire)
      }
      j3pDetruit(aVire)
    }

    if (scratch.params.MtgProg.crea) scratch.params.scratchProgDonnees.menu.push({ type: 'catégorie', nom: 'NOUVEAU BLOC', coul1: '#ffffff', coul2: '#030203', custom: 'PROCEDURE', contenu: [] })

    scratch.resetMtg()
    scratch.enonce(null, 'libre')
    if (window.top !== window) {
      parent.postMessage('BesoinDuInteger', '*')
      window.addEventListener('message', (event) => {
        function ajouteMenu (obj) {
          let indiceMesBloc = scratch.params.scratchProgDonnees.menu.length
          for (let i = 0; i < scratch.params.scratchProgDonnees.menu.length; i++) {
            if (scratch.params.scratchProgDonnees.menu[i].nom === obj.cat) indiceMesBloc = i
          }
          if (indiceMesBloc === scratch.params.scratchProgDonnees.menu.length) {
            scratch.params.scratchProgDonnees.menu.push({ type: 'catégorie', nom: obj.cat, coul1: obj.coul1, coul2: obj.coul2, contenu: [] })
          }
          if (!scratch.params.scratchProgDonnees.menu[indiceMesBloc].contenu.includes({ type: obj.type, value: obj.value })) scratch.params.scratchProgDonnees.menu[indiceMesBloc].contenu.push({ type: obj.type, value: obj.value })
        }
        if (event.data.act === 'donneInteger') {
          const strReverse = event.data.there
          let ok = false
          // cercle diam 1
          if (strReverse[0] === '1') {
            ajouteMenu({ cat: 'CERCLES', coul1: '#E55B16', coul2: '#090505', type: 'cercleDiametre', value: [{ nom: 'NOM', contenu: { valeur: 'C1' } }, { nom: 'DIAM', contenu: { valeur: 'AB' } }] })
            ok = true
          }
          // point alignés
          if (strReverse[1] === '1') {
            ajouteMenu({ cat: 'POINTS', coul1: '#3230B8', coul2: '#000000', type: 'pointAligne', value: [{ nom: 'NOM', contenu: { valeur: 'C' } }, { nom: 'P1', contenu: { valeur: 'A' } }, { nom: 'P2', contenu: { valeur: 'B' } }] })
            ok = true
          }
          // mediatrice
          if (strReverse[2] === '1') {
            ajouteMenu({ cat: 'DROITES', coul1: '#7900c5', coul2: '#000000', type: 'mediatrice', value: [{ nom: 'NOM', contenu: { valeur: 'd1' } }, { nom: 'CONT', contenu: { valeur: 'AB' } }] })
            ok = true
          }
          // triangle quelconque
          if (strReverse[3] === '1') {
            ajouteMenu({ cat: 'POLYGONES', coul1: '#0027c5', coul2: '#000000', type: 'triangleQuelconque', value: [{ nom: 'NOM', contenu: { valeur: 'ABC' } }] })
            ok = true
          }
          // triangle equilat
          if (strReverse[4] === '1') {
            ajouteMenu({ cat: 'POLYGONES', coul1: '#0027c5', coul2: '#000000', type: 'triangleEquilateral', value: [{ nom: 'NOM', contenu: { valeur: 'ABC' } }] })
            ok = true
          }

          if (!ok) return
          const x2 = scratch.scratchCreeMenu(scratch.params.scratchProgDonnees.menu, true)
          const xmlDoc2 = $.parseXML(x2)
          let xml2 = xmlDoc2.firstChild
          while (xml2.nodeType !== 1) {
            xml2 = xml2.nextSibling
          }
          scratch.params.scratchProgDonnees.workspace.updateToolbox(xml2)
          scratch.scratchMenuPerso()
          /// //
        }
      })
    }
    scratch.params.isMtgLibre = true

    me.finEnonce()
    me.cacheBoutonValider()
  }

  switch (this.etat) {
    case 'enonce':
      if (this.debutDeLaSection) {
        initSection()
      } else {
        enonceMain()
      }
      break // case "enonce":

    case 'correction':
      return

    case 'navigation':
      if (this.sectionTerminee()) {
        stor.scratch.lesdivs.BoutonsSuite.appendChild(this.buttonsElts.sectionSuivante)
      } else {
        this.etat = 'enonce'
        stor.scratch.lesdivs.BoutonsSuite.appendChild(this.buttonsElts.suite)
      }
      // Obligatoire
      this.finNavigation()
      break // case "navigation":
  }
}
