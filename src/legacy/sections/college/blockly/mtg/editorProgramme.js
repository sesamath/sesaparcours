import EditorProgrammeMtg from 'src/legacy/outils/paramEditors/EditorProgrammeMtg'

import { j3pAddContent, j3pAddElt, j3pAjouteBouton, j3pDetruit, j3pEmpty } from 'src/legacy/core/functions'
import { getNewScratchMathgraph } from 'src/legacy/outils/scratch/loaders'
import { j3pModale2 } from 'src/legacy/outils/zoneStyleMathquill/functions'
import loadMq from 'src/lib/mathquill/loadMathquill'
import { getMtgCore } from 'src/lib/outils/mathgraph'
import getDefaultProgramme from './defaultProgramme'

export default function editorProgramme (container, values) {
  console.debug('editorProgramme avec les valeurs initiales', values)

  return new Promise((resolve, reject) => {
    // on ajoute notre éditeur dans container
    const btnsContainer = j3pAddElt(container, 'div', 'Chargement en cours…', {
      style: {
        margin: '1rem auto',
        textAlign: 'center'
      }
    })
    let editor

    function verifYaPasExo () {
      if (editor.yaBlok || editor.stor.yaBlok) {
        const yy = j3pModale2({ titre: 'Avertissement ', contenu: 'Un exercice est en cours\n de modification.', divparent: container })
        j3pAddContent(yy, '\n')
        j3pAjouteBouton(yy, () => {
          j3pDetruit(yy)
          j3pDetruit(yy.masque)
        }, { value: 'Annuler' })
        j3pAddContent(yy, '\n')
        j3pAddContent(yy, '\n')
        j3pAjouteBouton(yy, () => {
          j3pDetruit(yy)
          j3pDetruit(yy.masque)
          onSaveParams()
        }, { value: 'Tant pis' })
        return
      }
      onSaveParams()
    }

    function onSaveParams () {
      if (document.fullscreenElement) document.exitFullscreen()
      // on affecte nos nouvelles valeurs (à récupérer dans l’éditeur mis ci-dessus)
      clearInterval(editor.intervalId)
      resolve({
        Parametres: JSON.stringify({
          // faut mettre du json valide pour ce param
          Programme: editor.stor.Programme,
          // on peut aussi affecter d’autres paramètres (ça fonctionne pas encore pour les boolean ni les listes)
          // ca c’est trop cool :)
          Sequence: editor.stor.ParamSequence.reponse === 'Oui',
          PasAPas: editor.stor.ParamPasAPas.reponse === 'Oui',
          Difficulte: editor.stor.ParamDifficul.reponse
        }),
        nbchances: (editor.stor.listeparamChance.reponse === 'Illimité') ? 0 : Number(editor.stor.inputNbChances.value),
        nbrepetitions: Number(editor.stor.ParamnbExos.value),
        limite: (editor.stor.listeparamLimite.reponse === 'Non') ? 0 : Number(editor.stor.inputLimite.value)
      })
    }

    getNewScratchMathgraph()
      .then(scratch => {
        container.parentNode.style.opacity = 1
        editor = new EditorProgrammeMtg({ container, getDefaultProgramme, values, scratch })
        return loadMq()
      })
      .then(() => import('src/legacy/outils/scratch/defaultFigure.js'))
      .then(({ default: figure }) => {
        editor.scratch.setFigure(figure)
        return getMtgCore({ withMathJax: true, withApiPromise: true })
      })
      .then(mtgAppLecteur => {
        editor.scratch.setMtg(mtgAppLecteur)
        editor.stor.listePourCalc = mtgAppLecteur.createList('TWF0aEdyYXBoSmF2YTEuMAAAABI+TMzNAAJmcv###wEA#wEAAAAAAAAAAAQsAAACvQAAAQEAAAAAAAAAAQAAAAP#####AAAAAQAKQ0NhbGNDb25zdAD#####AAJwaQAWMy4xNDE1OTI2NTM1ODk3OTMyMzg0Nv####8AAAABAApDQ29uc3RhbnRlQAkh+1RELRj#####AAAAAQAHQ0NhbGN1bAD#####AAFlAAZleHAoMSn#####AAAAAgAJQ0ZvbmN0aW9uBwAAAAE#8AAAAAAAAAAAAAIA#####wABeAABMAAAAAEAAAAAAAAAAP###############w==')
        editor.mtgAppLecteur2 = mtgAppLecteur
        // on vire le message chargement en cours
        j3pEmpty(btnsContainer)

        // Important, les boutons annuler / valider
        const btnProps = { style: { margin: '0.2rem', fontSize: '120%' } }
        j3pAddElt(btnsContainer, 'button', 'Annuler', btnProps)
          .addEventListener('click', () => {
            clearInterval(editor.intervalId)
            if (document.fullscreenElement) document.exitFullscreen()
            resolve()
          })
        j3pAddElt(btnsContainer, 'button', 'Valider', btnProps)
          .addEventListener('click', verifYaPasExo)

        editor.creeLesDiv()
        editor.menuTotal()
      })
      .catch(reject)
  }) // promesse retournée
}
