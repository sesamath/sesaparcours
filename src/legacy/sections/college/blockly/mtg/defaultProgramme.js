export default function getDefaultProgramme () {
  return {
    Sequence: true,
    PasAPas: false,
    Difficulte: 'Progressive',
    Programme: [
      getDefaultProgrammeElement()
    ]
  }
}

/**
 * Retourne un programme scratch avec ses propriétés par défaut
 * @returns {Object}
 */
function getDefaultProgrammeElement () {
  return {
    figdep: [],
    suite: [],
    placeDep: true,
    listeBloc: {
      pointLibreAvecNom: true,
      pointIntersection: true,
      pointSur: true,
      pointMilieu: true,
      segmentAB: true,
      segmentlongA: true,
      droitept: true,
      droitepara: true,
      droiteperp: true,
      demidroitept: true,
      cercleCentrePoint: true,
      cercleCentreRayon: true,
      arc1PointPasse: true,
      arccentreplus: true,
      arccentremoins: true,
      effacer: true,
      renommer: true,
      regroupe: true,
      ecriSeg: true,
      ecriDroite: true,
      ecriDDroite: true,
      nomAleatoire: true
    },
    crea: false,
    niv: 0,
    nom: 'Nouvel exercice',
    blocPerso: []
  }
}
