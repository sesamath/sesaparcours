import $ from 'jquery'

import { j3pAjouteBouton, j3pClone, j3pDetruit, j3pEmpty, j3pGetRandomInt, j3pShowError, j3pShuffle } from 'src/legacy/core/functions'
import { getNewScratchMathgraph } from 'src/legacy/outils/scratch/loaders'
import { addDefaultTable } from 'src/legacy/themes/table'
import { j3pBaseUrl } from 'src/lib/core/constantes'
import { getMtgCore } from 'src/lib/outils/mathgraph'
import Dialog from 'src/lib/widgets/dialog/Dialog'
import loadJqueryDialog from 'src/lib/core/loadJqueryDialog'

import getDefaultProgrammeBlokmtg01 from './defaultProgrammeBlokmtg01'

async function initEditor (container, values) {
  const { default: editor } = await import('./editorProgramme.js')
  return editor(container, values) // la fct qui prend un conteneur et les params comme arguments
}

export const params = {
  outils: ['mathquill'],
  /**
   * Ce qui est paramétrable dans l’éditeur de graphe
   * @type {LegacySectionParamsParametres}
   */
  parametres: [
    ['Parametres', JSON.stringify(getDefaultProgrammeBlokmtg01()), 'multiEditor', 'Json figure de base et figure attendue.', initEditor],
    ['nbrepetitions', 4, 'entier', 'Nombre de répétitions de la section'],
    ['nbchances', 2, 'entier', 'Nombre d’essais possibles (0  signifie illimité)'],
    ['limite', 0, 'entier', 'Temps disponible par répétition, en secondes (0  signifie illimité)']
  ]
}

const sortOnNivProp = (a, b) => b.niv - a.niv

/**
 * section blokmtg01
 * @this {Parcours}
 */
export default function main () {
  const me = this
  const stor = me.storage
  const ds = me.donneesSection

  function aidefich () {
    if (!stor.dialog) return console.error(Error('Pas de dialog disponible pour l’afficher'))
    stor.dialog.toggle()
  }

  function aidefichbase () {
    if (!stor.dialogbase) return console.error(Error('Pas de dialog disponible pour l’afficher'))
    stor.dialogbase.toggle()
  }

  function dist (p, q) {
    return Math.sqrt((p.x - q.x) * (p.x - q.x) + (p.y - q.y) * (p.y - q.y))
  }

  function aideMaFig () {
    const scratch = stor.scratch
    if (!scratch.params.MtgSortieD) return console.error(Error('Pas de dialog disponible pour l’afficher'))
    scratch.params.MtgSortieD.toggle()
    scratch.params.MtgBoutonFerm.style.display = ''
  }

  function verifYa (ob) {
    const scratch = stor.scratch
    if (ob.need === false) return { ok: true, mess: '' }
    const newob = scratch.scratchMtgReplaceLettres(ob)
    let okbuf = false
    let iagarde = -1
    for (let j = 0; j < newob.noms.length; j++) {
      for (let i = 0; i < stor.result.length; i++) {
        if (stor.result[i].noms.indexOf(newob.noms[j]) !== -1) {
          iagarde = i
          okbuf = true
          break
        }
        if (newob.type.indexOf('arc') !== -1) {
          if (stor.result[i].noms.indexOf('arc ' + newob.noms[j]) !== -1) {
            iagarde = i
            okbuf = true
            break
          }
        }
        if (okbuf) break
      }
    }
    if (!okbuf) {
      // test en plus pour droite sans nom / demi droite / arc passant m
      let sssaute = false
      if (newob.type === 'demidroitept') {
        let an = newob.noms[0].replace('[', '').replace(']', '').replace('(', '').replace(')', '')
        an = scratch.scratchDecomposeEnPoints(an)
        let ext1 = an[0]
        let ext2 = an[0]
        if (newob.noms[0][0] === '(') {
          const u = ext1
          ext1 = ext2
          ext2 = u
        }
        const de3 = scratch.params.mtgApplecteur.getPointPosition({ absCoord: true, a: scratch.scratchMtgRetrouveFin({ nom: ext1 }, 'tag') })
        const de4 = scratch.params.mtgApplecteur.getPointPosition({ absCoord: true, a: scratch.scratchMtgRetrouveFin({ nom: ext2 }, 'tag') })
        for (let i = 0; i < scratch.params.donnesMtg.length; i++) {
          if (scratch.params.donnesMtg[i].type === 'demidroite') {
            const de1 = scratch.params.mtgApplecteur.getPointPosition({ absCoord: true, a: scratch.scratchMtgRetrouveFin({ nom: scratch.params.donnesMtg[i].origine }, 'tag') })
            const de2 = scratch.params.mtgApplecteur.getPointPosition({ absCoord: true, a: scratch.scratchMtgRetrouveFin({ nom: scratch.params.donnesMtg[i].point }, 'tag') })
            if (dist(de1, de3) < 0.0001) {
              const v1 = { x: de2.x - de1.x, y: de2.y - de1.y }
              const v2 = { x: de4.x - de1.x, y: de4.y - de1.y }
              const pv = v1.x * v2.y - v2.x * v1.y
              if (Math.abs(pv) < 0.0001) {
                const signe1 = { x: v1.x > 0, y: v1.y > 0 }
                const signe2 = { x: v2.x > 0, y: v2.y > 0 }
                if (v1.x !== 0) {
                  sssaute = signe1.x === signe2.x
                } else {
                  sssaute = signe1.y === signe2.y
                }
              }
            }
          }
        }
      }
      if (!sssaute) {
        return {
          ok: false,
          mess: 'Je ne trouve pas ' + lela(newob.type) + ' ' + getType(newob.type) + ' ' + newob.noms[0] + '<br>(<i>regarde la figure modèle</i>)'
        }
      }
    } else {
      if (getType(newob.type) !== getType(stor.result[iagarde].type)) {
        return {
          ok: false,
          mess: newob.noms[0] + ' n’est pas défini correctement. <br> C’est ' + getArticle(newob.type) + ' ' + getType(newob.type)
        }
      }
    }
    try {
      if (ob.pointille) {
        if (!stor.result[iagarde]?.pointille) {
          return { ok: false, mess: newob.noms[0] + ' doit être en pointillés.' }
        }
      } else {
        if (stor.result[iagarde]?.pointille) {
          return { ok: false, mess: newob.noms[0] + ' ne doit pas être en pointillés.' }
        }
      }
    } catch (e) {
      console.warn(`Encore un problème avec les variables de la section : ob = ${JSON.stringufy(ob)}, stor.result = ${JSON.stringify(stor.result)}`)
    }
    // verif bonne def
    return scratch.verifCestBien(newob)
  }

  function lela (s) {
    switch (s) {
      case 'point':
      case 'pointMilieu':
      case 'pointLong':
      case 'pointSur':
      case 'pointIntersection':
      case 'pointLibreAvecNom':
      case 'segmentlongA':
      case 'segment':
      case 'cercleCentrePoint':
      case 'cercleCentreRayon':
      case 'cercle':
        return 'le'
      case 'droitept':
      case 'droitepara':
      case 'droiteperp':
      case 'demidroitept':
      case 'demidroite':
        return 'la'
      case 'arc1PointPasse':
      case 'arccentreplus':
      case 'arccentremoins':
        return "l'"
    }
  }

  function getType (s) {
    switch (s) {
      case 'point':
      case 'pointMilieu':
      case 'pointLong':
      case 'pointSur':
      case 'pointIntersection':
      case 'pointLibreAvecNom':
        return 'point'

      case 'segmentlongA':
      case 'segment':
        return 'segment'

      case 'cercleCentrePoint':
      case 'cercleCentreRayon':
      case 'cercle':
        return 'cercle'

      case 'droite':
      case 'droitept':
      case 'droitepara':
      case 'droiteperp':
        return 'droite'

      case 'demidroitept':
      case 'demidroite':
        return 'demi-droite'

      case 'arc':
      case 'arc1PointPasse':
      case 'arccentreplus':
      case 'arccentremoins':
        return 'arc'

      default:
        console.error(Error(`Type ${s} inconnu`))
    }
  }

  function getArticle (s) {
    switch (s) {
      case 'point':
      case 'pointMilieu':
      case 'pointLong':
      case 'pointSur':
      case 'pointIntersection':
      case 'pointLibreAvecNom':
      case 'segmentlongA':
      case 'segment':
      case 'cercleCentrePoint':
      case 'cercleCentreRayon':
      case 'arc1PointPasse':
      case 'arccentreplus':
      case 'arccentremoins':
      case 'cercle':
        return 'un'

      default:
        return 'une'
    }
  }

  function conserveCoordsPointLibre () {
    const scratch = stor.scratch
    stor.pointsAgarde = []
    stor.pointsAgarde2 = []
    for (let i = 0; i < scratch.params.donnesMtg.length; i++) {
      const don = scratch.params.donnesMtg[i]
      if (don.type === 'point' && don.classe === 'l') {
        scratch.params.mtgApplecteur.setApiDoc(scratch.params.svgId2)
        const de2 = scratch.params.mtgApplecteur.getPointPosition({ absCoord: true, a: don.tag })
        stor.pointsAgarde2.push({ ki: don.tag, place: de2 })
        scratch.params.mtgApplecteur.setApiDoc(scratch.params.svgId)
        const de1 = scratch.params.mtgApplecteur.getPointPosition({ absCoord: true, a: don.tag })
        stor.pointsAgarde.push({ ki: don.tag, place: de1 })
      }
    }
  }

  function bougePointsLibres () {
    const scratch = stor.scratch
    for (let i = 0; i < scratch.params.donnesMtg.length; i++) {
      const don = scratch.params.donnesMtg[i]
      if (don.type === 'point' && don.classe === 'l') {
        scratch.params.mtgApplecteur.setApiDoc(scratch.params.svgId)
        const de1 = scratch.params.mtgApplecteur.getPointPosition({ absCoord: true, a: don.tag })
        let dx = j3pGetRandomInt(-6, 6)
        if (dx === 0) dx = 6
        let dy = j3pGetRandomInt(-6, 6)
        if (dy === 0) dy = 6
        scratch.params.mtgApplecteur.setApiDoc(scratch.params.svgId)

        scratch.params.mtgApplecteur.setPointPosition(scratch.params.svgId, don.tag, de1.x + dx, de1.y + dy, true)
        scratch.params.mtgApplecteur.setPointPosition(scratch.params.svgId2, don.tag, de1.x + dx, de1.y + dy, true)
      }
    }
    scratch.params.mtgApplecteur.calculate(scratch.params.svgId)
    scratch.params.mtgApplecteur.calculate(scratch.params.svgId2)
  }

  function remetLesConserve () {
    const scratch = stor.scratch
    for (let i = 0; i < stor.pointsAgarde.length; i++) {
      const ag = stor.pointsAgarde[i]
      scratch.params.mtgApplecteur.setPointPosition(scratch.params.svgId, ag.ki, ag.place.x, ag.place.y, true)
      const ag2 = stor.pointsAgarde2[i]
      scratch.params.mtgApplecteur.setPointPosition(scratch.params.svgId2, ag2.ki, ag2.place.x, ag2.place.y, true)
    }
    scratch.params.mtgApplecteur.calculate(scratch.params.svgId)
    scratch.params.mtgApplecteur.calculate(scratch.params.svgId2)
  }

  function compteElemViz (tab) {
    let cmpt = 0
    for (let i = 0; i < tab.length; i++) {
      if (!tab[i].efface) cmpt++
    }
    return cmpt
  }

  async function initSection () {
    // faut s’assurer que jQuery-ui est chargé
    await loadJqueryDialog()
    // let callParent = () => undefined
    const scratch = await getNewScratchMathgraph()
    stor.scratch = scratch
    const { default: figure } = await import('src/legacy/outils/scratch/defaultFigure.js')
    scratch.setFigure(figure)
    stor.dejafait2 = []
    stor.dejafait = []
    me.validOnEnter = false // ex donneesSection.touche_entree

    let prog
    if (typeof ds.Parametres === 'object' && ds.Parametres !== null) {
      // @todo faut vérifier qu’il a le bon format !
      prog = ds.Parametres
    } else if (typeof ds.Parametres === 'string') {
      try {
        prog = JSON.parse(ds.Parametres)
      } catch (error) {
        console.error(error)
        j3pShowError('Programme mis en paramètre invalide')
        prog = getDefaultProgrammeBlokmtg01()
      }
    } else {
      console.error(Error(`type de Programme invalide ${typeof ds.Parametres}`))
      prog = getDefaultProgrammeBlokmtg01()
    }

    ds.Sequence = prog.Sequence
    ds.PasAPas = prog.PasAPas
    ds.Difficulte = prog.Difficulte
    ds.ProgrammeLT = prog.Programme

    scratch.initParams(me)

    scratch.testFlag = function () {
    }
    scratch.testFlagFin = function () {
      const aret = scratch.params.MtgProg.figdep.length - scratch.params.lesMArque.length
      const att = j3pClone(scratch.params.MtgProg.suite)
      for (let i = 0; i < att.length; i++) {
        if (att[i].type === 'mtgEffacer') {
          for (let j = 0; j < att.length; j++) {
            if (att[j].noms.includes(att[i].noms[0])) {
              att[j].need = false
            }
          }
        }
        if (att[i].type === 'mtgPointille') {
          att[i].need = false
          for (let j = 0; j < att.length; j++) {
            if (att[j].noms.includes(att[i].noms[0])) {
              att[j].pointille = true
            }
          }
        }
      }
      if (scratch.params.MtgProg.FoEfCo) {
        const mm = compteElemViz(scratch.params.donnesMtg)
        const mm2 = compteElemViz(scratch.params.donnesMtg2)
        if (mm > mm2) return { ok: false, mess: 'Tu dois effacer les <i><b>traits de construction</b></i>.' }
      }
      stor.result = j3pClone(scratch.params.donnesMtg)
      stor.result.splice(0, aret)
      let ok = true
      let mess = ''
      conserveCoordsPointLibre()
      bougePointsLibres()
      scratch.faitSuitSpe()
      for (let i = 0; i < att.length; i++) {
        const retour = verifYa(att[i])
        if (!retour.ok) {
          mess += retour.mess + '<br>'
          ok = false
          break
        }
      }
      remetLesConserve()
      scratch.faitSuitSpe()
      return { ok, mess }
    }

    me.construitStructurePage({ structure: 'presentation3', ratioGauche: 0.6, theme: 'zonesAvecImageDeFond' })
    me.stockage = [0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0]

    me.afficheTitre('Ecrire un programme de construction')

    stor.ProgrammeLTChoice = j3pShuffle(ds.ProgrammeLT)
    stor.ProgrammeLTFF = []
    let nivVu = []
    for (let i = 0; i < stor.ProgrammeLTChoice.length; i++) {
      const niniv = stor.ProgrammeLTChoice[i].niv
      if (nivVu.indexOf(niniv) === -1) {
        nivVu.push(niniv)
      }
    }
    if (ds.Difficulte === 'Progressive' && ds.nbitems <= nivVu.length) {
      nivVu = []
      for (let i = stor.ProgrammeLTChoice.length - 1; i > -1; i--) {
        const niniv = stor.ProgrammeLTChoice[i].niv
        if (nivVu.indexOf(niniv) === -1) {
          nivVu.push(niniv)
          if (stor.ProgrammeLTFF.length < ds.nbitems) stor.ProgrammeLTFF.push(j3pClone(stor.ProgrammeLTChoice[i]))
        }
      }
    } else {
      for (let i = 0; i < ds.nbitems; i++) {
        if (stor.ProgrammeLTChoice.length === 0) stor.ProgrammeLTChoice = j3pShuffle(ds.ProgrammeLT)
        stor.ProgrammeLTFF.push(stor.ProgrammeLTChoice.pop())
      }
    }
    if (ds.Difficulte === 'Progressive') stor.ProgrammeLTFF.sort(sortOnNivProp)

    stor.mtgApp = await getMtgCore({ withApiPromise: true })
    scratch.setMtg(stor.mtgApp)
    enonceMain()
  }

  function enonceMain () {
    const scratch = stor.scratch
    const Blockly = scratch.Blockly
    scratch.params.NomsVariableAutorise = []

    if (stor.dialog) {
      stor.dialog.destroy()
      stor.dialog = null
    }
    if (stor.dialogbase) {
      stor.dialogbase.destroy()
      stor.dialogbase = null
    }
    if (stor.dialogFig) {
      stor.dialogFig.destroy()
      stor.dialogFig = null
    }
    me.videLesZones()
    scratch.params.scratchProgDonnees.lesordres = []

    stor.dialog = new Dialog({
      title: 'Modèle à reproduire',
      showText: true,
      width: '430px',
      height: 500,
      position: { my: 'right bottom', at: 'right bottom', of: window }
    })
    stor.dialog.container.classList.add('divModele')
    const kkjjj = addDefaultTable(stor.dialog.container, 3, 1)
    stor.dialog.container.style.background = '#000'
    kkjjj[1][0].style.textAlign = 'center'
    kkjjj[0][0].style.textAlign = 'center'
    kkjjj[0][0].style.height = '40px'
    kkjjj[0][0].style.padding = '0px'
    j3pAjouteBouton(kkjjj[0][0], function () {
      stor.dialog.toggle()
    }, { value: 'Fermer', className: 'boutonFermerModele' })
    scratch.params.Model = kkjjj[2][0]
    scratch.params.ModelText = kkjjj[1][0]
    setTimeout(() => {
      stor.dialog.show()
    }, 4500)
    stor.dialogbase = new Dialog({
      title: 'Figure de départ',
      showText: true,
      width: '420px',
      height: 495,
      position: { my: 'left bottom', at: 'left bottom', of: window }
    })
    stor.dialogbase.container.classList.add('divFigureDepart')
    const kkjj = addDefaultTable(stor.dialogbase.container, 2, 1)
    stor.dialogbase.container.style.background = '#000'
    kkjj[0][0].style.textAlign = 'center'
    kkjj[0][0].style.height = '40px'
    kkjj[0][0].style.padding = '0px'
    j3pAjouteBouton(kkjj[0][0], function () {
      stor.dialogbase.toggle()
    }, { value: 'Fermer', className: 'boutonFermerFigureDepart' })
    scratch.params.Base = kkjj[1][0]
    setTimeout(() => {
      stor.dialogbase.show()
    }, 3500)

    stor.dialogFig = new Dialog({
      title: 'Mon Programme',
      width: '425px',
      height: 540
    })
    const kkjj3 = addDefaultTable(stor.dialogFig.container, 2, 1)
    stor.dialogFig.container.style.background = '#000'
    kkjj3[0][0].style.textAlign = 'center'
    kkjj3[0][0].style.height = '40px'
    kkjj3[0][0].style.padding = '0px'
    kkjj3[1][0].style.padding = '0px'
    kkjj3[1][0].style.background = '#FFF'
    scratch.params.MtgSortie = kkjj3[1][0]
    scratch.params.MtgSortieD = stor.dialogFig
    scratch.params.MtgBoutonFerm = j3pAjouteBouton(kkjj3[0][0], () => {
      stor.dialogFig.toggle()
    }, { value: 'Fermer' })
    scratch.params.MtgBoutonFerm.classList.add('boutonFermerMonProgramme')
    scratch.params.MtgProg = stor.ProgrammeLTFF.pop()
    scratch.params.MtgSortieD.show2 = () => {
      scratch.params.MtgSortieD.show()
      scratch.params.MtgBoutonFerm.style.display = 'none'
    }

    // genere list bloc
    scratch.params.scratchProgDonnees.menu = []
    scratch.params.scratchProgDonnees.progdeb = []
    scratch.params.scratchProgDonnees.progdeb.push({
      type: 'Drapo',
      editable: 'false',
      deletable: 'false',
      movable: 'false',
      x: '20',
      y: '20'
    })

    const lib = scratch.params.MtgProg.listeBloc
    if (lib.pointLibreAvecNom || lib.pointIntersection || lib.pointSur || lib.pointMilieu || lib.pointLong) {
      scratch.params.scratchProgDonnees.menu.push({
        type: 'catégorie',
        nom: 'POINTS',
        coul1: '#3230B8',
        coul2: '#000000',
        contenu: []
      })
      if (lib.pointLibreAvecNom) {
        scratch.params.scratchProgDonnees.menu[scratch.params.scratchProgDonnees.menu.length - 1].contenu.push({
          type: 'pointLibreAvecNom',
          value: [{ nom: 'NOM', contenu: { valeur: 'A' } }]
        })
      }
      if (lib.pointIntersection) {
        scratch.params.scratchProgDonnees.menu[scratch.params.scratchProgDonnees.menu.length - 1].contenu.push({
          type: 'pointIntersection',
          value: [{ nom: 'NOM', contenu: { valeur: 'A' } }, { nom: 'CONT1', contenu: { valeur: ' ' } }, {
            nom: 'CONT2',
            contenu: { valeur: ' ' }
          }]
        })
      }
      if (lib.pointSur) {
        scratch.params.scratchProgDonnees.menu[scratch.params.scratchProgDonnees.menu.length - 1].contenu.push({
          type: 'pointSur',
          value: [{ nom: 'NOM', contenu: { valeur: 'A' } }, { nom: 'CONT', contenu: { valeur: ' ' } }]
        })
      }
      if (lib.pointMilieu) {
        scratch.params.scratchProgDonnees.menu[scratch.params.scratchProgDonnees.menu.length - 1].contenu.push({
          type: 'pointMilieu',
          value: [{ nom: 'NOM', contenu: { valeur: 'I' } }, { nom: 'CONT', contenu: { valeur: 'AB' } }]
        })
      }
      if (lib.pointLong) {
        scratch.params.scratchProgDonnees.menu[scratch.params.scratchProgDonnees.menu.length - 1].contenu.push({
          type: 'pointLong',
          value: [{ nom: 'EXT1', contenu: { valeur: 'A' } }, { nom: 'LONG', contenu: { valeur: ' ' } }, { nom: 'P1', contenu: { valeur: ' ' } }]
        })
      }
    }

    if (lib.segmentAB || lib.segmentlongA) {
      scratch.params.scratchProgDonnees.menu.push({
        type: 'catégorie',
        nom: 'SEGMENTS',
        coul1: '#ff0000',
        coul2: '#000000',
        contenu: []
      })
      if (lib.segmentAB) {
        scratch.params.scratchProgDonnees.menu[scratch.params.scratchProgDonnees.menu.length - 1].contenu.push({
          type: 'segmentAB',
          value: [{ nom: 'EXT1', contenu: { valeur: 'AB' } }]
        })
      }
      if (lib.segmentlongA) {
        scratch.params.scratchProgDonnees.menu[scratch.params.scratchProgDonnees.menu.length - 1].contenu.push({
          type: 'segmentlongA',
          value: [{ nom: 'EXT1', contenu: { valeur: 'AB' } }, { nom: 'LONG', contenu: { valeur: ' ' } }]
        })
      }
    }

    if (lib.demidroitept || lib.angleplus || lib.anglemoins) {
      scratch.params.scratchProgDonnees.menu.push({
        type: 'catégorie',
        nom: 'DEMI-DROITES',
        coul1: '#e11ac3',
        coul2: '#000000',
        contenu: []
      })
      scratch.params.scratchProgDonnees.menu[scratch.params.scratchProgDonnees.menu.length - 1].contenu.push({
        type: 'demidroitept',
        value: [{ nom: 'ORIGINE', contenu: { valeur: 'AB' } }]
      })
      if (lib.angleplus) {
        scratch.params.scratchProgDonnees.menu[scratch.params.scratchProgDonnees.menu.length - 1].contenu.push({
          type: 'angleplus',
          value: [{ nom: 'NOM', contenu: { valeur: 'ABC' } }, { nom: 'MESURE', contenu: { valeur: '45' } }]
        })
      }
      if (lib.anglemoins) {
        scratch.params.scratchProgDonnees.menu[scratch.params.scratchProgDonnees.menu.length - 1].contenu.push({
          type: 'anglemoins',
          value: [{ nom: 'NOM', contenu: { valeur: 'ABC' } }, { nom: 'MESURE', contenu: { valeur: '45' } }]
        })
      }
    }

    if (lib.droitept || lib.droitepara || lib.droiteperp) {
      scratch.params.scratchProgDonnees.menu.push({
        type: 'catégorie',
        nom: 'DROITES',
        coul1: '#7900c5',
        coul2: '#000000',
        contenu: []
      })
      if (lib.droitept) {
        scratch.params.scratchProgDonnees.menu[scratch.params.scratchProgDonnees.menu.length - 1].contenu.push({
          type: 'droitept',
          value: [{ nom: 'POINT1', contenu: { valeur: 'AB' } }]
        })
      }
      if (lib.droitepara) {
        scratch.params.scratchProgDonnees.menu[scratch.params.scratchProgDonnees.menu.length - 1].contenu.push({
          type: 'droitepara',
          value: [{ nom: 'NOM', contenu: { valeur: '(d1)' } }, {
            nom: 'PARA',
            contenu: { valeur: '(AB)' }
          }, { nom: 'POINT', contenu: { valeur: 'C' } }]
        })
      }
      if (lib.droiteperp) {
        scratch.params.scratchProgDonnees.menu[scratch.params.scratchProgDonnees.menu.length - 1].contenu.push({
          type: 'droiteperp',
          value: [{ nom: 'NOM', contenu: { valeur: '(d1)' } }, {
            nom: 'PERP',
            contenu: { valeur: '(AB)' }
          }, { nom: 'POINT', contenu: { valeur: 'C' } }]
        })
      }
    }

    if (lib.cercleCentrePoint || lib.cercleCentreRayon) {
      scratch.params.scratchProgDonnees.menu.push({
        type: 'catégorie',
        nom: 'CERCLES',
        coul1: '#E55B16',
        coul2: '#090505',
        contenu: []
      })
      if (lib.cercleCentrePoint) {
        scratch.params.scratchProgDonnees.menu[scratch.params.scratchProgDonnees.menu.length - 1].contenu.push({
          type: 'cercleCentrePoint',
          value: [{ nom: 'CENTRE', contenu: { valeur: 'O' } }, { nom: 'POINT', contenu: { valeur: 'A' } }, {
            nom: 'NOM',
            contenu: { valeur: 'C1' }
          }]
        })
      }
      if (lib.cercleCentreRayon) {
        scratch.params.scratchProgDonnees.menu[scratch.params.scratchProgDonnees.menu.length - 1].contenu.push({
          type: 'cercleCentreRayon',
          value: [{ nom: 'CENTRE', contenu: { valeur: 'O' } }, { nom: 'RAYON', contenu: { valeur: ' ' } }, {
            nom: 'NOM',
            contenu: { valeur: 'C1' }
          }]
        })
      }
    }

    if (lib.arc1PointPasse || lib.arccentreplus || lib.arccentremoins) {
      scratch.params.scratchProgDonnees.menu.push({
        type: 'catégorie',
        nom: 'ARC DE CERCLES',
        coul1: '#E57E16',
        coul2: '#010202',
        contenu: []
      })
      if (lib.arc1PointPasse) {
        scratch.params.scratchProgDonnees.menu[scratch.params.scratchProgDonnees.menu.length - 1].contenu.push({
          type: 'arc1PointPasse',
          value: [{ nom: 'POINT', contenu: { valeur: 'M' } }, { nom: 'NOM', contenu: { valeur: 'BC' } }]
        })
      }
      if (lib.arccentreplus) {
        scratch.params.scratchProgDonnees.menu[scratch.params.scratchProgDonnees.menu.length - 1].contenu.push({
          type: 'arccentreplus',
          value: [{ nom: 'CENTRE', contenu: { valeur: 'O' } }, { nom: 'NOM', contenu: { valeur: 'AB' } }]
        })
      }
      if (lib.arccentremoins) {
        scratch.params.scratchProgDonnees.menu[scratch.params.scratchProgDonnees.menu.length - 1].contenu.push({
          type: 'arccentremoins',
          value: [{ nom: 'CENTRE', contenu: { valeur: 'O' } }, { nom: 'NOM', contenu: { valeur: 'AB' } }]
        })
      }
    }

    if (lib.effacer || lib.renommer || lib.regroupe || lib.ecriSeg || lib.ecriDroite || lib.ecriDDroite || lib.nomAleatoire || lib.pointille) {
      scratch.params.scratchProgDonnees.menu.push({
        type: 'catégorie',
        nom: 'UTILITAIRES',
        coul1: '#B5E61D',
        coul2: '#3373CC',
        contenu: []
      })
      if (lib.effacer) {
        scratch.params.scratchProgDonnees.menu[scratch.params.scratchProgDonnees.menu.length - 1].contenu.push({
          type: 'mtgEffacer',
          value: [{ nom: 'NOM', contenu: { valeur: ' ' } }]
        })
      }
      if (lib.pointille) {
        scratch.params.scratchProgDonnees.menu[scratch.params.scratchProgDonnees.menu.length - 1].contenu.push({
          type: 'mtgPointille',
          value: [{ nom: 'NOM', contenu: { valeur: '[AB]' } }]
        })
      }
      if (lib.renommer) {
        scratch.params.scratchProgDonnees.menu[scratch.params.scratchProgDonnees.menu.length - 1].contenu.push({
          type: 'Renommer',
          value: [{ nom: 'NOM', contenu: { valeur: ' ' } }, { nom: 'NOM2', contenu: { valeur: ' ' } }]
        })
      }
      if (lib.regroupe) {
        scratch.params.scratchProgDonnees.menu[scratch.params.scratchProgDonnees.menu.length - 1].contenu.push({
          type: 'Regroupe',
          value: [{ nom: 'A', contenu: { valeur: ' ' } }, { nom: 'B', contenu: { valeur: ' ' } }]
        })
      }
      if (lib.ecriSeg) {
        scratch.params.scratchProgDonnees.menu[scratch.params.scratchProgDonnees.menu.length - 1].contenu.push({
          type: 'ecriSeg',
          value: [{ nom: 'A', contenu: { valeur: ' ' } }]
        })
      }
      if (lib.ecriDroite) {
        scratch.params.scratchProgDonnees.menu[scratch.params.scratchProgDonnees.menu.length - 1].contenu.push({
          type: 'ecriDroite',
          value: [{ nom: 'A', contenu: { valeur: ' ' } }, { nom: 'B', contenu: { valeur: ' ' } }]
        })
      }
      if (lib.ecriDDroite) {
        scratch.params.scratchProgDonnees.menu[scratch.params.scratchProgDonnees.menu.length - 1].contenu.push({
          type: 'ecriDDroite',
          value: [{ nom: 'A', contenu: { valeur: ' ' } }, { nom: 'B', contenu: { valeur: ' ' } }]
        })
      }
      if (lib.nomAleatoire) scratch.params.scratchProgDonnees.menu[scratch.params.scratchProgDonnees.menu.length - 1].contenu.push({ type: 'NomAleatoire' })
    }

    if (scratch.params.MtgProg.blocPerso) {
      const x2 = scratch.scratchCreeMenu([], true)
      const xmlDoc2 = $.parseXML(x2)
      let xml2 = xmlDoc2.firstChild
      while (xml2.nodeType !== 1) {
        xml2 = xml2.nextSibling
      }
      const aVire = addDefaultTable(me.zonesElts.MG, 1, 1)[0][0]
      for (let i = 0; i < scratch.params.MtgProg.blocPerso.length; i++) {
        if (i === 0) {
          scratch.params.scratchProgDonnees.menu.push({
            type: 'catégorie',
            nom: 'MES BLOCS',
            coul1: '#D14C7D',
            coul2: '#B8436E',
            contenu: []
          })
        }

        function urlImg (img, onlyRel) {
          if (onlyRel) return '/outils/scratch/' + img
          return j3pBaseUrl + 'outils/scratch/' + img
        }

        const nwBlo = scratch.params.MtgProg.blocPerso[i]
        const obdep = {
          message0: nwBlo.txt,
          args0: [],
          category: Blockly.Categories.motion,
          colour: nwBlo.colour,
          extensions: ['shape_statement']
        }
        const value = []
        let funX = () => ''
        let funY = () => ''
        let funZ = () => ''
        if (nwBlo.nbVar > 0) {
          obdep.args0.push({ type: 'input_value', name: 'X', default: '' })
          funX = (block) => {
            return Blockly.JavaScript.valueToCode(block, 'X', Blockly.JavaScript.ORDER_ATOMIC).replace(/'/g, '')
          }
          value.push({ nom: 'X', contenu: { valeur: ' ' } })
        }
        if (nwBlo.nbVar > 1) {
          obdep.args0.push({ type: 'input_value', name: 'Y', default: '' })
          funY = (block) => {
            return Blockly.JavaScript.valueToCode(block, 'Y', Blockly.JavaScript.ORDER_ATOMIC).replace(/'/g, '')
          }
          value.push({ nom: 'Y', contenu: { valeur: ' ' } })
        }
        if (nwBlo.nbVar > 2) {
          obdep.args0.push({ type: 'input_value', name: 'Z', default: '' })
          funZ = (block) => {
            return Blockly.JavaScript.valueToCode(block, 'Z', Blockly.JavaScript.ORDER_ATOMIC).replace(/'/g, '')
          }
          value.push({ nom: 'Z', contenu: { valeur: ' ' } })
        }
        obdep.message0 = nwBlo.txt
        let yaicone = false
        if (scratch.params.creaIconeChoice) {
          obdep.args0.splice(0, 0, { type: 'field_vertical_separator' })
          obdep.args0.splice(0, 0, {
            type: 'field_image',
            src: urlImg(scratch.params.creaIconeChoice),
            width: 30,
            height: 30
          })
          yaicone = true
        }
        const xml = $.parseXML(nwBlo.xmlText)
        scratch.params.lesBlocPerso.push(nwBlo.nom)
        scratch.params.listeDef[nwBlo.nom] = { nom: nwBlo.nom, nbVar: nwBlo.nbVar, xml, txt: nwBlo.txt, yaicone }
        Blockly.Blocks[nwBlo.nom] = {
          init: function () {
            this.jsonInit({
              type: 'make',
              message0: 'Définir ' + nwBlo.nom,
              inputsInline: false,
              nextStatement: null,
              colour: 330,
              tooltip: '',
              helpUrl: ''
            })
          }
        }
        Blockly.JavaScript[nwBlo.nom] = function () {
          return { type: 'make', eventtype: 'make', nom: nwBlo.nom }
        }
        Blockly.Blocks[nwBlo.nom + 'b'] = {
          init: function () {
            this.jsonInit(obdep)
          }
        }
        Blockly.JavaScript[nwBlo.nom + 'b'] = function (block) {
          const retValx = function () {
            return funX(block)
          }
          const retvaly = function () {
            return funY(block)
          }
          const retvalz = function () {
            return funZ(block)
          }
          return { type: 'Mb', nom: nwBlo.nom + 'b', valx: retValx(), valy: retvaly(), valz: retvalz() }
        }
        scratch.params.scratchProgDonnees.menu[scratch.params.scratchProgDonnees.menu.length - 1].contenu.push({
          type: nwBlo.nom + 'b',
          value
        })

        const side = 'start'
        scratch.params.scratchProgDonnees.workspaceDef = Blockly.inject(aVire, {
          comments: true,
          disable: false,
          collapse: false,
          media: j3pBaseUrl + 'externals/blockly/media/',
          readOnly: false,
          rtl: null,
          scrollbars: true,
          toolbox: xml2,
          toolboxPosition: side === 'top' || side === 'start' ? 'start' : 'end',
          horizontalLayout: side === 'top' || side === 'bottom',
          sounds: false,
          zoom: {
            controls: true,
            wheel: true,
            startScale: 0.6,
            maxScale: 2,
            minScale: 0.25,
            scaleSpeed: 1.1
          },
          colours: {
            fieldShadow: 'rgba(100, 100, 100, 0.3)',
            dragShadowOpacity: 0.6
          }
        })
        Blockly.svgResize(scratch.params.scratchProgDonnees.workspaceDef)
        // scratch.params.scratchProgDonnees.workspaceDef.clear()
        let x = xml.firstChild
        while (x.nodeType !== 1) {
          x = x.nextSibling
        }
        Blockly.Xml.domToWorkspace(x, scratch.params.scratchProgDonnees.workspaceDef)
        const ladef = Blockly.JavaScript.workspaceToCode(scratch.params.scratchProgDonnees.workspaceDef)
        ladef[0].splice(0, 1)
        scratch.params.listeDef[nwBlo.nom].def = ladef[0]
        j3pEmpty(aVire)
      }
      j3pDetruit(aVire)
    }

    if (scratch.params.MtgProg.crea) {
      scratch.params.scratchProgDonnees.menu.push({
        type: 'catégorie',
        nom: 'NOUVEAU BLOC',
        coul1: '#ffffff',
        coul2: '#030203',
        custom: 'PROCEDURE',
        contenu: []
      })
    }

    scratch.resetMtg()
    scratch.enonce([], {
      pourInject: { },
      modeCorrection: {
        type: 'SansComparaison',
        aVerif: []
      },
      dirigeProg: { sequence: ds.Sequence, pasApas: ds.PasAPas, cacheTest: ds.CacheTest }
    })
    if (window.top !== window) {
      parent.postMessage('BesoinDuInteger', '*')
      window.addEventListener('message', (event) => {
        function ajouteMenu (obj) {
          let indiceMesBloc = scratch.params.scratchProgDonnees.menu.length
          for (let i = 0; i < scratch.params.scratchProgDonnees.menu.length; i++) {
            if (scratch.params.scratchProgDonnees.menu[i].nom === obj.cat) indiceMesBloc = i
          }
          if (indiceMesBloc === scratch.params.scratchProgDonnees.menu.length) {
            scratch.params.scratchProgDonnees.menu.push({
              type: 'catégorie',
              nom: obj.cat,
              coul1: obj.coul1,
              coul2: obj.coul2,
              contenu: []
            })
          }
          if (!scratch.params.scratchProgDonnees.menu[indiceMesBloc].contenu.includes({
            type: obj.type,
            value: obj.value
          })) scratch.params.scratchProgDonnees.menu[indiceMesBloc].contenu.push({ type: obj.type, value: obj.value })
        }

        if (event.data.act === 'donneInteger') {
          const strReverse = event.data.there
          let ok = false
          // cercle diam 1
          if (strReverse[0] === '1') {
            ajouteMenu({
              cat: 'CERCLES',
              coul1: '#E55B16',
              coul2: '#090505',
              type: 'cercleDiametre',
              value: [{ nom: 'NOM', contenu: { valeur: 'C1' } }, { nom: 'DIAM', contenu: { valeur: 'AB' } }]
            })
            ok = true
          }
          // point alignés
          if (strReverse[1] === '1') {
            ajouteMenu({
              cat: 'POINTS',
              coul1: '#3230B8',
              coul2: '#000000',
              type: 'pointAligne',
              value: [{ nom: 'NOM', contenu: { valeur: 'C' } }, {
                nom: 'P1',
                contenu: { valeur: 'A' }
              }, { nom: 'P2', contenu: { valeur: 'B' } }]
            })
            ok = true
          }
          // mediatrice
          if (strReverse[2] === '1') {
            ajouteMenu({
              cat: 'DROITES',
              coul1: '#7900c5',
              coul2: '#000000',
              type: 'mediatrice',
              value: [{ nom: 'NOM', contenu: { valeur: 'd1' } }, { nom: 'CONT', contenu: { valeur: 'AB' } }]
            })
            ok = true
          }
          // triangle quelconque
          if (strReverse[3] === '1') {
            ajouteMenu({
              cat: 'POLYGONES',
              coul1: '#0027c5',
              coul2: '#000000',
              type: 'triangleQuelconque',
              value: [{ nom: 'NOM', contenu: { valeur: 'ABC' } }]
            })
            ok = true
          }
          // tri    // triangle equilat
          if (strReverse[4] === '1') {
            ajouteMenu({
              cat: 'POLYGONES',
              coul1: '#0027c5',
              coul2: '#000000',
              type: 'triangleEquilateral',
              value: [{ nom: 'NOM', contenu: { valeur: 'ABC' } }]
            })
            ok = true
          }

          if (!ok) return
          const x2 = scratch.scratchCreeMenu(scratch.params.scratchProgDonnees.menu, true)
          const xmlDoc2 = $.parseXML(x2)
          let xml2 = xmlDoc2.firstChild
          while (xml2.nodeType !== 1) {
            xml2 = xml2.nextSibling
          }
          scratch.params.scratchProgDonnees.workspace.updateToolbox(xml2)
          scratch.scratchMenuPerso()
          /// //
        }
      })
    }
    // fait prog réponse
    for (let i = 0; i < scratch.params.MtgProg.suite.length; i++) {
      const el2 = j3pClone(scratch.params.MtgProg.suite[i])
      const el = scratch.scratchMtgReplaceLettres(el2)
      switch (el.type) {
        case 'segment': {
          const ty = (el.long) ? 'segmentlongA' : 'segmentAB'
          scratch.params.scratchProgDonnees.lesordres.push({
            type: ty,
            value: [{ nom: 'EXT1', contenu: { valeur: el.noms[0].substring(1, 3) } }, {
              nom: 'LONG',
              contenu: { valeur: el.long }
            }]
          })
        }
          break
        case 'pointLibreAvecNom':
          scratch.params.scratchProgDonnees.lesordres.push({
            type: 'pointLibreAvecNom',
            value: [{ nom: 'NOM', contenu: { valeur: el.noms[0] } }]
          })
          break
        case 'pointIntersection':
          scratch.params.scratchProgDonnees.lesordres.push({
            type: 'pointIntersection',
            value: [{ nom: 'NOM', contenu: { valeur: el.noms[0] } }, {
              nom: 'CONT1',
              contenu: { valeur: el.cont1 }
            }, {
              nom: 'CONT2',
              contenu: { valeur: el.cont2 }
            }]
          })
          break
        case 'pointSur':
          scratch.params.scratchProgDonnees.lesordres.push({
            type: 'pointSur',
            value: [
              { nom: 'NOM', contenu: { valeur: el.noms[0] } }, { nom: 'CONT', contenu: { valeur: el.cont } }
            ]
          })
          break
        case 'pointMilieu':
          scratch.params.scratchProgDonnees.lesordres.push({
            type: 'pointMilieu',
            value: [
              { nom: 'NOM', contenu: { valeur: el.noms[0] } }, { nom: 'CONT', contenu: { valeur: el.cont } }
            ]
          })
          break
        case 'pointLong':
          scratch.params.scratchProgDonnees.lesordres.push({
            type: 'pointLong',
            value: [
              { nom: 'EXT1', contenu: { valeur: el.noms[0] } }, { nom: 'P1', contenu: { valeur: el.p1 } }, { nom: 'LONG', contenu: { valeur: el.long } }
            ]
          })
          break
        case 'segmentAB':
          scratch.params.scratchProgDonnees.lesordres.push({
            type: 'segmentAB',
            value: [
              { nom: 'EXT1', contenu: { valeur: el.noms[0].replace('[', '').replace(']', '') } }
            ]
          })
          break
        case 'segmentlongA':
          scratch.params.scratchProgDonnees.lesordres.push({
            type: 'segmentlongA',
            value: [
              { nom: 'EXT1', contenu: { valeur: el.noms[0].replace('[', '').replace(']', '') } },
              { nom: 'LONG', contenu: { valeur: el.long } }
            ]
          })
          break
        case 'droitepara':
          scratch.params.scratchProgDonnees.lesordres.push({
            type: 'droitepara',
            value: [
              { nom: 'NOM', contenu: { valeur: el.noms[0] } },
              {
                nom: 'PARA',
                contenu: { valeur: el.para }
              },
              { nom: 'POINT', contenu: { valeur: el.point } }
            ]
          })
          break
        case 'droiteperp':
          scratch.params.scratchProgDonnees.lesordres.push({
            type: 'droiteperp',
            value: [{ nom: 'NOM', contenu: { valeur: el.noms[0] } },
              {
                nom: 'PERP',
                contenu: { valeur: el.perp }
              },
              { nom: 'POINT', contenu: { valeur: el.point } }]
          })
          break
        case 'cercleCentrePoint':
          scratch.params.scratchProgDonnees.lesordres.push({
            type: 'cercleCentrePoint',
            value: [{ nom: 'CENTRE', contenu: { valeur: el.centre } },
              { nom: 'POINT', contenu: { valeur: el.point } },
              {
                nom: 'NOM',
                contenu: { valeur: el.noms[0] }
              }
            ]
          })
          break
        case 'arc1PointPasse':
          scratch.params.scratchProgDonnees.lesordres.push({
            type: 'arc1PointPasse',
            value: [
              { nom: 'POINT', contenu: { valeur: el.point } }, { nom: 'NOM', contenu: { valeur: el.noms[0] } }
            ]
          })
          break
        case 'arccentreplus':
          scratch.params.scratchProgDonnees.lesordres.push({
            type: 'arccentreplus',
            value: [
              { nom: 'CENTRE', contenu: { valeur: el.centre } }, { nom: 'NOM', contenu: { valeur: el.noms[0] } }
            ]
          })
          break
        case 'arccentremoins':
          scratch.params.scratchProgDonnees.lesordres.push({
            type: 'arccentremoins',
            value: [
              { nom: 'CENTRE', contenu: { valeur: el.centre } }, { nom: 'NOM', contenu: { valeur: el.noms[0] } }
            ]
          })
          break
        case 'point':
          if (el.sur) {
            if (el.sur.length === 1) {
              scratch.params.scratchProgDonnees.lesordres.push({
                type: 'pointSur',
                value: [{ nom: 'NOM', contenu: { valeur: el.noms[0] } }, {
                  nom: 'CONT',
                  contenu: { valeur: el.sur[0] }
                }]
              })
            } else {
              scratch.params.scratchProgDonnees.lesordres.push({
                type: 'pointIntersection',
                value: [{ nom: 'NOM', contenu: { valeur: el.noms[0] } }, {
                  nom: 'CONT1',
                  contenu: { valeur: el.sur[0] }
                }, { nom: 'CONT2', contenu: { valeur: el.sur[1] } }]
              })
            }
          } else {
            scratch.params.scratchProgDonnees.lesordres.push({
              type: 'pointLibreAvecNom',
              value: [{ nom: 'NOM', contenu: { valeur: el.noms[0] } }]
            })
          }
          break
        case 'droitept':
          scratch.params.scratchProgDonnees.lesordres.push({
            type: 'droitept',
            value: [{ nom: 'POINT1', contenu: { valeur: el.noms[0].substring(1, 3) } }]
          })
          break
        case 'demidroitept':
        case 'demidroite':
          scratch.params.scratchProgDonnees.lesordres.push({
            type: 'demidroitept',
            value: [{ nom: 'ORIGINE', contenu: { valeur: el.noms[0].substring(1, 3) } }]
          })
          break
        case 'cercleCentreRayon':
          scratch.params.scratchProgDonnees.lesordres.push({
            type: 'cercleCentreRayon',
            value: [{ nom: 'CENTRE', contenu: { valeur: el.centre } }, {
              nom: 'RAYON',
              contenu: { valeur: el.rayon }
            }, { nom: 'NOM', contenu: { valeur: el.noms[0] } }]
          })
          break
        case 'mtgEffacer':
          scratch.params.scratchProgDonnees.lesordres.push({
            type: 'mtgEffacer',
            value: [{ nom: 'NOM', contenu: { valeur: el.noms[0] } }]
          })
          break
        case 'mtgPointille':
          scratch.params.scratchProgDonnees.lesordres.push({
            type: 'mtgPointille',
            value: [{ nom: 'NOM', contenu: { valeur: el.noms[0] } }]
          })
          break
        default:
          j3pShowError(`type ${el.type} non géré`, { mustNotify: true })
      }
    }
    //

    j3pAjouteBouton(scratch.params.BouModel, aidefich, { value: 'Modèle à reproduire', className: 'boutonModele' })
    j3pAjouteBouton(scratch.params.BouModelBase, aidefichbase, {
      value: 'Figure de départ',
      className: 'boutonFigureDepart'
    })
    scratch.params.BoutonFigDeb = j3pAjouteBouton(scratch.params.BouMaFig, aideMaFig, { value: 'Figure de travail' })
    scratch.params.BoutonFigDeb.style.display = 'none'

    scratch.params.scratchProgDonnees.Averif = []
    me.finEnonce()
    me.cacheBoutonValider()
  }

  switch (this.etat) {
    case 'enonce':
      if (this.debutDeLaSection) {
        initSection()
      } else {
        enonceMain()
      }
      break // case "enonce":

    case 'correction':
      try {
        const scratch = stor.scratch
        const ret = scratch.prepCorrec()
        const xmlDoc2 = ret.xmlDoc2
        const xmlText = ret.xmlText

        if (scratch.params.scratchProgDonnees.repEleve) {
          // Bonne réponse
          this.score++
          scratch.corrigeGood(xmlDoc2, xmlText)
        } else {
          // Pas de bonne réponse
          scratch.corrige(xmlDoc2, xmlText)
        }
        // on veut être rappelé dans l’état navigation
        this.finCorrection('navigation', true)
      } catch (error) {
        console.error(error)
      }
      break // case "correction":

    case 'navigation':
      if (this.sectionTerminee()) {
        stor.scratch.lesdivs.BoutonsSuite.appendChild(this.buttonsElts.sectionSuivante)
        this.buttonsElts.sectionSuivante.addEventListener('click', () => {
          stor.scratch.params.grosStop = true
        })
      } else {
        this.etat = 'enonce'
        stor.scratch.lesdivs.BoutonsSuite.appendChild(this.buttonsElts.suite)
        this.buttonsElts.suite.addEventListener('click', () => {
          stor.scratch.params.grosStop = true
        })
      }
      // Obligatoire
      this.finNavigation()
      break // case "navigation":
  }
}
