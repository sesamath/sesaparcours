import getDefaultProgramme from './defaultProgramme'

export default function getDefaultProgrammeBlokmtg01 () {
  const defaultProgramme = getDefaultProgramme()

  // pour la section blokmtg01 on surcharge ces trucs
  const prog = defaultProgramme.Programme[0]
  prog.figdep = [{ type: 'zoneSeg', noms: ['zoneSeg0'], c: { x: 174.75, y: 53.866668701171875 }, p: { x: 42.75, y: 163.86666870117188 }, dep: [] }, { type: 'zoneSeg', noms: ['zoneSeg1'], c: { x: 297.75, y: 130.86666870117188 }, p: { x: 158.75, y: 297.8666687011719 }, dep: [] }, { type: 'point', noms: ['#1#'], sur: ['zoneSeg0'], haz: true, dep: ['zoneSeg0'], coef: '31' }, { type: 'point', noms: ['#2#'], sur: ['zoneSeg1'], haz: true, dep: ['zoneSeg1'], coef: '43' }]
  prog.suite = [{ type: 'droitept', noms: ['(#1##2#)'] }, { type: 'pointSur', noms: ['#3#'], cont: '(#1##2#)' }]
  prog.placeDep = true
  prog.listeBloc = { pointLibreAvecNom: true, pointIntersection: true, pointSur: true, pointMilieu: true, pointLong: true, segmentAB: true, segmentlongA: false, droitept: true, droitepara: false, droiteperp: false, demidroitept: true, cercleCentrePoint: false, cercleCentreRayon: false, arc1PointPasse: false, arccentreplus: false, arccentremoins: false, effacer: false, renommer: false, regroupe: false, ecriSeg: false, ecriDroite: false, ecriDDroite: false, nomAleatoire: false }
  prog.crea = false
  prog.niv = 0
  prog.nom = 'aligne 1'
  prog.blocPerso = []

  // et on retourne ce nouvel objet qu’on vient de créer
  return defaultProgramme
}
