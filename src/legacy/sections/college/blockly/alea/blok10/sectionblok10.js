import { j3pGetRandomInt } from 'src/legacy/core/functions'
import { j3pAffiche } from 'src/lib/mathquill/functions'
import { getNewScratch } from 'src/legacy/outils/scratch/loaders'
import { addDefaultTable } from 'src/legacy/themes/table'
// test
export const params = {
  outils: ['mathquill'],
  /**
   * Ce qui est paramétrable dans l’éditeur de graphe
   * @type {LegacySectionParamsParametres}
   */
  parametres: [
    ['nbrepetitions', 4, 'entier', 'Nombre de répétitions de la section'],
    ['nbchances', 1, 'entier', 'Nombre d’essais possibles (illimité si 0)'],
    ['Completer', true, 'boolean', '<u>true</u>: Une partie du programme est déjà en place.'],
    ['PasAPas', false, 'boolean', "<u>true</u>: L’élève peut lancer son programme en mode 'Pas à pas'."],
    ['Sequence', true, 'boolean', '<u>true</u>: Chaque bloc a une durée d’exécution d’environ 1 seconde.'],
    ['CacheTest', false, 'boolean', '<u>true</u>: L’élève ne voit pas l’action de son programme lors d la correction.']
  ]
}

const txtConsigne = ' <b> Complète ce programme </b> pour injecter un nombre au hasard entre £a et £b dans la variable <i>x</i> .'

/**
 * section blok10
 * @this {Parcours} {
 */
export default function main () {
  const me = this
  const stor = me.storage
  const ds = me.donneesSection
  let scratch = stor.scratch

  async function initSection () {
    ds.theme = 'zonesAvecImageDeFond'
    scratch = await getNewScratch()
    stor.scratch = scratch
    stor.pbfait = []
    me.validOnEnter = false // ex donneesSection.touche_entree
    scratch.initParams(me)
    scratch.testFlag = function () {
    }
    scratch.testFlagFin = function () {
      let ok
      let mess = ''
      if (scratch.params.scratchProgDonnees.valeursaleatoiressorties.length < 1) { ok = false; mess = 'Tu dois utiliser le bloc <i>nombre aléatoire</i> ! ' } else if (scratch.params.scratchProgDonnees.valeursaleatoiressorties.length > 1) { ok = false; mess = 'Tu ne dois utiliser qu’un fois le bloc <i>nombre aléatoire</i> ! ' } else {
        ok = ((scratch.params.scratchProgDonnees.valeursaleatoiressorties[0].borne1 === String(stor.valmin)) && (scratch.params.scratchProgDonnees.valeursaleatoiressorties[0].borne2 === String(stor.valmax)))
        if (ok) { mess = '' } else { mess = 'Vérifie les deux nombres !' }
      }
      return { ok, mess }
    }
    scratch.params.AutoriseSupprimeVariable = false
    scratch.params.AutoriseRenomeVariable = false

    if (ds.nbVariables > 3) { ds.nbVariables = 3 }
    if (ds.nbVariables < 1) { ds.nbVariables = 1 }
    me.construitStructurePage({ structure: 'presentation3', ratioGauche: 0.6 })
    me.stockage = [0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0]
    me.score = 0
    stor.dejafait = []
    me.afficheTitre('Injecter un nombre aléatoire dans une variable')
    enonceMain()
  }

  function enonceMain () {
    me.videLesZones()
    scratch.params.scratchProgDonnees.Lutins[0].cox = 10
    scratch.params.scratchProgDonnees.lesordres = []
    scratch.params.scratchProgDonnees.progdeb = []
    scratch.params.scratchProgDonnees.menu = []
    stor.valmin = j3pGetRandomInt(0, 200)
    stor.valmax = j3pGetRandomInt(300, 1000)
    scratch.params.scratchProgDonnees.progdeb.push({ type: 'Drapo', editable: 'false', deletable: 'false', movable: 'false', x: '20', y: '20' })
    scratch.params.scratchProgDonnees.menu.push({ type: 'catégorie', nom: 'Ordres', coul1: '#22dd22', coul2: '#22dd22', contenu: [] })
    if (ds.Completer) {
      scratch.params.scratchProgDonnees.progdeb.push({ type: 'data_setvariableto', editable: 'false', deletable: 'false', movable: 'false', variable: [{ nom: 'VARIABLE', nomvar: 'x' }], value: [{ nom: 'VALUE', editable: 'false', deletable: 'false', movable: 'false', contenu: { valeur: '0' } }] })
    } else { scratch.params.scratchProgDonnees.menu[0].contenu.push({ type: 'data_setvariableto', editable: 'false', variable: [{ nom: 'VARIABLE', nomvar: 'x', editable: 'false', deletable: 'false', movable: 'false', id: 'aaaaaaa' }], value: [{ nom: 'VALUE', contenu: { valeur: '0' } }] }) }
    scratch.params.scratchProgDonnees.menu[0].contenu.push({ type: 'operator_random', value: [{ nom: 'NUM1', contenu: { valeur: '1' } }, { nom: 'NUM2', contenu: { valeur: '10' } }] })
    scratch.params.scratchProgDonnees.lesordres.push({
      type: 'data_setvariableto',
      variable: [{ nom: 'VARIABLE', nomvar: 'x', id: 'aaaaaaa' }],
      value: [{
        nom: 'VALUE',
        contenu: {
          operateur: 'operator_random',
          value: [{ nom: 'NUM1', contenu: { valeur: stor.valmin } },
            { nom: 'NUM2', contenu: { valeur: stor.valmax } }]
        }
      }]
    })

    scratch.enonce([], {
      pourInject: { StartScale: 0.75 },
      modeCorrection: {
        type: 'SansComparaison',
        aVerif: [
          { quoi: 'NombreMaxBloc', nombre: 3 },
          { quoi: 'QuandAvertir', quand: 'apres' },
          { quoi: 'AnnuleSiAvert', quand: false }],
        lesvarbonnes: [{ nom: 'x', val: 'vide', id: 'aaaaaaa' }]
      },
      dirigeProg: { sequence: ds.Sequence, pasApas: ds.PasAPas, cacheTest: ds.CacheTest }
    })
    const yy = addDefaultTable(scratch.lesdivs.enonceG, 1, 1)[0][0]

    j3pAffiche(yy, null, txtConsigne, { a: stor.valmin, b: stor.valmax })

    me.finEnonce()
    // on ne veut pas du bouton ok car c’est le clic sur le drapeau qui valide (faut donc mettre ça après finEnonce)
    me.cacheBoutonValider()
  }

  switch (this.etat) {
    case 'enonce':
      // code exécuté au lancement de la section
      if (this.debutDeLaSection) {
        initSection()
      } else {
        enonceMain()
      }
      break // case "enonce":

    case 'correction':
      try {
        const ret = scratch.prepCorrec()
        const xmlDoc2 = ret.xmlDoc2
        const xmlText = ret.xmlText

        if (scratch.params.scratchProgDonnees.repEleve) {
          // Bonne réponse
          this.score++
          scratch.corrigeGood(xmlDoc2, xmlText)
        } else {
          // Pas de bonne réponse
          scratch.corrige(xmlDoc2, xmlText)
        }
        // on veut être rappelé dans l’état navigation
        this.finCorrection('navigation', true)
      } catch (error) {
        console.error(error)
      }
      break // case "correction":

    case 'navigation':
      if (this.sectionTerminee()) {
        scratch.lesdivs.BoutonsSuite.appendChild(this.buttonsElts.sectionSuivante)
        this.buttonsElts.sectionSuivante.addEventListener('click', () => {
          scratch.params.grosStop = true
        })
      } else {
        this.etat = 'enonce'
        scratch.lesdivs.BoutonsSuite.appendChild(this.buttonsElts.suite)
        this.buttonsElts.suite.addEventListener('click', () => {
          scratch.params.grosStop = true
        })
      }
      // Obligatoire
      this.finNavigation()
      break // case "navigation":
  }
}
