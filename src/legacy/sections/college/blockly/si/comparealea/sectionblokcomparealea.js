import { j3pClone, j3pShowError, j3pShuffle } from 'src/legacy/core/functions'
import BulleAide from 'src/legacy/outils/bulleAide/BulleAide'
import { j3pAffiche } from 'src/lib/mathquill/functions'
import { addDefaultTable } from 'src/legacy/themes/table'
import { getNewScratch } from 'src/legacy/outils/scratch/loaders'

export const params = {
  outils: ['mathquill'],
  /**
   * Ce qui est paramétrable dans l’éditeur de graphe
   * @type {LegacySectionParamsParametres}
   */
  parametres: [
    ['nbrepetitions', 4, 'entier', 'Nombre de répétitions de la section'],
    ['nbchances', 0, 'entier', 'Nombre d’essais possibles (illimité si 0)'],
    ['PasAPas', false, 'boolean', "<u>true</u>: L’élève peut lancer son programme en mode 'Pas à pas'."],
    ['Sequence', false, 'boolean', '<u>true</u>: Chaque bloc a une durée d’exécution d’environ 1 seconde.'],
    ['Completer', true, 'boolean', '<u>true</u>: Une partie du programme est déjà en place.'],
    ['nombre', true, 'boolean', 'Comparer un nombre choisi aléatoirement et un nombre donné par l’utilisateur.'],
    ['suivant', true, 'boolean', 'Comparer le suivant d’un nombre choisi aléatoirement et un nombre donné par l’utilisateur.'],
    ['precedent', true, 'boolean', 'Comparer le précédent d’un nombre choisi aléatoirement et un nombre donné par l’utilisateur.'],
    ['double', true, 'boolean', 'Comparer le double d’un nombre choisi aléatoirement et un nombre donné par l’utilisateur.'],
    ['triple', true, 'boolean', 'Comparer le triple d’un nombre choisi aléatoirement et un nombre donné par l’utilisateur.'],
    ['moitie', true, 'boolean', 'Comparer la moitié d’un nombre choisi aléatoirement et un nombre donné par l’utilisateur.'],
    ['tiers', true, 'boolean', 'Comparer le tiers d’un nombre choisi aléatoirement et un nombre donné par l’utilisateur.'],
    ['carre', true, 'boolean', 'Comparer le carré d’un nombre choisi aléatoirement et un nombre donné par l’utilisateur.'],
    ['somme', true, 'boolean', 'Comparer la somme de 2 nombres choisis aléatoirement et un nombre donné par l’utilisateur.'],
    ['difference', true, 'boolean', 'Comparer la difference de 2 nombres choisis aléatoirement et un nombre donné par l’utilisateur.'],
    ['produit', true, 'boolean', 'Comparer le produit de 2 nombres choisis aléatoirement et un nombre donné par l’utilisateur.'],
    ['quotient', true, 'boolean', 'Comparer le quotient de 2 nombres choisis aléatoirement et un nombre donné par l’utilisateur.'],
    ['CacheTest', false, 'boolean', '<u>true</u>: L’élève ne voit pas l’action de son programme lors d la correction.']
  ]
}

/**
 * section blokcomparealea
 * @this {Parcours}
 */
export default function main () {
  const me = this
  const stor = me.storage
  const ds = me.donneesSection
  let scratch = stor.scratch

  async function initSection () {
    scratch = await getNewScratch()
    stor.scratch = scratch
    me.validOnEnter = false // ex donneesSection.touche_entree
    scratch.initParams(me)
    scratch.testFlag = function () {
    }
    scratch.testFlagFin = function () {
      if (!scratch.said()[0]) return { ok: false, mess: 'Scratch aurait répondre quelque chose dans ce cas !' }
      const correcteur = scratch.said()[0] === 'C’est bon !'
      const cop = j3pClone(scratch.params.scratchProgDonnees.valeursaleatoiressorties)
      const repUtil = scratch.params.scratchProgDonnees.reponseDonnesAGetUS[scratch.params.scratchProgDonnees.reponseDonnesAGetUS.length - 1]
      let rep
      if (stor.ya2) {
        const y = cop.pop().valeur
        const x = cop.pop().valeur
        rep = stor.funcComp(x, y)
      } else {
        const x = cop.pop().valeur
        rep = stor.funcComp(x)
      }
      const UtilABon = Number(repUtil) === rep
      return (correcteur === UtilABon) ? { ok: true, mess: 'Objectif atteint' } : { ok: false, mess: 'Objectif non atteint' }
    }
    scratch.params.AutoriseSupprimeVariable = false
    scratch.params.AutoriseRenomeVariable = false
    scratch.params.validation = [{ nbvalid: 2 }]
    scratch.params.verifDeb = true

    ds.nbitems = ds.nbetapes * ds.nbrepetitions

    me.construitStructurePage('presentation3')
    me.stockage = [0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0]

    stor.dejafait = []
    stor.pbpos = []
    if (ds.nombre) { stor.pbpos.push('nombre') }
    if (ds.suivant) { stor.pbpos.push('suivant') }
    if (ds.precedent) { stor.pbpos.push('precedent') }
    if (ds.double) { stor.pbpos.push('double') }
    if (ds.triple) { stor.pbpos.push('triple') }
    if (ds.moitie) { stor.pbpos.push('moitie') }
    if (ds.tiers) { stor.pbpos.push('tiers') }
    if (ds.carre) { stor.pbpos.push('carre') }
    if (ds.somme) { stor.pbpos.push('somme') }
    if (ds.difference) { stor.pbpos.push('difference') }
    if (ds.produit) { stor.pbpos.push('produit') }
    if (ds.quotient) { stor.pbpos.push('quotient') }
    if (stor.pbpos.length === 0) {
      j3pShowError('Le parmétrage ne permet aucun exercice !')
      stor.pbpos.push('nombre')
    }

    stor.pbpos = j3pShuffle(stor.pbpos)
    const s = stor.pbpos
    stor.lespb = []

    let i
    for (i = 0; i < ds.nbitems; i++) {
      stor.lespb.push(s[i % s.length])
    }

    stor.lespb = j3pShuffle(stor.lespb)
    me.afficheTitre('Ecrire une condition')

    scratch.params.blokmenulimite.push({ type: 'dire', nb: 1 })
    scratch.params.blokmenulimite.push({ type: 'dire2', nb: 1 })
    enonceMain()
  }

  function enonceMain () {
    me.videLesZones()

    scratch.params.scratchProgDonnees.lesordres = []
    scratch.params.scratchProgDonnees.menu = []
    scratch.params.scratchProgDonnees.progdeb = []
    scratch.params.scratchProgDonnees.lesvarbonnes = []
    scratch.params.scratchProgDonnees.progdeb.push({ type: 'Drapo', editable: 'false', deletable: 'false', movable: 'false', x: '20', y: '20' })
    scratch.params.scratchProgDonnees.progdeb.push({ type: 'data_setvariableto', editable: 'false', deletable: 'false', movable: 'false', variable: [{ nom: 'VARIABLE', nomvar: 'x', id: 'aaaaaaa' }], value: [{ nom: 'VALUE', editable: 'false', deletable: 'false', movable: 'false', contenu: { type: 'operator_random', editable: 'false', deletable: 'false', movable: 'false', value: [{ nom: 'NUM1', contenu: { valeur: 1, editable: 'false', deletable: 'false', movable: 'false' } }, { nom: 'NUM2', contenu: { valeur: 10, editable: 'false', deletable: 'false', movable: 'false' } }] } }] })
    scratch.params.scratchProgDonnees.progdeb.push({ type: 'Get_us', value: [{ nom: 'STRING', contenu: { valeur: 'XXX', editable: 'false', deletable: 'false', movable: 'false' } }], editable: 'false', deletable: 'false', movable: 'false' })

    scratch.params.scratchProgDonnees.menu.push({ type: 'catégorie', nom: 'CONDITIONS', coul1: '#40BF4A', coul2: '#389438', contenu: [] })
    scratch.params.scratchProgDonnees.menu[0].contenu.push({ type: 'operator_equals', value: [{ nom: 'OPERAND1', contenu: { valeur: ' ' } }, { nom: 'OPERAND2', contenu: { valeur: ' ' } }] })
    scratch.params.scratchProgDonnees.menu.push({ type: 'catégorie', nom: 'OPERATEURS', coul1: '#40BF4A', coul2: '#389438', contenu: [] })
    scratch.params.scratchProgDonnees.menu[1].contenu.push({ type: 'operator_add', value: [{ nom: 'NUM1', contenu: { valeur: ' ' } }, { nom: 'NUM2', contenu: { valeur: ' ' } }] })
    scratch.params.scratchProgDonnees.menu[1].contenu.push({ type: 'operator_subtract', value: [{ nom: 'NUM1', contenu: { valeur: ' ' } }, { nom: 'NUM2', contenu: { valeur: ' ' } }] })
    scratch.params.scratchProgDonnees.menu[1].contenu.push({ type: 'operator_multiply', value: [{ nom: 'NUM1', contenu: { valeur: ' ' } }, { nom: 'NUM2', contenu: { valeur: ' ' } }] })
    scratch.params.scratchProgDonnees.menu[1].contenu.push({ type: 'operator_divide', value: [{ nom: 'NUM1', contenu: { valeur: ' ' } }, { nom: 'NUM2', contenu: { valeur: ' ' } }] })
    scratch.params.scratchProgDonnees.menu.push({ type: 'catégorie', nom: 'VARIABLES', coul1: '#FF8C1A', coul2: '#DB6E00', contenu: [] })
    scratch.params.scratchProgDonnees.menu[2].contenu.push({ type: 'reponse' })
    scratch.params.scratchProgDonnees.menu[2].contenu.push({ type: 'data_variable', variable: [{ nom: 'VARIABLE', nomvar: 'x', id: 'aaaaaaa' }] })

    if (!ds.Completer) {
      scratch.params.scratchProgDonnees.menu.push({ type: 'catégorie', nom: 'DIRE', coul1: '#9966FF', coul2: '#774DCB', contenu: [] })
      scratch.params.scratchProgDonnees.menu[3].contenu.push({ type: 'dire', value: [{ nom: 'DELTA', contenu: { valeur: 'C’est bon !', editable: 'false', movable: 'false', deletable: 'false' } }, { nom: 'TEMPS', contenu: { valeur: 2, editable: 'false', deletable: 'false', movable: 'false' } }] })
      scratch.params.scratchProgDonnees.menu[3].contenu.push({ type: 'dire2', value: [{ nom: 'DELTA', contenu: { valeur: 'C’est faux !', editable: 'false', movable: 'false', deletable: 'false' } }, { nom: 'TEMPS', contenu: { valeur: 2, editable: 'false', deletable: 'false', movable: 'false' } }] })
      scratch.params.scratchProgDonnees.menu.push({ type: 'catégorie', nom: 'CONTROLES', coul1: '#FFAB19', coul2: '#FFAB19', contenu: [] })
      scratch.params.scratchProgDonnees.menu[4].contenu.push({ type: 'control_if' })
      scratch.params.scratchProgDonnees.menu[4].contenu.push({ type: 'control_if_else' })
    } else {
      scratch.params.scratchProgDonnees.progdeb.push({ type: 'control_if_else', editable: 'false', deletable: 'false', movable: 'false', statement: [{ nom: 'SUBSTACK', contenu: [{ type: 'dire', editable: 'false', deletable: 'false', movable: 'false', value: [{ nom: 'DELTA', contenu: { valeur: 'C’est bon !', editable: 'false', movable: 'false', deletable: 'false' } }, { nom: 'TEMPS', contenu: { valeur: 2, editable: 'false', deletable: 'false', movable: 'false' } }] }] }, { nom: 'SUBSTACK2', contenu: [{ type: 'dire2', editable: 'false', deletable: 'false', movable: 'false', value: [{ nom: 'DELTA', contenu: { valeur: 'C’est faux !', editable: 'false', movable: 'false', deletable: 'false' } }, { nom: 'TEMPS', contenu: { valeur: 2, editable: 'false', deletable: 'false', movable: 'false' } }] }] }] })
    }
    scratch.params.blokmenulimite.push({ type: 'dire', nb: 1 })
    scratch.params.scratchProgDonnees.lesvarbonnes.push({ nom: 'x', val: 0, id: 'aaaaaaa' })

    stor.pbencours = stor.lespb.pop()

    scratch.params.scratchProgDonnees.listValPos = []

    let Lacons = 'Nathéo a commmencé un programme pour tester si un utilisateur <br>connaît £a.  <b> Termine ce programme </b>.'
    let compcons
    const lacond = { nom: 'CONDITION', contenu: { type: 'operator_equals', value: [{ nom: 'OPERAND1', contenu: { reponse: '' } }, { nom: 'OPERAND2', contenu: {} }] } }
    stor.ya2 = false
    stor.pbencours = 'quotient'
    switch (stor.pbencours) {
      case 'nombre': scratch.params.scratchProgDonnees.progdeb[2].value[0].contenu.valeur = 'Donne la valeur de x'
        compcons = 'la valeur d’une variable'
        lacond.contenu.value[1].contenu = { type: 'data_variable', variable: [{ nom: 'VARIABLE', nomvar: 'x', id: 'aaaaaaa' }] }
        stor.funcComp = (x) => {
          return x
        }
        for (let i = 2; i < 8; i++) {
          scratch.params.scratchProgDonnees.listValPos.push([String(i)])
        }
        break
      case 'suivant': scratch.params.scratchProgDonnees.progdeb[2].value[0].contenu.valeur = 'Donne le suivant de x'
        compcons = 'le suivant d’un nombre'
        lacond.contenu.value[1].contenu = { operateur: '+', value: [{ nom: 'NUM1', contenu: { type: 'data_variable', variable: [{ nom: 'VARIABLE', nomvar: 'x', id: 'aaaaaaa' }] } }, { nom: 'NUM2', contenu: { valeur: 1 } }] }
        stor.funcComp = (x) => {
          return x + 1
        }
        for (let i = 3; i < 9; i++) {
          scratch.params.scratchProgDonnees.listValPos.push([String(i)])
        }
        break
      case 'precedent': scratch.params.scratchProgDonnees.progdeb[2].value[0].contenu.valeur = 'Donne le précédent de x'
        compcons = 'le précédent d’un nombre'
        lacond.contenu.value[1].contenu = { operateur: '-', value: [{ nom: 'NUM1', contenu: { type: 'data_variable', variable: [{ nom: 'VARIABLE', nomvar: 'x', id: 'aaaaaaa' }] } }, { nom: 'NUM2', contenu: { valeur: 1 } }] }
        stor.funcComp = (x) => {
          return x - 1
        }
        for (let i = 0; i < 7; i++) {
          scratch.params.scratchProgDonnees.listValPos.push([String(i)])
        }
        break
      case 'double': scratch.params.scratchProgDonnees.progdeb[2].value[0].contenu.valeur = 'Donne le double de x'
        compcons = 'le double d’une variable'
        lacond.contenu.value[1].contenu = { operateur: '*', value: [{ nom: 'NUM1', contenu: { type: 'data_variable', variable: [{ nom: 'VARIABLE', nomvar: 'x', id: 'aaaaaaa' }] } }, { nom: 'NUM2', contenu: { valeur: 2 } }] }
        stor.funcComp = (x) => {
          return 2 * x
        }
        for (let i = 2; i < 8; i++) {
          scratch.params.scratchProgDonnees.listValPos.push([String(i * 2)])
        }
        break
      case 'triple': scratch.params.scratchProgDonnees.progdeb[2].value[0].contenu.valeur = 'Donne le triple de x'
        compcons = 'le triple d’un nombre'
        lacond.contenu.value[1].contenu = { operateur: '*', value: [{ nom: 'NUM1', contenu: { type: 'data_variable', variable: [{ nom: 'VARIABLE', nomvar: 'x', id: 'aaaaaaa' }] } }, { nom: 'NUM2', contenu: { valeur: 3 } }] }
        stor.funcComp = (x) => {
          return 3 * x
        }
        for (let i = 2; i < 8; i++) {
          scratch.params.scratchProgDonnees.listValPos.push([String(i * 3)])
        }
        break
      case 'moitie': scratch.params.scratchProgDonnees.progdeb[2].value[0].contenu.valeur = 'Donne la moitié de x'
        compcons = 'la moitié d’un nombre'
        lacond.contenu.value[1].contenu = { operateur: '/', value: [{ nom: 'NUM1', contenu: { type: 'data_variable', variable: [{ nom: 'VARIABLE', nomvar: 'x', id: 'aaaaaaa' }] } }, { nom: 'NUM2', contenu: { valeur: 2 } }] }
        stor.funcComp = (x) => {
          return x / 2
        }
        scratch.params.scratchProgDonnees.listValPos = [['1'], ['2'], ['4']]
        break
      case 'tiers': scratch.params.scratchProgDonnees.progdeb[2].value[0].contenu.valeur = 'Donne le tiers de x'
        compcons = 'le tiers d’un nombre'
        lacond.contenu.value[1].contenu = { operateur: '/', value: [{ nom: 'NUM1', contenu: { type: 'data_variable', variable: [{ nom: 'VARIABLE', nomvar: 'x', id: 'aaaaaaa' }] } }, { nom: 'NUM2', contenu: { valeur: 3 } }] }
        stor.funcComp = (x) => {
          return x / 3
        }
        scratch.params.scratchProgDonnees.listValPos = [['1'], ['2'], ['3']]
        break
      case 'carre': scratch.params.scratchProgDonnees.progdeb[2].value[0].contenu.valeur = 'Donne le carré de x'
        compcons = 'le carré d’un nombre'
        lacond.contenu.value[1].contenu = { operateur: '*', value: [{ nom: 'NUM1', contenu: { type: 'data_variable', variable: [{ nom: 'VARIABLE', nomvar: 'x', id: 'aaaaaaa' }] } }, { nom: 'NUM2', contenu: { type: 'data_variable', variable: [{ nom: 'VARIABLE', nomvar: 'x', id: 'aaaaaaa' }] } }] }
        stor.funcComp = (x) => {
          return x * x
        }
        for (let i = 4; i < 8; i++) {
          scratch.params.scratchProgDonnees.listValPos.push([String(i * i)])
        }
        break
      case 'somme':
        scratch.params.scratchProgDonnees.lesvarbonnes.push({ nom: 'y', val: 'vide', id: 'bbbbbbb' })
        scratch.params.scratchProgDonnees.menu[2].contenu.push({ type: 'data_variable', variable: [{ nom: 'VARIABLE', nomvar: 'y', id: 'bbbbbbb' }] })
        scratch.params.scratchProgDonnees.progdeb[2].value[0].contenu.valeur = 'Donne la somme de x et y'
        compcons = 'la somme de deux termes'
        lacond.contenu.value[1].contenu = { operateur: '+', value: [{ nom: 'NUM1', contenu: { type: 'data_variable', variable: [{ nom: 'VARIABLE', nomvar: 'x', id: 'aaaaaaa' }] } }, { nom: 'NUM2', contenu: { type: 'data_variable', variable: [{ nom: 'VARIABLE', nomvar: 'y', id: 'bbbbbbb' }] } }] }
        stor.funcComp = (x, y) => {
          return x + y
        }
        scratch.params.scratchProgDonnees.listValPos = [['5'], ['10'], ['7'], ['12']]
        stor.ya2 = true
        break
      case 'difference': scratch.params.scratchProgDonnees.lesvarbonnes.push({ nom: 'y', val: 'vide', id: 'bbbbbbb' })
        scratch.params.scratchProgDonnees.menu[2].contenu.push({ type: 'data_variable', variable: [{ nom: 'VARIABLE', nomvar: 'y', id: 'bbbbbbb' }] })
        scratch.params.scratchProgDonnees.progdeb[2].value[0].contenu.valeur = 'Donne la différence x - y'
        compcons = 'la différence de deux termes'
        lacond.contenu.value[1].contenu = { operateur: '-', value: [{ nom: 'NUM1', contenu: { type: 'data_variable', variable: [{ nom: 'VARIABLE', nomvar: 'x', id: 'aaaaaaa' }] } }, { nom: 'NUM2', contenu: { type: 'data_variable', variable: [{ nom: 'VARIABLE', nomvar: 'y', id: 'bbbbbbb' }] } }] }
        stor.funcComp = (x, y) => {
          return x - y
        }
        scratch.params.scratchProgDonnees.listValPos = [['5'], ['6'], ['7'], ['8']]
        stor.ya2 = true
        break
      case 'produit':
        scratch.params.scratchProgDonnees.lesvarbonnes.push({ nom: 'y', val: 'vide', id: 'bbbbbbb' })
        scratch.params.scratchProgDonnees.menu[2].contenu.push({
          type: 'data_variable',
          variable: [{ nom: 'VARIABLE', nomvar: 'y', id: 'bbbbbbb' }]
        })
        scratch.params.scratchProgDonnees.progdeb[2].value[0].contenu.valeur = 'Donne le produit de x et y'
        compcons = 'le produit de deux facteurs'
        lacond.contenu.value[1].contenu = {
          operateur: '*',
          value: [{
            nom: 'NUM1',
            contenu: { type: 'data_variable', variable: [{ nom: 'VARIABLE', nomvar: 'x', id: 'aaaaaaa' }] }
          }, { nom: 'NUM2', contenu: { type: 'data_variable', variable: [{ nom: 'VARIABLE', nomvar: 'y', id: 'bbbbbbb' }] } }]
        }
        stor.funcComp = (x, y) => {
          return x * y
        }
        stor.ya2 = true
        scratch.params.scratchProgDonnees.listValPos = [['20'], ['30'], ['16'], ['24']]
        break
      case 'quotient':scratch.params.scratchProgDonnees.lesvarbonnes.push({ nom: 'y', val: 'vide', id: 'bbbbbbb' })
        scratch.params.scratchProgDonnees.menu[2].contenu.push({ type: 'data_variable', variable: [{ nom: 'VARIABLE', nomvar: 'y', id: 'bbbbbbb' }] })
        scratch.params.scratchProgDonnees.progdeb[2].value[0].contenu.valeur = 'Donne le quotient de x par y'
        compcons = 'le quotient de deux nombres'
        lacond.contenu.value[1].contenu = { operateur: '/', value: [{ nom: 'NUM1', contenu: { type: 'data_variable', variable: [{ nom: 'VARIABLE', nomvar: 'x', id: 'aaaaaaa' }] } }, { nom: 'NUM2', contenu: { type: 'data_variable', variable: [{ nom: 'VARIABLE', nomvar: 'y', id: 'bbbbbbb' }] } }] }
        stor.funcComp = (x, y) => {
          return x / y
        }
        stor.ya2 = true
        scratch.params.scratchProgDonnees.listValPos = [['3'], ['2']]
        break
      default: j3pShowError(stor.pbencours)
    }

    scratch.params.scratchProgDonnees.lesordres.push({ type: 'data_setvariableto', editable: 'false', deletable: 'false', movable: 'false', variable: [{ nom: 'VARIABLE', nomvar: 'x', id: 'aaaaaaa' }], value: [{ nom: 'VALUE', editable: 'false', deletable: 'false', movable: 'false', contenu: { type: 'operator_random', editable: 'false', deletable: 'false', movable: 'false', value: [{ nom: 'NUM1', contenu: { valeur: 1 } }, { nom: 'NUM2', contenu: { valeur: 10 } }] } }] })

    let ft = 2
    if (scratch.params.scratchProgDonnees.menu[2].contenu.length > 2) {
      ft++
      scratch.params.scratchProgDonnees.progdeb.splice(2, 0, { type: 'data_setvariableto', editable: 'false', deletable: 'false', movable: 'false', variable: [{ nom: 'VARIABLE', nomvar: 'y', id: 'bbbbbbb' }], value: [{ nom: 'VALUE', editable: 'false', deletable: 'false', movable: 'false', contenu: { type: 'operator_random', editable: 'false', deletable: 'false', movable: 'false', value: [{ nom: 'NUM1', contenu: { valeur: 1, editable: 'false', deletable: 'false', movable: 'false' } }, { nom: 'NUM2', contenu: { valeur: 10, editable: 'false', deletable: 'false', movable: 'false' } }] } }] })
      scratch.params.scratchProgDonnees.lesordres.push({ type: 'data_setvariableto', editable: 'false', deletable: 'false', movable: 'false', variable: [{ nom: 'VARIABLE', nomvar: 'y', id: 'bbbbbbb' }], value: [{ nom: 'VALUE', editable: 'false', deletable: 'false', movable: 'false', contenu: { type: 'operator_random', editable: 'false', deletable: 'false', movable: 'false', value: [{ nom: 'NUM1', contenu: { valeur: 1 } }, { nom: 'NUM2', contenu: { valeur: 10 } }] } }] })
    }
    scratch.params.scratchProgDonnees.lesordres.push({ type: 'Get_us', value: [{ nom: 'STRING', contenu: { valeur: scratch.params.scratchProgDonnees.progdeb[ft].value[0].contenu.valeur, editable: 'false', deletable: 'false', movable: 'false' } }], editable: 'false', deletable: 'false', movable: 'false' })

    scratch.params.scratchProgDonnees.lesordres.push({ type: 'control_if_else', value: [lacond], statement: [{ nom: 'SUBSTACK', contenu: [{ type: 'dire', value: [{ nom: 'DELTA', contenu: { valeur: 'C’est bon !', editable: 'false', movable: 'false', deletable: 'false' } }, { nom: 'TEMPS', contenu: { valeur: 2, editable: 'false', deletable: 'false', movable: 'false' } }] }] }, { nom: 'SUBSTACK2', contenu: [{ type: 'dire2', value: [{ nom: 'DELTA', contenu: { valeur: 'C’est faux !', editable: 'false', movable: 'false', deletable: 'false' } }, { nom: 'TEMPS', contenu: { valeur: 2, editable: 'false', deletable: 'false', movable: 'false' } }] }] }] })

    Lacons = Lacons.replace('£a', compcons)
    scratch.params.scratchProgDonnees.Lutins[0].coxbase = 10
    scratch.params.scratchProgDonnees.Lutins[0].cox = 10

    scratch.enonce([], {
      pourInject: { StartScale: 0.65 },
      modeCorrection: {
        type: 'SansComparaison',
        aVerif: [
          { quoi: 'QuandAvertir', quand: 'apres' },
          { quoi: 'AnnuleSiAvert', quand: false }]
      },
      dirigeProg: { sequence: ds.Sequence, pasApas: ds.PasAPas, cacheTest: ds.CacheTest }
    })

    const yy = addDefaultTable(scratch.lesdivs.enonceG, 1, 2)
    j3pAffiche(yy[0][0], null, Lacons + '&nbsp;')
    yy[0][1].innerHTML = '&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;'
    stor.aise = new BulleAide(yy[0][1], 'Tu dois ajouter la condition dans le bloc <b>si ... alors</b>', { place: 5 })

    const vVal = scratch.retrouveBorne()
    const idX = vVal[0].idBlok
    const idY = vVal[1].idBlok

    stor.scratch.params.valideAlea = [[]]
    switch (stor.pbencours) {
      case 'nombre':
        stor.scratch.params.valideAlea[0].push({ val: 2, idBlok: idX })
        stor.scratch.params.valideAlea[0].push({ val: 3, idBlok: idX })
        stor.scratch.params.valideAlea[0].push({ val: 7, idBlok: idX })
        break
      case 'suivant':
        stor.scratch.params.valideAlea[0].push({ val: 2, idBlok: idX })
        stor.scratch.params.valideAlea[0].push({ val: 3, idBlok: idX })
        stor.scratch.params.valideAlea[0].push({ val: 7, idBlok: idX })
        break
      case 'precedent':
        stor.scratch.params.valideAlea[0].push({ val: 2, idBlok: idX })
        stor.scratch.params.valideAlea[0].push({ val: 3, idBlok: idX })
        stor.scratch.params.valideAlea[0].push({ val: 7, idBlok: idX })
        break
      case 'double':
        stor.scratch.params.valideAlea[0].push({ val: 2, idBlok: idX })
        stor.scratch.params.valideAlea[0].push({ val: 3, idBlok: idX })
        stor.scratch.params.valideAlea[0].push({ val: 7, idBlok: idX })
        break
      case 'triple':
        stor.scratch.params.valideAlea[0].push({ val: 2, idBlok: idX })
        stor.scratch.params.valideAlea[0].push({ val: 3, idBlok: idX })
        stor.scratch.params.valideAlea[0].push({ val: 7, idBlok: idX })
        break
      case 'moitie':
        stor.scratch.params.valideAlea[0].push({ val: 2, idBlok: idX })
        stor.scratch.params.valideAlea[0].push({ val: 4, idBlok: idX })
        stor.scratch.params.valideAlea[0].push({ val: 8, idBlok: idX })
        break
      case 'tiers':
        stor.scratch.params.valideAlea[0].push({ val: 6, idBlok: idX })
        stor.scratch.params.valideAlea[0].push({ val: 3, idBlok: idX })
        stor.scratch.params.valideAlea[0].push({ val: 9, idBlok: idX })
        break
      case 'carre':
        stor.scratch.params.valideAlea[0].push({ val: 4, idBlok: idX })
        stor.scratch.params.valideAlea[0].push({ val: 3, idBlok: idX })
        stor.scratch.params.valideAlea[0].push({ val: 7, idBlok: idX })
        break
      case 'somme':
        stor.scratch.params.valideAlea[0].push({ val: 3, idBlok: idX })
        stor.scratch.params.valideAlea[0].push({ val: 5, idBlok: idX })
        stor.scratch.params.valideAlea[1] = []
        stor.scratch.params.valideAlea[1].push({ val: 2, idBlok: idY })
        stor.scratch.params.valideAlea[1].push({ val: 7, idBlok: idY })
        break
      case 'difference':
        stor.scratch.params.valideAlea[0].push({ val: 10, idBlok: idX })
        stor.scratch.params.valideAlea[0].push({ val: 8, idBlok: idX })
        stor.scratch.params.valideAlea[1] = []
        stor.scratch.params.valideAlea[1].push({ val: 2, idBlok: idY })
        stor.scratch.params.valideAlea[1].push({ val: 3, idBlok: idY })
        break
      case 'produit':
        stor.scratch.params.valideAlea[0].push({ val: 10, idBlok: idX })
        stor.scratch.params.valideAlea[0].push({ val: 8, idBlok: idX })
        stor.scratch.params.valideAlea[1] = []
        stor.scratch.params.valideAlea[1].push({ val: 2, idBlok: idY })
        stor.scratch.params.valideAlea[1].push({ val: 3, idBlok: idY })
        break
      case 'quotient':
        stor.scratch.params.valideAlea[0].push({ val: 6, idBlok: idX })
        stor.scratch.params.valideAlea[1] = []
        stor.scratch.params.valideAlea[1].push({ val: 2, idBlok: idY })
        stor.scratch.params.valideAlea[1].push({ val: 3, idBlok: idY })
        break
      default: j3pShowError(stor.pbencours)
    }

    me.finEnonce()
    me.cacheBoutonValider()
  }

  switch (this.etat) {
    case 'enonce':

      if (this.debutDeLaSection) {
        initSection()
      } else {
        enonceMain()
      }
      break // case "enonce":

    case 'correction':
      try {
        const ret = scratch.prepCorrec()
        const xmlDoc2 = ret.xmlDoc2
        const xmlText = ret.xmlText

        if (scratch.params.scratchProgDonnees.repEleve) {
          // Bonne réponse
          this.score++
          scratch.corrigeGood(xmlDoc2, xmlText)
        } else {
          // Pas de bonne réponse
          scratch.corrige(xmlDoc2, xmlText)
        }
        // on veut être rappelé dans l’état navigation
        this.finCorrection('navigation', true)
      } catch (error) {
        console.error(error)
      }
      break // case "correction":

    case 'navigation':

      if (this.sectionTerminee()) {
        scratch.lesdivs.BoutonsSuite.appendChild(this.buttonsElts.sectionSuivante)
        this.buttonsElts.sectionSuivante.addEventListener('click', () => {
          scratch.params.grosStop = true
        })
      } else {
        this.etat = 'enonce'
        scratch.lesdivs.BoutonsSuite.appendChild(this.buttonsElts.suite)
        this.buttonsElts.suite.addEventListener('click', () => {
          scratch.params.grosStop = true
        })
      }
      // Obligatoire
      this.finNavigation()

      break // case "navigation":
  }
}
