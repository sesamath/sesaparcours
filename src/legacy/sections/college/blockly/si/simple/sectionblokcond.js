import { j3pAddElt, j3pClone, j3pEmpty, j3pGetRandomInt, j3pShuffle } from 'src/legacy/core/functions'
import ListeDeroulante from 'src/legacy/outils/listeDeroulante/ListeDeroulante'
import { j3pAffiche } from 'src/lib/mathquill/functions'
import { addDefaultTable } from 'src/legacy/themes/table'
import ZoneStyleMathquill1 from 'src/legacy/outils/zoneStyleMathquill/ZoneStyleMathquill1'
import { getNewScratch } from 'src/legacy/outils/scratch/loaders'
import { j3pBaseUrl } from 'src/lib/core/constantes'
import textesGeneriques from 'src/lib/core/textes'

const { cBien, cFaux, essaieEncore, tempsDepasse } = textesGeneriques

/**
 * Ajouter une description ici
 * @fileOverview
 * @author Tommy Barroy <t.barroy@laposte.net>
 * @since 2017-10
*/

// chargé dynamiquement plus bas
let Blockly

export const params = {
  outils: ['mathquill'],
  /**
   * Ce qui est paramétrable dans l’éditeur de graphe
   * @type {LegacySectionParamsParametres}
   */
  parametres: [
    ['nbrepetitions', 3, 'entier', 'Nombre de répétitions de la section'],
    ['nbchances', 2, 'entier', 'Nombre d’essais possibles'],
    ['Justifie', true, 'boolean', '<u>true</u>: L’élève doit d’abord donner les valeurs des deux membres.'],
    ['Logique', true, 'boolean', '<u>true</u>: Utilisation des connecteurs logique <b><i>et</i></b> ,  <b><i>ou</i></b> et <b><i>non</i></b>.'],
    ['Operations', true, 'boolean', 'Une opération est nécessaire pour déterminer la valeur retournée'],
    ['Nb_Variable', 1, 'entier', 'Nombre de variables utilisées (entre 0 et 3)'],
    // le modèle gère la compatibilité ascendante avec l’ancien paramètre MiseEnPagePlusClaire
    ['theme', 'standard', 'liste', 'Modèle pour l’affichage', ['standard', 'zonesAvecImageDeFond']]
  ]
}

/**
 * section blokcond
 * @this {Parcours}
 */
export default function main () {
  const me = this
  const stor = me.storage
  const ds = me.donneesSection
  let scratch = stor.scratch

  async function initSection () {
    scratch = await getNewScratch()
    Blockly = scratch.Blockly
    stor.scratch = scratch
    let i
    // On surcharge avec les parametres passés par le graphe
    me.validOnEnter = false // ex donneesSection.touche_entree

    // Construction de la page
    me.construitStructurePage('presentation1bis')

    ds.lesExos = []

    let lp = ['operator_gt', 'operator_lt', 'operator_equals']
    lp = j3pShuffle(lp)
    for (i = 0; i < me.donneesSection.nbitems; i++) {
      ds.lesExos.push({ type: lp[i % lp.length] })
    }
    ds.lesExos = j3pShuffle(ds.lesExos)

    lp = j3pShuffle(lp)
    for (i = 0; i < me.donneesSection.nbitems; i++) {
      ds.lesExos[i].type2 = lp[i % lp.length]
    }
    ds.lesExos = j3pShuffle(ds.lesExos)

    lp = ['operator_not', 'operator_and', 'operator_or']
    lp = j3pShuffle(lp)
    for (i = 0; i < me.donneesSection.nbitems; i++) {
      ds.lesExos[i].logique = lp[i % lp.length]
    }
    ds.lesExos = j3pShuffle(ds.lesExos)

    lp = [true, false]
    lp = j3pShuffle(lp)
    for (i = 0; i < me.donneesSection.nbitems; i++) {
      ds.lesExos[i].cestTrue = lp[i % lp.length]
    }
    ds.lesExos = j3pShuffle(ds.lesExos)

    if (isNaN(ds.Nb_Variable)) ds.Nb_Variable = 0
    ds.Nb_Variable = Math.max(0, Math.min(3, ds.Nb_Variable))

    if (!ds.Justifie) ds.nbchances = 1
    me.typederreurs = [0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0]

    me.score = 0
    const tt = 'Déterminer une condition'

    me.afficheTitre(tt)

    enonceMain()
  }

  function faisChoixExos () {
    const e = ds.lesExos.pop()
    let ause = ['n', 'n2']
    let varipos = ['x', 'y', 'z']
    const varop = ['operator_add', 'operator_subtract', 'operator_multiply']
    varipos = j3pShuffle(varipos)
    if (ds.Nb_Variable < 3) varipos.pop()
    if (ds.Nb_Variable < 2) varipos.pop()
    if (ds.Nb_Variable < 1) varipos.pop()
    ause = ause.concat(varipos)
    const aplace = j3pShuffle(ause)
    stor.x = j3pGetRandomInt(1, 9)
    stor.y = j3pGetRandomInt(1, 9)
    stor.z = j3pGetRandomInt(1, 9)
    stor.n = j3pGetRandomInt(1, 9)
    stor.n2 = j3pGetRandomInt(1, 9)
    scratch.params.scratchProgDonnees = {}
    scratch.params.scratchProgDonnees.progdeb = []
    scratch.params.scratchProgDonnees.progdeb.push({
      type: 'Drapo',
      x: '20',
      y: '30'
    })
    scratch.params.scratchProgDonnees.progdeb.push({
      type: 'dire',
      value: [{
        nom: 'DELTA',
        contenu: {

        }
      }, {
        nom: 'TEMPS',
        contenu: {
          valeur: 2
        }
      }]
    })

    if (ds.Logique) {
      let agre = 'OPERAND1'
      scratch.params.scratchProgDonnees.progdeb[1].value[0].contenu.type = e.logique
      if (e.logique === 'operator_not') agre = 'OPERAND'
      scratch.params.scratchProgDonnees.progdeb[1].value[0].contenu.value = []
      if (!ds.Operations) {
        stor.o1 = aplace[j3pGetRandomInt(0, aplace.length - 1)]
        stor.o2 = aplace[j3pGetRandomInt(0, aplace.length - 1)]
        stor.o3 = aplace[j3pGetRandomInt(0, aplace.length - 1)]
        stor.o4 = aplace[j3pGetRandomInt(0, aplace.length - 1)]
        scratch.params.scratchProgDonnees.progdeb[1].value[0].contenu.value[0] = {
          nom: agre,
          contenu: {
            type: e.type,
            value: [
              {
                nom: 'OPERAND1',
                contenu: retP(stor.o1)
              },
              {
                nom: 'OPERAND2',
                contenu: retP(stor.o2)
              }
            ]
          }
        }
        scratch.params.scratchProgDonnees.progdeb[1].value[0].contenu.value[1] = {
          nom: 'OPERAND2',
          contenu: {
            type: e.type2,
            value: [
              {
                nom: 'OPERAND1',
                contenu: retP(stor.o3)
              },
              {
                nom: 'OPERAND2',
                contenu: retP(stor.o4)
              }
            ]
          }
        }
        stor.fRep = function () {
          return flog(e.logique)(fcomp(e.type)(fval(stor.o1), fval(stor.o2)), fcomp(e.type2)(fval(stor.o3), fval(stor.o4)))
        }
      } else {
        stor.o1 = aplace[j3pGetRandomInt(0, aplace.length - 1)]
        stor.o2 = aplace[j3pGetRandomInt(0, aplace.length - 1)]
        stor.o3 = aplace[j3pGetRandomInt(0, aplace.length - 1)]
        stor.o4 = aplace[j3pGetRandomInt(0, aplace.length - 1)]
        stor.o5 = aplace[j3pGetRandomInt(0, aplace.length - 1)]
        stor.o6 = aplace[j3pGetRandomInt(0, aplace.length - 1)]
        stor.o7 = aplace[j3pGetRandomInt(0, aplace.length - 1)]
        stor.o8 = aplace[j3pGetRandomInt(0, aplace.length - 1)]
        stor.op1 = varop[j3pGetRandomInt(0, varop.length - 1)]
        stor.op2 = varop[j3pGetRandomInt(0, varop.length - 1)]
        stor.op3 = varop[j3pGetRandomInt(0, varop.length - 1)]
        stor.op4 = varop[j3pGetRandomInt(0, varop.length - 1)]
        scratch.params.scratchProgDonnees.progdeb[1].value[0].contenu.value[0] = {
          nom: agre,
          contenu: {
            type: e.type,
            value: [
              {
                nom: 'OPERAND1',
                contenu: {
                  type: stor.op1,
                  value: [
                    {
                      nom: 'NUM1',
                      contenu: retP(stor.o1)
                    },
                    {
                      nom: 'NUM2',
                      contenu: retP(stor.o2)
                    }
                  ]
                }
              },
              {
                nom: 'OPERAND2',
                contenu: {
                  type: stor.op2,
                  value: [
                    {
                      nom: 'NUM1',
                      contenu: retP(stor.o3)
                    },
                    {
                      nom: 'NUM2',
                      contenu: retP(stor.o4)
                    }
                  ]
                }
              }
            ]
          }
        }
        scratch.params.scratchProgDonnees.progdeb[1].value[0].contenu.value[1] = {
          nom: 'OPERAND2',
          contenu: {
            type: e.type2,
            value: [
              {
                nom: 'OPERAND1',
                contenu: {
                  type: stor.op3,
                  value: [
                    {
                      nom: 'NUM1',
                      contenu: retP(stor.o5)
                    },
                    {
                      nom: 'NUM2',
                      contenu: retP(stor.o6)
                    }
                  ]
                }
              },
              {
                nom: 'OPERAND2',
                contenu: {
                  type: stor.op4,
                  value: [
                    {
                      nom: 'NUM1',
                      contenu: retP(stor.o7)
                    },
                    {
                      nom: 'NUM2',
                      contenu: retP(stor.o8)
                    }
                  ]
                }
              }
            ]
          }
        }

        stor.fRep = function () {
          return flog(e.logique)(fcomp(e.type)(fop(stor.op1)(fval(stor.o1), fval(stor.o2)), fop(stor.op2)(fval(stor.o3), fval(stor.o4))), fcomp(e.type2)(fop(stor.op3)(fval(stor.o5), fval(stor.o6)), fop(stor.op4)(fval(stor.o7), fval(stor.o8))))
        }
      }
    } else {
      stor.o1 = aplace[j3pGetRandomInt(0, aplace.length - 1)]
      stor.o2 = aplace[j3pGetRandomInt(0, aplace.length - 1)]
      scratch.params.scratchProgDonnees.progdeb[1].value[0].contenu.type = e.type
      scratch.params.scratchProgDonnees.progdeb[1].value[0].contenu.value = []
      if (!ds.Operations) {
        scratch.params.scratchProgDonnees.progdeb[1].value[0].contenu.value[0] = {
          nom: 'OPERAND1',
          contenu: retP(stor.o1)
        }
        scratch.params.scratchProgDonnees.progdeb[1].value[0].contenu.value[1] = {
          nom: 'OPERAND2',
          contenu: retP(stor.o2)
        }
        stor.fRep = function () {
          return fcomp(e.type)(fval(stor.o1), fval(stor.o2))
        }
      } else {
        stor.o3 = aplace[j3pGetRandomInt(0, aplace.length - 1)]
        stor.o4 = aplace[j3pGetRandomInt(0, aplace.length - 1)]
        stor.op1 = varop[j3pGetRandomInt(0, varop.length - 1)]
        stor.op2 = varop[j3pGetRandomInt(0, varop.length - 1)]
        scratch.params.scratchProgDonnees.progdeb[1].value[0].contenu.value[0] = {
          nom: 'OPERAND1',
          contenu: { type: stor.op1, value: [] }
        }
        scratch.params.scratchProgDonnees.progdeb[1].value[0].contenu.value[1] = {
          nom: 'OPERAND2',
          contenu: {
            type: stor.op2,
            value: []
          }
        }

        scratch.params.scratchProgDonnees.progdeb[1].value[0].contenu.value[0].contenu.value[0] = {
          nom: 'NUM1',
          contenu: retP(stor.o1)
          // {type:'operator_equals',contenu:[{ nom: 'OPERAND1', contenu: retP(aplace[0]) }, { nom: 'OPERAND2', contenu: retP(aplace[1%aplace.length]) }]}
        }
        scratch.params.scratchProgDonnees.progdeb[1].value[0].contenu.value[0].contenu.value[1] = {
          nom: 'NUM2',
          contenu: retP(stor.o2)
        }
        scratch.params.scratchProgDonnees.progdeb[1].value[0].contenu.value[1].contenu.value[0] = {
          nom: 'NUM1',
          contenu: retP(stor.o3)
          // {type:'operator_equals',contenu:[{ nom: 'OPERAND1', contenu: retP(aplace[0]) }, { nom: 'OPERAND2', contenu: retP(aplace[1%aplace.length]) }]}
        }
        scratch.params.scratchProgDonnees.progdeb[1].value[0].contenu.value[1].contenu.value[1] = {
          nom: 'NUM2',
          contenu: retP(stor.o4)
        }

        stor.fRep = function () {
          return fcomp(e.type)(fop(stor.op1)(fval(stor.o1), fval(stor.o2)), fop(stor.op2)(fval(stor.o3), fval(stor.o4)))
        }
      }
    }
    return e
  }

  function fval (k) {
    switch (k) {
      case 'n': return stor.n
      case 'n2': return stor.n2
      case 'x': return stor.x
      case 'y': return stor.y
      case 'z': return stor.z
    }
  }
  function fop (k) {
    switch (k) {
      case 'operator_add': return function (a, b) { return a + b }
      case 'operator_subtract': return function (a, b) { return a - b }
      case 'operator_multiply': return function (a, b) { return a * b }
      case 'operator_divide': return function (a, b) { return a / b }
    }
  }
  function fop2 (k) {
    switch (k) {
      case 'operator_add': return ' + '
      case 'operator_subtract': return ' - '
      case 'operator_multiply': return ' $ \\times $'
      case 'operator_divide':return ' / '
    }
  }
  function fcomp (k) {
    switch (k) {
      case 'operator_equals': return function (a, b) { return a === b }
      case 'operator_gt': return function (a, b) { return a > b }
      case 'operator_lt': return function (a, b) { return a < b }
    }
  }
  function fcomp2 (k) {
    switch (k) {
      case 'operator_equals': return '='
      case 'operator_gt': return '>'
      case 'operator_lt':return '<'
    }
  }
  function flog (k) {
    switch (k) {
      case 'operator_and': return function (a, b) { return a && b }
      case 'operator_or': return function (a, b) { return a || b }
      case 'operator_not': return function (a, b) { return !a }
    }
  }
  function retP (k) {
    if (k === 'n') return { valeur: stor.n }
    if (k === 'n2') return { valeur: stor.n2 }
    return {
      variable: k, id: retId(k)
    }
  }
  function retId (k) {
    switch (k) {
      case 'x': return 'aaaa1'
      case 'y': return 'aaaa2'
      case 'z': return 'aaaa3'
    }
  }
  function poseQuestion () {
    j3pAffiche(stor.lesdivs.consigne, null, '<b>On veut déterminer le résultat affiché par ce programme.</b>')
    scratch.params.scratchProgDonnees.lesvarbonnes = []
    scratch.params.scratchProgDonnees.lesvarbonnes[0] = { nom: 'x', id: 'aaaa1', val: stor.x }
    scratch.params.scratchProgDonnees.lesvarbonnes[1] = { nom: 'y', id: 'aaaa2', val: stor.y }
    scratch.params.scratchProgDonnees.lesvarbonnes[2] = { nom: 'z', id: 'aaaa3', val: stor.z }
    scratch.params.scratchProgDonnees.workspace = Blockly.inject(stor.lesdivs.prog, {
      comments: true,
      disable: false,
      collapse: false,
      media: j3pBaseUrl + 'externals/blockly/media/',
      readOnly: true,
      rtl: null,
      scrollbars: false,
      // toolbox: $(xml).find("#toolbox")[0],
      toolbox: {},
      maxInstances: scratch.params.scratchProgDonnees.menuLimite,
      toolboxPosition: 'top',
      horizontalLayout: 'top',
      sounds: false,
      zoom: {
        controls: false,
        wheel: true,
        startScale: 0.7,
        maxScale: 2,
        minScale: 0.25,
        scaleSpeed: 1.1
      },
      colours: {
        fieldShadow: 'rgba(100, 100, 100, 0.3)',
        dragShadowOpacity: 0.6
      }
    })
    // parametrages divers des fenêtres de SofusPy
    // document.getElementById("div_blockly").style.height=(window.innerHeight-100) + "px";
    stor.lesdivs.prog.style.height = '120px'
    stor.lesdivs.prog.style.width = '720px'
    Blockly.svgResize(scratch.params.scratchProgDonnees.workspace)
    // scratchSetLocale('fr')
    scratch.loadSample2total({})
    let i
    scratch.params.scratchProgDonnees.variables = []
    for (i = 0; i < scratch.params.scratchProgDonnees.variables.length; i++) {
      scratch.params.scratchProgDonnees.variables[i].val = 0
    }

    scratch.params.scratchProgDonnees.reponse = 0
    stor.rect1 = []
    stor.rect2 = []
    stor.text1 = []
    stor.text2 = []
    let svg = '<svg viewBox="-10 0 300 28" >  '
    scratch.params.scratchProgDonnees.variables = [{ name: 'x', val: stor.x }, { name: 'y', val: stor.y }, { name: 'z', val: stor.z }]
    svg += '<rect x="0" y="0" width="0" height="0" style="stroke-width:1px;fill:rgb(200,100,100);stroke:rgb(200,200,200)" />'
    const haut = 20
    for (i = 0; i < scratch.params.scratchProgDonnees.variables.length; i++) {
      svg += '<rect id="AffVarRect1' + i + '" x="5" y="' + (10 + (i * haut)) + '" rx="3" ry="3" width="2" height="2" style="stroke-width:1px;fill:rgb(200,200,255);stroke:rgb(100,100,150)" />'
      svg += '<rect id="AffVarRect2' + i + '" x="40" y="' + (10 + (i * haut)) + '" rx="2" ry="2" width="2" height="2" style="stroke-width:0;fill:rgb(229,83,0);stroke:rgb(200,200,200)" />'
      svg += '<text id="AffVarText1' + i + '" x="9" y="10"  style="font-size:10px;">' + scratch.params.scratchProgDonnees.variables[i].name + '</text>'
      svg += '<text id="AffVarText2' + i + '" x="25" y="10"  style="font-size:10px;stroke:#ffffff">' + scratch.params.scratchProgDonnees.variables[i].val + '</text>'
    }
    svg += '</svg>'
    stor.lesdivs.varia.innerHTML = svg
    stor.lesdivs.varia.style.height = '15px'
    stor.lesdivs.varia.style.width = '500px'

    stor.elZero = stor.lesdivs.varia.childNodes[0].childNodes[1]
    for (i = 0; i < scratch.params.scratchProgDonnees.variables.length; i++) {
      stor.rect1[i] = stor.lesdivs.varia.childNodes[0].childNodes[4 * i + 2]
      stor.rect2[i] = stor.lesdivs.varia.childNodes[0].childNodes[4 * i + 3]
      stor.text1[i] = stor.lesdivs.varia.childNodes[0].childNodes[4 * i + 4]
      stor.text2[i] = stor.lesdivs.varia.childNodes[0].childNodes[4 * i + 5]
    }
    scratchReplaceVar()
    /** @type {ListeDeroulante[]} */
    stor.listesDeroulantes = []
    stor.repj = []
    if (ds.Justifie) {
      if (ds.Logique) {
        const agard = j3pClone(scratch.params.scratchProgDonnees.progdeb[1].value[0].contenu)
        const yhtgr = addDefaultTable(stor.lesdivs.justi, 3, 2)
        if (ds.Operations) {
          scratch.params.scratchProgDonnees.progdeb = [j3pClone(agard.value[0].contenu)]
          scratch.params.scratchProgDonnees.workspace = Blockly.inject(yhtgr[0][0], {
            comments: true,
            disable: false,
            collapse: false,
            media: j3pBaseUrl + 'externals/blockly/media/',
            readOnly: true,
            rtl: null,
            scrollbars: false,
            // toolbox: $(xml).find("#toolbox")[0],
            toolbox: {},
            maxInstances: scratch.params.scratchProgDonnees.menuLimite,
            toolboxPosition: 'top',
            horizontalLayout: 'top',
            sounds: false,
            zoom: {
              controls: false,
              wheel: true,
              startScale: 0.7,
              maxScale: 2,
              minScale: 0.25,
              scaleSpeed: 1.1
            },
            colours: {
              fieldShadow: 'rgba(100, 100, 100, 0.3)',
              dragShadowOpacity: 0.6
            }
          })
          yhtgr[0][0].style.height = '60px'
          yhtgr[0][0].style.width = '300px'
          Blockly.svgResize(scratch.params.scratchProgDonnees.workspace)
          scratch.loadSample2total({})
          stor.listesDeroulantes.push(ListeDeroulante.create(yhtgr[0][1], ['Choisir', 'est vrai', 'est faux']))
          stor.repj.push(fcomp(stor.Lexo.type)(fop(stor.op1)(fval(stor.o1), fval(stor.o2)), fop(stor.op2)(fval(stor.o3), fval(stor.o4))))
        } else {
          scratch.params.scratchProgDonnees.progdeb = [j3pClone(agard.value[0].contenu)]
          scratch.params.scratchProgDonnees.workspace = Blockly.inject(yhtgr[0][0], {
            comments: true,
            disable: false,
            collapse: false,
            media: j3pBaseUrl + 'externals/blockly/media/',
            readOnly: true,
            rtl: null,
            scrollbars: false,
            // toolbox: $(xml).find("#toolbox")[0],
            toolbox: {},
            maxInstances: scratch.params.scratchProgDonnees.menuLimite,
            toolboxPosition: 'top',
            horizontalLayout: 'top',
            sounds: false,
            zoom: {
              controls: false,
              wheel: true,
              startScale: 0.7,
              maxScale: 2,
              minScale: 0.25,
              scaleSpeed: 1.1
            },
            colours: {
              fieldShadow: 'rgba(100, 100, 100, 0.3)',
              dragShadowOpacity: 0.6
            }
          })
          yhtgr[0][0].style.height = '60px'
          yhtgr[0][0].style.width = '300px'
          Blockly.svgResize(scratch.params.scratchProgDonnees.workspace)
          scratch.loadSample2total({})
          stor.listesDeroulantes[0] = ListeDeroulante.create(yhtgr[0][1], ['Choisir', 'est vrai', 'est faux'])
          stor.repj[0] = fcomp(stor.Lexo.type)(fval(stor.o1), fval(stor.o2))
        }
        if (stor.Lexo.logique !== 'operator_not') {
          yhtgr[1][0].style.height = '10px'
          if (ds.Operations) {
            scratch.params.scratchProgDonnees.progdeb = [j3pClone(agard.value[1].contenu)]
            scratch.params.scratchProgDonnees.workspace = Blockly.inject(yhtgr[2][0], {
              comments: true,
              disable: false,
              collapse: false,
              media: j3pBaseUrl + 'externals/blockly/media/',
              readOnly: true,
              rtl: null,
              scrollbars: false,
              // toolbox: $(xml).find("#toolbox")[0],
              toolbox: {},
              maxInstances: scratch.params.scratchProgDonnees.menuLimite,
              toolboxPosition: 'top',
              horizontalLayout: 'top',
              sounds: false,
              zoom: {
                controls: false,
                wheel: true,
                startScale: 0.7,
                maxScale: 2,
                minScale: 0.25,
                scaleSpeed: 1.1
              },
              colours: {
                fieldShadow: 'rgba(100, 100, 100, 0.3)',
                dragShadowOpacity: 0.6
              }
            })
            yhtgr[2][0].style.height = '60px'
            yhtgr[2][0].style.width = '300px'
            Blockly.svgResize(scratch.params.scratchProgDonnees.workspace)
            scratch.loadSample2total({})
            stor.listesDeroulantes.push(ListeDeroulante.create(yhtgr[2][1], ['Choisir', 'est vrai', 'est faux']))
            stor.repj.push(fcomp(stor.Lexo.type2)(fop(stor.op3)(fval(stor.o5), fval(stor.o6)), fop(stor.op4)(fval(stor.o7), fval(stor.o8))))
          } else {
            scratch.params.scratchProgDonnees.progdeb = [j3pClone(agard.value[1].contenu)]
            scratch.params.scratchProgDonnees.workspace = Blockly.inject(yhtgr[2][0], {
              comments: true,
              disable: false,
              collapse: false,
              media: j3pBaseUrl + 'externals/blockly/media/',
              readOnly: true,
              rtl: null,
              scrollbars: false,
              // toolbox: $(xml).find("#toolbox")[0],
              toolbox: {},
              maxInstances: scratch.params.scratchProgDonnees.menuLimite,
              toolboxPosition: 'top',
              horizontalLayout: 'top',
              sounds: false,
              zoom: {
                controls: false,
                wheel: true,
                startScale: 0.7,
                maxScale: 2,
                minScale: 0.25,
                scaleSpeed: 1.1
              },
              colours: {
                fieldShadow: 'rgba(100, 100, 100, 0.3)',
                dragShadowOpacity: 0.6
              }
            })
            yhtgr[2][0].style.height = '60px'
            yhtgr[2][0].style.width = '300px'
            Blockly.svgResize(scratch.params.scratchProgDonnees.workspace)
            scratch.loadSample2total({})
            stor.listesDeroulantes.push(ListeDeroulante.create(yhtgr[2][1], ['Choisir', 'est vrai', 'est faux']))
            stor.repj.push(fcomp(stor.Lexo.type2)(fval(stor.o3), fval(stor.o4)))
          }
        }
      } else {
        const agard = j3pClone(scratch.params.scratchProgDonnees.progdeb[1].value[0].contenu)
        if (ds.Operations) {
          const mlkjh = addDefaultTable(stor.lesdivs.justi, 1, 5)
          mlkjh[0][3].style.backgroundColor = '#3ad758'
          mlkjh[0][3].style.paddingBottom = '5px'
          j3pAffiche(mlkjh[0][1], null, '&nbsp; revient à dire &nbsp;')

          scratch.params.scratchProgDonnees.progdeb = [j3pClone(agard)]
          scratch.params.scratchProgDonnees.workspace = Blockly.inject(mlkjh[0][0], {
            comments: true,
            disable: false,
            collapse: false,
            media: j3pBaseUrl + 'externals/blockly/media/',
            readOnly: true,
            rtl: null,
            scrollbars: false,
            // toolbox: $(xml).find("#toolbox")[0],
            toolbox: {},
            maxInstances: scratch.params.scratchProgDonnees.menuLimite,
            toolboxPosition: 'top',
            horizontalLayout: 'top',
            sounds: false,
            zoom: {
              controls: false,
              wheel: true,
              startScale: 0.7,
              maxScale: 2,
              minScale: 0.25,
              scaleSpeed: 1.1
            },
            colours: {
              fieldShadow: 'rgba(100, 100, 100, 0.3)',
              dragShadowOpacity: 0.6
            }
          })
          mlkjh[0][0].style.height = '60px'
          mlkjh[0][0].style.width = '300px'
          Blockly.svgResize(scratch.params.scratchProgDonnees.workspace)
          scratch.loadSample2total({})

          const mlkjffffh = addDefaultTable(mlkjh[0][3], 1, 5)
          j3pAffiche(mlkjffffh[0][0], null, ' &nbsp; ')
          j3pAffiche(mlkjffffh[0][4], null, ' &nbsp; ')
          j3pAffiche(mlkjffffh[0][2], null, ' &nbsp; ' + fcomp2(stor.Lexo.type) + ' &nbsp; ')
          stor.listesDeroulantes[0] = new ZoneStyleMathquill1(mlkjffffh[0][1], { enter: me.sectionCourante.bind(me), limite: 5, restric: '-0123456789' })
          stor.repj[0] = fop(stor.op1)(fval(stor.o1), fval(stor.o2))
          stor.listesDeroulantes[1] = new ZoneStyleMathquill1(mlkjffffh[0][3], { enter: me.sectionCourante.bind(me), limite: 5, restric: '-0123456789' })
          stor.repj[1] = fop(stor.op2)(fval(stor.o3), fval(stor.o4))
        } else {
          if ((stor.o1 + stor.o2).indexOf('x') !== -1 || (stor.o1 + stor.o2).indexOf('y') !== -1 || (stor.o1 + stor.o2).indexOf('z') !== -1) {
            const mlkjh = addDefaultTable(stor.lesdivs.justi, 1, 3)
            mlkjh[0][2].style.backgroundColor = '#3ad758'
            j3pAffiche(mlkjh[0][1], null, '&nbsp; revient à dire &nbsp;')
            scratch.params.scratchProgDonnees.progdeb = [j3pClone(agard)]
            scratch.params.scratchProgDonnees.workspace = Blockly.inject(mlkjh[0][0], {
              comments: true,
              disable: false,
              collapse: false,
              media: j3pBaseUrl + 'externals/blockly/media/',
              readOnly: true,
              rtl: null,
              scrollbars: false,
              // toolbox: $(xml).find("#toolbox")[0],
              toolbox: {},
              maxInstances: scratch.params.scratchProgDonnees.menuLimite,
              toolboxPosition: 'top',
              horizontalLayout: 'top',
              sounds: false,
              zoom: {
                controls: false,
                wheel: true,
                startScale: 0.7,
                maxScale: 2,
                minScale: 0.25,
                scaleSpeed: 1.1
              },
              colours: {
                fieldShadow: 'rgba(100, 100, 100, 0.3)',
                dragShadowOpacity: 0.6
              }
            })
            mlkjh[0][0].style.height = '60px'
            mlkjh[0][0].style.width = '300px'
            Blockly.svgResize(scratch.params.scratchProgDonnees.workspace)
            scratch.loadSample2total({})
            const mlkjffffh = addDefaultTable(mlkjh[0][2], 1, 5)
            j3pAffiche(mlkjffffh[0][0], null, ' &nbsp; ')
            j3pAffiche(mlkjffffh[0][4], null, ' &nbsp; ')
            j3pAffiche(mlkjffffh[0][2], null, ' &nbsp; ' + fcomp2(stor.Lexo.type) + ' &nbsp; ')
            if (stor.o1.indexOf('n') === -1) {
              stor.listesDeroulantes[0] = new ZoneStyleMathquill1(mlkjffffh[0][1], { enter: me.sectionCourante.bind(me), limite: 5, restric: '-0123456789' })
              stor.repj[0] = fval(stor.o1)
            } else {
              j3pAffiche(mlkjffffh[0][1], null, ' &nbsp; $' + fval(stor.o1) + '$ &nbsp; ')
            }
            if (stor.o2.indexOf('n') === -1) {
              stor.listesDeroulantes[stor.listesDeroulantes.length] = new ZoneStyleMathquill1(mlkjffffh[0][3], { enter: me.sectionCourante.bind(me), limite: 5, restric: '-0123456789' })
              stor.repj.push(fval(stor.o2))
            } else {
              j3pAffiche(mlkjffffh[0][3], null, ' &nbsp; $' + fval(stor.o2) + '$ &nbsp; ')
            }
          }
        }
      }
    }
    const aqzs = addDefaultTable(stor.lesdivs.rep, 1, 2)
    j3pAffiche(aqzs[0][0], null, 'Ce programme va afficher &nbsp;')
    stor.laliste = ListeDeroulante.create(aqzs[0][1], ['Choisir', '"vrai"', '"faux"'])
    stor.lesdivs.ConSigne.classList.add('enonce')
    stor.lesdivs.Travail.classList.add('travail')
    me.zonesElts.MG.classList.add('fond')
  }
  function scratchReplaceVar () {
    const el = stor.elZero// or other selector like querySelector()
    const zero = el.getBoundingClientRect()
    if (zero.x === undefined) zero.x = zero.left
    if (zero.y === undefined) zero.y = zero.top
    for (let i = 0; i < scratch.params.scratchProgDonnees.variables.length; i++) {
      stor.text2[i].innerHTML = scratch.params.scratchProgDonnees.variables[i].val
      stor.rect1[i].width.baseVal.value = 40
      stor.rect2[i].width.baseVal.value = 18
      stor.rect1[i].height.baseVal.value = 13
      stor.rect2[i].height.baseVal.value = 11
      stor.rect1[i].y.baseVal.value = 2
      stor.rect2[i].y.baseVal.value = 3
      stor.rect2[i].x.baseVal.value = 20 + i * 60
      stor.text2[i].x.baseVal.getItem(0).value = 27 + i * 60
      stor.text2[i].y.baseVal.getItem(0).value = 11.5
      stor.rect1[i].x.baseVal.value = i * 60
      stor.text1[i].x.baseVal.getItem(0).value = 5 + i * 60
      stor.text1[i].y.baseVal.getItem(0).value = 11

      const mes = 'visible'
      stor.rect1[i].style.visibility = mes
      stor.rect2[i].style.visibility = mes
      stor.text1[i].style.visibility = mes
      stor.text2[i].style.visibility = mes
    }
  }
  function creeLesDiv () {
    stor.lesdivs = {}
    stor.lesdivs.conteneur = j3pAddElt(me.zonesElts.MG, 'div', {
      style: me.styles.etendre('toutpetit.enonce', { padding: '10px' })
    })
    stor.lesdivs.ConSigne = addDefaultTable(stor.lesdivs.conteneur, 1, 1)[0][0]
    stor.lesdivs.consigne = j3pAddElt(stor.lesdivs.ConSigne, 'div')
    stor.lesdivs.prog = j3pAddElt(stor.lesdivs.ConSigne, 'div')
    stor.lesdivs.varia1 = j3pAddElt(stor.lesdivs.ConSigne, 'div')
    const hj = addDefaultTable(stor.lesdivs.varia1, 1, 2)
    stor.lesdivs.varia = hj[0][1]
    j3pAffiche(hj[0][0], null, '&nbsp;')
    stor.lesdivs.Travail = addDefaultTable(stor.lesdivs.conteneur, 1, 1)[0][0]
    const hj2 = addDefaultTable(stor.lesdivs.Travail, 1, 2)
    stor.lesdivs.justi = j3pAddElt(hj2[0][0], 'div')
    stor.lesdivs.rep = j3pAddElt(hj2[0][0], 'div')
    stor.lesdivs.solution = hj2[0][1]
    stor.lesdivs.correction = j3pAddElt(me.zonesElts.MD, 'div', {
      style: me.styles.etendre('toutpetit.enonce', { padding: '10px' })
    })
    stor.lesdivs.explications = j3pAddElt(stor.lesdivs.conteneur, 'div')
    stor.lesdivs.explications.style.color = me.styles.cfaux
    stor.lesdivs.solution.style.color = me.styles.toutpetit.correction.color
  }

  function yaReponse () {
    let i
    if (stor.listesDeroulantes.length > 0) {
      for (i = 0; i < stor.listesDeroulantes.length; i++) {
        if (ds.Logique) {
          if (!stor.listesDeroulantes[i].changed) {
            stor.listesDeroulantes[i].focus()
            return false
          }
        } else {
          if (!stor.listesDeroulantes[i].reponse() === '') {
            stor.listesDeroulantes[i].focus()
            return false
          }
        }
      }
    }
    if (!stor.laliste.changed) {
      stor.laliste.focus()
      return false
    }
    return true
  }

  function isRepOk () {
    let i, ok
    stor.errJus = false

    ok = true
    if (stor.listesDeroulantes.length > 0) {
      for (i = 0; i < stor.listesDeroulantes.length; i++) {
        if (ds.Logique) {
          if ((stor.listesDeroulantes[i].getReponseIndex() === 1) !== stor.repj[i]) {
            stor.listesDeroulantes[i].corrige(false)
            ok = false
            stor.errJus = true
          }
        } else {
          if (parseInt(stor.listesDeroulantes[i].reponse()) !== stor.repj[i]) {
            stor.listesDeroulantes[i].corrige(false)
            ok = false
            stor.errJus = true
          }
        }
      }
    }
    if (!ok) return false
    if ((stor.laliste.getReponseIndex() === 1) !== stor.fRep()) {
      stor.laliste.corrige(false)
      return false
    }
    return true
  }

  function desactiveAll () {
    let i
    if (stor.listesDeroulantes.length > 0) {
      for (i = 0; i < stor.listesDeroulantes.length; i++) {
        stor.listesDeroulantes[i].disable()
      }
    }
    stor.laliste.disable()
  }

  function barrelesfo () {
    if (stor.listesDeroulantes.length > 0) {
      for (const liste of stor.listesDeroulantes) {
        liste.barreIfKo()
      }
    }
    stor.laliste.barreIfKo()
  }

  function mettouvert () {
    let i
    if (stor.listesDeroulantes.length > 0) {
      for (i = 0; i < stor.listesDeroulantes.length; i++) {
        stor.listesDeroulantes[i].corrige(true)
      }
    }
    stor.laliste.corrige(true)
  }
  function AffCorrection (bool) {
    j3pEmpty(stor.lesdivs.explications)
    if (stor.errJus) {
      stor.yaexplik = true
      j3pAffiche(stor.lesdivs.explications, null, 'La justification est fausse !\n')
    }

    if (bool) {
      barrelesfo()
      desactiveAll()
      stor.yaco = true
      stor.yadonc = false
      if (ds.Logique) {
        const hhh = addDefaultTable(stor.lesdivs.solution, 3, 2)
        if (ds.Operations) {
          var gr = '(' + stor.o1 + fop2(stor.op1) + stor.o2 + ') ' + fcomp2(stor.Lexo.type) + ' (' + stor.o3 + fop2(stor.op2) + stor.o4 + ') '
          gr = gr.replace(/n2/g, '$' + stor.n2 + '$')
          gr = '&nbsp; ' + gr.replace(/n/g, '$' + stor.n + '$') + ' &nbsp;'
          j3pAffiche(hhh[0][0], null, gr)
          stor.yadonc = true
          if (fcomp(stor.Lexo.type)(fop(stor.op1)(fval(stor.o1), fval(stor.o2)), fop(stor.op2)(fval(stor.o3), fval(stor.o4)))) {
            j3pAffiche(hhh[0][1], null, '&nbsp; est vrai ,')
            stor.yadonc = true
          } else {
            j3pAffiche(hhh[0][1], null, '&nbsp; est faux ,')
            stor.yadonc = true
          }
        } else {
          gr = stor.o1 + ' ' + fcomp2(stor.Lexo.type) + ' ' + stor.o2
          gr = gr.replace(/n2/g, '$' + stor.n2 + '$')
          gr = '&nbsp; ' + gr.replace(/n/g, '$' + stor.n + '$') + ' &nbsp;'
          j3pAffiche(hhh[0][0], null, gr)
          stor.yadonc = true
          if (fcomp(stor.Lexo.type)(fval(stor.o1), fval(stor.o2))) {
            j3pAffiche(hhh[0][1], null, '&nbsp; est vrai ,')
          } else {
            j3pAffiche(hhh[0][1], null, '&nbsp; est faux ,')
          }
        }
        if (stor.Lexo.logique !== 'operator_not') {
          hhh[1][0].style.height = '3px'
          if (ds.Operations) {
            gr = '(' + stor.o5 + fop2(stor.op3) + stor.o6 + ') ' + fcomp2(stor.Lexo.type2) + ' (' + stor.o7 + fop2(stor.op4) + stor.o8 + ') '
            gr = gr.replace(/n2/g, '$' + stor.n2 + '$')
            gr = '&nbsp; ' + gr.replace(/n/g, '$' + stor.n + '$') + ' &nbsp;'
            j3pAffiche(hhh[2][0], null, gr)
            stor.yadonc = true
            if (fcomp(stor.Lexo.type2)(fop(stor.op3)(fval(stor.o5), fval(stor.o6)), fop(stor.op4)(fval(stor.o7), fval(stor.o8)))) {
              j3pAffiche(hhh[2][1], null, '&nbsp; est vrai ,')
            } else {
              j3pAffiche(hhh[2][1], null, '&nbsp; est faux ,')
            }
          } else {
            gr = stor.o3 + ' ' + fcomp2(stor.Lexo.type2) + ' ' + stor.o4
            gr = gr.replace(/n2/g, '$' + stor.n2 + '$')
            gr = '&nbsp; ' + gr.replace(/n/g, '$' + stor.n + '$') + ' &nbsp;'
            j3pAffiche(hhh[2][0], null, gr)
            stor.yadonc = true
            if (fcomp(stor.Lexo.type2)(fval(stor.o3), fval(stor.o4))) {
              j3pAffiche(hhh[2][1], null, '&nbsp; est vrai ,')
            } else {
              j3pAffiche(hhh[2][1], null, '&nbsp; est faux ,')
            }
          }
        }
      } else {
        if (ds.Operations) {
          const ghju = addDefaultTable(stor.lesdivs.solution, 1, 3)
          j3pAffiche(ghju[0][1], null, '&nbsp; revient à &nbsp;')
          gr = '(' + stor.o1 + fop2(stor.op1) + stor.o2 + ') ' + fcomp2(stor.Lexo.type) + ' (' + stor.o3 + fop2(stor.op2) + stor.o4 + ') '
          gr = gr.replace(/n2/g, '$' + stor.n2 + '$')
          gr = '&nbsp; ' + gr.replace(/n/g, '$' + stor.n + '$') + ' &nbsp;'
          j3pAffiche(ghju[0][0], null, gr)
          const ghjfdu = addDefaultTable(ghju[0][2], 1, 5)
          stor.yadonc = true
          j3pAffiche(ghjfdu[0][0], null, ' &nbsp; ')
          j3pAffiche(ghjfdu[0][4], null, ' &nbsp; ')
          j3pAffiche(ghjfdu[0][2], null, ' &nbsp; ' + fcomp2(stor.Lexo.type) + ' &nbsp; ')
          j3pAffiche(ghjfdu[0][1], null, '$' + fop(stor.op1)(fval(stor.o1), fval(stor.o2)) + '$')
          j3pAffiche(ghjfdu[0][3], null, '$' + fop(stor.op2)(fval(stor.o3), fval(stor.o4)) + '$')
        } else {
          if ((stor.o1 + stor.o2).indexOf('x') !== -1 || (stor.o1 + stor.o2).indexOf('y') !== -1 || (stor.o1 + stor.o2).indexOf('z') !== -1) {
            stor.yadonc = true
            const ghju = addDefaultTable(stor.lesdivs.solution, 1, 3)
            j3pAffiche(ghju[0][1], null, '&nbsp; revient à &nbsp;')
            gr = stor.o1 + ' ' + fcomp2(stor.Lexo.type) + ' ' + stor.o2
            gr = gr.replace(/n2/g, '$' + stor.n2 + '$')
            gr = '&nbsp; ' + gr.replace(/n/g, '$' + stor.n + '$') + ' &nbsp;'
            j3pAffiche(ghju[0][0], null, gr)
            const ghjfdu = addDefaultTable(ghju[0][2], 1, 5)
            j3pAffiche(ghjfdu[0][0], null, ' &nbsp; ')
            j3pAffiche(ghjfdu[0][4], null, ' &nbsp; ')
            j3pAffiche(ghjfdu[0][2], null, ' &nbsp; ' + fcomp2(stor.Lexo.type) + ' &nbsp; ')
            j3pAffiche(ghjfdu[0][1], null, '$' + fval(stor.o1) + '$')
            j3pAffiche(ghjfdu[0][3], null, '$' + fval(stor.o2) + '$')
          }
        }
      }
      let af = '"vrai" .'
      if (!stor.fRep()) af = '"faux" .'
      const disdonc = (stor.yadonc) ? 'donc c' : 'C'
      j3pAffiche(stor.lesdivs.solution, null, disdonc + 'e programme va afficher ' + af)
    } else { me.afficheBoutonValider() }
  }

  function enonceMain () {
    if (me.etapeCourante === 1) {
      me.videLesZones()
      creeLesDiv()
      stor.Lexo = faisChoixExos()
    }
    poseQuestion()
    j3pEmpty(stor.lesdivs.correction)
    me.cacheBoutonSuite()
    me.finEnonce()
  }

  switch (this.etat) {
    case 'enonce':
      // code exécuté au lancement de la section
      if (me.debutDeLaSection) {
        initSection()
      } else {
        enonceMain()
      }
      break // case "enonce":

    case 'correction':
      // On teste si une réponse a été saisie
      stor.yaexplik = false
      stor.yaco = false
      stor.lesdivs.solution.classList.remove('correction')
      stor.lesdivs.explications.classList.remove('explique')
      j3pEmpty(stor.lesdivs.explications)

      if (yaReponse()) {
        // Bonne réponse
        if (isRepOk()) {
          stor.lesdivs.correction.style.color = this.styles.cbien
          stor.lesdivs.correction.innerHTML = cBien

          mettouvert()
          desactiveAll()

          j3pEmpty(stor.lesdivs.explikjus)
          j3pEmpty(stor.lesdivs.explikenc)

          this.cacheBoutonValider()
          this.score++

          this.typederreurs[0]++
          this.etat = 'navigation'
          this.sectionCourante()
        } else {
          // Pas de bonne réponse
          stor.lesdivs.correction.style.color = this.styles.cfaux
          stor.lesdivs.correction.innerHTML = cFaux
          if (this.isElapsed) {
            // A cause de la limite de temps :
            stor.lesdivs.correction.innerHTML += tempsDepasse + '<br>'
            this.typederreurs[10]++
            this.cacheBoutonValider()

            AffCorrection(true)

            this.etat = 'navigation'
            this.sectionCourante()
          } else {
            // Réponse fausse :
            if (me.essaiCourant < me.donneesSection.nbchances) {
              stor.lesdivs.correction.innerHTML += '<br>' + essaieEncore

              AffCorrection(false)

              this.typederreurs[1]++
            } else {
              // Erreur au nème essai
              this.cacheBoutonValider()

              AffCorrection(true)

              if (stor.encours === 'p') {
                stor.pose = false
                stor.encours = 't'
              }

              this.typederreurs[2]++
              this.etat = 'navigation'
              this.sectionCourante()
            }
          }
        }
      } else {
        // pas de réponse
        stor.lesdivs.correction.style.color = this.styles.cfaux
        stor.lesdivs.correction.innerHTML = 'Réponse incomplète !'
        if (stor.encours === 'm') {
          stor.lesdivs.correction.innerHTML = 'Réponse incomplète ! <br> Tu dois déplacer le point ' + stor.C + ' .'
        }
        me.afficheBoutonValider()
      }

      if (stor.yaexplik) stor.lesdivs.explications.classList.add('explique')
      if (stor.yaco) stor.lesdivs.solution.classList.add('correction')

      // Obligatoire
      this.finCorrection()
      break // case "correction":

    case 'navigation':
      if (!this.sectionTerminee()) {
        this.etat = 'enonce'
      }
      // Obligatoire
      this.finNavigation(true)
      break // case "navigation":
  }
}
