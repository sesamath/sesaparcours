import { j3pClone, j3pGetRandomInt, j3pShuffle } from 'src/legacy/core/functions'
import { getNewScratch } from 'src/legacy/outils/scratch/loaders'
import { j3pAffiche } from 'src/lib/mathquill/functions'
import { addDefaultTable } from 'src/legacy/themes/table'

export const params = {
  outils: ['mathquill'],
  /**
   * Ce qui est paramétrable dans l’éditeur de graphe
   * @type {LegacySectionParamsParametres}
   */
  parametres: [
    ['nbrepetitions', 4, 'entier', 'Nombre de répétitions de la section'],
    ['nbchances', 0, 'entier', 'Nombre d’essais possibles (illimité si 0)'],
    ['PasAPas', true, 'boolean', "<u>true</u>: L’élève peut lancer son programme en mode 'Pas à pas'."],
    ['Sequence', false, 'boolean', '<u>true</u>: Chaque bloc a une durée d’exécution d’environ 1 seconde.'],
    ['Completer', true, 'boolean', '<u>true</u>: Une partie du programme est déjà en place.'],
    ['Imbrication', 'les deux', 'liste', "<u>oui</u>: L’exercice peut demander un programme nécessitant l’imbrication de deux instructions 'SI'. <br><br> <u>non</u>: non.", ['oui', 'non', 'les deux']],
    ['Litteral', 'les deux', 'liste', '<u>oui</u>: On peut comparer un nombre contenu dans une variable. <br><br> <u>non</u>: non.', ['oui', 'non', 'les deux']],
    ['CacheTest', false, 'boolean', '<u>true</u>: L’élève ne voit pas l’action de son programme lors d la correction.']
  ]
}

/**
 * section bloksisimple
 * @this {Parcours}
 */
export default function main () {
  const me = this
  const stor = me.storage
  const ds = me.donneesSection
  let scratch = stor.scratch

  function tourne (tab) {
    const ret = []
    ret[0] = tab[1]
    ret[1] = tab[2]
    ret[2] = tab[0]
    return ret
  }

  async function initSection () {
    scratch = await getNewScratch()
    stor.scratch = scratch
    // pas de validation auto avec la touche entrée, il faut cliquer sur le drapeau vert
    me.validOnEnter = false // ex donneesSection.touche_entree
    scratch.initParams(me)
    scratch.testFlag = function () { }
    scratch.testFlagFin = function () {
      if (!scratch.yaeuLanceProg) return { ok: false, mess: 'Le programme n\'a pas démarré !' }
      let SAID = scratch.said()
      if (SAID.length === 0) SAID = ['']
      SAID = SAID.pop()
      const cop = j3pClone(scratch.params.scratchProgDonnees.valeursaleatoiressorties)
      const repUtil = Number(scratch.params.scratchProgDonnees.reponseDonnesAGetUS[scratch.params.scratchProgDonnees.reponseDonnesAGetUS.length - 1])
      if (scratch.params.scratchProgDonnees.reponseDonnesAGetUS.length > 1) {
        return { ok: false, mess: 'Il y a un problème avec le début du programme !' }
      }
      if (stor.lenb === 'y') {
        if (cop.length === 0) return { ok: false, mess: 'Il y a un problème avec le début du programme !' }
      }
      const nbACompare = (stor.lenb === 'y') ? cop[0].valeur : stor.lenb
      if (stor.pbencours % 2 !== 0) {
        if (stor.cv.indexOf('petit') !== -1) {
          if (SAID.indexOf('égal') !== -1) return { ok: false, mess: 'Scratch ne dit pas une phrase attendue !' }
          if (SAID.indexOf('grand') !== -1) return { ok: false, mess: 'Scratch ne dit pas une phrase attendue !' }
          if (repUtil < nbACompare) {
            if (SAID === '') return { ok: false, mess: 'Scratch aurait du dire<br>que ce nombre est plus petit que ' + nbACompare + ' !' }
          }
        }
        if (stor.cv.indexOf('égal') !== -1) {
          if (SAID.indexOf('petit') !== -1) return { ok: false, mess: 'Scratch ne dit pas une phrase attendue !' }
          if (SAID.indexOf('grand') !== -1) return { ok: false, mess: 'Scratch ne dit pas une phrase attendue !' }
          if (repUtil === nbACompare) {
            if (SAID === '') return { ok: false, mess: 'Scratch aurait du dire<br>que ce nombre est égal à ' + nbACompare + ' !' }
          }
        }
        if (stor.cv.indexOf('grand') !== -1) {
          if (SAID.indexOf('petit') !== -1) {
            return { ok: false, mess: 'Scratch ne dit pas une phrase attendue !' }
          }
          if (SAID.indexOf('égal') !== -1) {
            return { ok: false, mess: 'Scratch ne dit pas une phrase attendue !' }
          }
          if (repUtil > nbACompare) {
            if (SAID === '') return { ok: false, mess: 'Scratch aurait du dire<br>que ce nombre est plus grand que ' + nbACompare + ' !' }
          }
        }
      } else {
        if (SAID === '') return { ok: false, mess: 'Scratch aurait du dire quelque chose !' }
        if (repUtil > nbACompare) {
          if (SAID.indexOf('grand') === -1) return { ok: false, mess: 'Scratch aurait du dire que le nombre est plus grand que ' + nbACompare + ' !' }
        } else if (repUtil < nbACompare) {
          if (SAID.indexOf('petit') === -1) return { ok: false, mess: 'Scratch aurait du dire que le nombre est plus petit que ' + nbACompare + ' !' }
        } else {
          if (SAID.indexOf('égal') === -1) return { ok: false, mess: 'Scratch aurait du dire que le nombre est égal à ' + nbACompare + ' !' }
        }
      }
      return { ok: true, mess: 'Objectif atteint' }
    }
    scratch.scratchResetFlag = function () {
    }
    scratch.params.AutoriseSupprimeVariable = false
    scratch.params.AutoriseRenomeVariable = false
    scratch.params.forcealeatoire = true
    scratch.params.verifDeb = true

    me.construitStructurePage({ structure: 'presentation3', ratioGauche: 0.6, theme: 'zonesAvecImageDeFond' })
    me.afficheTitre('Ecrire une condition')

    stor.dejafait = []
    stor.dejafait2 = []
    stor.pbpos = []
    const pasOui = ['non', 'les deux']
    const pasNon = ['oui', 'les deux']
    if (pasOui.includes(ds.Litteral)) {
      if (pasOui.includes(ds.Imbrication)) stor.pbpos.push(1)
      if (pasNon.includes(ds.Imbrication)) stor.pbpos.push(2)
    }
    if (pasNon.includes(ds.Litteral)) {
      if (pasOui.includes(ds.Imbrication)) stor.pbpos.push(3)
      if (pasNon.includes(ds.Imbrication)) stor.pbpos.push(4)
    }
    stor.phbase = j3pShuffle(['tu as écrit un nombre égal à y', 'tu as écrit un nombre plus grand que y', 'tu as écrit un nombre plus petit que y'])
    enonceMain()
  }

  function enonceMain () {
    me.videLesZones()
    let consigne
    let ok
    let compt = 0
    do {
      compt++
      stor.pbencours = stor.pbpos[j3pGetRandomInt(0, stor.pbpos.length - 1)]
      ok = stor.dejafait.indexOf(stor.pbencours === -1)
    } while ((compt < 100) && (!ok))
    stor.dejafait.push(stor.pbencours)

    scratch.params.scratchProgDonnees.lesordres = []
    scratch.params.scratchProgDonnees.progdeb = []
    scratch.params.scratchProgDonnees.menu = []
    scratch.params.scratchProgDonnees.lesvarbonnes = []
    scratch.params.scratchProgDonnees.lesvarbonnes.push({
      nom: 'x',
      val: 0,
      id: 'aaaaaaa'
    })

    scratch.params.scratchProgDonnees.progdeb.push({
      type: 'Drapo',
      editable: 'false',
      deletable: 'false',
      movable: 'false',
      x: '20',
      y: '20'
    })

    stor.phbase = tourne(stor.phbase)
    stor.ph = scratch.copieDe(stor.phbase)

    if (stor.pbencours > 2) {
      scratch.params.scratchProgDonnees.lesvarbonnes.push({
        nom: 'y',
        val: 0,
        id: 'bbbbbbb'
      })
      scratch.params.scratchProgDonnees.progdeb.push({
        type: 'data_setvariableto',
        editable: 'false',
        deletable: 'false',
        movable: 'false',
        variable: [{
          nom: 'VARIABLE',
          nomvar: 'y'
        }],
        value: [{
          nom: 'VALUE',
          editable: 'false',
          deletable: 'false',
          movable: 'false',
          contenu: {
            type: 'operator_random',
            value: [{
              nom: 'NUM1',
              contenu: {
                valeur: '20',
                editable: 'false',
                deletable: 'false',
                movable: 'false'
              }
            }, {
              nom: 'NUM2',
              contenu: {
                valeur: 50,
                editable: 'false',
                deletable: 'false',
                movable: 'false'
              }
            }]
          }
        }]
      })
      scratch.params.scratchProgDonnees.lesordres.push({
        type: 'data_setvariableto',
        editable: 'false',
        deletable: 'false',
        movable: 'false',
        variable: [{
          nom: 'VARIABLE',
          nomvar: 'y'
        }],
        value: [{
          nom: 'VALUE',
          editable: 'false',
          deletable: 'false',
          movable: 'false',
          contenu: {
            type: 'operator_random',
            value: [{
              nom: 'NUM1',
              contenu: {
                valeur: '20',
                editable: 'false',
                deletable: 'false',
                movable: 'false'
              }
            }, {
              nom: 'NUM2',
              contenu: {
                valeur: 50,
                editable: 'false',
                deletable: 'false',
                movable: 'false'
              }
            }]
          }
        }]
      })
    } else {
      stor.nbcompare = j3pGetRandomInt(20, 50)
      stor.ph[0] = stor.ph[0].replace('y', stor.nbcompare)
      stor.ph[1] = stor.ph[1].replace('y', stor.nbcompare)
      stor.ph[2] = stor.ph[2].replace('y', stor.nbcompare)
    }

    scratch.params.scratchProgDonnees.progdeb.push({
      type: 'Get_us',
      value: [{
        nom: 'STRING',
        contenu: {
          valeur: 'Donne un nombre.',
          editable: 'false',
          deletable: 'false',
          movable: 'false'
        }
      }],
      editable: 'false',
      deletable: 'false',
      movable: 'false'
    })
    scratch.params.scratchProgDonnees.progdeb.push({
      type: 'data_setvariableto',
      editable: 'false',
      deletable: 'false',
      movable: 'false',
      variable: [{
        nom: 'VARIABLE',
        nomvar: 'x'
      }],
      value: [{
        nom: 'VALUE',
        editable: 'false',
        deletable: 'false',
        movable: 'false',
        contenu: {
          reponse: true,
          editable: 'false',
          deletable: 'false',
          movable: 'false'
        }
      }]
    })
    scratch.params.scratchProgDonnees.lesordres.push({
      type: 'Get_us',
      value: [{
        nom: 'STRING',
        contenu: {
          valeur: 'Donne un nombre.',
          editable: 'false',
          deletable: 'false',
          movable: 'false'
        }
      }],
      editable: 'false',
      deletable: 'false',
      movable: 'false'
    })
    scratch.params.scratchProgDonnees.lesordres.push({
      type: 'data_setvariableto',
      editable: 'false',
      deletable: 'false',
      movable: 'false',
      variable: [{
        nom: 'VARIABLE',
        nomvar: 'x'
      }],
      value: [{
        nom: 'VALUE',
        editable: 'false',
        deletable: 'false',
        movable: 'false',
        contenu: {
          reponse: true,
          editable: 'false',
          deletable: 'false',
          movable: 'false'
        }
      }]
    })

    if (ds.Completer) {
      if (stor.pbencours % 2 !== 0) {
        scratch.params.scratchProgDonnees.progdeb.push({
          type: 'control_if',
          editable: 'false',
          deletable: 'false',
          movable: 'false',
          statement: [{
            nom: 'SUBSTACK',
            contenu: [{
              type: 'dire',
              editable: 'false',
              deletable: 'false',
              movable: 'false',
              value: [{
                nom: 'DELTA',
                contenu: {
                  valeur: stor.ph[0],
                  editable: 'false',
                  deletable: 'false',
                  movable: 'false'
                }
              }, {
                nom: 'TEMPS',
                contenu: {
                  valeur: '2',
                  editable: 'false',
                  deletable: 'false',
                  movable: 'false'
                }
              }]
            }]
          }]
        })
      } else {
        scratch.params.scratchProgDonnees.progdeb.push({
          type: 'control_if_else',
          editable: 'false',
          deletable: 'false',
          movable: 'false',
          statement: [{
            nom: 'SUBSTACK',
            contenu: [{
              type: 'dire',
              editable: 'false',
              deletable: 'false',
              movable: 'false',
              value: [{
                nom: 'DELTA',
                contenu: {
                  valeur: stor.ph[0],
                  editable: 'false',
                  deletable: 'false',
                  movable: 'false'
                }
              }, {
                nom: 'TEMPS',
                contenu: {
                  valeur: '2',
                  editable: 'false',
                  deletable: 'false',
                  movable: 'false'
                }
              }]
            }]
          }, {
            nom: 'SUBSTACK2',
            contenu: [{
              type: 'control_if_else',
              editable: 'false',
              deletable: 'false',
              movable: 'false',
              statement: [{
                nom: 'SUBSTACK',
                contenu: [{
                  type: 'dire2',
                  editable: 'false',
                  deletable: 'false',
                  movable: 'false',
                  value: [{
                    nom: 'DELTA',
                    contenu: {
                      valeur: stor.ph[1],
                      editable: 'false',
                      deletable: 'false',
                      movable: 'false'
                    }
                  }, {
                    nom: 'TEMPS',
                    contenu: {
                      valeur: '2',
                      editable: 'false',
                      deletable: 'false',
                      movable: 'false'
                    }
                  }]
                }]
              }, {
                nom: 'SUBSTACK2',
                contenu: [{
                  type: 'dire3',
                  editable: 'false',
                  deletable: 'false',
                  movable: 'false',
                  value: [{
                    nom: 'DELTA',
                    contenu: {
                      valeur: stor.ph[2],
                      editable: 'false',
                      deletable: 'false',
                      movable: 'false'
                    }
                  }, {
                    nom: 'TEMPS',
                    contenu: {
                      valeur: '2',
                      editable: 'false',
                      deletable: 'false',
                      movable: 'false'
                    }
                  }]
                }]
              }]
            }]
          }]
        })
      }
    } else {
      scratch.params.scratchProgDonnees.menu.push({
        type: 'catégorie',
        nom: 'DIRE',
        coul1: '#9966FF',
        coul2: '#774DCB',
        contenu: []
      })
      scratch.params.scratchProgDonnees.menu[0].contenu.push({
        type: 'dire',
        value: [{
          nom: 'DELTA',
          contenu: {
            valeur: stor.ph[0],
            editable: 'false',
            deletable: 'false',
            movable: 'false'
          }
        }, {
          nom: 'TEMPS',
          contenu: {
            valeur: 2,
            editable: 'false',
            deletable: 'false',
            movable: 'false'
          }
        }]
      })
      scratch.params.scratchProgDonnees.menu.push({
        type: 'catégorie',
        nom: 'CONTROLES',
        coul1: '#FFAB19',
        coul2: '#CF8B17',
        contenu: []
      })

      if (stor.pbencours % 2 !== 1) {
        scratch.params.scratchProgDonnees.menu[0].contenu.push({
          type: 'dire2',
          value: [{
            nom: 'DELTA',
            contenu: {
              valeur: stor.ph[1],
              editable: 'false',
              deletable: 'false',
              movable: 'false'
            }
          }, {
            nom: 'TEMPS',
            contenu: {
              valeur: 2,
              editable: 'false',
              deletable: 'false',
              movable: 'false'
            }
          }]
        })
        scratch.params.scratchProgDonnees.menu[0].contenu.push({
          type: 'dire3',
          value: [{
            nom: 'DELTA',
            contenu: {
              valeur: stor.ph[2],
              editable: 'false',
              deletable: 'false',
              movable: 'false'
            }
          }, {
            nom: 'TEMPS',
            contenu: {
              valeur: 2,
              editable: 'false',
              deletable: 'false',
              movable: 'false'
            }
          }]
        })
        scratch.params.scratchProgDonnees.menu[1].contenu.push({ type: 'control_if_else' })
      } else {
        scratch.params.scratchProgDonnees.menu[1].contenu.push({ type: 'control_if' })
      }
    }

    scratch.params.scratchProgDonnees.menu.push({
      type: 'catégorie',
      nom: 'OPERATEURS',
      coul1: '#40BF4A',
      coul2: '#389438',
      contenu: []
    })
    scratch.params.scratchProgDonnees.menu[scratch.params.scratchProgDonnees.menu.length - 1].contenu.push({
      type: 'operator_equals',
      value: [{
        nom: 'OPERAND1',
        contenu: { valeur: ' ' }
      }, {
        nom: 'OPERAND2',
        contenu: { valeur: ' ' }
      }]
    })
    scratch.params.scratchProgDonnees.menu[scratch.params.scratchProgDonnees.menu.length - 1].contenu.push({
      type: 'operator_gt',
      value: [{
        nom: 'OPERAND1',
        contenu: { valeur: ' ' }
      }, {
        nom: 'OPERAND2',
        contenu: { valeur: ' ' }
      }]
    })
    scratch.params.scratchProgDonnees.menu[scratch.params.scratchProgDonnees.menu.length - 1].contenu.push({
      type: 'operator_lt',
      value: [{
        nom: 'OPERAND1',
        contenu: { valeur: ' ' }
      }, {
        nom: 'OPERAND2',
        contenu: { valeur: ' ' }
      }]
    })
    scratch.params.scratchProgDonnees.menu.push({
      type: 'catégorie',
      nom: 'VARIABLE',
      coul1: '#FF8C1A',
      coul2: '#DB6E00',
      contenu: []
    })
    scratch.params.scratchProgDonnees.menu[scratch.params.scratchProgDonnees.menu.length - 1].contenu.push({
      type: 'data_variable',
      variable: [{
        nom: 'VARIABLE',
        nomvar: 'x'
      }]
    })
    if (stor.pbencours > 2) {
      scratch.params.scratchProgDonnees.menu[scratch.params.scratchProgDonnees.menu.length - 1].contenu.push({
        type: 'data_variable',
        variable: [{
          nom: 'VARIABLE',
          nomvar: 'y'
        }]
      })
    }

    scratch.params.blokmenulimite.push({
      type: 'dire',
      nb: 1
    })
    scratch.params.blokmenulimite.push({
      type: 'dire2',
      nb: 1
    })
    scratch.params.blokmenulimite.push({
      type: 'dire3',
      nb: 1
    })

    let op1
    let op2
    let gh1
    let cv
    if (stor.ph[0].indexOf('égal') !== -1) {
      op1 = 'operator_equals'
      cv = 'égal à'
    }
    if (stor.ph[0].indexOf('grand') !== -1) {
      op1 = 'operator_gt'
      cv = 'plus grand que'
    }
    if (stor.ph[0].indexOf('petit') !== -1) {
      op1 = 'operator_lt'
      cv = 'plus petit que'
    }
    if (stor.ph[1].indexOf('égal') !== -1) {
      op2 = 'operator_equals'
    }
    if (stor.ph[1].indexOf('grand') !== -1) {
      op2 = 'operator_gt'
    }
    if (stor.ph[1].indexOf('petit') !== -1) {
      op2 = 'operator_lt'
    }

    let lenb
    if (stor.pbencours > 2) {
      lenb = 'y'
      gh1 = {
        type: 'data_variable',
        variable: [{
          nom: 'VARIABLE',
          nomvar: 'y',
          id: 'bbbbbbb'
        }]
      }
    } else {
      gh1 = { valeur: stor.nbcompare }
      lenb = stor.nbcompare
    }

    const cond1 = {
      type: op1,
      value: [{
        nom: 'OPERAND1',
        contenu: {
          type: 'data_variable',
          variable: [{
            nom: 'VARIABLE',
            nomvar: 'x',
            id: 'aaaaaaa'
          }]
        }
      }, {
        nom: 'OPERAND2',
        contenu: gh1
      }]
    }
    const cond2 = {
      type: op2,
      value: [{
        nom: 'OPERAND1',
        contenu: {
          type: 'data_variable',
          variable: [{
            nom: 'VARIABLE',
            nomvar: 'x',
            id: 'aaaaaaa'
          }]
        }
      }, {
        nom: 'OPERAND2',
        contenu: gh1
      }]
    }

    if (stor.pbencours % 2 !== 0) {
      scratch.params.scratchProgDonnees.lesordres.push({
        type: 'control_if',
        value: [{
          nom: 'CONDITION',
          contenu: cond1
        }],
        editable: 'false',
        deletable: 'false',
        movable: 'false',
        statement: [{
          nom: 'SUBSTACK',
          contenu: [{
            type: 'dire',
            editable: 'false',
            deletable: 'false',
            movable: 'false',
            value: [{
              nom: 'DELTA',
              contenu: {
                valeur: stor.ph[0],
                editable: 'false',
                deletable: 'false',
                movable: 'false'
              }
            }, {
              nom: 'TEMPS',
              contenu: {
                valeur: '2',
                editable: 'false',
                deletable: 'false',
                movable: 'false'
              }
            }]
          }]
        }]
      })
      consigne = '<b>Programme Scratch pour qu’il prévienne l’utilisateur </b> <br>quand le nombre qu’il a entré est ' + cv + ' ' + lenb + '.'
    } else {
      consigne = '<b>Programme Scratch pour qu’il compare </b><br>le nombre donné par l’utilisateur à ' + lenb + '.'
      scratch.params.scratchProgDonnees.lesordres.push({
        type: 'control_if_else',
        value: [{
          nom: 'CONDITION',
          contenu: cond1
        }],
        editable: 'false',
        deletable: 'false',
        movable: 'false',
        statement: [{
          nom: 'SUBSTACK',
          contenu: [{
            type: 'dire',
            editable: 'false',
            deletable: 'false',
            movable: 'false',
            value: [{
              nom: 'DELTA',
              contenu: {
                valeur: stor.ph[0],
                editable: 'false',
                deletable: 'false',
                movable: 'false'
              }
            }, {
              nom: 'TEMPS',
              contenu: {
                valeur: '2',
                editable: 'false',
                deletable: 'false',
                movable: 'false'
              }
            }]
          }]
        }, {
          nom: 'SUBSTACK2',
          contenu: [{
            type: 'control_if_else',
            value: [{
              nom: 'CONDITION',
              contenu: cond2
            }],
            editable: 'false',
            deletable: 'false',
            movable: 'false',
            statement: [{
              nom: 'SUBSTACK',
              contenu: [{
                type: 'dire2',
                editable: 'false',
                deletable: 'false',
                movable: 'false',
                value: [{
                  nom: 'DELTA',
                  contenu: {
                    valeur: stor.ph[1],
                    editable: 'false',
                    deletable: 'false',
                    movable: 'false'
                  }
                }, {
                  nom: 'TEMPS',
                  contenu: {
                    valeur: '2',
                    editable: 'false',
                    deletable: 'false',
                    movable: 'false'
                  }
                }]
              }]
            }, {
              nom: 'SUBSTACK2',
              contenu: [{
                type: 'dire3',
                editable: 'false',
                deletable: 'false',
                movable: 'false',
                value: [{
                  nom: 'DELTA',
                  contenu: {
                    valeur: stor.ph[2],
                    editable: 'false',
                    deletable: 'false',
                    movable: 'false'
                  }
                }, {
                  nom: 'TEMPS',
                  contenu: {
                    valeur: '2',
                    editable: 'false',
                    deletable: 'false',
                    movable: 'false'
                  }
                }]
              }]
            }]
          }]
        }]
      })
    }

    stor.cv = cv
    stor.lenb = lenb
    scratch.params.scratchProgDonnees.Lutins[0].coxbase = 10
    scratch.params.scratchProgDonnees.Lutins[0].cox = 10
    scratch.enonce([], {
      pourInject: { StartScale: 0.6 },
      modeCorrection: {
        type: 'SansComparaison',
        aVerif: [
          {
            quoi: 'QuandAvertir',
            quand: 'apres'
          },
          {
            quoi: 'AnnuleSiAvert',
            quand: false
          }]
      },
      dirigeProg: { sequence: ds.Sequence, pasApas: ds.PasAPas, cacheTest: ds.CacheTest }
    })
    const yy = addDefaultTable(scratch.lesdivs.enonceG, 1, 1)[0][0]
    j3pAffiche(yy, null, consigne)

    scratch.params.scratchProgDonnees.listValPos = []
    for (let i = 19; i < 52; i++) {
      scratch.params.scratchProgDonnees.listValPos.push([String(i)])
    }

    const bornes = stor.scratch.retrouveBorne()
    const verifAle = []
    for (let j = 0; j < bornes.length; j++) {
      verifAle[j] = []
      verifAle[j].push({ val: bornes[j].borneInf, idBlok: bornes[j].idBlok })
      verifAle[j].push({ val: bornes[j].borneSup, idBlok: bornes[j].idBlok })
      verifAle[j].push({ val: Math.floor((bornes[j].borneSup + bornes[j].borneInf) / 2), idBlok: bornes[j].idBlok })
    }
    scratch.params.valideAlea = (verifAle.length > 0) ? verifAle : undefined

    me.finEnonce()
    // on ne veut pas du bouton OK, c’est le drapeau vert qui fait office de validation
    me.cacheBoutonValider()
  } // enonceMain

  switch (this.etat) {
    case 'enonce':
      if (this.debutDeLaSection) {
        initSection().catch(error => {
          console.error(error)
          this.notif(Error('L’initialisation de Scratch a échoué, impossible de continuer'))
        })
      } else {
        enonceMain()
      }
      break // case "enonce":

    case 'correction':
      try {
        const ret = scratch.prepCorrec()
        const xmlDoc2 = ret.xmlDoc2
        const xmlText = ret.xmlText

        if (scratch.params.scratchProgDonnees.repEleve) {
          // Bonne réponse
          this.score++
          scratch.corrigeGood(xmlDoc2, xmlText)
        } else {
          // Pas de bonne réponse
          scratch.corrige(xmlDoc2, xmlText)
        }
        // on veut être rappelé dans l’état navigation
        this.finCorrection('navigation', true)
      } catch (error) {
        console.error(error)
      }
      break // case "correction":

    case 'navigation':
      if (this.sectionTerminee()) {
        scratch.lesdivs.BoutonsSuite.appendChild(this.buttonsElts.sectionSuivante)
        this.buttonsElts.sectionSuivante.addEventListener('click', () => {
          scratch.params.grosStop = true
        })
      } else {
        this.etat = 'enonce'
        scratch.lesdivs.BoutonsSuite.appendChild(this.buttonsElts.suite)
        this.buttonsElts.suite.addEventListener('click', () => {
          scratch.params.grosStop = true
        })
      }
      // Obligatoire
      this.finNavigation()

      break // case "navigation":
  }
}
