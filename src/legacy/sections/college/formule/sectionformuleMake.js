import { j3pAddContent, j3pAddElt, j3pClone, j3pGetRandomBool, j3pShowError, j3pShuffle } from 'src/legacy/core/functions'
import { addDefaultTable } from 'src/legacy/themes/table'
import getDefaultProgramme from './defaultProgramme'
import Zoneclick from 'src/legacy/outils/zoneclick/Zoneclick'
import { j3pAffiche } from 'src/lib/mathquill/functions'
import { getMtgCore } from 'src/lib/outils/mathgraph/index'
import { j3pCreeSVG } from 'src/legacy/core/functionsSvg'
import BulleAide from 'src/legacy/outils/bulleAide/BulleAide'
import { placeJouerSon, placeJouerSonFormule } from 'src/legacy/outils/zoneStyleMathquill/functions'
import textesGeneriques from 'src/lib/core/textes'

const { cBien, cFaux, essaieEncore, reponseIncomplete, regardeCorrection, tempsDepasse } = textesGeneriques

async function initEditor (container, values, btn) {
  const { default: editor } = await import('./editorProgramme.js')
  return editor(container, values, btn) // la fct qui prend un conteneur et les params comme arguments
}

/**
 * Ajouter une description ici
 * @fileOverview
 * @author Tommy Barroy <t.barroy@laposte.net>
 * @since 2017-10
 */
export const params = {
  outils: ['mathquill'],
  /**
   * Ce qui est paramétrable dans l’éditeur de graphe
   * @type {LegacySectionParamsParametres}
   */
  parametres: [
    ['nbrepetitions', 9, 'entier', 'Nombre de répétitions de la section'],
    ['limite', 60, 'entier', 'Temps maximum pour choisir (sinon ce sera le premier choix, mettre 0 pour ne pas limiter)'],
    ['NbChoix', 6, 'entier', 'Nombre de choix possibles (entre 3 et 12)'],
    ['Formules', JSON.stringify(getDefaultProgramme()), 'multiEditor', 'Json figure de base et figure attendue.', initEditor],
    ['ObligeToutes', 'Oui', 'liste', '<u>Oui</u>: L’élève doit sélectionner toutes les formules <BR> false : Une bonne réponse suffit.', ['Oui', 'Non']],
    ['theme', 'standard', 'liste', 'Modèle pour l’affichage', ['standard', 'zonesAvecImageDeFond']]
  ],
  // dans le cas d’une section QUANTITATIVE
  pe: [
  ]
}

/**
 * section choix
 * @this {Parcours}
 */
export default function main () {
  const me = this
  const ds = me.donneesSection
  const stor = me.storage

  function initSection () {
    // chargement mtg
    me.construitStructurePage('presentation3')
    try {
      stor.Formules = JSON.parse(ds.Formules)
      const aComp = []
      for (let i = 0; i < stor.Formules.liste.length; i++) {
        let aCompte = true
        if (typeof stor.Formules.liste[i].gauche !== 'string') {
          if (stor.Formules.liste[i].gauche.type === 'clone') {
            aCompte = false
            stor.Formules.liste[i].gauche = j3pClone(aComp[stor.Formules.liste[i].gauche.num])
          }
        }
        if (aCompte) aComp.push(j3pClone(stor.Formules.liste[i].gauche))
        aCompte = true
        if (typeof stor.Formules.liste[i].droite !== 'string') {
          if (stor.Formules.liste[i].droite.type === 'clone') {
            aCompte = false
            stor.Formules.liste[i].droite = j3pClone(aComp[stor.Formules.liste[i].droite.num])
          }
        }
        if (aCompte) aComp.push(j3pClone(stor.Formules.liste[i].droite))
        for (let j = 0; j < stor.Formules.liste[i].pieges.length; j++) {
          aCompte = true
          if (typeof stor.Formules.liste[i].pieges[j].cont !== 'string') {
            if (stor.Formules.liste[i].pieges[j].type === 'clone') {
              aCompte = false
              stor.Formules.liste[i].pieges[j] = j3pClone(aComp[stor.Formules.liste[i].pieges[j].num])
            }
          }
          if (aCompte) aComp.push(j3pClone(stor.Formules.liste[i].pieges[j]))
        }
      }
    } catch (error) {
      console.error(error)
      j3pShowError('Vignettes mises en paramètre invalide')
      stor.Formules = getDefaultProgramme()
    }
    stor.exosPossibles1 = j3pShuffle(j3pClone(stor.Formules.liste))
    const pourTri1 = stor.exosPossibles1.map(el => el.gauche)
    const pourTri = [faisId(pourTri1[0])]
    pourTri1.splice(0, 1)
    for (let i = 0; i < pourTri1.length; i++) {
      pourTri1[i] = faisId(pourTri1[i])
    }
    stor.exosPossibles = [j3pClone(stor.exosPossibles1[0])]
    stor.exosPossibles1.splice(0, 1)
    for (let i = 0; i < pourTri1.length; i++) {
      if (pourTri.indexOf(pourTri1[i]) === -1) {
        stor.exosPossibles.splice(0, 0, j3pClone(stor.exosPossibles1[i]))
      } else {
        stor.exosPossibles.push(j3pClone(stor.exosPossibles1[i]))
      }
      pourTri.push(pourTri1[i])
    }
    stor.lesExos = []
    for (let i = 0; i < ds.nbitems; i++) {
      stor.lesExos.splice(0, 0, j3pClone(stor.exosPossibles[i % stor.exosPossibles.length]))
    }
    stor.ammettre = []
    const lesZid = []
    stor.exosPossibles.forEach(el => {
      if (!stor.Formules.quegauche) {
        if (lesZid.indexOf(faisId(el.gauche)) === -1) {
          stor.ammettre.push(el.gauche)
          lesZid.push(faisId(el.gauche))
        }
      }
      if (lesZid.indexOf(faisId(el.droite)) === -1) {
        stor.ammettre.push(el.droite)
        lesZid.push(faisId(el.droite))
      }
    })
    stor.lesExos = j3pShuffle(stor.lesExos)
    let titre = stor.Formules.titre
    if (!titre || titre === '') titre = ' '
    me.afficheTitre('Vignettes: ' + titre)
    getMtgCore({ withMathJax: true })
      .then(
        // success
        (mtgAppLecteur) => {
          stor.mtgAppLecteur = mtgAppLecteur
          enonceMain()
        },
        // failure
        (error) => {
          j3pShowError(error, { message: 'mathgraph n’est pas correctement chargé', mustNotify: true })
        })
      // plantage dans le code de success
      .catch(j3pShowError)
  }

  function creeLesDiv () {
    stor.lesdiv = {}
    stor.lesdiv.conteneur = j3pAddElt(me.zonesElts.MG, 'div', {
      style: me.styles.etendre('toutpetit.enonce', { padding: '10px' })
    })
    const tab1 = addDefaultTable(stor.lesdiv.conteneur, 1, 3)
    j3pAddContent(tab1[0][1], '&nbsp;&nbsp;&nbsp;')
    stor.lesdiv.consigne = tab1[0][0]
    stor.lesdiv.travail = addDefaultTable(stor.lesdiv.conteneur, 1, 1)[0][0]
    stor.lesdiv.explications = j3pAddElt(stor.lesdiv.conteneur, 'div')
    stor.lesdiv.correction = tab1[0][2]
    stor.lesdiv.explications.style.color = me.styles.cfaux
    stor.divmatgraA = addDefaultTable(stor.lesdiv.conteneur, 1, 1)[0][0]
    stor.divmatgraB = addDefaultTable(stor.lesdiv.conteneur, 1, 1)[0][0]
    stor.divmatgraA.style.display = 'none'
    stor.divmatgraB.style.display = 'none'
    stor.lesdiv.consigne.classList.add('enonce')
    stor.lesdiv.travail.classList.add('travail')
    me.zonesElts.MG.classList.add('fond')
  }

  function enonceMain () {
    me.videLesZones()
    creeLesDiv()
    stor.lex = stor.lesExos.pop()
    const gauchedroite = (stor.Formules.quegauche) ? true : j3pGetRandomBool()
    const adonne = (gauchedroite) ? j3pClone(stor.lex.gauche) : j3pClone(stor.lex.droite)
    const acache = (!gauchedroite) ? j3pClone(stor.lex.gauche) : j3pClone(stor.lex.droite)
    stor.listezoneclik = [acache]
    stor.comPiege = []
    const licli = [faisId(acache), faisId(adonne)]
    let yaMG = false
    if (typeof adonne !== 'string') {
      if (adonne.type === 'mathgraph' || adonne.type === undefined) yaMG = true
    }
    stor.ammettre2 = j3pClone(j3pShuffle(stor.ammettre))
    stor.listIdPiege = []
    for (let i = 0; i < stor.lex.pieges.length; i++) {
      stor.listezoneclik.push(stor.lex.pieges[i])
      stor.listIdPiege.push(faisId(stor.lex.pieges[i]))
      licli.push(faisId(stor.lex.pieges[i]))
      if (stor.lex.piegesCom) {
        if (stor.lex.piegesCom[i]) {
          if (stor.lex.piegesCom[i] !== '') {
            stor.comPiege.push({ id: faisId(stor.lex.pieges[i]), com: stor.lex.piegesCom[i] })
          }
        }
      }
    }
    for (let i = stor.lex.pieges.length + 1; i < ds.NbChoix; i++) {
      let ok = false
      do {
        if (stor.ammettre2.length === 0) break
        const am = j3pClone(stor.ammettre2.pop())
        if (licli.indexOf(faisId(am)) === -1) {
          stor.listezoneclik.push(am)
          licli.push(faisId(am))
          ok = true
        }
      } while (!ok)
    }
    stor.listezoneclik = j3pShuffle(stor.listezoneclik)
    stor.acons = j3pClone(stor.listezoneclik)
    for (let i = 0; i < stor.acons.length; i++) {
      if (typeof stor.acons[i] !== 'string') {
        stor.acons[i] = ''
        if (stor.acons[i].type === 'mathgraph' || stor.acons[i].type === undefined) yaMG = true
      }
    }
    stor.zonesclick = new Zoneclick(stor.lesdiv.travail, stor.acons, { couleur: '#000000', dispo: 'carre', colonne: 3, reponsesmultiples: true, afaire: '', affiche: true, maxwidthcol: 250 })

    if (yaMG) stor.mtgAppLecteur.removeAllDoc()
    const tabCons = addDefaultTable(stor.lesdiv.consigne, 1, 3)
    j3pAffiche(tabCons[0][0], null, '<i>' + stor.Formules.consigne + '</i>')
    tabCons[0][0].style.whiteSpace = 'normal'
    j3pAddContent(tabCons[0][1], '&nbsp;&nbsp;&nbsp;')
    if (typeof adonne === 'string') {
      j3pAffiche(tabCons[0][2], null, adonne)
    } else {
      tabCons[0][0].style.verticalAlign = 'top'
      const ui = addDefaultTable(tabCons[0][2], 1, 1)[0][0]
      ui.style.padding = '0px'
      if (adonne.type === 'mathgraph' || adonne.type === undefined) {
        const mui = j3pCreeSVG(ui, { id: 'fhdsjkqfhdgauche', width: 37 + adonne.content.width * 340, height: 54 + adonne.content.height * 253 })
        stor.mtgAppLecteur.addDoc('fhdsjkqfhdgauche', adonne.content.txtFigure, true)
        mui.style.border = '1px solid black'
      }
      if (adonne.type === 'image') {
        ui.style.width = adonne.content.width + 'px'
        ui.style.height = adonne.content.height + 'px'
        j3pAddElt(ui, 'img', '', { src: adonne.content.txtFigure, width: adonne.content.width, height: adonne.content.height })
      }
      if (adonne.type === 'son') {
        ui.style.border = '1px solid black'
        ui.style.borderRadius = '5px'
        ui.style.padding = '5px'
        ui.style.paddingTop = '0px'
        ui.style.background = '#aeef59'
        ui.style.textAlign = 'center'
        placeJouerSon(ui, adonne.content, stor, 'txtFigure')
      }
    }
    if (ds.ObligeToutes === 'Oui') {
      const tabCons2 = addDefaultTable(stor.lesdiv.consigne, 1, 1)
      j3pAffiche(tabCons2[0][0], null, '<b> Tu dois sélectionner toutes les réponses correctes possibles.</b>')
    }
    /// metimage
    for (let i = 0; i < stor.listezoneclik.length; i++) {
      if (typeof stor.listezoneclik[i] !== 'string') {
        if (stor.listezoneclik[i].type === 'image') {
          const ui = addDefaultTable(stor.zonesclick.jegarde[i][3], 1, 1)[0][0]
          j3pAddElt(ui, 'img', '', { src: stor.listezoneclik[i].content.txtFigure, width: stor.listezoneclik[i].content.width, height: stor.listezoneclik[i].content.height })
          ui.style.width = stor.listezoneclik[i].content.width + 'px'
          ui.style.height = stor.listezoneclik[i].content.height + 'px'
        }
        if (stor.listezoneclik[i].type === 'son') {
          placeJouerSonFormule(stor.zonesclick.jegarde[i][3], stor.listezoneclik[i].content, stor, 'txtFigure')
        }
      }
    }

    if (yaMG) {
      for (let i = 0; i < stor.listezoneclik.length; i++) {
        if (typeof stor.listezoneclik[i] !== 'string') {
          if (stor.listezoneclik[i].type === 'mathgraph' || stor.listezoneclik[i].type === undefined) {
            const ui = addDefaultTable(stor.zonesclick.jegarde[i][3], 1, 1)[0][0]
            j3pCreeSVG(ui, { id: 'fhdsjkqfhdksj' + i + 'gauche', width: 37 + stor.listezoneclik[i].content.width * 340, height: 54 + stor.listezoneclik[i].content.height * 253 })
            stor.mtgAppLecteur.addDoc('fhdsjkqfhdksj' + i + 'gauche', stor.listezoneclik[i].content.txtFigure, true)
          }
        }
      }
      stor.mtgAppLecteur.calculateAndDisplayAll(true)
    }
    stor.ellrepPossible = [faisId(adonne), faisId(acache)]
    let aaj
    do {
      aaj = false
      stor.Formules.liste.forEach(f => {
        if (stor.ellrepPossible.includes(faisId(f.gauche)) && !stor.listIdPiege.includes(faisId(f.gauche))) {
          if (!stor.ellrepPossible.includes(faisId(f.droite)) && !stor.listIdPiege.includes(faisId(f.droite))) {
            stor.ellrepPossible.push(faisId(f.droite))
            aaj = true
          }
        }
        if (stor.ellrepPossible.includes(faisId(f.droite)) && !stor.listIdPiege.includes(faisId(f.droite))) {
          if (!stor.ellrepPossible.includes(faisId(f.gauche)) && !stor.listIdPiege.includes(faisId(f.gauche))) {
            stor.ellrepPossible.push(faisId(f.gauche))
            aaj = true
          }
        }
      })
    } while (aaj)

    stor.lesbonnesreponses = []
    for (let i = 0; i < stor.listezoneclik.length; i++) {
      if (stor.ellrepPossible.includes(faisId(stor.listezoneclik[i]))) {
        stor.lesbonnesreponses.push(i)
      }
    }

    me.finEnonce()
  }

  function faisId (s) {
    if (typeof s === 'string') return s
    return s.content.txtFigure
  }
  function isRepOk () {
    // cette fonction renvoie true ou false si du moins une réponse a été saisie
    // (dans le cas contraire, elle n’est pas appelée
    // elle est appelée dans la partie "correction" de la section

    stor.formuleTrouvee = false
    stor.formuleEnTrop = false
    stor.formuleRatee = false
    stor.casesCorrigees = []
    stor.formulesCorrigees = []
    stor.casesRattrappees = []

    // cherche si ya formule trouvee ou entrop
    for (let i = 0; i < stor.zonesclick.reponsenb.length; i++) {
      const rep = faisId(stor.listezoneclik[stor.zonesclick.reponsenb[i]])
      if (stor.ellrepPossible.includes(rep)) {
        stor.formuleTrouvee = true
      } else {
        stor.formuleEnTrop = true
        stor.casesCorrigees.push(stor.zonesclick.reponsenb[i])
        stor.formulesCorrigees.push(i)
      }
    }

    // cherche si ya formuleRatee
    for (let i = 0; i < stor.zonesclick.nb; i++) {
      if ((!stor.zonesclick.sel[i]) && ((stor.ellrepPossible.includes(faisId(stor.listezoneclik[i]))))) {
        stor.formuleRatee = true
        stor.casesRattrappees.push(i)
      }
    }
    if (ds.ObligeToutes === 'Oui') {
      return ((stor.formuleTrouvee) && (!stor.formuleEnTrop) && (!stor.formuleRatee))
    } else {
      return ((stor.formuleTrouvee) && (!stor.formuleEnTrop))
    }
  }

  function yareponse () {
    if (me.isElapsed) { return true }
    return (stor.zonesclick.reponsenb.length > 0)
  }

  switch (this.etat) {
    case 'enonce':
      // code exécuté au lancement de la section
      if (me.debutDeLaSection) {
        initSection()
      } else {
        stor.raz = true
        enonceMain()
      }
      break // case "enonce":

    case 'correction':
      // On teste si une réponse a été saisie
      if (!yareponse()) {
        stor.lesdiv.correction.style.color = this.styles.cfaux
        stor.lesdiv.correction.innerHTML = reponseIncomplete
        this.afficheBoutonValider()
      } else {
        // Bonne réponse
        if (isRepOk()) {
          stor.lesdiv.correction.style.color = this.styles.cbien
          stor.lesdiv.correction.innerHTML = cBien
          this._stopTimer()
          this.score++
          this.typederreurs[0]++
          this.cacheBoutonValider()

          /// /////////////////////////
          stor.zonesclick.corrige(true)
          stor.zonesclick.disable()
          /// //////////////////////////

          this.etat = 'navigation'
          this.sectionCourante()
        } else {
          // Pas de bonne réponse
          stor.lesdiv.correction.style.color = this.styles.cfaux

          // A cause de la limite de temps :
          if (this.isElapsed) { // limite de temps
            this._stopTimer()
            stor.lesdiv.correction.innerHTML = tempsDepasse
            stor.lesdiv.correction.innerHTML += '<br>' + '<br>' + regardeCorrection
            this.typederreurs[10]++
            this.cacheBoutonValider()

            /// ///////////////////////
            stor.zonesclick.corrige(stor.lesbonnesreponses, true)
            stor.zonesclick.disable()

            /// ///////////////////

            this.etat = 'navigation'
            this.sectionCourante()
          } else {
            stor.lesdiv.correction.innerHTML = cFaux

            if (this.essaiCourant < ds.nbchances) {
              stor.lesdiv.correction.innerHTML += '<br>' + essaieEncore
              this.typederreurs[1]++
              // indication éventuelle ici

              stor.zonesclick.corrigelesfaux(stor.lesbonnesreponses)

              /// ///////////////////////////
            } else {
              stor.lesdiv.correction.innerHTML += '<br>' + regardeCorrection
              stor.zonesclick.corrige(stor.lesbonnesreponses, true)
              stor.zonesclick.disable()
              for (let i = 0; i < stor.zonesclick.sel.length; i++) {
                if (stor.zonesclick.sel[i]) {
                  if (stor.lesbonnesreponses.indexOf(i) === -1) {
                    for (let k = 0; k < stor.comPiege.length; k++) {
                      if (stor.comPiege[k].id === faisId(stor.listezoneclik[i])) {
                        stor.bull = new BulleAide(stor.zonesclick.jegarde[i][1], stor.comPiege[k].com, { place: 5 })
                        break
                      }
                    }
                  }
                }
              }
              /// ////////////////////////////////

              this.typederreurs[2]++
              this.etat = 'navigation'
              this.sectionCourante()
            }
          }
        }
      }
      // Obligatoire
      this.finCorrection()
      break // case "correction":

    case 'navigation':
      if (!this.sectionTerminee()) {
        this.etat = 'enonce'
      }
      this.finNavigation()
      break // case "navigation":
  }
}
