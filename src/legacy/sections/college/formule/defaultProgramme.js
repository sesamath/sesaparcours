export default function getDefaultProgramme () {
  return {
    titre: 'Connaitre les carrés',
    consigne: 'Expression égale à',
    quegauche: true,
    liste: [{
      gauche: '$sin(-x)$',
      droite: '$-sin(x)$',
      pieges: ['$sin(x)$', '$cos(x)$', '$cos(-x)$']
    },
    {
      gauche: '$cos(-x)$',
      droite: '$cos(x)$',
      pieges: ['$-cos(x)$', '$sin(x)$', '$sin(-x)$']
    },
    {
      gauche: '$sin(\\pi+x)$',
      droite: '$-sin(x)$',
      pieges: ['$sin(x)$', '$cos(x)$', '$cos(-x)$']
    },
    {
      gauche: '$cos(\\pi+x)$',
      droite: '$-cos(x)$',
      pieges: ['$cos(x)$', '$sin(x)$', '$sin(-x)$']
    }
    ]
  }
}
