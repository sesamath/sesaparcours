import { j3pAddElt, j3pAjouteBouton, j3pArrondi, j3pEmpty, j3pGetRandomInt, j3pNotify, j3pShuffle } from 'src/legacy/core/functions'
import { j3pCreeFenetres, j3pToggleFenetres } from 'src/legacy/core/functionsJqDialog'
import ListeDeroulante from 'src/legacy/outils/listeDeroulante/ListeDeroulante'
import { verifNombreBienEcrit } from 'src/legacy/outils/fonctions/ecritureNombre'
import { getPe, initCptPe } from 'src/legacy/sections/college/helperTommy'
import { j3pAffiche } from 'src/lib/mathquill/functions'
import { addDefaultTable } from 'src/legacy/themes/table'
import ZoneStyleMathquill1 from 'src/legacy/outils/zoneStyleMathquill/ZoneStyleMathquill1'
import textesGeneriques from 'src/lib/core/textes'

const { cBien, cFaux, essaieEncore, reponseIncomplete, tempsDepasse } = textesGeneriques

/**
 * Ajouter une description ici
 * @fileOverview
 * @author Tommy Barroy <t.barroy@laposte.net>
 * @since 2017-10
*/
export const params = {
  outils: ['mathquill', 'calculatrice'],
  /**
   * Ce qui est paramétrable dans l’éditeur de graphe
   * @type {LegacySectionParamsParametres}
   */
  parametres: [
    ['nbrepetitions', 3, 'entier', 'Nombre de répétitions de la section'],
    ['calculatrice', false, 'boolean', 'true pour avoir une calculatrice disponible'],
    ['limite', 0, 'entier', 'Temps disponible par répétition, en secondes'],
    ['entier', false, 'boolean', '<u>true</u>&nbsp;:  Les nombres de départ sont focément entiers'],
    ['borne', 0, 'entier', '<i>Uniquement quand <b>entier</b> est à true</i> <br><br> Valeur absolue max des nombres utilisés'],
    ['operations', '+x÷', 'liste', 'opérations possibles', ['+', 'x', '÷', '+x', '+÷', 'x÷', '+x÷']],
    ['reponse', 'tout', 'liste', '<u>Signe</u>&nbsp;: L’élève doit d’abord donner le signe du résultat. <br><br> <u>Distance</u>&nbsp;: Lélève doit d’abord donner la distance à zéro du résultat. <br><br> <u>Résultat</u>&nbsp;: L’élève doit donner le résultat', ['tout', 'Signe', 'Distance', 'Résultat', 'Signe_Distance', 'Signe_Résultat', 'Distance_Résultat']],
    ['simplifie', false, 'boolean', '<u>true</u>&nbsp;:  L’élève ne doit pas écrire le signe + au résutat.'],
    // le modèle gère la compatibilité ascendante avec l’ancien paramètre MiseEnPagePlusClaire
    ['theme', 'standard', 'liste', 'Modèle pour l’affichage', ['standard', 'zonesAvecImageDeFond']]

  ],

  pe: [
    {
      pe_1: 'somme',
      pe_2: 'produit',
      pe_3: 'quotient',
      pe_4: 'signe',
      pe_5: 'distance',
      pe_6: 'écriture'
    }
  ]
}

/**
 * section relatifsuneop
 * @this {Parcours}
 */
export default function main () {
  const me = this
  const ds = me.donneesSection
  const stor = me.storage

  function initSection () {
    let i
    initCptPe(ds, stor)
    me.validOnEnter = false // ex donneesSection.touche_entree

    // fix changement : => ÷, il reste des opérations avec : dans les graphes
    if (typeof ds.operations === 'string') ds.operations = ds.operations.replace(':', '÷')
    else j3pNotify(Error(`ds.operations n’est pas une string : ${typeof ds.operations} ${ds.operations}`))

    // Construction de la page
    me.construitStructurePage('presentation1bis')

    ds.Aff = []
    let nbetapes = 0
    if ((ds.reponse === 'tout') || (ds.reponse.indexOf('Signe') !== -1)) {
      nbetapes++
      ds.Aff.push('S')
    }
    if ((ds.reponse === 'tout') || (ds.reponse.indexOf('Distance') !== -1)) {
      nbetapes++
      ds.Aff.push('D')
    }
    if ((ds.reponse === 'tout') || (ds.reponse.indexOf('Résultat') !== -1)) {
      nbetapes++
      ds.Aff.push('R')
    }

    me.surcharge({ nbetapes })

    ds.lesExos = []
    let lp = j3pShuffle(copiede(ds.operations))
    for (i = 0; i < ds.nbitems; i++) {
      ds.lesExos[i] = { op: lp[i % lp.length] }
    }
    ds.lesExos = j3pShuffle(ds.lesExos)

    lp = ['++', '--', '+-', '-+']
    lp = j3pShuffle(lp)
    for (i = 0; i < ds.nbitems; i++) {
      ds.lesExos[i].signes = lp[i % lp.length]
    }
    ds.lesExos = j3pShuffle(ds.lesExos)

    lp = [true, false]
    for (i = 0; i < ds.nbitems; i++) {
      ds.lesExos[i].cache = lp[i % lp.length]
    }
    ds.lesExos = j3pShuffle(ds.lesExos)

    stor.restric = '0123456789+- '
    if (!ds.entier) stor.restric += '.,'

    me.typederreurs = [0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0]

    me.score = 0
    let tt = 'Calculer avec des nombres relatifs'
    if (ds.operations.length === 1) {
      switch (ds.operations) {
        case '+': tt = 'Somme algébrique'
          break
        case 'x': tt = 'Produit de nombres relatifs'
          break
        case '÷': tt = 'Quotient de nombres relatifs'
          break
      }
    }

    me.afficheTitre(tt)
    enonceMain()
  }
  function toggleCalc () {
    j3pToggleFenetres('Calculatrice')
  }
  function faisChoixExos () {
    stor.clvspe = false
    const e = ds.lesExos.pop()
    let gigu = 99
    switch (e.op) {
      case '+' :
        e.n1 = j3pArrondi(j3pGetRandomInt(1, 99) / 10, 1)
        e.n2 = j3pArrondi(j3pGetRandomInt(1, 99) / 10, 1)
        if (ds.entier) {
          if (ds.borne !== 0) gigu = Math.abs(ds.borne)
          e.n1 = j3pGetRandomInt(1, gigu)
          e.n2 = j3pGetRandomInt(1, gigu)
        }
        break
      case 'x' :
        e.n1 = j3pArrondi(j3pGetRandomInt(1, 13) / 10, 1)
        e.n2 = j3pArrondi(j3pGetRandomInt(1, 13) / 10, 1)
        if (ds.entier) {
          if (ds.borne !== 0) gigu = Math.abs(ds.borne)
          e.n1 = j3pGetRandomInt(2, gigu)
          e.n2 = j3pGetRandomInt(2, gigu)
        }
        break
      case '÷': {
        e.n2 = j3pArrondi(j3pGetRandomInt(1, 13) / 10, 1)
        gigu = 13
        let u = j3pGetRandomInt(2, 13)
        if (ds.entier) {
          if (ds.borne !== 0) gigu = Math.abs(ds.borne)
          e.n2 = j3pGetRandomInt(1, gigu)
          u = j3pGetRandomInt(2, gigu)
        }
        e.n1 = j3pArrondi(e.n2 * u, 1)
        break
      }
    }
    if (e.signes[0] === '-') e.n1 = -e.n1
    if (e.signes[1] === '-') e.n2 = -e.n2
    switch (e.op) {
      case '+': stor.repat = j3pArrondi(e.n1 + e.n2, 1)
        break
      case 'x': stor.repat = j3pArrondi(e.n1 * e.n2, 2)
        break
      case '÷': stor.repat = j3pArrondi(e.n1 / e.n2, 0)
    }
    stor.affcalc = e.n1 + ' '
    switch (e.op) {
      case '+': if (e.n2 > 0) { stor.affcalc += '+ ' } else { stor.affcalc += '- ' }
        stor.affcalc += Math.abs(e.n2)
        break
      case 'x': stor.affcalc += '\\times '
        if (e.n2 > 0) { stor.affcalc += e.n2 } else { stor.affcalc += '(' + e.n2 + ')' }
        break
      case '÷': stor.affcalc += '\\div '
        if (e.n2 > 0) { stor.affcalc += e.n2 } else { stor.affcalc += '(' + e.n2 + ')' }
        break
      default:
        throw Error(`Opération non gérée : ${e.op}`)
    }
    j3pAffiche(stor.lesdiv.consigne, null, '<b>On considère l’expression $A = ' + stor.affcalc + '$ </b>\n')
    if ((ds.simplifie) && ((ds.Aff.length > 1) || (ds.Aff[0] !== 'S'))) {
      j3pAffiche(stor.lesdiv.consigne, null, '<i>Les réponses doivent être données sous forme simplifiée</i> \n')
    }
    j3pAffiche(stor.lesdiv.consigne, null, '\n')
    stor.encours = ds.Aff[ds.Aff.length - 1]
    stor.focoS = false
    stor.focoD = false
    return e
  }
  function poseQuestion () {
    let i = ds.Aff.indexOf(stor.encours) + 1
    if (i === ds.Aff.length) i = 0
    stor.encours = ds.Aff[i]
    stor.lesdiv.explications.classList.remove('explique')
    stor.lesdiv.solution.classList.remove('correction')
    switch (stor.encours) {
      case 'S': poseSigne()
        break
      case 'D': poseDistance()
        break
      case 'R': poseResultat()
        break
    }
  }
  function creeLesDiv () {
    stor.lesdiv = {}
    stor.lesdiv.conteneur = j3pAddElt(me.zonesElts.MG, 'div', {
      style: me.styles.etendre('toutpetit.enonce', { padding: '10px' })
    })
    // calculatrice
    if (me.donneesSection.calculatrice) {
      me.fenetresjq = [{ name: 'Calculatrice', title: 'Calculatrice' }]
      j3pCreeFenetres(me)
      const button = j3pAjouteBouton(stor.lesdiv.conteneur, toggleCalc, { className: 'MepBoutons', value: 'Calculatrice' })
      button.style.float = 'right'
    }
    stor.lesdiv.consigne = addDefaultTable(stor.lesdiv.conteneur, 1, 1)[0][0]
    stor.lesdiv.travail = addDefaultTable(stor.lesdiv.conteneur, 1, 1)[0][0]
    stor.lesdiv.zonesigne = addDefaultTable(stor.lesdiv.conteneur, 1, 1)[0][0]
    stor.lesdiv.zonedistance = addDefaultTable(stor.lesdiv.conteneur, 1, 1)[0][0]
    stor.lesdiv.zoneresultat = addDefaultTable(stor.lesdiv.conteneur, 1, 1)[0][0]
    stor.lesdiv.explications = j3pAddElt(stor.lesdiv.conteneur, 'div')
    stor.lesdiv.solution = j3pAddElt(stor.lesdiv.conteneur, 'div')
    stor.lesdiv.correction = j3pAddElt(me.zonesElts.MD, 'div', {
      style: me.styles.etendre('toutpetit.enonce', { padding: '10px' })
    })
    stor.lesdiv.explications.style.color = me.styles.cfaux
    stor.lesdiv.solution.style.color = me.styles.toutpetit.correction.color
    me.zonesElts.MG.classList.add('fond')
  }

  function poseSigne () {
    me.cacheBoutonValider()
    stor.lesdiv.zonesigne.classList.add('travail')
    stor.tab1 = addDefaultTable(stor.lesdiv.zonesigne, 1, 2)
    j3pAffiche(stor.tab1[0][0], null, '$A$ est un nombre &nbsp;')
    stor.listeSi = ListeDeroulante.create(stor.tab1[0][1], ['Choisir', ' positif. ', ' négatif. '], { centre: true, onChange: me.afficheBoutonValider.bind(me) })
  }
  function poseDistance () {
    me.afficheBoutonValider()
    stor.lesdiv.zonedistance.classList.add('travail')
    stor.lesdiv.explications.innerHTML = ''
    stor.lesdiv.correction.innerHTML = ''
    stor.lesdiv.solution.innerHTML = ''
    if (stor.focoS) {
      stor.focoS = false
      j3pEmpty(stor.tab1[0][1])
      let lesigne = 'positif.'
      if (stor.repat < 0) lesigne = 'négatif.'
      j3pAffiche(stor.tab1[0][1], null, lesigne)
      stor.tab1[0][1].style.color = me.styles.petit.correction.color
    }
    stor.tabEZ = addDefaultTable(stor.lesdiv.zonedistance, 1, 2)
    j3pAffiche(stor.tabEZ[0][0], null, 'La distance à zéro de $A$ est égale à &nbsp;')
    stor.listeDist = new ZoneStyleMathquill1(stor.tabEZ[0][1], { restric: stor.restric, limitenb: 3, limite: 10, enter: me.sectionCourante.bind(me) })
  }
  function poseResultat () {
    me.afficheBoutonValider()
    stor.lesdiv.explications.innerHTML = ''
    stor.lesdiv.correction.innerHTML = ''
    stor.lesdiv.solution.innerHTML = ''
    stor.lesdiv.zoneresultat.classList.add('travail')
    if (stor.focoS) {
      stor.focoS = false
      j3pEmpty(stor.tab1[0][1])
      let lesigne = 'positif.'
      if (stor.repat < 0) lesigne = 'négatif.'
      j3pAffiche(stor.tab1[0][1], null, lesigne)
      stor.tab1[0][1].style.color = me.styles.petit.correction.color
    }
    if (stor.focoD) {
      stor.focoS = false
      j3pEmpty(stor.tabEZ[0][1])
      j3pAffiche(stor.tabEZ[0][1], null, '$' + String(Math.abs(stor.repat)).replace('.', ',') + '$')
      stor.tabEZ[0][1].style.color = me.styles.petit.correction.color
    }
    const tabPo = addDefaultTable(stor.lesdiv.zoneresultat, 1, 2)
    j3pAffiche(tabPo[0][0], null, '$A$ = &nbsp;')
    stor.zonerep = new ZoneStyleMathquill1(tabPo[0][1], { restric: stor.restric, limitenb: 3, limite: 10, enter: me.sectionCourante.bind(me) })
  }

  function copiede (tab) {
    let i
    const ret = []
    for (i = 0; i < tab.length; i++) {
      ret[i] = tab[i]
    }
    return ret
  }

  function yaReponse () {
    if (me.isElapsed) return true
    if (stor.encours === 'D') {
      if (stor.listeDist.reponse() === '') {
        stor.listeDist.focus()
        return false
      }
    }
    if (stor.encours === 'R') {
      if (stor.zonerep.reponse() === '') {
        stor.zonerep.focus()
        return false
      }
    }
    return true
  }
  function isRepOk () {
    stor.errSi = false
    stor.errEcri = false
    stor.errNegDist = false
    stor.errDist = false
    stor.errSIrep = false
    stor.errRep = false
    stor.errPlus = false
    passetoutvert()
    if (stor.encours === 'S') {
      if ((stor.repat >= 0) !== (stor.listeSi.reponse === ' positif. ') || (!stor.listeSi.changed)) {
        stor.errSi = true
        stor.listeSi.corrige(false)
        return false
      }
      return true
    }
    if (stor.encours === 'D') {
      let rep = stor.listeDist.reponse()
      if (rep === '') {
        stor.listeDist.corrige(false)
        return false
      }
      const diskoi = verifNombreBienEcrit(rep, false, true)
      if (!diskoi.good) {
        stor.errEcri = true
        stor.errEcrikoi = diskoi.remede
        stor.listeDist.corrige(false)
        return false
      }
      rep = diskoi.nb
      if (rep < 0) {
        stor.errNegDist = true
        stor.listeDist.corrige(false)
        return false
      }
      if (rep !== Math.abs(stor.repat)) {
        stor.errDist = true
        stor.listeDist.corrige(false)
        return false
      }
      if ((stor.listeDist.reponse().indexOf('+') !== -1) && (ds.simplifie)) {
        stor.errPlus = true
        stor.listeDist.corrige(false)
        return false
      }
      return true
    }
    if (stor.encours === 'R') {
      let rep = stor.zonerep.reponse()
      if (rep === '') {
        stor.zonerep.corrige(false)
        return false
      }
      const diskoi = verifNombreBienEcrit(rep, false, true)
      if (!diskoi.good) {
        stor.errEcri = true
        stor.errEcrikoi = diskoi.remede
        stor.zonerep.corrige(false)
        return false
      }
      rep = diskoi.nb
      if ((rep < 0) !== (stor.repat < 0)) {
        stor.errSIrep = true
        stor.zonerep.corrige(false)
        return false
      }
      if (rep !== stor.repat) {
        stor.errRep = true
        stor.zonerep.corrige(false)
        return false
      }
      if ((stor.zonerep.reponse().indexOf('+') !== -1) && (ds.simplifie)) {
        stor.errPlus = true
        stor.zonerep.corrige(false)
        return false
      }
      return true
    }
  }
  function desactivezone () {
    if (stor.encours === 'S') {
      stor.listeSi.disable()
    }
    if (stor.encours === 'D') {
      stor.listeDist.disable()
    }
    if (stor.encours === 'R') {
      stor.zonerep.disable()
    }
  }
  function passetoutvert () {
    if (stor.encours === 'S') {
      stor.listeSi.corrige(true)
    }
    if (stor.encours === 'D') {
      stor.listeDist.corrige(true)
    }
    if (stor.encours === 'R') {
      stor.zonerep.corrige(true)
    }
  }
  function barrelesfo () {
    if (stor.encours === 'S') {
      stor.listeSi.barre()
    }
    if (stor.encours === 'D') {
      stor.listeDist.barre()
    }
    if (stor.encours === 'R') {
      stor.zonerep.barre()
    }
  }
  function AffCorrection (bool) {
    if (stor.errEcri) {
      j3pAffiche(stor.lesdiv.explications, null, '<u>Erreur d’écriture&nbsp;:</u> , \n' + stor.errEcrikoi + '\n')
      stor.yaexplik = true
      stor.compteurPe6++
    }
    if (stor.errNegDist) {
      j3pAffiche(stor.lesdiv.explications, null, 'Une distance à zéro est toujours positive !')
      stor.yaexplik = true
    }
    if (stor.errSIrep) {
      stor.yaexplik = true
      if (ds.Aff.indexOf('S') === -1) {
        stor.errSi = true
      } else {
        j3pAffiche(stor.lesdiv.explications, null, 'Le signe a été déterminé juste avant !')
      }
    }
    if (stor.errRep) {
      stor.yaexplik = true
      if (ds.Aff.indexOf('D') === -1) {
        stor.errDist = true
      } else {
        j3pAffiche(stor.lesdiv.explications, null, 'La distance à zéro a été déterminée juste avant !')
      }
    }
    if (stor.errSi) {
      stor.yaexplik = true
      stor.compteurPe4++
      switch (stor.Lexo.op) {
        case '+': {
          const ki = (Math.abs(stor.Lexo.n1) > Math.abs(stor.Lexo.n2)) ? stor.Lexo.n1 : stor.Lexo.n2
          j3pAffiche(stor.lesdiv.explications, null, 'Il faut prendre le signe de $' + ki + '$ , \ncar il a la plus grande distance à $0$ ! \n')
          break
        }
        case 'x':
          if (stor.Lexo.signes[0] === stor.Lexo.signes[1]) {
            j3pAffiche(stor.lesdiv.explications, null, 'Le produit de deux facteurs de même signe est positif ! \n')
          } else {
            j3pAffiche(stor.lesdiv.explications, null, 'Le produit de deux facteurs de signes contraires est négatif ! \n')
          }
          break
        case '÷':
          if (stor.Lexo.signes[0] === stor.Lexo.signes[1]) {
            j3pAffiche(stor.lesdiv.explications, null, 'Le quotient de deux nombres relatifs de même signe est positif ! \n')
          } else {
            j3pAffiche(stor.lesdiv.explications, null, 'Le quotient de deux nombres relatifs de signes contraires est négatif ! \n')
          }
          break
      }
    }
    if (stor.errDist) {
      stor.yaexplik = true
      stor.compteurPe5++
      switch (stor.Lexo.op) {
        case '+':
          if (stor.Lexo.signes[0] === stor.Lexo.signes[1]) {
            j3pAffiche(stor.lesdiv.explications, null, 'Quand les termes ont le même signe, on <u>ajoute</u> les distances à zéro ! \n')
          } else {
            j3pAffiche(stor.lesdiv.explications, null, 'Quand les termes ont des signes contraires, on <u>soustrait</u> les distances à zéro ! \n')
          }
          break
        case 'x':
          j3pAffiche(stor.lesdiv.explications, null, 'Il suffit de multiplier les distances à zéro ! \n')
          break
        case '÷':
          j3pAffiche(stor.lesdiv.explications, null, 'Il suffit de diviser les distances à zéro ! \n')
          break
      }
    }
    if (stor.errPlus) {
      stor.yaexplik = true
      j3pAffiche(stor.lesdiv.explications, null, 'Inutile d’écrire le signe + !')
      stor.compteurPe6++
    }
    switch (stor.Lexo.op) {
      case '+':
        stor.compteurPe1++
        break
      case 'x':
        stor.compteurPe2++
        break
      case '÷':
        stor.compteurPe3++
        break
    }

    if (bool) {
      stor.yaco = true
      desactivezone()
      barrelesfo()
      if (stor.encours === 'S') {
        let hjk = 'positif.'
        if (stor.repat < 0) hjk = 'négatif.'
        j3pAffiche(stor.lesdiv.solution, null, '$A$ est un nombre ' + hjk)
        stor.focoS = true
      }
      if (stor.encours === 'D') {
        j3pAffiche(stor.lesdiv.solution, null, 'La distance à zéro de $A$ est égale à $' + String(Math.abs(stor.repat)).replace('.', ',') + '$')
        stor.focoD = true
      }
      if (stor.encours === 'R') {
        j3pAffiche(stor.lesdiv.solution, null, ' $A$ = $' + String(stor.repat).replace('.', ',') + '$')
      }
    } else { me.afficheBoutonValider() }
  }
  function enonceMain () {
    if (me.etapeCourante === 1) {
      me.videLesZones()
      creeLesDiv()
      stor.Lexo = faisChoixExos()
      stor.lesdiv.consigne.classList.add('enonce')
    }
    poseQuestion()
    me.finEnonce()
  }

  switch (this.etat) {
    case 'enonce':
      // code exécuté au lancement de la section
      if (me.debutDeLaSection) {
        initSection()
      } else {
        enonceMain()
      }
      break // case "enonce":

    case 'correction':
      // On teste si une réponse a été saisie
      stor.yaexplik = false
      stor.lesdiv.explications.innerHTML = ''
      stor.lesdiv.correction.innerHTML = ''
      stor.yaco = false

      if (yaReponse()) {
        // Bonne réponse
        if (isRepOk()) {
          this._stopTimer()
          this.score++
          stor.lesdiv.correction.style.color = this.styles.cbien
          stor.lesdiv.correction.innerHTML = cBien

          desactivezone()
          passetoutvert()

          this.typederreurs[0]++
          this.cacheBoutonValider()
          this.etat = 'navigation'
          this.sectionCourante()
        } else {
          // Pas de bonne réponse
          stor.lesdiv.correction.style.color = this.styles.cfaux
          stor.lesdiv.correction.innerHTML = cFaux
          if (this.isElapsed) {
            // A cause de la limite de temps :
            this._stopTimer()

            stor.lesdiv.correction.innerHTML += tempsDepasse + '<BR>'
            this.typederreurs[10]++
            this.cacheBoutonValider()

            AffCorrection(true)

            this.etat = 'navigation'
            this.sectionCourante()
          } else {
            // Réponse fausse :
            if (me.essaiCourant < me.donneesSection.nbchances) {
              stor.lesdiv.correction.innerHTML += '<br>' + essaieEncore

              AffCorrection(false)

              this.typederreurs[1]++
            } else {
              // Erreur au nème essai
              this._stopTimer()
              this.cacheBoutonValider()

              AffCorrection(true)

              this.typederreurs[2]++
              this.etat = 'navigation'
              this.sectionCourante()
            }
          }
        }
      } else {
        // pas de réponse
        stor.lesdiv.correction.style.color = this.styles.cfaux
        stor.lesdiv.correction.innerHTML = reponseIncomplete
        me.afficheBoutonValider()
      }

      if (stor.yaexplik) stor.lesdiv.explications.classList.add('explique')
      if (stor.yaco) stor.lesdiv.solution.classList.add('correction')

      // Obligatoire
      this.finCorrection()
      break // case "correction":

    case 'navigation':
      if (this.sectionTerminee()) {
        if (ds.operations.length === 1) {
          // @todo expliquer pourquoi il faut les remettre à 0 ? Ils ont pu être incrémenté dans ce cas ?
          stor.compteurPe1 = stor.compteurPe2 = stor.compteurPe3 = 0
        }
        if (ds.Aff.length === 1) {
          if (ds.Aff[0] === 'S') stor.compteurPe4 = 0
          if (ds.Aff[0] === 'D') stor.compteurPe5 = 0
        }
        this.parcours.pe = getPe(ds, stor)
      } else {
        this.etat = 'enonce'
      }
      this.finNavigation()
      break // case "navigation":
  }
}
