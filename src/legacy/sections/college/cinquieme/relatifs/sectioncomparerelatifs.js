import { j3pAddElt, j3pArrondi, j3pGetRandomBool, j3pGetRandomInt, j3pShuffle } from 'src/legacy/core/functions'
import ListeDeroulante from 'src/legacy/outils/listeDeroulante/ListeDeroulante'
import { getPe, initCptPe } from 'src/legacy/sections/college/helperTommy'
import { j3pAffiche } from 'src/lib/mathquill/functions'
import { addDefaultTable } from 'src/legacy/themes/table'
import textesGeneriques from 'src/lib/core/textes'

const { cBien, cFaux, essaieEncore, reponseIncomplete, tempsDepasse } = textesGeneriques

/**
 * Ajouter une description ici
 * @fileOverview
 * @author Tommy Barroy <t.barroy@laposte.net>
 * @since 2017-10
*/
export const params = {
  outils: ['mathquill'],
  /**
   * Ce qui est paramétrable dans l’éditeur de graphe
   * @type {LegacySectionParamsParametres}
   */
  parametres: [
    ['nbrepetitions', 9, 'entier', 'Nombre de répétitions de la section'],
    ['Type_Nombres', 'Tous', 'liste', '<u>Entiers</u>:  L’exercice peut proposer des nombres entiers.  <br> <br> <u>Décimaux</u>: L’exercice peut proposer des nombres décimaux. <br> <br> <u>fractions</u>: L’exercice peut proposer des fractions (de même dénominateur). ', ['Entiers', 'Décimaux', 'Fractions', 'Entiers_Décimaux', 'Entiers_fractions', 'Décimaux_Fractions', 'Tous']],
    ['Positifs', false, 'boolean', '<u>true</u>: Les nombres sont forcément positifs.'],
    // le modèle gère la compatibilité ascendante avec l’ancien paramètre MiseEnPagePlusClaire
    ['theme', 'standard', 'liste', 'Modèle pour l’affichage', ['standard', 'zonesAvecImageDeFond']]

  ],
  // dans le cas d’une section QUANTITATIVE
  pe: [
    { pe_1: 'Zeros_inutiles' }
  ]
}

/**
 * section comparerelatifs
 * @this {Parcours}
 */
export default function main () {
  const me = this
  const ds = me.donneesSection
  const stor = me.storage

  function creeLesDiv () {
    stor.lesdiv = {}
    stor.lesdiv.conteneur = j3pAddElt(me.zonesElts.MG, 'div', {
      style: me.styles.etendre('toutpetit.enonce', { padding: '10px' })
    })
    stor.lesdiv.consigne = addDefaultTable(stor.lesdiv.conteneur, 1, 1)[0][0]
    stor.lesdiv.travail = addDefaultTable(stor.lesdiv.conteneur, 1, 1)[0][0]
    stor.lesdiv.explications = j3pAddElt(stor.lesdiv.conteneur, 'div')
    stor.lesdiv.solution = j3pAddElt(stor.lesdiv.conteneur, 'div')
    stor.lesdiv.correction = j3pAddElt(me.zonesElts.MD, 'div', {
      style: me.styles.etendre('toutpetit.enonce', { padding: '10px' })
    })
    stor.lesdiv.solution.style.color = me.styles.petit.correction.color
    stor.lesdiv.explications.style.color = me.styles.cfaux
  }
  function poseQuestion () {
    if (stor.Lexo.type === 'Fractions') {
      stor.b1 = '\\frac{' + stor.n1.Strnum + '}{' + String(stor.n1.den) + '}'
      stor.b2 = '\\frac{' + stor.n2.Strnum + '}{' + String(stor.n2.den) + '}'
    } else {
      stor.b1 = stor.Stringn1
      stor.b2 = stor.Stringn2
    }
    j3pAffiche(stor.lesdiv.consigne, null, '<b>Compare </b>  $' + stor.b1 + '$ et $' + stor.b2 + '$ \n\n')
    const tt = addDefaultTable(stor.lesdiv.travail, 1, 3)
    j3pAffiche(tt[0][0], null, '$' + stor.b1 + '$&nbsp;')
    j3pAffiche(tt[0][2], null, '&nbsp;$' + stor.b2 + '$')
    stor.liste1 = ListeDeroulante.create(tt[0][1], ['...', '$>$', '$=$', '$<$'], { centre: true, alignmathquill: true })
  }
  function suiv (j) {
    switch (j) {
      case 'ent': return 'mmdec'
      case 'mmdec': return 'dec'
      case 'dec': return '2'
      case '2': return 'ent'
    }
  }
  function faisChoixExos () {
    const e = ds.lesExos.pop()
    let rangi
    let rang
    let pent
    let pent2
    let rangi2

    switch (e.type) {
      case 'Entiers':
        stor.n1 = j3pGetRandomInt(1, 100)
        stor.n2 = j3pGetRandomInt(1, 100)
        if (stor.n1 === stor.n2) {
          stor.n2++
        }
        stor.Stringn1 = String(stor.n1)
        stor.Stringn2 = String(stor.n2)
        break
      case 'Décimaux':
        if (e.place !== '=') {
          stor.cas = suiv(stor.cas)

          switch (stor.cas) {
            case 'ent': // partie diff
              stor.n1 = j3pGetRandomInt(1, 100)
              stor.n2 = j3pGetRandomInt(1, 100)
              if (stor.n1 === stor.n2) {
                stor.n2++
              }
              rangi = j3pGetRandomInt(1, 3)
              rang = Math.pow(10, rangi)
              pent = j3pGetRandomInt(1, rang - 1)
              if (String(pent)[String(pent).length - 1] === '0') { pent++ }
              pent = pent / rang
              stor.n1 = j3pArrondi(stor.n1 + pent, rangi)
              rangi = j3pGetRandomInt(1, 3)
              rang = Math.pow(10, rangi)
              pent = j3pGetRandomInt(1, rang - 1)
              if (String(pent)[String(pent).length - 1] === '0') { pent++ }
              pent = pent / rang
              stor.n2 = j3pArrondi(stor.n2 + pent, rangi)
              stor.Stringn1 = String(stor.n1).replace('.', ',')
              stor.Stringn2 = String(stor.n2).replace('.', ',')
              break
            case 'mmdec': // mm partie ent , mm nb chiffres dec
              stor.n1 = j3pGetRandomInt(1, 100)
              stor.n2 = stor.n1
              rangi = j3pGetRandomInt(1, 3)
              rang = Math.pow(10, rangi)
              pent = j3pGetRandomInt(1, rang - 1)
              if (String(pent)[String(pent).length - 1] === '0') { pent++ }
              pent2 = j3pGetRandomInt(1, rang - 1)
              if (String(pent2)[String(pent2).length - 1] === '0') { pent2++ }
              do {
                if (pent === pent2) {
                  pent++
                  if (pent === rang) {
                    pent = j3pGetRandomInt(1, rang - 2)
                  }
                }
                if (String(pent)[String(pent).length - 1] === '0') {
                  pent++
                }
              } while (pent === pent2)
              pent = pent / rang
              pent2 = pent2 / rang
              stor.n1 = j3pArrondi(stor.n1 + pent, rangi)
              stor.n2 = j3pArrondi(stor.n2 + pent2, rangi)
              stor.Stringn1 = String(stor.n1).replace('.', ',')
              stor.Stringn2 = String(stor.n2).replace('.', ',')
              break
            case 'dec': // mm partie ent , pas mm nb chiffres dec
              stor.n1 = j3pGetRandomInt(1, 100)
              stor.n2 = stor.n1
              rangi = j3pGetRandomInt(1, 3)
              do {
                rangi2 = j3pGetRandomInt(1, 3)
              } while (rangi === rangi2)

              rang = Math.pow(10, rangi)
              pent = j3pGetRandomInt(1, rang - 1)
              if (String(pent)[String(pent).length - 1] === '0') { pent++ }
              pent = pent / rang
              stor.n1 = j3pArrondi(stor.n1 + pent, rangi)

              rang = Math.pow(10, rangi2)
              pent = j3pGetRandomInt(1, rang - 1)
              if (String(pent)[String(pent).length - 1] === '0') { pent++ }
              pent = pent / rang
              stor.n2 = j3pArrondi(stor.n2 + pent, rangi2)
              stor.Stringn1 = String(stor.n1).replace('.', ',')
              stor.Stringn2 = String(stor.n2).replace('.', ',')
              break
            case '2': // un ent et un dec colles
              stor.n1 = j3pGetRandomInt(1, 100)
              stor.n2 = stor.n1
              rangi = j3pGetRandomInt(1, 3)
              rang = Math.pow(10, rangi)
              pent = j3pGetRandomInt(1, rang - 1)
              if (String(pent)[String(pent).length - 1] === '0') { pent++ }
              pent = pent / rang
              stor.n1 = j3pArrondi(stor.n1 + pent, rangi)
              stor.Stringn1 = String(stor.n1).replace('.', ',')
              stor.Stringn2 = String(stor.n2).replace('.', ',')
          }
          break
        } else {
          stor.n1 = j3pGetRandomInt(1, 100)
          rangi = j3pGetRandomInt(1, 3)
          rang = Math.pow(10, rangi)
          pent = j3pGetRandomInt(1, rang - 1)
          if (String(pent)[String(pent).length - 1] === '0') { pent++ }
          pent = pent / rang
          stor.n1 = j3pArrondi(stor.n1 + pent, rangi)
          stor.n2 = stor.n1
          stor.Stringn1 = String(stor.n1).replace('.', ',')
          if (j3pGetRandomBool()) {
            stor.Stringn2 = stor.Stringn1 + '0'
          } else {
            stor.Stringn2 = '0' + stor.Stringn1
          }
        }
        break
      case 'Fractions': {
        const den = j3pGetRandomInt(2, 50)
        stor.n1 = { num: j3pGetRandomInt(2, 50), den }
        stor.n2 = { num: j3pGetRandomInt(2, 50), den }
        if (stor.n1.num === stor.n2.num) stor.n2.num++
        stor.n1.Strnum = String(stor.n1.num)
        stor.n2.Strnum = String(stor.n2.num)
        break
      }
    }

    switch (e.place) {
      case '--':
        if (e.type === 'Fractions') {
          stor.n1.num = -stor.n1.num
          stor.n2.num = -stor.n2.num
          stor.n1.Strnum = '-' + stor.n1.Strnum
          stor.n2.Strnum = '-' + stor.n2.Strnum
        } else {
          stor.n1 = -stor.n1
          stor.n2 = -stor.n2
          stor.Stringn1 = '-' + stor.Stringn1
          stor.Stringn2 = '-' + stor.Stringn2
        }
        break
      case '+-':
        if (j3pGetRandomBool()) {
          if (e.type === 'Fractions') {
            stor.n1.num = -stor.n1.num
            stor.n1.Strnum = '-' + stor.n1.Strnum
          } else {
            stor.n1 = -stor.n1
            stor.Stringn1 = '-' + stor.Stringn1
          }
        } else {
          if (e.type === 'Fractions') {
            stor.n2.num = -stor.n2.num
            stor.n2.Strnum = '-' + stor.n2.Strnum
          } else {
            stor.n2 = -stor.n2
            stor.Stringn2 = '-' + stor.Stringn2
          }
        }
        break
      case '=':
        switch (e.type) {
          case 'Entiers':
            stor.n2 = stor.n1
            stor.Stringn2 = '0' + stor.Stringn1
            break
          case 'Fractions':
            stor.n2.num = stor.n1.num
            stor.n2.Strnum = '0' + stor.n1.Strnum
        }
        if (j3pGetRandomBool() && (!ds.Positifs)) {
          if (e.type === 'Fractions') {
            stor.n1.num = stor.n2.num = -stor.n1.num
            stor.n1.Strnum = '-' + stor.n1.Strnum
            stor.n2.Strnum = '-' + stor.n2.Strnum
          } else {
            stor.n1 = -stor.n1
            stor.n2 = -stor.n2
            stor.Stringn1 = '-' + stor.Stringn1
            stor.Stringn2 = '-' + stor.Stringn2
          }
        }
    }
    return e
  }

  function initSection () {
    let i
    initCptPe(ds, stor)

    const hj = ['ent', 'mmdec', 'dec', '2']
    stor.cas = hj[j3pGetRandomInt(0, 3)]

    // Construction de la page
    me.construitStructurePage('presentation1')

    ds.lesExos = []

    let lp = []
    if ((ds.Type_Nombres.indexOf('Entiers') !== -1) || (ds.Type_Nombres === 'Tous'))lp.push('Entiers')
    if ((ds.Type_Nombres.indexOf('Décimaux') !== -1) || (ds.Type_Nombres === 'Tous'))lp.push('Décimaux')
    if ((ds.Type_Nombres.indexOf('Fractions') !== -1) || (ds.Type_Nombres === 'Tous'))lp.push('Fractions')
    lp = j3pShuffle(lp)
    for (i = 0; i < ds.nbitems; i++) {
      ds.lesExos.push({ type: lp[i % (lp.length)] })
    }
    ds.lesExos = j3pShuffle(ds.lesExos)

    lp = ['++', '++', '=']
    lp.push('++')
    if (!ds.Positifs) {
      lp = ['++', '+-', '--', '=']
    }
    lp = j3pShuffle(lp)
    for (i = 0; i < ds.nbitems; i++) {
      ds.lesExos[i].place = lp[i % (lp.length)]
    }
    ds.lesExos = j3pShuffle(ds.lesExos)
    me.typederreurs = [0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0]
    me.score = 0
    me.afficheTitre('Comparer')
    enonceMain()
  }
  function yaReponse () {
    if (!stor.liste1.changed) { stor.liste1.focus(); return false }
    return true
  }
  function isRepOk () {
    stor.errzeroinut = false // point inconnu
    stor.errDif = false // avec +-
    stor.errmoins = false // avec --
    stor.errEnt = false // partie ent diff
    stor.errMdec = false // m nb chiff
    stor.errDec = false // m nb chiff
    stor.err2 = false // m nb chiff
    if ((stor.Lexo.place === '=') && (stor.liste1.reponse !== '$=$')) {
      stor.errzeroinut = true
      stor.compteurPe1++
      return false
    }
    if ((stor.Lexo.place === '=') && (stor.liste1.reponse === '$=$')) {
      return true
    }
    const ok2 = (stor.liste1.reponse === '$>$')
    let ok
    if (stor.Lexo.type === 'Fractions') {
      stor.ok1 = (stor.n1.num > stor.n2.num)
    } else {
      stor.ok1 = (stor.n1 > stor.n2)
    }
    ok = (stor.ok1 === ok2)
    if (stor.liste1.reponse === '$=$') { ok = false }
    if (!ok) {
      switch (stor.Lexo.place) {
        case '+-': stor.errDif = true
          break
        case '--': stor.errmoins = true
      }
      if (stor.Lexo.type === 'Décimaux') {
        switch (stor.cas) {
          case 'ent': stor.errEnt = true
            break
          case 'mmdec': stor.errMdec = true
            break
          case 'dec': stor.errDec = true
            break
          case '2': stor.err2 = true
        }
      }
    }
    if (stor.ok1) { stor.symb = '$>$' } else { stor.symb = '$<$' }
    return ok
  }
  function desactiveAll () {
    stor.liste1.disable()
  }
  function passeToutVert () {
    stor.liste1.corrige(true)
  }
  function barrelesfo () {
    stor.liste1.barre()
  }
  function affCorrFaux (isFin) {
    /// //affiche indic
    if (stor.errzeroinut) {
      j3pAffiche(stor.lesdiv.explications, null, 'Il y avait un zéro inutile !\n')
      stor.yaexplik = true
    }
    if (stor.errDif) {
      j3pAffiche(stor.lesdiv.explications, null, 'Les nombres positifs sont plus grands que les nombres négatifs !\n')
      stor.yaexplik = true
    }
    if (stor.errmoins) {
      j3pAffiche(stor.lesdiv.explications, null, 'Les nombres négatifs sont rangés à l’envers !\n')
      stor.yaexplik = true
    }
    if (stor.errEnt) {
      j3pAffiche(stor.lesdiv.explications, null, 'Il faut d’abord comparer les parties entières !\n')
      stor.yaexplik = true
    }
    if (stor.errDec) {
      j3pAffiche(stor.lesdiv.explications, null, 'Attention quand les parties décimales n’ont pas le même nombre de chiffres !\n')
      stor.yaexplik = true
    }
    stor.liste1.corrige(false)

    if (isFin) {
      // barre les faux
      barrelesfo()
      desactiveAll()
      stor.yaco = true
      // afficheco

      // si == juste la correc

      let symb
      let ajout = ''
      // si != une info pour les déimaux juste
      if (stor.Lexo.place !== '=') {
        symb = stor.symb
        if ((stor.Lexo.type === 'Décimaux') && (stor.cas === 'dec')) {
          const n1 = stor.Stringn1.length
          const n2 = stor.Stringn2.length
          let buf1
          let buf2
          let i
          if (n1 > n2) {
            buf1 = stor.Stringn2
            buf2 = stor.Stringn2
            for (i = 0; i < n1 - n2; i++) {
              buf2 += '0'
            }
          } else {
            buf1 = stor.Stringn1
            buf2 = stor.Stringn1
            for (i = 0; i < n2 - n1; i++) {
              buf2 += '0'
            }
          }
          ajout = ' $' + buf1 + ' = ' + buf2 + '$  <br>'
        }
      } else {
        symb = '='
      }
      j3pAffiche(stor.lesdiv.solution, null, ajout + '$' + stor.b1 + ' ' + symb + ' ' + stor.b2 + '$')
    }
  }
  function enonceMain () {
    if (((me.questionCourante % ds.nbetapes) === 1) || (ds.nbetapes === 1)) {
      me.videLesZones()
    }
    creeLesDiv()
    stor.Lexo = faisChoixExos()
    stor.lesdiv.travail.classList.add('travail')
    stor.lesdiv.consigne.classList.add('enonce')
    me.zonesElts.MG.classList.add('fond')
    poseQuestion()
    me.afficheBoutonValider()
    me.finEnonce()
  }

  switch (this.etat) {
    case 'enonce':
      // code exécuté au lancement de la section
      if (me.debutDeLaSection) {
        initSection()
      } else {
        enonceMain()
      }
      break // case "enonce":

    case 'correction':
      // On teste si une réponse a été saisie
      stor.yaexplik = false
      stor.yaco = false
      stor.lesdiv.explications.innerHTML = ''
      stor.lesdiv.solution.innerHTML = ''
      stor.lesdiv.explications.classList.remove('explique')

      if (yaReponse.call(this)) {
        // Bonne réponse
        if (isRepOk.call(this)) {
          this._stopTimer()
          this.score++
          stor.lesdiv.correction.style.color = this.styles.cbien
          stor.lesdiv.correction.innerHTML = cBien

          passeToutVert.call(this)
          desactiveAll.call(this)
          this.raz = false

          this.typederreurs[0]++
          this.cacheBoutonValider()
          this.etat = 'navigation'
          this.sectionCourante()
        } else {
          // Pas de bonne réponse
          stor.lesdiv.correction.style.color = this.styles.cfaux
          stor.lesdiv.correction.innerHTML = cFaux
          if (this.isElapsed) {
            // A cause de la limite de temps :
            this._stopTimer()

            stor.lesdiv.correction.innerHTML += tempsDepasse + '<BR>'
            this.typederreurs[10]++
            this.cacheBoutonValider()

            affCorrFaux(true)
            stor.yaco = true

            this.etat = 'navigation'
            this.sectionCourante()
          } else {
            // Réponse fausse :
            if (this.essaiCourant < ds.nbchances) {
              stor.lesdiv.correction.innerHTML += '<br>' + essaieEncore

              affCorrFaux(false)

              // affCorrFaux.call(this, false)
              this.typederreurs[1]++
            } else {
              // Erreur au nème essai
              this._stopTimer()
              this.cacheBoutonValider()

              affCorrFaux(true)
              stor.yaco = true

              this.typederreurs[2]++
              this.etat = 'navigation'
              this.sectionCourante()
            }
          }
        }
      } else {
        // pas de réponse
        stor.lesdiv.correction.style.color = this.styles.cfaux
        stor.lesdiv.correction.innerHTML = reponseIncomplete
        this.afficheBoutonValider()
      }

      if (stor.yaexplik) {
        stor.lesdiv.explications.classList.add('explique')
      }
      if (stor.yaco) {
        stor.lesdiv.solution.classList.add('correction')
      }

      // Obligatoire
      this.finCorrection()
      break // case "correction":

    case 'navigation':
      if (this.sectionTerminee()) {
        this.parcours.pe = getPe(ds, stor)
      } else {
        this.etat = 'enonce'
      }
      // Obligatoire
      this.finNavigation()
      break // case "navigation":
  }
}
