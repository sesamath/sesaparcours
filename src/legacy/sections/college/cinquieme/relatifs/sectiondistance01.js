import { j3pAddElt, j3pAjouteBouton, j3pArrondi, j3pGetNewId, j3pGetRandomInt, j3pGetRandomLetters, j3pPGCD, j3pShowError, j3pShuffle } from 'src/legacy/core/functions'
import { j3pCreeSVG } from 'src/legacy/core/functionsSvg'
import ListeDeroulante from 'src/legacy/outils/listeDeroulante/ListeDeroulante'
import { getPe, initCptPe } from 'src/legacy/sections/college/helperTommy'
import { j3pAffiche } from 'src/lib/mathquill/functions'
import { addDefaultTable } from 'src/legacy/themes/table'
import ZoneStyleMathquill2 from 'src/legacy/outils/zoneStyleMathquill/ZoneStyleMathquill2'
import { j3pCreeFenetres, j3pAfficheCroixFenetres, j3pToggleFenetres } from 'src/legacy/core/functionsJqDialog'
import BrouillonCalculs from 'src/legacy/outils/brouillon/BrouillonCalculs'
import { getMtgCore } from 'src/lib/outils/mathgraph'
import textesGeneriques from 'src/lib/core/textes'

const { cBien, cFaux, essaieEncore, reponseIncomplete, tempsDepasse } = textesGeneriques

/**
 * Ajouter une description ici
 * @fileOverview
 * @author Tommy Barroy <t.barroy@laposte.net>
 * @since 2017-10
 */
export const params = {
  outils: ['mathquill', 'calculatrice'],
  /**
   * Ce qui est paramétrable dans l’éditeur de graphe
   * @type {LegacySectionParamsParametres}
   */
  parametres: [
    ['nbrepetitions', 9, 'entier', 'Nombre de répétitions de la section'],
    ['nbchances', 1, 'entier', 'Nombre de tentatives autorisées'],
    ['Type_Nombres', 'Tous', 'liste', '<u>Entiers</u>:  L’exercice peut proposer des nombres entiers.  <br> <br> <u>Décimaux</u>: L’exercice peut proposer des nombres décimaux. <br> <br> <u>fractions</u>: L’exercice peut proposer des fractions. ', ['Entiers', 'Décimaux', 'Fractions', 'Entiers_Décimaux', 'Entiers_fractions', 'Décimaux_Fractions', 'Tous']],
    ['Axe', 'oui', 'liste', '<u>true</u>: Les points sont représentés sur un axe.', ['oui', 'non', 'les deux']],
    ['Raisonnement', true, 'boolean', '<u>true</u>: L’élève doit justifier son calcul '],
    ['Calculatrice', true, 'boolean', '<u>true</u>:  Une calculatrice est disponible'],
    // le modèle gère la compatibilité ascendante avec l’ancien paramètre MiseEnPagePlusClaire
    ['theme', 'standard', 'liste', 'Modèle pour l’affichage', ['standard', 'zonesAvecImageDeFond']]

  ],
  // dans le cas d’une section QUANTITATIVE
  pe: [
    { pe_1: 'Comparaison' },
    { pe_2: 'Calcul' },
    { pe_3: 'Raisonnement' },
    { pe_4: 'Parenthèses' },
    { pe_5: 'Ecriture' }
  ]
}

/**
 * section distance01
 * @this {Parcours}
 */
export default function main () {
  const me = this
  const ds = me.donneesSection
  const stor = me.storage
  let svgId = stor.svgId

  function creeLesDiv () {
    stor.lesdiv = {}
    stor.lesdiv.conteneur = j3pAddElt(me.zonesElts.MG, 'div', {
      style: me.styles.etendre('toutpetit.enonce', { padding: '10px' })
    })
    stor.lesdiv.consigne = addDefaultTable(stor.lesdiv.conteneur, 1, 1)[0][0]
    stor.lesdiv.travail = addDefaultTable(stor.lesdiv.conteneur, 1, 1)[0][0]
    stor.lesdiv.explications = j3pAddElt(stor.lesdiv.conteneur, 'div')
    stor.lesdiv.solution = j3pAddElt(stor.lesdiv.conteneur, 'div')
    stor.lesdiv.calculatrice = j3pAddElt(me.zonesElts.MD, 'div', {
      style: me.styles.etendre('toutpetit.enonce', { padding: '10px' })
    })
    stor.lesdiv.correction = j3pAddElt(me.zonesElts.MD, 'div', {
      style: me.styles.etendre('toutpetit.enonce', { padding: '10px' })
    })
    stor.lesdiv.solution.style.color = me.styles.petit.correction.color
    stor.lesdiv.explications.style.color = me.styles.cfaux
  }

  function poseQuestion () {
    if (stor.Lexo.Axe) {
      stor.contA = addDefaultTable(stor.lesdiv.consigne, 2, 1)
      j3pAffiche(stor.contA[0][0], null, '<b>Tu dois calculer la distance</b> $' + stor.lettre1 + stor.lettre2 + '$')
      faisFig()
      stor.Lexo.unite = ' unités de longueur'
    } else {
      j3pAffiche(stor.lesdiv.consigne, null, '$' + stor.lettre1 + '$ et $' + stor.lettre2 + '$ sont deux points d’une droite graduée d’unité $1$ ' + stor.Lexo.unite + '. \n')
      j3pAffiche(stor.lesdiv.consigne, null, 'L’abscisse  $x_{' + stor.lettre1 + '}$ du point $' + stor.lettre1 + ' $ est égale à $' + stor.Stringn1 + '$, \n')
      j3pAffiche(stor.lesdiv.consigne, null, 'L’abscisse  $x_{' + stor.lettre2 + '}$ du point $' + stor.lettre2 + ' $ est égale à $' + stor.Stringn2 + '$. \n')
      j3pAffiche(stor.lesdiv.consigne, null, '<b>Tu dois calculer la distance</b> $' + stor.lettre1 + stor.lettre2 + '$')
    }
    stor.tabCCont = addDefaultTable(stor.lesdiv.travail, 4, 4)
    if (ds.Raisonnement) {
      poseRaiz()
    } else {
      poseRep()
    }
  }

  function poseRaiz () {
    me.afficheBoutonValider()
    stor.encours = 'Raiz'
    j3pAffiche(stor.tabCCont[0][0], null, ' $' + stor.lettre1 + stor.lettre2 + '$')
    j3pAffiche(stor.tabCCont[0][1], null, ' $=$')
    let ll = ['$x_{' + stor.lettre1 + '} - x_{' + stor.lettre2 + '}$']
    ll.push('$x_{' + stor.lettre2 + '} - x_{' + stor.lettre1 + '}$')
    ll.push('$x_{' + stor.lettre1 + '} + x_{' + stor.lettre2 + '}$')
    ll.push('$x_{' + stor.lettre2 + '} + x_{' + stor.lettre1 + '}$')
    ll.push('$x_{' + stor.lettre1 + '} \\times x_{' + stor.lettre2 + '}$')
    ll.push('$x_{' + stor.lettre2 + '} \\times x_{' + stor.lettre1 + '}$')
    ll = j3pShuffle(ll)
    ll.splice(0, 0, 'Choisir')
    stor.liste = ListeDeroulante.create(stor.tabCCont[0][2], ll)
  }
  function poseRaiz2 () {
    me.afficheBoutonValider()
    stor.encours = 'Raiz2'
    j3pAffiche(stor.tabCCont[1][0], null, ' $' + stor.lettre1 + stor.lettre2 + '$')
    j3pAffiche(stor.tabCCont[1][1], null, ' $=$')
    const tabR = addDefaultTable(stor.tabCCont[1][2], 1, 3)
    j3pAffiche(tabR[0][1], null, ' $ - $ ')
    stor.zoneRaiz1 = new ZoneStyleMathquill2(tabR[0][0], { id: 'RAIZ2l0c1fdsfq', restric: stor.restric, enter: () => { me.sectionCourante() } })
    let bob = ''
    if (stor.Lexo.type !== 'Fractions') bob = '()'
    stor.zoneRaiz2 = new ZoneStyleMathquill2(tabR[0][2], { id: 'RAIZ2l0c1gfsdgfd', restric: stor.restric + bob, hasAutoKeyboard: bob !== '', enter: () => { me.sectionCourante() } })
  }
  function poseRep () {
    me.afficheBoutonValider()
    stor.encours = 'Rep'
    j3pAffiche(stor.tabCCont[0][0], null, ' $' + stor.lettre1 + stor.lettre2 + '$')
    j3pAffiche(stor.tabCCont[0][1], null, ' $=$')
    j3pAffiche(stor.tabCCont[0][3], null, '&nbsp;' + stor.Lexo.unite)
    stor.zoneRep = new ZoneStyleMathquill2(stor.tabCCont[0][2], { id: 'CONCONTl0c1lll', restric: stor.restric, enter: () => { me.sectionCourante() } })
  }
  function poseRepBis () {
    me.afficheBoutonValider()
    stor.encours = 'RepBis'
    j3pAffiche(stor.tabCCont[3][0], null, ' $' + stor.lettre1 + stor.lettre2 + '$')
    j3pAffiche(stor.tabCCont[3][1], null, ' $=$')
    stor.brouillon1 = new BrouillonCalculs({
      caseBoutons: stor.tabCCont[2][0],
      caseEgal: stor.tabCCont[2][1],
      caseCalculs: stor.tabCCont[2][2],
      caseApres: stor.tabCCont[2][3]
    }, { limite: 30, limitenb: 20, restric: stor.restric + '*+', hasAutoKeyboard: true, lmax: 2 })
    const tRR = addDefaultTable(stor.tabCCont[3][2], 1, 2)
    stor.zoneRep = new ZoneStyleMathquill2(tRR[0][0], { id: 'CONCONTl0c1lll', restric: stor.restric, enter: () => { me.sectionCourante() } })
    j3pAffiche(tRR[0][1], null, '&nbsp;' + stor.Lexo.unite)
  }
  function faisChoixExos () {
    const e = ds.lesExos.pop()
    ;[stor.lettre1, stor.lettre2, stor.lettre3] = j3pGetRandomLetters(3)
    stor.n1 = {}
    stor.n2 = {}
    switch (e.type) {
      case 'Entiers':
        stor.n1.num = j3pGetRandomInt(1, 100)
        if (e.place[0] === '-') stor.n1.num = -stor.n1.num
        stor.n2.num = j3pGetRandomInt(1, 100)
        if (e.place[1] === '-') stor.n2.num = -stor.n2.num
        if (stor.n1.num === stor.n2.num) stor.n2.num++
        stor.Stringn1 = String(stor.n1.num)
        stor.Stringn2 = String(stor.n2.num)
        stor.restric = '-0123456789'
        stor.n1.den = stor.n2.den = 1
        break
      case 'Décimaux':
        stor.n1.num = j3pArrondi(j3pGetRandomInt(1, 1000) / 100, 2)
        if (e.place[0] === '-') stor.n1.num = -stor.n1.num
        stor.Stringn1 = String(stor.n1.num).replace('.', ',')
        stor.n2.num = j3pArrondi(j3pGetRandomInt(1, 1000) / 100, 2)
        if (e.place[1] === '-') stor.n2.num = -stor.n2.num
        if (stor.n1.num === stor.n2.num) stor.n2.num = j3pArrondi(stor.n2 + 0.2, 2)
        stor.Stringn2 = String(stor.n2.num).replace('.', ',')
        stor.restric = '-0123456789.,'
        stor.n1.den = stor.n2.den = 1
        break
      case 'Fractions': {
        const den1 = j3pGetRandomInt(2, 9)
        const den2 = j3pGetRandomInt(2, 9)
        stor.n1 = { num: j3pGetRandomInt(2, 3) * den1 + 1, den: den1 }
        stor.n2 = { num: j3pGetRandomInt(5, 7) * den2 + 1, den: den2 }
        if (e.place[0] === '-') stor.n1.num = -stor.n1.num
        if (e.place[1] === '-') stor.n2.num = -stor.n2.num
        stor.Stringn1 = '\\frac{ ' + stor.n1.num + ' }{ ' + stor.n1.den + ' }'
        stor.Stringn2 = '\\frac{ ' + stor.n2.num + ' }{ ' + stor.n2.den + ' }'
        stor.restric = '-0123456789/'
      }
    }

    if (stor.n1.num / stor.n1.den > stor.n2.num / stor.n2.den) {
      stor.lg = stor.lettre1
      stor.lp = stor.lettre2
      stor.leg = stor.n1
      stor.lep = stor.n2
      stor.Stg = stor.Stringn1
      stor.Stp = stor.Stringn2
    } else {
      stor.leg = stor.n2
      stor.lep = stor.n1
      stor.Stg = stor.Stringn2
      stor.Stp = stor.Stringn1
      stor.lg = stor.lettre2
      stor.lp = stor.lettre1
    }

    return e
  }

  function initSection () {
    let i
    initCptPe(ds, stor)

    // Construction de la page
    me.construitStructurePage('presentation1bis')

    me.donneesSection.lesExos = []

    let lp = []
    if ((ds.Type_Nombres.indexOf('Entiers') !== -1) || (ds.Type_Nombres === 'Tous'))lp.push('Entiers')
    if ((ds.Type_Nombres.indexOf('Décimaux') !== -1) || (ds.Type_Nombres === 'Tous'))lp.push('Décimaux')
    if ((ds.Type_Nombres.indexOf('Fractions') !== -1) || (ds.Type_Nombres === 'Tous'))lp.push('Fractions')
    lp = j3pShuffle(lp)
    for (i = 0; i < me.donneesSection.nbitems; i++) {
      me.donneesSection.lesExos.push({ type: lp[i % (lp.length)] })
    }
    me.donneesSection.lesExos = j3pShuffle(me.donneesSection.lesExos)

    lp = []
    if ((ds.Axe === 'oui') || (ds.Axe === 'les deux')) lp.push(true)
    if ((ds.Axe === 'non') || (ds.Axe === 'les deux')) lp.push(false)
    lp = j3pShuffle(lp)
    for (i = 0; i < me.donneesSection.nbitems; i++) {
      me.donneesSection.lesExos[i].Axe = lp[i % (lp.length)]
    }
    me.donneesSection.lesExos = j3pShuffle(me.donneesSection.lesExos)

    lp = ['++', '--', '+-', '-+']
    lp = j3pShuffle(lp)
    for (i = 0; i < me.donneesSection.nbitems; i++) {
      me.donneesSection.lesExos[i].place = lp[i % (lp.length)]
    }
    me.donneesSection.lesExos = j3pShuffle(me.donneesSection.lesExos)

    lp = ['km', 'cm']
    lp = j3pShuffle(lp)
    for (i = 0; i < me.donneesSection.nbitems; i++) {
      me.donneesSection.lesExos[i].unite = lp[i % (lp.length)]
    }
    me.donneesSection.lesExos = j3pShuffle(me.donneesSection.lesExos)

    me.typederreurs = [0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0]

    me.score = 0

    me.afficheTitre('Calculer une distance')
    // chargement mtg
    getMtgCore({ withMathJax: true })
      .then(
        // success
        (mtgAppLecteur) => {
          stor.mtgAppLecteur = mtgAppLecteur
          enonceMain()
        },
        // failure
        (error) => {
          j3pShowError(error, { message: 'mathgraph n’est pas correctement chargé', mustNotify: true })
        })
      // plantage dans enonceMain
      .catch(j3pShowError)
  }

  function faisFig () {
    function qc (x) {
      return { num: x.num, den: x.den }
    }

    let n1, n2, n3, nx
    let v1, v2, v3, vx
    let b1, b2, b3, bx
    let l1, l2, l3, lx
    l1 = qc(stor.n1)
    l2 = qc(stor.n2)
    l3 = { num: 0, den: 1 }
    n1 = stor.Stringn1
    n2 = stor.Stringn2
    n3 = ' 0 '
    v1 = stor.lettre1
    v2 = stor.lettre2
    v3 = 'e'
    b1 = b2 = false
    b3 = true
    if (l1.num / l1.den < l2.num / l2.den) {
      lx = qc(l1)
      vx = v1
      bx = b1
      nx = n1
      l1 = qc(l2)
      v1 = v2
      b1 = b2
      n1 = n2
      l2 = qc(lx)
      v2 = vx
      b2 = bx
      n2 = nx
    }
    if (l2.num / l2.den < l3.num / l3.den) {
      lx = qc(l3)
      vx = v3
      bx = b3
      nx = n3
      l3 = qc(l2)
      v3 = v2
      b3 = b2
      n3 = n2
      l2 = qc(lx)
      v2 = vx
      b2 = bx
      n2 = nx
    }
    if (l1.num / l1.den < l2.num / l2.den) {
      lx = qc(l1)
      vx = v1
      bx = b1
      nx = n1
      l1 = qc(l2)
      v1 = v2
      b1 = b2
      n1 = n2
      l2 = qc(lx)
      v2 = vx
      b2 = bx
      n2 = nx
    }

    stor.svgId = j3pGetNewId('mtg32')
    svgId = stor.svgId
    const txtFigure = 'TWF0aEdyYXBoSmF2YTEuMAAAABM+TMzNAAJmcv###wEA#wEAAAAAAAAAAANLAAACfwAAAQEAAAAAAAAAAQAAAB######AAAAAQAKQ0NhbGNDb25zdAD#####AAJwaQAWMy4xNDE1OTI2NTM1ODk3OTMyMzg0Nv####8AAAABAApDQ29uc3RhbnRlQAkh+1RELRj#####AAAAAQAKQ1BvaW50QmFzZQD#####AAAAAAAQAAAAAAAAAAAAAABACAAAAAAAAAAACQAAQFHAAAAAAABAUThR64UeuP####8AAAABABRDRHJvaXRlRGlyZWN0aW9uRml4ZQD#####AQAAfwEQAAABAAAAAgAAAAEBP#AAAAAAAAD#####AAAAAQAPQ1BvaW50TGllRHJvaXRlAP####8AAAAAABAAAAAAAAAAAAAAAEAIAAAAAAAAAAAJAABAVsAAAAAAAAAAAAIAAAAEAP####8AAAAAABAAAAAAAAAAAAAAAEAIAAAAAAAAAAAJAABAZqAAAAAAAAAAAAIAAAAEAP####8BAAB#ABAAAAAAAAAAAAAAAEAIAAAAAAAAAAAJAAFAcMAAAAAAAAAAAAIAAAACAP####8BAAB#ABAAAAAAAAAAAAAAAEAIAAAAAAAAAAAJAAFAc7AAAAAAAEBL8KPXCj1w#####wAAAAEAD0NTeW1ldHJpZUF4aWFsZQD#####AAAAAv####8AAAABAAtDUG9pbnRJbWFnZQD#####AQAAfwAQAAAAAAAAAAAAAABACAAAAAAAAAAACQAAAAAGAAAABwAAAAQA#####wEAAH8AEAAAAAAAAAAAAAAAQAgAAAAAAAAAAAkAAcBIf#######AAAAAv####8AAAABAAhDU2VnbWVudAD#####AAAAAAAQAAABAAAAAgAAAAkAAAAFAAAABwD#####AAAAAAAQAAABAAAAAgAAAAYAAAAFAAAABwD#####AAAAAAAQAAABAAAAAgAAAAUAAAAI#####wAAAAEAFkNEcm9pdGVQZXJwZW5kaWN1bGFpcmUA#####wEAAAAAEAAAAQAAAAIAAAABAAAACgAAAAgA#####wEAAAAAEAAAAQAAAAIAAAADAAAACgAAAAgA#####wEAAAAAEAAAAQAAAAIAAAAEAAAACgAAAAQA#####wEAAAAAEAAAAAAAAAAAAAAAQAgAAAAAAAAAAAkAAb+2nKs0ffsmAAAADQAAAAUA#####wAAAAoAAAAGAP####8BAAAAABAAAAAAAAAAAAAAAEAIAAAAAAAAAAAJAAAAABAAAAAR#####wAAAAEAEENEcm9pdGVQYXJhbGxlbGUA#####wEAAAAAEAAAAQAAAAIAAAAQAAAACgAAAAkA#####wEAAAAAEAAAAQAAAAIAAAASAAAACv####8AAAABABBDSW50RHJvaXRlRHJvaXRlAP####8BAAAAABAAAAAAAAAAAAAAAEAIAAAAAAAAAAAJAAAAAA4AAAATAAAACgD#####AQAAAAAQAAAAAAAAAAAAAABACAAAAAAAAAAACQAAAAAPAAAAEwAAAAoA#####wEAAAAAEAAAAAAAAAAAAAAAQAgAAAAAAAAAAAkAAAAADwAAABQAAAAKAP####8BAAAAABAAAAAAAAAAAAAAAEAIAAAAAAAAAAAJAAAAAA4AAAAU#####wAAAAIADENDb21tZW50YWlyZQD#####AAAAAAEAAUEAAAAQEAAAAAAAAQAAAAEAAAABAAAAAAAAAAAAAUEAAAALAP####8AAAAAAQABQgAAABUQAAAAAAABAAAAAQAAAAEAAAAAAAAAAAABQgAAAAsA#####wAAAAABAAFDAAAAFhAAAAAAAAEAAAABAAAAAQAAAAAAAAAAAAFDAAAACwD#####AAAA#wEAAmExAAAAEhIAAAAAAAEAAAABAAAAAQAAAAAAAAAAAAExAAAACwD#####AAAA#wEAAmEyAAAAGBIAAAAAAAEAAAABAAAAAQAAAAAAAAAAAAEyAAAACwD#####AAAA#wEAAmEzAAAAFxIAAAAAAAEAAAABAAAAAQAAAAAAAAAAAAEz################'
    const yy = j3pCreeSVG(stor.contA[1][0], { id: svgId, width: 380, height: 150 })
    yy.style.border = 'solid black 1px'
    stor.mtgAppLecteur.removeAllDoc()
    stor.mtgAppLecteur.addDoc(svgId, txtFigure, true)
    stor.mtgAppLecteur.giveFormula2(svgId, 'rotation', stor.rotation)
    stor.mtgAppLecteur.giveFormula2(svgId, 'lang', stor.deplacement)
    stor.mtgAppLecteur.setText(svgId, '#a1', '$' + n3 + '$', true)
    stor.mtgAppLecteur.setText(svgId, '#a2', '$' + n2 + '$', true)
    stor.mtgAppLecteur.setText(svgId, '#a3', '$' + n1 + '$', true)
    if (v1 !== 'e') {
      stor.mtgAppLecteur.setText(svgId, '#C', v1, true)
    } else {
      stor.mtgAppLecteur.setText(svgId, '#C', stor.lettre3, true)
    }
    if (v2 !== 'e') {
      stor.mtgAppLecteur.setText(svgId, '#B', v2, true)
    } else {
      stor.mtgAppLecteur.setText(svgId, '#B', stor.lettre3, true)
    }
    if (v3 !== 'e') {
      stor.mtgAppLecteur.setText(svgId, '#A', v3, true)
    } else {
      stor.mtgAppLecteur.setText(svgId, '#A', stor.lettre3, true)
    }
    stor.mtgAppLecteur.calculateAndDisplayAll(true)
  }

  function yaReponse () {
    if (stor.encours === 'Raiz') {
      if (!stor.liste.changed) {
        stor.liste.focus()
        return false
      }
      return true
    }
    if ((stor.encours === 'Rep') || (stor.encours === 'RepBis')) {
      if (stor.zoneRep.reponse() === '') {
        stor.zoneRep.focus()
        return false
      }
    }
    if (stor.encours === 'Raiz2') {
      if (!stor.zoneRaiz1.reponse() === '') {
        stor.zoneRaiz1.focus()
        return false
      }
      if (!stor.zoneRaiz2.reponse() === '') {
        stor.zoneRaiz2.focus()
        return false
      }
    }

    return true
  }

  function isRepOk () {
    let t, p1, p2
    let ok = true
    stor.errMalEcrit = false
    stor.errOP = false
    stor.errOrdre = false
    stor.errRemp = false
    stor.errParent2 = false
    stor.errParent1 = false
    stor.errCalcu = false

    if (stor.encours === 'Raiz') {
      t = stor.liste.reponse
      if (t.indexOf('-') === -1) {
        stor.errOP = true
        stor.liste.corrige(false)
        return false
      }
      p1 = t.indexOf(stor.lettre1)
      p2 = t.indexOf(stor.lettre2)
      t = (p1 < p2)
      p1 = stor.n1.num / stor.n1.den > stor.n2.num / stor.n2.den
      if (t !== p1) {
        stor.errOrdre = true
        stor.liste.corrige(false)
        return false
      }
      return true
    }
    if (stor.encours === 'Raiz2') {
      t = stor.zoneRaiz1.reponse().replace(/ /g, '')
      if (t !== stor.Stg.replace(/ /g, '')) {
        p1 = stor.zoneRaiz1.reponsenb()
        if (j3pArrondi(p1[0] / p1[1], 7) !== j3pArrondi(stor.leg.num / stor.leg.den, 7)) {
          stor.errRemp = true
        } else {
          stor.errMalEcrit = true
        }
        stor.zoneRaiz1.corrige(false)
        ok = false
      }
      t = stor.zoneRaiz2.reponse().replace(/ /g, '')
      if (stor.Lexo.type !== 'Fractions') {
        if (stor.lep.num < 0) {
          if ((t[0] !== '(') || (t[t.length - 1] !== ')')) {
            stor.errParent1 = true
            stor.zoneRaiz2.corrige(false)
            return false
          }
          t = t.replace(')', '')
          t = t.replace('(', '')
          if ((t.indexOf(')') !== -1) || (t.indexOf('(') !== -1)) {
            stor.errParent2 = true
            stor.zoneRaiz2.corrige(false)
            return false
          }
        }
      }
      if (t !== stor.Stp.replace(/ /g, '')) {
        p1 = stor.zoneRaiz2.reponsenb()
        if (j3pArrondi(p1[0] / p1[1], 7) !== j3pArrondi(stor.lep.num / stor.lep.den, 7)) {
          stor.errRemp = true
        } else {
          stor.errMalEcrit = true
        }
        stor.zoneRaiz2.corrige(false)
        ok = false
      }
      return ok
    }
    if ((stor.encours === 'Rep') || (stor.encours === 'RepBis')) {
      t = stor.zoneRep.reponse().replace(/ /g, '')
      // verif c un nombre bien ecris
      // si pas frac
      // ca commence pas par 0
      if (t[0] === '0' && t.length > 1) {
        if (t[1] !== ',') {
          stor.errMalEcrit = true
          stor.zoneRep.corrige(false)
          return false
        }
      }
      // si ya plusieurs virgule
      if (t.indexOf(',') !== t.lastIndexOf(',')) {
        stor.errMalEcrit = true
        stor.zoneRep.corrige(false)
        return false
      }
      if (t.indexOf(',') === 0 || t.indexOf(',') === t.length - 1) {
        stor.errMalEcrit = true
        stor.zoneRep.corrige(false)
        return false
      }
      p1 = stor.zoneRep.reponsenb()
      if (isNaN(p1[0]) || isNaN(p1[1])) {
        stor.errMalEcrit = true
        stor.zoneRep.corrige(false)
        return false
      }

      t = stor.zoneRep.reponsenb()
      if (j3pArrondi(t[0] / t[1], 7) !== j3pArrondi(Math.abs(stor.n1.num / stor.n1.den - stor.n2.num / stor.n2.den), 7)) {
        stor.errCalcu = true
        stor.zoneRep.corrige(false)
        ok = false
      }
      return ok
    }
  } // isRepOk

  function desactiveAll () {
    if (stor.encours === 'Raiz') stor.liste.disable()
    if (stor.encours === 'Rep') stor.zoneRep.disable()
    if (stor.encours === 'Raiz2') {
      stor.zoneRaiz1.disable()
      stor.zoneRaiz2.disable()
    }
    if (stor.encours === 'RepBis') {
      stor.zoneRep.disable()
      stor.brouillon1.disable()
    }
  } // desactiveAll

  function passeToutVert () {
    if (stor.encours === 'Raiz') stor.liste.corrige(true)
    if ((stor.encours === 'Rep') || (stor.encours === 'RepBis')) stor.zoneRep.corrige(true)
    if (stor.encours === 'Raiz2') {
      stor.zoneRaiz1.corrige(true)
      stor.zoneRaiz2.corrige(true)
    }
  } // passeToutVert
  function barrelesfo () {
    if (stor.encours === 'Raiz') stor.liste.barre()
    if ((stor.encours === 'Rep') || (stor.encours === 'RepBis')) stor.zoneRep.barre()
    if (stor.encours === 'Raiz2') {
      stor.zoneRaiz1.barreIfKo()
      stor.zoneRaiz2.barreIfKo()
    }
  }
  function affCorrFaux (isFin) {
    /// //affiche indic
    if (stor.errOP) {
      j3pAffiche(stor.lesdiv.explications, null, 'Ce n’est pas la bonne opération !\n')
      stor.compteurPe3++
      stor.yaexplik = true
    }
    if (stor.errOrdre) {
      j3pAffiche(stor.lesdiv.explications, null, 'Tu inverses les termes !\n')
      stor.compteurPe1++
      stor.yaexplik = true
    }
    if (stor.errMalEcrit) {
      j3pAffiche(stor.lesdiv.explications, null, 'Ce n’est pas une bonne idée de changer l’écriture des nombres ici !\n')
      stor.yaexplik = true
      stor.compteurPe5++
    }
    if (stor.errRemp) {
      j3pAffiche(stor.lesdiv.explications, null, 'Il faut prendre les valeurs de l’énoncé !\n')
      stor.yaexplik = true
    }
    if (stor.errParent1) {
      j3pAffiche(stor.lesdiv.explications, null, 'Tu oublies des parenthèses !\n')
      stor.yaexplik = true
      stor.compteurPe4++
    }
    if (stor.errParent2) {
      j3pAffiche(stor.lesdiv.explications, null, 'Il y a un problème de parenthèses !\n')
      stor.yaexplik = true
      stor.compteurPe4++
    }
    if (stor.errCalcu) {
      j3pAffiche(stor.lesdiv.explications, null, 'Erreur de calcul !\n')
      stor.yaexplik = true
      stor.compteurPe2++
    }

    if (isFin) {
      // barre les faux
      barrelesfo()
      desactiveAll()
      let rep
      stor.yaco = true
      if (stor.Lexo.type !== 'Fractions') {
        rep = (j3pArrondi(Math.abs(stor.n1.num - stor.n2.num), 2) + '').replace('.', ',')
      } else {
        const num = Math.abs(stor.n1.num * stor.n2.den - stor.n2.num * stor.n1.den)
        const den = stor.n1.den * stor.n2.den
        const pg = j3pPGCD(num, den, { negativesAllowed: true, valueIfZero: 1 })
        rep = '\\frac{' + Math.round(num / pg) + '}{' + Math.round(den / pg) + '}'
      }
      if (stor.lep.num < 0) stor.Stp = '(' + stor.Stp + ')'
      j3pAffiche(stor.lesdiv.solution, null, '$' + stor.lettre1 + stor.lettre2 + ' = x_{' + stor.lg + '} - x_{' + stor.lp + '}$ \n')
      j3pAffiche(stor.lesdiv.solution, null, '$' + stor.lettre1 + stor.lettre2 + ' = ' + stor.Stg + '- ' + stor.Stp + '$\n')
      j3pAffiche(stor.lesdiv.solution, null, '$' + stor.lettre1 + stor.lettre2 + ' = ' + rep + '$')
    } else { me.afficheBoutonValider() }
  }
  function boutonfen () {
    j3pToggleFenetres('Calculatrice')
  }
  function enonceMain () {
    if (me.etapeCourante === 1) {
      me.videLesZones()
    }
    creeLesDiv()
    stor.Lexo = faisChoixExos()
    stor.lesdiv.travail.classList.add('travail')
    stor.lesdiv.consigne.classList.add('enonce')
    me.zonesElts.MG.classList.add('fond')
    poseQuestion()
    if (ds.Calculatrice) {
      me.fenetresjq = [{ name: 'Calculatrice', title: 'Une calculatrice', left: 600, top: 10 }]
      j3pCreeFenetres(me)
      j3pAfficheCroixFenetres('Calculatrice')
      j3pAjouteBouton(stor.lesdiv.calculatrice, 'Calculatrice', 'MepBoutons', 'Calculatrice', boutonfen)
    }
    me.afficheBoutonValider()
    me.finEnonce()
  }

  switch (this.etat) {
    case 'enonce':
      // code exécuté au lancement de la section
      if (me.debutDeLaSection) {
        initSection()
      } else {
        enonceMain()
      }
      break // case "enonce":

    case 'correction':
      // On teste si une réponse a été saisie
      stor.yaexplik = false
      stor.yaco = false
      stor.lesdiv.explications.innerHTML = ''
      stor.lesdiv.solution.innerHTML = ''
      stor.lesdiv.explications.classList.remove('explique')

      if (yaReponse()) {
        // Bonne réponse
        if (isRepOk()) {
          this._stopTimer()
          stor.lesdiv.correction.style.color = this.styles.cbien
          stor.lesdiv.correction.innerHTML = cBien

          passeToutVert()
          desactiveAll()
          if (stor.encours === 'Raiz') {
            poseRaiz2()
            me.cacheBoutonSuite()
            return
          }
          if (stor.encours === 'Raiz2') {
            poseRepBis()
            me.cacheBoutonSuite()
            return
          }

          this.score++
          this.typederreurs[0]++
          this.cacheBoutonValider()
          this.etat = 'navigation'
          this.sectionCourante()
        } else {
          // Pas de bonne réponse
          stor.lesdiv.correction.style.color = this.styles.cfaux
          stor.lesdiv.correction.innerHTML = cFaux
          if (this.isElapsed) {
            // A cause de la limite de temps :
            this._stopTimer()

            stor.lesdiv.correction.innerHTML += tempsDepasse + '<BR>'
            this.typederreurs[10]++
            this.cacheBoutonValider()

            affCorrFaux(true)
            stor.yaco = true

            this.etat = 'navigation'
            this.sectionCourante()
          } else {
            // Réponse fausse :
            if (me.essaiCourant < me.donneesSection.nbchances) {
              stor.lesdiv.correction.innerHTML += '<br>' + essaieEncore

              affCorrFaux(false)

              // affCorrFaux.call(this, false)
              this.typederreurs[1]++
            } else {
              // Erreur au nème essai
              this._stopTimer()
              this.cacheBoutonValider()

              affCorrFaux(true)
              stor.yaco = true

              this.typederreurs[2]++
              this.etat = 'navigation'
              this.sectionCourante()
            }
          }
        }
      } else {
        // pas de réponse
        stor.lesdiv.correction.style.color = this.styles.cfaux
        stor.lesdiv.correction.innerHTML = reponseIncomplete
        this.afficheBoutonValider()
      }
      if (stor.yaexplik) {
        stor.lesdiv.explications.classList.add('explique')
      }
      if (stor.yaco) {
        stor.lesdiv.solution.classList.add('correction')
      }

      // Obligatoire
      this.finCorrection()
      break // case "correction":

    case 'navigation':
      if (this.sectionTerminee()) {
        this.parcours.pe = getPe(ds, stor)
      } else {
        this.etat = 'enonce'
      }
      this.finNavigation()
      break // case "navigation":
  }
}
