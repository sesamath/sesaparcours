import { j3pAddElt, j3pAjouteBouton, j3pArrondi, j3pClone, j3pEmpty, j3pGetRandomBool, j3pGetRandomInt, j3pShowError, j3pShuffle } from 'src/legacy/core/functions'
import { verifNombreBienEcrit } from 'src/legacy/outils/fonctions/ecritureNombre'
import { getPe, initCptPe } from 'src/legacy/sections/college/helperTommy'
import { j3pAffiche } from 'src/lib/mathquill/functions'
import { addDefaultTable } from 'src/legacy/themes/table'
import ZoneStyleMathquill3 from 'src/legacy/outils/zoneStyleMathquill/ZoneStyleMathquill3'
import { j3pCreeFenetres, j3pAfficheCroixFenetres, j3pToggleFenetres } from 'src/legacy/core/functionsJqDialog'
import textesGeneriques from 'src/lib/core/textes'

const { cBien, cFaux, essaieEncore, reponseIncomplete, tempsDepasse } = textesGeneriques

/**
 * Ajouter une description ici
 * @fileOverview
 * @author Tommy Barroy <t.barroy@laposte.net>
 * @since 2017-10
*/
export const params = {
  outils: ['mathquill', 'calculatrice'],
  /**
   * Ce qui est paramétrable dans l’éditeur de graphe
   * @type {LegacySectionParamsParametres}
   */
  parametres: [
    ['nbrepetitions', 3, 'entier', 'Nombre de répétitions de la section'],
    ['nbchances', 2, 'entier', 'Nombre d’essais possibles.'],
    ['Min_nb_termes', 1, 'entier', 'Nombre de termes minimum (supérieur ou égal à 4).'],
    ['Max_nb_termes', 10, 'entier', 'Nombre de termes maximul (inférieur ou égal à 10).'],
    ['Progression', 'linéaire', 'liste', '<u>aléatoire</u> ou <u>linéaire</u> pour déterminer', ['linéaire', 'aléatoire']],
    ['entier', false, 'boolean', '<u>true</u>:  Les nombres de départ sont focément entiers'],
    ['Deux_Parentheses', false, 'boolean', '<i>Uniquement pour <b>Supprimer</b><br><br></i><u>true</u>:  L’expression comporte deux parenthèses.'],
    ['Detail_deplace', true, 'boolean', '<i>Uniquement pour <b>Réduction</b><br><br></i><u>true</u>:  La ligne où l’on regroupe les termes à réduire est exigée.'],
    ['carre', true, 'boolean', '<u>true</u>:  Présence de x² ou y².'],
    ['Question', 'Supprime', 'liste', '<u>Supprime</u>:  Il faut supprimer des parenthèses. <br><br> <u>Simplifie</u>:  Il faut simplifier des produits. <br><br> <u>Réduis</u>:  Il faut réduire.', ['Simplifie_Supprime', 'Supprime_Réduis', 'Simplifie_Réduis', 'Simplifie', 'Supprime', 'Réduis', 'les trois']],
    ['simplifie', false, 'boolean', '<u>true</u>:  L’élève ne doit pas écrire le signe + au résutat.'],
    ['Calculatrice', true, 'boolean', '<u>true</u>: Une calculatrice est disponible.'],
    // le modèle gère la compatibilité ascendante avec l’ancien paramètre MiseEnPagePlusClaire
    ['theme', 'standard', 'liste', 'Modèle pour l’affichage', ['standard', 'zonesAvecImageDeFond']]

    // ["nom_de_la _letiable_déclarée_dans la fonction _Données,valeur par défaut,"type de la letiable","description"]
  ],

  // FIXME y'a 3 pe mais un seul compteur incrémenté (pe_3), les autres seront jamais affectées
  pe: [
    {
      pe_1: 'calul',
      pe_2: 'regroupement',
      pe_3: 'parentheses'
    }
  ]
}

/**
 * section parentheses
 * @this {Parcours}
 */
export default function main () {
  const me = this
  const ds = me.donneesSection
  const stor = this.storage

  function initSection () {
    stor.lesbulle = []
    initCptPe(ds, stor)

    me.validOnEnter = false // ex donneesSection.touche_entree

    // Construction de la page
    me.construitStructurePage('presentation3')

    stor.lafaire = []
    if ((ds.Question.indexOf('Simplifie') !== -1) || (ds.Question === 'les trois')) stor.lafaire.push('Simp')
    if ((ds.Question === 'les trois') || (ds.Question.indexOf('Supprime') !== -1)) stor.lafaire.push('Sup')
    if ((ds.Question === 'les trois') || (ds.Question.indexOf('Rédui') !== -1)) {
      if (ds.Detail_deplace) stor.lafaire.push('Dep')
      stor.lafaire.push('Red')
    }
    me.donneesSection.nbetapes = stor.lafaire.length
    me.donneesSection.nbitems = me.donneesSection.nbetapes * me.donneesSection.nbrepetitions
    stor.encours = stor.lafaire[stor.lafaire.length - 1]

    if (ds.Min_nb_termes < 1) ds.Min_nb_termes = 1
    if (ds.Max_nb_termes > 10) ds.Max_nb_termes = 10
    if (ds.Max_nb_termes < 1) ds.Max_nb_termes = 1
    if (ds.Min_nb_termes > 10) ds.Min_nb_termes = 10
    if ((ds.Question === 'les trois') || (ds.Question.indexOf('Supprime') !== -1)) {
      if (ds.Deux_Parentheses) ds.Min_nb_termes = Math.max(ds.Min_nb_termes, 2)
    }
    if ((ds.Question === 'les trois') || (ds.Question.indexOf('Rédui') !== -1)) {
      ds.Min_nb_termes = Math.max(ds.Min_nb_termes, 3)
    }
    if (ds.Min_nb_termes > ds.Max_nb_termes) ds.Max_nb_termes = ds.Min_nb_termes

    ds.lesExos = []
    let lp = []
    for (let i = ds.Min_nb_termes; i < ds.Max_nb_termes + 1; i++) {
      lp.push(i)
    }
    for (let i = 0; i < ds.nbitems; i++) {
      ds.lesExos[i] = { nb: lp[i % lp.length] }
    }
    ds.lesExos = j3pShuffle(ds.lesExos)

    lp = ['+', '-']
    if (ds.Deux_Parentheses) lp = ['++', '--', '+-', '-+']
    for (let i = 0; i < ds.nbitems; i++) {
      ds.lesExos[i].cas = lp[i % lp.length]
    }
    ds.lesExos = j3pShuffle(ds.lesExos)

    if (ds.Progression === 'linéaire') {
      ds.lesExos.sort(function (a, b) {
        return b.nb - a.nb
      })
    }

    me.typederreurs = [0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0]

    me.score = 0
    let tt
    switch (ds.Question) {
      case 'Supprime': tt = 'Supprimer des parenthèses'
        break
      case 'Réduis': tt = 'Réduire une expression littérale'
        break
      case 'Simplifie': tt = 'Simplifier une expression littérale'
        break
      case 'Simplifie_Supprime': tt = 'Simplifier puis supprimer des parenthèses'
        break
      case 'Supprime_Réduis': tt = 'Supprimer des parenthèses puis réduire'
        break
      case 'Simplifie_Réduis': tt = 'Simplifier puis réduire une expression littérale'
        break
      default: tt = 'Modifier l’écriture d’une expression littérale'
    }

    me.afficheTitre(tt)
    enonceMain()
  }
  function faisChoixExos () {
    stor.clvspe = false
    const e = ds.lesExos.pop()
    let yap = false
    let yan = false
    let st
    stor.lestermes = []
    if (ds.Question.indexOf('Réduis') !== -1 || ds.Question === 'les trois') e.nb = Math.max(e.nb, 4)
    // faut que y’est les 2 signes
    const used = []
    for (let i = 0; i < e.nb; i++) {
      st = j3pGetRandomBool()
      yap = yap || st
      yan = yan || (!st)
      stor.lestermes.push({ signe: st })
      let sto = 0
      if (ds.entier) {
        do {
          sto++
          stor.lestermes[stor.lestermes.length - 1].val1 = j3pGetRandomInt(1, 10)
          if (j3pGetRandomInt(0, 2) !== 0) stor.lestermes[stor.lestermes.length - 1].val1 = 1
          stor.lestermes[stor.lestermes.length - 1].val2 = j3pGetRandomInt(1, 10)
          stor.lestermes[stor.lestermes.length - 1].val = stor.lestermes[stor.lestermes.length - 1].val2 * stor.lestermes[stor.lestermes.length - 1].val1
        } while (used.indexOf(stor.lestermes[stor.lestermes.length - 1].val) !== -1 && sto < 1000)
        used.push(stor.lestermes[stor.lestermes.length - 1].val)
      } else {
        do {
          sto++
          stor.lestermes[stor.lestermes.length - 1].val1 = j3pGetRandomInt(1, 15)
          if (j3pGetRandomInt(0, 1) !== 0) stor.lestermes[stor.lestermes.length - 1].val1 = 1
          stor.lestermes[stor.lestermes.length - 1].val2 = j3pArrondi(j3pGetRandomInt(1, 3) / 10, 1)
          stor.lestermes[stor.lestermes.length - 1].val = j3pArrondi(stor.lestermes[stor.lestermes.length - 1].val2 * stor.lestermes[stor.lestermes.length - 1].val1, 1)
        } while (used.indexOf(stor.lestermes[stor.lestermes.length - 1].val) !== -1 && sto < 1000)
        used.push(stor.lestermes[stor.lestermes.length - 1].val)
      }
    }
    stor.par = []
    if (e.nb < 2) e.cas = e.cas[0]
    switch (e.cas) {
      case '+':
        stor.nbpar = 1
        stor.par[0] = { deb: j3pGetRandomInt(0, e.nb - 1) }
        stor.par[0].fin = j3pGetRandomInt(stor.par[0].deb + 1, e.nb)
        stor.par[1] = { deb: -1, fin: -1 }
        break
      case '-':
        stor.nbpar = 1
        stor.par[0] = { deb: j3pGetRandomInt(0, e.nb - 1) }
        stor.par[0].fin = j3pGetRandomInt(stor.par[0].deb + 1, e.nb)
        stor.par[1] = { deb: -1, fin: -1 }
        break
      case '++':
        stor.nbpar = 2
        stor.par[0] = { deb: j3pGetRandomInt(0, e.nb - 3) }
        stor.par[0].fin = j3pGetRandomInt(stor.par[0].deb + 1, e.nb - 2)
        stor.par[1] = { deb: j3pGetRandomInt(stor.par[0].fin + 1, e.nb - 1) }
        stor.par[1].fin = j3pGetRandomInt(stor.par[1].deb + 1, e.nb)
        break
      case '+-':
        stor.nbpar = 2
        stor.par[0] = { deb: j3pGetRandomInt(0, e.nb - 3) }
        stor.par[0].fin = j3pGetRandomInt(stor.par[0].deb + 1, e.nb - 2)
        stor.par[1] = { deb: j3pGetRandomInt(stor.par[0].fin + 1, e.nb - 1) }
        stor.par[1].fin = j3pGetRandomInt(stor.par[1].deb + 1, e.nb)
        break
      case '-+':
        stor.nbpar = 2
        stor.par[0] = { deb: j3pGetRandomInt(0, e.nb - 3) }
        stor.par[0].fin = j3pGetRandomInt(stor.par[0].deb + 1, e.nb - 2)
        stor.par[1] = { deb: j3pGetRandomInt(stor.par[0].fin + 1, e.nb - 1) }
        stor.par[1].fin = j3pGetRandomInt(stor.par[1].deb + 1, e.nb)
        break
      case '--':
        stor.nbpar = 2
        stor.par[0] = { deb: j3pGetRandomInt(0, e.nb - 3) }
        stor.par[0].fin = j3pGetRandomInt(stor.par[0].deb + 1, e.nb - 2)
        stor.par[1] = { deb: j3pGetRandomInt(stor.par[0].fin + 1, e.nb - 1) }
        stor.par[1].fin = j3pGetRandomInt(stor.par[1].deb + 1, e.nb)
        break
    }
    if (ds.Question.indexOf('Supp') === -1 && ds.Question !== 'les trois') {
      stor.nbpar = 0
      stor.par = [[-1, -1], [-1, -1]]
    }
    let posu = ['n', 'x', 'y']
    if (ds.carre) posu = posu.concat(['x²', 'y²'])
    let nu = j3pGetRandomInt(0, posu.length - 1)
    stor.u1 = { koi: posu[nu], ou: [] }
    posu.splice(nu, 1)
    nu = j3pGetRandomInt(0, posu.length - 1)
    stor.u2 = { koi: posu[nu], ou: [] }
    posu.splice(nu, 1)
    nu = j3pGetRandomInt(0, posu.length - 1)
    stor.u3 = { koi: posu[nu], ou: [] }
    let lp = [stor.u1.koi, stor.u2.koi, stor.u3.koi]
    lp = j3pShuffle(lp)
    for (let i = 0; i < stor.lestermes.length; i++) {
      stor.lestermes[i].koi = lp[i % lp.length]
      if (stor.lestermes[i].koi === stor.u1.koi) stor.u1.ou.push(stor.lestermes[i])
      if (stor.lestermes[i].koi === stor.u2.koi) stor.u2.ou.push(stor.lestermes[i])
      if (stor.lestermes[i].koi === stor.u3.koi) stor.u3.ou.push(stor.lestermes[i])
    }
    stor.lestermes = j3pShuffle(stor.lestermes)
    stor.restricbase = 'xy0123456789+- '
    if (!ds.entier) stor.restricbase += '.,'
    if ((lp.indexOf('x²') !== -1) || (lp.indexOf('y²') !== -1)) {
      stor.clvspe = true
      stor.restricbase += '²'
    }
    if ((ds.Detail_deplace) && (ds.Question.indexOf('Réduis') !== -1 || ds.Question === 'les trois')) {
      let ok = false
      let sto = 0
      do {
        sto++
        const tabvu = []
        let enc = ''
        for (let i = 0; i < stor.lestermes.length; i++) {
          if (tabvu.indexOf(stor.lestermes[i].koi) !== -1 && enc !== stor.lestermes[i].koi) {
            ok = true
          }
          tabvu.push(stor.lestermes[i].koi)
          enc = stor.lestermes[i].koi
        }
        if (!ok) {
          const buf = stor.lestermes.pop()
          stor.lestermes.splice(0, 0, buf)
        }
      } while (!ok && sto < 1000)
    }
    stor.lestermesatt = copiede(stor.lestermes)
    let iw = 'n'
    let pos = 'av'
    for (let i = 0; i < stor.lestermesatt.length; i++) {
      if (stor.par[0].fin === i) {
        iw = 'n'
        pos = 'ap1'
      }
      if (stor.par[1].fin === i) {
        iw = 'n'
        pos = 'ap2'
      }
      if (stor.par[0].deb === i) {
        iw = e.cas[0]
        pos = 'in1'
      }
      if (stor.par[1].deb === i) {
        iw = e.cas[1]
        pos = 'in2'
      }
      stor.lestermesatt[i].dou = iw
      stor.lestermes[i].pos = pos
      if (iw === '-') stor.lestermesatt[i].signe = !stor.lestermesatt[i].signe
    }
    stor.termesfin = []
    const uvu = []
    for (let i = 0; i < stor.lestermesatt.length; i++) {
      if (uvu.indexOf(stor.lestermesatt[i].koi) === -1) {
        stor.termesfin.push({ koi: stor.lestermesatt[i].koi, val: 0 })
        uvu.push(stor.lestermesatt[i].koi)
      }
    }
    for (let i = 0; i < stor.lestermesatt.length; i++) {
      for (let j = 0; j < stor.termesfin.length; j++) {
        if (stor.termesfin[j].koi === stor.lestermesatt[i].koi) {
          if (stor.lestermesatt[i].signe) { stor.termesfin[j].val = j3pArrondi(stor.termesfin[j].val + stor.lestermesatt[i].val, 1) } else {
            stor.termesfin[j].val = j3pArrondi(stor.termesfin[j].val - stor.lestermesatt[i].val, 1)
          }
        }
      }
    }
    stor.first = true
    return e
  }
  function poseQuestion () {
    j3pEmpty(stor.lesdiv.correction)
    j3pEmpty(stor.lesdiv.solution)
    j3pEmpty(stor.lesdiv.explications)
    if (stor.first) {
      stor.first = false
      stor.foco = false
      const tabGroo = addDefaultTable(stor.lesdiv.travail, 4, 1)
      stor.tab2 = addDefaultTable(tabGroo[0][0], 2, 24)
      stor.tabfd = addDefaultTable(tabGroo[0][0], 1, 3)
      stor.tab3 = addDefaultTable(tabGroo[1][0], 1, 3)
      stor.tab4 = addDefaultTable(tabGroo[2][0], 1, 3)
      stor.tab5 = addDefaultTable(tabGroo[3][0], 1, 3)
      j3pAffiche(stor.tab2[0][0], null, '$A$')
      j3pAffiche(stor.tab2[0][1], null, '$=$')
      if (stor.lafaire.indexOf('Simp') !== -1) {
        if (stor.lafaire.indexOf('Sup') === -1) {
          stor.aff = Affichemoi3(stor.lestermes)
          j3pAffiche(stor.tab2[0][2], null, '$' + stor.aff + '$')
        } else {
          const ltab = copiede(stor.lestermes)
          stor.Aff = Affichemoi3(ltab).replace(/ \\times 1 /g, ' ').replace(/ 1 \\times /g, ' ')
          for (let i = 0; i < stor.affS.length; i++) {
            j3pAffiche(stor.tab2[0][i + 2], null, '$' + stor.affS[i].replace(/ \\times 1 /g, ' ').replace(/ 1 \\times /g, ' ') + '$')
          }
        }
      } else if (stor.lafaire.indexOf('Sup') !== -1) {
        stor.aff = Affichemoi(stor.lestermes, false)
        j3pAffiche(stor.tab2[0][2], null, '$' + stor.aff + '$')
      } else {
        stor.aff = Affichemoi2(stor.lestermesatt)
        j3pAffiche(stor.tab2[0][2], null, '$' + stor.aff + '$')
      }
    }
    if (stor.foco) {
      stor.foco = false
      switch (stor.encours) {
        case 'Simp':
          for (let i = 0; i < 25; i++) {
            j3pEmpty(stor.tab2[1][i])
          }
          j3pAffiche(stor.tabfd[0][0], null, '$A$')
          j3pAffiche(stor.tabfd[0][1], null, '$=$')
          if (stor.lafaire.indexOf('Sup') !== -1) {
            stor.aff = Affichemoi(stor.lestermes, true)
            j3pAffiche(stor.tabfd[0][2], null, '$' + stor.aff + '$')
          } else {
            stor.aff = Affichemoi2(stor.lestermesatt)
            j3pAffiche(stor.tabfd[0][2], null, '$' + stor.aff + '$')
          }
          stor.tabfd[0][2].style.color = me.styles.petit.correction.color
          break
        case 'Sup':
          j3pEmpty(stor.tab3[0][2])
          stor.aff = Affichemoi2(stor.lestermesatt)
          j3pAffiche(stor.tab3[0][2], null, '$' + stor.aff + '$')
          stor.tab3[0][2].style.color = me.styles.petit.correction.color

          break
        case 'Dep':
          j3pEmpty(stor.tab4[0][2])
          j3pAffiche(stor.tab4[0][2], null, '$ ' + stor.aff + '$')
          stor.tab4[0][2].style.color = me.styles.petit.correction.color
      }
    }
    stor.encours = Suiv()
    switch (stor.encours) {
      case 'Simp':
        poseSimpli()
        break
      case 'Red':
        poseReduc()
        break
      case 'Sup':
        poseSuppri()
        break
      case 'Dep':
        poseDepla()
    }
  }

  function Suiv () {
    let i = stor.lafaire.indexOf(stor.encours) + 1
    if (i === stor.lafaire.length) i = 0
    return stor.lafaire[i]
  }
  function creeLesDiv () {
    stor.lesdiv = {}
    stor.lesdiv.conteneur = j3pAddElt(me.zonesElts.MG, 'div', {
      style: me.styles.etendre('toutpetit.enonce', { padding: '10px' })
    })
    const consigne1 = addDefaultTable(stor.lesdiv.conteneur, 1, 3)
    stor.lesdiv.consigne = consigne1[0][0]
    stor.lesdiv.correction = consigne1[0][2]
    consigne1[0][1].style.width = '40px'
    const travail1 = addDefaultTable(stor.lesdiv.conteneur, 1, 3)
    stor.lesdiv.travail = travail1[0][0]
    stor.lesdiv.calculatrice = travail1[0][2]
    travail1[0][1].style.width = '40px'
    stor.lesdiv.zonesigne = j3pAddElt(stor.lesdiv.travail, 'div')
    stor.lesdiv.zonedistance = j3pAddElt(stor.lesdiv.travail, 'div')
    stor.lesdiv.zoneresultat = j3pAddElt(stor.lesdiv.travail, 'div')
    stor.lesdiv.explications = j3pAddElt(stor.lesdiv.conteneur, 'div')
    stor.lesdiv.solution = j3pAddElt(stor.lesdiv.conteneur, 'div')
    stor.lesdiv.espace = j3pAddElt(stor.lesdiv.conteneur, 'div')
    j3pAffiche(stor.lesdiv.espace, null, '\n')
    stor.lesdiv.suite = j3pAddElt(stor.lesdiv.conteneur, 'div')
    stor.lesdiv.calculatrice = j3pAddElt(stor.lesdiv.correction, 'div')
    stor.lesdiv.explications.style.color = me.styles.cfaux
    stor.lesdiv.solution.style.color = me.styles.toutpetit.correction.color
    me.zonesElts.MG.classList.add('fond')
  }
  function Affichemoi (tab, b) {
    let aret = ''
    let cpt = 0
    let cpt2 = 0
    let cdeb = true
    let i // on l’utilise après le for
    for (i = 0; i < tab.length; i++) {
      if (stor.par[0].deb === i) {
        if (stor.Lexo.cas[0] === '+') {
          if (b) { aret += stor.souvps[cpt]; cpt++ } else {
            if (!cdeb || j3pGetRandomBool()) { aret += '+(' } else { aret += '(' }
          }
        } else {
          aret += '-('
        }
        cdeb = true
      }
      if (stor.par[0].fin === i) {
        aret += ')'
      }
      if (stor.par[1].fin === i) {
        aret += ')'
      }
      if (stor.par[1].deb === i) {
        if (stor.Lexo.cas[1] === '+') {
          if (b) { aret += stor.souvps[cpt]; cpt++ } else {
            if (!cdeb || j3pGetRandomBool()) { aret += '+(' } else { aret += '(' }
          }
        } else {
          aret += '-('
        }
        cdeb = true
      }
      if (!tab[i].signe) { aret += '-' } else {
        if (b) { aret += stor.souvps2[cpt2]; cpt2++ } else {
          if (!cdeb || j3pGetRandomBool()) aret += '+'
        }
      }
      cdeb = false
      if (tab[i].koi === 'n') {
        aret += String(tab[i].val).replace('.', ',')
      } else {
        if (tab[i].val === 1) {
          aret += tab[i].koi
        } else {
          aret += String(tab[i].val).replace('.', ',') + tab[i].koi
        }
      }
    }
    if (stor.par[0].fin === i) {
      aret += ')'
    }
    if (stor.par[1].fin === i) {
      aret += ')'
    }
    return aret
  }
  function Affichemoi3 (tab) {
    let aret = ''
    let kb, bufk
    let cdeb = true
    stor.souvps = []
    stor.souvps2 = []
    stor.affS = []
    const yap = ((ds.Question !== 'les trois') && (ds.Question.indexOf('Supprime') === -1))
    bufk = ''
    let i // on l’utilise après le for
    for (i = 0; i < tab.length; i++) {
      if (!yap) {
        if (stor.par[0].deb === i) {
          if (stor.Lexo.cas[0] === '+') {
            if (!cdeb || j3pGetRandomBool()) {
              stor.souvps.push('+(')
              aret += '+('
              if (bufk !== '') stor.affS.push(bufk)
              bufk = ''
              stor.affS.push('+(')
            } else {
              stor.souvps.push('(')
              aret += '('
              if (bufk !== '') stor.affS.push(bufk)
              bufk = ''
              stor.affS.push('(')
            }
          } else {
            aret += '-('
            if (bufk !== '') stor.affS.push(bufk)
            bufk = ''
            stor.affS.push('-(')
          }
          cdeb = true
        }
        if (stor.par[0].fin === i) {
          aret += ') '
          if (bufk !== '') stor.affS.push(bufk)
          bufk = ''
          stor.affS.push(') ')
        }
        if (stor.par[1].fin === i) {
          aret += ') '
          if (bufk !== '') stor.affS.push(bufk)
          bufk = ''
          stor.affS.push(') ')
        }
        if (stor.par[1].deb === i) {
          if (stor.Lexo.cas[1] === '+') {
            if (!cdeb || j3pGetRandomBool()) {
              stor.souvps.push('+(')
              aret += '+('
              if (bufk !== '') stor.affS.push(bufk)
              bufk = ''
              stor.affS.push('+(')
            } else {
              stor.souvps.push('(')
              aret += '('
              if (bufk !== '') stor.affS.push(bufk)
              bufk = ''
              stor.affS.push('(')
            }
          } else {
            aret += '-('
            if (bufk !== '') stor.affS.push(bufk)
            bufk = ''
            stor.affS.push('-(')
          }
          cdeb = true
        }
      }
      if (!tab[i].signe) { aret += '- '; stor.affS.push('- ') } else {
        if (!yap) {
          if (!cdeb || j3pGetRandomBool()) {
            aret += '+'; stor.souvps2.push('+ '); stor.affS.push('+ ')
          } else {
            stor.souvps2.push('')
          }
        } else {
          if (!cdeb || j3pGetRandomBool()) { aret += '+ '; stor.affS.push('+ ') }
        }
      }
      cdeb = false
      if (tab[i].koi === 'n') {
        aret += ' ' + String(tab[i].val1).replace('.', ',') + ' \\times ' + String(tab[i].val2).replace('.', ',') + ' '
        bufk += ' ' + String(tab[i].val1).replace('.', ',') + ' \\times ' + String(tab[i].val2).replace('.', ',') + ' '
      } else {
        if (tab[i].koi.indexOf('²') !== -1) {
          kb = ' \\times ' + tab[i].koi[0] + ' '
        } else { kb = ' ' }
        switch (j3pGetRandomInt(0, 3)) {
          case 0:
            aret += ' ' + String(tab[i].val1).replace('.', ',') + ' \\times ' + String(tab[i].val2).replace('.', ',') + ' \\times ' + tab[i].koi[0] + kb
            bufk += ' ' + String(tab[i].val1).replace('.', ',') + ' \\times ' + String(tab[i].val2).replace('.', ',') + ' \\times ' + tab[i].koi[0] + kb
            break
          case 1:
            aret += ' ' + String(tab[i].val2).replace('.', ',') + ' \\times ' + tab[i].koi[0] + ' \\times ' + String(tab[i].val1).replace('.', ',') + kb
            bufk += ' ' + String(tab[i].val2).replace('.', ',') + ' \\times ' + tab[i].koi[0] + ' \\times ' + String(tab[i].val1).replace('.', ',') + kb
            break
          case 2:
            aret += tab[i].koi[0] + ' \\times ' + String(tab[i].val2).replace('.', ',') + ' \\times ' + String(tab[i].val1).replace('.', ',') + kb
            bufk += tab[i].koi[0] + ' \\times ' + String(tab[i].val2).replace('.', ',') + ' \\times ' + String(tab[i].val1).replace('.', ',') + kb
            break
          case 3:
            aret += tab[i].koi[0] + kb + ' \\times ' + String(tab[i].val2).replace('.', ',') + ' \\times ' + String(tab[i].val1).replace('.', ',') + ' '
            bufk += tab[i].koi[0] + kb + ' \\times ' + String(tab[i].val2).replace('.', ',') + ' \\times ' + String(tab[i].val1).replace('.', ',') + ' '
            break
        }
      }
      stor.affS.push(bufk)
      bufk = ''
    }
    if (!yap) {
      if (stor.par[0].fin === i) {
        aret += ') '
        if (bufk !== '') stor.affS.push(bufk)
        bufk = ''
        stor.affS.push(') ')
      }
      if (stor.par[1].fin === i) {
        aret += ') '
        if (bufk !== '') stor.affS.push(bufk)
        bufk = ''
        stor.affS.push(') ')
      }
    }
    if (bufk !== '') stor.affS.push(bufk)
    return aret
  }
  function Affichemoi2 (tab) {
    let aret = ''
    let cdeb = true
    for (let i = 0; i < tab.length; i++) {
      if (!tab[i].signe) { aret += '-' } else {
        if (!cdeb) aret += '+'
      }
      cdeb = false
      if (tab[i].koi === 'n') {
        aret += String(tab[i].val).replace('.', ',')
      } else {
        if (tab[i].val === 1) {
          aret += tab[i].koi
        } else {
          aret += String(tab[i].val).replace('.', ',') + tab[i].koi
        }
      }
    }
    return aret
  }
  function Affichemoi4 (tab) {
    let aret = ''
    if (tab.koi === 'n') {
      aret += String(tab.val).replace('.', ',')
    } else {
      if (tab.val === 1) {
        aret += tab.koi
      } else {
        aret += String(tab.val).replace('.', ',') + tab.koi
      }
    }

    return aret
  }
  function poseDepla () {
    stor.restric = stor.restricbase
    let ph = '<b>A présent, tu dois déplacer les termes pour pouvoir ensuite réduire l’expression $A$</b><br><i>Aucun calcul n’est attendu à cette étape.</i><br>'
    if (stor.lafaire.indexOf('Simp') === -1 && stor.lafaire.indexOf('Sup') === -1) ph = '<b>Tu dois déplacer les termes pour pouvoir ensuite réduire l’expression $A$</b><br><i>Aucun calcul n’est attendu à cette étape</i><br>'
    j3pEmpty(stor.lesdiv.consigne)
    j3pAffiche(stor.lesdiv.consigne, null, ph)
    j3pAffiche(stor.lesdiv.consigne, null, '\n')
    j3pAffiche(stor.tab4[0][0], null, '$A$')
    j3pAffiche(stor.tab4[0][1], null, '$=$')
    stor.moncal3 = new ZoneStyleMathquill3(stor.tab4[0][2], { restric: stor.restric, limitenb: 20, limite: 40, hasAutoKeyboard: stor.clvspe, enter: me.sectionCourante.bind(me), inverse: true })
    stor.lesdiv.solution.classList.remove('correction')
    stor.lesdiv.explications.classList.remove('explique')
    stor.moncal3.focus()
  }
  function poseSuppri () {
    stor.restric = stor.restricbase
    let ph = '<b>A présent, tu dois supprimer les parenthèses dans l’expression $A$</b><br><i>Aucun déplacement ni calcul n’est attendu à cette étape.</i><br>'
    if (stor.lafaire.indexOf('Simp') === -1) ph = '<b>Tu dois supprimer les parenthèses dans l’expression $A$</b><br><i>Aucun calcul ni déplacement n’est attendu à cette étape</i><br>'
    j3pEmpty(stor.lesdiv.consigne)
    j3pAffiche(stor.lesdiv.consigne, null, ph)
    j3pAffiche(stor.lesdiv.consigne, null, '\n')
    j3pAffiche(stor.tab3[0][0], null, '$A$')
    j3pAffiche(stor.tab3[0][1], null, '$=$')
    stor.moncal = new ZoneStyleMathquill3(stor.tab3[0][2], { restric: stor.restric, limitenb: 20, limite: 45, hasAutoKeyboard: stor.clvspe, enter: me.sectionCourante.bind(me), inverse: true })
    stor.lesdiv.solution.classList.remove('correction')
    stor.lesdiv.explications.classList.remove('explique')
    stor.moncal.focus()
  }
  function poseReduc () {
    stor.restric = stor.restricbase
    let ph = '<b>Tu dois réduire l’expression $A$</b><br>'
    j3pEmpty(stor.lesdiv.consigne)
    if (stor.lafaire.length > 1) ph = '<b>Enfin, tu dois réduire l’expression $A$</b><br><br>'
    j3pAffiche(stor.lesdiv.consigne, null, ph)
    j3pAffiche(stor.tab5[0][0], null, '$A$')
    j3pAffiche(stor.tab5[0][1], null, '$=$')
    stor.moncal2 = new ZoneStyleMathquill3(stor.tab5[0][2], { restric: stor.restric, limitenb: 20, limite: 40, hasAutoKeyboard: stor.clvspe, enter: me.sectionCourante.bind(me), inverse: true })
    stor.lesdiv.solution.classList.remove('correction')
    stor.lesdiv.explications.classList.remove('explique')
  }
  function poseSimpli () {
    stor.restric = stor.restricbase
    let ph = '<b>Tu dois simplifier les produits dans l’expression $A$</b><br>'
    if (stor.Lexo.nb > 1) ph += '<i>Chaque terme doit rester à sa place pour cette étape</i><br>'

    j3pAffiche(stor.lesdiv.consigne, null, ph)
    j3pAffiche(stor.lesdiv.consigne, null, '\n')

    // j3pAffiche('TRATRAVl0c2', null, '$' + stor.Aff + '$')
    j3pAffiche(stor.tab2[1][0], null, '$A$')
    j3pAffiche(stor.tab2[1][1], null, '$=$')
    if (stor.lafaire.indexOf('Sup') !== -1) {
      // recopie parentheses et signe devant
      // au pire 5 zone
      stor.zonetab = []
      // gere av
      stor.souv1 = []
      for (let i = 0; i < stor.lestermes.length; i++) {
        if (stor.lestermes[i].pos === 'av') { stor.souv1.push(stor.lestermes[i]) }
      }
      if (stor.souv1.length > 0) {
        stor.zonetab.push('Av')
      }
      stor.zonetab.push('p1Ouvre')
      stor.zonetab.push('in1')
      stor.souv2 = []
      for (let i = 0; i < stor.lestermes.length; i++) {
        if (stor.lestermes[i].pos === 'in1') { stor.souv2.push(stor.lestermes[i]) }
      }
      stor.zonetab.push('p1ferm')
      stor.souv3 = []
      for (let i = 0; i < stor.lestermes.length; i++) {
        if (stor.lestermes[i].pos === 'ap1') { stor.souv3.push(stor.lestermes[i]) }
      }
      if (stor.souv3.length > 0) {
        stor.yaAp1 = true
        stor.zonetab.push('Ap1')
      }
      if (stor.nbpar === 2) {
        if (stor.zonetab[stor.zonetab.length - 1] === 'p1ferm') { stor.zonetab[stor.zonetab.length - 1] = 'p1top2' } else { stor.zonetab.push('p2ouvre') }
        stor.zonetab.push('in2')
        stor.souv4 = []
        for (let i = 0; i < stor.lestermes.length; i++) {
          if (stor.lestermes[i].pos === 'in2') { stor.souv4.push(stor.lestermes[i]) }
        }
        stor.zonetab.push('p2ferm')
        stor.souv5 = []
        for (let i = 0; i < stor.lestermes.length; i++) {
          if (stor.lestermes[i].pos === 'ap2') { stor.souv5.push(stor.lestermes[i]) }
        }
        if (stor.souv5.length > 0) {
          stor.yaAp2 = true
          stor.zonetab.push('Ap2')
        }
      }
      stor.monvalAv = undefined
      stor.moncalIn1 = undefined
      stor.moncalIAp1 = undefined
      stor.moncalIn2 = undefined
      stor.moncalIAp2 = undefined
      /*
      for (let i = 0; i < stor.zonetab.length; i++) {
        switch (stor.zonetab[i]) {
          case 'Av':
            stor.monvalAv = []
            for (let j = 0; j < stor.lestermes.length; j++) {
              if (stor.lestermes[j].pos === 'av') {
                stor.monvalAv.push(new ZoneStyleMathquill3(stor.tab2[1][groCompt + 2], { restric: stor.restric, limitenb: 20, limite: 40, hasAutoKeyboard: true, enter: me.sectionCourante.bind(me), inverse: true }))
                groCompt++
              }
            }
            break
          case 'p1Ouvre':
            if (stor.Lexo.cas[0] === '+') {
              j3pAffiche(stor.tab2[1][groCompt + 2], null, '$' + stor.souvps[ccpt] + '$')
              ccpt++
            } else {
              j3pAffiche(stor.tab2[1][groCompt + 2], null, '$-($')
            }
            groCompt++
            break
          case 'in1':
            stor.moncalIn1 = []
            for (let j = 0; j < stor.lestermes.length; j++) {
              if (stor.lestermes[j].pos === 'in1') {
                stor.moncalIn1.push(new ZoneStyleMathquill3(stor.tab2[1][groCompt + 2], { restric: stor.restric, limitenb: 20, limite: 40, hasAutoKeyboard: true, enter: me.sectionCourante.bind(me), inverse: true }))
                groCompt++
              }
            }
            break
          case 'p1ferm':
            j3pAffiche(stor.tab2[1][groCompt + 2], null, '$)$')
            groCompt++
            break
          case 'p1top2':
            if (stor.Lexo.cas[1] === '+') {
              j3pAffiche(stor.tab2[1][groCompt + 2], null, '$)+($')
              ccpt++
              groCompt++
            } else {
              j3pAffiche(stor.tab2[1][groCompt + 2], null, '$)-($')
              groCompt++
            }
            break
          case 'p2ouvre':
            if (stor.Lexo.cas[1] === '+') {
              j3pAffiche(stor.tab2[1][groCompt + 2], null, '$' + stor.souvps[ccpt] + '$')
              ccpt++
              groCompt++
            } else {
              j3pAffiche(stor.tab2[1][groCompt + 2], null, '$-($')
              groCompt++
            }
            break
          case 'Ap1':
            stor.moncalIAp1 = []
            for (let j = 0; j < stor.lestermes.length; j++) {
              if (stor.lestermes[j].pos === 'ap1') {
                stor.moncalIAp1.push(new ZoneStyleMathquill3(stor.tab2[1][groCompt + 2], { restric: stor.restric, limitenb: 20, limite: 40, hasAutoKeyboard: true, enter: me.sectionCourante.bind(me), inverse: true }))
                groCompt++
              }
            }
            break
          case 'in2':
            stor.moncalIn2 = []
            for (let j = 0; j < stor.lestermes.length; j++) {
              if (stor.lestermes[j].pos === 'in2') {
                stor.moncalIn2.push(new ZoneStyleMathquill3(stor.tab2[1][groCompt + 2], { restric: stor.restric, limitenb: 20, limite: 40, hasAutoKeyboard: true, enter: me.sectionCourante.bind(me), inverse: true }))
                groCompt++
              }
            }
            break
          case 'p2ferm':
            j3pAffiche(stor.tab2[1][groCompt + 2], null, '$)$')
            groCompt++
            break
          case 'Ap2':
            stor.moncalIAp2 = []
            for (let j = 0; j < stor.lestermes.length; j++) {
              if (stor.lestermes[j].pos === 'ap2') {
                stor.moncalIAp2.push(new ZoneStyleMathquill3(stor.tab2[1][groCompt + 2], { restric: stor.restric, limitenb: 20, limite: 40, hasAutoKeyboard: true, enter: me.sectionCourante.bind(me), inverse: true }))
                groCompt++
              }
            }
            break
          default:
            j3pShowError(Error(`Cas non prévu : ${stor.zonetab[i]}`), { message: 'Erreur interne (cas non prévu)', mustNotify: true })
        }
      }
      */
      stor.lilirep = []
      for (let i = 0; i < stor.affS.length; i++) {
        const afx = stor.affS[i]
        if (afx.indexOf('+') !== -1 || afx.indexOf('-') !== -1 || afx.indexOf(')') !== -1 || afx.indexOf('(') !== -1) {
          j3pAffiche(stor.tab2[1][i + 2], null, '$' + afx + '$')
        } else {
          stor.lilirep.push(new ZoneStyleMathquill3(stor.tab2[1][i + 2], { restric: stor.restric.replace('+', '').replace('-', ''), limitenb: 20, limite: 40, hasAutoKeyboard: true, enter: me.sectionCourante.bind(me), inverse: true }))
        }
      }
    } else {
      stor.complik = false
      stor.moncal1 = new ZoneStyleMathquill3(stor.tab2[1][2], { restric: stor.restric, limitenb: 20, limite: 40, hasAutoKeyboard: true, enter: me.sectionCourante.bind(me), inverse: true })
    }
    if (ds.theme === 'zonesAvecImageDeFond') {
      stor.lesdiv.solution.classList.remove('correction')
      stor.lesdiv.explications.classList.remove('explique')
    }
  }
  function copiede (tab) {
    const ret = []
    for (let i = 0; i < tab.length; i++) {
      ret[i] = j3pClone(tab[i])
    }
    return ret
  }
  function boutonfen () {
    j3pToggleFenetres('Calculatrice')
  }
  function termegal (k, l, b) {
    return ((k.signe === l.signe) || b) && (k.val === l.val) && (k.koi === l.koi)
  }

  function yaReponse () {
    if (stor.encours === 'Sup') {
      if (stor.moncal.reponse() === '') {
        stor.moncal.focus()
        return false
      }
    }
    if (stor.encours === 'Red') {
      if (stor.moncal2.reponse() === '') {
        stor.moncal2.focus()
        return false
      }
    }
    if (stor.encours === 'Simp') {
      if ((ds.Question === 'les trois') || (ds.Question.indexOf('Supprime') !== -1)) {
        for (let i = 0; i < stor.lilirep.length; i++) {
          if (stor.lilirep[i].reponse() === '') {
            stor.lilirep[i].focus()
            return false
          }
        }
      } else {
        if (stor.moncal1.reponse() === '') {
          stor.moncal1.focus()
          return false
        }
      }
    }
    if (stor.encours === 'Dep') {
      if (stor.moncal3.reponse() === '') {
        stor.moncal3.focus()
        return false
      }
    }
    return true
  }
  function isRepOk () {
    stor.errEcrit = false
    stor.errRemdEcrit = []

    stor.errTermMank = false
    stor.TermMankList = []
    stor.errTermTrop = false
    stor.errBouge = false
    stor.errSigne = false
    stor.lerrS = []
    stor.errTermInc = false
    stor.errDeuxPareil = false
    stor.errDisp = false
    stor.err0 = false
    stor.errReduc = false
    stor.errSigneP = false
    stor.errBis = false

    stor.errPafini = false
    stor.errRemed = ''
    stor.ErrPaRegroup = false
    stor.errPaBienRange = false
    stor.laligf = undefined
    stor.nedco = false
    stor.errReg = false
    stor.errpasDet = false

    let larep, tabrep, test, bufrep, ok
    if (stor.encours === 'Sup') {
      larep = stor.moncal.reponse()
      tabrep = larep.split(/[+-]/)
      /// separe signe et reste
      // verif alternance un signe et reste
      // verif fini pas par signe
      for (let j = 1; j < tabrep.length; j++) {
        if (tabrep[j] === '') {
          if (j === tabrep.length - 1) {
            stor.errEcrit = true
            stor.errRemdEcrit.push('La ligne finit par un signe !')
          } else {
            stor.errRemdEcrit.push('2 signes se suivent !')
            stor.errEcrit = true
          }
        }
      }

      // verif reste cest des nb bien ecrit
      for (let j = 0; j < tabrep.length; j++) {
        bufrep = tabrep[j]
        if ((bufrep.indexOf('²') !== -1) && (bufrep.indexOf('²') !== bufrep.length - 1)) {
          stor.errEcrit = true
          stor.errRemdEcrit.push('Erreur d’écriture')
          return false
        }
        bufrep = bufrep.replace('²', '')
        if ((bufrep.indexOf('x') !== -1) && (bufrep.indexOf('y') !== -1)) {
          stor.errEcrit = true
          stor.errRemdEcrit.push('Erreur d’écriture')
          return false
        }
        if ((bufrep.indexOf('x') !== -1) && (bufrep.indexOf('x') !== bufrep.length - 1)) {
          stor.errEcrit = true
          stor.errRemdEcrit.push('Erreur d’écriture')
          return false
        }
        bufrep = bufrep.replace('x', '')
        if ((bufrep.indexOf('y') !== -1) && (bufrep.indexOf('y') !== bufrep.length - 1)) {
          stor.errEcrit = true
          stor.errRemdEcrit.push('Erreur d’écriture')
          return false
        }
        bufrep = bufrep.replace('y', '')

        if (bufrep !== '') {
          test = verifNombreBienEcrit(bufrep, false, true)
          if (!test.good) {
            stor.errEcrit = true
            stor.errRemdEcrit.push(test.remede)
          }
        }
      }

      if (stor.errEcrit) {
        return false
      }

      // cree liste termes pour chaque ligne
      const signtab = []

      let sb = larep
      let jd = 0
      if (tabrep[0] === '') {
        if (ds.simplifie && (larep[0] === '+')) {
          stor.errEcrit = true
          stor.errRemdEcrit = ['Tu peux supprimer le signe + !']
          return false
        }
        tabrep.splice(0, 1)
      } else {
        jd = 1
        signtab.push('+')
        sb = sb.substring(sb.indexOf(tabrep[0]) + tabrep[0].length)
      }
      for (let j = jd; j < tabrep.length; j++) {
        signtab.push(sb[0])
        sb = sb.substring(1)
        sb = sb.substring(sb.indexOf(tabrep[j]) + tabrep[j].length)
      }

      stor.lestermesrep = []
      for (let j = 0; j < tabrep.length; j++) {
        bufrep = tabrep[j].replace(',', '.').replace('x', '').replace('y', '').replace('²', '')
        if (bufrep === '') bufrep = '1'
        stor.lestermesrep.push({
          signe: signtab[j] === '+',
          val: parseFloat(bufrep)
        })
        if (tabrep[j].indexOf('x²') !== -1) {
          stor.lestermesrep[stor.lestermesrep.length - 1].koi = 'x²'
        } else if (tabrep[j].indexOf('y²') !== -1) {
          stor.lestermesrep[stor.lestermesrep.length - 1].koi = 'y²'
        } else if (tabrep[j].indexOf('y') !== -1) {
          stor.lestermesrep[stor.lestermesrep.length - 1].koi = 'y'
        } else if (tabrep[j].indexOf('x') !== -1) {
          stor.lestermesrep[stor.lestermesrep.length - 1].koi = 'x'
        } else {
          stor.lestermesrep[stor.lestermesrep.length - 1].koi = 'n'
        }
      }

      // verif touts les mm termes
      for (let i = 0; i < stor.lestermes.length; i++) {
        ok = false
        for (let j = 0; j < stor.lestermesrep.length; j++) {
          ok = ok || termegal(stor.lestermesrep[j], stor.lestermes[i], true)
        }
        if (!ok) {
          stor.errTermMank = true
          stor.TermMankList.push(stor.lestermes[i])
        }
      }
      if (stor.errTermMank) return false

      if (stor.lestermes.length !== stor.lestermesrep.length) {
        if (stor.lestermes.length < stor.lestermesrep.length) stor.errTermTrop = true
        return false
      }

      // verif pas bouge
      for (let i = 0; i < stor.lestermes.length; i++) {
        if (!termegal(stor.lestermesrep[i], stor.lestermes[i], true)) {
          stor.errBouge = true
          return false
        }
      }

      // verif bon signe
      for (let i = 0; i < stor.lestermesatt.length; i++) {
        if (!termegal(stor.lestermesatt[i], stor.lestermesrep[i], false)) {
          stor.errSigne = true
          stor.lerrS = [stor.lestermesatt[i].dou, stor.lestermesrep[i]]
          return false
        }
      }

      return true
    }
    if (stor.encours === 'Simp') {
      if ((ds.Question === 'les trois') || (ds.Question.indexOf('Supprime') !== -1)) {
        stor.cons11 = ''
        larep = ''
        let compt = 0
        tabrep = []
        for (let i = 0; i < stor.affS.length; i++) {
          const afx = stor.affS[i]
          if (afx.indexOf('+') !== -1 || afx.indexOf('-') !== -1 || afx.indexOf(')') !== -1 || afx.indexOf('(') !== -1) {
            stor.cons11 += afx
            larep += afx
          } else {
            tabrep.push(stor.lilirep[compt].reponse())
            stor.cons11 += stor.lilirep[compt].reponse()
            larep += stor.lilirep[compt].reponse()
            compt++
          }
        }
        larep = larep.replace(/ /g, '')
        stor.cons11 = stor.cons11.replace(/ /g, '')
      } else {
        larep = stor.moncal1.reponse()
        stor.cons11 = larep
        tabrep = larep.split(/[+-]/)
      }

      /// separe signe et reste
      // verif alternance un signe et reste
      // verif fini pas par signe
      if (!((ds.Question !== 'les trois') || (ds.Question.indexOf('Supprime') !== -1))) {
        for (let j = 1; j < tabrep.length; j++) {
          if (tabrep[j] === '') {
            if (j === tabrep.length - 1) {
              stor.errEcrit = true
              stor.errRemdEcrit.push('La ligne finit par un signe !')
            } else {
              stor.errRemdEcrit.push('2 signes se suivent !')
              stor.errEcrit = true
            }
          }
        }
      }

      // verif reste cest des nb bien ecrit
      for (let j = 0; j < tabrep.length; j++) {
        bufrep = tabrep[j].replace(/\(/g, '').replace(/\)/g, '')
        if ((bufrep.indexOf('²') !== -1) && (bufrep.indexOf('²') !== bufrep.length - 1)) {
          stor.errEcrit = true
          stor.errRemdEcrit.push('Erreur d’écriture')
          if ((ds.Question === 'les trois') || (ds.Question.indexOf('Supprime') !== -1)) {
            stor.lilirep[j].corrige(false)
          }
          return false
        }
        let tt = ''
        if ((bufrep.indexOf('x²') !== -1) || (bufrep.indexOf('y²') !== -1)) tt = '²'
        bufrep = bufrep.replace('²', '')
        if ((bufrep.indexOf('x') !== -1) && (bufrep.indexOf('y') !== -1)) {
          stor.errEcrit = true
          stor.errRemdEcrit.push('Dans $A$ il n’y a pas de produit avec $x$ et $y$ !')
          if ((ds.Question === 'les trois') || (ds.Question.indexOf('Supprime') !== -1)) {
            stor.lilirep[j].corrige(false)
          }
          return false
        }
        // x suivi de qqchose
        if ((bufrep.indexOf('x') !== -1) && (bufrep.indexOf('x') !== bufrep.length - 1)) {
          stor.errEcrit = true
          if ((ds.Question === 'les trois') || (ds.Question.indexOf('Supprime') !== -1)) {
            stor.lilirep[j].corrige(false)
          }
          stor.errRemdEcrit.push('Dans le terme $' + tabrep[j] + '$ , il faut mettre $x' + tt + '$ à la fin !')
          return false
        }
        bufrep = bufrep.replace('x', '')
        if ((bufrep.indexOf('y') !== -1) && (bufrep.indexOf('y') !== bufrep.length - 1)) {
          stor.errEcrit = true
          if ((ds.Question === 'les trois') || (ds.Question.indexOf('Supprime') !== -1)) {
            stor.lilirep[j].corrige(false)
          }
          stor.errRemdEcrit.push('Dans le terme $' + tabrep[j] + '$ , il faut mettre $y' + tt + '$ à la fin !')
          return false
        }
        bufrep = bufrep.replace('y', '')

        if (bufrep.indexOf('×') !== -1) {
          stor.errEcrit = true
          if ((ds.Question === 'les trois') || (ds.Question.indexOf('Supprime') !== -1)) {
            stor.lilirep[j].corrige(false)
          }
          stor.errRemdEcrit.push('Tu dois supprimer tous les signes $\\times$ ! ')
          return false
        }
        bufrep = bufrep.replace('y', '')

        if (bufrep !== '') {
          test = verifNombreBienEcrit(bufrep, false, true)
          if (!test.good) {
            stor.errEcrit = true
            if ((ds.Question === 'les trois') || (ds.Question.indexOf('Supprime') !== -1)) {
              stor.lilirep[j].corrige(false)
            }
            stor.errRemdEcrit.push(test.remede)
          }
        }
      }

      if (stor.errEcrit) {
        return false
      }
      // cree liste termes pour chaque ligne
      const signtab = []

      let sb = larep
      let jd = 0
      if (tabrep[0] === '') {
        tabrep.splice(0, 1)
      } else {
        jd = 1
        signtab.push('+')
        sb = sb.substring(sb.indexOf(tabrep[0]) + tabrep[0].length)
      }
      for (let j = jd; j < tabrep.length; j++) {
        signtab.push(sb[0])
        sb = sb.substring(1)
        sb = sb.substring(sb.indexOf(tabrep[j]) + tabrep[j].length)
      }

      const acompare = copiede(stor.lestermes)
      if ((ds.Question === 'les trois') || (ds.Question.indexOf('Supprime') !== -1)) {
        for (let i = 0; i < acompare.length; i++) {
          acompare[i].signe = true
        }
      }
      stor.lestermesrep = []
      for (let j = 0; j < tabrep.length; j++) {
        bufrep = tabrep[j].replace(',', '.').replace('x', '').replace('y', '').replace('²', '')
        if (bufrep === '') bufrep = '1'
        stor.lestermesrep.push({
          signe: signtab[j] === '+',
          val: parseFloat(bufrep)
        })
        if (tabrep[j].indexOf('x²') !== -1) {
          stor.lestermesrep[stor.lestermesrep.length - 1].koi = 'x²'
        } else if (tabrep[j].indexOf('y²') !== -1) {
          stor.lestermesrep[stor.lestermesrep.length - 1].koi = 'y²'
        } else if (tabrep[j].indexOf('y') !== -1) {
          stor.lestermesrep[stor.lestermesrep.length - 1].koi = 'y'
        } else if (tabrep[j].indexOf('x') !== -1) {
          stor.lestermesrep[stor.lestermesrep.length - 1].koi = 'x'
        } else {
          stor.lestermesrep[stor.lestermesrep.length - 1].koi = 'n'
        }
      }
      if ((ds.Question === 'les trois') || (ds.Question.indexOf('Supprime') !== -1)) {
        for (let i = 0; i < stor.lestermesrep.length; i++) {
          stor.lestermesrep[i].signe = true
        }
      }

      // verif touts les mm termes
      for (let i = 0; i < stor.lestermesrep.length; i++) {
        ok = false
        for (let j = 0; j < acompare.length; j++) {
          ok = ok || termegal(stor.lestermesrep[i], acompare[j], true)
        }
        if (!ok) {
          stor.errTermInc = true
          if ((ds.Question === 'les trois') || (ds.Question.indexOf('Supprime') !== -1)) {
            stor.lilirep[i].corrige(false)
          }
          stor.TermMankList.push(stor.lestermesrep[i])
        }
      }
      if (stor.errTermInc) return false

      if (acompare.length !== stor.lestermesrep.length) {
        stor.errTermTrop = true
        return false
      }

      // verif pas bouge
      for (let i = 0; i < acompare.length; i++) {
        if (!termegal(stor.lestermesrep[i], acompare[i], true)) {
          if ((ds.Question === 'les trois') || (ds.Question.indexOf('Supprime') !== -1)) {
            stor.lilirep[i].corrige(false)
          }
          stor.errBouge = true
          return false
        }
      }

      // verif bon signe
      for (let i = 0; i < acompare.length; i++) {
        if (!termegal(acompare[i], stor.lestermesrep[i], false)) {
          stor.errSigneP = true
          stor.lerrS = [acompare[i].dou, stor.lestermesrep[i]]
          return false
        }
      }

      return true
    }
    if (stor.encours === 'Red') {
      larep = stor.moncal2.reponse()
      tabrep = larep.split(/[+-]/)
      /// separe signe et reste
      // verif alternance un signe et reste
      // verif fini pas par signe
      for (let j = 1; j < tabrep.length; j++) {
        if (tabrep[j] === '') {
          if (j === tabrep.length - 1) {
            stor.errEcrit = true
            stor.errRemdEcrit.push('La ligne finit par un signe !')
          } else {
            stor.errRemdEcrit.push('2 signes se suivent !')
            stor.errEcrit = true
          }
        }
      }

      // verif reste cest des nb bien ecrit
      for (let j = 0; j < tabrep.length; j++) {
        bufrep = tabrep[j]
        if ((bufrep.indexOf('²') !== -1) && (bufrep.indexOf('²') !== bufrep.length - 1)) {
          stor.errEcrit = true
          stor.errRemdEcrit.push('Erreur d’écriture')
          return false
        }
        bufrep = bufrep.replace('²', '')
        if ((bufrep.indexOf('x') !== -1) && (bufrep.indexOf('y') !== -1)) {
          stor.errEcrit = true
          stor.errRemdEcrit.push('Erreur d’écriture')
          return false
        }
        if ((bufrep.indexOf('x') !== -1) && (bufrep.indexOf('x') !== bufrep.length - 1)) {
          stor.errEcrit = true
          stor.errRemdEcrit.push('Erreur d’écriture')
          return false
        }
        bufrep = bufrep.replace('x', '')
        if ((bufrep.indexOf('y') !== -1) && (bufrep.indexOf('y') !== bufrep.length - 1)) {
          stor.errEcrit = true
          stor.errRemdEcrit.push('Erreur d’écriture')
          return false
        }
        bufrep = bufrep.replace('y', '')

        if (bufrep !== '') {
          test = verifNombreBienEcrit(bufrep, false, true)
          if (!test.good) {
            stor.errEcrit = true
            stor.errRemdEcrit.push(test.remede)
          }
        }
      }
      if (stor.errEcrit) {
        return false
      }

      const signtab = []

      let sb = larep
      let jd = 0
      if (tabrep[0] === '') {
        if (ds.simplifie && (larep[0] === '+')) {
          stor.errEcrit = true
          stor.errRemdEcrit = ['Tu peux supprimer le signe + !']
          return false
        }
        tabrep.splice(0, 1)
      } else {
        jd = 1
        signtab.push('+')
        sb = sb.substring(sb.indexOf(tabrep[0]) + tabrep[0].length)
      }
      for (let j = jd; j < tabrep.length; j++) {
        signtab.push(sb[0])
        sb = sb.substring(1)
        sb = sb.substring(sb.indexOf(tabrep[j]) + tabrep[j].length)
      }

      stor.lestermesrep = []
      for (let j = 0; j < tabrep.length; j++) {
        bufrep = tabrep[j].replace(',', '.').replace('x', '').replace('y', '').replace('²', '')
        if (bufrep === '') bufrep = '1'
        stor.lestermesrep.push({
          signe: signtab[j] === '+',
          val: parseFloat(bufrep)
        })
        if (tabrep[j].indexOf('x²') !== -1) {
          stor.lestermesrep[stor.lestermesrep.length - 1].koi = 'x²'
        } else if (tabrep[j].indexOf('y²') !== -1) {
          stor.lestermesrep[stor.lestermesrep.length - 1].koi = 'y²'
        } else if (tabrep[j].indexOf('y') !== -1) {
          stor.lestermesrep[stor.lestermesrep.length - 1].koi = 'y'
        } else if (tabrep[j].indexOf('x') !== -1) {
          stor.lestermesrep[stor.lestermesrep.length - 1].koi = 'x'
        } else {
          stor.lestermesrep[stor.lestermesrep.length - 1].koi = 'n'
        }
      }

      // verif y’en a pas 2 pareil
      const uvu = []
      for (let i = 0; i < stor.lestermesrep.length; i++) {
        // FIXME uvu est toujours vide !
        if (uvu.indexOf(stor.lestermesrep[i]) !== -1) {
          stor.errDeuxPareil = true
          return false
        }
      }

      // verif yen mank pas
      for (let i = 0; i < stor.termesfin.length; i++) {
        ok = false
        for (let j = 0; j < stor.lestermesrep.length; j++) {
          if ((stor.termesfin[i].koi === stor.lestermesrep[j].koi) || (stor.termesfin[i].val === 0)) ok = true
        }
        if (!ok) {
          stor.errDisp = true
          stor.errLdisp = stor.termesfin[i].koi
          return false
        }
      }

      // verif pas de 0
      for (let j = 0; j < stor.lestermesrep.length; j++) {
        if (stor.lestermesrep[j].val === 0) {
          stor.err0 = true
          return false
        }
      }

      for (let j = stor.termesfin.length - 1; j > -1; j--) {
        if (stor.termesfin[j].val === 0) {
          stor.termesfin.splice(j, 1)
        }
      }

      // verif tout
      for (let i = 0; i < stor.termesfin.length; i++) {
        for (let j = 0; j < stor.lestermesrep.length; j++) {
          if (stor.termesfin[i].koi === stor.lestermesrep[j].koi) {
            let hjkmpo = 0
            if (stor.lestermesrep[j].signe) { hjkmpo = stor.lestermesrep[j].val } else { hjkmpo = -stor.lestermesrep[j].val }
            if (hjkmpo !== stor.termesfin[i].val) {
              stor.errReduc = true
              stor.LerRed = stor.lestermesrep[j]
              return false
            }
          }
        }
      }

      return true
    }
    if (stor.encours === 'Dep') {
      larep = stor.moncal3.reponse()
      tabrep = larep.split(/[+-]/)
      /// separe signe et reste
      // verif alternance un signe et reste
      // verif fini pas par signe
      for (let j = 1; j < tabrep.length; j++) {
        if (tabrep[j] === '') {
          if (j === tabrep.length - 1) {
            stor.errEcrit = true
            stor.errRemdEcrit.push('La ligne finit par un signe !')
            stor.errBis = true
          } else {
            stor.errRemdEcrit.push('2 signes se suivent !')
            stor.errEcrit = true
            stor.errBis = true
          }
        }
      }

      // verif reste cest des nb bien ecrit
      for (let j = 0; j < tabrep.length; j++) {
        bufrep = tabrep[j]
        if ((bufrep.indexOf('²') !== -1) && (bufrep.indexOf('²') !== bufrep.length - 1)) {
          stor.errEcrit = true
          stor.errRemdEcrit.push('Erreur d’écriture')
          return false
        }
        bufrep = bufrep.replace('²', '')
        if ((bufrep.indexOf('x') !== -1) && (bufrep.indexOf('y') !== -1)) {
          stor.errEcrit = true
          stor.errRemdEcrit.push('Erreur d’écriture')
          return false
        }
        if ((bufrep.indexOf('x') !== -1) && (bufrep.indexOf('x') !== bufrep.length - 1)) {
          stor.errEcrit = true
          stor.errRemdEcrit.push('Erreur d’écriture')
          return false
        }
        bufrep = bufrep.replace('x', '')
        if ((bufrep.indexOf('y') !== -1) && (bufrep.indexOf('y') !== bufrep.length - 1)) {
          stor.errEcrit = true
          stor.errRemdEcrit.push('Erreur d’écriture')
          return false
        }
        bufrep = bufrep.replace('y', '')

        if (bufrep !== '') {
          test = verifNombreBienEcrit(bufrep, false, true)
          if (!test.good) {
            stor.errEcrit = true
            stor.errRemdEcrit.push(test.remede)
            stor.errBis = true
          }
        }
      }

      if (stor.errEcrit) {
        return false
      }

      const signtab = []

      let sb = larep
      let jd
      if (tabrep[0] === '') {
        tabrep.splice(0, 1)
        jd = 0
      } else {
        jd = 1
        signtab.push('+')
        sb = sb.substring(sb.indexOf(tabrep[0]) + tabrep[0].length)
      }
      for (let j = jd; j < tabrep.length; j++) {
        signtab.push(sb[0])
        sb = sb.substring(1)
        sb = sb.substring(sb.indexOf(tabrep[j]) + tabrep[j].length)
      }

      stor.lestermesrep = []
      for (let j = 0; j < tabrep.length; j++) {
        bufrep = tabrep[j].replace(',', '.').replace('x', '').replace('y', '').replace('²', '')
        if (bufrep === '') bufrep = '1'
        stor.lestermesrep.push({
          signe: signtab[j] === '+',
          val: parseFloat(bufrep)
        })
        if (tabrep[j].indexOf('x²') !== -1) {
          stor.lestermesrep[stor.lestermesrep.length - 1].koi = 'x²'
        } else if (tabrep[j].indexOf('y²') !== -1) {
          stor.lestermesrep[stor.lestermesrep.length - 1].koi = 'y²'
        } else if (tabrep[j].indexOf('y') !== -1) {
          stor.lestermesrep[stor.lestermesrep.length - 1].koi = 'y'
        } else if (tabrep[j].indexOf('x') !== -1) {
          stor.lestermesrep[stor.lestermesrep.length - 1].koi = 'x'
        } else {
          stor.lestermesrep[stor.lestermesrep.length - 1].koi = 'n'
        }
      }
      // verif y’en a pas 2 pareil
      const uvu = []
      for (let i = 0; i < stor.lestermesrep.length; i++) {
        // FIXME uvu est toujours vide, ce test ne sera jamais vérifié
        if (uvu.indexOf(stor.lestermesrep[i]) !== -1) {
          stor.errDeuxPareil = true
          stor.errBis = true
          return false
        }
      }

      // verif touts les mm termes
      const acompare = copiede(stor.lestermesatt)
      const asso = []
      if (acompare.length > stor.lestermesrep.length) {
        stor.errpasDet = true
        stor.errBis = true
        return false
      }
      for (let i = 0; i < stor.lestermesrep.length; i++) {
        ok = false
        for (let j = 0; j < acompare.length; j++) {
          ok = ok || termegal(stor.lestermesrep[i], acompare[j], true)
          if (termegal(stor.lestermesrep[i], acompare[j], true)) {
            asso[i] = j
          }
        }
        if (!ok) {
          stor.errTermInc = true
          stor.TermMankList.push(stor.lestermesrep[i])
          stor.errBis = true
        }
      }
      if (stor.errTermInc) return false
      if (acompare.length !== stor.lestermesrep.length) {
        stor.errTermTrop = true
        stor.errBis = true
        return false
      }

      // verif cote a cote
      const tabvu = []
      let enc = ''
      for (let i = 0; i < stor.lestermesrep.length; i++) {
        if (tabvu.indexOf(stor.lestermesrep[i].koi) !== -1 && enc !== stor.lestermesrep[i].koi) {
          stor.errReg = true
          stor.errBis = true
          return false
        }
        tabvu.push(stor.lestermesrep[i].koi)
        enc = stor.lestermesrep[i].koi
      }

      // verif bon signe
      for (let i = 0; i < acompare.length; i++) {
        if (acompare[asso[i]].signe !== stor.lestermesrep[i].signe) {
          stor.errSigneP = true
          stor.lerrS = [acompare[asso[i]].dou, stor.lestermesrep[i]]
          stor.errBis = true
          return false
        }
      }
      return true
    }
  }
  function desactivezone () {
    if (stor.encours === 'Sup') {
      stor.moncal.disable()
    }
    if (stor.encours === 'Red') {
      stor.moncal2.disable()
    }
    if (stor.encours === 'Dep') {
      stor.moncal3.disable()
    }
    if (stor.encours === 'Simp') {
      if ((ds.Question === 'les trois') || (ds.Question.indexOf('Supprime') !== -1)) {
        for (let i = 0; i < stor.lilirep.length; i++) {
          stor.lilirep[i].disable()
        }
      } else { stor.moncal1.disable() }
    }
  }
  function passetoutvert () {
    if (stor.encours === 'Sup') {
      stor.moncal.corrige(true)
    }
    if (stor.encours === 'Red') {
      stor.moncal2.corrige(true)
    }
    if (stor.encours === 'Dep') {
      stor.moncal3.corrige(true)
    }
    if (stor.encours === 'Simp') {
      if ((ds.Question === 'les trois') || (ds.Question.indexOf('Supprime') !== -1)) {
        for (let i = 0; i < stor.lilirep.length; i++) {
          stor.lilirep[i].corrige(true)
        }
      } else { stor.moncal1.corrige(true) }
    }
  }
  function barrelesfo () {
    if (stor.encours === 'Sup') {
      stor.moncal.barre()
    }
    if (stor.encours === 'Red') {
      stor.moncal2.barre()
    }
    if (stor.encours === 'Dep') {
      stor.moncal3.barre()
    }
    if (stor.encours === 'Simp') {
      if ((ds.Question === 'les trois') || (ds.Question.indexOf('Supprime') !== -1)) {
        for (let i = 0; i < stor.lilirep.length; i++) {
          stor.lilirep[i].barreIfKo()
        }
      } else {
        stor.moncal1.barre()
      }
    }
  }
  function AffCorrection (bool) {
    let i
    if (stor.errEcrit) {
      let jk = 'Une écriture est incorrecte !<br>'
      if (stor.errRemdEcrit.length > 1) jk = 'Des écritures sont incorrectes !<br>'
      j3pAffiche(stor.lesdiv.explications, null, jk)
      stor.yaexplik = true
      for (let i = 0; i < stor.errRemdEcrit.length; i++) {
        j3pAffiche(stor.lesdiv.explications, null, stor.errRemdEcrit[i])
      }
    }
    if (stor.errTermMank) {
      stor.yaexplik = true
      let jk = 'Je ne retrouve pas le terme $' + Affichemoi4(stor.TermMankList[0]) + '$ !'
      if (stor.TermMankList.length > 1) {
        jk = 'Je ne retrouve pas les termes suivants :'
        for (let i = 0; i < stor.TermMankList.length; i++) {
          jk += ' $ ' + Affichemoi4(stor.TermMankList[i]) + ' $ ;'
        }
        jk = jk.substring(0, jk.length - 1)
      }
      j3pAffiche(stor.lesdiv.explications, null, jk)
    }
    if (stor.errTermTrop) {
      stor.yaexplik = true
      j3pAffiche(stor.lesdiv.explications, null, 'Il y a trop de termes !')
    }
    if (stor.errBouge) {
      stor.yaexplik = true
      j3pAffiche(stor.lesdiv.explications, null, 'Tu ne dois pas déplacer de terme à cette étape !')
    }
    if (stor.errTermInc) {
      stor.yaexplik = true
      let jk = 'Il y a une erreur sur ces termes: '
      if (stor.TermMankList.length < 2) jk = 'Il y a une erreur sur ce terme : '
      for (let i = 0; i < stor.TermMankList.length; i++) {
        jk += ' $' + Affichemoi2([stor.TermMankList[i]]) + '$ ;'
      }
      jk = jk.substring(0, jk.length - 1)
      j3pAffiche(stor.lesdiv.explications, null, jk)
    }
    if (stor.errDisp) {
      stor.yaexplik = true
      if (stor.errLdisp !== 'n') {
        i = 'Les termes en $' + stor.errLdisp + '$ ont disparus !'
      } else { i = 'Il devrait rester un nombre !' }
      j3pAffiche(stor.lesdiv.explications, null, i)
    }
    if (stor.err0) {
      stor.yaexplik = true
      j3pAffiche(stor.lesdiv.explications, null, 'Tu peux simplifier ton écriture en supprimant un terme nul !')
    }
    if (stor.errReduc) {
      stor.yaexplik = true
      i = Affichemoi2([stor.LerRed])
      j3pAffiche(stor.lesdiv.explications, null, 'Erreur sur ce terme: $' + i + '$ !')
    }
    if (stor.errSigneP) {
      stor.yaexplik = true
      j3pAffiche(stor.lesdiv.explications, null, 'Erreur de signe !')
    }
    if (stor.errSigne) {
      stor.yaexplik = true
      let jk
      switch (stor.lerrS[0]) {
        case 'n': jk = 'Le signe de $' + (stor.lerrS[1].val + stor.lerrS[1].koi.replace('n', '')).replace('$1x', '$x').replace('$1y', '$y') + '$ ne change pas <br> car il n’est pas entre parenthèses !'
          break
        case '+': jk = 'Le signe de $' + (stor.lerrS[1].val + stor.lerrS[1].koi.replace('n', '')).replace('$1x', '$x').replace('$1y', '$y') + '$ ne change pas <br> car les parenthèses sont précédées du signe + !'
          break
        case '-': jk = 'Le signe de $' + (stor.lerrS[1].val + stor.lerrS[1].koi.replace('n', '')).replace('$1x', '$x').replace('$1y', '$y') + '$ change <br> car les parenthèses sont précédées du signe - !'
          break
      }
      j3pAffiche(stor.lesdiv.explications, null, '<u>Erreur de signe</u>: ' + jk)
    }
    if (stor.errDeuxPareil) {
      stor.yaexplik = true
      j3pAffiche(stor.lesdiv.explications, null, 'Tu peux encore réduire ta réponse !')
    }
    if (stor.errReg) {
      stor.yaexplik = true
      j3pAffiche(stor.lesdiv.explications, null, 'Il faut regrouper les $x²$ avec les $x²&, les $y²$ avec les $y²$, les $x$ avec les $x$, ... !')
    }
    if (stor.errpasDet) {
      stor.yaexplik = true
      j3pAffiche(stor.lesdiv.explications, null, 'A la première ligne, tu dois regrouper les termes (sans les ajouter) !')
    }

    if (stor.encours === 'Sup') {
      stor.moncal.corrige(false)
      stor.compteurPe3++
    }
    if (stor.encours === 'Red') {
      stor.moncal2.corrige(false)
      stor.compteurPe1++
    }
    if (stor.encours === 'Simp') {
      if (stor.lafaire.indexOf('Sup') === -1) {
        stor.moncal1.corrige(false)
      }
      stor.compteurPe1++
    }
    if (stor.encours === 'Dep') {
      stor.moncal3.corrige(false)
      stor.compteurPe2++
    }
    if (bool) {
      stor.foco = true
      stor.yaco = true
      desactivezone()
      barrelesfo()
      if (stor.encours === 'Sup') {
        stor.aff = Affichemoi2(stor.lestermesatt)
        j3pAffiche(stor.lesdiv.solution, null, '$A = ' + stor.aff + '$')
      }
      if (stor.encours === 'Simp') {
        if ((ds.Question === 'les trois') || (ds.Question.indexOf('Supprime') !== -1)) {
          stor.aff = '$A = '
          stor.aff += Affichemoi(stor.lestermes, true)
          /* for (let i = 0; i < stor.zonetab.length; i++) {
              switch (stor.zonetab[i]) {
                case 'Av':
                  stor.aff += Affichemoi(stor.souv1)
                  break
                case 'p1Ouvre':
                  if (stor.Lexo.cas[0] === '+') {
                    stor.aff += stor.souvps[ccpt]
                    ccpt++
                  } else {
                    stor.aff += '-('
                  }
                  break
                case 'in1':
                 stor.aff += Affichemoi(stor.souv2)
                  break
                case 'p1ferm':
                 stor.aff += ')'
                  break
                case 'p1top2':
                  if (stor.Lexo.cas[1] === '+') {
                    stor.aff += ')+('
                    ccpt++
                  } else {
                    stor.aff += ')-('
                  }
                  break
                case 'p2ouvre':
              if (stor.Lexo.cas[1] === '+') {
                stor.aff += stor.souvps[ccpt]
                ccpt++
              } else {
                stor.aff += '-('
            }
                  break
                case 'Ap1':
                  stor.aff += Affichemoi(stor.souv3)
                  break
                case 'in2':
                  stor.aff += Affichemoi(stor.souv4)
                  break
                case 'p2ferm':
                  stor.aff += ')'
                  break
                case 'Ap2':
                  stor.aff += Affichemoi(stor.souv5)
                  break
                default: alert(stor.zonetab[i])
              }
            } */
          j3pAffiche(stor.lesdiv.solution, null, stor.aff + '$')
        } else {
          stor.aff = Affichemoi2(stor.lestermesatt)
          j3pAffiche(stor.lesdiv.solution, null, '$A = ' + stor.aff + '$')
        }
      }
      if (stor.encours === 'Red') {
        const cp = copiede(stor.termesfin)
        for (let i = 0; i < cp.length; i++) {
          cp[i].signe = cp[i].val > 0
          cp[i].val = Math.abs(cp[i].val)
        }
        stor.aff = '$A = ' + Affichemoi2(cp)
        j3pAffiche(stor.lesdiv.solution, null, stor.aff + '$')
      }
      if (stor.encours === 'Dep') {
        let jkl = copiede(stor.lestermesatt)
        jkl = jkl.sort(function (a, b) { return classe(a, b) })
        stor.aff = Affichemoi2(jkl)
        stor.lestermesatt = copiede(jkl)
        j3pAffiche(stor.lesdiv.solution, null, '$A = ' + stor.aff + '$')
      }
    } else { me.afficheBoutonValider() }
  }

  function giveN (n) {
    switch (n) {
      case 'n': return 0
      case 'y': return 1
      case 'x': return 2
      case 'y²':return 3
      case 'x²': return 4
      default:
        j3pShowError(n)
        return 0
    }
  }
  function classe (a, b) {
    return giveN(b.koi) - giveN(a.koi)
  }
  function enonceMain () {
    if (me.etapeCourante === 1) {
      me.videLesZones()
      creeLesDiv()
      stor.Lexo = faisChoixExos()
      stor.lesdiv.travail.classList.add('travail')
      stor.lesdiv.consigne.classList.add('enonce')
    }
    poseQuestion()
    if (ds.Calculatrice) {
      me.fenetresjq = [{ name: 'Calculatrice', title: 'Une calculatrice', left: 600, top: 10 }]
      j3pCreeFenetres(me)
      j3pAfficheCroixFenetres('Calculatrice')
      j3pAjouteBouton(stor.lesdiv.calculatrice, 'Calculatrice', 'MepBoutons', 'Calculatrice', boutonfen)
    }
    me.afficheBoutonValider()
    me.finEnonce()
  }

  switch (this.etat) {
    case 'enonce':
      // code exécuté au lancement de la section
      if (me.debutDeLaSection) {
        initSection()
      } else {
        enonceMain()
      }
      break // case "enonce":

    case 'correction':
      // On teste si une réponse a été saisie
      stor.yaexplik = false
      stor.lesdiv.explications.innerHTML = ''
      stor.lesdiv.correction.innerHTML = ''
      stor.yaco = false

      if (yaReponse()) {
        // Bonne réponse
        if (isRepOk()) {
          this._stopTimer()
          stor.lesdiv.correction.style.color = this.styles.cbien
          stor.lesdiv.correction.innerHTML = cBien

          desactivezone()
          passetoutvert()
          this.typederreurs[0]++
          me.score++

          this.cacheBoutonValider()
          this.etat = 'navigation'
          this.sectionCourante()
        } else {
          // Pas de bonne réponse
          stor.lesdiv.correction.style.color = this.styles.cfaux
          stor.lesdiv.correction.innerHTML = cFaux
          if (this.isElapsed) {
            // A cause de la limite de temps :
            this._stopTimer()

            stor.lesdiv.correction.innerHTML += tempsDepasse + '<BR>'
            this.typederreurs[10]++
            this.cacheBoutonValider()

            AffCorrection(true)

            this.etat = 'navigation'
            this.sectionCourante()
          } else {
            // Réponse fausse :
            if (me.essaiCourant < me.donneesSection.nbchances) {
              stor.lesdiv.correction.innerHTML += '<br>' + essaieEncore

              AffCorrection(false)

              this.typederreurs[1]++
            } else {
              // Erreur au nème essai
              this._stopTimer()
              this.cacheBoutonValider()
              this.typederreurs[2]++

              AffCorrection(true)

              this.etat = 'navigation'
              this.sectionCourante()
            }
          }
        }
      } else {
        // pas de réponse
        stor.lesdiv.correction.style.color = this.styles.cfaux
        stor.lesdiv.correction.innerHTML = reponseIncomplete
        me.afficheBoutonValider()
      }

      if (ds.theme === 'zonesAvecImageDeFond') {
        if (stor.yaexplik) {
          stor.lesdiv.explications.classList.add('explique')
        }
        if (stor.yaco) {
          stor.lesdiv.solution.classList.add('correction')
        }
      }

      // Obligatoire
      this.finCorrection()
      break // case "correction":

    case 'navigation':
      if (this.sectionTerminee()) {
        this.parcours.pe = getPe(ds, stor)
      } else {
        this.etat = 'enonce'
        me.afficheBoutonSuite()
        this.focus('boutoncontinuer')
      }
      // Obligatoire
      this.finNavigation()
      break // case "navigation":
  }
}
