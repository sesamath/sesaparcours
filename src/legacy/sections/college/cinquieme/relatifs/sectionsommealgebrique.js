import { j3pAddElt, j3pAjouteBouton, j3pArrondi, j3pGetRandomBool, j3pGetRandomInt, j3pShuffle } from 'src/legacy/core/functions'
import BulleAide from 'src/legacy/outils/bulleAide/BulleAide'
import { verifNombreBienEcrit } from 'src/legacy/outils/fonctions/ecritureNombre'
import { getPe, initCptPe } from 'src/legacy/sections/college/helperTommy'
import { j3pAffiche } from 'src/lib/mathquill/functions'
import { addDefaultTable } from 'src/legacy/themes/table'
import { j3pCreeFenetres, j3pAfficheCroixFenetres, j3pToggleFenetres } from 'src/legacy/core/functionsJqDialog'
import BrouillonCalculs from 'src/legacy/outils/brouillon/BrouillonCalculs'
import textesGeneriques from 'src/lib/core/textes'

const { cBien, cFaux, essaieEncore, reponseIncomplete, tempsDepasse } = textesGeneriques

/**
 * Ajouter une description ici
 * @fileOverview
 * @author Tommy Barroy <t.barroy@laposte.net>
 * @since 2017-10
*/
export const params = {
  outils: ['mathquill', 'calculatrice'],
  /**
   * Ce qui est paramétrable dans l’éditeur de graphe
   * @type {LegacySectionParamsParametres}
   */
  parametres: [
    ['nbrepetitions', 3, 'entier', 'Nombre de répétitions de la section'],
    ['nbchances', 2, 'entier', 'Nombre d’essais possibles.'],
    ['Min_nb_termes', 3, 'entier', 'Nombre de termes minimum (supérieur ou égal à 2).'],
    ['Max_nb_termes', 10, 'entier', 'Nombre de termes maximul (inférieur ou égal à 10).'],
    ['Progression', 'linéaire', 'liste', '<u>aléatoire</u> ou <u>linéaire</u> pour déterminer', ['linéaire', 'aléatoire']],
    ['entier', false, 'boolean', '<u>true</u>:  Les nombres de départ sont focément entiers'],
    ['Detail_op', true, 'boolean', '<u>true</u>:  Chaque terme d’une ligne est le résultat d’une seule opération de la ligne précédente.'],
    ['Detail_deplace', true, 'boolean', '<u>true</u>:  Le déplacement de termes ne peut être réaliser en même temps qu’une opération.'],
    ['conseil', true, 'boolean', '<u>true</u>:  Regroupement conseillé quand le nombre de termes est supérieur à 4.'],
    ['Regroupe_seul', false, 'boolean', '<u>true</u>:  Seule l’étape de regroupement est attendue.'],
    ['simplifie', false, 'boolean', '<u>true</u>:  L’élève ne doit pas écrire le signe + au résutat.'],
    ['Calculatrice', true, 'boolean', '<u>true</u>:  Une calculatrice est disponible.'],
    // le modèle gère la compatibilité ascendante avec l’ancien paramètre MiseEnPagePlusClaire
    ['theme', 'standard', 'liste', 'Modèle pour l’affichage', ['standard', 'zonesAvecImageDeFond']]
  ],

  pe: [
    {
      pe_1: 'calul',
      pe_2: 'regroupement',
      pe_3: 'écriture'
    }
  ]
}

/**
 * section sommealgebrique
 * @this {Parcours}
 */
export default function main () {
  const me = this
  const ds = me.donneesSection
  const stor = this.storage

  function initSection () {
    stor.lesbulle = []
    initCptPe(ds, stor)
    me.validOnEnter = false // ex donneesSection.touche_entree

    // Construction de la page
    me.construitStructurePage('presentation1bis')

    if (ds.Min_nb_termes < 3) ds.Min_nb_termes = 3
    if (ds.Max_nb_termes > 10) ds.Max_nb_termes = 10
    if (ds.Max_nb_termes < 3) ds.Max_nb_termes = 3
    if (ds.Min_nb_termes > 10) ds.Min_nb_termes = 10
    if (ds.Min_nb_termes > ds.Max_nb_termes) ds.Min_nb_termes = ds.Max_nb_termes

    ds.lesExos = []
    const lp = []
    for (let i = ds.Min_nb_termes; i < ds.Max_nb_termes + 1; i++) {
      lp.push(i)
    }
    for (let i = 0; i < ds.nbitems; i++) {
      ds.lesExos[i] = { nb: lp[i % lp.length] }
    }
    ds.lesExos = j3pShuffle(ds.lesExos)

    if (ds.Progression === 'linéaire') {
      ds.lesExos.sort(function (a, b) {
        return b.nb - a.nb
      })
    }
    stor.restric = '0123456789+- '
    if (!ds.entier) stor.restric += '.,'
    me.score = 0
    let tt = 'Calculer une somme algébrique'
    if (ds.Detail_deplace || ds.Detail_op) tt = 'Justifier un calcul'
    me.afficheTitre(tt)
  } // initSection

  function faisChoixExos () {
    stor.clvspe = false
    const e = ds.lesExos.pop()
    let yap = false
    let yan = false
    let st
    stor.lestermes = []
    if (ds.Regroupe_seul) e.nb = Math.max(4, e.nb)
    // faut que y’est les 2 signes
    const used = []
    for (let i = 0; i < e.nb; i++) {
      st = j3pGetRandomBool()
      yap = yap || st
      yan = yan || (!st)
      stor.lestermes.push({ signe: st })
      if (ds.entier) {
        do {
          stor.lestermes[stor.lestermes.length - 1].val = j3pGetRandomInt(1, 29)
        } while (used.indexOf(stor.lestermes[stor.lestermes.length - 1].val) !== -1)
        used.push(stor.lestermes[stor.lestermes.length - 1].val)
      } else {
        do {
          stor.lestermes[stor.lestermes.length - 1].val = j3pArrondi(j3pGetRandomInt(1, 99) / 10, 1)
        } while (used.indexOf(stor.lestermes[stor.lestermes.length - 1].val) !== -1)
        used.push(stor.lestermes[stor.lestermes.length - 1].val)
      }
    }
    if (!yap) {
      stor.lestermes[j3pGetRandomInt(1, stor.lestermes.length - 1)].signe = true
    }
    if (!yan) {
      stor.lestermes[0].signe = false
    }
    if (ds.Regroupe_seul) {
      switch (j3pGetRandomInt(0, 7)) {
        case 0:
          stor.lestermes[0].signe = true
          stor.lestermes[1].signe = false
          stor.lestermes[2].signe = true
          stor.lestermes[3].signe = false
          break
        case 1:
          stor.lestermes[0].signe = false
          stor.lestermes[1].signe = true
          stor.lestermes[2].signe = false
          stor.lestermes[3].signe = true
          break
        case 2:
          stor.lestermes[0].signe = false
          stor.lestermes[1].signe = false
          stor.lestermes[2].signe = true
          stor.lestermes[3].signe = false
          break
        case 3:
          stor.lestermes[0].signe = true
          stor.lestermes[1].signe = false
          stor.lestermes[2].signe = true
          stor.lestermes[3].signe = true
          break
        case 4:
          stor.lestermes[0].signe = true
          stor.lestermes[1].signe = false
          stor.lestermes[2].signe = false
          stor.lestermes[3].signe = true
          break
        case 5:
          stor.lestermes[0].signe = false
          stor.lestermes[1].signe = true
          stor.lestermes[2].signe = true
          stor.lestermes[3].signe = false
          break
        case 6:
          stor.lestermes[0].signe = false
          stor.lestermes[1].signe = true
          stor.lestermes[2].signe = false
          stor.lestermes[3].signe = false
          break
        case 7:
          stor.lestermes[0].signe = true
          stor.lestermes[1].signe = true
          stor.lestermes[2].signe = false
          stor.lestermes[3].signe = true
          break
      }
    }
    return e
  }
  function poseQuestion () {
    stor.Aff = Affichemoi(stor.lestermes)
    let nbl = 10
    let ph = '<b>Tu dois calculer la somme algébrique A</b><br>'
    if (ds.Regroupe_seul) ph = '<b>Tu dois regrouper les termes de la somme algébrique $A$ en fonction de leur signe.</b><br>'
    j3pAffiche(stor.lesdiv.consigne, null, ph)
    if ((ds.Detail_op || ds.Detail_deplace) && !ds.Regroupe_seul) {
      j3pAffiche(stor.lesdiv.consigne, null, '<i>Des étapes de calcul sont nécessaires.</i>\n')
      if (ds.Detail_op) {
        j3pAffiche(stor.lesdiv.consigne, null, '<i><u>Détail attendu</u>: Chaque terme d’une nouvelle ligne est le résutat d’une seule opération.</i>')
        const spanBulle = j3pAddElt(stor.lesdiv.consigne, 'span')
        stor.aide1 = new BulleAide(spanBulle, 'Le résultat d’une seule opération<br>. Une soustraction<br>. Une addition (de 2 ou plusieurs termes <u>de même signe</u>)', { place: 0 })
      }
      if (ds.Detail_deplace) {
        j3pAffiche(stor.lesdiv.consigne, null, '\n<i><u>Détail attendu</u>: Avant d’opérer sur des nombres, il faut les placer côte à côte.</i>\n')
      }
    }
    j3pAffiche(stor.lesdiv.consigne, null, '\n')
    const tratr = addDefaultTable(stor.lesdiv.travail, 3, 5)
    j3pAffiche(tratr[0][0], null, '$A$')
    j3pAffiche(tratr[0][1], null, '$=$')
    j3pAffiche(tratr[0][2], null, '$' + stor.Aff + '$')
    if (ds.Regroupe_seul) nbl = 1
    stor.moncal = new BrouillonCalculs({
      caseBoutons: tratr[1][0],
      caseEgal: tratr[1][1],
      caseCalculs: tratr[1][2],
      caseApres: tratr[1][3],
      caseBulle: tratr[1][4]
    }, { limite: 30, limitenb: 4, restric: stor.restric, hasAutoKeyboard: false, lmax: nbl, isBrouillon: false })
    stor.lesdiv.explications.classList.remove('explique')
    stor.lesdiv.solution.classList.remove('correction')
  }
  function creeLesDiv () {
    stor.lesdiv = {}
    stor.lesdiv.conteneur = j3pAddElt(me.zonesElts.MG, 'div', {
      style: me.styles.etendre('toutpetit.enonce', { padding: '10px' })
    })
    stor.lesdiv.consigne = addDefaultTable(stor.lesdiv.conteneur, 1, 1)[0][0]
    stor.lesdiv.travail = addDefaultTable(stor.lesdiv.conteneur, 1, 1)[0][0]
    stor.lesdiv.explications = j3pAddElt(stor.lesdiv.conteneur, 'div')
    stor.lesdiv.solution = j3pAddElt(stor.lesdiv.conteneur, 'div')
    stor.lesdiv.calculatrice = j3pAddElt(me.zonesElts.MD, 'div', {
      style: me.styles.etendre('toutpetit.enonce', { padding: '10px' })
    })
    stor.lesdiv.correction = j3pAddElt(me.zonesElts.MD, 'div', {
      style: me.styles.etendre('toutpetit.enonce', { padding: '10px' })
    })
    stor.lesdiv.explications.style.color = me.styles.cfaux
    stor.lesdiv.solution.style.color = me.styles.toutpetit.correction.color
    me.zonesElts.MG.classList.add('fond')
  }
  function Affichemoi (tab) {
    let aret = ''
    for (let i = 0; i < tab.length; i++) {
      if (!tab[i].signe) { aret += '-' } else {
        if (i !== 0) aret += '+'
      }
      aret += String(tab[i].val).replace('.', ',')
    }
    return aret
  }

  function copiede (tab) {
    const ret = []
    for (let i = 0; i < tab.length; i++) {
      ret[i] = tab[i]
    }
    return ret
  }
  function verifTouteligne (tab) {
    let ok
    let survdep = false
    for (let i = 0; i < tab.length - 1; i++) {
      ok = verifLigne(tab[i], tab[i + 1])
      if (ok.justdep && survdep) {
        return { good: false, ligne: i + 1, remede: 'Déplace tous les termes en une seule fois !' }
      }
      if (!ok.good) {
        return { good: false, ligne: i + 1, remede: ok.remede }
      }
      survdep = ok.justdep
    }
    return { good: true }
  }
  function verifLigne (tab1, tab2) {
    /// mets de côte tous les termes dans les 2
    let ttseul1 = copiede(tab1)
    let ttseul2 = copiede(tab2)
    for (let i = ttseul1.length - 1; i > -1; i--) {
      let ok = false
      for (let j = ttseul2.length - 1; j > -1; j--) {
        if ((ttseul1[i].signe === ttseul2[j].signe) && (ttseul1[i].val === ttseul2[j].val)) {
          if (!ok) ttseul2.splice(j, 1)
          ok = true
        }
      }
      if (ok) ttseul1.splice(i, 1)
    }
    // la on peut dire si ya op
    const yaeuop = ((ttseul1.length > 0) || (ttseul2.length > 0))

    ttseul1 = copiede(tab1)
    ttseul2 = copiede(tab2)
    /// verifie que la mm somme
    let tot1 = 0
    let tot2 = 0
    for (let i = 0; i < ttseul2.length; i++) {
      if (ttseul2[i].signe) { tot2 += ttseul2[i].val } else { tot2 -= ttseul2[i].val }
    }
    for (let i = 0; i < ttseul1.length; i++) {
      if (ttseul1[i].signe) { tot1 += ttseul1[i].val } else { tot1 -= ttseul1[i].val }
    }
    tot1 = j3pArrondi(tot1, 1)
    tot2 = j3pArrondi(tot2, 1)
    if (tot1 !== tot2) {
      return { good: false, remede: 'Erreur de calcul !' }
    }

    // verifie que y’a pas plus de termes d’un coups
    if (ttseul2.length > ttseul1.length) {
      return { good: false, remede: 'Ligne inutile !' }
    }

    /// si ya pas op, verifie que ya eu deplavemebt
    let justdep
    if (!yaeuop) {
      let yaeudep = false
      for (let i = 0; i < tab1.length; i++) {
        if ((tab1[i].signe !== tab2[i].signe) || (tab1[i].val !== tab2[i].val)) {
          yaeudep = true
          break
        }
      }
      if (!yaeudep) return { good: false, remede: 'Ligne inutile !' }
      justdep = true
    } else {
      justdep = false
    }

    /// si ya eu verifie +devant - derrier
    stor.needConseilDeplace = false
    if (justdep) {
      let lilo = true
      for (let i = 0; i < tab2.length; i++) {
        lilo = lilo & tab2[i].signe
        if (tab2[i].signe && !lilo) stor.needConseilDeplace = true
      }
    }

    // verif nb op
    let sontcotcot = true
    let cotcot, lilorestants
    if (ds.Detail_op || ds.Detail_deplace) {
      let cmpt = 0
      let tailleact
      let newt = ttseul2.length
      do {
        cmpt++
        tailleact = newt
        lilorestants = []
        for (let i = ttseul2.length - 1; i > -1; i--) {
          const lilo = termesUsesPour(ttseul2[i], ttseul1)
          let ff
          if (lilo.length === 0) {
            if (ttseul2[i].signe) {
              ff = '+'
            } else {
              ff = '-'
            }
            ff += ttseul2[i].val
            return {
              good: false,
              remede: 'Je ne comprends pas comment tu trouves le terme $' + ff + '$ !'
            }
          } else if (lilo.length === 1) {
            cotcot = []
            for (let k = 0; k < lilo[0].length; k++) {
              for (let j = ttseul1.length - 1; j > -1; j--) {
                if (termegal(ttseul1[j], lilo[0][k])) {
                  ttseul1.splice(j, 1)
                  for (let u = 0; u < tab1.length; u++) {
                    if (termegal(tab1[u], lilo[0][k])) {
                      cotcot.push(u)
                      break
                    }
                  }
                  break
                }
              }
              sontcotcot = sontcotcot && sontcolle(cotcot)
            }
            ttseul2.splice(i, 1)
          } else {
            lilorestants.push({ nb: ttseul2[i], decomps: lilo })
          }
        }
        newt = ttseul2.length
      } while ((ttseul2.length > 0) && (cmpt < 100) && (newt < tailleact))

      if (newt === tailleact) {
        /// il en reste dans lilorestants
        let sp = copiede(lilorestants[0].decomps)
        for (let i = 1; i < lilorestants.length; i++) {
          for (let j = 0; j < lilorestants[i].decomps.length; j++) {
            sp = ajj(sp, lilorestants[i].decomps[j])
          }
        }
        // supprime les impossibles
        // doit y avoir tous les termes de ttseul1 restants et que ceux là
        // test sont tous la (en vrifiant les doublons)
        for (let i = sp.length - 1; i > -1; i--) {
          const op = copiede(ttseul1)
          tot2 = true
          for (let j = 0; j < sp[i].length; j++) {
            tot1 = false
            for (let k = 0; k < op.length; k++) {
              if (termegal(op[k], sp[i][j])) {
                tot1 = true
                op.splice(k, 1)
                break
              }
              if (!tot1) {
                tot2 = false
                break
              }
            }
            if (op.length !== 0) tot2 = false
            if (!tot2) {
              sp.splice(i, 1)
              break
            }
          }
        }

        if (sp.length === 0) return { good: false, remede: 'Les nouveaux termes ne sont pas justifiés !' }
        // puis cherche si yen a une qui respecte detailop ou detaildep ou les 2
        if (ds.Detail_op) {
          const sp2 = copiede(sp)
          for (let i = sp2.length - 1; i > -1; i--) {
            let ongarde = true
            if (sp2[i].length > 2) {
              const s = sp2[i][0].signe
              for (let j = sp2[i].length; j > 0; j--) {
                if (sp2[i][j].signe !== s) {
                  ongarde = false
                }
              }
            }
            if (!ongarde) sp2.splice(i, 1)
          }
          if (sp2.length === 0) return { good: false, remede: 'Les opérations effectuées ne sont pas toutes détaillées !' }
        }

        if (ds.Detail_deplace) {
          const sp2 = copiede(sp)
          for (let i = sp2.length - 1; i > -1; i--) {
            let ongarde = true
            cotcot = []
            if (sp2[i].length > 1) {
              for (let j = sp2[i].length; j > 0; j--) {
                for (let u = 0; u < tab1.length; u++) {
                  if (termegal(tab1[u], sp2[i][j])) {
                    cotcot.push(u)
                    break
                  }
                }
              }
              ongarde = ongarde && sontcolle(cotcot)
            }
            if (!ongarde) sp2.splice(i, 1)
          }
          if (sp2.length === 0) return { good: false, remede: 'Place les termes côte à côte avant d’effectuer un calcul !' }
        }
      }

      if (ds.Detail_deplace && !sontcotcot) {
        return { good: false, remede: 'Regroupe les termes que tu utilises !' }
      }
    }

    return { good: true, justdep }
  }
  function termesUsesPour (val, tab) {
    let kl, dt
    const aret = []
    for (let i = 0; i < tab.length; i++) {
      if (termegal(val, tab[i])) {
        aret.push([tab[i]])
      }
    }
    // on cherche dabord une somme avec tous les mm signe
    let cop = copiede(tab)
    for (let i = cop.length - 1; i > -1; i--) {
      if (cop[i].signe !== val.signe) cop.splice(i, 1)
    }
    let coup = []
    for (let j = 2; j < cop.length + 1; j++) {
      kl = toulesgr(j, cop)
      coup = coup.concat(kl)
    }
    for (let i = 0; i < coup.length; i++) {
      dt = 0
      for (let j = 0; j < coup[i].length; j++) {
        dt += coup[i][j].val
      }
      dt = j3pArrondi(dt, 1)
      if (dt === val.val) aret.push(coup[i])
    }

    // on cherche une diff entre 2 mm signe
    cop = copiede(tab)
    coup = []

    for (let i = cop.length - 1; i > -1; i--) {
      if (cop[i].signe) {
        coup.push(cop[i])
        cop.splice(i, 1)
      }
    }
    dt = val.val
    if (!val.signe) dt = j3pArrondi(-dt, 1)
    for (let i = 0; i < cop.length; i++) {
      for (let j = 0; j < coup.length; j++) {
        if (j3pArrondi(coup[j].val - cop[i].val, 1) === dt) aret.push([cop[i], coup[j]])
      }
    }
    return aret
  }
  function toulesgr (nb, tab) {
    let aret = []
    let tbuf, tttbbbu, jkl
    if (nb !== 1) {
      aret = []
      tbuf = copiede(tab)
      for (let j = tbuf.length - 1; j > -1; j--) {
        jkl = tbuf[j]
        tbuf.splice(tbuf.indexOf(jkl), 1)
        tttbbbu = toulesgr(nb - 1, tbuf)
        for (let i = 0; i < tttbbbu.length; i++) {
          aret.push([jkl].concat(tttbbbu[i]))
        }
      }
    } else {
      return tab
    }
    return aret
  }
  function boutonfen () {
    j3pToggleFenetres('Calculatrice')
  }
  function termegal (k, l) {
    return (k.signe === l.signe) && (k.val === l.val)
  }
  function sontcolle (tab) {
    if (tab.length === 1) return true
    let ok
    for (let i = 0; i < tab.length; i++) {
      ok = false
      for (let j = 0; j < tab.length; j++) {
        if (i === j) continue
        if (Math.abs(tab[i] - tab[j]) < 2) ok = true
      }
      if (!ok) return false
    }
    return true
  }
  function ajj (tab1, tab2) {
    const aret = copiede(tab1)
    for (let i = 0; i < aret.length; i++) {
      for (let j = 0; j < tab2.length; j++) {
        aret[i].concat(tab2[j])
      }
    }
    return aret
  }

  function yaReponse () {
    const avrif = stor.moncal.reponse()
    for (let i = 0; i < avrif.length; i++) {
      if (avrif[i] === '') {
        stor.moncal.items[i].zone.focus()
        return false
      }
    }
    return true
  }
  function isRepOk () {
    stor.errEcrit = false
    stor.ErrListeLigne = []
    stor.errRemdEcrit = []
    stor.errPafini = false
    stor.errRemed = ''
    stor.ErrPaRegroup = false
    stor.errPaBienRange = false
    stor.laligf = undefined
    stor.nedco = false
    stor.errSimpli = false

    const larep = stor.moncal.reponse()
    const tabrep = []
    let b, sb, jd
    for (let i = 0; i < larep.length; i++) {
      tabrep.push(larep[i].split(/[+-]/))
    }
    /// separe signe et reste
    // verif alternance un signe et reste
    // verif fini pas par signe
    for (let i = 0; i < tabrep.length; i++) {
      for (let j = 1; j < tabrep[i].length; j++) {
        if (tabrep[i][j] === '') {
          stor.ErrListeLigne.push(i)
          if (j === tabrep[i].length - 1) {
            stor.errEcrit = true
            stor.errRemdEcrit.push('La ligne finit par un signe !')
          } else {
            stor.errRemdEcrit.push('2 signes se suivent !')
            stor.errEcrit = true
          }
        }
      }
    }
    // verif reste cest des nb bien ecrit
    for (let i = 0; i < tabrep.length; i++) {
      for (let j = 0; j < tabrep[i].length; j++) {
        if (tabrep[i][j] !== '') {
          b = (i === tabrep.length)
          const test = verifNombreBienEcrit(tabrep[i][j], false, b)
          if (!test.good) {
            stor.errEcrit = true
            stor.ErrListeLigne.push(i)
            stor.errRemdEcrit.push(test.remede)
          }
        }
      }
    }
    if (stor.errEcrit) {
      return false
    }
    // cree liste termes pour chaque ligne
    const signtab = []
    for (let i = 0; i < larep.length; i++) {
      sb = larep[i]
      signtab[i] = []
      jd = 0
      if (tabrep[i][0] === '') {
        tabrep[i].splice(0, 1)
      } else {
        jd = 1
        signtab[i].push('+')
        sb = sb.substring(tabrep[i][0].length)
      }
      for (let j = jd; j < tabrep[i].length; j++) {
        signtab[i].push(sb[sb.indexOf(tabrep[i][j]) - 1])
        sb = sb.substring(tabrep[i][j].length + 1)
      }
    }
    stor.lestermesrep = []
    for (let i = 0; i < tabrep.length; i++) {
      stor.lestermesrep[i] = []
      for (let j = 0; j < tabrep[i].length; j++) {
        stor.lestermesrep[i].push({ signe: signtab[i][j] === '+', val: parseFloat(tabrep[i][j].replace(',', '.')) })
      }
    }
    stor.lestermesrep.splice(0, 0, stor.lestermes)
    const adire = verifTouteligne(stor.lestermesrep)

    if (!adire.good) {
      stor.errRemed = adire.remede
      stor.laligf = adire.ligne
      return false
    }
    // si detail dep, verifie que quand ya dep, ya pas op

    // si detail op verifie que une op à la fois

    // verifie si toutes lignes égales

    // verifie pas de ligne redondante
    // genre 2 dep sans op
    // ou exactement la mm

    // verif derniere ligne c’est que un nom
    if (!ds.Regroupe_seul) {
      if (tabrep[tabrep.length - 1].length > 1) {
        stor.errPafini = true
        return false
      }
      if ((larep[larep.length - 1].indexOf('+') !== -1) && (ds.simplifie)) {
        stor.errSimpli = true
        stor.errSimplili = larep.length - 1
        return false
      }
    } else {
      if (tabrep[tabrep.length - 1].length !== stor.lestermes.length) {
        stor.ErrPaRegroup = true
        return false
      }
      // verif range
      jd = stor.lestermesrep[stor.lestermesrep.length - 1][0].signe
      b = false
      for (let i = 0; i < stor.lestermesrep[stor.lestermesrep.length - 1].length; i++) {
        if (stor.lestermesrep[stor.lestermesrep.length - 1][i].signe !== jd) {
          b = true
        } else {
          if (b) {
            stor.errPaBienRange = true
            return false
          }
        }
      }
    }
    if (tabrep.length > 3) stor.nedco = true
    return true
  }
  function desactivezone () {
    stor.moncal.disable()
  }
  function passetoutvert () {
    stor.moncal.passetoutvert()
  }
  function barrelesfo () {
    stor.moncal.barrelesfo()
  }
  function AffCorrection (bool) {
    stor.moncal.vireAide()
    if (stor.errEcrit) {
      let jk = 'Une écriture est incorrecte !'
      if (stor.errRemdEcrit.length > 1) jk = 'Des écritures sont incorrectes'
      j3pAffiche(stor.lesdiv.explications, null, jk)
      stor.yaexplik = true
      for (let i = 0; i < stor.errRemdEcrit.length; i++) {
        stor.moncal.aide(stor.ErrListeLigne[i], stor.errRemdEcrit[i])
        stor.moncal.corrigeligne(stor.ErrListeLigne[i])
      }
      stor.compteurPe3++
    }
    if (stor.errPafini) {
      stor.yaexplik = true
      j3pAffiche(stor.lesdiv.explications, null, 'Tu dois terminer le calcul !')
    }
    if (stor.errRemed !== '') {
      stor.yaexplik = true
      if (ds.Regroupe_seul) {
        j3pAffiche(stor.lesdiv.explications, null, 'Tu dois simplement déplacer les termes avec leur signe !')
        if (stor.errRemed.indexOf('calcul') !== -1) {
          stor.compteurPe1++
        }
      } else {
        j3pAffiche(stor.lesdiv.explications, null, stor.errRemed)
        if (stor.laligf !== undefined) {
          stor.moncal.corrigeligne(stor.laligf - 1)
        }
        stor.compteurPe3++
      }
    }
    if (stor.ErrPaRegroup) {
      stor.yaexplik = true
      j3pAffiche(stor.lesdiv.explications, null, 'Aucun calcul n’est attendu dans cet exercice !')
    }
    if (stor.errPaBienRange) {
      stor.yaexplik = true
      j3pAffiche(stor.lesdiv.explications, null, 'Il faut regrouper les termes par signes !')
      stor.compteurPe2++
    }
    if (stor.errSimpli) {
      stor.yaexplik = true
      j3pAffiche(stor.lesdiv.explications, null, 'Tu dois simplifier ton résultat ')
      stor.moncal.corrigeligne(stor.errSimplili)
      stor.compteurPe3++
    }

    if (bool) {
      stor.yaco = true
      desactivezone()
      barrelesfo()
      let b, np, nm, p, o, m, g
      if (ds.Regroupe_seul) {
        np = ''
        nm = ''
        for (b = 0; b < stor.lestermes.length; b++) {
          if (stor.lestermes[b].signe) { np = np + '+' + stor.lestermes[b].val } else { nm = nm + '-' + stor.lestermes[b].val }
        }
        np = np.substring(1) + nm + '$'
        j3pAffiche(stor.lesdiv.solution, null, '$A = ' + np)
      } else {
        if (stor.lestermes.length >= 4) {
          np = ''
          nm = ''
          for (b = 0; b < stor.lestermes.length; b++) {
            if (stor.lestermes[b].signe) { np = np + '+' + stor.lestermes[b].val } else { nm = nm + '-' + stor.lestermes[b].val }
          }
          np = np.substring(1) + nm + '$'
          j3pAffiche(stor.lesdiv.solution, null, '$A = ' + np + '\n')
          np = 0
          nm = 0
          for (b = 0; b < stor.lestermes.length; b++) {
            if (stor.lestermes[b].signe) { np += stor.lestermes[b].val } else { nm += stor.lestermes[b].val }
          }
          np = j3pArrondi(np, 1)
          nm = j3pArrondi(nm, 1)
          j3pAffiche(stor.lesdiv.solution, null, '$A = ' + np + '-' + nm + '$\n')
          np = j3pArrondi(np - nm, 1)
          j3pAffiche(stor.lesdiv.solution, null, '$A = ' + np + '$\n')
        } else {
          if (stor.lestermes[0].signe) { p = stor.lestermes[0].val } else { p = -stor.lestermes[0].val }
          if (stor.lestermes[1].signe) { o = stor.lestermes[1].val } else { o = -stor.lestermes[1].val }
          if (stor.lestermes[2].signe) { m = stor.lestermes[2].val } else { m = -stor.lestermes[2].val }
          p = j3pArrondi(p + o, 1)
          if (stor.lestermes[2].signe) { g = '+' } else { g = '' }
          j3pAffiche(stor.lesdiv.solution, null, '$A = ' + p + g + m + '$\n')
          p = j3pArrondi(p + m, 1)
          j3pAffiche(stor.lesdiv.solution, null, '$A = ' + p + '$\n')
        }
      }
    } else {
      me.afficheBoutonValider()
    }
  }

  function enonceMain () {
    if (((me.questionCourante % ds.nbetapes) === 1) || (ds.nbetapes === 1)) {
      me.videLesZones()
      creeLesDiv()
      stor.Lexo = faisChoixExos()
      stor.lesdiv.travail.classList.add('travail')
      stor.lesdiv.consigne.classList.add('enonce')
    }

    poseQuestion()
    if (ds.Calculatrice) {
      me.fenetresjq = [{ name: 'Calculatrice', title: 'Une calculatrice', left: 600, top: 10 }]
      j3pCreeFenetres(me)
      j3pAfficheCroixFenetres('Calculatrice')
      j3pAjouteBouton(stor.lesdiv.calculatrice, 'Calculatrice', 'MepBoutons', 'Calculatrice', boutonfen)
    }
    me.finEnonce()
  }

  switch (this.etat) {
    case 'enonce':
      // code exécuté au lancement de la section
      if (me.debutDeLaSection) {
        initSection()
      }
      enonceMain()
      break // case "enonce":

    case 'correction':
      // On teste si une réponse a été saisie
      stor.yaexplik = false
      stor.lesdiv.explications.innerHTML = ''
      stor.lesdiv.correction.innerHTML = ''
      stor.yaco = false

      if (yaReponse()) {
        // Bonne réponse
        if (isRepOk()) {
          this._stopTimer()
          this.score++
          stor.lesdiv.correction.style.color = this.styles.cbien
          stor.lesdiv.correction.innerHTML = cBien

          desactivezone()
          passetoutvert()
          if (stor.nedco) {
            if (ds.conseil) {
              let np, nm, b
              j3pAffiche(stor.lesdiv.solution, null, 'Le calcul serait plus rapide en regroupant les termes. \n\n')
              np = ''
              nm = ''
              for (b = 0; b < stor.lestermes.length; b++) {
                if (stor.lestermes[b].signe) { np = np + '+' + stor.lestermes[b].val } else { nm = nm + '-' + stor.lestermes[b].val }
              }
              np = np.substring(1) + nm + '$'
              j3pAffiche(stor.lesdiv.solution, null, '$A = ' + np + '\n')
              np = 0
              nm = 0
              for (b = 0; b < stor.lestermes.length; b++) {
                if (stor.lestermes[b].signe) { np += stor.lestermes[b].val } else { nm += stor.lestermes[b].val }
              }
              np = j3pArrondi(np, 1)
              nm = j3pArrondi(nm, 1)
              j3pAffiche(stor.lesdiv.solution, null, '$A = ' + np + '-' + nm + '$\n')
              np = j3pArrondi(np - nm, 1)
              j3pAffiche(stor.lesdiv.solution, null, '$A = ' + np + '$\n')
            }
          }
          this.etat = 'navigation'
          this.sectionCourante()
        } else {
          // Pas de bonne réponse
          stor.lesdiv.correction.style.color = this.styles.cfaux
          stor.lesdiv.correction.innerHTML = cFaux
          if (this.isElapsed) {
            // A cause de la limite de temps :
            stor.lesdiv.correction.innerHTML += tempsDepasse + '<BR>'
            AffCorrection(true)
            this.etat = 'navigation'
            this.sectionCourante()
          } else {
            // Réponse fausse :
            if (this.essaiCourant < ds.nbchances) {
              stor.lesdiv.correction.innerHTML += '<br>' + essaieEncore
              AffCorrection(false)
            } else {
              // Erreur au nème essai
              AffCorrection(true)
              this.etat = 'navigation'
              this.sectionCourante()
            }
          }
        }
      } else {
        // pas de réponse
        stor.lesdiv.correction.style.color = this.styles.cfaux
        stor.lesdiv.correction.innerHTML = reponseIncomplete
        me.afficheBoutonValider()
      }

      if (ds.theme === 'zonesAvecImageDeFond') {
        if (stor.yaexplik) {
          stor.lesdiv.explications.classList.add('explique')
        }
        if (stor.yaco) {
          stor.lesdiv.solution.classList.add('correction')
        }
      }

      // Obligatoire
      this.finCorrection()
      break // case "correction":

    case 'navigation':
      if (this.sectionTerminee()) {
        this.parcours.pe = getPe(ds, stor)
      } else {
        this.etat = 'enonce'
      }
      this.finNavigation()
      break // case "navigation":
  }
}
