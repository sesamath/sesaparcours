import { j3pAddElt, j3pAjouteBouton, j3pArrondi, j3pGetRandomBool, j3pGetRandomInt, j3pNotify, j3pPGCD, j3pShuffle } from 'src/legacy/core/functions'
import ListeDeroulante from 'src/legacy/outils/listeDeroulante/ListeDeroulante'
import { j3pAffiche } from 'src/lib/mathquill/functions'
import ZoneStyleMathquill3 from 'src/legacy/outils/zoneStyleMathquill/ZoneStyleMathquill3'
import { j3pCreeFenetres, j3pAfficheCroixFenetres, j3pToggleFenetres } from 'src/legacy/core/functionsJqDialog'
import { addDefaultTable } from 'src/legacy/themes/table'
import BulleAide from 'src/legacy/outils/bulleAide/BulleAide'
import textesGeneriques from 'src/lib/core/textes'

const { cBien, cFaux, essaieEncore, reponseIncomplete, tempsDepasse } = textesGeneriques

/**
 * Ajouter une description ici
 * @fileOverview
 * @author Tommy Barroy <t.barroy@laposte.net>
 * @since 2017-10
 */
export const params = {
  outils: ['mathquill', 'calculatrice'],
  /**
   * Ce qui est paramétrable dans l’éditeur de graphe
   * @type {LegacySectionParamsParametres}
   */
  parametres: [
    ['nbrepetitions', 3, 'entier', 'Nombre de répétitions de la section'],
    ['nbchances', 1, 'entier', 'Nombre de répétitions de la section'],
    ['entier', true, 'boolean', '<u>true</u>:  Le nombre peut être entier'],
    ['decimal', true, 'boolean', '<u>true</u>:  Le nombre peut être proposé en écriture décimale'],
    ['fraction', true, 'boolean', '<u>true</u>:  Le nombre peut être proposé en écriture factionnaire'],
    ['racine', true, 'boolean', '<u>true</u>: Le nombre peut être proposé sous la forme d’un radical.'],
    ['transcendant', true, 'boolean', '<u>true</u>: Le nombre peut être transcendant.'],
    ['negatif', true, 'boolean', '<u>true</u>: Le nombre peut être négatif.'],
    ['Oppose', true, 'boolean', '<u>true</u>: L’élève doit donner l’opposé du nombre'],
    ['Distance_a_zero', true, 'boolean', '<u>true</u>: L’élève doit donner la distance à zéro du nombre'],
    ['Entier', true, 'boolean', '<u>true</u>: L’élève doit dire si le nombre est entier.'],
    ['Decimal', true, 'boolean', '<u>true</u>: L’élève doit dire si le nombre est décimal.'],
    ['Rationnel', true, 'boolean', '<u>true</u>: L’élève doit dire si le nombre est rationnel.'],
    ['Signe', true, 'boolean', '<u>true</u>: L’élève doit dire si le nombre est positif.'],
    ['Piege', true, 'boolean', '<u>true</u>: Le nombre peut nas être écrit simplement.'],
    // le modèle gère la compatibilité ascendante avec l’ancien paramètre MiseEnPagePlusClaire
    ['theme', 'standard', 'liste', 'Modèle pour l’affichage', ['standard', 'zonesAvecImageDeFond']]

  ]
}

/**
 * section relatifsoppose01
 * @this {Parcours}
 */
export default function main () {
  const me = this
  const ds = me.donneesSection
  const stor = this.storage
  let fi

  function desactivezone () {
    if (stor.zoneEnt) stor.zoneEnt.disable()
    if (stor.zoneDec) stor.zoneDec.disable()
    if (stor.zoneRat) stor.zoneRat.disable()
    if (stor.zoneSigne) stor.zoneSigne.disable()
    if (stor.zoneOp) stor.zoneOp.disable()
    if (stor.zoneDis) stor.zoneDis.disable()
  }
  function passetoutvert () {
    if (stor.zoneEnt) stor.zoneEnt.corrige(true)
    if (stor.zoneDec) stor.zoneDec.corrige(true)
    if (stor.zoneRat) stor.zoneRat.corrige(true)
    if (stor.zoneSigne) stor.zoneSigne.corrige(true)
    if (stor.zoneOp) stor.zoneOp.corrige(true)
    if (stor.zoneDis) stor.zoneDis.corrige(true)
  }

  function creeLesDiv () {
    stor.lesdiv = {}
    stor.lesdiv.conteneur = j3pAddElt(me.zonesElts.MG, 'div', {
      style: me.styles.etendre('toutpetit.enonce', { padding: '10px' })
    })
    stor.lesdiv.consigne = addDefaultTable(stor.lesdiv.conteneur, 1, 1)[0][0]
    stor.lesdiv.travail = addDefaultTable(stor.lesdiv.conteneur, 1, 1)[0][0]
    stor.lesdiv.explications = j3pAddElt(stor.lesdiv.conteneur, 'div')
    stor.lesdiv.calculatrice = j3pAddElt(me.zonesElts.MD, 'div', {
      style: me.styles.etendre('toutpetit.enonce', { padding: '10px' })
    })
    stor.lesdiv.correction = j3pAddElt(me.zonesElts.MD, 'div', {
      style: me.styles.etendre('toutpetit.enonce', { padding: '10px' })
    })
    stor.lesdiv.explications.style.color = me.styles.cfaux
  }
  function faisChoixExos () {
    stor.clvspe = false
    const e = stor.exos.pop()
    const radic = [2, 3, 5, 6, 7, 8, 10, 11, 12, 13, 14, 15, 17]
    let t
    switch (e.val) {
      case 'entier' :
        e.num = j3pGetRandomInt(1, 1000)
        e.den = 'z'
        e.sqrt = false
        e.pi = false
        break
      case 'decimal' :
        e.num = j3pArrondi(j3pGetRandomInt(1, 1000) / 10000, 4)
        if (Math.trunc(e.num) === e.num) {
          e.nb = j3pArrondi(e.num + 0.1, 1)
        }
        e.den = 'z'
        e.sqrt = false
        e.pi = false
        break
      case 'fraction':
        t = unefractionnondecimale()
        e.num = t.num
        e.den = t.den
        e.sqrt = false
        stor.clvspe = true
        e.pi = false
        break
      case 'racine':
        e.den = 'z'
        e.num = radic[j3pGetRandomInt(0, radic.length - 1)]
        e.sqrt = true
        stor.clvspe = true
        e.pi = false
        break
      case 'transcendant':
        t = unefractionnondecimale()
        e.num = t.num
        e.den = t.den
        e.sqrt = false
        stor.clvspe = true
        e.pi = true
    }
    return e
  }
  function isRepOk () {
    passetoutvert()
    let ok
    let ok2 = true
    let a, b, c
    stor.repEnt = false
    stor.repDec = false
    stor.repRat = false
    stor.repSi = false
    stor.repOpVal = false
    stor.repOpSi = false
    stor.repDisSi = false
    stor.repDisVal = false
    stor.errEcrit = false
    stor.errZeroInut = false
    stor.errVirg = false

    if (stor.zoneEnt !== undefined) {
      ok2 = (stor.zoneEnt.reponse === 'est entier.')
      switch (stor.Lexo.val) {
        case 'entier':stor.repEnt = !ok2
          break
        default:stor.repEnt = ok2
      }
    }
    if (stor.repEnt)stor.zoneEnt.corrige(false)
    ok = (!stor.repEnt)
    if (stor.zoneDec !== undefined) {
      ok2 = (stor.zoneDec.reponse === 'est décimal.')
      switch (stor.Lexo.val) {
        case 'entier':
        case 'decimal':
          stor.repDec = !ok2
          break
        default:stor.repDec = ok2
      }
    }
    if (stor.repDec) stor.zoneDec.corrige(false)
    ok = ok && (!stor.repDec)
    if (stor.zoneRat !== undefined) {
      ok2 = (stor.zoneRat.reponse === 'est rationnel.')
      switch (stor.Lexo.val) {
        case 'entier':
        case 'decimal':
        case 'fraction':
          stor.repRat = !ok2
          break
        default:stor.repRat = ok2
      }
    }
    if (stor.repRat) stor.zoneRat.corrige(false)
    ok = ok && (!stor.repRat)
    if (stor.zoneSigne !== undefined) {
      ok2 = stor.Lexo.s !== '-'
      if (stor.zoneSigne.reponse !== 'est négatif.') {
        stor.repSi = !ok2
      } else {
        stor.repSi = ok2
      }
    }
    if (stor.repSi)stor.zoneSigne.corrige(false)
    ok = ok && (!stor.repSi)
    if (stor.zoneOp !== undefined) {
      a = stor.zoneOp.reponse()
      // verif c’est "signe" + "nombre"
      if ((a.indexOf('+') !== -1) && (a.indexOf('+') !== 0)) {
        stor.zoneOp.corrige(false)
        stor.errEcrit = true
        ok = false
      } else
        if ((a.indexOf('-') !== -1) && (a.indexOf('-') !== 0)) {
          stor.zoneOp.corrige(false)
          stor.errEcrit = true
          ok = false
        } else
          if ((a.indexOf('+') !== a.lastIndexOf('+')) || (a.indexOf('-') !== a.lastIndexOf('-'))) {
            stor.zoneOp.corrige(false)
            stor.errEcrit = true
            ok = false
          } else {
            b = a.indexOf('-') === -1 // b = true si rep est positive
            a = a.replace('+', '').replace('-', '')
            // stor.repOpSi
            c = (stor.Lexo.s === '+') || (stor.Lexo.s === 'n') // c = true si nbdepart est positif
            if (c === b) {
              stor.zoneOp.corrige(false)
              stor.repOpSi = true
              ok = false
            } else {
              // stor.repOpVal
              a = donneVal(a)
              if (a === 'err') {
                stor.errEcrit = true
                stor.zoneOp.corrige(false)
                ok = false
              } else
                if (!cPasEgal(a)) {
                  stor.repOpVal = true
                  stor.zoneOp.corrige(false)
                  ok = false
                }
            }
          }
    }
    if (stor.repOpSi || stor.repOpVal)stor.zoneOp.corrige(false)
    ok = ok && (!stor.repOpSi) && (!stor.repOpVal)
    if (stor.zoneDis !== undefined) {
      a = stor.zoneDis.reponse()
      // verif c’est "signe" + "nombre"
      if ((a.indexOf('+') !== -1) && (a.indexOf('+') !== 0)) {
        stor.zoneDis.corrige(false)
        stor.errEcrit = true
        ok = false
      } else
        if ((a.indexOf('-') !== -1) && (a.indexOf('-') !== 0)) {
          stor.zoneDis.corrige(false)
          stor.errEcrit = true
          ok = false
        } else
          if ((a.indexOf('+') !== a.lastIndexOf('+')) || (a.indexOf('-') !== a.lastIndexOf('-'))) {
            stor.zoneDis.corrige(false)
            stor.errEcrit = true
            ok = false
          } else {
            b = a.indexOf('-') === -1
            a = a.replace('+', '').replace('-', '')
            if (!b) {
              stor.zoneDis.corrige(false)
              stor.repDisSi = true
              ok = false
            } else {
              // stor.repOpVal
              a = donneVal(a)
              if (a === 'err') {
                stor.errEcrit = true
                stor.zoneDis.corrige(false)
                ok = false
              } else
                if (!cPasEgal(a)) {
                  stor.repDisVal = true
                  stor.zoneDis.corrige(false)
                  ok = false
                }
            }
          }
    }
    if (stor.repDisSi || stor.repDisVal)stor.zoneDis.corrige(false)
    ok = ok && (!stor.repDisSi) && (!stor.repDisVal)
    return ok
  }
  function cPasEgal (s) {
    let n1, n2, d1, d2
    if (s.racnum) { n1 = s.num } else { n1 = s.num * s.num }
    if (s.racden) { d1 = s.den } else { d1 = s.den * s.den }
    d2 = stor.Lexo.den
    if (d2 === 'z') d2 = 1
    if (stor.Lexo.sqrt) {
      n2 = stor.Lexo.num
    } else {
      n2 = stor.Lexo.num * stor.Lexo.num
      d2 = d2 * d2
    }
    if (s.yapnum !== stor.Lexo.pi) return false
    if (s.yapden) return false
    if (s.yapracnum) return false
    return n1 * d2 === n2 * d1
  }
  function donneVal (q) {
    let a, b, yapnum, yapden
    yapnum = yapden = false
    let yapracnum = false
    if (q.indexOf('π') !== q.lastIndexOf('π')) {
      stor.errEcrit = true
      return 'err'
    }
    if (q.indexOf('π') === q.length - 1) {
      yapnum = true
      q = q.replace('π', '')
    }
    if (q.indexOf('frac') !== -1) {
      a = q.split('frac').length - 1
      if (a !== 1) return 'err'
      a = q.split('{').length - 1
      b = q.split('}').length - 1
      if (a !== 2) return 'err'
      if (b !== 2) return 'err'
      if (q.substring(0, 6) !== '\\frac{') return 'err'
      if (q[q.length - 1] !== '}') return 'err'
      a = q.substring(6, q.indexOf('}{'))
      b = q.substring(q.indexOf('}{') + 2, q.length - 1)
      if ((a.indexOf('π') !== -1) && (a.indexOf('π') !== a.length - 1)) {
        stor.errEcrit = true
        return 'err'
      }
      if (a.indexOf('π') !== -1) {
        a = a.replace('π', '')
        if (yapnum) {
          stor.errEcrit = true
          return 'err'
        }
        yapnum = true
      }
      if ((b.indexOf('π') !== -1) && (b.indexOf('π') !== b.length - 1)) {
        stor.errEcrit = true
        return 'err'
      }
      if (b.indexOf('π') !== -1) {
        b = b.replace('π', '')
        yapden = true
      }
      if (yapnum && yapden) {
        stor.errEcrit = true
        return 'err'
      }
      if (!estEntierBienEcrit(a)) return 'err'
      if (!estEntierBienEcrit(b)) return 'err'
      a = parseFloat(a)
      b = parseFloat(b)
      if (j3pPGCD(a, b, { negativesAllowed: true, valueIfZero: 1 }) !== 1) {
        stor.errEcrit = true
        return 'err'
      }
      return { num: a, den: b, racnum: false, racden: false, yapnum, yapden, yapracnum: false }
    }
    if (q.indexOf('rac') !== -1) {
      a = q.split('rac').length - 1
      if (a !== 1) return 'err'
      a = q.split('{').length - 1
      b = q.split('}').length - 1
      if (a !== 1) return 'err'
      if (b !== 1) return 'err'
      if (q.substring(0, 8) !== '\\racine{') return 'err'
      if (q[q.length - 1] !== '}') return 'err'
      a = q.substring(8, q.length - 1)
      if ((a.indexOf('π') !== -1) && (a.indexOf('π') !== a.length - 1)) {
        stor.errEcrit = true
        return 'err'
      }
      if (a.indexOf('π') !== -1) {
        a = a.replace('π', '')
        yapracnum = true
      }
      if (yapracnum && a === '1') return 'err'
      if (yapracnum && a === '') a = '1'
      if (!estDecimalBienEcrit(a)) return 'err'
      return { num: parseFloat(a.replace(',', '.')), den: 1, racnum: true, racden: true, yapnum, yapden, yapracnum }
    }
    if (q.indexOf(',') !== -1) {
      if (!estDecimalBienEcrit(q)) return 'err'
      return { num: parseFloat(q.replace(',', '.')), den: 1, racnum: false, racden: false, yapnum, yapden, yapracnum: false }
    }
    if (yapnum && q === '1') return 'err'
    if (yapnum && q === '') q = '1'
    if (!estEntierBienEcrit(q)) return 'err'
    return { num: parseFloat(q), den: 1, racnum: false, racden: false, yapnum, yapden, yapracnum: false }
  }

  function estDecimalBienEcrit (x) {
    const t = x.replace(/,/g, '.')
    if (t.indexOf('.') === -1) return estEntierBienEcrit(x)
    if ((t.indexOf('.') === 0) || (t.indexOf('.') === t.length - 1) || (t.indexOf('.') !== t.lastIndexOf('.'))) {
      stor.errVirg = true
      return false
    }
    if (((t[0] === '0') && (t[1] !== '.')) || (t[t.length - 1] === '0')) {
      stor.errZeroInut = true
      return false
    }
    return true
  }

  function estEntierBienEcrit (nbEnString) {
    if (nbEnString === '') return false
    if (/[.,].*0$/.test(nbEnString)) {
      // y’a une virgure et un 0 à la fin
      stor.errZeroInut = true
      return false
    }
    if (/[.,]/.test(nbEnString)) {
      stor.errEntierAttendu = true
      return false
    }
    if (/[^0-9]/.test(nbEnString)) {
      // y’a autre chose que des chiffres
      stor.errEcrit = true
      return false
    }
    return true
  }

  function initSection () {
    me.validOnEnter = false // ex donneesSection.touche_entree
    // Construction de la page
    me.construitStructurePage('presentation1bis')

    stor.yakR = false
    const cas = []
    if (ds.entier) cas.push('entier')
    if (ds.decimal) cas.push('decimal')
    if (ds.fraction) cas.push('fraction')
    if (ds.racine) cas.push('racine')
    if (ds.transcendant) cas.push('transcendant')
    if (cas.length === 0) cas.push('entier')

    stor.exosDistincts = []
    stor.tpos = []
    for (const val of cas) {
      stor.exosDistincts.push({ val })
      stor.tpos.push(val)
    }
    stor.exosDistincts = j3pShuffle(stor.exosDistincts)

    const signes = (ds.Signe || ds.negatif)
      ? ['+', '-', 'n']
      : ['n']

    for (const [i, obj] of stor.exosDistincts.entries()) {
      obj.s = signes[i % signes.length] // on prend chaque signe à tour de rôle
      obj.aff = ds.Piege ? j3pGetRandomBool() : true
    }
    // on veut à peu près autant de chaque, et les avoir tous si nbitems > exosDistincts.length
    // => pas de j3pGetRandomElt sur exosDistincts, on pioche dedans en repartant au début quand on a tout pris
    stor.exos = []
    while (stor.exos.length < ds.nbitems) stor.exos.push(stor.exosDistincts[stor.exos.length % stor.exosDistincts.length])

    stor.larestric = '0123456789+-/.,ùπ'
    stor.clvspe = false

    me.typederreurs = [0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0]

    stor.bulles = []
    me.afficheTitre('Etudier un nombre')
    enonceMain()
  }
  function afficheCorrection (bool) {
    // j3pBarre(stor.divtravailencours)
    let t, t2, i
    if (!stor.Lexo.aff) {
      stor.yaexplik = true
      j3pAffiche(stor.lesdiv.explications, null, '$ ' + stor.affnbfalse + ' = ' + stor.affnbtrue + ' $ \n')
    }
    if (stor.errEcrit) {
      j3pAffiche(stor.lesdiv.explications, null, 'Erreur d’écriture ! \n')
    }
    if (stor.errZeroInut) {
      j3pAffiche(stor.lesdiv.explications, null, 'Supprime tous les zéros inutiles ! \n')
    }
    if (stor.errVirg) {
      j3pAffiche(stor.lesdiv.explications, null, 'Problème de virgule! \n')
    }

    if (bool) {
      if (stor.repEnt) {
        switch (stor.Lexo.val) {
          case 'entier':
            if (stor.useAide) {
              stor.bulles.push(new BulleAide(stor.tabEnt[0][3], "Ce nombre est entier, <br><i>il peut s'écrire sans virgule</i>.", { place: 5 }))
            } else {
              j3pAffiche(stor.lesdiv.explications, null, "Ce nombre est entier, <i>il peut s'écrire sans virgule</i>. \n")
              stor.yaexplik = true
            }
            j3pAffiche(stor.tabEnt[0][2], null, 'est entier.')
            break
          case 'decimal':
            if (stor.useAide) {
              stor.bulles.push(new BulleAide(stor.tabEnt[0][3], 'Ce nombre n’est pas entier, <br><i>son écriture décimale comporte obligatroirement une virgule</i>.', { place: 5 }))
            } else {
              stor.yaexplik = true
              j3pAffiche(stor.lesdiv.explications, null, 'Ce nombre n’est pas entier, <i>son écriture décimale comporte obligatroirement une virgule</i>. \n')
            }
            j3pAffiche(stor.tabEnt[0][2], null, 'n’est pas entier.')
            break
          case 'fraction':
          case 'racine':
          case 'pi':
            if (stor.useAide) {
              stor.bulles.push(new BulleAide(stor.tabEnt[0][3], 'Ce nombre n’est pas entier, <br><i>il n’a pas d’écriture décimale finie</i>. ', { place: 5 }))
            } else {
              stor.yaexplik = true
              j3pAffiche(stor.lesdiv.explications, null, 'Ce nombre n’est pas entier, <i>il n’a pas d’écriture décimale finie</i>. \n')
            }
            j3pAffiche(stor.tabEnt[0][2], null, 'n’est pas entier.')
            break
        }
        stor.zoneEnt.barre()
      }

      if (stor.repDec) {
        switch (stor.Lexo.val) {
          case 'entier':
            if (stor.useAide) {
              const content = 'Ce nombre est aussi décimal: $ ' + stor.affnbtrue + ' = ' + stor.affnbtrue + ',0 $  '
              stor.bulles.push(new BulleAide(stor.tabDec[0][3], content, { place: 5 }))
            } else {
              stor.yaexplik = true
              j3pAffiche(stor.lesdiv.explications, null, 'Ce nombre est aussi décimal: $ ' + stor.affnbtrue + ' = ' + stor.affnbtrue + ',0 $ \n')
            }
            j3pAffiche(stor.tabDec[0][2], null, 'est décimal.')
            break
          case 'decimal':
            if (stor.useAide) {
              stor.bulles.push(new BulleAide(stor.tabDec[0][3], 'Ce nombre décimal, <i>puisqu’il a une écriture décimale</i>.', { place: 5 }))
            } else {
              stor.yaexplik = true
              j3pAffiche(stor.lesdiv.explications, null, 'Ce nombre décimal, <i>puisqu’il a une écriture décimale</i>. \n')
            }
            j3pAffiche(stor.tabDec[0][2], null, 'est décimal.')
            break
          case 'fraction':
          case 'racine':
          case 'pi':
            if (stor.useAide) {
              stor.bulles.push(new BulleAide(stor.tabDec[0][3], 'Ce nombre n’est pas décimal, <br><i>il n’a pas d’écriture décimale finie</i>.', { place: 5 }))
            } else {
              stor.yaexplik = true
              j3pAffiche(stor.lesdiv.explications, null, 'Ce nombre n’est pas décimal, <i>il n’a pas d’écriture décimale finie</i>. \n')
            }
            j3pAffiche(stor.tabDec[0][2], null, 'n’est pas décimal.')
            break
        }
        stor.zoneDec.barre()
      }

      if (stor.repRat) {
        switch (stor.Lexo.val) {
          case 'entier':
            if (stor.useAide) {
              stor.bulles.push(new BulleAide(stor.tabRat[0][3], 'Ce nombre est aussi rationnel: $ ' + stor.affnbtrue + ' = \\frac{' + stor.affnbtrue + '}{ 1 } $', { place: 5 }))
            } else {
              stor.yaexplik = true
              j3pAffiche(stor.lesdiv.explications, null, 'Ce nombre est aussi rationnel: $ ' + stor.affnbtrue + ' = \\frac{' + stor.affnbtrue + '}{ 1 } $ \n')
            }
            j3pAffiche(stor.tabRat[0][2], null, 'est rationnel.')
            break
          case 'decimal':
            t = stor.affnbtrue.indexOf(',')
            t2 = '1'
            for (i = 0; i < stor.affnbtrue.lenght - t; i++) {
              t2 += '0'
            }
            if (stor.useAide) {
              stor.bulles.push(new BulleAide(stor.tabRat[0][3], 'Ce nombre est aussi rationnel: $ ' + stor.affnbtrue + ' = \\frac{' + stor.affnbtrue.replace(',', '') + '}{ ' + t2 + ' } $', { place: 5 }))
            } else {
              stor.yaexplik = true
              j3pAffiche(stor.lesdiv.explications, null, 'Ce nombre est aussi rationnel: $ ' + stor.affnbtrue + ' = \\frac{' + stor.affnbtrue.replace(',', '') + '}{ ' + t2 + ' } $ \n')
            }
            j3pAffiche(stor.tabRat[0][2], null, 'est rationnel.')
            break
          case 'fraction':
            if (stor.useAide) {
              stor.bulles.push(new BulleAide(stor.tabRat[0][3], "Ce nombre est rationnel, <br><i>il peut s'écrire sous la forme d’une fraction</i>. <br>", { place: 5 }))
            } else {
              stor.yaexplik = true
              j3pAffiche(stor.lesdiv.explications, null, "Ce nombre est rationnel, <i>il peut s'écrire sous la forme d’une fraction</i>. \n")
            }
            j3pAffiche(stor.tabRat[0][2], null, 'est rationnel.')
            break
          case 'racine':
          case 'pi':
            if (stor.useAide) {
              stor.bulles.push(new BulleAide(stor.tabRat[0][3], "Ce nombre n’est pas rationnel, <br><i>il ne peut pas s'écrire sous la forme d’une fraction</i>. <br>", { place: 5 }))
            } else {
              stor.yaexplik = true
              j3pAffiche(stor.lesdiv.explications, null, "Ce nombre n’est pas rationnel, <i>il ne peut pas s'écrire sous la forme d’une fraction</i>. \n")
            }
            j3pAffiche(stor.tabRat[0][2], null, 'n’est pas rationnel.')
            break
        }
        stor.zoneRat.barre()
      }

      if (stor.repSi) {
        switch (stor.Lexo.s) {
          case '+':
            if (stor.useAide) {
              stor.bulles.push(new BulleAide(stor.tabSig[0][3], 'Le signe $+$ indique qu’un nombre est positif.', { place: 5 }))
            } else {
              stor.yaexplik = true
              j3pAffiche(stor.lesdiv.explications, null, 'Le signe $+$ indique qu’un nombre est positif. \n')
            }
            j3pAffiche(stor.tabSig[0][2], null, 'est positif.')
            break
          case '-':
            if (stor.useAide) {
              stor.bulles.push(new BulleAide(stor.tabSig[0][3], 'Le signe $-$ indique qu’un nombre est négatif.', { place: 5 }))
            } else {
              stor.yaexplik = true
              j3pAffiche(stor.lesdiv.explications, null, 'Le signe $-$ indique qu’un nombre est négatif. \n')
            }
            j3pAffiche(stor.tabSig[0][2], null, 'est négatif.')
            break
          case 'n':
            if (stor.useAide) {
              stor.bulles.push(new BulleAide(stor.tabSig[0][3], 'Quand aucun signe n’est indiqué, le nombre est positif.', { place: 5 }))
            } else {
              stor.yaexplik = true
              j3pAffiche(stor.lesdiv.explications, null, 'Quand aucun signe n’est indiqué, le nombre est positif. \n')
            }
            j3pAffiche(stor.tabSig[0][2], null, 'est positif.')
            break
        }
        stor.zoneSigne.barre()
      }

      if (stor.repOpSi) {
        if (stor.useAide) {
          stor.bulles.push(new BulleAide(stor.tabOp[0][3], 'Deux nombres opposés sont de signes contraires.', { place: 5 }))
        } else {
          stor.yaexplik = true
          j3pAffiche(stor.lesdiv.explications, null, 'Deux nombres opposés sont de signes contraires. \n')
        }
      }
      if (stor.repOpVal) {
        if (stor.useAide) {
          stor.bulles.push(new BulleAide(stor.tabOp[0][3], 'Deux nombres opposés ont la même distance à zéro.', { place: 5 }))
        } else {
          j3pAffiche(stor.lesdiv.explications, null, 'Deux nombres opposés ont la même distance à zéro. \n')
        }
      }
      if (stor.repOpSi || stor.repOpVal) {
        t = stor.affnbtrue.replace('+', '').replace('-', '')
        if (stor.Lexo.s !== '-') t = '-' + t
        j3pAffiche(stor.tabOp[0][2], null, '$' + t + '$')
        stor.zoneOp.barre()
      }

      if (stor.repDisSi) {
        if (stor.useAide) {
          stor.bulles.push(new BulleAide(stor.tabDis[0][3], 'Une distance à zéro est toujours positive.', { place: 5 }))
        } else {
          stor.yaexplik = true
          j3pAffiche(stor.lesdiv.explications, null, 'Une distance à zéro est toujours positive. \n')
        }
      }
      if (stor.repDisVal) {
        if (stor.useAide) {
          stor.bulles.push(new BulleAide(stor.tabDis[0][3], 'La distance à zéro d’un nombre est égale à sa valeur numérique.', { place: 5 }))
        } else {
          stor.yaexplik = true
          j3pAffiche(stor.lesdiv.explications, null, 'La distance à zéro d’un nombre est égale à sa valeur numérique. \n')
        }
      }
      if (stor.repDisSi || stor.repDisVal) {
        t = stor.affnbtrue.replace('+', '').replace('-', '')
        j3pAffiche(stor.tabDis[0][2], null, '$' + t + '$')
        stor.zoneDis.barre()
      }
      desactivezone()
    }
  }
  function poseQuestion () {
    let buffSigne = stor.Lexo.s
    if (buffSigne === 'n') buffSigne = ''
    stor.affnbtrue = buffSigne + Affichemoibien(stor.Lexo.num, stor.Lexo.den)
    if (stor.Lexo.aff) { stor.affnbfalse = stor.affnbtrue } else {
      stor.affnbfalse = Affichemoipasbien(stor.Lexo.num, stor.Lexo.den)
      if ((buffSigne === '-') && (stor.needP)) {
        stor.affnbfalse = '-(' + stor.affnbfalse + ')'
      } else {
        stor.affnbfalse = buffSigne + stor.affnbfalse
      }
    }
    j3pAffiche(stor.lesdiv.consigne, null, '<b>On désire étudier ce nombre: </b> $' + stor.affnbfalse + '$ \n')
    if (!stor.Lexo.aff && (ds.Oppose || ds.Distance_a_zero)) {
      const yu = addDefaultTable(stor.lesdiv.consigne, 1, 1)[0][0]
      j3pAffiche(yu, null, '<i>Les réponses doivent être simplifiées.</i> \n\n')
      yu.style.color = me.styles.petit.correction.color
    } else {
      j3pAffiche(stor.lesdiv.consigne, null, '\n')
    }
    stor.nbquest = 0
    if (ds.Entier) {
      stor.nbquest++
      stor.tabEnt = addDefaultTable(stor.lesdiv.travail, 1, 4)
      j3pAffiche(stor.tabEnt[0][0], null, 'Ce nombre&nbsp;')
    }
    if (ds.Decimal) {
      stor.nbquest++
      stor.tabDec = addDefaultTable(stor.lesdiv.travail, 1, 4)
      j3pAffiche(stor.tabDec[0][0], null, 'Ce nombre&nbsp;')
    }
    if (ds.Rationnel) {
      stor.nbquest++
      stor.tabRat = addDefaultTable(stor.lesdiv.travail, 1, 4)
      j3pAffiche(stor.tabRat[0][0], null, 'Ce nombre&nbsp;')
    }
    if (ds.Signe) {
      stor.nbquest++
      stor.tabSig = addDefaultTable(stor.lesdiv.travail, 1, 4)
      j3pAffiche(stor.tabSig[0][0], null, 'Ce nombre&nbsp;')
    }
    if (ds.Oppose) {
      stor.nbquest++
      stor.tabOp = addDefaultTable(stor.lesdiv.travail, 1, 4)
      j3pAffiche(stor.tabOp[0][0], null, 'L’opposé de ce nombre est&nbsp;')
    }
    if (ds.Distance_a_zero) {
      stor.nbquest++
      stor.tabDis = addDefaultTable(stor.lesdiv.travail, 1, 4)
      j3pAffiche(stor.tabDis[0][0], null, 'La distance à zéro de ce nombre est&nbsp;')
      stor.zoneDis = new ZoneStyleMathquill3(stor.tabDis[0][1], { restric: stor.larestric, limitenb: 1, limite: 12, hasAutoKeyboard: stor.clvspe, enter: me.sectionCourante.bind(me), bloqueracine: '+-/', bloqueFraction: '+-ùπ' })
      stor.tabDis[0][2].style.color = me.styles.petit.correction.color
    }
    if (ds.Entier) {
      stor.zoneEnt = ListeDeroulante.create(stor.tabEnt[0][1], ['Choisir', 'est entier.', 'n’est pas entier.'], { centre: true })
      stor.tabEnt[0][2].style.color = me.styles.petit.correction.color
    }
    if (ds.Decimal) {
      stor.zoneDec = ListeDeroulante.create(stor.tabDec[0][1], ['Choisir', 'est décimal.', 'n’est pas décimal.'], { centre: true })
      stor.tabDec[0][2].style.color = me.styles.petit.correction.color
    }
    if (ds.Rationnel) {
      stor.zoneRat = ListeDeroulante.create(stor.tabRat[0][1], ['Choisir', 'est rationnel.', 'n’est pas rationnel.'], { centre: true })
      stor.tabRat[0][2].style.color = me.styles.petit.correction.color
    }
    if (ds.Signe) {
      stor.zoneSigne = ListeDeroulante.create(stor.tabSig[0][1], ['Choisir', 'est positif.', 'est négatif.'], { centre: true })
      stor.tabSig[0][2].style.color = me.styles.petit.correction.color
    }
    if (ds.Oppose) {
      stor.zoneOp = new ZoneStyleMathquill3(stor.tabOp[0][1], { restric: stor.larestric, limitenb: 1, limite: 12, hasAutoKeyboard: stor.clvspe, enter: me.sectionCourante.bind(me), bloqueracine: '+-/', bloqueFraction: '+-ùπ' })
      stor.tabOp[0][2].style.color = me.styles.petit.correction.color
    }
    stor.useAide = stor.nbquest > 2
  }
  function yaReponse () {
    if (stor.zoneEnt !== undefined) {
      if (!stor.zoneEnt.changed) {
        stor.zoneEnt.focus()
        return false
      }
    }
    if (stor.zoneDec !== undefined) {
      if (!stor.zoneDec.changed) {
        stor.zoneDec.focus()
        return false
      }
    }
    if (stor.zoneRat !== undefined) {
      if (!stor.zoneRat.changed) {
        stor.zoneRat.focus()
        return false
      }
    }
    if (stor.zoneSigne !== undefined) {
      if (!stor.zoneSigne.changed) {
        stor.zoneSigne.focus()
        return false
      }
    }
    if (stor.zoneOp !== undefined) {
      if (stor.zoneOp.reponse() === '') {
        stor.zoneOp.focus()
        return false
      }
    }
    if (stor.zoneDis !== undefined) {
      if (stor.zoneDis.reponse() === '') {
        stor.zoneDis.focus()
        return false
      }
    }
    return true
  }
  function boutonfen () {
    j3pToggleFenetres('Calculatrice')
  }

  function copiede (tab) {
    let i
    const ret = []
    for (i = 0; i < tab.length; i++) {
      ret[i] = tab[i]
    }
    return ret
  }
  function unefractionnondecimale () {
    const ret = {}
    const multpos = [3, 7, 11, 13, 17, 2, 5, 1]
    const multposden = [3, 7, 11, 13, 17]
    const i = j3pGetRandomInt(0, multposden.length - 1)
    const kden = multposden[i]
    multposden.splice(i, 1)
    multpos.splice(i, 1)
    ret.num = multpos[j3pGetRandomInt(0, multpos.length - 1)]
    ret.den = kden
    return ret
  }
  function Affichemoibien () {
    switch (stor.Lexo.val) {
      case 'entier' :
        return stor.Lexo.num + ''
      case 'decimal' :
        return (stor.Lexo.num + '').replace('.', ',')
      case 'fraction':
        return '\\frac{' + stor.Lexo.num + '}{' + stor.Lexo.den + '}'
      case 'racine':
        return '\\sqrt{' + stor.Lexo.num + '}'
      case 'transcendant':
        return '\\frac{' + stor.Lexo.num + '}{' + stor.Lexo.den + '}\\pi'
    }
  }
  function Affichemoipasbien () {
    let pasbiencomment
    let nb
    let nb2
    let nb3
    let i
    stor.needP = false
    switch (stor.Lexo.val) {
      case 'entier' :
        pasbiencomment = copiede(stor.tpos)
        switch (pasbiencomment[j3pGetRandomInt(0, pasbiencomment.length - 1)]) {
          case 'entier':
            return '0' + stor.Lexo.num
          case 'decimal':
            return stor.Lexo.num + ',0'
          case 'fraction':
            nb = j3pGetRandomInt(2, 5)
            return '\\frac{' + nb * stor.Lexo.num + '}{' + nb + '}'
          case 'racine':
            return '\\sqrt {' + stor.Lexo.num * stor.Lexo.num + '}'
          case 'transcendant':
            return stor.Lexo.num + ' + 0\\pi'
        }
        break
      case 'decimal' :
        pasbiencomment = copiede(stor.tpos)
        switch (pasbiencomment[j3pGetRandomInt(0, pasbiencomment.length - 1)]) {
          case 'entier':
          case 'decimal':
            return ('0' + stor.Lexo.num + '0').replace('.', ',')
          case 'fraction':
            nb = stor.Lexo.num + ''
            nb2 = nb.indexOf('.')
            nb3 = '1'
            for (i = 0; i < nb.length - nb2 - 1; i++) {
              nb3 += '0'
            }
            return '\\frac{' + nb.replace('.', '') + '}{' + nb3 + '}'
          case 'racine':
            return '\\sqrt {' + (j3pArrondi(stor.Lexo.num * stor.Lexo.num, 8) + '').replace('.', ',') + '}'
          case 'transcendant':
            stor.needP = true
            return '0 \\pi + ' + (stor.Lexo.num + '').replace('.', ',')
        }
        break
      case 'fraction':
        pasbiencomment = copiede(stor.tpos)
        switch (pasbiencomment[j3pGetRandomInt(0, pasbiencomment.length - 1)]) {
          case 'entier':
          case 'decimal':
          case 'fraction':
            return '\\frac{0' + stor.Lexo.num + '}{0' + stor.Lexo.den + '}'
          case 'racine':
            return '\\frac {\\sqrt {' + ((stor.Lexo.num * stor.Lexo.num) + '').replace('.', ',') + '}}{' + stor.Lexo.den + '}'
          case 'transcendant':
            return '\\frac { ' + stor.Lexo.num + '\\pi}{ ' + stor.Lexo.den + '\\pi}'
        }
        break
      case 'racine':
        pasbiencomment = copiede(stor.tpos)
        switch (pasbiencomment[j3pGetRandomInt(0, pasbiencomment.length - 1)]) {
          case 'entier':
          case 'decimal':
          case 'fraction':
          case 'racine':
            return '\\frac {\\sqrt {' + (stor.Lexo.num + '').replace('.', ',') + '}}{1}'
          case 'transcendant':
            return '\\frac { \\sqrt {' + stor.Lexo.num + '}\\pi}{ \\pi }'
        }
        break
      case 'transcendant':
        nb = j3pGetRandomInt(2, 17)
        return '\\frac{' + (stor.Lexo.num * nb) + '}{' + (stor.Lexo.den * nb) + '}\\pi'
    }
  }

  function enonceMain () {
    for (const bulle of stor.bulles) {
      if (bulle) {
        try {
          bulle.disable()
        } catch (e) {
          j3pNotify('bug bulle', { storBulle: stor.bulles, labulle: bulle })
        }
      }
    }
    stor.bulles = []
    me.videLesZones()
    creeLesDiv()
    stor.Lexo = faisChoixExos()
    stor.lesdiv.travail.classList.add('travail')
    stor.lesdiv.consigne.classList.add('enonce')
    me.zonesElts.MG.classList.add('fond')
    poseQuestion()
    me.fenetresjq = [{ name: 'Calculatrice', title: 'Une calculatrice', left: 600, top: 10 }]
    j3pCreeFenetres(me)
    j3pAfficheCroixFenetres('Calculatrice')
    j3pAjouteBouton(stor.lesdiv.calculatrice, 'Calculatrice', 'MepBoutons', 'Calculatrice', boutonfen)
    me.finEnonce()
  }

  switch (this.etat) {
    case 'enonce':
      // code exécuté au lancement de la section
      if (me.debutDeLaSection) {
        initSection()
      } else {
        enonceMain()
      }
      break // case "enonce":

    case 'correction':
      // On teste si une réponse a été saisie
      stor.yaexplik = false
      stor.lesdiv.explications.innerHTML = ''
      stor.lesdiv.correction.innerHTML = ''
      fi = false

      if (yaReponse()) {
        // Bonne réponse
        if (isRepOk()) {
          this._stopTimer()
          this.score++
          stor.lesdiv.correction.style.color = this.styles.cbien
          stor.lesdiv.correction.innerHTML = cBien

          desactivezone()
          passetoutvert()

          this.typederreurs[0]++
          this.etat = 'navigation'
          this.sectionCourante()
        } else {
          // Pas de bonne réponse
          stor.lesdiv.correction.style.color = this.styles.cfaux
          stor.lesdiv.correction.innerHTML = cFaux
          if (this.isElapsed) {
            // A cause de la limite de temps :
            stor.lesdiv.correction.innerHTML += tempsDepasse + '<BR>'
            this.typederreurs[10]++
            afficheCorrection(true)
            fi = true
            this.etat = 'navigation'
            this.sectionCourante()
          } else {
            // Réponse fausse :
            if (me.essaiCourant < ds.nbchances) {
              stor.lesdiv.correction.innerHTML += '<br>' + essaieEncore

              afficheCorrection(false)

              this.typederreurs[1]++
            } else {
              // Erreur au nème essai
              afficheCorrection(true)
              fi = true

              this.typederreurs[2]++
              this.etat = 'navigation'
              this.sectionCourante()
            }
          }
        }
      } else {
        // pas de réponse
        stor.lesdiv.correction.style.color = this.styles.cfaux
        stor.lesdiv.correction.innerHTML = reponseIncomplete
      }

      if (stor.yaexplik) stor.lesdiv.explications.classList.add('explique')
      if (fi) {
        if (stor.repEnt) stor.tabEnt[0][2].classList.add('correction')
        if (stor.repDec) stor.tabDec[0][2].classList.add('correction')
        if (stor.repRat) stor.tabRat[0][2].classList.add('correction')
        if (stor.repSi) stor.tabSig[0][2].classList.add('correction')
        if (stor.repOpVal || stor.repOpSi) stor.tabOp[0][2].classList.add('correction')
        if (stor.repDisVal || stor.repDisSi) stor.tabDis[0][2].classList.add('correction')
      }
      // Obligatoire
      this.finCorrection()
      break // case "correction":

    case 'navigation':
      if (!this.sectionTerminee()) {
        this.etat = 'enonce'
      }
      this.finNavigation()
      break // case "navigation":
  }
}
