import { j3pAddElt, j3pAjouteBouton, j3pArrondi, j3pClone, j3pEmpty, j3pGetRandomInt, j3pPGCD, j3pShuffle } from 'src/legacy/core/functions'
import { verifNombreBienEcrit } from 'src/legacy/outils/fonctions/ecritureNombre'
import { j3pAffiche } from 'src/lib/mathquill/functions'
import ZoneStyleMathquill2 from 'src/legacy/outils/zoneStyleMathquill/ZoneStyleMathquill2'
import { j3pCreeFenetres, j3pAfficheCroixFenetres, j3pToggleFenetres } from 'src/legacy/core/functionsJqDialog'
import { addDefaultTable } from 'src/legacy/themes/table'
import { ecrisBienMathquill } from 'src/legacy/outils/zoneStyleMathquill/functions'
import ListeDeroulante from 'src/legacy/outils/listeDeroulante/ListeDeroulante'
import textesGeneriques from 'src/lib/core/textes'

const { cBien, cFaux, essaieEncore, reponseIncomplete, tempsDepasse } = textesGeneriques

/**
 * Ajouter une description ici
 * @fileOverview
 * @author Tommy Barroy <t.barroy@laposte.net>
 * @since 2017-10
*/
export const params = {
  outils: ['mathquill', 'calculatrice'],
  /**
   * Ce qui est paramétrable dans l’éditeur de graphe
   * @type {LegacySectionParamsParametres}
   */
  parametres: [
    ['nbrepetitions', 3, 'entier', 'Nombre de répétitions de la section'],
    ['nbchances', 2, 'entier', 'Nombre d’essais possibles'],
    ['Justifie', true, 'boolean', '<u>true</u>: L’élève doit d’abord calculer les deux proportions.'],
    ['Calculatrice', true, 'boolean', '<u>true</u>: L’élève dispose d’une calculatrice.'],
    ['theme', 'standard', 'liste', 'Modèle pour l’affichage', ['standard', 'zonesAvecImageDeFond']]
  ]
}

const pb = [
  {
    t: 'Dans la classe de £d, il y $£a$ filles pour $£b$ élèves.',
    min: 16,
    max: 32,
    jus: 'Calcule la proportion de filles dans les classes de £a et £b.',
    nju: 'Proportion de filles dans la classe de £a ',
    tenc: 'Compare la proportion de filles dans chaque classe.',
    fenc: 'La proportion de filles de la classe de £a est ',
    fenc2: ' à celle de la classe de £b.'
  },
  {
    t: 'Dans la classe de £d, il y $£c$ filles et $£a$ garçons.',
    min: 16,
    max: 32,
    jus: 'Calcule la proportion de garçons dans les classes de £a et £b.',
    nju: 'Proportion de garçons dans la classe de £a ',
    tenc: 'Compare la proportion de garçons dans chaque classe.',
    fenc: 'La proportion de garçons de la classe de £a est ',
    fenc2: ' à celle de la classe de £b.'
  },
  {
    t: 'Dans le collège de £d, il y $£a$ cinquièmes pour $£b$ élèves.',
    min: 300,
    max: 500,
    jus: 'Calcule la proportion de cinquièmes dans les collèges de £a et £b.',
    nju: 'Proportion de cinquièmes dans le collège de £a ',
    tenc: 'Compare la proportion de cinquièmes dans chaque collège.',
    fenc: 'La proportion de cinquièmes du collège de £a est ',
    fenc2: ' à celle du collège de £b.'
  },
  {
    t: 'Dans la société de £d, il y $£a$ ouvriers pour $£b$ employés.',
    min: 500,
    max: 900,
    jus: 'Calcule la proportion de d’ouviers dans les sociétés de £a et £b.',
    nju: 'Proportion d’ouviers dans la société de £a ',
    tenc: 'Compare la proportion d’ouvriers dans ces deux sociètés.',
    fenc: 'La proportion d’ouvriers dans la société de £a est ',
    fenc2: ' à celle de la société de £b.'
  },
  {
    t: 'Le marchand de glaces £d a vendu $£b$ glaces, dont $£a$ au chocolat.',
    min: 500,
    max: 900,
    jus: 'Calcule la proportion de glaces au chocolat chez £a et £b.',
    nju: 'Proportion de glaces au chocolat chez £a ',
    tenc: 'Compare la proportion de glaces au chocolat chez ces deux marchands.',
    fenc: 'La proportion de glaces au chocolat chez £a est ',
    fenc2: ' à celle chez £b.'
  }
]

const prenoms = ['Charlie', 'Félix', 'Llyn', 'Mathys', 'Nathéo', 'Daniel', 'Tommy', 'Rémi', 'Nicolas', 'Mohamed', 'Walid', 'Sofian', 'Safa']

/**
 * section comprop
 * @this {Parcours}
 */
export default function main () {
  const me = this
  const stor = me.storage
  const ds = me.donneesSection

  function initSection () {
    let i
    me.validOnEnter = false // ex donneesSection.touche_entree

    // Construction de la page
    me.construitStructurePage('presentation3')

    stor.afaire = ['enc']
    if (ds.Justifie) {
      me.donneesSection.nbetapes = 2
      stor.afaire = ['jus', 'enc']
    }
    stor.encours = stor.afaire[stor.afaire.length - 1]
    me.donneesSection.nbitems = me.donneesSection.nbetapes * me.donneesSection.nbrepetitions

    ds.lesExos = []
    stor.ListPrenGarde = j3pShuffle(prenoms)
    let lp = j3pClone(pb)
    lp = j3pShuffle(lp)
    for (i = 0; i < me.donneesSection.nbitems; i++) {
      ds.lesExos.push({ pb: lp[i % lp.length] })
    }
    ds.lesExos = j3pShuffle(ds.lesExos)

    lp = j3pShuffle(['supérieure', 'inférieure', 'égale'])
    for (i = 0; i < me.donneesSection.nbitems; i++) {
      ds.lesExos[i].cas = lp[i % lp.length]
    }
    ds.lesExos = j3pShuffle(ds.lesExos)

    me.typederreurs = [0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0]

    me.score = 0
    const tt = 'Comparer des proportions'

    me.afficheTitre(tt)

    enonceMain()
  }

  function faisChoixExos () {
    const e = ds.lesExos.pop()
    stor.restric = '0123456789.,'
    stor.tot1 = j3pGetRandomInt(e.pb.min, e.pb.max)
    stor.e1 = j3pGetRandomInt(stor.tot1 / 9, 8 * stor.tot1 / 9)
    const f = stor.e1 / stor.tot1
    e.cas = 'égale'
    switch (e.cas) {
      case '+': {
        stor.tot2 = j3pGetRandomInt(e.pb.min, e.pb.max)
        const limin = Math.ceil((f + 0.002) * stor.tot2)
        let limax = Math.ceil((f + 0.1) * stor.tot2)
        limax = Math.min(stor.tot2, limax)
        stor.e2 = j3pGetRandomInt(limin, limax)
      }
        break
      case '-': {
        stor.tot2 = j3pGetRandomInt(e.pb.min, e.pb.max)
        const limin = Math.floor((f - 0.002) * stor.tot2)
        let limax = Math.floor((f - 0.1) * stor.tot2)
        limax = Math.max(0, limax)
        stor.e2 = j3pGetRandomInt(limax, limin)
      }
        break
      default: {
        const mul = j3pGetRandomInt(2, 4)
        stor.e2 = mul * stor.e1
        stor.tot2 = mul * stor.tot1
      }
    }
    stor.yadeb = false
    stor.pren1 = newPren()
    stor.pren2 = newPren()
    return e
  }

  function newPren () {
    if (stor.ListPrenGarde.length === 0) {
      stor.ListPrenGarde = j3pShuffle(prenoms)
    }
    return stor.ListPrenGarde.pop()
  }

  function poseQuestion () {
    if (!stor.yadeb) {
      stor.yadeb = true
      j3pAffiche(stor.lesdiv.consigne, null, stor.Lexo.pb.t + '\n', {
        b: stor.tot1,
        a: stor.e1,
        c: stor.tot1 - stor.e1,
        d: stor.pren1
      })
      j3pAffiche(stor.lesdiv.consigne, null, stor.Lexo.pb.t, {
        b: stor.tot2,
        a: stor.e2,
        c: stor.tot2 - stor.e2,
        d: stor.pren2
      })
      stor.lesdiv.consigne.classList.add('enonce')
    }
    if (stor.errP1 || stor.errP2) {
      j3pEmpty(stor.lesdiv.travailjus)
      j3pEmpty(stor.lesdiv.explikjus)
    }
    if (stor.encours === 'enc') {
      j3pEmpty(stor.lesdiv.enoncejus)
      j3pAffiche(stor.lesdiv.enonceenc, null, '<b>' + stor.Lexo.pb.tenc + '.</b>')
      const tyu = addDefaultTable(stor.lesdiv.travailenc, 1, 3)
      j3pAffiche(tyu[0][0], null, stor.Lexo.pb.fenc + '&nbsp;', { a: stor.pren1 })
      const lp = ['Choisir', 'supérieure', 'égale', 'inférieure']
      j3pAffiche(tyu[0][2], null, '&nbsp;' + stor.Lexo.pb.fenc2, { b: stor.pren2 })
      stor.llist = ListeDeroulante.create(tyu[0][1], lp, { centre: true })
      stor.lesdiv.travailenc.classList.add('travail')
      stor.lesdiv.enonceenc.classList.add('enonce')
      stor.lesdiv.travailjus.classList.remove('travail')
      stor.lesdiv.enoncejus.classList.remove('enonce')
      stor.lesdiv.solutionjus.classList.add('enonce')
    } else {
      j3pAffiche(stor.lesdiv.enoncejus, null, '<b>' + stor.Lexo.pb.jus + '</b>\n', {
        a: stor.pren1, b: stor.pren2
      })
      j3pAffiche(stor.lesdiv.enoncejus, null, '(<i> Donner le résultat sous forme décimale décimale </i>)')
      const yui = addDefaultTable(stor.lesdiv.travailjus, 2, 1)
      const yui1 = addDefaultTable(yui[0][0], 1, 4)
      const yui2 = addDefaultTable(yui[1][0], 1, 4)
      j3pAffiche(yui1[0][0], null, stor.Lexo.pb.nju, { a: stor.pren1 })
      j3pAffiche(yui2[0][0], null, stor.Lexo.pb.nju, { a: stor.pren2 })
      if (j3pArrondi(stor.e1 / stor.tot1, 3) === j3pArrondi(stor.e1 / stor.tot1, 10)) {
        j3pAffiche(yui1[0][2], null, '&nbsp;=&nbsp;')
        stor.fo1 = false
      } else {
        j3pAffiche(yui1[0][2], null, '$\\approx$')
        stor.fo1 = true
      }
      if (j3pArrondi(stor.e2 / stor.tot2, 3) === j3pArrondi(stor.e2 / stor.tot2, 10)) {
        j3pAffiche(yui2[0][2], null, '&nbsp;=&nbsp;')
        stor.fo2 = false
      } else {
        j3pAffiche(yui2[0][2], null, '$\\approx$')
        stor.fo2 = true
      }
      stor.souv1 = yui1[0][1]
      stor.souv2 = yui2[0][1]
      stor.zoneJus1 = new ZoneStyleMathquill2(yui1[0][3], { id: 'zoneJus1', restric: stor.restric, hasAutoKeyboard: false, limite: 5, enter: me.sectionCourante.bind(me) })
      stor.zoneJus2 = new ZoneStyleMathquill2(yui2[0][3], { id: 'zoneJus2', restric: stor.restric, hasAutoKeyboard: false, limite: 5, enter: me.sectionCourante.bind(me) })
      stor.lesdiv.travailjus.classList.add('travail')
      stor.lesdiv.enoncejus.classList.add('enonce')
    }
    stor.lesdiv.solutionenc.classList.remove('correction')
    stor.lesdiv.solutionjus.classList.remove('correction')
    stor.lesdiv.explikjus.classList.remove('explique')
    stor.lesdiv.explikenc.classList.remove('explique')
  }

  function creeLesDiv () {
    stor.lesdiv = {}
    stor.lesdiv.conteneur = stor.lesdiv.conteneur = j3pAddElt(me.zonesElts.MG, 'div', {
      style: me.styles.etendre('toutpetit.enonce', { padding: '10px' })
    })
    const cont1 = addDefaultTable(stor.lesdiv.conteneur, 1, 3)
    cont1[0][1].style.width = '10px'
    stor.lesdiv.consigne = cont1[0][0]
    const cont2 = addDefaultTable(cont1[0][2], 2, 1)
    const cont = addDefaultTable(stor.lesdiv.conteneur, 8, 1)
    stor.lesdiv.enoncejus = cont[0][0]
    stor.lesdiv.travailjus = cont[1][0]
    stor.lesdiv.explikjus = cont[2][0]
    stor.lesdiv.solutionjus = cont[3][0]
    stor.lesdiv.enonceenc = cont[4][0]
    stor.lesdiv.travailenc = cont[5][0]
    stor.lesdiv.explikenc = cont[6][0]
    stor.lesdiv.solutionenc = cont[7][0]
    stor.lesdiv.calculatrice = cont2[0][0]
    stor.lesdiv.correction = cont2[1][0]
    stor.lesdiv.explikjus.style.color = me.styles.cfaux
    stor.lesdiv.explikenc.style.color = me.styles.cfaux
    stor.lesdiv.solutionjus.style.color = me.styles.toutpetit.correction.color
    stor.lesdiv.solutionenc.style.color = me.styles.toutpetit.correction.color
    me.zonesElts.MG.classList.add('fond')
  }

  function boutonfen () {
    j3pToggleFenetres('Calculatrice')
  }

  function yaReponse () {
    if (stor.encours === 'jus') {
      if (stor.zoneJus1.reponse() === '') {
        stor.zoneJus1.focus()
        return false
      }
      if (stor.zoneJus2.reponse() === '') {
        stor.zoneJus2.focus()
        return false
      }
    }
    if (stor.encours === 'enc') {
      if (!stor.llist.changed) {
        stor.llist.focus()
        return false
      }
    }
    return true
  }

  function isRepOk () {
    let ok, ok2
    stor.errFRac = false
    stor.err = false
    stor.errList = []
    stor.errP1 = false
    stor.errP2 = false

    if (stor.encours === 'jus') {
      ok = true
      ok2 = true
      const r1 = stor.zoneJus1.reponse()
      const r2 = stor.zoneJus2.reponse()

      const j1 = verifNombreBienEcrit(r1.replace(/ /g, ''), false, true)
      if (!j1.good) {
        stor.err = true
        ok = false
        stor.zoneJus1.corrige(false)
        stor.errList.push(j1.remede)
      } else {
        if (Math.abs(j1.nb - stor.e1 / stor.tot1) > 0.001) {
          stor.errP1 = true
          ok = false
          stor.zoneJus1.corrige(false)
        }
      }

      const j2 = verifNombreBienEcrit(r2.replace(/ /g, ''), false, true)
      if (!j2.good) {
        stor.err = true
        ok2 = false
        stor.zoneJus2.corrige(false)
        stor.errList.push(j1.remede)
      } else {
        if (Math.abs(j2.nb - stor.e2 / stor.tot2) > 0.001) {
          stor.errP2 = true
          ok2 = false
          stor.zoneJus2.corrige(false)
        }
      }

      stor.errP1 = !ok
      stor.errP2 = !ok2
      return ok && ok2
    }
    if (stor.encours === 'enc') {
      return stor.llist.reponse === stor.Lexo.cas
    }
  }

  function desactiveAll () {
    if (stor.encours === 'jus') {
      stor.zoneJus1.disable()
      stor.zoneJus2.disable()
    }
    if (stor.encours === 'enc') {
      stor.llist.disable()
    }
  }
  function metsSouv () {
    if (stor.encours === 'jus') {
      if (stor.fo1) j3pAffiche(stor.souv1, null, '&nbsp;= $\\frac{ ' + stor.e1 + ' }{ ' + stor.tot1 + '}$&nbsp;')
      if (stor.fo2) j3pAffiche(stor.souv2, null, '&nbsp;= $\\frac{ ' + stor.e2 + ' }{ ' + stor.tot2 + '}$&nbsp;')
    } else {
      if (stor.Lexo.cas === 'égale' && stor.fo1) {
        j3pAffiche(stor.lesdiv.solutionenc, null, 'En effet, ')
        if (j3pPGCD(stor.e1, stor.tot1) !== 1) {
          j3pAffiche(stor.lesdiv.solutionenc, null, '$\\frac{' + stor.e1 + '}{' + stor.tot1 + '} = \\frac{' + stor.e1 / j3pPGCD(stor.e1, stor.tot1) + '}{' + stor.tot1 / j3pPGCD(stor.e1, stor.tot1) + '}$ et ')
        }
        j3pAffiche(stor.lesdiv.solutionenc, null, '$\\frac{' + stor.e2 + '}{' + stor.tot2 + '} = \\frac{' + stor.e2 / j3pPGCD(stor.e2, stor.tot2) + '}{' + stor.tot2 / j3pPGCD(stor.e2, stor.tot2) + '}$')
      }
    }
  }
  function barrelesfo () {
    if (stor.encours === 'jus') {
      if (stor.zoneJus1.bon === false) stor.lbar1 = stor.zoneJus1.barre()
      if (stor.zoneJus2.bon === false) stor.lbar2 = stor.zoneJus2.barre()
    }
    if (stor.encours === 'enc') {
      stor.llist.barre()
    }
  }

  function mettouvert () {
    if (stor.encours === 'jus') {
      stor.zoneJus1.corrige(true)
      stor.zoneJus2.corrige(true)
    }
    if (stor.encours === 'enc') {
      stor.llist.corrige(true)
    }
  }
  function affCorrection (bool) {
    let yaexplik = false
    let yaco = false

    j3pEmpty(stor.lesdiv.explikjus)
    j3pEmpty(stor.lesdiv.explikenc)
    if (stor.errFRac) {
      yaexplik = true
      j3pAffiche(stor.lesdiv.explikjus, null, 'Pas de virgule pour une fraction !\n')
    }
    if (stor.err) {
      yaexplik = true
      const yaek = []
      for (let i = 0; i < stor.errList.length; i++) {
        if (yaek.indexOf(stor.errList[i]) === -1) {
          yaek.push(stor.errList[i])
          j3pAffiche(stor.lesdiv.explikjus, null, stor.errList[i] + '\n')
        }
      }
    }
    if (stor.encours === 'enc') stor.llist.corrige(false)

    if (bool) {
      barrelesfo()
      desactiveAll()
      yaco = true
      stor.foco = true
      if (stor.encours === 'jus') {
        const ad = stor.Lexo.pb.nju.replace('£a', stor.pren1) + '$\\frac{ ' + stor.e1 + ' }{ ' + stor.tot1 + '}$' + ((j3pArrondi(stor.e1 / stor.tot1, 3) === stor.e1 / stor.tot1) ? ' = $' + ecrisBienMathquill(j3pArrondi(stor.e1 / stor.tot1, 3)) + '$' : '  $\\approx ' + ecrisBienMathquill(j3pArrondi(stor.e1 / stor.tot1, 3)) + '$')
        j3pAffiche(stor.lesdiv.solutionjus, null, ad + '\n')
        const ad2 = stor.Lexo.pb.nju.replace('£a', stor.pren2) + '$\\frac{ ' + stor.e2 + ' }{ ' + stor.tot2 + '}$' + ((j3pArrondi(stor.e2 / stor.tot2, 3) === stor.e2 / stor.tot2) ? ' = $' + ecrisBienMathquill(j3pArrondi(stor.e2 / stor.tot2, 3)) + '$' : '  $\\approx ' + ecrisBienMathquill(j3pArrondi(stor.e2 / stor.tot2, 3)) + '$')
        j3pAffiche(stor.lesdiv.solutionjus, null, ad2 + '\n')
      }
      if (stor.encours === 'enc') {
        if (!ds.Justifie) {
          const ad = stor.Lexo.pb.nju.replace('£a', stor.pren1) + '$\\frac{ ' + stor.e1 + ' }{ ' + stor.tot1 + '}$' + ((j3pArrondi(stor.e1 / stor.tot1, 3) === stor.e1 / stor.tot1) ? ' = $' + ecrisBienMathquill(j3pArrondi(stor.e1 / stor.tot1, 3)) + '$' : '  $\\approx ' + ecrisBienMathquill(j3pArrondi(stor.e1 / stor.tot1, 3)) + '$')
          j3pAffiche(stor.lesdiv.solutionenc, null, ad + '\n')
          const ad2 = stor.Lexo.pb.nju.replace('£a', stor.pren2) + '$\\frac{ ' + stor.e2 + ' }{ ' + stor.tot2 + '}$' + ((j3pArrondi(stor.e2 / stor.tot2, 3) === stor.e2 / stor.tot2) ? ' = $' + ecrisBienMathquill(j3pArrondi(stor.e2 / stor.tot2, 3)) + '$' : '  $\\approx ' + ecrisBienMathquill(j3pArrondi(stor.e2 / stor.tot2, 3)) + '$')
          j3pAffiche(stor.lesdiv.solutionenc, null, ad2 + '\n')
        }
        const ui = stor.Lexo.pb.fenc.replace('£a', stor.pren1) + stor.Lexo.cas + stor.Lexo.pb.fenc2.replace('£b', stor.pren2)
        j3pAffiche(stor.lesdiv.solutionenc, null, ui)
        if (stor.Lexo.cas === 'égale' && stor.fo1) {
          j3pAffiche(stor.lesdiv.solutionenc, null, '\nEn effet, ')
          if (j3pPGCD(stor.e1, stor.tot1) !== 1) {
            j3pAffiche(stor.lesdiv.solutionenc, null, '$\\frac{' + stor.e1 + '}{' + stor.tot1 + '} = \\frac{' + stor.e1 / j3pPGCD(stor.e1, stor.tot1) + '}{' + stor.tot1 / j3pPGCD(stor.e1, stor.tot1) + '}$ et ')
          }
          j3pAffiche(stor.lesdiv.solutionenc, null, '$\\frac{' + stor.e2 + '}{' + stor.tot2 + '} = \\frac{' + stor.e2 / j3pPGCD(stor.e2, stor.tot2) + '}{' + stor.tot2 / j3pPGCD(stor.e2, stor.tot2) + '}$')
        }
      }
    }
    if (yaexplik) {
      if (stor.encours === 'jus') stor.lesdiv.explikjus.classList.add('explique')
      if (stor.encours === 'enc') stor.lesdiv.explikenc.classList.add('explique')
    }
    if (yaco) {
      if (stor.encours === 'jus') stor.lesdiv.solutionjus.classList.add('correction')
      if (stor.encours === 'enc') stor.lesdiv.solutionenc.classList.add('correction')
    }
  }

  function enonceMain () {
    if (me.etapeCourante === 1) {
      // A chaque répétition, on nettoie la scène
      me.videLesZones()
      creeLesDiv()
      stor.Lexo = faisChoixExos()
    }

    let ss = stor.afaire.indexOf(stor.encours) + 1
    if (ss === stor.afaire.length) ss = 0
    stor.encours = stor.afaire[ss]
    poseQuestion()
    j3pEmpty(stor.lesdiv.correction)
    j3pEmpty(stor.lesdiv.calculatrice)
    if (ds.Calculatrice) {
      me.fenetresjq = [{ name: 'Calculatrice', title: 'Une calculatrice', left: 600, top: 10 }]
      j3pCreeFenetres(me)
      j3pAfficheCroixFenetres('Calculatrice')
      j3pAjouteBouton(stor.lesdiv.calculatrice, 'Calculatrice', 'MepBoutons', 'Calculatrice', boutonfen)
    }
    me.finEnonce()
  }

  switch (me.etat) {
    case 'enonce':
      // code exécuté au lancement de la section
      if (me.debutDeLaSection) {
        initSection()
      } else {
        enonceMain()
      }
      break // case "enonce":

    case 'correction':
      // On teste si une réponse a été saisie
      stor.lesdiv.solutionenc.classList.remove('correction')
      stor.lesdiv.solutionjus.classList.remove('correction')
      stor.lesdiv.explikjus.classList.remove('explique')
      stor.lesdiv.explikenc.classList.remove('explique')

      if (yaReponse()) {
        // Bonne réponse
        if (isRepOk()) {
          stor.lesdiv.correction.style.color = me.styles.cbien
          stor.lesdiv.correction.innerHTML = cBien

          desactiveAll()
          mettouvert()

          metsSouv()

          j3pEmpty(stor.lesdiv.explikjus)
          j3pEmpty(stor.lesdiv.explikenc)

          me.score++

          me.typederreurs[0]++
          return this.finCorrection('navigation', true)
        }

        // Pas de bonne réponse
        stor.lesdiv.correction.style.color = me.styles.cfaux
        stor.lesdiv.correction.innerHTML = cFaux
        if (me.isElapsed) {
          // A cause de la limite de temps :

          stor.lesdiv.correction.innerHTML += tempsDepasse + '<BR>'
          me.typederreurs[10]++

          affCorrection(true)

          return this.finCorrection('navigation', true)
        }
        // Réponse fausse :
        if (me.essaiCourant < me.donneesSection.nbchances) {
          stor.lesdiv.correction.innerHTML += '<br>' + essaieEncore

          affCorrection(false)

          me.typederreurs[1]++
          return this.finCorrection()
        }

        // Erreur au dernier essai

        affCorrection(true)

        if (stor.encours === 'p') {
          stor.pose = false
          stor.encours = 't'
        }

        me.typederreurs[2]++
        return this.finCorrection('navigation', true)
      }

      // pas de réponse
      stor.lesdiv.correction.style.color = me.styles.cfaux
      stor.lesdiv.correction.innerHTML = reponseIncomplete
      if (stor.encours === 'm') {
        stor.lesdiv.correction.innerHTML = 'Réponse incomplète ! <br> Tu dois déplacer le point ' + stor.C + ' .'
      }
      me.finCorrection()
      break // case "correction":

    case 'navigation':
      if (!me.sectionTerminee()) {
        me.etat = 'enonce'
      }
      me.finNavigation()
      break // case "navigation":
  }
}
