import { j3pAddElt, j3pAjouteBouton, j3pArrondi, j3pClone, j3pEmpty, j3pGetRandomInt, j3pPGCD, j3pShuffle } from 'src/legacy/core/functions'
import { j3pAffiche } from 'src/lib/mathquill/functions'
import { j3pCreeFenetres, j3pAfficheCroixFenetres, j3pToggleFenetres } from 'src/legacy/core/functionsJqDialog'
import { addDefaultTable } from 'src/legacy/themes/table'
import { afficheFrac } from 'src/legacy/outils/zoneStyleMathquill/functions'
import ZoneStyleMathquill1 from 'src/legacy/outils/zoneStyleMathquill/ZoneStyleMathquill1'
import ListeDeroulante from 'src/legacy/outils/listeDeroulante/ListeDeroulante'
import textesGeneriques from 'src/lib/core/textes'

const { cBien, cFaux, essaieEncore, reponseIncomplete, tempsDepasse } = textesGeneriques

/**
 * Ajouter une description ici
 * @fileOverview
 * @author Tommy Barroy <t.barroy@laposte.net>
 * @since 2017-10
*/
export const params = {
  outils: ['mathquill', 'calculatrice'],
  /**
   * Ce qui est paramétrable dans l’éditeur de graphe
   * @type {LegacySectionParamsParametres}
   */
  parametres: [
    ['nbrepetitions', 3, 'entier', 'Nombre de répétitions de la section'],
    ['nbchances', 2, 'entier', 'Nombre d’essais possibles'],
    ['Justifie', true, 'boolean', '<u>true</u>: L’élève doit d’abord calculer les deux proportions.'],
    ['Calculatrice', true, 'boolean', '<u>true</u>: L’élève dispose d’une calculatrice.'],
    ['Question', 'les deux', 'liste', '<u>partage</u>: L’élève doit partager un total en fonction d’un ratio donné. <br><br><u>quantite</u>: L’élève doit calculer une quantité en fonction d’un ratio et d’une autre quantité donné-e-s.', ['partage', 'quantite', 'les deux']],
    ['theme', 'standard', 'liste', 'Modèle pour l’affichage', ['standard', 'zonesAvecImageDeFond']]
  ]
}

/**
 * section comprop
 * @this {Parcours}
 */
export default function main () {
  const me = this
  const stor = me.storage
  const ds = me.donneesSection
  const pb = [
    // £p , q , q --- les 3 prenoms
    // £a et £b --- a et b
    // £c , £d --- a et b de la situation
    // £s et £t --- 's' pour a et b
    {
      consignePQ: 'Dans la classe de £p, il y a $£a$ fille£s pour $£b$ garçon£t (ratio $£a$ : $£b$)',
      consignePR: 'L’effectif de la classe est de £e élèves.',
      consigneP: 'On veut calculer le nombre de filles et le nombre de garçons.',
      min: 13,
      max: 35,
      cal1: 'Nombre de filles',
      cal2: 'Nombre de garçons',
      tot: 'du nombre d’élèves',
      unit: 'élèves',
      consigneQ: 'On compte $£c$ filles dans la classe.',
      consigneQ2: 'On veut calculer le nombre de garçons.',

      nju: 'Proportion de filles dans la classe de £a ',
      tenc: 'Compare la proportion de filles dans chaque classe.',
      fenc: 'La proportion de filles de la classe de £a est ',
      fenc2: ' à celle de la classe de £b.'
    }
  ]

  function initSection () {
    let i
    me.validOnEnter = false // ex donneesSection.touche_entree

    // Construction de la page
    me.construitStructurePage('presentation3')

    stor.etapes = ['enc']
    if (ds.Justifie) {
      // faut modifier la valeur par défaut
      me.surcharge({ nbetapes: 2 })
      stor.etapes = ['jus', 'enc']
    }
    stor.etapeEncours = stor.etapes[stor.etapes.length - 1]

    ds.lesExos = []
    stor.listPren = ['Charlie', 'Félix', 'Llyn', 'Mathys', 'Nathéo', 'Daniel', 'Tommy', 'Rémi', 'Nicolas', 'Mohamed', 'Walid', 'Sofian', 'Safa']
    stor.ListPrenGarde = []
    let lp = j3pClone(pb)
    lp = j3pShuffle(lp)
    for (i = 0; i < ds.nbitems; i++) {
      ds.lesExos.push({ pb: lp[i % lp.length] })
    }
    ds.lesExos = j3pShuffle(ds.lesExos)

    lp = []
    if (ds.Question === 'les deux' || ds.Question === 'partage') lp.push('partage')
    if (ds.Question === 'les deux' || ds.Question === 'quantite') lp.push('quantite')
    for (i = 0; i < ds.nbitems; i++) {
      ds.lesExos[i].ques = lp[i % lp.length]
    }
    ds.lesExos = j3pShuffle(ds.lesExos)

    me.typederreurs = [0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0]

    me.afficheTitre('Utiliser les ratios')
    enonceMain()
  }

  function faisChoixExos () {
    const e = ds.lesExos.pop()
    stor.a = j3pGetRandomInt(1, 10)
    do {
      stor.b = j3pGetRandomInt(1, 10)
    } while (j3pPGCD(stor.a, stor.b) !== 1 || (stor.a === 1 && stor.b === 1))
    stor.p = newPren()
    stor.p2 = newPren()
    stor.p3 = newPren()
    let compt = 0
    do {
      compt++
      stor.mult = j3pGetRandomInt(2, 99)
    } while (compt < 1000 && ((stor.mult * (stor.a + stor.b) > e.pb.max) || (stor.mult * (stor.a + stor.b) < e.pb.min)))
    stor.tot = j3pArrondi(stor.mult * (stor.a + stor.b), 0)
    stor.na = j3pArrondi(stor.mult * stor.a, 0)
    stor.nb = j3pArrondi(stor.mult * stor.b, 0)
    stor.foco1 = false
    stor.foco2 = false
    stor.foco3 = false
    stor.foco4 = false
    stor.foco5 = false
    return e
  }
  function newPren () {
    if (stor.ListPrenGarde.length === 0) stor.ListPrenGarde = j3pShuffle(stor.listPren)
    return stor.ListPrenGarde.pop()
  }
  function poseQuestion () {
    if (stor.foco1) {
      j3pEmpty(stor.douko[0])
      j3pEmpty(stor.douko[1])
      j3pAffiche(stor.douko[0], null, '$' + stor.a + '$')
      j3pAffiche(stor.douko[1], null, '$' + (stor.a + stor.b) + '$')
      stor.foco1 = false
      stor.douko[0].style.color = me.styles.colorCorrection
      stor.douko[1].style.color = me.styles.colorCorrection
    }
    if (stor.foco2) {
      j3pEmpty(stor.douko[2])
      j3pEmpty(stor.douko[3])
      j3pAffiche(stor.douko[2], null, '$' + stor.b + '$')
      j3pAffiche(stor.douko[3], null, '$' + (stor.a + stor.b) + '$')
      stor.foco2 = false
      stor.douko[2].style.color = me.styles.colorCorrection
      stor.douko[3].style.color = me.styles.colorCorrection
    }
    if (stor.foco3) {
      j3pEmpty(stor.douko)
      j3pAffiche(stor.douko, null, stor.listAt)
      stor.foco3 = false
      stor.douko.style.color = me.styles.colorCorrection
    }
    if (stor.etapeEncours === 'jus') {
      switch (stor.Lexo.ques) {
        case 'partage': {
          const tt = addDefaultTable(stor.lesdiv.travail, 2, 1)
          modif(tt)
          const tt2 = addDefaultTable(tt[0][0], 1, 3)
          j3pAffiche(tt2[0][0], null, stor.Lexo.pb.cal1 + ' =&nbsp;')
          j3pAffiche(tt2[0][2], null, '&nbsp;' + stor.Lexo.pb.tot)
          const fra = afficheFrac(tt2[0][1], false, false)
          stor.zonec1num = new ZoneStyleMathquill1(fra[0], { restric: '0123456789', limite: 3, enter: () => { me.sectionCourante() } })
          stor.zonec1den = new ZoneStyleMathquill1(fra[2], { restric: '0123456789', limite: 3, enter: () => { me.sectionCourante() } })
          const tt3 = addDefaultTable(tt[1][0], 1, 3)
          j3pAffiche(tt3[0][0], null, stor.Lexo.pb.cal2 + ' =&nbsp;')
          j3pAffiche(tt3[0][2], null, '&nbsp;' + stor.Lexo.pb.tot)
          const fra2 = afficheFrac(tt3[0][1], false, false)
          stor.zonec2num = new ZoneStyleMathquill1(fra2[0], { restric: '0123456789', limite: 3, enter: () => { me.sectionCourante() } })
          stor.zonec2den = new ZoneStyleMathquill1(fra2[2], { restric: '0123456789', limite: 3, enter: () => { me.sectionCourante() } })
          stor.douko = [fra[0], fra[2], fra2[0], fra2[2]]
        }
          break
        case 'quantite': {
          const tt = addDefaultTable(stor.lesdiv.travail, 1, 2)
          j3pAffiche(tt[0][0], null, 'On sait que&nbsp;')
          const ll = j3pShuffle([
            '$\\frac{\\text{' + stor.Lexo.pb.cal1 + '}}{' + stor.a + '} = \\frac{\\text{' + stor.Lexo.pb.cal2 + '}}{' + stor.b + '}$',
            '$\\frac{\\text{' + stor.Lexo.pb.cal1 + '}}{' + stor.b + '} = \\frac{\\text{' + stor.Lexo.pb.cal2 + '}}{' + stor.a + '}$',
            '$\\frac{\\text{' + stor.Lexo.pb.cal1 + '}}{' + (stor.a + stor.b) + '} = \\frac{\\text{' + stor.Lexo.pb.cal2 + '}}{' + (stor.na) + '}$',
            '$\\frac{\\text{' + stor.Lexo.pb.cal1 + '}}{' + stor.na + '} = \\frac{\\text{' + stor.Lexo.pb.cal2 + '}}{' + (stor.a + stor.b) + '}$'
          ])
          ll.splice(0, 0, 'Choisir')
          stor.liste = ListeDeroulante.create(tt[0][1], ll)
          stor.douko = tt[0][1]
          stor.listAt = '$\\frac{\\text{' + stor.Lexo.pb.cal1 + '}}{' + stor.a + '} = \\frac{\\text{' + stor.Lexo.pb.cal2 + '}}{' + stor.b + '}$'
          console.error(ll)
        }
          break
      }
    } else {
      switch (stor.Lexo.ques) {
        case 'partage': {
          const tt = addDefaultTable(stor.lesdiv.travail, 2, 1)
          modif(tt)
          const tt2 = addDefaultTable(tt[0][0], 1, 3)
          j3pAffiche(tt2[0][0], null, stor.Lexo.pb.cal1 + ' =&nbsp;')
          j3pAffiche(tt2[0][2], null, '&nbsp;' + stor.Lexo.pb.unit)
          stor.zonec1 = new ZoneStyleMathquill1(tt2[0][1], { restric: '0123456789', limite: 3, enter: () => { me.sectionCourante() } })
          const tt3 = addDefaultTable(tt[1][0], 1, 3)
          j3pAffiche(tt3[0][0], null, stor.Lexo.pb.cal2 + ' =&nbsp;')
          j3pAffiche(tt3[0][2], null, '&nbsp;' + stor.Lexo.pb.unit)
          stor.zonec2 = new ZoneStyleMathquill1(tt3[0][1], { restric: '0123456789', limite: 3, enter: () => { me.sectionCourante() } })
        }
          break
        case 'quantite': {
          const tt = addDefaultTable(stor.lesdiv.travail, 1, 3)
          j3pAffiche(tt[0][0], null, stor.Lexo.pb.cal2 + ' =&nbsp;')
          j3pAffiche(tt[0][2], null, '&nbsp;' + stor.Lexo.pb.unit)
          stor.zonec = new ZoneStyleMathquill1(tt[0][1], { restric: '0123456789', limite: 3, enter: () => { me.sectionCourante() } })
        }
          break
      }
    }
    j3pEmpty(stor.lesdiv.solution)
    j3pEmpty(stor.lesdiv.explications)
    stor.lesdiv.solution.classList.remove('correction')
    stor.lesdiv.explications.classList.remove('explique')
  }
  function modif (tab) {
    for (let i = 0; i < tab.length; i++) {
      for (let j = 0; j < tab[i].length; j++) {
        tab[i][j].style.padding = '0px'
      }
    }
  }
  function poseQuestion0 () {
    switch (stor.Lexo.ques) {
      case 'partage':
        j3pAffiche(stor.lesdiv.consigne, null, stor.Lexo.pb.consignePQ + '<br>',
          { a: stor.a, b: stor.b, p: stor.p, q: stor.Lexo.p2, r: stor.p3, s: (stor.a > 1) ? 's' : '', t: (stor.b > 1) ? 's' : '', e: stor.tot })
        j3pAffiche(stor.lesdiv.consigne, null, stor.Lexo.pb.consignePR + '<br>',
          { a: stor.a, b: stor.b, p: stor.p, q: stor.Lexo.p2, r: stor.p3, s: (stor.a > 1) ? 's' : '', t: (stor.b > 1) ? 's' : '', e: stor.tot })
        j3pAffiche(stor.lesdiv.consigne, null, '<b>' + stor.Lexo.pb.consigneP + '</b>',
          { a: stor.a, b: stor.b, p: stor.p, q: stor.Lexo.p2, r: stor.p3, s: (stor.a > 1) ? 's' : '', t: (stor.b > 1) ? 's' : '', e: stor.tot })
        break
      case 'quantite':
        j3pAffiche(stor.lesdiv.consigne, null, stor.Lexo.pb.consignePQ + '<br>',
          { a: stor.a, b: stor.b, p: stor.p, q: stor.Lexo.p2, r: stor.p3, s: (stor.a > 1) ? 's' : '', t: (stor.b > 1) ? 's' : '', e: stor.tot })
        j3pAffiche(stor.lesdiv.consigne, null, stor.Lexo.pb.consigneQ + '<br>',
          { a: stor.a, b: stor.b, p: stor.p, q: stor.Lexo.p2, r: stor.p3, s: (stor.a > 1) ? 's' : '', t: (stor.b > 1) ? 's' : '', e: stor.tot, c: stor.na })
        j3pAffiche(stor.lesdiv.consigne, null, '<b>' + stor.Lexo.pb.consigneQ2 + '</b>',
          { a: stor.a, b: stor.b, p: stor.p, q: stor.Lexo.p2, r: stor.p3, s: (stor.a > 1) ? 's' : '', t: (stor.b > 1) ? 's' : '', e: stor.tot })
        break
    }
    stor.lesdiv.consigne.classList.add('enonce')
    stor.lesdiv.travail.classList.add('travail')
  }
  function suivEtape () {
    let i = stor.etapes.indexOf(stor.etapeEncours) + 1
    if (i > stor.etapes.length - 1) i = 0
    return stor.etapes[i]
  }
  function creeLesDiv () {
    stor.lesdiv = {}
    stor.lesdiv.conteneur = stor.lesdiv.conteneur = j3pAddElt(me.zonesElts.MG, 'div', {
      style: me.styles.etendre('toutpetit.enonce', { padding: '10px' })
    })
    const cont1 = addDefaultTable(stor.lesdiv.conteneur, 1, 3)
    cont1[0][1].style.width = '10px'
    stor.lesdiv.consigne = cont1[0][0]
    const cont2 = addDefaultTable(cont1[0][2], 2, 1)
    stor.lesdiv.travail = addDefaultTable(stor.lesdiv.conteneur, 1, 1)[0][0]
    stor.lesdiv.explications = j3pAddElt(me.zonesElts.MG, 'div')
    stor.lesdiv.solution = j3pAddElt(me.zonesElts.MG, 'div')
    stor.lesdiv.correction = cont2[1][0]
    stor.lesdiv.calculatrice = cont2[0][0]
    stor.lesdiv.solution.style.color = me.styles.toutpetit.correction.color
    me.zonesElts.MG.classList.add('fond')
  }

  function boutonfen () {
    j3pToggleFenetres('Calculatrice')
  }

  function yaReponse () {
    if (stor.etapeEncours === 'jus') {
      switch (stor.Lexo.ques) {
        case 'partage':
          if (stor.zonec1num.reponse() === '') {
            stor.zonec1num.focus()
            return false
          }
          if (stor.zonec1den.reponse() === '') {
            stor.zonec1den.focus()
            return false
          }
          if (stor.zonec2num.reponse() === '') {
            stor.zonec2num.focus()
            return false
          }
          if (stor.zonec2den.reponse() === '') {
            stor.zonec2den.focus()
            return false
          }
          break
        case 'quantite':
          if (!stor.liste.changed) {
            stor.liste.focus()
            return false
          }
          break
      }
    } else {
      switch (stor.Lexo.ques) {
        case 'partage':
          if (stor.zonec1.reponse() === '') {
            stor.zonec1.focus()
            return false
          }
          if (stor.zonec2.reponse() === '') {
            stor.zonec2.focus()
            return false
          }
          break
        case 'quantite':
          if (stor.zonec.reponse() === '') {
            stor.zonec.focus()
            return false
          }
          break
      }
    }
    return true
  }
  function barrelesfo () {
    if (stor.etapeEncours === 'jus') {
      switch (stor.Lexo.ques) {
        case 'partage':
          if (!stor.zonec1num.isOk()) {
            stor.foco1 = true
            stor.zonec1num.barre()
          }
          if (!stor.zonec1den.isOk()) {
            stor.foco1 = true
            stor.zonec1den.barre()
          }
          if (!stor.zonec2num.isOk()) {
            stor.foco2 = true
            stor.zonec2num.barre()
          }
          if (!stor.zonec2den.isOk()) {
            stor.foco2 = true
            stor.zonec2den.barre()
          }
          break
        case 'quantite':
          stor.liste.barre()
          stor.foco3 = true
          break
      }
    } else {
      switch (stor.Lexo.ques) {
        case 'partage':
          if (!stor.zonec1.isOk()) {
            stor.zonec1.barre()
          }
          if (!stor.zonec2.isOk()) {
            stor.zonec2.barre()
          }
          break
        case 'quantite':
          if (!stor.zonec.isOk()) {
            stor.zonec.barre()
          }
      }
    }
  }
  function mettouvert () {
    if (stor.etapeEncours === 'jus') {
      switch (stor.Lexo.ques) {
        case 'partage':
          stor.zonec1num.corrige(true)
          stor.zonec1den.corrige(true)
          stor.zonec2num.corrige(true)
          stor.zonec2den.corrige(true)
          break
        case 'quantite':
          stor.liste.corrige(true)
          break
      }
    } else {
      switch (stor.Lexo.ques) {
        case 'partage':
          stor.zonec1.corrige(true)
          stor.zonec2.corrige(true)
          break
        case 'quantite':
          stor.zonec.corrige(true)
          break
      }
    }
  }
  function desactiveAll () {
    if (stor.etapeEncours === 'jus') {
      switch (stor.Lexo.ques) {
        case 'partage':
          stor.zonec1num.disable()
          stor.zonec1den.disable()
          stor.zonec2num.disable()
          stor.zonec2den.disable()
          break
        case 'quantite':
          stor.liste.disable()
          break
      }
    } else {
      switch (stor.Lexo.ques) {
        case 'partage':
          stor.zonec1.disable()
          stor.zonec2.disable()
          break
        case 'quantite':
          stor.zonec.disable()
          break
      }
    }
  }

  function isRepOk () {
    let ok = true
    stor.errFRac = false
    stor.err = false
    stor.errList = []
    stor.errP1 = false
    stor.errP2 = false

    if (stor.etapeEncours === 'jus') {
      switch (stor.Lexo.ques) {
        case 'partage': {
          const num1 = Number(stor.zonec1num.reponse())
          const den1 = Number(stor.zonec1den.reponse())
          const num2 = Number(stor.zonec2num.reponse())
          const den2 = Number(stor.zonec2den.reponse())
          if (!fracEgal({ num: num1, den: den1 }, { num: stor.a, den: stor.a + stor.b })) {
            ok = false
            if (num1 !== stor.a) {
              stor.zonec1num.corrige(false)
            }
            if (den1 !== stor.a + stor.b) {
              stor.zonec1den.corrige(false)
            }
          }
          if (!fracEgal({ num: num2, den: den2 }, { num: stor.b, den: stor.a + stor.b })) {
            ok = false
            if (num2 !== stor.b) {
              stor.zonec2num.corrige(false)
            }
            if (den2 !== stor.a + stor.b) {
              stor.zonec2den.corrige(false)
            }
          }
          return ok
        }
        case 'quantite':
          if (stor.listAt !== stor.liste.reponse) {
            stor.liste.corrige(false)
            return false
          }
          return true
      }
    } else {
      switch (stor.Lexo.ques) {
        case 'partage': {
          const rep1 = Number(stor.zonec1.reponse())
          const rep2 = Number(stor.zonec2.reponse())
          if (rep1 !== stor.na) {
            stor.zonec1.corrige(false)
            ok = false
          }
          if (rep2 !== stor.nb) {
            stor.zonec2.corrige(false)
            ok = false
          }
          return ok
        }
        case 'quantite': {
          const rep1 = Number(stor.zonec.reponse())
          if (rep1 !== stor.nb) {
            stor.zonec.corrige(false)
            ok = false
          }
          return ok
        }
      }
    }
  }
  function fracEgal (a, b) {
    return (a.num * b.den === a.den * b.num) && (b.den !== 0)
  }

  function affCorrection (bool) {
    let yaexplik = false
    let yaco = false

    j3pEmpty(stor.lesdiv.explications)
    stor.lesdiv.explications.classList.remove('explique')
    if (stor.errFRac) {
      yaexplik = true
      j3pAffiche(stor.lesdiv.explikjus, null, 'Pas de virgule pour une fraction !\n')
    }
    if (stor.err) {
      yaexplik = true
      const yaek = []
      for (let i = 0; i < stor.errList.length; i++) {
        if (yaek.indexOf(stor.errList[i]) === -1) {
          yaek.push(stor.errList[i])
          j3pAffiche(stor.lesdiv.explikjus, null, stor.errList[i] + '\n')
        }
      }
    }

    if (bool) {
      barrelesfo()
      desactiveAll()
      yaco = true
      stor.foco = true
      if (stor.etapeEncours === 'jus') {
        switch (stor.Lexo.ques) {
          case 'partage':
            j3pAffiche(stor.lesdiv.solution, null, '$' + stor.a + ' + ' + stor.b + ' = ' + (stor.a + stor.b) + '$, donc<br>')
            j3pAffiche(stor.lesdiv.solution, null, stor.Lexo.pb.cal1 + ' = $\\frac{' + stor.a + '}{' + (stor.a + stor.b) + '}$ ' + stor.Lexo.pb.tot + '<br>')
            j3pAffiche(stor.lesdiv.solution, null, stor.Lexo.pb.cal2 + ' = $\\frac{' + stor.b + '}{' + (stor.a + stor.b) + '}$ ' + stor.Lexo.pb.tot)
            break
          case 'quantite':
            j3pAffiche(stor.lesdiv.solution, null, stor.listAt)
            break
        }
      } else {
        switch (stor.Lexo.ques) {
          case 'partage':
            j3pAffiche(stor.lesdiv.solution, null, stor.Lexo.pb.cal1 + ' = $\\frac{' + stor.a + '}{' + (stor.a + stor.b) + '} \\times  ' + stor.tot + ' = ' + stor.na + '$ ' + stor.Lexo.pb.unit + '<br>')
            j3pAffiche(stor.lesdiv.solution, null, stor.Lexo.pb.cal2 + ' = $\\frac{' + stor.b + '}{' + (stor.a + stor.b) + '} \\times  ' + stor.tot + ' = ' + stor.nb + '$ ' + stor.Lexo.pb.unit)
            break
          case 'quantite':
            j3pAffiche(stor.lesdiv.solution, null, '$\\frac{' + stor.na + '}{' + stor.a + '} =  \\frac{\\text{' + stor.Lexo.pb.cal2 + '}}{' + stor.b + '}$.<br>$\\text{' + stor.Lexo.pb.cal2 + '} = \\frac{' + stor.na + '\\times' + stor.b + '}{' + stor.a + '} = ' + stor.nb + '$ ' + stor.Lexo.pb.unit)
            break
        }
      }
    }

    if (yaexplik) {
      stor.lesdiv.explications.classList.add('explique')
    }
    if (yaco) {
      stor.lesdiv.solution.classList.add('correction')
    }
  }

  function enonceMain () {
    stor.etapeEncours = suivEtape()
    if (stor.etapeEncours === stor.etapes[0]) {
      // A chaque répétition, on nettoie la scène
      me.videLesZones()
      creeLesDiv()
      stor.Lexo = faisChoixExos()
      poseQuestion0()
      if (ds.Calculatrice) {
        me.fenetresjq = [{ name: 'Calculatrice', title: 'Une calculatrice', left: 600, top: 10 }]
        j3pCreeFenetres(me)
        j3pAfficheCroixFenetres('Calculatrice')
        j3pAjouteBouton(stor.lesdiv.calculatrice, 'Calculatrice', 'MepBoutons', 'Calculatrice', boutonfen)
      }
    }
    poseQuestion()
    j3pEmpty(stor.lesdiv.correction)
    me.finEnonce()
  }

  switch (me.etat) {
    case 'enonce':
      // code exécuté au lancement de la section
      if (me.debutDeLaSection) {
        initSection()
      } else {
        enonceMain()
      }
      break // case "enonce":

    case 'correction':
      // On teste si une réponse a été saisie
      stor.lesdiv.solution.classList.remove('correction')
      stor.lesdiv.correction.classList.remove('explique')

      if (!yaReponse()) {
        // pas de réponse
        stor.lesdiv.correction.style.color = me.styles.cfaux
        stor.lesdiv.correction.innerHTML = reponseIncomplete
        if (stor.encours === 'm') {
          stor.lesdiv.correction.innerHTML = 'Réponse incomplète ! <br> Tu dois déplacer le point ' + stor.C + ' .'
        }
        return this.finCorrection()
      }

      // Bonne réponse
      if (isRepOk()) {
        stor.lesdiv.correction.style.color = me.styles.cbien
        stor.lesdiv.correction.innerHTML = cBien

        mettouvert()
        desactiveAll()

        me.score++

        me.typederreurs[0]++
        return this.finCorrection('navigation', true)
      }

      // Pas de bonne réponse
      stor.lesdiv.correction.style.color = me.styles.cfaux
      stor.lesdiv.correction.innerHTML = cFaux
      if (me.isElapsed) {
        // A cause de la limite de temps :
        stor.lesdiv.correction.innerHTML += tempsDepasse + '<BR>'
        me.typederreurs[10]++
        affCorrection(true)
        return this.finCorrection('navigation', true)
      }

      // Réponse fausse :
      if (me.essaiCourant < ds.nbchances) {
        stor.lesdiv.correction.innerHTML += '<br>' + essaieEncore
        affCorrection(false)
        me.typederreurs[1]++
        return this.finCorrection()
      }

      // Erreur au dernier essai
      affCorrection(true)
      if (stor.encours === 'p') {
        stor.pose = false
        stor.encours = 't'
      }
      me.typederreurs[2]++
      return this.finCorrection('navigation', true)

    case 'navigation':
      if (!me.sectionTerminee()) {
        me.etat = 'enonce'
      }
      me.finNavigation()
      break // case "navigation":
  }
}
