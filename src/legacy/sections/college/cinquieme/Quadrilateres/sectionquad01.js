import loadJqueryDialog from 'src/lib/core/loadJqueryDialog'
import { j3pAddElt, j3pAjouteBouton, j3pArrondi, j3pClone, j3pEmpty, j3pGetNewId, j3pGetRandomBool, j3pGetRandomInt, j3pGetRandomLetters, j3pImage, j3pShowError, j3pShuffle, j3pSpan } from 'src/legacy/core/functions'
import { j3pCreeSVG } from 'src/legacy/core/functionsSvg'
import { colorKo } from 'src/legacy/core/StylesJ3p'
import Paint from 'src/legacy/outils/brouillon/Paint'
import BulleAide from 'src/legacy/outils/bulleAide/BulleAide'
import ListeDeroulante from 'src/legacy/outils/listeDeroulante/ListeDeroulante'
import { j3pAffiche } from 'src/lib/mathquill/functions'
import { getMtgCore } from 'src/lib/outils/mathgraph'
import Dialog from 'src/lib/widgets/dialog/Dialog'
import quadPng from '../../sixieme/Elementgeo/images/quadrilateres.jpg'
import { addDefaultTable } from 'src/legacy/themes/table'
import textesGeneriques from 'src/lib/core/textes'

const { cBien, cFaux, essaieEncore, regardeCorrection, tempsDepasse } = textesGeneriques

/**
 * Ajouter une description ici
 * @fileOverview
 * @author Tommy Barroy <t.barroy@laposte.net>
 * @since 2017-10
*/
export const params = {
  outils: ['calculatrice', 'mathquill'],
  /**
   * Ce qui est paramétrable dans l’éditeur de graphe
   * @type {LegacySectionParamsParametres}
   */
  parametres: [
    ['nbrepetitions', 9, 'entier', 'Nombre de répétitions de la section'],
    ['indication', '', 'string', 'Indication pour chaque répétition'],
    ['limite', 0, 'entier', 'Temps disponible par répétition, en secondes'],
    ['nbchances', 2, 'entier', 'Nombre d’essais par répétition'],
    ['TypeEnonce', 'Les deux', 'liste', '<u>Texte</u>: L’énoncé est un texte. <BR><BR> <u>Figure</u>: L’énoncé est une figure. <BR><BR> <u>Les deux</u>: L’un ou l’autre aléatoirement. <BR><BR> <u>En même temps</u>: Les deux en même temps.', ['Texte', 'Figure', 'Les deux', 'En même temps']],
    ['Context', 'oui', 'liste', '<u>oui</u>: les données sont énoncées dans le contexte du quadrilatère <BR><BR> <u>non</u>: Les données sont énoncées de façon générale. <BR><BR> <u>les deux</u>: Soit l’un, soit l’autre.', ['oui', 'non', 'les deux']],
    ['Ordre', false, 'boolean', '<u>true</u>: Les données doivent être énoncées dans l’ordre du chemin de démonstration. <BR><BR> <u>false</u>: L’ordre des données n’a pas d’importance.'],
    ['Aidefiche', true, 'boolean', '<u>true</u>: La fiche quadrilatères est visible. <BR><BR> <u>false</u>: elle n’est pas visible.'],
    ['ObligeUtile', true, 'boolean', '<u>true</u>: Les données inutiles sont comptées fausses. <BR><BR> <u>false</u>: elles sont ignorées.'],
    ['LimiteNombreDonnees', 3, 'entier', 'Nombre de maximum de données (entre 1 et 3). '],
    ['ObligeDem', true, 'boolean', '<u>true</u>: Les données permettent toujours de passer à un quadrilatère plus particulier. <BR><BR> <u>false</u>: pas forcément.'],
    ['OKquadrilatere', true, 'boolean', "<u>true</u>: le quadrilatère de départ peut n'être qu’un quadrilatère. <BR><BR> <u>false</u>: Ce cas ne se présentera pas."],
    ['OKparallelogramme', true, 'boolean', '<u>true</u>: le quadrilatère de départ peut être un parallélogramme. <BR><BR> <u>false</u>: Ce cas ne se présentera pas.'],
    ['OKrectangle', true, 'boolean', '<u>true</u>: le quadrilatère de départ peut être un rectangle. <BR><BR> <u>false</u>: Ce cas ne se présentera pas.'],
    ['OKlosange', true, 'boolean', '<u>true</u>: le quadrilatère de départ peut être un losange. <BR><BR> <u>false</u>: Ce cas ne se présentera pas.'],
    ['QuadBaseDejaComplete', false, 'boolean', '<u>true</u>: le type de quadrilatère en début de démonstration est déjà complété. <BR><BR> <u>false</u>: Il ne l’est pas.'],
    ['DonneesDejaCompletees', false, 'boolean', '<u>true</u>: la partie Données de la démonstration est déjà complétée. <BR><BR> <u>false</u>: Elle ne l’est pas.'],
    ['ConclusionDejaCompletee', false, 'boolean', '<u>true</u>: la partie Conclusion de la démonstration est déjà complétée. <BR><BR> <u>false</u>: Elle ne l’est pas.'],
    ['PartieCompletee1', false, 'boolean', '<u>true</u>: Une des trois partie au hasard, en plus, est déjà complétée.'],
    ['PartiesCompletees2', false, 'boolean', '<u>true</u>: Deux des trois parties, au hasard, sont déjà complétées.'],
    ['Brouillon', false, 'boolean', '<u>true</u>: L’élève dispose d’un brouillon quand l’énoncé est un texte.'],
    ['Presentation', true, 'boolean', "<u>true</u> Les indications de réussite et les boutons 'ok' et 'suivant' sont dans un cadre séparé à droite. <BR><BR> <u>false</u>: Un seul cadre pour tout"],
    // le modèle gère la compatibilité ascendante avec l’ancien paramètre MiseEnPagePlusClaire
    ['theme', 'standard', 'liste', 'Modèle pour l’affichage', ['standard', 'zonesAvecImageDeFond']]

  ],
  // dans le cas d’une section QUANTITATIVE
  pe: [
    { pe_1: 'Maitrise' },
    { pe_2: 'Suffisant' },
    { pe_3: 'Suffisant - Erreur décontextualise' },
    { pe_4: 'Suffisant - Erreur lecture des données du schéma' },
    { pe_5: 'Suffisant - Erreur conclusion' },
    { pe_6: 'Suffisant - Erreur quadrilatère de départ' },
    { pe_7: 'Suffisant - Erreur données inutiles' },
    { pe_8: 'Insuffisant' },
    { pe_9: 'Insuffisant - Erreur décontextualise' },
    { pe_10: 'Insuffisant - Erreur lecture des données du schéma' },
    { pe_11: 'Insuffisant - Erreur conclusion' },
    { pe_12: 'Insuffisant - Erreur quadrilatère de départ' },
    { pe_13: 'Insuffisant - Erreur données inutiles' },
    { pe_14: 'Insuffisant' },
    { pe_15: 'Insuffisant - Erreur décontextualise' },
    { pe_16: 'Insuffisant - Erreur lecture des données du schéma' },
    { pe_17: 'Insuffisant - Erreur conclusion' },
    { pe_18: 'Insuffisant - Erreur quadrilatère de départ' },
    { pe_19: 'Insuffisant - Erreur données inutiles' }
  ]
}

/**
 * Les pe sous forme d’objet { id: valeur }
 */
const pes = {}
for (const peObj of params.pe) {
  for (const [peId, pe] of Object.entries(peObj)) pes[peId] = pe
}

const textes = {
  consigne: 'Compléte la démonstration pour déterminer la nature du quadrilatère.',
  quadrilateres: ['quadrilatère', 'parallélogramme', 'rectangle', 'losange', 'carré'],
  reponseIncomplete: 'Réponse incomplète !',
  estUn: ' est un ',
  casEnonces: [
    'trois angles droits',
    'quatre côtés de même longueur',
    'un centre de symétrie',
    'des côtés opposés parallèles deux à deux',
    'des diagonales qui se coupent en leur milieu',
    'un angle droit',
    'deux côtés consécutifs de même longueur',
    'des diagonales de même longueur',
    'des diagonales perpendiculaires'
  ],
  a: 'a ',
  nArien: 'n’a pas de données utiles',
  estCentreDu: ' est le centre de symétrie du ',
  pourCentre: ' est son centre de symétrie',
  milieuSegments: ' est le milieu des segments ',
  et: ' et ',
  precise: 'ses diagonales se coupent en £a ,',
  telque: ' tel que: ',
  le: 'Le ',
  ajouter: 'Ajouter',
  choisir: 'Choisir',
  donc: 'donc £a est un ',
  point: ' .',
  supprimer: 'Supprimer',
  aidefiche: 'Clique ici pour avoir une aide',
  erquadbase: 'Il n’est pas écrit dans l’énoncé que £b est un £a !',
  erpasdedem: 'Ta démonstration doit contenir au moins un argument !',
  errienpasseule: 'L’argument "n’a pas de données utiles" s’utilise seul !',
  erinutilisee: 'Des données de l’énoncé n’ont pas été utilisées ! ',
  erinventee: 'Tu utilises des données qui ne sont pas dans l’énoncé !',
  erinvent2: 'cette donnée ne figure pas dans l’énoncé !',
  erquadfin: 'Les données choisies ne permettent pas de démontrer que £a est un £b !',
  erinutile: 'Tu utilises des données inutiles !',
  erinut2: 'cette donnée est inutile !',
  er2fois: 'Il est inutile d’utiliser deux fois la même donnée !',
  coquadbase: 'Le £b £a',
  codonc: 'donc £a est un £b .',
  cotitre: 'Correction:',
  ila: ' il a ',
  brouillontitre: 'Tu peux utiliser ce brouillon.',
  brouillonbout: 'Utiliser un brouillon',
  conseil: 'Il t’est conseillé d’effectuer une figure\nà main levée sur un brouillon\navant de répondre à la question. '
}

export function upgradeParametres (parametres) {
  // Context était un booléen avant de devenir une liste
  if (typeof parametres.Context === 'boolean') parametres.Context = parametres.Context ? 'oui' : 'non'
  // y’a eu des graphes avec ça…
  if (parametres.Context === 'Les deux') parametres.Context = 'les deux'
}

/**
 * section quad01
 * @this {Parcours}
 */
export default function main () {
  const me = this
  const ds = me.donneesSection
  const stor = this.storage
  let svgId = stor.svgId

  function aidefich () {
    if (!stor.dialog) return console.error(Error('Pas de dialog disponible pour l’afficher'))
    stor.dialog.toggle()
  }
  function fig (lettre) {
    const txtFigure = 'TWF0aEdyYXBoSmF2YTEuMAAAABM+TMzNAAJmcv###wEA#wEAAAAAAAAAAANLAAACfwAAAQEAAAAAAAAAAQAAAKr#####AAAAAQAKQ0NhbGNDb25zdAD#####AAJwaQAWMy4xNDE1OTI2NTM1ODk3OTMyMzg0Nv####8AAAABAApDQ29uc3RhbnRlQAkh+1RELRj#####AAAAAQAKQ1BvaW50QmFzZQD#####AQAAAAAOAAFVAMAkAAAAAAAAQBAAAAAAAAAAAAUAAEAv#nbItDlYQC#+dsi0OVj#####AAAAAQAUQ0Ryb2l0ZURpcmVjdGlvbkZpeGUA#####wEAAAAAEAAAAQAAAAEAAAABAT#wAAAAAAAA#####wAAAAEAD0NQb2ludExpZURyb2l0ZQD#####AQAAAAAOAAFWAMAAAAAAAAAAQBAAAAAAAAAAAAUAAUA##nbItDlYAAAAAv####8AAAABAAhDU2VnbWVudAD#####AQAAAAAQAAABAAAAAQAAAAEAAAAD#####wAAAAEAB0NNaWxpZXUA#####wEAAAAAEAAAAAAAAAAAAAAAQAgAAAAAAAAAAAUAAAAAAQAAAAP#####AAAAAgAMQ0NvbW1lbnRhaXJlAP####8BAAAAAAAAAAAAAAAAQBgAAAAAAAAAAAAAAAUMAAAAAAABAAAAAAAAAAEAAAAAAAAAAAABMf####8AAAABAAlDTG9uZ3VldXIA#####wAAAAEAAAADAAAAAgD#####AAAAAAAQAAAAAAAAAAAAAABACAAAAAAAAAAABwAAQFtAAAAAAABAR#Cj1wo9cAAAAAIA#####wAAAAAAEAAAAAAAAAAAAAAAQAgAAAAAAAAAAAcAAEBUQAAAAAAAQGS8KPXCj1wAAAACAP####8AAAAAABAAAAAAAAAAAAAAAEAIAAAAAAAAAAAHAABAcuAAAAAAAEBlPCj1wo9cAAAAAgD#####AAAAAAAQAAAAAAAAAAAAAABACAAAAAAAAAAABwAAQHFwAAAAAABATPCj1wo9cAAAAAUA#####wAAAAAAEAAAAQACQUIAAwAAAAgAAAALAAAABQD#####AAAAAAAQAAABAAJCQwADAAAACwAAAAoAAAAFAP####8AAAAAABAAAAEAAkNEAAMAAAAKAAAACQAAAAUA#####wAAAAAAEAAAAQACREEAAwAAAAkAAAAIAAAABQD#####AAAAAAAQAAABAAJCRAACAAAACQAAAAsAAAAFAP####8AAAAAABAAAAEAAkFDAAIAAAAKAAAACP####8AAAABABBDSW50RHJvaXRlRHJvaXRlAP####8BAAAAABAAAAAAAAAAAAAAAEAIAAAAAAAAAAAHAAAAABAAAAARAAAABgD#####AQAAAAAQAAAAAAAAAAAAAABACAAAAAAAAAAABQAAAAAIAAAAEgAAAAYA#####wEAAAAAEAAAAAAAAAAAAAAAQAgAAAAAAAAAAAUAAAAAEgAAAAoAAAAGAP####8BAAAAABAAAAAAAAAAAAAAAEAIAAAAAAAAAAAFAAAAAAsAAAASAAAABgD#####AQAAAAAQAAAAAAAAAAAAAABACAAAAAAAAAAABQAAAAASAAAACf####8AAAACAAlDQ2VyY2xlT1IA#####wH#AAAABG1kMTEAAgAAABUAAAABP9MzMzMzMzMAAAAACgD#####Af8AAAAEbWQyMQACAAAAFgAAAAE#0zMzMzMzMwAAAAACAP####8BAAD#ABAAAAAAAAAAAAAAAEAIAAAAAAAAAAAFAAFAYSAAAAAAAEBUuFHrhR64AAAAAgD#####AQAA#wAQAAAAAAAAAAAAAABACAAAAAAAAAAABQABQGSgAAAAAABAUDhR64UeuP####8AAAABABFDU3ltZXRyaWVDZW50cmFsZQD#####AAAAE#####8AAAABAAtDUG9pbnRJbWFnZQD#####AQAA#wAQAAAAAAAAAAAAAABACAAAAAAAAAAABQAAAAAaAAAAGwAAAAwA#####wEAAP8AEAAAAAAAAAAAAAAAQAgAAAAAAAAAAAUAAAAAGQAAABsAAAAFAP####8BAAD#ABAAAAEABG1kMTIAAgAAABkAAAAaAAAABQD#####AQAA#wAQAAABAARtZDIyAAIAAAAcAAAAHf####8AAAABAAhDVmVjdGV1cgD#####AQAA#wAQAAABAAAAAgAAABMAAAAUAP####8AAAABAA1DVHJhbnNQYXJWZWN0AP####8AAAAgAAAADAD#####AQAA#wAQAAAAAAAAAAAAAABACAAAAAAAAAAABQAAAAAcAAAAIQAAAAwA#####wEAAP8AEAAAAAAAAAAAAAAAQAgAAAAAAAAAAAUAAAAAHQAAACEAAAAFAP####8BAAD#ABAAAAEABG1kMzIAAgAAACIAAAAjAAAADAD#####AQAA#wAQAAAAAAAAAAAAAABACAAAAAAAAAAABQAAAAAZAAAAIQAAAAwA#####wEAAP8AEAAAAAAAAAAAAAAAQAgAAAAAAAAAAAUAAAAAGgAAACEAAAAFAP####8BAAD#ABAAAAEABG1kNDIAAgAAACUAAAAmAAAACgD#####AQAA#wAAAAIAAAAIAAAAAT#gAAAAAAAAAP####8AAAABABBDSW50RHJvaXRlQ2VyY2xlAP####8AAAAMAAAAKP####8AAAABABBDUG9pbnRMaWVCaXBvaW50AP####8BAAD#ABAAAAAAAAAAAAAAAEAIAAAAAAAAAAAFAAEAAAApAAAAEAD#####AAAA#wAQAAAAAAAAAAAAAABACAAAAAAAAAAABQACAAAAKQAAAA8A#####wAAAA8AAAAoAAAAEAD#####AAAA#wAQAAAAAAAAAAAAAABACAAAAAAAAAAABQABAAAALAAAABAA#####wEAAP8AEAAAAAAAAAAAAAAAQAgAAAAAAAAAAAUAAgAAACz#####AAAAAQAQQ0Ryb2l0ZVBhcmFsbGVsZQD#####AQAA#wAQAAABAAAAAgAAACoAAAAPAAAAEQD#####AQAA#wAQAAABAAAAAgAAAC4AAAAMAAAACQD#####AQAA#wAQAAAAAAAAAAAAAABACAAAAAAAAAAABQAAAAAwAAAALwAAAAUA#####wF#fwAAEAAAAQADYTEwAAIAAAAuAAAAMQAAAAUA#####wF#fwAAEAAAAQADYTIwAAIAAAAxAAAAKgAAAAoA#####wF#fwAAAAACAAAACQAAAAE#4AAAAAAAAAAAAAAPAP####8AAAAPAAAANAAAABAA#####wF#fwAAEAAAAAAAAAAAAAAAQAgAAAAAAAAAAAUAAQAAADUAAAAQAP####8Af38AABAAAAAAAAAAAAAAAEAIAAAAAAAAAAAFAAIAAAA1AAAADwD#####AAAADgAAADQAAAAQAP####8Af38AABAAAAAAAAAAAAAAAEAIAAAAAAAAAAAFAAEAAAA4AAAAEAD#####AX9#AAAQAAAAAAAAAAAAAABACAAAAAAAAAAABQACAAAAOAAAABEA#####wF#fwAAEAAAAQAAAAIAAAA6AAAADwAAABEA#####wF#fwAAEAAAAQAAAAIAAAA2AAAADgAAAAkA#####wF#fwAAEAAAAAAAAAAAAAAAQAgAAAAAAAAAAAUAAAAAOwAAADwAAAAFAP####8Bf38AABAAAAEAA2ExMQACAAAAPQAAADoAAAAFAP####8Bf38AABAAAAEAA2EyMQACAAAAPQAAADYAAAAKAP####8Bf38AAAAAAgAAAAsAAAABP+AAAAAAAAAAAAAADwD#####AAAADAAAAEAAAAAQAP####8Af38AABAAAAAAAAAAAAAAAEAIAAAAAAAAAAAFAAEAAABBAAAAEAD#####AX9#AAAQAAAAAAAAAAAAAABACAAAAAAAAAAABQACAAAAQQAAAA8A#####wAAAA0AAABAAAAAEAD#####AX9#AAAQAAAAAAAAAAAAAABACAAAAAAAAAAABQABAAAARAAAABAA#####wB#fwAAEAAAAAAAAAAAAAAAQAgAAAAAAAAAAAUAAgAAAEQAAAARAP####8Bf38AABAAAAEAAAACAAAAQwAAAA0AAAARAP####8Bf38AABAAAAEAAAACAAAARQAAAAwAAAAJAP####8Bf38AABAAAAAAAAAAAAAAAEAIAAAAAAAAAAAFAAAAAEcAAABIAAAABQD#####AX9#AAAQAAABAANhMTMAAgAAAEkAAABDAAAABQD#####AX9#AAAQAAABAANhMjMAAgAAAEkAAABFAAAACgD#####AX9#AAAAAAIAAAAKAAAAAT#gAAAAAAAAAAAAAA8A#####wAAAA0AAABMAAAAEAD#####AH9#AAAQAAAAAAAAAAAAAABACAAAAAAAAAAABQABAAAATQAAABAA#####wF#fwAAEAAAAAAAAAAAAAAAQAgAAAAAAAAAAAUAAgAAAE0AAAAPAP####8AAAAOAAAATAAAABAA#####wF#fwAAEAAAAAAAAAAAAAAAQAgAAAAAAAAAAAUAAQAAAFAAAAAQAP####8Af38AABAAAAAAAAAAAAAAAEAIAAAAAAAAAAAFAAIAAABQAAAAEQD#####AX9#AAAQAAABAAAAAgAAAFEAAAANAAAAEQD#####AX9#AAAQAAABAAAAAgAAAE8AAAAOAAAACQD#####AX9#AAAQAAAAAAAAAAAAAABACAAAAAAAAAAABQAAAABUAAAAUwAAAAUA#####wF#fwAAEAAAAQADYTEyAAIAAABVAAAATwAAAAUA#####wF#fwAAEAAAAQADYTIyAAIAAABVAAAAUQAAAAoA#####wF#fwAAAAACAAAAEgAAAAE#4AAAAAAAAAAAAAAPAP####8AAAARAAAAWAAAABAA#####wF#fwAAEAAAAAAAAAAAAAAAQAgAAAAAAAAAAAUAAQAAAFkAAAAQAP####8Bf38AABAAAAAAAAAAAAAAAEAIAAAAAAAAAAAFAAIAAABZAAAADwD#####AAAAEAAAAFgAAAAQAP####8Bf38AABAAAAAAAAAAAAAAAEAIAAAAAAAAAAAFAAEAAABcAAAAEAD#####AX9#AAAQAAAAAAAAAAAAAABACAAAAAAAAAAABQACAAAAXAAAAAYA#####wF#AH8AEAAAAAAAAAAAAAAAQAgAAAAAAAAAAAUAAAAACAAAAAsAAAAGAP####8BfwB#ABAAAAAAAAAAAAAAAEAIAAAAAAAAAAAFAAAAAAsAAAAKAAAABgD#####AX8AfwAQAAAAAAAAAAAAAABACAAAAAAAAAAABQAAAAAKAAAACQAAAAYA#####wF#AH8AEAAAAAAAAAAAAAAAQAgAAAAAAAAAAAUAAAAACQAAAAgAAAAKAP####8BfwB#AAAAAgAAAF8AAAABP+AAAAAAAAAAAAAACgD#####AX8AfwAAAAIAAABgAAAAAT#gAAAAAAAAAAAAAAoA#####wF#AH8AAAACAAAAYQAAAAE#4AAAAAAAAAAAAAAKAP####8BfwB#AAAAAgAAAGIAAAABP+AAAAAAAAAAAAAADwD#####AAAADAAAAGMAAAAQAP####8BfwB#ABAAAAAAAAAAAAAAAEAIAAAAAAAAAAAFAAIAAABnAAAAEAD#####AX8AfwAQAAAAAAAAAAAAAABACAAAAAAAAAAABQABAAAAZwAAAA8A#####wAAAA0AAABkAAAAEAD#####AX8AfwAQAAAAAAAAAAAAAABACAAAAAAAAAAABQACAAAAagAAABAA#####wF#AH8AEAAAAAAAAAAAAAAAQAgAAAAAAAAAAAUAAQAAAGoAAAAPAP####8AAAAOAAAAZQAAABAA#####wF#AH8AEAAAAAAAAAAAAAAAQAgAAAAAAAAAAAUAAQAAAG0AAAAQAP####8BfwB#ABAAAAAAAAAAAAAAAEAIAAAAAAAAAAAFAAIAAABtAAAADwD#####AAAADwAAAGYAAAAQAP####8BfwB#ABAAAAAAAAAAAAAAAEAIAAAAAAAAAAAFAAEAAABwAAAAEAD#####AX8AfwAQAAAAAAAAAAAAAABACAAAAAAAAAAABQACAAAAcP####8AAAABAAlDUm90YXRpb24A#####wAAAGEAAAABQEaAAAAAAAAAAAAMAP####8BfwB#ABAAAAAAAAAAAAAAAEAIAAAAAAAAAAAFAAAAAG8AAABzAAAACwD#####AAAAYQAAAAwA#####wF#AH8AEAAAAAAAAAAAAAAAQAgAAAAAAAAAAAUAAAAAdAAAAHUAAAAFAP####8BfwB#ABAAAAEAAmxiAAIAAAB2AAAAdAAAABIA#####wAAAF8AAAABQEaAAAAAAAAAAAAMAP####8BfwB#ABAAAAAAAAAAAAAAAEAIAAAAAAAAAAAFAAAAAGkAAAB4AAAAEgD#####AAAAYAAAAAFAYOAAAAAAAAAAAAwA#####wF#AH8AEAAAAAAAAAAAAAAAQAgAAAAAAAAAAAUAAAAAbAAAAHoAAAASAP####8AAABiAAAAAUBg4AAAAAAAAAAADAD#####AX8AfwAQAAAAAAAAAAAAAABACAAAAAAAAAAABQAAAAByAAAAfAAAAAsA#####wAAAGIAAAAMAP####8BfwB#ABAAAAAAAAAAAAAAAEAIAAAAAAAAAAAFAAAAAH0AAAB+AAAACwD#####AAAAXwAAAAwA#####wF#AH8AEAAAAAAAAAAAAAAAQAgAAAAAAAAAAAUAAAAAeQAAAIAAAAALAP####8AAABgAAAADAD#####AX8AfwAQAAAAAAAAAAAAAABACAAAAAAAAAAABQAAAAB7AAAAggAAAAUA#####wF#AH8AEAAAAQACbGgAAgAAAIEAAAB5AAAABQD#####AX8AfwAQAAABAAJsZwACAAAAfwAAAH0AAAAFAP####8BfwB#ABAAAAEAAmxkAAIAAACDAAAAewAAAAIA#####wF#AH8AEAAAAAAAAAAAAAAAQAgAAAAAAAAAAAUAAUBOAAAAAAAAQFg4UeuFHrgAAAACAP####8BfwB#ABAAAAAAAAAAAAAAAEAIAAAAAAAAAAAFAAFAdAAAAAAAAEBZOFHrhR64AAAAAgD#####AX8AfwAQAAAAAAAAAAAAAABACAAAAAAAAAAABQABQGjgAAAAAABANOFHrhR64v####8AAAABAAlDRHJvaXRlQUIA#####wEAAH8AEAAAAQAAAAEAAAAIAAAACgAAABMA#####wEAAH8AEAAAAQAAAAEAAAAJAAAACwAAAA8A#####wAAAIoAAAAoAAAAEAD#####AQAAfwAQAAAAAAAAAAAAAABACAAAAAAAAAAABQABAAAAjAAAABAA#####wEAAH8AEAAAAAAAAAAAAAAAQAgAAAAAAAAAAAUAAgAAAIwAAAAPAP####8AAACLAAAAQAAAABAA#####wEAAH8AEAAAAAAAAAAAAAAAQAgAAAAAAAAAAAUAAgAAAI8AAAAQAP####8BAAB#ABAAAAAAAAAAAAAAAEAIAAAAAAAAAAAFAAEAAACPAAAADwD#####AAAAigAAAEwAAAAQAP####8BAAB#ABAAAAAAAAAAAAAAAEAIAAAAAAAAAAAFAAIAAACSAAAAEAD#####AQAAfwAQAAAAAAAAAAAAAABACAAAAAAAAAAABQABAAAAkgAAAA8A#####wAAAIsAAAA0AAAAEAD#####AQAAfwAQAAAAAAAAAAAAAABACAAAAAAAAAAABQABAAAAlQAAABAA#####wEAAH8AEAAAAAAAAAAAAAAAQAgAAAAAAAAAAAUAAgAAAJUAAAACAP####8BAAB#ABAAAAAAAAAAAAAAAEAIAAAAAAAAAAAFAAFAacAAAAAAAEBpHCj1wo9c#####wAAAAEADENCaXNzZWN0cmljZQD#####AQAAfwAQAAABAAAAAQAAAAgAAAASAAAACwAAAAcA#####wAAAH8BAAFBAAAAjhAAAAAAAAEAAAABAAAAAQAAAAAAAAAAAAFBAAAABwD#####AAAAfwEAAUIAAACREAAAAAAAAQAAAAEAAAABAAAAAAAAAAAAAUIAAAAHAP####8AAAB#AQABQwAAAJQQAAAAAAABAAAAAQAAAAEAAAAAAAAAAAABQwAAAAcA#####wAAAH8BAAFEAAAAlxAAAAAAAAEAAAABAAAAAQAAAAAAAAAAAAFEAAAABwD#####AX8AfwEABmdhdWNoZQAAAIcSAAAAAAACAAAAAQAAAAEAAAAAAAAAAAACRDEAAAAHAP####8BfwB#AQAGZHJvaXRlAAAAiBIAAAAAAAAAAAABAAAAAQAAAAAAAAAAAAJEMgAAAAcA#####wEAfwABAARoYXV0AAAAiRIAAAAAAAEAAAABAAAAAQAAAAAAAAAAAAJEMwAAAAcA#####wEAAAABAANiYXMAAACYEgAAAAAAAQAAAAEAAAABAAAAAAAAAAAAAkQ0AAAADwD#####AAAAmQAAAFgAAAAQAP####8BAAAAABAAAAAAAAAAAAAAAEAIAAAAAAAAAAAHAAEAAACiAAAAEAD#####AQAAAAAQAAAAAAAAAAAAAABACAAAAAAAAAAABwACAAAAogAAAAcA#####wAAAH8BAAFFAAAApBAAAAAAAAEAAAABAAAAAQAAAAAAAAAAAAFFAAAAEQD#####AQAAfwAQAAABAAAAAQAAAF0AAAARAAAAEQD#####AQAAfwAQAAABAAAAAQAAAFoAAAAQAAAABQD#####AX9#AAAQAAABAANhMTUAAwAAAFoAAACjAAAABQD#####AX9#AAAQAAABAANhMjUAAwAAAKMAAABdAAAAB###########'
    svgId = j3pGetNewId()
    stor.svgId = svgId
    j3pCreeSVG(stor.lesdiv.consfig, { id: svgId, width: 450, height: 250, style: { border: 'solid black 1px' } })
    stor.mtgAppLecteur.removeAllDoc()
    stor.mtgAppLecteur.addDoc(svgId, txtFigure, true)
    stor.mtgAppLecteur.setText(svgId, '#A', lettre[0])
    stor.mtgAppLecteur.setText(svgId, '#D', lettre[1])
    stor.mtgAppLecteur.setText(svgId, '#C', lettre[2])
    stor.mtgAppLecteur.setText(svgId, '#B', lettre[3])
    stor.mtgAppLecteur.setText(svgId, '#E', lettre[4])
    stor.mtgAppLecteur.calculateAndDisplayAll(true)
    stor.mtgAppLecteur.setText(svgId, '#bas', lettre[0] + lettre[1] + lettre[2] + lettre[3] + ' est un ' + stor.quadBase + '.', true)
    for (let i = 0; i < stor.nbdonnees; i++) {
      switch (stor.donneesIndexes[i]) {
        case 0:
          for (let j = 0; j < 4; j++) {
            if (stor.context[i] !== j) {
              stor.mtgAppLecteur.setVisible(svgId, '#a1' + j, true, true)
              stor.mtgAppLecteur.setVisible(svgId, '#a2' + j, true, true)
            }
          }
          break
        case 1:
          stor.mtgAppLecteur.setVisible(svgId, '#lh', true, true)
          stor.mtgAppLecteur.setVisible(svgId, '#lb', true, true)
          stor.mtgAppLecteur.setVisible(svgId, '#ld', true, true)
          stor.mtgAppLecteur.setVisible(svgId, '#lg', true, true)
          break
        case 2:
          stor.mtgAppLecteur.setText(svgId, '#bas', lettre[4] + textes.estCentreDu + ' ' + stor.quadBase + ' ' + lettre[0] + lettre[1] + lettre[2] + lettre[3], true)
          break
        case 3:
          stor.mtgAppLecteur.setColor(svgId, '#AB', 255, 0, 0, true)
          stor.mtgAppLecteur.setColor(svgId, '#CD', 255, 0, 0, true)
          stor.mtgAppLecteur.setColor(svgId, '#BC', 50, 255, 50, true)
          stor.mtgAppLecteur.setColor(svgId, '#DA', 50, 255, 50, true)
          stor.mtgAppLecteur.setText(svgId, '#droite', '(' + lettre[0] + lettre[1] + ')//(' + lettre[3] + lettre[2] + ') \n (' + lettre[2] + lettre[1] + ')//(' + lettre[3] + lettre[0] + ')', true)
          break
        case 4:
          stor.mtgAppLecteur.setVisible(svgId, '#md11', true, true)
          stor.mtgAppLecteur.setVisible(svgId, '#md21', true, true)
          stor.mtgAppLecteur.setVisible(svgId, '#md12', true, true)
          stor.mtgAppLecteur.setVisible(svgId, '#md22', true, true)
          stor.mtgAppLecteur.setVisible(svgId, '#md32', true, true)
          stor.mtgAppLecteur.setVisible(svgId, '#md42', true, true)
          break
        case 5:
          for (let j = 0; j < 4; j++) {
            if (stor.context[i] === j) {
              stor.mtgAppLecteur.setVisible(svgId, '#a1' + j, true, true)
              stor.mtgAppLecteur.setVisible(svgId, '#a2' + j, true, true)
            }
          }
          break
        case 6:

          if ((stor.context[i][0] === 2) || (stor.context[i][0] === 1)) {
            stor.mtgAppLecteur.setText(svgId, '#droite', stor.context[i][1] + ' cm', true)
            stor.mtgAppLecteur.setVisible(svgId, '#ld', true, true)
          } else {
            stor.mtgAppLecteur.setText(svgId, '#gauche', stor.context[i][1] + ' cm', true)
            stor.mtgAppLecteur.setVisible(svgId, '#lg', true, true)
          }
          if ((stor.context[i][0] === 1) || (stor.context[i][0] === 0)) {
            stor.mtgAppLecteur.setVisible(svgId, '#lb', true, true)
          } else {
            stor.mtgAppLecteur.setVisible(svgId, '#lh', true, true)
          }
          break
        case 7:
          stor.mtgAppLecteur.setColor(svgId, '#AC', 0, 0, 255, true)
          stor.mtgAppLecteur.setColor(svgId, '#BD', 0, 0, 255, true)
          stor.mtgAppLecteur.setText(svgId, '#haut', stor.leslettres[0] + stor.leslettres[2] + ' = ' + stor.leslettres[1] + stor.leslettres[3] + ' = ' + stor.context[i] + '  cm', true)
          stor.mtgAppLecteur.setColor(svgId, '#haut', 0, 0, 255, true)
          break
        case 8:
          stor.mtgAppLecteur.setVisible(svgId, '#a15', true, true)
          stor.mtgAppLecteur.setVisible(svgId, '#a25', true, true)
      }
    }
  }
  function ajouteDonnee (num) {
    stor.nbdonneesuti++
    stor.placetrav[num] = true

    j3pEmpty(stor.lesdiv.leszonesTra[num][4])
    j3pEmpty(stor.lesdiv.leszonesTra[num - 1][4])
    const liste = []
    liste.push(textes.choisir)
    for (const [index, label] of stor.choix.entries()) {
      liste[index + 1] = textes.a + label
    }
    liste.push(textes.nArien)
    stor.listes.push(ListeDeroulante.create(stor.lesdiv.leszonesTra[num][0], liste, { centre: true, alignmathquill: true, sensHaut: true }))
    j3pAjouteBouton(stor.lesdiv.leszonesTra[num][3], 'boutons' + num, 'MepBoutons', textes.supprimer, supprimeDonnee.bind(null, num))
    j3pAffiche(stor.lesdiv.leszonesTra[num][0], null, ' ,')
    if (stor.nbdonneesuti < 3) {
      let i = 1
      while (stor.placetrav[i]) {
        i++
        if (i === 4) return j3pShowError(Error('Erreur interne (cas non prévu)'), { mustNotify: true })
      }
      j3pAjouteBouton(stor.lesdiv.leszonesTra[num][4], 'boutona' + i, 'MepBoutons', textes.ajouter, ajouteDonnee.bind(null, i))
    }
  }

  function supprimeDonnee (num) {
    stor.nbdonneesuti--
    stor.placetrav[num] = false
    const rep = []
    let indexDepart = 2
    if (stor.partiesCompletees[0]) indexDepart--
    if (stor.partiesCompletees[2]) indexDepart--

    let cpt = 0
    for (let i = indexDepart; i < stor.listes.length; i++) {
      cpt++
      if (cpt !== num) {
        rep.push([stor.listes[i].getReponseIndex(), stor.listes[i].isOk()])
      }
    }
    for (let i = stor.listes.length - 1; i > (indexDepart - 1); i--) {
      stor.listes[i].disable()
      stor.listes.pop()
    }
    for (let i = 1; i < 4; i++) {
      j3pEmpty(stor.lesdiv.leszonesTra[i][0])
      j3pEmpty(stor.lesdiv.leszonesTra[i][1])
      j3pEmpty(stor.lesdiv.leszonesTra[i][3])
      j3pEmpty(stor.lesdiv.leszonesTra[i][4])
    }
    stor.placetrav = [true, false, false, false]
    const liste = [textes.choisir, ...stor.choix.map(ch => textes.a + ch)]
    liste.push(textes.nArien)
    let finish = false
    for (let i = 1; i < 4; i++) {
      if (rep.length > 0) {
        stor.listes.push(ListeDeroulante.create(stor.lesdiv.leszonesTra[i][0], liste, { centre: true, select: rep[0][0], bon: rep[0][1], alignmathquill: true, sensHaut: true }))
        j3pAjouteBouton(stor.lesdiv.leszonesTra[i][3], 'bouton' + i, 'MepBoutons', textes.supprimer, supprimeDonnee.bind(null, i))
        stor.placetrav[i] = true
        rep.splice(0, 1)
        j3pAffiche(stor.lesdiv.leszonesTra[i][0], null, ' ,')
      } else {
        stor.placetrav[i] = false
        if (!finish) {
          j3pAjouteBouton(stor.lesdiv.leszonesTra[i - 1][4], 'boutona' + i, 'MepBoutons', textes.ajouter, ajouteDonnee.bind(null, i))
          finish = true
        }
      }
    }
  }
  function yareponse () {
    if (me.isElapsed) return true
    for (const liste of stor.listes) {
      if (!liste.changed) {
        liste.focus()
        return false
      }
    }
    return true
  }

  function bonneReponse () {
    stor.rep = stor.listes.map(liste => liste.getReponseIndex() - 1)
    if (stor.partiesCompletees[0]) {
      stor.rep.unshift(stor.quadBaseIndex)
    }
    if (stor.partiesCompletees[1]) {
      if (stor.donaftab.length === 0) {
        stor.rep.push(9)
      } else {
        for (const donaf of stor.donaftab) {
          stor.rep.push(donaf)
        }
      }
    }
    if (stor.partiesCompletees[2]) {
      stor.rep.splice(1, 0, stor.quadtrue)
    }
    if (stor.rep[0] === -1) stor.rep[0] = 0
    if (stor.rep[1] === -1) stor.rep[1] = 0
    const enonces = [...textes.casEnonces, textes.nArien]
    const choix = [...stor.choix, textes.nArien]
    for (let i = 2; i < stor.rep.length; i++) {
      if (stor.rep[i] === -1) stor.rep[i] = 9
      stor.rep[i] = enonces.indexOf(choix[stor.rep[i]])
    }
    const mm = j3pClone(stor.rep)
    // erreur quadrilateres
    stor.erreurquadbase = (stor.rep[0] !== stor.quadBaseIndex)
    if (stor.erreurquadbase) me.typederreurs[6]++
    // aucune donne
    stor.erreuraucunedonne = (stor.rep.length === 2)
    stor.erreurdonnerienpaseule = false
    stor.erreurdonnenonutiliseerien = false
    stor.erreurdonnenonutiliseenbrien = []
    // si ye rep 'rien de particulier'
    let yarien = false
    for (let i = 2; i < stor.rep.length; i++) {
      yarien = (yarien || (stor.rep[i] === 9))
    }
    if (yarien) { // 1. Elle est q’une fois et seule
      if (stor.rep.length > 3) {
        stor.erreurdonnerienpaseule = true
      }
      // 2. Y’en a vraiment pas une bien
      for (let i = 0; i < stor.donneesIndexes.length; i++) {
        if (stor.donneesIndexesutiles[stor.quadBaseIndex].indexOf(stor.donneesIndexes[i]) !== -1) {
          stor.erreurdonnenonutiliseerien = true
          stor.erreurdonnenonutiliseenbrien.push(stor.donneesIndexes[i])
        }
      }
    }

    // si pas ordre mettre dans l’ordre
    // 0.1.2.3.4.5.6.7.8
    stor.plapla = [2, 3, 4]
    if (!ds.Ordre) {
      if (stor.rep.length > 3) {
        const umax = Math.max(stor.rep[2], stor.rep[3])
        const umin = Math.min(stor.rep[2], stor.rep[3])
        if (umax === stor.rep[2]) {
          stor.plapla = [3, 2, 4]
        }
        stor.rep[2] = umin
        stor.rep[3] = umax
      }
      if (stor.rep.length > 4) {
        let umax = Math.max(stor.rep[3], stor.rep[4])
        let umin = Math.min(stor.rep[3], stor.rep[4])
        if (umax === stor.rep[3]) {
          stor.plapla = [stor.plapla[0], stor.plapla[2], stor.plapla[1]]
        }
        stor.rep[3] = umin
        stor.rep[4] = umax
        umax = Math.max(stor.rep[2], stor.rep[3])
        umin = Math.min(stor.rep[2], stor.rep[3])
        if (umax === stor.rep[2]) {
          stor.plapla = [stor.plapla[1], stor.plapla[0], stor.plapla[2]]
        }
        stor.rep[2] = umin
        stor.rep[3] = umax
      }
    }

    // verif donnee inventee
    stor.erreurdonneinventee = false
    stor.erreurdonneinventeeliste = []
    for (let i = 2; i < stor.rep.length; i++) {
      const rep = stor.rep[i]
      let ok = false
      for (let j = 0; j < stor.nbdonnees; j++) {
        ok = (ok || (stor.donneesIndexes[j] === rep))
        ok = (ok || ((rep === 5) && (stor.donneesIndexes[j] === 0)))
        ok = (ok || ((rep === 6) && (stor.donneesIndexes[j] === 1)))
        ok = (ok || (rep === 9))
      }
      if (!ok) {
        stor.erreurdonneinventee = true
        if ((stor.TypeEnonceencours === 'Figure') || (stor.TypeEnonceencours === 'En même temps')) { me.typederreurs[4]++ } else { if (stor.Context) { me.typederreurs[3]++ } }
        stor.erreurdonneinventeeliste.push(rep)
      }
    }

    // verif donne utiles dans la dem presentee
    // verif quad obtenu avec la dem presentee
    let quadbuf = stor.rep[0]
    let quadBaseIndex = stor.quadBaseIndex
    stor.erreurdonneinutile = false
    stor.erreurdonneinutileliste = []
    for (let i = 2; i < stor.rep.length; i++) {
      const rep = stor.rep[i]
      if (stor.erreurdonneinventeeliste.indexOf(rep) === -1) {
        quadBaseIndex = Math.max(stor.demquad[quadBaseIndex][stor.rep[i]], quadBaseIndex)
      }
      if ((stor.demquad[quadbuf][stor.rep[i]] === -1) && (rep !== 9)) {
        stor.erreurdonneinutile = true
        stor.erreurdonneinutileliste.push(stor.plapla[i - 2])
      } else if ((rep !== 9) && (stor.erreurdonneinventeeliste.indexOf(rep) === -1)) {
        quadbuf = stor.demquad[quadbuf][stor.rep[i]]
      }
    }
    stor.erreurquadfin = ((quadbuf !== stor.rep[1]) && (quadBaseIndex !== stor.rep[1]))
    if (stor.erreurquadfin) { me.typederreurs[5]++ }
    let quadtrue = stor.quadBaseIndex
    for (let i = 1; i < 4; i++) {
      for (let j = 0; j < stor.nbdonnees; j++) {
        quadtrue = Math.max(stor.demquad[quadtrue][stor.donneesIndexes[j]], quadtrue)
      }
    }
    stor.erreurquadfin2 = (quadtrue !== stor.rep[1])
    stor.erreurdonneinutile2 = (ds.ObligeUtile && (stor.erreurdonneinutile))

    if (stor.erreurdonneinutile) { me.typederreurs[7]++ }
    // verif donnee pas utilisee
    stor.erreurdonnenonutilise = false
    stor.erreurdonnenonutiliseliste = []
    for (let i = 0; i < stor.nbdonnees; i++) {
      if ((stor.demquad[quadbuf][stor.donneesIndexes[i]] !== -1) || (stor.demquad[quadBaseIndex][stor.donneesIndexes[i]] !== -1)) {
        stor.erreurdonnenonutilise = true
        stor.erreurdonnenonutiliseliste.push(stor.donneesIndexes[i])
      }
    }

    // verif donne utilisée deux fois
    stor.erreur2fois = false
    stor.erreur2foisqui = -1
    for (let i = 2; i < stor.rep.length; i++) {
      for (let j = 2; j < i; j++) {
        if (stor.rep[i] === stor.rep[j]) {
          stor.erreur2fois = true
          stor.erreur2foisqui = stor.rep[i]
        }
      }
    }
    stor.rep = j3pClone(mm)
    return ((!stor.erreurquadbase) && (!stor.erreuraucunedonne) && (!stor.erreurdonnerienpaseule) && (!stor.erreurdonnenonutiliseerien) && (!stor.erreurdonneinventee) && (!stor.erreurquadfin) && (!stor.erreurdonneinutile2) && (!stor.erreurdonnenonutilise) && (!stor.erreur2fois) && (!stor.erreurquadfin2))
  }

  async function initSection () {
    // faut s’assurer que jQuery-ui est chargé
    await loadJqueryDialog()
    // on contrôle certains params
    if (!Number.isInteger(ds.LimiteNombreDonnees) || ds.LimiteNombreDonnees < 1 || ds.LimiteNombreDonnees > 3) {
      console.error(`Paramètre LimiteNombreDonnees invalide (entier entre 1 et 3, ${ds.LimiteNombreDonnees} fourni, => 3`)
      ds.LimiteNombreDonnees = 3
    }
    const structure = ds.Presentation ? 'presentation1' : 'presentation3'
    me.construitStructurePage(structure)

    stor.donneesIndexesutiles = [
      [0, 1, 2, 3, 4],
      [0, 1, 5, 6, 7, 8],
      [1, 6, 8],
      [0, 5, 7],
      []
    ]
    stor.donneesIndexesinutiles = [
      [],
      [2, 3, 4],
      [0, 5, 7, 2, 3, 4],
      [2, 3, 4, 1, 6, 8],
      [0, 1, 2, 3, 4, 5, 6, 7, 8]
    ]
    stor.demquad = [
      [2, 3, 1, 1, 1, -1, -1, -1, -1, -1],
      [2, 3, -1, -1, -1, 2, 3, 2, 3, -1],
      [-1, 4, -1, -1, -1, -1, 4, -1, 4, -1],
      [4, -1, -1, -1, -1, 4, -1, 4, -1, -1],
      [-1, -1, -1, -1, -1, -1, -1, -1, -1, -1]
    ]
    stor.quadPossibles = [ds.OKquadrilatere, ds.OKparallelogramme, ds.OKrectangle, ds.OKlosange]
    // il ne peuvent pas être tous faux
    if (!stor.quadPossibles[0] && !stor.quadPossibles[1] && !stor.quadPossibles[2] && !stor.quadPossibles[3]) {
      console.error('Paramètres invalides, il faut donner au moins un cas de quadrilatère de départ')
      stor.quadPossibles = [true, true, true, true]
    }

    /**
     * Les parties qui doivent déjà être complétées dans l’énoncé (le nb varie de 0 à 2 d’après les params)
     * @type {boolean[]}
     */
    stor.partiesCompletees = [ds.QuadBaseDejaComplete, ds.DonneesDejaCompletees, ds.ConclusionDejaCompletee]
    // ils ne peuvent pas être tous à true (sinon l’exo est déjà résolu)
    let nbComplete = stor.partiesCompletees.reduce((nb, value) => value ? nb + 1 : nb, 0)
    if (nbComplete === 3) {
      console.error('Paramètres invalides, on ne peut pas tout compléter dès le départ')
      stor.partiesCompletees = [false, false, false]
      nbComplete = 0
    }
    // nbComplete peut valoir 0, 1 ou 2
    if (ds.PartiesCompletees2) {
      if (nbComplete !== 2) {
        // on veut un seul false sur les 2 ou 3 existants
        if (nbComplete === 0) {
          stor.partiesCompletees = j3pShuffle([true, true, false])
        } else {
          // on a un seul true => faut passer un des deux false à true
          const indexToChange = j3pGetRandomBool() ? stor.partiesCompletees.indexOf(false) : stor.partiesCompletees.lastIndexOf(false)
          stor.partiesCompletees[indexToChange] = true
        }
      }
    } else if (ds.PartieCompletee1) {
      if (nbComplete === 0) {
        stor.partiesCompletees = j3pShuffle([true, false, false])
      } else if (nbComplete === 2) {
        // on a un seul false => faut passer un des deux true à false
        const indexToChange = j3pGetRandomBool() ? stor.partiesCompletees.indexOf(true) : stor.partiesCompletees.lastIndexOf(true)
        stor.partiesCompletees[indexToChange] = false
      }
    }

    /*
     Par convention,
        me.typederreurs[0] = nombre de bonnes réponses
        me.typederreurs[1] = nombre de fois où l’élève a utilisé une chance supplémentaire
        me.typederreurs[2] = nombre de mauvaises réponses
        me.typederreurs[10] = nombre de fois où l’élève a eu faux à cause d’un temps limite
        LES AUTRES peuvent être utilisées par la détermination de la pe dans le cas d’une section qualitative

        me.typederreurs[3] =  Erreur décontextualise
        me.typederreurs[4] = Erreur lecture des données du schéma
        me.typederreurs[5] = Erreur conclusion
        me.typederreurs[6] = Erreur quadrilatère de départ
        me.typederreurs[7] = Erreur données inutiles

     */
    me.typederreurs = [0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0]
    stor.bulles = []

    me.afficheTitre('Démontrer qu’un quadrilatère est particulier')
    if (ds.indication) me.indication(me.zones.IG, ds.indication)
    // chargement mtg
    getMtgCore({ withMathJax: true })
      .then(
        // success
        (mtgAppLecteur) => {
          stor.mtgAppLecteur = mtgAppLecteur
          stor.listePourCalc = mtgAppLecteur.createList('TWF0aEdyYXBoSmF2YTEuMAAAABI+TMzNAAJmcv###wEA#wEAAAAAAAAAAAQsAAACvQAAAQEAAAAAAAAAAQAAAAP#####AAAAAQAKQ0NhbGNDb25zdAD#####AAJwaQAWMy4xNDE1OTI2NTM1ODk3OTMyMzg0Nv####8AAAABAApDQ29uc3RhbnRlQAkh+1RELRj#####AAAAAQAHQ0NhbGN1bAD#####AAFlAAZleHAoMSn#####AAAAAgAJQ0ZvbmN0aW9uBwAAAAE#8AAAAAAAAAAAAAIA#####wABeAABMAAAAAEAAAAAAAAAAP###############w==')
          enonceMain()
        },
        // failure
        (error) => {
          j3pShowError(error, { message: 'mathgraph n’est pas correctement chargé', mustNotify: true })
        })
      // plantage dans le code de success
      .catch(j3pShowError)
  } // initSection

  function enonceMain () {
    if (stor.dialog) {
      stor.dialog.destroy()
      stor.dialog = null
    }
    for (const bulle of stor.bulles) bulle.disable()
    stor.bulles = []
    if (stor.bullaidetext !== undefined) {
      stor.bullaidetext.disable()
    }
    me.videLesZones()
    stor.TypeEnonceencours = ds.TypeEnonce
    if (stor.TypeEnonceencours === 'Les deux') {
      const i = j3pGetRandomInt(0, 1)
      if (i === 0) { stor.TypeEnonceencours = 'Texte' } else { stor.TypeEnonceencours = 'Figure' }
    }
    /** @type {ListeDeroulante[]} */
    stor.listes = []
    stor.bulles = []
    if ((ds.Context !== 'oui') && (ds.Context !== 'non')) {
      const i = j3pGetRandomInt(0, 1)
      stor.Context = (i === 0)
    } else {
      stor.Context = (ds.Context === 'oui')
    }
    stor.lesdiv = {}
    stor.lesdiv.conteneur = j3pAddElt(me.zonesElts.MG, 'div', {
      style: me.styles.etendre('toutpetit.enonce', { padding: '10px' })
    })
    const tt = addDefaultTable(stor.lesdiv.conteneur, 1, 3)
    stor.lesdiv.consigne = tt[0][0]
    if (!ds.Presentation) {
      stor.lesdiv.aideeFich = tt[0][2]
      tt[0][1].style.width = '20px'
    } else {
      stor.lesdiv.aideeFich = j3pAddElt(me.zonesElts.MD, 'div', {
        style: me.styles.etendre('toutpetit.enonce', { padding: '10px' })
      })
    }
    const tt2 = addDefaultTable(stor.lesdiv.consigne, 2, 1)
    stor.lesdiv.titreZ = tt2[0][0]
    if (stor.TypeEnonceencours === 'Figure') {
      stor.lesdiv.consfig = tt2[1][0]
    } else {
      const tt3 = addDefaultTable(tt2[1][0], 1, 3)
      stor.lesdiv.cons1 = tt3[0][0]
      tt3[0][1].style.width = '10px'
      stor.lesdiv.consfig = tt3[0][2]
    }
    const tt4 = addDefaultTable(stor.lesdiv.titreZ, 1, 3)
    stor.lesdiv.titre = tt4[0][0]
    stor.lesdiv.BrouillTitre = tt4[0][2]
    tt4[0][1].style.width = '10px'

    const travail = addDefaultTable(stor.lesdiv.conteneur, 1, 1)[0][0]
    stor.lesdiv.explications = j3pAddElt(stor.lesdiv.conteneur, 'div')
    stor.lesdiv.explications.style.color = me.styles.cfaux
    stor.leslettres = j3pGetRandomLetters(5)

    let ok
    do {
      stor.quadBaseIndex = j3pGetRandomInt(0, 3)
      ok = stor.quadPossibles[stor.quadBaseIndex]
    } while (!ok)

    stor.quadBase = textes.quadrilateres[stor.quadBaseIndex]
    /** @type {number[]} */
    stor.donneesIndexes = []
    stor.nomquad = stor.leslettres[0] + stor.leslettres[1] + stor.leslettres[2] + stor.leslettres[3]

    let nbdonnesmax = 3
    if (stor.quadBaseIndex > 1) { nbdonnesmax = 2 }
    const donint = [[0, 5], [1, 6], [2], [3], [4], [5, 0], [6, 1], [7], [8]]
    const quadlim = [[], [2, 3, 4], [2, 3, 4, 0, 5, 7], [2, 3, 4, 1, 6, 8]]
    const quadob = [[0, 1, 2, 3, 4], [0, 1, 5, 6, 7, 8], [6, 8], [5, 7]]

    let lim = false
    nbdonnesmax = Math.min(ds.LimiteNombreDonnees, nbdonnesmax)
    stor.nbdonnees = j3pGetRandomInt(1, nbdonnesmax)

    let cpt = 0
    for (let i = 0; i < stor.nbdonnees; i++) {
      let ok
      do {
        stor.donneesIndexes[i] = j3pGetRandomInt(0, (textes.casEnonces.length - 1))
        if ((ds.ObligeDem) && (i === 0)) { stor.donneesIndexes[i] = quadob[stor.quadBaseIndex][j3pGetRandomInt(0, (quadob[stor.quadBaseIndex].length - 1))] }
        ok = true
        for (let j = 0; j < i; j++) {
          for (let u = 0; u < donint[stor.donneesIndexes[i]].length; u++) {
            if (stor.donneesIndexes[j] === donint[stor.donneesIndexes[i]][u]) { ok = false }
          }
        }
        for (let j = 0; j < quadlim[stor.quadBaseIndex].length; j++) {
          if (stor.donneesIndexes[i] === quadlim[stor.quadBaseIndex][j]) {
            if (lim) { ok = false } else { lim = true }
          }
        }
        cpt++
      } while ((!ok) && (cpt !== 100))
    }
    // DonneesContext

    /// contextualise
    stor.context = []
    stor.donntext = []
    for (let i = 0; i < stor.nbdonnees; i++) {
      switch (stor.donneesIndexes[i]) {
        case 0:// 3 angles droits
          stor.context[i] = j3pGetRandomInt(0, 3)
          stor.donntext[i] = textes.ila + textes.casEnonces[0]
          if (stor.Context) {
            stor.donntext[i] = '$ \\widehat{' + stor.leslettres[stor.context[i]] + stor.leslettres[(stor.context[i] + 1) % 4] + stor.leslettres[(stor.context[i] + 2) % 4] + '} = 90° $'
            stor.donntext[i] += ' , $ \\widehat{' + stor.leslettres[(stor.context[i] + 1) % 4] + stor.leslettres[(stor.context[i] + 2) % 4] + stor.leslettres[(stor.context[i] + 3) % 4] + '} = 90° $'
            stor.donntext[i] += ' , $ \\widehat{' + stor.leslettres[(stor.context[i] + 2) % 4] + stor.leslettres[(stor.context[i] + 3) % 4] + stor.leslettres[(stor.context[i] + 4) % 4] + '} = 90° $'
          }
          break
        case 1:// 4 cotes egaux
          stor.context[i] = j3pArrondi(j3pGetRandomInt(22, 98) / 10, 1)
          stor.donntext[i] = textes.ila + textes.casEnonces[1]
          if (stor.Context) {
            stor.donntext[i] = '$ ' + stor.leslettres[0] + stor.leslettres[1] + ' = '
            stor.donntext[i] += stor.leslettres[1] + stor.leslettres[2] + ' = '
            stor.donntext[i] += stor.leslettres[2] + stor.leslettres[3] + ' = '
            stor.donntext[i] += stor.leslettres[3] + stor.leslettres[0] + ' = ' + stor.context[i] + '  $ cm'
          }
          break
        case 2:// centre de symétrie
          stor.context[i] = j3pArrondi(j3pGetRandomInt(22, 98) / 10, 1)
          stor.donntext[i] = textes.ila + textes.casEnonces[2]
          if (stor.Context) {
            stor.donntext[i] = stor.leslettres[4] + textes.pourCentre
          }
          break
        case 3:// cotes opp //
          stor.donntext[i] = textes.ila + textes.casEnonces[3]
          if (stor.Context) {
            stor.donntext[i] = '(' + stor.leslettres[0] + stor.leslettres[1] + ')//(' + stor.leslettres[3] + stor.leslettres[2] + ') et (' + stor.leslettres[2] + stor.leslettres[1] + ')//(' + stor.leslettres[3] + stor.leslettres[0] + ')'
          }
          break
        case 4:// diag coupent milieu
          stor.donntext[i] = textes.ila + textes.casEnonces[4]
          if (stor.Context) {
            stor.donntext[i] = stor.leslettres[4] + textes.milieuSegments + '[' + stor.leslettres[0] + stor.leslettres[2] + ']' + textes.et + '[' + stor.leslettres[1] + stor.leslettres[3] + ']'
          }
          break
        case 5:// un angle droit
          stor.context[i] = j3pGetRandomInt(0, 3)
          stor.donntext[i] = textes.ila + textes.casEnonces[5]
          if (stor.Context) {
            stor.donntext[i] = '$ \\widehat{' + stor.leslettres[(stor.context[i] + 3) % 4] + stor.leslettres[stor.context[i]] + stor.leslettres[(stor.context[i] + 1) % 4] + '} = 90° $'
          }
          break
        case 6:// 2 consec mm longueur
          stor.context[i] = [j3pGetRandomInt(0, 3), j3pArrondi(j3pGetRandomInt(22, 98) / 10, 1)]
          stor.donntext[i] = textes.ila + textes.casEnonces[6]
          if (stor.Context) {
            stor.donntext[i] = '$ ' + stor.leslettres[stor.context[i][0]] + stor.leslettres[(stor.context[i][0] + 1) % 4] + ' = ' + stor.leslettres[(stor.context[i][0] + 1) % 4] + stor.leslettres[(stor.context[i][0] + 2) % 4] + ' = ' + stor.context[i][1] + ' $ cm'
          }
          break
        case 7:// 2 diag mm longueur
          stor.context[i] = j3pArrondi(j3pGetRandomInt(77, 98) / 10, 1)
          stor.donntext[i] = textes.ila + textes.casEnonces[7]
          if (stor.Context) {
            stor.donntext[i] = '$ ' + stor.leslettres[0] + stor.leslettres[2] + ' = ' + stor.leslettres[1] + stor.leslettres[3] + ' = ' + stor.context[i] + ' $ cm'
          }
          break
        case 8:// un angle droit
          stor.context[i] = j3pGetRandomInt(0, 3)
          stor.donntext[i] = textes.ila + textes.casEnonces[8]
          if (stor.Context) {
            stor.donntext[i] = '$ \\widehat{' + stor.leslettres[stor.context[i]] + stor.leslettres[4] + stor.leslettres[(stor.context[i] + 1) % 4] + '} = 90° $'
          }
          break
      }
    }

    j3pAffiche(stor.lesdiv.titre, null, '<b><u>' + textes.consigne + '</u></b>')

    if ((stor.TypeEnonceencours === 'Texte') || (stor.TypeEnonceencours === 'En même temps')) {
      j3pAffiche(stor.lesdiv.cons1, null, stor.nomquad + textes.estUn + stor.quadBase + textes.telque + '\n• ' + textes.precise + '\n',
        { a: stor.leslettres[4] })
      for (let i = 0; i < stor.nbdonnees; i++) {
        j3pAffiche(stor.lesdiv.cons1, null, '•' + stor.donntext[i] + '\n')
      }
      if (stor.TypeEnonceencours === 'Texte') {
        if (!ds.Brouillon) {
          stor.bullaidetext = new BulleAide(stor.lesdiv.consfig, textes.conseil, { place: 5 })
        } else {
          const retab = addDefaultTable(stor.lesdiv.consfig, 1, 2)
          retab[0][0].style.width = '10px'
          stor.lesdiv.bbrouill = retab[0][1]
          stor.mafoncbrouillon = function () {
            j3pEmpty(stor.lesdiv.bbrouill)
            const yy = addDefaultTable(stor.lesdiv.bbrouill, 2, 1)
            j3pAffiche(yy[0][0], null, textes.brouillontitre)
            const mun = Math.max(stor.lesdiv.cons1.offsetHeight, 120)
            Paint.create(yy[1][0], { width: 400, height: mun, textes: stor.leslettres })
          }
          j3pAjouteBouton(stor.lesdiv.bbrouill, 'boutonsbrouill', 'MepBoutons', textes.brouillonbout, stor.mafoncbrouillon)
        }
      }
    }
    if ((stor.TypeEnonceencours === 'Figure') || (stor.TypeEnonceencours === 'En même temps')) {
      fig(stor.leslettres)
    }
    // let bufprez = ''
    // if (!ds.Presentation) { bufprez = '<TD rowspan = 6 width = "20px" ></TD><TD rowspan = 6 nowrap valigne = "middle" id="' + prefixe + 'correction"></TD>' }
    stor.Grotab = addDefaultTable(travail, 1, 5)
    if (ds.Presentation) {
      stor.lesdiv.correction = j3pAddElt(me.zonesElts.MD, 'div', {
        style: me.styles.etendre('toutpetit.enonce', { padding: '10px' })
      })
    } else {
      stor.lesdiv.correction = stor.Grotab[0][4]
      stor.Grotab[0][3].style.width = '10px'
    }
    stor.lesdiv.solution = stor.Grotab[0][2]
    stor.lesdiv.solution.style.color = me.styles.petit.correction.color
    stor.Grotab[0][1].style.width = '10px'
    stor.lesdiv.travail = stor.Grotab[0][0]
    stor.lesdiv.leszonesTra = addDefaultTable(stor.lesdiv.travail, 5, 5)
    stor.lesdiv.leszonesTra[0][2].style.width = '20px'
    stor.lesdiv.leszonesTra[1][2].style.width = '20px'
    stor.lesdiv.leszonesTra[2][2].style.width = '20px'
    stor.lesdiv.leszonesTra[3][2].style.width = '20px'
    stor.lesdiv.leszonesTra[4][2].style.width = '20px'
    stor.lesdiv.travail.style.border = ' solid  1px grey'
    stor.lesdiv.consigne.classList.add('enonce')
    stor.lesdiv.travail.classList.add('travail')
    me.zonesElts.MG.classList.add('fond')

    const listequadbase = []
    const listequadfin = []
    listequadbase.push(textes.choisir)
    listequadfin.push(textes.choisir)
    for (let i = 0; i < textes.quadrilateres.length; i++) {
      listequadbase[i + 1] = textes.le + textes.quadrilateres[i]
      listequadfin[i + 1] = textes.quadrilateres[i]
    }
    if (!stor.partiesCompletees[0]) {
      stor.listes.push(ListeDeroulante.create(stor.lesdiv.leszonesTra[0][0], listequadbase, { centre: true, alignmathquill: true, sensHaut: true }))
    } else {
      j3pAffiche(stor.lesdiv.leszonesTra[0][0], null, textes.le + stor.quadBase)
    }
    j3pAffiche(stor.lesdiv.leszonesTra[0][0], null, ' ' + stor.nomquad + ' ')

    /// determine quadfin et donne importantes
    stor.donaf = []
    stor.donaftab = []
    for (let i = 0; i < stor.nbdonnees; i++) {
      stor.donaf[i] = stor.donneesIndexes[i]
    }
    if (stor.donaf.length > 1) {
      stor.donaf[0] = Math.min(stor.donaf[0], stor.donaf[1])
      stor.donaf[1] = Math.max(stor.donaf[0], stor.donaf[1])
    }
    if (stor.donaf.length > 2) {
      stor.donaf[1] = Math.min(stor.donaf[1], stor.donaf[2])
      stor.donaf[2] = Math.max(stor.donaf[1], stor.donaf[2])
      stor.donaf[0] = Math.min(stor.donaf[0], stor.donaf[1])
      stor.donaf[1] = Math.max(stor.donaf[0], stor.donaf[1])
    }
    stor.quadtrue = stor.quadBaseIndex
    for (let i = 1; i < 4; i++) {
      for (let j = 0; j < stor.nbdonnees; j++) {
        if (stor.quadtrue < stor.demquad[stor.quadtrue][stor.donaf[j]]) {
          stor.donaftab.push(stor.donaf[j])
        }
        stor.quadtrue = Math.max(stor.demquad[stor.quadtrue][stor.donaf[j]], stor.quadtrue)
      }
    }
    /// fin determine

    if (!stor.partiesCompletees[1]) {
      j3pAjouteBouton(stor.lesdiv.leszonesTra[0][4], 'boutona1', 'MepBoutons', textes.ajouter, function () { ajouteDonnee(1) })
    } else {
      if (stor.donaftab.length === 0) {
        j3pAffiche(stor.lesdiv.leszonesTra[1][3], null, textes.nArien + ' ,')
      } else {
        for (let i = 0; i < stor.donaftab.length; i++) {
          j3pAffiche(stor.lesdiv.leszonesTra[i + 1][0], null, textes.a + textes.casEnonces[stor.donaftab[i]])
        }
      }
    }

    j3pAffiche(stor.lesdiv.leszonesTra[4][0], null, textes.donc,
      {
        a: stor.nomquad
      })

    if (!stor.partiesCompletees[2]) {
      stor.listes.push(ListeDeroulante.create(stor.lesdiv.leszonesTra[4][0], listequadfin, { centre: true, alignmathquill: true, sensHaut: true }))
    } else {
      j3pAffiche(stor.lesdiv.leszonesTra[4][0], null, textes.quadrilateres[stor.quadtrue])
    }
    j3pAffiche(stor.lesdiv.leszonesTra[4][0], null, textes.point)
    stor.nbdonneesuti = 0
    stor.placetrav = [true, false, false, false]

    stor.choix = []
    const txtDonnees = [...textes.casEnonces]
    // on prend les textes qui correspondent aux index de donneesIndexes
    for (const donneesIndex of stor.donneesIndexes) {
      stor.choix.push(txtDonnees[donneesIndex])
    }
    // les items non pris mélangés
    const reste = j3pShuffle(txtDonnees.filter(txt => !stor.choix.includes(txt)))
    // que l’on ajoute aux choix pour en avoir au moins 5
    while (stor.choix.length < 5) {
      stor.choix.push(reste.pop())
    }
    stor.choix = j3pShuffle(stor.choix)
    if (!stor.partiesCompletees[1]) {
      ajouteDonnee(1)
    }
    if (ds.Aidefiche) {
      stor.dialog = new Dialog({ title: 'Aide', width: '500px' })
      j3pImage({ conteneur: stor.dialog.container, src: quadPng, larg: 1000 })
      j3pAjouteBouton(stor.lesdiv.aideeFich, 'boutonskl', 'MepBoutons', 'Aide', aidefich)
    }
    me.finEnonce()
  }

  switch (this.etat) {
    case 'enonce':
      // code exécuté au lancement de la section
      if (this.debutDeLaSection) {
        initSection()
      } else {
        enonceMain()
      }
      break // case "enonce":

    case 'correction': {
      if (!yareponse()) {
        stor.lesdiv.correction.style.color = this.styles.cfaux
        stor.lesdiv.correction.innerHTML = textes.reponseIncomplete
        return this.finCorrection()
      }

      // Une réponse a été saisie
      if (bonneReponse()) {
        // Bonne réponse
        this.score += j3pArrondi(1 / ds.nbetapes, 1)
        stor.lesdiv.correction.style.color = this.styles.cbien
        stor.lesdiv.correction.innerHTML = cBien

        stor.lesdiv.explications.innerHTML = ''
        for (const liste of stor.listes) {
          liste.corrige(true)
          liste.disable()
        }
        stor.listes = []
        for (const bulle of stor.bulles) bulle.disable()
        stor.bulles = []
        for (let i = 0; i < 5; i++) {
          j3pEmpty(stor.lesdiv.leszonesTra[i][1])
          j3pEmpty(stor.lesdiv.leszonesTra[i][3])
          j3pEmpty(stor.lesdiv.leszonesTra[i][4])
        }

        this.typederreurs[0]++
        return this.finCorrection('navigation', true)
      }

      // Pas de bonne réponse
      stor.lesdiv.correction.style.color = colorKo

      // A cause de la limite de temps :
      if (me.isElapsed) {
        stor.lesdiv.correction.innerHTML = tempsDepasse
        this.typederreurs[10]++
        for (const bulle of stor.bulles) {
          bulle.disable()
        }
        stor.bulles = []
        if (ds.theme === 'zonesAvecImageDeFond') {
          stor.lesdiv.explications.classList.add('explique')
        }
        stor.lesdiv.explications.innerHTML = ''
        for (let i = 0; i < 5; i++) {
          j3pEmpty(stor.lesdiv.leszonesTra[i][1])
          j3pEmpty(stor.lesdiv.leszonesTra[i][3])
          j3pEmpty(stor.lesdiv.leszonesTra[i][4])
        }

        if (stor.listes[0].changed) {
          if (stor.erreurquadbase) {
            stor.listes[0].corrige(false)
            j3pSpan(stor.lesdiv.leszonesTra[0][1], { id: 'rienrien', style: this.styles.toutpetit.correction })
            const content = textes.erquadbase
              .replace('£a', textes.quadrilateres[stor.rep[0]])
              .replace('£b', stor.nomquad)
            stor.bulles.push(new BulleAide('rienrien', content, { place: 5 }))
          }
        }
        if (stor.erreuraucunedonne) {
          j3pAffiche(stor.lesdiv.explications, null, textes.erpasdedem + '\n')
        }

        if (stor.erreurdonnerienpaseule) {
          let indexDepart = 2
          let indexOffset = 0
          if (stor.partiesCompletees[0]) {
            indexDepart--
            indexOffset++
          }
          if (stor.partiesCompletees[2]) {
            indexDepart--
            indexOffset++
          }
          for (let i = indexDepart; i < stor.listes.length; i++) {
            if (stor.rep[i + indexOffset] === 9) {
              const yy = j3pSpan(stor.lesdiv.leszonesTra[i + indexOffset - 1][1], { style: this.styles.toutpetit.correction })
              stor.bulles[stor.bulles.length] = new BulleAide(yy, textes.errienpasseule, { place: 5 })
              stor.listes[i].corrige(false)
              stor.listes[i].disable(true)
            }
          }
        }
        if (stor.erreurdonnenonutiliseerien || stor.erreurdonnenonutilise) {
          j3pAffiche(stor.lesdiv.explications, null, textes.erinutilisee + '\n')
        }
        if (stor.erreurdonneinventee) {
          j3pAffiche(stor.lesdiv.explications, null, textes.erinventee + '\n')
          let indexDepart = 2
          let indexOffset = 0
          if (stor.partiesCompletees[0]) {
            indexDepart--
            indexOffset++
          }
          if (stor.partiesCompletees[2]) {
            indexDepart--
            indexOffset++
          }
          for (let i = indexDepart; i < stor.listes.length; i++) {
            for (let j = 0; j < stor.erreurdonneinventeeliste.length; j++) {
              if (stor.rep[i + indexOffset] === stor.erreurdonneinventeeliste[j]) {
                stor.listes[i].corrige(false)
                const yy = j3pSpan(stor.lesdiv.leszonesTra[i + indexOffset - 1][1], { style: this.styles.toutpetit.correction })
                stor.bulles.push(new BulleAide(yy, textes.erinvent2, { place: 5 }))
                break
              }
            }
          }
        }
        if (stor.erreurquadfin) {
          j3pAffiche(stor.lesdiv.explications, null, textes.erquadfin + '\n', {
            a: stor.nomquad,
            b: textes.quadrilateres[stor.rep[1]]
          })
        }
        if (stor.erreurquadfin2) {
          let index = 1
          if (stor.partiesCompletees[0]) index--
          stor.listes[index].corrige(false)
        }
        if (stor.erreurdonneinutile2) {
          j3pAffiche(stor.lesdiv.explications, null, textes.erinutile + '\n')
          let indexDepart = 2
          let indexOffset = 0
          if (stor.partiesCompletees[0]) {
            indexDepart--
            indexOffset++
          }
          if (stor.partiesCompletees[2]) {
            indexDepart--
            indexOffset++
          }
          for (let i = indexDepart; i < stor.listes.length; i++) {
            for (let j = 0; j < stor.erreurdonneinutileliste.length; j++) {
              if (stor.rep[i + indexOffset] === stor.erreurdonneinutileliste[j]) {
                stor.listes[i].corrige(false)
                const yy = j3pSpan(stor.lesdiv.leszonesTra[i + indexOffset - 1][1], { style: this.styles.toutpetit.correction })
                stor.bulles[stor.bulles.length] = new BulleAide(yy, textes.erinut2, { place: 5 })
                break
              }
            }
          }
        }

        if (stor.erreur2fois) {
          j3pAffiche(stor.lesdiv.explications, null, textes.er2fois + '\n')
        }
        /// affiche co
        j3pAffiche(stor.lesdiv.solution, null, '<b><u>' + textes.cotitre + '</u></b> \n' + textes.coquadbase + '\n', {
          a: stor.nomquad,
          b: stor.quadBase
        })

        let donnbuf = []
        donnbuf = [stor.donneesIndexes[0], 9, 9]
        if (stor.donneesIndexes.length > 1) {
          donnbuf[1] = Math.max(stor.donneesIndexes[0], stor.donneesIndexes[1])
          donnbuf[0] = Math.min(stor.donneesIndexes[0], stor.donneesIndexes[1])
        }
        if (stor.donneesIndexes.length > 2) {
          donnbuf[2] = stor.donneesIndexes[2]
          let umax = Math.max(donnbuf[2], donnbuf[1])
          let umin = Math.min(donnbuf[2], donnbuf[1])
          donnbuf[1] = umin
          donnbuf[2] = umax
          umax = Math.max(donnbuf[0], donnbuf[1])
          umin = Math.min(donnbuf[0], donnbuf[1])
          donnbuf[0] = umin
          donnbuf[1] = umax
        }

        let quadbuf = stor.quadBaseIndex
        for (let i = 0; i < 3; i++) {
          if (donnbuf[i] === 9) {
            if (i === 0) { j3pAffiche(stor.lesdiv.solution, null, textes.nArien + ' ,\n') }
            break
          }
          if (stor.demquad[quadbuf][donnbuf[i]] > quadbuf) {
            if (quadbuf > 0) {
              if ((donnbuf[i] === 0) || (donnbuf[i] === 0)) { donnbuf[i] = donnbuf[i] + 5 }
            }
            j3pAffiche(stor.lesdiv.solution, null, textes.a + textes.casEnonces[donnbuf[i]] + ' ,\n')
          }
          quadbuf = Math.max(quadbuf, stor.demquad[quadbuf][donnbuf[i]])
        }
        if (ds.theme === 'zonesAvecImageDeFond') {
          stor.lesdiv.solution.classList.add('correction')
        }
        j3pAffiche(stor.lesdiv.solution, null, textes.codonc, {
          a: stor.nomquad,
          b: textes.quadrilateres[quadbuf]
        })

        for (const liste of stor.listes) {
          if (!liste.changed) liste.corrige(false)
          liste.disable(!liste.isOk())
        }
        stor.listes = []

        return this.finCorrection('navigation', true)
      }

      // Réponse fausse sans limite de temps
      stor.lesdiv.correction.innerHTML = cFaux
      if (me.essaiCourant < ds.nbchances) {
        stor.lesdiv.correction.innerHTML += '<br>' + essaieEncore
        stor.lesdiv.explications.classList.add('explique')
        for (const bulle of stor.bulles) bulle.disable()
        stor.bulles = []
        for (let i = 0; i < 5; i++) {
          j3pEmpty(stor.lesdiv.leszonesTra[i][1])
        }
        stor.lesdiv.explications.innerHTML = ''
        for (const liste of stor.listes) {
          liste.corrige(true)
        }
        if (stor.erreurquadbase) {
          stor.listes[0].corrige(false)
          const yy = j3pSpan(stor.lesdiv.leszonesTra[0][1], { style: this.styles.toutpetit.correction })
          const content = textes.erquadbase
            .replace('£a', textes.quadrilateres[stor.rep[0]])
            .replace('£b', stor.nomquad)
          stor.bulles[stor.bulles.length] = new BulleAide(yy, content, { place: 5 })
        }
        if (stor.erreuraucunedonne) {
          j3pAffiche(stor.lesdiv.explications, null, textes.erpasdedem + '\n')
        }
        if (stor.erreurdonnerienpaseule) {
          let indexDepart = 2
          let indexOffset = 0
          if (stor.partiesCompletees[0]) {
            indexDepart--
            indexOffset++
          }
          if (stor.partiesCompletees[2]) {
            indexDepart--
            indexOffset++
          }
          for (let i = indexDepart; i < stor.listes.length; i++) {
            if (stor.rep[i + indexOffset] === 9) {
              const yy = j3pSpan(stor.lesdiv.leszonesTra[i + indexOffset - 1][1], { style: this.styles.toutpetit.correction })
              stor.bulles[stor.bulles.length] = new BulleAide(yy, textes.errienpasseule, { place: 5 })
              stor.listes[i].corrige(false)
            }
          }
        }
        if (stor.erreurdonnenonutiliseerien || stor.erreurdonnenonutilise) {
          j3pAffiche(stor.lesdiv.explications, null, textes.erinutilisee + '\n')
        }
        if (stor.erreurdonneinventee) {
          j3pAffiche(stor.lesdiv.explications, null, textes.erinventee + '\n')
          let indexDepart = 2
          let indexOffset = 0
          if (stor.partiesCompletees[0]) {
            indexDepart--
            indexOffset++
          }
          if (stor.partiesCompletees[2]) {
            indexDepart--
            indexOffset++
          }
          for (let i = indexDepart; i < stor.listes.length; i++) {
            for (let j = 0; j < stor.erreurdonneinventeeliste.length; j++) {
              if (stor.rep[i + indexOffset] === stor.erreurdonneinventeeliste[j]) {
                stor.listes[i].corrige(false)
                const yy = j3pSpan(stor.lesdiv.leszonesTra[i + indexOffset - 1][1], { style: this.styles.toutpetit.correction })
                stor.bulles.push(new BulleAide(yy, textes.erinvent2, { place: 5 }))
                break
              }
            }
          }
        }
        if (stor.erreurquadfin) {
          j3pAffiche(stor.lesdiv.explications, null, textes.erquadfin + '\n', {
            a: stor.nomquad,
            b: textes.quadrilateres[stor.rep[1]]
          })
        }
        if (stor.erreurquadfin2) {
          const indexDepart = stor.partiesCompletees[0] ? 0 : 1
          stor.listes[indexDepart].corrige(false)
        }
        if (stor.erreurdonneinutile2) {
          j3pAffiche(stor.lesdiv.explications, null, textes.erinutile + '\n')
          let indexDepart = 2
          let indexOffset = 0
          if (stor.partiesCompletees[0]) {
            indexDepart--
            indexOffset++
          }
          if (stor.partiesCompletees[2]) {
            indexDepart--
            indexOffset++
          }
          for (let i = indexDepart; i < stor.listes.length; i++) {
            for (let j = 0; j < stor.erreurdonneinutileliste.length; j++) {
              if ((i + indexOffset) === stor.erreurdonneinutileliste[j]) {
                stor.listes[i].corrige(false)
                const yy = j3pSpan(stor.lesdiv.leszonesTra[i + indexOffset - 1][1], { style: this.styles.toutpetit.correction })
                stor.bulles[stor.bulles.length] = new BulleAide(yy, textes.erinut2, { place: 5 })
                break
              }
            }
          }
        }

        if (stor.erreur2fois) {
          j3pAffiche(stor.lesdiv.explications, null, textes.er2fois + '\n')
        }
        this.typederreurs[1]++
        return this.finCorrection()
      }

      // Erreur au dernier essai
      stor.lesdiv.correction.innerHTML += '<br>' + regardeCorrection
      if (ds.theme === 'zonesAvecImageDeFond') {
        stor.lesdiv.explications.style.paddingTop = '5px'
      }
      stor.lesdiv.explications.innerHTML = ''
      for (const liste of stor.listes) liste.corrige(true)
      for (const bulle of stor.bulles) bulle.disable()
      stor.bulles = []
      for (let i = 0; i < 5; i++) {
        j3pEmpty(stor.lesdiv.leszonesTra[i][1])
        j3pEmpty(stor.lesdiv.leszonesTra[i][3])
        j3pEmpty(stor.lesdiv.leszonesTra[i][4])
      }

      if (stor.erreurquadbase) {
        stor.listes[0].corrige(false)
        const yy = j3pSpan(stor.lesdiv.leszonesTra[0][1], { style: this.styles.toutpetit.correction })
        const content = textes.erquadbase
          .replace('£a', textes.quadrilateres[stor.rep[0]])
          .replace('£b', stor.nomquad)
        stor.bulles[stor.bulles.length] = new BulleAide(yy, content, { place: 5 })
      }
      if (stor.erreuraucunedonne) {
        j3pAffiche(stor.lesdiv.explications, null, textes.erpasdedem + '\n')
      }
      if (stor.erreurdonnerienpaseule) {
        let indexDepart = 2
        let indexOffset = 0
        if (stor.partiesCompletees[0]) {
          indexDepart--
          indexOffset++
        }
        if (stor.partiesCompletees[2]) {
          indexDepart--
          indexOffset++
        }
        for (let i = indexDepart; i < stor.listes.length; i++) {
          if (stor.rep[i + indexOffset] === 9) {
            const yy = j3pSpan(stor.lesdiv.leszonesTra[i + indexOffset - 1][1], { style: this.styles.toutpetit.correction })
            stor.bulles[stor.bulles.length] = new BulleAide(yy, textes.errienpasseule, { place: 5 })
            stor.listes[i].corrige(false)
            stor.listes[i].disable(true)
          }
        }
      }
      if (stor.erreurdonnenonutiliseerien || stor.erreurdonnenonutilise) {
        j3pAffiche(stor.lesdiv.explications, null, textes.erinutilisee + '\n')
      }
      if (stor.erreurdonneinventee) {
        j3pAffiche(stor.lesdiv.explications, null, textes.erinventee + '\n')
        let indexDepart = 2
        let indexOffset = 0
        if (stor.partiesCompletees[0]) {
          indexDepart--
          indexOffset++
        }
        if (stor.partiesCompletees[2]) {
          indexDepart--
          indexOffset++
        }
        for (let i = indexDepart; i < stor.listes.length; i++) {
          for (let j = 0; j < stor.erreurdonneinventeeliste.length; j++) {
            if (stor.rep[i + indexOffset] === stor.erreurdonneinventeeliste[j]) {
              stor.listes[i].corrige(false)
              const yy = j3pSpan(stor.lesdiv.leszonesTra[i + indexOffset - 1][1], { style: this.styles.toutpetit.correction })
              stor.bulles[stor.bulles.length] = new BulleAide(yy, textes.erinvent2, { place: 5 })
              break
            }
          }
        }
      }
      if (stor.erreurquadfin) {
        j3pAffiche(stor.lesdiv.explications, null, textes.erquadfin + '\n', {
          a: stor.nomquad,
          b: textes.quadrilateres[stor.rep[1]]
        })
      }
      if (stor.erreurquadfin2) {
        const index = stor.partiesCompletees[0] ? 0 : 1
        stor.listes[index].corrige(false)
      }

      if (stor.erreurdonneinutile2) {
        j3pAffiche(stor.lesdiv.explications, null, textes.erinutile + '\n')
        let indexDepart = 2
        let indexOffet = 0
        if (stor.partiesCompletees[0]) {
          indexDepart--
          indexOffet++
        }
        if (stor.partiesCompletees[2]) {
          indexDepart--
          indexOffet++
        }
        for (let i = indexDepart; i < stor.listes.length; i++) {
          for (let j = 0; j < stor.erreurdonneinutileliste.length; j++) {
            if ((i + indexOffet) === stor.erreurdonneinutileliste[j]) {
              stor.listes[i].corrige(false)
              const yy = j3pSpan(stor.lesdiv.leszonesTra[i + indexOffet - 1][1], { style: this.styles.toutpetit.correction })
              stor.bulles[stor.bulles.length] = new BulleAide(yy, textes.erinut2, { place: 5 })
              break
            }
          }
        }
      }

      if (stor.erreur2fois) {
        j3pAffiche(stor.lesdiv.explications, null, textes.er2fois + '\n')
      }
      /// affiche co
      j3pAffiche(stor.lesdiv.solution, null, '<b><u>' + textes.cotitre + '</u></b> \n' + textes.coquadbase + '\n', {
        a: stor.nomquad,
        b: stor.quadBase
      })

      const donnbuf = [stor.donneesIndexes[0], 9, 9]
      if (stor.donneesIndexes.length > 1) {
        donnbuf[1] = Math.max(stor.donneesIndexes[0], stor.donneesIndexes[1])
        donnbuf[0] = Math.min(stor.donneesIndexes[0], stor.donneesIndexes[1])
      }
      if (stor.donneesIndexes.length > 2) {
        donnbuf[2] = stor.donneesIndexes[2]
        let umax = Math.max(donnbuf[2], donnbuf[1])
        let umin = Math.min(donnbuf[2], donnbuf[1])
        donnbuf[1] = umin
        donnbuf[2] = umax
        umax = Math.max(donnbuf[0], donnbuf[1])
        umin = Math.min(donnbuf[0], donnbuf[1])
        donnbuf[0] = umin
        donnbuf[1] = umax
      }

      let azue = false
      let quadbuf = stor.quadBaseIndex
      for (let i = 0; i < 3; i++) {
        if (donnbuf[i] === 9) {
          if (i === 0) {
            azue = true
            j3pAffiche(stor.lesdiv.solution, null, textes.nArien + ',\n')
          }
          break
        }
        if (stor.demquad[quadbuf][donnbuf[i]] > quadbuf) {
          if (quadbuf > 0) {
            if ((donnbuf[i] === 0) || (donnbuf[i] === 0)) { donnbuf[i] = donnbuf[i] + 5 }
          }
          azue = true
          j3pAffiche(stor.lesdiv.solution, null, textes.a + textes.casEnonces[donnbuf[i]] + ' ,\n')
        }
        quadbuf = Math.max(quadbuf, stor.demquad[quadbuf][donnbuf[i]])
      }

      if (!azue) {
        j3pAffiche(stor.lesdiv.solution, null, textes.nArien + ' ,\n')
      }
      if (ds.theme === 'zonesAvecImageDeFond') {
        stor.lesdiv.solution.classList.add('correction')
      }
      j3pAffiche(stor.lesdiv.solution, null, textes.codonc, {
        a: stor.nomquad,
        b: textes.quadrilateres[quadbuf]
      })

      for (let i = stor.listes.length - 1; i > -1; i--) {
        stor.listes[i].disable({ barreIfKo: true })
        stor.listes.pop()
      }

      this.typederreurs[2]++
      this.finCorrection('navigation', true)
      break // case "correction":
    }

    case 'navigation':
      if (this.sectionTerminee()) {
        // On détermine la phrase d’état renvoyée par la section
        const sco = this.score / ds.nbitems
        let peibase = 0
        if (sco >= 0.8) { peibase = 0 }
        if ((sco >= 0.55) && (sco < 0.8)) { peibase = 1 }
        if ((sco >= 0.3) && (sco < 0.55)) { peibase = 7 }
        if (sco < 0.3) { peibase = 13 }
        let peisup = 0
        let compt = this.typederreurs[2] / 4
        if (this.typederreurs[3] > compt) {
          peisup = 1
          compt = this.typederreurs[3]
        }
        if (this.typederreurs[4] > compt) {
          peisup = 2
          compt = this.typederreurs[4]
        }
        if (this.typederreurs[5] > compt) {
          peisup = 3
          compt = this.typederreurs[5]
        }
        if (this.typederreurs[6] > compt) {
          peisup = 4
          compt = this.typederreurs[6]
        }
        if (this.typederreurs[7] > compt) {
          peisup = 5
          compt = this.typederreurs[7]
        }
        this.parcours.pe = pes['pe_' + (peibase + peisup)]
      } else {
        this.etat = 'enonce'
      }
      this.finNavigation()
      break // case "navigation":
  }
}
