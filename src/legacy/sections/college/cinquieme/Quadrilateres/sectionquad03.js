import { j3pAddElt, j3pArrondi, j3pEmpty, j3pGetNewId, j3pGetRandomBool, j3pGetRandomInt, j3pShowError } from 'src/legacy/core/functions'
import { j3pCreeSVG } from 'src/legacy/core/functionsSvg'
import { j3pAffiche } from 'src/lib/mathquill/functions'
import Zoneclick from 'src/legacy/outils/zoneclick/Zoneclick'
import { addDefaultTable } from 'src/legacy/themes/table'
import { getMtgCore } from 'src/lib/outils/mathgraph'
import textesGeneriques from 'src/lib/core/textes'

const { cBien, cFaux, essaieEncore, regardeCorrection, reponseIncomplete, tempsDepasse } = textesGeneriques

/**
 * Ajouter une description ici
 * @fileOverview
 * @author Tommy Barroy <t.barroy@laposte.net>
 * @since 2017-10
*/

export const params = {
  outils: ['calculatrice', 'mathquill'],
  /**
   * Ce qui est paramétrable dans l’éditeur de graphe
   * @type {LegacySectionParamsParametres}
   */
  parametres: [
    ['nbrepetitions', 9, 'entier', 'Nombre de répétitions de la section'],
    ['indication', '', 'string', 'Indication pour chaque répétition'],
    ['limite', 0, 'entier', 'Temps disponible par répétition, en secondes'],
    ['Context', 'oui', 'liste', '<u>oui</u>: Les données sont énoncées dans le contexte du quadrilatère. <BR><BR> <u>non</u>: Les données sont énoncées de façon générale. <BR><BR> <u>les deux</u>: Soit l’un , soit l’autre', ['oui', 'non', 'les deux']],
    ['LimiteNombreDonnees', 3, 'entier', 'Nombre de maximum de données (entre 1 et 3).'],
    ['Presentation', true, 'boolean', "<u>true</u>: Les indications de réussite et les boutons 'ok' et 'suivant' sont dans un cadre séparé à droite. <BR><BR> <u>false</u>: Un seul cadre pour tout"],
    // le modèle gère la compatibilité ascendante avec l’ancien paramètre MiseEnPagePlusClaire
    ['theme', 'standard', 'liste', 'Modèle pour l’affichage', ['standard', 'zonesAvecImageDeFond']]
  ]
}

const textes = {
  couleurexplique: '#A9E2F3',
  couleurcorrec: '#A9F5A9',
  couleurenonce: '#D8D8D8',
  couleurenonceg: '#F3E2A9',
  couleuretiquettes: '#A9E2F3',
  repch: [['Compte bien le nombre d’angles droits', 'Ce quadrilatère a plus d’un angle droit !', 'Si un quadrilatère a des angles droits, c’est entre deux côtés consécutifs !', 'Il n’y a pas autant d’angles droits !'],
    ['Vérifie le nombre de côtés ayant la même longueur', 'Encore plus de côtés sont concernés !', 'Encore plus de côtés sont concernés !'],
    ['Il est noté que le quadrilatère a un centre de symétrie !'],
    ['Il y a plusieurs indications de côtés parallèles !', 'L’énoncé indique qu’ils sont parallèles !', 'Il faut prendre en compte tous les côtés !'],
    ['Il est indiqué que deux segments ont le même milieu !', 'Des diagonales qui ont le même milieu n’ont pas forcément la même longueur', 'Les données n’indiquent pas d’angle droit !'],
    ['Observe bien les angles entre deux côtés consécutifs', 'Il n’y a pas autant d’angles droits !', 'Il faut vérifier si il s’agit de côtés ou de diagonales', 'Il n’y a pas autant d’angles droits !'],
    ['Observe bien les longueurs des côtés', 'Moins de côtés sont concernés', 'Deux côtés opposés sont l’un en face de l’autre'],
    ['Observe bien les diagonales !', 'Il n’y a pas d’incation concernant le milieu des diagonales', 'Les deux longueurs égales ne concernent pas des côtés !', 'Les deux longueurs égales ne concernent pas des côtés !'],
    ['Observe bien l’angle que forment les diagonales !', 'L’angle droit ne se situe pas entre deux côtés !', 'klm', 'klm'],
    ['Observe bien les côtés opposés !', 'Il n’est pas indiqué qu’ils sont parallèles !', 'Les longueurs égales ne sont pas celles des diagonales !', 'Deux côtés consécutifs se touchent !', 'Moins de côtés sont concernés'],
    ['Il est indiqué que deux côtés sont parallèles !', 'Moins de côtés sont concernés !'],
    ['Observe bien les angles entre deux côtés consécutifs', 'Ce quadrilatère a plus d’un angle droit !', 'Ce quadrilatère a plus d’un angle droit !']],
  Consigne1: 'Sélectionne le bon schéma du quadrilatère.',
  quadbase: 'quadrilatère',
  estun: ' est un ',
  donnes: ['trois angles droits', 'quatre côtés de même longueur', 'deux angles droits', 'des côtés opposés parallèles deux à deux', 'des diagonales qui se coupent en leur milieu', 'un angle droit', 'deux côtés consécutifs de même longueur', 'des diagonales de même longueur', 'des diagonales perpendiculaires', 'deux côtés opposés de même longueur', 'deux côtés opposés paralléles', 'quatre angles droits'],
  a: 'a ',
  estcentredu: ' est le centre de symétrie du ',
  pourcentre: ' est son centre de symétrie',
  milieudiag: ' est le milieu des segments ',
  et: ' et ',
  precise: 'ses diagonales se coupent en £a ,',
  telque: ' tel que: ',
  le: 'Le ',
  cotitre: 'Correction:',
  ila: ' il a ',
  conseil: ' Vérifie les codes. <BR> (deux côtés opposés parallèles ont la même couleur) ',
  dez: 'Le quadrilatère que tu as choisi ne peut pas s’appeler £a !',
  erdez: 'Fais le tour du quadrilatère pour avoir un nom possible',
  enmoins: 'Le quadrilatère que tu as choisi n’a pas £b !'
}

export function upgradeParametres (parametres) {
  // Context était un booléen avant de devenir une liste
  if (typeof parametres.Context === 'boolean') parametres.Context = parametres.Context ? 'oui' : 'non'
}

/**
 * section quad03
 * @this {Parcours}
 */
export default function main () {
  const stor = this.storage
  const me = this
  const ds = me.donneesSection

  function fig (lettry, donny, conty, id) {
    const donnees = []
    for (let i = 0; i < donny.length; i++) donnees[i] = donny[i]
    const lettre = []
    for (let i = 0; i < lettry.length; i++) lettre[i] = lettry[i]
    const context = []
    for (let i = 0; i < conty.length; i++) context[i] = conty[i]
    if (donnees[0] === 12) {
      const u = lettre[2]
      if (j3pGetRandomBool()) {
        lettre[2] = lettre[3]
        lettre[3] = u
      } else {
        lettre[2] = lettre[1]
        lettre[1] = u
      }
      donnees.splice(0, 1)
      context.splice(0, 1)
    }

    const txtFigure = 'TWF0aEdyYXBoSmF2YTEuMAAAABM+TMzNAAJmcv###wEA#wEAAAAAAAAAAANLAAACfwAAAQEAAAAAAAAAAQAAAKr#####AAAAAQAKQ0NhbGNDb25zdAD#####AAJwaQAWMy4xNDE1OTI2NTM1ODk3OTMyMzg0Nv####8AAAABAApDQ29uc3RhbnRlQAkh+1RELRj#####AAAAAQAKQ1BvaW50QmFzZQD#####AQAAAAAOAAFVAMAkAAAAAAAAQBAAAAAAAAAAAAUAAUBPAAAAAAABQGbcKPXCj1z#####AAAAAQAUQ0Ryb2l0ZURpcmVjdGlvbkZpeGUA#####wEAAAAAEAAAAQAAAAEAAAABAT+37T+vm3Pd#####wAAAAEAD0NQb2ludExpZURyb2l0ZQD#####AQAAAAAOAAFWAMAAAAAAAAAAQBAAAAAAAAAAAAUAAUByuUusgYgoAAAAAv####8AAAABAAhDU2VnbWVudAD#####AQAAAAAQAAABAAAAAQAAAAEAAAAD#####wAAAAEAB0NNaWxpZXUA#####wEAAAAAEAAAAAAAAAAAAAAAQAgAAAAAAAAAAAUAAAAAAQAAAAP#####AAAAAgAMQ0NvbW1lbnRhaXJlAP####8BAAAAAAAAAAAAAAAAQBgAAAAAAAAAAAAAAAUMAAAAAAABAAAAAAAAAAEAAAAAAAAAAAABMf####8AAAABAAlDTG9uZ3VldXIA#####wAAAAEAAAADAAAAAgD#####AAAAAAAQAAAAAAAAAAAAAABACAAAAAAAAAAABwAAQEGAAAAAAABAMOFHrhR64AAAAAIA#####wAAAAAAEAAAAAAAAAAAAAAAQAgAAAAAAAAAAAcAAEA7AAAAAAACQFv4UeuFHrgAAAACAP####8AAAAAABAAAAAAAAAAAAAAAEAIAAAAAAAAAAAHAABAbEAAAAAAAEBfeFHrhR64AAAAAgD#####AAAAAAAQAAAAAAAAAAAAAABACAAAAAAAAAAABwAAQGtgAAAAAABANuFHrhR64gAAAAUA#####wAAAAAAEAAAAQACQUIAAwAAAAgAAAALAAAABQD#####AAAAAAAQAAABAAJCQwADAAAACwAAAAoAAAAFAP####8AAAAAABAAAAEAAkNEAAMAAAAKAAAACQAAAAUA#####wAAAAAAEAAAAQACREEAAwAAAAkAAAAIAAAABQD#####AAAAAAAQAAABAAJCRAACAAAACQAAAAsAAAAFAP####8AAAAAABAAAAEAAkFDAAIAAAAKAAAACP####8AAAABABBDSW50RHJvaXRlRHJvaXRlAP####8BAAAAABAAAAAAAAAAAAAAAEAIAAAAAAAAAAAHAAAAABAAAAARAAAABgD#####AQAAAAAQAAAAAAAAAAAAAABACAAAAAAAAAAABQAAAAAIAAAAEgAAAAYA#####wEAAAAAEAAAAAAAAAAAAAAAQAgAAAAAAAAAAAUAAAAAEgAAAAoAAAAGAP####8BAAAAABAAAAAAAAAAAAAAAEAIAAAAAAAAAAAFAAAAAAsAAAASAAAABgD#####AQAAAAAQAAAAAAAAAAAAAABACAAAAAAAAAAABQAAAAASAAAACf####8AAAACAAlDQ2VyY2xlT1IA#####wH#AAAABG1kMTEAAgAAABUAAAABP9MzMzMzMzMAAAAACgD#####Af8AAAAEbWQyMQACAAAAFgAAAAE#0zMzMzMzMwAAAAACAP####8BAAD#ABAAAAAAAAAAAAAAAEAIAAAAAAAAAAAFAAFAVUAAAAAAAEBBcKPXCj1xAAAAAgD#####AQAA#wAQAAAAAAAAAAAAAABACAAAAAAAAAAABQABQFCAAAAAAABARnCj1wo9cf####8AAAABABFDU3ltZXRyaWVDZW50cmFsZQD#####AAAAE#####8AAAABAAtDUG9pbnRJbWFnZQD#####AQAA#wAQAAAAAAAAAAAAAABACAAAAAAAAAAABQAAAAAaAAAAGwAAAAwA#####wEAAP8AEAAAAAAAAAAAAAAAQAgAAAAAAAAAAAUAAAAAGQAAABsAAAAFAP####8BAAD#ABAAAAEABG1kMTIAAgAAABkAAAAaAAAABQD#####AQAA#wAQAAABAARtZDIyAAIAAAAcAAAAHf####8AAAABAAhDVmVjdGV1cgD#####AQAA#wAQAAABAAAAAgAAABMAAAAUAP####8AAAABAA1DVHJhbnNQYXJWZWN0AP####8AAAAgAAAADAD#####AQAA#wAQAAAAAAAAAAAAAABACAAAAAAAAAAABQAAAAAcAAAAIQAAAAwA#####wEAAP8AEAAAAAAAAAAAAAAAQAgAAAAAAAAAAAUAAAAAHQAAACEAAAAFAP####8BAAD#ABAAAAEABG1kMzIAAgAAACIAAAAjAAAADAD#####AQAA#wAQAAAAAAAAAAAAAABACAAAAAAAAAAABQAAAAAZAAAAIQAAAAwA#####wEAAP8AEAAAAAAAAAAAAAAAQAgAAAAAAAAAAAUAAAAAGgAAACEAAAAFAP####8BAAD#ABAAAAEABG1kNDIAAgAAACUAAAAmAAAACgD#####AQAA#wAAAAIAAAAIAAAAAT#gAAAAAAAAAP####8AAAABABBDSW50RHJvaXRlQ2VyY2xlAP####8AAAAMAAAAKP####8AAAABABBDUG9pbnRMaWVCaXBvaW50AP####8BAAD#ABAAAAAAAAAAAAAAAEAIAAAAAAAAAAAFAAEAAAApAAAAEAD#####AAAA#wAQAAAAAAAAAAAAAABACAAAAAAAAAAABQACAAAAKQAAAA8A#####wAAAA8AAAAoAAAAEAD#####AAAA#wAQAAAAAAAAAAAAAABACAAAAAAAAAAABQABAAAALAAAABAA#####wEAAP8AEAAAAAAAAAAAAAAAQAgAAAAAAAAAAAUAAgAAACz#####AAAAAQAQQ0Ryb2l0ZVBhcmFsbGVsZQD#####AQAA#wAQAAABAAAAAgAAACoAAAAPAAAAEQD#####AQAA#wAQAAABAAAAAgAAAC4AAAAMAAAACQD#####AQAA#wAQAAAAAAAAAAAAAABACAAAAAAAAAAABQAAAAAwAAAALwAAAAUA#####wF#fwAAEAAAAQADYTEwAAIAAAAuAAAAMQAAAAUA#####wF#fwAAEAAAAQADYTIwAAIAAAAxAAAAKgAAAAoA#####wF#fwAAAAACAAAACQAAAAE#4AAAAAAAAAAAAAAPAP####8AAAAPAAAANAAAABAA#####wF#fwAAEAAAAAAAAAAAAAAAQAgAAAAAAAAAAAUAAQAAADUAAAAQAP####8Af38AABAAAAAAAAAAAAAAAEAIAAAAAAAAAAAFAAIAAAA1AAAADwD#####AAAADgAAADQAAAAQAP####8Af38AABAAAAAAAAAAAAAAAEAIAAAAAAAAAAAFAAEAAAA4AAAAEAD#####AX9#AAAQAAAAAAAAAAAAAABACAAAAAAAAAAABQACAAAAOAAAABEA#####wF#fwAAEAAAAQAAAAIAAAA6AAAADwAAABEA#####wF#fwAAEAAAAQAAAAIAAAA2AAAADgAAAAkA#####wF#fwAAEAAAAAAAAAAAAAAAQAgAAAAAAAAAAAUAAAAAOwAAADwAAAAFAP####8Bf38AABAAAAEAA2ExMQACAAAAPQAAADoAAAAFAP####8Bf38AABAAAAEAA2EyMQACAAAAPQAAADYAAAAKAP####8Bf38AAAAAAgAAAAsAAAABP+AAAAAAAAAAAAAADwD#####AAAADAAAAEAAAAAQAP####8Af38AABAAAAAAAAAAAAAAAEAIAAAAAAAAAAAFAAEAAABBAAAAEAD#####AX9#AAAQAAAAAAAAAAAAAABACAAAAAAAAAAABQACAAAAQQAAAA8A#####wAAAA0AAABAAAAAEAD#####AX9#AAAQAAAAAAAAAAAAAABACAAAAAAAAAAABQABAAAARAAAABAA#####wB#fwAAEAAAAAAAAAAAAAAAQAgAAAAAAAAAAAUAAgAAAEQAAAARAP####8Bf38AABAAAAEAAAACAAAAQwAAAA0AAAARAP####8Bf38AABAAAAEAAAACAAAARQAAAAwAAAAJAP####8Bf38AABAAAAAAAAAAAAAAAEAIAAAAAAAAAAAFAAAAAEcAAABIAAAABQD#####AX9#AAAQAAABAANhMTMAAgAAAEkAAABDAAAABQD#####AX9#AAAQAAABAANhMjMAAgAAAEkAAABFAAAACgD#####AX9#AAAAAAIAAAAKAAAAAT#gAAAAAAAAAAAAAA8A#####wAAAA0AAABMAAAAEAD#####AH9#AAAQAAAAAAAAAAAAAABACAAAAAAAAAAABQABAAAATQAAABAA#####wF#fwAAEAAAAAAAAAAAAAAAQAgAAAAAAAAAAAUAAgAAAE0AAAAPAP####8AAAAOAAAATAAAABAA#####wF#fwAAEAAAAAAAAAAAAAAAQAgAAAAAAAAAAAUAAQAAAFAAAAAQAP####8Af38AABAAAAAAAAAAAAAAAEAIAAAAAAAAAAAFAAIAAABQAAAAEQD#####AX9#AAAQAAABAAAAAgAAAFEAAAANAAAAEQD#####AX9#AAAQAAABAAAAAgAAAE8AAAAOAAAACQD#####AX9#AAAQAAAAAAAAAAAAAABACAAAAAAAAAAABQAAAABUAAAAUwAAAAUA#####wF#fwAAEAAAAQADYTEyAAIAAABVAAAATwAAAAUA#####wF#fwAAEAAAAQADYTIyAAIAAABVAAAAUQAAAAoA#####wF#fwAAAAACAAAAEgAAAAE#4AAAAAAAAAAAAAAPAP####8AAAARAAAAWAAAABAA#####wF#fwAAEAAAAAAAAAAAAAAAQAgAAAAAAAAAAAUAAQAAAFkAAAAQAP####8Bf38AABAAAAAAAAAAAAAAAEAIAAAAAAAAAAAFAAIAAABZAAAADwD#####AAAAEAAAAFgAAAAQAP####8Bf38AABAAAAAAAAAAAAAAAEAIAAAAAAAAAAAFAAEAAABcAAAAEAD#####AX9#AAAQAAAAAAAAAAAAAABACAAAAAAAAAAABQACAAAAXAAAAAYA#####wF#AH8AEAAAAAAAAAAAAAAAQAgAAAAAAAAAAAUAAAAACAAAAAsAAAAGAP####8BfwB#ABAAAAAAAAAAAAAAAEAIAAAAAAAAAAAFAAAAAAsAAAAKAAAABgD#####AX8AfwAQAAAAAAAAAAAAAABACAAAAAAAAAAABQAAAAAKAAAACQAAAAYA#####wF#AH8AEAAAAAAAAAAAAAAAQAgAAAAAAAAAAAUAAAAACQAAAAgAAAAKAP####8BfwB#AAAAAgAAAF8AAAABP+AAAAAAAAAAAAAACgD#####AX8AfwAAAAIAAABgAAAAAT#gAAAAAAAAAAAAAAoA#####wF#AH8AAAACAAAAYQAAAAE#4AAAAAAAAAAAAAAKAP####8BfwB#AAAAAgAAAGIAAAABP+AAAAAAAAAAAAAADwD#####AAAADAAAAGMAAAAQAP####8BfwB#ABAAAAAAAAAAAAAAAEAIAAAAAAAAAAAFAAIAAABnAAAAEAD#####AX8AfwAQAAAAAAAAAAAAAABACAAAAAAAAAAABQABAAAAZwAAAA8A#####wAAAA0AAABkAAAAEAD#####AX8AfwAQAAAAAAAAAAAAAABACAAAAAAAAAAABQACAAAAagAAABAA#####wF#AH8AEAAAAAAAAAAAAAAAQAgAAAAAAAAAAAUAAQAAAGoAAAAPAP####8AAAAOAAAAZQAAABAA#####wF#AH8AEAAAAAAAAAAAAAAAQAgAAAAAAAAAAAUAAQAAAG0AAAAQAP####8BfwB#ABAAAAAAAAAAAAAAAEAIAAAAAAAAAAAFAAIAAABtAAAADwD#####AAAADwAAAGYAAAAQAP####8BfwB#ABAAAAAAAAAAAAAAAEAIAAAAAAAAAAAFAAEAAABwAAAAEAD#####AX8AfwAQAAAAAAAAAAAAAABACAAAAAAAAAAABQACAAAAcP####8AAAABAAlDUm90YXRpb24A#####wAAAGEAAAABQEaAAAAAAAAAAAAMAP####8BfwB#ABAAAAAAAAAAAAAAAEAIAAAAAAAAAAAFAAAAAG8AAABzAAAACwD#####AAAAYQAAAAwA#####wF#AH8AEAAAAAAAAAAAAAAAQAgAAAAAAAAAAAUAAAAAdAAAAHUAAAAFAP####8BfwB#ABAAAAEAAmxiAAIAAAB2AAAAdAAAABIA#####wAAAF8AAAABQEaAAAAAAAAAAAAMAP####8BfwB#ABAAAAAAAAAAAAAAAEAIAAAAAAAAAAAFAAAAAGkAAAB4AAAAEgD#####AAAAYAAAAAFAYOAAAAAAAAAAAAwA#####wF#AH8AEAAAAAAAAAAAAAAAQAgAAAAAAAAAAAUAAAAAbAAAAHoAAAASAP####8AAABiAAAAAUBg4AAAAAAAAAAADAD#####AX8AfwAQAAAAAAAAAAAAAABACAAAAAAAAAAABQAAAAByAAAAfAAAAAsA#####wAAAGIAAAAMAP####8BfwB#ABAAAAAAAAAAAAAAAEAIAAAAAAAAAAAFAAAAAH0AAAB+AAAACwD#####AAAAXwAAAAwA#####wF#AH8AEAAAAAAAAAAAAAAAQAgAAAAAAAAAAAUAAAAAeQAAAIAAAAALAP####8AAABgAAAADAD#####AX8AfwAQAAAAAAAAAAAAAABACAAAAAAAAAAABQAAAAB7AAAAggAAAAUA#####wF#AH8AEAAAAQACbGgAAgAAAIEAAAB5AAAABQD#####AX8AfwAQAAABAAJsZwACAAAAfwAAAH0AAAAFAP####8BfwB#ABAAAAEAAmxkAAIAAACDAAAAewAAAAIA#####wF#AH8AEAAAAAAAAAAAAAAAQAgAAAAAAAAAAAUAAUB9HJ4YzxWqQHhBaXuwZrsAAAACAP####8BfwB#ABAAAAAAAAAAAAAAAEAIAAAAAAAAAAAFAAFAfqFtY7SzxkB4R2TLnE2XAAAAAgD#####AX8AfwAQAAAAAAAAAAAAAABACAAAAAAAAAAABQABQH3sezIUrCpAd8#CjS5EVP####8AAAABAAlDRHJvaXRlQUIA#####wEAAH8AEAAAAQAAAAEAAAAIAAAACgAAABMA#####wEAAH8AEAAAAQAAAAEAAAAJAAAACwAAAA8A#####wAAAIoAAAAoAAAAEAD#####AQAAfwAQAAAAAAAAAAAAAABACAAAAAAAAAAABQABAAAAjAAAABAA#####wEAAH8AEAAAAAAAAAAAAAAAQAgAAAAAAAAAAAUAAgAAAIwAAAAPAP####8AAACLAAAAQAAAABAA#####wEAAH8AEAAAAAAAAAAAAAAAQAgAAAAAAAAAAAUAAgAAAI8AAAAQAP####8BAAB#ABAAAAAAAAAAAAAAAEAIAAAAAAAAAAAFAAEAAACPAAAADwD#####AAAAigAAAEwAAAAQAP####8BAAB#ABAAAAAAAAAAAAAAAEAIAAAAAAAAAAAFAAIAAACSAAAAEAD#####AQAAfwAQAAAAAAAAAAAAAABACAAAAAAAAAAABQABAAAAkgAAAA8A#####wAAAIsAAAA0AAAAEAD#####AQAAfwAQAAAAAAAAAAAAAABACAAAAAAAAAAABQABAAAAlQAAABAA#####wEAAH8AEAAAAAAAAAAAAAAAQAgAAAAAAAAAAAUAAgAAAJUAAAACAP####8BAAB#ABAAAAAAAAAAAAAAAEAIAAAAAAAAAAAFAAFAffby#fGAKUB43O+Zpdkr#####wAAAAEADENCaXNzZWN0cmljZQD#####AQAAfwAQAAABAAAAAQAAAAgAAAASAAAACwAAAAcA#####wAAAH8BAAFBAAAAjhAAAAAAAAEAAAABAAAAAQAAAAAAAAAAAAFBAAAABwD#####AAAAfwEAAUIAAACREAAAAAAAAQAAAAEAAAABAAAAAAAAAAAAAUIAAAAHAP####8AAAB#AQABQwAAAJQQAAAAAAABAAAAAQAAAAEAAAAAAAAAAAABQwAAAAcA#####wAAAH8BAAFEAAAAlxAAAAAAAAEAAAABAAAAAQAAAAAAAAAAAAFEAAAABwD#####AX8AfwEABmdhdWNoZQAAAIcSAAAAAAACAAAAAQAAAAEAAAAAAAAAAAACRDEAAAAHAP####8BfwB#AQAGZHJvaXRlAAAAiBIAAAAAAAAAAAABAAAAAQAAAAAAAAAAAAJEMgAAAAcA#####wEAfwABAARoYXV0AAAAiRIAAAAAAAEAAAABAAAAAQAAAAAAAAAAAAJEMwAAAAcA#####wEAAAABAANiYXMAAACYEgAAAAAAAQAAAAEAAAABAAAAAAAAAAAAAkQ0AAAADwD#####AAAAmQAAAFgAAAAQAP####8BAAAAABAAAAAAAAAAAAAAAEAIAAAAAAAAAAAHAAEAAACiAAAAEAD#####AQAAAAAQAAAAAAAAAAAAAABACAAAAAAAAAAABwACAAAAogAAAAcA#####wAAAH8BAAFFAAAApBAAAAAAAAEAAAABAAAAAQAAAAAAAAAAAAFFAAAAEQD#####AQAAfwAQAAABAAAAAQAAAF0AAAARAAAAEQD#####AQAAfwAQAAABAAAAAQAAAFoAAAAQAAAABQD#####AX9#AAAQAAABAANhMTUAAwAAAFoAAACjAAAABQD#####AX9#AAAQAAABAANhMjUAAwAAAKMAAABdAAAAB###########'
    const svgId = j3pGetNewId()
    const uu = addDefaultTable(id, 1, 1)[0][0]
    const yy = j3pCreeSVG(uu, { id: svgId, width: 250, height: 150, style: { cursor: 'pointer' } })
    stor.mtgAppLecteur.addDoc(svgId, txtFigure, true)
    stor.mtgAppLecteur.setText(svgId, '#A', lettre[0])
    stor.mtgAppLecteur.setText(svgId, '#D', lettre[1])
    stor.mtgAppLecteur.setText(svgId, '#C', lettre[2])
    stor.mtgAppLecteur.setText(svgId, '#B', lettre[3])
    stor.mtgAppLecteur.setText(svgId, '#E', lettre[4])
    stor.mtgAppLecteur.calculateAndDisplayAll(true)
    for (let i = 0; i < donnees.length; i++) {
      switch (donnees[i]) {
        case 0:
          for (let j = 0; j < 4; j++) {
            if (context[i] !== j) {
              stor.mtgAppLecteur.setVisible(svgId, '#a1' + j, true, true)
              stor.mtgAppLecteur.setVisible(svgId, '#a2' + j, true, true)
            }
          }
          break
        case 1:
          stor.mtgAppLecteur.setVisible(svgId, '#lh', true, true)
          stor.mtgAppLecteur.setVisible(svgId, '#lb', true, true)
          stor.mtgAppLecteur.setVisible(svgId, '#ld', true, true)
          stor.mtgAppLecteur.setVisible(svgId, '#lg', true, true)
          break
        case 2:
          for (let j = 0; j < 4; j++) {
            if (context[i] !== j && (context[i] - 1) % 4 !== j) {
              stor.mtgAppLecteur.setVisible(svgId, '#a1' + j, true, true)
              stor.mtgAppLecteur.setVisible(svgId, '#a2' + j, true, true)
            }
          }
          break
        case 3:
          stor.mtgAppLecteur.setColor(svgId, '#AB', 255, 0, 0, true)
          stor.mtgAppLecteur.setColor(svgId, '#CD', 255, 0, 0, true)
          stor.mtgAppLecteur.setColor(svgId, '#BC', 50, 255, 50, true)
          stor.mtgAppLecteur.setColor(svgId, '#DA', 50, 255, 50, true)
          break
        case 4:
          stor.mtgAppLecteur.setVisible(svgId, '#md11', true, true)
          stor.mtgAppLecteur.setVisible(svgId, '#md21', true, true)
          stor.mtgAppLecteur.setVisible(svgId, '#md12', true, true)
          stor.mtgAppLecteur.setVisible(svgId, '#md22', true, true)
          stor.mtgAppLecteur.setVisible(svgId, '#md32', true, true)
          stor.mtgAppLecteur.setVisible(svgId, '#md42', true, true)
          break
        case 5:
          for (let j = 0; j < 4; j++) {
            if (context[i] === j) {
              stor.mtgAppLecteur.setVisible(svgId, '#a1' + j, true, true)
              stor.mtgAppLecteur.setVisible(svgId, '#a2' + j, true, true)
            }
          }
          break
        case 6:
          if ((context[i][0] === 2) || (context[i][0] === 1)) {
            stor.mtgAppLecteur.setVisible(svgId, '#ld', true, true)
          } else {
            stor.mtgAppLecteur.setVisible(svgId, '#lg', true, true)
          }
          if ((context[i][0] === 1) || (context[i][0] === 0)) {
            stor.mtgAppLecteur.setVisible(svgId, '#lb', true, true)
          } else {
            stor.mtgAppLecteur.setVisible(svgId, '#lh', true, true)
          }
          break
        case 7:
          stor.mtgAppLecteur.setColor(svgId, '#AC', 0, 0, 255, true)
          stor.mtgAppLecteur.setColor(svgId, '#BD', 0, 0, 255, true)
          break
        case 8:
          stor.mtgAppLecteur.setVisible(svgId, '#a15', true, true)
          stor.mtgAppLecteur.setVisible(svgId, '#a25', true, true)
          break
        case 9:
          if ((context[i][0] === 2) || (context[i][0] === 0)) {
            stor.mtgAppLecteur.setVisible(svgId, '#ld', true, true)
            stor.mtgAppLecteur.setVisible(svgId, '#lg', true, true)
          } else {
            stor.mtgAppLecteur.setVisible(svgId, '#lh', true, true)
            stor.mtgAppLecteur.setVisible(svgId, '#lb', true, true)
          }
          break
        case 10:
          if ((context[i] === 1) || (context[i] === 3)) {
            stor.mtgAppLecteur.setColor(svgId, '#AB', 50, 255, 50, true)
            stor.mtgAppLecteur.setColor(svgId, '#CD', 50, 255, 50, true)
          } else {
            stor.mtgAppLecteur.setColor(svgId, '#BC', 50, 255, 50, true)
            stor.mtgAppLecteur.setColor(svgId, '#DA', 50, 255, 50, true)
          }
          break
        case 11:
          for (let j = 0; j < 4; j++) {
            stor.mtgAppLecteur.setVisible(svgId, '#a1' + j, true, true)
            stor.mtgAppLecteur.setVisible(svgId, '#a2' + j, true, true)
          }
      }
    }
    stor.reperes.push(yy)
  }
  function yareponse () {
    let yaya = (stor.zoneclick[0].reponsenb.length !== 0)
    if (me.isElapsed) { yaya = true }
    return yaya
  }
  function bonneReponse () {
    return (stor.zoneclick[0].reponsenb[0] === stor.listeetiq.indexOf('OK'))
  }
  function shuffle (n) {
    if (!n) n = stor.listeetiq.length
    if (n > 1) {
      const i1 = j3pGetRandomInt(0, (stor.listeetiq.length - 1))
      const i2 = j3pGetRandomInt(0, (stor.listeetiq.length - 1))
      let tmp = stor.listeetiq[i1]
      stor.listeetiq[i1] = stor.listeetiq[i2]
      stor.listeetiq[i2] = tmp
      tmp = stor.listecontext[i1]
      stor.listecontext[i1] = stor.listecontext[i2]
      stor.listecontext[i2] = tmp
      tmp = stor.listedonnees[i1]
      stor.listedonnees[i1] = stor.listedonnees[i2]
      stor.listedonnees[i2] = tmp
      shuffle(n - 1)
    }
  }
  function initSection () {
    // Construction de la page
    const structure = ds.Presentation ? 'presentation1' : 'presentation3'
    me.construitStructurePage(structure)
    me.typederreurs = [0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0]
    me.afficheTitre('Retrouver une donnée dans un quadrilatère')
    if (ds.indication) me.indication(me.zones.IG, ds.indication)
    stor.bull = []

    // chargement mtg
    getMtgCore({ withMathJax: true })
      .then(
        // success
        (mtgAppLecteur) => {
          stor.mtgAppLecteur = mtgAppLecteur
          stor.listePourCalc = mtgAppLecteur.createList('TWF0aEdyYXBoSmF2YTEuMAAAABI+TMzNAAJmcv###wEA#wEAAAAAAAAAAAQsAAACvQAAAQEAAAAAAAAAAQAAAAP#####AAAAAQAKQ0NhbGNDb25zdAD#####AAJwaQAWMy4xNDE1OTI2NTM1ODk3OTMyMzg0Nv####8AAAABAApDQ29uc3RhbnRlQAkh+1RELRj#####AAAAAQAHQ0NhbGN1bAD#####AAFlAAZleHAoMSn#####AAAAAgAJQ0ZvbmN0aW9uBwAAAAE#8AAAAAAAAAAAAAIA#####wABeAABMAAAAAEAAAAAAAAAAP###############w==')
          enonceMain()
        },
        // failure
        (error) => {
          j3pShowError(error, { message: 'mathgraph n’est pas correctement chargé', mustNotify: true })
        })
      // plantage dans le code de success
      .catch(j3pShowError)
  } // initSection

  function enonceMain () {
    for (let i = stor.bull.length - 1; i > -1; i--) {
      stor.bull[i].disable()
      stor.bull.pop()
    }
    if (stor.bullaidetext !== undefined) {
      stor.bullaidetext.disable()
    }
    me.videLesZones()

    stor.reperes = []
    stor.zoneclick = []
    stor.bull = []
    stor.bullaidetext = undefined
    stor.lesdiv = {}
    stor.lesdiv.conteneur = j3pAddElt(me.zonesElts.MG, 'div', {
      style: me.styles.etendre('toutpetit.enonce', { padding: '10px' })
    })
    const tt = addDefaultTable(stor.lesdiv.conteneur, 1, 3)
    stor.lesdiv.consigne1 = tt[0][0]
    stor.lesdiv.travail = addDefaultTable(stor.lesdiv.conteneur, 1, 1)[0][0]

    stor.Context = ds.Context === 'oui'
    if (ds.Context === 'les deux') {
      stor.Context = j3pGetRandomBool()
    }
    const tablettres = []
    for (let i = 0; i < 5; i++) {
      let ok, il
      do {
        il = j3pGetRandomInt(65, 90)
        ok = true
        for (let j = 0; j < i; j++) {
          if (il === tablettres[j]) { ok = false; break }
        }
      } while (!ok)
      tablettres.push(il)
    }
    stor.leslettres = []
    for (let i = 0; i < tablettres.length; i++) {
      stor.leslettres.push(String.fromCharCode(tablettres[i]))
    }

    stor.donnees = []
    stor.nomquad = stor.leslettres[0] + stor.leslettres[1] + stor.leslettres[2] + stor.leslettres[3]

    const nbdonnesmax = Math.min(ds.LimiteNombreDonnees, 3)
    stor.nbdonnees = j3pGetRandomInt(1, nbdonnesmax)

    const donint = [[0, 5, 11, 2], [1, 6, 9], [2, 0, 5, 11], [3, 10], [4, 7], [5, 2, 0, 11], [6, 1, 9], [7, 4], [8], [9, 6, 1], [10, 3], [11, 5, 0, 2]]

    let cpt = 0
    for (let i = 0; i < stor.nbdonnees; i++) {
      let ok
      do {
        stor.donnees[i] = j3pGetRandomInt(0, (textes.donnes.length - 1))
        ok = true
        for (let j = 0; j < i; j++) {
          for (let u = 0; u < donint[stor.donnees[i]].length; u++) {
            if (stor.donnees[j] === donint[stor.donnees[i]][u]) { ok = false }
          }
        }
        cpt++
      } while ((!ok) && (cpt !== 100))
    }
    // DonneesContext

    /// contextualise
    stor.context = []
    stor.donntext = []

    for (let i = 0; i < stor.nbdonnees; i++) {
      switch (stor.donnees[i]) {
        case 0:// 3 angles droits
          stor.context[i] = j3pGetRandomInt(0, 3)
          stor.donntext[i] = textes.ila + textes.donnes[0]
          if (stor.Context) {
            stor.donntext[i] = '$ \\widehat{' + stor.leslettres[stor.context[i]] + stor.leslettres[(stor.context[i] + 1) % 4] + stor.leslettres[(stor.context[i] + 2) % 4] + '} = 90° $'
            stor.donntext[i] += ' , $ \\widehat{' + stor.leslettres[(stor.context[i] + 1) % 4] + stor.leslettres[(stor.context[i] + 2) % 4] + stor.leslettres[(stor.context[i] + 3) % 4] + '} = 90° $'
            stor.donntext[i] += ' , $ \\widehat{' + stor.leslettres[(stor.context[i] + 2) % 4] + stor.leslettres[(stor.context[i] + 3) % 4] + stor.leslettres[(stor.context[i] + 4) % 4] + '} = 90° $'
          }
          break
        case 1:// 4 cotes egaux
          stor.context[i] = j3pArrondi(j3pGetRandomInt(22, 98) / 10, 1)
          stor.donntext[i] = textes.ila + textes.donnes[1]
          if (stor.Context) {
            stor.donntext[i] = '$ ' + stor.leslettres[0] + stor.leslettres[1] + ' = '
            stor.donntext[i] += stor.leslettres[1] + stor.leslettres[2] + ' = '
            stor.donntext[i] += stor.leslettres[2] + stor.leslettres[3] + ' = '
            stor.donntext[i] += stor.leslettres[3] + stor.leslettres[0] + ' = ' + stor.context[i] + '  $ cm'
          }
          break
        case 2:// 2 angles droits
          stor.context[i] = j3pGetRandomInt(0, 3)
          stor.donntext[i] = textes.ila + textes.donnes[2]
          if (stor.Context) {
            stor.donntext[i] = '$ \\widehat{' + stor.leslettres[stor.context[i]] + stor.leslettres[(stor.context[i] + 1) % 4] + stor.leslettres[(stor.context[i] + 2) % 4] + '} = 90° $'
            stor.donntext[i] += ' , $ \\widehat{' + stor.leslettres[(stor.context[i] + 1) % 4] + stor.leslettres[(stor.context[i] + 2) % 4] + stor.leslettres[(stor.context[i] + 3) % 4] + '} = 90° $'
            break
          }
          break
        case 3:// cotes opp //
          stor.donntext[i] = textes.ila + textes.donnes[3]
          if (stor.Context) {
            stor.donntext[i] = '(' + stor.leslettres[0] + stor.leslettres[1] + ')//(' + stor.leslettres[3] + stor.leslettres[2] + ') et (' + stor.leslettres[2] + stor.leslettres[1] + ')//(' + stor.leslettres[3] + stor.leslettres[0] + ')'
          }
          break
        case 4:// diag coupent milieu
          stor.donntext[i] = textes.ila + textes.donnes[4]
          if (stor.Context) {
            stor.donntext[i] = stor.leslettres[4] + textes.milieudiag + '[' + stor.leslettres[0] + stor.leslettres[2] + ']' + textes.et + '[' + stor.leslettres[1] + stor.leslettres[3] + ']'
          }
          break
        case 5:// un angle droit
          stor.context[i] = j3pGetRandomInt(0, 3)
          stor.donntext[i] = textes.ila + textes.donnes[5]
          if (stor.Context) {
            stor.donntext[i] = '$ \\widehat{' + stor.leslettres[(stor.context[i] + 3) % 4] + stor.leslettres[stor.context[i]] + stor.leslettres[(stor.context[i] + 1) % 4] + '} = 90° $'
          }
          break
        case 6:// 2 consec mm longueur
          stor.context[i] = [j3pGetRandomInt(0, 3), j3pArrondi(j3pGetRandomInt(22, 98) / 10, 1)]
          stor.donntext[i] = textes.ila + textes.donnes[6]
          if (stor.Context) {
            stor.donntext[i] = '$ ' + stor.leslettres[stor.context[i][0]] + stor.leslettres[(stor.context[i][0] + 1) % 4] + ' = ' + stor.leslettres[(stor.context[i][0] + 1) % 4] + stor.leslettres[(stor.context[i][0] + 2) % 4] + ' = ' + stor.context[i][1] + ' $ cm'
          }
          break
        case 7:// 2 diag mm longueur
          stor.context[i] = j3pArrondi(j3pGetRandomInt(77, 98) / 10, 1)
          stor.donntext[i] = textes.ila + textes.donnes[7]
          if (stor.Context) {
            stor.donntext[i] = '$ ' + stor.leslettres[0] + stor.leslettres[2] + ' = ' + stor.leslettres[1] + stor.leslettres[3] + ' = ' + stor.context[i] + ' $ cm'
          }
          break
        case 8:// un angle droit
          stor.context[i] = j3pGetRandomInt(0, 3)
          stor.donntext[i] = textes.ila + textes.donnes[8]
          if (stor.Context) {
            stor.donntext[i] = '$ \\widehat{' + stor.leslettres[stor.context[i]] + stor.leslettres[4] + stor.leslettres[(stor.context[i] + 1) % 4] + '} = 90° $'
          }
          break
        case 9:// deux op mm longueur
          stor.donntext[i] = textes.ila + textes.donnes[9]
          stor.context[i] = [j3pGetRandomInt(0, 3), j3pArrondi(j3pGetRandomInt(22, 98) / 10, 1)]
          if (stor.Context) {
            stor.donntext[i] = '$ ' + stor.leslettres[stor.context[i][0]] + stor.leslettres[(stor.context[i][0] + 1) % 4] + ' = ' + stor.leslettres[(stor.context[i][0] + 2) % 4] + stor.leslettres[(stor.context[i][0] + 3) % 4] + ' = ' + stor.context[i][1] + ' $ cm'
          }
          break
        case 10:// deux op parall
          stor.donntext[i] = textes.ila + textes.donnes[10]
          stor.context[i] = j3pGetRandomInt(0, 3)
          if (stor.Context) {
            stor.donntext[i] = '(' + stor.leslettres[stor.context[i]] + stor.leslettres[(stor.context[i] + 1) % 4] + ')//(' + stor.leslettres[(stor.context[i] + 2) % 4] + stor.leslettres[(stor.context[i] + 3) % 4] + ') '
          }
          break
        case 11:// 4 angle droit
          stor.donntext[i] = textes.ila + textes.donnes[11]
          if (stor.Context) {
            stor.donntext[i] = '$ \\widehat{' + stor.leslettres[0] + stor.leslettres[1] + stor.leslettres[2] + '} =  '
            stor.donntext[i] += '  \\widehat{' + stor.leslettres[1] + stor.leslettres[2] + stor.leslettres[3] + '} =  '
            stor.donntext[i] += '  \\widehat{' + stor.leslettres[2] + stor.leslettres[3] + stor.leslettres[0] + '} =  '
            stor.donntext[i] += '  \\widehat{' + stor.leslettres[3] + stor.leslettres[0] + stor.leslettres[1] + '} = 90° $'
          }
          break
      }
    }

    if (ds.Presentation) {
      stor.lesdiv.correction = j3pAddElt(me.zonesElts.MD, 'div', {
        style: me.styles.etendre('toutpetit.enonce', { padding: '10px' })
      })
    } else {
      stor.lesdiv.correction = tt[0][2]
      tt[0][1].style.width = '20px'
    }
    stor.lesdiv.explications = j3pAddElt(stor.lesdiv.conteneur, 'div')
    stor.lesdiv.explications.style.color = me.styles.cfaux

    j3pAffiche(stor.lesdiv.consigne1, null, '<b><u>' + textes.Consigne1 + '</u></b>\n')
    j3pAffiche(stor.lesdiv.consigne1, null, '<i>( <u>codage</u>: deux côtés opposés en couleur sont parallèles,</i>\n', { style: { color: me.styles.petit.correction.color } })
    j3pAffiche(stor.lesdiv.consigne1, null, '&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;<i>deux diagonales colorées sont de même longueur )</i>\n', { style: { color: me.styles.petit.correction.color } })
    j3pAffiche(stor.lesdiv.consigne1, null, stor.nomquad + textes.estun + textes.quadbase + textes.telque + '\n• ' + textes.precise + '\n',
      { a: stor.leslettres[4] })
    for (let i = 0; i < stor.nbdonnees; i++) {
      j3pAffiche(stor.lesdiv.consigne1, null, '•' + stor.donntext[i] + '\n')
    }

    me.zonesElts.MG.classList.add('fond')
    stor.lesdiv.travail.classList.add('travail')
    stor.lesdiv.consigne1.classList.add('enonce')
    const lacool = textes.couleuretiquettes

    stor.listeetiq = []
    stor.listedonnees = []
    stor.listecontext = []
    stor.listeetiq[0] = 'OK'
    stor.listedonnees[0] = []
    stor.listecontext[0] = []
    for (let i = 0; i < stor.donnees.length; i++) {
      stor.listedonnees[0][i] = stor.donnees[i]
      stor.listecontext[0][i] = stor.context[i]
    }

    cpt = 0
    stor.listeetiq[1] = 'Dez'
    stor.listedonnees[1] = []
    stor.listedonnees[1][0] = 12
    stor.listecontext[1] = []
    stor.listecontext[1][0] = ''
    for (let i = 0; i < stor.donnees.length; i++) {
      stor.listedonnees[1][i + 1] = stor.donnees[i]
      stor.listecontext[1][i + 1] = stor.context[i]
    }

    stor.listeetiq[2] = 'en-'
    stor.listedonnees[2] = []
    stor.listecontext[2] = []
    for (let i = 0; i < stor.donnees.length - 1; i++) {
      stor.listedonnees[2][i] = stor.donnees[i]
      stor.listecontext[2][i] = stor.context[i]
    }

    for (let i = stor.listeetiq.length; i < 6; i++) {
      stor.listeetiq[i] = 'Fo' + i
      stor.listedonnees[i] = []
      stor.listecontext[i] = []
      for (let j = 0; j < stor.donnees.length; j++) {
        stor.listedonnees[i][j] = stor.donnees[j]
        stor.listecontext[i][j] = stor.context[j]
      }
      const j = (i - 3) % stor.donnees.length

      let ok
      do {
        stor.listedonnees[i][j] = j3pGetRandomInt(0, (textes.donnes.length - 1))
        ok = true
        for (let m = 0; m < stor.listedonnees[i].length; m++) {
          if (m !== j) {
            for (let u = 0; u < donint[stor.listedonnees[i][m]].length; u++) {
              if (stor.listedonnees[i][j] === donint[stor.listedonnees[i][m]][u]) { ok = false }
            }
          }
        }
        for (let m = 0; m < stor.donnees.length; m++) {
          if (stor.listedonnees[i][j] === stor.donnees[m]) { ok = false }
        }
        for (let m = 3; m < i; m++) {
          if (stor.listedonnees[i][j] === stor.listedonnees[m][j]) { ok = false }
        }
        cpt++
      } while ((!ok) && (cpt !== 100))

      switch (stor.listedonnees[i][j]) {
        case 0:stor.listecontext[i][j] = j3pGetRandomInt(0, 3)
          break
        case 1:stor.listecontext[i][j] = j3pArrondi(j3pGetRandomInt(22, 98) / 10, 1)
          break
        case 2:stor.listecontext[i][j] = j3pGetRandomInt(0, 3)
          break
        case 5:stor.listecontext[i][j] = j3pGetRandomInt(0, 3)
          break
        case 6:stor.listecontext[i][j] = [j3pGetRandomInt(0, 3), j3pArrondi(j3pGetRandomInt(22, 98) / 10, 1)]
          break
        case 7:stor.listecontext[i][j] = j3pArrondi(j3pGetRandomInt(77, 98) / 10, 1)
          break
        case 8:stor.listecontext[i][j] = j3pGetRandomInt(0, 3)
          break
        case 9:stor.listecontext[i][j] = [j3pGetRandomInt(0, 3), j3pArrondi(j3pGetRandomInt(22, 98) / 10, 1)]
          break
        case 10:stor.listecontext[i][j] = j3pGetRandomInt(0, 3)
          break
      }
    }

    shuffle(12)

    stor.zoneclick[stor.zoneclick.length] = new Zoneclick(stor.lesdiv.travail, stor.listeetiq, { couleur: lacool, dispo: 'fixe', colonne: 3, ligne: 2, reponsesmultiples: false, afaire: '', width: 240, height: 124, affiche: false, image: true })
    stor.mtgAppLecteur.removeAllDoc()
    for (let i = 0; i < stor.listedonnees.length; i++) {
      const idbuf = stor.zoneclick[0].recupid(i)
      fig(stor.leslettres, stor.listedonnees[i], stor.listecontext[i], idbuf)
    }

    me.finEnonce()
  }

  switch (this.etat) {
    case 'enonce':
      // code exécuté au lancement de la section
      if (this.debutDeLaSection) {
        initSection()
      } else {
        enonceMain()
      }
      break // case "enonce":

    case 'correction':
      // On teste si une réponse a été saisie
      stor.lesdiv.explications.classList.remove('explique')
      j3pEmpty(stor.lesdiv.explications)
      if (!yareponse()) {
        stor.lesdiv.correction.style.color = this.styles.cfaux
        stor.lesdiv.correction.innerHTML = reponseIncomplete
      } else { // Une réponse a été saisie
        // Bonne réponse
        if (bonneReponse()) {
          this._stopTimer()
          this.score++
          stor.lesdiv.correction.style.color = this.styles.cbien
          stor.lesdiv.correction.innerHTML = cBien

          stor.lesdiv.explications.innerHTML = ''

          stor.zoneclick[0].disable()
          stor.zoneclick[0].corrige(true)

          this.typederreurs[0]++
          this.cacheBoutonValider()
          this.etat = 'navigation'
          this.sectionCourante()
        } else {
          // Pas de bonne réponse
          stor.lesdiv.correction.style.color = this.styles.cfaux

          // A cause de la limite de temps :
          if (this.isElapsed) { // limite de temps
            stor.lesdiv.explication.classList.add('explique')
            stor.lesdiv.correction.innerHTML = tempsDepasse
            this.typederreurs[10]++
            this.cacheBoutonValider()

            stor.zoneclick[0].disable()
            stor.zoneclick[0].corrige([stor.listeetiq.indexOf('OK')], true)
            if (stor.zoneclick[0].reponsenb.length !== 0) {
              if (stor.zoneclick[0].reponse === 'Dez') {
                j3pAffiche(stor.lesdiv.explications, null, textes.dez, { a: stor.nomquad })
              } else {
                let buf
                for (let i = 0; i < stor.nbdonnees; i++) {
                  if (stor.listedonnees[stor.zoneclick[0].reponsenb[0]].indexOf(stor.donnees[i]) === -1) {
                    buf = textes.donnes[stor.donnees[i]]
                    break
                  }
                }
                if (buf === 'un angle droit') {
                  buf = 'un seul angle droit codé'
                }
                if (buf === 'deux côtés opposés de même longueur') {
                  buf = 'exactement ' + buf + ' codés'
                }
                if (buf.indexOf('angles droits') !== -1) {
                  buf = buf + ' codés'
                }
                j3pAffiche(stor.lesdiv.explications, null, textes.enmoins, { a: stor.nomquad, b: buf })
              }
            }
            /// //

            this.etat = 'navigation'
            this.sectionCourante()
          } else {
            stor.lesdiv.correction.innerHTML = cFaux

            if (this.essaiCourant < ds.nbchances) {
              stor.lesdiv.correction.innerHTML += '<br>' + essaieEncore

              /// //
              /// //

              this.typederreurs[1]++
              // indication éventuelle ici
            } else { // Erreur au nème essai
              this._stopTimer()
              this.cacheBoutonValider()
              stor.lesdiv.correction.innerHTML += '<br>' + regardeCorrection
              stor.lesdiv.explications.classList.add('explique')
              /// //
              stor.zoneclick[0].disable()
              stor.zoneclick[0].corrige([stor.listeetiq.indexOf('OK')], true)
              if (stor.zoneclick[0].reponse[0] === 'Dez') {
                j3pAffiche(stor.lesdiv.explications, null, textes.dez, { a: stor.nomquad })
              } else {
                let buf
                for (let i = 0; i < stor.nbdonnees; i++) {
                  if (stor.listedonnees[stor.zoneclick[0].reponsenb[0]].indexOf(stor.donnees[i]) === -1) {
                    buf = textes.donnes[stor.donnees[i]]
                    break
                  }
                }

                if (buf === 'un angle droit') {
                  buf = 'un seul angle droit codé'
                }
                if (buf === 'deux côtés opposés de même longueur') {
                  buf = 'exactement ' + buf + ' codés'
                }
                if (buf.indexOf('angles droits') !== -1) {
                  buf = buf + ' codés'
                }
                j3pAffiche(stor.lesdiv.explications, null, textes.enmoins, { a: stor.nomquad, b: buf })
              }
              stor.zoneclick[0].corrige([stor.listeetiq.indexOf('OK')], true)
              stor.zoneclick[0].disable()

              this.typederreurs[2]++
              this.etat = 'navigation'
              this.sectionCourante()
            }
          }
        }
      }
      // Obligatoire
      this.finCorrection()
      break // case "correction":

    case 'navigation':

      if (this.sectionTerminee()) {
        // On détermine la phrase d’état renvoyée par la section
        this.parcours.pe = (ds.nbetapes * this.score) / ds.nbitems
        this.afficheBoutonSectionSuivante()
        this.focus('sectioncontinuer')
      } else {
        this.etat = 'enonce'
        this.afficheBoutonSuite()
        this.focus('boutoncontinuer')
      }
      // Obligatoire
      this.finNavigation()

      break // case "navigation":
  }
}
