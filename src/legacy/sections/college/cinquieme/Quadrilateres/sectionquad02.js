import { j3pAddElt, j3pAjouteBouton, j3pArrondi, j3pDiv, j3pEmpty, j3pGetNewId, j3pGetRandomBool, j3pGetRandomInt, j3pShowError, j3pShuffle } from 'src/legacy/core/functions'
import Paint from 'src/legacy/outils/brouillon/Paint'
import BulleAide from 'src/legacy/outils/bulleAide/BulleAide'
import Etiquettes from 'src/legacy/outils/etiquette/Etiquettes'
import PlaceEtiquettes from 'src/legacy/outils/etiquette/PlaceEtiquettes'
import { j3pAffiche } from 'src/lib/mathquill/functions'
import { addDefaultTable } from 'src/legacy/themes/table'
import { getMtgCore } from 'src/lib/outils/mathgraph'
import { j3pCreeSVG } from 'src/legacy/core/functionsSvg'
import textesGeneriques from 'src/lib/core/textes'

const { cBien, cFaux, essaieEncore, regardeCorrection, reponseIncomplete, tempsDepasse } = textesGeneriques

/**
 * Ajouter une description ici
 * @fileOverview
 * @author Tommy Barroy <t.barroy@laposte.net>
 * @since 2017-10
*/

export const params = {
  outils: ['calculatrice', 'mathquill'],
  /**
   * Ce qui est paramétrable dans l’éditeur de graphe
   * @type {LegacySectionParamsParametres}
   */
  parametres: [
    ['nbrepetitions', 9, 'entier', 'Nombre de répétitions de la section'],
    ['indication', '', 'string', 'Indication pour chaque répétition'],
    ['limite', 0, 'entier', 'Temps disponible par répétition, en secondes'],
    ['TypeEnonce', 'Les deux', 'liste', '<u>Texte</u>: L’énoncé est un texte. <BR><BR> <u>Figure</u>: L’énoncé est une figure. <BR><BR> <u>Les deux</u>: L’un ou l’autre aléatoirement. <BR><BR> <u>En même temps</u>: Les deux en même temps.', ['Texte', 'Figure', 'Les deux', 'En même temps']],
    ['LimiteNombreDonnees', 3, 'entier', 'Nombre de maximum de données (entre 1 et 3)'],
    ['Brouillon', false, 'boolean', '<u>true</u>: L’élève dispose d’un brouillon quand l’énoncé est un texte'],
    ['Presentation', true, 'boolean', "<u>true</u>: Les indications de réussite et les boutons 'ok' et 'suivant' sont dans un cadre séparé à droite. <BR><BR> <u>false</u>: Un seul cadre pour tout"],
    // le modèle gère la compatibilité ascendante avec l’ancien paramètre MiseEnPagePlusClaire
    ['theme', 'standard', 'liste', 'Modèle pour l’affichage', ['standard', 'zonesAvecImageDeFond']]

  ]
}

const textes = {
  couleurexplique: '#A9E2F3',
  couleurcorrec: '#A9F5A9',
  couleurenonce: '#D8D8D8',
  couleurenonceg: '#F3E2A9',
  couleuretiquettes: '#A9E2F3',
  repch: [
    ['Compte bien le nombre d’angles droits !', 'Ce quadrilatère a plus d’un angle droit !', 'Si un quadrilatère a des angles droits, c’est entre deux côtés consécutifs !', 'Il n’y a pas autant d’angles droits !'],
    ['Vérifie le nombre de côtés ayant la même longueur !', 'Encore plus de côtés sont concernés !', 'Encore plus de côtés sont concernés !'],
    ['Il est noté que le quadrilatère a un centre de symétrie !'],
    ['Il y a plusieurs indications de côtés parallèles !', 'L’énoncé indique qu’ils sont parallèles !', 'Il faut prendre en compte tous les côtés !'],
    ['Il est indiqué que deux segments ont le même milieu !', 'Des diagonales qui ont le même milieu n’ont pas forcément la même longueur !', 'Les données n’indiquent pas d’angle droit !'],
    ['Observe bien les angles entre deux côtés consécutifs !', 'Il n’y a pas autant d’angles droits !', 'Il faut vérifier si il s’agit de côtés ou de diagonales !', 'Il n’y a pas autant d’angles droits !'],
    ['Observe bien les longueurs des côtés !', 'Moins de côtés sont concernés !', 'Deux côtés opposés sont l’un en face de l’autre !'],
    ['Observe bien les diagonales !', 'Il n’y a pas d’incation concernant le milieu des diagonales !', 'Les deux longueurs égales ne concernent pas des côtés !', 'Les deux longueurs égales ne concernent pas des côtés !'],
    ['Observe bien l’angle que forment les diagonales !', 'L’angle droit ne se situe pas entre deux côtés !'],
    ['Observe bien les côtés opposés !', 'Il n’est pas indiqué qu’ils sont parallèles !', 'Les longueurs égales ne sont pas celles des diagonales !', 'Deux côtés consécutifs se touchent !', 'Moins de côtés sont concernés !'],
    ['Il est indiqué que deux côtés sont parallèles !', 'Moins de côtés sont concernés !'],
    ['Observe bien les angles entre deux côtés consécutifs !', 'Ce quadrilatère a plus d’un angle droit !', 'Ce quadrilatère a plus d’un angle droit !']
  ],
  Consigne1: 'Retrouve les données du quadrilatère.',
  quadbase: 'quadrilatère',
  estun: ' est un ',
  // ATTENTION à mettre le même nombre d’éléments que dans repch
  donnes: [
    'trois angles droits',
    'quatre côtés de même longueur',
    'un centre de symétrie',
    'des côtés opposés parallèles deux à deux',
    'des diagonales qui se coupent en leur milieu',
    'un angle droit',
    'deux côtés consécutifs de même longueur',
    'des diagonales de même longueur',
    'des diagonales perpendiculaires',
    'deux côtés opposés de même longueur',
    'deux côtés opposés paralléles',
    'quatre angles droits'
  ],
  a: 'a ',
  estcentredu: ' est le centre de symétrie du ',
  pourcentre: ' est son centre de symétrie',
  milieudiag: ' est le milieu des segments ',
  et: ' et ',
  precise: 'ses diagonales se coupent en £a ,',
  telque: ' tel que: ',
  le: 'Le ',
  cotitre: 'Correction:',
  ila: ' il a ',
  brouillontitre: 'Tu peux utiliser ce brouillon.',
  brouillonbout: 'Utiliser un brouillon',
  conseil: 'Il t’est conseillé d’effectuer une figure <BR> à main levée sur un brouillon <BR> avant de répondre à la question. '
}

/**
 * section quad02
 * @this {Parcours}
 */
export default function main () {
  const me = this
  const stor = this.storage
  const ds = me.donneesSection
  let svgId = stor.svgId

  function fig (lettre) {
    const txtFigure = 'TWF0aEdyYXBoSmF2YTEuMAAAABM+TMzNAAJmcv###wEA#wEAAAAAAAAAAANLAAACfwAAAQEAAAAAAAAAAQAAAKr#####AAAAAQAKQ0NhbGNDb25zdAD#####AAJwaQAWMy4xNDE1OTI2NTM1ODk3OTMyMzg0Nv####8AAAABAApDQ29uc3RhbnRlQAkh+1RELRj#####AAAAAQAKQ1BvaW50QmFzZQD#####AQAAAAAOAAFVAMAkAAAAAAAAQBAAAAAAAAAAAAUAAEAv#nbItDlYQC#+dsi0OVj#####AAAAAQAUQ0Ryb2l0ZURpcmVjdGlvbkZpeGUA#####wEAAAAAEAAAAQAAAAEAAAABAT#wAAAAAAAA#####wAAAAEAD0NQb2ludExpZURyb2l0ZQD#####AQAAAAAOAAFWAMAAAAAAAAAAQBAAAAAAAAAAAAUAAUA##nbItDlYAAAAAv####8AAAABAAhDU2VnbWVudAD#####AQAAAAAQAAABAAAAAQAAAAEAAAAD#####wAAAAEAB0NNaWxpZXUA#####wEAAAAAEAAAAAAAAAAAAAAAQAgAAAAAAAAAAAUAAAAAAQAAAAP#####AAAAAgAMQ0NvbW1lbnRhaXJlAP####8BAAAAAAAAAAAAAAAAQBgAAAAAAAAAAAAAAAUMAAAAAAABAAAAAAAAAAEAAAAAAAAAAAABMf####8AAAABAAlDTG9uZ3VldXIA#####wAAAAEAAAADAAAAAgD#####AAAAAAAQAAAAAAAAAAAAAABACAAAAAAAAAAABwAAQFtAAAAAAABAR#Cj1wo9cAAAAAIA#####wAAAAAAEAAAAAAAAAAAAAAAQAgAAAAAAAAAAAcAAEBUQAAAAAAAQGS8KPXCj1wAAAACAP####8AAAAAABAAAAAAAAAAAAAAAEAIAAAAAAAAAAAHAABAcuAAAAAAAEBlPCj1wo9cAAAAAgD#####AAAAAAAQAAAAAAAAAAAAAABACAAAAAAAAAAABwAAQHFwAAAAAABATPCj1wo9cAAAAAUA#####wAAAAAAEAAAAQACQUIAAwAAAAgAAAALAAAABQD#####AAAAAAAQAAABAAJCQwADAAAACwAAAAoAAAAFAP####8AAAAAABAAAAEAAkNEAAMAAAAKAAAACQAAAAUA#####wAAAAAAEAAAAQACREEAAwAAAAkAAAAIAAAABQD#####AAAAAAAQAAABAAJCRAACAAAACQAAAAsAAAAFAP####8AAAAAABAAAAEAAkFDAAIAAAAKAAAACP####8AAAABABBDSW50RHJvaXRlRHJvaXRlAP####8BAAAAABAAAAAAAAAAAAAAAEAIAAAAAAAAAAAHAAAAABAAAAARAAAABgD#####AQAAAAAQAAAAAAAAAAAAAABACAAAAAAAAAAABQAAAAAIAAAAEgAAAAYA#####wEAAAAAEAAAAAAAAAAAAAAAQAgAAAAAAAAAAAUAAAAAEgAAAAoAAAAGAP####8BAAAAABAAAAAAAAAAAAAAAEAIAAAAAAAAAAAFAAAAAAsAAAASAAAABgD#####AQAAAAAQAAAAAAAAAAAAAABACAAAAAAAAAAABQAAAAASAAAACf####8AAAACAAlDQ2VyY2xlT1IA#####wH#AAAABG1kMTEAAgAAABUAAAABP9MzMzMzMzMAAAAACgD#####Af8AAAAEbWQyMQACAAAAFgAAAAE#0zMzMzMzMwAAAAACAP####8BAAD#ABAAAAAAAAAAAAAAAEAIAAAAAAAAAAAFAAFAYSAAAAAAAEBUuFHrhR64AAAAAgD#####AQAA#wAQAAAAAAAAAAAAAABACAAAAAAAAAAABQABQGSgAAAAAABAUDhR64UeuP####8AAAABABFDU3ltZXRyaWVDZW50cmFsZQD#####AAAAE#####8AAAABAAtDUG9pbnRJbWFnZQD#####AQAA#wAQAAAAAAAAAAAAAABACAAAAAAAAAAABQAAAAAaAAAAGwAAAAwA#####wEAAP8AEAAAAAAAAAAAAAAAQAgAAAAAAAAAAAUAAAAAGQAAABsAAAAFAP####8BAAD#ABAAAAEABG1kMTIAAgAAABkAAAAaAAAABQD#####AQAA#wAQAAABAARtZDIyAAIAAAAcAAAAHf####8AAAABAAhDVmVjdGV1cgD#####AQAA#wAQAAABAAAAAgAAABMAAAAUAP####8AAAABAA1DVHJhbnNQYXJWZWN0AP####8AAAAgAAAADAD#####AQAA#wAQAAAAAAAAAAAAAABACAAAAAAAAAAABQAAAAAcAAAAIQAAAAwA#####wEAAP8AEAAAAAAAAAAAAAAAQAgAAAAAAAAAAAUAAAAAHQAAACEAAAAFAP####8BAAD#ABAAAAEABG1kMzIAAgAAACIAAAAjAAAADAD#####AQAA#wAQAAAAAAAAAAAAAABACAAAAAAAAAAABQAAAAAZAAAAIQAAAAwA#####wEAAP8AEAAAAAAAAAAAAAAAQAgAAAAAAAAAAAUAAAAAGgAAACEAAAAFAP####8BAAD#ABAAAAEABG1kNDIAAgAAACUAAAAmAAAACgD#####AQAA#wAAAAIAAAAIAAAAAT#gAAAAAAAAAP####8AAAABABBDSW50RHJvaXRlQ2VyY2xlAP####8AAAAMAAAAKP####8AAAABABBDUG9pbnRMaWVCaXBvaW50AP####8BAAD#ABAAAAAAAAAAAAAAAEAIAAAAAAAAAAAFAAEAAAApAAAAEAD#####AAAA#wAQAAAAAAAAAAAAAABACAAAAAAAAAAABQACAAAAKQAAAA8A#####wAAAA8AAAAoAAAAEAD#####AAAA#wAQAAAAAAAAAAAAAABACAAAAAAAAAAABQABAAAALAAAABAA#####wEAAP8AEAAAAAAAAAAAAAAAQAgAAAAAAAAAAAUAAgAAACz#####AAAAAQAQQ0Ryb2l0ZVBhcmFsbGVsZQD#####AQAA#wAQAAABAAAAAgAAACoAAAAPAAAAEQD#####AQAA#wAQAAABAAAAAgAAAC4AAAAMAAAACQD#####AQAA#wAQAAAAAAAAAAAAAABACAAAAAAAAAAABQAAAAAwAAAALwAAAAUA#####wF#fwAAEAAAAQADYTEwAAIAAAAuAAAAMQAAAAUA#####wF#fwAAEAAAAQADYTIwAAIAAAAxAAAAKgAAAAoA#####wF#fwAAAAACAAAACQAAAAE#4AAAAAAAAAAAAAAPAP####8AAAAPAAAANAAAABAA#####wF#fwAAEAAAAAAAAAAAAAAAQAgAAAAAAAAAAAUAAQAAADUAAAAQAP####8Af38AABAAAAAAAAAAAAAAAEAIAAAAAAAAAAAFAAIAAAA1AAAADwD#####AAAADgAAADQAAAAQAP####8Af38AABAAAAAAAAAAAAAAAEAIAAAAAAAAAAAFAAEAAAA4AAAAEAD#####AX9#AAAQAAAAAAAAAAAAAABACAAAAAAAAAAABQACAAAAOAAAABEA#####wF#fwAAEAAAAQAAAAIAAAA6AAAADwAAABEA#####wF#fwAAEAAAAQAAAAIAAAA2AAAADgAAAAkA#####wF#fwAAEAAAAAAAAAAAAAAAQAgAAAAAAAAAAAUAAAAAOwAAADwAAAAFAP####8Bf38AABAAAAEAA2ExMQACAAAAPQAAADoAAAAFAP####8Bf38AABAAAAEAA2EyMQACAAAAPQAAADYAAAAKAP####8Bf38AAAAAAgAAAAsAAAABP+AAAAAAAAAAAAAADwD#####AAAADAAAAEAAAAAQAP####8Af38AABAAAAAAAAAAAAAAAEAIAAAAAAAAAAAFAAEAAABBAAAAEAD#####AX9#AAAQAAAAAAAAAAAAAABACAAAAAAAAAAABQACAAAAQQAAAA8A#####wAAAA0AAABAAAAAEAD#####AX9#AAAQAAAAAAAAAAAAAABACAAAAAAAAAAABQABAAAARAAAABAA#####wB#fwAAEAAAAAAAAAAAAAAAQAgAAAAAAAAAAAUAAgAAAEQAAAARAP####8Bf38AABAAAAEAAAACAAAAQwAAAA0AAAARAP####8Bf38AABAAAAEAAAACAAAARQAAAAwAAAAJAP####8Bf38AABAAAAAAAAAAAAAAAEAIAAAAAAAAAAAFAAAAAEcAAABIAAAABQD#####AX9#AAAQAAABAANhMTMAAgAAAEkAAABDAAAABQD#####AX9#AAAQAAABAANhMjMAAgAAAEkAAABFAAAACgD#####AX9#AAAAAAIAAAAKAAAAAT#gAAAAAAAAAAAAAA8A#####wAAAA0AAABMAAAAEAD#####AH9#AAAQAAAAAAAAAAAAAABACAAAAAAAAAAABQABAAAATQAAABAA#####wF#fwAAEAAAAAAAAAAAAAAAQAgAAAAAAAAAAAUAAgAAAE0AAAAPAP####8AAAAOAAAATAAAABAA#####wF#fwAAEAAAAAAAAAAAAAAAQAgAAAAAAAAAAAUAAQAAAFAAAAAQAP####8Af38AABAAAAAAAAAAAAAAAEAIAAAAAAAAAAAFAAIAAABQAAAAEQD#####AX9#AAAQAAABAAAAAgAAAFEAAAANAAAAEQD#####AX9#AAAQAAABAAAAAgAAAE8AAAAOAAAACQD#####AX9#AAAQAAAAAAAAAAAAAABACAAAAAAAAAAABQAAAABUAAAAUwAAAAUA#####wF#fwAAEAAAAQADYTEyAAIAAABVAAAATwAAAAUA#####wF#fwAAEAAAAQADYTIyAAIAAABVAAAAUQAAAAoA#####wF#fwAAAAACAAAAEgAAAAE#4AAAAAAAAAAAAAAPAP####8AAAARAAAAWAAAABAA#####wF#fwAAEAAAAAAAAAAAAAAAQAgAAAAAAAAAAAUAAQAAAFkAAAAQAP####8Bf38AABAAAAAAAAAAAAAAAEAIAAAAAAAAAAAFAAIAAABZAAAADwD#####AAAAEAAAAFgAAAAQAP####8Bf38AABAAAAAAAAAAAAAAAEAIAAAAAAAAAAAFAAEAAABcAAAAEAD#####AX9#AAAQAAAAAAAAAAAAAABACAAAAAAAAAAABQACAAAAXAAAAAYA#####wF#AH8AEAAAAAAAAAAAAAAAQAgAAAAAAAAAAAUAAAAACAAAAAsAAAAGAP####8BfwB#ABAAAAAAAAAAAAAAAEAIAAAAAAAAAAAFAAAAAAsAAAAKAAAABgD#####AX8AfwAQAAAAAAAAAAAAAABACAAAAAAAAAAABQAAAAAKAAAACQAAAAYA#####wF#AH8AEAAAAAAAAAAAAAAAQAgAAAAAAAAAAAUAAAAACQAAAAgAAAAKAP####8BfwB#AAAAAgAAAF8AAAABP+AAAAAAAAAAAAAACgD#####AX8AfwAAAAIAAABgAAAAAT#gAAAAAAAAAAAAAAoA#####wF#AH8AAAACAAAAYQAAAAE#4AAAAAAAAAAAAAAKAP####8BfwB#AAAAAgAAAGIAAAABP+AAAAAAAAAAAAAADwD#####AAAADAAAAGMAAAAQAP####8BfwB#ABAAAAAAAAAAAAAAAEAIAAAAAAAAAAAFAAIAAABnAAAAEAD#####AX8AfwAQAAAAAAAAAAAAAABACAAAAAAAAAAABQABAAAAZwAAAA8A#####wAAAA0AAABkAAAAEAD#####AX8AfwAQAAAAAAAAAAAAAABACAAAAAAAAAAABQACAAAAagAAABAA#####wF#AH8AEAAAAAAAAAAAAAAAQAgAAAAAAAAAAAUAAQAAAGoAAAAPAP####8AAAAOAAAAZQAAABAA#####wF#AH8AEAAAAAAAAAAAAAAAQAgAAAAAAAAAAAUAAQAAAG0AAAAQAP####8BfwB#ABAAAAAAAAAAAAAAAEAIAAAAAAAAAAAFAAIAAABtAAAADwD#####AAAADwAAAGYAAAAQAP####8BfwB#ABAAAAAAAAAAAAAAAEAIAAAAAAAAAAAFAAEAAABwAAAAEAD#####AX8AfwAQAAAAAAAAAAAAAABACAAAAAAAAAAABQACAAAAcP####8AAAABAAlDUm90YXRpb24A#####wAAAGEAAAABQEaAAAAAAAAAAAAMAP####8BfwB#ABAAAAAAAAAAAAAAAEAIAAAAAAAAAAAFAAAAAG8AAABzAAAACwD#####AAAAYQAAAAwA#####wF#AH8AEAAAAAAAAAAAAAAAQAgAAAAAAAAAAAUAAAAAdAAAAHUAAAAFAP####8BfwB#ABAAAAEAAmxiAAIAAAB2AAAAdAAAABIA#####wAAAF8AAAABQEaAAAAAAAAAAAAMAP####8BfwB#ABAAAAAAAAAAAAAAAEAIAAAAAAAAAAAFAAAAAGkAAAB4AAAAEgD#####AAAAYAAAAAFAYOAAAAAAAAAAAAwA#####wF#AH8AEAAAAAAAAAAAAAAAQAgAAAAAAAAAAAUAAAAAbAAAAHoAAAASAP####8AAABiAAAAAUBg4AAAAAAAAAAADAD#####AX8AfwAQAAAAAAAAAAAAAABACAAAAAAAAAAABQAAAAByAAAAfAAAAAsA#####wAAAGIAAAAMAP####8BfwB#ABAAAAAAAAAAAAAAAEAIAAAAAAAAAAAFAAAAAH0AAAB+AAAACwD#####AAAAXwAAAAwA#####wF#AH8AEAAAAAAAAAAAAAAAQAgAAAAAAAAAAAUAAAAAeQAAAIAAAAALAP####8AAABgAAAADAD#####AX8AfwAQAAAAAAAAAAAAAABACAAAAAAAAAAABQAAAAB7AAAAggAAAAUA#####wF#AH8AEAAAAQACbGgAAgAAAIEAAAB5AAAABQD#####AX8AfwAQAAABAAJsZwACAAAAfwAAAH0AAAAFAP####8BfwB#ABAAAAEAAmxkAAIAAACDAAAAewAAAAIA#####wF#AH8AEAAAAAAAAAAAAAAAQAgAAAAAAAAAAAUAAUBOAAAAAAAAQFg4UeuFHrgAAAACAP####8BfwB#ABAAAAAAAAAAAAAAAEAIAAAAAAAAAAAFAAFAdAAAAAAAAEBZOFHrhR64AAAAAgD#####AX8AfwAQAAAAAAAAAAAAAABACAAAAAAAAAAABQABQGjgAAAAAABANOFHrhR64v####8AAAABAAlDRHJvaXRlQUIA#####wEAAH8AEAAAAQAAAAEAAAAIAAAACgAAABMA#####wEAAH8AEAAAAQAAAAEAAAAJAAAACwAAAA8A#####wAAAIoAAAAoAAAAEAD#####AQAAfwAQAAAAAAAAAAAAAABACAAAAAAAAAAABQABAAAAjAAAABAA#####wEAAH8AEAAAAAAAAAAAAAAAQAgAAAAAAAAAAAUAAgAAAIwAAAAPAP####8AAACLAAAAQAAAABAA#####wEAAH8AEAAAAAAAAAAAAAAAQAgAAAAAAAAAAAUAAgAAAI8AAAAQAP####8BAAB#ABAAAAAAAAAAAAAAAEAIAAAAAAAAAAAFAAEAAACPAAAADwD#####AAAAigAAAEwAAAAQAP####8BAAB#ABAAAAAAAAAAAAAAAEAIAAAAAAAAAAAFAAIAAACSAAAAEAD#####AQAAfwAQAAAAAAAAAAAAAABACAAAAAAAAAAABQABAAAAkgAAAA8A#####wAAAIsAAAA0AAAAEAD#####AQAAfwAQAAAAAAAAAAAAAABACAAAAAAAAAAABQABAAAAlQAAABAA#####wEAAH8AEAAAAAAAAAAAAAAAQAgAAAAAAAAAAAUAAgAAAJUAAAACAP####8BAAB#ABAAAAAAAAAAAAAAAEAIAAAAAAAAAAAFAAFAacAAAAAAAEBpHCj1wo9c#####wAAAAEADENCaXNzZWN0cmljZQD#####AQAAfwAQAAABAAAAAQAAAAgAAAASAAAACwAAAAcA#####wAAAH8BAAFBAAAAjhAAAAAAAAEAAAABAAAAAQAAAAAAAAAAAAFBAAAABwD#####AAAAfwEAAUIAAACREAAAAAAAAQAAAAEAAAABAAAAAAAAAAAAAUIAAAAHAP####8AAAB#AQABQwAAAJQQAAAAAAABAAAAAQAAAAEAAAAAAAAAAAABQwAAAAcA#####wAAAH8BAAFEAAAAlxAAAAAAAAEAAAABAAAAAQAAAAAAAAAAAAFEAAAABwD#####AX8AfwEABmdhdWNoZQAAAIcSAAAAAAACAAAAAQAAAAEAAAAAAAAAAAACRDEAAAAHAP####8BfwB#AQAGZHJvaXRlAAAAiBIAAAAAAAAAAAABAAAAAQAAAAAAAAAAAAJEMgAAAAcA#####wEAfwABAARoYXV0AAAAiRIAAAAAAAEAAAABAAAAAQAAAAAAAAAAAAJEMwAAAAcA#####wEAAAABAANiYXMAAACYEgAAAAAAAQAAAAEAAAABAAAAAAAAAAAAAkQ0AAAADwD#####AAAAmQAAAFgAAAAQAP####8BAAAAABAAAAAAAAAAAAAAAEAIAAAAAAAAAAAHAAEAAACiAAAAEAD#####AQAAAAAQAAAAAAAAAAAAAABACAAAAAAAAAAABwACAAAAogAAAAcA#####wAAAH8BAAFFAAAApBAAAAAAAAEAAAABAAAAAQAAAAAAAAAAAAFFAAAAEQD#####AQAAfwAQAAABAAAAAQAAAF0AAAARAAAAEQD#####AQAAfwAQAAABAAAAAQAAAFoAAAAQAAAABQD#####AX9#AAAQAAABAANhMTUAAwAAAFoAAACjAAAABQD#####AX9#AAAQAAABAANhMjUAAwAAAKMAAABdAAAAB###########'
    svgId = j3pGetNewId()
    stor.svgId = svgId
    j3pCreeSVG(stor.lesdiv.consfig, { id: svgId, width: 450, height: 230, style: { border: 'solid black 1px' } })
    stor.mtgAppLecteur.removeAllDoc()
    stor.mtgAppLecteur.addDoc(svgId, txtFigure, true)
    stor.mtgAppLecteur.setText(svgId, '#A', lettre[0])
    stor.mtgAppLecteur.setText(svgId, '#D', lettre[1])
    stor.mtgAppLecteur.setText(svgId, '#C', lettre[2])
    stor.mtgAppLecteur.setText(svgId, '#B', lettre[3])
    stor.mtgAppLecteur.setText(svgId, '#E', lettre[4])
    stor.mtgAppLecteur.calculateAndDisplayAll(true)
    for (let i = 0; i < stor.nbdonnees; i++) {
      switch (stor.donnees[i]) {
        case 0:
          for (let j = 0; j < 4; j++) {
            if (stor.context[i] !== j) {
              stor.mtgAppLecteur.setVisible(svgId, '#a1' + j, true, true)
              stor.mtgAppLecteur.setVisible(svgId, '#a2' + j, true, true)
            }
          }
          break
        case 1:
          stor.mtgAppLecteur.setVisible(svgId, '#lh', true, true)
          stor.mtgAppLecteur.setVisible(svgId, '#lb', true, true)
          stor.mtgAppLecteur.setVisible(svgId, '#ld', true, true)
          stor.mtgAppLecteur.setVisible(svgId, '#lg', true, true)
          break
        case 2:
          stor.mtgAppLecteur.setText(svgId, '#bas', lettre[4] + textes.estcentredu + ' quadrilatère.', true)
          break
        case 3:
          stor.mtgAppLecteur.setColor(svgId, '#AB', 255, 0, 0, true)
          stor.mtgAppLecteur.setColor(svgId, '#CD', 255, 0, 0, true)
          stor.mtgAppLecteur.setColor(svgId, '#BC', 50, 255, 50, true)
          stor.mtgAppLecteur.setColor(svgId, '#DA', 50, 255, 50, true)
          stor.mtgAppLecteur.setText(svgId, '#droite', '(' + lettre[0] + lettre[1] + ')//(' + lettre[3] + lettre[2] + ') \n (' + lettre[2] + lettre[1] + ')//(' + lettre[3] + lettre[0] + ')', true)
          break
        case 4:
          stor.mtgAppLecteur.setVisible(svgId, '#md11', true, true)
          stor.mtgAppLecteur.setVisible(svgId, '#md21', true, true)
          stor.mtgAppLecteur.setVisible(svgId, '#md12', true, true)
          stor.mtgAppLecteur.setVisible(svgId, '#md22', true, true)
          stor.mtgAppLecteur.setVisible(svgId, '#md32', true, true)
          stor.mtgAppLecteur.setVisible(svgId, '#md42', true, true)
          break
        case 5:
          for (let j = 0; j < 4; j++) {
            if (stor.context[i] === j) {
              stor.mtgAppLecteur.setVisible(svgId, '#a1' + j, true, true)
              stor.mtgAppLecteur.setVisible(svgId, '#a2' + j, true, true)
            }
          }
          break
        case 6:
          if ((stor.context[i][0] === 2) || (stor.context[i][0] === 1)) {
            stor.mtgAppLecteur.setText(svgId, '#droite', stor.context[i][1] + ' cm', true)
            stor.mtgAppLecteur.setVisible(svgId, '#ld', true, true)
          } else {
            stor.mtgAppLecteur.setText(svgId, '#gauche', stor.context[i][1] + ' cm', true)
            stor.mtgAppLecteur.setVisible(svgId, '#lg', true, true)
          }
          if ((stor.context[i][0] === 1) || (stor.context[i][0] === 0)) {
            stor.mtgAppLecteur.setVisible(svgId, '#lb', true, true)
          } else {
            stor.mtgAppLecteur.setVisible(svgId, '#lh', true, true)
          }
          break
        case 7:
          stor.mtgAppLecteur.setColor(svgId, '#AC', 0, 0, 255, true)
          stor.mtgAppLecteur.setColor(svgId, '#BD', 0, 0, 255, true)
          stor.mtgAppLecteur.setText(svgId, '#haut', stor.leslettres[0] + stor.leslettres[2] + ' = ' + stor.leslettres[1] + stor.leslettres[3] + ' = ' + stor.context[i] + '  cm', true)
          stor.mtgAppLecteur.setColor(svgId, '#haut', 0, 0, 255, true)
          break
        case 8:
          stor.mtgAppLecteur.setVisible(svgId, '#a15', true, true)
          stor.mtgAppLecteur.setVisible(svgId, '#a25', true, true)
          break
        case 9:
          if ((stor.context[i][0] === 2) || (stor.context[i][0] === 0)) {
            stor.mtgAppLecteur.setText(svgId, '#droite', stor.context[i][1] + ' cm', true)
            stor.mtgAppLecteur.setVisible(svgId, '#ld', true, true)
            stor.mtgAppLecteur.setVisible(svgId, '#lg', true, true)
          } else {
            stor.mtgAppLecteur.setText(svgId, '#haut', stor.context[i][1] + ' cm', true)
            stor.mtgAppLecteur.setColor(svgId, '#haut', 150, 50, 150, true)
            stor.mtgAppLecteur.setVisible(svgId, '#lh', true, true)
            stor.mtgAppLecteur.setVisible(svgId, '#lb', true, true)
          }
          break
        case 10:
          stor.mtgAppLecteur.setColor(svgId, '#droite', 50, 255, 50, true)
          if ((stor.context[i] === 1) || (stor.context[i] === 3)) {
            stor.mtgAppLecteur.setColor(svgId, '#AB', 50, 255, 50, true)
            stor.mtgAppLecteur.setColor(svgId, '#CD', 50, 255, 50, true)
            stor.mtgAppLecteur.setText(svgId, '#droite', '(' + lettre[2] + lettre[1] + ')//(' + lettre[3] + lettre[0] + ')', true)
          } else {
            stor.mtgAppLecteur.setColor(svgId, '#BC', 50, 255, 50, true)
            stor.mtgAppLecteur.setColor(svgId, '#DA', 50, 255, 50, true)
            stor.mtgAppLecteur.setText(svgId, '#droite', '(' + lettre[0] + lettre[1] + ')//(' + lettre[3] + lettre[2] + ')', true)
          }
          break
        case 11:
          for (let j = 0; j < 4; j++) {
            stor.mtgAppLecteur.setVisible(svgId, '#a1' + j, true, true)
            stor.mtgAppLecteur.setVisible(svgId, '#a2' + j, true, true)
          }
      }
    }
  }
  function yareponse () {
    let yaya = true
    for (let i = 0; i < stor.nbdonnees; i++) {
      if (stor.placeetiquettes[i].etiq === -1) { yaya = false; break }
    }
    if (me.isElapsed) { yaya = true }
    return yaya
  }
  function bonneReponse () {
    stor.rep = []
    for (let i = 0; i < stor.nbdonnees; i++) {
      stor.rep.push(textes.donnes.indexOf(stor.placeetiquettes[i].contenu))
    }
    for (let i = 0; i < stor.nbdonnees; i++) {
      stor.placeetiquettes[i].corrige(true)
    }
    stor.erentrop = false
    stor.erentropliste = []
    for (let i = 0; i < stor.nbdonnees; i++) {
      if (stor.donnees.indexOf(stor.rep[i]) === -1) {
        stor.erentrop = true
        stor.placeetiquettes[i].corrige(false)
        stor.placeetiquettes[i].barre()
        stor.erentropliste.push([stor.rep[i], i])
      }
    }
    stor.ermank = false
    stor.ermankliste = []
    for (let i = 0; i < stor.nbdonnees; i++) {
      if (stor.rep.indexOf(stor.donnees[i]) === -1) {
        stor.ermank = true
        stor.ermankliste.push(stor.donnees[i])
      }
    }
    return ((!stor.erentrop) && (!stor.ermank))
  }

  function enonceMain () {
    for (let i = stor.bull.length - 1; i > -1; i--) {
      stor.bull[i].disable()
      stor.bull.pop()
    }
    if (stor.bullaidetext !== undefined) {
      stor.bullaidetext.disable()
    }
    stor.TypeEnonceencours = stor.TypeEnonce
    if (stor.TypeEnonceencours === 'Les deux') {
      stor.TypeEnonceencours = j3pGetRandomBool() ? 'Texte' : 'Figure'
    }
    me.videLesZones()
    stor.etiquettes = []
    stor.placeetiquettes = []
    stor.bull = []
    stor.bullaidetext = undefined
    stor.lesdiv = {}
    stor.lesdiv.conteneur = j3pAddElt(me.zonesElts.MG, 'div', {
      style: me.styles.etendre('toutpetit.enonce', { padding: '10px' })
    })
    const tt = addDefaultTable(stor.lesdiv.conteneur, 1, 4)
    stor.lesdiv.consigne = tt[0][0]
    if (!ds.Presentation) {
      stor.lesdiv.aideeFich = tt[0][2]
      tt[0][1].style.width = '20px'
    } else {
      stor.lesdiv.aideeFich = j3pAddElt(me.zonesElts.MD, 'div', {
        style: me.styles.etendre('toutpetit.enonce', { padding: '10px' })
      })
    }
    const tt2 = addDefaultTable(stor.lesdiv.consigne, 2, 1)
    stor.lesdiv.titreZ = tt2[0][0]
    if (stor.TypeEnonceencours === 'Figure') {
      stor.lesdiv.consfig = tt2[1][0]
    } else {
      const tt3 = addDefaultTable(tt2[1][0], 1, 3)
      stor.lesdiv.cons1 = tt3[0][0]
      tt3[0][1].style.width = '10px'
      stor.lesdiv.consfig = tt3[0][2]
    }
    const tt4 = addDefaultTable(stor.lesdiv.titreZ, 1, 3)
    stor.lesdiv.titre = tt4[0][0]
    stor.lesdiv.BrouillTitre = tt4[0][2]
    tt4[0][1].style.width = '10px'

    j3pAffiche(stor.lesdiv.titre, null, '<b><u>' + textes.Consigne1 + '</u></b>')

    const tablettres = []
    for (let i = 0; i < 5; i++) {
      let ok, il
      do {
        il = j3pGetRandomInt(65, 90)
        ok = true
        for (let j = 0; j < i; j++) {
          if (il === tablettres[j]) {
            ok = false
            break
          }
        }
      } while (!ok)
      tablettres.push(il)
    }
    stor.leslettres = []
    for (let i = 0; i < tablettres.length; i++) {
      stor.leslettres.push(String.fromCharCode(tablettres[i]))
    }

    stor.donnees = []
    stor.nomquad = stor.leslettres[0] + stor.leslettres[1] + stor.leslettres[2] + stor.leslettres[3]

    ds.LimiteNombreDonnees = Math.max(1, ds.LimiteNombreDonnees)
    const nbdonnesmax = Math.min(ds.LimiteNombreDonnees, 3)
    stor.nbdonnees = j3pGetRandomInt(1, nbdonnesmax)

    const donint = [[0, 5, 11], [1, 6, 9], [2], [3, 10], [4], [5, 0, 11], [6, 1, 9], [7], [8], [9, 6, 1], [10, 3], [11, 5, 0]]
    const foaff = [[0, 5, 11, 8], [1, 6, 7, 9], [2, 4], [3, 9, 10], [4, 7, 8], [5, 0, 11, 8], [6, 1, 7, 9], [7, 1, 6, 9], [8, 7, 4], [9, 1, 6, 7], [10, 3, 9], [11, 0, 5, 8]]

    let cpt = 0
    for (let i = 0; i < stor.nbdonnees; i++) {
      let ok
      do {
        stor.donnees[i] = j3pGetRandomInt(0, (textes.donnes.length - 1))
        ok = true
        for (let j = 0; j < i; j++) {
          for (let u = 0; u < donint[stor.donnees[i]].length; u++) {
            if (stor.donnees[j] === donint[stor.donnees[i]][u]) { ok = false }
          }
        }
        cpt++
      }
      while ((!ok) && (cpt !== 100))
    }
    // DonneesContext

    /// contextualise
    stor.context = []
    stor.donntext = []
    for (let i = 0; i < stor.nbdonnees; i++) {
      switch (stor.donnees[i]) {
        case 0:// 3 angles droits
          stor.context[i] = j3pGetRandomInt(0, 3)
          stor.donntext[i] = '$ \\widehat{' + stor.leslettres[stor.context[i]] + stor.leslettres[(stor.context[i] + 1) % 4] + stor.leslettres[(stor.context[i] + 2) % 4] + '} = 90° $'
          stor.donntext[i] += ' , $ \\widehat{' + stor.leslettres[(stor.context[i] + 1) % 4] + stor.leslettres[(stor.context[i] + 2) % 4] + stor.leslettres[(stor.context[i] + 3) % 4] + '} = 90° $'
          stor.donntext[i] += ' , $ \\widehat{' + stor.leslettres[(stor.context[i] + 2) % 4] + stor.leslettres[(stor.context[i] + 3) % 4] + stor.leslettres[(stor.context[i] + 4) % 4] + '} = 90° $'
          break
        case 1:// 4 cotes egaux
          stor.context[i] = j3pArrondi(j3pGetRandomInt(22, 98) / 10, 1)
          stor.donntext[i] = '$ ' + stor.leslettres[0] + stor.leslettres[1] + ' = '
          stor.donntext[i] += stor.leslettres[1] + stor.leslettres[2] + ' = '
          stor.donntext[i] += stor.leslettres[2] + stor.leslettres[3] + ' = '
          stor.donntext[i] += stor.leslettres[3] + stor.leslettres[0] + ' = ' + stor.context[i] + '  $ cm'
          break
        case 2:// centre de symétrie
          stor.context[i] = j3pArrondi(j3pGetRandomInt(22, 98) / 10, 1)
          stor.donntext[i] = stor.leslettres[4] + textes.pourcentre
          break
        case 3:// cotes opp //
          stor.donntext[i] = textes.ila + textes.donnes[3]
          stor.donntext[i] = '(' + stor.leslettres[0] + stor.leslettres[1] + ')//(' + stor.leslettres[3] + stor.leslettres[2] + ') et (' + stor.leslettres[2] + stor.leslettres[1] + ')//(' + stor.leslettres[3] + stor.leslettres[0] + ')'
          break
        case 4:// diag coupent milieu
          stor.donntext[i] = textes.ila + textes.donnes[4]
          stor.donntext[i] = stor.leslettres[4] + textes.milieudiag + '[' + stor.leslettres[0] + stor.leslettres[2] + ']' + textes.et + '[' + stor.leslettres[1] + stor.leslettres[3] + ']'
          break
        case 5:// un angle droit
          stor.context[i] = j3pGetRandomInt(0, 3)
          stor.donntext[i] = '$ \\widehat{' + stor.leslettres[(stor.context[i] + 3) % 4] + stor.leslettres[stor.context[i]] + stor.leslettres[(stor.context[i] + 1) % 4] + '} = 90° $'
          break
        case 6:// 2 consec mm longueur
          stor.context[i] = [j3pGetRandomInt(0, 3), j3pArrondi(j3pGetRandomInt(22, 98) / 10, 1)]
          stor.donntext[i] = '$ ' + stor.leslettres[stor.context[i][0]] + stor.leslettres[(stor.context[i][0] + 1) % 4] + ' = ' + stor.leslettres[(stor.context[i][0] + 1) % 4] + stor.leslettres[(stor.context[i][0] + 2) % 4] + ' = ' + stor.context[i][1] + ' $ cm'
          break
        case 7:// 2 diag mm longueur
          stor.context[i] = j3pArrondi(j3pGetRandomInt(77, 98) / 10, 1)
          stor.donntext[i] = '$ ' + stor.leslettres[0] + stor.leslettres[2] + ' = ' + stor.leslettres[1] + stor.leslettres[3] + ' = ' + stor.context[i] + ' $ cm'
          break
        case 8:// un angle droit
          stor.context[i] = j3pGetRandomInt(0, 3)
          stor.donntext[i] = '$ \\widehat{' + stor.leslettres[stor.context[i]] + stor.leslettres[4] + stor.leslettres[(stor.context[i] + 1) % 4] + '} = 90° $'
          break
        case 9:// deux op mm longueur
          stor.context[i] = [j3pGetRandomInt(0, 3), j3pArrondi(j3pGetRandomInt(22, 98) / 10, 1)]
          stor.donntext[i] = '$ ' + stor.leslettres[stor.context[i][0]] + stor.leslettres[(stor.context[i][0] + 1) % 4] + ' = ' + stor.leslettres[(stor.context[i][0] + 2) % 4] + stor.leslettres[(stor.context[i][0] + 3) % 4] + ' = ' + stor.context[i][1] + ' $ cm'
          break
        case 10:// deux op parall
          stor.context[i] = j3pGetRandomInt(0, 3)
          stor.donntext[i] = '(' + stor.leslettres[stor.context[i]] + stor.leslettres[(stor.context[i] + 1) % 4] + ')//(' + stor.leslettres[(stor.context[i] + 2) % 4] + stor.leslettres[(stor.context[i] + 3) % 4] + ') '
          break
        case 11:// 4 angle droit
          stor.donntext[i] = '$ \\widehat{' + stor.leslettres[0] + stor.leslettres[1] + stor.leslettres[2] + '} =  '
          stor.donntext[i] += '  \\widehat{' + stor.leslettres[1] + stor.leslettres[2] + stor.leslettres[3] + '} =  '
          stor.donntext[i] += '  \\widehat{' + stor.leslettres[2] + stor.leslettres[3] + stor.leslettres[0] + '} =  '
          stor.donntext[i] += '  \\widehat{' + stor.leslettres[3] + stor.leslettres[0] + stor.leslettres[1] + '} = 90° $'
          break
      }
    }

    if ((stor.TypeEnonceencours === 'Texte') || (stor.TypeEnonceencours === 'En même temps')) {
      j3pAffiche(stor.lesdiv.cons1, null, stor.nomquad + textes.estun + textes.quadbase + textes.telque + '\n• ' + textes.precise + '\n',
        { a: stor.leslettres[4] })
      for (let i = 0; i < stor.nbdonnees; i++) {
        j3pAffiche(stor.lesdiv.cons1, null, '•' + stor.donntext[i] + '\n')
      }
      if (stor.TypeEnonceencours === 'Texte') {
        if (!ds.Brouillon) {
          stor.bullaidetext = new BulleAide(stor.lesdiv.consfig, textes.conseil, { place: 5 })
        } else {
          const retab = addDefaultTable(stor.lesdiv.consfig, 1, 2)
          retab[0][0].style.width = '10px'
          stor.lesdiv.bbrouill = retab[0][1]
          stor.mafoncbrouillon = function () {
            j3pEmpty(stor.lesdiv.bbrouill)
            const yy = addDefaultTable(stor.lesdiv.bbrouill, 2, 1)
            j3pAffiche(yy[0][0], null, textes.brouillontitre)
            const mun = Math.max(stor.lesdiv.cons1.offsetHeight, 120)
            Paint.create(yy[1][0], { width: 400, height: mun, textes: stor.leslettres })
          }
          j3pAjouteBouton(stor.lesdiv.bbrouill, 'boutonsbrouill', 'MepBoutons', textes.brouillonbout, stor.mafoncbrouillon)
        }
      }
    }
    if ((stor.TypeEnonceencours === 'Figure') || (stor.TypeEnonceencours === 'En même temps')) {
      fig(stor.leslettres)
    }

    const travail = addDefaultTable(stor.lesdiv.conteneur, 1, 1)[0][0]
    stor.lesdiv.explications = j3pAddElt(stor.lesdiv.conteneur, 'div')
    stor.lesdiv.explications.style.color = me.styles.cfaux

    stor.Grotab = addDefaultTable(travail, 1, 5)
    if (ds.Presentation) {
      stor.lesdiv.correction = j3pAddElt(me.zonesElts.MD, 'div', {
        style: me.styles.etendre('toutpetit.enonce', { padding: '10px' })
      })
    } else {
      stor.lesdiv.correction = tt[0][3]
    }
    stor.Grotab[0][3].style.width = '10px'
    stor.lesdiv.etik = stor.Grotab[0][4]
    stor.lesdiv.solution = stor.Grotab[0][2]
    stor.lesdiv.solution.style.color = me.styles.petit.correction.color
    stor.Grotab[0][1].style.width = '10px'
    stor.lesdiv.travail = stor.Grotab[0][0]
    stor.lesdiv.travail.style.verticalAlign = 'top'
    stor.lesdiv.leszonesTra = addDefaultTable(stor.lesdiv.travail, 5, 2)
    stor.lesdiv.travail.style.border = ' solid  1px grey'
    stor.lesdiv.consigne.classList.add('enonce')
    stor.lesdiv.travail.classList.add('travail')
    me.zonesElts.MG.classList.add('fond')

    j3pAffiche(stor.lesdiv.leszonesTra[0][0], null, textes.le + textes.quadbase + ' ' + stor.nomquad + ' ' + textes.a + ':')
    let listeetiq = []
    for (let i = 0; i < stor.nbdonnees; i++) {
      for (let j = 0; j < foaff[stor.donnees[i]].length; j++) {
        if (listeetiq.indexOf(foaff[stor.donnees[i]][j]) === -1) { listeetiq.push(foaff[stor.donnees[i]][j]) }
      }
    }
    let lacool

    cpt = 0
    while ((listeetiq.length > 6) && (cpt < 1000)) {
      const i = j3pGetRandomInt(0, (listeetiq.length - 1))
      let ok = true
      for (let j = 0; j < stor.nbdonnees; j++) {
        ok = ok && (stor.donnees[j] !== listeetiq[i])
      }
      if (ok) { listeetiq.splice(i, 1) }
      cpt++
    }
    while ((listeetiq.length < 6) && (cpt < 1000)) {
      const i = j3pGetRandomInt(0, (listeetiq.length - 1))
      if (listeetiq.indexOf(i) === -1) { listeetiq.push(i) }
      cpt++
    }
    for (let j = 0; j < listeetiq.length; j++) {
      listeetiq[j] = textes.donnes[listeetiq[j]]
    }
    listeetiq = j3pShuffle(listeetiq)
    stor.etiquettes[stor.etiquettes.length] = new Etiquettes(stor.lesdiv.etik, 'etik', listeetiq, {
      couleur: lacool, // undefined si on est pas passé dans le if qui l’affecte plus haut
      dispo: 'colonne',
      colonne: 2,
      reutilisable: false,
      width: 369
    })

    for (let i = 0; i < stor.nbdonnees; i++) {
      stor.placeetiquettes[stor.placeetiquettes.length] = new PlaceEtiquettes(stor.lesdiv.leszonesTra[i + 1][0], 'peti' + i, stor.etiquettes[0], {})
    }
    me.finEnonce()
  }
  function initSection () {
    // Construction de la page
    const structure = ds.Presentation ? 'presentation1' : 'presentation3'
    me.construitStructurePage({ structure, ratioGauche: 0.8 })

    // DECLARATION DES VARIABLES
    // reponse attendue
    stor.TypeEnonce = 'Les deux'
    if ((ds.TypeEnonce === 'Texte') || (ds.TypeEnonce === 'texte') || (ds.TypeEnonce === 'Textes') || (ds.TypeEnonce === 'textes')) { stor.TypeEnonce = 'Texte' }
    if ((ds.TypeEnonce === 'Figure') || (ds.TypeEnonce === 'figure') || (ds.TypeEnonce === 'figures') || (ds.TypeEnonce === 'Figures')) { stor.TypeEnonce = 'Figure' }
    if ((ds.TypeEnonce === 'En même temps') || (ds.TypeEnonce === 'Enmêmetemps') || (ds.TypeEnonce === 'en même temps') || (ds.TypeEnonce === 'En meme temps')) { stor.TypeEnonce = 'En même temps' }
    /*
             Par convention,`
            `   me.typederreurs[0] = nombre de bonnes réponses
                me.typederreurs[1] = nombre de fois où l’élève a utilisé une chance supplémentaire
                me.typederreurs[2] = nombre de mauvaises réponses
                me.typederreurs[10] = nombre de fois où l’élève a eu faux à cause d’un temps limite
                LES AUTRES peuvent être utilisées par la détermination de la pe dans le cas d’une section qualitative
             */
    me.typederreurs = [0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0]
    me.afficheTitre('Traduire une donnée dans un quadrilatère')
    if (ds.indication) me.indication(me.zones.IG, ds.indication)
    stor.bull = []

    // chargement mtg
    getMtgCore({ withMathJax: true })
      .then(
        // success
        (mtgAppLecteur) => {
          stor.mtgAppLecteur = mtgAppLecteur
          stor.listePourCalc = mtgAppLecteur.createList('TWF0aEdyYXBoSmF2YTEuMAAAABI+TMzNAAJmcv###wEA#wEAAAAAAAAAAAQsAAACvQAAAQEAAAAAAAAAAQAAAAP#####AAAAAQAKQ0NhbGNDb25zdAD#####AAJwaQAWMy4xNDE1OTI2NTM1ODk3OTMyMzg0Nv####8AAAABAApDQ29uc3RhbnRlQAkh+1RELRj#####AAAAAQAHQ0NhbGN1bAD#####AAFlAAZleHAoMSn#####AAAAAgAJQ0ZvbmN0aW9uBwAAAAE#8AAAAAAAAAAAAAIA#####wABeAABMAAAAAEAAAAAAAAAAP###############w==')
          enonceMain()
        },
        // failure
        (error) => {
          j3pShowError(error, { message: 'mathgraph n’est pas correctement chargé', mustNotify: true })
        })
      // plantage dans le code de success
      .catch(j3pShowError)
  } // initSection

  switch (this.etat) {
    case 'enonce': {
      // code exécuté au lancement de la section
      if (this.debutDeLaSection) {
        initSection()
      } else {
        enonceMain()
      }
      break // case "enonce":
    }

    case 'correction': {
      // On teste si une réponse a été saisie
      if (!yareponse()) {
        stor.lesdiv.correction.style.color = this.styles.cfaux
        stor.lesdiv.correction.innerHTML = reponseIncomplete
        return this.finCorrection()
      }
      // Une réponse a été saisie
      if (bonneReponse()) {
        this.score += j3pArrondi(1 / ds.nbetapes, 1)
        stor.lesdiv.correction.style.color = this.styles.cbien
        stor.lesdiv.correction.innerHTML = cBien

        stor.lesdiv.explications.innerHTML = ''
        for (let i = stor.placeetiquettes.length - 1; i > -1; i--) {
          stor.placeetiquettes[i].corrige(true)
          stor.placeetiquettes[i].disable()
          stor.placeetiquettes.pop()
        }
        for (let i = stor.etiquettes.length - 1; i > -1; i--) {
          stor.etiquettes[i].detruit()
          stor.placeetiquettes.pop()
        }

        this.typederreurs[0]++
        this.cacheBoutonValider()
        this.etat = 'navigation'
        return this.finCorrection('navigation', true)
      }

      // Pas de bonne réponse
      stor.lesdiv.correction.style.color = this.styles.cfaux

      // A cause de la limite de temps :
      if (this.isElapsed) { // limite de temps
        stor.lesdiv.correction.innerHTML = tempsDepasse
        this.typederreurs[10]++
        this.cacheBoutonValider()
        const repchoisie = [[5, 8, 11], [6, 9], [], [9, 10], [7, 8], [0, 8, 11], [1, 9], [4, 9, 6], [5], [10, 7, 6, 1], [3], [0, 5]]
        const repchois = []
        for (let i = 0; i < stor.ermankliste.length; i++) {
          for (let j = 0; j < stor.erentropliste.length; j++) {
            if (repchoisie[stor.ermankliste[i]].indexOf(stor.erentropliste[j][0]) !== -1) {
              repchois.push([textes.repch[stor.ermankliste[i]][repchoisie[stor.ermankliste[i]].indexOf(stor.erentropliste[j][0]) + 1], stor.erentropliste[j][1]])
            }
          }
        }
        for (let i = 0; i < repchois.length; i++) {
          stor.bull[stor.bull.length] = new BulleAide(stor.lesdiv.leszonesTra[repchois[i][1] + 1][1], repchois[i][0], { place: 5 })
        }
        for (let i = 0; i < stor.ermankliste.length; i++) {
          j3pAffiche(stor.lesdiv.explications, null, textes.repch[stor.ermankliste[i]][0] + '\n')
        }
        stor.etiquettes[0].disable()
        stor.etiquettes[0].detruit()
        stor.etiquettes.pop()
        stor.lesdiv.etik.innerHTML = ''
        const yy = j3pDiv(stor.lesdiv.etik, { contenu: '', style: this.styles.toutpetit.correction })
        j3pAffiche(yy, null, '<b><u>' + textes.cotitre + '</u></b> \n' + textes.le + textes.quadbase + ' ' + stor.nomquad + ' ' + textes.a + ': \n')
        for (let i = 0; i < stor.nbdonnees; i++) {
          j3pAffiche(yy, null, '• ' + textes.donnes[stor.donnees[i]] + '\n')
        }
        yy.classList.add('correction')
        return this.finCorrection('navigation', true)
      }

      // Réponse fausse :
      stor.lesdiv.correction.innerHTML = cFaux

      if (me.essaiCourant < ds.nbchances) {
        stor.lesdiv.correction.innerHTML += '<br>' + essaieEncore
        this.typederreurs[1]++
        return this.finCorrection() // on reste en état correction
      }

      // Erreur au dernier essai
      this.cacheBoutonValider()
      stor.lesdiv.correction.innerHTML += '<br>' + regardeCorrection
      const repchoisie = [[5, 8, 11], [6, 9], [], [9, 10], [7, 8], [0, 8, 11], [1, 9], [4, 9, 6], [5], [10, 7, 6, 1], [3], [0, 5]]
      const repchois = []
      for (let i = 0; i < stor.ermankliste.length; i++) {
        for (let j = 0; j < stor.erentropliste.length; j++) {
          if (repchoisie[stor.ermankliste[i]].indexOf(stor.erentropliste[j][0]) !== -1) {
            repchois.push([textes.repch[stor.ermankliste[i]][repchoisie[stor.ermankliste[i]].indexOf(stor.erentropliste[j][0]) + 1], stor.erentropliste[j][1]])
          }
        }
      }
      for (let i = 0; i < repchois.length; i++) {
        stor.bull[stor.bull.length] = new BulleAide(stor.lesdiv.leszonesTra[repchois[i][1] + 1][1], repchois[i][0], { place: 5 })
      }
      for (let i = 0; i < stor.ermankliste.length; i++) {
        j3pAffiche(stor.lesdiv.explications, null, textes.repch[stor.ermankliste[i]][0] + '\n')
      }
      stor.etiquettes[0].disable()
      stor.etiquettes[0].detruit()
      stor.etiquettes.pop()
      stor.lesdiv.etik.innerHTML = ''
      const yy = j3pDiv(stor.lesdiv.etik, { contenu: '', style: this.styles.toutpetit.correction })
      j3pAffiche(yy, null, '<b><u>' + textes.cotitre + '</u></b> \n' + textes.le + textes.quadbase + ' ' + stor.nomquad + ' ' + textes.a + ': \n')
      for (let i = 0; i < stor.nbdonnees; i++) {
        j3pAffiche(yy, null, '• ' + textes.donnes[stor.donnees[i]] + '\n')
      }
      yy.classList.add('correction')
      stor.lesdiv.explications.classList.add('explique')

      this.typederreurs[2]++
      this.finCorrection('navigation', true)
      break // case "correction":
    }

    case 'navigation':

      if (this.sectionTerminee()) {
        // On détermine la phrase d’état renvoyée par la section
        this.parcours.pe = (ds.nbetapes * this.score) / ds.nbitems
        this.afficheBoutonSectionSuivante()
        this.focus('sectioncontinuer')
      } else {
        this.etat = 'enonce'
        this.afficheBoutonSuite()
        this.focus('boutoncontinuer')
      }
      // Obligatoire
      this.finNavigation()

      break // case "navigation":
  }
}
