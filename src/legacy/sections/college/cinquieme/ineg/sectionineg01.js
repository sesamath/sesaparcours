import { j3pAddElt, j3pAjouteBouton, j3pArrondi, j3pGetNewId, j3pGetRandomInt, j3pShowError, j3pShuffle } from 'src/legacy/core/functions'
import { j3pCreeFenetres, j3pAfficheCroixFenetres, j3pToggleFenetres } from 'src/legacy/core/functionsJqDialog'
import { j3pCreeSVG } from 'src/legacy/core/functionsSvg'
import ListeDeroulante from 'src/legacy/outils/listeDeroulante/ListeDeroulante'
import ZoneStyleMathquill1 from 'src/legacy/outils/zoneStyleMathquill/ZoneStyleMathquill1'
import { getPe, initCptPe } from 'src/legacy/sections/college/helperTommy'
import { addDefaultTable } from 'src/legacy/themes/table'
import { j3pAffiche } from 'src/lib/mathquill/functions'
import { getMtgCore } from 'src/lib/outils/mathgraph'
import { addMaj } from 'src/lib/utils/string'
import textesGeneriques from 'src/lib/core/textes'

const { cBien, cFaux, essaieEncore, reponseIncomplete, tempsDepasse } = textesGeneriques

/**
 * Ajouter une description ici
 * @fileOverview
 * @author Tommy Barroy <t.barroy@laposte.net>
 * @since 2017-10
*/

export const params = {
  outils: ['mathquill', 'calculatrice'],
  /**
   * Ce qui est paramétrable dans l’éditeur de graphe
   * @type {LegacySectionParamsParametres}
   */
  parametres: [
    ['nbrepetitions', 9, 'entier', 'Nombre de répétitions de la section'],
    ['nbchances', 2, 'entier', 'Nombre d’essais possibles.'],
    ['Entiers', true, 'boolean', '<u>true</u>:  Les longueurs sont des nombres entiers'],
    ['Calculatrice', true, 'boolean', '<u>true</u>:  Une calculatrice est disponible'],
    // le modèle gère la compatibilité ascendante avec l’ancien paramètre MiseEnPagePlusClaire
    ['theme', 'standard', 'liste', 'Modèle pour l’affichage', ['standard', 'zonesAvecImageDeFond']]

  ],
  // dans le cas d’une section QUANTITATIVE
  pe: [
    { pe_1: 'Comparaison' },
    { pe_2: 'Addition' }
  ]
}

/**
 * section ineg01
 * @this {Parcours}
 */
export default function main () {
  const me = this
  const ds = me.donneesSection
  const stor = me.storage
  let svgId = stor.svgId

  function creeLesDiv () {
    stor.lesdiv = {}
    stor.lesdiv.conteneur = j3pAddElt(me.zonesElts.MG, 'div', {
      style: me.styles.etendre('toutpetit.enonce', { padding: '10px' })
    })
    stor.lesdiv.consigneG = addDefaultTable(stor.lesdiv.conteneur, 1, 1)[0][0]
    const tt = addDefaultTable(stor.lesdiv.conteneur, 1, 3)
    stor.lesdiv.travail = tt[0][2]
    stor.lesdiv.figure = tt[0][0]
    tt[0][1].style.width = '10px'
    stor.lesdiv.explications = j3pAddElt(stor.lesdiv.conteneur, 'div')
    stor.lesdiv.solution = j3pAddElt(stor.lesdiv.conteneur, 'div')
    stor.lesdiv.calculatrice = j3pAddElt(me.zonesElts.MD, 'div', {
      style: me.styles.etendre('toutpetit.enonce', { padding: '10px' })
    })
    stor.lesdiv.correction = j3pAddElt(me.zonesElts.MD, 'div', {
      style: me.styles.etendre('toutpetit.enonce', { padding: '10px' })
    })
    stor.lesdiv.solution.style.color = me.styles.toutpetit.correction.color
    stor.lesdiv.explications.style.color = me.styles.cfaux
    me.zonesElts.MG.classList.add('fond')
  }
  function poseQuestion () {
    j3pAffiche(stor.lesdiv.consigneG, null, '<b> On veut déterminer si le triangle ' + stor.nomtri + ' est constructible. </b> \n')
    me.afficheBoutonValider()
    stor.tabCont = addDefaultTable(stor.lesdiv.travail, 4, 3)
    stor.tabCont[0][1].style.width = '30px'
    j3pAffiche(stor.tabCont[0][0], null, 'Dans le triangle ' + stor.nomtri + ' ,')
    const yui = addDefaultTable(stor.tabCont[0][0], 1, 2)
    const ll = ['Choisir', stor.nomcote1, stor.nomcote2, stor.nomcote3]
    stor.liste1 = ListeDeroulante.create(yui[0][0], ll, { centre: true })
    j3pAffiche(yui[0][1], null, '&nbsp; est le plus grand côté.')
    // Affboo();
    j3pAffiche(stor.lesdiv.figure, null, '<b><i>Schéma</i></b>')
    stor.encours = 'Plusgrand'
  }
  function poseQuestCompare () {
    stor.lesdiv.explications.classList.remove('explique')
    stor.lesdiv.solution.classList.remove('correction')
    stor.encours = 'compare'
    me.afficheBoutonValider()
    const fk = addDefaultTable(stor.tabCont[1][0], 1, 2)
    j3pAffiche(fk[0][0], null, 'Je compare &nbsp;')
    const ll = ['Choisir']
    ll.push(stor.nomlc1 + ' et ' + stor.nomlc2 + ' + ' + stor.nomlc3)
    ll.push(stor.nomlc2 + ' et ' + stor.nomlc3 + ' + ' + stor.nomlc1)
    ll.push(stor.nomlc3 + ' et ' + stor.nomlc1 + ' + ' + stor.nomlc2)
    stor.liste2 = ListeDeroulante.create(fk[0][1], ll, { centre: true })
  }
  function poseCalcul () {
    stor.lesdiv.explications.classList.remove('explique')
    stor.lesdiv.solution.classList.remove('correction')
    stor.encours = 'calcul'
    me.afficheBoutonValider()
    const tabghd = addDefaultTable(stor.tabCont[2][0], 1, 5)
    const ghju = addDefaultTable(tabghd[0][0], 1, 3)
    j3pAffiche(ghju[0][0], null, stor.noml3 + ' = &nbsp;')
    j3pAffiche(ghju[0][2], null, '&nbsp; cm ')
    stor.zgauche = new ZoneStyleMathquill1(ghju[0][1], { restric: '0123456789.,', enter: me.sectionCourante.bind(me) })
    tabghd[0][1].style.width = '5px'
    tabghd[0][3].style.width = '5px'
    tabghd[0][2].style.width = '1px'
    tabghd[0][2].style.background = '#000000'
    const fgddy = addDefaultTable(tabghd[0][4], 2, 2)
    j3pAffiche(fgddy[0][0], null, stor.noml2 + ' + ' + stor.noml1 + ' = &nbsp;')
    j3pAffiche(fgddy[1][0], null, stor.noml2 + ' + ' + stor.noml1 + ' = &nbsp;')
    const hj = addDefaultTable(fgddy[0][1], 1, 3)
    j3pAffiche(hj[0][1], null, '&nbsp; + &nbsp;')
    const hp = addDefaultTable(fgddy[1][1], 1, 2)
    j3pAffiche(hp[0][1], null, '&nbsp; cm ')
    stor.zdroite1 = new ZoneStyleMathquill1(hj[0][0], { restric: '0123456789.,', enter: me.sectionCourante.bind(me) })
    stor.zdroite2 = new ZoneStyleMathquill1(hj[0][2], { restric: '0123456789.,', enter: me.sectionCourante.bind(me) })
    stor.zdroite3 = new ZoneStyleMathquill1(hp[0][0], { restric: '0123456789.,', enter: me.sectionCourante.bind(me) })
  }
  function poseConclu () {
    stor.lesdiv.explications.classList.remove('explique')
    stor.lesdiv.solution.classList.remove('correction')
    stor.encours = 'concl'
    me.afficheBoutonValider()
    const yy = addDefaultTable(stor.tabCont[3][0], 2, 1)
    let ll = ['Choisir', stor.noml3 + '$ \\leq $' + stor.noml1 + ' + ' + stor.noml2, stor.noml3 + '$>$' + stor.noml1 + ' + ' + stor.noml2]
    stor.liste3 = ListeDeroulante.create(yy[0][0], ll, { centre: true })
    const yy2 = addDefaultTable(yy[1][0], 1, 2)
    j3pAffiche(yy2[0][0], null, ' donc le triangle ' + stor.nomtri + '&nbsp; ')
    ll = ['Choisir', ' est constructible. ', ' n’est pas constructible. ']
    stor.liste4 = ListeDeroulante.create(yy2[0][1], ll, { centre: true })
  }

  function boutonfen () {
    j3pToggleFenetres('Calculatrice')
  }

  function faisChoixExos () {
    const e = ds.lesExos.pop()
    e.sam = e.prop !== '-'
    return e
  }
  function faiFigure () {
    stor.lettres = []
    for (let i = 0; i < 3; i++) {
      addMaj(stor.lettres)
    }
    const lettres = stor.lettres
    stor.nomtri = lettres[0] + lettres[1] + lettres[2]
    stor.nomcote1 = '[' + lettres[0] + lettres[1] + ']'
    stor.nomcote2 = '[' + lettres[0] + lettres[2] + ']'
    stor.nomcote3 = '[' + lettres[1] + lettres[2] + ']'
    stor.nomlc1 = lettres[0] + lettres[1]
    stor.nomlc2 = lettres[0] + lettres[2]
    stor.nomlc3 = lettres[1] + lettres[2]

    /// determine longueurs et angles

    if (!ds.Entiers) {
      stor.l1 = j3pGetRandomInt(500, 5000)
      stor.l2 = j3pGetRandomInt(500, 5000)
      if (stor.Lexo.sam) {
        stor.l3 = j3pGetRandomInt(Math.max(stor.l1 + 1, stor.l2 + 1), stor.l1 + stor.l2)
        if (stor.Lexo.prop === '=') stor.l3 = stor.l1 + stor.l2
      } else {
        stor.l3 = j3pGetRandomInt(stor.l1 + stor.l2 + 1, stor.l1 + stor.l2 + 700)
      }

      stor.l1 = j3pArrondi(stor.l1 / 100, 2)
      stor.l2 = j3pArrondi(stor.l2 / 100, 2)
      stor.l3 = j3pArrondi(stor.l3 / 100, 2)
    } else {
      stor.l1 = j3pGetRandomInt(2, 50)
      stor.l2 = j3pGetRandomInt(2, 50)
      if (stor.Lexo.sam) {
        stor.l3 = j3pGetRandomInt(Math.max(stor.l1 + 1, stor.l2 + 1), stor.l1 + stor.l2)
        if (stor.Lexo.prop === '=') stor.l3 = stor.l1 + stor.l2
      } else {
        stor.l3 = j3pGetRandomInt(stor.l1 + stor.l2 + 1, stor.l1 + stor.l2 + 7)
      }
    }

    stor.lc1 = stor.l1
    stor.lc2 = stor.l2
    stor.lc3 = stor.l3
    stor.cotelong = stor.nomcote3
    stor.noml3 = stor.nomlc3
    stor.noml1 = stor.nomlc2
    stor.noml2 = stor.nomlc1
    stor.c1 = stor.nomcote1
    stor.c2 = stor.nomcote2
    stor.lepo = lettres[0]

    const txtFigure = 'TWF0aEdyYXBoSmF2YTEuMAAAABM+TMzNAAJmcv###wEA#wEAAAAAAAAAAANLAAACfwAAAQEAAAAAAAAAAQAAADT#####AAAAAQAKQ0NhbGNDb25zdAD#####AAJwaQAWMy4xNDE1OTI2NTM1ODk3OTMyMzg0Nv####8AAAABAApDQ29uc3RhbnRlQAkh+1RELRj#####AAAAAQAKQ1BvaW50QmFzZQD#####AQAAAAAOAAFVAMAkAAAAAAAAQBAAAAAAAAAAAAUAAEAv#nbItDlYQC#+dsi0OVj#####AAAAAQAUQ0Ryb2l0ZURpcmVjdGlvbkZpeGUA#####wEAAAAAEAAAAQAAAAEAAAABAT#wAAAAAAAA#####wAAAAEAD0NQb2ludExpZURyb2l0ZQD#####AQAAAAAOAAFWAMAAAAAAAAAAQBAAAAAAAAAAAAUAAUA##nbItDlYAAAAAv####8AAAABAAhDU2VnbWVudAD#####AQAAAAAQAAABAAAAAQAAAAEAAAAD#####wAAAAEAB0NNaWxpZXUA#####wEAAAAAEAAAAAAAAAAAAAAAQAgAAAAAAAAAAAUAAAAAAQAAAAP#####AAAAAgAMQ0NvbW1lbnRhaXJlAP####8BAAAAAAAAAAAAAAAAQBgAAAAAAAAAAAAAAAUMAAAAAAABAAAAAAAAAAEAAAAAAAAAAAABMf####8AAAABAAlDTG9uZ3VldXIA#####wAAAAEAAAADAAAAAgD#####AAAAAAAQAAAAQAgAAAAAAAAAAAAAAAAAAAAABwAAQFbAAAAAAABAfP4UeuFHrv####8AAAABAAdDQ2FsY3VsAP####8ABW1pbmkxAAEwAAAAAQAAAAAAAAAAAAAACQD#####AAVtYXhpMQADMzYwAAAAAUB2gAAAAAAA#####wAAAAEAFENJbXBsZW1lbnRhdGlvblByb3RvAP####8AB0N1cnNldXIAAAAFAAAABQAAAAMAAAAJAAAACgAAAAgAAAADAAAAAAsBAAAAABAAAAEAAAABAAAACAE#8AAAAAAAAAAAAAQBAAAACwAAAP8AEAAAAMAIAAAAAAAAP#AAAAAAAAAAAAUAAEBdQAAAAAAAAAAADP####8AAAABAAtDSG9tb3RoZXRpZQAAAAALAAAACP####8AAAABAApDT3BlcmF0aW9uA#####8AAAABAA9DUmVzdWx0YXRWYWxldXIAAAAJAAAADAEAAAANAAAACQAAAA0AAAAK#####wAAAAEAC0NQb2ludEltYWdlAAAAAAsBAAAAAA0AAk8xAMAQAAAAAAAAQBAAAAAAAAAAAAUAAAAADQAAAA4AAAALAAAAAAsAAAAIAAAADAMAAAAMAQAAAAE#8AAAAAAAAAAAAA0AAAAJAAAADAEAAAANAAAACgAAAA0AAAAJAAAADgAAAAALAQAAAAANAAJJMQDAAAAAAAAAAEAIAAAAAAAAAAAFAAAAAA0AAAAQAAAABQEAAAALAAAA#wAQAAABAAAAAgAAAAgAAAANAAAABAEAAAALAAAA#wEQAAJrMQDAAAAAAAAAAEAAAAAAAAAAAAABAAE#7c#c#c#c#gAAABL#####AAAAAgAPQ01lc3VyZUFic2Npc3NlAQAAAAsAAmExAAAADwAAABEAAAAT#####wAAAAEAD0NWYWxldXJBZmZpY2hlZQEAAAALAAAA#wAAAAAAAAAAAMAYAAAAAAAAAAAAAAATDwAB####AAAAAQAAAAIAAAABAAAAAAAAAAAAAAAAAgAAABQAAAAJAP####8AAkExAAJhMQAAAA0AAAAUAAAAAgD#####AQAA#wAQAAAAAAAAAAAAAABACAAAAAAAAAAABwABQGAAAAAAAABAX3hR64UeuAAAAAIA#####wEAAP8AEAAAAAAAAAAAAAAAQAgAAAAAAAAAAAcAAUBfwAAAAAAAQC3Cj1wo9cQAAAAFAP####8BAAD#ABAAAAEAAAACAAAAFwAAABgAAAAEAP####8BAAD#ABAAAAAAAAAAAAAAAEAIAAAAAAAAAAAHAAE#7Rsv75Au#gAAABn#####AAAAAQAJQ1JvdGF0aW9uAP####8AAAAXAAAADQAAABYAAAAOAP####8B#wAAABAAAAAAAAAAAAAAAEAIAAAAAAAAAAAHAAAAABoAAAAbAAAADgD#####Af8AAAAQAAAAAAAAAAAAAABACAAAAAAAAAAABwAAAAAYAAAAGwAAABEA#####wAAABcAAAABQF4AAAAAAAAAAAAOAP####8B#wAAABAAAAAAAAAAAAAAAEAIAAAAAAAAAAAHAAAAABwAAAAeAAAADgD#####AQAAfwAQAAAAAAAAAAAAAABACAAAAAAAAAAABwAAAAAdAAAAHgAAAA4A#####wH#AAAAEAAAAAAAAAAAAAAAQAgAAAAAAAAAAAcAAAAAHwAAAB4AAAAOAP####8B#wAAABAAAAAAAAAAAAAAAEAIAAAAAAAAAAAHAAAAACAAAAAeAAAABQD#####AAAAAAAQAAABAAAAAgAAABwAAAAfAAAABQD#####AAAAAAAQAAABAAAAAgAAAB8AAAAhAAAABQD#####AAAAAAAQAAABAAAAAgAAACEAAAAcAAAABgD#####AQAAAAAQAAAAAAAAAAAAAABACAAAAAAAAAAABwAAAAAiAAAAHf####8AAAABABZDRHJvaXRlUGVycGVuZGljdWxhaXJlAP####8BAAAAABAAAAEAAAACAAAAJgAAACX#####AAAAAgAJQ0NlcmNsZU9SAP####8BAAAAAAAAAgAAACYAAAABP+AAAAAAAAAA#####wAAAAEAEENJbnREcm9pdGVDZXJjbGUA#####wAAACcAAAAo#####wAAAAEAEENQb2ludExpZUJpcG9pbnQA#####wEAAAAAEAAAAAAAAAAAAAAAQAgAAAAAAAAAAAcAAgAAACkAAAAVAP####8BAAAAABAAAAAAAAAAAAAAAEAIAAAAAAAAAAAHAAEAAAApAAAADgD#####AQAAAAAQAAAAAAAAAAAAAABACAAAAAAAAAAABwAAAAArAAAAHgAAAA4A#####wEAAAAAEAAAAAAAAAAAAAAAQAgAAAAAAAAAAAcAAAAALAAAAB4AAAAHAP####8AAAAAAQABQQAAACAQAAAAAAABAAAAAQAAAAEAAAAAAAAAAAABQQAAAAcA#####wAAAH8BAAFCAAAAHRAAAAAAAAEAAAABAAAAAQAAAAAAAAAAAAFCAAAABwD#####AAAAfwEAAUMAAAAiEAAAAAAAAQAAAAEAAAABAAAAAAAAAAAAAUMAAAAHAP####8AfwB#AQACbDEAAAArEAAAAAAAAQAAAAEAAAABAAAAAAAAAAAAAmwxAAAABwD#####AH8AfwEAAmwyAAAALBAAAAAAAAEAAAABAAAAAQAAAAAAAAAAAAJsMgAAAAcA#####wB#AH8BAAJsMwAAAC0QAAAAAAABAAAAAQAAAAEAAAAAAAAAAAACbDMAAAAH##########8='
    svgId = j3pGetNewId()
    stor.svgId = svgId
    const yy = addDefaultTable(stor.lesdiv.figure, 1, 1)[0][0]
    j3pCreeSVG(yy, { id: svgId, width: 280, height: 270, style: { border: 'solid black 1px' } })
    stor.mtgAppLecteur.removeAllDoc()
    stor.mtgAppLecteur.addDoc(svgId, txtFigure, true)
    stor.mtgAppLecteur.giveFormula2(svgId, 'A1', String(j3pGetRandomInt(0, 359)))
    stor.mtgAppLecteur.setText(svgId, '#A', lettres[0])
    stor.mtgAppLecteur.setText(svgId, '#B', lettres[1])
    stor.mtgAppLecteur.setText(svgId, '#C', lettres[2])
    stor.mtgAppLecteur.setText(svgId, '#l1', stor.lc3 + ' cm')
    stor.mtgAppLecteur.setText(svgId, '#l2', stor.lc2 + ' cm')
    stor.mtgAppLecteur.setText(svgId, '#l3', stor.lc1 + ' cm')
    stor.mtgAppLecteur.calculateAndDisplayAll(true)
  }

  function initSection () {
    let i
    initCptPe(ds, stor)

    // Construction de la page
    me.construitStructurePage('presentation1bis')

    me.donneesSection.lesExos = []

    let lp = ['+', '+', '-', '-', '=']
    lp = j3pShuffle(lp)

    for (i = 0; i < me.donneesSection.nbitems; i++) {
      me.donneesSection.lesExos.push({ prop: lp[i % (lp.length)] })
    }
    me.donneesSection.lesExos = j3pShuffle(me.donneesSection.lesExos)
    me.typederreurs = [0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0]
    me.afficheTitre('Constructible ou non ?')
    // chargement mtg
    getMtgCore({ withMathJax: true })
      .then(
        // success
        (mtgAppLecteur) => {
          stor.mtgAppLecteur = mtgAppLecteur
          stor.listePourCalc = mtgAppLecteur.createList('TWF0aEdyYXBoSmF2YTEuMAAAABI+TMzNAAJmcv###wEA#wEAAAAAAAAAAAQsAAACvQAAAQEAAAAAAAAAAQAAAAP#####AAAAAQAKQ0NhbGNDb25zdAD#####AAJwaQAWMy4xNDE1OTI2NTM1ODk3OTMyMzg0Nv####8AAAABAApDQ29uc3RhbnRlQAkh+1RELRj#####AAAAAQAHQ0NhbGN1bAD#####AAFlAAZleHAoMSn#####AAAAAgAJQ0ZvbmN0aW9uBwAAAAE#8AAAAAAAAAAAAAIA#####wABeAABMAAAAAEAAAAAAAAAAP###############w==')
          enonceMain()
        },
        // failure
        (error) => {
          j3pShowError(error, { message: 'mathgraph n’est pas correctement chargé', mustNotify: true })
        })
      // plantage dans le code de success
      .catch(j3pShowError)
  }

  function yaReponse () {
    if (stor.encours === 'Plusgrand') {
      if (!stor.liste1.changed) {
        stor.liste1.focus()
        return false
      }
      return true
    }
    if (stor.encours === 'compare') {
      if (!stor.liste2.changed) {
        stor.liste2.focus()
        return false
      }
      return true
    }
    if (stor.encours === 'calcul') {
      if (stor.zgauche.reponse() === '') {
        stor.zgauche.focus()
        return false
      }
      if (stor.zdroite1.reponse() === '') {
        stor.zdroite1.focus()
        return false
      }
      if (stor.zdroite2.reponse() === '') {
        stor.zdroite2.focus()
        return false
      }
      if (stor.zdroite3.reponse() === '') {
        stor.zdroite3.focus()
        return false
      }
      return true
    }
    if (stor.encours === 'concl') {
      if (!stor.liste3.changed) {
        stor.liste3.focus()
        return false
      }
      if (!stor.liste4.changed) {
        stor.liste4.focus()
        return false
      }
      return true
    }
  }

  function isRepOk () {
    stor.errGrand = false
    stor.errComp = false
    stor.errCopie = false
    stor.errAdd = false
    stor.errDouble = false

    let ok = true
    let t1
    let t2
    let t3

    if (stor.encours === 'Plusgrand') {
      switch (stor.liste1.reponse) {
        case stor.nomcote1:
          stor.t1 = stor.lc1
          break
        case stor.nomcote2:
          stor.t1 = stor.lc2
          break
        case stor.nomcote3:
          stor.t1 = stor.lc3
          break
      }
      if (stor.t1 !== Math.max(stor.lc1, stor.lc2, stor.lc3)) {
        stor.errGrand = true
        stor.compteurPe1++
        stor.liste1.corrige(false)
        return false
      }
      return true
    }
    if (stor.encours === 'compare') {
      if (stor.liste2.reponse.substring(0, 2) !== stor.cotelong.substring(1, 3)) {
        stor.errComp = true
        stor.liste2.corrige(false)
        return false
      }
      return true
    }
    if (stor.encours === 'calcul') {
      t3 = [(stor.l3 + '').replace('.', ','), (stor.l2 + '').replace('.', ','), (stor.l1 + '').replace('.', ',')]
      t1 = stor.zgauche.reponse()
      if (t1 !== (stor.l3 + '').replace('.', ',')) {
        stor.zgauche.corrige(false)
        // pour plus tard, verif 0 inut
        if (t3.indexOf(t1) === -1) {
          stor.errCopie = true
        } else {
          stor.errInv = true
        }
        ok = false
      }
      t1 = stor.zdroite1.reponse()
      if ((t1 !== (stor.l2 + '').replace('.', ',')) && t1 !== (stor.l1 + '').replace('.', ',')) {
        stor.zdroite1.corrige(false)
        // pour plus tard, verif 0 inut
        if (t3.indexOf(t1) === -1) {
          stor.errCopie = true
        } else {
          stor.errInv = true
        }
        ok = false
      }
      t2 = stor.zdroite2.reponse()
      if ((t2 !== (stor.l2 + '').replace('.', ',')) && t2 !== (stor.l1 + '').replace('.', ',')) {
        stor.zdroite2.corrige(false)
        // pour plus tard, verif 0 inut
        if (t3.indexOf(t2) === -1) {
          stor.errCopie = true
        } else {
          stor.errInv = true
        }
        ok = false
      }
      if (t1 === t2) {
        if (stor.l1 !== stor.l2) {
          stor.errDouble = true
          stor.zdroite2.corrige(false)
          ok = false
        }
      }
      t2 = parseFloat(stor.zdroite3.reponse().replace(',', '.'))
      if (t2 !== j3pArrondi(stor.l1 + stor.l2, 2)) {
        stor.zdroite3.corrige(false)
        // pour plus tard, verif 0 inut
        if (ok) stor.errAdd = true
        ok = false
      }
      return ok
    }
    if (stor.encours === 'concl') {
      t1 = (stor.liste3.reponse.indexOf('>') !== -1)
      if ((t1 && stor.Lexo.sam) || (!t1 && !stor.Lexo.sam)) {
        stor.liste3.corrige(false)
        ok = false
        stor.compteurPe1++
      }
      t1 = (stor.liste4.reponse.indexOf('pas') !== -1)
      if ((t1 && stor.Lexo.sam) || (!t1 && !stor.Lexo.sam)) {
        stor.liste4.corrige(false)
        ok = false
      }
    }
    return ok
  }

  function desactiveAll () {
    if (stor.encours === 'Plusgrand') {
      stor.liste1.disable()
    }
    if (stor.encours === 'compare') {
      stor.liste2.disable()
    }
    if (stor.encours === 'calcul') {
      stor.zgauche.disable()
      stor.zdroite1.disable()
      stor.zdroite2.disable()
      stor.zdroite3.disable()
    }
    if (stor.encours === 'concl') {
      stor.liste3.disable()
      stor.liste4.disable()
    }
  }

  function passeToutVert () {
    if (stor.encours === 'Plusgrand') {
      stor.liste1.corrige(true)
    }
    if (stor.encours === 'compare') {
      stor.liste2.corrige(true)
    }
    if (stor.encours === 'calcul') {
      stor.zgauche.corrige(true)
      stor.zdroite1.corrige(true)
      stor.zdroite2.corrige(true)
      stor.zdroite3.corrige(true)
    }
    if (stor.encours === 'concl') {
      stor.liste3.corrige(true)
      stor.liste4.corrige(true)
    }
  }
  function barrelesfo () {
    if (stor.encours === 'calcul') {
      stor.zgauche.barreIfKo()
      stor.zdroite1.barreIfKo()
      stor.zdroite2.barreIfKo()
      stor.zdroite3.barreIfKo()
    }
    if (stor.encours === 'concl') {
      stor.liste3.barreIfKo()
      stor.liste4.barreIfKo()
    }
  }
  function affCorrFaux (isFin) {
    /// //affiche indic
    stor.lesdiv.explications.classList.remove('explique')
    stor.lesdiv.solution.classList.remove('correction')
    if (stor.errGrand) {
      j3pAffiche(stor.lesdiv.explications, null, 'Il y a un côté de longueur supérieure à ' + (stor.t1 + '').replace('.', ',') + ' cm  !\n')
      stor.liste1.corrige(false)
      stor.yaexplik = true
    }
    if (stor.errComp) {
      j3pAffiche(stor.lesdiv.explications, null, 'Il faut comparer la longueur du plus grand côté \nà la somme des deux autres !\n')
      stor.liste2.corrige(false)
      stor.yaexplik = true
    }
    if (stor.errCopie) {
      j3pAffiche(stor.lesdiv.explications, null, 'Une longueur n’est pas dans l’énoncé !\n')
      stor.yaexplik = true
    }
    if (stor.errInv) {
      j3pAffiche(stor.lesdiv.explications, null, 'Tu utilises la longueur d’un autre côté !\n')
      stor.yaexplik = true
    }
    if (stor.errDouble) {
      j3pAffiche(stor.lesdiv.explications, null, 'Tu écris deux fois la même longueur !\n')
      stor.yaexplik = true
    }
    if (stor.errAdd) {
      j3pAffiche(stor.lesdiv.explications, null, 'Une erreur de calcul !\n')
      stor.yaexplik = true
      stor.compteurPe2++
    }

    if (isFin) {
      // barre les faux
      barrelesfo()
      desactiveAll()
      stor.yaco = true
      // afficheco
      if (stor.encours === 'Plusgrand') {
        j3pAffiche(stor.lesdiv.solution, null, stor.cotelong + ' est le plus grand côté.')
        stor.liste1.barre()
      }
      if (stor.encours === 'compare') {
        j3pAffiche(stor.lesdiv.solution, null, 'Je compare ' + stor.noml3 + ' et ' + stor.noml2 + ' + ' + stor.noml1)
        stor.liste2.barre()
      }
      if (stor.encours === 'calcul') {
        j3pAffiche(stor.lesdiv.solution, null, stor.noml3 + ' = $' + (stor.l3 + '').replace('.', ',') + '$ cm &nbsp;&nbsp;et&nbsp;&nbsp; ' + stor.noml2 + ' + ' + stor.noml1 + ' = $' + ((stor.l1 + stor.l2) + '').replace('.', ',') + '$ cm.')
      }
      if (stor.encours === 'concl') {
        if (stor.Lexo.sam) {
          j3pAffiche(stor.lesdiv.solution, null, stor.noml3 + '$ \\leq $' + stor.noml1 + ' + ' + stor.noml2 + '\n')
          j3pAffiche(stor.lesdiv.solution, null, 'donc le triangle ' + stor.nomtri + ' est constructible.')
          if (stor.Lexo.prop === '=') {
            j3pAffiche(stor.lesdiv.solution, null, '\n' + stor.noml3 + '$ = $' + stor.noml1 + ' + ' + stor.noml2 + ', \ndonc le point ' + stor.lepo + ' appartient au segment ' + stor.cotelong + '.')
          }
        } else {
          j3pAffiche(stor.lesdiv.solution, null, stor.noml3 + '$ > $' + stor.noml1 + ' + ' + stor.noml2 + '\n')
          j3pAffiche(stor.lesdiv.solution, null, 'donc le triangle ' + stor.nomtri + ' n’est pas constructible.')
        }
      }
    } else { me.afficheBoutonValider() }
  }
  function enonceMain () {
    if (me.etapeCourante === 1) {
      me.videLesZones()
    }
    creeLesDiv()
    stor.Lexo = faisChoixExos()

    stor.lesdiv.solution.style.color = me.styles.toutpetit.correction.color
    stor.lesdiv.explications.style.color = me.styles.cfaux

    stor.lesdiv.travail.classList.add('travail')
    stor.lesdiv.consigneG.classList.add('enonce')
    stor.lesdiv.figure.classList.add('enonce')

    faiFigure()

    poseQuestion()
    if (ds.Calculatrice) {
      me.fenetresjq = [{ name: 'Calculatrice', title: 'Une calculatrice', left: 600, top: 10 }]
      j3pCreeFenetres(me)
      j3pAfficheCroixFenetres('Calculatrice')
      j3pAjouteBouton(stor.lesdiv.calculatrice, 'Calculatrice', 'MepBoutons', 'Calculatrice', boutonfen)
    }
    me.afficheBoutonValider()
    me.finEnonce()
  }

  switch (this.etat) {
    case 'enonce':
      if (me.debutDeLaSection) {
        initSection()
      } else {
        enonceMain()
      }
      break // case "enonce":

    case 'correction':
      // On teste si une réponse a été saisie
      stor.yaexplik = false
      stor.yaco = false
      stor.lesdiv.explications.innerHTML = ''
      stor.lesdiv.solution.innerHTML = ''

      if (yaReponse()) {
        // Bonne réponse
        if (isRepOk()) {
          stor.lesdiv.correction.style.color = this.styles.cbien
          stor.lesdiv.correction.innerHTML = cBien

          passeToutVert()
          desactiveAll()
          if (stor.encours === 'Plusgrand') {
            poseQuestCompare()
            me.cacheBoutonSuite()
            return
          }
          if (stor.encours === 'compare') {
            poseCalcul()
            me.cacheBoutonSuite()
            return
          }
          if (stor.encours === 'calcul') {
            poseConclu()
            me.cacheBoutonSuite()
            return
          }
          if (stor.encours === 'concl') {
            if (stor.Lexo.prop === '=') {
              j3pAffiche(stor.lesdiv.solution, null, '\n' + stor.noml3 + '$ = $' + stor.noml1 + ' + ' + stor.noml2 + ' , \ndonc le point ' + stor.lepo + ' appartient au segment ' + stor.cotelong + '.')
            }
          }
          stor.encours = ''

          this.score++
          this.typederreurs[0]++
          this.cacheBoutonValider()
          this.etat = 'navigation'
          this.sectionCourante()
        } else {
          // Pas de bonne réponse
          stor.lesdiv.correction.style.color = this.styles.cfaux
          stor.lesdiv.correction.innerHTML = cFaux
          if (this.isElapsed) {
            // A cause de la limite de temps :
            stor.lesdiv.correction.innerHTML += tempsDepasse + '<BR>'
            this.typederreurs[10]++
            affCorrFaux(true)
            stor.yaco = true
            this.sectionCourante('navigation')
          } else {
            // Réponse fausse :
            if (me.essaiCourant < me.donneesSection.nbchances) {
              stor.lesdiv.correction.innerHTML += '<br>' + essaieEncore

              affCorrFaux(false)

              // affCorrFaux.call(this, false)
              this.typederreurs[1]++
            } else {
              // Erreur au nème essai
              affCorrFaux(true)
              stor.yaco = true

              if (stor.encours === 'compare') {
                this.score = j3pArrondi(this.score + 0.1, 1)
              }
              if (stor.encours === 'calcul') {
                this.score = j3pArrondi(this.score + 0.4, 1)
              }
              if (stor.encours === 'concl') {
                this.score = j3pArrondi(this.score + 0.7, 1)
              }

              this.typederreurs[2]++
              this.sectionCourante('navigation')
            }
          }
        }
      } else {
        // pas de réponse
        stor.lesdiv.correction.style.color = this.styles.cfaux
        stor.lesdiv.correction.innerHTML = reponseIncomplete
        this.afficheBoutonValider()
      }

      if (stor.yaexplik) {
        stor.lesdiv.explications.classList.add('explique')
      }
      if (stor.yaco) {
        stor.lesdiv.solution.classList.add('correction')
      }

      // Obligatoire
      this.finCorrection()
      break // case "correction":

    case 'navigation':
      if (this.sectionTerminee()) {
        this.parcours.pe = getPe(ds, stor)
      } else {
        this.etat = 'enonce'
      }
      this.finNavigation(true)
      break // case "navigation":
  }
}
