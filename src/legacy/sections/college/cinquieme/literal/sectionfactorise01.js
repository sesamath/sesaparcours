import { j3pAddElt, j3pAjouteBouton, j3pArrondi, j3pClone, j3pDetruit, j3pEmpty, j3pGetRandomBool, j3pGetRandomInt, j3pNotify, j3pPGCD, j3pShowError, j3pShuffle } from 'src/legacy/core/functions'
import { j3pAffiche } from 'src/lib/mathquill/functions'
import { addDefaultTable, addDefaultTableDetailed } from 'src/legacy/themes/table'
import ZoneStyleMathquill3 from 'src/legacy/outils/zoneStyleMathquill/ZoneStyleMathquill3'
import { j3pCreeFenetres, j3pAfficheCroixFenetres, j3pToggleFenetres } from 'src/legacy/core/functionsJqDialog'
import BrouillonCalculs from 'src/legacy/outils/brouillon/BrouillonCalculs'
import { affichefois } from 'src/legacy/outils/zoneStyleMathquill/functions'
import textesGeneriques from 'src/lib/core/textes'

const { cBien, cFaux, essaieEncore, reponseIncomplete, tempsDepasse } = textesGeneriques

/**
 * Ajouter une description ici
 * @fileOverview
 * @author Tommy Barroy <t.barroy@laposte.net>
 * @since 2017-10
 */
export const params = {
  outils: ['mathquill', 'calculatrice'],
  /**
   * Ce qui est paramétrable dans l’éditeur de graphe
   * @type {LegacySectionParamsParametres}
   */
  parametres: [
    ['nbrepetitions', 9, 'entier', 'Nombre de répétitions de la section'],
    ['nbchances', 2, 'entier', 'Nombre d’essais possibles.'],
    ['facteur', true, 'boolean', '<u>true</u>: L’élève doit trouver apparaitre le facteur commun.'],
    ['Detail', true, 'boolean', '<u>true</u>: Il faut ré écrire les produits avant de factoriser.'],
    ['Max', true, 'boolean', '<u>true</u>: L’élève doit factoriser au maximum.'],
    ['carre', 'oui', 'liste', '<u>oui</u>:  présence de x².', ['oui', 'non', 'les deux']],
    ['nombres', 'entiers', 'liste', '<u>entiers</u>:  Les coefficients sont entiers. <br><br> <u>fractions</u>: Les coeffiients sont des fractions.', ['entiers', 'fractions', 'les deux']],
    ['Positifs', true, 'boolean', '<u>true</u>:  Les coefficients sont positifs'],
    ['Calculatrice', true, 'boolean', '<u>true</u>:  Une calculatrice est disponible'],
    // le modèle gère la compatibilité ascendante avec l’ancien paramètre MiseEnPagePlusClaire
    ['theme', 'standard', 'liste', 'Modèle pour l’affichage', ['standard', 'zonesAvecImageDeFond']]

  ]
}

/**
 * section factorise01
 * @this {Parcours}
 */
export default function main () {
  const me = this
  const ds = me.donneesSection
  const stor = me.storage

  function creeLesDiv () {
    stor.lesdiv = {}
    stor.lesdiv.conteneur = j3pAddElt(me.zonesElts.MG, 'div', {
      style: me.styles.etendre('toutpetit.enonce', { padding: '10px' })
    })
    stor.lesdiv.consigneG = addDefaultTable(stor.lesdiv.conteneur, 1, 1)[0][0]
    stor.lesdiv.etape = addDefaultTable(stor.lesdiv.conteneur, 1, 1)[0][0]
    stor.lesdiv.travail = addDefaultTable(stor.lesdiv.conteneur, 1, 1)[0][0]
    stor.lesdiv.explications = j3pAddElt(stor.lesdiv.conteneur, 'div')
    stor.lesdiv.solution = j3pAddElt(stor.lesdiv.conteneur, 'div')
    stor.lesdiv.calculatrice = j3pAddElt(me.zonesElts.MD, 'div', {
      style: me.styles.etendre('toutpetit.enonce', { padding: '10px' })
    })
    stor.lesdiv.aide = j3pAddElt(me.zonesElts.MD, 'div', {
      style: me.styles.etendre('toutpetit.enonce', { padding: '10px' })
    })
    stor.lesdiv.correction = j3pAddElt(me.zonesElts.MD, 'div', {
      style: me.styles.etendre('toutpetit.enonce', { padding: '10px' })
    })
    stor.lesdiv.etape.style.color = me.styles.toutpetit.correction.color
    stor.lesdiv.solution.style.color = me.styles.toutpetit.correction.color
    stor.lesdiv.explications.style.color = me.styles.cfaux
    me.zonesElts.MG.classList.add('fond')
  }
  function faisChoixExos () {
    const e = ds.lesExos.pop()
    let num, den
    let ok
    let cmpt = 0
    do {
      cmpt++
      num = j3pGetRandomInt(2, 9)
      den = premsavec(num)
      stor.a = { num, den }
      num = j3pGetRandomInt(1, 9)
      den = premsavec(num)
      stor.b = { num, den }
      num = j3pGetRandomInt(1, 9)
      den = premsavec(num)
      stor.c = { num, den }
      num = premsavec(stor.c.num)
      den = premsavec(num)
      stor.d = { num, den }
      ok = j3pPGCD(stor.a.num * stor.c.num, stor.a.den * stor.c.den) === 1
      ok = ok && j3pPGCD(stor.a.num * stor.d.num, stor.a.den * stor.d.den) === 1
      ok = ok && j3pPGCD(stor.c.den, stor.d.den) === 1
      ok = ok && j3pPGCD(stor.d.num, stor.c.num) === 1
    } while (!ok && (cmpt < 1000))
    stor.restric = '0123456789/'
    if (e.type === 'ent') {
      stor.a.den = stor.b.den = stor.c.den = stor.d.den = 1
      stor.restric = '0123456789'
    }
    if (!ds.Positifs) {
      if (j3pGetRandomBool()) {
        stor.a.num = -stor.a.num
      } else {
        if (j3pGetRandomBool()) {
          stor.c.num = -stor.c.num
        } else {
          if (j3pGetRandomBool()) stor.b.num = -stor.b.num
          if (j3pGetRandomBool()) stor.d.num = -stor.d.num
        }
      }
      stor.restric += '()-'
    }
    if (!e.parent) {
      stor.b.num = 0
    }
    stor.a.yav = (e.carre === 'car')
    stor.b.yav = !stor.a.yav
    stor.c.yav = j3pGetRandomBool()
    stor.d.yav = !stor.c.yav

    const fg = 'xyzabt'
    stor.nomvar = fg[j3pGetRandomInt(0, 5)]
    stor.restric += stor.nomvar
    stor.t1 = []
    stor.t2 = []
    if (stor.b.num === 0) {
      stor.t1.push(cal(stor.a, stor.c))
      stor.t1.push(cal(stor.a, stor.d))
    } else {
      // on verra
    }
    return e
  }

  function initSection () {
    let i
    me.validOnEnter = false // ex donneesSection.touche_entree

    // Construction de la page
    me.construitStructurePage('presentation1bis')

    ds.lesExos = []

    let lp = []
    if ((ds.nombres === 'entiers') || (ds.nombres === 'les deux')) lp.push('ent')
    if ((ds.nombres === 'fractions') || (ds.nombres === 'les deux')) lp.push('frac')
    lp = j3pShuffle(lp)
    for (i = 0; i < ds.nbitems; i++) {
      ds.lesExos.push({ type: lp[i % lp.length] })
    }
    ds.lesExos = j3pShuffle(ds.lesExos)

    lp = []
    if ((ds.carre === 'oui') || (ds.carre === 'les deux')) lp.push('car')
    if ((ds.carre === 'non') || (ds.carre === 'les deux')) lp.push('pacar')
    lp = j3pShuffle(lp)
    for (i = 0; i < ds.lesExos.length; i++) {
      ds.lesExos[i].carre = lp[i % lp.length]
    }
    ds.lesExos = j3pShuffle(ds.lesExos)
    for (i = 0; i < ds.lesExos.length; i++) {
      // on avait un ds.parentheses mais il n'est pas dans les params et valait toujours non
      ds.lesExos[i].parent = false
    }
    ds.lesExos = j3pShuffle(ds.lesExos)
    me.typederreurs = [0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0]
    me.afficheTitre('Factoriser')
    enonceMain()
  }

  function poseQuestion () {
    stor.fact = undefined
    stor.brouill = undefined
    stor.niv = 0

    const tabsisi = addDefaultTable(stor.lesdiv.consigneG, 1, 3)
    j3pAffiche(tabsisi[0][0], null, '<b> On veut factoriser </b> l’expression &nbsp;')
    j3pAffiche(tabsisi[0][1], null, '$ A = $')
    j3pAffiche(tabsisi[0][2], null, '$' + affichemonexpfact(false) + '$')
    stor.tabGcont = addDefaultTable(stor.lesdiv.travail, 2, 1)
    if (ds.facteur) {
      faitFacteur()
    } else if (ds.Detail) {
      faitDetail()
    } else {
      faitResult()
    }
    stor.lesdiv.explications.classList.remove('explique')
    stor.lesdiv.solution.classList.remove('correction')
  }
  function faitFacteur () {
    stor.encours = 'Facteur'

    j3pAffiche(stor.lesdiv.etape, null, '<b><u>Etape</u>: Trouver le facteur commun.</b>')
    stor.nunuTAb = addDefaultTable(stor.tabGcont[0][0], 2, 1)
    stor.ppTab = addDefaultTable(stor.nunuTAb[0][0], 1, 2)
    let bibi = 'un'
    if (ds.Max) bibi = '"le plus grand"'
    j3pAffiche(stor.ppTab[0][0], null, 'Donne ' + bibi + ' facteur commun aux deux termes: &nbsp; \n')
    stor.zone1 = new ZoneStyleMathquill3(stor.ppTab[0][1], { restric: stor.restric, bloqueFraction: '+-²()' + stor.nomvar, enter: me.sectionCourante.bind(me) })
  }
  function faitTab () {
    stor.tabCont = addDefaultTableDetailed(stor.tabGcont[1][0], 3, 4)
    for (let i = 0; i < 3; i++) {
      for (let j = 0; j < 4; j++) stor.tabCont.cells[i][j].style.padding = 0
    }
    j3pAffiche(stor.tabCont.cells[0][0], null, '$A$')
    j3pAffiche(stor.tabCont.cells[0][1], null, '$=$')
    j3pAffiche(stor.tabCont.cells[0][2], null, '$' + affichemonexpfact(false) + '$')
  }
  function faitDetail () {
    faitTab()

    if (stor.fact === undefined) stor.fact = j3pClone(stor.a)
    stor.encours = 'Detail'
    j3pEmpty(stor.lesdiv.etape)
    j3pAffiche(stor.lesdiv.etape, null, '<b><u>Etape</u>: Ecrire les produits.</b>')
    j3pAffiche(stor.tabCont.cells[1][0], null, '$A$')
    j3pAffiche(stor.tabCont.cells[1][1], null, '$=$')
    const zaziTab = addDefaultTableDetailed(stor.tabCont.cells[1][2], 1, 3)
    stor.tabCont.cells[1][2].style.padding = '0px 5px 5px 5px'
    zaziTab.cells[0][0].style.padding = 0
    zaziTab.cells[0][1].style.padding = 0
    zaziTab.cells[0][2].style.padding = 0
    const t2 = affichefois(zaziTab.cells[0][0])
    const t3 = affichefois(zaziTab.cells[0][2])
    stor.tabCont.cells[1][2].style.border = '1px solid'
    stor.tabCont.cells[1][2].style.background = '#fff'
    stor.zone2 = new ZoneStyleMathquill3(t2[2], { restric: stor.restric + '²', bloqueFraction: '+-²()' + stor.nomvar, enter: me.sectionCourante.bind(me) })
    stor.zone4 = new ZoneStyleMathquill3(t3[2], { restric: stor.restric + '²', bloqueFraction: '+-²()' + stor.nomvar, enter: me.sectionCourante.bind(me) })
    j3pAffiche(t2[0], null, affichemonexpfact2(true) + '$')
    j3pAffiche(t3[0], null, affichemonexpfact2(true) + '$')
    if (affichemonexpfact2(true)[1] !== '-') j3pAffiche(zaziTab.cells[0][1], null, '$+$')
    me.afficheBoutonValider()
  }

  function faitResult () {
    j3pEmpty(stor.lesdiv.etape)

    j3pAffiche(stor.lesdiv.etape, null, '<b><u>Etape</u>: Ecrire l’expression factorisée.</b>')
    if (!ds.Detail) faitTab()
    stor.brouill = new BrouillonCalculs({
      caseBoutons: stor.tabCont.cells[1][0],
      caseEgal: stor.tabCont.cells[1][1],
      caseCalculs: stor.tabCont.cells[1][2],
      caseApres: stor.tabCont.cells[1][3]
    }, {
      limite: 30,
      limitenb: 20,
      restric: '0123456789*/,.+-',
      hasAutoKeyboard: false,
      lmax: 1
    })
    faitResult2()
  }
  function faitResult2 () {
    stor.encours = 'simpl'
    if (stor.fact === undefined) stor.fact = j3pClone(stor.a)
    j3pEmpty(stor.lesdiv.etape)
    j3pAffiche(stor.lesdiv.etape, null, '<b><u>Etape</u>: Ecrire l’expression factorisée.</b>')
    j3pAffiche(stor.tabCont.cells[2][0], null, '$A$')
    j3pAffiche(stor.tabCont.cells[2][1], null, '$=$')
    if (ds.Aide) {
      const tabAi = addDefaultTable(stor.tabCont.cells[2][2], 1, 4)
      j3pAffiche(tabAi[0][0], null, '$' + regroupe(affichemoibien(stor.a, false), affichemoibien(stor.b, false)) + '$')
      j3pAffiche(tabAi[0][1], null, '$($')
      j3pAffiche(tabAi[0][3], null, '$)$')
      stor.zoneSimpl = new ZoneStyleMathquill3(tabAi[0][2], { restric: stor.restric + '+-²', bloqueFraction: '+-²()' + stor.nomvar, enter: me.sectionCourante.bind(me) })
    } else {
      stor.zoneSimpl = new ZoneStyleMathquill3(stor.tabCont.cells[2][2], { restric: stor.restric + '+-()²', bloqueFraction: '+-²()' + stor.nomvar, enter: me.sectionCourante.bind(me) })
    }
    me.afficheBoutonValider()
  }

  function affichemonexp (bool) {
    const a = affichemoibien(stor.a, stor.lx1 === 1, false)
    const yo = a !== ''
    const b = affichemoibien(stor.b, stor.lx1 === 2, yo)
    const c = affichemoibien(stor.c, stor.lx2 === 1, false)
    const g = affichemoibien(stor.d, stor.lx2 === 2, true)
    let ret = ((a !== '') && (b !== '')) ? '$(' + a + b + ')' : '$' + a + b
    if (bool) return ret
    if ((c !== '') && (g !== '')) {
      ret += '(' + c + g + ')$'
    } else {
      ret = c + g + '$'
    }
    return ret
  }
  function affichemonexpfact () {
    let na, nb, pg, afg
    if (stor.a.num === 0) {
      na = { num: stor.b.num * stor.c.num, den: stor.b.den * stor.c.den }
      pg = j3pPGCD(Math.abs(na.num), Math.abs(na.den))
      na.num = Math.round(na.num / pg)
      na.den = Math.round(na.den / pg)
      na.yav = stor.b.yav || stor.c.yav
      na.car = stor.b.yav && stor.c.yav

      nb = { num: stor.b.num * stor.d.num, den: stor.b.den * stor.d.den }
      pg = j3pPGCD(Math.abs(nb.num), Math.abs(nb.den))
      nb.num = Math.round(nb.num / pg)
      nb.den = Math.round(nb.den / pg)
      nb.yav = stor.b.yav || stor.d.yav
      nb.car = stor.b.yav && stor.d.yav
    } else if (stor.b.num === 0) {
      na = { num: stor.a.num * stor.c.num, den: stor.a.den * stor.c.den }
      pg = j3pPGCD(Math.abs(na.num), Math.abs(na.den))
      na.num = Math.round(na.num / pg)
      na.den = Math.round(na.den / pg)
      na.yav = stor.a.yav || stor.c.yav
      na.car = stor.a.yav && stor.c.yav

      nb = { num: stor.a.num * stor.d.num, den: stor.a.den * stor.d.den }
      pg = j3pPGCD(Math.abs(nb.num), Math.abs(nb.den))
      nb.num = Math.round(nb.num / pg)
      nb.den = Math.round(nb.den / pg)
      nb.yav = stor.a.yav || stor.d.yav
      nb.car = stor.a.yav && stor.d.yav
    }
    afg = affichemoibien(na, false)
    afg += affichemoibien(nb, true)
    return afg
  }
  function affichemonexpfact2 () {
    if (stor.fact !== undefined) {
      return '$' + affichemoibien(stor.fact)
    } else {
      return affichemonexp(true)
    }
  }
  function affichemoibien (f, fautsigne) {
    const x = f.yav
    const car = f.car
    let j = ''
    if (car === true) j = '²'
    if (f.num === 0) return ''
    let ret = ''
    if (f.den === 1) {
      if (f.num < 0) { ret += '-' } else if (fautsigne) { ret += '+' }
      if (x && Math.abs(f.num) === 1) {
        return ret + stor.nomvar + j
      } else {
        ret += Math.abs(f.num)
      }
    } else {
      if (f.num < 0) { ret += '-' } else if (fautsigne) { ret += '+' }
      ret += '\\frac{' + Math.abs(f.num) + '}{' + f.den + '}'
    }
    if (x) ret += stor.nomvar + j
    return ret
  }
  function boutonfen () {
    j3pToggleFenetres('Calculatrice')
  }

  function premsavec (x) {
    if (typeof x !== 'number' || x <= 0) throw TypeError('nombre invalide')
    let ret
    do {
      ret = j3pGetRandomInt(2, 9)
    } while (j3pPGCD(x, ret) !== 1)
    return ret
  }

  function existedonc (q) {
    let i
    let oki = false
    for (i = 0; i < 2; i++) {
      if (egalite(q, stor.t1[i])) {
        oki = true
        stor.ya[i] = true
      }
    }
    return oki
  }
  function egalite (a, b) {
    if (a.puis !== b.puis) return false
    return (a.num * b.den === a.den * b.num)
  }
  function recupe (s) {
    s = s.replace(')', '').replace('(', '')
    let yav = false
    let car = false
    if (s.indexOf(stor.nomvar) !== -1) {
      yav = true
    }
    if (s.indexOf('²') !== -1) car = true
    s = s.replace('²', '').replace(stor.nomvar, '')
    if ((s.indexOf('²') !== -1) || (s.indexOf(stor.nomvar) !== -1)) return { num: 0, den: 0, yav: false, car: false }
    const t = extnumden(s, yav)
    return { num: t.num, den: t.den, yav, car }
  }

  function extnumden (s) {
    let neg = 1
    if (s.indexOf('-') !== -1) neg = -1
    s = s.replace('-', '')
    if (s.indexOf('frac') === -1) {
      if (s === '') return { num: neg, den: 1 }
      return { num: parseFloat(s) * neg, den: 1 }
    } else {
      return { num: parseFloat(s.substring(6, s.indexOf('}{'))) * neg, den: parseFloat(s.substring(s.indexOf('}{') + 2, s.length - 1)) }
    }
  }
  function bienecrit (q, osef) {
    // verif parent
    let yaeupar = false
    if ((q.indexOf('(') !== -1) || (q.indexOf(')') !== -1)) {
      if (q.indexOf('(') !== q.lastIndexOf('(')) return false
      if (q.indexOf(')') !== q.lastIndexOf(')')) return false
      if (q.indexOf('(') !== 0) return false
      if (q[q.length - 1] === stor.nomvar) {
        if (q.indexOf(')') !== q.length - 2) return false
      } else if (q.indexOf(')') !== q.length - 1) { return false }
      q = q.replace('(', '').replace(')', '')
      yaeupar = true
    }
    if ((q.indexOf('-') !== -1) && (q.indexOf('-') !== 0)) return false
    if (osef !== true) {
      if (!yaeupar && (q.indexOf('-') === 0)) return false
    }
    q = q.replace('-', '')
    if (Array.isArray(q)) {
      if (q.length > 2) return false
      if (q.length === 2) {
        if (q[1] !== stor.nomvar) return false
      }
    } else {
      if (q.indexOf(stor.nomvar) !== -1) {
        if (q.replace(stor.nomvar, '').indexOf(stor.nomvar) !== -1) return false
        if ('0123456789'.indexOf(q[q.length - 1]) !== -1) {
          return false
        }
        if (q.substring(0, q.length - 1) === '1') return false
      }
    }
    return true
  }

  function yaReponse () {
    if (stor.encours === 'Facteur') {
      if (stor.zone1.reponse() === '') {
        stor.zone1.focus()
        return false
      }
      return true
    }
    if (stor.encours === 'Detail') {
      if (stor.zone2.reponse() === '') {
        stor.zone2.focus()
        return false
      }
      if (stor.zone4.reponse() === '') {
        stor.zone4.focus()
        return false
      }
      return true
    }
    if (stor.encours === 'simpl') {
      if (stor.zoneSimpl.reponse() === '') {
        stor.zoneSimpl.focus()
        return false
      }
      return true
    }
  }
  function isRepOk () {
    let i, i0
    stor.errFoisauMil = false
    stor.errDiv = false
    stor.err1 = false
    stor.errMax = false
    stor.errSigneF = false
    stor.errSigneM = false
    stor.errPAsfact = false
    stor.errRetrouvePas = false
    stor.errDiv2 = false
    stor.errCal = false
    stor.errYapa = false
    stor.errProd = false

    let ok = true
    let t1, t2, t3
    let n3, n2, s1, cs

    if (stor.encours === 'Facteur') {
      stor.fact = j3pClone(stor.a)
      t1 = stor.zone1.reponse()
      if (!bienecrit(t1, true)) {
        stor.errFoisauMil = true
        stor.zone1.corrige(false)
        return false
      }
      t2 = recupe(t1)
      if (!divise(t2)) {
        stor.errDiv = true
        stor.zone1.corrige(false)
        return false
      }
      if (t2.num === 1 && !t2.yav) {
        stor.err1 = true
        stor.zone1.corrige(false)
        return false
      }
      if ((stor.a.num > 0) && (t2.num < 0)) {
        stor.errSigneF = true
        stor.zone1.corrige(false)
        return false
      }
      if (ds.Max) {
        if ((stor.a.num < 0) && (t2.num > 0)) {
          stor.errSigneM = true
          stor.zone1.corrige(false)
          return false
        }
        if (!estmax(t2)) {
          stor.errMax = true
          stor.zone1.corrige(false)
          return false
        }
      }
      stor.fact = j3pClone(t2)
      return true
    }
    if (stor.encours === 'Detail') {
      stor.lesassos = []
      stor.signes = []
      stor.facteur = []

      stor.facteur.push({ f1: affichemonexpfact2().replace('$', ''), f2: stor.zone2.reponse() })
      stor.facteur.push({ f1: affichemonexpfact2().replace('$', ''), f2: stor.zone4.reponse() })

      // verif chaque facteur bien ecrit
      // soit nb , soit nb + lettre
      // verfi nb devant
      for (i = 0; i < stor.facteur.length; i++) {
        if (!bienecrit(stor.facteur[i].f2)) {
          stor.errFoisauMil = true
          if (i === 0) {
            stor.zone2.corrige(false)
          } else {
            stor.zone4.corrige(false)
          }
          ok = false
        }
      }
      if (!ok) return false
      stor.facteur[0].f1 = recupe(stor.facteur[0].f1)
      stor.facteur[0].f2 = recupe(stor.facteur[0].f2)
      stor.facteur[1].f1 = recupe(stor.facteur[1].f1)
      stor.facteur[1].f2 = recupe(stor.facteur[1].f2)

      // verif enrpodit je retouve données
      stor.ya = [false, false]
      for (i = 0; i < stor.facteur.length; i++) {
        n3 = cal(stor.facteur[i].f1, stor.facteur[i].f2)
        if (!existedonc(n3)) {
          stor.errProd = true
          ok = false
          if (i === 0) {
            stor.zone2.corrige(false)
          } else {
            stor.zone4.corrige(false)
          }
        }
      }
      if (!ok) return false
      // verif ya pas autre chose que produit attendu

      // verif presence des prods attendus
      ok = stor.ya[0] && stor.ya[1]
      if (!ok) {
        stor.errYapa = true
        return false
      }
      // verif les signes

      return ok
    }
    if (stor.encours === 'simpl') {
      // verif ya parenthese correct
      t1 = stor.zoneSimpl.reponse()
      if ((t1.indexOf('(') === -1) && (t1.indexOf(')') === -1)) {
        stor.errPAsfact = true
        stor.zoneSimpl.corrige(false)
        return false
      }
      if ((t1.indexOf('(') === -1) || (t1.indexOf(')') === -1)) {
        stor.errFoisauMil = true
        stor.zoneSimpl.corrige(false)
        return false
      }
      if ((t1.indexOf('(') !== t1.lastIndexOf('(')) || (t1.indexOf(')') !== t1.lastIndexOf(')'))) {
        stor.errFoisauMil = true
        stor.zoneSimpl.corrige(false)
        return false
      }
      if (t1.indexOf('(') > t1.indexOf(')')) {
        stor.errFoisauMil = true
        stor.zoneSimpl.corrige(false)
        return false
      }

      // separe gauche et droite
      t2 = t1.substring(0, t1.indexOf('('))
      t3 = t1.substring(t1.indexOf('(') + 1, t1.indexOf(')'))
      t1 = t1.substring(t1.indexOf(')') + 1)
      if ((t2 !== '') && (t1 !== '')) {
        stor.errFoisauMil = true
        stor.zoneSimpl.corrige(false)
        return false
      }
      if (t2 === '') {
        stor.errFoisauMil = true
        stor.zoneSimpl.corrige(false)
        return false
      }
      if (t1 !== '') t2 = t1

      // verif que 1 c fact
      if ((t2.indexOf('+') !== -1) && (t2.indexOf('+') !== 0)) {
        stor.errPAsfact = true
        stor.zoneSimpl.corrige(false)
        return false
      }
      if (!bienecrit(t2)) {
        stor.errFoisauMil = true
        stor.zoneSimpl.corrige(false)
      }
      if (ds.facteur || ds.Detail) {
        t1 = affichemonexpfact2().replace('$', '')
        if (t1 !== t2) {
          stor.errRetrouvePas = true
          stor.zoneSimpl.corrige(false)
          return false
        }
        t2 = recupe(t2)
      } else {
        // faut verif que le facteur c’est good
        t2 = recupe(t2)
        if (!divise(t2)) {
          stor.errDiv2 = true
          stor.zoneSimpl.corrige(false)
          return false
        }
        if (t2.num === 1 && !t2.yav) {
          stor.err1 = true
          stor.zoneSimpl.corrige(false)
          return false
        }
        if ((stor.a.num > 0) && (t2.num < 0)) {
          stor.errSigneF = true
          stor.zoneSimpl.corrige(false)
          return false
        }
        if (ds.Max) {
          if ((stor.a.num < 0) && (t2.num > 0)) {
            stor.errSigneM = true
            stor.zoneSimpl.corrige(false)
            return false
          }
          if (!estmax(t2)) {
            stor.errMax = true
            stor.zoneSimpl.corrige(false)
            return false
          }
        }
        stor.fact = j3pClone(t2)
      }

      // decoupe l’autre en fonction des signes
      n2 = t3.split('-').length - 1
      n3 = t3.split('+').length - 1
      if (((n2 + n3) > 2) || (n2 + n3 === 0) || (t3[0] === '+') || (t3.length < 2)) {
        stor.errFoisauMil = true
        stor.zoneSimpl.corrige(false)
        return false
      }
      t1 = decoupe(t3)
      t3 = []
      // fo verif alternance signe exp
      cs = true
      if (t1[0] === '-') { s1 = -1; i0 = 1; cs = true } else { s1 = 1; i0 = 0; cs = true }
      for (i = i0; i < t1.length; i++) {
        if ((t1[i] === '+') || (t1[i] === '-')) {
          if (cs) {
            stor.errFoisauMil = true
            stor.zoneSimpl.corrige(false)
            return false
          }
          cs = true
          if (t1[i] === '+') { s1 = 1 } else { s1 = -1 }
        } else {
          if (!cs) {
            stor.errFoisauMil = true
            stor.zoneSimpl.corrige(false)
            return false
          }
          cs = false
          t3.push(recupe(t1[i]))
          t3[t3.length - 1].num = s1 * t3[t3.length - 1].num
        }
      }
      if (cs || (t3.length !== 2)) {
        stor.errFoisauMil = true
        stor.zoneSimpl.corrige(false)
        return false
      }
      // verif les 2 bouts
      t3[0] = cal(t3[0], t2)
      t3[1] = cal(t3[1], t2)
      n2 = cal(stor.a, stor.c)
      n3 = cal(stor.a, stor.d)
      if (egalite(n2, t3[0])) {
        if (!egalite(n3, t3[1])) {
          stor.errCal = true
          stor.zoneSimpl.corrige(false)
          return false
        }
      } else if (!egalite(n3, t3[0])) {
        stor.errCal = true
        stor.zoneSimpl.corrige(false)
        return false
      } else if (!egalite(n2, t3[1])) {
        stor.errCal = true
        stor.zoneSimpl.corrige(false)
        return false
      }
      return true
    }

    return ok
  } // isRepOk
  function decoupe (q) {
    let i
    let buf = ''
    let dans = false
    const ret = []
    for (i = 0; i < q.length; i++) {
      if ((q[i] === '+') || (q[i] === '-')) {
        if (dans) {
          ret.push(buf)
          buf = ''
        }
        dans = false
        ret.push(q[i])
      } else {
        dans = true
        buf += q[i]
      }
    }
    if (dans) {
      ret.push(buf)
    }
    return ret
  }
  function virefact2 () {
    j3pEmpty(stor.ppTab[0][0])
    j3pAffiche(stor.ppTab[0][0], null, 'facteur commun: &nbsp;')
    j3pEmpty(stor.nunuTAb[1][0])
    stor.niv++
    j3pDetruit(stor.lebout)
    if (ds.Detail) {
      faitDetail()
    } else {
      faitResult()
    }
  }
  function virefact () {
    stor.encours = 'blok'
    me.cacheBoutonValider()
    stor.lebout = j3pAjouteBouton('BoutonsJ3P', virefact2, { className: 'big suite', value: 'Suite' })
    stor.lebout.focus()
  }
  function estmax (q) {
    if (q.num === 0) return false
    if (stor.a.num !== 0) {
      if (stor.b.num !== 0) {
        // afaire
        // FIXME mettre un message plus adapté pour l’élève ! (sinon utiliser j3pNotify pour envoyer à bugsnag sans rien dire à l’utilisateur)
        j3pShowError('Erreur interne (cas non prévu)')
        j3pNotify(Error('un cas non codé (le message d’origine était "yaprente pas fait divise")'))
        // return undefined ?
      } else {
        if (stor.a.num !== q.num) return false
        if (stor.a.den !== q.den) return false
        return !(stor.a.yav && !q.yav)
      }
    } else {
      if (stor.b.num !== q.num) return false
      if (stor.b.den !== q.den) return false
      return !(stor.b.yav && !q.yav)
    }
  }
  function divise (q) {
    if (q.num === 0) return false
    if (stor.a.num !== 0) {
      if (stor.b.num !== 0) {
        // FIXME afaire
        j3pShowError('Erreur interne (cas non prévu)')
        j3pNotify(Error('un cas non codé (le message d’origine était "yaprente pas fait divise")'))
      } else {
        if (Math.abs(stor.a.num) % Math.abs(q.num) !== 0) return false
        if (stor.a.den % q.den !== 0) return false
        return !(!stor.a.yav && q.yav)
      }
    } else {
      if (Math.abs(stor.b.num) % Math.abs(q.num) !== 0) return false
      if (stor.b.den % q.den !== 0) return false
      return !(!stor.b.yav && q.yav)
    }
  }
  function cal (x1, x2) {
    const ret = {}
    const num = x1.num * x2.num
    const den = x1.den * x2.den
    let pg = j3pPGCD(Math.abs(num), Math.abs(den))
    ret.num = num / pg
    ret.den = den / pg
    pg = 0
    if (x1.yav) {
      if (x1.car) {
        pg = 2
      } else {
        pg = 1
      }
    }
    if (x2.yav) {
      pg++
      if (x2.car) {
        pg++
      }
    }
    ret.puis = pg
    return ret
  }
  function cal2 (x1, x3) {
    const ret = {}
    let puis3 = 0
    if (x3.yav) puis3 = 1
    ret.puis = x1.puis - puis3
    ret.yav = (ret.puis > 0)
    ret.car = (ret.puis > 1)
    ret.num = Math.round(x1.num / x3.num)
    ret.den = Math.round(x1.den / x3.den)
    return ret
  }
  function desactiveAll () {
    if (stor.encours === 'Facteur') {
      stor.zone1.disable()
    }
    if (stor.encours === 'Detail') {
      stor.zone2.disable()
      stor.zone4.disable()
      stor.tabCont.cells[1][2].style.border = ''
      stor.tabCont.cells[1][2].style.background = ''
    }
    if (stor.encours === 'simpl') {
      stor.zoneSimpl.disable()
      if (stor.brouill !== undefined) stor.brouill.disable()
    }
  } // desactiveAll
  function passeToutVert () {
    if (stor.encours === 'Facteur') {
      stor.zone1.corrige(true)
    }
    if (stor.encours === 'Detail') {
      stor.zone2.corrige(true)
      stor.zone4.corrige(true)
    }
    if (stor.encours === 'simpl') {
      stor.zoneSimpl.corrige(true)
    }
  } // passeToutVert
  function barrelesfo () {
    if (stor.encours === 'Facteur') {
      stor.zone1.barre()
    }
    if (stor.encours === 'Detail') {
      stor.zone2.barreIfKo()
      stor.zone4.barreIfKo()
    }
    if (stor.encours === 'simpl') {
      stor.zoneSimpl.barre()
    }
  }
  function affCorrFaux (isFin) {
    /// //affiche indic

    if (stor.errProd) {
      j3pAffiche(stor.lesdiv.explications, null, 'Je ne retrouve pas les termes de départ en calculant les produits !\n')
      stor.yaexplik = true
    }
    if (stor.errFoisauMil) {
      j3pAffiche(stor.lesdiv.explications, null, 'Il y a des erreurs d’écritures !\n')
      stor.yaexplik = true
    }
    if (stor.errYapa) {
      j3pAffiche(stor.lesdiv.explications, null, 'Je ne trouve pas tous les produits attendus !\n')
      stor.yaexplik = true
    }
    if (stor.errDiv) {
      j3pAffiche(stor.lesdiv.explications, null, 'Ce facteur ne divise pas les deux termes !\n')
      stor.yaexplik = true
    }
    if (stor.err1) {
      j3pAffiche(stor.lesdiv.explications, null, 'Cela ne sert à rien de mettre $1$ en facteur !\n')
      stor.yaexplik = true
    }
    if (stor.errMax) {
      j3pAffiche(stor.lesdiv.explications, null, 'Tu peux en mettre plus en facteur !\n')
      stor.yaexplik = true
    }
    if (stor.errSigneF) {
      j3pAffiche(stor.lesdiv.explications, null, 'Le signe $-$ n’apparait pas dans les deux termes !\n')
      stor.yaexplik = true
    }
    if (stor.errSigneM) {
      j3pAffiche(stor.lesdiv.explications, null, 'Le signe $-$ apparait dans les deux termes !\n')
      stor.yaexplik = true
    }
    if (stor.errPAsfact) {
      j3pAffiche(stor.lesdiv.explications, null, 'La réponse doit être un produit , \nde la forme <i><b>$k\\text{ }(a+b)$</b></i> ! ')
      stor.yaexplik = true
    }
    if (stor.errRetrouvePas) {
      j3pAffiche(stor.lesdiv.explications, null, 'Le facteur commun a changé !\n')
      stor.yaexplik = true
    }
    if (stor.errDiv2) {
      j3pAffiche(stor.lesdiv.explications, null, 'Le facteur commun est faux !\n')
      stor.yaexplik = true
    }
    if (stor.errCal) {
      j3pAffiche(stor.lesdiv.explications, null, 'Le facteur entre parenthèses est faux !\n')
      stor.yaexplik = true
    }

    if (isFin) {
      // barre les faux
      let bibi, sur
      stor.yaco = true
      sur = 1
      if (ds.Detail) sur++
      if (ds.facteur) sur++
      me.score = j3pArrondi(me.score + stor.niv / sur, 2)
      // afficheco
      if (stor.encours === 'Facteur') {
        j3pAffiche(stor.lesdiv.solution, null, 'facteur commun: ' + affichemonexpfact2(true) + '$\n')
        bibi = ''
        if (affichemonexpfact2(true)[1] !== '-') bibi = '+'
        j3pAffiche(stor.lesdiv.solution, null, 'En effet, $ A =  $' + affichemonexpfact2(true) + ' \\times ' + verifpar(affichemoibien(cal2(cal(stor.a, stor.c), stor.fact))) + bibi + '$' + affichemonexpfact2(true) + ' \\times ' + verifpar(affichemoibien(cal2(cal(stor.a, stor.d), stor.fact))) + '$')
      }
      if (stor.encours === 'Detail') {
        bibi = ''
        if (affichemonexpfact2(true)[1] !== '-') bibi = '+'
        j3pAffiche(stor.lesdiv.solution, null, '$ A =  $' + affichemonexpfact2(true) + ' \\times ' + verifpar(affichemoibien(cal2(cal(stor.a, stor.c), stor.fact))) + bibi + '$' + affichemonexpfact2(true) + ' \\times ' + verifpar(affichemoibien(cal2(cal(stor.a, stor.d), stor.fact))) + '$')
      }
      if (stor.encours === 'simpl') {
        if (!ds.Detail) {
          bibi = ''
          if (affichemonexpfact2(true)[1] !== '-') bibi = '+'
          j3pAffiche(stor.lesdiv.solution, null, '$ A =  $' + affichemonexpfact2(true) + ' \\times ' + verifpar(affichemoibien(cal2(cal(stor.a, stor.c), stor.fact))) + bibi + '$' + affichemonexpfact2(true) + ' \\times ' + verifpar(affichemoibien(cal2(cal(stor.a, stor.d), stor.fact))) + '$\n')
        }
        j3pAffiche(stor.lesdiv.solution, null, '$ A =  $' + affichemonexpfact2(true) + ' (' + affichemoibien(cal2(cal(stor.a, stor.c), stor.fact)) + affichemoibien(cal2(cal(stor.a, stor.d), stor.fact), true) + ')$')
      }
      barrelesfo()
      desactiveAll()
    } else { me.afficheBoutonValider() }
  } // affCorrFaux
  function verifpar (s) {
    if (s[0] === '-') return '(' + s + ')'
    return s
  }
  function regroupe (a, b) {
    if ((a === '') || (b === '')) return a + b
    if ((b.indexOf('-') === -1) && (b.indexOf('+') === -1)) return a + ' + ' + b
    return a + b
  }
  function enonceMain () {
    me.videLesZones()

    creeLesDiv()
    stor.lesdiv.travail.classList.add('travail')
    stor.lesdiv.consigneG.classList.add('enonce')
    stor.Lexo = faisChoixExos()
    poseQuestion()
    if (ds.Calculatrice) {
      me.fenetresjq = [{ name: 'Calculatrice', title: 'Une calculatrice', left: 600, top: 10 }]
      j3pCreeFenetres(me)
      j3pAfficheCroixFenetres('Calculatrice')
      j3pAjouteBouton(stor.lesdiv.calculatrice, boutonfen, { className: 'MepBoutons', value: 'Calculatrice' })
    }
    me.afficheBoutonValider()
    me.finEnonce()
  }

  switch (this.etat) {
    case 'enonce':
      // code exécuté au lancement de la section
      if (me.debutDeLaSection) {
        initSection()
      } else {
        enonceMain()
      }
      break // case "enonce":

    case 'correction':
      // On teste si une réponse a été saisie
      stor.yaexplik = false
      stor.yaco = false
      stor.lesdiv.explications.innerHTML = ''
      stor.lesdiv.solution.innerHTML = ''
      stor.lesdiv.explications.classList.remove('explique')
      stor.lesdiv.solution.classList.remove('correction')
      if (stor.encours === 'blok') return

      if (yaReponse()) {
        // Bonne réponse
        if (isRepOk()) {
          stor.lesdiv.correction.style.color = this.styles.cbien
          stor.lesdiv.correction.innerHTML = cBien
          desactiveAll()
          passeToutVert()
          if (stor.encours === 'Detail') {
            stor.niv++
            faitResult2()
            return
          }
          if (stor.encours === 'Facteur') {
            me.cacheBoutonSuite()
            virefact()
            return
          }

          this.score++
          this.typederreurs[0]++
          this.sectionCourante('navigation')
        } else {
          // Pas de bonne réponse
          stor.lesdiv.correction.style.color = this.styles.cfaux
          stor.lesdiv.correction.innerHTML = cFaux
          if (this.isElapsed) {
            // A cause de la limite de temps :
            stor.lesdiv.correction.innerHTML += tempsDepasse + '<BR>'
            this.typederreurs[10]++

            affCorrFaux(true)
            stor.yaco = true

            this.sectionCourante('navigation')
          } else {
            // Réponse fausse :
            if (this.essaiCourant < ds.nbchances) {
              stor.lesdiv.correction.innerHTML += '<br>' + essaieEncore

              affCorrFaux(false)

              // affCorrFaux.call(this, false)
              this.typederreurs[1]++
            } else {
              // Erreur au nème essai
              affCorrFaux(true)
              stor.yaco = true

              if (stor.encours === 'compare') {
                this.score = j3pArrondi(this.score + 0.1, 1)
              }
              if (stor.encours === 'calcul') {
                this.score = j3pArrondi(this.score + 0.4, 1)
              }
              if (stor.encours === 'concl') {
                this.score = j3pArrondi(this.score + 0.7, 1)
              }

              this.typederreurs[2]++
              this.sectionCourante('navigation')
            }
          }
        }
      } else {
        // pas de réponse
        stor.lesdiv.correction.style.color = this.styles.cfaux
        stor.lesdiv.correction.innerHTML = reponseIncomplete
        this.afficheBoutonValider()
      }
      if (stor.yaexplik) stor.lesdiv.explications.classList.add('explique')
      if (stor.yaco) stor.lesdiv.solution.classList.add('correction')
      // Obligatoire
      this.finCorrection()
      break // case "correction":

    case 'navigation':
      if (!this.sectionTerminee()) {
        this.etat = 'enonce'
      }
      this.finNavigation()
      break // case "navigation":
  }
}
