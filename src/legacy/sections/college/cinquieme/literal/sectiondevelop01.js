import { j3pAddElt, j3pAjouteBouton, j3pArrondi, j3pClone, j3pDetruit, j3pDiv, j3pEmpty, j3pGetRandomBool, j3pGetRandomInt, j3pPGCD, j3pShowError, j3pShuffle } from 'src/legacy/core/functions'
import ListeDeroulante from 'src/legacy/outils/listeDeroulante/ListeDeroulante'
import { getPe, initCptPe } from 'src/legacy/sections/college/helperTommy'
import { j3pAffiche } from 'src/lib/mathquill/functions'
import { addDefaultTable, addDefaultTableDetailed } from 'src/legacy/themes/table'
import ZoneStyleMathquill3 from 'src/legacy/outils/zoneStyleMathquill/ZoneStyleMathquill3'
import { j3pCreeFenetres, j3pAfficheCroixFenetres, j3pToggleFenetres } from 'src/legacy/core/functionsJqDialog'
import BrouillonCalculs from 'src/legacy/outils/brouillon/BrouillonCalculs'
import textesGeneriques from 'src/lib/core/textes'

import imgPlus from './plus.png'
import imgMoins from './moins.png'

const { cBien, cFaux, essaieEncore, reponseIncomplete, tempsDepasse } = textesGeneriques

// Nos pe
const pe1 = 'Egalite'
const pe2 = 'Ecriture'
const pe3 = 'Produit_en_croix'
const pe4 = 'Simplification'
/**
 * Ajouter une description ici
 * @fileOverview
 * @author Tommy Barroy <t.barroy@laposte.net>
 * @since 2017-10
*/
export const params = {
  outils: ['mathquill', 'calculatrice'],
  /**
   * Ce qui est paramétrable dans l’éditeur de graphe
   * @type {LegacySectionParamsParametres}
   */
  parametres: [
    ['nbrepetitions', 9, 'entier', 'Nombre de répétitions de la section'],
    ['nbchances', 2, 'entier', 'Nombre d’essais possibles.'],
    ['fleches', true, 'boolean', '<u>true</u>:  Aide possible.'],
    ['produit', true, 'boolean', '<u>true</u>: L’élève doit écrire les produits.'],
    ['NbProduitFixe', true, 'boolean', '<i>Valable uniquement pour <b>produit</b> à true</i> <br><br> <u>true</u>: Le nombre de produits attendus est fixé.'],
    ['simplification', true, 'boolean', '<u>true</u>: L’étape de simplification des produits est attendue.'],
    ['reduction', true, 'boolean', '<u>true</u>:  La réponse réduite est attendue.'],
    ['distributivite', 'les deux', 'liste', '<u>simple</u> ou <u>double</u>.', ['simple', 'double', 'les deux']],
    ['carre', 'oui', 'liste', '<u>oui</u>:  présence de x².', ['oui', 'non', 'les deux']],
    ['formule', 'k_a_plus_b', 'liste', "<u>k_a_plus_b'</u>:  Formule k (a + b) disponible <br><br> <u>k_a_moins_b'</u>:  Formule k (a - b) disponible.", ['k_a_plus_b', 'k_a_moins_b', 'les deux']],
    ['nombres', 'entiers', 'liste', '<u>entiers</u>:  Les coefficients sont entiers. <br><br> <u>fractions</u>: Les coeffiients sont des fractions.', ['entiers', 'fractions', 'les deux']],
    ['Positifs', true, 'boolean', '<u>true</u>:  Les coefficients sont positifs'],
    ['AideSigne', true, 'boolean', '<i> Valable uniquement quand <b>Positifs</b> est à true.</i><br><br><u>true</u>:  Les signes sont fixés'],
    ['Calculatrice', true, 'boolean', '<u>true</u>:  Une calculatrice est disponible'],
    // le modèle gère la compatibilité ascendante avec l’ancien paramètre MiseEnPagePlusClaire
    ['theme', 'standard', 'liste', 'Modèle pour l’affichage', ['standard', 'zonesAvecImageDeFond']]
  ],
  // dans le cas d’une section QUANTITATIVE
  pe: [
    { pe_1: pe1 },
    { pe_2: pe2 },
    { pe_3: pe3 },
    { pe_4: pe4 }
  ]
}

/**
 * section develop01
 * @this {Parcours}
 */
export default function main () {
  const me = this
  const stor = me.storage
  const ds = me.donneesSection

  function creeLesDiv () {
    stor.lesdiv = {}
    stor.lesdiv.conteneur = j3pAddElt(me.zonesElts.MG, 'div', {
      style: me.styles.etendre('toutpetit.enonce', { padding: '10px' })
    })
    stor.lesdiv.consigneG = addDefaultTable(stor.lesdiv.conteneur, 1, 1)[0][0]
    stor.lesdiv.etape = addDefaultTable(stor.lesdiv.conteneur, 1, 1)[0][0]
    stor.lesdiv.travail = addDefaultTable(stor.lesdiv.conteneur, 1, 1)[0][0]
    stor.lesdiv.explications = j3pAddElt(stor.lesdiv.conteneur, 'div')
    stor.lesdiv.solution = j3pAddElt(stor.lesdiv.conteneur, 'div')
    stor.lesdiv.calculatrice = j3pAddElt(me.zonesElts.MD, 'div', {
      style: me.styles.etendre('toutpetit.enonce', { padding: '10px' })
    })
    stor.lesdiv.aide = j3pAddElt(me.zonesElts.MD, 'div', {
      style: me.styles.etendre('toutpetit.enonce', { padding: '10px' })
    })
    stor.lesdiv.correction = j3pAddElt(me.zonesElts.MD, 'div', {
      style: me.styles.etendre('toutpetit.enonce', { padding: '10px' })
    })
    stor.lesdiv.etape.style.color = me.styles.toutpetit.correction.color
    stor.lesdiv.solution.style.color = me.styles.toutpetit.correction.color
    stor.lesdiv.explications.style.color = me.styles.cfaux
    me.zonesElts.MG.classList.add('fond')
  }

  function initSection () {
    initCptPe(ds, stor)

    stor.etapos = []
    if (ds.produit) stor.etapos.push('prod')
    if (ds.simplification) stor.etapos.push('simpl')
    if (ds.reduction) stor.etapos.push('red')
    if (stor.etaposLength === 0) {
      j3pShowError('Pb de paramètrage')
      stor.etapos.push('fl')
    }

    // Construction de la page
    me.construitStructurePage('presentation1bis')
    stor.exos = []

    let lp = []
    if ((ds.formule === 'k_a_plus_b') || (ds.formule === 'les deux')) lp.push('+')
    if ((ds.formule === 'k_a_moins_b') || (ds.formule === 'les deux')) lp.push('-')
    lp = j3pShuffle(lp)
    for (let i = 0; i < ds.nbitems; i++) {
      stor.exos.push({ formule: lp[i % (lp.length)] })
    }
    stor.exos = j3pShuffle(stor.exos)

    lp = []
    if ((ds.nombres === 'entiers') || (ds.nombres === 'les deux')) lp.push('ent')
    if ((ds.nombres === 'fractions') || (ds.nombres === 'les deux')) lp.push('frac')
    lp = j3pShuffle(lp)
    for (let i = 0; i < stor.exos.length; i++) {
      stor.exos[i].type = lp[i % lp.length]
    }
    stor.exos = j3pShuffle(stor.exos)

    lp = []
    if ((ds.carre === 'oui') || (ds.carre === 'les deux')) lp.push('car')
    if ((ds.carre === 'non') || (ds.carre === 'les deux')) lp.push('pacar')
    lp = j3pShuffle(lp)
    for (let i = 0; i < stor.exos.length; i++) {
      stor.exos[i].carre = lp[i % lp.length]
    }
    stor.exos = j3pShuffle(stor.exos)

    lp = []
    if ((ds.distributivite === 'simple') || (ds.distributivite === 'les deux')) lp.push('simp')
    if ((ds.distributivite === 'doub') || (ds.distributivite === 'les deux')) lp.push('doub')
    lp = j3pShuffle(lp)
    for (let i = 0; i < stor.exos.length; i++) {
      stor.exos[i].distri = lp[i % lp.length]
    }
    stor.exos = j3pShuffle(stor.exos)

    me.typederreurs = [0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0]

    me.score = 0

    me.afficheTitre('Développer')
    enonceMain()
  }

  function faisChoixExos () {
    const e = stor.exos.pop()
    let num, den, t3, ok
    num = j3pGetRandomInt(2, 9)
    den = premsavec(num)
    stor.a = { num, den }
    num = j3pGetRandomInt(1, 9)
    den = premsavec(num)
    stor.b = { num, den }
    num = j3pGetRandomInt(2, 9)
    den = premsavec(num)
    stor.c = { num, den }
    num = j3pGetRandomInt(1, 9)
    den = premsavec(num)
    stor.d = { num, den }
    stor.restric = '0123456789/'
    if (e.type === 'ent') {
      stor.a.den = stor.b.den = stor.c.den = stor.d.den = 1
      stor.restric = '0123456789'
    }
    if (!ds.Positifs) {
      if (j3pGetRandomBool()) stor.a.num = -stor.a.num
      if (j3pGetRandomBool()) stor.c.num = -stor.c.num
    }
    stor.lx2 = j3pGetRandomInt(1, 2)
    stor.lx1 = j3pGetRandomInt(1, 2)
    if (e.distri === 'simp') {
      if (e.carre === 'car') {
        if (stor.lx1 === 1) {
          stor.b.num = 0
        } else {
          stor.a.num = 0
        }
      } else {
        if (stor.lx1 === 2) { stor.b.num = 0 } else { stor.a.num = 0 }
      }
    }
    const fg = 'xyzabt'
    stor.nomvar = fg[j3pGetRandomInt(0, 5)]
    stor.restric += stor.nomvar
    stor.t1 = []
    stor.t2 = []
    stor.t3 = []
    if (e.formule === '+') {
      t3 = 1
    } else { t3 = -1 }
    if (stor.a.num !== 0) {
      if (stor.c.num !== 0) {
        stor.t1.push({ f1: { n: stor.a, yav: stor.lx1 === 1 }, f2: { n: stor.c, yav: stor.lx2 === 1 }, signe: 1 })
      }
      if (stor.d.num !== 0) {
        stor.t1.push({ f1: { n: stor.a, yav: stor.lx1 === 1 }, f2: { n: stor.d, yav: stor.lx2 === 2 }, signe: t3 })
      }
    }
    if (stor.b.num !== 0) {
      if (stor.c.num !== 0) {
        stor.t1.push({ f1: { n: stor.b, yav: stor.lx1 === 2 }, f2: { n: stor.c, yav: stor.lx2 === 1 }, signe: 1 })
      }
      if (stor.d.num !== 0) {
        stor.t1.push({ f1: { n: stor.b, yav: stor.lx1 === 2 }, f2: { n: stor.d, yav: stor.lx2 === 2 }, signe: t3 })
      }
    }
    for (let i = 0; i < stor.t1.length; i++) {
      num = stor.t1[i].f1.n.num * stor.t1[i].f2.n.num
      den = stor.t1[i].f1.n.den * stor.t1[i].f2.n.den
      t3 = j3pPGCD(num, den, { negativesAllowed: true, valueIfZero: 1 })
      num = Math.round(num / t3)
      den = Math.round(den / t3)
      stor.t2.push({ n: { num, den }, signe: stor.t1[i].signe, x: stor.t1[i].f1.yav || stor.t1[i].f2.yav, car: stor.t1[i].f1.yav && stor.t1[i].f2.yav })
    }
    for (let i = 0; i < stor.t2.length; i++) {
      ok = false
      for (let j = 0; j < stor.t3.length; j++) {
        if ((stor.t3[j].x === stor.t2[i].x) && (stor.t3[j].car === stor.t2[i].car)) {
          ok = true
          num = stor.t3[j].n.num * stor.t2[i].n.den * stor.t3[j].signe + stor.t3[j].n.den * stor.t2[i].n.num * stor.t2[i].signe
          den = stor.t3[j].n.den * stor.t2[i].n.den
          stor.t3[j].signe = 1
          t3 = j3pPGCD(num, den, { negativesAllowed: true, valueIfZero: 1 })
          num = Math.round(num / t3)
          den = Math.round(den / t3)
          stor.t3[j].n.num = num
          stor.t3[j].n.den = den
        }
      }
      if (!ok) {
        stor.t3.push(j3pClone(stor.t2[i]))
      }
    }
    for (let i = stor.t3.length - 1; i > -1; i--) {
      if (stor.t3[i].n.num === 0) {
        stor.t3.splice(i, 1)
      }
    }
    return e
  }
  function poseQuestion () {
    stor.asup = []
    stor.Yaeusimpl = false
    const ts = addDefaultTableDetailed(stor.lesdiv.consigneG, 1, 3)
    ts.table.style.padding = 0
    j3pAffiche(ts.cells[0][0], null, '<b> On veut développer </b> l’expression &nbsp; ')
    j3pAffiche(ts.cells[0][1], null, '$ A = $')
    j3pAffiche(ts.cells[0][1], null, affichemonexp(false))
    stor.tabCont = addDefaultTableDetailed(stor.lesdiv.travail, 4, 4)
    stor.tabCont.table.style.padding = 0
    for (let i = 0; i < 4; i++) {
      stor.tabCont.lines[i].style.padding = 0
      for (let j = 0; j < 4; j++) {
        stor.tabCont.cells[i][j].style.padding = 0
      }
    }
    affichemonexp(true)
    if (ds.fleches) j3pAjouteBouton(stor.lesdiv.aide, 'Fleches', 'MepBoutons', 'Aide', faitFleches)
    stor.divencours = stor.tabCont.cells[1]
    stor.encours = stor.etapos[0]
    if (stor.encours === 'prod') {
      faitProduit()
      return
    } else if (stor.encours === 'simpl') {
      if (stor.t2.length !== stor.t3.length) {
        faitSimpl()
        return
      }
    }
    faitReduc()
  }

  function faitReduc () {
    j3pEmpty(stor.lesdiv.etape)
    for (let i = 0; i < stor.asup.length; i++) {
      j3pDetruit(stor.asup[i])
    }
    stor.asup = []
    stor.encours = 'red'
    let doudpon = stor.tabCont.cells[1]
    stor.divencours = stor.tabCont.cells[2]
    if (stor.a.num !== 0 && stor.b.num !== 0) {
      j3pAffiche(stor.lesdiv.etape, null, '<u>Étape</u>: Donner la réponse sous la forme d’une expression réduite.')
    } else {
      j3pAffiche(stor.lesdiv.etape, null, '<u>Étape</u>: Donne le résultat du développement')
    }
    if (ds.produit) {
      if (!stor.Yaeusimpl) {
        doudpon = stor.tabCont.cells[2]
        stor.divencours = stor.tabCont.cells[3]
      }
    }
    if (!stor.Yaeusimpl) {
      let pppo = 1
      if (!ds.produit && !stor.Yaeusimpl) pppo++
      stor.brouillRed = new BrouillonCalculs({
        caseBoutons: doudpon[0],
        caseEgal: doudpon[1],
        caseCalculs: doudpon[2],
        caseApres: doudpon[3]
      }, {
        limite: 30,
        limitenb: 20,
        restric: '0123456789*/,.²+-()*' + stor.nomvar,
        hasAutoKeyboard: false,
        lmax: pppo
      })
    }
    if (stor.Yaeusimpl) {
      stor.divencours = stor.tabCont.cells[3]
      stor.brouillRed = { disable: function () {} }
    }
    j3pAffiche(stor.divencours[0], null, '$A$')
    j3pAffiche(stor.divencours[1], null, '$=$')
    stor.zoneRed = new ZoneStyleMathquill3(stor.divencours[2], { inverse: true, restric: stor.restric + '+-²', bloqueFraction: '+-²' + stor.nomvar, glissefrac: '²' + stor.nomvar, hasAutoKeyboard: true, enter: me.sectionCourante.bind(me) })
    stor.zoneRed.focus()
  }
  function faitSimpl () {
    j3pEmpty(stor.lesdiv.etape)
    stor.Yaeusimpl = true
    for (let i = 0; i < stor.asup.length; i++) {
      j3pDetruit(stor.asup[i])
    }
    stor.asup = []
    stor.encours = 'simpl'
    j3pAffiche(stor.lesdiv.etape, null, '<u>Étape</u>: Écris les produits <u>simplifiés</u> obtenus en développant $A$. \n(<i> sans réduire</i>)')
    if (!ds.produit) {
      stor.brouill = new BrouillonCalculs({
        caseBoutons: stor.tabCont.cells[1][0],
        caseEgal: stor.tabCont.cells[1][1],
        caseCalculs: stor.tabCont.cells[1][2],
        caseApres: stor.tabCont.cells[1][3]
      }, {
        limite: 30,
        limitenb: 20,
        restric: '0123456789*/,.' + stor.nomvar,
        hasAutoKeyboard: false,
        lmax: 1
      })
    }
    stor.divencours = stor.tabCont.cells[2]
    j3pAffiche(stor.divencours[0], null, '$A$')
    j3pAffiche(stor.divencours[1], null, '$=$')
    stor.zoneSimpl = new ZoneStyleMathquill3(stor.divencours[2], { inverse: true, restric: stor.restric + '+-²', bloqueFraction: '+-²' + stor.nomvar, glissefrac: '²' + stor.nomvar, hasAutoKeyboard: true, enter: me.sectionCourante.bind(me) })
  }
  function faitFleches () {
    stor.tabZed[0][0].style.height = '20px'
    stor.tabZed[2][0].style.height = '20px'
    const pos1 = stor.tabZed[0][1].getBoundingClientRect()
    const pos21 = stor.tabZed[0][2].getBoundingClientRect()
    const pos22 = stor.tabZed[0][3].getBoundingClientRect()
    const pos2 = { left: (pos21.left + pos22.left) / 2, top: pos21.top }
    const pos3 = stor.tabZed[2][6].getBoundingClientRect()
    const pos4 = stor.tabZed[2][7].getBoundingClientRect()
    let haut = false
    if (stor.tabZed[0][1].offsetWidth > 4) {
      if (stor.tabZed[0][5].offsetWidth > 4) Tracefleche(pos1, pos3, false, 'f1')
      if (stor.tabZed[0][6].offsetWidth > 4) Tracefleche(pos1, pos4, false, 'f2', true)
      haut = true
    }
    if (stor.tabZed[0][2].offsetWidth > 4) {
      if (stor.tabZed[0][5].offsetWidth > 4) Tracefleche(pos2, pos3, haut, 'f3')
      if (stor.tabZed[0][6].offsetWidth > 4) Tracefleche(pos2, pos4, haut, 'f4', true)
    }
    if (document.getElementById('Fleches')) j3pDetruit('Fleches')
  }
  function Tracefleche (p1, p2, hau, name, larg) {
    let large = 15
    if (larg) large = 30
    const pos3 = me.zonesElts.MG.getBoundingClientRect()
    let lot = p1.top - 15
    if (!larg) lot += 15
    if (hau) lot = p2.top - 5
    const wwi = p2.left - p1.left - 10
    let num1, num2, si2
    if (hau) {
      num2 = large
      num1 = 0
      si2 = 1
    } else {
      num2 = 0
      num1 = large
      si2 = -1
    }
    const flechdeb = wwi
    const si = -1
    const yy = j3pDiv(me.zonesElts.MG, {
      contenu: '',
      coord: [p1.left - pos3.left, lot - pos3.top],
      style: me.styles.toutpetit.enonce
    })
    stor.asup.push(yy)
    yy.style.width = wwi + 'px'
    yy.style.height = (large + 5) + 'px'
    yy.wwi = wwi
    let xx = si2 * 2
    if (si2 === -1) xx = 0
    yy.innerHTML = '<svg viewBox="0 0 ' + wwi + ' ' + (large + 5) + '" ><path d="M0 ' + num1 + ' Q ' + (wwi / 2) + ' ' + num2 + ', ' + wwi + ' ' + num1 + '  M ' + flechdeb + ' ' + num1 + ' L ' + (flechdeb + si * wwi * 7 / 55) + ' ' + (num1 + si2 * 12) + '  M ' + flechdeb + ' ' + num1 + ' L ' + (wwi - 15) + ' ' + (num1 + xx) + '" stroke-width="3" stroke="#000000" fill="transparent" /></svg>'
  }
  function faitProduit () {
    j3pAffiche(stor.lesdiv.etape, null, '<u>Étape</u>: Détaille les produits obtenus en développant $A$.')
    stor.tabProd = addDefaultTableDetailed(stor.divencours[2], 1, 7)
    stor.tabProd.table.style.padding = 0
    stor.tabProd.lines[0].style.padding = 0
    for (let j = 0; j < 7; j++) {
      stor.tabProd.cells[0][j].style.padding = 0
    }
    j3pAffiche(stor.divencours[0], null, '$A$')
    j3pAffiche(stor.divencours[1], null, '$=$')
    stor.nbprod = 0
    stor.lesprod = []
    stor.divencours[2].style.border = '1px solid #000000'
    stor.divencours[2].style.padding = '5px 5px 5px 5px'
    stor.divencours[2].style.background = '#fff'
    AddProd(true)
    if (ds.NbProduitFixe) {
      for (let i = 0; i < stor.t2.length - 1; i++) {
        AddProd()
      }
      j3pEmpty(stor.divencours[3])
    }
  }
  function affichefoisBis (where, bb) {
    const tt = addDefaultTableDetailed(where, 1, 5)
    tt.table.style.padding = 0
    tt.lines[0].style.padding = 0
    for (let j = 0; j < 5; j++) {
      tt.cells[0][j].style.padding = 0
    }
    j3pAffiche(tt.cells[0][3], null, '$\\times$')
    if (bb === '') j3pAffiche(tt.cells[0][1], null, '&nbsp;&nbsp;')
    j3pAffiche(tt.cells[0][0], null, '&nbsp;')
    return tt.cells[0]
  }
  function AddProd (bb) {
    if (stor.nbprod === 5) return
    stor.nbprod++
    const t = affichefoisBis(stor.tabProd.cells[0][stor.nbprod], bb)
    let lsi
    if (!ds.AideSigne || !ds.Positifs || !ds.NbProduitFixe) {
      const ll = ['&nbsp;', '+', '-']
      lsi = ListeDeroulante.create(t[1], ll, { centre: true, sansFleche: true })
    } else {
      let gg
      if (stor.nbprod % 2 === 1) { gg = '+' } else {
        gg = stor.Lexo.formule
      }
      lsi = { reponse: gg, disable: function () {}, corrige: function () {}, changed: true, conteneur: t[1] }
      if (stor.nbprod === 1 && gg === '+') gg = ''
      j3pAffiche(t[1], null, '$' + gg + '$')
    }
    const zsm1 = new ZoneStyleMathquill3(t[2], { inverse: true, restric: stor.restric, hasAutoKeyboard: false, bloqueFraction: stor.nomvar, glissefrac: '²' + stor.nomvar, enter: me.sectionCourante.bind(me) })
    const zsm2 = new ZoneStyleMathquill3(t[4], { inverse: true, restric: stor.restric, hasAutoKeyboard: false, bloqueFraction: stor.nomvar, glissefrac: '²' + stor.nomvar, enter: me.sectionCourante.bind(me) })
    stor.lesprod.push({ liste: lsi, zsm1, zsm2, fois: t[3] })
    metsBoutProd()
  }
  function metsBoutProd (ou) {
    j3pEmpty(stor.divencours[3])
    const mai = addDefaultTableDetailed(stor.divencours[3], 2, 1)
    mai.table.style.padding = 0
    mai.lines[0].style.padding = 0
    mai.lines[1].style.padding = 0
    mai.cells[0][0].style.padding = 0
    mai.cells[1][0].style.padding = 0
    const ttim = j3pAddElt(mai.cells[0][0], 'img')
    ttim.style.width = '15px'
    ttim.style.height = '15px'
    ttim.style.display = 'none'
    const ttim2 = j3pAddElt(mai.cells[1][0], 'img')
    ttim2.style.width = '15px'
    ttim2.style.height = '15px'
    ttim2.style.display = 'none'
    if (stor.nbprod < 5) {
      ttim.src = imgPlus
      ttim.style.width = '15px'
      ttim.style.cursor = 'pointer'
      ttim.style.display = ''
      ttim.addEventListener('click', AddProd, false)
    }
    if (stor.nbprod > 1) {
      ttim2.src = imgMoins
      ttim2.style.width = '15px'
      ttim2.style.cursor = 'pointer'
      ttim2.style.display = ''
      ttim2.addEventListener('click', SouProd, false)
    }
  }
  function SouProd () {
    if (stor.nbprod === 1) return
    const agere = stor.lesprod.pop()
    agere.liste.disable()
    agere.zsm1.disable()
    agere.zsm2.disable()
    j3pEmpty(stor.tabProd.cells[0][stor.nbprod])
    stor.nbprod--
    metsBoutProd()
  }
  function affichemonexp (bool) {
    let ret, yo
    const a = affichemoibien(stor.a, stor.lx1 === 1, false)
    yo = a !== ''
    const b = affichemoibien(stor.b, stor.lx1 === 2, yo)
    const c = affichemoibien(stor.c, stor.lx2 === 1, false)
    yo = c === ''
    const g = affichemoibien(stor.d, stor.lx2 === 2, yo)
    if (!bool) {
      ret = ''
      if ((a !== '') && (b !== '')) {
        ret = '$(' + a + b + ')'
      } else {
        ret = '$' + a + b
      }
      if ((c !== '') && (g !== '')) {
        ret += '(' + c + stor.Lexo.formule + g + ')$'
      } else {
        ret = c + stor.Lexo.formule + g + '$'
      }
      return ret
    } else {
      stor.tabZed = addDefaultTable(stor.tabCont.cells[0][2], 3, 8)
      j3pAffiche(stor.tabCont.cells[0][0], null, '$A$')
      j3pAffiche(stor.tabCont.cells[0][1], null, '$=$')
      j3pAffiche(stor.tabZed[1][1], null, '$' + a + '$')
      j3pAffiche(stor.tabZed[1][2], null, '$' + b + '$')
      j3pAffiche(stor.tabZed[1][5], null, '$' + c + '$')
      j3pAffiche(stor.tabZed[1][6], null, '$' + stor.Lexo.formule + g + '$')
      if ((a !== '') && (b !== '')) {
        j3pAffiche(stor.tabZed[1][0], null, '$($')
        j3pAffiche(stor.tabZed[1][3], null, '$)$')
      }
      if ((c !== '') && (g !== '')) {
        j3pAffiche(stor.tabZed[1][4], null, '$($')
        j3pAffiche(stor.tabZed[1][7], null, '$)$')
      }
    }
  }
  function affichemoibien (f, x, fautsigne, car) {
    let j = ''
    if (car === true) j = '²'
    if (f.num === 0) return ''
    let ret = ''
    if (f.den === 1) {
      if (f.num < 0) { ret += '-' } else if (fautsigne) { ret += '+' }
      if (x && Math.abs(f.num) === 1) {
        return ret + stor.nomvar + j
      } else {
        ret += Math.abs(f.num)
      }
    } else {
      if (f.num < 0) { ret += '-' } else if (fautsigne) { ret += '+' }
      ret += '\\frac{' + Math.abs(f.num) + '}{' + f.den + '}'
    }
    if (x) ret += stor.nomvar + j
    return ret
  }
  function boutonfen () {
    j3pToggleFenetres('Calculatrice')
  }
  function premsavec (x) {
    let ret
    do {
      ret = j3pGetRandomInt(2, 9)
    } while (j3pPGCD(ret, x, { negativesAllowed: true, valueIfZero: 1 }) !== 1)
    return ret
  }
  function pasatt (q) {
    let ach1
    let ach2
    for (let i = 0; i < stor.t1.length; i++) {
      ach1 = ''
      ach2 = ''
      if (stor.t1[i].f1.n.den === 1) {
        if (Math.abs(stor.t1[i].f1.n.num) !== 1 || !stor.t1[i].f1.yav) {
          ach1 = String(Math.abs(stor.t1[i].f1.n.num))
        }
      } else {
        ach1 = 'ù' + Math.abs(stor.t1[i].f1.n.num) + '#' + stor.t1[i].f1.n.den + '@'
      }
      if (stor.t1[i].f1.yav) ach1 += stor.nomvar
      if (stor.t1[i].f2.n.den === 1) { if (Math.abs(stor.t1[i].f2.n.num) !== 1 || !stor.t1[i].f2.yav) { ach2 = String(Math.abs(stor.t1[i].f2.n.num)) } } else {
        ach2 = 'ù' + Math.abs(stor.t1[i].f2.n.num) + '#' + stor.t1[i].f2.n.den + '@'
      }
      if (stor.t1[i].f2.yav) ach2 += stor.nomvar
      if (((q.f1 === ach1) && (q.f2 === ach2)) || ((q.f1 === ach2) && (q.f2 === ach1))) {
        stor.lesassos.push(i)
        return true
      }
    }
    return false
  }
  function yapas (q) {
    let ach1 = ''
    if (q.f1.n.den === 1) { if (Math.abs(q.f1.n.num) !== 1 || !q.f1.yav) { ach1 = String(Math.abs(q.f1.n.num)) } } else {
      ach1 = 'ù' + Math.abs(q.f1.n.num) + '#' + q.f1.n.den + '@'
    }
    if (q.f1.yav) ach1 += stor.nomvar
    let ach2 = ''
    if (q.f2.n.den === 1) { if (Math.abs(q.f2.n.num) !== 1 || !q.f2.yav) { ach2 = String(Math.abs(q.f2.n.num)) } } else {
      ach2 = 'ù' + Math.abs(q.f2.n.num) + '#' + q.f2.n.den + '@'
    }
    if (q.f2.yav) ach2 += stor.nomvar
    for (let i = 0; i < stor.facteur.length; i++) {
      if ((stor.facteur[i].f1 === ach1) && (stor.facteur[i].f2 === ach2)) return true
      if ((stor.facteur[i].f1 === ach2) && (stor.facteur[i].f2 === ach1)) return true
    }
    return false
  }
  function existedonc (q) {
    let pourtrav
    let yavar
    if (Array.isArray(q)) {
      pourtrav = q[0]
    } else {
      yavar = q.indexOf(stor.nomvar) !== -1
      pourtrav = q.replace(stor.nomvar, '').replace('²', '')
    }
    pourtrav = extnumden(pourtrav)
    return !(!egalite(pourtrav, stor.a, yavar, 1, stor.lx1) && !egalite(pourtrav, stor.b, yavar, 2, stor.lx1) && !egalite(pourtrav, stor.c, yavar, 1, stor.lx2) && !egalite(pourtrav, stor.d, yavar, 2, stor.lx2))
  }
  function egalite (a, b, c, r, t) {
    const varat = t === r
    if (c !== varat) return false
    return (Math.abs(a.num * b.den) === Math.abs(a.den * b.num))
  }
  function egalite2 (a, b) {
    return (Math.abs(a.num * b.den) === Math.abs(a.den * b.num))
  }
  function extnumden (s) {
    if (s.indexOf('ù') === -1) {
      if (s === '') return { num: 1, den: 1 }
      return { num: parseFloat(s), den: 1 }
    } else {
      return { num: parseFloat(s.substring(1, s.indexOf('#'))), den: parseFloat(s.substring(s.indexOf('#') + 1, s.length - 1)) }
    }
  }
  function bienecrit (q) {
    if (Array.isArray(q)) {
      if (q.length > 2) return false
      if (q.length === 2) {
        if (q[1] !== stor.nomvar) return false
      }
    } else {
      if (q.indexOf(stor.nomvar) !== -1) {
        if ('0123456789'.indexOf(q[q.length - 1]) !== -1) {
          return false
        }
        if (q.substring(0, q.length - 1) === '1') return false
      }
    }
    return true
  }

  function yaReponse () {
    if (stor.encours === 'prod') {
      for (const prod of stor.lesprod) {
        if (!prod.liste.changed) {
          prod.liste.focus()
          return false
        }
        if (prod.zsm1.reponse() === '') {
          prod.zsm1.focus()
          return false
        }
        if (prod.zsm2.reponse() === '') {
          prod.zsm2.focus()
          return false
        }
      }
      return true
    }
    if (stor.encours === 'simpl') {
      if (stor.zoneSimpl.reponse() === '') {
        stor.zoneSimpl.focus()
        return false
      }
      return true
    }
    if (stor.encours === 'red') {
      if (stor.zoneRed.reponse() === '') {
        stor.zoneRed.focus()
        return false
      }
      return true
    }
  }
  function isRepOk () {
    stor.errNbFacteurTrop = false
    stor.errNbFacteurManq = false
    stor.errFoisauMil = false
    stor.errExiste = false
    stor.errYapa = false
    stor.errPasAtt = false
    stor.errSigne = false
    stor.errSimFo = false

    let ok = true
    let t1, t2, t3, t4, t5, t6
    let n1, n2, n3, n4, n5, n6
    let ll1

    if (stor.encours === 'prod') {
      stor.lesassos = []
      /// verif nb de facteur
      if (stor.Lexo.distri === 'simp') { n1 = 2 } else { n1 = 4 }
      if (stor.lesprod.length > n1) {
        stor.errNbFacteurTrop = true
        stor.lesprod[stor.lesprod.length - 1].liste.corrige(false)
        stor.lesprod[stor.lesprod.length - 1].zsm1.corrige(false)
        return false
      }
      if (stor.lesprod.length < n1) {
        stor.errNbFacteurManq = true
        return false
      }
      // verif x au milieeu
      stor.signes = []
      stor.facteur = []
      for (const prod of stor.lesprod) {
        stor.signes.push(prod.liste.reponse)
        stor.facteur.push({ f1: prod.zsm1.rep()[0], f2: prod.zsm2.rep()[0] })
      }
      if (!ok) return false
      // verif chaque facteur bien ecrit
      // soit nb , soit nb + lettre
      // verfi nb devant
      for (let i = 0; i < stor.facteur.length; i++) {
        n1 = stor.facteur[i].f1
        n2 = stor.facteur[i].f2
        if (!bienecrit(n1)) {
          stor.errFoisauMil = true
          stor.lesprod[i].zsm1.corrige(false)
          ok = false
        }
        if (n2 === undefined) {
          stor.errFoisauMil = true
          stor.lesprod[i].zsm1.corrige(false)
          ok = false
          continue
        }
        if (!bienecrit(n2)) {
          stor.errFoisauMil = true
          stor.lesprod[i].zsm1.corrige(false)
          ok = false
        }
      }
      if (!ok) return false
      // verif tous les facteur sont dans données
      for (let i = 0; i < stor.facteur.length; i++) {
        n1 = stor.facteur[i].f1
        n2 = stor.facteur[i].f2
        if (!existedonc(n1)) {
          stor.errExiste = true
          stor.lesprod[i].zsm1.corrige(false)
          ok = false
        }
        if (!existedonc(n2)) {
          stor.errExiste = true
          stor.lesprod[i].zsm2.corrige(false)
          ok = false
        }
      }
      if (!ok) return false
      // verif ya pas autre chose que produit attendu
      for (let i = 0; i < stor.facteur.length; i++) {
        if (!pasatt(stor.facteur[i])) {
          stor.errPasAtt = true
          stor.lesprod[i].zsm1.corrige(false)
          return false
        }
      }
      if (!ok) return false

      // verif presence des prods attendus
      for (let i = 0; i < stor.t1.length; i++) {
        if (!yapas(stor.t1[i])) {
          stor.errYapa = true
          return false
        }
      }
      if (!ok) return false
      // verif les signes
      for (let i = 0; i < stor.signes.length; i++) {
        stor.signes[i] = (stor.signes[i] !== '-')
      }
      for (let i = 0; i < stor.lesassos.length; i++) {
        t2 = stor.t1[stor.lesassos[i]].f1.n.num * stor.t1[stor.lesassos[i]].f2.n.num * stor.t1[stor.lesassos[i]].signe > 0
        if (t2 !== stor.signes[i]) {
          stor.errSigne = true
          stor.lesprod[i].liste.corrige(false)
          ok = false
        }
      }

      stor.t2 = []
      for (let i = 0; i < stor.lesassos.length; i++) {
        let num = stor.t1[stor.lesassos[i]].f1.n.num * stor.t1[stor.lesassos[i]].f2.n.num
        let den = stor.t1[stor.lesassos[i]].f1.n.den * stor.t1[stor.lesassos[i]].f2.n.den
        t3 = j3pPGCD(num, den, { negativesAllowed: true, valueIfZero: 1 })
        num = Math.round(num / t3)
        den = Math.round(den / t3)
        stor.t2.push({ n: { num, den }, signe: stor.t1[stor.lesassos[i]].signe, x: stor.t1[stor.lesassos[i]].f1.yav || stor.t1[stor.lesassos[i]].f2.yav, car: stor.t1[stor.lesassos[i]].f1.yav && stor.t1[stor.lesassos[i]].f2.yav })
      }

      return ok
    }
    if (stor.encours === 'simpl') {
      t1 = stor.zoneSimpl.rep()
      t4 = 0
      t6 = []
      n5 = []
      stor.signes = []
      // verif pas deux signes à la suite
      t3 = ((t1[0] === '+') || (t1[0] === '-'))
      if (!t3) {
        t4 = 1
        t6.push(t1[0])
        stor.signes.push('+')
      } else { stor.signes.push(t1[0]) }
      for (let i = 1; i < t1.length; i++) {
        t5 = ((t1[i] === '+') || (t1[i] === '-'))
        if (!t5) {
          t4++
          t6.push(t1[i])
        } else { stor.signes.push(t1[i]) }
        if (t5 === t3) {
          stor.errFoisauMil = true
          stor.zoneSimpl.corrige(false)
          return false
        }
        t3 = t5
      }

      if (t4 < stor.t1.length) {
        stor.errNbFacteurManq = true
        stor.zoneSimpl.corrige(false)
        return false
      }
      if (t4 > stor.t1.length) {
        stor.errNbFacteurTrop = true
        stor.zoneSimpl.corrige(false)
        return false
      }

      for (let i = 0; i < t6.length; i++) {
        n3 = false
        if (t6[i].indexOf(stor.nomvar) !== t6[i].lastIndexOf(stor.nomvar)) {
          stor.errFoisauMil = true
          stor.zoneSimpl.corrige(false)
          return false
        }
        if (t6[i].indexOf('²') !== t6[i].lastIndexOf('²')) {
          stor.errFoisauMil = true
          stor.zoneSimpl.corrige(false)
          return false
        }
        n6 = false
        if (t6[i].includes('²')) { n6 = true }
        if (t6[i].includes(stor.nomvar)) {
          n3 = true
          if (t6[i].indexOf(stor.nomvar) !== t6[i].length - 1) {
            if ((t6[i].indexOf(stor.nomvar) !== t6[i].length - 2) || t6[i][t6[i].length - 1] !== '²') {
              stor.errFoisauMil = true
              stor.zoneSimpl.corrige(false)
              return false
            }
          }
        }
        n4 = t6[i].replace(stor.nomvar + '²', '').replace(stor.nomvar, '')
        if (n4.includes('²')) {
          stor.errFoisauMil = true
          stor.zoneSimpl.corrige(false)
          return false
        }
        if (n3 && n4 === '1') {
          stor.errFoisauMil = true
          stor.zoneSimpl.corrige(false)
          return false
        }
        n5.push({ n: extnumden(n4), x: n3, car: n6 })
      }
      ll1 = []
      // verif yen a pas 1 de faux
      for (let i = 0; i < n5.length; i++) {
        ok = false
        for (let j = 0; j < stor.t2.length; j++) {
          if ((egalite2(n5[i].n, stor.t2[j].n)) && (n5[i].x === stor.t2[j].x) && (n5[i].car === stor.t2[j].car) && ll1[j] === undefined) {
            ll1[j] = i
            ok = true
            break
          }
        }
        if (!ok) {
          stor.errSimFo = true
          stor.zoneSimpl.corrige(false)
          return false
        }
      }
      // verif les signes
      for (let i = 0; i < stor.signes.length; i++) {
        stor.signes[i] = (stor.signes[i] !== '-')
      }
      for (let i = 0; i < ll1.length; i++) {
        t2 = stor.t2[ll1[i]].n.num * stor.t2[ll1[i]].signe > 0
        if (t2 !== stor.signes[i]) {
          stor.errSigne = true
          stor.zoneSimpl.corrige(false)
          ok = false
        }
      }
      return true
    }
    if (stor.encours === 'red') {
      t1 = stor.zoneRed.rep()
      t4 = 0
      t6 = []
      n5 = []
      stor.signes = []
      // verif pas deux signes à la suite
      t3 = ((t1[0] === '+') || (t1[0] === '-'))
      if (!t3) {
        t4 = 1
        t6.push(t1[0])
        stor.signes.push('+')
      } else { stor.signes.push(t1[0]) }
      for (let i = 1; i < t1.length; i++) {
        t5 = ((t1[i] === '+') || (t1[i] === '-'))
        if (!t5) {
          t4++
          t6.push(t1[i])
        } else { stor.signes.push(t1[i]) }
        if (t5 === t3) {
          stor.errFoisauMil = true
          stor.zoneRed.corrige(false)
          return false
        }
        t3 = t5
      }

      for (let i = 0; i < t6.length; i++) {
        n3 = false
        if (t6[i].indexOf(stor.nomvar) !== t6[i].lastIndexOf(stor.nomvar)) {
          stor.errFoisauMil = true
          stor.zoneRed.corrige(false)
          return false
        }
        if (t6[i].indexOf('²') !== t6[i].lastIndexOf('²')) {
          stor.errFoisauMil = true
          stor.zoneRed.corrige(false)
          return false
        }
        n6 = false
        if (t6[i].includes('²')) { n6 = true }
        if (t6[i].includes(stor.nomvar)) {
          n3 = true
          if (t6[i].indexOf(stor.nomvar) !== t6[i].length - 1) {
            if ((t6[i].indexOf(stor.nomvar) !== t6[i].length - 2) || t6[i][t6[i].length - 1] !== '²') {
              stor.errFoisauMil = true
              stor.zoneRed.corrige(false)
              return false
            }
          }
        }
        n4 = t6[i].replace(stor.nomvar + '²', '').replace(stor.nomvar, '')
        if (n4.indexOf('²') !== -1) {
          stor.errFoisauMil = true
          stor.zoneRed.corrige(false)
          return false
        }
        if (n3 && n4 === '1') {
          stor.errFoisauMil = true
          stor.zoneRed.corrige(false)
          return false
        }
        n5.push({ n: extnumden(n4), x: n3, car: n6 })
      }
      ll1 = []
      // verif yen a pas 1 de faux
      for (let i = 0; i < n5.length; i++) {
        ok = false
        for (let j = 0; j < stor.t3.length; j++) {
          if ((egalite2(n5[i].n, stor.t3[j].n)) && (n5[i].x === stor.t3[j].x) && (n5[i].car === stor.t3[j].car) && ll1[j] === undefined) {
            ll1[j] = i
            ok = true
            break
          }
        }
        if (!ok) {
          stor.errSimFo = true
          stor.zoneRed.corrige(false)
          return false
        }
      }

      // verfi ya tout...
      if (n5.length !== stor.t3.length) {
        stor.errNbFacteurManq = true
        stor.zoneRed.corrige(false)
        ok = false
      }
      // verif les signes
      for (let i = 0; i < stor.signes.length; i++) {
        stor.signes[i] = (stor.signes[i] !== '-')
      }
      for (let i = 0; i < ll1.length; i++) {
        t2 = stor.t3[i].n.num * stor.t3[i].signe > 0
        if (t2 !== stor.signes[ll1[i]]) {
          stor.errSigne = true
          stor.zoneRed.corrige(false)
          ok = false
        }
      }
      return ok
    }

    return ok
  } // isRepOk
  function desactiveAll () {
    if (stor.encours === 'prod') {
      for (const prod of stor.lesprod) {
        prod.liste.disable()
        prod.zsm1.disable()
        prod.zsm2.disable()
      }
      j3pEmpty(stor.divencours[3])
      stor.divencours[2].style.border = ''
    }
    if (stor.encours === 'simpl') {
      stor.zoneSimpl.disable()
      if (!ds.produit) stor.brouill.disable()
    }
    if (stor.encours === 'red') {
      stor.zoneRed.disable()
      if (!ds.produit || !ds.simplification) stor.brouillRed.disable()
    }
  } // desactiveAll
  function passeToutVert () {
    let i
    let bb
    if (stor.encours === 'prod') {
      for (const prod of stor.lesprod) {
        bb = prod.liste.reponse
        if (bb === '+' && i === 0) bb = ''
        prod.liste.corrige(true)
        prod.zsm1.corrige(true)
        prod.zsm2.corrige(true)
        prod.fois.style.color = me.styles.cbien
      }
    }
    if (stor.encours === 'simpl') {
      stor.zoneSimpl.corrige(true)
    }
    if (stor.encours === 'red') {
      stor.zoneRed.corrige(true)
    }
  } // passeToutVert
  function barrelesfo () {
    if (stor.encours === 'prod') {
      for (const prod of stor.lesprod) {
        // attention, liste n’est pas forcément une ListeDeroulante !
        if (prod.liste.barreIfKo) prod.liste.barreIfKo()
        prod.zsm1.barreIfKo()
        prod.zsm2.barreIfKo()
      }
    }
    if (stor.encours === 'simpl') {
      stor.zoneSimpl.barre()
    }
    if (stor.encours === 'red') {
      stor.zoneRed.barre()
    }
  }
  function affCorrFaux (isFin) {
    /// //affiche indic

    if (stor.errNbFacteurTrop) {
      j3pAffiche(stor.lesdiv.explications, null, 'Il y a trop de termes !\n')
      stor.yaexplik = true
    }
    if (stor.errNbFacteurManq) {
      j3pAffiche(stor.lesdiv.explications, null, 'Il n’y a pas assez de termes !\n')
      stor.yaexplik = true
    }
    if (stor.errFoisauMil) {
      j3pAffiche(stor.lesdiv.explications, null, 'Il y a des erreurs d’écritures !\n')
      stor.yaexplik = true
    }
    if (stor.errExiste) {
      j3pAffiche(stor.lesdiv.explications, null, 'Justification fausse, \nje ne retrouve pas les facteurs attendus !\n')
      stor.yaexplik = true
    }
    if (stor.errYapa) {
      j3pAffiche(stor.lesdiv.explications, null, 'Je ne trouve pas tous les produits attendus !\n')
      stor.yaexplik = true
    }
    if (stor.errPasAtt) {
      j3pAffiche(stor.lesdiv.explications, null, 'Des produits sont faux (regarde les flèches ci-dessus) !\n')
      stor.yaexplik = true
    }
    if (stor.errSigne) {
      j3pAffiche(stor.lesdiv.explications, null, 'Erreur de signe !\n')
      stor.yaexplik = true
    }
    if (stor.errSimFo) {
      j3pAffiche(stor.lesdiv.explications, null, 'Erreur de calcul !\n')
      stor.yaexplik = true
    }

    if (stor.errProp) {
      j3pAffiche(stor.lesdiv.explications, null, 'Il faut utiliser la bonne propriété !\n')
      stor.yaexplik = true
    }
    if (stor.errDouble) {
      j3pAffiche(stor.lesdiv.explications, null, 'Tu écris plusieurs fois la même longueur !\n')
      stor.yaexplik = true
    }
    if (stor.errMauvaistri) {
      j3pAffiche(stor.lesdiv.explications, null, 'Les trois longueurs d’un même triangle doivent être \nsoit toutes en numérateur, soit toutes en dénominateur !\n')
      stor.yaexplik = true
      stor.compteurPe1++
    }
    if (stor.errPasCoress) {
      j3pAffiche(stor.lesdiv.explications, null, 'Des longueurs ne se correspondent pas !\n')
      stor.yaexplik = true
      stor.compteurPe1++
    }
    if (stor.errPascote) {
      j3pAffiche(stor.lesdiv.explications, null, 'On ne peut utiliser que les longueurs des côtés des deux triangles !\n')
      stor.yaexplik = true
      stor.compteurPe1++
    }
    if (stor.errCopie) {
      j3pAffiche(stor.lesdiv.explications, null, 'Il faut prendre les longueurs de l’énoncé !\n')
      stor.yaexplik = true
    }
    if (stor.errDouble2) {
      j3pAffiche(stor.lesdiv.explications, null, 'Tu écris plusieurs fois la même longueur !\n')
      stor.yaexplik = true
    }
    if (stor.errProduit) {
      j3pAffiche(stor.lesdiv.explications, null, 'On multiplie en diagonale, puis on divise par le troisième !\n')
      stor.yaexplik = true
      stor.compteurPe3++
    }
    if (stor.errSimpli) {
      j3pAffiche(stor.lesdiv.explications, null, 'La réponse doit être simplifiée !\n')
      stor.yaexplik = true
      stor.compteurPe4++
    }
    if (stor.errMalEcrit) {
      j3pAffiche(stor.lesdiv.explications, null, 'La réponse est mal écrite !\n')
      stor.yaexplik = true
      stor.compteurPe2++
    }
    if (stor.errVirgFrac) {
      j3pAffiche(stor.lesdiv.explications, null, 'Il n’y a pas de virgule dans une fraction !\n')
      stor.yaexplik = true
    }

    if (isFin) {
      // barre les faux
      barrelesfo()
      desactiveAll()
      let t1, t2
      stor.yaco = true
      // afficheco
      if (document.getElementById('Fleches')) faitFleches()
      if (!ds.fleches) faitFleches()
      if (stor.encours === 'prod') {
        t1 = 'A = '
        for (let i = 0; i < stor.t1.length; i++) {
          t2 = stor.t1[i].f1.n.num * stor.t1[i].f2.n.num * stor.t1[i].signe > 0
          if (t2) { if (i !== 0) t1 += '+' } else { t1 += '-' }
          t1 += affichemoibien(stor.t1[i].f1.n, stor.t1[i].f1.yav, false).replace('+', '').replace('-', '')
          t1 += '  \\times '
          t1 += affichemoibien(stor.t1[i].f2.n, stor.t1[i].f2.yav, false).replace('+', '').replace('-', '')
        }
        j3pAffiche(stor.lesdiv.solution, null, '$' + t1 + '$')
      }
      if (stor.encours === 'simpl') {
        t1 = 'A = '
        for (let i = 0; i < stor.t2.length; i++) {
          t2 = stor.t2[i].n.num * stor.t2[i].signe > 0
          if (t2) { if (i !== 0) t1 += '+' } else { t1 += '-' }
          t1 += affichemoibien(stor.t2[i].n, stor.t2[i].x, false, stor.t2[i].car).replace('+', '').replace('-', '')
        }
        j3pAffiche(stor.lesdiv.solution, null, '$' + t1 + '$')
      }
      if (stor.encours === 'red') {
        t1 = 'A = '
        for (let i = 0; i < stor.t2.length; i++) {
          t2 = stor.t2[i].n.num * stor.t2[i].signe > 0
          if (t2) { if (i !== 0) t1 += '+' } else { t1 += '-' }
          t1 += affichemoibien(stor.t2[i].n, stor.t2[i].x, false, stor.t2[i].car).replace('+', '').replace('-', '')
        }
        if (stor.t2.length !== stor.t3.length) {
          t1 += '$<br>$ A = '
          for (let i = 0; i < stor.t3.length; i++) {
            t2 = stor.t3[i].n.num * stor.t3[i].signe > 0
            if (t2) { if (i !== 0) t1 += '+' } else { t1 += '-' }
            t1 += affichemoibien(stor.t3[i].n, stor.t3[i].x, false, stor.t3[i].car).replace('+', '').replace('-', '')
          }
        }
        j3pAffiche(stor.lesdiv.solution, null, '$' + t1 + '$')
      }
    }
  } // affCorrFaux

  function enonceMain () {
    if (((me.questionCourante % ds.nbetapes) === 1) || (ds.nbetapes === 1)) {
      // A chaque répétition, on nettoie la scène
      // A commenter si besoin
      me.videLesZones()
    }

    creeLesDiv()
    stor.lesdiv.travail.classList.add('travail')
    stor.lesdiv.consigneG.classList.add('enonce')
    stor.Lexo = faisChoixExos()
    poseQuestion()
    if (ds.Calculatrice) {
      me.fenetresjq = [{ name: 'Calculatrice', title: 'Une calculatrice', left: 600, top: 10 }]
      j3pCreeFenetres(me)
      j3pAfficheCroixFenetres('Calculatrice')
      j3pAjouteBouton(stor.lesdiv.calculatrice, 'Calculatrice', 'MepBoutons', 'Calculatrice', boutonfen)
    }
    me.finEnonce()
  }

  switch (this.etat) {
    case 'enonce':
      // code exécuté au lancement de la section
      if (me.debutDeLaSection) {
        initSection()
      } else {
        enonceMain()
      }
      break // case "enonce":

    case 'correction':
      // On teste si une réponse a été saisie
      stor.yaexplik = false
      stor.yaco = false
      stor.lesdiv.explications.innerHTML = ''
      stor.lesdiv.solution.innerHTML = ''

      if (yaReponse()) {
        // Bonne réponse
        if (isRepOk()) {
          stor.lesdiv.correction.style.color = this.styles.cbien
          stor.lesdiv.correction.innerHTML = cBien

          desactiveAll()
          passeToutVert()
          if (stor.encours === 'prod') {
            if (document.getElementById('Fleches')) j3pDetruit('Fleches')
            stor.divencours = stor.tabCont.cells[2]
            if (ds.simplification) {
              if (stor.t2.length !== stor.t3.length) {
                faitSimpl()
                stor.niv++
                return
              } else {
                stor.Yaeusimpl = true
                faitReduc()
                stor.niv++
                return
              }
            }
            if (ds.reduction) {
              if (stor.t2.length === stor.t3.length) stor.Yaeusimpl = true
              faitReduc()
              stor.niv++
              return
            }
          }
          if (stor.encours === 'simpl') {
            if (ds.reduction) {
              faitReduc()
              stor.niv++
              return
            }
          }
          stor.encours = ''

          this.score++
          this.typederreurs[0]++
          this.etat = 'navigation'
          this.sectionCourante()
        } else {
          // Pas de bonne réponse
          stor.lesdiv.correction.style.color = this.styles.cfaux
          stor.lesdiv.correction.innerHTML = cFaux
          if (this.isElapsed) {
            // A cause de la limite de temps :
            stor.lesdiv.correction.innerHTML += tempsDepasse + '<BR>'
            this.typederreurs[10]++
            affCorrFaux(true)
            stor.yaco = true
            this.sectionCourante('navigation')
          } else {
            // Réponse fausse :
            if (me.essaiCourant < ds.nbchances) {
              stor.lesdiv.correction.innerHTML += '<br>' + essaieEncore

              affCorrFaux(false)

              // affCorrFaux.call(this, false)
              this.typederreurs[1]++
            } else {
              // Erreur au nème essai
              affCorrFaux(true)
              stor.yaco = true

              if (stor.encours === 'compare') {
                this.score = j3pArrondi(this.score + 0.1, 1)
              }
              if (stor.encours === 'calcul') {
                this.score = j3pArrondi(this.score + 0.4, 1)
              }
              if (stor.encours === 'concl') {
                this.score = j3pArrondi(this.score + 0.7, 1)
              }

              this.typederreurs[2]++
              this.sectionCourante('navigation')
            }
          }
        }
      } else {
        // pas de réponse
        stor.lesdiv.correction.style.color = this.styles.cfaux
        stor.lesdiv.correction.innerHTML = reponseIncomplete
      }
      if (stor.yaexplik) stor.lesdiv.explications.classList.add('explique')
      if (stor.yaco) stor.lesdiv.solution.classList.add('correction')
      this.finCorrection()
      break // case "correction":

    case 'navigation':
      if (this.sectionTerminee()) {
        this.parcours.pe = getPe(ds, stor)
      } else {
        this.etat = 'enonce'
      }
      this.finNavigation()
      break // case "navigation":
  }
}
