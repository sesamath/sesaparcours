import { j3pAddElt, j3pAjouteBouton, j3pArrondi, j3pClone, j3pDetruit, j3pEmpty, j3pGetRandomInt, j3pPGCD, j3pShuffle } from 'src/legacy/core/functions'
import ListeDeroulante from 'src/legacy/outils/listeDeroulante/ListeDeroulante'
import { getPe, initCptPe } from 'src/legacy/sections/college/helperTommy'
import { j3pAffiche } from 'src/lib/mathquill/functions'
import { addDefaultTable, addDefaultTableDetailed } from 'src/legacy/themes/table'
import ZoneStyleMathquill3 from 'src/legacy/outils/zoneStyleMathquill/ZoneStyleMathquill3'
import { j3pCreeFenetres, j3pAfficheCroixFenetres, j3pToggleFenetres } from 'src/legacy/core/functionsJqDialog'
import BrouillonCalculs from 'src/legacy/outils/brouillon/BrouillonCalculs'
import textesGeneriques from 'src/lib/core/textes'

import imgPlus from './plus.png'
import imgMoins from './moins.png'

const { cBien, cFaux, essaieEncore, reponseIncomplete, tempsDepasse } = textesGeneriques

// Nos pe
const pe1 = 'Egalite'
const pe2 = 'Ecriture'
const pe3 = 'Produit_en_croix'
const pe4 = 'Simplification'
/**
 * Ajouter une description ici
 * @fileOverview
 * @author Tommy Barroy <t.barroy@laposte.net>
 * @since 2017-10
*/
export const params = {
  outils: ['mathquill', 'calculatrice'],
  /**
   * Ce qui est paramétrable dans l’éditeur de graphe
   * @type {LegacySectionParamsParametres}
   */
  parametres: [
    ['nbrepetitions', 9, 'entier', 'Nombre de répétitions de la section'],
    ['nbchances', 2, 'entier', 'Nombre d’essais possibles.'],
    ['nombres', 'entiers', 'liste', '<u>entiers</u>:  Les coefficients sont entiers. <br><br> <u>fractions</u>: Les coeffiients sont des fractions.', ['entiers', 'fractions', 'les deux']],
    ['Calculatrice', true, 'boolean', '<u>true</u>:  Une calculatrice est disponible'],
    // le modèle gère la compatibilité ascendante avec l’ancien paramètre MiseEnPagePlusClaire
    ['theme', 'standard', 'liste', 'Modèle pour l’affichage', ['standard', 'zonesAvecImageDeFond']]
  ],
  // dans le cas d’une section QUANTITATIVE
  pe: [
    { pe_1: pe1 },
    { pe_2: pe2 },
    { pe_3: pe3 },
    { pe_4: pe4 }
  ]
}

/**
 * section develop01
 * @this {Parcours}
 */
export default function main () {
  const me = this
  const stor = me.storage
  const ds = me.donneesSection

  function creeLesDiv () {
    stor.lesdiv = {}
    stor.lesdiv.conteneur = j3pAddElt(me.zonesElts.MG, 'div', {
      style: me.styles.etendre('toutpetit.enonce', { padding: '10px' })
    })
    stor.lesdiv.consigneG = addDefaultTable(stor.lesdiv.conteneur, 1, 1)[0][0]
    stor.lesdiv.etape = addDefaultTable(stor.lesdiv.conteneur, 1, 1)[0][0]
    stor.lesdiv.travail = addDefaultTable(stor.lesdiv.conteneur, 1, 1)[0][0]
    stor.lesdiv.explications = j3pAddElt(stor.lesdiv.conteneur, 'div')
    stor.lesdiv.solution = j3pAddElt(stor.lesdiv.conteneur, 'div')
    stor.lesdiv.calculatrice = j3pAddElt(me.zonesElts.MD, 'div', {
      style: me.styles.etendre('toutpetit.enonce', { padding: '10px' })
    })
    stor.lesdiv.aide = j3pAddElt(me.zonesElts.MD, 'div', {
      style: me.styles.etendre('toutpetit.enonce', { padding: '10px' })
    })
    stor.lesdiv.correction = j3pAddElt(me.zonesElts.MD, 'div', {
      style: me.styles.etendre('toutpetit.enonce', { padding: '10px' })
    })
    stor.lesdiv.etape.style.color = me.styles.toutpetit.correction.color
    stor.lesdiv.solution.style.color = me.styles.toutpetit.correction.color
    stor.lesdiv.explications.style.color = me.styles.cfaux
    me.zonesElts.MG.classList.add('fond')
  }

  function initSection () {
    initCptPe(ds, stor)

    let lp = []
    stor.exos = []
    if ((ds.nombres === 'entiers') || (ds.nombres === 'les deux')) lp.push('ent')
    if ((ds.nombres === 'fractions') || (ds.nombres === 'les deux')) lp.push('frac')
    lp = j3pShuffle(lp)
    for (let i = 0; i < ds.nbrepetitions; i++) {
      stor.exos.push({ type: lp[i % lp.length] })
    }
    stor.exos = j3pShuffle(stor.exos)

    // Construction de la page
    me.construitStructurePage('presentation1bis')

    me.typederreurs = [0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0]

    me.score = 0

    me.afficheTitre('Développer')
    enonceMain()
  }

  function faisChoixExos () {
    const e = stor.exos.pop()
    e.formule = '-'
    let num, den
    num = j3pGetRandomInt(2, 9)
    den = premsavec(num)
    stor.a = { num, den }
    num = j3pGetRandomInt(1, 9)
    den = premsavec(num)
    stor.b = { num, den }
    stor.c = j3pClone(stor.a)
    stor.d = j3pClone(stor.b)
    stor.restric = '0123456789/'
    if (e.type === 'ent') {
      stor.a.den = stor.b.den = stor.c.den = stor.d.den = 1
      stor.restric = '0123456789'
    }
    const fg = 'xyzabt'
    stor.nomvar = fg[j3pGetRandomInt(0, 5)]
    stor.restric += stor.nomvar
    stor.lx2 = j3pGetRandomInt(1, 2)
    stor.lx1 = stor.lx2
    stor.t3 = [{ n: { num: stor.a.num * stor.a.num, den: stor.a.den * stor.a.den }, x: stor.lx1 === 1, car: stor.lx1 === 1, signe: true }, { n: { num: stor.b.num * stor.b.num, den: stor.b.den * stor.b.den }, x: stor.lx1 === 2, car: stor.lx1 === 2, signe: false }]
    return e
  }
  function poseQuestion () {
    stor.asup = []
    stor.Yaeusimpl = false
    const ts = addDefaultTableDetailed(stor.lesdiv.consigneG, 1, 3)
    ts.table.style.padding = 0
    j3pAffiche(ts.cells[0][0], null, '<b> On veut développer </b> l’expression &nbsp; ')
    j3pAffiche(ts.cells[0][1], null, '$ A = $')
    j3pAffiche(ts.cells[0][1], null, affichemonexp(false))
    stor.tabCont = addDefaultTableDetailed(stor.lesdiv.travail, 4, 4)
    stor.tabCont.table.style.padding = 0
    for (let i = 0; i < 4; i++) {
      stor.tabCont.lines[i].style.padding = 0
      for (let j = 0; j < 4; j++) {
        stor.tabCont.cells[i][j].style.padding = 0
      }
    }
    affichemonexp(true)
    stor.divencours = stor.tabCont.cells[1]
    faitReduc()
  }

  function faitReduc () {
    j3pEmpty(stor.lesdiv.etape)
    for (let i = 0; i < stor.asup.length; i++) {
      j3pDetruit(stor.asup[i])
    }
    stor.asup = []
    stor.encours = 'red'
    let doudpon = stor.tabCont.cells[1]
    stor.divencours = stor.tabCont.cells[2]
    if (stor.a.num !== 0 && stor.b.num !== 0) {
      j3pAffiche(stor.lesdiv.etape, null, '<u>Étape</u>: Donner la réponse sous la forme d’une expression réduite.')
    } else {
      j3pAffiche(stor.lesdiv.etape, null, '<u>Étape</u>: Donne le résultat du développement')
    }
    if (ds.produit) {
      if (!stor.Yaeusimpl) {
        doudpon = stor.tabCont.cells[2]
        stor.divencours = stor.tabCont.cells[3]
      }
    }
    if (!stor.Yaeusimpl) {
      let pppo = 1
      if (!ds.produit && !stor.Yaeusimpl) pppo++
      stor.brouillRed = new BrouillonCalculs({
        caseBoutons: doudpon[0],
        caseEgal: doudpon[1],
        caseCalculs: doudpon[2],
        caseApres: doudpon[3]
      }, {
        limite: 30,
        limitenb: 20,
        restric: '0123456789*/,.²+-()*' + stor.nomvar,
        hasAutoKeyboard: false,
        lmax: pppo
      })
    }
    if (stor.Yaeusimpl) {
      stor.divencours = stor.tabCont.cells[3]
      stor.brouillRed = { disable: function () {} }
    }
    j3pAffiche(stor.divencours[0], null, '$A$')
    j3pAffiche(stor.divencours[1], null, '$=$')
    stor.zoneRed = new ZoneStyleMathquill3(stor.divencours[2], { inverse: true, restric: stor.restric + '+-²', bloqueFraction: '+-²' + stor.nomvar, glissefrac: '²' + stor.nomvar, hasAutoKeyboard: true, enter: me.sectionCourante.bind(me) })
    stor.zoneRed.focus()
  }
  function affichefoisBis (where, bb) {
    const tt = addDefaultTableDetailed(where, 1, 5)
    tt.table.style.padding = 0
    tt.lines[0].style.padding = 0
    for (let j = 0; j < 5; j++) {
      tt.cells[0][j].style.padding = 0
    }
    j3pAffiche(tt.cells[0][3], null, '$\\times$')
    if (bb === '') j3pAffiche(tt.cells[0][1], null, '&nbsp;&nbsp;')
    j3pAffiche(tt.cells[0][0], null, '&nbsp;')
    return tt.cells[0]
  }
  function AddProd (bb) {
    if (stor.nbprod === 5) return
    stor.nbprod++
    const t = affichefoisBis(stor.tabProd.cells[0][stor.nbprod], bb)
    let lsi
    if (!ds.AideSigne || !ds.Positifs || !ds.NbProduitFixe) {
      const ll = ['&nbsp;', '+', '-']
      lsi = ListeDeroulante.create(t[1], ll, { centre: true, sansFleche: true })
    } else {
      let gg
      if (stor.nbprod % 2 === 1) { gg = '+' } else {
        gg = stor.Lexo.formule
      }
      lsi = { reponse: gg, disable: function () {}, corrige: function () {}, changed: true, conteneur: t[1] }
      if (stor.nbprod === 1 && gg === '+') gg = ''
      j3pAffiche(t[1], null, '$' + gg + '$')
    }
    const zsm1 = new ZoneStyleMathquill3(t[2], { inverse: true, restric: stor.restric, hasAutoKeyboard: false, bloqueFraction: stor.nomvar, glissefrac: '²' + stor.nomvar, enter: me.sectionCourante.bind(me) })
    const zsm2 = new ZoneStyleMathquill3(t[4], { inverse: true, restric: stor.restric, hasAutoKeyboard: false, bloqueFraction: stor.nomvar, glissefrac: '²' + stor.nomvar, enter: me.sectionCourante.bind(me) })
    stor.lesprod.push({ liste: lsi, zsm1, zsm2, fois: t[3] })
    metsBoutProd()
  }
  function metsBoutProd (ou) {
    j3pEmpty(stor.divencours[3])
    const mai = addDefaultTableDetailed(stor.divencours[3], 2, 1)
    mai.table.style.padding = 0
    mai.lines[0].style.padding = 0
    mai.lines[1].style.padding = 0
    mai.cells[0][0].style.padding = 0
    mai.cells[1][0].style.padding = 0
    const ttim = j3pAddElt(mai.cells[0][0], 'img')
    ttim.style.width = '15px'
    ttim.style.height = '15px'
    ttim.style.display = 'none'
    const ttim2 = j3pAddElt(mai.cells[1][0], 'img')
    ttim2.style.width = '15px'
    ttim2.style.height = '15px'
    ttim2.style.display = 'none'
    if (stor.nbprod < 5) {
      ttim.src = imgPlus
      ttim.style.width = '15px'
      ttim.style.cursor = 'pointer'
      ttim.style.display = ''
      ttim.addEventListener('click', AddProd, false)
    }
    if (stor.nbprod > 1) {
      ttim2.src = imgMoins
      ttim2.style.width = '15px'
      ttim2.style.cursor = 'pointer'
      ttim2.style.display = ''
      ttim2.addEventListener('click', SouProd, false)
    }
  }
  function SouProd () {
    if (stor.nbprod === 1) return
    const agere = stor.lesprod.pop()
    agere.liste.disable()
    agere.zsm1.disable()
    agere.zsm2.disable()
    j3pEmpty(stor.tabProd.cells[0][stor.nbprod])
    stor.nbprod--
    metsBoutProd()
  }
  function affichemonexp (bool) {
    let ret, yo
    const a = affichemoibien(stor.a, stor.lx1 === 1, false)
    yo = a !== ''
    const b = affichemoibien(stor.b, stor.lx1 === 2, yo)
    const c = affichemoibien(stor.c, stor.lx2 === 1, false)
    yo = c === ''
    const g = affichemoibien(stor.d, stor.lx2 === 2, yo)
    if (!bool) {
      ret = ''
      if ((a !== '') && (b !== '')) {
        ret = '$(' + a + b + ')'
      } else {
        ret = '$' + a + b
      }
      if ((c !== '') && (g !== '')) {
        ret += '(' + c + stor.Lexo.formule + g + ')$'
      } else {
        ret = c + stor.Lexo.formule + g + '$'
      }
      return ret
    } else {
      stor.tabZed = addDefaultTable(stor.tabCont.cells[0][2], 3, 8)
      j3pAffiche(stor.tabCont.cells[0][0], null, '$A$')
      j3pAffiche(stor.tabCont.cells[0][1], null, '$=$')
      j3pAffiche(stor.tabZed[1][1], null, '$' + a + '$')
      j3pAffiche(stor.tabZed[1][2], null, '$' + b + '$')
      j3pAffiche(stor.tabZed[1][5], null, '$' + c + '$')
      j3pAffiche(stor.tabZed[1][6], null, '$' + stor.Lexo.formule + g + '$')
      if ((a !== '') && (b !== '')) {
        j3pAffiche(stor.tabZed[1][0], null, '$($')
        j3pAffiche(stor.tabZed[1][3], null, '$)$')
      }
      if ((c !== '') && (g !== '')) {
        j3pAffiche(stor.tabZed[1][4], null, '$($')
        j3pAffiche(stor.tabZed[1][7], null, '$)$')
      }
    }
  }
  function affichemoibien (f, x, fautsigne, car) {
    let j = ''
    if (car === true) j = '²'
    if (f.num === 0) return ''
    let ret = ''
    if (f.den === 1) {
      if (f.num < 0) { ret += '-' } else if (fautsigne) { ret += '+' }
      if (x && Math.abs(f.num) === 1) {
        return ret + stor.nomvar + j
      } else {
        ret += Math.abs(f.num)
      }
    } else {
      if (f.num < 0) { ret += '-' } else if (fautsigne) { ret += '+' }
      ret += '\\frac{' + Math.abs(f.num) + '}{' + f.den + '}'
    }
    if (x) ret += stor.nomvar + j
    return ret
  }
  function boutonfen () {
    j3pToggleFenetres('Calculatrice')
  }
  function premsavec (x) {
    let ret
    do {
      ret = j3pGetRandomInt(2, 9)
    } while (j3pPGCD(ret, x, { negativesAllowed: true, valueIfZero: 1 }) !== 1)
    return ret
  }
  function egalite2 (a, b) {
    return (Math.abs(a.num * b.den) === Math.abs(a.den * b.num))
  }
  function extnumden (s) {
    if (s.indexOf('ù') === -1) {
      if (s === '') return { num: 1, den: 1 }
      return { num: parseFloat(s), den: 1 }
    } else {
      return { num: parseFloat(s.substring(1, s.indexOf('#'))), den: parseFloat(s.substring(s.indexOf('#') + 1, s.length - 1)) }
    }
  }

  function yaReponse () {
    if (stor.encours === 'prod') {
      for (const prod of stor.lesprod) {
        if (!prod.liste.changed) {
          prod.liste.focus()
          return false
        }
        if (prod.zsm1.reponse() === '') {
          prod.zsm1.focus()
          return false
        }
        if (prod.zsm2.reponse() === '') {
          prod.zsm2.focus()
          return false
        }
      }
      return true
    }
    if (stor.encours === 'simpl') {
      if (stor.zoneSimpl.reponse() === '') {
        stor.zoneSimpl.focus()
        return false
      }
      return true
    }
    if (stor.encours === 'red') {
      if (stor.zoneRed.reponse() === '') {
        stor.zoneRed.focus()
        return false
      }
      return true
    }
  }
  function isRepOk () {
    stor.errNbFacteurTrop = false
    stor.errNbFacteurManq = false
    stor.errFoisauMil = false
    stor.errExiste = false
    stor.errYapa = false
    stor.errPasAtt = false
    stor.errSigne = false
    stor.errSimFo = false

    let ok = true
    let t2, t3, t5
    let n3, n4, n6

    const t1 = stor.zoneRed.rep()
    const t6 = []
    const n5 = []
    stor.signes = []
    // verif pas deux signes à la suite
    t3 = ((t1[0] === '+') || (t1[0] === '-'))
    if (!t3) {
      t6.push(t1[0])
      stor.signes.push('+')
    } else { stor.signes.push(t1[0]) }
    for (let i = 1; i < t1.length; i++) {
      t5 = ((t1[i] === '+') || (t1[i] === '-'))
      if (!t5) {
        t6.push(t1[i])
      } else { stor.signes.push(t1[i]) }
      if (t5 === t3) {
        stor.errFoisauMil = true
        stor.zoneRed.corrige(false)
        return false
      }
      t3 = t5
    }

    for (let i = 0; i < t6.length; i++) {
      n3 = false
      if (t6[i].indexOf(stor.nomvar) !== t6[i].lastIndexOf(stor.nomvar)) {
        stor.errFoisauMil = true
        stor.zoneRed.corrige(false)
        return false
      }
      if (t6[i].indexOf('²') !== t6[i].lastIndexOf('²')) {
        stor.errFoisauMil = true
        stor.zoneRed.corrige(false)
        return false
      }
      n6 = false
      if (t6[i].includes('²')) { n6 = true }
      if (t6[i].includes(stor.nomvar)) {
        n3 = true
        if (t6[i].indexOf(stor.nomvar) !== t6[i].length - 1) {
          if ((t6[i].indexOf(stor.nomvar) !== t6[i].length - 2) || t6[i][t6[i].length - 1] !== '²') {
            stor.errFoisauMil = true
            stor.zoneRed.corrige(false)
            return false
          }
        }
      }
      n4 = t6[i].replace(stor.nomvar + '²', '').replace(stor.nomvar, '')
      if (n4.indexOf('²') !== -1) {
        stor.errFoisauMil = true
        stor.zoneRed.corrige(false)
        return false
      }
      if (n3 && n4 === '1') {
        stor.errFoisauMil = true
        stor.zoneRed.corrige(false)
        return false
      }
      n5.push({ n: extnumden(n4), x: n3, car: n6 })
    }
    const ll1 = []
    // verif yen a pas 1 de faux
    for (let i = 0; i < n5.length; i++) {
      ok = false
      for (let j = 0; j < stor.t3.length; j++) {
        if ((egalite2(n5[i].n, stor.t3[j].n)) && (n5[i].x === stor.t3[j].x) && (n5[i].car === stor.t3[j].car) && ll1[j] === undefined) {
          ll1[j] = i
          ok = true
          break
        }
      }
      if (!ok) {
        stor.errSimFo = true
        stor.zoneRed.corrige(false)
        return false
      }
    }

    // verfi ya tout...
    if (n5.length !== stor.t3.length) {
      stor.errNbFacteurManq = true
      stor.zoneRed.corrige(false)
      ok = false
    }
    // verif les signes
    for (let i = 0; i < stor.signes.length; i++) {
      stor.signes[i] = (stor.signes[i] !== '-')
    }
    for (let i = 0; i < ll1.length; i++) {
      t2 = stor.t3[i].n.num * stor.t3[i].signe > 0
      if (t2 !== stor.signes[ll1[i]]) {
        stor.errSigne = true
        stor.zoneRed.corrige(false)
        ok = false
      }
    }
    return ok
  } // isRepOk
  function desactiveAll () {
    if (stor.encours === 'prod') {
      for (const prod of stor.lesprod) {
        prod.liste.disable()
        prod.zsm1.disable()
        prod.zsm2.disable()
      }
      j3pEmpty(stor.divencours[3])
      stor.divencours[2].style.border = ''
    }
    if (stor.encours === 'simpl') {
      stor.zoneSimpl.disable()
      if (!ds.produit) stor.brouill.disable()
    }
    if (stor.encours === 'red') {
      stor.zoneRed.disable()
      if (!ds.produit || !ds.simplification) stor.brouillRed.disable()
    }
  } // desactiveAll
  function passeToutVert () {
    let i
    let bb
    if (stor.encours === 'prod') {
      for (const prod of stor.lesprod) {
        bb = prod.liste.reponse
        if (bb === '+' && i === 0) bb = ''
        prod.liste.corrige(true)
        prod.zsm1.corrige(true)
        prod.zsm2.corrige(true)
        prod.fois.style.color = me.styles.cbien
      }
    }
    if (stor.encours === 'simpl') {
      stor.zoneSimpl.corrige(true)
    }
    if (stor.encours === 'red') {
      stor.zoneRed.corrige(true)
    }
  } // passeToutVert
  function barrelesfo () {
    if (stor.encours === 'prod') {
      for (const prod of stor.lesprod) {
        // attention, liste n’est pas forcément une ListeDeroulante !
        if (prod.liste.barreIfKo) prod.liste.barreIfKo()
        prod.zsm1.barreIfKo()
        prod.zsm2.barreIfKo()
      }
    }
    if (stor.encours === 'simpl') {
      stor.zoneSimpl.barre()
    }
    if (stor.encours === 'red') {
      stor.zoneRed.barre()
    }
  }
  function affCorrFaux (isFin) {
    /// //affiche indic

    if (stor.errNbFacteurTrop) {
      j3pAffiche(stor.lesdiv.explications, null, 'Il y a trop de termes !\n')
      stor.yaexplik = true
    }
    if (stor.errNbFacteurManq) {
      j3pAffiche(stor.lesdiv.explications, null, 'Il n’y a pas assez de termes !\n')
      stor.yaexplik = true
    }
    if (stor.errFoisauMil) {
      j3pAffiche(stor.lesdiv.explications, null, 'Il y a des erreurs d’écritures !\n')
      stor.yaexplik = true
    }
    if (stor.errExiste) {
      j3pAffiche(stor.lesdiv.explications, null, 'Justification fausse, \nje ne retrouve pas les facteurs attendus !\n')
      stor.yaexplik = true
    }
    if (stor.errYapa) {
      j3pAffiche(stor.lesdiv.explications, null, 'Je ne trouve pas tous les produits attendus !\n')
      stor.yaexplik = true
    }
    if (stor.errPasAtt) {
      j3pAffiche(stor.lesdiv.explications, null, 'Des produits sont faux (regarde les flèches ci-dessus) !\n')
      stor.yaexplik = true
    }
    if (stor.errSigne) {
      j3pAffiche(stor.lesdiv.explications, null, 'Erreur de signe !\n')
      stor.yaexplik = true
    }
    if (stor.errSimFo) {
      j3pAffiche(stor.lesdiv.explications, null, 'Erreur de calcul !\n')
      stor.yaexplik = true
    }

    if (stor.errProp) {
      j3pAffiche(stor.lesdiv.explications, null, 'Il faut utiliser la bonne propriété !\n')
      stor.yaexplik = true
    }
    if (stor.errDouble) {
      j3pAffiche(stor.lesdiv.explications, null, 'Tu écris plusieurs fois la même longueur !\n')
      stor.yaexplik = true
    }
    if (stor.errMauvaistri) {
      j3pAffiche(stor.lesdiv.explications, null, 'Les trois longueurs d’un même triangle doivent être \nsoit toutes en numérateur, soit toutes en dénominateur !\n')
      stor.yaexplik = true
      stor.compteurPe1++
    }
    if (stor.errPasCoress) {
      j3pAffiche(stor.lesdiv.explications, null, 'Des longueurs ne se correspondent pas !\n')
      stor.yaexplik = true
      stor.compteurPe1++
    }
    if (stor.errPascote) {
      j3pAffiche(stor.lesdiv.explications, null, 'On ne peut utiliser que les longueurs des côtés des deux triangles !\n')
      stor.yaexplik = true
      stor.compteurPe1++
    }
    if (stor.errCopie) {
      j3pAffiche(stor.lesdiv.explications, null, 'Il faut prendre les longueurs de l’énoncé !\n')
      stor.yaexplik = true
    }
    if (stor.errDouble2) {
      j3pAffiche(stor.lesdiv.explications, null, 'Tu écris plusieurs fois la même longueur !\n')
      stor.yaexplik = true
    }
    if (stor.errProduit) {
      j3pAffiche(stor.lesdiv.explications, null, 'On multiplie en diagonale, puis on divise par le troisième !\n')
      stor.yaexplik = true
      stor.compteurPe3++
    }
    if (stor.errSimpli) {
      j3pAffiche(stor.lesdiv.explications, null, 'La réponse doit être simplifiée !\n')
      stor.yaexplik = true
      stor.compteurPe4++
    }
    if (stor.errMalEcrit) {
      j3pAffiche(stor.lesdiv.explications, null, 'La réponse est mal écrite !\n')
      stor.yaexplik = true
      stor.compteurPe2++
    }
    if (stor.errVirgFrac) {
      j3pAffiche(stor.lesdiv.explications, null, 'Il n’y a pas de virgule dans une fraction !\n')
      stor.yaexplik = true
    }

    if (isFin) {
      // barre les faux
      barrelesfo()
      desactiveAll()
      let t1, t2
      stor.yaco = true
      // afficheco

      t1 = 'A = '
      let buft = ''
      let fop = false
      if (stor.a.den === 1 || stor.a.den === 'z') {
        buft = String(stor.a.num)
      } else {
        fop = true
        buft = '\\frac{' + stor.a.num + '}{' + stor.a.den + '}'
      }
      if (stor.lx1 === 1) {
        fop = true
        buft += stor.nomvar
      }
      if (fop) buft = '(' + buft + ')'
      t1 += buft + '² - '
      buft = ''
      fop = false
      if (stor.b.den === 1 || stor.b.den === 'z') {
        buft = String(stor.b.num)
      } else {
        fop = true
        buft = '\\frac{' + stor.b.num + '}{' + stor.b.den + '}'
      }
      if (stor.lx1 === 2) {
        fop = true
        buft += stor.nomvar
      }
      if (fop) buft = '(' + buft + ')'
      t1 += buft + '²'
      t1 += '$<br>$ A = '
      for (let i = 0; i < stor.t3.length; i++) {
        t2 = stor.t3[i].n.num * stor.t3[i].signe > 0
        if (t2) { if (i !== 0) t1 += '+' } else { t1 += '-' }
        t1 += affichemoibien(stor.t3[i].n, stor.t3[i].x, false, stor.t3[i].car).replace('+', '').replace('-', '')
      }
      j3pAffiche(stor.lesdiv.solution, null, '$' + t1 + '$')
    }
  } // affCorrFaux

  function enonceMain () {
    me.videLesZones()
    creeLesDiv()
    stor.lesdiv.travail.classList.add('travail')
    stor.lesdiv.consigneG.classList.add('enonce')
    stor.Lexo = faisChoixExos()
    poseQuestion()
    if (ds.Calculatrice) {
      me.fenetresjq = [{ name: 'Calculatrice', title: 'Une calculatrice', left: 600, top: 10 }]
      j3pCreeFenetres(me)
      j3pAfficheCroixFenetres('Calculatrice')
      j3pAjouteBouton(stor.lesdiv.calculatrice, 'Calculatrice', 'MepBoutons', 'Calculatrice', boutonfen)
    }
    me.finEnonce()
  }

  switch (this.etat) {
    case 'enonce':
      // code exécuté au lancement de la section
      if (me.debutDeLaSection) {
        initSection()
      } else {
        enonceMain()
      }
      break // case "enonce":

    case 'correction':
      // On teste si une réponse a été saisie
      stor.yaexplik = false
      stor.yaco = false
      stor.lesdiv.explications.innerHTML = ''
      stor.lesdiv.solution.innerHTML = ''

      if (yaReponse()) {
        // Bonne réponse
        if (isRepOk()) {
          stor.lesdiv.correction.style.color = this.styles.cbien
          stor.lesdiv.correction.innerHTML = cBien

          desactiveAll()
          passeToutVert()

          this.score++
          this.typederreurs[0]++
          this.etat = 'navigation'
          this.sectionCourante()
        } else {
          // Pas de bonne réponse
          stor.lesdiv.correction.style.color = this.styles.cfaux
          stor.lesdiv.correction.innerHTML = cFaux
          if (this.isElapsed) {
            // A cause de la limite de temps :
            stor.lesdiv.correction.innerHTML += tempsDepasse + '<BR>'
            this.typederreurs[10]++
            affCorrFaux(true)
            stor.yaco = true
            this.sectionCourante('navigation')
          } else {
            // Réponse fausse :
            if (me.essaiCourant < ds.nbchances) {
              stor.lesdiv.correction.innerHTML += '<br>' + essaieEncore

              affCorrFaux(false)

              // affCorrFaux.call(this, false)
              this.typederreurs[1]++
            } else {
              // Erreur au nème essai
              affCorrFaux(true)
              stor.yaco = true

              if (stor.encours === 'compare') {
                this.score = j3pArrondi(this.score + 0.1, 1)
              }
              if (stor.encours === 'calcul') {
                this.score = j3pArrondi(this.score + 0.4, 1)
              }
              if (stor.encours === 'concl') {
                this.score = j3pArrondi(this.score + 0.7, 1)
              }

              this.typederreurs[2]++
              this.sectionCourante('navigation')
            }
          }
        }
      } else {
        // pas de réponse
        stor.lesdiv.correction.style.color = this.styles.cfaux
        stor.lesdiv.correction.innerHTML = reponseIncomplete
      }
      if (stor.yaexplik) stor.lesdiv.explications.classList.add('explique')
      if (stor.yaco) stor.lesdiv.solution.classList.add('correction')
      this.finCorrection()
      break // case "correction":

    case 'navigation':
      if (this.sectionTerminee()) {
        this.parcours.pe = getPe(ds, stor)
      } else {
        this.etat = 'enonce'
      }
      this.finNavigation()
      break // case "navigation":
  }
}
