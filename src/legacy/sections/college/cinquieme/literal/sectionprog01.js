import { j3pAddElt, j3pAjouteBouton, j3pClone, j3pEmpty, j3pGetRandomBool, j3pGetRandomInt, j3pShuffle } from 'src/legacy/core/functions'
import Algebrite from 'algebrite'
import ListeDeroulante from 'src/legacy/outils/listeDeroulante/ListeDeroulante'
import { j3pAffiche } from 'src/lib/mathquill/functions'
import { addDefaultTable } from 'src/legacy/themes/table'
import ZoneStyleMathquill2 from 'src/legacy/outils/zoneStyleMathquill/ZoneStyleMathquill2'
import { j3pCreeFenetres, j3pAfficheCroixFenetres, j3pToggleFenetres } from 'src/legacy/core/functionsJqDialog'
import textesGeneriques from 'src/lib/core/textes'

const { cBien, cFaux, essaieEncore, reponseIncomplete, tempsDepasse } = textesGeneriques

/**
 * Ajouter une description ici
 * @fileOverview
 * @author Tommy Barroy <t.barroy@laposte.net>
 * @since 2017-10
*/
export const params = {
  outils: ['mathquill', 'calculatrice'],
  /**
   * Ce qui est paramétrable dans l’éditeur de graphe
   * @type {LegacySectionParamsParametres}
   */
  parametres: [
    ['nbrepetitions', 3, 'entier', 'Nombre de répétitions de la section'],
    ['nbchances', 2, 'entier', 'Nombre d’essais possibles'],
    ['limite', 0, 'entier', 'Temps disponible'],
    ['MinOperations', 1, 'entier', 'Nombre minimum d’opérations imbriquées (entre 1 et 4)'],
    ['Carre', 'jamais', 'liste', 'L’expression comporte des carrés', ['toujours', 'jamais', 'parfois']],
    ['Question', 'traduire', 'liste', '<u>traduire</u>: Il faut traduire un programme de calcul par une expression litérale. <br><br><u>comparer</u>: Déterminer si deux programmes de calculs sont équivalents en comparant leur expression associée. <br><br><u>calcul</u>: Il donner le résultat d’un programme de calcul pour une valeur donnée. <br><br><u>comparer_long</u>:<i>Ce paramètre n’est pas encore implémenté</i><br> Il faut trouver pour quelles valeurs deux programmes de calculs donnent le même résultat. ', ['traduire', 'comparer', 'comparer_long', 'calcul']],
    ['Justifie', false, 'boolean', '<i>Uniquement quand le paramètre <b>Question</b> est à <b>Calcul</b></i><br><br><u>true</u>: Les étapes du calcul sont attendues.'],
    ['Calculatrice', true, 'boolean', '<u>true</u>: Calculatrice disponible.'],
    // le modèle gère la compatibilité ascendante avec l’ancien paramètre MiseEnPagePlusClaire
    ['theme', 'standard', 'liste', 'Modèle pour l’affichage', ['standard', 'zonesAvecImageDeFond']]
  ]
}

/**
 * section prog01
 * @this {Parcours}
 */
export default function main () {
  const me = this
  const ds = me.donneesSection
  const stor = this.storage

  function initSection () {
    let i
    me.validOnEnter = false // ex donneesSection.touche_entree

    // Construction de la page
    me.construitStructurePage('presentation3')

    if (ds.Question === 'comparer') {
      me.donneesSection.nbetapes = 3
    }
    me.donneesSection.nbitems = me.donneesSection.nbetapes * me.donneesSection.nbrepetitions

    ds.lesExos = []
    let lp = []
    if (ds.Carre === 'toujours' || ds.Carre === 'parfois') lp.push(true)
    if (ds.Carre === 'jamais' || ds.Carre === 'parfois') lp.push(false)
    lp = j3pShuffle(lp)
    for (i = 0; i < me.donneesSection.nbitems; i++) {
      ds.lesExos.push({ carre: lp[i % lp.length] })
    }
    ds.lesExos = j3pShuffle(ds.lesExos)

    lp = [0, 1, 2]
    lp = j3pShuffle(lp)
    for (i = 0; i < me.donneesSection.nbitems; i++) {
      ds.lesExos[i].cas = lp[i % lp.length]
    }
    ds.lesExos = j3pShuffle(ds.lesExos)

    lp = [true, false]
    lp = j3pShuffle(lp)
    for (i = 0; i < me.donneesSection.nbitems; i++) {
      ds.lesExos[i].chA = lp[i % lp.length]
    }
    ds.lesExos = j3pShuffle(ds.lesExos)

    me.typederreurs = [0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0]

    me.score = 0
    let tt
    if (ds.Question === 'traduire') tt = 'Exprimer en fonction de <i>x</i>'
    if (ds.Question === 'calcul') tt = 'Utiliser un programme de calcul'
    if (ds.Question === 'comparer') tt = 'Comparer deux programmes de calculs'
    if (ds.Question === 'comparer_long') tt = 'Comparer deux programmes de calculs'

    me.afficheTitre(tt)

    // Construction des boutons
    // Le bouton Continuer est masqué à chaque fois, donc on le masque dans le corps du code

    enonceMain()
  }
  function faisChoixExos () {
    stor.Lexo = ds.lesExos.pop()
    stor.Lex = new Express()
    stor.Lex.makeAB()
    stor.nbchoiz = j3pGetRandomInt(-10, 10)
    stor.encours = ''
    if (ds.Question === 'comparer') stor.encours = 'deb'
    if (ds.Question === 'comparer_long') stor.encours = 'deb'
    stor.focoA = stor.focoB = false
  }
  function creeLesDiv () {
    stor.lesdiv = {}
    stor.lesdiv.conteneur = j3pAddElt(me.zonesElts.MG, 'div', {
      style: me.styles.etendre('toutpetit.enonce', { padding: '10px' })
    })
    const tts = addDefaultTable(stor.lesdiv.conteneur, 1, 3)
    const hhh = addDefaultTable(tts[0][0], 2, 1)
    stor.lesdiv.consigne = hhh[0][0]
    tts[0][1].style.width = '10px'
    tts[0][2].style.verticalAlign = 'top'
    const hjk = addDefaultTable(tts[0][2], 2, 1)
    stor.lesdiv.Calculatrice = hjk[0][0]
    stor.lesdiv.correction = hjk[1][0]
    const tt = addDefaultTable(hhh[1][0], 4, 3)
    stor.lesdiv.ProgA = tt[0][0]
    stor.lesdiv.ProgB = tt[0][2]
    stor.lesdiv.travA = tt[1][0]
    stor.lesdiv.travB = tt[1][2]
    stor.lesdiv.travA.style.verticalAlign = 'top'
    stor.lesdiv.travB.style.verticalAlign = 'top'
    stor.lesdiv.corA = tt[2][0]
    stor.lesdiv.corB = tt[2][2]
    stor.lesdiv.solA = tt[3][0]
    stor.lesdiv.solB = tt[3][2]
    tt[3][1].style.width = '40px'
    stor.lesdiv.zoneComp = addDefaultTable(stor.lesdiv.conteneur, 1, 1)[0][0]
    stor.lesdiv.cor = j3pAddElt(stor.lesdiv.conteneur, 'div')
    stor.lesdiv.sol = j3pAddElt(stor.lesdiv.conteneur, 'div')
    stor.lesdiv.sol.style.color = me.styles.toutpetit.correction.color
    stor.lesdiv.solA.style.color = me.styles.toutpetit.correction.color
    stor.lesdiv.solB.style.color = me.styles.toutpetit.correction.color
    stor.lesdiv.corA.style.color = me.styles.cfaux
    stor.lesdiv.cor.style.color = me.styles.cfaux
    stor.lesdiv.corB.style.color = me.styles.cfaux
  }

  function poseQuestion () {
    j3pEmpty(stor.lesdiv.solA)
    j3pEmpty(stor.lesdiv.solB)
    j3pEmpty(stor.lesdiv.corA)
    j3pEmpty(stor.lesdiv.corB)
    j3pEmpty(stor.lesdiv.cor)
    j3pEmpty(stor.lesdiv.sol)
    stor.lesdiv.sol.classList.remove('correction')
    stor.lesdiv.corA.classList.remove('explique')
    stor.lesdiv.corB.classList.remove('explique')
    stor.lesdiv.solA.classList.remove('correction')
    stor.lesdiv.solB.classList.remove('correction')
    let i, ok
    if (ds.Question === 'traduire') {
      j3pAffiche(stor.lesdiv.consigne, null, 'On appelle <i>x</i> le nombre choisi.\n<b> Exprime le résultat $A$ de ce programme de calcul en fonction de <i>x</i></b>.')
      if (stor.Lexo.chA) {
        stor.Lex.ProgA = j3pClone(stor.Lex.ProgB)
        stor.Lex.A = stor.Lex.B
        stor.Lex.AlgA = j3pClone(stor.Lex.AlgB)
        stor.Lex.AlgACo = j3pClone(stor.Lex.AlgBCo)
      }
      stor.lesdiv.travA.classList.add('travail')
    }
    if (ds.Question === 'calcul') {
      j3pAffiche(stor.lesdiv.consigne, null, '<b>Donne le résultat affiché par ce programme de calcul\nquand le nombre choisi est $' + stor.nbchoiz + '$</b>.')
      if (stor.Lexo.chA) {
        stor.Lex.ProgA = j3pClone(stor.Lex.ProgB)
        stor.Lex.A = stor.Lex.B
        stor.Lex.AlgA = j3pClone(stor.Lex.AlgB)
        stor.Lex.AlgACo = j3pClone(stor.Lex.AlgBCo)
      }
      stor.lesdiv.travA.classList.add('travail')
    }
    if (stor.encours !== 'Red' && stor.encours !== 'Compfin' && stor.encours !== 'Complong') {
      j3pAffiche(stor.lesdiv.ProgA, null, '<u>Programme de calcul A</u>:\n')
      for (i = 0; i < stor.Lex.ProgA.length; i++) {
        j3pAffiche(stor.lesdiv.ProgA, null, stor.Lex.ProgA[i] + '\n')
      }
      stor.lesdiv.ProgA.style.border = '1px solid black'
    }
    if (stor.encours === 'Red') {
      stor.tabHs[1][0].classList.remove('correction')
      stor.tabHs[1][1].classList.remove('correction')
      stor.tabzz[1][0].classList.remove('correction')
      stor.tabzz[1][1].classList.remove('correction')
      j3pEmpty(stor.lesdiv.consigne)
      j3pAffiche(stor.lesdiv.consigne, null, 'On veut comparer les deux programmes de calcul suivants.\n')
      ok = true
      j3pEmpty(stor.lesdiv.correction)
      if (stor.focoB) {
        j3pEmpty(stor.tabzz[0][0])
        j3pEmpty(stor.tabzz[0][1])
        stor.focoB = false
      }
      if (stor.focoA) {
        j3pEmpty(stor.tabHs[0][0])
        j3pEmpty(stor.tabHs[0][1])
        stor.focoA = false
      }
      if (!pasreduite(stor.tradA)) {
        ok = false
        j3pAffiche(stor.lesdiv.consigne, null, '<b><u>Etape 2</u>: Donne une écriture développée, réduite et simplifiée de A</b>')
        j3pAffiche(stor.tabHs[2][0], null, '$A =$&nbsp;')
        stor.restric = '0123456789x()+-*'
        if (ds.Carre !== 'jamais') {
          stor.restric += '²^'
        }
        stor.zoneTradA2 = new ZoneStyleMathquill2(stor.tabHs[2][1], { id: 'zoneTradA2', enter: me.sectionCourante.bind(me), restric: stor.restric })
      } else { stor.zoneTradA2 = undefined }
      if (!pasreduite(stor.tradB)) {
        if (!ok) {
          j3pAffiche(stor.lesdiv.consigne, null, '<b> et de B</b>')
        } else {
          j3pAffiche(stor.lesdiv.consigne, null, '<b><b><u>Etape 2</u>: Donne une écriture développée, réduite et simplifiée de B</b>')
        }
        ok = false
        j3pAffiche(stor.tabzz[2][0], null, '$B =$&nbsp;')
        stor.restric = '0123456789x()+-*'
        if (ds.Carre !== 'jamais') {
          stor.restric += '²^'
        }
        stor.zoneTradB2 = new ZoneStyleMathquill2(stor.tabzz[2][1], { id: 'zoneTradB2', enter: me.sectionCourante.bind(me), restric: stor.restric })
      } else { stor.zoneTradB2 = undefined }
      j3pAffiche(stor.lesdiv.consigne, null, '.')
      if (ok) {
        me.score++
        me.questionCourante++
        if (ds.Question === 'comparer') {
          stor.encours = 'Compfin'
        } else {
          stor.encours = 'Compfin'
        }
      }
    }
    if (stor.encours === 'deb') {
      stor.lesdiv.travA.classList.add('travail')
      stor.lesdiv.travB.classList.add('travail')
      stor.lesdiv.ProgB.classList.add('enonce')
      j3pAffiche(stor.lesdiv.ProgB, null, '<u>Programme de calcul B</u>: \n')
      for (i = 0; i < stor.Lex.ProgB.length; i++) {
        j3pAffiche(stor.lesdiv.ProgB, null, stor.Lex.ProgB[i] + '\n')
      }
      stor.lesdiv.ProgB.style.border = '1px solid black'
      j3pAffiche(stor.lesdiv.consigne, null, 'On veut comparer les deux programmes de calcul suivants.\n')
      j3pAffiche(stor.lesdiv.consigne, null, '<u>Etape 1</u>: On nomme <i>x</i> le nombre choisi.\n<b> Exprime en fonction de <i>x</i> le résultat affiché par chaque programme.</b>')
      stor.tabHs = addDefaultTable(stor.lesdiv.travA, 4, 2)
      j3pAffiche(stor.tabHs[0][0], null, '$A =$&nbsp;')
      stor.restric = '0123456789x()+-*'
      if (ds.Carre !== 'jamais') {
        stor.restric += '²^'
      }
      stor.zoneTradA = new ZoneStyleMathquill2(stor.tabHs[0][1], { id: 'zoneTradA', enter: me.sectionCourante.bind(me), restric: stor.restric })
      stor.tabzz = addDefaultTable(stor.lesdiv.travB, 4, 2)
      j3pAffiche(stor.tabzz[0][0], null, '$B =$&nbsp;')
      stor.zoneTradB = new ZoneStyleMathquill2(stor.tabzz[0][1], { id: 'zoneTradB', enter: me.sectionCourante.bind(me), restric: stor.restric })
    }
    if (stor.encours === 'Compfin') {
      stor.tabHs[3][0].classList.remove('correction')
      stor.tabHs[3][1].classList.remove('correction')
      stor.tabzz[3][0].classList.remove('correction')
      stor.tabzz[3][1].classList.remove('correction')
      if (stor.focoB) {
        j3pEmpty(stor.tabzz[2][0])
        j3pEmpty(stor.tabzz[2][1])
        stor.focoB = false
      }
      if (stor.focoA) {
        j3pEmpty(stor.tabHs[2][0])
        j3pEmpty(stor.tabHs[2][1])
        stor.focoA = false
      }
      stor.lesdiv.zoneComp.classList.add('travail')
      j3pEmpty(stor.lesdiv.consigne)
      j3pAffiche(stor.lesdiv.consigne, null, 'On veut comparer les deux programmes de calcul suivants.\n')
      j3pAffiche(stor.lesdiv.consigne, null, '<b><u>Etape 3</u>: Choisis la bonne conclusion.</b>')
      const tyu = addDefaultTable(stor.lesdiv.zoneComp, 1, 2)
      j3pAffiche(tyu[0][0], null, 'Les deux programmes &nbsp;')
      stor.listComp = ListeDeroulante.create(tyu[0][1], ['Choisir', 'donnent toujours le même résultat.', 'ne donnent pas toujours le même résultat.'], { centre: true })
    }
    if (ds.Question === 'traduire') {
      stor.tabHs = addDefaultTable(stor.lesdiv.travA, 1, 2)
      j3pAffiche(stor.tabHs[0][0], null, '$A =$&nbsp;')
      stor.restric = '0123456789x()+-*'
      if (ds.Carre !== 'jamais') {
        stor.restric += '²^'
      }
      stor.zoneTradA = new ZoneStyleMathquill2(stor.tabHs[0][1], { id: 'zoneTradA', enter: me.sectionCourante.bind(me), restric: stor.restric })
    }
    if (ds.Question === 'calcul') {
      stor.tabHs = addDefaultTable(stor.lesdiv.travA, stor.Lex.ProgA.length + 1, 3)
      j3pAffiche(stor.tabHs[0][0], null, '1. Choisir un nombre:')
      j3pAffiche(stor.tabHs[0][1], null, '$' + stor.nbchoiz + '$')
      stor.zoneCalc = []
      if (ds.Justifie) {
        stor.bloqchange = true
        for (i = 0; i < stor.Lex.ProgA.length - 1; i++) {
          if (i !== stor.Lex.ProgA.length - 2) {
            j3pAffiche(stor.tabHs[i + 1][0], null, stor.Lex.ProgA[i + 1].replace('le résultat', '?').replace('au résultat', 'à ?') + ' :')
          } else {
            j3pAffiche(stor.tabHs[i + 1][0], null, stor.Lex.ProgA[i + 1] + ' :')
          }
          stor.zoneCalc.push(new ZoneStyleMathquill2(stor.tabHs[i + 1][1], { id: 'zoneCalc' + i, enter: me.sectionCourante.bind(me), onchange: function () { faischange() }, restric: '0123456789-' })
          )
        }
        stor.bloqchange = false
      } else {
        j3pAffiche(stor.tabHs[1][0], null, '... ...')
        j3pAffiche(stor.tabHs[2][0], null, stor.Lex.ProgA[stor.Lex.ProgA.length - 1] + ':')
        stor.zoneCalc = new ZoneStyleMathquill2(stor.tabHs[2][1], { id: 'zoneCalc', enter: me.sectionCourante.bind(me), restric: '0123456789-' })
      }
    }
  }

  function pasreduite (k) {
    if (k.indexOf('x^2') !== k.lastIndexOf('x^2')) return false
    if (k.indexOf('x^{2}') !== k.lastIndexOf('x^{2}')) return false
    if (k.indexOf('x²') !== k.lastIndexOf('x²')) return false
    const l = k.includes('x²')
    const m = k.includes('x^2')
    const n = k.includes('x^{2}')
    if ((l && m) || (m && n) || (n && l)) return false
    if (k.includes('(')) return false
    k = k.replace('x^2', '')
    k = k.replace('x^{2}', '')
    k = k.replace('x²', '')
    if (k.indexOf('x') !== k.lastIndexOf('x')) return false
    return !k.includes('times')
  }
  function faischange () {
    if (stor.bloqchange) return
    let aff, ajout
    for (let i = 1; i < stor.Lex.ProgA.length - 2; i++) {
      j3pEmpty(stor.tabHs[i + 1][0])
      ajout = stor.zoneCalc[i - 1].reponse().replace(/ /g, '')
      if (ajout === '') { ajout = '?' } else { ajout = '$' + ajout + '$' }
      aff = stor.Lex.ProgA[i + 1].replace('le résultat', ajout)
      aff = aff.replace('au résultat', 'à ' + ajout) + ' :'
      j3pAffiche(stor.tabHs[i + 1][0], null, aff)
    }
  }

  function boutonfen () {
    j3pToggleFenetres('Calculatrice')
  }

  // @todo en faire une classe avec des méthodes (plutôt que de la déclarer à l'intérieur de cette section et affecter des fonctions à des propriétés)
  function Express () {
    const objet = this
    objet.makeAB = function () {
      let buf = '+'
      if (stor.Lexo.carre) {
        objet.c = j3pGetRandomInt(1, 9)
        if (j3pGetRandomBool()) objet.c = -objet.c
        if (objet.c < 0) buf = ''
        objet.A = '(x' + buf + objet.c + ')^2'
        objet.ProgA = ['1. Choisir un nombre', '2. Ajouter $' + objet.c + '$', '3. Mettre le résultat au carré', '4. Afficher le résultat']
        objet.AlgA = ['x', 'x+(' + objet.c + ')', '(x+(' + objet.c + '))^2', '(x' + buf + objet.c + ')^2', '(x' + buf + objet.c + ')^2', 'x^2' + buf + (2 * objet.c) + 'x + ' + (objet.c * objet.c)]
        objet.AlgACo = ['$x$']
        if (objet.c >= 0) {
          objet.AlgACo.push('$x+' + objet.c + '$')
          buf = 'x+' + objet.c
        } else {
          objet.AlgACo.push('$x + (' + objet.c + ')$')
          buf = 'x + (' + objet.c + ')'
        }
        objet.AlgACo.push('$(' + buf + ')^2$')
        if (objet.c >= 0) {
          objet.AlgACo.push('$(x + ' + objet.c + ')^2$')
        } else {
          objet.AlgACo.push('$(x ' + objet.c + ')^2$')
        }
        buf = '+'
        if (objet.c < 0) buf = ''
        objet.AlgACo.push('$x^2' + buf + (2 * objet.c) + 'x + ' + (objet.c * objet.c) + '$')
        if (stor.Lexo.cas === 0) {
          objet.B = 'x^2 +(' + (2 * objet.c) + 'x)+' + (objet.c * objet.c)
          objet.ProgB = ['1. Choisir un nombre', '2. Multiplier par $' + (2 * objet.c) + '$', '3. Ajouter $' + (objet.c * objet.c) + '$', '4. Ajouter le carré du nombre de départ', '5. Afficher le résultat']
          objet.AlgB = ['x']
          objet.AlgB.push('x*(' + 2 * objet.c + ')')
          objet.AlgB.push('x*(' + 2 * objet.c + ') +' + objet.c * objet.c)
          objet.AlgB.push('x*(' + 2 * objet.c + ') +' + objet.c * objet.c + ' +(x)^2')
          objet.AlgB.push('x*(' + 2 * objet.c + ') +' + objet.c * objet.c + ' +(x)^2')

          objet.AlgBCo = ['$x$']
          if (objet.c >= 0) {
            objet.AlgBCo.push('$x \\times ' + 2 * objet.c + '$')
            buf = 'x \\times ' + 2 * objet.c
          } else {
            objet.AlgBCo.push('$x \\times (' + 2 * objet.c + ')$')
            buf = 'x \\times (' + 2 * objet.c + ')'
          }
          objet.AlgBCo.push('$' + buf + ' + ' + objet.c * objet.c + '$')
          objet.AlgBCo.push('$' + buf + ' + ' + objet.c * objet.c + ' + x^2$')
          if (objet.c >= 0) {
            objet.AlgBCo.push('$ x^2 + ' + 2 * objet.c + 'x + ' + objet.c * objet.c + '$')
          } else {
            objet.AlgBCo.push('$ x^2 ' + 2 * objet.c + 'x + ' + objet.c * objet.c + '$')
          }
        }
        if (stor.Lexo.cas === 1) {
          const dif = j3pGetRandomInt(1, 9)
          objet.B = '(x)^2+(' + (2 * objet.c) + ')*x+(' + (objet.c * objet.c + dif) + ')'
          objet.ProgB = ['1. Choisir un nombre', '2. Multiplier par $' + (2 * objet.c) + '$', '3. Ajouter $' + (objet.c * objet.c + dif) + '$', '4. Ajouter le carré du nombre de départ', '5. Afficher le résultat']

          objet.AlgB = ['x']
          objet.AlgB.push('x*(' + 2 * objet.c + ')')
          objet.AlgB.push('x*(' + 2 * objet.c + ') +(' + (objet.c * objet.c + dif) + ')')
          objet.AlgB.push('x*(' + 2 * objet.c + ') +(' + (objet.c * objet.c + dif) + ') +(x)^2')
          objet.AlgB.push('x*(' + 2 * objet.c + ') +(' + (objet.c * objet.c + dif) + ') +(x)^2')

          objet.AlgBCo = ['$x$']
          if (objet.c >= 0) {
            objet.AlgBCo.push('$x \\times ' + 2 * objet.c + '$')
            buf = 'x \\times ' + 2 * objet.c
          } else {
            objet.AlgBCo.push('$x \\times (' + 2 * objet.c + ')$')
            buf = 'x \\times (' + 2 * objet.c + ')'
          }
          if (objet.c * objet.c + dif >= 0) {
            objet.AlgBCo.push('$' + buf + ' + ' + (objet.c * objet.c + dif) + '$')
            objet.AlgBCo.push('$' + buf + ' + ' + (objet.c * objet.c + dif) + ' + x^2$')
          } else {
            objet.AlgBCo.push('$' + buf + ' ' + (objet.c * objet.c + dif) + '$')
            objet.AlgBCo.push('$' + buf + ' ' + (objet.c * objet.c + dif) + ' + x^2$')
          }
          if (objet.c >= 0) {
            if (objet.c * objet.c + dif >= 0) {
              objet.AlgBCo.push('$ x^2 + ' + 2 * objet.c + 'x + ' + (objet.c * objet.c + dif) + '$')
            } else {
              objet.AlgBCo.push('$ x^2 + ' + 2 * objet.c + 'x ' + (objet.c * objet.c + dif) + '$')
            }
          } else {
            if (objet.c * objet.c + dif >= 0) {
              objet.AlgBCo.push('$ x^2 ' + 2 * objet.c + 'x + ' + (objet.c * objet.c + dif) + '$')
            } else {
              objet.AlgBCo.push('$ x^2 ' + 2 * objet.c + 'x ' + (objet.c * objet.c + dif) + '$')
            }
          }
        }
        if (stor.Lexo.cas === 2) {
          const dif = j3pGetRandomInt(1, 9)
          objet.B = '(x)^2 +(' + (2 * objet.c + dif) + ')x+(' + (objet.c * (objet.c + dif)) + ')'
          objet.ProgB = ['1. Choisir un nombre', '2. Multiplier par $' + (2 * objet.c + dif) + '$', '3. Ajouter $' + (objet.c * (objet.c + dif)) + '$', '4. Ajouter le carré du nombre de départ', '5. Afficher le résultat']

          objet.AlgB = ['x']
          objet.AlgB.push('x*(' + (2 * objet.c + dif) + ')')
          objet.AlgB.push('x*(' + (2 * objet.c + dif) + ') + (' + (objet.c * (objet.c + dif)) + ')')
          objet.AlgB.push('x*(' + (2 * objet.c + dif) + ') + (' + (objet.c * (objet.c + dif)) + ') +(x)^2')
          objet.AlgB.push('x*(' + (2 * objet.c + dif) + ') + (' + (objet.c * (objet.c + dif)) + ') +(x)^2')

          objet.AlgBCo = ['$x$']
          if (2 * objet.c + dif >= 0) {
            objet.AlgBCo.push('$x \\times ' + (2 * objet.c + dif) + '$')
            buf = 'x \\times ' + (2 * objet.c + dif)
          } else {
            objet.AlgBCo.push('$x \\times (' + (2 * objet.c + dif) + ')$')
            buf = 'x \\times (' + (2 * objet.c + dif) + ')'
          }
          if (objet.c * objet.c + dif >= 0) {
            objet.AlgBCo.push('$' + buf + ' + ' + (objet.c * (objet.c + dif)) + '$')
            objet.AlgBCo.push('$' + buf + ' + ' + (objet.c * (objet.c + dif)) + ' + x^2$')
          } else {
            objet.AlgBCo.push('$' + buf + ' ' + (objet.c * (objet.c + dif)) + '$')
            objet.AlgBCo.push('$' + buf + ' ' + (objet.c * (objet.c + dif)) + ' + x^2$')
          }
          if (2 * objet.c + dif >= 0) {
            if (objet.c * objet.c + dif >= 0) {
              objet.AlgBCo.push('$ x^2 + ' + (2 * objet.c + dif) + 'x + ' + (objet.c * (objet.c + dif)) + '$')
            } else {
              objet.AlgBCo.push('$ x^2 + ' + (2 * objet.c + dif) + 'x ' + (objet.c * (objet.c + dif)) + '$')
            }
          } else {
            if (objet.c * objet.c + dif >= 0) {
              objet.AlgBCo.push('$ x^2 ' + (2 * objet.c + dif) + 'x + ' + (objet.c * (objet.c + dif)) + '$')
            } else {
              objet.AlgBCo.push('$ x^2 ' + (2 * objet.c + dif) + 'x ' + (objet.c * (objet.c + dif)) + '$')
            }
          }
        }
      } else {
        objet.c = j3pGetRandomInt(1, 9)
        if (j3pGetRandomBool()) objet.c = -objet.c
        objet.k = j3pGetRandomInt(2, 9)
        if (j3pGetRandomBool()) objet.k = -objet.k
        if (objet.c < 0) buf = ''
        objet.A = objet.k + '*(x' + buf + objet.c + ')'
        objet.ProgA = ['1. Choisir un nombre', '2. Ajouter $' + objet.c + '$', '3. Multiplier le résultat par $' + objet.k + '$', '4. Afficher le résultat']
        objet.AlgA = ['x', 'x+(' + objet.c + ')', '(x+(' + objet.c + '))*(' + objet.k + ')', '(x+(' + objet.c + '))*(' + objet.k + ')']
        objet.AlgACo = ['$x$']
        if (objet.c > 0) {
          objet.AlgACo.push('$x+' + objet.c + '$')
          buf = 'x+' + objet.c
        } else {
          objet.AlgACo.push('$x' + objet.c + '$')
          buf = 'x' + objet.c
        }
        if (objet.k > 0) {
          objet.AlgACo.push('$(' + buf + ') \\times ' + objet.k + '$')
        } else {
          objet.AlgACo.push('$(' + buf + ') \\times (' + objet.k + ')$')
        }
        objet.AlgACo.push('$' + objet.k + '(' + buf + ')$')
        if (objet.c * objet.k > 0) {
          objet.AlgACo.push('$' + objet.k + 'x+' + objet.c * objet.k + '$')
        } else {
          objet.AlgACo.push('$' + objet.k + 'x' + objet.c * objet.k + '$')
        }
        if (stor.Lexo.cas === 0) {
          buf = '+'
          if (objet.k * objet.c < 0) buf = ''
          objet.B = objet.k + '*x' + buf + (objet.k * objet.c)
          objet.ProgB = ['1. Choisir un nombre', '2. Multiplier par $' + (objet.k) + '$', '3. Ajouter $' + (objet.k * objet.c) + '$ au résultat', '4. Afficher le résultat']
          objet.AlgB = ['x', 'x*(' + objet.k + ')', 'x*(' + objet.k + ')+(' + objet.k * objet.c + ')', 'x*(' + objet.k + ')+(' + objet.k * objet.c + ')']

          objet.AlgBCo = ['$x$']
          if (objet.k > 0) {
            objet.AlgBCo.push('$x \\times ' + objet.k + '$')
            buf = 'x \\times ' + objet.k
          } else {
            objet.AlgBCo.push('$x \\times (' + objet.k + ')$')
            buf = 'x \\times (' + objet.k + ')'
          }
          if (objet.k * objet.c > 0) {
            objet.AlgBCo.push('$' + buf + '+' + objet.k * objet.c + '$')
          } else {
            objet.AlgBCo.push('$' + buf + objet.k * objet.c + '$')
          }
          if (objet.k * objet.c > 0) {
            objet.AlgBCo.push('$' + objet.k + 'x+' + objet.k * objet.c + '$')
          } else {
            objet.AlgBCo.push('$' + objet.k + 'x' + objet.k * objet.c + '$')
          }
        }
        if (stor.Lexo.cas === 1) {
          let dif
          do {
            dif = j3pGetRandomInt(1, 9)
          } while (objet.k * objet.c + dif === 0)

          buf = '+'
          if (objet.k * objet.c + dif < 0) buf = ''
          objet.B = objet.k + '*x' + buf + (objet.k * objet.c + dif)
          objet.ProgB = ['1. Choisir un nombre', '2. Multiplier par $' + (objet.k) + '$', '3. Ajouter $' + (objet.k * objet.c + dif) + '$ au résultat', '4. Afficher le résultat']
          objet.AlgB = ['x', 'x*(' + objet.k + ')', 'x*(' + objet.k + ')+(' + (objet.k * objet.c + dif) + ')', 'x*(' + objet.k + ')+(' + (objet.k * objet.c + dif) + ')']

          objet.AlgBCo = ['$x$']
          if (objet.k > 0) {
            objet.AlgBCo.push('$x \\times ' + objet.k + '$')
            buf = 'x \\times ' + objet.k
          } else {
            objet.AlgBCo.push('$x \\times (' + objet.k + ')$')
            buf = 'x \\times (' + objet.k + ')'
          }
          if (objet.k * objet.c + dif > 0) {
            objet.AlgBCo.push('$' + buf + '+' + (objet.k * objet.c + dif) + '$')
          } else {
            objet.AlgBCo.push('$' + buf + (objet.k * objet.c + dif) + '$')
          }
          if (objet.k * objet.c + dif > 0) {
            objet.AlgBCo.push('$' + objet.k + 'x+' + (objet.k * objet.c + dif) + '$')
          } else {
            objet.AlgBCo.push('$' + objet.k + 'x' + (objet.k * objet.c + dif) + '$')
          }
        }
        if (stor.Lexo.cas === 2) {
          let dif1, dif2
          do {
            dif1 = j3pGetRandomInt(1, 9)
          } while (objet.k * objet.c + dif1 === 0)

          do {
            dif2 = j3pGetRandomInt(1, 9)
          } while (objet.k + dif2 === 0 || objet.k + dif2 === 1 || objet.k + dif2 === -1)

          buf = '+'
          if (objet.k * objet.c + dif1 < 0) buf = ''
          objet.B = (objet.k + dif2) + '*x' + buf + (objet.k * objet.c + dif1)
          objet.ProgB = ['1. Choisir un nombre', '2. Multiplier par $' + (objet.k + dif2) + '$', '3. Ajouter $' + (objet.k * objet.c + dif1) + '$ au résultat', '4. Afficher le résultat']
          objet.AlgB = ['x', 'x*(' + (objet.k + dif2) + ')', 'x*(' + (objet.k + dif2) + ')+(' + (objet.k * objet.c + dif1) + ')', 'x*(' + (objet.k + dif2) + ')+(' + (objet.k * objet.c + dif1) + ')']

          objet.AlgBCo = ['$x$']
          if (objet.k + dif2 > 0) {
            objet.AlgBCo.push('$x \\times ' + (objet.k + dif2) + '$')
            buf = 'x \\times ' + (objet.k + dif2)
          } else {
            objet.AlgBCo.push('$x \\times (' + (objet.k + dif2) + ')$')
            buf = 'x \\times (' + (objet.k + dif2) + ')'
          }
          if (objet.k * objet.c + dif1 > 0) {
            objet.AlgBCo.push('$' + buf + '+' + (objet.k * objet.c + dif1) + '$')
          } else {
            objet.AlgBCo.push('$' + buf + (objet.k * objet.c + dif1) + '$')
          }
          if (objet.k * objet.c + dif1 > 0) {
            objet.AlgBCo.push('$' + (objet.k + dif2) + 'x+' + (objet.k * objet.c + dif1) + '$')
          } else {
            objet.AlgBCo.push('$' + (objet.k + dif2) + 'x' + (objet.k * objet.c + dif1) + '$')
          }
        }
      }
    }
  }

  function yaReponse () {
    let i
    if (me.isElapsed) return true
    if (ds.Question === 'traduire') {
      if (stor.zoneTradA.reponse() === '') {
        stor.zoneTradA.focus()
        return false
      }
    }
    if (ds.Question === 'calcul') {
      if (ds.Justifie) {
        for (i = 0; i < stor.zoneCalc.length; i++) {
          if (stor.zoneCalc[i].reponse() === '') {
            stor.zoneCalc[i].focus()
            return false
          }
        }
        return true
      }
      if (stor.zoneCalc.reponse() === '') {
        stor.zoneCalc.focus()
        return false
      }
      return true
    }
    if (stor.encours === 'deb') {
      if (stor.zoneTradA.reponse() === '') {
        stor.zoneTradA.focus()
        return false
      }
      if (stor.zoneTradB.reponse() === '') {
        stor.zoneTradB.focus()
        return false
      }
    }
    if (stor.encours === 'Red') {
      if (stor.zoneTradA2 !== undefined) {
        if (stor.zoneTradA2.reponse() === '') {
          stor.zoneTradA2.focus()
          return false
        }
      }
      if (stor.zoneTradB2 !== undefined) {
        if (stor.zoneTradB2.reponse() === '') {
          stor.zoneTradB2.focus()
          return false
        }
      }
    }
    if (stor.encours === 'Compfin') {
      if (!stor.listComp.changed) {
        stor.listComp.focus()
        return false
      }
    }
    return true
  }
  function isRepOk () {
    stor.bloqchange = true
    stor.errPasRedA = false
    stor.errPasRedB = false
    stor.errEgA = false
    stor.errEgB = false
    stor.errEcritA = false
    stor.errEcritB = false

    if (ds.Question === 'traduire') {
      let r1 = stor.zoneTradA.reponse()
      while (r1.indexOf(') (') !== -1) {
        r1 = r1.replace(') (', ')*(')
      }
      r1 = r1.replace(/ /g, '').replace(/\\times/g, '*')
      if (r1.indexOf('--') !== -1) {
        stor.errEcritA = true
        stor.zoneTradA.corrige(false)
        return false
      }
      while (r1.indexOf('^{2}') !== -1) {
        r1 = r1.replace('^{2}', '^2')
      }
      const algbuf = Algebrite.run(r1)
      if (algbuf.indexOf('Stop') !== -1) {
        stor.errEcritA = true
        stor.zoneTradA.corrige(false)
        return false
      }
      const ok = Algebrite.run(r1 + '-(' + stor.Lex.A + ')')
      if (ok === '0') return true
      if (ok === '0.0') return true
      if (ok === '-0') return true
      if (ok === '-0.0') return true
      stor.zoneTradA.corrige(false)
      stor.errEgA = false
      return false
    }

    if (ds.Question === 'calcul') {
      if (ds.Justifie) {
        const rep = []
        let ok = true
        for (let i = 0; i < stor.zoneCalc.length; i++) {
          rep.push(stor.zoneCalc[i].reponse().replace(/ /g, ''))
        }
        for (let i = 0; i < stor.zoneCalc.length; i++) {
          if (rep[i].indexOf('--') !== -1 || rep[i].indexOf('-') === rep[i].length - 1) {
            stor.errEcrit = true
            ok = false
            stor.zoneCalc[i].corrige(false)
          }
        }
        if (!ok) return false
        for (let i = 0; i < stor.zoneCalc.length; i++) {
          const algbuf = Algebrite.run(stor.Lex.AlgA[i + 1].replace(/x/g, stor.nbchoiz))
          const okbis = Algebrite.run(algbuf + '-(' + rep[i] + ')')
          if (okbis !== '0') {
            ok = false
            stor.zoneCalc[i].corrige(false)
          }
        }
        return ok
      }

      // Justifie false
      const rep = stor.zoneCalc.reponse().replace(/ /g, '')
      if (rep.indexOf('-') !== rep.lastIndexOf('-') || !(rep.indexOf('-') === -1 || rep.indexOf('-') === 0)) {
        stor.errEcrit = true
        stor.zoneCalc.corrige(false)
        return false
      }
      const algbuf = Algebrite.run(stor.Lex.AlgA[stor.Lex.AlgA.length - 2].replace('x^2', '(x)^2').replace(/x/g, stor.nbchoiz))
      const okbis = Algebrite.run(algbuf + '-(' + rep + ')')
      if (okbis !== '0') {
        stor.zoneCalc.corrige(false)
        return false
      }
      return true
    }

    if (stor.encours === 'deb') {
      let okA = true
      let okB = true

      let r1 = stor.zoneTradA.reponse()
      stor.tradA = r1.replace(/ /g, '').replace(/\\times/g, '*')
      while (r1.indexOf(') (') !== -1) {
        r1 = r1.replace(') (', ')*(')
      }
      r1 = r1.replace(/ /g, '').replace(/\\times/g, '*')
      if (r1.indexOf('--') !== -1) {
        stor.errEcritA = true
        stor.zoneTradA.corrige(false)
        stor.tradA = ''
        okA = false
      }
      while (r1.indexOf('^{2}') !== -1) {
        r1 = r1.replace('^{2}', '^2')
      }
      while (r1.indexOf('²') !== -1) {
        r1 = r1.replace('²', '^2')
      }

      let r2 = stor.zoneTradB.reponse()
      stor.tradB = r2.replace(/ /g, '').replace(/\\times/g, '*')
      while (r2.indexOf(') (') !== -1) {
        r2 = r2.replace(') (', ')*(')
      }
      r2 = r2.replace(/ /g, '').replace(/\\times/g, '*')
      if (r2.indexOf('--') !== -1) {
        stor.errEcritB = true
        stor.zoneTradB.corrige(false)
        stor.tradB = ''
        okB = false
      }
      while (r2.indexOf('^{2}') !== -1) {
        r2 = r2.replace('^{2}', '^2')
      }
      while (r2.indexOf('²') !== -1) {
        r2 = r2.replace('²', '^2')
      }
      if (okA) {
        const algbuf = Algebrite.run(r1)
        if (algbuf.indexOf('Stop') !== -1) {
          stor.errEcritA = true
          stor.zoneTradA.corrige(false)
          okA = false
          stor.tradA = ''
        }
      }

      if (okB) {
        const algbuf2 = Algebrite.run(r2)
        if (algbuf2.indexOf('Stop') !== -1) {
          stor.errEcritB = true
          stor.zoneTradB.corrige(false)
          okB = false
          stor.tradB = ''
        }
      }

      if (okA) {
        const okbis = Algebrite.run(r1 + '-(' + stor.Lex.A + ')')
        if (!(okbis === '0' || okbis === '0.0' || okbis === '-0' || okbis === '-0.0')) {
          okA = false
          stor.zoneTradA.corrige(false)
          stor.tradA = ''
          stor.errEgA = true
        }
      }

      if (okB) {
        const okbis = Algebrite.run(r2 + '-(' + stor.Lex.B + ')')
        if (!(okbis === '0' || okbis === '0.0' || okbis === '-0' || okbis === '-0.0')) {
          okB = false
          stor.tradB = ''
          stor.zoneTradB.corrige(false)
          stor.errEgB = true
        }
      }

      return okA && okB
    }

    if (stor.encours === 'Red') {
      let okA = true
      let okB = true

      if (stor.zoneTradA2 !== undefined) {
        let r1 = stor.zoneTradA2.reponse()
        stor.tradA = r1.replace(/ /g, '')
        while (r1.indexOf(') (') !== -1) {
          r1 = r1.replace(') (', ')*(')
        }
        r1 = r1.replace(/ /g, '').replace(/\\times/g, '*')
        if (r1.indexOf('--') !== -1) {
          stor.errEcritA = true
          stor.zoneTradA2.corrige(false)
          stor.tradA = ''
          okA = false
        }
        while (r1.indexOf('^{2}') !== -1) {
          r1 = r1.replace('^{2}', '^2')
        }
        while (r1.indexOf('²') !== -1) {
          r1 = r1.replace('²', '^2')
        }

        if (okA) {
          const algbuf = Algebrite.run(r1)
          if (algbuf.indexOf('Stop') !== -1) {
            stor.errEcritA = true
            stor.zoneTradA2.corrige(false)
            okA = false
            stor.tradA = ''
          }
        }

        if (okA) {
          const okbis = Algebrite.run(r1 + '-(' + stor.Lex.A + ')')
          if (!(okbis === '0')) {
            okA = false
            stor.zoneTradA2.corrige(false)
            stor.tradA = ''
            stor.errEgA = true
          }
        }

        if (okA) {
          if (!pasreduite(stor.tradA)) {
            okA = false
            stor.zoneTradA2.corrige(false)
            stor.tradA = ''
            stor.errPasRedA = true
          }
        }
      }

      if (stor.zoneTradB2 !== undefined) {
        let r2 = stor.zoneTradB2.reponse()
        stor.tradB = r2.replace(/ /g, '')
        while (r2.indexOf(') (') !== -1) {
          r2 = r2.replace(') (', ')*(')
        }
        r2 = r2.replace(/ /g, '').replace(/\\times/g, '*')
        if (r2.indexOf('--') !== -1) {
          stor.errEcritB = true
          stor.zoneTradB.corrige(false)
          stor.tradB = ''
          okB = false
        }
        while (r2.indexOf('^{2}') !== -1) {
          r2 = r2.replace('^{2}', '^2')
        }
        while (r2.indexOf('²') !== -1) {
          r2 = r2.replace('²', '^2')
        }
        if (okB) {
          const algbuf2 = Algebrite.run(r2)
          if (algbuf2.indexOf('Stop') !== -1) {
            stor.errEcritB = true
            stor.zoneTradB.corrige(false)
            okB = false
            stor.tradB = ''
          }
        }

        if (okB) {
          const okbis = Algebrite.run(r2 + '-(' + stor.Lex.B + ')')
          if (okbis !== '0') {
            okB = false
            stor.tradB = ''
            stor.errEgB = true
            stor.zoneTradB.corrige(false)
          }
        }

        if (okB) {
          if (!pasreduite(stor.tradB)) {
            okB = false
            stor.zoneTradB2.corrige(false)
            stor.tradB = ''
            stor.errPasRedB = true
          }
        }
      }

      return okA && okB
    }
    if (stor.encours === 'Compfin') {
      const r1 = stor.listComp.getReponseIndex()
      if (r1 === 1) {
        if (stor.Lexo.cas !== 0) {
          stor.listComp.corrige(false)
          return false
        }
        return true
      }
      if (r1 !== 1) {
        if (stor.Lexo.cas === 0) {
          stor.listComp.corrige(false)
          return false
        }
        return true
      }
    }
  }

  function desactiveAll () {
    if (ds.Question === 'traduire') {
      stor.zoneTradA.disable()
    }
    if (ds.Question === 'calcul') {
      if (ds.Justifie) {
        for (let i = 0; i < stor.zoneCalc.length; i++) {
          stor.zoneCalc[i].disable()
        }
      } else {
        stor.zoneCalc.disable()
      }
      return true
    }
    if (stor.encours === 'Compfin') {
      stor.listComp.disable()
      stor.encours = 'GLOB'
    }
    if (stor.encours === 'Red') {
      if (stor.zoneTradA2 !== undefined) stor.zoneTradA2.disable()
      if (stor.zoneTradB2 !== undefined) stor.zoneTradB2.disable()
      if (ds.Question === 'comparer') {
        stor.encours = 'Compfin'
      } else {
        stor.encours = 'Complong'
      }
    }
    if (stor.encours === 'deb') {
      stor.zoneTradA.disable()
      stor.zoneTradB.disable()
      stor.encours = 'Red'
    }
  }
  function barrelesfo () {
    if (ds.Question === 'traduire') {
      stor.zoneTradA.barreIfKo()
    }
    if (ds.Question === 'calcul') {
      if (ds.Justifie) {
        for (let i = 0; i < stor.zoneCalc.length; i++) {
          stor.zoneCalc[i].barreIfKo()
        }
        return true
      }
      stor.zoneCalc.barreIfKo()
    }
    if (stor.encours === 'Red') {
      stor.zoneTradA.barreIfKo()
      stor.zoneTradB.barreIfKo()
    }
    if (stor.encours === 'Compfin' || stor.encours === 'Complong') {
      stor.zoneTradA2?.barreIfKo()
      stor.zoneTradB2?.barreIfKo()
    }
    if (stor.encours === 'GLOB') {
      stor.listComp.barre()
    }
  }

  function mettouvert () {
    if (ds.Question === 'traduire') {
      stor.zoneTradA.corrige(true)
    }
    if (ds.Question === 'calcul') {
      if (ds.Justifie) {
        for (const zone of stor.zoneCalc) {
          zone.corrige(true)
        }
        return true
      }
      stor.zoneCalc.corrige(true)
    }
    if (stor.encours === 'deb') {
      stor.zoneTradA.corrige(true)
      stor.zoneTradB.corrige(true)
    }
    if (stor.encours === 'Red') {
      if (stor.zoneTradA2 !== undefined) stor.zoneTradA2.corrige(true)
      if (stor.zoneTradB2 !== undefined) stor.zoneTradB2.corrige(true)
    }
    if (stor.encours === 'Compfin') {
      stor.listComp.corrige('true')
    }
  }
  function affCorrection (isLast) {
    let yaexplikA = false
    let yaexplikB = false
    let yacoA = false
    let yacoB = false

    j3pEmpty(stor.lesdiv.corA)
    j3pEmpty(stor.lesdiv.corB)
    j3pEmpty(stor.lesdiv.cor)
    let i, buf

    if (stor.errEcritA) {
      yaexplikA = true
      j3pAffiche(stor.lesdiv.corA, null, 'Erreur d’écriture !\n')
    }
    if (stor.errEcritB) {
      yaexplikB = true
      j3pAffiche(stor.lesdiv.corB, null, 'Erreur d’écriture !\n')
    }
    if (stor.errEgA) {
      yaexplikA = true
      j3pAffiche(stor.lesdiv.corA, null, 'Ce résultat est faux')
    }
    if (stor.errEgB) {
      yaexplikB = true
      j3pAffiche(stor.lesdiv.corB, null, 'Ce résultat est faux')
    }
    if (stor.errPasRedA) {
      yaexplikA = true
      j3pAffiche(stor.lesdiv.corA, null, 'La forme de $A$ ne convient pas')
    }
    if (stor.errPasRedB) {
      yaexplikB = true
      j3pAffiche(stor.lesdiv.corB, null, 'La forme de $B$ ne convient pas')
    }

    if (isLast) {
      if (stor.encours === 'deb') {
        if (stor.zoneTradA.bon === false) {
          stor.tabFh = addDefaultTable(stor.lesdiv.solA, stor.Lex.ProgA.length, 2)
          for (i = 0; i < stor.Lex.ProgA.length; i++) {
            j3pAffiche(stor.tabFh[i][0], null, stor.Lex.ProgA[i] + ': &nbsp;')
            j3pAffiche(stor.tabFh[i][1], null, stor.Lex.AlgACo[i])
          }
          stor.tradA = stor.Lex.AlgACo[stor.Lex.AlgACo.length - 2]
          j3pAffiche(stor.tabHs[1][0], null, '$A =$&nbsp;')
          j3pAffiche(stor.tabHs[1][1], null, stor.tradA)
          stor.tabHs[1][0].style.color = me.styles.petit.correction.color
          stor.tabHs[1][1].style.color = me.styles.petit.correction.color
          stor.tabHs[1][0].classList.add('correction')
          stor.tabHs[1][1].classList.add('correction')
          stor.tradA = stor.tradA.replace(/$/g, '')
          stor.focoA = true
          yacoA = true
        }
        if (stor.zoneTradB.isOk() === false) {
          stor.tabzz[1][0].classList.add('correction')
          stor.tabzz[1][1].classList.add('correction')
          const tabgh = addDefaultTable(stor.lesdiv.solB, stor.Lex.ProgB.length, 2)
          for (i = 0; i < stor.Lex.ProgB.length; i++) {
            j3pAffiche(tabgh[i][0], null, stor.Lex.ProgB[i] + ': &nbsp;')
            j3pAffiche(tabgh[i][1], null, stor.Lex.AlgBCo[i])
          }
          stor.tradB = stor.Lex.AlgBCo[stor.Lex.AlgBCo.length - 1]
          j3pAffiche(stor.tabzz[1][0], null, '$B =$&nbsp;')
          j3pAffiche(stor.tabzz[1][1], null, stor.tradB)
          stor.tabzz[1][0].style.color = me.styles.petit.correction.color
          stor.tabzz[1][1].style.color = me.styles.petit.correction.color
          stor.tradB = stor.tradB.replace(/$/g, '')
          stor.focoB = true
          yacoB = true
        }
      }
      if (stor.encours === 'Red') {
        if (stor.zoneTradA2?.isOk() === false) {
          j3pAffiche(stor.tabHs[3][0], null, '$A =$&nbsp;')
          j3pAffiche(stor.tabHs[3][1], null, stor.Lex.AlgACo[stor.Lex.AlgACo.length - 1])
          stor.tabHs[3][0].style.color = me.styles.petit.correction.color
          stor.tabHs[3][1].style.color = me.styles.petit.correction.color
          stor.tabHs[3][0].classList.add('correction')
          stor.tabHs[3][1].classList.add('correction')
          stor.focoA = true
        }
        if (stor.zoneTradB2?.isOk() === false) {
          j3pAffiche(stor.tabzz[3][0], null, '$B =$&nbsp;')
          j3pAffiche(stor.tabzz[3][1], null, stor.Lex.AlgBCo[stor.Lex.AlgBCo.length - 1])
          stor.tabzz[3][0].style.color = me.styles.petit.correction.color
          stor.tabzz[3][1].style.color = me.styles.petit.correction.color
          stor.tabzz[3][0].classList.add('correction')
          stor.tabzz[3][1].classList.add('correction')
          stor.focoB = true
        }
      }
      if (stor.encours === 'Compfin') {
        stor.lesdiv.sol.classList.add('correction')
        if (stor.Lex.cas === 0) {
          j3pAffiche(stor.lesdiv.sol, null, 'Les deux programmes donnent toujours le même résultat.')
        } else {
          j3pAffiche(stor.lesdiv.sol, null, 'Les deux programmes ne donnent pas toujours le même résultat.')
        }
      }
      if (ds.Question === 'calcul') {
        yacoA = true
        stor.tabFh = addDefaultTable(stor.lesdiv.solA, stor.Lex.ProgA.length, 2)
        for (i = 0; i < stor.Lex.ProgA.length - 1; i++) {
          buf = stor.Lex.AlgACo[i].replace('$', '')
          buf = buf.replace('$', '')
          buf = buf.replace('\\times', '*')
          buf = buf.replace('x^2', '(x)^2')
          j3pAffiche(stor.tabFh[i][0], null, stor.Lex.ProgA[i] + ': &nbsp;')
          j3pAffiche(stor.tabFh[i][1], null, '$' + Algebrite.run(buf.replace(/x/g, stor.nbchoiz)) + '$')
        }
        j3pAffiche(stor.tabFh[i][0], null, stor.Lex.ProgA[stor.Lex.ProgA.length - 1] + ': &nbsp;')
        j3pAffiche(stor.tabFh[i][1], null, '$' + Algebrite.run(buf.replace(/x/g, stor.nbchoiz)) + '$')
      }
      if (ds.Question === 'traduire') {
        yacoA = true
        stor.tabFh = addDefaultTable(stor.lesdiv.solA, stor.Lex.ProgA.length, 2)
        for (i = 0; i < stor.Lex.ProgA.length; i++) {
          j3pAffiche(stor.tabFh[i][0], null, stor.Lex.ProgA[i] + ': &nbsp;')
          j3pAffiche(stor.tabFh[i][1], null, stor.Lex.AlgACo[i])
        }
      }
      barrelesfo()
      desactiveAll()
    }

    if (yaexplikA) stor.lesdiv.corA.classList.add('explique')
    if (yaexplikB) stor.lesdiv.corB.classList.add('explique')
    if (yacoA) stor.lesdiv.solA.classList.add('correction')
    if (yacoB) stor.lesdiv.solB.classList.add('correction')
  }

  function enonceMain () {
    if (stor.encours !== 'Red' && stor.encours !== 'Compfin' && stor.encours !== 'Complong') {
      me.videLesZones()
      creeLesDiv()
      faisChoixExos()
      stor.lesdiv.consigne.classList.add('enonce')
      me.zonesElts.MG.classList.add('fond')
      stor.lesdiv.ProgA.classList.add('enonce')
    }
    poseQuestion()
  }

  switch (this.etat) {
    case 'enonce':
      // code exécuté au lancement de la section
      if (me.debutDeLaSection) {
        initSection()
      } else {
        enonceMain()
      }

      if (ds.Calculatrice) {
        this.fenetresjq = [{ name: 'Calculatrice', title: 'Une calculatrice', left: 600, top: 10 }]
        j3pCreeFenetres(this)
        j3pAfficheCroixFenetres('Calculatrice')
        j3pEmpty(stor.lesdiv.Calculatrice)
        j3pAjouteBouton(stor.lesdiv.Calculatrice, 'Calculatrice', 'MepBoutons', 'Calculatrice', boutonfen)
      }
      this.finEnonce()
      break // case "enonce":

    case 'correction':
      if (!yaReponse()) {
        // pas de réponse
        stor.lesdiv.correction.style.color = this.styles.cfaux
        stor.lesdiv.correction.innerHTML = reponseIncomplete
        return this.finCorrection()
      }

      if (isRepOk()) {
      // Bonne réponse
        stor.lesdiv.correction.style.color = this.styles.cbien
        stor.lesdiv.correction.innerHTML = cBien
        mettouvert()
        desactiveAll()
        me.score++
        this.typederreurs[0]++
        return this.finCorrection('navigation', true)
      }

      // Pas de bonne réponse
      stor.lesdiv.correction.style.color = this.styles.cfaux
      stor.lesdiv.correction.innerHTML = cFaux

      if (this.isElapsed) {
        // A cause de la limite de temps :
        stor.lesdiv.correction.innerHTML += tempsDepasse + '<BR>'
        this.typederreurs[10]++
        affCorrection(true)
        return this.finCorrection('navigation', true)
      }

      if (me.essaiCourant < me.donneesSection.nbchances) {
        stor.lesdiv.correction.innerHTML += '<br>' + essaieEncore
        affCorrection(false)
        stor.bloqchange = false
        this.typederreurs[1]++
        return this.finCorrection()
      }

      // Erreur au dernier essai
      affCorrection(true)

      this.typederreurs[2]++
      return this.finCorrection('navigation', true)

    case 'navigation':
      if (!this.sectionTerminee()) {
        this.etat = 'enonce'
      }
      this.finNavigation()
      break // case "navigation":
  }
}
