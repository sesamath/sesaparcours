import { j3pAddElt, j3pAjouteBouton, j3pArrondi, j3pBarre, j3pClone, j3pDetruit, j3pEmpty, j3pGetRandomBool, j3pGetRandomInt, j3pPGCD, j3pShowError, j3pShuffle } from 'src/legacy/core/functions'
import { j3pCreeFenetres, j3pAfficheCroixFenetres, j3pToggleFenetres } from 'src/legacy/core/functionsJqDialog'
import ZoneStyleMathquill3 from 'src/legacy/outils/zoneStyleMathquill/ZoneStyleMathquill3'
import { addDefaultTable, addDefaultTableDetailed } from 'src/legacy/themes/table'
import { j3pAffiche } from 'src/lib/mathquill/functions'
import textesGeneriques from 'src/lib/core/textes'

import 'src/legacy/sections/college/css/operations.scss'

const { cBien, cFaux, essaieEncore, reponseIncomplete, tempsDepasse } = textesGeneriques

/**
 * Ajouter une description ici
 * @fileOverview
 * @author Tommy Barroy <t.barroy@laposte.net>
 * @since 2017-10
*/
export const params = {
  outils: ['mathquill', 'calculatrice'],
  /**
   * Ce qui est paramétrable dans l’éditeur de graphe
   * @type {LegacySectionParamsParametres}
   */
  parametres: [
    ['nbrepetitions', 3, 'entier', 'Nombre de répétitions de la section'],
    ['nbchances', 2, 'entier', 'Nombre d’essais possibles'],
    ['nbInconnues', 2, 'entier', 'Nombre d’inconnues'],
    ['MaxOperations', 3, 'entier', 'Nombre maximum d’opérations imbriquées (entre 1 et 4)'],
    ['MinOperations', 2, 'entier', 'Nombre minimum d’opérations imbriquées (entre 1 et 4)'],
    ['Relatifs', true, 'boolean', '<u>true</u>:  Les nombres peuvent être négatifs'],
    ['Addition', true, 'boolean', '<u>true</u>:  Additions possibles <br><br> Si <b><i>Relatifs</i></b> est à true, addition et soustractions sont liées.'],
    ['Soustraction', true, 'boolean', '<u>true</u>:  Soustractions possibles <br><br> Si <b><i>Relatifs</i></b> est à true, addition et soustractions sont liées.'],
    ['Multiplication', true, 'boolean', '<u>true</u>:  Multiplications possibles'],
    ['Division', true, 'boolean', '<u>true</u>:  Divisions possibles'],
    ['CarreCube', true, 'boolean', '<u>true</u>:  carré et cube possibles'],
    ['Puissance', true, 'boolean', '<u>true</u>:  Puissances possibles (2, 3 , 4 ou 5)'],
    ['RestricPuis', false, 'boolean', '<u>true</u>:  Pas de puissance d’un calcul.'],
    ['Sans_echec', true, 'boolean', '<u>true</u>:  Un épuisement des chances entraîne un nouvel exercice. <br><br> <u>false</u>: L’exercie doit être terminé pour passer au suivant.'],
    ['Xvisible', false, 'boolean', '<u>true</u>:  Le symbole fois est toujours écrit.'],
    ['Calculatrice', true, 'boolean', '<u>true</u>:  Une calculatrice est disponible'],
    ['Ordre', 'aléatoire', 'liste', 'Ordre d’imbrication des opérations', ['aléatoire', 'poly']],
    // le modèle gère la compatibilité ascendante avec l’ancien paramètre MiseEnPagePlusClaire
    ['theme', 'standard', 'liste', 'Modèle pour l’affichage', ['standard', 'zonesAvecImageDeFond']]
  ]
}

/**
 * section remplace
 * @this {Parcours}
 */
export default function main () {
  const me = this
  const ds = me.donneesSection
  const stor = this.storage

  function initSection () {
    let i, max, min
    me.validOnEnter = false // ex donneesSection.touche_entree

    // Construction de la page
    me.construitStructurePage('presentation1bis')
    stor.gardechance = ds.nbchances
    ds.nbchances = 1

    stor.elembase = []
    if (ds.Addition || ds.Soustraction) stor.elembase.push('+')
    if (ds.Multiplication || ds.Division) stor.elembase.push('x')
    if (ds.Puissance || ds.CarreCube) stor.elembase.push('p')
    if (stor.elembase.length === 0) {
      j3pShowError('erreur paramétage')
      stor.elembase.push('+')
    }
    stor.elembase = j3pShuffle(stor.elembase)

    ds.lesExos = []

    max = ds.MaxOperations
    min = ds.MinOperations
    if (isNaN(max)) max = 1
    if (isNaN(min)) min = 1
    max = Math.min(max, 4)
    max = Math.max(max, 1)
    min = Math.min(min, 4)
    min = Math.max(min, 1)
    max = Math.max(min, max)

    if (isNaN(ds.nbInconnues)) ds.nbInconnues = 1
    ds.nbInconnues = Math.max(0, Math.min(3, ds.nbInconnues))

    let lp = []
    for (i = min; i < max + 1; i++) lp.push(i)
    lp = j3pShuffle(lp)
    for (i = 0; i < ds.nbitems; i++) {
      ds.lesExos.push({ nbOp: lp[i % lp.length] })
    }
    ds.lesExos = j3pShuffle(ds.lesExos)

    for (i = 0; i < ds.nbitems; i++) {
      ds.lesExos[i].base = stor.elembase[i % stor.elembase.length]
    }
    ds.lesExos = j3pShuffle(ds.lesExos)

    me.typederreurs = [0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0]

    me.score = 0
    const tt = 'Calculer pas à pas'

    me.afficheTitre(tt)

    enonceMain()
  }
  function faisChoixExos () {
    let i, buf, buf2, j
    const e = ds.lesExos.pop()
    stor.lesop = [e.base]
    if (ds.RestricPuis && e.nbOp > 1) {
      if (e.base === 'p' && stor.elembase.length === 1) {
        e.nbOp = 1
      } else {
        while (e.base === 'p') {
          e.base = stor.elembase[j3pGetRandomInt(0, stor.elembase.length - 1)]
        }
        stor.lesop = [e.base]
      }
    }
    if (stor.elembase.length > 1) {
      buf = e.base
      for (i = 0; i < e.nbOp - 1; i++) {
        do {
          buf2 = stor.elembase[j3pGetRandomInt(0, stor.elembase.length - 1)]
        } while (buf2 === buf)
        buf = buf2
        stor.lesop.push(buf2)
      }
    }
    stor.encours = 'op'
    if (ds.Ordre !== 'aléatoire') {
      switch (ds.Ordre) {
        case 'poly': {
          stor.lexpres = new Express()
          stor.lexpres.ajSomme(stor.lexpres.cont)
          stor.lexpres.ajProd(stor.lexpres.cont.terme1.cont)
          stor.lexpres.ajProd(stor.lexpres.cont.terme2.cont)
          stor.lexpres.ajPuissance(stor.lexpres.cont.terme1.cont.fact2.cont)
          stor.lexpres.ajPuissance(stor.lexpres.cont.terme2.cont.fact2.cont)
          stor.lexpres.cont.terme1.cont.fact2.fois = true
          stor.lexpres.cont.terme2.cont.fact2.fois = true
          let cpt = 0
          let ttu
          do {
            cpt++
            ttu = stor.lexpres.chopelesVal()
            for (i = 0; i < ttu; i++) {
              ttu[i].type = 'empty'
            }
            stor.lexpres.nombrilise()
          }
          while ((stor.lexpres.yavirg() || (!ds.Relatifs && !stor.lexpres.Cposit())) && cpt < 1000)
          if (ds.nbInconnues > 0) {
            stor.lexpres.metInc(stor.lexpres.cont.terme1.cont.fact2.cont.fact)
            stor.encours = 'remp'
          }
          if (ds.nbInconnues > 1) {
            stor.lexpres.metInc(stor.lexpres.cont.terme2.cont.fact2.cont.fact)
          }
          break
        }
      }
    } else {
      let cpt = 0
      do {
        stor.lexpres = new Express()
        for (i = 0; i < stor.lesop.length; i++) {
          buf = stor.lexpres.chopelesvides()
          if (ds.RestricPuis) {
            for (j = buf.length - 1; j > -1; j--) {
              if (buf[j].dad === 'p') buf.splice(j, 1)
            }
          }
          for (j = buf.length - 1; j > -1; j--) {
            if (buf[j].dad === stor.lesop[i]) buf.splice(j, 1)
          }
          if (buf.length > 0) {
            buf2 = buf[j3pGetRandomInt(0, buf.length - 1)]
            switch (stor.lesop[i]) {
              case '+':
                stor.lexpres.ajSomme(buf2)
                break
              case 'x':
                stor.lexpres.ajProd(buf2)
                break
              case 'p':
                stor.lexpres.ajPuissance(buf2)
                break
            }
          }
        }
        cpt++
        const ttu = stor.lexpres.chopelesVal()
        for (i = 0; i < ttu; i++) {
          ttu[i].type = 'empty'
        }
        stor.lexpres.nombrilise()
      }
      while ((stor.lexpres.yavirg() || (!ds.Relatifs && !stor.lexpres.Cposit())) && cpt < 1000)
      buf = stor.lexpres.chopelesVal()
      buf = j3pShuffle(buf)
      for (i = 0; i < Math.min(ds.nbInconnues, buf.length); i++) {
        stor.lexpres.metInc(buf[i])
        stor.encours = 'remp'
      }
    }

    stor.deb = true
    stor.encours2 = true
    stor.dev = stor.foco = false
    stor.park = 0
    stor.npark = 0
    stor.squizz = false
    stor.nocal = false
    stor.zouzoul = false
    return e
  }
  function creeLesDiv () {
    stor.lesdiv = {}
    stor.lesdiv.conteneur = j3pAddElt(me.zonesElts.MG, 'div', {
      style: me.styles.etendre('toutpetit.enonce', { padding: '10px' })
    })
    stor.lesdiv.consigne = addDefaultTable(stor.lesdiv.conteneur, 1, 1)[0][0]
    const cells = addDefaultTable(stor.lesdiv.conteneur, 8, 1)
    stor.lesdiv.enoncejus = cells[0][0]
    stor.lesdiv.travailjus = cells[1][0]
    stor.lesdiv.travail = cells[2][0]
    stor.lesdiv.explikjus = cells[3][0]
    stor.lesdiv.solutionjus = cells[4][0]
    stor.lesdiv.calculatrice = j3pAddElt(me.zonesElts.MD, 'div', {
      style: me.styles.etendre('toutpetit.enonce', { padding: '10px' })
    })
    stor.lesdiv.correction = j3pAddElt(me.zonesElts.MD, 'div', {
      style: me.styles.etendre('toutpetit.enonce', { padding: '10px' })
    })
    stor.lesdiv.solutionjus.style.color = me.styles.toutpetit.correction.color
    stor.lesdiv.explikjus.style.color = me.styles.cfaux
  }

  function poseQuestion () {
    let laf, buf, i
    stor.tentative = 1
    j3pEmpty(stor.lesdiv.correction)
    j3pEmpty(stor.lesdiv.explikjus)
    j3pEmpty(stor.lesdiv.solutionjus)
    if (stor.dev) {
      stor.dev = false
      j3pEmpty(stor.lesdiv.travailjus)
      const tty = addDefaultTable(stor.lesdiv.travailjus, 1, 2)
      j3pAffiche(tty[0][0], null, '$A = $')
      stor.lexpres.afficheRemp(tty[0][1], true)
      stor.lesdiv.travailjus.style.color = me.styles.toutpetit.correction.color
    }
    stor.park++
    if (stor.deb) {
      stor.deb = false
      laf = stor.lexpres.faitAffiche()
      j3pAffiche(stor.lesdiv.consigne, null, 'On considère l’expression $ A = ' + laf + '$\n')
      if (stor.encours === 'remp') {
        buf = ''
        for (i = 0; i < stor.lexpres.listlet.length; i++) {
          if (i > 0 && i < stor.lexpres.listlet.length - 1) buf += ' , '
          if (i === stor.lexpres.listlet.length - 1 && stor.lexpres.listlet.length > 1) buf += ' et '
          buf += '$ ' + stor.lexpres.listlet[i].let + ' = ' + stor.lexpres.listlet[i].val + ' $'
        }
        j3pAffiche(stor.lesdiv.consigne, null, 'On veut calculer $A$ pour ' + buf)
      }
      stor.nbligne = 0
    }
    if (stor.encours === 'remp') poseRemp()
    if (stor.encours === 'cal') poseCal()
    if (stor.encours === 'op') poseOp()
  }

  function poseRemp () {
    j3pEmpty(stor.lesdiv.enoncejus)
    j3pAffiche(stor.lesdiv.enoncejus, null, '<b>Remplace les lettres par leur valeur</b>')
    const tty = addDefaultTable(stor.lesdiv.travailjus, 1, 2)
    j3pAffiche(tty[0][0], null, '$A = $')
    stor.lexpres.afficheRemp(tty[0][1])
  }
  function poseOp () {
    let i
    if (stor.lexpres.isQu()) {
      if (stor.foco) {
        stor.nbligne++
        stor.where = stor.tabGh.cells[stor.nbligne]
        j3pAffiche(stor.where[0], null, '$A = $')
        stor.lexpres.afficheRemp(stor.where[1], true)
        stor.foco = false
        stor.where[0].style.color = me.styles.petit.correction.color
        stor.where[1].style.color = me.styles.petit.correction.color
      }
    } else {
      if (stor.nocal) {
        j3pEmpty(stor.tabGh.lines[stor.nbligne])
      }
    }

    const tab = stor.lexpres.chopelesLet()
    for (i = 0; i < tab.length; i++) {
      tab[i].type = 'val'
    }
    stor.nbligne++
    j3pEmpty(stor.lesdiv.enoncejus)
    j3pAffiche(stor.lesdiv.enoncejus, null, '<b>Sélectionne une (ou plusieurs) opération(s) que l’on peut effectuer en premier.</b>')
    if (!stor.zouzoul) {
      stor.tabGh = addDefaultTableDetailed(stor.lesdiv.travail, 30, 2)
      for (let i = 0; i < 30; i++) {
        for (let j = 0; j < 2; j++) stor.tabGh.cells[i][j].style.padding = 0
      }
      stor.zouzoul = true
    }
    stor.where = stor.tabGh.cells[stor.nbligne]

    if (!stor.lexpres.isQu()) {
      j3pAffiche(stor.where[0], null, '$A = $')
      stor.lexpres.afficheOp(stor.where[1])
      stor.foco = false
      return
    }
    stor.squizz = true
    stor.encours = 'cal'
    poseCal()
  }
  function poseCal () {
    if (stor.squizz) {
      stor.lexpres.listSel = [{ rep: true, coince: 'o1', ki: { select: true }, elem: stor.lexpres.cont }]
    }
    stor.lexpres.TransExp()
    stor.foco = false
    stor.nbligne++
    stor.nocal = true
    j3pEmpty(stor.lesdiv.enoncejus)
    j3pAffiche(stor.lesdiv.enoncejus, null, '<b>Effectue le (ou les) calcul(s).</b>')
    stor.where = stor.tabGh.cells[stor.nbligne]
    j3pAffiche(stor.where[0], null, '$A = $')

    stor.lexpres.afficheRemp(stor.where[1])
  }

  function boutonfen () {
    j3pToggleFenetres('Calculatrice')
  }

  // appelé par le switch correction
  // (qui ne fait donc pas de finCorrection() pour ne pas incrémenter le nb d'essais)
  function etape2 () {
    j3pDetruit(stor.fakeBtnSuite)
    stor.fakeBtnSuite = null
    // on peut remettre le bouton caché précédemment (pour mettre notre fakeBtnSuite)
    me.afficheBoutonValider()
    // on change l'opération courante et on repose une question
    if (stor.encours === 'remp' || stor.encours === 'cal') {
      stor.encours = 'op'
    } else {
      stor.encours = 'cal'
    }
    poseQuestion()
  }

  // @todo en faire une classe avec des méthodes (plutôt que de la déclarer à l'intérieur de cette section et affecter des fonctions à des propriétés)
  function Express () {
    const objet = this
    objet.cont = { id: 'main', type: 'empty' }
    objet.id = 'a0azrr'
    objet.listSel = []
    objet.comptop = 0

    function initLet () {
      objet.letpos = ['a', 'b', 'c', 'g', 'h', 'j', 'k', 'l', 'n', 'o', 'p', 'q', 'r', 'u', 'w', 'x', 'y', 'z']
      objet.letpos = j3pShuffle(objet.letpos)
      objet.listlet = []
      objet.zones = []
      objet.needFois = []
      objet.posFois = []
      objet.needPar = []
    }

    function newlet () {
      return objet.letpos.pop()
    }

    function detPlus () {
      let ret = j3pGetRandomBool()
      if (!ds.Addition) ret = false
      if (!ds.Soustraction) ret = true
      return ret
    }

    function detFois () {
      let ret = j3pGetRandomBool()
      if (!ds.Multiplication) ret = false
      if (!ds.Division) ret = true
      return ret
    }

    function calculSomme (elem) {
      let ret = elem.terme1.cont.val
      if (elem.terme2.plus) {
        ret += elem.terme2.cont.val
      } else {
        ret -= elem.terme2.cont.val
      }
      if (elem.nbtermes > 2) {
        if (elem.terme3.plus) {
          ret += elem.terme3.cont.val
        } else {
          ret -= elem.terme3.cont.val
        }
      }
      if (elem.nbtermes > 3) {
        if (elem.terme4.plus) {
          ret += elem.terme4.cont.val
        } else {
          ret -= elem.terme4.cont.val
        }
      }
      return ret
    }

    function calculpuis (elem) {
      return Math.pow(elem.fact.val, elem.expo.val)
    }

    function calculFois (elem) {
      let ret = elem.fact1.cont.val
      if (elem.fact2.fois) {
        ret = elem.fact2.cont.val * ret
      } else {
        ret = ret / elem.fact2.cont.val
      }
      if (elem.nbfact > 2) {
        if (elem.fact3.fois) {
          ret = elem.fact3.cont.val * ret
        } else {
          ret = ret / elem.fact3.cont.val
        }
      }
      if (elem.nbfact > 3) {
        if (elem.fact4.fois) {
          ret = ret * elem.fact4.cont.val
        } else {
          ret = ret / elem.fact4.cont.val
        }
      }
      return ret
    }

    function calculFoisDiv (elem) {
      let ret = 1
      if (!elem.fact2.fois) {
        ret = elem.fact2.cont.val * ret
      }
      if (elem.nbfact > 2) {
        if (!elem.fact3.fois) {
          ret = elem.fact3.cont.val * ret
        }
      }
      if (elem.nbfact > 3) {
        if (!elem.fact4.fois) {
          ret = ret * elem.fact4.cont.val
        }
      }
      return j3pArrondi(ret, 0)
    }

    function calculFoisPlus (elem) {
      let ret = elem.fact1.cont.val
      if (elem.fact2.fois) {
        ret = elem.fact2.cont.val * ret
      }
      if (elem.nbfact > 2) {
        if (elem.fact3.fois) {
          ret = elem.fact3.cont.val * ret
        }
      }
      if (elem.nbfact > 3) {
        if (elem.fact4.fois) {
          ret = ret * elem.fact4.cont.val
        }
      }
      return j3pArrondi(ret, 0)
    }

    function verif (elem) {
      const ret = {}
      if (elem.type !== 'empty') {
        objet.nombrilise(elem)
        ret.used = true
        ret.val = elem.val
      } else {
        ret.used = false
        ret.val = 0
      }
      return ret
    }

    function verifP (elem) {
      const ret = {}
      if (elem.type !== 'empty') {
        objet.nombrilise(elem)
        ret.used = true
        ret.val = elem.val
      } else {
        ret.used = false
        ret.val = 1
      }
      return ret
    }

    function modifPuis (elem, cb) {
      const facbase = elem.fact.val
      const expo = elem.expo.val
      let tto, nf
      let mod = 1
      do {
        mod++
        nf = facbase * mod
        tto = Math.pow(nf, expo)
      } while (tto - elem.val < cb)
      switch (elem.fact) {
        case 'val':
          elem.fact.val = nf
          break
        case '+':
          modifSom(elem.fact, nf - facbase)
          break
        case 'x':
          modifFois(elem.fact, nf - facbase)
          break
        case 'p':
          modifPuis(elem.fact, nf - facbase)
          break
      }
      elem.val = calculpuis(elem)
    }

    function modifPuisFois (elem, cb) {
      switch (elem.fact.type) {
        case 'val':
          elem.fact.val *= cb
          break
        case '+':
          modifSom(elem.fact, cb - elem.fact.val % cb)
          break
        case 'x':
          modifFois(elem.fact, cb)
          break
        case 'puis':
          modifPuisFois(elem.fact, cb)
          break
      }
      elem.val = calculpuis(elem)
    }

    function modifFois (elem, cb) {
      switch (elem.fact1.cont.type) {
        case 'val':
          elem.fact1.cont.val *= cb
          return
        case '+':
          modifSom(elem.fact1.cont, cb)
          return
        case 'x':
          modifFois(elem.fact1.cont, cb)
          return
        case 'puis':
          modifPuisFois(elem.fact1.cont, cb)
          return
      }
      elem.val = calculFois(elem)
    }

    function modifSom (elem, cb) {
      let newmod
      switch (elem.terme1.cont.type) {
        case 'val':
          elem.terme1.cont.val += cb
          break
        case '+':
          modifSom(elem.terme1.cont, cb)
          break
        case 'x':
          newmod = Math.ceil(elem.terme1.cont.val / cb)
          modifFois(elem.terme1.cont, newmod)
          break
        case 'p':
          modifPuis(elem.terme1.cont, cb)
          break
      }
      elem.val = calculSomme(elem)
    }

    function transElem (elem, repere) {
      switch (elem.type) {
        case 'som':
          if (repere === 'o1') {
            if (elem.terme1.cont.type === 'let') vireLet(elem.terme1.cont.let)
            if (elem.terme2.cont.type === 'let') vireLet(elem.terme2.cont.let)
            if (elem.nbtermes === 2) {
              objet.metInc(elem)
            } else {
              if (elem.terme2.plus) {
                elem.terme1.cont.val = elem.terme1.cont.val + elem.terme2.cont.val
              } else {
                elem.terme1.cont.val = elem.terme1.cont.val - elem.terme2.cont.val
              }
              objet.metInc(elem.terme1.cont)
              elem.nbtermes--
              elem.terme2 = j3pClone(elem.terme3)
              elem.terme3 = j3pClone(elem.terme4)
            }
          }
          if (repere === 'o2') {
            if (elem.terme3.cont.type === 'let') vireLet(elem.terme3.cont.let)
            if (elem.terme2.cont.type === 'let') vireLet(elem.terme2.cont.let)
            if (elem.terme3.plus) {
              elem.terme2.cont.val = elem.terme2.cont.val + elem.terme3.cont.val
            } else {
              elem.terme2.cont.val = elem.terme2.cont.val - elem.terme3.cont.val
            }
            objet.metInc(elem.terme2.cont)
            elem.nbtermes--
            elem.terme3 = j3pClone(elem.terme4)
          }
          if (repere === 'o3') {
            if (elem.terme3.cont.type === 'let') vireLet(elem.terme3.cont.let)
            if (elem.terme4.cont.type === 'let') vireLet(elem.terme4.cont.let)
            if (elem.terme4.plus) {
              elem.terme3.cont.val = elem.terme3.cont.val + elem.terme4.cont.val
            } else {
              elem.terme3.cont.val = elem.terme3.cont.val - elem.terme4.cont.val
            }
            objet.metInc(elem.terme3.cont)
            elem.nbtermes--
          }
          break
        case 'puis':
          objet.metInc(elem)
          break
        case 'prod':
          if (repere === 'o1') {
            if (elem.fact1.cont.type === 'let') vireLet(elem.fact1.cont.let)
            if (elem.fact2.cont.type === 'let') vireLet(elem.fact2.cont.let)
            if (elem.nbfact === 2) {
              objet.metInc(elem)
            } else {
              if (elem.fact2.fois) {
                elem.fact1.cont.val = elem.fact1.cont.val * elem.fact2.cont.val
              } else {
                elem.fact1.cont.val = elem.fact1.cont.val / elem.fact2.cont.val
              }
              objet.metInc(elem.fact1.cont)
              elem.nbfact--
              elem.fact2 = j3pClone(elem.fact3)
              elem.fact3 = j3pClone(elem.fact4)
            }
          }
          if (repere === 'o2') {
            if (elem.fact3.cont.type === 'let') vireLet(elem.fact3.cont.let)
            if (elem.fact2.cont.type === 'let') vireLet(elem.fact2.cont.let)
            if (elem.fact3.fois) {
              elem.fact2.cont.val = elem.fact2.cont.val * elem.fact3.cont.val
            } else {
              elem.fact2.cont.val = elem.fact2.cont.val / elem.fact3.cont.val
            }
            objet.metInc(elem.fact3.cont)
            elem.nbfact--
            elem.fact3 = j3pClone(elem.fact4)
          }
          if (repere === 'o3') {
            if (elem.fact3.cont.type === 'let') vireLet(elem.fact3.cont.let)
            if (elem.fact4.cont.type === 'let') vireLet(elem.fact4.cont.let)
            if (elem.fact4.plus) {
              elem.fact3.cont.val = elem.fact3.cont.val * elem.fact4.cont.val
            } else {
              elem.fact3.cont.val = elem.fact3.cont.val / elem.fact4.cont.val
            }
            objet.metInc(elem.fact4.cont)
            elem.nbfact--
          }
          break
      }
    }

    function vireLet (letr) {
      let i
      for (i = 0; i < objet.listlet.length; i++) {
        if (objet.listlet[i].let === letr) {
          objet.listlet.splice(i, 1)
          break
        }
      }
    }

    objet.ccc = 0
    objet.isQu = function (elem) {
      let bim
      if (elem === undefined) elem = objet.cont
      switch (elem.type) {
        case 'val': return true
        case 'let': return true
        case 'empty': return true
        case 'puis': return (elem.fact.type === 'val' || elem.fact.type === 'let')
        case 'som':
          bim = (elem.terme1.cont.type === 'val' || elem.terme1.cont.type === 'let') && (elem.terme2.cont.type === 'val' || elem.terme2.cont.type === 'let')
          if (elem.nbtermes > 2) bim = bim && (elem.terme3.cont.type === 'val' || elem.terme3.cont.type === 'let')
          if (elem.nbtermes > 3) bim = bim && (elem.terme4.cont.type === 'val' || elem.terme4.cont.type === 'let')
          return bim
        case 'prod':
          bim = (elem.fact1.cont.type === 'val' || elem.fact1.cont.type === 'let') && (elem.fact2.cont.type === 'val' || elem.fact2.cont.type === 'let')
          if (elem.nbfact > 2) bim = bim && (elem.fact3.cont.type === 'val' || elem.fact3.cont.type === 'let')
          if (elem.nbfact > 3) bim = bim && (elem.fact4.cont.type === 'val' || elem.fact4.cont.type === 'let')
          return bim
      }
    }
    objet.MakeClic = function () {
      this.select = !this.select
      if (this.select) {
        this.setAttribute('class', 'opautreselect')
      } else {
        this.setAttribute('class', 'opautre')
      }
    }
    objet.MakeClicP = function () {
      this.select = !this.select
      if (this.select) {
        this.setAttribute('class', 'opautreselectpuis')
      } else {
        this.setAttribute('class', 'opautrepuis')
      }
    }
    objet.metVal = function (elem, val) {
      elem.type = 'val'
      elem.val = val
      return val
    }
    objet.ajVal = function (elem, val) {
      elem.type = 'val'
      elem.val = val
    }
    objet.ajLettre = function (elem) {
      elem.type = 'let'
      elem.let = newlet()
    }
    objet.ajSomme = function (elem) {
      if (!ds.Addition && !ds.Soustraction) {
        // FIXME ce truc réaffecte la variable locale elem locale, mais ça ne modifie pas l’objet passé en paramètre (2 autres cas plus loin)
        // c’est une mauvaise pratique de modifier les paramètres passés à une fonction, il vaut mieux retourner le nouvel objet
        elem = { type: 'empty' }
        return
      }
      elem.type = 'som'
      elem.nbtermes = j3pGetRandomInt(2, 2)
      elem.terme1 = {
        plus: 'ind',
        cont: {
          dad: '+',
          type: 'empty',
          id: elem.id + 't1'
        }
      }
      elem.terme2 = {
        plus: detPlus(),
        cont: {
          dad: '+',
          type: 'empty',
          id: elem.id + 't2'
        }
      }
      if (elem.nbtermes > 2) {
        elem.terme3 = {
          plus: detPlus(),
          cont: {
            dad: '+',
            type: 'empty',
            id: elem.id + 't3'
          }
        }
      } else {
        elem.terme3 = {
          type: 'rien',
          cont: {
            type: 'empty',
            id: elem.id + 't3'
          }
        }
      }
      if (elem.nbtermes > 3) {
        elem.terme4 = {
          plus: detPlus(),
          cont: {
            dad: '+',
            type: 'empty',
            id: elem.id + 't4'
          }
        }
      } else {
        elem.terme4 = {
          type: 'rien',
          cont: {
            type: 'empty',
            id: elem.id + 't4'
          }
        }
      }
    }
    objet.ajProd = function (elem) {
      if (!ds.Multiplication && !ds.Division) {
        elem = { type: 'empty' }
        return
      }
      elem.type = 'prod'
      elem.nbfact = j3pGetRandomInt(2, 2)
      elem.fact1 = {
        fois: 'ind',
        cont: {
          dad: 'x',
          type: 'empty',
          id: elem.id + 'f1'
        }
      }
      elem.fact2 = {
        fois: detFois(),
        cont: {
          dad: 'x',
          type: 'empty',
          id: elem.id + 'f2'
        }
      }
      if (elem.nbfact > 2) {
        elem.fact3 = {
          fois: detFois(),
          cont: {
            dad: 'x',
            type: 'empty',
            id: elem.id + 'f3'
          }
        }
      } else {
        elem.fact3 = {
          type: 'rien',
          cont: {
            type: 'empty',
            id: elem.id + 'f3'
          }
        }
      }
      if (elem.nbfact > 3) {
        elem.fact4 = {
          fois: detFois(),
          cont: {
            dad: 'x',
            type: 'empty',
            id: elem.id + 'f4'
          }
        }
      } else {
        elem.fact4 = {
          type: 'rien',
          cont: {
            type: 'empty',
            id: elem.id + 'f4'
          }
        }
      }
    }
    objet.ajPuissance = function (elem) {
      if (!ds.CarreCube && !ds.Puissance) {
        elem = { type: 'empty' }
        return
      }
      elem.type = 'puis'
      elem.fact = {
        dad: 'p',
        type: 'empty',
        id: elem.id + 'fact'
      }
      elem.expo = {
        dad: 'p',
        type: 'empty',
        id: elem.id + 'expo'
      }
    }
    objet.retrouveById = function (id, elem) {
      if (elem === undefined) elem = objet.cont
      if (elem.id === id) return elem
      switch (elem.type) {
        case 'som':
          if (objet.retrouveById(id, elem.terme1.cont) !== undefined) return objet.retrouveById(id, elem.terme1.cont)
          if (objet.retrouveById(id, elem.terme2.cont) !== undefined) return objet.retrouveById(id, elem.terme2.cont)
          if (objet.retrouveById(id, elem.terme3.cont) !== undefined) return objet.retrouveById(id, elem.terme3.cont)
          if (objet.retrouveById(id, elem.terme4.cont) !== undefined) return objet.retrouveById(id, elem.terme4.cont)
          break
        case 'prod':
          if (objet.retrouveById(id, elem.fact1.cont) !== undefined) return objet.retrouveById(id, elem.fact1.cont)
          if (objet.retrouveById(id, elem.fact2.cont) !== undefined) return objet.retrouveById(id, elem.fact2.cont)
          if (objet.retrouveById(id, elem.fact3.cont) !== undefined) return objet.retrouveById(id, elem.fact3.cont)
          if (objet.retrouveById(id, elem.fact4.cont) !== undefined) return objet.retrouveById(id, elem.fact4.cont)
          break
        case 'puis':
          if (objet.retrouveById(id, elem.fact) !== undefined) return objet.retrouveById(id, elem.fact)
          break
      }
    }
    objet.chopelesvides = function (elem) {
      if (elem === undefined) elem = objet.cont
      let buf
      switch (elem.type) {
        case 'empty':
          return [elem]
        case 'val':
          return []
        case 'let':
          return []
        case 'som':
          buf = []
          buf = buf.concat(objet.chopelesvides(elem.terme1.cont))
          buf = buf.concat(objet.chopelesvides(elem.terme2.cont))
          if (elem.terme3.type !== 'rien') buf = buf.concat(objet.chopelesvides(elem.terme3.cont))
          if (elem.terme4.type !== 'rien') buf = buf.concat(objet.chopelesvides(elem.terme4.cont))
          return buf
        case 'prod':
          buf = []
          buf = buf.concat(objet.chopelesvides(elem.fact1.cont))
          buf = buf.concat(objet.chopelesvides(elem.fact2.cont))
          if (elem.fact3.type !== 'rien') buf = buf.concat(objet.chopelesvides(elem.fact3.cont))
          if (elem.fact4.type !== 'rien') buf = buf.concat(objet.chopelesvides(elem.fact4.cont))
          return buf
        case 'puis':
          buf = []
          buf = buf.concat(objet.chopelesvides(elem.fact))
          return buf
      }
    }
    objet.chopelesVal = function (elem) {
      if (elem === undefined) elem = objet.cont
      let buf
      switch (elem.type) {
        case 'empty':
          return []
        case 'val':
          return [elem]
        case 'let':
          return []
        case 'som':
          buf = []
          buf = buf.concat(objet.chopelesVal(elem.terme1.cont))
          buf = buf.concat(objet.chopelesVal(elem.terme2.cont))
          if (elem.terme3.type !== 'rien') buf = buf.concat(objet.chopelesVal(elem.terme3.cont))
          if (elem.terme4.type !== 'rien') buf = buf.concat(objet.chopelesVal(elem.terme4.cont))
          return buf
        case 'prod':
          buf = []
          buf = buf.concat(objet.chopelesVal(elem.fact1.cont))
          buf = buf.concat(objet.chopelesVal(elem.fact2.cont))
          if (elem.fact3.type !== 'rien') buf = buf.concat(objet.chopelesVal(elem.fact3.cont))
          if (elem.fact4.type !== 'rien') buf = buf.concat(objet.chopelesVal(elem.fact4.cont))
          return buf
        case 'puis':
          buf = []
          buf = buf.concat(objet.chopelesVal(elem.fact))
          return buf
      }
    }
    objet.chopelesLet = function (elem) {
      if (elem === undefined) elem = objet.cont
      let buf
      switch (elem.type) {
        case 'empty':
          return []
        case 'val':
          return []
        case 'let':
          return [elem]
        case 'som':
          buf = []
          buf = buf.concat(objet.chopelesLet(elem.terme1.cont))
          buf = buf.concat(objet.chopelesLet(elem.terme2.cont))
          if (elem.terme3.type !== 'rien') buf = buf.concat(objet.chopelesLet(elem.terme3.cont))
          if (elem.terme4.type !== 'rien') buf = buf.concat(objet.chopelesLet(elem.terme4.cont))
          return buf
        case 'prod':
          buf = []
          buf = buf.concat(objet.chopelesLet(elem.fact1.cont))
          buf = buf.concat(objet.chopelesLet(elem.fact2.cont))
          if (elem.fact3.type !== 'rien') buf = buf.concat(objet.chopelesLet(elem.fact3.cont))
          if (elem.fact4.type !== 'rien') buf = buf.concat(objet.chopelesLet(elem.fact4.cont))
          return buf
        case 'puis':
          buf = []
          buf = buf.concat(objet.chopelesLet(elem.fact))
          return buf
      }
    }
    objet.nombrilise = function (elem) {
      let n1, n2, n3, n4, mod, bornemoins, buftot, ok, f1, d1
      bornemoins = -9
      const borneplus = 9
      if (!ds.Relatifs) bornemoins = 0
      if (elem === undefined) elem = objet.cont
      switch (elem.type) {
        case 'som': {
          n1 = verif(elem.terme1.cont)
          n2 = verif(elem.terme2.cont)
          if (elem.nbtermes > 2) {
            n3 = verif(elem.terme3.cont)
          } else {
            n3 = {
              used: true,
              val: 0
            }
          }
          if (elem.nbtermes > 3) {
            n4 = verif(elem.terme4.cont)
          } else {
            n4 = {
              used: true,
              val: 0
            }
          }
          let cpt = 0
          while (!(n1.used && n2.used && n3.used && n4.used)) {
            cpt++
            buftot = 0
            if (n1.used) buftot = n1.val
            if (n2.used) {
              if (elem.terme2.plus) {
                buftot += n2.val
              } else {
                buftot -= n2.val
              }
            }
            do {
              mod = j3pGetRandomInt(bornemoins, borneplus)
            } while (mod - buftot === 0 || mod === 0)
            if (!n1.used) {
              if (!ds.Relatifs && mod - buftot < 0) {
                mod = -mod
                buftot = -buftot
                elem.terme2.plus = !elem.terme2.plus
                elem.terme3.plus = !elem.terme3.plus
                elem.terme4.plus = !elem.terme4.plus
              }
              n1.val = objet.metVal(elem.terme1.cont, mod - buftot)
              n1.used = true
            } else if (!n2.used) {
              n2.used = true
              if (elem.terme2.plus) {
                n2.val = objet.metVal(elem.terme2.cont, mod - buftot)
              } else {
                n2.val = objet.metVal(elem.terme2.cont, buftot - mod)
              }
            } else if (!n3.used) {
              n3.used = true
              if (elem.terme3.plus) {
                n3.val = objet.metVal(elem.terme3.cont, mod - buftot)
              } else {
                n3.val = objet.metVal(elem.terme3.cont, buftot - mod)
              }
            } else {
              n4.used = true
              if (elem.terme4.plus) {
                n4.used = true
                n4.val = objet.metVal(elem.terme4.cont, mod - buftot)
              } else {
                n4.val = objet.metVal(elem.terme4.cont, buftot - mod)
              }
            }
            if (cpt === 1000) {
              return
            }
          }

          buftot = calculSomme(elem)
          if (Math.abs(buftot) - 50 > 0) {
            if (buftot > 0) {
              modifSom(elem, -buftot + 50 - j3pGetRandomInt(1, 20))
            } else {
              modifSom(elem, -buftot - 50 + j3pGetRandomInt(1, 20))
            }
          }
          elem.val = calculSomme(elem)
          break
        }
        case 'prod': {
          n1 = verifP(elem.fact1.cont)
          n2 = verifP(elem.fact2.cont)
          if (elem.nbfact > 2) {
            n3 = verifP(elem.fact3.cont)
          } else {
            n3 = {
              used: true,
              val: 1
            }
          }
          if (elem.nbfact > 3) {
            n4 = verifP(elem.fact4.cont)
          } else {
            n4 = {
              used: true,
              val: 1
            }
          }
          mod = 1
          let ddiv = 1
          if (n1.used) mod *= n1.val
          if (n2.used) {
            if (elem.fact2.fois) {
              mod *= n2.val
            } else {
              ddiv *= n2.val
            }
          }
          if (n3.used) {
            if (elem.fact3.fois) { mod *= n3.val } else { ddiv *= n3.val }
          }
          if (n4.used) {
            if (elem.fact4.fois) { mod *= n4.val } else { ddiv *= n4.val }
          }
          while (!(n1.used && n2.used && n3.used && n4.used)) {
            if (!n1.used) {
              ok = j3pGetRandomInt(2, 9)
              if (ds.Relatifs && j3pGetRandomBool()) ok = -ok
              n1.val = objet.metVal(elem.fact1.cont, ddiv * ok)
              n1.used = true
              mod *= ok
            } else if (!n2.used) {
              n2.used = true
              if (elem.fact2.fois) {
                ok = j3pGetRandomInt(2, 9)
                if (ds.Relatifs && j3pGetRandomBool()) ok = -ok
                n2.val = objet.metVal(elem.fact2.cont, ok)
                mod *= ok
              } else {
                ok = undivde(Math.abs(mod))
                if (ds.Relatifs && j3pGetRandomBool()) ok = -ok
                n2.val = objet.metVal(elem.fact2.cont, ok)
                ddiv *= ok
              }
            } else if (!n3.used) {
              n3.used = true
              n3.val = objet.metVal(elem.fact3.cont, mod)
            } else {
              n4.used = true
              n4.val = objet.metVal(elem.fact4.cont, mod)
            }
          }
          buftot = calculFois(elem)
          if (Math.floor(buftot) !== buftot) {
            f1 = calculFoisPlus(elem)
            d1 = calculFoisDiv(elem)
            mod = j3pPGCD(f1, d1, { negativesAllowed: true, valueIfZero: 1 })
            modifFois(elem, Math.abs(j3pArrondi(d1 / mod, 0)))
          }
          elem.val = calculFois(elem)
          break
        }

        case 'puis':
          if (elem.fact.type !== 'empty') {
            objet.nombrilise(elem.fact)
          } else {
            elem.fact.type = 'val'
            elem.fact.val = j3pGetRandomInt(2, 10)
            if (ds.Relatifs && j3pGetRandomBool()) elem.fact.val = -elem.fact.val
          }

          if (ds.Puissance) {
            elem.expo.val = j3pGetRandomInt(2, 5)
            elem.expo.type = 'val'
          } else {
            elem.expo.val = j3pGetRandomInt(2, 3)
            elem.expo.type = 'val'
          }
          elem.val = calculpuis(elem)
      }
    }
    objet.faitAffiche = function (elem) {
      if (elem === undefined) elem = objet.cont
      let buf = ''
      switch (elem.type) {
        case 'som':
          buf = objet.faitAffiche(elem.terme1.cont)
          if (elem.terme2.cont.type === 'val') {
            if (elem.terme2.plus) {
              if (elem.terme2.cont.val > 0) {
                buf += ' + '
              } else {
                buf += ' - '
              }
            } else {
              if (elem.terme2.cont.val > 0) {
                buf += ' - '
              } else {
                buf += ' + '
              }
            }
            buf += Math.abs(elem.terme2.cont.val)
          } else {
            if (elem.terme2.plus) {
              buf += ' + '
            } else {
              buf += ' - '
            }
            buf += objet.faitAffiche(elem.terme2.cont) + ' '
          }
          if (elem.nbtermes > 2) {
            if (elem.terme3.cont.type === 'val') {
              if (elem.terme3.plus) {
                if (elem.terme3.cont.val > 0) {
                  buf += ' + '
                } else {
                  buf += ' - '
                }
              } else {
                if (elem.terme3.cont.val > 0) {
                  buf += ' - '
                } else {
                  buf += ' + '
                }
              }
              buf += Math.abs(elem.terme3.cont.val)
            } else {
              if (elem.terme3.plus) {
                buf += ' + '
              } else {
                buf += ' - '
              }
              buf += objet.faitAffiche(elem.terme3.cont) + ' '
            }
          }
          if (elem.nbtermes > 3) {
            if (elem.terme4.cont.type === 'val') {
              if (elem.terme4.plus) {
                if (elem.terme4.cont.val > 0) {
                  buf += ' + '
                } else {
                  buf += ' - '
                }
              } else {
                if (elem.terme4.cont.val > 0) {
                  buf += ' - '
                } else {
                  buf += ' + '
                }
              }
              buf += Math.abs(elem.terme4.cont.val)
            } else {
              if (elem.terme4.plus) {
                buf += ' + '
              } else {
                buf += ' - '
              }
              buf += objet.faitAffiche(elem.terme4.cont) + ' '
            }
          }
          if (elem.dad === 'p' || elem.dad === 'x') {
            buf = ' (' + buf + ') '
          }
          if (elem.terme2.cont.type === 'let' && elem.terme2.cont.val < 0) objet.needPar.push(elem.terme2.cont.let)
          if (elem.nbtermes > 2) if (elem.terme3.cont.type === 'let' && elem.terme3.cont.val < 0) objet.needPar.push(elem.terme3.cont.let)
          if (elem.nbtermes > 3) if (elem.terme4.cont.type === 'let' && elem.terme4.cont.val < 0) objet.needPar.push(elem.terme4.cont.let)
          return buf
        case 'prod':
          if (elem.dad !== undefined && elem.fact1.cont.type === 'val' && elem.fact1.cont.val < 0) {
            buf = ' (' + elem.fact1.cont.val + ') '
          } else {
            buf = objet.faitAffiche(elem.fact1.cont)
          }
          if (elem.fact2.cont.type !== 'val') {
            if (elem.fact2.fois) {
              if (!ds.Xvisible) {
                elem.fact2.cont.foispos = true
                if (elem.fact2.cont.type === 'let') objet.posFois.push(elem.fact2.cont.let)
                if ((elem.fact1.cont.type === 'val') && elem.fact1.cont.val > 0) {
                  if (elem.fact2.cont.type === 'let' && elem.fact2.cont.val > 0) {
                    objet.needFois.push(elem.fact2.cont.let)
                  } else {
                    elem.fact2.cont.needfois = true
                  }
                }
                if (elem.fact1.cont.type === 'let') {
                  elem.fact2.cont.foispos = false
                  buf += ' \\times ' + objet.faitAffiche(elem.fact2.cont)
                } else {
                  buf += objet.faitAffiche(elem.fact2.cont)
                }
              } else {
                buf += ' \\times ' + objet.faitAffiche(elem.fact2.cont)
              }
            } else {
              buf += ' \\div ' + objet.faitAffiche(elem.fact2.cont)
            }
          } else {
            if (elem.fact2.fois) {
              buf += ' \\times '
            } else {
              buf += ' \\div '
            }
            if (elem.fact2.cont.type === 'val' && elem.fact2.cont.val < 0) {
              buf += ' (' + elem.fact2.cont.val + ') '
            } else {
              buf += elem.fact2.cont.val
            }
          }
          if (elem.nbfact > 2) {
            if (elem.fact3.cont.type === 'let' && (elem.fact2.fois === elem.fact3.fois) && (elem.fact3.fois)) {
              buf += elem.fact3.cont.let
              objet.needFois.push(elem.fact3.cont.let)
            } else {
              if (elem.fact3.fois) {
                buf += ' \\times '
              } else {
                buf += ' \\div '
              }
              if (elem.fact3.cont.type === 'val' && elem.fact3.cont.val < 0) {
                buf += ' (' + elem.fact3.cont.val + ') '
              } else {
                buf += objet.faitAffiche(elem.fact3.cont)
              }
            }
          }
          if (elem.nbfact > 3) {
            if (elem.fact4.cont.type === 'let' && (elem.fact3.fois === elem.fact4.fois) && elem.fact4.fois) {
              buf += elem.fact4.cont.let
              objet.needFois.push(elem.fact4.cont.let)
            } else {
              if (elem.fact4.fois) {
                buf += ' \\times '
              } else {
                buf += ' \\div '
              }
              if (elem.fact4.cont.type === 'val' && elem.fact4.cont.val < 0) {
                buf += ' (' + elem.fact4.cont.val + ') '
              } else {
                buf += objet.faitAffiche(elem.fact4.cont)
              }
            }
          }
          if (elem.dad === 'p') {
            buf = ' (' + buf + ') '
          }
          if (elem.fact2.cont.type === 'let' && elem.fact2.cont.val < 0) objet.needPar.push(elem.fact2.cont.let)
          if (elem.nbfact > 2) if (elem.fact3.cont.type === 'let' && elem.fact3.cont.val < 0) objet.needPar.push(elem.fact3.cont.let)
          if (elem.nbfact > 3) if (elem.fact4.cont.type === 'let' && elem.fact4.cont.val < 0) objet.needPar.push(elem.fact4.cont.let)
          return buf
        case 'puis':
          if (elem.fact.type === 'val') {
            if (elem.fact.val < 0) {
              buf = ' (' + elem.fact.val + ')^{' + elem.expo.val + '} '
            } else {
              buf = ' ' + elem.fact.val + '^{' + elem.expo.val + '} '
            }
          } else {
            buf = ' ' + objet.faitAffiche(elem.fact) + '^{' + elem.expo.val + '} '
          }
          if (elem.fact.type === 'let' && elem.fact.val < 0) objet.needPar.push(elem.fact.let)
          if (elem.fact.type === 'let' && elem.needfois && elem.fact.val > 0) objet.needFois.push(elem.fact.let)
          if (elem.fact.type === 'let' && elem.foispos) objet.posFois.push(elem.fact.let)
          return buf
        case 'empty':
          return ' \\text{vide} '
        case 'let':
          return ' ' + elem.let + ' '
        case 'val':
          return ' ' + elem.val + ' '
      }
    }
    objet.metInc = function (elem) {
      elem.type = 'let'
      elem.let = newlet()
      objet.listlet.push({ let: elem.let, val: elem.val })
    }
    objet.afficheRemp = function (w, b) {
      objet.ccc++
      let laf = objet.faitAffiche()
      const tlaf = []
      let i, buf
      const gub = j3pClone(objet.listlet)
      for (i = 0; i < gub.length; i++) {
        gub[i].dist = laf.indexOf(gub[i].let)
      }
      gub.sort(function (a, b) { return a.dist - b.dist })
      for (i = 0; i < gub.length; i++) {
        tlaf.push(laf.substring(0, laf.indexOf(gub[i].let)))
        laf = laf.substring(laf.indexOf(gub[i].let) + 1)
      }
      tlaf.push(laf)
      const tyud = addDefaultTable(w, 1, tlaf.length + gub.length)
      for (i = 0; i < tlaf.length; i++) {
        j3pAffiche(tyud[0][i * 2], null, '$' + tlaf[i] + '$')
      }
      if (b === true) {
        for (i = 0; i < gub.length; i++) {
          buf = ''
          if (objet.needFois.indexOf(gub[i].let) !== -1) buf = ' \\times '
          if (objet.needPar.indexOf(gub[i].let) !== -1) buf += ' ('
          buf += gub[i].val
          if (objet.needPar.indexOf(gub[i].let) !== -1) buf += ') '
          j3pAffiche(tyud[0][i * 2 + 1], null, '$' + buf + '$')
        }
      } else {
        for (i = 0; i < gub.length; i++) {
          objet.zones.push(new ZoneStyleMathquill3(tyud[0][i * 2 + 1], { restric: '0123456789*()-', limite: 15, hasAutoKeyboard: true, inverse: true, limitenb: 4, enter: me.sectionCourante.bind(me) }))
        }
        objet.listlet = j3pClone(gub)
      }
    }
    objet.afficheOp = function (w, elem, b) {
      let nbc, cpt, ok2, ok3, ok1, er1, er2, er3, er4, apush
      if (elem === undefined) {
        elem = objet.cont
        objet.listSel = []
      }
      let id
      switch (elem.type) {
        case 'som':
          nbc = 3
          cpt = 0
          ok1 = ok2 = ok3 = true
          er1 = er2 = er3 = er4 = ''
          if (elem.terme1.cont.type !== 'val') ok1 = false
          if (elem.terme1.cont.type === 'x') er1 = 'Les multiplications et divisions sont prioritaires sur les additions et les soustractions'
          if (elem.terme1.cont.type === 'puis') er1 = 'Les puissances sont prioritaires sur les additions et les soustractions'
          if (elem.terme2.cont.type !== 'val') ok1 = ok2 = false
          if (elem.terme2.cont.type === 'x') er2 = 'Les multiplications et divisions sont prioritaires sur les additions et les soustractions'
          if (elem.terme2.cont.type === 'puis') er2 = 'Les puissances sont prioritaires sur les additions et les soustractions'
          if (elem.dad === 'p' || elem.dad === 'x') nbc += 2
          if (elem.nbtermes > 2) nbc += 2
          if (elem.nbtermes > 3) nbc += 2
          id = addDefaultTableDetailed(w, 1, nbc)
          modif(id, 1, nbc)
          if (elem.dad === 'p' || elem.dad === 'x') {
            j3pAffiche(id.cells[0][0], null, '$($')
            j3pAffiche(id.cells[0][nbc - 1], null, '$)$')
            cpt++
          }
          objet.afficheOp(id.cells[0][cpt], elem.terme1.cont, b)
          cpt++
          if (elem.terme2.cont.type === 'val') {
            if (elem.terme2.plus) {
              if (elem.terme2.cont.val > 0) {
                j3pAffiche(id.cells[0][cpt], null, '$+$')
                cpt++
              } else {
                j3pAffiche(id.cells[0][cpt], null, '$-$')
                er3 = 'Il faut faire les additions soustractions dans l’ordre (ou les regrouper)!'
                ok2 = false
                cpt++
              }
            } else {
              if (elem.terme2.cont.val > 0) {
                j3pAffiche(id.cells[0][cpt], null, '$-$')
                er3 = 'Il faut faire les additions soustractions dans l’ordre (ou les regrouper)!'
                ok2 = false
                cpt++
              } else {
                j3pAffiche(id.cells[0][cpt], null, '$+$')
                cpt++
              }
            }
            j3pAffiche(id.cells[0][cpt], null, '$' + Math.abs(elem.terme2.cont.val) + '$')
            cpt++
          } else {
            if (elem.terme2.plus) {
              j3pAffiche(id.cells[0][cpt], null, '$+$')
              cpt++
            } else {
              j3pAffiche(id.cells[0][cpt], null, '$-$')
              er3 = 'Il faut faire les additions soustractions dans l’ordre (ou les regrouper)!'
              ok2 = false
              cpt++
            }
            objet.afficheOp(id.cells[0][cpt], elem.terme2.cont, b)
            cpt++
          }
          if (b !== true) {
            id.cells[0][cpt - 2].addEventListener('click', objet.MakeClic, false)
            id.cells[0][cpt - 2].select = false
            id.cells[0][cpt - 2].setAttribute('class', 'opautre')
            apush = er1
            if (er2 !== er1) {
              if (er1 !== '') {
                if (er2 !== '') {
                  apush += '<br>' + er2
                }
              } else {
                apush = er2
              }
            }
            objet.listSel.push({ ki: id.cells[0][cpt - 2], rep: ok1, err: apush, elem, coince: 'o1' })
          }
          if (elem.nbtermes > 2) {
            if (elem.terme3.cont.type !== 'val') ok2 = ok3 = false
            if (elem.terme3.cont.type === 'x') er3 = 'Les multiplications et divisions sont prioritaires sur les additions et les soustractions'
            if (elem.terme3.cont.type === 'puis') er3 = 'Les puissances sont prioritaires sur les additions et les soustractions'
          }
          if (elem.nbtermes > 2) {
            if (elem.terme3.cont.type === 'val') {
              if (elem.terme3.plus) {
                if (elem.terme3.cont.val > 0) {
                  j3pAffiche(id.cells[0][cpt], null, '$+$')
                  cpt++
                } else {
                  j3pAffiche(id.cells[0][cpt], null, '$-$')
                  er4 = 'Il faut faire les additions soustractions dans l’ordre (ou les regrouper)!'
                  ok3 = false
                  cpt++
                }
              } else {
                if (elem.terme3.cont.val > 0) {
                  j3pAffiche(id.cells[0][cpt], null, '$-$')
                  er4 = 'Il faut faire les additions soustractions dans l’ordre (ou les regrouper)!'
                  ok3 = false
                  cpt++
                } else {
                  j3pAffiche(id.cells[0][cpt], null, '$+$')
                  cpt++
                }
              }
              j3pAffiche(id.cells[0][cpt], null, '$' + Math.abs(elem.terme3.cont.val) + '$')
              cpt++
            } else {
              if (elem.terme3.plus) {
                j3pAffiche(id.cells[0][cpt], null, '$+$')
                cpt++
              } else {
                j3pAffiche(id.cells[0][cpt], null, '$-$')
                er4 = 'Il faut faire les additions soustractions dans l’ordre (ou les regrouper)!'
                ok3 = false
                cpt++
              }
              objet.afficheOp(id.cells[0][cpt], elem.terme3.cont, b)
              cpt++
            }
            if (b !== true) {
              id.cells[0][cpt - 2].addEventListener('click', objet.MakeClic, false)
              id.cells[0][cpt - 2].select = false
              id.cells[0][cpt - 2].setAttribute('class', 'opautre')
              apush = er2
              if (er3 !== er2) {
                if (er2 !== '') {
                  if (er3 !== '') {
                    apush += '<br>' + er3
                  }
                } else {
                  apush = er3
                }
              }
              objet.listSel.push({ ki: id.cells[0][cpt - 2], rep: ok2, err: apush, elem, coince: 'o2' })
            }
          }
          if (elem.nbtermes > 3) {
            if (elem.terme4.cont.type !== 'val') ok3 = false
            if (elem.terme4.cont.type === 'x') er4 = 'Les multiplications et divisions sont prioritaires sur les additions et les soustractions'
            if (elem.terme4.cont.type === 'puis') er4 = 'Les puissances sont prioritaires sur les additions et les soustractions'
          }
          if (elem.nbtermes > 3) {
            if (elem.terme4.cont.type === 'val') {
              if (elem.terme4.plus) {
                if (elem.terme4.cont.val > 0) {
                  j3pAffiche(id.cells[0][cpt], null, '$+$')
                  cpt++
                } else {
                  j3pAffiche(id.cells[0][cpt], null, '$-$')
                  cpt++
                }
              } else {
                if (elem.terme4.cont.val > 0) {
                  j3pAffiche(id.cells[0][cpt], null, '$-$')
                  cpt++
                } else {
                  j3pAffiche(id.cells[0][cpt], null, '$+$')
                  cpt++
                }
              }
              j3pAffiche(id.cells[0][cpt], null, '$' + Math.abs(elem.terme4.cont.val) + '$')
              cpt++
            } else {
              if (elem.terme2.plus) {
                j3pAffiche(id.cells[0][cpt], null, '$+$')
                cpt++
              } else {
                j3pAffiche(id.cells[0][cpt], null, '$-$')
                cpt++
              }
              objet.afficheOp(id.cells[0][cpt], elem.terme4.cont, b)
              cpt++
            }
            if (b !== true) {
              id.cells[0][cpt - 2].addEventListener('click', objet.MakeClic, false)
              id.cells[0][cpt - 2].select = false
              id.cells[0][cpt - 2].setAttribute('class', 'opautre')
              apush = er3
              if (er4 !== er3) {
                if (er3 !== '') {
                  if (er4 !== '') {
                    apush += '<br>' + er4
                  }
                } else {
                  apush = er4
                }
              }
              objet.listSel.push({ ki: id.cells[0][cpt - 2], rep: ok3, err: apush, elem, coince: 'o3' })
            }
          }
          break
        case 'prod':
          nbc = 3
          cpt = 0
          ok1 = ok2 = ok3 = true
          er1 = er2 = er3 = er4 = ''
          if (elem.fact1.cont.type !== 'val') ok1 = false
          if (elem.fact1.cont.type === 'som') er1 = 'Les parenthèses sont prioritaires !'
          if (elem.fact1.cont.type === 'puis') er1 = 'Les puissances sont prioritaires sur les multiplications/divisions !'
          if (elem.fact2.cont.type !== 'val') ok1 = ok2 = false
          if (elem.fact2.cont.type === 'som') er2 = 'Les parenthèses sont prioritaires !'
          if (elem.fact2.cont.type === 'puis') er2 = 'Les puissances sont prioritaires sur les multiplications/divisions !'

          if (elem.dad === 'p') nbc += 2
          if (elem.nbfact > 2) nbc += 2
          if (elem.nbfact > 3) nbc += 2
          id = addDefaultTableDetailed(w, 1, nbc)
          modif(id, 1, nbc)
          if (elem.dad === 'p') {
            j3pAffiche(id.cells[0][0], null, '$($')
            j3pAffiche(id.cells[0][nbc - 1], null, '$)$')
            cpt++
          }
          if (elem.fact1.cont.type === 'val' && elem.fact1.cont.val < 0) {
            j3pAffiche(id.cells[0][cpt], null, '$(' + elem.fact1.cont.val + ')$')
          } else {
            objet.afficheOp(id.cells[0][cpt], elem.fact1.cont, b)
          }
          cpt++
          if (elem.fact2.fois) {
            j3pAffiche(id.cells[0][cpt], null, ' $\\times$ ')
            cpt++
          } else {
            ok2 = false
            j3pAffiche(id.cells[0][cpt], null, ' $\\div$ ')
            er3 = 'Il faut faire les multiplications et divisions dans l’ordre (ou les regrouper)!'
            cpt++
          }
          if (elem.fact2.cont.type === 'val') {
            if (elem.fact2.cont.val < 0) {
              j3pAffiche(id.cells[0][cpt], null, '$(' + elem.fact2.cont.val + ')$')
              cpt++
            } else {
              j3pAffiche(id.cells[0][cpt], null, '$' + elem.fact2.cont.val + '$')
              cpt++
            }
          } else {
            objet.afficheOp(id.cells[0][cpt], elem.fact2.cont, b)
            cpt++
          }
          if (b !== true) {
            id.cells[0][cpt - 2].addEventListener('click', objet.MakeClic, false)
            id.cells[0][cpt - 2].select = false
            id.cells[0][cpt - 2].setAttribute('class', 'opautre')
            apush = er1
            if (er2 !== er1) {
              if (er1 !== '') {
                if (er2 !== '') {
                  apush += '<br>' + er2
                }
              } else {
                apush = er2
              }
            }
            objet.listSel.push({ ki: id.cells[0][cpt - 2], rep: ok1, err: apush, elem, coince: 'o1' })
          }
          if (elem.nbfact > 2) {
            if (elem.fact3.cont.type !== 'val') ok2 = ok3 = false
            if (elem.fact3.cont.type === 'som') er3 = 'Les parenthèses sont prioritaires !'
            if (elem.fact3.cont.type === 'puis') er3 = 'Les puissances sont prioritaires sur les multiplications/divisions !'
          }
          if (elem.nbfact > 2) {
            if (elem.fact3.fois) {
              j3pAffiche(id.cells[0][cpt], null, '$ \\times$ ')
              cpt++
            } else {
              er4 = 'Il faut faire les multiplications et divisions dans l’ordre (ou les regrouper)!'
              ok3 = false
              j3pAffiche(id.cells[0][cpt], null, ' $\\div$ ')
              cpt++
            }
            if (elem.fact3.cont.type === 'val') {
              if (elem.fact3.cont.val < 0) {
                j3pAffiche(id.cells[0][cpt], null, '$(' + elem.fact3.cont.val + ')$')
                cpt++
              } else {
                j3pAffiche(id.cells[0][cpt], null, '$' + elem.fact3.cont.val + '$')
                cpt++
              }
            } else {
              objet.afficheOp(id.cells[0][cpt], elem.fact3.cont, b)
              cpt++
            }
            if (b !== true) {
              id.cells[0][cpt - 2].addEventListener('click', objet.MakeClic, false)
              id.cells[0][cpt - 2].select = false
              id.cells[0][cpt - 2].setAttribute('class', 'opautre')
              apush = er2
              if (er3 !== er2) {
                if (er2 !== '') {
                  if (er3 !== '') {
                    apush += '<br>' + er3
                  }
                } else {
                  apush = er3
                }
              }
              objet.listSel.push({ ki: id.cells[0][cpt - 2], rep: ok2, err: apush, elem, coince: 'o2' })
            }
          }
          if (elem.nbfact > 3) {
            if (elem.fact4.cont.type !== 'val') ok3 = false
            if (elem.fact4.cont.type === 'som') er4 = 'Les parenthèses sont prioritaires !'
            if (elem.fact4.cont.type === 'puis') er4 = 'Les puissances sont prioritaires sur les multiplications/divisions !'
          }
          if (elem.nbfact > 3) {
            if (elem.fact4.fois) {
              j3pAffiche(id.cells[0][cpt], null, ' $\\times$ ')
              cpt++
            } else {
              j3pAffiche(id.cells[0][cpt], null, ' $\\div$ ')
              cpt++
            }
            if (elem.fact4.cont.type === 'val') {
              if (elem.fact4.cont.val < 0) {
                j3pAffiche(id.cells[0][cpt], null, '$(' + elem.fact4.cont.val + ')$')
                cpt++
              } else {
                j3pAffiche(id.cells[0][cpt], null, '$' + elem.fact4.cont.val + '$')
                cpt++
              }
            } else {
              objet.afficheOp(id.cells[0][cpt], elem.fact4.cont, b)
              cpt++
            }
            if (b !== true) {
              id.cells[0][cpt - 2].addEventListener('click', objet.MakeClic, false)
              id.cells[0][cpt - 2].select = false
              id.cells[0][cpt - 2].setAttribute('class', 'opautre')
              apush = er3
              if (er4 !== er3) {
                if (er3 !== '') {
                  if (er4 !== '') {
                    apush += '<br>' + er4
                  }
                } else {
                  apush = er4
                }
              }
              objet.listSel.push({ ki: id.cells[0][cpt - 2], rep: ok3, err: apush, elem, coince: 'o3' })
            }
          }
          break
        case 'puis':
          id = addDefaultTableDetailed(w, 1, 2)
          modif(id, 1, 2)
          if (elem.fact.type === 'val') {
            ok1 = true
            if (elem.fact.val < 0) {
              j3pAffiche(id.cells[0][0], null, ' $(' + elem.fact.val + ')$')
            } else {
              j3pAffiche(id.cells[0][0], null, ' $' + elem.fact.val + '$')
            }
          } else {
            ok1 = false
            objet.afficheOp(id.cells[0][0], elem.fact, b)
          }
          j3pAffiche(id.cells[0][1], null, ' $^{' + elem.expo.val + '}$')
          if (b !== true) {
            id.cells[0][1].addEventListener('click', objet.MakeClicP, false)
            id.cells[0][1].select = false
            id.cells[0][1].spe = true
            id.cells[0][1].setAttribute('class', 'opautrepuis')
            objet.listSel.push({ ki: id.cells[0][1], rep: ok1, err: 'Les parenthèses sont prioritaires !', elem, coince: 'o1' })
          }
          break
        case 'val':
          j3pAffiche(w, null, '$' + elem.val + '$')
          break
      }
    }
    objet.faitFoSel = function (ki) {
      if (ki.spe === true) {
        ki.setAttribute('class', 'opautrefop')
      } else {
        ki.setAttribute('class', 'opautrefo')
      }
    }
    objet.disableop = function () {
      let i
      for (i = 0; i < objet.listSel.length; i++) {
        objet.listSel[i].ki.classList.remove('opautre')
        objet.listSel[i].ki.classList.remove('opautrepuis')
        if (objet.listSel[i].ki.spe) {
          objet.listSel[i].ki.removeEventListener('click', objet.MakeClicP, false)
        } else {
          objet.listSel[i].ki.removeEventListener('click', objet.MakeClic, false)
        }
      }
    }
    objet.metVerOp = function () {
      let i
      for (i = 0; i < objet.listSel.length; i++) {
        if (objet.listSel[i].ki.spe) {
          if (objet.listSel[i].ki.select && objet.listSel[i].rep) {
            objet.listSel[i].ki.setAttribute('class', 'opautrevraip')
          }
          if (objet.listSel[i].ki.select && !objet.listSel[i].rep) objet.listSel[i].ki.setAttribute('class', 'opautrefop')
        } else {
          if (objet.listSel[i].ki.select && objet.listSel[i].rep) objet.listSel[i].ki.setAttribute('class', 'opautrevrai')
          if (objet.listSel[i].ki.select && !objet.listSel[i].rep) objet.listSel[i].ki.setAttribute('class', 'opautrefo')
        }
      }
    }
    objet.barrelesfo = function () {
      let i
      for (i = 0; i < objet.listSel.length; i++) {
        if (objet.listSel[i].fo) {
          j3pBarre(objet.listSel[i].ki)
        }
      }
    }
    objet.metBleusOpOublis = function () {
      let i
      for (i = 0; i < objet.listSel.length; i++) {
        if (objet.listSel[i].ki.spe) {
          if (!objet.listSel[i].ki.select && objet.listSel[i].rep) {
            objet.listSel[i].ki.setAttribute('class', 'opautrecop')
            objet.listSel[i].ki.select = true
          }
        } else {
          if (!objet.listSel[i].ki.select && objet.listSel[i].rep) {
            objet.listSel[i].ki.setAttribute('class', 'opautreco')
            objet.listSel[i].ki.select = true
          }
        }
      }
    }
    objet.TransExp = function () {
      initLet()
      let i
      for (i = objet.listSel.length - 1; i > -1; i--) {
        if (objet.listSel[i].rep && (objet.listSel[i].ki.select || stor.foco)) {
          transElem(objet.listSel[i].elem, objet.listSel[i].coince)
        }
      }
      objet.listSel = []
    }
    objet.yavirg = function (elem) {
      if (elem === undefined) {
        elem = objet.cont
      }

      switch (elem.type) {
        case 'som':
          return objet.yavirg(elem.terme1.cont) || objet.yavirg(elem.terme2.cont) || objet.yavirg(elem.terme3.cont) || objet.yavirg(elem.terme4.cont) || (elem.val !== Math.floor(elem.val))
        case 'prod':
          return objet.yavirg(elem.fact1.cont) || objet.yavirg(elem.fact2.cont) || objet.yavirg(elem.fact3.cont) || objet.yavirg(elem.fact4.cont) || (elem.val !== Math.floor(elem.val))
        case 'puis':
          return objet.yavirg(elem.fact) || (elem.val !== Math.floor(elem.val))
        case 'val':
          return elem.val !== Math.floor(elem.val)
        case 'empty':
          return false
        case 'let':
          return elem.val !== Math.floor(elem.val)
      }
    }
    objet.Cposit = function (elem) {
      if (elem === undefined) {
        elem = objet.cont
      }

      switch (elem.type) {
        case 'som':
          return objet.Cposit(elem.terme1.cont) && objet.Cposit(elem.terme2.cont) && (elem.val >= 0)
        case 'prod':
          return objet.Cposit(elem.fact1.cont) && objet.Cposit(elem.fact2.cont) && (elem.val >= 0)
        case 'puis':
          return objet.Cposit(elem.fact) && (elem.val >= 0)
        case 'val':
          return elem.val >= 0
        case 'empty':
          return false
        case 'let':
          return elem.val >= 0
      }
    }
    initLet()
  }

  function modif (id, x, nbc) {
    for (let i = 0; i < x; i++) {
      for (let j = 0; j < nbc; j++) id.cells[i][j].style.padding = 0
    }
  }
  function undivde (n) {
    let i, lim1, lim2
    const tty = []
    for (i = 1; i <= Math.sqrt(n); i++) {
      if (n % i === 0) tty.push(i)
    }
    lim1 = 1
    lim2 = tty.length - 1
    if (tty.length > 2) {
      lim1 = 1
      lim2 = tty.length - 2
    }
    if (tty.length === 1) return n

    return tty[j3pGetRandomInt(lim1, lim2)]
  }
  function yaReponse () {
    let i, ok
    if (stor.encours === 'remp' || stor.encours === 'cal') {
      for (i = 0; i < stor.lexpres.zones.length; i++) {
        if (stor.lexpres.zones[i].reponse() === '') {
          stor.lexpres.zones[i].focus()
          return false
        }
      }
      return true
    }
    if (stor.encours === 'op') {
      ok = false
      for (i = 0; i < stor.lexpres.listSel.length; i++) {
        ok = ok || stor.lexpres.listSel[i].ki.select
      }
      return ok
    }
  }
  function isRepOk () {
    let r1, ok
    stor.errValManque = false
    stor.errValManquelist = []
    stor.errFois = false
    stor.errPar = false
    stor.errEcrit = false

    stor.errSel = false
    stor.errSelList = []

    let i
    if (stor.encours === 'remp' || stor.encours === 'cal') {
      ok = true
      r1 = []
      for (i = 0; i < stor.lexpres.zones.length; i++) {
        r1.push(stor.lexpres.zones[i].reponse())
      }
      for (i = 0; i < r1.length; i++) {
        // verif bien écrit x
        if ((r1[i].indexOf('×') !== -1 && r1[i].indexOf('×') !== 0) || (r1[i].indexOf('×') !== r1[i].lastIndexOf('×'))) {
          stor.errEcrit = true
          ok = false
          stor.lexpres.zones[i].corrige(false)
        }
        // verif ya x si ya besoin
        if (stor.lexpres.needFois.indexOf(stor.lexpres.listlet[i].let) !== -1) {
          if (r1[i][0] !== '×') {
            stor.errFois = true
            ok = false
            stor.lexpres.zones[i].corrige(false)
          }
        }
        if (stor.lexpres.posFois.indexOf(stor.lexpres.listlet[i].let) === -1) {
          if (r1[i][0] === '×') {
            stor.errEcrit = true
            ok = false
            stor.lexpres.zones[i].corrige(false)
          }
        }
        r1[i] = r1[i].replace(/×/g, '')
        // verif bien écrit ()
        if ((r1[i].indexOf('(') !== -1 && r1[i].indexOf('(') !== 0) || (r1[i].indexOf('(') !== r1[i].lastIndexOf('('))) {
          stor.errEcrit = true
          ok = false
          stor.lexpres.zones[i].corrige(false)
        }
        if (r1[i].indexOf('(') === 0) {
          if (r1[i].indexOf(')') !== -1 && r1[i].indexOf(')') !== r1[i].length - 1) {
            stor.errEcrit = true
            ok = false
            stor.lexpres.zones[i].corrige(false)
          }
        }
        // verif si ya besoin parentheses
        if (stor.lexpres.needPar.indexOf(stor.lexpres.listlet[i].let) !== -1) {
          if (r1[i][0] !== '(') {
            stor.errPar = true
            ok = false
            stor.lexpres.zones[i].corrige(false)
          }
        }
        r1[i] = r1[i].replace(/\(/g, '')
        r1[i] = r1[i].replace(/\)/g, '')
        if ((r1[i].indexOf('-') !== -1 && r1[i].indexOf('-') !== 0) || (r1[i].indexOf('-') !== r1[i].lastIndexOf('-'))) {
          stor.errEcrit = true
          ok = false
          stor.lexpres.zones[i].corrige(false)
        }
        // verif ca contient val
        if (r1[i] !== String(stor.lexpres.listlet[i].val)) {
          stor.errValManque = true
          stor.errValManquelist.push('Tu n’as pas remplacé $' + stor.lexpres.listlet[i].let + '$ par la bonne valeur !')
          ok = false
          stor.lexpres.zones[i].corrige(false)
        }
      }
      return ok
    }
    if (stor.encours === 'op') {
      for (i = 0; i < stor.lexpres.listSel.length; i++) {
        if (stor.lexpres.listSel[i].ki.select && !stor.lexpres.listSel[i].rep) {
          stor.errSel = true
          stor.errSelList.push({ ki: i, err: stor.lexpres.listSel[i].err })
          stor.lexpres.faitFoSel(stor.lexpres.listSel[i].ki)
          stor.lexpres.listSel[i].fo = true
        } else {
          stor.lexpres.listSel[i].fo = false
        }
      }
      return (stor.errSel === false)
    }
  }
  function desactiveAll () {
    let i
    if (stor.encours === 'remp' || stor.encours === 'cal') {
      for (i = 0; i < stor.lexpres.zones.length; i++) {
        stor.lexpres.zones[i].disable()
      }
    }
    if (stor.encours === 'op') {
      stor.lexpres.disableop()
    }
  }
  function barrelesfo () {
    if (stor.encours === 'remp' || stor.encours === 'cal') {
      for (const zone of stor.lexpres.zones) zone.barreIfKo()
    }
    if (stor.encours === 'op') {
      stor.lexpres.barrelesfo()
    }
  }
  function mettouvert () {
    let i
    if (stor.encours === 'remp' || stor.encours === 'cal') {
      for (i = 0; i < stor.lexpres.zones.length; i++) {
        stor.lexpres.zones[i].corrige(true)
      }
    }
    if (stor.encours === 'op') {
      stor.lexpres.metVerOp()
    }
  }
  function affCorrection (bool) {
    j3pEmpty(stor.lesdiv.explikjus)
    let i, buf
    if (stor.errValManque) {
      stor.yaexplik = true
      if (stor.encours === 'remp') {
        for (i = 0; i < stor.errValManquelist.length; i++) {
          if (stor.errValManquelist[i] !== '') j3pAffiche(stor.lesdiv.explikjus, null, stor.errValManquelist[i] + '\n')
        }
      } else {
        j3pAffiche(stor.lesdiv.explikjus, null, 'Erreur de calcul !\n')
      }
    }
    if (stor.errFois) {
      stor.yaexplik = true
      j3pAffiche(stor.lesdiv.explikjus, null, 'Tu oublies le signe $×$ !\n')
    }
    if (stor.errPar) {
      stor.yaexplik = true
      j3pAffiche(stor.lesdiv.explikjus, null, 'Tu oublies les parenthèses !\n')
    }
    if (stor.errEcrit) {
      stor.yaexplik = true
      j3pAffiche(stor.lesdiv.explikjus, null, 'Erreur d’écriture !\n')
    }

    if (stor.errSel) {
      buf = ''
      for (i = 0; i < stor.errSelList.length; i++) {
        if (stor.errSelList[i].err !== '') buf += stor.errSelList[i].err + '<br>'
      }
      if (buf !== '') {
        stor.yaexplik = true
        j3pAffiche(stor.lesdiv.explikjus, null, buf)
      }
    }

    if (bool) {
      barrelesfo()
      desactiveAll()
      stor.yaco = true
      if (stor.encours === 'remp') {
        stor.dev = true
        const yuo = addDefaultTable(stor.lesdiv.solutionjus, 1, 2)
        j3pAffiche(yuo[0][0], null, '$A = $')
        stor.lexpres.afficheRemp(yuo[0][1], true)
      }
      if (stor.encours === 'op') {
        stor.lexpres.metVerOp()
        stor.lexpres.metBleusOpOublis()
        stor.foco = true
      }
      if (stor.encours === 'cal') {
        stor.foco = true
        const yuo = addDefaultTable(stor.lesdiv.solutionjus, 1, 2)
        j3pAffiche(yuo[0][0], null, '$A = $')
        stor.lexpres.afficheRemp(yuo[0][1], true)
      }
    }
    faiscoolCo()
  }
  function faiscoolCo () {
    if (ds.theme === 'zonesAvecImageDeFond') {
      if (stor.yaexplik) {
        stor.lesdiv.explikjus.classList.add('explique')
      }
      if (stor.yaco) {
        if (stor.encours !== 'op') stor.lesdiv.solutionjus.classList.add('correction')
      }
    }
  }

  function enonceMain () {
    me.videLesZones()
    creeLesDiv()
    stor.Lexo = faisChoixExos()

    poseQuestion()
    stor.lesdiv.travail.classList.add('travail')
    stor.lesdiv.consigne.classList.add('enonce')
    stor.lesdiv.enoncejus.classList.add('enonce')
    stor.lesdiv.travailjus.classList.add('travail')
    me.zonesElts.MG.classList.add('fond')

    j3pEmpty(stor.lesdiv.calculatrice)
    if (ds.Calculatrice) {
      me.fenetresjq = [{ name: 'Calculatrice', title: 'Une calculatrice', left: 600, top: 10 }]
      j3pCreeFenetres(me)
      j3pAfficheCroixFenetres('Calculatrice')
      j3pAjouteBouton(stor.lesdiv.calculatrice, 'Calculatrice', 'MepBoutons', 'Calculatrice', boutonfen)
    }

    me.finEnonce()
  }

  switch (this.etat) {
    case 'enonce':
      // code exécuté au lancement de la section
      if (me.debutDeLaSection) {
        initSection()
      } else {
        enonceMain()
      }
      break // case "enonce":

    case 'correction':
      // On teste si une réponse a été saisie
      stor.yaexplik = false
      stor.yaco = false
      stor.lesdiv.explikjus.classList.remove('explique')
      stor.lesdiv.solutionjus.classList.remove('correction')

      if (!yaReponse()) {
        // pas de réponse
        stor.lesdiv.correction.style.color = this.styles.cfaux
        stor.lesdiv.correction.innerHTML = reponseIncomplete
        if (stor.encours === 'op') stor.lesdiv.correction.innerHTML = 'Réponse incomplète ! <br> <i>Clique sur une opération <br> pour la sélectionner.</i>'
        return this.finCorrection('navigation', true)
      }

      // Bonne réponse
      if (isRepOk()) {
        stor.lesdiv.correction.style.color = this.styles.cbien
        stor.lesdiv.correction.innerHTML = cBien
        mettouvert()
        desactiveAll()
        j3pEmpty(stor.lesdiv.explikjus)
        j3pEmpty(stor.lesdiv.explikenc)

        if (stor.lexpres.cont.type === 'val' || stor.lexpres.cont.type === 'let') {
          me.score++
          return this.finCorrection('navigation', true)
        }

        // le "faux" bouton suite pour passer à l'étape 2 (faut cacher le vrai)
        me.cacheBoutonValider()
        stor.fakeBtnSuite = j3pAjouteBouton('BoutonsJ3P', etape2, { className: 'big suite', value: 'Suite' })
        return
      }

      // Pas de bonne réponse
      stor.npark++
      stor.lesdiv.correction.style.color = this.styles.cfaux
      stor.lesdiv.correction.innerHTML = cFaux
      if (this.isElapsed) {
        // A cause de la limite de temps :
        stor.lesdiv.correction.innerHTML += tempsDepasse + '<BR>'
        this.typederreurs[10]++
        affCorrection(true)
        return this.finCorrection('navigation', true)
      }

      // Réponse fausse :
      if (this.essaiCourant < ds.nbchances) {
        // il reste des essais
        stor.lesdiv.correction.innerHTML += '<br>' + essaieEncore
        affCorrection(false)
        this.typederreurs[1]++
        return this.finCorrection()
      }

      stor.tentative++
      if (stor.tentative <= stor.gardechance) {
        affCorrection(false)
        return this.finCorrection()
      }

      // Erreur au dernier essai
      affCorrection(true)
      if (!ds.Sans_echec) {
        if (stor.lexpres.cont.type === 'val' || stor.lexpres.cont.type === 'let') {
          return this.finCorrection('navigation', true)
        }
        // le "faux" bouton suite pour passer à l'étape 2 (faut cacher le vrai)
        me.cacheBoutonValider()
        stor.fakeBtnSuite = j3pAjouteBouton('BoutonsJ3P', etape2, { className: 'big suite', value: 'Suite' })
        return
      }

      this.typederreurs[2]++
      return this.finCorrection('navigation', true)

    case 'navigation':
      if (!this.sectionTerminee()) {
        this.etat = 'enonce'
      }
      this.finNavigation()
      break // case "navigation":
  }
}
