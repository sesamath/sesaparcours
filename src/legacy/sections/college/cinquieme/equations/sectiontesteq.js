import { j3pAddElt, j3pAjouteBouton, j3pArrondi, j3pClone, j3pDetruit, j3pEmpty, j3pGetRandomBool, j3pGetRandomInt, j3pModale, j3pPGCD, j3pShowError, j3pShuffle } from 'src/legacy/core/functions'
import ListeDeroulante from 'src/legacy/outils/listeDeroulante/ListeDeroulante'
import { j3pAffiche } from 'src/lib/mathquill/functions'
import { addDefaultTable } from 'src/legacy/themes/table'
import ZoneStyleMathquill2 from 'src/legacy/outils/zoneStyleMathquill/ZoneStyleMathquill2'
import ZoneStyleMathquill3 from 'src/legacy/outils/zoneStyleMathquill/ZoneStyleMathquill3'
import { j3pCreeFenetres, j3pAfficheCroixFenetres, j3pToggleFenetres } from 'src/legacy/core/functionsJqDialog'
import BrouillonCalculs from 'src/legacy/outils/brouillon/BrouillonCalculs'
import textesGeneriques from 'src/lib/core/textes'

const { cBien, cFaux, essaieEncore, reponseIncomplete, tempsDepasse } = textesGeneriques

/**
 * Ajouter une description ici
 * @fileOverview
 * @author Tommy Barroy <t.barroy@laposte.net>
 * @since 2017-10
 */
export const params = {
  outils: ['mathquill', 'calculatrice'],
  /**
   * Ce qui est paramétrable dans l’éditeur de graphe
   * @type {LegacySectionParamsParametres}
   */
  parametres: [
    ['nbrepetitions', 3, 'entier', 'Nombre de répétitions de la section'],
    ['nbchances', 1, 'entier', 'Nombre de répétitions de la section'],
    ['entier', true, 'boolean', '<u>true</u>:  Le nombre à tester peut être entier'],
    ['negatif', true, 'boolean', '<u>true</u>:  Le nombre à tester peut être négatif'],
    ['decimal', true, 'boolean', '<u>true</u>:  Le nombre à tester peut être décimal'],
    ['fraction', true, 'boolean', '<u>true</u>:  Le nombre à tester peut être une fraction'],
    ['Degre', 'un', 'liste', '<u>un</u>: Equation du premier degré', ['un', 'deux', 'un ou deux']],
    ['Nb_inconnu', 'un', 'liste', '<u>un</u>: Equation à 1 inconnue', ['un', 'deux', 'un ou deux']],
    ['Calculatrice', true, 'boolean', '<u>true</u>:  Une calculatrice est disponible'],
    // le modèle gère la compatibilité ascendante avec l’ancien paramètre MiseEnPagePlusClaire
    ['theme', 'standard', 'liste', 'Modèle pour l’affichage', ['standard', 'zonesAvecImageDeFond']]
  ]
}

/**
 * section testeq
 * @this {Parcours}
 */
export default function main () {
  const me = this
  const ds = me.donneesSection
  const stor = me.storage

  function modif (tab) {
    for (let i = 0; i < tab.length; i++) {
      for (let j = 0; j < tab[i].length; j++) {
        tab[i][j].style.padding = '0px'
      }
    }
  }
  function creeLesDiv () {
    stor.lesdiv = {}
    stor.lesdiv.conteneur = j3pAddElt(me.zonesElts.MG, 'div', {
      style: me.styles.etendre('toutpetit.enonce', { padding: '10px' })
    })
    const tt1 = addDefaultTable(stor.lesdiv.conteneur, 1, 3)
    modif(tt1)
    stor.lesdiv.consigneG = tt1[0][0]
    tt1[0][1].style.width = '20px'
    const ttbis = addDefaultTable(tt1[0][2], 2, 1)
    modif(ttbis)
    tt1[0][2].style.verticalAlign = 'top'
    stor.lesdiv.calculatrice = ttbis[0][0]

    stor.lesdiv.action = addDefaultTable(stor.lesdiv.conteneur, 1, 1)[0][0]
    stor.lesdiv.correction = ttbis[1][0]

    stor.lesdiv.calculs = addDefaultTable(stor.lesdiv.conteneur, 1, 3)[0][0]
    stor.tabCal = addDefaultTable(stor.lesdiv.calculs, 1, 5)
    stor.lesdiv.calGauche = stor.tabCal[0][0]
    stor.lesdiv.calDroite = stor.tabCal[0][4]
    stor.tabCal[0][1].style.width = '10px'
    stor.tabCal[0][3].style.width = '10px'
    stor.lesdiv.labarre = stor.tabCal[0][2]
    stor.lesdiv.conclusion = addDefaultTable(stor.lesdiv.conteneur, 1, 3)[0][0]
    stor.lesdiv.explications = j3pAddElt(stor.lesdiv.conteneur, 'div')
    stor.lesdiv.solution = j3pAddElt(stor.lesdiv.conteneur, 'div')
    stor.lesdiv.explications.style.color = me.styles.cfaux
    stor.lesdiv.solution.style.color = me.styles.petit.correction.color
  }
  function faisChoixExos () {
    const e = ds.lesExos.pop()
    let t
    let u
    let u2, pg
    let popos = [1, 2, 3, 4, 5, 6, 7, 8, 9, 10, 11, 12, 13]
    popos = j3pShuffle(popos)
    switch (e.val) {
      case 'entier' :
        e.num = popos.pop()
        e.den = 'z'
        e.num2 = popos.pop()
        e.den2 = 'z'
        break
      case 'decimal' :
        popos = [1, 2, 3, 4, 5, 6, 7, 8, 9, 11, 12, 13]
        popos = j3pShuffle(popos)
        e.num = j3pArrondi(popos.pop() / 10, 1)
        e.den = 'z'
        e.num2 = j3pArrondi(popos.pop() / 10, 1)
        e.den2 = 'z'
        break
      case 'fraction':
        t = unefractionnondecimale()
        e.num = t.num
        e.den = t.den
        t = unefractionnondecimale()
        e.num2 = t.num
        e.den2 = t.den
        break
    }
    if (e.s === '-') {
      e.s1 = '-'
      e.num = -e.num
      if (j3pGetRandomBool()) {
        e.s2 = '-'
        e.num2 = -e.num2
      } else { e.s2 = '+' }
    }
    e.eq = { a1: j3pGetRandomIntp0(-10, 10), b1: j3pGetRandomIntp0(-10, 10), c1: j3pGetRandomInt(-10, 10), a2: j3pGetRandomIntp0(-10, 10), b2: j3pGetRandomIntp0(-10, 10), a3: j3pGetRandomIntp0(-10, 10), b3: j3pGetRandomIntp0(-10, 10), c3: j3pGetRandomInt(-10, 10), a4: j3pGetRandomIntp0(-10, 10), b4: j3pGetRandomIntp0(-10, 10) }
    const base = 'abcdxyzt'
    if (e.nbinc === 'un') {
      e.eq.a2 = e.eq.b2 = e.eq.a4 = e.eq.b4 = 0
      const i = j3pGetRandomInt(0, base.length - 1)
      e.nx = base[i]
      e.ny = 'p'
    } else {
      e.nx = 'x'
      e.ny = 'y'
    }
    if (e.deg === 'un') {
      e.eq.a1 = e.eq.a2 = e.eq.a3 = e.eq.a4 = 0
    }
    if (e.deg === 'deux' && e.nbinc === 'deux') {
      if (j3pGetRandomBool()) {
        e.eq.a1 = 0
        e.eq.b2 = 0
        e.eq.a4 = 0
        e.eq.b3 = 0
      } else {
        e.eq.a2 = 0
        e.eq.b1 = 0
        e.eq.a3 = 0
        e.eq.b4 = 0
      }
    }
    if ((e.eq.a1 === 0) && (e.eq.b1 === 0) && (e.eq.a2 === 0) && (e.eq.b2 === 0)) {
      e.eq.b1 = 1
    }
    e.eq.c3 = {}
    if (e.sam) {
      if (e.den === 'z') { u = 1 } else { u = e.den }
      if (e.den2 === 'z') { u2 = 1 } else { u2 = e.den2 }
      if (e.eq.b1 === e.eq.b3) e.eq.b3++
      t = e.eq.a1 * e.num * e.num * u2 * u2 + e.eq.b1 * e.num * u * u2 * u2 + e.eq.c1 * u * u * u2 * u2 + e.eq.a2 * e.num2 * e.num2 * u * u + e.eq.b2 * e.num2 * u2 * u * u
      t -= e.eq.a3 * e.num * e.num * u2 * u2 + e.eq.b3 * e.num * u * u2 * u2
      t -= e.eq.a4 * e.num2 * e.num2 * u * u + e.eq.b4 * e.num2 * u * u * u2
      switch (e.val) {
        case 'entier' :
          e.eq.c3.num = Math.round(t / (u * u * u2 * u2))
          e.eq.c3.den = 1
          break
        case 'decimal' :
          e.eq.c3.num = j3pArrondi(t / (u * u * u2 * u2), 2)
          e.eq.c3.den = 1
          break
        case 'fraction':
          pg = j3pPGCD(t, u * u * u2 * u2, { negativesAllowed: true, valueIfZero: 1 })
          e.eq.c3.num = Math.round(t / pg)
          e.eq.c3.den = Math.round(u * u * u2 * u2 / pg)
          break
      }
    } else {
      switch (e.val) {
        case 'entier' :
          e.eq.c3.num = j3pGetRandomIntp0(-10, 10)
          e.eq.c3.den = 1
          break
        case 'decimal' :
          e.eq.c3.num = j3pArrondi(j3pGetRandomIntp0(-999, 999), 1)
          e.eq.c3.den = 1
          break
        case 'fraction':
          popos = [1, 2, 3, 4, 5, 6, 7, 8, 9, 10, 11, 12, 13]
          popos = j3pShuffle(popos)
          e.eq.c3.num = popos.pop()
          e.eq.c3.den = popos.pop()
          break
      }
    }
    return e
  }
  function j3pGetRandomIntp0 (a, b) {
    let aret
    do {
      aret = j3pGetRandomInt(a, b)
    } while (aret === 0)
    return aret
  }

  function initSection () {
    let i
    // On surcharge avec les parametres passés par le graphe
    me.validOnEnter = false // ex donneesSection.touche_entree

    // Construction de la page
    me.construitStructurePage('presentation3')

    stor.yakR = false
    ds.lesExos = []
    let lp = []
    if (ds.entier) lp.push('entier')
    if (ds.decimal) lp.push('decimal')
    if (ds.fraction) lp.push('fraction')
    if (lp.length === 0) {
      j3pShuffle('Erreur de paramétrage: il faut accepter entier, décimal ou fraction !')
      lp = ['entier']
    }
    lp = j3pShuffle(lp)
    stor.lp = []
    for (i = 0; i < ds.nbitems; i++) {
      stor.lp[i] = { val: lp[i % lp.length] }
    }
    stor.lp = j3pShuffle(stor.lp)

    lp = ['+']
    if (ds.negatif) { lp.push('-') }
    lp = j3pShuffle(lp)
    for (i = 0; i < stor.lp.length; i++) {
      stor.lp[i].s = lp[i % lp.length]
    }
    stor.lp = j3pShuffle(stor.lp)

    lp = []
    if ((ds.Nb_inconnu === 'un') || (ds.Nb_inconnu === 'un ou deux')) lp.push('un')
    if ((ds.Nb_inconnu === 'deux') || (ds.Nb_inconnu === 'un ou deux')) lp.push('deux')
    lp = j3pShuffle(lp)
    for (i = 0; i < stor.lp.length; i++) {
      stor.lp[i].nbinc = lp[i % lp.length]
    }
    stor.lp = j3pShuffle(stor.lp)

    lp = []
    if ((ds.Degre === 'un') || (ds.Degre === 'un ou deux')) lp.push('un')
    if ((ds.Degre === 'deux') || (ds.Degre === 'un ou deux')) lp.push('deux')
    lp = j3pShuffle(lp)
    for (i = 0; i < stor.lp.length; i++) {
      stor.lp[i].deg = lp[i % lp.length]
    }
    stor.lp = j3pShuffle(stor.lp)

    const u = j3pGetRandomInt(0, 1)
    for (i = 0; i < stor.lp.length; i++) {
      stor.lp[i].sam = ((i + u) % 2 === 0)
    }
    stor.lp = j3pShuffle(stor.lp)

    ds.lesExos = j3pClone(stor.lp)

    stor.larestric = '0123456789()+-/.,'
    stor.clvspe = false

    me.typederreurs = [0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0]
    me.afficheTitre('Tester une valeur')
    enonceMain()
  }
  function AffCorrection (bool) {
    let i
    switch (stor.lerr) {
      case 'ConclusionHative':
        stor.listeaction.corrige(false)
        j3pAffiche(stor.lesdiv.explications, null, 'Pour tester une solution, il faut d’abord \ncalculer chaque membre en utilisant cette valeur !')
        break
      case 'CalculEquation':
        stor.listeaction.corrige(false)
        j3pAffiche(stor.lesdiv.explications, null, 'On peut pas "<i>Calculer une équation</i>" !')
        break
      case 'RemplecementFoireu':
        stor.listeaction.corrige(false)
        if (stor.Lexo.nbinc === 'un') {
          j3pAffiche(stor.lesdiv.explications, null, 'Il n’y a pas ' + 'de "$' + stor.Lexo.ny + '$" dans cette équation !')
        } else {
          j3pAffiche(stor.lesdiv.explications, null, 'Tu inverses les valeurs de "$' + stor.Lexo.nx + '$" et "$' + stor.Lexo.ny + '$" !')
        }
        break
      case '3emeMembre':
        stor.listeaction.corrige(false)
        j3pAffiche(stor.lesdiv.explications, null, 'Il n’a que deux membres (à gauche et à droite du symbole <b> = </b>) !')
        break
      case 'oubliFois':
        j3pAffiche(stor.lesdiv.explications, null, 'Tu oublies le signe $\\times$ !')
        break
      case 'nbmalecrit':
        j3pAffiche(stor.lesdiv.explications, null, 'Des nombres sont mals écrits !')
        break
      case 'pbparent':
        j3pAffiche(stor.lesdiv.explications, null, 'Il manque des parenthèses !')
        break
      case 'malcopie':
        j3pAffiche(stor.lesdiv.explications, null, 'Il faut remplacer les lettres par leur valeurs, \nidentiques aux données !')
        break
      case 'egal1':
        j3pAffiche(stor.lesdiv.explications, null, 'Erreur de calcul !')
        break
      case 'oubliFois2':
        j3pAffiche(stor.lesdiv.explications, null, 'Tu oublies le signe $\\times$ !')
        break
      case 'nbmalecrit2':
        j3pAffiche(stor.lesdiv.explications, null, 'Des nombres sont mals écrits !')
        break
      case 'pbparent2':
        j3pAffiche(stor.lesdiv.explications, null, 'Il manque des parenthèses !')
        break
      case 'malcopie2':
        j3pAffiche(stor.lesdiv.explications, null, 'Il faut remplacer les lettres par leur valeurs, \nidentiques aux données !')
        break
      case 'egal2':
        j3pAffiche(stor.lesdiv.explications, null, 'Erreur de calcul !')
        break
      case 'fauusegal':
        stor.listeconcle.corrige(false)
        j3pAffiche(stor.lesdiv.explications, null, 'Observe bien les deux résultats !')
        break
      case 'fauusssol':
        stor.listeconcle2.corrige(false)
        j3pAffiche(stor.lesdiv.explications, null, 'La conclusion est fausse !')
        break
    }
    if (bool) {
      stor.lesdiv.explications.classList.add('explique')
      switch (stor.lerr) {
        case 'ConclusionHative':
        case 'CalculEquation':
        case 'RemplecementFoireu':
        case '3emeMembre':
          stor.listeaction.barre(false)
          stor.listeaction.disable()
          j3pAffiche(stor.lesdiv.solution, null, '<b>Avant de conclure , il fallait choisir: </b>\n' + stor.ch3 + '\n<b>et</b>\n' + stor.ch5)
          j3pDetruit('BOUBOUT')
          break
        case 'oubliFois':
        case 'nbmalecrit':
        case 'malcopie':
        case 'pbparent':
        case 'egal1': {
          stor.rep1.disable()
          stor.rep1.barreIfKo()
          for (i = 0; i < stor.listeZoneEtape1l1x.length; i++) {
            stor.listeZoneEtape1l1x[i].zone.disable()
            stor.listeZoneEtape1l1x[i].zone.barreIfKo()
          }
          for (i = 0; i < stor.listeZoneEtape1l1y.length; i++) {
            stor.listeZoneEtape1l1y[i].zone.disable()
            stor.listeZoneEtape1l1y[i].zone.barreIfKo()
          }
          stor.brouillon1.disable()
          j3pDetruit('BOUBOUT')
          let maph = '$' + stor.affMembre1
          let sph = ''
          sph = aj2(sph, stor.Lexo.eq.a1, stor.affnb1, '²')
          sph = aj2(sph, stor.Lexo.eq.a2, stor.affnb2, '²')
          sph = aj2(sph, stor.Lexo.eq.b1, stor.affnb1, '')
          sph = aj2(sph, stor.Lexo.eq.b2, stor.affnb2, '')
          sph = aj2(sph, stor.Lexo.eq.c1, '', '')
          if (sph[1] === '+') sph = sph.substring(2)
          maph += ' = ' + sph + '$ <br>'
          let na1 = 0
          let na2 = 0
          let affna1 = Affichemoibien2carre({ num: stor.Lexo.num, den: stor.Lexo.den })
          let affna2 = Affichemoibien2carre({ num: stor.Lexo.num2, den: stor.Lexo.den2 })
          if (stor.Lexo.eq.a1 !== 0 || stor.Lexo.eq.a2 !== 0) {
            sph = ''
            sph = aj2(sph, stor.Lexo.eq.a1, affna1, '')
            sph = aj2(sph, stor.Lexo.eq.a2, affna2, '')
            sph = aj2(sph, stor.Lexo.eq.b1, stor.affnb1, '')
            sph = aj2(sph, stor.Lexo.eq.b2, stor.affnb2, '')
            sph = aj2(sph, stor.Lexo.eq.c1, '', '')
            if (sph[1] === '+') sph = sph.substring(2)
            maph += '$' + stor.affMembre1 + ' = ' + sph + '$ <br>'
          }
          let nb1 = 0
          let nb2 = 0
          let affnb1 = ''
          let affnb2 = ''
          if (stor.Lexo.eq.b1 !== 0 || stor.Lexo.eq.b2 !== 0) {
            nb1 = calculfois(stor.Lexo.eq.b1, stor.Lexo.num, stor.Lexo.den)
            nb2 = calculfois(stor.Lexo.eq.b2, stor.Lexo.num2, stor.Lexo.den2)
            affnb1 = Affichemoibien2(nb1)
            affnb2 = Affichemoibien2(nb2)
            let pass = stor.Lexo.den
            if (pass !== 'z') pass = stor.Lexo.den * stor.Lexo.den
            let pass2 = stor.Lexo.den2
            if (pass2 !== 'z') pass2 = stor.Lexo.den2 * stor.Lexo.den2
            na1 = calculfois(stor.Lexo.eq.a1, j3pArrondi(stor.Lexo.num * stor.Lexo.num, 4), pass)
            na2 = calculfois(stor.Lexo.eq.a2, j3pArrondi(stor.Lexo.num2 * stor.Lexo.num2, 4), pass2)
            affna1 = Affichemoibien2(na1)
            affna2 = Affichemoibien2(na2)
            if (nb1.num === 0) {
              nb1 = 0
            } else {
              nb1 = 1
            }
            if (nb2.num === 0) {
              nb2 = 0
            } else {
              nb2 = 1
            }
            if (na1.num === 0) {
              na1 = 0
            } else {
              na1 = 1
            }
            if (na2.num === 0) {
              na2 = 0
            } else {
              na2 = 1
            }
            maph += '$' + stor.affMembre1
            sph = ''
            sph = aj2(sph, na1, affna1, '')
            sph = aj2(sph, na2, affna2, '')
            sph = aj2(sph, nb1, affnb1, '')
            sph = aj2(sph, nb2, affnb2, '')
            sph = aj2(sph, stor.Lexo.eq.c1, '', '')
            if (sph[1] === '+') sph = sph.substring(2)
            maph += ' = ' + sph + '$ <br>'
          }
          maph += '$' + stor.affMembre1
          sph = Affichemoibien(stor.resul1)
          maph += ' = ' + sph + '$'
          maph = maph.replace('+-', ' -').replace('+-', ' -').replace('+-', ' -')
          j3pAffiche(stor.lesdiv.solution, null, maph)
          break
        }
        case 'oubliFois2':
        case 'nbmalecrit2':
        case 'malcopie2':
        case 'pbparent2':
        case 'egal2': {
          stor.rep2.disable()
          stor.rep2.barreIfKo()
          for (i = 0; i < stor.listeZoneEtape1l1x2.length; i++) {
            stor.listeZoneEtape1l1x2[i].zone.disable()
            stor.listeZoneEtape1l1x2[i].zone.barreIfKo()
          }
          for (i = 0; i < stor.listeZoneEtape1l1y2.length; i++) {
            stor.listeZoneEtape1l1y2[i].zone.disable()
            stor.listeZoneEtape1l1y2[i].zone.barreIfKo()
          }
          stor.brouillon2.disable()
          j3pDetruit('BOUBOUT')

          let maph = '$' + stor.affMembre2
          let sph = ''
          sph = aj2(sph, stor.Lexo.eq.a3, stor.affnb1, '²')
          sph = aj2(sph, stor.Lexo.eq.a4, stor.affnb2, '²')
          sph = aj2(sph, stor.Lexo.eq.b3, stor.affnb1, '')
          sph = aj2(sph, stor.Lexo.eq.b4, stor.affnb2, '')
          sph = aj2(sph, 1, stor.afc3, '')
          if (sph[1] === '+') sph = sph.substring(2)
          maph += ' = ' + sph + '$ <br>'
          let na3 = 0
          let na4 = 0
          let affna3 = Affichemoibien2carre({ num: stor.Lexo.num, den: stor.Lexo.den })
          let affna4 = Affichemoibien2carre({ num: stor.Lexo.num2, den: stor.Lexo.den2 })
          if (stor.Lexo.eq.a3 !== 0 || stor.Lexo.eq.a4 !== 0) {
            sph = ''
            sph = aj2(sph, stor.Lexo.eq.a3, affna3, '')
            sph = aj2(sph, stor.Lexo.eq.a4, affna4, '')
            sph = aj2(sph, stor.Lexo.eq.b3, stor.affnb1, '')
            sph = aj2(sph, stor.Lexo.eq.b4, stor.affnb2, '')
            sph = aj2(sph, 1, stor.afc3, '')
            if (sph[1] === '+') sph = sph.substring(2)
            maph += '$' + stor.affMembre2 + ' = ' + sph + '$ <br>'
          }
          let nb3 = 0
          let nb4 = 0
          let affnb3 = ''
          let affnb4 = ''
          if (stor.Lexo.eq.b3 !== 0 || stor.Lexo.eq.b4 !== 0) {
            nb3 = calculfois(stor.Lexo.eq.b3, stor.Lexo.num, stor.Lexo.den)
            nb4 = calculfois(stor.Lexo.eq.b4, stor.Lexo.num2, stor.Lexo.den2)
            affnb3 = Affichemoibien2(nb3)
            affnb4 = Affichemoibien2(nb4)
            let pass = stor.Lexo.den
            if (pass !== 'z') pass = stor.Lexo.den * stor.Lexo.den
            let pass2 = stor.Lexo.den2
            if (pass2 !== 'z') pass2 = stor.Lexo.den2 * stor.Lexo.den2
            na3 = calculfois(stor.Lexo.eq.a3, j3pArrondi(stor.Lexo.num * stor.Lexo.num, 4), pass)
            na4 = calculfois(stor.Lexo.eq.a4, j3pArrondi(stor.Lexo.num2 * stor.Lexo.num2, 4), pass2)
            affna3 = Affichemoibien2(na3)
            affna4 = Affichemoibien2(na4)
            if (nb3.num === 0) {
              nb3 = 0
            } else {
              nb3 = 1
            }
            if (nb4.num === 0) {
              nb4 = 0
            } else {
              nb4 = 1
            }
            if (na3.num === 0) {
              na3 = 0
            } else {
              na3 = 1
            }
            if (na4.num === 0) {
              na4 = 0
            } else {
              na4 = 1
            }
            maph += '$' + stor.affMembre2
            sph = ''
            sph = aj2(sph, na3, affna3, '')
            sph = aj2(sph, na4, affna4, '')
            sph = aj2(sph, nb3, affnb3, '')
            sph = aj2(sph, nb4, affnb4, '')
            sph = aj2(sph, 1, stor.afc3, '')
            if (sph[1] === '+') sph = sph.substring(2)
            maph += ' = ' + sph + '$ <br>'
          }
          maph += '$' + stor.affMembre2
          sph = Affichemoibien(stor.resul2)
          maph += ' = ' + sph + '$'
          maph = maph.replace('+-', ' -').replace('+-', ' -').replace('+-', ' -')
          j3pAffiche(stor.lesdiv.solution, null, maph)
          break
        }
        case 'fauusssol':
        case 'fauusegal':
          stor.listeconcle.disable()
          stor.listeconcle.barreIfKo()
          stor.listeconcle2.disable()
          stor.listeconcle2.barreIfKo()
          j3pDetruit('BOUBOUT')
          if (egalite2(stor.resul1, stor.resul2)) {
            j3pAffiche(stor.lesdiv.solution, null, 'Les deux membres sont égaux, on a donc testé une solution.')
          } else {
            j3pAffiche(stor.lesdiv.solution, null, 'Les deux membres ne sont pas égaux, \nil ne s’agit donc pas d’une solution.')
          }
          break
      }
    } else {
      me.cacheBoutonSuite()
      if (stor.encours === 'e1CO') stor.encours = 'e1'
      if (stor.encours === 'e2CO') stor.encours = 'e2'
      if (stor.encours === 'COconcl') stor.encours = 'concl'
    }
  }
  function poseQuestion () {
    let bv
    stor.encours = ''
    stor.etape1 = stor.etape2 = false
    stor.affnb1 = Affichemoibien2({ num: stor.Lexo.num, den: stor.Lexo.den })
    stor.affnb2 = Affichemoibien2({ num: stor.Lexo.num2, den: stor.Lexo.den2 })
    stor.afc3 = Affichemoibien2(stor.Lexo.eq.c3)
    stor.affMembre1 = ''
    stor.affMembre1 = aj(stor.affMembre1, stor.Lexo.eq.a1, stor.Lexo.nx, '²')
    stor.affMembre1 = aj(stor.affMembre1, stor.Lexo.eq.a2, stor.Lexo.ny, '²')
    stor.affMembre1 = aj(stor.affMembre1, stor.Lexo.eq.b1, stor.Lexo.nx, '')
    stor.affMembre1 = aj(stor.affMembre1, stor.Lexo.eq.b2, stor.Lexo.ny, '')
    stor.affMembre1 = aj(stor.affMembre1, stor.Lexo.eq.c1, '', '')
    if (stor.affMembre1[1] === '+') stor.affMembre1 = stor.affMembre1.substring(2)
    stor.affMembre2 = ''
    stor.affMembre2 = aj(stor.affMembre2, stor.Lexo.eq.a3, stor.Lexo.nx, '²')
    stor.affMembre2 = aj(stor.affMembre2, stor.Lexo.eq.a4, stor.Lexo.ny, '²')
    stor.affMembre2 = aj(stor.affMembre2, stor.Lexo.eq.b3, stor.Lexo.nx, '')
    stor.affMembre2 = aj(stor.affMembre2, stor.Lexo.eq.b4, stor.Lexo.ny, '')
    stor.affMembre2 = aj(stor.affMembre2, 1, stor.afc3, '').replace('+-', '-')
    if (stor.affMembre2[1] === '+') stor.affMembre2 = stor.affMembre2.substring(2)
    if (stor.Lexo.nbinc === 'un') {
      bv = 'la valeur $' + stor.affnb1 + '$ '
    } else {
      bv = 'le couple $(\\text{ }' + stor.affnb1 + '\\text{ } ;\\text{ } ' + stor.affnb2 + '\\text{ }) $'
    }
    j3pAffiche(stor.lesdiv.consigneG, null, 'Tu dois tester si ' + bv + ' \nest une solution de l’équation  $' + stor.affMembre1 + ' = ' + stor.affMembre2 + '$')
    remetchoix()
  }
  function yaReponse (osef) {
    let yaya = true
    let i
    if (stor.encours === 'e1') {
      for (i = 0; i < stor.listeZoneEtape1l1x.length; i++) {
        if (stor.listeZoneEtape1l1x[i].zone.reponse() === '') {
          yaya = false
          stor.listeZoneEtape1l1x[i].zone.focus()
          break
        }
      }
      if (yaya) {
        for (i = 0; i < stor.listeZoneEtape1l1y.length; i++) {
          if (stor.listeZoneEtape1l1y[i].zone.reponse() === '') {
            yaya = false
            stor.listeZoneEtape1l1y[i].zone.focus()
            break
          }
        }
      }
      if (yaya) {
        if (stor.rep1.reponse() === '') {
          yaya = false
          stor.rep1.focus()
        }
      }
    }
    if (stor.encours === 'e2') {
      for (i = 0; i < stor.listeZoneEtape1l1x2.length; i++) {
        if (stor.listeZoneEtape1l1x2[i].zone.reponse() === '') {
          yaya = false
          stor.listeZoneEtape1l1x2[i].zone.focus()
          break
        }
      }
      if (yaya) {
        for (i = 0; i < stor.listeZoneEtape1l1y2.length; i++) {
          if (stor.listeZoneEtape1l1y2[i].zone.reponse() === '') {
            yaya = false
            stor.listeZoneEtape1l1y2[i].zone.focus()
            break
          }
        }
      }
      if (yaya) {
        if (stor.rep2.reponse() === '') {
          yaya = false
          stor.rep2.focus()
        }
      }
    }
    if (stor.encours === 'concl') {
      if (!stor.listeconcle2.changed) {
        stor.listeconcle2.focus()
        yaya = false
      }
      if (!stor.listeconcle.changed) {
        stor.listeconcle.focus()
        yaya = false
      }
    }
    if (yaya) { return true } else {
      if (osef) return false
      stor.lesdiv.correction.style.color = me.styles.cfaux
      stor.lesdiv.correction.innerHTML = reponseIncomplete
      return false
    }
  }
  function boutonfen () {
    j3pToggleFenetres('Calculatrice')
  }

  function PoseEtape1 () {
    stor.lesdiv.solution.innerHTML = ''
    stor.lesdiv.correction.innerHTML = ''
    stor.lesdiv.explications.innerHTML = ''
    stor.listeaction.disable()
    stor.lesdiv.calculs.classList.add('travail')
    stor.lesdiv.labarre.style.border = '3px solid black'
    j3pEmpty(stor.lesdiv.zAction1)
    stor.lesdiv.zAction2.style.color = me.styles.petit.correction.color
    let i
    stor.encours = 'e1'
    const tgf = addDefaultTable(stor.lesdiv.calGauche, 3, 1)
    j3pAffiche(tgf[0][0], null, '<u>Calcul du 1er membre</u>:')
    stor.tabGGG = addDefaultTable(tgf[1][0], 3, 4)
    j3pAffiche(stor.tabGGG[0][0], null, '$' + stor.affMembre1 + '$')
    j3pAffiche(stor.tabGGG[0][1], null, '$=$')
    j3pAffiche(stor.tabGGG[2][0], null, '$' + stor.affMembre1 + '$')
    j3pAffiche(stor.tabGGG[2][1], null, '$=$')
    stor.tabGGG[0][2].style.background = '#fff'
    stor.tabGGG[0][2].style.padding = '0px 5px 5px 5px'
    stor.brouillon1 = new BrouillonCalculs({
      caseBoutons: stor.tabGGG[1][0],
      caseEgal: stor.tabGGG[1][1],
      caseCalculs: stor.tabGGG[1][2],
      caseApres: stor.tabGGG[1][3]
    },
    { limite: 30, limitenb: 20, restric: '0123456789()+-*/,.', hasAutoKeyboard: true, lmax: 3 })
    stor.ZoneXY = []
    stor.listeZoneEtape1l1x = []
    stor.listeZoneEtape1l1y = []
    let nx
    let ny
    let oksort = false
    const maf = []
    let x = stor.affMembre1
    do {
      nx = x.replace(/frac/g, '').indexOf(stor.Lexo.nx)
      ny = x.replace(/frac/g, '').indexOf(stor.Lexo.ny)
      if ((nx !== -1) || (ny !== -1)) {
        if (nx === -1) nx = 100000
        if (ny === -1) ny = 100000
        if (nx < ny) {
          maf.push(x.substring(0, nx))
          x = x.substring(nx + 1)
          stor.ZoneXY.push('x')
        } else {
          maf.push(x.substring(0, ny))
          x = x.substring(ny + 1)
          stor.ZoneXY.push('y')
        }
      } else { oksort = true }
    } while (!oksort)
    maf.push(x)
    const ghMM = addDefaultTable(stor.tabGGG[0][2], 1, maf.length * 2 - 1)
    stor.lco = ''
    for (i = 0; i < maf.length; i++) {
      j3pAffiche(ghMM[0][i * 2], null, '$' + maf[i] + '$')
      if (i < maf.length - 1) {
        if (stor.ZoneXY[i] === 'x') {
          stor.listeZoneEtape1l1x[stor.listeZoneEtape1l1x.length] = { co: suivCoef() }
          stor.listeZoneEtape1l1x[stor.listeZoneEtape1l1x.length - 1].zone = new ZoneStyleMathquill3(ghMM[0][i * 2 + 1], { restric: '0123456789+-*/(),.', limitenb: 1, limite: 12, bloqueFraction: '+-*()', hasAutoKeyboard: true, enter: function () { verifEtape1(true) } })
        } else {
          stor.listeZoneEtape1l1y[stor.listeZoneEtape1l1y.length] = { co: suivCoef() }
          stor.listeZoneEtape1l1y[stor.listeZoneEtape1l1y.length - 1].zone = new ZoneStyleMathquill3(ghMM[0][i * 2 + 1], { restric: '0123456789+-*/(),.', limitenb: 1, limite: 12, bloqueFraction: '+-*()', hasAutoKeyboard: true, enter: function () { verifEtape1(true) } })
        }
      }
    }
    stor.rep1 = new ZoneStyleMathquill2(stor.tabGGG[2][2], { id: 'GHTYrezUZUl2c2z', restric: '0123456789()-/,.', limitenb: 1, limite: 12, hasAutoKeyboard: true, enter: function () { verifEtape1(false) }, inverse: true })
    j3pDetruit('BOUBOUT')
    j3pAjouteBouton(tgf[2][0], 'BOUBOUT', 'big ok', 'OK', function () { verifEtape1(false) })
  }
  function suivCoef () {
    if (stor.lco === '') {
      if (stor.Lexo.eq.a1 !== 0) {
        stor.lco = 'a1'
        return { n: stor.Lexo.eq.a1, c: true }
      }
    }
    if (stor.lco === 'a1' || stor.lco === '') {
      if (stor.Lexo.eq.a2 !== 0) {
        stor.lco = 'a2'
        return { n: stor.Lexo.eq.a2, c: true }
      }
    }
    if (stor.lco === 'a2' || stor.lco === 'a1' || stor.lco === '') {
      if (stor.Lexo.eq.b1 !== 0) {
        stor.lco = 'b1'
        return { n: stor.Lexo.eq.b1, c: false }
      }
    }
    if (stor.lco === 'b1' || stor.lco === 'a2' || stor.lco === 'a1' || stor.lco === '') {
      if (stor.Lexo.eq.b2 !== 0) {
        stor.lco = 'b2'
        return { n: stor.Lexo.eq.b2, c: false }
      }
    }
    return 0
  }
  function verifEtape1 (osef) {
    let i
    const b = []
    let c = {}
    const x = {}
    const y = {}
    switch (stor.Lexo.val) {
      case 'entier':
        x.num = stor.Lexo.num
        x.den = 1
        y.num = stor.Lexo.num2
        y.den = 1
        break
      case 'decimal':
        x.num = Math.round(stor.Lexo.num * 10000)
        x.den = 10000
        y.num = Math.round(stor.Lexo.num2 * 10000)
        y.den = 10000
        break
      case 'fraction':
        x.num = stor.Lexo.num
        x.den = stor.Lexo.den
        y.num = stor.Lexo.num2
        y.den = stor.Lexo.den2
    }
    c.num = (stor.Lexo.eq.a1 * x.num * x.num + stor.Lexo.eq.b1 * x.num * x.den + stor.Lexo.eq.c1 * x.den * x.den) * y.den * y.den
    c.num += (stor.Lexo.eq.a2 * y.num * y.num + stor.Lexo.eq.b2 * y.num * y.den) * x.den * x.den
    c.den = x.den * x.den * y.den * y.den
    let de = j3pPGCD(c.num, c.den, { negativesAllowed: true, valueIfZero: 1 })
    c.num = Math.round(c.num / de)
    c.den = Math.round(c.den / de)
    de = stor.rep1.reponsenb()
    stor.resul1 = c
    let ok = true
    if (!yaReponse(osef)) return
    // verif que ya x
    // sauf si coef = 1 ou -1
    for (i = 0; i < stor.listeZoneEtape1l1x.length; i++) {
      b.push(stor.listeZoneEtape1l1x[i].zone.reponse())
      if ((stor.listeZoneEtape1l1x[i].co.n !== 1) && (stor.listeZoneEtape1l1x[i].co.n !== -1)) {
        c = b[i].substring(0, 1)
        if (c !== '×') {
          stor.lerr = 'oubliFois'
          stor.listeZoneEtape1l1x[i].zone.corrige(false)
          ok = false
        } else {
          b[i] = b[i].substring(1)
        }
      }
    }
    for (i = 0; i < stor.listeZoneEtape1l1y.length; i++) {
      b.push(stor.listeZoneEtape1l1y[i].zone.reponse())
      if ((stor.listeZoneEtape1l1y[i].co.n !== 1) && (stor.listeZoneEtape1l1y[i].co.n !== -1)) {
        c = b[b.length - 1].substring(0, 1)
        if (c !== '×') {
          stor.lerr = 'oubliFois'
          stor.listeZoneEtape1l1y[i].zone.corrige(false)
          ok = false
        } else {
          b[b.length - 1] = b[b.length - 1].substring(1)
        }
      }
    }
    if (!ok) {
      stor.encours = 'e1CO'
      stor.repEL = false
      me.sectionCourante()
      return
    }
    // verif () pour négatif
    // sauf si 1er et pas carré
    for (i = 0; i < stor.listeZoneEtape1l1x.length; i++) {
      if (b[i] === '') {
        stor.lerr = 'nbmalecrit'
        stor.listeZoneEtape1l1x[i].zone.corrige(false)
        ok = false
        continue
      }
      c = parseFloat2(b[i])
      if (c === 'err') {
        stor.lerr = 'nbmalecrit'
        ok = false
        stor.listeZoneEtape1l1x[i].zone.corrige(false)
      }
      b[i] = b[i].replace(/ /g, '')
      if ((c < 0) && ((stor.listeZoneEtape1l1x[i].co.c) || (i !== 0) || (stor.listeZoneEtape1l1x[i].co.n !== 1))) {
        if ((b[i][0] !== '(') || (b[i][b[i].length - 1] !== ')')) {
          stor.lerr = 'pbparent'
          stor.listeZoneEtape1l1x[i].zone.corrige(false)
          ok = false
        } else {
          b[i] = b[i].replace('(', '').replace(')', '')
        }
      }
    }
    let cmpt = i
    for (i = 0; i < stor.listeZoneEtape1l1y.length; i++) {
      if (b[i + cmpt] === '') {
        stor.lerr = 'nbmalecrit'
        stor.listeZoneEtape1l1y[i].zone.corrige(false)
        ok = false
        continue
      }
      c = parseFloat2(b[i + cmpt])
      if (c === 'err') {
        stor.lerr = 'nbmalecrit'
        ok = false
        stor.listeZoneEtape1l1y[i].zone.corrige(false)
      }
      if ((c < 0) && ((stor.listeZoneEtape1l1y[i].co.c) || (i + cmpt !== 0))) {
        if ((b[i + cmpt][0] !== '(') || (b[i + cmpt][b[i + cmpt].length - 1] !== ')')) {
          stor.lerr = 'pbparent'
          stor.listeZoneEtape1l1y[i].zone.corrige(false)
          ok = false
        } else {
          b[i + cmpt] = b[i + cmpt].replace('(', '').replace(')', '')
        }
      }
    }
    if (!ok) {
      stor.encours = 'e1CO'
      stor.repEL = false
      me.sectionCourante()
      return
    }
    // verif que c’est les bonnes valeurs
    for (i = 0; i < stor.listeZoneEtape1l1x.length; i++) {
      b[i] = b[i].replace('(', '').replace(')', '')
      if (b[i] !== stor.affnb1) {
        stor.lerr = 'malcopie'
        stor.listeZoneEtape1l1x[i].zone.corrige(false)
        ok = false
      }
    }
    cmpt = i
    for (i = 0; i < stor.listeZoneEtape1l1y.length; i++) {
      b[i + cmpt] = b[i + cmpt].replace('(', '').replace(')', '')
      if (b[i + cmpt] !== stor.affnb2) {
        stor.lerr = 'malcopie'
        stor.listeZoneEtape1l1y[i].zone.corrige(false)
        ok = false
      }
    }
    if (!ok) {
      stor.encours = 'e1CO'
      stor.repEL = false
      me.sectionCourante()
      return
    }
    /// et reslutat quand mm ...
    if (!egalite(stor.resul1, de)) {
      stor.lerr = 'egal1'
      stor.repEl = false
      stor.encours = 'e1CO'
      stor.rep1.corrige(false)
      me.sectionCourante()
      return
    }
    stor.etape1 = true
    stor.encours = ''
    Close1()
  }
  function Close1 () {
    let i
    j3pDetruit('BOUBOUT')
    stor.rep1.disable()
    stor.rep1.corrige(true)
    for (i = 0; i < stor.listeZoneEtape1l1x.length; i++) {
      stor.listeZoneEtape1l1x[i].zone.disable()
      stor.listeZoneEtape1l1x[i].zone.corrige(true)
    }
    for (i = 0; i < stor.listeZoneEtape1l1y.length; i++) {
      stor.listeZoneEtape1l1y[i].zone.disable()
      stor.listeZoneEtape1l1y[i].zone.corrige(true)
    }
    stor.brouillon1.disable()
    remetchoix()
  }

  function PoseEtape2 () {
    stor.lesdiv.solution.innerHTML = ''
    stor.lesdiv.correction.innerHTML = ''
    stor.lesdiv.explications.innerHTML = ''
    stor.listeaction.disable()
    stor.lesdiv.calculs.classList.add('travail')
    stor.lesdiv.labarre.style.border = '3px solid black'
    j3pEmpty(stor.lesdiv.zAction1)
    stor.lesdiv.zAction2.style.color = me.styles.petit.correction.color
    let i
    stor.encours = 'e2'
    const gthfd = addDefaultTable(stor.lesdiv.calDroite, 3, 1)
    j3pAffiche(gthfd[0][0], null, '<u>Calcul du 2ème membre</u>:')
    stor.tabGGG2 = addDefaultTable(gthfd[1][0], 3, 4)
    j3pAffiche(stor.tabGGG2[0][0], null, '$' + stor.affMembre2 + '$')
    j3pAffiche(stor.tabGGG2[0][1], null, '$=$')
    j3pAffiche(stor.tabGGG2[2][0], null, '$' + stor.affMembre2 + '$')
    j3pAffiche(stor.tabGGG2[2][1], null, '$=$')
    stor.tabGGG2[0][2].style.background = '#fff'
    stor.tabGGG2[0][2].style.padding = '0px 5px 5px 5px'
    stor.brouillon2 = new BrouillonCalculs({
      caseBoutons: stor.tabGGG2[1][0],
      caseEgal: stor.tabGGG2[1][1],
      caseCalculs: stor.tabGGG2[1][2],
      caseApres: stor.tabGGG2[1][3]
    }, { limite: 30, limitenb: 20, restric: '0123456789()+-*/,.', hasAutoKeyboard: true, lmax: 3 })
    stor.ZoneXY2 = []
    stor.listeZoneEtape1l1x2 = []
    stor.listeZoneEtape1l1y2 = []
    let nx
    let ny
    let oksort = false
    const maf = []
    let x = stor.affMembre2
    do {
      nx = x.replace(/frac/g, '').indexOf(stor.Lexo.nx)
      ny = x.replace(/frac/g, '').indexOf(stor.Lexo.ny)
      if ((nx !== -1) || (ny !== -1)) {
        if (nx === -1) nx = 100000
        if (ny === -1) ny = 100000
        if (nx < ny) {
          maf.push(x.substring(0, nx))
          x = x.substring(nx + 1)
          stor.ZoneXY2.push('x')
        } else {
          maf.push(x.substring(0, ny))
          x = x.substring(ny + 1)
          stor.ZoneXY2.push('y')
        }
      } else { oksort = true }
    } while (!oksort)
    maf.push(x)
    const rty = addDefaultTable(stor.tabGGG2[0][2], 1, maf.length * 2 - 1)
    stor.lco = ''
    for (i = 0; i < maf.length; i++) {
      j3pAffiche(rty[0][i * 2], null, '$' + maf[i] + '$')
      if (i < maf.length - 1) {
        if (stor.ZoneXY2[i] === 'x') {
          stor.listeZoneEtape1l1x2[stor.listeZoneEtape1l1x2.length] = { co: suivCoef2() }
          stor.listeZoneEtape1l1x2[stor.listeZoneEtape1l1x2.length - 1].zone = new ZoneStyleMathquill3(rty[0][i * 2 + 1], { restric: '0123456789+-*/(),.', limitenb: 1, limite: 12, bloqueFraction: '+-*()', hasAutoKeyboard: true, enter: function () { verifEtape2(true) } })
        } else {
          stor.listeZoneEtape1l1y2[stor.listeZoneEtape1l1y2.length] = { co: suivCoef2() }
          stor.listeZoneEtape1l1y2[stor.listeZoneEtape1l1y2.length - 1].zone = new ZoneStyleMathquill3(rty[0][i * 2 + 1], { restric: '0123456789+-*/(),.', limitenb: 1, limite: 12, bloqueFraction: '+-*()', hasAutoKeyboard: true, enter: function () { verifEtape2(true) } })
        }
      }
    }
    stor.rep2 = new ZoneStyleMathquill2(stor.tabGGG2[2][2], { id: 'GHTYrUZU2l2c2z', restric: '0123456789-/(),.', limitenb: 1, limite: 12, hasAutoKeyboard: true, enter: function () { verifEtape2(false) }, inverse: true })
    j3pDetruit('BOUBOUT')
    j3pAjouteBouton(gthfd[2][0], 'BOUBOUT', 'big ok', 'OK', function () { verifEtape2(false) })
  }
  function suivCoef2 () {
    if (stor.lco === '') {
      if (stor.Lexo.eq.a3 !== 0) {
        stor.lco = 'a3'
        return { n: stor.Lexo.eq.a3, c: true }
      }
    }
    if (stor.lco === 'a3' || stor.lco === '') {
      if (stor.Lexo.eq.a4 !== 0) {
        stor.lco = 'a4'
        return { n: stor.Lexo.eq.a4, c: true }
      }
    }
    if (stor.lco === 'a4' || stor.lco === 'a3' || stor.lco === '') {
      if (stor.Lexo.eq.b3 !== 0) {
        stor.lco = 'b3'
        return { n: stor.Lexo.eq.b3, c: false }
      }
    }
    if (stor.lco === 'b3' || stor.lco === 'a4' || stor.lco === 'a3' || stor.lco === '') {
      if (stor.Lexo.eq.b4 !== 0) {
        stor.lco = 'b4'
        return { n: stor.Lexo.eq.b4, c: false }
      }
    }
    return 0
  }
  function verifEtape2 (osef) {
    let i
    const b = []
    let c = {}
    const x = {}
    const y = {}
    switch (stor.Lexo.val) {
      case 'entier':
        x.num = stor.Lexo.num
        x.den = 1
        y.num = stor.Lexo.num2
        y.den = 1
        break
      case 'decimal':
        x.num = Math.round(stor.Lexo.num * 10000)
        x.den = 10000
        y.num = Math.round(stor.Lexo.num2 * 10000)
        y.den = 10000
        break
      case 'fraction':
        x.num = stor.Lexo.num
        x.den = stor.Lexo.den
        y.num = stor.Lexo.num2
        y.den = stor.Lexo.den2
    }
    c.num = (stor.Lexo.eq.a3 * x.num * x.num * stor.Lexo.eq.c3.den + stor.Lexo.eq.b3 * x.num * x.den * stor.Lexo.eq.c3.den + stor.Lexo.eq.c3.num * x.den * x.den) * y.den * y.den
    c.num += (stor.Lexo.eq.a4 * y.num * y.num + stor.Lexo.eq.b4 * y.num * y.den) * x.den * x.den * stor.Lexo.eq.c3.den
    c.den = x.den * x.den * y.den * y.den * stor.Lexo.eq.c3.den
    let de = j3pPGCD(c.num, c.den, { negativesAllowed: true, valueIfZero: 1 })
    c.num = Math.round(c.num / de)
    c.den = Math.round(c.den / de)
    stor.resul2 = c
    let ok = true
    if (!yaReponse(osef)) return
    // verif que ya x
    // sauf si coef = 1 ou -1
    for (i = 0; i < stor.listeZoneEtape1l1x2.length; i++) {
      b.push(stor.listeZoneEtape1l1x2[i].zone.reponse())
      if ((stor.listeZoneEtape1l1x2[i].co.n !== 1) && (stor.listeZoneEtape1l1x2[i].co.n !== -1)) {
        c = b[i].substring(0, 1)
        if (c !== '×') {
          stor.lerr = 'oubliFois2'
          stor.listeZoneEtape1l1x2[i].zone.corrige(false)
          ok = false
        } else {
          b[i] = b[i].substring(1)
        }
      }
    }
    for (i = 0; i < stor.listeZoneEtape1l1y2.length; i++) {
      b.push(stor.listeZoneEtape1l1y2[i].zone.reponse())
      if ((stor.listeZoneEtape1l1y2[i].co.n !== 1) && (stor.listeZoneEtape1l1y2[i].co.n !== -1)) {
        c = b[b.length - 1].substring(0, 1)
        if (c !== '×') {
          stor.lerr = 'oubliFois2'
          stor.listeZoneEtape1l1y2[i].zone.corrige(false)
          ok = false
        } else {
          b[b.length - 1] = b[b.length - 1].substring(1)
        }
      }
    }
    if (!ok) {
      stor.encours = 'e2CO'
      stor.repEL = false
      me.sectionCourante()
      return
    }
    // verif () pour négatif
    // sauf si 1er et pas carré
    for (i = 0; i < stor.listeZoneEtape1l1x2.length; i++) {
      if (b[i] === '') {
        stor.lerr = 'nbmalecrit2'
        stor.listeZoneEtape1l1x2[i].zone.corrige(false)
        ok = false
        continue
      }
      b[i] = b[i].replace(/ /g, '')
      c = parseFloat2(b[i])
      if (c === 'err') {
        stor.lerr = 'nbmalecrit2'
        ok = false
        stor.listeZoneEtape1l1x2[i].zone.corrige(false)
      }
      if ((c < 0) && ((stor.listeZoneEtape1l1x2[i].co.c) || (i !== 0) || (stor.listeZoneEtape1l1x2[i].co.n !== 1))) {
        if ((b[i][0] !== '(') || (b[i][b[i].length - 1] !== ')')) {
          stor.lerr = 'pbparent2'
          stor.listeZoneEtape1l1x2[i].zone.corrige(false)
          ok = false
        } else {
          b[i] = b[i].replace('(', '').replace(')', '')
        }
      }
    }
    let cmpt = i
    for (i = 0; i < stor.listeZoneEtape1l1y2.length; i++) {
      if (b[i + cmpt] === '') {
        stor.lerr = 'nbmalecrit2'
        stor.listeZoneEtape1l1y2[i].zone.corrige(false)
        ok = false
        continue
      }
      c = parseFloat2(b[i + cmpt])
      if (c === 'err') {
        stor.lerr = 'nbmalecrit2'
        ok = false
        stor.listeZoneEtape1l1y2[i].zone.corrige(false)
      }
      if ((c < 0) && ((stor.listeZoneEtape1l1y2[i].co.c) || (i + cmpt !== 0))) {
        if ((b[i + cmpt][0] !== '(') || (b[i + cmpt][b[i + cmpt].length - 1] !== ')')) {
          stor.lerr = 'pbparent2'
          stor.listeZoneEtape1l1y2[i].zone.corrige(false)
          ok = false
        } else {
          b[i + cmpt] = b[i + cmpt].replace('(', '').replace(')', '')
        }
      }
    }
    if (!ok) {
      stor.encours = 'e2CO'
      stor.repEL = false
      me.sectionCourante()
      return
    }
    // verif que c’est les bonnes valeurs
    for (i = 0; i < stor.listeZoneEtape1l1x2.length; i++) {
      b[i] = b[i].replace('(', '').replace(')', '')
      if (b[i] !== stor.affnb1) {
        stor.lerr = 'malcopie2'
        stor.listeZoneEtape1l1x2[i].zone.corrige(false)
        ok = false
      }
    }
    cmpt = i
    for (i = 0; i < stor.listeZoneEtape1l1y2.length; i++) {
      b[i + cmpt] = b[i + cmpt].replace('(', '').replace(')', '')
      if (b[i + cmpt] !== stor.affnb2) {
        stor.lerr = 'malcopie2'
        stor.listeZoneEtape1l1y2[i].zone.corrige(false)
        ok = false
      }
    }
    if (!ok) {
      stor.encours = 'e2CO'
      stor.repEL = false
      me.sectionCourante()
      return
    }

    /// et reslutat quand mm ...
    de = stor.rep2.reponsenb()
    if (!egalite(stor.resul2, de)) {
      stor.lerr = 'egal2'
      stor.repEl = false
      stor.encours = 'e2CO'
      stor.rep2.corrige(false)
      me.sectionCourante()
      return
    }
    stor.etape2 = true
    stor.encours = ''
    Close2()
  }
  function Close2 () {
    let i
    j3pDetruit('BOUBOUT')
    stor.rep2.disable()
    stor.rep2.corrige(true)
    for (i = 0; i < stor.listeZoneEtape1l1x2.length; i++) {
      stor.listeZoneEtape1l1x2[i].zone.disable()
      stor.listeZoneEtape1l1x2[i].zone.corrige(true)
    }
    for (i = 0; i < stor.listeZoneEtape1l1y2.length; i++) {
      stor.listeZoneEtape1l1y2[i].zone.disable()
      stor.listeZoneEtape1l1y2[i].zone.corrige(true)
    }
    stor.brouillon2.disable()
    remetchoix()
  }

  function remetchoix () {
    stor.lesdiv.solution.innerHTML = ''
    stor.lesdiv.correction.innerHTML = ''
    stor.lesdiv.explications.innerHTML = ''
    let bob1
    let bob2
    if (stor.Lexo.nbinc === 'un') {
      bob1 = '$' + stor.Lexo.nx + ' = ' + stor.affnb1 + '$'
      bob2 = '$' + stor.Lexo.ny + ' = ' + stor.affnb2 + '$'
    } else {
      bob1 = '$' + stor.Lexo.nx + ' = ' + stor.affnb1 + '$ et $' + stor.Lexo.ny + ' = ' + stor.affnb2 + '$'
      bob2 = '$' + stor.Lexo.nx + ' = ' + stor.affnb2 + '$ et $' + stor.Lexo.ny + ' = ' + stor.affnb1 + '$'
    }
    let mch = ['Conclure', 'Calculer l’équation pour ' + bob1]
    stor.ch0 = 'Choisir'
    stor.ch1 = 'Conclure'
    stor.ch2 = 'Calculer l’équation pour ' + bob1
    stor.ch3 = 'Calculer le 1er membre pour ' + bob1
    stor.ch4 = 'Calculer le 1er membre pour ' + bob2
    stor.ch5 = 'Calculer le 2ème membre pour ' + bob1
    stor.ch6 = 'Calculer le 3ème membre pour ' + bob1
    if (!stor.etape1) mch.push('Calculer le 1er membre pour ' + bob1)
    mch.push('Calculer le 1er membre pour ' + bob2)
    if (!stor.etape2) mch.push('Calculer le 2ème membre pour ' + bob1)
    mch.push('Calculer le 3ème membre pour ' + bob1)
    mch = j3pShuffle(mch)
    mch.splice(0, 0, 'Choisir')
    j3pEmpty(stor.lesdiv.action)
    const ty = addDefaultTable(stor.lesdiv.action, 2, 1)
    const gg = addDefaultTable(ty[1][0], 1, 2)
    stor.lesdiv.zAction1 = ty[0][0]
    stor.lesdiv.zAction2 = gg[0][0]
    stor.lesdiv.zAction3 = gg[0][1]
    j3pAffiche(stor.lesdiv.zAction1, null, '<u>Choisir une étape:</u>')
    stor.lesdiv.zAction1.style.color = me.styles.petit.correction.color
    stor.listeaction = ListeDeroulante.create(stor.lesdiv.zAction2, mch, { centre: true })
    j3pAjouteBouton(stor.lesdiv.zAction3, 'BOUBOUT', 'big ok', 'OK', executeaction)
  }
  function egalite (a, b) {
    if (a.den === 0) {
      j3pShowError('erreur du programme')
      return true
    }
    if (b[1] === 0) return false
    return (Math.abs(a.num / a.den - b[0] / b[1]) < 0.00000001)
  }
  function parseFloat2 (c) {
    c = c.replace(/ /g, '')
    c = c.replace('(', '')
    c = c.replace(')', '')
    c = c.replace(',', '.')

    if (c.indexOf('+') !== -1) {
      return 'err'
    }
    if ((c.indexOf('-') !== -1) && (c.indexOf('-') !== 0)) {
      return 'err'
    }
    if (c.indexOf('-') !== c.lastIndexOf('-')) {
      return 'err'
    }
    let b = 1
    if (c.indexOf('-') !== -1) { b = -1 }
    c = c.replace('-', '')

    if (c.indexOf('frac') !== -1) {
      return b * parseFloat(c.substring(6, c.indexOf('}{')) / parseFloat(c.substring(c.indexOf('}{') + 2, c.length - 1)))
    } else {
      return b * parseFloat(c)
    }
  }

  function executeaction () {
    // cette fct appelle sectionCourante car … (à détailler)
    switch (stor.listeaction.reponse) {
      case stor.ch0: affModaleInfo('Il faut faire un choix !')
        return
      case stor.ch1:
        if (!stor.etape1 || !stor.etape2) {
          stor.repEl = false
          stor.lerr = 'ConclusionHative'
          me.sectionCourante()
          return
        }
        PoseConclusion()
        break
      case stor.ch2:
        stor.repEl = false
        stor.lerr = 'CalculEquation'
        me.sectionCourante()
        break
      case stor.ch3:
        PoseEtape1()
        break
      case stor.ch4:
        stor.repEl = false
        stor.lerr = 'RemplecementFoireu'
        me.sectionCourante()
        break
      case stor.ch5:
        PoseEtape2()
        break
      case stor.ch6:
        stor.repEl = false
        stor.lerr = '3emeMembre'
        me.sectionCourante()
    }
  }

  function PoseConclusion () {
    j3pDetruit('BOUBOUT')
    j3pEmpty(stor.lesdiv.zAction1)
    stor.lesdiv.zAction2.style.color = me.styles.petit.correction.color
    stor.listeaction.disable()
    stor.lesdiv.conclusion.classList.add('travail')
    stor.encours = 'concl'
    let buf
    let buf2
    const tabConc = addDefaultTable(stor.lesdiv.conclusion, 2, 1)
    const tabRrT = addDefaultTable(tabConc[0][0], 1, 2)
    const tabRT = addDefaultTable(tabConc[1][0], 1, 3)
    if (stor.Lexo.nbinc === 'un') {
      buf = '$' + stor.Lexo.nx + '$ par $' + stor.affnb1 + '$'
      buf2 = '$' + stor.affnb1 + '$'
    } else {
      buf = '$(' + stor.Lexo.nx + ' ; ' + stor.Lexo.ny + ')$ par $(' + stor.affnb1 + ' ; ' + stor.affnb2 + ')$'
      buf2 = '$(' + stor.affnb1 + ' ; ' + stor.affnb2 + ')$'
    }
    j3pAffiche(tabRrT[0][0], null, 'Quand je remplace ' + buf + ' , les deux membres ')
    j3pAffiche(tabRT[0][0], null, 'Donc ' + buf2 + ' ')
    j3pAffiche(tabRT[0][2], null, ' de cette équation.')
    stor.listeconcle = ListeDeroulante.create(tabRrT[0][1], ['Choisir', 'sont égaux.', 'ne sont pas égaux.'])
    stor.listeconcle2 = ListeDeroulante.create(tabRT[0][1], ['Choisir', 'est une solution', ' n’est pas une solution '])
    j3pAjouteBouton(stor.lesdiv.conclusion, 'BOUBOUT', 'big ok', 'OK', verifConcl)
  }
  function verifConcl () {
    if (!yaReponse()) return
    const d1 = (stor.listeconcle.reponse === 'sont égaux.')
    const d2 = egalite2(stor.resul1, stor.resul2)
    if (d1 !== d2) {
      stor.lerr = 'fauusegal'
      stor.repEl = false
      stor.encours = 'COconcl'
      return
    }
    const d3 = (stor.listeconcle2.reponse === 'est une solution')
    if (d1 !== d3) {
      stor.lerr = 'fauusssol'
      stor.encours = 'COconcl'
      stor.repEl = false
      me.sectionCourante()
      return
    }
    stor.repEl = true
    stor.encours = 'COconcl'
    stor.listeconcle.corrige(true)
    stor.listeconcle2.corrige(true)
    stor.listeconcle.disable()
    stor.listeconcle2.disable()
    j3pDetruit('BOUBOUT')
    me.sectionCourante()
  }

  function calculfois (a, b, c) {
    return { num: j3pArrondi(a * b, 4), den: c }
  }
  function affModaleInfo (texte) {
    const yy = j3pModale({ titre: 'Information', contenu: '' })
    j3pAffiche(yy, null, texte)
    yy.style.color = me.styles.toutpetit.correction.color
  }
  function egalite2 (a, b) {
    if (a.den === 0) return false
    if (b.den === 0) return false
    return (j3pArrondi(a.num * b.den, 10) === j3pArrondi(b.num * a.den, 10))
  }

  function aj (a, b, c, d) {
    if (b === 0) return a
    let bs
    let bx = b
    if (b < 0) { bs = ' ' } else { bs = ' +' }
    if (b === -1 && c !== '') { bx = ''; bs = ' -' }
    if (b === 1 && c !== '') bx = ''
    return a + bs + bx + c + d + ' '
  }
  function aj2 (a, b, c, d) {
    if (b === 0) return a
    let bs
    let bx = b
    let bc = c
    if ((c[0] === '-') && (b !== 1)) bc = '(' + bc + ')'
    if (b !== 1 && b !== -1 && c !== '') bc = ' \\times ' + bc
    if (b < 0) { bs = ' ' } else { bs = ' +' }
    if (b === -1 && c !== '') { bx = ''; bs = ' -' }
    if (b === 1 && c !== '') bx = ''
    return a + bs + bx + bc + d + ' '
  }
  function unefractionnondecimale () {
    const ret = {}
    let multpos = [2, 3, 5, 7, 11]
    multpos = j3pShuffle(multpos)
    ret.num = multpos.pop()
    ret.den = multpos.pop()
    return ret
  }
  function Affichemoibien (x) {
    switch (stor.Lexo.val) {
      case 'entier' :
        return x.num + ''
      case 'decimal' :
        return (j3pArrondi(x.num / x.den, 4) + '').replace('.', ',')
      case 'fraction':
        return '\\frac{' + x.num + '}{' + x.den + '}'
    }
  }
  function Affichemoibien2 (x) {
    switch (stor.Lexo.val) {
      case 'entier' :
        return x.num + ''
      case 'decimal' :
        return (x.num + '').replace('.', ',')
      case 'fraction':
        if (x.den === 1) return x.num + ''
        if (x.num > 0) { return '\\frac{' + x.num + '}{' + x.den + '}' } else {
          return '-\\frac{' + Math.abs(x.num) + '}{' + x.den + '}'
        }
      case 'racine':
        return '\\sqrt {' + x.num + '}'
    }
  }
  function Affichemoibien2carre (x) {
    switch (stor.Lexo.val) {
      case 'entier' :
        return Affichemoibien2({ num: Math.round(x.num * x.num), den: 1 })
      case 'decimal' :
        return Affichemoibien2({ num: j3pArrondi(x.num * x.num, 4), den: 1 })
      case 'fraction':
        return Affichemoibien2({ num: Math.round(x.num * x.num), den: Math.round(x.den * x.den) })
      case 'racine':
        return '\\sqrt {' + x.num + '}'
    }
  }

  function enonceMain () {
    if (me.etapeCourante === 1) {
      me.videLesZones()
    }
    creeLesDiv()

    stor.Lexo = faisChoixExos()

    stor.lesdiv.action.classList.add('travail')
    stor.lesdiv.consigneG.classList.add('enonce')
    me.zonesElts.MG.classList.add('fond')
    poseQuestion()
    if (ds.Calculatrice) {
      me.fenetresjq = [{ name: 'Calculatrice', title: 'Une calculatrice', left: 600, top: 10 }]
      j3pCreeFenetres(me)
      j3pAfficheCroixFenetres('Calculatrice')
      j3pAjouteBouton(stor.lesdiv.calculatrice, 'Calculatrice', 'MepBoutons', 'Calculatrice', boutonfen)
    }

    me.finEnonce()
    me.cacheBoutonValider()
  }

  switch (this.etat) {
    case 'enonce':
      // code exécuté au lancement de la section
      if (me.debutDeLaSection) {
        initSection()
      } else {
        enonceMain()
      }
      break // case "enonce":

    case 'correction': {
      stor.lesdiv.explications.innerHTML = ''
      stor.lesdiv.correction.innerHTML = ''
      if (stor.encours === 'e1') {
        verifEtape1()
        return this.finCorrection()
      }
      if (stor.encours === 'e2') {
        verifEtape2()
        return this.finCorrection()
      }
      if (stor.encours === 'concl') {
        verifConcl()
        return this.finCorrection()
      }

      if (stor.repEl === true) {
        this.score++
        stor.lesdiv.correction.style.color = this.styles.cbien
        stor.lesdiv.correction.innerHTML = cBien

        this.typederreurs[0]++
        return this.finCorrection('navigation', true)
      }

      // Pas de bonne réponse
      stor.lesdiv.correction.style.color = this.styles.cfaux
      stor.lesdiv.correction.innerHTML = cFaux
      if (this.isElapsed) {
        // A cause de la limite de temps :
        stor.lesdiv.correction.innerHTML += tempsDepasse + '<BR>'
        this.typederreurs[10]++
        AffCorrection(true)
        stor.lesdiv.solution.classList.add('correction')
        return this.finCorrection('navigation', true)
      }

      // Réponse fausse :
      if (me.essaiCourant < me.donneesSection.nbchances) {
        stor.lesdiv.correction.innerHTML += '<br>' + essaieEncore
        AffCorrection(false)
        this.typederreurs[1]++
        return this.finCorrection()
      }

      // Erreur au dernier essai
      AffCorrection(true)
      stor.lesdiv.solution.classList.add('correction')
      this.typederreurs[2]++
      this.finCorrection('navigation', true)
      break // case "correction":
    }

    case 'navigation':
      if (!this.sectionTerminee()) {
        this.etat = 'enonce'
      }
      this.finNavigation()
      break // case "navigation":
  }
}
