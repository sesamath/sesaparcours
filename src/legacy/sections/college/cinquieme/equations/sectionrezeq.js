import { j3pAddElt, j3pAjouteBouton, j3pAjouteCaseCoche, j3pArrondi, j3pDetruit, j3pEmpty, j3pGetRandomBool, j3pGetRandomInt, j3pModale, j3pPGCD, j3pShuffle } from 'src/legacy/core/functions'
import ListeDeroulante from 'src/legacy/outils/listeDeroulante/ListeDeroulante'
import MenuContextuel from 'src/legacy/outils/menuContextuel'
import { getPe, initCptPe } from 'src/legacy/sections/college/helperTommy'
import { j3pAffiche } from 'src/lib/mathquill/functions'
import { addDefaultTable, addDefaultTableDetailed } from 'src/legacy/themes/table'
import ZoneStyleMathquill3 from 'src/legacy/outils/zoneStyleMathquill/ZoneStyleMathquill3'
import { j3pCreeFenetres, j3pAfficheCroixFenetres, j3pToggleFenetres } from 'src/legacy/core/functionsJqDialog'
import { affichefois, afficheFrac, affichemoins, afficheplus } from 'src/legacy/outils/zoneStyleMathquill/functions'
import textesGeneriques from 'src/lib/core/textes'

const { cBien, cFaux, essaieEncore, tempsDepasse } = textesGeneriques

/**
 * Ajouter une description ici
 * @fileOverview
 * @author Tommy Barroy <t.barroy@laposte.net>
 * @since 2017-10
 */
export const params = {
  outils: ['mathquill', 'calculatrice'],
  /**
   * Ce qui est paramétrable dans l’éditeur de graphe
   * @type {LegacySectionParamsParametres}
   */
  parametres: [
    ['nbrepetitions', 3, 'entier', 'Nombre de répétitions de la section'],
    ['nbchances', 3, 'entier', 'Nombre de répétitions de la section'],
    ['fraction', true, 'boolean', '<u>true</u>:  Des coefficients peuvent être fractionnaires'],
    ['cinquieme', false, 'boolean', '<u>true</u>:  Une seule opération sur <i>x</i>'],
    ['Conseil', true, 'boolean', '<u>true</u>:  Le programme avertis l’élève quand une meilleure méthode existe.'],
    ['Anti_Boucle', true, 'boolean', '<u>true</u>:  Une méthode qui complique les calcul est comptée fausse.'],
    ['Exercice', 'Raisonnement_Calcul', 'liste', '<u>Raisonnement</u>:  L’élève doit donner l’opération à effectuer de chaque côté. <br> <br> <u>Calcul</u>: L’élève doit calculer chaque résultat intermédiaire.', ['Raisonnement', 'Calcul', 'Raisonnement_Calcul']],
    ['Calculatrice', true, 'boolean', '<u>true</u>:  Une calculatrice est disponible'],
    ['Simplifie', true, 'boolean', '<u>true</u>:  Si la réponse est une fraction, elle doit être simplifiée.'],
    // le modèle gère la compatibilité ascendante avec l’ancien paramètre MiseEnPagePlusClaire
    ['theme', 'standard', 'liste', 'Modèle pour l’affichage', ['standard', 'zonesAvecImageDeFond']]

  ],
  // dans le cas d’une section QUANTITATIVE
  pe: [
    { pe_1: 'Raisonnement' },
    { pe_2: 'Reduction' }
  ]
}

/**
 * section rezeq
 * @this {Parcours}
 */
export default function main () {
  const me = this
  const ds = me.donneesSection
  const stor = me.storage

  function creeLesDiv () {
    stor.lesdiv = {}
    stor.lesdiv.conteneur = j3pAddElt(me.zonesElts.MG, 'div', {
      style: me.styles.etendre('toutpetit.enonce', { padding: '10px' })
    })
    const L1 = addDefaultTable(stor.lesdiv.conteneur, 1, 3)
    stor.lesdiv.consigneG = L1[0][0]
    L1[0][1].style.width = '20px'
    stor.lesdiv.calculatrice = L1[0][2]
    stor.lesdiv.consigneG2 = addDefaultTable(stor.lesdiv.conteneur, 1, 3)[0][0]
    const L2 = addDefaultTable(stor.lesdiv.conteneur, 1, 3)
    stor.lesdiv.travail = L2[0][0]
    L2[0][1].style.width = '20px'
    stor.lesdiv.correction = L2[0][2]
    stor.lesdiv.explications = j3pAddElt(stor.lesdiv.conteneur, 'div')
    stor.lesdiv.solution = j3pAddElt(stor.lesdiv.conteneur, 'div')
    stor.lesdiv.explications.style.color = me.styles.cfaux
    stor.lesdiv.solution.style.color = me.styles.petit.correction.color
    stor.lesdiv.consigneG2.style.color = me.styles.petit.correction.color
    stor.lesdiv.correction.style.verticalAlign = 'bottom'
    stor.lesdiv.travail.style.verticalAlign = 'top'
  }
  function faisChoixExos () {
    const e = ds.lesExos.pop()
    let t = -50
    switch (e.val) {
      case 'entier' :
        e.num = j3pGetRandomInt(t, 50)
        e.den = 'z'
        e.num2 = j3pGetRandomInt(t, 50)
        e.den2 = 'z'
        e.num3 = j3pGetRandomInt(t, 50)
        e.den3 = 'z'
        e.num4 = j3pGetRandomInt(t, 50)
        e.den4 = 'z'
        break
      case 'fraction':
        t = unefractionnondecimale()
        e.num = t.num
        e.den = t.den
        t = unefractionnondecimale()
        e.num2 = t.num
        e.den2 = t.den
        t = unefractionnondecimale()
        e.num3 = t.num
        e.den3 = t.den
        t = unefractionnondecimale()
        e.num4 = t.num
        e.den4 = t.den
        break
    }
    if (ds.cinquieme) {
      e.num = e.num2 = e.num3 = e.num4 = 0
      e.den = e.den2 = e.den3 = e.den4 = 'z'
      const maxCas = (e.val === 'fraction') ? 5 : 3
      switch (j3pGetRandomInt(0, maxCas)) {
        case 0: // type x + a = b
          e.num = 1
          e.num2 = j3pGetRandomInt(1, 50)
          if (j3pGetRandomBool()) e.num2 = -e.num2
          e.num4 = j3pGetRandomInt(1, 50)
          if (j3pGetRandomBool()) e.num4 = -e.num4
          if (e.val === 'fraction') {
            e.den2 = e.den4 = j3pGetRandomInt(2, 9)
          }
          break
        case 1: // type b = x + a
          e.num3 = 1
          e.num2 = j3pGetRandomInt(1, 50)
          if (j3pGetRandomBool()) e.num2 = -e.num2
          e.num4 = j3pGetRandomInt(1, 50)
          if (j3pGetRandomBool()) e.num4 = -e.num4
          if (e.val === 'fraction') {
            e.den2 = e.den4 = j3pGetRandomInt(2, 9)
          }
          break
        case 2: // type ax = b
          e.num = j3pGetRandomInt(2, 50)
          e.num2 = 0
          e.num4 = j3pGetRandomInt(1, 50)
          break
        case 3: // type b = ax
          e.num3 = j3pGetRandomInt(2, 50)
          e.num4 = 0
          e.num2 = j3pGetRandomInt(1, 50)
          break
        case 4: // type x/a = b
          e.num = 1
          e.den = j3pGetRandomInt(2, 50)
          e.num2 = 0
          e.num4 = j3pGetRandomInt(1, 50)
          break
        case 5: // type b = x/a
          e.num3 = 1
          e.den3 = j3pGetRandomInt(2, 50)
          e.num4 = 0
          e.num2 = j3pGetRandomInt(1, 50)
      }
    }
    if ((e.num === 1) && ((e.den === 1) || (e.den === 'z')) && (e.num2 === 0) && (e.num3 === 0)) {
      e.num = 6
    }
    if ((e.num3 === 1) && ((e.den3 === 1) || (e.den3 === 'z')) && (e.num4 === 0) && (e.num === 0)) {
      e.num3 = -2
    }
    if (e.num === 0 && e.num3 === 0) e.num = e.num3 = 2
    return e
  }

  function initSection () {
    let i
    initCptPe(ds, stor)

    // On surcharge avec les parametres passés par le graphe
    me.validOnEnter = false // ex donneesSection.touche_entree

    // Construction de la page
    me.construitStructurePage('presentation3')
    ds.Raisonnement = ds.Exercice.indexOf('Raisonnement') !== -1
    ds.Calculs = ds.Exercice.indexOf('Calcul') !== -1

    stor.yakR = false
    ds.lesExos = []
    const lp = ['entier']
    if (ds.fraction) lp.push('fraction')

    stor.lp = []
    for (i = 0; i < lp.length; i++) {
      stor.lp[i] = { val: lp[i] }
    }
    stor.lp = j3pShuffle(stor.lp)

    for (i = 0; i < ds.nbitems; i++) {
      ds.lesExos[i] = { val: stor.lp[i % (stor.lp.length)].val }
    }

    stor.nbchances = ds.nbchances
    ds.nbchances = 1
    me.typederreurs = [0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0]

    me.score = 0

    me.afficheTitre('Résoudre une équation')
    enonceMain()
  }

  function AffCorrection () {
    let x
    let f = ''
    if (stor.encours === 'red') {
      stor.zd.disable()
      stor.zg.disable()
      stor.zg.barreIfKo()
      stor.zd.barreIfKo()
      stor.lesdiv.solution.innerHTML = '<table style="margin:0"><tr><td nowrap  id="anvfrfnfrhf"></td><td nowrap  id="anvfrfnfrhf2"></td><td nowrap  id="anvfrfnfrhf3"></td></tr></table>'
      j3pAffiche('anvfrfnfrhf2', null, '$=$')
      stor.eqencours.num = stor.at1.num
      stor.eqencours.den = stor.at1.den
      stor.eqencours.num2 = stor.at2.num
      stor.eqencours.den2 = stor.at2.den
      stor.eqencours.num3 = stor.at3.num
      stor.eqencours.den3 = stor.at3.den
      stor.eqencours.num4 = stor.at4.num
      stor.eqencours.den4 = stor.at4.den
      affMembre('anvfrfnfrhf', true, true)
      affMembre('anvfrfnfrhf3', false, true)
    }
    if (stor.encours === 'concl') {
      if (stor.repat === 'admet une seule solution:') {
        x = j3pPGCD(Math.abs(stor.sol.num), Math.abs(stor.sol.den))
        if (x !== 1) {
          f += ' = ' + Affichemoibien2({ num: Math.round(stor.sol.num / x), den: Math.round(stor.sol.den / x) }, false, false)
        }
        stor.sol = Affichemoibien2(stor.sol, false, false) + f
      } else {
        stor.sol = ''
      }
      j3pAffiche(stor.lesdiv.solution, null, 'cette équation ' + stor.repat + ' $' + stor.sol + '$')
      stor.liste.disable(stor.liste.isOk() === false)
      if (stor.zoneSol !== null) {
        stor.zoneSol.corrige(false)
        stor.zoneSol.disable()
        stor.zoneSol.barre()
      }
    }
    if (stor.encours === 'simpl') {
      stor.deb.barre()
      stor.deb.disable()
      j3pDetruit('AaAaAaAaAaaze2')
      let listm = ''
      for (x = 0; x < stor.den.length; x++) {
        if (stor.den[x] !== 1) listm += '$' + stor.den[x] + '$ ,'
      }
      listm = listm.substring(0, listm.length - 1)
      listm += '.'
      f = listm.lastIndexOf(',')
      if (f !== -1) {
        listm = listm.substring(0, f) + ' et ' + listm.substring(f + 1)
      }
      j3pAffiche(stor.lesdiv.solution, null, 'Tu pouvais multiplier par $' + stor.denmul + ' $ \ncar $' + stor.denmul + ' $ est un multiple de ' + listm)
    }
    if (stor.encours === 'cadre') {
      if (stor.rep === 1) j3pAffiche(stor.lesdiv.solution, null, 'Pour supprimer une soustraction il faut choisir une addition.')
      if (stor.rep === 2) j3pAffiche(stor.lesdiv.solution, null, 'Pour supprimer une addition il faut choisir une soustraction.')
      if (stor.rep === 3) j3pAffiche(stor.lesdiv.solution, null, 'Pour supprimer une 3 il faut choisir une 3.')
      if (stor.rep === 4) j3pAffiche(stor.lesdiv.solution, null, 'Pour supprimer une multiplication il faut choisir une division.')
    }
    if (stor.encours === '') {
      const yaxgauxhe = stor.eqencours.num !== 0
      const yaxdroite = stor.eqencours.num3 !== 0
      const yanbgauche = stor.eqencours.num2 !== 0
      const yanbdroite = stor.eqencours.num4 !== 0
      if (yaxgauxhe && yaxdroite) {
        if (yanbgauche && !yanbdroite) {
          j3pAffiche(stor.lesdiv.solution, null, 'Tu pouvais déplacer $' + fait(stor.eqencours.num, stor.eqencours.den, 'x') + '$')
        } else if (!yanbgauche && yanbdroite) {
          j3pAffiche(stor.lesdiv.solution, null, 'Tu pouvais déplacer $' + fait(stor.eqencours.num3, stor.eqencours.den3, 'x') + '$')
        }
      } else if (!yaxgauxhe) {
        if (yanbdroite) {
          j3pAffiche(stor.lesdiv.solution, null, 'Tu pouvais déplacer $' + fait(stor.eqencours.num4, stor.eqencours.den4, '') + '$')
        } else {
          j3pAffiche(stor.lesdiv.solution, null, 'Tu pouvais déplacer $' + fait(stor.eqencours.num3, stor.eqencours.den3, '') + '$')
        }
      } else {
        if (yanbgauche) {
          j3pAffiche(stor.lesdiv.solution, null, 'Tu pouvais déplacer $' + fait(stor.eqencours.num2, stor.eqencours.den2, '') + '$')
        } else {
          j3pAffiche(stor.lesdiv.solution, null, 'Tu pouvais déplacer $' + fait(stor.eqencours.num, stor.eqencours.den, '') + '$')
        }
      }
    }
    j3pAffiche(stor.lesdiv.explications, null, stor.lerr)
  }
  function fait (n, d, p) {
    if (d === 1 || d === 'z') {
      if (p === 'x' && n === 1) return 'x'
      if (p === 'x' && n === -1) return '-x'
      return String(n) + p
    } else {
      return '\\frac{' + n + '}{' + d + '}' + p
    }
  }
  function poseQuestion () {
    stor.bigTab = addDefaultTableDetailed(stor.lesdiv.travail, 1, 3)
    stor.zoneSol = null
    stor.inv = false
    stor.encours = ''
    stor.nbtente = stor.nbchances
    stor.listM = []
    stor.encours = ''
    stor.eqencours = copiede(stor.Lexo)
    const dd = addDefaultTable(stor.lesdiv.consigneG, 1, 4)
    j3pAffiche(dd[0][0], null, '<b>On souhaite résoudre l’équation suivante</b>&nbsp;: ')
    j3pAffiche(dd[0][2], null, ' &nbsp;$=$&nbsp;')
    affMembre(dd[0][1], true, false)
    affMembre(dd[0][3], false, false)
    if ((stor.Lexo.val === 'fraction') && !ds.cinquieme) {
      j3pAffiche(stor.lesdiv.consigneG2, null, '<i>Tu peux commencer la résolution ou faire une opération pour supprimer les dénominateurs</i>\n\n')
      const tyu = addDefaultTable(stor.lesdiv.consigneG2, 1, 3)
      tyu[0][1].style.width = '40px'
      j3pAjouteBouton(tyu[0][0], commence, { className: 'MepBoutons', value: 'Commencer' })
      j3pAjouteBouton(tyu[0][2], simpl, { className: 'MepBoutons', value: 'Supprimer les dénominateurs' })
    } else {
      j3pAffiche(stor.lesdiv.consigneG2, null, '<i>Clique sur un élément pour le supprimer de son membre.</i>\n\n')
      // affiche equ dans tableau avec elements menu context si indistinct
      AfficheEq()
    }
  }

  function commence () {
    j3pEmpty(stor.lesdiv.consigneG2)
    j3pAffiche(stor.lesdiv.consigneG2, null, '<i>Clique sur un élément pour le supprimer de son membre.</i>\n\n')
    // affiche equ dans tableau avec elements menu context si indistinct
    AfficheEq()
  }
  function simpl () {
    j3pEmpty(stor.lesdiv.consigneG2)
    const yy = addDefaultTable(stor.lesdiv.consigneG2, 2, 1)
    const tt = addDefaultTable(yy[0][0], 1, 2)
    j3pAffiche(tt[0][0], null, 'Il faut multiplier chaque membre par: &nbsp;')
    stor.deb = new ZoneStyleMathquill3(tt[0][1], { limite: 3, imitenb: 3, restric: '0123456789', enter: tienval })
    j3pAjouteBouton(yy[1][0], tienval, { className: 'big ok', value: 'OK' })
  }
  function tienval () {
    if (stor.deb.rep().length === 0) {
      stor.deb.focus()
      oublieRep()
      return
    }
    let i, j
    stor.encours = 'simpl'
    const parquoi = parseFloat(stor.deb.rep()[0])
    stor.den = [stor.Lexo.den, stor.Lexo.den2, stor.Lexo.den3, stor.Lexo.den4]
    if (stor.Lexo.num === 0) stor.den[0] = 1
    if (stor.Lexo.num2 === 0) stor.den[1] = 1
    if (stor.Lexo.num3 === 0) stor.den[2] = 1
    if (stor.Lexo.num4 === 0) stor.den[3] = 1
    for (i = 0; i < 4; i++) {
      if (stor.den[i] === 'z') stor.den[i] = 1
      for (j = 0; j < i; j++) {
        if (stor.den[i] === stor.den[j]) stor.den[i] = 1
      }
    }
    stor.denmul = stor.den[0] * stor.den[1] * stor.den[2] * stor.den[3]
    if (parquoi % stor.denmul !== 0) {
      stor.deb.corrige(false)
      stor.lerr = 'Ce facteur ne convient pas !'
      affModale(stor.lerr)
    } else {
      stor.denmul = parquoi
      commenceenfin()
    }
  }
  function commenceenfin () {
    stor.deb.disable()
    AfficheEq()
    detruitAll()
    stor.depasse = true
    stor.rep = 3
    stor.aff = '$' + stor.denmul + '$'
    stor.elt = stor.lesdiv.correction
    suiteMethode()
  }
  function AfficheEq () {
    stor.ppppar = []
    detruitAll()
    addRow()
    affMembre(stor.bigTab.cells[stor.bigTab.cells.length - 1][0], true, true)
    affMembre(stor.bigTab.cells[stor.bigTab.cells.length - 1][2], false, true)
  }
  function affMembre (a, b, c, x) {
    let n1, d1, n2, d2
    let af1, af2
    let dej = false
    if (b) {
      n1 = stor.eqencours.num
      d1 = stor.eqencours.den
      n2 = stor.eqencours.num2
      d2 = stor.eqencours.den2
    } else {
      n1 = stor.eqencours.num3
      d1 = stor.eqencours.den3
      n2 = stor.eqencours.num4
      d2 = stor.eqencours.den4
    }
    if ((n1 !== 0) && (n2 !== 0) && x) { j3pAffiche(a, null, '$($') }
    if (n1 !== 0) {
      af1 = Affichemoibien2({ num: n1, den: d1 }, false, true)
      const rrpourmenu1 = j3pAffiche(a, null, '$' + af1 + 'x$')
      if (c) addMenuC(rrpourmenu1.mqList[0], af1, true, b)
      dej = true
    }
    if (n2 !== 0) {
      af2 = Affichemoibien2({ num: n2, den: d2 }, dej, false)
      const rrpourmenu2 = j3pAffiche(a, null, '$' + af2 + '$')
      if (c) addMenuC(rrpourmenu2.mqList[0], af2, false, b)
    }
    if ((n1 !== 0) && (n2 !== 0) && x) { j3pAffiche(a, null, '$)$') }
    if ((n1 === 0) && (n2 === 0)) {
      j3pAffiche(a, null, '$0$')
    }
  }
  function Supprime (a, b, c, m) {
    // faire test dabord si c’est judicieux
    if (c === 3) {
      FaisFoSup('x')
      return
    }
    if (c === 2) {
      if (a) { // gauche
        if (b) { // x
          if (((stor.eqencours.num2 === 0 && stor.eqencours.num4 !== 0) || ((stor.eqencours.num3 === 0) && (stor.eqencours.num4 !== 0)))) {
            if (ds.Anti_Boucle) {
              FaisFoSup('xgauche')
              return
            }
            if (ds.Conseil) {
              AffModaleConseil(c, m)
              return
            }
          }
        } else {
          if (((stor.eqencours.num === 0) || ((stor.eqencours.num4 === 0) && (stor.eqencours.num3 !== 0)))) {
            if (ds.Anti_Boucle) {
              FaisFoSup('ngauche')
              return
            }
            if (ds.Conseil) {
              AffModaleConseil(c, m)
              return
            }
          }
        }
      } else {
        if (b) { // x
          if (((stor.eqencours.num4 === 0 && stor.eqencours.num2 !== 0) || ((stor.eqencours.num === 0) && (stor.eqencours.num2 !== 0)))) {
            if (ds.Anti_Boucle) {
              FaisFoSup('xdroite')
              return
            }
            if (ds.Conseil) {
              AffModaleConseil(c, m)
              return
            }
          }
        } else {
          if (((stor.eqencours.num3 === 0) || ((stor.eqencours.num2 === 0) && (stor.eqencours.num !== 0)))) {
            if (ds.Anti_Boucle) {
              FaisFoSup('ndroite')
              return
            }
            if (ds.Conseil) {
              AffModaleConseil(c, m)
              return
            }
          }
        }
      }
    }
    if (c === 1) {
      if (a) {
        if (((stor.eqencours.num2 !== 0) || (stor.eqencours.num3 !== 0)) && (stor.eqencours.num !== 1)) {
          if (ds.Conseil) {
            AffModaleConseil(c, m)
            return
          }
        }
      } else {
        if (((stor.eqencours.num4 !== 0) || (stor.eqencours.num !== 0)) && (stor.eqencours.num3 !== 1)) {
          if (ds.Conseil) {
            AffModaleConseil(c, m)
            return
          }
        }
      }
    }

    affCadre('Comment ' + m + ' ?', m, c)
  }
  function AffModaleConseil (c, m) {
    const kkk = j3pModale({ titre: 'Erreur', contenu: '' })
    j3pAffiche(kkk, null, 'Je te déconseille cette méthode , \ncela va compliquer tes calculs.')
    const tt = addDefaultTable(kkk, 1, 3)
    tt[0][1].style.width = '100%'
    j3pAjouteBouton(tt[0][0], boutonvalC2, { className: 'MepBoutons2', value: 'Changer' })
    j3pAjouteBouton(tt[0][2], () => { boutonvalC1(c, m) }, { className: 'MepBoutons2', value: 'Continuer' })
  }
  function boutonvalC1 (c, m) {
    j3pDetruit('modale')
    j3pDetruit('j3pmasque')
    affCadre('Comment ' + m + ' ?', m, c)
  }
  function boutonvalC2 () {
    j3pDetruit('modale')
    j3pDetruit('j3pmasque')
    resumeAll()
  }
  function FaisFoSup (k) {
    //
    switch (k) {
      case 'x': stor.lerr = 'Supprimer <i>x</i> n’est pas une bonne méthode !'
        break
      case 'xgauche': stor.lerr = 'Déplacer les <i>x</i> à droite ne va pas te servir pour isoler <i>x</i> !'
        break
      case 'xdroite': stor.lerr = 'Déplacer les <i>x</i> à gauche ne va pas te servir pour isoler <i>x</i> !'
        break
      case 'ngauche': stor.lerr = 'Déplacer les nombres à droite ne va pas te servir pour isoler <i>x</i> !'
        break
      case 'ndroite': stor.lerr = 'Déplacer les nombres à gauche ne va pas te servir pour isoler <i>x</i> !'
        break
    }
    affModale(stor.lerr)
  }
  function affCadre (text, m, b) {
    stor.encours = 'cadre'
    let k
    let n, p, m2
    const s = m.indexOf('-') !== -1
    m = m.replace('supprimer ', '')
    stor.aff = m
    if (b === 1) stor.rep = 4
    if (b === 2) {
      if (s) { stor.rep = 1 } else { stor.rep = 2 }
    }
    if (b === 3) { stor.rep = 4 }
    p = 'Diviser'
    m2 = m
    if (m.indexOf('frac') !== -1) {
      k = m.replace('$', '').replace('$', '')
      if (m.indexOf('-') !== -1) { n = '-' } else { n = '' }
      k = k.replace(/ /g, '')
      k = { num: parseFloat(k.substring(k.indexOf('c{') + 2, k.indexOf('}{'))), den: parseFloat(k.substring(k.indexOf('}{') + 2, k.length - 1)) }
      m2 = '$' + n + ' \\frac{' + k.den + '}{' + k.num + '}$'
      if (k.num === 1) m2 = '$' + n + k.den + '$'
      p = 'Multiplier'
      stor.aff2 = m2
      stor.inv = true
    }
    if (!ds.Raisonnement) {
      boutonval()
      pauseAll()
      return
    }
    stor.elt = stor.lesdiv.correction
    const toutou = addDefaultTable(stor.elt, 7, 1)
    const haut = toutou[0][0]
    haut.style.textAlign = 'center'
    // j3pAddElt(mil, 'br')
    j3pAffiche(haut, null, text)
    haut.align = 'center'
    const bout = toutou[3][0]
    const bout2 = toutou[6][0]
    bout2.style.textAlign = 'center'
    if (m === '$ -$') m = '$-1$'
    if (m2 === '$ -$') m2 = '$-1$'
    stor.c1 = j3pAjouteCaseCoche(bout, { id: 'chochea', label: 'Ajouter ' + m.replace('-', '').replace('+', '') + ' à chaque membre' })
    stor.c1.name = 'famille'
    stor.c1.type = 'radio'
    stor.c1.style.cursor = 'pointer'
    if (stor.rep === 1) stor.co = 'Il fallait choisir: <b> Ajouter ' + m.replace('-', '').replace('+', '') + ' à chaque membre</b>'
    stor.c1.label.style.cursor = 'pointer'
    j3pAffiche(bout, null, '\n')
    stor.c2 = j3pAjouteCaseCoche(bout, { id: 'choche-', label: 'Soustraire ' + m.replace('-', '').replace('+', '') + ' à chaque membre' })
    stor.c2.name = 'famille'
    stor.c2.type = 'radio'
    stor.c2.style.cursor = 'pointer'
    if (stor.rep === 2) stor.co = 'Il fallait choisir: <b> Soustraire ' + m.replace('-', '').replace('+', '') + ' à chaque membre</b>'
    stor.c2.label.style.cursor = 'pointer'
    j3pAffiche(bout, null, '\n')
    stor.c3 = j3pAjouteCaseCoche(bout, { id: 'chochex', label: 'Multiplier chaque membre par ' + m })
    stor.c3.name = 'famille'
    stor.c3.type = 'radio'
    stor.c3.style.cursor = 'pointer'
    if (stor.rep === 3) stor.co = 'Il fallait choisir: <b> Multiplier chaque membre par ' + m + '</b>'
    stor.c3.label.style.cursor = 'pointer'
    j3pAffiche(bout, null, '\n')
    stor.c4 = j3pAjouteCaseCoche(bout, { id: 'choched', label: p + ' chaque membre par ' + m2 })
    stor.c4.name = 'famille'
    stor.c4.type = 'radio'
    stor.c4.style.cursor = 'pointer'
    if (stor.rep === 3) stor.co = 'Il fallait choisir: <b> ' + p + '  chaque membre par ' + m2 + '</b>'
    stor.c4.label.style.cursor = 'pointer'
    j3pAffiche(bout, null, '\n')
    j3pAjouteBouton(bout2, boutonval, { className: 'MepBoutons2', value: 'Valider' })
    stor.elt.style.background = '#00FFFF'
    stor.elt.style.border = '1px solid black'
    pauseAll()
  } // affCadre
  function boutonval () {
    let t = 0
    if (!ds.Raisonnement) { t = stor.rep } else {
      if (stor.c1.checked) t = 1
      if (stor.c2.checked) t = 2
      if (stor.c3.checked) t = 3
      if (stor.c4.checked) t = 4
      if (t === 0) return
    }
    if (t !== stor.rep) {
      if (stor.rep === 4 && t === 3 && stor.aff === '$ -$') {
        stor.rep = t; stor.aff = '$-1$'
      }
    }

    if (t !== stor.rep) {
      stor.compteurPe1++
      affModale('Cette opération ne te servira pas à supprimer l’expression choisie !')
    } else {
      if (stor.aff === '$ -$') stor.aff = '$-1$'
      if (stor.inv && stor.rep === 4) { stor.aff = stor.aff2; stor.rep = 3 }
      suiteMethode()
    }
  }
  function suiteMethode () {
    // detruit cadreinfo
    let t
    let b
    j3pEmpty(stor.lesdiv.correction)
    stor.ppppar = []
    stor.encours = 'red'
    if ((stor.rep === 1) || (stor.rep === 2)) stor.aff = stor.aff.replace('-', '')
    stor.aff = stor.aff.replace('+', '')
    j3pEmpty(stor.lesdiv.consigneG2)
    j3pAffiche(stor.lesdiv.consigneG2, null, '<i>Réduis  chaque membre</i>\n\n')
    if (ds.Raisonnement) {
      j3pEmpty(stor.elt)
      stor.elt.style.background = ''
      stor.elt.style.border = ''
    }
    // vire les menus
    detruitAll()
    // affiche une nouvelle ligne avec l’opération en rouge
    addRow()
    switch (stor.rep) {
      case 1:
        t = afficheplus(stor.bigTab.cells[stor.bigTab.cells.length - 1][0])
        affMembre(t[0], true, true)
        j3pAffiche(t[2], null, stor.aff)
        t[0].parentNode.parentNode.style.display = 'inline-table'
        t[2].style.color = '#f00'
        t = afficheplus(stor.bigTab.cells[stor.bigTab.cells.length - 1][2])
        affMembre(t[0], false, true)
        j3pAffiche(t[2], null, stor.aff)
        t[2].style.color = '#f00'
        break
      case 2:
        t = affichemoins(stor.bigTab.cells[stor.bigTab.cells.length - 1][0])
        affMembre(t[0], true, true)
        j3pAffiche(t[2], null, stor.aff)
        t[0].parentNode.parentNode.style.display = 'inline-table'
        t[2].style.color = '#f00'
        t = affichemoins(stor.bigTab.cells[stor.bigTab.cells.length - 1][2])
        affMembre(t[0], false, true)
        j3pAffiche(t[2], null, stor.aff)
        t[2].style.color = '#f00'
        break
      case 3:
        t = affichefois(stor.bigTab.cells[stor.bigTab.cells.length - 1][0])
        b = stor.aff
        if (b.indexOf('-') !== -1) b = '(' + b + ')'
        affMembre(t[0], true, true, true)
        j3pAffiche(t[2], null, b)
        t[2].style.color = '#f00'
        t = affichefois(stor.bigTab.cells[stor.bigTab.cells.length - 1][2])
        affMembre(t[0], false, true, true)
        j3pAffiche(t[2], null, b)
        t[2].style.color = '#f00'
        t[0].parentNode.parentNode.style.display = 'inline-table'
        break
      case 4:
        if (stor.inv) {
          b = stor.aff
          if (b.indexOf('-') !== -1) b = '(' + b + ')'
          t = affichefois(stor.bigTab.cells[stor.bigTab.cells.length - 1][0])
          t[0].parentNode.parentNode.style.display = 'inline-table'
          affMembre(t[0], true, true, true)
          j3pAffiche(t[2], null, b)
          t[2].style.color = '#f00'
          t = affichefois(stor.bigTab.cells[stor.bigTab.cells.length - 1][2])
          affMembre(t[0], false, true, true)
          j3pAffiche(t[2], null, b)
          t[2].style.color = '#f00'
        } else {
          t = afficheFrac(stor.bigTab.cells[stor.bigTab.cells.length - 1][0])
          t[0].parentNode.parentNode.parentNode.parentNode.style.display = 'inline-table'
          affMembre(t[0], true, true)
          j3pAffiche(t[2], null, stor.aff)
          t[2].style.color = '#f00'
          t = afficheFrac(stor.bigTab.cells[stor.bigTab.cells.length - 1][2])
          affMembre(t[0], false, true)
          j3pAffiche(t[2], null, stor.aff)
          t[2].style.color = '#f00'
        }
        break
    }

    // affiche un nouvelle ligne avec à compléter
    addRow()
    stor.zg = new ZoneStyleMathquill3(stor.bigTab.cells[stor.bigTab.cells.length - 1][0], {
      limite: 17,
      limitenb: 4,
      restric: '0123456789 /+-*x',
      bloqueFraction: 'x',
      enter: verifRed
    })
    stor.zd = new ZoneStyleMathquill3(stor.bigTab.cells[stor.bigTab.cells.length - 1][2], {
      limite: 17,
      limitenb: 4,
      restric: '01234567 89/ +-*x',
      clavier: '01234567 89/ +-*x',
      bloqueFraction: 'x',
      enter: verifRed
    })
    // mais un bouton ok
    j3pAjouteBouton(stor.lesdiv.explications, verifRed, { className: 'big ok', value: 'OK' })
    pauseAll()
    if (!ds.Calculs) { verifRed() }
  }
  function verifRed () {
    let n1 = {}
    let n2 = {}
    if (ds.Calculs) {
      const r1 = stor.zg.rep()
      const r2 = stor.zd.rep()
      if (r1.length === 0) {
        stor.zg.focus()
        oublieRep()
        return
      }
      if (r2.length === 0) {
        stor.zd.focus()
        oublieRep()
        return
      }

      /// a verif
      // pas deux signes qui se suivent
      n1 = coach(r1)
      n2 = coach(r2)
      if (stor.depasse) {
        if (n1.good && n2.good) {
          if (((n1.n1.den !== 1) && (n1.n1.den !== 'z')) || ((n1.n2.den !== 1) && (n1.n2.den !== 'z'))) {
            n1.good = false
            n1.erreur = 'Il ne doit plus y avoir de fractions !'
          }
          if (((n2.n1.den !== 1) && (n2.n1.den !== 'z')) || ((n2.n2.den !== 1) && (n2.n2.den !== 'z'))) {
            n2.good = false
            n2.erreur = 'Il ne doit plus y avoir de fractions !'
          }
        }
      }
    } else {
      n1.good = true
      n2.good = true
    }

    if (!n1.good) stor.zg.corrige(false)
    if (!n2.good) stor.zd.corrige(false)

    /// verif result
    const v1 = verifresult(n1, true)
    const v2 = verifresult(n2, false)

    if (!v1) stor.zg.corrige(false)
    if (!v2) stor.zd.corrige(false)

    if ((!n1.good) || (!n2.good)) {
      faisFoRed2(n1.good, n2.good, n1.erreur, n2.erreur)
      return
    }

    if (v1 && v2) {
      stor.depasse = false
      reposeQuset1(n1, n2)
    } else {
      faisFoRed2(v1, v2, 'Erreur de calcul', 'Erreur de calcul')
    }
  }
  function reposeQuset1 (a, b) {
    stor.inv = false
    stor.encours = ''
    stor.ppppar = []
    j3pEmpty(stor.lesdiv.correction)
    j3pEmpty(stor.lesdiv.explications)
    stor.eqencours.num = a.n2.num
    stor.eqencours.den = a.n2.den
    if (stor.eqencours.den < 0) {
      stor.eqencours.num = -stor.eqencours.num
      stor.eqencours.den = -stor.eqencours.den
    }
    stor.eqencours.num2 = a.n1.num
    stor.eqencours.den2 = a.n1.den
    if (stor.eqencours.den2 < 0) {
      stor.eqencours.num2 = -stor.eqencours.num2
      stor.eqencours.den2 = -stor.eqencours.den2
    }
    stor.eqencours.num3 = b.n2.num
    stor.eqencours.den3 = b.n2.den
    if (stor.eqencours.den3 < 0) {
      stor.eqencours.num3 = -stor.eqencours.num3
      stor.eqencours.den3 = -stor.eqencours.den3
    }
    stor.eqencours.num4 = b.n1.num
    stor.eqencours.den4 = b.n1.den
    if (stor.eqencours.den4 < 0) {
      stor.eqencours.num4 = -stor.eqencours.num4
      stor.eqencours.den4 = -stor.eqencours.den4
    }
    stor.zg.disable()
    stor.zd.disable()
    suppRow()
    AfficheEq()
    j3pDetruit('BOUBOUT')
    // fé test on peut conclure
    // on peut conclure si x tout seul d’un cote et pas de x en face
    if (((stor.eqencours.num === 1) && ((stor.eqencours.den === 1) || (stor.eqencours.den === 'z'))) && (stor.eqencours.num2 === 0) && (stor.eqencours.num3 === 0)) {
      lanceconclusion('1soluce1')
      return
    }
    if (((stor.eqencours.num3 === 1) && ((stor.eqencours.den3 === 1) || (stor.eqencours.den3 === 'z'))) && (stor.eqencours.num4 === 0) && (stor.eqencours.num === 0)) {
      lanceconclusion('1soluce2')
      return
    }
    if (stor.eqencours.num3 === 0 && stor.eqencours.num === 0) {
      lanceconclusion('toutourien')
      return
    }
    // si pas de x nulle part
    j3pEmpty(stor.lesdiv.consigneG2)
    j3pAffiche(stor.lesdiv.consigneG2, null, '<i>Clique sur un élément pour le supprimer de son membre.</i>\n\n')
  }
  function lanceconclusion (t) {
    stor.encours = 'concl'
    j3pEmpty(stor.lesdiv.correction)
    if (t === '1soluce1') {
      stor.repat = 'admet une seule solution:'
      stor.sol = { num: stor.eqencours.num4, den: stor.eqencours.den4 }
    }
    if (t === '1soluce2') {
      stor.repat = 'admet une seule solution:'
      stor.sol = { num: stor.eqencours.num2, den: stor.eqencours.den2 }
    }
    if (t === 'toutourien') {
      if (egalite({ num: stor.eqencours.num2, den: stor.eqencours.den2 }, { num: stor.eqencours.num4, den: stor.eqencours.den4 })) {
        stor.repat = 'a une infinité de solutions'
      } else {
        stor.repat = 'n’admet aucune solution.'
      }
    }
    detruitAll()
    stor.tabjj = addDefaultTable(stor.lesdiv.travail, 1, 3)
    j3pAffiche(stor.tabjj[0][0], null, 'Cette équation&nbsp;')
    const ty = ['Choisir', 'n’admet aucune solution.', 'a une infinité de solutions', 'admet une seule solution:']
    stor.liste = ListeDeroulante.create(stor.tabjj[0][1], ty, { onChange: gereC })
    j3pAjouteBouton(stor.lesdiv.explications, verifC, { className: 'big ok', value: 'OK' })
    if (ds.Simplifie) {
      j3pAffiche(stor.lesdiv.explications, null, '&nbsp;&nbsp;La réponse doit être simplifiée ')
    }
    j3pEmpty(stor.lesdiv.consigneG2)
    j3pAffiche(stor.lesdiv.consigneG2, null, '<i>Conclusion</i>\n\n')
  }
  function gereC () {
    if (stor.zoneSol !== null) {
      stor.zoneSol.disable()
      stor.zoneSol = null
    }
    j3pEmpty(stor.tabjj[0][2])
    if (stor.liste.reponse === 'admet une seule solution:') {
      stor.zoneSol = new ZoneStyleMathquill3(stor.tabjj[0][2], { restric: '-/0123456789', limitenb: 1, limite: 12, enter: verifC })
    }
  }
  function oublieRep () {
    stor.lesdiv.correction.innerHTML = ''
    j3pAffiche(stor.lesdiv.correction, null, 'Réponse incomplète !')
    stor.lesdiv.correction.style.color = me.styles.cfaux
  }
  function verifC () {
    if (!stor.liste.changed) {
      stor.liste.focus()
      oublieRep()
      return
    }
    if (stor.liste.reponse === 'admet une seule solution:') {
      if (stor.zoneSol.rep().length === 0) {
        stor.zoneSol.focus()
        oublieRep()
        return
      }
    }
    if (stor.repat !== stor.liste.reponse) {
      stor.lerr = 'Conclusion fausse'
      affModale('Conclusion fausse')
      stor.liste.corrige(false)
      if (stor.zoneSol !== null) {
        stor.zoneSol.corrige(false)
      }
      return
    }
    stor.liste.corrige(true)
    if (stor.repat === 'admet une seule solution:') {
      let vap = stor.zoneSol.rep()
      if (vap.length > 2) {
        stor.zoneSol.corrige(false)
        stor.lerr = 'Il suffit de recopier la solution !'
        affModale('Il suffit de recopier la solution !')
        return
      }
      if (vap.length === 2) {
        if (vap[0] !== '-') {
          stor.zoneSol.corrige(false)
          stor.lerr = 'Il suffit de recopier la solution !'
          affModale('Il suffit de recopier la solution !')
          return
        }
        vap = trans(vap[1])
        vap.num = -vap.num
      } else {
        vap = trans(vap[0])
      }
      if (!egalite(stor.sol, vap)) {
        stor.zoneSol.corrige(false)
        stor.lerr = 'Il suffit de recopier la solution !'
        affModale('Il suffit de recopier la solution !')
        return
      }
      if (ds.Simplifie) {
        if (j3pPGCD(vap.num, vap.den, { negativesAllowed: true, valueIfZero: 1 }) !== 1) {
          stor.zoneSol.corrige(false)
          stor.lerr = 'Tu dois simplifier ta réponse !'
          affModale('Tu dois simplifier ta réponse !')
          return
        }
      }
      if (j3pPGCD(vap.num, vap.den, { negativesAllowed: true, valueIfZero: 1 }) !== 1) {
        let t = Affichemoibien2(vap, false, false) + ' = '
        const pgcd = j3pPGCD(vap.num, vap.den, { negativesAllowed: true, valueIfZero: 1 })
        vap = {
          num: Math.round(vap.num / pgcd),
          den: Math.round(vap.den / pgcd)
        }
        t += Affichemoibien2(vap, false, false)
        j3pAffiche(stor.lesdiv.solution, null, '$' + t + '$')
      }
      stor.zoneSol.corrige(true)
      stor.zoneSol.disable()
    }
    stor.liste.disable()
    stor.repEl = true
    j3pDetruit('BOUTBOUT')
    me.sectionCourante()
  }
  function trans (t) {
    if (t.indexOf('ù') === -1) {
      return { num: parseFloat(t), den: 1 }
    } else {
      return { num: parseFloat(t.substring(1, t.indexOf('#'))), den: parseFloat(t.substring(t.indexOf('#') + 1, t.length - 1)) }
    }
  }
  function faisFoRed2 (a, b, c, x) {
    let bub = ''
    const comp = '<i><b>1x</b></i> s’écrit simplement <i><b>x</b></i> !'
    stor.compteurPe2++
    if (!a) bub += '<u>Membre de gauche</u>:' + c + '<br>'
    if (!b) bub += '<u>Membre de droite</u>:' + x + '<br>'

    if (((c === comp) && (b)) || ((x === comp) && (a)) || ((c === comp) && (x === comp))) {
      stor.nbtente++
    }

    stor.lerr = bub
    affModale(bub)
  }
  function verifresult (a, b) {
    let zu = stor.aff.replace('$', '').replace('$', '')
    zu = zu.replace(/ /g, '')
    const pu = zu.indexOf('x') !== -1
    zu = zu.replace('x', '')
    if (zu.indexOf('frac') !== -1) {
      if (zu.indexOf('-') !== -1) {
        zu = { num: -parseFloat(zu.substring(zu.indexOf('c{') + 2, zu.indexOf('}{'))), den: parseFloat(zu.substring(zu.indexOf('}{') + 2, zu.length - 1)) }
      } else {
        zu = { num: parseFloat(zu.substring(zu.indexOf('c{') + 2, zu.indexOf('}{'))), den: parseFloat(zu.substring(zu.indexOf('}{') + 2, zu.length - 1)) }
      }
    } else {
      if (zu === '') {
        zu = { num: 1, den: 1 }
      } else {
        zu = { num: parseFloat(zu), den: 1 }
      }
    }

    let n1, d1, n2, d2
    if (b) {
      n1 = stor.eqencours.num
      d1 = stor.eqencours.den
      n2 = stor.eqencours.num2
      d2 = stor.eqencours.den2
    } else {
      n1 = stor.eqencours.num3
      d1 = stor.eqencours.den3
      n2 = stor.eqencours.num4
      d2 = stor.eqencours.den4
    }
    const at1 = calculF(n1, d1, zu, pu, true)
    const at2 = calculF(n2, d2, zu, pu, false)
    if (b) { stor.at1 = at1; stor.at2 = at2 } else { stor.at3 = at1; stor.at4 = at2 }
    if (!ds.Calculs) { a.n1 = at2; a.n2 = at1 }
    if ((a.n1 === null) || (a.n2 === null)) return false
    return egalite(a.n1, at2) && egalite(a.n2, at1)
  }
  function calculF (a, b, c, x, e) {
    // si e true c coef x
    // si x tru , c nb op avec x
    if (b === 'z') b = 1
    let n, de, p
    switch (stor.rep) {
      case 1: // plus
        if (x !== e) return { num: a, den: b }
        n = a * c.den + b * c.num
        de = c.den * b
        p = j3pPGCD(n, de, { negativesAllowed: true, valueIfZero: 1 })
        n = Math.round(n / p)
        de = Math.round(de / p)
        return { num: n, den: de }
      case 2: // moins
        if (x !== e) return { num: a, den: b }
        n = a * c.den - b * c.num
        de = c.den * b
        p = j3pPGCD(n, de, { negativesAllowed: true, valueIfZero: 1 })
        n = Math.round(n / p)
        de = Math.round(de / p)
        return { num: n, den: de }
      case 3: // fois
        n = a * c.num
        de = c.den * b
        p = j3pPGCD(n, de, { negativesAllowed: true, valueIfZero: 1 })
        n = Math.round(n / p)
        de = Math.round(de / p)
        return { num: n, den: de }
      case 4: // fois
        n = a * c.den
        de = c.num * b
        p = j3pPGCD(n, de, { negativesAllowed: true, valueIfZero: 1 })
        n = Math.round(n / p)
        de = Math.round(de / p)
        return { num: n, den: de }
      default: console.error('erreur rezeq pour Tom')
        console.error(stor.rep, a, b, c, x, e)
        return { num: 0, den: 1 }
    }
  }
  function coach (t) {
    const l = []
    let i
    let ok
    let a, b, c
    let nn = 0
    let nx = 0
    let n1, n2
    for (i = 0; i < t.length; i++) {
      if ((t[i] === '+') || (t[i] === '-')) {
        l.push('s')
      } else if (t[i].indexOf('x') === -1) {
        l.push('n')
        nn++
      } else {
        l.push('x')
        nx++
      }
    }
    // pas deux nb sans signe entre deux
    // pas deux signe de suite
    for (i = 0; i < l.length; i++) {
      if (l[i] === 's') {
        if (ok === true) {
          return {
            good: false,
            n1: null,
            n2: null,
            erreur: 'Expression incorrecte (deux signes se suivent) !'
          }
        } else {
          ok = true
        }
      } else {
        if (ok === false) {
          return {
            good: false,
            n1: null,
            n2: null,
            erreur: 'Expression incorrecte (deux nombres se suivent sans opération) !'
          }
        } else {
          ok = false
        }
      }
      if (l[i] !== 's') {
        if (t[i].indexOf('ù') !== -1) {
          a = t[i].indexOf('ù')
          b = t[i].indexOf('#')
          c = t[i].indexOf('@')
          if (b - a < 2) {
            return {
              good: false,
              n1: null,
              n2: null,
              erreur: 'Expression incorrecte (une fraction sans numérateur) !'
            }
          }
          if (c - b < 2) {
            return {
              good: false,
              n1: null,
              n2: null,
              erreur: 'Expression incorrecte (une fraction sans dénominateur) !'
            }
          }
          if (a !== 0) {
            if (t[i][0] !== 'x') {
              return {
                good: false,
                n1: null,
                n2: null,
                erreur: 'Simplifie l’écriture de tes nombres !'
              }
            } else {
              return {
                good: false,
                n1: null,
                n2: null,
                erreur: 'Par convention, on écris <i><b>x</b></i> après son facteur !'
              }
            }
          }
          if (c !== t[i].length - 1) {
            if (t[i][t[i].length - 1] !== 'x') {
              return {
                good: false,
                n1: null,
                n2: null,
                erreur: 'Simplifie l’écriture de tes nombres !'
              }
            }
          }
        }
        if ((t[i].indexOf('x') !== t[i].length - 1) && (t[i].indexOf('x') !== -1)) {
          return {
            good: false,
            n1: null,
            n2: null,
            erreur: 'Par convention, on écris <i><b>x</b></i> après son facteur !'
          }
        }
        if ((l[i] === 'x') && (t[i][0] === '1') && (t[i].length === 2)) {
          return {
            good: false,
            n1: null,
            n2: null,
            erreur: "<i><b>1x</b></i> s'écrit simplement <i><b>x</b></i> !"
          }
        }
      }
    }

    if (l[l.length - 1] === 's') {
      return {
        good: false,
        n1: null,
        n2: null,
        erreur: 'Une expression ne peut pas se terminer par un signe + ou - !'
      }
    }

    // pas deux nb , et pas deux nb*x
    if ((nn > 1) || (nx > 1)) {
      return {
        good: false,
        n1: null,
        n2: null,
        erreur: 'Tu dois réduire tes expressions !'
      }
    }

    nn = l.indexOf('n')
    if (nn === -1) {
      n1 = { num: 0, den: 1 }
    } else {
      i = 1
      if (nn > 0) {
        if (t[nn - 1] === '-') i = -1
      }
      a = t[nn].indexOf('ù')
      if (a !== -1) {
        b = t[nn].indexOf('#')
        c = t[nn].indexOf('@')
        n1 = { num: i * parseFloat(t[nn].substring(1, b)), den: parseFloat(t[nn].substring(b + 1, c)) }
      } else {
        n1 = { num: i * parseFloat(t[nn]), den: 1 }
      }
    }

    nx = l.indexOf('x')
    if (nx === -1) {
      n2 = { num: 0, den: 1 }
    } else {
      i = 1
      if (nx > 0) {
        if (t[nx - 1] === '-') i = -1
      }
      a = t[nx].indexOf('ù')
      if (a !== -1) {
        b = t[nx].indexOf('#')
        c = t[nx].indexOf('@')
        n2 = { num: i * parseFloat(t[nx].substring(1, b)), den: parseFloat(t[nx].substring(b + 1, c)) }
      } else {
        b = t[nx].replace('x', '')
        if (b === '') b = '1'
        n2 = { num: i * parseFloat(b), den: 1 }
      }
    }
    // apres verif les nombres
    return { good: true, n1, n2, erreur: '' }
  }
  function affModale (texte) {
    stor.nbtente--
    if (stor.nbtente > 0) {
      let bufchance
      if (stor.nbtente > 1) {
        bufchance = 'chances.'
      } else {
        bufchance = 'chance.'
      }
      texte += ' <br> Il te reste ' + stor.nbtente + ' ' + bufchance
      const kkk = j3pModale({ titre: 'Erreur', contenu: '' })
      j3pAffiche(kkk, null, texte)
      /// /PB ICI
      kkk.style.color = me.styles.toutpetit.correction
    } else {
      if (stor.encours === 'cadre') {
        j3pEmpty(stor.elt)
        stor.elt.style.background = ''
        stor.elt.style.border = ''
      }
      if (stor.encours === 'red') { j3pDetruit('BOUTBOUT') }
      stor.repEl = false
      me.sectionCourante()
    }
  }
  function pauseAll () {
    let i
    for (i = 0; i < stor.listM.length; i++) {
      stor.listM[i].setActive(false)
    }
  }
  function resumeAll () {
    let i
    for (i = 0; i < stor.listM.length; i++) {
      stor.listM[i].setActive(true)
    }
  }
  function detruitAll () {
    let i
    for (i = stor.listM.length - 1; i > -1; i--) {
      stor.listM[i].destroy()
      stor.listM.pop()
    }
  }
  function addMenuC (x, y, z, g) {
    const m = []
    if (z) {
      if (y.replace(/ /g, '') !== '') {
        m.push({
          name: 'men1',
          label: 'Supprimer $' + y + '$',
          callback: function (menu/*, choice, event */) {
            Supprime(g, z, 1, 'supprimer $' + y + '$')
          }
        })
      }
      m.push({
        name: 'men2',
        label: 'Supprimer $' + y + 'x$',
        callback: function (menu/*, choice, event */) {
          Supprime(g, z, 2, 'supprimer $' + y + 'x$')
        }
      })
      if (y.replace(/ /g, '') !== '') {
        m.push({
          name: 'men3',
          label: 'Supprimer $x$',
          callback: function (menu/*, choice, event */) {
            Supprime(g, z, 3, 'supprimer $ x$')
          }
        })
      }
    } else {
      m.push({
        name: 'men1',
        label: 'Supprimer $' + y + '$',
        callback: function (menu/*, choice, event */) {
          Supprime(g, z, 2, 'supprimer $' + y + '$')
        }
      })
    }
    stor.listM.push(new MenuContextuel(x, m))
  }
  function suppRow () {
    j3pDetruit(stor.bigTab.lines[stor.bigTab.lines.length - 1])
    stor.bigTab.lines.pop()
    stor.bigTab.cells.pop()
  }
  function addRow () {
    /*
      if (!ds.Positifs) {
        if (document.getElementById('AAAAAAAAAAABBBBBBBBBBBROW') !== null) {
          j3pDetruit('AAAAAAAAAAABBBBBBBBBBBROW')
        }
        var trow = document.createElement('tr')
        elmt.appendChild(trow)
        trow.id = 'AAAAAAAAAAABBBBBBBBBBBROW'
        var td1row = document.createElement('td')
        var td2row = document.createElement('td')
        trow.appendChild(td1row)
        trow.appendChild(td2row)
        td1row.style.height = '35px'
      }
      */
    const elmt = stor.bigTab.table
    const tr = document.createElement('tr')
    elmt.appendChild(tr)
    const td1 = document.createElement('td')
    const td2 = document.createElement('td')
    const td3 = document.createElement('td')
    tr.appendChild(td1)
    tr.appendChild(td2)
    tr.appendChild(td3)
    td1.style.textAlign = 'right'
    j3pAffiche(td2, null, '$=$')
    tr.classList.add('travail')
    stor.bigTab.lines.push(tr)
    stor.bigTab.cells.push([td1, td2, td3])
  }
  function boutonfen () {
    j3pToggleFenetres('Calculatrice')
  }
  function copiede (x) {
    const ret = {}
    ret.num = x.num
    ret.den = x.den
    ret.num2 = x.num2
    ret.den2 = x.den2
    ret.num3 = x.num3
    ret.den3 = x.den3
    ret.num4 = x.num4
    ret.den4 = x.den4
    return ret
  }

  function egalite (a, b) {
    if (a.den === 0) {
      return false
    }
    if (b.den === 0) return false
    return (j3pArrondi(a.num * b.den, 10) === j3pArrondi(b.num * a.den, 10))
  }
  function enonceMain () {
    if (me.etapeCourante === 1) {
      me.videLesZones()
    }
    creeLesDiv()
    stor.Lexo = faisChoixExos()
    stor.lesdiv.travail.classList.add('travail')
    stor.lesdiv.consigneG.classList.add('enonce')
    stor.lesdiv.consigneG2.classList.add('enonce')
    me.zonesElts.MG.classList.add('fond')
    poseQuestion()
    if (ds.Calculatrice) {
      me.fenetresjq = [{ name: 'Calculatrice', title: 'Une calculatrice', left: 600, top: 10 }]
      j3pCreeFenetres(me)
      j3pAfficheCroixFenetres('Calculatrice')
      j3pAjouteBouton(stor.lesdiv.calculatrice, boutonfen, { className: 'MepBoutons', value: 'Calculatrice' })
    }

    me.finEnonce()
    me.cacheBoutonValider()
  }

  function unefractionnondecimale () {
    const ret = {}
    const multpos = [3, 7, 11, 2, 5, 1]
    const multposden = [3, 7, 11]
    const i = j3pGetRandomInt(0, multposden.length - 1)
    const kden = multposden[i]
    multposden.splice(i, 1)
    multpos.splice(i, 1)
    ret.num = multpos[j3pGetRandomInt(0, multpos.length - 1)]
    ret.den = kden
    if (j3pGetRandomBool()) ret.num = -ret.num
    return ret
  }
  function Affichemoibien2 (x, t, f) {
    let dev = ' '
    let zu = x.den
    if (zu === 'z') zu = 1
    if (t && (x.num * zu) > 0) dev += '+'
    if (x.den !== 'z') {
      if (x.num / x.den < 0) dev += '-'
      if (Math.abs(x.den) === 1) {
        if ((Math.abs(x.num) === 1) && f) return dev
        return dev + Math.abs(x.num)
      }
      return dev + '\\frac{' + Math.abs(x.num) + '}{' + Math.abs(x.den) + '}'
    }
    if ((x.num === 1) && f) return dev
    if ((x.num === -1) && f) return '-'
    return dev + x.num
  }

  switch (this.etat) {
    case 'enonce':
      // code exécuté au lancement de la section
      if (me.debutDeLaSection) {
        initSection()
      } else {
        enonceMain()
      }
      break // case "enonce":

    case 'correction': {
      stor.yaexplik = false
      stor.lesdiv.explications.innerHTML = ''
      stor.lesdiv.correction.innerHTML = ''
      stor.lesdiv.explications.classList.remove('explique')
      stor.lesdiv.solution.classList.remove('correction')

      let fi = false

      if (stor.repEl === true) {
        this._stopTimer()
        this.score++
        stor.lesdiv.correction.style.color = this.styles.cbien
        stor.lesdiv.correction.innerHTML = cBien

        fi = false

        this.typederreurs[0]++
        this.cacheBoutonValider()
        this.etat = 'navigation'
        this.sectionCourante()
      } else {
        // Pas de bonne réponse
        stor.lesdiv.correction.style.color = this.styles.cfaux
        stor.lesdiv.correction.innerHTML = cFaux
        if (this.isElapsed) {
          stor.lesdiv.correction.innerHTML += tempsDepasse + '<BR>'
          this.typederreurs[10]++
          this.cacheBoutonValider()

          AffCorrection(true)
          fi = true
          stor.yaexplik = true

          this.etat = 'navigation'
          this.sectionCourante()
        } else {
          // Réponse fausse :
          if (me.essaiCourant < me.donneesSection.nbchances) {
            stor.lesdiv.correction.innerHTML += '<br>' + essaieEncore

            AffCorrection(false)

            this.typederreurs[1]++
          } else {
            // Erreur au nème essai
            this.cacheBoutonValider() // probablement inutile

            AffCorrection()
            fi = true
            stor.yaexplik = true

            this.typederreurs[2]++
            this.etat = 'navigation'
            this.sectionCourante()
          }
        }
      }

      if (stor.yaexplik) stor.lesdiv.explications.classList.add('explique')
      if (fi) stor.lesdiv.solution.classList.add('correction')

      // Obligatoire
      this.finCorrection()
      break // case "correction":
    }

    case 'navigation':
      if (this.sectionTerminee()) {
        this.parcours.pe = getPe(ds, stor)
      } else {
        this.etat = 'enonce'
      }
      // Obligatoire
      this.finNavigation(true)
      break // case "navigation":
  }
}
