import { j3pAddElt, j3pAjouteBouton, j3pArrondi, j3pDetruit, j3pEmpty, j3pGetNewId, j3pGetRandomInt, j3pNotify, j3pShowError, j3pShuffle } from 'src/legacy/core/functions'
import ListeDeroulante from 'src/legacy/outils/listeDeroulante/ListeDeroulante'
import { j3pAffiche } from 'src/lib/mathquill/functions'
import { addDefaultTable } from 'src/legacy/themes/table'
import { j3pCreeSVG } from 'src/legacy/core/functionsSvg'
import textesGeneriques from 'src/lib/core/textes'
import { getMtgCore } from 'src/lib/outils/mathgraph'

const { cBien, cFaux, essaieEncore, reponseIncomplete, tempsDepasse } = textesGeneriques

/**
 * Ajouter une description ici
 * @fileOverview
 * @author Tommy Barroy <t.barroy@laposte.net>
 * @since 2017-10
*/
export const params = {
  outils: ['mathquill', 'calculatrice'],
  /**
   * Ce qui est paramétrable dans l’éditeur de graphe
   * @type {LegacySectionParamsParametres}
   */
  parametres: [
    ['nbrepetitions', 9, 'entier', 'Nombre de répétitions de la section'],
    ['nbchances', 1, 'entier', 'Nombre de chances'],
    ['Difficile', false, 'boolean', '<u>true</u>: Les mises en équations sont plus difficiles.'],
    // le modèle gère la compatibilité ascendante avec l’ancien paramètre MiseEnPagePlusClaire
    ['theme', 'standard', 'liste', 'Modèle pour l’affichage', ['standard', 'zonesAvecImageDeFond']]

  ]
}

const tousLesPbs = [
  {
    figure: '',
    diff: false,
    enonce: 'Yanis a obtenu $£a$ et $£b$ aux deux derniers devoirs. <br> Quelle note doit-il avoir au dernier devoir pour obtenir $£c$ de moyenne ?',
    choix: 'On appelle <i>x</i> la note du dernier devoir.',
    grandeurs: [{
      nom: 'Moyenne',
      expressions: ['$\\frac{£a+£b+x}{3}$', '$\\frac{x}{3}$', '$£c$', '$x$'],
      aide: 'Pour calculer une moyenne, on divise la somme des notes par le nombre de notes'
    }],
    equation: { membregauche: ['$\\frac{£a+£b+x}{3}$', '$\\frac{x}{3}$', '$£c$', '$x$'], membredroite: ['$£c$', '$\\frac{£a+£b+x}{3}$', '$\\frac{x}{3}$', '$x$'] },
    num: 0
  },
  {
    figure: '',
    diff: false,
    enonce: 'Elsa achète $£a$ assiettes plates, $£b$ assiettes creuses et $£c$ assiettes à dessert. <br> Une assiette creuse coûte $£d$ € de moins qu’une assiette plate. <br> Une assiette à dessert coûte $£e$ € de moins qu’une assiette plate. <br> Elle dépense en tout $£f$ €. <br> Quel est le prix de chaque sorte d’assiette ? ',
    choix: 'On appelle <i>x</i> le prix d’une assiette plate.',
    grandeurs: [
      {
        nom: 'Prix d’une assiette creuse',
        expressions: ['$x - £d$', '$x - £e$', '$£f$', '$£a \\times x$'],
        aide: 'Une assiette creuse coûte  $£d$ € de moins qu’une assiette plate.'
      },
      {
        nom: 'Prix d’une assiette à dessert',
        expressions: ['$x - £e$', '$x - £d$', '$£f$', '$£a \\times x$'],
        aide: 'Une assiette à dessert coûte  $£e$ € de moins qu’une assiette plate.'
      },
      {
        nom: 'Prix total',
        expressions: ['$£ax +£b(x - £d) + £c(x - £e)$', '$£bx +£a(x - £d) + £c(x - £e)$', '$£cx +£b(x - £d) + £a(x - £e)$', '$£f$'],
        aide: 'Elsa achète $£a$ assiettes plates, $£b$ assiettes creuses et $£c$ assiettes à dessert !'
      }

    ],
    equation: { membregauche: ['$£ax +£b(x - £d) + £c(x - £e)$', '$£bx +£a(x - £d) + £c(x - £e)$', '$£cx +£b(x - £d) + £a(x - £e)$', '$x$'], membredroite: ['$£f$', '$£a$', '$£b$', '$£c$'] },
    num: 1
  },
  {
    figure: '',
    diff: false,
    enonce: 'La somme des âges de Marie, de sa mère et de sa grand-mère est $£a$ ans. <br> La grand-mère a le double de l’âge de la mère <br> et l’âge de Marie est le tiers de celui de sa mère.<br> Quel est l’âge de chacune ?',
    choix: "On appelle <i>x</i> l'âge de la mère.",
    grandeurs: [{
      nom: 'âge de la grand-mère',
      expressions: ['$2x$', '$\\frac{x}{2}$', '$\\frac{x}{3}$', '$x+2$'],
      aide: 'La grand-mère a le double de l’âge de la mère '
    },
    {
      nom: 'âge de Marie',
      expressions: ['$\\frac{x}{3}$', '$\\frac{x}{2}$', '$x-3$', '$x+2$'],
      aide: 'L’âge de Marie est le tiers de celui de sa mère '
    },
    {
      nom: 'somme des trois âges',
      expressions: ['$\\frac{x}{3} + x + 2x$', '$3x$', '$\\frac{x}{3} - x - 2x$', '$\\frac{x + x + 2x}{3} $'],
      aide: "Il faut ajouter l'âge de Marie, l'êge de sa mère et l'êge de sa grand-mère !"
    }
    ],
    equation: { membregauche: ['$\\frac{x}{3} + x + 2x$', '$\\frac{x}{3} - x - 2x$', '$3x$', '$£a$'], membredroite: ['$£a$', '$\\frac{x}{3} + x + 2x$', '$3x$', '$x$'] },
    num: 2
  },
  {
    figure: '',
    diff: false,
    enonce: 'Pierre dit : «il y a $£a$ ans, j’avais la moitié de l’âge que j’aurai dans $£b$ ans.<br> Quel est l’âge de Pierre ?',
    choix: "On appelle <i>x</i> l'âge de Pierre.",
    grandeurs: [{
      nom: 'âge de Pierre il y a $£a$ ans',
      expressions: ['$x - £a$', '$x + £a$', '$£ax$', '$\\frac{£ax}{2}$'],
      aide: "Il faut retirer $£a$ ans à l'âge de Pierre !"
    },
    {
      nom: 'âge de Pierre dans $£b$ ans',
      expressions: ['$x + £b$', '$x - £b$', '$£bx$', '$2 \\times £bx$'],
      aide: "Il faut ajouter $£b$ ans à l'âge de Pierre !"
    },
    {
      nom: 'la moitié de l’âge de Pierre dans $£b$ ans',
      expressions: ['$\\frac{x + £b}{2}$', '$2 \\times (x + £b)$', '$\\frac{x + £b}{3}$', '$3 \\times (x + £b)$'],
      aide: "Il faut diviser par deux l'âge de Pierre dans $£b$ ans !"
    }
    ],
    equation: { membregauche: ['$x - £a$', '$\\frac{x + £b}{2}$', '$£a$', '$£b$'], membredroite: ['$\\frac{x + £b}{2}$', '$x - £a$', '$£a$', '$£b$'] },
    num: 3
  },
  {
    figure: '',
    diff: true,
    enonce: 'Christian dépense $£a$ d’une somme puis les $£b$ du reste. <br> Finalement, il lui reste $£d$ euros. <br> Quelle était la somme initiale ?',
    choix: 'On appelle <i>x</i> la somme initiale.',
    grandeurs: [{
      nom: 'La première dépense',
      expressions: ['$£ax$', '$£a+x$', '$£a-x$', '$£bx$'],
      aide: 'Christian dépense $£a$ d’une somme !'
    },
    {
      nom: 'Le reste après la première dépense',
      expressions: ['$£cx$', '$x + £b$', '$x + £ax$', '$x - £cx$'],
      aide: 'Il faut retirer la dépense à la somme de départ !'
    },
    {
      nom: 'La deuxième dépense',
      expressions: ['$£b \\times £cx$', '$£b + £cx$', '$£b - £cx$', '$£b \\times £ax$'],
      aide: 'Il a dépensé les $£b$ du reste de la première dépense !'
    },
    {
      nom: 'Le reste après la deuxième dépense',
      expressions: ['$£cx - £b \\times £cx$', '$£d$', '$x - £ax \\times £b \\times £cx$', '$x - £b \\times £ax$'],
      aide: 'Il faut retirer la deuxème dépense au premier reste !'
    }
    ],
    equation: { membregauche: ['$£cx - £b \\times £cx$', '$£d$', '$£a$', '$x - £b \\times £ax$'], membredroite: ['$£d$', '$£cx - £b \\times £cx$', '$x - £b \\times £ax$', '$£b$'] },
    num: 4
  },
  {
    figure: '',
    diff: false,
    enonce: 'Deux enfants ont ensemble $£a$ €. <br>L’un des deux enfants a $£b$ € de plus que l’autre. <br>Combien a chaque enfant ?',
    choix: 'On appelle <i>x</i> la plus petite des deux sommes d’argent.',
    grandeurs: [{
      nom: 'L’autre somme d’argent',
      expressions: ['$x + £b$', '$x - £b$', '$£a$', '$£a - £b$'],
      aide: 'L’un des deux enfants a $£b$ € de plus que l’autre !'
    },
    {
      nom: 'La somme totale',
      expressions: ['$x + x + £b$', '$x + £b$', '$£a - £b$', '$£a - x - £b$'],
      aide: 'Il faut ajouter les deux sommes !'
    }
    ],
    equation: { membregauche: ['$x + x + £b$', '$£a$', '$£b$', '$£a - £b$'], membredroite: ['$£a$', '$x + x + £b$', '$£b$', '$£b + £a$'] },
    num: 5
  },
  {
    figure: '',
    diff: false,
    enonce: 'Cindy, Wardia et Kevin se sont partagés $£a$ bonbons. <br> Cindy a pris $£b$ fois plus  de bonbons que Wardia <br> et Kevin a pris $£c$ bonbons de plus que Cindy. <br>Combien ont-ils de bonbons chacun ?',
    choix: 'On appelle <i>x</i> le nombre de bonbons de Wardia.',
    grandeurs: [{
      nom: 'Nombre de bonbons de Cindy',
      expressions: ['$£bx$', '$x + £b$', '$£a$', '$£a + x$'],
      aide: 'Cindy a pris $£b$ fois plus  de bonbons que Wardia !'
    },
    {
      nom: 'Nombre de bonbons de Kevin',
      expressions: ['$£bx + £c$', '$x + £c$', '$£bx \\times £c$', '$£a - x - £b$'],
      aide: 'Kevin a pris $£c$ bonbons de plus que Cindy !'
    },
    {
      nom: 'Nombre total de bonbons',
      expressions: ['$x + £bx + £bx + £c$', '$£a$', '$x + £bx + £c$', '$x + £bx + £bx$'],
      aide: 'Il faut ajouter les bonbons de Wardia, de Cindy et de Kevin !'
    }
    ],
    equation: { membregauche: ['$x + £bx + £bx + £c$', '$£a$', '$£bx + £bx + £c$', '$£a - £b$'], membredroite: ['$£a$', '$x + £bx + £bx + £c$', '$£b$', '$£b + £a$'] },
    num: 6
  },
  {
    figure: '',
    diff: true,
    enonce: 'Si on augmente de $£b$ m un côté d’un carré et si on diminue de $£a$ m l’autre côté,<br> on obtient un rectangle  de même aire que celle du carré. <br>Combien mesure le côté de ce carré ?',
    choix: 'On appelle <i>x</i> la longueur du côté du carré.',
    grandeurs: [{
      nom: 'Aire du carré',
      expressions: ['$x²$', '$£ax$', '$x + £a$', '$x - £b$'],
      aide: 'Aire d’un carré = côté² !'
    },
    {
      nom: 'Longueur du rectangle',
      expressions: ['$x + £b$', '$x - £b$', '$£b - £a$', '$£a \\times £a$'],
      aide: 'On augmente de $£b$ m un côté d’un carré !'
    },
    {
      nom: 'Largeur du rectangle',
      expressions: ['$x - £a$', '$x - £b$', '$x + £a$', '$£b \\times £a$'],
      aide: 'On diminue de $£a$ m l’autre côté du carré !'
    },
    {
      nom: 'Aire du rectangle',
      expressions: ['$(x + £b)(x - £a)$', '$(x + £b)²$', '$2 \\times (x + £b + x - £a) $', '$£b \\times £a$'],
      aide: 'Aire d’un rectangle = longueur × largeur !'
    }
    ],
    equation: { membregauche: ['$x²$', '$(x + £b)(x - £a)$', '$(x - £a)$', '$£a - £b$'], membredroite: ['$(x + £b)(x - £a)$', '$x²$', '$(x + £b)$', '$x$'] },
    num: 7
  },
  {
    figure: 'TWF0aEdyYXBoSmF2YTEuMAAAABM+TMzNAAJmcv###wEA#wEAAAAAAAAAAANLAAAChwAAAQEAAAAAAAAAAQAAADf#####AAAAAQAKQ0NhbGNDb25zdAD#####AAJwaQAWMy4xNDE1OTI2NTM1ODk3OTMyMzg0Nv####8AAAABAApDQ29uc3RhbnRlQAkh+1RELRj#####AAAAAQAKQ1BvaW50QmFzZQD#####AQAAAAAOAAFVAMAkAAAAAAAAQBAAAAAAAAAAAAUAAEAvmBBiTdLyQC+YEGJN0vL#####AAAAAQAUQ0Ryb2l0ZURpcmVjdGlvbkZpeGUA#####wEAAAAAEAAAAQAAAAEAAAABAT#wAAAAAAAA#####wAAAAEAD0NQb2ludExpZURyb2l0ZQD#####AQAAAAAOAAFWAMAAAAAAAAAAQBAAAAAAAAAAAAUAAUA#mBBiTdLyAAAAAv####8AAAABAAhDU2VnbWVudAD#####AQAAAAAQAAABAAAAAQAAAAEAAAAD#####wAAAAEAB0NNaWxpZXUA#####wEAAAAAEAAAAAAAAAAAAAAAQAgAAAAAAAAAAAUAAAAAAQAAAAP#####AAAAAgAMQ0NvbW1lbnRhaXJlAP####8BAAAAAAAAAAAAAAAAQBgAAAAAAAAAAAAAAAUMAAAAAAABAAAAAAAAAAEAAAAAAAAAAAABMf####8AAAABAAlDTG9uZ3VldXIA#####wAAAAEAAAADAAAAAgD#####AAAAAAAQAAAAQAgAAAAAAAAAAAAAAAAAAAAABQAAQFxAAAAAAABAfN4UeuFHrv####8AAAABAAdDQ2FsY3VsAP####8ABW1pbmkxAAExAAAAAT#wAAAAAAAAAAAACQD#####AAVtYXhpMQACMTAAAAABQCQAAAAAAAD#####AAAAAQAUQ0ltcGxlbWVudGF0aW9uUHJvdG8A#####wAHQ3Vyc2V1cgAAAAUAAAAFAAAAAwAAAAkAAAAKAAAACAAAAAMAAAAACwEAAAAAEAAAAQAAAAEAAAAIAT#wAAAAAAAAAAAABAEAAAALAAAAAAAQAAAAwAgAAAAAAAA#8AAAAAAAAAAABQAAQF1AAAAAAAAAAAAM#####wAAAAEAC0NIb21vdGhldGllAAAAAAsAAAAI#####wAAAAEACkNPcGVyYXRpb24D#####wAAAAEAD0NSZXN1bHRhdFZhbGV1cgAAAAkAAAAMAQAAAA0AAAAJAAAADQAAAAr#####AAAAAQALQ1BvaW50SW1hZ2UAAAAACwEAAAAADQACTzEAwBAAAAAAAABAEAAAAAAAAAAABQAAAAANAAAADgAAAAsAAAAACwAAAAgAAAAMAwAAAAwBAAAAAT#wAAAAAAAAAAAADQAAAAkAAAAMAQAAAA0AAAAKAAAADQAAAAkAAAAOAAAAAAsBAAAAAA0AAkkxAMAAAAAAAAAAQAgAAAAAAAAAAAUAAAAADQAAABAAAAAFAQAAAAsAAAAAABAAAAEAAAABAAAACAAAAA0AAAAEAQAAAAsAAAAAARAAAmsxAMAAAAAAAAAAQAAAAAAAAAAAAAEAAT#c#c#c#c#dAAAAEv####8AAAACAA9DTWVzdXJlQWJzY2lzc2UBAAAACwACYTEAAAAPAAAAEQAAABP#####AAAAAQAPQ1ZhbGV1ckFmZmljaGVlAQAAAAsAAAAAAAAAAAAAAAAAwBgAAAAAAAAAAAAAABMPAAH###8AAAABAAAAAgAAAAEAAAAAAAAAAAAAAAAAAAAAFAAAAAkA#####wACQTEAAmExAAAADQAAABQAAAACAP####8BAAAAABAAAAAAAAAAAAAAAEAIAAAAAAAAAAAFAAFARwAAAAAAAEBF8KPXCj1wAAAAAwD#####AQAAAAEQAAABAAAAAQAAABcAP#AAAAAAAAAAAAAEAP####8BAAAAABAAAAAAAAAAAAAAAEAIAAAAAAAAAAAFAAFAVkAAAAAAAAAAABgAAAADAP####8BAAAAARAAAAEAAAABAAAAGQE#8AAAAAAAAP####8AAAABAAlDQ2VyY2xlT0EA#####wEAAAAAAAABAAAAGQAAABf#####AAAAAQAQQ0ludERyb2l0ZUNlcmNsZQD#####AAAAGgAAABv#####AAAAAQAQQ1BvaW50TGllQmlwb2ludAD#####AQAAAAAQAAAAAAAAAAAAAABACAAAAAAAAAAABQACAAAAHAAAABMA#####wEAAAAAEAAAAAAAAAAAAAAAQAgAAAAAAAAAAAUAAQAAABwAAAARAP####8BAAAAAAAAAQAAAB4AAAAZAAAAEQD#####AQAAAAAAAAEAAAAXAAAAGf####8AAAABAB5DQXV0cmVQb2ludEludGVyc2VjdGlvbkNlcmNsZXMA#####wEAAAAAEAAAAAAAAAAAAAAAQAgAAAAAAAAAAAUAAAAAIAAAAB8AAAAZAAAABQD#####AAAAAAAQAAABAAAAAgAAABcAAAAZAAAABQD#####AAAAAAAQAAABAAAAAgAAABkAAAAeAAAABQD#####AAAAAAAQAAABAAAAAgAAAB4AAAAhAAAABQD#####AAAAAAAQAAABAAAAAgAAACEAAAAXAAAAAgD#####AQAAAAAQAAAAAAAAAAAAAABACAAAAAAAAAAABQABQEAAAAAAAABAVHhR64UeuAAAAAcA#####wAAAAABAAAAAAAmDgAAAAAAAQAAAAEAAAABAAAAAAAAAAAABSNJI0d4#####wAAAAEAC0NNZWRpYXRyaWNlAP####8BAAAAABAAAAEAAAABAAAAIQAAAB4AAAAEAP####8BAAAAABAAAAAAAAAAAAAAAEAIAAAAAAAAAAAFAAG#8#R+j9H6PwAAACgAAAAFAP####8AAAAAABAAAAEAAAACAAAAIQAAACkAAAAFAP####8AAAAAABAAAAEAAAACAAAAHgAAACkAAAAHAP####8AAAAAAMAAAAAAAAAAwBQAAAAAAAAABGxldDEAAAAXEAAAAAAAAgAAAAIAAAABAAAAAAAAAAAAAUEAAAAHAP####8AAAAAAEAIAAAAAAAAwBAAAAAAAAAABGxldDIAAAAhEAAAAAAAAQAAAAIAAAABAAAAAAAAAAAAAUIAAAAHAP####8AAAAAAEAcAAAAAAAAAAAAAAAAAAAABGxldDMAAAApEAAAAAAAAAAAAAEAAAABAAAAAAAAAAAAAUUAAAAHAP####8AAAAAAEAYAAAAAAAAQAAAAAAAAAAABGxldDQAAAAeEAAAAAAAAQAAAAAAAAABAAAAAAAAAAAAAUMAAAAHAP####8AAAAAAMAIAAAAAAAAAAAAAAAAAAAABGxldDUAAAAZEAAAAAAAAgAAAAAAAAABAAAAAAAAAAAAAUQAAAAGAP####8BAAAAABAAAAAAAAAAAAAAAEAIAAAAAAAAAAAFAAAAACEAAAApAAAABgD#####AQAAAAAQAAAAAAAAAAAAAABACAAAAAAAAAAABQAAAAAeAAAAKf####8AAAACAAlDQ2VyY2xlT1IA#####wAAAAAAAAACAAAAMQAAAAE#yZmZmZmZmgAAAAAWAP####8AAAAAAAAAAgAAADIAAAABP8mZmZmZmZoAAAAAAgD#####AQAAAAAQAAAAAAAAAAAAAABACAAAAAAAAAAABQABQGggAAAAAABAQPCj1wo9cAAAAAcA#####wAAAAABAAR2YWwxAAAANRAAAAAAAAAAAAAAAAAAAQAAAAAAAAAAAAsjVmFsKEExKSBjbQAAAAf##########w==',
    diff: false,
    enonce: 'Quelle valeur doit-on donner à <i>x</i> pour que le carré<br> et le triangle isocèle aient le même périmètre ?',
    choix: '',
    grandeurs: [{
      nom: 'Périmètre du carré',
      expressions: ['$4x$', '$x²$', '$x + £a$', '$x - £a$'],
      aide: 'Le périmètre d’un carré est la longueur de son contour !'
    },
    {
      nom: 'Périmètre du triangle',
      expressions: ['$x + £b$', '$x + 2 \\times £b$', '$x + £a$', '$4x$'],
      aide: 'Le périmètre d’un triangle est la longueur de son contour !'
    }
    ],
    equation: { membregauche: ['$4x$', '$x - £a$', '$x²$', '$£a - £b$'], membredroite: ['$x + £b$', '$x²$', '$x + 2 \\times £b$', '$x$'] },
    num: 8
  },
  {
    figure: 'TWF0aEdyYXBoSmF2YTEuMAAAABM+TMzNAAJmcv###wEA#wEAAAAAAAAAAANLAAAChwAAAQEAAAAAAAAAAQAAAD3#####AAAAAQAKQ0NhbGNDb25zdAD#####AAJwaQAWMy4xNDE1OTI2NTM1ODk3OTMyMzg0Nv####8AAAABAApDQ29uc3RhbnRlQAkh+1RELRj#####AAAAAQAKQ1BvaW50QmFzZQD#####AQAAAAAOAAFVAMAkAAAAAAAAQBAAAAAAAAAAAAUAAEAvmBBiTdLyQC+YEGJN0vL#####AAAAAQAUQ0Ryb2l0ZURpcmVjdGlvbkZpeGUA#####wEAAAAAEAAAAQAAAAEAAAABAT#wAAAAAAAA#####wAAAAEAD0NQb2ludExpZURyb2l0ZQD#####AQAAAAAOAAFWAMAAAAAAAAAAQBAAAAAAAAAAAAUAAUA#mBBiTdLyAAAAAv####8AAAABAAhDU2VnbWVudAD#####AQAAAAAQAAABAAAAAQAAAAEAAAAD#####wAAAAEAB0NNaWxpZXUA#####wEAAAAAEAAAAAAAAAAAAAAAQAgAAAAAAAAAAAUAAAAAAQAAAAP#####AAAAAgAMQ0NvbW1lbnRhaXJlAP####8BAAAAAAAAAAAAAAAAQBgAAAAAAAAAAAAAAAUMAAAAAAABAAAAAAAAAAEAAAAAAAAAAAABMf####8AAAABAAlDTG9uZ3VldXIA#####wAAAAEAAAADAAAAAgD#####AAAAAAAQAAAAQAgAAAAAAAAAAAAAAAAAAAAABQAAQFxAAAAAAABAfN4UeuFHrv####8AAAABAAdDQ2FsY3VsAP####8ABW1pbmkxAAExAAAAAT#wAAAAAAAAAAAACQD#####AAVtYXhpMQACMTAAAAABQCQAAAAAAAD#####AAAAAQAUQ0ltcGxlbWVudGF0aW9uUHJvdG8A#####wAHQ3Vyc2V1cgAAAAUAAAAFAAAAAwAAAAkAAAAKAAAACAAAAAMAAAAACwEAAAAAEAAAAQAAAAEAAAAIAT#wAAAAAAAAAAAABAEAAAALAAAAAAAQAAAAwAgAAAAAAAA#8AAAAAAAAAAABQAAQF1AAAAAAAAAAAAM#####wAAAAEAC0NIb21vdGhldGllAAAAAAsAAAAI#####wAAAAEACkNPcGVyYXRpb24D#####wAAAAEAD0NSZXN1bHRhdFZhbGV1cgAAAAkAAAAMAQAAAA0AAAAJAAAADQAAAAr#####AAAAAQALQ1BvaW50SW1hZ2UAAAAACwEAAAAADQACTzEAwBAAAAAAAABAEAAAAAAAAAAABQAAAAANAAAADgAAAAsAAAAACwAAAAgAAAAMAwAAAAwBAAAAAT#wAAAAAAAAAAAADQAAAAkAAAAMAQAAAA0AAAAKAAAADQAAAAkAAAAOAAAAAAsBAAAAAA0AAkkxAMAAAAAAAAAAQAgAAAAAAAAAAAUAAAAADQAAABAAAAAFAQAAAAsAAAAAABAAAAEAAAABAAAACAAAAA0AAAAEAQAAAAsAAAAAARAAAmsxAMAAAAAAAAAAQAAAAAAAAAAAAAEAAT#c#c#c#c#dAAAAEv####8AAAACAA9DTWVzdXJlQWJzY2lzc2UBAAAACwACYTEAAAAPAAAAEQAAABP#####AAAAAQAPQ1ZhbGV1ckFmZmljaGVlAQAAAAsAAAAAAAAAAAAAAAAAwBgAAAAAAAAAAAAAABMPAAH###8AAAABAAAAAgAAAAEAAAAAAAAAAAAAAAAAAAAAFAAAAAkA#####wACQTEAAmExAAAADQAAABQAAAACAP####8BAAAAABAAAAAAAAAAAAAAAEAIAAAAAAAAAAAFAAFARwAAAAAAAEBF8KPXCj1wAAAAAwD#####AQAAAAEQAAABAAAAAQAAABcAP#AAAAAAAAAAAAAEAP####8BAAAAABAAAAAAAAAAAAAAAEAIAAAAAAAAAAAFAAFAVkAAAAAAAAAAABgAAAADAP####8BAAAAARAAAAEAAAABAAAAGQE#8AAAAAAAAP####8AAAABAAlDQ2VyY2xlT0EA#####wEAAAAAAAABAAAAGQAAABf#####AAAAAQAQQ0ludERyb2l0ZUNlcmNsZQD#####AAAAGgAAABv#####AAAAAQAQQ1BvaW50TGllQmlwb2ludAD#####AQAAAAAQAAAAAAAAAAAAAABACAAAAAAAAAAABQACAAAAHAAAABMA#####wEAAAAAEAAAAAAAAAAAAAAAQAgAAAAAAAAAAAUAAQAAABwAAAARAP####8BAAAAAAAAAQAAAB4AAAAZAAAAEQD#####AQAAAAAAAAEAAAAXAAAAGf####8AAAABAB5DQXV0cmVQb2ludEludGVyc2VjdGlvbkNlcmNsZXMA#####wEAAAAAEAAAAAAAAAAAAAAAQAgAAAAAAAAAAAUAAAAAIAAAAB8AAAAZAAAABQD#####AAAAAAAQAAABAAAAAgAAABcAAAAZAAAABQD#####AAAAAAAQAAABAAAAAgAAABkAAAAeAAAABQD#####AAAAAAAQAAABAAAAAgAAAB4AAAAhAAAABQD#####AAAAAAAQAAABAAAAAgAAACEAAAAXAAAAAgD#####AQAAAAAQAAAAAAAAAAAAAABACAAAAAAAAAAABQABQEAAAAAAAABAVHhR64UeuAAAAAcA#####wAAAAABAAAAAAAmDgAAAAAAAQAAAAEAAAABAAAAAAAAAAAABSNJI0d4AAAABwD#####AAAAAADAAAAAAAAAAMAUAAAAAAAAAARsZXQxAAAAFxAAAAAAAAIAAAACAAAAAQAAAAAAAAAAAAFBAAAABwD#####AAAAAABACAAAAAAAAMAQAAAAAAAAAARsZXQyAAAAIRAAAAAAAAEAAAACAAAAAQAAAAAAAAAAAAFCAAAABwD#####AAAAAABAGAAAAAAAAEAAAAAAAAAAAARsZXQ0AAAAHhAAAAAAAAEAAAAAAAAAAQAAAAAAAAAAAAFDAAAABwD#####AAAAAADACAAAAAAAAAAAAAAAAAAAAARsZXQ1AAAAGRAAAAAAAAIAAAAAAAAAAQAAAAAAAAAAAAFEAAAAAgD#####AQAAAAAQAAAAAAAAAAAAAABACAAAAAAAAAAABQABQGiAAAAAAABANeFHrhR64gAAAAcA#####wAAAAABAAR2YWwxAAAALBAAAAAAAAAAAAAAAAAAAQAAAAAAAAAAAAsjVmFsKEExKSBjbf####8AAAABAAlDRHJvaXRlQUIA#####wEAAAAAEAAAAQAAAAIAAAAXAAAAIQAAAAQA#####wEAAAAAEAABRQAAAAAAAAAAAEAIAAAAAAAAAAAFAAFABzDmHMOYcwAAAC4AAAAFAP####8AAAAAABAAAAEAAAACAAAAHgAAAC8AAAAEAP####8BAAAAABAAAAAAAAAAAAAAAEAIAAAAAAAAAAAFAAE#8ihFCKEUIwAAAC4AAAARAP####8BAAAAAAAAAgAAACEAAAAxAAAAEgD#####AAAAJAAAADIAAAATAP####8AAAAAABAAAAAAAAAAAAAAAEAIAAAAAAAAAAAFAAEAAAAzAAAAEwD#####AQAAAAAQAAAAAAAAAAAAAABACAAAAAAAAAAABQACAAAAM#####8AAAABABBDRHJvaXRlUGFyYWxsZWxlAP####8BAAAAABAAAAEAAAACAAAANQAAAC4AAAAWAP####8BAAAAABAAAAEAAAACAAAAMQAAACL#####AAAAAQAQQ0ludERyb2l0ZURyb2l0ZQD#####AQAAAAAQAAAAAAAAAAAAAABACAAAAAAAAAAABQAAAAA3AAAANgAAAAUA#####wAAAAAAEAAAAQAAAAIAAAAhAAAALwAAAAUA#####wAAAAAAEAAAAQAAAAEAAAA1AAAAOAAAAAUA#####wAAAAAAEAAAAQAAAAEAAAA4AAAAMQAAAAcA#####wAAAAAAQBAAAAAAAAAAAAAAAAAAAAAAAAAALxAAAAAAAAAAAAAAAAAAAQAAAAAAAAAAAAFFAAAAB###########',
    diff: false,
    enonce: 'Quelle valeur doit-on donner à <i>x</i> pour que le carré<br> et le triangle rectangle aient la même aire ?',
    choix: '',
    grandeurs: [{
      nom: 'Aire du carré',
      expressions: ['$x²$', '$4x$', '$x + £a$', '$x - £a$'],
      aide: 'Aire du carré = côté² !'
    },
    {
      nom: 'Aire du triangle rectangle',
      expressions: ['$\\frac{£a x}{2}$', '$x + £a$', '$x² + £a²$', '$4x$'],
      aide: 'Aire d’un triangle rectangle = $\\frac{\\text{produit des côtés de l’angle droit}}{2}$ !'
    }
    ],
    equation: { membregauche: ['$x²$', '$x - £a$', '$4x$', '$£a - £b$'], membredroite: ['$\\frac{£a x}{2}$', '$x²$', '$x + 2 \\times £b$', '$x$'] },
    num: 9
  }
]

/**
 * section miseeneq01
 * @this {Parcours}
 */
export default function main () {
  const me = this
  const ds = me.donneesSection
  const stor = this.storage

  /**
   * Remplace £a par stor.a, etc.
   * @param {string} texte
   * @return {string}
   */
  function substitueVars (texte) {
    if (typeof texte !== 'string') {
      console.error(Error('param invalide : ' + typeof texte))
      return ''
    }
    return texte
      .replace(/£a/g, stor.a)
      .replace(/£b/g, stor.b)
      .replace(/£c/g, stor.c)
      .replace(/£d/g, stor.d)
      .replace(/£e/g, stor.e)
      .replace(/£f/g, stor.f)
  }

  function poseQuestion () {
    stor.indexGrandeur = 0
    stor.lespoints = 0
    stor.hitori.push({ t: 'passe dans poseQuestion', indexGrandeur: stor.indexGrandeur })
    if (stor.exo.formule.figure !== '') {
      const idSvg = j3pGetNewId()
      j3pCreeSVG(stor.lesdiv.figure, { id: idSvg, width: 400, height: 150 })
      // stor.lesdiv.figure.style.border = 'solid black 1px'
      stor.mtgAppLecteur.removeAllDoc()
      stor.mtgAppLecteur.addDoc(idSvg, stor.exo.formule.figure, true)
      stor.mtgAppLecteur.calculateAndDisplayAll(true)
      stor.mtgAppLecteur.setText(idSvg, '#val1', stor.a + ' cm', true)
    }
    j3pAffiche(stor.lesdiv.consigneX, null, '<b>On désire mettre ce problème en équation:</b>\n')
    j3pAffiche(stor.lesdiv.consigneX, null, stor.exo.formule.enonce, { a: stor.a, b: stor.b, c: stor.c, d: stor.d, e: stor.e, f: stor.f })
    j3pAffiche(stor.lesdiv.etape, null, '<b><u>Étape</u>: Exprimer en fonction de <i>x</i></b>')
    stor.encours = 'grand'
    stor.lesdiv.grop = addDefaultTable(stor.lesdiv.travail, 2, 1)
    j3pAffiche(stor.lesdiv.grop[0][0], null, '<b>' + stor.exo.formule.choix + '</b>')
    stor.lesdiv.gro = addDefaultTable(stor.lesdiv.grop[1][0], stor.exo.formule.grandeurs.length + 1, 1)
    poseGrand()
  }

  function poseGrand () {
    me.essaiCourant = 0
    stor.hitori.push({ t: 'passe dans poseGrand', indexGrandeur: stor.indexGrandeur })
    stor.lesdiv.solution.classList.remove('correction')
    stor.lesdiv.explications.classList.remove('explique')
    if (stor.indexGrandeur > 0) {
      if (stor.cfo === true) {
        j3pEmpty(stor.lesdiv.grandAZ[0][1])
        j3pEmpty(stor.lesdiv.grandA[0][1])
        j3pAffiche(stor.lesdiv.grandAZ[0][2], null, stor.lesrp)
        stor.cfo = false
      }
    }
    j3pEmpty(stor.lesdiv.explications)
    j3pEmpty(stor.lesdiv.solution)
    stor.lesdiv.grandA = addDefaultTable(stor.lesdiv.gro[stor.indexGrandeur][0], 2, 2)
    stor.lesdiv.grandAZ = addDefaultTable(stor.lesdiv.grandA[0][0], 1, 3)
    try {
      const grandeur = stor.exo.formule.grandeurs[stor.indexGrandeur]
      if (!grandeur) throw Error(`L’index ${stor.indexGrandeur} ne correspond à aucune grandeur`)
      const contenu = substitueVars(grandeur.nom)
      j3pAffiche(stor.lesdiv.grandAZ[0][0], null, contenu + ' $=$ ')
      let ll = []
      for (const expression of grandeur.expressions) {
        ll.push(substitueVars(expression))
      }
      stor.lesrp = ll[0]
      ll = j3pShuffle(ll)
      ll.splice(0, 0, 'Choisir')
      stor.liste = ListeDeroulante.create(stor.lesdiv.grandA[0][1], ll, { centre: true })
      stor.lesdiv.grandAZ[0][2].style.color = me.styles.toutpetit.correction.color
      stor.lesdiv.grandAZ[0][1].style.color = me.styles.cfaux
      stor.aide = substitueVars(grandeur.aide)
    } catch (error) {
      stor.encours = 'fin'
      j3pNotify(error, { histori: stor.hitori })
    }
  }

  function faisFin () {
    me.essaiCourant = 0
    stor.lesdiv.solution.classList.remove('correction')
    stor.lesdiv.explications.classList.remove('explique')
    stor.encours = 'fin'
    j3pEmpty(stor.lesdiv.explications)
    j3pEmpty(stor.lesdiv.solution)
    if (stor.cfo === true) {
      j3pEmpty(stor.lesdiv.grandAZ[0][1])
      j3pEmpty(stor.lesdiv.grandA[0][1])
      j3pAffiche(stor.lesdiv.grandAZ[0][2], null, stor.lesrp)
    }
    stor.lespoints = stor.indexGrandeur = 1
    let j
    stor.reppos = []
    j3pEmpty(stor.lesdiv.etape)
    j3pAffiche(stor.lesdiv.etape, null, '<b><u>Étape</u>: Mettre le problème en équation</b>')
    stor.lesdiv.fin = addDefaultTable(stor.lesdiv.gro[stor.exo.formule.grandeurs.length][0], 2, 1)
    j3pAffiche(stor.lesdiv.fin[0][0], null, '<i>x</i> doit être une solution de l’équation suivante:')
    stor.lesdiv.fin2 = addDefaultTable(stor.lesdiv.fin[1][0], 1, 3)
    j3pAffiche(stor.lesdiv.fin2[0][1], null, '$=$')
    let ll = []
    for (j = 0; j < stor.exo.formule.equation.membregauche.length; j++) {
      ll.push(stor.exo.formule.equation.membregauche[j].replace(/£a/g, stor.a).replace(/£b/g, stor.b).replace(/£c/g, stor.c).replace(/£d/g, stor.d).replace(/£e/g, stor.e).replace(/£f/g, stor.f))
    }
    stor.reppos.push(ll[0])
    ll = j3pShuffle(ll)
    ll.splice(0, 0, 'Choisir')
    stor.list1 = ListeDeroulante.create(stor.lesdiv.fin2[0][0], ll, { centre: true })
    ll = []
    for (j = 0; j < stor.exo.formule.equation.membredroite.length; j++) {
      ll.push(stor.exo.formule.equation.membredroite[j].replace(/£a/g, stor.a).replace(/£b/g, stor.b).replace(/£c/g, stor.c).replace(/£d/g, stor.d).replace(/£e/g, stor.e).replace(/£f/g, stor.f))
    }
    stor.reppos.push(ll[0])
    ll = j3pShuffle(ll)
    ll.splice(0, 0, 'Choisir')
    stor.list2 = ListeDeroulante.create(stor.lesdiv.fin2[0][2], ll, { centre: true })
  }

  function creeLesDiv () {
    stor.lesdiv = {}
    stor.lesdiv.conteneur = j3pAddElt(me.zones.MG, 'div', {
      style: me.styles.etendre('toutpetit.enonce', { margin: '10px' })
    })
    const tt = addDefaultTable(stor.lesdiv.conteneur, 1, 1)
    stor.lesdiv.consigneG = j3pAddElt(tt[0][0], 'div')
    stor.tbax = addDefaultTable(stor.lesdiv.consigneG, 2, 1)
    stor.lesdiv.consigneX = stor.tbax[0][0]
    stor.lesdiv.figure = stor.tbax[1][0]
    stor.lesdiv.etape = j3pAddElt(stor.lesdiv.conteneur, 'div')
    stor.lesdiv.travail = addDefaultTable(stor.lesdiv.conteneur, 1, 1)[0][0]
    stor.lesdiv.explications = j3pAddElt(stor.lesdiv.conteneur, 'div')
    stor.lesdiv.solution = j3pAddElt(stor.lesdiv.conteneur, 'div')
    stor.lesdiv.calculatrice = j3pAddElt(me.zonesElts.MD, 'div', {
      style: me.styles.etendre('toutpetit.enonce', { margin: '10px' })
    })
    stor.lesdiv.correction = j3pAddElt(me.zonesElts.MD, 'div', {
      style: me.styles.etendre('toutpetit.enonce', { margin: '10px' })
    })
    stor.lesdiv.etape.style.color = me.styles.toutpetit.correction.color
    stor.lesdiv.solution.style.color = me.styles.toutpetit.correction.color
    stor.lesdiv.explications.style.color = me.styles.cfaux
    me.zonesElts.MG.classList.add('fond')
    stor.lesdiv.travail.classList.add('travail')
    stor.lesdiv.consigneG.classList.add('enonce')
  }
  function faisChoixExos () {
    const e = ds.exos.pop()
    switch (e.formule.num) {
      case 0:
        stor.a = j3pGetRandomInt(10, 19)
        stor.b = j3pGetRandomInt(10, 19)
        stor.x = 15 + (3 - (stor.a + stor.b) % 3)
        stor.c = Math.round((stor.a + stor.b + stor.x) / 3)
        break
      case 1:
        stor.a = j3pGetRandomInt(20, 23)
        stor.b = j3pGetRandomInt(9, 13)
        stor.c = j3pGetRandomInt(9, 13)
        stor.d = j3pGetRandomInt(1, 3)
        stor.e = j3pGetRandomInt(4, 6)
        stor.x = j3pGetRandomInt(10, 15)
        stor.f = stor.a * stor.x + stor.b * (stor.x - stor.d) + stor.c * (stor.x - stor.e)
        break
      case 2:
        stor.x = 3 * j3pGetRandomInt(10, 15)
        stor.a = stor.x + stor.x / 3 + 2 * stor.x
        break
      case 3:
        do {
          stor.x = j3pGetRandomInt(20, 40)
          stor.a = j3pGetRandomInt(5, 15)
          stor.na = 2 * (stor.x - stor.a)
          stor.b = stor.na - stor.x
        } while (stor.b < 1)

        break
      case 4:
        stor.den1 = j3pGetRandomInt(3, 7)
        do { stor.den2 = j3pGetRandomInt(3, 7) } while (stor.den1 === stor.den2)
        stor.x = j3pGetRandomInt(2, 7) * stor.den1 * stor.den2
        do { stor.num1 = j3pGetRandomInt(1, stor.den1 - 1) } while (stor.num1 === stor.den1 - stor.num1)
        stor.a = '\\frac{' + stor.num1 + '}{' + stor.den1 + '}'
        stor.c = '\\frac{' + (stor.den1 - stor.num1) + '}{' + stor.den1 + '}'
        stor.num2 = j3pGetRandomInt(2, stor.den2 - 1)
        stor.b = '\\frac{' + stor.num2 + '}{' + stor.den2 + '}'
        stor.d = Math.round(stor.x * (stor.den1 - stor.num1) / stor.den1 * (1 - stor.num2 / stor.den2))
        break
      case 5:
        stor.x = j3pGetRandomInt(30, 100)
        stor.b = j3pGetRandomInt(30, 50)
        stor.a = stor.x * 2 + stor.b
        break
      case 6:
        stor.x = j3pGetRandomInt(6, 20)
        stor.b = j3pGetRandomInt(2, 5)
        do { stor.c = j3pGetRandomInt(2, 7) } while (stor.c === stor.b)
        stor.a = stor.x + 2 * stor.b * stor.x + stor.c
        break
      case 7:
        stor.a = j3pGetRandomInt(2, 10)
        do { stor.b = j3pGetRandomInt(2, 10) } while (stor.a === stor.b)
        break
      case 8:
        stor.a = j3pGetRandomInt(3, 10)
        stor.b = 2 * stor.a
        break
      case 9:
        stor.a = j3pGetRandomInt(3, 10)
        stor.b = 2 * stor.a
        break
    }
    stor.hitori = [e]
    stor.cfo = false
    return e
  }

  function initSection () {
    // on veut pas du listener sur la touche entrée
    me.validOnEnter = false // ex donneesSection.touche_entree
    // on impose le nb d’étapes à 2
    me.surcharge({ nbetapes: 2 })
    // Construction de la page
    me.construitStructurePage('presentation1bis')
    me.donneesSection.exos = []

    const pbPossibles = ds.Difficile ? [...tousLesPbs] : tousLesPbs.filter(pb => !pb.diff)
    let pbaDonn = j3pShuffle(pbPossibles)
    for (let i = 0; i < me.donneesSection.nbitems; i++) {
      if (pbaDonn.length === 0) pbaDonn = j3pShuffle(pbPossibles)
      me.donneesSection.exos.push({ formule: pbaDonn.pop() })
    }
    me.typederreurs = [0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0]
    me.afficheTitre('Mettre en équation')

    // chargement mtg
    getMtgCore({ withMathJax: true })
      .then(
        // success
        (mtgAppLecteur) => {
          stor.mtgAppLecteur = mtgAppLecteur
          // on ajoute notre listener sur le bouton suite
          enonceMain()
        },
        // failure
        (error) => {
          j3pShowError(error, { message: 'mathgraph n’est pas correctement chargé', mustNotify: true })
        })
      // plantage dans enonceMain
      .catch(j3pShowError)
  }

  function onClickSuite () {
    stor.hitori.push({ t: 'click Suite', indexGrandeur: stor.indexGrandeur })
    j3pDetruit(stor.boubout)
    me.afficheBoutonValider()
    poseGrand()
  }

  function FaisBoutSuitPourGrand () {
    me.cacheBoutonValider()
    stor.boubout = j3pAjouteBouton(me.buttonsElts.container, onClickSuite, { value: 'Suite', className: 'big suite' })
  }

  function yaReponse () {
    stor.hitori.push({ t: 'passe dans yaReponse', encours: stor.encours })
    if (stor.encours === 'grand') {
      if (!stor.liste.changed) {
        stor.liste.focus()
        return false
      }

      return true
    }
    if (stor.encours === 'fin') {
      if (!stor.list1.changed) {
        stor.list1.focus()
        return false
      }
      if (!stor.list2.changed) {
        stor.list2.focus()
        return false
      }
      return true
    }
  }
  function isRepOk () {
    let ok = true
    stor.hitori.push({ t: 'passe dans isrepok', encours: stor.encours })
    stor.lesdiv.solution.classList.remove('correction')
    stor.lesdiv.explications.classList.remove('explique')
    stor.errFonction = false
    stor.errAide = false
    stor.errDouble = false
    stor.errGauche = false
    stor.errDroite = false

    if (stor.encours === 'grand') {
      if (stor.liste.reponse !== stor.lesrp) {
        ok = false
        stor.liste.corrige(false)
        // cherche erreur
        if (stor.liste.reponse.indexOf('x') === -1) {
          stor.errFonction = true
        } else {
          stor.errAide = true
        }
      } else {
        stor.liste.corrige(true)
      }

      return ok
    }
    if (stor.encours === 'fin') {
      if (stor.list1.reponse === stor.list2.reponse) {
        stor.errDouble = true
        stor.list2.corrige(false)
        return false
      }
      if (stor.reppos[0] === stor.list1.reponse) {
        if (stor.reppos[1] === stor.list2.reponse) {
          return true
        } else {
          stor.list2.corrige(false)
          stor.errDroite = true
          return false
        }
      } else if (stor.reppos[1] === stor.list1.reponse) {
        if (stor.reppos[0] === stor.list2.reponse) {
          return true
        } else {
          stor.list2.corrige(false)
          stor.errDroite = true
          return false
        }
      } else {
        stor.list1.corrige(false)
        stor.errGauche = true
        if ((stor.reppos[0] !== stor.list2.reponse) && (stor.reppos[1] !== stor.list2.reponse)) {
          stor.list2.corrige(false)
          stor.errDroite = true
        }
        return false
      }
    }
  } // isRepOk
  function desactiveAll () {
    if (stor.encours === 'grand') {
      stor.liste.disable()
    }
    if (stor.encours === 'fin') {
      stor.list1.disable()
      stor.list2.disable()
    }
  } // desactiveAll
  function passeToutVert () {
    if (stor.encours === 'grand') {
      stor.liste.corrige(true)
    }
    if (stor.encours === 'fin') {
      stor.list1.corrige(true)
      stor.list2.corrige(true)
    }
  } // passeToutVert
  function barreLesFaux () {
    if (stor.encours === 'grand') {
      stor.cfo = true
      stor.liste.barre()
    }
    if (stor.encours === 'fin') {
      stor.list1.barreIfKo()
      stor.list2.barreIfKo()
    }
  }
  function affCorrFaux (isFin) {
    let yaco = false
    let yaexplik = false
    stor.hitori.push({ t: 'passe dans affcorfaux', encours: stor.encours, indexGrandeur: stor.indexGrandeur, isfin: isFin })
    for (let i = 0; i < stor.errFonction.length; i++) {
      j3pAffiche(stor.lesdiv.grandAZ[1][0], null, 'L’expression doit dépendre de <i>x</i> !')
    }
    for (let i = 0; i < stor.errAide.length; i++) {
      j3pAffiche(stor.lesdiv.grandAZ[1][0], null, stor.errAide[i].quoi)
    }
    if (stor.errDouble) {
      j3pAffiche(stor.lesdiv.explications, null, 'Cette équation est vraie pour tous les nombres !')
      yaexplik = true
    }
    if (stor.errGauche) {
      j3pAffiche(stor.lesdiv.explications, null, 'Le membre de gauche ne convient pas !')
      yaexplik = true
    }
    if (stor.errDroite) {
      j3pAffiche(stor.lesdiv.explications, null, 'Le membre de droite ne convient pas !')
      yaexplik = true
    }

    if (stor.errFonction) {
      j3pAffiche(stor.lesdiv.explications, null, 'L’expression doit dépendre de <i>x</i> !')
      yaexplik = true
    }
    if (stor.errAide) {
      j3pAffiche(stor.lesdiv.explications, null, stor.aide)
      yaexplik = true
    }

    if (isFin) {
      barreLesFaux()
      desactiveAll()
      yaco = true
      // barre les faux
      if (stor.encours === 'grand') {
        j3pAffiche(stor.lesdiv.solution, null, stor.exo.formule.grandeurs[stor.indexGrandeur].nom.replace(/£a/g, stor.a).replace(/£b/g, stor.b).replace(/£c/g, stor.c).replace(/£d/g, stor.d).replace(/£e/g, stor.e).replace(/£f/g, stor.f) + ' $=$ ' + stor.lesrp)
      }
      if (stor.encours === 'fin') {
        j3pAffiche(stor.lesdiv.solution, null, stor.reppos[0] + '$=$' + stor.reppos[1])
      }
    }
    // ces classes css ne sont pour le moment définie que dans le theme zonesAvecImageDeFond, mais ça pourrait servir à d’autres themes
    if (yaexplik) stor.lesdiv.explications.classList.add('explique')
    if (yaco) stor.lesdiv.solution.classList.add('correction')
  } // affCorrFaux

  function enonceMain () {
    if (stor.hitori !== undefined) stor.hitori.push({ t: 'passe dans enonceMain', indexGrandeur: stor.indexGrandeur })

    if (me.etapeCourante === 1) {
      if (stor.hitori !== undefined) stor.hitori.push({ t: 'enonceMain _> videleszone', indexGrandeur: stor.indexGrandeur })
      me.videLesZones()
    }

    if ((me.questionCourante % me.donneesSection.nbetapes) === 1) {
      if (stor.hitori !== undefined) stor.hitori.push({ t: 'enonceMain _> creelesdiv', indexGrandeur: stor.indexGrandeur })
      creeLesDiv()
      stor.exo = faisChoixExos()
      poseQuestion()
    } else {
      if (stor.hitori !== undefined) stor.hitori.push({ t: 'enonceMain _> directversfais fin', indexGrandeur: stor.indexGrandeur })
      faisFin()
    }
    me.finEnonce()
  }

  switch (this.etat) {
    case 'enonce':
      // code exécuté au lancement de la section
      if (me.debutDeLaSection) {
        initSection()
      } else {
        enonceMain()
      }
      break // case "enonce":

    case 'correction':
      // On teste si une réponse a été saisie
      stor.lesdiv.explications.innerHTML = ''
      stor.lesdiv.solution.innerHTML = ''

      if (yaReponse()) {
        // Bonne réponse
        if (isRepOk()) {
          stor.hitori.push({ t: 'passe dans cbien', encours: stor.encours })
          stor.lesdiv.correction.style.color = this.styles.cbien
          stor.lesdiv.correction.innerHTML = cBien
          desactiveAll()
          passeToutVert()
          if (stor.encours === 'grand') {
            stor.lespoints++
            stor.indexGrandeur++
            if (stor.indexGrandeur < stor.exo.formule.grandeurs.length) {
              FaisBoutSuitPourGrand()
              stor.hitori.push({ t: 'sort de cbien avec return', encours: stor.encours, indexGrandeur: stor.indexGrandeur })
              return
            }
            stor.lespoints = stor.lespoints / stor.indexGrandeur
          }

          stor.hitori.push({ t: 'sort de cbien', encours: stor.encours, indexGrandeur: stor.indexGrandeur })
          this.score = j3pArrondi(this.score + stor.lespoints, 1)
          this.typederreurs[0]++
          this.etat = 'navigation'
          this.sectionCourante()
        } else {
          // Pas de bonne réponse
          stor.lesdiv.correction.style.color = this.styles.cfaux
          stor.lesdiv.correction.innerHTML = cFaux
          if (this.isElapsed) {
            // A cause de la limite de temps :
            stor.lesdiv.correction.innerHTML += tempsDepasse + '<BR>'
            this.typederreurs[10]++
            affCorrFaux(true)
            if (stor.encours === 'grand') {
              stor.indexGrandeur++
              if (stor.indexGrandeur < stor.exo.formule.grandeurs.length) {
                FaisBoutSuitPourGrand()
                return
              }
              stor.lespoints = stor.lespoints / stor.indexGrandeur
              this.score = j3pArrondi(this.score + stor.lespoints, 1)
            }
            this.sectionCourante('navigation')
          } else {
            // Réponse fausse :
            if (me.essaiCourant < me.donneesSection.nbchances) {
              stor.lesdiv.correction.innerHTML += '<br>' + essaieEncore

              affCorrFaux(false)

              // affCorrFaux.call(this, false)
              this.typederreurs[1]++
            } else {
              // Erreur au nème essai
              affCorrFaux(true)
              if (stor.encours === 'grand') {
                stor.indexGrandeur++
                if (stor.indexGrandeur < stor.exo.formule.grandeurs.length) {
                  FaisBoutSuitPourGrand()
                  stor.hitori.push({ t: 'sort de cfaux avec return', encours: stor.encours, indexGrandeur: stor.indexGrandeur })
                  return
                }
                stor.lespoints = stor.lespoints / stor.indexGrandeur
                this.score = j3pArrondi(this.score + stor.lespoints, 1)
              }
              stor.hitori.push({ t: 'sort de cfaux SANS return', encours: stor.encours, indexGrandeur: stor.indexGrandeur })
              this.typederreurs[2]++
              this.sectionCourante('navigation')
            }
          }
        }
      } else {
        // pas de réponse
        stor.lesdiv.correction.style.color = this.styles.cfaux
        stor.lesdiv.correction.innerHTML = reponseIncomplete
      }

      // Obligatoire
      this.finCorrection()
      break // case "correction":

    case 'navigation':
      if (!this.sectionTerminee()) {
        this.etat = 'enonce'
      }
      this.finNavigation()
      break // case "navigation":
  }
}
