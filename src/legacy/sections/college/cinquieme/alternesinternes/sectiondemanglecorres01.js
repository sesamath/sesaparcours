import { j3pAddElt, j3pArrondi, j3pGetNewId, j3pGetRandomBool, j3pGetRandomInt, j3pShowError } from 'src/legacy/core/functions'
import { j3pCreeSVG } from 'src/legacy/core/functionsSvg'
import ListeDeroulante from 'src/legacy/outils/listeDeroulante/ListeDeroulante'
import { estNomdroite, estNompoint, estMesureangle } from 'src/legacy/outils/zoneStyleMathquill/functions'
import ZoneStyleMathquill1 from 'src/legacy/outils/zoneStyleMathquill/ZoneStyleMathquill1'
import { addDefaultTable } from 'src/legacy/themes/table'
import textesGeneriques from 'src/lib/core/textes'
import { j3pAffiche } from 'src/lib/mathquill/functions'
import { getMtgCore } from 'src/lib/outils/mathgraph'
import { addMaj } from 'src/lib/utils/string'

const { cBien, cFaux, essaieEncore, regardeCorrection } = textesGeneriques

/**
 * Ajouter une description ici
 * @fileOverview
 * @author Tommy Barroy <t.barroy@laposte.net>
 * @since 2017-10
*/

export const params = {
  outils: ['calculatrice', 'mathquill'],
  /**
   * Ce qui est paramétrable dans l’éditeur de graphe
   * @type {LegacySectionParamsParametres}
   */
  parametres: [
    ['nbrepetitions', 9, 'entier', 'Nombre de répétitions de la section'],
    ['limite', 0, 'entier', 'Temps disponible par répétition, en secondes'],
    ['nbchances', 2, 'entier', 'Nombre d’essais par répétition'],
    ['Etape_Description', false, 'boolean', 'Il faut décrire la configuration.'],
    ['Exercice', 'Parallélisme', 'liste', '<u>Parallélisme</u>: L’élève doit démontrer que deux droites sont parallèles ou que des points sont alignés. <BR> <u>Calcul</u>: L’élève doit calculer un angle.', ['Parallélisme', 'Calcul', 'Les deux']],
    ['correspondants', false, 'boolean', 'Les angles peuvent être correspondants.'],
    ['alternes_externes', false, 'boolean', 'Les angles peuvent être alternes-externes.'],
    ['alternes_internes', false, 'boolean', 'Les angles peuvent être alternes-internes.'],
    ['supplementaires_et_adjacents', false, 'boolean', 'Les angles peuvent être adjacents et supplémentaires.'],
    ['oppose_sommet', false, 'boolean', 'Les angles peuvent être opposés par le sommet.'],
    ['Eclaire', true, 'boolean', 'Les angles de l’exercice sont repérés sur le schéma.'],
    // le modèle gère la compatibilité ascendante avec l’ancien paramètre MiseEnPagePlusClaire
    ['theme', 'standard', 'liste', 'Modèle pour l’affichage', ['standard', 'zonesAvecImageDeFond']]
  ],
  pe: [
    { pe_1: 'Erreur écriture angle' }
  ]
}

const reponseIncomplete = 'Réponse incomplète !'

/**
 * section demanglecorres01
 * @this {Parcours}
 */
export default function main () {
  const me = this
  const stor = me.storage
  const ds = me.donneesSection
  let svgId = stor.svgId

  function opopop (num) {
    switch (num) {
      case 0: return [2]
      case 1: return [3]
      case 2: return [0]
      case 3: return [1]
      case 4: return [6]
      case 5: return [7]
      case 6: return [4]
      case 7: return [5]
    }
  }
  function alalt (num) {
    switch (num) {
      case 1: return [7]
      case 2: return [4]
      case 4: return [2]
      case 7: return [1]
    }
  }
  function alalte (num) {
    switch (num) {
      case 0: return [6]
      case 3: return [5]
      case 5: return [3]
      case 6: return [0]
    }
  }
  function cococo (num) {
    switch (num) {
      case 0: return [4]
      case 1: return [5]
      case 2: return [6]
      case 3: return [7]
      case 4: return [0]
      case 5: return [1]
      case 6: return [2]
      case 7: return [3]
    }
  }
  function supsup (num) {
    switch (num) {
      case 0: return [1, 3]
      case 1: return [0, 2]
      case 2: return [3, 1]
      case 3: return [0, 2]
      case 4: return [5, 7]
      case 5: return [4, 6]
      case 6: return [5, 7]
      case 7: return [4, 6]
    }
  }

  function AfficheFig () {
    stor.LeNomDuTruc = ''

    const nomspris = []

    const txtFigure = 'TWF0aEdyYXBoSmF2YTEuMAAAABM+TMzNAAJmcv###wEA#wEAAAAAAAAAAANLAAACfwAAAQEAAAAAAAAAAQAAAN######AAAAAQAKQ0NhbGNDb25zdAD#####AAJwaQAWMy4xNDE1OTI2NTM1ODk3OTMyMzg0Nv####8AAAABAApDQ29uc3RhbnRlQAkh+1RELRj#####AAAAAQAKQ1BvaW50QmFzZQD#####AQAAAAAOAAFVAMAkAAAAAAAAQBAAAAAAAAAAAAUAAEAv#nbItDlYQC#+dsi0OVj#####AAAAAQAUQ0Ryb2l0ZURpcmVjdGlvbkZpeGUA#####wEAAAAAEAAAAQAAAAEAAAABAT#wAAAAAAAA#####wAAAAEAD0NQb2ludExpZURyb2l0ZQD#####AQAAAAAOAAFWAMAAAAAAAAAAQBAAAAAAAAAAAAUAAUA##nbItDlYAAAAAv####8AAAABAAhDU2VnbWVudAD#####AQAAAAAQAAABAAAAAQAAAAEAAAAD#####wAAAAEAB0NNaWxpZXUA#####wEAAAAAEAAAAAAAAAAAAAAAQAgAAAAAAAAAAAUAAAAAAQAAAAP#####AAAAAgAMQ0NvbW1lbnRhaXJlAP####8BAAAAAAAAAAAAAAAAQBgAAAAAAAAAAAAAAAUMAAAAAAABAAAAAAAAAAEAAAAAAAAAAAABMf####8AAAABAAlDTG9uZ3VldXIA#####wAAAAEAAAADAAAAAgD#####AQAAAAAQAAAAAAAAAAAAAABACAAAAAAAAAAABQABQHewAAAAAABAcd4UeuFHrgAAAAMA#####wEAAAABEAAAAQAAAAEAAAAIAD#wAAAAAAAAAAAAAwD#####AQAAAAEQAAABAAAAAQAAAAgBP#AAAAAAAAAAAAACAP####8BAAAAABAAAAAAAAAAAAAAAEAIAAAAAAAAAAAFAAFAZUAAAAAAAEBifCj1wo9cAAAAAgD#####AQAAAAAQAAAAAAAAAAAAAABACAAAAAAAAAAABQABQGUgAAAAAABAQXCj1wo9cf####8AAAABAAlDQ2VyY2xlT0EA#####wEAAAAAAAABAAAACwAAAAwAAAACAP####8AAAAAABAAAABACAAAAAAAAAAAAAAAAAAAAAAFAABAVAAAAAAAAEB93hR64Ueu#####wAAAAEAB0NDYWxjdWwA#####wAFbWluaTEAATEAAAABP#AAAAAAAAAAAAAKAP####8ABW1heGkxAAMzNjAAAAABQHaAAAAAAAD#####AAAAAQAUQ0ltcGxlbWVudGF0aW9uUHJvdG8A#####wAHQ3Vyc2V1cgAAAAUAAAAFAAAAAwAAAA8AAAAQAAAADgAAAAMAAAAAEQEAAAAAEAAAAQAAAAEAAAAOAT#wAAAAAAAAAAAABAEAAAARAAAAAAAQAAAAwAgAAAAAAAA#8AAAAAAAAAAABQAAQF1AAAAAAAAAAAAS#####wAAAAEAC0NIb21vdGhldGllAAAAABEAAAAO#####wAAAAEACkNPcGVyYXRpb24D#####wAAAAEAD0NSZXN1bHRhdFZhbGV1cgAAAA8AAAANAQAAAA4AAAAPAAAADgAAABD#####AAAAAQALQ1BvaW50SW1hZ2UAAAAAEQEAAAAADQACTzEAwBAAAAAAAABAEAAAAAAAAAAABQAAAAATAAAAFAAAAAwAAAAAEQAAAA4AAAANAwAAAA0BAAAAAT#wAAAAAAAAAAAADgAAAA8AAAANAQAAAA4AAAAQAAAADgAAAA8AAAAPAAAAABEBAAAAAA0AAkkxAMAAAAAAAAAAQAgAAAAAAAAAAAUAAAAAEwAAABYAAAAFAQAAABEAAAAAABAAAAEAAAABAAAADgAAABMAAAAEAQAAABEAAAAAARAAAmsxAMAAAAAAAAAAQAAAAAAAAAAAAAEAAT#eoeoeoeofAAAAGP####8AAAACAA9DTWVzdXJlQWJzY2lzc2UBAAAAEQACYTEAAAAVAAAAFwAAABn#####AAAAAQAPQ1ZhbGV1ckFmZmljaGVlAQAAABEAAAAAAEA9AAAAAAAAwBQAAAAAAAAAAAAAABkPAAH###8AAAABAAAAAgAAAAEAAAAAAAAAAAAAAAACAAAAGgAAAAIA#####wAAAAAAEAAAAEAIAAAAAAAAAAAAAAAAAAAAAAUAAEBUAAAAAAAAQICPCj1wo9cAAAAKAP####8ABW1pbmkyAAEwAAAAAQAAAAAAAAAAAAAACgD#####AAVtYXhpMgACMTAAAAABQCQAAAAAAAAAAAALAP####8AB0N1cnNldXIAAAAFAAAABQAAAAMAAAAdAAAAHgAAABwAAAADAAAAAB8BAAAAABAAAAEAAAABAAAAHAE#8AAAAAAAAAAAAAQBAAAAHwAAAAAAEAAAAMAIAAAAAAAAP#AAAAAAAAAAAAUAAEBdQAAAAAAAAAAAIAAAAAwAAAAAHwAAABwAAAANAwAAAA4AAAAdAAAADQEAAAAOAAAAHQAAAA4AAAAeAAAADwAAAAAfAQAAAAANAAJPMQDAEAAAAAAAAEAQAAAAAAAAAAAFAAAAACEAAAAiAAAADAAAAAAfAAAAHAAAAA0DAAAADQEAAAABP#AAAAAAAAAAAAAOAAAAHQAAAA0BAAAADgAAAB4AAAAOAAAAHQAAAA8AAAAAHwEAAAAADQACSTEAwAAAAAAAAABACAAAAAAAAAAABQAAAAAhAAAAJAAAAAUBAAAAHwAAAAAAEAAAAQAAAAEAAAAcAAAAIQAAAAQBAAAAHwAAAAABEAABawDAAAAAAAAAAEAAAAAAAAAAAAABAAE#4CMCMCMCMAAAACYAAAAQAQAAAB8AAmEyAAAAIwAAACUAAAAnAAAAEQEAAAAfAAAAAAAAAAAAAAAAAMAYAAAAAAAAAAAAAAAnDwAB####AAAAAQAAAAIAAAABAAAAAAAAAAAAAAAAAgAAACgAAAACAP####8AAAAAABAAAABACAAAAAAAAAAAAAAAAAAAAAAFAABAbsAAAAAAAEB93hR64UeuAAAACgD#####AAVtaW5pMwABMAAAAAEAAAAAAAAAAAAAAAoA#####wAFbWF4aTMAAjEwAAAAAUAkAAAAAAAAAAAACwD#####AAdDdXJzZXVyAAAABQAAAAUAAAADAAAAKwAAACwAAAAqAAAAAwAAAAAtAQAAAAAQAAABAAAAAQAAACoBP#AAAAAAAAAAAAAEAQAAAC0AAAAAABAAAADACAAAAAAAAD#wAAAAAAAAAAAFAABAXUAAAAAAAAAAAC4AAAAMAAAAAC0AAAAqAAAADQMAAAAOAAAAKwAAAA0BAAAADgAAACsAAAAOAAAALAAAAA8AAAAALQEAAAAADQACTzEAwBAAAAAAAABAEAAAAAAAAAAABQAAAAAvAAAAMAAAAAwAAAAALQAAACoAAAANAwAAAA0BAAAAAT#wAAAAAAAAAAAADgAAACsAAAANAQAAAA4AAAAsAAAADgAAACsAAAAPAAAAAC0BAAAAAA0AAkkxAMAAAAAAAAAAQAgAAAAAAAAAAAUAAAAALwAAADIAAAAFAQAAAC0AAAAAABAAAAEAAAABAAAAKgAAAC8AAAAEAQAAAC0AAAAAARAAAmsyAMAAAAAAAAAAQAAAAAAAAAAAAAEAAT#fufufufugAAAANAAAABABAAAALQACYTMAAAAxAAAAMwAAADUAAAARAQAAAC0AAAAAAAAAAAAAAAAAwBgAAAAAAAAAAAAAADUPAAH###8AAAABAAAAAgAAAAEAAAAAAAAAAAAAAAACAAAANgAAAAIA#####wAAAAAAEAAAAEAIAAAAAAAAAAAAAAAAAAAAAAUAAEBvIAAAAAAAQICfCj1wo9cAAAAKAP####8ABW1pbmk0AAEwAAAAAQAAAAAAAAAAAAAACgD#####AAVtYXhpNAACMTAAAAABQCQAAAAAAAAAAAALAP####8AB0N1cnNldXIAAAAFAAAABQAAAAMAAAA5AAAAOgAAADgAAAADAAAAADsBAAAAABAAAAEAAAABAAAAOAE#8AAAAAAAAAAAAAQBAAAAOwAAAAAAEAAAAMAIAAAAAAAAP#AAAAAAAAAAAAUAAEBdQAAAAAAAAAAAPAAAAAwAAAAAOwAAADgAAAANAwAAAA4AAAA5AAAADQEAAAAOAAAAOQAAAA4AAAA6AAAADwAAAAA7AQAAAAANAAJPMQDAEAAAAAAAAEAQAAAAAAAAAAAFAAAAAD0AAAA+AAAADAAAAAA7AAAAOAAAAA0DAAAADQEAAAABP#AAAAAAAAAAAAAOAAAAOQAAAA0BAAAADgAAADoAAAAOAAAAOQAAAA8AAAAAOwEAAAAADQACSTEAwAAAAAAAAABACAAAAAAAAAAABQAAAAA9AAAAQAAAAAUBAAAAOwAAAAAAEAAAAQAAAAEAAAA4AAAAPQAAAAQBAAAAOwAAAAABEAACazMAwAAAAAAAAABAAAAAAAAAAAAAAQABP9VVVVVVVVUAAABCAAAAEAEAAAA7AAJhNAAAAD8AAABBAAAAQwAAABEBAAAAOwAAAAAAAAAAAAAAAADAGAAAAAAAAAAAAAAAQw8AAf###wAAAAEAAAACAAAAAQAAAAAAAAAAAAAAAAIAAABEAAAACgD#####AAJBMQACYTEAAAAOAAAAGgAAAAoA#####wACQTIAAmEyAAAADgAAACgAAAAKAP####8AAkEzAAJhMwAAAA4AAAA2AAAACgD#####AAJBNAACYTQAAAAOAAAARP####8AAAABAAlDUm90YXRpb24A#####wAAAAsAAAAOAAAARgAAAA8A#####wAAAAAAEAAAAAAAAAAAAAAAQAgAAAAAAAAAAAkAAAAADAAAAEr#####AAAAAQARQ1N5bWV0cmllQ2VudHJhbGUA#####wAAAAsAAAAPAP####8AAAAAABAAAAAAAAAAAAAAAEAIAAAAAAAAAANwcEUJAAAAAEsAAABMAAAABQD#####AQAAAAAQAAABAAAAAQAAAE0AAABL#####wAAAAIACUNDZXJjbGVPUgD#####AQAAAAAAAAEAAAALAAAAAT#wAAAAAAAAAP####8AAAABABBDSW50RHJvaXRlQ2VyY2xlAP####8AAABOAAAAT#####8AAAABABBDUG9pbnRMaWVCaXBvaW50AP####8BAAAAABAAAAAAAAAAAAAAAEAIAAAAAAAAAAAFAAEAAABQAAAAFgD#####AQAAAAAQAAAAAAAAAAAAAABACAAAAAAAAAAABQACAAAAUAAAAAwA#####wAAAFIAAAANAwAAAA4AAABHAAAAAUAkAAAAAAAAAAAADwD#####AAAAAAAQAAAAAAAAAAAAAABACAAAAAAAAAADcHBCCQAAAABNAAAAUwAAAAwA#####wAAAFEAAAANAwAAAA4AAABIAAAAAUAkAAAAAAAAAAAADwD#####AAAAAAAQAAAAAAAAAAAAAABACAAAAAAAAAAABQAAAABLAAAAVQAAABQA#####wEAAAAAAAABAAAATQAAAAFAAAAAAAAAAAD#####AAAAAQAQQ0ludENlcmNsZUNlcmNsZQD#####AAAADQAAAFcAAAAWAP####8BAAAAABAAAAAAAAAAAAAAAEAIAAAAAAAAAAAFAAEAAABYAAAAFgD#####AQAAAAAQAAAAAAAAAAAAAABACAAAAAAAAAAABQACAAAAWAAAABQA#####wEAAAAAAAABAAAASwAAAAFACAAAAAAAAAAAAAAXAP####8AAAANAAAAWwAAABYA#####wEAAAAAEAAAAAAAAAAAAAAAQAgAAAAAAAAAAAUAAgAAAFwAAAAWAP####8BAAAAABAAAAAAAAAAAAAAAEAIAAAAAAAAAAAFAAEAAABcAAAAEgD#####AAAACwAAAA0DAAAADQIAAAABQFhAAAAAAAAAAAAOAAAASQAAAAFAJAAAAAAAAAAAAA8A#####wAAAAAAEAAAAAAAAAAAAAAAQAgAAAAAAAAAA3BwRgkAAAAAWgAAAF######AAAAAQAJQ0Ryb2l0ZUFCAP####8BAAAAABAAAAEAAAACAAAAYAAAAFT#####AAAAAQAQQ0Ryb2l0ZVBhcmFsbGVsZQD#####AQAAAAAQAAABAAJkeAACAAAAVgAAAGH#####AAAAAQAjQ0F1dHJlUG9pbnRJbnRlcnNlY3Rpb25Ecm9pdGVDZXJjbGUA#####wAAAAAAEAAAAAAAAAAAAAAAQAgAAAAAAAAAA3BwRAkAAAAADQAAAGEAAABgAAAAFQD#####AAAAYgAAAA0AAAAWAP####8AAAAAABAAAAAAAAAAAAAAAEAIAAAAAAAAAAAJAAIAAABkAAAAFgD#####AAAAAAAQAAAAAAAAAAAAAABACAAAAAAAAAAACQABAAAAZAAAABgA#####wAAAAAAEAAAAQAAAAIAAABNAAAAS#####8AAAABABZDRHJvaXRlUGVycGVuZGljdWxhaXJlAP####8BAH8AABAAAAEAAAABAAAAYAAAAGEAAAAbAP####8BAH8AABAAAAEAAAABAAAAYwAAAGEAAAAbAP####8BAH8AABAAAAEAAAABAAAAZQAAAGIAAAAbAP####8BAH8AABAAAAEAAAABAAAAZgAAAGIAAAAbAP####8BAH8AABAAAAEAAAABAAAASwAAAGcAAAAbAP####8BAH8AABAAAAEAAAABAAAATQAAAGcAAAAUAP####8BAH8AAAAAAQAAAGYAAAABP+MzMzMzMzMAAAAAFAD#####AQB#AAAAAAEAAABjAAAAAT#jMzMzMzMzAAAAABQA#####wEAfwAAAAABAAAAVAAAAAE#4zMzMzMzMwAAAAAUAP####8BAH8AAAAAAQAAAE0AAAABP+MzMzMzMzMAAAAAFAD#####AQB#AAAAAAEAAABWAAAAAT#jMzMzMzMzAAAAABQA#####wEAfwAAAAABAAAASwAAAAE#4zMzMzMzMwAAAAAUAP####8BAH8AAAAAAQAAAGUAAAABP+MzMzMzMzMAAAAAFAD#####AQB#AAAAAAEAAABgAAAAAT#jMzMzMzMzAAAAABUA#####wAAAGgAAAB1AAAAFgD#####AQB#AAAQAAAAAAAAAAAAAABACAAAAAAAAAAABQABAAAAdgAAABYA#####wEAfwAAEAAAAAAAAAAAAAAAQAgAAAAAAAAAAAUAAgAAAHYAAAAVAP####8AAABqAAAAdAAAABYA#####wEAfwAAEAAAAAAAAAAAAAAAQAgAAAAAAAAAAAUAAQAAAHkAAAAWAP####8BAH8AABAAAAAAAAAAAAAAAEAIAAAAAAAAAAAFAAIAAAB5AAAAFQD#####AAAAawAAAG4AAAAWAP####8BAH8AABAAAAAAAAAAAAAAAEAIAAAAAAAAAAAFAAEAAAB8AAAAFgD#####AQB#AAAQAAAAAAAAAAAAAABACAAAAAAAAAAABQACAAAAfAAAABUA#####wAAAGwAAABzAAAAFgD#####AQB#AAAQAAAAAAAAAAAAAABACAAAAAAAAAAABQABAAAAfwAAABYA#####wEAfwAAEAAAAAAAAAAAAAAAQAgAAAAAAAAAAAUAAgAAAH######AAAAAQAMQ0Jpc3NlY3RyaWNlAP####8BAH8AABAAAAEAAAABAAAAVAAAAFYAAABlAAAAFQD#####AAAAggAAAHIAAAAWAP####8BAH8AABAAAAAAAAAAAAAAAEAIAAAAAAAAAAAFAAIAAACDAAAAFgD#####AQB#AAAQAAAAAAAAAAAAAABACAAAAAAAAAAABQABAAAAgwAAABwA#####wEAfwAAEAAAAQAAAAEAAABNAAAAVAAAAGAAAAAVAP####8AAACGAAAAcAAAABYA#####wEAfwAAEAAAAAAAAAAAAAAAQAgAAAAAAAAAAAUAAgAAAIcAAAAWAP####8BAH8AABAAAAAAAAAAAAAAAEAIAAAAAAAAAAAFAAEAAACHAAAAFQD#####AAAAbQAAAHEAAAAWAP####8BAH8AABAAAAAAAAAAAAAAAEAIAAAAAAAAAAAFAAEAAACKAAAAFgD#####AQB#AAAQAAAAAAAAAAAAAABACAAAAAAAAAAABQACAAAAigAAABUA#####wAAAGkAAABvAAAAFgD#####AQB#AAAQAAAAAAAAAAAAAABACAAAAAAAAAAABQABAAAAjQAAABYA#####wEAfwAAEAAAAAAAAAAAAAAAQAgAAAAAAAAAAAUAAgAAAI0AAAAUAP####8BAH8AAAAAAQAAAFQAAAABP#AAAAAAAAAAAAAAFAD#####AQB#AAAAAAEAAABWAAAAAT#wAAAAAAAAAAAAABUA#####wAAAGcAAACQAAAAFgD#####AQB#AAAQAAAAAAAAAAAAAABACAAAAAAAAAAABQABAAAAkgAAABYA#####wEAfwAAEAAAAAAAAAAAAAAAQAgAAAAAAAAAAAUAAgAAAJIAAAAVAP####8AAABhAAAAkAAAABYA#####wEAfwAAEAAAAAAAAAAAAAAAQAgAAAAAAAAAAAUAAgAAAJUAAAAWAP####8BAH8AABAAAAAAAAAAAAAAAEAIAAAAAAAAAAAFAAEAAACVAAAAFQD#####AAAAYgAAAJEAAAAWAP####8BAH8AABAAAAAAAAAAAAAAAEAIAAAAAAAAAAAFAAIAAACYAAAAFgD#####AQB#AAAQAAAAAAAAAAAAAABACAAAAAAAAAAABQABAAAAmAAAABUA#####wAAAGcAAACRAAAAFgD#####AQB#AAAQAAAAAAAAAAAAAABACAAAAAAAAAAABQACAAAAmwAAABYA#####wEAfwAAEAAAAAAAAAAAAAAAQAgAAAAAAAAAAAUAAQAAAJv#####AAAAAQASQ0FyY0RlQ2VyY2xlRGlyZWN0AP####8BAAAAAAAAAwAAAFQAAACXAAAAlAAAAB0A#####wEAAAAAAAADAAAAVAAAAJQAAACWAAAAHQD#####AQAAAAAAAAMAAABUAAAAkwAAAJcAAAAdAP####8BAAAAAAAAAwAAAFQAAACWAAAAkwAAAB0A#####wEAAAAAAAADAAAAVgAAAJoAAACcAAAAHQD#####AQAAAAAAAAMAAABWAAAAnAAAAJkAAAAdAP####8BAAAAAAAAAwAAAFYAAACZAAAAnQAAAB0A#####wEAAAAAAAADAAAAVgAAAJ0AAACa#####wAAAAEAGUNTdXJmYWNlU2VjdGV1ckNpcmN1bGFpcmUA#####wDY2NgAAnMwAAAABAAAAKUAAAAeAP####8A2NjYAAJzMQAAAAQAAACiAAAAHgD#####ANjY2AACczIAAAAEAAAAowAAAB4A#####wDY2NgAAnMzAAAABAAAAKQAAAAeAP####8A2NjYAAJzNAAAAAQAAACgAAAAHgD#####ANjY2AACczUAAAAEAAAAngAAAB4A#####wDY2NgAAnM2AAAABAAAAJ8AAAAeAP####8A2NjYAAJzNwAAAAQAAACh#####wAAAAEADkNPYmpldER1cGxpcXVlAP####8BAAAAAAJkMQAAAGEAAAAfAP####8BAAAAAAJkMgAAAGIAAAAfAP####8AAAAAAAJkMwAAAGcAAAAHAP####8AAAB#AQADbnBBAAAAhRAAAAAAAAEAAAABAAAAAQAAAAAAAAAAAAFBAAAABwD#####AAAAfwEAA25wQgAAAIkQAAAAAAABAAAAAQAAAAEAAAAAAAAAAAABQgAAAAcA#####wAAAH8BAANucEMAAAB+EAAAAAAAAQAAAAEAAAABAAAAAAAAAAAAAUMAAAAHAP####8AAAB#AQADbnBEAAAAjxAAAAAAAAEAAAABAAAAAQAAAAAAAAAAAAFEAAAABwD#####AAAAfwEAA25wRQAAAIwQAAAAAAABAAAAAQAAAAEAAAAAAAAAAAABRQAAAAcA#####wAAAH8BAANucEYAAAB4EAAAAAAAAQAAAAEAAAABAAAAAAAAAAAAAUYAAAAHAP####8AAAB#AQADbnBHAAAAexAAAAAAAAEAAAABAAAAAQAAAAAAAAAAAAFHAAAABwD#####AAAAfwEAA25wSAAAAIEQAAAAAAABAAAAAQAAAAEAAAAAAAAAAAABSAAAABQA#####wEAAAAAAAADAAAAVAAAAAE#5mZmZmZmZgAAAAAUAP####8BAAAAAAAAAwAAAFYAAAABP+ZmZmZmZmYAAAAAFQD#####AAAAYQAAALkAAAAWAP####8BAAAAABAAAAAAAAAAAAAAAEAIAAAAAAAAAAAJAAIAAAC7AAAAFgD#####AQAAAAAQAAAAAAAAAAAAAABACAAAAAAAAAAACQABAAAAuwAAABUA#####wAAAGcAAAC5AAAAFgD#####AQAAAAAQAAAAAAAAAAAAAABACAAAAAAAAAAACQACAAAAvgAAABYA#####wEAAAAAEAAAAAAAAAAAAAAAQAgAAAAAAAAAAAkAAQAAAL4AAAAVAP####8AAABiAAAAugAAABYA#####wEAAAAAEAAAAAAAAAAAAAAAQAgAAAAAAAAAAAkAAgAAAMEAAAAWAP####8BAAAAABAAAAAAAAAAAAAAAEAIAAAAAAAAAAAJAAEAAADBAAAAFQD#####AAAAZwAAALoAAAAWAP####8BAAAAABAAAAAAAAAAAAAAAEAIAAAAAAAAAAAJAAIAAADEAAAAFgD#####AQAAAAAQAAAAAAAAAAAAAABACAAAAAAAAAAACQABAAAAxAAAABQA#####wH#AAAAAAAMAAAAVAAAAAE#8AAAAAAAAAAAAAAUAP####8B#wAAAAAADAAAAFYAAAABP#AAAAAAAAAAAAAAFQD#####AAAAYQAAAMcAAAAWAP####8B#wAAABAAAAAAAAAAAAAAAEAIAAAAAAAAAAAJAAIAAADJAAAAFgD#####Af8AAAAQAAAAAAAAAAAAAABACAAAAAAAAAAACQABAAAAyQAAABUA#####wAAAGIAAADIAAAAFgD#####Af8AAAAQAAAAAAAAAAAAAABACAAAAAAAAAAACQACAAAAzAAAABYA#####wH#AAAAEAAAAAAAAAAAAAAAQAgAAAAAAAAAAAkAAQAAAMwAAAAVAP####8AAABnAAAAyAAAABYA#####wH#AAAAEAAAAAAAAAAAAAAAQAgAAAAAAAAAAAkAAgAAAM8AAAAWAP####8B#wAAABAAAAAAAAAAAAAAAEAIAAAAAAAAAAAJAAEAAADPAAAAFQD#####AAAAZwAAAMcAAAAWAP####8B#wAAABAAAAAAAAAAAAAAAEAIAAAAAAAAAAAJAAEAAADSAAAAFgD#####Af8AAAAQAAAAAAAAAAAAAABACAAAAAAAAAAACQACAAAA0gAAAB0A#####wAAAAAAA2FhMAADAAAAVgAAANEAAADOAAAAHQD#####AAAAAAADYWExAAMAAABWAAAAzgAAANAAAAAdAP####8AAAAAAANhYTIAAwAAAFYAAADQAAAAzQAAAB0A#####wAAAAAAA2FhMwADAAAAVgAAAM0AAADRAAAAHQD#####AAAAAAADYWE0AAMAAABUAAAA0wAAAMsAAAAdAP####8AAAAAAANhYTUAAwAAAFQAAADLAAAA1AAAAB0A#####wAAAAAAA2FhNgADAAAAVAAAANQAAADKAAAAHQD#####AAAAAAADYWE3AAMAAABUAAAAygAAANMAAAAYAP####8AAAAAABAAAAEAAmRyAAIAAABlAAAAZgAAABgA#####wAAAAAAEAAAAQACZG0AAgAAAGAAAABjAAAAB###########'
    const A = addMaj(nomspris)
    const B = addMaj(nomspris)
    const C = addMaj(nomspris)
    const D = addMaj(nomspris)
    const E = addMaj(nomspris)
    const F = addMaj(nomspris)
    const G = addMaj(nomspris)
    const H = addMaj(nomspris)
    stor.lettres = [A, B, C, D, E, F, G, H]
    stor.nndd11 = [D + F, F + D, D + B, B + D, F + B, B + F]
    stor.nndd22 = [G + C, C + G, G + A, A + G, C + A, A + C]
    stor.nnSec = [E + H, H + E, E + B, A + H, H + A, B + E, E + A, A + E, B + A, A + B, B + H, H + B]
    stor.listangle = [H + A + C, C + A + B, B + A + G, G + A + H, A + B + D, D + B + E, E + B + F, F + B + A]
    svgId = j3pGetNewId()
    stor.svgId = svgId
    j3pCreeSVG(stor.lesdiv.figure, { id: svgId, width: 400, height: 350, style: { border: 'solid black 1px' } })
    stor.mtgAppLecteur.removeAllDoc()
    stor.mtgAppLecteur.addDoc(svgId, txtFigure, true)
    stor.mtgAppLecteur.giveFormula2(svgId, 'A1', j3pGetRandomInt(0, 359))
    const A2 = j3pGetRandomInt(0, 5)
    let A3 = j3pGetRandomInt(0, 5)
    if (A2 === A3 && A2 === 0) A3 = 1
    stor.mtgAppLecteur.giveFormula2(svgId, 'A2', A2)
    stor.mtgAppLecteur.giveFormula2(svgId, 'A3', A3)
    stor.mtgAppLecteur.giveFormula2(svgId, 'A4', j3pGetRandomInt(0, 5))
    stor.mtgAppLecteur.setText(svgId, '#npA', A)
    stor.mtgAppLecteur.setText(svgId, '#npC', C)
    stor.mtgAppLecteur.setText(svgId, '#npG', G)
    stor.mtgAppLecteur.setText(svgId, '#npH', H)
    stor.mtgAppLecteur.setText(svgId, '#npB', B)

    stor.mtgAppLecteur.calculateAndDisplayAll(true)
    if (stor.pbencours === 'angles opposés par le sommet') {
      stor.mtgAppLecteur.setVisible(svgId, '#dm', false, true)
      stor.mtgAppLecteur.setVisible(svgId, '#npD', false, true)
      stor.mtgAppLecteur.setVisible(svgId, '#npE', false, true)
      stor.mtgAppLecteur.setVisible(svgId, '#npF', false, true)
      stor.mtgAppLecteur.setVisible(svgId, '#ppD', false, true)
      stor.mtgAppLecteur.setVisible(svgId, '#ppE', false, true)
      stor.mtgAppLecteur.setVisible(svgId, '#ppF', false, true)
    } else {
      stor.mtgAppLecteur.setText(svgId, '#npD', D)
      stor.mtgAppLecteur.setText(svgId, '#npE', E)
      stor.mtgAppLecteur.setText(svgId, '#npF', F)
      stor.mtgAppLecteur.setVisible(svgId, '#ppB', false, true)
    }

    stor.angle1 = j3pGetRandomInt(0, 7)
    stor.angle2 = []
    switch (stor.pbencours) {
      case 'angles opposés par le sommet':
        stor.angle1 = j3pGetRandomInt(0, 3)
        stor.angle2 = opopop(stor.angle1)
        stor.nnSec = [A + H, H + A, B + A, A + B, B + H, H + B]
        break
      case 'angles alternes-internes': // pas 0 //pas 1 //pas 4 //pas 6
        switch (stor.angle1) {
          case 0: stor.angle1 = 1
            break
          case 3 : stor.angle1 = 2
            break
          case 5: stor.angle1 = 4
            break
          case 6 : stor.angle1 = 7
            break
        }
        stor.angle2 = alalt(stor.angle1)
        break
      case 'angles correspondants': stor.angle2 = cococo(stor.angle1)
        break
      case 'angles alternes-externes': // pas 0 //pas 1 //pas 4 //pas 6
        switch (stor.angle1) {
          case 1: stor.angle1 = 0
            break
          case 2 : stor.angle1 = 3
            break
          case 4: stor.angle1 = 5
            break
          case 7 : stor.angle1 = 6
            break
        }
        stor.angle2 = alalte(stor.angle1)

        break
      case 'angles adjacents supplémentaires':
        if (stor.questencours === 'P') {
          if (j3pGetRandomBool()) {
            stor.angle1 = 4; stor.angle2 = [7]
          } else {
            stor.angle1 = 5; stor.angle2 = [6]
          }
        } else {
          stor.angle2 = supsup(stor.angle1)
        }
        break
      default :
        j3pShowError(Error(`Cas non prévu (${stor.pbencours})`), { message: 'Erreur interne (cas non prévu)', mustNotify: true })
    }

    for (let i = 0; i < 8; i++) {
      stor.mtgAppLecteur.setColor(svgId, '#a' + i, 0, 0, 0, true, 0)
      stor.mtgAppLecteur.setVisible(svgId, '#aa' + i, false, true)
      stor.mtgAppLecteur.setVisible(svgId, '#s' + i, false, true)
    }

    stor.angle2 = stor.angle2[0]
    if (ds.Eclaire) {
      stor.mtgAppLecteur.setVisible(svgId, '#aa' + stor.angle1, true, true)
      stor.mtgAppLecteur.setVisible(svgId, '#aa' + stor.angle2, true, true)
      stor.mtgAppLecteur.setVisible(svgId, '#s' + stor.angle1, true, true)
      stor.mtgAppLecteur.setVisible(svgId, '#s' + stor.angle2, true, true)
    }
  }

  function afcofo (boolfin) {
    stor.yaco = false
    let i
    if (stor.adire1.length > 0) {
      let bufe = ''
      stor.yaexplik1 = true
      for (i = 0; i < stor.adire1.length; i++) {
        j3pAffiche(stor.lesdiv.explications1, null, bufe + stor.adire1[i])
        bufe = '<BR>'
      }
    }
    if (stor.adire2.length > 0) {
      let bufe = ''
      stor.yaexplik2 = true
      for (i = 0; i < stor.adire2.length; i++) {
        j3pAffiche(stor.lesdiv.explications, null, bufe + stor.adire2[i])
        bufe = '<BR>'
      }
    }

    if (boolfin) {
      stor.yaco = true
      /// /affiche la co
      /// /
      /// pour la descrip
      let bufdesc = ''
      if (ds.Etape_Description) {
        if (['angles alternes-internes', 'angles alternes-externes', 'angles correspondants'].indexOf(stor.pbencours) !== -1) {
          if (stor.questencours === 'C') {
            j3pAffiche(stor.lesdiv.solution, null, 'Les droites parallèles (' + stor.nndd11[0] + ') et (' + stor.nndd22[0] + ') sont coupées par la sécante (' + stor.nnSec[0] + ') .')
          } else {
            j3pAffiche(stor.lesdiv.solution, null, 'Les droites (' + stor.nndd11[0] + ') et (' + stor.nndd22[0] + ') sont coupées par la sécante (' + stor.nnSec[0] + ') .')
          }
        }
        if (stor.pbencours === 'angles adjacents supplémentaires') {
          j3pAffiche(stor.lesdiv.solution, null, 'La somme des angles adjacents $\\widehat{' + stor.nomchoisiangle1 + '}$ et  $\\widehat{' + stor.nomchoisiangle2 + '}$ est égale à $180°$ . ')
        }
        if (stor.pbencours === 'angles opposés par le sommet') {
          j3pAffiche(stor.lesdiv.solution, null, 'Les droites (' + stor.nndd22[0] + ') et (' + stor.nnSec[4] + ') sont sécantes en  ' + stor.lettres[0] + ' .')
        }
        bufdesc = '<br>'
      }

      /// /pour la quest
      if (stor.questencours === 'P') {
        if (['angles alternes-internes', 'angles alternes-externes', 'angles correspondants'].indexOf(stor.pbencours) !== -1) {
          j3pAffiche(stor.lesdiv.solution, null, bufdesc + 'Les angles ' + stor.pbencours.replace('angles ', '') + ' $\\widehat{' + stor.nomchoisiangle1 + '}$ et $\\widehat{' + stor.nomchoisiangle2 + '}$ sont égaux,')
          j3pAffiche(stor.lesdiv.solution, null, ' \ndonc les droites (' + stor.nndd11[0] + ') et (' + stor.nndd22[0] + ') sont parallèles.')
        }
        if (stor.pbencours === 'angles adjacents supplémentaires') {
          j3pAffiche(stor.lesdiv.solution, null, bufdesc + 'donc les points ' + stor.lettres[3] + ' , ' + stor.lettres[1] + ' et ' + stor.lettres[5] + ' sont alignés.')
        }
      } else {
        if (['angles alternes-internes', 'angles alternes-externes', 'angles correspondants'].indexOf(stor.pbencours) !== -1) {
          j3pAffiche(stor.lesdiv.solution, null, bufdesc + 'Les angles $\\widehat{' + stor.nomchoisiangle2 + '}$ et $\\widehat{' + stor.nomchoisiangle1 + '}$ sont ' + stor.pbencours.replace('angles ', '') + ' , \ndonc $\\widehat{' + stor.nomchoisiangle2 + '}$ = $\\widehat{' + stor.nomchoisiangle1 + '}$ = $' + valeurangle(stor.angle1) + '°$ ')
        }
        if (stor.pbencours === 'angles adjacents supplémentaires') {
          j3pAffiche(stor.lesdiv.solution, null, bufdesc)
          const hui = addDefaultTable(stor.lesdiv.solution, 3, 2)
          hui[0][0].style.verticalAlign = 'bottom'
          j3pAffiche(hui[0][0], null, 'donc&nbsp;&nbsp;')
          j3pAffiche(hui[0][1], null, ' $\\widehat{' + stor.nomchoisiangle2 + '} = 180 - \\widehat{' + stor.nomchoisiangle1 + '}$')
          j3pAffiche(hui[1][1], null, ' $\\widehat{' + stor.nomchoisiangle2 + '} = 180 - ' + valeurangle(stor.angle1) + '$')
          j3pAffiche(hui[2][1], null, '$\\widehat{' + stor.nomchoisiangle2 + '} =' + valeurangle(stor.angle2) + '°$')
        }
        if (stor.pbencours === 'angles opposés par le sommet') {
          j3pAffiche(stor.lesdiv.solution, null, bufdesc + 'Les angles $\\widehat{' + stor.nomchoisiangle2 + '}$ et $\\widehat{' + stor.nomchoisiangle1 + '}$ sont opposés par le sommet, \ndonc $\\widehat{' + stor.nomchoisiangle2 + '}$ = $\\widehat{' + stor.nomchoisiangle1 + '}$ = $' + valeurangle(stor.angle1) + '°$')
        }
      }
      for (i = 0; i < stor.elemaco.length; i++) {
        stor.elemaco[i].barre()
      }
      desatout()
    } else { me.afficheBoutonValider() }
  }

  function yareponse () {
    if (ds.Etape_Description) {
      if (['angles alternes-internes', 'angles alternes-externes', 'angles correspondants'].indexOf(stor.pbencours) !== -1) {
        if (stor.zoned1.reponse() === '') { stor.zoned1.focus(); return false }
        if (stor.zoned2.reponse() === '') { stor.zoned2.focus(); return false }
        if (stor.zoned3.reponse() === '') { stor.zoned3.focus(); return false }
      }
      if (stor.pbencours === 'angles adjacents supplémentaires') {
        if (stor.zoneegd.reponse() === '') { stor.zoneegd.focus(); return false }
      }
      if (stor.pbencours === 'angles opposés par le sommet') {
        if (stor.zoned1.reponse() === '') { stor.zoned1.focus(); return false }
        if (stor.zoned2.reponse() === '') { stor.zoned2.focus(); return false }
        if (stor.zonep1.reponse() === '') { stor.zonep1.focus(); return false }
      }
    }
    if (stor.questencours === 'P') {
      if (['angles alternes-internes', 'angles alternes-externes', 'angles correspondants'].indexOf(stor.pbencours) !== -1) {
        if (stor.listeDeroulanteX.getReponseIndex() === 0) { stor.listeDeroulanteX.focus(); return false }
        if (stor.zoned4.reponse() === '') { stor.zoned4.focus(); return false }
        if (stor.zoned5.reponse() === '') { stor.zoned5.focus(); return false }
        if (stor.listeDeroulanteX2.getReponseIndex() === 0) { stor.listeDeroulanteX2.focus(); return false }
      }
      if (stor.pbencours === 'angles adjacents supplémentaires') {
        if (stor.zonep1.reponse() === '') { stor.zonep1.focus(); return false }
        if (stor.zonep2.reponse() === '') { stor.zonep2.focus(); return false }
        if (stor.zonep3.reponse() === '') { stor.zonep3.focus(); return false }
      }
    } else {
      if (['angles alternes-internes', 'angles alternes-externes', 'angles correspondants'].indexOf(stor.pbencours) !== -1) {
        if (stor.listeDeroulanteX.getReponseIndex() === 0) { stor.listeDeroulanteX.focus(); return false }
        if (stor.zoneeg.reponse() === '') { stor.zoneeg.focus(); return false }
      }
      if (stor.pbencours === 'angles adjacents supplémentaires') {
        if (stor.zoneas1.reponse() === '') { stor.zoneas1.focus(); return false }
        if (stor.zoneas2.reponse() === '') { stor.zoneas2.focus(); return false }
      }
      if (stor.pbencours === 'angles opposés par le sommet') {
        if (stor.listeDeroulanteX.getReponseIndex() === 0) { stor.listeDeroulanteX.focus(); return false }
        if (stor.zoneeg.reponse() === '') { stor.zoneeg.focus(); return false }
      }
    }
    return true
  }

  function bonneReponse () {
    stor.lesdiv.explications.innerHTML = ''
    stor.lesdiv.explications1.innerHTML = ''
    stor.lesdiv.explications1.classList.remove('explique')
    stor.lesdiv.explications.classList.remove('explique')

    stor.elemaco = []
    stor.adire1 = []
    stor.adire2 = []
    let test
    passetoutvert()
    // test si reponse coherentes (droite maj parent , ponit maj tout seul , mesure avec °)
    if (ds.Etape_Description) {
      if (['angles alternes-internes', 'angles alternes-externes', 'angles correspondants'].indexOf(stor.pbencours) !== -1) {
        test = estNomdroite(stor.zoned1.reponse(), { pointspossibles: stor.lettres })
        if (!test.good) { stor.zoned1.corrige(false); stor.elemaco.push(stor.zoned1); stor.adire1.push(test.adire) }
        test = estNomdroite(stor.zoned2.reponse(), { pointspossibles: stor.lettres })
        if (!test.good) { stor.zoned2.corrige(false); stor.elemaco.push(stor.zoned2); stor.adire1.push(test.adire) }
        test = estNomdroite(stor.zoned3.reponse(), { pointspossibles: stor.lettres })
        if (!test.good) { stor.zoned3.corrige(false); stor.elemaco.push(stor.zoned3); stor.adire1.push(test.adire) }
      }
      if (stor.pbencours === 'angles adjacents supplémentaires') {
        test = estMesureangle(stor.zoneegd.reponse())
        if (!test.good) { stor.zoneegd.corrige(false); stor.elemaco.push(stor.zoneegd); stor.adire1.push(test.adire) }
      }
      if (stor.pbencours === 'angles opposés par le sommet') {
        test = estNomdroite(stor.zoned1.reponse(), { pointspossibles: stor.lettres })
        if (!test.good) { stor.zoned1.corrige(false); stor.elemaco.push(stor.zoned1); stor.adire1.push(test.adire) }
        test = estNomdroite(stor.zoned2.reponse(), { pointspossibles: stor.lettres })
        if (!test.good) { stor.zoned2.corrige(false); stor.elemaco.push(stor.zoned2); stor.adire1.push(test.adire) }
        test = estNompoint(stor.zonep1.reponse(), { pointspossibles: stor.lettres })
        if (!test.good) { stor.zonep1.corrige(false); stor.elemaco.push(stor.zonep1); stor.adire1.push(test.adire) }
      }
    }
    if (stor.questencours === 'P') {
      if (['angles alternes-internes', 'angles alternes-externes', 'angles correspondants'].indexOf(stor.pbencours) !== -1) {
        test = estNomdroite(stor.zoned4.reponse(), { pointspossibles: stor.lettres })
        if (!test.good) { stor.zoned4.corrige(false); stor.elemaco.push(stor.zoned4); stor.adire2.push(test.adire) }
        test = estNomdroite(stor.zoned5.reponse(), { pointspossibles: stor.lettres })
        if (!test.good) { stor.zoned5.corrige(false); stor.elemaco.push(stor.zoned5); stor.adire2.push(test.adire) }
      }
      if (stor.pbencours === 'angles adjacents supplémentaires') {
        test = estNompoint(stor.zonep1.reponse(), { pointspossibles: stor.lettres })
        if (!test.good) { stor.zonep1.corrige(false); stor.elemaco.push(stor.zonep1); stor.adire2.push(test.adire) }
        test = estNompoint(stor.zonep2.reponse(), { pointspossibles: stor.lettres })
        if (!test.good) { stor.zonep2.corrige(false); stor.elemaco.push(stor.zonep2); stor.adire2.push(test.adire) }
        test = estNompoint(stor.zonep3.reponse(), { pointspossibles: stor.lettres })
        if (!test.good) { stor.zonep3.corrige(false); stor.elemaco.push(stor.zonep3); stor.adire2.push(test.adire) }
      }
    } else {
      if (['angles alternes-internes', 'angles alternes-externes', 'angles correspondants'].indexOf(stor.pbencours) !== -1) {
        test = estMesureangle(stor.zoneeg.reponse())
        if (!test.good) { stor.zoneeg.corrige(false); stor.elemaco.push(stor.zoneeg); stor.adire2.push(test.adire) }
      }
      if (stor.pbencours === 'angles adjacents supplémentaires') {
        test = estMesureangle(stor.zoneas2.reponse())
        if (!test.good) { stor.zoneas2.corrige(false); stor.elemaco.push(stor.zoneas2); stor.adire2.push(test.adire) }
      }
      if (stor.pbencours === 'angles opposés par le sommet') {
        test = estMesureangle(stor.zoneeg.reponse())
        if (!test.good) { stor.zoneeg.corrige(false); stor.elemaco.push(stor.zoneeg); stor.adire2.push(test.adire) }
      }
    }

    /// si pas cohere on repond false

    let yaerr = ((stor.adire1.length + stor.adire2.length) > 0)
    /// test si réponses justes
    if (ds.Etape_Description) {
      if (['angles alternes-internes', 'angles alternes-externes', 'angles correspondants'].indexOf(stor.pbencours) !== -1) {
      // verif d1 et d2 sont les deux droites
        let dd1 = -1
        let dd2 = -1
        const d1 = stor.zoned1.reponse().replace('(', '').replace(')', '')
        const d2 = stor.zoned2.reponse().replace('(', '').replace(')', '')
        if (stor.nndd11.indexOf(d1) !== -1) { dd1 = 1 }
        if (stor.nndd22.indexOf(d1) !== -1) { dd1 = 2 }
        if (stor.nndd11.indexOf(d2) !== -1) { dd2 = 1 }
        if (stor.nndd22.indexOf(d2) !== -1) { dd2 = 2 }
        if (dd1 === -1) {
          stor.zoned1.corrige(false); stor.elemaco.push(stor.zoned1); yaerr = true
        }
        if (dd2 === -1) {
          stor.zoned2.corrige(false); stor.elemaco.push(stor.zoned2); yaerr = true
        }
        if ((dd2 === dd1) && (dd1 !== -1)) {
          stor.zoned2.corrige(false); stor.elemaco.push(stor.zoned2); stor.adire1.push('Tu nommes deux fois la même droite !'); yaerr = true
        }
        // verif d3 est la sécante
        const d3 = stor.zoned3.reponse().replace('(', '').replace(')', '')
        if (stor.nnSec.indexOf(d3) === -1) { stor.zoned3.corrige(false); stor.elemaco.push(stor.zoned3); yaerr = true }
      }
      if (stor.pbencours === 'angles adjacents supplémentaires') {
        if (stor.zoneegd.reponse() !== '180°') { stor.zoneegd.corrige(false); stor.elemaco.push(stor.zoneegd); stor.adire1.push('Un angle plat mesure $180°$ !'); yaerr = true }
      }
      if (stor.pbencours === 'angles opposés par le sommet') {
        let dd1 = -1
        let dd2 = -1
        const d1 = stor.zoned1.reponse().replace('(', '').replace(')', '')
        const d2 = stor.zoned2.reponse().replace('(', '').replace(')', '')
        if (stor.nndd22.indexOf(d1) !== -1) { dd1 = 1 }
        if (stor.nnSec.indexOf(d1) !== -1) { dd1 = 2 }
        if (stor.nndd22.indexOf(d2) !== -1) { dd2 = 1 }
        if (stor.nnSec.indexOf(d2) !== -1) { dd2 = 2 }
        if (dd1 === -1) {
          stor.zoned1.corrige(false); stor.elemaco.push(stor.zoned1); yaerr = true
        }
        if (dd2 === -1) {
          stor.zoned2.corrige(false); stor.elemaco.push(stor.zoned2); yaerr = true
        }
        if ((dd2 === dd1) && (dd1 !== -1)) {
          stor.zoned2.corrige(false); stor.elemaco.push(stor.zoned2); stor.adire1.push('Tu nommes deux fois la même droite !'); yaerr = true
        }
        if (stor.zonep1.reponse() !== stor.lettres[0]) { stor.zonep1.corrige(false); stor.elemaco.push(stor.zonep1); yaerr = true }
      }
    }
    if (stor.questencours === 'P') {
      if (['angles alternes-internes', 'angles alternes-externes', 'angles correspondants'].indexOf(stor.pbencours) !== -1) {
        let dd4 = -1
        let dd5 = -1
        const d4 = stor.zoned4.reponse().replace('(', '').replace(')', '')
        const d5 = stor.zoned5.reponse().replace('(', '').replace(')', '')
        if (stor.nndd11.indexOf(d4) !== -1) { dd4 = 1 }
        if (stor.nndd22.indexOf(d4) !== -1) { dd4 = 2 }
        if (stor.nndd11.indexOf(d5) !== -1) { dd5 = 1 }
        if (stor.nndd22.indexOf(d5) !== -1) { dd5 = 2 }
        if (dd4 === -1) {
          stor.zoned4.corrige(false); stor.elemaco.push(stor.zoned4); yaerr = true
        }
        if (dd5 === -1) {
          stor.zoned5.corrige(false); stor.elemaco.push(stor.zoned5); yaerr = true
        }
        if ((dd4 === dd5) && (dd4 !== -1)) {
          stor.zoned5.corrige(false); stor.elemaco.push(stor.zoned5)
          if (stor.adire2.indexOf('Tu nommes deux fois la même droite !') === -1) { stor.adire2.push('Tu nommes deux fois la même droite !') }
          yaerr = true
        }

        if (stor.pbencours.indexOf(stor.listeDeroulanteX.reponse) === -1) {
          stor.listeDeroulanteX.corrige(false)
          stor.elemaco.push(stor.listeDeroulanteX)
          yaerr = true
        }

        if (stor.listeDeroulanteX2.reponse !== 'parallèles') {
          stor.listeDeroulanteX2.corrige(false)
          stor.elemaco.push(stor.listeDeroulanteX2)
          yaerr = true
        }
      }
      if (stor.pbencours === 'angles adjacents supplémentaires') {
        let pp1 = -1
        let pp2 = -1
        let pp3 = -1
        const p1 = stor.zonep1.reponse()
        const p2 = stor.zonep2.reponse()
        const p3 = stor.zonep3.reponse()
        if (p1 === stor.lettres[1]) { pp1 = 1 }
        if (p1 === stor.lettres[3]) { pp1 = 2 }
        if (p1 === stor.lettres[5]) { pp1 = 3 }
        if (p2 === stor.lettres[1]) { pp2 = 1 }
        if (p2 === stor.lettres[3]) { pp2 = 2 }
        if (p2 === stor.lettres[5]) { pp2 = 3 }
        if (p3 === stor.lettres[1]) { pp3 = 1 }
        if (p3 === stor.lettres[3]) { pp3 = 2 }
        if (p3 === stor.lettres[5]) { pp3 = 3 }
        if (pp1 === -1) {
          stor.zonep1.corrige(false); stor.elemaco.push(stor.zonep1); yaerr = true
        }
        if (pp2 === -1) {
          stor.zonep2.corrige(false); stor.elemaco.push(stor.zonep2); yaerr = true
        }
        if (pp3 === -1) {
          stor.zonep3.corrige(false); stor.elemaco.push(stor.zonep3); yaerr = true
        }
        if (pp1 === pp2) {
          stor.zonep2.corrige(false); stor.elemaco.push(stor.zonep2); stor.adire2.push('Tu nommes deux fois le même point !'); yaerr = true
        }
        if ((pp1 === pp3) && (pp1 !== -1)) {
          stor.zonep3.corrige(false); stor.elemaco.push(stor.zonep3)
          if (stor.adire2.indexOf('Tu nommes deux fois le même point !') === -1) { stor.adire2.push('Tu nommes deux fois le même point !') }
          yaerr = true
        }
        if ((pp2 === pp3) && (pp2 !== -1)) {
          stor.zonep3.corrige(false); stor.elemaco.push(stor.zonep3)
          if (stor.adire2.indexOf('Tu nommes deux fois le même point !') === -1) { stor.adire2.push('Tu nommes deux fois le même point !') }
          yaerr = true
        }
      }
    } else {
      if (['angles alternes-internes', 'angles alternes-externes', 'angles correspondants'].indexOf(stor.pbencours) !== -1) {
        if (stor.pbencours.indexOf(stor.listeDeroulanteX.reponse) === -1) {
          stor.listeDeroulanteX.corrige(false)
          stor.elemaco.push(stor.listeDeroulanteX)
          yaerr = true
        }
        const a = stor.zoneeg.reponse().replace('°', '')
        if (Number(a) !== valeurangle(stor.angle2)) { stor.zoneeg.corrige(false); stor.elemaco.push(stor.zoneeg); yaerr = true }
      }
      if (stor.pbencours === 'angles adjacents supplémentaires') {
        const a1 = stor.zoneas1.reponse().replace('°', '')
        if (Number(a1) !== valeurangle(stor.angle1)) { stor.zoneas1.corrige(false); stor.elemaco.push(stor.zoneas1); yaerr = true }
        const a2 = stor.zoneas2.reponse().replace('°', '')
        if (Number(a2) !== valeurangle(stor.angle2)) { stor.zoneas2.corrige(false); stor.elemaco.push(stor.zoneas2); yaerr = true }
      }
      if (stor.pbencours === 'angles opposés par le sommet') {
        if (stor.listeDeroulanteX.reponse !== 'opposés par le sommet') {
          stor.listeDeroulanteX.corrige(false)
          stor.elemaco.push(stor.listeDeroulanteX)
          yaerr = true
        }
        const a = stor.zoneeg.reponse().replace('°', '')
        if (Number(a) !== valeurangle(stor.angle2)) { stor.zoneeg.corrige(false); stor.elemaco.push(stor.zoneeg); yaerr = true }
      }
    }

    stor.adire1 = nettoie(stor.adire1)
    stor.adire2 = nettoie(stor.adire2)
    stor.elemaco = nettoie(stor.elemaco)
    return !yaerr
  } // bonneReponse

  function nettoie (tab) {
    let i
    const ret = []
    for (i = 0; i < tab.length; i++) {
      if (ret.indexOf(tab[i]) === -1) { ret.push(tab[i]) }
    }
    return ret
  }

  function Nomspos (nb) {
    switch (nb) {
      case 0: return [stor.lettres[2] + stor.lettres[0] + stor.lettres[7], stor.lettres[7] + stor.lettres[0] + stor.lettres[2]]
      case 1: return [stor.lettres[2] + stor.lettres[0] + stor.lettres[1], stor.lettres[2] + stor.lettres[0] + stor.lettres[4], stor.lettres[1] + stor.lettres[0] + stor.lettres[2], stor.lettres[4] + stor.lettres[0] + stor.lettres[2]]
      case 2: return [stor.lettres[6] + stor.lettres[0] + stor.lettres[1], stor.lettres[6] + stor.lettres[0] + stor.lettres[4], stor.lettres[4] + stor.lettres[0] + stor.lettres[6], stor.lettres[1] + stor.lettres[0] + stor.lettres[6]]
      case 3: return [stor.lettres[7] + stor.lettres[0] + stor.lettres[6], stor.lettres[6] + stor.lettres[0] + stor.lettres[7]]
      case 4: return [stor.lettres[3] + stor.lettres[1] + stor.lettres[0], stor.lettres[0] + stor.lettres[1] + stor.lettres[3], stor.lettres[3] + stor.lettres[1] + stor.lettres[7], stor.lettres[7] + stor.lettres[1] + stor.lettres[3]]
      case 5: return [stor.lettres[3] + stor.lettres[1] + stor.lettres[4], stor.lettres[4] + stor.lettres[1] + stor.lettres[3]]
      case 6: return [stor.lettres[5] + stor.lettres[1] + stor.lettres[4], stor.lettres[4] + stor.lettres[1] + stor.lettres[5]]
      case 7: return [stor.lettres[5] + stor.lettres[1] + stor.lettres[0], stor.lettres[5] + stor.lettres[1] + stor.lettres[7], stor.lettres[7] + stor.lettres[1] + stor.lettres[5], stor.lettres[0] + stor.lettres[1] + stor.lettres[5]]
    }
  }

  function valeurangle (n) {
    switch (n) {
      case 0:
      case 2:
      case 4:
      case 6: if (stor.obtus) { return stor.valobtus } else { return stor.valaigu }
      default: if (!stor.obtus) { return stor.valobtus } else { return stor.valaigu }
    }
  }

  function desatout () {
    if (['angles alternes-internes', 'angles alternes-externes', 'angles correspondants'].indexOf(stor.pbencours) !== -1) {
      if (ds.Etape_Description) {
        stor.zoned1.disable()
        stor.zoned2.disable()
        stor.zoned3.disable()
      }
    }
    if (stor.pbencours === 'angles adjacents supplémentaires') {
      if (ds.Etape_Description) {
        stor.zoneegd.disable()
      }
    }
    if (stor.pbencours === 'angles opposés par le sommet') {
      if (ds.Etape_Description) {
        stor.zoned1.disable()
        stor.zoned2.disable()
        stor.zonep1.disable()
      }
    }

    /// /pour la quest
    if (stor.questencours === 'P') {
      if (['angles alternes-internes', 'angles alternes-externes', 'angles correspondants'].indexOf(stor.pbencours) !== -1) {
        stor.listeDeroulanteX.disable()
        stor.zoned4.disable()
        stor.zoned5.disable()
        stor.listeDeroulanteX2.disable()
      }
      if (stor.pbencours === 'angles adjacents supplémentaires') {
        stor.zonep1.disable()
        stor.zonep2.disable()
        stor.zonep3.disable()
      }
    } else {
      if (['angles alternes-internes', 'angles alternes-externes', 'angles correspondants'].indexOf(stor.pbencours) !== -1) {
        stor.listeDeroulanteX.disable()
        stor.zoneeg.disable()
      }
      if (stor.pbencours === 'angles adjacents supplémentaires') {
        stor.zoneas1.disable()
        stor.zoneas2.disable()
      }
      if (stor.pbencours === 'angles opposés par le sommet') {
        stor.listeDeroulanteX.disable()
        stor.zoneeg.disable()
      }
    }
  }

  function passetoutvert () {
    if (['angles alternes-internes', 'angles alternes-externes', 'angles correspondants'].indexOf(stor.pbencours) !== -1) {
      if (ds.Etape_Description) {
        stor.zoned1.corrige(true)
        stor.zoned2.corrige(true)
        stor.zoned3.corrige(true)
      }
    }
    if (stor.pbencours === 'angles adjacents supplémentaires') {
      if (ds.Etape_Description) {
        stor.zoneegd.corrige(true)
      }
    }
    if (stor.pbencours === 'angles opposés par le sommet') {
      if (ds.Etape_Description) {
        stor.zoned1.corrige(true)
        stor.zoned2.corrige(true)
        stor.zonep1.corrige(true)
      }
    }

    /// /pour la quest
    if (stor.questencours === 'P') {
      if (['angles alternes-internes', 'angles alternes-externes', 'angles correspondants'].indexOf(stor.pbencours) !== -1) {
        stor.listeDeroulanteX.corrige(true)
        stor.zoned4.corrige(true)
        stor.zoned5.corrige(true)
        stor.listeDeroulanteX2.corrige(true)
      }
      if (stor.pbencours === 'angles adjacents supplémentaires') {
        stor.zonep1.corrige(true)
        stor.zonep2.corrige(true)
        stor.zonep3.corrige(true)
      }
    } else {
      if (['angles alternes-internes', 'angles alternes-externes', 'angles correspondants'].indexOf(stor.pbencours) !== -1) {
        stor.listeDeroulanteX.corrige(true)
        stor.zoneeg.corrige(true)
      }
      if (stor.pbencours === 'angles adjacents supplémentaires') {
        stor.zoneas1.corrige(true)
        stor.zoneas2.corrige(true)
      }
      if (stor.pbencours === 'angles opposés par le sommet') {
        stor.listeDeroulanteX.corrige(true)
        stor.zoneeg.corrige(true)
      }
    }
  }

  function creeLesdiv () {
    stor.lesdiv = {}
    stor.lesdiv.conteneur = j3pAddElt(me.zonesElts.MG, 'div', {
      style: me.styles.etendre('toutpetit.enonce', { padding: '10px' })
    })
    const ttt = addDefaultTable(stor.lesdiv.conteneur, 2, 3)
    ttt[0][0].style.textAlign = 'center'
    ttt[0][2].style.textAlign = 'center'
    j3pAffiche(ttt[0][0], null, '<i><b>Schéma</b></i>')
    stor.lesdiv.consigne = ttt[0][2]
    stor.lesdiv.figure = ttt[1][0]
    ttt[1][2].style.verticalAlign = 'top'
    const gg = ttt[1][2]
    ttt[0][1].style.width = '5px'
    const yu = addDefaultTable(gg, 1, 3)
    stor.lesdiv.consigne2 = yu[0][0]
    yu[0][1].style.width = '10px'
    stor.lesdiv.correction = yu[0][2]
    stor.lesdiv.travail = j3pAddElt(gg, 'div')
    stor.lesdiv.travail1 = j3pAddElt(stor.lesdiv.travail, 'div')
    stor.lesdiv.explications1 = j3pAddElt(stor.lesdiv.travail, 'div')
    stor.lesdiv.travail2 = j3pAddElt(stor.lesdiv.travail, 'div')
    stor.lesdiv.travail3 = j3pAddElt(stor.lesdiv.travail, 'div')
    stor.lesdiv.explications = j3pAddElt(stor.lesdiv.travail, 'div')
    stor.lesdiv.solution = j3pAddElt(stor.lesdiv.travail, 'div')
    stor.lesdiv.bouton = j3pAddElt(stor.lesdiv.travail, 'div')
    stor.lesdiv.explications.style.color = me.styles.cfaux
    stor.lesdiv.explications1.style.color = me.styles.cfaux
    stor.lesdiv.solution.style.color = me.styles.petit.correction.color
    ttt[0][0].classList.add('enonce')
    stor.lesdiv.figure.classList.add('enonce')
  }

  function initSection () {
    ds.pe = []
    // Construction de la page
    me.construitStructurePage('presentation3')

    let letitredeb
    let pasaccept = false
    if (ds.Exercice === 'Parallélisme') { stor.questpos = ['P']; letitredeb = 'Démontrer en utilisant des angles' }
    if (ds.Exercice === 'Calcul') { stor.questpos = ['C']; letitredeb = 'Calculer un angle' }
    if (ds.Exercice === 'Les deux') { stor.questpos = ['C', 'P']; letitredeb = 'Deux droites coupées par une sécante' }

    if ((stor.questpos.length === 1) && (stor.questpos[0] === 'P')) { pasaccept = true }

    stor.pbpos = []
    if (ds.oppose_sommet && (!pasaccept)) { stor.pbpos.push('angles opposés par le sommet') }
    if (ds.alternes_internes) { stor.pbpos.push('angles alternes-internes') }
    if (ds.correspondants) { stor.pbpos.push('angles correspondants') }
    if (ds.alternes_externes) { stor.pbpos.push('angles alternes-externes') }
    if (ds.supplementaires_et_adjacents) { stor.pbpos.push('angles adjacents supplémentaires') }
    if (stor.pbpos.length === 0) {
      j3pShowError('Paramétrage: Aucun exercice possible !')
      stor.pbpos.push('angles correspondants')
    }

    me.typederreurs = [0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0]
    me.afficheTitre(letitredeb)

    stor.pbfaits = []
    stor.questfait = []

    // chargement mtg
    getMtgCore({ withMathJax: true })
      .then(
        // success
        (mtgAppLecteur) => {
          stor.mtgAppLecteur = mtgAppLecteur
          stor.listePourCalc = mtgAppLecteur.createList('TWF0aEdyYXBoSmF2YTEuMAAAABI+TMzNAAJmcv###wEA#wEAAAAAAAAAAAQsAAACvQAAAQEAAAAAAAAAAQAAAAP#####AAAAAQAKQ0NhbGNDb25zdAD#####AAJwaQAWMy4xNDE1OTI2NTM1ODk3OTMyMzg0Nv####8AAAABAApDQ29uc3RhbnRlQAkh+1RELRj#####AAAAAQAHQ0NhbGN1bAD#####AAFlAAZleHAoMSn#####AAAAAgAJQ0ZvbmN0aW9uBwAAAAE#8AAAAAAAAAAAAAIA#####wABeAABMAAAAAEAAAAAAAAAAP###############w==')
          enonceMain()
        },
        // failure
        (error) => {
          j3pShowError(error, { message: 'mathgraph n’est pas correctement chargé', mustNotify: true })
        })
      // plantage dans le code de success
      .catch(j3pShowError)
  }

  function enonceMain () {
    me.videLesZones()

    creeLesdiv()

    stor.lesdiv.travail.classList.add('travail')
    stor.lesdiv.consigne.classList.add('enonce')
    stor.lesdiv.consigne2.classList.add('enonce')
    me.zonesElts.MG.classList.add('fond')
    // selectionne pb et mode

    let cptuse = 0
    let oksort = false
    do {
      stor.pbencours = stor.pbpos[j3pGetRandomInt(0, (stor.pbpos.length - 1))]
      cptuse++
      oksort = (stor.pbfaits.indexOf(stor.pbencours) === -1)
    } while ((!oksort) && (cptuse < 50))
    stor.pbfaits.push(stor.pbencours)

    let advi2 = ''
    switch (stor.pbencours) {
      case 'angles opposés par le sommet':
        advi2 = 'opposés par le sommet.'
        break
      case 'angles alternes-internes':
        advi2 = 'alternes-internes.'
        break
      case 'angles correspondants':
        advi2 = 'correspondants.'
        break
      case 'angles alternes-externes':
        advi2 = 'alternes-externes.'
        break
      case 'angles adjacents supplémentaires':
        advi2 = 'adjacents et supplémentaires.'
        break
      default :
        console.error(Error('cas non géré'), stor.pbencours)
    }

    stor.advi2 = advi2

    if (stor.pbencours === 'angles opposés par le sommet') {
      stor.questencours = 'C'
    } else {
      cptuse = 0
      oksort = false
      do {
        stor.questencours = stor.questpos[j3pGetRandomInt(0, (stor.questpos.length - 1))]
        cptuse++
        oksort = (stor.questfait.indexOf(stor.questencours) === -1)
      } while ((!oksort) && (cptuse < 50))
    }
    stor.questfait.push(stor.questencours)

    /// la fig
    stor.lafig = new AfficheFig()

    /// la question
    stor.nomsposangle1 = Nomspos(stor.angle1)
    stor.nomsposangle2 = Nomspos(stor.angle2)
    stor.nomchoisiangle1 = stor.nomsposangle1[j3pGetRandomInt(0, stor.nomsposangle1.length - 1)]
    stor.nomchoisiangle2 = stor.nomsposangle2[j3pGetRandomInt(0, stor.nomsposangle2.length - 1)]

    if (stor.pbencours === 'angles opposés par le sommet') {
      stor.nomchoisiangle1 = stor.nomsposangle1[0]
      stor.nomchoisiangle2 = stor.nomsposangle2[0]
    }
    stor.valobtus = j3pGetRandomInt(92, 164)
    stor.valaigu = 180 - stor.valobtus
    j3pAffiche(stor.lesdiv.consigne, null, '<b>Complète la démonstration ci-dessous</b>')

    /// affiche les données
    j3pAffiche(stor.lesdiv.consigne2, null, '<b><u>Données</u></b> \n')

    if (['angles alternes-internes', 'angles alternes-externes', 'angles correspondants'].indexOf(stor.pbencours) !== -1) {
      if (stor.questencours === 'P') {
        j3pAffiche(stor.lesdiv.consigne2, null, '$\\widehat{' + stor.nomchoisiangle1 + '}$ = $' + valeurangle(stor.angle1) + '$°  et $\\widehat{' + stor.nomchoisiangle2 + '}$ = $' + valeurangle(stor.angle2) + '$°')
      } else {
        j3pAffiche(stor.lesdiv.consigne2, null, '$\\widehat{' + stor.nomchoisiangle1 + '} = ' + valeurangle(stor.angle1) + '$° \nLes droites  (' + stor.nndd11[0] + ') et (' + stor.nndd22[0] + ') sont parallèles.')
      }
    }
    if (stor.pbencours === 'angles adjacents supplémentaires') {
      if (stor.questencours === 'P') {
        j3pAffiche(stor.lesdiv.consigne2, null, '$\\widehat{' + stor.nomchoisiangle1 + '} = ' + valeurangle(stor.angle1) + '$°  et $\\widehat{' + stor.nomchoisiangle2 + '} = ' + valeurangle(stor.angle2) + '$° \nLes angles $\\widehat{' + stor.nomchoisiangle1 + '}$ et $\\widehat{' + stor.nomchoisiangle2 + '}$ sont adjacents.')
      } else {
        const p1 = stor.nomchoisiangle1[0]
        const p2 = stor.nomchoisiangle1[2]
        const p3 = stor.nomchoisiangle2[0]
        const p4 = stor.nomchoisiangle2[2]
        const PP = (p1 === p3 || p1 === p4) ? p2 : p1
        const PF = (p3 === p1 || p3 === p2) ? p4 : p3
        j3pAffiche(stor.lesdiv.consigne2, null, '$\\widehat{' + stor.nomchoisiangle1 + '} = ' + valeurangle(stor.angle1) + '$°  \nLes points ' + PP + ' , ' + stor.nomchoisiangle1[1] + ' et ' + PF + ' sont alignés.')
      }
    }
    if (stor.pbencours === 'angles opposés par le sommet') {
      j3pAffiche(stor.lesdiv.consigne2, null, '$\\widehat{' + stor.nomchoisiangle1 + '} = ' + valeurangle(stor.angle1) + '$° ')
    }

    /// pour la descrip
    if (['angles alternes-internes', 'angles alternes-externes', 'angles correspondants'].indexOf(stor.pbencours) !== -1) {
      if (ds.Etape_Description) {
        let bhu = 'parallèles'
        if (stor.questencours === 'P') bhu = ''
        j3pAffiche(stor.lesdiv.travail1, null, '<b><u>Démonstration</u></b> \n')
        const jjk = addDefaultTable(stor.lesdiv.travail1, 1, 7)
        j3pAffiche(jjk[0][0], null, 'Les droites ' + bhu + ' &nbsp;')
        stor.zoned1 = new ZoneStyleMathquill1(jjk[0][1], {
          restric: 'ABCDEFGHIJKLMNOPQRSTUVWXYZabcdefghijklmnopqrstuvwxyz[]().^$',
          limitenb: 9,
          limite: 3,
          enter: () => { me.sectionCourante() }
        })
        j3pAffiche(jjk[0][2], null, '&nbsp; et &nbsp;')
        stor.zoned2 = new ZoneStyleMathquill1(jjk[0][3], {
          restric: 'ABCDEFGHIJKLMNOPQRSTUVWXYZabcdefghijklmnopqrstuvwxyz[]().^$',
          limitenb: 9,
          limite: 3,
          enter: () => { me.sectionCourante() }
        })
        j3pAffiche(jjk[0][4], null, '&nbsp; sont coupées par la sécante &nbsp;')
        stor.zoned3 = new ZoneStyleMathquill1(jjk[0][5], {
          restric: 'ABCDEFGHIJKLMNOPQRSTUVWXYZabcdefghijklmnopqrstuvwxyz[]().^$',
          limitenb: 9,
          limite: 3,
          enter: () => { me.sectionCourante() }

        })
        j3pAffiche(jjk[0][6], null, '&nbsp; .')
      } else {
        const bhu = stor.questencours === 'P' ? '' : 'parallèles'
        j3pAffiche(stor.lesdiv.travail1, null, '<b><u>Démonstration</u></b> \nLes droites ' + bhu + ' (' + stor.nndd11[0] + ') et (' + stor.nndd22[0] + ') sont coupées par la sécante (' + stor.nnSec[0] + ').')
      }
    }
    if (stor.pbencours === 'angles adjacents supplémentaires') {
      if (ds.Etape_Description) {
        j3pAffiche(stor.lesdiv.travail1, null, '<b><u>Démonstration</u></b>\n')
        const ttti = addDefaultTable(stor.lesdiv.travail1, 1, 3)
        j3pAffiche(ttti[0][0], null, 'La somme des angles adjacents $\\widehat{' + stor.nomchoisiangle1 + '}$ et $\\widehat{' + stor.nomchoisiangle2 + '}$ est égale à &nbsp;')
        stor.zoneegd = new ZoneStyleMathquill1(ttti[0][1], {
          restric: '0123456789°',
          limitenb: 9,
          limite: 3,
          enter: me.sectionCourante.bind(me)
        })
        j3pAffiche(ttti[0][2], null, '&nbsp; , ')
      } else {
        j3pAffiche(stor.lesdiv.travail1, null, '<b><u>Démonstration</u></b>\n')
        j3pAffiche(stor.lesdiv.travail1, null, 'La somme des angles adjacents $\\widehat{' + stor.nomchoisiangle1 + '}$ et $\\widehat{' + stor.nomchoisiangle2 + '}$ est égale à $180°$')
      }
    }
    if (stor.pbencours === 'angles opposés par le sommet') {
      if (ds.Etape_Description) {
        j3pAffiche(stor.lesdiv.travail1, null, '<b><u>Démonstration</u></b>')
        const ttti = addDefaultTable(stor.lesdiv.travail1, 1, 7)
        j3pAffiche(ttti[0][0], null, 'Les droites &nbsp;')
        stor.zoned1 = new ZoneStyleMathquill1(ttti[0][1], {
          restric: 'ABCDEFGHIJKLMNOPQRSTUVWXYZabcdefghijklmnopqrstuvwxyz()[]',
          limitenb: 9,
          limite: 3,
          enter: () => { me.sectionCourante() }
        })
        j3pAffiche(ttti[0][2], null, '&nbsp; et &nbsp;')
        stor.zoned2 = new ZoneStyleMathquill1(ttti[0][3], {
          restric: 'ABCDEFGHIJKLMNOPQRSTUVWXYZabcdefghijklmnopqrstuvwxyz()[]',
          limitenb: 9,
          limite: 3,
          enter: () => { me.sectionCourante() }
        })
        j3pAffiche(ttti[0][4], null, '&nbsp; sont sécantes en &nbsp;')
        stor.zonep1 = new ZoneStyleMathquill1(ttti[0][5], {
          restric: 'ABCDEFGHIJKLMNOPQRSTUVWXYZabcdefghijklmnopqrstuvwxyz',
          limitenb: 1,
          limite: 1,
          enter: () => { me.sectionCourante() }
        })
        j3pAffiche(ttti[0][6], null, '&nbsp;.')
      } else {
        j3pAffiche(stor.lesdiv.travail1, null, ' <b><u>Démonstration</u></b> \nLes droites  (' + stor.nndd22[0] + ') et (' + stor.nnSec[4] + ') sont sécantes en ' + stor.lettres[0] + '.')
      }
    }

    /// /pour la quest
    if (stor.questencours === 'P') {
      if (['angles alternes-internes', 'angles alternes-externes', 'angles correspondants'].indexOf(stor.pbencours) !== -1) {
        const yyy = addDefaultTable(stor.lesdiv.travail2, 1, 7)
        j3pAffiche(yyy[0][0], null, 'Les angles &nbsp;')
        stor.listeDeroulanteX = ListeDeroulante.create(yyy[0][1], ['Choisir', 'correspondants', 'alternes-internes', 'alternes-externes', 'adjacents', 'opposés'], {
          centre: true
        })
        j3pAffiche(yyy[0][2], null, '&nbsp; $\\widehat{' + stor.nomchoisiangle1 + '}$ et $\\widehat{' + stor.nomchoisiangle2 + '}$ sont égaux, ')

        const yyy2 = addDefaultTable(stor.lesdiv.travail3, 1, 7)
        j3pAffiche(yyy2[0][0], null, 'donc les droites &nbsp;')
        stor.zoned4 = new ZoneStyleMathquill1(yyy2[0][1], {
          restric: 'ABCDEFGHIJKLMNOPQRSTUVWXYZabcdefghijklmnopqrstuvwxyz[]().^$',
          limitenb: 9,
          limite: 3,
          enter: () => { me.sectionCourante() }
        })
        j3pAffiche(yyy2[0][2], null, '&nbsp; et &nbsp;')
        stor.zoned5 = new ZoneStyleMathquill1(yyy2[0][3], {
          restric: 'ABCDEFGHIJKLMNOPQRSTUVWXYZabcdefghijklmnopqrstuvwxyz[]().^$',
          limitenb: 9,
          limite: 3,
          enter: () => { me.sectionCourante() }
        })
        j3pAffiche(yyy2[0][4], null, '&nbsp; sont &nbsp;')
        stor.listeDeroulanteX2 = ListeDeroulante.create(yyy2[0][5], ['Choisir', 'parallèles', 'perpendiculaires', 'simplement sécantes'], {
          centre: true
        })
        j3pAffiche(yyy2[0][6], null, '&nbsp; . ')
      }
      if (stor.pbencours === 'angles adjacents supplémentaires') {
        const yyy = addDefaultTable(stor.lesdiv.travail2, 1, 7)
        j3pAffiche(yyy[0][0], null, 'donc les points &nbsp;')
        j3pAffiche(yyy[0][2], null, '&nbsp; , &nbsp;')
        j3pAffiche(yyy[0][4], null, '&nbsp; et &nbsp;')
        j3pAffiche(yyy[0][6], null, '&nbsp; sont alignés. ')
        stor.zonep1 = new ZoneStyleMathquill1(yyy[0][1], {
          restric: 'ABCDEFGHIJKLMNOPQRSTUVWXYZabcdefghijklmnopqrstuvwxyz',
          limitenb: 1,
          limite: 1,
          enter: () => { me.sectionCourante() }
        })
        stor.zonep2 = new ZoneStyleMathquill1(yyy[0][3], {
          restric: 'ABCDEFGHIJKLMNOPQRSTUVWXYZabcdefghijklmnopqrstuvwxyz',
          limitenb: 1,
          limite: 1,
          enter: () => { me.sectionCourante() }
        })
        stor.zonep3 = new ZoneStyleMathquill1(yyy[0][5], {
          restric: 'ABCDEFGHIJKLMNOPQRSTUVWXYZabcdefghijklmnopqrstuvwxyz',
          limitenb: 1,
          limite: 1,
          enter: () => { me.sectionCourante() }
        })
      }
    } else {
      if (['angles alternes-internes', 'angles alternes-externes', 'angles correspondants'].indexOf(stor.pbencours) !== -1) {
        const yyy = addDefaultTable(stor.lesdiv.travail2, 1, 3)
        j3pAffiche(yyy[0][0], null, 'Les angles $\\widehat{' + stor.nomchoisiangle2 + '}$ et $\\widehat{' + stor.nomchoisiangle1 + '}$ sont &nbsp;')
        stor.listeDeroulanteX = ListeDeroulante.create(yyy[0][1], ['Choisir', 'correspondants', 'alternes-internes', 'alternes-externes', 'adjacents', 'opposés'], {
          centre: true
        })
        j3pAffiche(yyy[0][2], null, '&nbsp; ,')

        const yyy2 = addDefaultTable(stor.lesdiv.travail2, 1, 2)
        j3pAffiche(yyy2[0][0], null, 'donc $\\widehat{' + stor.nomchoisiangle2 + '}$ = $\\widehat{' + stor.nomchoisiangle1 + '}$ = &nbsp;')
        stor.zoneeg = new ZoneStyleMathquill1(yyy2[0][1], {
          restric: '0123456789°',
          limitenb: 9,
          limite: 3,
          enter: () => { me.sectionCourante() }
        })
      }
      if (stor.pbencours === 'angles adjacents supplémentaires') {
        const tttty = addDefaultTable(stor.lesdiv.travail2, 3, 3)
        j3pAffiche(tttty[0][0], null, 'donc &nbsp;')
        j3pAffiche(tttty[0][2], null, ' $180 - \\widehat{' + stor.nomchoisiangle1 + '}$')
        const ab = addDefaultTable(tttty[1][2], 1, 2)
        j3pAffiche(ab[0][0], null, ' $180 - $&nbsp;')
        j3pAffiche(tttty[0][1], null, '$\\widehat{' + stor.nomchoisiangle2 + '}$ =&nbsp;')
        j3pAffiche(tttty[1][1], null, '$\\widehat{' + stor.nomchoisiangle2 + '}$ =&nbsp;')
        j3pAffiche(tttty[2][1], null, '$\\widehat{' + stor.nomchoisiangle2 + '}$ =&nbsp;')
        const ab2 = addDefaultTable(tttty[2][2], 1, 1)[0][0]

        stor.zoneas1 = new ZoneStyleMathquill1(ab[0][1], {
          restric: '0123456789°',
          limitenb: 9,
          limite: 3,
          enter: () => { me.sectionCourante() }
        })
        stor.zoneas2 = new ZoneStyleMathquill1(ab2, {
          restric: '0123456789°',
          limitenb: 9,
          limite: 3,
          enter: () => { me.sectionCourante() }
        })
      }
      if (stor.pbencours === 'angles opposés par le sommet') {
        const tttty = addDefaultTable(stor.lesdiv.travail2, 1, 3)
        j3pAffiche(tttty[0][0], null, 'Les angles $\\widehat{' + stor.nomchoisiangle2 + '}$ et $\\widehat{' + stor.nomchoisiangle1 + '}$ sont &nbsp;')
        stor.listeDeroulanteX = ListeDeroulante.create(tttty[0][1], ['Choisir', 'correspondants', 'alternes-internes', 'alternes-externes', 'adjacents', 'opposés par le sommet'], {
          centre: true
        })
        j3pAffiche(tttty[0][2], null, '&nbsp; ,')

        const ttetty = addDefaultTable(stor.lesdiv.travail3, 1, 3)
        j3pAffiche(ttetty[0][0], null, 'donc $\\widehat{' + stor.nomchoisiangle2 + '}$ = $\\widehat{' + stor.nomchoisiangle1 + '}$ = &nbsp;')
        stor.zoneeg = new ZoneStyleMathquill1(ttetty[0][1], {
          restric: '0123456789°',
          limitenb: 9,
          limite: 3,
          enter: () => { me.sectionCourante() }
        })
      }
    }

    // Obligatoire

    me.finEnonce()
    stor.lesdiv.bouton.appendChild(me.buttonsElts.valider)
  }

  switch (me.etat) {
    case 'enonce':
      // code exécuté au lancement de la section
      if (me.debutDeLaSection) {
        initSection()
      } else {
        enonceMain()
      }
      break // case "enonce":

    case 'correction':
      // On teste si une réponse a été saisie

      stor.lesdiv.explications.classList.remove('explique')
      stor.lesdiv.solution.classList.remove('correction')

      stor.yaexplik1 = false
      stor.yaexplik2 = false
      stor.yaco = false

      if (!yareponse()) {
        // ici on donne le message à afficher si une ou plusieurs zone(s) n’est pas complétée (tout d’abord on met en rouge : styles.cfaux)
        stor.lesdiv.correction.style.color = this.styles.cfaux
        stor.lesdiv.correction.innerHTML = reponseIncomplete
        this.afficheBoutonValider()
      } else {
        // Bonne réponse
        if (bonneReponse()) {
          this.score += j3pArrondi(1 / ds.nbetapes, 1)
          stor.lesdiv.correction.style.color = this.styles.cbien
          stor.lesdiv.correction.innerHTML = cBien
          desatout()
          this.typederreurs[0]++
          this.cacheBoutonValider()
          this.etat = 'navigation'
          this.sectionCourante()
        } else {
          // Pas de bonne réponse
          stor.lesdiv.correction.style.color = this.styles.cfaux

          // A cause de la limite de temps :
          if (this.isElapsed) { // limite de temps
            afcofo(true)
            this.sectionCourante('navigation')
          } else {
            stor.lesdiv.correction.innerHTML = cFaux

            if (this.essaiCourant < ds.nbchances) {
              stor.lesdiv.correction.innerHTML += '<br>' + essaieEncore

              afcofo(false)

              this.typederreurs[1]++
              // indication éventuelle ici
            } else {
              stor.lesdiv.correction.innerHTML += '<br>' + regardeCorrection
              afcofo(true)
              this.typederreurs[2]++
              this.sectionCourante('navigation')
            }
          }
        }
      }

      if (stor.yaexplik1) stor.lesdiv.explications1.classList.add('explique')
      if (stor.yaexplik2) stor.lesdiv.explications.classList.add('explique')
      if (stor.yaco) stor.lesdiv.solution.classList.add('correction')

      // Obligatoire
      this.finCorrection()
      break // case "correction":

    case 'navigation':
      if (this.sectionTerminee()) {
        // On détermine la phrase d’état renvoyée par la section
        if ((me.typederreurs[4] / ds.nbitems) > 0.3) {
          this.parcours.pe = ds.pe_1
        } else {
          this.parcours.pe = ''
        }
      } else {
        this.etat = 'enonce'
      }
      this.finNavigation()
      break // case "navigation":
  }
}
