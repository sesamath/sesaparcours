import $ from 'jquery'
import { j3pDiv, j3pElement, j3pFocus, j3pGetBornesIntervalle, j3pGetRandomInt, j3pNombreBienEcrit, j3pRestriction, j3pValeurde } from 'src/legacy/core/functions'
import { j3pAffiche } from 'src/lib/mathquill/functions'
import textesGeneriques from 'src/lib/core/textes'

const { cBien, cFaux, essaieEncore, tempsDepasse } = textesGeneriques

/*
    JP Vanroyen, Françoise Chomat, Xavier Jérôme
    Cette section n’est utilisée par aucune ressource, faut-il la conserver ?
*/

export const params = {
  outils: ['mathquill'],
  /**
   * Ce qui est paramétrable dans l’éditeur de graphe
   * @type {LegacySectionParamsParametres}
   */
  parametres: [
    ['nbrepetitions', 5, 'entier', 'Nombre de répétitions de la section'],
    ['indication', '', 'string', 'Indication pour chaque répétition'],
    ['limite', 20, 'entier', 'Temps disponible par répétition, en secondes'],
    ['nbchances', 2, 'entier', 'Nombre d’essais par répétition'],
    ['s_a', '[40;70]', 'string', ' parametre a de l’expression a/b +- c*d'],
    ['s_b', '[7;9]', 'string', ' parametre b de l’expression a/b +- c*d'],
    ['s_c', '[2;4]', 'string', ' parametre c de l’expression a/b +- c*d'],
    ['s_d', '[2;6]', 'string', ' parametre d de l’expression a/b +- c*d']
  ]
}

const phraseEnonce = 'Calcule l’expression le plus rapidement possible'
const phraseSolution = 'La solution était'

/**
 * section priorites
 * @this {Parcours}
 */
export default function main () {
  const me = this
  const stor = this.storage

  function determineab () {
    const [amin, amax] = j3pGetBornesIntervalle(me.donneesSection.s_a)
    const [bmin, bmax] = j3pGetBornesIntervalle(me.donneesSection.s_b)
    let a, b
    do {
      a = j3pGetRandomInt(amin, amax)
      b = j3pGetRandomInt(bmin, bmax)
    } while (a % b !== 0)
    return { a, b }
  }
  function determinecd () {
    const [cmin, cmax] = j3pGetBornesIntervalle(me.donneesSection.s_c)
    const [dmin, dmax] = j3pGetBornesIntervalle(me.donneesSection.s_d)
    return {
      c: j3pGetRandomInt(cmin, cmax),
      d: j3pGetRandomInt(dmin, dmax)
    }
  }

  switch (this.etat) {
    case 'enonce': {
      // code exécuté au lancement de la section
      if (this.debutDeLaSection) {
        // Construction de la page
        this.construitStructurePage({ structure: 'presentation2', ratioGauche: 0.6 })
        /*
         Par convention,`
            this.typederreurs[0] = nombre de bonnes réponses
            this.typederreurs[1] = nombre de fois où l’élève a utilisé une chance supplémentaire
            this.typederreurs[2] = nombre de mauvaises réponses
            this.typederreurs[10] = nombre de fois où l’élève a eu faux à cause d’un temps limite
            LES AUTRES peuvent être utilisées par la détermination de la pe dans le cas d’une section qualitative
         */
        this.typederreurs = [0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0]
        this.afficheTitre('Calcul rapide')
        if (this.donneesSection.indication) this.indication(this.zones.IG, this.donneesSection.indication)
      } else {
        // A chaque répétition, on nettoie la scène
        this.videLesZones()
      }

      /// //////////////////////////////////
      /* LE CODE PRINCIPAL DE LA SECTION */
      /// //////////////////////////////////

      j3pDiv(this.zones.MG, 'conteneur1', '', [100, 50], this.styles.petit.enonce)
      const { a, b } = determineab()
      const { c, d } = determinecd()

      // la solution
      stor.solution = a / b * c * d
      // l’input
      stor.input = j3pAffiche('conteneur1', 'affiche', phraseEnonce + '\nA = £a : £b $×$ £c $×$  £d \nA = @1@ ',
        {
          style: { fontSize: '20px', color: '#00F' },
          a,
          b,
          c,
          d,
          input1: { width: '40px', couleur: '#F00' }
        }).inputList[0]
      j3pRestriction('afficheinput1', '0-9')
      j3pFocus('afficheinput1')

      /// //////////////////////////////////////
      /* FIN DU CODE PRINCIPAL DE LA SECTION */
      /// //////////////////////////////////////
      j3pDiv(this.zones.MG, { id: 'correction', contenu: '', coord: [100, 250], style: this.styles.petit.correction })
      this.finEnonce()
      break // case "enonce":
    }

    case 'correction': {
      // On teste si une réponse a été saisie
      const repEleve = j3pValeurde('afficheinput1')
      if (!repEleve && !this.isElapsed) {
        this.reponseManquante('correction')
        j3pFocus('afficheinput1')
        return this.finCorrection() // on reste en correction
      }

      // Une réponse a été saisie (ou la limite de temps est atteinte)
      const solution = stor.solution
      if (repEleve === j3pNombreBienEcrit(solution) || repEleve === String(solution)) {
        // Bonne réponse
        this.score++
        j3pElement('correction').style.color = this.styles.cbien
        j3pElement('correction').innerHTML = cBien
        this.typederreurs[0]++
        return this.finCorrection('navigation', true)
      }

      // Pas de bonne réponse
      j3pElement('correction').style.color = this.styles.cfaux

      // A cause de la limite de temps :
      if (this.isElapsed) { // limite de temps
        j3pElement('correction').innerHTML = tempsDepasse
        this.typederreurs[10]++
        j3pElement('correction').innerHTML += '<br>' + phraseSolution + '&nbsp;: ' + solution
        return this.finCorrection('navigation', true)
      }

      // Réponse fausse sans limite de temps
      j3pElement('correction').innerHTML = cFaux
      if (this.essaiCourant < this.donneesSection.nbchances) {
        j3pElement('correction').innerHTML += '<br>' + essaieEncore
        $('#afficheinput1').val('').focus()
        // il reste des essais, on reste en correction
        return this.finCorrection()
      }

      // Erreur au dernier essai
      j3pElement('correction').innerHTML = cFaux
      j3pElement('correction').innerHTML += '<br>' + phraseSolution + '&nbsp;: ' + solution
      this.typederreurs[2]++
      this.finCorrection('navigation', true)
      break // case "correction":
    }

    case 'navigation':
      if (!this.sectionTerminee()) this.etat = 'enonce'
      this.finNavigation()
      break // case "navigation":
  }
}
