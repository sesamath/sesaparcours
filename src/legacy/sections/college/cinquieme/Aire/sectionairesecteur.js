import { j3pAddElt, j3pAjouteBouton, j3pArrondi, j3pBarre, j3pDetruit, j3pDiv, j3pElement, j3pEmpty, j3pFocus, j3pGetRandomInt, j3pModale, j3pNombreBienEcrit, j3pShowError, j3pValeurde } from 'src/legacy/core/functions'
import ListeDeroulante from 'src/legacy/outils/listeDeroulante/ListeDeroulante'
import { j3pAffiche, j3pDesactive, mqRestriction } from 'src/lib/mathquill/functions'
import { j3pCreeFenetres, j3pAfficheCroixFenetres, j3pToggleFenetres, j3pDetruitFenetres } from 'src/legacy/core/functionsJqDialog'
import { j3pCreeSVG } from 'src/legacy/core/functionsSvg'
import { getMtgCore } from 'src/lib/outils/mathgraph'
import textesGeneriques from 'src/lib/core/textes'

const { cBien, cFaux, essaieEncore, regardeCorrection, reponseIncomplete, tempsDepasse } = textesGeneriques

/**
 * Ajouter une description ici
 * @fileOverview
 * @author Tommy Barroy <t.barroy@laposte.net>
 * @since 2017-10
 * Section revue par Yves pour adpater au clavier virtuel 10/2021
*/

export const params = {
  outils: ['calculatrice', 'mathquill'],
  /**
   * Ce qui est paramétrable dans l’éditeur de graphe
   * @type {LegacySectionParamsParametres}
   */
  parametres: [
    ['nbrepetitions', 3, 'entier', 'Nombre de répétitions de la section'],
    ['indication', '', 'string', 'Indication pour chaque répétition'],
    ['limite', 0, 'entier', 'Temps disponible par répétition, en secondes'],
    ['nbchances', 2, 'entier', 'Nombre d’essais par répétition'],
    ['TypeEnonce', 'les deux', 'liste', '<u>figure</u> , <u>texte</u> ou <u>les deux</u> .', ['figure', 'texte', 'les deux']],
    ['Presentation', true, 'boolean', "<u>true</u>: Les indications de réussite et les boutons 'ok' et 'suivant' sont dans un cadre séparé à droite. <BR><BR> <u>false</u>: Un seul cadre pour tout."],
    // le modèle gère la compatibilité ascendante avec l’ancien paramètre MiseEnPagePlusClaire
    ['theme', 'standard', 'liste', 'Modèle pour l’affichage', ['standard', 'zonesAvecImageDeFond']]
  ]
}

const cssPrefix = 'airesecteur'
const nbEtapes = 4

const textes = {
  couleurexplique: '#A9E2F3',
  couleurcorrec: '#A9F5A9',
  couleurenonce: '#D8D8D8',
  couleurenonceg: '#F3E2A9',
  couleuretiquettes: '#A9E2F3',
  ConsTextCyl: '<b>Nous souhaitons calculer l’aire d’un secteur angulaire</b> <BR> de rayon $r = £a$ £c et d’angle  $£b$° . <BR> (valeur approchée £d) <BR><BR>',
  ConsFigCyl: '<b>Nous souhaitons calculer l’aire de ce secteur angulaire.</b> <BR> (valeur approchée £d)',
  ConsFigCylRayon: 'Unité de longueur : £c .',
  ConsApproche: '(valeur approchée £d £e)',
  ConsFigCylHauteur: '$angle = £a$ ° .',
  Errcarre: 'Tu as confondu $£a^2 = £a \\times £a$ et $£a \\times 2$ .',
  Reecalcul: 'Erreur de calcul',
  ReponseFormule: ' Aire du secteur = ',
  ReponseFormulesuite: ' Aire du secteur $ ≈ $ &1& ',
  ReponseFormulesuite2: ' Aire du secteur $ ≈ $ ',
  Listeformule2_1: 'angle',
  Listeformule2_2: 'rayon',
  Listeformule2_3: 'diamètre',
  Listeformule2_4: 'hauteur',
  Listeformule3_1: '360',
  Listeformule3_2: '10',
  Listeformule3_3: '2',
  Listeformule3_4: '180',
  Listeformule1_3: '$2 \\times \\pi \\times r$',
  Listeformule1_2: '$2 \\times \\pi \\times r²$',
  Listeformule1_1: '$\\pi \\times r²$',
  Listeformule1_4: '$4 \\times \\pi$',
  Choisir: 'Choisir',
  unite: ['mm', 'cm', 'dm', 'm', 'dam', 'hm', 'km'],
  approche: ['à l’unité', 'au dixième', 'au centième', 'au millième'],
  pardef: [' par défaut', ' par excès']
}

/**
 * section airesecteur
 * @this {Parcours}
 */
export default function main () {
  const me = this
  const stor = this.storage
  const indexEtapeCourante = (me.questionCourante ?? 1) % nbEtapes // de 0 à 3 car nbetapes vaut toujours 4

  function boutonfen () {
    j3pToggleFenetres('Calculatrice')
  }

  function chinput1 () {
    stor.couleurinput1 = null
    j3pElement('phraserep2inputmq1').style.color = null
  }

  function chinput2 () {
    stor.couleurinput2 = null
    j3pElement('phraserep3inputmq1').style.color = null
  }

  function yareponse () {
    if (me.isElapsed) return true
    if (indexEtapeCourante === 1) {
      for (let i = 2; i > -1; i--) {
        if (!stor.zoneliste[i].changed) {
          stor.zoneliste[i].focus()
          return false
        }
      }
      return true
    }

    if (indexEtapeCourante === 2) {
      if (j3pValeurde('phraserep3inputmq1') === '') {
        j3pFocus('phraserep3inputmq1')
        return false
      }
      if (j3pValeurde('phraserep2inputmq1') === '') {
        j3pFocus('phraserep2inputmq1')
        return false
      }
      return true
    }

    if (indexEtapeCourante === 3) {
      if (j3pValeurde('phraserep2inputmq1') === '') {
        j3pFocus('phraserep2inputmq1')
        return false
      }
      return true
    }

    if (indexEtapeCourante === 0) {
      if (!stor.zoneliste[0].changed) {
        stor.zoneliste[0].focus()
        return false
      }
      if (j3pValeurde('phraserep2inputmq1') === '') {
        j3pFocus('phraserep2inputmq1')
        return false
      }
      return true
    }
  }

  function bonneReponse () {
    // cette fonction renvoie true ou false si du moins une réponse a été saisie
    // (dans le cas contraire, elle n’est pas appelée
    // elle est appelée dans la partie "correction" de la section

    // cas 0 traité à la fin…
    if (indexEtapeCourante === 1) {
      stor.boform1 = false
      stor.boform2 = false
      stor.boform3 = false

      if (stor.zoneliste[1].reponse === textes.Listeformule2_1) {
        stor.boform2 = true
      }
      stor.zoneliste[1].corrige(stor.boform2)

      if (stor.zoneliste[0].reponse === textes.Listeformule1_1) {
        stor.boform1 = true
      }
      stor.zoneliste[0].corrige(stor.boform1)

      if (stor.zoneliste[2].reponse === textes.Listeformule3_1) {
        stor.boform3 = true
      }
      stor.zoneliste[2].corrige(stor.boform3)

      return (stor.boform1) && (stor.boform2) && (stor.boform3)
    }

    if (indexEtapeCourante === 2) {
      const rep1 = parseFloat(j3pValeurde('phraserep2inputmq1').replace(',', '.'))
      const rep2 = parseFloat(j3pValeurde('phraserep3inputmq1').replace(',', '.'))
      stor.boform1 = (Math.abs(rep1 - stor.rayon) < Math.pow(10, -12))
      stor.boform2 = (Math.abs(rep2 - stor.angle) < Math.pow(10, -12))
      if (stor.boform1) {
        j3pElement('phraserep2inputmq1').style.color = me.styles.cbien
      } else { j3pElement('phraserep2inputmq1').style.color = me.styles.cfaux }
      if (stor.boform2) {
        j3pElement('phraserep3inputmq1').style.color = me.styles.cbien
      } else { j3pElement('phraserep3inputmq1').style.color = me.styles.cfaux }
      return (stor.boform1 && stor.boform2)
    }

    if (indexEtapeCourante === 3) {
      const rep1 = parseFloat(j3pValeurde('phraserep2inputmq1').replace(',', '.'))
      stor.boform1 = (Math.abs(rep1 - Math.pow(stor.rayon, 2)) < Math.pow(10, -12))
      if (stor.boform1) {
        j3pElement('phraserep2inputmq1').style.color = me.styles.cbien
      } else { j3pElement('phraserep2inputmq1').style.color = me.styles.cfaux }
      stor.boform2 = (Math.abs(rep1 - stor.rayon * 2) < Math.pow(10, -12))
      return (stor.boform1)
    }

    // indexEtapeCourante === 0
    stor.bonnerep = j3pArrondi(Math.trunc(Math.pow(stor.rayon, 2) * stor.angle * Math.PI / 360 * Math.pow(10, stor.approche)) / Math.pow(10, stor.approche), stor.approche)
    if (stor.pardef === 1) { stor.bonnerep = j3pArrondi(stor.bonnerep + Math.pow(10, -stor.approche), stor.approche) }
    const rep1 = parseFloat(j3pValeurde('phraserep2inputmq1').replace(',', '.'))
    if (Math.abs(rep1 - stor.bonnerep) < Math.pow(10, -12)) {
      j3pElement('phraserep2inputmq1').style.color = me.styles.cbien
      stor.boform2 = true
      stor.couleurinput1 = me.styles.cbien
    } else {
      j3pElement('phraserep2inputmq1').style.color = me.styles.cfaux
      stor.boform2 = false
      stor.couleurinput1 = me.styles.cfaux
    }
    stor.boform1 = stor.zoneliste[0].reponse === stor.unite + '²'
    stor.zoneliste[0].corrige(stor.boform1)

    return stor.boform2 && stor.boform1
  }

  function initSection () {
    // nbetapes n’est pas dans les paramètres, faut le passer au modèle ici
    me.surcharge({ nbetapes: nbEtapes })
    // Construction de la page
    const structure = me.donneesSection.Presentation ? 'presentation1bis' : 'presentation3'
    me.construitStructurePage(structure)

    // DECLARATION DES VARIABLES
    stor.unite = ''
    stor.approche = 0
    stor.numunite = 0
    stor.rayon = 2
    stor.angle = 2
    stor.ListeFormule1 = []
    stor.ListeFormule2 = []
    stor.ListeFormule3 = []
    stor.maphraseformule = ''
    stor.couleurb = null
    stor.couleurc = null
    stor.couleurd = null
    stor.boform1 = false
    stor.boform2 = false
    stor.boform3 = false
    stor.Liste2Formule = []
    stor.Liste3Formule = []
    stor.Liste4Formule = []
    stor.bonnerep = ''

    // contiendra soit consigne1 soit consigne2 (1 fois sur 2)
    stor.consigneaffichee = ''
    /*
     Par convention,`
    `   me.typederreurs[0] = nombre de bonnes réponses
        me.typederreurs[1] = nombre de fois où l’élève a utilisé une chance supplémentaire
        me.typederreurs[2] = nombre de mauvaises réponses
        me.typederreurs[10] = nombre de fois où l’élève a eu faux à cause d’un temps limite
        LES AUTRES peuvent être utilisées par la détermination de la pe dans le cas d’une section qualitative
     */
    me.typederreurs = [0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0]
    me.afficheTitre('Aire d’un secteur de disque')
    if (me.donneesSection.indication) me.indication(me.zones.IG, me.donneesSection.indication)
    loadMtg()
  }

  function loadMtg () {
    getMtgCore({ withMathJax: true })
      .then(
        // success
        (mtgAppLecteur) => {
          stor.mtgAppLecteur = mtgAppLecteur
          enonceMain()
        },
        // failure
        (error) => {
          j3pShowError(error, { message: 'mathgraph n’est pas correctement chargé', mustNotify: true })
        })
      // plantage dans le code de success
      .catch(j3pShowError)
  }

  function enonceMain () {
    stor.zoneliste = []

    if (indexEtapeCourante === 1) {
      me.donneesSection.consencours = me.donneesSection.TypeEnonce
      if ((me.donneesSection.TypeEnonce !== 'figure') && (me.donneesSection.TypeEnonce !== 'texte')) {
        if (j3pGetRandomInt(0, 1) === 1) { me.donneesSection.consencours = 'figure' } else { me.donneesSection.consencours = 'texte' }
      }

      stor.angle = j3pGetRandomInt(15, 259)
      stor.rayon = j3pArrondi(j3pGetRandomInt(11, 999) / 10, 1)
      const enonce = (me.donneesSection.consencours === 'figure') ? textes.ConsFigCyl : textes.ConsTextCyl

      stor.approche = j3pGetRandomInt(0, 3)
      stor.numunite = j3pGetRandomInt(0, 6)
      stor.pardef = j3pGetRandomInt(0, 1)
      stor.unite = textes.unite[stor.numunite]

      // création du conteneur dans lequel se trouveront toutes les consignes
      const heidiv = (me.donneesSection.limite >= 1) ? 40 : 0
      const conteneur = j3pDiv(me.zones.MG, {
        id: cssPrefix + 'conteneur',
        contenu: '',
        coord: [0, heidiv],
        style: me.styles.etendre('toutpetit.enonce', { padding: '10' })
      })
      const consigne1 = j3pAddElt(conteneur, 'div', '', { id: cssPrefix + 'zone_consigne1' })
      consigne1.innerHTML = '<TABLE style="margin-bottom:0" ><TR><TD id="' + cssPrefix + 'constexte"></TD><TD id="' + cssPrefix + 'fig"></TD></TR><TR><TD colspan = 2 id="' + cssPrefix + 'formules"></TD></TR></TABLE>'

      j3pAffiche(cssPrefix + 'constexte', cssPrefix + 'zone1', enonce,
        {
          a: j3pNombreBienEcrit(stor.rayon),
          b: j3pNombreBienEcrit(stor.angle),
          c: stor.unite,
          d: textes.approche[stor.approche] + textes.pardef[stor.pardef]

        })

      if (me.donneesSection.consencours === 'figure') {
        j3pCreeSVG(cssPrefix + 'fig', { id: 'mtg32svg', width: 350, height: 150 })
        // ce qui suit lance la création initiale de la figure
        depart(stor.angle, stor.rayon, stor.numunite)
      }

      let i = j3pGetRandomInt(0, 3)
      stor.ListeFormule1[0] = textes.Choisir
      stor.ListeFormule1[i % 4 + 1] = textes.Listeformule1_1
      stor.ListeFormule1[(i + 1) % 4 + 1] = textes.Listeformule1_2
      stor.ListeFormule1[(i + 2) % 4 + 1] = textes.Listeformule1_3
      stor.ListeFormule1[(i + 3) % 4 + 1] = textes.Listeformule1_4
      i = j3pGetRandomInt(0, 3)
      stor.ListeFormule2[0] = textes.Choisir
      stor.ListeFormule2[i % 4 + 1] = textes.Listeformule2_1
      stor.ListeFormule2[(i + 1) % 4 + 1] = textes.Listeformule2_2
      stor.ListeFormule2[(i + 2) % 4 + 1] = textes.Listeformule2_3
      stor.ListeFormule2[(i + 3) % 4 + 1] = textes.Listeformule2_4
      i = j3pGetRandomInt(0, 3)
      stor.ListeFormule3[0] = textes.Choisir
      stor.ListeFormule3[i % 4 + 1] = textes.Listeformule3_1
      stor.ListeFormule3[(i + 1) % 4 + 1] = textes.Listeformule3_2
      stor.ListeFormule3[(i + 2) % 4 + 1] = textes.Listeformule3_3
      stor.ListeFormule3[(i + 3) % 4 + 1] = textes.Listeformule3_4
      const rep = j3pAddElt(conteneur, 'div', '', { id: cssPrefix + 'zone_rep' })
      const rep1 = j3pAddElt(rep, 'div', '', { id: cssPrefix + 'zone_rep1' })
      const rep2 = j3pAddElt(rep, 'div', '', { id: cssPrefix + 'zone_rep2' })

      stor.maphraseformule = textes.ReponseFormule

      const bufprez = (me.donneesSection.Presentation) ? '' : '<TD width = "20" ></TD><TD nowrap valigne = "middle" id="' + cssPrefix + 'correction"></TD>'
      rep1.innerHTML = '<TABLE    style="margin-bottom: 0;padding:0" ><TR><TD valign = "Top" id ="' + cssPrefix + 'zone_TRAVtu" >' +
        '<TABLE   style="margin-bottom:0" ><TR><TD rowspan=3 id="secteur"></TD><TD id ="n1"></TD><TD id ="n2"></TD><TD id ="n3"></TD></TR><TR><TD  bgcolor="#000000" colspan=3></TD></TR><TR><TD align = "center" id ="div" colspan=3></TD></TR></TABLE>' +
        '</TD> ' + bufprez + '</TR></TABLE>'
      j3pAffiche('secteur', 'phraserep', textes.ReponseFormule,
        {
          a: me.donneesSection.solideencours
        })
      j3pAffiche('n2', 'phraserepfois', '$ \\times $')
      stor.zoneliste.push(ListeDeroulante.create('n1', stor.ListeFormule1, {
        mathquill: true,
        centre: true
      }))
      stor.zoneliste.push(ListeDeroulante.create('n3', stor.ListeFormule2, {
        mathquill: false,
        centre: true
      }))
      stor.zoneliste.push(ListeDeroulante.create('div', stor.ListeFormule3, {
        mathquill: true,
        centre: true
      }))

      j3pAddElt(rep2, 'div', '', {
        id: cssPrefix + 'explication',
        style: me.styles.toutpetit.correction
      })
    } else if (indexEtapeCourante === 2) {
      j3pDetruit(cssPrefix + 'zone_rep')
      j3pDiv(cssPrefix + 'formules', cssPrefix + 'zone_formule', '')
      const rep = j3pAddElt(cssPrefix + 'conteneur', 'div', '', { id: cssPrefix + 'zone_rep' })
      const rep1 = j3pAddElt(rep, 'div', '', { id: cssPrefix + 'zone_rep1' })
      const rep2 = j3pAddElt(rep, 'div', '', { id: cssPrefix + 'zone_rep2' })

      const formuleco = textes.ReponseFormule + ' $ \\frac {' + textes.Listeformule1_1.replace(/\$/g, '') + ' \\times ' + textes.Listeformule2_1 + '}{360} $'
      j3pAffiche(cssPrefix + 'zone_formule', 'formule', formuleco)

      const bufprez = me.donneesSection.Presentation ? '' : '<TD width = "20" ></TD><TD nowrap valigne = "middle" id="' + cssPrefix + 'correction"></TD>'
      rep1.innerHTML = '<TABLE    style="margin-bottom: 0;padding:0" ><TR><TD valign = "Top" id ="' + cssPrefix + 'zone_TRAVtu" >' +
        '<TABLE   style="margin-bottom:2px" ><TR><TD rowspan=3 id="secteur"></TD><TD id ="n1"></TD><TD id ="n2"></TD><TD id ="n3"></TD></TR><TR><TD  bgcolor="#000000" colspan=3></TD></TR><TR><TD align = "center" id ="div" colspan=3></TD></TR></TABLE>' +
        '</TD> ' + bufprez + '</TR></TABLE>'

      j3pAffiche('secteur', 'phraserep', textes.ReponseFormule)
      j3pAffiche('n1', 'phraserepfois', '$ \\pi \\times $')
      j3pAffiche('n3', 'phraserep3', '$ \\times $ &1&',
        {
          inputmq1: { dynamique: true, width: '30', maxlength: 5, couleur: stor.couleurinput1 }
        })
      j3pAffiche('div', 'phraserepdiv', '$ 360 $')
      j3pAffiche('n2', 'phraserep2', '&1&$^2$',
        {
          inputmq1: { dynamique: true, width: '30', maxlength: 5, couleur: stor.couleurinput1 }
        })
      j3pElement('phraserep2inputmq1').style.backgroundColor = 'white'
      j3pElement('phraserep3inputmq1').style.backgroundColor = 'white'
      mqRestriction('phraserep2inputmq1', '\\d,.', {
        boundingContainer: me.zonesElts.MG
      })
      mqRestriction('phraserep3inputmq1', '\\d,.', {
        boundingContainer: me.zonesElts.MG
      })
      j3pFocus('phraserep2inputmq1') // Ajouté ar Yves
      j3pElement('phraserep2inputmq1').onchange = chinput1
      j3pElement('phraserep3inputmq1').onchange = chinput2

      j3pAddElt(rep2, 'div', '', { id: cssPrefix + 'explication', style: me.styles.toutpetit.correction })
    } else if (indexEtapeCourante === 3) {
      j3pDetruit(cssPrefix + 'zone_rep')
      j3pDiv(cssPrefix + 'formules', cssPrefix + 'ligne2', '')
      j3pDiv(cssPrefix + 'conteneur', cssPrefix + 'zone_rep', '')
      j3pDiv(cssPrefix + 'zone_rep', cssPrefix + 'zone_rep1', '')
      j3pDiv(cssPrefix + 'zone_rep', cssPrefix + 'zone_rep2', '')

      if (me.donneesSection.Presentation) { j3pDiv('MepMD', 'emplacecalc', '') }

      const formuleco = textes.ReponseFormule + ' $ \\frac { \\pi \\times ' + stor.rayon + '^2 \\times ' + stor.angle + '}{360} $'
      j3pAffiche(cssPrefix + 'ligne2', 'ligne2', formuleco)

      const bufprez = me.donneesSection.Presentation ? '' : '<TD width = "20" ></TD><TD nowrap valigne = "middle" id="emplacecalc"></TD><TD width = "20" ></TD><TD nowrap valigne = "middle" id="' + cssPrefix + 'correction"></TD>'
      j3pElement(cssPrefix + 'zone_rep1').innerHTML = '<TABLE    style="margin-bottom: 0;padding:0" ><TR><TD valign = "Top" id ="' + cssPrefix + 'zone_TRAVtu" ></TD> ' + bufprez + '</TR></TABLE>'
      j3pElement(cssPrefix + 'zone_TRAVtu').innerHTML = '<TABLE   style="margin-bottom:2px" ><TR><TD rowspan=3 id="secteur"></TD><TD id ="n1"></TD><TD id ="n2"></TD><TD id ="n3"></TD></TR><TR><TD  bgcolor="#000000" colspan=3></TD></TR><TR><TD align = "center" id ="div" colspan=3></TD></TR></TABLE>'

      j3pAffiche('secteur', 'phraserep', textes.ReponseFormule)
      j3pAffiche('n1', 'phraserepfois', '$ \\pi \\times $')
      j3pAffiche('n3', 'phraserep3', '$ \\times ' + stor.angle + '$',
        {
          inputmq1: { dynamique: true, width: '30', maxchars: '5', couleur: stor.couleurinput1 }
        })
      j3pAffiche('div', 'phraserepdiv', '$ 360 $')
      j3pAffiche('n2', 'phraserep2', '&1&',
        {
          inputmq1: { dynamique: true, width: '30', maxlength: 9, couleur: stor.couleurinput1 }
        })
      j3pElement('phraserep2inputmq1').style.backgroundColor = 'white'
      mqRestriction('phraserep2inputmq1', '\\d,.', {
        boundingContainer: me.zonesElts.MG
      })
      j3pFocus('phraserep2inputmq1') // Ajouté par Yves
      j3pElement('phraserep2inputmq1').onchange = chinput1

      j3pDiv(cssPrefix + 'zone_rep2', {
        id: cssPrefix + 'explication',
        contenu: '',
        style: me.styles.toutpetit.correction
      })

      me.fenetresjq = [{ name: 'Calculatrice', title: 'Une calculatrice', left: 200, top: 10 }]
      j3pCreeFenetres(me)
      j3pAfficheCroixFenetres('Calculatrice')
      j3pAjouteBouton('emplacecalc', boutonfen, { id: 'BCalculatrice', className: 'MepBoutons', value: 'Calculatrice' })
    } else if (indexEtapeCourante === 0) {
      j3pDetruit(cssPrefix + 'zone_rep')
      j3pDiv(cssPrefix + 'formules', cssPrefix + 'ligne3', '')
      j3pDiv(cssPrefix + 'conteneur', cssPrefix + 'zone_rep', '')
      j3pDiv(cssPrefix + 'zone_rep', cssPrefix + 'zone_rep1', '')
      j3pDiv(cssPrefix + 'zone_rep', cssPrefix + 'zone_rep2', '')
      if (me.donneesSection.Presentation) { j3pDiv('MepMD', 'emplacecalc', '') }
      j3pDiv(cssPrefix + 'zone_rep2', {
        id: cssPrefix + 'explication',
        contenu: '',
        style: me.styles.toutpetit.correction
      })

      const formuleco = textes.ReponseFormule + ' $ \\frac { \\pi \\times ' + j3pArrondi(Math.pow(stor.rayon, 2), 4) + ' \\times ' + stor.angle + '}{360} $'
      j3pAffiche(cssPrefix + 'ligne3', 'ligne3', formuleco)

      const i = j3pGetRandomInt(0, 3)
      stor.Liste4Formule[0] = textes.Choisir
      stor.Liste4Formule[i % 4 + 1] = stor.unite
      stor.Liste4Formule[(i + 1) % 4 + 1] = stor.unite + '²'
      stor.Liste4Formule[(i + 2) % 4 + 1] = textes.unite[(stor.numunite + 1) % 6] + '²'
      stor.Liste4Formule[(i + 3) % 4 + 1] = textes.unite[(stor.numunite + 2) % 6] + '²'
      stor.couleurinput1 = null

      const bufprez = me.donneesSection.Presentation ? '' : '<TD width = "20" ></TD><TD nowrap valigne = "middle" id="emplacecalc"></TD><TD width = "20" ></TD><TD nowrap valigne = "middle" id="' + cssPrefix + 'correction"></TD>'
      j3pElement(cssPrefix + 'zone_rep1').innerHTML = '<TABLE    style="margin-bottom: 0;padding:0" ><TR><TD valign = "Top" id ="' + cssPrefix + 'zone_TRAVtu" ></TD> ' + bufprez + '</TR></TABLE>'

      j3pAffiche(cssPrefix + 'zone_TRAVtu', 'phraserep2', textes.ReponseFormulesuite,
        {
          inputmq1: { dynamique: true, width: '30', maxlength: 10, couleur: stor.couleurinput1 }
        })
      j3pElement('phraserep2inputmq1').style.backgroundColor = 'white'
      mqRestriction('phraserep2inputmq1', '\\d,.', {
        boundingContainer: me.zonesElts.MG
      })
      j3pElement('phraserep2inputmq1').onchange = chinput1
      j3pFocus('phraserep2inputmq1')

      stor.zoneliste.push(ListeDeroulante.create(cssPrefix + 'zone_TRAVtu', stor.Liste4Formule, {
        mathquill: false,
        centre: true,
        alignmathquill: true
      }))

      j3pAffiche(cssPrefix + 'zone_rep1', 'phraserep', textes.ConsApproche,
        {
          d: textes.approche[stor.approche],
          e: textes.pardef[stor.pardef]
        })

      me.fenetresjq = [{ name: 'Calculatrice', title: 'Une calculatrice', left: 200, top: 10 }]
      j3pCreeFenetres(me)
      j3pAjouteBouton('emplacecalc', boutonfen, { id: 'BCalculatrice', className: 'MepBoutons', value: 'Calculatrice' })
    }

    me.zonesElts.MG.classList.add('fond')
    const rep1 = j3pElement(cssPrefix + 'zone_rep1')
    rep1.style.paddingTop = '5px'
    rep1.style.paddingBottom = '5px'
    rep1.style.paddingLeft = '5px'
    rep1.style.paddingRight = '5px'
    j3pElement(cssPrefix + 'zone_rep1').style.backgroundColor = textes.couleurenonce
    const consigne1 = j3pElement(cssPrefix + 'zone_consigne1')
    consigne1.style.paddingTop = '5px'
    consigne1.style.paddingBottom = '5px'
    consigne1.style.paddingLeft = '5px'
    consigne1.style.paddingRight = '5px'
    consigne1.style.backgroundColor = textes.couleurenonceg

    if (me.donneesSection.Presentation) {
      j3pDiv(me.zones.MD, {
        id: cssPrefix + 'correction',
        contenu: '',
        coord: [0, 100],
        style: me.styles.etendre('toutpetit.correction', { padding: '10' })
      })
    }
    me.finEnonce()
  }

  function depart (a, b, c) {
    stor.mtgAppLecteur.removeAllDoc()
    // txt html de la figure
    const txtFigure = 'TWF0aEdyYXBoSmF2YTEuMAAAABE+D1wpAANmcmH2+v4BAP8BAAAAAAAAAAABNgAAAJUAAAAAAAAAAAAAAAEAAABQ#####wAAAAEACkNDYWxjQ29uc3QA#####wACcGkAFjMuMTQxNTkyNjUzNTg5NzkzMjM4NDb#####AAAAAQAKQ0NvbnN0YW50ZUAJIftURC0Y#####wAAAAEACkNQb2ludEJhc2UA#####wEAAAAAEAABTwDAJgAAAAAAAMBAgAAAAAAABQABQFTAAAAAAABAUgAAAAAAAP####8AAAABABRDRHJvaXRlRGlyZWN0aW9uRml4ZQD#####AQAAAAANAAABAAEAAAABAT#ihL2hL2hN#####wAAAAEAD0NQb2ludExpZURyb2l0ZQD#####AQAAAAEQAAFJAMAQAAAAAAAAQBAAAAAAAAAFAAFAQuXLly5cuQAAAAL#####AAAAAQAJQ0Ryb2l0ZUFCAP####8BAAAAABAAAAEAAQAAAAEAAAAD#####wAAAAEAFkNEcm9pdGVQZXJwZW5kaWN1bGFpcmUA#####wEAAAAADQAAAQABAAAAAQAAAAT#####AAAAAQAJQ0NlcmNsZU9BAP####8BAAAAAAEAAAABAAAAA#####8AAAABABBDSW50RHJvaXRlQ2VyY2xlAP####8AAAAFAAAABv####8AAAABABBDUG9pbnRMaWVCaXBvaW50AP####8BAAAAAA0AAADAKAAAAAAAAMAQAAAAAAAABQABAAAABwAAAAkA#####wEAAAABEAABSgDAKAAAAAAAAMAQAAAAAAAABQACAAAAB#####8AAAACAAdDUmVwZXJlAP####8A#2YAAQEAAAABAAAAAwAAAAkAAAAAAAABAAAAAAAAAAAAAAABAAAAAAAAAAAAAAABP#AAAAAAAAAAAAABP#AAAAAAAAD#####AAAAAQAJQ0xvbmd1ZXVyAP####8AAAABAAAAA#####8AAAABAAdDQ2FsY3VsAP####8AB25iZ3JhZHgAAjIwAAAAAUA0AAAAAAAAAAAADAD#####AAduYmdyYWR5AAIyMAAAAAFANAAAAAAAAP####8AAAABABRDSW1wbGVtZW50YXRpb25Qcm90bwD#####ABdHcmFkdWF0aW9uQXhlc1JlcGVyZU5ldwAAABsAAAAIAAAAAwAAAAoAAAAMAAAADf####8AAAABABNDQWJzY2lzc2VPcmlnaW5lUmVwAAAAAA4ABWFic29yAAAACv####8AAAABABNDT3Jkb25uZWVPcmlnaW5lUmVwAAAAAA4ABW9yZG9yAAAACv####8AAAABAApDVW5pdGV4UmVwAAAAAA4ABnVuaXRleAAAAAr#####AAAAAQAKQ1VuaXRleVJlcAAAAAAOAAZ1bml0ZXkAAAAK#####wAAAAEAEENQb2ludERhbnNSZXBlcmUAAAAADgAAAAAADgAAAQUAAAAACv####8AAAABAA9DUmVzdWx0YXRWYWxldXIAAAAPAAAAEwAAABAAAAASAAAAAA4AAAAAAA4AAAEFAAAAAAr#####AAAAAQAKQ09wZXJhdGlvbgAAAAATAAAADwAAABMAAAARAAAAEwAAABAAAAASAAAAAA4AAAAAAA4AAAEFAAAAAAoAAAATAAAADwAAABQAAAAAEwAAABAAAAATAAAAEv####8AAAABAAtDSG9tb3RoZXRpZQAAAAAOAAAAEwAAABMAAAAM#####wAAAAEAC0NQb2ludEltYWdlAAAAAA4AAAAAAA4AAAEFAAAAABQAAAAWAAAAFQAAAAAOAAAAEwAAABMAAAANAAAAFgAAAAAOAAAAAAAOAAABBQAAAAAVAAAAGP####8AAAABAAhDU2VnbWVudAAAAAAOAQAAAAAAAAABAAEAAAAUAAAAFwAAABcAAAAADgEAAAAAAAAAAQABAAAAFQAAABkAAAAEAAAAAA4BAAAAAAsAAVcAwBQAAAAAAADANAAAAAAAAAUAAT#cVniavN8OAAAAGv####8AAAACAAhDTWVzdXJlWAAAAAAOAAd4Q29vcmQxAAAACgAAABwAAAAMAAAAAA4ABWFic3cxAAd4Q29vcmQxAAAAEwAAAB3#####AAAAAgASQ0xpZXVPYmpldFBhclB0TGllAQAAAA4BZmZmAAAAHAAAABMAAAAMAAAAHAAAAAIAAAAcAAAAHAAAAAwAAAAADgAFYWJzdzIADTIqYWJzb3ItYWJzdzEAAAAUAQAAABQCAAAAAUAAAAAAAAAAAAAAEwAAAA8AAAATAAAAHgAAABIAAAAADgEAAAAACwAAAQUAAAAACgAAABMAAAAgAAAAEwAAABAAAAAZAQAAAA4BZmZmAAAAIQAAABMAAAAMAAAAHAAAAAUAAAAcAAAAHQAAAB4AAAAgAAAAIQAAAAQAAAAADgEAAAAACwABUgBAIAAAAAAAAMAgAAAAAAAABQABP9EbToG06B8AAAAb#####wAAAAIACENNZXN1cmVZAAAAAA4AB3lDb29yZDEAAAAKAAAAIwAAAAwAAAAADgAFb3JkcjEAB3lDb29yZDEAAAATAAAAJAAAABkBAAAADgFmZmYAAAAjAAAAEwAAAA0AAAAjAAAAAgAAACMAAAAjAAAADAAAAAAOAAVvcmRyMgANMipvcmRvci1vcmRyMQAAABQBAAAAFAIAAAABQAAAAAAAAAAAAAATAAAAEAAAABMAAAAlAAAAEgAAAAAOAQAAAAALAAABBQAAAAAKAAAAEwAAAA8AAAATAAAAJwAAABkBAAAADgFmZmYAAAAoAAAAEwAAAA0AAAAjAAAABQAAACMAAAAkAAAAJQAAACcAAAAo#####wAAAAIADENDb21tZW50YWlyZQAAAAAOAWZmZgAAAAAAAAAAAEAYAAAAAAAAAAAAHAsAAf###wAAAAEAAAAAAAsjVmFsKGFic3cxKQAAABkBAAAADgFmZmYAAAAqAAAAEwAAAAwAAAAcAAAABAAAABwAAAAdAAAAHgAAACoAAAAbAAAAAA4BZmZmAAAAAAAAAAAAQBgAAAAAAAAAAAAhCwAB####AAAAAQAAAAAACyNWYWwoYWJzdzIpAAAAGQEAAAAOAWZmZgAAACwAAAATAAAADAAAABwAAAAGAAAAHAAAAB0AAAAeAAAAIAAAACEAAAAsAAAAGwAAAAAOAWZmZgDAIAAAAAAAAD#wAAAAAAAAAAAAIwsAAf###wAAAAIAAAABAAsjVmFsKG9yZHIxKQAAABkBAAAADgFmZmYAAAAuAAAAEwAAAA0AAAAjAAAABAAAACMAAAAkAAAAJQAAAC4AAAAbAAAAAA4BZmZmAMAcAAAAAAAAAAAAAAAAAAAAAAAoCwAB####AAAAAgAAAAEACyNWYWwob3JkcjIpAAAAGQEAAAAOAWZmZgAAADAAAAATAAAADQAAACMAAAAGAAAAIwAAACQAAAAlAAAAJwAAACgAAAAwAAAAEgD#####AQAAAAANAAAAAAAAAAAAAABACAAAAAAAAAUAAAAACgAAAAFACAAAAAAAAAAAAAEAAAAAAAAAAAAAAAcA#####wEAAAAAAQAAAAEAAAAy#####wAAAAEAD0NQb2ludExpZUNlcmNsZQD#####AAAAAAANAAABBQABP9TfvKC#GcIAAAAzAAAAFwD#####AAAAAAAAAAABAAIAAAA0AAAAAQAAAAwA#####wACYTIAAzMwMAAAAAFAcsAAAAAAAP####8AAAABAAlDUm90YXRpb24A#####wAAAAEAAAATAAAANgAAABYA#####wEAAAAADQAAAQUAAAAANAAAADcAAAAXAP####8AAAAAAAAAAAEAAgAAADgAAAAB#####wAAAAEAEkNBcmNEZUNlcmNsZURpcmVjdAD#####AAAAAAACAAAAAQAAADQAAAA4#####wAAAAEAGUNTdXJmYWNlU2VjdGV1ckNpcmN1bGFpcmUA#####wCZmZkAAAAEAAAAOgAAAAQA#####wH#AAAADQAAAAAAAAAAAAAAQAgAAAAAAAAFAAE#6pcO7V+6OgAAADUAAAAeAP####8AAH8AAAMAAAABAAAAPP####8AAAATAAAANgAAAAwA#####wAFcmF5b24AAjQ1AAAAAUBGgAAAAAAAAAAADAD#####AAFyAAE2AAAAAUAYAAAAAAAAAAAAGwD#####AP8AAAH#####E0Bl4AAAAAAAQEaAAAAAAAAAAAAAAAAAAAAAABZyYXlvbiByID0gI1ZhbChyYXlvbikgAAAAGwD#####AAB#AAH#####E0BloAAAAAAAQCgAAAAAAAAAAAAAAAAAAAAAABJhbmdsZSA9ICNWYWwoYTIpwrAAAAASAP####8B#wAAABAAAAAAAAAAAAAAAEAIAAAAAAAABQAAAAAKAAAAAUAjAAAAAAAAAAAAFAIAAAAUAQAAABMAAAA#AAAAAT#wAAAAAAAAAAAAAUAkAAAAAAAAAAAAGwD#####AP8AAABAHAAAAAAAAMA6AAAAAAAAAAAAQhMAAAAAAAEAAAAAAAJjbQAAABIA#####wH#AAAAEAAAAAAAAAAAAAAAQAgAAAAAAAAFAAAAAAoAAAABQCMAAAAAAAAAAAAUAAAAABQCAAAAEwAAAD8AAAABQCQAAAAAAAAAAAABP+AAAAAAAAAAAAAbAP####8A#wAAAEAwAAAAAAAAwC4AAAAAAAAAAABEEwAAAAAAAQAAAAAAAm1tAAAAEgD#####Af8AAAAQAAAAAAAAAAAAAABACAAAAAAAAAUAAAAACgAAAAFAIwAAAAAAAAAAABQAAAAAAT#wAAAAAAAAAAAAFAIAAAAUAQAAABMAAAA#AAAAAUAAAAAAAAAAAAAAAUAkAAAAAAAAAAAAGwD#####AP8AAABAIgAAAAAAAMAQAAAAAAAAAAAARhMAAAAAAAEAAAAAAAJkbQAAABIA#####wH#AAAAEAAAAAAAAAAAAAAAQAgAAAAAAAAFAAAAAAoAAAABQCMAAAAAAAAAAAAUAAAAABQCAAAAFAEAAAATAAAAPwAAAAFACAAAAAAAAAAAAAFAJAAAAAAAAAAAAAE#8AAAAAAAAAAAABsA#####wD#AAAAQCAAAAAAAADAEAAAAAAAAAAAAEgTAAAAAAABAAAAAAABbQAAABIA#####wH#AAAAEAAAAAAAAAAAAAAAQAgAAAAAAAAFAAAAAAoAAAABQCMAAAAAAAAAAAAUAAAAABQCAAAAFAEAAAATAAAAPwAAAAFAEAAAAAAAAAAAAAFAJAAAAAAAAAAAAAE#8AAAAAAAAAAAABsA#####wD#AAAAQCgAAAAAAADAEAAAAAAAAAAAAEoTAAAAAAABAAAAAAADZGFtAAAAEgD#####Af8AAAAQAAAAAAAAAAAAAABACAAAAAAAAAUAAAAACgAAAAFAIwAAAAAAAAAAABQAAAAAFAIAAAAUAQAAABMAAAA#AAAAAUAUAAAAAAAAAAAAAUAkAAAAAAAAAAAAAT#wAAAAAAAAAAAAGwD#####AP8AAABAJAAAAAAAAMAUAAAAAAAAAAAATBMAAAAAAAEAAAAAAAJobQAAABIA#####wH#AAAAEAAAAAAAAAAAAAAAQAgAAAAAAAAFAAAAAAoAAAABQCMAAAAAAAAAAAAUAAAAABQCAAAAFAEAAAATAAAAPwAAAAFAGAAAAAAAAAAAAAFAJAAAAAAAAAAAAAE#8AAAAAAAAAAAABsA#####wD#AAAAQCQAAAAAAADAFAAAAAAAAAAAAE4TAAAAAAABAAAAAAACa20AAAAL##########8='

    stor.mtgAppLecteur.addDoc('mtg32svg', txtFigure, true)
    stor.mtgAppLecteur.calculateAndDisplayAll(true)
    stor.mtgAppLecteur.giveFormula2('mtg32svg', 'a2', String(a))
    stor.mtgAppLecteur.giveFormula2('mtg32svg', 'rayon', String(b))
    stor.mtgAppLecteur.giveFormula2('mtg32svg', 'r', String(c))
    stor.mtgAppLecteur.calculate('mtg32svg', true)// on recalcule tout dans MathGraph32
    stor.mtgAppLecteur.display('mtg32svg')
  }

  switch (this.etat) {
    case 'enonce':
      if (this.debutDeLaSection) {
        initSection()
      } else {
        // A chaque répétition, on nettoie la scène, mais pas tout
        if (indexEtapeCourante === 1) {
          j3pEmpty(this.zonesElts.MG)
        } else if (this.donneesSection.limite > 0) {
          j3pDetruit('MepMGfigure')
        }
        if ((this.donneesSection.structure === 'presentation1') || (this.donneesSection.structure === 'presentation1bis')) {
          j3pEmpty(this.zonesElts.MD)
        }
        enonceMain()
      }
      break // case "enonce":

    case 'correction': {
      const eltCorrection = j3pElement(cssPrefix + 'correction')
      if (!yareponse()) {
        eltCorrection.style.color = this.styles.cfaux
        eltCorrection.innerHTML = reponseIncomplete
        return this.finCorrection()
      }

      // Une réponse a été saisie
      // Bonne réponse
      if (bonneReponse()) {
        if (indexEtapeCourante === 1) {
          for (const zone of stor.zoneliste) zone.disable()
        } else if (indexEtapeCourante === 2) {
          j3pDesactive('phraserep2inputmq1')
          j3pDesactive('phraserep3inputmq1')
        } else if (indexEtapeCourante === 3) {
          j3pDetruitFenetres('Calculatrice')
          j3pDetruit('BCalculatrice')
          j3pDesactive('phraserep2inputmq1')
        } else { // indexEtapeCourante === 0
          j3pDetruitFenetres('Calculatrice')
          j3pDetruit('BCalculatrice')
          j3pDesactive('phraserep2inputmq1')
          for (const zone of stor.zoneliste) zone.disable()
        }

        eltCorrection.style.color = this.styles.cbien
        eltCorrection.innerHTML = cBien
        this.score++
        this.typederreurs[0]++
        return this.finCorrection('navigation', true)
      }

      // Pas de bonne réponse
      eltCorrection.style.color = this.styles.cfaux

      // A cause de la limite de temps :
      if (this.isElapsed) { // limite de temps
        eltCorrection.innerHTML = tempsDepasse
        this.typederreurs[10]++
        if (me.donneesSection.theme === 'zonesAvecImageDeFond') {
          j3pElement(cssPrefix + 'explication').style.paddingTop = '5px'
          j3pElement(cssPrefix + 'explication').style.paddingBottom = '5px'
          j3pElement(cssPrefix + 'explication').style.paddingLeft = '5px'
          j3pElement(cssPrefix + 'explication').style.paddingRight = '5px'
          j3pElement(cssPrefix + 'explication').style.backgroundColor = textes.couleurcorrec
        }
        if (indexEtapeCourante === 1) {
          for (const zone of stor.zoneliste) zone.disable()

          if (!stor.boform1) {
            stor.zoneliste[0].barre()
          }
          if (!stor.boform2) {
            stor.zoneliste[1].barre()
          }
          if (!stor.boform3) {
            stor.zoneliste[2].barre()
          }
          const formuleco = textes.ReponseFormule + ' $ \\frac {' + textes.Listeformule1_1.replace(/\$/g, '') + ' \\times ' + textes.Listeformule2_1 + '}{360} $'
          j3pAffiche(cssPrefix + 'explication', 'coco', formuleco)
        }

        if (indexEtapeCourante === 2) {
          j3pDesactive('phraserep2inputmq1')
          j3pDesactive('phraserep3inputmq1')
          if (!stor.boform2) {
            j3pBarre('phraserep3inputmq1')
          }
          if (!stor.boform1) {
            j3pBarre('phraserep2inputmq1')
          }

          const formuleco = textes.ReponseFormule + ' $ \\frac { \\pi \\times ' + stor.rayon + '^2 \\times ' + stor.angle + '}{360} $'
          j3pAffiche(cssPrefix + 'explication', 'coco', formuleco)
        }

        if (indexEtapeCourante === 3) {
          j3pDetruitFenetres('Calculatrice')
          j3pDetruit('BCalculatrice')
          j3pDesactive('phraserep2inputmq1')
          j3pBarre('phraserep2inputmq1')
          if (stor.boform2) {
            j3pModale({ titre: textes.Reecalcul, contenu: '' })
            j3pAffiche('modale', 'affichemodale',
              '<BR>' + textes.Errcarre + '<BR>',
              {
                a: stor.rayon
              })
          }
          const formuleco = textes.ReponseFormule + ' $ \\frac { \\pi \\times ' + j3pArrondi(Math.pow(stor.rayon, 2), 4) + ' \\times ' + stor.angle + '}{360} $'
          j3pAffiche(cssPrefix + 'explication', 'coco', formuleco)
        }

        if (indexEtapeCourante === 0) {
          j3pDetruitFenetres('Calculatrice')
          j3pDetruit('BCalculatrice')
          j3pDesactive('phraserep2inputmq1')
          for (const zone of stor.zoneliste) zone.disable()

          if (!stor.boform2) {
            j3pBarre('phraserep2inputmq1')
          }
          if (!stor.boform1) {
            stor.zoneliste[0].barre()
          }

          const formuleco = textes.ReponseFormulesuite2 + ' $ ' + stor.bonnerep + ' $ ' + ' ' + stor.unite + '²'
          j3pAffiche(cssPrefix + 'explication', 'coco', formuleco)
        }

        return this.finCorrection('navigation', true)
      }

      // Réponse fausse :
      eltCorrection.innerHTML = cFaux

      if (this.essaiCourant < this.donneesSection.nbchances) {
        eltCorrection.innerHTML += '<br>' + essaieEncore
        this.typederreurs[1]++
        // il reste des essais, on reste en état correction
        return this.finCorrection()
      }

      // Erreur au dernier essai
      eltCorrection.innerHTML += '<br>' + regardeCorrection

      j3pElement(cssPrefix + 'explication').style.paddingTop = '5px'
      j3pElement(cssPrefix + 'explication').style.paddingBottom = '5px'
      j3pElement(cssPrefix + 'explication').style.paddingLeft = '5px'
      j3pElement(cssPrefix + 'explication').style.paddingRight = '5px'
      j3pElement(cssPrefix + 'explication').style.backgroundColor = textes.couleurcorrec

      if (indexEtapeCourante === 1) {
        for (const zone of stor.zoneliste) zone.disable()

        if (!stor.boform1) {
          stor.zoneliste[0].barre()
        }
        if (!stor.boform2) {
          stor.zoneliste[1].barre()
        }
        if (!stor.boform3) {
          stor.zoneliste[2].barre()
        }
        const formuleco = textes.ReponseFormule + ' $ \\frac {' + textes.Listeformule1_1.replace(/\$/g, '') + ' \\times ' + textes.Listeformule2_1 + '}{360} $'
        j3pAffiche(cssPrefix + 'explication', 'coco', formuleco)
      }

      if (indexEtapeCourante === 2) {
        j3pDesactive('phraserep2inputmq1')
        j3pDesactive('phraserep3inputmq1')
        if (!stor.boform2) {
          j3pBarre('phraserep3inputmq1')
        }
        if (!stor.boform1) {
          j3pBarre('phraserep2inputmq1')
        }

        const formuleco = textes.ReponseFormule + ' $ \\frac { \\pi \\times ' + stor.rayon + '^2 \\times ' + stor.angle + '}{360} $'
        j3pAffiche(cssPrefix + 'explication', 'coco', formuleco)
      }

      if (indexEtapeCourante === 3) {
        j3pDetruitFenetres('Calculatrice')
        j3pDetruit('BCalculatrice')
        j3pDesactive('phraserep2inputmq1')
        j3pBarre('phraserep2inputmq1')
        if (stor.boform2) {
          j3pModale({ titre: textes.Reecalcul, contenu: '' })
          j3pAffiche('modale', 'affichemodale',
            '<BR>' + textes.Errcarre + '<BR>',
            {
              a: stor.rayon
            })
        }
        const formuleco = textes.ReponseFormule + ' $ \\frac { \\pi \\times ' + j3pArrondi(Math.pow(stor.rayon, 2), 4) + ' \\times ' + stor.angle + '}{360} $'
        j3pAffiche(cssPrefix + 'explication', 'coco', formuleco)
      }

      if (indexEtapeCourante === 0) {
        j3pDetruitFenetres('Calculatrice')
        j3pDetruit('BCalculatrice')
        j3pDesactive('phraserep2inputmq1')
        stor.zoneliste[0].disable()

        if (!stor.boform1) {
          stor.zoneliste[0].barre()
        }
        if (!stor.boform2) {
          j3pBarre('phraserep2inputmq1')
        }

        const formuleco = textes.ReponseFormulesuite2 + ' $ ' + stor.bonnerep + ' $ ' + ' ' + stor.unite + '²'
        j3pAffiche(cssPrefix + 'explication', 'coco', formuleco)
      }
      this.typederreurs[2]++
      this.finCorrection('navigation', true)
      break // case "correction":
    }

    case 'navigation':
      if (!this.sectionTerminee()) {
        this.etat = 'enonce'
      }
      this.finNavigation(true)
      break // case "navigation":
  }
}
