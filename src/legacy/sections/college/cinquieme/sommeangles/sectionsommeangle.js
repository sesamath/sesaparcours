import $ from 'jquery'
import { j3pAddElt, j3pDetruit, j3pEmpty, j3pGetNewId, j3pGetRandomBool, j3pGetRandomInt, j3pShowError, j3pShuffle } from 'src/legacy/core/functions'
import ListeDeroulante from 'src/legacy/outils/listeDeroulante/ListeDeroulante'
import { j3pAffiche } from 'src/lib/mathquill/functions'
import { addDefaultTable } from 'src/legacy/themes/table'
import ZoneStyleMathquill1 from 'src/legacy/outils/zoneStyleMathquill/ZoneStyleMathquill1'
import Repere from 'src/legacy/outils/geometrie/Repere'
import { addMaj } from 'src/lib/utils/string'
import textesGeneriques from 'src/lib/core/textes'

const { cBien, cFaux, essaieEncore, reponseIncomplete, tempsDepasse } = textesGeneriques

/**
 * Ajouter une description ici
 * @fileOverview
 * @author Tommy Barroy <t.barroy@laposte.net>
 * @since 2017-10
 * @since 2017-10
 */

export const params = {
  outils: ['mathquill'],
  /**
   * Ce qui est paramétrable dans l’éditeur de graphe
   * @type {LegacySectionParamsParametres}
   */
  parametres: [
    ['nbrepetitions', 9, 'entier', 'Nombre de répétitions de la section'],
    ['nbchances', 2, 'entier', 'Nombre d’essais possibles.'],
    ['Type_Enonce', 'Les deux', 'liste', '<u>Figure</u>:  Les données se trouvent sur un schéma.  <br> <br> <u>Texte</u>: Les données sont dans un texte. ', ['Les deux', 'Figure', 'Texte']],
    ['Raisonnement', true, 'boolean', '<u>true</u>:  Le raisonnement est attendu. '],
    ['Description', true, 'boolean', '<u>true</u>: L’élève doit compléter la description.'],
    ['Quelconque', true, 'boolean', '<u>true</u>: Le triangle peut être quelconque.'],
    ['Isocele', true, 'boolean', '<u>true</u>: Le triangle peut être isocèle.'],
    ['Equilateral', true, 'boolean', '<u>true</u>: Le triangle peut être équilatéral.'],
    ['Rectangle', true, 'boolean', '<u>true</u>: Le triangle peut être rectangle.'],
    ['Donnees_Inutiles', 'oui', 'liste', '<u>oui</u>: L’énoncé comporte des données inutiles. <br><br> <u>non</u>: non', ['oui', 'non', 'Les deux']],
    // le modèle gère la compatibilité ascendante avec l’ancien paramètre MiseEnPagePlusClaire
    ['theme', 'standard', 'liste', 'Modèle pour l’affichage', ['standard', 'zonesAvecImageDeFond']]

  ]
}

/**
 * section sommeangle
 * @this {Parcours}
 */
export default function main () {
  const stor = this.storage
  const me = this
  const ds = me.donneesSection

  function modif (ki) {
    for (let i = 0; i < ki.length; ki++) {
      for (let j = 0; j < ki[i].length; j++) {
        ki[i][j].style.padding = '0px'
      }
    }
  }

  function creeLesDiv () {
    stor.lesdiv = {}
    stor.lesdiv.conteneur = j3pAddElt(me.zonesElts.MG, 'div', {
      style: me.styles.etendre('toutpetit.enonce', { padding: '10px' })
    })
    stor.lesdiv.consigne = addDefaultTable(stor.lesdiv.conteneur, 1, 1)[0][0]
    stor.lesdiv.consigne.style.padding = '0px'
    const tatab = addDefaultTable(stor.lesdiv.conteneur, 3, 3)
    modif(tatab)
    tatab[0][0].setAttribute('colspan', 3)
    tatab[0][1].setAttribute('rowspan', 2)
    tatab[0][2].setAttribute('rowspan', 2)
    tatab[1][0].setAttribute('rowspan', 2)
    tatab[1][1].setAttribute('rowspan', 2)
    tatab[1][1].innerHTML = '&nbsp;&nbsp;&nbsp;'
    j3pDetruit(tatab[2][1])
    j3pDetruit(tatab[2][2])
    tatab[2][0].setAttribute('colspan', 4)
    stor.lesdiv.zone1 = tatab[0][0]
    stor.lesdiv.zone2 = tatab[0][1]
    stor.lesdiv.zone3 = tatab[1][0]
    stor.lesdiv.zone4 = tatab[2][0]
    stor.lesdiv.zone5 = tatab[0][2]
    me.zonesElts.MG.classList.add('fond')
  }

  function poseQuestion () {
    const ff = stor.lafig
    stor.zoneR = undefined
    stor.zoneR2 = undefined
    stor.zoneR3 = undefined
    stor.zoneI = undefined
    stor.zoneI2 = undefined
    stor.zonesomme = undefined
    if (ds.Raisonnement) {
      j3pAffiche(stor.lesdiv.consigne, null, '<b>Complète la démonstration ci-dessous </b> pour calculer l’angle $\\widehat{' + ff.nomangle + '}$')
    } else {
      j3pAffiche(stor.lesdiv.consigne, null, '<b>Donne la mesure </b> de l’angle $\\widehat{' + ff.nomangle + '}$')
    }
    affichePhrase()
  }

  function gerelistenature (choix) {
    if (stor.zonesomme !== undefined) stor.zonesomme.disable()
    stor.zonesomme = undefined
    if ((choix === 'isocèle') || (choix === 'rectangle')) {
      stor.tabP1[0][4].innerHTML = '&nbsp;en&nbsp;'
      stor.zonesomme = new ZoneStyleMathquill1(stor.tabP1[0][5], {
        restric: 'ABCDEFGHIJKLMNOPQRSTUVWXYZabcdefghijklmnopqrstuvwxyz',
        limitenb: 9,
        limite: 0
      })
    } else {
      stor.tabP1[0][4].innerHTML = ''
      stor.tabP1[0][5].innerHTML = ''
    }
  }

  function gerelistepropriete (choix) {
    if (stor.zoneR !== undefined) stor.zoneR.disable()
    if (stor.zoneR3 !== undefined) stor.zoneR3.disable()
    if (stor.zoneR2 !== undefined) stor.zoneR2.disable()
    if (stor.zoneI !== undefined) stor.zoneI.disable()
    if (stor.zoneI2 !== undefined) stor.zoneI2.disable()
    stor.zoneR = undefined
    stor.zoneR2 = undefined
    stor.zoneR3 = undefined
    stor.zoneI = undefined
    stor.zoneI2 = undefined
    j3pEmpty(stor.tabGro[2][0])
    const ff = stor.lafig
    let tabX
    switch (choix) {
      case 'la somme des angles aigus égale 90°.': {
        tabX = addDefaultTable(stor.tabGro[2][0], 3, 3)
        modif(tabX)
        const kl = addDefaultTable(tabX[0][2], 1, 2)
        modif(kl)
        j3pAffiche(kl[0][0], null, '$90 - $&nbsp;')
        stor.zoneR = new ZoneStyleMathquill1(kl[0][1], {
          restric: 'ABCDEFGHIJKLMNOPQRSTUVWXYZabcdefghijklmnopqrstuvwxyz[]()^$',
          limitenb: 9,
          limite: 4,
          hasAutoKeyboard: true
        })
        const hj = addDefaultTable(tabX[1][2], 1, 4)
        modif(hj)
        j3pAffiche(hj[0][0], null, '$90 - $&nbsp;')
        stor.zoneR2 = new ZoneStyleMathquill1(hj[0][1], {
          restric: '0123456789°',
          limitenb: 9,
          limite: 4
        })
        stor.zoneR3 = new ZoneStyleMathquill1(tabX[2][2], {
          restric: '0123456789°',
          limitenb: 9,
          limite: 4,
          hasAutoKeyboard: true
        })
        j3pAffiche(tabX[1][1], null, '$\\widehat{' + ff.nomangle + '}$ = &nbsp;')
        j3pAffiche(tabX[2][1], null, '$\\widehat{' + ff.nomangle + '}$ = &nbsp;')
      }
        break
      case 'les angles à la base sont égaux.':
        tabX = addDefaultTable(stor.tabGro[2][0], 2, 4)
        modif(tabX)
        stor.zoneI = new ZoneStyleMathquill1(tabX[0][3], {
          restric: 'ABCDEFGHIJKLMNOPQRSTUVWXYZabcdefghijklmnopqrstuvwxyz[]()^$',
          limitenb: 9,
          limite: 4,
          hasAutoKeyboard: true
        })
        stor.zoneI2 = new ZoneStyleMathquill1(tabX[1][3], {
          restric: '0123456789°',
          limitenb: 9,
          limite: 4,
          hasAutoKeyboard: true
        })
        j3pAffiche(tabX[1][1], null, '$\\widehat{' + ff.nomangle + '}$&nbsp; = &nbsp;')
        break
      case 'les angles mesurent 60°.':
        tabX = addDefaultTable(stor.tabGro[2][0], 1, 3)
        modif(tabX)
        j3pAffiche(tabX[0][2], null, '$ 60° $')
        tabX[0][2].style.verticalAlign = 'bottom'
        break
    }
    tabX[0][0].innerHTML = 'Donc&nbsp;'
    tabX[0][0].style.verticalAlign = 'bottom'
    j3pAffiche(tabX[0][1], null, '$\\widehat{' + ff.nomangle + '}$ = &nbsp;')
  }

  function affichePhrase () {
    const ff = stor.lafig
    if (ds.Raisonnement) {
      switch (stor.Lexo.prop) {
        case 'Quelconque': {
          stor.tabGro = addDefaultTable(stor.lesdiv.travail, 3, 2)
          modif(stor.tabGro)
          if (ds.Description) {
            const tabP = addDefaultTable(stor.tabGro[0][0], 1, 3)
            modif(tabP)
            j3pAffiche(tabP[0][0], null, 'Dans le triangle &nbsp;')
            stor.zone1 = new ZoneStyleMathquill1(tabP[0][1], {
              restric: 'ABCDEFGHIJKLMNOPQRSTUVWXYZabcdefghijklmnopqrstuvwxyz[]()^$',
              limitenb: 9,
              limite: 4,
              hasAutoKeyboard: true

            })
            tabP[0][2].innerHTML = '&nbsp;,'
            const tabP2 = addDefaultTable(stor.tabGro[0][0], 1, 3)
            modif(tabP2)
            j3pAffiche(tabP2[0][0], null, 'la somme des angles est égale à &nbsp;')
            stor.zone2 = new ZoneStyleMathquill1(tabP2[0][1], {
              restric: '0123456789°',
              limitenb: 9,
              limite: 4,
              hasAutoKeyboard: true

            })
            tabP2[0][2].innerHTML = '&nbsp;.'
          } else {
            j3pAffiche(stor.tabGro[0][0], null, 'Dans le triangle ' + ff.nomautreangle1 + ',\n')
            j3pAffiche(stor.tabGro[0][0], null, 'la somme des angles est égale à $180°$.')
          }
          const tabP3 = addDefaultTable(stor.tabGro[1][0], 4, 3)
          modif(tabP3)
          j3pAffiche(tabP3[0][0], null, 'Donc &nbsp;')
          tabP3[0][0].style.verticalAlign = 'bottom'
          j3pAffiche(tabP3[0][1], null, ' $\\widehat{' + ff.nomangle + '}=$ ')
          j3pAffiche(tabP3[1][1], null, ' $\\widehat{' + ff.nomangle + '}=$ ')
          j3pAffiche(tabP3[2][1], null, ' $\\widehat{' + ff.nomangle + '}=$ ')
          j3pAffiche(tabP3[3][1], null, ' $\\widehat{' + ff.nomangle + '}=$ ')
          // L1
          const tabP4 = addDefaultTable(tabP3[0][2], 1, 5)
          modif(tabP4)
          j3pAffiche(tabP4[0][1], null, ' $-$ ')
          j3pAffiche(tabP4[0][3], null, ' $-$ ')
          stor.zone3 = new ZoneStyleMathquill1(tabP4[0][0], {
            restric: '0123456789°',
            limitenb: 9,
            limite: 4,
            hasAutoKeyboard: true

          })
          stor.zone4 = new ZoneStyleMathquill1(tabP4[0][2], {
            restric: 'ABCDEFGHIJKLMNOPQRSTUVWXYZabcdefghijklmnopqrstuvwxyz[]()^$',
            limitenb: 9,
            limite: 4,
            hasAutoKeyboard: true

          })
          stor.zone5 = new ZoneStyleMathquill1(tabP4[0][4], {
            restric: 'ABCDEFGHIJKLMNOPQRSTUVWXYZabcdefghijklmnopqrstuvwxyz[]()^$',
            limitenb: 9,
            limite: 4,
            hasAutoKeyboard: true

          })
          // L2
          const tabP5 = addDefaultTable(tabP3[1][2], 1, 5)
          modif(tabP5)
          j3pAffiche(tabP5[0][1], null, ' $-$ ')
          j3pAffiche(tabP5[0][3], null, ' $-$ ')
          stor.zone6 = new ZoneStyleMathquill1(tabP5[0][0], {
            restric: '0123456789°',
            limitenb: 9,
            limite: 4,
            hasAutoKeyboard: true

          })
          stor.zone7 = new ZoneStyleMathquill1(tabP5[0][2], {
            restric: '0123456789°',
            limitenb: 9,
            limite: 4,
            hasAutoKeyboard: true

          })
          stor.zone8 = new ZoneStyleMathquill1(tabP5[0][4], {
            restric: '0123456789°',
            limitenb: 9,
            limite: 4,
            hasAutoKeyboard: true

          })
          // L3
          const tabP6 = addDefaultTable(tabP3[2][2], 1, 3)
          modif(tabP6)
          j3pAffiche(tabP6[0][1], null, ' $-$ ')
          stor.zone9 = new ZoneStyleMathquill1(tabP6[0][0], {
            restric: '0123456789°',
            limitenb: 9,
            limite: 4,
            hasAutoKeyboard: true

          })
          stor.zone10 = new ZoneStyleMathquill1(tabP6[0][2], {
            restric: '0123456789°',
            limitenb: 9,
            limite: 4,
            hasAutoKeyboard: true

          })
          // L4
          stor.zone11 = new ZoneStyleMathquill1(tabP3[3][2], {
            restric: '0123456789°',
            limitenb: 9,
            limite: 4,
            hasAutoKeyboard: true

          })
        }
          break
        case 'Isocele':
        case 'Equilateral':
        case 'Rectangle':
          stor.tabGro = addDefaultTable(stor.lesdiv.travail, 3, 2)
          modif(stor.tabGro)
          if (ds.Description) {
            stor.tabP1 = addDefaultTable(stor.tabGro[0][0], 1, 7)
            modif(stor.tabP1)
            j3pAffiche(stor.tabP1[0][0], null, 'Dans le triangle &nbsp;')
            stor.zone1 = new ZoneStyleMathquill1(stor.tabP1[0][1], {
              restric: 'ABCDEFGHIJKLMNOPQRSTUVWXYZabcdefghijklmnopqrstuvwxyz[]()^$',
              limitenb: 9,
              limite: 4,
              hasAutoKeyboard: true

            })
            stor.tabP1[0][2].innerHTML = '&nbsp;'
            stor.liste1 = ListeDeroulante.create(stor.tabP1[0][3], ['Choisir', 'rectangle', 'quelconque', 'isocèle', 'équilatéral'], { centre: true, alignmathquill: true, onChange: gerelistenature })
            stor.tabP1[0][6].innerHTML = '&nbsp;,'
          } else {
            let aaj
            switch (stor.Lexo.prop) {
              case 'Isocele': aaj = ff.nomangle + ' isocèle en ' + ff.nomsom
                break
              case 'Equilateral': aaj = ' équilatéral ' + ff.nomangle
                break
              case 'Rectangle': aaj = ff.nomangle + ' rectangle en ' + ff.nomsom
            }
            j3pAffiche(stor.tabGro[0][0], null, 'Dans le triangle ' + aaj + ' ,\n')
          }
          stor.liste2 = ListeDeroulante.create(stor.tabGro[1][0], ['Choisir', 'la somme des angles aigus égale 90°.', 'les angles à la base sont égaux.', 'les angles mesurent 60°.'], { centre: false, alignmathquill: true, onChange: gerelistepropriete })
          break
      }
    } else {
      stor.tabGro = addDefaultTable(stor.lesdiv.travail, 1, 2)
      modif(stor.tabGro)
      j3pAffiche(stor.tabGro[0][0], null, ' $ \\widehat{' + ff.nomangle + '}=$ ')
      stor.zoneRep = new ZoneStyleMathquill1(stor.tabGro[0][1], {
        restric: '0123456789°',
        limitenb: 9,
        limite: 4,
        hasAutoKeyboard: true

      })
    }
  }

  function faisChoixExos () {
    return ds.exos.pop()
  }

  function Figure () {
    const objet = this
    let i, ctx
    objet.asel = []
    objet.listesel = []

    function ajoutelangle (l1, l2, l3) {
      // l angle
      const coc = cooxy(l2)
      const cop1 = cooxy(l1)
      const cop2 = cooxy(l3)
      let u
      let angledeb = Math.atan((coc.y - cop1.y) / (coc.x - cop1.x)) * 180 / Math.PI
      let anglefin = Math.atan((coc.y - cop2.y) / (coc.x - cop2.x)) * 180 / Math.PI
      if (cop1.x < coc.x) {
        angledeb += 180
      }
      if (cop2.x < coc.x) {
        anglefin += 180
      }
      if ((coc.x === cop1.x) && (cop1.y < coc.y)) {
        angledeb = 270
      }
      if ((coc.x === cop2.x) && (cop2.y < coc.y)) {
        anglefin = 270
      }
      if ((coc.x === cop1.x) && (cop1.y > coc.y)) {
        angledeb = 90
      }
      if ((coc.x === cop2.x) && (cop2.y > coc.y)) {
        anglefin = 90
      }
      if (angledeb < 0) {
        angledeb += 360
      }
      if (angledeb >= 360) {
        angledeb -= 360
      }
      if (anglefin < 0) {
        anglefin += 360
      }
      if (anglefin >= 360) {
        anglefin -= 360
      }
      if (anglefin < angledeb) {
        u = angledeb
        angledeb = anglefin
        anglefin = u
      }
      const angle = anglefin - angledeb
      if (angle > 180) {
        u = angledeb
        angledeb = anglefin
        anglefin = u
      }

      objet.fifig.push({
        type: 'secteur',
        nom: 'angle(' + l1 + l2 + l3 + ')',
        par1: l2,
        par2: angledeb,
        par3: anglefin,
        par4: 1,
        visible: true,
        style: {
          couleur: '#5555ff',
          epaisseur: 1,
          opacite: 1
        },
        affichenom: false
      })

      return 'angle(' + l1 + l2 + l3 + ')'
    }

    function cooxy (lettre) {
      for (let i = 0; i < objet.fifig.length; i++) {
        if (objet.fifig[i].nom === lettre) {
          return {
            x: objet.fifig[i].par1,
            y: objet.fifig[i].par2
          }
        }
      }
      j3pShowError(Error(`Point : ${lettre} introuvable`))
    }

    function ajoutelepoint (a, b, c, f) {
      const d = (f !== true)
      objet.fifig.push({
        type: 'point',
        nom: a,
        par1: b,
        x: b,
        par2: c,
        y: c,
        fixe: true,
        visible: true,
        etiquette: false,
        style: {
          couleur: '#000000',
          epaisseur: 0,
          taillepoint: 9,
          decal: [+10, 0]
        },
        affichenom: d,
        phantom: !d
      })
    }

    function ajoutesegment (a, b, f) {
      const col = f ?? '#000000'
      objet.fifig.push({
        type: 'segment',
        nom: '[' + a + b + ']',
        par1: a,
        par2: b,
        fixe: true,
        visible: true,
        etiquette: false,
        style: {
          couleur: col,
          epaisseur: 2,
          taillepoint: 9,
          decal: [+10, 0]
        },
        affichenom: false,
        phantom: false
      })
    }

    function ajoutemesure (a, b, c, f) {
      objet.fifig.push({
        type: 'point',
        nom: f,
        foNom: a,
        par1: b,
        x: b,
        par2: c,
        y: c,
        fixe: true,
        visible: true,
        etiquette: false,
        style: {
          couleur: '#000000',
          epaisseur: 0,
          taillepoint: 0,
          decal: [+10, 0]
        },
        affichenom: true,
        phantom: true,
        angle: true
      })
    }

    function getType (nom) {
      const fifig = objet.fifig.find(fifig => fifig.nom === nom)
      return fifig ? fifig.type : 'error'
    }

    function compareArray (a1) {
      for (let a = 0; a < a1.length; ++a) {
        if (Array.isArray(a1[a])) {
          return compareArray(a1[a])
        } else {
          if (a1[a] !== 0) {
            return false
          }
        }
      }

      return true
    }

    function fillCircle (x, y, z) {
      ctx.beginPath()
      ctx.lineWidth = 4
      ctx.fillStyle = '#000000'
      ctx.arc(x, y, z, 0, 2 * Math.PI)
      ctx.fill()
    }

    function drawLine (x, y, z, u) {
      ctx.beginPath()
      ctx.lineWidth = 4
      ctx.moveTo(x, y)
      ctx.lineTo(z, u)
      ctx.stroke()
    }

    function carrevide2 (x, y, z, t) {
      return compareArray(ctx.getImageData(x, y, z, t).data)
    }

    function drawrect (x, y, h, l) {
      drawLine(x, y, x + l, y)
      drawLine(x + l, y, x + l, y + h)
      drawLine(x + l, y + h, x, y + h)
      drawLine(x, y + h, x, y)
    }

    objet.corrige = function (bool) {
      let i
      let lacoul1 = me.styles.cbien
      let lacoul2 = '#ccffee'
      if (!bool) {
        lacoul1 = me.styles.cfaux
        lacoul2 = '#ff9999'
      }
      for (i = 0; i < objet.listesel.length; i++) {
        if (objet.listesel[i].clicked) {
          objet.listesel[i].style = 'stroke-width:4;stroke:' + lacoul1 + ';fill:' + lacoul2
        }
      }
    }

    objet.desactiveTout = function () {
      let i
      for (i = 0; i < objet.listesel.length; i++) {
        objet.listesel[i].removeEventListener('mousedown', objet.mousedown2, false)
        objet.listesel[i].removeEventListener('mouseout', objet.mouseout2, false)
        objet.listesel[i].removeEventListener('mouseover', objet.mouseover2, false)
        objet.listesel[i].style.cursor = ''
      }

      if (me.donneesSection.Deplacement) {
        j3pDetruit(objet.monbout)
        j3pDetruit(objet.monbout2)
        j3pDetruit(objet.monbout3)
        j3pDetruit(objet.monbout4)
        objet.montri.setAttribute('cursor', '')
        objet.montri.removeEventListener('click', objet.select, false)
        objet.montri.removeEventListener('mouseover', objet.foncover, false)
        objet.montri.removeEventListener('mouseout', objet.foncout, false)

        objet.montriflip.setAttribute('cursor', '')
        objet.montriflip.removeEventListener('click', objet.select, false)
        objet.montriflip.removeEventListener('mouseover', objet.foncover, false)
        objet.montriflip.removeEventListener('mouseout', objet.foncout, false)
      }
    }

    objet.conteneur = stor.lesdiv.figure
    objet.lexo = stor.Lexo
    objet.listedeselemedessine = []
    objet.longx = 15
    objet.hauty = 7.5
    objet.nomspris = []
    objet.fifig = []
    objet.lisecss = []

    objet.lettres = []
    for (i = 0; i < 4; i++) {
      addMaj(objet.lettres)
    }
    const lettres = objet.lettres

    /// determine longueurs et angles

    do {
      objet.a1 = j3pGetRandomInt(15, 50)
      objet.a2 = j3pGetRandomInt(80, 100)
      objet.a3 = 180 - objet.a1 - objet.a2
    } while (objet.a1 === objet.a2 || objet.a2 === objet.a3 || objet.a1 === objet.a3)
    do {
      objet.a4 = j3pGetRandomInt(50, 70)
      objet.a5 = j3pGetRandomInt(50, 70)
      objet.a6 = 180 - objet.a4 - objet.a5
    } while (objet.a4 === objet.a5 || objet.a5 === objet.a6 || objet.a6 === objet.a4)

    /// det qui sera visibile
    objet.va11 = false
    objet.va12 = false
    objet.va13 = false
    objet.va22 = false
    objet.va23 = false
    objet.va21 = false

    // pour equi faudra modif un peu
    let px = 1
    let py = 1

    objet.numc = stor.numc
    stor.numc++
    stor.boolinut = !stor.boolinut
    if (stor.numc === 3) { stor.numc = 0 }
    objet.doninut = ((ds.Donnees_Inutiles === 'oui') || ((ds.Donnees_Inutiles === 'Les deux') && j3pGetRandomBool()))
    if (objet.doninut && stor.boolinut) {
      objet.numc += 3
    }
    const haz = j3pGetRandomInt(0, 2)
    stor.doninutG = objet.doninut
    switch (stor.Lexo.prop) {
      case 'Quelconque':
        switch (objet.numc) {
          case 0:
            if (objet.doninut) {
              objet.va21 = objet.va23 = true
              objet.nomtriangle2 = lettres[1 + haz] + lettres[1 + (1 + haz) % 3] + lettres[1 + (2 + haz) % 3]
              objet.angle3 = lettres[2] + lettres[3] + lettres[1]
              objet.angle4 = lettres[1] + lettres[2] + lettres[3]
              objet.val3 = objet.a6
              objet.val4 = objet.a4
            }
            objet.nomtriangle = lettres[haz] + lettres[(1 + haz) % 3] + lettres[(2 + haz) % 3]
            objet.nomangle = lettres[0] + lettres[1] + lettres[2]
            objet.va11 = objet.va13 = true
            objet.nomautreangle1 = lettres[1] + lettres[0] + lettres[2]
            objet.nomautreangle2 = lettres[0] + lettres[2] + lettres[1]
            objet.val1 = objet.a1
            objet.val2 = objet.a3
            objet.lam = objet.a2
            break
          case 1:
            if (objet.doninut) {
              objet.va21 = objet.va23 = true
              objet.nomtriangle2 = lettres[1 + haz] + lettres[1 + (1 + haz) % 3] + lettres[1 + (2 + haz) % 3]
              objet.angle3 = lettres[2] + lettres[3] + lettres[1]
              objet.angle4 = lettres[1] + lettres[2] + lettres[3]
              objet.val3 = objet.a6
              objet.val4 = objet.a4
            }
            objet.nomtriangle = lettres[haz] + lettres[(1 + haz) % 3] + lettres[(2 + haz) % 3]
            objet.nomangle = lettres[0] + lettres[2] + lettres[1]
            objet.va11 = objet.va12 = true
            objet.nomautreangle1 = lettres[2] + lettres[1] + lettres[0]
            objet.nomautreangle2 = lettres[1] + lettres[0] + lettres[2]
            objet.val1 = objet.a2
            objet.val2 = objet.a1
            objet.lam = objet.a3
            break
          case 2:
            objet.va13 = objet.va12 = true
            if (objet.doninut) {
              objet.va21 = objet.va23 = true
              objet.nomtriangle2 = lettres[1 + haz] + lettres[1 + (1 + haz) % 3] + lettres[1 + (2 + haz) % 3]
              objet.angle3 = lettres[2] + lettres[3] + lettres[1]
              objet.angle4 = lettres[1] + lettres[2] + lettres[3]
              objet.val3 = objet.a6
              objet.val4 = objet.a4
            }
            objet.nomtriangle = lettres[haz] + lettres[(1 + haz) % 3] + lettres[(2 + haz) % 3]
            objet.nomangle = lettres[1] + lettres[0] + lettres[2]
            objet.nomautreangle1 = lettres[1] + lettres[2] + lettres[0]
            objet.nomautreangle2 = lettres[0] + lettres[1] + lettres[2]
            objet.val1 = objet.a3
            objet.val2 = objet.a2
            objet.lam = objet.a1
            break
          case 3:
            objet.va22 = objet.va23 = true
            if (objet.doninut) {
              objet.va11 = objet.va13 = true
              objet.nomtriangle2 = lettres[haz] + lettres[(1 + haz) % 3] + lettres[(2 + haz) % 3]
              objet.angle3 = lettres[1] + lettres[0] + lettres[2]
              objet.angle4 = lettres[1] + lettres[2] + lettres[0]
              objet.val3 = objet.a1
              objet.val4 = objet.a3
            }
            objet.nomtriangle = lettres[1 + haz] + lettres[1 + (1 + haz) % 3] + lettres[1 + (2 + haz) % 3]
            objet.nomangle = lettres[3] + lettres[2] + lettres[1]
            objet.nomautreangle1 = lettres[1] + lettres[3] + lettres[2]
            objet.nomautreangle2 = lettres[3] + lettres[1] + lettres[2]
            objet.val1 = objet.a6
            objet.val2 = objet.a5
            objet.lam = objet.a4
            break
          case 4:
            objet.va21 = objet.va23 = true
            if (objet.doninut) {
              objet.va11 = objet.va13 = true
              objet.nomtriangle2 = lettres[haz] + lettres[(1 + haz) % 3] + lettres[(2 + haz) % 3]
              objet.angle3 = lettres[1] + lettres[0] + lettres[2]
              objet.angle4 = lettres[1] + lettres[2] + lettres[0]
              objet.val3 = objet.a1
              objet.val4 = objet.a3
            }
            objet.nomtriangle = lettres[1 + haz] + lettres[1 + (1 + haz) % 3] + lettres[1 + (2 + haz) % 3]
            objet.nomangle = lettres[3] + lettres[1] + lettres[2]
            objet.nomautreangle1 = lettres[1] + lettres[3] + lettres[2]
            objet.nomautreangle2 = lettres[3] + lettres[2] + lettres[1]
            objet.val1 = objet.a6
            objet.val2 = objet.a4
            objet.lam = objet.a5
            break
          case 5:
            objet.va22 = objet.va21 = true
            if (objet.doninut) {
              objet.va11 = objet.va13 = true
              objet.nomtriangle2 = lettres[haz] + lettres[(1 + haz) % 3] + lettres[(2 + haz) % 3]
              objet.angle3 = lettres[1] + lettres[0] + lettres[2]
              objet.angle4 = lettres[1] + lettres[2] + lettres[0]
              objet.val3 = objet.a1
              objet.val4 = objet.a3
            }
            objet.nomtriangle = lettres[1 + haz] + lettres[1 + (1 + haz) % 3] + lettres[1 + (2 + haz) % 3]
            objet.nomangle = lettres[1] + lettres[3] + lettres[2]
            objet.nomautreangle1 = lettres[1] + lettres[2] + lettres[3]
            objet.nomautreangle2 = lettres[3] + lettres[1] + lettres[2]
            objet.val1 = objet.a4
            objet.val2 = objet.a5
            objet.lam = objet.a6
        }
        break
      case 'Isocele':
        switch (objet.numc) {
          case 0:
            objet.a3 = objet.a2 = j3pGetRandomInt(75, 89)
            objet.a1 = 180 - objet.a2 - objet.a3
            objet.va13 = true
            if (objet.doninut) {
              objet.va21 = objet.va23 = true
              objet.nomtriangle2 = lettres[1 + haz] + lettres[1 + (1 + haz) % 3] + lettres[1 + (2 + haz) % 3]
              objet.angle3 = lettres[2] + lettres[3] + lettres[1]
              objet.angle4 = lettres[1] + lettres[2] + lettres[3]
              objet.val3 = objet.a6
              objet.val4 = objet.a4
            }
            objet.nomtriangle = lettres[haz] + lettres[(1 + haz) % 3] + lettres[(2 + haz) % 3]
            ajoutelepoint('a', 4, 4.5, true)
            ajoutelepoint('b', 5, 3.5, true)
            ajoutelepoint('c', 4.3, 4.5, true)
            ajoutelepoint('d', 5.3, 3.5, true)
            ajoutesegment('a', 'b', '#ff8888')
            ajoutesegment('c', 'd', '#ff8888')
            ajoutelepoint('e', 6, 2, true)
            ajoutelepoint('f', 5, 1, true)
            ajoutelepoint('g', 6.3, 2, true)
            ajoutelepoint('h', 5.3, 1, true)
            ajoutesegment('e', 'f', '#ff8888')
            ajoutesegment('g', 'h', '#ff8888')
            objet.nomangle = lettres[0] + lettres[1] + lettres[2]
            objet.nomautreangle1 = lettres[1] + lettres[2] + lettres[0]
            objet.val1 = objet.a3
            objet.nomsom = lettres[0]
            objet.lam = objet.a3
            break
          case 1:
            objet.a3 = objet.a2 = j3pGetRandomInt(75, 89)
            objet.a1 = 180 - objet.a2 - objet.a3
            if (objet.doninut) {
              objet.va22 = objet.va21 = true
              objet.nomtriangle2 = lettres[1 + haz] + lettres[1 + (1 + haz) % 3] + lettres[1 + (2 + haz) % 3]
              objet.angle3 = lettres[2] + lettres[3] + lettres[1]
              objet.angle4 = lettres[3] + lettres[1] + lettres[2]
              objet.val3 = objet.a6
              objet.val4 = objet.a5
            }
            objet.nomtriangle = lettres[haz] + lettres[(1 + haz) % 3] + lettres[(2 + haz) % 3]
            ajoutelepoint('a', 4, 4.5, true)
            ajoutelepoint('b', 5, 3.5, true)
            ajoutelepoint('c', 4.3, 4.5, true)
            ajoutelepoint('d', 5.3, 3.5, true)
            ajoutesegment('a', 'b', '#ff8888')
            ajoutesegment('c', 'd', '#ff8888')
            ajoutelepoint('e', 6, 2, true)
            ajoutelepoint('f', 5, 1, true)
            ajoutelepoint('g', 6.3, 2, true)
            ajoutelepoint('h', 5.3, 1, true)
            ajoutesegment('e', 'f', '#ff8888')
            ajoutesegment('g', 'h', '#ff8888')
            objet.nomsom = lettres[0]
            if (ds.Raisonnement === true) {
              objet.va12 = true
              objet.nomangle = lettres[0] + lettres[2] + lettres[1]
              objet.nomautreangle1 = lettres[2] + lettres[1] + lettres[0]
              objet.val1 = objet.a2
              objet.lam = objet.a2
            } else {
              objet.va11 = true
              objet.nomangle = lettres[0] + lettres[2] + lettres[1]
              objet.nomautreangle1 = lettres[2] + lettres[0] + lettres[1]
              objet.nomautreangle2 = lettres[2] + lettres[1] + lettres[0]
              objet.val1 = objet.a1
              objet.lam = objet.a2
            }
            break
          case 2:
            objet.a1 = objet.a3 = j3pGetRandomInt(20, 59)
            objet.a2 = 180 - objet.a3 - objet.a1
            if (objet.doninut) {
              objet.va22 = objet.va21 = true
              objet.nomtriangle2 = lettres[1 + haz] + lettres[1 + (1 + haz) % 3] + lettres[1 + (2 + haz) % 3]
              objet.angle3 = lettres[2] + lettres[3] + lettres[1]
              objet.angle4 = lettres[3] + lettres[1] + lettres[2]
              objet.val3 = objet.a6
              objet.val4 = objet.a5
            }
            objet.nomtriangle = lettres[haz] + lettres[(1 + haz) % 3] + lettres[(2 + haz) % 3]
            ajoutelepoint('a', 4, 4.5, true)
            ajoutelepoint('b', 5, 3.5, true)
            ajoutelepoint('c', 4.3, 4.5, true)
            ajoutelepoint('d', 5.3, 3.5, true)
            ajoutesegment('a', 'b', '#ff8888')
            ajoutesegment('c', 'd', '#ff8888')
            ajoutelepoint('e', 8.8, 4.5, true)
            ajoutelepoint('f', 7.8, 3.5, true)
            ajoutelepoint('g', 9.1, 4.5, true)
            ajoutelepoint('h', 8.1, 3.5, true)
            ajoutesegment('e', 'f', '#ff8888')
            ajoutesegment('g', 'h', '#ff8888')
            objet.nomsom = lettres[1]
            if (ds.Raisonnement === true) {
              objet.va13 = true
              objet.nomangle = lettres[2] + lettres[0] + lettres[1]
              objet.nomautreangle1 = lettres[1] + lettres[2] + lettres[0]
              objet.val1 = objet.a3
              objet.lam = objet.a1
            } else {
              objet.va11 = true
              objet.nomangle = lettres[2] + lettres[1] + lettres[0]
              objet.nomautreangle1 = lettres[1] + lettres[0] + lettres[2]
              objet.nomautreangle2 = lettres[1] + lettres[2] + lettres[0]
              objet.val1 = objet.a1
              objet.lam = objet.a2
            }
            break
          case 3:
            objet.a4 = objet.a6 = j3pGetRandomInt(20, 59)
            objet.a5 = 180 - objet.a4 - objet.a6
            objet.va23 = true
            if (objet.doninut) {
              objet.va12 = objet.va11 = true
              objet.nomtriangle2 = lettres[haz] + lettres[(1 + haz) % 3] + lettres[(2 + haz) % 3]
              objet.angle3 = lettres[1] + lettres[0] + lettres[2]
              objet.angle4 = lettres[0] + lettres[1] + lettres[2]
              objet.val3 = objet.a1
              objet.val4 = objet.a2
            }
            objet.nomtriangle = lettres[1 + haz] + lettres[1 + (1 + haz) % 3] + lettres[1 + (2 + haz) % 3]
            ajoutelepoint('a', 10, 6.7, true)
            ajoutelepoint('b', 9, 5.7, true)
            ajoutelepoint('c', 10.3, 6.7, true)
            ajoutelepoint('d', 9.3, 5.7, true)
            ajoutesegment('a', 'b', '#ff8888')
            ajoutesegment('c', 'd', '#ff8888')
            ajoutelepoint('e', 8.8, 4.5, true)
            ajoutelepoint('f', 7.8, 3.5, true)
            ajoutelepoint('g', 9.1, 4.5, true)
            ajoutelepoint('h', 8.1, 3.5, true)
            ajoutesegment('e', 'f', '#ff8888')
            ajoutesegment('g', 'h', '#ff8888')
            objet.nomangle = lettres[3] + lettres[2] + lettres[1]
            objet.nomautreangle1 = lettres[2] + lettres[3] + lettres[1]
            objet.val1 = objet.a6
            objet.nomsom = lettres[1]
            objet.lam = objet.a6
            break
          case 4:
            objet.a5 = objet.a6 = j3pGetRandomInt(20, 59)
            objet.a4 = 180 - objet.a5 - objet.a6
            if (objet.doninut) {
              objet.va12 = objet.va11 = true
              objet.nomtriangle2 = lettres[haz] + lettres[(1 + haz) % 3] + lettres[(2 + haz) % 3]
              objet.angle3 = lettres[1] + lettres[0] + lettres[2]
              objet.angle4 = lettres[0] + lettres[1] + lettres[2]
              objet.val3 = objet.a1
              objet.val4 = objet.a2
            }
            objet.nomtriangle = lettres[1 + haz] + lettres[1 + (1 + haz) % 3] + lettres[1 + (2 + haz) % 3]
            ajoutelepoint('a', 11, 4.5, true)
            ajoutelepoint('b', 12, 3.5, true)
            ajoutelepoint('c', 11.3, 4.5, true)
            ajoutelepoint('d', 12.3, 3.5, true)
            ajoutesegment('a', 'b', '#ff8888')
            ajoutesegment('c', 'd', '#ff8888')
            ajoutelepoint('e', 8.8, 4.5, true)
            ajoutelepoint('f', 7.8, 3.5, true)
            ajoutelepoint('g', 9.1, 4.5, true)
            ajoutelepoint('h', 8.1, 3.5, true)
            ajoutesegment('e', 'f', '#ff8888')
            ajoutesegment('g', 'h', '#ff8888')
            if (ds.Raisonnement === true) {
              objet.va22 = true
              objet.nomautreangle1 = lettres[2] + lettres[1] + lettres[3]
              objet.val1 = objet.a5
            } else {
              objet.va21 = true
              objet.nomautreangle1 = lettres[3] + lettres[2] + lettres[1]
              objet.nomautreangle2 = lettres[3] + lettres[1] + lettres[2]
              objet.val1 = objet.a4
            }
            objet.nomangle = lettres[2] + lettres[3] + lettres[1]
            objet.lam = objet.a6
            objet.nomsom = lettres[2]
            break
          case 5:
            objet.a6 = objet.a5 = j3pGetRandomInt(20, 59)
            objet.a4 = 180 - objet.a6 - objet.a5
            if (objet.doninut) {
              objet.va12 = objet.va11 = true
              objet.nomtriangle2 = lettres[haz] + lettres[(1 + haz) % 3] + lettres[(2 + haz) % 3]
              objet.angle3 = lettres[1] + lettres[0] + lettres[2]
              objet.angle4 = lettres[0] + lettres[1] + lettres[2]
              objet.val3 = objet.a1
              objet.val4 = objet.a2
            }
            objet.nomtriangle = lettres[1 + haz] + lettres[1 + (1 + haz) % 3] + lettres[1 + (2 + haz) % 3]
            ajoutelepoint('a', 11, 4.5, true)
            ajoutelepoint('b', 12, 3.5, true)
            ajoutelepoint('c', 11.3, 4.5, true)
            ajoutelepoint('d', 12.3, 3.5, true)
            ajoutesegment('a', 'b', '#ff8888')
            ajoutesegment('c', 'd', '#ff8888')
            ajoutelepoint('e', 8.8, 4.5, true)
            ajoutelepoint('f', 7.8, 3.5, true)
            ajoutelepoint('g', 9.1, 4.5, true)
            ajoutelepoint('h', 8.1, 3.5, true)
            ajoutesegment('e', 'f', '#ff8888')
            ajoutesegment('g', 'h', '#ff8888')
            if (ds.Raisonnement === true) {
              objet.va23 = true
              objet.nomautreangle1 = lettres[1] + lettres[3] + lettres[2]
              objet.val1 = objet.a6
              objet.nomangle = lettres[2] + lettres[1] + lettres[3]
              objet.lam = objet.a5
            } else {
              objet.nomautreangle1 = lettres[2] + lettres[3] + lettres[1]
              objet.nomautreangle2 = lettres[2] + lettres[1] + lettres[3]
              objet.val1 = objet.a6
              objet.va23 = true
              objet.nomangle = lettres[1] + lettres[2] + lettres[3]
              objet.lam = objet.a4
            }
            objet.nomsom = lettres[2]
            break
        }
        break
      case 'Rectangle':
        ajoutelepoint('b', 6.6, 5.7, true)
        ajoutelepoint('c', 6.9, 5.3, true)
        ajoutelepoint('d', 7.3, 5.6, true)
        ajoutesegment('c', 'b', '#ff8888')
        ajoutesegment('c', 'd', '#ff8888')
        objet.nomtriangle = lettres[haz] + lettres[(1 + haz) % 3] + lettres[(2 + haz) % 3]
        objet.nomtriangle2 = lettres[1 + haz] + lettres[1 + (1 + haz) % 3] + lettres[1 + (2 + haz) % 3]
        switch (objet.numc) {
          case 0:
          case 1:
          case 2:
            objet.a1 = j3pGetRandomInt(10, 40)
            objet.a2 = 90
            objet.a3 = 90 - objet.a1
            objet.va13 = true
            if (objet.doninut) {
              objet.va21 = objet.va23 = true
              objet.nomtriangle2 = lettres[1 + haz] + lettres[1 + (1 + haz) % 3] + lettres[1 + (2 + haz) % 3]
              objet.angle3 = lettres[2] + lettres[3] + lettres[1]
              objet.angle4 = lettres[1] + lettres[2] + lettres[3]
              objet.val3 = objet.a6
              objet.val4 = objet.a4
            }
            objet.nomangle = lettres[2] + lettres[0] + lettres[1]
            objet.nomautreangle1 = lettres[1] + lettres[2] + lettres[0]
            objet.val1 = objet.a3
            objet.nomsom = lettres[1]
            objet.lam = objet.a1
            break
          case 3:
          case 4:
          case 5:
            objet.a1 = j3pGetRandomInt(10, 40)
            objet.a2 = 90
            objet.a3 = 90 - objet.a1
            objet.va11 = true
            if (objet.doninut) {
              objet.va21 = objet.va23 = true
              objet.nomtriangle2 = lettres[1 + haz] + lettres[1 + (1 + haz) % 3] + lettres[1 + (2 + haz) % 3]
              objet.angle3 = lettres[2] + lettres[3] + lettres[1]
              objet.angle4 = lettres[1] + lettres[2] + lettres[3]
              objet.val3 = objet.a6
              objet.val4 = objet.a4
            }
            objet.nomangle = lettres[0] + lettres[2] + lettres[1]
            objet.nomautreangle1 = lettres[1] + lettres[0] + lettres[2]
            objet.val1 = objet.a1
            objet.nomsom = lettres[1]
            objet.lam = objet.a3
            break
        }
        break
      case 'Equilateral':
        px = 4
        py = 2
        ajoutelepoint('i', 8.8, 4.5, true)
        ajoutelepoint('j', 7.8, 3.5, true)
        ajoutelepoint('k', 9.1, 4.5, true)
        ajoutelepoint('l', 8.1, 3.5, true)
        ajoutesegment('i', 'j', '#ff8888')
        ajoutesegment('k', 'l', '#ff8888')
        if (objet.numc < 3) {
          ajoutelepoint('e', 7.3, 2.5, true)
          ajoutelepoint('f', 6.3, 1.5, true)
          ajoutelepoint('g', 7.6, 2.5, true)
          ajoutelepoint('h', 6.6, 1.5, true)
          ajoutesegment('e', 'f', '#ff8888')
          ajoutesegment('g', 'h', '#ff8888')

          ajoutelepoint('a', 4.9, 4.5, true)
          ajoutelepoint('b', 5.9, 3.5, true)
          ajoutelepoint('c', 5.2, 4.5, true)
          ajoutelepoint('d', 6.2, 3.5, true)
          ajoutesegment('a', 'b', '#ff8888')
          ajoutesegment('c', 'd', '#ff8888')
        } else {
          ajoutelepoint('e', 9, 6.6, true)
          ajoutelepoint('f', 10, 5.6, true)
          ajoutelepoint('g', 9.3, 6.6, true)
          ajoutelepoint('h', 10.3, 5.6, true)
          ajoutesegment('e', 'f', '#ff8888')
          ajoutesegment('g', 'h', '#ff8888')

          ajoutelepoint('a', 10.9, 4.5, true)
          ajoutelepoint('b', 11.9, 3.5, true)
          ajoutelepoint('c', 11.2, 4.5, true)
          ajoutelepoint('d', 12.2, 3.5, true)
          ajoutesegment('a', 'b', '#ff8888')
          ajoutesegment('c', 'd', '#ff8888')
        }
        switch (objet.numc) {
          case 0:
            objet.a1 = objet.a2 = objet.a3 = 60
            if (objet.doninut) {
              objet.va21 = objet.va23 = true
              objet.nomtriangle2 = lettres[1 + haz] + lettres[1 + (1 + haz) % 3] + lettres[1 + (2 + haz) % 3]
              objet.angle3 = lettres[2] + lettres[3] + lettres[1]
              objet.angle4 = lettres[1] + lettres[2] + lettres[3]
              objet.val3 = objet.a6
              objet.val4 = objet.a4
            }
            objet.nomangle = lettres[0] + lettres[1] + lettres[2]
            objet.nomtriangle = lettres[haz] + lettres[(1 + haz) % 3] + lettres[(2 + haz) % 3]
            break
          case 1:
            objet.a1 = objet.a2 = objet.a3 = 60
            if (objet.doninut) {
              objet.va21 = objet.va23 = true
              objet.nomtriangle2 = lettres[1 + haz] + lettres[1 + (1 + haz) % 3] + lettres[1 + (2 + haz) % 3]
              objet.angle3 = lettres[2] + lettres[3] + lettres[1]
              objet.angle4 = lettres[1] + lettres[2] + lettres[3]
              objet.val3 = objet.a6
              objet.val4 = objet.a4
            }
            objet.nomangle = lettres[0] + lettres[2] + lettres[1]
            objet.nomtriangle = lettres[haz] + lettres[(1 + haz) % 3] + lettres[(2 + haz) % 3]
            break
          case 2:
            objet.a1 = objet.a2 = objet.a3 = 60
            if (objet.doninut) {
              objet.va21 = objet.va23 = true
              objet.nomtriangle2 = lettres[1 + haz] + lettres[1 + (1 + haz) % 3] + lettres[1 + (2 + haz) % 3]
              objet.angle3 = lettres[2] + lettres[3] + lettres[1]
              objet.angle4 = lettres[1] + lettres[2] + lettres[3]
              objet.val3 = objet.a6
              objet.val4 = objet.a4
            }
            objet.nomangle = lettres[1] + lettres[0] + lettres[2]
            objet.nomtriangle = lettres[haz] + lettres[(1 + haz) % 3] + lettres[(2 + haz) % 3]
            break
          case 3:
            objet.a4 = objet.a5 = objet.a6 = 60
            if (objet.doninut) {
              objet.va12 = objet.va13 = true
              objet.nomtriangle2 = lettres[haz] + lettres[(1 + haz) % 3] + lettres[(2 + haz) % 3]
              objet.angle3 = lettres[2] + lettres[1] + lettres[0]
              objet.angle4 = lettres[1] + lettres[2] + lettres[0]
              objet.val3 = objet.a2
              objet.val4 = objet.a3
            }
            objet.nomtriangle = lettres[1 + haz] + lettres[1 + (1 + haz) % 3] + lettres[1 + (2 + haz) % 3]
            objet.nomangle = lettres[1] + lettres[3] + lettres[2]
            break
          case 4:
            objet.a4 = objet.a5 = objet.a6 = 60
            if (objet.doninut) {
              objet.va13 = objet.va12 = true
              objet.nomtriangle2 = lettres[haz] + lettres[(1 + haz) % 3] + lettres[(2 + haz) % 3]
              objet.angle3 = lettres[2] + lettres[1] + lettres[0]
              objet.angle4 = lettres[1] + lettres[2] + lettres[0]
              objet.val3 = objet.a2
              objet.val4 = objet.a3
            }
            objet.nomtriangle = lettres[1 + haz] + lettres[1 + (1 + haz) % 3] + lettres[1 + (2 + haz) % 3]
            objet.nomangle = lettres[1] + lettres[2] + lettres[3]
            break
          case 5:
            objet.a4 = objet.a5 = objet.a6 = 60
            if (objet.doninut) {
              objet.va13 = objet.va12 = true
              objet.nomtriangle2 = lettres[haz] + lettres[(1 + haz) % 3] + lettres[(2 + haz) % 3]
              objet.angle3 = lettres[2] + lettres[1] + lettres[0]
              objet.angle4 = lettres[1] + lettres[2] + lettres[0]
              objet.val3 = objet.a2
              objet.val4 = objet.a3
            }
            objet.nomtriangle = lettres[1 + haz] + lettres[1 + (1 + haz) % 3] + lettres[1 + (2 + haz) % 3]
            objet.nomangle = lettres[2] + lettres[1] + lettres[3]
            break
        }
        objet.lam = 60
        break
    }

    let adire = []
    let fdeb = objet.nomtriangle + ' est un triangle tel que:<br>'
    if (objet.doninut) {
      let uzm
      let ozm
      if (j3pGetRandomBool()) {
        uzm = objet.nomtriangle
        ozm = objet.nomtriangle2
      } else {
        ozm = objet.nomtriangle
        uzm = objet.nomtriangle2
      }
      fdeb = uzm + ' et ' + ozm + ' sont deux triangles tels que:<br>'
    }
    if (stor.Lexo.prop === 'Rectangle') {
      fdeb += objet.nomtriangle + ' est rectangle en ' + objet.nomsom + ',<br>'
    }
    if (stor.Lexo.prop === 'Isocele') {
      fdeb += objet.nomtriangle + ' est isocèle en ' + objet.nomsom + ',<br>'
    }
    if (stor.Lexo.prop === 'Equilateral') {
      fdeb += objet.nomtriangle + ' est  équilatéral .<br>'
    }

    if (stor.Lexo.type === 'Figure') {
      objet.points = []
      // objet.va11 = objet.va12 = objet.va13 = objet.va21 = objet.va22 = objet.va23 = false

      ajoutelepoint(lettres[0], px, py)
      ajoutelepoint(lettres[1], 7, 6)
      ajoutelepoint(lettres[2], 10, 2)
      if (objet.doninut) ajoutelepoint(lettres[3], 13.5, 6.5)

      if (objet.va12) ajoutelangle(lettres[0], lettres[1], lettres[2])
      if (objet.va13) ajoutelangle(lettres[1], lettres[2], lettres[0])
      if (objet.va11) ajoutelangle(lettres[2], lettres[0], lettres[1])

      if (objet.va11) ajoutemesure(objet.a1 + '°', 3.3, 1.7, 'qs')
      if (objet.va12) ajoutemesure(objet.a2 + '°', 7, 4.6, 'sd')
      if (objet.va13) ajoutemesure(objet.a3 + '°', 8.5, 2.6, 'df')

      if (objet.va22) ajoutelangle(lettres[3], lettres[1], lettres[2])
      if (objet.va23) ajoutelangle(lettres[1], lettres[3], lettres[2])
      if (objet.va21) ajoutelangle(lettres[1], lettres[2], lettres[3])

      if (objet.va21) ajoutemesure(objet.a4 + '°', 10.3, 3.4, 'kl')
      if (objet.va22) ajoutemesure(objet.a5 + '°', 9, 5.5, 'pm')
      if (objet.va23) ajoutemesure(objet.a6 + '°', 12, 5.5, 'ki')

      ajoutesegment(lettres[0], lettres[1])
      ajoutesegment(lettres[1], lettres[2])
      ajoutesegment(lettres[2], lettres[0])

      if (objet.doninut) ajoutesegment(lettres[3], lettres[1])
      if (objet.doninut) ajoutesegment(lettres[3], lettres[2])
      // ajoutesegment(lettres[5], lettres[3])

      objet.canvasContainer = j3pAddElt(objet.conteneur, 'div')
      const canvas = j3pAddElt(objet.canvasContainer, 'canvas', { style: { display: 'none', border: '2px solid black' } })
      canvas.setAttribute('width', objet.longx * 30)
      canvas.setAttribute('height', objet.hauty * 30)
      ctx = canvas.getContext('2d')

      let j
      for (i = 0; i < objet.fifig.length; i++) {
        switch (getType(objet.fifig[i].nom)) {
          case 'point':
            if (!objet.fifig[i].phantom) fillCircle(objet.fifig[i].par1 * 30, (objet.hauty - objet.fifig[i].par2) * 30, 5)
            break
          case 'segment': {
            let x1, x2, y1, y2
            for (j = 0; j < i; j++) {
              if (objet.fifig[i].par1 === objet.fifig[j].nom) {
                x1 = objet.fifig[j].par1
                y1 = objet.fifig[j].par2
                break
              }
            }
            for (j = 0; j < i; j++) {
              if (objet.fifig[i].par2 === objet.fifig[j].nom) {
                x2 = objet.fifig[j].par1
                y2 = objet.fifig[j].par2
                break
              }
            }
            drawLine(x1 * 30, (objet.hauty - y1) * 30, x2 * 30, (objet.hauty - y2) * 30)
            break
          }
        }
      }

      objet.unetaille = function (str) {
        let fontZone = objet.conteneur.style.fontFamily
        let elParent = objet.conteneur
        const el = stor.lesdiv.figure

        while (fontZone === '') {
          elParent = elParent.parentNode
          fontZone = (elParent.style && elParent.style.fontFamily) || me.styles.grand.enonce.fontFamily
        }
        const tailleFontZone = el.style.fontSize

        return Math.max($(el).textWidth(str, fontZone, tailleFontZone), 5)
      }

      const dn = 17
      for (i = 0; i < objet.fifig.length; i++) {
        if (objet.fifig[i].affichenom) {
          if (getType(objet.fifig[i].nom === 'point')) {
            let latailletextpoint = Math.round(1.5 * objet.unetaille(objet.fifig[i].nom))
            let cmptplace = -1
            let dnuse = dn - 4
            let pointplace = false

            let xbase, ybase
            do {
              cmptplace++
              dnuse += 2
              xbase = objet.fifig[i].par1 * 30
              ybase = (objet.hauty * 30) - objet.fifig[i].par2 * 30

              for (j = 0; j < 360; j = j + 4) {
                const xj = Math.round(xbase + dnuse * Math.cos(j * Math.PI / 180))
                const yj = Math.round(ybase + dnuse * Math.sin(j * Math.PI / 180))
                if (carrevide2(xj - Math.round(latailletextpoint / 2), yj - 10, latailletextpoint, 20)) {
                  pointplace = true
                  break
                }
              }
            } while ((!pointplace) && (cmptplace < 7))

            let monx = xbase + dnuse * Math.cos(j * Math.PI / 180)
            let mony = ybase + dnuse * Math.sin(j * Math.PI / 180)
            if (objet.fifig[i].foNom) {
              latailletextpoint = Math.round(objet.unetaille(objet.fifig[i].foNom))
              monx = xbase - latailletextpoint / 2
              mony = ybase
            }

            drawrect(monx - Math.round(latailletextpoint / 2), mony - 10, 20, latailletextpoint)

            let coolpoint = '#2E2E2E'
            if (objet.fifig[i].angle) coolpoint = '#5555FF'
            // for (let um=0;um<lepoint[lelemlepoint].points.length;um++){
            //        if (lepoint[lelemlepoint].points[um]==objet.fifig[i].nom){
            //            let coolpoint = '#8800FF';
            //            break;
            //    }
            // }
            let lenomaaf = objet.fifig[i].nom
            if (objet.fifig[i].foNom) lenomaaf = objet.fifig[i].foNom
            let nomok
            do {
              nomok = true
              for (let um = 0; um < objet.fifig.length; um++) {
                if (lenomaaf === objet.fifig[um].nom) {
                  nomok = false
                  lenomaaf += ' '
                  break
                }
              }
            } while (!nomok)
            objet.fifig.push({
              type: 'point',
              nom: lenomaaf,
              par1: monx / 30,
              par2: (objet.hauty * 30 - mony) / 30,
              fixe: true,
              visible: true,
              etiquette: true,
              style: {
                couleur: coolpoint,
                epaisseur: 3,
                taillepoint: 0,
                decal: [0, +20]
              },
              affichenom: false
            })
          }
        }
      }

      j3pDetruit(objet.canvasContainer)
      const hjk = addDefaultTable(objet.conteneur, 1, 2)
      modif(hjk)
      hjk[0][0].style.background = '#FFF'
      hjk[0][0].style.border = '1px solid black'
      hjk[0][1].style.width = '10px'
      const repere = new Repere({
        idConteneur: hjk[0][0],
        idDivRepere: j3pGetNewId(),
        visible: false,
        aimantage: false,
        larg: objet.longx * 30,
        haut: objet.hauty * 30,
        pasdunegraduationX: 1,
        pixelspargraduationX: 30,
        pasdunegraduationY: 1,
        pixelspargraduationY: 30,
        xO: 0,
        yO: objet.hauty * 30,
        debuty: 0,
        negatifs: true,
        trame: false,
        objets: objet.fifig,
        fixe: true
      })
      repere.construit()
    } else {
      switch (stor.Lexo.prop) {
        case 'Quelconque':
          adire.push({ nom: objet.nomautreangle1, val: objet.val1 })
          adire.push({ nom: objet.nomautreangle2, val: objet.val2 })
          if (objet.doninut) {
            adire.push({ nom: objet.angle3, val: objet.val3 })
            adire.push({ nom: objet.angle4, val: objet.val4 })
          }
          break
        case 'Isocele':
          adire.push({ nom: objet.nomautreangle1, val: objet.val1 })
          if (objet.doninut) {
            adire.push({ nom: objet.angle3, val: objet.val3 })
            adire.push({ nom: objet.angle4, val: objet.val4 })
          }
          break
        case 'Rectangle':
          adire.push({ nom: objet.nomautreangle1, val: objet.val1 })
          if (objet.doninut) {
            adire.push({ nom: objet.angle3, val: objet.val3 })
            adire.push({ nom: objet.angle4, val: objet.val4 })
          }
          break
      }
      j3pAffiche(stor.lesdiv.figure, null, '<b><u>Données</u></b>\n')
      j3pAffiche(stor.lesdiv.figure, null, fdeb)
      adire = j3pShuffle(adire)
      for (i = 0; i < adire.length; i++) {
        j3pAffiche(stor.lesdiv.figure, null, '$\\widehat{' + adire[i].nom + '} = ' + adire[i].val + ' °$ \n')
      }
    }
  }

  function initSection () {
    let i
    stor.numc = j3pGetRandomInt(0, 2)
    stor.boolinut = j3pGetRandomBool()

    // Construction de la page
    me.construitStructurePage('presentation3')

    me.donneesSection.exos = []

    let lp = []
    if (ds.Quelconque)lp.push('Quelconque')
    if (ds.Isocele)lp.push('Isocele')
    if (ds.Equilateral)lp.push('Equilateral')
    if (ds.Rectangle)lp.push('Rectangle')
    if (lp.lenght === 0) {
      console.error('Paramètres: Aucun exercice possible !')
      lp.push('Quelconque')
    }
    lp = j3pShuffle(lp)

    for (i = 0; i < me.donneesSection.nbitems; i++) {
      me.donneesSection.exos.push({ prop: lp[i % (lp.length)] })
    }
    me.donneesSection.exos = j3pShuffle(me.donneesSection.exos)

    lp = []
    if ((ds.Type_Enonce === 'Les deux') || (ds.Type_Enonce === 'Figure'))lp.push('Figure')
    if ((ds.Type_Enonce === 'Les deux') || (ds.Type_Enonce === 'Texte'))lp.push('Texte')
    lp = j3pShuffle(lp)

    for (i = 0; i < me.donneesSection.nbitems; i++) {
      me.donneesSection.exos[i].type = lp[i % (lp.length)]
    }
    me.donneesSection.exos = j3pShuffle(me.donneesSection.exos)

    me.typederreurs = [0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0]

    me.score = 0

    me.afficheTitre('Calculer un angle dans un triangle')
  }

  function yaReponse () {
    if (ds.Raisonnement) {
      switch (stor.Lexo.prop) {
        case 'Quelconque':
          if (ds.Description) {
            if (stor.zone1.reponse() === '') { stor.zone1.focus(); return false }
            if (stor.zone2.reponse() === '') { stor.zone2.focus(); return false }
          }
          if (stor.zone3.reponse() === '') { stor.zone3.focus(); return false }
          if (stor.zone4.reponse() === '') { stor.zone4.focus(); return false }
          if (stor.zone5.reponse() === '') { stor.zone5.focus(); return false }
          if (stor.zone6.reponse() === '') { stor.zone6.focus(); return false }
          if (stor.zone7.reponse() === '') { stor.zone7.focus(); return false }
          if (stor.zone8.reponse() === '') { stor.zone8.focus(); return false }
          if (stor.zone9.reponse() === '') { stor.zone9.focus(); return false }
          if (stor.zone10.reponse() === '') { stor.zone10.focus(); return false }
          if (stor.zone11.reponse() === '') { stor.zone11.focus(); return false }
          break
        default :
          if (ds.Description) {
            if (stor.zone1.reponse() === '') { stor.zone1.focus(); return false }
            if (!stor.liste1.changed) { stor.liste1.focus(); return false }
            if ((stor.liste1.reponse === 'rectangle') || (stor.liste1.reponse === 'isocèle')) {
              if (stor.zonesomme.reponse() === '') { stor.zonesomme.focus(); return false }
            }
          }
          if (!stor.liste2.changed) { stor.liste2.focus(); return false }
          switch (stor.liste2.reponse) {
            case 'la somme des angles aigus égale 90°.':
              if (stor.zoneR.reponse() === '') { stor.zoneR.focus(); return false }
              if (stor.zoneR2.reponse() === '') { stor.zoneR2.focus(); return false }
              if (stor.zoneR3.reponse() === '') { stor.zoneR3.focus(); return false }
              break
            case 'les angles à la base sont égaux.':
              if (stor.zoneI.reponse() === '') { stor.zoneI.focus(); return false }
              if (stor.zoneI2.reponse() === '') { stor.zoneI2.focus(); return false }
              break
          }
      }
    } else {
      if (stor.zoneRep.reponse() === '') { stor.zoneRep.focus(); return false }
    }
    return true
  }

  function tetang (x) {
    let i
    let j
    const f = x.reponsechap()
    const ff = stor.lafig

    // 3 lettres
    if (f[0].length < 3) {
      stor.ErrEcritureAngle3 = true
      x.corrige(false)
      return false
    }
    // majuscules
    for (i = 0; i < 3; i++) {
      if ('abcdefghijklmnopqrstuvwxyz'.indexOf(f[0][i]) !== -1) {
        stor.ErrEcritureAngleMaj = true
        x.corrige(false)
        return false
      }
    }
    // pas deux fois la mm
    for (i = 0; i < 2; i++) {
      for (j = i + 1; j < 3; j++) {
        if (f[0][i] === f[0][j]) {
          stor.ErrEcritureAngleDouble = true
          x.corrige(false)
          return false
        }
      }
    }
    // des lettres presentes
    for (i = 0; i < 3; i++) {
      if (ff.lettres.indexOf(f[0][i]) === -1) {
        stor.ErrEcritureAngleIcon = true
        x.corrige(false)
        return false
      }
    }

    // dan bon tri
    const bug = ff.nomtriangle
    for (i = 0; i < 3; i++) {
      if (bug.indexOf(f[0][i]) === -1) {
        stor.horstri = true
        x.corrige(false)
        return false
      }
    }
    if (f[1] !== 1) {
      stor.ErrEcritureAngleChap = true
      x.corrige(false)
      return false
    }
    return true
  }

  function tetmes (x, b) {
    const a = x.reponse()
    if (b) {
      if (a.indexOf('°') === -1) {
        stor.ErrMesDeg = true
        x.corrige(false)
        return false
      }
    }
    if (a.replace('°', '').indexOf('°') !== -1) {
      stor.ErrMesDegDoub = true
      x.corrige(false)

      return false
    }
    if (a[0] === '0') {
      stor.ErrMesZero = true
      x.corrige(false)
      return false
    }
    return true
  }

  function tetpoint (x) {
    const a = x.reponse()
    /// test que 1
    if (a.length !== 1) {
      stor.errPointNb = true
      x.corrige(false)
      return false
    }
    /// test pas de min
    if ('abcdefghijklmnopqrstuvwxyz'.indexOf(a[0]) !== -1) {
      stor.errPointMaj = true
      x.corrige(false)
      return false
    }
    /// test point existe
    if (stor.lafig.lettres.indexOf(a[0]) === -1) {
      stor.errPointIcon = true
      x.corrige(false)
      return false
    }
    return true
  }

  function tettriangle (a, b, x) {
    let i
    const c = x.reponse()

    // verif pas crochets
    if ((c.indexOf('[') !== -1) || (c.indexOf(']') !== -1)) {
      stor.EcrTriCroch = true
      x.corrige(false)
      return false
    }
    // verif pas parent
    if ((c.indexOf(')') !== -1) || (c.indexOf('(') !== -1)) {
      stor.EcrTriPar = true
      x.corrige(false)
      return false
    }
    // verif pas chapeau
    if (x.reponsechap()[1] !== 0) {
      stor.EcrTriChap = true
      x.corrige(false)
      return false
    }
    // test 3 lettres
    if (c.length !== 3) {
      stor.EcrTri3let = true
      x.corrige(false)
      return false
    }
    // test en majuscule
    for (i = 0; i < 3; i++) {
      if ('ABCDEFGHIJKLMNOPQRSTUVWXYZ'.indexOf(c[i]) === -1) {
        stor.EcrTriMaj = true
        x.corrige(false)
        return false
      }
    }
    // test pas de doublons
    if ((c[0] === c[1]) || (c[0] === c[2]) || (c[2] === c[1])) {
      stor.EcrTriDoub = true
      x.corrige(false)
      return false
    }
    // test ya les bonnes
    if ((c.indexOf(a[0]) === -1) || (c.indexOf(a[1]) === -1) || (c.indexOf(a[2]) === -1)) {
      // si ya pas les bonnes , test ya les fause
      if (stor.doninutG) {
        if ((c.indexOf(b[0]) === -1) || (c.indexOf(b[1]) === -1) || (c.indexOf(b[2]) === -1)) {
          stor.ErrTriNom2 = true
          x.corrige(false)
          return false
        } else {
          stor.ErrTriNom1 = true
          x.corrige(false)
          return false
        }
      } else {
        stor.ErrTriNom1 = true
        x.corrige(false)
        return false
      }
    }
    return true
  }

  function isRepOk () {
    stor.errPointIcon = false // point inconnu
    stor.errPointMaj = false // point majuscule
    stor.errPointNb = false // un point 1 lettre
    stor.ErrEcritureAngle3 = false // ok
    stor.ErrEcritureAngleMaj = false // ok
    stor.ErrEcritureAngleDouble = false // ok
    stor.ErrEcritureAngleIcon = false // ok
    stor.ErrEcritureAngleChap = false // ok
    stor.horstri = false // ok
    stor.ErrAngUnit = false // oubli de degre
    stor.EcrTri3let = false // 3 let dans un tri
    stor.EcrTriMaj = false // des maj dans un tri
    stor.EcrTriCroch = false // crochets dans un tri
    stor.EcrTriPar = false // parent dans un tri
    stor.EcrTriChap = false // parent dans un tri
    stor.ErrTriNom1 = false // a pris le mauvais tri
    stor.ErrTriNom2 = false // pas le bon de tri
    stor.EcrTriDoub = false // deux mm lettres dans tri
    stor.ErrMesDeg = false // oubli °
    stor.ErrMesDegDoub = false // deux fois °
    stor.ErrMesZero = false // zero inut
    stor.err180 = false // attendu 180
    stor.errerrWrongAngl = false // angle cherché pour calcul ?
    stor.errerrWrongAnglMM = false // angle 2 fois mm
    stor.errWrongVal = false // valeur inconnue
    stor.errWrongValM = false // velaur 2 fois
    stor.errWrongCal = false // erreur calc
    stor.errWrongRep = false // mauv rep
    stor.ErrNature = false // nature du tri
    stor.ErrSom = false // iso en ou rect en
    stor.errWrongAnglBase = false // angle base
    stor.ErrProp = false // la ppt correspond pas au tri part

    const ff = stor.lafig
    let ok = true
    let t1
    let t2
    let t3

    if (ds.Raisonnement) {
      switch (stor.Lexo.prop) {
        case 'Quelconque': {
          if (ds.Description) {
            ok = ok && tettriangle(ff.nomtriangle, ff.nomtriangle2, stor.zone1)
            ok = ok && tetmes(stor.zone2, true)
            if (stor.zone2.reponse() !== '180°') {
              stor.zone2.corrige(false)
              stor.err180 = true
              ok = false
            }
          }
          ok = ok && tetmes(stor.zone3, false)
          ok = ok && tetmes(stor.zone6, false)
          ok = ok && tetmes(stor.zone7, false)
          ok = ok && tetmes(stor.zone8, false)
          ok = ok && tetmes(stor.zone9, false)
          ok = ok && tetmes(stor.zone10, false)
          ok = ok && tetmes(stor.zone11, true)
          ok = ok && tetang(stor.zone4)
          ok = ok && tetang(stor.zone5)
          if (!ok) { return false }
          // verifi 3 = 180
          if ((stor.zone3.reponse() !== '180') && (stor.zone3.reponse() !== '180°')) {
            stor.zone3.corrige(false)
            stor.err180 = true
            ok = false
          }
          // verifie en zone 4 et 5 y’a les deux sommets des deux autres angles
          const ota = stor.zone4.reponse()[1]
          const ota2 = stor.zone5.reponse()[1]
          if ((ota !== ff.nomautreangle1[1]) && (ota !== ff.nomautreangle2[1])) {
            stor.zone4.corrige(false)
            stor.errWrongAngl = true
            ok = false
          }
          if ((ota2 !== ff.nomautreangle1[1]) && (ota2 !== ff.nomautreangle2[1])) {
            stor.zone5.corrige(false)
            stor.errWrongAngl = true
            ok = false
          }
          if (ota === ota2) {
            stor.zone5.corrige(false)
            stor.errWrongAnglMM = true
            ok = false
          }
          // veirfie 6 - 7 - 8 = rep  et 6 = 180
          if ((stor.zone6.reponse() !== '180') && (stor.zone6.reponse() !== '180°')) {
            stor.zone6.corrige(false)
            stor.err180 = true
            ok = false
          }
          t1 = parseInt(stor.zone7.reponse())
          t2 = parseInt(stor.zone8.reponse())
          if ((t1 !== ff.val1) && (t1 !== ff.val2)) {
            stor.zone7.corrige(false)
            stor.errWrongVal = true
            ok = false
          }
          if ((t2 !== ff.val1) && (t2 !== ff.val2)) {
            stor.zone8.corrige(false)
            stor.errWrongVal = true
            ok = false
          }
          if (t1 === t2) {
            stor.zone8.corrige(false)
            stor.errWrongValM = true
            ok = false
          }
          // verifi 9 - 10 = rep
          t1 = parseInt(stor.zone9.reponse())
          t2 = parseInt(stor.zone10.reponse())
          if (t1 - t2 !== ff.lam) {
            stor.zone9.corrige(false)
            stor.zone10.corrige(false)
            stor.errWrongCal = true
            ok = false
          }
          // verifi 11 = rep
          t1 = parseInt(stor.zone11.reponse())
          if (t1 !== ff.lam) {
            stor.zone11.corrige(false)
            stor.errWrongRep = true
            ok = false
          }
          return ok
        }

        default :
          t1 = stor.Lexo.prop
          if (ds.Description) {
            ok = ok && tettriangle(ff.nomtriangle, ff.nomtriangle2, stor.zone1)
            if (t1 !== stor.Lexo.prop) {
              stor.liste1.corrige(false)
              ok = false
              stor.ErrNature = true
            }
            t3 = stor.liste1.reponse
            switch (t3) {
              case 'isocèle': t3 = 'Isocele'
                break
              case 'rectangle': t3 = 'Rectangle'
                break
              case 'quelconque': t3 = 'Quelconque'
                break
              case 'équilatéral': t3 = 'Equilateral'
            }
            if ((t3 === 'Rectangle') || (t3 === 'Isocele')) {
              ok = ok && tetpoint(stor.zonesomme)
            }
            if (t3 !== t1) {
              stor.liste1.corrige(false)
              ok = false
            }
          }

          switch (stor.liste2.reponse) {
            case 'la somme des angles aigus égale 90°.':
              ok = ok && tetang(stor.zoneR)
              ok = ok && tetmes(stor.zoneR2, false)
              ok = ok && tetmes(stor.zoneR3, true)
              break
            case 'les angles à la base sont égaux.':
              ok = ok && tetang(stor.zoneI)
              ok = ok && tetmes(stor.zoneI2, true)
              break
          }
          if (!ok) return false

          // verif iso en
          if (ds.Raisonnement && ds.Description) {
            if ((t3 === 'Rectangle') || (t3 === 'Isocele')) {
              if (stor.zonesomme.reponse() !== ff.nomsom) {
                stor.zonesomme.corrige(false)
                ok = false
                stor.ErrSom = true
              }
            }
          }
          // verif bonne ppt
          if (((t1 === 'Rectangle') && (stor.liste2.reponse !== 'la somme des angles aigus égale 90°.')) ||
            ((t1 === 'Isocele') && (stor.liste2.reponse !== 'les angles à la base sont égaux.')) ||
            ((t1 === 'Equilateral') && (stor.liste2.reponse !== 'les angles mesurent 60°.'))) {
            stor.liste2.corrige(false)
            stor.ErrProp = true
            return false
          }

          switch (stor.liste2.reponse) {
            case 'la somme des angles aigus égale 90°.':
              if (stor.zoneR.reponse()[1] !== ff.nomautreangle1[1]) {
                stor.zoneR.corrige(false)
                stor.errWrongAngl = true
                ok = false
              }
              t1 = parseInt(stor.zoneR2.reponse())
              if (t1 !== ff.val1) {
                stor.zoneR2.corrige(false)
                stor.errWrongVal = true
                ok = false
              }
              t1 = parseInt(stor.zoneR3.reponse())
              if (t1 !== ff.lam) {
                stor.zoneR3.corrige(false)
                stor.errWrongRep = true
                ok = false
              }
              break
            case 'les angles à la base sont égaux.':
              if (stor.zoneI.reponse()[1] !== ff.nomautreangle1[1]) {
                stor.zoneI.corrige(false)
                ok = false
                if (stor.zoneI.reponse()[1] !== ff.nomangle[1]) {
                  stor.errWrongAnglBase = true
                } else {
                  stor.errerrWrongAnglMM = true
                }
              }
              t1 = parseInt(stor.zoneI2.reponse())
              if (t1 !== ff.lam) {
                stor.zoneI2.corrige(false)
                stor.errWrongRep = true
                ok = false
              }
              break
          }
      }
    } else {
      ok = tetmes(stor.zoneRep, true)
      ok = ok && (stor.zoneRep.reponse().replace('°', '') === String(ff.lam))
    }
    return ok
  } // isRepOk

  function desactiveAll () {
    if (ds.Raisonnement) {
      switch (stor.Lexo.prop) {
        case 'Quelconque':
          if (ds.Description) {
            stor.zone1.disable()
            stor.zone2.disable()
          }
          stor.zone3.disable()
          stor.zone4.disable()
          stor.zone5.disable()
          stor.zone6.disable()
          stor.zone7.disable()
          stor.zone8.disable()
          stor.zone9.disable()
          stor.zone10.disable()
          stor.zone11.disable()
          break
        default :
          if (ds.Description) {
            stor.zone1.disable()
            stor.liste1.disable()
            if ((stor.liste1.reponse === 'rectangle') || (stor.liste1.reponse === 'isocèle')) {
              stor.zonesomme.disable()
            }
          }
          stor.liste2.disable()
          switch (stor.liste2.reponse) {
            case 'la somme des angles aigus égale 90°.':
              stor.zoneR.disable()
              stor.zoneR2.disable()
              stor.zoneR3.disable()
              break
            case 'les angles à la base sont égaux.':
              stor.zoneI.disable()
              stor.zoneI2.disable()
              break
          }
      }
    } else {
      stor.zoneRep.disable()
    }
  } // desactiveAll

  function passeToutVert () {
    if (ds.Raisonnement) {
      switch (stor.Lexo.prop) {
        case 'Quelconque':
          if (ds.Description) {
            stor.zone1.corrige(true)
            stor.zone2.corrige(true)
          }
          stor.zone3.corrige(true)
          stor.zone4.corrige(true)
          stor.zone5.corrige(true)
          stor.zone6.corrige(true)
          stor.zone7.corrige(true)
          stor.zone8.corrige(true)
          stor.zone9.corrige(true)
          stor.zone10.corrige(true)
          stor.zone11.corrige(true)
          break
        default :
          if (ds.Description) {
            stor.zone1.corrige(true)
            stor.liste1.corrige(true)
            if ((stor.liste1.reponse === 'rectangle') || (stor.liste1.reponse === 'isocèle')) {
              stor.zonesomme.corrige(true)
            }
          }
          stor.liste2.corrige(true)
          switch (stor.liste2.reponse) {
            case 'la somme des angles aigus égale 90°.':
              stor.zoneR.corrige(true)
              stor.zoneR2.corrige(true)
              stor.zoneR3.corrige(true)
              break
            case 'les angles à la base sont égaux.':
              stor.zoneI.corrige(true)
              stor.zoneI2.corrige(true)
              break
          }
      }
    } else {
      stor.zoneRep.corrige(true)
    }
  } // passeToutVert

  function barrelesfo () {
    if (ds.Raisonnement) {
      switch (stor.Lexo.prop) {
        case 'Quelconque':
          if (ds.Description) {
            stor.zone1.barreIfKo()
            stor.zone2.barreIfKo()
          }
          stor.zone3.barreIfKo()
          stor.zone4.barreIfKo()
          stor.zone5.barreIfKo()
          stor.zone6.barreIfKo()
          stor.zone7.barreIfKo()
          stor.zone8.barreIfKo()
          stor.zone9.barreIfKo()
          stor.zone10.barreIfKo()
          stor.zone11.barreIfKo()
          break
        default :
          if (ds.Description) {
            stor.zone1.barreIfKo()
            stor.liste1.barreIfKo()
            if ((stor.liste1.reponse === 'rectangle') || (stor.liste1.reponse === 'isocèle')) {
              stor.zonesomme.barreIfKo()
            }
          }
          stor.liste2.barreIfKo()
          switch (stor.liste2.reponse) {
            case 'la somme des angles aigus égale 90°.':
              stor.zoneR.barreIfKo()
              stor.zoneR2.barreIfKo()
              stor.zoneR3.barreIfKo()
              break
            case 'les angles à la base sont égaux.':
              stor.zoneI.barreIfKo()
              stor.zoneI2.barreIfKo()
              break
          }
      }
    } else {
      stor.zoneRep.barreIfKo()
    }
  }

  function affCorrFaux (isFin) {
    const ff = stor.lafig
    let yaexplik = false
    let yaco = false

    /// //affiche indic
    if (stor.ErrNature) {
      j3pAffiche(stor.lesdiv.explications, null, 'Vérifie bien la nature de ce triangle !\n')
      yaexplik = true
    }
    if (stor.ErrEcritureAngle3) {
      j3pAffiche(stor.lesdiv.explications, null, 'Il faut trois lettres pour nommer un angle !\n')
      yaexplik = true
    }
    if (stor.ErrEcritureAngleMaj) {
      j3pAffiche(stor.lesdiv.explications, null, "Les noms des points s'écrivent en majuscules !\n")
      yaexplik = true
    }
    if (stor.ErrEcritureAngleDouble) {
      j3pAffiche(stor.lesdiv.explications, null, 'Tu utilises deux fois le même point !\n')
      yaexplik = true
    }
    if (stor.ErrEcritureAngleIcon) {
      j3pAffiche(stor.lesdiv.explications, null, 'Tu utilises une lettre qui n’est pas dans la figure !\n')
      yaexplik = true
    }
    if (stor.ErrEcritureAngleChap) {
      j3pAffiche(stor.lesdiv.explications, null, 'Pour nommer un angle, il faut un chapeau pointu !\n')
      yaexplik = true
    }
    if (stor.horstri) {
      j3pAffiche(stor.lesdiv.explications, null, 'Tu dois nommer un élèment du triangle ' + stor.lafig.lettres[0] + stor.lafig.lettres[1] + stor.lafig.lettres[2] + ' !\n')
      yaexplik = true
    }
    if (stor.errPointIcon) {
      j3pAffiche(stor.lesdiv.explications, null, 'Tu utilises une lettre qui ne fait pas partie des données !\n')
      yaexplik = true
    }
    if (stor.errPointMaj || stor.EcrTriMaj) {
      j3pAffiche(stor.lesdiv.explications, null, "Le nom d’un point s'écrit en majuscule !\n")
      yaexplik = true
    }
    if (stor.errPointNb) {
      j3pAffiche(stor.lesdiv.explications, null, 'Une seule lettre pour nommer un point !\n')
      yaexplik = true
    }
    if (stor.ErrAngUnit) {
      j3pAffiche(stor.lesdiv.explications, null, 'La mesure d’un angle s’exprime en degrés !\n')
      yaexplik = true
    }
    if (stor.EcrTri3let) {
      j3pAffiche(stor.lesdiv.explications, null, 'Il faut trois lettres pour nommer un triangle !\n')
      yaexplik = true
    }
    if (stor.EcrTriCroch) {
      j3pAffiche(stor.lesdiv.explications, null, 'Pas de crochets dans le nom d’un triangle !\n')
      yaexplik = true
    }
    if (stor.EcrTriPar) {
      j3pAffiche(stor.lesdiv.explications, null, 'Pas de parenthèses dans le nom d’un triangle !\n')
      yaexplik = true
    }
    if (stor.EcrTriChap) {
      j3pAffiche(stor.lesdiv.explications, null, 'Pas de chapeau dans le nom d’un triangle !\n')
      yaexplik = true
    }
    if (stor.ErrTriNom1) {
      j3pAffiche(stor.lesdiv.explications, null, 'Tu ne travailles pas dans le bon triangle !\n')
      yaexplik = true
    }
    if (stor.ErrTriNom2) {
      j3pAffiche(stor.lesdiv.explications, null, 'Ce triangle n’existe pas !\n')
      yaexplik = true
    }
    if (stor.EcrTriDoub) {
      j3pAffiche(stor.lesdiv.explications, null, 'Tu écris deux fois la même lettre !\n')
      yaexplik = true
    }
    if (stor.ErrMesDeg) {
      j3pAffiche(stor.lesdiv.explications, null, 'Un angle s’exprime en degrés !\n')
      yaexplik = true
    }
    if (stor.ErrMesDegDoub) {
      j3pAffiche(stor.lesdiv.explications, null, 'Tu écris deux fois ° !\n')
      yaexplik = true
    }
    if (stor.ErrMesZero) {
      j3pAffiche(stor.lesdiv.explications, null, 'Attention aux zéros inutiles !\n')
      yaexplik = true
    }
    if (stor.err180) {
      j3pAffiche(stor.lesdiv.explications, null, 'La somme des angles est égale à 180° !\n')
      yaexplik = true
    }
    if (stor.errerrWrongAngl) {
      j3pAffiche(stor.lesdiv.explications, null, 'Tu ne peux pas utiliser la mesure de l’angle que tu veux calculer !\n')
      yaexplik = true
    }
    if (stor.errerrWrongAnglMM) {
      j3pAffiche(stor.lesdiv.explications, null, 'Tu utilises deux fois le même angle !\n')
      yaexplik = true
    }
    if (stor.errWrongVal) {
      j3pAffiche(stor.lesdiv.explications, null, 'Cette mesure ne concerne pas notre triangle !\n')
      yaexplik = true
    }
    if (stor.errWrongValM) {
      j3pAffiche(stor.lesdiv.explications, null, 'Tu utilises deux fois la même mesure !\n')
      yaexplik = true
    }
    if (stor.errWrongCal) {
      j3pAffiche(stor.lesdiv.explications, null, 'Il y a une erreur dans ce calcul !\n')
      yaexplik = true
    }
    if (stor.errWrongRep) {
      j3pAffiche(stor.lesdiv.explications, null, 'La réponse est fausse !\n')
      yaexplik = true
    }
    if (stor.ErrSom) {
      if (stor.Lexo.prop === 'Rectangle') {
        j3pAffiche(stor.lesdiv.explications, null, 'Vérifie bien le sommet de l’angle droit !\n')
      } else {
        j3pAffiche(stor.lesdiv.explications, null, 'Vérifie bien le sommet principal !\n')
      }
      yaexplik = true
    }
    if (stor.errWrongAnglBase) {
      j3pAffiche(stor.lesdiv.explications, null, 'Tu dois utiliser un angle à la base !\n')
      yaexplik = true
    }
    if (stor.ErrProp) {
      j3pAffiche(stor.lesdiv.explications, null, 'la propriété utilisée est fausse pour ce triangle !\n')
      yaexplik = true
    }

    if (isFin) {
      // barre les faux
      yaco = true
      // afficheco
      switch (stor.Lexo.prop) {
        case 'Quelconque': {
          j3pAffiche(stor.lesdiv.solution, null, 'Dans le triangle ' + ff.nomtriangle + ' ,\n')
          j3pAffiche(stor.lesdiv.solution, null, 'la somme des angles égale $180°$ .\n')
          const tt = addDefaultTable(stor.lesdiv.solution, 4, 2)
          modif(tt)
          j3pAffiche(tt[0][0], null, 'Donc&nbsp;')
          j3pAffiche(tt[0][1], null, '$\\widehat{' + ff.nomangle + '} = 180 -  \\widehat{' + ff.nomautreangle1 + '} - \\widehat{' + ff.nomautreangle2 + '}$')
          j3pAffiche(tt[1][1], null, '$\\widehat{' + ff.nomangle + '}= 180 - ' + ff.val1 + ' - ' + ff.val2 + ' $ \n')
          j3pAffiche(tt[2][1], null, '$\\widehat{' + ff.nomangle + '} = 180 - ' + (ff.val1 + ff.val2) + ' $ \n')
          j3pAffiche(tt[3][1], null, '$\\widehat{' + ff.nomangle + '} = ' + ff.lam + '° $ \n')
        }
          break
        case 'Rectangle': {
          j3pAffiche(stor.lesdiv.solution, null, 'Dans le triangle ' + ff.nomtriangle + ' rectangle en ' + ff.nomsom + ' ,\n')
          j3pAffiche(stor.lesdiv.solution, null, 'la somme des angles aigus égale $90°$ .\n')
          const tt = addDefaultTable(stor.lesdiv.solution, 3, 2)
          modif(tt)
          j3pAffiche(tt[0][0], null, 'Donc&nbsp;')
          j3pAffiche(tt[0][1], null, '$\\widehat{' + ff.nomangle + '}= 90 - \\widehat{' + ff.nomautreangle1 + '}$ \n')
          j3pAffiche(tt[1][1], null, '$\\widehat{' + ff.nomangle + '}= 90 - ' + ff.val1 + ' $ \n')
          j3pAffiche(tt[2][1], null, '$\\widehat{' + ff.nomangle + '}= ' + ff.lam + '° $ \n')
        }
          break
        case 'Equilateral':
          j3pAffiche(stor.lesdiv.solution, null, 'Dans le triangle ' + ff.nomtriangle + ' équilatéral ,\n')
          j3pAffiche(stor.lesdiv.solution, null, 'les angles mesurent $60°$ .\n')
          j3pAffiche(stor.lesdiv.solution, null, 'Donc $\\widehat{' + ff.nomangle + '}= 60° $ \n')
          break
        case 'Isocele':
          if ((ds.Raisonnement) || (ff.numc === 0) || (ff.numc === 3)) {
            j3pAffiche(stor.lesdiv.solution, null, 'Dans le triangle ' + ff.nomtriangle + ' isocèle en ' + ff.nomsom + ' ,\n')
            j3pAffiche(stor.lesdiv.solution, null, 'les angles à la base sont égaux .\n')
            j3pAffiche(stor.lesdiv.solution, null, 'Donc $\\widehat{' + ff.nomangle + '} = \\widehat{' + ff.nomautreangle1 + '}$ \n')
            stor.lesdiv.solution.innerHTML += '&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp'
            j3pAffiche(stor.lesdiv.solution, null, '$\\widehat{' + ff.nomangle + '}= ' + ff.lam + '° $ \n')
          } else if ((ff.numc === 1) || (ff.numc === 4)) {
            j3pAffiche(stor.lesdiv.solution, null, 'En utilisant la somme des angles \ndans le triangle ' + ff.nomtriangle + ' ,\n')
            j3pAffiche(stor.lesdiv.solution, null, 'je trouve $\\widehat{' + ff.nomangle + '} + \\widehat{' + ff.nomautreangle2 + '} = 180 - ' + ff.val1 + ' = ' + (180 - ff.val1) + '° $ .\n\n')
            j3pAffiche(stor.lesdiv.solution, null, 'Comme le triangle ' + ff.nomtriangle + ' est isocèle en  ' + ff.nomsom + ', \n')
            j3pAffiche(stor.lesdiv.solution, null, '$\\widehat{' + ff.nomangle + '} = \\widehat{' + ff.nomautreangle1 + '}$\n')
            j3pAffiche(stor.lesdiv.solution, null, 'Donc $\\widehat{' + ff.nomangle + '} = \\frac{' + (180 - ff.val1) + '}{2} = ' + ff.lam + '° $  \n')
          } else {
            j3pAffiche(stor.lesdiv.solution, null, 'Dans le triangle ' + ff.nomtriangle + ' isocèle en ' + ff.nomsom + ' ,\n')
            j3pAffiche(stor.lesdiv.solution, null, 'les angles à la base sont égaux .\n')
            j3pAffiche(stor.lesdiv.solution, null, 'Donc $\\widehat{' + ff.nomautreangle2 + '} = \\widehat{' + ff.nomautreangle1 + '} = ' + ff.val1 + '°$  \n\n')
            j3pAffiche(stor.lesdiv.solution, null, 'En utilisant la somme des angles \ndans le triangle ' + ff.nomtriangle + ' ,\n')
            j3pAffiche(stor.lesdiv.solution, null, 'je trouve  $\\widehat{' + ff.nomangle + '} = 180 - 2 \\times ' + ff.val1 + ' = ' + ff.lam + '° $ .')
          }
          break
      }
      barrelesfo()
      desactiveAll()
    }

    if (yaexplik) {
      stor.lesdiv.explications.classList.add('explique')
    }
    if (yaco) {
      stor.lesdiv.solution.classList.add('correction')
    }
  } // affCorrFaux

  switch (this.etat) {
    case 'enonce':
      // code exécuté au lancement de la section
      if (me.debutDeLaSection) {
        initSection()
      }

      if (me.etapeCourante === 1) {
        // A chaque répétition, on nettoie la scène
        me.videLesZones()
      }
      creeLesDiv()
      stor.Lexo = faisChoixExos()
      stor.lesdiv.travail = stor.lesdiv.zone2
      stor.lesdiv.figure = stor.lesdiv.zone1
      if (stor.Lexo.type === 'Figure') {
        stor.lesdiv.solution = stor.lesdiv.zone3
        stor.lesdiv.correction = stor.lesdiv.zone5
        stor.lesdiv.explications = stor.lesdiv.zone4
      } else {
        stor.lesdiv.travail = stor.lesdiv.zone2
        stor.lesdiv.figure = stor.lesdiv.zone1
        stor.lesdiv.solution = stor.lesdiv.zone5
        stor.lesdiv.correction = stor.lesdiv.zone3
        stor.lesdiv.explications = stor.lesdiv.zone4
      }
      stor.lesdiv.solution.style.color = me.styles.toutpetit.correction.color
      stor.lesdiv.explications.style.color = me.styles.cfaux

      stor.lesdiv.travail.classList.add('travail')
      stor.lesdiv.consigne.classList.add('enonce')
      stor.lesdiv.figure.classList.add('enonce')
      if (!ds.MiseEnPagePlusClaire && stor.Lexo.type === 'Texte') {
        stor.lesdiv.travail.style.border = '1px solid'
      }

      stor.lafig = new Figure()
      poseQuestion()
      this.finEnonce()
      break // case "enonce":

    case 'correction':
      // On teste si une réponse a été saisie
      stor.lesdiv.explications.innerHTML = ''
      stor.lesdiv.solution.innerHTML = ''
      stor.lesdiv.explications.classList.remove('explique')

      if (yaReponse()) {
        // Bonne réponse
        if (isRepOk()) {
          this._stopTimer()
          this.score++
          stor.lesdiv.correction.style.color = this.styles.cbien
          stor.lesdiv.correction.innerHTML = cBien

          passeToutVert.call(this)
          desactiveAll.call(this)
          this.raz = false

          this.typederreurs[0]++
          this.etat = 'navigation'
          this.sectionCourante()
        } else {
          // Pas de bonne réponse
          stor.lesdiv.correction.style.color = this.styles.cfaux
          stor.lesdiv.correction.innerHTML = cFaux
          if (this.isElapsed) {
            // A cause de la limite de temps :
            this._stopTimer()

            stor.lesdiv.correction.innerHTML += tempsDepasse + '<BR>'
            this.typederreurs[10]++
            affCorrFaux(true)
            this.etat = 'navigation'
            this.sectionCourante()
          } else {
            // Réponse fausse :
            if (me.essaiCourant < me.donneesSection.nbchances) {
              stor.lesdiv.correction.innerHTML += '<br>' + essaieEncore
              affCorrFaux(false)
              this.typederreurs[1]++
            } else {
              // Erreur au nème essai
              affCorrFaux(true)
              this.typederreurs[2]++
              this.etat = 'navigation'
              this.sectionCourante()
            }
          }
        }
      } else {
        // pas de réponse
        stor.lesdiv.correction.style.color = this.styles.cfaux
        stor.lesdiv.correction.innerHTML = reponseIncomplete
      }

      this.finCorrection()
      break // case "correction":

    case 'navigation':
      if (!this.sectionTerminee()) {
        this.etat = 'enonce'
      }
      this.finNavigation()
      break // case "navigation":
  }
}
