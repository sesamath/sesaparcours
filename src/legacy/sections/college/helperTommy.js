import { j3pNotify } from 'src/legacy/core/functions'

/**
 * Initialise les ds.storage.compteurPeX à 0 d'après les pe déclarées
 * @param {LegacyDonneesSection} ds
 * @param {Object} storage
 */
export function initCptPe (ds, storage) {
  try {
    let numPe = 1
    while (ds[`pe_${numPe}`]) {
      storage[`compteurPe${numPe}`] = 0
      numPe++
    }
  } catch (error) {
    j3pNotify(Error('Erreur initCptPe'), { ds })
  }
}

/**
 * Retourne la pe pour les sections de Tommy où on a un donneesSection.storage.compteurPeN qui est incrémenté lors des corrections, c'est le plus élevé qui détermine la pe.
 * @param {LegacyDonneesSection} ds
 * @param {number} [seuilMin=0] Un seuil minimal éventuel pour affecter la pe
 * @returns {string|void} la pe à affecter (undefined si y'a eu un pb pour la trouver)
 */
export function getPe (ds, storage, seuilMin = 0) {
  let pe
  let numPe = 1
  if (!ds[`pe_${numPe}`]) {
    return j3pNotify(Error('Il n’y a pas de pe_1 dans donneesSection'))
  }
  // on prend le compteurPeN le plus élevé pour choisir la pe qui sera affectée
  let nb = 0
  try {
    while (ds[`pe_${numPe}`]) {
      if (!Number.isInteger(storage[`compteurPe${numPe}`])) {
        // ça c'est une section avec des pe dont CompteurPe n'a pas été initialisé
        return j3pNotify(Error(`compteurPe${numPe} n’est pas un entier`))
      }
      if (storage[`compteurPe${numPe}`] > nb) {
        nb = storage[`compteurPe${numPe}`]
        if (nb >= seuilMin) {
          pe = ds[`pe_${numPe}`]
        }
      }
      numPe++
    }
    // y'a plus de ds[`pe_${numPe}`], mais si on trouve un storage[`compteurPe${numPe}`]
    // on râle, car c'est incohérent
    if (storage[`compteurPe${numPe}`] != null) {
      j3pNotify(Error(`On a pas de ds.pe_${numPe} mais storage.compteurPe${numPe} existe`), { ds })
    }
  } catch (error) {
    j3pNotify(error, { ds, storage, seuilMin })
  }
  return pe
}
