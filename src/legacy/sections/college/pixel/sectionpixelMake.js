import { j3pAddContent, j3pAddElt, j3pAjouteBouton, j3pArrondi, j3pClone, j3pGetNewId, j3pGetRandomInt, j3pShowError, j3pShuffle } from 'src/legacy/core/functions'
import { addDefaultTable } from 'src/legacy/themes/table'
import { getMtgCore } from 'src/lib/outils/mathgraph'
import ZoneColoriage from 'src/legacy/outils/zoneColoriage/ZoneColoriage'
import getDefaultProgramme from './defaultProgramme'
import { j3pCreeSVG } from 'src/legacy/core/functionsSvg'
import { j3pAfficheCroixFenetres, j3pCreeFenetres, j3pToggleFenetres } from 'src/legacy/core/functionsJqDialog'
import BulleAide from 'src/legacy/outils/bulleAide/BulleAide'
import textesGeneriques from 'src/lib/core/textes'

const { cBien, cFaux, essaieEncore, reponseIncomplete, tempsDepasse } = textesGeneriques

async function initEditor (container, values, btn) {
  const { default: editor } = await import('./editorProgramme.js')
  return editor(container, values, btn) // la fct qui prend un conteneur et les params comme arguments
}

/**
 * Ajouter une description ici
 * @fileOverview
 * @author Tommy Barroy <t.barroy@laposte.net>
 * @since 2017-10
 */
export const params = {
  outils: ['calculatrice', 'mathquill'],
  /**
   * Ce qui est paramétrable dans l’éditeur de graphe
   * @type {LegacySectionParamsParametres}
   */
  parametres: [
    ['limite', 0, 'entier', 'Temps maximum pour choisir (sinon ce sera le premier choix, mettre 0 pour ne pas limiter)'],
    ['Legendes', JSON.stringify(getDefaultProgramme()), 'multiEditor', 'Json figure de base et figure attendue.', initEditor],
    ['Calculatrice', 'Non', 'liste', '<u>true</u>:  Une calculatrice est disponible', ['Oui', 'Non']],
    ['theme', 'standard', 'liste', 'Modèle pour l’affichage', ['standard', 'zonesAvecImageDeFond']]
  ]
}

/**
 * section choix
 * @this {Parcours}
 */
export default function main () {
  const me = this
  const ds = me.donneesSection
  const stor = me.storage

  function initSection () {
    // chargement mtg
    me.construitStructurePage('presentation1bis')
    try {
      stor.Legendes = JSON.parse(ds.Legendes)
    } catch (error) {
      console.error(error)
      j3pShowError('Cartes mises en paramètre invalide')
      stor.Legendes = getDefaultProgramme()
    }
    let titre = stor.Legendes.titre
    if (!titre || titre === '') titre = ' '
    me.afficheTitre('Pixel Art: ' + titre)
    getMtgCore({ withMathJax: true })
      .then(
        // success
        (mtgAppLecteur) => {
          stor.mtgAppLecteur = mtgAppLecteur
          enonceMain()
        },
        // failure
        (error) => {
          j3pShowError(error, { message: 'mathgraph n’est pas correctement chargé', mustNotify: true })
        })
    // plantage dans le code de success
      .catch(j3pShowError)
  }
  function boutonfen () {
    j3pToggleFenetres('Calculatrice')
  }
  function creeLesDiv () {
    stor.lesdiv = {}
    stor.lesdiv.conteneur = j3pAddElt(me.zonesElts.MG, 'div', {
      style: me.styles.etendre('toutpetit.enonce', { padding: '10px' })
    })
    stor.lesdiv.calculatrice = j3pAddElt(me.zonesElts.MD, 'div', {
      style: me.styles.etendre('toutpetit.enonce', { padding: '10px' })
    })
    stor.lesdiv.enonce = addDefaultTable(stor.lesdiv.conteneur, 1, 1)[0][0]
    const tab1 = addDefaultTable(stor.lesdiv.conteneur, 1, 3)
    stor.lesdiv.enonce = tab1[0][0]
    stor.lesdiv.divAide = tab1[0][2]
    tab1[0][1].style.width = '10px'
    stor.lesdiv.travail = addDefaultTable(stor.lesdiv.conteneur, 1, 1)[0][0]
    stor.lesdiv.explications = j3pAddElt(stor.lesdiv.conteneur, 'div')
    stor.lesdiv.correction = j3pAddElt(me.zonesElts.MD, 'div', {
      style: me.styles.etendre('toutpetit.enonce', { padding: '10px' })
    })
    stor.lesdiv.explications.style.color = me.styles.cfaux
    stor.divmatgraA = addDefaultTable(stor.lesdiv.conteneur, 1, 1)[0][0]
    stor.divmatgraB = addDefaultTable(stor.lesdiv.conteneur, 1, 1)[0][0]
    stor.divmatgraA.style.display = 'none'
    stor.divmatgraB.style.display = 'none'
    stor.lesdiv.enonce.classList.add('enonce')
    stor.lesdiv.travail.classList.add('travail')
    me.zonesElts.MG.classList.add('fond')
  }

  function enonceMain () {
    me.videLesZones()
    creeLesDiv()
    if (ds.Calculatrice === 'Oui') {
      me.fenetresjq = [{ name: 'Calculatrice', title: 'Une calculatrice', left: 600, top: 10 }]
      j3pCreeFenetres(me)
      j3pAfficheCroixFenetres('Calculatrice')
      j3pAjouteBouton(stor.lesdiv.calculatrice, boutonfen, { className: 'MepBoutons', value: 'Calculatrice' })
    }
    j3pAddContent(stor.lesdiv.enonce, '<b>Colorie ce carré en fonction de son contenu</b')
    stor.bulaide = new BulleAide(stor.lesdiv.divAide, 'Clique sur une petite case pour voir son contenu,<br> et modifier sa couleur.')
    const Legendes = []
    let gg = []
    if (stor.Legendes.liste.parGen !== 'expl') {
      stor.mtgAppLecteur.removeAllDoc()
      if (stor.Legendes.liste.parGenMod === 'genC') {
        stor.svgIdA = j3pGetNewId('mtg')
        j3pCreeSVG(stor.divmatgraA, { id: stor.svgIdA, width: 0, height: 0 })
        stor.mtgAppLecteur.addDoc(stor.svgIdA, stor.Legendes.liste.GenMod, true)
        stor.mtgAppLecteur.calculate(stor.svgIdA, true)
      }
      if (stor.Legendes.liste.parGenCont === 'genC') {
        stor.svgIdB = j3pGetNewId('mtg')
        j3pCreeSVG(stor.divmatgraB, { id: stor.svgIdB, width: 0, height: 0 })
        stor.mtgAppLecteur.addDoc(stor.svgIdB, stor.Legendes.liste.genCont, true)
        stor.mtgAppLecteur.calculate(stor.svgIdB, true)
      }
      const fM = (stor.Legendes.liste.parGenMod === 'genC') ? getImMod : getImModM
      const fAs = (stor.Legendes.liste.parGenCont === 'genC') ? getImCont : getImContMG
      const i1 = stor.Legendes.liste.minA || 1
      const i2 = stor.Legendes.liste.maxA || 10
      for (let i = i1; i < i2 + 1; i++) {
        gg.push({ mod: fM(i), func: fAs(i) })
      }
    } else {
      stor.Legendes.liste.listeMod.forEach(el => {
        let lemod
        if (typeof el.mod === 'string') {
          lemod = stor.Legendes.liste.phrasemodel.replace(/£a/g, el.mod)
        } else {
          lemod = j3pClone(el.mod)
        }
        gg.push({ mod: lemod, func: makeFuncCont(el.contAs) })
      })
    }
    gg = j3pShuffle(gg)
    for (let i = 0; i < stor.Legendes.liste.nbModel; i++) Legendes.push(gg.pop())
    stor.pixel = new ZoneColoriage({ conteneur: stor.lesdiv.travail, classes: Legendes, dim: Number(stor.Legendes.difficulte) })
    me.finEnonce()
  }

  function makeFuncCont (tab) {
    return () => {
      return tab[j3pGetRandomInt(0, tab.length - 1)]
    }
  }

  function getImMod (i) {
    stor.mtgAppLecteur.giveFormula2(stor.svgIdA, 'A', i)
    stor.mtgAppLecteur.calculate(stor.svgIdA, true)
    return stor.Legendes.liste.phrasemodel.replace(/£a/g, '$' + stor.mtgAppLecteur.getLatexCode(stor.svgIdA, '#formule') + '$')
  }
  function getImModM (i) {
    return { type: 'mathgraph', content: { A: i, width: stor.Legendes.liste.modDimx, height: stor.Legendes.liste.modDimy, txtFigure: stor.Legendes.liste.GenMod } }
  }

  function getImCont (i) {
    stor.mtgAppLecteur.giveFormula2(stor.svgIdB, 'A', i)
    const tab = []
    for (let j = 1; j < 6; j++) {
      stor.mtgAppLecteur.giveFormula2(stor.svgIdB, 'ALEA', j)
      stor.mtgAppLecteur.calculate(stor.svgIdB, true)
      tab.push('$' + stor.mtgAppLecteur.getLatexCode(stor.svgIdB, '#formule') + '$')
    }
    return makeFuncCont(tab)
  }
  function getImContMG (i) {
    const tab = []
    for (let j = 1; j < 6; j++) {
      tab.push({ type: 'mathgraph', content: { A: i, ALEA: j, width: stor.Legendes.liste.contDimx, height: stor.Legendes.liste.contDimy, txtFigure: stor.Legendes.liste.genCont } })
    }
    return makeFuncCont(tab)
  }

  function isRepOk () {
    return stor.pixel.isOk()
  }

  function yaReponse () {
    if (me.isElapsed) return true
    return stor.pixel.yareponse()
  }

  switch (this.etat) {
    case 'enonce':
      // code exécuté au lancement de la section
      if (me.debutDeLaSection) {
        initSection()
      } else {
        enonceMain()
      }
      break // case "enonce":

    case 'correction':
      // On teste si une réponse a été saisie
      if (!yaReponse()) {
        // pas de réponse
        stor.lesdiv.correction.style.color = me.styles.cfaux
        stor.lesdiv.correction.innerHTML = reponseIncomplete
        return this.finCorrection()
      }

      // Bonne réponse
      if (isRepOk()) {
        stor.lesdiv.correction.style.color = me.styles.cbien
        stor.lesdiv.correction.innerHTML = cBien

        stor.pixel.disable()
        stor.pixel.corrige()

        me.score = j3pArrondi(me.score + 1, 1)
        me.typederreurs[0]++
        return this.finCorrection('navigation', true)
      }

      // Pas de bonne réponse
      stor.lesdiv.correction.style.color = me.styles.cfaux
      stor.lesdiv.correction.innerHTML = cFaux
      if (me.isElapsed) {
        // A cause de la limite de temps :
        stor.lesdiv.correction.innerHTML += '<br>' + tempsDepasse + '<BR>'
        me.typederreurs[10]++
        stor.pixel.corrige(false)
        stor.pixel.disable()
        me.score = j3pArrondi(me.score + stor.pixel.getScore(), 1)

        return this.finCorrection('navigation', true)
      }

      // Réponse fausse :
      if (me.essaiCourant < me.donneesSection.nbchances) {
        stor.lesdiv.correction.innerHTML += '<br>' + essaieEncore
        stor.pixel.corrige()
        me.typederreurs[1]++
        return this.finCorrection()
      }

      // Erreur au dernier essai
      stor.pixel.corrige(false)
      stor.pixel.disable()
      me.score = j3pArrondi(me.score + stor.pixel.getScore(), 1)

      me.typederreurs[2]++
      return this.finCorrection('navigation', true)
      // case "correction":

    case 'navigation':
      if (!me.sectionTerminee()) {
        me.etat = 'enonce'
      }
      me.finNavigation()
      break // case "navigation":
  }
}
