export default function getDefaultProgramme () {
  return {
    titre: 'Fractions',
    difficulte: 4,
    liste: {
      // nb de modèles
      nbModel: 2,
      parGen: 'expl',
      // 'expl': en dur
      // 'genC': fonction chaine
      // 'genM': fonction figure
      // une fonction qui génére les modeles possibles ( mgfig )
      // la liste des modèles possibles en dur
      listeMod: [
        {
          // mod est un string ou { type: 'mathgraph', cntent: { txtFigure } }
          mod: '$\\frac{2}{3}$',
          contAs: ['$\\frac{4}{6}$', '$\\frac{1}{3} + \\frac{1}{3} $', '$1 - \\frac{2}{6} $']
        },
        {
          mod: '$\\frac{1}{2}$',
          contAs: ['$0,5$', '$\\frac{3}{6}$', '$2 \\times \\frac{1}{4}$']
        },
        {
          mod: '$\\frac{4}{3}$',
          contAs: ['$\\frac{8}{6}$', '$\\frac{2}{3} + \\frac{2}{3}$', '$\\frac{5}{3} - \\frac{1}{3}$']
        }

      ],
      // la phrase dans légende ( £a remplace modèle généré )
      phrasemodel: 'Un nombre égal à £a'
      // ghfjk
      // une fonction qui génére les contenus possibles ( mgfig )
    }
  }
}
