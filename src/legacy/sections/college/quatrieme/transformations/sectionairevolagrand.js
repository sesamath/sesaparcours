import { j3pAddElt, j3pAjouteBouton, j3pArrondi, j3pEmpty, j3pGetRandomBool, j3pGetRandomElt, j3pGetRandomInt, j3pPGCD, j3pShowError, j3pShuffle } from 'src/legacy/core/functions'
import { j3pAfficheCroixFenetres, j3pCreeFenetres, j3pToggleFenetres } from 'src/legacy/core/functionsJqDialog'
import ListeDeroulante from 'src/legacy/outils/listeDeroulante/ListeDeroulante'
import { affichefois, ecrisBienMathquill } from 'src/legacy/outils/zoneStyleMathquill/functions'
import { verifNombreBienEcrit } from 'src/legacy/outils/fonctions/ecritureNombre'
import ZoneStyleMathquill2 from 'src/legacy/outils/zoneStyleMathquill/ZoneStyleMathquill2'
import { addDefaultTable } from 'src/legacy/themes/table'
import { j3pAffiche } from 'src/lib/mathquill/functions'
import textesGeneriques from 'src/lib/core/textes'

const { cBien, cFaux, essaieEncore, reponseIncomplete, tempsDepasse } = textesGeneriques

/**
 * Ajouter une description ici
 * @fileOverview
 * @author Tommy Barroy <t.barroy@laposte.net>
 * @since 2017-10
*/
export const params = {
  outils: ['mathquill', 'calculatrice'],
  /**
   * Ce qui est paramétrable dans l’éditeur de graphe
   * @type {LegacySectionParamsParametres}
   */
  parametres: [
    ['nbrepetitions', 9, 'entier', 'Nombre de répétitions de la section'],
    ['nbchances', 1, 'entier', 'Nombre d’essais possibles.'],
    ['longueur', true, 'boolean', '<u>true</u>:  L’exercice peut concerner un   de longueur'],
    ['aire', true, 'boolean', '<u>true</u>:  L’exercice peut concerner un calcul de d’aire'],
    ['volume', true, 'boolean', '<u>true</u>:  L’exercice peut concerner un calcul de volume'],
    ['entiers', true, 'boolean', '<u>true</u>:  Les nombres peuvent être entiers'],
    ['decimaux', true, 'boolean', '<u>true</u>:  Les nombres peuvent être décimaux'],
    ['fractions', true, 'boolean', '<u>true</u>:  Les nombres peuvent être fractionnaires'],
    ['justifie', true, 'boolean', '<u>true</u>:  La formule est demandée'],
    ['Calculatrice', true, 'boolean', '<u>true</u>:  Une calculatrice est disponible'],
    ['theme', 'standard', 'liste', 'Modèle pour l’affichage', ['standard', 'zonesAvecImageDeFond']]
  ]
}

/**
 * section airevolagrand
 * @this {Parcours}
 */
export default function main () {
  const me = this
  const stor = this.storage
  const ds = me.donneesSection

  function creeLesDiv () {
    stor.lesdiv = {}
    stor.lesdiv.conteneur = j3pAddElt(me.zonesElts.MG, 'div', {
      style: me.styles.etendre('toutpetit.enonce', { padding: '10px' })
    })
    stor.lesdiv.consigneG = addDefaultTable(stor.lesdiv.conteneur, 1, 1)[0][0]
    modif(stor.lesdiv.consigneG)
    stor.lesdiv.travail = addDefaultTable(stor.lesdiv.conteneur, 1, 1)[0][0]
    modif(stor.lesdiv.travail)
    const yy = addDefaultTable(stor.lesdiv.travail, 2, 1)
    modif(yy)
    stor.lesdiv.justifie = yy[0][0]
    stor.lesdiv.calcul = yy[1][0]
    stor.lesdiv.explications = j3pAddElt(stor.lesdiv.conteneur, 'div')
    stor.lesdiv.solution = j3pAddElt(stor.lesdiv.conteneur, 'div')
    stor.lesdiv.calculatrice = j3pAddElt(me.zonesElts.MD, 'div', {
      style: me.styles.etendre('toutpetit.enonce', { padding: '10px' })
    })
    stor.lesdiv.correction = j3pAddElt(me.zonesElts.MD, 'div', {
      style: me.styles.etendre('toutpetit.enonce', { padding: '10px' })
    })
    stor.lesdiv.solution.style.color = me.styles.toutpetit.correction.color
    stor.lesdiv.explications.style.color = me.styles.cfaux
  }

  function boutonfen () {
    j3pToggleFenetres('Calculatrice')
  }

  function initSection () {
    if (ds.justifie) me.surcharge({ nbetapes: 2 }) // sinon on laisse le 1 mis par défaut
    me.construitStructurePage('presentation1bis')
    stor.exos = []

    const grandeursPossibles = []
    if (ds.longueur) grandeursPossibles.push('longueur')
    if (ds.aire) grandeursPossibles.push('aire')
    if (ds.volume) grandeursPossibles.push('volume')
    if (grandeursPossibles.length === 0) {
      j3pShowError('Paramètrage invalide (aucune grandeur donnée)')
      grandeursPossibles.push('longueur')
    }
    for (let i = 0; i < ds.nbitems; i++) {
      stor.exos.push({ quest: j3pGetRandomElt(grandeursPossibles) })
    }
    stor.exos = j3pShuffle(stor.exos)

    const typesNb = []
    if (ds.entiers) typesNb.push('entiers')
    // décimaux ou fractions mais pas les deux ensemble
    if (ds.decimaux) typesNb.push('decimaux')
    else if (ds.fractions) typesNb.push('fractions')
    if (typesNb.length === 0) {
      j3pShowError('Paramètrage invalide (aucun type de nombre)')
      typesNb.push('entiers')
    }
    for (const exo of stor.exos) {
      exo.typeNb = j3pGetRandomElt(typesNb)
      exo.isAgrandissement = j3pGetRandomBool()
    }

    stor.afaire = ['calcul']
    if (ds.justifie) stor.afaire.splice(0, 0, 'justifie')
    stor.encours = 'calcul'
    me.typederreurs = [0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0]
    me.afficheTitre('Agrandissement/Réduction: calcul de grandeurs')
    enonceMain()
  }

  function faisChoixExos () {
    const e = stor.exos.pop()
    const grandeurs = [
      { s: true, n1: 'solide (S)', n2: "solide (S')", n1s: '(S)', n2s: "(S')" },
      { s: false, n1: 'figure (F)', n2: "figure (F')", n1s: '(F)', n2s: "(F')" },
      { s: true, n1: 'segment [AB]', n2: "segment [A'B']", n1s: 'AB', n2s: "A'B'" }
    ]
    const expoUnite = e.quest === 'longueur'
      ? 1
      : e.quest === 'aire'
        ? 2
        : 3
    switch (e.quest) {
      case 'longueur': {
        const ch = j3pGetRandomInt(0, 2)
        stor.grandeur = grandeurs[ch]
        stor.deQuoi = (ch === 2) ? 'longueur' : 'périmètre'
        stor.deQuoiDe = (ch === 2) ? 'de longueur' : ' de périmètre'
        stor.deQuoiDe2 = (ch === 2) ? 'Longueur ' : 'Périmètre de '
      }
        break
      case 'aire': {
        const ch = j3pGetRandomInt(0, 1)
        stor.grandeur = grandeurs[ch]
        stor.deQuoi = 'aire'
        stor.deQuoiDe = 'd’aire'
        stor.deQuoiDe2 = 'Aire de '
      }
        break
      default:
        stor.grandeur = grandeurs[0]
        stor.deQuoi = 'volume'
        stor.deQuoiDe = 'de volume'
        stor.deQuoiDe2 = 'Volume de '
    }
    switch (e.typeNb) {
      case 'entiers':
        stor.valeurInitiale = j3pGetRandomInt(10, 23)
        stor.maxNumCoeff = e.quest === 'longueur'
          ? 23
          : e.quest === 'aire'
            ? 12
            : 5
        stor.numCoefficient = e.isAgrandissement ? j3pGetRandomInt(2, stor.maxNumCoeff) : 1
        stor.denCoefficient = !e.isAgrandissement ? j3pGetRandomInt(2, stor.maxNumCoeff) : 1
        stor.numCoeffExponentiel = e.isAgrandissement ? Math.pow(stor.numCoefficient, expoUnite) : 1
        stor.denCoeffExponentiel = e.isAgrandissement ? 1 : Math.pow(stor.denCoefficient, expoUnite)
        stor.numValeurProduit = (e.isAgrandissement ? stor.numCoeffExponentiel : stor.denCoeffExponentiel) * stor.valeurInitiale
        stor.denValeurProduit = 1
        break
      case 'decimaux':
        stor.valeurInitiale = e.isAgrandissement ? j3pArrondi(j3pGetRandomInt(1, 40) / 10, 1) : j3pGetRandomInt(1, 40)
        stor.numCoefficient = e.isAgrandissement ? j3pArrondi(j3pGetRandomInt(15, 35) / 10, 1) : j3pArrondi(j3pGetRandomInt(1, 99) / 100, 1)
        stor.denCoefficient = 1
        stor.numCoeffExponentiel = j3pArrondi(Math.pow(stor.numCoefficient, expoUnite), expoUnite)
        stor.denCoeffExponentiel = 1
        stor.numValeurProduit = j3pArrondi(stor.numCoeffExponentiel * stor.valeurInitiale, expoUnite + 1)
        stor.denValeurProduit = 1
        break
      case 'fractions':
        stor.valeurInitiale = j3pGetRandomInt(1, 23)
        stor.maxNumCoeff = e.quest === 'longueur' ? 23 : e.quest === 'aire' ? 12 : 11
        stor.numCoefficient = j3pGetRandomInt(6, stor.maxNumCoeff)
        stor.denCoefficient = premsAvec(stor.numCoefficient)
        stor.numCoeffExponentiel = Math.pow(stor.numCoefficient, expoUnite)
        stor.denCoeffExponentiel = Math.pow(stor.denCoefficient, expoUnite)
        stor.numValeurProduit = stor.numCoeffExponentiel * stor.valeurInitiale
        stor.denValeurProduit = stor.denCoeffExponentiel
        break
      default: throw Error('Type de coefficient inconnu')
    }
    stor.encours = stor.afaire[0]
    if (e.isAgrandissement) {
      stor.numValeurInitiale = stor.valeurInitiale
      stor.denValeurInitiale = 1
      stor.numValeurFinale = stor.numValeurProduit
      stor.denValeurFinale = stor.denValeurProduit
      switch (e.typeNb) {
        case 'entiers':
        case 'decimaux':
          stor.coeffString = ecrisBienMathquill(stor.numCoefficient)
          stor.coeffCarreString = ecrisBienMathquill(stor.numCoefficient) + '²'
          stor.coeffCubeString = ecrisBienMathquill(stor.numCoefficient) + '³'
          break
        case 'fractions':
          stor.coeffString = '\\frac{' + ecrisBienMathquill(stor.numCoefficient) + '}{' + ecrisBienMathquill(stor.denCoefficient) + '}'
          stor.coeffCarreString = '(\\frac{' + ecrisBienMathquill(stor.numCoefficient) + '}{' + ecrisBienMathquill(stor.denCoefficient) + '})²'
          stor.coeffCubeString = '(\\frac{' + ecrisBienMathquill(stor.numCoefficient) + '}{' + ecrisBienMathquill(stor.denCoefficient) + '})³'
          break
        default: throw Error('Type de coefficient inconnu')
      }
    } else {
      switch (e.typeNb) {
        case 'entiers':
          // On inverse valeur initiale et valeur finale
          stor.numValeurInitiale = stor.numValeurProduit
          stor.denValeurInitiale = stor.denValeurProduit
          stor.numValeurFinale = stor.valeurInitiale
          stor.denValeurFinale = 1
          stor.coeffString = '\\frac{1}{' + ecrisBienMathquill(stor.denCoefficient) + '}'
          stor.coeffCarreString = '\\frac{1}{' + ecrisBienMathquill(stor.denCoefficient) + '²}'
          stor.coeffCubeString = '\\frac{1}{' + ecrisBienMathquill(stor.denCoefficient) + '³}'
          break
        case 'decimaux':
          // on n’inverse pas finale et initiale car le coefficient n’est pas inversé
          stor.numValeurFinale = stor.numValeurProduit
          stor.denValeurFinale = 1
          stor.numValeurInitiale = stor.valeurInitiale
          stor.denValeurInitiale = 1
          stor.coeffString = ecrisBienMathquill(stor.numCoefficient)
          stor.coeffCarreString = ecrisBienMathquill(stor.numCoefficient) + '²'
          stor.coeffCubeString = ecrisBienMathquill(stor.numCoefficient) + '³'
          break
        case 'fractions': {
          // On inverse valeur initiale et valeur finale
          stor.numValeurInitiale = stor.numValeurProduit
          stor.denValeurInitiale = stor.denValeurProduit
          stor.numValeurFinale = stor.valeurInitiale
          stor.denValeurFinale = 1
          const ui = stor.numCoeffExponentiel
          stor.numCoeffExponentiel = stor.denCoeffExponentiel
          stor.denCoeffExponentiel = ui
          stor.coeffString = '\\frac{' + ecrisBienMathquill(stor.denCoefficient) + '}{' + ecrisBienMathquill(stor.numCoefficient) + '}'
          stor.coeffCarreString = '(\\frac{' + ecrisBienMathquill(stor.denCoefficient) + '}{' + ecrisBienMathquill(stor.numCoefficient) + '})²'
          stor.coeffCubeString = '(\\frac{' + ecrisBienMathquill(stor.denCoefficient) + '}{' + ecrisBienMathquill(stor.numCoefficient) + '})³'
        }
          break
        default: throw Error('Type de coefficient inconnu')
      }
    }
    stor.fautCorrection = false
    return e
  }

  function ecrireValeurInitiale () {
    if (stor.denValeurInitiale !== 1) {
      const pg = j3pPGCD(stor.numValeurInitiale, stor.denValeurInitiale)
      // Si la fraction est un entier (dénominateur égal au pgcd(num,den)) on retourne l’entier sinon on retourne la fraction simplifiée.
      if (stor.denValeurInitiale === pg) return ecrisBienMathquill(j3pArrondi(stor.numValeurInitiale / pg, 0))
      return '\\frac{' + ecrisBienMathquill(j3pArrondi(stor.numValeurInitiale / pg, 0)) + '}{' + ecrisBienMathquill(j3pArrondi(stor.denValeurInitiale / pg, 0)) + '}'
    }
    return ecrisBienMathquill(stor.numValeurInitiale)
  }

  function ecrireCoeffExponentiel () {
    if (stor.numCoeffExponentiel === 1) return '\\frac{1}{' + ecrisBienMathquill(stor.denCoeffExponentiel) + '}'
    if (stor.denCoeffExponentiel !== 1) {
      const pg = j3pPGCD(stor.numCoeffExponentiel, stor.denCoeffExponentiel)
      if (stor.denCoeffExponentiel === pg) return ecrisBienMathquill(j3pArrondi(stor.numCoeffExponentiel / pg, 0))
      return '\\frac{' + ecrisBienMathquill(j3pArrondi(stor.numCoeffExponentiel / pg, 0)) + '}{' + ecrisBienMathquill(j3pArrondi(stor.denCoeffExponentiel / pg, 0)) + '}'
    }
    return ecrisBienMathquill(stor.numCoeffExponentiel)
  }

  function ecrireValeurFinale () {
    if (stor.denValeurFinale !== 1) {
      const pg = j3pPGCD(stor.numValeurFinale, stor.denValeurFinale)
      if (stor.denValeurFinale === pg) return ecrisBienMathquill(j3pArrondi(stor.numValeurFinale / pg, 0))
      return '\\frac{' + ecrisBienMathquill(j3pArrondi(stor.numValeurFinale / pg, 0)) + '}{' + ecrisBienMathquill(j3pArrondi(stor.denValeurFinale / pg, 0)) + '}'
    }
    return ecrisBienMathquill(stor.numValeurFinale)
  }

  function affEnonce () {
    const eff = stor.exo.isAgrandissement ? 'un agrandissement' : 'une réduction'
    const adire = detDet(stor.grandeur.s) + stor.grandeur.n2 + ' est ' + eff + ' ' + detUnd(stor.grandeur.s) + stor.grandeur.n1 + ' de rapport $' + stor.coeffString + '$<br>'
    j3pAffiche(stor.lesdiv.consigneG, null, adire)
    stor.idire1 = stor.deQuoiDe2
    stor.idire1 += stor.grandeur.n1s
    stor.iadire = stor.deQuoiDe2
    stor.iadire += stor.grandeur.n2s
    let idire = stor.idire1 + '$= '
    idire += ecrireValeurInitiale()
    idire += '$ '
    stor.unit = 'cm' + (stor.exo.quest === 'longueur' ? '' : stor.exo.quest === 'aire' ? '²' : '³')
    j3pAffiche(stor.lesdiv.consigneG, null, idire + stor.unit + '\n\n')
    stor.idire = stor.deQuoiDe2
    stor.idire += stor.grandeur.n2s
    idire = 'On veut calculer ' + stor.idire
    j3pAffiche(stor.lesdiv.consigneG, null, '<b>' + idire + '</b>\n')
  }

  function modif (ki) {
    for (let i = 0; i < ki.length; i++) {
      for (let j = 0; j < ki[i].length; j++) ki[i][j].style.padding = '0px'
    }
  }

  function premsAvec (ki) {
    do {
      const rep = j3pGetRandomInt(2, ki - 1)
      if (j3pPGCD(rep, ki) === 1) return rep
    } while (true)
  }

  function poseQuestion () {
    j3pEmpty(stor.lesdiv.explications)
    j3pEmpty(stor.lesdiv.solution)
    j3pEmpty(stor.lesdiv.correction)
    stor.lesdiv.solution.classList.remove('correction')
    stor.lesdiv.explications.classList.remove('explique')
    if (stor.encours === 'justifie') {
      stor.yy = addDefaultTable(stor.lesdiv.justifie, 1, 2)
      modif(stor.yy)
      j3pAffiche(stor.yy[0][0], null, stor.iadire + '$=$&nbsp;')
      const ll = ['choisir']
      ll.push(stor.idire1 + ' $\\times ' + stor.coeffString + '$')
      ll.push(stor.idire1 + ' $\\times ' + stor.coeffCarreString + '$')
      ll.push(stor.idire1 + ' $\\times ' + stor.coeffCubeString + '$')
      stor.larep = ListeDeroulante.create(stor.yy[0][1], ll, { choix0: false })
      stor.repat = stor.exo.quest === 'longueur' ? ll[1] : stor.exo.quest === 'aire' ? ll[2] : ll[3]
    } else {
      if (stor.fautCorrection) {
        j3pEmpty(stor.yy[0][1])
        stor.yy[0][1].style.color = me.styles.petit.correction.color
        j3pAffiche(stor.yy[0][1], null, stor.repat)
      }
      const tw = addDefaultTable(stor.lesdiv.calcul, 2, 3)
      const tw2 = addDefaultTable(tw[1][1], 1, 2)
      tw2[0][1].style.padding = '0px'
      tw2[0][0].style.padding = '0px'
      j3pAffiche(tw[0][0], null, stor.iadire + '$=$&nbsp;')
      j3pAffiche(tw[1][0], null, stor.iadire + '$=$&nbsp;')
      j3pAffiche(tw2[0][1], null, '&nbsp;' + stor.unit)
      const ty = affichefois(tw[0][1])
      stor.zone1 = new ZoneStyleMathquill2(ty[0], { id: 'zone11', restric: '0123456789,./', enter: () => { me.sectionCourante() } })
      stor.zone2 = new ZoneStyleMathquill2(ty[2], { id: 'zone12', restric: '0123456789,./', enter: () => { me.sectionCourante() } })
      stor.zone3 = new ZoneStyleMathquill2(tw2[0][0], { id: 'zone13', restric: '0123456789,./', enter: () => { me.sectionCourante() } })
    }
  }

  function detDet (b) {
    if (b) return 'Le '
    return 'La '
  }

  function detUnd (b) {
    if (b) return 'du '
    return 'de la '
  }

  function yaReponse () {
    if (stor.encours === 'justifie') {
      if (!stor.larep.changed) {
        stor.larep.focus()
        return false
      }
    } else {
      if (stor.zone1.reponse() === '') {
        stor.zone1.focus()
        return false
      }
      if (stor.zone2.reponse() === '') {
        stor.zone2.focus()
        return false
      }
      if (stor.zone3.reponse() === '') {
        stor.zone3.focus()
        return false
      }
    }
    return true
  }

  function barrelesfo () {
    if (stor.encours === 'justifie') {
      stor.larep.barre()
    } else {
      stor.zone1.barreIfKo()
      stor.zone2.barreIfKo()
      stor.zone3.barreIfKo()
    }
    return true
  }

  function isRepOk () {
    stor.errEcritList = []
    stor.errMank1 = false
    stor.errMM = false
    stor.errMank2 = false
    stor.errInver = false
    stor.errRez = false
    let ok = true
    if (stor.encours === 'justifie') {
      return stor.larep.reponse === stor.repat
    } else {
      const rep1 = stor.zone1.reponse()
      const rep2 = stor.zone2.reponse()
      const rep3 = stor.zone3.reponse()
      const nb1 = testNb(rep1)
      const nb2 = testNb(rep2)
      const nb3 = testNb(rep3)
      if (!nb1.good) {
        ok = false
        stor.zone1.corrige(false)
        stor.errEcritList.push(nb1.remede)
      }
      if (!nb2.good) {
        ok = false
        stor.zone2.corrige(false)
        stor.errEcritList.push(nb2.remede)
      }
      if (!nb3.good) {
        ok = false
        stor.zone3.corrige(false)
        stor.errEcritList.push(nb3.remede)
      }
      if (!ok) return false
      // l1
      const m1 = nb1.frac ? nb1.nb : { num: nb1.nb, den: 1 }
      const m2 = nb2.frac ? nb2.nb : { num: nb2.nb, den: 1 }
      const m3 = nb3.frac ? nb3.nb : { num: nb3.nb, den: 1 }
      // ndeb
      if ((!egal(m1, { num: stor.numValeurInitiale, den: stor.denValeurInitiale })) && (!egal(m1, { num: stor.numCoeffExponentiel, den: stor.denCoeffExponentiel }))) {
        ok = false
        stor.zone1.corrige(false)
      }
      if ((!egal(m2, { num: stor.numValeurInitiale, den: stor.denValeurInitiale })) && (!egal(m2, { num: stor.numCoeffExponentiel, den: stor.denCoeffExponentiel }))) {
        ok = false
        stor.zone2.corrige(false)
      }
      if (m1 === m2) {
        stor.errMM = true
        stor.zone2.corrige(false)
      }
      if ((!egal(m1, { num: stor.numValeurInitiale, den: stor.denValeurInitiale })) && (!egal(m2, { num: stor.numValeurInitiale, den: stor.denValeurInitiale }))) {
        stor.errMank1 = true
      }
      // coefg
      if ((!egal(m2, { num: stor.numCoeffExponentiel, den: stor.denCoeffExponentiel })) && (!egal(m1, { num: stor.numCoeffExponentiel, den: stor.denCoeffExponentiel }))) {
        stor.errMank2 = true
      }
      if (!ok) return false
      if (egal(m1, { num: stor.numCoeffExponentiel, den: stor.denCoeffExponentiel })) {
        stor.errInver = true
      }
      // test inverse
      if (!egal(m3, { num: stor.numValeurFinale, den: stor.denValeurFinale })) {
        stor.errRez = true
        stor.zone3.corrige(false)
        return false
      }
      return true
    }
  } // isRepOk

  function egal (a, b) {
    return Math.abs(a.num * b.den - b.num * a.den) < 0.000000001
  }

  function testNb (s) {
    if (s.indexOf('frac') === -1) {
      s = s.replace(/ /g, '')
      return verifNombreBienEcrit(s, false, true)
    }
    s = s.replace('\\frac{', '')
    s = s.replace('{', '')
    s = s.split('}')
    const k1 = verifNombreBienEcrit(s[0].replace(/ /g, ''), false, true)
    const k2 = verifNombreBienEcrit(s[1].replace(/ /g, ''), false, true)
    const ret = { good: k1.good && k2.good }
    if (!ret.good) {
      if (!k1.good) {
        ret.remede = k1.remede
      } else {
        ret.remede = k2.remede
      }
    } else {
      ret.nb = { num: k1.nb, den: k2.nb }
      ret.frac = true
    }
    return ret
  } // testNb

  function desactiveAll () {
    if (stor.encours === 'justifie') {
      stor.larep.disable()
    } else {
      stor.zone1.disable()
      stor.zone2.disable()
      stor.zone3.disable()
    }
  } // desactiveAll

  function passeToutVert () {
    if (stor.encours === 'justifie') {
      stor.larep.corrige(true)
    } else {
      stor.zone1.corrige(true)
      stor.zone2.corrige(true)
      stor.zone3.corrige(true)
    }
  } // passeToutVert

  function affCorrFaux (isFin) {
    let hasCorrection = false
    let hasExplanation = false
    if (stor.encours === 'justifie') {
      stor.larep.corrige(false)
    }
    if (stor.errEcritList.length > 0) {
      hasExplanation = true
      const dd = []
      for (let i = 0; i < stor.errEcritList.length; i++) {
        if (dd.indexOf(stor.errEcritList[i]) !== -1) continue
        dd.push(stor.errEcritList[i])
        j3pAffiche(stor.lesdiv.explications, null, stor.errEcritList[i] + '\n')
      }
    }
    if (stor.errMank1) {
      hasExplanation = true
      j3pAffiche(stor.lesdiv.explications, null, 'Je ne retrouve la grandeur de départ !\n')
    }
    if (stor.errMM) {
      hasExplanation = true
      j3pAffiche(stor.lesdiv.explications, null, 'Tu écris deux fois la même valeur !\n')
    }
    if (stor.errMank2) {
      hasExplanation = true
      j3pAffiche(stor.lesdiv.explications, null, 'Je ne trouve pas le bon multiplicateur !\n')
    }
    if (stor.errInver) {
      hasExplanation = true
      j3pAffiche(stor.lesdiv.explications, null, 'Tu inverses les deux valeurs \n')
    }
    if (stor.errRez) {
      hasExplanation = true
      j3pAffiche(stor.lesdiv.explications, null, 'erreur de calcul \n')
    }
    if (isFin) {
      hasCorrection = true
      stor.fautCorrection = true
      barrelesfo()
      desactiveAll()
      if (stor.encours === 'justifie') {
        j3pAffiche(stor.lesdiv.solution, null, stor.iadire + ' $ = $' + stor.repat)
      } else {
        const ad = stor.exo.quest === 'longueur' ? stor.coeffString : stor.exo.quest === 'aire' ? stor.coeffCarreString : stor.coeffCubeString
        j3pAffiche(stor.lesdiv.solution, null, stor.iadire + ' $ = ' + ecrireValeurInitiale() + ' \\times ' + ad + '$\n')
        if (stor.exo.quest !== 'longueur') j3pAffiche(stor.lesdiv.solution, null, stor.iadire + ' $ = ' + ecrireValeurInitiale() + ' \\times ' + ecrireCoeffExponentiel() + '$\n')
        j3pAffiche(stor.lesdiv.solution, null, stor.iadire + ' $ = ' + ecrireValeurFinale() + '$ ' + stor.unit)
      }
    }
    if (hasCorrection) {
      stor.lesdiv.solution.classList.add('correction')
    }
    if (hasExplanation) {
      stor.lesdiv.explications.classList.add('explique')
    }
  } // affCorrFaux

  function suiv () {
    return (stor.encours === 'justifie') ? 'calcul' : stor.afaire[0]
  }

  // appelé par initSection ou directement dans le case enonce si on est pas au début de la section
  function enonceMain () {
    stor.encours = suiv(stor.encours)
    if (stor.encours === stor.afaire[0]) {
      me.videLesZones()
      creeLesDiv()
      stor.lesdiv.travail.classList.add('travail')
      stor.lesdiv.consigneG.classList.add('enonce')
      stor.exo = faisChoixExos()
      me.zonesElts.MG.classList.add('fond')
      affEnonce()
      if (ds.Calculatrice) {
        me.fenetresjq = [{ name: 'Calculatrice', title: 'Une calculatrice', left: 600, top: 10 }]
        j3pCreeFenetres(me)
        j3pAfficheCroixFenetres('Calculatrice')
        j3pAjouteBouton(stor.lesdiv.calculatrice, boutonfen, { className: 'MepBoutons', value: 'Calculatrice' })
      }
    }
    poseQuestion()
    me.finEnonce()
  }

  switch (this.etat) {
    case 'enonce':
      // code exécuté au lancement de la section
      if (me.debutDeLaSection) {
        initSection()
        // initSection appellera enonceMain quand il aura fini ses chargements
      } else {
        enonceMain()
      }
      break // case "enonce":

    case 'correction':
      if (!stor.lesdiv) throw Error('Initialisation de l’exercice incorrecte')
      // On teste si une réponse a été saisie
      stor.lesdiv.explications.innerHTML = ''
      stor.lesdiv.solution.innerHTML = ''

      if (yaReponse()) {
        // Bonne réponse
        if (isRepOk()) {
          stor.lesdiv.correction.style.color = this.styles.cbien
          stor.lesdiv.correction.innerHTML = cBien
          passeToutVert()
          desactiveAll()
          this.score++
          this.typederreurs[0]++
          return this.finCorrection('navigation', true)
        }

        // Pas de bonne réponse
        stor.lesdiv.correction.style.color = this.styles.cfaux
        stor.lesdiv.correction.innerHTML = cFaux
        if (this.isElapsed) {
          // A cause de la limite de temps :
          stor.lesdiv.correction.innerHTML += tempsDepasse + '<BR>'
          this.typederreurs[10]++
          affCorrFaux(true)
          return this.finCorrection('navigation', true)
        }

        // Réponse fausse sans limite de temps
        if (me.essaiCourant < ds.nbchances) {
          stor.lesdiv.correction.innerHTML += '<br>' + essaieEncore
          affCorrFaux(false)
          this.typederreurs[1]++
          // il reste des essais, on reste en correction
          return this.finCorrection()
        }

        // Erreur au dernier essai
        affCorrFaux(true)
        // Pas de dixièmes de points ajoutés selon les étapes
        this.typederreurs[2]++
        return this.finCorrection('navigation', true)
      }

      // pas de réponse
      stor.lesdiv.correction.style.color = this.styles.cfaux
      stor.lesdiv.correction.innerHTML = reponseIncomplete
      this.finCorrection()
      break // case "correction":

    case 'navigation':
      if (!this.sectionTerminee()) {
        this.etat = 'enonce'
      }
      this.finNavigation()
      break // case "navigation":
  }
}
