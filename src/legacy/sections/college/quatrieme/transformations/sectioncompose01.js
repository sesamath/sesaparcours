import { j3pAddElt, j3pAjouteBouton, j3pArrondi, j3pGetNewId, j3pShowError, j3pShuffle } from 'src/legacy/core/functions'
import ListeDeroulante from 'src/legacy/outils/listeDeroulante/ListeDeroulante'
import { j3pAffiche } from 'src/lib/mathquill/functions'
import { addDefaultTable } from 'src/legacy/themes/table'
import { j3pCreeSVG } from 'src/legacy/core/functionsSvg'
import BulleAide from 'src/legacy/outils/bulleAide/BulleAide'
import textesGeneriques from 'src/lib/core/textes'
import { getMtgCore } from 'src/lib/outils/mathgraph'
import { addMaj } from 'src/lib/utils/string'

const { cBien, cFaux, essaieEncore, reponseIncomplete, tempsDepasse } = textesGeneriques

/**
 * Ajouter une description ici
 * @fileOverview
 * @author Tommy Barroy <t.barroy@laposte.net>
 * @since 2017-10
*/
export const params = {
  outils: ['mathquill'],
  /**
   * Ce qui est paramétrable dans l’éditeur de graphe
   * @type {LegacySectionParamsParametres}
   */
  parametres: [
    ['nbrepetitions', 9, 'entier', 'Nombre de répétitions de la section'],
    ['nbchances', 1, 'entier', 'Nombre d’essais possibles.'],
    ['Sym_Ax_perp', true, 'boolean', '<u>true</u>:  composée de deux symétrie d’axe perpendiculaires'],
    ['Sym_Ax_para', true, 'boolean', '<u>true</u>:  composée de deux symétrie d’axe parallèles'],
    ['Sym_Axe_sec', true, 'boolean', '<u>true</u>:  composée de deux symétrie d’axe simplement sécants'],
    ['Sym_Ce', true, 'boolean', '<u>true</u>:  composée de deux symétrie centrales'],
    ['Sym_Ce_Sym_Ax', true, 'boolean', '<u>true</u>:  composée d’une symétrie axiale et d’une symétrie centrale'],
    ['Trans', true, 'boolean', '<u>true</u>:  composée de deux translations'],
    ['Trans_Sym_Ce', true, 'boolean', '<u>true</u>:  composée d’une translation et d’une symétrie centrale'],
    ['Trans_Sym_Ax_perp', true, 'boolean', '<u>true</u>:  composée d’une translation et d’une symétrie axiale'],
    ['theme', 'standard', 'liste', 'Modèle pour l’affichage', ['standard', 'zonesAvecImageDeFond']]
  ]
}

/**
 * section compose01
 * @this {Parcours}
 */
export default function main () {
  const me = this
  const stor = this.storage
  let ds = me.donneesSection
  let svgId = stor.svgId

  function creeLesDiv () {
    stor.lesdiv = {}
    stor.lesdiv.conteneur = j3pAddElt(me.zonesElts.MG, 'div', {
      style: me.styles.etendre('toutpetit.enonce', { padding: '10px' })
    })
    const tabCont = addDefaultTable(stor.lesdiv.conteneur, 5, 1)
    modif(tabCont)
    stor.lesdiv.consigneG = tabCont[0][0]
    stor.lesdiv.travail = tabCont[1][0]
    const hu = addDefaultTable(tabCont[2][0], 1, 3)
    modif(hu)
    hu[0][1].style.width = '10px'
    stor.lesdiv.figure = hu[0][0]
    stor.lesdiv.bout1 = tabCont[3][0]
    stor.lesdiv.bout2 = tabCont[4][0]
    stor.lesdiv.bout1.style.color = 'green'
    stor.lesdiv.bout2.style.color = 'blue'
    stor.lesdiv.explications = j3pAddElt(stor.lesdiv.conteneur, 'div')
    const tyty = addDefaultTable(hu[0][2], 2, 1)
    stor.lesdiv.correction = tyty[0][0]
    stor.lesdiv.solution = tyty[1][0]
    stor.lesdiv.solution.style.color = me.styles.toutpetit.correction.color
    stor.lesdiv.explications.style.color = me.styles.cfaux
  }
  function getDonnees () {
    return {

      // Nombre de répétitions de l’exercice
      nbrepetitions: 9,

      Sym_Ax_perp: true,
      Sym_Ax_para: true,
      Sym_Axe_sec: true,
      Sym_Ce: true,
      Sym_Ce_Sym_Ax: true,
      Trans: true,
      Trans_Sym_Ce: true,
      Trans_Sym_Ax_perp: true,

      // Si indication != "" alors un lien "Indication" s’affiche dans la zone Inférieure Gauche (this.zones.IG
      // une indication<br>de deux lignes..."
      indication: '',

      // pour le cas de presentation1bis
      // En effet pas d’indication possible pour cette présentation
      // surchargeindication: false,

      // Nombre de chances dont dispose l’él§ve pour répondre à la question
      nbchances: 1,

      Choix_Phrase: true,
      //  || "presentation2" || "presentation3"  || "presentation1bis"
      structure: 'presentation3'
    }
  }
  function initSection () {
    let i
    me.donneesSection = getDonnees()
    // On surcharge avec les parametres passés par le graphe
    me.surcharge()
    // et on réaffecte notre var
    ds = me.donneesSection

    // Construction de la page
    me.construitStructurePage(ds.structure)

    ds.lesExos = []

    let lp2 = []
    if (ds.Sym_Ax_perp) lp2.push('Sym_Ax_perp')
    if (ds.Sym_Ax_para) lp2.push('Sym_Ax_para')
    if (ds.Sym_Axe_sec) lp2.push('Sym_Axe_sec')
    if (ds.Sym_Ce) lp2.push('Sym_Ce')
    if (ds.Sym_Ce_Sym_Ax) lp2.push('Sym_Ce_Sym_Ax')
    if (ds.Trans) lp2.push('Trans')
    if (ds.Trans_Sym_Ce) lp2.push('Trans_Sym_Ce')
    if (ds.Trans_Sym_Ax_perp) lp2.push('Trans_Sym_Ax_perp')
    if (lp2.length === 0) {
      j3pShowError('Paramètrage invalide')
      lp2 = ['Sym_Ax_perp']
    }
    lp2 = j3pShuffle(lp2)
    for (i = 0; i < ds.nbitems; i++) {
      ds.lesExos.push(lp2[i % lp2.length])
    }
    ds.lesExos = j3pShuffle(ds.lesExos)

    me.typederreurs = [0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0]

    me.score = 0

    me.afficheTitre('Composer deux transformation')
    // chargement mtg
    getMtgCore({ withMathJax: true })
      .then(
        // success
        (mtgAppLecteur) => {
          stor.mtgAppLecteur = mtgAppLecteur
          enonceMain()
        },
        // failure
        (error) => {
          j3pShowError(error, { message: 'mathgraph n’est pas correctement chargé', mustNotify: true })
        })
      // plantage dans enonceMain
      .catch(j3pShowError)
  }

  function faisChoixExos () {
    const e = ds.lesExos.pop()
    switch (e.transfo) {
      case 'symc':
        stor.nomtrans = 'la symétrie centrale'
        stor.nomtransUnd = 'une symétrie centrale'
        break
      case 'syma':
        stor.nomtrans = 'la symétrie axiale'
        stor.nomtransUnd = 'une symétrie axiale'
        break
      case 'trans':
        stor.nomtrans = 'la translation'
        stor.nomtransUnd = 'une translation'
        break
      case 'rot':
        stor.nomtrans = 'la rotation'
        stor.nomtransUnd = 'une rotation'
        break
      case 'homot':
        stor.nomtrans = 'l’homothétie'
        stor.nomtransUnd = 'une homothétie'
        break
      case 'agrand':
        stor.nomtrans = 'l’agrandissement-réduction'
        stor.nomtransUnd = 'un agrandissement-réduction'
        break
    }
    stor.ttourne = true
    return e
  }
  function modif (ki) {
    for (let i = 0; i < ki.length; i++) {
      for (let j = 0; j < ki[i].length; j++) ki[i][j].style.padding = '0px'
    }
  }
  function faiFigure () {
    svgId = j3pGetNewId()
    stor.svgId = svgId
    const nomspris = []
    stor.lettres = [addMaj(nomspris), addMaj(nomspris), addMaj(nomspris)]

    const txtfig = 'TWF0aEdyYXBoSmF2YTEuMAAAABM+TMzNAAJmcv###wEA#wEAAAAAAAAAAAM5AAACbQAAAQEAAAAAAAAAAQAAAVv#####AAAAAQAKQ0NhbGNDb25zdAD#####AAJwaQAWMy4xNDE1OTI2NTM1ODk3OTMyMzg0Nv####8AAAABAApDQ29uc3RhbnRlQAkh+1RELRj#####AAAAAQAKQ1BvaW50QmFzZQD#####AQAAAAAOAAFVAMAkAAAAAAAAQBAAAAAAAAAAAAUAAEAv#nbItDlYQC#+dsi0OVj#####AAAAAQAUQ0Ryb2l0ZURpcmVjdGlvbkZpeGUA#####wEAAAAAEAAAAQAAAAEAAAABAT#wAAAAAAAA#####wAAAAEAD0NQb2ludExpZURyb2l0ZQD#####AQAAAAAOAAFWAMAAAAAAAAAAQBAAAAAAAAAAAAUAAUA##nbItDlYAAAAAv####8AAAABAAhDU2VnbWVudAD#####AQAAAAAQAAABAAAAAQAAAAEAAAAD#####wAAAAEAB0NNaWxpZXUA#####wEAAAAAEAAAAAAAAAAAAAAAQAgAAAAAAAAAAAUAAAAAAQAAAAP#####AAAAAgAMQ0NvbW1lbnRhaXJlAP####8BAAAAAAAAAAAAAAAAQBgAAAAAAAAAAAAAAAUMAAAAAAABAAAAAAAAAAEAAAAAAAAAAAABMf####8AAAABAAlDTG9uZ3VldXIA#####wAAAAEAAAAD#####wAAAAEAB0NDYWxjdWwA#####wABQQADMC4zAAAAAT#TMzMzMzMzAAAACQD#####AAFCAAEyAAAAAUAAAAAAAAAAAAAAAgD#####AQAAAAAQAAAAAAAAAAAAAABACAAAAAAAAAAACQABQGEgAAAAAABAaTwo9cKPXAAAAAIA#####wEAAAAAEAAAAAAAAAAAAAAAQAgAAAAAAAAAAAkAAUBpQAAAAAAAQGr8KPXCj1wAAAAFAP####8B#wAAABAAAAEAAmNBAQEAAAAKAAAACwAAAAIA#####wEAAAAAEAAAAAAAAAAAAAAAQAgAAAAAAAAAAAkAAUB0YAAAAAAAQGccKPXCj1wAAAACAP####8BAAAAABAAAUYAQAAAAAAAAADAQAAAAAAAAAAACQABQG6AAAAAAABAYfwo9cKPXAAAAAQA#####wH#AAAAEAAAAAAAAAAAAAAAQAgAAAAAAAAAAUEIAAE#5NLCy09NLQAAAAz#####AAAAAQAJQ0Ryb2l0ZUFCAP####8BAAAAABAABChkMSkBAAJkMQACP+zMzMzMzM0AAAAPAAAADv####8AAAABABBDRHJvaXRlUGFyYWxsZWxlAP####8BAAAAABAABChkMikBAAJkMgACP+zMzMzMzM0AAAANAAAAEP####8AAAABABZDRHJvaXRlUGVycGVuZGljdWxhaXJlAP####8BAAAAABAABChkMykAQAkVx8fkDADAMMxABozG4AACZDMAAj#sfWXKOyOwAAAADgAAABAAAAAKAP####8BAAAAABAABChkNCkAQBHjy8PAX8DAKG4pxyLdgAACZDQAAj#stfMly4r#AAAADQAAAA4AAAACAP####8BAAAAABAAAAAAAAAAAAAAAEAIAAAAAAAAAAAIAAFAbUAAAAAAAEB+jhR64UeuAAAAAgD#####AQAAAAAQAAAAAAAAAAAAAABACAAAAAAAAAAACAABQHIgAAAAAABAf74UeuFHrgAAAAIA#####wEAAAAAEAAAAAAAAAAAAAAAQAgAAAAAAAAAAAgAAUBtgAAAAAAAQIEXCj1wo9cAAAACAP####8BAAAAABAAAAAAAAAAAAAAAEAIAAAAAAAAAAAIAAFAckAAAAAAAECBnwo9cKPXAAAAAgD#####AQAAAAAQAAAAAAAAAAAAAABACAAAAAAAAAAACAABQGlgAAAAAABAgfcKPXCj1wAAAAIA#####wEAAAAAEAAAAAAAAAAAAAAAQAgAAAAAAAAAAAgAAUBm4AAAAAAAQIA#Cj1wo9cAAAACAP####8BAAAAABAAAAAAAAAAAAAAAEAIAAAAAAAAAAAIAAFAY4AAAAAAAEBgvCj1wo9c#####wAAAAIACUNDZXJjbGVPUgD#####Af8AAAACY00BAQAAABoAAAABQAgAAAAAAAAA#####wAAAAEAFUNQb2ludEludGVyaWV1ckNlcmNsZQD#####AP8AAAAQAAAAAAAAAAAAAABACAAAAAAAAAABTQgAAQAAABs#79j74T0erkACjatLW1nQAAAAAgD#####AQAAAAAQAAAAAAAAAAAAAABACAAAAAAAAAAACAABQG6AAAAAAABAgBcKPXCj1#####8AAAABAAhDVmVjdGV1cgD#####AQAAAAAQAAABAAAAAgAAAB0AAAAcAP####8AAAABAA1DVHJhbnNQYXJWZWN0AP####8AAAAe#####wAAAAEAC0NQb2ludEltYWdlAP####8BAAAAABAAAAAAAAAAAAAAAEAIAAAAAAAAAAAIAAAAABQAAAAfAAAAEQD#####AQAAAAAQAAAAAAAAAAAAAABACAAAAAAAAAAACAAAAAAVAAAAHwAAABEA#####wEAAAAAEAAAAAAAAAAAAAAAQAgAAAAAAAAAAAgAAAAAFgAAAB8AAAARAP####8BAAAAABAAAUUAAAAAAAAAAABACAAAAAAAAAAACAAAAAAXAAAAHwAAABEA#####wEAAAAAEAAAAAAAAAAAAAAAQAgAAAAAAAAAAAgAAAAAGAAAAB8AAAARAP####8BAAAAABAAAAAAAAAAAAAAAEAIAAAAAAAAAAAIAAAAABkAAAAf#####wAAAAEACUNQb2x5Z29uZQD#####AAAAAAAAAAIAAAAHAAAAIQAAACIAAAAjAAAAJAAAACUAAAAgAAAAIf####8AAAABABBDU3VyZmFjZVBvbHlnb25lAP####8AAAAAAAAAAAAFAAAAJgAAAA0A#####wH#AAAABHJlcE0ABAAAABz#####AAAAAQAPQ1Jlc3VsdGF0VmFsZXVyAAAACAD#####AAAAAQAPQ1N5bWV0cmllQXhpYWxlAP####8AAAAQAAAAEQD#####AQB#AAAQAAAAAAAAAAAAAABACAAAAAAAAAAACAAAAAAjAAAAKQAAABEA#####wEAfwAAEAAAAAAAAAAAAAAAQAgAAAAAAAAAAAgAAAAAIgAAACkAAAARAP####8BAH8AABAAAAAAAAAAAAAAAEAIAAAAAAAAAAAIAAAAACEAAAApAAAAEQD#####AQB#AAAQAAAAAAAAAAAAAABACAAAAAAAAAAACAAAAAAgAAAAKQAAABEA#####wEAfwAAEAAAAAAAAAAAAAAAQAgAAAAAAAAAAAgAAAAAJQAAACkAAAARAP####8BAH8AABAAAAAAAAAAAAAAAEAIAAAAAAAAAAAIAAAAACQAAAApAAAAEgD#####AQB#AAAGc3ltQTFQAAIAAAAHAAAAKgAAACsAAAAsAAAALQAAAC4AAAAvAAAAKgAAABMA#####wEAfwAABnN5bUExUwAAAAUAAAAwAAAAFQD#####AAAAEQAAABEA#####wEAAP8AEAAAAAAAAAAAAAAAQAgAAAAAAAAAAAgAAAAAKgAAADIAAAARAP####8BAAD#ABAAAAAAAAAAAAAAAEAIAAAAAAAAAAAIAAAAAC8AAAAyAAAAEQD#####AQAA#wAQAAAAAAAAAAAAAABACAAAAAAAAAAACAAAAAAuAAAAMgAAABEA#####wEAAP8AEAAAAAAAAAAAAAAAQAgAAAAAAAAAAAgAAAAALQAAADIAAAARAP####8BAAD#ABAAAAAAAAAAAAAAAEAIAAAAAAAAAAAIAAAAACwAAAAyAAAAEQD#####AQAA#wAQAAAAAAAAAAAAAABACAAAAAAAAAAACAAAAAArAAAAMgAAABIA#####wEAAP8ABnN5bUEyUAACAAAABwAAADQAAAAzAAAAOAAAADcAAAA2AAAANQAAADQAAAATAP####8BAAD#AAZzeW1BMlMAAAAFAAAAOQAAABUA#####wAAABIAAAARAP####8BAAD#ABAAAAAAAAAAAAAAAEAIAAAAAAAAAAAIAAAAACwAAAA7AAAAEQD#####AQAA#wAQAAAAAAAAAAAAAABACAAAAAAAAAAACAAAAAAtAAAAOwAAABEA#####wEAAP8AEAAAAAAAAAAAAAAAQAgAAAAAAAAAAAgAAAAALgAAADsAAAARAP####8BAAD#ABAAAAAAAAAAAAAAAEAIAAAAAAAAAAAIAAAAAC8AAAA7AAAAEQD#####AQAA#wAQAAAAAAAAAAAAAABACAAAAAAAAAAACAAAAAAqAAAAOwAAABEA#####wEAAP8AEAAAAAAAAAAAAAAAQAgAAAAAAAAAAAgAAAAAKwAAADsAAAASAP####8BAAD#AAZzeW1BM1AAAgAAAAcAAABAAAAAPwAAAD4AAAA9AAAAPAAAAEEAAABAAAAAEwD#####AQAA#wAGc3ltQTNTAAAABQAAAEIAAAAVAP####8AAAATAAAAEQD#####AQAA#wAQAAFHAAAAAAAAAAAAQAgAAAAAAAAAAAgAAAAAKgAAAEQAAAARAP####8BAAD#ABAAAAAAAAAAAAAAAEAIAAAAAAAAAAAIAAAAAC8AAABEAAAAEQD#####AQAA#wAQAAAAAAAAAAAAAABACAAAAAAAAAAACAAAAAAuAAAARAAAABEA#####wEAAP8AEAAAAAAAAAAAAAAAQAgAAAAAAAAAAAgAAAAALQAAAEQAAAARAP####8BAAD#ABAAAAAAAAAAAAAAAEAIAAAAAAAAAAAIAAAAACwAAABEAAAAEQD#####AQAA#wAQAAAAAAAAAAAAAABACAAAAAAAAAAACAAAAAArAAAARAAAABIA#####wEAAP8ABnN5bUE0UAACAAAABwAAAEUAAABKAAAASQAAAEgAAABHAAAARgAAAEUAAAATAP####8BAAD#AAZzeW1BNFMAAAAFAAAASwAAAA0A#####wH#AAAABHJlcEEABQAAAA8AAAAUAAAACAAAAAACAP####8B#wAAABAAAAAAAAAAAAAAAEAIAAAAAAAAAAAIAAFAaQAAAAAAAEB+PhR64UeuAAAAEQD#####Af8AAAAQAAAAAAAAAAAAAABACAAAAAAAAAAACAAAAABOAAAAHwAAAAcA#####wAAAAABAAAAAABPEAAAAAAAAQAAAAEAAAABAAAAAAAAAAAAAyhGKQAAABEA#####wEAAAAAEAAAAAAAAAAAAAAAQAgAAAAAAAAAAAgAAAAATwAAACkAAAAHAP####8BAH8AAQAGc3ltQTF0AAAAURAAAAAAAAEAAAABAAAAAQAAAAAAAAAAAAQoRicpAAAAEQD#####AQAAfwAQAAAAAAAAAAAAAABACAAAAAAAAAAACAAAAABRAAAAOwAAAAcA#####wEAAH8BAAZzeW1BM3QAAABTEAAAAAAAAQAAAAEAAAABAAAAAAAAAAAABShGJycpAAAAEQD#####AQAAfwAQAAAAAAAAAAAAAABACAAAAAAAAAAACAAAAABRAAAAMgAAAAcA#####wEAAH8BAAZzeW1BMnQAAABVEAAAAAAAAQAAAAEAAAABAAAAAAAAAAAABShGJycpAAAAEQD#####AQAAfwAQAAAAAAAAAAAAAABACAAAAAAAAAAACAAAAABRAAAARAAAAAcA#####wEAAH8BAAZzeW1BNHQAAABXEAAAAAAAAQAAAAEAAAABAAAAAAAAAAAABShGJycpAAAAAgD#####AQB#AAAQAAAAAAAAAAAAAABACAAAAAAAAAAACQABQG#gAAAAAABAYFwo9cKPXAAAAAIA#####wH#AAAAEAAAAAAAAAAAAAAAQAgAAAAAAAAAAAkAAUBsIAAAAAAAQF84UeuFHrgAAAACAP####8B#wAAABAAAAAAAAAAAAAAAEAIAAAAAAAAAAAJAAFAclAAAAAAAEBgvCj1wo9cAAAABQD#####Af8AAAAQAAABAAJjUgEBAAAAWgAAAFsAAAAEAP####8B#wAAABAAAUEAwCAAAAAAAADAPAAAAAAAAAABUgkAAQAAAAAAAAAAAAAAXP####8AAAABABFDU3ltZXRyaWVDZW50cmFsZQD#####AAAAXQAAABEA#####wEAfwAAEAAAAAAAAAAAAAAAQAgAAAAAAAAAAAkAAAAAIwAAAF4AAAARAP####8BAH8AABAAAAAAAAAAAAAAAEAIAAAAAAAAAAAJAAAAACIAAABeAAAAEQD#####AQB#AAAQAAAAAAAAAAAAAABACAAAAAAAAAAACQAAAAAhAAAAXgAAABEA#####wEAfwAAEAAAAAAAAAAAAAAAQAgAAAAAAAAAAAkAAAAAIAAAAF4AAAARAP####8BAH8AABAAAAAAAAAAAAAAAEAIAAAAAAAAAAAJAAAAACUAAABeAAAAEQD#####AQB#AAAQAAAAAAAAAAAAAABACAAAAAAAAAAACQAAAAAkAAAAXgAAABEA#####wEAfwAAEAAAAAAAAAAAAAAAQAgAAAAAAAAAAAkAAAAATwAAAF4AAAASAP####8BAH8AAAZzeW1DMlAAAgAAAAcAAABfAAAAYAAAAGEAAABiAAAAYwAAAGQAAABfAAAAEwD#####AQB#AAAGc3ltQzJTAAAABQAAAGYAAAAHAP####8BAH8AAQAGc3ltQzJ0AAAAZRAAAAAAAAEAAAABAAAAAQAAAAAAAAAAAAQoRicpAAAADQD#####Af8AAAAEcmVwUgAEAAAAXQAAABQAAAAIAAAAAAIA#####wH#AAAAEAAAAAAAAAAAAAAAQAgAAAAAAAAAAAkAAUBs4AAAAAAAQG#8KPXCj1wAAAACAP####8B#wAAABAAAAAAAAAAAAAAAEAIAAAAAAAAAAAJAAFAc5AAAAAAAEBtPCj1wo9cAAAABQD#####Af8AAAAQAAABAAJjWAEBAAAAagAAAGsAAAAEAP####8B#wAAABAAAUIAAAAAAAAAAABACAAAAAAAAAABWAkAAQAAAAAAAAAAAAAAbAAAABYA#####wAAAG0AAAARAP####8BAAB#ABAAAAAAAAAAAAAAAEAIAAAAAAAAAAAJAAAAAF8AAABuAAAAEQD#####AQAAfwAQAAAAAAAAAAAAAABACAAAAAAAAAAACQAAAABhAAAAbgAAABEA#####wEAAH8AEAAAAAAAAAAAAAAAQAgAAAAAAAAAAAkAAAAAYgAAAG4AAAARAP####8BAAB#ABAAAAAAAAAAAAAAAEAIAAAAAAAAAAAJAAAAAGUAAABuAAAAEQD#####AQAAfwAQAAAAAAAAAAAAAABACAAAAAAAAAAACQAAAABjAAAAbgAAABEA#####wEAAH8AEAAAAAAAAAAAAAAAQAgAAAAAAAAAAAkAAAAAZAAAAG4AAAARAP####8BAAD#ABAAAAAAAAAAAAAAAEAIAAAAAAAAAAAJAAAAAGAAAABuAAAAEgD#####AQAAfwAGc3ltQzNQAAIAAAAHAAAAcwAAAHEAAABwAAAAdQAAAG8AAAB0AAAAcwAAABMA#####wEAAH8ABnN5bUMzUwAAAAUAAAB2AAAABwD#####AQAAfwEABnN5bUMzdAAAAHIQAAAAAAABAAAAAQAAAAEAAAAAAAAAAAAFKEYnJykAAAANAP####8B#wAAAARyZXBYAAQAAABtAAAAFAAAAAgAAAAAAgD#####Af8AAAAQAAAAAAAAAAAAAABACAAAAAAAAAAACQABQHCwAAAAAABAcq4UeuFHrgAAAAoA#####wEAAAAAEAAEKGQ1KQBAGGpaPmlwQMAzfPbUgMvgAAJkNQACP+yf1vvBBUMAAABdAAAAegAAABUA#####wAAAHsAAAARAP####8BAAB#ABAAAAAAAAAAAAAAAEAIAAAAAAAAAAAJAAAAAF8AAAB8AAAAEQD#####AQAAfwAQAAAAAAAAAAAAAABACAAAAAAAAAAACQAAAABkAAAAfAAAABEA#####wEAAH8AEAAAAAAAAAAAAAAAQAgAAAAAAAAAAAkAAAAAYwAAAHwAAAARAP####8BAAB#ABAAAAAAAAAAAAAAAEAIAAAAAAAAAAAJAAAAAGUAAAB8AAAAEQD#####AQAAfwAQAAAAAAAAAAAAAABACAAAAAAAAAAACQAAAABiAAAAfAAAABEA#####wEAAH8AEAAAAAAAAAAAAAAAQAgAAAAAAAAAAAkAAAAAYQAAAHwAAAARAP####8BAAB#ABAAAAAAAAAAAAAAAEAIAAAAAAAAAAAJAAAAAGAAAAB8AAAAEgD#####AQAA#wAGc3ltQTVQAAIAAAAHAAAAfwAAAIEAAACCAAAAgwAAAH0AAAB+AAAAfwAAABMA#####wEAAP8ABnN5bUE1UwAAAAUAAACEAAAABwD#####AQAAAAEABnN5bUE1dAAAAIAQAAAAAAABAAAAAQAAAAEAAAAAAAAAAAAFKEYnJykAAAAPAP####8BAH8AABAAAAEABHZlY0ICAgAAAF0AAABtAAAAABAA#####wAAAIcAAAARAP####8BAH8AABAAAAAAAAAAAAAAAEAIAAAAAAAAAAAJAAAAACMAAACIAAAAEQD#####AQB#AAAQAAAAAAAAAAAAAABACAAAAAAAAAAACQAAAAAkAAAAiAAAABEA#####wEAfwAAEAAAAAAAAAAAAAAAQAgAAAAAAAAAAAkAAAAAJQAAAIgAAAARAP####8BAH8AABAAAAAAAAAAAAAAAEAIAAAAAAAAAAAJAAAAACAAAACIAAAAEQD#####AQB#AAAQAAAAAAAAAAAAAABACAAAAAAAAAAACQAAAAAhAAAAiAAAABEA#####wEAfwAAEAAAAAAAAAAAAAAAQAgAAAAAAAAAAAkAAAAAIgAAAIgAAAARAP####8BAH8AABAAAAAAAAAAAAAAAEAIAAAAAAAAAAAJAAAAAE8AAACIAAAAEgD#####AQB#AAAGc3ltVDFQAAIAAAAHAAAAjQAAAI4AAACJAAAAigAAAIsAAACMAAAAjQAAABMA#####wEAfwAABnN5bVQxUwAAAAUAAACQAAAABwD#####AQB#AAEABnN5bVQxdAAAAI8QAAAAAAABAAAAAQAAAAEAAAAAAAAAAAAEKEYnKQAAAAIA#####wEAAAAAEAAAAAAAAAAAAAAAQAgAAAAAAAAAAAkAAUB9UAAAAAAAQGp8KPXCj1wAAAACAP####8BAAAAABAAAAAAAAAAAAAAAEAIAAAAAAAAAAAJAAFAeHAAAAAAAEBq#Cj1wo9cAAAABQD#####Af8AAAAQAAABAAJjTwEBAAAAlAAAAJMAAAAEAP####8B#wAAABAAAUMAAAAAAAAAAABACAAAAAAAAAABTwkAAT#eMgZhBzfmAAAAlQAAAA8A#####wEAAH8AEAAAAQAEdmVjTwICAAAAbQAAAJYAAAAAEAD#####AAAAlwAAABEA#####wEAAH8AEAAAAAAAAAAAAAAAQAgAAAAAAAAAAAkAAAAAjQAAAJgAAAARAP####8BAAB#ABAAAAAAAAAAAAAAAEAIAAAAAAAAAAAJAAAAAI4AAACYAAAAEQD#####AQAAfwAQAAAAAAAAAAAAAABACAAAAAAAAAAACQAAAACJAAAAmAAAABEA#####wEAAH8AEAAAAAAAAAAAAAAAQAgAAAAAAAAAAAkAAAAAigAAAJgAAAARAP####8BAAB#ABAAAAAAAAAAAAAAAEAIAAAAAAAAAAAJAAAAAIsAAACYAAAAEQD#####AQAAfwAQAAAAAAAAAAAAAABACAAAAAAAAAAACQAAAACPAAAAmAAAABEA#####wEAAH8AEAAAAAAAAAAAAAAAQAgAAAAAAAAAAAkAAAAAjAAAAJgAAAASAP####8BAAD#AAZzeW1UMlAAAgAAAAcAAACbAAAAmgAAAJkAAACfAAAAnQAAAJwAAACbAAAAEwD#####AQAA#wAGc3ltVDJTAAAABQAAAKAAAAAHAP####8BAAB#AQAGc3ltVDJ0AAAAnhAAAAAAAAEAAAABAAAAAQAAAAAAAAAAAAUoRicnKQAAAA0A#####wH#AAAABHJlcE8ABAAAAJYAAAAUAAAACAAAAAACAP####8BAAAAABAAAUQAAAAAAAAAAABACAAAAAAAAAABUwkAAEBzAAAAAAAAQGZ8KPXCj1wAAAAWAP####8AAACkAAAAEQD#####AQAAfwAQAAAAAAAAAAAAAABACAAAAAAAAAAACQAAAACNAAAApQAAABEA#####wEAAH8AEAAAAAAAAAAAAAAAQAgAAAAAAAAAAAkAAAAAiQAAAKUAAAARAP####8BAAB#ABAAAAAAAAAAAAAAAEAIAAAAAAAAAAAJAAAAAI4AAAClAAAAEQD#####AQAAfwAQAAAAAAAAAAAAAABACAAAAAAAAAAACQAAAACMAAAApQAAABEA#####wEAAH8AEAAAAAAAAAAAAAAAQAgAAAAAAAAAAAkAAAAAjwAAAKUAAAARAP####8BAAB#ABAAAAAAAAAAAAAAAEAIAAAAAAAAAAAJAAAAAIsAAAClAAAAEQD#####AQAAfwAQAAAAAAAAAAAAAABACAAAAAAAAAAACQAAAACKAAAApQAAABIA#####wEAAP8ABnN5bUM0UAACAAAABwAAAKkAAACrAAAArAAAAKcAAACoAAAApgAAAKkAAAATAP####8BAAD#AAZzeW1DNFMAAAAFAAAArQAAAAcA#####wEAAH8BAAZzeW1DNHQAAACqEAAAAAAAAQAAAAEAAAABAAAAAAAAAAAABShGJycpAAAACgD#####AQAAAAAQAAABAAABAgAAAF0AAABtAAAAAgD#####AQAAAAAQAAAAAAAAAAAAAABACAAAAAAAAAAACQABQHRgAAAAAABAcV4UeuFHrgAAAAwA#####wEAAAAAEAAEKGQ2KQEAAmQ2AAI#7MzMzMzMzQAAALEAAACwAAAAFQD#####AAAAsgAAABEA#####wEAAH8AEAAAAAAAAAAAAAAAQAgAAAAAAAAAAAkAAAAAiQAAALMAAAARAP####8BAAB#ABAAAAAAAAAAAAAAAEAIAAAAAAAAAAAJAAAAAI0AAACzAAAAEQD#####AQAAfwAQAAAAAAAAAAAAAABACAAAAAAAAAAACQAAAACMAAAAswAAABEA#####wEAAH8AEAAAAAAAAAAAAAAAQAgAAAAAAAAAAAkAAAAAjwAAALMAAAARAP####8BAAB#ABAAAAAAAAAAAAAAAEAIAAAAAAAAAAAJAAAAAIsAAACzAAAAEQD#####AQAAfwAQAAAAAAAAAAAAAABACAAAAAAAAAAACQAAAACKAAAAswAAABEA#####wEAAH8AEAAAAAAAAAAAAAAAQAgAAAAAAAAAAAkAAAAAjgAAALMAAAASAP####8BAAB#AAZzeW1BNlAAAgAAAAcAAAC1AAAAugAAALQAAAC5AAAAuAAAALYAAAC1AAAAEwD#####AQAAfwAGc3ltQTZTAAAABQAAALsAAAAHAP####8BAAB#AQAGc3ltQTZ0AAAAtxAAAAAAAAEAAAABAAAAAQAAAAAAAAAAAAUoRicnKQAAAAUA#####wF#AH8AEAAAAQADdmkxAAIAAAAjAAAADgAAAAUA#####wF#AH8AEAAAAQADdmkyAAIAAAAOAAAAQAAAAAUA#####wF#AH8AEAAAAQADdmkzAAIAAAAhAAAADgAAAAUA#####wF#AH8AEAAAAQADdmk0AAIAAAAOAAAAPAAAAAYA#####wF#AH8AEAAAAAAAAAAAAAAAQAgAAAAAAAAAAAUAAAAAIwAAAA4AAAAGAP####8BfwB#ABAAAAAAAAAAAAAAAEAIAAAAAAAAAAAFAAAAAA4AAABAAAAABgD#####AX8AfwAQAAAAAAAAAAAAAABACAAAAAAAAAAABQAAAAAhAAAADgAAAAYA#####wF#AH8AEAAAAAAAAAAAAAAAQAgAAAAAAAAAAAUAAAAADgAAADwAAAANAP####8BfwB#AAN2aTUAAgAAAMQAAAABP8mZmZmZmZoAAAAADQD#####AX8AfwADdmk2AAIAAADFAAAAAT#JmZmZmZmaAAAAAA0A#####wF#AH8AAAACAAAAwgAAAAE#0zMzMzMzMwD#####AAAAAQAJQ1JvdGF0aW9uAP####8AAADCAAAAAUBGgAAAAAAAAAAAEQD#####AX8AfwAQAAAAAAAAAAAAAABACAAAAAAAAAAABQAAAAAOAAAAyQAAAAUA#####wF#AH8AEAAAAQAAAAIAAADCAAAAyv####8AAAABABBDSW50RHJvaXRlQ2VyY2xlAP####8AAADLAAAAyP####8AAAABABBDUG9pbnRMaWVCaXBvaW50AP####8BfwB#ABAAAAAAAAAAAAAAAEAIAAAAAAAAAAAFAAEAAADMAAAAGQD#####AH8AfwAQAAAAAAAAAAAAAABACAAAAAAAAAAABQACAAAAzAAAABYA#####wAAAMIAAAARAP####8BfwB#ABAAAAAAAAAAAAAAAEAIAAAAAAAAAAAFAAAAAM0AAADPAAAABQD#####AX8AfwAQAAABAAN2aTcAAgAAANAAAADNAAAADwD#####AX8AfwAQAAABAAAAAgAAAMIAAADDAAAAABAA#####wAAANIAAAARAP####8BfwB#ABAAAAAAAAAAAAAAAEAIAAAAAAAAAAAFAAAAANAAAADTAAAAEQD#####AX8AfwAQAAAAAAAAAAAAAABACAAAAAAAAAAABQAAAADNAAAA0wAAAAUA#####wF#AH8AEAAAAQADdmk4AAIAAADUAAAA1QAAAA8A#####wF#AH8AEAAAAQADdnUxAAIAAAAhAAAANwAAAAAPAP####8BfwB#ABAAAAEAA3Z1MgACAAAAJAAAADQA#####wAAAAEADENBcmNEZUNlcmNsZQD#####AX8AfwAEdm8xMAICAAAADgAAACMAAABFAAAACgD#####AX8AfwAQAAABAAACAgAAAA4AAAAjAAAADQD#####AX8AfwAAAgIAAAAjAAAAAT#wAAAAAAAAAAAAABgA#####wAAANoAAADbAAAAGQD#####AX8AfwAQAAAAAAAAAAAAAABACAAAAAAAAAAABQABAAAA3AAAABkA#####wF#AH8AEAAAAAAAAAAAAAAAQAgAAAAAAAAAAAUAAgAAANwAAAAFAP####8BfwB#ABAAAAEAA3ZvOQACAAAA3QAAAA7#####AAAAAQAUQ0ltcGxlbWVudGF0aW9uUHJvdG8A#####wAbTWVzdXJlIGQnYW5nbGUgbm9uIG9yaWVudMOpAAAAAgAAAAMAAAADAAAAIwAAAA4AAABF#####wAAAAEADENCaXNzZWN0cmljZQAAAADgAQAAAAAQAAABAAAAAQAAACMAAAAOAAAARQAAAAQAAAAA4AEAAAAAEAAAAQAABQABQG8lqGWscakAAADh#####wAAAAEAF0NNZXN1cmVBbmdsZUdlb21ldHJpcXVlAQAAAOAAAAAjAAAADgAAAEX#####AAAAAgAXQ01hcnF1ZUFuZ2xlR2VvbWV0cmlxdWUBAAAA4AF#AH8AA3ZvOAADAAAAAUAzzIqZr1RTAAAAIwAAAA4AAABF#####wAAAAEAD0NWYWxldXJBZmZpY2hlZQEAAADgAX8AfwBAEAAAAAAAAMAQAAAAAAAAAAN2bzcAAADiDwAAAAAAAQAAAAEAAAABAAAAAAAAAAAAAAACwrAAAAAA4wAAAAoA#####wF#AH8AEAAAAQAAAAIAAAAOAAAARQAAAA0A#####wF#AH8AAAACAAAARQAAAAE#8AAAAAAAAAAAAAAYAP####8AAADmAAAA5wAAABkA#####wF#AH8AEAAAAAAAAAAAAAAAQAgAAAAAAAAAAAUAAgAAAOgAAAAZAP####8BfwB#ABAAAAAAAAAAAAAAAEAIAAAAAAAAAAAFAAEAAADoAAAABQD#####AX8AfwAQAAABAAN2bzYAAgAAAA4AAADqAAAACgD#####AX8AAAAQAAABAAAAAgAAAA4AAAAgAAAADQD#####AX8AAAAAAAIAAAAgAAAAAT#wAAAAAAAAAAAAABgA#####wAAAOwAAADtAAAAGQD#####AX8AAAAQAAAAAAAAAAAAAABACAAAAAAAAAAABQACAAAA7gAAABkA#####wF#AAAAEAABSAAAAAAAAAAAAEAIAAAAAAAAAAAFAAEAAADuAAAABQD#####Af8A#wAQAAABAAN2bzUAAgAAAPAAAAAOAAAADQD#####AX8AAAAAAAIAAABGAAAAAT#wAAAAAAAAAAAAAAoA#####wF#AAAAEAAAAQAAAAIAAAAOAAAARgAAABoA#####wH#AP8AA3ZvNAICAAAADgAAACAAAABIAAAACgD#####AX8AAAAQAAABAAACAgAAAA4AAABIAAAADQD#####AX8AAAAAAgIAAABIAAAAAT#wAAAAAAAAAAAAABgA#####wAAAPUAAAD2AAAAGQD#####AX8AAAAQAAAAAAAAAAAAAABACAAAAAAAAAAABQACAAAA9wAAABkA#####wF#AAAAEAABSQAAAAAAAAAAAEAIAAAAAAAAAAAFAAEAAAD3AAAABQD#####Af8A#wAQAAABAAN2bzMAAgAAAA4AAAD5AAAAGwD#####ABtNZXN1cmUgZCdhbmdsZSBub24gb3JpZW50w6kAAAACAAAAAwAAAAMAAADwAAAADgAAAPkAAAAcAAAAAPsBAAAAABAAAAEAAAABAAAA8AAAAA4AAAD5AAAABAAAAAD7AQAAAAAQAAABAAAFAAFAbyWoZaxxqQAAAPwAAAAdAQAAAPsAAADwAAAADgAAAPkAAAAeAQAAAPsB#wD#AAN2bzIAAwAAAAFASMoDJYQJOgAAAPAAAAAOAAAA+QAAAB8BAAAA+wH#AP8Av#AAAAAAAADAQgAAAAAAAAADdm8xAAAA#Q8AAAAAAAEAAAABAAAAAQAAAAAAAAAAAAAAAsKwAAAAAP4AAAAPAP####8BfwB#ABAAAAEAA3ZyMQACAAAAJQAAAHMAAAAADwD#####AX8AfwAQAAABAAN2cjIAAgAAACMAAABvAP####8AAAABAAtDTWVkaWF0cmljZQD#####AX8AfwAQAAABAAR2czExAAIAAAAjAAAAfQAAAAUA#####wF#AH8AEAAAAQADdnM2AAIAAAAkAAAAfgAAAAUA#####wF#AH8AEAAAAQADdnM1AAIAAAAjAAAAff####8AAAABABBDSW50RHJvaXRlRHJvaXRlAP####8BfwB#ABAAAAAAAAAAAAAAAEAIAAAAAAAAAAAFAAAAAQMAAAEEAAAAIQD#####AX8AfwAQAAAAAAAAAAAAAABACAAAAAAAAAAABQAAAAEDAAABBQAAAAUA#####wF#AH8AEAAAAQAAAAIAAAAkAAABBgAAAAUA#####wF#AH8AEAAAAQAAAAIAAAEGAAAAfgAAAAYA#####wF#AH8AEAAAAAAAAAAAAAAAQAgAAAAAAAAAAAUAAAAAJAAAAQYAAAAGAP####8BfwB#ABAAAAAAAAAAAAAAAEAIAAAAAAAAAAAFAAAAAH4AAAEGAAAADQD#####AX8AfwADdnM3AAQAAAEKAAAAAT+5mZmZmZmaAAAAAA0A#####wF#AH8AA3ZzOAAEAAABCwAAAAE#uZmZmZmZmgAAAAAFAP####8BfwB#ABAAAAEAAAACAAABBwAAACMAAAAFAP####8BfwB#ABAAAAEAAAACAAABBwAAAH0AAAAGAP####8BfwB#ABAAAAAAAAAAAAAAAEAIAAAAAAAAAAAFAAAAACMAAAEHAAAABgD#####AX8AfwAQAAAAAAAAAAAAAABACAAAAAAAAAAABQAAAAB9AAABBwAAAA0A#####wF#AH8AA3ZzOQACAAABEQAAAAE#wzMzMzMzMwAAAAANAP####8BfwB#AAR2czEwAAIAAAEQAAAAAT#DMzMzMzMzAAAAAA0A#####wF#fwAAAAACAAABBwAAAAE#yZmZmZmZmgAAAAAKAP####8Bf38AABAAAAEAAAACAAAAfQAAACMAAAAYAP####8AAAEVAAABFAAAABkA#####wF#fwAAEAAAAAAAAAAAAAAAQAgAAAAAAAAAAAUAAQAAARYAAAAZAP####8Bf38AABAAAAAAAAAAAAAAAEAIAAAAAAAAAAAFAAIAAAEWAAAAGAD#####AAABAwAAARQAAAAZAP####8Bf38AABAAAAAAAAAAAAAAAEAIAAAAAAAAAAAFAAEAAAEZAAAAGQD#####AX9#AAAQAAAAAAAAAAAAAABACAAAAAAAAAAABQACAAABGQAAAAsA#####wF#fwAAEAAAAQAAAAIAAAEXAAABAwAAAAsA#####wF#fwAAEAAAAQAAAAIAAAEbAAABFQAAACEA#####wF#fwAAEAAAAAAAAAAAAAAAQAgAAAAAAAAAAAUAAAABHAAAAR0AAAAPAP####8Bf38AABAAAAEAAAACAAABBwAAAQYAAAAAEAD#####AAABHwAAABEA#####wF#fwAAEAAAAAAAAAAAAAAAQAgAAAAAAAAAAAUAAAABFwAAASAAAAARAP####8Bf38AABAAAAAAAAAAAAAAAEAIAAAAAAAAAAAFAAAAAR4AAAEgAAAAEQD#####AX9#AAAQAAAAAAAAAAAAAABACAAAAAAAAAAABQAAAAEbAAABIAAAAAUA#####wF#fwAAEAAAAQADdnM0AAIAAAEXAAABHgAAAAUA#####wF#fwAAEAAAAQADdnMzAAIAAAEeAAABGwAAAAUA#####wF#fwAAEAAAAQADdnMyAAIAAAEhAAABIgAAAAUA#####wF#fwAAEAAAAQADdnMxAAIAAAEiAAABIwAAAA8A#####wF#AH8AEAAAAQADdnAxAAIAAAAkAAAAnAAAAAAPAP####8BfwB#ABAAAAEAA3ZwMgACAAAAIQAAAJkAAAAABQD#####AX8AfwAQAAABAAN2ejYAAgAAACEAAACmAAAABQD#####AX8AfwAQAAABAAN2ejUAAgAAACMAAACnAAAAIQD#####AX8AfwAQAAAAAAAAAAAAAABACAAAAAAAAAAABQAAAAEqAAABKwAAAAYA#####wF#AH8AEAAAAAAAAAAAAAAAQAgAAAAAAAAAAAUAAAAAIQAAASwAAAAGAP####8BfwB#ABAAAAAAAAAAAAAAAEAIAAAAAAAAAAAFAAAAACMAAAEsAAAABgD#####AX8AfwAQAAAAAAAAAAAAAABACAAAAAAAAAAABQAAAAEsAAAApwAAAAYA#####wF#AH8AEAAAAAAAAAAAAAAAQAgAAAAAAAAAAAUAAAABLAAAAKYAAAANAP####8BfwB#AAN2ejQABQAAAS0AAAABP7mZmZmZmZoAAAAADQD#####AX8AfwADdnozAAUAAAEwAAAAAT+5mZmZmZmaAAAAAA0A#####wF#AH8AA3Z6MgABAAABLgAAAAE#wzMzMzMzMwAAAAANAP####8BfwB#AAN2ejEAAQAAAS8AAAABP8MzMzMzMzMAAAAAIAD#####AX8AfwAQAAABAAR2dzExAAIAAAAjAAAAtAAAAAYA#####wF#AH8AEAAAAAAAAAAAAAAAQAgAAAAAAAAAAAUAAAAAIwAAALQAAAAGAP####8BfwB#ABAAAAAAAAAAAAAAAEAIAAAAAAAAAAAFAAAAACQAAAC5AAAABgD#####AX8AfwAQAAAAAAAAAAAAAABACAAAAAAAAAAABQAAAAAjAAABNgAAAAYA#####wF#AH8AEAAAAAAAAAAAAAAAQAgAAAAAAAAAAAUAAAAAJAAAATcAAAAGAP####8BfwB#ABAAAAAAAAAAAAAAAEAIAAAAAAAAAAAFAAAAATcAAAC5AAAABgD#####AX8AfwAQAAAAAAAAAAAAAABACAAAAAAAAAAABQAAAAE2AAAAtAAAAA0A#####wF#AH8ABHZ3MTAAAgAAAToAAAABP8mZmZmZmZoAAAAADQD#####AX8AfwADdnc3AAIAAAE5AAAAAT#JmZmZmZmaAAAAAA0A#####wF#AH8AA3Z3OQAEAAABOAAAAAE#uZmZmZmZmgAAAAANAP####8BfwB#AAN2dzgABAAAATsAAAABP7mZmZmZmZoAAAAADQD#####AX9#AAAAAAQAAAE2AAAAAT#JmZmZmZmaAAAAAAUA#####wF#AH8AEAAAAQADdnc2AAIAAAAjAAAAtAAAAAUA#####wF#AH8AEAAAAQADdnc1AAIAAAAkAAAAuQAAABgA#####wAAAUEAAAFAAAAAGQD#####AX9#AAAQAAAAAAAAAAAAAABACAAAAAAAAAAABQABAAABQwAAABkA#####wF#fwAAEAAAAAAAAAAAAAAAQAgAAAAAAAAAAAUAAgAAAUMAAAAYAP####8AAAE1AAABQAAAABkA#####wF#fwAAEAAAAAAAAAAAAAAAQAgAAAAAAAAAAAUAAQAAAUYAAAAZAP####8Bf38AABAAAAAAAAAAAAAAAEAIAAAAAAAAAAAFAAIAAAFGAAAACwD#####AX9#AAAQAAABAAAAAgAAAUgAAAFBAAAACwD#####AX9#AAAQAAABAAAAAgAAAUUAAAE1AAAAIQD#####AX9#AAAQAAAAAAAAAAAAAABACAAAAAAAAAAABQAAAAFJAAABSgAAAAUA#####wF#fwAAEAAAAQADdnc0AAIAAAFFAAABSwAAAAUA#####wF#fwAAEAAAAQADdnczAAIAAAFLAAABSAAAAA8A#####wF#fwAAEAAAAQAAAAIAAAE2AAABNwAAAAAQAP####8AAAFOAAAAEQD#####AX9#AAAQAAAAAAAAAAAAAABACAAAAAAAAAAABQAAAAFFAAABTwAAABEA#####wF#fwAAEAAAAAAAAAAAAAAAQAgAAAAAAAAAAAUAAAABSwAAAU8AAAARAP####8Bf38AABAAAAAAAAAAAAAAAEAIAAAAAAAAAAAFAAAAAUgAAAFPAAAABQD#####AX9#AAAQAAABAAN2dzIAAgAAAVAAAAFRAAAABQD#####AX9#AAAQAAABAAN2dzEAAgAAAVEAAAFSAAAACQD#####AAJCQgADMC41AAAAAT#gAAAAAAAAAAAADQD#####AQAAAAACZ1IADAAAAF0AAAAUAAABVQAAAAANAP####8BAAAAAAJnTwAMAAAAlgAAABQAAAFVAAAAAA0A#####wEAAAAAAmdYAAwAAABtAAAAFAAAAVUAAAAADQD#####AQAAAAACZ0EADAAAAA8AAAAUAAABVQAAAAANAP####8BAAAAAAJnTQAMAAAAHAAAABQAAAFVAAAAAAf##########w=='
    const kk = j3pCreeSVG(stor.lesdiv.figure, { id: svgId, width: 615, height: 400 })
    kk.style.border = 'solid black 1px'
    stor.mtgAppLecteur.removeAllDoc()
    stor.mtgAppLecteur.addDoc(svgId, txtfig, true)
    stor.mtgAppLecteur.calculateAndDisplayAll(true)
    faisMv('M')
  }
  function poseQuestion () {
    let adire
    stor.disab = false
    stor.lesdiv.bout2.style.display = 'none'
    stor.aco = []
    switch (stor.exo) {
      case 'Sym_Ax_perp':
        adire = 'Les droites (d1) et (d3) sont perpendiculaires. <br> On compose la symétrie d’axe (d1) et la symétrie d’axe (d3).'
        stor.mtgAppLecteur.setVisible(svgId, '#d1', true, true)
        stor.mtgAppLecteur.setVisible(svgId, '#d3', true, true)
        faisMv('A')
        stor.att = 'symétrie centrale.'
        stor.comp1 = ' (symétrique de (F) par rapport à la droite (d1))'
        stor.comp2 = " (symétrique de (F') par rapport à la droite (d3))"
        stor.im1 = ['#symA1P', '#symA1S', '#symA1t']
        stor.im2 = ['#symA3P', '#symA3S', '#symA3t']
        stor.aco = ['#vi1', '#vi2', '#vi3', '#vi4', '#vi5', '#vi6', '#vi7', '#vi8']
        break
      case 'Sym_Ax_para':
        adire = 'Les droites (d1) et (d2) sont parallèles. <br> On compose la symétrie d’axe (d1) et la symétrie d’axe (d2).'
        stor.mtgAppLecteur.setVisible(svgId, '#d1', true, true)
        stor.mtgAppLecteur.setVisible(svgId, '#d2', true, true)
        faisMv('A')
        stor.att = 'translation.'
        stor.comp1 = ' (symétrique de (F) par rapport à la droite (d1))'
        stor.comp2 = " (symétrique de (F') par rapport à la droite (d2))"
        stor.im1 = ['#symA1P', '#symA1S', '#symA1t']
        stor.im2 = ['#symA2P', '#symA2S', '#symA2t']
        stor.aco = ['#vu1', '#vu2']
        break
      case 'Sym_Axe_sec':
        adire = 'Les droites (d1) et (d4) sont simplement sécantes. <br> On compose la symétrie d’axe (d1) et la symétrie d’axe (d4).'
        stor.mtgAppLecteur.setVisible(svgId, '#d1', true, true)
        stor.mtgAppLecteur.setVisible(svgId, '#d4', true, true)
        faisMv('A')
        stor.att = 'rotation.'
        stor.comp1 = ' (symétrique de (F) par rapport à la droite (d1))'
        stor.comp2 = " (symétrique de (F') par rapport à la droite (d4))"
        stor.im1 = ['#symA1P', '#symA1S', '#symA1t']
        stor.im2 = ['#symA4P', '#symA4S', '#symA4t']
        stor.aco = ['#vo1', '#vo2', '#vo3', '#vo4', '#vo5', '#vo6', '#vo7', '#vo8', '#vo9', '#vo10']
        break
      case 'Sym_Ce':
        adire = 'On compose la symétrie de centre A et la symétrie de centre B.'
        faisMv('R')
        faisMv('X')
        stor.att = 'translation.'
        stor.comp1 = ' (symétrique de (F) par rapport au point A)'
        stor.comp2 = " (symétrique de (F') par rapport au point B)"
        stor.im1 = ['#symC2P', '#symC2S', '#symC2t']
        stor.im2 = ['#symC3P', '#symC3S', '#symC3t']
        stor.aco = ['#vr1', '#vr2']
        break
      case 'Sym_Ce_Sym_Ax':
        adire = 'Le point A appartient à la droite (d5). <br> On compose la symétrie de centre A et la symétrie d’axe (d5).'
        faisMv('R')
        stor.mtgAppLecteur.setVisible(svgId, '#d5', true, true)
        stor.att = 'symétrie axiale.'
        stor.comp1 = ' (symétrique de (F) par rapport au point A)'
        stor.comp2 = " (symétrique de (F') par rapport à la droite (d5))"
        stor.im1 = ['#symC2P', '#symC2S', '#symC2t']
        stor.im2 = ['#symA5P', '#symA5S', '#symA5t']
        stor.aco = ['#vs1', '#vs2', '#vs3', '#vs4', '#vs5', '#vs6', '#vs7', '#vs8', '#vs9', '#vs10', '#vs11']
        break
      case 'Trans':
        adire = 'Les points A , B et C sont trois points distincts. <br> On compose la translation qui envoie A en B et la translation qui envoie B en C.'
        faisMv('R')
        faisMv('X')
        faisMv('O')
        stor.mtgAppLecteur.setVisible(svgId, '#vecB', true, true)
        stor.mtgAppLecteur.setVisible(svgId, '#vecO', true, true)
        stor.att = 'translation.'
        stor.comp1 = ' (image de (F) par la translation qui envoie A en B)'
        stor.comp2 = " (image de (F') par la translation qui envoie B en C)"
        stor.im1 = ['#symT1P', '#symT1S', '#symT1t']
        stor.im2 = ['#symT2P', '#symT2S', '#symT2t']
        stor.aco = ['#vp1', '#vp2']
        break
      case 'Trans_Sym_Ce':
        adire = 'Les points A , B et D sont trois points distincts. <br> On compose la translation qui envoie A en B et la symétrie de centre D.'
        faisMv('R')
        faisMv('X')
        stor.mtgAppLecteur.setVisible(svgId, '#vecB', true, true)
        stor.mtgAppLecteur.setVisible(svgId, '#S', true, true)
        stor.att = 'symétrie centrale.'
        stor.comp1 = ' (image de (F) par la translation qui envoie A en B)'
        stor.comp2 = " (image de (F') par la symétrie de centre D)"
        stor.im1 = ['#symT1P', '#symT1S', '#symT1t']
        stor.im2 = ['#symC4P', '#symC4S', '#symC4t']
        stor.aco = ['#vz1', '#vz2', '#vz3', '#vz4', '#vz5', '#vz6']
        break
      case 'Trans_Sym_Ax_perp':
        adire = 'Les points A et B sont deux points distincts, la droite (d6) est perpendiculaire à la droite (AB). <br> On compose la translation qui envoie A en B et la symétrie d’axe (d6).'
        faisMv('R')
        faisMv('X')
        stor.mtgAppLecteur.setVisible(svgId, '#vecB', true, true)
        stor.mtgAppLecteur.setVisible(svgId, '#d6', true, true)
        stor.att = 'symétrie axiale.'
        stor.comp1 = ' (image de (F) par la translation qui envoie A en B)'
        stor.comp2 = " (image de (F') par la symétrie d’axe (d6))"
        stor.im1 = ['#symT1P', '#symT1S', '#symT1t']
        stor.im2 = ['#symA6P', '#symA6S', '#symA6t']
        stor.aco = ['#vw1', '#vw2', '#vw3', '#vw4', '#vw5', '#vw6', '#vw7', '#vw8', '#vw9', '#vw10', '#vw11']
        break
      default: j3pShowError('Pas encore fait')
    }
    stor.lebou1 = j3pAjouteBouton(stor.lesdiv.bout1, makAf(stor.im1[0], stor.im1[1], stor.im1[2], 1), { className: '', value: "Afficher (F') " + stor.comp1 })
    stor.lebou2 = j3pAjouteBouton(stor.lesdiv.bout2, makAf(stor.im2[0], stor.im2[1], stor.im2[2], 2), { className: '', value: "Afficher (F'') " + stor.comp2 })
    stor.t1 = stor.t2 = false
    const uio = addDefaultTable(stor.lesdiv.consigneG, 1, 3)
    modif(uio)
    uio[0][1].style.width = '10px'
    j3pAffiche(uio[0][0], null, adire)
    const yy = addDefaultTable(stor.lesdiv.travail, 1, 2)
    modif(yy)
    j3pAffiche(yy[0][0], null, 'La transformation composée est une&nbsp;')
    stor.larep = ListeDeroulante.create(yy[0][1], ['Choisir', 'symétrie axiale.', 'symétrie centrale.', 'translation.', 'rotation.'], { choix0: false })
    stor.bullaide = new BulleAide(uio[0][2], 'Déplace le(s) point(s) rouge(s) pour modifier la figure')
  }

  function faisMv (L) {
    stor.mtgAppLecteur.setVisible(svgId, '#' + L, true, true)
    stor.mtgAppLecteur.setVisible(svgId, '#g' + L, true, true)
    stor.mtgAppLecteur.setColor(svgId, '#g' + L, 0, 0, 0, true, 0)
    stor.mtgAppLecteur.addEventListener(svgId, '#g' + L, 'mouseover', () => {
      stor.mtgAppLecteur.setVisible(svgId, '#c' + L, true, true)
      stor.mtgAppLecteur.setVisible(svgId, '#rep' + L, true, true)
    })
    stor.mtgAppLecteur.addEventListener(svgId, '#g' + L, 'mouseout', () => {
      stor.mtgAppLecteur.setVisible(svgId, '#c' + L, false, true)
      stor.mtgAppLecteur.setVisible(svgId, '#rep' + L, false, true)
    })
  }
  function makAf (a, b, c, n) {
    return function () {
      if (!stor.disab) stor.lesdiv.bout2.style.display = ''
      let viz
      if (n === 1) {
        stor.t1 = !stor.t1
        viz = stor.t1
        stor.lebou1.value = (viz ? "Afficher (F') " : "Cacher (F') ") + stor.comp1
      } else {
        stor.t2 = !stor.t2
        viz = stor.t2
        stor.lebou2.value = (viz ? "Afficher (F'') " : "Cacher (F'') ") + stor.comp2
      }
      stor.mtgAppLecteur.setVisible(svgId, a, viz, true)
      stor.mtgAppLecteur.setVisible(svgId, b, viz, true)
      stor.mtgAppLecteur.setVisible(svgId, c, viz, true)
    }
  }
  function yaReponse () {
    if (!stor.larep.changed) {
      stor.larep.focus()
      return false
    }
    return true
  }
  function isRepOk () {
    return stor.larep.reponse === stor.att
  } // isRepOk
  // utilise this
  function desactiveAll () {
    stor.larep.disable()
    stor.disab = true
    stor.lesdiv.bout2.style.display = 'none'
    stor.mtgAppLecteur.setVisible(svgId, stor.im2[0], true, true)
    stor.mtgAppLecteur.setVisible(svgId, stor.im2[1], true, true)
    stor.mtgAppLecteur.setVisible(svgId, stor.im2[2], true, true)
    for (let i = 0; i < stor.aco.length; i++) {
      stor.mtgAppLecteur.setVisible(svgId, stor.aco[i], true, true)
    }
  } // desactiveAll
  function passeToutVert () {
    stor.larep.corrige(true)
  } // passeToutVert

  function affCorrFaux (isFin) {
    stor.larep.corrige(false)
    if (isFin) {
      stor.larep.barre()
      desactiveAll()
      j3pAffiche(stor.lesdiv.solution, null, 'La transformation composée est une ' + stor.att)
    }
  } // affCorrFaux

  // appelé par initSection ou directement dans le case enonce si on est pas au début de la section
  function enonceMain () {
    creeLesDiv()
    stor.lesdiv.travail.classList.add('travail')
    stor.lesdiv.consigneG.classList.add('enonce')
    stor.exo = faisChoixExos()
    faiFigure()
    poseQuestion()
    stor.lesdiv.figure.classList.add('enonce')
    me.zonesElts.MG.classList.add('fond')
    stor.lesdiv.bout1.classList.add('travail')
    stor.lesdiv.bout2.classList.add('travail')
    me.finEnonce()
  }

  switch (this.etat) {
    case 'enonce':
      // code exécuté au lancement de la section
      if (me.debutDeLaSection) {
        initSection()
        // initSection appellera enonceMain quand il aura fini ses chargements
      } else {
        this.videLesZones()
        enonceMain()
      }
      break // case "enonce":

    case 'correction':
      if (!stor.lesdiv) throw Error('Initialisation de l’exercice incorrecte')
      // On teste si une réponse a été saisie
      stor.yaexplik = false
      stor.yaco = false
      stor.lesdiv.explications.innerHTML = ''
      stor.lesdiv.solution.innerHTML = ''

      if (yaReponse()) {
        // Bonne réponse
        if (isRepOk()) {
          this._stopTimer()
          stor.lesdiv.correction.style.color = this.styles.cbien
          stor.lesdiv.correction.innerHTML = cBien

          passeToutVert()
          desactiveAll()

          this.score++
          this.typederreurs[0]++
          this.cacheBoutonValider()
          this.etat = 'navigation'
          this.sectionCourante()
        } else {
          // Pas de bonne réponse
          stor.lesdiv.correction.style.color = this.styles.cfaux
          stor.lesdiv.correction.innerHTML = cFaux
          if (this.isElapsed) {
            // A cause de la limite de temps :
            this._stopTimer()

            stor.lesdiv.correction.innerHTML += tempsDepasse + '<BR>'
            this.typederreurs[10]++
            this.cacheBoutonValider()

            affCorrFaux(true)
            stor.yaco = true

            this.etat = 'navigation'
            this.sectionCourante()
          } else {
            // Réponse fausse :
            if (this.essaiCourant < ds.nbchances) {
              stor.lesdiv.correction.innerHTML += '<br>' + essaieEncore

              affCorrFaux(false)

              // affCorrFaux.call(this, false)
              this.typederreurs[1]++
            } else {
              // Erreur au nème essai
              this._stopTimer()
              this.cacheBoutonValider()

              affCorrFaux(true)
              stor.yaco = true

              if (stor.encours === 'compare') {
                this.score = j3pArrondi(this.score + 0.1, 1)
              }
              if (stor.encours === 'calcul') {
                this.score = j3pArrondi(this.score + 0.4, 1)
              }
              if (stor.encours === 'concl') {
                this.score = j3pArrondi(this.score + 0.7, 1)
              }

              this.typederreurs[2]++
              this.etat = 'navigation'
              this.sectionCourante()
            }
          }
        }
      } else {
        // pas de réponse
        stor.lesdiv.correction.style.color = this.styles.cfaux
        stor.lesdiv.correction.innerHTML = reponseIncomplete
        this.afficheBoutonValider()
      }

      if (stor.yaco) {
        stor.lesdiv.solution.classList.add('correction')
      }
      if (stor.yaexplik) {
        stor.lesdiv.explications.classList.add('explique')
      }

      // Obligatoire
      this.finCorrection()
      break // case "correction":

    case 'navigation':
      if (!this.sectionTerminee()) {
        this.etat = 'enonce'
      }
      this.finNavigation()
      break // case "navigation":
  }
}
