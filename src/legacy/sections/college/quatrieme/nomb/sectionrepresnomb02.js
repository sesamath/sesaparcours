import { j3pAddContent, j3pAddElt, j3pAjouteBouton, j3pArrondi, j3pEmpty, j3pGetRandomInt, j3pNombreBienEcrit, j3pShuffle } from 'src/legacy/core/functions'
import { addDefaultTable } from 'src/legacy/themes/table'
import { j3pAfficheCroixFenetres, j3pCreeFenetres, j3pToggleFenetres } from 'src/legacy/core/functionsJqDialog'
import Memory from 'src/legacy/outils/memory/Memory'
import { j3pAffiche } from 'src/lib/mathquill/functions'
import textesGeneriques from 'src/lib/core/textes'

const { cBien, tempsDepasse } = textesGeneriques

export const params = {
  outils: ['mathquill', 'calculatrice'],
  /**
   * Ce qui est paramétrable dans l’éditeur de graphe
   * @type {LegacySectionParamsParametres}
   */
  parametres: [
    ['nbrepetitions', 3, 'entier', 'Nombre de répétitions de la section'],
    ['limite', 20, 'entier', 'Temps disponible par répétition, en secondes'],
    ['entier', true, 'boolean', '<u>true</u>: Le nombre peut être entier'],
    ['decimal', true, 'boolean', '<u>true</u>: Le nombre peut être proposé en écriture décimale'],
    ['fraction', true, 'boolean', '<u>true</u>: Le nombre peut être proposé en écriture factionnaire'],
    ['racine', true, 'boolean', '<u>true</u>: Le nombre peut être proposé sous la forme d’un radical.'],
    ['negatif', true, 'boolean', '<u>true</u>: Le nombre peut être négatif.'],
    ['nbcouples', 3, 'entier', 'Nombre de couples de cartes, entre 3 et 6'],
    ['difficulte', 2, 'entier', 'Un nombre entre 1 et 2'],
    ['theme', 'standard', 'liste', 'Modèle pour l’affichage', ['standard', 'zonesAvecImageDeFond']]
  ]
}

/**
 * section represnomb01
 * @this {Parcours}
 */
export default function main () {
  const me = this
  const ds = me.donneesSection
  const stor = me.storage

  function initSection () {
    // c’est la fin du memory qui valide
    me.validOnEnter = false // ex donneesSection.touche_entree

    // Construction de la page
    me.construitStructurePage('presentation3')

    // les étapes
    stor.tpos = []
    if (ds.entier) stor.tpos.push('entier')
    if (ds.decimal) stor.tpos.push('decimal')
    if (ds.fraction) stor.tpos.push('fraction')
    if (ds.racine) stor.tpos.push('racine')
    if (stor.tpos.length === 0) stor.tpos.push('entier')
    if (ds.nbcouples < 3) ds.nbcouples = 3
    if (ds.nbcouples > 6) ds.nbcouples = 6
    if (ds.difficulte < 1) ds.difficulte = 1
    if (ds.difficulte > 2) ds.difficulte = 2
    stor.exosDistincts = []
    for (const val of stor.tpos) {
      for (const val2 of stor.tpos) {
        stor.exosDistincts.push({ first: val, second: val2 })
      }
    }
    ds.nbchances = 1
    stor.exos = []

    let sbuf = []
    for (let i = 0; i < ds.nbitems; i++) {
      if (sbuf.length === 0) sbuf = j3pShuffle(stor.exosDistincts)
      stor.exos[i] = sbuf.pop()
    }
    stor.exos = j3pShuffle(stor.exos)

    me.typederreurs = [0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0]
    me.afficheTitre('Représentations des nombres - memory')
    enonceMain()
  }

  function faisChoixExos () {
    const exo = stor.exos.pop()
    switch (exo.first) {
      case 'entier':
        switch (exo.second) {
          case 'entier':
            if (ds.difficulte === 2) {
              stor.n1 = j3pGetRandomInt(100, 999)
              stor.afn1 = String(stor.n1)
              stor.afn2 = putUnZero(stor.afn1)
              stor.afn3 = putUnZero(stor.afn2)
              stor.afn4 = putUnZero(stor.afn3)
              stor.afn5 = putUnZero(stor.afn4)
              stor.afn6 = putUnZero(stor.afn5)
            } else {
              let ln = []
              for (let i = 0; i < 1000; i++) ln.push(i)
              ln = j3pShuffle(ln)
              stor.n1 = ln.pop()
              stor.n2 = ln.pop()
              stor.n3 = ln.pop()
              stor.n4 = ln.pop()
              stor.n5 = ln.pop()
              stor.n6 = ln.pop()
              stor.afn1 = String(stor.n1)
              stor.afn2 = String(stor.n2)
              stor.afn3 = String(stor.n3)
              stor.afn4 = String(stor.n4)
              stor.afn5 = String(stor.n5)
              stor.afn6 = String(stor.n6)
            }
            stor.afcon1 = ajZer(stor.afn1)
            stor.afcon2 = ajZer(stor.afn2)
            stor.afcon3 = ajZer(stor.afn3)
            stor.afcon4 = ajZer(stor.afn4)
            stor.afcon5 = ajZer(stor.afn5)
            stor.afcon6 = ajZer(stor.afn6)
            stor.afcon1 = metMath(stor.afcon1)
            stor.afcon2 = metMath(stor.afcon2)
            stor.afcon3 = metMath(stor.afcon3)
            stor.afcon4 = metMath(stor.afcon4)
            stor.afcon5 = metMath(stor.afcon5)
            stor.afcon6 = metMath(stor.afcon6)
            break
          case 'decimal':
            makeDeciEntier()
            stor.afcon1 = metMath(stor.afcon1)
            stor.afcon2 = metMath(stor.afcon2)
            stor.afcon3 = metMath(stor.afcon3)
            stor.afcon4 = metMath(stor.afcon4)
            stor.afcon5 = metMath(stor.afcon5)
            stor.afcon6 = metMath(stor.afcon6)
            break
          case 'fraction':
            makeFracEntier()
            break
          case 'racine':
            makeRacEntier()
        }
        stor.afn1 = metMath(stor.afn1)
        stor.afn2 = metMath(stor.afn2)
        stor.afn3 = metMath(stor.afn3)
        stor.afn4 = metMath(stor.afn4)
        stor.afn5 = metMath(stor.afn5)
        stor.afn6 = metMath(stor.afn6)
        break
      case 'decimal':
        switch (exo.second) {
          case 'entier':
            makeDeciEntier()
            stor.afcon1 = metMath(stor.afcon1)
            stor.afcon2 = metMath(stor.afcon2)
            stor.afcon3 = metMath(stor.afcon3)
            stor.afcon4 = metMath(stor.afcon4)
            stor.afcon5 = metMath(stor.afcon5)
            stor.afcon6 = metMath(stor.afcon6)
            break
          case 'decimal':
            if (ds.difficulte === 1) {
              let ln = []
              for (let i = 1; i < 1000; i++) {
                if (i % 10 !== 0) ln.push(i)
              }
              ln = j3pShuffle(ln)
              stor.n1 = hdiv(ln.pop())
              stor.n2 = hdiv(ln.pop())
              stor.n3 = hdiv(ln.pop())
              stor.n4 = hdiv(ln.pop())
              stor.n5 = hdiv(ln.pop())
              stor.n6 = hdiv(ln.pop())
              stor.afn1 = String(stor.n1).replace('.', ',')
              stor.afn2 = String(stor.n2).replace('.', ',')
              stor.afn3 = String(stor.n3).replace('.', ',')
              stor.afn4 = String(stor.n4).replace('.', ',')
              stor.afn5 = String(stor.n5).replace('.', ',')
              stor.afn6 = String(stor.n6).replace('.', ',')
            } else {
              let ln = []
              for (let i = 101; i < 1000; i++) {
                if (i % 10 !== 0) ln.push(i)
              }
              ln = j3pShuffle(ln)
              stor.n1 = hdiv(ln.pop())
              stor.afn1 = String(stor.n1)
              stor.afn2 = putUnZero(stor.afn1)
              stor.afn3 = putUnZero(stor.afn2)
              stor.afn4 = putUnZero(stor.afn3)
              stor.afn5 = putUnZero(stor.afn4)
              stor.afn6 = putUnZero(stor.afn5)
            }
            stor.afcon1 = ajZerDec2(stor.afn1)
            stor.afcon2 = ajZerDec2(stor.afn2)
            stor.afcon3 = ajZerDec2(stor.afn3)
            stor.afcon4 = ajZerDec2(stor.afn4)
            stor.afcon5 = ajZerDec2(stor.afn5)
            stor.afcon6 = ajZerDec2(stor.afn6)
            stor.afcon1 = metMath(stor.afcon1)
            stor.afcon2 = metMath(stor.afcon2)
            stor.afcon3 = metMath(stor.afcon3)
            stor.afcon4 = metMath(stor.afcon4)
            stor.afcon5 = metMath(stor.afcon5)
            stor.afcon6 = metMath(stor.afcon6)
            break
          case 'fraction':
            makeFracDeci()
            break
          case 'racine':
            makeRacDeci()
        }
        stor.afn1 = metMath(stor.afn1)
        stor.afn2 = metMath(stor.afn2)
        stor.afn3 = metMath(stor.afn3)
        stor.afn4 = metMath(stor.afn4)
        stor.afn5 = metMath(stor.afn5)
        stor.afn6 = metMath(stor.afn6)
        break
      case 'fraction':
        switch (exo.second) {
          case 'entier':
            makeFracEntier()
            stor.afn1 = metMath(stor.afn1)
            stor.afn2 = metMath(stor.afn2)
            stor.afn3 = metMath(stor.afn3)
            stor.afn4 = metMath(stor.afn4)
            stor.afn5 = metMath(stor.afn5)
            stor.afn6 = metMath(stor.afn6)
            break
          case 'decimal':
            makeFracDeci()
            stor.afn1 = metMath(stor.afn1)
            stor.afn2 = metMath(stor.afn2)
            stor.afn3 = metMath(stor.afn3)
            stor.afn4 = metMath(stor.afn4)
            stor.afn5 = metMath(stor.afn5)
            stor.afn6 = metMath(stor.afn6)
            break
          case 'fraction': {
            const ln = (ds.difficulte === 1) ? j3pShuffle([2, 3, 5, 7, 11, 13, 17, 19, 23, 29, 31, 37, 41]) : j3pShuffle([17, 19, 23, 29, 31, 37, 41, 43, 47, 53, 59, 61, 67])
            stor.n1 = { num: ln.pop(), den: ln.pop() }
            stor.n2 = { num: ln.pop(), den: ln.pop() }
            stor.n3 = { num: ln.pop(), den: ln.pop() }
            stor.n4 = { num: ln.pop(), den: ln.pop() }
            stor.n5 = { num: ln.pop(), den: ln.pop() }
            stor.n6 = { num: ln.pop(), den: ln.pop() }
            stor.afn1 = '\\frac{' + stor.n1.num + '}{' + stor.n1.den + '}'
            stor.afcon1 = ajZerFrac3(stor.n1)
            stor.afn2 = '\\frac{' + stor.n2.num + '}{' + stor.n2.den + '}'
            stor.afcon2 = ajZerFrac3(stor.n2)
            stor.afn3 = '\\frac{' + stor.n3.num + '}{' + stor.n3.den + '}'
            stor.afcon3 = ajZerFrac3(stor.n3)
            stor.afn4 = '\\frac{' + stor.n4.num + '}{' + stor.n4.den + '}'
            stor.afcon4 = ajZerFrac3(stor.n4)
            stor.afn5 = '\\frac{' + stor.n5.num + '}{' + stor.n5.den + '}'
            stor.afcon5 = ajZerFrac3(stor.n5)
            stor.afn6 = '\\frac{' + stor.n6.num + '}{' + stor.n6.den + '}'
            stor.afcon6 = ajZerFrac3(stor.n6)
          }
            break
          case 'racine':
            makeRacFrac()
        }
        break
      case 'racine':
        switch (exo.second) {
          case 'entier':
            makeRacEntier()
            stor.afn1 = metMath(stor.afn1)
            stor.afn2 = metMath(stor.afn2)
            stor.afn3 = metMath(stor.afn3)
            stor.afn4 = metMath(stor.afn4)
            stor.afn5 = metMath(stor.afn5)
            stor.afn6 = metMath(stor.afn6)
            break
          case 'decimal':
            makeRacDeci()
            stor.afn1 = metMath(stor.afn1)
            stor.afn2 = metMath(stor.afn2)
            stor.afn3 = metMath(stor.afn3)
            stor.afn4 = metMath(stor.afn4)
            stor.afn5 = metMath(stor.afn5)
            stor.afn6 = metMath(stor.afn6)
            break
          case 'fraction':
            makeRacFrac()
            break
          case 'racine': {
            const ln = (ds.difficulte === 1) ? j3pShuffle([2, 3, 5, 7, 11, 13, 17, 23, 27]) : j3pShuffle([7, 11, 13, 17, 23, 27])
            stor.n1 = ln.pop()
            stor.n2 = ln.pop()
            stor.n3 = ln.pop()
            stor.n4 = ln.pop()
            stor.n5 = ln.pop()
            stor.n6 = ln.pop()
            stor.afn1 = '2\\sqrt{' + stor.n1 + '}'
            stor.afcon1 = '\\sqrt{' + stor.n1 * 4 + '}'
            stor.afn2 = '3\\sqrt{' + stor.n2 + '}'
            stor.afcon2 = '\\sqrt{' + stor.n2 * 9 + '}'
            stor.afn3 = '4\\sqrt{' + stor.n3 + '}'
            stor.afcon3 = '\\sqrt{' + stor.n3 * 16 + '}'
            stor.afn4 = '5\\sqrt{' + stor.n4 + '}'
            stor.afcon4 = '\\sqrt{' + stor.n4 * 25 + '}'
            stor.afn5 = '6\\sqrt{' + stor.n5 + '}'
            stor.afcon5 = '\\sqrt{' + stor.n5 * 36 + '}'
            stor.afn6 = '7\\sqrt{' + stor.n6 + '}'
            stor.afcon6 = '\\sqrt{' + stor.n6 * 49 + '}'
          }
        }
        break
    }
    if (ds.negatif) {
      const signe = ['', '-', '']
      const signe2 = ['+', '-', '']
      let haz = j3pGetRandomInt(0, 2)
      stor.afn1 = signe[haz] + stor.afn1
      stor.afcon1 = signe2[haz] + stor.afcon1
      haz = j3pGetRandomInt(0, 2)
      stor.afn2 = signe[haz] + stor.afn2
      stor.afcon2 = signe2[haz] + stor.afcon2
      haz = j3pGetRandomInt(0, 2)
      stor.afn3 = signe[haz] + stor.afn3
      stor.afcon3 = signe2[haz] + stor.afcon3
      haz = j3pGetRandomInt(0, 2)
      stor.afn4 = signe[haz] + stor.afn4
      stor.afcon4 = signe2[haz] + stor.afcon4
      haz = j3pGetRandomInt(0, 2)
      stor.afn5 = signe[haz] + stor.afn5
      stor.afcon5 = signe2[haz] + stor.afcon5
      haz = j3pGetRandomInt(0, 2)
      stor.afn6 = signe[haz] + stor.afn6
      stor.afcon6 = signe2[haz] + stor.afcon6
    }
    return exo
  }

  function metMath (s) {
    return j3pNombreBienEcrit(s, { garderZeroNonSignificatif: true }).replace(/ /g, '\\text{ }')
  }

  function putUnZero (s) {
    let haz = j3pGetRandomInt(1, s.length - 1)
    if (s.substring(0, 2) === '0.') haz = Math.max(haz, 2)
    return s.substring(0, haz) + '0' + s.substring(haz)
  }

  function makeRacFrac () {
    const ln = (ds.difficulte === 1) ? j3pShuffle([2, 3, 5, 7, 11, 13, 17, 19, 23, 29, 31, 37, 41]) : j3pShuffle([17, 19, 23, 29, 31, 37, 41, 43, 47, 53, 59, 61, 67])
    stor.n1 = { num: ln.pop(), den: ln.pop() }
    stor.n2 = { num: ln.pop(), den: ln.pop() }
    stor.n3 = { num: ln.pop(), den: ln.pop() }
    stor.n4 = { num: ln.pop(), den: ln.pop() }
    stor.n5 = { num: ln.pop(), den: ln.pop() }
    stor.n6 = { num: ln.pop(), den: ln.pop() }
    stor.afn1 = '\\frac{' + stor.n1.num + '}{' + stor.n1.den + '}'
    stor.afcon1 = ajZerRac3(stor.n1)
    stor.afn2 = '\\frac{' + stor.n2.num + '}{' + stor.n2.den + '}'
    stor.afcon2 = ajZerRac3(stor.n2)
    stor.afn3 = '\\frac{' + stor.n3.num + '}{' + stor.n3.den + '}'
    stor.afcon3 = ajZerRac3(stor.n3)
    stor.afn4 = '\\frac{' + stor.n4.num + '}{' + stor.n4.den + '}'
    stor.afcon4 = ajZerRac3(stor.n4)
    stor.afn5 = '\\frac{' + stor.n5.num + '}{' + stor.n5.den + '}'
    stor.afcon5 = ajZerRac3(stor.n5)
    stor.afn6 = '\\frac{' + stor.n6.num + '}{' + stor.n6.den + '}'
    stor.afcon6 = ajZerRac3(stor.n6)
  }

  function makeRacDeci () {
    if (ds.difficulte === 1) {
      let ln = []
      for (let i = 1; i < 1000; i++) {
        if (i % 10 !== 0) ln.push(i)
      }
      ln = j3pShuffle(ln)
      stor.n1 = hdiv(ln.pop())
      stor.n2 = hdiv(ln.pop())
      stor.n3 = hdiv(ln.pop())
      stor.n4 = hdiv(ln.pop())
      stor.n5 = hdiv(ln.pop())
      stor.n6 = hdiv(ln.pop())
      stor.afn1 = String(stor.n1).replace('.', ',')
      stor.afn2 = String(stor.n2).replace('.', ',')
      stor.afn3 = String(stor.n3).replace('.', ',')
      stor.afn4 = String(stor.n4).replace('.', ',')
      stor.afn5 = String(stor.n5).replace('.', ',')
      stor.afn6 = String(stor.n6).replace('.', ',')
    } else {
      let ln = []
      for (let i = 101; i < 1000; i++) {
        if (i % 10 !== 0) ln.push(i)
      }
      ln = j3pShuffle(ln)
      stor.n1 = hdiv(ln.pop())
      stor.afn1 = String(stor.n1)
      stor.afn2 = putUnZero(stor.afn1)
      stor.afn3 = putUnZero(stor.afn2)
      stor.afn4 = putUnZero(stor.afn3)
      stor.afn5 = putUnZero(stor.afn4)
      stor.afn6 = putUnZero(stor.afn5)
      stor.n2 = Number(stor.afn2.replace(',', '.'))
      stor.n3 = Number(stor.afn3.replace(',', '.'))
      stor.n4 = Number(stor.afn4.replace(',', '.'))
      stor.n5 = Number(stor.afn5.replace(',', '.'))
      stor.n6 = Number(stor.afn6.replace(',', '.'))
    }
    stor.afcon1 = ajZerRac2(stor.n1)
    stor.afcon2 = ajZerRac2(stor.n2)
    stor.afcon3 = ajZerRac2(stor.n3)
    stor.afcon4 = ajZerRac2(stor.n4)
    stor.afcon5 = ajZerRac2(stor.n5)
    stor.afcon6 = ajZerRac2(stor.n6)
  }

  function makeFracDeci () {
    if (ds.difficulte === 1) {
      let ln = []
      for (let i = 1; i < 1000; i++) {
        if (i % 10 !== 0) ln.push(i)
      }
      ln = j3pShuffle(ln)
      stor.n1 = hdiv(ln.pop())
      stor.n2 = hdiv(ln.pop())
      stor.n3 = hdiv(ln.pop())
      stor.n4 = hdiv(ln.pop())
      stor.n5 = hdiv(ln.pop())
      stor.n6 = hdiv(ln.pop())
      stor.afn1 = String(stor.n1).replace('.', ',')
      stor.afn2 = String(stor.n2).replace('.', ',')
      stor.afn3 = String(stor.n3).replace('.', ',')
      stor.afn4 = String(stor.n4).replace('.', ',')
      stor.afn5 = String(stor.n5).replace('.', ',')
      stor.afn6 = String(stor.n6).replace('.', ',')
    } else {
      let ln = []
      for (let i = 101; i < 1000; i++) {
        if (i % 10 !== 0) ln.push(i)
      }
      ln = j3pShuffle(ln)
      stor.n1 = hdiv(ln.pop())
      stor.afn1 = String(stor.n1)
      stor.afn2 = putUnZero(stor.afn1)
      stor.afn3 = putUnZero(stor.afn2)
      stor.afn4 = putUnZero(stor.afn3)
      stor.afn5 = putUnZero(stor.afn4)
      stor.afn6 = putUnZero(stor.afn5)
      stor.n2 = Number(stor.afn2.replace(',', '.'))
      stor.n3 = Number(stor.afn3.replace(',', '.'))
      stor.n4 = Number(stor.afn4.replace(',', '.'))
      stor.n5 = Number(stor.afn5.replace(',', '.'))
      stor.n6 = Number(stor.afn6.replace(',', '.'))
    }
    stor.afcon1 = ajZerFrac2(stor.n1)
    stor.afcon2 = ajZerFrac2(stor.n2)
    stor.afcon3 = ajZerFrac2(stor.n3)
    stor.afcon4 = ajZerFrac2(stor.n4)
    stor.afcon5 = ajZerFrac2(stor.n5)
    stor.afcon6 = ajZerFrac2(stor.n6)
  }

  function makeRacEntier () {
    if (ds.difficulte === 1) {
      let ln = []
      for (let i = 0; i < 1000; i++) ln.push(i)
      ln = j3pShuffle(ln)
      stor.n1 = ln.pop()
      stor.n2 = ln.pop()
      stor.n3 = ln.pop()
      stor.n4 = ln.pop()
      stor.n5 = ln.pop()
      stor.n6 = ln.pop()
      stor.afn1 = String(stor.n1)
      stor.afn2 = String(stor.n2)
      stor.afn3 = String(stor.n3)
      stor.afn4 = String(stor.n4)
      stor.afn5 = String(stor.n5)
      stor.afn6 = String(stor.n6)
    } else {
      stor.n1 = j3pGetRandomInt(100, 999)
      stor.afn1 = String(stor.n1)
      stor.afn2 = putUnZero(stor.afn1)
      stor.afn3 = putUnZero(stor.afn2)
      stor.afn4 = putUnZero(stor.afn3)
      stor.afn5 = putUnZero(stor.afn4)
      stor.afn6 = putUnZero(stor.afn5)
      stor.n2 = Number(stor.afn2)
      stor.n3 = Number(stor.afn3)
      stor.n4 = Number(stor.afn4)
      stor.n5 = Number(stor.afn5)
      stor.n6 = Number(stor.afn6)
    }
    stor.afcon1 = ajZerRac(stor.n1)
    stor.afcon2 = ajZerRac(stor.n2)
    stor.afcon3 = ajZerRac(stor.n3)
    stor.afcon4 = ajZerRac(stor.n4)
    stor.afcon5 = ajZerRac(stor.n5)
    stor.afcon6 = ajZerRac(stor.n6)
  }

  function makeDeciEntier () {
    if (ds.difficulte === 1) {
      let ln = []
      for (let i = 0; i < 1000; i++) ln.push(i)
      ln = j3pShuffle(ln)
      stor.n1 = ln.pop()
      stor.n2 = ln.pop()
      stor.n3 = ln.pop()
      stor.n4 = ln.pop()
      stor.n5 = ln.pop()
      stor.n6 = ln.pop()
      stor.afn1 = String(stor.n1)
      stor.afn2 = String(stor.n2)
      stor.afn3 = String(stor.n3)
      stor.afn4 = String(stor.n4)
      stor.afn5 = String(stor.n5)
      stor.afn6 = String(stor.n6)
    } else {
      stor.n1 = j3pGetRandomInt(100, 999)
      stor.afn1 = String(stor.n1)
      stor.afn2 = putUnZero(stor.afn1)
      stor.afn3 = putUnZero(stor.afn2)
      stor.afn4 = putUnZero(stor.afn3)
      stor.afn5 = putUnZero(stor.afn4)
      stor.afn6 = putUnZero(stor.afn5)
    }
    stor.afcon1 = ajZerDec(stor.afn1)
    stor.afcon2 = ajZerDec(stor.afn2)
    stor.afcon3 = ajZerDec(stor.afn3)
    stor.afcon4 = ajZerDec(stor.afn4)
    stor.afcon5 = ajZerDec(stor.afn5)
    stor.afcon6 = ajZerDec(stor.afn6)
  }

  function makeFracEntier () {
    if (ds.difficulte === 1) {
      let ln = []
      for (let i = 0; i < 1000; i++) ln.push(i)
      ln = j3pShuffle(ln)
      stor.n1 = ln.pop()
      stor.n2 = ln.pop()
      stor.n3 = ln.pop()
      stor.n4 = ln.pop()
      stor.n5 = ln.pop()
      stor.n6 = ln.pop()
      stor.afn1 = String(stor.n1)
      stor.afn2 = String(stor.n2)
      stor.afn3 = String(stor.n3)
      stor.afn4 = String(stor.n4)
      stor.afn5 = String(stor.n5)
      stor.afn6 = String(stor.n6)
    } else {
      stor.n1 = j3pGetRandomInt(100, 999)
      stor.afn1 = String(stor.n1)
      stor.afn2 = putUnZero(stor.afn1)
      stor.afn3 = putUnZero(stor.afn2)
      stor.afn4 = putUnZero(stor.afn3)
      stor.afn5 = putUnZero(stor.afn4)
      stor.afn6 = putUnZero(stor.afn5)
      stor.n2 = Number(stor.afn2)
      stor.n3 = Number(stor.afn3)
      stor.n4 = Number(stor.afn4)
      stor.n5 = Number(stor.afn5)
      stor.n6 = Number(stor.afn6)
    }
    stor.afcon1 = ajZerFrac(stor.n1)
    stor.afcon2 = ajZerFrac(stor.n2)
    stor.afcon3 = ajZerFrac(stor.n3)
    stor.afcon4 = ajZerFrac(stor.n4)
    stor.afcon5 = ajZerFrac(stor.n5)
    stor.afcon6 = ajZerFrac(stor.n6)
  }

  function ajZer (s) {
    const haz = j3pGetRandomInt(0, 2)
    if (haz === 0) return '0' + s
    if (haz === 1) return '00' + s
    if (haz === 2) return '000' + s
  }

  function hdiv (n) {
    const haz = j3pGetRandomInt(1, 3)
    return j3pArrondi(n / Math.pow(10, haz), haz)
  }

  function ajZerDec (s) {
    const haz = j3pGetRandomInt(0, 5)
    if (haz === 0) return s + ',0'
    if (haz === 1) return s + ',00'
    if (haz === 2) return s + ',000'
    if (haz === 3) return '00' + s + ',0'
    if (haz === 4) return '0' + s + ',00'
    if (haz === 5) return '0' + s + ',000'
  }

  function ajZerFrac (n) {
    const ll = j3pShuffle([1, 2, 10, 100])
    const cas = ll.pop()
    return '\\frac{' + metMath(cas * n) + '}{' + cas + '}'
  }

  function ajZerRac (n) {
    return '\\sqrt{' + metMath(n * n) + '}'
  }

  function ajZerDec2 (s) {
    const haz = j3pGetRandomInt(0, 5)
    if (haz === 0) return s + '0'
    if (haz === 1) return s + '00'
    if (haz === 2) return s + '000'
    if (haz === 3) return '00' + s + '0'
    if (haz === 4) return '0' + s + '00'
    if (haz === 5) return '0' + s + '000'
  }

  function ajZerFrac2 (n) {
    const ll = j3pShuffle([1, 2, 10, 100, 1000])
    const cas = ll.pop()
    return '\\frac{' + metMath(String(j3pArrondi(cas * n, 9)).replace('.', ',')) + '}{' + metMath(String(cas)) + '}'
  }

  function ajZerRac2 (n) {
    return '\\sqrt{' + metMath(String(j3pArrondi(n * n, 10)).replace('.', ',')) + '}'
  }

  function ajZerFrac3 (t) {
    const ll = j3pShuffle([2, 3, 5, 10, 100, 1000])
    const cas = ll.pop()
    return '\\frac{' + metMath(String(cas * t.num).replace('.', ',')) + '}{' + metMath(String(cas * t.den).replace('.', ',')) + '}'
  }

  function ajZerRac3 (t) {
    const haz = j3pGetRandomInt(0, 2)
    if (haz === 0) return '\\sqrt{\\frac{' + metMath(String(t.num * t.num).replace('.', ',')) + '}{' + metMath(String(t.den * t.den).replace('.', ',')) + '}}'
    if (haz === 1) return '\\frac{\\sqrt{' + metMath(String(t.num * t.num).replace('.', ',')) + '}}{' + metMath(t.den) + '}'
    if (haz === 2) return '\\frac{' + t.num + '}{\\sqrt{' + metMath(String(t.den * t.den).replace('.', ',')) + '}}'
  }

  function poseQuestion () {
    stor.afn1 = '$' + stor.afn1 + '$'
    stor.afn2 = '$' + stor.afn2 + '$'
    stor.afn3 = '$' + stor.afn3 + '$'
    stor.afn4 = '$' + stor.afn4 + '$'
    stor.afn5 = '$' + stor.afn5 + '$'
    stor.afn6 = '$' + stor.afn6 + '$'
    stor.afcon1 = '$' + stor.afcon1 + '$'
    stor.afcon2 = '$' + stor.afcon2 + '$'
    stor.afcon3 = '$' + stor.afcon3 + '$'
    stor.afcon4 = '$' + stor.afcon4 + '$'
    stor.afcon5 = '$' + stor.afcon5 + '$'
    stor.afcon6 = '$' + stor.afcon6 + '$'
    stor.lesdiv.consigne.classList.add('enonce')
    stor.lesdiv.travail.classList.add('travail')

    stor.couples = [[stor.afcon1, stor.afn1], [stor.afcon2, stor.afn2], [stor.afcon3, stor.afn3]]
    if (ds.nbcouples > 3) stor.couples.push([stor.afcon4, stor.afn4])
    if (ds.nbcouples > 4) stor.couples.push([stor.afcon5, stor.afn5])
    if (ds.nbcouples > 5) stor.couples.push([stor.afcon6, stor.afn6])
    stor.couples = j3pShuffle(stor.couples)

    // on pourrait récupérer le score ici mais il est calculé dans le case correction
    const onComplete = (/* score */) => me.sectionCourante('correction')
    stor.memory = Memory.create(stor.lesdiv.travail, stor.couples, onComplete, { antimemory: false })
    j3pAffiche(stor.lesdiv.consigne, null, 'Observe bien les cartes ci dessous&nbsp;')
    j3pAjouteBouton(stor.lesdiv.consigne, clicPret, { value: 'Commencer' })
  }
  function clicPret () {
    j3pEmpty(stor.lesdiv.consigne)
    j3pAffiche(stor.lesdiv.consigne, null, '<b>Retrouve les écritures égales</b>')
    stor.memory.clicPretListener()
  }
  function creeLesDiv () {
    stor.lesdiv = {}
    stor.lesdiv.conteneur = j3pAddElt(me.zonesElts.MG, 'div', {
      style: me.styles.etendre('toutpetit.enonce', { padding: '10px' })
    })
    const tab = addDefaultTable(stor.lesdiv.conteneur, 1, 5)
    j3pAddContent(tab[0][1], '&nbsp;&nbsp;&nbsp;')
    j3pAddContent(tab[0][3], '&nbsp;&nbsp;&nbsp;')
    stor.lesdiv.consigne = tab[0][0]
    stor.lesdiv.calculatrice = tab[0][2]
    stor.lesdiv.travail = addDefaultTable(stor.lesdiv.conteneur, 1, 1)[0][0]
    stor.lesdiv.explications = j3pAddElt(stor.lesdiv.conteneur, 'div')
    stor.lesdiv.correction = tab[0][4]
    stor.lesdiv.explications.style.color = me.styles.cfaux
    me.zonesElts.MG.classList.add('fond')
  }

  function isRepOk () {
    stor.memory.disable()
    return stor.memory.isOk()
  }

  function boutonfen () {
    j3pToggleFenetres('Calculatrice')
  }

  function afficheCorrection () {
    stor.lesdiv.explications.classList.add('explique')
    j3pAddContent(stor.lesdiv.explications, 'Clique une carte pour voir la carte qui lui correspond.')
  }

  function enonceMain () {
    me.videLesZones()
    creeLesDiv()
    stor.exo = faisChoixExos()
    me.fenetresjq = [{ name: 'Calculatrice', title: 'Une calculatrice', left: 600, top: 10 }]
    j3pCreeFenetres(me)
    j3pAfficheCroixFenetres('Calculatrice')
    j3pAjouteBouton(stor.lesdiv.calculatrice, boutonfen, { className: 'MepBoutons', value: 'Calculatrice' })
    poseQuestion()
    me.finEnonce()
    // ici on ne veut pas du bouton valider, c’est la fin du memory qui valide
    me.cacheBoutonValider()
  }

  switch (this.etat) {
    case 'enonce':
      // code exécuté au lancement de la section
      if (me.debutDeLaSection) {
        initSection()
      } else {
        stor.raz = true
        enonceMain()
      }
      break // case "enonce":

    case 'correction':
      // On teste si une réponse a été saisie
      if (stor.lesdiv === undefined) return
      stor.lesdiv.explications.innerHTML = ''
      stor.lesdiv.correction.innerHTML = ''

      if (isRepOk()) {
        // Bonne réponse
        stor.lesdiv.correction.style.color = this.styles.cbien
        stor.lesdiv.correction.innerHTML = cBien
        this.score = j3pArrondi(this.score + stor.memory.getScore(), 1)

        this.typederreurs[0]++
        return this.finCorrection('navigation', true)
      }

      // Pas de bonne réponse

      stor.lesdiv.correction.style.color = this.styles.cfaux
      stor.lesdiv.correction.innerHTML = tempsDepasse + '<BR>'
      this.typederreurs[10]++
      afficheCorrection(true)
      return this.finCorrection('navigation', true)

    case 'navigation':
      if (!this.sectionTerminee()) {
        this.etat = 'enonce'
      }
      this.finNavigation()
      break // case "navigation":
  }
}
