import { j3pAddElt, j3pAjouteBouton, j3pArrondi, j3pDetruit, j3pEmpty, j3pGetRandomInt, j3pModale, j3pPGCD, j3pShuffle } from 'src/legacy/core/functions'
import { verifNombreBienEcrit } from 'src/legacy/outils/fonctions/ecritureNombre'
import { j3pAffiche } from 'src/lib/mathquill/functions'
import ZoneStyleMathquill1 from 'src/legacy/outils/zoneStyleMathquill/ZoneStyleMathquill1'
import { addDefaultTable } from 'src/legacy/themes/table'
import { ecrisBienMathquill } from 'src/legacy/outils/zoneStyleMathquill/functions'
import $ from 'jquery'
import { j3pAfficheCroixFenetres, j3pCreeFenetres, j3pToggleFenetres } from 'src/legacy/core/functionsJqDialog'
import textesGeneriques from 'src/lib/core/textes'

const { cBien, cFaux, essaieEncore, regardeCorrection, reponseIncomplete, tempsDepasse } = textesGeneriques

/**
 * Ajouter une description ici
 * @fileOverview
 * @author Tommy Barroy <t.barroy@laposte.net>
 * @since 2017-10
 */
export const params = {
  outils: ['calculatrice', 'mathquill'],
  /**
   * Ce qui est paramétrable dans l’éditeur de graphe
   * @type {LegacySectionParamsParametres}
   */
  parametres: [
    ['nbrepetitions', 9, 'entier', 'Nombre de répétitions de la section'],
    ['nbchances', 2, 'entier', 'Nombre de tentatives possibles.'],
    ['limite', 0, 'entier', 'Temps disponible par répétition, en secondes'],
    ['Espace', true, 'boolean', '<u>true</u>: Les espaces entre chaque groupe sont exigés.'],
    ['cm', true, 'boolean', '<u>true</u>: Rang proposé.'],
    ['cm_2', true, 'boolean', '<u>true</u>: Rang proposé. '],
    ['cm_3', true, 'boolean', '<u>true</u>: Rang proposé. '],
    ['h_m_s', true, 'boolean', '<u>true</u>: Rang proposé.'],
    ['j_m_a', true, 'boolean', '<u>true</u>: Rang proposé.'],
    ['Litre', true, 'boolean', '<u>true</u>: Rang proposé..'],
    ['gramme', true, 'boolean', '<u>true</u>: Rang proposé.'],
    ['Lien', 'les deux', 'liste', 'Type de lien', ['produit', 'quotient', 'les deux']],
    ['Calculatrice', true, 'boolean', '<u>true</u>: Une calculatrice est disponible.'],
    ['theme', 'standard', 'liste', 'Modèle pour l’affichage', ['standard', 'zonesAvecImageDeFond']]

  ]
}

/**
 * section conunitcomp
 * @this {Parcours}
 */
export default function main () {
  const me = this
  const stor = this.storage
  let ds = me.donneesSection

  function yareponse () {
    if (me.isElapsed) { return true }
    if (stor.zoneE !== undefined) {
      if (stor.zoneE.reponse() === '') {
        stor.zoneE.focus()
        return false
      }
    }
    if (stor.zoneG.reponse() === '') {
      stor.zoneG.focus()
      return false
    }
    return true
  }
  function faisChoixExo () {
    const e = ds.lesExos.pop()
    switch (e.units.u1) {
      case 'cm': {
        const uns = ['km', 'hm', 'dam', 'm', 'dm', 'cm', 'mm']
        const uns2 = j3pShuffle(uns)
        stor.u1dep = uns2.pop()
        stor.u1arr = uns2.pop()
        const rang = uns.indexOf(stor.u1dep) - uns.indexOf(stor.u1arr)
        stor.mul1 = (rang > 0) ? { num: 1, den: Math.pow(10, rang) } : { num: Math.pow(10, -rang), den: 1 }
      }
        break
      case 'cm²': {
        const uns = ['km²', 'hm²', 'dam²', 'm²', 'dm²', 'cm²', 'mm²']
        const haz = j3pGetRandomInt(0, 4)
        const unspriv = [uns[haz], uns[haz + 1], uns[haz + 2]]
        const uns2 = j3pShuffle(unspriv)
        stor.u1dep = uns2.pop()
        stor.u1arr = uns2.pop()
        const rang = uns.indexOf(stor.u1dep) - uns.indexOf(stor.u1arr)
        stor.mul1 = (rang > 0) ? { num: 1, den: Math.pow(100, rang) } : { num: Math.pow(100, -rang), den: 1 }
      }
        break
      case 'cm³': {
        const uns = ['km³', 'hm³', 'dam³', 'm³', 'dm³', 'cm³', 'mm³']
        const haz = j3pGetRandomInt(0, 5)
        const unspriv = [uns[haz], uns[haz + 1]]
        const uns2 = j3pShuffle(unspriv)
        stor.u1dep = uns2.pop()
        stor.u1arr = uns2.pop()
        const rang = uns.indexOf(stor.u1dep) - uns.indexOf(stor.u1arr)
        stor.mul1 = (rang > 0) ? { num: 1, den: Math.pow(1000, rang) } : { num: Math.pow(1000, -rang), den: 1 }
      }
        break
      case 'h_m_s': {
        const uns = ['heures', 'minutes', 'secondes']
        const uns2 = j3pShuffle(uns)
        stor.u1dep = uns2.pop()
        stor.u1arr = uns2.pop()
        const rang = uns.indexOf(stor.u1dep) - uns.indexOf(stor.u1arr)
        stor.mul1 = (rang > 0) ? { num: 1, den: Math.pow(60, rang) } : { num: Math.pow(60, -rang), den: 1 }
      }
        break
      case 'j_m_a': {
        const uns = ['jours', 'mois', 'années']
        const uns2 = j3pShuffle(uns)
        stor.u1dep = uns2.pop()
        stor.u1arr = uns2.pop()
        if (stor.u1dep === 'jours') {
          if (stor.u1arr === 'mois') {
            stor.mul1 = { num: 1, den: 30 }
          } else {
            stor.mul1 = { num: 1, den: 365 }
          }
        }
        if (stor.u1dep === 'mois') {
          if (stor.u1arr === 'jours') {
            stor.mul1 = { num: 30, den: 1 }
          } else {
            stor.mul1 = { num: 1, den: 12 }
          }
        }
        if (stor.u1dep === 'années') {
          if (stor.u1arr === 'mois') {
            stor.mul1 = { num: 12, den: 1 }
          } else {
            stor.mul1 = { num: 365, den: 1 }
          }
        }
      }
        break
      case 'L': {
        const uns = ['hL', 'daL', 'L', 'dL', 'cL', 'mL']
        const uns2 = j3pShuffle(uns)
        stor.u1dep = uns2.pop()
        stor.u1arr = uns2.pop()
        const rang = uns.indexOf(stor.u1dep) - uns.indexOf(stor.u1arr)
        stor.mul1 = (rang > 0) ? { num: 1, den: Math.pow(10, rang) } : { num: Math.pow(10, -rang), den: 1 }
      }
        break
      case 'g': {
        const uns = ['kg', 'hg', 'dag', 'g', 'dg', 'cg', 'mg']
        const uns2 = j3pShuffle(uns)
        stor.u1dep = uns2.pop()
        stor.u1arr = uns2.pop()
        const rang = uns.indexOf(stor.u1dep) - uns.indexOf(stor.u1arr)
        stor.mul1 = (rang > 0) ? { num: 1, den: Math.pow(10, rang) } : { num: Math.pow(10, -rang), den: 1 }
      }
        break
    }
    switch (e.units.u2) {
      case 'cm': {
        const uns = ['km', 'hm', 'dam', 'm', 'dm', 'cm', 'mm']
        const uns2 = j3pShuffle(uns)
        stor.u2dep = uns2.pop()
        stor.u2arr = uns2.pop()
        const rang = uns.indexOf(stor.u2dep) - uns.indexOf(stor.u2arr)
        stor.mul2 = (rang > 0) ? { num: 1, den: Math.pow(10, rang) } : { num: Math.pow(10, -rang), den: 1 }
      }
        break
      case 'cm²': {
        const uns = ['km²', 'hm²', 'dam²', 'm²', 'dm²', 'cm²', 'mm²']
        const haz = j3pGetRandomInt(0, 4)
        const unspriv = [uns[haz], uns[haz + 1], uns[haz + 2]]
        const uns2 = j3pShuffle(unspriv)
        stor.u2dep = uns2.pop()
        stor.u2arr = uns2.pop()
        const rang = uns.indexOf(stor.u2dep) - uns.indexOf(stor.u2arr)
        stor.mul2 = (rang > 0) ? { num: 1, den: Math.pow(100, rang) } : { num: Math.pow(100, -rang), den: 1 }
      }
        break
      case 'cm³': {
        const uns = ['km³', 'hm³', 'dam³', 'm³', 'dm³', 'cm³', 'mm³']
        const haz = j3pGetRandomInt(0, 5)
        const unspriv = [uns[haz], uns[haz + 1]]
        const uns2 = j3pShuffle(unspriv)
        stor.u2dep = uns2.pop()
        stor.u2arr = uns2.pop()
        const rang = uns.indexOf(stor.u2dep) - uns.indexOf(stor.u2arr)
        stor.mul2 = (rang > 0) ? { num: 1, den: Math.pow(1000, rang) } : { num: Math.pow(1000, -rang), den: 1 }
      }
        break
      case 'h_m_s': {
        const uns = ['heures', 'minutes', 'secondes']
        const uns2 = j3pShuffle(uns)
        stor.u2dep = uns2.pop()
        stor.u2arr = uns2.pop()
        const rang = uns.indexOf(stor.u2dep) - uns.indexOf(stor.u2arr)
        stor.mul2 = (rang > 0) ? { num: 1, den: Math.pow(60, rang) } : { num: Math.pow(60, -rang), den: 1 }
      }
        break
      case 'j_m_a': {
        const uns = ['jours', 'mois', 'années']
        const uns2 = j3pShuffle(uns)
        stor.u2dep = uns2.pop()
        stor.u2arr = uns2.pop()
        if (stor.u2dep === 'jours') {
          if (stor.u2arr === 'mois') {
            stor.mul2 = { num: 1, den: 30 }
          } else {
            stor.mul2 = { num: 1, den: 365 }
          }
        }
        if (stor.u2dep === 'mois') {
          if (stor.u2arr === 'jours') {
            stor.mul2 = { num: 30, den: 1 }
          } else {
            stor.mul2 = { num: 1, den: 12 }
          }
        }
        if (stor.u2dep === 'années') {
          if (stor.u2arr === 'mois') {
            stor.mul2 = { num: 12, den: 1 }
          } else {
            stor.mul2 = { num: 365, den: 1 }
          }
        }
      }
        break
      case 'L': {
        const uns = ['hL', 'daL', 'L', 'dL', 'cL', 'mL']
        const uns2 = j3pShuffle(uns)
        stor.u2dep = uns2.pop()
        stor.u2arr = uns2.pop()
        const rang = uns.indexOf(stor.u2dep) - uns.indexOf(stor.u2arr)
        stor.mul2 = (rang > 0) ? { num: 1, den: Math.pow(10, rang) } : { num: Math.pow(10, -rang), den: 1 }
      }
        break
      case 'g': {
        const uns = ['kg', 'hg', 'dag', 'g', 'dg', 'cg', 'mg']
        const uns2 = j3pShuffle(uns)
        stor.u2dep = uns2.pop()
        stor.u2arr = uns2.pop()
        const rang = uns.indexOf(stor.u2dep) - uns.indexOf(stor.u2arr)
        stor.mul2 = (rang > 0) ? { num: 1, den: Math.pow(10, rang) } : { num: Math.pow(10, -rang), den: 1 }
      }
        break
    }
    stor.lien = e.lien
    if (e.lien === 'p') {
      stor.pass1 = stor.u1dep + ' . ' + stor.u2arr
      stor.pass2 = stor.u1arr + ' . ' + stor.u2dep
      stor.nom = '<b>' + stor.u1dep + ' $ \\times $' + stor.u2dep + '</b>'
      stor.lUnit = stor.u1dep + ' . ' + stor.u2dep
      stor.lmul = { num: stor.mul1.num * stor.mul2.num, den: stor.mul1.den * stor.mul2.den }
      stor.lunitAr = stor.u1arr + ' . ' + stor.u2arr
    } else {
      stor.pass1 = stor.u1dep + ' / ' + stor.u2arr
      stor.pass2 = stor.u1arr + ' / ' + stor.u2dep
      stor.nom = '<b>' + stor.u1dep + ' par ' + stor.u2dep + '</b>'
      stor.lUnit = stor.u1dep + ' / ' + stor.u2dep
      stor.lmul = { num: stor.mul1.num * stor.mul2.den, den: stor.mul1.den * stor.mul2.num }
      stor.lunitAr = stor.u1arr + ' / ' + stor.u2arr
    }
    const pg = j3pPGCD(stor.lmul.num, stor.lmul.den)
    stor.lmul = { num: stor.lmul.num / pg, den: stor.lmul.den / pg }
    stor.nbdep = j3pGetRandomInt(2, 50) * stor.lmul.den
    stor.nbfin = j3pArrondi(stor.nbdep * stor.lmul.num / stor.lmul.den, 0)
  }
  function isRepOk () {
    stor.errEcrit = []

    let okg = true
    let oke = true

    stor.repEleveG = stor.zoneG.reponse()
    const testG = verifNombreBienEcrit(stor.repEleveG, ds.Espace, true)
    if (!testG.good) {
      if (stor.errEcrit.indexOf(testG.remede) === -1) stor.errEcrit.push(testG.remede)
      stor.zoneG.corrige(false)
      okg = false
    }
    if (okg) {
      stor.repEleveG = testG.nb
      if (stor.nbfin !== stor.repEleveG) {
        stor.errRep = true
        stor.zoneG.corrige(false)
        okg = false
      }
    }

    if (stor.zoneE !== undefined) {
      stor.repEleveE = stor.zoneE.reponse()
      const testE = verifNombreBienEcrit(stor.repEleveE, ds.Espace, true)
      if (!testE.good) {
        if (stor.errEcrit.indexOf(testE.remede) === -1) stor.errEcrit.push(testE.remede)
        stor.zoneE.corrige(false)
        oke = false
      }
      if (oke) {
        stor.repEleveE = testE.nb
        if (j3pArrondi(stor.etmul.num * stor.nbdep / stor.etmul.den, 6) !== stor.repEleveE) {
          stor.errRep = true
          stor.zoneE.corrige(false)
          oke = false
        }
      }
    }
    return okg && oke
  }

  function creeLesDiv () {
    stor.lesdiv = {}
    stor.lesdiv.conteneur = j3pAddElt(me.zones.MG, 'div', { style: me.styles.etendre('toutpetit.enonce', { padding: '10px' }) })
    stor.lesdiv.consigne = addDefaultTable(stor.lesdiv.conteneur, 1, 1)[0][0]
    stor.lesdiv.travail = addDefaultTable(stor.lesdiv.conteneur, 1, 1)[0][0]
    stor.lesdiv.explications = addDefaultTable(stor.lesdiv.conteneur, 1, 1)[0][0]
    stor.lesdiv.solution = addDefaultTable(stor.lesdiv.conteneur, 1, 1)[0][0]
    stor.lesdiv.calculatrice = j3pAddElt(me.zones.MD, 'div', { style: me.styles.etendre('toutpetit.enonce', { padding: '10px' }) })
    stor.lesdiv.correction = j3pAddElt(me.zones.MD, 'div', { style: me.styles.etendre('toutpetit.enonce', { padding: '10px' }) })
    stor.lesdiv.explications.style.color = me.styles.cfaux
    stor.lesdiv.solution.style.color = me.styles.petit.correction.color
    stor.lesdiv.travail.classList.add('travail')
    stor.lesdiv.consigne.classList.add('enonce')
    me.zonesElts.MG.classList.add('fond')
  }
  function getDonnees () {
    return {
      typesection: 'college', //

      // Nombre de répétitions de l’exercice
      nbrepetitions: 9,
      nbetapes: 1,

      limite: 0,
      Espace: true,
      cm: true,
      cm_2: true,
      cm_3: true,
      h_m_s: true,
      j_m_a: true,
      Litre: true,
      gramme: true,
      Lien: 'les deux',
      Calculatrice: true,

      // Si indication !== "" alors un lien "Indication" s’affiche dans la zone Inférieure Gauche (zones.IG
      // une indication<br>de deux lignes...";
      indication: '',

      // pour le cas de presentation1bis
      // En effet pas d’indication possible pour cette présentation
      // surchargeindication: false;

      // Nombre de chances dont dispose l’él§ve pour répondre à la question
      nbchances: 2,

      structure: 'presentation1bis', //  || "presentation2" || "presentation3"  || "presentation1bis"

      // Paramètres de la section, avec leurs valeurs par défaut.

      /*
          Les textes présents dans la section
          Sont donc accessibles dans le code par donneesSection.textes.phrase1
          Possibilité d’écrire dans le code :
          var lestextes: donneesSection.textes;
          puis accès à l’aide de lestextes.phrase1
          */
      textes: {

        aide: 'Attention !',
        aidezero: 'Clique sur un zéro pour le supprimer.',
        aideesp: 'Clique sur une espace par la faire disparaître. <BR> Clique sous deux chiffres pour insérer une espace.',
        soluce: 'Il fallait écrire $£a$ .',
        AttVirg: 'Tu ne peux pas supprimer la virgule, <BR> Il reste des chiffres dans la partie décimale !',
        consigne: '<b>Ecris en chiffres ce nombre:</b> <BR> £a.',
        consigne3: ' $N: £a$ <BR><BR><BR>',
        consigne2bis: 'Ecris correctement ce nombre: $£a$',
        errtropvirgule: 'Il ne peut y avoir plusieurs virgules dans un nombre !',
        errvirguleseule: 'Il ne peut pas y avoir de virgule sans partie décimale !',
        errespace: 'Dans la partie entière, les chiffres doivent être regroupés par 3 en commençant par la droite !',
        errespacedec: 'Dans la partie décimale, les chiffres doivent être regroupés par 3 en commençant par la gauche !',
        errDeprang2: 'Supprime les zéros inutiles !',
        errespVirg: 'Il ne doit pas y avoir d’espace à côté de la virgule !',
        errespVirg2: 'Il ne doit pas y avoir d’espace à la fin de la partie entière !',
        err2esp: 'Tu as mis 2 espaces côte à côte !',
        errZerosEnMoins: 'Ces deux nombres ne sont pas égaux !',
        errZerosInutTrop: 'Tu dois supprimer tous les zéros inutiles !'

      },

      // Par défaut, pas de temps limite. Décommentez pour avoir un temps limite par défaut. En secondes.

      // limite: 10;
      /*
          Phrase d’état renvoyée par la section
          Si pe=0 alors la section est quantitative et renvoie un score entre 0 et 1.
          Dans le cas où la section est qualitative, lister les différents pe renvoyées :
              pe_1: "toto"
              pe_2: "tata"
              etc
          Dans la partie navigation, il faudra affecter la variable parcours.pe à l’une des précédentes.
          Par exemple,
          parcours.pe: donneesSection.pe_2
          */
      pe: 0
    }
  }
  function PoseQuestion () {
    j3pAffiche(stor.lesdiv.consigne, null, 'V est une valeur exprimée en ' + stor.nom + '\n')
    j3pAffiche(stor.lesdiv.consigne, null, 'V = $' + ecrisBienMathquill(stor.nbdep) + '$ ' + stor.lUnit + '\n')
    j3pAffiche(stor.lesdiv.consigne, null, '<b>Convertis V en ' + stor.lunitAr + '</b>\n\n')
    stor.tavfdsg = addDefaultTable(stor.lesdiv.travail, 1, 3)
    stor.tavfdsg[0][1].style.width = '40px'
    stor.tavg = addDefaultTable(stor.tavfdsg[0][0], 3, 2)
    j3pAffiche(stor.tavg[0][0], null, 'V = &nbsp;')
    j3pAffiche(stor.tavg[0][1], null, '$' + ecrisBienMathquill(stor.nbdep) + '$ ' + stor.lUnit)
    const tavg2 = addDefaultTable(stor.tavg[2][1], 1, 2)
    j3pAffiche(tavg2[0][1], null, '&nbsp;' + stor.lunitAr)
    j3pAffiche(stor.tavg[2][0], null, 'V = &nbsp;')
    stor.zoneG = new ZoneStyleMathquill1(tavg2[0][0], { restric: '0123456789., ', enter: function () { me.sectionCourante() } })
    stor.zoneE = undefined
    j3pAjouteBouton(stor.tavfdsg[0][2], makeEtape, { value: 'Ajouter une étape' })
    stor.zoneG.focus()
  }

  function makeEtape () {
    j3pModale({ titre: 'Ajouter une étape', contenu: '' })
    const jk = addDefaultTable('modale', 5, 1)
    jk[1][0].style.height = '20px'
    jk[3][0].style.height = '20px'
    const elem = $('.croix')[0]
    j3pDetruit(elem)
    j3pAjouteBouton(jk[0][0], makeUni1, { value: 'Utiliser ' + stor.pass1 })
    j3pAjouteBouton(jk[2][0], makeUni2, { value: 'Utiliser ' + stor.pass2 })
    j3pAjouteBouton(jk[4][0], annule, { value: 'Annuler ' })
  }
  function annule () {
    j3pDetruit('modale')
    j3pDetruit('j3pmasque')
  }
  function makeUni1 () {
    const tavg2 = addDefaultTable(stor.tavg[1][1], 1, 2)
    j3pAffiche(tavg2[0][1], null, '&nbsp;' + stor.pass1)
    j3pAffiche(stor.tavg[1][0], null, 'V = &nbsp;')
    j3pDetruit('modale')
    j3pDetruit('j3pmasque')
    j3pEmpty(stor.tavfdsg[0][2])
    stor.zoneE = new ZoneStyleMathquill1(tavg2[0][0], { restric: '0123456789., ', enter: function () { me.sectionCourante() } })
    j3pAjouteBouton(stor.tavfdsg[0][2], suppri, { value: 'Supprimer l’étape' })
    if (stor.lien === 'p') {
      stor.etmul = stor.mul2
    } else {
      stor.etmul = { num: stor.mul2.den, den: stor.mul2.num }
    }
  }
  function makeUni2 () {
    const tavg2 = addDefaultTable(stor.tavg[1][1], 1, 2)
    j3pAffiche(tavg2[0][1], null, '&nbsp;' + stor.pass2)
    j3pAffiche(stor.tavg[1][0], null, 'V = &nbsp;')
    j3pDetruit('modale')
    j3pDetruit('j3pmasque')
    j3pEmpty(stor.tavfdsg[0][2])
    stor.zoneE = new ZoneStyleMathquill1(tavg2[0][0], { restric: '0123456789., ', enter: function () { me.sectionCourante() } })
    j3pAjouteBouton(stor.tavfdsg[0][2], suppri, { value: 'Supprimer l’étape' })
    stor.etmul = stor.mul1
  }
  function suppri () {
    j3pEmpty(stor.tavfdsg[0][2])
    j3pAjouteBouton(stor.tavfdsg[0][2], makeEtape, { value: 'Ajouter une étape' })
    stor.zoneE.disable()
    j3pEmpty(stor.tavg[1][0])
    j3pEmpty(stor.tavg[1][1])
    stor.zoneE = undefined
  }
  function afcofo (boolfin) {
    if (stor.errEcrit.length > 0) {
      stor.yaexplik = true
      for (let i = 0; i < stor.errEcrit.length; i++) {
        j3pAffiche(stor.lesdiv.explications, null, stor.errEcrit[i] + '\n')
      }
    }

    if (boolfin) {
      barrelesfo()
      desatout()
      j3pAffiche(stor.lesdiv.solution, null, 'V = $' + ecrisBienMathquill(stor.nbdep) + '$ ' + stor.lUnit + '\n')
      let res = stor.nbdep * stor.mul1.num / stor.mul1.den
      if (j3pArrondi(res, 0) === res) {
        j3pAffiche(stor.lesdiv.solution, null, 'V = $' + ecrisBienMathquill(res) + '$ ' + stor.pass2 + '\n')
      } else {
        if (stor.lien === 'q') {
          res = stor.nbdep * stor.mul2.den / stor.mul2.num
        } else {
          res = stor.nbdep * stor.mul2.num / stor.mul2.den
        }
        j3pAffiche(stor.lesdiv.solution, null, 'V = $' + ecrisBienMathquill(j3pArrondi(res, 6)) + '$ ' + stor.pass1 + '\n')
      }

      j3pAffiche(stor.lesdiv.solution, null, 'V = $' + ecrisBienMathquill(stor.nbfin) + '$ ' + stor.lunitAr + '\n')
    } else { me.afficheBoutonValider() }
  }

  function desatout () {
    if (stor.zoneE !== undefined) stor.zoneE.disable()
    stor.zoneG.disable()
    j3pEmpty(stor.tavfdsg[0][2])
  }
  function boutonfen () {
    j3pToggleFenetres('Calculatrice')
  }
  function barrelesfo () {
    stor.zoneG.barreIfKo()
    // zoneE existe pas forcément
    stor.zoneE?.barreIfKo()
  }

  function initSection () {
    me.donneesSection = getDonnees()
    ds = me.donneesSection

    me.surcharge()
    // Construction de la page
    me.construitStructurePage(me.donneesSection.structure)

    ds.nbitems = ds.nbetapes * ds.nbrepetitions

    me.typederreurs = [0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0]

    me.score = 0

    me.afficheTitre('Convertir des unités composées')
    let lp = []
    if (ds.cm) lp.push('cm')
    if (ds.cm_2) lp.push('cm²')
    if (ds.cm_3) lp.push('cm³')
    if (ds.Litre) lp.push('L')
    if (ds.h_m_s) lp.push('h_m_s')
    if (ds.j_m_a) lp.push('j_m_a')
    if (ds.gramme) lp.push('g')
    if (lp.length < 2) lp = ['cm', 'h_m_s']
    let lp2 = []
    for (let i = 0; i < lp.length; i++) {
      for (let j = 0; j < lp.length; j++) {
        if (i !== j) lp2.push({ u1: lp[i], u2: lp[j] })
      }
    }
    lp2 = j3pShuffle(lp2)
    ds.lesExos = []
    for (let i = 0; i < ds.nbitems; i++) {
      ds.lesExos.push({ units: lp2[i % lp2.length] })
    }
    ds.lesExos = j3pShuffle(ds.lesExos)

    lp = []
    if (ds.Lien === 'les deux' || ds.Lien === 'produit') lp.push('p')
    if (ds.Lien === 'les deux' || ds.Lien === 'quotient') lp.push('q')
    lp = j3pShuffle(lp)
    for (let i = 0; i < ds.nbitems; i++) {
      ds.lesExos[i].lien = lp[i % lp.length]
    }
    ds.lesExos = j3pShuffle(ds.lesExos)

    enonceMain()
  }
  function metoutvert () {
    if (stor.zoneE !== undefined) stor.zoneE.corrige(true)
    stor.zoneG.corrige(true)
  }
  function enonceMain () {
    // A chaque répétition, on nettoie la scène
    // A commenter si besoin

    me.videLesZones()

    faisChoixExo()

    creeLesDiv()

    PoseQuestion()
    j3pEmpty(stor.lesdiv.calculatrice)
    if (ds.Calculatrice) {
      me.fenetresjq = [{ name: 'Calculatrice', title: 'Une calculatrice', left: 600, top: 10 }]
      j3pCreeFenetres(me)
      j3pAfficheCroixFenetres('Calculatrice')
      j3pAjouteBouton(stor.lesdiv.calculatrice, 'Calculatrice', 'MepBoutons', 'Calculatrice', boutonfen)
    }
    me.finEnonce()
  }

  switch (this.etat) {
    case 'enonce':
      // code exécuté au lancement de la section
      if (this.debutDeLaSection) {
        initSection()
      } else {
        enonceMain()
      }
      break // case "enonce":

    case 'correction':
      // On teste si une réponse a été saisie
      if (ds.theme === 'zonesAvecImageDeFond') {
        stor.lesdiv.explications.classList.remove('explique')
        stor.lesdiv.solution.classList.remove('correction')
        stor.lesdiv.correction.classList.remove('feedback')
      }
      j3pEmpty(stor.lesdiv.explications)

      if (!yareponse()) {
        // ici on donne le message à afficher si une ou plusieurs zone(s) n’est pas complétée (tout d’abord on met en rouge : styles.cfaux)
        stor.lesdiv.correction.style.color = this.styles.cfaux
        stor.lesdiv.correction.innerHTML = reponseIncomplete
        me.afficheBoutonValider()
        return
      } else { // Une réponse a été saisie
        stor.yaexplik = false
        stor.yaco = false
        // Bonne réponse
        if (isRepOk()) {
          this._stopTimer()
          this.score++
          stor.lesdiv.correction.style.color = this.styles.cbien
          stor.lesdiv.correction.innerHTML = cBien

          metoutvert()
          desatout()

          this.typederreurs[0]++
          this.cacheBoutonValider()
          this.etat = 'navigation'
          this.sectionCourante()
        } else {
          // Pas de bonne réponse
          stor.lesdiv.correction.style.color = this.styles.cfaux
          // A cause de la limite de temps :
          if (this.isElapsed) { // limite de temps
            stor.lesdiv.correction.innerHTML = tempsDepasse
            this.typederreurs[10]++
            this.cacheBoutonValider()
            afcofo(true)
            this.etat = 'navigation'
            this.sectionCourante()
          } else { // Réponse fausse :
            stor.lesdiv.correction.innerHTML = cFaux

            if (this.essaiCourant < this.donneesSection.nbchances) {
              stor.lesdiv.correction.innerHTML += '<br>' + essaieEncore
              afcofo(false)
              this.typederreurs[1]++
              // indication éventuelle ici
            } else { // Erreur au nème essai
              this._stopTimer()
              this.cacheBoutonValider()
              afcofo(true)
              stor.lesdiv.correction.innerHTML += '<br>' + regardeCorrection
              this.typederreurs[2]++
              this.etat = 'navigation'
              this.sectionCourante()
            }
          }
        }
      }

      if (ds.theme === 'zonesAvecImageDeFond') {
        if (stor.yaexplik) stor.lesdiv.explications.classList.add('explique')
        if (stor.yaco) stor.lesdiv.solution.classList.add('correction')
        stor.lesdiv.correction.classList.add('feedback')
      }
      // Obligatoire
      this.finCorrection()
      break // case "correction":

    case 'navigation':
      if (this.sectionTerminee()) {
        // On détermine la phrase d’état renvoyée par la section
        const sco = this.score / this.donneesSection.nbitems
        let peibase = 0
        if (sco >= 0.8) { peibase = 0 }
        if ((sco >= 0.55) && (sco < 0.8)) { peibase = 1 }
        if ((sco >= 0.3) && (sco < 0.55)) { peibase = 4 }
        if (sco < 0.3) { peibase = 7 }
        let peisup = 0
        let compt = this.typederreurs[2] / 4
        if (this.typederreurs[6] > compt) {
          peisup = 2
          compt = this.typederreurs[6]
        }
        if (this.typederreurs[3] > compt) {
          peisup = 1
        }
        this.parcours.pe = this.donneesSection.pe[peibase + peisup]
      } else {
        this.etat = 'enonce'
      }
      this.finNavigation()
      break // case "navigation":
  }
}
