import { j3pAddElt, j3pGetRandomBool, j3pGetRandomInt, j3pPGCD, j3pShowError, j3pShuffle } from 'src/legacy/core/functions'
import ListeDeroulante from 'src/legacy/outils/listeDeroulante/ListeDeroulante'
import { j3pAffiche } from 'src/lib/mathquill/functions'
import { addDefaultTable } from 'src/legacy/themes/table'
import textesGeneriques from 'src/lib/core/textes'

const { cBien, cFaux, essaieEncore, reponseIncomplete, tempsDepasse } = textesGeneriques

/**
 * Ajouter une description ici
 * @fileOverview
 * @author Tommy Barroy <t.barroy@laposte.net>
 * @since 2017-10
*/
export const params = {
  outils: ['mathquill'],
  /**
   * Ce qui est paramétrable dans l’éditeur de graphe
   * @type {LegacySectionParamsParametres}
   */
  parametres: [
    ['nbrepetitions', 3, 'entier', 'Nombre de répétitions de la section'],
    ['nbchances', 1, 'entier', 'Nombre de chances'],
    ['limite', 0, 'entier', 'Temps disponible par essai.'],
    ['Nombre_operations', 3, 'entier', 'Nombre d’opérations (max 3)'],
    ['produit', true, 'boolean', '<u>true</u>:  La réponse peut être un produit'],
    ['quotient', true, 'boolean', '<u>true</u>:  La réponse peut être un quotient'],
    ['somme', true, 'boolean', '<u>true</u>:  La réponse peut être une somme'],
    ['difference', true, 'boolean', '<u>true</u>:  La réponse peut être une différence'],
    ['Positifs', false, 'boolean', '<u>true</u>: Les nombres sont forcément positifs.'],
    ['Fractions', true, 'boolean', '<u>true</u>: L’expression peut comporter des fractions.'],
    // le modèle gère la compatibilité ascendante avec l’ancien paramètre MiseEnPagePlusClaire
    ['theme', 'standard', 'liste', 'Modèle pour l’affichage', ['standard', 'zonesAvecImageDeFond']]

  ]
}

/**
 * section nature
 * @this {Parcours}
 */
export default function main () {
  const me = this
  const stor = this.storage
  const ds = me.donneesSection

  function colle (qui, quoi) {
  // quoi c’est un tab avec les differents elements possibles a coller
  // qui c’est un chaine
    const tabret = []
    for (let i = 0; i < quoi.length; i++) {
      tabret.push(qui + quoi[i])
    }
    return tabret
  // returne un tab avec lachaine + les differents element
  }

  function desactivezone () {
    stor.liste.disable()
  }
  function passetoutvert () {
    stor.liste.corrige(true)
  }

  function creeLesDiv () {
    stor.lesdiv = {}
    stor.lesdiv.conteneur = j3pAddElt(me.zonesElts.MG, 'div', {
      style: me.styles.etendre('toutpetit.enonce', { padding: '10px' })
    })
    const consigneG = addDefaultTable(stor.lesdiv.conteneur, 1, 3)
    stor.lesdiv.correction = consigneG[0][2]
    stor.lesdiv.consigne = consigneG[0][0]
    consigneG[0][1].style.width = '20px'
    stor.lesdiv.travail = addDefaultTable(stor.lesdiv.conteneur, 1, 1)[0][0]
    stor.lesdiv.explications = j3pAddElt(stor.lesdiv.conteneur, 'div')
    stor.lesdiv.solution = j3pAddElt(stor.lesdiv.conteneur, 'div')
    stor.lesdiv.solution.style.color = me.styles.petit.correction.color
    stor.lesdiv.explications.style.color = me.styles.cfaux
    me.zonesElts.MG.classList.add('fond')
  }
  function faisChoixExos () {
    const e = ds.lesExos.pop()
    e.frac = []
    for (let i = 0; i < ds.Nombre_operations + 1; i++) {
      let num = j3pGetRandomInt(2, 9)
      if ((!ds.Positifs) && (j3pGetRandomBool())) {
        num = -num
      }
      let den
      do {
        den = j3pGetRandomInt(2, 9)
      } while (j3pPGCD(den, Math.abs(num)) !== 1)
      e.frac[i] = { num, den }
    }
    stor.forcebool = [false, false, false, false, false]

    for (let i = 0; i < e.frac.length; i++) {
      if (!ds.Fractions || j3pGetRandomInt(0, 2) < 2) {
        e.frac[i].den = 'z'
        stor.forcebool[i] = true
      }
    }
    return e
  }
  function yaReponse () {
    if (me.isElapsed) return true
    if (!stor.liste.changed) {
      stor.liste.focus()
      return false
    }
    return true
  }
  function isRepOk () {
    const e = stor.liste.reponse
    const f = stor.calculeencours.prop[stor.calculeencours.prop.length - 1]
    let f2 = 'M'
    if (stor.calculeencours.prop.length > 1) f2 = stor.calculeencours.prop[stor.calculeencours.prop.length - 2]
    if (e === 'une somme.') {
      return (f === '+') && (f2 !== '-')
    }
    if (e === 'une différence.') {
      return (f === '-') && (f2 !== '-') && (f2 !== '+')
    }
    if (e === 'un produit.') {
      return (f === 'x')
    }
    if (e === 'un quotient.') {
      return (f === '/')
    }
    return false
  }

  function initSection () {
    let tabuf
    // Construction de la page
    me.construitStructurePage('presentation3')

    if (ds.Nombre_operations < 1) { ds.Nombre_operations = 1 }
    if (ds.Nombre_operations > 3) { ds.Nombre_operations = 3 }

    ds.lesExos = []
    stor.montab = ['Choisir']

    stor.lp = []

    const lp = ['+', '-', 'x', '/']
    let lp2 = []

    if (ds.produit) {
      lp2.push('x')
      stor.montab.push('un produit.')
    }
    if (ds.quotient) {
      lp2.push('/')
      stor.montab.push('un quotient.')
    }
    if (ds.somme) {
      lp2.push('+')
      stor.montab.push('une somme.')
    }
    if (ds.difference) {
      lp2.push('-')
      stor.montab.push('une différence.')
    }

    if (lp2.length === 0) {
      j3pShowError('problème de paramétrage')
      lp2 = ['+', '-']
      stor.montab = ['une somme', 'une différence']
    }
    for (let i = 0; i < lp.length; i++) { stor.lp[i] = lp[i] }

    for (let i = 1; i < ds.Nombre_operations - 1; i++) {
      tabuf = []
      for (let j = 0; j < stor.lp.length; j++) {
        tabuf = tabuf.concat(colle(stor.lp[j], lp))
      }
      stor.lp = []
      for (let j = 0; j < tabuf.length; j++) { stor.lp[j] = tabuf[j] }
    }

    const tb = []
    for (let i = stor.lp.length - 1; i > -1; i--) {
      for (let j = 0; j < lp2.length; j++) {
        if (lp2[j] === 'sa') {
          tb.push(stor.lp[i] + '+-')
          tb.push(stor.lp[i] + '-+')
          tb.push(stor.lp[i] + '--')
        } else {
          tb.push(stor.lp[i] + lp2[j])
        }
      }
    }

    for (let i = tb.length - 1; i > -1; i--) {
      if (tb[i].length > ds.Nombre_operations) {
        tb[i] = tb[i].substring(1)
      }
    }

    for (let i = tb.length - 1; i > -1; i--) {
      if ((tb[i].replace('/', '').indexOf('/') !== -1) ||
      (tb[i].indexOf('+++') !== -1) ||
        (tb[i].indexOf('--') !== -1) ||
        (tb[i].indexOf('+-+') !== -1) ||
        (tb[i].indexOf('+--') !== -1) ||
        (tb[i].indexOf('++-') !== -1) ||
        (tb[i].indexOf('-++') !== -1) ||
        (tb[i].indexOf('-+-') !== -1) ||
        (tb[i].indexOf('--+') !== -1) ||
        (tb[i].indexOf('xxx') !== -1)) {
        tb.splice(i, 1)
      }
    }

    if (ds.Nombre_operations > 1) {
      for (let i = tb.length - 1; i > -1; i--) {
        if (((tb[i][tb[i].length - 1] === '-') && ((tb[i][tb[i].length - 2] === '+') || (tb[i][tb[i].length - 2] === '-'))) ||
          ((tb[i][tb[i].length - 1] === '+') && (tb[i][tb[i].length - 2] === '-'))) {
          tb.splice(i, 1)
        }
      }
    }
    stor.lp = j3pShuffle(tb)

    for (let i = 0; i < ds.nbitems; i++) {
      ds.lesExos[i] = { prop: stor.lp[i % (stor.lp.length)] }
    }

    stor.larestric = '0123456789'
    stor.clvspe = false
    if (!ds.Positifs) {
      stor.larestric += '-()'
      stor.clvspe = true
    }

    me.typederreurs = [0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0]

    me.score = 0

    me.afficheTitre('Nature d’une expression')
  }
  function AffCorrection (bool) {
    stor.liste.corrige(false)
    stor.yaexplik = true
    switch (stor.liste.reponse) {
      case 'une somme.':
        j3pAffiche(stor.lesdiv.explications, null, 'Une somme est le résultat d’une addition !')
        break
      case 'une différence.':
        j3pAffiche(stor.lesdiv.explications, null, 'Une différence est le résultat d’une soustraction !')
        break
      case 'un produit.':
        j3pAffiche(stor.lesdiv.explications, null, 'Un produit est le résultat d’une multiplication !')
        break
      case 'un quotient.':
        j3pAffiche(stor.lesdiv.explications, null, 'Un quotient est le résultat d’une division !')
        break
    }
    if (bool) {
      stor.liste.barre()
      stor.liste.disable()
      affso()
    } else {
      me.afficheBoutonValider()
    }
  }
  function poseQuestion () {
    let unbool
    stor.listebool = []
    stor.listepar = []
    stor.calculeencours = {}
    stor.calculeencours.prop = stor.Lexo.prop
    stor.calculeencours.frac = []
    for (let i = 0; i < stor.Lexo.frac.length; i++) {
      stor.calculeencours.frac[i] = { num: stor.Lexo.frac[i].num, den: stor.Lexo.frac[i].den }
    }
    for (let i = 0; i < stor.Lexo.prop.length; i++) {
      switch (stor.Lexo.prop[i]) {
        case '+':
          stor.listebool.push(j3pGetRandomBool())
          if (i < stor.Lexo.prop.length - 1) {
            if (stor.Lexo.prop[i + 1] === 'x') {
              stor.listepar.push(true)
            } else {
              stor.listepar.push(false)
            }
          } else {
            stor.listepar.push(false)
          }
          break
        case '-':
          stor.listebool.push(j3pGetRandomBool())
          if (i < stor.Lexo.prop.length - 1) {
            if (stor.Lexo.prop[i + 1] === 'x') {
              stor.listepar.push(true)
            } else {
              stor.listepar.push(false)
            }
          } else {
            stor.listepar.push(false)
          }
          break
        case 'x':
          stor.listepar.push(false)
          unbool = j3pGetRandomBool()
          if (i > 0) {
            if (stor.Lexo.prop[i - 1] === 'x') {
              unbool = true
            }
          }
          stor.listebool.push(unbool)
          break
        case '/':
          stor.listepar.push(false)
          stor.listebool.push(j3pGetRandomBool())
          break
      }
      if (stor.forcebool[i]) stor.listebool[i] = true
    }
    j3pAffiche(stor.lesdiv.consigne, null, '<b>Donne la nature de l’expression A </b>\n\n')
    stor.divA = addDefaultTable(stor.lesdiv.travail, 1, 2)
    affichetoutlecalc()
    j3pAffiche(stor.divA[0][0], null, 'L’expression $A$ est &nbsp;')
    stor.liste = ListeDeroulante.create(stor.divA[0][1], stor.montab)
  }
  function affichetoutlecalc () {
    j3pAffiche(stor.lesdiv.consigne, null, '$ A = ' + affop(stor.calculeencours, stor.listebool, stor.listepar) + '$')
  }

  function affop (calculeencours, listebool, listepar) {
    let v = false
    let aret, f1, f2
    aret = ''
    for (let i = calculeencours.prop.length - 1; i > -1; i--) {
      if (!listebool[i]) {
        if (calculeencours.frac[i + 1].den !== 'z') {
          f1 = '\\frac{' + calculeencours.frac[i + 1].num + '}{' + calculeencours.frac[i + 1].den + '}'
        } else {
          if (calculeencours.frac[i + 1].num < 0) {
            f1 = '(' + calculeencours.frac[i + 1].num + ')'
          } else {
            f1 = calculeencours.frac[i + 1].num
          }
        }
        f2 = 'XX'
      } else {
        if (calculeencours.frac[i + 1].den !== 'z') {
          f2 = '\\frac{' + calculeencours.frac[i + 1].num + '}{' + calculeencours.frac[i + 1].den + '}'
        } else {
          if (calculeencours.frac[i + 1].num < 0) {
            f2 = '(' + calculeencours.frac[i + 1].num + ')'
          } else {
            f2 = calculeencours.frac[i + 1].num
          }
        }
        f1 = 'XX'
      }

      switch (calculeencours.prop[i]) {
        case '+':
          if (!v) {
            v = true
            aret = f1 + ' + ' + f2
          } else {
            if (listepar[i]) {
              aret = aret.replace('XX', '(' + f1 + ' + ' + f2 + ')')
            } else {
              aret = aret.replace('XX', f1 + ' + ' + f2)
            }
          }
          break
        case '-':
          if (!v) {
            v = true
            aret = f1 + ' - ' + f2
          } else {
            if (listepar[i]) {
              aret = aret.replace('XX', '(' + f1 + ' - ' + f2 + ')')
            } else {
              aret = aret.replace('XX', f1 + ' - ' + f2)
            }
          }
          break
        case 'x':
          if (!v) {
            v = true
            aret = f1 + ' \\times ' + f2
          } else {
            aret = aret.replace('XX', f1 + ' \\times ' + f2)
          }
          break
        case '/':
          if (!v) {
            v = true
            aret = '\\frac{' + f1 + '}{' + f2 + '}'
          } else {
            aret = aret.replace('XX', '\\frac{' + f1 + '}{' + f2 + '}')
          }
          break
      }
    }
    if (calculeencours.frac[0].den !== 'z') {
      if (!v) {
        aret = '\\frac{' + calculeencours.frac[0].num + '}{' + calculeencours.frac[0].den + '}'
      } else {
        aret = aret.replace('XX', '\\frac{' + calculeencours.frac[0].num + '}{' + calculeencours.frac[0].den + '}')
      }
    } else {
      if (!v) {
        aret = calculeencours.frac[0].num
      } else {
        if (calculeencours.frac[0].num < 0) {
          aret = aret.replace('XX', '(' + calculeencours.frac[0].num + ')')
        } else {
          aret = aret.replace('XX', calculeencours.frac[0].num)
        }
      }
    }
    return aret
  }
  function copiedecalcul () {
    const e = stor.calculeencours
    const ob1 = { prop: '', bool: [true], par: [false], frac: [{ num: e.frac[e.prop.length].num, den: e.frac[e.prop.length].den }] }
    const ob2 = {}
    ob2.prop = e.prop.substring(0, e.prop.length - 1)
    ob2.bool = []
    ob2.par = []
    ob2.frac = []
    for (let i = 0; i < ob2.prop.length; i++) {
      ob2.bool[i] = stor.listebool[i]
      ob2.par[i] = stor.listepar[i]
      ob2.frac[i] = { num: e.frac[i].num, den: e.frac[i].den }
    }
    ob2.frac[ob2.prop.length] = { num: e.frac[ob2.prop.length].num, den: e.frac[ob2.prop.length].den }
    if (!stor.listebool[stor.listebool.length - 1]) {
      return [ob1, ob2]
    }
    return [ob2, ob1]
  }
  function copiedecalcul2 () {
    const e = stor.calculeencours
    const ob1 = { prop: '', bool: [true], par: [false], frac: [{ num: e.frac[e.prop.length].num, den: e.frac[e.prop.length].den }] }
    const ob2 = { prop: '', bool: [true], par: [false], frac: [{ num: e.frac[e.prop.length - 1].num, den: e.frac[e.prop.length - 1].den }] }
    const ob3 = {}
    ob3.prop = e.prop.substring(0, e.prop.length - 2)
    ob3.bool = []
    ob3.par = []
    ob3.frac = []
    for (let i = 0; i < ob3.prop.length; i++) {
      ob3.bool[i] = stor.listebool[i]
      ob3.par[i] = stor.listepar[i]
      ob3.frac[i] = { num: e.frac[i].num, den: e.frac[i].den }
    }
    ob3.frac[ob3.prop.length] = { num: e.frac[ob3.prop.length].num, den: e.frac[ob3.prop.length].den }
    if (!stor.listebool[stor.listebool.length - 1]) {
      if (!stor.listebool[stor.listebool.length - 2]) {
        return [ob1, ob2, ob3]
      }
      return [ob1, ob3, ob2]
    }
    if (!stor.listebool[stor.listebool.length - 2]) {
      return [ob2, ob3, ob1]
    }
    return [ob3, ob2, ob1]
  }
  function affso () {
    let buf1
    let buf2
    let buf3 = ''
    let fob = false
    stor.yaco = true
    const tt = addDefaultTable(stor.lesdiv.solution, 1, 8)
    tt[0][2].style.color = '#000000'
    tt[0][4].style.color = '#000000'
    tt[0][6].style.color = '#000000'
    j3pAffiche(tt[0][0], null, '$A$ est ')
    switch (stor.calculeencours.prop[stor.calculeencours.prop.length - 1]) {
      case '+':
        if (stor.calculeencours.prop.length > 1) {
          if (stor.calculeencours.prop[stor.calculeencours.prop.length - 2] === '+') {
            fob = true
            buf1 = ' la <b>somme</b> du terme '
            buf2 = ' , du terme '
            buf3 = ' et du terme '
          } else if (stor.calculeencours.prop[stor.calculeencours.prop.length - 2] === '-') {
            fob = true
            buf1 = ' une <b>somme algébrique</b> composée du terme '
            buf2 = ' , du terme '
            buf3 = ' et du terme '
          } else {
            buf1 = ' la <b>somme</b> du terme '
            buf2 = ' et du terme '
          }
        } else {
          buf1 = ' la <b>somme</b> du terme '
          buf2 = ' et du terme '
        }
        break
      case '-':
        if (stor.calculeencours.prop.length > 1) {
          if ('+-'.indexOf(stor.calculeencours.prop[stor.calculeencours.prop.length - 2]) !== -1) {
            fob = true
            buf1 = ' une <b>somme algébrique</b> composée du terme '
            buf2 = ' , du terme '
            buf3 = ' et du terme '
          } else {
            buf1 = ' la <b>différence</b> entre le terme '
            buf2 = ' et le terme '
          }
        } else {
          buf1 = ' la <b>différence</b> entre le terme '
          buf2 = ' et le terme '
        }
        break
      case 'x':
        if (stor.calculeencours.prop.length > 1) {
          if (stor.calculeencours.prop[stor.calculeencours.prop.length - 2] === 'x') {
            fob = true
            buf1 = ' le <b>produit</b> du facteur '
            buf2 = ' , du facteur '
            buf3 = ' et du facteur '
          } else {
            buf1 = ' le <b>produit</b> entre le facteur '
            buf2 = ' et le facteur '
          }
        } else {
          buf1 = ' le <b>produit</b> entre le facteur '
          buf2 = ' et le facteur '
        }
        break
      case '/':
        buf1 = ' le <b>quotient</b> entre le dividende '
        buf2 = ' et le diviseur '
        break
    }
    j3pAffiche(tt[0][1], null, '&nbsp;' + buf1 + '&nbsp;')
    j3pAffiche(tt[0][3], null, '&nbsp;' + buf2 + '&nbsp;')
    j3pAffiche(tt[0][5], null, '&nbsp;' + buf3 + '&nbsp;')
    j3pAffiche(tt[0][7], null, ' .')
    const x = fob ? copiedecalcul2() : copiedecalcul()
    j3pAffiche(tt[0][2], null, '$' + affop(x[0], x[0].bool, x[0].par) + '$')
    j3pAffiche(tt[0][4], null, '$' + affop(x[1], x[1].bool, x[1].par) + '$')
    if (fob) {
      j3pAffiche(tt[0][6], null, '$' + affop(x[2], x[2].bool, x[2].par) + '$')
    }
  } // affso

  switch (this.etat) {
    case 'enonce':
      // code exécuté au lancement de la section
      if (me.debutDeLaSection) {
        initSection()
      }

      if (me.etapeCourante === 1) {
        // A chaque répétition, on nettoie la scène
        me.videLesZones()
      }

      creeLesDiv()

      stor.Lexo = faisChoixExos()

      stor.lesdiv.travail.classList.add('travail')
      stor.lesdiv.consigne.classList.add('enonce')

      poseQuestion()

      me.afficheBoutonValider()
      this.finEnonce()

      break // case "enonce":

    case 'correction':
      // On teste si une réponse a été saisie
      stor.yaexplik = false
      stor.yaco = false
      stor.lesdiv.explications.innerHTML = ''
      stor.lesdiv.solution.innerHTML = ''
      stor.lesdiv.correction.innerHTML = ''
      stor.lesdiv.explications.classList.remove('explique')

      if (yaReponse()) {
        // Bonne réponse
        if (isRepOk()) {
          this._stopTimer()
          this.score++
          stor.lesdiv.correction.style.color = this.styles.cbien
          stor.lesdiv.correction.innerHTML = cBien

          desactivezone()
          passetoutvert()
          affso()

          this.typederreurs[0]++
          this.cacheBoutonValider()
          this.etat = 'navigation'
          this.sectionCourante()
        } else {
          // Pas de bonne réponse
          stor.lesdiv.correction.style.color = this.styles.cfaux
          stor.lesdiv.correction.innerHTML = cFaux
          if (this.isElapsed) {
            // A cause de la limite de temps :
            this._stopTimer()

            stor.lesdiv.correction.innerHTML = tempsDepasse + '<BR>'
            this.typederreurs[10]++
            this.cacheBoutonValider()

            AffCorrection(true)

            this.etat = 'navigation'
            this.sectionCourante()
          } else {
            // Réponse fausse :
            if (me.essaiCourant < ds.nbchances) {
              stor.lesdiv.correction.innerHTML += '<br>' + essaieEncore

              AffCorrection(false)

              this.typederreurs[1]++
            } else {
              // Erreur au nème essai
              this._stopTimer()
              this.cacheBoutonValider()

              AffCorrection(true)

              this.typederreurs[2]++
              this.etat = 'navigation'
              this.sectionCourante()
            }
          }
        }
      } else {
        // pas de réponse
        stor.lesdiv.correction.style.color = this.styles.cfaux
        stor.lesdiv.correction.innerHTML = reponseIncomplete
        me.afficheBoutonValider()
      }

      if (stor.yaexplik) {
        stor.lesdiv.explications.classList.add('explique')
      }
      if (stor.yaco) {
        stor.lesdiv.solution.classList.add('correction')
      }

      // Obligatoire
      this.finCorrection()
      break // case "correction":

    case 'navigation':
      if (!this.sectionTerminee()) {
        this.etat = 'enonce'
      }
      this.finNavigation()
      break // case "navigation":
  }
}
