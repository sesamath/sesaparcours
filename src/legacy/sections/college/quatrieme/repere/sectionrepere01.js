import { j3pAddElt, j3pArrondi, j3pGetNewId, j3pGetRandomBool, j3pGetRandomInt, j3pShowError, j3pShuffle } from 'src/legacy/core/functions'
import { getPe, initCptPe } from 'src/legacy/sections/college/helperTommy'
import { j3pAffiche } from 'src/lib/mathquill/functions'
import { addDefaultTable } from 'src/legacy/themes/table'
import ZoneStyleMathquill3 from 'src/legacy/outils/zoneStyleMathquill/ZoneStyleMathquill3'
import { j3pCreeSVG } from 'src/legacy/core/functionsSvg'
import { getMtgCore } from 'src/lib/outils/mathgraph'
import { addMaj } from 'src/lib/utils/string'
import textesGeneriques from 'src/lib/core/textes'

const { cBien, cFaux, essaieEncore, reponseIncomplete, tempsDepasse } = textesGeneriques

/**
 * Ajouter une description ici
 * @fileOverview
 * @author Tommy Barroy <t.barroy@laposte.net>
 * @since 2017-10
*/
export const params = {
  outils: ['mathquill', 'calculatrice'],
  /**
   * Ce qui est paramétrable dans l’éditeur de graphe
   * @type {LegacySectionParamsParametres}
   */
  parametres: [
    ['nbrepetitions', 9, 'entier', 'Nombre de répétitions de la section'],
    ['nbchances', 2, 'entier', 'Nombre d’essais possibles.'],
    ['Coordonnees', 'unite', 'liste', '<u>unite</u>: Chaque graduation vaut 1.<br><br> <u>entiere</u>: Une graduation correspond à un nombre entier. <br><br> <u>decimale</u>: Une graduation correspond à un nombre décimal.', ['unite', 'entiere', 'decimale', 'unite_decimale', 'unite_entiere', 'decimale_entiere', 'les trois']],
    ['position', 'sommet_arete', 'liste', '<u>sommet</u>: Le point est un sommet du pavé droit <br><br>. <u>arete</u>: Le point est sur une arête <br><br>. <u>face</u>: Le point est sur une face <br><br>. <u>intérieur</u>: Le point est à l’intérieur du pavé droit <br><br>.', ['sommet', 'arete', 'face', 'interieur', 'sommet_arete', 'sommet_face', 'sommet_interieur', 'arete_face', 'arete_interieur', 'face_interieur', 'sommet_arete_face', 'sommet_arete_interieur', 'sommet_face_interieur', 'arete_face_interieur', 'partout']],
    // le modèle gère la compatibilité ascendante avec l’ancien paramètre MiseEnPagePlusClaire
    ['theme', 'standard', 'liste', 'Modèle pour l’affichage', ['standard', 'zonesAvecImageDeFond']]

  ],
  // dans le cas d’une section QUANTITATIVE
  pe: [
    { pe_1: 'Inversion' },
    { pe_2: 'Ecriture' },
    { pe_3: 'Calcul' }
  ]
}

/**
 * section repere01
 * @this {Parcours}
 */
export default function main () {
  const me = this
  const ds = me.donneesSection
  const stor = me.storage
  let svgId = stor.svgId

  function creeLesDiv () {
    stor.lesdiv = {}
    stor.lesdiv.conteneur = j3pAddElt(me.zonesElts.MG, 'div', {
      style: me.styles.etendre('toutpetit.enonce', { padding: '10px' })
    })
    stor.lesdiv.consigneG = addDefaultTable(stor.lesdiv.conteneur, 1, 1)[0][0]
    stor.lesdiv.figure = addDefaultTable(stor.lesdiv.conteneur, 1, 1)[0][0]
    stor.lesdiv.travail = addDefaultTable(stor.lesdiv.conteneur, 1, 1)[0][0]
    stor.lesdiv.explications = j3pAddElt(stor.lesdiv.conteneur, 'div')
    stor.lesdiv.solution = j3pAddElt(stor.lesdiv.conteneur, 'div')
    stor.lesdiv.correction = j3pAddElt(me.zonesElts.MD, 'div', {
      style: me.styles.etendre('toutpetit.enonce', { padding: '10px' })
    })
    stor.lesdiv.solution.style.color = me.styles.toutpetit.correction.color
    stor.lesdiv.explications.style.color = me.styles.cfaux
    stor.lesdiv.explications.style.color = me.styles.cfaux
  }
  function faisChoixExos () {
    return ds.lesExos.pop()
  }

  function initSection () {
    let i
    initCptPe(ds, stor)

    me.validOnEnter = false // ex donneesSection.touche_entree

    // Construction de la page
    me.construitStructurePage('presentation1bis')

    me.donneesSection.lesExos = []

    let lp = []
    if ((ds.Coordonnees.indexOf('unite') !== -1) || (ds.Coordonnees === 'les trois')) lp.push('un')
    if ((ds.Coordonnees.indexOf('enti') !== -1) || (ds.Coordonnees === 'les trois')) lp.push('ent')
    if ((ds.Coordonnees.indexOf('deci') !== -1) || (ds.Coordonnees === 'les trois')) lp.push('dec')
    lp = j3pShuffle(lp)
    for (i = 0; i < me.donneesSection.nbitems; i++) {
      me.donneesSection.lesExos.push({ coo: lp[i % (lp.length)] })
    }
    me.donneesSection.lesExos = j3pShuffle(me.donneesSection.lesExos)

    lp = ['rep']
    for (i = 0; i < ds.lesExos.length; i++) {
      ds.lesExos[i].donnees = lp[i % lp.length]
    }
    me.donneesSection.lesExos = j3pShuffle(me.donneesSection.lesExos)

    lp = []
    if ((ds.position.indexOf('sommet') !== -1) || (ds.position === 'partout')) lp.push('som')
    if ((ds.position.indexOf('arete') !== -1) || (ds.position === 'partout')) lp.push('aret')
    if ((ds.position.indexOf('face') !== -1) || (ds.position === 'partout')) lp.push('face')
    if ((ds.position.indexOf('interieur') !== -1) || (ds.position === 'partout')) lp.push('int')
    lp = j3pShuffle(lp)
    for (i = 0; i < ds.lesExos.length; i++) {
      ds.lesExos[i].posi = lp[i % lp.length]
    }
    me.donneesSection.lesExos = j3pShuffle(me.donneesSection.lesExos)
    me.typederreurs = [0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0]
    me.afficheTitre('Déterminer les coordonnées d’un point')

    // chargement mtg
    getMtgCore({ withMathJax: true })
      .then(
        // success
        (mtgAppLecteur) => {
          stor.mtgAppLecteur = mtgAppLecteur
          enonceMain()
        },
        // failure
        (error) => {
          j3pShowError(error, { message: 'mathgraph n’est pas correctement chargé', mustNotify: true })
        })
      // plantage dans enonceMain
      .catch(j3pShowError)
  }

  function poseQuestion () {
    let bub = ''
    let bubu = 'point'
    if (stor.Lexo.posi === 'som') bubu = 'sommet'
    if (stor.Lexo.posi === 'int') bubu = 'point à l’intérieur'
    stor.nompave = stor.lettres[3] + stor.lettres[6] + stor.lettres[7] + stor.lettres[2] + stor.lettres[0] + stor.lettres[4] + stor.lettres[5] + stor.lettres[1]
    if (stor.Lexo.donnees === 'dim') {
      bub = '<br> tel que $' + stor.lettres[0] + stor.lettres[1] + ' = ' + stor.longx + '$  ; $' + stor.lettres[0] + stor.lettres[3] + ' = ' + stor.longy + '$  et  $' + stor.lettres[0] + stor.lettres[4] + ' = ' + stor.longz + '$ .'
    }
    j3pAffiche(stor.lesdiv.consigneG, null, stor.lettres[8] + ' est un ' + bubu + ' du pavé droit ' + stor.nompave + bub)
    const tt = addDefaultTable(stor.lesdiv.travail, 1, 7)
    j3pAffiche(tt[0][0], null, 'Dans le repère $(' + stor.lettres[0] + ',x,y,z)$, le point ' + stor.lettres[8] + ' a pour coordonnées (&nbsp;')
    j3pAffiche(tt[0][2], null, '&nbsp;;&nbsp;')
    j3pAffiche(tt[0][4], null, '&nbsp;;&nbsp;')
    j3pAffiche(tt[0][6], null, '&nbsp;)')
    stor.zonex = new ZoneStyleMathquill3(tt[0][1], { restric: '0123456789.,', enter: me.sectionCourante.bind(me) })
    stor.zoney = new ZoneStyleMathquill3(tt[0][3], { restric: '0123456789.,', enter: me.sectionCourante.bind(me) })
    stor.zonez = new ZoneStyleMathquill3(tt[0][5], { restric: '0123456789.,', enter: me.sectionCourante.bind(me) })
  }
  function faiFigure () {
    let i
    svgId = j3pGetNewId()
    stor.svgId = svgId
    stor.lettres = []
    for (i = 0; i < 12; i++) {
      addMaj(stor.lettres)
    }
    const lettres = stor.lettres

    // fait les div x
    stor.nbdivy = j3pGetRandomInt(3, 4)
    stor.nbdivz = j3pGetRandomInt(2, 4)
    stor.nbdivx = j3pGetRandomInt(3, 8)

    // place un point M
    let vi
    switch (stor.Lexo.posi) {
      case 'som':
        stor.lettres[8] = stor.lettres[j3pGetRandomInt(0, 7)]
        switch (stor.lettres[8]) {
          case lettres[0]:
            stor.cox = 0
            stor.coy = 0
            stor.coz = 0
            vi = 5
            break
          case lettres[1]:
            stor.cox = 0
            stor.coy = 0
            stor.coz = stor.nbdivz
            vi = 8
            break
          case lettres[2]:
            stor.cox = 0
            stor.coy = stor.nbdivy
            stor.coz = stor.nbdivz
            vi = 4
            break
          case lettres[3]:
            stor.cox = 0
            stor.coy = stor.nbdivy
            stor.coz = 0
            vi = 1
            break
          case lettres[4]:
            stor.cox = stor.nbdivx
            stor.coy = 0
            stor.coz = 0
            vi = 6
            break
          case lettres[5]:
            stor.cox = stor.nbdivx
            stor.coy = 0
            stor.coz = stor.nbdivz
            vi = 7
            break
          case lettres[6]:
            stor.cox = stor.nbdivx
            stor.coy = stor.nbdivy
            stor.coz = 0
            vi = 2
            break
          case lettres[7]:
            stor.cox = stor.nbdivx
            stor.coy = stor.nbdivy
            stor.coz = stor.nbdivz
            vi = 3
            break
        }
        break
      case 'aret':
        stor.cox = j3pGetRandomInt(1, stor.nbdivx - 1)
        stor.coy = j3pGetRandomInt(1, stor.nbdivy - 1)
        stor.coz = j3pGetRandomInt(1, stor.nbdivz - 1)
        switch (j3pGetRandomInt(0, 2)) {
          case 0:
            if (j3pGetRandomBool()) { stor.cox = 0 } else { stor.cox = stor.nbdivx }
            if (j3pGetRandomBool()) { stor.coy = 0 } else { stor.coy = stor.nbdivy }
            break
          case 1:
            if (j3pGetRandomBool()) { stor.coy = 0 } else { stor.coy = stor.nbdivy }
            if (j3pGetRandomBool()) { stor.coz = 0 } else { stor.coz = stor.nbdivz }
            break
          case 2:
            if (j3pGetRandomBool()) { stor.cox = 0 } else { stor.cox = stor.nbdivx }
            if (j3pGetRandomBool()) { stor.coz = 0 } else { stor.coz = stor.nbdivz }
            break
        }
        break
      case 'face':
        stor.cox = j3pGetRandomInt(1, stor.nbdivx - 1)
        stor.coy = j3pGetRandomInt(1, stor.nbdivy - 1)
        stor.coz = j3pGetRandomInt(1, stor.nbdivz - 1)
        switch (j3pGetRandomInt(0, 2)) {
          case 0:
            if (j3pGetRandomBool()) { stor.cox = 0 } else { stor.cox = stor.nbdivx }
            break
          case 1:
            if (j3pGetRandomBool()) { stor.coy = 0 } else { stor.coy = stor.nbdivy }
            break
          case 2:
            if (j3pGetRandomBool()) { stor.cox = 0 } else { stor.cox = stor.nbdivx }
            break
        }
        break
      case 'int':
        stor.cox = j3pGetRandomInt(1, stor.nbdivx - 1)
        stor.coy = j3pGetRandomInt(1, stor.nbdivy - 1)
        stor.coz = j3pGetRandomInt(1, stor.nbdivz - 1)
    }

    switch (stor.Lexo.coo) {
      case 'un':
        stor.unix = 1
        stor.uniy = 1
        stor.uniz = 1
        break
      case 'ent':
        stor.unix = j3pGetRandomInt(2, 10)
        stor.uniy = j3pGetRandomInt(2, 10)
        stor.uniz = j3pGetRandomInt(2, 10)
        break
      case 'dec':
        stor.unix = j3pArrondi(j3pGetRandomInt(2, 9) / 10, 1)
        stor.uniy = j3pArrondi(j3pGetRandomInt(2, 9) / 10, 1)
        stor.uniz = j3pArrondi(j3pGetRandomInt(2, 9) / 10, 1)
    }
    stor.longx = j3pArrondi(stor.nbdivx * stor.unix, 1)
    stor.longy = j3pArrondi(stor.nbdivy * stor.uniy, 1)
    stor.longz = j3pArrondi(stor.nbdivz * stor.uniz, 1)

    stor.lafig = 'TWF0aEdyYXBoSmF2YTEuMAAAABM+TMzNAAJmcv###wEA#wEAAAAAAAAAAANLAAAChwAAAQEAAAAAAAAAAQAAASr#####AAAAAQAKQ0NhbGNDb25zdAD#####AAJwaQAWMy4xNDE1OTI2NTM1ODk3OTMyMzg0Nv####8AAAABAApDQ29uc3RhbnRlQAkh+1RELRj#####AAAAAQAKQ1BvaW50QmFzZQD#####AQAAAAAOAAFVAMAkAAAAAAAAQBAAAAAAAAAAAAUAAEAvmBBiTdLyQC+YEGJN0vL#####AAAAAQAUQ0Ryb2l0ZURpcmVjdGlvbkZpeGUA#####wEAAAAAEAAAAQAAAAEAAAABAT#wAAAAAAAA#####wAAAAEAD0NQb2ludExpZURyb2l0ZQD#####AQAAAAAOAAFWAMAAAAAAAAAAQBAAAAAAAAAAAAUAAUA#mBBiTdLyAAAAAv####8AAAABAAhDU2VnbWVudAD#####AQAAAAAQAAABAAAAAQAAAAEAAAAD#####wAAAAEAB0NNaWxpZXUA#####wEAAAAAEAAAAAAAAAAAAAAAQAgAAAAAAAAAAAUAAAAAAQAAAAP#####AAAAAgAMQ0NvbW1lbnRhaXJlAP####8BAAAAAAAAAAAAAAAAQBgAAAAAAAAAAAAAAAUMAAAAAAABAAAAAAAAAAEAAAAAAAAAAAABMf####8AAAABAAlDTG9uZ3VldXIA#####wAAAAEAAAADAAAAAgD#####AQAAAAAQAAAAAAAAAAAAAABACAAAAAAAAAAABQABQF#AAAAAAABAZrwo9cKPXAAAAAIA#####wEAAAABEAABQQAAAAAAAAAAAEAIAAAAAAAAAAAFAAFARoAAAAAAAEB0DhR64UeuAAAAAwD#####AQAAAAEQAAABAAAAAQAAAAkAP#AAAAAAAAAAAAAEAP####8BAAAAABAAAAAAAAAAAAAAAEAIAAAAAAAAAAAFAAHAcQAAAAAAAAAAAAoAAAADAP####8BAAAAARAAAAEAAAABAAAACQE#8AAAAAAAAAAAAAQA#####wEAAAAAEAAAAAAAAAAAAAAAQAgAAAAAAAAAAAUAAUB9AAAAAAAAAAAADP####8AAAABAAhDVmVjdGV1cgD#####AAAAAAAQAAABAAAAAwAAAAkAAAALAAAAAAkA#####wAAAAAAEAAAAQAAAAMAAAAJAAAADQAAAAACAP####8BAAAAABAAAAAAAAAAAAAAAEAIAAAAAAAAAAAFAAFAgkAAAAAAAEBzThR64UeuAAAAAgD#####AQAAAAAQAAAAAAAAAAAAAABACAAAAAAAAAAABQABQIOgAAAAAABAc#4UeuFHrv####8AAAABAAlDQ2VyY2xlT0EA#####wAAAAAAAAABAAAAEAAAABH#####AAAAAQAVQ1BvaW50SW50ZXJpZXVyQ2VyY2xlAP####8A#wAAABAAAAAAAAAAAAAAAEAIAAAAAAAAAAAIAAEAAAASP5aT7sCZOYgAAAAAAAAAAAAAAAkA#####wEAAAAAEAAAAQAAAAEAAAAQAAAAEwD#####AAAAAQAMQ1RyYW5zbGF0aW9uAP####8AAAAQAAAAE#####8AAAABAAtDUG9pbnRJbWFnZQD#####AQAAAAAQAAAAAAAAAAAAAABACAAAAAAAAAAABQAAAAAIAAAAFQAAAAkA#####wAAAAAAEAAAAQAAAAMAAAAJAAAAFgAAAAAEAP####8BAAAAABAAAAAAAAAAAAAAAEAIAAAAAAAAAAAFAAE#42lpaWlpaQAAAA4AAAAEAP####8BAAAAARAAAUIAAAAAAAAAAABACAAAAAAAAAAABQABP+uWEae5YRoAAAAPAAAABAD#####AAAAAAAQAAAAAAAAAAAAAABACAAAAAAAAAAABQABP+cPhh6u0pQAAAAXAAAACQD#####Af8AAAAQAAABAAAAAQAAAAkAAAAZAAAAAAkA#####wH#AAAAEAAAAQAAAAEAAAAJAAAAGgAAAAAJAP####8B#wAAABAAAAEAAAABAAAACQAAABgA#####wAAAAEADUNUcmFuc1BhclZlY3QA#####wAAABsAAAANAP####8B#wAAABAAAAAAAAAAAAAAAEAIAAAAAAAAAAAFAAAAABoAAAAeAAAADQD#####Af8AAAAQAAAAAAAAAAAAAABACAAAAAAAAAAABQAAAAAYAAAAHgAAAA4A#####wAAAB0AAAANAP####8B#wAAABAAAAAAAAAAAAAAAEAIAAAAAAAAAAAFAAAAABoAAAAhAAAADQD#####Af8AAAAQAAAAAAAAAAAAAABACAAAAAAAAAAABQAAAAAfAAAAIQAAAAIA#####wAAAAAAEAAAAEAIAAAAAAAAAAAAAAAAAAAAAAUAAEBZAAAAAAAAQHzuFHrhR67#####AAAAAQAHQ0NhbGN1bAD#####AAVtaW5pMQABMgAAAAFAAAAAAAAAAAAAAA8A#####wAFbWF4aTEAAjEwAAAAAUAkAAAAAAAA#####wAAAAEAFENJbXBsZW1lbnRhdGlvblByb3RvAP####8AB0N1cnNldXIAAAAFAAAABQAAAAMAAAAlAAAAJgAAACQAAAADAAAAACcBAAAAABAAAAEAAAABAAAAJAE#8AAAAAAAAAAAAAQBAAAAJwAAAH8AEAAAAMAIAAAAAAAAP#AAAAAAAAAAAAUAAEBdQAAAAAAAAAAAKP####8AAAABAAtDSG9tb3RoZXRpZQAAAAAnAAAAJP####8AAAABAApDT3BlcmF0aW9uA#####8AAAABAA9DUmVzdWx0YXRWYWxldXIAAAAlAAAAEgEAAAATAAAAJQAAABMAAAAmAAAADQAAAAAnAQAAAAANAAJPMQDAEAAAAAAAAEAQAAAAAAAAAAAFAAAAACkAAAAqAAAAEQAAAAAnAAAAJAAAABIDAAAAEgEAAAABP#AAAAAAAAAAAAATAAAAJQAAABIBAAAAEwAAACYAAAATAAAAJQAAAA0AAAAAJwEAAAAADQACSTEAwAAAAAAAAABACAAAAAAAAAAABQAAAAApAAAALAAAAAUBAAAAJwAAAH8AEAAAAQAAAgMAAAAkAAAAKQAAAAQBAAAAJwAAAH8BEAACazEAwAAAAAAAAABAAAAAAAAAAAAAAQABP9ididididkAAAAu#####wAAAAIAD0NNZXN1cmVBYnNjaXNzZQEAAAAnAAJhMQAAACsAAAAtAAAAL#####8AAAABAA9DVmFsZXVyQWZmaWNoZWUBAAAAJwAAAH8AAAAAAAAAAADAGAAAAAAAAAAAAAAALw8AAf###wAAAAEAAAACAAAAAQAAAAAAAAAAAAAAAAAAAAAwAAAAAgD#####AAAAAAAQAAAAQAgAAAAAAAAAAAAAAAAAAAAABQAAQFeAAAAAAABAf94UeuFHrgAAAA8A#####wAFbWluaTIAATIAAAABQAAAAAAAAAAAAAAPAP####8ABW1heGkyAAIxMAAAAAFAJAAAAAAAAAAAABAA#####wAHQ3Vyc2V1cgAAAAUAAAAFAAAAAwAAADMAAAA0AAAAMgAAAAMAAAAANQEAAAAAEAAAAQAAAAEAAAAyAT#wAAAAAAAAAAAABAEAAAA1AAAAfwAQAAAAwAgAAAAAAAA#8AAAAAAAAAAABQAAQF1AAAAAAAAAAAA2AAAAEQAAAAA1AAAAMgAAABIDAAAAEwAAADMAAAASAQAAABMAAAAzAAAAEwAAADQAAAANAAAAADUBAAAAAA0AAk8xAMAQAAAAAAAAQBAAAAAAAAAAAAUAAAAANwAAADgAAAARAAAAADUAAAAyAAAAEgMAAAASAQAAAAE#8AAAAAAAAAAAABMAAAAzAAAAEgEAAAATAAAANAAAABMAAAAzAAAADQAAAAA1AQAAAAANAAJJMQDAAAAAAAAAAEAIAAAAAAAAAAAFAAAAADcAAAA6AAAABQEAAAA1AAAAfwAQAAABAAACAwAAADIAAAA3AAAABAEAAAA1AAAAfwEQAAFrAMAAAAAAAAAAQAAAAAAAAAAAAAEAAT#iUyUyUyUyAAAAPAAAABQBAAAANQACYTIAAAA5AAAAOwAAAD0AAAAVAQAAADUAAAB#AAAAAAAAAAAAwBgAAAAAAAAAAAAAAD0PAAH###8AAAABAAAAAgAAAAEAAAAAAAAAAAAAAAAAAAAAPgAAAAIA#####wAAAAAAEAAAAEAIAAAAAAAAAAAAAAAAAAAAAAUAAEBWwAAAAAAAQIFPCj1wo9cAAAAPAP####8ABW1pbmkzAAEyAAAAAUAAAAAAAAAAAAAADwD#####AAVtYXhpMwACMTAAAAABQCQAAAAAAAAAAAAQAP####8AB0N1cnNldXIAAAAFAAAABQAAAAMAAABBAAAAQgAAAEAAAAADAAAAAEMBAAAAABAAAAEAAAABAAAAQAE#8AAAAAAAAAAAAAQBAAAAQwAAAH8AEAAAAMAIAAAAAAAAP#AAAAAAAAAAAAUAAEBdQAAAAAAAAAAARAAAABEAAAAAQwAAAEAAAAASAwAAABMAAABBAAAAEgEAAAATAAAAQQAAABMAAABCAAAADQAAAABDAQAAAAANAAJPMQDAEAAAAAAAAEAQAAAAAAAAAAAFAAAAAEUAAABGAAAAEQAAAABDAAAAQAAAABIDAAAAEgEAAAABP#AAAAAAAAAAAAATAAAAQQAAABIBAAAAEwAAAEIAAAATAAAAQQAAAA0AAAAAQwEAAAAADQACSTEAwAAAAAAAAABACAAAAAAAAAAABQAAAABFAAAASAAAAAUBAAAAQwAAAH8AEAAAAQAAAgMAAABAAAAARQAAAAQBAAAAQwAAAH8BEAACazIAwAAAAAAAAABAAAAAAAAAAAAAAQABP9kpkpkpkpkAAABKAAAAFAEAAABDAAJhMwAAAEcAAABJAAAASwAAABUBAAAAQwAAAH8AAAAAAAAAAADAGAAAAAAAAAAAAAAASw8AAf###wAAAAEAAAACAAAAAQAAAAAAAAAAAAAAAAAAAABMAAAADwD#####AAJBMQACYTEAAAATAAAAMAAAAA8A#####wACQTIAAmEyAAAAEwAAAD4AAAAPAP####8AAkEzAAJhMwAAABMAAABMAAAAAgD#####AAAAAAAQAAAAQAgAAAAAAAAAAAAAAAAAAAAABQAAQHPQAAAAAABAfK4UeuFHrgAAAA8A#####wAFbWluaTQAATAAAAABAAAAAAAAAAAAAAAPAP####8ABW1heGk0AAIxMAAAAAFAJAAAAAAAAAAAABAA#####wAHQ3Vyc2V1cgAAAAUAAAAFAAAAAwAAAFIAAABTAAAAUQAAAAMAAAAAVAEAAAAAEAAAAQAAAAEAAABRAT#wAAAAAAAAAAAABAEAAABUAAAA#wAQAAAAwAgAAAAAAAA#8AAAAAAAAAAABQAAQF1AAAAAAAAAAABVAAAAEQAAAABUAAAAUQAAABIDAAAAEwAAAFIAAAASAQAAABMAAABSAAAAEwAAAFMAAAANAAAAAFQBAAAAAA0AAk8xAMAQAAAAAAAAQBAAAAAAAAAAAAUAAAAAVgAAAFcAAAARAAAAAFQAAABRAAAAEgMAAAASAQAAAAE#8AAAAAAAAAAAABMAAABSAAAAEgEAAAATAAAAUwAAABMAAABSAAAADQAAAABUAQAAAAANAAJJMQDAAAAAAAAAAEAIAAAAAAAAAAAFAAAAAFYAAABZAAAABQEAAABUAAAA#wAQAAABAAAAAgAAAFEAAABWAAAABAEAAABUAAAA#wEQAAJrMwDAAAAAAAAAAEAAAAAAAAAAAAABAAE#0yUyUyUyUwAAAFsAAAAUAQAAAFQAAnoxAAAAWAAAAFoAAABcAAAAFQEAAABUAAAA#wAAAAAAAAAAAMAYAAAAAAAAAAAAAABcDwAB####AAAAAQAAAAIAAAABAAAAAAAAAAAAAAAAAgAAAF0AAAACAP####8AAAAAABAAAABACAAAAAAAAAAAAAAAAAAAAAAFAABAc0AAAAAAAEB#bhR64UeuAAAADwD#####AAVtaW5pNQABMAAAAAEAAAAAAAAAAAAAAA8A#####wAFbWF4aTUAAjEwAAAAAUAkAAAAAAAAAAAAEAD#####AAdDdXJzZXVyAAAABQAAAAUAAAADAAAAYAAAAGEAAABfAAAAAwAAAABiAQAAAAAQAAABAAAAAQAAAF8BP#AAAAAAAAAAAAAEAQAAAGIAAAD#ABAAAADACAAAAAAAAD#wAAAAAAAAAAAFAABAXUAAAAAAAAAAAGMAAAARAAAAAGIAAABfAAAAEgMAAAATAAAAYAAAABIBAAAAEwAAAGAAAAATAAAAYQAAAA0AAAAAYgEAAAAADQACTzEAwBAAAAAAAABAEAAAAAAAAAAABQAAAABkAAAAZQAAABEAAAAAYgAAAF8AAAASAwAAABIBAAAAAT#wAAAAAAAAAAAAEwAAAGAAAAASAQAAABMAAABhAAAAEwAAAGAAAAANAAAAAGIBAAAAAA0AAkkxAMAAAAAAAAAAQAgAAAAAAAAAAAUAAAAAZAAAAGcAAAAFAQAAAGIAAAD#ABAAAAEAAAACAAAAXwAAAGQAAAAEAQAAAGIAAAD#ARAAAms0AMAAAAAAAAAAQAAAAAAAAAAAAAEAAT#ZtZtZtZtaAAAAaQAAABQBAAAAYgACejIAAABmAAAAaAAAAGoAAAAVAQAAAGIAAAD#AAAAAAAAAAAAwBgAAAAAAAAAAAAAAGoPAAH###8AAAABAAAAAgAAAAEAAAAAAAAAAAAAAAACAAAAawAAAAIA#####wAAAAAAEAAAAEAIAAAAAAAAAAAAAAAAAAAAAAUAAEBzQAAAAAAAQIFvCj1wo9cAAAAPAP####8ABW1pbmk2AAEwAAAAAQAAAAAAAAAAAAAADwD#####AAVtYXhpNgACMTAAAAABQCQAAAAAAAAAAAAQAP####8AB0N1cnNldXIAAAAFAAAABQAAAAMAAABuAAAAbwAAAG0AAAADAAAAAHABAAAAABAAAAEAAAABAAAAbQE#8AAAAAAAAAAAAAQBAAAAcAAAAP8AEAAAAMAIAAAAAAAAP#AAAAAAAAAAAAUAAEBdQAAAAAAAAAAAcQAAABEAAAAAcAAAAG0AAAASAwAAABMAAABuAAAAEgEAAAATAAAAbgAAABMAAABvAAAADQAAAABwAQAAAAANAAJPMQDAEAAAAAAAAEAQAAAAAAAAAAAFAAAAAHIAAABzAAAAEQAAAABwAAAAbQAAABIDAAAAEgEAAAABP#AAAAAAAAAAAAATAAAAbgAAABIBAAAAEwAAAG8AAAATAAAAbgAAAA0AAAAAcAEAAAAADQACSTEAwAAAAAAAAABACAAAAAAAAAAABQAAAAByAAAAdQAAAAUBAAAAcAAAAP8AEAAAAQAAAAIAAABtAAAAcgAAAAQBAAAAcAAAAP8BEAACazUAwAAAAAAAAABAAAAAAAAAAAAAAQABP9rNrNrNrNsAAAB3AAAAFAEAAABwAAJ6MwAAAHQAAAB2AAAAeAAAABUBAAAAcAAAAP8AAAAAAAAAAADAGAAAAAAAAAAAAAAAeA8AAf###wAAAAEAAAACAAAAAQAAAAAAAAAAAAAAAAIAAAB5AAAADwD#####AAJaMQACejEAAAATAAAAXQAAAA8A#####wACWjIAAnoyAAAAEwAAAGsAAAAPAP####8AAlozAAJ6MwAAABMAAAB5AAAAAgD#####AP8AAAAQAAAAAAAAAAAAAABACAAAAAAAAAAACAABQIGwAAAAAABAgXcKPXCj1wAAABAA#####wASTWVzdXJlIGRlIGxvbmd1ZXVyAAAABQAAAAIAAAACAAAACQAAABn#####AAAAAQALQ01lZGlhdHJpY2UAAAAAfwEAAAAAEAAAAQAAAAEAAAAJAAAAGQAAAAYAAAAAfwEAAAAAEAAAAQAABQAAAAAJAAAAGf####8AAAACAAlDQ2VyY2xlT1IAAAAAfwEAAAAAAAABAAAAgQAAAAFAMAAAAAAAAAH#####AAAAAQAQQ0ludERyb2l0ZUNlcmNsZQAAAAB#AAAAgAAAAIL#####AAAAAQAQQ1BvaW50TGllQmlwb2ludAAAAAB#AQAAAAAQAAABAAAFAAEAAACDAAAACAEAAAB#AAAACQAAABkAAAAVAQAAAH8B#wAAAQAAAAAAhBEAAAAAAAEAAAABAAAAAQAAAAAAAAAAAAAAAAEAAACFAAAAEQD#####AAAACQAAABICAAAAEgMAAAABP#AAAAAAAAD#####AAAAAgAJQ0ZvbmN0aW9uAgAAABMAAABPAAAAEwAAAHsAAAANAP####8B#wAAABAAAAAAAAAAAAAAAEAIAAAAAAAAAAAIAAAAABkAAACHAAAAEQD#####AAAACQAAABICAAAAEgMAAAABP#AAAAAAAAAAAAAaAgAAABMAAABOAAAAEwAAAHwAAAANAP####8B#wAAABAAAAAAAAAAAAAAAEAIAAAAAAAAAAAIAAAAABoAAACJAAAAEQD#####AAAACQAAABICAAAAEgMAAAABP#AAAAAAAAAAAAAaAgAAABMAAABQAAAAEwAAAH0AAAANAP####8B#wAAABAAAAAAAAAAAAAAAEAIAAAAAAAAAAAIAAAAABgAAACL#####wAAAAEAEENEcm9pdGVQYXJhbGxlbGUA#####wH#AAAAEAAAAQAAAgEAAACIAAAAFwAAAAkA#####wF#AAAAEAAAAQAAAgUAAAAJAAAAiAAAAAAJAP####8BfwAAABAAAAEAAAIFAAAACQAAAIwAAAAACQD#####AX8AAAAQAAABAAACBQAAAAkAAACKAAAAAA4A#####wAAAI4AAAANAP####8BfwAAABAAAAAAAAAAAAAAAEAIAAAAAAAAAAAIAAAAAAkAAACRAAAADgD#####AAAAjwAAAA0A#####wF#AAAAEAAAAAAAAAAAAAAAQAgAAAAAAAAAAAgAAAAAkgAAAJMAAAAOAP####8AAACQAAAABwD#####AQAAAAEAAP####8QQCIAAAAAAARASvCj1wo9cQAAAAAAAAAAAAAAAAABAAAAAAAAAAAAAyRYJAAAAAcA#####wAAAAABAAD#####EECACAAAAAAAQHRuFHrhR64AAAAAAAAAAAAAAAAAAQAAAAAAAAAAAAMkeCQAAAAHAP####8BAAAAAQAA#####xBAYMAAAAAAAEB3fhR64UeuAAAAAAAAAAAAAAAAAAEAAAAAAAAAAAADJHkkAAAABAD#####AAAAAAAQAAAAAAAAAAAAAABACAAAAAAAAAAABwABP+1C3S#NGIwAAAAXAAAAFwD#####AQAAAAAAAAMAAACZAAAAAT#gAAAAAAAAAAAAAAMA#####wEAAAABEAAAAQAAAAMAAACZAT#wAAAAAAAAAAAAGAD#####AAAAmwAAAJoAAAAZAP####8BAAAAABAAAAAAAAAAAAAAAEAIAAAAAAAAAAAHAAIAAACcAAAAGQD#####AQAAAAAQAAAAAAAAAAAAAABACAAAAAAAAAAABwABAAAAnAAAAAcA#####wAAAAABAAAAAACeEAAAAAAAAAAAAAAAAAABAAAAAAAAAAAAAyR5JAAAAAcA#####wAAAAABAAD#####EEA+AAAAAAACQFE4UeuFHrgAAAAAAAAAAAAAAAAAAQAAAAAAAAAAAAMkeiQAAAARAP####8AAAAJAAAAEgMAAAABP#AAAAAAAAAAAAAaAgAAABMAAABPAAAADQD#####Af8AAAAQAAAAAAAAAAAAAABACAAAAAAAAAAACAAAAAAZAAAAoQAAABcA#####wH#AAAAAAADAAAAogAAAAE#4zMzMzMzMwAAAAADAP####8B#wAAARAAAAEAAAADAAAAogA#8AAAAAAAAAAAABgA#####wAAAKQAAACjAAAAGQD#####Af8AAAAQAAAAAAAAAAAAAABACAAAAAAAAAAACAACAAAApQAAABkA#####wH#AAAAEAAAAAAAAAAAAAAAQAgAAAAAAAAAAAgAAQAAAKUAAAAHAP####8AAAAAAQADYWZYAAAApxAAAAAAAAEAAAABAAAAAQAAAAAAAAAAAAF2AAAAEQD#####AAAACQAAABIDAAAAAT#wAAAAAAAAAAAAGgIAAAATAAAATgAAAA0A#####wH#AAAAEAAAAAAAAAAAAAAAQAgAAAAAAAAAAAgAAAAAGgAAAKkAAAAXAP####8B#wAAAAAAAwAAAKoAAAABP+ZmZmZmZmYAAAAAAwD#####Af8AAAEQAAABAAAAAwAAAKoBP#AAAAAAAAAAAAAYAP####8AAACsAAAAqwAAABkA#####wH#AAAAEAAAAAAAAAAAAAAAQAgAAAAAAAAAAAgAAgAAAK0AAAAZAP####8B#wAAABAAAAAAAAAAAAAAAEAIAAAAAAAAAAAIAAEAAACtAAAABwD#####AAAAAAEAA2FmWQAAAK8QAAAAAAABAAAAAQAAAAEAAAAAAAAAAAABdgAAABEA#####wAAAAkAAAASAwAAAAE#8AAAAAAAAAAAABoCAAAAEwAAAFAAAAANAP####8B#wAAABAAAAAAAAAAAAAAAEAIAAAAAAAAAAAIAAAAABgAAACxAAAAFwD#####Af8AAAAAAAMAAACyAAAAAT#mZmZmZmZmAAAAAAMA#####wH#AAABEAAAAQAAAAMAAACyAT#wAAAAAAAAAAAAGAD#####AAAAtAAAALMAAAAZAP####8B#wAAABAAAAAAAAAAAAAAAEAIAAAAAAAAAAAIAAIAAAC1AAAAGQD#####Af8AAAAQAAAAAAAAAAAAAABACAAAAAAAAAAACAABAAAAtQAAAAcA#####wAAAAABAANhZloAAAC2EAAAAAAAAQAAAAEAAAABAAAAAAAAAAAAAXYAAAAXAP####8B#wAAAAAAAgAAACAAAAABP+MzMzMzMzMA#####wAAAAEAD0NQb2ludExpZUNlcmNsZQD#####AP###wAQAAAAAAAAAAAAAABACAAAAAAAAAAABwABP80HvqGUuZIAAAC5AAAABwD#####AAAA#wEAA0FmMgAAALoQAAAAAAABAAAAAQAAAAEAAAAAAAAAAAABQQAAABcA#####wEAAP8AAAACAAAAIwAAAAE#4zMzMzMzMwAAAAAXAP####8BAAD#AAAAAgAAACIAAAABP+MzMzMzMzMAAAAAHAD#####AP###wAQAAAAAAAAAAAAAABACAAAAAAAAAAABwABQBcBTz2+N84AAAC8AAAAHAD#####AP###wAQAAAAAAAAAAAAAABACAAAAAAAAAAABwABQAgr+ar4wEYAAAC9AAAABwD#####AAAA#wEAA0FmNAAAAL8QAAAAAAABAAAAAQAAAAEAAAAAAAAAAAABQQAAAAcA#####wAAAP8BAANBZjMAAAC+EAAAAAAAAQAAAAEAAAABAAAAAAAAAAAAAUEAAAAXAP####8BAAD#AAAAAgAAAB8AAAABP+MzMzMzMzMAAAAAFwD#####AQAA#wAAAAIAAAAaAAAAAT#jMzMzMzMzAAAAABcA#####wEAAP8AAAACAAAAGQAAAAE#4zMzMzMzMwAAAAAXAP####8BAAD#AAAAAgAAAAkAAAABP+MzMzMzMzMAAAAAHAD#####AQAA#wAQAAAAAAAAAAAAAABACAAAAAAAAAAABwAAQBASEwa09IEAAADFAAAABwD#####AAAA#wEAA0FmNQAAAMYQAAAAAAABAAAAAQAAAAEAAAAAAAAAAAABQQAAABwA#####wEAAP8AEAAAAAAAAAAAAAAAQAgAAAAAAAAAAAcAAEAS2Xx#MyHSAAAAxAAAAAcA#####wAAAP8BAANBZjYAAADIEAAAAAAAAQAAAAEAAAABAAAAAAAAAAAAAUEAAAAcAP####8A####ABAAAAAAAAAAAAAAAEAIAAAAAAAAAAAHAAFAGHc9Ilr1JgAAAMIAAAAcAP####8A####ABAAAAAAAAAAAAAAAEAIAAAAAAAAAAAHAAFAFhjhFC+vYgAAAMMAAAAHAP####8AAAD#AQADQWY3AAAAyhAAAAAAAAEAAAABAAAAAQAAAAAAAAAAAAFBAAAABwD#####AAAA#wEAA0FmOAAAAMsQAAAAAAABAAAAAQAAAAEAAAAAAAAAAAABQQAAAAUA#####wAAAP8AEAAAAQAAAAMAAAAiAAAAGAAAABsA#####wH#AAAAEAAAAQAAAgEAAACMAAAAzgAAAAUA#####wAAAP8AEAAAAQAAAAMAAAAjAAAAIAAAAAUA#####wAAAP8AEAAAAQAAAAMAAAAZAAAAHwAAAAUA#####wAAAP8AEAAAAQAAAAMAAAAfAAAAIwAAAAUA#####wAAAP8AEAAAAQAAAAMAAAAjAAAAIgAAAAUA#####wAAAP8AEAAAAQAAAgMAAAAfAAAAGgAAAAUA#####wAAAP8AEAAAAQAAAgMAAAAaAAAAIgAAABsA#####wH#AAAAEAAAAQAAAgEAAACKAAAA1f####8AAAABABBDSW50RHJvaXRlRHJvaXRlAP####8B#wAAABAAAAAAAAAAAAAAAEAIAAAAAAAAAAAIAAAAANYAAADPAAAABQD#####AP8AAAAQAAABAAJMNQICAAAAjAAAANcAAAAFAP####8A#wAAABAAAAEAAkw2AgIAAADXAAAAigAAAAUA#####wAAAP8AEAAAAQAAAgMAAAAJAAAAGgAAAAQA#####wEAAH8AEAAAAAAAAAAAAAAAQAgAAAAAAAAAAAUAAT#mm2732GtDAAAA2gAAABcA#####wH#AAAAAAABAAAA2wAAAAE#0zMzMzMzMwAAAAADAP####8B#wAAARAAAAEAAAABAAAA2wE#8AAAAAAAAAAAAAMA#####wH#AAABEAAAAQAAAAEAAADbAD#wAAAAAAAAAAAAGAD#####AAAA3QAAANwAAAAZAP####8B#wAAABAAAAAAAAAAAAAAAEAIAAAAAAAAAAAFAAIAAADfAAAAGQD#####Af8AAAAQAAAAAAAAAAAAAABACAAAAAAAAAAABQABAAAA3wAAABgA#####wAAAN4AAADcAAAAGQD#####Af8AAAAQAAAAAAAAAAAAAABACAAAAAAAAAAABQACAAAA4gAAABkA#####wH#AAAAEAAAAAAAAAAAAAAAQAgAAAAAAAAAAAUAAQAAAOIAAAAFAP####8BAAAAABAAAAEAAAACAAAA4wAAAOQAAAAFAP####8BAAAAABAAAAEAAAACAAAA4QAAAOD#####AAAAAgASQ0xpZXVPYmpldFBhclB0TGllAP####8BAAAAAAAAAADlAAAAEgAAAAATAAAATgAAAAE#8AAAAAAAAAAAANsAAAAHAAAA2wAAANwAAADeAAAA4gAAAOMAAADkAAAA5QAAAB4A#####wAAAAAAAAAAAOYAAAASAAAAABMAAABOAAAAAT#wAAAAAAAAAAAA2wAAAAcAAADbAAAA3AAAAN0AAADfAAAA4AAAAOEAAADmAAAABQD#####AAAA#wAQAAABAAAAAwAAABgAAAAgAAAABQD#####AAAA#wAQAAABAAAAAwAAABgAAAAJAAAABAD#####AQAA#wAQAAAAAAAAAAAAAABACAAAAAAAAAAABQABP9Z#OWfzlnwAAADqAAAAFwD#####AQAA#wAAAAIAAADrAAAAAT#TMzMzMzMzAAAAAAMA#####wEAAP8BEAAAAQAAAAIAAADrAT#wAAAAAAAAAAAAGAD#####AAAA7QAAAOwAAAAZAP####8BAAD#ABAAAAAAAAAAAAAAAEAIAAAAAAAAAAAFAAEAAADuAAAAGQD#####AQAA#wAQAAAAAAAAAAAAAABACAAAAAAAAAAABQACAAAA7gAAAAUA#####wEAAP8AEAAAAQAAAAIAAADwAAAA7wAAAB4A#####wAAAAAAAAAAAPEAAAASAAAAABoCAAAAEwAAAFAAAAABP#AAAAAAAAAAAADrAAAABwAAAOsAAADsAAAA7QAAAO4AAADvAAAA8AAAAPEAAAAbAP####8B#wAAABAAAAEAAAIBAAAAiAAAAOoAAAAFAP####8AAAD#ABAAAAEAAAADAAAAIAAAABkAAAAFAP####8AAAD#ABAAAAEAAAADAAAAGQAAAAkAAAAEAP####8BAAD#ABAAAAAAAAAAAAAAAEAIAAAAAAAAAAAFAAE#1HrhR64UewAAAPUAAAAXAP####8BAAD#AAAAAgAAAPYAAAABP9MzMzMzMzMAAAAAAwD#####AQAA#wEQAAABAAAAAgAAAPYAP#AAAAAAAAAAAAAYAP####8AAAD4AAAA9wAAABkA#####wEAAP8AEAAAAAAAAAAAAAAAQAgAAAAAAAAAAAUAAQAAAPkAAAAZAP####8BAAD#ABAAAAAAAAAAAAAAAEAIAAAAAAAAAAAFAAIAAAD5AAAABQD#####AQAA#wAQAAABAAAAAgAAAPsAAAD6AAAAHgD#####AAAAAAAAAAAA#AAAABIAAAAAGgIAAAATAAAATwAAAAE#8AAAAAAAAAAAAPYAAAAHAAAA9gAAAPcAAAD4AAAA+QAAAPoAAAD7AAAA#AAAABsA#####wH#AAAAEAAAAQAAAgEAAACKAAAA9QAAAB0A#####wH#AAAAEAAAAAAAAAAAAAAAQAgAAAAAAAAAAAgAAAAA#gAAAI0AAAAFAP####8A#wAAABAAAAEAAkwxAgIAAACKAAAA#wAAAAUA#####wD#AAAAEAAAAQACTDICAgAAAIgAAAD#AAAAGwD#####Af8AAAAQAAABAAACAQAAAIwAAAD1AAAAHQD#####Af8AAAAQAAAAAAAAAAAAAABACAAAAAAAAAAACAAAAADzAAABAgAAAAUA#####wD#AAAAEAAAAQACTDMAAgAAAIwAAAEDAAAABQD#####AP8AAAAQAAABAAJMNAACAAABAwAAAIj#####AAAAAQAOQ09iamV0RHVwbGlxdWUA#####wAAAP8AAAAAAOkAAAAfAP####8AAAD#AAAAAAD0AAAAHwD#####AAAA#wAAAAAA0wAAAB8A#####wAAAP8AAAAAANAAAAAfAP####8AAAD#AAAAAADSAAAAHwD#####AAAA#wAAAAAA0QAAAB8A#####wAAAP8AAAAAAM4AAAANAP####8A#wAAABAAAAAAAAAAAAAAAEAIAAAAAAAAAAAIAAAAAJQAAACVAAAAFwD#####AX8AAAAAAgUAAAENAAAAAT#TMzMzMzMzAAAAAAMA#####wF#AAABEAAAAQAAAgUAAAENAD#wAAAAAAAAAAAAGAD#####AAABDwAAAQ4AAAAZAP####8BfwAAABAAAAAAAAAAAAAAAEAIAAAAAAAAAAAIAAIAAAEQAAAAGQD#####AX8AAAAQAAAAAAAAAAAAAABACAAAAAAAAAAACAABAAABEAAAAAMA#####wF#AAABEAAAAQAAAgUAAAENAT#wAAAAAAAAAAAAGAD#####AAABEwAAAQ4AAAAZAP####8BfwAAABAAAAAAAAAAAAAAAEAIAAAAAAAAAAAIAAEAAAEUAAAAGQD#####AX8AAAAQAAAAAAAAAAAAAABACAAAAAAAAAAACAACAAABFAAAAAUA#####wD#AAAAEAAAAQAAAAIAAAERAAABEgAAAAUA#####wD#AAAAEAAAAQAAAAIAAAEVAAABFgAAAAUA#####wD#AAAAEAAAAQACTDcCAgAAAJQAAAENAAAAFwD#####AQAAAAAAAAMAAAENAAAAAT#pmZmZmZmaAAAAABwA#####wD###8AEAAAAAAAAAAAAAAAQAgAAAAAAAAAAAcAAT#iBDX9+UsPAAABGgAAAAcA#####wD#AAABAANhZkwAAAEbEAAAAAAAAQAAAAEAAAABAAAAAAAAAAAAAWEAAAAFAP####8A#wAAABAAAAEAAkw4AgIAAAENAAAA1wAAABsA#####wF#AAAAEAAAAQAAAgUAAAENAAAA2gAAABgA#####wAAAR4AAAEOAAAAGQD#####AX8AAAAQAAAAAAAAAAAAAABACAAAAAAAAAAACAACAAABHwAAABkA#####wF#AAAAEAAAAAAAAAAAAAAAQAgAAAAAAAAAAAgAAQAAAR8AAAAFAP####8A#wAAABAAAAEAAAACAAABIAAAASEAAAAFAP####8A#wAAABAAAAEAAkw5AgIAAAENAAAA#wAAABcA#####wEAAP8AAAACAAAAGAAAAAE#4zMzMzMzMwAAAAADAP####8BAAD#ARAAAAEAAAACAAAAGAE#8AAAAAAAAAAAABgA#####wAAASUAAAEkAAAAGQD#####AQAA#wAQAAAAAAAAAAAAAABACAAAAAAAAAAABwACAAABJgAAABkA#####wEAAP8AEAAAAAAAAAAAAAAAQAgAAAAAAAAAAAcAAQAAASYAAAAHAP####8AAAD#AQADQWYxAAABJxAAAAAAAAEAAAABAAAAAQAAAAAAAAAAAAFBAAAAB###########'

    const yy = j3pCreeSVG(stor.lesdiv.figure, { id: svgId, width: 700, height: 380 })
    yy.style.border = 'solid black 1px'
    stor.mtgAppLecteur.removeAllDoc()
    stor.mtgAppLecteur.addDoc(svgId, stor.lafig, true)
    stor.mtgAppLecteur.calculateAndDisplayAll(true)
    stor.mtgAppLecteur.giveFormula2(svgId, 'A1', stor.nbdivz)
    stor.mtgAppLecteur.giveFormula2(svgId, 'A2', stor.nbdivx)
    stor.mtgAppLecteur.giveFormula2(svgId, 'A3', stor.nbdivy)
    stor.mtgAppLecteur.giveFormula2(svgId, 'Z1', stor.cox)
    stor.mtgAppLecteur.giveFormula2(svgId, 'Z2', stor.coz)
    stor.mtgAppLecteur.giveFormula2(svgId, 'Z3', stor.coy)
    stor.mtgAppLecteur.setText(svgId, '#Af1', stor.lettres[3], true)
    stor.mtgAppLecteur.setText(svgId, '#Af2', stor.lettres[6], true)
    stor.mtgAppLecteur.setText(svgId, '#Af3', stor.lettres[7], true)
    stor.mtgAppLecteur.setText(svgId, '#Af4', stor.lettres[2], true)
    stor.mtgAppLecteur.setText(svgId, '#Af5', stor.lettres[0], true)
    stor.mtgAppLecteur.setText(svgId, '#Af6', stor.lettres[4], true)
    stor.mtgAppLecteur.setText(svgId, '#Af7', stor.lettres[5], true)
    stor.mtgAppLecteur.setText(svgId, '#Af8', stor.lettres[1], true)
    if (stor.Lexo.posi !== 'som') {
      stor.mtgAppLecteur.setText(svgId, '#afL', stor.lettres[8], true)
    } else {
      stor.mtgAppLecteur.setVisible(svgId, '#afL', false, true)
      stor.mtgAppLecteur.setColor(svgId, '#Af' + vi, 255, 0, 0, true)
    }
    if ((stor.cox === 0 || stor.cox === stor.nbdivx) && (stor.coy === 0 || stor.coy === stor.nbdivy)) {
      stor.mtgAppLecteur.setVisible(svgId, '#L5', false, true)
      stor.mtgAppLecteur.setVisible(svgId, '#L2', false, true)
      stor.mtgAppLecteur.setVisible(svgId, '#L7', false, true)
    }
    if ((stor.coz === 0 || stor.coz === stor.nbdivz) && (stor.cox === 0 || stor.cox === stor.nbdivx)) {
      stor.mtgAppLecteur.setVisible(svgId, '#L4', false, true)
      stor.mtgAppLecteur.setVisible(svgId, '#L6', false, true)
      stor.mtgAppLecteur.setVisible(svgId, '#L9', false, true)
    }
    if ((stor.coz === 0 || stor.coz === stor.nbdivz) && (stor.coy === 0 || stor.coy === stor.nbdivy)) {
      stor.mtgAppLecteur.setVisible(svgId, '#L1', false, true)
      stor.mtgAppLecteur.setVisible(svgId, '#L3', false, true)
      stor.mtgAppLecteur.setVisible(svgId, '#L8', false, true)
    }
    stor.mtgAppLecteur.setText(svgId, '#afX', '$' + stor.unix + '$', true)
    stor.mtgAppLecteur.setText(svgId, '#afY', '$' + stor.uniz + '$', true)
    stor.mtgAppLecteur.setText(svgId, '#afZ', '$' + stor.uniy + '$', true)
    stor.mtgAppLecteur.updateFigure(svgId)
  }

  function yaReponse () {
    if (stor.zonex.reponse() === '') {
      stor.zonex.focus()
      return false
    }
    if (stor.zoney.reponse() === '') {
      stor.zoney.focus()
      return false
    }
    if (stor.zonez.reponse() === '') {
      stor.zonez.focus()
      return false
    }
    return true
  }
  function isRepOk () {
    stor.errInvers = false
    stor.errMalEcrit = false
    stor.errYap = false
    stor.yapcmpt = 0

    let ok = true
    let t1, t2, t3

    t1 = stor.zonex.reponse()
    t2 = stor.zoney.reponse()
    t3 = stor.zonez.reponse()
    if (malEcrit(t1)) {
      stor.zonex.corrige(false)
      stor.errMalEcrit = true
      ok = false
    }
    if (malEcrit(t2)) {
      stor.zoney.corrige(false)
      stor.errMalEcrit = true
      ok = false
    }
    if (malEcrit(t3)) {
      stor.zonez.corrige(false)
      stor.errMalEcrit = true
      ok = false
    }
    if (!ok) return false
    t1 = parseFloat(t1.replace(',', '.'))
    t2 = parseFloat(t2.replace(',', '.'))
    t3 = parseFloat(t3.replace(',', '.'))
    const ok1 = t1 === j3pArrondi(stor.unix * stor.cox, 1)
    const ok2 = t2 === j3pArrondi(stor.uniz * stor.coz, 1)
    const ok3 = t3 === j3pArrondi(stor.uniy * stor.coy, 1)
    if (ok1 && ok2 && ok3) return true
    if (!ok1) stor.zonex.corrige(false)
    if (!ok2) stor.zoney.corrige(false)
    if (!ok3) stor.zonez.corrige(false)
    ok = true
    if (yapas(t1)) {
      stor.errYap = true
      stor.zonex.corrige(false)
      ok = false
      stor.yapcmpt++
    }
    if (yapas(t2)) {
      stor.errYap = true
      stor.zoney.corrige(false)
      ok = false
      stor.yapcmpt++
    }
    if (yapas(t3)) {
      stor.errYap = true
      stor.zonez.corrige(false)
      ok = false
      stor.yapcmpt++
    }
    if (!ok) return false

    if ((ok1 && ok2) || (ok2 && ok3) || (ok1 && ok3)) {
      return false
    }
    stor.errInvers = true
    return false
  } // isRepOk

  function yapas (f) {
    return (f !== j3pArrondi(stor.unix * stor.cox, 1)) && (f !== j3pArrondi(stor.uniy * stor.coy, 1)) && (f !== j3pArrondi(stor.uniz * stor.coz, 1))
  }
  function malEcrit (t1) {
    return ((t1.indexOf('.') === 0) || (t1.indexOf('.') === t1.length - 1) || (t1.indexOf('.') !== t1.lastIndexOf('.')) || (t1 === ''))
  }
  function desactiveAll () {
    stor.zonex.disable()
    stor.zoney.disable()
    stor.zonez.disable()
  } // desactiveAll
  function passeToutVert () {
    stor.zonex.corrige(true)
    stor.zoney.corrige(true)
    stor.zonez.corrige(true)
  } // passeToutVert
  function barrelesfo () {
    stor.zonex.barreIfKo()
    stor.zoney.barreIfKo()
    stor.zonez.barreIfKo()
  }
  function affCorrFaux (isFin) {
    /// //affiche indic
    let yaexplik = false
    if (stor.errInvers) {
      j3pAffiche(stor.lesdiv.explications, null, 'Il faut mettre dans l’ordre abscisse, puis ordonnée, puis hauteur !\n')
      yaexplik = true
      stor.compteurPe1++
    }
    if (stor.errMalEcrit) {
      j3pAffiche(stor.lesdiv.explications, null, 'Tous les nombres ne sont pas correctement écrits !\n')
      yaexplik = true
      stor.compteurPe2++
    }
    if (stor.errYap) {
      if (stor.yapcmpt === 1) {
        j3pAffiche(stor.lesdiv.explications, null, 'Une valeur est fausse !\n')
      } else {
        j3pAffiche(stor.lesdiv.explications, null, 'Il y a des valeurs fausses !\n')
      }
      yaexplik = true
      stor.compteurPe3++
    }

    if (isFin) {
      // barre les faux
      barrelesfo()
      desactiveAll()
      j3pAffiche(stor.lesdiv.solution, null, 'Le point ' + stor.lettres[8] + ' a pour coordonnées $(\\text{ }' + j3pArrondi(stor.unix * stor.cox, 1) + '\\text{ } ; \\text{ }' + j3pArrondi(stor.uniz * stor.coz, 1) + '\\text{ } ;\\text{ } ' + j3pArrondi(stor.uniy * stor.coy, 1) + '\\text{ })$ ')
    }
    if (yaexplik) {
      stor.lesdiv.explications.classList.add('explique')
    }
  } // affCorrFaux

  function enonceMain () {
    // OBLIGATOIRE
    me.videLesZones()

    creeLesDiv()
    stor.Lexo = faisChoixExos()
    faiFigure()
    poseQuestion()

    stor.lesdiv.travail.classList.add('travail')
    stor.lesdiv.consigneG.classList.add('enonce')
    stor.lesdiv.figure.classList.add('enonce')
    me.zonesElts.MG.classList.add('fond')
    me.finEnonce()
  }

  switch (this.etat) {
    case 'enonce':
      // code exécuté au lancement de la section
      if (me.debutDeLaSection) {
        initSection()
      } else {
        enonceMain()
      }
      break // case "enonce":

    case 'correction':
      // On teste si une réponse a été saisie
      stor.lesdiv.explications.innerHTML = ''
      stor.lesdiv.solution.innerHTML = ''
      stor.lesdiv.solution.classList.remove('correction')
      stor.lesdiv.explications.classList.remove('explique')

      if (yaReponse()) {
        // Bonne réponse
        if (isRepOk()) {
          stor.lesdiv.correction.style.color = this.styles.cbien
          stor.lesdiv.correction.innerHTML = cBien

          passeToutVert()
          desactiveAll()
          stor.encours = ''
          this.score++
          this.typederreurs[0]++
          return this.finCorrection('navigation', true)
        }

        // Pas de bonne réponse
        stor.lesdiv.correction.style.color = this.styles.cfaux
        stor.lesdiv.correction.innerHTML = cFaux
        if (this.isElapsed) {
          // A cause de la limite de temps :
          this._stopTimer()

          stor.lesdiv.correction.innerHTML = tempsDepasse + '<BR>'
          this.typederreurs[10]++

          affCorrFaux(true)
          stor.lesdiv.solution.classList.add('correction')
          return this.finCorrection('navigation', true)
        }

        // Réponse fausse :
        if (me.essaiCourant < me.donneesSection.nbchances) {
          stor.lesdiv.correction.innerHTML += '<br>' + essaieEncore

          affCorrFaux(false)

          // affCorrFaux.call(this, false)
          this.typederreurs[1]++
          return this.finCorrection()
        }
        // Erreur au dernier essai
        affCorrFaux(true)
        stor.lesdiv.solution.classList.add('correction')

        this.typederreurs[2]++
        return this.finCorrection('navigation', true)
      }

      // pas de réponse
      stor.lesdiv.correction.style.color = this.styles.cfaux
      stor.lesdiv.correction.innerHTML = reponseIncomplete
      this.finCorrection()
      break // case "correction":

    case 'navigation':
      if (this.sectionTerminee()) {
        this.parcours.pe = getPe(ds, stor)
      } else {
        this.etat = 'enonce'
      }
      this.finNavigation()
      break // case "navigation":
  }
}
