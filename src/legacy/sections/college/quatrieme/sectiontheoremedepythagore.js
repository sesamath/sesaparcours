import $ from 'jquery'
import { j3pAddElt, j3pAjouteBouton, j3pArrondi, j3pDetruit, j3pDiv, j3pElement, j3pFocus, j3pNombre, j3pGetRandomInt, j3pRandomdec, j3pRandomTab, j3pRestriction, j3pVirgule, j3pStyle, j3pAddContent } from 'src/legacy/core/functions'
import ValidationZones from 'src/legacy/outils/calculatrice/ValidationZones'
import { j3pAffiche } from 'src/lib/mathquill/functions'
import { j3pCreeFenetres, j3pAfficheCroixFenetres, j3pToggleFenetres, j3pDetruitToutesLesFenetres } from 'src/legacy/core/functionsJqDialog'
import Repere from 'src/legacy/outils/geometrie/Repere'
import textesGeneriques from 'src/lib/core/textes'

const { cBien, cFaux, essaieEncore, regardeCorrection, tempsDepasse } = textesGeneriques

/*
    Isabelle FRITSCH
    novembre 2013
    EN BAS, vous trouverez le code NON COMMENTE si vous ne désirez pas copier-coller les commentaires
    Repris par Rémi en octobre 2019
*/

export const params = {
  outils: ['calculatrice', 'mathquill'],
  /**
   * Ce qui est paramétrable dans l’éditeur de graphe
   * @type {LegacySectionParamsParametres}
   */
  parametres: [
    ['nbrepetitions', 2, 'entier', 'Nombre de répétitions de la section'],
    ['indication', '', 'string', 'Indication pour chaque répétition'],
    ['limite', 0, 'entier', 'Temps disponible par répétition, en secondes'],
    ['nbchances', 2, 'entier', 'Nombre d’essais par répétition'],
    ['calcul', 'lesdeux', 'liste', 'hypotenuse : que le calcul de l’hypoténuse || cote_angle_droit : que le calcul d’un des côtés de l’angle droit || lesdeux : calcul de l’hypoténuse ou d’un côté de l’angle droit', ['hypotenuse', 'cote_angle_droit', 'lesdeux']]
  ]
}

/**
 * section theoremedepythagore
 * @this {Parcours}
 */
export default function main () {
  const me = this
  const prefix = 'theoremeDePythagore'
  const stor = me.storage
  let ds = me.donneesSection

  function genereDonnees () {
    // gère tout l’alétoire
    // On détermine le nom des sommets
    let pR, p1, p2
    do {
      pR = j3pGetRandomInt(1, 26)
      p1 = j3pGetRandomInt(1, 26)
      p2 = j3pGetRandomInt(1, 26)
    } while (pR === p1 || pR === p2 || p1 === p2)
    const nompointR = String.fromCharCode(64 + pR)
    const nompoint1 = String.fromCharCode(64 + p1)
    const nompoint2 = String.fromCharCode(64 + p2)

    // On détermine les coordonnées des sommets  (angle droit) point R aléatoire, point1 aléatoire, point 2 abscisse aléatoire et calcul pour ordonnée)
    let pointR, point1, longueur, nombre1, nombre2, calculPoint2, xPoint2, k, yPoint2
    do {
      const choix = j3pGetRandomInt(1, 4) // permet de choisir le secteur de l’angle droit
      switch (choix) {
        case 1 : // angle droit (point R) secteur bas gauche, point1 haut gauche et point2 bas droite
          do {
            do {
              pointR = { x: j3pRandomdec('[1;4],1'), y: j3pRandomdec('[1.001;3.9],1') }
              point1 = { x: j3pRandomdec('[1;4],1'), y: j3pRandomdec('[4.1;7],1') }
            } while (Math.abs(pointR.x - point1.x) < Math.pow(10, -12))
            longueur = Math.sqrt(Math.pow(point1.x - pointR.x, 2) + Math.pow(point1.y - pointR.y, 2))
            nombre1 = (-4 + pointR.x) * (point1.x - pointR.x) / (7 - pointR.y)
            nombre2 = (-7 + pointR.x) * (point1.x - pointR.x) / (1 - pointR.y)
          } while ((pointR.y - (point1.x - pointR.x) < 1) && (longueur < 1) && (nombre1 < (point1.y - pointR.y) + 10) && (nombre2 < (point1.y - pointR.y) + 10))
          calculPoint2 = 1
          break
        case 2 : // angle droit (point R) secteur haut gauche, point1 bas gauche et point2 haut droite
          do {
            do {
              pointR = { x: j3pRandomdec('[1;4],1'), y: j3pRandomdec('[4.1;7],1') }
              point1 = { x: j3pRandomdec('[1;4],1'), y: j3pRandomdec('[1;3.9],1') }
            } while (Math.abs(pointR.x - point1.x) < Math.pow(10, -12))
            longueur = Math.sqrt(Math.pow(point1.x - pointR.x, 2) + Math.pow(point1.y - pointR.y, 2))
            nombre1 = (-4 + pointR.x) * (point1.x - pointR.x) / (7 - pointR.y)
            nombre2 = (-7 + pointR.x) * (point1.x - pointR.x) / (1 - pointR.y)
          } while ((pointR.y + (point1.x - pointR.x) > 7) && (longueur < 1) && (nombre1 > (point1.y - pointR.y)) && (nombre2 > (point1.y - pointR.y)))
          calculPoint2 = 1
          break
        case 3 : // angle droit (point R) secteur haut droit, point1 bas droite et point2 haut gauche
          do {
            do {
              pointR = { x: j3pRandomdec('[4;7],1'), y: j3pRandomdec('[4.1;7],1') }
              point1 = { x: j3pRandomdec('[4;7],1'), y: j3pRandomdec('[1;3.9],1') }
            } while (Math.abs(pointR.x - point1.x) < Math.pow(10, -12))
            longueur = Math.sqrt(Math.pow(point1.x - pointR.x, 2) + Math.pow(point1.y - pointR.y, 2))
            nombre1 = (-4 + pointR.x) * (point1.x - pointR.x) / (1 - pointR.y)
            nombre2 = (-1 + pointR.x) * (point1.x - pointR.x) / (7 - pointR.y)
          } while ((pointR.y + (pointR.x - point1.x) > 7) && (longueur < 1) && (nombre1 > (point1.y - pointR.y)) && (nombre2 > (point1.y - pointR.y)))
          calculPoint2 = 2
          break
        case 4 : // angle droit (point R) secteur bas droit, point1 haut droite et point2 bas gauche
          do {
            do {
              pointR = { x: j3pRandomdec('[4;7],1'), y: j3pRandomdec('[1;3.9],1') }
              point1 = { x: j3pRandomdec('[4;7],1'), y: j3pRandomdec('[4.1;7],1') }
            } while (Math.abs(pointR.x - point1.x) < Math.pow(10, -12))
            longueur = Math.sqrt(Math.pow(point1.x - pointR.x, 2) + Math.pow(point1.y - pointR.y, 2))
            nombre1 = (-4 + pointR.x) * (point1.x - pointR.x) / (1 - pointR.y)
            nombre2 = (-1 + pointR.x) * (point1.x - pointR.x) / (7 - pointR.y)
          } while ((pointR.y - (pointR.x - point1.x) < 1) && (longueur < 1) && (nombre1 < (point1.y - pointR.y)) && (nombre2 < (point1.y - pointR.y)))
          calculPoint2 = 2
          break
      }
      switch (calculPoint2) { // Pour calculer les coordonnées du dernier sommet du triangle
        case 1:
          xPoint2 = j3pRandomdec('[4;7],1')
          k = (xPoint2 - pointR.x) / (point1.y - pointR.y)
          yPoint2 = pointR.y - k * (point1.x - pointR.x)
          break
        case 2:
          xPoint2 = j3pRandomdec('[1;4],1')
          k = (xPoint2 - pointR.x) / (point1.y - pointR.y)
          yPoint2 = pointR.y - k * (point1.x - pointR.x)
          break
      }
      // on calcule les longueurs des côtés
      me.stockage[4] = Math.sqrt(Math.pow(point1.x - pointR.x, 2) + Math.pow(point1.y - pointR.y, 2))
      me.stockage[5] = Math.sqrt(Math.pow(xPoint2 - pointR.x, 2) + Math.pow(yPoint2 - pointR.y, 2))
      // si l’un des côtés a une longueur inférieure à 1.3, l’angle droit ne sera pas bien visible (pas besoin de vérifier pour l’hypoténuse)
    } while (yPoint2 < 1 || yPoint2 > 7 || me.stockage[4] < 1.3 || me.stockage[5] < 1.3)

    // on reporte l’ensemble dans des tableaux pour simplifier la gestion
    me.stockage[1] = [nompoint1, point1.x, point1.y]
    me.stockage[2] = [nompoint2, xPoint2, yPoint2]
    me.stockage[3] = [nompointR, pointR.x, pointR.y]
    // longueur de l’hypoténuse
    me.stockage[6] = Math.sqrt(Math.pow(me.stockage[2][1] - me.stockage[1][1], 2) + Math.pow(me.stockage[2][2] - me.stockage[1][2], 2))
  }

  function construitTriangle () {
    // On construit le triangle rectangle
    // on détermine le rapport d’homotetie qui sera utile pour le codage de la figure

    const kabsi = 0.3 / (Math.sqrt(Math.pow(me.stockage[2][1] - me.stockage[3][1], 2) + Math.pow(me.stockage[2][2] - me.stockage[3][2], 2)))
    const kordo = 0.3 / (Math.sqrt(Math.pow(me.stockage[1][1] - me.stockage[3][1], 2) + Math.pow(me.stockage[1][2] - me.stockage[3][2], 2)))

    stor.repere = new Repere({
      idConteneur: prefix + 'conteneurD',
      idDivRepere: 'unrepere',
      aimantage: true,
      visible: false,
      trame: false,
      fixe: true,
      larg: 280,
      haut: 280,
      pasdunegraduationX: 1,
      pixelspargraduationX: 30,
      pasdunegraduationY: 1,
      pixelspargraduationY: 30,
      xO: 30,
      yO: 280,
      debuty: 0,
      negatifs: true,

      objets: [

        // les points du triangle
        {
          type: 'point',
          nom: me.stockage[1][0],
          par1: me.stockage[1][1],
          par2: me.stockage[1][2],
          fixe: true,
          visible: true,
          etiquette: true,
          style: { couleur: '#456', epaisseur: 2, taille: 11, taillepoint: 2, decal: me.stockage[3][0] + ',' + me.stockage[1][0] + ',' + me.stockage[2][0] }
        },
        {
          type: 'point',
          nom: me.stockage[3][0],
          par1: me.stockage[3][1],
          par2: me.stockage[3][2],
          fixe: true,
          visible: true,
          etiquette: true,
          style: { couleur: '#456', epaisseur: 2, taille: 11, taillepoint: 2, decal: me.stockage[1][0] + ',' + me.stockage[3][0] + ',' + me.stockage[2][0] }
        },
        {
          type: 'point',
          nom: me.stockage[2][0],
          par1: me.stockage[2][1],
          par2: me.stockage[2][2],
          fixe: true,
          visible: true,
          etiquette: true,
          style: { couleur: '#456', epaisseur: 2, taille: 11, taillepoint: 2, decal: me.stockage[1][0] + ',' + me.stockage[2][0] + ',' + me.stockage[3][0] }
        },

        // les côtés du triangle
        { type: 'segment', nom: 'S1', par1: me.stockage[3][0], par2: me.stockage[1][0], style: { couleur: '#456', epaisseur: 2 } },
        { type: 'segment', nom: 'S2', par1: me.stockage[3][0], par2: me.stockage[2][0], style: { couleur: '#456', epaisseur: 2 } },
        { type: 'segment', nom: 'hypotenuse', par1: me.stockage[1][0], par2: me.stockage[2][0], style: { couleur: '#456', epaisseur: 2 } },

        // le codage
        { type: 'droite', nom: 'd1', par1: me.stockage[3][0], par2: me.stockage[1][0], style: { couleur: '#FFF', epaisseur: 0 } },
        { type: 'droite', nom: 'd2', par1: me.stockage[3][0], par2: me.stockage[2][0], style: { couleur: '#FFF', epaisseur: 0 } },
        {
          type: 'pointsur',
          nom: 'C2',
          par1: 'd2',
          par2: kabsi,
          fixe: true,
          visible: false,
          etiquette: false,
          style: { couleur: '#FFF', epaisseur: 0, taille: 0, taillepoint: 0 }
        },
        {
          type: 'pointsur',
          nom: 'C1',
          par1: 'd1',
          par2: kordo,
          fixe: true,
          visible: false,
          etiquette: false,
          style: { couleur: '#FFF', epaisseur: 0, taille: 0, taillepoint: 0 }
        },
        { type: 'droiteperpendiculaire', nom: 'd3', par1: 'C2', par2: 'd2', style: { couleur: '#FFF', epaisseur: 0 } },
        { type: 'droiteperpendiculaire', nom: 'd4', par1: 'C1', par2: 'd1', style: { couleur: '#FFF', epaisseur: 0 } },
        {
          type: 'pointintersection',
          nom: 'C3',
          par1: 'd3',
          par2: 'd4',
          visible: false,
          etiquette: false,
          style: { couleur: '#FFF', epaisseur: 0, taille: 0, taillepoint: 0 }
        },
        { type: 'segment', nom: 'codage1', par1: 'C1', par2: 'C3', style: { couleur: '#456', epaisseur: 1 } },
        { type: 'segment', nom: 'codage2', par1: 'C2', par2: 'C3', style: { couleur: '#456', epaisseur: 1 } }

      ]
    })

    stor.repere.construit()
  }

  function afficheCorrection (bonneReponse) {
    // fonction appelée pour afficher la correction (que la réponse soit bonne ou fausse
    j3pDiv(prefix + 'conteneur', prefix + 'explications', '<br>')
    // le style n’est pas le même suivant que la réponse soit bonne ou fausse'
    if (bonneReponse) {
      j3pElement(prefix + 'explications').style.color = me.styles.cbien
    } else {
      j3pElement(prefix + 'explications').style.color = me.styles.toutpetit.correction.color
    }
    // le div prefix+"explications" contiendra un ou plusieurs div suivant le nombre de phrases à écrire.
    for (let i = 1; i <= 1; i++) {
      j3pDiv(prefix + 'explications', prefix + 'explication1', '')
    }
    switch (me.questionCourante % ds.nbetapes) {
      case 1:
        j3pDetruit(prefix + 'laPalette')
        j3pAffiche(prefix + 'explication1', prefix + 'corr1', ds.textes.corr1, {
          t: me.stockage[1][0] + me.stockage[2][0] + me.stockage[3][0],
          p: me.stockage[3][0],
          s: '[' + me.stockage[1][0] + me.stockage[2][0] + ']'
        })
        break
      case 2:
        j3pDetruit(prefix + 'laPalette')
        j3pAffiche(prefix + 'explication1', prefix + 'corr1', ds.textes.corr2 + '\n' + me.stockage[1][0] + me.stockage[2][0] + '$^2=$' + me.stockage[3][0] + me.stockage[2][0] + '$^2+$' + me.stockage[3][0] + me.stockage[1][0] + '$^2$',
          {
            s: me.stockage[1][0] + me.stockage[2][0]
          })
        break
      case 3:
        j3pAffiche(prefix + 'explication1', prefix + 'corr1', stor.corrq3)
        j3pDetruitToutesLesFenetres()
        break
      default:
        try {
          j3pDetruit(prefix + 'zone_consigne7')
        } catch (e) {}

        j3pAffiche(prefix + 'explication1', prefix + 'corr1', ds.textes.corr4, {
          c: stor.longueurdemande,
          d: j3pArrondi(stor.longueurCarre, 2),
          r: (Math.abs(j3pElement('partie6input1').reponse[0] - Math.round(j3pElement('partie6input1').reponse[0], 2)) < Math.pow(10, -10)) ? j3pElement('partie6input1').reponse[0] : j3pArrondi(j3pElement('partie6input1').reponse[0], 1),
          s: stor.leSigne
        })

        j3pDetruitToutesLesFenetres()
        break
    }
  }
  function AjouteCrochets (zone, leType) {
    // zone est le nom avec son id
    // leType vaut "[" ou "("
    const zoneVide = j3pElement(zone).value === ''
    const sepFermer = (leType === '[') ? ']' : ')'
    j3pElement(zone).value = leType + j3pElement(zone).value + sepFermer
    j3pFocus(zone)
    j3pElement(zone).setSelectionRange(1, 1)
    const largeurZone = j3pElement(zone).style.width
    const newLarg = Math.max(Number(largeurZone.substring(0, largeurZone.length - 2)), 35)

    if (!zoneVide)j3pElement(zone).style.width = String(newLarg) + 'px'
  }
  function afficheCarre (zone) {
    // zone est le nom avec son id
    j3pElement(zone).value += '²'
    const largeurZone = j3pElement(zone).style.width
    const newLarg = Number(largeurZone.substring(0, largeurZone.length - 2)) + 7
    j3pElement(zone).style.width = String(newLarg) + 'px'
  }
  function getDonnees () {
    return {

      // Nombre de répétitions de l’exercice
      nbrepetitions: 2,
      nbetapes: 4,

      // Si indication != "" alors un lien "Indication" s’affiche dans la zone Inférieure Gauche (zones.IG
      // une indication<br>de deux lignes...";
      indication: '',

      // pour le cas de presentation1bis
      // En effet pas d’indication possible pour cette présentation
      // surchargeindication: false;

      // Nombre de chances dont dispose l’él§ve pour répondre à la question
      nbchances: 2,

      structure: 'presentation1', //  || "presentation2" || "presentation3"  || "presentation1bis"
      demande: 'justelegalite', // ||"legaliteetlescalculs";
      calcul: 'lesdeux', // ||"hypotenuse"||"cote_angledroit";

      /*
          Les textes présents dans la section
          Sont donc accessibles dans le code par donneesSection.textes.phrase1
          Possibilité d’écrire dans le code :
          var lestextes: donneesSection.textes;
          puis accès à l’aide de lestextes.phrase1
          */
      textes: {
        titreExo: 'Théorème de Pythagore',
        consigne1: 'En s’aidant de la figure ci-contre, compléter la phrase suivante.',
        consigne2: 'Le triangle @1@ est rectangle en @2@.',
        consigne3: 'Son hypoténuse est @1@.',
        consigne4: 'Sur la figure ci-contre, le triangle £t est rectangle en £p.',
        consigne5: 'D’après le théorème de Pythagore&nbsp;:<br/>@1@$=$@2@$+$@3@.',
        consigne6: 'D’après le théorème de Pythagore&nbsp;:<br/>£h$^2=$£c$^2+$£d$^2$.',
        consigne7: 'On donne : £c$=$£v et £d$=$£w.',
        consigne8: 'On souhaite calculer £e en deux étapes.',
        consigne9: 'En s’aidant éventuellement de la calculatrice, £c$^2=$@1@.',
        consigne10: 'Ainsi, £c$£s$@1@.',
        consigne11: 'On donnera la valeur arrondie au dixième.',
        phrase20: ' Il faut compléter toutes les zones ! ',
        phrase21: ' Il y a ',
        phrase22: ' erreur(s).',
        comment1: 'Quelle est la nature de l’hypoténuse&nbsp;?',
        comment2: 'Peut-être est-ce un problème d’arrondi !',
        corr1: 'Le triangle £t est rectangle en £p.<br/>Son hypoténuse est £s.',
        corr2: 'L’hypoténuse étant le segment [£s], d’après le théorème Pythagore&nbsp;:',
        corr4: 'On obtient £c$=\\sqrt{£d}£s£r$.'
      },

      pe: 0
    }
  }

  switch (me.etat) {
    case 'enonce': {
      // code exécuté au lancement de la section
      if (me.debutDeLaSection) {
        me.donneesSection = getDonnees()
        ds = me.donneesSection

        me.surcharge()
        // Construction de la page
        me.construitStructurePage(ds.structure)

        // par convention, me.stockage[0] contient la solution
        // les autres peuvent contenir ce que vous voulez, en général des valeurs que l’on doit mémoriser entre les différents appels de la section
        me.stockage = [0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0]

        /*
                 Par convention,`
                `   me.typederreurs[0] = nombre de bonnes réponses
                    me.typederreurs[1] = nombre de fois où l’élève a utilisé une chance supplémentaire
                    me.typederreurs[2] = nombre de mauvaises réponses
                    me.typederreurs[10] = nombre de fois où l’élève a eu faux à cause d’un temps limite
                    LES AUTRES peuvent être utilisées par la détermination de la pe dans le cas d’une section qualitative
                 */
        me.typederreurs = [0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0]
        me.afficheTitre(ds.textes.titreExo)
        if (ds.indication) me.indication(me.zones.IG, ds.indication)
      } else {
        me.videLesZones()
      }

      /// //////////////////////////////////
      /* LE CODE PRINCIPAL DE LA SECTION */
      /// //////////////////////////////////
      j3pDiv(me.zones.MG, {
        id: prefix + 'conteneur',
        contenu: '',
        style: me.styles.etendre('petit.enonce', { padding: '10px' })
      })
      j3pDiv(me.zones.MD, {
        id: prefix + 'conteneurD',
        contenu: '',
        style: me.styles.etendre('petit.enonce', { padding: '10px' })
      })
      stor.zoneCalc = j3pAddElt(prefix + 'conteneurD', 'div', '', { style: { paddingBottom: '10px' } })
      let zonesSaisie
      let longueurdemande
      switch (me.questionCourante % ds.nbetapes) {
        case 1: {
          // 1ère étape ; identifier triangle et hypoténuse
          for (let i = 1; i <= 3; i++) {
            j3pDiv(prefix + 'conteneur', prefix + 'zone_consigne' + i, '')
          }
          genereDonnees() // fonction du début de la section (ligne 125)
          construitTriangle() // fonction du début de la section (ligne 200)

          me.stockage[90] = {
            triangle: me.stockage[3][0] + me.stockage[1][0] + me.stockage[2][0],
            angledroit: me.stockage[3][0],
            hypotenuse: me.stockage[1][0] + me.stockage[2][0]
          }
          // on stocke les solutions de la 1ère étape dans un tableau {triangle,angledroit,hypotenuse}

          j3pAffiche(prefix + 'zone_consigne1', 'partie1', ds.textes.consigne1)
          j3pAffiche(prefix + 'zone_consigne2', 'partie2', ds.textes.consigne2,
            {
              input1: { texte: '', dynamique: true, width: '12px', maxchars: 3 },
              input2: { texte: '', dynamique: true, width: '12px', maxchars: 1 }
            })
          j3pAffiche(prefix + 'zone_consigne3', 'partie3', ds.textes.consigne3,
            {
              input1: { texte: '', dynamique: true, width: '12px', maxchars: 4 }
            })
          j3pRestriction('partie2input1', 'A-Za-z') // On ne peut plus saisir que des lettres
          j3pRestriction('partie2input2', 'A-Za-z') // On ne peut plus saisir que des lettres
          j3pRestriction('partie3input1', 'A-Za-z\\[\\]\\(\\)') // On ne peut plus saisir que des lettres
          for (let i = 1; i <= 2; i++) {
            j3pElement('partie2input' + i).addEventListener('input',
              function () { this.value = this.value.toUpperCase() }, false)
          }
          const partie3input1 = j3pElement('partie3input1')
          partie3input1.addEventListener('input', function () {
            this.value = this.value.toUpperCase()
            if ((this.value.charAt(this.value.length - 1) === ')') || (this.value.charAt(this.value.length - 1) === ']')) {
              partie3input1.setSelectionRange(this.value.length - 1, this.value.length - 1)
            }
          }, false)

          // la palette de boutons
          const divPalette = j3pAddElt(prefix + 'conteneur', 'div', '', { id: prefix + 'laPalette', style: { visibility: 'hidden' } })
          const showPalette = () => { divPalette.style.visibility = 'visible' }
          const hidePalette = () => { divPalette.style.visibility = 'hidden' }
          // bouton crochets
          const clickCrochetsListener = () => {
            if ((j3pElement('partie3input1').value[0] !== '[') && (j3pElement('partie3input1').value[0] !== '(')) {
              AjouteCrochets('partie3input1', '[')
            }
          }
          j3pAjouteBouton(divPalette, clickCrochetsListener, { className: 'mqBtn', value: '[..]' })
          // bouton parenthèses
          const clickParenthesesListener = () => {
            if ((j3pElement('partie3input1').value[0] !== '[') && (j3pElement('partie3input1').value[0] !== '(')) {
              AjouteCrochets('partie3input1', '(')
            }
          }
          j3pAjouteBouton(divPalette, clickParenthesesListener, { className: 'mqBtn', value: '(..)' })
          // le comportement au focus, on montre la palette pour le dernier input seulement
          $('#partie3input1').focusin(showPalette)
          for (let i = 1; i <= 2; i++) {
            $('#partie2input' + i).focusin(hidePalette)
          }
          j3pFocus('partie2input1') // On donne le focus à la 1ère zone de saisie
          j3pElement('partie2input1').typeReponse = ['texte']
          j3pElement('partie2input2').typeReponse = ['texte']
          j3pElement('partie3input1').typeReponse = ['texte']
          j3pElement('partie2input1').reponse = [me.stockage[3][0] + me.stockage[1][0] + me.stockage[2][0], me.stockage[3][0] + me.stockage[2][0] + me.stockage[1][0], me.stockage[1][0] + me.stockage[2][0] + me.stockage[3][0], me.stockage[1][0] + me.stockage[3][0] + me.stockage[2][0], me.stockage[2][0] + me.stockage[1][0] + me.stockage[3][0], me.stockage[2][0] + me.stockage[3][0] + me.stockage[1][0]]
          j3pElement('partie2input2').reponse = [me.stockage[3][0]]
          j3pElement('partie3input1').reponse = ['[' + me.stockage[1][0] + me.stockage[2][0] + ']', '[' + me.stockage[2][0] + me.stockage[1][0] + ']']
          zonesSaisie = ['partie2input1', 'partie2input2', 'partie3input1']
          stor.fctsValid = new ValidationZones({ zones: zonesSaisie, parcours: me })
          break // fin 1ère étape
        }

        case 2: {
          // 2ème étape ; écrire théorème de pythagore
          construitTriangle() // fonction du début de la section (ligne 200)
          for (let i = 1; i <= 3; i++) {
            j3pDiv(prefix + 'conteneur', prefix + 'zone_consigne' + i, '')
          }
          const p2 = String.fromCharCode(178) /// pour le carré !

          me.stockage[91] = {
            hypotenuse: me.stockage[1][0] + me.stockage[2][0] + p2,
            terme1: me.stockage[3][0] + me.stockage[1][0] + p2,
            terme2: me.stockage[3][0] + me.stockage[2][0] + p2
          }
          // on stocke les solutions de la 2ème étape dans un tableau {hypoténuse,terme1,terme2} ; le carré fait partie pour chaque zone de la solution
          // consigne4 : "Sur la figure ci-contre, le triangle £t est rectangle en £p.",
          // consigne5 : "D’après le théorème de Pythagore&nbsp;:<br/>@1@$=$@2@$+$@3@.",
          j3pAffiche(prefix + 'zone_consigne1', 'partie1', ds.textes.consigne4, {
            t: me.stockage[3][0] + me.stockage[1][0] + me.stockage[2][0],
            p: me.stockage[3][0]
          })
          j3pAffiche(prefix + 'zone_consigne2', 'partie2', ds.textes.consigne5, {
            input1: { texte: '', dynamique: true, width: '12px', maxchars: 3 },
            input2: { texte: '', dynamique: true, width: '12px', maxchars: 3 },
            input3: { texte: '', dynamique: true, width: '12px', maxchars: 3 }
          })
          for (let i = 1; i <= 3; i++) {
            j3pRestriction('partie2input' + i, 'A-Za-z²') // On ne peut plus saisir que des lettres
            j3pElement('partie2input' + i).addEventListener('keyup', function () { this.value = this.value.toUpperCase() }, false)
          }
          j3pDiv(prefix + 'conteneur', prefix + 'laPalette', '')
          j3pStyle(prefix + 'laPalette', { paddingTop: '20px' })
          j3pAjouteBouton(prefix + 'laPalette', () => afficheCarre('partie2input' + numZone), { className: 'mqBtn', value: 'x²' })
          let numZone = 1
          for (let i = 1; i <= 3; i++) {
            $('#' + 'partie2input' + i).focusin(function () {
              numZone = String(this.id).split('input')[1]
            })
          }
          j3pElement('partie2input1').typeReponse = ['texte']
          j3pElement('partie2input2').typeReponse = ['texte']
          j3pElement('partie2input3').typeReponse = ['texte']
          j3pElement('partie2input1').reponse = [me.stockage[1][0] + me.stockage[2][0] + '²', me.stockage[2][0] + me.stockage[1][0] + '²']
          j3pElement('partie2input2').reponse = [me.stockage[3][0] + me.stockage[2][0] + '²']
          j3pElement('partie2input3').reponse = [me.stockage[3][0] + me.stockage[1][0] + '²']
          zonesSaisie = ['partie2input1', 'partie2input2', 'partie2input3']
          stor.fctsValid = new ValidationZones({
            zones: zonesSaisie,
            validePerso: ['partie2input2', 'partie2input3'],
            parcours: me
          })
          j3pFocus('partie2input1') // On donne le focus à la 1ère zone de saisie
          break // fin 2ème étape
        }

        case 3: {
          // 3ème étape ; on reporte les longeurs des côtés et on demande le calcul du troisième
          construitTriangle() // fonction du début de la section (ligne 200)
          for (let i = 1; i <= 6; i++) {
            j3pDiv(prefix + 'conteneur', prefix + 'zone_consigne' + i, '')
          }
          j3pAffiche(prefix + 'zone_consigne1', 'partie1', ds.textes.consigne4, {
            t: me.stockage[3][0] + me.stockage[1][0] + me.stockage[2][0],
            p: me.stockage[3][0]
          })
          j3pAffiche(prefix + 'zone_consigne2', 'partie2', ds.textes.consigne6, {
            h: me.stockage[1][0] + me.stockage[2][0],
            c: me.stockage[3][0] + me.stockage[1][0],
            d: me.stockage[3][0] + me.stockage[2][0]
          })
          let typeQuest = ds.calcul
          if (typeQuest === 'lesdeux') {
            typeQuest = j3pRandomTab(['hypotenuse', 'cote_angledroit'], [0.5, 0.5])
          }
          const longueur1 = me.stockage[3][0] + me.stockage[1][0] // fait partie de l’affichage des longueurs
          const longueur11 = j3pArrondi(me.stockage[4], 1)
          const longueur2 = me.stockage[3][0] + me.stockage[2][0] // fait partie de l’affichage des longueurs
          const longueur21 = j3pArrondi(me.stockage[5], 1)
          const hypotenuse = me.stockage[1][0] + me.stockage[2][0] // fait partie de l’affichage des longueurs
          const hypotenuselong = j3pArrondi(me.stockage[6], 1)
          stor.donnees = {
            longueur1,
            longueur11,
            longueur2,
            longueur21,
            hypotenuse,
            hypotenuselong
          }
          let queldemande
          if (typeQuest === 'hypotenuse') {
            j3pAffiche(prefix + 'zone_consigne3', 'partie3', ds.textes.consigne7, {
              c: longueur1,
              v: j3pVirgule(longueur11),
              d: longueur2,
              w: j3pVirgule(longueur21)
            })
            j3pAffiche(prefix + 'zone_consigne4', 'partie4', ds.textes.consigne8, {
              e: me.stockage[1][0] + me.stockage[2][0]
            })
            j3pAffiche(prefix + 'zone_consigne5', 'partie5', me.stockage[1][0] + me.stockage[2][0] + '$^2=$@1@$^2+$@2@$^2$', {
              input1: { texte: '', dynamique: true, width: '12px', maxchars: 7 },
              input2: { texte: '', dynamique: true, width: '12px', maxchars: 7 }
            })
            j3pAffiche(prefix + 'zone_consigne6', 'partie6', ds.textes.consigne9, {
              c: me.stockage[1][0] + me.stockage[2][0],
              input1: { texte: '', dynamique: true, width: '12px', maxchars: 8 }
            })
            j3pElement('partie5input1').reponse = [j3pArrondi(me.stockage[4], 1)]
            j3pElement('partie5input2').reponse = [j3pArrondi(me.stockage[5], 1)]
            j3pElement('partie6input1').reponse = [Math.pow(j3pArrondi(me.stockage[4], 1), 2) + Math.pow(j3pArrondi(me.stockage[5], 1), 2)]
            stor.corrq3 = me.stockage[1][0] + me.stockage[2][0] + '$^2=$' + j3pVirgule(j3pElement('partie5input1').reponse[0]) + '$^2+$' + j3pVirgule(j3pElement('partie5input2').reponse[0]) + '$^2$=' + j3pVirgule(j3pArrondi(j3pElement('partie6input1').reponse[0], 2))
          } else {
            // enonce_cote_angledroit();
            queldemande = j3pGetRandomInt(1, 2)
            if (queldemande === 1) {
              j3pAffiche(prefix + 'zone_consigne3', 'partie3', ds.textes.consigne7, {
                c: hypotenuse,
                v: j3pVirgule(hypotenuselong),
                d: longueur2,
                w: j3pVirgule(longueur21)
              })
              longueurdemande = me.stockage[3][0] + me.stockage[1][0]
            } else {
              j3pAffiche(prefix + 'zone_consigne3', 'partie3', ds.textes.consigne7, {
                c: hypotenuse,
                v: j3pVirgule(hypotenuselong),
                d: longueur1,
                w: j3pVirgule(longueur11)
              })
              longueurdemande = me.stockage[3][0] + me.stockage[2][0]
            }
            j3pAffiche(prefix + 'zone_consigne4', 'partie4', ds.textes.consigne8, {
              e: longueurdemande
            })
            j3pAffiche(prefix + 'zone_consigne5', 'partie5', longueurdemande + '$^2=$@1@$^2-$@2@$^2$', {
              input1: { texte: '', dynamique: true, width: '12px', maxchars: 7 },
              input2: { texte: '', dynamique: true, width: '12px', maxchars: 7 }
            })
            j3pAffiche(prefix + 'zone_consigne6', 'partie6', ds.textes.consigne9, {
              c: longueurdemande,
              input1: { texte: '', dynamique: true, width: '12px', maxchars: 7 }
            })
            j3pElement('partie5input1').reponse = [hypotenuselong]
            if (queldemande === 1) {
              j3pElement('partie5input2').reponse = [longueur21]
              j3pElement('partie6input1').reponse = [Math.pow(hypotenuselong, 2) - Math.pow(longueur21, 2)]
              stor.corrq3 = me.stockage[3][0] + me.stockage[1][0] + '$^2=$' + j3pVirgule(hypotenuselong) + '$^2-$' + j3pVirgule(longueur21) + '$^2$=' + j3pVirgule(j3pArrondi(j3pElement('partie6input1').reponse[0], 2))
            } else {
              j3pElement('partie5input2').reponse = [longueur11]
              j3pElement('partie6input1').reponse = [Math.pow(hypotenuselong, 2) - Math.pow(longueur11, 2)]
              stor.corrq3 = me.stockage[3][0] + me.stockage[2][0] + '$^2=$' + j3pVirgule(hypotenuselong) + '$^2-$' + j3pVirgule(longueur11) + '$^2$=' + j3pVirgule(j3pArrondi(j3pElement('partie6input1').reponse[0], 2))
            }
          }

          j3pFocus('partie5input1') // On donne le focus à la 1ère zone de saisie

          j3pRestriction('partie5input1', '0-9,.') //  on restreint la saisie aux chiffres
          j3pRestriction('partie5input2', '0-9,.')
          j3pRestriction('partie6input1', '0-9,.')
          j3pElement('partie5input1').typeReponse = ['nombre', 'exact']
          j3pElement('partie5input2').typeReponse = ['nombre', 'exact']
          j3pElement('partie6input1').typeReponse = ['nombre', 'exact']

          zonesSaisie = ['partie5input1', 'partie5input2', 'partie6input1']
          for (let i = 0; i < zonesSaisie.length; i++) {
            j3pElement(zonesSaisie[i]).addEventListener('input',
              function () { this.value = this.value.replace('.', ',') }, false)
          }
          if (typeQuest === 'hypotenuse') {
            stor.fctsValid = new ValidationZones({
              zones: zonesSaisie,
              validePerso: ['partie5input1', 'partie5input2'],
              parcours: me
            })
          } else {
            stor.fctsValid = new ValidationZones({
              zones: zonesSaisie,
              parcours: me
            })
          }
          stor.typeQuest = typeQuest
          // FIXME queldemande peut être undefined si on est pas passé dans le else qui l’affecte
          stor.coteDemande = queldemande
          stor.longueurCarre = j3pElement('partie6input1').reponse[0]
          break // fin de la 3ème étape
        }

        case 0: {
          // 4ème étape ; on prend la racine carrée et on donne le résultat final
          construitTriangle() // fonction du début de la section (ligne 200)
          for (let i = 1; i <= 7; i++) {
            j3pDiv(prefix + 'conteneur', prefix + 'zone_consigne' + i, '')
          }
          j3pAffiche(prefix + 'zone_consigne1', 'partie1', ds.textes.consigne4, {
            t: me.stockage[3][0] + me.stockage[1][0] + me.stockage[2][0],
            p: me.stockage[3][0]
          })
          j3pAffiche(prefix + 'zone_consigne2', 'partie2', ds.textes.consigne6, {
            h: me.stockage[1][0] + me.stockage[2][0],
            c: me.stockage[3][0] + me.stockage[1][0],
            d: me.stockage[3][0] + me.stockage[2][0]
          })
          if (stor.typeQuest === 'hypotenuse') {
            j3pAffiche(prefix + 'zone_consigne3', 'partie3', ds.textes.consigne7, {
              c: stor.donnees.longueur1,
              v: j3pVirgule(stor.donnees.longueur11),
              d: stor.donnees.longueur2,
              w: j3pVirgule(stor.donnees.longueur21)
            })
            j3pAffiche(prefix + 'zone_consigne4', 'partie4', ds.textes.consigne8, {
              e: me.stockage[1][0] + me.stockage[2][0]
            })
            longueurdemande = me.stockage[1][0] + me.stockage[2][0]
          } else {
            if (stor.coteDemande === 1) {
              j3pAffiche(prefix + 'zone_consigne3', 'partie3', ds.textes.consigne7, {
                c: stor.donnees.hypotenuse,
                v: j3pVirgule(stor.donnees.hypotenuselong),
                d: stor.donnees.longueur2,
                w: j3pVirgule(stor.donnees.longueur21)
              })
              longueurdemande = me.stockage[3][0] + me.stockage[1][0]
            } else {
              j3pAffiche(prefix + 'zone_consigne3', 'partie3', ds.textes.consigne7, {
                c: stor.donnees.hypotenuse,
                v: j3pVirgule(stor.donnees.hypotenuselong),
                d: stor.donnees.longueur1,
                w: j3pVirgule(stor.donnees.longueur11)
              })
              longueurdemande = me.stockage[3][0] + me.stockage[2][0]
            }
            j3pAffiche(prefix + 'zone_consigne4', 'partie4', ds.textes.consigne8, {
              e: longueurdemande
            })
          }
          j3pAffiche(prefix + 'zone_consigne5', 'partie5', stor.corrq3)
          const laReponse = Math.sqrt(stor.longueurCarre)
          stor.leSigne = (Math.abs(laReponse - Math.round(laReponse, 2)) < Math.pow(10, -10)) ? '=' : '\\approx'
          j3pAffiche(prefix + 'zone_consigne6', 'partie6', ds.textes.consigne10, {
            c: longueurdemande,
            s: stor.leSigne,
            input1: { texte: '', dynamique: true, width: '12px', maxchars: 7 }
          })
          j3pRestriction('partie6input1', '0-9,.')
          if (Math.abs(laReponse - Math.round(laReponse, 2)) < Math.pow(10, -10)) {
            j3pElement('partie6input1').typeReponse = ['nombre', 'exact']
          } else {
            j3pElement('partie6input1').typeReponse = ['nombre', 'arrondi', 0.1]
            j3pAffiche(prefix + 'zone_consigne7', 'partie7', ds.textes.consigne11)
            const $partie7 = $('#partie7')
            let tailleFont = $partie7.css('font-size')
            tailleFont = Number(tailleFont.substring(0, tailleFont.length - 2)) - 2
            $partie7.css('font-size', String(tailleFont + 'px'))
          }
          j3pElement('partie6input1').reponse = [Math.sqrt(stor.longueurCarre)]
          j3pRestriction('partie6input1', '0-9,.')
          j3pElement('partie6input1').addEventListener('input', function () { this.value = this.value.replace('.', ',') }, false)
          stor.longueurdemande = longueurdemande
          zonesSaisie = ['partie6input1']
          stor.fctsValid = new ValidationZones({ zones: zonesSaisie, parcours: me })
          break // fin de la 4ème étape
        }
      }
      if ((me.questionCourante % ds.nbetapes === 0) || (me.questionCourante % ds.nbetapes === 3)) {
        // on propose la calculatrice
        me.fenetresjq = [{ name: 'Calculatrice', title: 'Calculatrice' }]
        j3pCreeFenetres(me)
        j3pAjouteBouton(stor.zoneCalc, j3pToggleFenetres.bind(null, 'Calculatrice'), { id: 'Calculatrice', className: 'MepBoutons', value: 'Calculatrice' })
        j3pAfficheCroixFenetres('Calculatrice')
      }
      j3pFocus(zonesSaisie[0])
      j3pAddElt(prefix + 'conteneurD', 'div', '', { id: prefix + 'correction', style: me.styles.moyen.correction })
      me.finEnonce()
      break // case "enonce" ; fin de etat = enonce
    }

    case 'correction': {
      // On teste si une réponse a été saisie
      const fctsValid = stor.fctsValid// ici je crée juste une variable pour raccourcir le nom de l’oject fctsValid
      // ce qui suit sert pour la validation de toutes les zones
      // le tableau contenant toutes les zones de saisie
      const reponse = fctsValid.validationGlobale()
      let bonneReponse2
      if (reponse.aRepondu) {
        if (me.questionCourante % ds.nbetapes === 2) {
          bonneReponse2 = false
          bonneReponse2 = (bonneReponse2 || (fctsValid.valideUneZone('partie2input2', [me.stockage[3][0] + me.stockage[2][0] + '²', me.stockage[2][0] + me.stockage[3][0] + '²']).bonneReponse && fctsValid.valideUneZone('partie2input3', [me.stockage[3][0] + me.stockage[1][0] + '²', me.stockage[1][0] + me.stockage[3][0] + '²']).bonneReponse))
          bonneReponse2 = (bonneReponse2 || (fctsValid.valideUneZone('partie2input2', [me.stockage[3][0] + me.stockage[1][0] + '²', me.stockage[1][0] + me.stockage[3][0] + '²']).bonneReponse && fctsValid.valideUneZone('partie2input3', [me.stockage[3][0] + me.stockage[2][0] + '²', me.stockage[2][0] + me.stockage[3][0] + '²']).bonneReponse))
          reponse.bonneReponse = (reponse.bonneReponse && bonneReponse2)
          // bonneReponse2 ne prend en compte que les zones dans validePerso
          if (!bonneReponse2) {
            fctsValid.zones.bonneReponse[1] = false
            fctsValid.zones.bonneReponse[2] = false
            // si les 2 zones de validePerso ne sont pas bonnes, on met false dans fctsValid.zones.bonneReponse[i] où i est la position de la zone dans le tableau zonesSaisie
          }
          // on appelle alors la fonction qui va mettre en couleur les réponses bonnes ou fausses de ces 2 zones
          fctsValid.coloreUneZone('partie2input2')
          fctsValid.coloreUneZone('partie2input3')
        } else if (me.questionCourante % ds.nbetapes === 3) {
          if (fctsValid.zones.validePerso[0]) {
            // Nous sommes dans le cas de l’hypoténuse à calculer
            bonneReponse2 = false
            bonneReponse2 = (bonneReponse2 || (fctsValid.valideUneZone('partie5input1', j3pElement('partie5input1').reponse).bonneReponse && fctsValid.valideUneZone('partie5input2', j3pElement('partie5input2').reponse).bonneReponse))
            bonneReponse2 = (bonneReponse2 || (fctsValid.valideUneZone('partie5input1', j3pElement('partie5input2').reponse).bonneReponse && fctsValid.valideUneZone('partie5input2', j3pElement('partie5input1').reponse).bonneReponse))
            reponse.bonneReponse = (reponse.bonneReponse && bonneReponse2)
            // bonneReponse2 ne prend en compte que les zones dans validePerso
            if (!bonneReponse2) {
              fctsValid.zones.bonneReponse[0] = false
              fctsValid.zones.bonneReponse[1] = false
              // si les 2 zones de validePerso ne sont pas bonnes, on met false dans fctsValid.zones.bonneReponse[i] où i est la position de la zone dans le tableau zonesSaisie
            }
            // on appelle alors la fonction qui va mettre en couleur les réponses bonnes ou fausses de ces 2 zones
            fctsValid.coloreUneZone('partie5input1')
            fctsValid.coloreUneZone('partie5input2')
          }
        }
      }
      if ((!reponse.aRepondu) && (!me.isElapsed)) {
        // ici on donne le message à afficher si une ou plusieurs zone(s) n’est pas complétée (tout d’abord on met en rouge : styles.cfaux)
        me.reponseManquante(prefix + 'correction')
      } else {
        // Une réponse a été saisie
        // Bonne réponse
        if (reponse.bonneReponse) {
          me._stopTimer()
          me.score++
          j3pElement(prefix + 'correction').style.color = me.styles.cbien
          j3pAddContent(prefix + 'correction', cBien, { replace: true })
          // Même si la réponse est bonne on peut afficher des éléments de correction/explication :
          // afficheCorrection(true);
          me.typederreurs[0]++
          me.cacheBoutonValider()
          try {
            j3pDetruit(prefix + 'laPalette')
          } catch (e) {}
          try {
            j3pDetruitToutesLesFenetres()
          } catch (e) {}
          me.etat = 'navigation'
          me.sectionCourante()
        } else {
          // Pas de bonne réponse
          j3pElement(prefix + 'correction').style.color = me.styles.cfaux

          // A cause de la limite de temps :
          if (me.isElapsed) { // limite de temps
            me._stopTimer()
            j3pAddContent(prefix + 'correction', tempsDepasse, { replace: true })
            me.typederreurs[10]++
            me.cacheBoutonValider()
            // L’élève avait un temps limite mais ne l’a pas respecté. On affiche alors la correction
            afficheCorrection(false)
            me.etat = 'navigation'
            me.sectionCourante()
          } else {
            // Réponse fausse :
            j3pAddContent(prefix + 'correction', cFaux, { replace: true })
            if (me.questionCourante % ds.nbetapes === 1) {
              // si l’élève oublie les crochets dans la troisième zone, je mets un commentaire particulier
              if (!fctsValid.zones.bonneReponse[2]) j3pAddContent(prefix + 'correction', ds.textes.comment1)
            } else if (me.questionCourante % ds.nbetapes === 0) {
              if (Math.abs(j3pElement('partie6input1').reponse[0] - j3pNombre(fctsValid.zones.reponseSaisie[0])) <= 0.1) {
                // pb d’arrondi
                j3pAddContent(prefix + 'correction', ds.textes.comment2)
              }
            }
            if (me.essaiCourant < ds.nbchances) {
              j3pAddContent(prefix + 'correction', '<br>' + essaieEncore)
              me.typederreurs[1]++
              // indication éventuelle ici
              // ici il a encore la possibilité de se corriger
            } else {
              // Erreur au nème essai
              me._stopTimer()
              me.cacheBoutonValider()
              j3pAddContent(prefix + 'correction', '<br>' + regardeCorrection)
              // Là il ne peut plus se corriger. On lui affiche alors la solution
              afficheCorrection(false)
              me.typederreurs[2]++
              me.etat = 'navigation'
              me.sectionCourante()
            }
          }
        }
      }
      // Obligatoire
      me.finCorrection()
      break // case "correction":
    }

    case 'navigation':
      if (!this.sectionTerminee()) this.etat = 'enonce'
      this.finNavigation()
      break
  }
  // console.log("fin du code : ",me.etat)
}
