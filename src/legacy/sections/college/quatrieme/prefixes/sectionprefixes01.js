import { j3pAddElt, j3pAjouteBouton, j3pArrondi, j3pClone, j3pEmpty, j3pGetRandomBool, j3pGetRandomInt, j3pShowError, j3pShuffle } from 'src/legacy/core/functions'
import ListeDeroulante from 'src/legacy/outils/listeDeroulante/ListeDeroulante'
import { j3pAffiche } from 'src/lib/mathquill/functions'
import { addDefaultTable } from 'src/legacy/themes/table'
import ZoneStyleMathquill3 from 'src/legacy/outils/zoneStyleMathquill/ZoneStyleMathquill3'
import { j3pCreeFenetres, j3pAfficheCroixFenetres, j3pToggleFenetres } from 'src/legacy/core/functionsJqDialog'
import { affichepuis } from 'src/legacy/outils/zoneStyleMathquill/functions'
import textesGeneriques from 'src/lib/core/textes'

const { cBien, cFaux, essaieEncore, reponseIncomplete, tempsDepasse } = textesGeneriques

/**
 * Ajouter une description ici
 * @fileOverview
 * @author Tommy Barroy <t.barroy@laposte.net>
 * @since 2017-10
*/
export const params = {
  outils: ['mathquill'],
  /**
   * Ce qui est paramétrable dans l’éditeur de graphe
   * @type {LegacySectionParamsParametres}
   */
  parametres: [
    ['nbrepetitions', 3, 'entier', 'Nombre de répétitions de la section'],
    ['nbchances', 2, 'entier', 'Nombre d’essais possibles'],
    ['limite', 0, 'entier', 'Temps disponible (en secondes)'],
    ['prefixe', 'les deux', 'liste', '<u>donnée</u>: Le nombre est donnée avec un préfixe. <br><br> <u>réponse</u>: Réponse avec préfixe attendue.', ['donnée', 'réponse', 'les deux', 'non']],
    ['symbole', 'les deux', 'liste', '<u>donnée</u>: Le nombre est donnée avec un symbole pour préfixe. <br><br> <u>réponse</u>: Réponse avec symbole attendue.', ['donnée', 'réponse', 'les deux', 'non']],
    ['puissance', 'les deux', 'liste', '<u>donnée</u>: Le nombre est donnée avec une puissance de 10. <br><br> <u>réponse</u>: Réponse avec une puissance de 10.', ['donnée', 'réponse', 'les deux', 'non']],
    ['signification', 'les deux', 'liste', '<u>donnée</u>: Le nombre est donnée avec une expression en lettre (millions, millièmes) . <br><br> <u>réponse</u>: Réponse avec une expressions en lettres attendue.', ['donnée', 'réponse', 'les deux', 'non']],
    // le modèle gère la compatibilité ascendante avec l’ancien paramètre MiseEnPagePlusClaire
    ['theme', 'standard', 'liste', 'Modèle pour l’affichage', ['standard', 'zonesAvecImageDeFond']]
  ]
}

/**
 * section prefixes01
 * @this {Parcours}
 */
export default function main () {
  const me = this
  const stor = this.storage
  const ds = me.donneesSection

  function initSection () {
    let i
    me.validOnEnter = false // ex donneesSection.touche_entree

    // Construction de la page
    me.construitStructurePage('presentation1bis')

    ds.lesExos = []

    let lp = []
    if (ds.puissance === 'les deux' || ds.puissance === 'donnée') lp.push('puis')
    if (ds.signification === 'les deux' || ds.signification === 'donnée') lp.push('sin')
    if (ds.prefixe === 'les deux' || ds.prefixe === 'donnée') lp.push('pref')
    if (ds.symbole === 'les deux' || ds.symbole === 'donnée') lp.push('symb')
    if (lp.length === 0) {
      j3pShowError('erreur paramétrage: manque donnée')
      lp = ['pref']
    }
    lp = j3pShuffle(lp)
    for (i = 0; i < me.donneesSection.nbitems; i++) {
      ds.lesExos.push({ donnee: lp[i % lp.length] })
    }
    ds.lesExos = j3pShuffle(ds.lesExos)

    lp = []
    if (ds.puissance === 'les deux' || ds.puissance === 'réponse') lp.push('puis')
    if (ds.signification === 'les deux' || ds.signification === 'réponse') lp.push('sin')
    if (ds.prefixe === 'les deux' || ds.prefixe === 'réponse') lp.push('pref')
    if (ds.symbole === 'les deux' || ds.symbole === 'réponse') lp.push('symb')
    if (lp.length === 0) {
      j3pShowError('erreur paramétrage: manque réponse')
      lp = ['puis']
    }
    stor.ModeRep = j3pClone(lp)

    lp = ['giga', 'méga', 'kilo', 'milli', 'micro', 'nano']
    lp = j3pShuffle(lp)
    for (i = 0; i < me.donneesSection.nbitems; i++) {
      ds.lesExos[i].pref = lp[i % lp.length]
    }
    ds.lesExos = j3pShuffle(ds.lesExos)

    lp = ['s', 'm', 'g', 'L', 'o', 'W']
    lp = j3pShuffle(lp)
    for (i = 0; i < me.donneesSection.nbitems; i++) {
      ds.lesExos[i].unite = lp[i % lp.length]
    }
    ds.lesExos = j3pShuffle(ds.lesExos)

    me.typederreurs = [0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0]
    me.score = 0
    const tt = 'Préfixes'

    me.afficheTitre(tt)

    enonceMain()
  }

  function faisChoixExos () {
    const e = ds.lesExos.pop()
    stor.restric = '-0123456789'
    stor.lenb = j3pGetRandomInt(11, 100) / 10
    if (j3pGetRandomBool()) stor.lenb = stor.lenb / 10
    stor.lenb = j3pArrondi(stor.lenb, 1)
    if (stor.lenb === 1) stor.lenb = 2
    switch (e.pref) {
      case 'giga':
        stor.nom = 'giga'
        stor.puis = 9
        stor.sin = 'milliards'
        stor.symb = 'G'
        break
      case 'méga':
        stor.nom = 'méga'
        stor.puis = 6
        stor.sin = 'millions'
        stor.symb = 'M'
        break
      case 'kilo':
        stor.nom = 'kilo'
        stor.puis = 3
        stor.sin = 'milliers'
        stor.symb = 'k'
        break
      case 'milli':
        if (e.unite === 'o') e.unite = 'm'
        stor.nom = 'milli'
        stor.puis = -3
        stor.sin = 'millièmes'
        stor.symb = 'm'
        break
      case 'micro':
        if (e.unite === 'o') e.unite = 'm'
        stor.nom = 'micro'
        stor.puis = -6
        stor.sin = 'millionièmes'
        stor.symb = 'μ'
        break
      case 'nano':
        if (e.unite === 'o') e.unite = 'm'
        stor.nom = 'nano'
        stor.puis = -9
        stor.sin = 'milliardièmes'
        stor.symb = 'n'
        break
    }
    stor.yapuis = stor.yasymb = stor.yapref = stor.yasin = false
    return e
  }

  function poseQuestion () {
    let buf = 'On considère ' + sinSin3(stor.Lexo.unite) + ' $N '
    let yaq = false
    switch (stor.Lexo.donnee) {
      case 'puis':
        buf += ' = ' + stor.lenb + ' \\times 10^{' + stor.puis + '}$ ' + stor.Lexo.unite
        break
      case 'sin':
        buf += '$ égal' + sinSin4(stor.Lexo.unite) + ' à $' + stor.lenb + '$ ' + stor.sin + sinSin(stor.Lexo.unite)
        break
      case 'pref':
        buf += ' = ' + stor.lenb + ' $ ' + stor.nom + sinSin1(stor.Lexo.unite)
        break
      case 'symb':
        buf += ' = ' + stor.lenb + ' $ ' + stor.symb + stor.Lexo.unite
        break
    }
    j3pAffiche(stor.lesdiv.consigne, null, buf + '\n')
    if (stor.ModeRep.length > 1) {
      j3pAffiche(stor.lesdiv.consigne, null, '<b>Complète les lignes suivantes </b>\n')
    } else { j3pAffiche(stor.lesdiv.consigne, null, '<b>Complète la ligne suivante </b>\n') }

    if (stor.ModeRep.indexOf('puis') !== -1 && stor.Lexo.donnee !== 'puis') {
      yaq = true
      faisPuis()
    }
    if (stor.ModeRep.indexOf('symb') !== -1 && stor.Lexo.donnee !== 'symb') {
      yaq = true
      faisSymb()
    }
    if (stor.ModeRep.indexOf('pref') !== -1 && stor.Lexo.donnee !== 'pref') {
      yaq = true
      faisPref()
    }
    if (stor.ModeRep.indexOf('sin') !== -1 && stor.Lexo.donnee !== 'sin') {
      yaq = true
      faisSin()
    }
    if (!yaq) {
      if (stor.Lexo.donnee === 'pef') {
        faisPuis()
      } else {
        faisPref()
      }
    }
  }

  function creeLesDiv () {
    stor.lesdiv = {}
    stor.lesdiv.conteneur = j3pAddElt(me.zonesElts.MG, 'div', {
      style: me.styles.etendre('toutpetit.enonce', { padding: '10px' })
    })
    stor.lesdiv.consigne = addDefaultTable(stor.lesdiv.conteneur, 1, 1)[0][0]
    const tabJK = addDefaultTable(stor.lesdiv.conteneur, 4, 1)

    const tabPy = addDefaultTable(tabJK[1][0], 3, 1)
    stor.lesdiv.travPuis = addDefaultTable(tabPy[0][0], 1, 1)[0][0]
    const tabPyZ = addDefaultTable(stor.lesdiv.travPuis, 1, 3)
    stor.lesdiv.travPuis1 = tabPyZ[0][0]
    stor.lesdiv.travPuis2 = tabPyZ[0][1]
    stor.lesdiv.travPuis3 = tabPyZ[0][2]
    stor.lesdiv.expliPuis = tabPy[1][0]
    stor.lesdiv.solPuis = tabPy[2][0]
    stor.lesdiv.expliPuis.style.color = me.styles.cfaux
    stor.lesdiv.solPuis.style.color = me.styles.toutpetit.correction.color

    const tabsy = addDefaultTable(tabJK[3][0], 3, 1)
    stor.lesdiv.travSymb = addDefaultTable(tabJK[0][0], 1, 1)[0][0]
    const tabsyZ = addDefaultTable(stor.lesdiv.travSymb, 1, 2)
    stor.lesdiv.travSymb1 = tabsyZ[0][0]
    stor.lesdiv.travSymb2 = tabsyZ[0][1]
    stor.lesdiv.expliPSymb = tabsy[1][0]
    stor.lesdiv.solSymb = tabsy[2][0]
    stor.lesdiv.expliPSymb.style.color = me.styles.cfaux
    stor.lesdiv.solSymb.style.color = me.styles.toutpetit.correction.color

    const tabp = addDefaultTable(tabJK[2][0], 3, 1)
    stor.lesdiv.travPref = addDefaultTable(tabp[0][0], 1, 1)[0][0]
    const tabpZ = addDefaultTable(stor.lesdiv.travPref, 1, 2)
    stor.lesdiv.travpref1 = tabpZ[0][0]
    stor.lesdiv.travpref2 = tabpZ[0][1]
    stor.lesdiv.expliPref = tabp[1][0]
    stor.lesdiv.solPref = tabp[2][0]
    stor.lesdiv.expliPref.style.color = me.styles.cfaux
    stor.lesdiv.solPref.style.color = me.styles.toutpetit.correction.color

    const tabs = addDefaultTable(tabJK[0][0], 3, 1)
    stor.lesdiv.travSin = addDefaultTable(tabs[0][0], 1, 1)[0][0]
    const tabsZ = addDefaultTable(stor.lesdiv.travSin, 1, 3)
    stor.lesdiv.travsin1 = tabsZ[0][0]
    stor.lesdiv.travsin2 = tabsZ[0][1]
    stor.lesdiv.travsin3 = tabsZ[0][2]
    stor.lesdiv.expliSin = tabs[1][0]
    stor.lesdiv.solSin = tabs[2][0]
    stor.lesdiv.expliSin.style.color = me.styles.cfaux
    stor.lesdiv.solSin.style.color = me.styles.toutpetit.correction.color
    stor.lesdiv.correction = j3pAddElt(me.zonesElts.MD, 'div', {
      style: me.styles.etendre('toutpetit.enonce', { padding: '10px' })
    })
  }

  function faisPuis () {
    stor.yapuis = true
    j3pAffiche(stor.lesdiv.travPuis1, null, '$N = ' + stor.lenb + ' \\times $&nbsp;')
    const t = affichepuis(stor.lesdiv.travPuis2)
    j3pAffiche(t[0], null, '$10$')
    j3pAffiche(stor.lesdiv.travPuis3, null, '&nbsp; ' + stor.Lexo.unite)
    stor.zonePuis = new ZoneStyleMathquill3(t[2], { restric: stor.restric, enter: me.sectionCourante.bind(me) })
  }
  function faisSymb () {
    stor.yasymb = true
    j3pAffiche(stor.lesdiv.travSymb1, null, '$N = ' + stor.lenb + '$&nbsp;')
    let ll = ['G' + stor.Lexo.unite]
    ll.push('M' + stor.Lexo.unite)
    ll.push('k' + stor.Lexo.unite)
    ll.push('m' + stor.Lexo.unite)
    ll.push('n' + stor.Lexo.unite)
    ll.push('μ' + stor.Lexo.unite)
    ll = j3pShuffle(ll)
    ll.splice(0, 0, 'Choisir')
    stor.zoneSymb = ListeDeroulante.create(stor.lesdiv.travSymb2, ll, { centre: true })
  }
  function faisPref () {
    stor.yapref = true
    j3pAffiche(stor.lesdiv.travpref1, null, '$N = ' + stor.lenb + '$&nbsp;')
    let ll = ['giga' + sinSin1(stor.Lexo.unite)]
    ll.push('méga' + sinSin1(stor.Lexo.unite))
    ll.push('kilo' + sinSin1(stor.Lexo.unite))
    ll.push('milli' + sinSin1(stor.Lexo.unite))
    ll.push('nano' + sinSin1(stor.Lexo.unite))
    ll.push('micro' + sinSin1(stor.Lexo.unite))
    ll = j3pShuffle(ll)
    ll.splice(0, 0, 'Choisir')
    stor.zonePref = ListeDeroulante.create(stor.lesdiv.travpref2, ll, { centre: true })
  }
  function faisSin () {
    stor.yasin = true
    j3pAffiche(stor.lesdiv.travsin1, null, '$N$ est égal' + sinSin4(stor.Lexo.unite) + ' à $' + stor.lenb + '$&nbsp;')
    let ll = ['milliards']
    ll.push('millions')
    ll.push('milliers')
    ll.push('millièmes')
    ll.push('millionièmes')
    ll.push('milliardièmes')
    ll = j3pShuffle(ll)
    ll.splice(0, 0, 'Choisir')
    stor.zoneSin = ListeDeroulante.create(stor.lesdiv.travsin2, ll, { centre: true })
    ll = [' de grammes.']
    ll.push(' de secondes.')
    ll.push(' de mètres.')
    ll.push(' de litres.')
    ll.push(' de watts.')
    ll.push(' d’octets.')
    ll = j3pShuffle(ll)
    ll.splice(0, 0, 'Choisir')
    stor.zoneSin2 = ListeDeroulante.create(stor.lesdiv.travsin3, ll, { centre: true })
  }

  function sinSin (k) {
    if (k === 'g') return ' de grammes.'
    if (k === 'm') return ' de mètres.'
    if (k === 's') return ' de secondes.'
    if (k === 'L') return ' de litres.'
    if (k === 'W') return ' de watts.'
    if (k === 'o') return ' d’octets.'
  }
  function sinSin1 (k) {
    if (k === 'g') return 'grammes'
    if (k === 'm') return 'mètres.'
    if (k === 's') return 'secondes'
    if (k === 'L') return 'litres'
    if (k === 'W') return 'watts'
    if (k === 'o') return 'octets'
  }
  function sinSin3 (k) {
    if (k === 'g') return ' la masse '
    if (k === 'm') return ' l’altitude '
    if (k === 's') return ' la durée '
    if (k === 'L') return ' la contenance '
    if (k === 'W') return ' le travail '
    if (k === 'o') return ' la capacité '
  }
  function sinSin4 (k) {
    if (k === 'g') return 'e '
    if (k === 'm') return 'e '
    if (k === 's') return 'e '
    if (k === 'L') return 'e '
    if (k === 'W') return ' '
    if (k === 'o') return 'e '
  }
  function boutonfen () {
    j3pToggleFenetres('Calculatrice')
  }

  function yaReponse () {
    if (me.isElapsed) return true
    if (stor.yapuis) {
      if (stor.zonePuis.reponse() === '') {
        stor.zonePuis.focus()
        return false
      }
    }
    if (stor.yasymb) {
      if (!stor.zoneSymb.changed) {
        stor.zoneSymb.focus()
        return false
      }
    }
    if (stor.yapref) {
      if (!stor.zonePref.changed) {
        stor.zonePref.focus()
        return false
      }
    }
    if (stor.yasin) {
      if (!stor.zoneSin.changed) {
        stor.zoneSin.focus()
        return false
      }
      if (!stor.zoneSin2.changed) {
        stor.zoneSin2.focus()
        return false
      }
    }
    return true
  }

  function isRepOk () {
    let r1, ok
    stor.errEcri = false
    stor.errPuis = false
    stor.errSymb = false
    stor.errPref = false
    stor.errSin = false
    stor.errSin2 = false
    stor.focopuis = stor.focosymb = stor.focopref = stor.focosin = false

    mettouvert()
    ok = true
    if (stor.yapuis) {
      r1 = stor.zonePuis.reponse()
      if (r1.indexOf('-') !== r1.lastIndexOf('-') || r1.indexOf('-') > 0) {
        stor.errEcri = true
        stor.zonePuis.corrige(false)
        ok = false
        stor.focopuis = true
      }
      if (ok) {
        if (parseFloat(r1) !== stor.puis) {
          stor.errPuis = true
          stor.zonePuis.corrige(false)
          ok = false
          stor.focopuis = true
        }
      }
    }
    if (stor.yasymb) {
      if (stor.zoneSymb.reponse !== stor.symb + stor.Lexo.unite) {
        stor.errSymb = true
        stor.zoneSymb.corrige(false)
        ok = false
        stor.focosymb = true
      }
    }
    if (stor.yapref) {
      if (stor.zonePref.reponse !== stor.nom + sinSin1(stor.Lexo.unite)) {
        stor.errPref = true
        stor.zonePref.corrige(false)
        ok = false
        stor.focopref = true
      }
    }
    if (stor.yasin) {
      if (stor.zoneSin.reponse !== stor.sin) {
        stor.errSin = true
        stor.zoneSin.corrige(false)
        ok = false
        stor.focosin = true
      }
      if (stor.zoneSin2.reponse !== sinSin(stor.Lexo.unite)) {
        stor.errSin2 = true
        stor.zoneSin2.corrige(false)
        ok = false
        stor.focosin = true
      }
    }
    return ok
  }

  function desactiveAll () {
    if (stor.yapuis) {
      stor.zonePuis.disable()
    }
    if (stor.yasymb) {
      stor.zoneSymb.disable()
    }
    if (stor.yapref) {
      stor.zonePref.disable()
    }
    if (stor.yasin) {
      stor.zoneSin.disable()
      stor.zoneSin2.disable()
    }
  }

  function barrelesfo () {
    if (stor.yapuis) {
      stor.zonePuis.barreIfKo()
    }
    if (stor.yasymb) {
      stor.zoneSymb.barreIfKo()
    }
    if (stor.yapref) {
      stor.zonePref.barreIfKo()
    }
    if (stor.yasin) {
      stor.zoneSin.barreIfKo()
      stor.zoneSin2.barreIfKo()
    }
  }

  function mettouvert () {
    if (stor.yapuis) {
      stor.zonePuis.corrige(true)
    }
    if (stor.yasymb) {
      stor.zoneSymb.corrige(true)
    }
    if (stor.yapref) {
      stor.zonePref.corrige(true)
    }
    if (stor.yasin) {
      stor.zoneSin.corrige(true)
      stor.zoneSin2.corrige(true)
    }
  }
  function AffCorrection (bool) {
    if (stor.errEcri) {
      stor.yaexplikPuis = true
      j3pAffiche(stor.lesdiv.expliPuis, null, 'Erreur d’écriture !\n')
    }
    if (stor.errSymb) {
      stor.yaexplikSymb = true
      j3pAffiche(stor.lesdiv.expliPSymb, null, 'Tu n’utilises pas le bon symbole !\n')
    }
    if (stor.errSin2) {
      stor.yaexplikSin = true
      j3pAffiche(stor.lesdiv.expliSin, null, 'Problème d’unité !\n')
    }

    if (bool) {
      if (stor.focopref) {
        j3pAffiche(stor.lesdiv.solPref, null, '$N = ' + stor.lenb + ' $ ' + stor.nom + sinSin1(stor.Lexo.unite))
      }
      if (stor.focopuis) {
        j3pAffiche(stor.lesdiv.solPuis, null, '$N = ' + stor.lenb + ' \\times 10^{' + stor.puis + '}$ ' + stor.Lexo.unite)
      }
      if (stor.focosymb) {
        j3pAffiche(stor.lesdiv.solSymb, null, '$N = ' + stor.lenb + ' $ ' + stor.symb + stor.Lexo.unite)
      }
      if (stor.focosin) {
        j3pAffiche(stor.lesdiv.solSin, null, '$N$ est égal' + sinSin4(stor.Lexo.unite) + ' à $' + stor.lenb + '$ ' + stor.sin + sinSin(stor.Lexo.unite))
      }
      barrelesfo()
      desactiveAll()
      stor.yaco = true
    } else { me.afficheBoutonValider() }
  }

  function enonceMain () {
    if (me.etapeCourante === 1) {
      me.videLesZones()
      creeLesDiv()
      stor.Lexo = faisChoixExos()
    }
    poseQuestion()
    me.cacheBoutonSuite()
    j3pEmpty(stor.lesdiv.calculatrice)
    if (ds.Calculatrice) {
      me.fenetresjq = [{ name: 'Calculatrice', title: 'Une calculatrice', left: 600, top: 10 }]
      j3pCreeFenetres(me)
      j3pAfficheCroixFenetres('Calculatrice')
      j3pAjouteBouton(stor.lesdiv.calculatrice, 'Calculatrice', 'MepBoutons', 'Calculatrice', boutonfen)
    }
    me.finEnonce()
    stor.lesdiv.consigne.classList.add('enonce')
    me.zonesElts.MG.classList.add('fond')
    if (stor.yapuis) {
      stor.lesdiv.travPuis.classList.add('travail')
    }
    if (stor.yasymb) {
      stor.lesdiv.travSymb.classList.add('travail')
    }
    if (stor.yapref) {
      stor.lesdiv.travPref.classList.add('travail')
    }
    if (stor.yasin) {
      stor.lesdiv.travSin.classList.add('travail')
    }
  }

  switch (this.etat) {
    case 'enonce':
      // code exécuté au lancement de la section
      if (me.debutDeLaSection) {
        initSection()
      } else {
        enonceMain()
      }
      break // case "enonce":

    case 'correction':
      // On teste si une réponse a été saisie
      stor.yaco = false
      stor.yaexplikPuis = stor.yaexplikSymb = stor.yaexplikSin = false
      stor.focopref = stor.focopuis = stor.focosymb = stor.focosin = false
      j3pEmpty(stor.lesdiv.expliPSymb)
      j3pEmpty(stor.lesdiv.expliPuis)
      j3pEmpty(stor.lesdiv.expliSin)
      stor.lesdiv.expliPSymb.classList.remove('explique')
      stor.lesdiv.expliPuis.classList.remove('explique')
      stor.lesdiv.expliSin.classList.remove('explique')

      if (yaReponse()) {
        // Bonne réponse
        if (isRepOk()) {
          this._stopTimer()
          stor.lesdiv.correction.style.color = this.styles.cbien
          stor.lesdiv.correction.innerHTML = cBien

          desactiveAll()
          mettouvert()

          this.cacheBoutonValider()
          this.score++

          this.typederreurs[0]++
          this.etat = 'navigation'
          this.sectionCourante()
        } else {
          // Pas de bonne réponse
          stor.lesdiv.correction.style.color = this.styles.cfaux
          stor.lesdiv.correction.innerHTML = cFaux
          if (this.isElapsed) {
            // A cause de la limite de temps :
            this._stopTimer()

            stor.lesdiv.correction.innerHTML += tempsDepasse + '<BR>'
            this.typederreurs[10]++
            this.cacheBoutonValider()

            AffCorrection(true)

            this.etat = 'navigation'
            this.sectionCourante()
          } else {
            // Réponse fausse :
            if (me.essaiCourant < me.donneesSection.nbchances) {
              stor.lesdiv.correction.innerHTML += '<br>' + essaieEncore

              AffCorrection(false)

              this.typederreurs[1]++
            } else {
              // Erreur au nème essai
              this._stopTimer()
              this.cacheBoutonValider()

              AffCorrection(true)

              if (stor.encours === 'p') {
                stor.pose = false
                stor.encours = 't'
              }

              this.typederreurs[2]++
              this.etat = 'navigation'
              this.sectionCourante()
            }
          }
        }
      } else {
        // pas de réponse
        stor.lesdiv.correction.style.color = this.styles.cfaux
        stor.lesdiv.correction.innerHTML = reponseIncomplete
        if (stor.encours === 'm') {
          stor.lesdiv.correction.innerHTML = 'Réponse incomplète ! <br> Tu dois déplacer le point ' + stor.C + ' .'
        }
        me.afficheBoutonValider()
      }

      if (stor.yaexplikPuis) stor.lesdiv.expliPuis.classList.add('explique')
      if (stor.yaexplikSymb) stor.lesdiv.expliPSymb.classList.add('explique')
      if (stor.yaexplikSin) stor.lesdiv.expliSin.classList.add('explique')
      if (stor.focopref && stor.yaco) stor.lesdiv.solPref.classList.add('correction')
      if (stor.focopuis && stor.yaco) stor.lesdiv.solPuis.classList.add('correction')
      if (stor.focosymb && stor.yaco) stor.lesdiv.solSymb.classList.add('correction')
      if (stor.focosin && stor.yaco) stor.lesdiv.solSin.classList.add('correction')

      // Obligatoire
      this.finCorrection()
      break // case "correction":

    case 'navigation':
      if (!this.sectionTerminee()) {
        this.etat = 'enonce'
      }
      this.finNavigation()
      break // case "navigation":
  }
}
