import { j3pAddElt, j3pAjouteBouton, j3pArrondi, j3pBarre, j3pClone, j3pDetruit, j3pEmpty, j3pGetRandomBool, j3pGetRandomInt, j3pModale, j3pPGCD, j3pShuffle } from 'src/legacy/core/functions'
import BoutonStyleMathquill from 'src/legacy/outils/boutonsStyleMatquill'
import ListeDeroulante from 'src/legacy/outils/listeDeroulante/ListeDeroulante'
import MenuContextuel from 'src/legacy/outils/menuContextuel'
import { j3pCreeFenetres, j3pAfficheCroixFenetres, j3pToggleFenetres } from 'src/legacy/core/functionsJqDialog'
import ZoneStyleMathquill1 from 'src/legacy/outils/zoneStyleMathquill/ZoneStyleMathquill1'
import { affichefois, afficheFrac, affichemoins, affichemoinsPar, afficheplus, afficheplusPar, barreZone } from 'src/legacy/outils/zoneStyleMathquill/functions'
import { getPe, initCptPe } from 'src/legacy/sections/college/helperTommy'
import { addDefaultTable } from 'src/legacy/themes/table'
import textesGeneriques from 'src/lib/core/textes'
import { j3pAffiche } from 'src/lib/mathquill/functions'

import '../../css/fractions.scss'
import '../../css/operations.scss'

const { tempsDepasse } = textesGeneriques
/**
 * Ajouter une description ici
 * @fileOverview
 * @author Tommy Barroy <t.barroy@laposte.net>
 * @since 2017-10
*/
export const params = {
  outils: ['mathquill', 'calculatrice'],
  /**
   * Ce qui est paramétrable dans l’éditeur de graphe
   * @type {LegacySectionParamsParametres}
   */
  parametres: [
    ['nbrepetitions', 3, 'entier', 'Nombre de répétitions de la section'],
    ['nbTentatives', 2, 'entier', 'Nombre de tentatives autorisées'],
    ['multiplication', true, 'boolean', '<u>true</u>:  L’exercice peut proposer des multiplications'],
    ['division', true, 'boolean', '<u>true</u>:  L’exercice peut proposer des divisions'],
    ['somme', true, 'boolean', '<u>true</u>:  L’exercice peut proposer des additions'],
    ['difference', true, 'boolean', '<u>true</u>:  L’exercice peut proposer des soustractions'],
    ['Positifs', false, 'boolean', '<u>true</u>: Les nombres sont forcément positifs.'],
    ['Nombre_operations', 3, 'entier', 'Nombre d’opérations (max 3).\n\n<i><b>Ignoré s’il n’y a qu’une opération possible (1 sera imposé dans ce cas)</b></i>'],
    ['Reduction', true, 'boolean', '<u>true</u>: L’élève doit réduire le résultat.'],
    ['Simpl_Mult', false, 'boolean', '<u>true</u>: Proposition de simplification en cours de multiplication.'],
    ['Entiers', true, 'boolean', '<u>true</u>: Il peut y avoir des nombres entiers.'],
    ['Detail_Entiers', true, 'boolean', '<u>true</u>: La transformation d’un entier en fraction est détaillée.'],
    ['Detail_Somme', true, 'boolean', '<u>true</u>: Le calcul d’une somme est détaillé.'],
    ['Detail_Reduction', true, 'boolean', '<u>true</u>: Les opérations pour réduire au même dénominateur sont détaillées.'],
    ['Detail_Produit', true, 'boolean', '<u>true</u>: Le calcul d’un produit est détaillé.'],
    ['Detail_Simplification', true, 'boolean', '<u>true</u>: La simplification est détaillée.'],
    ['Detail_Signe', true, 'boolean', '<u>true</u>: La simplification des signes est détaillée.'],
    ['Priorite', false, 'boolean', '<u>true</u>: Annule la quasi-totalité des autres paramètres pour ne faire qu’un exercice de priorité opératoire.'],
    ['Multiples', false, 'boolean', '<u>true</u>: Dénominateurs multiples pour les sommes et différences, simplification évidente pour les produits.'],
    ['MemeDeno', false, 'boolean', '<u>true</u>: Les sommes et différences proposent le même dénominateur.'],
    // le modèle gère la compatibilité ascendante avec l’ancien paramètre MiseEnPagePlusClaire
    ['theme', 'standard', 'liste', 'Modèle pour l’affichage', ['standard', 'zonesAvecImageDeFond']]
  ],
  // dans le cas d’une section QUANTITATIVE
  pe: [
    { pe_1: 'Priorité' },
    { pe_2: 'Somme' },
    { pe_3: 'Produit' },
    { pe_4: 'Quotient' },
    { pe_5: 'Irréductible_Reconnaissance' },
    { pe_6: 'Entier' },
    { pe_7: 'Signes' },
    { pe_8: 'Calculs' }
  ]
}

/**
 * Récupère nbchances s'il existe pour initialiser nbTentatives
 * @param {LegacySectionParamsParametres} parametres ceux du graphe
 */
export function upgradeParametres (parametres) {
  // on avait un param nbchances qui était récupéré puis forcé à 1, on l'a remplacé par un param nbTentatives
  if (parametres.nbchances) {
    parametres.nbTentatives = parametres.nbchances
    delete parametres.nbchances
  }
}

/**
 * section fractioncalculs
 * @this {Parcours}
 */
export default function main () {
// un préfix css, on lui donne le nom de la section
  const me = this
  const stor = me.storage
  const ds = me.donneesSection

  const pgcd = (a, b) => j3pPGCD(a, b, { returnOtherIfZero: true })

  function addRow (elmt) {
    // pourquoi cette ligne de plus si y’a des négatifs ?
    if (!ds.Positifs) {
      j3pDetruit(stor.trow)
      stor.trow = j3pAddElt(elmt, 'tr', '', { style: { height: '35px' } })
      j3pAddElt(stor.trow, 'td')
      j3pAddElt(stor.trow, 'td')
    }
    // on ajoute une ligne
    const tr = j3pAddElt(elmt, 'tr', '', { style: { height: '35px' } })
    // cellule pour le bouton calculatrice, on vire le précédent (ne fait rien s’il n’existait pas encore)
    j3pEmpty(stor.lesdiv.calculatrice)
    stor.lesdiv.calculatrice = j3pAddElt(tr, 'td')
    j3pAjouteBouton(stor.lesdiv.calculatrice, boutonfen, { value: 'Calculatrice', className: 'MepBoutons' })
    // td pour écrire
    const td2 = j3pAddElt(tr, 'td', '')
    stor.lesdiv.travailEnCours = td2
    stor.comptediv++
    td2.classList.add('travail')
  }

  function colle (qui, quoi) {
    // quoi c’est un tab avec les differents elements possibles a coller
    // qui c’est un chaine
    let i
    const tabret = []
    for (i = 0; i < quoi.length; i++) {
      tabret.push(qui + quoi[i])
    }
    return tabret
    // returne un tab avec lachaine + les differents element
  }
  function initChoices () {
    // la liste des choix possibles
    stor.choicesAdd = {
      m1: {
        name: 'op1col',
        label: 'Réduire au même dénominateur',
        callback: function (/* menu, choice, event */) {
          gerechoixad('r')
        }
      },
      m2: {
        name: 'aj2col',
        label: 'Ajouter en haut et en bas',
        callback: function () {
          gerechoixad('a')
        }
      },
      m3: {
        name: 'sub2col',
        label: 'Garder le dénominateur puis ajouter',
        callback: function () {
          gerechoixad('g')
        }
      },
      s: {
        name: 'sss1',
        label: 'Simplifier d’abord une des deux fractions',
        callback: geresimple
      }
    }
    stor.choicesTransAd = {
      m1: {
        name: 'op1col',
        label: 'Réduire au même dénominateur',
        callback: function () {
          gerechoixad('rs')
        }
      },
      m2: {
        name: 'aj2col',
        label: 'Ajouter en haut et en bas',
        callback: function () {
          gerechoixad('a')
        }
      },
      m3: {
        name: 'sub2col',
        label: 'Garder le dénominateur puis ajouter',
        callback: function () {
          gerechoixad('g')
        }
      },
      s: {
        name: 'sss1',
        label: 'Simplifier d’abord une des deux fractions',
        callback: geresimple
      }
    }
    stor.choicesMoins = {
      m1: {
        name: 'M1col',
        label: 'Réduire au même dénominateur',
        callback: function () {
          gerechoixmoins('r')
        }
      },
      m2: {
        name: 'M2col',
        label: 'Soustraire en haut et en bas',
        callback: function () {
          gerechoixmoins('a')
        }
      },
      m3: {
        name: 'M3col',
        label: 'Garder le dénominateur puis soustraire',
        callback: function () {
          gerechoixmoins('g')
        }
      },
      s: {
        name: 'sss1',
        label: 'Simplifier d’abord une des deux fractions',
        callback: geresimple
      }
    }
    stor.choicesTransMoins = {
      m1: {
        name: 'M1col',
        label: 'Réduire au même dénominateur',
        callback: function () {
          gerechoixmoins('rs')
        }
      },
      m2: {
        name: 'M2col',
        label: 'Soustraire en haut et en bas',
        callback: function () {
          gerechoixmoins('a')
        }
      },
      m3: {
        name: 'M3col',
        label: 'Garder le dénominateur puis soustraire',
        callback: function () {
          gerechoixmoins('g')
        }
      },
      s: {
        name: 'sss1',
        label: 'Simplifier d’abord une des deux fractions',
        callback: geresimple
      }
    }
    stor.choicesFois = {
      m1: {
        name: 'F1col',
        label: 'Réduire au même dénominateur',
        callback: function () {
          gerechoixfois('1')
        }
      },
      m2: {
        name: 'F2col',
        label: 'Multiplier en haut et en bas',
        callback: function () {
          gerechoixfois('2')
        }
      },
      m3: {
        name: 'F3col',
        label: 'Multiplier par l’inverse',
        callback: function () {
          gerechoixfois('3')
        }
      },
      s: {
        name: 'sss1',
        label: 'Simplifier d’abord une des deux fractions',
        callback: geresimple
      }
    }
    stor.choicesDiv = {
      m1: {
        name: 'D1col',
        label: 'Réduire au même dénominateur',
        callback: function () {
          gerechoixdiv('1')
        }
      },
      m2: {
        name: 'D2col',
        label: 'Diviser en haut et en bas',
        callback: function () {
          gerechoixdiv('2')
        }
      },
      m3: {
        name: 'D3col',
        label: 'Multiplier par l’inverse',
        callback: function () {
          gerechoixdiv('3')
        }
      },
      s: {
        name: 'sss1',
        label: 'Simplifier d’abord une des deux fractions',
        callback: geresimple
      }
    }
    stor.choicesTransFois = {
      m1: {
        name: 'F1col',
        label: 'Multiplier en haut par £a',
        callback: function () {
          gerechoixfois('0')
        }
      },
      m2: {
        name: 'F2col',
        label: 'Multiplier en bas par £a',
        callback: function () {
          gerechoixfois('4')
        }
      },
      m3: {
        name: 'F3col',
        label: 'Multiplier en haut et en bas par £a',
        callback: function () {
          gerechoixfois('4')
        }
      },
      s: {
        name: 'sss1',
        label: 'Simplifier d’abord la fraction',
        callback: geresimple
      }
    }
  }
  function lanceFunc (e) {
    let i
    let z
    for (i = 0; i < stor.listFuncSimpl.length; i++) {
      z = stor.listFuncSimpl[i]
      if (z.id === e) {
        z.fonc(z.n1, z.n2)
        return
      }
    }
    console.error('Pas trouvé la fonction ' + e)
  }
  function newMenuSimpl (t, pl) {
    let i
    let e
    const men = []
    for (i = 0; i < t.length; i++) {
      e = newIdFunc()
      stor.listFuncSimpl.push({ pl, id: e, n1: t[i].f1, n2: t[i].f2, fonc: rempSimpl })
      men.push({
        name: 'men' + i,
        label: 'Remplacer par ' + t[i].f1 + 'x' + t[i].f2,
        callback: function () {
          lanceFunc(e)
        }
      })
    }
    const f = newIdFunc()
    stor.listFuncSimpl.push({ pl, id: f, n1: 0, n2: 0, fonc: barreSimpl })
    men.push({
      name: 'men' + i,
      label: 'Barrer ce facteur',
      callback: function () {
        lanceFunc(f)
      }
    })
    return men
  }
  function rempSimpl (a, b) {
    let i
    let cnum
    for (i = 0; i < stor.lesnumsimpl.length; i++) {
      if (stor.lesnumsimpl[i].pl === this.pl) {
        stor.lesnumsimpl.splice(i, 1)
        cnum = true
        break
      }
    }
    for (i = 0; i < stor.lesdensimpl.length; i++) {
      if (stor.lesdensimpl[i].pl === this.pl) {
        stor.lesdensimpl.splice(i, 1)
        cnum = false
        break
      }
    }
    let na
    let nb
    DetruitMenuSimple(this.pl)
    if (a < 0) { na = '(' + a + ')' } else { na = a }
    if (b < 0) { nb = '(' + b + ')' } else { nb = b }
    j3pEmpty(this.pl)
    const t = affichefois2(this.pl)
    j3pAffiche(t[0], null, '$' + na + '$')
    j3pAffiche(t[2], null, '$' + nb + '$')
    CreeMenuSimpl([{ val: a, pl: t[0] }, { val: b, pl: t[2] }])
    if (cnum) {
      stor.lesnumsimpl.push({ val: a, pl: t[0] })
      stor.lesnumsimpl.push({ val: b, pl: t[2] })
    } else {
      stor.lesdensimpl.push({ val: a, pl: t[0] })
      stor.lesdensimpl.push({ val: b, pl: t[2] })
    }
  }
  function barreSimpl () {
    let cnum
    let i
    let cb
    let ok = false
    let abarre
    let isov
    for (i = 0; i < stor.lesnumsimpl.length; i++) {
      if (stor.lesnumsimpl[i].pl === this.pl) {
        cb = stor.lesnumsimpl[i].val
        isov = i
        cnum = true
        break
      }
    }
    for (i = 0; i < stor.lesdensimpl.length; i++) {
      if (stor.lesdensimpl[i].pl === this.pl) {
        cb = stor.lesdensimpl[i].val
        isov = i
        cnum = false
        break
      }
    }
    if (cnum) {
      for (i = 0; i < stor.lesdensimpl.length; i++) {
        if (Math.abs(stor.lesdensimpl[i].val) === Math.abs(cb)) {
          ok = true
          abarre = stor.lesdensimpl[i].pl
          stor.lesdensimpl.splice(i, 1)
          stor.lesnumsimpl.splice(isov, 1)
          break
        }
      }
    } else {
      for (i = 0; i < stor.lesnumsimpl.length; i++) {
        if (Math.abs(stor.lesnumsimpl[i].val) === Math.abs(cb)) {
          ok = true
          abarre = stor.lesnumsimpl[i].pl
          stor.lesdensimpl.splice(isov, 1)
          stor.lesnumsimpl.splice(i, 1)
          break
        }
      }
    }
    if (ok) {
      barreZone(this.pl)
      barreZone(abarre)
      DetruitMenuSimple(this.pl)
      DetruitMenuSimple(abarre)
      stor.cons2 = stor.cons2 * cb
    } else {
      let texte = 'Je ne trouve pas ce facteur au '
      if (cnum) { texte += 'dénominateur !' } else { texte += 'numérateur !' }
      const yy = j3pModale({ titre: 'Information', contenu: '' })
      j3pAffiche(yy, null, texte)
      /// /PB ICI
      yy.style.color = me.styles.toutpetit.correction.color
    }
  }
  function affModale (texte) {
    let bufchance
    if (stor.nbtente > 1) {
      bufchance = 'chances.'
    } else {
      bufchance = 'chance.'
    }
    texte += ' <br> Il te reste ' + stor.nbtente + ' ' + bufchance
    const yy = j3pModale({ titre: 'Erreur', contenu: '' })
    j3pAffiche(yy, null, texte)
    yy.style.color = me.styles.toutpetit.correction.color
  }
  function affModaleInfo (texte) {
    const yy = j3pModale({ titre: 'Information', contenu: '' })
    j3pAffiche(yy, null, texte)
    yy.style.color = me.styles.toutpetit.correction.color
  }
  function affModaleSimpl1 () {
    j3pModale({ titre: 'Information', contenu: '' })
    const lam = addDefaultTable('modale', 2, 3)
    modif(lam)
    j3pAffiche(lam[0][1], null, 'Souhaites-tu décomposer des facteurs \npour simplifier ton calcul dès maintenant ?')
    j3pAjouteBouton(lam[1][0], boutonSimplOui, { className: 'MepBoutons', value: 'Oui' })
    j3pAjouteBouton(lam[1][2], boutonSimplNon, { className: 'MepBoutons', value: 'Non' })
  }
  function boutonSimplOui () {
    j3pDetruit('modale')
    j3pDetruit('j3pmasque')
    stor.listFuncSimpl = []
    stor.lesnumsimpl = [{ pl: stor.op[0].pl, val: parseFloat2(stor.zone[0].reponse()) }, { pl: stor.op[1].pl, val: parseFloat2(stor.zone[1].reponse()) }]
    if (stor.etat === 'x') {
      stor.lesdensimpl = [{ pl: stor.op[2].pl, val: parseFloat2(stor.zone[2].reponse()) }, { pl: stor.op[3].pl, val: parseFloat2(stor.zone[3].reponse()) }]
    } else {
      stor.lesdensimpl = [{ pl: stor.op[2].pl, val: parseFloat2(stor.zone[2].reponse()) }]
    }
    desactivezone(true)
    stor.listemenussimple = []
    CreeMenuSimpl(stor.lesnumsimpl)
    CreeMenuSimpl(stor.lesdensimpl)
    stor.etat = 'SIMPLFOIS'
  }
  function CreeMenuSimpl (t) {
    let i
    let fl = []
    let lm
    for (i = 0; i < t.length; i++) {
      fl = lesdecompode(t[i].val)
      lm = newMenuSimpl(fl, t[i].pl)
      stor.listemenussimple[stor.listemenussimple.length] = new MenuContextuel(t[i].pl, lm)
    }
  }
  function DetruitMenuSimple (s) {
    let i
    for (i = 0; i < stor.listemenussimple.length; i++) {
      if (s === stor.listemenussimple[i].trigger) {
        stor.listemenussimple[i].destroy()
        stor.listemenussimple.splice(i, 1)
        return
      }
    }
  }
  function lesdecompode (x) {
    const ret = []
    let i
    for (i = 2; i <= Math.sqrt(Math.abs(x)); i++) {
      if (Math.abs(x) % i === 0) {
        ret.push({ f1: i, f2: x / i })
      }
    }
    return ret
  }
  function boutonSimplNon () {
    j3pDetruit('modale')
    j3pDetruit('j3pmasque')
    poseLastquest()
  }
  function desactivezone (b) {
    let i
    let x
    let id
    if (!b) {
      for (i = 0; i < stor.zone.length; i++) {
        stor.zone[i].disable()
        stor.zone[i].corrige(b)
      }
    } else {
      for (i = 0; i < stor.zone.length; i++) {
        x = stor.zone[i].reponse()
        id = stor.zone[i].conteneur
        stor.zone[i].disable()
        id.innerHTML = ''
        id.style.color = me.styles.cbien
        j3pAffiche(id, null, '$' + x + '$')
      }
    }
    stor.zone = []
    resize()
  }
  function melange (s) {
    const ret = []
    const ch = j3pGetRandomInt(0, 5)
    switch (ch) {
      case 0:
        ret.push(Cop(s.m1))
        ret.push(Cop(s.m2))
        ret.push(Cop(s.m3))
        ret.push(Cop(s.s))
        break
      case 1:
        ret.push(Cop(s.m1))
        ret.push(Cop(s.m3))
        ret.push(Cop(s.m2))
        ret.push(Cop(s.s))
        break
      case 2:
        ret.push(Cop(s.m2))
        ret.push(Cop(s.m1))
        ret.push(Cop(s.m3))
        ret.push(Cop(s.s))
        break
      case 3:
        ret.push(Cop(s.m2))
        ret.push(Cop(s.m3))
        ret.push(Cop(s.m1))
        ret.push(Cop(s.s))
        break
      case 4:
        ret.push(Cop(s.m3))
        ret.push(Cop(s.m1))
        ret.push(Cop(s.m2))
        ret.push(Cop(s.s))
        break
      case 5:
        ret.push(Cop(s.m3))
        ret.push(Cop(s.m2))
        ret.push(Cop(s.m1))
        ret.push(Cop(s.s))
        break
    }
    return ret
  }
  function Cop (t) {
    const ret = {}
    ret.name = t.name
    ret.label = t.label
    ret.callback = t.callback
    return ret
  }
  function parseFloat2 (s) {
    let yaparf = false
    let yaparo = false
    let cop = s
    if (cop.indexOf(')') !== -1) {
      yaparf = true
      if (cop.indexOf(')') !== cop.length - 1) {
        return 'NaN'
      }
      cop = cop.replace(')', '')
    }
    cop = s
    if (cop.indexOf('(') !== -1) {
      yaparo = true
      if (cop.indexOf('(') !== 0) {
        return 'NaN'
      }
      cop = cop.replace('(', '')
    }
    if (yaparo !== yaparf) return 'NaN'
    return parseFloat(cop)
  }
  function notsimpli (q) {
    if (q.den === 'z') return false
    return pgcd(Math.abs(q.num), Math.abs(q.den)) !== 1
  }
  function boutonfen () {
    j3pToggleFenetres('Calculatrice')
  }
  function newIdFunc () {
    if (stor.listIdFunc === undefined) {
      stor.listIdFunc = 0
    }
    stor.listIdFunc++
    return 'IDDUNEFUNC' + stor.listIdFunc
  }
  function viretouslesmenus () {
    let i
    for (i = stor.listemenussimple.length - 1; i > -1; i--) {
      stor.listemenussimple[i].destroy()
      stor.listemenussimple.pop()
    }
  }

  function creeLesDiv () {
    stor.lesdiv = {}
    // attention, faut cloner sinon l’ajout du postion relative ensuite va modifier l’objet global me.styles, donc y compris pour toutes les autres sections qui suivront
    const style = j3pClone(me.styles.toutpetit.enonce)
    style.position = 'relative'
    const props = { style }
    stor.lesdiv.conteneur = j3pAddElt(me.zonesElts.MG, 'div', '', props)
    if (ds.Priorite) {
      stor.lesdiv.consigneG = j3pAddElt(stor.lesdiv.conteneur, 'div', '', props)
    }
    // @todo construction html à simplifier, les tables dans des tables c'était au XXe siècle…
    const div1 = j3pAddElt(stor.lesdiv.conteneur, 'div', '', props)
    const propsTab = { style: { margin: 0, padding: 0, borderCollapse: 'collapse' } }
    const tabConteneur = j3pAddElt(div1, 'table', '', propsTab)
    let tr = j3pAddElt(tabConteneur, 'tr')
    const case2 = j3pAddElt(tr, 'td')
    stor.lesdiv.correction = j3pAddElt(tr, 'td')
    // dans la case de gauche du tabConteneur on met consigne & travail
    stor.lesdiv.tablo = j3pAddElt(case2, 'table')
    tr = j3pAddElt(stor.lesdiv.tablo, 'tr')
    stor.lesdiv.consigne = j3pAddElt(tr, 'td')
    stor.lesdiv.travail = j3pAddElt(tr, 'td')
    // dessous les étapes
    const styleEtape = j3pClone(me.styles.toutpetit.enonce)
    styleEtape.color = '#0000FF'
    stor.lesdiv.etape = j3pAddElt(stor.lesdiv.conteneur, 'div', '', { style: styleEtape })
    stor.lesdiv.travailz2 = j3pAddElt(stor.lesdiv.conteneur, 'div', '', props)
    const styleExplications = j3pClone(me.styles.toutpetit.enonce)
    styleExplications.color = me.styles.cfaux
    stor.lesdiv.explications = j3pAddElt(stor.lesdiv.conteneur, 'div', '', { style: styleExplications })
    const styleSolution = j3pClone(me.styles.toutpetit.enonce)
    styleSolution.color = me.styles.petit.correction.color
    stor.lesdiv.solution = j3pAddElt(stor.lesdiv.conteneur, 'div', '', { style: styleSolution })
    stor.lesdiv.boutons = j3pAddElt(stor.lesdiv.conteneur, 'div', '', { style: styleSolution })
    // lui est un div qui change en cours de route
    stor.lesdiv.travailEnCours = null
    me.zonesElts.MG.classList.add('fond')
  }
  function faisChoixExos () {
    const e = ds.lesExos.pop()
    let i
    let num
    let den
    stor.zone = []
    stor.nbtente = ds.nbTentatives
    e.frac = []
    for (i = 0; i < ds.Nombre_operations + 1; i++) {
      num = j3pGetRandomInt(2, 9)
      if ((!ds.Positifs) && (j3pGetRandomBool())) {
        num = -num
      }
      do {
        den = j3pGetRandomInt(2, 9)
      } while (pgcd(den, Math.abs(num)) !== 1)
      e.frac[i] = { num, den }
    }
    if (stor.yakR) {
      e.prop = ''
      const iop = [2, 3, 4, 5, 6, 7, 8, 9, 10, 11, 12, 14, 15, 16, 18, 20, 21, 22, 24, 25, 27, 28, 30, 35, 40, 45, 50, 55, 60, 70, 70, 90, 100, 200, 300, 400, 500, 600, 700, 800, 900]
      const max = iop.length - 1
      const indice = max > 0 ? j3pGetRandomInt(0, max) : 0
      const mul = iop[indice]
      e.frac[0].num = mul * e.frac[0].num
      e.frac[0].den = mul * e.frac[0].den
    } else {
      stor.forcebool = [false, false, false, false, false]
      if (!ds.Multiples && e.Entiers && j3pGetRandomInt(0, 2) > 0) {
        const ipos = []
        for (i = 0; i < e.prop.length; i++) {
          if ((e.prop[i] !== '+') && (e.prop[i] !== '-')) ipos.push(i)
        }
        if (ipos.length > 0) {
          const max = ipos.length - 1
          const zu = max > 0 ? j3pGetRandomInt(0, max) : 0

          e.frac[ipos[zu] + 1].den = 'z'
          stor.forcebool[zu] = true
        }
      }
    }
    if (ds.Multiples || ds.MemeDeno) {
      let ebuf = { num: e.frac[0].num, den: e.frac[0].den }
      let p
      for (i = 0; i < e.prop.length; i++) {
        switch (e.prop[i]) {
          case '+':
            e.frac[i + 1].den = j3pGetRandomInt(2, 5) * ebuf.den
            if (ds.MemeDeno) {
              e.frac[i + 1].den = ebuf.den
              if (e.frac[i + 1].den === 1) e.frac[i + 1].num = Math.abs(e.frac[i + 1].num)
            }
            num = ebuf.num * e.frac[i + 1].den + ebuf.den * e.frac[i + 1].num
            den = ebuf.den * e.frac[i + 1].den
            if (den < 0) { den = -den; num = -num }
            p = pgcd(Math.abs(num), Math.abs(den))
            ebuf = { num: Math.round(num / p), den: Math.round(den / p) }
            if (e.frac[i + 1].den === 1) e.frac[i + 1].den = 'z'
            break
          case '-':
            e.frac[i + 1].den = j3pGetRandomInt(2, 5) * ebuf.den
            if (ds.MemeDeno) {
              e.frac[i + 1].den = ebuf.den
              if (e.frac[i + 1].den === 1) e.frac[i + 1].num = Math.abs(e.frac[i + 1].num)
            }
            if (stor.forcebool[i]) {
              num = ebuf.num * e.frac[i + 1].den - ebuf.den * e.frac[i + 1].num
            } else {
              num = ebuf.den * e.frac[i + 1].num - ebuf.num * e.frac[i + 1].den
            }
            den = ebuf.den * e.frac[i + 1].den
            if (den < 0) { den = -den; num = -num }
            p = pgcd(Math.abs(num), Math.abs(den))
            ebuf = { num: Math.round(num / p), den: Math.round(den / p) }
            if (e.frac[i + 1].den === 1) e.frac[i + 1].den = 'z'
            break
          case 'x':
            if (j3pGetRandomBool()) {
              e.frac[i + 1].num = ebuf.den
            } else {
              e.frac[i + 1].den = ebuf.num
            }
            num = ebuf.num * e.frac[i + 1].num
            den = ebuf.den * e.frac[i + 1].den
            if (den < 0) { den = -den; num = -num }
            p = pgcd(Math.abs(num), Math.abs(den))
            ebuf = { num: Math.round(num / p), den: Math.round(den / p) }
            break
          case '/':
            if (j3pGetRandomBool()) {
              e.frac[i + 1].num = ebuf.num
            } else {
              e.frac[i + 1].den = ebuf.den
            }
            if (stor.forcebool[i]) {
              num = ebuf.num * e.frac[i + 1].den
              den = ebuf.den * e.frac[i + 1].num
            } else {
              num = ebuf.den * e.frac[i + 1].num
              den = ebuf.num * e.frac[i + 1].den
            }
            if (den < 0) { den = -den; num = -num }
            p = pgcd(Math.abs(num), Math.abs(den))
            ebuf = { num: Math.round(num / p), den: Math.round(den / p) }
            break
        }
      }
    }
    let unbool
    stor.listebool = []
    stor.listepar = []
    let fbuf
    const ebuf = { num: e.frac[0].num, den: e.frac[0].den }
    if (ebuf.den === 'z') e.buf.den = 1
    const fop = ds.difference && ds.Positifs
    for (i = 0; i < e.prop.length; i++) {
      fbuf = { num: e.frac[i + 1].num, den: e.frac[i + 1].den }
      if (fbuf.den === 'z') fbuf.den = 1
      switch (e.prop[i]) {
        case '+':
          ebuf.num = ebuf.num * fbuf.den + ebuf.den * fbuf.den
          ebuf.den = ebuf.den * fbuf.den
          stor.listebool.push(j3pGetRandomBool())
          if (!stor.listebool[i] && i > 0) {
            if ('+-'.indexOf(e.prop[i - 1]) !== -1) {
              stor.listepar[i - 1] = true
            }
          }
          if (i < e.prop.length - 1) {
            if (e.prop[i + 1] === 'x') {
              stor.listepar.push(true)
            } else {
              stor.listepar.push(false)
            }
          } else {
            stor.listepar.push(false)
          }
          break
        case '-':
          stor.listebool.push(j3pGetRandomBool())
          if (fop) {
            stor.listebool[stor.listebool.length - 1] = (ebuf.num / ebuf.den > fbuf.num / fbuf.den)
          }
          if (stor.listebool[stor.listebool.length - 1]) {
            ebuf.num = ebuf.num * fbuf.den - ebuf.den * fbuf.num
            ebuf.den = ebuf.den * fbuf.den
          } else {
            ebuf.num = ebuf.den * fbuf.num - ebuf.num * fbuf.den
            ebuf.den = ebuf.den * fbuf.den
          }
          if (!stor.listebool[i] && i > 0) {
            if ('+-'.indexOf(e.prop[i - 1]) !== -1) {
              stor.listepar[i - 1] = true
            }
          }
          if (i < e.prop.length - 1) {
            if (e.prop[i + 1] === 'x') {
              stor.listepar.push(true)
            } else {
              stor.listepar.push(false)
            }
          } else {
            stor.listepar.push(false)
          }
          break
        case 'x':
          ebuf.num = ebuf.num * fbuf.num
          ebuf.den = ebuf.den * fbuf.den
          stor.listepar.push(false)
          unbool = j3pGetRandomBool()
          if (i > 0) {
            if (e.prop[i - 1] === 'x') {
              unbool = true
            }
          }
          stor.listebool.push(unbool)
          break
        case '/':
          stor.listepar.push(false)
          stor.listebool.push(j3pGetRandomBool())
          if (stor.listebool[stor.listebool.length - 1]) {
            ebuf.num = ebuf.num * fbuf.den
            ebuf.den = ebuf.den * fbuf.num
          } else {
            ebuf.num = ebuf.den * fbuf.num
            ebuf.den = ebuf.num * fbuf.den
          }
          break
      }
      if (stor.forcebool[i]) stor.listebool[i] = true
    }
    // e.frac[0] = {num:5,den:7}
    // e.frac[1] = {num:3,den:5}
    // e.prop = '+'
    return e
  }
  function yaReponse () {
    if (stor.zzzSigne) {
      if (!stor.liste.changed) {
        stor.liste.focus()
        return false
      }
    }
    if (stor.etat === 'reduc') {
      if (!stor.liste.changed) {
        stor.liste.focus()
        return false
      }
      if (stor.liste.reponse === 'non') {
        if (stor.zones.reponse() === '') {
          stor.zones.focus()
          return false
        }
      }
      return true
    }
    let i
    if (stor.zone.length !== 0) {
      for (i = 0; i < stor.zone.length; i++) {
        if (stor.zone[i].reponse() === '') {
          stor.zone[i].focus()
          return false
        }
      }
    }
    return true
  }
  function isRepOk () {
    return stor.repEl
  }
  function isRepOkReduc () {
    const red = notsimpli(stor.calculeencours.frac[0]) && ((Math.abs(stor.calculeencours.frac[0].num) !== 1) && (stor.calculeencours.frac[0].den !== 1))
    const larep = (stor.liste.reponse === 'non')
    if (red !== larep) {
      affRedFo(1)
    } else {
      if (!larep) {
        stor.repEl = true
        stor.etat = 'fin'
        stor.liste.disable()
        stor.liste.corrige(true)
        me.sectionCourante()
      } else {
        const y = parseFloat2(stor.zones.reponse())
        const pg = pgcd(Math.abs(stor.calculeencours.frac[0].num), Math.abs(stor.calculeencours.frac[0].den))
        if (pg % Math.abs(y) !== 0) {
          affRedFo(2)
        } else {
          poseReduc2()
        }
      }
    }
  }
  function isRepOkSignes () {
    if (stor.calculeencours.prop[0] === '+') {
      if (stor.liste.reponse === '+') {
        stor.compteurPe7++
        feFoSigne(1)
      } else {
        stor.liste.corrige(true)
        stor.liste.disable()
        poseLastquest()
      }
    } else {
      if (stor.liste.reponse === '+') {
        stor.liste.corrige(true)
        stor.liste.disable()
        poseLastquest()
      } else {
        stor.compteurPe7++
        feFoSigne(2)
      }
    }
  }
  function feFoSigne (x) {
    stor.liste.corrige(false)
    stor.nbtente--
    stor.lechhh = ''
    let text
    if (x === 2) { text = 'Quand on supprime des parenthèses précédées du signe <b>-</b> , <br> les signes à l’intèrieur des parenthèses changent !' } else {
      text = 'Quand on supprime des parenthèses précédées du signe <b>+</b> , <br> les signes à l’intèrieur des parenthèses ne changent pas !'
    }
    stor.lastsaid = text
    if (stor.nbtente === 0) {
      stor.repEl = false
      stor.zzzSigne = false
      stor.liste.disable()
      me.sectionCourante()
    } else {
      affModale(text)
    }
  }

  function initSection () {
    let i, tabuf, j
    initCptPe(ds, stor)
    initChoices()
    stor.Iid = j3pGetRandomInt(1, 100000)

    // Construction de la page
    me.construitStructurePage('presentation3')
    let mimin = 1
    if (ds.Priorite) mimin = 2
    if (ds.Nombre_operations < mimin) { ds.Nombre_operations = mimin }
    if (ds.Nombre_operations > 3) { ds.Nombre_operations = 3 }

    stor.yakR = false
    ds.lesExos = []
    let lp = []
    if (ds.multiplication)lp.push('x')
    if (ds.division)lp.push('/')
    if (ds.somme) lp.push('+')
    if (ds.difference) lp.push('-')
    if (lp.length === 0) {
      if (!ds.Reduction) {
        console.error('Problème dans le paramétrage: aucune opération possible !')
      } else {
        stor.yakR = true
      }
      lp = ['+']
    }
    if (lp.length === 1) ds.Nombre_operations = 1
    stor.lp = []
    for (i = 0; i < lp.length; i++) { stor.lp[i] = lp[i] }

    for (i = 1; i < ds.Nombre_operations; i++) {
      tabuf = []
      for (j = 0; j < stor.lp.length; j++) {
        tabuf = tabuf.concat(colle(stor.lp[j], lp))
      }
      stor.lp = []
      for (j = 0; j < tabuf.length; j++) { stor.lp[j] = tabuf[j] }
    }
    for (i = stor.lp.length - 1; i > -1; i--) {
      if ((stor.lp[i].replace('/', '').indexOf('/') !== -1) ||
        (stor.lp[i].indexOf('++') !== -1) ||
        (stor.lp[i].indexOf('+-') !== -1) ||
        (stor.lp[i].indexOf('xx') !== -1) ||
        (stor.lp[i].indexOf('/x') !== -1)) {
        stor.lp.splice(i, 1)
      }
    }
    stor.lp = j3pShuffle(stor.lp)

    for (i = 0; i < ds.nbrepetitions; i++) {
      ds.lesExos[i] = { prop: stor.lp[i % (stor.lp.length)] }
    }

    stor.lp = [false]
    if (ds.Entiers) stor.lp.push(true)
    stor.lp = j3pShuffle(stor.lp)

    for (i = 0; i < ds.nbrepetitions; i++) {
      ds.lesExos[i].Entiers = stor.lp[i % stor.lp.length]
    }
    ds.lesExos = j3pShuffle(ds.lesExos)

    stor.larestric = '0123456789'
    stor.clvspe = false
    if (!ds.Positifs) {
      stor.larestric += '-()'
      stor.clvspe = true
    }
    me.typederreurs = [0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0]
    me.afficheTitre('Calculer pas à pas')
    enonceMain()
  }

  function AffCorrection () {
    stor.lesdiv.etape.innerHTML = ''
    stor.lesdiv.explications.classList.add('explique')
    if (stor.lechhh !== '') {
      const yy = j3pAffiche(stor.lesdiv.explications, null, stor.lechhh + '\n')
      j3pBarre(yy.parent)
    }
    j3pAffiche(stor.lesdiv.explications, null, stor.lastsaid)
  }
  function poseQuestion () {
    stor.rrrrrrr = false
    stor.lechhh = ''
    stor.sauteEtape = false
    stor.comptediv = 0
    stor.zones = undefined
    stor.lesdiv.travailEnCours = stor.lesdiv.travail
    stor.lololo = false
    stor.lololo2 = false
    let i
    stor.buf = []
    stor.buf[0] = '\\frac{' + stor.Lexo.frac[0].num + '}{' + stor.Lexo.frac[0].den + '}'
    stor.calculeencours = {}
    stor.calculeencours.prop = stor.Lexo.prop
    stor.calculeencours.frac = []
    for (i = 0; i < stor.Lexo.frac.length; i++) {
      stor.calculeencours.frac[i] = { num: stor.Lexo.frac[i].num, den: stor.Lexo.frac[i].den }
    }
    if (ds.Priorite) {
      j3pAffiche(stor.lesdiv.consigneG, null, '<b>Sélectionne l’opération à effectuer en premier </b> \n\n')
    } else {
      j3pAffiche(stor.lesdiv.consigne, null, '<b>On veut calculer </b> ')
    }
    stor.etat = 'prio'
    affichepourprio()
  }
  function poseQuestion2 () {
    stor.lesdiv.etape.innerHTML = ''
    j3pAffiche(stor.lesdiv.etape, null, '<b><u>Étape</u>:</b> Clique sur le cadre bleu pour sélectionner une méthode')
    addRow(stor.lesdiv.tablo)
    stor.lerepere = affichetoutlecalc(false)[1].parentNode.parentNode
    ajouteMasqueMethode(stor.lerepere)
    me.cacheBoutonValider()
    stor.etat = stor.calculeencours.prop[0]
    toobas()
  }
  function toobas () {
    me.zonesElts.MG.scrollTo({
      left: 0,
      top: me.zonesElts.MG.scrollHeight - me.zonesElts.MG.clientHeight,
      behavior: 'smooth'
    })
  }
  function poseQuestion3 (f) {
    stor.lechhh = ''
    stor.zzzSigne = false
    stor.cons = []
    stor.cons2 = 1
    let bufetape
    let i
    let y
    stor.lalala = true
    stor.lesdiv.etape.innerHTML = ''
    stor.opencours = f
    stor.op = []
    stor.zone = []
    stor.etat = f
    let t
    let t1
    let t2
    let t3
    let t4
    let n1
    let n2
    let n3
    let n4
    let f1
    let f2
    let ag
    let ad
    let wt2
    let wt3
    let npg
    let npd
    stor.lerepere.innerHTML = ''
    j3pDetruit(stor.masque)
    stor.menuMasque.destroy()
    switch (f) {
      case '+1': {
        bufetape = 'Ajouter'
        if (!ds.Detail_Somme) {
          stor.sauteEtape = true
          poseLastquest()
          return
        }
        const tabplus = afficheFrac(stor.lerepere)
        const tt = afficheplus(tabplus[0])
        stor.op.push({
          pl: tt[0],
          inv: false
        })
        stor.op.push({
          pl: tt[2],
          inv: false
        })
        stor.op.push({
          pl: tabplus[2],
          inv: true
        })
        t = stor.calculeencours.frac[1].num
        f1 = stor.calculeencours.frac[0].num
        if (!stor.listebool[0]) {
          t3 = t
          t = f1
          f1 = t3
        }
        if (t < 0) t = '(' + t + ')'
        stor.uneco = '\\frac{' + f1 + '+' + t + '}{' + stor.calculeencours.frac[0].den + '}'
      }
        break
      case '+2':
        bufetape = 'Reduire au même dénominateur'
        n1 = stor.calculeencours.frac[0].den
        n2 = stor.calculeencours.frac[1].den
        y = pgcd(Math.abs(n1), Math.abs(n2))
        if ((y !== Math.abs(n1)) && (y !== Math.abs(n2))) {
          if (!ds.Detail_Reduction) {
            stor.sauteEtape = true
            poseLastquest()
            return
          }
          if (stor.listepar[0]) { t = afficheplus2par2(stor.lerepere) } else { t = afficheplus2(stor.lerepere) }
          f1 = '\\frac{' + stor.calculeencours.frac[0].num + ' \\times ' + stor.calculeencours.frac[1].den + '}{' + stor.calculeencours.frac[0].den + ' \\times ' + stor.calculeencours.frac[1].den + '}'
          f2 = '\\frac{' + stor.calculeencours.frac[1].num + ' \\times ' + stor.calculeencours.frac[0].den + '}{' + stor.calculeencours.frac[1].den + ' \\times ' + stor.calculeencours.frac[0].den + '}'
          if (!stor.listebool[0]) {
            t3 = t[0]
            t[0] = t[2]
            t[2] = t3
            stor.uneco = f2 + '+' + f1
          } else {
            stor.uneco = f1 + '+' + f2
          }
          t1 = affichefrac2(t[0], false, false)
          t2 = affichefrac2(t[2], false, false)
          t3 = affichefois2(t1[0], false, false)
          t4 = affichefois2(t1[2], false, false)
          j3pAffiche(t3[0], null, '$' + stor.calculeencours.frac[0].num + '$')
          j3pAffiche(t4[0], null, '$' + stor.calculeencours.frac[0].den + '$')
          stor.op.push({ pl: t3[2], inv: false })
          stor.op.push({ pl: t4[2], inv: true })
          t3 = affichefois2(t2[0])
          t4 = affichefois2(t2[2])
          j3pAffiche(t3[0], null, '$' + stor.calculeencours.frac[1].num + '$')
          j3pAffiche(t4[0], null, '$' + stor.calculeencours.frac[1].den + '$')
          stor.op.push({ pl: t3[2], inv: false })
          stor.op.push({ pl: t4[2], inv: true })
        } else if (Math.abs(n1) < Math.abs(n2)) {
          stor.etat = '+3'
          if (!ds.Detail_Reduction) {
            stor.sauteEtape = true
            poseLastquest()
            return
          }
          if (stor.listepar[0]) { t = afficheplus2par2(stor.lerepere) } else { t = afficheplus2(stor.lerepere) }
          f1 = '\\frac{' + stor.calculeencours.frac[0].num + ' \\times ' + Math.round(stor.calculeencours.frac[1].den / stor.calculeencours.frac[0].den) + '}{' + stor.calculeencours.frac[0].den + ' \\times ' + Math.round(stor.calculeencours.frac[1].den / stor.calculeencours.frac[0].den) + '} '
          f2 = '\\frac{' + stor.calculeencours.frac[1].num + ' }{' + stor.calculeencours.frac[1].den + '}'
          if (!stor.listebool[0]) {
            t3 = t[0]
            t[0] = t[2]
            t[2] = t3
            stor.uneco = f2 + '+' + f1
          } else {
            stor.uneco = f1 + '+' + f2
          }
          t1 = affichefrac2(t[0], false, false)
          t2 = affichefrac2(t[2], false, false)
          t3 = affichefois2(t1[0], false, false)
          t4 = affichefois2(t1[2], false, false)
          j3pAffiche(t3[0], null, '$' + stor.calculeencours.frac[0].num + '$')
          j3pAffiche(t4[0], null, '$' + stor.calculeencours.frac[0].den + '$')
          stor.op.push({ pl: t3[2], inv: false })
          stor.op.push({ pl: t4[2], inv: true })
          j3pAffiche(t2[0], null, '$' + stor.calculeencours.frac[1].num + '$')
          j3pAffiche(t2[2], null, '$' + stor.calculeencours.frac[1].den + '$')
        } else {
          stor.etat = '+4'
          if (!ds.Detail_Reduction) {
            stor.sauteEtape = true
            poseLastquest()
            return
          }
          if (stor.listepar[0]) { t = afficheplus2par2(stor.lerepere) } else { t = afficheplus2(stor.lerepere) }
          f1 = '\\frac{' + stor.calculeencours.frac[0].num + ' }{' + stor.calculeencours.frac[0].den + '}'
          f2 = '\\frac{' + stor.calculeencours.frac[1].num + ' \\times ' + Math.round(stor.calculeencours.frac[0].den / stor.calculeencours.frac[1].den) + '}{' + stor.calculeencours.frac[1].den + ' \\times ' + Math.round(stor.calculeencours.frac[0].den / stor.calculeencours.frac[1].den) + '} '
          if (!stor.listebool[0]) {
            t3 = t[0]
            t[0] = t[2]
            t[2] = t3
            stor.uneco = f2 + '+' + f1
          } else {
            stor.uneco = f1 + '+' + f2
          }
          t1 = affichefrac2(t[0], false, false)
          t2 = affichefrac2(t[2], false, false)
          t3 = affichefois2(t2[0], false, false)
          t4 = affichefois2(t2[2], false, false)
          j3pAffiche(t3[0], null, '$' + stor.calculeencours.frac[1].num + '$')
          j3pAffiche(t4[0], null, '$' + stor.calculeencours.frac[1].den + '$')
          stor.op.push({ pl: t3[2], inv: false })
          stor.op.push({ pl: t4[2], inv: true })
          j3pAffiche(t1[0], null, '$' + stor.calculeencours.frac[0].num + '$')
          j3pAffiche(t1[2], null, '$' + stor.calculeencours.frac[0].den + '$')
        }
        break
      case '-1': {
        bufetape = 'Soustraire'
        if (!ds.Detail_Somme) {
          stor.sauteEtape = true
          poseLastquest()
          return
        }
        const tabplus = afficheFrac(stor.lerepere)
        const tt = affichemoins(tabplus[0])
        stor.op.push({
          pl: tt[0],
          inv: false
        })
        stor.op.push({
          pl: tt[2],
          inv: false
        })
        stor.op.push({
          pl: tabplus[2],
          inv: true
        })
        t = stor.calculeencours.frac[1].num
        if (t < 0) t = '(' + t + ')'
        stor.uneco = '\\frac{' + stor.calculeencours.frac[0].num + '-' + t + '}{' + stor.calculeencours.frac[0].den + '}'
      }
        break
      case '-2': bufetape = 'Reduire au même dénominateur'
        n1 = stor.calculeencours.frac[0].den
        n2 = stor.calculeencours.frac[1].den
        y = pgcd(Math.abs(n1), Math.abs(n2))
        if ((y !== Math.abs(n1)) && (y !== Math.abs(n2))) {
          if (!ds.Detail_Reduction) {
            stor.sauteEtape = true
            poseLastquest()
            return
          }
          if (stor.listepar[0]) { t = affichemoins2par2(stor.lerepere) } else { t = affichemoins2(stor.lerepere) }
          f1 = '\\frac{' + stor.calculeencours.frac[0].num + ' \\times ' + stor.calculeencours.frac[1].den + '}{' + stor.calculeencours.frac[0].den + ' \\times ' + stor.calculeencours.frac[1].den + '}'
          f2 = '\\frac{' + stor.calculeencours.frac[1].num + ' \\times ' + stor.calculeencours.frac[0].den + '}{' + stor.calculeencours.frac[1].den + ' \\times ' + stor.calculeencours.frac[0].den + '}'
          if (!stor.listebool[0]) {
            t3 = t[0]
            t[0] = t[2]
            t[2] = t3
            stor.uneco = f2 + '-' + f1
          } else {
            stor.uneco = f1 + '-' + f2
          }
          t1 = affichefrac2(t[0], false, false)
          t2 = affichefrac2(t[2], false, false)
          t3 = affichefois2(t1[0], false, false)
          t4 = affichefois2(t1[2], false, false)
          j3pAffiche(t3[0], null, '$' + stor.calculeencours.frac[0].num + '$')
          j3pAffiche(t4[0], null, '$' + stor.calculeencours.frac[0].den + '$')
          stor.op.push({ pl: t3[2], inv: false })
          stor.op.push({ pl: t4[2], inv: true })
          t3 = affichefois2(t2[0])
          t4 = affichefois2(t2[2])
          j3pAffiche(t3[0], null, '$' + stor.calculeencours.frac[1].num + '$')
          j3pAffiche(t4[0], null, '$' + stor.calculeencours.frac[1].den + '$')
          stor.op.push({ pl: t3[2], inv: false })
          stor.op.push({ pl: t4[2], inv: true })
        } else if (Math.abs(n1) < Math.abs(n2)) {
          stor.etat = '-3'
          if (!ds.Detail_Reduction) {
            stor.sauteEtape = true
            poseLastquest()
            return
          }
          if (stor.listepar[0]) { t = affichemoins2par2(stor.lerepere) } else { t = affichemoins2(stor.lerepere) }
          if (!stor.listebool[0]) {
            t3 = t[0]
            t[0] = t[2]
            t[2] = t3
          }
          t1 = affichefrac2(t[0], false, false)
          t2 = affichefrac2(t[2], false, false)
          t3 = affichefois2(t1[0], false, false)
          t4 = affichefois2(t1[2], false, false)
          j3pAffiche(t3[0], null, '$' + stor.calculeencours.frac[0].num + '$')
          j3pAffiche(t4[0], null, '$' + stor.calculeencours.frac[0].den + '$')
          stor.op.push({ pl: t3[2], inv: false })
          stor.op.push({ pl: t4[2], inv: true })
          j3pAffiche(t2[0], null, '$' + stor.calculeencours.frac[1].num + '$')
          j3pAffiche(t2[2], null, '$' + stor.calculeencours.frac[1].den + '$')
          stor.uneco = '\\frac{' + stor.calculeencours.frac[0].num + ' \\times ' + Math.round(stor.calculeencours.frac[1].den / stor.calculeencours.frac[0].den) + '}{' + stor.calculeencours.frac[0].den + ' \\times ' + Math.round(stor.calculeencours.frac[1].den / stor.calculeencours.frac[0].den) + '} - '
          stor.uneco += '\\frac{' + stor.calculeencours.frac[1].num + ' }{' + stor.calculeencours.frac[1].den + '}'
        } else {
          stor.etat = '-4'
          if (!ds.Detail_Reduction) {
            stor.sauteEtape = true
            poseLastquest()
            return
          }
          if (stor.listepar[0]) { t = affichemoins2par2(stor.lerepere) } else { t = affichemoins2(stor.lerepere) }
          if (!stor.listebool[0]) {
            t3 = t[0]
            t[0] = t[2]
            t[2] = t3
          }
          t1 = affichefrac2(t[0], false, false)
          t2 = affichefrac2(t[2], false, false)
          t3 = affichefois2(t2[0], false, false)
          t4 = affichefois2(t2[2], false, false)
          j3pAffiche(t3[0], null, '$' + stor.calculeencours.frac[1].num + '$')
          j3pAffiche(t4[0], null, '$' + stor.calculeencours.frac[1].den + '$')
          stor.op.push({ pl: t3[2], inv: false })
          stor.op.push({ pl: t4[2], inv: true })
          j3pAffiche(t1[0], null, '$' + stor.calculeencours.frac[0].num + '$')
          j3pAffiche(t1[2], null, '$' + stor.calculeencours.frac[0].den + '$')
          stor.uneco = '\\frac{' + stor.calculeencours.frac[0].num + ' }{' + stor.calculeencours.frac[0].den + '} -'
          stor.uneco += '\\frac{' + stor.calculeencours.frac[1].num + ' \\times ' + Math.round(stor.calculeencours.frac[0].den / stor.calculeencours.frac[1].den) + '}{' + stor.calculeencours.frac[1].den + ' \\times ' + Math.round(stor.calculeencours.frac[0].den / stor.calculeencours.frac[1].den) + '} '
        }
        break
      case 'x': bufetape = 'Multiplier'
        if (!ds.Detail_Produit) {
          stor.sauteEtape = true
          poseLastquest()
          return
        }
        t1 = affichefrac2(stor.lerepere, false, false)
        t2 = affichefois2(t1[0])
        t3 = affichefois2(t1[2])
        stor.op.push({ pl: t2[0], inv: false })
        stor.op.push({ pl: t2[2], inv: false })
        stor.op.push({ pl: t3[0], inv: true })
        stor.op.push({ pl: t3[2], inv: true })
        n1 = stor.calculeencours.frac[1].num
        n2 = stor.calculeencours.frac[1].den
        n3 = stor.calculeencours.frac[0].num
        n4 = stor.calculeencours.frac[0].den
        if (!stor.listebool[0]) {
          t = n1
          n1 = n3
          n3 = t
          t = n2
          n2 = n4
          n4 = t
        }
        if (n1 < 0) n1 = '(' + n1 + ')'
        if (n2 < 0) n2 = '(' + n2 + ')'
        stor.uneco = '\\frac{' + n3 + ' \\times ' + n1 + '}{' + n4 + ' \\times ' + n2 + '} '
        break
      case 'xs': bufetape = 'Multiplier'
        if (!ds.Detail_Produit) {
          stor.sauteEtape = true
          poseLastquest()
          return
        }
        t1 = affichefrac2(stor.lerepere, false, false)
        t2 = affichefois2(t1[0])
        stor.op.push({ pl: t2[0], inv: false })
        stor.op.push({ pl: t2[2], inv: false })
        stor.op.push({ pl: t1[2], inv: true })
        n1 = stor.calculeencours.frac[1].num
        n2 = stor.calculeencours.frac[1].den
        n3 = stor.calculeencours.frac[0].num
        n4 = stor.calculeencours.frac[0].den
        if (!stor.listebool[0]) {
          t = n1
          n1 = n3
          n3 = t
          t = n2
          n2 = n4
          n4 = t
        }
        if (n1 < 0) n1 = '(' + n1 + ')'
        if (n3 < 0) n3 = '(' + n3 + ')'
        if (!stor.listebool[0]) {
          t = n1
          n1 = n3
          n3 = t
        }
        if (n4 === 'z') { n4 = n2 }
        stor.uneco = '\\frac{' + n3 + ' \\times ' + n1 + '}{' + n4 + '} '
        break
      case '/': bufetape = 'Transformer la division en multiplication'
        t1 = affichefois2(stor.lerepere)
        if (!stor.listebool[0]) {
          f1 = stor.calculeencours.frac[1]
          f2 = stor.calculeencours.frac[0]
        } else {
          f1 = stor.calculeencours.frac[0]
          f2 = stor.calculeencours.frac[1]
        }
        if (f1.den !== 'z') {
          t2 = affichefrac2(t1[0], f1, false)
          stor.uneco = '\\frac{' + f1.num + '}{' + f1.den + '} \\times '
        } else {
          npg = 'z'
          if (!stor.listepar[0] && stor.calculeencours.prop.length > 1) {
            if (stor.calculeencours.prop[1] !== '/' && !stor.listebool[1]) {
              npg = 'p'
            }
          }
          afficheNb(t1[0], f1, npg)
          stor.uneco = f1.num + ' \\times '
        }
        t3 = affichefrac2(t1[2], false, false)
        stor.op.push({ pl: t3[0], inv: false })
        stor.op.push({ pl: t3[2], inv: true })
        n1 = f2.num
        n2 = f2.den
        if (f2.den === 'z') { f2.den = n2 = 1 }
        if (n1 < 0) {
          n1 = -n1
          n2 = -n2
        }
        stor.uneco += ' \\frac{' + n2 + '}{' + n1 + '}'
        break
      case 's0':
      case 's1':
      case 's10':
        if (!ds.Detail_Simplification) {
          stor.sauteEtape = true
          poseLastquest()
          return
        }
        bufetape = 'Simplifier'
        npg = npd = true
        switch (stor.calculeencours.prop[0]) {
          case '+': if (stor.listepar[0]) { t1 = afficheplus2par2(stor.lerepere) } else { t1 = afficheplus2(stor.lerepere) }
            stor.uneco = '£a + £b'
            break
          case '-': if (stor.listepar[0]) { t1 = affichemoins2par2(stor.lerepere) } else { t1 = affichemoins2(stor.lerepere) }
            stor.uneco = '£a - £b'
            break
          case 'x': t1 = affichefois2(stor.lerepere)
            stor.uneco = '£a \\times £b'
            break
          case '/': t1 = affichediv2(stor.lerepere)
            stor.uneco = '\\frac{ £a }{ £b }'
            npg = npd = false
            break
        }
        if (!stor.listebool[0]) {
          wt3 = t1[0]
          wt2 = t1[2]
          ad = '£a'
          ag = '£b'
          if (npd) {
            npd = true
            npg = false
            if (stor.calculeencours.prop.length !== 1) {
              if ((stor.calculeencours.prop[1] !== '/') && (!stor.listebool[1]) && (!stor.listepar[0])) {
                npg = true
              }
            }
          }
        } else {
          wt3 = t1[2]
          wt2 = t1[0]
          ad = '£b'
          ag = '£a'
          if (npd) {
            npd = false
            npg = true
            if (stor.calculeencours.prop.length !== 1) {
              if ((stor.calculeencours.prop[1] !== '/') && (!stor.listebool[1]) && (!stor.listepar[0])) {
                npd = true
              }
            }
          }
        }
        f1 = stor.calculeencours.frac[0]
        f2 = stor.calculeencours.frac[1]
        if (stor.etat.indexOf('0') !== -1) {
          t2 = affichefrac2(wt2, false, false)
          t4 = affichefois2(t2[0])
          stor.op.push({ pl: t4[0], inv: false })
          stor.op.push({ pl: t4[2], inv: false })
          t4 = affichefois2(t2[2])
          stor.op.push({ pl: t4[0], inv: true })
          stor.op.push({ pl: t4[2], inv: true })
          y = pgcd(Math.abs(f1.num), Math.abs(f1.den))
          stor.uneco = stor.uneco.replace(ag, '\\frac{ ' + Math.round(f1.num / y) + ' \\times ' + y + ' }{ ' + Math.round(f1.den / y) + ' \\times ' + y + ' }')
        } else {
          if (f1.den !== 'z') {
            t2 = affichefrac2(wt2, false, false)
            stor.uneco = stor.uneco.replace(ag, '\\frac{ ' + f1.num + ' }{ ' + f1.den + ' }')
            j3pAffiche(t2[0], null, '$' + f1.num + '$')
            j3pAffiche(t2[2], null, '$' + f1.den + '$')
          } else {
            if (npd && f1.num < 0) {
              stor.uneco = stor.uneco.replace(ag, '(' + f1.num + ')')
              j3pAffiche(wt2, null, '$(' + f1.num + ')$')
            } else {
              stor.uneco = stor.uneco.replace(ag, f1.num)
              j3pAffiche(wt2, null, '$' + f1.num + '$')
            }
          }
        }
        if (stor.etat.indexOf('1') !== -1) {
          t3 = affichefrac2(wt3, false, false)
          t4 = affichefois2(t3[0])
          stor.op.push({ pl: t4[0], inv: false })
          stor.op.push({ pl: t4[2], inv: false })
          t4 = affichefois2(t3[2])
          stor.op.push({ pl: t4[0], inv: true })
          stor.op.push({ pl: t4[2], inv: true })
          y = pgcd(Math.abs(f2.num), Math.abs(f2.den))
          stor.uneco = stor.uneco.replace(ad, '\\frac{ ' + Math.round(f2.num / y) + ' \\times ' + y + ' }{ ' + Math.round(f2.den / y) + ' \\times ' + y + ' }')
        } else {
          if (f2.den !== 'z') {
            t3 = affichefrac2(wt3, false, false)
            stor.uneco = stor.uneco.replace(ad, '\\frac{ ' + f2.num + ' }{ ' + f2.den + ' }')
            j3pAffiche(t3[0], null, '$' + f2.num + '$')
            j3pAffiche(t3[2], null, '$' + f2.den + '$')
          } else {
            if (npg && f2.num < 0) {
              stor.uneco = stor.uneco.replace(ad, '(' + f2.num + ')')
              j3pAffiche(wt3, null, '$(' + f2.num + ')$')
            } else {
              stor.uneco = stor.uneco.replace(ad, f2.num)
              j3pAffiche(wt3, null, '$' + f2.num + '$')
            }
          }
        }
    }
    j3pAffiche(stor.lesdiv.etape, null, '<b><u>Étape</u>:</b> ' + bufetape)
    for (i = 0; i < stor.op.length; i++) {
      stor.zone[stor.zone.length] = new ZoneStyleMathquill1(stor.op[i].pl, { restric: stor.larestric, limitenb: 1, limite: 6, collapse: true, hasAutoKeyboard: stor.clvspe, enter: me.sectionCourante.bind(me), inverse: stor.op[i].inv })
    }
    stor.zone[0].focus()
    resize()
    me.afficheBoutonValider()
    toobas()
  }
  function poseLastquest () {
    stor.lechhh = ''
    stor.lalala = false
    stor.zzzSigne = false
    desactivezone(true)
    stor.op = []
    if (stor.etat === '/') {
      if (stor.listebool[0]) {
        stor.calculeencours.frac[1] = { num: stor.cons[0], den: stor.cons[1] }
      } else {
        stor.calculeencours.frac[0] = { num: stor.cons[0], den: stor.cons[1] }
      }
      stor.calculeencours.prop = 'x' + stor.calculeencours.prop.substring(1)
      retouretape1()
      return
    }
    let i
    let t
    let t1
    let t2
    let f1
    let f2
    let t3
    let ag
    let ad
    let wt2
    let wt3
    let npg
    let npd
    let den1
    let den2
    if (!stor.sauteEtape) {
      addRow(stor.lesdiv.tablo)
      stor.lerepere = affichetoutlecalc(false)[1].parentNode
    }
    j3pEmpty(stor.lerepere)
    switch (stor.etat) {
      case '+4':
        if (stor.listepar[0]) { t = afficheplus2par2(stor.lerepere) } else { t = afficheplus2(stor.lerepere) }
        f1 = '\\frac{' + stor.calculeencours.frac[0].num + '}{' + stor.calculeencours.frac[0].den + '} '
        f2 = '\\frac{' + Math.round(stor.calculeencours.frac[1].num * stor.calculeencours.frac[0].den / stor.calculeencours.frac[1].den) + '}{' + stor.calculeencours.frac[0].den + '}'
        if (!stor.listebool[0]) {
          t3 = t[0]
          t[0] = t[2]
          t[2] = t3
          stor.uneco = f2 + '+' + f1
        } else {
          stor.uneco = f1 + '+' + f2
        }
        t1 = affichefrac2(t[0], false, false)
        t2 = affichefrac2(t[2], false, false)
        j3pAffiche(t1[0], null, '$' + stor.calculeencours.frac[0].num + '$')
        j3pAffiche(t1[2], null, '$' + stor.calculeencours.frac[0].den + '$')
        stor.op.push({ pl: t2[0], inv: false })
        stor.op.push({ pl: t2[2], inv: true })
        break
      case '+3':
        if (stor.listepar[0]) { t = afficheplus2par2(stor.lerepere) } else { t = afficheplus2(stor.lerepere) }
        f1 = '\\frac{' + Math.round(stor.calculeencours.frac[0].num * stor.calculeencours.frac[1].den / stor.calculeencours.frac[0].den) + '}{' + stor.calculeencours.frac[1].den + '}'
        f2 = '\\frac{' + stor.calculeencours.frac[1].num + '}{' + stor.calculeencours.frac[1].den + '}'
        if (!stor.listebool[0]) {
          t3 = t[0]
          t[0] = t[2]
          t[2] = t3
          stor.uneco = f2 + '+' + f1
        } else {
          stor.uneco = f1 + '+' + f2
        }
        t1 = affichefrac2(t[0], false, false)
        t2 = affichefrac2(t[2], false, false)
        j3pAffiche(t2[0], null, '$' + stor.calculeencours.frac[1].num + '$')
        j3pAffiche(t2[2], null, '$' + stor.calculeencours.frac[1].den + '$')
        stor.op.push({ pl: t1[0], inv: false })
        stor.op.push({ pl: t1[2], inv: true })
        break
      case '+2':
        if (stor.listepar[0]) { t = afficheplus2par2(stor.lerepere) } else { t = afficheplus2(stor.lerepere) }
        if (stor.cons.length === 0) {
          stor.cons[0] = Math.round(stor.calculeencours.frac[1].den / pgcd(Math.abs(stor.calculeencours.frac[0].den), Math.abs(stor.calculeencours.frac[1].den)))
          stor.cons[1] = Math.round(stor.calculeencours.frac[0].den / pgcd(Math.abs(stor.calculeencours.frac[0].den), Math.abs(stor.calculeencours.frac[1].den)))
        }
        f1 = '\\frac{' + Math.round(stor.calculeencours.frac[0].num * stor.cons[0]) + '}{' + Math.round(stor.calculeencours.frac[0].den * stor.cons[0]) + '} '
        f2 = '\\frac{' + stor.calculeencours.frac[1].num * stor.cons[1] + '}{' + stor.calculeencours.frac[1].den * stor.cons[1] + '}'
        if (stor.listebool[0]) {
          t3 = t[0]
          t[0] = t[2]
          t[2] = t3
          stor.uneco = f1 + '+' + f2
        } else {
          stor.uneco = f2 + '+' + f1
        }
        t1 = affichefrac2(t[0], false, false)
        t2 = affichefrac2(t[2], false, false)
        stor.op.push({ pl: t1[0], inv: false })
        stor.op.push({ pl: t1[2], inv: true })
        stor.op.push({ pl: t2[0], inv: false })
        stor.op.push({ pl: t2[2], inv: true })
        break
      case '+1':
        t1 = affichefrac2(stor.lerepere, false, false)
        stor.op.push({ pl: t1[0], inv: false })
        stor.op.push({ pl: t1[2], inv: true })
        stor.uneco = '\\frac{' + (stor.calculeencours.frac[0].num + stor.calculeencours.frac[1].num) + '}{' + stor.calculeencours.frac[0].den + '}'
        break
      case '-4':
        if (stor.listepar[0]) { t = affichemoins2par2(stor.lerepere) } else { t = affichemoins2(stor.lerepere) }
        f1 = '\\frac{' + stor.calculeencours.frac[0].num + '}{' + stor.calculeencours.frac[0].den + '} '
        f2 = '\\frac{' + Math.round(stor.calculeencours.frac[1].num * stor.calculeencours.frac[0].den / stor.calculeencours.frac[1].den) + '}{' + stor.calculeencours.frac[0].den + '}'
        if (!stor.listebool[0]) {
          t3 = t[0]
          t[0] = t[2]
          t[2] = t3
          stor.uneco = f2 + '-' + f1
        } else {
          stor.uneco = f1 + '-' + f2
        }
        t1 = affichefrac2(t[0], false, false)
        t2 = affichefrac2(t[2], false, false)
        j3pAffiche(t1[0], null, '$' + stor.calculeencours.frac[0].num + '$')
        j3pAffiche(t1[2], null, '$' + stor.calculeencours.frac[0].den + '$')
        stor.op.push({ pl: t2[0], inv: false })
        stor.op.push({ pl: t2[2], inv: true })
        break
      case '-3':
        if (stor.listepar[0]) { t = affichemoins2par2(stor.lerepere) } else { t = affichemoins2(stor.lerepere) }
        f1 = '\\frac{' + stor.calculeencours.frac[0].num + '}{' + stor.calculeencours.frac[0].den + '} '
        f2 = '\\frac{' + Math.round(stor.calculeencours.frac[1].num * stor.calculeencours.frac[0].den / stor.calculeencours.frac[1].den) + '}{' + stor.calculeencours.frac[0].den + '}'
        if (!stor.listebool[0]) {
          t3 = t[0]
          t[0] = t[2]
          t[2] = t3
          stor.uneco = f2 + '-' + f1
        } else {
          stor.uneco = f1 + '-' + f2
        }
        t1 = affichefrac2(t[0], false, false)
        t2 = affichefrac2(t[2], false, false)
        j3pAffiche(t2[0], null, '$' + stor.calculeencours.frac[1].num + '$')
        j3pAffiche(t2[2], null, '$' + stor.calculeencours.frac[1].den + '$')
        stor.op.push({ pl: t1[0], inv: false })
        stor.op.push({ pl: t1[2], inv: true })
        break
      case '-2':
        if (stor.listepar[0]) { t = affichemoins2par2(stor.lerepere) } else { t = affichemoins2(stor.lerepere) }
        if (stor.cons.length === 0) {
          stor.cons[0] = Math.round(stor.calculeencours.frac[1].den / pgcd(Math.abs(stor.calculeencours.frac[0].den), Math.abs(stor.calculeencours.frac[1].den)))
          stor.cons[1] = Math.round(stor.calculeencours.frac[0].den / pgcd(Math.abs(stor.calculeencours.frac[0].den), Math.abs(stor.calculeencours.frac[1].den)))
        }
        f1 = '\\frac{' + Math.round(stor.calculeencours.frac[0].num * stor.cons[0]) + '}{' + Math.round(stor.calculeencours.frac[0].den * stor.cons[0]) + '} '
        f2 = '\\frac{' + stor.calculeencours.frac[1].num * stor.cons[1] + '}{' + stor.calculeencours.frac[1].den * stor.cons[1] + '}'
        if (stor.listebool[0]) {
          t3 = t[0]
          t[0] = t[2]
          t[2] = t3
          stor.uneco = f1 + '-' + f2
        } else {
          stor.uneco = f2 + '-' + f1
        }
        t1 = affichefrac2(t[0], false, false)
        t2 = affichefrac2(t[2], false, false)
        stor.op.push({ pl: t1[0], inv: false })
        stor.op.push({ pl: t1[2], inv: true })
        stor.op.push({ pl: t2[0], inv: false })
        stor.op.push({ pl: t2[2], inv: true })
        break
      case '-1': {
        t1 = affichefrac2(stor.lerepere, false, false)
        stor.op.push({ pl: t1[0], inv: false })
        stor.op.push({ pl: t1[2], inv: true })
        const dedans = stor.listebool[0] ? (stor.calculeencours.frac[0].num - stor.calculeencours.frac[1].num) : (stor.calculeencours.frac[1].num - stor.calculeencours.frac[0].num)
        stor.uneco = '\\frac{' + dedans + '}{' + stor.calculeencours.frac[0].den + '}'
      }
        break
      case 'xs':
        t1 = affichefrac2(stor.lerepere, false, false)
        stor.op.push({ pl: t1[0], inv: false })
        stor.op.push({ pl: t1[2], inv: true })
        den1 = stor.calculeencours.frac[0].den
        den2 = stor.calculeencours.frac[1].den
        if (den1 === 'z') den1 = den2
        stor.uneco = '\\frac{' + (stor.calculeencours.frac[0].num * stor.calculeencours.frac[1].num / stor.cons2) + '}{' + den1 / stor.cons2 + '}'
        break
      case 'x':
        t1 = affichefrac2(stor.lerepere, false, false)
        stor.op.push({ pl: t1[0], inv: false })
        stor.op.push({ pl: t1[2], inv: true })
        stor.uneco = '\\frac{' + (stor.calculeencours.frac[0].num * stor.calculeencours.frac[1].num / stor.cons2) + '}{' + (stor.calculeencours.frac[0].den * stor.calculeencours.frac[1].den / stor.cons2) + '}'
        break
      case 's0':
      case 's1':
      case 's10':
        npg = npd = true
        switch (stor.calculeencours.prop[0]) {
          case '+': if (stor.listepar[0]) { t1 = afficheplus2par2(stor.lerepere) } else { t1 = afficheplus2(stor.lerepere) }
            stor.uneco = '£a + £b'
            break
          case '-': if (stor.listepar[0]) { t1 = affichemoins2par2(stor.lerepere) } else { t1 = affichemoins2(stor.lerepere) }
            stor.uneco = '£a - £b'
            break
          case 'x': t1 = affichefois2(stor.lerepere)
            stor.uneco = '£a \\times £b'
            break
          case '/': t1 = affichediv2(stor.lerepere)
            stor.uneco = '\\frac{ £a }{ £b }'
            break
        }
        if (!stor.listebool[0]) {
          wt3 = t1[0]
          wt2 = t1[2]
          ad = '£a'
          ag = '£b'
          if (npd) {
            npd = true
            npg = false
            if (stor.calculeencours.prop.length !== 1) {
              if ((stor.calculeencours.prop[1] !== '/') && (!stor.listebool[1]) && (!stor.listepar[0])) {
                npg = true
              }
            }
          }
        } else {
          wt3 = t1[2]
          wt2 = t1[0]
          ad = '£b'
          ag = '£a'
          if (npd) {
            npd = false
            npg = true
            if (stor.calculeencours.prop.length !== 1) {
              if ((stor.calculeencours.prop[1] !== '/') && (!stor.listebool[1]) && (!stor.listepar[0])) {
                npd = true
              }
            }
          }
        }
        f1 = stor.calculeencours.frac[0]
        f2 = stor.calculeencours.frac[1]
        if (stor.etat.indexOf('0') !== -1) {
          t2 = affichefrac2(wt2, false, false)
          stor.op.push({ pl: t2[0], inv: false })
          stor.op.push({ pl: t2[2], inv: true })
          if (stor.sauteEtape) {
            stor.uneco = stor.uneco.replace(ag, '\\frac{ ' + Math.round(f1.num / pgcd(Math.abs(f1.num), Math.abs(f1.den))) + ' }{ ' + Math.round(f1.den / pgcd(Math.abs(f1.num), Math.abs(f1.den))) + ' }')
          } else {
            stor.uneco = stor.uneco.replace(ag, '\\frac{ ' + Math.round(f1.num / stor.cons[0]) + ' }{ ' + Math.round(f1.den / stor.cons[0]) + ' }')
          }
        } else {
          if (f1.den !== 'z') {
            t2 = affichefrac2(wt2, false, false)
            stor.uneco = stor.uneco.replace(ag, '\\frac{ ' + f1.num + ' }{ ' + f1.den + ' }')
            j3pAffiche(t2[0], null, '$' + f1.num + '$')
            j3pAffiche(t2[2], null, '$' + f1.den + '$')
          } else {
            if (npd && f1.num < 0) {
              stor.uneco = stor.uneco.replace(ag, '(' + f1.num + ')')
              j3pAffiche(wt2, null, '$(' + f1.num + ')$')
            } else {
              stor.uneco = stor.uneco.replace(ag, f1.num)
              j3pAffiche(wt2, null, '$' + f1.num + '$')
            }
          }
        }
        if (stor.etat.indexOf('1') !== -1) {
          t3 = affichefrac2(wt3, false, false)
          stor.op.push({ pl: t3[0], inv: false })
          stor.op.push({ pl: t3[2], inv: true })
          if (stor.sauteEtape) {
            stor.uneco = stor.uneco.replace(ag, '\\frac{ ' + Math.round(f2.num / pgcd(Math.abs(f2.num), Math.abs(f2.den))) + ' }{ ' + Math.round(f2.den / pgcd(Math.abs(f2.num), Math.abs(f2.den))) + ' }')
          } else {
            stor.uneco = stor.uneco.replace(ad, '\\frac{ ' + Math.round(f2.num / stor.cons[stor.cons.length - 1]) + ' }{ ' + Math.round(f2.den / stor.cons[stor.cons.length - 1]) + ' }')
          }
        } else {
          if (f2.den !== 'z') {
            t3 = affichefrac2(wt3, false, false)
            stor.uneco = stor.uneco.replace(ad, '\\frac{ ' + f2.num + ' }{ ' + f2.den + ' }')
            j3pAffiche(t3[0], null, '$' + f2.num + '$')
            j3pAffiche(t3[2], null, '$' + f2.den + '$')
          } else {
            if (npg && f2.num < 0) {
              stor.uneco = stor.uneco.replace(ad, '(' + f2.num + ')')
              j3pAffiche(wt3, null, '$(' + f2.num + ')$')
            } else {
              stor.uneco = stor.uneco.replace(ad, f2.num)
              j3pAffiche(wt3, null, '$' + f2.num + '$')
            }
          }
        }
    }
    stor.lesdiv.etape.innerHTML = ''
    j3pAffiche(stor.lesdiv.etape, null, '<b><u>Étape</u>:</b> Terminer le calcul intermédiaire.')
    for (i = 0; i < stor.op.length; i++) {
      stor.zone[stor.zone.length] = new ZoneStyleMathquill1(stor.op[i].pl, { restric: stor.larestric, limitenb: 1, limite: 6, collapse: true, hasAutoKeyboard: stor.clvspe, inverse: stor.op[i].inv, enter: me.sectionCourante.bind(me) })
    }
    stor.zone[0].focus()
    me.afficheBoutonValider()
    resize()
    stor.sauteEtape = false
    toobas()
  }
  function retouretape1 () {
    desactivezone(true)
    stor.zone = []
    stor.op = []
    // FIXME, avant on avait stor.divtravailencours.innerHTML = '', sauf que divtravailencours était un id, donc ça ne faisait rien
    // pour vider la zone ce serait
    // j3pEmpty(stor.lesdiv.travailEnCours)
    stor.etat = 'prio'
    if (stor.calculeencours.prop.length === 0) {
      if (ds.Reduction) {
        poseReduc()
        return
      }
      stor.repEl = true
      me.sectionCourante()
      return
    }
    affichepourprio()
    toobas()
  }
  function poseReduc () {
    stor.lesdiv.etape.innerHTML = '<b><u>Étape</u>:</b> Simplification'
    stor.rrrrrrr = true
    stor.tabffi = addDefaultTable(stor.lesdiv.travailz2, 2, 2)
    modif(stor.tabffi)
    const tabti = addDefaultTable(stor.tabffi[0][0], 1, 2)
    modif(tabti)
    j3pAffiche(tabti[0][0], null, 'Cette fraction est-elle simplifiée au maximum ?&nbsp;')
    stor.liste = ListeDeroulante.create(tabti[0][1], ['Choisir', 'oui', 'non'], { onChange: gereirre })
    stor.lesdiv.travailz2.classList.add('travail')
    me.afficheBoutonValider()
    stor.etat = 'reduc'
    toobas()
  }
  function modif (tab) {
    for (let i = 0; i < tab.length; i++) {
      for (let j = 0; j < tab[i].length; j++) {
        tab[i][j].style.padding = 0
      }
    }
  }
  function poseReduc2 () {
    stor.cons = stor.zones.reponse()
    stor.zones.disable()
    stor.tabffi[0][0].innerHTML = ''
    stor.tabffi[1][0].innerHTML = ''
    if (!ds.Detail_Simplification) {
      stor.sauteEtape = true
      poseReduc3()
      return
    }
    stor.lololo = true
    stor.lololo2 = false
    addRow(stor.lesdiv.tablo)
    stor.whetab = addDefaultTable(stor.lesdiv.travailEnCours, 1, 2)
    j3pEmpty(stor.whetab[0][0])
    j3pAffiche(stor.whetab[0][0], null, '$ A = $')
    const where = stor.whetab[0][1]
    const t = affichefrac2(where, false, false)
    const t2 = affichefois2(t[0])
    const t3 = affichefois2(t[2])
    j3pAffiche(t2[2], null, '$' + stor.cons + '$')
    j3pAffiche(t3[2], null, '$' + stor.cons + '$')
    stor.op = [{ pl: t2[0], inv: false }, { pl: t3[0], inv: true }]
    for (let i = 0; i < stor.op.length; i++) {
      stor.zone[stor.zone.length] = new ZoneStyleMathquill1(stor.op[i].pl, { restric: stor.larestric, limitenb: 1, limite: 6, collapse: true, hasAutoKeyboard: stor.clvspe, inverse: stor.op[i].inv, enter: me.sectionCourante.bind(me) })
    }
    resize()
    me.afficheBoutonValider()
    stor.zone[0].focus()
    toobas()
  }
  function poseReduc3 () {
    stor.lololo = false
    stor.lololo2 = true
    stor.lesdiv.etape.innerHTML = '<b><u>Étape</u>:</b> Simplification par ' + stor.cons
    desactivezone()
    if (!stor.sauteEtape) {
      j3pEmpty(stor.lesdiv.travailEnCours)
      j3pAffiche(stor.lesdiv.travailEnCours, null, '$ A = ' + stor.uneco + '$')
    }
    addRow(stor.lesdiv.tablo)
    stor.whetab = addDefaultTable(stor.lesdiv.travailEnCours, 1, 2)
    j3pEmpty(stor.whetab[0][0])
    j3pAffiche(stor.whetab[0][0], null, '$ A = $')
    const where = stor.whetab[0][1]
    const t = affichefrac2(where, false, false)
    stor.op = [{ pl: t[0], inv: false }, { pl: t[2], inv: true }]
    for (let i = 0; i < stor.op.length; i++) {
      stor.zone[stor.zone.length] = new ZoneStyleMathquill1(stor.op[i].pl, { restric: stor.larestric, limitenb: 1, limite: 6, collapse: true, hasAutoKeyboard: stor.clvspe, inverse: stor.op[i].inv, enter: me.sectionCourante.bind(me) })
    }
    resize()
    me.afficheBoutonValider()
    stor.zone[0].focus()
    toobas()
  }
  function poseSimplMult () {
    affModaleSimpl1()
  }
  function remp0 () {
    j3pDetruit('modale')
    j3pDetruit('j3pmasque')
    if (stor.calculeencours.frac[0].den === 'z') { stor.calculeencours.frac[0].den = 1 } else { stor.calculeencours.frac[1].den = 1 }
    if (stor.calculeencours.prop[0] === '+') { poseQuestion3('+2') } else { poseQuestion3('-2') }
  }
  function foremp1 () {
    foremp(1)
  }
  function foremp2 () {
    foremp(2)
  }
  function foremp (x) {
    j3pDetruit('modale')
    j3pDetruit('j3pmasque')
    stor.nbtente--
    stor.compteurPe6++
    stor.lechhh = '$' + stor.jkl.num + ' \\ne '
    if (x === 1) { stor.lechhh += '\\frac{' + stor.jkl.num + '}{' + stor.jkl.num + '}$' } else { stor.lechhh += '\\frac{1}{' + stor.jkl.num + '}$' }
    const text = 'Pour transformer un entier en fraction <br> il te suffit de mettre $1$ au dénominateur !!'
    stor.lastsaid = text
    if (stor.nbtente === 0) {
      stor.repEl = false
      j3pDetruit(stor.masque)
      stor.menuMasque.destroy()
      me.sectionCourante()
    } else {
      affModale(text)
    }
  }
  function affichetoutlecalc (boll) {
    stor.ppppar = []
    let i
    stor.nbpar = 0
    for (i = 0; i < stor.calculeencours.prop.length; i++) {
      if (stor.listepar[i]) stor.nbpar++
    }
    let repere = []
    let v
    j3pEmpty(stor.lesdiv.travailEnCours)
    stor.whetab = addDefaultTable(stor.lesdiv.travailEnCours, 1, 2)
    modif(stor.whetab)
    j3pEmpty(stor.whetab[0][0])
    j3pAffiche(stor.whetab[0][0], null, '$ A = $')
    let where = stor.whetab[0][1]
    for (i = stor.calculeencours.prop.length - 1; i > -1; i--) {
      switch (stor.calculeencours.prop[i]) {
        case '+': if (!stor.listepar[i]) { repere = afficheplus2(where, boll) } else { repere = afficheplus2par2(where, boll) }
          break
        case '-': if (!stor.listepar[i]) { repere = affichemoins2(where, boll) } else { repere = affichemoins2par2(where, boll) }
          break
        case 'x': repere = affichefois2(where, boll)
          break
        case '/': repere = affichediv2(where, boll)
          break
      }
      if (!stor.listebool[i]) {
        if (stor.calculeencours.frac[i + 1].den !== 'z') {
          affichefrac2(repere[0], stor.calculeencours.frac[i + 1], boll)
        } else {
          v = 'z'
          if (!stor.listepar[i] && i < stor.calculeencours.prop.length) {
            if (stor.calculeencours.prop[i + 1] !== '/' && !stor.listebool[i + 1]) {
              v = 'p'
            }
          }
          if (stor.calculeencours.prop[i] === '/') v = 'h'
          afficheNb(repere[0], stor.calculeencours.frac[i + 1], v)
        }
        where = repere[2]
      } else {
        if (stor.calculeencours.frac[i + 1].den !== 'z') {
          affichefrac2(repere[2], stor.calculeencours.frac[i + 1], boll)
        } else {
          v = 'p'
          if (stor.calculeencours.prop[i] === '/') v = 'b'
          afficheNb(repere[2], stor.calculeencours.frac[i + 1], v)
        }
        where = repere[0]
      }
    }
    if (stor.calculeencours.frac[0].den !== 'z') {
      if ((stor.calculeencours.frac[0].den === 1) && (stor.calculeencours.prop === '')) {
        j3pAffiche(where, null, '$' + stor.calculeencours.frac[0].num + '$')
      } else {
        affichefrac2(where, stor.calculeencours.frac[0], boll)
      }
    } else {
      if (stor.listebool[0]) {
        v = 'z'
        if (!stor.listepar[0] && stor.calculeencours.prop.length > 1) {
          if (stor.calculeencours.prop[1] !== '/' && !stor.listebool[1]) {
            v = 'p'
          }
        }
      } else { v = 'p' }
      if (stor.calculeencours.prop[0] === '/') {
        if (stor.listebool[0]) { v = 'h' } else { v = 'b' }
      }
      afficheNb(where, stor.calculeencours.frac[0], v)
    }
    resize()
    return repere
  }
  function affichepourprio () {
    // reecris le calcul avec event sur les op
    stor.lesdiv.etape.innerHTML = ''
    if (!ds.Priorite) j3pAffiche(stor.lesdiv.etape, null, '<b><u>Étape</u>:</b> Sélectionne l’opération à effectuer en premier.')
    stor.listeadza = []
    stor.opat = affichetoutlecalc(true)[1]
    me.cacheBoutonValider()
    if (stor.calculeencours.prop === '') {
      desactiveprio()
      poseReduc()
    }
  }
  function clicprio (r) {
    if (stor.repEl) {
      if (!ds.Priorite) {
        if (stor.typop !== '/') {
          stor.oucli.setAttribute('class', 'opautrevrai')
        } else {
          stor.oucli.style.background = ''
          stor.oucli.setAttribute('class', 'fracdivvrai')
        }
        desactiveprio()
        poseQuestion2()
      } else {
        desactiveprio()
        if (stor.typop !== '/') {
          stor.oucli.setAttribute('class', 'opautrevrai')
        } else {
          stor.oucli.style.background = ''
          stor.oucli.setAttribute('class', 'fracdivvrai')
        }
        // stor.oucli.setAttribute('class', 'opautrevrai')
        me.sectionCourante()
      }
    } else {
      stor.nbtente--
      if (!r) {
        stor.compteurPe1++
        stor.lastsaid = 'Cette opération n’est pas prioritaire !'
      } else {
        stor.lastsaid = 'Pour effectuer un calcul fractionnaire, <br> on ne calcule jamais la valeur décimale des fractions !'
      }

      if (stor.nbtente === 0) {
        stor.repel = false
        desactiveprio()
        if (stor.typop !== '/') {
          stor.oucli.setAttribute('class', 'opautrefo')
        } else {
          stor.oucli.style.background = ''
          stor.oucli.setAttribute('class', 'fracdivfo')
        }
        stor.oucli.setAttribute('class', 'opautrefo')
        if (stor.calculeencours.prop[0] !== '/') {
          stor.opat.setAttribute('class', 'opautreco')
        } else {
          stor.opat.style.background = ''
          stor.opat.setAttribute('class', 'fracdivco')
        }
        me.sectionCourante()
      } else {
        affModale(stor.lastsaid)
      }
    }
  }
  function desactiveprio () {
    let i
    let e
    for (i = 0; i < stor.listeadza.length; i++) {
      e = stor.listeadza[i].id
      e.removeEventListener('click', gereop, false)
      e.removeEventListener('click', gereop2, false)
      e.style.cursor = ''
      e.setAttribute('class', '')
      if (stor.listeadza[i].frac) {
        e.style.background = '#000000'
      }
    }
  }
  function ajouteMasqueMethode (qui) {
    const acache = qui
    const pos = acache.getBoundingClientRect()
    const posbase = me.zonesElts.MG.getBoundingClientRect()
    const dec = 0
    stor.lechhh = ''
    const f1 = stor.calculeencours.frac[0]
    const f2 = stor.calculeencours.frac[1]
    let buf
    stor.masque = j3pAddElt(me.zones.MG, 'div', {
      style: me.styles.petit.enonce
    })
    stor.masque.style.position = 'absolute'
    stor.masque.style.left = (pos.left - posbase.left - 4) + 'px'
    stor.masque.style.top = (pos.top - posbase.top - 2 + me.zonesElts.MG.scrollTop) + 'px'
    stor.masque.style.background = '#00FFFF'
    stor.masque.style.border = '1px solid black'
    stor.masque.style.width = (pos.width + 4) + 'px'
    stor.masque.style.height = (pos.height - dec) + 'px'
    stor.masque.style.textAlign = 'center'
    stor.masque.style.verticalAlign = 'middle'
    stor.masque.style.cursor = 'pointer'
    stor.masque.style.opacity = 0.5
    j3pAffiche(stor.masque, null, '<b>?</b>')

    stor.leschoix = []
    // test si y’a un entier , ou 2
    if ((f1.den === 'z') || (f2.den === 'z')) {
      if ((f1.den === 'z') && (f2.den === 'z')) {
        /// AFAIRE !!!!  ya deux entiers
        switch (stor.calculeencours.prop[0]) {
          case '+': stor.leschoix = melange(stor.choicesAdd)
            break
          case '-': stor.leschoix = melange(stor.choicesMoins)
            break
          case 'x': stor.leschoix = melange(stor.choicesFois)
            break
          case '/': stor.leschoix = melange(stor.choicesDiv)
            break
        }
        stor.menuMasque = new MenuContextuel(stor.masque, stor.leschoix)
        return
      } else {
        /// AFAIRE !!!!  ya que 1 entier
        if (ds.Detail_Entiers) {
          // on ajoute une étape pour transormer en fraction
          if (f1.den === 'z') { buf = f1.num } else { buf = f2.num }
          switch (stor.calculeencours.prop[0]) {
            case '+':
              stor.leschoix = melange(stor.choicesTransAd)
              stor.menuMasque = new MenuContextuel(stor.masque, stor.leschoix)
              return
            case '-':
              stor.leschoix = melange(stor.choicesTransMoins)
              stor.menuMasque = new MenuContextuel(stor.masque, stor.leschoix)
              return
            case 'x':
              stor.leschoix = melange(stor.choicesTransFois)
              stor.leschoix[0].label = stor.leschoix[0].label.replace('£a', buf)
              stor.leschoix[1].label = stor.leschoix[1].label.replace('£a', buf)
              stor.leschoix[2].label = stor.leschoix[2].label.replace('£a', buf)
              stor.leschoix[3].label = stor.leschoix[3].label.replace('£a', buf)
              stor.menuMasque = new MenuContextuel(stor.masque, stor.leschoix)
              return
          }
        } else {
          // on transofrme en fraction sans le dire et on envoie à la suite
          if (f1.den === 'z') { stor.calculeencours.frac[0].den = 1 } else { stor.calculeencours.frac[1].den = 1 }
        }
      }
    }
    // pas d’entiers

    // test détails

    switch (stor.calculeencours.prop[0]) {
      case '+': stor.leschoix = melange(stor.choicesAdd)
        break
      case '-': stor.leschoix = melange(stor.choicesMoins)
        break
      case 'x': stor.leschoix = melange(stor.choicesFois)
        break
      case '/': stor.leschoix = melange(stor.choicesDiv)
        break
    }
    stor.menuMasque = new MenuContextuel(stor.masque, stor.leschoix)
  }

  function gereirre () {
    if (stor.zones !== undefined) {
      stor.zones.disable()
    }
    stor.zones = undefined
    stor.tabffi[1][0].innerHTML = ''
    if (stor.liste.reponse === 'non') {
      const yyt = addDefaultTable(stor.tabffi[1][0], 1, 2)
      j3pAffiche(yyt[0][0], null, 'Je simplifie par&nbsp;')
      stor.zones = new ZoneStyleMathquill1(yyt[0][1], { restric: '0123456789', limitenb: 1, limite: 6, collapse: true, hasAutoKeyboard: false, inverse: true, enter: me.sectionCourante.bind(me) })
      stor.zones.focus()
    }
  }
  function gereop2 () {
    stor.oucli = this
    stor.typop = this.typop
    stor.repEl = false
    clicprio(true)
  }
  function gereop () {
    stor.oucli = this
    stor.typop = this.typop
    stor.repEl = (this === stor.opat)
    clicprio()
  }
  function gerechoixad (s) {
    if (s === 'a') {
      stor.lechhh = 'Ajouter en haut et en bas'
      faisfoOpAd(1)
      return
    }
    if (s === 'rs') {
      faisRempEnt()
      return
    }
    const b = (stor.calculeencours.frac[0].den === stor.calculeencours.frac[1].den)
    if (s === 'g') {
      if (!b) {
        stor.lechhh = 'Garder le dénominateur puis ajouter'
        faisfoOpAd(2)
      } else {
        poseQuestion3('+1')
      }
    } else {
      if (b) {
        stor.lechhh = 'Réduire au même dénominateur'
        faisfoOpAd(3)
      } else {
        poseQuestion3('+2')
      }
    }
  }
  function faisRempEnt () {
    let f
    if (stor.calculeencours.frac[0].den === 'z') { f = stor.calculeencours.frac[0] } else { f = stor.calculeencours.frac[1] }
    stor.jkl = f
    // on crée la modale puis utilise j3pAffiche pour mettre du LaTeX dans le contenu
    const yy = j3pModale({ titre: 'Remplacement', contenu: '' })
    // FIXME SSS1 n’est jamais utilisé ! (ni SSS2 ni SSS3)
    j3pAffiche(yy, null, 'Remplacer $ ' + f.num + ' $ par  <span id ="SSS1"></span> ou  <span id ="SSS2"></span> ou  <span id ="SSS3"></span> ?')
    const wt = addDefaultTable(yy, 1, 7)
    j3pAffiche(wt[0][0], null, 'Remplacer $ ' + f.num + ' $ par  &nbsp;')
    j3pAffiche(wt[0][2], null, '&nbsp;,&nbsp;')
    j3pAffiche(wt[0][4], null, '&nbsp;ou&nbsp;')
    j3pAffiche(wt[0][6], null, '&nbsp;?')
    stor.b1 = new BoutonStyleMathquill(wt[0][1], 'BBBSSS1', '$\\frac{' + f.num + '}{ 1 }$', remp0, { taille: '20px', mathquill: true })
    stor.b2 = new BoutonStyleMathquill(wt[0][3], 'BBBSSS2', '$\\frac{' + f.num + '}{ ' + f.num + ' }$', foremp1, { taille: '20px', mathquill: true })
    stor.b3 = new BoutonStyleMathquill(wt[0][5], 'BBBSSS3', '$\\frac{ 1 }{' + stor.calculeencours.frac[0].num + '} $', foremp2, { taille: '20px', mathquill: true })
    /// /PB ICI
    yy.style.color = me.styles.toutpetit.correction.color
  }
  function faisfoOpAd (u) {
    stor.nbtente--
    stor.compteurPe2++
    let text
    switch (u) {
      case 1:text = 'Pour ajouter ou soustraire deux fractions, <br> il faut avoir le même dénominateur ! <br> On ajoute (ou soustrait) ensuite les numérateurs.'
        break
      case 2: text = 'Ces deux fractions n’ont pas le même dénominateur !'
        break
      case 3: text = 'Les deux fractions ont déjà le même dénominateur !'
    }
    stor.lastsaid = text
    if (stor.nbtente === 0) {
      stor.repEl = false
      j3pDetruit(stor.masque)
      stor.menuMasque.destroy()
      me.sectionCourante()
    } else {
      affModale(text)
    }
  }
  function gerechoixmoins (s) {
    if (s === 'a') {
      stor.lechhh = 'Soustraire en haut et en bas'
      faisfoOpMoins(1)
      return
    }
    if (s === 'rs') {
      faisRempEnt()
      return
    }
    const b = (stor.calculeencours.frac[0].den === stor.calculeencours.frac[1].den)
    if (s === 'g') {
      if (!b) {
        stor.lechhh = 'Garder le dénominateur puis soustraire'
        faisfoOpMoins(2)
      } else {
        poseQuestion3('-1')
      }
    } else {
      if (b) {
        stor.lechhh = 'Réduire au même dénominateur'
        faisfoOpMoins(3)
      } else {
        poseQuestion3('-2')
      }
    }
  }
  function faisfoOpMoins (u) {
    stor.nbtente--
    stor.compteurPe2++
    let text
    switch (u) {
      case 1:text = 'Pour ajouter ou soustraire deux fractions, <br> il faut avoir le même dénominateur ! <br> On ajoute (ou soustrait) ensuite les numérateurs.'
        break
      case 2: text = 'Ces deux fractions n’ont pas le même dénominateur !'
        break
      case 3: text = 'Les deux fractions ont déjà le même dénominateur !'
    }
    stor.lastsaid = text
    if (stor.nbtente === 0) {
      stor.repEl = false
      j3pDetruit(stor.masque)
      stor.menuMasque.destroy()
      me.sectionCourante()
    } else {
      affModale(text)
    }
  }
  function gerechoixfois (s) {
    if (s === '1') {
      stor.lechhh = 'Réduire au même dénominateur'
      faisfoOpfois(1)
      return
    }
    if (s === '3') {
      stor.lechhh = 'Multiplier par l’inverse'
      faisfoOpfois(2)
      return
    }
    if (s === '4') {
      faisfoOpfois(3)
      return
    }
    if (s === '2') { poseQuestion3('x') } else { poseQuestion3('xs') }
  }
  function faisfoOpfois (u) {
    stor.nbtente--
    stor.compteurPe3++
    let e
    let text
    switch (u) {
      case 1:text = 'Pas besoin de réduire au même dénominateur pour multiplier !'
        stor.lastsaid = 'Pour multiplier deux fractions, <br> on multiplie les numérateurs entre eux, <br> puis les dénominateurs entre eux.'
        break
      case 2: text = 'Inutile de passer par l’inverse pour multiplier !'
        stor.lastsaid = 'Pour multiplier deux fractions, <br> on multiplie les numérateurs entre eux, <br> puis les dénominateurs entre eux.'
        break
      case 3: text = 'Pour transformer un entier en fraction, il suffit de mettre $1$ au dénominateur !'
        if (stor.calculeencours.frac[0].den === 'z') { e = stor.calculeencours.frac[0].num } else { e = stor.calculeencours.frac[1].num }
        stor.lastsaid = 'Il faut multiplier le numérateur par $' + e + '$'
        break
    }
    if (stor.nbtente === 0) {
      stor.repEl = false
      j3pDetruit(stor.masque)
      stor.menuMasque.destroy()
      me.sectionCourante()
    } else {
      affModale(text)
    }
  }
  function gerechoixdiv (s) {
    if (s === '1') {
      stor.lechhh = 'Réduire au même dénominateur'
      faisfoOpdiv(1)
      return
    }
    if (s === '2') {
      stor.lechhh = 'Diviser en haut et en bas'
      faisfoOpdiv(2)
      return
    }
    poseQuestion3('/')
  }
  function faisfoOpdiv (u) {
    stor.nbtente--
    stor.compteurPe4++
    let text
    switch (u) {
      case 1:text = 'Pas besoin de réduire au même dénominateur pour diviser !'
        break
      case 2: text = 'On n’applique jamais cette méthode, pour éviter les virgules !'
    }
    stor.lastsaid = text
    if (stor.nbtente === 0) {
      stor.repEl = false
      j3pDetruit(stor.masque)
      stor.menuMasque.destroy()
      desactiveprio()
      me.sectionCourante()
    } else {
      affModale(text)
    }
  }
  function geresimple () {
    // fo vérifier que y’en a une de simplifiable
    const f1 = stor.calculeencours.frac[0]
    const f2 = stor.calculeencours.frac[1]
    const s1 = notsimpli(f1)
    const s2 = notsimpli(f2)
    if (!s1 && !s2) {
      stor.compteurPe5++
      stor.lechhh = 'Simplifier une fraction'
      faisfoopsimple(1)
      return
    }
    // si oui verifier que c’est une bonne idée
    if ((stor.calculeencours.prop[0] === '+') || (stor.calculeencours.prop[0] === '-')) {
      if (f1.den === f2.den) {
        stor.lechhh = 'Simplifier une fraction'
        faisfoopsimple(2)
        return
      }
    }
    demandelaquell()
  }
  function demandelaquell () {
    if (stor.calculeencours.frac[0].den === 'z') {
      simpli1()
      return
    }
    if (stor.calculeencours.frac[1].den === 'z') {
      simpli0()
      return
    }
    const yy = j3pModale({ titre: 'Information', contenu: '' })
    const wt = addDefaultTable('modale', 1, 7)
    j3pAffiche(wt[0][0], null, 'Simplifier&nbsp;')
    j3pAffiche(wt[0][2], null, '&nbsp;,&nbsp;')
    j3pAffiche(wt[0][4], null, '&nbsp;ou&nbsp;')
    j3pAffiche(wt[0][6], null, '&nbsp;?')
    stor.b1 = new BoutonStyleMathquill(wt[0][1], 'BBBSSS1', '$\\frac{' + stor.calculeencours.frac[0].num + '}{' + stor.calculeencours.frac[0].den + '}$', simpli0, { taille: '20px', mathquill: true })
    stor.b2 = new BoutonStyleMathquill(wt[0][3], 'BBBSSS2', '$\\frac{' + stor.calculeencours.frac[1].num + '}{' + stor.calculeencours.frac[1].den + '}$', simpli1, { taille: '20px', mathquill: true })
    stor.b3 = new BoutonStyleMathquill(wt[0][5], 'BBBSSS3', '$\\frac{' + stor.calculeencours.frac[0].num + '}{' + stor.calculeencours.frac[0].den + '}$  et $\\frac{' + stor.calculeencours.frac[1].num + '}{' + stor.calculeencours.frac[1].den + '}$', simpli10, { taille: '20px', mathquill: true })
    yy.style.color = me.styles.toutpetit.correction.color
  }
  function faisfoopsimple (m) {
    stor.nbtente--
    let text
    switch (m) {
      case 1:text = 'Aucune des deux ne peut être simplifiée !'
        stor.compteurPe5++
        break
      case 2: text = 'Ce n’est pas une bonne idée, tu as les mêmes dénominateurs !'
        break
      case 3: text = 'Cette fraction n’est pas simplifiable !'
        stor.compteurPe5++
        break
      case 4: text = 'Elles ne sont pas toutes les deux simplifiables !'
        stor.compteurPe5++
    }
    stor.lastsaid = text
    if (stor.nbtente === 0) {
      stor.repEl = false
      j3pDetruit(stor.masque)
      stor.menuMasque.destroy()
      me.sectionCourante()
    } else {
      affModale(text)
    }
  }
  function simpli0 () {
    j3pDetruit('modale')
    j3pDetruit('j3pmasque')
    const f = stor.calculeencours.frac[0]
    if (!notsimpli(f)) {
      stor.lechhh = 'Simplifier $\\frac{' + f.num + '}{' + f.den + '}$'
      faisfoopsimple(3)
      return
    }
    poseQuestion3('s0')
  }
  function simpli1 () {
    j3pDetruit('modale')
    j3pDetruit('j3pmasque')
    const f = stor.calculeencours.frac[1]
    if (!notsimpli(f)) {
      stor.lechhh = 'Simplifier $\\frac{' + f.num + '}{' + f.den + '}$'
      faisfoopsimple(3)
      return
    }
    poseQuestion3('s1')
  }
  function simpli10 () {
    j3pDetruit('modale')
    j3pDetruit('j3pmasque')
    const f = stor.calculeencours.frac[0]
    const f2 = stor.calculeencours.frac[1]
    if (!notsimpli(f) || !notsimpli(f2)) {
      if (!notsimpli(f)) {
        stor.lechhh = 'Simplifier $\\frac{' + f.num + '}{' + f.den + '}$'
      } else {
        stor.lechhh = 'Simplifier $\\frac{' + f2.num + '}{' + f2.den + '}$'
      }
      faisfoopsimple(4)
      return
    }
    poseQuestion3('s10')
  }
  function affRedFo (s) {
    stor.nbtente--
    stor.compteurPe5++
    const mg = pgcd(Math.abs(stor.calculeencours.frac[0].num), Math.abs(stor.calculeencours.frac[0].den))
    let text
    switch (s) {
      case 1: if (stor.liste.reponse === 'oui') {
        text = 'La fraction n’est pas irréductible, on peut la simplifier !'
        stor.lastsaid = 'On peut simplifier cette fraction par ' + mg + ' !'
      } else {
        stor.lastsaid = 'On ne peut pas simplifier ici !'
        text = stor.lastsaid
      }
        break
      case 2: text = 'Le numérateur et le dénominateur ne sont pas tous les deux dans la table de ' + stor.zones.reponse() + '  !'
        stor.lastsaid = 'On peut simplifier cette fraction par ' + mg + ' !'
    }
    if (stor.nbtente === 0) {
      stor.repEl = false
      desactiveRedduc(true)
      stor.etat = 'fin'
      me.sectionCourante()
    } else {
      affModale(text)
    }
  }
  function desactiveRedduc (a) {
    stor.liste.disable()
    if (a) {
      stor.liste.corrige(false)
      stor.liste.barre()
      if (stor.zones !== undefined) {
        stor.zones.disable()
        stor.zones.corrige(false)
        stor.zones.barre()
      }
    } else {
      stor.zones.corrige(false)
      stor.zones.disable()
    }
  }
  function afficheNb (where, frac, b) {
    let p1 = ''
    let p2 = ''
    if (b === 'p' && frac.num < 0) {
      p1 = ' ('
      p2 = ') '
    }
    const aaf = afficheFrac(where)
    aaf[1].style.display = 'none'
    switch (b) {
      case 'b':
        j3pAffiche(aaf[0], null, '$' + p1 + frac.num + p2 + '$')
        j3pAffiche(aaf[2], null, '$ \\text{ }$')
        break
      case 'h':
        j3pAffiche(aaf[2], null, '$' + p1 + frac.num + p2 + '$')
        j3pAffiche(aaf[0], null, '$ \\text{ }$')
        break
      default:
        j3pAffiche(aaf[0], null, '$' + p1 + frac.num + p2 + '$')
        aaf[2].style.display = 'none'
        break
    }
  }
  function resize () {
    for (let i = 0; i < stor.ppppar.length; i++) {
      if (stor.ppppar[i].num === null) continue
      if (stor.ppppar[i].den === null) continue
      const t = Math.max(stor.ppppar[i].num.offsetHeight, stor.ppppar[i].den.offsetHeight)
      stor.ppppar[i].num.style.height = t + 'px'
      stor.ppppar[i].den.style.height = t + 'px'
    }
  }

  /**
   * Initialise le tableau avec ses 3 cellules (ou 5 avec les parenthèses)
   * @private
   * @param {string} id
   * @param {Object} [options]
   * @param {string} [options.addParentheses] Passer true pour ajouter les cellules de parenthèse autour de nos 3 cellules habituelles
   * @param {boolean} [options.boll] C’est quoi ?
   * @return {HTMLTableDataCellElement[]} Les 3 cellules (n1, op et n2)
   */
  function affichefrac2 (where, frac, boll) {
    const ttt = afficheFrac(where)
    if (frac !== false) {
      j3pAffiche(ttt[0], null, '$' + frac.num + '$')
      j3pAffiche(ttt[2], null, '$' + frac.den + '$')
    }
    ttt[1].typop = '/'
    if (boll) {
      stor.listeadza.push({ frac: true, id: ttt[1] })
      ttt[1].addEventListener('click', gereop2, false)
      ttt[1].style.cursor = 'pointer'
      ttt[1].setAttribute('class', 'fracdiv')
    }
    stor.ppppar.push({ num: ttt[0], den: ttt[2] })
    return ttt
  }
  function affichediv2 (where, boll) {
    const ttt = afficheFrac(where)
    ttt[1].typop = '/'
    if (boll) {
      stor.listeadza.push({ frac: true, id: ttt[1] })
      ttt[1].addEventListener('click', gereop, false)
      ttt[1].style.cursor = 'pointer'
      ttt[1].setAttribute('class', 'fracdiv')
    }
    stor.ppppar.push({ num: ttt[0], den: ttt[2] })
    return ttt
  }
  function afficheplus2 (where, boll) {
    const ttt = afficheplus(where)
    ttt[1].typop = '+'
    if (boll) {
      stor.listeadza.push({ frac: false, id: ttt[1] })
      ttt[1].addEventListener('click', gereop, false)
      ttt[1].style.cursor = 'pointer'
      ttt[1].setAttribute('class', 'opautre')
    }
    return ttt
  }
  function afficheplus2par2 (where, boll) {
    const ttt = afficheplusPar(where)
    ttt[1].typop = '+'
    if (boll) {
      stor.listeadza.push({ frac: false, id: ttt[1] })
      ttt[1].addEventListener('click', gereop, false)
      ttt[1].style.cursor = 'pointer'
      ttt[1].setAttribute('class', 'opautre')
    }
    return ttt
  }
  function affichemoins2 (where, boll) {
    const ttt = affichemoins(where)
    ttt[1].typop = '-'
    if (boll) {
      stor.listeadza.push({ frac: false, id: ttt[1] })
      ttt[1].addEventListener('click', gereop, false)
      ttt[1].style.cursor = 'pointer'
      ttt[1].setAttribute('class', 'opautre')
    }
    return ttt
  }
  function affichemoins2par2 (where, boll) {
    const ttt = affichemoinsPar(where)
    ttt[1].typop = '-'
    if (boll) {
      stor.listeadza.push({ frac: false, id: ttt[1] })
      ttt[1].addEventListener('click', gereop, false)
      ttt[1].style.cursor = 'pointer'
      ttt[1].setAttribute('class', 'opautre')
    }
    return ttt
  }
  function affichefois2 (where, boll) {
    const ttt = affichefois(where)
    ttt[1].typop = '*'
    if (boll) {
      stor.listeadza.push({ frac: false, id: ttt[1] })
      ttt[1].addEventListener('click', gereop, false)
      ttt[1].style.cursor = 'pointer'
      ttt[1].setAttribute('class', 'opautre')
    }
    return ttt
  }

  function coop () {
    let n1
    let n2
    let n3
    let n4
    let n5
    let n6
    let n7
    let n8
    let num2
    let den1
    let den2
    let f1
    let f2
    let needSign = false
    let ok = true
    let okbis = true
    let okbis2 = true
    const num1 = stor.calculeencours.frac[0].num
    den1 = stor.calculeencours.frac[0].den
    num2 = stor.calculeencours.frac[1].num
    den2 = stor.calculeencours.frac[1].den
    switch (stor.etat) {
      case '+4':
        n1 = parseFloat2(stor.zone[0].reponse())
        n2 = parseFloat2(stor.zone[1].reponse())
        if (n1 !== n2) {
          stor.compteurPe2++
          faisFoCalculNb('Tu dois multiplier le dénominateur et le numérateur d’une fraction par le même nombre !')
          ok = false
        }
        if (ok && (n1 === 0)) {
          stor.compteurPe2++
          faisFoCalculNb('Tu ne peux pas multiplier par 0 !')
          ok = false
        }
        if (ok && (n2 * den2 !== den1)) {
          stor.compteurPe2++
          faisFoCalculNb('Tu n’obtiendras pas la bon dénominateur avec ce coefficient !')
          ok = false
        }
        break
      case '+3':
        n1 = parseFloat2(stor.zone[0].reponse())
        n2 = parseFloat2(stor.zone[1].reponse())
        if (n1 !== n2) {
          stor.compteurPe2++
          faisFoCalculNb('Tu dois multiplier le dénominateur et le numérateur d’une fraction par le même nombre !')
          ok = false
        }
        if (ok && (n1 === 0)) {
          stor.compteurPe2++
          faisFoCalculNb('Tu ne peux pas multiplier par 0 !')
          ok = false
        }
        if (ok && (n2 * den1 !== den2)) {
          stor.compteurPe2++
          faisFoCalculNb('Tu n’obtiendras pas la bon dénominateur avec ce coefficient !')
          ok = false
        }
        break
      case '+2':
        n1 = parseFloat2(stor.zone[0].reponse())
        n2 = parseFloat2(stor.zone[1].reponse())
        n3 = parseFloat2(stor.zone[2].reponse())
        n4 = parseFloat2(stor.zone[3].reponse())
        if ((n1 !== n2) ||
          (n3 !== n4)) {
          stor.compteurPe2++
          faisFoCalculNb('Tu dois multiplier le dénominateur et le numérateur d’une fraction par le même nombre !')
          ok = false
        }
        if (ok && ((n1 === 0) || (n3 === 0))) {
          stor.compteurPe2++
          faisFoCalculNb('Tu ne peux pas multiplier par 0 !')
          ok = false
        }
        if (ok && (n2 * den1 !== n4 * den2)) {
          stor.compteurPe2++
          faisFoCalculNb('Les nombres que tu as donné ne donnent pas le même dénominateur !')
          ok = false
        }
        stor.cons = [n2, n4]
        break
      case '+1':
        n1 = parseFloat2(stor.zone[0].reponse())
        n2 = parseFloat2(stor.zone[1].reponse())
        n3 = parseFloat2(stor.zone[2].reponse())
        if (n3 === den1 + den2) {
          stor.compteurPe2++
          faisFoCalculNb('Tu ne dois pas ajouter les dénominateurs !')
          ok = false
        }
        if (ok && (n3 !== den1)) {
          stor.compteurPe2++
          faisFoCalculNb('On garde le dénominateur !')
          ok = false
        }
        if (ok) {
          if (n1 !== num1) {
            if (n2 !== num1) {
              okbis = false
            } else {
              if (n1 !== num2) {
                okbis = false
              }
            }
          } else {
            if (n2 !== num2) {
              okbis = false
            }
          }
          if (!okbis) {
            faisFoCalculNb('Il y a une erreur au numérateur !')
            ok = false
          }
          if (ok && ((n2 < 0) && (stor.zone[1].reponse().indexOf('(') === -1))) {
            faisFoCalculNb('Il manque des parenthèses après le <b>+</b> !')
            ok = false
          }
          if (ok && ((n2 < 0))) {
            stor.cons2 = [n1, n2]
            needSign = true
          }
        }
        break
      case '-4':
        n1 = parseFloat2(stor.zone[0].reponse())
        n2 = parseFloat2(stor.zone[1].reponse())
        if (n1 !== n2) {
          stor.compteurPe2++
          faisFoCalculNb('Tu dois multiplier le dénominateur et le numérateur d’une fraction par le même nombre !')
          ok = false
        }
        if (ok && (n1 === 0)) {
          stor.compteurPe2++
          faisFoCalculNb('Tu ne peux pas multiplier par 0 !')
          ok = false
        }
        if (ok && (n2 * den2 !== den1)) {
          stor.compteurPe2++
          faisFoCalculNb('Tu n’obtiendras pas la bon dénominateur avec ce coefficient !')
          ok = false
        }
        break
      case '-3':
        n1 = parseFloat2(stor.zone[0].reponse())
        n2 = parseFloat2(stor.zone[1].reponse())
        if (n1 !== n2) {
          stor.compteurPe2++
          faisFoCalculNb('Tu dois multiplier le dénominateur et le numérateur d’une fraction par le même nombre !')
          ok = false
        }
        if (ok && (n1 === 0)) {
          stor.compteurPe2++
          faisFoCalculNb('Tu ne peux pas multiplier par 0 !')
          ok = false
        }
        if (ok && (n2 * den1 !== den2)) {
          stor.compteurPe2++
          faisFoCalculNb('Tu n’obtiendras pas la bon dénominateur avec ce coefficient !')
          ok = false
        }
        break
      case '-2':
        n1 = parseFloat2(stor.zone[0].reponse())
        n2 = parseFloat2(stor.zone[1].reponse())
        n3 = parseFloat2(stor.zone[2].reponse())
        n4 = parseFloat2(stor.zone[3].reponse())
        if ((n1 !== n2) ||
          (n3 !== n4)) {
          stor.compteurPe2++
          faisFoCalculNb('Tu dois multiplier le dénominateur et le numérateur d’une fraction par le même nombre !')
          ok = false
        }
        if (ok && ((n1 === 0) || (n3 === 0))) {
          stor.compteurPe2++
          faisFoCalculNb('Tu ne peux pas multiplier par 0 !')
          ok = false
        }
        if (ok && (n2 * den1 !== n4 * den2)) {
          stor.compteurPe2++
          faisFoCalculNb('Les nombres que tu as donné ne donnent pas le même dénominateur !')
          ok = false
        }
        stor.cons = [n2, n4]
        break
      case '-1':
        n1 = parseFloat2(stor.zone[0].reponse())
        n2 = parseFloat2(stor.zone[1].reponse())
        n3 = parseFloat2(stor.zone[2].reponse())
        if (n3 === den1 + den2) {
          stor.compteurPe2++
          faisFoCalculNb('Tu ne dois pas ajouter les dénominateurs !')
          ok = false
        }
        if (n3 === 0) {
          stor.compteurPe2++
          faisFoCalculNb('Tu ne dois pas soustraire les dénominateurs !')
          ok = false
        }
        if (ok && (n3 !== den1)) {
          stor.compteurPe2++
          faisFoCalculNb('On garde le dénominateur !')
          ok = false
        }
        if (ok) {
          if (n1 !== num1) {
            if (n2 !== num1) {
              okbis = false
            } else {
              if (n1 !== num2) {
                okbis = false
              }
            }
          } else {
            if (n2 !== num2) {
              okbis = false
            }
          }
          if (!okbis) {
            faisFoCalculNb('Il y a une erreur aux numérateurs !')
            ok = false
          }
          if (ok && ((n2 < 0) && (stor.zone[1].reponse().indexOf('(') === -1))) {
            faisFoCalculNb('Il manque des parenthèses après le <b>-</b> !')
            ok = false
          }
        }
        if (ok && ((n2 < 0))) {
          stor.cons2 = [n1, n2]
          needSign = true
        }
        break
      case 'x':
        n1 = parseFloat2(stor.zone[0].reponse())
        n2 = parseFloat2(stor.zone[1].reponse())
        n3 = parseFloat2(stor.zone[2].reponse())
        n4 = parseFloat2(stor.zone[3].reponse())

        if (j3pArrondi((n1 * n2) / (n3 * n4), 7) !== j3pArrondi((num1 * num2) / (den1 * den2), 7)) {
          ok = false
          stor.compteurPe3++
          faisFoCalculNb('Il y a un problème de calcul !')
        }
        if (ok && ((n2 < 0) && (stor.zone[1].reponse().indexOf('(') === -1))) {
          faisFoCalculNb('Il manque des parenthèses après le <b>x</b> au numérateur !')
          ok = false
        }
        if (ok && ((n4 < 0) && (stor.zone[3].reponse().indexOf('(') === -1))) {
          faisFoCalculNb('Il manque des parenthèses après le <b>x</b> au dénominateur !')
          ok = false
        }
        stor.cons = [n1, n2, n3, n4]
        break
      case 'xs':
        n1 = parseFloat2(stor.zone[0].reponse())
        n2 = parseFloat2(stor.zone[1].reponse())
        n3 = parseFloat2(stor.zone[2].reponse())
        if (den1 === 'z') { den1 = den2 }
        if (j3pArrondi((n1 * n2) / n3, 7) !== j3pArrondi((num1 * num2) / den1, 7)) {
          ok = false
          stor.compteurPe3++
          faisFoCalculNb('Il y a un problème de calcul !')
        }
        if (ok && ((n2 < 0) && (stor.zone[1].reponse().indexOf('(') === -1))) {
          faisFoCalculNb('Il manque des parenthèses après le <b>x</b> au numérateur !')
          ok = false
        }
        stor.cons = [n1, n2, n3]
        break
      case '/':
        if (!stor.listebool[0]) {
          den2 = den1
          num2 = num1
        }
        if (num2 === 'z') { num2 = 1 }
        n1 = parseFloat2(stor.zone[0].reponse())
        n2 = parseFloat2(stor.zone[1].reponse())
        if (j3pArrondi(n1 / n2, 7) !== j3pArrondi(den2 / num2, 7)) {
          stor.compteurPe4++
          faisFoCalculNb('Il faut échanger numérateurs et dénominateurs !')
          ok = false
        }
        if (ok && (n2 < 0)) {
          n1 = -n1
          n2 = -n2
          affModaleInfo('Je modifie un peu ta réponse <br> pour éviter les signes - au dénominateur, <br> c’est plus pratique.')
        }
        if (ok) {
          stor.cons = [n1, n2]
        }
        break
      case 's0':
        f1 = stor.calculeencours.frac[0]
        n1 = parseFloat2(stor.zone[0].reponse())
        n2 = parseFloat2(stor.zone[1].reponse())
        n3 = parseFloat2(stor.zone[2].reponse())
        n4 = parseFloat2(stor.zone[3].reponse())
        if (n1 * n2 !== f1.num) {
          ok = false
          faisFoCalculNb('Il y a une erreur au numérateur !')
        }
        if (ok && (n3 * n4 !== f1.den)) {
          ok = false
          faisFoCalculNb('Il y a une erreur au dénominateur !')
        }
        if (ok && ((n2 < 0) && (stor.zone[1].reponse().indexOf('(') === -1))) {
          faisFoCalculNb('Il manque des parenthèses après le <b>x</b> au numérateur !')
          ok = false
        }
        if (ok && ((n4 < 0) && (stor.zone[3].reponse().indexOf('(') === -1))) {
          faisFoCalculNb('Il manque des parenthèses après le <b>x</b> au dénominateur !')
          ok = false
        }
        okbis = ((n1 === n3) || (n1 === n4))
        okbis2 = ((n2 === n3) || (n2 === n4))
        if (ok && (!okbis && !okbis2)) {
          faisFoCalculNb('IL devrait y avoir le même nombre en haut et en bas pour pouvoir simplifier !')
          ok = false
        }
        if (ok && (okbis && (n1 === 1) && !okbis2)) {
          faisFoCalculNb('Il ne sert à rien de simplifier par 1 !')
          ok = false
        }
        if (ok && (okbis2 && (n2 === 1) && !okbis)) {
          faisFoCalculNb('Il ne sert à rien de simplifier par 1 !')
          ok = false
        }
        if (okbis) { stor.cons = [n1] }
        if (okbis2 && (n2 !== 1)) { stor.cons = [n2] }
        break
      case 's1':
        f1 = stor.calculeencours.frac[1]
        n1 = parseFloat2(stor.zone[0].reponse())
        n2 = parseFloat2(stor.zone[1].reponse())
        n3 = parseFloat2(stor.zone[2].reponse())
        n4 = parseFloat2(stor.zone[3].reponse())
        if (n1 * n2 !== f1.num) {
          ok = false
          faisFoCalculNb('Il y a une erreur au numérateur !')
        }
        if (ok && (n3 * n4 !== f1.den)) {
          ok = false
          faisFoCalculNb('Il y a une erreur au dénominateur !')
        }
        if (ok && ((n2 < 0) && (stor.zone[1].reponse().indexOf('(') === -1))) {
          faisFoCalculNb('Il manque des parenthèses après le <b>x</b> au numérateur !')
          ok = false
        }
        if (ok && ((n4 < 0) && (stor.zone[3].reponse().indexOf('(') === -1))) {
          faisFoCalculNb('Il manque des parenthèses après le <b>x</b> au dénominateur !')
          ok = false
        }
        okbis = ((n1 === n3) || (n1 === n4))
        okbis2 = ((n2 === n3) || (n2 === n4))
        if (ok && (!okbis && !okbis2)) {
          faisFoCalculNb('IL devrait y avoir le même nombre en haut et en bas pour pouvoir simplifier !')
          ok = false
        }
        if (ok && (okbis && (n1 === 1) && !okbis2)) {
          faisFoCalculNb('Il ne sert à rien de simplifier par 1 !')
          ok = false
        }
        if (ok && (okbis2 && (n2 === 1) && !okbis)) {
          faisFoCalculNb('Il ne sert à rien de simplifier par 1 !')
          ok = false
        }
        if (okbis) { stor.cons = [n1] }
        if (okbis2 && (n2 !== 1)) { stor.cons = [n2] }
        break
      case 's10':
        f1 = stor.calculeencours.frac[0]
        n1 = parseFloat2(stor.zone[0].reponse())
        n2 = parseFloat2(stor.zone[1].reponse())
        n3 = parseFloat2(stor.zone[2].reponse())
        n4 = parseFloat2(stor.zone[3].reponse())
        f2 = stor.calculeencours.frac[1]
        n5 = parseFloat2(stor.zone[4].reponse())
        n6 = parseFloat2(stor.zone[5].reponse())
        n7 = parseFloat2(stor.zone[6].reponse())
        n8 = parseFloat2(stor.zone[7].reponse())

        if (n1 * n2 !== f1.num) {
          ok = false
          faisFoCalculNb('Il y a une erreur au numérateur !')
        }
        if (ok && (n3 * n4 !== f1.den)) {
          ok = false
          faisFoCalculNb('Il y a une erreur au dénominateur !')
        }
        if (ok && ((n2 < 0) && (stor.zone[1].reponse().indexOf('(') === -1))) {
          faisFoCalculNb('Il manque des parenthèses après le <b>x</b> au numérateur !')
          ok = false
        }
        if (ok && ((n4 < 0) && (stor.zone[3].reponse().indexOf('(') === -1))) {
          faisFoCalculNb('Il manque des parenthèses après le <b>x</b> au dénominateur !')
          ok = false
        }
        okbis = ((n1 === n3) || (n1 === n4))
        okbis2 = ((n2 === n3) || (n2 === n4))
        if (ok && (okbis && (n1 === 1) && !okbis2)) {
          faisFoCalculNb('Il ne sert à rien de simplifier par 1 !')
          ok = false
        }
        if (ok && (okbis2 && (n2 === 1) && !okbis)) {
          faisFoCalculNb('Il ne sert à rien de simplifier par 1 !')
          ok = false
        }
        if (okbis) { stor.cons = [n1] }
        if (okbis2 && (n2 !== 1)) { stor.cons = [n2] }
        if (ok && (!okbis && !okbis2)) {
          faisFoCalculNb('IL devrait y avoir le même nombre en haut et en bas pour pouvoir simplifier !')
          ok = false
        }

        if (ok && (n5 * n6 !== f2.num)) {
          ok = false
          faisFoCalculNb('Il y a une erreur au numérateur !')
        }
        if (ok && (n7 * n8 !== f2.den)) {
          ok = false
          faisFoCalculNb('Il y a une erreur au dénominateur !')
        }
        if (ok && ((n6 < 0) && (stor.zone[5].reponse().indexOf('(') === -1))) {
          faisFoCalculNb('Il manque des parenthèses après le <b>x</b> au numérateur !')
          ok = false
        }
        if (ok && ((n8 < 0) && (stor.zone[7].reponse().indexOf('(') === -1))) {
          faisFoCalculNb('Il manque des parenthèses après le <b>x</b> au dénominateur !')
          ok = false
        }
        okbis = ((n5 === n7) || (n5 === n8))
        okbis2 = ((n6 === n7) || (n6 === n8))
        if (ok && (!okbis && !okbis2)) {
          faisFoCalculNb('IL devrait y avoir le même nombre en haut et en bas pour pouvoir simplifier !')
          ok = false
        }
        if (ok && (okbis && (n5 === 1) && !okbis2)) {
          faisFoCalculNb('Il ne sert à rien de simplifier par 11 !')
          ok = false
        }
        if (ok && (okbis2 && (n6 === 1) && !okbis)) {
          faisFoCalculNb('Il ne sert à rien de simplifier par 12 !')
          ok = false
        }
        if (okbis && (n1 !== 1)) { stor.cons.push(n5) } else { stor.cons.push(n6) }
        break
    }
    if (ok && ((stor.etat === 'x') || (stor.etat === 'xs')) && (ds.Simpl_Mult)) {
      poseSimplMult()
      return
    }
    if (needSign && ds.Detail_Signe) {
      GereSign()
      return
    }
    if (ok) { poseLastquest() }
    resize()
  }
  function GereSign () {
    stor.lechhh = ''
    stor.lalala = false
    stor.zzzSigne = true
    desactivezone(true)
    stor.op = []
    addRow(stor.lesdiv.tablo)
    stor.lerepere = affichetoutlecalc(false)[1].parentNode
    j3pEmpty(stor.lerepere)
    const t1 = affichefrac2(stor.lerepere, false, false)
    j3pAffiche(t1[2], null, '$' + stor.calculeencours.frac[0].den + '$')
    const utab = addDefaultTable(t1[0], 1, 3)
    j3pAffiche(utab[0][0], null, '$' + stor.cons2[0] + '$')
    j3pAffiche(utab[0][2], null, '$' + (-stor.cons2[1]) + '$')
    stor.liste = ListeDeroulante.create(utab[0][1], ['---', '+', '-'], { mathquill: true, boutonAgauche: true, centre: true })
    stor.lesdiv.etape.innerHTML = ''
    j3pAffiche(stor.lesdiv.etape, null, '<b><u>Étape</u>:</b> Supprimer des parenthèses.')
    me.afficheBoutonValider()
    resize()
    stor.sauteEtape = false
  }

  function coop2 () {
    let n1
    let n2
    let n3
    let n4
    let den1
    let den2
    let at1
    let at2
    let ok = true
    let okbis = true
    const num1 = stor.calculeencours.frac[0].num
    den1 = stor.calculeencours.frac[0].den
    const num2 = stor.calculeencours.frac[1].num
    den2 = stor.calculeencours.frac[1].den

    switch (stor.etat) {
      case '+4':
        n1 = parseFloat2(stor.zone[0].reponse())
        n2 = parseFloat2(stor.zone[1].reponse())
        if (n2 !== den1) {
          faisFoCalculNb('Tu devrais obtenir le même dénominateur !')
          ok = false
        }
        if (ok && (n1 !== Math.round(den1 / den2 * num2))) {
          faisFoCalculNb('Il y a une erreur au numérateur !')
          ok = false
        }
        if (ok) {
          stor.calculeencours.frac[1] = { num: n1, den: n2 }
        }
        break
      case '+3':
        n1 = parseFloat2(stor.zone[0].reponse())
        n2 = parseFloat2(stor.zone[1].reponse())
        if (n2 !== den2) {
          faisFoCalculNb('Tu devrais obtenir le même dénominateur !')
          ok = false
        }
        if (ok && (n1 !== Math.round(den2 / den1 * num1))) {
          faisFoCalculNb('Il y a une erreur au numérateur !')
          ok = false
        }
        if (ok) {
          stor.calculeencours.frac[0] = { num: n1, den: n2 }
        }
        break
      case '+2':

        n1 = parseFloat2(stor.zone[0].reponse())
        n2 = parseFloat2(stor.zone[1].reponse())
        n3 = parseFloat2(stor.zone[2].reponse())
        n4 = parseFloat2(stor.zone[3].reponse())
        if (n2 !== n4) {
          faisFoCalculNb('Tu devrais obtenir le même dénominateur !')
          ok = false
        }
        if ((n2 !== 0 && n4 !== 0) && (n1 * den1 === n2 * num1) && (n3 * den2 === n4 * num2)) {
          stor.calculeencours.frac[1] = { num: n1, den: n2 }
          stor.calculeencours.frac[0] = { num: n3, den: n4 }
        } else {
          if (ok && ((Math.abs(n2) % Math.abs(den1) !== 0) || (Math.abs(n2) % Math.abs(den2) !== 0))) {
            faisFoCalculNb('Le dénominateur est faux !')
            ok = false
          }
          if (ok) {
            at1 = num1 * (n2 / den1)
            at2 = num2 * (n2 / den2)
            if (n1 === at1) {
              if (n3 !== at2) {
                okbis = false
              }
            } else {
              if ((n1 !== at2) || (n3 !== at1)) {
                okbis = false
              }
            }
          }
          if (!okbis) {
            faisFoCalculNb('Il y a une erreur aux numérateurs !')
            ok = false
          }
          if (ok) {
            stor.calculeencours.frac[1] = { num: n1, den: n2 }
            stor.calculeencours.frac[0] = { num: n3, den: n4 }
          }
        }
        break
      case '+1':
        n1 = parseFloat2(stor.zone[0].reponse())
        n2 = parseFloat2(stor.zone[1].reponse())
        if (n2 === den1 + den2) {
          faisFoCalculNb('Tu ne dois pas ajouter les dénominateurs !')
          ok = false
        }
        if (ok && (n2 !== den1)) {
          faisFoCalculNb('On garde le dénominateur !')
          ok = false
        }
        if (ok && (n1 !== num1 + num2)) {
          faisFoCalculNb('Il y a une erreur au numérateur !')
          ok = false
        }
        if (ok) {
          stor.calculeencours.frac.splice(0, 1)
          stor.calculeencours.frac[0] = { num: n1, den: n2 }
          stor.calculeencours.prop = stor.calculeencours.prop.substring(1)
          stor.listepar.splice(0, 1)
          stor.listebool.splice(0, 1)
        }
        break
      case '-4':
        n1 = parseFloat2(stor.zone[0].reponse())
        n2 = parseFloat2(stor.zone[1].reponse())
        if (n2 !== den1) {
          faisFoCalculNb('Tu devrais obtenir le même dénominateur !')
          ok = false
        }
        if (ok && (n1 !== Math.round(den1 / den2 * num2))) {
          faisFoCalculNb('Il y a une erreur au numérateur !')
          ok = false
        }
        if (ok) {
          stor.calculeencours.frac[1] = { num: n1, den: n2 }
        }
        break
      case '-3':
        n1 = parseFloat2(stor.zone[0].reponse())
        n2 = parseFloat2(stor.zone[1].reponse())
        if (n2 !== den2) {
          faisFoCalculNb('Tu devrais obtenir le même dénominateur !')
          ok = false
        }
        if (ok && (n1 !== Math.round(den2 / den1 * num1))) {
          faisFoCalculNb('Il y a une erreur au numérateur !')
          ok = false
        }
        if (ok) {
          stor.calculeencours.frac[0] = { num: n1, den: n2 }
        }
        break
      case '-2':
        n1 = parseFloat2(stor.zone[0].reponse())
        n2 = parseFloat2(stor.zone[1].reponse())
        n3 = parseFloat2(stor.zone[2].reponse())
        n4 = parseFloat2(stor.zone[3].reponse())
        if (n2 !== n4) {
          faisFoCalculNb('Tu devrais obtenir le même dénominateur !')
          ok = false
        }
        if ((n2 !== 0 && n4 !== 0) && (n1 * den1 === n2 * num1) && (n3 * den2 === n4 * num2)) {
          stor.calculeencours.frac[0] = { num: n1, den: n2 }
          stor.calculeencours.frac[1] = { num: n3, den: n4 }
        } else {
          if (ok && ((Math.abs(n2) % Math.abs(den1) !== 0) || (Math.abs(n2) % Math.abs(den2) !== 0))) {
            faisFoCalculNb('Le dénominateur est faux !')
            ok = false
          }
          if (ok) {
            at1 = num1 * (n2 / den1)
            at2 = num2 * (n2 / den2)
            if (n1 === at1) {
              if (n3 !== at2) {
                okbis = false
              }
            } else {
              if ((n1 !== at2) || (n3 !== at1)) {
                okbis = false
              }
            }
          }
          if (!okbis) {
            faisFoCalculNb('Il y a une erreur aux numérateurs !')
            ok = false
          }
          if (ok) {
            stor.calculeencours.frac[1] = { num: n1, den: n2 }
            stor.calculeencours.frac[0] = { num: n3, den: n4 }
          }
        }
        break
      case '-1':
        n1 = parseFloat2(stor.zone[0].reponse())
        n2 = parseFloat2(stor.zone[1].reponse())
        if (n2 === den1 + den2) {
          faisFoCalculNb('Tu ne dois pas ajouter les dénominateurs !')
          ok = false
        }
        if (ok && (n2 !== den1)) {
          faisFoCalculNb('On garde le dénominateur !')
          ok = false
        }
        if (stor.listebool[0]) {
          if (ok && (n1 !== num1 - num2)) {
            faisFoCalculNb('Il y a une erreur au numérateur !')
            ok = false
          }
        } else {
          if (ok && (n1 !== num2 - num1)) {
            faisFoCalculNb('Il y a une erreur au numérateur !')
            ok = false
          }
        }
        if (ok) {
          stor.calculeencours.frac.splice(0, 1)
          stor.calculeencours.frac[0] = { num: n1, den: n2 }
          stor.calculeencours.prop = stor.calculeencours.prop.substring(1)
          stor.listepar.splice(0, 1)
          stor.listebool.splice(0, 1)
        }
        break
      case 'xs':
        n1 = parseFloat2(stor.zone[0].reponse())
        n2 = parseFloat2(stor.zone[1].reponse())
        if (den1 === 'z') den1 = den2
        if (j3pArrondi(n1 / n2, 7) !== j3pArrondi((num1 * num2) / den1, 7)) {
          faisFoCalculNb('Il y a une erreur !')
          ok = false
        }
        if (ok) {
          stor.calculeencours.frac.splice(0, 1)
          stor.calculeencours.frac[0] = { num: n1, den: n2 }
          stor.calculeencours.prop = stor.calculeencours.prop.substring(1)
          stor.listepar.splice(0, 1)
          stor.listebool.splice(0, 1)
        }
        break
      case 'x':
        n1 = parseFloat2(stor.zone[0].reponse())
        n2 = parseFloat2(stor.zone[1].reponse())
        if (den1 === 'z') den1 = 1
        if (den2 === 'z') den2 = 1
        if (j3pArrondi(n1 / n2, 7) !== j3pArrondi((num1 * num2) / (den1 * den2), 7)) {
          faisFoCalculNb('Il y a une erreur !')
          ok = false
        }
        if (ok) {
          stor.calculeencours.frac.splice(0, 1)
          stor.calculeencours.frac[0] = { num: n1, den: n2 }
          stor.calculeencours.prop = stor.calculeencours.prop.substring(1)
          stor.listepar.splice(0, 1)
          stor.listebool.splice(0, 1)
        }
        break
      case 's0':
        n1 = parseFloat2(stor.zone[0].reponse())
        n2 = parseFloat2(stor.zone[1].reponse())
        if (Math.abs(num1) <= Math.abs(n1)) {
          ok = false
          faisFoCalculNb('Les valeurs numériques devraient être plus petites !')
        }
        if (ok && (Math.abs(den1) <= Math.abs(n2))) {
          ok = false
          faisFoCalculNb('Les valeurs numériques devraient être plus petites !')
        }
        if (ok && (j3pArrondi(num1 / den1, 6) !== j3pArrondi(n1 / n2, 6))) {
          ok = false
          faisFoCalculNb('La simplifiction est fausse !')
        }
        if (ok) {
          stor.calculeencours.frac[0] = { num: n1, den: n2 }
        }
        break
      case 's1':
        n1 = parseFloat2(stor.zone[0].reponse())
        n2 = parseFloat2(stor.zone[1].reponse())
        if (Math.abs(num2) <= Math.abs(n1)) {
          ok = false
          faisFoCalculNb('Les Valeurs numériques devraient être plus petites !')
        }
        if (ok && (Math.abs(den2) <= Math.abs(n2))) {
          ok = false
          faisFoCalculNb('Les Valeurs numériques devraient être plus petites !')
        }
        if (ok && (j3pArrondi(num2 / den2, 6) !== j3pArrondi(n1 / n2, 6))) {
          ok = false
          faisFoCalculNb('La simplifiction est fausse !')
        }
        if (ok) {
          stor.calculeencours.frac[1] = { num: n1, den: n2 }
        }
        break
      case 's10':
        n1 = parseFloat2(stor.zone[0].reponse())
        n2 = parseFloat2(stor.zone[1].reponse())
        n3 = parseFloat2(stor.zone[2].reponse())
        n4 = parseFloat2(stor.zone[3].reponse())
        if (Math.abs(num1) <= Math.abs(n1)) {
          ok = false
          faisFoCalculNb('Les Valeurs numériques devraient être plus petite !')
        }
        if (ok && (Math.abs(den1) <= Math.abs(n2))) {
          ok = false
          faisFoCalculNb('Les Valeurs numériques devraient être plus petite !')
        }
        if (ok && (j3pArrondi(num1 / den1, 6) !== j3pArrondi(n1 / n2, 6))) {
          ok = false
          faisFoCalculNb('La simplifiction est fausse !')
        }
        if (ok && (Math.abs(num2) <= Math.abs(n3))) {
          ok = false
          faisFoCalculNb('Les Valeurs numériques devraient être plus petites !')
        }
        if (ok && (Math.abs(den2) <= Math.abs(n4))) {
          ok = false
          faisFoCalculNb('Les Valeurs numériques devraient être plus petites !')
        }
        if (ok && (j3pArrondi(num2 / den2, 6) !== j3pArrondi(n3 / n4, 6))) {
          ok = false
          faisFoCalculNb('Une simplifiction est fausse !')
        }
        if (ok) {
          stor.calculeencours.frac[0] = { num: n1, den: n2 }
          stor.calculeencours.frac[1] = { num: n3, den: n4 }
        }
        break
    }
    if (ok) {
      retouretape1()
    } else {
      stor.compteurPe8++
    }
    resize()
  }
  function coopR () {
    const n1 = parseFloat2(stor.zone[0].reponse())
    const n2 = parseFloat2(stor.zone[1].reponse())
    const f = stor.calculeencours.frac[0]
    let ok = true
    stor.uneco = '\\frac{ ' + Math.round(f.num / stor.cons) + ' \\times ' + stor.cons + '}{ ' + Math.round(f.den / stor.cons) + '\\times ' + stor.cons + '}'
    // test n1 * cons = num
    if (n1 * stor.cons !== f.num) {
      affCooRfo(1)
      ok = false
    }
    if (ok && (n2 * stor.cons !== f.den)) {
      affCooRfo(2)
      ok = false
    }
    if (ok) {
      poseReduc3()
    }
  }
  function coopR2 () {
    const n1 = parseFloat2(stor.zone[0].reponse())
    const n2 = parseFloat2(stor.zone[1].reponse())
    const f = stor.calculeencours.frac[0]
    let ok = true
    stor.uneco = '\\frac{ ' + Math.round(f.num / stor.cons) + '}{ ' + Math.round(f.den / stor.cons) + '}'
    // test n1 * cons = num
    if (n1 * stor.cons !== f.num) {
      affCooRfo(1)
      ok = false
    }
    if (ok && (n2 * stor.cons !== f.den)) {
      affCooRfo(2)
      ok = false
    }
    if (ok) {
      desactivezone(true)
      stor.calculeencours.frac[0] = { num: n1, den: n2 }
      if (n2 === 1) {
        j3pEmpty(stor.lesdiv.travailEnCours)
        j3pAffiche(stor.lesdiv.travailEnCours, null, '$ A = ' + n1 + '$')
        stor.lesdiv.travailEnCours.style.color = me.styles.cbien
        stor.repEl = true
        stor.etat = ''
        stor.lesdiv.etape.innerHTML = ''
        me.sectionCourante()
        return
      }
      poseReduc()
    }
  }
  function faisFoCalculNb (text) {
    stor.nbtente--
    stor.lastsaid = text
    if (stor.nbtente === 0) {
      stor.repEl = false
      desactivezone(false)
      stor.lesdiv.travailEnCours = stor.lesdiv.solution
      stor.comptediv++
      stor.lerepere = affichetoutlecalc(false)[1].parentNode
      const zou = stor.lerepere
      j3pEmpty(zou)
      j3pAffiche(zou, null, '$' + stor.uneco + '$')
      me.sectionCourante()
    } else {
      affModale(text)
    }
  }
  function affCooRfo (t) {
    stor.nbtente--
    if (t === 1) {
      stor.lastsaid = 'Erreur de calcul au numérateur!'
    } else {
      stor.lastsaid = 'Erreur de calcul au dénominateur !'
    }
    if (stor.nbtente === 0) {
      stor.repEl = false
      desactivezone(false)
      stor.etat = 'fin'
      me.sectionCourante()
      j3pAffiche(stor.lesdiv.solution, null, '$ A = ' + stor.uneco + '$')
    } else {
      affModale(stor.lastsaid)
    }
  }

  function enonceMain () {
    // A chaque répétition, on nettoie la scène
    me.videLesZones()
    creeLesDiv()
    stor.Lexo = faisChoixExos()

    if (ds.theme === 'zonesAvecImageDeFond') {
      stor.lesdiv.travail.classList.add('travail')
      stor.lesdiv.consigne.classList.add('enonce')
    }

    poseQuestion()
    me.fenetresjq = [{ name: 'Calculatrice', title: 'Une calculatrice', left: 600, top: 10 }]
    j3pCreeFenetres(me)
    j3pAfficheCroixFenetres('Calculatrice')

    me.finEnonce()
    // on ne veut pas du bouton valider tout de suite, on le mettra quand il aura répondu
    if (stor.etat !== 'reduc') me.cacheBoutonValider()
    // on veut que les boutons soient juste sous la fin de l’énoncé, à droite, donc on modifie le style du conteneur
    // (ça ne sera valable que pour cette section, lorsque le modèle va avancer dans le graphe il va recréer les boutons avec leur style habituel)
    // me.buttonsElts.container.style.position = 'relative'
    // me.buttonsElts.container.style.bottom = undefined
    // me.buttonsElts.container.style.top = '1em'
    // me.buttonsElts.container.style.left = '10em'
    // et on remet notre div avant les boutons
    // me.buttonsElts.container = stor.lesdiv.boutons
    me.zonesElts.MG.insertBefore(stor.lesdiv.conteneur, me.buttonsElts.container)
  }

  switch (this.etat) {
    case 'enonce':
      if (this.debutDeLaSection) {
        initSection()
      } else {
        enonceMain()
      }
      break // case "enonce":

    case 'correction':
      // On teste si une réponse a été saisie
      if (stor.lesdiv === undefined) return
      stor.lesdiv.explications.innerHTML = ''
      stor.lesdiv.correction.innerHTML = ''
      if (yaReponse.call(this)) {
        if (stor.zone.length !== 0) {
          if (stor.lololo) {
            coopR()
            return this.finCorrection()
          }
          if (stor.lololo2) {
            coopR2()
            return this.finCorrection()
          }
          if (stor.lalala) {
            coop()
          } else {
            coop2()
          }
          return this.finCorrection()
        }
        if (stor.etat === 'reduc') {
          isRepOkReduc()
          return this.finCorrection()
        }
        if (stor.etat === 'SIMPLFOIS') {
          stor.etat = 'x'
          viretouslesmenus()
          poseLastquest()
          return this.finCorrection()
        }
        if (stor.zzzSigne) {
          isRepOkSignes()
          return this.finCorrection()
        }

        // Bonne réponse
        if (isRepOk.call(this)) {
          this.score++
          this.reponseOk(stor.lesdiv.correction)
          j3pEmpty(stor.lesdiv.etape)
          this.typederreurs[0]++
          return this.finCorrection('navigation', true)
        }

        // Pas de bonne réponse
        this.reponseKo(stor.lesdiv.correction)
        if (this.isElapsed) {
          // A cause de la limite de temps :
          stor.lesdiv.correction.innerHTML += tempsDepasse + '<BR>'
          this.typederreurs[10]++
        } else {
          // Réponse fausse (dernier essai car y'en a qu'un)
          this.typederreurs[2]++
        }
        AffCorrection()
        return this.finCorrection('navigation', true)
      }

      // pas de réponse
      this.reponseManquante(stor.lesdiv.correction, 'Réponse incomplète !')
      this.finCorrection()
      break // case "correction":

    case 'navigation':
      if (this.sectionTerminee()) {
        this.parcours.pe = getPe(ds, stor)
      } else {
        this.etat = 'enonce'
      }
      // ça va activer suite (si on est avec l’état enonce) ou section suivante (etat navigation)
      this.finNavigation()
      break // case "navigation":
  }
}
