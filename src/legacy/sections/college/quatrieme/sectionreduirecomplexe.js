import $ from 'jquery'
import { j3pChaine, j3pDiv, j3pEcritBienAxPlusB, j3pFocus, j3pGetBornesIntervalle, j3pGetRandomInt, j3pMonome, j3pShowError } from 'src/legacy/core/functions'
import ValidationZones from 'src/legacy/outils/calculatrice/ValidationZones'
import { j3pAffiche, j3pMathsAjouteDans, mqRestriction } from 'src/lib/mathquill/functions'
import { getMtgCore } from 'src/lib/outils/mathgraph'
import textesGeneriques from 'src/lib/core/textes'

const { cBien, cFaux, essaieEncore, tempsDepasse } = textesGeneriques

/*
Alexis LECOMTE
Décembre 2015, suite remarques bugs ancienne section (vraisemblablement à cause d’XCAS)
Usage de MTG32

Principe : ax+bx+c+d dans le désordre, à réduire

Accès à la section :
Exemple de Graphe de test
   me.html?graphe=[1,"reduirecomplexe",[{pe:">=0",nn:"fin",conclusion:"fin"};
 */

/*
Variables :
 me.stockage[0]== solution ;
 me.stockage[1] et me.stockage[2] == nombres a et b dans ax +bx+c+d ou...;
 me.stockage[3] et me.stockage[4] == nombres c et d dans ax +bx+c+d ou...;
 me.stockage[5] == me.stockage[1]+me.stockage[2];
 me.stockage[6] == me.stockage[3]+me.stockage[4];
 me.stockage[7] == me.stockage[1]+me.stockage[2]+me.stockage[4];
 me.stockage[10] ==  choix de l’expression
 me.stockage[11] ==  ordre des termes dans l’expression
 me.stockage[21] == signeNombre(me.stockage[1]);
 me.stockage[211] == "" ou "-"  selon que me.stockage[1]=+/-1;
 me.stockage[22] == signeNombre(me.stockage[2]);
 me.stockage[23] == signeNombre(me.stockage[3]);
 me.stockage[24] == signeNombre(me.stockage[4]);
 me.stockage[26] == signeNombre(me.stockage[6]);
*/

export const params = {
  outils: ['calculatrice', 'mathquill'],
  /**
   * Ce qui est paramétrable dans l’éditeur de graphe
   * @type {LegacySectionParamsParametres}
   */
  parametres: [
    ['nbrepetitions', 10, 'entier', 'Nombre de répétitions de la section'],
    ['indication', '', 'string', 'Indication pour chaque répétition'],
    ['limite', 0, 'entier', 'Temps disponible par répétition, en secondes'],
    ['nbchances', 2, 'entier', 'Nombre d’essais par répétition'],

    ['entier1', '[-15;15]', 'string', 'Intervalle auquel appartient le nombre a dans ax+bx+c+d'],
    ['entier2', '[-15;15]', 'string', 'Intervalle auquel appartient le nombre b dans ax+bx+c+d'],
    ['entier3', '[-15;15]', 'string', 'Intervalle auquel appartient le nombre c dans ax+bx+c+d'],
    ['entier4', '[-15;15]', 'string', 'Intervalle auquel appartient le nombre d dans ax+bx+c+d']
  ]
}

const textes = {
  titreExo: 'Réduire une expression littérale',
  phrase1: 'Réduire : ',
  phrase9: 'tu as recopié l’énoncé alors qu’on te demande de réduire ; essaie encore&nbsp;!',
  phrase10: 'La solution était ',
  phrase20: 'En effet, ',
  pas_simplifie: 'La réponse est correcte mais pas simplifiée&nbsp;!'
}

/**
 * section reduirecomplexe
 * @this {Parcours}
 */
export default function main () {
  const me = this
  const stor = me.storage
  const espace = '&nbsp;&nbsp;&nbsp;&nbsp;'
  const ds = me.donneesSection
  function traiteMathQuill (ch) {
    ch = ch.replace(/}{/g, '}/{') // Pour traiter les \frac
    ch = ch.replace(/\\times/g, '*') // POur traiter les signes de multiplication
    ch = ch.replace(/\\left\(/g, '(') // Pour traiter les parenthèses ouvrantes
    ch = ch.replace(/\\right\)/g, ')') // Pour traiter les parenthèses fermantes
    ch = ch.replace(/\\left\|/g, 'abs(') // Traitement des valeurs absolues
    ch = ch.replace(/\\right\|/g, ')')
    ch = ch.replace(/\\frac/g, '') // Les fractions ont déja été remplacées par des divisions
    ch = ch.replace(/\\sqrt/g, 'sqrt') // Traitement des racines carrées
    ch = ch.replace(/\\ln/g, 'ln') // Traitement des ln
    ch = ch.replace(/\\le/g, '<=') // Traitement des signes <=
    ch = ch.replace(/\\ge/g, '>=') // Traitement des signes >=
    ch = ch.replace(/{/g, '(') // Les accolades deviennet des parenthèses
    ch = ch.replace(/}/g, ')')
    ch = ch.replace(/\)(\d)/g, ')*$1') // Remplace les parenthèses ")" suivies d’un chiffre en ajoutant un *
    ch = ch.replace(/\^(\d)(\d)/g, '^$1*$2') // On peut avoir par exemple 2^35^4 pour 2^3*5^4
    ch = ch.replace(/,/g, '.') // Remplacement des virgules par des points décimaux
    ch = ch.replace(/PI/g, 'pi')
    ch = ch.replace(/\\pi */g, 'pi')
    ch = ch.replace(/\s/g, '') // ON retire aussi les espaces inutiles
    ch = stor.liste.addImplicitMult(ch)// Traitement des multiplications implicites
    return ch
  }

  function signeNombre (nombre) {
    if (nombre < 0) {
      return ('-')
    } else {
      return ('+')
    }
  }

  function signeNombre1 (nombre) {
    if (nombre < 0) {
      return ('-')
    } else {
      return ('')
    }
  }

  function afficheEnonce2litteroet2num () {
    me.stockage[0] = j3pMonome(1, 1, me.stockage[5]) + j3pMonome(2, 0, me.stockage[6])
    switch (me.stockage[11]) {
      case 1: // ax+bx+c+d
        stor.elts = j3pAffiche(stor.enonce, '', textes.phrase1 + espace + '$£ax' + me.stockage[22] + '£bx' + me.stockage[23] + '£c' + me.stockage[24] + '£d=$&1&',
          {
            a: me.stockage[1],
            b: Math.abs(me.stockage[2]),
            c: Math.abs(me.stockage[3]),
            d: Math.abs(me.stockage[4]),
            inputmq1: { texte: '', correction: me.stockage[0] }
          })
        break
      case 2: // ax+c+bx+d
        stor.elts = j3pAffiche(stor.enonce, '', textes.phrase1 + espace + ' $£ax' + me.stockage[23] + '£c' + me.stockage[22] + '£bx' + me.stockage[24] + '£d=$&1&',
          {
            a: me.stockage[1],
            b: Math.abs(me.stockage[2]),
            c: Math.abs(me.stockage[3]),
            d: Math.abs(me.stockage[4]),
            inputmq1: { texte: '', correction: me.stockage[0] }
          })
        break
      case 3: // ax+c+d+bx
        stor.elts = j3pAffiche(stor.enonce, '', textes.phrase1 + espace + ' $£ax' + me.stockage[23] + '£c' + me.stockage[24] + '£d' + me.stockage[22] + '£bx=$&1&',
          {
            a: me.stockage[1],
            b: Math.abs(me.stockage[2]),
            c: Math.abs(me.stockage[3]),
            d: Math.abs(me.stockage[4]),
            inputmq1: { texte: '', correction: me.stockage[0] }
          })
        break
      case 4: // c+ax+d+bx
        stor.elts = j3pAffiche(stor.enonce, '', textes.phrase1 + espace + ' $£c' + me.stockage[21] + '£ax' + me.stockage[24] + '£d' + me.stockage[22] + '£bx=$&1&',
          {
            a: Math.abs(me.stockage[1]),
            b: Math.abs(me.stockage[2]),
            c: me.stockage[3],
            d: Math.abs(me.stockage[4]),
            inputmq1: { texte: '', correction: me.stockage[0] }
          })
        break
      case 5: // c+ax+bx+d
        stor.elts = j3pAffiche(stor.enonce, '', textes.phrase1 + espace + ' $£c' + me.stockage[21] + '£ax' + me.stockage[22] + '£bx' + me.stockage[24] + '£d=$&1&',
          {
            a: Math.abs(me.stockage[1]),
            b: Math.abs(me.stockage[2]),
            c: me.stockage[3],
            d: Math.abs(me.stockage[4]),
            inputmq1: { texte: '', correction: me.stockage[0] }
          })
        break
    }
  }

  function afficheEnoncePluslitterokenum () {
    switch (me.stockage[11]) {
      case 1: { // c+ax+bx
        me.stockage[0] = j3pMonome(1, 1, me.stockage[5]) + j3pMonome(2, 0, me.stockage[3])
        stor.elts = j3pAffiche(stor.enonce, '', textes.phrase1 + espace + '$£c' + me.stockage[21] + '£ax' + me.stockage[22] + '£bx=$&1&',
          {
            a: Math.abs(me.stockage[1]),
            b: Math.abs(me.stockage[2]),
            c: me.stockage[3],
            inputmq1: { texte: '', correction: me.stockage[0] }
          })
        break
      }

      case 2: { // ax+c+bx
        me.stockage[0] = j3pMonome(1, 1, me.stockage[5]) + j3pMonome(2, 0, me.stockage[3])
        stor.elts = j3pAffiche(stor.enonce, '', textes.phrase1 + espace + ' $£ax' + me.stockage[23] + '£c' + me.stockage[22] + '£bx=$&1&',
          {
            a: me.stockage[1],
            b: Math.abs(me.stockage[2]),
            c: Math.abs(me.stockage[3]),
            inputmq1: { texte: '', correction: me.stockage[0] }
          })
        break
      }
      case 3: { // ax+bx+c+dx
        me.stockage[0] = j3pMonome(1, 1, me.stockage[7]) + j3pMonome(2, 0, me.stockage[3])
        stor.elts = j3pAffiche(stor.enonce, '', textes.phrase1 + espace + ' $£ax' + me.stockage[22] + '£bx' + me.stockage[23] + '£c' + me.stockage[24] + '£dx=$&1&',
          {
            a: me.stockage[1],
            b: Math.abs(me.stockage[2]),
            c: Math.abs(me.stockage[3]),
            d: Math.abs(me.stockage[4]),
            inputmq1: { texte: '', correction: me.stockage[0] }
          })
        break
      }
      case 4: { // ax+c+bx+dx
        me.stockage[0] = j3pMonome(1, 1, me.stockage[7]) + j3pMonome(2, 0, me.stockage[3])
        stor.elts = j3pAffiche(stor.enonce, '', textes.phrase1 + espace + ' $£ax' + me.stockage[23] + '£c' + me.stockage[22] + '£bx' + me.stockage[24] + '£dx=$&1&',
          {
            a: me.stockage[1],
            b: Math.abs(me.stockage[2]),
            c: Math.abs(me.stockage[3]),
            d: Math.abs(me.stockage[4]),
            inputmq1: { texte: '', correction: me.stockage[0] }
          })
        break
      }
    }
  }

  function afficheEnonce2litteroet2numCoeff1 () {
    me.stockage[0] = j3pMonome(1, 1, me.stockage[5]) + j3pMonome(2, 0, me.stockage[6])
    switch (me.stockage[11]) {
      case 1 :
        // ax+bx+c+d
        if (Math.abs(me.stockage[1]) === 1 && Math.abs(me.stockage[2]) === 1) { ///  a=+/-1 et b=+/-1
          stor.elts = j3pAffiche(stor.enonce, '', textes.phrase1 + espace + ' $' + me.stockage[211] + 'x' + me.stockage[22] + 'x' + me.stockage[23] + '£c' + me.stockage[24] + '£d=$&1&',
            {
              c: Math.abs(me.stockage[3]),
              d: Math.abs(me.stockage[4]),
              inputmq1: { texte: '', correction: me.stockage[0] }
            })
        } else { /// on a a=1 ou b=1 (pas les deux !)
          if (Math.abs(me.stockage[1]) === 1) { /// c’est a=1  (b <> 1)
            stor.elts = j3pAffiche(stor.enonce, '', textes.phrase1 + espace + ' $' + me.stockage[211] + 'x' + me.stockage[22] + '£bx' + me.stockage[23] + '£c' + me.stockage[24] + '£d=$&1&',
              {
                b: Math.abs(me.stockage[2]),
                c: Math.abs(me.stockage[3]),
                d: Math.abs(me.stockage[4]),
                inputmq1: { texte: '', correction: me.stockage[0] }
              })
          } else { /// c’est b=1  (a <> 1)
            stor.elts = j3pAffiche(stor.enonce, '', textes.phrase1 + espace + ' $£ax' + me.stockage[22] + 'x' + me.stockage[23] + '£c' + me.stockage[24] + '£d=$&1&',
              {
                a: me.stockage[1],
                c: Math.abs(me.stockage[3]),
                d: Math.abs(me.stockage[4]),
                inputmq1: { texte: '', correction: me.stockage[0] }
              })
          }
        }
        break
      case 2 :

        if (Math.abs(me.stockage[1]) === 1 && Math.abs(me.stockage[2]) === 1) { ///  a=+/-1 et b=+/-1
          stor.elts = j3pAffiche(stor.enonce, '', textes.phrase1 + espace + ' $' + me.stockage[211] + 'x' + me.stockage[23] + '£c' + me.stockage[22] + 'x' + me.stockage[24] + '£d=$&1&',
            {
              c: Math.abs(me.stockage[3]),
              d: Math.abs(me.stockage[4]),
              inputmq1: { texte: '', correction: me.stockage[0] }
            })
        } else { /// on a a=1 ou b=1 (pas les deux !)
          if (Math.abs(me.stockage[1]) === 1) { /// c’est a=1  (b <> 1)
            stor.elts = j3pAffiche(stor.enonce, '', textes.phrase1 + espace + ' $' + me.stockage[211] + 'x' + me.stockage[23] + '£c' + me.stockage[22] + '£bx' + me.stockage[24] + '£d=$&1&',
              {
                b: Math.abs(me.stockage[2]),
                c: Math.abs(me.stockage[3]),
                d: Math.abs(me.stockage[4]),
                inputmq1: { texte: '', correction: me.stockage[0] }
              })
          } else { /// c’est b=1  (a <> 1)
            stor.elts = j3pAffiche(stor.enonce, '', textes.phrase1 + espace + ' $£ax' + me.stockage[23] + '£c' + me.stockage[22] + 'x' + me.stockage[24] + '£d=$&1&',
              {
                a: me.stockage[1],
                c: Math.abs(me.stockage[3]),
                d: Math.abs(me.stockage[4]),
                inputmq1: { texte: '', correction: me.stockage[0] }
              })
          }
        }
        break
      case 3 :
        if (Math.abs(me.stockage[1]) === 1 && Math.abs(me.stockage[2]) === 1) { ///  a=+/-1 et b=+/-1
          stor.elts = j3pAffiche(stor.enonce, '', textes.phrase1 + espace + ' $' + me.stockage[211] + 'x' + me.stockage[23] + '£c' + me.stockage[24] + '£d' + me.stockage[22] + 'x=$&1&',
            {
              c: Math.abs(me.stockage[3]),
              d: Math.abs(me.stockage[4]),
              inputmq1: { texte: '', correction: me.stockage[0] }
            })
        } else { /// on a a=1 ou b=1 (pas les deux !)
          if (Math.abs(me.stockage[1]) === 1) { /// c’est a=+/-1  (b <> 1)
            stor.elts = j3pAffiche(stor.enonce, '', textes.phrase1 + espace + ' $' + me.stockage[211] + 'x' + me.stockage[23] + '£c' + me.stockage[24] + '£d' + me.stockage[22] + '£bx=$&1&',
              {
                b: Math.abs(me.stockage[2]),
                c: Math.abs(me.stockage[3]),
                d: Math.abs(me.stockage[4]),
                inputmq1: { texte: '', correction: me.stockage[0] }
              })
          } else { /// c’est b=+/-1  (a <> 1)
            stor.elts = j3pAffiche(stor.enonce, '', textes.phrase1 + espace + ' $£ax' + me.stockage[23] + '£c' + me.stockage[24] + '£d' + me.stockage[22] + 'x=$&1&',
              {
                a: me.stockage[1],
                c: Math.abs(me.stockage[3]),
                d: Math.abs(me.stockage[4]),
                inputmq1: { texte: '', correction: me.stockage[0] }
              })
          }
        }
        break
      case 4 :
        if (Math.abs(me.stockage[1]) === 1 && Math.abs(me.stockage[2]) === 1) { ///  a=+/-1 et b=+/-1
          stor.elts = j3pAffiche(stor.enonce, '', textes.phrase1 + espace + ' $£c' + me.stockage[21] + 'x' + me.stockage[24] + '£d' + me.stockage[22] + 'x=$&1&',
            {
              c: me.stockage[3],
              d: Math.abs(me.stockage[4]),
              inputmq1: { texte: '', correction: me.stockage[0] }
            })
        } else { /// on a a=1 ou b=1 (pas les deux !)
          if (Math.abs(me.stockage[1]) === 1) { /// c’est a=+/-1  (b <> 1)
            stor.elts = j3pAffiche(stor.enonce, '', textes.phrase1 + espace + ' $£c' + me.stockage[21] + 'x' + me.stockage[24] + '£d' + me.stockage[22] + '£bx=$&1&',
              {
                b: Math.abs(me.stockage[2]),
                c: me.stockage[3],
                d: Math.abs(me.stockage[4]),
                inputmq1: { texte: '', correction: me.stockage[0] }
              })
          } else { /// c’est b=+/-1  (a <> 1)
            stor.elts = j3pAffiche(stor.enonce, '', textes.phrase1 + espace + ' $£c' + me.stockage[21] + '£ax' + me.stockage[24] + '£d' + me.stockage[22] + 'x=$&1&',
              {
                a: Math.abs(me.stockage[1]),
                c: me.stockage[3],
                d: Math.abs(me.stockage[4]),
                inputmq1: { texte: '', correction: me.stockage[0] }
              })
          }
        }
        break
      case 5 :
        if (Math.abs(me.stockage[1]) === 1 && Math.abs(me.stockage[2]) === 1) { ///  a=+/-1 et b=+/-1
          stor.elts = j3pAffiche(stor.enonce, '', textes.phrase1 + espace + ' $£c' + me.stockage[21] + 'x' + me.stockage[22] + 'x' + me.stockage[24] + '£d=$&1&',
            {
              c: me.stockage[3],
              d: Math.abs(me.stockage[4]),
              inputmq1: { texte: '', correction: me.stockage[0] }
            })
        } else { /// on a a=1 ou b=1 (pas les deux !)
          if (Math.abs(me.stockage[1]) === 1) { /// c’est a=+/-1  (b <> 1)
            stor.elts = j3pAffiche(stor.enonce, '', textes.phrase1 + espace + ' $£c' + me.stockage[21] + 'x' + me.stockage[22] + '£bx' + me.stockage[24] + '£d=$&1&',
              {
                b: Math.abs(me.stockage[2]),
                c: me.stockage[3],
                d: Math.abs(me.stockage[4]),
                inputmq1: { texte: '', correction: me.stockage[0] }
              })
          } else { /// c’est b=+/-1  (a <> 1)
            stor.elts = j3pAffiche(stor.enonce, '', textes.phrase1 + espace + ' $£c' + me.stockage[21] + '£ax' + me.stockage[22] + 'x' + me.stockage[24] + '£d=$&1&',
              {
                a: Math.abs(me.stockage[1]),
                c: me.stockage[3],
                d: Math.abs(me.stockage[4]),
                inputmq1: { texte: '', correction: me.stockage[0] }
              })
          }
        }
        break
    }
  }

  function afficheEnoncePluslitterokenumCoeff1 () {
    switch (me.stockage[11]) {
      case 1 : // c+ax+bx
        me.stockage[0] = j3pMonome(1, 1, me.stockage[5]) + j3pMonome(2, 0, me.stockage[3])
        if (Math.abs(me.stockage[1]) === 1 && Math.abs(me.stockage[2]) === 1) { ///  a=+/-1 et b=+/-1
          stor.elts = j3pAffiche(stor.enonce, '', textes.phrase1 + espace + ' $£c' + me.stockage[21] + 'x' + me.stockage[22] + 'x=$&1&',
            {
              c: me.stockage[3],
              d: Math.abs(me.stockage[4]),
              inputmq1: { texte: '', correction: me.stockage[0] }
            })
        } else { /// on a a=1 ou b=1 (pas les deux !)
          if (Math.abs(me.stockage[1]) === 1) { /// c’est a=+/-1  (b <> 1)
            stor.elts = j3pAffiche(stor.enonce, '', textes.phrase1 + espace + ' $£c' + me.stockage[21] + 'x' + me.stockage[22] + '£bx=$&1&',
              {
                b: Math.abs(me.stockage[2]),
                c: me.stockage[3],
                d: Math.abs(me.stockage[4]),
                inputmq1: { texte: '', correction: me.stockage[0] }
              })
          } else { /// c’est b=+/-1  (a <> 1)
            stor.elts = j3pAffiche(stor.enonce, '', textes.phrase1 + espace + ' $£c' + me.stockage[21] + '£ax' + me.stockage[22] + 'x=$&1&',
              {
                a: Math.abs(me.stockage[1]),
                c: me.stockage[3],
                d: Math.abs(me.stockage[4]),
                inputmq1: { texte: '', correction: me.stockage[0] }
              })
          }
        }
        break
      case 2 : // ax+c+b
        me.stockage[0] = j3pMonome(1, 1, me.stockage[5]) + j3pMonome(2, 0, me.stockage[3])
        if (Math.abs(me.stockage[1]) === 1 && Math.abs(me.stockage[2]) === 1) { ///  a=+/-1 et b=+/-1
          stor.elts = j3pAffiche(stor.enonce, '', textes.phrase1 + espace + ' $' + me.stockage[211] + 'x' + me.stockage[23] + '£c' + me.stockage[22] + 'x=$&1&',
            {
              c: Math.abs(me.stockage[3]),
              d: Math.abs(me.stockage[4]),
              inputmq1: { texte: '', correction: me.stockage[0] }
            })
        } else { /// on a a=1 ou b=1 (pas les deux !)
          if (Math.abs(me.stockage[1]) === 1) { /// c’est a=1  (b <> 1)
            stor.elts = j3pAffiche(stor.enonce, '', textes.phrase1 + espace + ' $' + me.stockage[211] + 'x' + me.stockage[23] + '£c' + me.stockage[22] + '£bx=$&1&',
              {
                b: Math.abs(me.stockage[2]),
                c: Math.abs(me.stockage[3]),
                d: Math.abs(me.stockage[4]),
                inputmq1: { texte: '', correction: me.stockage[0] }
              })
          } else { /// c’est b=1  (a <> 1)
            stor.elts = j3pAffiche(stor.enonce, '', textes.phrase1 + espace + ' $£ax' + me.stockage[23] + '£c' + me.stockage[22] + 'x=$&1&',
              {
                a: me.stockage[1],
                c: Math.abs(me.stockage[3]),
                d: Math.abs(me.stockage[4]),
                inputmq1: { texte: '', correction: me.stockage[0] }
              })
          }
        }
        break
      case 3 : //  ax+bx+c+dx
        me.stockage[0] = j3pMonome(1, 1, me.stockage[7]) + j3pMonome(2, 0, me.stockage[3])
        if (Math.abs(me.stockage[4]) === 1) { // d=+/-1
          if (Math.abs(me.stockage[1]) === 1 && Math.abs(me.stockage[2]) === 1) { ///  a=+/-1 et b=+/-1
            stor.elts = j3pAffiche(stor.enonce, '', textes.phrase1 + espace + ' $' + me.stockage[211] + 'x' + me.stockage[22] + 'x' + me.stockage[23] + '£c' + me.stockage[24] + 'x=$&1&',
              {
                c: Math.abs(me.stockage[3]),
                inputmq1: { texte: '', correction: me.stockage[0] }
              })
          } else { /// on a a=1 ou b=1 (pas les deux !)
            if (Math.abs(me.stockage[1]) === 1) { /// c’est a=1  (b <> 1)
              stor.elts = j3pAffiche(stor.enonce, '', textes.phrase1 + espace + ' $' + me.stockage[211] + 'x' + me.stockage[22] + '£bx' + me.stockage[23] + '£c' + me.stockage[24] + 'x=$&1&',
                {
                  b: Math.abs(me.stockage[2]),
                  c: Math.abs(me.stockage[3]),
                  inputmq1: { texte: '', correction: me.stockage[0] }
                })
            } else { /// c’est b=1  (a <> 1)
              stor.elts = j3pAffiche(stor.enonce, '', textes.phrase1 + espace + ' $£ax' + me.stockage[22] + 'x' + me.stockage[23] + '£c' + me.stockage[24] + 'x=$&1&',
                {
                  a: me.stockage[1],
                  c: Math.abs(me.stockage[3]),
                  inputmq1: { texte: '', correction: me.stockage[0] }
                })
            }
          }
        } else { // d<>+/-1
          if (Math.abs(me.stockage[1]) === 1 && Math.abs(me.stockage[2]) === 1) { ///  a=+/-1 et b=+/-1
            stor.elts = j3pAffiche(stor.enonce, '', textes.phrase1 + espace + ' $' + me.stockage[211] + 'x' + me.stockage[22] + 'x' + me.stockage[23] + '£c' + me.stockage[24] + '£dx=$&1&',
              {
                c: Math.abs(me.stockage[3]),
                d: Math.abs(me.stockage[4]),
                inputmq1: { texte: '', correction: me.stockage[0] }
              })
          } else { /// on a a=1 ou b=1 (pas les deux !)
            if (Math.abs(me.stockage[1]) === 1) { /// c’est a=1  (b <> 1)
              stor.elts = j3pAffiche(stor.enonce, '', textes.phrase1 + espace + ' $' + me.stockage[211] + 'x' + me.stockage[22] + '£bx' + me.stockage[23] + '£c' + me.stockage[24] + '£dx=$&1&',
                {
                  b: Math.abs(me.stockage[2]),
                  c: Math.abs(me.stockage[3]),
                  d: Math.abs(me.stockage[4]),
                  inputmq1: { texte: '', correction: me.stockage[0] }
                })
            } else { /// c’est b=1  (a <> 1)
              stor.elts = j3pAffiche(stor.enonce, '', textes.phrase1 + espace + ' $£ax' + me.stockage[22] + 'x' + me.stockage[23] + '£c' + me.stockage[24] + '£dx=$&1&',
                {
                  a: me.stockage[1],
                  c: Math.abs(me.stockage[3]),
                  d: Math.abs(me.stockage[4]),
                  inputmq1: { texte: '', correction: me.stockage[0] }
                })
            }
          }
        }
        break
      case 4 : // ax+c+bx+dx
        me.stockage[0] = j3pMonome(1, 1, me.stockage[7]) + j3pMonome(2, 0, me.stockage[3])
        if (Math.abs(me.stockage[4]) === 1) { // d=+/-1
          if (Math.abs(me.stockage[1]) === 1 && Math.abs(me.stockage[2]) === 1) { ///  a=+/-1 et b=+/-1
            stor.elts = j3pAffiche(stor.enonce, 'stor.elts = ', textes.phrase1 + espace + ' $' + me.stockage[211] + 'x' + me.stockage[23] + '£c' + me.stockage[22] + 'x' + me.stockage[24] + 'x=$&1&',
              {
                c: Math.abs(me.stockage[3]),
                inputmq1: { texte: '', correction: me.stockage[0] }
              })
          } else { /// on a a=1 ou b=1 (pas les deux !)
            if (Math.abs(me.stockage[1]) === 1) { /// c’est a=1  (b <> 1)
              stor.elts = j3pAffiche(stor.enonce, '', textes.phrase1 + espace + ' $' + me.stockage[211] + 'x' + me.stockage[23] + '£c' + me.stockage[22] + '£bx' + me.stockage[24] + 'x=$&1&',
                {
                  b: Math.abs(me.stockage[2]),
                  c: Math.abs(me.stockage[3]),
                  inputmq1: { texte: '', correction: me.stockage[0] }
                })
            } else { /// c’est b=1  (a <> 1)
              stor.elts = j3pAffiche(stor.enonce, '', textes.phrase1 + espace + ' $£ax' + me.stockage[23] + '£c' + me.stockage[22] + 'x' + me.stockage[24] + 'x=$&1&',
                {
                  a: me.stockage[1],
                  c: Math.abs(me.stockage[3]),
                  inputmq1: { texte: '', correction: me.stockage[0] }
                })
            }
          }
        } else { // d<>+/-1
          if (Math.abs(me.stockage[1]) === 1 && Math.abs(me.stockage[2]) === 1) { ///  a=+/-1 et b=+/-1
            stor.elts = j3pAffiche(stor.enonce, '', textes.phrase1 + espace + ' $' + me.stockage[211] + 'x' + me.stockage[23] + '£c' + me.stockage[22] + 'x' + me.stockage[24] + '£dx=$&1&',
              {
                c: Math.abs(me.stockage[3]),
                d: Math.abs(me.stockage[4]),
                inputmq1: { texte: '', correction: me.stockage[0] }
              })
          } else { /// on a a=1 ou b=1 (pas les deux !)
            if (Math.abs(me.stockage[1]) === 1) { /// c’est a=1  (b <> 1)
              stor.elts = j3pAffiche(stor.enonce, '', textes.phrase1 + espace + ' $' + me.stockage[211] + 'x' + me.stockage[23] + '£c' + me.stockage[22] + '£bx' + me.stockage[24] + '£dx=$&1&',
                {
                  b: Math.abs(me.stockage[2]),
                  c: Math.abs(me.stockage[3]),
                  d: Math.abs(me.stockage[4]),
                  inputmq1: { texte: '', correction: me.stockage[0] }
                })
            } else { /// c’est b=1  (a <> 1)
              stor.elts = j3pAffiche(stor.enonce, '', textes.phrase1 + espace + ' $£ax' + me.stockage[23] + '£c' + me.stockage[22] + 'x' + me.stockage[24] + '£dx=$&1&',
                {
                  a: me.stockage[1],
                  c: Math.abs(me.stockage[3]),
                  d: Math.abs(me.stockage[4]),
                  inputmq1: { texte: '', correction: me.stockage[0] }
                })
            }
          }
        }
        break
    }
  }

  function afficheCorrection () {
    switch (me.stockage[10]) {
      case '2litteroet2num':
        j3pMathsAjouteDans(stor.divCorr, {
          content: ' <br>' + textes.phrase10 + j3pEcritBienAxPlusB(me.stockage[5], me.stockage[6]),
          parametres: {
            a: me.stockage[5],
            b: me.stockage[6]
          }
        })
        break
      case 'pluslitterokenum':
        if (me.stockage[11] === 1 || me.stockage[11] === 2) {
          j3pMathsAjouteDans(stor.divCorr, {
            content: ' <br>' + textes.phrase10 + j3pEcritBienAxPlusB(me.stockage[5], me.stockage[3]),
            parametres: {
              a: me.stockage[5],
              b: me.stockage[3]
            }
          })
        }
        if (me.stockage[11] === 3 || me.stockage[11] === 4) {
          j3pMathsAjouteDans(stor.divCorr, {
            content: ' <br>' + textes.phrase10 + j3pEcritBienAxPlusB(me.stockage[7], me.stockage[3]),
            parametres: {
              a: me.stockage[7],
              b: me.stockage[3]
            }
          })
        }
        break
    }
  }

  function afficheExplication2litterauxEt2num () {
    let explications = ''
    switch (me.stockage[11]) {
      case 1:
        explications = j3pMonome(1, 1, me.stockage[1]) + j3pMonome(2, 1, me.stockage[2]) + j3pMonome(3, 0, me.stockage[3]) + j3pMonome(4, 0, me.stockage[4])
        j3pChaine('£ax' + me.stockage[22] + '£bx' + me.stockage[23] + '£c' + me.stockage[24] + '£d',
          {
            a: me.stockage[1],
            b: Math.abs(me.stockage[2]),
            c: Math.abs(me.stockage[3]),
            d: Math.abs(me.stockage[4])
          })
        break
      case 2:
        explications = j3pMonome(1, 1, me.stockage[1]) + j3pMonome(2, 0, me.stockage[3]) + j3pMonome(3, 1, me.stockage[2]) + j3pMonome(4, 0, me.stockage[4])
        j3pChaine('£ax' + me.stockage[23] + '£c' + me.stockage[22] + '£bx' + me.stockage[24] + '£d',
          {
            a: me.stockage[1],
            b: Math.abs(me.stockage[2]),
            c: Math.abs(me.stockage[3]),
            d: Math.abs(me.stockage[4])
          })
        break
      case 3:
        explications = j3pMonome(1, 1, me.stockage[1]) + j3pMonome(2, 0, me.stockage[3]) + j3pMonome(3, 0, me.stockage[4]) + j3pMonome(4, 1, me.stockage[2])
        j3pChaine('£ax' + me.stockage[23] + '£c' + me.stockage[24] + '£d' + me.stockage[22] + '£bx',
          {
            a: me.stockage[1],
            b: Math.abs(me.stockage[2]),
            c: Math.abs(me.stockage[3]),
            d: Math.abs(me.stockage[4])
          })
        break
      case 4:explications = j3pMonome(1, 0, me.stockage[3]) + j3pMonome(2, 1, me.stockage[1]) + j3pMonome(3, 0, me.stockage[4]) + j3pMonome(4, 1, me.stockage[2])
        j3pChaine('£c' + me.stockage[21] + '£ax' + me.stockage[24] + '£d' + me.stockage[22] + '£bx',
          {
            a: Math.abs(me.stockage[1]),
            b: Math.abs(me.stockage[2]),
            c: me.stockage[3],
            d: Math.abs(me.stockage[4])
          })
        break
      case 5:
        explications = j3pMonome(1, 0, me.stockage[3]) + j3pMonome(2, 1, me.stockage[1]) + j3pMonome(3, 1, me.stockage[2]) + j3pMonome(4, 0, me.stockage[3])
        j3pChaine('£c' + me.stockage[21] + '£ax' + me.stockage[22] + '£bx' + me.stockage[24] + '£d',
          {
            a: me.stockage[1],
            b: Math.abs(me.stockage[2]),
            c: Math.abs(me.stockage[3]),
            d: Math.abs(me.stockage[4])
          })
        break
    }
    j3pAffiche(stor.tabZonesExpli[1], '', '&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;$' + explications + '$')
  }

  function afficheExplicationPlusLitterOkEnum () {
    let explications = ''
    switch (me.stockage[11]) {
      case 1:
        explications = j3pMonome(1, 0, me.stockage[3]) + j3pMonome(2, 1, me.stockage[1]) + j3pMonome(3, 1, me.stockage[2])
        j3pChaine('£c' + me.stockage[21] + '£ax' + me.stockage[22] + '£bx',
          {
            a: Math.abs(me.stockage[1]),
            b: Math.abs(me.stockage[2]),
            c: me.stockage[3]
          })
        break
      case 2:
        explications = j3pMonome(1, 1, me.stockage[1]) + j3pMonome(2, 0, me.stockage[3]) + j3pMonome(3, 1, me.stockage[2])
        j3pChaine('£ax' + me.stockage[23] + '£c' + me.stockage[22] + '£bx',
          {
            a: me.stockage[1],
            b: Math.abs(me.stockage[2]),
            c: Math.abs(me.stockage[3])
          })
        break
      case 3:
        explications = j3pMonome(1, 1, me.stockage[1]) + j3pMonome(2, 1, me.stockage[2]) + j3pMonome(3, 0, me.stockage[3]) + j3pMonome(3, 1, me.stockage[4])
        j3pChaine('£ax' + me.stockage[22] + '£bx' + me.stockage[23] + '£c' + me.stockage[24] + '£dx',
          {
            a: me.stockage[1],
            b: Math.abs(me.stockage[2]),
            c: Math.abs(me.stockage[3]),
            d: Math.abs(me.stockage[4])
          })
        break
      case 4:
        explications = j3pMonome(1, 1, me.stockage[1]) + j3pMonome(2, 0, me.stockage[3]) + j3pMonome(3, 1, me.stockage[2]) + j3pMonome(3, 1, me.stockage[4])
        j3pChaine('£ax' + me.stockage[23] + '£c' + me.stockage[22] + '£bx' + me.stockage[24] + '£dx',
          {
            a: me.stockage[1],
            b: Math.abs(me.stockage[2]),
            c: Math.abs(me.stockage[3]),
            d: Math.abs(me.stockage[4])
          })
        break
    }
    j3pAffiche(stor.tabZonesExpli[1], '', '&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;$' + explications + '$')
  }

  function afficheExplication2Litteroet2numCoeff1 () {
    switch (me.stockage[11]) {
      case 1 :
        if (Math.abs(me.stockage[1]) === 1 && Math.abs(me.stockage[2]) === 1) { ///  a=+/-1 et b=+/-1
          j3pAffiche(stor.tabZonesExpli[1], '', '&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;$' + me.stockage[211] + 'x' + me.stockage[22] + 'x' + me.stockage[23] + '£c' + me.stockage[24] + '£d$',
            {
              c: Math.abs(me.stockage[3]),
              d: Math.abs(me.stockage[4])
            })
        } else { /// on a a=1 ou b=1 (pas les deux !)
          if (Math.abs(me.stockage[1]) === 1) { /// c’est a=1  (b <> 1)
            j3pAffiche(stor.tabZonesExpli[1], '', '&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;$' + me.stockage[211] + 'x' + me.stockage[22] + '£bx' + me.stockage[23] + '£c' + me.stockage[24] + '£d$',
              {
                b: Math.abs(me.stockage[2]),
                c: Math.abs(me.stockage[3]),
                d: Math.abs(me.stockage[4])
              })
          } else { /// c’est b=1  (a <> 1)
            j3pAffiche(stor.tabZonesExpli[1], '', '&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;$£ax' + me.stockage[22] + 'x' + me.stockage[23] + '£c' + me.stockage[24] + '£d$',
              {
                a: me.stockage[1],
                c: Math.abs(me.stockage[3]),
                d: Math.abs(me.stockage[4])
              })
          }
        }
        break
      case 2 :
        if (Math.abs(me.stockage[1]) === 1 && Math.abs(me.stockage[2]) === 1) { ///  a=+/-1 et b=+/-1
          j3pAffiche(stor.tabZonesExpli[1], '', '&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;$' + me.stockage[211] + 'x' + me.stockage[23] + '£c' + me.stockage[22] + 'x' + me.stockage[24] + '£d$',
            {
              c: Math.abs(me.stockage[3]),
              d: Math.abs(me.stockage[4])
            })
        } else { /// on a a=1 ou b=1 (pas les deux !)
          if (Math.abs(me.stockage[1]) === 1) { /// c’est a=1  (b <> 1)
            j3pAffiche(stor.tabZonesExpli[1], '', '&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;$' + me.stockage[211] + 'x' + me.stockage[23] + '£c' + me.stockage[22] + '£bx' + me.stockage[24] + '£d$',
              {
                b: Math.abs(me.stockage[2]),
                c: Math.abs(me.stockage[3]),
                d: Math.abs(me.stockage[4])
              })
          } else { /// c’est b=1  (a <> 1)
            j3pAffiche(stor.tabZonesExpli[1], '', '&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;$£ax' + me.stockage[23] + '£c' + me.stockage[22] + 'x' + me.stockage[24] + '£d$',
              {
                a: me.stockage[1],
                c: Math.abs(me.stockage[3]),
                d: Math.abs(me.stockage[4])
              })
          }
        }
        break
      case 3 :
        if (Math.abs(me.stockage[1]) === 1 && Math.abs(me.stockage[2]) === 1) { ///  a=+/-1 et b=+/-1
          j3pAffiche(stor.tabZonesExpli[1], '', '&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;$' + me.stockage[211] + 'x' + me.stockage[23] + '£c' + me.stockage[24] + '£d' + me.stockage[22] + 'x$',
            {
              c: Math.abs(me.stockage[3]),
              d: Math.abs(me.stockage[4])
            })
        } else { /// on a a=1 ou b=1 (pas les deux !)
          if (Math.abs(me.stockage[1]) === 1) { /// c’est a=+/-1  (b <> 1)
            j3pAffiche(stor.tabZonesExpli[1], '', '&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;$' + me.stockage[211] + 'x' + me.stockage[23] + '£c' + me.stockage[24] + '£d' + me.stockage[22] + '£bx$',
              {
                b: Math.abs(me.stockage[2]),
                c: Math.abs(me.stockage[3]),
                d: Math.abs(me.stockage[4])
              })
          } else { /// c’est b=+/-1  (a <> 1)
            j3pAffiche(stor.tabZonesExpli[1], '', '&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;$£ax' + me.stockage[23] + '£c' + me.stockage[24] + '£d' + me.stockage[22] + 'x$',
              {
                a: me.stockage[1],
                c: Math.abs(me.stockage[3]),
                d: Math.abs(me.stockage[4])
              })
          }
        }
        break
      case 4 :
        if (Math.abs(me.stockage[1]) === 1 && Math.abs(me.stockage[2]) === 1) { ///  a=+/-1 et b=+/-1
          j3pAffiche(stor.tabZonesExpli[1], '', '&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;$£c' + me.stockage[21] + 'x' + me.stockage[24] + '£d' + me.stockage[22] + 'x$',
            {
              c: me.stockage[3],
              d: Math.abs(me.stockage[4])
            })
        } else { /// on a a=1 ou b=1 (pas les deux !)
          if (Math.abs(me.stockage[1]) === 1) { /// c’est a=+/-1  (b <> 1)
            j3pAffiche(stor.tabZonesExpli[1], '', '&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;$£c' + me.stockage[21] + 'x' + me.stockage[24] + '£d' + me.stockage[22] + '£bx$',
              {
                b: Math.abs(me.stockage[2]),
                c: me.stockage[3],
                d: Math.abs(me.stockage[4])
              })
          } else { /// c’est b=+/-1  (a <> 1)
            j3pAffiche(stor.tabZonesExpli[1], '', '&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;$£c' + me.stockage[21] + '£ax' + me.stockage[24] + '£d' + me.stockage[22] + 'x$',
              {
                a: Math.abs(me.stockage[1]),
                c: me.stockage[3],
                d: Math.abs(me.stockage[4])
              })
          }
        }
        break
      case 5 :
        if (Math.abs(me.stockage[1]) === 1 && Math.abs(me.stockage[2]) === 1) { ///  a=+/-1 et b=+/-1
          j3pAffiche(stor.tabZonesExpli[1], '', '&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;$£c' + me.stockage[21] + 'x' + me.stockage[22] + 'x' + me.stockage[24] + '£d$',
            {
              c: me.stockage[3],
              d: Math.abs(me.stockage[4])
            })
        } else { /// on a a=1 ou b=1 (pas les deux !)
          if (Math.abs(me.stockage[1]) === 1) { /// c’est a=+/-1  (b <> 1)
            j3pAffiche(stor.tabZonesExpli[1], '', '&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;$£c' + me.stockage[21] + 'x' + me.stockage[22] + '£bx' + me.stockage[24] + '£d$',
              {
                b: Math.abs(me.stockage[2]),
                c: me.stockage[3],
                d: Math.abs(me.stockage[4])
              })
          } else { /// c’est b=+/-1  (a <> 1)
            j3pAffiche(stor.tabZonesExpli[1], '', '&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;$£c' + me.stockage[21] + '£ax' + me.stockage[22] + 'x' + me.stockage[24] + '£d$',
              {
                a: Math.abs(me.stockage[1]),
                c: me.stockage[3],
                d: Math.abs(me.stockage[4])
              })
          }
        }
        break
    }
  }

  function afficheExplicationPluslitterokenumCoeff1 () {
    switch (me.stockage[11]) {
      case 1 :
        me.stockage[0] = j3pMonome(1, 1, me.stockage[5]) + j3pMonome(2, 0, me.stockage[3])
        if (Math.abs(me.stockage[1]) === 1 && Math.abs(me.stockage[2]) === 1) { ///  a=+/-1 et b=+/-1
          j3pAffiche(stor.tabZonesExpli[1], '', '&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;$£c' + me.stockage[21] + 'x' + me.stockage[22] + 'x$',
            {
              c: me.stockage[3],
              d: Math.abs(me.stockage[4])
            })
        } else { /// on a a=1 ou b=1 (pas les deux !)
          if (Math.abs(me.stockage[1]) === 1) { /// c’est a=+/-1  (b <> 1)
            j3pAffiche(stor.tabZonesExpli[1], '', '&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;$£c' + me.stockage[21] + 'x' + me.stockage[22] + '£bx$',
              {
                b: Math.abs(me.stockage[2]),
                c: me.stockage[3],
                d: Math.abs(me.stockage[4])
              })
          } else { /// c’est b=+/-1  (a <> 1)
            j3pAffiche(stor.tabZonesExpli[1], '', '&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;$£c' + me.stockage[21] + '£ax' + me.stockage[22] + 'x$',
              {
                a: Math.abs(me.stockage[1]),
                c: me.stockage[3],
                d: Math.abs(me.stockage[4])
              })
          }
        }
        break
      case 2 :
        me.stockage[0] = j3pMonome(1, 1, me.stockage[5]) + j3pMonome(2, 0, me.stockage[3])
        if (Math.abs(me.stockage[1]) === 1 && Math.abs(me.stockage[2]) === 1) { ///  a=+/-1 et b=+/-1
          j3pAffiche(stor.tabZonesExpli[1], '', '&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;$' + me.stockage[211] + 'x' + me.stockage[23] + '£c' + me.stockage[22] + 'x$',
            {
              c: Math.abs(me.stockage[3]),
              d: Math.abs(me.stockage[4])
            })
        } else { /// on a a=1 ou b=1 (pas les deux !)
          if (Math.abs(me.stockage[1]) === 1) { /// c’est a=1  (b <> 1)
            j3pAffiche(stor.tabZonesExpli[1], '', '&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;$' + me.stockage[211] + 'x' + me.stockage[23] + '£c' + me.stockage[22] + '£bx$',
              {
                b: Math.abs(me.stockage[2]),
                c: Math.abs(me.stockage[3]),
                d: Math.abs(me.stockage[4])
              })
          } else { /// c’est b=1  (a <> 1)
            j3pAffiche(stor.tabZonesExpli[1], '', '&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;$£ax' + me.stockage[23] + '£c' + me.stockage[22] + 'x$',
              {
                a: me.stockage[1],
                c: Math.abs(me.stockage[3]),
                d: Math.abs(me.stockage[4])
              })
          }
        }
        break
      case 3 :
        me.stockage[0] = j3pMonome(1, 1, me.stockage[7]) + j3pMonome(2, 0, me.stockage[3])
        if (Math.abs(me.stockage[4]) === 1) { // d=+/-1
          if (Math.abs(me.stockage[1]) === 1 && Math.abs(me.stockage[2]) === 1) { ///  a=+/-1 et b=+/-1
            j3pAffiche(stor.tabZonesExpli[1], '', '&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;$' + me.stockage[211] + 'x' + me.stockage[22] + 'x' + me.stockage[23] + '£c' + me.stockage[24] + 'x$',
              { c: Math.abs(me.stockage[3]) })
          } else { /// on a a=1 ou b=1 (pas les deux !)
            if (Math.abs(me.stockage[1]) === 1) { /// c’est a=1  (b <> 1)
              j3pAffiche(stor.tabZonesExpli[1], '', '&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;$' + me.stockage[211] + 'x' + me.stockage[22] + '£bx' + me.stockage[23] + '£c' + me.stockage[24] + 'x$',
                {
                  b: Math.abs(me.stockage[2]),
                  c: Math.abs(me.stockage[3])
                })
            } else { /// c’est b=1  (a <> 1)
              j3pAffiche(stor.tabZonesExpli[1], '', '&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;$£ax' + me.stockage[22] + 'x' + me.stockage[23] + '£c' + me.stockage[24] + 'x$',
                {
                  a: me.stockage[1],
                  c: Math.abs(me.stockage[3])
                })
            }
          }
        } else { // d<>+/-1
          if (Math.abs(me.stockage[1]) === 1 && Math.abs(me.stockage[2]) === 1) { ///  a=+/-1 et b=+/-1
            j3pAffiche(stor.tabZonesExpli[1], '', '&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;$' + me.stockage[211] + 'x' + me.stockage[22] + 'x' + me.stockage[23] + '£c' + me.stockage[24] + '£dx$',
              {
                c: Math.abs(me.stockage[3]),
                d: Math.abs(me.stockage[4])
              })
          } else { /// on a a=1 ou b=1 (pas les deux !)
            if (Math.abs(me.stockage[1]) === 1) { /// c’est a=1  (b <> 1)
              j3pAffiche(stor.tabZonesExpli[1], '', '&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;$' + me.stockage[211] + 'x' + me.stockage[22] + '£bx' + me.stockage[23] + '£c' + me.stockage[24] + '£dx$',
                {
                  b: Math.abs(me.stockage[2]),
                  c: Math.abs(me.stockage[3]),
                  d: Math.abs(me.stockage[4])
                })
            } else { /// c’est b=1  (a <> 1)
              j3pAffiche(stor.tabZonesExpli[1], '', '&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;$£ax' + me.stockage[22] + 'x' + me.stockage[23] + '£c' + me.stockage[24] + '£dx$',
                {
                  a: me.stockage[1],
                  c: Math.abs(me.stockage[3]),
                  d: Math.abs(me.stockage[4])
                })
            }
          }
        }
        break
      case 4 :
        me.stockage[0] = j3pMonome(1, 1, me.stockage[7]) + j3pMonome(2, 0, me.stockage[3])
        if (Math.abs(me.stockage[4]) === 1) { // d=+/-1
          if (Math.abs(me.stockage[1]) === 1 && Math.abs(me.stockage[2]) === 1) { ///  a=+/-1 et b=+/-1
            j3pAffiche(stor.tabZonesExpli[1], '', '&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;$' + me.stockage[211] + 'x' + me.stockage[23] + '£c' + me.stockage[22] + 'x' + me.stockage[24] + 'x$',
              { c: Math.abs(me.stockage[3]) })
          } else { /// on a a=1 ou b=1 (pas les deux !)
            if (Math.abs(me.stockage[1]) === 1) { /// c’est a=1  (b <> 1)
              j3pAffiche(stor.tabZonesExpli[1], '', '&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;$' + me.stockage[211] + 'x' + me.stockage[23] + '£c' + me.stockage[22] + '£bx' + me.stockage[24] + 'x$',
                {
                  b: Math.abs(me.stockage[2]),
                  c: Math.abs(me.stockage[3])
                })
            } else { /// c’est b=1  (a <> 1)
              j3pAffiche(stor.tabZonesExpli[1], '', '&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;$£ax' + me.stockage[23] + '£c' + me.stockage[22] + 'x' + me.stockage[24] + 'x$',
                {
                  a: me.stockage[1],
                  c: Math.abs(me.stockage[3])
                })
            }
          }
        } else { // d<>+/-1
          if (Math.abs(me.stockage[1]) === 1 && Math.abs(me.stockage[2]) === 1) { ///  a=+/-1 et b=+/-1
            j3pAffiche(stor.tabZonesExpli[1], '', '&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;$' + me.stockage[211] + 'x' + me.stockage[23] + '£c' + me.stockage[22] + 'x' + me.stockage[24] + '£dx$',
              {
                c: Math.abs(me.stockage[3]),
                d: Math.abs(me.stockage[4])
              })
          } else { /// on a a=1 ou b=1 (pas les deux !)
            if (Math.abs(me.stockage[1]) === 1) { /// c’est a=1  (b <> 1)
              j3pAffiche(stor.tabZonesExpli[1], '', '&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;$' + me.stockage[211] + 'x' + me.stockage[23] + '£c' + me.stockage[22] + '£bx' + me.stockage[24] + '£dx$',
                {
                  b: Math.abs(me.stockage[2]),
                  c: Math.abs(me.stockage[3]),
                  d: Math.abs(me.stockage[4])
                })
            } else { /// c’est b=1  (a <> 1)
              j3pAffiche(stor.tabZonesExpli[1], '', '&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;$£ax' + me.stockage[23] + '£c' + me.stockage[22] + 'x' + me.stockage[24] + '£dx$',
                {
                  a: me.stockage[1],
                  c: Math.abs(me.stockage[3]),
                  d: Math.abs(me.stockage[4])
                })
            }
          }
        }
        break
    }
  }

  function enonceMain () {
    do {
      const [min1, max1] = j3pGetBornesIntervalle(ds.entier1)
      me.stockage[1] = j3pGetRandomInt(min1, max1)
      const [min2, max2] = j3pGetBornesIntervalle(ds.entier1)
      me.stockage[2] = j3pGetRandomInt(min2, max2)
      const [min3, max3] = j3pGetBornesIntervalle(ds.entier1)
      me.stockage[3] = j3pGetRandomInt(min3, max3)
      const [min4, max4] = j3pGetBornesIntervalle(ds.entier1)
      me.stockage[4] = j3pGetRandomInt(min4, max4)
    } while (me.stockage[1] === 0 || me.stockage[2] === 0 || me.stockage[3] === 0 || me.stockage[4] === 0)

    // a voir si tout ça utile, surement en correction

    me.stockage[5] = me.stockage[1] + me.stockage[2]
    me.stockage[6] = me.stockage[3] + me.stockage[4]
    me.stockage[7] = me.stockage[1] + me.stockage[2] + me.stockage[4]

    me.stockage[21] = signeNombre(me.stockage[1])
    me.stockage[22] = signeNombre(me.stockage[2])
    me.stockage[23] = signeNombre(me.stockage[3])
    me.stockage[24] = signeNombre(me.stockage[4])
    me.stockage[26] = signeNombre(me.stockage[6])

    // création du conteneur dans lequel se trouveront toutes les consignes
    stor.conteneur = j3pDiv(me.zones.MG, {
      contenu: '',
      style: me.styles.etendre('moyen.enonce', { padding: '15px' })
    })// j’ai viré ,coord:[0,0] qui donne une position absolute à la div et non plus relative comme son parent

    stor.enonce = j3pDiv(stor.conteneur, {
      contenu: '',
      style: me.styles.etendre('moyen.enonce')
    }
    )
    const kelchoix = j3pGetRandomInt(1, 2) /// permet de définir 2littéraux et 2 numériques ou plus de littéraux que de numériques ou ax+bx+c ou ax+c+d
    // si kelchoix vaut 1, on affiche ax+bx+c+d dans un certain ordre
    // sinon on affiche ax+bx+c ou ax+c+d OU  ax+bx+c+dx
    switch (kelchoix) {
      case 1 :
        // ce qui suit lance la création initiale de la figure
        depart('ax+bx+c+d')
        /// //////////////////////////////////////Fin de l’initialisation de la figure MathGraph32'

        me.stockage[10] = '2litteroet2num'
        me.stockage[11] = j3pGetRandomInt(1, 5) // permet de définir l’ordre des termes

        if (Math.abs(me.stockage[1]) === 1 || Math.abs(me.stockage[2]) === 1) {
          me.stockage[211] = signeNombre1(me.stockage[1])
          afficheEnonce2litteroet2numCoeff1()
        } else {
          afficheEnonce2litteroet2num()
        }

        break
      case 2 :
        /// //////////////////////////////////////Fin de l’initialisation de la figure MathGraph32'

        me.stockage[10] = 'pluslitterokenum'
        me.stockage[11] = j3pGetRandomInt(1, 4)
        if (me.stockage[11] === 1 || me.stockage[11] === 2) { //  c+ax+bx  ou   ax+c+bx
          depart('ax+bx+c')
          if (Math.abs(me.stockage[1]) === 1 || Math.abs(me.stockage[2]) === 1) {
            me.stockage[211] = signeNombre1(me.stockage[1])
            afficheEnoncePluslitterokenumCoeff1()
          } else {
            afficheEnoncePluslitterokenum()
          }
        } else { //    ax+bx+c+dx    ou     ax+c+bx+dx
          depart('ax+bx+c+dx')
          if (Math.abs(me.stockage[1]) === 1 || Math.abs(me.stockage[2]) === 1 || Math.abs(me.stockage[4]) === 1) {
            me.stockage[211] = signeNombre1(me.stockage[1])
            afficheEnoncePluslitterokenumCoeff1()
          } else {
            afficheEnoncePluslitterokenum()
          }
        }
        break
    }
    stor.zoneInput = stor.elts.inputmqList[0]
    mqRestriction(stor.zoneInput, '\\d,.x+-*')
    j3pFocus(stor.zoneInput)

    stor.zoneExpli = j3pDiv(stor.conteneur, { paddingTop: '15px' })
    stor.tabZonesExpli = []
    for (let i = 1; i <= 3; i++) {
      stor.tabZonesExpli.push(j3pDiv(stor.zoneExpli))
    }
    const zonesSaisie = [stor.zoneInput]
    stor.fctsValid = new ValidationZones({
      zones: zonesSaisie,
      validePerso: [stor.zoneInput],
      parcours: me
    })
    stor.divCorr = j3pDiv(me.zones.MD, { contenu: '', coord: [20, 100], style: me.styles.moyen.correction })
    // Obligatoire
    me.finEnonce()
  }

  function depart () {
    // txt html de la figure
    // ax+bx+c+d
    const fig = 'TWF0aEdyYXBoSmF2YTEuMAAAABE+TMzNAAJmcv###wEA#wEAAAAAAAAAAAWIAAACrgAAAQEAAAAAAAAAAAAAAAn#####AAAAAQAKQ0NhbGNDb25zdAD#####AAJwaQAWMy4xNDE1OTI2NTM1ODk3OTMyMzg0Nv####8AAAABAApDQ29uc3RhbnRlQAkh+1RELRj#####AAAAAQAFQ0ZvbmMA#####wABZgABeP####8AAAACABFDVmFyaWFibGVGb3JtZWxsZQAAAAAAAXgAAAACAP####8AA3JlcAABMAAAAAEAAAAAAAAAAAABeP####8AAAADABBDVGVzdEVxdWl2YWxlbmNlAP####8ABnJlc29sdQAAAAEAAAACAQAAAAABP#AAAAAAAAABAAAAAgD#####AAR6ZXJvABJhYnMoeCk8MC4wMDAwMDAwMDH#####AAAAAQAKQ09wZXJhdGlvbgT#####AAAAAgAJQ0ZvbmN0aW9uAAAAAAMAAAAAAAAAAT4RLgvoJtaVAAF4#####wAAAAEAB0NDYWxjdWwA#####wACeDEABTEuMjM3AAAAAT#zysCDEm6YAAAABwD#####AAJ4MgAFMS45OTMAAAABP##jU#fO2RcAAAAHAP####8AAngzAAYzLjExNDcAAAABQAjq59Vmz0IAAAAHAP####8ABWV4YWN0ADt6ZXJvKGYoeDEpLXJlcCh4MSkpJnplcm8oZih4MiktcmVwKHgyKSkmemVybyhmKHgzKS1yZXAoeDMpKQAAAAUKAAAABQr#####AAAAAQAOQ0FwcGVsRm9uY3Rpb24AAAAEAAAABQEAAAAIAAAAAf####8AAAABAA9DUmVzdWx0YXRWYWxldXIAAAAFAAAACAAAAAIAAAAJAAAABQAAAAgAAAAEAAAABQEAAAAIAAAAAQAAAAkAAAAGAAAACAAAAAIAAAAJAAAABgAAAAgAAAAEAAAABQEAAAAIAAAAAQAAAAkAAAAHAAAACAAAAAIAAAAJAAAAB################w=='
    stor.liste = stor.mtgAppLecteur.createList(fig)
  }

  switch (me.etat) {
    case 'enonce': {
      // code exécuté au lancement de la section
      if (me.debutDeLaSection) {
        // Construction de la page
        me.surcharge()
        me.construitStructurePage('presentation1')
        // par convention, me.stockage[0] contient la solution
        // les autres peuvent contenir ce que vous voulez, en général des valeurs que l’on doit mémoriser entre les différents appels de la section
        me.stockage = [0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0]
        /*
                 Par convention,`
                `   me.typederreurs[0] = nombre de bonnes réponses
                    me.typederreurs[1] = nombre de fois où l’élève a utilisé une chance supplémentaire
                    me.typederreurs[2] = nombre de mauvaises réponses
                    me.typederreurs[10] = nombre de fois où l’élève a eu faux à cause d’un temps limite
                    LES AUTRES peuvent être utilisées par la détermination de la pe dans le cas d’une section qualitative
                 */
        me.typederreurs = [0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0]
        me.afficheTitre(textes.titreExo)
        if (ds.indication) me.indication(me.zonesElts.IG, ds.indication)// chargement mtg
        getMtgCore({ withMathJax: true })
          .then(
            // success
            (mtgAppLecteur) => {
              stor.mtgAppLecteur = mtgAppLecteur
              enonceMain()
            },
            // failure
            (error) => {
              j3pShowError(error, { message: 'mathgraph n’est pas correctement chargé', mustNotify: true })
            })
          // plantage dans le code de success
          .catch(j3pShowError)
      } else {
        // A chaque répétition, on nettoie la scène
        me.videLesZones()
        enonceMain()
      }
      break // case "enonce":
    }

    case 'correction': {
      // On teste si une réponse a été saisie
      const fctsValid = stor.fctsValid// ici je crée juste une variable pour raccourcir le nom de l’oject fctsValid
      const reponse = fctsValid.validationGlobale()
      let exact, note
      if (reponse.aRepondu) {
        const expSolution = traiteMathQuill(String(me.stockage[0]))
        const repeleve = $(stor.zoneInput).mathquill('latex')
        const expEleve = traiteMathQuill(repeleve)
        stor.liste.giveFormula2('f', expSolution)
        stor.liste.giveFormula2('rep', expEleve)
        stor.liste.calculateNG(false)

        exact = stor.liste.valueOf('exact')
        const resolu = stor.liste.valueOf('resolu')
        note = exact && resolu
        if (!note) fctsValid.zones.bonneReponse[0] = false
        fctsValid.coloreUneZone(stor.zoneInput)
      }

      if (!reponse.aRepondu && (!me.isElapsed)) {
        me.reponseManquante(stor.divCorr)
        $(stor.zoneInput).focus()
        return me.finCorrection()
      }

      // Une réponse a été saisie
      if (note) {
        // Bonne réponse
        me.score++
        stor.divCorr.style.color = me.styles.cbien
        stor.divCorr.innerHTML = cBien

        me.typederreurs[0]++
        return me.finCorrection('navigation', true)
      }

      // Pas de bonne réponse
      stor.divCorr.style.color = me.styles.cfaux

      // A cause de la limite de temps :
      if (me.isElapsed) { // limite de temps
        stor.divCorr.innerHTML = tempsDepasse
        me.typederreurs[10]++
        afficheCorrection()
        return me.finCorrection('navigation', true)
      }

      // Réponse fausse :
      if (exact) {
        stor.divCorr.innerHTML = textes.pas_simplifie
      } else {
        stor.divCorr.innerHTML = cFaux
      }

      if (me.essaiCourant < ds.nbchances) {
        stor.divCorr.innerHTML += '<br>' + essaieEncore
        $(stor.zoneInput).focus()
        me.typederreurs[1]++
        return me.finCorrection()
      }

      // Erreur au dernier essai
      stor.divCorr.innerHTML += '<br>'

      afficheCorrection()
      stor.zoneExpli.style.color = me.styles.toutpetit.correction.color
      stor.tabZonesExpli[0].innerHTML = textes.phrase20

      switch (me.stockage[10]) {
        case '2litteroet2num':
          if (Math.abs(me.stockage[1]) === 1 || Math.abs(me.stockage[2]) === 1) {
            afficheExplication2Litteroet2numCoeff1()
          } else {
            afficheExplication2litterauxEt2num()
          }

          j3pAffiche(stor.tabZonesExpli[2], '', '$=(£a' + me.stockage[22] + '£b)x+(£c' + me.stockage[24] + '£d)$\n',
            {
              a: me.stockage[1],
              b: Math.abs(me.stockage[2]),
              c: me.stockage[3],
              d: Math.abs(me.stockage[4])
            }
          )
          j3pMathsAjouteDans(stor.tabZonesExpli[2], {
            content: '$=$' + j3pEcritBienAxPlusB(me.stockage[5], me.stockage[6]),
            parametres: {
              a: me.stockage[5],
              b: me.stockage[6]
            }
          })

          break
        case 'pluslitterokenum':
          if (Math.abs(me.stockage[11]) === 1 || Math.abs(me.stockage[11]) === 2) {
            if (Math.abs(me.stockage[1]) === 1 || Math.abs(me.stockage[2]) === 1) {
              afficheExplicationPluslitterokenumCoeff1()
            } else {
              afficheExplicationPlusLitterOkEnum()
            }

            j3pAffiche(stor.tabZonesExpli[2], '', '$=(£a' + me.stockage[22] + '£b)x' + me.stockage[23] + '£c$\n',
              {
                a: me.stockage[1],
                b: Math.abs(me.stockage[2]),
                c: Math.abs(me.stockage[3])
              }
            )
            j3pMathsAjouteDans(stor.tabZonesExpli[2], {
              content: '$=$' + j3pEcritBienAxPlusB(me.stockage[5], me.stockage[3]),
              parametres: {
                a: me.stockage[5],
                b: me.stockage[3]
              }
            })
          } else { //  me.stockage[11])=3 ou 4  (ax+bx+c+dx ou ax+c+bx+dx)
            if (Math.abs(me.stockage[1]) === 1 || Math.abs(me.stockage[2]) === 1 || Math.abs(me.stockage[4]) === 1) {
              afficheExplicationPluslitterokenumCoeff1()
            } else {
              afficheExplicationPlusLitterOkEnum()
            }

            j3pAffiche(stor.tabZonesExpli[2], '', '$=(£a' + me.stockage[22] + '£b' + me.stockage[24] + '£d)x' + me.stockage[23] + '£c$\n',
              {
                a: me.stockage[1],
                b: Math.abs(me.stockage[2]),
                c: Math.abs(me.stockage[3]),
                d: Math.abs(me.stockage[4])
              }
            )
            j3pMathsAjouteDans(stor.tabZonesExpli[2], {
              content: '$=$' + j3pEcritBienAxPlusB(me.stockage[7], me.stockage[3]),
              parametres: {
                a: me.stockage[7],
                b: me.stockage[3]
              }
            })
          }
          break
      }

      me.typederreurs[2]++
      me.finCorrection('navigation', true)
      break // case "correction":
    }

    case 'navigation':
      if (!this.sectionTerminee()) this.etat = 'enonce'
      this.finNavigation()
      break
  }
}
