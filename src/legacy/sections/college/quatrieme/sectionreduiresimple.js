import $ from 'jquery'
import { j3pAddElt, j3pDetruit, j3pEcritBienAxCarre, j3pEcritBienAxPlusB, j3pEmpty, j3pFocus, j3pGetRandomInt, j3pMonome, j3pPaletteMathquill, j3pShowError, j3pStyle } from 'src/legacy/core/functions'
import ValidationZones from 'src/legacy/outils/calculatrice/ValidationZones'
import { j3pAffiche, j3pMathsAjouteDans, mqRestriction, traiteMathQuillSansMultImplicites } from 'src/lib/mathquill/functions'
import { getMtgCore } from 'src/lib/outils/mathgraph'
import textesGeneriques from 'src/lib/core/textes'

const { cBien, cFaux, essaieEncore, tempsDepasse } = textesGeneriques

/*
    Isabelle FRITSCH
    aout_septembre 2013
    Revu par Yves Biton 2019 pour utiliser mtg32, puis mathgraph
    +correction par Rémi en mai 2019

Accès à la section :
Exemple de Graphe de test
   me.html?graphe=[1,"reduiresommeproduitsimplelitteral",[{pe:">=0",nn:"fin",conclusion:"fin"};

/*

Variables :
 me.stockage[0]== solution ;
 me.stockage[1] et me.stockage[2] == nombres a et b dans ax +bx ou... (nombre1 et nombre2 dans ennonce);
 me.stockage[5] == me.stockage[1]+me.stockage[2];
 me.stockage[6] == me.stockage[1]*me.stockage[2];
 me.stockage[10] ==  somme ou produit
 me.stockage[11] ==  forme ax +bx ou a +bx ou ax +b
 me.stockage[20] == signeNombre(me.stockage[2]);
 me.stockage[21] == signeNombre(me.stockage[1]);
 me.stockage[30] == peut copier ou ne peut pas copier énonce

*/
export const params = {
  outils: ['calculatrice', 'mathquill'],
  /**
   * Ce qui est paramétrable dans l’éditeur de graphe
   * @type {LegacySectionParamsParametres}
   */
  parametres: [
    ['nbrepetitions', 10, 'entier', 'aucun'],
    ['indication', '', 'string', 'aucun'],
    ['limite', 0, 'entier', 'Temps disponible par répétition, en secondes'],
    ['nbchances', 2, 'entier', 'Nombre d’erreurs possibles (pour tenter de les corriger)'],
    ['entier1', '[-10;10]', 'string', 'Description du paramètre', 'Intervalle auquel appartient le nombre a dans ax+bx ou a+bx ou ax*bx ou a*bx'],
    ['entier2', '[-10;10]', 'string', 'Description du paramètre', 'Intervalle auquel appartient le nombre b dans ax+bx ou ax+b ou ax*bx ou ax*b'],
    ['typequestion', 'lesdeux', 'liste', 'type de calcul à réduire (par défaut, on propose "lesdeux" c’est-à-dire aussi bien des sommes que des produits).', ['sommedelitteraux', 'produitdelitteraux', 'lesdeux']]
  ]
}

const textes = {
  titre_exo: 'Réduire une expression littérale',
  phrase1: ' Réduire si possible (<i>sinon, recopier l’énoncé</i>)&nbsp;:',
  phrase8: 'L’expression proposée peut être réduite ; essaie encore !',
  phrase9: 'Tu as recopié l’énoncé alors que l’on peut réduire ; essaie encore !',
  phrase10: 'La réponse était ',
  phrase11: 'En effet, ',
  phrase12: 'En effet, on ne peut pas réduire ; il fallait recopier l’énoncé ',
  phrase20: 'Attention, on ajoute ou on soustrait des ',
  phrase21: 'Attention, on ne peut pas additionner (soustraire) des lettres et des nombres !',
  phrase22: 'Attention, on multiplie des '
}

/**
 * section reduiresimple
 * @this {Parcours}
 */
export default function main () {
  const me = this
  const stor = me.storage
  const ds = me.donneesSection
  const figure = 'TWF0aEdyYXBoSmF2YTEuMAAAABE+TMzNAAJmcv###wEA#wEAAAAAAAAAAAWHAAACrgAAAQEAAAAAAAAAAAAAAAT#####AAAAAQAKQ0NhbGNDb25zdAD#####AAJwaQAWMy4xNDE1OTI2NTM1ODk3OTMyMzg0Nv####8AAAABAApDQ29uc3RhbnRlQAkh+1RELRj#####AAAAAQAFQ0ZvbmMA#####wABZgABeP####8AAAACABFDVmFyaWFibGVGb3JtZWxsZQAAAAAAAXgAAAACAP####8AA3JlcAABMAAAAAEAAAAAAAAAAAABeP####8AAAADABBDVGVzdEVxdWl2YWxlbmNlAP####8ACmVxdWl2YWxlbnQAAAABAAAAAgEAAAAAAT#wAAAAAAAAAf###############w=='
  // stor.cListeObjets affecté au chargement mtg, c’est un CListeObjets

  function enonceMain () {
    let nombre1, nombre2
    do {
      nombre1 = j3pGetRandomInt(me.donneesSection.entier1)
      nombre2 = j3pGetRandomInt(me.donneesSection.entier2)
    } while (nombre1 === 0 || nombre2 === 0)

    me.stockage[1] = nombre1
    me.stockage[2] = nombre2
    me.stockage[5] = me.stockage[1] + me.stockage[2]
    /** @type {number} */
    me.stockage[6] = me.stockage[1] * me.stockage[2]

    me.stockage[20] = signeNombre(me.stockage[2])
    me.stockage[21] = signeNombre(me.stockage[1])

    const kelexpress = j3pGetRandomInt(1, 2)
    me.stockage[11] = j3pGetRandomInt(1, 3)

    stor.zoneConteneur = j3pAddElt(me.zonesElts.MG, 'div', '', { style: me.styles.etendre('moyen.enonce', { padding: '20px' }) })
    stor.zoneEnonce = j3pAddElt(stor.zoneConteneur, 'div')
    j3pAffiche(stor.zoneEnonce, '', textes.phrase1)
    stor.zoneEnonce2 = j3pAddElt(stor.zoneConteneur, 'div')
    switch (me.donneesSection.typequestion) {
      case 'sommedelitteraux':
        me.stockage[10] = 'sommelitteraux'
        if ((Math.abs(me.stockage[1]) === 1) || (Math.abs(me.stockage[2]) === 1)) {
          afficheEnonceSommelitterauxCoeff1()
        } else {
          afficheEnonceSommelitteraux()
        }
        break

      case 'produitdelitteraux':
        me.stockage[10] = 'produitlitteraux'
        if ((Math.abs(me.stockage[1]) === 1) || (Math.abs(me.stockage[2]) === 1)) {
          afficheEnonceProduitlitterauxCoeff1()
        } else {
          afficheEnonceProduitlitteraux()
        }
        break

      case 'lesdeux':
        switch (kelexpress) {
          case 1: // sommelitteraux
            me.stockage[10] = 'sommelitteraux'
            if ((Math.abs(me.stockage[1]) === 1) || (Math.abs(me.stockage[2]) === 1)) {
              afficheEnonceSommelitterauxCoeff1()
            } else {
              afficheEnonceSommelitteraux()
            }
            break

          case 2: // produitdefacteurslitteraux
            me.stockage[10] = 'produitlitteraux'
            if ((Math.abs(me.stockage[1]) === 1) || (Math.abs(me.stockage[2]) === 1)) {
              afficheEnonceProduitlitterauxCoeff1()
            } else {
              afficheEnonceProduitlitteraux()
            }
            break
        }
        break
    }

    stor.divPalette = j3pAddElt(stor.zoneConteneur, 'div')
    j3pPaletteMathquill(stor.divPalette, stor.zoneInput, { liste: ['puissance'] })
    me.logIfDebug('stockage:', me.stockage)
    /*
     C’est dans le DIV "explications" que les explications sont écrites.
     En général, ces explications figurent à côté de l’énoncé.
     En revanche, en général, on écrit "C’est bon", "C’est faux" , etc, dans la partie Droite de la scène (cad dans me.zonesElts.MD)
     et dans le DIV correction
    */
    stor.zoneIndic = j3pAddElt(stor.zoneConteneur, 'div')
    j3pStyle(stor.zoneIndic, { paddingTop: '40px', color: me.styles.cfaux })
    stor.zoneExpli = j3pAddElt(stor.zoneConteneur, 'div')
    j3pStyle(stor.zoneExpli, { paddingTop: '15px' })
    stor.zoneCorr = j3pAddElt(me.zonesElts.MD, 'div', '', { style: me.styles.etendre('moyen.enonce', { padding: '15px' }) })
    mqRestriction(stor.zoneInput, '\\d,.x+\\-\\*')
    j3pFocus(stor.zoneInput)

    const zones = [stor.zoneInput]
    stor.fctsValid = new ValidationZones({ zones, validePerso: zones, parcours: me })
    me.finEnonce()
  } // enonceMain

  function traiteMathQuill (ch) {
    ch = traiteMathQuillSansMultImplicites(ch)
    ch = stor.cListeObjets.addImplicitMult(ch)// Traitement des multiplications implicites
    return ch
  }

  function signeNombre (nombre) {
    return (nombre < 0) ? '-' : '+'
  }

  function remplaceSigne1 (nombre) {
    return (nombre === 1) ? '' : '-'
  }

  function parentheseOuvre (nombre) {
    return (nombre > 0) ? '' : '('
  }

  function parentheseFerme (nombre) {
    return (nombre > 0) ? '' : ')'
  }

  function afficheEnonceSommelitteraux () {
    let consigne
    switch (me.stockage[11]) {
      case 1: //  ax+bx
        me.stockage[30] = 'pascopie'
        me.stockage[0] = (me.stockage[5]) + 'x'
        me.stockage[31] = j3pMonome(1, 1, me.stockage[1]) + j3pMonome(2, 1, me.stockage[2]) // réponse proposée par l’élève s’il recopie l’énoncé
        me.stockage[32] = j3pMonome(1, 1, me.stockage[2]) + j3pMonome(2, 1, me.stockage[1]) // autre réponse possible si l’élève recopie l’énoncé
        if (me.stockage[5] === 0) { me.stockage[0] = 0 }
        if (me.stockage[5] === 1) { me.stockage[0] = 'x' }
        if (me.stockage[5] === -1) { me.stockage[0] = '-x' }
        me.stockage[99] = (me.stockage[5]) + 'x^2' // C’est une erreur que l’élève peut faire ; varaiable sert dans les indications après 1er esssai
        consigne = '$£a x ' + me.stockage[20] + ' £b x =$ &1&'
        break
      case 2: //  a+bx
        me.stockage[30] = 'okcopie'
        me.stockage[0] = j3pMonome(1, 1, me.stockage[2]) + j3pMonome(2, 0, me.stockage[1])
        me.stockage[31] = j3pMonome(1, 0, me.stockage[1]) + j3pMonome(2, 1, me.stockage[2]) // réponse proposée par l’élève s’il recopie l’énoncé
        me.stockage[32] = j3pMonome(1, 1, me.stockage[2]) + j3pMonome(2, 0, me.stockage[1]) // autre réponse possible si l’élève recopie l’énoncé
        me.stockage[99] = me.stockage[5] + 'x' // C’est une erreur que l’élève peut faire ; varaiable sert dans les indications après 1er esssai
        consigne = '$£a ' + me.stockage[20] + ' £b x =$ &1&'
        break
      case 3: //  ax+b
        me.stockage[30] = 'okcopie'
        me.stockage[0] = j3pMonome(1, 1, me.stockage[1]) + j3pMonome(2, 0, me.stockage[2])
        me.stockage[31] = j3pMonome(1, 1, me.stockage[1]) + j3pMonome(2, 0, me.stockage[2])// réponse proposée par l’élève s’il recopie l’énoncé
        me.stockage[32] = j3pMonome(1, 0, me.stockage[2]) + j3pMonome(2, 1, me.stockage[1]) // autre réponse possible si l’élève recopie l’énoncé
        me.stockage[99] = me.stockage[5] + 'x' // C’est une erreur que l’élève peut faire ; varaiable sert dans les indications après 1er esssai
        consigne = '$£a x ' + me.stockage[20] + ' £b =$ &1&'
        break
    }
    const elt = j3pAffiche(stor.zoneEnonce2, '', consigne,
      {
        a: me.stockage[1],
        b: Math.abs(me.stockage[2]),
        inputmq1: { texte: '', correction: me.stockage[0] }
      }
    )
    stor.zoneInput = elt.inputmqList[0]
  }

  function afficheEnonceSommelitterauxCoeff1 () {
    let consigne
    let signe
    switch (me.stockage[11]) {
      case 1: //  ax+bx
        me.stockage[30] = 'pascopie'
        me.stockage[0] = (me.stockage[5]) + 'x'
        me.stockage[31] = j3pMonome(1, 1, me.stockage[1]) + j3pMonome(2, 1, me.stockage[2]) // réponse proposée par l’élève s’il recopie l’énoncé
        me.stockage[32] = j3pMonome(1, 1, me.stockage[2]) + j3pMonome(2, 1, me.stockage[1]) // autre réponse possible si l’élève recopie l’énoncé
        if (me.stockage[5] === 0) me.stockage[0] = 0 // pour qu'à la correction s’affiche 0 au lieu de 0x
        if (me.stockage[5] === 1) me.stockage[0] = 'x' // pour qu'à la correction s’affiche x au lieu de 1x
        if (me.stockage[5] === -1) me.stockage[0] = '-x' // pour qu'à la correction s’affiche -x au lieu de -1x
        me.stockage[99] = (me.stockage[5]) + 'x^2' // C’est une erreur que l’élève peut faire ; variable sert dans les indications après 1er esssai
        if ((Math.abs(me.stockage[1]) === 1) && (Math.abs(me.stockage[2]) === 1)) { // a =+/-1  et b = +/-1
          signe = remplaceSigne1(me.stockage[1])
          consigne = '$' + signe + 'x ' + me.stockage[20] + ' x =$ &1&'
        } else { // a <> 1  ou   b <> 1
          if (Math.abs(me.stockage[1]) === 1) { // seulement a =+/-1
            signe = remplaceSigne1(me.stockage[1])
            consigne = '$' + signe + 'x ' + me.stockage[20] + ' £b x =$ &1&'
          }
          if (Math.abs(me.stockage[2]) === 1) { // seulement b =+/-1
            consigne = '$£a x ' + me.stockage[20] + ' x =$ &1&'
          }
        }
        break
      case 2: // a+bx
        me.stockage[30] = 'okcopie'
        me.stockage[31] = j3pMonome(1, 0, me.stockage[1]) + j3pMonome(2, 1, me.stockage[2]) // réponse proposée par l’élève s’il recopie l’énoncé
        me.stockage[32] = j3pMonome(1, 1, me.stockage[2]) + j3pMonome(2, 0, me.stockage[1]) // autre réponse possible si l’élève recopie l’énoncé
        if (Math.abs(me.stockage[2]) === 1) { // b = +/- 1
          signe = remplaceSigne1(me.stockage[2])
          me.stockage[0] = (signe) + 'x' + j3pMonome(2, 0, me.stockage[1])
          me.stockage[99] = (me.stockage[5]) + 'x' // C’est une erreur que l’élève peut faire ; variable sert dans les indications après 1er esssai
          consigne = '$£a ' + me.stockage[20] + ' x =$ &1&'
        } else { // b <> +/-1  c’est a =+/-1
          me.stockage[0] = j3pMonome(1, 1, me.stockage[2]) + j3pMonome(2, 0, me.stockage[1])
          me.stockage[99] = (me.stockage[5]) + 'x' // C’est une erreur que l’élève peut faire ; variable sert dans les indications après 1er esssai
          consigne = '$£a ' + me.stockage[20] + ' £b x =$ &1&'
        }
        break
      case 3: // ax+b
        me.stockage[30] = 'okcopie'
        me.stockage[31] = j3pMonome(1, 1, me.stockage[1]) + j3pMonome(2, 0, me.stockage[2]) // réponse proposée par l’élève s’il recopie l’énoncé
        me.stockage[32] = j3pMonome(1, 0, me.stockage[2]) + j3pMonome(2, 1, me.stockage[1]) // autre réponse possible si l’élève recopie l’énoncé
        if (Math.abs(me.stockage[1]) === 1) { // a = +/-1
          signe = remplaceSigne1(me.stockage[1])
          me.stockage[0] = (signe) + 'x' + j3pMonome(2, 0, me.stockage[2])
          me.stockage[99] = (me.stockage[5]) + 'x' // C’est une erreur que l’élève peut faire ; variable sert dans les indications après 1er esssai
          consigne = '$' + signe + 'x ' + me.stockage[20] + ' £b =$ &1&'
        } else { // a <> +/-1   c’est b = +/-1
          me.stockage[0] = (me.stockage[1]) + 'x' + j3pMonome(2, 0, me.stockage[2])
          me.stockage[99] = (me.stockage[5]) + 'x' // C’est une erreur que l’élève peut faire ; variable sert dans les indications après 1er esssai
          consigne = '$£a x ' + me.stockage[20] + ' £b =$ &1&'
        }
        break
    }
    const elt = j3pAffiche(stor.zoneEnonce2, '', consigne,
      {
        a: me.stockage[1],
        b: Math.abs(me.stockage[2]),
        inputmq1: { texte: '', correction: me.stockage[0] }
      }
    )
    stor.zoneInput = elt.inputmqList[0]
  }

  function afficheEnonceProduitlitteraux () {
    me.logIfDebug('afficheEnonceProduitlitteraux avec stockage[11]=', me.stockage[11])
    let consigne
    const po = parentheseOuvre(me.stockage[2])
    const pf = parentheseFerme(me.stockage[2])
    const poa = parentheseOuvre(me.stockage[1])
    const pfa = parentheseFerme(me.stockage[1])
    switch (me.stockage[11]) {
      case 1: // ax * bx
        me.stockage[30] = 'pascopie'
        me.stockage[31] = (me.stockage[1]) + 'x*' + po + (me.stockage[2]) + 'x' + pf // réponse proposée par l’élève s’il recopie l’énoncé
        me.stockage[32] = (me.stockage[2]) + 'x*' + poa + (me.stockage[1]) + 'x' + pfa // autre réponse possible si l’élève recopie l’énoncé
        me.stockage[0] = me.stockage[6] + 'x^2'
        if (me.stockage[6] === 0) me.stockage[0] = 0 // pour qu'à la correction s’affiche 0 au lieu de 0x²
        if (me.stockage[6] === 1) me.stockage[0] = 'x^2' // pour qu'à la correction s’affiche x² au lieu de 1x²
        if (me.stockage[6] === -1) me.stockage[0] = '-x^2' // pour qu'à la correction s’affiche -x² au lieu de -1x²
        me.stockage[99] = (me.stockage[6]) + 'x' // C’est une erreur que l’élève peut faire ; variable sert dans les indications après 1er esssai
        consigne = '$£a x ×' + po + '£b x' + pf + ' = $&1&'
        break
      case 2: // a * bx
        me.stockage[30] = 'pascopie'
        me.stockage[0] = me.stockage[6] + 'x'
        me.stockage[31] = (me.stockage[1]) + '*' + po + (me.stockage[2]) + 'x' + pf // réponse proposée par l’élève s’il recopie l’énoncé
        me.stockage[32] = (me.stockage[2]) + 'x*' + poa + (me.stockage[1]) + pfa // autre réponse possible si l’élève recopie l’énoncé
        if (me.stockage[6] === 0) me.stockage[0] = 0 // pour qu'à la correction s’affiche 0 au lieu de 0x
        if (me.stockage[6] === 1) me.stockage[0] = 'x' // pour qu'à la correction s’affiche x² au lieu de 1x
        if (me.stockage[6] === -1) me.stockage[0] = '-x' // pour qu'à la correction s’affiche -x² au lieu de -1x
        consigne = '$£a \\times ' + po + '£b x' + pf + ' = $&1&'
        break
      case 3: // ax * b
        me.stockage[30] = 'pascopie'
        me.stockage[0] = me.stockage[6] + 'x'
        me.stockage[31] = (me.stockage[1]) + 'x*' + po + (me.stockage[2]) + pf // réponse proposée par l’élève s’il recopie l’énoncé
        me.stockage[32] = (me.stockage[2]) + 'x*' + poa + (me.stockage[1]) + 'x' + pfa // autre réponse possible si l’élève recopie l’énoncé
        if (me.stockage[6] === 0) me.stockage[0] = 0 // pour qu'à la correction s’affiche 0 au lieu de 0x
        if (me.stockage[6] === 1) me.stockage[0] = 'x' // pour qu'à la correction s’affiche x² au lieu de 1x
        if (me.stockage[6] === -1) me.stockage[0] = '-x' // pour qu'à la correction s’affiche -x² au lieu d
        consigne = '$£a x \\times ' + po + '£b' + pf + ' = $&1&'
        break
    }
    const elt = j3pAffiche(stor.zoneEnonce2, '', consigne,
      {
        a: me.stockage[1],
        b: me.stockage[2],
        inputmq1: { texte: '', correction: me.stockage[0] }
      }
    )
    stor.zoneInput = elt.inputmqList[0]
  }

  function afficheEnonceProduitlitterauxCoeff1 () {
    let consigne
    const po = parentheseOuvre(me.stockage[2])
    const pf = parentheseFerme(me.stockage[2])
    const poa = parentheseOuvre(me.stockage[1])
    const pfa = parentheseFerme(me.stockage[1])
    let signeB
    let signeA
    switch (me.stockage[11]) {
      case 1: //  ax*bx
        me.stockage[30] = 'pascopie'
        me.stockage[0] = me.stockage[6] + 'x^2'
        me.stockage[31] = (me.stockage[1]) + 'x*' + po + (me.stockage[2]) + 'x' + pf // réponse proposée par l’élève s’il recopie l’énoncé
        me.stockage[32] = (me.stockage[2]) + 'x*' + poa + (me.stockage[1]) + 'x' + pfa // autre réponse possible si l’élève recopie l’énoncé
        if (me.stockage[6] === 0) me.stockage[0] = 0 // pour qu'à la correction s’affiche 0 au lieu de 0x²
        if (me.stockage[6] === 1) me.stockage[0] = 'x^2' // pour qu'à la correction s’affiche x² au lieu de 1x²
        if (me.stockage[6] === -1) me.stockage[0] = '-x^2' // pour qu'à la correction s’affiche -x² au lieu de -1x²
        signeA = remplaceSigne1(me.stockage[1])
        signeB = remplaceSigne1(me.stockage[2])
        if ((Math.abs(me.stockage[1]) === 1) && (Math.abs(me.stockage[2]) === 1)) { //  a = +/-1   et     b  = +/-1
          const signeR = remplaceSigne1(me.stockage[6])
          me.stockage[99] = signeR + 'x'
          consigne = '$' + signeA + 'x \\times ' + po + signeB + 'x' + pf + ' = $&1&'
        } else { //  a <>1  ou b<>1
          me.stockage[99] = me.stockage[6] + 'x' // C’est une erreur que l’élève peut faire ; variable sert dans les indications après 1er esssai
          if (Math.abs(me.stockage[1]) === 1) { //  c’est a qui vaut +/-1
            consigne = '$' + signeA + ' x \\times ' + po + '£b x' + pf + ' = $&1&'
          } else { // c’est b = +/-1
            consigne = '$ £a x \\times ' + po + signeB + 'x' + pf + ' = $&1&'
          }
        }
        break
      case 2: //  a*bx
        me.stockage[30] = 'pascopie'
        me.stockage[0] = me.stockage[6] + 'x'
        me.stockage[31] = (me.stockage[1]) + '*' + po + (me.stockage[2]) + 'x' + pf // réponse proposée par l’élève s’il recopie l’énoncé
        me.stockage[32] = (me.stockage[2]) + 'x*' + poa + (me.stockage[1]) + pfa // autre réponse possible si l’élève recopie l’énoncé
        if (me.stockage[6] === 0) me.stockage[0] = 0 // pour qu'à la correction s’affiche 0 au lieu de 0x
        if (me.stockage[6] === 1) me.stockage[0] = 'x' // pour qu'à la correction s’affiche x² au lieu de 1x
        if (me.stockage[6] === -1) me.stockage[0] = '-x' // pour qu'à la correction s’affiche -x² au lieu de -1x
        signeB = remplaceSigne1(me.stockage[2])
        if (me.stockage[2] === 1) { // b = +/-1
          consigne = '$£a \\times ' + po + signeB + 'x' + pf + ' = $&1&'
        } else { // b <> 1    c’est a=+/-1
          consigne = '$£a \\times ' + po + '£b x' + pf + ' = $&1&'
        }
        break
      case 3: //  ax*b
        me.stockage[30] = 'pascopie'
        me.stockage[0] = (me.stockage[6]) + 'x'
        me.stockage[31] = (me.stockage[1]) + 'x*' + po + (me.stockage[2]) + pf // réponse proposée par l’élève s’il recopie l’énoncé
        me.stockage[32] = (me.stockage[2]) + '*' + poa + (me.stockage[1]) + 'x' + pfa // autre réponse possible si l’élève recopie l’énoncé
        if (me.stockage[6] === 0) me.stockage[0] = 0 // pour qu'à la correction s’affiche 0 au lieu de 0x
        if (me.stockage[6] === 1) me.stockage[0] = 'x' // pour qu'à la correction s’affiche x² au lieu de 1x
        if (me.stockage[6] === -1) me.stockage[0] = '-x' // pour qu'à la correction s’affiche -x² au lieu de -1x
        if (Math.abs(me.stockage[1]) === 1) { //   a = +/-1
          const signe = remplaceSigne1(me.stockage[1])
          consigne = '$' + signe + ' x \\times ' + po + '£b' + pf + ' = $&1&'
        } else { //  a <> +/-1  c’est b = =+/-1
          consigne = '$£a x \\times ' + po + '£b' + pf + ' = $&1&'
        }
        break
    }
    const elt = j3pAffiche(stor.zoneEnonce2, '', consigne,
      {
        a: me.stockage[1],
        b: me.stockage[2],
        inputmq1: { texte: '', correction: me.stockage[0] }
      }
    )
    stor.zoneInput = elt.inputmqList[0]
  }

  function afficheIndication (repeleve) {
    j3pEmpty(stor.zoneIndic)
    switch (me.stockage[10]) {
      case 'sommelitteraux' :
        if (repeleve === me.stockage[99]) {
          const indicTexte = (me.stockage[11] === 1)
            ? textes.phrase20 + '$x !$'
            : textes.phrase21
          stor.zoneTexte = j3pMathsAjouteDans(stor.zoneIndic, { content: indicTexte })
        }
        break
      case 'produitlitteraux' :
        if ((me.stockage[11] === 1) && (repeleve === me.stockage[99])) {
          // j3pElement("indication").innerHTML=cFaux;
          stor.zoneTexte = j3pMathsAjouteDans(stor.zoneIndic, { content: textes.phrase22 + '$x !$' })
        }
        break
    }
  }

  function afficheCorrection () {
    stor.zoneExpli1 = j3pAddElt(stor.zoneExpli, 'div')
    j3pDetruit(stor.divPalette)
    switch (me.stockage[10]) {
      case 'sommelitteraux':
        switch (me.stockage[11]) {
          case 1: // ax+bx
            j3pMathsAjouteDans(stor.zoneExpli1, {
              content: textes.phrase10 + j3pEcritBienAxPlusB(me.stockage[5], 0) + '.',
              parametres: { a: me.stockage[5] }
            })
            break
          case 2: // a+bx
            j3pMathsAjouteDans(stor.zoneExpli1, {
              content: textes.phrase10 + j3pEcritBienAxPlusB(me.stockage[2], me.stockage[1]) + '.',
              parametres: {
                a: me.stockage[2],
                b: me.stockage[1]
              }
            })
            break
          case 3: // ax+b
            j3pMathsAjouteDans(stor.zoneExpli1, {
              content: textes.phrase10 + j3pEcritBienAxPlusB(me.stockage[1], me.stockage[2]) + '.',
              parametres: {
                a: me.stockage[1],
                b: me.stockage[2]
              }
            })
            break
        }

        break

      case 'produitlitteraux':

        if (me.stockage[11] === 1) { // ax*bx
          j3pMathsAjouteDans(stor.zoneExpli1, {
            content: textes.phrase10 + j3pEcritBienAxCarre(me.stockage[6]) + '.',
            parametres: { a: me.stockage[6] }
          }) /// /    me.stockage[6] peut valoir +/-1 et aloir il s’affiche 1 x !!!
        } else { // a*bx ou ax*b
          j3pMathsAjouteDans(stor.zoneExpli1, {
            content: textes.phrase10 + j3pEcritBienAxPlusB(me.stockage[6], 0) + '.',
            parametres: { a: me.stockage[6] }
          })
        }
        break
    }
  }

  function afficheExplications () {
    switch (me.stockage[10]) {
      case 'sommelitteraux' :
        switch (me.stockage[11]) {
          case 1: // ax+bx
            if (me.stockage[5] === 0) { // on a 0x !
              j3pAffiche(stor.zoneExpli, '', textes.phrase11 + '$£a x ' + me.stockage[20] + ' £b x = (£a' + me.stockage[20] + '£b) x = £s x = 0 $',
                {
                  a: me.stockage[1],
                  b: Math.abs(me.stockage[2]),
                  s: me.stockage[5]
                }
              )
            } else {
              if (Math.abs(me.stockage[5]) === 1) { // on a +/-1x !
                const signeR = remplaceSigne1(me.stockage[5])
                j3pAffiche(stor.zoneExpli, '', textes.phrase11 + '$£a x ' + me.stockage[20] + ' £b x = (£a ' + me.stockage[20] + ' £b) x = £s x$',
                  {
                    a: me.stockage[1],
                    b: Math.abs(me.stockage[2]),
                    s: signeR
                  }
                )
              } else {
                j3pAffiche(stor.zoneExpli, '', textes.phrase11 + '$£a x ' + me.stockage[20] + ' £b x = (£a ' + me.stockage[20] + ' £b) x = £s x$',
                  {
                    a: me.stockage[1],
                    b: Math.abs(me.stockage[2]),
                    s: me.stockage[5]
                  }
                )
              }
            }
            break
          case 2: // a+bx
            j3pAffiche(stor.zoneExpli, '', textes.phrase12 + '$£b x ' + me.stockage[21] + ' £a$',
              {
                b: me.stockage[2],
                a: Math.abs(me.stockage[1])
              }
            )
            break
          case 3: // ax+b
            j3pAffiche(stor.zoneExpli, '', textes.phrase12 + '$£a x ' + me.stockage[20] + ' £b$',
              {
                a: me.stockage[1],
                b: Math.abs(me.stockage[2])
              }
            )
            break
        }
        break

      case 'produitlitteraux': {
        const po = parentheseOuvre(me.stockage[2])
        const pf = parentheseFerme(me.stockage[2])
        const explication = (me.stockage[11] === 1)
          ? '$£a x \\times ' + po + '£b x' + pf + ' = £a \\times ' + po + '£b' + pf + ' \\times  x \\times  x = £s x^2$' // ax*bx
          : (me.stockage[11] === 2)
              ? '$£a \\times ' + po + '£b x' + pf + ' = £a \\times ' + po + '£b' + pf + ' \\times x = £s x$' // a*bx
              : '$£a x \\times ' + po + '£b' + pf + ' = £a \\times  ' + po + '£b' + pf + ' \\times  x = £s x$' // ax*b
        j3pAffiche(stor.zoneExpli, '', textes.phrase11 + explication,
          {
            a: me.stockage[1],
            b: me.stockage[2],
            s: me.stockage[6]
          }
        )
        break
      }
    }
  }

  function afficheExplicationsCoeff1 () {
    let signe
    switch (me.stockage[10]) {
      case 'sommelitteraux' :
        switch (me.stockage[11]) {
          case 1: // ax+bx    avec a=+/-1 ou/et b=+/-1
            signe = remplaceSigne1(me.stockage[1])
            if ((Math.abs(me.stockage[1]) === 1) && (Math.abs(me.stockage[2]) === 1)) { // a=+/-1  et b = +/-1 (seul cas où on peut avoir "0x"!)
              if (me.stockage[5] === 0) {
                j3pAffiche(stor.zoneExpli, '', textes.phrase11 + ' $' + signe + 'x' + me.stockage[20] + 'x = (£a ' + me.stockage[20] + ' £b) x = £s x= 0$',
                  {
                    a: me.stockage[1],
                    b: Math.abs(me.stockage[2]),
                    s: me.stockage[5]
                  }
                )
              } else {
                j3pAffiche(stor.zoneExpli, '', textes.phrase11 + ' $' + signe + 'x' + me.stockage[20] + 'x = (£a ' + me.stockage[20] + ' £b) x = £s x$',
                  {
                    a: me.stockage[1],
                    b: Math.abs(me.stockage[2]),
                    s: me.stockage[5]
                  }
                )
              }
            } else { // a <> +/-1  ou b <> +/-1
              if (Math.abs(me.stockage[1]) === 1) { // c’est a = +/-1  et b <> +/-1
                j3pAffiche(stor.zoneExpli, '', textes.phrase11 + signe + '$x ' + me.stockage[20] + ' £b x = (£a ' + me.stockage[20] + ' £b) x = £s x$',
                  {
                    a: me.stockage[1],
                    b: Math.abs(me.stockage[2]),
                    s: me.stockage[5]
                  }
                )
              } else { // a <> +/-1  c’est b = +/-1
                j3pAffiche(stor.zoneExpli, '', textes.phrase11 + '$£a x ' + me.stockage[20] + ' x = (£a ' + me.stockage[20] + ' £b) x = £s x$',
                  {
                    a: me.stockage[1],
                    b: Math.abs(me.stockage[2]),
                    s: me.stockage[5]
                  }
                )
              }
            }
            break
          case 2: // a+bx    avec a=+/-1 ou/et b=+/-1
            signe = remplaceSigne1(me.stockage[2])
            if (Math.abs(me.stockage[2]) === 1) { //   b=+/-1  la valeur de a n’est pas importante
              j3pAffiche(stor.zoneExpli, '', textes.phrase12 + '$ ' + signe + 'x ' + me.stockage[21] + '£a$',
                {
                  a: Math.abs(me.stockage[1])
                }
              )
            } else { //   b <> +/-1   on a a = +/-1   pas important !
              j3pAffiche(stor.zoneExpli, '', textes.phrase12 + '$£b x ' + me.stockage[21] + ' £a$',
                {
                  b: me.stockage[2],
                  a: Math.abs(me.stockage[1])
                }
              )
            }
            break
          case 3: // ax+b    avec a=+/-1 ou/et b=+/-1
            if (Math.abs(me.stockage[1]) === 1) { //  c’est a = +/-1  la valeur de b n’est pas importante
              signe = remplaceSigne1(me.stockage[1])
              j3pAffiche(stor.zoneExpli, '', textes.phrase12 + '$ ' + signe + 'x' + me.stockage[20] + ' £b$',
                {
                  b: Math.abs(me.stockage[2])
                }
              )
            } else { //  a <> +/-1  c’est b=+/-1 pas important !
              j3pAffiche(stor.zoneExpli, '', textes.phrase12 + '$£a x ' + me.stockage[20] + ' £b$',
                {
                  a: me.stockage[1],
                  b: Math.abs(me.stockage[2])
                }
              )
              break
            }
            break
        }
        break

      case 'produitlitteraux' : {
        const po = parentheseOuvre(me.stockage[2])
        const pf = parentheseFerme(me.stockage[2])
        const signeA = remplaceSigne1(me.stockage[1])
        const signeB = remplaceSigne1(me.stockage[2])
        const signeR = ((Math.abs(me.stockage[1]) === 1) && (Math.abs(me.stockage[2]) === 1))
          ? remplaceSigne1(me.stockage[6])
          : me.stockage[6]
        const explication = (me.stockage[11] === 1)// ax*bx              avec a = +/-1 ou b = +/-1
          ? ((Math.abs(me.stockage[1]) === 1) && (Math.abs(me.stockage[2]) === 1))
              ? '$ ' + signeA + 'x \\times ' + po + signeB + 'x' + pf + ' = £a \\times ' + po + '£b' + pf + ' \\times x \\times x = £s x^2$' //  a=+/-1  et  b =+/-1
              : (Math.abs(me.stockage[1]) === 1)
                  ? '$ ' + signeA + 'x \\times ' + po + '£b x' + pf + ' = £a \\times ' + po + '£b' + pf + ' \\times x \\times x = £s x^2$'//  c’est a=+/-1 et b<>+-1
                  : '$£a x \\times ' + po + signeB + 'x' + pf + ' = £a \\times ' + po + '£b' + pf + ' \\times x \\times x = £s x^2$' // c’est b=+/-1 et a<>+-1
          : (me.stockage[11] === 2) // a*bx               avec a = +/-1 ou b = +/-1
              ? ((Math.abs(me.stockage[1]) === 1) && (Math.abs(me.stockage[2]) === 1))
                  ? '$£a \\times ' + po + signeB + 'x' + pf + ' = £a \\times ' + po + '£b' + pf + ' \\times x = £s x$' //  a=+/-1  et  b =+/-1
                  : (Math.abs(me.stockage[2]) === 1)
                      ? '$£a \\times ' + po + signeB + 'x' + pf + ' = £a \\times ' + po + '£b' + pf + ' \\times x = £s x$' // b=+/-1    valeur de a sans importance
                      : '$£a \\times' + po + '£b x' + pf + ' = £a \\times ' + po + '£b' + pf + ' \\times x = £s x$' // b<>+/-1   c’est a = +/-1
              : ((Math.abs(me.stockage[1]) === 1) && (Math.abs(me.stockage[2]) === 1))
                  ? '$ ' + signeA + 'x \\times ' + po + '£b' + pf + ' = £a \\times ' + po + '£b' + pf + ' \\times x = £s x$' //  a=+/-1  et  b =+/-1
                  : (Math.abs(me.stockage[1]) === 1)
                      ? '$ ' + signeA + 'x \\times ' + po + '£b' + pf + ' = £a \\times ' + po + '£b' + pf + ' \\times  x = £s x$' // a=+/-1   valeur de b sans importance
                      : '$£a x \\times ' + po + '£b' + pf + ' = £a \\times ' + po + '£b' + pf + ' \\times x = £s x$' // a<>+/-1   valeur de b sans importance

        // console.log("explication,",explication)
        j3pAffiche(stor.zoneExpli, '', textes.phrase11 + explication,
          {
            a: me.stockage[1],
            b: me.stockage[2],
            s: signeR
          }
        )
        break
      }
    }
  }

  switch (me.etat) {
    case 'enonce': {
      // code exécuté au lancement de la section
      if (me.debutDeLaSection) {
        me.surcharge()
        me.construitStructurePage('presentation1')

        // par convention, me.stockage[0] contient la solution
        // les autres peuvent contenir ce que vous voulez, en général des valeurs que l’on doit mémoriser entre les différents appels de la section
        me.stockage = [0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0]

        /*
         Par convention,`
        `   me.typederreurs[0] = nombre de bonnes réponses
            me.typederreurs[1] = nombre de fois où l’élève a utilisé une chance supplémentaire
            me.typederreurs[2] = nombre de mauvaises réponses
            me.typederreurs[10] = nombre de fois où l’élève a eu faux à cause d’un temps limite
            LES AUTRES peuvent être utilisées par la détermination de la pe dans le cas d’une section qualitative
         */
        me.typederreurs = [0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0]
        me.afficheTitre(textes.titre_exo)
        if (ds.indication) me.indication(me.zonesElts.IG, ds.indication)
        // chargement mtg
        getMtgCore({ withMathJax: true })
          .then(
            // success
            (mtgAppLecteur) => {
              stor.cListeObjets = mtgAppLecteur.createList(figure)
              enonceMain()
            },
            // failure
            (error) => {
              j3pShowError(error, { message: 'mathgraph n’est pas correctement chargé', mustNotify: true })
            })
          // plantage dans le code de success
          .catch(j3pShowError)
      } else {
        // A chaque répétition, on nettoie la scène
        me.videLesZones()
        enonceMain()
      }
      break
    } // case enonce

    case 'correction': {
      let reduire
      let expSolution
      const fctsValid = stor.fctsValid
      // On teste si une réponse a été saisie
      const repeleve = $(stor.zoneInput).mathquill('latex')
      const expEleve = traiteMathQuill(repeleve)
      const expEleveSansMultImp = traiteMathQuillSansMultImplicites(repeleve)
      if ((me.stockage[5] === 0) && (me.stockage[10] === 'sommelitteraux') && (me.stockage[11] === 1)) { // on a ax +bx  et (a+b = 0)
        expSolution = 0
      } else {
        expSolution = traiteMathQuill(me.stockage[0])
      }
      stor.cListeObjets.giveFormula2('f', expSolution)
      stor.cListeObjets.giveFormula2('rep', expEleve)
      stor.cListeObjets.calculateNG(false)
      // let note = (j3pEgaliteFormelle(expEleve,expSolution));
      let note = stor.cListeObjets.valueOf('equivalent') === 1
      // On regarde si, en remplaçant les multiplications 1* ou *1 par "" ça devient équivalent.
      // Si oui on indiquera que c’est bon mais pas assez simplifié
      if (!note) {
        const exp = expEleve.replace('1*', '').replace('*1', '')
        stor.cListeObjets.giveFormula2('rep', exp)
        stor.cListeObjets.calculateNG(false)
        note = stor.cListeObjets.valueOf('equivalent') === 1
      }

      if ((me.stockage[5] === 0) && (me.stockage[10] === 'sommelitteraux') && (me.stockage[11] === 1)) { // on a ax +bx  et (a+b = 0)
        reduire = (repeleve === '0')
      } else {
        if (expEleveSansMultImp.length <= me.stockage[0].length) {
          //  On compare la longueur de la réponse de l’élève avec la solution attendue  ; elle est égale
          reduire = true
        } else { // la réponse de l’élève n’a pas la même longeur que la solution attendue
          reduire = false
          // Il y a quand même un cas à la con : a-b où l’élève peut répondre -b+a
          if (me.stockage[0][0] !== '-' && expEleveSansMultImp[0] === '-') {
            reduire = (expEleveSansMultImp.length <= me.stockage[0].length + 1)
          }
        }
      }

      stor.zoneIndic.innerHTML = '<br/>'
      if ((repeleve === '') && (!me.isElapsed)) {
        me.reponseManquante(stor.zoneCorr)
        j3pFocus(stor.zoneInput)
        return me.finCorrection() // on reste dans l’état correction
      }

      // Une réponse a été saisie
      // Bonne réponse
      if (note) {
        if (reduire) { // élève n’a pas recopié énoncé ou y était autorisé
          me.score++
          stor.zoneCorr.style.color = me.styles.cbien
          stor.zoneCorr.innerHTML = cBien
          fctsValid.zones.bonneReponse[0] = true
          fctsValid.coloreUneZone(stor.zoneInput)
          j3pDetruit(stor.divPalette)
          me.typederreurs[0]++
          return me.finCorrection('navigation', true)
        }

        // élève a recopié énoncé et n’y était pas autorisé  ou a bricolé
        if (repeleve === me.stockage[31] || repeleve === me.stockage[32]) { // élève a recopié énoncé et n’y était pas autorisé
          stor.zoneCorr.style.color = me.styles.cfaux
          stor.zoneCorr.innerHTML = textes.phrase9
          j3pFocus(stor.zoneInput)
        } else { /// eleve a "bricolé"
          stor.zoneCorr.style.color = me.styles.cfaux
          stor.zoneCorr.innerHTML = textes.phrase8
          j3pFocus(stor.zoneInput)
        }
        return me.finCorrection()
      }

      // Pas de bonne réponse
      stor.zoneCorr.style.color = me.styles.cfaux

      // A cause de la limite de temps :
      if (me.isElapsed) { // limite de temps
        stor.zoneCorr.innerHTML = tempsDepasse
        me.typederreurs[10]++
        me.cacheBoutonValider()
        stor.zoneExpli.style.color = me.styles.toutpetit.correction.color
        afficheCorrection()
        if ((Math.abs(me.stockage[1]) === 1) || (Math.abs(me.stockage[2]) === 1)) {
          afficheExplicationsCoeff1()
        } else {
          afficheExplications()
        }
        return me.finCorrection('navigation', true)
      }

      // Réponse fausse :
      stor.zoneCorr.innerHTML = cFaux

      fctsValid.zones.bonneReponse[0] = false
      fctsValid.coloreUneZone(stor.zoneInput)

      if (me.essaiCourant < ds.nbchances) {
        afficheIndication(repeleve)
        stor.zoneCorr.style.color = me.styles.cfaux
        stor.zoneCorr.innerHTML += '<br>' + essaieEncore
        j3pFocus(stor.zoneInput)
        me.typederreurs[1]++
        // indication éventuelle ici
        return me.finCorrection()
      }

      // Erreur au dernier essai
      stor.zoneCorr.innerHTML += '<br>'
      stor.zoneExpli.style.color = me.styles.toutpetit.correction.color
      afficheCorrection()
      // EXPLICATIONS
      if ((Math.abs(me.stockage[1]) === 1) || (Math.abs(me.stockage[2]) === 1)) {
        afficheExplicationsCoeff1()
      } else {
        afficheExplications()
      }
      me.typederreurs[2]++
      return me.finCorrection('navigation', true)
    } // case "correction":

    case 'navigation':
      if (!this.sectionTerminee()) this.etat = 'enonce'
      this.finNavigation()
      break
  }
}
