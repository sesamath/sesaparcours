import { j3pAddClass, j3pAddElt, j3pDetruit, j3pEmpty, j3pGetNewId, j3pGetRandomBool, j3pGetRandomElt, j3pGetRandomInt, j3pModale, j3pShowError, j3pShuffle } from 'src/legacy/core/functions'
import { j3pCreeSVG } from 'src/legacy/core/functionsSvg'
import Zoneclick from 'src/legacy/outils/zoneclick/Zoneclick'
import { getMtgCore } from 'src/lib/outils/mathgraph'
import { addDefaultTable } from 'src/legacy/themes/table'
import { j3pAffiche } from 'src/lib/mathquill/functions'
import { addMaj } from 'src/lib/utils/string'
import textesGeneriques from 'src/lib/core/textes'

const { cBien, cFaux, reponseIncomplete, tempsDepasse } = textesGeneriques

/**
 * Ajouter une description ici
 * @fileOverview
 * @author Tommy Barroy <t.barroy@laposte.net>
 * @since 2017-10
*/
/* global THREE */
export const params = {
  outils: ['mathquill', 'three'],
  /**
   * Ce qui est paramétrable dans l’éditeur de graphe
   * @type {LegacySectionParamsParametres}
   */
  parametres: [
    ['nbrepetitions', 3, 'entier', 'Nombre de répétitions de la section'],
    ['nbchances', 2, 'entier', 'Nombre d’essais possibles.'],
    ['Cube', false, 'boolean', '<u>true</u>: Cube proposé.'],
    ['Pave_Droit', false, 'boolean', '<u>true</u>: Pavé droit proposé.'],
    ['Prisme_Droit', false, 'boolean', '<u>true</u>: Primse droit proposé.'],
    ['Cylindre_revo', false, 'boolean', '<u>true</u>: Cylindre de révolution proposé.'],
    ['Pyramide', false, 'boolean', '<u>true</u>: Pyramide proposée.'],
    ['Cone_revo', false, 'boolean', '<u>true</u>: Cône de révolution proposé.'],
    ['Choix', true, 'boolean', '<u>true</u>: Il faut choisir un patron correspondant au solide proposé.'],
    ['Faces', true, 'boolean', '<u>true</u>: Il faut colorer les faces du patron corespondantes à celles de la perpsective cavailère'],
    ['Aretes', true, 'boolean', '<u>true</u>: Il faut colorer les segments du patron corespondants aux arêtes de la perpsective cavailère'],
    // ['Longueurs', false, 'boolean', '<u>true</u>: Il faut reporter des longueurs sur le patron.'],
    // le modèle gère la compatibilité ascendante avec l’ancien paramètre MiseEnPagePlusClaire
    ['theme', 'standard', 'liste', 'Modèle pour l’affichage', ['standard', 'zonesAvecImageDeFond']]

  ]
}

/**
 * section patron01
 * @this {Parcours}
 */
export default function main () {
  const me = this
  const ds = me.donneesSection
  const stor = me.storage
  let svgPat = stor.svgPat

  function PrismGeometry (vertices, height) {
    const Shape = new THREE.Shape();

    (function f (ctx) {
      ctx.moveTo(vertices[0].x, vertices[0].y)
      for (let i = 1; i < vertices.length; i++) {
        ctx.lineTo(vertices[i].x, vertices[i].y)
      }
      ctx.lineTo(vertices[0].x, vertices[0].y)
    })(Shape)

    const settings = { }
    settings.depth = height
    settings.bevelEnabled = false
    THREE.ExtrudeGeometry.call(this, Shape, settings)
  }

  function torGeo (r1, r2, height) {
    const tab = []
    for (let i = 0; i < 360; i++) {
      tab.push({ x: r1 * Math.cos(i * Math.PI / 180), y: r1 * Math.sin(i * Math.PI / 180) })
    }
    for (let i = 360; i > 0; i--) {
      tab.push({ x: r2 * Math.cos(i * Math.PI / 180), y: r2 * Math.sin(i * Math.PI / 180) })
    }
    return new PrismGeometry(tab, height)
  }

  function initSection () {
    stor.epAr = 5
    stor.widthetiq = 200
    stor.heightetiq = 200
    stor.tempo = 60
    stor.tempodeb = 1000
    stor.depCentreCo = 2

    me.validOnEnter = false // ex donneesSection.touche_entree

    // Construction de la page
    me.construitStructurePage('presentation3')
    // les étapes
    stor.etapes = []
    if (ds.Choix) stor.etapes.push('c')
    if (ds.Faces) stor.etapes.push('f')
    if (ds.Aretes) stor.etapes.push('a')
    if (ds.Longueurs) stor.etapes.push('l')
    if (stor.etapes.length === 0) {
      j3pShowError('<u>Paramètres invalides:</u>: Aucune question choisie.')
      stor.etapes = ['c']
    }
    // si nbetapes n’est pas dans les params exportés (à priori toujours le cas), il faut appeler surcharge pour imposer nbetapes ≠ 1
    const nbetapes = stor.etapes.length
    if (nbetapes > 1) me.surcharge({ nbetapes })

    const solides = []
    if (ds.Cube) solides.push('un cube')
    if (ds.Pave_Droit) solides.push('un pavé droit')
    if (ds.Prisme_Droit) solides.push('un prisme droit')
    if (ds.Cylindre_revo) solides.push('un cylindre de révolution')
    if (ds.Pyramide) solides.push('une pyramide')
    if (ds.Cone_revo) solides.push('un cône de révolution')
    if (solides.length === 0) {
      j3pShowError('<u>Paramètres invalides:</u>: Aucun solide choisi.')
      solides.push('un cube')
    }
    // tirage aléatoire pour chaque item
    stor.exos = []
    while (stor.exos.length < ds.nbitems) {
      stor.exos.push({
        solide: j3pGetRandomElt(solides),
        col: j3pGetRandomBool()
      })
    }

    me.typederreurs = [0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0]

    // détermination du titre
    let titre
    if (stor.etapes.length === 1) {
      switch (stor.etapes[0]) {
        case 'c': titre = "Reconnaitre le patron d'"
          break
        case 'f': titre = "Patron: Retrouver les faces d'"
          break
        case 'a': titre = "Patron: Retouver les arêtes d'"
          break
        case 'l': titre = "Patron: Retrouver les longueurs d'"
          break
        default:
          me.notif(Error(`étape de type ${stor.etapes[0]} non géré`))
      }
    } else {
      titre = "Patron d'"
    }
    if (solides.length === 1) {
      titre += solides[0]
    } else {
      titre += 'un solide'
    }
    me.afficheTitre(titre)

    stor.restric = '0123456789,.'

    getMtgCore({ withMathJax: true })
      .then(
        // success
        (mtgAppLecteur) => {
          stor.mtgAppLecteur = mtgAppLecteur
          enonceMain()
        },
        // failure
        (error) => {
          j3pShowError(error, { message: 'mathgraph n’est pas correctement chargé', mustNotify: true })
        })
      // plantage dans enonceMain
      .catch(j3pShowError)
  }

  function faisChoixExos () {
    stor.evir = []
    const exo = stor.exos.pop()
    const nomspris = []
    stor.A = addMaj(nomspris)
    stor.B = addMaj(nomspris)
    stor.C = addMaj(nomspris)
    stor.rotation = j3pGetRandomInt(1, 359)
    stor.basequa = j3pGetRandomBool()
    stor.encours = stor.etapes[0]
    stor.placeok = true
    stor.zommfait = false
    stor.pose = false
    return exo
  }

  function poseQuestion () {
    PrismGeometry.prototype = Object.create(THREE.ExtrudeGeometry.prototype)
    for (let i = 0; i < stor.evir.length; i++) {
      j3pDetruit(stor.evir[i])
    }
    stor.evir = []
    let buf
    stor.yaya = stor.encours
    j3pEmpty(stor.lesdiv.consigne)
    j3pEmpty(stor.lesdiv.consigneF)
    j3pEmpty(stor.lesdiv.travail)
    j3pEmpty(stor.lesdiv.zonedrep)
    j3pEmpty(stor.lesdiv.correction)
    j3pEmpty(stor.lesdiv.solution)
    j3pEmpty(stor.lesdiv.explications)
    buf = 'On considère ' + stor.exo.solide.replace('une', 'la').replace('un', 'le') + ' ci-dessous (à gauche)'
    if (stor.exo.solide === 'une pyramide' && stor.encours === 'a') buf += '<br><i>Aucune face de cette pyramide n’est un triangle isocèle.</i>'
    j3pAffiche(stor.lesdiv.consigne, null, buf)
    stor.ligne = 'un segment'
    stor.lignesS = 'segments'
    stor.toulignes = 'tous les segments'
    stor.lignes = 'plusieurs segments'
    stor.leligne = 'le segment'
    stor.lammaret = 'arête'
    stor.larettt = 'l’arête'
    stor.lale = 'le'
    stor.ligneE = ''
    stor.decx = 110
    stor.decy = 120
    stor.ratZoom = 2
    switch (stor.exo.solide) {
      case 'un cube':
        stor.ppos = 'cu1'
        stor.listep = [16, 17, 44]
        stor.listeFacePat = []
        // gauche
        stor.listeFacePat.push({ a: true, r: [0, 0, 0], pos: [0, 0, -25], l: 45, l2: 45 })
        // dessous
        stor.listeFacePat.push({ a: true, r: [Math.PI / 2, 0, 0], pos: [0, -25, 0], l: 45, l2: 45 })
        // droite
        stor.listeFacePat.push({ a: true, r: [0, 0, 0], pos: [0, 0, 25], l: 45, l2: 45 })
        // dessus
        stor.listeFacePat.push({ a: true, r: [Math.PI / 2, 0, 0], pos: [0, 25, 0], l: 45, l2: 45 })
        // derriere
        stor.listeFacePat.push({ a: true, r: [0, Math.PI / 2, 0], pos: [25, 0, 0], l: 45, l2: 45 })

        // devant
        stor.listeFacePat.push({ a: true, r: [0, Math.PI / 2, 0], pos: [-25, 0, 0], l: 45, l2: 45 })

        stor.listeLignePat = []
        stor.listeLignePat.push({ e: stor.epAr, a: true, r: [0, Math.PI / 2, 0], pos: [-25, -25, 0], l: 52 })
        stor.listeLignePat.push({ e: stor.epAr, a: true, r: [0, Math.PI / 2, 0], pos: [-25, 25, 0], l: 52 })
        stor.listeLignePat.push({ e: stor.epAr, a: true, r: [0, 0, Math.PI / 2], pos: [-25, 0, -25], l: 52 })
        stor.listeLignePat.push({ e: stor.epAr, a: true, r: [0, 0, 0], pos: [0, -25, -25], l: 52 })
        stor.listeLignePat.push({ e: stor.epAr, a: true, r: [0, 0, Math.PI / 2], pos: [-25, 0, 25], l: 52 })
        stor.listeLignePat.push({ e: stor.epAr, a: true, r: [0, 0, 0], pos: [0, 25, -25], l: 52 })

        stor.listeLignePat.push({ e: stor.epAr, a: true, r: [0, 0, Math.PI / 2], pos: [25, 0, -25], l: 52 })
        stor.listeLignePat.push({ e: stor.epAr, a: true, r: [0, 0, Math.PI / 2], pos: [25, 0, 25], l: 52 })
        stor.listeLignePat.push({ e: stor.epAr, a: true, r: [0, 0, 0], pos: [0, -25, 25], l: 52 })
        stor.listeLignePat.push({ e: stor.epAr, a: true, r: [0, 0, 0], pos: [0, 25, 25], l: 52 })

        stor.listeLignePat.push({ e: stor.epAr, a: true, r: [0, Math.PI / 2, 0], pos: [25, -25, 0], l: 52 })
        stor.listeLignePat.push({ e: stor.epAr, a: true, r: [0, Math.PI / 2, 0], pos: [25, 25, 0], l: 52 })

        stor.listeLignePat.push({ e: stor.epAr, a: true, r: [0, 0, 0], pos: [0, -25, -25], l: 52 })

        stor.numFace = [[0], [1], [2], [3], [4], [5]]
        stor.numFacePat = [{ fixe: ['#s7'], moov: ['#s5'] }, { fixe: ['#s8'], moov: ['#s4'] }, { fixe: ['#s9'], moov: ['#s3'] }, { fixe: ['#s10'], moov: ['#s2'] }, { fixe: ['#s11'], moov: ['#s1'] }, { fixe: ['#s12'], moov: ['#s6'] }]
        stor.lescotes = [
          [
            { n: 0, perspe: [109, 184, 193], fixe: [['#seg28']], moov: [['#seg28']] },
            { n: 1, perspe: [192, 120, 198], fixe: [['#seg0'], ['#seg35']], moov: [['#seg33'], ['#seg30']] },
            { n: 2, perspe: [199, 190, 118], fixe: [['#seg10'], ['#seg36']], moov: [['#seg31'], ['#seg27']] },
            { n: 3, perspe: [111, 188, 200], fixe: [['#seg4']], moov: [['#seg23']] },
            { n: 4, perspe: [114, 174, 196], fixe: [['#seg11'], ['#seg34']], moov: [['#seg32'], ['#seg29']] },
            { n: 5, perspe: [195, 178, 113], fixe: [['#seg3'], ['#seg7']], moov: [['#seg22'], ['#seg26']] },
            { n: 6, perspe: [202, 116, 180], fixe: [['#seg8'], ['#seg12']], moov: [['#seg16'], ['#seg19']] },
            { n: 7, perspe: [115, 176, 197], fixe: [['#seg9'], ['#seg14']], moov: [['#seg17'], ['#seg20']] },
            { n: 8, perspe: [119, 122, 126], fixe: [['#seg5']], moov: [['#seg24']] },
            { n: 9, perspe: [110, 182, 194], fixe: [['#seg6']], moov: [['#seg25']] },
            { n: 10, perspe: [201, 112, 186], fixe: [['#seg2']], moov: [['#seg18']] },
            { n: 11, perspe: [125, 124, 117], fixe: [['#seg1'], ['#seg13']], moov: [['#seg15'], ['#seg21']] }
          ]
        ]
        break // cube

      case 'un pavé droit':
        stor.ppos = 'pa1'

        stor.listeFacePat = []
        // gauche
        stor.listeFacePat.push({ a: true, r: [0, 0, 0], pos: [0, 0, -12], l: 60, l2: 60 })
        // dessous
        stor.listeFacePat.push({ a: true, r: [Math.PI / 2, 0, 0], pos: [0, -30, 0], l: 60, l2: 24 })
        // droite
        stor.listeFacePat.push({ a: true, r: [0, 0, 0], pos: [0, 0, 12], l: 60, l2: 60 })
        // dessus
        stor.listeFacePat.push({ a: true, r: [Math.PI / 2, 0, 0], pos: [0, 30, 0], l: 60, l2: 24 })
        // derriere
        stor.listeFacePat.push({ a: true, r: [0, Math.PI / 2, 0], pos: [30, 0, 0], l: 24, l2: 60 })

        // devant
        stor.listeFacePat.push({ a: true, r: [0, Math.PI / 2, 0], pos: [-30, 0, 0], l: 24, l2: 60 })

        stor.listeLignePat = []

        stor.listeLignePat.push({ e: stor.epAr, a: true, r: [0, Math.PI / 2, 0], pos: [31, -31, 0], l: 30 })
        // haut derriere
        stor.listeLignePat.push({ e: stor.epAr, a: true, r: [0, Math.PI / 2, 0], pos: [31, 31, 0], l: 30 })
        // haut devant
        stor.listeLignePat.push({ e: stor.epAr, a: true, r: [0, Math.PI / 2, 0], pos: [-31, 30, 0], l: 30 })
        // bas devant
        stor.listeLignePat.push({ e: stor.epAr, a: true, r: [0, Math.PI / 2, 0], pos: [-31, -30, 0], l: 30 })

        // gauche derrier
        stor.listeLignePat.push({ e: stor.epAr, a: true, r: [0, 0, Math.PI / 2], pos: [31, 0, -13], l: 65 })

        stor.listeLignePat.push({ e: stor.epAr, a: true, r: [0, 0, Math.PI / 2], pos: [31, 0, 13], l: 65 })
        // gauche devanat
        stor.listeLignePat.push({ e: stor.epAr, a: true, r: [0, 0, Math.PI / 2], pos: [-31, 0, -13], l: 65 })

        // droite devant
        stor.listeLignePat.push({ e: stor.epAr, a: true, r: [0, 0, Math.PI / 2], pos: [-31, 0, 13], l: 65 })

        /// //////////////////////////////
        stor.listeLignePat.push({ e: stor.epAr, a: true, r: [0, 0, 0], pos: [0, -31, -13], l: 65 })
        // droite
        stor.listeLignePat.push({ e: stor.epAr, a: true, r: [0, 0, 0], pos: [0, -31, 13], l: 65 })
        // lautro
        stor.listeLignePat.push({ e: stor.epAr, a: true, r: [0, 0, 0], pos: [0, 31, 13], l: 65 })

        // haut
        stor.listeLignePat.push({ e: stor.epAr, a: true, r: [0, 0, 0], pos: [0, 31, -13], l: 65 })

        stor.numFace = [[0], [1], [2], [3], [4], [5]]

        stor.numFacePat = [{ fixe: ['#s1'], moov: ['#s2'] }, { fixe: ['#s3'], moov: ['#s4'] }, { fixe: ['#s5'], moov: ['#s6'] }, { fixe: ['#s7'], moov: ['#s8'] }, { fixe: ['#s9'], moov: ['#s10'] }, { fixe: ['#s11'], moov: ['#s12'] }]
        stor.lescotes = [
          [
            { n: 0, perspe: [110, 198, 176], fixe: [['#seg0']], moov: [['#seg1']] },
            { n: 1, perspe: [119, 124], fixe: [['#seg2'], ['#seg3']], moov: [['#seg4'], ['#seg5']] },
            { n: 2, perspe: [117, 128, 130], fixe: [['#seg6'], ['#seg7']], moov: [['#seg8'], ['#seg9']] },
            { n: 3, perspe: [112, 184, 206], fixe: [['#seg10']], moov: [['#seg10']] }],
          [
            { n: 4, perspe: [115, 164], fixe: [['#seg11'], ['#seg12']], moov: [['#seg13'], ['#seg14']] },
            { n: 5, perspe: [114, 160], fixe: [['#seg15'], ['#seg16']], moov: [['#seg17'], ['#seg21']] },
            { n: 6, perspe: [116, 172], fixe: [['#seg18'], ['#seg19']], moov: [['#seg20'], ['#seg22']] },
            { n: 7, perspe: [113, 168], fixe: [['#seg23'], ['#seg24']], moov: [['#seg25'], ['#seg26']] }
          ], [
            { n: 8, perspe: [111, 188], fixe: [['#seg27']], moov: [['#seg28']] },
            { n: 9, perspe: [109, 180], fixe: [['#seg29']], moov: [['#seg30']] },
            { n: 10, perspe: [120, 196], fixe: [['#seg31']], moov: [['#seg32']] },
            { n: 11, perspe: [118, 192], fixe: [['#seg33'], ['#seg34']], moov: [['#seg35'], ['#seg36']] }
          ]
        ]
        stor.listep = [1, 3, 43]
        break // pavé droit

      case 'un prisme droit':
        if (stor.basequa) {
          stor.listeFacePat = []
          // gauche
          stor.listeFacePat.push({ a: true, r: [13 * Math.PI / 180, 0, Math.PI / 2], pos: [0, 0, -23], l: 60, l2: 60 })
          // dessous
          stor.listeFacePat.push({ a: true, r: [Math.PI / 2, 0, 0], pos: [0, -30, 1], l: 60, l2: 56 })
          // droite
          stor.listeFacePat.push({ a: true, r: [-27 * Math.PI / 180, 0, Math.PI / 2], pos: [0, 0, 17], l: 63, l2: 59 })
          // dessus
          stor.listeFacePat.push({ a: true, r: [Math.PI / 2, 0, 0], pos: [0, 30, -7], l: 60, l2: 15 })

          // derriere
          stor.listeFacePat.push({ a: 'p', r: [0, Math.PI / 2, 0], pos: [28, 0, 0], tab: [{ x: -30, y: -30 }, { x: 30, y: -30 }, { x: 15, y: 30 }, { x: 0, y: 30 }] })

          // devant
          stor.listeFacePat.push({ a: 'p', r: [0, Math.PI / 2, 0], pos: [-34, 0, 0], tab: [{ x: -30, y: -30 }, { x: 30, y: -30 }, { x: 15, y: 30 }, { x: 0, y: 30 }] })

          stor.listeLignePat = []

          // bas derriere
          stor.listeLignePat.push({ e: stor.epAr, a: true, r: [0, Math.PI / 2, 0], pos: [31, -31, 0], l: 65 })
          // haut derriere
          stor.listeLignePat.push({ e: stor.epAr, a: true, r: [0, Math.PI / 2, 0], pos: [31, 31, -8], l: 20 })
          // haut devant
          stor.listeLignePat.push({ e: stor.epAr, a: true, r: [0, Math.PI / 2, 0], pos: [-31, 30, -8], l: 20 })
          // bas devant
          stor.listeLignePat.push({ e: stor.epAr, a: true, r: [0, Math.PI / 2, 0], pos: [-31, -30, 0], l: 65 })

          // gauche derrier
          stor.listeLignePat.push({ e: stor.epAr, a: true, r: [13 * Math.PI / 180, 0, Math.PI / 2], pos: [31, 0, -23], l: 63 })

          // droite derriere
          stor.listeLignePat.push({ e: stor.epAr, a: true, r: [-27 * Math.PI / 180, 0, Math.PI / 2], pos: [31, 0, 17], l: 71 })
          // gauche devanat
          stor.listeLignePat.push({ e: stor.epAr, a: true, r: [13 * Math.PI / 180, 0, Math.PI / 2], pos: [-31, 0, -23], l: 63 })

          // droite devant
          stor.listeLignePat.push({ e: stor.epAr, a: true, r: [-27 * Math.PI / 180, 0, Math.PI / 2], pos: [-31, 0, 17], l: 71 })

          /// //////////////////////////////
          stor.listeLignePat.push({ e: stor.epAr, a: true, r: [0, 0, 0], pos: [0, -31, -29], l: 65 })
          // droite
          stor.listeLignePat.push({ e: stor.epAr, a: true, r: [0, 0, 0], pos: [0, -31, 32], l: 65 })
          // lautro
          stor.listeLignePat.push({ e: stor.epAr, a: true, r: [0, 0, 0], pos: [0, 31, 1], l: 65 })

          // haut
          stor.listeLignePat.push({ e: stor.epAr, a: true, r: [0, 0, 0], pos: [0, 31, -15], l: 65 })

          stor.numFace = [[0], [1], [2], [3], [4], [5]]
          stor.numFacePat = [
            { fixe: ['#s1'], moov: ['#s7'] },
            { fixe: ['#s2'], moov: ['#s8'] },
            { fixe: ['#s3'], moov: ['#s9'] },
            { fixe: ['#s4'], moov: ['#s10'] },
            { fixe: ['#s5'], moov: ['#s11'] },
            { fixe: ['#s6'], moov: ['#s12'] }]
          stor.ppos = 'pr1'
          stor.listep = [1, 2, 3, 36]
          stor.lescotes = [
            [
              { n: 11, perspe: [88, 128], fixe: [['#seg0'], ['#seg1']], moov: [['#seg2'], ['#seg3']] },
              { n: 8, perspe: [67, 100], fixe: [['#seg4']], moov: [['#seg7']] },
              { n: 9, perspe: [68, 108], fixe: [['#seg5']], moov: [['#seg6']] },
              { n: 10, perspe: [89, 118], fixe: [['#seg8']], moov: [['#seg9']] }],
            [
              { n: 1, perspe: [83, 157], fixe: [['#seg10'], ['#seg11']], moov: [['#seg12'], ['#seg13']] },
              { n: 2, perspe: [91], fixe: [['#seg14'], ['#seg15']], moov: [['#seg16'], ['#seg17']] }
            ],
            [
              { n: 5, perspe: [85, 146], fixe: [['#seg18'], ['#seg19']], moov: [['#seg20'], ['#seg25']] },
              { n: 7, perspe: [90], fixe: [['#seg21'], ['#seg22']], moov: [['#seg23'], ['#seg24']] }
            ],
            [
              { n: 4, perspe: [84, 168], fixe: [['#seg26'], ['#seg27']], moov: [['#seg28'], ['#seg29']] },
              { n: 6, perspe: [92], fixe: [['#seg30'], ['#seg31']], moov: [['#seg32'], ['#seg33']] }
            ],
            [
              { n: 0, perspe: [44, 135], fixe: [['#seg34']], moov: [['#seg35']] },
              { n: 3, perspe: [69], fixe: [['#seg36']], moov: [['#seg36']] }
            ]
          ]
        } else {
          stor.listeFacePat = []
          // gauche
          stor.listeFacePat.push({ a: true, r: [19 * Math.PI / 180, 0, Math.PI / 2], pos: [0, 0, -23], l: 37, l2: 36 })
          // dessous
          stor.listeFacePat.push({ a: true, r: [Math.PI / 2, 0, 0], pos: [0, -20, 0], l: 35, l2: 60 })
          // droite
          stor.listeFacePat.push({ a: true, r: [-50 * Math.PI / 180, 0, Math.PI / 2], pos: [0, 0, 7], l: 60, l2: 37 })

          // derriere
          stor.listeFacePat.push({ a: 'p', r: [0, Math.PI / 2, 0], pos: [18, 0, 0], tab: [{ x: -25, y: -18 }, { x: 28, y: -18 }, { x: 16, y: 18 }] })

          // devant
          stor.listeFacePat.push({ a: 'p', r: [0, Math.PI / 2, 0], pos: [-22, 0, 0], tab: [{ x: -25, y: -18 }, { x: 28, y: -18 }, { x: 16, y: 18 }] })

          stor.listeLignePat = []

          // bas derriere
          stor.listeLignePat.push({ e: stor.epAr, a: true, r: [0, Math.PI / 2, 0], pos: [20, -20, 0], l: 65 })

          // bas devant
          stor.listeLignePat.push({ e: stor.epAr, a: true, r: [0, Math.PI / 2, 0], pos: [-20, -20, 0], l: 65 })

          // gauche derrier
          stor.listeLignePat.push({ e: stor.epAr, a: true, r: [19 * Math.PI / 180, 0, Math.PI / 2], pos: [20, 0, -25], l: 45 })
          // droite derriere
          stor.listeLignePat.push({ e: stor.epAr, a: true, r: [-50 * Math.PI / 180, 0, Math.PI / 2], pos: [20, 0, 8], l: 65 })

          // gauche devanat
          stor.listeLignePat.push({ e: stor.epAr, a: true, r: [19 * Math.PI / 180, 0, Math.PI / 2], pos: [-20, 0, -25], l: 45 })

          // droite devant
          stor.listeLignePat.push({ e: stor.epAr, a: true, r: [-50 * Math.PI / 180, 0, Math.PI / 2], pos: [-20, 0, 8], l: 65 })

          /// //////////////////////////////
          stor.listeLignePat.push({ e: stor.epAr, a: true, r: [0, 0, 0], pos: [0, -20, -32], l: 45 })
          // droite
          stor.listeLignePat.push({ e: stor.epAr, a: true, r: [0, 0, 0], pos: [0, -20, 32], l: 45 })

          // haut
          stor.listeLignePat.push({ e: stor.epAr, a: true, r: [0, 0, 0], pos: [0, 20, -17], l: 45 })

          stor.numFace = [[0], [1], [2], [3], [4]]
          stor.numFacePat = [
            { fixe: ['#S76'], moov: ['#S81'] },
            { fixe: ['#S77'], moov: ['#S82'] },
            { fixe: ['#S78'], moov: ['#S83'] },
            { fixe: ['#S79'], moov: ['#S84'] },
            { fixe: ['#S80'], moov: ['#S85'] }]
          stor.ppos = 'pr4'
          stor.listep = [1, 2, 3, 34]
          stor.lescotes = [
            [
              { n: 8, perspe: [73, 95], fixe: [['#S91'], ['#S94']], moov: [['#S05'], ['#S08']] },
              { n: 6, perspe: [74, 104], fixe: [['#S92']], moov: [['#S06']] },
              { n: 7, perspe: [62, 86], fixe: [['#S93']], moov: [['#S07']] }],
            [
              { n: 2, perspe: [71, 124], fixe: [['#S86'], ['#S88']], moov: [['#S00'], ['#S02']] },
              { n: 4, perspe: [76], fixe: [['#S95'], ['#S98']], moov: [['#S09'], ['#S11']] }
            ],
            [
              { n: 3, perspe: [70, 114], fixe: [['#S87'], ['#S90']], moov: [['#S01'], ['#S04']] },
              { n: 5, perspe: [75], fixe: [['#S97'], ['#S99']], moov: [['#S10'], ['#S12']] }
            ],
            [
              { n: 0, perspe: [72, 134, 136], fixe: [['#S89']], moov: [['#S03']] },
              { n: 1, perspe: [77], fixe: [['#S96']], moov: [['#S96']] }
            ]
          ]
        }
        break // prisme droit

      case 'un cylindre de révolution':
        stor.listeFacePat = []
        // base bas
        stor.listeFacePat.push({ a: 'c', r: [0, 0, 0], pos: [0, 31, 0], l1: 30, l2: 30, e: 5 })
        // lateral
        stor.listeFacePat.push({ a: 't', r: [Math.PI / 2, 0, 0], pos: [0, 30, 0], l1: 32, l2: 29, e: 60 })
        // base haut
        stor.listeFacePat.push({ a: 'c', r: [0, 0, 0], pos: [0, -31, 0], l1: 30, l2: 30, e: 5 })

        stor.listeLignePat = []

        // cercle base devant
        stor.listeLignePat.push({ e: stor.epAr / 2, a: 'c', r: [Math.PI / 2, 0, 0], pos: [0, -31, 0], l1: 30, l2: 30 })
        // cercle base derriere
        stor.listeLignePat.push({ e: stor.epAr / 2, a: 'c', r: [Math.PI / 2, 0, 0], pos: [0, 31, 0], l1: 30, l2: 30 })
        // gene1
        stor.listeLignePat.push({ e: stor.epAr / 5, a: true, r: [0, 0, Math.PI / 2], pos: [32, 0, 0], l: 65 })
        // gene2
        stor.listeLignePat.push({ e: stor.epAr / 5, a: true, r: [0, 0, Math.PI / 2], pos: [-32, 0, 0], l: 65 })
        // gene3
        stor.listeLignePat.push({ e: stor.epAr / 5, a: true, r: [0, 0, Math.PI / 2], pos: [0, 0, -32], l: 65 })
        // gene3
        stor.listeLignePat.push({ e: stor.epAr / 5, a: true, r: [0, 0, Math.PI / 2], pos: [0, 0, 32], l: 65 })

        stor.numFace = [[0], [1], [2]]
        stor.numFacePat = [
          { fixe: ['#r1'], moov: ['#yoyo'] },
          { fixe: ['#r2'], moov: ['#sp', '#s1', '#s2', '#s3', '#s4', '#s5', '#s6', '#s7', '#s8', '#s9', '#s10', '#s11', '#s12', '#s13', '#s14', '#s15', '#s16', '#s17', '#s18', '#s19', '#s20', '#s21', '#s22', '#s23', '#s24', '#s25', '#s26', '#s27', '#s28', '#s29', '#s30', '#s40', '#s41', '#s42', '#s43', '#s44', '#s45', '#s46', '#s47', '#s48', '#s49', '#s50', '#s51', '#s52', '#s53', '#s54', '#s55', '#s56', '#s57', '#s58', '#s59', '#s60', '#s61', '#s62', '#s63', '#s64', '#s65', '#s66', '#s67', '#s68', '#s69', '#s70', '#s71', '#s72', '#s73'] },
          { fixe: ['#r3'], moov: ['#SF'] }]
        stor.ppos = 'cy1'
        stor.listep = [1, 2, 61]
        stor.lescotes =
          [ // deux peri + 2 long
            [
              { n: 1, perspe: [78, 107], fixe: [[3], ['#v1']], moov: [['#m1', '#m2', '#m3', '#m4'], ['#sdf', '#arc4', '#arc3']] },
              { n: 0, perspe: [72, 110], fixe: [['#c2'], ['#v3']], moov: [['#fer1', '#fer2', '#t2', '#t1'], ['#l1', '#l2', '#arc1', '#arc2']] }
            ],
            [// 1 hauteur
              { n: 2, perspe: [88], fixe: [['#v2'], ['#v4']], moov: [[623], [624]] }
            ]
          ]
        stor.ligne = 'une ligne'
        stor.lignesS = 'lignes'
        stor.toulignes = 'toutes les lignes'
        stor.lignes = 'plusieurs lignes'
        stor.leligne = 'la ligne'
        stor.lammaret = 'ligne'
        stor.larettt = 'la ligne'
        stor.lale = 'la'
        stor.ligneE = 'te'
        break // cylindre de révolution

      case 'une pyramide':
        if (stor.basequa) {
          stor.listeFacePat = []

          // gauche
          stor.listeFacePat.push({ a: 'p', r: [31 * Math.PI / 180, 0, 0], pos: [0, 0, -15], tab: [{ x: -27, y: -32 }, { x: 27, y: -32 }, { x: 0, y: 14 }] })
          // dessous
          stor.listeFacePat.push({ a: true, r: [Math.PI / 2, 0, 0], pos: [0, -30, 0], l: 60, l2: 56 })
          // droite
          stor.listeFacePat.push({ a: 'p', r: [-30 * Math.PI / 180, 0, 0], pos: [0, -3, 8], tab: [{ x: -27, y: -32 }, { x: 27, y: -32 }, { x: 0, y: 14 }] })
          // devant
          stor.listeFacePat.push({ a: 'p', r: [Math.PI / 2, 55 * Math.PI / 180, -Math.PI / 2], pos: [-13, 0, 0], tab: [{ x: -25, y: -33 }, { x: 25, y: -33 }, { x: 0, y: 15 }] })
          // deriere
          stor.listeFacePat.push({ a: 'p', r: [Math.PI / 2, -55 * Math.PI / 180, Math.PI / 2], pos: [13, 0, 0], tab: [{ x: -25, y: -33 }, { x: 25, y: -33 }, { x: 0, y: 15 }] })
          stor.listeLignePat = []

          // bas derriere
          stor.listeLignePat.push({ e: stor.epAr, a: true, r: [0, Math.PI / 2, 0], pos: [31, -31, 0], l: 65 })

          // bas devant
          stor.listeLignePat.push({ e: stor.epAr, a: true, r: [0, Math.PI / 2, 0], pos: [-31, -31, 0], l: 65 })

          // gauche derrier
          stor.listeLignePat.push({ e: stor.epAr, a: true, r: [31 * Math.PI / 180, 0, -60 * Math.PI / 180], pos: [14, -6, -14], l: 63 })

          // droite derriere
          stor.listeLignePat.push({ e: stor.epAr, a: true, r: [-31 * Math.PI / 180, 0, -60 * Math.PI / 180], pos: [14, -6, 14], l: 63 })
          // gauche devanat
          stor.listeLignePat.push({ e: stor.epAr, a: true, r: [31 * Math.PI / 180, 0, 60 * Math.PI / 180], pos: [-14, -6, -14], l: 63 })

          // droite devant
          stor.listeLignePat.push({ e: stor.epAr, a: true, r: [-31 * Math.PI / 180, 0, 60 * Math.PI / 180], pos: [-14, -6, 14], l: 63 })

          /// //////////////////////////////
          stor.listeLignePat.push({ e: stor.epAr, a: true, r: [0, 0, 0], pos: [0, -31, -31], l: 65 })
          // droite
          stor.listeLignePat.push({ e: stor.epAr, a: true, r: [0, 0, 0], pos: [0, -31, 31], l: 65 })

          stor.numFace = [[0], [1], [2], [4], [3]]
          stor.numFacePat = [
            { fixe: ['#S52'], moov: ['#S57'] },
            { fixe: ['#S53'], moov: ['#S58'] },
            { fixe: ['#S54'], moov: ['#S59'] },
            { fixe: ['#S55'], moov: ['#S60'] },
            { fixe: ['#S56'], moov: ['#S61'] }]
          stor.ppos = 'pi1'
          stor.listep = [1, 2, 3, 27, 107]
          stor.lescotes = [
            [ // haut
              { n: 0, perspe: [72, 116], fixe: [['#S65']], moov: [['#S77']] }
            ],
            [ // bas
              { n: 1, perspe: [70, 132], fixe: [['#S70']], moov: [['#S70']] }
            ],
            [ // gauche
              { n: 6, perspe: [69, 124], fixe: [['#S67']], moov: [['#S79']] }
            ],
            [ // droite
              { n: 7, perspe: [71, 140], fixe: [['#S68']], moov: [['#S80']] }
            ],
            [ // diag hg
              { n: 2, perspe: [74, 92], fixe: [['#S62'], ['#S64']], moov: [['#S74'], ['#S76']] }
            ],
            [ // diag bg
              { n: 4, perspe: [100, 75], fixe: [['#S69'], ['#S72']], moov: [['#S81'], ['#S83']] }
            ],
            [ // diag hd
              { n: 3, perspe: [73, 84], fixe: [['#S63'], ['#S66']], moov: [['#S75'], ['#S78']] }
            ],
            [ // diag bd
              { n: 5, perspe: [108, 76], fixe: [['#S71'], ['#S73']], moov: [['#S82'], ['#S84']] }
            ]
          ]
        } else {
          stor.listeFacePat = []

          // gauche
          stor.listeFacePat.push({ a: 'p', r: [-27.5 * Math.PI / 180, -55 * Math.PI / 180, 0], pos: [0, 0, 0], tab: [{ x: -50, y: -15 }, { x: 4, y: -35 }, { x: 5, y: 11 }] })
          // dessous
          stor.listeFacePat.push({ a: 'p', r: [Math.PI / 2, 0, 0], pos: [0, -28, 0], tab: [{ x: -2, y: 19 }, { x: -30, y: 26 }, { x: -29, y: -26 }] })

          // droite
          stor.listeFacePat.push({ a: 'p', r: [-20 * Math.PI / 180, 20 * Math.PI / 180, -33 * Math.PI / 180], pos: [0, -5, 5], tab: [{ x: -7, y: -42 }, { x: 13, y: -26.5 }, { x: -9, y: 8 }] })
          // devant

          stor.listeFacePat.push({ a: 'p', r: [Math.PI / 2, 55 * Math.PI / 180, -Math.PI / 2], pos: [-13, 0, 0], tab: [{ x: -25, y: -33 }, { x: 25, y: -33 }, { x: 0, y: 15 }] })

          stor.listeLignePat = []

          // bas devant
          stor.listeLignePat.push({ e: stor.epAr, a: true, r: [0, Math.PI / 2, 0], pos: [-31, -31, 0], l: 65 })

          // droite derriere
          stor.listeLignePat.push({ e: stor.epAr, a: true, r: [-25 * Math.PI / 180, 0, -91 * Math.PI / 180], pos: [0, -8, 10], l: 54 })
          // gauche devanat
          stor.listeLignePat.push({ e: stor.epAr, a: true, r: [31 * Math.PI / 180, 0, 60 * Math.PI / 180], pos: [-14, -6, -14], l: 63 })

          // droite devant
          stor.listeLignePat.push({ e: stor.epAr, a: true, r: [-31 * Math.PI / 180, 0, 60 * Math.PI / 180], pos: [-14, -6, 14], l: 63 })

          /// //////////////////////////////
          stor.listeLignePat.push({ e: stor.epAr, a: true, r: [0, -60 * Math.PI / 180, 0], pos: [-14, -31, -5], l: 60 })
          // droite
          stor.listeLignePat.push({ e: stor.epAr, a: true, r: [0, 20 * Math.PI / 180, 0], pos: [-15, -31, 25], l: 36 })

          stor.numFace = [[2], [3], [1], [0]]
          stor.numFacePat = [
            { fixe: ['#S36'], moov: ['#S38'] },
            { fixe: ['#S35'], moov: ['#S39'] },
            { fixe: ['#S37'], moov: ['#S41'] },
            { fixe: ['#S34'], moov: ['#S40'] }]
          stor.ppos = 'pi4'
          stor.listep = [1, 2, 3, 5, 21]
          stor.lescotes = [
            [ // bas
              { n: 0, perspe: [70, 99], fixe: [['#S48']], moov: [['#S48']] }
            ],
            [ // gauche
              { n: 4, perspe: [100, 108], fixe: [['#S45']], moov: [['#S54']] }
            ],
            [ // droite
              { n: 5, perspe: [69, 116], fixe: [['#S46']], moov: [['#S55']] }
            ],
            [ // diag h
              { n: 1, perspe: [68, 91], fixe: [['#S42'], ['#S44']], moov: [['#S51'], ['#S53']] }
            ],
            [ // diag g
              { n: 2, perspe: [67, 77], fixe: [['#S43'], ['#S49']], moov: [['#S52'], ['#S57']] }
            ],
            [ // diag d
              { n: 3, perspe: [61, 84], fixe: [['#S47'], ['#S50']], moov: [['#S56'], ['#S58']] }
            ]
          ]
        }
        break // pyramide

      case 'un cône de révolution':
        stor.listeFacePat = []

        // lateral
        stor.listeFacePat.push({ a: 'co', r: [0, 0, 0], pos: [0, 0, 0], l1: 32, l2: 29, e: 60 })
        // base haut
        stor.listeFacePat.push({ a: 'c', r: [0, 0, 0], pos: [0, -31, 0], l1: 30, l2: 30, e: 5 })

        stor.listeLignePat = []

        // cercle base devant
        stor.listeLignePat.push({ e: stor.epAr / 2, a: 'c', r: [Math.PI / 2, 0, 0], pos: [0, -31, 0], l1: 30, l2: 30 })

        // gene1
        stor.listeLignePat.push({ e: stor.epAr / 5, a: true, r: [0, 0, -64 * Math.PI / 180], pos: [16, 0, 0], l: 73 })
        // gene2
        stor.listeLignePat.push({ e: stor.epAr / 5, a: true, r: [0, 0, 64 * Math.PI / 180], pos: [-16, 0, 0], l: 73 })
        // gene3
        stor.listeLignePat.push({ e: stor.epAr / 5, a: true, r: [0, 90 * Math.PI / 180, 64 * Math.PI / 180], pos: [0, 0, 16], l: 73 })
        // gene3
        stor.listeLignePat.push({ e: stor.epAr / 5, a: true, r: [0, 90 * Math.PI / 180, -64 * Math.PI / 180], pos: [0, 0, -16], l: 73 })

        stor.numFace = [[1], [0]]
        stor.numFacePat = [
          { fixe: ['#s1'], moov: ['#S33'] },
          { fixe: ['#s2'], moov: ['#S16', '#S17', '#S18', '#S19', '#S20', '#S21', '#S22', '#S23', '#S24', '#S25', '#S26', '#S27', '#S28', '#S29', '#S30', '#S31', '#S32', '#S38', '#S39', '#S40', '#S41', '#S42', '#S43', '#S44', '#S45', '#S46', '#S47', '#S48', '#S49', '#S50', '#S51', '#S52', '#S53', '#S54', '#S55', '#S56', '#S57', '#S58', '#S59', '#S60', '#S61', '#S62', '#S63', '#S64', '#S65', '#S66', '#S67', '#S68', '#S70', '#S71', '#S72', '#S73', '#S74', '#S75', '#S76', '#S77', '#S78', '#S79'] }]
        stor.ppos = 'co1'
        stor.listep = [1, 2, 70]
        stor.lescotes = [
          [ // basE
            { n: 0, perspe: [49, 102], fixe: [['#s3'], ['#s33']], moov: [['#s116', '#s117', '#S05', '#S02'], ['#S34', '#S02', '#s343', '#s226', '#s227', '#s296', '#s364', '#S98']] }
          ],
          [ // rayons secteur
            { n: 4, perspe: [], fixe: [[34], [35]], moov: [['#s344', '#s249'], ['#s297', '#s250']] }
          ]
        ]
        stor.ligne = 'une ligne'
        stor.lignesS = 'lignes'
        stor.toulignes = 'toutes les lignes'
        stor.lignes = 'plusieurs lignes'
        stor.leligne = 'la ligne'
        stor.lammaret = 'arête'
        stor.larettt = 'l’arête'
        stor.lale = 'la'
        stor.ligneE = 'te'
        break // cône de révolution

      default:
        me.notif(Error(`solide « ${stor.exo.solide} » non géré`))
    }

    if (stor.encours === 'c') {
      poseChoix()
    }
    if (stor.encours === 'f') {
      poseFace()
    }
    if (stor.encours === 'a') {
      poseAretes()
    }

    makefig()
    if (stor.encours !== 'm') {
      stor.lesdiv.travail.classList.add('travail')
    } else {
      j3pAddClass('#JLKJKLl0c3', 'travail')
    }
    stor.lesdiv.consigne.classList.add('enonce')
    stor.lesdiv.solution.classList.remove('correction')
    stor.lesdiv.explications.classList.remove('explique')
  }

  function poseChoix () {
    stor.lareteNi = 700
    let ppos = ['pr4', 'pr5', 'pr6', 'pi4', 'pi5', 'pi6', 'cu1', 'cu2', 'cu3', 'pa1', 'pa2', 'pa3', 'pr1', 'pr2', 'pr3', 'cy1', 'cy2', 'cy3', 'co1', 'co2', 'co3', 'pi1', 'pi2', 'pi3']
    stor.ppos = []
    j3pAffiche(stor.lesdiv.consigne, null, '\n<b>Sélectionne un patron possible de ce solide.</b>')
    switch (stor.exo.solide) {
      case 'un cube': stor.ppos = ['cu1', 'cu2', 'cu3']
        break
      case 'un pavé droit': stor.ppos = ['pa1', 'pa2', 'pa3']
        break
      case 'un prisme droit':
        if (stor.basequa) { stor.ppos = ['pr1', 'pr2', 'pr3'] } else { stor.ppos = ['pr4', 'pr5', 'pr6'] }
        break
      case 'un cylindre de révolution': stor.ppos = ['cy1', 'cy2', 'cy3']
        break
      case 'une pyramide':
        if (stor.basequa) { stor.ppos = ['pi1', 'pi2', 'pi3'] } else { stor.ppos = ['pi4', 'pi5', 'pi6'] }
        break
      case 'un cône de révolution': stor.ppos = ['co1', 'co2', 'co3']
        break
      default:
        me.notif(Error(`solide « ${stor.exo.solide} » non géré`))
    }
    for (let i = 0; i < 3; i++) {
      ppos.splice(ppos.indexOf(stor.ppos[i]), 1)
    }
    ppos = j3pShuffle(ppos)
    stor.ppos.push(ppos.pop())
    stor.ppos.push(ppos.pop())
    stor.ppos.push(ppos.pop())
    stor.txtfigChoix = []
    for (let i = 0; i < 6; i++) {
      stor.txtfigChoix.push(detTextPat(stor.ppos[i]))
    }
    stor.lesrep = [0, 1, 2, 3, 4, 5]
    stor.lesrep = j3pShuffle(stor.lesrep)
    stor.repG = stor.lesrep.indexOf(0)
    stor.lazone = new Zoneclick(stor.lesdiv.travail, stor.lesrep, { reponsesmultiples: false, width: stor.widthetiq, height: stor.heightetiq, image: true, dispo: 'fixe', colonne: 3 })
    let idbuf
    stor.mtgAppLecteur.removeAllDoc()
    for (let i = 0; i < 6; i++) {
      idbuf = stor.lazone.recupid(i)
      const yu = faisFigEtiq(idbuf, stor.txtfigChoix[stor.lesrep[i]])
      if (stor.lesrep[i] === 0) stor.lid = yu
    }
  }

  function poseFace () {
    stor.lareteNi = 700
    stor.cFs = [127, 0, 255]
    stor.cFns = [150, 50, 200]
    stor.cFnos = [255, 255, 255]
    stor.cFos = [100, 0, 150]
    stor.facer = []
    stor.facev = []
    let vpos
    stor.bufID = svgPat
    j3pAffiche(stor.lesdiv.consigne, null, '\n<b>Sélectionne la face du patron qui correspond à la face violette du solide.</b>\n<i>(Quand on replie ce patron vers le haut)</i>')

    switch (stor.exo.solide) {
      case 'un cube':
        stor.faceb = j3pGetRandomInt(1, 6)
        stor.faceb = 6
        stor.exo.col = false
        if (stor.exo.col) {
          switch (stor.faceb) {
            case 1: stor.facer = [3]
              break
            case 2: stor.facer = [4]
              break
            case 3: stor.facer = [1]
              break
            case 4: stor.facer = [2]
              break
            case 5: stor.facer = [6]
              break
            case 6: stor.facer = [5]
              break
          }
        } else {
          switch (stor.faceb) {
            case 1:
              stor.facer = [j3pGetRandomInt(0, 1) + 5]
              stor.facev = [j3pGetRandomInt(1, 2) * 2]
              break
            case 2:
              stor.facer = [j3pGetRandomInt(0, 1) + 5]
              stor.facev = [j3pGetRandomInt(0, 1) * 2 + 1]
              break
            case 3:
              stor.facer = [j3pGetRandomInt(0, 1) + 5]
              stor.facev = [j3pGetRandomInt(1, 2) * 2]
              break
            case 4:
              stor.facer = [j3pGetRandomInt(0, 1) + 5]
              stor.facev = [j3pGetRandomInt(0, 1) * 2 + 1]
              break
            case 5:
              stor.facev = [j3pGetRandomInt(1, 2) * 2]
              stor.facer = [j3pGetRandomInt(0, 1) * 2 + 1]
              break
            case 6:
              stor.facev = [j3pGetRandomInt(1, 2) * 2]
              stor.facer = [j3pGetRandomInt(0, 1) * 2 + 1]
              break
          }
        }
        break
      case 'un pavé droit':
        stor.faceb = j3pGetRandomInt(1, 6)
        if (stor.exo.col) {
          switch (stor.faceb) {
            case 1: stor.facer = [3]
              break
            case 2: stor.facer = [4]
              break
            case 3: stor.facer = [1]
              break
            case 4: stor.facer = [2]
              break
            case 5: stor.facer = [6]
              break
            case 6: stor.facer = [5]
              break
          }
        } else {
          switch (stor.faceb) {
            case 1:
              stor.facer = [j3pGetRandomInt(0, 1) + 5]
              stor.facev = [j3pGetRandomInt(1, 2) * 2]
              break
            case 2:
              stor.facer = [j3pGetRandomInt(0, 1) + 5]
              stor.facev = [j3pGetRandomInt(0, 1) * 2 + 1]
              break
            case 3:
              stor.facer = [j3pGetRandomInt(0, 1) + 5]
              stor.facev = [j3pGetRandomInt(1, 2) * 2]
              break
            case 4:
              stor.facer = [j3pGetRandomInt(0, 1) + 5]
              stor.facev = [j3pGetRandomInt(0, 1) * 2 + 1]
              break
            case 5:
              stor.facev = [j3pGetRandomInt(1, 2) * 2]
              stor.facer = [j3pGetRandomInt(0, 1) * 2 + 1]
              break
            case 6:
              stor.facev = [j3pGetRandomInt(1, 2) * 2]
              stor.facer = [j3pGetRandomInt(0, 1) * 2 + 1]
              break
          }
        }
        break
      case 'un prisme droit':
        if (stor.basequa) {
          stor.faceb = j3pGetRandomInt(1, 6)
          if (stor.exo.col) {
            switch (stor.faceb) {
              case 1: stor.facer = [3]
                break
              case 2: stor.facer = [4]
                break
              case 3: stor.facer = [1]
                break
              case 4: stor.facer = [2]
                break
              case 5: stor.facer = [6]
                break
              case 6: stor.facer = [5]
                break
            }
          } else {
            switch (stor.faceb) {
              case 1:
                stor.facer = [j3pGetRandomInt(0, 1) + 5]
                stor.facev = [j3pGetRandomInt(1, 2) * 2]
                break
              case 2:
                stor.facer = [j3pGetRandomInt(0, 1) + 5]
                stor.facev = [j3pGetRandomInt(0, 1) * 2 + 1]
                break
              case 3:
                stor.facer = [j3pGetRandomInt(0, 1) + 5]
                stor.facev = [j3pGetRandomInt(1, 2) * 2]
                break
              case 4:
                stor.facer = [j3pGetRandomInt(0, 1) + 5]
                stor.facev = [j3pGetRandomInt(0, 1) * 2 + 1]
                break
              case 5:
                stor.facev = [j3pGetRandomInt(1, 2) * 2]
                stor.facer = [j3pGetRandomInt(0, 1) * 2 + 1]
                break
              case 6:
                stor.facev = [j3pGetRandomInt(1, 2) * 2]
                stor.facer = [j3pGetRandomInt(0, 1) * 2 + 1]
                break
            }
          }
        } else {
          vpos = [1, 2, 3, 4, 5]
          vpos = j3pShuffle(vpos)
          stor.faceb = vpos.pop()
          stor.facer = [vpos.pop()]
          stor.facev = [vpos.pop()]
        }
        break
      case 'un cylindre de révolution':
        if (stor.exo.col) {
          stor.faceb = j3pGetRandomInt(0, 1) * 2 + 1
          stor.facer = [-stor.faceb + 4]
        } else {
          stor.faceb = 2
        }
        break
      case 'une pyramide':
        stor.exo.col = false
        if (stor.basequa) {
          if (stor.exo.col) {
            stor.faceb = 2
          } else {
            vpos = [1, 5, 3, 4]
            vpos = j3pShuffle(vpos)
            stor.faceb = vpos.pop()
            vpos.push(2)
            vpos = j3pShuffle(vpos)
            stor.facer = [vpos.pop()]
            stor.facev = [vpos.pop()]
          }
        } else {
          vpos = [1, 2, 3, 4]
          vpos = j3pShuffle(vpos)
          stor.faceb = vpos.pop()
          stor.facer = [vpos.pop()]
          stor.facev = [vpos.pop()]
        }
        break
      case 'un cône de révolution':
        if (stor.exo.col) {
          stor.faceb = 1
        } else {
          stor.faceb = 2
        }
        break

      default:
        me.notif(Error(`solide « ${stor.exo.solide} » non géré`))
    }
    stor.txtPat = detTextPat(stor.ppos)
    affPat()
    afficheSegFix(true)
    stor.nbclic = 0
  }

  function poseAretes () {
    stor.cFs = [100, 100, 100]
    stor.cFns = [100, 100, 100]
    stor.cFnos = [0, 0, 0]
    stor.cFos = [255, 0, 0]
    if (stor.exo.col) {
      j3pAffiche(stor.lesdiv.consigne, null, '\n<b>Sélectionne ' + stor.leligne + ' du patron qui représente la même ' + stor.lammaret + ' du solide.</b>')
    } else {
      j3pAffiche(stor.lesdiv.consigne, null, '\n<b>Sélectionne ' + stor.toulignes + ' du patron qui doivent avoir la même longueur que ' + stor.leligne + ' violet' + stor.ligneE + '.</b>')
    }
    stor.txtPat = detTextPat(stor.ppos)
    do {
      stor.legroupel = j3pGetRandomInt(0, stor.lescotes.length - 1)
      stor.lareteN = j3pGetRandomInt(0, stor.lescotes[stor.legroupel].length - 1)
      stor.laligneP = j3pGetRandomInt(0, stor.lescotes[stor.legroupel][stor.lareteN].fixe.length - 1)
    } while ((stor.exo.col && stor.lescotes[stor.legroupel][stor.lareteN].fixe.length === 1) ||
    (!stor.exo.col && stor.lescotes[stor.legroupel].length === 1 && stor.lescotes[stor.legroupel][stor.lareteN].fixe.length === 1))
    stor.lareteNi = stor.lescotes[stor.legroupel][stor.lareteN].n
    affPat()
    stor.nbclic = 0
  }

  function affPat () {
    const txtFigure = stor.txtPat
    svgPat = j3pGetNewId('svgPat')
    stor.svgPat = svgPat
    j3pCreeSVG(stor.lesdiv.travail, { id: svgPat, width: 400, height: 400 })
    stor.lesdiv.travail.style.border = 'solid black 1px'
    stor.mtgAppLecteur.removeAllDoc()
    stor.mtgAppLecteur.addDoc(svgPat, txtFigure, true)
    stor.mtgAppLecteur.calculateAndDisplayAll(true)
    for (let i = 0; i < stor.listep.length; i++) {
      stor.mtgAppLecteur.translatePoint(svgPat, stor.listep[i], stor.decx, stor.decy, true)
    }
    stor.mtgAppLecteur.zoom(svgPat, 200, 200, stor.ratZoom, true)

    if (stor.encours === 'f') {
      stor.FaceSel = []
      for (let i = 0; i < stor.numFacePat.length; i++) {
        stor.FaceSel.push({ select: false, id: [], moov: [], fautVoir: false, coolMo: [] })
        for (let j = 0; j < stor.numFacePat[i].fixe.length; j++) {
          stor.mtgAppLecteur.setVisible(svgPat, stor.numFacePat[i].fixe[j], true, true)
          stor.mtgAppLecteur.setColor(svgPat, stor.numFacePat[i].fixe[j], 255, 255, 255, true)
          stor.FaceSel[stor.FaceSel.length - 1].id.push(stor.numFacePat[i].fixe[j])
          rendFaceSel(stor.numFacePat[i].fixe[j])
        }
        for (let j = 0; j < stor.numFacePat[i].moov.length; j++) {
          stor.FaceSel[stor.FaceSel.length - 1].moov.push(stor.numFacePat[i].moov[j])
        }
      }
      for (let i = 0; i < stor.numFace[stor.faceb - 1].length; i++) {
        stor.mtgAppLecteur.setVisible(svgPat, stor.numFace[stor.faceb - 1][i], true, true)
        stor.mtgAppLecteur.setColor(svgPat, stor.numFace[stor.faceb - 1][i], 127, 0, 255, true)
      }
      for (let j = 0; j < stor.facer.length; j++) {
        stor.FaceSel[stor.facer[j] - 1].disabled = true
        stor.FaceSel[stor.facer[j] - 1].fautVoir = true
        stor.FaceSel[stor.facer[j] - 1].coolMo = [255, 0, 0]
        for (let i = 0; i < stor.numFace[stor.facer[j] - 1].length; i++) {
          stor.mtgAppLecteur.setVisible(svgPat, stor.numFace[stor.facer[j] - 1][i], true, true)
          stor.mtgAppLecteur.setColor(svgPat, stor.numFace[stor.facer[j] - 1][i], 255, 0, 0, true)
          for (let k = 0; k < stor.numFacePat[stor.facer[j] - 1].fixe.length; k++) {
            stor.mtgAppLecteur.setColor(svgPat, stor.numFacePat[stor.facer[j] - 1].fixe[k], 255, 0, 0, true)
          }
          for (let k = 0; k < stor.numFacePat[stor.facer[j] - 1].moov.length; k++) {
            stor.mtgAppLecteur.setColor(svgPat, stor.numFacePat[stor.facer[j] - 1].moov[i], 255, 0, 0, true)
          }
        }
      }
      for (let j = 0; j < stor.facev.length; j++) {
        stor.FaceSel[stor.facev[j] - 1].disabled = true
        stor.FaceSel[stor.facev[j] - 1].fautVoir = true
        stor.FaceSel[stor.facev[j] - 1].coolMo = [0, 150, 0]
        for (let i = 0; i < stor.numFace[stor.facev[j] - 1].length; i++) {
          stor.mtgAppLecteur.setVisible(svgPat, stor.numFace[stor.facev[j] - 1][i], true, true)
          stor.mtgAppLecteur.setColor(svgPat, stor.numFace[stor.facev[j] - 1][i], 0, 150, 0, true)
          for (let k = 0; k < stor.numFacePat[stor.facev[j] - 1].fixe.length; k++) {
            stor.mtgAppLecteur.setColor(svgPat, stor.numFacePat[stor.facev[j] - 1].fixe[k], 0, 150, 0, true)
          }
          for (let k = 0; k < stor.numFacePat[stor.facev[j] - 1].moov.length; k++) {
            stor.mtgAppLecteur.setColor(svgPat, stor.numFacePat[stor.facev[j] - 1].moov[i], 0, 150, 0, true)
          }
        }
      }
      stor.mtgAppLecteur.updateFigure(svgPat)
    }
    if (stor.encours === 'a') {
      stor.FaceSel = []
      for (let i = 0; i < stor.lescotes.length; i++) {
        for (let k = 0; k < stor.lescotes[i].length; k++) {
          for (let j = 0; j < stor.lescotes[i][k].fixe.length; j++) {
            stor.FaceSel.push({ select: false, id: [], moov: stor.lescotes[i][k].moov[j], fautVoir: false, coolMo: [], groupe: i, areteN: k, seg: j })
            for (let l = 0; l < stor.lescotes[i][k].fixe[j].length; l++) {
              stor.mtgAppLecteur.setVisible(svgPat, stor.lescotes[i][k].fixe[j][l], true, true)
              stor.mtgAppLecteur.setColor(svgPat, stor.lescotes[i][k].fixe[j][l], 0, 0, 0, true)
              stor.mtgAppLecteur.setLineStyle(svgPat, stor.lescotes[i][k].fixe[j][l], null, 4, true)
              stor.FaceSel[stor.FaceSel.length - 1].id.push(stor.lescotes[i][k].fixe[j][l])
              rendFaceSel(stor.lescotes[i][k].fixe[j][l])
            }
          }
        }
      }
      // met cool à la fixe choisie
      for (let i = 0; i < stor.FaceSel.length; i++) {
        if (stor.FaceSel[i].groupe === stor.legroupel && stor.FaceSel[i].areteN === stor.lareteN && stor.FaceSel[i].seg === stor.laligneP) {
          colorFace(stor.FaceSel[i].id, 127, 0, 255, 5)
          stor.FaceSel[i].disabled = true
          stor.FaceSel[i].fautVoir = true
          stor.FaceSel[i].coolMo = [127, 0, 255]
        }
      }
    }
  }

  function rendFaceSel (numh) {
    let ok = false
    let numi
    for (let i = 0; i < stor.FaceSel.length; i++) {
      for (let j = 0; j < stor.FaceSel[i].id.length; j++) {
        if (stor.FaceSel[i].id[j] === numh) ok = true
      }
      if (ok) {
        numi = i
        break
      }
    }
    const tabh = []
    for (let i = 0; i < stor.FaceSel[numi].id.length; i++) {
      tabh.push(stor.FaceSel[numi].id[i])
    }

    // cree func over
    stor.mtgAppLecteur.addEventListener(svgPat, numh, 'mouseover', function () {
      if (stor.FaceSel[numi].disabled) return
      stor.mtgAppLecteur.getDoc(svgPat).defaultCursor = 'pointer'
      if (stor.FaceSel[numi].select) {
        colorFace(tabh, stor.cFs[0], stor.cFs[1], stor.cFs[2], 7)
      } else {
        colorFace(tabh, stor.cFns[0], stor.cFns[1], stor.cFns[2], 7)
      }
    })
    // cree fonc out
    stor.mtgAppLecteur.addEventListener(svgPat, numh, 'mouseout', function () {
      if (stor.FaceSel[numi].disabled) return
      stor.mtgAppLecteur.getDoc(svgPat).defaultCursor = null
      if (stor.FaceSel[numi].select) {
        colorFace(tabh, stor.cFos[0], stor.cFos[1], stor.cFos[2], 5)
      } else {
        colorFace(tabh, stor.cFnos[0], stor.cFnos[1], stor.cFnos[2], 4)
      }
    })
    // cree fonc click
    stor.mtgAppLecteur.addEventListener(svgPat, numh, 'click', function () {
      // retrouv le num dans stor.FaceSel pour changer select
      if (stor.FaceSel[numi].disabled) return
      stor.FaceSel[numi].select = !stor.FaceSel[numi].select
      if (stor.FaceSel[numi].select) {
        colorFace(tabh, stor.cFos[0], stor.cFos[1], stor.cFos[2], 7)
        stor.nbclic++
      } else {
        colorFace(tabh, stor.cFnos[0], stor.cFnos[1], stor.cFnos[2], 7)
        stor.nbclic--
      }
    })
  }

  function colorFace (tab, n1, n2, n3, n4) {
    for (let i = 0; i < tab.length; i++) {
      stor.mtgAppLecteur.setColor(svgPat, tab[i], n1, n2, n3, true)
      if (stor.yaya === 'a') stor.mtgAppLecteur.setLineStyle(svgPat, tab[i], null, n4, true)
    }
  }

  function detTextPat (ki) {
    switch (ki) {
      case 'cu1': return 'TWF0aEdyYXBoSmF2YTEuMAAAABM+TMzNAAJmcv###wEA#wEAAAAAAAAAAANLAAAChwAAAQEAAAAAAAAAAQAAANP#####AAAAAQAKQ0NhbGNDb25zdAD#####AAJwaQAWMy4xNDE1OTI2NTM1ODk3OTMyMzg0Nv####8AAAABAApDQ29uc3RhbnRlQAkh+1RELRj#####AAAAAQAKQ1BvaW50QmFzZQD#####AAAAAAAQAAAAQAgAAAAAAAAAAAAAAAAAAAAABQAAQEuAAAAAAAFAe#4UeuFHrv####8AAAABAAdDQ2FsY3VsAP####8ABW1pbmkxAAEwAAAAAQAAAAAAAAAAAAAAAwD#####AAVtYXhpMQABMQAAAAE#8AAAAAAAAP####8AAAABABRDSW1wbGVtZW50YXRpb25Qcm90bwD#####AAdDdXJzZXVyAAAABQAAAAUAAAADAAAAAgAAAAMAAAAB#####wAAAAEAFENEcm9pdGVEaXJlY3Rpb25GaXhlAAAAAAQBAAAAABAAAAEAAAABAAAAAQE#8AAAAAAAAP####8AAAABAA9DUG9pbnRMaWVEcm9pdGUBAAAABAAAAP8AEAAAAMAIAAAAAAAAP#AAAAAAAAAAAAUAAEBdQAAAAAAAAAAABf####8AAAABAAtDSG9tb3RoZXRpZQAAAAAEAAAAAf####8AAAABAApDT3BlcmF0aW9uA#####8AAAABAA9DUmVzdWx0YXRWYWxldXIAAAACAAAACAEAAAAJAAAAAgAAAAkAAAAD#####wAAAAEAC0NQb2ludEltYWdlAAAAAAQBAAAAAA0AAk8xAMAQAAAAAAAAQBAAAAAAAAAAAAUAAAAABgAAAAcAAAAHAAAAAAQAAAABAAAACAMAAAAIAQAAAAE#8AAAAAAAAAAAAAkAAAACAAAACAEAAAAJAAAAAwAAAAkAAAACAAAACgAAAAAEAQAAAAANAAJJMQDAAAAAAAAAAEAIAAAAAAAAAAAFAAAAAAYAAAAJ#####wAAAAEACENTZWdtZW50AQAAAAQAAAD#ABAAAAEAAAABAAAAAQAAAAYAAAAGAQAAAAQAAAD#ARAAAmsxAMAAAAAAAAAAQAAAAAAAAAAAAAEAAQAAAAAAAAAAAAAAC#####8AAAACAA9DTWVzdXJlQWJzY2lzc2UBAAAABAABYQAAAAgAAAAKAAAADP####8AAAABAA9DVmFsZXVyQWZmaWNoZWUBAAAABAAAAP8AAAAAAAAAAADAGAAAAAAAAAAAAAAADA8AAf###wAAAAEAAAACAAAAAQAAAAAAAAAAAAAAAAIAAAANAAAAAwD#####AAJhYQABYQAAAAkAAAANAAAAAgD#####AQAAAAAQAAAAAAAAAAAAAABACAAAAAAAAAAABQABQFcAAAAAAABAWbhR64UeuAAAAAIA#####wEAAAAAEAAAAAAAAAAAAAAAQAgAAAAAAAAAAAUAAUBXAAAAAAAAQE3wo9cKPXH#####AAAAAQAJQ1JvdGF0aW9uAP####8AAAARAAAAAUBWgAAAAAAAAAAACgD#####AQAAAAAQAAAAAAAAAAAAAABACAAAAAAAAAAABQAAAAAQAAAAEgAAAA4A#####wAAABMAAAABQFaAAAAAAAAAAAAKAP####8BAAAAABAAAAAAAAAAAAAAAEAIAAAAAAAAAAAFAAAAABEAAAAU#####wAAAAEAEUNTeW1ldHJpZUNlbnRyYWxlAP####8AAAARAAAACgD#####AQAAAAAQAAAAAAAAAAAAAABACAAAAAAAAAAABQAAAAAQAAAAFgAAAAoA#####wEAAAABEAABQwAAAAAAAAAAAEAIAAAAAAAAAAAFAAAAABMAAAAWAAAACgD#####AQAAAAAQAAAAAAAAAAAAAABACAAAAAAAAAAABQAAAAAVAAAAFgAAAA8A#####wAAABAAAAAKAP####8BAAAAABAAAAAAAAAAAAAAAEAIAAAAAAAAAAAFAAAAABEAAAAaAAAACgD#####AQAAAAAQAAAAAAAAAAAAAABACAAAAAAAAAAABQAAAAATAAAAGgAAAAoA#####wEAAAABEAABRgAAAAAAAAAAAEAIAAAAAAAAAAAFAAAAABUAAAAaAAAADwD#####AAAAGAAAAAoA#####wEAAAAAEAAAAAAAAAAAAAAAQAgAAAAAAAAAAAUAAAAAEQAAAB4AAAAPAP####8AAAAdAAAACgD#####AQAAAAAQAAAAAAAAAAAAAABACAAAAAAAAAAABQAAAAAQAAAAIAAAAA8A#####wAAABUAAAAKAP####8BAAAAABAAAAAAAAAAAAAAAEAIAAAAAAAAAAAFAAAAABAAAAAiAAAADwD#####AAAAEwAAAAoA#####wEAAAAAEAAAAAAAAAAAAAAAQAgAAAAAAAAAAAUAAAAAEQAAACT#####AAAAAQAJQ1BvbHlnb25lAP####8Bf39#AAAAAQAAAAUAAAAcAAAAHQAAABAAAAAbAAAAHAAAABAA#####wF#f38AAAABAAAABQAAACEAAAAfAAAAGAAAAB0AAAAhAAAAEAD#####AX9#fwAAAAEAAAAFAAAAGAAAABkAAAAXAAAAEQAAABgAAAAQAP####8Bf39#AAAAAQAAAAUAAAARAAAAEwAAABUAAAAQAAAAEQAAABAA#####wF#f38AAAABAAAABQAAABMAAAAlAAAAIwAAABUAAAATAAAAEAD#####AX9#fwAAAAEAAAAFAAAAGAAAABEAAAAQAAAAHQAAABgAAAACAP####8BAAD#ARAAAUQAAAAAAAAAAABACAAAAAAAAAAABQABQFFAAAAAAABAUvhR64UeuAAAAAsA#####wEAAP8AEAAAAQAAAAEAAAAYAAAALAAAAAIA#####wEAAP8AEAABQQAAAAAAAAAAAEAIAAAAAAAAAAAFAAFATgAAAAAAAUB#vhR64UeuAAAAAgD#####AQAA#wAQAAFCAAAAAAAAAAAAQAgAAAAAAAAAAAUAAUBTQAAAAAAAQH#eFHrhR67#####AAAAAQAJQ0xvbmd1ZXVyAP####8AAAAuAAAALwAAAAQA#####wASTWVzdXJlIGRlIGxvbmd1ZXVyAAAABQAAAAIAAAACAAAAGAAAACz#####AAAAAQALQ01lZGlhdHJpY2UAAAAAMQEAAAAAEAAAAQAAAAEAAAAYAAAALP####8AAAABAAdDTWlsaWV1AAAAADEBAAAAABAAAAEAAAUAAAAAGAAAACz#####AAAAAgAJQ0NlcmNsZU9SAAAAADEBAAAAAAAAAQAAADMAAAABQDAAAAAAAAAB#####wAAAAEAEENJbnREcm9pdGVDZXJjbGUAAAAAMQAAADIAAAA0#####wAAAAEAEENQb2ludExpZUJpcG9pbnQAAAAAMQEAAAAAEAAAAQAABQABAAAANQAAABEBAAAAMQAAABgAAAAsAAAADQEAAAAxAQAA#wEAAAAAADYRAAAAAAABAAAAAQAAAAEAAAAAAAAAAAAAAAABAAAANwAAABQA#####wEAAP8AAAABAAAAGAAAAAgCAAAACQAAADcAAAAJAAAADwAAAAAVAP####8AAAAtAAAAOQAAABYA#####wH#AAAAEAAAAAAAAAAAAAAAQAgAAAAAAAAAAAUAAQAAADoAAAAWAP####8AAAD#ABAAAAAAAAAAAAAAAEAIAAAAAAAAAAAFAAIAAAA6#####wAAAAEACENWZWN0ZXVyAP####8B#wAAABAAAAEAAAABAAAAHQAAABAA#####wAAAAEADUNUcmFuc1BhclZlY3QA#####wAAAD0AAAAKAP####8B#wAAABAAAAAAAAAAAAAAAEAIAAAAAAAAAAAFAAAAADsAAAA+AAAACgD#####Af8AAAAQAAAAAAAAAAAAAABACAAAAAAAAAAABQAAAAA#AAAAPgAAAA8A#####wAAADsAAAAKAP####8B####ABAAAAAAAAAAAAAAAEAIAAAAAAAAAAAFAAAAAD8AAABBAAAAFwD#####Af8AAAAQAAABAAAAAQAAAB0AAAA7AAAAAAoA#####wH#AAABEAABRQAAAAAAAAAAAEAIAAAAAAAAAAAFAAAAADsAAAAgAAAACgD#####Af8AAAAQAAAAAAAAAAAAAABACAAAAAAAAAAABQAAAAA#AAAAGgAAAAoA#####wH#AAAAEAAAAAAAAAAAAAAAQAgAAAAAAAAAAAUAAAAAHQAAAEEAAAAPAP####8AAAA#AAAACgD#####Af8AAAAQAAAAAAAAAAAAAABACAAAAAAAAAAABQAAAAAQAAAARwAAABAA#####wF#f38AAAABAAAABQAAAEIAAAA7AAAAHQAAACEAAABCAAAAEAD#####AX9#fwAAAAEAAAAFAAAAOwAAAD8AAAAQAAAAHQAAADsAAAAQAP####8Bf39#AAAAAQAAAAUAAAA#AAAAQAAAABUAAAAQAAAAPwAAABAA#####wF#f38AAAABAAAABQAAADsAAABGAAAASAAAAD8AAAA7AAAAEAD#####AX9#fwAAAAEAAAAFAAAAHQAAAEQAAABFAAAAEAAAAB0AAAAPAP####8AAABAAAAACgD#####Af###wAQAAAAAAAAAAAAAABACAAAAAAAAAAABQAAAAA#AAAATgAAABAA#####wF#f38AAAABAAAABQAAAEAAAABPAAAAIwAAABUAAABAAAAABAD#####ABtNZXN1cmUgZCdhbmdsZSBub24gb3JpZW50w6kAAAACAAAAAwAAAAMAAABEAAAAHQAAABj#####AAAAAQAMQ0Jpc3NlY3RyaWNlAAAAAFEBAAAAABAAAAEAAAABAAAARAAAAB0AAAAYAAAABgAAAABRAQAAAAAQAAABAAAFAAFAbyWoZaxxqQAAAFL#####AAAAAQAXQ01lc3VyZUFuZ2xlR2VvbWV0cmlxdWUBAAAAUQAAAEQAAAAdAAAAGP####8AAAACABdDTWFycXVlQW5nbGVHZW9tZXRyaXF1ZQEAAABRAf8AAAAAAAEAAAABQEUGF39UkbsAAABEAAAAHQAAABgAAAANAQAAAFEB#wAAAEAIAAAAAAAAAAAAAAAAAAAAAAAAAFMPAAAAAAABAAAAAQAAAAEAAAAAAAAAAAAAAALCsAAAAABUAAAAAgD#####AAAAAAAQAAAAQAgAAAAAAAAAAAAAAAAAAAAABQAAQGqgAAAAAABAfB4UeuFHrgAAAAMA#####wAFbWluaTIAATAAAAABAAAAAAAAAAAAAAADAP####8ABW1heGkyAAExAAAAAT#wAAAAAAAAAAAABAD#####AAdDdXJzZXVyAAAABQAAAAUAAAADAAAAWAAAAFkAAABXAAAABQAAAABaAQAAAAAQAAABAAAAAQAAAFcBP#AAAAAAAAAAAAAGAQAAAFoA#wAAABAAAADACAAAAAAAAD#wAAAAAAAAAAAFAABAXUAAAAAAAAAAAFsAAAAHAAAAAFoAAABXAAAACAMAAAAJAAAAWAAAAAgBAAAACQAAAFgAAAAJAAAAWQAAAAoAAAAAWgEAAAAADQACTzEAwBAAAAAAAABAEAAAAAAAAAAABQAAAABcAAAAXQAAAAcAAAAAWgAAAFcAAAAIAwAAAAgBAAAAAT#wAAAAAAAAAAAACQAAAFgAAAAIAQAAAAkAAABZAAAACQAAAFgAAAAKAAAAAFoBAAAAAA0AAkkxAMAAAAAAAAAAQAgAAAAAAAAAAAUAAAAAXAAAAF8AAAALAQAAAFoA#wAAABAAAAEAAAABAAAAVwAAAFwAAAAGAQAAAFoA#wAAARAAAWsAwAAAAAAAAABAAAAAAAAAAAAAAQABAAAAAAAAAAAAAABhAAAADAEAAABaAAFiAAAAXgAAAGAAAABiAAAADQEAAABaAP8AAAAAAAAAAAAAAMAYAAAAAAAAAAAAAABiDwAB####AAAAAQAAAAIAAAABAAAAAAAAAAAAAAAAAgAAAGMAAAADAP####8AAmJiAAFiAAAACQAAAGMAAAACAP####8B#wAAABAAAAAAAAAAAAAAAEAIAAAAAAAAAAAFAAFAeyAAAAAAAEB7bhR64UeuAAAAAgD#####Af8AAAAQAAAAAAAAAAAAAABACAAAAAAAAAAABQABQH2AAAAAAABAe34UeuFHrgAAABcA#####wH#AAAAEAAAAQAAAAEAAAAdAAAAZgAAAAAYAP####8AAABoAAAACgD#####Af8AAAAQAAFHAAAAAAAAAAAAQAgAAAAAAAAAAAUAAAAARAAAAGkAAAAKAP####8B#wAAABAAAUkAAAAAAAAAAABACAAAAAAAAAAABQAAAAAYAAAAaQAAABMA#####wH#AAAAEAAAAAAAAAAAAAAAQAgAAAAAAAAAAAUAAAAAawAAAGr#####AAAAAQAJQ0Ryb2l0ZUFCAP####8B#wAAABAAAAEAAAABAAAAagAAAGv#####AAAAAQAWQ0Ryb2l0ZVBlcnBlbmRpY3VsYWlyZQD#####Af8AAAAQAAABAAAAAQAAAGwAAABtAAAABgD#####Af8AAAAQAAFIAAAAAAAAAAAAQAgAAAAAAAAAAAUAAT#z0usRwD3UAAAAbv####8AAAABAAlDQ2VyY2xlT0EA#####wH#AAAAAAABAAAAbwAAAGsAAAAEAP####8AG01lc3VyZSBkJ2FuZ2xlIG5vbiBvcmllbnTDqQAAAAIAAAADAAAAAwAAAGoAAABvAAAAawAAABkAAAAAcQEAAAAAEAAAAQAAAAEAAABqAAAAbwAAAGsAAAAGAAAAAHEBAAAAABAAAAEAAAUAAUBvJahlrHGpAAAAcgAAABoBAAAAcQAAAGoAAABvAAAAawAAABsBAAAAcQH#AAAAAAABAAAAAUBFBhd#VJG7AAAAagAAAG8AAABrAAAADQEAAABxAf8AAABACAAAAAAAAAAAAAAAAAAAAAAAAABzDwAAAAAAAQAAAAEAAAABAAAAAAAAAAAAAAACwrAAAAAAdAAAAA4A#####wAAAG######AAAAAQAMQ01vaW5zVW5haXJlAAAACAIAAAAJAAAAdAAAAAkAAABlAAAACgD#####Af8AAAAQAAAAAAAAAAAAAABACAAAAAAAAAAABQAAAABqAAAAdwAAABcA#####wH#AAAAEAAAAQAAAAEAAABmAAAAHQAAAAAYAP####8AAAB5AAAACgD#####AQAA#wAQAAAAAAAAAAAAAABACAAAAAAAAAAABQAAAAB4AAAAegAAAAoA#####wH###8AEAAAAAAAAAAAAAAAQAgAAAAAAAAAAAUAAAAAewAAAD4AAAAQAP####8Bf39#AAAAAQAAAAUAAAB7AAAAHQAAABAAAAB8AAAAewAAAA4A#####wAAABAAAAAIAgAAAAFAVoAAAAAAAAAAAAkAAABlAAAACgD#####AQAA#wAQAAAAAAAAAAAAAABACAAAAAAAAAAABQAAAAAVAAAAfgAAABgA#####wAAAEMAAAAKAP####8BAAD#ABAAAAAAAAAAAAAAAEAIAAAAAAAAAAAFAAAAAH8AAACAAAAAEAD#####AX9#fwAAAAEAAAAFAAAAEAAAAD8AAACBAAAAfwAAABAAAAASAP####8BAAD#ABAAAAEAAAABAAAAGQAAABf#####AAAAAQAPQ1N5bWV0cmllQXhpYWxlAP####8AAACDAAAACgD#####AQAA#wAQAAAAAAAAAAAAAABACAAAAAAAAAAABQAAAAB#AAAAhAAAAAoA#####wEAAP8AEAAAAAAAAAAAAAAAQAgAAAAAAAAAAAUAAAAAhQAAAIAAAAAQAP####8Bf39#AAAAAQAAAAUAAACFAAAAhgAAADsAAAAdAAAAhQAAABcA#####wEAAP8AEAAAAQAAAAEAAAAjAAAAFQAAAAAYAP####8AAACIAAAAFwD#####AQAA#wAQAAABAAAAAQAAACMAAAAlAAAAABgA#####wAAAIoAAAAKAP####8BAAD#ABAAAAAAAAAAAAAAAEAIAAAAAAAAAAAFAAAAAD8AAACLAAAACgD#####AQAA#wAQAAFMAAAAAAAAAAAAQAgAAAAAAAAAAAUAAAAAjAAAAGkAAAAKAP####8BAAD#ABAAAUoAAAAAAAAAAABACAAAAAAAAAAABQAAAABIAAAAaQAAABIA#####wEAAP8AEAAAAQAAAAEAAACNAAAAjgAAAAYA#####wEAAP8AEAABSwAAAAAAAAAAAEAIAAAAAAAAAAAFAAFABzQIU2olegAAAI8AAAAEAP####8AG01lc3VyZSBkJ2FuZ2xlIG5vbiBvcmllbnTDqQAAAAIAAAADAAAAAwAAAI4AAACQAAAAjQAAABkAAAAAkQEAAAAAEAAAAQAAAAEAAACOAAAAkAAAAI0AAAAGAAAAAJEBAAAAABAAAAEAAAUAAUBvJahlrHGpAAAAkgAAABoBAAAAkQAAAI4AAACQAAAAjQAAABsBAAAAkQEAAP8AAAABAAAAAUBFBhd#VJG7AAAAjgAAAJAAAACNAAAADQEAAACRAQAA#wBACAAAAAAAAAAAAAAAAAAAAAAAAACTDwAAAAAAAQAAAAEAAAABAAAAAAAAAAAAAAACwrAAAAAAlAAAAA4A#####wAAAJAAAAAIAgAAAAkAAACUAAAACQAAAGUAAAAKAP####8BAAD#ABAAAAAAAAAAAAAAAEAIAAAAAAAAAAAFAAAAAI4AAACXAAAACgD#####AQAA#wAQAAAAAAAAAAAAAABACAAAAAAAAAAABQAAAACYAAAAegAAAAoA#####wEAAP8AEAAAAAAAAAAAAAAAQAgAAAAAAAAAAAUAAAAAmQAAAIkAAAAQAP####8Bf39#AAAAAQAAAAUAAAA#AAAAOwAAAJoAAACZAAAAPwAAAA8A#####wAAAH8AAAAKAP####8BAAD#ABAAAAAAAAAAAAAAAEAIAAAAAAAAAAAFAAAAABAAAACcAAAADgD#####AAAAfwAAAAgCAAAAAUBWgAAAAAAAAAAACQAAAGUAAAAKAP####8BAAD#ABAAAAAAAAAAAAAAAEAIAAAAAAAAAAAFAAAAAJ0AAACeAAAACgD#####AP###wAQAAAAAAAAAAAAAABACAAAAAAAAAAABQAAAACfAAAAgAAAABAA#####wF#f38AAAABAAAABQAAAH8AAACBAAAAoAAAAJ8AAAB######wAAAAEAEENTdXJmYWNlUG9seWdvbmUA#####wH###8AAnM3AAAABAAAACcAAAAhAP####8B####AAJzOAAAAAQAAAArAAAAIQD#####Af###wACczkAAAAEAAAAKQAAACEA#####wH###8AA3MxMAAAAAQAAAAqAAAAIQD#####Af###wADczExAAAABAAAACgAAAAhAP####8B####AANzMTIAAAAEAAAAJgAAAAsA#####wAAAAAAEAAAAQAEc2VnMAABAAAAHAAAABsAAAALAP####8AAAAAABAAAAEABHNlZzEAAQAAABkAAAAXAAAACwD#####AAAAAAAQAAABAARzZWcyAAEAAAAYAAAAEQAAAAsA#####wAAAAAAEAAAAQAEc2VnMwABAAAAHwAAACEAAAALAP####8AAAAAABAAAAEABHNlZzQAAQAAABgAAAAdAAAACwD#####AAAAAAAQAAABAARzZWc1AAEAAAARAAAAEAAAAAsA#####wAAAAAAEAAAAQAEc2VnNgABAAAAEwAAABUAAAALAP####8AAAAAABAAAAEABHNlZzcAAQAAACUAAAAjAAAACwD#####AAAAAAAQAAABAARzZWc4AAEAAAAZAAAAGAAAAAsA#####wAAAAAAEAAAAQAEc2VnOQABAAAAFwAAABEAAAALAP####8AAAAAABAAAAEABXNlZzEwAAEAAAAdAAAAHAAAAAsA#####wAAAAAAEAAAAQAFc2VnMTEAAQAAABAAAAAbAAAACwD#####AAAAAAAQAAABAAVzZWcxMgABAAAAHwAAABgAAAALAP####8AAAAAABAAAAEABXNlZzEzAAEAAAATAAAAJQAAACEA#####wH###8AAnMxAAAABAAAAJsAAAALAP####8AAAAAABAAAAEABXNlZzE0AAEAAAARAAAAEwAAAAsA#####wEAAAAAEAAAAQAFc2VnMTUAAQAAAJoAAACZAAAACwD#####AQAAAAAQAAABAAVzZWcxNgABAAAAmgAAADsAAAALAP####8BAAAAABAAAAEABXNlZzE3AAEAAACZAAAAPwAAAAsA#####wEAAAAAEAAAAQAFc2VnMTgAAQAAADsAAAA#AAAAIQD#####Af###wACczUAAAAEAAAAhwAAAAsA#####wEAAAAAEAAAAQAFc2VnMTkAAQAAAIYAAAA7AAAAIQD#####Af###wACczQAAAAEAAAASgAAACEA#####wH###8AAnMzAAAABAAAAIIAAAAhAP####8B####AAJzMgAAAAQAAAChAAAACwD#####AQAAAAAQAAABAAVzZWcyMAABAAAAPwAAAIEAAAALAP####8BAAAAABAAAAEABXNlZzIxAAEAAACBAAAAoAAAAAsA#####wEAAAAAEAAAAQAFc2VnMjIAAQAAAIUAAACGAAAACwD#####AQAAAAAQAAABAAVzZWcyMwABAAAAOwAAAB0AAAALAP####8BAAAAABAAAAEABXNlZzI0AAEAAAA#AAAAEAAAAAsA#####wEAAAAAEAAAAQAFc2VnMjUAAQAAAH8AAACBAAAACwD#####AQAAAAAQAAABAAVzZWcyNgABAAAAnwAAAKAAAAALAP####8BAAAAABAAAAEABXNlZzI3AAEAAACFAAAAHQAAAAsA#####wAAAAAAEAAAAQAFc2VnMjgAAQAAAB0AAAAQAAAACwD#####AQAAAAAQAAABAAVzZWcyOQABAAAAEAAAAH8AAAALAP####8BAAAAABAAAAEABXNlZzMwAAEAAAB#AAAAnwAAAAsA#####wEAAAAAEAAAAQAFc2VnMzEAAQAAAB0AAAB7AAAACwD#####AQAAAAAQAAABAAVzZWczMgABAAAAEAAAAHwAAAALAP####8BAAAAABAAAAEABXNlZzMzAAEAAAB7AAAAfAAAAAsA#####wAAAAAAEAAAAQAFc2VnMzQAAQAAABAAAAAVAAAACwD#####AAAAAAAQAAABAAVzZWczNQABAAAAFQAAACMAAAALAP####8AAAAAABAAAAEABXNlZzM2AAEAAAAhAAAAHQAAACEA#####wH###8AAnM2AAAABAAAAH0AAAAw##########8='
      case 'cu2': return 'TWF0aEdyYXBoSmF2YTEuMAAAABI+TMzNAAJmcv###wEA#wEAAAAAAAAAAAM7AAACdwAAAQEAAAAAAAAAAQAAABz#####AAAAAQAKQ0NhbGNDb25zdAD#####AAJwaQAWMy4xNDE1OTI2NTM1ODk3OTMyMzg0Nv####8AAAABAApDQ29uc3RhbnRlQAkh+1RELRj#####AAAAAQAKQ1BvaW50QmFzZQD#####AQAAAAAQAAAAAAAAAAAAAABACAAAAAAAAAUAAUBbgAAAAAAAQFo4UeuFHrgAAAACAP####8BAAAAABAAAAAAAAAAAAAAAEAIAAAAAAAABQABQFuAAAAAAABAT3Cj1wo9cP####8AAAABAAlDUm90YXRpb24A#####wAAAAIAAAABQFaAAAAAAAD#####AAAAAQALQ1BvaW50SW1hZ2UA#####wEAAAAAEAAAAAAAAAAAAAAAQAgAAAAAAAAFAAAAAAEAAAADAAAAAwD#####AAAABAAAAAFAVoAAAAAAAAAAAAQA#####wEAAAAAEAAAAAAAAAAAAAAAQAgAAAAAAAAFAAAAAAIAAAAF#####wAAAAEAEUNTeW1ldHJpZUNlbnRyYWxlAP####8AAAACAAAABAD#####AQAAAAAQAAAAAAAAAAAAAABACAAAAAAAAAUAAAAAAQAAAAcAAAAEAP####8BAAAAABAAAAAAAAAAAAAAAEAIAAAAAAAABQAAAAAEAAAABwAAAAQA#####wEAAAAAEAAAAAAAAAAAAAAAQAgAAAAAAAAFAAAAAAYAAAAHAAAABQD#####AAAAAQAAAAQA#####wEAAAAAEAAAAAAAAAAAAAAAQAgAAAAAAAAFAAAAAAIAAAALAAAABAD#####AQAAAAAQAAAAAAAAAAAAAABACAAAAAAAAAUAAAAABAAAAAsAAAAEAP####8BAAAAABAAAAAAAAAAAAAAAEAIAAAAAAAABQAAAAAGAAAACwAAAAUA#####wAAAAkAAAAEAP####8BAAAAABAAAAAAAAAAAAAAAEAIAAAAAAAABQAAAAACAAAADwAAAAUA#####wAAAA4AAAAEAP####8BAAAAABAAAAAAAAAAAAAAAEAIAAAAAAAABQAAAAABAAAAEQAAAAUA#####wAAAAYAAAAEAP####8BAAAAABAAAAAAAAAAAAAAAEAIAAAAAAAABQAAAAABAAAAEwAAAAUA#####wAAAAQAAAAEAP####8BAAAAABAAAAAAAAAAAAAAAEAIAAAAAAAABQAAAAACAAAAFf####8AAAABAAlDUG9seWdvbmUA#####wAAAAAAAQAAAAUAAAANAAAADgAAAAEAAAAMAAAADQAAAAYA#####wAAAAAAAQAAAAUAAAASAAAAEAAAAAkAAAAOAAAAEgAAAAYA#####wAAAAAAAQAAAAUAAAAJAAAACgAAAAgAAAACAAAACQAAAAYA#####wAAAAAAAQAAAAUAAAACAAAABAAAAAYAAAABAAAAAgAAAAYA#####wEAAAAAAQAAAAUAAAAEAAAAFgAAABQAAAAGAAAABP###############w=='
      case 'cu3': return 'TWF0aEdyYXBoSmF2YTEuMAAAABI+TMzNAAJmcv###wEA#wEAAAAAAAAAAAM7AAACdwAAAQEAAAAAAAAAAQAAACH#####AAAAAQAKQ0NhbGNDb25zdAD#####AAJwaQAWMy4xNDE1OTI2NTM1ODk3OTMyMzg0Nv####8AAAABAApDQ29uc3RhbnRlQAkh+1RELRj#####AAAAAQAKQ1BvaW50QmFzZQD#####AQAAAAAQAAAAAAAAAAAAAABACAAAAAAAAAUAAUBWwAAAAAAAQFc4UeuFHrgAAAACAP####8BAAAAABAAAAAAAAAAAAAAAEAIAAAAAAAABQABQFbAAAAAAABASPCj1wo9cP####8AAAABAAlDUm90YXRpb24A#####wAAAAIAAAABQFaAAAAAAAD#####AAAAAQALQ1BvaW50SW1hZ2UA#####wEAAAAAEAAAAAAAAAAAAAAAQAgAAAAAAAAFAAAAAAEAAAADAAAAAwD#####AAAABAAAAAFAVoAAAAAAAAAAAAQA#####wEAAAAAEAAAAAAAAAAAAAAAQAgAAAAAAAAFAAAAAAIAAAAF#####wAAAAEAEUNTeW1ldHJpZUNlbnRyYWxlAP####8AAAACAAAABAD#####AQAAAAAQAAAAAAAAAAAAAABACAAAAAAAAAUAAAAAAQAAAAcAAAAEAP####8BAAAAABAAAAAAAAAAAAAAAEAIAAAAAAAABQAAAAAEAAAABwAAAAQA#####wEAAAAAEAAAAAAAAAAAAAAAQAgAAAAAAAAFAAAAAAYAAAAHAAAABQD#####AAAAAQAAAAQA#####wEAAAAAEAAAAAAAAAAAAAAAQAgAAAAAAAAFAAAAAAIAAAALAAAABAD#####AQAAAAAQAAAAAAAAAAAAAABACAAAAAAAAAUAAAAABAAAAAsAAAAEAP####8BAAAAABAAAAAAAAAAAAAAAEAIAAAAAAAABQAAAAAGAAAACwAAAAUA#####wAAAAkAAAAEAP####8BAAAAABAAAAAAAAAAAAAAAEAIAAAAAAAABQAAAAACAAAADwAAAAUA#####wAAAA4AAAAEAP####8BAAAAABAAAAAAAAAAAAAAAEAIAAAAAAAABQAAAAABAAAAEQAAAAUA#####wAAAAYAAAAEAP####8BAAAAABAAAAAAAAAAAAAAAEAIAAAAAAAABQAAAAABAAAAEwAAAAUA#####wAAAAQAAAAEAP####8BAAAAABAAAAAAAAAAAAAAAEAIAAAAAAAABQAAAAACAAAAFf####8AAAABAAlDUG9seWdvbmUA#####wAAAAAAAQAAAAUAAAANAAAADgAAAAEAAAAMAAAADQAAAAYA#####wAAAAAAAQAAAAUAAAASAAAAEAAAAAkAAAAOAAAAEgAAAAYA#####wEAAAAAAQAAAAUAAAAJAAAACgAAAAgAAAACAAAACQAAAAYA#####wAAAAAAAQAAAAUAAAACAAAABAAAAAYAAAABAAAAAgAAAAYA#####wAAAAAAAQAAAAUAAAAEAAAAFgAAABQAAAAGAAAABAAAAAUA#####wAAABQAAAAEAP####8BAAAAABAAAAAAAAAAAAAAAEAIAAAAAAAABQAAAAAWAAAAHAAAAAQA#####wEAAAAAEAAAAAAAAAAAAAAAQAgAAAAAAAAFAAAAAAQAAAATAAAABgD#####AAAAAAABAAAABQAAAAYAAAAUAAAAHQAAAB4AAAAGAAAABgD#####AAAAAAABAAAABQAAAAkAAAACAAAAAQAAAA4AAAAJ################'
      case 'pa1': return 'TWF0aEdyYXBoSmF2YTEuMAAAABM+TMzNAAJmcv###wEA#wEAAAAAAAAAAANLAAAChwAAAQEAAAAAAAAAAQAAAOH#####AAAAAQAKQ0NhbGNDb25zdAD#####AAJwaQAWMy4xNDE1OTI2NTM1ODk3OTMyMzg0Nv####8AAAABAApDQ29uc3RhbnRlQAkh+1RELRj#####AAAAAQAKQ1BvaW50QmFzZQD#####AQAAAAEQAAFDAAAAAAAAAAAAQAgAAAAAAAAAAAUAAUBP9xxxxxxwQFCmis8TV57#####AAAAAQARQ1N5bWV0cmllQ2VudHJhbGUA#####wAAAAEAAAACAP####8BAAAAABAAAAAAAAAAAAAAAEAIAAAAAAAAAAAFAAFAVvuOOOOOPEBQporPE1ee#####wAAAAEACUNEcm9pdGVBQgD#####AQAAAAAQAAABAAAAAQAAAAEAAAAD#####wAAAAEAFkNEcm9pdGVQZXJwZW5kaWN1bGFpcmUA#####wEAAAAAEAAAAQAAAAEAAAADAAAABP####8AAAABAA9DUG9pbnRMaWVEcm9pdGUA#####wEAAAAAEAAAAAAAAAAAAAAAQAgAAAAAAAAAAAUAAT#2xOxOxOxRAAAABQAAAAUA#####wEAAAAAEAAAAQAAAAEAAAAGAAAABQAAAAUA#####wEAAAAAEAAAAQAAAAEAAAABAAAABP####8AAAABABBDSW50RHJvaXRlRHJvaXRlAP####8BAAAAABAAAAAAAAAAAAAAAEAIAAAAAAAAAAAFAAAAAAgAAAAHAAAABgD#####AQAAAAAQAAAAAAAAAAAAAABACAAAAAAAAAAABQABQAdidididicAAAAEAAAABQD#####AQAAAAAQAAABAAAAAQAAAAoAAAAEAAAABwD#####AQAAAAAQAAFLAAAAAAAAAAAAQAgAAAAAAAAAAAUAAAAACwAAAAf#####AAAAAQAJQ0NlcmNsZU9BAP####8BAAAAAAAAAQAAAAYAAAAM#####wAAAAEAEENJbnREcm9pdGVDZXJjbGUA#####wAAAAUAAAAN#####wAAAAEAEENQb2ludExpZUJpcG9pbnQA#####wEAAAAAEAAAAAAAAAAAAAAAQAgAAAAAAAAAAAUAAgAAAA4AAAAKAP####8BAAAAABAAAAAAAAAAAAAAAEAIAAAAAAAAAAAFAAEAAAAOAAAABQD#####AQAAAAAQAAABAAAAAQAAABAAAAAFAAAABwD#####AQAAAAAQAAAAAAAAAAAAAABACAAAAAAAAAAABQAAAAAIAAAAEQAAAAgA#####wEAAAAAAAABAAAACQAAABIAAAAJAP####8AAAAHAAAAEwAAAAoA#####wEAAAAAEAAAAAAAAAAAAAAAQAgAAAAAAAAAAAUAAgAAABQAAAAKAP####8BAAAAABAAAAAAAAAAAAAAAEAIAAAAAAAAAAAFAAEAAAAUAAAABQD#####AQAAAAAQAAABAAAAAQAAABYAAAAHAAAABwD#####AQAAAAAQAAAAAAAAAAAAAABACAAAAAAAAAAABQAAAAAXAAAABAAAAAgA#####wEAAAAAAAABAAAAAQAAABgAAAAJAP####8AAAAIAAAAGQAAAAoA#####wEAAAAAEAAAAAAAAAAAAAAAQAgAAAAAAAAAAAUAAgAAABoAAAAKAP####8BAAAAABAAAAAAAAAAAAAAAEAIAAAAAAAAAAAFAAEAAAAaAAAACAD#####AQAAAAAAAAEAAAADAAAACgAAAAkA#####wAAAAUAAAAdAAAACgD#####AQAAAAAQAAAAAAAAAAAAAABACAAAAAAAAAAABQABAAAAHgAAAAoA#####wEAAAAAEAAAAAAAAAAAAAAAQAgAAAAAAAAAAAUAAgAAAB7#####AAAAAQAJQ1BvbHlnb25lAP####8BAAAAAAAAAQAAAAUAAAAYAAAAAQAAAAkAAAAWAAAAGAAAAAsA#####wEAAAAAAAABAAAABQAAAAEAAAADAAAABgAAAAkAAAABAAAACwD#####AQAAAAAAAAEAAAAFAAAAAwAAAAoAAAAMAAAABgAAAAMAAAALAP####8BAAAAAAAAAQAAAAUAAAAJAAAABgAAABAAAAASAAAACQAAAAsA#####wEAAAAAAAABAAAABQAAABsAAAAgAAAAAwAAAAEAAAAb#####wAAAAEACENWZWN0ZXVyAP####8BAAAAABAAAAEAAAABAAAAAQAAAAMA#####wAAAAEADUNUcmFuc1BhclZlY3QA#####wAAACb#####AAAAAQALQ1BvaW50SW1hZ2UA#####wEAAAAAEAAAAAAAAAAAAAAAQAgAAAAAAAAAAAUAAAAACgAAACcAAAAOAP####8BAAAAABAAAUwAAAAAAAAAAABACAAAAAAAAAAABQAAAAAMAAAAJwAAAAsA#####wEAAAAAAAABAAAABQAAAAoAAAAoAAAAKQAAAAwAAAAKAAAAAgD#####AQAA#wEQAAFEAAAAAAAAAAAAQAgAAAAAAAAAAAUAAUBTRcxrtapEQFMXePvUAYQAAAACAP####8BAAD#ABAAAUEAAAAAAAAAAABACAAAAAAAAAAABQABwHFYAAAAAABAeA1wo9cKPgAAAAIA#####wEAAP8AEAABQgAAAAAAAAAAAEAIAAAAAAAAAAAFAAHASD56HxlpEEBw#39uXUw8#####wAAAAEACUNMb25ndWV1cgD#####AAAALAAAAC3#####AAAAAQAIQ1NlZ21lbnQA#####wEAAP8AEAAAAQAAAAEAAAABAAAAK#####8AAAABABRDSW1wbGVtZW50YXRpb25Qcm90bwD#####ABJNZXN1cmUgZGUgbG9uZ3VldXIAAAAFAAAAAgAAAAIAAAABAAAAK#####8AAAABAAtDTWVkaWF0cmljZQAAAAAwAQAAAAAQAAABAAAAAQAAAAEAAAAr#####wAAAAEAB0NNaWxpZXUAAAAAMAEAAAAAEAAAAQAABQAAAAABAAAAK#####8AAAACAAlDQ2VyY2xlT1IAAAAAMAEAAAAAAAABAAAAMgAAAAFAMAAAAAAAAAEAAAAJAAAAADAAAAAxAAAAMwAAAAoAAAAAMAEAAAAAEAAAAQAABQABAAAANAAAAA8BAAAAMAAAAAEAAAAr#####wAAAAEAD0NWYWxldXJBZmZpY2hlZQEAAAAwAQAA#wEAAAAAADURAAAAAAABAAAAAQAAAAEAAAAAAAAAAAAAAAABAAAANgAAAAIA#####wAAAAAAEAAAAEAIAAAAAAAAAAAAAAAAAAAAAAUAAEBjsAAAAAAAQH79cKPXCj7#####AAAAAQAHQ0NhbGN1bAD#####AAVtaW5pMQABMAAAAAEAAAAAAAAAAAAAABYA#####wAFbWF4aTEAATEAAAABP#AAAAAAAAAAAAARAP####8AB0N1cnNldXIAAAAFAAAABQAAAAMAAAA5AAAAOgAAADj#####AAAAAQAUQ0Ryb2l0ZURpcmVjdGlvbkZpeGUAAAAAOwEAAAAAEAAAAQAAAAEAAAA4AT#wAAAAAAAAAAAABgEAAAA7AAAA#wAQAAAAwAgAAAAAAAA#8AAAAAAAAAAABQAAQF1AAAAAAAAAAAA8#####wAAAAEAC0NIb21vdGhldGllAAAAADsAAAA4#####wAAAAEACkNPcGVyYXRpb24D#####wAAAAEAD0NSZXN1bHRhdFZhbGV1cgAAADkAAAAZAQAAABoAAAA5AAAAGgAAADoAAAAOAAAAADsBAAAAAA0AAk8xAMAQAAAAAAAAQBAAAAAAAAAAAAUAAAAAPQAAAD4AAAAYAAAAADsAAAA4AAAAGQMAAAAZAQAAAAE#8AAAAAAAAAAAABoAAAA5AAAAGQEAAAAaAAAAOgAAABoAAAA5AAAADgAAAAA7AQAAAAANAAJJMQDAAAAAAAAAAEAIAAAAAAAAAAAFAAAAAD0AAABAAAAAEAEAAAA7AAAA#wAQAAABAAAAAQAAADgAAAA9AAAABgEAAAA7AAAA#wEQAAJrMQDAAAAAAAAAAEAAAAAAAAAAAAABAAEAAAAAAAAAAAAAAEL#####AAAAAgAPQ01lc3VyZUFic2Npc3NlAQAAADsAAWEAAAA#AAAAQQAAAEMAAAAVAQAAADsAAAD#AAAAAAAAAAAAwBgAAAAAAAAAAAAAAEMPAAH###8AAAABAAAAAgAAAAEAAAAAAAAAAAAAAAACAAAARAAAAAIA#####wAAAAAAEAAAAEAIAAAAAAAAAAAAAAAAAAAAAAUAAEB3GAAAAAAAQH7dcKPXCj4AAAAWAP####8ABW1pbmkyAAEwAAAAAQAAAAAAAAAAAAAAFgD#####AAVtYXhpMgABMQAAAAE#8AAAAAAAAAAAABEA#####wAHQ3Vyc2V1cgAAAAUAAAAFAAAAAwAAAEcAAABIAAAARgAAABcAAAAASQEAAAAAEAAAAQAAAAEAAABGAT#wAAAAAAAAAAAABgEAAABJAP8AAAAQAAAAwAgAAAAAAAA#8AAAAAAAAAAABQAAQF1AAAAAAAAAAABKAAAAGAAAAABJAAAARgAAABkDAAAAGgAAAEcAAAAZAQAAABoAAABHAAAAGgAAAEgAAAAOAAAAAEkBAAAAAA0AAk8xAMAQAAAAAAAAQBAAAAAAAAAAAAUAAAAASwAAAEwAAAAYAAAAAEkAAABGAAAAGQMAAAAZAQAAAAE#8AAAAAAAAAAAABoAAABHAAAAGQEAAAAaAAAASAAAABoAAABHAAAADgAAAABJAQAAAAANAAJJMQDAAAAAAAAAAEAIAAAAAAAAAAAFAAAAAEsAAABOAAAAEAEAAABJAP8AAAAQAAABAAAAAQAAAEYAAABLAAAABgEAAABJAP8AAAEQAAFrAMAAAAAAAAAAQAAAAAAAAAAAAAEAAQAAAAAAAAAAAAAAUAAAABsBAAAASQABYgAAAE0AAABPAAAAUQAAABUBAAAASQD#AAAAAAAAAAAAAADAGAAAAAAAAAAAAAAAUQ8AAf###wAAAAEAAAACAAAAAQAAAAAAAAAAAAAAAAIAAABSAAAAFgD#####AAJhYQABYQAAABoAAABEAAAAFgD#####AAJiYgABYgAAABoAAABSAAAAFAD#####Af8AAAAAAAEAAAABAAAAGQIAAAAaAAAANgAAABoAAABUAAAAAAkA#####wAAAC8AAABWAAAACgD#####Af8AAAAQAAAAAAAAAAAAAABACAAAAAAAAAAABQABAAAAVwAAAAoA#####wD#AAAAEAAAAAAAAAAAAAAAQAgAAAAAAAAAAAUAAgAAAFcAAAAMAP####8BAAD#ABAAAAEAAAABAAAACQAAAFgAAAAADQD#####AAAAWgAAAA4A#####wH#AAAAEAAAAAAAAAAAAAAAQAgAAAAAAAAAAAUAAAAAFgAAAFsAAAAOAP####8B#wAAABAAAAAAAAAAAAAAAEAIAAAAAAAAAAAFAAAAAAYAAABbAAAADgD#####Af8AAAAQAAAAAAAAAAAAAABACAAAAAAAAAAABQAAAAAMAAAAWwAAAA4A#####wH#AAAAEAAAAAAAAAAAAAAAQAgAAAAAAAAAAAUAAAAAKQAAAFsAAAAMAP####8BAAD#ABAAAAEAAAABAAAAXAAAABYAAAAACwD#####Af8AAAAAAAEAAAAFAAAAXAAAABYAAAAJAAAAWAAAAFwAAAALAP####8B#wAAAAAAAQAAAAUAAAAJAAAABgAAAF0AAABYAAAACQAAAAsA#####wH#AAAAAAABAAAABQAAAAYAAAAMAAAAXgAAAF0AAAAGAAAACwD#####Af8AAAAAAAEAAAAFAAAADAAAACkAAABfAAAAXgAAAAwAAAAEAP####8BAAD#ABAAAAEAAAABAAAACQAAAFgAAAAEAP####8BAAD#ABAAAAEAAAABAAAAAQAAAFj#####AAAAAQAQQ0Ryb2l0ZVBhcmFsbGVsZQD#####AQAA#wAQAAABAAAAAQAAABsAAABmAAAABwD#####Af8AAAAQAAAAAAAAAAAAAABACAAAAAAAAAAABQAAAABnAAAAZQAAAAwA#####wEAAP8AEAAAAQAAAAEAAABYAAAAXQAAAAANAP####8AAABpAAAADgD#####Af8AAAAQAAAAAAAAAAAAAABACAAAAAAAAAAABQAAAABoAAAAagAAAAwA#####wEAAP8AEAAAAQAAAAEAAABoAAAAWAAAAAANAP####8AAABsAAAADgD#####Af8AAAAQAAAAAAAAAAAAAABACAAAAAAAAAAABQAAAAAJAAAAbQAAAA4A#####wH#AAAAEAAAAAAAAAAAAAAAQAgAAAAAAAAAAAUAAAAABgAAAG0AAAALAP####8B#wAAAAAAAQAAAAUAAAAJAAAAbgAAAG8AAAAGAAAACQAAAAsA#####wH#AAAAAAABAAAABQAAAFgAAABoAAAAawAAAF0AAABY#####wAAAAEACUNSb3RhdGlvbgD#####AAAACf####8AAAABAAxDTW9pbnNVbmFpcmUAAAAZAgAAAAFAVoAAAAAAAAAAABoAAABVAAAADgD#####AQAA#wAQAAAAAAAAAAAAAABACAAAAAAAAAAABQAAAAAWAAAAcgAAAA4A#####wEAAP8AEAAAAAAAAAAAAAAAQAgAAAAAAAAAAAUAAAAAcwAAAFsAAAALAP####8BAAAAAAAAAQAAAAUAAAAJAAAAcwAAAHQAAABYAAAACQAAAB0A#####wAAAAYAAAAZAgAAAAFAVoAAAAAAAAAAABoAAABVAAAADgD#####AQAA#wAQAAAAAAAAAAAAAABACAAAAAAAAAAABQAAAAAMAAAAdgAAAA4A#####wEAAP8AEAAAAAAAAAAAAAAAQAgAAAAAAAAAAAUAAAAAdwAAAFsAAAALAP####8BAAAAAAAAAQAAAAUAAAAGAAAAdwAAAHgAAABdAAAABgAAAB0A#####wAAAAkAAAAeAAAAAUBWgAAAAAAAAAAADgD#####AQAAAAAQAAAAAAAAAAAAAABACAAAAAAAAAAACQAAAAAWAAAAegAAAAIA#####wEAAAAAEAABRQAAAAAAAAAAAEAIAAAAAAAAAAAJAAFAcZ86N18xsEBlQ7x96gDEAAAADAD#####AQAAAAAQAAABAAAAAQAAAG4AAAB8AAAAAA0A#####wAAAH0AAAAOAP####8BAAAAABAAAUcAAAAAAAAAAABACAAAAAAAAAAACQAAAAB7AAAAfgAAABIA#####wEAAAAAEAAAAQAAAAEAAAB8AAAAfwAAAAYA#####wEAAAAAEAABRgAAAAAAAAAAAEAIAAAAAAAAAAAJAAE#9fDHTMcDzwAAAIAAAAARAP####8AG01lc3VyZSBkJ2FuZ2xlIG5vbiBvcmllbnTDqQAAAAIAAAADAAAAAwAAAHwAAACBAAAAf#####8AAAABAAxDQmlzc2VjdHJpY2UAAAAAggEAAAAAEAAAAQAAAAEAAAB8AAAAgQAAAH8AAAAGAAAAAIIBAAAAABAAAAEAAAUAAUBvJahlrHGpAAAAg#####8AAAABABdDTWVzdXJlQW5nbGVHZW9tZXRyaXF1ZQEAAACCAAAAfAAAAIEAAAB######wAAAAIAF0NNYXJxdWVBbmdsZUdlb21ldHJpcXVlAQAAAIIBAAAAAAAAAQAAAAFARQYXf1SRuwAAAHwAAACBAAAAfwAAABUBAAAAggEAAAAAQAgAAAAAAAAAAAAAAAAAAAAAAAAAhA8AAAAAAAEAAAABAAAAAQAAAAAAAAAAAAAAAsKwAAAAAIUAAAAdAP####8AAACBAAAAHgAAABkCAAAAGgAAAIUAAAAaAAAAVQAAAA4A#####wEAAAAAEAAAAAAAAAAAAAAAQAgAAAAAAAAAAAkAAAAAfAAAAIgAAAAMAP####8BAAAAABAAAAEAAAABAAAAfAAAAG4AAAAADQD#####AAAAigAAAA4A#####wEAAP8AEAAAAAAAAAAAAAAAQAgAAAAAAAAAAAUAAAAAiQAAAIsAAAAOAP####8BAAD#ABAAAAAAAAAAAAAAAEAIAAAAAAAAAAAFAAAAAIwAAABqAAAACwD#####AQAAAAAAAAEAAAAFAAAABgAAAI0AAACMAAAACQAAAAYAAAAOAP####8BAAD#ABAAAAAAAAAAAAAAAEAIAAAAAAAAAAAFAAAAAHsAAABbAAAADgD#####AQAA#wAQAAFKAAAAAAAAAAAAQAgAAAAAAAAAAAUAAAAAjwAAAH4AAAAOAP####8BAAD#ABAAAUgAAAAAAAAAAABACAAAAAAAAAAABQAAAABoAAAAfgAAABIA#####wEAAP8AEAAAAQAAAAEAAACQAAAAkQAAAAYA#####wEAAP8AEAABSQAAAAAAAAAAAEAIAAAAAAAAAAAFAAFAEb#KMhw7sAAAAJIAAAARAP####8AG01lc3VyZSBkJ2FuZ2xlIG5vbiBvcmllbnTDqQAAAAIAAAADAAAAAwAAAJEAAACTAAAAkAAAAB8AAAAAlAEAAAAAEAAAAQAAAAEAAACRAAAAkwAAAJAAAAAGAAAAAJQBAAAAABAAAAEAAAUAAUBvJahlrHGpAAAAlQAAACABAAAAlAAAAJEAAACTAAAAkAAAACEBAAAAlAEAAP8AAAABAAAAAUBFBhd#VJG7AAAAkQAAAJMAAACQAAAAFQEAAACUAQAA#wBACAAAAAAAAAAAAAAAAAAAAAAAAACWDwAAAAAAAQAAAAEAAAABAAAAAAAAAAAAAAACwrAAAAAAlwAAAB0A#####wAAAJMAAAAZAgAAABoAAACXAAAAGgAAAFUAAAAOAP####8BAAD#ABAAAAAAAAAAAAAAAEAIAAAAAAAAAAAFAAAAAJEAAACaAAAADgD#####AQAA#wAQAAAAAAAAAAAAAABACAAAAAAAAAAABQAAAACbAAAAiwAAAA4A#####wEAAP8AEAAAAAAAAAAAAAAAQAgAAAAAAAAAAAUAAAAAnAAAAGoAAAALAP####8BAAAAAAAAAQAAAAUAAACcAAAAnQAAAF0AAABYAAAAnP####8AAAABAA1DRGVtaURyb2l0ZU9BAP####8BAAD#AA0AAAEAAAABAAAABgAAAHcAAAARAP####8AEk1lc3VyZSBkZSBsb25ndWV1cgAAAAUAAAACAAAAAgAAAAwAAAApAAAAEgAAAACgAQAAAAAQAAABAAAAAQAAAAwAAAApAAAAEwAAAACgAQAAAAAQAAABAAAFAAAAAAwAAAApAAAAFAAAAACgAQAAAAAAAAEAAACiAAAAAUAwAAAAAAAAAQAAAAkAAAAAoAAAAKEAAACjAAAACgAAAACgAQAAAAAQAAABAAAFAAEAAACkAAAADwEAAACgAAAADAAAACkAAAAVAQAAAKABAAD#AQAAAAAApREAAAAAAAEAAAABAAAAAQAAAAAAAAAAAAAAAAEAAACmAAAAFAD#####AQAA#wAAAAEAAAB3AAAAGgAAAKYAAAAACQD#####AAAAnwAAAKgAAAAKAP####8BAAD#ABAAAAAAAAAAAAAAAEAIAAAAAAAAAAAFAAEAAACpAAAACgD#####AQAA#wAQAAAAAAAAAAAAAABACAAAAAAAAAAABQACAAAAqQAAAB0A#####wAAAHcAAAAZAgAAAAFAVoAAAAAAAAAAABoAAABVAAAADgD#####AQAA#wAQAAAAAAAAAAAAAABACAAAAAAAAAAABQAAAACqAAAArAAAAA4A#####wEAAP8AEAAAAAAAAAAAAAAAQAgAAAAAAAAAAAUAAAAArQAAAFsAAAALAP####8BAAAAAAAAAQAAAAUAAAB3AAAAeAAAAK4AAACtAAAAd#####8AAAABABBDU3VyZmFjZVBvbHlnb25lAP####8B####AAJzMQAAAAQAAAAhAAAAIwD#####Af###wACczMAAAAEAAAAIgAAACMA#####wH###8AAnM1AAAABAAAACMAAAAjAP####8B####AAJzNwAAAAQAAAAqAAAAIwD#####Af###wACczkAAAAEAAAAJQAAACMA#####wH###8AA3MxMQAAAAQAAAAkAAAAEAD#####AAAAAAAQAAABAARzZWcyAAEAAAAbAAAAIAAAABAA#####wAAAAAAEAAAAQAFc2VnMTEAAQAAABsAAAABAAAAEAD#####AAAAAAAQAAABAAVzZWcxNQABAAAAIAAAAAMAAAAQAP####8AAAAAABAAAAEABHNlZzAAAQAAAAEAAAADAAAAEAD#####AAAAAAAQAAABAAVzZWcxMgABAAAAGAAAAAEAAAAQAP####8AAAAAABAAAAEABXNlZzE2AAEAAAADAAAACgAAABAA#####wAAAAAAEAAAAQAEc2VnMwABAAAACgAAACgAAAAQAP####8AAAAAABAAAAEABXNlZzMzAAEAAAAYAAAAFgAAABAA#####wAAAAAAEAAAAQAFc2VnMjcAAQAAAAEAAAAJAAAAEAD#####AAAAAAAQAAABAAVzZWcyOQABAAAAAwAAAAYAAAAQAP####8AAAAAABAAAAEABXNlZzMxAAEAAAAKAAAADAAAABAA#####wAAAAAAEAAAAQAFc2VnMzQAAQAAACgAAAApAAAAEAD#####AQAAAAAQAAABAAVzZWcxOAABAAAAFgAAAAkAAAAQAP####8BAAAAABAAAAEABXNlZzIzAAEAAAAGAAAADAAAABAA#####wEAAAAAEAAAAQAEc2VnNwABAAAADAAAACkAAAAQAP####8AAAAAABAAAAEABXNlZzE5AAEAAAAJAAAAEgAAABAA#####wAAAAAAEAAAAQAFc2VnMjQAAQAAAAYAAAAQAAAAEAD#####AAAAAAAQAAABAARzZWc2AAEAAAASAAAAEAAAACMA#####wH###8AA3MxMAAAAAQAAACeAAAAEAD#####AQAAAAAQAAABAAVzZWcxNwABAAAAXQAAAJ0AAAAQAP####8BAAAAABAAAAEABHNlZzQAAQAAAJwAAACdAAAAEAD#####AQAAAAAQAAABAAVzZWcxMwABAAAAWAAAAJwAAAAQAP####8BAAAAABAAAAEABHNlZzEAAQAAAFgAAABdAAAAIwD#####Af###wACczQAAAAEAAAAYgAAACMA#####wH###8AAnMyAAAABAAAAHUAAAAjAP####8B####AAJzNgAAAAQAAAB5AAAAEAD#####AQAAAAAQAAABAAVzZWcxNAABAAAAdAAAAFgAAAAjAP####8B####AAJzOAAAAAQAAACvAAAAEAD#####AQAAAAAQAAABAAVzZWcyMQABAAAAXQAAAHgAAAAQAP####8BAAAAABAAAAEABHNlZzUAAQAAAHgAAACuAAAAEAD#####AQAAAAAQAAABAAVzZWczNQABAAAAdAAAAHMAAAAQAP####8BAAAAABAAAAEABXNlZzI4AAEAAABYAAAACQAAABAA#####wEAAAAAEAAAAQAFc2VnMzAAAQAAAF0AAAAGAAAAEAD#####AQAAAAAQAAABAAVzZWczMgABAAAAeAAAAHcAAAAQAP####8BAAAAABAAAAEABXNlZzM2AAEAAACuAAAArQAAABAA#####wAAAAAAEAAAAQAFc2VnMjAAAQAAAHMAAAAJAAAAEAD#####AAAAAAAQAAABAAVzZWcxMAABAAAACQAAAAYAAAAQAP####8AAAAAABAAAAEABXNlZzI1AAEAAAAGAAAAdwAAABAA#####wAAAAAAEAAAAQAEc2VnOQABAAAAdwAAAK0AAAAjAP####8B####AANzMTIAAAAEAAAAjgAAABAA#####wEAAAAAEAAAAQAFc2VnMjIAAQAAAAkAAACMAAAAEAD#####AQAAAAAQAAABAARzZWc4AAEAAACMAAAAjQAAABAA#####wEAAAAAEAAAAQAFc2VnMjYAAQAAAAYAAACNAAAALv##########'
      case 'pa2': return 'TWF0aEdyYXBoSmF2YTEuMAAAABI+TMzNAAJmcv###wEA#wEAAAAAAAAAAARBAAACSgAAAQEAAAAAAAAAAQAAACv#####AAAAAQAKQ0NhbGNDb25zdAD#####AAJwaQAWMy4xNDE1OTI2NTM1ODk3OTMyMzg0Nv####8AAAABAApDQ29uc3RhbnRlQAkh+1RELRj#####AAAAAQAKQ1BvaW50QmFzZQD#####AQAAAAAQAAAAAAAAAAAAAABACAAAAAAAAAUAAUBSQAAAAAAAQFH4UeuFHrj#####AAAAAQARQ1N5bWV0cmllQ2VudHJhbGUA#####wAAAAEAAAACAP####8BAAAAABAAAAAAAAAAAAAAAEAIAAAAAAAABQABQFlAAAAAAABAUfhR64UeuP####8AAAABAAlDRHJvaXRlQUIA#####wEAAAAAEAAAAQABAAAAAQAAAAP#####AAAAAQAWQ0Ryb2l0ZVBlcnBlbmRpY3VsYWlyZQD#####AQAAAAAQAAABAAEAAAADAAAABP####8AAAABAA9DUG9pbnRMaWVEcm9pdGUA#####wEAAAAAEAAAAAAAAAAAAAAAQAgAAAAAAAAFAAE#9sTsTsTsUQAAAAUAAAAFAP####8BAAAAABAAAAEAAQAAAAYAAAAFAAAABQD#####AQAAAAAQAAABAAEAAAABAAAABP####8AAAABABBDSW50RHJvaXRlRHJvaXRlAP####8BAAAAABAAAAAAAAAAAAAAAEAIAAAAAAAABQAAAAAIAAAABwAAAAYA#####wEAAAAAEAAAAAAAAAAAAAAAQAgAAAAAAAAFAAFAB2J2J2J2JwAAAAQAAAAFAP####8BAAAAABAAAAEAAQAAAAoAAAAEAAAABwD#####AQAAAAAQAAAAAAAAAAAAAABACAAAAAAAAAUAAAAACwAAAAf#####AAAAAQAJQ0NlcmNsZU9BAP####8BAAAAAAEAAAAGAAAADP####8AAAABABBDSW50RHJvaXRlQ2VyY2xlAP####8AAAAFAAAADf####8AAAABABBDUG9pbnRMaWVCaXBvaW50AP####8BAAAAABAAAAAAAAAAAAAAAEAIAAAAAAAABQACAAAADgAAAAoA#####wEAAAAAEAAAAAAAAAAAAAAAQAgAAAAAAAAFAAEAAAAOAAAABQD#####AQAAAAAQAAABAAEAAAAQAAAABQAAAAcA#####wEAAAAAEAAAAAAAAAAAAAAAQAgAAAAAAAAFAAAAAAgAAAARAAAACAD#####AQAAAAABAAAACQAAABIAAAAJAP####8AAAAHAAAAEwAAAAoA#####wEAAAAAEAAAAAAAAAAAAAAAQAgAAAAAAAAFAAIAAAAUAAAACgD#####AQAAAAAQAAAAAAAAAAAAAABACAAAAAAAAAUAAQAAABQAAAAFAP####8BAAAAABAAAAEAAQAAABYAAAAHAAAABwD#####AQAAAAAQAAAAAAAAAAAAAABACAAAAAAAAAUAAAAAFwAAAAQAAAAIAP####8BAAAAAAEAAAABAAAAGAAAAAkA#####wAAAAgAAAAZAAAACgD#####AQAAAAAQAAAAAAAAAAAAAABACAAAAAAAAAUAAgAAABoAAAAKAP####8BAAAAABAAAAAAAAAAAAAAAEAIAAAAAAAABQABAAAAGgAAAAgA#####wEAAAAAAQAAAAMAAAAKAAAACQD#####AAAABQAAAB0AAAAKAP####8BAAAAABAAAAAAAAAAAAAAAEAIAAAAAAAABQABAAAAHgAAAAoA#####wEAAAAAEAAAAAAAAAAAAAAAQAgAAAAAAAAFAAIAAAAe#####wAAAAEACUNQb2x5Z29uZQD#####AAAAAAABAAAABQAAABgAAAABAAAACQAAABYAAAAYAAAACwD#####AAAAAAABAAAABQAAAAEAAAADAAAABgAAAAkAAAABAAAACwD#####AAAAAAABAAAABQAAAAMAAAAKAAAADAAAAAYAAAADAAAACwD#####AQAAAAABAAAABQAAAAkAAAAGAAAAEAAAABIAAAAJAAAACwD#####AAAAAAABAAAABQAAABsAAAAgAAAAAwAAAAEAAAAb#####wAAAAEACENWZWN0ZXVyAP####8BAAAAABAAAAEAAQAAAAEAAAADAP####8AAAABAA1DVHJhbnNQYXJWZWN0AP####8AAAAm#####wAAAAEAC0NQb2ludEltYWdlAP####8BAAAAABAAAAAAAAAAAAAAAEAIAAAAAAAABQAAAAAKAAAAJwAAAA4A#####wEAAAAAEAAAAAAAAAAAAAAAQAgAAAAAAAAFAAAAAAwAAAAnAAAACwD#####AAAAAAABAAAABQAAAAoAAAAoAAAAKQAAAAwAAAAK################'
      case 'pa3': return 'TWF0aEdyYXBoSmF2YTEuMAAAABI+TMzNAAJmcv###wEA#wEAAAAAAAAAAAM7AAACdwAAAQEAAAAAAAAAAQAAADL#####AAAAAQAKQ0NhbGNDb25zdAD#####AAJwaQAWMy4xNDE1OTI2NTM1ODk3OTMyMzg0Nv####8AAAABAApDQ29uc3RhbnRlQAkh+1RELRj#####AAAAAQAKQ1BvaW50QmFzZQD#####AQAAAAAQAAAAAAAAAAAAAABACAAAAAAAAAUAAUBRQAAAAAAAQFE4UeuFHrj#####AAAAAQARQ1N5bWV0cmllQ2VudHJhbGUA#####wAAAAEAAAACAP####8BAAAAABAAAAAAAAAAAAAAAEAIAAAAAAAABQABQFhAAAAAAABAUThR64UeuP####8AAAABAAlDRHJvaXRlQUIA#####wEAAAAAEAAAAQABAAAAAQAAAAP#####AAAAAQAWQ0Ryb2l0ZVBlcnBlbmRpY3VsYWlyZQD#####AQAAAAAQAAABAAEAAAADAAAABP####8AAAABAA9DUG9pbnRMaWVEcm9pdGUA#####wEAAAAAEAAAAAAAAAAAAAAAQAgAAAAAAAAFAAE#9sTsTsTsUQAAAAUAAAAFAP####8BAAAAABAAAAEAAQAAAAYAAAAFAAAABQD#####AQAAAAAQAAABAAEAAAABAAAABP####8AAAABABBDSW50RHJvaXRlRHJvaXRlAP####8BAAAAABAAAAAAAAAAAAAAAEAIAAAAAAAABQAAAAAIAAAABwAAAAYA#####wEAAAAAEAAAAAAAAAAAAAAAQAgAAAAAAAAFAAFAB2J2J2J2JwAAAAQAAAAFAP####8BAAAAABAAAAEAAQAAAAoAAAAEAAAABwD#####AQAAAAAQAAAAAAAAAAAAAABACAAAAAAAAAUAAAAACwAAAAf#####AAAAAQAJQ0NlcmNsZU9BAP####8BAAAAAAEAAAAGAAAADP####8AAAABABBDSW50RHJvaXRlQ2VyY2xlAP####8AAAAFAAAADf####8AAAABABBDUG9pbnRMaWVCaXBvaW50AP####8BAAAAABAAAAAAAAAAAAAAAEAIAAAAAAAABQACAAAADgAAAAoA#####wEAAAAAEAAAAAAAAAAAAAAAQAgAAAAAAAAFAAEAAAAOAAAABQD#####AQAAAAAQAAABAAEAAAAQAAAABQAAAAcA#####wEAAAAAEAAAAAAAAAAAAAAAQAgAAAAAAAAFAAAAAAgAAAARAAAACAD#####AQAAAAABAAAACQAAABIAAAAJAP####8AAAAHAAAAEwAAAAoA#####wEAAAAAEAAAAAAAAAAAAAAAQAgAAAAAAAAFAAIAAAAUAAAACgD#####AQAAAAAQAAAAAAAAAAAAAABACAAAAAAAAAUAAQAAABQAAAAFAP####8BAAAAABAAAAEAAQAAABYAAAAHAAAABwD#####AQAAAAAQAAAAAAAAAAAAAABACAAAAAAAAAUAAAAAFwAAAAQAAAAIAP####8BAAAAAAEAAAABAAAAGAAAAAkA#####wAAAAgAAAAZAAAACgD#####AQAAAAAQAAAAAAAAAAAAAABACAAAAAAAAAUAAgAAABoAAAAKAP####8BAAAAABAAAAAAAAAAAAAAAEAIAAAAAAAABQABAAAAGgAAAAgA#####wEAAAAAAQAAAAMAAAAKAAAACQD#####AAAABQAAAB0AAAAKAP####8BAAAAABAAAAAAAAAAAAAAAEAIAAAAAAAABQABAAAAHgAAAAoA#####wEAAAAAEAAAAAAAAAAAAAAAQAgAAAAAAAAFAAIAAAAe#####wAAAAEACUNQb2x5Z29uZQD#####AAAAAAABAAAABQAAABgAAAABAAAACQAAABYAAAAYAAAACwD#####AAAAAAABAAAABQAAAAEAAAADAAAABgAAAAkAAAABAAAACwD#####AAAAAAABAAAABQAAAAMAAAAKAAAADAAAAAYAAAADAAAACwD#####AQAAAAABAAAABQAAAAkAAAAGAAAAEAAAABIAAAAJAAAACwD#####AAAAAAABAAAABQAAABsAAAAgAAAAAwAAAAEAAAAb#####wAAAAEACENWZWN0ZXVyAP####8BAAAAABAAAAEAAQAAAAEAAAADAP####8AAAABAA1DVHJhbnNQYXJWZWN0AP####8AAAAm#####wAAAAEAC0NQb2ludEltYWdlAP####8BAAAAABAAAAAAAAAAAAAAAEAIAAAAAAAABQAAAAAKAAAAJwAAAA4A#####wEAAAAAEAAAAAAAAAAAAAAAQAgAAAAAAAAFAAAAAAwAAAAnAAAACwD#####AAAAAAABAAAABQAAAAoAAAAoAAAAKQAAAAwAAAAKAAAADAD#####AQAAAAAQAAABAAEAAAADAAAACgAAAAANAP####8AAAArAAAADgD#####AQAAAAAQAAAAAAAAAAAAAABACAAAAAAAAAUAAAAAIAAAACwAAAAMAP####8BAAAAABAAAAEAAQAAAAoAAAAoAAAAAA0A#####wAAAC4AAAAOAP####8BAAAAABAAAAAAAAAAAAAAAEAIAAAAAAAABQAAAAAtAAAALwAAAAsA#####wAAAAAAAQAAAAUAAAAtAAAACgAAACgAAAAwAAAALf###############w=='
      case 'pr1': return 'TWF0aEdyYXBoSmF2YTEuMAAAABM+TMzNAAJmcv###wEA#wEAAAAAAAAAAANLAAAChwAAAQEAAAAAAAAAAQAAAP######AAAAAQAKQ0NhbGNDb25zdAD#####AAJwaQAWMy4xNDE1OTI2NTM1ODk3OTMyMzg0Nv####8AAAABAApDQ29uc3RhbnRlQAkh+1RELRj#####AAAAAQAKQ1BvaW50QmFzZQD#####AQAAAAAQAAAAAAAAAAAAAABACAAAAAAAAAAABQABQE0CHhaAsgBAKeKpp8JOkAAAAAIA#####wEAAAAAEAAAAAAAAAAAAAAAQAgAAAAAAAAAAAUAAUBY4AAAAAAAQEfrhR64UewAAAACAP####8BAAAAABAAAUMAAAAAAAAAAABACAAAAAAAAAAABQABQEa9LePvUABAR5dhuPO8vP####8AAAABAAlDRHJvaXRlQUIA#####wEAAAAAEAAAAQAAAAEAAAADAAAAAv####8AAAABABZDRHJvaXRlUGVycGVuZGljdWxhaXJlAP####8BAAAAABAAAAEAAAABAAAAAwAAAAQAAAAEAP####8BAAAAABAAAAEAAAABAAAAAgAAAAT#####AAAAAQAPQ1BvaW50TGllRHJvaXRlAP####8BAAAAABAAAAAAAAAAAAAAAEAIAAAAAAAAAAAFAAE#5CwMLZ9z7AAAAAX#####AAAAAQAQQ0Ryb2l0ZVBhcmFsbGVsZQD#####AQAAAAAQAAABAAAAAQAAAAcAAAAE#####wAAAAEAEENJbnREcm9pdGVEcm9pdGUA#####wEAAAAAEAAAAAAAAAAAAAAAQAgAAAAAAAAAAAUAAAAABgAAAAj#####AAAAAQAIQ1ZlY3RldXIA#####wEAAAAAEAAAAQAAAAEAAAADAAAABwD#####AAAAAQANQ1RyYW5zUGFyVmVjdAD#####AAAACgAAAAYA#####wH#AAAAEAAAAQAAAAEAAAABAAAABAAAAAUA#####wEAAAAAEAAAAAAAAAAAAAAAQAgAAAAAAAAAAAUAAT#Rt96tMPCpAAAADP####8AAAABAAtDUG9pbnRJbWFnZQD#####Af8AAAAQAAFPAAAAAAAAAAAAQAgAAAAAAAAAAAUAAAAAAQAAAAsAAAAKAP####8B#wAAABAAAU4AAAAAAAAAAABACAAAAAAAAAAABQAAAAANAAAAC#####8AAAABAA1DRGVtaURyb2l0ZU9BAP####8BAAD#ABAAAAEAAAABAAAACQAAAA8AAAAFAP####8BAAD#ABAAAU0AAAAAAAAAAABACAAAAAAAAAAABQABQAo+tvYMjlwAAAAQ#####wAAAAEAFENJbXBsZW1lbnRhdGlvblByb3RvAP####8AG01lc3VyZSBkJ2FuZ2xlIG5vbiBvcmllbnTDqQAAAAIAAAADAAAAAwAAABEAAAAPAAAADv####8AAAABAAxDQmlzc2VjdHJpY2UAAAAAEgEAAAAAEAAAAQAAAAEAAAARAAAADwAAAA4AAAAFAAAAABIBAAAAABAAAAEAAAUAAUBvJahlrHGpAAAAE#####8AAAABABdDTWVzdXJlQW5nbGVHZW9tZXRyaXF1ZQEAAAASAAAAEQAAAA8AAAAO#####wAAAAIAF0NNYXJxdWVBbmdsZUdlb21ldHJpcXVlAQAAABIBAAD#AAAAAQAAAAFARQYXf1SRuwAAABEAAAAPAAAADv####8AAAABAA9DVmFsZXVyQWZmaWNoZWUBAAAAEgEAAP8AQAgAAAAAAAAAAAAAAAAAAAAAAAAAFA8AAAAAAAEAAAABAAAAAQAAAAAAAAAAAAAAAsKwAAAAABX#####AAAAAQAJQ1BvbHlnb25lAP####8BAAAAAAAAAQAAAAUAAAADAAAAAgAAAAkAAAAHAAAAA#####8AAAABAAlDQ2VyY2xlT0EA#####wEAAAAAAAABAAAAAwAAAAH#####AAAAAQAQQ0ludERyb2l0ZUNlcmNsZQD#####AAAABAAAABn#####AAAAAQAQQ1BvaW50TGllQmlwb2ludAD#####AQAAAAAQAAAAAAAAAAAAAABACAAAAAAAAAAABQABAAAAGgAAABQA#####wEAAAAAEAAAAAAAAAAAAAAAQAgAAAAAAAAAAAUAAgAAABoAAAAKAP####8BAAAAABAAAAAAAAAAAAAAAEAIAAAAAAAAAAAFAAAAABwAAAAL#####wAAAAEAB0NNaWxpZXUA#####wEAAAAAEAAAAAAAAAAAAAAAQAgAAAAAAAAAAAUAAAAAAwAAAAcAAAAVAP####8BAAAAABAAAAAAAAAAAAAAAEAIAAAAAAAAAAAFAAAAAAIAAAAJAAAAAwD#####AQAAAAAQAAABAAAAAQAAAB4AAAAf#####wAAAAEAD0NTeW1ldHJpZUF4aWFsZQD#####AAAAIAAAAAoA#####wEAAAAAEAAAAAAAAAAAAAAAQAgAAAAAAAAAAAUAAAAAAQAAACEAAAARAP####8BAAAAAAAAAQAAAAUAAAAcAAAAAwAAAAcAAAAdAAAAHAAAAAIA#####wEAAP8AEAABRAAAAAAAAAAAAEAIAAAAAAAAAAAFAAFAT+bbt7AaSEBMLDii1CHgAAAAAgD#####AQAA#wAQAAFBAAAAAAAAAAAAQAgAAAAAAAAAAAUAAcBiwAAAAAAAQHKOFHrhR64AAAACAP####8BAAD#ABAAAUIAAAAAAAAAAABACAAAAAAAAAAABQABQEOatcqmnwBAaQHSHBCv+v####8AAAABAAlDTG9uZ3VldXIA#####wAAACUAAAAmAAAADAD#####ABJNZXN1cmUgZGUgbG9uZ3VldXIAAAAFAAAAAgAAAAIAAAADAAAAJP####8AAAABAAtDTWVkaWF0cmljZQAAAAAoAQAAAAAQAAABAAAAAQAAAAMAAAAkAAAAFQAAAAAoAQAAAAAQAAABAAAFAAAAAAMAAAAk#####wAAAAIACUNDZXJjbGVPUgAAAAAoAQAAAAAAAAEAAAAqAAAAAUAwAAAAAAAAAQAAABMAAAAAKAAAACkAAAArAAAAFAAAAAAoAQAAAAAQAAABAAAFAAEAAAAsAAAAFwEAAAAoAAAAAwAAACQAAAAQAQAAACgBAAD#AQAAAAAALREAAAAAAAEAAAABAAAAAQAAAAAAAAAAAAAAAAEAAAAu#####wAAAAEACENTZWdtZW50AP####8BAAD#ABAAAAEAAAABAAAAAwAAACQAAAACAP####8AAAAAABAAAABACAAAAAAAAAAAAAAAAAAAAAAFAABAYaAAAAAAAEB+3hR64Ueu#####wAAAAEAB0NDYWxjdWwA#####wAFbWluaTEAATAAAAABAAAAAAAAAAAAAAAbAP####8ABW1heGkxAAExAAAAAT#wAAAAAAAAAAAADAD#####AAdDdXJzZXVyAAAABQAAAAUAAAADAAAAMgAAADMAAAAx#####wAAAAEAFENEcm9pdGVEaXJlY3Rpb25GaXhlAAAAADQBAAAAABAAAAEAAAABAAAAMQE#8AAAAAAAAAAAAAUBAAAANAAAAP8AEAAAAMAIAAAAAAAAP#AAAAAAAAAAAAUAAEBdQAAAAAAAAAAANf####8AAAABAAtDSG9tb3RoZXRpZQAAAAA0AAAAMf####8AAAABAApDT3BlcmF0aW9uA#####8AAAABAA9DUmVzdWx0YXRWYWxldXIAAAAyAAAAHgEAAAAfAAAAMgAAAB8AAAAzAAAACgAAAAA0AQAAAAANAAJPMQDAEAAAAAAAAEAQAAAAAAAAAAAFAAAAADYAAAA3AAAAHQAAAAA0AAAAMQAAAB4DAAAAHgEAAAABP#AAAAAAAAAAAAAfAAAAMgAAAB4BAAAAHwAAADMAAAAfAAAAMgAAAAoAAAAANAEAAAAADQACSTEAwAAAAAAAAABACAAAAAAAAAAABQAAAAA2AAAAOQAAABoBAAAANAAAAP8AEAAAAQAAAAEAAAAxAAAANgAAAAUBAAAANAAAAP8BEAACazEAwAAAAAAAAABAAAAAAAAAAAAAAQABAAAAAAAAAAAAAAA7#####wAAAAIAD0NNZXN1cmVBYnNjaXNzZQEAAAA0AAFhAAAAOAAAADoAAAA8AAAAEAEAAAA0AAAA#wAAAAAAAAAAAMAYAAAAAAAAAAAAAAA8DwAB####AAAAAQAAAAIAAAABAAAAAAAAAAAAAAAAAgAAAD0AAAACAP####8AAAAAABAAAABACAAAAAAAAAAAAAAAAAAAAAAFAABAdCAAAAAAAEB+bhR64UeuAAAAGwD#####AAVtaW5pMgABMAAAAAEAAAAAAAAAAAAAABsA#####wAFbWF4aTIAATEAAAABP#AAAAAAAAAAAAAMAP####8AB0N1cnNldXIAAAAFAAAABQAAAAMAAABAAAAAQQAAAD8AAAAcAAAAAEIBAAAAABAAAAEAAAABAAAAPwE#8AAAAAAAAAAAAAUBAAAAQgAAAP8AEAAAAMAIAAAAAAAAP#AAAAAAAAAAAAUAAEBdQAAAAAAAAAAAQwAAAB0AAAAAQgAAAD8AAAAeAwAAAB8AAABAAAAAHgEAAAAfAAAAQAAAAB8AAABBAAAACgAAAABCAQAAAAANAAJPMQDAEAAAAAAAAEAQAAAAAAAAAAAFAAAAAEQAAABFAAAAHQAAAABCAAAAPwAAAB4DAAAAHgEAAAABP#AAAAAAAAAAAAAfAAAAQAAAAB4BAAAAHwAAAEEAAAAfAAAAQAAAAAoAAAAAQgEAAAAADQACSTEAwAAAAAAAAABACAAAAAAAAAAABQAAAABEAAAARwAAABoBAAAAQgAAAP8AEAAAAQAAAAEAAAA#AAAARAAAAAUBAAAAQgAAAP8BEAABawDAAAAAAAAAAEAAAAAAAAAAAAABAAEAAAAAAAAAAAAAAEkAAAAgAQAAAEIAAWIAAABGAAAASAAAAEoAAAAQAQAAAEIAAAD#AAAAAAAAAAAAwBgAAAAAAAAAAAAAAEoPAAH###8AAAABAAAAAgAAAAEAAAAAAAAAAAAAAAACAAAASwAAABsA#####wACYWEAAWEAAAAfAAAAPQAAABsA#####wACYmIAAWIAAAAfAAAASwAAABkA#####wEAAP8AAAABAAAAAwAAAB4CAAAAHwAAAE0AAAAfAAAALgAAAAATAP####8AAAAwAAAATwAAABQA#####wH#AAAAEAAAAAAAAAAAAAAAQAgAAAAAAAAAAAUAAQAAAFAAAAAUAP####8AAAD#ABAAAAAAAAAAAAAAAEAIAAAAAAAAAAAFAAIAAABQAAAACAD#####Af8AAAAQAAABAAAAAQAAAAcAAABRAAAAAAkA#####wAAAFMAAAAKAP####8B#wAAABAAAAAAAAAAAAAAAEAIAAAAAAAAAAAFAAAAAB0AAABUAAAACgD#####Af8AAAAQAAAAAAAAAAAAAABACAAAAAAAAAAABQAAAAAJAAAAVAAAABEA#####wEAAAAAAAABAAAABQAAAAEAAAADAAAAAgAAAA0AAAABAAAAEgD#####AQAAAAAAAAEAAAACAAAADQAAABMA#####wAAAAQAAABYAAAAFAD#####AQAAAAAQAAAAAAAAAAAAAABACAAAAAAAAAAABQACAAAAWQAAABQA#####wEAAAAAEAAAAAAAAAAAAAAAQAgAAAAAAAAAAAUAAQAAAFkAAAAKAP####8BAAAAABAAAAAAAAAAAAAAAEAIAAAAAAAAAAAFAAAAAFsAAAALAAAACAD#####AQAAAAAQAAABAAAAAQAAAAEAAAANAAAAAAkA#####wAAAF0AAAAKAP####8BAAAAABAAAAAAAAAAAAAAAEAIAAAAAAAAAAAFAAAAAFsAAABeAAAAEgD#####AQAAAAAAAAEAAABbAAAAXwAAABMA#####wAAAAQAAABgAAAAFAD#####AQAAAAAQAAAAAAAAAAAAAABACAAAAAAAAAAABQACAAAAYQAAABQA#####wEAAAAAEAAAAAAAAAAAAAAAQAgAAAAAAAAAAAUAAQAAAGEAAAAKAP####8BAAAAABAAAAAAAAAAAAAAAEAIAAAAAAAAAAAFAAAAAGMAAAALAAAACgD#####AQAAAAAQAAAAAAAAAAAAAABACAAAAAAAAAAABQAAAAANAAAAIQAAABEA#####wEAAAAAAAABAAAABQAAAAIAAABbAAAAXAAAAAkAAAACAAAAEQD#####AQAAAAAAAAEAAAAFAAAAWwAAAGMAAABkAAAAXAAAAFsAAAARAP####8BAAAAAAAAAQAAAAUAAAAHAAAAIgAAAGUAAAAJAAAABwAAAAoA#####wH#AAAAEAAAAAAAAAAAAAAAQAgAAAAAAAAAAAUAAAAAXAAAAFQAAAAKAP####8B#wAAABAAAAAAAAAAAAAAAEAIAAAAAAAAAAAFAAAAAGQAAABUAAAABwD#####Af8AAAAQAAAAAAAAAAAAAABACAAAAAAAAAAABQAAAAAMAAAABQAAAAgA#####wH#AAAAEAAAAQAAAAEAAABrAAAAAQAAAAADAP####8B#wAAABAAAAEAAAABAAAABwAAAFEAAAADAP####8B#wAAABAAAAEAAAABAAAAAwAAAFEAAAAGAP####8B#wAAABAAAAEAAAABAAAAawAAAG4AAAAHAP####8B#wAAABAAAAAAAAAAAAAAAEAIAAAAAAAAAAAFAAAAAG8AAABtAAAACQD#####AAAAbAAAAAoA#####wH#AAAAEAAAAAAAAAAAAAAAQAgAAAAAAAAAAAUAAAAAcAAAAHEAAAAKAP####8B#wAAABAAAAAAAAAAAAAAAEAIAAAAAAAAAAAFAAAAAHIAAABeAAAAEQD#####Af8AAAAAAAEAAAAFAAAAUQAAAHIAAABzAAAAVgAAAFEAAAARAP####8B#wAAAAAAAQAAAAUAAABVAAAAUQAAAAcAAAAdAAAAVQAAABEA#####wEAAAAAAAABAAAABQAAAAcAAABRAAAAVgAAAAkAAAAHAAAAEQD#####Af8AAAAAAAEAAAAFAAAAVgAAAGkAAABcAAAACQAAAFYAAAARAP####8B#wAAAAAAAQAAAAUAAABpAAAAagAAAGQAAABcAAAAaQAAAAYA#####wH#AAAAEAAAAQAAAAEAAAAiAAAACAAAAAcA#####wH#AAAAEAAAAAAAAAAAAAAAQAgAAAAAAAAAAAUAAAAABQAAAHkAAAAGAP####8B#wAAABAAAAEAAAABAAAAegAAAG4AAAAHAP####8B#wAAABAAAAAAAAAAAAAAAEAIAAAAAAAAAAAFAAAAAG0AAAB7AAAACgD#####Af8AAAAQAAAAAAAAAAAAAABACAAAAAAAAAAABQAAAAB8AAAAcQAAAAoA#####wH#AAAAEAAAAAAAAAAAAAAAQAgAAAAAAAAAAAUAAAAAfQAAAF4AAAARAP####8B#wAAAAAAAQAAAAUAAAAHAAAAfQAAAH4AAAAJAAAABwAAAAIA#####wEAAAAAEAABRQAAAAAAAAAAAEAIAAAAAAAAAAAFAAFAcsG4swKnpEBki9huPO8wAAAACAD#####AQAAAAAQAAABAAAAAQAAAH0AAACAAAAAAAkA#####wAAAIEAAAAKAP####8BAAAAABAAAUcAAAAAAAAAAABACAAAAAAAAAAABQAAAAAOAAAAggAAABgA#####wEAAAAAEAAAAQAAAAEAAACDAAAAgAAAAAUA#####wEAAAAAEAABRgAAAAAAAAAAAEAIAAAAAAAAAAAFAAG#7plqUA5+KQAAAIQAAAAMAP####8AG01lc3VyZSBkJ2FuZ2xlIG5vbiBvcmllbnTDqQAAAAIAAAADAAAAAwAAAIAAAACFAAAAgwAAAA0AAAAAhgEAAAAAEAAAAQAAAAEAAACAAAAAhQAAAIMAAAAFAAAAAIYBAAAAABAAAAEAAAUAAUBvJahlrHGpAAAAhwAAAA4BAAAAhgAAAIAAAACFAAAAgwAAAA8BAAAAhgEAAAAAAAABAAAAAUBFBhd#VJG7AAAAgAAAAIUAAACDAAAAEAEAAACGAQAAAABACAAAAAAAAAAAAAAAAAAAAAAAAACIDwAAAAAAAQAAAAEAAAABAAAAAAAAAAAAAAACwrAAAAAAif####8AAAABAAlDUm90YXRpb24A#####wAAAIX#####AAAAAQAMQ01vaW5zVW5haXJlAAAAHgIAAAAfAAAATgAAAB8AAACJAAAACgD#####AQAAAAAQAAAAAAAAAAAAAABACAAAAAAAAAAABQAAAACAAAAAjAAAAAgA#####wEAAAAAEAAAAQAAAAEAAACAAAAAfQAAAAAJAP####8AAACOAAAACgD#####AQAA#wAQAAAAAAAAAAAAAABACAAAAAAAAAAABQAAAACNAAAAjwAAAAoA#####wEAAP8AEAAAAAAAAAAAAAAAQAgAAAAAAAAAAAUAAAAAkAAAAF4AAAARAP####8BAAAAAAAAAQAAAAUAAAAHAAAAkAAAAJEAAAAJAAAABwAAAAoA#####wEAAP8AEAABSAAAAAAAAAAAAEAIAAAAAAAAAAAFAAAAAB0AAACCAAAAGAD#####AQAA#wAQAAABAAAAAQAAAJMAAACDAAAABQD#####AQAA#wAQAAFJAAAAAAAAAAAAQAgAAAAAAAAAAAUAAT#ayZzsWB#NAAAAlAAAAAwA#####wAbTWVzdXJlIGQnYW5nbGUgbm9uIG9yaWVudMOpAAAAAgAAAAMAAAADAAAAkwAAAJUAAACDAAAADQAAAACWAQAAAAAQAAABAAAAAQAAAJMAAACVAAAAgwAAAAUAAAAAlgEAAAAAEAAAAQAABQABQG8lqGWscakAAACXAAAADgEAAACWAAAAkwAAAJUAAACDAAAADwEAAACWAQAA#wAAAAEAAAABQEUGF39UkbsAAACTAAAAlQAAAIMAAAAQAQAAAJYBAAD#AEAIAAAAAAAAAAAAAAAAAAAAAAAAAJgPAAAAAAABAAAAAQAAAAEAAAAAAAAAAAAAAALCsAAAAACZAAAAIQD#####AAAAlQAAACIAAAAeAgAAAB8AAABOAAAAHwAAAJkAAAAKAP####8BAAD#ABAAAAAAAAAAAAAAAEAIAAAAAAAAAAAFAAAAAJMAAACcAAAACgD#####AQAA#wAQAAAAAAAAAAAAAABACAAAAAAAAAAABQAAAACdAAAAjwAAAAoA#####wEAAP8AEAAAAAAAAAAAAAAAQAgAAAAAAAAAAAUAAAAAngAAAFQAAAARAP####8BAAAAAAAAAQAAAAUAAACeAAAAnwAAAFEAAAAHAAAAngAAAAoA#####wEAAP8AEAABTAAAAAAAAAAAAEAIAAAAAAAAAAAFAAAAAFwAAACCAAAACgD#####AQAA#wAQAAFKAAAAAAAAAAAAQAgAAAAAAAAAAAUAAAAADwAAAIIAAAAYAP####8BAAD#ABAAAAEAAAABAAAAogAAAKEAAAAFAP####8BAAD#ABAAAUsAAAAAAAAAAABACAAAAAAAAAAABQABP8lLe0A2PpkAAACjAAAADAD#####ABtNZXN1cmUgZCdhbmdsZSBub24gb3JpZW50w6kAAAACAAAAAwAAAAMAAACiAAAApAAAAKEAAAANAAAAAKUBAAAAABAAAAEAAAABAAAAogAAAKQAAAChAAAABQAAAAClAQAAAAAQAAABAAAFAAFAbyWoZaxxqQAAAKYAAAAOAQAAAKUAAACiAAAApAAAAKEAAAAPAQAAAKUBAAD#AAAAAQAAAAFARQYXf1SRuwAAAKIAAACkAAAAoQAAABABAAAApQEAAP8AQAgAAAAAAAAAAAAAAAAAAAAAAAAApw8AAAAAAAEAAAABAAAAAQAAAAAAAAAAAAAAAsKwAAAAAKgAAAAhAP####8AAACkAAAAHgIAAAAfAAAATgAAAB8AAACoAAAACgD#####AQAA#wAQAAAAAAAAAAAAAABACAAAAAAAAAAABQAAAAChAAAAqwAAAAoA#####wEAAP8AEAAAAAAAAAAAAAAAQAgAAAAAAAAAAAUAAAAArAAAAI8AAAAKAP####8BAAD#ABAAAAAAAAAAAAAAAEAIAAAAAAAAAAAFAAAAAK0AAABUAAAAEQD#####AQAAAAAAAAEAAAAFAAAArgAAAK0AAAAJAAAAVgAAAK4AAAALAP####8BAAD#ABAAAAEAAAABAAAACQAAAK0AAAAKAP####8BAAD#ABAAAAAAAAAAAAAAAEAIAAAAAAAAAAAFAAAAAK0AAABeAAAAEgD#####AQAA#wAAAAEAAACtAAAAsQAAABMA#####wAAALAAAACyAAAAFAD#####AQAA#wAQAAAAAAAAAAAAAABACAAAAAAAAAAABQACAAAAswAAABQA#####wEAAP8AEAAAAAAAAAAAAAAAQAgAAAAAAAAAAAUAAQAAALMAAAAhAP####8AAACtAAAAHgIAAAAfAAAAFQAAAB8AAABOAAAACgD#####AQAA#wAQAAAAAAAAAAAAAABACAAAAAAAAAAABQAAAAC1AAAAtgAAAAoA#####wEAAP8AEAAAAAAAAAAAAAAAQAgAAAAAAAAAAAUAAAAAtwAAAFQAAAARAP####8BAAAAAAAAAQAAAAUAAAC3AAAAuAAAAK4AAACtAAAAtwAAAAgA#####wEAAP8AEAAAAQAAAAEAAAADAAAAUQAAAAAJAP####8AAAC6AAAACgD#####AQAAAAAQAAAAAAAAAAAAAABACAAAAAAAAAAABQAAAAANAAAAuwAAAAoA#####wEAAAAAEAABUAAAAAAAAAAAAEAIAAAAAAAAAAAFAAAAAHMAAACCAAAACgD#####AQAAAAAQAAFSAAAAAAAAAAAAQAgAAAAAAAAAAAUAAAAAvAAAAIIAAAAYAP####8BAAAAABAAAAEAAAABAAAAvgAAAL0AAAAFAP####8BAAAAABAAAVEAAAAAAAAAAABACAAAAAAAAAAABQABP+ARZUZXyr4AAAC#AAAADAD#####ABtNZXN1cmUgZCdhbmdsZSBub24gb3JpZW50w6kAAAACAAAAAwAAAAMAAAC9AAAAwAAAAL4AAAANAAAAAMEBAAAAABAAAAEAAAABAAAAvQAAAMAAAAC+AAAABQAAAADBAQAAAAAQAAABAAAFAAFAbyWoZaxxqQAAAMIAAAAOAQAAAMEAAAC9AAAAwAAAAL4AAAAPAQAAAMEBAAAAAAAAAQAAAAFARQYXf1SRuwAAAL0AAADAAAAAvgAAABABAAAAwQEAAAAAQAgAAAAAAAAAAAAAAAAAAAAAAAAAww8AAAAAAAEAAAABAAAAAQAAAAAAAAAAAAAAAsKwAAAAAMQAAAAhAP####8AAADAAAAAHgIAAAAfAAAATgAAAB8AAADEAAAACgD#####AQAAAAAQAAAAAAAAAAAAAABACAAAAAAAAAAABQAAAAC9AAAAxwAAAAoA#####wEAAP8AEAAAAAAAAAAAAAAAQAgAAAAAAAAAAAUAAAAAyAAAAI8AAAAIAP####8BAAD#ABAAAAEAAAABAAAADQAAAAEAAAAACQD#####AAAAygAAAAoA#####wEAAP8AEAAAAAAAAAAAAAAAQAgAAAAAAAAAAAUAAAAAyQAAAMsAAAARAP####8BAAAAAAAAAQAAAAUAAABRAAAAzAAAAMkAAABWAAAAUf####8AAAABABBDU3VyZmFjZVBvbHlnb25lAP####8B####AAJzMQAAAAQAAAAjAAAAIwD#####Af###wACczIAAAAEAAAAGAAAACMA#####wH###8AAnMzAAAABAAAAGYAAAAjAP####8B####AAJzNAAAAAQAAABnAAAAIwD#####Af###wACczUAAAAEAAAAVwAAACMA#####wH###8AAnM2AAAABAAAAGgAAAAaAP####8AAAAAABAAAAEABXNlZzEwAAEAAAABAAAADQAAABoA#####wAAAAAAEAAAAQAFc2VnMjYAAQAAAAEAAAADAAAAGgD#####AAAAAAAQAAABAAVzZWcxOAABAAAADQAAAAIAAAAaAP####8AAAAAABAAAAEABXNlZzI3AAEAAAAcAAAAAwAAABoA#####wAAAAAAEAAAAQAFc2VnMzQAAQAAAAMAAAACAAAAGgD#####AAAAAAAQAAABAAVzZWcxOQABAAAAAgAAAFsAAAAaAP####8AAAAAABAAAAEABXNlZzExAAEAAABbAAAAXwAAABoA#####wAAAAAAEAAAAQAEc2VnMQABAAAAHAAAAB0AAAAaAP####8AAAAAABAAAAEABHNlZzQAAQAAAAMAAAAHAAAAGgD#####AAAAAAAQAAABAARzZWc1AAEAAAACAAAACQAAABoA#####wAAAAAAEAAAAQAEc2VnOAABAAAAWwAAAFwAAAAaAP####8AAAAAABAAAAEABHNlZzAAAQAAAF8AAABkAAAAGgD#####AAAAAAAQAAABAAVzZWczMAABAAAAHQAAAAcAAAAaAP####8AAAAAABAAAAEABXNlZzIxAAEAAAAJAAAAXAAAABoA#####wAAAAAAEAAAAQAFc2VnMTUAAQAAAFwAAABkAAAAGgD#####AAAAAAAQAAABAAVzZWczMQABAAAABwAAACIAAAAaAP####8AAAAAABAAAAEABXNlZzIyAAEAAAAJAAAAZQAAABoA#####wAAAAAAEAAAAQAFc2VnMTQAAQAAACIAAABlAAAAIwD#####Af###wADczExAAAABAAAAM0AAAAaAP####8BAAAAABAAAAEABXNlZzI4AAEAAADMAAAAUQAAABoA#####wEAAAAAEAAAAQAFc2VnMTIAAQAAAMwAAADJAAAAGgD#####AQAAAAAQAAABAAVzZWcyMAABAAAAyQAAAFYAAAAaAP####8BAAAAABAAAAEABXNlZzM1AAEAAABRAAAAVgAAACMA#####wH###8AAnM3AAAABAAAAKAAAAAaAP####8BAAAAABAAAAEABXNlZzI5AAEAAACfAAAAUQAAACMA#####wH###8AAnM4AAAABAAAAHYAAAAjAP####8B####AAJzOQAAAAQAAACvAAAAIwD#####Af###wADczEwAAAABAAAALkAAAAaAP####8BAAAAABAAAAEABXNlZzI1AAEAAABWAAAArgAAABoA#####wEAAAAAEAAAAQAFc2VnMTMAAQAAAK4AAAC4AAAAGgD#####AQAAAAAQAAABAARzZWczAAEAAACfAAAAngAAABoA#####wEAAAAAEAAAAQAEc2VnNwABAAAAUQAAAAcAAAAaAP####8BAAAAABAAAAEABHNlZzYAAQAAAFYAAAAJAAAAGgD#####AQAAAAAQAAABAARzZWc5AAEAAACuAAAArQAAABoA#####wEAAAAAEAAAAQAEc2VnMgABAAAAuAAAALcAAAAaAP####8BAAAAABAAAAEABXNlZzMyAAEAAACeAAAABwAAABoA#####wAAAAAAEAAAAQAFc2VnMzYAAQAAAAcAAAAJAAAAGgD#####AQAAAAAQAAABAAVzZWcyMwABAAAACQAAAK0AAAAaAP####8BAAAAABAAAAEABXNlZzE3AAEAAACtAAAAtwAAACMA#####wH###8AA3MxMgAAAAQAAACSAAAAGgD#####AQAAAAAQAAABAAVzZWczMwABAAAABwAAAJAAAAAaAP####8BAAAAABAAAAEABXNlZzI0AAEAAAAJAAAAkQAAABoA#####wEAAAAAEAAAAQAFc2VnMTYAAQAAAJAAAACRAAAAJ###########'
      case 'pr2': return 'TWF0aEdyYXBoSmF2YTEuMAAAABI+TMzNAAJmcv###wEA#wEAAAAAAAAAAANLAAAChwAAAQEAAAAAAAAAAQAAAE7#####AAAAAQAKQ0NhbGNDb25zdAD#####AAJwaQAWMy4xNDE1OTI2NTM1ODk3OTMyMzg0Nv####8AAAABAApDQ29uc3RhbnRlQAkh+1RELRj#####AAAAAQAKQ1BvaW50QmFzZQD#####AQAAAAAQAAAAAAAAAAAAAABACAAAAAAAAAUAAUBNAh4WgLIAQCniqafCTpAAAAACAP####8BAAAAABAAAAAAAAAAAAAAAEAIAAAAAAAABQABQFsK73BjHPRAR1mnQNp0DAAAAAIA#####wEAAAAAEAABQwAAAAAAAAAAAEAIAAAAAAAABQABQEa9LePvUABAR5dhuPO8vP####8AAAABAAlDRHJvaXRlQUIA#####wEAAAAAEAAAAQABAAAAAwAAAAL#####AAAAAQAWQ0Ryb2l0ZVBlcnBlbmRpY3VsYWlyZQD#####AQAAAAAQAAABAAEAAAADAAAABAAAAAQA#####wEAAAAAEAAAAQABAAAAAgAAAAT#####AAAAAQAPQ1BvaW50TGllRHJvaXRlAP####8BAAAAABAAAAAAAAAAAAAAAEAIAAAAAAAABQABP+QsDC2fc+wAAAAF#####wAAAAEAEENEcm9pdGVQYXJhbGxlbGUA#####wEAAAAAEAAAAQABAAAABwAAAAT#####AAAAAQAQQ0ludERyb2l0ZURyb2l0ZQD#####AQAAAAAQAAAAAAAAAAAAAABACAAAAAAAAAUAAAAABgAAAAj#####AAAAAQAIQ1ZlY3RldXIA#####wEAAAAAEAAAAQABAAAAAwAAAAcA#####wAAAAEADUNUcmFuc1BhclZlY3QA#####wAAAAoAAAAGAP####8B#wAAABAAAAEAAQAAAAEAAAAEAAAABQD#####AQAAAAAQAAAAAAAAAAAAAABACAAAAAAAAAUAAT#Rt96tMPCpAAAADP####8AAAABAAtDUG9pbnRJbWFnZQD#####Af8AAAAQAAFPAAAAAAAAAAAAQAgAAAAAAAAFAAAAAAEAAAALAAAACgD#####Af8AAAAQAAFOAAAAAAAAAAAAQAgAAAAAAAAFAAAAAA0AAAAL#####wAAAAEADUNEZW1pRHJvaXRlT0EA#####wEAAP8AEAAAAQABAAAACQAAAA8AAAAFAP####8BAAD#ABAAAU0AAAAAAAAAAABACAAAAAAAAAUAAUAKPrb2DI5cAAAAEP####8AAAABABRDSW1wbGVtZW50YXRpb25Qcm90bwD#####ABtNZXN1cmUgZCdhbmdsZSBub24gb3JpZW50w6kAAAACAAAAAwAAAAMAAAARAAAADwAAAA7#####AAAAAQAMQ0Jpc3NlY3RyaWNlAAAAABIBAAAAABAAAAEAAQAAABEAAAAPAAAADgAAAAUAAAAAEgEAAAAAEAAAAQUAAUBvJahlrHGpAAAAE#####8AAAABABdDTWVzdXJlQW5nbGVHZW9tZXRyaXF1ZQEAAAASAAAAEQAAAA8AAAAO#####wAAAAIAF0NNYXJxdWVBbmdsZUdlb21ldHJpcXVlAQAAABIBAAD#AAEAAAABQEUGF39UkbsAAAARAAAADwAAAA7#####AAAAAQAPQ1ZhbGV1ckFmZmljaGVlAQAAABIBAAD#AEAIAAAAAAAAAAAAAAAAAAAAAAAUDwAAAAAAAQAAAAEAAAABAAAAAAAAAAAAAAACwrAAAAAAFf####8AAAABAAlDUG9seWdvbmUA#####wAAAAAAAQAAAAUAAAADAAAAAgAAAAkAAAAHAAAAA#####8AAAABAAlDQ2VyY2xlT0EA#####wEAAAAAAQAAAAMAAAAB#####wAAAAEAEENJbnREcm9pdGVDZXJjbGUA#####wAAAAQAAAAZ#####wAAAAEAEENQb2ludExpZUJpcG9pbnQA#####wEAAAAAEAAAAAAAAAAAAAAAQAgAAAAAAAAFAAEAAAAaAAAAFAD#####AQAAAAAQAAAAAAAAAAAAAABACAAAAAAAAAUAAgAAABoAAAAKAP####8BAAAAABAAAAAAAAAAAAAAAEAIAAAAAAAABQAAAAAcAAAAC#####8AAAABAAdDTWlsaWV1AP####8BAAAAABAAAAAAAAAAAAAAAEAIAAAAAAAABQAAAAADAAAABwAAABUA#####wEAAAAAEAAAAAAAAAAAAAAAQAgAAAAAAAAFAAAAAAIAAAAJAAAAAwD#####AQAAAAAQAAABAAEAAAAeAAAAH#####8AAAABAA9DU3ltZXRyaWVBeGlhbGUA#####wAAACAAAAAKAP####8BAAAAABAAAAAAAAAAAAAAAEAIAAAAAAAABQAAAAABAAAAIQAAABEA#####wAAAAAAAQAAAAUAAAAcAAAAAwAAAAcAAAAdAAAAHAAAAAIA#####wEAAP8AEAABRAAAAAAAAAAAAEAIAAAAAAAABQABQE#m27ewGkhATCw4otQh4AAAAAIA#####wEAAP8AEAABQQAAAAAAAAAAAEAIAAAAAAAABQABwGLAAAAAAABAco4UeuFHrgAAAAIA#####wEAAP8AEAABQgAAAAAAAAAAAEAIAAAAAAAABQABQEOatcqmnwBAaQHSHBCv+v####8AAAABAAlDTG9uZ3VldXIA#####wAAACUAAAAmAAAADAD#####ABJNZXN1cmUgZGUgbG9uZ3VldXIAAAAFAAAAAgAAAAIAAAADAAAAJP####8AAAABAAtDTWVkaWF0cmljZQAAAAAoAQAAAAAQAAABAAEAAAADAAAAJAAAABUAAAAAKAEAAAAAEAAAAQUAAAAAAwAAACT#####AAAAAgAJQ0NlcmNsZU9SAAAAACgBAAAAAAEAAAAqAAAAAUAwAAAAAAAAAQAAABMAAAAAKAAAACkAAAArAAAAFAAAAAAoAQAAAAAQAAABBQABAAAALAAAABcBAAAAKAAAAAMAAAAkAAAAEAEAAAAoAQAA#wEAAAAtEQAAAAAAAQAAAAEAAAABAAAAAAAAAAAAAAAAAQAAAC7#####AAAAAQAIQ1NlZ21lbnQA#####wEAAP8AEAAAAQABAAAAAwAAACT#####AAAAAQAHQ0NhbGN1bAD#####AAVtaW5pMQABMAAAAAEAAAAAAAAAAAAAABsA#####wAFbWF4aTEAATEAAAABP#AAAAAAAAAAAAAbAP####8ABW1pbmkyAAEwAAAAAQAAAAAAAAAAAAAAGwD#####AAVtYXhpMgABMQAAAAE#8AAAAAAAAAAAABEA#####wAAAAAAAQAAAAUAAAABAAAAAwAAAAIAAAANAAAAAQAAABIA#####wEAAAAAAQAAAAIAAAANAAAAEwD#####AAAABAAAADYAAAAUAP####8BAAAAABAAAAAAAAAAAAAAAEAIAAAAAAAABQACAAAANwAAABQA#####wEAAAAAEAAAAAAAAAAAAAAAQAgAAAAAAAAFAAEAAAA3AAAACgD#####AQAAAAAQAAAAAAAAAAAAAABACAAAAAAAAAUAAAAAOQAAAAsAAAAIAP####8BAAAAABAAAAEAAQAAAAEAAAANAAAAAAkA#####wAAADsAAAAKAP####8BAAAAABAAAAAAAAAAAAAAAEAIAAAAAAAABQAAAAA5AAAAPAAAABIA#####wEAAAAAAQAAADkAAAA9AAAAEwD#####AAAABAAAAD4AAAAUAP####8BAAAAABAAAAAAAAAAAAAAAEAIAAAAAAAABQACAAAAPwAAABQA#####wEAAAAAEAAAAAAAAAAAAAAAQAgAAAAAAAAFAAEAAAA#AAAACgD#####AQAAAAAQAAAAAAAAAAAAAABACAAAAAAAAAUAAAAAQQAAAAsAAAAKAP####8BAAAAABAAAAAAAAAAAAAAAEAIAAAAAAAABQAAAAANAAAAIQAAABEA#####wAAAAAAAQAAAAUAAAACAAAAOQAAADoAAAAJAAAAAgAAABEA#####wAAAAAAAQAAAAUAAAAHAAAAIgAAAEMAAAAJAAAABwAAAAcA#####wH#AAAAEAAAAAAAAAAAAAAAQAgAAAAAAAAFAAAAAAwAAAAFAAAACAD#####Af8AAAAQAAABAAEAAABGAAAAAQAAAAAJAP####8AAABHAAAABgD#####Af8AAAAQAAABAAEAAAAiAAAACAAAAAcA#####wH#AAAAEAAAAAAAAAAAAAAAQAgAAAAAAAAFAAAAAAUAAABJAAAAAgD#####AQAAAAAQAAFFAAAAAAAAAAAAQAgAAAAAAAAFAAFAcsG4swKnpEBki9huPO8wAAAACAD#####AQAA#wAQAAABAAEAAAANAAAAAQAAAAAJAP####8AAABMAAAAJ###########'
      case 'pr3': return 'TWF0aEdyYXBoSmF2YTEuMAAAABI+TMzNAAJmcv###wEA#wEAAAAAAAAAAANLAAAChwAAAQEAAAAAAAAAAQAAAFT#####AAAAAQAKQ0NhbGNDb25zdAD#####AAJwaQAWMy4xNDE1OTI2NTM1ODk3OTMyMzg0Nv####8AAAABAApDQ29uc3RhbnRlQAkh+1RELRj#####AAAAAQAKQ1BvaW50QmFzZQD#####AQAAAAAQAAAAAAAAAAAAAABACAAAAAAAAAUAAUBFgh4WgLIAQC#iqafCTpAAAAACAP####8BAAAAABAAAAAAAAAAAAAAAEAIAAAAAAAABQABQFdK73BjHPRASNmnQNp0DAAAAAIA#####wEAAAAAEAABQwAAAAAAAAAAAEAIAAAAAAAABQABQD56W8feoABASRdhuPO8vP####8AAAABAAlDRHJvaXRlQUIA#####wEAAAAAEAAAAQABAAAAAwAAAAL#####AAAAAQAWQ0Ryb2l0ZVBlcnBlbmRpY3VsYWlyZQD#####AQAAAAAQAAABAAEAAAADAAAABAAAAAQA#####wEAAAAAEAAAAQABAAAAAgAAAAT#####AAAAAQAPQ1BvaW50TGllRHJvaXRlAP####8BAAAAABAAAAAAAAAAAAAAAEAIAAAAAAAABQABP+QsDC2fc+wAAAAF#####wAAAAEAEENEcm9pdGVQYXJhbGxlbGUA#####wEAAAAAEAAAAQABAAAABwAAAAT#####AAAAAQAQQ0ludERyb2l0ZURyb2l0ZQD#####AQAAAAAQAAAAAAAAAAAAAABACAAAAAAAAAUAAAAABgAAAAj#####AAAAAQAIQ1ZlY3RldXIA#####wEAAAAAEAAAAQABAAAAAwAAAAcA#####wAAAAEADUNUcmFuc1BhclZlY3QA#####wAAAAoAAAAGAP####8B#wAAABAAAAEAAQAAAAEAAAAEAAAABQD#####AQAAAAAQAAAAAAAAAAAAAABACAAAAAAAAAUAAT#Rt96tMPCpAAAADP####8AAAABAAtDUG9pbnRJbWFnZQD#####Af8AAAAQAAFPAAAAAAAAAAAAQAgAAAAAAAAFAAAAAAEAAAALAAAACgD#####Af8AAAAQAAFOAAAAAAAAAAAAQAgAAAAAAAAFAAAAAA0AAAAL#####wAAAAEADUNEZW1pRHJvaXRlT0EA#####wEAAP8AEAAAAQABAAAACQAAAA8AAAAFAP####8BAAD#ABAAAU0AAAAAAAAAAABACAAAAAAAAAUAAUAKPrb2DI5cAAAAEP####8AAAABABRDSW1wbGVtZW50YXRpb25Qcm90bwD#####ABtNZXN1cmUgZCdhbmdsZSBub24gb3JpZW50w6kAAAACAAAAAwAAAAMAAAARAAAADwAAAA7#####AAAAAQAMQ0Jpc3NlY3RyaWNlAAAAABIBAAAAABAAAAEAAQAAABEAAAAPAAAADgAAAAUAAAAAEgEAAAAAEAAAAQUAAUBvJahlrHGpAAAAE#####8AAAABABdDTWVzdXJlQW5nbGVHZW9tZXRyaXF1ZQEAAAASAAAAEQAAAA8AAAAO#####wAAAAIAF0NNYXJxdWVBbmdsZUdlb21ldHJpcXVlAQAAABIBAAD#AAEAAAABQEUGF39UkbsAAAARAAAADwAAAA7#####AAAAAQAPQ1ZhbGV1ckFmZmljaGVlAQAAABIBAAD#AEAIAAAAAAAAAAAAAAAAAAAAAAAUDwAAAAAAAQAAAAEAAAABAAAAAAAAAAAAAAACwrAAAAAAFf####8AAAABAAlDUG9seWdvbmUA#####wAAAAAAAQAAAAUAAAADAAAAAgAAAAkAAAAHAAAAA#####8AAAABAAlDQ2VyY2xlT0EA#####wEAAAAAAQAAAAMAAAAB#####wAAAAEAEENJbnREcm9pdGVDZXJjbGUA#####wAAAAQAAAAZ#####wAAAAEAEENQb2ludExpZUJpcG9pbnQA#####wEAAAAAEAAAAAAAAAAAAAAAQAgAAAAAAAAFAAEAAAAaAAAAFAD#####AQAAAAAQAAAAAAAAAAAAAABACAAAAAAAAAUAAgAAABoAAAAKAP####8BAAAAABAAAAAAAAAAAAAAAEAIAAAAAAAABQAAAAAcAAAAC#####8AAAABAAdDTWlsaWV1AP####8BAAAAABAAAAAAAAAAAAAAAEAIAAAAAAAABQAAAAADAAAABwAAABUA#####wEAAAAAEAAAAAAAAAAAAAAAQAgAAAAAAAAFAAAAAAIAAAAJAAAAAwD#####AQAAAAAQAAABAAEAAAAeAAAAH#####8AAAABAA9DU3ltZXRyaWVBeGlhbGUA#####wAAACAAAAAKAP####8BAAAAABAAAAAAAAAAAAAAAEAIAAAAAAAABQAAAAABAAAAIQAAABEA#####wEAAAAAAQAAAAUAAAAcAAAAAwAAAAcAAAAdAAAAHAAAAAIA#####wEAAP8AEAABRAAAAAAAAAAAAEAIAAAAAAAABQABQEhm27ewGkhATaw4otQh4AAAAAIA#####wEAAP8AEAABQQAAAAAAAAAAAEAIAAAAAAAABQABwGSgAAAAAABAcr4UeuFHrgAAAAIA#####wEAAP8AEAABQgAAAAAAAAAAAEAIAAAAAAAABQABQDg1a5VNPgBAaWHSHBCv+v####8AAAABAAlDTG9uZ3VldXIA#####wAAACUAAAAmAAAADAD#####ABJNZXN1cmUgZGUgbG9uZ3VldXIAAAAFAAAAAgAAAAIAAAADAAAAJP####8AAAABAAtDTWVkaWF0cmljZQAAAAAoAQAAAAAQAAABAAEAAAADAAAAJAAAABUAAAAAKAEAAAAAEAAAAQUAAAAAAwAAACT#####AAAAAgAJQ0NlcmNsZU9SAAAAACgBAAAAAAEAAAAqAAAAAUAwAAAAAAAAAQAAABMAAAAAKAAAACkAAAArAAAAFAAAAAAoAQAAAAAQAAABBQABAAAALAAAABcBAAAAKAAAAAMAAAAkAAAAEAEAAAAoAQAA#wEAAAAtEQAAAAAAAQAAAAEAAAABAAAAAAAAAAAAAAAAAQAAAC7#####AAAAAQAIQ1NlZ21lbnQA#####wEAAP8AEAAAAQABAAAAAwAAACT#####AAAAAQAHQ0NhbGN1bAD#####AAVtaW5pMQABMAAAAAEAAAAAAAAAAAAAABsA#####wAFbWF4aTEAATEAAAABP#AAAAAAAAAAAAAbAP####8ABW1pbmkyAAEwAAAAAQAAAAAAAAAAAAAAGwD#####AAVtYXhpMgABMQAAAAE#8AAAAAAAAAAAABEA#####wAAAAAAAQAAAAUAAAABAAAAAwAAAAIAAAANAAAAAQAAABIA#####wEAAAAAAQAAAAIAAAANAAAAEwD#####AAAABAAAADYAAAAUAP####8BAAAAABAAAAAAAAAAAAAAAEAIAAAAAAAABQACAAAANwAAABQA#####wEAAAAAEAAAAAAAAAAAAAAAQAgAAAAAAAAFAAEAAAA3AAAACgD#####AQAAAAAQAAAAAAAAAAAAAABACAAAAAAAAAUAAAAAOQAAAAsAAAAIAP####8BAAAAABAAAAEAAQAAAAEAAAANAAAAAAkA#####wAAADsAAAAKAP####8BAAAAABAAAAAAAAAAAAAAAEAIAAAAAAAABQAAAAA5AAAAPAAAABIA#####wEAAAAAAQAAADkAAAA9AAAAEwD#####AAAABAAAAD4AAAAUAP####8BAAAAABAAAAAAAAAAAAAAAEAIAAAAAAAABQACAAAAPwAAABQA#####wEAAAAAEAAAAAAAAAAAAAAAQAgAAAAAAAAFAAEAAAA#AAAACgD#####AQAAAAAQAAAAAAAAAAAAAABACAAAAAAAAAUAAAAAQQAAAAsAAAAKAP####8BAAAAABAAAAAAAAAAAAAAAEAIAAAAAAAABQAAAAANAAAAIQAAABEA#####wAAAAAAAQAAAAUAAAACAAAAOQAAADoAAAAJAAAAAgAAABEA#####wAAAAAAAQAAAAUAAAA5AAAAQQAAAEIAAAA6AAAAOQAAABEA#####wAAAAAAAQAAAAUAAAAHAAAAIgAAAEMAAAAJAAAABwAAAAcA#####wH#AAAAEAAAAAAAAAAAAAAAQAgAAAAAAAAFAAAAAAwAAAAFAAAACAD#####Af8AAAAQAAABAAEAAABHAAAAAQAAAAAJAP####8AAABIAAAABgD#####Af8AAAAQAAABAAEAAAAiAAAACAAAAAcA#####wH#AAAAEAAAAAAAAAAAAAAAQAgAAAAAAAAFAAAAAAUAAABKAAAAAgD#####AQAAAAAQAAFFAAAAAAAAAAAAQAgAAAAAAAAFAAFAcdG4swKnpEBk69huPO8wAAAACAD#####AQAA#wAQAAABAAEAAAANAAAAAQAAAAAJAP####8AAABNAAAAFQD#####AQAA#wAQAAAAAAAAAAAAAABACAAAAAAAAAUAAAAAAwAAABwAAAAVAP####8BAAD#ABAAAAAAAAAAAAAAAEAIAAAAAAAABQAAAAAHAAAAHQAAABUA#####wEAAP8AEAAAAAAAAAAAAAAAQAgAAAAAAAAFAAAAAAMAAABPAAAAFQD#####AQAA#wAQAAAAAAAAAAAAAABACAAAAAAAAAUAAAAABwAAAFAAAAARAP####8AAAAAAAEAAAAFAAAAAwAAAFEAAABSAAAABwAAAAMAAAAn##########8='
      case 'pr4': return 'TWF0aEdyYXBoSmF2YTEuMAAAABM+TMzNAAJmcv###wEA#wEAAAAAAAAAAANLAAAChwAAAQEAAAAAAAAAAQAAANX#####AAAAAQAKQ0NhbGNDb25zdAD#####AAJwaQAWMy4xNDE1OTI2NTM1ODk3OTMyMzg0Nv####8AAAABAApDQ29uc3RhbnRlQAkh+1RELRj#####AAAAAQAKQ1BvaW50QmFzZQD#####AQAAAAAQAAFHAAAAAAAAAAAAQAgAAAAAAAAAAAUAAUBL1X30ynCwQEEJfR0IzrAAAAACAP####8BAAAAABAAAUMAAAAAAAAAAABACAAAAAAAAAAABQABQEgAAAAAAAFAT#Cj1wo9cQAAAAIA#####wEAAAAAEAABSAAAAAAAAAAAAEAIAAAAAAAAAAAFAAFAWsk#J1g6HEBPrjXTpAXY#####wAAAAEACUNQb2x5Z29uZQD#####AQAAAAAAAAEAAAAEAAAAAQAAAAIAAAADAAAAAf####8AAAABAAlDRHJvaXRlQUIA#####wEAAAAAEAAAAQAAAAEAAAACAAAAA#####8AAAABABZDRHJvaXRlUGVycGVuZGljdWxhaXJlAP####8BAAAAABAAAAEAAAABAAAAAgAAAAUAAAAFAP####8BAAAAABAAAAEAAAABAAAAAwAAAAX#####AAAAAQAPQ1BvaW50TGllRHJvaXRlAP####8BAAAAABAAAAAAAAAAAAAAAEAIAAAAAAAAAAAFAAE#5UaqyzGQ#gAAAAb#####AAAAAQAIQ1ZlY3RldXIA#####wEAAAAAEAAAAQAAAAEAAAACAAAAAwD#####AAAAAQANQ1RyYW5zUGFyVmVjdAD#####AAAACf####8AAAABAAtDUG9pbnRJbWFnZQD#####AQAAAAAQAAAAAAAAAAAAAABACAAAAAAAAAAABQAAAAAIAAAACgAAAAMA#####wEAAAAAAAABAAAABQAAAAIAAAADAAAACwAAAAgAAAAC#####wAAAAEACUNDZXJjbGVPQQD#####AQAAAAAAAAEAAAADAAAAAf####8AAAABABBDSW50RHJvaXRlQ2VyY2xlAP####8AAAAFAAAADf####8AAAABABBDUG9pbnRMaWVCaXBvaW50AP####8BAAAAABAAAAAAAAAAAAAAAEAIAAAAAAAAAAAFAAIAAAAOAAAADAD#####AQAAAAAQAAAAAAAAAAAAAABACAAAAAAAAAAABQABAAAADgAAAAUA#####wEAAAAAEAAAAQAAAAEAAAAQAAAABQAAAAQA#####wEAAAAAEAAAAQAAAAEAAAAIAAAAC#####8AAAABABBDSW50RHJvaXRlRHJvaXRlAP####8BAAAAABAAAAAAAAAAAAAAAEAIAAAAAAAAAAAFAAAAABEAAAASAAAAAwD#####AQAAAAAAAAEAAAAFAAAAAwAAABAAAAATAAAACwAAAAMAAAAKAP####8BAAAAAAAAAQAAAAIAAAABAAAACwD#####AAAABQAAABUAAAAMAP####8BAAAAABAAAAAAAAAAAAAAAEAIAAAAAAAAAAAFAAEAAAAWAAAADAD#####AQAAAAAQAAFJAAAAAAAAAAAAQAgAAAAAAAAAAAUAAgAAABYAAAAFAP####8BAAAAABAAAAEAAAABAAAAGAAAAAUAAAANAP####8BAAAAABAAAUUAAAAAAAAAAABACAAAAAAAAAAABQAAAAAZAAAAEgAAAAMA#####wEAAAAAAAABAAAABQAAABgAAAACAAAACAAAABoAAAAY#####wAAAAEAB0NNaWxpZXUA#####wEAAAAAEAAAAAAAAAAAAAAAQAgAAAAAAAAAAAUAAAAAAgAAAAgAAAAOAP####8BAAAAABAAAAAAAAAAAAAAAEAIAAAAAAAAAAAFAAAAAAMAAAALAAAABAD#####AQAAAAAQAAABAAAAAQAAABwAAAAd#####wAAAAEAD0NTeW1ldHJpZUF4aWFsZQD#####AAAAHgAAAAkA#####wEAAAAAEAAAAAAAAAAAAAAAQAgAAAAAAAAAAAUAAAAAAQAAAB8AAAADAP####8BAAAAAAAAAQAAAAQAAAAIAAAAIAAAAAsAAAAIAAAAAgD#####AQAA#wEQAAFEAAAAAAAAAAAAQAgAAAAAAAAAAAUAAUBRXtErIyFwQFP08THhLoAAAAACAP####8AAAD#ABAAAUEAAAAAAAAAAABACAAAAAAAAAAABQABQIPwAAAAAABAjS8KPXCj2AAAAAIA#####wAAAP8AEAABQgAAAAAAAAAAAEAIAAAAAAAAAAAFAAFAWD7RKyMhdEB8RTxMeEug#####wAAAAEACUNMb25ndWV1cgD#####AAAAIwAAACT#####AAAAAQAUQ0ltcGxlbWVudGF0aW9uUHJvdG8A#####wASTWVzdXJlIGRlIGxvbmd1ZXVyAAAABQAAAAIAAAACAAAAAgAAACL#####AAAAAQALQ01lZGlhdHJpY2UAAAAAJgEAAAAAEAAAAQAAAAEAAAACAAAAIgAAAA4AAAAAJgEAAAAAEAAAAQAABQAAAAACAAAAIv####8AAAACAAlDQ2VyY2xlT1IAAAAAJgEAAAAAAAABAAAAKAAAAAFAMAAAAAAAAAEAAAALAAAAACYAAAAnAAAAKQAAAAwAAAAAJgEAAAAAEAAAAQAABQABAAAAKgAAABABAAAAJgAAAAIAAAAi#####wAAAAEAD0NWYWxldXJBZmZpY2hlZQEAAAAmAQAA#wEAAAAAACsRAAAAAAABAAAAAQAAAAEAAAAAAAAAAAAAAAABAAAALAAAAAIA#####wAAAAAAEAAAAEAIAAAAAAAAAAAAAAAAAAAAAAUAAEBjQAAAAAAAQH1OFHrhR67#####AAAAAQAHQ0NhbGN1bAD#####AAVtaW5pMQABMAAAAAEAAAAAAAAAAAAAABUA#####wAFbWF4aTEAATEAAAABP#AAAAAAAAAAAAARAP####8AB0N1cnNldXIAAAAFAAAABQAAAAMAAAAvAAAAMAAAAC7#####AAAAAQAUQ0Ryb2l0ZURpcmVjdGlvbkZpeGUAAAAAMQEAAAAAEAAAAQAAAAEAAAAuAT#wAAAAAAAAAAAABgEAAAAxAAAA#wAQAAAAwAgAAAAAAAA#8AAAAAAAAAAABQAAQF1AAAAAAAAAAAAy#####wAAAAEAC0NIb21vdGhldGllAAAAADEAAAAu#####wAAAAEACkNPcGVyYXRpb24D#####wAAAAEAD0NSZXN1bHRhdFZhbGV1cgAAAC8AAAAYAQAAABkAAAAvAAAAGQAAADAAAAAJAAAAADEBAAAAAA0AAk8xAMAQAAAAAAAAQBAAAAAAAAAAAAUAAAAAMwAAADQAAAAXAAAAADEAAAAuAAAAGAMAAAAYAQAAAAE#8AAAAAAAAAAAABkAAAAvAAAAGAEAAAAZAAAAMAAAABkAAAAvAAAACQAAAAAxAQAAAAANAAJJMQDAAAAAAAAAAEAIAAAAAAAAAAAFAAAAADMAAAA2#####wAAAAEACENTZWdtZW50AQAAADEAAAD#ABAAAAEAAAABAAAALgAAADMAAAAGAQAAADEAAAD#ARAAAmsxAMAAAAAAAAAAQAAAAAAAAAAAAAEAAQAAAAAAAAAAAAAAOP####8AAAACAA9DTWVzdXJlQWJzY2lzc2UBAAAAMQABYQAAADUAAAA3AAAAOQAAABQBAAAAMQAAAP8AwC4AAAAAAADAJgAAAAAAAAAAAAAAOQ8AAf###wAAAAEAAAACAAAAAQAAAAAAAAAAAAAAAAIAAAA6AAAAAgD#####AAAAAAAQAAAAQAgAAAAAAAAAAAAAAAAAAAAABQAAQHWgAAAAAABAfV4UeuFHrgAAABUA#####wAFbWluaTIAATAAAAABAAAAAAAAAAAAAAAVAP####8ABW1heGkyAAExAAAAAT#wAAAAAAAAAAAAEQD#####AAdDdXJzZXVyAAAABQAAAAUAAAADAAAAPQAAAD4AAAA8AAAAFgAAAAA#AQAAAAAQAAABAAAAAQAAADwBP#AAAAAAAAAAAAAGAQAAAD8A#wAAABAAAADACAAAAAAAAD#wAAAAAAAAAAAFAABAXUAAAAAAAAAAAEAAAAAXAAAAAD8AAAA8AAAAGAMAAAAZAAAAPQAAABgBAAAAGQAAAD0AAAAZAAAAPgAAAAkAAAAAPwEAAAAADQACTzEAwBAAAAAAAABAEAAAAAAAAAAABQAAAABBAAAAQgAAABcAAAAAPwAAADwAAAAYAwAAABgBAAAAAT#wAAAAAAAAAAAAGQAAAD0AAAAYAQAAABkAAAA+AAAAGQAAAD0AAAAJAAAAAD8BAAAAAA0AAkkxAMAAAAAAAAAAQAgAAAAAAAAAAAUAAAAAQQAAAEQAAAAaAQAAAD8A#wAAABAAAAEAAAABAAAAPAAAAEEAAAAGAQAAAD8A#wAAARAAAWsAwAAAAAAAAABAAAAAAAAAAAAAAQABAAAAAAAAAAAAAABGAAAAGwEAAAA#AAFiAAAAQwAAAEUAAABHAAAAFAEAAAA#AP8AAAAAAAAAAAAAAMAYAAAAAAAAAAAAAABHDwAB####AAAAAQAAAAIAAAABAAAAAAAAAAAAAAAAAgAAAEgAAAAVAP####8AAmFhAAFhAAAAGQAAADoAAAAVAP####8AAmJiAAFiAAAAGQAAAEgAAAATAP####8B#wAAAAAAAQAAAAIAAAAYAgAAABkAAAAsAAAAGQAAAEoAAAAAGgD#####Af8AAAAQAAABAAAAAQAAAAIAAAAiAAAACwD#####AAAATQAAAEwAAAAMAP####8B#wAAABAAAAAAAAAAAAAAAEAIAAAAAAAAAAAFAAEAAABOAAAADAD#####AP8AAAAQAAAAAAAAAAAAAABACAAAAAAAAAAABQACAAAATgAAAAcA#####wH#AAAAEAAAAQAAAAEAAAAIAAAATwAAAAAIAP####8AAABRAAAACQD#####Af8AAAAQAAFGAAAAAAAAAAAAQAgAAAAAAAAAAAUAAAAAGgAAAFIAAAAJAP####8B#wAAABAAAAAAAAAAAAAAAEAIAAAAAAAAAAAFAAAAAAsAAABSAAAACQD#####Af8AAAAQAAAAAAAAAAAAAABACAAAAAAAAAAABQAAAAATAAAAUgAAAAMA#####wH#AAAAAAABAAAABQAAABoAAABTAAAATwAAAAgAAAAaAAAAAwD#####AQAAAAAAAAEAAAAFAAAATwAAAFQAAAALAAAACAAAAE8AAAADAP####8B#wAAAAAAAQAAAAUAAABUAAAAVQAAABMAAAALAAAAVAAAAAQA#####wH#AAAAEAAAAQAAAAEAAAAaAAAAU#####8AAAABABBDRHJvaXRlUGFyYWxsZWxlAP####8B#wAAABAAAAEAAAABAAAAAQAAAAUAAAANAP####8B#wAAABAAAAAAAAAAAAAAAEAIAAAAAAAAAAAFAAAAAFoAAAAGAAAADQD#####Af8AAAAQAAAAAAAAAAAAAABACAAAAAAAAAAABQAAAAAHAAAAWgAAAAQA#####wH#AAAAEAAAAQAAAAEAAAAIAAAATwAAAAQA#####wH#AAAAEAAAAQAAAAEAAAALAAAAVAAAAAQA#####wH#AAAAEAAAAQAAAAEAAAACAAAATwAAABwA#####wH#AAAAEAAAAQAAAAEAAABbAAAAXwAAAA0A#####wEAAP8AEAAAAAAAAAAAAAAAQAgAAAAAAAAAAAUAAAAAXQAAAGAAAAAHAP####8BAAD#ABAAAAEAAAABAAAACAAAAAsAAAAACAD#####AAAAYgAAAAkA#####wEAAP8AEAAAAAAAAAAAAAAAQAgAAAAAAAAAAAUAAAAAYQAAAGMAAAAHAP####8BAAD#ABAAAAEAAAABAAAAWwAAAAEAAAAACAD#####AAAAZQAAAAkA#####wH#AAAAEAAAAAAAAAAAAAAAQAgAAAAAAAAAAAUAAAAAYQAAAGYAAAADAP####8B#wAAAAAAAQAAAAQAAABPAAAAZwAAAFQAAABPAAAAHAD#####AQAA#wAQAAABAAAAAQAAACAAAAAFAAAADQD#####AQAA#wAQAAAAAAAAAAAAAABACAAAAAAAAAAABQAAAAAGAAAAaQAAABwA#####wEAAP8AEAAAAQAAAAEAAABqAAAAXwAAAA0A#####wEAAP8AEAAAAAAAAAAAAAAAQAgAAAAAAAAAAAUAAAAAXQAAAGsAAAAHAP####8BAAD#ABAAAAEAAAABAAAAagAAACAAAAAACAD#####AAAAbQAAAAkA#####wH#AAAAEAAAAAAAAAAAAAAAQAgAAAAAAAAAAAUAAAAAbAAAAG4AAAADAP####8B#wAAAAAAAQAAAAQAAAAIAAAAbwAAAAsAAAAIAAAADwD#####AAAAEgAAAAkA#####wEAAP8AEAAAAAAAAAAAAAAAQAgAAAAAAAAAAAUAAAAAIAAAAHEAAAACAP####8BAAAAABAAAUoAAAAAAAAAAABACAAAAAAAAAAABQABQGi0#BvsumJAZml++JjwmAAAAAcA#####wEAAAAAEAAAAQAAAAEAAABvAAAAcwAAAAAIAP####8AAAB0AAAACQD#####AQAAAAAQAAFMAAAAAAAAAAAAQAgAAAAAAAAAAAUAAAAAcgAAAHUAAAASAP####8BAAAAABAAAAEAAAABAAAAdgAAAHMAAAAGAP####8BAAAAABAAAUsAAAAAAAAAAABACAAAAAAAAAAABQABv#teNhpSRlwAAAB3AAAAEQD#####ABtNZXN1cmUgZCdhbmdsZSBub24gb3JpZW50w6kAAAACAAAAAwAAAAMAAABzAAAAeAAAAHb#####AAAAAQAMQ0Jpc3NlY3RyaWNlAAAAAHkBAAAAABAAAAEAAAABAAAAcwAAAHgAAAB2AAAABgAAAAB5AQAAAAAQAAABAAAFAAFAbyWoZaxxqQAAAHr#####AAAAAQAXQ01lc3VyZUFuZ2xlR2VvbWV0cmlxdWUBAAAAeQAAAHMAAAB4AAAAdv####8AAAACABdDTWFycXVlQW5nbGVHZW9tZXRyaXF1ZQEAAAB5AQAAAAAAAAEAAAABQEUGF39UkbsAAABzAAAAeAAAAHYAAAAUAQAAAHkBAAAAAEAIAAAAAAAAAAAAAAAAAAAAAAAAAHsPAAAAAAABAAAAAQAAAAEAAAAAAAAAAAAAAALCsAAAAAB8#####wAAAAEACUNSb3RhdGlvbgD#####AAAAeP####8AAAABAAxDTW9pbnNVbmFpcmUAAAAYAgAAABkAAABLAAAAGQAAAHwAAAAJAP####8BAAAAABAAAAAAAAAAAAAAAEAIAAAAAAAAAAAFAAAAAHMAAAB#AAAABwD#####AQAAAAAQAAABAAAAAQAAAHMAAABvAAAAAAgA#####wAAAIEAAAAJAP####8BAAD#ABAAAAAAAAAAAAAAAEAIAAAAAAAAAAAFAAAAAIAAAACCAAAAAwD#####AQAAAAAAAAEAAAAEAAAAgwAAAAgAAAALAAAAgwAAAAkA#####wEAAP8AEAAAAAAAAAAAAAAAQAgAAAAAAAAAAAUAAAAAcgAAAFIAAAAJAP####8BAAD#ABAAAU0AAAAAAAAAAABACAAAAAAAAAAABQAAAABnAAAAdQAAAAkA#####wEAAP8AEAABTwAAAAAAAAAAAEAIAAAAAAAAAAAFAAAAAIUAAAB1AAAAEgD#####AQAA#wAQAAABAAAAAQAAAIcAAACGAAAABgD#####AQAA#wAQAAFOAAAAAAAAAAAAQAgAAAAAAAAAAAUAAUASup8pl1Z0AAAAiAAAABEA#####wAbTWVzdXJlIGQnYW5nbGUgbm9uIG9yaWVudMOpAAAAAgAAAAMAAAADAAAAhgAAAIkAAACHAAAAHQAAAACKAQAAAAAQAAABAAAAAQAAAIYAAACJAAAAhwAAAAYAAAAAigEAAAAAEAAAAQAABQABQG8lqGWscakAAACLAAAAHgEAAACKAAAAhgAAAIkAAACHAAAAHwEAAACKAQAA#wAAAAEAAAABQEUGF39UkbsAAACGAAAAiQAAAIcAAAAUAQAAAIoBAAD#AEAIAAAAAAAAAAAAAAAAAAAAAAAAAIwPAAAAAAABAAAAAQAAAAEAAAAAAAAAAAAAAALCsAAAAACNAAAAIAD#####AAAAiQAAABgCAAAAGQAAAEsAAAAZAAAAjQAAAAkA#####wEAAP8AEAAAAAAAAAAAAAAAQAgAAAAAAAAAAAUAAAAAhgAAAJAAAAAJAP####8BAAD#ABAAAAAAAAAAAAAAAEAIAAAAAAAAAAAFAAAAAJEAAACCAAAAAwD#####AQAAAAAAAAEAAAAEAAAATwAAAJIAAABUAAAATwAAAAkA#####wEAAP8AEAABUAAAAAAAAAAAAEAIAAAAAAAAAAAFAAAAABoAAAB1AAAAEgD#####AQAA#wAQAAABAAAAAQAAAJQAAAB2AAAABgD#####AQAA#wAQAAFRAAAAAAAAAAAAQAgAAAAAAAAAAAUAAT#4bfyfN4BHAAAAlQAAABEA#####wAbTWVzdXJlIGQnYW5nbGUgbm9uIG9yaWVudMOpAAAAAgAAAAMAAAADAAAAlAAAAJYAAAB2AAAAHQAAAACXAQAAAAAQAAABAAAAAQAAAJQAAACWAAAAdgAAAAYAAAAAlwEAAAAAEAAAAQAABQABQG8lqGWscakAAACYAAAAHgEAAACXAAAAlAAAAJYAAAB2AAAAHwEAAACXAQAA#wAAAAEAAAABQEUGF39UkbsAAACUAAAAlgAAAHYAAAAUAQAAAJcBAAD#AEAIAAAAAAAAAAAAAAAAAAAAAAAAAJkPAAAAAAABAAAAAQAAAAEAAAAAAAAAAAAAAALCsAAAAACaAAAAIAD#####AAAAlgAAABgCAAAAGAEAAAABP#AAAAAAAAAAAAAZAAAASwAAABkAAACaAAAACQD#####AQAA#wAQAAAAAAAAAAAAAABACAAAAAAAAAAABQAAAAB2AAAAnQAAAAkA#####wEAAP8AEAAAAAAAAAAAAAAAQAgAAAAAAAAAAAUAAAAAngAAAIIAAAAJAP####8BAAD#ABAAAAAAAAAAAAAAAEAIAAAAAAAAAAAFAAAAAJ8AAABSAAAAAwD#####AQAAAAAAAAEAAAAFAAAACAAAAJ8AAACgAAAATwAAAAgAAAAJAP####8BAAD#ABAAAVIAAAAAAAAAAABACAAAAAAAAAAABQAAAAATAAAAdQAAABIA#####wEAAP8AEAAAAQAAAAEAAAB2AAAAogAAAAYA#####wEAAP8AEAABUwAAAAAAAAAAAEAIAAAAAAAAAAAFAAE#7bivWu8##gAAAKMAAAARAP####8AG01lc3VyZSBkJ2FuZ2xlIG5vbiBvcmllbnTDqQAAAAIAAAADAAAAAwAAAKIAAACkAAAAdgAAAB0AAAAApQEAAAAAEAAAAQAAAAEAAACiAAAApAAAAHYAAAAGAAAAAKUBAAAAABAAAAEAAAUAAUBvJahlrHGpAAAApgAAAB4BAAAApQAAAKIAAACkAAAAdgAAAB8BAAAApQEAAP8AAAABAAAAAUBFBhd#VJG7AAAAogAAAKQAAAB2AAAAFAEAAAClAQAA#wBACAAAAAAAAAAAAAAAAAAAAAAAAACnDwAAAAAAAQAAAAEAAAABAAAAAAAAAAAAAAACwrAAAAAAqAAAACAA#####wAAAKQAAAAYAgAAABkAAABLAAAAGQAAAKgAAAAJAP####8BAAD#ABAAAAAAAAAAAAAAAEAIAAAAAAAAAAAFAAAAAKIAAACrAAAACQD#####AQAA#wAQAAAAAAAAAAAAAABACAAAAAAAAAAABQAAAACsAAAAggAAAAkA#####wEAAP8AEAAAAAAAAAAAAAAAQAgAAAAAAAAAAAUAAAAArQAAAFIAAAADAP####8BAAAAAAAAAQAAAAUAAACuAAAArQAAAAsAAABUAAAArv####8AAAABABBDU3VyZmFjZVBvbHlnb25lAP####8B####AANTNzYAAAAEAAAAGwAAACIA#####wH###8AA1M3NwAAAAQAAAAMAAAAIgD#####Af###wADUzc4AAAABAAAABQAAAAiAP####8B####AANTNzkAAAAEAAAABAAAACIA#####wH###8AA1M4MAAAAAQAAAAhAAAAGgD#####AAAAAAAQAAABAANTODYAAQAAAAEAAAAPAAAAGgD#####AAAAAAAQAAABAANTODcAAQAAAAEAAAADAAAAGgD#####AAAAAAAQAAABAANTODgAAQAAABgAAAAPAAAAGgD#####AAAAAAAQAAABAANTODkAAQAAAA8AAAADAAAAGgD#####AAAAAAAQAAABAANTOTAAAQAAAAMAAAAQAAAAGgD#####AAAAAAAQAAABAANTOTEAAQAAABgAAAAaAAAAGgD#####AAAAAAAQAAABAANTOTIAAQAAAA8AAAAIAAAAGgD#####AAAAAAAQAAABAANTOTMAAQAAAAMAAAALAAAAGgD#####AAAAAAAQAAABAANTOTQAAQAAABAAAAATAAAAGgD#####AAAAAAAQAAABAANTOTUAAQAAABoAAAAIAAAAGgD#####AAAAAAAQAAABAANTOTcAAQAAAAsAAAATAAAAGgD#####AAAAAAAQAAABAANTOTgAAQAAAAgAAAAgAAAAGgD#####AAAAAAAQAAABAANTOTkAAQAAACAAAAALAAAAIgD#####Af###wADUzg0AAAABAAAAJMAAAAaAP####8BAAAAABAAAAEAA1MwMAABAAAAkgAAAE8AAAAaAP####8BAAAAABAAAAEAA1MwMQABAAAAkgAAAFQAAAAaAP####8BAAAAABAAAAEAA1MwMwABAAAATwAAAFQAAAAaAP####8BAAAAABAAAAEAA1MwMgABAAAAoAAAAE8AAAAaAP####8BAAAAABAAAAEAA1MwNAABAAAAVAAAAK4AAAAiAP####8B####AANTODIAAAAEAAAAVwAAACIA#####wH###8AA1M4MQAAAAQAAAChAAAAIgD#####Af###wADUzgzAAAABAAAAK8AAAAaAP####8BAAAAABAAAAEAA1MwNQABAAAAoAAAAJ8AAAAaAP####8BAAAAABAAAAEAA1MwNgABAAAATwAAAAgAAAAaAP####8BAAAAABAAAAEAA1MwNwABAAAAVAAAAAsAAAAaAP####8BAAAAABAAAAEAA1MwOAABAAAArgAAAK0AAAAaAP####8BAAAAABAAAAEAA1MwOQABAAAAnwAAAAgAAAAaAP####8AAAAAABAAAAEAA1M5NgABAAAACAAAAAsAAAAaAP####8BAAAAABAAAAEAA1MxMAABAAAACwAAAK0AAAAiAP####8B####AANTODUAAAAEAAAAhAAAABoA#####wEAAAAAEAAAAQADUzExAAEAAAAIAAAAgwAAABoA#####wEAAAAAEAAAAQADUzEyAAEAAACDAAAACwAAACX##########w=='
      case 'pr5': return 'TWF0aEdyYXBoSmF2YTEuMAAAABI+TMzNAAJmcv###wEA#wEAAAAAAAAAAAM7AAACdwAAAQEAAAAAAAAAAQAAADf#####AAAAAQAKQ0NhbGNDb25zdAD#####AAJwaQAWMy4xNDE1OTI2NTM1ODk3OTMyMzg0Nv####8AAAABAApDQ29uc3RhbnRlQAkh+1RELRj#####AAAAAQAKQ1BvaW50QmFzZQD#####AQAAAAAQAAFHAAAAAAAAAAAAQAgAAAAAAAAFAAFASNV99MpwsEBBiX0dCM6wAAAAAgD#####AQAAAAAQAAFDAAAAAAAAAAAAQAgAAAAAAAAFAAFARHrKKMNqwEBQFxrp0gLsAAAAAgD#####AQAAAAAQAAFIAAAAAAAAAAAAQAgAAAAAAAAFAAFAWUk#J1g6HEBQFxrp0gLs#####wAAAAEACUNQb2x5Z29uZQD#####AAAAAAABAAAABAAAAAEAAAACAAAAAwAAAAH#####AAAAAQAJQ0Ryb2l0ZUFCAP####8BAAAAABAAAAEAAQAAAAIAAAAD#####wAAAAEAFkNEcm9pdGVQZXJwZW5kaWN1bGFpcmUA#####wEAAAAAEAAAAQABAAAAAgAAAAUAAAAFAP####8BAAAAABAAAAEAAQAAAAMAAAAF#####wAAAAEAD0NQb2ludExpZURyb2l0ZQD#####AQAAAAAQAAAAAAAAAAAAAABACAAAAAAAAAUAAT#lRqrLMZD+AAAABv####8AAAABAAhDVmVjdGV1cgD#####AQAAAAAQAAABAAEAAAACAAAAAwD#####AAAAAQANQ1RyYW5zUGFyVmVjdAD#####AAAACf####8AAAABAAtDUG9pbnRJbWFnZQD#####AQAAAAAQAAAAAAAAAAAAAABACAAAAAAAAAUAAAAACAAAAAoAAAADAP####8AAAAAAAEAAAAFAAAAAgAAAAMAAAALAAAACAAAAAL#####AAAAAQAJQ0NlcmNsZU9BAP####8BAAAAAAEAAAADAAAAAf####8AAAABABBDSW50RHJvaXRlQ2VyY2xlAP####8AAAAFAAAADf####8AAAABABBDUG9pbnRMaWVCaXBvaW50AP####8BAAAAABAAAAAAAAAAAAAAAEAIAAAAAAAABQACAAAADgAAAAwA#####wEAAAAAEAAAAAAAAAAAAAAAQAgAAAAAAAAFAAEAAAAOAAAABQD#####AQAAAAAQAAABAAEAAAAQAAAABQAAAAQA#####wEAAAAAEAAAAQABAAAACAAAAAv#####AAAAAQAQQ0ludERyb2l0ZURyb2l0ZQD#####AQAAAAAQAAAAAAAAAAAAAABACAAAAAAAAAUAAAAAEQAAABIAAAADAP####8AAAAAAAEAAAAFAAAAAwAAABAAAAATAAAACwAAAAMAAAAKAP####8BAAAAAAEAAAACAAAAAQAAAAsA#####wAAAAUAAAAVAAAADAD#####AQAAAAAQAAAAAAAAAAAAAABACAAAAAAAAAUAAQAAABYAAAAMAP####8BAAAAABAAAUkAAAAAAAAAAABACAAAAAAAAAUAAgAAABYAAAAFAP####8BAAAAABAAAAEAAQAAABgAAAAFAAAADQD#####AQAAAAAQAAFFAAAAAAAAAAAAQAgAAAAAAAAFAAAAABkAAAASAAAAAwD#####AQAAAAABAAAABQAAABgAAAACAAAACAAAABoAAAAY#####wAAAAEAB0NNaWxpZXUA#####wEAAAAAEAAAAAAAAAAAAAAAQAgAAAAAAAAFAAAAAAIAAAAIAAAADgD#####AQAAAAAQAAAAAAAAAAAAAABACAAAAAAAAAUAAAAAAwAAAAsAAAAEAP####8BAAAAABAAAAEAAQAAABwAAAAd#####wAAAAEAD0NTeW1ldHJpZUF4aWFsZQD#####AAAAHgAAAAkA#####wEAAAAAEAAAAAAAAAAAAAAAQAgAAAAAAAAFAAAAAAEAAAAfAAAAAwD#####AAAAAAABAAAABAAAAAgAAAAgAAAACwAAAAgAAAACAP####8BAAD#ARAAAUQAAAAAAAAAAABACAAAAAAAAAUAAUBPvaJWRkLgQFQ08THhLoAAAAACAP####8AAAD#ABAAAUEAAAAAAAAAAABACAAAAAAAAAUAAUCDwAAAAAAAQI03Cj1wo9j#####AAAAAQAHQ0NhbGN1bAD#####AAVtaW5pMQABMAAAAAEAAAAAAAAAAAAAABAA#####wAFbWF4aTEAATEAAAABP#AAAAAAAAAAAAAQAP####8ABW1pbmkyAAEwAAAAAQAAAAAAAAAAAAAAEAD#####AAVtYXhpMgABMQAAAAE#8AAAAAAAAP####8AAAABAAhDU2VnbWVudAD#####Af8AAAAQAAABAAEAAAACAAAAIv####8AAAABABBDRHJvaXRlUGFyYWxsZWxlAP####8B#wAAABAAAAEAAQAAAAEAAAAFAAAADQD#####Af8AAAAQAAAAAAAAAAAAAABACAAAAAAAAAUAAAAAKQAAAAYAAAANAP####8B#wAAABAAAAAAAAAAAAAAAEAIAAAAAAAABQAAAAAHAAAAKQAAAAcA#####wEAAP8AEAAAAQABAAAACAAAAAsAAAAACAD#####AAAALAAAAAcA#####wEAAP8AEAAAAQABAAAAKgAAAAEAAAAACAD#####AAAALgAAABIA#####wEAAP8AEAAAAQABAAAAIAAAAAUAAAANAP####8BAAD#ABAAAAAAAAAAAAAAAEAIAAAAAAAABQAAAAAGAAAAMAAAAAcA#####wEAAP8AEAAAAQABAAAAMQAAACAAAAAACAD#####AAAAMgAAAA8A#####wAAABIAAAAJAP####8BAAD#ABAAAAAAAAAAAAAAAEAIAAAAAAAABQAAAAAgAAAANAAAAAIA#####wEAAAAAEAABSgAAAAAAAAAAAEAIAAAAAAAABQABQGf0#BvsumJAZol++JjwmP###############w=='
      case 'pr6': return 'TWF0aEdyYXBoSmF2YTEuMAAAABI+TMzNAAJmcv###wEA#wEAAAAAAAAAAAM7AAACdwAAAQEAAAAAAAAAAQAAADz#####AAAAAQAKQ0NhbGNDb25zdAD#####AAJwaQAWMy4xNDE1OTI2NTM1ODk3OTMyMzg0Nv####8AAAABAApDQ29uc3RhbnRlQAkh+1RELRj#####AAAAAQAKQ1BvaW50QmFzZQD#####AQAAAAAQAAFHAAAAAAAAAAAAQAgAAAAAAAAFAAFAUGq++mU4WEBBiX0dCM6wAAAAAgD#####AQAAAAAQAAFDAAAAAAAAAAAAQAgAAAAAAAAFAAFATHrKKMNqwEBQFxrp0gLsAAAAAgD#####AQAAAAAQAAFIAAAAAAAAAAAAQAgAAAAAAAAFAAFAXUk#J1g6HEBQFxrp0gLs#####wAAAAEACUNQb2x5Z29uZQD#####AAAAAAABAAAABAAAAAEAAAACAAAAAwAAAAH#####AAAAAQAJQ0Ryb2l0ZUFCAP####8BAAAAABAAAAEAAQAAAAIAAAAD#####wAAAAEAFkNEcm9pdGVQZXJwZW5kaWN1bGFpcmUA#####wEAAAAAEAAAAQABAAAAAgAAAAUAAAAFAP####8BAAAAABAAAAEAAQAAAAMAAAAF#####wAAAAEAD0NQb2ludExpZURyb2l0ZQD#####AQAAAAAQAAAAAAAAAAAAAABACAAAAAAAAAUAAT#lRqrLMZD+AAAABv####8AAAABAAhDVmVjdGV1cgD#####AQAAAAAQAAABAAEAAAACAAAAAwD#####AAAAAQANQ1RyYW5zUGFyVmVjdAD#####AAAACf####8AAAABAAtDUG9pbnRJbWFnZQD#####AQAAAAAQAAAAAAAAAAAAAABACAAAAAAAAAUAAAAACAAAAAoAAAADAP####8AAAAAAAEAAAAFAAAAAgAAAAMAAAALAAAACAAAAAL#####AAAAAQAJQ0NlcmNsZU9BAP####8BAAAAAAEAAAADAAAAAf####8AAAABABBDSW50RHJvaXRlQ2VyY2xlAP####8AAAAFAAAADf####8AAAABABBDUG9pbnRMaWVCaXBvaW50AP####8BAAAAABAAAAAAAAAAAAAAAEAIAAAAAAAABQACAAAADgAAAAwA#####wEAAAAAEAAAAAAAAAAAAAAAQAgAAAAAAAAFAAEAAAAOAAAABQD#####AQAAAAAQAAABAAEAAAAQAAAABQAAAAQA#####wEAAAAAEAAAAQABAAAACAAAAAv#####AAAAAQAQQ0ludERyb2l0ZURyb2l0ZQD#####AQAAAAAQAAAAAAAAAAAAAABACAAAAAAAAAUAAAAAEQAAABIAAAADAP####8BAAAAAAEAAAAFAAAAAwAAABAAAAATAAAACwAAAAMAAAAKAP####8BAAAAAAEAAAACAAAAAQAAAAsA#####wAAAAUAAAAVAAAADAD#####AQAAAAAQAAAAAAAAAAAAAABACAAAAAAAAAUAAQAAABYAAAAMAP####8BAAAAABAAAUkAAAAAAAAAAABACAAAAAAAAAUAAgAAABYAAAAFAP####8BAAAAABAAAAEAAQAAABgAAAAFAAAADQD#####AQAAAAAQAAFFAAAAAAAAAAAAQAgAAAAAAAAFAAAAABkAAAASAAAAAwD#####AAAAAAABAAAABQAAABgAAAACAAAACAAAABoAAAAY#####wAAAAEAB0NNaWxpZXUA#####wEAAAAAEAAAAAAAAAAAAAAAQAgAAAAAAAAFAAAAAAIAAAAIAAAADgD#####AQAAAAAQAAAAAAAAAAAAAABACAAAAAAAAAUAAAAAAwAAAAsAAAAEAP####8BAAAAABAAAAEAAQAAABwAAAAd#####wAAAAEAD0NTeW1ldHJpZUF4aWFsZQD#####AAAAHgAAAAkA#####wEAAAAAEAAAAAAAAAAAAAAAQAgAAAAAAAAFAAAAAAEAAAAfAAAAAwD#####AAAAAAABAAAABAAAAAgAAAAgAAAACwAAAAgAAAACAP####8BAAD#ARAAAUQAAAAAAAAAAABACAAAAAAAAAUAAUBT3tErIyFwQFQ08THhLoAAAAACAP####8AAAD#ABAAAUEAAAAAAAAAAABACAAAAAAAAAUAAUCEQAAAAAAAQI03Cj1wo9j#####AAAAAQAHQ0NhbGN1bAD#####AAVtaW5pMQABMAAAAAEAAAAAAAAAAAAAABAA#####wAFbWF4aTEAATEAAAABP#AAAAAAAAAAAAAQAP####8ABW1pbmkyAAEwAAAAAQAAAAAAAAAAAAAAEAD#####AAVtYXhpMgABMQAAAAE#8AAAAAAAAP####8AAAABAAhDU2VnbWVudAD#####Af8AAAAQAAABAAEAAAACAAAAIv####8AAAABABBDRHJvaXRlUGFyYWxsZWxlAP####8B#wAAABAAAAEAAQAAAAEAAAAFAAAADQD#####Af8AAAAQAAAAAAAAAAAAAABACAAAAAAAAAUAAAAAKQAAAAYAAAANAP####8B#wAAABAAAAAAAAAAAAAAAEAIAAAAAAAABQAAAAAHAAAAKQAAAAcA#####wEAAP8AEAAAAQABAAAACAAAAAsAAAAACAD#####AAAALAAAAAcA#####wEAAP8AEAAAAQABAAAAKgAAAAEAAAAACAD#####AAAALgAAABIA#####wEAAP8AEAAAAQABAAAAIAAAAAUAAAANAP####8BAAD#ABAAAAAAAAAAAAAAAEAIAAAAAAAABQAAAAAGAAAAMAAAAAcA#####wEAAP8AEAAAAQABAAAAMQAAACAAAAAACAD#####AAAAMgAAAA8A#####wAAABIAAAAJAP####8BAAD#ABAAAAAAAAAAAAAAAEAIAAAAAAAABQAAAAAgAAAANAAAAAIA#####wEAAAAAEAABSgAAAAAAAAAAAEAIAAAAAAAABQABQGn0#BvsumJAZol++JjwmAAAAA4A#####wEAAAAAEAAAAAAAAAAAAAAAQAgAAAAAAAAFAAAAAAMAAAAQAAAADgD#####AQAAAAAQAAAAAAAAAAAAAABACAAAAAAAAAUAAAAAEwAAAAsAAAAOAP####8BAAAAABAAAAAAAAAAAAAAAEAIAAAAAAAABQAAAAA3AAAAAwAAAA4A#####wEAAAAAEAAAAAAAAAAAAAAAQAgAAAAAAAAFAAAAAAsAAAA4AAAAAwD#####AAAAAAABAAAABQAAAAMAAAA5AAAAOgAAAAsAAAAD################'
      case 'cy1': return 'TWF0aEdyYXBoSmF2YTEuMAAAABM+GZmaAAJmcv###wEA#wEAAAAAAAAAAANLAAAChwAAAQEAAAAAAAAAAQAAAnT#####AAAAAQAKQ0NhbGNDb25zdAD#####AAJwaQAWMy4xNDE1OTI2NTM1ODk3OTMyMzg0Nv####8AAAABAApDQ29uc3RhbnRlQAkh+1RELRj#####AAAAAQAKQ1BvaW50QmFzZQD#####AQAAAAEQAAFDAAAAAAAAAAAAQAgAAAAAAAAAAAkAAUBXhobRCs78QEfX7K+ezEAAAAACAP####8BAAAAABAAAUQAAAAAAAAAAABACAAAAAAAAAAACQABQFdsOjTJk3hAUwRlFjLD3P####8AAAABAAlDQ2VyY2xlT0EA#####wAAAAAAAAABAAAAAQAAAAL#####AAAAAQAJQ0Ryb2l0ZUFCAP####8BAAAAABAAAAEAAAABAAAAAQAAAAL#####AAAAAQAWQ0Ryb2l0ZVBlcnBlbmRpY3VsYWlyZQD#####AQAAAAAQAAABAAAAAQAAAAIAAAAEAAAAAgD#####AQAAAAAQAAFBAAAAAAAAAAAAQAgAAAAAAAAAAAkAAUCkjgAAAAAAQJh7hR64UewAAAACAP####8BAAAAABAAAUIAAAAAAAAAAABACAAAAAAAAAAACQABQEMRV#fMCoBAcNWw+bKd+v####8AAAABAAlDTG9uZ3VldXIA#####wAAAAYAAAAH#####wAAAAEAFENJbXBsZW1lbnRhdGlvblByb3RvAP####8AEk1lc3VyZSBkZSBsb25ndWV1cgAAAAUAAAACAAAAAgAAAAEAAAAC#####wAAAAEAC0NNZWRpYXRyaWNlAAAAAAkBAAAAABAAAAEAAAABAAAAAQAAAAL#####AAAAAQAHQ01pbGlldQAAAAAJAQAAAAAQAAABAAAFAAAAAAEAAAAC#####wAAAAIACUNDZXJjbGVPUgAAAAAJAQAAAAAAAAEAAAALAAAAAUAwAAAAAAAAAf####8AAAABABBDSW50RHJvaXRlQ2VyY2xlAAAAAAkAAAAKAAAADP####8AAAABABBDUG9pbnRMaWVCaXBvaW50AAAAAAkBAAAAABAAAAEAAAUAAQAAAA0AAAAGAQAAAAkAAAABAAAAAv####8AAAABAA9DVmFsZXVyQWZmaWNoZWUBAAAACQEAAAABAAAAAAAOEQAAAAAAAQAAAAEAAAABAAAAAAAAAAAAAAAAAQAAAA######AAAAAQAHQ0NhbGN1bAD#####AARyYXlvAAkyKnBpKkNELzL#####AAAAAQAKQ09wZXJhdGlvbgMAAAAPAgAAAA8CAAAAAUAAAAAAAAAA#####wAAAAEAD0NSZXN1bHRhdFZhbGV1cgAAAAAAAAAQAAAADwAAAAFAAAAAAAAAAAAAAAoA#####wEAAAAAAAABAAAAAgAAABAAAAARAAAAAAsA#####wAAAAUAAAASAAAADAD#####AQAAAAAQAAFFAAAAAAAAAAAAQAgAAAAAAAAAAAkAAQAAABMAAAAMAP####8BAAAAABAAAAAAAAAAAAAAAEAIAAAAAAAAAAAJAAIAAAATAAAABQD#####AQAAAAAQAAABAAAAAQAAABUAAAAF#####wAAAAEAD0NQb2ludExpZURyb2l0ZQD#####AQAAAAAQAAFIAAAAAAAAAAAAQAgAAAAAAAAAAAkAAb#ypfVoKl9VAAAAFgAAAAUA#####wEAAAAAEAAAAQAAAAEAAAAUAAAABQAAAAUA#####wEAAAAAEAAAAQAAAAEAAAAXAAAAFv####8AAAABABBDSW50RHJvaXRlRHJvaXRlAP####8BAAAAABAAAAAAAAAAAAAAAEAIAAAAAAAAAAAJAAAAABgAAAAZAAAACQD#####AQAAAAAQAAAAAAAAAAAAAABACAAAAAAAAAAACQAAAAAaAAAAF#####8AAAABAAhDVmVjdGV1cgD#####AQAAAAAQAAABAAAAAQAAAAEAAAACAP####8AAAABAA1DVHJhbnNQYXJWZWN0AP####8AAAAc#####wAAAAEACUNQb2x5Z29uZQD#####AQAAAAAAAAEAAAAFAAAAFAAAABUAAAAXAAAAGgAAABQAAAACAP####8AAAAAABAAAABACAAAAAAAAAAAAAAAAAAAAAAFAABAY4AAAAAAAEB#ThR64UeuAAAADgD#####AAVtaW5pMQABMAAAAAEAAAAAAAAAAAAAAA4A#####wAFbWF4aTEAATEAAAABP#AAAAAAAAAAAAAHAP####8AB0N1cnNldXIAAAAFAAAABQAAAAMAAAAgAAAAIQAAAB######AAAAAQAUQ0Ryb2l0ZURpcmVjdGlvbkZpeGUAAAAAIgEAAAAAEAAAAQAAAAEAAAAfAT#wAAAAAAAAAAAAEQEAAAAiAAAAAAAQAAAAwAgAAAAAAAA#8AAAAAAAAAAABQAAQF1AAAAAAAAAAAAj#####wAAAAEAC0NIb21vdGhldGllAAAAACIAAAAfAAAADwMAAAAQAAAAIAAAAA8BAAAAEAAAACAAAAAQAAAAIf####8AAAABAAtDUG9pbnRJbWFnZQAAAAAiAQAAAAANAAJPMQDAEAAAAAAAAEAQAAAAAAAAAAAFAAAAACQAAAAlAAAAFwAAAAAiAAAAHwAAAA8DAAAADwEAAAABP#AAAAAAAAAAAAAQAAAAIAAAAA8BAAAAEAAAACEAAAAQAAAAIAAAABgAAAAAIgEAAAAADQACSTEAwAAAAAAAAABACAAAAAAAAAAABQAAAAAkAAAAJ#####8AAAABAAhDU2VnbWVudAEAAAAiAAAAAAAQAAABAAAAAQAAAB8AAAAkAAAAEQEAAAAiAAAAAAEQAAJrMQDAAAAAAAAAAEAAAAAAAAAAAAABAAEAAAAAAAAAAAAAACn#####AAAAAgAPQ01lc3VyZUFic2Npc3NlAQAAACIAAWEAAAAmAAAAKAAAACoAAAANAQAAACIAAAAAAAAAAAAAAAAAwBgAAAAAAAAAAAAAACoPAAH###8AAAABAAAAAgAAAAEAAAAAAAAAAAAAAAACAAAAKwAAAAIA#####wAAAAAAEAAAAEAIAAAAAAAAAAAAAAAAAAAAAAUAAEB2IAAAAAAAQH9eFHrhR64AAAAOAP####8ABW1pbmkyAAEwAAAAAQAAAAAAAAAAAAAADgD#####AAVtYXhpMgABMQAAAAE#8AAAAAAAAAAAAAcA#####wAHQ3Vyc2V1cgAAAAUAAAAFAAAAAwAAAC4AAAAvAAAALQAAABYAAAAAMAEAAAAAEAAAAQAAAAEAAAAtAT#wAAAAAAAAAAAAEQEAAAAwAAAAAAAQAAAAwAgAAAAAAAA#8AAAAAAAAAAABQAAQF1AAAAAAAAAAAAxAAAAFwAAAAAwAAAALQAAAA8DAAAAEAAAAC4AAAAPAQAAABAAAAAuAAAAEAAAAC8AAAAYAAAAADABAAAAAA0AAk8xAMAQAAAAAAAAQBAAAAAAAAAAAAUAAAAAMgAAADMAAAAXAAAAADAAAAAtAAAADwMAAAAPAQAAAAE#8AAAAAAAAAAAABAAAAAuAAAADwEAAAAQAAAALwAAABAAAAAuAAAAGAAAAAAwAQAAAAANAAJJMQDAAAAAAAAAAEAIAAAAAAAAAAAFAAAAADIAAAA1AAAAGQEAAAAwAAAAAAAQAAABAAAAAQAAAC0AAAAyAAAAEQEAAAAwAAAAAAEQAAFrAMAAAAAAAAAAQAAAAAAAAAAAAAEAAQAAAAAAAAAAAAAANwAAABoBAAAAMAABYgAAADQAAAA2AAAAOAAAAA0BAAAAMAAAAAAAAAAAAAAAAADAGAAAAAAAAAAAAAAAOA8AAf###wAAAAEAAAACAAAAAQAAAAAAAAAAAAAAAAIAAAA5AAAADgD#####AAJhYQABYQAAABAAAAArAAAADgD#####AAJiYgABYgAAABAAAAA5AAAAAgD#####AQAAAAAQAAFGAAAAAAAAAAAAQAgAAAAAAAAAAAUAAUAydaZ0VbZAQFWfD8DdbnQAAAAZAP####8BAAAAABAAAAEAAAABAAAAFAAAAD0AAAAHAP####8AEk1lc3VyZSBkZSBsb25ndWV1cgAAAAUAAAACAAAAAgAAABQAAAA9AAAACAAAAAA#AQAAAAAQAAABAAAAAQAAABQAAAA9AAAACQAAAAA#AQAAAAAQAAABAAAFAAAAABQAAAA9AAAACgAAAAA#AQAAAAAAAAEAAABBAAAAAUAwAAAAAAAAAQAAAAsAAAAAPwAAAEAAAABCAAAADAAAAAA#AQAAAAAQAAABAAAFAAEAAABDAAAABgEAAAA#AAAAFAAAAD0AAAANAQAAAD8BAAAAAQAAAAAARBEAAAAAAAEAAAABAAAAAQAAAAAAAAAAAAAAAAEAAABFAAAACgD#####AQAAAAAAAAEAAAAUAAAADwIAAAAQAAAARQAAABAAAAA7AAAAAAsA#####wAAAD4AAABHAAAADAD#####Af8AAAAQAAAAAAAAAAAAAABACAAAAAAAAAAABQABAAAASAAAAAwA#####wAAAAAAEAAAAAAAAAAAAAAAQAgAAAAAAAAAAAUAAgAAAEgAAAATAP####8B#wAAABAAAAEAAAABAAAAGgAAABcAAAAAFAD#####AAAASwAAABgA#####wH#AAAAEAAAAAAAAAAAAAAAQAgAAAAAAAAAAAUAAAAASQAAAEwAAAAVAP####8B#wAAAAAAAQAAAAUAAAAXAAAATQAAAEkAAAAaAAAAF#####8AAAABABBDRHJvaXRlUGFyYWxsZWxlAP####8B#wAAABAAAAEAAAABAAAAAQAAAAUAAAAEAP####8B#wAAABAAAAEAAAABAAAAGgAAAEkAAAAEAP####8B#wAAABAAAAEAAAABAAAAFAAAAEkAAAASAP####8B#wAAABAAAAAAAAAAAAAAAEAIAAAAAAAAAAAFAAAAABgAAABPAAAAGwD#####Af8AAAAQAAABAAAAAQAAAFIAAABRAAAAEgD#####Af8AAAAQAAAAAAAAAAAAAABACAAAAAAAAAAABQAAAABQAAAAUwAAABMA#####wH#AAAAEAAAAQAAAAEAAABSAAAAAQAAAAATAP####8B#wAAABAAAAEAAAABAAAAFAAAAAIAAAAACwD#####AAAATwAAAAMAAAAMAP####8B#wAAABAAAAAAAAAAAAAAAEAIAAAAAAAAAAAFAAIAAABXAAAADAD#####Af8AAAAQAAAAAAAAAAAAAABACAAAAAAAAAAABQABAAAAVwAAABMA#####wH#AAAAEAAAAQAAAAEAAAABAAAAWAAAAAAZAP####8B#wAAABAAAAEAAAABAAAASQAAAE3#####AAAAAQAPQ1BvaW50TGllQ2VyY2xlAP####8B#wAAABAAAAAAAAAAAAAAAEAIAAAAAAAAAAAIAAFAAA80C4Gk8AAAAAMAAAAbAP####8B#wAAABAAAAEAAAABAAAAXAAAAAUAAAASAP####8B#wAAABAAAAAAAAAAAAAAAEAIAAAAAAAAAAAIAAAAAF0AAAAYAAAAGwD#####Af8AAAAQAAABAAAAAQAAAF4AAABRAAAAEgD#####Af8AAAAQAAAAAAAAAAAAAABACAAAAAAAAAAACAAAAABfAAAAUAAAABMA#####wH#AAAAEAAAAQAAAAEAAABeAAAAXAAAAAAUAP####8AAABhAAAAGAD#####Af8AAAAQAAAAAAAAAAAAAABACAAAAAAAAAAACAAAAABgAAAAYv####8AAAACAA1DTGlldURlUG9pbnRzAP####8B#wAAAAAAAQAAAGMAAADIAAAAAABcAAAACAAAAFwAAABdAAAAXgAAAF8AAABgAAAAYQAAAGIAAABj#####wAAAAEAFUNQb2ludExpZUxpZXVQYXJQdExpZQD#####AQAA#wAQAAAAAAAAAAAAAABACAAAAAAAAAAACAABP#Uci2++Y1IAAABkP#Uci2++Y1IAAAAYAP####8BAAAAABAAAAAAAAAAAAAAAEAIAAAAAAAAAAAJAAAAABsAAAAdAAAAAwD#####AAAAAAACYzIAAQAAAGYAAAAbAAAAHAD#####Af8AAAAQAAAAAAAAAAAAAABACAAAAAAAAAAACQABQBivfGEj9gAAAABnAAAAGwD#####Af8AAAAQAAABAAAAAQAAAGgAAAAZAAAAEgD#####Af8AAAAQAAAAAAAAAAAAAABACAAAAAAAAAAACQAAAABpAAAAGAAAABsA#####wH#AAAAEAAAAQAAAAEAAABqAAAAUQAAABIA#####wH#AAAAEAAAAAAAAAAAAAAAQAgAAAAAAAAAAAkAAAAAawAAAFAAAAATAP####8B#wAAABAAAAEAAAABAAAAagAAAGgAAAAAFAD#####AAAAbQAAABgA#####wH#AAAAEAAAAAAAAAAAAAAAQAgAAAAAAAAAAAkAAAAAbAAAAG4AAAAdAP####8B#wAAAAAAAQAAAG8AAADIAQEAAABoAAAACAAAAGgAAABpAAAAagAAAGsAAABsAAAAbQAAAG4AAABvAAAACwD#####AAAABAAAAGcAAAAMAP####8B#wAAABAAAU8AAAAAAAAAAABACAAAAAAAAAAACQACAAAAcQAAAAwA#####wH#AAAAEAABSgAAAAAAAAAAAEAIAAAAAAAAAAAJAAEAAABx#####wAAAAEAEUNTeW1ldHJpZUNlbnRyYWxlAP####8AAAAbAAAAGAD#####AQAA#wAQAAFHAAAAAAAAAAAAQAgAAAAAAAAAAAkAAAAAcwAAAHQAAAAZAP####8BAAD#ABAAAAEAAAABAAAAdQAAABoAAAAZAP####8BAAD#ABAAAAEAAAABAAAAdQAAABcAAAAHAP####8AEk1lc3VyZSBkZSBsb25ndWV1cgAAAAUAAAACAAAAAgAAAHUAAAAXAAAACAAAAAB4AQAAAAAQAAABAAAAAQAAAHUAAAAXAAAACQAAAAB4AQAAAAAQAAABAAAFAAAAAHUAAAAXAAAACgAAAAB4AQAAAAAAAAEAAAB6AAAAAUAwAAAAAAAAAQAAAAsAAAAAeAAAAHkAAAB7AAAADAAAAAB4AQAAAAAQAAABAAAFAAEAAAB8AAAABgEAAAB4AAAAdQAAABcAAAANAQAAAHgBAAD#AQAAAAAAfREAAAAAAAEAAAABAAAAAQAAAAAAAAAAAAAAAAEAAAB+AAAACgD#####AQAA#wAAAAEAAAB1AAAADwIAAAAPAQAAAAE#8AAAAAAAAAAAABAAAAA8AAAAEAAAAH4AAAAACwD#####AAAAdgAAAIAAAAAMAP####8BAAD#ABAAAAAAAAAAAAAAAEAIAAAAAAAAAAAJAAEAAACBAAAADAD#####AAAA#wAQAAAAAAAAAAAAAABACAAAAAAAAAAACQACAAAAgQAAAAsA#####wAAAHcAAACAAAAADAD#####AQAA#wAQAAAAAAAAAAAAAABACAAAAAAAAAAACQABAAAAhAAAAAwA#####wAAAP8AEAAAAAAAAAAAAAAAQAgAAAAAAAAAAAkAAgAAAIQAAAAbAP####8BAAD#ABAAAAEAAAABAAAAggAAAAQAAAAbAP####8BAAD#ABAAAAEAAAABAAAAhQAAAAQAAAASAP####8BAAD#ABAAAAAAAAAAAAAAAEAIAAAAAAAAAAAJAAAAAIcAAAAZAAAAEgD#####AQAA#wAQAAFYAAAAAAAAAAAAQAgAAAAAAAAAAAkAAAAAiAAAABkAAAAJAP####8BAAD#ABAAAAAAAAAAAAAAAEAIAAAAAAAAAAAJAAAAAIIAAACJAAAACQD#####AQAA#wAQAAFXAAAAAAAAAAAAQAgAAAAAAAAAAAkAAAAAhQAAAIoAAAAZAP####8AAAAAABAAAAEAAmwxAAEAAACJAAAAGwAAABkA#####wAAAAAAEAAAAQACbDIAAQAAABsAAACKAAAAGwD#####AQAA#wAQAAABAAAAAQAAAHMAAAAZAAAAEgD#####AQAA#wAQAAAAAAAAAAAAAABACAAAAAAAAAAACQAAAAAYAAAAjwAAABsA#####wEAAP8AEAAAAQAAAAEAAACQAAAAUQAAABIA#####wEAAP8AEAAAAAAAAAAAAAAAQAgAAAAAAAAAAAkAAAAAUAAAAJEAAAATAP####8BAAD#ABAAAAEAAAABAAAAkAAAAHMAAAAAFAD#####AAAAkwAAABgA#####wEAAP8AEAABSQAAAAAAAAAAAEAIAAAAAAAAAAAJAAAAAJIAAACUAAAAFAD#####AAAAWgAAABQA#####wAAAFYAAAATAP####8BAAD#ABAAAAEAAAABAAAAlQAAAHIAAAAAGAD#####AQAA#wAQAAAAAAAAAAAAAABACAAAAAAAAAAACAAAAABJAAAAlwAAABQA#####wAAAJgAAAAYAP####8BAAD#ABAAAVAAAAAAAAAAAABACAAAAAAAAAAACAAAAACZAAAAmgAAABMA#####wEAAP8AEAAAAQAAAAEAAABzAAAAcgAAAAAUAP####8AAACcAAAAGAD#####AQAA#wAQAAFSAAAAAAAAAAAAQAgAAAAAAAAAAAgAAAAAmQAAAJ0AAAAIAP####8BAAD#ABAAAAEAAAABAAAAngAAAJsAAAARAP####8BAAD#ABAAAVEAAAAAAAAAAABACAAAAAAAAAAACAABP#SG#M3SxvcAAACfAAAABwD#####ABtNZXN1cmUgZCdhbmdsZSBub24gb3JpZW50w6kAAAACAAAAAwAAAAMAAACbAAAAoAAAAJ7#####AAAAAQAMQ0Jpc3NlY3RyaWNlAAAAAKEBAAAAABAAAAEAAAABAAAAmwAAAKAAAACeAAAAEQAAAAChAQAAAAAQAAABAAAFAAFAbyWoZaxxqQAAAKL#####AAAAAQAXQ01lc3VyZUFuZ2xlR2VvbWV0cmlxdWUBAAAAoQAAAJsAAACgAAAAnv####8AAAACABdDTWFycXVlQW5nbGVHZW9tZXRyaXF1ZQEAAAChAQAA#wAAAAEAAAABQEUGF39UkbsAAACbAAAAoAAAAJ4AAAANAQAAAKEBAAD#AEAIAAAAAAAAAAAAAAAAAAAAAAAAAKMPAAAAAAABAAAAAQAAAAEAAAAAAAAAAAAAAALCsAAAAACk#####wAAAAEACUNSb3RhdGlvbgD#####AAAAoAAAAA8CAAAAEAAAADwAAAAQAAAApAAAABgA#####wEAAP8AEAABVgAAAAAAAAAAAEAIAAAAAAAAAAAIAAAAAJsAAACnAAAACQD#####AQAA#wAQAAFTAAAAAAAAAAAAQAgAAAAAAAAAAAgAAAAAqAAAAJkAAAAYAP####8BAAD#ABAAAVUAAAAAAAAAAABACAAAAAAAAAAACAAAAACpAAAAlgAAAB8A#####wAAAKkAAAAYAP####8BAAD#ABAAAAAAAAAAAAAAAEAIAAAAAAAAAAAIAAAAAKoAAACrAAAAGQD#####AQAA#wAQAAABAAAAAQAAAJkAAACoAAAAGQD#####AQAA#wAQAAABAAAAAQAAAKoAAACsAAAAEQD#####AQAA#wAQAAFUAAAAAAAAAAAAQAgAAAAAAAAAAAgAAT#NyRD2pDD7AAAArgAAAAcA#####wASTWVzdXJlIGRlIGxvbmd1ZXVyAAAABQAAAAIAAAACAAAAqQAAAK8AAAAIAAAAALABAAAAABAAAAEAAAABAAAAqQAAAK8AAAAJAAAAALABAAAAABAAAAEAAAUAAAAAqQAAAK8AAAAKAAAAALABAAAAAAAAAQAAALIAAAABQDAAAAAAAAABAAAACwAAAACwAAAAsQAAALMAAAAMAAAAALABAAAAABAAAAEAAAUAAQAAALQAAAAGAQAAALAAAACpAAAArwAAAA0BAAAAsAEAAP8BAAAAAAC1EQAAAAAAAQAAAAEAAAABAAAAAAAAAAAAAAAAAQAAALYAAAAHAP####8AEk1lc3VyZSBkZSBsb25ndWV1cgAAAAUAAAACAAAAAgAAAKkAAACqAAAACAAAAAC4AQAAAAAQAAABAAAAAQAAAKkAAACqAAAACQAAAAC4AQAAAAAQAAABAAAFAAAAAKkAAACqAAAACgAAAAC4AQAAAAAAAAEAAAC6AAAAAUAwAAAAAAAAAQAAAAsAAAAAuAAAALkAAAC7AAAADAAAAAC4AQAAAAAQAAABAAAFAAEAAAC8AAAABgEAAAC4AAAAqQAAAKoAAAANAQAAALgBAAD#AQAAAAAAvREAAAAAAAEAAAABAAAAAQAAAAAAAAAAAAAAAAEAAAC+AAAABwD#####ABJNZXN1cmUgZGUgbG9uZ3VldXIAAAAFAAAAAgAAAAIAAACpAAAAqAAAAAgAAAAAwAEAAAAAEAAAAQAAAAEAAACpAAAAqAAAAAkAAAAAwAEAAAAAEAAAAQAABQAAAACpAAAAqAAAAAoAAAAAwAEAAAAAAAABAAAAwgAAAAFAMAAAAAAAAAEAAAALAAAAAMAAAADBAAAAwwAAAAwAAAAAwAEAAAAAEAAAAQAABQABAAAAxAAAAAYBAAAAwAAAAKkAAACoAAAADQEAAADAAQAA#wEAAAAAAMURAAAAAAABAAAAAQAAAAEAAAAAAAAAAAAAAAABAAAAxgAAAAoA#####wEAAP8AAAABAAAAqQAAAA8C#####wAAAAIACUNGb25jdGlvbgEAAAAPAQAAAAE#8AAAAAAAAAAAAA8CAAAADwMAAAAQAAAAtgAAABAAAAC+AAAADwMAAAAQAAAAtgAAABAAAAC+AAAAEAAAAMYAAAAACwD#####AAAArQAAAMgAAAAbAP####8BAAD#ABAAAAEAAAABAAAArwAAAK0AAAAMAP####8BAAD#ABAAAAAAAAAAAAAAAEAIAAAAAAAAAAAIAAEAAADJAAAAGwD#####AQAA#wAQAAABAAAAAQAAAMsAAACuAAAAEgD#####AQAA#wAQAAAAAAAAAAAAAABACAAAAAAAAAAACAAAAADKAAAAzAAAAB0A#####wEAAAAAAm0yAAEAAADNAAABkAABAAAArwAAAAkAAACvAAAAsAAAALYAAADIAAAAyQAAAMsAAADMAAAAygAAAM0AAAAMAP####8BAAD#ABAAAAAAAAAAAAAAAEAIAAAAAAAAAAAIAAIAAADJAAAAGwD#####AQAA#wAQAAABAAAAAQAAAM8AAACuAAAAEgD#####AQAA#wAQAAAAAAAAAAAAAABACAAAAAAAAAAACAAAAADQAAAAygAAAB0A#####wEAAAAAAm0xAAEAAADRAAABkAABAAAArwAAAAkAAACvAAAAsAAAALYAAADIAAAAyQAAAM8AAADQAAAAygAAANH#####AAAAAQASQ0FyY0RlQ2VyY2xlRGlyZWN0AP####8AAAAAAARhcmMxAAEAAACMAAAAigAAAIUAAAAlAP####8AAAAAAARhcmMyAAEAAACLAAAAggAAAIkAAAATAP####8BAAD#ABAAAAEAAAABAAAAGgAAAEkAAAAAFAD#####AAAA1QAAABgA#####wEAAP8AEAAAAAAAAAAAAAAAQAgAAAAAAAAAAAkAAAAAggAAANYAAAAYAP####8BAAD#ABAAAAAAAAAAAAAAAEAIAAAAAAAAAAAJAAAAAIsAAADWAAAAHgD#####AQAAAAAQAAAAAAAAAAAAAABACAAAAAAAAAAABQABP+#rd#rd#rcAAADOP+#rd#rd#rcAAAAeAP####8BAAAAABAAAAAAAAAAAAAAAEAIAAAAAAAAAAAFAAE#7+t3+t3+twAAANI#7+t3+t3+twAAABkA#####wEAAAAAEAAAAQACbTQAAQAAANkAAADaAAAAGAD#####AQAA#wAQAAAAAAAAAAAAAABACAAAAAAAAAAACQAAAACJAAAA1gAAABgA#####wEAAP8AEAAAAAAAAAAAAAAAQAgAAAAAAAAAAAkAAAAAhQAAANYAAAAYAP####8BAAD#ABAAAAAAAAAAAAAAAEAIAAAAAAAAAAAJAAAAAIwAAADWAAAAHgD#####AQAAAAAQAAAAAAAAAAAAAABACAAAAAAAAAAABQABP2SIBSIBSIAAAADOP2SIBSIBSIAAAAAeAP####8BAAAAABAAAAAAAAAAAAAAAEAIAAAAAAAAAAAFAAE#ZIgFIgFIgAAAANI#ZIgFIgFIgAAAABkA#####wEAAAAAEAAAAQACbTMAAQAAAN8AAADgAAAAGAD#####AQAA#wAQAAAAAAAAAAAAAABACAAAAAAAAAAACQAAAACKAAAA1gAAACUA#####wAAAAAABGFyYzMAAQAAAN4AAADiAAAA3QAAACUA#####wAAAAAABGFyYzQAAQAAANgAAADXAAAA3AAAABkA#####wEAAAAAEAAAAQAAAAEAAACCAAAA1wAAABkA#####wEAAAAAEAAAAQAAAAEAAADdAAAAhQAAABkA#####wEAAAAAEAAAAQADc2RmAAEAAADcAAAA4gAAAAUA#####wEAAP8AEAAAAQAAAAEAAADeAAAAUAAAAAUA#####wEAAP8AEAAAAQAAAAEAAACMAAAAUAAAAAsA#####wAAAOkAAADTAAAADAD#####AQAA#wAQAAAAAAAAAAAAAABACAAAAAAAAAAACQABAAAA6gAAAAwA#####wAAAP8AEAAAAAAAAAAAAAAAQAgAAAAAAAAAAAkAAgAAAOoAAAALAP####8AAADoAAAA4wAAAAwA#####wEAAP8AEAAAAAAAAAAAAAAAQAgAAAAAAAAAAAkAAQAAAO0AAAAMAP####8AAAD#ABAAAAAAAAAAAAAAAEAIAAAAAAAAAAAJAAIAAADtAAAAGQD#####AAAAAAAQAAABAAAAAQAAAO4AAADrAAAABQD#####AQAA#wAQAAABAAAAAQAAANgAAABQAAAABQD#####AQAA#wAQAAABAAAAAQAAAIsAAABQAAAACwD#####AAAA8gAAANQAAAAMAP####8BAAD#ABAAAAAAAAAAAAAAAEAIAAAAAAAAAAAJAAEAAADzAAAADAD#####AQAA#wAQAAAAAAAAAAAAAABACAAAAAAAAAAACQACAAAA8wAAAAsA#####wAAAPEAAADkAAAADAD#####AQAA#wAQAAAAAAAAAAAAAABACAAAAAAAAAAACQABAAAA9gAAAAwA#####wEAAP8AEAAAAAAAAAAAAAAAQAgAAAAAAAAAAAkAAgAAAPYAAAAZAP####8AAAAAABAAAAEAAAABAAAA9QAAAPgAAAAHAP####8AEk1lc3VyZSBkZSBsb25ndWV1cgAAAAUAAAACAAAAAgAAAHMAAAB1AAAACAAAAAD6AQAAAAAQAAABAAAAAQAAAHMAAAB1AAAACQAAAAD6AQAAAAAQAAABAAAFAAAAAHMAAAB1AAAACgAAAAD6AQAAAAAAAAEAAAD8AAAAAUAwAAAAAAAAAQAAAAsAAAAA+gAAAPsAAAD9AAAADAAAAAD6AQAAAAAQAAABAAAFAAEAAAD+AAAABgEAAAD6AAAAcwAAAHUAAAANAQAAAPoB#wD#AQAAAAAA#xEAAAAAAAEAAAABAAAAAQAAAAAAAAAAAAAAAAEAAAEAAAAACgD#####Af8A#wAAAAEAAABzAAAADwIAAAAQAAAAPAAAABAAAAEAAAAAABMA#####wH#AP8AEAAAAQAAAAEAAAAUAAAASQAAAAAIAP####8B#wD#ABAAAAEAAAABAAAAdQAAAJUAAAARAP####8B#wD#ABAAAUsAAAAAAAAAAABACAAAAAAAAAAACAABv#XMRv84aOMAAAEEAAAABwD#####ABtNZXN1cmUgZCdhbmdsZSBub24gb3JpZW50w6kAAAACAAAAAwAAAAMAAACVAAABBQAAAHUAAAAgAAAAAQYBAAAAABAAAAEAAAABAAAAlQAAAQUAAAB1AAAAEQAAAAEGAQAAAAAQAAABAAAFAAFAbyWoZaxxqQAAAQcAAAAhAQAAAQYAAACVAAABBQAAAHUAAAAiAQAAAQYB#wD#AAAAAQAAAAFARQYXf1SRuwAAAJUAAAEFAAAAdQAAAA0BAAABBgH#AP8AQAgAAAAAAAAAAAAAAAAAAAAAAAABCA8AAAAAAAEAAAABAAAAAQAAAAAAAAAAAAAAAsKwAAAAAQkAAAAjAP####8AAAEF#####wAAAAEADENNb2luc1VuYWlyZQAAAA8CAAAAEAAAADwAAAAQAAABCQAAABgA#####wH#AP8AEAAAAAAAAAAAAAAAQAgAAAAAAAAAAAgAAAAAlQAAAQwAAAAJAP####8B#wD#ABAAAUwAAAAAAAAAAABACAAAAAAAAAAACAAAAAByAAABDQAAABsA#####wH#AP8AEAAAAQAAAAEAAAEOAAAAGQAAABgA#####wH#AP8AEAABTQAAAAAAAAAAAEAIAAAAAAAAAAAIAAAAAQ4AAACWAAAAHwD#####AAABDgAAABgA#####wH#AP8AEAAAAAAAAAAAAAAAQAgAAAAAAAAAAAgAAAABEAAAAREAAAAZAP####8B#wD#ABAAAAEAAAABAAABDQAAAHIAAAAZAP####8B#wD#ABAAAAEAAAABAAABEgAAARAAAAARAP####8BAAD#ABAAAU4AAAAAAAAAAABACAAAAAAAAAAACAABP7hJKRkhzsgAAAEUAAAABwD#####ABJNZXN1cmUgZGUgbG9uZ3VldXIAAAAFAAAAAgAAAAIAAAEOAAABEAAAAAgAAAABFgEAAAAAEAAAAQAAAAEAAAEOAAABEAAAAAkAAAABFgEAAAAAEAAAAQAABQAAAAEOAAABEAAAAAoAAAABFgEAAAAAAAABAAABGAAAAAFAMAAAAAAAAAEAAAALAAAAARYAAAEXAAABGQAAAAwAAAABFgEAAAAAEAAAAQAABQABAAABGgAAAAYBAAABFgAAAQ4AAAEQAAAADQEAAAEWAQAA#wEAAAAAARsRAAAAAAABAAAAAQAAAAEAAAAAAAAAAAAAAAABAAABHAAAAAcA#####wASTWVzdXJlIGRlIGxvbmd1ZXVyAAAABQAAAAIAAAACAAABDgAAARUAAAAIAAAAAR4BAAAAABAAAAEAAAABAAABDgAAARUAAAAJAAAAAR4BAAAAABAAAAEAAAUAAAABDgAAARUAAAAKAAAAAR4BAAAAAAAAAQAAASAAAAABQDAAAAAAAAABAAAACwAAAAEeAAABHwAAASEAAAAMAAAAAR4BAAAAABAAAAEAAAUAAQAAASIAAAAGAQAAAR4AAAEOAAABFQAAAA0BAAABHgEAAP8BAAAAAAEjEQAAAAAAAQAAAAEAAAABAAAAAAAAAAAAAAAAAQAAASQAAAAHAP####8AEk1lc3VyZSBkZSBsb25ndWV1cgAAAAUAAAACAAAAAgAAAQ4AAAByAAAACAAAAAEmAQAAAAAQAAABAAAAAQAAAQ4AAAByAAAACQAAAAEmAQAAAAAQAAABAAAFAAAAAQ4AAAByAAAACgAAAAEmAQAAAAAAAAEAAAEoAAAAAUAwAAAAAAAAAQAAAAsAAAABJgAAAScAAAEpAAAADAAAAAEmAQAAAAAQAAABAAAFAAEAAAEqAAAABgEAAAEmAAABDgAAAHIAAAANAQAAASYBAAD#AQAAAAABKxEAAAAAAAEAAAABAAAAAQAAAAAAAAAAAAAAAAEAAAEsAAAACgD#####AQAA#wAAAAEAAAEOAAAADwIAAAAkAQAAAA8BAAAAAT#wAAAAAAAAAAAADwIAAAAPAwAAABAAAAEkAAAAEAAAARwAAAAPAwAAABAAAAEkAAAAEAAAARwAAAAQAAABLAAAAAALAP####8AAAETAAABLgAAAAwA#####wEAAP8AEAAAAAAAAAAAAAAAQAgAAAAAAAAAAAgAAgAAAS8AAAAMAP####8BAAD#ABAAAAAAAAAAAAAAAEAIAAAAAAAAAAAIAAEAAAEvAAAAGwD#####AQAA#wAQAAABAAAAAQAAATEAAAEUAAAAGwD#####AQAA#wAQAAABAAAAAQAAARUAAAETAAAAEgD#####AQAA#wAQAAAAAAAAAAAAAABACAAAAAAAAAAACAAAAAEyAAABMwAAABsA#####wEAAP8AEAAAAQAAAAEAAAEwAAABMgAAAB0A#####wEAAAAABGZlcjEAAQAAATQAAAGQAAEAAAEVAAAACQAAARUAAAEeAAABJAAAAS4AAAEvAAABMQAAATIAAAEzAAABNAAAABIA#####wEAAP8AEAAAAAAAAAAAAAAAQAgAAAAAAAAAAAgAAAABNQAAATMAAAAdAP####8BAAAAAARmZXIyAAEAAAE3AAABkAABAAABFQAAAAsAAAEVAAABHgAAASQAAAEuAAABLwAAATAAAAExAAABMgAAATMAAAE1AAABNwAAABgA#####wEAAP8AEAAAAAAAAAAAAAAAQAgAAAAAAAAAAAgAAAAA9wAAAJf#####AAAAAQAOQ1N1cmZhY2VEaXNxdWUA#####wH###8AAnIxAAAABAAAAAP#####AAAAAQAQQ1N1cmZhY2VQb2x5Z29uZQD#####Af###wACcjIAAAAEAAAAHgAAACcA#####wH###8AAnIzAAAABAAAAGf#####AAAAAQARQ1N1cmZhY2VEZXV4TGlldXgA#####wH###8ABHlveW8AAAAEAAAAzgAAANIAAAAVAP####8BAAAAAAAAAQAAAAUAAADcAAAAiQAAAIoAAADiAAAA3AAAACgA#####wH###8AAnNwAAAABAAAAT4AAAAbAP####8BAAAAABAAAAEAAAABAAAAjAAAAAUAAAAZAP####8BAAAAABAAAAEAAAABAAAAhQAAAIoAAAAHAP####8AEk1lc3VyZSBkZSBsb25ndWV1cgAAAAUAAAACAAAAAgAAAIwAAACKAAAACAAAAAFCAQAAAAAQAAABAAAAAQAAAIwAAACKAAAACQAAAAFCAQAAAAAQAAABAAAFAAAAAIwAAACKAAAACgAAAAFCAQAAAAAAAAEAAAFEAAAAAUAwAAAAAAAAAQAAAAsAAAABQgAAAUMAAAFFAAAADAAAAAFCAQAAAAAQAAABAAAFAAEAAAFGAAAABgEAAAFCAAAAjAAAAIoAAAANAQAAAUIBAAAAAQAAAAABRxEAAAAAAAEAAAABAAAAAQAAAAAAAAAAAAAAAAEAAAFIAAAAEwD#####AQAAAAAQAAABAAAAAQAAAIoAAADiAAAAABQA#####wAAAUr#####AAAAAQAPQ1N5bWV0cmllQXhpYWxlAP####8AAAAEAAAAAgD#####Af8AAAAQAAAAAAAAAAAAAABACAAAAAAAAAAABQABQGrPLMCkLxRAORJoSNbXcAAAAAIA#####wH#AAAAEAAAAAAAAAAAAAAAQAgAAAAAAAAAAAUAAUBsniNFYdA8QDl5SburpgAAAAATAP####8B#wAAABAAAAEAAAABAAABTQAAAU4AAAAAFAD#####AAABTwAAACMA#####wAAAIwAAAABQBQAAAAAAAAAAAAYAP####8BZmZmABAAAAAAAAAAAAAAAEAIAAAAAAAAAAAFAAAAAIoAAAFRAAAAGAD#####AWZmZgAQAAAAAAAAAAAAAABACAAAAAAAAAAABQAAAAFSAAABUQAAABgA#####wFmZmYAEAAAAAAAAAAAAAAAQAgAAAAAAAAAAAUAAAABUwAAAVEAAAAYAP####8BZmZmABAAAAAAAAAAAAAAAEAIAAAAAAAAAAAFAAAAAVQAAAFRAAAAGAD#####AWZmZgAQAAAAAAAAAAAAAABACAAAAAAAAAAABQAAAAFVAAABUQAAABgA#####wFmZmYAEAAAAAAAAAAAAAAAQAgAAAAAAAAAAAUAAAABVgAAAVEAAAAYAP####8BZmZmABAAAAAAAAAAAAAAAEAIAAAAAAAAAAAFAAAAAVcAAAFRAAAAGAD#####AWZmZgAQAAAAAAAAAAAAAABACAAAAAAAAAAABQAAAAFYAAABUQAAABgA#####wFmZmYAEAAAAAAAAAAAAAAAQAgAAAAAAAAAAAUAAAABWQAAAVEAAAAYAP####8BZmZmABAAAAAAAAAAAAAAAEAIAAAAAAAAAAAFAAAAAVoAAAFRAAAAGAD#####AWZmZgAQAAAAAAAAAAAAAABACAAAAAAAAAAABQAAAAFbAAABUQAAABgA#####wFmZmYAEAAAAAAAAAAAAAAAQAgAAAAAAAAAAAUAAAABXAAAAVEAAAAYAP####8BZmZmABAAAAAAAAAAAAAAAEAIAAAAAAAAAAAFAAAAAV0AAAFRAAAAGAD#####AWZmZgAQAAAAAAAAAAAAAABACAAAAAAAAAAABQAAAAFeAAABUQAAABgA#####wFmZmYAEAAAAAAAAAAAAAAAQAgAAAAAAAAAAAUAAAABXwAAAVEAAAAYAP####8BZmZmABAAAAAAAAAAAAAAAEAIAAAAAAAAAAAFAAAAAWAAAAFRAAAAGAD#####AWZmZgAQAAAAAAAAAAAAAABACAAAAAAAAAAABQAAAAFhAAABUQAAABgA#####wFmZmYAEAAAAAAAAAAAAAAAQAgAAAAAAAAAAAUAAAABYgAAAVEAAAAYAP####8BZmZmABAAAAAAAAAAAAAAAEAIAAAAAAAAAAAFAAAAAWMAAAFRAAAAGAD#####AWZmZgAQAAAAAAAAAAAAAABACAAAAAAAAAAABQAAAAFkAAABUQAAABgA#####wFmZmYAEAAAAAAAAAAAAAAAQAgAAAAAAAAAAAUAAAABZQAAAVEAAAAYAP####8BZmZmABAAAAAAAAAAAAAAAEAIAAAAAAAAAAAFAAAAAWYAAAFRAAAAGAD#####AWZmZgAQAAAAAAAAAAAAAABACAAAAAAAAAAABQAAAAFnAAABUQAAABgA#####wFmZmYAEAAAAAAAAAAAAAAAQAgAAAAAAAAAAAUAAAABaAAAAVEAAAAYAP####8BZmZmABAAAAAAAAAAAAAAAEAIAAAAAAAAAAAFAAAAAWkAAAFRAAAAGAD#####AWZmZgAQAAAAAAAAAAAAAABACAAAAAAAAAAABQAAAAFqAAABUQAAABgA#####wFmZmYAEAAAAAAAAAAAAAAAQAgAAAAAAAAAAAUAAAABawAAAVEAAAAYAP####8BZmZmABAAAAAAAAAAAAAAAEAIAAAAAAAAAAAFAAAAAWwAAAFRAAAAGAD#####AWZmZgAQAAAAAAAAAAAAAABACAAAAAAAAAAABQAAAAFtAAABUQAAABgA#####wFmZmYAEAAAAAAAAAAAAAAAQAgAAAAAAAAAAAUAAAABbgAAAVEAAAAYAP####8BZmZmABAAAAAAAAAAAAAAAEAIAAAAAAAAAAAFAAAAAW8AAAFRAAAAGAD#####AWZmZgAQAAAAAAAAAAAAAABACAAAAAAAAAAABQAAAAFwAAABUQAAABgA#####wFmZmYAEAAAAAAAAAAAAAAAQAgAAAAAAAAAAAUAAAABcQAAAVEAAAAYAP####8BZmZmABAAAAAAAAAAAAAAAEAIAAAAAAAAAAAFAAAAAXIAAAFRAAAAGAD#####AWZmZgAQAAAAAAAAAAAAAABACAAAAAAAAAAABQAAAAFzAAABUQAAABgA#####wFmZmYAEAAAAAAAAAAAAAAAQAgAAAAAAAAAAAUAAAABUgAAAUsAAAAYAP####8BZmZmABAAAAAAAAAAAAAAAEAIAAAAAAAAAAAFAAAAAVMAAAFLAAAAGAD#####AWZmZgAQAAAAAAAAAAAAAABACAAAAAAAAAAABQAAAAFUAAABSwAAABgA#####wFmZmYAEAAAAAAAAAAAAAAAQAgAAAAAAAAAAAUAAAABVQAAAUsAAAAYAP####8BZmZmABAAAAAAAAAAAAAAAEAIAAAAAAAAAAAFAAAAAVYAAAFLAAAAGAD#####AWZmZgAQAAAAAAAAAAAAAABACAAAAAAAAAAABQAAAAFXAAABSwAAABgA#####wFmZmYAEAAAAAAAAAAAAAAAQAgAAAAAAAAAAAUAAAABWAAAAUsAAAAYAP####8BZmZmABAAAAAAAAAAAAAAAEAIAAAAAAAAAAAFAAAAAVkAAAFLAAAAGAD#####AWZmZgAQAAAAAAAAAAAAAABACAAAAAAAAAAABQAAAAFaAAABSwAAABgA#####wFmZmYAEAAAAAAAAAAAAAAAQAgAAAAAAAAAAAUAAAABWwAAAUsAAAAYAP####8BZmZmABAAAAAAAAAAAAAAAEAIAAAAAAAAAAAFAAAAAVwAAAFLAAAAGAD#####AWZmZgAQAAAAAAAAAAAAAABACAAAAAAAAAAABQAAAAFdAAABSwAAABgA#####wFmZmYAEAAAAAAAAAAAAAAAQAgAAAAAAAAAAAUAAAABXgAAAUsAAAAYAP####8BZmZmABAAAAAAAAAAAAAAAEAIAAAAAAAAAAAFAAAAAV8AAAFLAAAAGAD#####AWZmZgAQAAAAAAAAAAAAAABACAAAAAAAAAAABQAAAAFgAAABSwAAABgA#####wFmZmYAEAAAAAAAAAAAAAAAQAgAAAAAAAAAAAUAAAABYQAAAUsAAAAYAP####8BZmZmABAAAAAAAAAAAAAAAEAIAAAAAAAAAAAFAAAAAWIAAAFLAAAAGAD#####AWZmZgAQAAAAAAAAAAAAAABACAAAAAAAAAAABQAAAAFjAAABSwAAABgA#####wFmZmYAEAAAAAAAAAAAAAAAQAgAAAAAAAAAAAUAAAABZAAAAUsAAAAYAP####8BZmZmABAAAAAAAAAAAAAAAEAIAAAAAAAAAAAFAAAAAWUAAAFLAAAAGAD#####AWZmZgAQAAAAAAAAAAAAAABACAAAAAAAAAAABQAAAAFmAAABSwAAABgA#####wFmZmYAEAAAAAAAAAAAAAAAQAgAAAAAAAAAAAUAAAABZwAAAUsAAAAYAP####8BZmZmABAAAAAAAAAAAAAAAEAIAAAAAAAAAAAFAAAAAWgAAAFLAAAAGAD#####AWZmZgAQAAAAAAAAAAAAAABACAAAAAAAAAAABQAAAAFpAAABSwAAABgA#####wFmZmYAEAAAAAAAAAAAAAAAQAgAAAAAAAAAAAUAAAABagAAAUsAAAAYAP####8BZmZmABAAAAAAAAAAAAAAAEAIAAAAAAAAAAAFAAAAAWsAAAFLAAAAGAD#####AWZmZgAQAAAAAAAAAAAAAABACAAAAAAAAAAABQAAAAFsAAABSwAAABgA#####wFmZmYAEAAAAAAAAAAAAAAAQAgAAAAAAAAAAAUAAAABbQAAAUsAAAAYAP####8BZmZmABAAAAAAAAAAAAAAAEAIAAAAAAAAAAAFAAAAAW4AAAFLAAAAGAD#####AWZmZgAQAAAAAAAAAAAAAABACAAAAAAAAAAABQAAAAFvAAABSwAAABgA#####wFmZmYAEAAAAAAAAAAAAAAAQAgAAAAAAAAAAAUAAAABcAAAAUsAAAAYAP####8BZmZmABAAAAAAAAAAAAAAAEAIAAAAAAAAAAAFAAAAAXEAAAFLAAAAGAD#####AWZmZgAQAAAAAAAAAAAAAABACAAAAAAAAAAABQAAAAFyAAABSwAAABgA#####wFmZmYAEAAAAAAAAAAAAAAAQAgAAAAAAAAAAAUAAAABcwAAAUsAAAAYAP####8BZmZmABAAAAAAAAAAAAAAAEAIAAAAAAAAAAAFAAAAAXQAAAFLAAAAFQD#####AWZmZgAAAAEAAAAFAAAAhQAAAXQAAAGXAAAA3QAAAIUAAAAoAP####8B####AAJzMQAAAAQAAAGYAAAAFQD#####AX9#fwAAAAEAAAAFAAABdAAAAXMAAAGWAAABlwAAAXQAAAAoAP####8B####AAJzMgAAAAQAAAGaAAAAFQD#####AX9#fwAAAAEAAAAFAAABcwAAAXIAAAGVAAABlgAAAXMAAAAoAP####8B####AAJzMwAAAAQAAAGcAAAAFQD#####AX9#fwAAAAEAAAAFAAABcgAAAZUAAAGUAAABcQAAAXIAAAAoAP####8B####AAJzNAAAAAQAAAGeAAAAFQD#####AX9#fwAAAAEAAAAFAAABcQAAAXAAAAGTAAABlAAAAXEAAAAoAP####8B####AAJzNQAAAAQAAAGgAAAAFQD#####AX9#fwAAAAEAAAAFAAABkwAAAZIAAAFvAAABcAAAAZMAAAAVAP####8Bf39#AAAAAQAAAAUAAAGSAAABkQAAAW4AAAFvAAABkgAAABUA#####wF#f38AAAABAAAABQAAAZEAAAGQAAABbQAAAW4AAAGRAAAAFQD#####AX9#fwAAAAEAAAAFAAABkAAAAY8AAAFsAAABbQAAAZAAAAAVAP####8Bf39#AAAAAQAAAAUAAAGPAAABjgAAAWsAAAFsAAABjwAAABUA#####wF#f38AAAABAAAABQAAAY4AAAGNAAABagAAAWsAAAGOAAAAFQD#####AX9#fwAAAAEAAAAFAAABagAAAY0AAAGMAAABaQAAAWoAAAAVAP####8Bf39#AAAAAQAAAAUAAAFpAAABjAAAAYsAAAFoAAABaQAAABUA#####wF#f38AAAABAAAABQAAAYsAAAGKAAABZwAAAWgAAAGLAAAAFQD#####AX9#fwAAAAEAAAAFAAABigAAAYkAAAFmAAABZwAAAYoAAAAVAP####8Bf39#AAAAAQAAAAUAAAFlAAABZgAAAYkAAAGIAAABZQAAABUA#####wF#f38AAAABAAAABQAAAWQAAAFlAAABiAAAAYcAAAFkAAAAFQD#####AX9#fwAAAAEAAAAFAAABYwAAAWQAAAGHAAABhgAAAWMAAAAoAP####8B####AAJzNgAAAAQAAAGuAAAAKAD#####Af###wACczcAAAAEAAABrQAAACgA#####wH###8AAnM4AAAABAAAAawAAAAoAP####8B####AAJzOQAAAAQAAAGrAAAAKAD#####Af###wADczEwAAAABAAAAaoAAAAoAP####8B####AANzMTEAAAAEAAABqQAAACgA#####wH###8AA3MxMgAAAAQAAAGoAAAAKAD#####Af###wADczEzAAAABAAAAacAAAAoAP####8B####AANzMTQAAAAEAAABpgAAACgA#####wH###8AA3MxNQAAAAQAAAGlAAAAKAD#####Af###wADczE2AAAABAAAAaQAAAAoAP####8B####AANzMTcAAAAEAAABowAAACgA#####wH###8AA3MxOAAAAAQAAAGiAAAAFQD#####AX9#fwAAAAEAAAAFAAABYwAAAYYAAAGFAAABYgAAAWMAAAAVAP####8Bf39#AAAAAQAAAAUAAAFhAAABYgAAAYUAAAGEAAABYQAAACgA#####wH###8AA3MxOQAAAAQAAAG8AAAAKAD#####Af###wADczIwAAAABAAAAb0AAAAVAP####8Bf39#AAAAAQAAAAUAAADiAAABdQAAAVIAAACKAAAA4gAAABUA#####wF#f38AAAABAAAABQAAAXUAAAF2AAABUwAAAVIAAAF1AAAAFQD#####AX9#fwAAAAEAAAAFAAABdgAAAXcAAAFUAAABUwAAAXYAAAAVAP####8Bf39#AAAAAQAAAAUAAAF3AAABeAAAAVUAAAFUAAABdwAAABUA#####wF#f38AAAABAAAABQAAAXkAAAFWAAABVQAAAXgAAAF5AAAAFQD#####AX9#fwAAAAEAAAAFAAABegAAAXkAAAFWAAABVwAAAXoAAAAVAP####8Bf39#AAAAAQAAAAUAAAF7AAABegAAAVcAAAFYAAABewAAABUA#####wF#f38AAAABAAAABQAAAVkAAAFYAAABewAAAXwAAAFZAAAAFQD#####AX9#fwAAAAEAAAAFAAABWgAAAVkAAAF8AAABfQAAAVoAAAAVAP####8Bf39#AAAAAQAAAAUAAAFbAAABWgAAAX0AAAF+AAABWwAAABUA#####wF#f38AAAABAAAABQAAAYQAAAFhAAABYAAAAYMAAAGEAAAAKAD#####Af###wADczIxAAAABAAAAcAAAAAoAP####8B####AANzMjIAAAAEAAABwQAAACgA#####wH###8AA3MyMwAAAAQAAAHCAAAAKAD#####Af###wADczI0AAAABAAAAcMAAAAoAP####8B####AANzMjUAAAAEAAABxAAAACgA#####wH###8AA3MyNgAAAAQAAAHFAAAAKAD#####Af###wADczI3AAAABAAAAcYAAAAoAP####8B####AANzMjgAAAAEAAABxwAAACgA#####wH###8AA3MyOQAAAAQAAAHIAAAAKAD#####Af###wADczMwAAAABAAAAckAAAAoAP####8B####AANzMzEAAAAEAAABygAAABUA#####wF#f38AAAABAAAABQAAAVsAAAFcAAABfwAAAX4AAAFbAAAAKAD#####Af###wADczMyAAAABAAAAdYAAAAVAP####8Bf39#AAAAAQAAAAUAAAGDAAABggAAAV8AAAFgAAABgwAAACgA#####wH###8AA3MzMwAAAAQAAAHYAAAAIwD#####AAAAiwAAACYAAAABQBQAAAAAAAAAAAAYAP####8Bf39#ABAAAAAAAAAAAAAAAEAIAAAAAAAAAAAFAAAAAIkAAAHaAAAAGAD#####AQAAAAAQAAAAAAAAAAAAAABACAAAAAAAAAAABQAAAAHbAAAB2gAAABgA#####wEAAAAAEAAAAAAAAAAAAAAAQAgAAAAAAAAAAAUAAAAB3AAAAdoAAAAYAP####8BAAAAABAAAAAAAAAAAAAAAEAIAAAAAAAAAAAFAAAAAd0AAAHaAAAAGAD#####AQAAAAAQAAAAAAAAAAAAAABACAAAAAAAAAAABQAAAAHeAAAB2gAAABgA#####wEAAAAAEAAAAAAAAAAAAAAAQAgAAAAAAAAAAAUAAAAB3wAAAdoAAAAYAP####8BAAAAABAAAAAAAAAAAAAAAEAIAAAAAAAAAAAFAAAAAeAAAAHaAAAAGAD#####AQAAAAAQAAAAAAAAAAAAAABACAAAAAAAAAAABQAAAAHhAAAB2gAAABgA#####wEAAAAAEAAAAAAAAAAAAAAAQAgAAAAAAAAAAAUAAAAB4gAAAdoAAAAYAP####8BAAAAABAAAAAAAAAAAAAAAEAIAAAAAAAAAAAFAAAAAeMAAAHaAAAAGAD#####AQAAAAAQAAAAAAAAAAAAAABACAAAAAAAAAAABQAAAAHkAAAB2gAAABgA#####wEAAAAAEAAAAAAAAAAAAAAAQAgAAAAAAAAAAAUAAAAB5QAAAdoAAAAYAP####8BAAAAABAAAAAAAAAAAAAAAEAIAAAAAAAAAAAFAAAAAeYAAAHaAAAAGAD#####AQAAAAAQAAAAAAAAAAAAAABACAAAAAAAAAAABQAAAAHnAAAB2gAAABgA#####wEAAAAAEAAAAAAAAAAAAAAAQAgAAAAAAAAAAAUAAAAB6AAAAdoAAAAYAP####8BAAAAABAAAAAAAAAAAAAAAEAIAAAAAAAAAAAFAAAAAekAAAHaAAAAGAD#####AQAAAAAQAAAAAAAAAAAAAABACAAAAAAAAAAABQAAAAHqAAAB2gAAABgA#####wEAAAAAEAAAAAAAAAAAAAAAQAgAAAAAAAAAAAUAAAAB6wAAAdoAAAAYAP####8BAAAAABAAAAAAAAAAAAAAAEAIAAAAAAAAAAAFAAAAAewAAAHaAAAAGAD#####AQAAAAAQAAAAAAAAAAAAAABACAAAAAAAAAAABQAAAAHtAAAB2gAAABgA#####wEAAAAAEAAAAAAAAAAAAAAAQAgAAAAAAAAAAAUAAAAB7gAAAdoAAAAYAP####8BAAAAABAAAAAAAAAAAAAAAEAIAAAAAAAAAAAFAAAAAe8AAAHaAAAAGAD#####AQAAAAAQAAAAAAAAAAAAAABACAAAAAAAAAAABQAAAAHwAAAB2gAAABgA#####wEAAAAAEAAAAAAAAAAAAAAAQAgAAAAAAAAAAAUAAAAB8QAAAdoAAAAYAP####8BAAAAABAAAAAAAAAAAAAAAEAIAAAAAAAAAAAFAAAAAfIAAAHaAAAAGAD#####AQAAAAAQAAAAAAAAAAAAAABACAAAAAAAAAAABQAAAAHzAAAB2gAAABgA#####wEAAAAAEAAAAAAAAAAAAAAAQAgAAAAAAAAAAAUAAAAB9AAAAdoAAAAYAP####8BAAAAABAAAAAAAAAAAAAAAEAIAAAAAAAAAAAFAAAAAfUAAAHaAAAAGAD#####AQAAAAAQAAAAAAAAAAAAAABACAAAAAAAAAAABQAAAAH2AAAB2gAAABgA#####wEAAAAAEAAAAAAAAAAAAAAAQAgAAAAAAAAAAAUAAAAB9wAAAdoAAAAYAP####8BAAAAABAAAAAAAAAAAAAAAEAIAAAAAAAAAAAFAAAAAfgAAAHaAAAAGAD#####AQAAAAAQAAAAAAAAAAAAAABACAAAAAAAAAAABQAAAAH5AAAB2gAAABgA#####wEAAAAAEAAAAAAAAAAAAAAAQAgAAAAAAAAAAAUAAAAB+gAAAdoAAAAYAP####8BAAAAABAAAAAAAAAAAAAAAEAIAAAAAAAAAAAFAAAAAfsAAAHaAAAAGAD#####AQAAAAAQAAAAAAAAAAAAAABACAAAAAAAAAAABQAAAAH8AAAB2gAAABMA#####wEAAAAAEAAAAQAAAAEAAACJAAAA3AAAAAAUAP####8AAAH+AAAAGAD#####AQAAAAAQAAAAAAAAAAAAAABACAAAAAAAAAAABQAAAAH9AAAB#wAAABgA#####wEAAAAAEAAAAAAAAAAAAAAAQAgAAAAAAAAAAAUAAAAB#AAAAf8AAAAYAP####8BAAAAABAAAAAAAAAAAAAAAEAIAAAAAAAAAAAFAAAAAfsAAAH#AAAAGAD#####AQAAAAAQAAAAAAAAAAAAAABACAAAAAAAAAAABQAAAAH6AAAB#wAAABgA#####wEAAAAAEAAAAAAAAAAAAAAAQAgAAAAAAAAAAAUAAAAB+QAAAf8AAAAYAP####8BAAAAABAAAAAAAAAAAAAAAEAIAAAAAAAAAAAFAAAAAdsAAAH#AAAAGAD#####AQAAAAAQAAAAAAAAAAAAAABACAAAAAAAAAAABQAAAAHcAAAB#wAAABgA#####wEAAAAAEAAAAAAAAAAAAAAAQAgAAAAAAAAAAAUAAAAB3QAAAf8AAAAYAP####8BAAAAABAAAAAAAAAAAAAAAEAIAAAAAAAAAAAFAAAAAd4AAAH#AAAAGAD#####AQAAAAAQAAAAAAAAAAAAAABACAAAAAAAAAAABQAAAAHfAAAB#wAAABgA#####wEAAAAAEAAAAAAAAAAAAAAAQAgAAAAAAAAAAAUAAAAB4AAAAf8AAAAYAP####8BAAAAABAAAAAAAAAAAAAAAEAIAAAAAAAAAAAFAAAAAeEAAAH#AAAAGAD#####AQAAAAAQAAAAAAAAAAAAAABACAAAAAAAAAAABQAAAAHiAAAB#wAAABgA#####wEAAAAAEAAAAAAAAAAAAAAAQAgAAAAAAAAAAAUAAAAB4wAAAf8AAAAYAP####8BAAAAABAAAAAAAAAAAAAAAEAIAAAAAAAAAAAFAAAAAeQAAAH#AAAAGAD#####AQAAAAAQAAAAAAAAAAAAAABACAAAAAAAAAAABQAAAAHlAAAB#wAAABgA#####wEAAAAAEAAAAAAAAAAAAAAAQAgAAAAAAAAAAAUAAAAB5gAAAf8AAAAYAP####8BAAAAABAAAAAAAAAAAAAAAEAIAAAAAAAAAAAFAAAAAecAAAH#AAAAGAD#####AQAAAAAQAAAAAAAAAAAAAABACAAAAAAAAAAABQAAAAHoAAAB#wAAABgA#####wEAAAAAEAAAAAAAAAAAAAAAQAgAAAAAAAAAAAUAAAAB6QAAAf8AAAAYAP####8BAAAAABAAAAAAAAAAAAAAAEAIAAAAAAAAAAAFAAAAAeoAAAH#AAAAGAD#####AQAAAAAQAAAAAAAAAAAAAABACAAAAAAAAAAABQAAAAHrAAAB#wAAABgA#####wEAAAAAEAAAAAAAAAAAAAAAQAgAAAAAAAAAAAUAAAAB7AAAAf8AAAAYAP####8BAAAAABAAAAAAAAAAAAAAAEAIAAAAAAAAAAAFAAAAAe0AAAH#AAAAGAD#####AQAAAAAQAAAAAAAAAAAAAABACAAAAAAAAAAABQAAAAHxAAAB#wAAABgA#####wEAAAAAEAAAAAAAAAAAAAAAQAgAAAAAAAAAAAUAAAAB8gAAAf8AAAAYAP####8BAAAAABAAAAAAAAAAAAAAAEAIAAAAAAAAAAAFAAAAAfMAAAH#AAAAGAD#####AQAAAAAQAAAAAAAAAAAAAABACAAAAAAAAAAABQAAAAH0AAAB#wAAABgA#####wEAAAAAEAAAAAAAAAAAAAAAQAgAAAAAAAAAAAUAAAAB9QAAAf8AAAAYAP####8BAAAAABAAAAAAAAAAAAAAAEAIAAAAAAAAAAAFAAAAAfYAAAH#AAAAGAD#####AQAAAAAQAAAAAAAAAAAAAABACAAAAAAAAAAABQAAAAH3AAAB#wAAABgA#####wEAAAAAEAAAAAAAAAAAAAAAQAgAAAAAAAAAAAUAAAAB+AAAAf8AAAAYAP####8BAAAAABAAAAAAAAAAAAAAAEAIAAAAAAAAAAAFAAAAAfAAAAH#AAAAGAD#####AQAAAAAQAAAAAAAAAAAAAABACAAAAAAAAAAABQAAAAHvAAAB#wAAABgA#####wEAAAAAEAAAAAAAAAAAAAAAQAgAAAAAAAAAAAUAAAAB7gAAAf8AAAAVAP####8BAAAAAAAAAQAAAAUAAADcAAACBQAAAdsAAACJAAAA3AAAABUA#####wEAAAAAAAABAAAABQAAAgUAAAIGAAAB3AAAAdsAAAIFAAAAFQD#####AQAAAAAAAAEAAAAFAAACBwAAAgYAAAHcAAAB3QAAAgcAAAAVAP####8BAAAAAAAAAQAAAAUAAAIIAAACBwAAAd0AAAHeAAACCAAAABUA#####wEAAAAAAAABAAAABQAAAgkAAAIIAAAB3gAAAd8AAAIJAAAAFQD#####AQAAAAAAAAEAAAAFAAACCgAAAgkAAAHfAAAB4AAAAgoAAAAVAP####8BAAAAAAAAAQAAAAUAAAILAAACCgAAAeAAAAHhAAACCwAAABUA#####wEAAAAAAAABAAAABQAAAgwAAAILAAAB4QAAAeIAAAIMAAAAFQD#####AQAAAAAAAAEAAAAFAAACDQAAAgwAAAHiAAAB4wAAAg0AAAAVAP####8BAAAAAAAAAQAAAAUAAAIOAAACDQAAAeMAAAHkAAACDgAAABUA#####wEAAAAAAAABAAAABQAAAg8AAAIOAAAB5AAAAeUAAAIPAAAAFQD#####AQAAAAAAAAEAAAAFAAACEAAAAg8AAAHlAAAB5gAAAhAAAAAVAP####8BAAAAAAAAAQAAAAUAAAIRAAACEAAAAeYAAAHnAAACEQAAABUA#####wEAAAAAAAABAAAABQAAAhIAAAIRAAAB5wAAAegAAAISAAAAFQD#####AQAAAAAAAAEAAAAFAAACEwAAAhIAAAHoAAAB6QAAAhMAAAAVAP####8BAAAAAAAAAQAAAAUAAAIUAAACEwAAAekAAAHqAAACFAAAABUA#####wEAAAAAAAABAAAABQAAAhUAAAIUAAAB6gAAAesAAAIVAAAAFQD#####AQAAAAAAAAEAAAAFAAACFgAAAhUAAAHrAAAB7AAAAhYAAAAVAP####8BAAAAAAAAAQAAAAUAAAIXAAACFgAAAewAAAHtAAACFwAAABUA#####wEAAAAAAAABAAAABQAAAiIAAAIXAAAB7QAAAe4AAAIiAAAAFQD#####AQAAAAAAAAEAAAAFAAACIQAAAiIAAAHuAAAB7wAAAiEAAAAoAP####8B####AANzNDAAAAAEAAACIwAAACgA#####wH###8AA3M0MQAAAAQAAAIkAAAAKAD#####Af###wADczQyAAAABAAAAiUAAAAoAP####8B####AANzNDMAAAAEAAACJgAAACgA#####wH###8AA3M0NAAAAAQAAAInAAAAKAD#####Af###wADczQ1AAAABAAAAigAAAAoAP####8B####AANzNDYAAAAEAAACKQAAACgA#####wH###8AA3M0NwAAAAQAAAIqAAAAKAD#####Af###wADczQ4AAAABAAAAisAAAAoAP####8B####AANzNDkAAAAEAAACLAAAACgA#####wH###8AA3M1MAAAAAQAAAItAAAAKAD#####Af###wADczUxAAAABAAAAi4AAAAoAP####8B####AANzNTIAAAAEAAACLwAAACgA#####wH###8AA3M1MwAAAAQAAAIwAAAAKAD#####Af###wADczU0AAAABAAAAjEAAAAoAP####8B####AANzNTUAAAAEAAACMgAAACgA#####wH###8AA3M1NgAAAAQAAAIzAAAAKAD#####Af###wADczU3AAAABAAAAjQAAAAoAP####8B####AANzNTgAAAAEAAACNQAAACgA#####wH###8AA3M1OQAAAAQAAAI2AAAAKAD#####AQAAAAADczYwAAAABQAAAjcAAAAVAP####8BAAAAAAAAAQAAAAUAAAHvAAAB8AAAAiAAAAIhAAAB7wAAACgA#####wEAAAAAA3M2MQAAAAUAAAJNAAAAFQD#####AQAAAAAAAAEAAAAFAAAA1wAAAgAAAAH9AAAAggAAANcAAAAVAP####8BAAAAAAAAAQAAAAUAAAIAAAACAQAAAfwAAAH9AAACAAAAABUA#####wEAAAAAAAABAAAABQAAAgEAAAICAAAB+wAAAfwAAAIBAAAAFQD#####AQAAAAAAAAEAAAAFAAACAgAAAgMAAAH6AAAB+wAAAgIAAAAVAP####8BAAAAAAAAAQAAAAUAAAIDAAACBAAAAfkAAAH6AAACAwAAABUA#####wEAAAAAAAABAAAABQAAAgQAAAIfAAAB+AAAAfkAAAIEAAAAFQD#####AQAAAAAAAAEAAAAFAAACHwAAAh4AAAH3AAAB+AAAAh8AAAAVAP####8BAAAAAAAAAQAAAAUAAAIeAAACHQAAAfYAAAH3AAACHgAAABUA#####wEAAAAAAAABAAAABQAAAh0AAAIcAAAB9QAAAfYAAAIdAAAAFQD#####AQAAAAAAAAEAAAAFAAACHAAAAhsAAAH0AAAB9QAAAhwAAAAVAP####8BAAAAAAAAAQAAAAUAAAIbAAACGgAAAfMAAAH0AAACGwAAABUA#####wEAAAAAAAABAAAABQAAAfMAAAHyAAACGAAAAhkAAAHzAAAAKAD#####Af###wADczYyAAAABAAAAk8AAAAoAP####8B####AANzNjMAAAAEAAACUAAAACgA#####wH###8AA3M2NAAAAAQAAAJRAAAAKAD#####Af###wADczY1AAAABAAAAlIAAAAoAP####8B####AANzNjYAAAAEAAACUwAAACgA#####wH###8AA3M2NwAAAAQAAAJUAAAAKAD#####Af###wADczY4AAAABAAAAlUAAAAoAP####8B####AANzNjkAAAAEAAACVgAAACgA#####wH###8AA3M3MAAAAAQAAAJXAAAAKAD#####Af###wADczcxAAAABAAAAlgAAAAoAP####8B####AANzNzIAAAAEAAACWQAAACgA#####wH###8AA3M3MwAAAAQAAAJaAAAAGQD#####AAAAAAAQAAABAAJ2MQABAAAAFAAAABUAAAAZAP####8AAAAAABAAAAEAAnYyAAEAAAAUAAAAGgAAABkA#####wEAAAAAEAAAAQACdjMAAQAAABoAAAAXAAAAGQD#####AAAAAAAQAAABAAJ2NAABAAAAFQAAABcAAAAeAP####8BAAAAABAAAAAAAAAAAAAAAEAIAAAAAAAAAAAFAAE#ZIgFIgFIgAAAATg#ZIgFIgFIgAAAAB4A#####wEAAAAAEAAAAAAAAAAAAAAAQAgAAAAAAAAAAAUAAT#v63f63f63AAABOD#v63f63f63AAAAHgD#####AQAAAAAQAAAAAAAAAAAAAABACAAAAAAAAAAABQABP2SIBSIBSIAAAAE2P2SIBSIBSIAAAAAeAP####8BAAAAABAAAAAAAAAAAAAAAEAIAAAAAAAAAAAFAAE#7+t3+t3+twAAATY#7+t3+t3+twAAABkA#####wEAAAAAEAAAAQAAAAEAAADXAAAB#QAAABkA#####wEAAAAAEAAAAQAAAAEAAAGXAAABdAAAABkA#####wEAAAAAEAAAAQACdDEAAQAAAm4AAAJsAAAAGQD#####AQAAAAAQAAABAAJ0MgABAAACbQAAAmsAAAApAP####8B####AAJTRgAAAAQAAAE4AAABNgAAAAj##########w=='
      case 'cy2': return 'TWF0aEdyYXBoSmF2YTEuMAAAABI+TMzNAAJmcv###wEA#wEAAAAAAAAAAANLAAAChwAAAQEAAAAAAAAAAQAAAIT#####AAAAAQAKQ0NhbGNDb25zdAD#####AAJwaQAWMy4xNDE1OTI2NTM1ODk3OTMyMzg0Nv####8AAAABAApDQ29uc3RhbnRlQAkh+1RELRj#####AAAAAQAKQ1BvaW50QmFzZQD#####AAAAAAEQAAFDAAAAAAAAAAAAQAgAAAAAAAAJAAFAVwAAAAAAAEBB8KPXCj1xAAAAAgD#####AQAAAAAQAAFEAAAAAAAAAAAAQAgAAAAAAAAJAAFAVuWzY77EdEBQEMCp6Hxk#####wAAAAEACUNDZXJjbGVPQQD#####AAAAAAABAAAAAQAAAAL#####AAAAAQAJQ0Ryb2l0ZUFCAP####8BAAAAABAAAAEAAQAAAAEAAAAC#####wAAAAEAFkNEcm9pdGVQZXJwZW5kaWN1bGFpcmUA#####wEAAAAAEAAAAQABAAAAAgAAAAQAAAACAP####8BAAAAABAAAUEAAAAAAAAAAABACAAAAAAAAAkAAcBcwAAAAAAAQHJ+FHrhR64AAAACAP####8BAAAAABAAAUIAAAAAAAAAAABACAAAAAAAAAkAAUBCBEpVtmx4QHAYx96gDCT#####AAAAAQAJQ0xvbmd1ZXVyAP####8AAAAGAAAAB#####8AAAABABRDSW1wbGVtZW50YXRpb25Qcm90bwD#####ABJNZXN1cmUgZGUgbG9uZ3VldXIAAAAFAAAAAgAAAAIAAAABAAAAAv####8AAAABAAtDTWVkaWF0cmljZQAAAAAJAQAAAAANAAABAAEAAAABAAAAAv####8AAAABAAdDTWlsaWV1AAAAAAkBAAAAAA0AAAEFAAAAAAEAAAAC#####wAAAAIACUNDZXJjbGVPUgAAAAAJAQAAAAABAAAACwAAAAFAMAAAAAAAAAH#####AAAAAQAQQ0ludERyb2l0ZUNlcmNsZQAAAAAJAAAACgAAAAz#####AAAAAQAQQ1BvaW50TGllQmlwb2ludAAAAAAJAQAAAAANAAABBQABAAAADQAAAAYBAAAACQAAAAEAAAAC#####wAAAAEAD0NWYWxldXJBZmZpY2hlZQEAAAAJAQAAAAEAAAAOEQAAAAAAAQAAAAEAAAABAAAAAAAAAAAAAAAAAQAAAA######AAAAAQAHQ0NhbGN1bAD#####AARyYXlvAAkyKnBpKkNELzL#####AAAAAQAKQ09wZXJhdGlvbgMAAAAPAgAAAA8CAAAAAUAAAAAAAAAA#####wAAAAEAD0NSZXN1bHRhdFZhbGV1cgAAAAAAAAAQAAAADwAAAAFAAAAAAAAAAAAAAAoA#####wEAAAAAAQAAAAIAAAAQAAAAEQAAAAALAP####8AAAAFAAAAEgAAAAwA#####wEAAAAAEAABRQAAAAAAAAAAAEAIAAAAAAAACQABAAAAEwAAAAwA#####wEAAAAAEAAAAAAAAAAAAAAAQAgAAAAAAAAJAAIAAAATAAAABQD#####AQAAAAAQAAABAAEAAAAVAAAABf####8AAAABAA9DUG9pbnRMaWVEcm9pdGUA#####wEAAAAAEAABSAAAAAAAAAAAAEAIAAAAAAAACQABv#Kl9WgqX1UAAAAWAAAABQD#####AQAAAAAQAAABAAEAAAAUAAAABQAAAAUA#####wEAAAAAEAAAAQABAAAAFwAAABb#####AAAAAQAQQ0ludERyb2l0ZURyb2l0ZQD#####AQAAAAAQAAAAAAAAAAAAAABACAAAAAAAAAkAAAAAGAAAABkAAAAJAP####8BAAAAABAAAAAAAAAAAAAAAEAIAAAAAAAACQAAAAAaAAAAF#####8AAAABAAhDVmVjdGV1cgD#####AQAAAAAQAAABAAEAAAABAAAAAgD#####AAAAAQANQ1RyYW5zUGFyVmVjdAD#####AAAAHP####8AAAABAAlDUG9seWdvbmUA#####wAAAAAAAQAAAAUAAAAUAAAAFQAAABcAAAAaAAAAFAAAAAIA#####wAAAAAAEAAAAEAIAAAAAAAAAAAAAAAAAAAFAABAY4AAAAAAAEB#ThR64UeuAAAADgD#####AAVtaW5pMQABMAAAAAEAAAAAAAAAAAAAAA4A#####wAFbWF4aTEAATEAAAABP#AAAAAAAAAAAAAHAP####8AB0N1cnNldXIAAAAFAAAABQAAAAMAAAAgAAAAIQAAAB######AAAAAQAUQ0Ryb2l0ZURpcmVjdGlvbkZpeGUAAAAAIgEAAAAADwAAAQABAAAAHwE#8AAAAAAAAAAAABEBAAAAIgAAAAAAEAAAAMAIAAAAAAAAP#AAAAAAAAAFAABAXUAAAAAAAAAAACP#####AAAAAQALQ0hvbW90aGV0aWUAAAAAIgAAAB8AAAAPAwAAABAAAAAgAAAADwEAAAAQAAAAIAAAABAAAAAh#####wAAAAEAC0NQb2ludEltYWdlAAAAACIBAAAAAA0AAk8xAMAQAAAAAAAAQBAAAAAAAAAFAAAAACQAAAAlAAAAFwAAAAAiAAAAHwAAAA8DAAAADwEAAAABP#AAAAAAAAAAAAAQAAAAIAAAAA8BAAAAEAAAACEAAAAQAAAAIAAAABgAAAAAIgEAAAAADQACSTEAwAAAAAAAAABACAAAAAAAAAUAAAAAJAAAACf#####AAAAAQAIQ1NlZ21lbnQBAAAAIgAAAAAAEAAAAQABAAAAHwAAACQAAAARAQAAACIAAAAAARAAAmsxAMAAAAAAAAAAQAAAAAAAAAABAAEAAAAAAAAAAAAAACn#####AAAAAgAPQ01lc3VyZUFic2Npc3NlAQAAACIAAWEAAAAmAAAAKAAAACoAAAANAQAAACIAAAAAAAAAAAAAAAAAwBgAAAAAAAAAAAAqDwAB####AAAAAQAAAAIAAAABAAAAAAAAAAAAAAAAAgAAACsAAAACAP####8AAAAAABAAAABACAAAAAAAAAAAAAAAAAAABQAAQHYgAAAAAABAf14UeuFHrgAAAA4A#####wAFbWluaTIAATAAAAABAAAAAAAAAAAAAAAOAP####8ABW1heGkyAAExAAAAAT#wAAAAAAAAAAAABwD#####AAdDdXJzZXVyAAAABQAAAAUAAAADAAAALgAAAC8AAAAtAAAAFgAAAAAwAQAAAAAPAAABAAEAAAAtAT#wAAAAAAAAAAAAEQEAAAAwAAAAAAAQAAAAwAgAAAAAAAA#8AAAAAAAAAUAAEBdQAAAAAAAAAAAMQAAABcAAAAAMAAAAC0AAAAPAwAAABAAAAAuAAAADwEAAAAQAAAALgAAABAAAAAvAAAAGAAAAAAwAQAAAAANAAJPMQDAEAAAAAAAAEAQAAAAAAAABQAAAAAyAAAAMwAAABcAAAAAMAAAAC0AAAAPAwAAAA8BAAAAAT#wAAAAAAAAAAAAEAAAAC4AAAAPAQAAABAAAAAvAAAAEAAAAC4AAAAYAAAAADABAAAAAA0AAkkxAMAAAAAAAAAAQAgAAAAAAAAFAAAAADIAAAA1AAAAGQEAAAAwAAAAAAAQAAABAAEAAAAtAAAAMgAAABEBAAAAMAAAAAABEAABawDAAAAAAAAAAEAAAAAAAAAAAQABAAAAAAAAAAAAAAA3AAAAGgEAAAAwAAFiAAAANAAAADYAAAA4AAAADQEAAAAwAAAAAAAAAAAAAAAAAMAYAAAAAAAAAAAAOA8AAf###wAAAAEAAAACAAAAAQAAAAAAAAAAAAAAAAIAAAA5AAAADgD#####AAJhYQABYQAAABAAAAArAAAADgD#####AAJiYgABYgAAABAAAAA5AAAAEwD#####Af8AAAAQAAABAAEAAAAaAAAAFwAAAAAUAP####8AAAA9#####wAAAAEAEENEcm9pdGVQYXJhbGxlbGUA#####wH#AAAAEAAAAQABAAAAAQAAAAUAAAASAP####8B#wAAABAAAAAAAAAAAAAAAEAIAAAAAAAABQAAAAAYAAAAPwAAABMA#####wH#AAAAEAAAAQABAAAAQAAAAAEAAAAAEwD#####Af8AAAAQAAABAAEAAAAUAAAAAgAAAAALAP####8AAAA#AAAAAwAAAAwA#####wH#AAAAEAAAAAAAAAAAAAAAQAgAAAAAAAAFAAIAAABDAAAADAD#####Af8AAAAQAAAAAAAAAAAAAABACAAAAAAAAAUAAQAAAEMAAAATAP####8B#wAAABAAAAEAAQAAAAEAAABEAP####8AAAABAA9DUG9pbnRMaWVDZXJjbGUA#####wH#AAAAEAAAAAAAAAAAAAAAQAgAAAAAAAAIAAFAAA80C4Gk8AAAAAMAAAAbAP####8B#wAAABAAAAEAAQAAAEcAAAAFAAAAEgD#####Af8AAAAQAAAAAAAAAAAAAABACAAAAAAAAAgAAAAASAAAABgAAAATAP####8B#wAAABAAAAEAAQAAAEkAAABHAAAAABQA#####wAAAEoAAAAYAP####8BAAAAABAAAAAAAAAAAAAAAEAIAAAAAAAACQAAAAAbAAAAHQAAAAMA#####wEAAAAAAQAAAEwAAAAbAAAAHAD#####Af8AAAAQAAAAAAAAAAAAAABACAAAAAAAAAkAAUAYr3xhI#YAAAAATQAAABsA#####wH#AAAAEAAAAQABAAAATgAAABkAAAASAP####8B#wAAABAAAAAAAAAAAAAAAEAIAAAAAAAACQAAAABPAAAAGAAAABMA#####wH#AAAAEAAAAQABAAAAUAAAAE4AAAAAFAD#####AAAAUQAAAAsA#####wAAAAQAAABNAAAADAD#####Af8AAAAQAAFPAAAAAAAAAAAAQAgAAAAAAAAJAAIAAABTAAAADAD#####Af8AAAAQAAFKAAAAAAAAAAAAQAgAAAAAAAAJAAEAAABT#####wAAAAEAEUNTeW1ldHJpZUNlbnRyYWxlAP####8AAAAbAAAAGAD#####AQAA#wAQAAFHAAAAAAAAAAAAQAgAAAAAAAAJAAAAAFUAAABWAAAAGQD#####AQAA#wAQAAABAAEAAABXAAAAGgAAABkA#####wEAAP8AEAAAAQABAAAAVwAAABcAAAAHAP####8AEk1lc3VyZSBkZSBsb25ndWV1cgAAAAUAAAACAAAAAgAAAFcAAAAXAAAACAAAAABaAQAAAAANAAABAAEAAABXAAAAFwAAAAkAAAAAWgEAAAAADQAAAQUAAAAAVwAAABcAAAAKAAAAAFoBAAAAAAEAAABcAAAAAUAwAAAAAAAAAQAAAAsAAAAAWgAAAFsAAABdAAAADAAAAABaAQAAAAANAAABBQABAAAAXgAAAAYBAAAAWgAAAFcAAAAXAAAADQEAAABaAQAA#wEAAABfEQAAAAAAAQAAAAEAAAABAAAAAAAAAAAAAAAAAQAAAGAAAAAKAP####8BAAD#AAEAAABXAAAADwIAAAAPAQAAAAE#8AAAAAAAAAAAABAAAAA8AAAAEAAAAGAAAAAACwD#####AAAAWAAAAGIAAAAMAP####8BAAD#ABAAAAAAAAAAAAAAAEAIAAAAAAAACQABAAAAYwAAAAwA#####wAAAP8AEAAAAAAAAAAAAAAAQAgAAAAAAAAJAAIAAABjAAAACwD#####AAAAWQAAAGIAAAAMAP####8BAAD#ABAAAAAAAAAAAAAAAEAIAAAAAAAACQABAAAAZgAAAAwA#####wAAAP8AEAAAAAAAAAAAAAAAQAgAAAAAAAAJAAIAAABmAAAAGwD#####AQAA#wAQAAABAAEAAABkAAAABAAAABsA#####wEAAP8AEAAAAQABAAAAZwAAAAQAAAASAP####8BAAD#ABAAAAAAAAAAAAAAAEAIAAAAAAAACQAAAABpAAAAGQAAABIA#####wEAAP8AEAAAAAAAAAAAAAAAQAgAAAAAAAAJAAAAAGoAAAAZAAAACQD#####AQAA#wAQAAAAAAAAAAAAAABACAAAAAAAAAkAAAAAZAAAAGsAAAAJAP####8BAAD#ABAAAAAAAAAAAAAAAEAIAAAAAAAACQAAAABnAAAAbAAAABkA#####wAAAAAAEAAAAQABAAAAawAAABsAAAAZAP####8AAAAAABAAAAEAAQAAABsAAABs#####wAAAAEAEkNBcmNEZUNlcmNsZURpcmVjdAD#####AAAA#wABAAAAbgAAAGwAAABnAAAAHgD#####AAAA#wABAAAAbQAAAGQAAABrAAAAGwD#####AQAA#wAQAAABAAEAAABVAAAAGQAAABIA#####wEAAP8AEAAAAAAAAAAAAAAAQAgAAAAAAAAJAAAAABgAAABzAAAAEwD#####AQAA#wAQAAABAAEAAAB0AAAAVQAAAAAUAP####8AAAB1AAAABwD#####ABJNZXN1cmUgZGUgbG9uZ3VldXIAAAAFAAAAAgAAAAIAAABVAAAAVwAAAAgAAAAAdwEAAAAADQAAAQABAAAAVQAAAFcAAAAJAAAAAHcBAAAAAA0AAAEFAAAAAFUAAABXAAAACgAAAAB3AQAAAAABAAAAeQAAAAFAMAAAAAAAAAEAAAALAAAAAHcAAAB4AAAAegAAAAwAAAAAdwEAAAAADQAAAQUAAQAAAHsAAAAGAQAAAHcAAABVAAAAVwAAAA0BAAAAdwH#AP8BAAAAfBEAAAAAAAEAAAABAAAAAQAAAAAAAAAAAAAAAAEAAAB9AAAACgD#####Af8A#wABAAAAVQAAAA8CAAAAEAAAADwAAAAQAAAAfQAAAAAUAP####8AAABGAAAAFAD#####AAAAQgAAABMA#####wEAAP8AEAAAAQABAAAAVQAAAFQAAAAAFAD#####AAAAggAAAAj##########w=='
      case 'cy3': return 'TWF0aEdyYXBoSmF2YTEuMAAAABI+TMzNAAJmcv###wEA#wEAAAAAAAAAAAM7AAACdwAAAQEAAAAAAAAAAQAAAI3#####AAAAAQAKQ0NhbGNDb25zdAD#####AAJwaQAWMy4xNDE1OTI2NTM1ODk3OTMyMzg0Nv####8AAAABAApDQ29uc3RhbnRlQAkh+1RELRj#####AAAAAQAKQ1BvaW50QmFzZQD#####AAAAAAEQAAFDAAAAAAAAAAAAQAgAAAAAAAAJAABAVwAAAAAAAEBB8KPXCj1xAAAAAgD#####AQAAAAAQAAFEAAAAAAAAAAAAQAgAAAAAAAAJAAFAVuWzY77EdEBQEMCp6Hxk#####wAAAAEACUNDZXJjbGVPQQD#####AAAAAAABAAAAAQAAAAL#####AAAAAQAJQ0Ryb2l0ZUFCAP####8BAAAAABAAAAEAAQAAAAEAAAAC#####wAAAAEAFkNEcm9pdGVQZXJwZW5kaWN1bGFpcmUA#####wEAAAAAEAAAAQABAAAAAgAAAAQAAAACAP####8BAAAAABAAAUEAAAAAAAAAAABACAAAAAAAAAkAAcBcwAAAAAAAQHJ+FHrhR64AAAACAP####8BAAAAABAAAUIAAAAAAAAAAABACAAAAAAAAAkAAUBCBEpVtmx4QHAYx96gDCT#####AAAAAQAJQ0xvbmd1ZXVyAP####8AAAAGAAAAB#####8AAAABABRDSW1wbGVtZW50YXRpb25Qcm90bwD#####ABJNZXN1cmUgZGUgbG9uZ3VldXIAAAAFAAAAAgAAAAIAAAABAAAAAv####8AAAABAAtDTWVkaWF0cmljZQAAAAAJAQAAAAANAAABAAEAAAABAAAAAv####8AAAABAAdDTWlsaWV1AAAAAAkBAAAAAA0AAAEFAAAAAAEAAAAC#####wAAAAIACUNDZXJjbGVPUgAAAAAJAQAAAAABAAAACwAAAAFAMAAAAAAAAAH#####AAAAAQAQQ0ludERyb2l0ZUNlcmNsZQAAAAAJAAAACgAAAAz#####AAAAAQAQQ1BvaW50TGllQmlwb2ludAAAAAAJAQAAAAANAAABBQABAAAADQAAAAYBAAAACQAAAAEAAAAC#####wAAAAEAD0NWYWxldXJBZmZpY2hlZQEAAAAJAQAAAAEAAAAOEQAAAAAAAQAAAAEAAAABAAAAAAAAAAAAAAAAAQAAAA######AAAAAQAHQ0NhbGN1bAD#####AARyYXlvAAkyKnBpKkNELzL#####AAAAAQAKQ09wZXJhdGlvbgMAAAAPAgAAAA8CAAAAAUAAAAAAAAAA#####wAAAAEAD0NSZXN1bHRhdFZhbGV1cgAAAAAAAAAQAAAADwAAAAFAAAAAAAAAAAAAAAoA#####wEAAAAAAQAAAAIAAAAQAAAAEQAAAAALAP####8AAAAFAAAAEgAAAAwA#####wEAAAAAEAABRQAAAAAAAAAAAEAIAAAAAAAACQABAAAAEwAAAAwA#####wEAAAAAEAAAAAAAAAAAAAAAQAgAAAAAAAAJAAIAAAATAAAABQD#####AQAAAAAQAAABAAEAAAAVAAAABf####8AAAABAA9DUG9pbnRMaWVEcm9pdGUA#####wEAAAAAEAABSAAAAAAAAAAAAEAIAAAAAAAACQABv#Kl9WgqX1UAAAAWAAAABQD#####AQAAAAAQAAABAAEAAAAUAAAABQAAAAUA#####wEAAAAAEAAAAQABAAAAFwAAABb#####AAAAAQAQQ0ludERyb2l0ZURyb2l0ZQD#####AQAAAAAQAAAAAAAAAAAAAABACAAAAAAAAAkAAAAAGAAAABkAAAAJAP####8BAAAAABAAAAAAAAAAAAAAAEAIAAAAAAAACQAAAAAaAAAAF#####8AAAABAAhDVmVjdGV1cgD#####AQAAAAAQAAABAAEAAAABAAAAAgD#####AAAAAQANQ1RyYW5zUGFyVmVjdAD#####AAAAHP####8AAAABAAlDUG9seWdvbmUA#####wEAAAAAAQAAAAUAAAAUAAAAFQAAABcAAAAaAAAAFAAAAAIA#####wAAAAAAEAAAAEAIAAAAAAAAAAAAAAAAAAAFAABAY4AAAAAAAEB#ThR64UeuAAAADgD#####AAVtaW5pMQABMAAAAAEAAAAAAAAAAAAAAA4A#####wAFbWF4aTEAATEAAAABP#AAAAAAAAAAAAAHAP####8AB0N1cnNldXIAAAAFAAAABQAAAAMAAAAgAAAAIQAAAB######AAAAAQAUQ0Ryb2l0ZURpcmVjdGlvbkZpeGUAAAAAIgEAAAAADwAAAQABAAAAHwE#8AAAAAAAAAAAABEBAAAAIgAAAAAAEAAAAMAIAAAAAAAAP#AAAAAAAAAFAABAXUAAAAAAAAAAACP#####AAAAAQALQ0hvbW90aGV0aWUAAAAAIgAAAB8AAAAPAwAAABAAAAAgAAAADwEAAAAQAAAAIAAAABAAAAAh#####wAAAAEAC0NQb2ludEltYWdlAAAAACIBAAAAAA0AAk8xAMAQAAAAAAAAQBAAAAAAAAAFAAAAACQAAAAlAAAAFwAAAAAiAAAAHwAAAA8DAAAADwEAAAABP#AAAAAAAAAAAAAQAAAAIAAAAA8BAAAAEAAAACEAAAAQAAAAIAAAABgAAAAAIgEAAAAADQACSTEAwAAAAAAAAABACAAAAAAAAAUAAAAAJAAAACf#####AAAAAQAIQ1NlZ21lbnQBAAAAIgAAAAAAEAAAAQABAAAAHwAAACQAAAARAQAAACIAAAAAARAAAmsxAMAAAAAAAAAAQAAAAAAAAAABAAEAAAAAAAAAAAAAACn#####AAAAAgAPQ01lc3VyZUFic2Npc3NlAQAAACIAAWEAAAAmAAAAKAAAACoAAAANAQAAACIAAAAAAAAAAAAAAAAAwBgAAAAAAAAAAAAqDwAB####AAAAAQAAAAIAAAABAAAAAAAAAAAAAAAAAgAAACsAAAACAP####8AAAAAABAAAABACAAAAAAAAAAAAAAAAAAABQAAQHYgAAAAAABAf14UeuFHrgAAAA4A#####wAFbWluaTIAATAAAAABAAAAAAAAAAAAAAAOAP####8ABW1heGkyAAExAAAAAT#wAAAAAAAAAAAABwD#####AAdDdXJzZXVyAAAABQAAAAUAAAADAAAALgAAAC8AAAAtAAAAFgAAAAAwAQAAAAAPAAABAAEAAAAtAT#wAAAAAAAAAAAAEQEAAAAwAAAAAAAQAAAAwAgAAAAAAAA#8AAAAAAAAAUAAEBdQAAAAAAAAAAAMQAAABcAAAAAMAAAAC0AAAAPAwAAABAAAAAuAAAADwEAAAAQAAAALgAAABAAAAAvAAAAGAAAAAAwAQAAAAANAAJPMQDAEAAAAAAAAEAQAAAAAAAABQAAAAAyAAAAMwAAABcAAAAAMAAAAC0AAAAPAwAAAA8BAAAAAT#wAAAAAAAAAAAAEAAAAC4AAAAPAQAAABAAAAAvAAAAEAAAAC4AAAAYAAAAADABAAAAAA0AAkkxAMAAAAAAAAAAQAgAAAAAAAAFAAAAADIAAAA1AAAAGQEAAAAwAAAAAAAQAAABAAEAAAAtAAAAMgAAABEBAAAAMAAAAAABEAABawDAAAAAAAAAAEAAAAAAAAAAAQABAAAAAAAAAAAAAAA3AAAAGgEAAAAwAAFiAAAANAAAADYAAAA4AAAADQEAAAAwAAAAAAAAAAAAAAAAAMAYAAAAAAAAAAAAOA8AAf###wAAAAEAAAACAAAAAQAAAAAAAAAAAAAAAAIAAAA5AAAADgD#####AAJhYQABYQAAABAAAAArAAAADgD#####AAJiYgABYgAAABAAAAA5AAAAEwD#####Af8AAAAQAAABAAEAAAAaAAAAFwAAAAAUAP####8AAAA9#####wAAAAEAEENEcm9pdGVQYXJhbGxlbGUA#####wH#AAAAEAAAAQABAAAAAQAAAAUAAAASAP####8B#wAAABAAAAAAAAAAAAAAAEAIAAAAAAAABQAAAAAYAAAAPwAAABMA#####wH#AAAAEAAAAQABAAAAQAAAAAEAAAAAEwD#####Af8AAAAQAAABAAEAAAAUAAAAAgAAAAALAP####8AAAA#AAAAAwAAAAwA#####wH#AAAAEAAAAAAAAAAAAAAAQAgAAAAAAAAFAAIAAABDAAAADAD#####Af8AAAAQAAAAAAAAAAAAAABACAAAAAAAAAUAAQAAAEMAAAATAP####8B#wAAABAAAAEAAQAAAAEAAABEAP####8AAAABAA9DUG9pbnRMaWVDZXJjbGUA#####wH#AAAAEAAAAAAAAAAAAAAAQAgAAAAAAAAIAAFAAA80C4Gk8AAAAAMAAAAbAP####8B#wAAABAAAAEAAQAAAEcAAAAFAAAAEgD#####Af8AAAAQAAAAAAAAAAAAAABACAAAAAAAAAgAAAAASAAAABgAAAATAP####8B#wAAABAAAAEAAQAAAEkAAABHAAAAABQA#####wAAAEoAAAAYAP####8AAAAAABAAAAAAAAAAAAAAAEAIAAAAAAAACQAAAAAbAAAAHQAAAAMA#####wAAAAAAAQAAAEwAAAAbAAAAHAD#####Af8AAAAQAAAAAAAAAAAAAABACAAAAAAAAAkAAUAYr3xhI#YAAAAATQAAABsA#####wH#AAAAEAAAAQABAAAATgAAABkAAAASAP####8B#wAAABAAAAAAAAAAAAAAAEAIAAAAAAAACQAAAABPAAAAGAAAABMA#####wH#AAAAEAAAAQABAAAAUAAAAE4AAAAAFAD#####AAAAUQAAAAsA#####wAAAAQAAABNAAAADAD#####Af8AAAAQAAFPAAAAAAAAAAAAQAgAAAAAAAAJAAIAAABTAAAADAD#####Af8AAAAQAAFKAAAAAAAAAAAAQAgAAAAAAAAJAAEAAABT#####wAAAAEAEUNTeW1ldHJpZUNlbnRyYWxlAP####8AAAAbAAAAGAD#####AQAA#wAQAAFHAAAAAAAAAAAAQAgAAAAAAAAJAAAAAFUAAABWAAAAGQD#####AQAA#wAQAAABAAEAAABXAAAAGgAAABkA#####wEAAP8AEAAAAQABAAAAVwAAABcAAAAHAP####8AEk1lc3VyZSBkZSBsb25ndWV1cgAAAAUAAAACAAAAAgAAAFcAAAAXAAAACAAAAABaAQAAAAANAAABAAEAAABXAAAAFwAAAAkAAAAAWgEAAAAADQAAAQUAAAAAVwAAABcAAAAKAAAAAFoBAAAAAAEAAABcAAAAAUAwAAAAAAAAAQAAAAsAAAAAWgAAAFsAAABdAAAADAAAAABaAQAAAAANAAABBQABAAAAXgAAAAYBAAAAWgAAAFcAAAAXAAAADQEAAABaAQAA#wEAAABfEQAAAAAAAQAAAAEAAAABAAAAAAAAAAAAAAAAAQAAAGAAAAAKAP####8BAAD#AAEAAABXAAAADwIAAAAPAQAAAAE#8AAAAAAAAAAAABAAAAA8AAAAEAAAAGAAAAAACwD#####AAAAWAAAAGIAAAAMAP####8BAAD#ABAAAAAAAAAAAAAAAEAIAAAAAAAACQABAAAAYwAAAAwA#####wAAAP8AEAAAAAAAAAAAAAAAQAgAAAAAAAAJAAIAAABjAAAACwD#####AAAAWQAAAGIAAAAMAP####8BAAD#ABAAAAAAAAAAAAAAAEAIAAAAAAAACQABAAAAZgAAAAwA#####wAAAP8AEAAAAAAAAAAAAAAAQAgAAAAAAAAJAAIAAABmAAAAGwD#####AQAA#wAQAAABAAEAAABkAAAABAAAABsA#####wEAAP8AEAAAAQABAAAAZwAAAAQAAAASAP####8BAAD#ABAAAAAAAAAAAAAAAEAIAAAAAAAACQAAAABpAAAAGQAAABIA#####wEAAP8AEAAAAAAAAAAAAAAAQAgAAAAAAAAJAAAAAGoAAAAZAAAACQD#####AQAA#wAQAAAAAAAAAAAAAABACAAAAAAAAAkAAAAAZAAAAGsAAAAJAP####8BAAD#ABAAAAAAAAAAAAAAAEAIAAAAAAAACQAAAABnAAAAbAAAABkA#####wEAAAAAEAAAAQABAAAAawAAABsAAAAZAP####8BAAAAABAAAAEAAQAAABsAAABs#####wAAAAEAEkNBcmNEZUNlcmNsZURpcmVjdAD#####AAAA#wABAAAAbgAAAGwAAABnAAAAHgD#####AAAA#wABAAAAbQAAAGQAAABrAAAAGwD#####AQAA#wAQAAABAAEAAABVAAAAGQAAABIA#####wEAAP8AEAAAAAAAAAAAAAAAQAgAAAAAAAAJAAAAABgAAABzAAAAEwD#####AQAA#wAQAAABAAEAAAB0AAAAVQAAAAAUAP####8AAAB1AAAABwD#####ABJNZXN1cmUgZGUgbG9uZ3VldXIAAAAFAAAAAgAAAAIAAABVAAAAVwAAAAgAAAAAdwEAAAAADQAAAQABAAAAVQAAAFcAAAAJAAAAAHcBAAAAAA0AAAEFAAAAAFUAAABXAAAACgAAAAB3AQAAAAABAAAAeQAAAAFAMAAAAAAAAAEAAAALAAAAAHcAAAB4AAAAegAAAAwAAAAAdwEAAAAADQAAAQUAAQAAAHsAAAAGAQAAAHcAAABVAAAAVwAAAA0BAAAAdwH#AP8BAAAAfBEAAAAAAAEAAAABAAAAAQAAAAAAAAAAAAAAAAEAAAB9AAAACgD#####Af8A#wABAAAAVQAAAA8CAAAAEAAAADwAAAAQAAAAfQAAAAAUAP####8AAABGAAAAFAD#####AAAAQgAAABMA#####wEAAP8AEAAAAQABAAAAVQAAAFQAAAAAFAD#####AAAAggAAAAkA#####wEAAAAAEAAAAAAAAAAAAAAAQAgAAAAAAAAFAAAAAAIAAAAVAAAACQD#####AQAAAAAQAAAAAAAAAAAAAABACAAAAAAAAAUAAAAAVAAAAG4AAAAJAP####8BAAAAABAAAAAAAAAAAAAAAEAIAAAAAAAABQAAAAAUAAAAAgAAAAkA#####wEAAAAAEAAAAAAAAAAAAAAAQAgAAAAAAAAFAAAAAG0AAABUAAAACQD#####AQAAAAAQAAAAAAAAAAAAAABACAAAAAAAAAUAAAAAhgAAAAIAAAAJAP####8BAAAAABAAAAAAAAAAAAAAAEAIAAAAAAAABQAAAACHAAAAVAAAAAkA#####wEAAAAAEAAAAAAAAAAAAAAAQAgAAAAAAAAFAAAAAAIAAACEAAAACQD#####AQAAAAAQAAAAAAAAAAAAAABACAAAAAAAAAUAAAAAhQAAAFQAAAAVAP####8AAAAAAAEAAAAFAAAAiAAAAIoAAACLAAAAiQAAAIgAAAAI##########8='
      case 'co1': return 'TWF0aEdyYXBoSmF2YTEuMAAAABM+TMzNAAJmcv###wEA#wEAAAAAAAAAAANLAAAChwAAAQEAAAAAAAAAAQAAAhj#####AAAAAQAKQ0NhbGNDb25zdAD#####AAJwaQAWMy4xNDE1OTI2NTM1ODk3OTMyMzg0Nv####8AAAABAApDQ29uc3RhbnRlQAkh+1RELRj#####AAAAAQAKQ1BvaW50QmFzZQD#####AQAAAAAQAAFDAAAAAAAAAAAAQAgAAAAAAAAAAAkAAUBFRgl2ZH0QQFXkah84qgIAAAACAP####8BAAAAABAAAUQAAAAAAAAAAABACAAAAAAAAAAABQABQFSF4fc+4uxAVeRqHziqAv####8AAAABAAlDQ2VyY2xlT0EA#####wAAAAAAAnMzAAEAAAABAAAAAv####8AAAABABFDU3ltZXRyaWVDZW50cmFsZQD#####AAAAAv####8AAAABAAtDUG9pbnRJbWFnZQD#####AQAAAAAQAAAAAAAAAAAAAABACAAAAAAAAAAABQAAAAABAAAABP####8AAAABAA1DRGVtaURyb2l0ZU9BAP####8BAAAAAA0AAAEAAAABAAAAAgAAAAX#####AAAAAQAPQ1BvaW50TGllRHJvaXRlAP####8BAAAAABAAAUUAAAAAAAAAAABACAAAAAAAAAAABQABP#q6VFq6VFwAAAAGAAAAAgD#####AQAAAAAQAAFBAAAAAAAAAAAAQAgAAAAAAAAAAAUAAUCjjgAAAAAAQKCNwo9cKPYAAAACAP####8BAAAAABAAAUIAAAAAAAAAAABACAAAAAAAAAAABQABQDd#h9z7i#BAZhnDSH#jPP####8AAAABAAlDTG9uZ3VldXIA#####wAAAAgAAAAJ#####wAAAAEAFENJbXBsZW1lbnRhdGlvblByb3RvAP####8AEk1lc3VyZSBkZSBsb25ndWV1cgAAAAUAAAACAAAAAgAAAAEAAAAC#####wAAAAEAC0NNZWRpYXRyaWNlAAAAAAsBAAAAABAAAAEAAAABAAAAAQAAAAL#####AAAAAQAHQ01pbGlldQAAAAALAQAAAAAQAAABAAAFAAAAAAEAAAAC#####wAAAAIACUNDZXJjbGVPUgAAAAALAQAAAAAAAAEAAAANAAAAAUAwAAAAAAAAAf####8AAAABABBDSW50RHJvaXRlQ2VyY2xlAAAAAAsAAAAMAAAADv####8AAAABABBDUG9pbnRMaWVCaXBvaW50AAAAAAsBAAAAABAAAAEAAAUAAQAAAA8AAAAIAQAAAAsAAAABAAAAAv####8AAAABAA9DVmFsZXVyQWZmaWNoZWUBAAAACwEAAAABAAAAAAAQEQAAAAAAAQAAAAEAAAABAAAAAAAAAAAAAAAAAQAAABH#####AAAAAQAHQ0NhbGN1bAD#####AANwZXIABzIqcGkqQ0T#####AAAAAQAKQ09wZXJhdGlvbgIAAAARAgAAAAFAAAAAAAAAAP####8AAAABAA9DUmVzdWx0YXRWYWxldXIAAAAAAAAAEgAAABEAAAAJAP####8AEk1lc3VyZSBkZSBsb25ndWV1cgAAAAUAAAACAAAAAgAAAAIAAAAHAAAACgAAAAAUAQAAAAAQAAABAAAAAQAAAAIAAAAHAAAACwAAAAAUAQAAAAAQAAABAAAFAAAAAAIAAAAHAAAADAAAAAAUAQAAAAAAAAEAAAAWAAAAAUAwAAAAAAAAAQAAAA0AAAAAFAAAABUAAAAXAAAADgAAAAAUAQAAAAAQAAABAAAFAAEAAAAYAAAACAEAAAAUAAAAAgAAAAcAAAAPAQAAABQBAAAAAQAAAAAAGREAAAAAAAEAAAABAAAAAQAAAAAAAAAAAAAAAAEAAAAaAAAAEAD#####AAVhbHBoYQAJQ0QqMzYwL0RFAAAAEQMAAAARAgAAABIAAAARAAAAAUB2gAAAAAAAAAAAEgAAABr#####AAAAAQAJQ1JvdGF0aW9uAP####8AAAAHAAAAEQMAAAASAAAAHAAAAAFAAAAAAAAAAAAAAAUA#####wEAAAAAEAAAAAAAAAAAAAAAQAgAAAAAAAAAAAUAAAAAAgAAAB0AAAATAP####8AAAAH#####wAAAAEADENNb2luc1VuYWlyZQAAABEDAAAAEgAAABwAAAABQAAAAAAAAAAAAAAFAP####8BAAAAABAAAAAAAAAAAAAAAEAIAAAAAAAAAAAFAAAAAAIAAAAf#####wAAAAEAEkNBcmNEZUNlcmNsZURpcmVjdAD#####AAAAAAADczMzAAEAAAAHAAAAIAAAAB7#####AAAAAQAIQ1NlZ21lbnQA#####wAAAAAAEAAAAQAAAAEAAAAeAAAABwAAABYA#####wAAAAAAEAAAAQAAAAEAAAAHAAAAIAAAAAIA#####wAAAAAAEAAAAEAIAAAAAAAAAAAAAAAAAAAAAAUAAEBjMAAAAAAAQH7NcKPXCj4AAAAQAP####8ABW1pbmkxAAEwAAAAAQAAAAAAAAAAAAAAEAD#####AAVtYXhpMQABMQAAAAE#8AAAAAAAAAAAAAkA#####wAHQ3Vyc2V1cgAAAAUAAAAFAAAAAwAAACUAAAAmAAAAJP####8AAAABABRDRHJvaXRlRGlyZWN0aW9uRml4ZQAAAAAnAQAAAAAQAAABAAAAAQAAACQBP#AAAAAAAAAAAAAHAQAAACcAAAAAABAAAADACAAAAAAAAD#wAAAAAAAAAAAFAABAXUAAAAAAAAAAACj#####AAAAAQALQ0hvbW90aGV0aWUAAAAAJwAAACQAAAARAwAAABIAAAAlAAAAEQEAAAASAAAAJQAAABIAAAAmAAAABQAAAAAnAQAAAAANAAJPMQDAEAAAAAAAAEAQAAAAAAAAAAAFAAAAACkAAAAqAAAAGAAAAAAnAAAAJAAAABEDAAAAEQEAAAABP#AAAAAAAAAAAAASAAAAJQAAABEBAAAAEgAAACYAAAASAAAAJQAAAAUAAAAAJwEAAAAADQACSTEAwAAAAAAAAABACAAAAAAAAAAABQAAAAApAAAALAAAABYBAAAAJwAAAAAAEAAAAQAAAAEAAAAkAAAAKQAAAAcBAAAAJwAAAAABEAACazEAwAAAAAAAAABAAAAAAAAAAAAAAQABAAAAAAAAAAAAAAAu#####wAAAAIAD0NNZXN1cmVBYnNjaXNzZQEAAAAnAAFhAAAAKwAAAC0AAAAvAAAADwEAAAAnAAAAAAAAAAAAAAAAAMAYAAAAAAAAAAAAAAAvDwAB####AAAAAQAAAAIAAAABAAAAAAAAAAAAAAAAAgAAADAAAAACAP####8AAAAAABAAAABACAAAAAAAAAAAAAAAAAAAAAAFAABAdHgAAAAAAEB+vXCj1wo+AAAAEAD#####AAVtaW5pMgABMAAAAAEAAAAAAAAAAAAAABAA#####wAFbWF4aTIAATEAAAABP#AAAAAAAAAAAAAJAP####8AB0N1cnNldXIAAAAFAAAABQAAAAMAAAAzAAAANAAAADIAAAAXAAAAADUBAAAAABAAAAEAAAABAAAAMgE#8AAAAAAAAAAAAAcBAAAANQAAAAAAEAAAAMAIAAAAAAAAP#AAAAAAAAAAAAUAAEBdQAAAAAAAAAAANgAAABgAAAAANQAAADIAAAARAwAAABIAAAAzAAAAEQEAAAASAAAAMwAAABIAAAA0AAAABQAAAAA1AQAAAAANAAJPMQDAEAAAAAAAAEAQAAAAAAAAAAAFAAAAADcAAAA4AAAAGAAAAAA1AAAAMgAAABEDAAAAEQEAAAABP#AAAAAAAAAAAAASAAAAMwAAABEBAAAAEgAAADQAAAASAAAAMwAAAAUAAAAANQEAAAAADQACSTEAwAAAAAAAAABACAAAAAAAAAAABQAAAAA3AAAAOgAAABYBAAAANQAAAAAAEAAAAQAAAAEAAAAyAAAANwAAAAcBAAAANQAAAAABEAABawDAAAAAAAAAAEAAAAAAAAAAAAABAAEAAAAAAAAAAAAAADwAAAAZAQAAADUAAWIAAAA5AAAAOwAAAD0AAAAPAQAAADUAAAAAAAAAAAAAAAAAwBgAAAAAAAAAAAAAAD0PAAH###8AAAABAAAAAgAAAAEAAAAAAAAAAAAAAAACAAAAPgAAABAA#####wACYWEAAWEAAAASAAAAMAAAABAA#####wACYmIAAWIAAAASAAAAPv####8AAAABABZDRHJvaXRlUGVycGVuZGljdWxhaXJlAP####8BAAAAABAAAAEAAAABAAAAAQAAAAYAAAANAP####8AAABCAAAAAwAAAA4A#####wEAAAAAEAAAAAAAAAAAAAAAQAgAAAAAAAAAAAUAAQAAAEMAAAAOAP####8BAAAAABAAAUYAAAAAAAAAAABACAAAAAAAAAAABQACAAAAQwAAAAIA#####wEAAAAAEAABRwAAAAAAAAAAAEAIAAAAAAAAAAAFAAFAUGx##ZEPHEBPfK5RZ9i0AAAAFgD#####AQAAAAAQAAABAAAAAQAAAEUAAABGAAAACQD#####ABJNZXN1cmUgZGUgbG9uZ3VldXIAAAAFAAAAAgAAAAIAAABFAAAARgAAAAoAAAAASAEAAAAAEAAAAQAAAAEAAABFAAAARgAAAAsAAAAASAEAAAAAEAAAAQAABQAAAABFAAAARgAAAAwAAAAASAEAAAAAAAABAAAASgAAAAFAMAAAAAAAAAEAAAANAAAAAEgAAABJAAAASwAAAA4AAAAASAEAAAAAEAAAAQAABQABAAAATAAAAAgBAAAASAAAAEUAAABGAAAADwEAAABIAQAAAAEAAAAAAE0RAAAAAAABAAAAAQAAAAEAAAAAAAAAAAAAAAABAAAATgAAAAwA#####wEAAAAAAAABAAAARQAAABECAAAAEgAAAEAAAAASAAAATgAAAAANAP####8AAABHAAAAUAAAAA4A#####wH#AAAAEAABSQAAAAAAAAAAAEAIAAAAAAAAAAAFAAEAAABRAAAADgD#####AAAAAAAQAAAAAAAAAAAAAABACAAAAAAAAAAABQACAAAAUQAAAAQA#####wAAAAEAAAAFAP####8B#wAAABAAAAAAAAAAAAAAAEAIAAAAAAAAAAAFAAAAAFIAAABUAAAABQD#####Af8AAAAQAAAAAAAAAAAAAABACAAAAAAAAAAABQAAAAACAAAAVAAAABYA#####wH#AAAAEAAAAQAAAAEAAABWAAAAAgAAABYA#####wH#AAAAEAAAAQAAAAEAAABVAAAAUgAAAAcA#####wH#AAAAEAABSAAAAAAAAAAAAEAIAAAAAAAAAAAFAAE#7ODHzgx84QAAAFcAAAAHAP####8B#wAAABAAAAAAAAAAAAAAAEAIAAAAAAAAAAAFAAE#56PAmr40qgAAAFgAAAAJAP####8AEk1lc3VyZSBkZSBsb25ndWV1cgAAAAUAAAACAAAAAgAAAAEAAABZAAAACgAAAABbAQAAAAAQAAABAAAAAQAAAAEAAABZAAAACwAAAABbAQAAAAAQAAABAAAFAAAAAAEAAABZAAAADAAAAABbAQAAAAAAAAEAAABdAAAAAUAwAAAAAAAAAQAAAA0AAAAAWwAAAFwAAABeAAAADgAAAABbAQAAAAAQAAABAAAFAAEAAABfAAAACAEAAABbAAAAAQAAAFkAAAAPAQAAAFsB#wAAAQAAAAAAYBEAAAAAAAEAAAABAAAAAQAAAAAAAAAAAAAAAAEAAABhAAAACQD#####ABJNZXN1cmUgZGUgbG9uZ3VldXIAAAAFAAAAAgAAAAIAAAABAAAAUgAAAAoAAAAAYwEAAAAAEAAAAQAAAAEAAAABAAAAUgAAAAsAAAAAYwEAAAAAEAAAAQAABQAAAAABAAAAUgAAAAwAAAAAYwEAAAAAAAABAAAAZQAAAAFAMAAAAAAAAAEAAAANAAAAAGMAAABkAAAAZgAAAA4AAAAAYwEAAAAAEAAAAQAABQABAAAAZwAAAAgBAAAAYwAAAAEAAABSAAAADwEAAABjAf8AAAEAAAAAAGgRAAAAAAABAAAAAQAAAAEAAAAAAAAAAAAAAAABAAAAaQAAAAwA#####wH#AAAAAAABAAAAAQAAABEC#####wAAAAIACUNGb25jdGlvbgEAAAARAQAAAAE#8AAAAAAAAAAAABECAAAAEQMAAAASAAAAYQAAABIAAAARAAAAEQMAAAASAAAAYQAAABIAAAARAAAAEgAAAGkAAAAADQD#####AAAAWAAAAGsAAAAOAP####8B#wAAABAAAAAAAAAAAAAAAEAIAAAAAAAAAAAFAAIAAABsAAAADgD#####Af8AAAAQAAAAAAAAAAAAAABACAAAAAAAAAAABQABAAAAbP####8AAAABABBDRHJvaXRlUGFyYWxsZWxlAP####8B#wAAABAAAAEAAAABAAAAbgAAAFcAAAAcAP####8B#wAAABAAAAEAAAABAAAAbQAAAFcAAAAcAP####8B#wAAABAAAAEAAAABAAAAWQAAAFj#####AAAAAQAQQ0ludERyb2l0ZURyb2l0ZQD#####Af8AAAAQAAAAAAAAAAAAAABACAAAAAAAAAAABQAAAABxAAAAbwAAAB0A#####wH#AAAAEAAAAAAAAAAAAAAAQAgAAAAAAAAAAAUAAAAAcQAAAHD#####AAAAAgANQ0xpZXVEZVBvaW50cwD#####AQAAAAAEczExNgABAAAAcgAAAZAAAQAAAFkAAAAJAAAAWQAAAFsAAABhAAAAawAAAGwAAABuAAAAbwAAAHEAAAByAAAAHgD#####AQAAAAAEczExNwABAAAAcwAAAZAAAQAAAFkAAAAJAAAAWQAAAFsAAABhAAAAawAAAGwAAABtAAAAcAAAAHEAAABzAAAAHAD#####Af8AAAAQAAABAAAAAQAAACAAAAAGAAAAHQD#####Af8AAAAQAAAAAAAAAAAAAABACAAAAAAAAAAABQAAAABCAAAAdv####8AAAABAAlDRHJvaXRlQUIA#####wH#AAAAEAAAAQAAAAEAAABFAAAAUgAAABwA#####wH#AAAAEAAAAQAAAAEAAAB3AAAAeAAAAB8A#####wH#AAAAEAAAAQAAAAEAAAABAAAAUgAAAB0A#####wH#AAAAEAAAAAAAAAAAAAAAQAgAAAAAAAAAAAUAAAAAeQAAAHr#####AAAAAQAIQ1ZlY3RldXIA#####wH#AAAAEAAAAQAAAAEAAAB3AAAAIAD#####AAAAAQANQ1RyYW5zUGFyVmVjdAD#####AAAAfAAAAAUA#####wH#AAAAEAAAAAAAAAAAAAAAQAgAAAAAAAAAAAUAAAAAewAAAH0AAAAcAP####8B#wAAABAAAAEAAAABAAAAHgAAAAYAAAAdAP####8B#wAAABAAAAAAAAAAAAAAAEAIAAAAAAAAAAAFAAAAAH8AAABCAAAAHAD#####Af8AAAAQAAABAAAAAQAAAIAAAAB4AAAAHQD#####Af8AAAAQAAAAAAAAAAAAAABACAAAAAAAAAAABQAAAACBAAAAegAAACAA#####wH#AAAAEAAAAQAAAAEAAACAAAAAHgAAAAAhAP####8AAACDAAAABQD#####Af8AAAAQAAAAAAAAAAAAAABACAAAAAAAAAAABQAAAACCAAAAhAAAABYA#####wH#AAAAEAAAAQAAAAEAAAAgAAAAHgAAAB0A#####wH#AAAAEAABTgAAAAAAAAAAAEAIAAAAAAAAAAAFAAAAAIYAAAAGAAAAGgD#####AQAA#wAQAAABAAAAAQAAAAcAAAAGAAAADQD#####AAAAiAAAACEAAAAOAP####8BAAD#ABAAAAAAAAAAAAAAAEAIAAAAAAAAAAAFAAEAAACJAAAADgD#####AQAA#wAQAAAAAAAAAAAAAABACAAAAAAAAAAABQACAAAAiQAAABwA#####wEAAP8AEAAAAQAAAAEAAACLAAAABgAAABwA#####wEAAP8AEAAAAQAAAAEAAACKAAAABgAAAB0A#####wEAAP8AEAAAAAAAAAAAAAAAQAgAAAAAAAAAAAUAAAAAQgAAAIwAAAAdAP####8BAAD#ABAAAAAAAAAAAAAAAEAIAAAAAAAAAAAFAAAAAI0AAABCAAAAIAD#####AQAA#wAQAAABAAAAAQAAAI4AAACLAAAAABwA#####wEAAP8AEAAAAQAAAAEAAACOAAAAeAAAABwA#####wEAAP8AEAAAAQAAAAEAAACPAAAAeAAAAB0A#####wEAAP8AEAAAAAAAAAAAAAAAQAgAAAAAAAAAAAUAAAAAegAAAJEAAAAdAP####8BAAD#ABAAAAAAAAAAAAAAAEAIAAAAAAAAAAAFAAAAAJIAAAB6AAAAIQD#####AAAAkAAAAAUA#####wEAAP8AEAABSwAAAAAAAAAAAEAIAAAAAAAAAAAFAAAAAJMAAACVAAAABQD#####AQAA#wAQAAAAAAAAAAAAAABACAAAAAAAAAAABQAAAACUAAAAlQAAABYA#####wEAAP8AEAAAAQAAAAEAAACWAAAAlwAAABYA#####wEAAP8AEAAAAQAAAAEAAAACAAAAhwAAAAcA#####wEAAP8AEAABSgAAAAAAAAAAAEAIAAAAAAAAAAAFAAE#xupCl1DIgQAAAJkAAAAJAP####8AEk1lc3VyZSBkZSBsb25ndWV1cgAAAAUAAAACAAAAAgAAAAcAAACaAAAACgAAAACbAQAAAAAQAAABAAAAAQAAAAcAAACaAAAACwAAAACbAQAAAAAQAAABAAAFAAAAAAcAAACaAAAADAAAAACbAQAAAAAAAAEAAACdAAAAAUAwAAAAAAAAAQAAAA0AAAAAmwAAAJwAAACeAAAADgAAAACbAQAAAAAQAAABAAAFAAEAAACfAAAACAEAAACbAAAABwAAAJoAAAAPAQAAAJsBAAD#AQAAAAAAoBEAAAAAAAEAAAABAAAAAQAAAAAAAAAAAAAAAAEAAAChAAAACQD#####ABJNZXN1cmUgZGUgbG9uZ3VldXIAAAAFAAAAAgAAAAIAAAAHAAAAlgAAAAoAAAAAowEAAAAAEAAAAQAAAAEAAAAHAAAAlgAAAAsAAAAAowEAAAAAEAAAAQAABQAAAAAHAAAAlgAAAAwAAAAAowEAAAAAAAABAAAApQAAAAFAMAAAAAAAAAEAAAANAAAAAKMAAACkAAAApgAAAA4AAAAAowEAAAAAEAAAAQAABQABAAAApwAAAAgBAAAAowAAAAcAAACWAAAADwEAAACjAQAA#wEAAAAAAKgRAAAAAAABAAAAAQAAAAEAAAAAAAAAAAAAAAABAAAAqQAAAAwA#####wEAAP8AAAABAAAABwAAABECAAAAGwEAAAARAQAAAAE#8AAAAAAAAAAAABECAAAAEQMAAAASAAAAoQAAABIAAAAaAAAAEQMAAAASAAAAoQAAABIAAAAaAAAAEgAAAKkAAAAADQD#####AAAAmAAAAKsAAAAOAP####8BAAD#ABAAAAAAAAAAAAAAAEAIAAAAAAAAAAAFAAEAAACsAAAADgD#####AQAA#wAQAAAAAAAAAAAAAABACAAAAAAAAAAABQACAAAArAAAABwA#####wEAAP8AEAAAAQAAAAEAAACuAAAABgAAABwA#####wEAAP8AEAAAAQAAAAEAAACtAAAABgAAABwA#####wEAAP8AEAAAAQAAAAEAAACaAAAAmAAAAB0A#####wEAAP8AEAAAAAAAAAAAAAAAQAgAAAAAAAAAAAUAAAAAsQAAAK8AAAAdAP####8BAAD#ABAAAAAAAAAAAAAAAEAIAAAAAAAAAAAFAAAAALEAAACwAAAAHgD#####Af8AAAAAAAEAAACyAAABkAABAAAAmgAAAAkAAACaAAAAmwAAAKEAAACrAAAArAAAAK4AAACvAAAAsQAAALIAAAAeAP####8B#wAAAAAAAQAAALMAAAGQAAEAAACaAAAACQAAAJoAAACbAAAAoQAAAKsAAACsAAAArQAAALAAAACxAAAAswAAABYA#####wH#AAAAEAAAAQAAAAEAAAAHAAAAfgAAABYA#####wH#AAAAEAAAAQAAAAEAAAAHAAAAhQAAAAcA#####wEAAP8AEAABTAAAAAAAAAAAAEAIAAAAAAAAAAAFAAG##USu1ErtRQAAAEIAAAAKAP####8BAAD#ABAAAAEAAAABAAAAuAAAAAcAAAAHAP####8BAAD#ABAAAU0AAAAAAAAAAABACAAAAAAAAAAABQABP#DdqALxld8AAAC5AAAACQD#####ABtNZXN1cmUgZCdhbmdsZSBub24gb3JpZW50w6kAAAACAAAAAwAAAAMAAAC4AAAAugAAAAf#####AAAAAQAMQ0Jpc3NlY3RyaWNlAAAAALsBAAAAABAAAAEAAAABAAAAuAAAALoAAAAHAAAABwAAAAC7AQAAAAAQAAABAAAFAAFAbyWoZaxxqQAAALz#####AAAAAQAXQ01lc3VyZUFuZ2xlR2VvbWV0cmlxdWUBAAAAuwAAALgAAAC6AAAAB#####8AAAACABdDTWFycXVlQW5nbGVHZW9tZXRyaXF1ZQEAAAC7AQAA#wAAAAEAAAABQEUGF39UkbsAAAC4AAAAugAAAAcAAAAPAQAAALsBAAD#AEAIAAAAAAAAAAAAAAAAAAAAAAAAAL0PAAAAAAABAAAAAQAAAAEAAAAAAAAAAAAAAALCsAAAAAC+AAAAEwD#####AAAAugAAABECAAAAEgAAAEEAAAASAAAAvgAAAAUA#####wEAAP8AEAAAAAAAAAAAAAAAQAgAAAAAAAAAAAUAAAAABwAAAMEAAAAJAP####8AEk1lc3VyZSBkZSBsb25ndWV1cgAAAAUAAAACAAAAAgAAAAIAAACHAAAACgAAAADDAQAAAAAQAAABAAAAAQAAAAIAAACHAAAACwAAAADDAQAAAAAQAAABAAAFAAAAAAIAAACHAAAADAAAAADDAQAAAAAAAAEAAADFAAAAAUAwAAAAAAAAAQAAAA0AAAAAwwAAAMQAAADGAAAADgAAAADDAQAAAAAQAAABAAAFAAEAAADHAAAACAEAAADDAAAAAgAAAIcAAAAPAQAAAMMBAAD#AQAAAAAAyBEAAAAAAAEAAAABAAAAAQAAAAAAAAAAAAAAAAEAAADJAAAADAD#####AQAA#wAAAAEAAAACAAAAEQIAAAARAQAAAAE#8AAAAAAAAAAAABIAAABBAAAAEgAAAMkAAAAADQD#####AAAAmQAAAMsAAAAOAP####8BAAD#ABAAAVoAAAAAAAAAAABACAAAAAAAAAAABQABAAAAzAAAAA4A#####wAAAP8AEAAAAAAAAAAAAAAAQAgAAAAAAAAAAAUAAgAAAMwAAAAWAP####8BAAD#ABAAAAEAAAABAAAAAgAAAM0AAAAHAP####8BAAD#ABAAAU8AAAAAAAAAAABACAAAAAAAAAAABQABP+Qf2U4QeXoAAADPAAAACQD#####ABJNZXN1cmUgZGUgbG9uZ3VldXIAAAAFAAAAAgAAAAIAAAAHAAAA0AAAAAoAAAAA0QEAAAAAEAAAAQAAAAEAAAAHAAAA0AAAAAsAAAAA0QEAAAAAEAAAAQAABQAAAAAHAAAA0AAAAAwAAAAA0QEAAAAAAAABAAAA0wAAAAFAMAAAAAAAAAEAAAANAAAAANEAAADSAAAA1AAAAA4AAAAA0QEAAAAAEAAAAQAABQABAAAA1QAAAAgBAAAA0QAAAAcAAADQAAAADwEAAADRAQAA#wEAAAAAANYRAAAAAAABAAAAAQAAAAEAAAAAAAAAAAAAAAABAAAA1wAAABwA#####wEAAP8AEAAAAQAAAAEAAADQAAAAmAAAAAwA#####wEAAP8AAAABAAAABwAAABECAAAAGwEAAAARAQAAAAE#8AAAAAAAAAAAABECAAAAEQMAAAASAAAA1wAAABIAAAAaAAAAEQMAAAASAAAA1wAAABIAAAAaAAAAEgAAAKkAAAAADQD#####AAAAmAAAANoAAAAOAP####8BAAD#ABAAAAAAAAAAAAAAAEAIAAAAAAAAAAAFAAIAAADbAAAADgD#####AQAA#wAQAAAAAAAAAAAAAABACAAAAAAAAAAABQABAAAA2wAAABwA#####wEAAP8AEAAAAQAAAAEAAADdAAAAzwAAABwA#####wEAAP8AEAAAAQAAAAEAAADcAAAA3gAAAB0A#####wEAAP8AEAAAAAAAAAAAAAAAQAgAAAAAAAAAAAUAAAAA2QAAAN8AAAAdAP####8BAAD#ABAAAAAAAAAAAAAAAEAIAAAAAAAAAAAFAAAAANkAAADeAAAAHgD#####AQAAAAAEczIyNgABAAAA4AAAAZAAAQAAANAAAAALAAAA0AAAANEAAADXAAAA2QAAANoAAADbAAAA3AAAAN0AAADeAAAA3wAAAOAAAAAeAP####8BAAAAAARzMjI3AAEAAADhAAABkAABAAAA0AAAAAkAAADQAAAA0QAAANcAAADZAAAA2gAAANsAAADdAAAA3gAAAOEAAAAcAP####8BAAD#ABAAAAEAAAABAAAAzQAAAJgAAAAJAP####8AEk1lc3VyZSBkZSBsb25ndWV1cgAAAAUAAAACAAAAAgAAAM0AAAACAAAACgAAAADlAQAAAAAQAAABAAAAAQAAAM0AAAACAAAACwAAAADlAQAAAAAQAAABAAAFAAAAAM0AAAACAAAADAAAAADlAQAAAAAAAAEAAADnAAAAAUAwAAAAAAAAAQAAAA0AAAAA5QAAAOYAAADoAAAADgAAAADlAQAAAAAQAAABAAAFAAEAAADpAAAACAEAAADlAAAAzQAAAAIAAAAPAQAAAOUBAAD#AQAAAAAA6hEAAAAAAAEAAAABAAAAAQAAAAAAAAAAAAAAAAEAAADrAAAACQD#####ABJNZXN1cmUgZGUgbG9uZ3VldXIAAAAFAAAAAgAAAAIAAADNAAAABwAAAAoAAAAA7QEAAAAAEAAAAQAAAAEAAADNAAAABwAAAAsAAAAA7QEAAAAAEAAAAQAABQAAAADNAAAABwAAAAwAAAAA7QEAAAAAAAABAAAA7wAAAAFAMAAAAAAAAAEAAAANAAAAAO0AAADuAAAA8AAAAA4AAAAA7QEAAAAAEAAAAQAABQABAAAA8QAAAAgBAAAA7QAAAM0AAAAHAAAADwEAAADtAQAA#wEAAAAAAPIRAAAAAAABAAAAAQAAAAEAAAAAAAAAAAAAAAABAAAA8wAAAAwA#####wEAAP8AAAABAAAAzQAAABECAAAAGwEAAAARAQAAAAE#8AAAAAAAAAAAABECAAAAEQEAAAABP#AAAAAAAAAAAAARAwAAABIAAADrAAAAEgAAABoAAAARAQAAAAE#8AAAAAAAAAAAABEDAAAAEgAAAOsAAAASAAAAGgAAABIAAACpAAAAAA0A#####wAAAOQAAAD1#####wAAAAEAEUNTdXJmYWNlRGV1eExpZXV4AP####8A####AANTMzMAAAAEAAAAdQAAAHQAAAAOAP####8BAAD#ABAAAAAAAAAAAAAAAEAIAAAAAAAAAAAFAAEAAAD2AAAADgD#####AQAA#wAQAAAAAAAAAAAAAABACAAAAAAAAAAABQACAAAA9gAAABYA#####wAAAAAAEAAAAQAEczI0OQABAAAAwgAAAPgAAAAWAP####8BAAAAABAAAAEABHMyNTAAAQAAAMIAAAD5AAAAHAD#####AQAA#wAQAAABAAAAAQAAAPkAAABXAAAADAD#####AQAA#wAAAAEAAAD5AAAAEQIAAAARAgAAAAFAAAAAAAAAAAAAABIAAAARAAAAEgAAAEEAAAAADQD#####AAAA#AAAAP0AAAAOAP####8BAAD#ABAAAAAAAAAAAAAAAEAIAAAAAAAAAAAFAAEAAAD+AAAADgD#####AQAA#wAQAAFSAAAAAAAAAAAAQAgAAAAAAAAAAAUAAgAAAP4AAAALAP####8BAAD#ABAAAVAAAAAAAAAAAABACAAAAAAAAAAABQAAAAEAAAAA+QAAACAA#####wEAAP8AEAAAAQAAAAEAAAABAAAAUgAAAAAhAP####8AAAECAAAABQD#####AQAA#wAQAAFTAAAAAAAAAAAAQAgAAAAAAAAAAAUAAAABAQAAAQMAAAAEAP####8AAAEBAAAABQD#####AQAA#wAQAAAAAAAAAAAAAABACAAAAAAAAAAABQAAAAEEAAABBQAAABYA#####wEAAP8AEAAAAQAAAAEAAAEAAAAA+QAAAAcA#####wEAAP8AEAABUQAAAAAAAAAAAEAIAAAAAAAAAAAFAAE#0fGkU#AiLwAAAQcAAAAJAP####8AEk1lc3VyZSBkZSBsb25ndWV1cgAAAAUAAAACAAAAAgAAAQEAAAEIAAAACgAAAAEJAQAAAAAQAAABAAAAAQAAAQEAAAEIAAAACwAAAAEJAQAAAAAQAAABAAAFAAAAAQEAAAEIAAAADAAAAAEJAQAAAAAAAAEAAAELAAAAAUAwAAAAAAAAAQAAAA0AAAABCQAAAQoAAAEMAAAADgAAAAEJAQAAAAAQAAABAAAFAAEAAAENAAAACAEAAAEJAAABAQAAAQgAAAAPAQAAAQkBAAD#AQAAAAABDhEAAAAAAAEAAAABAAAAAQAAAAAAAAAAAAAAAAEAAAEPAAAACQD#####ABJNZXN1cmUgZGUgbG9uZ3VldXIAAAAFAAAAAgAAAAIAAAEBAAABAAAAAAoAAAABEQEAAAAAEAAAAQAAAAEAAAEBAAABAAAAAAsAAAABEQEAAAAAEAAAAQAABQAAAAEBAAABAAAAAAwAAAABEQEAAAAAAAABAAABEwAAAAFAMAAAAAAAAAEAAAANAAAAAREAAAESAAABFAAAAA4AAAABEQEAAAAAEAAAAQAABQABAAABFQAAAAgBAAABEQAAAQEAAAEAAAAADwEAAAERAQAA#wEAAAAAARYRAAAAAAABAAAAAQAAAAEAAAAAAAAAAAAAAAABAAABFwAAAAkA#####wASTWVzdXJlIGRlIGxvbmd1ZXVyAAAABQAAAAIAAAACAAABAQAAAQQAAAAKAAAAARkBAAAAABAAAAEAAAABAAABAQAAAQQAAAALAAAAARkBAAAAABAAAAEAAAUAAAABAQAAAQQAAAAMAAAAARkBAAAAAAAAAQAAARsAAAABQDAAAAAAAAABAAAADQAAAAEZAAABGgAAARwAAAAOAAAAARkBAAAAABAAAAEAAAUAAQAAAR0AAAAIAQAAARkAAAEBAAABBAAAAA8BAAABGQEAAP8BAAAAAAEeEQAAAAAAAQAAAAEAAAABAAAAAAAAAAAAAAAAAQAAAR8AAAAWAP####8BAAD#ABAAAAEAAAABAAABBAAAAQYAAAAMAP####8BAAD#AAAAAQAAAQEAAAARAgAAABECAAAAGwEAAAARAQAAAAE#8AAAAAAAAAAAABEDAAAAEQIAAAARAwAAABIAAAEPAAAAEgAAARcAAAASAAABDwAAABIAAAEXAAAAEgAAAR8AAAASAAAAQQAAAAANAP####8AAAEhAAABIgAAAA4A#####wEAAP8AEAAAAAAAAAAAAAAAQAgAAAAAAAAAAAUAAQAAASMAAAAOAP####8BAAD#ABAAAAAAAAAAAAAAAEAIAAAAAAAAAAAFAAIAAAEjAAAAHAD#####AQAA#wAQAAABAAAAAQAAASUAAAEHAAAAHAD#####AQAA#wAQAAABAAAAAQAAAQgAAAEhAAAAHQD#####AQAA#wAQAAAAAAAAAAAAAABACAAAAAAAAAAABQAAAAEnAAABJgAAAB4A#####wAAAAAABHMyOTYAAQAAASgAAAGQAAEAAAEIAAAACQAAAQgAAAEJAAABDwAAASIAAAEjAAABJQAAASYAAAEnAAABKAAAABYA#####wEAAAAAEAAAAQAEczI5NwABAAAAwgAAAQD#####AAAAAQAOQ1N1cmZhY2VEaXNxdWUA#####wH###8AAnMxAAAABAAAAAP#####AAAAAQAZQ1N1cmZhY2VTZWN0ZXVyQ2lyY3VsYWlyZQD#####Af###wACczIAAAAEAAAAIQAAABwA#####wEAAAAAEAAAAQAAAAEAAAD4AAAA#AAAAAwA#####wH#AAAAAAABAAAA+AAAABECAAAAEQIAAAABQAAAAAAAAAAAAAASAAAAEQAAABIAAABBAAAAAA0A#####wAAAS0AAAEuAAAADgD#####Af8AAAAQAAAAAAAAAAAAAABACAAAAAAAAAAABQABAAABLwAAAA4A#####wH#AAAAEAABVgAAAAAAAAAAAEAIAAAAAAAAAAAFAAIAAAEvAAAACwD#####Af8AAAAQAAFVAAAAAAAAAAAAQAgAAAAAAAAAAAUAAAABMQAAAPgAAAAFAP####8B#wAAABAAAVcAAAAAAAAAAABACAAAAAAAAAAABQAAAAEyAAABAwAAAAQA#####wAAATIAAAAFAP####8B#wAAABAAAAAAAAAAAAAAAEAIAAAAAAAAAAAFAAAAATMAAAE0AAAAFgD#####Af8AAAAQAAABAAAAAQAAATEAAAD4AAAABwD#####AQAAAAAQAAFUAAAAAAAAAAAAQAgAAAAAAAAAAAUAAT#sN0nXgF+GAAABNgAAAAkA#####wASTWVzdXJlIGRlIGxvbmd1ZXVyAAAABQAAAAIAAAACAAABNwAAATIAAAAKAAAAATgBAAAAABAAAAEAAAABAAABNwAAATIAAAALAAAAATgBAAAAABAAAAEAAAUAAAABNwAAATIAAAAMAAAAATgBAAAAAAAAAQAAAToAAAABQDAAAAAAAAABAAAADQAAAAE4AAABOQAAATsAAAAOAAAAATgBAAAAABAAAAEAAAUAAQAAATwAAAAIAQAAATgAAAE3AAABMgAAAA8BAAABOAEAAAABAAAAAAE9EQAAAAAAAQAAAAEAAAABAAAAAAAAAAAAAAAAAQAAAT4AAAAJAP####8AEk1lc3VyZSBkZSBsb25ndWV1cgAAAAUAAAACAAAAAgAAATEAAAEyAAAACgAAAAFAAQAAAAAQAAABAAAAAQAAATEAAAEyAAAACwAAAAFAAQAAAAAQAAABAAAFAAAAATEAAAEyAAAADAAAAAFAAQAAAAAAAAEAAAFCAAAAAUAwAAAAAAAAAQAAAA0AAAABQAAAAUEAAAFDAAAADgAAAAFAAQAAAAAQAAABAAAFAAEAAAFEAAAACAEAAAFAAAABMQAAATIAAAAPAQAAAUABAAAAAQAAAAABRREAAAAAAAEAAAABAAAAAQAAAAAAAAAAAAAAAAEAAAFGAAAACQD#####ABJNZXN1cmUgZGUgbG9uZ3VldXIAAAAFAAAAAgAAAAIAAAEyAAABMwAAAAoAAAABSAEAAAAAEAAAAQAAAAEAAAEyAAABMwAAAAsAAAABSAEAAAAAEAAAAQAABQAAAAEyAAABMwAAAAwAAAABSAEAAAAAAAABAAABSgAAAAFAMAAAAAAAAAEAAAANAAAAAUgAAAFJAAABSwAAAA4AAAABSAEAAAAAEAAAAQAABQABAAABTAAAAAgBAAABSAAAATIAAAEzAAAADwEAAAFIAQAAAAEAAAAAAU0RAAAAAAABAAAAAQAAAAEAAAAAAAAAAAAAAAABAAABTgAAAAwA#####wEAAAAAAAABAAABMgAAABECAAAAGwEAAAARAQAAAAE#8AAAAAAAAAAAABEDAAAAEQIAAAARAwAAABIAAAE+AAAAEgAAAUYAAAASAAABPgAAABIAAAFGAAAAEgAAAU4AAAAAHwD#####AQAA#wAQAAABAAAAAQAAATIAAAEzAAAAHAD#####AQAAAAAQAAABAAAAAQAAATcAAAFRAAAADQD#####AAABUQAAAVAAAAAOAP####8BAAAAABAAAAAAAAAAAAAAAEAIAAAAAAAAAAAFAAEAAAFTAAAADgD#####AQAAAAAQAAAAAAAAAAAAAABACAAAAAAAAAAABQACAAABUwAAABwA#####wEAAAAAEAAAAQAAAAEAAAFVAAABLQAAAB0A#####wEAAAAAEAAAAAAAAAAAAAAAQAgAAAAAAAAAAAUAAAABUgAAAVYAAAAeAP####8BAAAAAARzMzQzAAEAAAFXAAAAyAABAAABNwAAAAkAAAE3AAABOAAAAT4AAAFQAAABUgAAAVMAAAFVAAABVgAAAVcAAAADAP####8B#wAAAAAAAQAAAQEAAAD5AAAADQD#####AAABUQAAAVkAAAAOAP####8B#wAAABAAAAAAAAAAAAAAAEAIAAAAAAAAAAAFAAEAAAFaAAAADgD#####Af8AAAAQAAAAAAAAAAAAAABACAAAAAAAAAAABQACAAABWgAAABgA#####wAAAQEAAAABP+szMzMzMzMAAAAFAP####8BAAD#ABAAAAAAAAAAAAAAAEAIAAAAAAAAAAAFAAAAAVsAAAFd#####wAAAAEAFUNQb2ludExpZUxpZXVQYXJQdExpZQD#####AQAA#wAQAAAAAAAAAAAAAABACAAAAAAAAAAABQABP+2YD2YD2YAAAADjP+2YD2YD2YAAAAAoAP####8BAAD#ABAAAAAAAAAAAAAAAEAIAAAAAAAAAAAFAAE#6hCuhCuhCgAAAOM#6hCuhCuhCgAAACgA#####wEAAP8AEAAAAAAAAAAAAAAAQAgAAAAAAAAAAAUAAT#mndWndWndAAAA4z#mndWndWndAAAAKAD#####AQAA#wAQAAAAAAAAAAAAAABACAAAAAAAAAAABQABP+Mq#Mq#MrAAAADjP+Mq#Mq#MrAAAAAoAP####8BAAD#ABAAAAAAAAAAAAAAAEAIAAAAAAAAAAAFAAE#3x4nx4nx4gAAAOM#3x4nx4nx4gAAACgA#####wEAAP8AEAAAAAAAAAAAAAAAQAgAAAAAAAAAAAUAAT#YOHYOHYOHAAAA4z#YOHYOHYOHAAAAKAD#####AQAA#wAQAAAAAAAAAAAAAABACAAAAAAAAAAABQABP9EApEApEAoAAADjP9EApEApEAoAAAAoAP####8BAAD#ABAAAAAAAAAAAAAAAEAIAAAAAAAAAAAFAAE#xIgFIgFIgAAAAOM#xIgFIgFIgAAAACgA#####wEAAP8AEAAAAAAAAAAAAAAAQAgAAAAAAAAAAAUAAT+x9wR9wR9wAAAA4z+x9wR9wR9wAAAAKAD#####AQAA#wAQAAAAAAAAAAAAAABACAAAAAAAAAAABQABP5mqBmqBmqAAAADjP5mqBmqBmqAAAAAoAP####8BAAD#ABAAAAAAAAAAAAAAAEAIAAAAAAAAAAAFAAE#ZIgFIgFIgAAAAOM#ZIgFIgFIgAAAACgA#####wEAAP8AEAAAAAAAAAAAAAAAQAgAAAAAAAAAAAUAAT9kiAUiAUiAAAAA4j9kiAUiAUiAAAAAFgD#####AQAAAAAQAAABAARzMzY0AAEAAAFpAAABagAAACgA#####wEAAP8AEAAAAAAAAAAAAAAAQAgAAAAAAAAAAAUAAT+cOwcOwcOwAAAA4j+cOwcOwcOwAAAAKAD#####AQAA#wAQAAAAAAAAAAAAAABACAAAAAAAAAAABQABP7FSxFSxFSwAAADiP7FSxFSxFSwAAAAoAP####8BAAD#ABAAAAAAAAAAAAAAAEAIAAAAAAAAAAAFAAE#vDsHDsHDsAAAAOI#vDsHDsHDsAAAACgA#####wEAAP8AEAAAAAAAAAAAAAAAQAgAAAAAAAAAAAUAAT#DP4TP4TP4AAAA4j#DP4TP4TP4AAAAKAD#####AQAA#wAQAAAAAAAAAAAAAABACAAAAAAAAAAABQABP8ryhryhrygAAADiP8ryhryhrygAAAAoAP####8BAAD#ABAAAAAAAAAAAAAAAEAIAAAAAAAAAAAFAAE#0XvUXvUXvQAAAOI#0XvUXvUXvQAAACgA#####wEAAP8AEAAAAAAAAAAAAAAAQAgAAAAAAAAAAAUAAT#V+ZV+ZV+ZAAAA4j#V+ZV+ZV+ZAAAAKAD#####AQAA#wAQAAAAAAAAAAAAAABACAAAAAAAAAAABQABP9vo5vo5vo4AAADiP9vo5vo5vo4AAAAoAP####8BAAD#ABAAAAAAAAAAAAAAAEAIAAAAAAAAAAAFAAE#4T48T48T5AAAAOI#4T48T48T5AAAACgA#####wEAAP8AEAAAAAAAAAAAAAAAQAgAAAAAAAAAAAUAAT#kSm0Sm0SmAAAA4j#kSm0Sm0SmAAAAKAD#####AQAA#wAQAAAAAAAAAAAAAABACAAAAAAAAAAABQABP+dWndWndWoAAADiP+dWndWndWoAAAAoAP####8BAAD#ABAAAAAAAAAAAAAAAEAIAAAAAAAAAAAFAAE#6qBmqBmqBgAAAOI#6qBmqBmqBv####8AAAABAAlDUG9seWdvbmUA#####wEAAP8AAAABAAAABAAAAMIAAAD4AAABXwAAAMIAAAApAP####8BAAD#AAAAAQAAAAQAAADCAAABXwAAAWAAAADCAAAAKQD#####AQAA#wAAAAEAAAAEAAAAwgAAAWAAAAFhAAAAwgAAACkA#####wEAAP8AAAABAAAABAAAAMIAAAFhAAABYgAAAMIAAAApAP####8BAAD#AAAAAQAAAAQAAADCAAABYgAAAWMAAADCAAAAKQD#####AQAA#wAAAAEAAAAEAAAAwgAAAWMAAAFkAAAAwgAAACkA#####wEAAP8AAAABAAAABAAAAMIAAAFkAAABZQAAAMIAAAApAP####8BAAD#AAAAAQAAAAQAAADCAAABZQAAAWYAAADCAAAAKQD#####AQAA#wAAAAEAAAAEAAAAwgAAAWYAAAFnAAAAwgAAACkA#####wEAAP8AAAABAAAABAAAAMIAAAFnAAABaAAAAMIAAAApAP####8BAAD#AAAAAQAAAAQAAADCAAABaAAAAWkAAADCAAAAKQD#####AQAA#wAAAAEAAAAEAAAAwgAAAWkAAAFqAAAAwgAAACkA#####wEAAP8AAAABAAAABAAAAMIAAAFqAAABbAAAAMIAAAApAP####8BAAD#AAAAAQAAAAQAAADCAAABbAAAAW0AAADCAAAAKQD#####AQAA#wAAAAEAAAAEAAAAwgAAAW0AAAFuAAAAwgAAACkA#####wEAAP8AAAABAAAABAAAAMIAAAFvAAABcAAAAMIAAAApAP####8BAAD#AAAAAQAAAAQAAADCAAABcAAAAXEAAADCAAAAKQD#####AQAA#wAAAAEAAAAEAAAAwgAAAXEAAAFyAAAAwgAAACkA#####wEAAP8AAAABAAAABAAAAMIAAAFyAAABcwAAAMIAAAApAP####8BAAD#AAAAAQAAAAQAAADCAAABcwAAAXQAAADCAAAAKQD#####AQAA#wAAAAEAAAAEAAAAwgAAAXQAAAF1AAAAwgAAACkA#####wEAAP8AAAABAAAABAAAAMIAAAF1AAABdgAAAMIAAAApAP####8BAAD#AAAAAQAAAAQAAADCAAABdgAAAXcAAADCAAAAKQD#####AQAA#wAAAAEAAAAEAAAAwgAAAXcAAAD5AAAAwgAAACgA#####wEAAP8AEAAAAAAAAAAAAAAAQAgAAAAAAAAAAAUAAT90lTnjstBnAAABWD90lTnjstBnAAAAFgD#####AQAAAAAQAAABAANTMDIAAQAAATEAAAGQAAAAKAD#####AQAA#wAQAAAAAAAAAAAAAABACAAAAAAAAAAABQABP2SIBSIBSIAAAAB0P2SIBSIBSIAAAAAoAP####8BAAD#ABAAAAAAAAAAAAAAAEAIAAAAAAAAAAAFAAE#ZIgFIgFIgAAAAHU#ZIgFIgFIgAAAABYA#####wEAAAAAEAAAAQADUzA1AAEAAAGSAAABkwAAACgA#####wEAAP8AEAAAAAAAAAAAAAAAQAgAAAAAAAAAAAUAAT+iApKnPHZaAAABWD+iApKnPHZaAAAAKAD#####AQAA#wAQAAAAAAAAAAAAAABACAAAAAAAAAAABQABP+#W1Yw4ml8AAAFYP+#W1Yw4ml8AAAAoAP####8BAAD#ABAAAAAAAAAAAAAAAEAIAAAAAAAAAAAFAAE#71tWMOJpfQAAAVg#71tWMOJpfQAAACgA#####wEAAP8AEAAAAAAAAAAAAAAAQAgAAAAAAAAAAAUAAT#utqxhxNL6AAABWD#utqxhxNL6AAAAKAD#####AQAA#wAQAAAAAAAAAAAAAABACAAAAAAAAAAABQABP+2WgzdRC5QAAAFYP+2WgzdRC5QAAAAoAP####8BAAD#ABAAAAAAAAAAAAAAAEAIAAAAAAAAAAAFAAE#7E0vmRXejgAAAVg#7E0vmRXejgAAACgA#####wEAAP8AEAAAAAAAAAAAAAAAQAgAAAAAAAAAAAUAAT#qsYcTS+ZGAAABWD#qsYcTS+ZGAAAAKAD#####AQAA#wAQAAAAAAAAAAAAAABACAAAAAAAAAAABQABP+iaXzIrvRsAAAFYP+iaXzIrvRsAAAAoAP####8BAAD#ABAAAAAAAAAAAAAAAEAIAAAAAAAAAAAFAAE#5qxhxNL5kQAAAVg#5qxhxNL5kQAAACgA#####wEAAP8AEAAAAAAAAAAAAAAAQAgAAAAAAAAAAAUAAT#kGbqIXJ+FAAABWD#kGbqIXJ+FAAAAKAD#####AQAA#wAQAAAAAAAAAAAAAABACAAAAAAAAAAABQABP+E0vmRXejYAAAFYP+E0vmRXejYAAAAoAP####8BAAD#ABAAAAAAAAAAAAAAAEAIAAAAAAAAAAAFAAE#3PHZaDN1EQAAAVg#3PHZaDN1EQAAACgA#####wEAAP8AEAAAAAAAAAAAAAAAQAgAAAAAAAAAAAUAAT#WgzdRC5PxAAABWD#WgzdRC5PxAAAAKAD#####AQAA#wAQAAAAAAAAAAAAAABACAAAAAAAAAAABQABP87f1tWMOJoAAAFYP87f1tWMOJoAAAAoAP####8BAAD#ABAAAAAAAAAAAAAAAEAIAAAAAAAAAAAFAAE#xTnjstBm6gAAAVg#xTnjstBm6gAAACgA#####wEAAP8AEAAAAAAAAAAAAAAAQAgAAAAAAAAAAAUAAT+5uohcn4SBAAABWD+5uohcn4SBAAAAKQD#####AQAA#wAAAAEAAAAEAAAAwgAAAPgAAAGWAAAAwgAAACkA#####wEAAP8AAAABAAAABAAAAMIAAAGWAAABlwAAAMIAAAApAP####8BAAD#AAAAAQAAAAQAAADCAAABlwAAAZgAAADCAAAAKQD#####AQAA#wAAAAEAAAAEAAAAwgAAAZgAAAGZAAAAwgAAACkA#####wEAAP8AAAABAAAABAAAAMIAAAGZAAABmgAAAMIAAAApAP####8BAAD#AAAAAQAAAAQAAADCAAABmgAAAZsAAADCAAAAKQD#####AQAA#wAAAAEAAAAEAAAAwgAAAZsAAAGcAAAAwgAAACkA#####wEAAP8AAAABAAAABAAAAMIAAAGcAAABnQAAAMIAAAApAP####8BAAD#AAAAAQAAAAQAAADCAAABnQAAAZ4AAADCAAAAKQD#####AQAA#wAAAAEAAAAEAAAAwgAAAZ4AAAGfAAAAwgAAACkA#####wEAAP8AAAABAAAABAAAAMIAAAGfAAABoAAAAMIAAAApAP####8BAAD#AAAAAQAAAAQAAADCAAABoAAAAaEAAADCAAAAKQD#####AQAA#wAAAAEAAAAEAAAAwgAAAaEAAAGiAAAAwgAAACkA#####wEAAP8AAAABAAAABAAAAMIAAAGiAAABowAAAMIAAAApAP####8BAAD#AAAAAQAAAAQAAADCAAABowAAAaQAAADCAAAAKQD#####AQAA#wAAAAEAAAAEAAAAwgAAAaQAAAGVAAAAwv####8AAAABABBDU3VyZmFjZVBvbHlnb25lAP####8B####AANTMzgAAAAEAAABtAAAACoA#####wH###8AA1MzOQAAAAQAAAGzAAAAKgD#####Af###wADUzQwAAAABAAAAbIAAAAqAP####8B####AANTNDEAAAAEAAABsQAAACoA#####wH###8AA1M0MgAAAAQAAAGwAAAAKgD#####Af###wADUzQzAAAABAAAAa8AAAAqAP####8B####AANTNDQAAAAEAAABrgAAACoA#####wH###8AA1M0NQAAAAQAAAGtAAAAKgD#####Af###wADUzQ2AAAABAAAAawAAAAqAP####8B####AANTNDcAAAAEAAABqwAAACoA#####wH###8AA1M0OAAAAAQAAAGqAAAAKgD#####Af###wADUzQ5AAAABAAAAakAAAAqAP####8B####AANTNTAAAAAEAAABqAAAACoA#####wH###8AA1M1MQAAAAQAAAGnAAAAKgD#####Af###wADUzUyAAAABAAAAaYAAAAqAP####8B####AANTNTMAAAAEAAABpQAAACoA#####wH###8AA1M1NAAAAAQAAAF4AAAAKgD#####Af###wADUzU1AAAABAAAAXkAAAAqAP####8B####AANTNTYAAAAEAAABegAAACoA#####wH###8AA1M1NwAAAAQAAAF7AAAAKgD#####Af###wADUzU4AAAABAAAAXwAAAAqAP####8B####AANTNTkAAAAEAAABfQAAACoA#####wH###8AA1M2MAAAAAQAAAF+AAAAKgD#####Af###wADUzYxAAAABAAAAX8AAAAqAP####8B####AANTNjIAAAAEAAABgAAAACoA#####wH###8AA1M2MwAAAAQAAAGBAAAAKgD#####Af###wADUzY0AAAABAAAAYIAAAAqAP####8B####AANTNjUAAAAEAAABgwAAACoA#####wH###8AA1M2NgAAAAQAAAGEAAAAKgD#####Af###wADUzY3AAAABAAAAYUAAAAqAP####8B####AANTNjgAAAAEAAABhgAAACkA#####wEAAP8AAAABAAAABAAAAW8AAAFuAAAAwgAAAW8AAAAqAP####8B####AANTNzAAAAAEAAAB1AAAACoA#####wH###8AA1M3MQAAAAQAAAGHAAAAKgD#####Af###wADUzcyAAAABAAAAYgAAAAqAP####8B####AANTNzMAAAAEAAABiQAAACoA#####wH###8AA1M3NAAAAAQAAAGKAAAAKgD#####Af###wADUzc1AAAABAAAAYsAAAAqAP####8B####AANTNzYAAAAEAAABjAAAACoA#####wH###8AA1M3NwAAAAQAAAGNAAAAKgD#####Af###wADUzc4AAAABAAAAY4AAAAqAP####8B####AANTNzkAAAAEAAABjwAAACgA#####wEAAP8AEAAAAAAAAAAAAAAAQAgAAAAAAAAAAAUAAT#v63f63f63AAABKT#v63f63f63AAAAFgD#####AQAAAAAQAAABAANTODEAAQAAAPkAAAHfAAAAKAD#####AQAA#wAQAAAAAAAAAAAAAABACAAAAAAAAAAABQABP++Ez+Ez+EwAAAEpP++Ez+Ez+EwAAAAoAP####8BAAD#ABAAAAAAAAAAAAAAAEAIAAAAAAAAAAAFAAE#7o5vo5vo5gAAASk#7o5vo5vo5gAAACgA#####wEAAP8AEAAAAAAAAAAAAAAAQAgAAAAAAAAAAAUAAT#sJn8Jn8JoAAABKT#sJn8Jn8JoAAAAKAD#####AQAA#wAQAAAAAAAAAAAAAABACAAAAAAAAAAABQABP+lsblsblsYAAAEpP+lsblsblsYAAAAoAP####8BAAD#ABAAAAAAAAAAAAAAAEAIAAAAAAAAAAAFAAE#5ttttttttgAAASk#5ttttttttgAAACgA#####wEAAP8AEAAAAAAAAAAAAAAAQAgAAAAAAAAAAAUAAT#ksRUsRUsRAAABKT#ksRUsRUsRAAAAKAD#####Af8AAAAQAAAAAAAAAAAAAABACAAAAAAAAAAACAABP+H3BH3BH3AAAAEpP+H3BH3BH3AAAAAoAP####8BAAD#ABAAAAAAAAAAAAAAAEAIAAAAAAAAAAAFAAE#3nnnnnnnngAAASk#3nnnnnnnngAAACgA#####wEAAP8AEAAAAAAAAAAAAAAAQAgAAAAAAAAAAAUAAT#YipYipYipAAABKT#YipYipYipAAAAKAD#####AQAA#wAQAAAAAAAAAAAAAABACAAAAAAAAAAABQABP9MWdMWdMWcAAAEpP9MWdMWdMWcAAAAoAP####8BAAD#ABAAAAAAAAAAAAAAAEAIAAAAAAAAAAAFAAE#zdWndWndWgAAASk#zdWndWndWgAAACgA#####wEAAP8AEAAAAAAAAAAAAAAAQAgAAAAAAAAAAAUAAT#GIqWIqWIqAAABKT#GIqWIqWIqAAAAKAD#####AQAA#wAQAAAAAAAAAAAAAABACAAAAAAAAAAABQABP79wR9wR9wQAAAEpP79wR9wR9wQAAAAoAP####8BAAD#ABAAAAAAAAAAAAAAAEAIAAAAAAAAAAAFAAE#sApEApEApAAAASk#sApEApEApAAAACgA#####wEAAP8AEAAAAAAAAAAAAAAAQAgAAAAAAAAAAAUAAT+groQroQroAAABKT+groQroQroAAAAKAD#####AQAA#wAQAAAAAAAAAAAAAABACAAAAAAAAAAABQABP37MB7MB7MAAAAEpP37MB7MB7MAAAAAWAP####8BAAAAABAAAAEAAAABAAABAAAAAfAAAAApAP####8B#wAAAAAAAQAAAAQAAADCAAABAAAAAfAAAADCAAAAKQD#####Af8AAAAAAAEAAAAEAAAAwgAAAfAAAAHvAAAAwgAAACkA#####wH#AAAAAAABAAAABAAAAMIAAAHvAAAB7gAAAMIAAAApAP####8B#wAAAAAAAQAAAAQAAADCAAAB7gAAAe0AAADCAAAAKQD#####Af8AAAAAAAEAAAAEAAAAwgAAAe0AAAHsAAAAwgAAACkA#####wH#AAAAAAABAAAABAAAAMIAAAHsAAAB6wAAAMIAAAApAP####8B#wAAAAAAAQAAAAQAAADCAAAB6wAAAeoAAADCAAAAKQD#####Af8AAAAAAAEAAAAEAAAAwgAAAeoAAAHpAAAAwgAAACkA#####wH#AAAAAAABAAAABAAAAMIAAAHpAAAB6AAAAMIAAAApAP####8B#wAAAAAAAQAAAAQAAADCAAAB6AAAAecAAADCAAAAKQD#####Af8AAAAAAAEAAAAEAAAAwgAAAecAAAHmAAAAwgAAACkA#####wH#AAAAAAABAAAABAAAAMIAAAHmAAAB5QAAAMIAAAApAP####8B#wAAAAAAAQAAAAQAAADCAAAB5QAAAeQAAADCAAAAKQD#####Af8AAAAAAAEAAAAEAAAAwgAAAeQAAAHjAAAAwgAAACkA#####wH#AAAAAAABAAAABAAAAMIAAAHjAAAB4gAAAMIAAAApAP####8B#wAAAAAAAQAAAAQAAADCAAAB4gAAAeEAAADCAAAAKQD#####Af8AAAAAAAEAAAAEAAAAwgAAAeEAAAD5AAAAwgAAABYA#####wAAAAAAEAAAAQAAAAEAAADCAAABXgAAACoA#####wH###8AA1MxNgAAAAQAAAICAAAAKgD#####Af###wADUzE3AAAABAAAAgEAAAAqAP####8B####AANTMTgAAAAEAAACAAAAACoA#####wH###8AA1MxOQAAAAQAAAH#AAAAKgD#####Af###wADUzIwAAAABAAAAf4AAAAqAP####8Bf39#AANTMjEAAAAFAAAB#QAAACoA#####wH###8AA1MyMgAAAAQAAAH8AAAAKgD#####Af###wADUzIzAAAABAAAAfsAAAAqAP####8B####AANTMjQAAAAEAAAB+gAAACoA#####wH###8AA1MyNQAAAAQAAAH5AAAAKgD#####Af###wADUzI2AAAABAAAAfgAAAAqAP####8B####AANTMjcAAAAEAAAB9wAAACoA#####wH###8AA1MyOAAAAAQAAAH2AAAAKgD#####Af###wADUzI5AAAABAAAAfUAAAAqAP####8B####AANTMzAAAAAEAAAB9AAAACoA#####wH###8AA1MzMQAAAAQAAAHzAAAAKgD#####Af###wADUzMyAAAABAAAAfIAAAAWAP####8BAAAAABAAAAEAA1MzNAABAAABlgAAAPgAAAAWAP####8AAAAAABAAAAEAA1MzNQABAAAB5AAAAMIAAAAWAP####8AAAAAABAAAAEABHMzNDQAAQAAATEAAADCAAAACv##########'
      case 'co2': return 'TWF0aEdyYXBoSmF2YTEuMAAAABI+TMzNAAJmcv###wEA#wEAAAAAAAAAAARBAAACSgAAAQEAAAAAAAAAAQAAACr#####AAAAAQAKQ0NhbGNDb25zdAD#####AAJwaQAWMy4xNDE1OTI2NTM1ODk3OTMyMzg0Nv####8AAAABAApDQ29uc3RhbnRlQAkh+1RELRj#####AAAAAQAKQ1BvaW50QmFzZQD#####AAAAAAEQAAFDAAAAAAAAAAAAQAgAAAAAAAAJAAFAS6odlQyD+EBU5IJ3FmBUAAAAAgD#####AQAAAAAQAAFEAAAAAAAAAAAAQAgAAAAAAAAFAAFAWiE0t4+9PEBVeKhkH9uY#####wAAAAEACUNDZXJjbGVPQQD#####AAAAAAABAAAAAQAAAAL#####AAAAAQARQ1N5bWV0cmllQ2VudHJhbGUA#####wAAAAL#####AAAAAQALQ1BvaW50SW1hZ2UA#####wEAAAAAEAAAAAAAAAAAAAAAQAgAAAAAAAAFAAAAAAEAAAAE#####wAAAAEADUNEZW1pRHJvaXRlT0EA#####wEAAAAAEAAAAQABAAAAAgAAAAX#####AAAAAQAPQ1BvaW50TGllRHJvaXRlAP####8BAAAAABAAAUUAAAAAAAAAAABACAAAAAAAAAUAAT#3VxTCtdCLAAAABgAAAAIA#####wEAAAAAEAABQQAAAAAAAAAAAEAIAAAAAAAABQABwGsAAAAAAABAafwo9cKPXAAAAAIA#####wEAAAAAEAABQgAAAAAAAAAAAEAIAAAAAAAABQABQERuQ4IV#zhAaENKts2O#P####8AAAABAAlDTG9uZ3VldXIA#####wAAAAgAAAAJ#####wAAAAEAFENJbXBsZW1lbnRhdGlvblByb3RvAP####8AEk1lc3VyZSBkZSBsb25ndWV1cgAAAAUAAAACAAAAAgAAAAEAAAAC#####wAAAAEAC0NNZWRpYXRyaWNlAAAAAAsBAAAAAA0AAAEAAQAAAAEAAAAC#####wAAAAEAB0NNaWxpZXUAAAAACwEAAAAADQAAAQUAAAAAAQAAAAL#####AAAAAgAJQ0NlcmNsZU9SAAAAAAsBAAAAAAEAAAANAAAAAUAwAAAAAAAAAf####8AAAABABBDSW50RHJvaXRlQ2VyY2xlAAAAAAsAAAAMAAAADv####8AAAABABBDUG9pbnRMaWVCaXBvaW50AAAAAAsBAAAAAA0AAAEFAAEAAAAPAAAACAEAAAALAAAAAQAAAAL#####AAAAAQAPQ1ZhbGV1ckFmZmljaGVlAQAAAAsBAAAAAQAAABARAAAAAAABAAAAAQAAAAEAAAAAAAAAAAAAAAABAAAAEf####8AAAABAAdDQ2FsY3VsAP####8AA3BlcgAHMipwaSpDRP####8AAAABAApDT3BlcmF0aW9uAgAAABECAAAAAUAAAAAAAAAA#####wAAAAEAD0NSZXN1bHRhdFZhbGV1cgAAAAAAAAASAAAAEQAAAAkA#####wASTWVzdXJlIGRlIGxvbmd1ZXVyAAAABQAAAAIAAAACAAAAAgAAAAcAAAAKAAAAABQBAAAAAA0AAAEAAQAAAAIAAAAHAAAACwAAAAAUAQAAAAANAAABBQAAAAACAAAABwAAAAwAAAAAFAEAAAAAAQAAABYAAAABQDAAAAAAAAABAAAADQAAAAAUAAAAFQAAABcAAAAOAAAAABQBAAAAAA0AAAEFAAEAAAAYAAAACAEAAAAUAAAAAgAAAAcAAAAPAQAAABQBAAAAAQAAABkRAAAAAAABAAAAAQAAAAEAAAAAAAAAAAAAAAABAAAAGgAAABAA#####wAFYWxwaGEACUNEKjM2MC9ERQAAABEDAAAAEQIAAAASAAAAEQAAAAFAdoAAAAAAAAAAABIAAAAa#####wAAAAEACUNSb3RhdGlvbgD#####AAAABwAAABEDAAAAEgAAABwAAAABQAAAAAAAAAAAAAAFAP####8BAAAAABAAAAAAAAAAAAAAAEAIAAAAAAAABQAAAAACAAAAHQAAABMA#####wAAAAf#####AAAAAQAMQ01vaW5zVW5haXJlAAAAEQMAAAASAAAAHAAAAAFAAAAAAAAAAAAAAAUA#####wEAAAAAEAAAAAAAAAAAAAAAQAgAAAAAAAAFAAAAAAIAAAAf#####wAAAAEACENTZWdtZW50AP####8BAAAAABAAAAEAAQAAAB4AAAAHAAAAFQD#####AQAAAAAQAAABAAEAAAAHAAAAIP####8AAAABABJDQXJjRGVDZXJjbGVEaXJlY3QA#####wEAAAAAAQAAAAcAAAAgAAAAHv####8AAAABABZDRHJvaXRlUGVycGVuZGljdWxhaXJlAP####8BAAAAABAAAAEAAQAAAAcAAAAG#####wAAAAEAD0NQb2ludExpZUNlcmNsZQD#####AQAAAAAQAAAAAAAAAAAAAABACAAAAAAAAAkAAT#VY2Vr+r3hAAAAIwAAABgA#####wEAAAAAEAAAAAAAAAAAAAAAQAgAAAAAAAAJAAE#5F+DjIn8WwAAACMAAAAVAP####8AAAAAABAAAAEAAQAAACUAAAAHAAAAFQD#####AAAAAAAQAAABAAEAAAAHAAAAJgAAABYA#####wAAAAAAAQAAAAcAAAAlAAAAJgAAAAr##########w=='
      case 'co3': return 'TWF0aEdyYXBoSmF2YTEuMAAAABI+TMzNAAJmcv###wEA#wEAAAAAAAAAAARBAAACSgAAAQEAAAAAAAAAAQAAADL#####AAAAAQAKQ0NhbGNDb25zdAD#####AAJwaQAWMy4xNDE1OTI2NTM1ODk3OTMyMzg0Nv####8AAAABAApDQ29uc3RhbnRlQAkh+1RELRj#####AAAAAQAKQ1BvaW50QmFzZQD#####AAAAAAEQAAFDAAAAAAAAAAAAQAgAAAAAAAAJAABARoAAAAAAAEBOcKPXCj1wAAAAAgD#####AQAAAAAQAAFEAAAAAAAAAAAAQAgAAAAAAAAFAAFAVCAAAAAAAEBO64UeuFHs#####wAAAAEACUNDZXJjbGVPQQD#####AAAAAAABAAAAAQAAAAL#####AAAAAQARQ1N5bWV0cmllQ2VudHJhbGUA#####wAAAAL#####AAAAAQALQ1BvaW50SW1hZ2UA#####wEAAAAAEAAAAAAAAAAAAAAAQAgAAAAAAAAFAAAAAAEAAAAE#####wAAAAEADUNEZW1pRHJvaXRlT0EA#####wEAAAAAEAAAAQABAAAAAgAAAAX#####AAAAAQAPQ1BvaW50TGllRHJvaXRlAP####8BAAAAABAAAUUAAAAAAAAAAABACAAAAAAAAAUAAUADeqTSSXdQAAAABgAAAAIA#####wEAAAAAEAABQQAAAAAAAAAAAEAIAAAAAAAABQABwEWAAAAAAABAcU4UeuFHrgAAAAIA#####wEAAAAAEAABQgAAAAAAAAAAAEAIAAAAAAAABQABwFS9DlYEGJBAcUXztkWhy#####8AAAABAAlDTG9uZ3VldXIA#####wAAAAgAAAAJ#####wAAAAEAFENJbXBsZW1lbnRhdGlvblByb3RvAP####8AEk1lc3VyZSBkZSBsb25ndWV1cgAAAAUAAAACAAAAAgAAAAEAAAAC#####wAAAAEAC0NNZWRpYXRyaWNlAAAAAAsBAAAAAA0AAAEAAQAAAAEAAAAC#####wAAAAEAB0NNaWxpZXUAAAAACwEAAAAADQAAAQUAAAAAAQAAAAL#####AAAAAgAJQ0NlcmNsZU9SAAAAAAsBAAAAAAEAAAANAAAAAUAwAAAAAAAAAf####8AAAABABBDSW50RHJvaXRlQ2VyY2xlAAAAAAsAAAAMAAAADv####8AAAABABBDUG9pbnRMaWVCaXBvaW50AAAAAAsBAAAAAA0AAAEFAAEAAAAPAAAACAEAAAALAAAAAQAAAAL#####AAAAAQAPQ1ZhbGV1ckFmZmljaGVlAQAAAAsBAAAAAQAAABARAAAAAAABAAAAAQAAAAEAAAAAAAAAAAAAAAABAAAAEf####8AAAABAAdDQ2FsY3VsAP####8AA3BlcgAHMipwaSpDRP####8AAAABAApDT3BlcmF0aW9uAgAAABECAAAAAUAAAAAAAAAA#####wAAAAEAD0NSZXN1bHRhdFZhbGV1cgAAAAAAAAASAAAAEQAAAAkA#####wASTWVzdXJlIGRlIGxvbmd1ZXVyAAAABQAAAAIAAAACAAAAAgAAAAcAAAAKAAAAABQBAAAAAA0AAAEAAQAAAAIAAAAHAAAACwAAAAAUAQAAAAANAAABBQAAAAACAAAABwAAAAwAAAAAFAEAAAAAAQAAABYAAAABQDAAAAAAAAABAAAADQAAAAAUAAAAFQAAABcAAAAOAAAAABQBAAAAAA0AAAEFAAEAAAAYAAAACAEAAAAUAAAAAgAAAAcAAAAPAQAAABQBAAAAAQAAABkRAAAAAAABAAAAAQAAAAEAAAAAAAAAAAAAAAABAAAAGgAAABAA#####wAFYWxwaGEACUNEKjM2MC9ERQAAABEDAAAAEQIAAAASAAAAEQAAAAFAdoAAAAAAAAAAABIAAAAa#####wAAAAEACUNSb3RhdGlvbgD#####AAAABwAAABEDAAAAEgAAABwAAAABQAAAAAAAAAAAAAAFAP####8BAAAAABAAAAAAAAAAAAAAAEAIAAAAAAAABQAAAAACAAAAHQAAABMA#####wAAAAf#####AAAAAQAMQ01vaW5zVW5haXJlAAAAEQMAAAASAAAAHAAAAAFAAAAAAAAAAAAAAAUA#####wEAAAAAEAAAAAAAAAAAAAAAQAgAAAAAAAAFAAAAAAIAAAAf#####wAAAAEACENTZWdtZW50AP####8BAAAAABAAAAEAAQAAAB4AAAAHAAAAFQD#####AQAAAAAQAAABAAEAAAAHAAAAIP####8AAAABABJDQXJjRGVDZXJjbGVEaXJlY3QA#####wEAAAAAAQAAAAcAAAAgAAAAHv####8AAAABABZDRHJvaXRlUGVycGVuZGljdWxhaXJlAP####8BAAAAABAAAAEAAQAAAAcAAAAG#####wAAAAEAD0NQb2ludExpZUNlcmNsZQD#####AQAAAAAQAAAAAAAAAAAAAABACAAAAAAAAAkAAT#VY2Vr+r3hAAAAIwAAABgA#####wEAAAAAEAAAAAAAAAAAAAAAQAgAAAAAAAAJAAE#5F+DjIn8WwAAACMAAAAVAP####8BAAAAABAAAAEAAQAAACUAAAAHAAAAFQD#####AQAAAAAQAAABAAEAAAAHAAAAJgAAABYA#####wEAAAAAAQAAAAcAAAAlAAAAJgAAABcA#####wEAAAAAEAAAAQABAAAAAgAAAAYAAAAHAP####8BAAAAABAAAAAAAAAAAAAAAEAIAAAAAAAACQABP#cXyxwtZRUAAAAqAAAAFQD#####AQAAAAAQAAABAAEAAAArAAAABwAAAAUA#####wEAAAAAEAAAAAAAAAAAAAAAQAgAAAAAAAAJAAAAACsAAAAEAAAAFQD#####AQAAAAAQAAABAAEAAAAtAAAAB#####8AAAABAAlDUG9seWdvbmUA#####wEAAAAAAQAAAAQAAAAtAAAAKwAAAAcAAAAtAAAAFgD#####AAAAAAABAAAAAgAAACsAAAAtAAAAFQD#####AAAAAAAQAAABAAEAAAAtAAAAKwAAAAr##########w=='
      case 'pi1': return 'TWF0aEdyYXBoSmF2YTEuMAAAABM+TMzNAAJmcv###wEA#wEAAAAAAAAAAANLAAAChwAAAQEAAAAAAAAAAQAAALn#####AAAAAQAKQ0NhbGNDb25zdAD#####AAJwaQAWMy4xNDE1OTI2NTM1ODk3OTMyMzg0Nv####8AAAABAApDQ29uc3RhbnRlQAkh+1RELRj#####AAAAAQAKQ1BvaW50QmFzZQD#####AQAAAAAQAAFDAAAAAAAAAAAAQAgAAAAAAAAAAAUAAUBNFB2VDIPwQExzPdG6+YgAAAACAP####8BAAAAABAAAAAAAAAAAAAAAEAIAAAAAAAAAAAFAAFAYFePS#pwAkBMMwtAWP#cAAAAAgD#####AQAAAAAQAAAAAAAAAAAAAABACAAAAAAAAAAABQABQFodBU9D4yhAYR6J7IgHE#####8AAAABAAlDRHJvaXRlQUIA#####wEAAAAAEAAAAQAAAAEAAAABAAAAAv####8AAAABABZDRHJvaXRlUGVycGVuZGljdWxhaXJlAP####8BAAAAABAAAAEAAAABAAAAAQAAAAT#####AAAAAQAPQ1BvaW50TGllRHJvaXRlAP####8BAAAAABAAAAAAAAAAAAAAAEAIAAAAAAAAAAAFAAE#3i273cwI5wAAAAUAAAAEAP####8BAAAAABAAAAEAAAABAAAABgAAAAUAAAAEAP####8BAAAAABAAAAEAAAABAAAAAgAAAAf#####AAAAAQAQQ0ludERyb2l0ZURyb2l0ZQD#####AQAAAAAQAAAAAAAAAAAAAABACAAAAAAAAAAABQAAAAAIAAAAB#####8AAAABAAlDUG9seWdvbmUA#####wEAAAAAAAABAAAABQAAAAEAAAACAAAACQAAAAYAAAAB#####wAAAAEAC0NNZWRpYXRyaWNlAP####8BAAAAABAAAAEAAAABAAAAAQAAAAYAAAAFAP####8BAAAAABAAAAAAAAAAAAAAAEAIAAAAAAAAAAAFAAE#+EnS4SfLOgAAAAsAAAAHAP####8BAAAAAAAAAQAAAAQAAAAMAAAABgAAAAEAAAAM#####wAAAAEACENTZWdtZW50AP####8BAAAAABAAAAEAAAABAAAAAQAAAAkAAAAGAP####8BAAAAABAAAAAAAAAAAAAAAEAIAAAAAAAAAAAFAAAAAA4AAAAL#####wAAAAEAEUNTeW1ldHJpZUNlbnRyYWxlAP####8AAAAP#####wAAAAEAC0NQb2ludEltYWdlAP####8BAAAAABAAAAAAAAAAAAAAAEAIAAAAAAAAAAAFAAAAAAwAAAAQ#####wAAAAEACUNDZXJjbGVPQQD#####AQAAAAAAAAEAAAAGAAAADAAAAAwA#####wEAAAAAAAABAAAACQAAABH#####AAAAAQAQQ0ludENlcmNsZUNlcmNsZQD#####AAAAEgAAABP#####AAAAAQAQQ1BvaW50TGllQmlwb2ludAD#####AQAAAAAQAAAAAAAAAAAAAABACAAAAAAAAAAABQACAAAAFAAAAA4A#####wEAAAAAEAAAAAAAAAAAAAAAQAgAAAAAAAAAAAUAAQAAABQAAAALAP####8BAAAAABAAAAAAAAAAAAAAAEAIAAAAAAAAAAAFAAAAABYAAAAQAAAABwD#####AQAAAAAAAAEAAAAEAAAAAgAAABEAAAAJAAAAAgAAAAcA#####wEAAAAAAAABAAAABAAAABYAAAAJAAAABgAAABYAAAAHAP####8BAAAAAAAAAQAAAAQAAAAXAAAAAQAAAAIAAAAXAAAAAgD#####Af8A#wAQAAFEAAAAAAAAAAAAQAgAAAAAAAAAAAgAAUBS8JnaU7CYQFChq401+zAAAAAJAP####8B#wD#ABAAAAEAAAABAAAAAQAAABsAAAACAP####8A#wD#ABAAAUEAAAAAAAAAAABACAAAAAAAAAAACAABwHJoAAAAAABAc11wo9cKPgAAAAIA#####wD#AP8AEAABQgAAAAAAAAAAAEAIAAAAAAAAAAAIAAHAGif25dTEAEBtmamHZUMg#####wAAAAEACUNMb25ndWV1cgD#####AAAAHQAAAB7#####AAAAAQAUQ0ltcGxlbWVudGF0aW9uUHJvdG8A#####wASTWVzdXJlIGRlIGxvbmd1ZXVyAAAABQAAAAIAAAACAAAAAQAAABsAAAAIAAAAACABAAAAABAAAAEAAAABAAAAAQAAABv#####AAAAAQAHQ01pbGlldQAAAAAgAQAAAAAQAAABAAAFAAAAAAEAAAAb#####wAAAAIACUNDZXJjbGVPUgAAAAAgAQAAAAAAAAEAAAAiAAAAAUAwAAAAAAAAAf####8AAAABABBDSW50RHJvaXRlQ2VyY2xlAAAAACAAAAAhAAAAIwAAAA4AAAAAIAEAAAAAEAAAAQAABQABAAAAJAAAAA8BAAAAIAAAAAEAAAAb#####wAAAAEAD0NWYWxldXJBZmZpY2hlZQEAAAAgAf8A#wEAAAAAACURAAAAAAABAAAAAQAAAAEAAAAAAAAAAAAAAAABAAAAJgAAAAIA#####wAAAAAAEAAAAEAIAAAAAAAAAAAAAAAAAAAAAAgAAEBkcAAAAAAAQHutcKPXCj7#####AAAAAQAHQ0NhbGN1bAD#####AAVtaW5pMQABMAAAAAEAAAAAAAAAAAAAABUA#####wAFbWF4aTEAATEAAAABP#AAAAAAAAAAAAAQAP####8AB0N1cnNldXIAAAAFAAAABQAAAAMAAAApAAAAKgAAACj#####AAAAAQAUQ0Ryb2l0ZURpcmVjdGlvbkZpeGUAAAAAKwEAAAAAEAAAAQAAAAEAAAAoAT#wAAAAAAAAAAAABQEAAAArAP8A#wAQAAAAwAgAAAAAAAA#8AAAAAAAAAAABQAAQF1AAAAAAAAAAAAs#####wAAAAEAC0NIb21vdGhldGllAAAAACsAAAAo#####wAAAAEACkNPcGVyYXRpb24D#####wAAAAEAD0NSZXN1bHRhdFZhbGV1cgAAACkAAAAYAQAAABkAAAApAAAAGQAAACoAAAALAAAAACsBAAAAAA0AAk8xAMAQAAAAAAAAQBAAAAAAAAAAAAUAAAAALQAAAC4AAAAXAAAAACsAAAAoAAAAGAMAAAAYAQAAAAE#8AAAAAAAAAAAABkAAAApAAAAGAEAAAAZAAAAKgAAABkAAAApAAAACwAAAAArAQAAAAANAAJJMQDAAAAAAAAAAEAIAAAAAAAAAAAFAAAAAC0AAAAwAAAACQEAAAArAP8A#wAQAAABAAAAAQAAACgAAAAtAAAABQEAAAArAP8A#wEQAAJrMQDAAAAAAAAAAEAAAAAAAAAAAAABAAEAAAAAAAAAAAAAADL#####AAAAAgAPQ01lc3VyZUFic2Npc3NlAQAAACsAAWEAAAAvAAAAMQAAADMAAAAUAQAAACsA#wD#AAAAAAAAAAAAwBgAAAAAAAAAAAAAADMPAAH###8AAAABAAAAAgAAAAEAAAAAAAAAAAAAAAACAAAANAAAAAIA#####wAAAAAAEAAAAEAIAAAAAAAAAAAAAAAAAAAAAAgAAEB0uAAAAAAAQHt9cKPXCj4AAAAVAP####8ABW1pbmkyAAEwAAAAAQAAAAAAAAAAAAAAFQD#####AAVtYXhpMgABMQAAAAE#8AAAAAAAAAAAABAA#####wAHQ3Vyc2V1cgAAAAUAAAAFAAAAAwAAADcAAAA4AAAANgAAABYAAAAAOQEAAAAAEAAAAQAAAAEAAAA2AT#wAAAAAAAAAAAABQEAAAA5AP8A#wAQAAAAwAgAAAAAAAA#8AAAAAAAAAAABQAAQF1AAAAAAAAAAAA6AAAAFwAAAAA5AAAANgAAABgDAAAAGQAAADcAAAAYAQAAABkAAAA3AAAAGQAAADgAAAALAAAAADkBAAAAAA0AAk8xAMAQAAAAAAAAQBAAAAAAAAAAAAUAAAAAOwAAADwAAAAXAAAAADkAAAA2AAAAGAMAAAAYAQAAAAE#8AAAAAAAAAAAABkAAAA3AAAAGAEAAAAZAAAAOAAAABkAAAA3AAAACwAAAAA5AQAAAAANAAJJMQDAAAAAAAAAAEAIAAAAAAAAAAAFAAAAADsAAAA+AAAACQEAAAA5AP8A#wAQAAABAAAAAQAAADYAAAA7AAAABQEAAAA5AP8A#wEQAAFrAMAAAAAAAAAAQAAAAAAAAAAAAAEAAQAAAAAAAAAAAAAAQAAAABoBAAAAOQABYgAAAD0AAAA#AAAAQQAAABQBAAAAOQD#AP8AAAAAAAAAAADAGAAAAAAAAAAAAAAAQQ8AAf###wAAAAEAAAACAAAAAQAAAAAAAAAAAAAAAAIAAABCAAAAFQD#####AAJhYQABYQAAABkAAAA0AAAAFQD#####AAJiYgABYgAAABkAAABCAAAAEgD#####Af8A#wAAAAEAAAABAAAAGAIAAAAZAAAARAAAABkAAAAmAAAAABMA#####wAAABwAAABGAAAADgD#####Af8AAAAQAAAAAAAAAAAAAABACAAAAAAAAAAAAwABAAAARwAAAA4A#####wD#AP8AEAAAAAAAAAAAAAAAQAgAAAAAAAAAAAgAAgAAAEf#####AAAAAQAIQ1ZlY3RldXIA#####wH#AAAAEAAAAQAAAAEAAAAGAAAASAD#####AAAAAQANQ1RyYW5zUGFyVmVjdAD#####AAAASgAAAAsA#####wH#AAAAEAAAAAAAAAAAAAAAQAgAAAAAAAAAAAMAAAAACQAAAEv#####AAAAAQAQQ0Ryb2l0ZVBhcmFsbGVsZQD#####Af8AAAAQAAABAAAAAQAAABYAAAALAAAAHQD#####Af8AAAAQAAABAAAAAQAAABcAAAALAAAABgD#####Af8AAAAQAAAAAAAAAAAAAABACAAAAAAAAAAAAwAAAABOAAAABQAAAAYA#####wH#AAAAEAAAAAAAAAAAAAAAQAgAAAAAAAAAAAMAAAAATQAAAAUAAAAGAP####8B#wAAABAAAAAAAAAAAAAAAEAIAAAAAAAAAAADAAAAAAsAAAAFAAAAAwD#####Af8AAAAQAAABAAAAAQAAAAEAAABIAAAAHQD#####Af8AAAAQAAABAAAAAQAAAFEAAABSAAAAHQD#####Af8AAAAQAAABAAAAAQAAAFAAAABSAAAAHQD#####Af8AAAAQAAABAAAAAQAAAE8AAABSAAAAAwD#####Af8AAAAQAAABAAAAAQAAAAYAAABIAAAABgD#####Af8AAAAQAAAAAAAAAAAAAABACAAAAAAAAAAAAwAAAABVAAAAVgAAAAYA#####wH#AAAAEAAAAAAAAAAAAAAAQAgAAAAAAAAAAAMAAAAAUwAAAFYAAAAGAP####8B#wAAABAAAAAAAAAAAAAAAEAIAAAAAAAAAAADAAAAAFQAAABWAAAAGwD#####Af8AAAAQAAABAAAAAQAAAFAAAAAWAAAAABsA#####wH#AAAAEAAAAQAAAAEAAABPAAAAFwAAAAAcAP####8AAABbAAAACwD#####Af8AAAAQAAFLAAAAAAAAAAAAQAgAAAAAAAAAAAMAAAAAVwAAAFwAAAAcAP####8AAABaAAAACwD#####Af8AAAAQAAFFAAAAAAAAAAAAQAgAAAAAAAAAAAMAAAAAWQAAAF4AAAAbAP####8B#wAAABAAAAEAAAABAAAAUQAAAAwAAAAAGwD#####Af8AAAAQAAABAAAAAQAAAFEAAAARAAAAABwA#####wAAAGAAAAALAP####8B#wAAABAAAUgAAAAAAAAAAABACAAAAAAAAAAAAwAAAABYAAAAYgAAABwA#####wAAAGEAAAALAP####8B#wAAABAAAU0AAAAAAAAAAABACAAAAAAAAAAAAwAAAABYAAAAZAAAAAcA#####wEAAAAAAAABAAAABQAAAAYAAABIAAAATAAAAAkAAAAGAAAABwD#####Af8AAAAAAAEAAAAEAAAATAAAAGUAAAAJAAAATAAAAAcA#####wH#AAAAAAABAAAABAAAAGMAAAAGAAAASAAAAGMAAAAHAP####8B#wAAAAAAAQAAAAQAAAAGAAAAXwAAAAkAAAAGAAAABwD#####Af8AAAAAAAEAAAAEAAAASAAAAF0AAABMAAAASAAAAAIA#####wEAAP8AEAABRwAAAAAAAAAAAEAIAAAAAAAAAAAIAAFAWHxapJk4gEBG5QTuLMCoAAAACAD#####AQAA#wAQAAABAAAAAQAAAF8AAABrAAAABQD#####AQAA#wAQAAFGAAAAAAAAAAAAQAgAAAAAAAAAAAgAAT#RopmxkajAAAAAbAAAABAA#####wAbTWVzdXJlIGQnYW5nbGUgbm9uIG9yaWVudMOpAAAAAgAAAAMAAAADAAAAXwAAAG0AAABr#####wAAAAEADENCaXNzZWN0cmljZQAAAABuAQAAAAAQAAABAAAAAQAAAF8AAABtAAAAawAAAAUAAAAAbgEAAAAAEAAAAQAABQABQG8lqGWscakAAABv#####wAAAAEAF0NNZXN1cmVBbmdsZUdlb21ldHJpcXVlAQAAAG4AAABfAAAAbQAAAGv#####AAAAAgAXQ01hcnF1ZUFuZ2xlR2VvbWV0cmlxdWUBAAAAbgEAAP8AAAABAAAAAUBFBhd#VJG7AAAAXwAAAG0AAABrAAAAFAEAAABuAQAA#wBACAAAAAAAAAAAAAAAAAAAAAAAAABwDwAAAAAAAQAAAAEAAAABAAAAAAAAAAAAAAACwrAAAAAAcf####8AAAABAAlDUm90YXRpb24A#####wAAAG3#####AAAAAQAMQ01vaW5zVW5haXJlAAAAGAIAAAAZAAAARQAAABkAAABxAAAACwD#####AQAA#wAQAAAAAAAAAAAAAABACAAAAAAAAAAAAwAAAABfAAAAdAAAAAgA#####wEAAP8AEAAAAQAAAAEAAABjAAAAawAAAAUA#####wEAAP8AEAABSQAAAAAAAAAAAEAIAAAAAAAAAAAIAAE#3rXxVNv3RwAAAHYAAAAQAP####8AG01lc3VyZSBkJ2FuZ2xlIG5vbiBvcmllbnTDqQAAAAIAAAADAAAAAwAAAGMAAAB3AAAAawAAAB4AAAAAeAEAAAAAEAAAAQAAAAEAAABjAAAAdwAAAGsAAAAFAAAAAHgBAAAAABAAAAEAAAUAAUBvJahlrHGpAAAAeQAAAB8BAAAAeAAAAGMAAAB3AAAAawAAACABAAAAeAEAAP8AAAABAAAAAUBFBhd#VJG7AAAAYwAAAHcAAABrAAAAFAEAAAB4AQAA#wBACAAAAAAAAAAAAAAAAAAAAAAAAAB6DwAAAAAAAQAAAAEAAAABAAAAAAAAAAAAAAACwrAAAAAAewAAACEA#####wAAAHcAAAAiAAAAGAIAAAAZAAAARQAAABkAAAB7AAAACwD#####AQAA#wAQAAAAAAAAAAAAAABACAAAAAAAAAAAAwAAAABjAAAAfgAAAAgA#####wEAAP8AEAAAAQAAAAEAAABrAAAAXQAAAAUA#####wEAAP8AEAABSgAAAAAAAAAAAEAIAAAAAAAAAAAIAAE#gAHIXpZOTgAAAIAAAAAQAP####8AG01lc3VyZSBkJ2FuZ2xlIG5vbiBvcmllbnTDqQAAAAIAAAADAAAAAwAAAGsAAACBAAAAXQAAAB4AAAAAggEAAAAAEAAAAQAAAAEAAABrAAAAgQAAAF0AAAAFAAAAAIIBAAAAABAAAAEAAAUAAUBvJahlrHGpAAAAgwAAAB8BAAAAggAAAGsAAACBAAAAXQAAACABAAAAggEAAP8AAAABAAAAAUBFBhd#VJG7AAAAawAAAIEAAABdAAAAFAEAAACCAQAA#wBACAAAAAAAAAAAAAAAAAAAAAAAAACEDwAAAAAAAQAAAAEAAAABAAAAAAAAAAAAAAACwrAAAAAAhQAAACEA#####wAAAIEAAAAYAgAAABkAAABFAAAAGQAAAIUAAAALAP####8BAAD#ABAAAAAAAAAAAAAAAEAIAAAAAAAAAAADAAAAAF0AAACIAAAACAD#####AQAA#wAQAAABAAAAAQAAAGUAAABrAAAABQD#####AQAA#wAQAAFMAAAAAAAAAAAAQAgAAAAAAAAAAAgAAb#erezWpql8AAAAigAAABAA#####wAbTWVzdXJlIGQnYW5nbGUgbm9uIG9yaWVudMOpAAAAAgAAAAMAAAADAAAAawAAAIsAAABlAAAAHgAAAACMAQAAAAAQAAABAAAAAQAAAGsAAACLAAAAZQAAAAUAAAAAjAEAAAAAEAAAAQAABQABQG8lqGWscakAAACNAAAAHwEAAACMAAAAawAAAIsAAABlAAAAIAEAAACMAQAA#wAAAAEAAAABQEUGF39UkbsAAABrAAAAiwAAAGUAAAAUAQAAAIwBAAD#AEAIAAAAAAAAAAAAAAAAAAAAAAAAAI4PAAAAAAABAAAAAQAAAAEAAAAAAAAAAAAAAALCsAAAAACPAAAAIQD#####AAAAiwAAABgCAAAAGQAAAEUAAAAZAAAAjwAAAAsA#####wEAAP8AEAAAAAAAAAAAAAAAQAgAAAAAAAAAAAMAAAAAZQAAAJIAAAAHAP####8BAAAAAAAAAQAAAAQAAAB1AAAABgAAAAkAAAB1AAAABwD#####AQAAAAAAAAEAAAAEAAAATAAAAJMAAAAJAAAATAAAAAcA#####wEAAAAAAAABAAAABAAAAEwAAACJAAAASAAAAEwAAAAHAP####8BAAAAAAAAAQAAAAQAAABIAAAAfwAAAAYAAABI#####wAAAAEAEENTdXJmYWNlUG9seWdvbmUA#####wH###8AA1M1MgAAAAQAAAANAAAAIwD#####Af###wADUzUzAAAABAAAAAoAAAAjAP####8B####AANTNTQAAAAEAAAAGAAAACMA#####wH###8AA1M1NQAAAAQAAAAaAAAAIwD#####Af###wADUzU2AAAABAAAABkAAAAJAP####8AAAAAABAAAAEAA1M2MgABAAAAFwAAAAEAAAAJAP####8AAAAAABAAAAEAA1M2MwABAAAAFwAAAAIAAAAJAP####8AAAAAABAAAAEAA1M2NAABAAAADAAAAAEAAAAJAP####8AAAAAABAAAAEAA1M2NQABAAAAAQAAAAIAAAAJAP####8AAAAAABAAAAEAA1M2NgABAAAAAgAAABEAAAAJAP####8AAAAAABAAAAEAA1M2NwABAAAAAQAAAAYAAAAJAP####8AAAAAABAAAAEAA1M2OAABAAAAAgAAAAkAAAAJAP####8AAAAAABAAAAEAA1M2OQABAAAADAAAAAYAAAAJAP####8AAAAAABAAAAEAA1M3MQABAAAACQAAABEAAAAJAP####8AAAAAABAAAAEAA1M3MgABAAAABgAAABYAAAAJAP####8AAAAAABAAAAEAA1M3MwABAAAAFgAAAAkAAAAjAP####8B####AANTNjAAAAAEAAAAlgAAAAkA#####wEAAAAAEAAAAQADUzc3AAEAAABIAAAATAAAAAkA#####wEAAAAAEAAAAQADUzc0AAEAAACJAAAASAAAAAkA#####wEAAAAAEAAAAQADUzc1AAEAAACJAAAATAAAACMA#####wH###8AA1M1OAAAAAQAAABmAAAAIwD#####Af###wADUzU3AAAABAAAAJcAAAAjAP####8B####AANTNTkAAAAEAAAAlQAAAAkA#####wEAAAAAEAAAAQADUzc2AAEAAAB#AAAASAAAAAkA#####wEAAAAAEAAAAQADUzc4AAEAAABMAAAAkwAAAAkA#####wEAAAAAEAAAAQADUzgyAAEAAAAJAAAAkwAAAAkA#####wEAAAAAEAAAAQADUzc5AAEAAABIAAAABgAAAAkA#####wEAAAAAEAAAAQADUzgxAAEAAAB#AAAABgAAAAkA#####wEAAAAAEAAAAQADUzgwAAEAAABMAAAACQAAAAkA#####wAAAAAAEAAAAQADUzcwAAEAAAAGAAAACQAAACMA#####wH###8AA1M2MQAAAAQAAACUAAAACQD#####AQAAAAAQAAABAANTODQAAQAAAHUAAAAJAAAACQD#####AQAAAAAQAAABAANTODMAAQAAAAYAAAB1AAAAH###########'
      case 'pi2': return 'TWF0aEdyYXBoSmF2YTEuMAAAABI+TMzNAAJmcv###wEA#wEAAAAAAAAAAARBAAACSgAAAQEAAAAAAAAAAQAAADr#####AAAAAQAKQ0NhbGNDb25zdAD#####AAJwaQAWMy4xNDE1OTI2NTM1ODk3OTMyMzg0Nv####8AAAABAApDQ29uc3RhbnRlQAkh+1RELRj#####AAAAAQAKQ1BvaW50QmFzZQD#####AQAAAAAQAAFDAAAAAAAAAAAAQAgAAAAAAAAFAAFATRQdlQyD8EBMcz3RuvmIAAAAAgD#####AQAAAAAQAAAAAAAAAAAAAABACAAAAAAAAAUAAUBgV49L+nACQEwzC0BY#9wAAAACAP####8BAAAAABAAAAAAAAAAAAAAAEAIAAAAAAAABQABQFodBU9D4yhAYR6J7IgHE#####8AAAABAAlDRHJvaXRlQUIA#####wEAAAAAEAAAAQABAAAAAQAAAAL#####AAAAAQAWQ0Ryb2l0ZVBlcnBlbmRpY3VsYWlyZQD#####AQAAAAAQAAABAAEAAAABAAAABP####8AAAABAA9DUG9pbnRMaWVEcm9pdGUA#####wEAAAAAEAAAAAAAAAAAAAAAQAgAAAAAAAAFAAE#3i273cwI5wAAAAUAAAAEAP####8BAAAAABAAAAEAAQAAAAYAAAAFAAAABAD#####AQAAAAAQAAABAAEAAAACAAAAB#####8AAAABABBDSW50RHJvaXRlRHJvaXRlAP####8BAAAAABAAAAAAAAAAAAAAAEAIAAAAAAAABQAAAAAIAAAAB#####8AAAABAAlDUG9seWdvbmUA#####wAAAAAAAQAAAAUAAAABAAAAAgAAAAkAAAAGAAAAAf####8AAAABAAtDTWVkaWF0cmljZQD#####AQAAAAAQAAABAAEAAAABAAAABgAAAAUA#####wEAAAAAEAAAAAAAAAAAAAAAQAgAAAAAAAAFAAE#+EnS4SfLOgAAAAsAAAAHAP####8AAAAAAAEAAAAEAAAADAAAAAYAAAABAAAADP####8AAAABAAhDU2VnbWVudAD#####AQAAAAAQAAABAAEAAAABAAAACQAAAAYA#####wEAAAAAEAAAAAAAAAAAAAAAQAgAAAAAAAAFAAAAAA4AAAAL#####wAAAAEAEUNTeW1ldHJpZUNlbnRyYWxlAP####8AAAAP#####wAAAAEAC0NQb2ludEltYWdlAP####8BAAAAABAAAAAAAAAAAAAAAEAIAAAAAAAABQAAAAAMAAAAEP####8AAAABAAlDQ2VyY2xlT0EA#####wEAAAAAAQAAAAYAAAAMAAAADAD#####AQAAAAABAAAACQAAABH#####AAAAAQAQQ0ludENlcmNsZUNlcmNsZQD#####AAAAEgAAABP#####AAAAAQAQQ1BvaW50TGllQmlwb2ludAD#####AQAAAAAQAAAAAAAAAAAAAABACAAAAAAAAAUAAgAAABQAAAAOAP####8BAAAAABAAAAAAAAAAAAAAAEAIAAAAAAAABQABAAAAFAAAAAsA#####wEAAAAAEAAAAAAAAAAAAAAAQAgAAAAAAAAFAAAAABYAAAAQAAAABwD#####AAAAAAABAAAABAAAAAIAAAARAAAACQAAAAIAAAAHAP####8AAAAAAAEAAAAEAAAAFgAAAAkAAAAGAAAAFgAAAAcA#####wEAAAAAAQAAAAQAAAAXAAAAAQAAAAIAAAAXAAAAAgD#####Af8A#wAQAAFEAAAAAAAAAAAAQAgAAAAAAAAIAAFAUvCZ2lOwmEBQoauNNfswAAAACQD#####Af8A#wAQAAABAAEAAAABAAAAGwAAAAIA#####wD#AP8AEAABQQAAAAAAAAAAAEAIAAAAAAAACAABwHJoAAAAAABAc11wo9cKPgAAAAIA#####wD#AP8AEAABQgAAAAAAAAAAAEAIAAAAAAAACAABwBon9uXUxABAbZmph2VDIP####8AAAABAAlDTG9uZ3VldXIA#####wAAAB0AAAAe#####wAAAAEAFENJbXBsZW1lbnRhdGlvblByb3RvAP####8AEk1lc3VyZSBkZSBsb25ndWV1cgAAAAUAAAACAAAAAgAAAAEAAAAbAAAACAAAAAAgAQAAAAANAAABAAEAAAABAAAAG#####8AAAABAAdDTWlsaWV1AAAAACABAAAAAA0AAAEFAAAAAAEAAAAb#####wAAAAIACUNDZXJjbGVPUgAAAAAgAQAAAAABAAAAIgAAAAFAMAAAAAAAAAH#####AAAAAQAQQ0ludERyb2l0ZUNlcmNsZQAAAAAgAAAAIQAAACMAAAAOAAAAACABAAAAAA0AAAEFAAEAAAAkAAAADwEAAAAgAAAAAQAAABv#####AAAAAQAPQ1ZhbGV1ckFmZmljaGVlAQAAACAB#wD#AQAAACURAAAAAAABAAAAAQAAAAEAAAAAAAAAAAAAAAABAAAAJv####8AAAABAAdDQ2FsY3VsAP####8ABW1pbmkxAAEwAAAAAQAAAAAAAAAAAAAAFQD#####AAVtYXhpMQABMQAAAAE#8AAAAAAAAAAAABUA#####wAFbWluaTIAATAAAAABAAAAAAAAAAAAAAAVAP####8ABW1heGkyAAExAAAAAT#wAAAAAAAA#####wAAAAEAEENEcm9pdGVQYXJhbGxlbGUA#####wH#AAAAEAAAAQABAAAAFgAAAAsAAAAWAP####8B#wAAABAAAAEAAQAAABcAAAALAAAABgD#####Af8AAAAQAAAAAAAAAAAAAABACAAAAAAAAAMAAAAALQAAAAUAAAAGAP####8B#wAAABAAAAAAAAAAAAAAAEAIAAAAAAAAAwAAAAAsAAAABQAAAAYA#####wH#AAAAEAAAAAAAAAAAAAAAQAgAAAAAAAADAAAAAAsAAAAF#####wAAAAEACENWZWN0ZXVyAP####8B#wAAABAAAAEAAQAAAC8AAAAWAAAAABcA#####wH#AAAAEAAAAQABAAAALgAAABcA#####wAAAAEADUNUcmFuc1BhclZlY3QA#####wAAADIAAAAYAP####8AAAAxAAAAFwD#####Af8AAAAQAAABAAEAAAAwAAAADAAAAAAXAP####8B#wAAABAAAAEAAQAAADAAAAARAAAAABgA#####wAAADUAAAAYAP####8AAAA2AAAAAgD#####AQAA#wAQAAFHAAAAAAAAAAAAQAgAAAAAAAAIAAFAWHxapJk4gEBG5QTuLMCoAAAAH###########'
      case 'pi3': return 'TWF0aEdyYXBoSmF2YTEuMAAAABI+TMzNAAJmcv###wEA#wEAAAAAAAAAAARBAAACSgAAAQEAAAAAAAAAAQAAADr#####AAAAAQAKQ0NhbGNDb25zdAD#####AAJwaQAWMy4xNDE1OTI2NTM1ODk3OTMyMzg0Nv####8AAAABAApDQ29uc3RhbnRlQAkh+1RELRj#####AAAAAQAKQ1BvaW50QmFzZQD#####AQAAAAAQAAFDAAAAAAAAAAAAQAgAAAAAAAAFAAFATRQdlQyD8EBMcz3RuvmIAAAAAgD#####AQAAAAAQAAAAAAAAAAAAAABACAAAAAAAAAUAAUBgV49L+nACQEwzC0BY#9wAAAACAP####8BAAAAABAAAAAAAAAAAAAAAEAIAAAAAAAABQABQFodBU9D4yhAYR6J7IgHE#####8AAAABAAlDRHJvaXRlQUIA#####wEAAAAAEAAAAQABAAAAAQAAAAL#####AAAAAQAWQ0Ryb2l0ZVBlcnBlbmRpY3VsYWlyZQD#####AQAAAAAQAAABAAEAAAABAAAABP####8AAAABAA9DUG9pbnRMaWVEcm9pdGUA#####wEAAAAAEAAAAAAAAAAAAAAAQAgAAAAAAAAFAAE#3i273cwI5wAAAAUAAAAEAP####8BAAAAABAAAAEAAQAAAAYAAAAFAAAABAD#####AQAAAAAQAAABAAEAAAACAAAAB#####8AAAABABBDSW50RHJvaXRlRHJvaXRlAP####8BAAAAABAAAAAAAAAAAAAAAEAIAAAAAAAABQAAAAAIAAAAB#####8AAAABAAlDUG9seWdvbmUA#####wAAAAAAAQAAAAUAAAABAAAAAgAAAAkAAAAGAAAAAf####8AAAABAAtDTWVkaWF0cmljZQD#####AQAAAAAQAAABAAEAAAABAAAABgAAAAUA#####wEAAAAAEAAAAAAAAAAAAAAAQAgAAAAAAAAFAAE#+EnS4SfLOgAAAAsAAAAHAP####8AAAAAAAEAAAAEAAAADAAAAAYAAAABAAAADP####8AAAABAAhDU2VnbWVudAD#####AQAAAAAQAAABAAEAAAABAAAACQAAAAYA#####wEAAAAAEAAAAAAAAAAAAAAAQAgAAAAAAAAFAAAAAA4AAAAL#####wAAAAEAEUNTeW1ldHJpZUNlbnRyYWxlAP####8AAAAP#####wAAAAEAC0NQb2ludEltYWdlAP####8BAAAAABAAAAAAAAAAAAAAAEAIAAAAAAAABQAAAAAMAAAAEP####8AAAABAAlDQ2VyY2xlT0EA#####wEAAAAAAQAAAAYAAAAMAAAADAD#####AQAAAAABAAAACQAAABH#####AAAAAQAQQ0ludENlcmNsZUNlcmNsZQD#####AAAAEgAAABP#####AAAAAQAQQ1BvaW50TGllQmlwb2ludAD#####AQAAAAAQAAAAAAAAAAAAAABACAAAAAAAAAUAAgAAABQAAAAOAP####8BAAAAABAAAAAAAAAAAAAAAEAIAAAAAAAABQABAAAAFAAAAAsA#####wEAAAAAEAAAAAAAAAAAAAAAQAgAAAAAAAAFAAAAABYAAAAQAAAABwD#####AAAAAAABAAAABAAAAAIAAAARAAAACQAAAAIAAAAHAP####8BAAAAAAEAAAAEAAAAFgAAAAkAAAAGAAAAFgAAAAcA#####wEAAAAAAQAAAAQAAAAXAAAAAQAAAAIAAAAXAAAAAgD#####Af8A#wAQAAFEAAAAAAAAAAAAQAgAAAAAAAAIAAFAUvCZ2lOwmEBQoauNNfswAAAACQD#####Af8A#wAQAAABAAEAAAABAAAAGwAAAAIA#####wD#AP8AEAABQQAAAAAAAAAAAEAIAAAAAAAACAABwHJoAAAAAABAc11wo9cKPgAAAAIA#####wD#AP8AEAABQgAAAAAAAAAAAEAIAAAAAAAACAABwBon9uXUxABAbZmph2VDIP####8AAAABAAlDTG9uZ3VldXIA#####wAAAB0AAAAe#####wAAAAEAFENJbXBsZW1lbnRhdGlvblByb3RvAP####8AEk1lc3VyZSBkZSBsb25ndWV1cgAAAAUAAAACAAAAAgAAAAEAAAAbAAAACAAAAAAgAQAAAAANAAABAAEAAAABAAAAG#####8AAAABAAdDTWlsaWV1AAAAACABAAAAAA0AAAEFAAAAAAEAAAAb#####wAAAAIACUNDZXJjbGVPUgAAAAAgAQAAAAABAAAAIgAAAAFAMAAAAAAAAAH#####AAAAAQAQQ0ludERyb2l0ZUNlcmNsZQAAAAAgAAAAIQAAACMAAAAOAAAAACABAAAAAA0AAAEFAAEAAAAkAAAADwEAAAAgAAAAAQAAABv#####AAAAAQAPQ1ZhbGV1ckFmZmljaGVlAQAAACAB#wD#AQAAACURAAAAAAABAAAAAQAAAAEAAAAAAAAAAAAAAAABAAAAJv####8AAAABAAdDQ2FsY3VsAP####8ABW1pbmkxAAEwAAAAAQAAAAAAAAAAAAAAFQD#####AAVtYXhpMQABMQAAAAE#8AAAAAAAAAAAABUA#####wAFbWluaTIAATAAAAABAAAAAAAAAAAAAAAVAP####8ABW1heGkyAAExAAAAAT#wAAAAAAAA#####wAAAAEAEENEcm9pdGVQYXJhbGxlbGUA#####wH#AAAAEAAAAQABAAAAFgAAAAsAAAAWAP####8B#wAAABAAAAEAAQAAABcAAAALAAAABgD#####Af8AAAAQAAAAAAAAAAAAAABACAAAAAAAAAMAAAAALQAAAAUAAAAGAP####8B#wAAABAAAAAAAAAAAAAAAEAIAAAAAAAAAwAAAAAsAAAABQAAAAYA#####wH#AAAAEAAAAAAAAAAAAAAAQAgAAAAAAAADAAAAAAsAAAAF#####wAAAAEACENWZWN0ZXVyAP####8B#wAAABAAAAEAAQAAAC8AAAAWAAAAABcA#####wH#AAAAEAAAAQABAAAALgAAABcA#####wAAAAEADUNUcmFuc1BhclZlY3QA#####wAAADIAAAAYAP####8AAAAxAAAAFwD#####Af8AAAAQAAABAAEAAAAwAAAADAAAAAAXAP####8B#wAAABAAAAEAAQAAADAAAAARAAAAABgA#####wAAADUAAAAYAP####8AAAA2AAAAAgD#####AQAA#wAQAAFHAAAAAAAAAAAAQAgAAAAAAAAIAAFAWHxapJk4gEBG5QTuLMCoAAAAH###########'
      case 'pi4': return 'TWF0aEdyYXBoSmF2YTEuMAAAABM+TMzNAAJmcv###wEA#wEAAAAAAAAAAANLAAAChwAAAQEAAAAAAAAAAQAAAJ######AAAAAQAKQ0NhbGNDb25zdAD#####AAJwaQAWMy4xNDE1OTI2NTM1ODk3OTMyMzg0Nv####8AAAABAApDQ29uc3RhbnRlQAkh+1RELRj#####AAAAAQAKQ1BvaW50QmFzZQD#####AQAAAAAQAAAAAAAAAAAAAABACAAAAAAAAAAABQABQFmi+Y12tVBARy3FmBU9FAAAAAIA#####wEAAAAAEAAAAAAAAAAAAAAAQAgAAAAAAAAAAAUAAUAa2kOCFf8AQFlW4swKnooAAAACAP####8BAAAAABAAAAAAAAAAAAAAAEAIAAAAAAAAAAAFAAFAXw2kOCFf+EBZVuLMCp6K#####wAAAAEACUNQb2x5Z29uZQD#####AQAAAAAAAAEAAAAEAAAAAQAAAAIAAAADAAAAAQAAAAIA#####wEAAAAAEAAAAAAAAAAAAAAAQAgAAAAAAAAAAAUAAUBldtIcEK#6QEyYcEK#58AAAAADAP####8BAAAAAAAAAQAAAAQAAAABAAAABQAAAAMAAAAB#####wAAAAEACUNDZXJjbGVPQQD#####AQAAAAAAAAEAAAADAAAABQAAAAQA#####wEAAAAAAAABAAAAAQAAAAX#####AAAAAQAPQ1BvaW50TGllQ2VyY2xlAP####8BAAAAABAAAAAAAAAAAAAAAEAIAAAAAAAAAAAFAAFABAD9sei54AAAAAgAAAADAP####8BAAAAAAAAAQAAAAQAAAAJAAAAAgAAAAEAAAAJAAAABAD#####AQAAAAAAAAEAAAACAAAACf####8AAAABAAhDVmVjdGV1cgD#####AQAAAAAQAAABAAAAAQAAAAIAAAADAP####8AAAABABBDSW50Q2VyY2xlQ2VyY2xlAP####8AAAALAAAAB#####8AAAABABBDUG9pbnRMaWVCaXBvaW50AP####8BAAAAABAAAUsAAAAAAAAAAABACAAAAAAAAAAABQACAAAADQAAAAgA#####wEAAAAAEAAAAAAAAAAAAAAAQAgAAAAAAAAAAAUAAQAAAA0AAAADAP####8BAAAAAAAAAQAAAAQAAAACAAAAAwAAAA8AAAAC#####wAAAAEACUNEcm9pdGVBQgD#####Af8AAAAQAAABAAAAAQAAAAIAAAAD#####wAAAAEAFkNEcm9pdGVQZXJwZW5kaWN1bGFpcmUA#####wH#AAAAEAAAAQAAAAEAAAACAAAAEQAAAAoA#####wH#AAAAEAAAAQAAAAEAAAABAAAAEv####8AAAABABBDSW50RHJvaXRlRHJvaXRlAP####8B#wAAABAAAUMAAAAAAAAAAABACAAAAAAAAAAABQAAAAASAAAAEwAAAAIA#####wH#AAAAEAABRAAAAAAAAAAAAEAIAAAAAAAAAAAFAAFARUXzGu1qkEBP7cWYFT0UAAAAAgD#####AP8AAAAQAAFBAAAAAAAAAAAAQAgAAAAAAAAAAAUAAUBmIAAAAAAAQIGPCj1wo9cAAAACAP####8A#wAAABAAAUIAAAAAAAAAAABACAAAAAAAAAAABQABQFYYTuLMCqRAemMOCFf8+P####8AAAABAAlDTG9uZ3VldXIA#####wAAABYAAAAX#####wAAAAEACENTZWdtZW50AP####8B#wAAABAAAAEAAAABAAAAFAAAABX#####AAAAAQAUQ0ltcGxlbWVudGF0aW9uUHJvdG8A#####wASTWVzdXJlIGRlIGxvbmd1ZXVyAAAABQAAAAIAAAACAAAAFAAAABX#####AAAAAQALQ01lZGlhdHJpY2UAAAAAGgEAAAAAEAAAAQAAAAEAAAAUAAAAFf####8AAAABAAdDTWlsaWV1AAAAABoBAAAAABAAAAEAAAUAAAAAFAAAABX#####AAAAAgAJQ0NlcmNsZU9SAAAAABoBAAAAAAAAAQAAABwAAAABQDAAAAAAAAAB#####wAAAAEAEENJbnREcm9pdGVDZXJjbGUAAAAAGgAAABsAAAAdAAAACAAAAAAaAQAAAAAQAAABAAAFAAEAAAAeAAAADAEAAAAaAAAAFAAAABX#####AAAAAQAPQ1ZhbGV1ckFmZmljaGVlAQAAABoB#wAAAQAAAAAAHxEAAAAAAAEAAAABAAAAAQAAAAAAAAAAAAAAAAEAAAAgAAAAAgD#####AAAAAAAQAAAAQAgAAAAAAAAAAAAAAAAAAAAABQAAQGdgAAAAAABAfs4UeuFHrv####8AAAABAAdDQ2FsY3VsAP####8ABW1pbmkxAAEwAAAAAQAAAAAAAAAAAAAAFAD#####AAVtYXhpMQABMQAAAAE#8AAAAAAAAAAAAA4A#####wAHQ3Vyc2V1cgAAAAUAAAAFAAAAAwAAACMAAAAkAAAAIv####8AAAABABRDRHJvaXRlRGlyZWN0aW9uRml4ZQAAAAAlAQAAAAAQAAABAAAAAQAAACIBP#AAAAAAAAD#####AAAAAQAPQ1BvaW50TGllRHJvaXRlAQAAACUA#wAAABAAAADACAAAAAAAAD#wAAAAAAAAAAAFAABAXUAAAAAAAAAAACb#####AAAAAQALQ0hvbW90aGV0aWUAAAAAJQAAACL#####AAAAAQAKQ09wZXJhdGlvbgP#####AAAAAQAPQ1Jlc3VsdGF0VmFsZXVyAAAAIwAAABgBAAAAGQAAACMAAAAZAAAAJP####8AAAABAAtDUG9pbnRJbWFnZQAAAAAlAQAAAAANAAJPMQDAEAAAAAAAAEAQAAAAAAAAAAAFAAAAACcAAAAoAAAAFwAAAAAlAAAAIgAAABgDAAAAGAEAAAABP#AAAAAAAAAAAAAZAAAAIwAAABgBAAAAGQAAACQAAAAZAAAAIwAAABoAAAAAJQEAAAAADQACSTEAwAAAAAAAAABACAAAAAAAAAAABQAAAAAnAAAAKgAAAA0BAAAAJQD#AAAAEAAAAQAAAAEAAAAiAAAAJwAAABYBAAAAJQD#AAABEAACazEAwAAAAAAAAABAAAAAAAAAAAAAAQABAAAAAAAAAAAAAAAs#####wAAAAIAD0NNZXN1cmVBYnNjaXNzZQEAAAAlAAFhAAAAKQAAACsAAAAtAAAAEwEAAAAlAP8AAAAAAAAAAAAAAMAYAAAAAAAAAAAAAAAtDwAB####AAAAAQAAAAIAAAABAAAAAAAAAAAAAAAAAgAAAC4AAAACAP####8AAAAAABAAAABACAAAAAAAAAAAAAAAAAAAAAAFAABAdjAAAAAAAEB+rhR64UeuAAAAFAD#####AAVtaW5pMgABMAAAAAEAAAAAAAAAAAAAABQA#####wAFbWF4aTIAATEAAAABP#AAAAAAAAAAAAAOAP####8AB0N1cnNldXIAAAAFAAAABQAAAAMAAAAxAAAAMgAAADAAAAAVAAAAADMBAAAAABAAAAEAAAABAAAAMAE#8AAAAAAAAAAAABYBAAAAMwD#AAAAEAAAAMAIAAAAAAAAP#AAAAAAAAAAAAUAAEBdQAAAAAAAAAAANAAAABcAAAAAMwAAADAAAAAYAwAAABkAAAAxAAAAGAEAAAAZAAAAMQAAABkAAAAyAAAAGgAAAAAzAQAAAAANAAJPMQDAEAAAAAAAAEAQAAAAAAAAAAAFAAAAADUAAAA2AAAAFwAAAAAzAAAAMAAAABgDAAAAGAEAAAABP#AAAAAAAAAAAAAZAAAAMQAAABgBAAAAGQAAADIAAAAZAAAAMQAAABoAAAAAMwEAAAAADQACSTEAwAAAAAAAAABACAAAAAAAAAAABQAAAAA1AAAAOAAAAA0BAAAAMwD#AAAAEAAAAQAAAAEAAAAwAAAANQAAABYBAAAAMwD#AAABEAABawDAAAAAAAAAAEAAAAAAAAAAAAABAAEAAAAAAAAAAAAAADoAAAAbAQAAADMAAWIAAAA3AAAAOQAAADsAAAATAQAAADMA#wAAAAAAAAAAAAAAwBgAAAAAAAAAAAAAADsPAAH###8AAAABAAAAAgAAAAEAAAAAAAAAAAAAAAACAAAAPAAAABQA#####wACYWEAAWEAAAAZAAAALgAAABQA#####wACYmIAAWIAAAAZAAAAPAAAABEA#####wH#AAAAAAABAAAAFAAAABgCAAAAGQAAAD4AAAAZAAAAIAAAAAASAP####8AAAAZAAAAQAAAAAgA#####wH#AAAAEAAAAAAAAAAAAAAAQAgAAAAAAAAAAAUAAQAAAEEAAAAIAP####8A#wAAABAAAAAAAAAAAAAAAEAIAAAAAAAAAAAFAAIAAABBAAAABgD#####Af8AAAAQAAABAAAAAQAAABQAAABCAP####8AAAABAA1DVHJhbnNQYXJWZWN0AP####8AAABEAAAAGgD#####Af8AAAAQAAAAAAAAAAAAAABACAAAAAAAAAAABQAAAAABAAAARQAAAAMA#####wEAAAAAAAABAAAABAAAAAIAAABGAAAAAwAAAAIAAAAJAP####8B#wAAABAAAAEAAAABAAAAFAAAAEL#####AAAAAQAQQ0Ryb2l0ZVBhcmFsbGVsZQD#####Af8AAAAQAAABAAAAAQAAAAkAAAASAAAAHQD#####Af8AAAAQAAABAAAAAQAAAAkAAAATAAAACwD#####Af8AAAAQAAAAAAAAAAAAAABACAAAAAAAAAAABQAAAAASAAAASgAAAB0A#####wH#AAAAEAAAAQAAAAEAAABLAAAASAAAAAYA#####wH#AAAAEAAAAQAAAAEAAABLAAAACQAAAAAcAP####8AAABNAAAACQD#####Af8AAAAQAAABAAAAAQAAAAIAAABCAAAACwD#####Af8AAAAQAAAAAAAAAAAAAABACAAAAAAAAAAABQAAAABPAAAATAAAABoA#####wH#AAAAEAABSgAAAAAAAAAAAEAIAAAAAAAAAAAFAAAAAFAAAABOAAAAAwD#####Af8AAAAAAAEAAAAEAAAAAgAAAFEAAABGAAAAAgAAAB0A#####wH#AAAAEAAAAQAAAAEAAAAFAAAAEQAAAAsA#####wH#AAAAEAAAAAAAAAAAAAAAQAgAAAAAAAAAAAUAAAAAUwAAABIAAAAdAP####8B#wAAABAAAAEAAAABAAAAVAAAAEgAAAALAP####8B#wAAABAAAAAAAAAAAAAAAEAIAAAAAAAAAAAFAAAAAE8AAABVAAAABgD#####Af8AAAAQAAABAAAAAQAAAFQAAAAFAAAAABwA#####wAAAFcAAAAaAP####8B#wAAABAAAUgAAAAAAAAAAABACAAAAAAAAAAABQAAAABWAAAAWAAAAAMA#####wH#AAAAAAABAAAABAAAAAMAAABGAAAAWQAAAAMAAAAdAP####8B#wAAABAAAAEAAAABAAAADwAAABEAAAALAP####8B#wAAABAAAAAAAAAAAAAAAEAIAAAAAAAAAAAFAAAAABIAAABbAAAAHQD#####Af8AAAAQAAABAAAAAQAAAFwAAABVAAAACwD#####Af8AAAAQAAAAAAAAAAAAAABACAAAAAAAAAAABQAAAABPAAAAXQAAAAYA#####wH#AAAAEAAAAQAAAAEAAABcAAAADwAAAAAcAP####8AAABfAAAAGgD#####Af8AAAAQAAFFAAAAAAAAAAAAQAgAAAAAAAAAAAUAAAAAXgAAAGAAAAADAP####8B#wAAAAAAAQAAAAQAAAACAAAAYQAAAAMAAAAC#####wAAAAEAD0NTeW1ldHJpZUF4aWFsZQD#####AAAAEQAAABoA#####wEAAP8AEAABRwAAAAAAAAAAAEAIAAAAAAAAAAAFAAAAAA8AAABjAAAADwD#####AQAA#wAQAAABAAAAAQAAAGQAAABhAAAAFgD#####AQAA#wAQAAFGAAAAAAAAAAAAQAgAAAAAAAAAAAUAAb#iTqitAMZVAAAAZQAAAA4A#####wAbTWVzdXJlIGQnYW5nbGUgbm9uIG9yaWVudMOpAAAAAgAAAAMAAAADAAAAYQAAAGYAAABk#####wAAAAEADENCaXNzZWN0cmljZQAAAABnAQAAAAAQAAABAAAAAQAAAGEAAABmAAAAZAAAABYAAAAAZwEAAAAAEAAAAQAABQABQG8lqGWscakAAABo#####wAAAAEAF0NNZXN1cmVBbmdsZUdlb21ldHJpcXVlAQAAAGcAAABhAAAAZgAAAGT#####AAAAAgAXQ01hcnF1ZUFuZ2xlR2VvbWV0cmlxdWUBAAAAZwEAAP8AAAABAAAAAUBFBhd#VJG7AAAAYQAAAGYAAABkAAAAEwEAAABnAQAA#wBACAAAAAAAAAAAAAAAAAAAAAAAAABpDwAAAAAAAQAAAAEAAAABAAAAAAAAAAAAAAACwrAAAAAAav####8AAAABAAlDUm90YXRpb24A#####wAAAGb#####AAAAAQAMQ01vaW5zVW5haXJlAAAAGAIAAAAZAAAAPwAAABkAAABqAAAAGgD#####AQAA#wAQAAAAAAAAAAAAAABACAAAAAAAAAAABQAAAABhAAAAbQAAAAMA#####wEAAAAAAAABAAAABAAAAAIAAAADAAAAbgAAAAIAAAAPAP####8BAAD#ABAAAAEAAAABAAAAZAAAAFkAAAAWAP####8BAAD#ABAAAUkAAAAAAAAAAABACAAAAAAAAAAABQABP+aV94D+YzAAAABwAAAADgD#####ABtNZXN1cmUgZCdhbmdsZSBub24gb3JpZW50w6kAAAACAAAAAwAAAAMAAABZAAAAcQAAAGQAAAAfAAAAAHIBAAAAABAAAAEAAAABAAAAWQAAAHEAAABkAAAAFgAAAAByAQAAAAAQAAABAAAFAAFAbyWoZaxxqQAAAHMAAAAgAQAAAHIAAABZAAAAcQAAAGQAAAAhAQAAAHIBAAD#AAAAAQAAAAFARQYXf1SRuwAAAFkAAABxAAAAZAAAABMBAAAAcgEAAP8AQAgAAAAAAAAAAAAAAAAAAAAAAAAAdA8AAAAAAAEAAAABAAAAAQAAAAAAAAAAAAAAAsKwAAAAAHUAAAAiAP####8AAABxAAAAGAIAAAAZAAAAPwAAABkAAAB1AAAAGgD#####AQAA#wAQAAAAAAAAAAAAAABACAAAAAAAAAAABQAAAABZAAAAeAAAAA8A#####wEAAAAAEAAAAQAAAAEAAAAOAAAAUQAAABYA#####wEAAAAAEAABTAAAAAAAAAAAAEAIAAAAAAAAAAAFAAE#p8Q17vH6mwAAAHoAAAAOAP####8AG01lc3VyZSBkJ2FuZ2xlIG5vbiBvcmllbnTDqQAAAAIAAAADAAAAAwAAAA4AAAB7AAAAUQAAAB8AAAAAfAEAAAAAEAAAAQAAAAEAAAAOAAAAewAAAFEAAAAWAAAAAHwBAAAAABAAAAEAAAUAAUBvJahlrHGpAAAAfQAAACABAAAAfAAAAA4AAAB7AAAAUQAAACEBAAAAfAEAAAAAAAABAAAAAUBFBhd#VJG7AAAADgAAAHsAAABRAAAAEwEAAAB8AQAAAABACAAAAAAAAAAAAAAAAAAAAAAAAAB+DwAAAAAAAQAAAAEAAAABAAAAAAAAAAAAAAACwrAAAAAAfwAAACIA#####wAAAHsAAAAYAgAAABkAAAA#AAAAGQAAAH8AAAAaAP####8BAAAAABAAAAAAAAAAAAAAAEAIAAAAAAAAAAAFAAAAAFEAAACCAAAAAwD#####AQAAAAAAAAEAAAAEAAAAgwAAAEYAAAACAAAAgwAAAAMA#####wEAAAAAAAABAAAABAAAAHkAAABGAAAAAwAAAHn#####AAAAAQAQQ1N1cmZhY2VQb2x5Z29uZQD#####Af###wADUzM0AAAABAAAAAoAAAAkAP####8B####AANTMzUAAAAEAAAAEAAAACQA#####wH###8AA1MzNgAAAAQAAAAGAAAAJAD#####Af###wADUzM3AAAABAAAAAQAAAANAP####8AAAAAABAAAAEAA1M0MgABAAAACQAAAAEAAAANAP####8AAAAAABAAAAEAA1M0MwABAAAACQAAAAIAAAANAP####8AAAAAABAAAAEAA1M0NAABAAAAAQAAAAUAAAANAP####8AAAAAABAAAAEAA1M0NQABAAAAAQAAAAIAAAANAP####8AAAAAABAAAAEAA1M0NgABAAAAAQAAAAMAAAANAP####8AAAAAABAAAAEAA1M0NwABAAAAAwAAAAUAAAANAP####8AAAAAABAAAAEAA1M0OQABAAAAAgAAAA8AAAANAP####8AAAAAABAAAAEAA1M1MAABAAAAAwAAAA8AAAAkAP####8B####AANTNDAAAAAEAAAAhAAAAA0A#####wEAAAAAEAAAAQADUzUxAAEAAACDAAAARgAAAA0A#####wEAAAAAEAAAAQADUzUyAAEAAACDAAAAAgAAAA0A#####wEAAAAAEAAAAQADUzU0AAEAAABGAAAAAgAAACQA#####wH###8AA1M0MQAAAAQAAABHAAAAJAD#####Af###wADUzM4AAAABAAAAIUAAAANAP####8BAAAAABAAAAEAA1M1MwABAAAARgAAAHkAAAANAP####8BAAAAABAAAAEAA1M1NgABAAAAAwAAAHkAAAANAP####8BAAAAABAAAAEAA1M1NQABAAAARgAAAAMAAAANAP####8AAAAAABAAAAEAA1M0OAABAAAAAgAAAAMAAAAkAP####8B####AANTMzkAAAAEAAAAbwAAAA0A#####wEAAAAAEAAAAQADUzU4AAEAAABuAAAAAwAAAA0A#####wEAAAAAEAAAAQADUzU3AAEAAAACAAAAbgAAABj##########w=='
      case 'pi5': return 'TWF0aEdyYXBoSmF2YTEuMAAAABI+TMzNAAJmcv###wEA#wEAAAAAAAAAAARLAAACWAAAAQEAAAAAAAAAAQAAADX#####AAAAAQAKQ0NhbGNDb25zdAD#####AAJwaQAWMy4xNDE1OTI2NTM1ODk3OTMyMzg0Nv####8AAAABAApDQ29uc3RhbnRlQAkh+1RELRj#####AAAAAQAKQ1BvaW50QmFzZQD#####AQAAAAAQAAAAAAAAAAAAAABACAAAAAAAAAUAAUBaJtOgbToGQE3x64UeuFAAAAACAP####8BAAAAABAAAAAAAAAAAAAAAEAIAAAAAAAABQABQCGL8li#JYBAXLj1wo9cKAAAAAIA#####wEAAAAAEAAAAAAAAAAAAAAAQAgAAAAAAAAFAAFAX5F+SxfksEBcuPXCj1wo#####wAAAAEACUNQb2x5Z29uZQD#####AAAAAAABAAAABAAAAAEAAAACAAAAAwAAAAEAAAACAP####8BAAAAABAAAAAAAAAAAAAAAEAIAAAAAAAABQABQGW4vyWL8lhAUa5LF+SxfgAAAAMA#####wAAAAAAAQAAAAQAAAABAAAABQAAAAMAAAAB#####wAAAAEACUNDZXJjbGVPQQD#####AQAAAAABAAAAAwAAAAUAAAAEAP####8BAAAAAAEAAAABAAAABf####8AAAABAA9DUG9pbnRMaWVDZXJjbGUA#####wEAAAAAEAAAAAAAAAAAAAAAQAgAAAAAAAAFAAFABAD9sei54AAAAAgAAAADAP####8AAAAAAAEAAAAEAAAACQAAAAIAAAABAAAACQAAAAQA#####wEAAAAAAQAAAAIAAAAJ#####wAAAAEACENWZWN0ZXVyAP####8BAAAAABAAAAEAAQAAAAIAAAADAP####8AAAABABBDSW50Q2VyY2xlQ2VyY2xlAP####8AAAALAAAAB#####8AAAABABBDUG9pbnRMaWVCaXBvaW50AP####8BAAAAABAAAAAAAAAAAAAAAEAIAAAAAAAABQACAAAADQAAAAgA#####wEAAAAAEAAAAAAAAAAAAAAAQAgAAAAAAAAFAAEAAAANAAAAAwD#####AQAAAAABAAAABAAAAAIAAAADAAAADwAAAAL#####AAAAAQAJQ0Ryb2l0ZUFCAP####8B#wAAABAAAAEAAQAAAAIAAAAD#####wAAAAEAFkNEcm9pdGVQZXJwZW5kaWN1bGFpcmUA#####wH#AAAAEAAAAQABAAAAAgAAABEAAAAKAP####8B#wAAABAAAAEAAQAAAAEAAAAS#####wAAAAEAEENJbnREcm9pdGVEcm9pdGUA#####wH#AAAAEAABQwAAAAAAAAAAAEAIAAAAAAAABQAAAAASAAAAEwAAAAIA#####wH#AAAAEAABRAAAAAAAAAAAAEAIAAAAAAAABQABQEZNp0DadAhAU1j1wo9cKAAAAAIA#####wD#AAAAEAABQQAAAAAAAAAAAEAIAAAAAAAABQABwDAAAAAAAABAfZ4UeuFHrgAAAAIA#####wD#AAAAEAABQgAAAAAAAAAAAEAIAAAAAAAABQABQFacKPXCj1xAezuSxfksYP####8AAAABAAlDTG9uZ3VldXIA#####wAAABYAAAAX#####wAAAAEACENTZWdtZW50AP####8B#wAAABAAAAEAAQAAABQAAAAV#####wAAAAEAFENJbXBsZW1lbnRhdGlvblByb3RvAP####8AEk1lc3VyZSBkZSBsb25ndWV1cgAAAAUAAAACAAAAAgAAABQAAAAV#####wAAAAEAC0NNZWRpYXRyaWNlAAAAABoBAAAAAA0AAAEAAQAAABQAAAAV#####wAAAAEAB0NNaWxpZXUAAAAAGgEAAAAADQAAAQUAAAAAFAAAABX#####AAAAAgAJQ0NlcmNsZU9SAAAAABoBAAAAAAEAAAAcAAAAAUAwAAAAAAAAAf####8AAAABABBDSW50RHJvaXRlQ2VyY2xlAAAAABoAAAAbAAAAHQAAAAgAAAAAGgEAAAAADQAAAQUAAQAAAB4AAAAMAQAAABoAAAAUAAAAFf####8AAAABAA9DVmFsZXVyQWZmaWNoZWUBAAAAGgH#AAABAAAAHxEAAAAAAAEAAAABAAAAAQAAAAAAAAAAAAAAAAEAAAAg#####wAAAAEAB0NDYWxjdWwA#####wAFbWluaTEAATAAAAABAAAAAAAAAAAAAAAUAP####8ABW1heGkxAAExAAAAAT#wAAAAAAAAAAAAFAD#####AAVtaW5pMgABMAAAAAEAAAAAAAAAAAAAABQA#####wAFbWF4aTIAATEAAAABP#AAAAAAAAD#####AAAAAQAQQ0Ryb2l0ZVBhcmFsbGVsZQD#####Af8AAAAQAAABAAEAAAAJAAAAEgAAABUA#####wH#AAAAEAAAAQABAAAACQAAABMAAAALAP####8B#wAAABAAAAAAAAAAAAAAAEAIAAAAAAAABQAAAAASAAAAJwAAAAYA#####wH#AAAAEAAAAQABAAAAKAAAAAkA#####wAAAAEADUNUcmFuc1BhclZlY3QA#####wAAACkAAAAVAP####8B#wAAABAAAAEAAQAAAAUAAAARAAAACwD#####Af8AAAAQAAAAAAAAAAAAAABACAAAAAAAAAUAAAAAKwAAABIAAAAGAP####8B#wAAABAAAAEAAQAAACwAAAAFAAAAABYA#####wAAAC0AAAAVAP####8B#wAAABAAAAEAAQAAAA8AAAARAAAACwD#####Af8AAAAQAAAAAAAAAAAAAABACAAAAAAAAAUAAAAAEgAAAC8AAAAGAP####8B#wAAABAAAAEAAQAAADAAAAAPAAAAABYA#####wAAADH#####AAAAAQAPQ1N5bWV0cmllQXhpYWxlAP####8AAAAR#####wAAAAEAC0NQb2ludEltYWdlAP####8BAAD#ABAAAUcAAAAAAAAAAABACAAAAAAAAAUAAAAADwAAADMAAAAY##########8='
      case 'pi6': return 'TWF0aEdyYXBoSmF2YTEuMAAAABI+TMzNAAJmcv###wEA#wEAAAAAAAAAAARLAAACWAAAAQEAAAAAAAAAAQAAADj#####AAAAAQAKQ0NhbGNDb25zdAD#####AAJwaQAWMy4xNDE1OTI2NTM1ODk3OTMyMzg0Nv####8AAAABAApDQ29uc3RhbnRlQAkh+1RELRj#####AAAAAQAKQ1BvaW50QmFzZQD#####AQAAAAAQAAAAAAAAAAAAAABACAAAAAAAAAUAAUBaJtOgbToGQEbx64UeuFAAAAACAP####8BAAAAABAAAAAAAAAAAAAAAEAIAAAAAAAABQABQCGL8li#JYBAWTj1wo9cKAAAAAIA#####wEAAAAAEAAAAAAAAAAAAAAAQAgAAAAAAAAFAAFAX5F+SxfksEBZOPXCj1wo#####wAAAAEACUNQb2x5Z29uZQD#####AAAAAAABAAAABAAAAAEAAAACAAAAAwAAAAEAAAACAP####8BAAAAABAAAAAAAAAAAAAAAEAIAAAAAAAABQABQGW4vyWL8lhATFyWL8li#AAAAAMA#####wAAAAAAAQAAAAQAAAABAAAABQAAAAMAAAAB#####wAAAAEACUNDZXJjbGVPQQD#####AQAAAAABAAAAAwAAAAUAAAAEAP####8BAAAAAAEAAAABAAAABf####8AAAABAA9DUG9pbnRMaWVDZXJjbGUA#####wEAAAAAEAAAAAAAAAAAAAAAQAgAAAAAAAAFAAFABAD9sei54AAAAAgAAAADAP####8AAAAAAAEAAAAEAAAACQAAAAIAAAABAAAACQAAAAQA#####wEAAAAAAQAAAAIAAAAJ#####wAAAAEACENWZWN0ZXVyAP####8BAAAAABAAAAEAAQAAAAIAAAADAP####8AAAABABBDSW50Q2VyY2xlQ2VyY2xlAP####8AAAALAAAAB#####8AAAABABBDUG9pbnRMaWVCaXBvaW50AP####8BAAAAABAAAAAAAAAAAAAAAEAIAAAAAAAABQACAAAADQAAAAgA#####wEAAAAAEAAAAAAAAAAAAAAAQAgAAAAAAAAFAAEAAAANAAAAAwD#####AQAAAAABAAAABAAAAAIAAAADAAAADwAAAAL#####AAAAAQAJQ0Ryb2l0ZUFCAP####8B#wAAABAAAAEAAQAAAAIAAAAD#####wAAAAEAFkNEcm9pdGVQZXJwZW5kaWN1bGFpcmUA#####wH#AAAAEAAAAQABAAAAAgAAABEAAAAKAP####8B#wAAABAAAAEAAQAAAAEAAAAS#####wAAAAEAEENJbnREcm9pdGVEcm9pdGUA#####wH#AAAAEAABQwAAAAAAAAAAAEAIAAAAAAAABQAAAAASAAAAEwAAAAIA#####wH#AAAAEAABRAAAAAAAAAAAAEAIAAAAAAAABQABQEZNp0DadAhAT7HrhR64UAAAAAIA#####wD#AAAAEAABQQAAAAAAAAAAAEAIAAAAAAAABQABwDAAAAAAAABAfL4UeuFHrgAAAAIA#####wD#AAAAEAABQgAAAAAAAAAAAEAIAAAAAAAABQABQFacKPXCj1xAeluSxfksYP####8AAAABAAlDTG9uZ3VldXIA#####wAAABYAAAAX#####wAAAAEACENTZWdtZW50AP####8B#wAAABAAAAEAAQAAABQAAAAV#####wAAAAEAFENJbXBsZW1lbnRhdGlvblByb3RvAP####8AEk1lc3VyZSBkZSBsb25ndWV1cgAAAAUAAAACAAAAAgAAABQAAAAV#####wAAAAEAC0NNZWRpYXRyaWNlAAAAABoBAAAAAA0AAAEAAQAAABQAAAAV#####wAAAAEAB0NNaWxpZXUAAAAAGgEAAAAADQAAAQUAAAAAFAAAABX#####AAAAAgAJQ0NlcmNsZU9SAAAAABoBAAAAAAEAAAAcAAAAAUAwAAAAAAAAAf####8AAAABABBDSW50RHJvaXRlQ2VyY2xlAAAAABoAAAAbAAAAHQAAAAgAAAAAGgEAAAAADQAAAQUAAQAAAB4AAAAMAQAAABoAAAAUAAAAFf####8AAAABAA9DVmFsZXVyQWZmaWNoZWUBAAAAGgH#AAABAAAAHxEAAAAAAAEAAAABAAAAAQAAAAAAAAAAAAAAAAEAAAAg#####wAAAAEAB0NDYWxjdWwA#####wAFbWluaTEAATAAAAABAAAAAAAAAAAAAAAUAP####8ABW1heGkxAAExAAAAAT#wAAAAAAAAAAAAFAD#####AAVtaW5pMgABMAAAAAEAAAAAAAAAAAAAABQA#####wAFbWF4aTIAATEAAAABP#AAAAAAAAD#####AAAAAQAQQ0Ryb2l0ZVBhcmFsbGVsZQD#####Af8AAAAQAAABAAEAAAAJAAAAEgAAABUA#####wH#AAAAEAAAAQABAAAACQAAABMAAAALAP####8B#wAAABAAAAAAAAAAAAAAAEAIAAAAAAAABQAAAAASAAAAJwAAAAYA#####wH#AAAAEAAAAQABAAAAKAAAAAkA#####wAAAAEADUNUcmFuc1BhclZlY3QA#####wAAACkAAAAVAP####8B#wAAABAAAAEAAQAAAAUAAAARAAAACwD#####Af8AAAAQAAAAAAAAAAAAAABACAAAAAAAAAUAAAAAKwAAABIAAAAGAP####8B#wAAABAAAAEAAQAAACwAAAAFAAAAABYA#####wAAAC0AAAAVAP####8B#wAAABAAAAEAAQAAAA8AAAARAAAACwD#####Af8AAAAQAAAAAAAAAAAAAABACAAAAAAAAAUAAAAAEgAAAC8AAAAGAP####8B#wAAABAAAAEAAQAAADAAAAAPAAAAABYA#####wAAADH#####AAAAAQAPQ1N5bWV0cmllQXhpYWxlAP####8AAAAR#####wAAAAEAC0NQb2ludEltYWdlAP####8BAAD#ABAAAUcAAAAAAAAAAABACAAAAAAAAAUAAAAADwAAADMAAAACAP####8BAAD#ABAAAAAAAAAAAAAAAEAIAAAAAAAABQABQF7AAAAAAABAX3hR64UeuAAAAAMA#####wAAAAAAAQAAAAQAAAADAAAANQAAAAIAAAADAAAAAwD#####AAAAAAABAAAABAAAADUAAAADAAAABQAAADUAAAAY##########8='
    }
  }

  function creeLesDiv () {
    stor.lesdiv = {}
    stor.lesdiv.conteneur = j3pAddElt(me.zonesElts.MG, 'div', {
      style: me.styles.etendre('toutpetit.enonce', { padding: '10px' })
    })
    const ttabz = addDefaultTable(stor.lesdiv.conteneur, 1, 3)
    stor.lesdiv.consigne = addDefaultTable(ttabz[0][0], 1, 1)[0][0]
    stor.lesdiv.explications = j3pAddElt(stor.lesdiv.conteneur, 'div')
    const ttabg = addDefaultTable(stor.lesdiv.conteneur, 1, 4)
    stor.lesdiv.consigneF = ttabg[0][0]
    stor.lesdiv.travail = ttabg[0][1]
    ttabg[0][2].style.width = '10px'

    ttabz[0][1].style.width = '10px'
    const ttab = addDefaultTable(ttabg[0][3], 2, 1)
    stor.lesdiv.correction = ttabz[0][2]
    ttab[0][0].style.height = '10px'
    stor.lesdiv.solution = ttab[1][0]

    stor.lesdiv.explications.style.color = me.styles.cfaux
    stor.lesdiv.solution.style.color = me.styles.toutpetit.correction.color
  }

  function makefig () {
    stor.lesdiv.consigneF.style.border = 'solid black 1px'
    stor.x = stor.y = stor.z = 0
    stor.captu = false
    try {
      stor.renderer = new THREE.WebGLRenderer({ alpha: true })
    } catch (error) {
      console.error(error)
      j3pShowError('Votre navigateur ne permet pas d’affichage 3D, impossible de continuer')
      return
    }
    stor.renderer.setClearColor(0x000000, 0)
    stor.renderer.setSize(200, 400)
    stor.lesdiv.consigneF.appendChild(stor.renderer.domElement)
    stor.scene = new THREE.Scene()
    stor.camera = new THREE.PerspectiveCamera(50, 0.5, 1, 1000)
    stor.camera.position.set(0, 0, 200)
    stor.scene.add(stor.camera)
    stor.groupe = new THREE.Group()

    let matmat
    let line
    let materl = new THREE.MeshBasicMaterial({ color: 0xeeeeee, transparent: true, opacity: 0.9 })
    const materlb = new THREE.MeshBasicMaterial({ color: 0xff00ff, transparent: true, opacity: 0.8 })
    const materlr = new THREE.MeshBasicMaterial({ color: 0xff0000, transparent: true, opacity: 0.8 })
    const materlv = new THREE.MeshBasicMaterial({ color: 0x008800, transparent: true, opacity: 0.8 })

    stor.listFace = []
    for (let i = 0; i < stor.listeFacePat.length; i++) {
      matmat = materl
      if (stor.encours === 'f') {
        if (stor.numFace[stor.faceb - 1] !== undefined) if (stor.numFace[stor.faceb - 1].indexOf(i) !== -1) matmat = materlb
        if (stor.numFace[stor.facer - 1] !== undefined) if (stor.numFace[stor.facer - 1].indexOf(i) !== -1) matmat = materlr
        if (stor.numFace[stor.facev - 1] !== undefined) if (stor.numFace[stor.facev - 1].indexOf(i) !== -1) matmat = materlv
      }
      if (stor.encours === 'c') {
        matmat = materlb
      }
      if (stor.listeFacePat[i].a === true) {
        line = new THREE.BoxGeometry(stor.listeFacePat[i].l, stor.listeFacePat[i].l2, stor.epAr, 1, 1, 1)
        stor.listFace.push(new THREE.Mesh(line, matmat))
        stor.listFace[stor.listFace.length - 1].position.set(stor.listeFacePat[i].pos[0], stor.listeFacePat[i].pos[1], stor.listeFacePat[i].pos[2])
        stor.listFace[stor.listFace.length - 1].rotation.x = stor.listeFacePat[i].r[0]
        stor.listFace[stor.listFace.length - 1].rotation.y = stor.listeFacePat[i].r[1]
        stor.listFace[stor.listFace.length - 1].rotation.z = stor.listeFacePat[i].r[2]
      } else if (stor.listeFacePat[i].a === 'p') {
        line = new PrismGeometry(stor.listeFacePat[i].tab, stor.epAr)
        stor.listFace.push(new THREE.Mesh(line, matmat))
        stor.listFace[stor.listFace.length - 1].position.set(stor.listeFacePat[i].pos[0], stor.listeFacePat[i].pos[1], stor.listeFacePat[i].pos[2])
        stor.listFace[stor.listFace.length - 1].rotation.x = stor.listeFacePat[i].r[0]
        stor.listFace[stor.listFace.length - 1].rotation.y = stor.listeFacePat[i].r[1]
        stor.listFace[stor.listFace.length - 1].rotation.z = stor.listeFacePat[i].r[2]
      } else if (stor.listeFacePat[i].a === 'c') {
        line = new THREE.CylinderGeometry(stor.listeFacePat[i].l1, stor.listeFacePat[i].l2, stor.listeFacePat[i].e, 100)
        stor.listFace.push(new THREE.Mesh(line, matmat))
        stor.listFace[stor.listFace.length - 1].position.set(stor.listeFacePat[i].pos[0], stor.listeFacePat[i].pos[1], stor.listeFacePat[i].pos[2])
        stor.listFace[stor.listFace.length - 1].rotation.x = stor.listeFacePat[i].r[0]
        stor.listFace[stor.listFace.length - 1].rotation.y = stor.listeFacePat[i].r[1]
        stor.listFace[stor.listFace.length - 1].rotation.z = stor.listeFacePat[i].r[2]
      } else if (stor.listeFacePat[i].a === 't') {
        line = torGeo(stor.listeFacePat[i].l1, stor.listeFacePat[i].l2, stor.listeFacePat[i].e)
        stor.listFace.push(new THREE.Mesh(line, matmat))
        stor.listFace[stor.listFace.length - 1].position.set(stor.listeFacePat[i].pos[0], stor.listeFacePat[i].pos[1], stor.listeFacePat[i].pos[2])
        stor.listFace[stor.listFace.length - 1].rotation.x = stor.listeFacePat[i].r[0]
        stor.listFace[stor.listFace.length - 1].rotation.y = stor.listeFacePat[i].r[1]
        stor.listFace[stor.listFace.length - 1].rotation.z = stor.listeFacePat[i].r[2]
      } else {
        line = new THREE.ConeGeometry(30, 60, 50)
        stor.listFace.push(new THREE.Mesh(line, matmat))
        stor.listFace[stor.listFace.length - 1].position.set(stor.listeFacePat[i].pos[0], stor.listeFacePat[i].pos[1], stor.listeFacePat[i].pos[2])
        stor.listFace[stor.listFace.length - 1].rotation.x = stor.listeFacePat[i].r[0]
        stor.listFace[stor.listFace.length - 1].rotation.y = stor.listeFacePat[i].r[1]
        stor.listFace[stor.listFace.length - 1].rotation.z = stor.listeFacePat[i].r[2]
      }
    }
    for (let i = 0; i < stor.listFace.length; i++) {
      stor.groupe.add(stor.listFace[i])
    }

    materl = new THREE.MeshBasicMaterial({ color: 0x000000 })
    const materlM = new THREE.MeshBasicMaterial({ color: 0xaa00aa })
    stor.listLigne = []
    for (let i = 0; i < stor.listeLignePat.length; i++) {
      matmat = materl
      if (stor.lareteNi === i) matmat = materlM
      if (stor.listeLignePat[i].a === true) {
        line = new THREE.BoxGeometry(stor.listeLignePat[i].l, stor.listeLignePat[i].e, stor.listeLignePat[i].e, 1, 1, 1)
        stor.listLigne.push(new THREE.Mesh(line, matmat))
        stor.listLigne[stor.listLigne.length - 1].position.set(stor.listeLignePat[i].pos[0], stor.listeLignePat[i].pos[1], stor.listeLignePat[i].pos[2])
        stor.listLigne[stor.listLigne.length - 1].rotation.x = stor.listeLignePat[i].r[0]
        stor.listLigne[stor.listLigne.length - 1].rotation.y = stor.listeLignePat[i].r[1]
        stor.listLigne[stor.listLigne.length - 1].rotation.z = stor.listeLignePat[i].r[2]
      } else if (stor.listeLignePat[i].a === 'c') {
        line = new THREE.TorusGeometry(stor.listeLignePat[i].l1, stor.listeLignePat[i].e, 16, 150)
        stor.listLigne.push(new THREE.Mesh(line, matmat))
        stor.listLigne[stor.listLigne.length - 1].position.set(stor.listeLignePat[i].pos[0], stor.listeLignePat[i].pos[1], stor.listeLignePat[i].pos[2])
        stor.listLigne[stor.listLigne.length - 1].rotation.x = stor.listeLignePat[i].r[0]
        stor.listLigne[stor.listLigne.length - 1].rotation.y = stor.listeLignePat[i].r[1]
        stor.listLigne[stor.listLigne.length - 1].rotation.z = stor.listeLignePat[i].r[2]
      }
    }
    for (let i = 0; i < stor.listLigne.length; i++) {
      stor.groupe.add(stor.listLigne[i])
    }

    stor.scene.add(stor.groupe)
    // on effectue le rendu de la scène
    stor.renderer.render(stor.scene, stor.camera)
    stor.renderer.domElement.addEventListener('mousedown', animateDown, false)
    stor.renderer.domElement.addEventListener('mouseup', animateStop, false)
    stor.renderer.domElement.addEventListener('mousemove', animateMov, false)
    stor.renderer.domElement.style.cursor = 'pointer'
    animate()
  }

  function animateDown (ev) {
    stor.captu = true
    stor.x = ev.clientX
    stor.y = ev.clientY
  }

  function animateStop () {
    stor.captu = false
  }

  function animateMov (ev) {
    if (!stor.captu) return
    const X = ev.clientX
    const Y = ev.clientY
    if ((Math.abs(stor.groupe.rotation.x) % (2 * Math.PI) < 3 * Math.PI / 2) && (Math.abs(stor.groupe.rotation.x) % (2 * Math.PI) > Math.PI / 2)) {
      stor.groupe.rotation.y -= Math.atan((X - stor.x) / 100)
    } else {
      stor.groupe.rotation.y += Math.atan((X - stor.x) / 100)
    }

    stor.groupe.rotation.x += Math.atan((Y - stor.y) / 100)
    stor.x = X
    stor.y = Y
    stor.renderer.render(stor.scene, stor.camera)
  }

  function animate () {
    if (stor.exo.solide === 'un cylindre de révolution') {
      if (stor.groupe.rotation.x < -0.8) return
      requestAnimationFrame(animate)
      stor.groupe.rotation.y -= 0.02
      stor.groupe.rotation.x -= 0.01
    } else {
      if (stor.groupe.rotation.x > 0.6) return
      requestAnimationFrame(animate)
      stor.groupe.rotation.y += 0.02
      stor.groupe.rotation.x += 0.01
    }
    stor.renderer.render(stor.scene, stor.camera)
  }

  function faisFigEtiq (id, txt) {
    const yui = j3pGetNewId('mtg32')
    j3pCreeSVG(id, { id: yui, width: stor.widthetiq - 10, height: stor.heightetiq - 10 })
    stor.mtgAppLecteur.addDoc(yui, txt, true)
    stor.mtgAppLecteur.calculateAndDisplayAll(true)
    stor.bufID = yui
    stor.mtgAppLecteur.updateFigure(yui)
    id.setAttribute('class', 'zoneClikBase')
    id.classList.add('zoneClikActive')
    return yui
  }

  function yaReponse () {
    if (stor.encours === 'c') {
      return stor.lazone.reponse.length !== 0
    }
    if (stor.encours === 'f') {
      if (stor.nbclic === 0) {
        affModale('Il faut sélectionner au moins une face ! <br><i>Clique sur une face du patron pour la sélectionner.</i>')
        return false
      }
      if (stor.nbclic > 1) {
        affModale('Tu as sélectionné plusieurs faces ! <br><i>Re-Clique sur une face du patron pour retirer la sélection.</i>')
        return false
      }
      return true
    }
    if (stor.encours === 'a') {
      if (stor.nbclic === 0) {
        affModale('Il faut sélectionner au moins ' + stor.ligne + ' ! <br><i>Clique sur ' + stor.ligne + ' du patron pour ' + stor.lale + ' sélectionner.</i>')
        return false
      }
      if (stor.nbclic > 1 && stor.exo.col) {
        affModale('Tu as sélectionné ' + stor.lignes + ' ! <br><i>Re-Clique sur ' + stor.ligne + ' du patron pour retirer la sélection.</i>')
        return false
      }
      return true
    }
  }

  function affModale (texte) {
    const yy = j3pModale({ titre: 'Erreur', contenu: '' })
    j3pAffiche(yy, null, texte)
    /// /PB ICI
    yy.style.color = me.styles.toutpetit.correction.color
  }

  function isRepOk () {
    stor.lerrTrop = false
    stor.lerrMank = false

    if (stor.encours === 'c') {
      if (stor.lazone.reponse[0] !== 0) stor.lazone.corrige(false)
      stor.lazrep = stor.lazone.reponse[0]
      return stor.lazone.reponse[0] === 0
    }
    if (stor.encours === 'f') {
      for (let i = 0; i < stor.FaceSel.length; i++) {
        if (stor.FaceSel[i].select) stor.larep = i
      }
      return (stor.larep === stor.faceb - 1)
    }
    if (stor.encours === 'a') {
      stor.larep = []
      for (let i = 0; i < stor.FaceSel.length; i++) {
        if (!stor.FaceSel[i].disabled) stor.FaceSel[i].fautVoir = false
        if (stor.FaceSel[i].select) {
          stor.larep.push(stor.FaceSel[i])
          stor.larep[stor.larep.length - 1].dou = i
        }
      }
      if (stor.exo.col) {
        if ((stor.larep[0].groupe !== stor.legroupel) || (stor.larep[0].areteN !== stor.lareteN)) {
          colorFace(stor.larep[0].id, 214, 71, 0, 5)
          stor.FaceSel[stor.larep[0].dou].coolMo = [214, 71, 0]
          stor.FaceSel[stor.larep[0].dou].fautVoir = true
          return false
        }
        return true
      }

      // verif les select sont dans mm group
      let ok = true
      for (let i = 0; i < stor.larep.length; i++) {
        stor.FaceSel[stor.larep[i].dou].fautVoir = true
        if (stor.larep[i].groupe !== stor.legroupel) {
          ok = false
          colorFace(stor.larep[i].id, 214, 71, 0, 5)
          stor.FaceSel[stor.larep[i].dou].coolMo = [214, 71, 0]
          stor.FaceSel[stor.larep[i].dou].fautVoir = true
        } else {
          colorFace(stor.larep[i].id, 0, 138, 115, 5)
          stor.FaceSel[stor.larep[i].dou].coolMo = [0, 138, 115]
          stor.FaceSel[stor.larep[i].dou].fautVoir = true
        }
      }
      if (!ok) {
        stor.lerrTrop = true
      }

      ok = true
      // verif dans les pas select que y’a pas le groupe
      for (let i = 0; i < stor.FaceSel.length; i++) {
        if (stor.FaceSel[i].groupe === stor.legroupel && !stor.FaceSel[i].select && !stor.FaceSel[i].disabled) {
          // colorFace(stor.FaceSel[i].id, 0, 0, 255)
          stor.FaceSel[i].fautVoir = true
          stor.FaceSel[i].coolMo = [100, 100, 200]
          ok = false
        }
      }
      if (!ok) {
        stor.lerrMank = true
      }
      return !stor.lerrTrop && !stor.lerrMank
    }

    stor.lesdiv.solution.classList.remove('correction')
    stor.lesdiv.explications.classList.remove('explique')
    return true
  }

  function desactivezone () {
    if (stor.encours === 'c') {
      stor.lazone.disable()
    }
    if (stor.encours === 'f') {
      for (let i = 0; i < stor.FaceSel.length; i++) {
        stor.FaceSel[i].disabled = true
      }
    }
    if (stor.encours === 'a') {
      for (let i = 0; i < stor.FaceSel.length; i++) {
        stor.FaceSel[i].disabled = true
      }
    }
  }

  function setMoov4 () {
    for (let i = 0; i < stor.lescotes.length; i++) {
      for (let j = 0; j < stor.lescotes[i].length; j++) {
        for (let k = 0; k < stor.lescotes[i][j].moov.length; k++) {
          for (let l = 0; l < stor.lescotes[i][j].moov[k].length; l++) {
            stor.mtgAppLecteur.setVisible(stor.bufID, stor.lescotes[i][j].moov[k][l], true, true)
            stor.mtgAppLecteur.setLineStyle(stor.bufID, stor.lescotes[i][j].moov[k][l], null, 4, true)
          }
        }
      }
    }
  }

  function passetoutvert () {
    if (stor.encours === 'c') {
      stor.lazone.corrige(true)
    }
    if (stor.encours === 'f') {
      for (let i = 0; i < stor.FaceSel.length; i++) {
        if (stor.FaceSel[i].select) {
          colorFace(stor.FaceSel[i].id, 127, 0, 255)
          stor.FaceSel[i].fautVoir = true
          stor.FaceSel[i].coolMo = [127, 0, 255]
          colorFace(stor.FaceSel[i].moov, 127, 0, 255)
        }
      }
    }
    if (stor.encours === 'a') {
      for (let i = 0; i < stor.FaceSel.length; i++) {
        if (stor.FaceSel[i].select) {
          colorFace(stor.FaceSel[i].id, 0, 138, 115)
          stor.FaceSel[i].fautVoir = true
          stor.FaceSel[i].coolMo = [0, 138, 115]
          colorFace(stor.FaceSel[i].moov, 0, 138, 115)
        }
      }
    }
  }

  function faceCache (tab, bool) {
    for (let i = 0; i < tab.length; i++) {
      stor.mtgAppLecteur.setVisible(stor.bufID, tab[i], bool, true)
    }
  }

  function afficheFaceFix (bool) {
    for (let i = 0; i < stor.numFacePat.length; i++) {
      for (let j = 0; j < stor.numFacePat[i].fixe.length; j++) {
        stor.mtgAppLecteur.setVisible(stor.bufID, stor.numFacePat[i].fixe[j], bool, true)
      }
    }
  }

  function afficheSegFix (bool) {
    for (let i = 0; i < stor.lescotes.length; i++) {
      for (let j = 0; j < stor.lescotes[i].length; j++) {
        for (let k = 0; k < stor.lescotes[i][j].fixe.length; k++) {
          for (let l = 0; l < stor.lescotes[i][j].fixe[k].length; l++) {
            stor.mtgAppLecteur.setVisible(stor.bufID, stor.lescotes[i][j].fixe[k][l], bool, true)
            if (stor.yaya === 'a') stor.mtgAppLecteur.setLineStyle(stor.bufID, stor.lescotes[i][j].fixe[k][l], null, 4, true)
          }
        }
      }
    }
    /* for (let i = 0; i < stor.FaceSel.length; i++) {
        if (stor.FaceSel[i].fautVoir) {
          colorFace(stor.FaceSel[i].id, stor.FaceSel[i].coolMo[0], stor.FaceSel[i].coolMo[1], stor.FaceSel[i].coolMo[2], 7)
        }
      } */
  }

  function afficheSegMoov (bool) {
    for (let i = 0; i < stor.lescotes.length; i++) {
      for (let j = 0; j < stor.lescotes[i].length; j++) {
        for (let k = 0; k < stor.lescotes[i][j].moov.length; k++) {
          for (let l = 0; l < stor.lescotes[i][j].moov[k].length; l++) {
            stor.mtgAppLecteur.setVisible(stor.bufID, stor.lescotes[i][j].moov[k][l], bool, true)
            if (stor.yaya === 'a') stor.mtgAppLecteur.setLineStyle(stor.bufID, stor.lescotes[i][j].moov[k][l], null, 6, true)
          }
        }
      }
    }
    /*
      for (let i = 0; i < stor.FaceSel.length; i++) {
        if (stor.FaceSel[i].fautVoir) {
          colorFace(stor.FaceSel[i].id, stor.FaceSel[i].coolMo[0], stor.FaceSel[i].coolMo[1], stor.FaceSel[i].coolMo[2], 7)
        }
      }

       */
  }

  function lanceanimePatron () {
    stor.vala = 0
    stor.valb = 0
    stor.animeCours = 'd'
    setTimeout(animePatron, stor.tempodeb)
  }

  function animePatron () {
    if (stor.raz) return
    if (stor.lesdiv.consigne === null) return
    if (!document.getElementById(stor.bufID)) return
    switch (stor.animeCours) {
      case 'd': // cache polyN
        // faceCache(stor.polyN, false)
        if (stor.yaya === 'f' || stor.yaya === 'a') afficheFaceFix(false)
        afficheSegFix(false)
        afficheSegMoov(true)
        stor.animeCours = 'a'
      // eslint-disable-next-line no-fallthrough
      case 'a':
        stor.vala += 0.05
        if (stor.vala >= 1) {
          stor.vala = 1
          stor.mtgAppLecteur.giveFormula2(stor.bufID, 'aa', stor.vala)
          stor.mtgAppLecteur.calculateAndDisplayAll(true)
          stor.animeCours = 'b'
          setTimeout(animePatron, stor.tempodeb)
          return
        }
        stor.mtgAppLecteur.giveFormula2(stor.bufID, 'aa', stor.vala)
        afficheSegMoov(true)
        stor.mtgAppLecteur.updateFigure(stor.bufID)
        if (stor.yaya === 'a') setMoov4()
        setVisibleFaut()
        setTimeout(animePatron, stor.tempo)
        break
      case 'b':
        stor.valb += 0.05
        if (stor.valb >= 1) {
          stor.valb = 0.9999
          stor.mtgAppLecteur.giveFormula2(stor.bufID, 'bb', stor.valb)
          stor.mtgAppLecteur.calculateAndDisplayAll(true)
          stor.animeCours = 'c'
          setTimeout(animePatron, stor.tempodeb)
          return
        }
        stor.mtgAppLecteur.giveFormula2(stor.bufID, 'bb', stor.valb)
        stor.mtgAppLecteur.calculateAndDisplayAll(true)
        afficheSegMoov(true)
        setVisibleFaut()
        if (stor.yaya === 'a') {
          if (stor.exo.solide === 'un cône de révolution') {
            stor.mtgAppLecteur.setColor(stor.bufID, '#s249', 0, 0, 0, true)
            stor.mtgAppLecteur.setLineStyle(stor.bufID, '#s249', null, 1, true)
            stor.mtgAppLecteur.setColor(stor.bufID, '#s250', 0, 0, 0, true)
            stor.mtgAppLecteur.setLineStyle(stor.bufID, '#s250', null, 1, true)
          }
        }
        setTimeout(animePatron, stor.tempo)
        break
      case 'c':
        stor.vala = 0
        stor.valb = 0
        stor.animeCours = 'd'
        stor.mtgAppLecteur.giveFormula2(stor.bufID, 'bb', stor.valb)
        stor.mtgAppLecteur.giveFormula2(stor.bufID, 'aa', stor.vala)
        stor.mtgAppLecteur.calculateAndDisplayAll(true)
        // montre polyN
        // faceCache(stor.polyN, true)
        if (stor.yaya === 'f') afficheFaceFix(true)
        afficheSegFix(true)
        setTimeout(animePatron, stor.tempodeb)
        break
    }
  }

  function barreLesfaux () {
    if (stor.encours === 'c') {
      stor.evir = stor.evir.concat(stor.lazone.corrige([stor.repG], true))
    }
  }

  function setVisibleFaut () {
    if (stor.bufID !== svgPat) return
    for (let i = 0; i < stor.FaceSel.length; i++) {
      faceCache(stor.FaceSel[i].moov, true)
      if (stor.FaceSel[i].fautVoir) {
        colorFace(stor.FaceSel[i].moov, stor.FaceSel[i].coolMo[0], stor.FaceSel[i].coolMo[1], stor.FaceSel[i].coolMo[2], 7)
      }
    }
  }

  function afficheCorrection (bool) {
    let yaexplik = false
    let yaco = false
    if (stor.lerrTrop) {
      yaexplik = true
      j3pAffiche(stor.lesdiv.explications, null, 'Tu as sélectionné des ' + stor.lignesS + ' qui \nn’ont pas la même longueur !\n<i>(ils sont indiqués en marron)</i>\n\n')
    }
    if (stor.lerrMank) {
      yaexplik = true
      j3pAffiche(stor.lesdiv.explications, null, 'Tu oublies des ' + stor.lignesS + ' !\n')
    }
    if (stor.encours === 'f') {
      for (let i = 0; i < stor.FaceSel.length; i++) {
        if (stor.FaceSel[i].select) {
          colorFace(stor.FaceSel[i].id, 214, 71, 0)
        }
      }
    }
    if (stor.encours === 'c') {
      let buf
      switch (stor.exo.solide) {
        case 'un cube':
          switch (stor.ppos[stor.lazrep]) {
            case 'cu3': buf = 'Les faces ne doivent pas se superposer !'
              break
            case 'pa1':
            case 'pa2':
            case 'pa3':
            case 'pr1':
            case 'pr2':
            case 'pr3':
            case 'pr4':
            case 'pr5':
            case 'pr6':
            case 'cy1':
            case 'cy2':
            case 'cy3':
            case 'co1':
            case 'co2':
            case 'co3':
            case 'pi1':
            case 'pi2':
            case 'pi3':
            case 'pi4':
            case 'pi5':
            case 'pi6':
            case 'cu2': buf = 'Un cube contient 6 faces carrées !'
              break
          }
          break
        case 'un pavé droit':
          switch (stor.ppos[stor.lazrep]) {
            case 'pa3': buf = 'Les faces ne doivent pas se superposer !'
              break
            case 'cu1':
            case 'cu2':
            case 'cu3':
              buf = 'Les faces ne sont pas toutes carrées !'
              break
            case 'pr1':
            case 'pr2':
            case 'pr3':
            case 'pr4':
            case 'pr5':
            case 'pr6':
            case 'cy1':
            case 'cy2':
            case 'cy3':
            case 'co1':
            case 'co2':
            case 'co3':
            case 'pi1':
            case 'pi2':
            case 'pi3':
            case 'pi4':
            case 'pi5':
            case 'pi6':
            case 'pa2': buf = 'Un pavé droit contient 6 faces rectangulaires !'
              break
          }
          break
        case 'une pyramide':
          switch (stor.ppos[stor.lazrep]) {
            case 'pa1':
            case 'pa2':
            case 'pa3': buf = 'Je ne vois pas de face rectangulaire !'
              break
            case 'cu1':
            case 'cu2':
            case 'cu3':
              buf = 'Je ne vois pas de face carrée !'
              break
            case 'pi1':
            case 'pi4':
              buf = 'Regarde bien la base !'
              break
            case 'pi2':
              if (stor.basequa) {
                buf = 'Compte bien le nombre de faces'
              } else {
                buf = 'Regarde bien la base !'
              }
              break
            case 'pi5':
              buf = 'Compte bien le nombre de faces !'
              break
            case 'pi3':
              buf = 'Compte bien le nombre de faces !'
              break
            case 'pi6':
              buf = 'Il est impossible de plier ce patron !'
              break
            case 'cy1':
            case 'cy2':
            case 'cy3':
            case 'co1':
            case 'co2':
            case 'co3':
              buf = 'Aucune face n’est circulaire !'
              break
            case 'pr1':
            case 'pr2':
            case 'pr3':
            case 'pr4':
            case 'pr5':
            case 'pr6':
              buf = 'Les faces latèrales sont des triangles !'
              break
          }
          break
        case 'un cylindre de révolution':
          switch (stor.ppos[stor.lazrep]) {
            case 'pa1':
            case 'pa2':
            case 'pa3':
            case 'cu1':
            case 'cu2':
            case 'cu3':
            case 'pr1':
            case 'pr4':
            case 'pr2':
            case 'pr5':
            case 'pr3':
            case 'pr6':
            case 'co1':
            case 'co2':
            case 'co3':
            case 'pi1':
            case 'pi2':
            case 'pi3':
            case 'pi4':
            case 'pi5':
            case 'pi6':
              buf = 'Je ne vois pas les deux bases circulaires !'
              break
            case 'cy2':
              buf = 'Compte bien le nombre de faces !'
              break
            case 'cy3':
              buf = 'La longueur du rectangle latéral doit correspondre à la longueur des bases !'
              break
          }
          break
        case 'un prisme droit':
          switch (stor.ppos[stor.lazrep]) {
            case 'pa1':
            case 'pa2':
            case 'pa3': buf = 'Les faces ne sont pas toutes rectangulaires !'
              break
            case 'cu1':
            case 'cu2':
            case 'cu3':
              buf = 'Les faces ne sont pas toutes carrées !'
              break
            case 'pr1':
            case 'pr4':
              buf = 'Regarde bien les bases !'
              break
            case 'pr2':
              if (stor.basequa) {
                buf = 'Compte bien le nombre de faces latérales'
              } else {
                buf = 'Regarde bien les bases !'
              }
              break
            case 'pr5':
              if (!stor.basequa) {
                buf = 'Compte bien le nombre de faces latérales'
              } else {
                buf = 'Regarde bien les bases !'
              }
              break
            case 'pr3':
              buf = 'Deux arêtes qui se superposent doivent avoir la même longueur !'
              break
            case 'pr6':
              if (!stor.basequa) {
                buf = 'Deux arêtes qui se superposent doivent avoir la même longueur !'
              } else {
                buf = 'Regarde bien les bases !'
              }
              break
            case 'cy1':
            case 'cy2':
            case 'cy3':
            case 'co1':
            case 'co2':
            case 'co3':
              buf = 'Aucune face n’est circulaire !'
              break
            case 'pi1':
            case 'pi2':
            case 'pi3':
            case 'pi4':
            case 'pi5':
            case 'pi6':
              buf = 'Les faces latèrales sont des rectangles !'
              break
          }
          break
        case 'un cône de révolution':
          switch (stor.ppos[stor.lazrep]) {
            case 'pa1':
            case 'pa2':
            case 'pa3':
            case 'cu1':
            case 'cu2':
            case 'cu3':
            case 'pr1':
            case 'pr4':
            case 'pr2':
            case 'pr5':
            case 'pr3':
            case 'pr6':
            case 'pi1':
            case 'pi2':
            case 'pi3':
            case 'pi4':
            case 'pi5':
            case 'pi6':
              buf = 'Je ne vois pas la base circulaire !'
              break
            case 'co2':
              buf = 'L’angle de la face latérale semble bien trop petit !'
              break
            case 'co3':
              buf = 'La face latérale est mal positionnée !'
              break
            case 'cy2':
              buf = 'La face latérale ne doit pas être rectangulaire !'
              break
            case 'cy1':
            case 'cy3':
              buf = 'Il n’y a qu’une seule base circulaire dans un cône !'
              break
          }
          break
      }
      yaexplik = true
      j3pAffiche(stor.lesdiv.explications, null, buf)
    }

    if (bool) {
      if (stor.lerrMank) {
        yaexplik = true
        j3pAffiche(stor.lesdiv.explications, null, '<i>(ils sont indiqués en bleu)</i>')
      }
      if (stor.encours === 'f') {
        for (let i = 0; i < stor.FaceSel.length; i++) {
          if (stor.FaceSel[i].select) {
            colorFace(stor.FaceSel[i].id, 214, 71, 0)
            stor.FaceSel[i].fautVoir = true
            stor.FaceSel[i].coolMo = [214, 71, 0]
            colorFace(stor.FaceSel[i].moov, 214, 71, 0)
          }
          if (i === stor.faceb - 1) {
            colorFace(stor.FaceSel[i].id, 127, 0, 255)
            stor.FaceSel[i].fautVoir = true
            stor.FaceSel[i].coolMo = [127, 0, 255]
            colorFace(stor.FaceSel[i].moov, 127, 0, 255)
          }
        }
      }
      if (stor.encours === 'a') {
        if (!stor.exo.col) {
          yaco = true
          switch (stor.exo.solide) {
            case 'un cube':
              j3pAffiche(stor.lesdiv.solution, null, 'Dans un cube, toutes les arêtes \nont la même longueur.')
              break
            case 'un pavé droit':
              j3pAffiche(stor.lesdiv.solution, null, 'Dans un pavé droit, toutes les arêtes parallèles\nont la même longueur.')
              break
            case 'un prisme droit':
              j3pAffiche(stor.lesdiv.solution, null, 'Dans un prisme droit, \nles arêtes latérales ont la même longueur, \nles bases ont des côtés égaux deux à deux \nde même longueur.')
              break
            case 'un cylindre de révolution':
              j3pAffiche(stor.lesdiv.solution, null, 'Dans un cylindre de révolution, \nla longueur de la face latèrale rectangulaire \nest égale au périmètre d’une des deux bases.')
              break
            case 'un cône de révolution':
              j3pAffiche(stor.lesdiv.solution, null, 'Dans un cône de révolution, \nle périmètre de la base est \négal à la longueur de l’arc de cercle.')
              break
          }
          for (let i = 0; i < stor.FaceSel.length; i++) {
            if (stor.FaceSel[i].groupe === stor.legroupel) {
              if (!stor.FaceSel[i].disabled && !stor.FaceSel[i].select) {
                colorFace(stor.FaceSel[i].id, 100, 100, 200)
                stor.FaceSel[i].fautVoir = true
                stor.FaceSel[i].coolMo = [100, 100, 200]
              }
            }
          }
        } else {
          for (let i = 0; i < stor.FaceSel.length; i++) {
            if (stor.FaceSel[i].groupe === stor.legroupel && stor.FaceSel[i].areteN === stor.lareteN) {
              if (!stor.FaceSel[i].disabled && !stor.FaceSel[i].select) {
                colorFace(stor.FaceSel[i].id, 100, 100, 200)
                stor.FaceSel[i].fautVoir = true
                stor.FaceSel[i].coolMo = [100, 100, 200]
              }
            }
          }
        }
        // FaceCache3(true)
      }
      barreLesfaux()
      desactivezone()
    }
    if (yaexplik) {
      stor.lesdiv.explications.classList.add('explique')
    }
    if (yaco) {
      stor.lesdiv.solution.classList.add('correction')
    }
  }

  function enonceMain () {
    stor.raz = true
    if (((me.questionCourante % ds.nbetapes) === 1) || (ds.nbetapes === 1)) {
      me.videLesZones()
      creeLesDiv()
      stor.exo = faisChoixExos()
      stor.lesdiv.travail.classList.add('travail')
      stor.lesdiv.consigne.classList.add('enonce')
      stor.lesdiv.solution.classList.remove('correction')
      stor.lesdiv.explications.classList.remove('explique')
      me.zonesElts.MG.classList.add('fond')
      stor.lesdiv.consigneF.classList.add('enonce')
    }
    poseQuestion()
    me.finEnonce()
  }

  switch (this.etat) {
    case 'enonce':
      // code exécuté au lancement de la section
      if (me.debutDeLaSection) {
        initSection()
      } else {
        stor.raz = true
        enonceMain()
      }
      break // case "enonce":

    case 'correction':
      // On teste si une réponse a été saisie
      if (stor.lesdiv === undefined) return
      stor.lesdiv.explications.innerHTML = ''
      stor.lesdiv.correction.innerHTML = ''

      if (!yaReponse()) {
        // pas de réponse
        stor.lesdiv.correction.style.color = this.styles.cfaux
        stor.lesdiv.correction.innerHTML = reponseIncomplete
        return this.finCorrection()
      }

      if (isRepOk()) {
        // Bonne réponse
        stor.lesdiv.correction.style.color = this.styles.cbien
        stor.lesdiv.correction.innerHTML = cBien
        this.score++
        desactivezone()
        passetoutvert()
        if (stor.encours === 'c') {
          stor.raz = false
          stor.bufID = stor.lid
          lanceanimePatron()
        } else if (stor.encours === 'f' || stor.encours === 'a') {
          stor.raz = false
          stor.bufID = svgPat
          setVisibleFaut()
          lanceanimePatron()
        } else if (stor.encours === 'l') {
          // a faire
        }
        stor.encours = stor.etapes[stor.etapes.indexOf(stor.encours) + 1]

        this.typederreurs[0]++
        return this.finCorrection('navigation', true)
      }

      // Pas de bonne réponse
      stor.lesdiv.correction.style.color = this.styles.cfaux
      stor.lesdiv.correction.innerHTML = cFaux
      if (this.isElapsed) {
        // A cause de la limite de temps :
        stor.lesdiv.correction.innerHTML += tempsDepasse + '<BR>'
        this.typederreurs[10]++
        afficheCorrection(true)
        return this.finCorrection('navigation', true)
      }

      // Réponse fausse sans limite de temps
      if (me.essaiCourant < ds.nbchances) {
        // il reste des essais
        stor.lesdiv.correction.innerHTML += '<br> Essaie encore.'
        afficheCorrection(false)
        this.typederreurs[1]++
        return this.finCorrection()
      }

      // Erreur au dernier essai
      afficheCorrection(true)

      if (stor.encours === 'c') {
        stor.raz = false
        stor.bufID = stor.lid
        lanceanimePatron()
      } else if (stor.encours === 'f' || stor.encours === 'a') {
        stor.raz = false
        stor.bufID = svgPat
        setVisibleFaut()
        lanceanimePatron()
      } else if (stor.encours === 'l') {
        // @todo finir ce code
        me.notif(Error('étape l choisie mais non implémentée'))
      }
      stor.encours = stor.etapes[stor.etapes.indexOf(stor.encours) + 1]

      this.typederreurs[2]++
      this.finCorrection('navigation', true)
      break // case "correction":

    case 'navigation':
      if (!this.sectionTerminee()) {
        this.etat = 'enonce'
      }
      this.finNavigation()
      break // case "navigation":
  }
}
