import { j3pDiv } from 'src/legacy/core/functions'
import { capitalize } from 'src/lib/utils/string'

import './dessin.scss'

/**
 * Ajoute les boutons dans container
 * @param {HTMLElement} container
 * @param {Object} store On va lui ajouter les propriétés btnXxx avec chaque bouton xxx, il doit avoir les listeners onClickXxx (sinon ça râle)
 * @param {string[]} buttons la liste des boutons à afficher (leur nom, idem png, en minuscule)
 */
export function addButtons (container, store, buttons) {
  buttons.forEach((button, i) => {
    const x = 13 + 30 * i
    const suffix = capitalize(button)
    const btn = j3pDiv(container, {
      contenu: ' ',
      coord: [x, 13]
    })
    btn.classList.add('btn' + suffix)
    if (typeof store['onClick' + suffix] === 'function') btn.addEventListener('mousedown', store['onClick' + suffix], false)
    else console.error(Error('Pas de listener onClick' + suffix + ' dans l’objet passé'), store)
    store['btn' + suffix] = btn
  })
}
