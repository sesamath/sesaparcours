import { j3pAddElt, j3pAjouteBouton, j3pArrondi, j3pClone, j3pEmpty, j3pGetNewId, j3pGetRandomBool, j3pGetRandomInt, j3pPGCD, j3pShowError, j3pShuffle } from 'src/legacy/core/functions'
import { j3pCreeFenetres, j3pAfficheCroixFenetres, j3pToggleFenetres } from 'src/legacy/core/functionsJqDialog'
import ListeDeroulante from 'src/legacy/outils/listeDeroulante/ListeDeroulante'
import { affichefois, afficheFrac } from 'src/legacy/outils/zoneStyleMathquill/functions'
import ZoneStyleMathquill1 from 'src/legacy/outils/zoneStyleMathquill/ZoneStyleMathquill1'
import ZoneStyleMathquill2 from 'src/legacy/outils/zoneStyleMathquill/ZoneStyleMathquill2'
import BrouillonCalculs from 'src/legacy/outils/brouillon/BrouillonCalculs'
import { getPe, initCptPe } from 'src/legacy/sections/college/helperTommy'
import { addDefaultTable } from 'src/legacy/themes/table'
import { j3pAffiche } from 'src/lib/mathquill/functions'
import { j3pCreeSVG } from 'src/legacy/core/functionsSvg'
import { getMtgCore } from 'src/lib/outils/mathgraph'
import { addMaj } from 'src/lib/utils/string'
import textesGeneriques from 'src/lib/core/textes'

const { cBien, cFaux, essaieEncore, reponseIncomplete, tempsDepasse } = textesGeneriques

/**
 * Ajouter une description ici
 * @fileOverview
 * @author Tommy Barroy <t.barroy@laposte.net>
 * @since 2017-10
 */
export const params = {
  outils: ['mathquill', 'calculatrice'],
  /**
   * Ce qui est paramétrable dans l’éditeur de graphe
   * @type {LegacySectionParamsParametres}
   */
  parametres: [
    ['nbrepetitions', 9, 'entier', 'Nombre de répétitions de la section'],
    ['nbchances', 2, 'entier', 'Nombre d’essais possibles.'],
    ['Deplacement', true, 'boolean', '<u>true</u>:  Les positions des triangles peuvent compliquer le problème'],
    ['Egalite', true, 'boolean', '<u>true</u>:  L’élève doit écrire l’égalité'],
    ['Reponse', true, 'boolean', '<u>true</u>:  L’élève doit donner la réponse'],
    ['Entiers', true, 'boolean', '<u>true</u>:  Les longueurs sont des nombres entiers'],
    ['Simplifie', true, 'boolean', '<u>true</u>:  Si la réponse est une fraction, elle doit être simplifiée'],
    ['Calculatrice', true, 'boolean', '<u>true</u>:  Une calculatrice est disponible'],
    ['Approximation', false, 'boolean', '<u>true</u>:  Une approximation décimale du résultat est demandée.'],
    // le modèle gère la compatibilité ascendante avec l’ancien paramètre MiseEnPagePlusClaire
    ['theme', 'standard', 'liste', 'Modèle pour l’affichage', ['standard', 'zonesAvecImageDeFond']]

  ],
  // dans le cas d’une section QUANTITATIVE
  pe: [
    { pe_1: 'Egalite' },
    { pe_2: 'Ecriture' },
    { pe_3: 'Produit_en_croix' },
    { pe_4: 'Simplification' }
  ]
}

/**
 * section trianglessemblables04
 * @this {Parcours}
 */
export default function main () {
  const me = this
  const ds = me.donneesSection
  const stor = me.storage

  function creeLesDiv () {
    stor.lesdiv = {}
    stor.lesdiv.conteneur = j3pAddElt(me.zones.MG, 'div', {
      style: me.styles.etendre('toutpetit.enonce', { padding: '10px' })
    })
    let tt = addDefaultTable(stor.lesdiv.conteneur, 1, 3)
    stor.lesdiv.consigneG = tt[0][0]
    tt[0][1].style.width = '30px'
    stor.lesdiv.calculatrice = tt[0][2]
    tt = addDefaultTable(stor.lesdiv.conteneur, 1, 3)
    stor.lesdiv.figure = tt[0][0]
    const huj = addDefaultTable(stor.lesdiv.figure, 2, 1)
    stor.lesdiv.figure1 = huj[0][0]
    stor.lesdiv.figure2 = huj[1][0]
    tt[0][1].style.width = '5px'
    stor.lesdiv.travail = tt[0][2]

    tt = addDefaultTable(stor.lesdiv.conteneur, 1, 3)
    stor.lesdiv.correction = tt[0][0]
    tt[0][1].style.width = '20px'
    const tt2 = addDefaultTable(tt[0][2], 2, 1)
    stor.lesdiv.solution = tt2[1][0]
    stor.lesdiv.explications = tt2[0][0]
    stor.lesdiv.solution.style.color = me.styles.toutpetit.correction.color
    stor.lesdiv.explications.style.color = me.styles.cfaux
  }
  function poseQuestion () {
    stor.niv = 0
    if (ds.Egalite && !ds.Reponse) { j3pAffiche(stor.lesdiv.consigneG, null, 'Les triangles ' + stor.nomtri + ' et ' + stor.nomtri2 + ' sont semblables, \n<b> On veut écrire l’égalité des rapports </b> . \n') } else {
      j3pAffiche(stor.lesdiv.consigneG, null, 'Les triangles ' + stor.nomtri + ' et ' + stor.nomtri2 + ' sont semblables, \n<b> On veut calculer la longueur  $' + stor.lch + '$ </b> \n')
    }
    stor.lespar = []
    stor.tabCont = addDefaultTable(stor.lesdiv.travail, 5, 1)
    rempliDescription()
  }
  function rempliDescription () {
    j3pAffiche(stor.tabCont[0][0], null, 'Les triangles ' + stor.nomtri + ' et ' + stor.nomtri2 + ' sont semblables, \n')
    poseEgalite()
  }
  function poseEgalite () {
    stor.encours = 'egalite'
    if (!ds.Egalite) {
      rempliEgalite()
      return
    }
    const tt = addDefaultTable(stor.tabCont[1][0], 1, 6)
    j3pAffiche(tt[0][0], null, 'donc &nbsp;')
    j3pAffiche(tt[0][2], null, ' $ = $ ')
    j3pAffiche(tt[0][4], null, ' $ = $ ')
    const f1 = afficheFrac(tt[0][1])
    const f2 = afficheFrac(tt[0][3])
    const f3 = afficheFrac(tt[0][5])
    let ll = [stor.lettres[0] + stor.lettres[1], stor.lettres[1] + stor.lettres[2], stor.lettres[2] + stor.lettres[0], stor.lettres[3] + stor.lettres[4], stor.lettres[4] + stor.lettres[5], stor.lettres[5] + stor.lettres[3]]
    ll = j3pShuffle(ll)
    ll.splice(0, 0, '--')
    ll = modif(ll)
    stor.listeE1 = ListeDeroulante.create(f1[0], ll, { centre: true })
    stor.listeE2 = ListeDeroulante.create(f1[2], ll, { centre: true })
    stor.listeE3 = ListeDeroulante.create(f2[0], ll, { centre: true })
    stor.listeE4 = ListeDeroulante.create(f2[2], ll, { centre: true })
    stor.listeE5 = ListeDeroulante.create(f3[0], ll, { centre: true })
    stor.listeE6 = ListeDeroulante.create(f3[2], ll, { centre: true })
  }
  function modif (tab) {
    const hh = j3pClone(tab)
    for (let i = 1; i < hh.length; i++) {
      hh[i] = '$' + hh[i] + '$'
    }
    return hh
  }
  function rempliEgalite () {
    j3pAffiche(stor.tabCont[1][0], null, 'donc $\\frac{' + stor.nomlc1 + '}{' + stor.nomlc4 + '} = \\frac{' + stor.nomlc2 + '}{' + stor.nomlc5 + '} = \\frac{' + stor.nomlc3 + '}{' + stor.nomlc6 + '}$')
    stor.listeE1 = { reponse: stor.nomlc1 }
    stor.listeE2 = { reponse: stor.nomlc4 }
    stor.listeE3 = { reponse: stor.nomlc2 }
    stor.listeE4 = { reponse: stor.nomlc5 }
    stor.listeE5 = { reponse: stor.nomlc3 }
    stor.listeE6 = { reponse: stor.nomlc6 }
    poseRempli()
  }
  function poseRempli () {
    stor.encours = 'rempli'
    stor.lespar = []
    const tt = addDefaultTable(stor.tabCont[2][0], 1, 6)
    j3pAffiche(tt[0][0], null, 'donc &nbsp;')
    j3pAffiche(tt[0][2], null, ' $ = $ ')
    j3pAffiche(tt[0][4], null, ' $ = $ ')
    const f1 = afficheFrac(tt[0][1])
    const f2 = afficheFrac(tt[0][3])
    const f3 = afficheFrac(tt[0][5])
    if (stor.nominc.indexOf(stor.listeE1.reponse.replace(/\$/g, '')) === -1) {
      stor.z1 = new ZoneStyleMathquill1(f1[0], { restric: '0123456789.,', enter: me.sectionCourante.bind(me) })
    } else {
      j3pAffiche(f1[0], null, '$' + stor.listeE1.reponse.replace(/\$/g, '') + '$'); stor.z1 = { ffalse: true }
    }
    if (stor.nominc.indexOf(stor.listeE2.reponse.replace(/\$/g, '')) === -1) {
      stor.z2 = new ZoneStyleMathquill1(f1[2], { restric: '0123456789.,', enter: me.sectionCourante.bind(me) })
    } else {
      j3pAffiche(f1[2], null, '$' + stor.listeE2.reponse.replace(/\$/g, '') + '$'); stor.z2 = { ffalse: true }
    }
    if (stor.nominc.indexOf(stor.listeE3.reponse.replace(/\$/g, '')) === -1) {
      stor.z3 = new ZoneStyleMathquill1(f2[0], { restric: '0123456789.,', enter: me.sectionCourante.bind(me) })
    } else {
      j3pAffiche(f2[0], null, '$' + stor.listeE3.reponse.replace(/\$/g, '') + '$'); stor.z3 = { ffalse: true }
    }
    if (stor.nominc.indexOf(stor.listeE4.reponse.replace(/\$/g, '')) === -1) {
      stor.z4 = new ZoneStyleMathquill1(f2[2], { restric: '0123456789.,', enter: me.sectionCourante.bind(me) })
    } else {
      j3pAffiche(f2[2], null, '$' + stor.listeE4.reponse.replace(/\$/g, '') + '$'); stor.z4 = { ffalse: true }
    }
    if (stor.nominc.indexOf(stor.listeE5.reponse.replace(/\$/g, '')) === -1) {
      stor.z5 = new ZoneStyleMathquill1(f3[0], { restric: '0123456789.,', enter: me.sectionCourante.bind(me) })
    } else {
      j3pAffiche(f3[0], null, '$' + stor.listeE5.reponse.replace(/\$/g, '') + '$'); stor.z5 = { ffalse: true }
    }
    if (stor.nominc.indexOf(stor.listeE6.reponse.replace(/\$/g, '')) === -1) {
      stor.z6 = new ZoneStyleMathquill1(f3[2], { restric: '0123456789.,', enter: me.sectionCourante.bind(me) })
    } else {
      j3pAffiche(f3[2], null, '$' + stor.listeE6.reponse.replace(/\$/g, '') + '$'); stor.z6 = { ffalse: true }
    }
  }
  function poseProduit () {
    stor.encours = 'prod'
    stor.tabProd = addDefaultTable(stor.tabCont[3][0], 3, 4)
    stor.lespar = []
    j3pAffiche(stor.tabProd[0][0], null, ' donc $ ' + stor.lch + ' $')
    j3pAffiche(stor.tabProd[0][1], null, '$ = $')
    const t = afficheFrac(stor.tabProd[0][2])
    const t2 = affichefois(t[0])
    stor.zP1 = new ZoneStyleMathquill1(t2[0], { restric: '0123456789.,', enter: me.sectionCourante.bind(me) })
    stor.zP2 = new ZoneStyleMathquill1(t2[2], { restric: '0123456789.,', enter: me.sectionCourante.bind(me) })
    stor.zP3 = new ZoneStyleMathquill1(t[2], { restric: '0123456789.,', enter: me.sectionCourante.bind(me) })
  }
  function poseFin () {
    stor.encours = 'concl'
    stor.Yaapprox = false
    let bobi = '/'
    stor.brouillon1 = new BrouillonCalculs(
      {
        caseBoutons: stor.tabProd[1][0],
        caseEgal: stor.tabProd[1][1],
        caseCalculs: stor.tabProd[1][2],
        caseApres: stor.tabProd[1][3]
      },
      {
        limite: 30,
        limitenb: 20,
        restric: '0123456789*/,.',
        hasAutoKeyboard: false,
        lmax: 2
      }
    )
    j3pAffiche(stor.tabProd[2][1], null, '$ = $')
    const tt = addDefaultTable(stor.tabProd[2][2], 1, 2)
    j3pAffiche(tt[0][1], null, '&nbsp; cm')
    if (ds.Approximation) {
      const n1 = parseFloat(stor.atthaut[0].replace(',', '.')) * 10 * parseFloat(stor.atthaut[1].replace(',', '.')) * 10 / (parseFloat(stor.attbas.replace(',', '.')) * 100)
      const n2 = j3pArrondi(n1, 2)
      const n3 = j3pArrondi(n1, 10)
      if (n2 !== n3) {
        j3pEmpty(stor.tabProd[2][1])
        j3pAffiche(stor.tabProd[2][1], null, '$ \\approx $')
        j3pAffiche(stor.lesdiv.travail, null, '<i>Approximation décimale à $0,1$ cm près.</i>')
        bobi = ''
        stor.Yaapprox = true
      }
    } else if (ds.Simplifie) {
      j3pAffiche(stor.lesdiv.travail, null, '<i>La réponse doit être simplifiée.</i>')
    }
    stor.zFin = new ZoneStyleMathquill2(tt[0][0], { id: 'dzFin', restric: '0123456789.,' + bobi, enter: me.sectionCourante.bind(me) })
  }
  function boutonfen () {
    j3pToggleFenetres('Calculatrice')
  }
  function faisChoixExos () {
    return stor.lesExos.pop()
  }
  function faiFigure () {
    const objet = {}
    let i
    const fig = 'TWF0aEdyYXBoSmF2YTEuMAAAABM+TMzNAAJmcv###wEA#wEAAAAAAAAAAAOIAAACjQAAAQEAAAAAAAAAAQAAAST#####AAAAAQAKQ0NhbGNDb25zdAD#####AAJwaQAWMy4xNDE1OTI2NTM1ODk3OTMyMzg0Nv####8AAAABAApDQ29uc3RhbnRlQAkh+1RELRj#####AAAAAQAKQ1BvaW50QmFzZQD#####AAAAAAAQAAAAQAgAAAAAAAAAAAAAAAAAAAAACAAAQEnAAAAAAABAfGo9cKPXCv####8AAAABAAdDQ2FsY3VsAP####8ABW1pbmkxAAEwAAAAAQAAAAAAAAAAAAAAAwD#####AAVtYXhpMQADMzU5AAAAAUB2cAAAAAAA#####wAAAAEAFENJbXBsZW1lbnRhdGlvblByb3RvAP####8AB0N1cnNldXIAAAAFAAAABQAAAAMAAAACAAAAAwAAAAH#####AAAAAQAUQ0Ryb2l0ZURpcmVjdGlvbkZpeGUAAAAABAEAAAAAEAAAAQAAAAEAAAABAT#wAAAAAAAA#####wAAAAEAD0NQb2ludExpZURyb2l0ZQEAAAAEAAAAAAAQAAAAwAgAAAAAAAA#8AAAAAAAAAAABQAAQF1AAAAAAAAAAAAF#####wAAAAEAC0NIb21vdGhldGllAAAAAAQAAAAB#####wAAAAEACkNPcGVyYXRpb24D#####wAAAAEAD0NSZXN1bHRhdFZhbGV1cgAAAAIAAAAIAQAAAAkAAAACAAAACQAAAAP#####AAAAAQALQ1BvaW50SW1hZ2UAAAAABAEAAAAADQACTzEAwBAAAAAAAABAEAAAAAAAAAAABQAAAAAGAAAABwAAAAcAAAAABAAAAAEAAAAIAwAAAAgBAAAAAT#wAAAAAAAAAAAACQAAAAIAAAAIAQAAAAkAAAADAAAACQAAAAIAAAAKAAAAAAQBAAAAAA0AAkkxAMAAAAAAAAAAQAgAAAAAAAAAAAUAAAAABgAAAAn#####AAAAAQAIQ1NlZ21lbnQBAAAABAAAAAAAEAAAAQAAAAIAAAABAAAABgAAAAYBAAAABAAAAAABEAACazEAwAAAAAAAAABAAAAAAAAAAAAAAQABP9tZtZtZtZsAAAAL#####wAAAAIAD0NNZXN1cmVBYnNjaXNzZQEAAAAEAAJhMQAAAAgAAAAKAAAADP####8AAAABAA9DVmFsZXVyQWZmaWNoZWUBAAAABAAAAAAAAAAAAAAAAADAGAAAAAAAAAAAAAAADA8AAf###wAAAAEAAAACAAAAAQAAAAAAAAAAAAAAAAIAAAANAAAAAwD#####AAJBMQACYTEAAAAJAAAADQAAAAIA#####wAAAAAAEAAAAEAIAAAAAAAAAAAAAAAAAAAAAAgAAEBr8AAAAAAAQHwKPXCj1woAAAADAP####8ABW1pbmkyAAEwAAAAAQAAAAAAAAAAAAAAAwD#####AAVtYXhpMgADMzU5AAAAAUB2cAAAAAAAAAAABAD#####AAdDdXJzZXVyAAAABQAAAAUAAAADAAAAEQAAABIAAAAQAAAABQAAAAATAQAAAAAQAAABAAAAAQAAABABP#AAAAAAAAAAAAAGAQAAABMAAAAAABAAAADACAAAAAAAAD#wAAAAAAAAAAAFAABAXUAAAAAAAAAAABQAAAAHAAAAABMAAAAQAAAACAMAAAAJAAAAEQAAAAgBAAAACQAAABEAAAAJAAAAEgAAAAoAAAAAEwEAAAAADQACTzEAwBAAAAAAAABAEAAAAAAAAAAABQAAAAAVAAAAFgAAAAcAAAAAEwAAABAAAAAIAwAAAAgBAAAAAT#wAAAAAAAAAAAACQAAABEAAAAIAQAAAAkAAAASAAAACQAAABEAAAAKAAAAABMBAAAAAA0AAkkxAMAAAAAAAAAAQAgAAAAAAAAAAAUAAAAAFQAAABgAAAALAQAAABMAAAAAABAAAAEAAAACAAAAEAAAABUAAAAGAQAAABMAAAAAARAAAWsAwAAAAAAAAABAAAAAAAAAAAAAAQABP+SDSDSDSDUAAAAaAAAADAEAAAATAAJhMgAAABcAAAAZAAAAGwAAAA0BAAAAEwAAAAAAAAAAAAAAAADAGAAAAAAAAAAAAAAAGw8AAf###wAAAAEAAAACAAAAAQAAAAAAAAAAAAAAAAIAAAAcAAAAAwD#####AAJBMgACYTIAAAAJAAAAHAAAAAIA#####wEAAAAAEAAAAAAAAAAAAAAAQAgAAAAAAAAAAAgAAUBhEAAAAAAAQGJ0euFHrhQAAAACAP####8BAAAAABAAAAAAAAAAAAAAAEAIAAAAAAAAAAAIAAFAa5AAAAAAAEBdaPXCj1wp#####wAAAAEACUNDZXJjbGVPQQD#####AQAAAAAAAAIAAAAfAAAAIP####8AAAABAA9DUG9pbnRMaWVDZXJjbGUA#####wEAAAAAEAAAAAAAAAAAAAAAQAgAAAAAAAAAAAgAAUACLaEQoEDQAAAAIQAAAA8A#####wF#f38AEAAAAAAAAAAAAAAAQAgAAAAAAAAAAAcAAUAMRLC2oUJeAAAAIf####8AAAABAAlDUm90YXRpb24A#####wAAAB8AAAAJAAAADwAAAAoA#####wH#AAAAEAAAAAAAAAAAAAAAQAgAAAAAAAAAAAgAAAAAIAAAACQAAAAKAP####8Bf39#ABAAAAAAAAAAAAAAAEAIAAAAAAAAAAAHAAAAACIAAAAkAAAACgD#####AX9#fwAQAAAAAAAAAAAAAABACAAAAAAAAAAABwAAAAAjAAAAJAAAAAUA#####wEAAAABEAAAAQAAAAIAAAAfAT#wAAAAAAAAAAAABgD#####AQAAAAAQAAAAAAAAAAAAAABACAAAAAAAAAAACAABQHCAAAAAAAAAAAAo#####wAAAAEACENWZWN0ZXVyAP####8BAAAAABAAAAEAAAACAAAAHwAAACkA#####wAAAAEADUNUcmFuc1BhclZlY3QA#####wAAACoAAAAKAP####8BAAAAABAAAAAAAAAAAAAAAEAIAAAAAAAAAAAIAAAAACAAAAArAAAACgD#####AQAAAAAQAAAAAAAAAAAAAABACAAAAAAAAAAACAAAAAAiAAAAKwAAAAoA#####wEAAAAAEAAAAAAAAAAAAAAAQAgAAAAAAAAAAAgAAAAAIwAAACsAAAAQAP####8AAAApAAAACQAAAB4AAAAKAP####8B#wAAABAAAAAAAAAAAAAAAEAIAAAAAAAAAAAIAAAAACwAAAAvAAAACgD#####Af8AAAAQAAAAAAAAAAAAAABACAAAAAAAAAAACAAAAAAtAAAALwAAAAoA#####wH#AAAAEAAAAAAAAAAAAAAAQAgAAAAAAAAAAAgAAAAALgAAAC8AAAALAP####8AAAAAABAAAAEAA3MxMgACAAAAMgAAADAAAAALAP####8AAAAAABAAAAEAA3MyMgACAAAAMAAAADEAAAALAP####8AAAAAABAAAAEAA3MzMgACAAAAMQAAADL#####AAAAAQAWQ0Ryb2l0ZVBlcnBlbmRpY3VsYWlyZQD#####AQAAAAAQAAABAAAAAgAAACkAAAAo#####wAAAAEAD0NTeW1ldHJpZUF4aWFsZQD#####AAAANgAAAAoA#####wEAAP8AEAAAAAAAAAAAAAAAQAgAAAAAAAAAAAgAAAAAMQAAADcAAAAKAP####8BAAD#ABAAAAAAAAAAAAAAAEAIAAAAAAAAAAAIAAAAADIAAAA3AAAACgD#####AQAA#wAQAAAAAAAAAAAAAABACAAAAAAAAAAACAAAAAAwAAAANwAAAAsA#####wAAAAAAEAAAAQADczExAAIAAAA6AAAAOQAAAAsA#####wAAAAAAEAAAAQADczIxAAIAAAA4AAAAOQAAAAsA#####wAAAAAAEAAAAQADczMxAAIAAAA4AAAAOgAAAAIA#####wAAAAAAEAABQQAAAAAAAAAAAEAIAAAAAAAAAAAIAAFAekgAAAAAAEB9Wj1wo9cKAAAAAgD#####AAAAAAAQAAFCAAAAAAAAAAAAQAgAAAAAAAAAAAgAAUB7SAAAAAAAQH1qPXCj1wr#####AAAAAQAJQ0xvbmd1ZXVyAP####8AAAA+AAAAP#####8AAAACAAlDQ2VyY2xlT1IA#####wF#f38AAAABAAAAJgAAAAE#8AAAAAAAAAAAAAAWAP####8BAAAAAAAAAgAAACUAAAABP#AAAAAAAAAAAAAAFgD#####AQAAAAAAAAIAAAAnAAAAAT#wAAAAAAAAAAAAABYA#####wEAAAAAAAACAAAAMAAAAAE#8AAAAAAAAAAAAAAWAP####8BAAAAAAAAAgAAADoAAAABP#AAAAAAAAAAAAAAFgD#####AQAAAAAAAAIAAAAxAAAAAT#wAAAAAAAAAAAAABYA#####wEAAAAAAAACAAAAOAAAAAE#8AAAAAAAAAAAAAAWAP####8BAAAAAAAAAgAAADkAAAABP#AAAAAAAAAAAAAAFgD#####AQAAAAAAAAIAAAAyAAAAAT#wAAAAAAAAAP####8AAAABAA1DRGVtaURyb2l0ZU9BAP####8Bf39#AA0AAAEAAAABAAAAHwAAACUAAAAXAP####8Bf39#AA0AAAEAAAABAAAAHwAAACcAAAAXAP####8Bf39#AA0AAAEAAAABAAAAHwAAACYAAAAXAP####8Bf39#AA0AAAEAAAABAAAAKQAAADAAAAAXAP####8Bf39#AA0AAAEAAAABAAAAKQAAADoAAAAXAP####8Bf39#AA0AAAEAAAABAAAAKQAAADEAAAAXAP####8Bf39#AA0AAAEAAAABAAAAKQAAADIAAAAXAP####8Bf39#AA0AAAEAAAABAAAAKQAAADkAAAAXAP####8Bf39#AA0AAAEAAAABAAAAKQAAADj#####AAAAAQAQQ0ludERyb2l0ZUNlcmNsZQD#####AAAASgAAAEL#####AAAAAQAQQ1BvaW50TGllQmlwb2ludAD#####AQB#AAAQAAAAAAAAAAAAAABACAAAAAAAAAAACAACAAAAUwAAABkA#####wEAfwAAEAAAAAAAAAAAAAAAQAgAAAAAAAAAAAgAAQAAAFMAAAAYAP####8AAABMAAAAQQAAABkA#####wEAfwAAEAAAAAAAAAAAAAAAQAgAAAAAAAAAAAgAAgAAAFYAAAAZAP####8Bf39#ABAAAAAAAAAAAAAAAEAIAAAAAAAAAAAHAAEAAABWAAAAGAD#####AAAASwAAAEMAAAAZAP####8BAH8AABAAAAAAAAAAAAAAAEAIAAAAAAAAAAAIAAIAAABZAAAAGQD#####AX9#fwAQAAAAAAAAAAAAAABACAAAAAAAAAAABwABAAAAWQAAABgA#####wAAAFIAAABHAAAAGQD#####AQB#AAAQAAAAAAAAAAAAAABACAAAAAAAAAAACAACAAAAXAAAABkA#####wEAfwAAEAAAAAAAAAAAAAAAQAgAAAAAAAAAAAgAAQAAAFwAAAAYAP####8AAABRAAAASAAAABkA#####wEAfwAAEAAAAAAAAAAAAAAAQAgAAAAAAAAAAAgAAgAAAF8AAAAZAP####8BAH8AABAAAAAAAAAAAAAAAEAIAAAAAAAAAAAIAAEAAABfAAAAGAD#####AAAAUAAAAEkAAAAZAP####8BAH8AABAAAAAAAAAAAAAAAEAIAAAAAAAAAAAIAAIAAABiAAAAGQD#####AQB#AAAQAAAAAAAAAAAAAABACAAAAAAAAAAACAABAAAAYgAAABgA#####wAAAE8AAABGAAAAGQD#####AQB#AAAQAAAAAAAAAAAAAABACAAAAAAAAAAACAACAAAAZQAAABkA#####wEAfwAAEAAAAAAAAAAAAAAAQAgAAAAAAAAAAAgAAQAAAGUAAAAYAP####8AAABOAAAARQAAABkA#####wEAfwAAEAAAAAAAAAAAAAAAQAgAAAAAAAAAAAgAAgAAAGgAAAAZAP####8BAH8AABAAAAAAAAAAAAAAAEAIAAAAAAAAAAAIAAEAAABoAAAAGAD#####AAAATQAAAEQAAAAZAP####8BAH8AABAAAAAAAAAAAAAAAEAIAAAAAAAAAAAIAAIAAABrAAAAGQD#####AQB#AAAQAAAAAAAAAAAAAABACAAAAAAAAAAACAABAAAAa#####8AAAACAAxDQ29tbWVudGFpcmUA#####wAAfwABAANuQzEAAABhEAAAAAAAAQAAAAEAAAABAAAAAAAAAAAAAkMxAAAAGgD#####AAB#AAEAA25CMQAAAF4QAAAAAAABAAAAAQAAAAEAAAAAAAAAAAACQjEAAAAaAP####8AAH8AAQADbkExAAAAahAAAAAAAAEAAAABAAAAAQAAAAAAAAAAAAJBMQAAABoA#####wAAfwABAANuQjIAAABnEAAAAAAAAQAAAAEAAAABAAAAAAAAAAAAAkIyAAAAGgD#####AAB#AAEAA25DMgAAAGQQAAAAAAABAAAAAQAAAAEAAAAAAAAAAAACQzIAAAAaAP####8AAH8AAQADbkEyAAAAbRAAAAAAAAEAAAABAAAAAQAAAAAAAAAAAAJBMgAAABgA#####wAAADQAAABGAAAAGQD#####AAB#AAAQAAAAAAAAAAAAAABACAAAAAAAAAAACAABAAAAdAAAABkA#####wEAfwAAEAAAAAAAAAAAAAAAQAgAAAAAAAAAAAgAAgAAAHQAAAAYAP####8AAAAzAAAARAAAABkA#####wAAfwAAEAAAAAAAAAAAAAAAQAgAAAAAAAAAAAgAAQAAAHcAAAAZAP####8BAH8AABAAAAAAAAAAAAAAAEAIAAAAAAAAAAAIAAIAAAB3AAAAGAD#####AAAANQAAAEkAAAAZAP####8AAH8AABAAAAAAAAAAAAAAAEAIAAAAAAAAAAAIAAEAAAB6AAAAGQD#####AQB#AAAQAAAAAAAAAAAAAABACAAAAAAAAAAACAACAAAAegAAABgA#####wAAADsAAABIAAAAGQD#####AAB#AAAQAAAAAAAAAAAAAABACAAAAAAAAAAACAABAAAAfQAAABkA#####wEAfwAAEAAAAAAAAAAAAAAAQAgAAAAAAAAAAAgAAgAAAH0AAAAYAP####8AAAA9AAAARQAAABkA#####wAAfwAAEAAAAAAAAAAAAAAAQAgAAAAAAAAAAAgAAQAAAIAAAAAZAP####8BAH8AABAAAAAAAAAAAAAAAEAIAAAAAAAAAAAIAAIAAACAAAAAGAD#####AAAAPAAAAEcAAAAZAP####8BAH8AABAAAAAAAAAAAAAAAEAIAAAAAAAAAAAIAAEAAACDAAAAGQD#####AAB#AAAQAAAAAAAAAAAAAABACAAAAAAAAAAACAACAAAAg#####8AAAABABRDQXJjRGVDZXJjbGVJbmRpcmVjdAD#####AAB#AAAFbmFyYzEABQAAADkAAAB#AAAAOAAAABsA#####wD#AAAABW5hcmMyAAUAAAA4AAAAhAAAADoAAAAbAP####8AAAD#AAVuYXJjMwAFAAAAOgAAAIIAAAA5AAAAGwD#####AP8AAAAFbmFyYzQABQAAADEAAAB2AAAAMgAAABsA#####wAAfwAABW5hcmM1AAUAAAAyAAAAfAAAADAAAAAbAP####8AAAD#AAVuYXJjNgAFAAAAMAAAAHkAAAAx#####wAAAAEAB0NNaWxpZXUA#####wH#AAAAEAAAAAAAAAAAAAAAQAgAAAAAAAAAAAgAAAAAJwAAACUAAAAcAP####8Bf39#ABAAAAAAAAAAAAAAAEAIAAAAAAAAAAAHAAAAACYAAAAlAAAAHAD#####Af8AAAAQAAAAAAAAAAAAAABACAAAAAAAAAAACAAAAAA5AAAAOAAAABwA#####wH#AAAAEAAAAAAAAAAAAAAAQAgAAAAAAAAAAAgAAAAAOQAAADoAAAAcAP####8B#wAAABAAAAAAAAAAAAAAAEAIAAAAAAAAAAAIAAAAADoAAAA4AAAAHAD#####Af8AAAAQAAAAAAAAAAAAAABACAAAAAAAAAAACAAAAAAyAAAAMQAAABwA#####wH#AAAAEAAAAAAAAAAAAAAAQAgAAAAAAAAAAAgAAAAAMQAAADAAAAAcAP####8B#wAAABAAAAAAAAAAAAAAAEAIAAAAAAAAAAAIAAAAADIAAAAwAAAAEwD#####AX9#fwAQAAABAAAAAQAAAI4AAAA8AAAAEwD#####Af8AAAAQAAABAAAABQAAAJAAAAA9AAAAEwD#####Af8AAAAQAAABAAAABQAAAJIAAAA0AAAAEwD#####AX9#fwAQAAABAAAAAQAAAJEAAAA1AAAAFgD#####AX9#fwAAAAEAAACNAAAAAT#wAAAAAAAAAAAAABYA#####wF#f38AAAABAAAAjAAAAAE#8AAAAAAAAAAAAAAWAP####8B#wAAAAAABQAAAJIAAAABP#AAAAAAAAAAAAAAFgD#####Af8AAAAAAAUAAACRAAAAAT#wAAAAAAAAAAAAABYA#####wH#AAAAAAAFAAAAjgAAAAE#8AAAAAAAAAAAAAAWAP####8B#wAAAAAABQAAAJAAAAABP#AAAAAAAAAAAAAAGAD#####AAAAlAAAAJwAAAAZAP####8B#wD#ABAAAAAAAAAAAAAAAEAIAAAAAAAAAAAIAAEAAACeAAAAGQD#####Af8A#wAQAAAAAAAAAAAAAABACAAAAAAAAAAACAACAAAAngAAABgA#####wAAAJcAAACbAAAAGQD#####Af8A#wAQAAAAAAAAAAAAAABACAAAAAAAAAAACAACAAAAoQAAABkA#####wH#AP8AEAAAAAAAAAAAAAAAQAgAAAAAAAAAAAgAAQAAAKEAAAAYAP####8AAACWAAAAmgAAABkA#####wH#AP8AEAAAAAAAAAAAAAAAQAgAAAAAAAAAAAgAAgAAAKQAAAAZAP####8B#wD#ABAAAAAAAAAAAAAAAEAIAAAAAAAAAAAIAAEAAACkAAAAGAD#####AAAAlQAAAJ0AAAAZAP####8B#wD#ABAAAAAAAAAAAAAAAEAIAAAAAAAAAAAIAAIAAACnAAAAGQD#####Af8A#wAQAAAAAAAAAAAAAABACAAAAAAAAAAACAABAAAApwAAABYA#####wH#AP8AAAAFAAAAjwAAAAE#8AAAAAAAAAAAAAATAP####8Bf39#ABAAAAEAAAABAAAAjwAAADsAAAAYAP####8AAACrAAAAqgAAABkA#####wEAfwAAEAAAAAAAAAAAAAAAQAgAAAAAAAAAAAgAAgAAAKwAAAAZAP####8BAH8AABAAAAAAAAAAAAAAAEAIAAAAAAAAAAAIAAEAAACsAAAAFgD#####Af8A#wAAAAUAAACTAAAAAT#wAAAAAAAAAAAAABMA#####wF#f38AEAAAAQAAAAEAAACTAAAAMwAAABgA#####wAAALAAAACvAAAAGQD#####AQAA#wAQAAAAAAAAAAAAAABACAAAAAAAAAAACAACAAAAsQAAABkA#####wEAAP8AEAAAAAAAAAAAAAAAQAgAAAAAAAAAAAgAAQAAALH#####AAAAAQAMQ0Jpc3NlY3RyaWNlAP####8Bf39#ABAAAAEAAAABAAAAJQAAACYAAAAnAAAAFgD#####AX9#fwAAAAEAAAAmAAAAAUAAAAAAAAAAAAAAABYA#####wF#f38AAAABAAAAJQAAAAFAAAAAAAAAAAAAAAAWAP####8Bf39#AAAAAQAAACcAAAABQAAAAAAAAAAAAAAAHQD#####AX9#fwAQAAABAAAAAQAAACYAAAAnAAAAJQAAAB0A#####wF#f38AEAAAAQAAAAEAAAAnAAAAJQAAACYAAAAYAP####8AAAC5AAAAtgAAABkA#####wF#f38AEAAAAAAAAAAAAAAAQAgAAAAAAAAAAAcAAQAAALoAAAAZAP####8B#wAAABAAAAAAAAAAAAAAAEAIAAAAAAAAAAAIAAIAAAC6AAAAGAD#####AAAAtAAAALUAAAAZAP####8Bf39#ABAAAAAAAAAAAAAAAEAIAAAAAAAAAAAHAAIAAAC9AAAAGQD#####AX9#fwAQAAAAAAAAAAAAAABACAAAAAAAAAAABwABAAAAvQAAABgA#####wAAALgAAAC3AAAAGQD#####Af8AAAAQAAAAAAAAAAAAAABACAAAAAAAAAAACAACAAAAwAAAABkA#####wF#f38AEAAAAAAAAAAAAAAAQAgAAAAAAAAAAAcAAQAAAMAAAAAaAP####8AMzMzAQAEbmwxMgAAALMQAAAAAAABAAAAAQAAAAEAAAAAAAAAAAADbDEyAAAAGgD#####ADMzMwEABG5sMjIAAACjEAAAAAAAAQAAAAEAAAABAAAAAAAAAAAAA2wyMgAAABoA#####wAzMzMBAARubDMyAAAAphAAAAAAAAEAAAABAAAAAQAAAAAAAAAAAANsMzIAAAAaAP####8AMzMzAQAEbmwyMQAAAKAQAAAAAAABAAAAAQAAAAEAAAAAAAAAAAADbDIxAAAAGgD#####ADMzMwEABG5sMzEAAACpEAAAAAAAAQAAAAEAAAABAAAAAAAAAAAAA2wzMQAAABoA#####wAzMzMBAARubDExAAAArhAAAAAAAAEAAAABAAAAAQAAAAAAAAAAAANsMTEAAAAdAP####8BAAD#ABAAAAEAAAAFAAAAOAAAADkAAAA6AAAAHQD#####AX9#fwAQAAABAAAAAQAAADoAAAA4AAAAOQAAAB0A#####wF#f38AEAAAAQAAAAEAAAA5AAAAOgAAADgAAAAWAP####8BAAD#AAAABQAAADkAAAABQAAAAAAAAAAAAAAAFgD#####AX9#fwAAAAEAAAA4AAAAAUAAAAAAAAAAAAAAABYA#####wEAAP8AAAAFAAAAOgAAAAFAAAAAAAAAAAAAAAAYAP####8AAADLAAAAzgAAABkA#####wH#AP8AEAAAAAAAAAAAAAAAQAgAAAAAAAAAAAgAAQAAAM8AAAAZAP####8B#wD#ABAAAAAAAAAAAAAAAEAIAAAAAAAAAAAIAAIAAADPAAAAGAD#####AAAAygAAAM0AAAAZAP####8B#wD#ABAAAAAAAAAAAAAAAEAIAAAAAAAAAAAIAAEAAADSAAAAGQD#####AX9#fwAQAAAAAAAAAAAAAABACAAAAAAAAAAABwACAAAA0gAAABgA#####wAAAMkAAADMAAAAGQD#####Af8A#wAQAAAAAAAAAAAAAABACAAAAAAAAAAACAABAAAA1QAAABkA#####wH#AP8AEAAAAAAAAAAAAAAAQAgAAAAAAAAAAAgAAgAAANUAAAAdAP####8Bf39#ABAAAAEAAAABAAAAMgAAADEAAAAwAAAAHQD#####AQAA#wAQAAABAAAABQAAADAAAAAyAAAAMQAAAB0A#####wF#f38AEAAAAQAAAAEAAAAxAAAAMAAAADIAAAAWAP####8BAAD#AAAABQAAADAAAAABQAAAAAAAAAAAAAAAFgD#####AQAA#wAAAAUAAAAxAAAAAUAAAAAAAAAAAAAAABYA#####wEAAP8AAAAFAAAAMgAAAAFAAAAAAAAAAAAAAAAYAP####8AAADaAAAA2wAAABkA#####wH#AP8AEAAAAAAAAAAAAAAAQAgAAAAAAAAAAAgAAQAAAN4AAAAZAP####8B#wD#ABAAAAAAAAAAAAAAAEAIAAAAAAAAAAAIAAIAAADeAAAAGAD#####AAAA2AAAANwAAAAZAP####8B#wD#ABAAAAAAAAAAAAAAAEAIAAAAAAAAAAAIAAEAAADhAAAAGQD#####Af8A#wAQAAAAAAAAAAAAAABACAAAAAAAAAAACAACAAAA4QAAABgA#####wAAANkAAADdAAAAGQD#####Af8A#wAQAAAAAAAAAAAAAABACAAAAAAAAAAACAABAAAA5AAAABkA#####wH#AP8AEAAAAAAAAAAAAAAAQAgAAAAAAAAAAAgAAgAAAOQAAAADAP####8AAkhvAAMxLjMAAAABP#TMzMzMzM0AAAAHAP####8AAAAfAAAACQAAAOcAAAAKAP####8A#wAAABAAAAAAAAAAAAAAAEAIAAAAAAAAAAAHAAAAACcAAADoAAAACgD#####AP8AAAAQAAAAAAAAAAAAAABACAAAAAAAAAAABwAAAAAmAAAA6AAAAAoA#####wD#AAAAEAAAAAAAAAAAAAAAQAgAAAAAAAAAAAcAAAAAJQAAAOgAAAALAP####8AAAAAABAAAAEAAAACAAAA6wAAAOoAAAALAP####8AAAAAABAAAAEAAAACAAAA6QAAAOoAAAALAP####8AAAAAABAAAAEAAAACAAAA6QAAAOsAAAAWAP####8BAAD#AAAAAgAAAOsAAAABP#AAAAAAAAAAAAAAFgD#####AQAA#wAAAAIAAADpAAAAAT#wAAAAAAAAAAAAABYA#####wEAAP8AAAACAAAA6gAAAAE#8AAAAAAAAAAAAAAYAP####8AAADuAAAA7wAAABkA#####wAAAP8AEAAAAAAAAAAAAAAAQAgAAAAAAAAAAAcAAQAAAPIAAAAZAP####8BAAD#ABAAAAAAAAAAAAAAAEAIAAAAAAAAAAAHAAIAAADyAAAAGAD#####AAAA7QAAAPAAAAAZAP####8BAAD#ABAAAAAAAAAAAAAAAEAIAAAAAAAAAAAHAAEAAAD1AAAAGQD#####AAAA#wAQAAAAAAAAAAAAAABACAAAAAAAAAAABwACAAAA9QAAABgA#####wAAAOwAAADxAAAAGQD#####AAAA#wAQAAAAAAAAAAAAAABACAAAAAAAAAAABwABAAAA+AAAABkA#####wEAAP8AEAAAAAAAAAAAAAAAQAgAAAAAAAAAAAcAAgAAAPgAAAAbAP####8AAH8AAAAABQAAAOkAAAD2AAAA6wAAABsA#####wAAAP8AAAAFAAAA6wAAAPQAAADqAAAAGwD#####AP8AAAAAAAUAAADqAAAA+gAAAOkAAAAdAP####8BAH8AABAAAAEAAAABAAAA6gAAAOkAAADrAAAAHQD#####AQB#AAAQAAABAAAAAQAAAOsAAADqAAAA6QAAAB0A#####wEAfwAAEAAAAQAAAAEAAADpAAAA6wAAAOoAAAAYAP####8AAAEAAAAA7wAAABkA#####wEAfwAAEAAAAAAAAAAAAAAAQAgAAAAAAAAAAAcAAgAAAQEAAAAZAP####8BAH8AABAAAAAAAAAAAAAAAEAIAAAAAAAAAAAHAAEAAAEBAAAAGAD#####AAAA#wAAAPEAAAAZAP####8BAH8AABAAAAAAAAAAAAAAAEAIAAAAAAAAAAAHAAEAAAEEAAAAGQD#####AQB#AAAQAAAAAAAAAAAAAABACAAAAAAAAAAABwACAAABBAAAABgA#####wAAAP4AAADwAAAAGQD#####AQB#AAAQAAAAAAAAAAAAAABACAAAAAAAAAAABwABAAABBwAAABkA#####wEAfwAAEAAAAAAAAAAAAAAAQAgAAAAAAAAAAAcAAgAAAQcAAAAWAP####8BAH8AAAAAAQAAAOsAAAABQAAAAAAAAAAAAAAAFgD#####AQB#AAAAAAEAAADpAAAAAUAAAAAAAAAAAAAAABoA#####wAAfwABAAJuQQAAAQIQAAAAAAABAAAAAQAAAAEAAAAAAAAAAAABQQAAABoA#####wAAfwABAAJuQgAAAQYQAAAAAAABAAAAAQAAAAEAAAAAAAAAAAABQgAAABoA#####wAAfwABAAJuQwAAAQkQAAAAAAABAAAAAQAAAAEAAAAAAAAAAAABQwAAABwA#####wEAfwAAEAAAAAAAAAAAAAAAQAgAAAAAAAAAAAgAAAAA6gAAAOsAAAAcAP####8BAH8AABAAAAAAAAAAAAAAAEAIAAAAAAAAAAAIAAAAAOsAAADpAAAAHAD#####AQB#AAAQAAAAAAAAAAAAAABACAAAAAAAAAAACAAAAADpAAAA6gAAABYA#####wEAfwAAAAABAAABDwAAAAE#+AAAAAAAAAAAAAAWAP####8BAH8AAAAAAQAAARAAAAABP#gAAAAAAAAAAAAAFgD#####AQB#AAAAAAEAAAERAAAAAT#4AAAAAAAAAAAAABMA#####wEAfwAAEAAAAQAAAAEAAAEPAAAA7AAAABMA#####wEAfwAAEAAAAQAAAAEAAAEQAAAA7gAAABMA#####wEAfwAAEAAAAQAAAAEAAAERAAAA7QAAABgA#####wAAARUAAAESAAAAGQD#####AQB#AAAQAAAAAAAAAAAAAABACAAAAAAAAAAACAACAAABGAAAABkA#####wEAfwAAEAAAAAAAAAAAAAAAQAgAAAAAAAAAAAgAAQAAARgAAAAYAP####8AAAEWAAABEwAAABkA#####wEAfwAAEAAAAAAAAAAAAAAAQAgAAAAAAAAAAAgAAgAAARsAAAAZAP####8BZmZmABAAAAAAAAAAAAAAAEAIAAAAAAAAAAAIAAEAAAEbAAAAGAD#####AAABFwAAARQAAAAZAP####8BAH8AABAAAAAAAAAAAAAAAEAIAAAAAAAAAAAIAAIAAAEeAAAAGQD#####AQB#AAAQAAAAAAAAAAAAAABACAAAAAAAAAAACAABAAABHgAAABoA#####wBmZmYBAANubDEAAAEdEAAAAAAAAQAAAAEAAAABAAAAAAAAAAAAAmwxAAAAGgD#####AGZmZgEAA25sMwAAARoQAAAAAAABAAAAAQAAAAEAAAAAAAAAAAACbDMAAAAaAP####8AZmZmAQADbmwyAAABHxAAAAAAAAEAAAABAAAAAQAAAAAAAAAAAAJsMgAAAED##########w=='
    let A1 = 0
    let A2 = 0
    const liid = j3pGetNewId()
    const hh = j3pCreeSVG(stor.lesdiv.figure1, { id: liid, width: 570, height: 300 })
    hh.style.border = 'solid black 1px'
    stor.mtgAppLecteur.removeAllDoc()
    stor.mtgAppLecteur.addDoc(liid, fig, true)
    stor.mtgAppLecteur.calculateAndDisplayAll(true)

    objet.listedeselemedessine = []
    objet.longx = 15
    objet.hauty = 8
    objet.nomspris = []
    objet.fifig = []
    objet.lisecss = []

    objet.lettres = []
    for (i = 0; i < 7; i++) {
      addMaj(objet.lettres)
    }
    const lettres = objet.lettres
    stor.lettres = lettres
    stor.nomtri = lettres[0] + lettres[1] + lettres[2]
    stor.nomtri2 = lettres[3] + lettres[4] + lettres[5]
    if (j3pGetRandomBool()) {
      stor.nomtri2 = lettres[4] + lettres[3] + lettres[5]
    }
    if (j3pGetRandomBool()) {
      stor.nomtri2 = lettres[5] + lettres[3] + lettres[4]
    }
    stor.nomcote1 = '[' + lettres[0] + lettres[1] + ']'
    stor.nomcote2 = '[' + lettres[1] + lettres[2] + ']'
    stor.nomcote3 = '[' + lettres[2] + lettres[0] + ']'
    stor.nomcote4 = '[' + lettres[3] + lettres[4] + ']'
    stor.nomcote5 = '[' + lettres[4] + lettres[5] + ']'
    stor.nomcote6 = '[' + lettres[5] + lettres[3] + ']'
    stor.nomlc1 = lettres[0] + lettres[1]
    stor.nomlc2 = lettres[1] + lettres[2]
    stor.nomlc3 = lettres[2] + lettres[0]
    stor.nomlc4 = lettres[3] + lettres[4]
    stor.nomlc5 = lettres[4] + lettres[5]
    stor.nomlc6 = lettres[5] + lettres[3]

    switch (stor.Lexo.prop) {
      case 1:
        stor.visible = [false, false, false, true, true, true]
        stor.visible[j3pGetRandomInt(0, 2)] = true
        break
      case 2:
        switch (j3pGetRandomInt(0, 2)) {
          case 0:
            stor.visible = [false, true, true, true, false, false]
            stor.visible[j3pGetRandomInt(4, 5)] = true
            break
          case 1:
            stor.visible = [true, false, true, false, true, false]
            stor.visible[j3pGetRandomInt(0, 1) * 2 + 3] = true
            break
          case 2:
            stor.visible = [true, true, false, false, false, true]
            stor.visible[j3pGetRandomInt(3, 4)] = true
            break
        }
        break
      case 3:
        stor.visible = [true, true, true, false, false, false]
        stor.visible[j3pGetRandomInt(3, 5)] = true
        break
    }

    /// determine longueurs et angles
    if (!ds.Entiers) {
      stor.l1 = j3pGetRandomInt(250, 450)
      stor.l2 = j3pGetRandomInt(250, 450)
      if (stor.l2 === stor.l1) stor.l2 = 240
      stor.l3 = j3pGetRandomInt(250, 450)
      if ((stor.l3 === stor.l2) || (stor.l3 === stor.l1)) stor.l3 = 480
      stor.l4 = j3pGetRandomInt(500, 900)
      stor.l5 = j3pGetRandomInt(500, 900)
      if (stor.l4 === stor.l5) stor.l4 = 480
      stor.l6 = j3pGetRandomInt(500, 900)
      if ((stor.l6 === stor.l4) || (stor.l6 === stor.l5)) stor.l6 = 920
      stor.l1 = j3pArrondi(stor.l1 / 10, 2)
      stor.l2 = j3pArrondi(stor.l2 / 10, 2)
      stor.l3 = j3pArrondi(stor.l3 / 10, 2)
      stor.l4 = j3pArrondi(stor.l4 / 10, 2)
      stor.l5 = j3pArrondi(stor.l5 / 10, 2)
      stor.l6 = j3pArrondi(stor.l6 / 10, 2)
    } else {
      stor.l1 = j3pGetRandomInt(22, 40)
      stor.l2 = j3pGetRandomInt(21, 40)
      if (stor.l2 === stor.l1) stor.l2 = 20
      stor.l3 = j3pGetRandomInt(21, 40)
      if ((stor.l3 === stor.l2) || (stor.l3 === stor.l1)) stor.l3 = 41
      stor.l4 = j3pGetRandomInt(45, 80)
      stor.l5 = j3pGetRandomInt(45, 80)
      if (stor.l4 === stor.l5) stor.l4 = 43
      stor.l6 = j3pGetRandomInt(45, 80)
      if ((stor.l6 === stor.l4) || (stor.l6 === stor.l5)) stor.l6 = 82
    }

    stor.nominc = []

    objet.points = []
    const achache = []

    let nl1, nl2, nl3, nA, nB, nC
    switch (stor.Lexo.sens) {
      case 1:
        A1 = j3pGetRandomInt(0, 359)
        A2 = A1
        nl1 = '#nl12'; nl2 = '#nl22'; nl3 = '#nl32'; nA = '#nA2'; nB = '#nB2'; nC = '#nC2'
        achache.push('#nl11', '#nl21', '#nl31', '#nA1', '#nB1', '#nC1', '#narc1', '#narc2', '#narc3', '#s11', '#s21', '#s31')
        break
      case 2:
        A1 = j3pGetRandomInt(0, 359)
        do { j3pGetRandomInt(20, 340) } while (Math.abs(A1 - A2) < 30)
        nl1 = '#nl12'; nl2 = '#nl22'; nl3 = '#nl32'; nA = '#nA2'; nB = '#nB2'; nC = '#nC2'
        achache.push('#nl11', '#nl21', '#nl31', '#nA1', '#nB1', '#nC1', '#narc1', '#narc2', '#narc3', '#s11', '#s21', '#s31')
        break
      case 3:
        A1 = j3pGetRandomInt(0, 359)
        A2 = A1
        nl1 = '#nl11'; nl2 = '#nl21'; nl3 = '#nl31'; nA = '#nA1'; nB = '#nB1'; nC = '#nC1'
        achache.push('#nl12', '#nl22', '#nl32', '#nA2', '#nB2', '#nC2', '#narc4', '#narc5', '#narc6', '#s12', '#s22', '#s32')
        break
      case 4:
        A1 = j3pGetRandomInt(0, 359)
        do { j3pGetRandomInt(20, 340) } while (Math.abs(A1 - A2) < 30)
        nl1 = '#nl11'; nl2 = '#nl21'; nl3 = '#nl31'; nA = '#nA1'; nB = '#nB1'; nC = '#nC1'
        achache.push('#nl12', '#nl22', '#nl32', '#nA2', '#nB2', '#nC2', '#narc4', '#narc5', '#narc6', '#s12', '#s22', '#s32')
        break
    }

    for (let k = 0; k < achache.length; k++) {
      stor.mtgAppLecteur.setVisible(liid, achache[k], false, true)
    }
    stor.mtgAppLecteur.setText(liid, '#nA', lettres[0])
    stor.mtgAppLecteur.setText(liid, '#nB', lettres[1])
    stor.mtgAppLecteur.setText(liid, '#nC', lettres[2])
    stor.mtgAppLecteur.setText(liid, nA, lettres[3])
    stor.mtgAppLecteur.setText(liid, nB, lettres[4])
    stor.mtgAppLecteur.setText(liid, nC, lettres[5])

    if (stor.visible[0]) {
      stor.mtgAppLecteur.setText(liid, nl3, String(stor.l1).replace('.', ','))
    } else {
      stor.nominc.push(lettres[3] + lettres[4])
      stor.mtgAppLecteur.setVisible(liid, nl3, false, true)
    }
    if (stor.visible[1]) {
      stor.mtgAppLecteur.setText(liid, nl2, String(stor.l2).replace('.', ','))
    } else {
      stor.nominc.push(lettres[4] + lettres[5])
      stor.mtgAppLecteur.setVisible(liid, nl2, false, true)
    }
    if (stor.visible[2]) {
      stor.mtgAppLecteur.setText(liid, nl1, String(stor.l3).replace('.', ','))
    } else {
      stor.nominc.push(lettres[5] + lettres[3])
      stor.mtgAppLecteur.setVisible(liid, nl1, false, true)
    }

    if (stor.visible[3]) {
      stor.mtgAppLecteur.setText(liid, '#nl3', String(stor.l4).replace('.', ','))
    } else {
      stor.nominc.push(lettres[0] + lettres[1])
      stor.mtgAppLecteur.setVisible(liid, '#nl3', false, true)
    }
    if (stor.visible[4]) {
      stor.mtgAppLecteur.setText(liid, '#nl2', String(stor.l5).replace('.', ','))
    } else {
      stor.nominc.push(lettres[1] + lettres[2])
      stor.mtgAppLecteur.setVisible(liid, '#nl2', false, true)
    }
    if (stor.visible[5]) {
      stor.mtgAppLecteur.setText(liid, '#nl1', String(stor.l6).replace('.', ','))
    } else {
      stor.nominc.push(lettres[2] + lettres[0])
      stor.mtgAppLecteur.setVisible(liid, '#nl1', false, true)
    }
    stor.mtgAppLecteur.giveFormula2(liid, 'A1', A1)
    stor.mtgAppLecteur.giveFormula2(liid, 'A2', A2)
    stor.mtgAppLecteur.calculateAndDisplayAll(true)

    i = j3pGetRandomInt(0, 5)
    while (stor.visible[i]) {
      i++
      if (i === 6) i = 0
    }
    stor.li = i
    switch (i) {
      case 3: stor.lch = stor.lettres[0] + stor.lettres[1]
        stor.lbas = stor.l1
        break
      case 4: stor.lch = stor.lettres[1] + stor.lettres[2]
        stor.lbas = stor.l2
        break
      case 5: stor.lch = stor.lettres[2] + stor.lettres[0]
        stor.lbas = stor.l3
        break
      case 0: stor.lch = stor.lettres[3] + stor.lettres[4]
        stor.lbas = stor.l4
        break
      case 1: stor.lch = stor.lettres[4] + stor.lettres[5]
        stor.lbas = stor.l5
        break
      case 2: stor.lch = stor.lettres[5] + stor.lettres[3]
        stor.lbas = stor.l6
        break
    }
    if (stor.visible[0] && stor.visible[3]) stor.lhaut = [stor.l1, stor.l4]
    if (stor.visible[1] && stor.visible[4]) stor.lhaut = [stor.l2, stor.l5]
    if (stor.visible[2] && stor.visible[5]) stor.lhaut = [stor.l3, stor.l6]
    j3pAffiche(stor.lesdiv.figure2, null, '<i>Les longueurs sont exprimées en cm.</i>')
  }
  function egalitefrac (x, y) {
    return (x.num * y.den === x.den * y.num)
  }

  function initSection () {
    let i
    initCptPe(ds, stor)
    // On surcharge avec les parametres passés par le graphe
    me.surcharge()
    me.validOnEnter = false // ex donneesSection.touche_entree

    // Construction de la page
    me.construitStructurePage('presentation3')
    ds.nbetapes = 1
    ds.nbitems = ds.nbetapes * ds.nbrepetitions
    stor.lesExos = []

    let lp = [1, 2, 3]
    lp = j3pShuffle(lp)

    for (i = 0; i < ds.nbitems; i++) {
      stor.lesExos.push({ prop: lp[i % (lp.length)] })
    }
    stor.lesExos = j3pShuffle(stor.lesExos)

    lp = [1]
    if (ds.Deplacement) lp = [1, 2, 3, 4]
    // 1 mm pos
    // 2 retourn horizontal
    // 180
    // 4 retourn vertical
    lp = j3pShuffle(lp)
    for (i = 0; i < stor.lesExos.length; i++) {
      stor.lesExos[i].sens = lp[i % lp.length]
    }
    stor.lesExos = j3pShuffle(stor.lesExos)

    me.typederreurs = [0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0]

    me.score = 0
    let buft = 'Calculer une longueur avec des triangles semblables'
    if (!ds.Egalite && !ds.Reponse) { ds.Egalite = true }
    if (ds.Egalite && !ds.Reponse) buft = 'Ecrire l’égalité des rapports'

    me.afficheTitre(buft)
    getMtgCore({ withMathJax: true })
      .then(
        // success
        (mtgAppLecteur) => {
          stor.mtgAppLecteur = mtgAppLecteur
          enonceMain()
        },
        // failure
        (error) => {
          j3pShowError(error, { message: 'mathgraph n’est pas correctement chargé', mustNotify: true })
        })
      // plantage dans enonceMain
      .catch(j3pShowError)
  }

  function yaReponse () {
    if (stor.encours === 'Descrip') {
      if (!stor.listeAp1.changed) {
        stor.d.listeAp1.focus()
        return false
      }
      if (!stor.listeAp2.changed) {
        stor.d.listeAp2.focus()
        return false
      }
      if (!stor.listePara.changed) {
        stor.d.listePara.focus()
        return false
      }
      if (!stor.listeProp.changed) {
        stor.d.listeProp.focus()
        return false
      }
      return true
    }
    if (stor.encours === 'egalite') {
      if (!stor.listeE1.changed) {
        stor.listeE1.focus()
        return false
      }
      if (!stor.listeE2.changed) {
        stor.listeE2.focus()
        return false
      }
      if (!stor.listeE3.changed) {
        stor.listeE3.focus()
        return false
      }
      if (!stor.listeE4.changed) {
        stor.listeE4.focus()
        return false
      }
      if (!stor.listeE5.changed) {
        stor.listeE5.focus()
        return false
      }
      if (!stor.listeE6.changed) {
        stor.listeE6.focus()
        return false
      }
      return true
    }
    if (stor.encours === 'rempli') {
      if (!(stor.z1.ffalse === true)) {
        if (stor.z1.reponse() === '') {
          stor.z1.focus()
          return false
        }
      }
      if (!(stor.z2.ffalse === true)) {
        if (stor.z2.reponse() === '') {
          stor.z2.focus()
          return false
        }
      }
      if (!(stor.z3.ffalse === true)) {
        if (stor.z3.reponse() === '') {
          stor.z3.focus()
          return false
        }
      }
      if (!(stor.z4.ffalse === true)) {
        if (stor.z4.reponse() === '') {
          stor.z4.focus()
          return false
        }
      }
      if (!(stor.z5.ffalse === true)) {
        if (stor.z5.reponse() === '') {
          stor.z5.focus()
          return false
        }
      }
      if (!(stor.z6.ffalse === true)) {
        if (stor.z6.reponse() === '') {
          stor.z6.focus()
          return false
        }
      }
      return true
    }
    if (stor.encours === 'prod') {
      if (stor.zP1.reponse() === '') {
        stor.zP1.focus()
        return false
      }
      if (stor.zP2.reponse() === '') {
        stor.zP2.focus()
        return false
      }
      if (stor.zP3.reponse() === '') {
        stor.zP3.focus()
        return false
      }
      return true
    }
    if (stor.encours === 'concl') {
      if (stor.zFin.reponse() === '') {
        stor.zFin.focus()
        return false
      }
      return true
    }
  }

  function isRepOk () {
    stor.errAppartient = false
    stor.errPara = false
    stor.errProp = false
    stor.errDouble = false
    stor.errMauvaistri = false
    stor.errPasCoress = false
    stor.errCopie = false
    stor.errProduit = false
    stor.errDouble2 = false
    stor.errSimpli = false
    stor.errMalEcrit = false
    stor.errVirgFrac = false

    let ok = true
    let t1, t2, t3, t4, t5, t6
    let n1, n2, n3, n4, n5, n6

    if (stor.encours === 'egalite') {
      t1 = stor.listeE1.reponse.replace(/\$/g, '')
      t2 = stor.listeE2.reponse.replace(/\$/g, '')
      t3 = stor.listeE3.reponse.replace(/\$/g, '')
      t4 = stor.listeE4.reponse.replace(/\$/g, '')
      t5 = stor.listeE5.reponse.replace(/\$/g, '')
      t6 = stor.listeE6.reponse.replace(/\$/g, '')
      // verif pas double
      if (t1 === t2) {
        stor.listeE1.corrige(false)
        stor.listeE2.corrige(false)
        stor.errDouble = true
        ok = false
      }
      if (t1 === t3) {
        stor.listeE1.corrige(false)
        stor.listeE3.corrige(false)
        stor.errDouble = true
        ok = false
      }
      if (t1 === t4) {
        stor.listeE1.corrige(false)
        stor.listeE4.corrige(false)
        stor.errDouble = true
        ok = false
      }
      if (t1 === t5) {
        stor.listeE1.corrige(false)
        stor.listeE5.corrige(false)
        stor.errDouble = true
        ok = false
      }
      if (t1 === t6) {
        stor.listeE1.corrige(false)
        stor.listeE6.corrige(false)
        stor.errDouble = true
        ok = false
      }
      if (t2 === t3) {
        stor.listeE2.corrige(false)
        stor.listeE3.corrige(false)
        stor.errDouble = true
        ok = false
      }
      if (t2 === t4) {
        stor.listeE2.corrige(false)
        stor.listeE4.corrige(false)
        stor.errDouble = true
        ok = false
      }
      if (t2 === t5) {
        stor.listeE2.corrige(false)
        stor.listeE5.corrige(false)
        stor.errDouble = true
        ok = false
      }
      if (t2 === t6) {
        stor.listeE2.corrige(false)
        stor.listeE6.corrige(false)
        stor.errDouble = true
        ok = false
      }
      if (t3 === t4) {
        stor.listeE3.corrige(false)
        stor.listeE4.corrige(false)
        stor.errDouble = true
        ok = false
      }
      if (t3 === t5) {
        stor.listeE3.corrige(false)
        stor.listeE5.corrige(false)
        stor.errDouble = true
        ok = false
      }
      if (t3 === t6) {
        stor.listeE3.corrige(false)
        stor.listeE6.corrige(false)
        stor.errDouble = true
        ok = false
      }
      if (t4 === t5) {
        stor.listeE4.corrige(false)
        stor.listeE5.corrige(false)
        stor.errDouble = true
        ok = false
      }
      if (t4 === t6) {
        stor.listeE4.corrige(false)
        stor.listeE6.corrige(false)
        stor.errDouble = true
        ok = false
      }
      if (t6 === t5) {
        stor.listeE6.corrige(false)
        stor.listeE5.corrige(false)
        stor.errDouble = true
        ok = false
      }
      if (!ok) return false
      // verif bon tri
      n1 = (t1.indexOf(stor.lettres[3]) === -1) && (t1.indexOf(stor.lettres[4]) === -1)
      n2 = (t2.indexOf(stor.lettres[3]) === -1) && (t2.indexOf(stor.lettres[4]) === -1)
      n3 = (t3.indexOf(stor.lettres[3]) === -1) && (t3.indexOf(stor.lettres[4]) === -1)
      n4 = (t4.indexOf(stor.lettres[3]) === -1) && (t4.indexOf(stor.lettres[4]) === -1)
      n5 = (t5.indexOf(stor.lettres[3]) === -1) && (t5.indexOf(stor.lettres[4]) === -1)
      n6 = (t6.indexOf(stor.lettres[3]) === -1) && (t6.indexOf(stor.lettres[4]) === -1)
      if (n1 !== n3 || n3 !== n5) {
        if (n1 === n3) {
          stor.listeE5.corrige(false)
          stor.errMauvaistri = true
          ok = false
        } else if (n1 === n5) {
          stor.listeE3.corrige(false)
          stor.errMauvaistri = true
          ok = false
        } else {
          stor.listeE1.corrige(false)
          stor.errMauvaistri = true
          ok = false
        }
      }
      if (n2 !== n4 || n4 !== n6) {
        if (n2 === n4) {
          stor.listeE6.corrige(false)
          stor.errMauvaistri = true
          ok = false
        } else if (n2 === n6) {
          stor.listeE4.corrige(false)
          stor.errMauvaistri = true
          ok = false
        } else {
          stor.listeE2.corrige(false)
          stor.errMauvaistri = true
          ok = false
        }
      }
      if (!ok) return false
      // verif bien par deux
      if ((t1 === stor.lettres[3] + stor.lettres[4]) || (t1 === stor.lettres[0] + stor.lettres[1])) n1 = 3
      if ((t2 === stor.lettres[3] + stor.lettres[4]) || (t2 === stor.lettres[0] + stor.lettres[1])) n2 = 3
      if ((t3 === stor.lettres[3] + stor.lettres[4]) || (t3 === stor.lettres[0] + stor.lettres[1])) n3 = 3
      if ((t4 === stor.lettres[3] + stor.lettres[4]) || (t4 === stor.lettres[0] + stor.lettres[1])) n4 = 3
      if ((t5 === stor.lettres[3] + stor.lettres[4]) || (t5 === stor.lettres[0] + stor.lettres[1])) n5 = 3
      if ((t6 === stor.lettres[3] + stor.lettres[4]) || (t6 === stor.lettres[0] + stor.lettres[1])) n6 = 3

      if ((t1 === stor.lettres[1] + stor.lettres[2]) || (t1 === stor.lettres[4] + stor.lettres[5])) n1 = 1
      if ((t2 === stor.lettres[1] + stor.lettres[2]) || (t2 === stor.lettres[4] + stor.lettres[5])) n2 = 1
      if ((t3 === stor.lettres[1] + stor.lettres[2]) || (t3 === stor.lettres[4] + stor.lettres[5])) n3 = 1
      if ((t4 === stor.lettres[1] + stor.lettres[2]) || (t4 === stor.lettres[4] + stor.lettres[5])) n4 = 1
      if ((t5 === stor.lettres[1] + stor.lettres[2]) || (t5 === stor.lettres[4] + stor.lettres[5])) n5 = 1
      if ((t6 === stor.lettres[1] + stor.lettres[2]) || (t6 === stor.lettres[4] + stor.lettres[5])) n6 = 1

      if ((t1 === stor.lettres[2] + stor.lettres[0]) || (t1 === stor.lettres[5] + stor.lettres[3])) n1 = 2
      if ((t2 === stor.lettres[2] + stor.lettres[0]) || (t2 === stor.lettres[5] + stor.lettres[3])) n2 = 2
      if ((t3 === stor.lettres[2] + stor.lettres[0]) || (t3 === stor.lettres[5] + stor.lettres[3])) n3 = 2
      if ((t4 === stor.lettres[2] + stor.lettres[0]) || (t4 === stor.lettres[5] + stor.lettres[3])) n4 = 2
      if ((t5 === stor.lettres[2] + stor.lettres[0]) || (t5 === stor.lettres[5] + stor.lettres[3])) n5 = 2
      if ((t6 === stor.lettres[2] + stor.lettres[0]) || (t6 === stor.lettres[5] + stor.lettres[3])) n6 = 2
      if (n1 !== n2) {
        stor.listeE1.corrige(false)
        stor.listeE4.corrige(false)
        stor.errPasCoress = true
        ok = false
      }
      if (n3 !== n4) {
        stor.listeE2.corrige(false)
        stor.listeE5.corrige(false)
        stor.errPasCoress = true
        ok = false
      }
      if (n5 !== n6) {
        stor.listeE6.corrige(false)
        stor.listeE3.corrige(false)
        stor.errPasCoress = true
        ok = false
      }
      return ok
    }
    if (stor.encours === 'rempli') {
      if (!(stor.z6.ffalse === true)) {
        t1 = longueurat(stor.listeE6.reponse.replace(/\$/g, ''))
        t1 = (t1 + '').replace('.', ',')
        if (t1 !== stor.z6.reponse()) {
          stor.errCopie = true
          stor.z6.corrige(false)
          ok = false
        }
      }
      if (!(stor.z4.ffalse === true)) {
        t1 = longueurat(stor.listeE4.reponse.replace(/\$/g, ''))
        t1 = (t1 + '').replace('.', ',')
        if (t1 !== stor.z4.reponse()) {
          stor.errCopie = true
          stor.z4.corrige(false)
          ok = false
        }
      }
      if (!(stor.z3.ffalse === true)) {
        t1 = longueurat(stor.listeE3.reponse.replace(/\$/g, ''))
        t1 = (t1 + '').replace('.', ',')
        if (t1 !== stor.z3.reponse()) {
          stor.errCopie = true
          stor.z3.corrige(false)
          ok = false
        }
      }
      if (!(stor.z2.ffalse === true)) {
        t1 = longueurat(stor.listeE2.reponse.replace(/\$/g, ''))
        t1 = (t1 + '').replace('.', ',')
        if (t1 !== stor.z2.reponse()) {
          stor.errCopie = true
          stor.z2.corrige(false)
          ok = false
        }
      }
      if (!(stor.z5.ffalse === true)) {
        t1 = longueurat(stor.listeE5.reponse.replace(/\$/g, ''))
        t1 = (t1 + '').replace('.', ',')
        if (t1 !== stor.z5.reponse()) {
          stor.errCopie = true
          stor.z5.corrige(false)
          ok = false
        }
      }
      if (!(stor.z1.ffalse === true)) {
        t1 = longueurat(stor.listeE1.reponse.replace(/\$/g, ''))
        t1 = (t1 + '').replace('.', ',')
        if (t1 !== stor.z1.reponse()) {
          stor.errCopie = true
          stor.z1.corrige(false)
          ok = false
        }
      }
      return ok
    }
    if (stor.encours === 'prod') {
      // cherche les deux longueur en haut
      stor.atthaut = [(stor.lbas + '').replace('.', ',')]
      // si stor.lch est en ptittrianglr, on pend stor.lhaut[1]
      if ((stor.lch.indexOf(stor.lettres[0]) !== -1) || (stor.lch.indexOf(stor.lettres[2]) !== -1)) {
        stor.atthaut.push((stor.lhaut[1] + '').replace('.', ','))
        stor.attbas = (stor.lhaut[0] + '').replace('.', ',')
      } else {
        stor.atthaut.push((stor.lhaut[0] + '').replace('.', ','))
        stor.attbas = (stor.lhaut[1] + '').replace('.', ',')
      }
      if (stor.zP2.reponse() === stor.zP1.reponse()) {
        stor.zP2.corrige(false)
        stor.zP1.corrige(false)
        stor.errDouble2 = true
        ok = false
      }
      if (stor.zP2.reponse() === stor.zP3.reponse()) {
        stor.zP2.corrige(false)
        stor.zP3.corrige(false)
        stor.errDouble2 = true
        ok = false
      }
      if (stor.zP1.reponse() === stor.zP3.reponse()) {
        stor.zP1.corrige(false)
        stor.zP3.corrige(false)
        stor.errDouble2 = true
        ok = false
      }
      if (stor.zP3.reponse() !== stor.attbas) {
        stor.zP3.corrige(false)
        stor.errProduit = true
        ok = false
      }
      if ((stor.zP1.reponse() !== stor.atthaut[0]) && (stor.zP1.reponse() !== stor.atthaut[1])) {
        stor.zP1.corrige(false)
        stor.errProduit = true
        ok = false
      }
      if ((stor.zP2.reponse() !== stor.atthaut[0]) && (stor.zP2.reponse() !== stor.atthaut[1])) {
        stor.zP2.corrige(false)
        stor.errProduit = true
        ok = false
      }
      return ok
    }
    if (stor.encours === 'concl') {
      t1 = stor.zFin.reponse()
      t2 = stor.zFin.reponse2()
      t3 = stor.zFin.reponsenb()
      t4 = t2[0].replace(/ /g, '')
      t5 = (t2[1] + '').replace(/ /g, '')
      if ((t4.indexOf('.') === 0) || (t4.indexOf('.') === t4.length - 1) || (t4.indexOf('.') !== t4.lastIndexOf('.')) || (t4 === '')) {
        stor.zFin.corrige(false)
        stor.errMalEcrit = true
        ok = false
      }
      if ((t5.indexOf('.') === 0) || (t4.indexOf('.') === t5.length - 1) || (t5.indexOf('.') !== t5.lastIndexOf('.')) || (t5 === '')) {
        stor.zFin.corrige(false)
        stor.errMalEcrit = true
        ok = false
      }
      if (!ok) return false
      n1 = { num: Math.round(parseFloat(stor.atthaut[0].replace(',', '.')) * 100 * parseFloat(stor.atthaut[1].replace(',', '.'))), den: Math.round(parseFloat(stor.attbas.replace(',', '.')) * 100) }
      n2 = { num: t3[0], den: t3[1] }
      while (n2.num !== Math.round(n2.num)) {
        n2.num = j3pArrondi(n2.num * 10, 10)
        n2.den = j3pArrondi(n2.den * 10, 10)
      }
      if (stor.Yaapprox) {
        // verif que 1 chiffre apres virgule
        if (t4.indexOf('.') !== t4.length - 2) {
          ok = false
          stor.zFin.corrige(false)
          stor.errAprox = true
        }
        if (Math.abs(n1.num / n1.den - n2.num / n2.den) > 0.1) {
          ok = false
          stor.zFin.corrige(false)
          stor.errAprox2 = true
        }
      } else if (!egalitefrac(n1, n2)) {
        stor.zFin.corrige(false)
        return false
      }
      if (t1.indexOf('frac') !== -1) {
        if ((t4.indexOf('.') !== -1) || (t5.indexOf('.') !== -1)) {
          stor.errVirgFrac = true
          ok = false
        } else {
          if ((j3pPGCD(t3[0], t3[1], { negativesAllowed: true, valueIfZero: 1 }) !== 1) && (ds.Simplifie)) {
            stor.errSimpli = true
            ok = false
            stor.zFin.corrige(false)
          }
        }
      }
    }

    return ok
  } // isRepOk

  function longueurat (st) {
    switch (st) {
      case stor.lettres[0] + stor.lettres[1]: return stor.l4
      case stor.lettres[1] + stor.lettres[2]: return stor.l5
      case stor.lettres[2] + stor.lettres[0]: return stor.l6
      case stor.lettres[3] + stor.lettres[4]: return stor.l1
      case stor.lettres[4] + stor.lettres[5]: return stor.l2
      case stor.lettres[5] + stor.lettres[3]: return stor.l3
    }
  }

  function desactiveAll () {
    if (stor.encours === 'Descrip') {
      stor.listeAp1.disable()
      stor.listeAp2.disable()
      stor.listePara.disable()
      stor.listeProp.disable()
    }
    if (stor.encours === 'egalite') {
      stor.listeE1.disable()
      stor.listeE2.disable()
      stor.listeE3.disable()
      stor.listeE4.disable()
      stor.listeE5.disable()
      stor.listeE6.disable()
    }
    if (stor.encours === 'rempli') {
      if (!(stor.z6.ffalse === true)) {
        stor.z6.disable()
      }
      if (!(stor.z5.ffalse === true)) {
        stor.z5.disable()
      }
      if (!(stor.z4.ffalse === true)) {
        stor.z4.disable()
      }
      if (!(stor.z3.ffalse === true)) {
        stor.z3.disable()
      }
      if (!(stor.z2.ffalse === true)) {
        stor.z2.disable()
      }
      if (!(stor.z1.ffalse === true)) {
        stor.z1.disable()
      }
    }
    if (stor.encours === 'prod') {
      stor.zP1.disable()
      stor.zP2.disable()
      stor.zP3.disable()
    }
    if (stor.encours === 'concl') {
      stor.zFin.disable()
      stor.brouillon1.disable()
    }
  } // desactiveAll

  function passeToutVert () {
    if (stor.encours === 'Descrip') {
      stor.listeAp1.corrige(true)
      stor.listeAp2.corrige(true)
      stor.listePara.corrige(true)
      stor.listeProp.corrige(true)
    }
    if (stor.encours === 'egalite') {
      stor.listeE1.corrige(true)
      stor.listeE2.corrige(true)
      stor.listeE3.corrige(true)
      stor.listeE4.corrige(true)
      stor.listeE5.corrige(true)
      stor.listeE6.corrige(true)
    }
    if (stor.encours === 'rempli') {
      if (!(stor.z6.ffalse === true)) {
        stor.z6.corrige(true)
      }
      if (!(stor.z5.ffalse === true)) {
        stor.z5.corrige(true)
      }
      if (!(stor.z4.ffalse === true)) {
        stor.z4.corrige(true)
      }
      if (!(stor.z3.ffalse === true)) {
        stor.z3.corrige(true)
      }
      if (!(stor.z2.ffalse === true)) {
        stor.z2.corrige(true)
      }
      if (!(stor.z1.ffalse === true)) {
        stor.z1.corrige(true)
      }
    }
    if (stor.encours === 'prod') {
      stor.zP1.corrige(true)
      stor.zP2.corrige(true)
      stor.zP3.corrige(true)
    }
    if (stor.encours === 'concl') {
      stor.zFin.corrige(true)
    }
  } // passeToutVert
  function barrelesfo () {
    if (stor.encours === 'Descrip') {
      stor.listeAp1.barreIfKo()
      stor.listeAp2.barreIfKo()
      stor.listePara.barreIfKo()
      stor.listeProp.barreIfKo()
    }
    if (stor.encours === 'egalite') {
      stor.listeE1.barreIfKo()
      stor.listeE2.barreIfKo()
      stor.listeE3.barreIfKo()
      stor.listeE4.barreIfKo()
      stor.listeE5.barreIfKo()
      stor.listeE6.barreIfKo()
    }
    if (stor.encours === 'rempli') {
      if (!(stor.z6.ffalse === true)) {
        stor.z6.barreIfKo()
      }
      if (!(stor.z5.ffalse === true)) {
        stor.z5.barreIfKo()
      }
      if (!(stor.z4.ffalse === true)) {
        stor.z4.barreIfKo()
      }
      if (!(stor.z3.ffalse === true)) {
        stor.z3.barreIfKo()
      }
      if (!(stor.z2.ffalse === true)) {
        stor.z2.barreIfKo()
      }
      if (!(stor.z1.ffalse === true)) {
        stor.z1.barreIfKo()
      }
    }
    if (stor.encours === 'prod') {
      stor.zP1.barreIfKo()
      stor.zP2.barreIfKo()
      stor.zP3.barreIfKo()
    }
    if (stor.encours === 'concl') {
      stor.zFin.barre()
    }
  }

  function affCorrFaux (isFin) {
    /// //affiche indic

    if (stor.errAppartient) {
      j3pAffiche(stor.lesdiv.explications, null, 'Vérifie sur quel côté se trouvent les points  !\n')
      stor.yaexplik = true
    }
    if (stor.errPara) {
      j3pAffiche(stor.lesdiv.explications, null, 'Des droites parallèles ne se coupent pas !\n')
      stor.yaexplik = true
    }
    if (stor.errProp) {
      j3pAffiche(stor.lesdiv.explications, null, 'Il faut utiliser la bonne propriété !\n')
      stor.yaexplik = true
    }
    if (stor.errDouble) {
      j3pAffiche(stor.lesdiv.explications, null, 'Tu écris plusieurs fois la même longueur !\n')
      stor.yaexplik = true
    }
    if (stor.errMauvaistri) {
      j3pAffiche(stor.lesdiv.explications, null, 'Les trois longueurs d’un même triangle doivent être \nsoit toutes en numérateur, soit toutes en dénominateur !\n')
      stor.yaexplik = true
      stor.compteurPe1++
    }
    if (stor.errPasCoress) {
      j3pAffiche(stor.lesdiv.explications, null, 'Des longueurs ne se correspondent pas !\n')
      stor.yaexplik = true
      stor.compteurPe1++
    }
    if (stor.errCopie) {
      j3pAffiche(stor.lesdiv.explications, null, 'Il faut prendre les longueurs de l’énoncé !\n')
      stor.yaexplik = true
    }
    if (stor.errDouble2) {
      j3pAffiche(stor.lesdiv.explications, null, 'Tu écris plusieurs fois la même longueur !\n')
      stor.yaexplik = true
    }
    if (stor.errProduit) {
      j3pAffiche(stor.lesdiv.explications, null, 'On multiplie en diagonale, puis on divise par le troisième !\n')
      stor.yaexplik = true
      stor.compteurPe3++
    }
    if (stor.errSimpli) {
      j3pAffiche(stor.lesdiv.explications, null, 'La réponse doit être simplifiée !\n')
      stor.yaexplik = true
      stor.compteurPe4++
    }
    if (stor.errMalEcrit) {
      j3pAffiche(stor.lesdiv.explications, null, 'La réponse est mal écrite !\n')
      stor.yaexplik = true
      stor.compteurPe2++
    }
    if (stor.errVirgFrac) {
      j3pAffiche(stor.lesdiv.explications, null, 'Il n’y a pas de virgule dans une fraction !\n')
      stor.yaexplik = true
    }
    if (stor.errAprox) {
      j3pAffiche(stor.lesdiv.explications, null, 'Il ne doit y avoir qu’un seul chiffre après la virgule !\n')
      stor.yaexplik = true
    }

    if (isFin) {
      let t1, t2, t3, t4, t5, t6
      stor.yaco = true
      // afficheco
      if (stor.encours === 'egalite') {
        j3pAffiche(stor.lesdiv.solution, null, 'donc $\\frac{' + stor.nomlc1 + '}{' + stor.nomlc4 + '} = \\frac{' + stor.nomlc2 + '}{' + stor.nomlc5 + '} = \\frac{' + stor.nomlc3 + '}{' + stor.nomlc6 + '}$')
      }
      if (stor.encours === 'rempli') {
        if (stor.z1.ffalse === true) {
          t1 = stor.listeE1.reponse
        } else {
          t1 = (longueurat(stor.listeE1.reponse) + '').replace('.', ',')
        }
        if (stor.z2.ffalse === true) {
          t2 = stor.listeE2.reponse
        } else {
          t2 = (longueurat(stor.listeE2.reponse) + '').replace('.', ',')
        }
        if (stor.z3.ffalse === true) {
          t3 = stor.listeE3.reponse
        } else {
          t3 = (longueurat(stor.listeE3.reponse) + '').replace('.', ',')
        }
        if (stor.z4.ffalse === true) {
          t4 = stor.listeE4.reponse
        } else {
          t4 = (longueurat(stor.listeE4.reponse) + '').replace('.', ',')
        }
        if (stor.z5.ffalse === true) {
          t5 = stor.listeE5.reponse
        } else {
          t5 = (longueurat(stor.listeE5.reponse) + '').replace('.', ',')
        }
        if (stor.z6.ffalse === true) {
          t6 = stor.listeE6.reponse
        } else {
          t6 = (longueurat(stor.listeE6.reponse) + '').replace('.', ',')
        }
        j3pAffiche(stor.lesdiv.solution, null, 'donc $\\frac{' + t1 + '}{' + t2 + '} = \\frac{' + t3 + '}{' + t4 + '} = \\frac{' + t5 + '}{' + t6 + '}$')
      }
      if (stor.encours === 'prod') {
        j3pAffiche(stor.lesdiv.solution, null, '$' + stor.lch + ' = \\frac{' + stor.atthaut[0] + ' \\times ' + stor.atthaut[1] + ' }{' + stor.attbas + '}$')
      }
      if (stor.encours === 'concl') {
        t1 = parseFloat(stor.atthaut[0].replace(',', '.')) * parseFloat(stor.atthaut[1].replace(',', '.'))
        t2 = parseFloat(stor.attbas.replace(',', '.'))
        while ((Math.round(t1) !== t1) || (Math.round(t2) !== t2)) {
          t1 = j3pArrondi(t1 * 10, 8)
          t2 = j3pArrondi(t2 * 10, 8)
        }
        t3 = j3pPGCD(t1, t2, { negativesAllowed: true, valueIfZero: 1 })
        t1 = Math.round(t1 / t3)
        t2 = Math.round(t2 / t3)
        if (t2 === 1) {
          j3pAffiche(stor.lesdiv.solution, null, '$' + stor.lch + ' = ' + t1 + ' $ cm ')
        } else {
          if (stor.Yaapprox) {
            j3pAffiche(stor.lesdiv.solution, null, '$' + stor.lch + ' \\approx ' + (j3pArrondi(t1 / t2, 1) + '').replace('.', ',') + '$ cm')
          } else {
            j3pAffiche(stor.lesdiv.solution, null, '$' + stor.lch + ' = \\frac{' + t1 + ' }{' + t2 + '}$ cm')
          }
        }
      }
      // barre les faux
      barrelesfo()
      desactiveAll()
    } else {
      me.afficheBoutonValider()
    }
    if (stor.yaexplik) stor.lesdiv.explications.classList.add('explique')
    if (stor.yaco) stor.lesdiv.solution.classList.add('correction')
  } // affCorrFaux
  function enonceMain () {
    me.videLesZones()
    creeLesDiv()
    stor.lesdiv.travail.classList.add('travail')
    stor.lesdiv.consigneG.classList.add('enonce')
    stor.lesdiv.figure.classList.add('enonce')
    me.zonesElts.MG.classList.add('fond')
    stor.Lexo = faisChoixExos()
    faiFigure()
    poseQuestion()
    if (ds.Calculatrice) {
      me.fenetresjq = [{ name: 'Calculatrice', title: 'Une calculatrice', left: 600, top: 10 }]
      j3pCreeFenetres(me)
      j3pAfficheCroixFenetres('Calculatrice')
      j3pAjouteBouton(stor.lesdiv.calculatrice, 'Calculatrice', 'MepBoutons', 'Calculatrice', boutonfen)
    }
    me.afficheBoutonValider()
    me.finEnonce()
  }

  switch (this.etat) {
    case 'enonce':
      // code exécuté au lancement de la section
      if (me.debutDeLaSection) {
        initSection()
      } else {
        enonceMain()
      }
      break // case "enonce":

    case 'correction':
      // On teste si une réponse a été saisie
      stor.yaexplik = false
      stor.yaco = false
      stor.lesdiv.explications.innerHTML = ''
      stor.lesdiv.solution.innerHTML = ''
      stor.lesdiv.explications.classList.remove('explique')
      stor.lesdiv.solution.classList.remove('correction')

      if (yaReponse()) {
        // Bonne réponse
        if (isRepOk()) {
          this._stopTimer()
          stor.lesdiv.correction.style.color = this.styles.cbien
          stor.lesdiv.correction.innerHTML = cBien

          passeToutVert()
          desactiveAll()
          if (stor.encours === 'Descrip') {
            if (ds.Egalite) {
              poseEgalite()
              stor.niv++
              return
            }
            if (ds.Reponse) {
              rempliEgalite()
              stor.niv++
              return
            }
          }
          if (stor.encours === 'egalite') {
            if (ds.Reponse) {
              poseRempli()
              stor.niv++
              return
            }
          }
          if (stor.encours === 'rempli') {
            poseProduit()
            return
          }
          if (stor.encours === 'prod') {
            poseFin()
            return
          }
          stor.encours = ''

          this.score++
          this.typederreurs[0]++
          this.cacheBoutonValider()
          this.etat = 'navigation'
          this.sectionCourante()
        } else {
          // Pas de bonne réponse
          stor.lesdiv.correction.style.color = this.styles.cfaux
          stor.lesdiv.correction.innerHTML = cFaux
          if (this.isElapsed) {
            // A cause de la limite de temps :
            this._stopTimer()

            stor.lesdiv.correction.innerHTML += tempsDepasse + '<BR>'
            this.typederreurs[10]++
            this.cacheBoutonValider()

            affCorrFaux(true)
            stor.yaco = true

            this.etat = 'navigation'
            this.sectionCourante()
          } else {
            // Réponse fausse :
            if (me.essaiCourant < ds.nbchances) {
              stor.lesdiv.correction.innerHTML += '<br>' + essaieEncore

              affCorrFaux(false)

              // affCorrFaux.call(this, false)
              this.typederreurs[1]++
            } else {
              // Erreur au nème essai
              this._stopTimer()
              this.cacheBoutonValider()

              affCorrFaux(true)
              stor.yaco = true

              if (stor.encours === 'compare') {
                this.score = j3pArrondi(this.score + 0.1, 1)
              }
              if (stor.encours === 'calcul') {
                this.score = j3pArrondi(this.score + 0.4, 1)
              }
              if (stor.encours === 'concl') {
                this.score = j3pArrondi(this.score + 0.7, 1)
              }

              this.typederreurs[2]++
              this.etat = 'navigation'
              this.sectionCourante()
            }
          }
        }
      } else {
        // pas de réponse
        stor.lesdiv.correction.style.color = this.styles.cfaux
        stor.lesdiv.correction.innerHTML = reponseIncomplete
        this.afficheBoutonValider()
      }
      // Obligatoire
      this.finCorrection()
      break // case "correction":

    case 'navigation':
      if (this.sectionTerminee()) {
        this.parcours.pe = getPe(ds, stor)
      } else {
        this.etat = 'enonce'
      }
      this.finNavigation()
      break // case "navigation":
  }
}
