import { j3pAddElt, j3pArrondi, j3pClone, j3pEmpty, j3pGetRandomBool, j3pGetRandomInt, j3pPGCD, j3pShuffle } from 'src/legacy/core/functions'
import BulleAide from 'src/legacy/outils/bulleAide/BulleAide'
import ListeDeroulante from 'src/legacy/outils/listeDeroulante/ListeDeroulante'
import Zoneclick from 'src/legacy/outils/zoneclick/Zoneclick'
import { verifNombreBienEcrit } from 'src/legacy/outils/fonctions/ecritureNombre'
import { j3pAffiche } from 'src/lib/mathquill/functions'
import { addDefaultTable } from 'src/legacy/themes/table'
import ZoneStyleMathquill2 from 'src/legacy/outils/zoneStyleMathquill/ZoneStyleMathquill2'
import textesGeneriques from 'src/lib/core/textes'

const { cBien, cFaux, essaieEncore, regardeCorrection, tempsDepasse } = textesGeneriques

/**
 * Ajouter une description ici
 * @fileOverview
 * @author Tommy Barroy <t.barroy@laposte.net>
 * @since 2017-10
*/

export const params = {
  outils: ['mathquill'],
  /**
   * Ce qui est paramétrable dans l’éditeur de graphe
   * @type {LegacySectionParamsParametres}
   */
  parametres: [
    ['nbrepetitions', 9, 'entier', 'Nombre de répétitions de la section'],
    ['limite', 0, 'entier', 'Temps disponible par essai.'],
    ['Question', 'les deux', 'liste', '<u>Détermine</u>: QCM, il faut retrouver l’événement contraire d’un événement donné. <BR><BR> <u>Calcul</u>: Calculer la probabilité d’un événement contraire à un événement dont on connait la probabilité.', ['Détermine', 'Calcul', 'les deux']],
    ['Justifie', true, 'boolean', '<u>true</u> Une justification du calcul est demandée.'],
    ['theme', 'standard', 'liste', 'Modèle pour l’affichage', ['standard', 'zonesAvecImageDeFond']]
  ]
}
const reponseIncomplete = 'Réponse incomplète !'
const pbs = [
  { sit: 'Une urne contient des boules £as et £bs indiscernables au toucher. <br> Yann tire une boule au hasard.', comp: ['rouge', 'verte', 'bleue', 'blanche', 'noire'], ev: { ev1: 'tirer une boule £b', ev2: 'tirer une boule £a', p1: 'ne pas tirer de boule', e1: 'La personne doit tirer une boule !', p2: 'tirer une boule de chaque couleur', e2: 'La personne ne peut tirer qu’une seule boule !' } },
  { sit: 'Une urne contient des boules £as, £bs et £cs indiscernables au toucher. <br> Johann tire une boule au hasard.', comp: ['rouge', 'verte', 'bleue', 'blanche', 'noire'], ev: { ev1: 'tirer une boule £b', ev2: 'tirer une boule £a ou £c', p1: 'tirer une boule £c', e1: 'Cet événement n’est pas contraire !', p2: 'tirer une boule £a et £c', e2: 'Il n’y a qu’une couleur par boule !' } },
  { sit: 'Un sachet contient des bonbons £a, £b et £c.<br> Jasmin prend un bonbon au hasard.', comp: ['à la menthe', 'à la fraise', 'au citron', 'au chocolat'], ev: { ev1: 'prendre un bonbon £a', ev2: 'prendre un bonbon £b ou £c', p1: 'prendre un bonbon £b', e1: 'Cet événement n’est pas contraire !', p2: 'prendre un bonbon £b et £c', e2: 'Un seul goût par bonbon !' } }
]

/**
 * section contraire01
 * @this {Parcours}
 */
export default function main () {
  const me = this
  const stor = me.storage
  const ds = me.donneesSection

  function yareponse () {
    if (me.isElapsed) { return true }
    if (stor.etapeencours === 'Détermine') {
      if (stor.zoneclick.reponsenb.length === 0) return false
    }
    if (stor.etapeencours === 'Justifie') {
      if (!stor.zoneliste.changed) {
        stor.zoneliste.focus()
        return false
      }
    }
    if (stor.etapeencours === 'Calcul') {
      if (stor.zoneJus1.reponse() === '') {
        stor.zoneJus1.focus()
        return false
      }
    }
    return true
  }
  function BonneReponse () {
    stor.errEcritFrac = false
    stor.errEcrit = false
    if (stor.etapeencours === 'Détermine') {
      stor.garderep = stor.zoneclick.reponse
      if (stor.zoneclick.reponse[0] !== chabc(stor.evco)) {
        if (stor.zoneclick.reponsenb.length !== 0) {
          stor.douaide = stor.zoneclick.recupidaide(stor.zoneclick.reponsenb[0])
        } else {
          stor.douaide = -1
        }
      }
      return stor.zoneclick.reponse[0] === chabc(stor.evco)
    }
    if (stor.etapeencours === 'Justifie') {
      if (stor.zoneliste.reponse !== stor.repat) {
        stor.zoneliste.corrige(false)
        return false
      } else return true
    }
    if (stor.etapeencours === 'Calcul') {
      let ok = true
      const larep = stor.zoneJus1.reponse2()
      const larep2 = stor.zoneJus1.reponse()
      switch (stor.lexo.ty) {
        case 0:
          if (larep2.indexOf('{}') !== -1) {
            ok = false
            stor.errEcritFrac = true
            stor.zoneJus1.corrige(false)
          }
          break
        case 1:
        {
          const nb = String(larep[0]).replace(/ /g, '').replace(/\./g, ',')
          const jj = verifNombreBienEcrit(nb)
          if (!jj.good) {
            stor.errEcrit = true
            stor.errQuoiEcrit = jj.remede
            ok = false
            stor.zoneJus1.corrige(false)
          }
          break
        }
      }
      if (!ok) return false
      const larepnum = Number(String(larep[0]).replace(/ /g, ''))
      const larepden = Number(String(larep[1]).replace(/ /g, ''))
      if (Math.abs(larepnum * stor.rpn[1] - stor.rpn[0] * larepden) > 0.00000001) stor.zoneJus1.corrige(false)
      return (Math.abs(larepnum * stor.rpn[1] - stor.rpn[0] * larepden) < 0.00000001)
    }
  }
  function creeLedDiv () {
    const divConteneur = j3pAddElt(me.zonesElts.MG, 'div', '', {
      style: me.styles.etendre('toutpetit.enonce', { padding: '10px' })
    })
    const gg = addDefaultTable(divConteneur, 1, 3)
    stor.lesdiv.consigneG = gg[0][0]
    gg[0][1].style.width = '10px'
    stor.lesdiv.consigneG2 = addDefaultTable(divConteneur, 1, 3)[0][0]
    stor.lesdiv.consigne = addDefaultTable(divConteneur, 1, 1)[0][0]
    stor.lesdiv.travailjus = addDefaultTable(divConteneur, 1, 1)[0][0]
    stor.lesdiv.travail = addDefaultTable(divConteneur, 1, 1)[0][0]
    stor.lesdiv.explications = j3pAddElt(divConteneur, 'div', '', {
      style: me.styles.toutpetit.correction
    })
    stor.lesdiv.explications.style.color = me.styles.cfaux
    stor.lesdiv.solution = j3pAddElt(divConteneur, 'div', '', {
      style: me.styles.toutpetit.correction
    })
    const gg2 = addDefaultTable(gg[0][2], 2, 1)
    stor.lesdiv.calculatrice = gg2[1][0]
    stor.lesdiv.correction = gg2[0][0]
    me.zonesElts.MG.classList.add('fond')
    stor.lesdiv.travail.classList.add('travail')
    stor.lesdiv.consigne.classList.add('enonce')
    stor.lesdiv.consigneG.classList.add('enonce')
    stor.lesdiv.consigneG2.classList.add('enonce')
  }

  function poseQuestion () {
    stor.lesdiv.solution.classList.remove('correction')
    stor.lesdiv.explications.classList.remove('explique')
    j3pEmpty(stor.lesdiv.correction)
    j3pEmpty(stor.lesdiv.solution)
    j3pEmpty(stor.lesdiv.explications)
    if (stor.etapeencours === 'Détermine') {
      j3pAffiche(stor.lesdiv.consigne, null, '<b>Sélectionne l’événement contraire de l’événement ' + '\n"</b>' + chabc(stor.evch) + '<b>".</b>')
      stor.listeetiq = j3pShuffle([chabc(stor.lexo.pb.ev.ev1), chabc(stor.lexo.pb.ev.ev2), chabc(stor.lexo.pb.ev.p1), chabc(stor.lexo.pb.ev.p2)])
      stor.zoneclick = new Zoneclick(stor.lesdiv.travail, stor.listeetiq, { dispo: 'fixe', colonne: 2, ligne: 2, reponsesmultiples: false, afaire: '', width: 240, height: 124, affiche: true, image: false })
      stor.douco = stor.listeetiq.indexOf(chabc(stor.evco))
    } else {
      j3pEmpty(stor.lesdiv.consigneG2)
      j3pAffiche(stor.lesdiv.consigneG2, null, '$p\\text{ }("\\text{' + chabc(stor.evch) + '}") = ' + stor.lad + '$\n')
      j3pAffiche(stor.lesdiv.consigneG2, null, 'Les événements "' + chabc(stor.evch) + '" et "' + chabc(stor.evco) + '" sont contraires.')
      if (stor.etapeencours === 'Justifie') {
        j3pAffiche(stor.lesdiv.consigne, null, '<b>Sélectionne la bonne expression pour calculer $p\\text{ }("\\text{' + chabc(stor.evco) + '}") $</b>')
        const hhh = addDefaultTable(stor.lesdiv.travailjus, 1, 2)
        j3pAffiche(hhh[0][0], null, '$p\\text{ }("\\text{' + chabc(stor.evco) + '}") = $')
        stor.lesdiv.travailjus.classList.add('travail')
        let liste = []
        stor.repat = '$1 - p\\text{ }("\\text{' + chabc(stor.evch) + '}$")'
        liste.push('$1 + p\\text{ }("\\text{' + chabc(stor.evch) + '}")$')
        liste.push('$p\\text{ }("\\text{' + chabc(stor.evch) + '}")$')
        liste.push('$-p\\text{ }("\\text{' + chabc(stor.evch) + '}")$')
        liste.push('$p\\text{ }("\\text{' + chabc(stor.evch) + '}") + 1$')
        liste.push('$p\\text{ }("\\text{' + chabc(stor.evch) + '}") - 1$')
        liste = j3pShuffle(liste)
        liste.pop(); liste.pop()
        liste.push(stor.repat)
        liste = j3pShuffle(liste)
        liste.splice(0, 0, 'Choisir')
        stor.zoneliste = ListeDeroulante.create(hhh[0][1], liste, { centre: true })
        stor.douko = hhh[0][1]
      } else {
        if (stor.foco) {
          j3pEmpty(stor.douko)
          j3pAffiche(stor.douko, null, stor.repat)
          stor.douko.style.color = me.styles.toutpetit.correction.color
        }
        j3pAffiche(stor.lesdiv.consigne, null, '<b>Calcule $p\\text{ }("\\text{' + chabc(stor.evco) + '}") $</b>')
        const glob = (stor.lexo.ty === 2) ? 3 : 2
        const hhh = addDefaultTable(stor.lesdiv.travail, 1, glob)
        j3pAffiche(hhh[0][0], null, '$p\\text{ }("\\text{' + chabc(stor.evco) + '}") = $&nbsp;')
        if (glob === 3) j3pAffiche(hhh[0][2], null, '&nbsp;%')
        let restric = '0123456789'
        let lim = 6
        switch (stor.lexo.ty) {
          case 0: restric += '/'
            lim += 1
            break
          case 1: restric += '.,'
            break
        }
        stor.lesdiv.travail.classList.add('travail')
        stor.zoneJus1 = new ZoneStyleMathquill2(hhh[0][1], { id: 'zoneJus1', restric, hasAutoKeyboard: false, limite: lim, enter: me.sectionCourante.bind(me) })
      }
    }
  }
  function chabc (t) {
    return t.replace('£a', stor.rempa).replace('£b', stor.rempb).replace('£c', stor.rempc)
  }

  function initSection () {
    // Construction de la page
    me.construitStructurePage('presentation3')
    stor.afaire = []
    if (ds.Question === 'Détermine' || ds.Question === 'les deux') stor.afaire.push('Détermine')
    if (ds.Question === 'Calcul' || ds.Question === 'les deux') {
      if (ds.Justifie) stor.afaire.push('Justifie')
      stor.afaire.push('Calcul')
    }
    const nbetapes = stor.afaire.length
    me.surcharge({ nbetapes })
    stor.etapeencours = stor.afaire[stor.afaire.length - 1]
    stor.listeExo = []
    let fg = []
    for (let i = 0; i < ds.nbrepetitions; i++) {
      if (fg.length === 0) fg = j3pShuffle(j3pClone(pbs))
      stor.listeExo.push({ pb: fg.pop() })
    }
    stor.listeExo = j3pShuffle(stor.listeExo)

    fg = j3pShuffle([0, 1, 2])
    for (let i = 0; i < ds.nbrepetitions; i++) {
      if (fg.length === 0) fg = j3pShuffle(j3pClone(pbs))
      stor.listeExo[i].ty = fg[i % 3]
    }
    stor.listeExo = j3pShuffle(stor.listeExo)

    me.afficheTitre('Evénements contraires')
    stor.lesdiv = {}
    enonceMain()
  }
  function faisChoixExo () {
    stor.lexo = stor.listeExo.pop()
    const remp = j3pShuffle(stor.lexo.pb.comp)
    stor.rempa = remp.pop()
    stor.rempb = remp.pop()
    stor.rempc = remp.pop()
    if (j3pGetRandomBool()) {
      stor.evch = stor.lexo.pb.ev.ev1
      stor.evco = stor.lexo.pb.ev.ev2
    } else {
      stor.evch = stor.lexo.pb.ev.ev2
      stor.evco = stor.lexo.pb.ev.ev1
    }
    switch (stor.lexo.ty) {
      case 0: {
        const aa = j3pGetRandomInt(5, 50)
        const bb = j3pGetRandomInt(1, aa - 1)
        const pg = j3pPGCD(bb, aa)
        stor.lad = '\\frac{' + j3pArrondi(bb / pg, 0) + '}{' + j3pArrondi(aa / pg, 0) + '}'
        stor.lad2 = stor.lad
        stor.rpp = '\\frac{' + j3pArrondi((aa - bb) / pg, 0) + '}{' + j3pArrondi(aa / pg, 0) + '}'
        stor.rpn = [aa - bb, aa]
        break
      }
      case 1:
        stor.lad = j3pArrondi(j3pGetRandomInt(1, 99) / 100, 2)
        stor.lad2 = stor.lad
        stor.rpp = String(j3pArrondi(1 - stor.lad, 2))
        stor.rpn = [1 - stor.lad, 1]
        break
      default:
        stor.lad = j3pGetRandomInt(1, 99)
        stor.rpp = String(100 - stor.lad)
        stor.rpn = [100 - stor.lad, 1]
        stor.lad2 = '0,' + stor.lad
        stor.lad += '%'
        stor.rpp += '%'
    }
    stor.foco = false
  }
  function suiv () {
    let i = stor.afaire.indexOf(stor.etapeencours)
    i++
    if (i === stor.afaire.length) i = 0
    stor.etapeencours = stor.afaire[i]
  }
  function enonceMain () {
    suiv()
    if (stor.etapeencours === stor.afaire[0]) {
      faisChoixExo()
      me.videLesZones()
      creeLedDiv()
      j3pAffiche(stor.lesdiv.consigneG, null, chabc(stor.lexo.pb.sit))
    }
    j3pEmpty(stor.lesdiv.consigne)
    j3pEmpty(stor.lesdiv.travail)

    poseQuestion()
    me.finEnonce()
  }
  function affcofo (isFin) {
    if (stor.errEcritFrac) {
      j3pAffiche(stor.lesdiv.explications, null, 'La fraction est incomplète !')
      stor.yaexplik = true
    }
    if (stor.errEcrit) {
      j3pAffiche(stor.lesdiv.explications, null, stor.errQuoiEcrit)
      stor.yaexplik = true
    }
    if (stor.etapeencours === 'Détermine') {
      let quoidire = 'Un événement ne peut être son contraire !'
      if (stor.zoneclick.reponse[0] === chabc(stor.lexo.pb.ev.p1)) quoidire = chabc(stor.lexo.pb.ev.e1)
      if (stor.zoneclick.reponse[0] === chabc(stor.lexo.pb.ev.p2)) quoidire = chabc(stor.lexo.pb.ev.e2)
      stor.zoneclick.corrige([stor.douco], false)
      if (stor.douaide !== -1) stor.aideAvire = new BulleAide(stor.douaide, quoidire, {})
    }
    if (stor.etapeencours === 'Justifie') {
      j3pAffiche(stor.lesdiv.explications, null, 'Erreur de formule !')
      stor.yaexplik = true
    }
    if (isFin) {
      if (stor.etapeencours === 'Détermine') {
        stor.zoneclick.corrige([stor.douco], true)
        stor.zoneclick.disable()
      }
      if (stor.etapeencours === 'Justifie') {
        j3pAffiche(stor.lesdiv.solution, null, '$p\\text{ }("\\text{' + chabc(stor.evco) + '}") = $' + stor.repat)
        stor.zoneliste.barre()
        stor.zoneliste.disable()
        stor.foco = true
        stor.yaco = true
      }
      if (stor.etapeencours === 'Calcul') {
        j3pAffiche(stor.lesdiv.solution, null, '$p\\text{ }("\\text{' + chabc(stor.evco) + '}") = 1 - ' + stor.lad2 + '$\n')
        j3pAffiche(stor.lesdiv.solution, null, '$p\\text{ }("\\text{' + chabc(stor.evco) + '}") = ' + stor.rpp + '$')
        stor.zoneJus1.barre()
        stor.zoneJus1.disable()
        stor.yaco = true
      }
    }
  }

  switch (this.etat) {
    case 'enonce': {
      // code exécuté au lancement de la section
      if (this.debutDeLaSection) {
        initSection()
      } else {
        // A chaque répétition, on nettoie la scène
        // A commenter si besoin
        enonceMain()
      }
      break // case "enonce":
    }

    case 'correction':
      stor.yaco = false
      stor.yaexplik = false
      stor.lesdiv.solution.classList.remove('correction')
      stor.lesdiv.explications.classList.remove('explique')
      j3pEmpty(stor.lesdiv.solution)
      j3pEmpty(stor.lesdiv.explications)

      if (!yareponse()) {
        // ici on donne le message à afficher si une ou plusieurs zone(s) n’est pas complétée (tout d’abord on met en rouge : styles.cfaux)
        stor.lesdiv.correction.style.color = this.styles.cfaux
        stor.lesdiv.correction.innerHTML = reponseIncomplete
      } else {
        // Une réponse a été saisie
        // Bonne réponse
        if (BonneReponse()) {
          this.score++
          stor.lesdiv.correction.style.color = this.styles.cbien
          stor.lesdiv.correction.innerHTML = cBien

          if (stor.etapeencours === 'Détermine') {
            stor.zoneclick.corrige(true)
            stor.zoneclick.disable()
          }
          if (stor.etapeencours === 'Justifie') {
            stor.zoneliste.corrige(true)
            stor.zoneliste.disable()
          }
          if (stor.etapeencours === 'Calcul') {
            stor.zoneJus1.corrige(true)
            stor.zoneJus1.disable()
          }

          this.typederreurs[0]++
          this.cacheBoutonValider()
          this.etat = 'navigation'
          this.sectionCourante()
        } else {
          // Pas de bonne réponse
          stor.lesdiv.correction.style.color = this.styles.cfaux

          // A cause de la limite de temps :
          if (this.isElapsed) { // limite de temps
            stor.lesdiv.correction.innerHTML = tempsDepasse
            this.typederreurs[10]++
            this.cacheBoutonValider()

            affcofo(true)
            this.etat = 'navigation'
            this.sectionCourante()
          } else { // Réponse fausse :
            stor.lesdiv.correction.innerHTML = cFaux

            if (this.essaiCourant < this.donneesSection.nbchances) {
              stor.lesdiv.correction.innerHTML += '<br>' + essaieEncore
              affcofo(false)
              this.typederreurs[1]++
              // indication éventuelle ici
            } else { // Erreur au nème essai
              this.cacheBoutonValider()
              this._stopTimer()
              affcofo(true)
              stor.lesdiv.correction.innerHTML += '<br>' + regardeCorrection
              this.typederreurs[2]++
              this.etat = 'navigation'
              this.sectionCourante()
            }
          }
        }
      }

      if (stor.yaco) stor.lesdiv.solution.classList.add('correction')
      if (stor.yaexplik) stor.lesdiv.explications.classList.add('explique')

      // Obligatoire
      this.finCorrection()
      break // case "correction":

    case 'navigation':
      if (!this.sectionTerminee()) {
        this.etat = 'enonce'
      }
      this.finNavigation(true)
      break // case "navigation":
  }
}
