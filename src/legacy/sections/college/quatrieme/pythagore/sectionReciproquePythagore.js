import { j3pAddElt, j3pAjouteBouton, j3pArrondi, j3pGetNewId, j3pGetRandomInt, j3pShowError, j3pShuffle } from 'src/legacy/core/functions'
import { j3pCreeSVG } from 'src/legacy/core/functionsSvg'
import { j3pCreeFenetres, j3pAfficheCroixFenetres, j3pToggleFenetres } from 'src/legacy/core/functionsJqDialog'
import ListeDeroulante from 'src/legacy/outils/listeDeroulante/ListeDeroulante'
import ZoneStyleMathquill1 from 'src/legacy/outils/zoneStyleMathquill/ZoneStyleMathquill1'
import { getPe, initCptPe } from 'src/legacy/sections/college/helperTommy'
import { getMtgCore } from 'src/lib/outils/mathgraph'
import { addDefaultTable } from 'src/legacy/themes/table'
import { j3pAffiche } from 'src/lib/mathquill/functions'
import textesGeneriques from 'src/lib/core/textes'
import { addMaj } from 'src/lib/utils/string'

const { cBien, cFaux, essaieEncore, reponseIncomplete, tempsDepasse } = textesGeneriques

/**
 * Ajouter une description ici
 * @fileOverview
 * @author Tommy Barroy <t.barroy@laposte.net>
 * @since 2017-10
 */
export const params = {
  outils: ['mathquill', 'calculatrice'],
  /**
   * Ce qui est paramétrable dans l’éditeur de graphe
   * @type {LegacySectionParamsParametres}
   */
  parametres: [
    ['nbrepetitions', 9, 'entier', 'Nombre de répétitions de la section'],
    ['nbchances', 2, 'entier', 'Nombre d’essais possibles.'],
    ['Entiers', true, 'boolean', '<u>true</u>:  Les longueurs sont des nombres entiers'],
    ['Calculatrice', true, 'boolean', '<u>true</u>:  Une calculatrice est disponible'],
    // le modèle gère la compatibilité ascendante avec l’ancien paramètre MiseEnPagePlusClaire
    ['theme', 'standard', 'liste', 'Modèle pour l’affichage', ['standard', 'zonesAvecImageDeFond']]

  ],
  // dans le cas d’une section QUANTITATIVE
  pe: [
    { pe_1: 'Comparaison' },
    { pe_2: 'Addition' },
    { pe_3: 'Carré' }
  ]
}

/**
 * section ReciproquePythagore
 * @this {Parcours}
 */
export default function main () {
  const me = this
  const ds = me.donneesSection
  const stor = me.storage
  let svgId = stor.svgId

  function creeLesDiv () {
    stor.lesdiv = {}
    stor.lesdiv.conteneur = j3pAddElt(me.zonesElts.MG, 'div', {
      style: me.styles.etendre('toutpetit.enonce', { padding: '10px' })
    })
    stor.lesdiv.consigneG = addDefaultTable(stor.lesdiv.conteneur, 1, 1)[0][0]
    const tabL1 = addDefaultTable(stor.lesdiv.conteneur, 1, 3)
    stor.lesdiv.travail = tabL1[0][2]
    stor.lesdiv.figure = tabL1[0][0]
    tabL1[0][1].style.width = '5px'
    stor.lesdiv.explications = j3pAddElt(stor.lesdiv.conteneur, 'div')
    stor.lesdiv.solution = j3pAddElt(stor.lesdiv.conteneur, 'div')
    stor.lesdiv.calculatrice = j3pAddElt(me.zonesElts.MD, 'div', {
      style: me.styles.etendre('toutpetit.enonce', { padding: '10px' })
    })
    stor.lesdiv.correction = j3pAddElt(me.zonesElts.MD, 'div', {
      style: me.styles.etendre('toutpetit.enonce', { padding: '10px' })
    })
    stor.lesdiv.solution.style.color = me.styles.toutpetit.correction.color
    stor.lesdiv.explications.style.color = me.styles.cfaux
  }
  function poseQuestion () {
    j3pAffiche(stor.lesdiv.consigneG, null, '<b> On veut déterminer si le triangle ' + stor.nomtri + ' est rectangle. </b> \n')
    me.afficheBoutonValider()
    stor.tabConCot = addDefaultTable(stor.lesdiv.travail, 4, 3)
    stor.tabConCot[0][1].style.width = '30px'
    j3pAffiche(stor.tabConCot[0][0], null, 'Dans le triangle ' + stor.nomtri + ' ,')
    const tabQuest1 = addDefaultTable(stor.tabConCot[0][0], 1, 2)
    const ll = ['Choisir', stor.nomcote1, stor.nomcote2, stor.nomcote3]
    stor.liste1 = ListeDeroulante.create(tabQuest1[0][0], ll, { centre: true })
    j3pAffiche(tabQuest1[0][1], null, '&nbsp; est le plus grand côté.')
    j3pAffiche(stor.lesdiv.figure, null, '<b><i>Schéma (longueurs en cm)</i></b>')
    stor.encours = 'Plusgrand'
  }
  function poseQuestCompare () {
    stor.encours = 'compare'
    me.afficheBoutonValider()
    const tabQu2 = addDefaultTable(stor.tabConCot[1][0], 1, 2)
    j3pAffiche(tabQu2[0][0], null, 'Je compare &nbsp;')
    const ll = ['Choisir']
    ll.push(stor.nomlc1 + '² et ' + stor.nomlc2 + '² + ' + stor.nomlc3 + '²')
    ll.push(stor.nomlc2 + '² et ' + stor.nomlc3 + '² + ' + stor.nomlc1 + '²')
    ll.push(stor.nomlc3 + '² et ' + stor.nomlc1 + '² + ' + stor.nomlc2 + '²')
    stor.liste2 = ListeDeroulante.create(tabQu2[0][1], ll, { centre: true })
  }
  function poseCalcul () {
    stor.encours = 'calcul'
    me.afficheBoutonValider()
    const tabQ3 = addDefaultTable(stor.tabConCot[2][0], 2, 5)
    const tabgG = addDefaultTable(tabQ3[0][0], 2, 2)
    const tabGGX = addDefaultTable(tabgG[0][1], 1, 3)
    const tabDFE = addDefaultTable(tabgG[1][1], 1, 3)
    j3pAffiche(tabGGX[0][0], null, stor.noml3 + '² = &nbsp;')
    j3pAffiche(tabGGX[0][2], null, '&nbsp;²')
    j3pAffiche(tabDFE[0][0], null, stor.noml3 + '² = &nbsp;')
    j3pAffiche(tabDFE[0][2], null, '&nbsp; cm² ')
    stor.zgauche = new ZoneStyleMathquill1(tabGGX[0][1], { restric: '0123456789.,', enter: me.sectionCourante.bind(me) })
    stor.zgauche2 = new ZoneStyleMathquill1(tabDFE[0][1], { restric: '0123456789.,', enter: me.sectionCourante.bind(me) })
    const tabQue3dt = addDefaultTable(tabQ3[0][4], 3, 2)
    tabQ3[0][1].style.width = '5px'
    tabQ3[0][2].style.background = '#000000'
    tabQ3[0][2].style.width = '2px'
    tabQ3[0][3].style.width = '5px'
    tabQ3[0][0].style.verticalAlign = 'top'

    const tabXXLz3 = addDefaultTable(tabQue3dt[0][1], 1, 4)
    const tabXXLz2 = addDefaultTable(tabQue3dt[1][1], 1, 4)
    const tabXXLz = addDefaultTable(tabQue3dt[2][1], 1, 3)
    j3pAffiche(tabQue3dt[0][0], null, stor.noml2 + '² + ' + stor.noml1 + '² = &nbsp;')
    j3pAffiche(tabQue3dt[1][0], null, stor.noml2 + '² + ' + stor.noml1 + '² = &nbsp;')
    j3pAffiche(tabQue3dt[2][0], null, stor.noml2 + '² + ' + stor.noml1 + '² = &nbsp;')

    j3pAffiche(tabXXLz3[0][1], null, '&nbsp;² + &nbsp;')
    j3pAffiche(tabXXLz2[0][1], null, '&nbsp; + &nbsp;')
    j3pAffiche(tabXXLz3[0][3], null, '&nbsp;²')
    j3pAffiche(tabXXLz[0][2], null, '&nbsp; cm² ')
    stor.zdroite1 = new ZoneStyleMathquill1(tabXXLz3[0][0], { restric: '0123456789.,', enter: me.sectionCourante.bind(me) })
    stor.zdroite2 = new ZoneStyleMathquill1(tabXXLz3[0][2], { restric: '0123456789.,', enter: me.sectionCourante.bind(me) })
    stor.zdroite4 = new ZoneStyleMathquill1(tabXXLz2[0][0], { restric: '0123456789.,', enter: me.sectionCourante.bind(me) })
    stor.zdroite5 = new ZoneStyleMathquill1(tabXXLz2[0][3], { restric: '0123456789.,', enter: me.sectionCourante.bind(me) })
    stor.zdroite3 = new ZoneStyleMathquill1(tabXXLz[0][1], { restric: '0123456789.,', enter: me.sectionCourante.bind(me) })
  }
  function poseConclu () {
    stor.encours = 'concl'
    me.afficheBoutonValider()
    const tab4 = addDefaultTable(stor.tabConCot[3][0], 3, 1)
    let ll = ['Choisir', stor.noml3 + '²$ = $' + stor.noml1 + '² + ' + stor.noml2 + '²', stor.noml3 + '²$\\neq$' + stor.noml1 + '² + ' + stor.noml2 + '²']
    stor.liste3 = ListeDeroulante.create(tab4[0][0], ll, { centre: true })
    const tab4mil = addDefaultTable(tab4[2][0], 1, 2)
    ll = ['Choisir', 'L’égalité de Pythagore est vérifiée,', 'L’égalité de Pythagore n’est pas vérifiée,']
    stor.liste5 = ListeDeroulante.create(tab4mil[0][1], ll, { centre: true })
    const tab4fin = addDefaultTable(tab4[2][0], 1, 2)
    j3pAffiche(tab4fin[0][0], null, ' donc le triangle ' + stor.nomtri + '&nbsp; ')
    ll = [' est rectangle en ' + stor.lepo + ' .', ' est rectangle en ' + stor.lepo2 + ' .', ' n’est pas rectangle. ']
    ll = j3pShuffle(ll)
    ll.splice(0, 0, 'Choisir')
    stor.liste4 = ListeDeroulante.create(tab4fin[0][1], ll, { centre: true })
  }

  function boutonfen () {
    j3pToggleFenetres('Calculatrice')
  }

  function faisChoixExos () {
    const e = ds.lesExos.pop()
    e.sam = e.prop !== '+'
    return e
  }
  function faiFigure () {
    stor.lettres = []
    for (let i = 0; i < 4; i++) {
      addMaj(stor.lettres)
    }
    const lettres = stor.lettres
    stor.nomtri = lettres[0] + lettres[1] + lettres[2]
    stor.nomcote1 = '[' + lettres[0] + lettres[1] + ']'
    stor.nomcote2 = '[' + lettres[0] + lettres[2] + ']'
    stor.nomcote3 = '[' + lettres[1] + lettres[2] + ']'
    stor.nomlc1 = lettres[0] + lettres[1]
    stor.nomlc2 = lettres[0] + lettres[2]
    stor.nomlc3 = lettres[1] + lettres[2]

    /// determine longueurs et angles

    let tp = [[3, 4, 5], [5, 12, 13], [8, 15, 17], [7, 24, 25]]
    tp = j3pShuffle(tp)
    const u = tp.pop()
    stor.l3 = u.pop()
    stor.l2 = u.pop()
    stor.l1 = u.pop()
    const i = j3pGetRandomInt(1, 11)
    stor.l1 = stor.l1 * i
    stor.l2 = stor.l2 * i
    stor.l3 = stor.l3 * i
    if (!stor.Lexo.sam) {
      stor.l1 += 1
    }
    if (!ds.Entiers) {
      stor.l1 = j3pArrondi(stor.l1 / 10, 1)
      stor.l2 = j3pArrondi(stor.l2 / 10, 1)
      stor.l3 = j3pArrondi(stor.l3 / 10, 1)
    }

    switch (j3pGetRandomInt(0, 2)) {
      case 1:
        stor.lc1 = stor.l1
        stor.lc2 = stor.l2
        stor.lc3 = stor.l3
        stor.cotelong = stor.nomcote3
        stor.noml3 = stor.nomlc3
        stor.noml1 = stor.nomlc2
        stor.noml2 = stor.nomlc1
        stor.c1 = stor.nomcote1
        stor.c2 = stor.nomcote2
        stor.lepo = lettres[0]
        stor.lepo2 = lettres[1]
        break
      case 2:
        stor.lc1 = stor.l3
        stor.lc2 = stor.l2
        stor.lc3 = stor.l1
        stor.cotelong = stor.nomcote1
        stor.noml3 = stor.nomlc1
        stor.noml1 = stor.nomlc3
        stor.noml2 = stor.nomlc2
        stor.c1 = stor.nomcote3
        stor.c2 = stor.nomcote2
        stor.lepo = lettres[2]
        stor.lepo2 = lettres[1]
        break
      case 0:
        stor.lc1 = stor.l2
        stor.lc2 = stor.l3
        stor.lc3 = stor.l1
        stor.cotelong = stor.nomcote2
        stor.noml3 = stor.nomlc2
        stor.noml1 = stor.nomlc1
        stor.noml2 = stor.nomlc3
        stor.c1 = stor.nomcote1
        stor.c2 = stor.nomcote3
        stor.lepo = lettres[1]
        stor.lepo2 = lettres[0]
    }

    stor.svgId = j3pGetNewId('mtg32')
    svgId = stor.svgId
    const hjk = addDefaultTable(stor.lesdiv.figure, 1, 1)[0][0]
    const txtFigure = 'TWF0aEdyYXBoSmF2YTEuMAAAABM+TMzNAAJmcv###wEA#wEAAAAAAAAAAANLAAACfwAAAQEAAAAAAAAAAQAAABz#####AAAAAQAKQ0NhbGNDb25zdAD#####AAJwaQAWMy4xNDE1OTI2NTM1ODk3OTMyMzg0Nv####8AAAABAApDQ29uc3RhbnRlQAkh+1RELRj#####AAAAAQAKQ1BvaW50QmFzZQD#####AQAAAAAOAAFVAMAkAAAAAAAAQBAAAAAAAAAAAAUAAEAv#nbItDlYQC#+dsi0OVj#####AAAAAQAUQ0Ryb2l0ZURpcmVjdGlvbkZpeGUA#####wEAAAAAEAAAAQAAAAEAAAABAT#wAAAAAAAA#####wAAAAEAD0NQb2ludExpZURyb2l0ZQD#####AQAAAAAOAAFWAMAAAAAAAAAAQBAAAAAAAAAAAAUAAUA##nbItDlYAAAAAv####8AAAABAAhDU2VnbWVudAD#####AQAAAAAQAAABAAAAAQAAAAEAAAAD#####wAAAAEAB0NNaWxpZXUA#####wEAAAAAEAAAAAAAAAAAAAAAQAgAAAAAAAAAAAUAAAAAAQAAAAP#####AAAAAgAMQ0NvbW1lbnRhaXJlAP####8BAAAAAAAAAAAAAAAAQBgAAAAAAAAAAAAAAAUMAAAAAAABAAAAAAAAAAEAAAAAAAAAAAABMf####8AAAABAAlDTG9uZ3VldXIA#####wAAAAEAAAAD#####wAAAAEAB0NDYWxjdWwA#####wABQQABMQAAAAE#8AAAAAAAAAAAAAkA#####wABQgABMgAAAAFAAAAAAAAAAAAAAAIA#####wH#AAAAEAAAAAAAAAAAAAAAQAgAAAAAAAAAAAgAAUBbAAAAAAAAQD7hR64UeuAAAAACAP####8B#wAAABAAAAAAAAAAAAAAAEAIAAAAAAAAAAAIAAFAQAAAAAAAAUBp#Cj1wo9cAAAAAgD#####Af8AAAAQAAAAAAAAAAAAAABACAAAAAAAAAAACAABQGtAAAAAAABAaNwo9cKPXAAAAAUA#####wAAAAAAEAAAAQAAAAIAAAAKAAAACwAAAAUA#####wAAAAAAEAAAAQAAAAIAAAALAAAADAAAAAUA#####wAAAAAAEAAAAQAAAAIAAAAMAAAACgAAAAIA#####wEAAAAAEAAAAAAAAAAAAAAAQAgAAAAAAAAAAAgAAUBawAAAAAAAQCvCj1wo9cQAAAACAP####8BAAAAABAAAAAAAAAAAAAAAEAIAAAAAAAAAAAIAAFAKgAAAAAABEBqvCj1wo9cAAAAAgD#####AQAAAAAQAAAAAAAAAAAAAABACAAAAAAAAAAACAABQGzAAAAAAABAaRwo9cKPXAAAAAIA#####wEAAAAAEAAAAAAAAAAAAAAAQAgAAAAAAAAAAAgAAUBl4AAAAAAAQFo4UeuFHrgAAAACAP####8BAAAAABAAAAAAAAAAAAAAAEAIAAAAAAAAAAAIAAFATIAAAAAAAEBbuFHrhR64AAAAAgD#####AQAAAAAQAAAAAAAAAAAAAABACAAAAAAAAAAACAABQF7AAAAAAABAa3wo9cKPXAAAAAcA#####wAAAH8BAAFBAAAAEBAAAAAAAAEAAAABAAAAAQAAAAAAAAAAAAFBAAAABwD#####AAAAfwEAAUIAAAAREAAAAAAAAQAAAAEAAAABAAAAAAAAAAAAAUIAAAAHAP####8AAAB#AQABQwAAABIQAAAAAAABAAAAAQAAAAEAAAAAAAAAAAABQwAAAAcA#####wB#AH8BAAJsMQAAABMQAAAAAAABAAAAAQAAAAEAAAAAAAAAAAACbDEAAAAHAP####8AfwB#AQACbDIAAAAUEAAAAAAAAgAAAAEAAAABAAAAAAAAAAAAAmwyAAAABwD#####AH8AfwEAAmwzAAAAFRAAAAAAAAEAAAABAAAAAQAAAAAAAAAAAAJsMwAAAAf##########w=='
    const yy = j3pCreeSVG(hjk, { id: svgId, width: 250, height: 240 })
    yy.style.border = '1px solid black'
    stor.mtgAppLecteur.removeAllDoc()
    stor.mtgAppLecteur.addDoc(svgId, txtFigure, true)
    stor.mtgAppLecteur.setText(svgId, '#A', stor.lettres[0], true)
    stor.mtgAppLecteur.setText(svgId, '#B', stor.lettres[1], true)
    stor.mtgAppLecteur.setText(svgId, '#C', stor.lettres[2], true)
    stor.mtgAppLecteur.setText(svgId, '#l1', String(stor.lc2), true)
    stor.mtgAppLecteur.setText(svgId, '#l2', String(stor.lc1), true)
    stor.mtgAppLecteur.setText(svgId, '#l3', String(stor.lc3), true)
    stor.mtgAppLecteur.calculateAndDisplayAll(true)
  }

  function initSection () {
    let i
    initCptPe(ds, stor)

    // Construction de la page
    me.construitStructurePage('presentation1bis')

    me.donneesSection.lesExos = []

    let lp = ['+', '=']
    lp = j3pShuffle(lp)

    for (i = 0; i < me.donneesSection.nbitems; i++) {
      me.donneesSection.lesExos.push({ prop: lp[i % (lp.length)] })
    }
    me.donneesSection.lesExos = j3pShuffle(me.donneesSection.lesExos)

    me.typederreurs = [0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0]

    me.afficheTitre('rectangle ou non ?')
    getMtgCore({ withMathJax: true })
      .then(
        // success
        (mtgAppLecteur) => {
          stor.mtgAppLecteur = mtgAppLecteur
          enonceMain()
        },
        // failure
        (error) => {
          j3pShowError(error, { message: 'mathgraph n’est pas correctement chargé', mustNotify: true })
        })
      // plantage dans enonceMain
      .catch(j3pShowError)
  }

  function yaReponse () {
    if (me.isElapsed) return true
    if (stor.encours === 'Plusgrand') {
      if (!stor.liste1.changed) {
        stor.liste1.focus()
        return false
      }
      return true
    }
    if (stor.encours === 'compare') {
      if (!stor.liste2.changed) {
        stor.liste2.focus()
        return false
      }
      return true
    }
    if (stor.encours === 'calcul') {
      if (stor.zgauche.reponse() === '') {
        stor.zgauche.focus()
        return false
      }
      if (stor.zgauche2.reponse() === '') {
        stor.zgauche2.focus()
        return false
      }
      if (stor.zdroite1.reponse() === '') {
        stor.zdroite1.focus()
        return false
      }
      if (stor.zdroite2.reponse() === '') {
        stor.zdroite2.focus()
        return false
      }
      if (stor.zdroite4.reponse() === '') {
        stor.zdroite4.focus()
        return false
      }
      if (stor.zdroite5.reponse() === '') {
        stor.zdroite5.focus()
        return false
      }
      if (stor.zdroite3.reponse() === '') {
        stor.zdroite3.focus()
        return false
      }
      return true
    }
    if (stor.encours === 'concl') {
      if (!stor.liste3.changed) {
        stor.liste3.focus()
        return false
      }
      if (!stor.liste5.changed) {
        stor.liste5.focus()
        return false
      }
      if (!stor.liste4.changed) {
        stor.liste4.focus()
        return false
      }
      return true
    }
  }

  function isRepOk () {
    stor.errGrand = false
    stor.errComp = false
    stor.errCopie = false
    stor.errAdd = false
    stor.errDouble = false
    stor.errCarre = false
    stor.errProp = false

    let ok = true
    let t1, t2, t3, o1, o2
    o1 = o2 = false
    if (stor.encours === 'Plusgrand') {
      switch (stor.liste1.reponse) {
        case stor.nomcote1:
          stor.t1 = stor.lc1
          break
        case stor.nomcote2:
          stor.t1 = stor.lc2
          break
        case stor.nomcote3:
          stor.t1 = stor.lc3
          break
      }
      if (stor.t1 !== Math.max(stor.lc1, stor.lc2, stor.lc3)) {
        stor.errGrand = true
        stor.compteurPe1++
        stor.liste1.corrige(false)
        return false
      }
      return true
    }
    if (stor.encours === 'compare') {
      if (stor.liste2.reponse.substring(0, 2) !== stor.cotelong.substring(1, 3)) {
        stor.errComp = true
        stor.liste2.corrige(false)
        return false
      }
      return true
    }
    if (stor.encours === 'calcul') {
      t3 = [(stor.l3 + '').replace('.', ','), (stor.l2 + '').replace('.', ','), (stor.l1 + '').replace('.', ',')]
      t1 = stor.zgauche.reponse()
      if (t1 !== (stor.l3 + '').replace('.', ',')) {
        stor.zgauche.corrige(false)
        // pour plus tard, verif 0 inut
        if (t3.indexOf(t1) === -1) {
          stor.errCopie = true
        } else {
          stor.errInv = true
        }
        ok = false
      }
      t1 = stor.zgauche2.reponse()
      if (t1 !== (j3pArrondi(stor.l3 * stor.l3, 2) + '').replace('.', ',')) {
        stor.zgauche2.corrige(false)
        // pour plus tard, verif 0 inut
        if (!stor.errCopie && !stor.errInv) {
          stor.errCarre = true
        }
        ok = false
      }
      t1 = stor.zdroite1.reponse()
      if ((t1 !== (stor.l2 + '').replace('.', ',')) && t1 !== (stor.l1 + '').replace('.', ',')) {
        stor.zdroite1.corrige(false)
        o1 = true
        // pour plus tard, verif 0 inut
        if (t3.indexOf(t1) === -1) {
          stor.errCopie = true
        } else {
          stor.errInv = true
        }
        ok = false
      }
      t2 = stor.zdroite2.reponse()
      if ((t2 !== (stor.l2 + '').replace('.', ',')) && t2 !== (stor.l1 + '').replace('.', ',')) {
        stor.zdroite2.corrige(false)
        o2 = true
        // pour plus tard, verif 0 inut
        if (t3.indexOf(t2) === -1) {
          stor.errCopie = true
        } else {
          stor.errInv = true
        }
        ok = false
      }
      if (t1 === t2) {
        if (stor.l1 !== stor.l2) {
          stor.errDouble = true
          stor.zdroite2.corrige(false)
          o1 = o2 = true
          ok = false
        }
      }

      t1 = stor.zdroite4.reponse()
      if ((t1 !== (j3pArrondi(stor.l2 * stor.l2, 2) + '').replace('.', ',')) && t1 !== (j3pArrondi(stor.l1 * stor.l1, 2) + '').replace('.', ',')) {
        stor.zdroite4.corrige(false)
        // pour plus tard, verif 0 inut
        if (!o1 && !o2) {
          stor.errCarre = true
        }
        ok = false
      }
      t2 = stor.zdroite5.reponse()
      if ((t2 !== (j3pArrondi(stor.l2 * stor.l2, 2) + '').replace('.', ',')) && t2 !== (j3pArrondi(stor.l1 * stor.l1, 2) + '').replace('.', ',')) {
        stor.zdroite5.corrige(false)
        o2 = true
        // pour plus tard, verif 0 inut
        if (!o1 && !o2) {
          stor.errCarre = true
        }
        ok = false
      }
      if (t1 === t2) {
        if (stor.l1 !== stor.l2) {
          stor.errDouble = true
          stor.zdroite5.corrige(false)
          ok = false
        }
      }
      t2 = parseFloat(stor.zdroite3.reponse().replace(',', '.'))
      if (t2 !== j3pArrondi(stor.l1 * stor.l1 + stor.l2 * stor.l2, 2)) {
        stor.zdroite3.corrige(false)
        // pour plus tard, verif 0 inut
        if (ok) stor.errAdd = true
        ok = false
      }
      return ok
    }
    if (stor.encours === 'concl') {
      t1 = (stor.liste3.reponse.indexOf('=') === -1)
      if ((t1 && stor.Lexo.sam) || (!t1 && !stor.Lexo.sam)) {
        stor.liste3.corrige(false)
        stor.compteurPe1++
        return false
      }
      t1 = stor.liste5.reponse
      t1 = (stor.liste5.reponse.indexOf('pas') !== -1)
      if ((t1 && stor.Lexo.sam) || (!t1 && !stor.Lexo.sam)) {
        stor.liste5.corrige(false)
        stor.errProp = true
        return false
      }
      t1 = (stor.liste4.reponse.indexOf('pas') !== -1)
      if ((t1 && stor.Lexo.sam) || (!t1 && !stor.Lexo.sam)) {
        stor.liste4.corrige(false)
        return false
      }
      if (!stor.Lexo.sam) return true
      if (stor.liste4.reponse.indexOf(stor.lepo) === -1) {
        stor.liste4.corrige(false)
        return false
      } else { return true }
    }
    return ok
  } // isRepOk

  function desactiveAll () {
    if (stor.encours === 'Plusgrand') {
      stor.liste1.disable()
    }
    if (stor.encours === 'compare') {
      stor.liste2.disable()
    }
    if (stor.encours === 'calcul') {
      stor.zgauche.disable()
      stor.zgauche2.disable()
      stor.zdroite1.disable()
      stor.zdroite2.disable()
      stor.zdroite3.disable()
      stor.zdroite4.disable()
      stor.zdroite5.disable()
    }
    if (stor.encours === 'concl') {
      stor.liste3.disable()
      stor.liste4.disable()
      stor.liste5.disable()
    }
  } // desactiveAll

  function passeToutVert () {
    if (stor.encours === 'Plusgrand') {
      stor.liste1.corrige(true)
    }
    if (stor.encours === 'compare') {
      stor.liste2.corrige(true)
    }
    if (stor.encours === 'calcul') {
      stor.zgauche.corrige(true)
      stor.zgauche2.corrige(true)
      stor.zdroite1.corrige(true)
      stor.zdroite2.corrige(true)
      stor.zdroite3.corrige(true)
      stor.zdroite4.corrige(true)
      stor.zdroite5.corrige(true)
    }
    if (stor.encours === 'concl') {
      stor.liste3.corrige(true)
      stor.liste4.corrige(true)
      stor.liste5.corrige(true)
    }
  } // passeToutVert
  function barrelesfo () {
    if (stor.encours === 'calcul') {
      stor.zgauche.barreIfKo()
      stor.zgauche2.barreIfKo()
      stor.zdroite1.barreIfKo()
      stor.zdroite2.barreIfKo()
      stor.zdroite3.barreIfKo()
      stor.zdroite4.barreIfKo()
      stor.zdroite5.barreIfKo()
    }
    if (stor.encours === 'concl') {
      stor.liste3.barreIfKo()
      stor.liste4.barreIfKo()
      stor.liste5.barreIfKo()
    }
  }
  function affCorrFaux (isFin) {
    if (stor.errGrand) {
      j3pAffiche(stor.lesdiv.explications, null, 'Il y a un côté de longueur supérieure à ' + (stor.t1 + '').replace('.', ',') + ' cm  !\n')
      stor.liste1.corrige(false)
      stor.yaexplik = true
    }
    if (stor.errComp) {
      j3pAffiche(stor.lesdiv.explications, null, 'Il faut comparer le carré de la longueur du plus grand côté \nà la somme des carrés des longueurs des deux autres côtés!\n')
      stor.liste2.corrige(false)
      stor.yaexplik = true
    }
    if (stor.errCopie) {
      j3pAffiche(stor.lesdiv.explications, null, 'Une longueur n’est pas dans l’énoncé !\n')
      stor.yaexplik = true
    }
    if (stor.errInv) {
      j3pAffiche(stor.lesdiv.explications, null, 'Tu utilises la longueur d’un autre côté !\n')
      stor.yaexplik = true
    }
    if (stor.errDouble) {
      j3pAffiche(stor.lesdiv.explications, null, 'Tu écris deux fois la même longueur !\n')
      stor.yaexplik = true
    }
    if (stor.errAdd) {
      j3pAffiche(stor.lesdiv.explications, null, 'Une erreur de calcul !\n')
      stor.yaexplik = true
      stor.compteurPe2++
    }
    if (stor.errCarre) {
      j3pAffiche(stor.lesdiv.explications, null, 'Multiplie le nombre par lui-même por obtenir son carré !\n')
      stor.yaexplik = true
      stor.compteurPe3++
    }
    if (stor.errProp) {
      j3pAffiche(stor.lesdiv.explications, null, 'Vérifie si les membres sont bien égaux !\n')
      stor.yaexplik = true
    }

    if (isFin) {
      // barre les faux
      barrelesfo()
      desactiveAll()
      stor.yaco = true
      // afficheco
      if (stor.encours === 'Plusgrand') {
        j3pAffiche(stor.lesdiv.solution, null, stor.cotelong + ' est le plus grand côté.')
        stor.liste1.barre()
      }
      if (stor.encours === 'compare') {
        j3pAffiche(stor.lesdiv.solution, null, 'Je compare ' + stor.noml3 + '² et ' + stor.noml2 + '² + ' + stor.noml1 + '²')
        stor.liste2.barre()
      }
      if (stor.encours === 'calcul') {
        const tttabco = addDefaultTable(stor.lesdiv.solution, 1, 3)
        tttabco[0][1].style.background = '#000000'
        tttabco[0][1].style.width = '1px'
        j3pAffiche(tttabco[0][0], null, stor.noml3 + '² = $' + (stor.l3 + '').replace('.', ',').replace('.', ',') + '²$ \n')
        j3pAffiche(tttabco[0][0], null, stor.noml3 + '² = $' + (j3pArrondi(stor.l3 * stor.l3, 2) + '').replace('.', ',').replace('.', ',') + '$ cm²')
        j3pAffiche(tttabco[0][2], null, stor.noml1 + '² + ' + stor.noml2 + '² = $' + (stor.l1 + '² + ' + stor.l2 + '²').replace('.', ',').replace('.', ',') + '$  \n')
        j3pAffiche(tttabco[0][2], null, stor.noml1 + '² + ' + stor.noml2 + '² = $' + (j3pArrondi(stor.l1 * stor.l1, 2) + ' + ' + j3pArrondi(stor.l2 * stor.l2, 2)).replace('.', ',').replace('.', ',') + '$  \n')
        j3pAffiche(tttabco[0][2], null, stor.noml1 + '² + ' + stor.noml2 + '² = $' + (j3pArrondi(stor.l1 * stor.l1 + stor.l2 * stor.l2, 2) + '').replace('.', ',') + '$ cm²')
      }
      if (stor.encours === 'concl') {
        if (stor.Lexo.sam) {
          j3pAffiche(stor.lesdiv.solution, null, stor.noml3 + '²$ = $' + stor.noml1 + '² + ' + stor.noml2 + '² \n')
          j3pAffiche(stor.lesdiv.solution, null, 'L’égalité de Pythagore est vérifiée, \n')
          j3pAffiche(stor.lesdiv.solution, null, 'Donc le triangle ' + stor.nomtri + ' est rectangle en ' + stor.lepo + ' .')
        } else {
          j3pAffiche(stor.lesdiv.solution, null, stor.noml3 + '²$ \\neq $' + stor.noml1 + '² + ' + stor.noml2 + '² \n')
          j3pAffiche(stor.lesdiv.solution, null, 'L’égalité de Pythagore n’est pas vérifiée, \n')
          j3pAffiche(stor.lesdiv.solution, null, 'donc le triangle ' + stor.nomtri + ' n’est pas rectangle.')
        }
      }
    } else { me.afficheBoutonValider() }
  } // affCorrFaux
  function enonceMain () {
    if (me.etapeCourante === 1) {
      // A chaque répétition, on nettoie la scène
      // A commenter si besoin
      me.videLesZones()
    }

    creeLesDiv()

    stor.Lexo = faisChoixExos()

    stor.lesdiv.solution.style.color = me.styles.toutpetit.correction.color
    stor.lesdiv.explications.style.color = me.styles.cfaux

    stor.lesdiv.travail.classList.add('travail')
    stor.lesdiv.consigneG.classList.add('enonce')
    stor.lesdiv.figure.classList.add('enonce')
    me.zonesElts.MG.classList.add('fond')
    faiFigure()

    poseQuestion()
    if (ds.Calculatrice) {
      me.fenetresjq = [{ name: 'Calculatrice', title: 'Une calculatrice', left: 600, top: 10 }]
      j3pCreeFenetres(me)
      j3pAfficheCroixFenetres('Calculatrice')
      j3pAjouteBouton(stor.lesdiv.calculatrice, boutonfen, { className: 'MepBoutons', value: 'Calculatrice' })
    }
    me.afficheBoutonValider()
    me.finEnonce()
  }

  switch (this.etat) {
    case 'enonce':
      // code exécuté au lancement de la section
      if (me.debutDeLaSection) {
        initSection()
      } else {
        enonceMain()
      }
      break // case "enonce":

    case 'correction':
      // On teste si une réponse a été saisie
      stor.yaexplik = false
      stor.yaco = false
      stor.lesdiv.explications.innerHTML = ''
      stor.lesdiv.solution.innerHTML = ''

      if (yaReponse()) {
        // Bonne réponse
        if (isRepOk()) {
          this._stopTimer()
          stor.lesdiv.correction.style.color = this.styles.cbien
          stor.lesdiv.correction.innerHTML = cBien

          passeToutVert()
          desactiveAll()
          if (stor.encours === 'Plusgrand') {
            poseQuestCompare()
            return
          }
          if (stor.encours === 'compare') {
            poseCalcul()
            return
          }
          if (stor.encours === 'calcul') {
            poseConclu()
            return
          }
          stor.encours = ''

          this.score++
          this.typederreurs[0]++
          this.cacheBoutonValider()
          this.etat = 'navigation'
          this.sectionCourante()
        } else {
          // Pas de bonne réponse
          stor.lesdiv.correction.style.color = this.styles.cfaux
          stor.lesdiv.correction.innerHTML = cFaux
          if (this.isElapsed) {
            // A cause de la limite de temps :
            this._stopTimer()

            stor.lesdiv.correction.innerHTML += tempsDepasse + '<BR>'
            this.typederreurs[10]++
            this.cacheBoutonValider()

            affCorrFaux(true)
            stor.yaco = true

            this.etat = 'navigation'
            this.sectionCourante()
          } else {
            // Réponse fausse :
            if (me.essaiCourant < me.donneesSection.nbchances) {
              stor.lesdiv.correction.innerHTML += '<br>' + essaieEncore

              affCorrFaux(false)

              // affCorrFaux.call(this, false)
              this.typederreurs[1]++
            } else {
              // Erreur au nème essai
              this._stopTimer()
              this.cacheBoutonValider()

              affCorrFaux(true)
              stor.yaco = true

              if (stor.encours === 'compare') {
                this.score = j3pArrondi(this.score + 0.1, 1)
              }
              if (stor.encours === 'calcul') {
                this.score = j3pArrondi(this.score + 0.4, 1)
              }
              if (stor.encours === 'concl') {
                this.score = j3pArrondi(this.score + 0.7, 1)
              }

              this.typederreurs[2]++
              this.etat = 'navigation'
              this.sectionCourante()
            }
          }
        }
      } else {
        // pas de réponse
        stor.lesdiv.correction.style.color = this.styles.cfaux
        stor.lesdiv.correction.innerHTML = reponseIncomplete
        this.afficheBoutonValider()
      }

      if (stor.yaexplik) {
        stor.lesdiv.explications.classList.add('explique')
      }
      if (stor.yaco) {
        stor.lesdiv.solution.classList.add('correction')
      }

      // Obligatoire
      this.finCorrection()
      break // case "correction":

    case 'navigation':
      if (this.sectionTerminee()) {
        this.parcours.pe = getPe(ds, stor)
      } else {
        this.etat = 'enonce'
      }
      this.finNavigation()
      break // case "navigation":
  }
}
