import { j3pAddElt, j3pAjouteBouton, j3pArrondi, j3pDetruit, j3pEmpty, j3pGetRandomBool, j3pGetRandomInt, j3pShowError, j3pShuffle } from 'src/legacy/core/functions'
import { j3pCreeFenetres, j3pAfficheCroixFenetres, j3pToggleFenetres } from 'src/legacy/core/functionsJqDialog'
import { j3pCreeSVG } from 'src/legacy/core/functionsSvg'
import BoutonStyleMathquill from 'src/legacy/outils/boutonsStyleMatquill'
import Paint from 'src/legacy/outils/brouillon/Paint'
import BrouillonCalculs from 'src/legacy/outils/brouillon/BrouillonCalculs'
import ListeDeroulante from 'src/legacy/outils/listeDeroulante/ListeDeroulante'
import { estNompoint, estNomTriangle } from 'src/legacy/outils/zoneStyleMathquill/functions'
import { verifNombreBienEcrit } from 'src/legacy/outils/fonctions/ecritureNombre'
import ZoneStyleMathquill1 from 'src/legacy/outils/zoneStyleMathquill/ZoneStyleMathquill1'
import ZoneStyleMathquill2 from 'src/legacy/outils/zoneStyleMathquill/ZoneStyleMathquill2'
import { getPe, initCptPe } from 'src/legacy/sections/college/helperTommy'
import { addDefaultTable } from 'src/legacy/themes/table'
import { j3pAffiche } from 'src/lib/mathquill/functions'
import { getMtgCore } from 'src/lib/outils/mathgraph'
import { addMaj } from 'src/lib/utils/string'
import textesGeneriques from 'src/lib/core/textes'

const { cBien, cFaux, essaieEncore, reponseIncomplete, tempsDepasse } = textesGeneriques

const voidFct = () => undefined
const returnSpace = () => ' '

// @todo virer ça
// un truc qui ressemble à une ZoneStyleMathquill (pour éviter que le code plante quand un des zoneRx n’existe pas
const zsmFake = { disable: voidFct, corrige: voidFct, barreIfKo: voidFct, reponse: returnSpace }

/**
 * Ajouter une description ici
 * @fileOverview
 * @author Tommy Barroy <t.barroy@laposte.net>
 * @since 2017-10
 */

// FIXME lorsque l'on demande une approx au millième / centième, si le dernier chiffre est un 0 il est refusé (il faut supprimer les zéros inutiles, mais en cas d'approx le dernier 0 n'est pas inutile, il donne la précision)
// par ex racine(125) au millième => 11,180 et le dernier 0 est refusé

export const params = {
  outils: ['mathquill', 'calculatrice'],
  /**
   * Ce qui est paramétrable dans l’éditeur de graphe
   * @type {LegacySectionParamsParametres}
   */
  parametres: [
    ['nbrepetitions', 3, 'entier', 'Nombre de répétitions de la section'],
    ['nbchances', 2, 'entier', 'Nombre d’essais possibles.'],
    ['Cote', 'les deux', 'liste', '<u>hypothénuse</u>:  L’exercice propose le calcul de l’hypoténuse. <br><br> <u>angle_droit</u> L’exercice propose le calcul d’un côté de l’angle droit.', ['hypoténuse', 'angle_droit', 'les deux']],
    ['Enonce', 'les deux', 'liste', '<u>Figure</u>:  L’énoncé est présenté sur une figure <br><br> <u>Texte</u> L’énoncé est un texte.', ['Figure', 'Texte', 'les deux']],
    ['Inutiles', 'les deux', 'liste', '<u>Oui</u>:  L’énoncé comporte des données inutiles.', ['Oui', 'Non', 'les deux']],
    ['Brouillon', true, 'boolean', '<u>true</u>:  L’élève dispose d’un brouillon quand l’énoncé est un texte.'],
    ['Description', true, 'boolean', '<u>true</u>:  L’élève doit renseigner les conditions'],
    ['Egalite', true, 'boolean', '<u>true</u>:  L’élève doit écrire l’égalité.'],
    ['Remplacement', true, 'boolean', '<u>true</u>:  L’élève doit remplacer les longuers par leur valeur.'],
    ['carre', true, 'boolean', '<u>true</u>:  L’élève doit donner le carré de longueur cherchée.'],
    ['racine', true, 'boolean', '<u>true</u>:  Le passage à la racine est évalué.'],
    ['reponse', true, 'boolean', '<u>true</u>:  L’élève doit donner la réponse.'],
    ['Entiers', 'les deux', 'liste', '<u>Oui</u>:  Les longueurs sont des nombres entiers.', ['Oui', 'Non', 'les deux']],
    ['Exacte', 'les deux', 'liste', '<u>Oui</u>:  La longueur recherchée tombe juste.', ['Oui', 'Non', 'les deux']],
    ['Calculatrice', true, 'boolean', '<u>true</u>:  Une calculatrice est disponible'],
    ['Val_Ap', true, 'boolean', "<u>true</u>:  L’expression 'valeur approchée' remplace 'approximation décimale'."],
    ['Sans_Echec', true, 'boolean', '<u>true</u>:  L’élève doit réussir chaque étape pour terminer l’exercice.'],
    // le modèle gère la compatibilité ascendante avec l’ancien paramètre MiseEnPagePlusClaire
    ['theme', 'standard', 'liste', 'Modèle pour l’affichage', ['standard', 'zonesAvecImageDeFond']]

  ],
  // dans le cas d’une section QUANTITATIVE
  pe: [
    { pe_1: 'Conditions' },
    { pe_2: 'Egalite' },
    { pe_3: 'Calculs' }
  ]
}

/**
 * section pyth01
 * @this {Parcours}
 */
export default function main () {
  const me = this
  const ds = this.donneesSection
  const stor = this.storage

  function initSection () {
    let i
    stor.encours = 'reponse'
    initCptPe(ds, stor)

    me.validOnEnter = false // ex donneesSection.touche_entree

    // Construction de la page
    me.construitStructurePage('presentation3')

    let buft = 'Calculer une longueur.'
    if (!ds.reponse && !ds.racine) buft = 'Calculer le carré d’une longueur'
    if (ds.Egalite && !ds.Remplacement && !ds.carre && !ds.racine && !ds.reponse) buft = 'Ecrire l’égalité'
    if (!ds.Egalite && !ds.Remplacement && !ds.carre && !ds.racine && !ds.reponse) buft = 'Pour débuter'

    stor.listencours = []
    if (ds.Description) stor.listencours.push('Descrip')
    if (ds.Egalite) stor.listencours.push('egalite')
    if (ds.Remplacement) stor.listencours.push('rempli')
    if (ds.carre) stor.listencours.push('carre')
    if (ds.racine) stor.listencours.push('racine')
    if (ds.reponse) stor.listencours.push('reponse')
    if (stor.listencours.length === 0) {
      j3pShowError('Erreur de paramétrage')
      stor.listencours = ['Descrip', 'egalite', 'rempli', 'carre', 'racine', 'reponse']
      buft = 'Calculer une longueur.'
    }

    // ça impose le nb d'étapes
    me.surcharge({ nbetapes: stor.listencours.length })

    const exos = []
    let cotes = []
    if ((ds.Cote === 'hypoténuse') || (ds.Cote === 'les deux')) cotes.push(0)
    if ((ds.Cote === 'angle_droit') || (ds.Cote === 'les deux')) {
      cotes.push(1)
      cotes.push(2)
    }
    cotes = j3pShuffle(cotes)
    for (i = 0; i < ds.nbitems; i++) {
      exos.push({ cote: cotes[i % (cotes.length)] })
    }

    let enonces = []
    if ((ds.Enonce === 'Figure') || (ds.Enonce === 'les deux')) enonces.push('fig')
    if ((ds.Enonce === 'Texte') || (ds.Enonce === 'les deux')) enonces.push('text')
    enonces = j3pShuffle(enonces)
    for (i = 0; i < exos.length; i++) {
      exos[i].enonce = enonces[i % (enonces.length)]
    }

    let donnes = []
    if ((ds.Inutiles === 'Non') || (ds.Inutiles === 'les deux')) donnes.push(0)
    if ((ds.Inutiles === 'Oui') || (ds.Inutiles === 'les deux')) {
      donnes.push(1)
      donnes.push(2)
    }
    donnes = j3pShuffle(donnes)
    for (i = 0; i < exos.length; i++) {
      exos[i].donnes = donnes[i % donnes.length]
    }

    let entiers = []
    if ((ds.Entiers === 'Oui') || (ds.Entiers === 'les deux')) entiers.push(true)
    if ((ds.Entiers === 'Non') || (ds.Entiers === 'les deux')) entiers.push(false)
    entiers = j3pShuffle(entiers)
    for (i = 0; i < exos.length; i++) {
      exos[i].entiers = entiers[i % entiers.length]
    }

    const rangs = j3pShuffle([0, 1, 2, 3])
    for (i = 0; i < exos.length; i++) {
      exos[i].rang = rangs[i % rangs.length]
    }

    let exacts = []
    if ((ds.Exacte === 'Oui') || (ds.Exacte === 'les deux')) exacts.push(true)
    if ((ds.Exacte === 'Non') || (ds.Exacte === 'les deux')) exacts.push(false)
    exacts = j3pShuffle(exacts)
    for (i = 0; i < exos.length; i++) {
      exos[i].exact = exacts[i % exacts.length]
    }

    // on mélange et on stocke ça
    stor.exos = j3pShuffle(exos)
    me.typederreurs = [0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0]
    me.afficheTitre(buft)

    // chargement mtg
    getMtgCore({ withMathJax: true })
      .then(
        // success
        (mtgAppLecteur) => {
          stor.mtgAppLecteur = mtgAppLecteur
          enonceMain()
        },
        // failure
        (error) => {
          j3pShowError(error, { message: 'mathgraph n’est pas correctement chargé', mustNotify: true })
        })
      // plantage dans enonceMain
      .catch(j3pShowError)
  }

  function faisChoixExos () {
    const exo = stor.exos.pop()
    stor.deb = stor.deb1 = stor.deb2 = stor.deb3 = stor.deb4 = stor.deb5 = stor.foco = false
    const nomspris = []
    stor.nA = addMaj(nomspris)
    stor.nB = addMaj(nomspris)
    stor.nC = addMaj(nomspris)
    stor.nD = addMaj(nomspris)
    stor.rotation = j3pGetRandomInt(0, 360)
    stor.listBarre = []

    if (exo.exact) {
      let list = [[3, 4, 5], [5, 12, 13], [12, 8, 17], [7, 24, 25], [21, 20, 29]]
      list = list[j3pGetRandomInt(0, 4)]
      switch (exo.cote) {
        case 0:
          stor.AB = 0
          stor.AC = list[0]
          stor.BC = list[1]
          break
        case 1:
          stor.AB = list[2]
          stor.AC = 0
          stor.BC = list[1]
          break
        case 2:
          stor.AB = list[2]
          stor.AC = list[0]
          stor.BC = 0
      }
    } else {
      switch (exo.cote) {
        case 0:
          do {
            stor.AC = j3pGetRandomInt(1, 11)
            stor.BC = j3pGetRandomInt(1, 11)
          } while (Math.abs(Math.sqrt(stor.AC * stor.AC + stor.BC * stor.BC) - Math.floor(Math.sqrt(stor.AC * stor.AC + stor.BC * stor.BC))) < 0.01)
          stor.AB = 0
          break
        case 1:
          do {
            stor.AB = j3pGetRandomInt(12, 30)
            stor.AC = 0
            stor.BC = j3pGetRandomInt(1, 11)
          }
          while (Math.abs(Math.sqrt(stor.AB * stor.AB - stor.BC * stor.BC) - Math.floor(Math.sqrt(stor.AB + stor.AB - stor.BC * stor.BC))) < 0.01)
          break
        case 2:
          do {
            stor.AB = j3pGetRandomInt(12, 30)
            stor.AC = j3pGetRandomInt(1, 11)
          }
          while (Math.abs(Math.sqrt(stor.AB * stor.AB - stor.AC * stor.AC) - Math.floor(Math.sqrt(stor.AB * stor.AB - stor.AC * stor.AC))) < 0.01)
          stor.BC = 0
      }
    }
    stor.DA = j3pGetRandomInt(3, stor.AB - 1)
    if (exo.cote === 0) stor.DA = j3pGetRandomInt(3, Math.sqrt(stor.AC * stor.AC + stor.BC * stor.BC) - 1)
    if (!exo.entiers) {
      stor.AB = j3pArrondi(stor.AB / 10, 1)
      stor.AC = j3pArrondi(stor.AC / 10, 1)
      stor.BC = j3pArrondi(stor.BC / 10, 1)
      stor.DA = j3pArrondi(stor.DA / 10, 1)
    }
    switch (exo.cote) {
      case 0:
        stor.v2 = 2 * Math.acos(stor.DA / Math.sqrt(stor.AC * stor.AC + stor.BC * stor.BC)) * 180 / Math.PI
        stor.v1 = 2 * Math.acos(stor.AC / Math.sqrt(stor.AC * stor.AC + stor.BC * stor.BC)) * 180 / Math.PI
        stor.rech = stor.nA + stor.nB
        stor.carrBon = j3pArrondi(stor.AC * stor.AC + stor.BC * stor.BC, 2)
        stor.carrFo = [j3pArrondi(2 * stor.AC + stor.BC * stor.BC, 2)]
        stor.carrFo.push(j3pArrondi(2 * stor.AC + 2 * stor.BC, 2))
        stor.carrFo.push(j3pArrondi(stor.AC * stor.AC + 2 * stor.BC, 2))
        break
      case 1:
        stor.v2 = 2 * Math.acos(stor.DA / stor.AB) * 180 / Math.PI
        stor.v1 = 2 * Math.acos(Math.sqrt(stor.AB * stor.AB - stor.BC * stor.BC) / stor.AB) * 180 / Math.PI
        stor.rech = stor.nA + stor.nC
        stor.carrBon = j3pArrondi(stor.AB * stor.AB - stor.BC * stor.BC, 2)
        stor.carrFo = [j3pArrondi(2 * stor.AB - stor.BC * stor.BC, 2)]
        stor.carrFo.push(j3pArrondi(2 * stor.AB - 2 * stor.BC, 2))
        stor.carrFo.push(j3pArrondi(stor.AB * stor.AB - 2 * stor.BC, 2))
        stor.lot = String(j3pArrondi(stor.BC * stor.BC, 2)).replace('.', ',')
        break
      case 2:
        stor.v2 = 2 * Math.acos(stor.DA / stor.AB) * 180 / Math.PI
        stor.v1 = 2 * Math.acos(stor.AC / stor.AB) * 180 / Math.PI
        stor.rech = stor.nB + stor.nC
        stor.carrBon = j3pArrondi(stor.AB * stor.AB - stor.AC * stor.AC, 2)
        stor.carrFo = [j3pArrondi(2 * stor.AB - stor.AC * stor.AC, 2)]
        stor.carrFo.push(j3pArrondi(2 * stor.AB - 2 * stor.AC, 2))
        stor.carrFo.push(j3pArrondi(stor.AB * stor.AB - 2 * stor.AC, 2))
        stor.lot = String(j3pArrondi(stor.AC * stor.AC, 2)).replace('.', ',')
    }
    stor.valBon = Math.sqrt(stor.carrBon)
    stor.v1 = Math.min(160, Math.max(20, stor.v1))
    stor.v2 = Math.min(160, Math.max(20, stor.v2))
    switch (j3pGetRandomInt(0, 5)) {
      case 0:
        stor.nomtri = stor.nA + stor.nB + stor.nC
        stor.nomtri2 = stor.nA + stor.nD + stor.nB
        break
      case 1:
        stor.nomtri = stor.nA + stor.nC + stor.nB
        stor.nomtri2 = stor.nA + stor.nB + stor.nD
        break
      case 2:
        stor.nomtri = stor.nB + stor.nA + stor.nC
        stor.nomtri2 = stor.nD + stor.nA + stor.nB
        break
      case 3:
        stor.nomtri = stor.nB + stor.nC + stor.nA
        stor.nomtri2 = stor.nD + stor.nB + stor.nA
        break
      case 4:
        stor.nomtri = stor.nC + stor.nB + stor.nA
        stor.nomtri2 = stor.nB + stor.nD + stor.nA
        break
      case 5:
        stor.nomtri = stor.nC + stor.nA + stor.nB
        stor.nomtri2 = stor.nB + stor.nA + stor.nD
        break
    }
    if (exo.donnees === 0) stor.DA = 0

    stor.tabFhs = [[]]
    stor.tabGfdd = [[]]
    stor.tabGGG = [[]]
    stor.tabA = [[]]
    stor.tabVkf = [[]]
    return exo
  }
  function creeLesDiv () {
    stor.lesdiv = {}
    stor.lesdiv.conteneur = j3pAddElt(me.zonesElts.MG, 'div', {
      style: me.styles.etendre('toutpetit.enonce', { padding: '10px' })
    })
    const tt = addDefaultTable(stor.lesdiv.conteneur, 1, 5)
    j3pAffiche(tt[0][1], null, '&nbsp;&nbsp;&nbsp;')
    j3pAffiche(tt[0][3], null, '&nbsp;&nbsp;&nbsp;')
    stor.lesdiv.consigneG = tt[0][0]
    stor.lesdiv.calculatrice = tt[0][2]
    stor.lesdiv.correction = tt[0][4]
    const ff = addDefaultTable(stor.lesdiv.conteneur, 1, 3)
    stor.lesdiv.travail = ff[0][2]
    stor.lesdiv.figure = ff[0][0]
    ff[0][1].style.width = '20px'
    stor.lesdiv.fig1 = j3pAddElt(stor.lesdiv.figure, 'div')
    stor.lesdiv.fig2 = j3pAddElt(stor.lesdiv.figure, 'div')
    stor.lesdiv.explications = j3pAddElt(stor.lesdiv.conteneur, 'div')
    stor.lesdiv.solution = j3pAddElt(stor.lesdiv.conteneur, 'div')
    stor.lesdiv.solution.style.color = me.styles.toutpetit.correction.color
    stor.lesdiv.explications.style.color = me.styles.cfaux
  }
  function poseQuestion () {
    stor.lesdiv.explications.innerHTML = ''
    stor.lesdiv.solution.innerHTML = ''
    stor.lesdiv.correction.innerHTML = ''
    if (stor.tabFhs[0][0] !== null) j3pEmpty(stor.tabFhs[0][0])
    if (stor.tabGfdd[0][0] !== null) j3pEmpty(stor.tabGfdd[0][0])
    if (stor.tabGGG[0][0] !== null) j3pEmpty(stor.tabGGG[0][0])
    if (stor.tabA[0][0] !== null) j3pEmpty(stor.tabA[0][0])
    if (stor.tabVkf[0][0] !== null) j3pEmpty(stor.tabVkf[0][0])
    if (stor.deb === false) {
      stor.deb = true
      let buf, i
      let adire = []
      if (stor.Lexo.enonce === 'text') {
        if (stor.Lexo.donnes < 2) {
          buf = 'Le triangle $' + stor.nomtri + '$ est rectangle en $' + stor.nC + '$.<br>'
        } else {
          buf = 'Les triangles $' + stor.nomtri + '$ et $' + stor.nomtri2 + '$ sont <br>rectangles en $' + stor.nC + '$ et $' + stor.nD + '$.<br>'
        }
        j3pAffiche(stor.lesdiv.fig2, null, buf)
      }
      j3pAffiche(stor.lesdiv.consigneG, null, 'On veut calculer la longueur $' + stor.rech + '$ avec les données ci-dessous.\n')
      // donne mesures
      if (stor.AB !== 0) adire.push('$' + stor.nA + stor.nB + ' = ' + String(stor.AB).replace('.', ',') + '$')
      if (stor.AC !== 0) adire.push('$' + stor.nA + stor.nC + ' = ' + String(stor.AC).replace('.', ',') + '$')
      if (stor.BC !== 0) adire.push('$' + stor.nB + stor.nC + ' = ' + String(stor.BC).replace('.', ',') + '$')
      if (stor.Lexo.donnes === 2 || (stor.Lexo.donnes === 1 && stor.Lexo.enonce !== 'text')) if (stor.DA !== 0) adire.push('$' + stor.nD + stor.nA + ' = ' + String(stor.DA).replace('.', ',') + '$')
      adire = j3pShuffle(adire)
      for (i = 0; i < adire.length; i++) {
        j3pAffiche(stor.lesdiv.fig2, null, adire[i] + ' cm \n')
      }
      stor.tabfgd = addDefaultTable(stor.lesdiv.travail, 6, 1)
    } else {
      for (const eltBarre of stor.listBarre) {
        j3pDetruit(eltBarre)
      }
      stor.listBarre = []
    }
    poseDescription()
  }

  function makefig () {
    if (stor.Lexo.enonce === 'fig') {
      const txtfigure = 'TWF0aEdyYXBoSmF2YTEuMAAAABM+TMzNAAJmcv###wEA#wEAAAAAAAAAAARMAAACWQAAAQEAAAAAAAAAAQAAAHL#####AAAAAQAKQ0NhbGNDb25zdAD#####AAJwaQAWMy4xNDE1OTI2NTM1ODk3OTMyMzg0Nv####8AAAABAApDQ29uc3RhbnRlQAkh+1RELRj#####AAAAAQAKQ1BvaW50QmFzZQD#####AQAAAAAOAAFVAMAkAAAAAAAAQBAAAAAAAAAAAAUAAEAuAo9cKPXDQC4Cj1wo9cP#####AAAAAQAUQ0Ryb2l0ZURpcmVjdGlvbkZpeGUA#####wEAAAAAEAAAAQAAAAEAAAABAT#wAAAAAAAA#####wAAAAEAD0NQb2ludExpZURyb2l0ZQD#####AQAAAAAOAAFWAMAAAAAAAAAAQBAAAAAAAAAAAAUAAUA+Ao9cKPXDAAAAAv####8AAAABAAhDU2VnbWVudAD#####AQAAAAAQAAABAAAAAQAAAAEAAAAD#####wAAAAEAB0NNaWxpZXUA#####wEAAAAAEAAAAAAAAAAAAAAAQAgAAAAAAAAAAAUAAAAAAQAAAAP#####AAAAAgAMQ0NvbW1lbnRhaXJlAP####8BAAAAAAAAAAAAAAAAQBgAAAAAAAAAAAAAAAUMAAAAAAABAAAAAAAAAAEAAAAAAAAAAAABMf####8AAAABAAlDTG9uZ3VldXIA#####wAAAAEAAAADAAAAAgD#####AQAAAAAQAAAAAAAAAAAAAABACAAAAAAAAAAABQABQF5gAAAAAABAXgzMzMzMzAAAAAIA#####wAAAAAAEAAAAAAAAAAAAAAAQAgAAAAAAAAAAAUAAcBQ4AAAAAAAv+mZmZmZmYAAAAADAP####8AAAAAARAAAAEAAAABAAAACQE#8AAAAAAAAP####8AAAABABFDU3ltZXRyaWVDZW50cmFsZQD#####AAAACP####8AAAABAAxDRHJvaXRlSW1hZ2UA#####wAAAAAAEAAAAQAAAAEAAAAKAAAACwAAAAMA#####wAAAAABEAAAAQAAAAEAAAAJAD#wAAAAAAAAAAAACgD#####AAAAAAAQAAABAAAAAQAAAA0AAAALAAAAAgD#####AQAAAAAQAAAAAAAAAAAAAABACAAAAAAAAAAABQABQFrgAAAAAABAPTMzMzMzNP####8AAAABAAlDQ2VyY2xlT0EA#####wEAAAAAAAABAAAACAAAAA8AAAACAP####8AAAAAABAAAABACAAAAAAAAAAAAAAAAAAAAAAFAABAVKAAAAAAAEB60zMzMzMz#####wAAAAEAB0NDYWxjdWwA#####wAFbWluaTEAATAAAAABAAAAAAAAAAAAAAAMAP####8ABW1heGkxAAMzNjAAAAABQHaAAAAAAAD#####AAAAAQAUQ0ltcGxlbWVudGF0aW9uUHJvdG8A#####wAHQ3Vyc2V1cgAAAAUAAAAFAAAAAwAAABIAAAATAAAAEQAAAAMAAAAAFAEAAAAAEAAAAQAAAAEAAAARAT#wAAAAAAAAAAAABAEAAAAUAAAAAAAQAAAAwAgAAAAAAAA#8AAAAAAAAAAABQAAQF1AAAAAAAAAAAAV#####wAAAAEAC0NIb21vdGhldGllAAAAABQAAAAR#####wAAAAEACkNPcGVyYXRpb24D#####wAAAAEAD0NSZXN1bHRhdFZhbGV1cgAAABIAAAAPAQAAABAAAAASAAAAEAAAABP#####AAAAAQALQ1BvaW50SW1hZ2UAAAAAFAEAAAAADQACTzEAwBAAAAAAAABAEAAAAAAAAAAABQAAAAAWAAAAFwAAAA4AAAAAFAAAABEAAAAPAwAAAA8BAAAAAT#wAAAAAAAAAAAAEAAAABIAAAAPAQAAABAAAAATAAAAEAAAABIAAAARAAAAABQBAAAAAA0AAkkxAMAAAAAAAAAAQAgAAAAAAAAAAAUAAAAAFgAAABkAAAAFAQAAABQAAAAAABAAAAEAAAABAAAAEQAAABYAAAAEAQAAABQAAAAAARAAAmsxAMAAAAAAAAAAQAAAAAAAAAAAAAEAAT#fufufufugAAAAG#####8AAAACAA9DTWVzdXJlQWJzY2lzc2UBAAAAFAAEcm90VAAAABgAAAAaAAAAHP####8AAAABAA9DVmFsZXVyQWZmaWNoZWUBAAAAFAAAAAAAAAAAAAAAAADAGAAAAAAAAAAAAAAAHA8AAf###wAAAAEAAAACAAAAAQAAAAAAAAAAAAAAAAAAAAAdAAAADAD#####AAhyb3RhdGlvbgAEcm90VAAAABAAAAAd#####wAAAAEACUNSb3RhdGlvbgD#####AAAACAAAABAAAAAfAAAAEQD#####Af8AAAAQAAAAAAAAAAAAAABACAAAAAAAAAAABQAAAAAPAAAAIAAAABEA#####wH#AAAAEAAAAAAAAAAAAAAAQAgAAAAAAAAAAAUAAAAAIQAAAAsAAAACAP####8AAAAAABAAAABACAAAAAAAAAAAAAAAAAAAAAAFAABAVaAAAAAAAEB+szMzMzMzAAAADAD#####AAVtaW5pMgACMjAAAAABQDQAAAAAAAAAAAAMAP####8ABW1heGkyAAMxNjAAAAABQGQAAAAAAAAAAAANAP####8AB0N1cnNldXIAAAAFAAAABQAAAAMAAAAkAAAAJQAAACMAAAADAAAAACYBAAAAABAAAAEAAAABAAAAIwE#8AAAAAAAAAAAAAQBAAAAJgD#AAAAEAAAAMAIAAAAAAAAP#AAAAAAAAAAAAUAAEBdQAAAAAAAAAAAJwAAAA4AAAAAJgAAACMAAAAPAwAAABAAAAAkAAAADwEAAAAQAAAAJAAAABAAAAAlAAAAEQAAAAAmAQAAAAANAAJPMQDAEAAAAAAAAEAQAAAAAAAAAAAFAAAAACgAAAApAAAADgAAAAAmAAAAIwAAAA8DAAAADwEAAAABP#AAAAAAAAAAAAAQAAAAJAAAAA8BAAAAEAAAACUAAAAQAAAAJAAAABEAAAAAJgEAAAAADQACSTEAwAAAAAAAAABACAAAAAAAAAAABQAAAAAoAAAAKwAAAAUBAAAAJgD#AAAAEAAAAQAAAAEAAAAjAAAAKAAAAAQBAAAAJgD#AAABEAABawDAAAAAAAAAAEAAAAAAAAAAAAABAAE#2BGBGBGBGAAAAC0AAAASAQAAACYAAnYxAAAAKgAAACwAAAAuAAAAEwEAAAAmAP8AAAAAAAAAAAAAAMAYAAAAAAAAAAAAAAAuDwAB####AAAAAQAAAAIAAAABAAAAAAAAAAAAAAAAAAAAAC8AAAAMAP####8AA3Z2MQACdjEAAAAQAAAALwAAABQA#####wAAAAgAAAAQAAAAMQAAABEA#####wH#AAAAEAAAAAAAAAAAAAAAQAgAAAAAAAAAAAUAAAAAIQAAADIAAAAFAP####8AAAAAABAAAAEAAAACAAAAIQAAADMAAAAFAP####8AAAAAABAAAAEAAAACAAAAMwAAACIAAAAFAP####8AAAAAABAAAAEAAAACAAAAIgAAACH#####AAAAAgAJQ0NlcmNsZU9SAP####8B#wAAAAAAAQAAADMAAAABP+AAAAAAAAAA#####wAAAAEAEENJbnREcm9pdGVDZXJjbGUA#####wAAADUAAAA3#####wAAAAEAEENQb2ludExpZUJpcG9pbnQA#####wH#AAAAEAAAAAAAAAAAAAAAQAgAAAAAAAAAAAUAAQAAADgAAAAXAP####8A#wAAABAAAAAAAAAAAAAAAEAIAAAAAAAAAAAFAAIAAAA4AAAAFgD#####AAAANAAAADcAAAAXAP####8A#wAAABAAAAAAAAAAAAAAAEAIAAAAAAAAAAAFAAEAAAA7AAAAFwD#####Af8AAAAQAAAAAAAAAAAAAABACAAAAAAAAAAABQACAAAAO#####8AAAABAAhDVmVjdGV1cgD#####AQAAfwAQAAABAAAAAQAAADMAAAA9AP####8AAAABAA1DVHJhbnNQYXJWZWN0AP####8AAAA+AAAAEQD#####Af8AAAAQAAAAAAAAAAAAAABACAAAAAAAAAAABQAAAAA5AAAAPwAAAAUA#####wAAAAAAEAAAAQAAAAEAAAA9AAAAQAAAAAUA#####wAAAAAAEAAAAQAAAAEAAABAAAAAOQAAAA4A#####wAAADMAAAABP#zMzMzMzM0AAAARAP####8B#wAAABAAAAAAAAAAAAAAAEAIAAAAAAAAAAAFAAAAAAgAAABDAAAABQD#####AAAAAAAQAAABAAN0MjEAAgAAAEQAAAAhAAAABQD#####AAAAAAAQAAABAAN0MjIAAgAAAEQAAAAiAAAAAgD#####AAAAAAAQAAAAQAgAAAAAAAAAAAAAAAAAAAAABQAAQG8wAAAAAABAevMzMzMzMwAAAAwA#####wAFbWluaTMAAjIwAAAAAUA0AAAAAAAAAAAADAD#####AAVtYXhpMwADMTYwAAAAAUBkAAAAAAAAAAAADQD#####AAdDdXJzZXVyAAAABQAAAAUAAAADAAAASAAAAEkAAABHAAAAAwAAAABKAQAAAAAQAAABAAAAAQAAAEcBP#AAAAAAAAAAAAAEAQAAAEoA#wAAABAAAADACAAAAAAAAD#wAAAAAAAAAAAFAABAXUAAAAAAAAAAAEsAAAAOAAAAAEoAAABHAAAADwMAAAAQAAAASAAAAA8BAAAAEAAAAEgAAAAQAAAASQAAABEAAAAASgEAAAAADQACTzEAwBAAAAAAAABAEAAAAAAAAAAABQAAAABMAAAATQAAAA4AAAAASgAAAEcAAAAPAwAAAA8BAAAAAT#wAAAAAAAAAAAAEAAAAEgAAAAPAQAAABAAAABJAAAAEAAAAEgAAAARAAAAAEoBAAAAAA0AAkkxAMAAAAAAAAAAQAgAAAAAAAAAAAUAAAAATAAAAE8AAAAFAQAAAEoA#wAAABAAAAEAAAABAAAARwAAAEwAAAAEAQAAAEoA#wAAARAAAmsyAMAAAAAAAAAAQAAAAAAAAAAAAAEAAT#YnYnYnYnZAAAAUQAAABIBAAAASgACdjIAAABOAAAAUAAAAFIAAAATAQAAAEoA#wAAAAAAAAAAAAAAwBgAAAAAAAAAAAAAAFIPAAH###8AAAABAAAAAgAAAAEAAAAAAAAAAAAAAAACAAAAUwAAAAwA#####wADdnYyAAJ2MgAAABAAAABTAAAAFAD#####AAAACAAAABAAAABVAAAAEQD#####Af8AAAAQAAAAAAAAAAAAAABACAAAAAAAAAAABQAAAAAiAAAAVgAAAAUA#####wAAAAAAEAAAAQADdDMxAAIAAABXAAAAIQAAAAUA#####wAAAAAAEAAAAQADdDMyAAIAAABXAAAAIgAAABUA#####wH#AAAAAAABAAAAVwAAAAE#4AAAAAAAAAAAAAAWAP####8AAABYAAAAWgAAABcA#####wH#AAAAEAAAAAAAAAAAAAAAQAgAAAAAAAAAAAUAAQAAAFsAAAAXAP####8A#wAAABAAAAAAAAAAAAAAAEAIAAAAAAAAAAAFAAIAAABbAAAAFgD#####AAAAWQAAAFoAAAAXAP####8B#wAAABAAAAAAAAAAAAAAAEAIAAAAAAAAAAAFAAEAAABeAAAAFwD#####AP8AAAAQAAAAAAAAAAAAAABACAAAAAAAAAAABQACAAAAXgAAABgA#####wH#AAAAEAAAAQAAAAEAAABXAAAAXwAAAAAZAP####8AAABhAAAAEQD#####Af8AAAAQAAAAAAAAAAAAAABACAAAAAAAAAAABQAAAABcAAAAYgAAAAUA#####wAAAAAAEAAAAQADdDMzAAEAAABfAAAAYwAAAAUA#####wAAAAAAEAAAAQADdDM0AAEAAABjAAAAXAAAAAwA#####wAEZGlzdAAEMS4xNQAAAAE#8mZmZmZmZgAAAA4A#####wAAAAgAAAAQAAAAZgAAABEA#####wH#AAAAEAAAAAAAAAAAAAAAQAgAAAAAAAAAAAUAAAAAIgAAAGcAAAARAP####8B#wAAABAAAAAAAAAAAAAAAEAIAAAAAAAAAAAFAAAAAEQAAABnAAAAEQD#####Af8AAAAQAAAAAAAAAAAAAABACAAAAAAAAAAABQAAAABXAAAAZwAAABEA#####wH#AAAAEAAAAAAAAAAAAAAAQAgAAAAAAAAAAAUAAAAAIQAAAGcAAAARAP####8B#wAAABAAAAAAAAAAAAAAAEAIAAAAAAAAAAAFAAAAADMAAABnAAAABwD#####AAAAAAEAA3B0MQAAAGgQAAAAAAABAAAAAQAAAAEAAAAAAAAAAAABYQAAAAcA#####wAAAAABAANwdDIAAABsEAAAAAAAAQAAAAEAAAABAAAAAAAAAAAAAWEAAAAHAP####8AAAAAAQADcHQzAAAAaxAAAAAAAAEAAAABAAAAAQAAAAAAAAAAAAFhAAAABwD#####AAAAAAEABHB0NDEAAABpEAAAAAAAAQAAAAEAAAABAAAAAAAAAAAAAWEAAAAHAP####8AAAAAAQAEcHQ0MgAAAGoQAAAAAAABAAAAAQAAAAEAAAAAAAAAAAABYQAAAAf##########w=='
      j3pCreeSVG(stor.lesdiv.fig1, { id: 'mtg32svg', width: 240, height: 240 })
      stor.lesdiv.fig1.style.border = 'solid black 1px'
      stor.lesdiv.fig1.style.background = '#ffffff'
      stor.mtgAppLecteur.removeAllDoc()
      stor.mtgAppLecteur.addDoc('mtg32svg', txtfigure, true)
      stor.mtgAppLecteur.giveFormula2('mtg32svg', 'rotation', stor.rotation, true)
      stor.mtgAppLecteur.giveFormula2('mtg32svg', 'vv1', stor.v1, true)
      stor.mtgAppLecteur.giveFormula2('mtg32svg', 'vv2', stor.v2, true)
      stor.mtgAppLecteur.setText('mtg32svg', '#pt1', stor.nA, true)
      stor.mtgAppLecteur.setText('mtg32svg', '#pt2', stor.nC, true)
      stor.mtgAppLecteur.setText('mtg32svg', '#pt3', stor.nB, true)
      stor.mtgAppLecteur.calculateAndDisplayAll(true)
      switch (stor.Lexo.donnes) {
        case 0:
          stor.mtgAppLecteur.setVisible('mtg32svg', '#t21', false, true)
          stor.mtgAppLecteur.setVisible('mtg32svg', '#t22', false, true)
          stor.mtgAppLecteur.setVisible('mtg32svg', '#t31', false, true)
          stor.mtgAppLecteur.setVisible('mtg32svg', '#t32', false, true)
          stor.mtgAppLecteur.setVisible('mtg32svg', '#t33', false, true)
          stor.mtgAppLecteur.setVisible('mtg32svg', '#t34', false, true)
          stor.mtgAppLecteur.setVisible('mtg32svg', '#pt41', false, true)
          stor.mtgAppLecteur.setVisible('mtg32svg', '#pt42', false, true)
          break
        case 1:
          stor.mtgAppLecteur.setVisible('mtg32svg', '#t31', false, true)
          stor.mtgAppLecteur.setVisible('mtg32svg', '#t32', false, true)
          stor.mtgAppLecteur.setVisible('mtg32svg', '#t33', false, true)
          stor.mtgAppLecteur.setVisible('mtg32svg', '#t34', false, true)
          stor.mtgAppLecteur.setVisible('mtg32svg', '#pt42', false, true)
          stor.mtgAppLecteur.setText('mtg32svg', '#pt41', stor.nD, true)
          break
        case 2:
          stor.mtgAppLecteur.setVisible('mtg32svg', '#t21', false, true)
          stor.mtgAppLecteur.setVisible('mtg32svg', '#t22', false, true)
          stor.mtgAppLecteur.setVisible('mtg32svg', '#pt41', false, true)
          stor.mtgAppLecteur.setText('mtg32svg', '#pt42', stor.nD, true)
      }
    } else {
      stor.lesdiv.travail.style.border = '1px solid black'
      if (ds.Brouillon) {
        stor.lesdiv.fig1.style.width = '270px'
        stor.lesdiv.fig1.style.height = '250px'
        j3pAjouteBouton(stor.lesdiv.fig1, 'Bbrouill', 'MepBoutons', 'Utiliser un brouillon', faisBrouill)
      } else {
        j3pAffiche(stor.lesdiv.fig1, null, '<u>Données:</u>')
      }
    }

  // chope point pour centre
  }

  function poseDescription () {
    if (stor.deb1) {
      poseEgalite()
      return
    }
    stor.deb1 = true
    if (stor.encours !== 'Descrip') {
      rempliDescription()
      return
    }
    stor.tabFhs = addDefaultTable(stor.tabfgd[0][0], 3, 1)
    j3pAffiche(stor.tabFhs[0][0], null, '<b>Complète la phrase suivante.</b>')
    const tyuif = addDefaultTable(stor.tabFhs[1][0], 1, 5)
    j3pAffiche(tyuif[0][0], null, 'Le triangle&nbsp;')
    j3pAffiche(tyuif[0][2], null, '&nbsp; est rectangle en&nbsp; ')
    j3pAffiche(tyuif[0][4], null, '&nbsp; , ')
    // idem recallMe = () => me.sectionCourante()
    const recallMe = me.sectionCourante.bind(me)
    stor.zoneD1 = new ZoneStyleMathquill1(tyuif[0][1], { limite: 5, restric: stor.nA + stor.nB + stor.nC + stor.nD, enter: recallMe })
    stor.zoneD2 = new ZoneStyleMathquill1(tyuif[0][3], { limite: 2, restric: stor.nA + stor.nB + stor.nC + stor.nD, enter: recallMe })
    const jjk = addDefaultTable(stor.tabFhs[2][0], 1, 2)
    j3pAffiche(jjk[0][0], null, 'J’utilise &nbsp;')
    let ll = [' le théorème de Pythagore, ', ' la réciproque du théorème de Pythagore, ', ' le théorème de Thalès, ', ' un théorème, ']
    ll = j3pShuffle(ll)
    ll.splice(0, 0, 'Choisir')
    stor.listeD1 = ListeDeroulante.create(jjk[0][1], ll, { centre: true })
  }
  function rempliDescription () {
    j3pAffiche(stor.tabfgd[0][0], null, 'Le triangle $' + stor.nomtri + '$ est rectangle en $' + stor.nC + '$,\n')
    j3pAffiche(stor.tabfgd[0][0], null, 'j’utilise le théorème de Pythagore,')
    poseEgalite()
  }

  function poseEgalite () {
    if (stor.deb2) {
      poseRempli()
      return
    }
    stor.deb2 = true
    if (stor.encours !== 'egalite') {
      rempliEgalite()
      return
    }
    j3pEmpty('fjkskfksldkjfl1c0')
    stor.tabGfdd = addDefaultTable(stor.tabfgd[1][0], 2, 1)
    j3pAffiche(stor.tabGfdd[0][0], null, '<b>Ecris l’égalité de Pythagore.</b>')
    stor.tabAAA = addDefaultTable(stor.tabGfdd[1][0], 2, 2)
    j3pAffiche(stor.tabAAA[0][0], null, ' donc &nbsp;')
    const focusE1 = () => stor.zoneE1.focus()
    const f1 = function () {
      stor.zoneE1.majaffiche('$$' + stor.nA + stor.nC)
      setTimeout(focusE1, 10)
    }
    const f2 = function () {
      stor.zoneE1.majaffiche('$$' + stor.nB + stor.nC)
      setTimeout(focusE1, 10)
    }
    const f3 = function () {
      stor.zoneE1.majaffiche('$$' + stor.nA + stor.nB)
      setTimeout(focusE1, 10)
    }
    const l1 = '$' + stor.nA + stor.nC + '$'
    const l2 = '$' + stor.nB + stor.nC + '$'
    const l3 = '$' + stor.nA + stor.nB + '$'
    const lfonc = [f1, f2, f3]
    const llong = [l1, l2, l3]
    if (stor.Lexo.donnes !== 0) {
      const f4 = function () {
        stor.zoneE1.majaffiche('$$' + stor.nD + stor.nA)
        setTimeout(focusE1, 10)
      }
      const l4 = '$' + stor.nD + stor.nA + '$'
      llong.push(l4)
      lfonc.push(f4)
    }
    if (j3pGetRandomBool()) {
      lfonc.splice(0, 0, lfonc.pop())
      llong.splice(0, 0, llong.pop())
    }
    if (j3pGetRandomBool()) {
      lfonc.splice(0, 0, lfonc.pop())
      llong.splice(0, 0, llong.pop())
    }
    stor.zoneE1 = new ZoneStyleMathquill2(stor.tabAAA[0][1], { id: 'zoneE1', limite: 15, restric: '²+-*=', hasAutoKeyboard: true, enter: me.sectionCourante.bind(me) })
    stor.boutb = new BoutonStyleMathquill(stor.tabAAA[1][1], 'bout111', llong, lfonc, { mathquill: true })
  }
  function rempliEgalite () {
    stor.laform = 'z^{2}=y^{2}+x^{2}'
    j3pAffiche(stor.tabfgd[1][0], null, ' donc $' + stor.nA + stor.nB + '^{2} = ' + stor.nB + stor.nC + '^{2}+' + stor.nA + stor.nC + '^{2}$')
    poseRempli()
  }

  function poseRempli () {
    if (stor.deb3) {
      poseCarre()
      return
    }
    stor.deb3 = true
    if (stor.encours !== 'rempli') {
      rempliRempli()
      return
    }
    stor.tabGGG = addDefaultTable(stor.tabfgd[2][0], 2, 1)
    j3pAffiche(stor.tabGGG[0][0], null, '<b>Remplace les longueurs connues par leur valeur.</b>')
    const yui = addDefaultTable(stor.tabGGG[1][0], 1, 7)
    const dou = [{ k: 'x', p: stor.laform.indexOf('x') }, { k: 'y', p: stor.laform.indexOf('y') }, { k: 'z', p: stor.laform.indexOf('z') }]
    dou.sort(function (a, b) { return a.p - b.p })
    const buf = stor.laform.split(/[xyz]/)
    stor.laval = []
    stor.laval.push(lavalde(dou[0].k))
    stor.laval.push(lavalde(dou[1].k))
    stor.laval.push(lavalde(dou[2].k))
    j3pAffiche(yui[0][0], null, ' soit &nbsp; ')
    j3pAffiche(yui[0][2], null, '$' + buf[1] + '$')
    j3pAffiche(yui[0][4], null, '$' + buf[2] + '$')
    j3pAffiche(yui[0][6], null, '$' + buf[3] + '$')
    if (okval(dou[0].k)) {
      stor.zoneR1 = new ZoneStyleMathquill1(yui[0][1], { restric: '0123456789.,', enter: me.sectionCourante.bind(me) })
    } else {
      stor.zoneR1 = zsmFake
      j3pAffiche(yui[0][1], null, vaval(dou[0].k))
    }
    if (okval(dou[1].k)) {
      stor.zoneR2 = new ZoneStyleMathquill1(yui[0][3], {
        restric: '0123456789.,',
        enter: me.sectionCourante.bind(me)
      })
    } else {
      stor.zoneR2 = zsmFake
      j3pAffiche(yui[0][3], null, vaval(dou[1].k))
    }
    if (okval(dou[2].k)) {
      stor.zoneR3 = new ZoneStyleMathquill1(yui[0][5], {
        restric: '0123456789.,',
        enter: me.sectionCourante.bind(me)
      })
    } else {
      stor.zoneR3 = zsmFake
      j3pAffiche(yui[0][5], null, vaval(dou[2].k))
    }
  }
  function rempliRempli () {
    let buf = stor.laform
    if (stor.AC !== 0) {
      buf = buf.replace('x', String(stor.AC).replace('.', ','))
    } else {
      buf = buf.replace('x', stor.nA + stor.nC)
    }
    if (stor.AB !== 0) {
      buf = buf.replace('z', String(stor.AB).replace('.', ','))
    } else {
      buf = buf.replace('z', stor.nA + stor.nB)
    }
    if (stor.BC !== 0) {
      buf = buf.replace('y', String(stor.BC).replace('.', ','))
    } else {
      buf = buf.replace('y', stor.nB + stor.nC)
    }
    j3pAffiche(stor.tabfgd[2][0], null, 'soit &nbsp; $' + buf + '$')
    poseCarre()
  }

  function poseCarre () {
    if (stor.deb4) {
      poseRacine()
      return
    }
    stor.deb4 = true
    if (stor.encours !== 'carre') {
      rempliCarre()
      return
    }
    stor.tabA = addDefaultTable(stor.tabfgd[3][0], 3, 1)
    j3pAffiche(stor.tabA[0][0], null, '<b>Calcule $' + stor.rech + '²$.</b>')
    const tghy = addDefaultTable(stor.tabA[1][0], 3, 4)
    stor.brouill = new BrouillonCalculs(
      {
        caseBoutons: tghy[1][0],
        caseEgal: tghy[1][1],
        caseCalculs: tghy[1][2],
        caseApres: tghy[1][3]
      },
      {
        limite: 30,
        limitenb: 20,
        restric: '0123456789,.²+-=' + stor.nA + stor.nB + stor.nC,
        hasAutoKeyboard: false,
        lmax: 3,
        justeg: true,
        titre: '&nbsp;'
      }
    )
    const gkdfj = addDefaultTable(stor.tabA[2][0], 1, 3)
    j3pAffiche(gkdfj[0][0], null, '&nbsp;&nbsp;&nbsp;')
    j3pAffiche(gkdfj[0][1], null, '$' + stor.rech + '²=$')
    stor.zoneC1 = new ZoneStyleMathquill1(gkdfj[0][2], { restric: '0123456789.,', enter: me.sectionCourante.bind(me) })
  }
  function rempliCarre () {
    stor.tabSoluY = addDefaultTable(stor.tabfgd[3][0], 4, 1)

    let buf = stor.laform
    while (buf.indexOf('^{2}') !== -1) {
      buf = buf.replace('^{2}', '')
    }
    if (stor.AC !== 0) {
      buf = buf.replace('x', String(j3pArrondi(stor.AC * stor.AC, 2)).replace('.', ','))
    } else {
      buf = buf.replace('x', stor.nA + stor.nC + '²')
    }
    if (stor.AB !== 0) {
      buf = buf.replace('z', String(j3pArrondi(stor.AB * stor.AB, 2)).replace('.', ','))
    } else {
      buf = buf.replace('z', stor.nA + stor.nB + '²')
    }
    if (stor.BC !== 0) {
      buf = buf.replace('y', String(j3pArrondi(stor.BC * stor.BC, 2)).replace('.', ','))
    } else {
      buf = buf.replace('y', stor.nB + stor.nC + '²')
    }
    j3pAffiche(stor.tabSoluY[0][0], null, '&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp; $' + buf + '$')
    if (stor.Lexo.cote !== 0) {
      if (buf.indexOf('-') === -1) {
        j3pAffiche(stor.tabSoluY[1][0], null, '&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp; $' + stor.rech + '² = ' + String(j3pArrondi(stor.AB * stor.AB, 2)).replace('.', ',') + '-' + stor.lot + '$')
      }
    }
    j3pAffiche(stor.tabSoluY[2][0], null, '&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp; $' + stor.rech + '² = ' + String(stor.carrBon).replace('.', ',') + '$')
    poseRacine()
  }

  function poseRacine () {
    if (stor.deb5) {
      poseReponse()
      return
    }
    stor.deb5 = true
    if (stor.encours !== 'racine') {
      rempliRacine()
      return
    }
    stor.tabVkf = addDefaultTable(stor.tabfgd[4][0], 2, 1)
    j3pAffiche(stor.tabVkf[0][0], null, '<b>Complète le calcul.</b>')
    const tbgr = addDefaultTable(stor.tabVkf[1][0], 1, 2)
    j3pAffiche(tbgr[0][0], null, 'Donc &nbsp;')
    let ll = []
    ll.push('$' + stor.rech + ' = \\sqrt{' + String(stor.carrBon).replace('.', ',') + '}$')
    ll.push('$' + stor.rech + '² = \\sqrt{' + String(stor.carrBon).replace('.', ',') + '}$')
    ll.push('$' + stor.rech + ' = ' + String(stor.carrBon).replace('.', ',') + '²$')
    ll.push('$' + stor.rech + '² = ' + String(stor.carrBon).replace('.', ',') + '²$')
    ll = j3pShuffle(ll)
    ll.splice(0, 0, 'Choisir')
    stor.listR1 = ListeDeroulante.create(tbgr[0][1], ll, { centre: true })
  }
  function rempliRacine () {
    j3pAffiche(stor.tabfgd[4][0], null, '&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;$' + stor.rech + ' = \\sqrt{' + String(stor.carrBon).replace('.', ',') + '}$')
    poseReponse()
  }

  function poseReponse () {
    let buf
    stor.encours = 'reponse'
    stor.ceteg = stor.valBon === j3pArrondi(stor.valBon, stor.Lexo.rang)
    if (stor.ceteg) {
      j3pAffiche(stor.tabfgd[5][0], null, '<b>Calcule la valeur de $' + stor.rech + '$.</b>\n')
    } else {
      switch (stor.Lexo.rang) {
        case 0:
          buf = ' au cm près '
          break
        case 1:
          buf = ' au dixième de cm près'
          break
        case 2:
          buf = ' au centième de cm près '
          break
        case 3:
          buf = ' au millième de cm près '
      }
      buf = '<b>Calcule une approximation décimale de $' + stor.rech + '$ ' + buf + '.</b><br>'
      if (ds.Val_Ap) {
        buf = buf.replace('approximation décimale', 'valeur approchée')
      }
      j3pAffiche(stor.tabfgd[5][0], null, buf)
    }
    stor.tabRai = addDefaultTable(stor.tabfgd[5][0], 1, 4)
    j3pAffiche(stor.tabRai[0][0], null, '&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;$' + stor.rech + '$ ')
    j3pAffiche(stor.tabRai[0][3], null, '&nbsp; cm ')
    const ll = ['Choisir', '$=$', '$\\approx$']
    stor.listRep1 = ListeDeroulante.create(stor.tabRai[0][1], ll, { centre: true, onChange: gerelisteRep })
    stor.zoneRep1 = new ZoneStyleMathquill1(stor.tabRai[0][2], { limite: 5, restric: '0123456789.,', enter: me.sectionCourante.bind(me) })
  }

  function faisco () {
    stor.foco = false
    let buf
    switch (stor.encours) {
      case 'Descrip':
        j3pEmpty(stor.tabfgd[0][0])
        stor.tabfgd[0][0].style.color = me.styles.petit.correction.color
        j3pAffiche(stor.tabfgd[0][0], null, 'Le triangle $' + stor.nomtri + '$ est rectangle en $' + stor.nC + '$,\n')
        j3pAffiche(stor.tabfgd[0][0], null, 'j’utilise le théorème de Pythagore,')
        break
      case 'egalite':
        j3pEmpty(stor.tabfgd[1][0])
        stor.tabfgd[1][0].style.color = me.styles.petit.correction.color
        stor.laform = 'z^{2}=x^{2}+y^{2}'
        j3pAffiche(stor.tabfgd[1][0], null, 'donc $' + stor.nA + stor.nB + '^2=' + stor.nB + stor.nC + '^2+' + stor.nA + stor.nC + '^2$')
        break
      case 'rempli':
        j3pEmpty(stor.tabfgd[2][0])
        stor.tabfgd[2][0].style.color = me.styles.petit.correction.color
        buf = stor.laform
        if (stor.AC !== 0) {
          buf = buf.replace('x', String(stor.AC).replace('.', ','))
        } else {
          buf = buf.replace('x', stor.nA + stor.nC)
        }
        if (stor.AB !== 0) {
          buf = buf.replace('z', String(stor.AB).replace('.', ','))
        } else {
          buf = buf.replace('z', stor.nA + stor.nB)
        }
        if (stor.BC !== 0) {
          buf = buf.replace('y', String(stor.BC).replace('.', ','))
        } else {
          buf = buf.replace('y', stor.nB + stor.nC)
        }
        j3pAffiche(stor.tabfgd[2][0], null, 'soit &nbsp; $' + buf + '$')
        break
      case 'carre':
        j3pEmpty(stor.tabfgd[3][0])
        stor.tabfgd[3][0].style.color = me.styles.petit.correction.color
        stor.tabSoluY = addDefaultTable(stor.tabfgd[3][0], 4, 1)
        buf = stor.laform
        while (buf.indexOf('^{2}') !== -1) {
          buf = buf.replace('^{2}', '')
        }
        if (stor.AC !== 0) {
          buf = buf.replace('x', String(j3pArrondi(stor.AC * stor.AC, 2)).replace('.', ','))
        } else {
          buf = buf.replace('x', stor.nA + stor.nC + '²')
        }
        if (stor.AB !== 0) {
          buf = buf.replace('z', String(j3pArrondi(stor.AB * stor.AB, 2)).replace('.', ','))
        } else {
          buf = buf.replace('z', stor.nA + stor.nB + '²')
        }
        if (stor.BC !== 0) {
          buf = buf.replace('y', String(j3pArrondi(stor.BC * stor.BC, 2)).replace('.', ','))
        } else {
          buf = buf.replace('y', stor.nB + stor.nC + '²')
        }
        j3pAffiche(stor.tabSoluY[0][0], null, '&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp; $' + buf + '$')
        if (stor.Lexo.cote !== 0) {
          if (buf.indexOf('-') === -1) {
            j3pAffiche(stor.tabSoluY[1][0], null, '&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp; $' + stor.rech + '² = ' + String(j3pArrondi(stor.AB * stor.AB, 2)).replace('.', ',') + '-' + stor.lot + '$')
          }
        }
        j3pAffiche(stor.tabSoluY[2][0], null, '&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp; $' + stor.rech + '² = ' + String(stor.carrBon).replace('.', ',') + '$')
        break
      case 'racine':
        j3pEmpty(stor.tabfgd[4][0])
        stor.tabfgd[4][0].style.color = me.styles.petit.correction.color
        j3pAffiche(stor.tabfgd[4][0], null, '&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;$' + stor.rech + ' = \\sqrt{' + String(stor.carrBon).replace('.', ',') + '}$')
        break
    }
  }
  function faisBrouill () {
    j3pEmpty(stor.lesdiv.fig1)
    const buf = [stor.nA, stor.nB, stor.nC]
    if (stor.Lexo.donnes !== 0) buf.push(stor.nD)
    Paint.create(stor.lesdiv.fig1, { width: 270, height: 240, textes: buf })
  }
  // utilise this
  function boutonfen () {
    j3pToggleFenetres('Calculatrice')
  }
  function gerelisteRep () {
    let buf
    if (stor.listRep1.reponse !== '$=$') {
      switch (stor.Lexo.rang) {
        case 0:
          buf = ' cm (au cm près) '
          break
        case 1:
          buf = ' cm (au dixième de cm près) '
          break
        case 2:
          buf = ' cm (au centième de cm près) '
          break
        case 3:
          buf = ' cm (au millième de cm près) '
      }
      j3pEmpty(stor.tabRai[0][3])
      j3pAffiche(stor.tabRai[0][3], null, '&nbsp;' + buf)
    } else {
      j3pEmpty(stor.tabRai[0][3])
      j3pAffiche(stor.tabRai[0][3], null, '&nbsp; cm')
    }
  }
  function vaval (l) {
    switch (l) {
      case 'x': return '$' + stor.nA + stor.nC + '$'
      case 'y': return '$' + stor.nB + stor.nC + '$'
      case 'z': return '$' + stor.nA + stor.nB + '$'
    }
  }
  function lavalde (l) {
    switch (l) {
      case 'x': return stor.AC
      case 'y': return stor.BC
      case 'z': return stor.AB
    }
  }
  function okval (l) {
    switch (stor.Lexo.cote) {
      case 0:
        if (l === 'z') return false
        break
      case 1:
        if (l === 'x') return false
        break
      case 2:
        if (l === 'y') return false
    }
    return true
  }

  function lesvalp (k, r) {
    const arte = []
    arte.push(j3pArrondi(Math.floor(Math.pow(10, r) * k) / Math.pow(10, r), r))
    arte.push(j3pArrondi(arte[0] + Math.pow(10, -r), r))
    return arte
  }
  function remp (x, t, y) {
    while (t.indexOf(x) !== -1) {
      t = t.replace(x, y)
    }
    return t
  }

  function yaReponse () {
    if (stor.encours === 'Descrip') {
      if (stor.zoneD1.reponse() === '') {
        stor.zoneD1.focus()
        return false
      }
      if (stor.zoneD2.reponse() === '') {
        stor.zoneD2.focus()
        return false
      }
      if (!stor.listeD1.changed) {
        stor.listeD1.focus()
        return false
      }
      return true
    }
    if (stor.encours === 'egalite') {
      if (!stor.zoneE1.reponse() === '') {
        stor.zoneE1.focus()
        return false
      }
      return true
    }
    if (stor.encours === 'rempli') {
      if (stor.zoneR1.reponse() === '') {
        stor.zoneR1.focus()
        return false
      }
      if (stor.zoneR2.reponse() === '') {
        stor.zoneR2.focus()
        return false
      }
      if (stor.zoneR3.reponse() === '') {
        stor.zoneR3.focus()
        return false
      }
      return true
    }
    if (stor.encours === 'carre') {
      if (stor.zoneC1.reponse() === '') {
        stor.zoneC1.focus()
        return false
      }
      return true
    }
    if (stor.encours === 'racine') {
      if (!stor.listR1.changed) {
        stor.listR1.focus()
        return false
      }
      return true
    }
    if (stor.encours === 'reponse') {
      if (!stor.listRep1.changed) {
        stor.listRep1.focus()
        return false
      }
      if (stor.zoneRep1.reponse() === '') {
        stor.zoneRep1.focus()
        return false
      }
      return true
    }
  }
  function isRepOk () {
    stor.errTh = false
    stor.errTh2 = false
    stor.errTri = false
    stor.errTriList = []
    stor.errPoint = false
    stor.errPointList = []

    stor.errOubCaree = false
    stor.errTro = false
    stor.errPlusInut = false
    stor.errFormBonne = false
    stor.errOubEg = false

    stor.errEcrit = false
    stor.errEcritList = []
    stor.errRemp = false

    stor.errCarreDouble = false
    stor.errCalc = false

    stor.errEg = false

    let ok = true
    let t1, t2, t3, t4, n1, n2

    if (stor.encours === 'Descrip') {
      t1 = estNomTriangle(stor.zoneD1.reponse(), stor.nomtri)
      if (!t1.good) {
        stor.zoneD1.corrige(false)
        stor.errTri = true
        stor.errTriList.push(t1.err)
        ok = false
      }
      t1 = estNompoint(stor.zoneD2.reponse(), {})
      if (!t1.good) {
        stor.zoneD2.corrige(false)
        stor.errPoint = true
        stor.errPointList.push(t1.adire)
        ok = false
      } else {
        if (stor.zoneD2.reponse() !== stor.nC) {
          stor.zoneD2.corrige(false)
          if (!stor.errTri) {
            stor.errPoint = true
            stor.errPointList.push('Le triangle $' + stor.zoneD1.reponse() + '$ n’est pas rectangle en $' + stor.zoneD2.reponse() + '$ !<br>')
            ok = false
          }
        }
      }
      if (stor.listeD1.reponse !== ' le théorème de Pythagore, ') {
        stor.listeD1.corrige(false)
        if (stor.listeD1.reponse === ' un théorème, ') {
          stor.errTh2 = true
        } else { stor.errTh = true }
        ok = false
      }
      return ok
    }
    if (stor.encours === 'egalite') {
      t1 = stor.zoneE1.reponse()
      t1 = remp('$$' + stor.nA + stor.nC, t1, 'x')
      t1 = remp('$$' + stor.nB + stor.nC, t1, 'y')
      t1 = remp('$$' + stor.nA + stor.nB, t1, 'z')
      t1 = remp('$$' + stor.nD + stor.nA, t1, 'k')
      t1 = t1.replace(/ /g, '')

      if (t1.indexOf('k') !== -1) {
        stor.errTro = true
      }
      if (t1.indexOf('^{2}') === -1) {
        stor.errOubCaree = true
      }
      stor.laform = t1
      if (t1 === 'z^{2}=x^{2}+y^{2}' || t1 === 'z^{2}=y^{2}+x^{2}' || t1 === 'x^{2}+y^{2}=z^{2}' || t1 === 'y^{2}+x^{2}=z^{2}') return true
      if (t1 === 'x^{2}=z^{2}-y^{2}' || t1 === 'z^{2}-y^{2}=x^{2}' || t1 === 'x^{2}=-y^{2}+z^{2}' || t1 === '-y^{2}+z^{2}=x^{2}') {
        if (stor.Lexo.cote === 1) { return true } else {
          stor.errFormBonne = true
        }
      }
      if (t1 === 'y^{2}=z^{2}-x^{2}' || t1 === 'z^{2}-x^{2}=y^{2}' || t1 === 'y^{2}=-x^{2}+z^{2}' || t1 === '-x^{2}+z^{2}=y^{2}') {
        if (stor.Lexo.cote === 2) { return true } else {
          stor.errFormBonne = true
        }
      }
      if (t1 === '-z^{2}=-x^{2}-y^{2}' || t1 === '-z^{2}=-y^{2}-x^{2}' || t1 === '-x^{2}-y^{2}=-z^{2}' || t1 === '-y^{2}-x^{2}=-z^{2}' || t1 === '-x^{2}=-z^{2}+y^{2}' || t1 === '-z^{2}+y^{2}=-x^{2}' || t1 === '-x^{2}=y^{2}-z^{2}' || t1 === 'y^{2}-z^{2}=-x^{2}' || t1 === '-y^{2}=-z^{2}+x^{2}' || t1 === '-z^{2}+x^{2}=-y^{2}' || t1 === '-y^{2}=x^{2}-z^{2}' || t1 === 'x^{2}-z^{2}=-y^{2}') {
        stor.errFormBonne = true
      }
      stor.zoneE1.corrige(false)
      if (t1.indexOf('=') === -1) {
        stor.errOubEg = true
        return false
      }
      if (t1[0] === '+' || t1[t1.indexOf('=') + 1] === '+') {
        stor.errPlusInut = true
      }
      return false
    }
    if (stor.encours === 'rempli') {
      t1 = stor.zoneR1.reponse()
      t2 = stor.zoneR2.reponse()
      t3 = stor.zoneR3.reponse()
      t4 = (stor.laform.indexOf('-') === -1)
      if (t1 !== ' ') {
        n1 = verifNombreBienEcrit(t1, false, true)
        if (!n1.good) {
          stor.errEcrit = true
          stor.errEcritList.push(n1.remede)
        }
      }
      if (t2 !== ' ') {
        n1 = verifNombreBienEcrit(t2, false, true)
        if (!n1.good) {
          stor.errEcrit = true
          stor.errEcritList.push(n1.remede)
        }
      }
      if (t3 !== ' ') {
        n1 = verifNombreBienEcrit(t3, false, true)
        if (!n1.good) {
          stor.errEcrit = true
          stor.errEcritList.push(n1.remede)
        }
      }
      if (stor.errEcrit) return false
      n2 = []
      if (t1 !== ' ') {
        if (parseFloat(t1.replace(',', '.')) !== stor.laval[0]) {
          ok = false
          n2.push({ w: 1, k: t1, c: stor.laval[0] })
        }
      }
      if (t2 !== ' ') {
        if (parseFloat(t2.replace(',', '.')) !== stor.laval[1]) {
          ok = false
          n2.push({ w: 2, k: t2, c: stor.laval[1] })
        }
      }
      if (t3 !== ' ') {
        if (parseFloat(t3.replace(',', '.')) !== stor.laval[2]) {
          ok = false
          n2.push({ w: 3, k: t3, c: stor.laval[2] })
        }
      }
      if (ok) return true
      if (n2.length === 1) {
        switch (n2[0]) {
          case 1: stor.zoneR1.corrige(false)
            break
          case 2: stor.zoneR2.corrige(false)
            break
          case 3: stor.zoneR3.corrige(false)
        }
        stor.errRemp = true
        return false
      }
      if (t4 && (parseFloat(n2[0].k.replace(',', '.')) === n2[1].c) && (parseFloat(n2[1].k.replace(',', '.')) === n2[0].c)) return true
      stor.zoneR1.corrige(false)
      stor.zoneR2.corrige(false)
      stor.zoneR3.corrige(false)
      return false
    }
    if (stor.encours === 'carre') {
    // cherche les deux longueur en haut
      t1 = stor.zoneC1.reponse()
      n1 = verifNombreBienEcrit(t1, false, true)
      if (!n1.good) {
        stor.errEcrit = true
        stor.errEcritList.push(n1.remede)
        stor.zoneC1.corrige(false)
        return false
      }
      n1 = parseFloat(t1.replace(',', '.'))
      if (n1 === stor.carrBon) return true
      stor.zoneC1.corrige(false)
      for (let i = 0; i < stor.carrFo.length; i++) {
        if (n1 === stor.carrFo[i]) {
          stor.errCarreDouble = true
          return false
        }
      }
      stor.errCalc = true
      return false
    }
    if (stor.encours === 'racine') {
      return stor.listR1.reponse === '$' + stor.rech + ' = \\sqrt{' + String(stor.carrBon).replace('.', ',') + '}$'
    }
    if (stor.encours === 'reponse') {
      t1 = stor.zoneRep1.reponse()
      n1 = verifNombreBienEcrit(t1, false, true)
      if (!n1.good) {
        stor.errEcrit = true
        stor.errEcritList.push(n1.remede)
        stor.zoneC1.corrige(false)
        return false
      }
      n1 = parseFloat(t1.replace(',', '.'))
      if (stor.ceteg) {
        if (n1 !== stor.valBon) {
          stor.zoneRep1.corrige(false)
          stor.errCalc = true
          return false
        }
        if (stor.listRep1.reponse !== '$=$') {
          stor.errEg = true
          stor.listRep1.corrige(false)
          return false
        }
        return true
      } else {
        if (lesvalp(stor.valBon, stor.Lexo.rang).indexOf(n1) === -1) {
          stor.zoneRep1.corrige(false)
          stor.errCalc = true
          return false
        }
        if (stor.listRep1.reponse === '$=$') {
          stor.errEg = true
          stor.listRep1.corrige(false)
          return false
        }
        return true
      }
    }
  } // isRepOk
  function desactiveAll () {
    if (stor.encours === 'Descrip') {
      stor.zoneD2.disable()
      stor.zoneD1.disable()
      stor.listeD1.disable()
    }
    if (stor.encours === 'egalite') {
      stor.zoneE1.disable()
      j3pEmpty(stor.tabAAA[1][1])
    }
    if (stor.encours === 'rempli') {
      stor.zoneR1.disable()
      stor.zoneR2.disable()
      stor.zoneR3.disable()
    }
    if (stor.encours === 'carre') {
      stor.zoneC1.disable()
      stor.brouill.disable()
    }
    if (stor.encours === 'racine') {
      stor.listR1.disable()
    }
    if (stor.encours === 'reponse') {
      stor.listRep1.disable()
      stor.zoneRep1.disable()
    }
  } // desactiveAll
  function passeToutVert () {
    if (stor.encours === 'Descrip') {
      stor.zoneD1.corrige(true)
      stor.zoneD2.corrige(true)
      stor.listeD1.corrige(true)
    }
    if (stor.encours === 'egalite') {
      stor.zoneE1.corrige(true)
    }
    if (stor.encours === 'rempli') {
      stor.zoneR1.corrige(true)
      stor.zoneR2.corrige(true)
      stor.zoneR3.corrige(true)
    }
    if (stor.encours === 'carre') {
      stor.zoneC1.corrige(true)
    }
    if (stor.encours === 'racine') {
      stor.listR1.corrige(true)
    }
    if (stor.encours === 'reponse') {
      stor.listRep1.corrige(true)
      stor.zoneRep1.corrige(true)
    }
  } // passeToutVert

  function barrelesfo () {
    // FIXME barreIfKo() retourne toujours undefined, pourquoi remplir des tableaux avec ????
    if (stor.encours === 'Descrip') {
      stor.listeD1.barreIfKo()
      stor.listBarre.push(stor.zoneD1.barreIfKo())
      stor.listBarre.push(stor.zoneD2.barreIfKo())
    }
    if (stor.encours === 'egalite') {
      stor.listBarre.push(stor.zoneE1.barre())
    }
    if (stor.encours === 'rempli') {
      stor.listBarre.push(stor.zoneR1.barreIfKo())
      stor.listBarre.push(stor.zoneR2.barreIfKo())
      stor.listBarre.push(stor.zoneR3.barreIfKo())
    }
    if (stor.encours === 'carre') {
      stor.listBarre.push(stor.zoneC1.barre())
    }
    if (stor.encours === 'racine') {
      stor.listBarre.push(stor.listR1.barre())
    }
    if (stor.encours === 'reponse') {
      stor.listRep1.barreIfKo()
      stor.listBarre.push(stor.zoneRep1.barreIfKo())
    }
  }
  function affCorrFaux (isFin) {
  /// //affiche indic
    let i, buf
    let yaexplik = false
    if (stor.errTh) {
      j3pAffiche(stor.lesdiv.explications, null, 'Tu n’utilises pas le bon théorème ! \n')
      yaexplik = true
      stor.compteurPe1++
    }
    if (stor.errTh2) {
      j3pAffiche(stor.lesdiv.explications, null, 'Tu dois préciser le théorème que tu utilises ! \n')
      yaexplik = true
      stor.compteurPe1++
    }
    if (stor.errTri) {
      buf = ''
      for (i = 0; i < stor.errTriList.length; i++) {
        buf += stor.errTriList[i] + ' <br>'
      }
      j3pAffiche(stor.lesdiv.explications, null, buf)
      yaexplik = true
      stor.compteurPe1++
    }
    if (stor.errPoint) {
      buf = ''
      for (i = 0; i < stor.errPointList.length; i++) {
        buf += stor.errPointList[i] + ' <br>'
      }
      j3pAffiche(stor.lesdiv.explications, null, buf)
      yaexplik = true
      stor.compteurPe1++
    }
    if (stor.errOubCaree) {
      j3pAffiche(stor.lesdiv.explications, null, "L’égalité de Pythagore s'écrit avec des ² ! \n")
      yaexplik = true
      stor.compteurPe2++
    }
    if (stor.errTro) {
      j3pAffiche(stor.lesdiv.explications, null, 'La longueur $' + stor.nD + stor.nA + '$ n’est pas un longueur du triangle $' + stor.nomtri + '$ !\n')
      yaexplik = true
      stor.compteurPe2++
    }
    if (stor.errPlusInut) {
      j3pAffiche(stor.lesdiv.explications, null, 'Ta réponse comporte un signe "+" que tu peux supprimer !')
      yaexplik = true
    }
    if (stor.errFormBonne) {
      j3pAffiche(stor.lesdiv.explications, null, 'Ecris la formule de manière plus simple !')
      yaexplik = true
    }
    if (stor.errOubEg) {
      j3pAffiche(stor.lesdiv.explications, null, "Il s’agit de <u><b>l’égalité</b></u> de Pythagore ! \n(je ne vois pas de signe ''='')")
      yaexplik = true
      stor.compteurPe2++
    }
    if (stor.errEcrit) {
      buf = ''
      for (i = 0; i < stor.errEcritList.length; i++) {
        buf += stor.errEcritList[i] + ' <br>'
      }
      j3pAffiche(stor.lesdiv.explications, null, buf)
      yaexplik = true
      stor.compteurPe3++
    }
    if (stor.errRemp) {
      j3pAffiche(stor.lesdiv.explications, null, 'Tu dois remplacer les longueurs par leur valeur !')
      yaexplik = true
      stor.compteurPe3++
    }
    if (stor.errCarreDouble) {
      j3pAffiche(stor.lesdiv.explications, null, 'Tu as confondu $\\times 2$ et <i><b>au carré</b></i> !\n')
      yaexplik = true
      stor.compteurPe3++
    }
    if (stor.errCalc) {
      j3pAffiche(stor.lesdiv.explications, null, 'Erreur de calcul !\n')
      yaexplik = true
      stor.compteurPe3++
    }
    if (stor.errEg) {
      j3pAffiche(stor.lesdiv.explications, null, 'On utilise $=$ pour une valeur exacte ,\net $\\aprox$ pour une valeur approchée !')
      yaexplik = true
      stor.compteurPe3++
    }

    if (isFin) {
    // barre les faux
      barrelesfo()
      desactiveAll()
      stor.foco = true

      // afficheco
      if (stor.encours === 'Descrip') {
        j3pAffiche(stor.lesdiv.solution, null, 'Le triangle $' + stor.nomtri + '$ est rectangle en $' + stor.nC + '$,\n')
        j3pAffiche(stor.lesdiv.solution, null, 'j’utilise le théorème de Pythagore,')
      }
      if (stor.encours === 'egalite') {
        stor.laform = 'z^{2}=x^{2}+y^{2}'
        j3pAffiche(stor.lesdiv.solution, null, 'donc $' + stor.nA + stor.nB + '^2=' + stor.nB + stor.nC + '^2+' + stor.nA + stor.nC + '^2$')
      }
      if (stor.encours === 'rempli') {
        buf = stor.laform
        if (stor.AC !== 0) {
          buf = buf.replace('x', String(stor.AC).replace('.', ','))
        } else {
          buf = buf.replace('x', stor.nA + stor.nC)
        }
        if (stor.AB !== 0) {
          buf = buf.replace('z', String(stor.AB).replace('.', ','))
        } else {
          buf = buf.replace('z', stor.nA + stor.nB)
        }
        if (stor.BC !== 0) {
          buf = buf.replace('y', String(stor.BC).replace('.', ','))
        } else {
          buf = buf.replace('y', stor.nB + stor.nC)
        }
        j3pAffiche(stor.lesdiv.solution, null, 'soit &nbsp; $' + buf + '$')
      }
      if (stor.encours === 'carre') {
        stor.tabSoluY = addDefaultTable(stor.lesdiv.solution, 4, 1)
        buf = stor.laform
        while (buf.indexOf('^{2}') !== -1) {
          buf = buf.replace('^{2}', '')
        }
        if (stor.AC !== 0) {
          buf = buf.replace('x', String(j3pArrondi(stor.AC * stor.AC, 2)).replace('.', ','))
        } else {
          buf = buf.replace('x', stor.nA + stor.nC + '²')
        }
        if (stor.AB !== 0) {
          buf = buf.replace('z', String(j3pArrondi(stor.AB * stor.AB, 2)).replace('.', ','))
        } else {
          buf = buf.replace('z', stor.nA + stor.nB + '²')
        }
        if (stor.BC !== 0) {
          buf = buf.replace('y', String(j3pArrondi(stor.BC * stor.BC, 2)).replace('.', ','))
        } else {
          buf = buf.replace('y', stor.nB + stor.nC + '²')
        }
        j3pAffiche(stor.tabSoluY[0][0], null, ' $' + buf + '$')
        if (stor.Lexo.cote !== 0) {
          if (buf.indexOf('-') === -1) {
            j3pAffiche(stor.tabSoluY[1][0], null, ' $' + stor.rech + '² = ' + String(j3pArrondi(stor.AB * stor.AB, 2)).replace('.', ',') + '-' + stor.lot + '$')
          }
        }
        j3pAffiche(stor.tabSoluY[2][0], null, ' $' + stor.rech + '² = ' + String(stor.carrBon).replace('.', ',') + '$')
      }
      if (stor.encours === 'racine') {
        j3pAffiche(stor.lesdiv.solution, null, '$' + stor.rech + ' = \\sqrt{' + String(stor.carrBon).replace('.', ',') + '}$')
      }
      if (stor.encours === 'reponse') {
        buf = '= ' + String(stor.valBon).replace('.', ',')
        if (!stor.ceteg) {
          buf = '\\approx ' + String(j3pArrondi(stor.valBon, stor.Lexo.rang)).replace('.', ',')
        }
        j3pAffiche(stor.lesdiv.solution, null, '$' + stor.rech + ' ' + buf + ' $ cm')
      }
      if (ds.Sans_Echec) {
        stor.foco = false
        // FIXME il ne faut pas affecter ça, c'est Parcours qui s'en charge, il faut imposer nbetapes `if  (ds.Sans_Echec) this.surcharge({nbetapes: 1})`
        me.etapeCourante = ds.nbetapes
        me.questionCourante += (ds.nbetapes - me.questionCourante % ds.nbetapes)
        stor.encours = stor.listencours[stor.listencours.length - 1]
      }
      stor.lesdiv.explications.classList.add('explique')
    }
    if (yaexplik) {
      stor.lesdiv.explications.classList.add('explique')
    }
  } // affCorrFaux

  function enonceMain () {
    if (me.etapeCourante === 1) {
      me.videLesZones()
      creeLesDiv()
      stor.Lexo = faisChoixExos()
      if (ds.Calculatrice) {
        me.fenetresjq = [{ name: 'Calculatrice', title: 'Une calculatrice', left: 600, top: 10 }]
        j3pCreeFenetres(me)
        j3pAfficheCroixFenetres('Calculatrice')
        j3pAjouteBouton(stor.lesdiv.calculatrice, 'Calculatrice', 'MepBoutons', 'Calculatrice', boutonfen)
      }
      stor.lesdiv.travail.classList.add('travail')
      stor.lesdiv.consigneG.classList.add('enonce')
      stor.lesdiv.fig2.classList.add('enonce')
      me.zonesElts.MG.classList.add('fond')
      stor.lesdiv.explications.classList.remove('explique')
      stor.lesdiv.solution.classList.remove('correction')
    }

    if (stor.foco) faisco()
    let tt = stor.listencours.indexOf(stor.encours)
    tt++
    if (tt === stor.listencours.length) tt = 0
    stor.encours = stor.listencours[tt]
    if (me.etapeCourante === 1) {
      makefig()
    }
    poseQuestion()
    me.finEnonce()
  }

  switch (this.etat) {
    case 'enonce':
      // code exécuté au lancement de la section
      if (me.debutDeLaSection) {
        initSection()
      } else {
        enonceMain()
      }
      break // case "enonce":

    case 'correction':
      // On teste si une réponse a été saisie
      stor.lesdiv.explications.innerHTML = ''
      stor.lesdiv.solution.innerHTML = ''
      stor.lesdiv.explications.classList.remove('explique')
      stor.lesdiv.solution.classList.remove('correction')

      if (!yaReponse()) {
        // pas de réponse
        stor.lesdiv.correction.style.color = this.styles.cfaux
        stor.lesdiv.correction.innerHTML = reponseIncomplete
        return this.finCorrection()
      }

      // Bonne réponse
      if (isRepOk()) {
        this._stopTimer()
        stor.lesdiv.correction.style.color = this.styles.cbien
        stor.lesdiv.correction.innerHTML = cBien
        this.score++

        passeToutVert()
        desactiveAll()

        this.typederreurs[0]++
        return this.finCorrection('navigation', true)
      }

      // Pas de bonne réponse
      stor.lesdiv.correction.style.color = this.styles.cfaux
      stor.lesdiv.correction.innerHTML = cFaux
      if (this.isElapsed) {
        // A cause de la limite de temps :
        stor.lesdiv.correction.innerHTML += tempsDepasse + '<BR>'
        this.typederreurs[10]++
        affCorrFaux(true)
        return this.finCorrection('navigation', true)
      }

      // Réponse fausse :
      if (me.essaiCourant < ds.nbchances) {
        stor.lesdiv.correction.innerHTML += '<br>' + essaieEncore
        affCorrFaux(false)
        this.typederreurs[1]++
        return this.finCorrection()
      }

      // Erreur au dernier essai
      affCorrFaux(true)
      if (stor.encours === 'compare') {
        this.score = j3pArrondi(this.score + 0.1, 1)
      }
      if (stor.encours === 'calcul') {
        this.score = j3pArrondi(this.score + 0.4, 1)
      }
      if (stor.encours === 'concl') {
        this.score = j3pArrondi(this.score + 0.7, 1)
      }
      this.typederreurs[2]++
      this.finCorrection('navigation', true)
      break // case "correction":

    case 'navigation':
      if (this.sectionTerminee()) {
        this.parcours.pe = getPe(ds, stor)
      } else {
        this.etat = 'enonce'
      }
      this.finNavigation()
      break // case "navigation":
  }
}
