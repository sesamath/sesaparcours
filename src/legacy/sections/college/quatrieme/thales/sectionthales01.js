import { j3pAddElt, j3pAjouteBouton, j3pArrondi, j3pEmpty, j3pGetNewId, j3pGetRandomInt, j3pPGCD, j3pShowError, j3pShuffle } from 'src/legacy/core/functions'
import { j3pCreeFenetres, j3pAfficheCroixFenetres, j3pToggleFenetres } from 'src/legacy/core/functionsJqDialog'
import { j3pCreeSVG } from 'src/legacy/core/functionsSvg'
import Paint from 'src/legacy/outils/brouillon/Paint'
import ListeDeroulante from 'src/legacy/outils/listeDeroulante/ListeDeroulante'
import ZoneStyleMathquill1 from 'src/legacy/outils/zoneStyleMathquill/ZoneStyleMathquill1'
import ZoneStyleMathquill2 from 'src/legacy/outils/zoneStyleMathquill/ZoneStyleMathquill2'
import BrouillonCalculs from 'src/legacy/outils/brouillon/BrouillonCalculs'
import { getPe, initCptPe } from 'src/legacy/sections/college/helperTommy'
import { addDefaultTable } from 'src/legacy/themes/table'
import { j3pAffiche } from 'src/lib/mathquill/functions'
import { affichefois, afficheFrac } from 'src/legacy/outils/zoneStyleMathquill/functions'
import textesGeneriques from 'src/lib/core/textes'
import { getMtgCore } from 'src/lib/outils/mathgraph'
import { addMaj } from 'src/lib/utils/string'

const { cBien, cFaux, essaieEncore, reponseIncomplete, tempsDepasse } = textesGeneriques

/**
 * Ajouter une description ici
 * @fileOverview
 * @author Tommy Barroy <t.barroy@laposte.net>
 * @since 2017-10
 */

/**
 * Paramètres communs à thales01 et thales02
 */
export const params = {
  outils: ['mathquill', 'calculatrice'],
  /**
   * Ce qui est paramétrable dans l’éditeur de graphe
   * @type {LegacySectionParamsParametres}
   */
  parametres: [
    ['nbrepetitions', 9, 'entier', 'Nombre de répétitions de la section'],
    ['nbchances', 2, 'entier', 'Nombre d’essais possibles.'],
    ['Description', true, 'boolean', '<u>true</u>:  L’élève doit renseigner les conditions'],
    ['Enonce', 'Figure', 'liste', '<u>Figure</u>:  L’énoncé est présenté sur une figure <br><br> <u>Texte</u> L’énoncé est un texte.', ['Figure', 'Texte', 'Les deux']],
    ['Brouillon', true, 'boolean', '<u>true</u>:  L’élève dispose d’un brouillon quand l’énoncé est un texte.'],
    ['Egalite', true, 'boolean', '<u>true</u>:  L’élève doit écrire l’égalité.'],
    ['Reponse', true, 'boolean', '<u>true</u>:  L’élève doit donner la réponse.'],
    ['Entiers', true, 'boolean', '<u>true</u>:  Les longueurs sont des nombres entiers.'],
    ['Simplifie', true, 'boolean', '<u>true</u>:  Si la réponse est une fraction, elle doit être simplifiée.'],
    ['Couleur', true, 'boolean', '<u>true</u>:  Les deux triangles sont de couleurs différentes.'],
    ['Approximation', false, 'boolean', '<u>true</u>:  Une approximation décimale du résultat est demandée.'],
    ['Calculatrice', true, 'boolean', '<u>true</u>:  Une calculatrice est disponible'],
    // le modèle gère la compatibilité ascendante avec l’ancien paramètre MiseEnPagePlusClaire
    ['theme', 'standard', 'liste', 'Modèle pour l’affichage', ['standard', 'zonesAvecImageDeFond']]
  ],
  pe: [
    { pe_1: 'Egalite' },
    { pe_2: 'Ecriture' },
    { pe_3: 'Produit_en_croix' },
    { pe_4: 'Simplification' }
  ]
}

/**
 * section thales01
 * @this {Parcours}
 */
export default function main () {
  const me = this
  const ds = me.donneesSection
  const stor = me.storage
  let svgId = stor.svgId

  function creeLesDiv () {
    stor.lesdiv = {}
    stor.lesdiv.conteneur = j3pAddElt(me.zonesElts.MG, 'div', {
      style: me.styles.etendre('toutpetit.enonce', { padding: '10px' })
    })
    stor.lesdiv.consigneG = addDefaultTable(stor.lesdiv.conteneur, 1, 1)[0][0]
    const tabL1 = addDefaultTable(stor.lesdiv.conteneur, 1, 3)
    stor.lesdiv.travail = tabL1[0][2]
    stor.lesdiv.figure = tabL1[0][0]
    tabL1[0][1].style.width = '5px'
    stor.lesdiv.explications = j3pAddElt(stor.lesdiv.conteneur, 'div')
    stor.lesdiv.solution = j3pAddElt(stor.lesdiv.conteneur, 'div')
    stor.lesdiv.calculatrice = j3pAddElt(me.zonesElts.MD, 'div', {
      style: me.styles.etendre('toutpetit.enonce', { padding: '10px' })
    })
    stor.lesdiv.correction = j3pAddElt(me.zonesElts.MD, 'div', {
      style: me.styles.etendre('toutpetit.enonce', { padding: '10px' })
    })
    stor.lesdiv.solution.style.color = me.styles.toutpetit.correction.color
    stor.lesdiv.explications.style.color = me.styles.cfaux
  }
  function poseQuestion () {
    stor.niv = 0
    if (ds.Egalite && !ds.Reponse) { j3pAffiche(stor.lesdiv.consigneG, null, '<b> On veut écrire l’égalité de Thalès </b> dans le triangle ' + stor.nomtri + '. \n') } else {
      j3pAffiche(stor.lesdiv.consigneG, null, '<b> On veut calculer la longueur  $' + stor.lch + '$ </b> \n')
    }
    stor.lespar = []
    stor.tabCont = addDefaultTable(stor.lesdiv.travail, 5, 1)
    if (ds.Description) {
      poseDescription()
    } else {
      rempliDescription()
    }
  }
  function poseDescription () {
    stor.encours = 'Descrip'
    const tabDes = addDefaultTable(stor.tabCont[0][0], 5, 1)
    j3pAffiche(tabDes[0][0], null, 'Dans le triangle ' + stor.nomtri + ',')
    const tabParaDes = addDefaultTable(tabDes[1][0], 1, 2)
    const tabPaDes = addDefaultTable(tabDes[2][0], 1, 2)
    j3pAffiche(tabParaDes[0][0], null, 'le point ' + stor.lettres[3] + ' appartient au côté &nbsp;')
    j3pAffiche(tabPaDes[0][0], null, 'le point ' + stor.lettres[4] + ' appartient au côté &nbsp;')
    let ll = ['[' + stor.lettres[1] + stor.lettres[2] + ']', '[' + stor.lettres[1] + stor.lettres[0] + ']', '[' + stor.lettres[0] + stor.lettres[2] + ']']
    ll = j3pShuffle(ll)
    ll.splice(0, 0, 'Choisir')
    stor.listeAp1 = ListeDeroulante.create(tabParaDes[0][1], ll)
    stor.listeAp2 = ListeDeroulante.create(tabPaDes[0][1], ll)
    const tabHg = addDefaultTable(tabDes[3][0], 1, 2)
    j3pAffiche(tabHg[0][1], null, '&nbsp; sont parallèles. ')
    ll = ['les droites $(' + stor.lettres[1] + stor.lettres[2] + ')$ et $(' + stor.lettres[3] + stor.lettres[4] + ')$', 'les droites $(' + stor.lettres[1] + stor.lettres[0] + ')$ et $(' + stor.lettres[3] + stor.lettres[4] + ')$', 'les droites $(' + stor.lettres[0] + stor.lettres[2] + ')$ et $(' + stor.lettres[3] + stor.lettres[4] + ')$']
    ll = j3pShuffle(ll)
    ll.splice(0, 0, 'Choisir')
    stor.listePara = ListeDeroulante.create(tabHg[0][0], ll, { centre: true })
    ll = ['J’utilise la propriété de Thalès,', 'J’utilise le théorème de Pythagore,', 'J’utilise une propriété,']
    ll = j3pShuffle(ll)
    ll.splice(0, 0, 'Choisir')
    stor.listeProp = ListeDeroulante.create(tabDes[4][0], ll)
  }
  function rempliDescription () {
    j3pAffiche(stor.tabCont[0][0], null, 'Dans le triangle ' + stor.nomtri + ', \n')
    j3pAffiche(stor.tabCont[0][0], null, 'le point ' + stor.lettres[3] + ' appartient au côté [' + stor.lettres[1] + stor.lettres[0] + '] , \n')
    j3pAffiche(stor.tabCont[0][0], null, 'le point ' + stor.lettres[4] + ' appartient au côté [' + stor.lettres[1] + stor.lettres[2] + '] , \n')
    j3pAffiche(stor.tabCont[0][0], null, 'les droites (' + stor.lettres[3] + stor.lettres[4] + ') et (' + stor.lettres[0] + stor.lettres[2] + ') sont parallèles. \n')
    j3pAffiche(stor.tabCont[0][0], null, 'J’utilise la propriété de Thalès ,')
    poseEgalite()
  }
  function poseEgalite () {
    stor.encours = 'egalite'
    if (!ds.Egalite) {
      rempliEgalite()
      return
    }
    const tabEg = addDefaultTable(stor.tabCont[1][0], 1, 6)
    j3pAffiche(tabEg[0][0], null, 'donc &nbsp;')
    j3pAffiche(tabEg[0][2], null, ' $ = $ ')
    j3pAffiche(tabEg[0][4], null, ' $ = $ ')
    const f1 = afficheFrac(tabEg[0][1])
    const f2 = afficheFrac(tabEg[0][3])
    const f3 = afficheFrac(tabEg[0][5])
    let ll = [stor.lettres[1] + stor.lettres[0], stor.lettres[1] + stor.lettres[2], stor.lettres[0] + stor.lettres[2], stor.lettres[1] + stor.lettres[3], stor.lettres[1] + stor.lettres[4], stor.lettres[3] + stor.lettres[4], stor.lettres[0] + stor.lettres[3], stor.lettres[2] + stor.lettres[4]]
    ll = j3pShuffle(ll)
    ll.splice(0, 0, '--')
    stor.listeE1 = ListeDeroulante.create(f1[0], ll, { centre: true })
    stor.listeE2 = ListeDeroulante.create(f1[2], ll, { centre: true })
    stor.listeE3 = ListeDeroulante.create(f2[0], ll, { centre: true })
    stor.listeE4 = ListeDeroulante.create(f2[2], ll, { centre: true })
    stor.listeE5 = ListeDeroulante.create(f3[0], ll, { centre: true })
    stor.listeE6 = ListeDeroulante.create(f3[2], ll, { centre: true })
  }
  function rempliEgalite () {
    j3pAffiche(stor.tabCont[1][0], null, 'donc $\\frac{' + stor.lettres[1] + stor.lettres[3] + '}{' + stor.lettres[1] + stor.lettres[0] + '} = \\frac{' + stor.lettres[1] + stor.lettres[4] + '}{' + stor.lettres[1] + stor.lettres[2] + '} = \\frac{' + stor.lettres[3] + stor.lettres[4] + '}{' + stor.lettres[0] + stor.lettres[2] + '}$')
    stor.listeE1 = { reponse: stor.lettres[1] + stor.lettres[3] }
    stor.listeE2 = { reponse: stor.lettres[1] + stor.lettres[0] }
    stor.listeE3 = { reponse: stor.lettres[1] + stor.lettres[4] }
    stor.listeE4 = { reponse: stor.lettres[1] + stor.lettres[2] }
    stor.listeE5 = { reponse: stor.lettres[3] + stor.lettres[4] }
    stor.listeE6 = { reponse: stor.lettres[0] + stor.lettres[2] }
    poseRempli()
  }
  function poseRempli () {
    stor.encours = 'rempli'
    stor.lespar = []
    const tabRemp = addDefaultTable(stor.tabCont[2][0], 1, 6)
    j3pAffiche(tabRemp[0][0], null, 'donc &nbsp;')
    j3pAffiche(tabRemp[0][2], null, ' $ = $ ')
    j3pAffiche(tabRemp[0][4], null, ' $ = $ ')
    const f1 = afficheFrac(tabRemp[0][1])
    const f2 = afficheFrac(tabRemp[0][3])
    const f3 = afficheFrac(tabRemp[0][5])
    if (stor.nominc.indexOf(stor.listeE1.reponse) === -1) { stor.z1 = new ZoneStyleMathquill1(f1[0], { restric: '0123456789.,', enter: me.sectionCourante.bind(me) }) } else { j3pAffiche(f1[0], null, '$' + stor.listeE1.reponse + '$'); stor.z1 = { ffalse: true } }
    if (stor.nominc.indexOf(stor.listeE2.reponse) === -1) { stor.z2 = new ZoneStyleMathquill1(f1[2], { restric: '0123456789.,', enter: me.sectionCourante.bind(me) }) } else { j3pAffiche(f1[2], null, '$' + stor.listeE2.reponse + '$'); stor.z2 = { ffalse: true } }
    if (stor.nominc.indexOf(stor.listeE3.reponse) === -1) { stor.z3 = new ZoneStyleMathquill1(f2[0], { restric: '0123456789.,', enter: me.sectionCourante.bind(me) }) } else { j3pAffiche(f2[0], null, '$' + stor.listeE3.reponse + '$'); stor.z3 = { ffalse: true } }
    if (stor.nominc.indexOf(stor.listeE4.reponse) === -1) { stor.z4 = new ZoneStyleMathquill1(f2[2], { restric: '0123456789.,', enter: me.sectionCourante.bind(me) }) } else { j3pAffiche(f2[2], null, '$' + stor.listeE4.reponse + '$'); stor.z4 = { ffalse: true } }
    if (stor.nominc.indexOf(stor.listeE5.reponse) === -1) { stor.z5 = new ZoneStyleMathquill1(f3[0], { restric: '0123456789.,', enter: me.sectionCourante.bind(me) }) } else { j3pAffiche(f3[0], null, '$' + stor.listeE5.reponse + '$'); stor.z5 = { ffalse: true } }
    if (stor.nominc.indexOf(stor.listeE6.reponse) === -1) { stor.z6 = new ZoneStyleMathquill1(f3[2], { restric: '0123456789.,', enter: me.sectionCourante.bind(me) }) } else { j3pAffiche(f3[2], null, '$' + stor.listeE6.reponse + '$'); stor.z6 = { ffalse: true } }
  }
  function poseProduit () {
    stor.encours = 'prod'
    stor.tabProDL = addDefaultTable(stor.tabCont[3][0], 3, 4)
    stor.lespar = []
    j3pAffiche(stor.tabProDL[0][0], null, ' donc $ ' + stor.lch + ' $')
    j3pAffiche(stor.tabProDL[0][1], null, '$ = $')
    const t = afficheFrac(stor.tabProDL[0][2])
    const t2 = affichefois(t[0])
    stor.zP1 = new ZoneStyleMathquill1(t2[0], { restric: '0123456789.,', enter: me.sectionCourante.bind(me) })
    stor.zP2 = new ZoneStyleMathquill1(t2[2], { restric: '0123456789.,', enter: me.sectionCourante.bind(me) })
    stor.zP3 = new ZoneStyleMathquill1(t[2], { restric: '0123456789.,', enter: me.sectionCourante.bind(me) })
  }
  function poseFin () {
    stor.encours = 'concl'
    stor.Yaapprox = false
    let bobi = '/'
    stor.brouillon1 = new BrouillonCalculs({
      caseBoutons: stor.tabProDL[1][0],
      caseEgal: stor.tabProDL[1][1],
      caseCalculs: stor.tabProDL[1][2],
      caseApres: stor.tabProDL[1][3]
    }, { limite: 30, limitenb: 20, restric: '0123456789*/,.', hasAutoKeyboard: false, lmax: 2 })
    j3pAffiche(stor.tabProDL[2][1], null, '$ = $')
    const tabF = addDefaultTable(stor.tabProDL[2][2], 1, 2)
    j3pAffiche(tabF[0][1], null, '&nbsp; cm')
    if (ds.Approximation) {
      const n1 = parseFloat(stor.atthaut[0].replace(',', '.')) * 10 * parseFloat(stor.atthaut[1].replace(',', '.')) * 10 / (parseFloat(stor.attbas.replace(',', '.')) * 100)
      const n2 = j3pArrondi(n1, 2)
      const n3 = j3pArrondi(n1, 10)
      if (n2 !== n3) {
        j3pEmpty(stor.tabProDL[2][1])
        j3pAffiche(stor.tabProDL[2][1], null, '$ \\approx $')
        j3pAffiche(stor.lesdiv.travail, null, '<i>Approximation décimale à $0,1$ cm près.</i>')
        bobi = ''
        stor.Yaapprox = true
      }
    } else if (ds.Simplifie) {
      j3pAffiche(stor.lesdiv.travail, null, '<i>La réponse doit être simplifiée.</i>')
    }
    stor.zFin = new ZoneStyleMathquill2(tabF[0][0], { id: 'dzFin', restric: '0123456789.,' + bobi, enter: me.sectionCourante.bind(me) })
  }
  function boutonfen () {
    j3pToggleFenetres('Calculatrice')
  }
  function faisChoixExos () {
    const e = ds.lesExos.pop()
    e.sam = e.prop !== '-'
    return e
  }

  function faiFigure () {
    const objet = {}
    let i

    objet.conteneur = stor.lesdiv.figure
    objet.listedeselemedessine = []
    objet.longx = 8.3
    objet.hauty = 8
    objet.fifig = []
    objet.lisecss = []

    objet.lettres = []
    for (i = 0; i < 6; i++) {
      addMaj(objet.lettres)
    }
    const lettres = objet.lettres
    stor.lettres = lettres
    stor.nomtri = lettres[0] + lettres[1] + lettres[2]
    stor.nomcote1 = '[' + lettres[0] + lettres[1] + ']'
    stor.nomcote2 = '[' + lettres[0] + lettres[2] + ']'
    stor.nomcote3 = '[' + lettres[1] + lettres[2] + ']'
    stor.nomlc1 = lettres[0] + lettres[1]
    stor.nomlc2 = lettres[0] + lettres[2]
    stor.nomlc3 = lettres[1] + lettres[2]

    switch (stor.Lexo.prop) {
      case 1:
        stor.visible = [false, false, false, true, true, true]
        stor.visible[j3pGetRandomInt(0, 2)] = true
        break
      case 2:
        switch (j3pGetRandomInt(0, 2)) {
          case 0:
            stor.visible = [false, true, true, true, false, false]
            stor.visible[j3pGetRandomInt(4, 5)] = true
            break
          case 1:
            stor.visible = [true, false, true, false, true, false]
            stor.visible[j3pGetRandomInt(0, 1) * 2 + 3] = true
            break
          case 2:
            stor.visible = [true, true, false, false, false, true]
            stor.visible[j3pGetRandomInt(3, 4)] = true
            break
        }
        break
      case 3:
        stor.visible = [true, true, true, false, false, false]
        stor.visible[j3pGetRandomInt(3, 5)] = true
        break
    }

    /// determine longueurs et angles
    if (!ds.Entiers) {
      stor.l1 = j3pGetRandomInt(250, 450)
      stor.l2 = j3pGetRandomInt(250, 450)
      if (stor.l2 === stor.l1) stor.l2 = 240
      stor.l3 = j3pGetRandomInt(250, 450)
      if ((stor.l3 === stor.l2) || (stor.l3 === stor.l1)) stor.l3 = 480
      stor.l4 = j3pGetRandomInt(500, 900)
      stor.l5 = j3pGetRandomInt(500, 900)
      if (stor.l4 === stor.l5) stor.l4 = 480
      stor.l6 = j3pGetRandomInt(500, 900)
      if ((stor.l6 === stor.l4) || (stor.l6 === stor.l5)) stor.l6 = 920
      stor.l1 = j3pArrondi(stor.l1 / 10, 2)
      stor.l2 = j3pArrondi(stor.l2 / 10, 2)
      stor.l3 = j3pArrondi(stor.l3 / 10, 2)
      stor.l4 = j3pArrondi(stor.l4 / 10, 2)
      stor.l5 = j3pArrondi(stor.l5 / 10, 2)
      stor.l6 = j3pArrondi(stor.l6 / 10, 2)
    } else {
      stor.l1 = j3pGetRandomInt(22, 40)
      stor.l2 = j3pGetRandomInt(21, 40)
      if (stor.l2 === stor.l1) stor.l2 = 20
      stor.l3 = j3pGetRandomInt(21, 40)
      if ((stor.l3 === stor.l2) || (stor.l3 === stor.l1)) stor.l3 = 41
      stor.l4 = j3pGetRandomInt(45, 80)
      stor.l5 = j3pGetRandomInt(45, 80)
      if (stor.l4 === stor.l5) stor.l4 = 43
      stor.l6 = j3pGetRandomInt(45, 80)
      if ((stor.l6 === stor.l4) || (stor.l6 === stor.l5)) stor.l6 = 82
    }

    stor.nominc = []
    if (stor.Lexo.enonc === 'Fig') {
      const txtFigure = 'TWF0aEdyYXBoSmF2YTEuMAAAABM+TMzNAAJmcv###wEA#wEAAAAAAAAAAANLAAACfwAAAQEAAAAAAAAAAQAAAVD#####AAAAAQAKQ0NhbGNDb25zdAD#####AAJwaQAWMy4xNDE1OTI2NTM1ODk3OTMyMzg0Nv####8AAAABAApDQ29uc3RhbnRlQAkh+1RELRj#####AAAAAQAKQ1BvaW50QmFzZQD#####AQAAfwAQAAAAAAAAAAAAAABACAAAAAAAAAAACQABQHOwAAAAAABAS#Cj1wo9cAAAAAIA#####wEAAP8AEAABQwAAAAAAAAAAAEAIAAAAAAAAAAAJAAFAdgAAAAAAAEB0zhR64Ueu#####wAAAAEAFENEcm9pdGVEaXJlY3Rpb25GaXhlAP####8BAAD#ARAAAAEAAAACAAAAAgE#8AAAAAAAAAAAAAMA#####wEAAP8BEAAAAQAAAAIAAAACAD#wAAAAAAAAAAAAAgD#####AAAAAAAQAAAAQAgAAAAAAAAAAAAAAAAAAAAACQAAQEUAAAAAAAFAfL4UeuFHrv####8AAAABAAdDQ2FsY3VsAP####8ABW1pbmkxAAEwAAAAAQAAAAAAAAAAAAAABAD#####AAVtYXhpMQADMzYwAAAAAUB2gAAAAAAA#####wAAAAEAFENJbXBsZW1lbnRhdGlvblByb3RvAP####8AB0N1cnNldXIAAAAFAAAABQAAAAMAAAAGAAAABwAAAAUAAAADAAAAAAgBAAAAABAAAAEAAAABAAAABQE#8AAAAAAAAP####8AAAABAA9DUG9pbnRMaWVEcm9pdGUBAAAACAAAAP8AEAAAAMAIAAAAAAAAP#AAAAAAAAAAAAUAAEBdQAAAAAAAAAAACf####8AAAABAAtDSG9tb3RoZXRpZQAAAAAIAAAABf####8AAAABAApDT3BlcmF0aW9uA#####8AAAABAA9DUmVzdWx0YXRWYWxldXIAAAAGAAAACAEAAAAJAAAABgAAAAkAAAAH#####wAAAAEAC0NQb2ludEltYWdlAAAAAAgBAAAAAA0AAk8xAMAQAAAAAAAAQBAAAAAAAAAAAAUAAAAACgAAAAsAAAAHAAAAAAgAAAAFAAAACAMAAAAIAQAAAAE#8AAAAAAAAAAAAAkAAAAGAAAACAEAAAAJAAAABwAAAAkAAAAGAAAACgAAAAAIAQAAAAANAAJJMQDAAAAAAAAAAEAIAAAAAAAAAAAFAAAAAAoAAAAN#####wAAAAEACENTZWdtZW50AQAAAAgAAAD#ABAAAAEAAAACAAAABQAAAAoAAAAGAQAAAAgAAAD#ARAAAmsxAMAAAAAAAAAAQAAAAAAAAAAAAAEAAT#ptZtZtZtaAAAAD#####8AAAACAA9DTWVzdXJlQWJzY2lzc2UBAAAACAACYTEAAAAMAAAADgAAABD#####AAAAAQAPQ1ZhbGV1ckFmZmljaGVlAQAAAAgAAAD#AAAAAAAAAAAAwBgAAAAAAAAAAAAAABAPAAH###8AAAABAAAAAgAAAAEAAAAAAAAAAAAAAAACAAAAEQAAAAIA#####wAAAAAAEAAAAEAIAAAAAAAAAAAAAAAAAAAAAAkAAEBDgAAAAAABQH8+FHrhR64AAAAEAP####8ABW1pbmkyAAEwAAAAAQAAAAAAAAAAAAAABAD#####AAVtYXhpMgACMTAAAAABQCQAAAAAAAAAAAAFAP####8AB0N1cnNldXIAAAAFAAAABQAAAAMAAAAUAAAAFQAAABMAAAADAAAAABYBAAAAABAAAAEAAAABAAAAEwE#8AAAAAAAAAAAAAYBAAAAFgAAAP8AEAAAAMAIAAAAAAAAP#AAAAAAAAAAAAUAAEBdQAAAAAAAAAAAFwAAAAcAAAAAFgAAABMAAAAIAwAAAAkAAAAUAAAACAEAAAAJAAAAFAAAAAkAAAAVAAAACgAAAAAWAQAAAAANAAJPMQDAEAAAAAAAAEAQAAAAAAAAAAAFAAAAABgAAAAZAAAABwAAAAAWAAAAEwAAAAgDAAAACAEAAAABP#AAAAAAAAAAAAAJAAAAFAAAAAgBAAAACQAAABUAAAAJAAAAFAAAAAoAAAAAFgEAAAAADQACSTEAwAAAAAAAAABACAAAAAAAAAAABQAAAAAYAAAAGwAAAAsBAAAAFgAAAP8AEAAAAQAAAAIAAAATAAAAGAAAAAYBAAAAFgAAAP8BEAABawDAAAAAAAAAAEAAAAAAAAAAAAABAAEAAAAAAAAAAAAAAB0AAAAMAQAAABYAAmEyAAAAGgAAABwAAAAeAAAADQEAAAAWAAAA#wAAAAAAAAAAAMAYAAAAAAAAAAAAAAAeDwAB####AAAAAQAAAAIAAAABAAAAAAAAAAAAAAAAAgAAAB8AAAACAP####8AAAAAABAAAABACAAAAAAAAAAAAAAAAAAAAAAJAABARQAAAAAAAUCA7wo9cKPXAAAABAD#####AAVtaW5pMwABMAAAAAEAAAAAAAAAAAAAAAQA#####wAFbWF4aTMAAjEwAAAAAUAkAAAAAAAAAAAABQD#####AAdDdXJzZXVyAAAABQAAAAUAAAADAAAAIgAAACMAAAAhAAAAAwAAAAAkAQAAAAAQAAABAAAAAQAAACEBP#AAAAAAAAAAAAAGAQAAACQAAAD#ABAAAADACAAAAAAAAD#wAAAAAAAAAAAFAABAXUAAAAAAAAAAACUAAAAHAAAAACQAAAAhAAAACAMAAAAJAAAAIgAAAAgBAAAACQAAACIAAAAJAAAAIwAAAAoAAAAAJAEAAAAADQACTzEAwBAAAAAAAABAEAAAAAAAAAAABQAAAAAmAAAAJwAAAAcAAAAAJAAAACEAAAAIAwAAAAgBAAAAAT#wAAAAAAAAAAAACQAAACIAAAAIAQAAAAkAAAAjAAAACQAAACIAAAAKAAAAACQBAAAAAA0AAkkxAMAAAAAAAAAAQAgAAAAAAAAAAAUAAAAAJgAAACkAAAALAQAAACQAAAD#ABAAAAEAAAACAAAAIQAAACYAAAAGAQAAACQAAAD#ARAAAmsyAMAAAAAAAAAAQAAAAAAAAAAAAAEAAT#mJ2J2J2J2AAAAKwAAAAwBAAAAJAACYTMAAAAoAAAAKgAAACwAAAANAQAAACQAAAD#AAAAAAAAAAAAwBgAAAAAAAAAAAAAACwPAAH###8AAAABAAAAAgAAAAEAAAAAAAAAAAAAAAACAAAALQAAAAQA#####wACQTEAAmExAAAACQAAABEAAAAEAP####8AAkEyAAJhMgAAAAkAAAAfAAAABAD#####AAJBMwACYTMAAAAJAAAALQAAAAIA#####wEAAP8AEAAAAAAAAAAAAAAAQAgAAAAAAAAAAlAxCQABQGPAAAAAAABAZFwo9cKPXAAAAAIA#####wEAAP8AEAAAAAAAAAAAAAAAQAgAAAAAAAAAAlAyCQABQGTgAAAAAABAQvCj1wo9cf####8AAAABAAlDQ2VyY2xlT0EA#####wEAAP8AAAACAAAAMgAAADP#####AAAAAQAJQ1JvdGF0aW9uAP####8AAAAyAAAACQAAAC8AAAAKAP####8B#wAAABAAAAAAAAAAAAAAAEAIAAAAAAAAAAAJAAAAADMAAAA1AAAADwD#####AAAAMgAAAAgAAAAAAUBJAAAAAAAAAAAACAIAAAAIAwAAAAkAAAAwAAAAAUAkAAAAAAAAAAAAAUBRgAAAAAAAAAAACgD#####Af8AAAAQAAAAAAAAAAAAAABACAAAAAAAAAAACQAAAAA2AAAANwAAAA8A#####wAAADIAAAAIAAAAAAFAZoAAAAAAAAAAAAgCAAAACAMAAAAJAAAAMQAAAAFAJAAAAAAAAAAAAAFAYEAAAAAAAAAAAAoA#####wH#AAAAEAAAAAAAAAAAAAAAQAgAAAAAAAAAAAkAAAAANgAAADkAAAALAP####8AAAAAABAAAAEAAnMxAAUAAAA6AAAANgAAAAsA#####wAAAAAAEAAAAQACczIABQAAADYAAAA4AAAACwD#####AAAAAAAQAAABAAJzMwAFAAAAOAAAADoAAAACAP####8A#wAAABAAAUEAAAAAAAAAAABACAAAAAAAAAAACQABQEkAAAAAAAFAgncKPXCj1wAAAAIA#####wD#AAAAEAABQgAAAAAAAAAAAEAIAAAAAAAAAAAJAAFAV0AAAAAAAECChwo9cKPX#####wAAAAEACUNMb25ndWV1cgD#####AAAAPgAAAD######AAAAAgAJQ0NlcmNsZU9SAP####8B#wAAAAAAAgAAADoAAAABQAAAAAAAAAAAAAAAEQD#####Af8AAAAAAAIAAAA2AAAAAUAAAAAAAAAAAP####8AAAABABBDSW50RHJvaXRlQ2VyY2xlAP####8AAAA7AAAAQf####8AAAABABBDUG9pbnRMaWVCaXBvaW50AP####8B#wAAABAAAAAAAAAAAAAAAEAIAAAAAAAAAAAJAAEAAABDAAAAEwD#####AP8AAAAQAAAAAAAAAAAAAABACAAAAAAAAAAACQACAAAAQwAAABIA#####wAAADsAAABCAAAAEwD#####AP8AAAAQAAAAAAAAAAAAAABACAAAAAAAAAAACQABAAAARgAAABMA#####wH#AAAAEAAAAAAAAAAAAAAAQAgAAAAAAAAAAAkAAgAAAEYAAAACAP####8AAAAAABAAAABACAAAAAAAAAAAAAAAAAAAAAAJAABAamAAAAAAAEB8rhR64UeuAAAABAD#####AAVtaW5pNAABMAAAAAEAAAAAAAAAAAAAAAQA#####wAFbWF4aTQAAjEwAAAAAUAkAAAAAAAAAAAABQD#####AAdDdXJzZXVyAAAABQAAAAUAAAADAAAASgAAAEsAAABJAAAAAwAAAABMAQAAAAAQAAABAAAAAQAAAEkBP#AAAAAAAAAAAAAGAQAAAEwA#wAAABAAAADACAAAAAAAAD#wAAAAAAAAAAAFAABAXUAAAAAAAAAAAE0AAAAHAAAAAEwAAABJAAAACAMAAAAJAAAASgAAAAgBAAAACQAAAEoAAAAJAAAASwAAAAoAAAAATAEAAAAADQACTzEAwBAAAAAAAABAEAAAAAAAAAAABQAAAABOAAAATwAAAAcAAAAATAAAAEkAAAAIAwAAAAgBAAAAAT#wAAAAAAAAAAAACQAAAEoAAAAIAQAAAAkAAABLAAAACQAAAEoAAAAKAAAAAEwBAAAAAA0AAkkxAMAAAAAAAAAAQAgAAAAAAAAAAAUAAAAATgAAAFEAAAALAQAAAEwA#wAAABAAAAEAAAACAAAASQAAAE4AAAAGAQAAAEwA#wAAARAAAmszAMAAAAAAAAAAQAAAAAAAAAAAAAEAAT#hxxxxxxxyAAAAUwAAAAwBAAAATAACYTQAAABQAAAAUgAAAFQAAAANAQAAAEwA#wAAAAAAAAAAAAAAwBgAAAAAAAAAAAAAAFQPAAH###8AAAABAAAAAgAAAAEAAAAAAAAAAAAAAAACAAAAVQAAAAQA#####wACQTQAAmE0AAAACQAAAFUAAAAHAP####8AAABEAAAACAMAAAAJAAAAVwAAAAFAJAAAAAAAAAAAAAoA#####wH#AAAAEAAAAAAAAAAAAAAAQAgAAAAAAAAAAAkAAAAASAAAAFj#####AAAAAQAQQ0Ryb2l0ZVBhcmFsbGVsZQD#####Af8AAAAQAAABAAAAAgAAAFkAAAA8#####wAAAAEAEENJbnREcm9pdGVEcm9pdGUA#####wH#AAAAEAAAAAAAAAAAAAAAQAgAAAAAAAAAAAkAAAAAWgAAAD0AAAALAP####8AAAAAABAAAAEAAnM0AAMAAABZAAAAWwAAAAsA#####wAAAAAAEAAAAQACczUAAwAAADoAAABZAAAACwD#####AAAAAAAQAAABAAJzNgADAAAAOgAAAFv#####AAAAAQAWQ0Ryb2l0ZVBlcnBlbmRpY3VsYWlyZQD#####AX8AfwAQAAABAAAAAwAAADgAAAA9AAAAFgD#####AX8AfwAQAAABAAAAAwAAADoAAAA9AAAAEQD#####AX8AfwAAAQEAAABbAAAAAT#gAAAAAAAAAAAAABYA#####wF#AH8AEAAAAQAAAQEAAABbAAAAXAAAABYA#####wF#AH8AEAAAAQAAAQEAAABZAAAAXAAAABEA#####wF#AH8AAAEBAAAAWQAAAAE#4AAAAAAAAAAAAAASAP####8AAABiAAAAYQAAABMA#####wF#AH8AEAAAAAAAAAAAAAAAQAgAAAAAAAAAAAkAAgAAAGUAAAATAP####8BfwB#ABAAAAAAAAAAAAAAAEAIAAAAAAAAAAAJAAEAAABlAAAAEgD#####AAAAYwAAAGQAAAATAP####8BfwB#ABAAAAAAAAAAAAAAAEAIAAAAAAAAAAAJAAIAAABoAAAAEwD#####AX8AfwAQAAAAAAAAAAAAAABACAAAAAAAAAAACQABAAAAaP####8AAAABAAdDTWlsaWV1AP####8BfwB#ABAAAAAAAAAAAAAAAEAIAAAAAAAAAAAJAAAAAGoAAABnAAAAFgD#####AX8AfwAQAAABAAACAQAAAFsAAAA9AAAAEgD#####AAAAbAAAAGEAAAATAP####8BfwB#ABAAAAAAAAAAAAAAAEAIAAAAAAAAAAAJAAIAAABtAAAAEwD#####AQAAfwAQAAAAAAAAAAAAAABACAAAAAAAAAAACAABAAAAbQAAABYA#####wF#AH8AEAAAAQAAAgEAAABvAAAAbAAAABUA#####wF#AH8AEAAAAAAAAAAAAAAAQAgAAAAAAAAAAAkAAAAAYAAAAHAAAAAXAP####8BfwB#ABAAAAAAAAAAAAAAAEAIAAAAAAAAAAAJAAAAAHEAAABv#####wAAAAEAEUNTeW1ldHJpZUNlbnRyYWxlAP####8AAABxAAAACgD#####AX8AfwAQAAAAAAAAAAAAAABACAAAAAAAAAAACQAAAAA6AAAAcwAAABgA#####wAAAG8AAAAKAP####8BfwB#ABAAAAAAAAAAAAAAAEAIAAAAAAAAAAAJAAAAAG4AAAB1AAAAGAD#####AAAAdAAAAAoA#####wF#AH8AEAAAAAAAAAAAAAAAQAgAAAAAAAAAAAkAAAAAcQAAAHcAAAAWAP####8BfwB#ABAAAAEAAAIBAAAAdAAAAGAAAAAVAP####8BfwB#ABAAAAAAAAAAAAAAAEAIAAAAAAAAAAAJAAAAAF8AAAB5AAAAFgD#####AX8AfwAQAAABAAACAQAAAHgAAABgAAAAFQD#####AX8AfwAQAAAAAAAAAAAAAABACAAAAAAAAAAACQAAAABfAAAAe#####8AAAABAAhDVmVjdGV1cgD#####AH8AfwAQAAABAAN2MTECAQAAAHoAAAB0AAAAABkA#####wB#AH8AEAAAAQADdjIxAgEAAAB0AAAAegAAAAALAP####8AfwB#ABAAAAEAA3MxMQEBAAAAfAAAADgAAAALAP####8AfwB#ABAAAAEAA3MyMQEBAAAAeAAAADoAAAAXAP####8BfwB#ABAAAAAAAAAAAAAAAEAIAAAAAAAAAAAJAAAAAHgAAAB8AAAAFgD#####AX8AfwAQAAABAAABAQAAADgAAAA8AAAAFgD#####AX8AfwAQAAABAAABAQAAADYAAAA8AAAAEQD#####AX8AfwAAAQEAAAA4AAAAAT#gAAAAAAAAAAAAABEA#####wF#AH8AAAEBAAAANgAAAAE#4AAAAAAAAAAAAAASAP####8AAACCAAAAhAAAABMA#####wF#AH8AEAAAAAAAAAAAAAAAQAgAAAAAAAAAAAkAAgAAAIYAAAATAP####8BAAB#ABAAAAAAAAAAAAAAAEAIAAAAAAAAAAAIAAEAAACGAAAAEgD#####AAAAgwAAAIUAAAATAP####8BfwB#ABAAAAAAAAAAAAAAAEAIAAAAAAAAAAAJAAIAAACJAAAAEwD#####AQAAfwAQAAAAAAAAAAAAAABACAAAAAAAAAAACAABAAAAiQAAABcA#####wF#AH8AEAAAAAAAAAAAAAAAQAgAAAAAAAAAAAkAAAAAiwAAAIgAAAAWAP####8BfwB#ABAAAAEAAAEBAAAAOgAAAF0AAAAWAP####8BfwB#ABAAAAEAAAEBAAAANgAAADsAAAAWAP####8BfwB#ABAAAAEAAAEBAAAAWQAAADsAAAARAP####8BfwB#AAABAQAAADoAAAABP+AAAAAAAAAAAAAAEgD#####AAAAjQAAAJAAAAATAP####8BfwB#ABAAAAAAAAAAAAAAAEAIAAAAAAAAAAAJAAIAAACRAAAAEwD#####AX8AfwAQAAAAAAAAAAAAAABACAAAAAAAAAAACQABAAAAkQAAABYA#####wF#AH8AEAAAAQAAAQEAAACTAAAAjQAAABUA#####wEAAH8AEAAAAAAAAAAAAAAAQAgAAAAAAAAAAAgAAAAAjwAAAJQAAAAXAP####8BfwB#ABAAAAAAAAAAAAAAAEAIAAAAAAAAAAAJAAAAAJMAAACVAAAAGAD#####AAAAkwAAAAoA#####wF#AH8AEAAAAAAAAAAAAAAAQAgAAAAAAAAAAAkAAAAAOgAAAJcAAAAWAP####8BfwB#ABAAAAEAAAEBAAAAmAAAAI8AAAAVAP####8BfwB#ABAAAAAAAAAAAAAAAEAIAAAAAAAAAAAJAAAAAI4AAACZAAAAGAD#####AAAAmAAAAAoA#####wF#AH8AEAAAAAAAAAAAAAAAQAgAAAAAAAAAAAkAAAAAkwAAAJsAAAAWAP####8BfwB#ABAAAAEAAAEBAAAAnAAAAI8AAAAVAP####8BfwB#ABAAAAAAAAAAAAAAAEAIAAAAAAAAAAAJAAAAAI4AAACdAAAAGQD#####AH8AfwAQAAABAAN2MTICAQAAAJgAAACaAAAAABkA#####wB#AH8AEAAAAQADdjIyAgEAAACaAAAAmAAAAAAXAP####8BfwB#ABAAAAAAAAAAAAAAAEAIAAAAAAAAAAAJAAAAAJwAAACeAAAACwD#####AH8AfwAQAAABAANzMTIBAQAAAJ4AAAA2AAAACwD#####AH8AfwAQAAABAANzMjIBAQAAAJwAAAA6AAAAFgD#####AX8AfwAQAAABAAABAQAAAHwAAAAEAAAAFgD#####AX8AfwAQAAABAAABAQAAAIEAAAAEAAAAFgD#####AX8AfwAQAAABAAABAQAAAHgAAAAEAAAAFgD#####AX8AfwAQAAABAAABAQAAAJwAAAAEAAAAFgD#####AX8AfwAQAAABAAABAQAAAIgAAAAEAAAAFgD#####AX8AfwAQAAABAAABAQAAAJ4AAAAEAAAAFgD#####AX8AfwAQAAABAAABAQAAAIsAAAAEAAAAFQD#####AQAA#wAQAAFKAAAAAAAAAAAAQAgAAAAAAAAAAAkAAAAABAAAAKgAAAAVAP####8BAAD#ABAAAUQAAAAAAAAAAABACAAAAAAAAAAACQAAAAAEAAAApwAAABUA#####wEAAP8AEAABRQAAAAAAAAAAAEAIAAAAAAAAAAAJAAAAAAQAAACmAAAAFQD#####AQAA#wAQAAFGAAAAAAAAAAAAQAgAAAAAAAAAAAkAAAAABAAAAKUAAAAVAP####8BAAD#ABAAAUcAAAAAAAAAAABACAAAAAAAAAAACQAAAAAEAAAAqQAAABUA#####wEAAP8AEAABSAAAAAAAAAAAAEAIAAAAAAAAAAAJAAAAAAQAAACqAAAAFQD#####AQAA#wAQAAFJAAAAAAAAAAAAQAgAAAAAAAAAAAkAAAAABAAAAKQAAAAFAP####8AEk1lc3VyZSBkZSBsb25ndWV1cgAAAAUAAAACAAAAAgAAAAIAAACs#####wAAAAEAC0NNZWRpYXRyaWNlAAAAALIBAAAAABAAAAEAAAABAAAAAgAAAKwAAAAXAAAAALIBAAAAABAAAAEAAAUAAAAAAgAAAKwAAAARAAAAALIBAAAAAAAAAQAAALQAAAABQDAAAAAAAAABAAAAEgAAAACyAAAAswAAALUAAAATAAAAALIBAAAAABAAAAEAAAUAAQAAALYAAAAQAQAAALIAAAACAAAArAAAAA0BAAAAsgEAAP8BAAAAAAC3EQAAAAAAAQAAAAEAAAABAAAAAAAAAAAAAAAAAQAAALgAAAAFAP####8AEk1lc3VyZSBkZSBsb25ndWV1cgAAAAUAAAACAAAAAgAAAAIAAACtAAAAGgAAAAC6AQAAAAAQAAABAAAAAQAAAAIAAACtAAAAFwAAAAC6AQAAAAAQAAABAAAFAAAAAAIAAACtAAAAEQAAAAC6AQAAAAAAAAEAAAC8AAAAAUAwAAAAAAAAAQAAABIAAAAAugAAALsAAAC9AAAAEwAAAAC6AQAAAAAQAAABAAAFAAEAAAC+AAAAEAEAAAC6AAAAAgAAAK0AAAANAQAAALoBAAD#AQAAAAAAvxEAAAAAAAEAAAABAAAAAQAAAAAAAAAAAAAAAAEAAADAAAAABQD#####ABJNZXN1cmUgZGUgbG9uZ3VldXIAAAAFAAAAAgAAAAIAAAACAAAArgAAABoAAAAAwgEAAAAAEAAAAQAAAAEAAAACAAAArgAAABcAAAAAwgEAAAAAEAAAAQAABQAAAAACAAAArgAAABEAAAAAwgEAAAAAAAABAAAAxAAAAAFAMAAAAAAAAAEAAAASAAAAAMIAAADDAAAAxQAAABMAAAAAwgEAAAAAEAAAAQAABQABAAAAxgAAABABAAAAwgAAAAIAAACuAAAADQEAAADCAQAA#wEAAAAAAMcRAAAAAAABAAAAAQAAAAEAAAAAAAAAAAAAAAABAAAAyAAAAAUA#####wASTWVzdXJlIGRlIGxvbmd1ZXVyAAAABQAAAAIAAAACAAAAAgAAAK8AAAAaAAAAAMoBAAAAABAAAAEAAAABAAAAAgAAAK8AAAAXAAAAAMoBAAAAABAAAAEAAAUAAAAAAgAAAK8AAAARAAAAAMoBAAAAAAAAAQAAAMwAAAABQDAAAAAAAAABAAAAEgAAAADKAAAAywAAAM0AAAATAAAAAMoBAAAAABAAAAEAAAUAAQAAAM4AAAAQAQAAAMoAAAACAAAArwAAAA0BAAAAygEAAP8BAAAAAADPEQAAAAAAAQAAAAEAAAABAAAAAAAAAAAAAAAAAQAAANAAAAAFAP####8AEk1lc3VyZSBkZSBsb25ndWV1cgAAAAUAAAACAAAAAgAAAAIAAACwAAAAGgAAAADSAQAAAAAQAAABAAAAAQAAAAIAAACwAAAAFwAAAADSAQAAAAAQAAABAAAFAAAAAAIAAACwAAAAEQAAAADSAQAAAAAAAAEAAADUAAAAAUAwAAAAAAAAAQAAABIAAAAA0gAAANMAAADVAAAAEwAAAADSAQAAAAAQAAABAAAFAAEAAADWAAAAEAEAAADSAAAAAgAAALAAAAANAQAAANIBAAD#AQAAAAAA1xEAAAAAAAEAAAABAAAAAQAAAAAAAAAAAAAAAAEAAADYAAAABQD#####ABJNZXN1cmUgZGUgbG9uZ3VldXIAAAAFAAAAAgAAAAIAAAACAAAAsQAAABoAAAAA2gEAAAAAEAAAAQAAAAEAAAACAAAAsQAAABcAAAAA2gEAAAAAEAAAAQAABQAAAAACAAAAsQAAABEAAAAA2gEAAAAAAAABAAAA3AAAAAFAMAAAAAAAAAEAAAASAAAAANoAAADbAAAA3QAAABMAAAAA2gEAAAAAEAAAAQAABQABAAAA3gAAABABAAAA2gAAAAIAAACxAAAADQEAAADaAQAA#wEAAAAAAN8RAAAAAAABAAAAAQAAAAEAAAAAAAAAAAAAAAABAAAA4AAAAAUA#####wASTWVzdXJlIGRlIGxvbmd1ZXVyAAAABQAAAAIAAAACAAAAAgAAAKsAAAAaAAAAAOIBAAAAABAAAAEAAAABAAAAAgAAAKsAAAAXAAAAAOIBAAAAABAAAAEAAAUAAAAAAgAAAKsAAAARAAAAAOIBAAAAAAAAAQAAAOQAAAABQDAAAAAAAAABAAAAEgAAAADiAAAA4wAAAOUAAAATAAAAAOIBAAAAABAAAAEAAAUAAQAAAOYAAAAQAQAAAOIAAAACAAAAqwAAAA0BAAAA4gEAAP8BAAAAAADnEQAAAAAAAQAAAAEAAAABAAAAAAAAAAAAAAAAAQAAAOgAAAAEAP####8AAlIxADJtYXgoQ0QsbWF4KENFLG1heChDRixtYXgoQ0csbWF4KENILG1heChDSSxDSikpKSkpKf####8AAAABAA1DRm9uY3Rpb24yVmFyAAAAAAkAAAC4AAAAGwAAAAAJAAAAwAAAABsAAAAACQAAAMgAAAAbAAAAAAkAAADQAAAAGwAAAAAJAAAA2AAAABsAAAAACQAAAOAAAAAJAAAA6AAAAAQA#####wACcjEAMm1pbihDRCxtaW4oQ0UsbWluKENGLG1pbihDRyxtaW4oQ0gsbWF4KENJLENKKSkpKSkpAAAAGwEAAAAJAAAAuAAAABsBAAAACQAAAMAAAAAbAQAAAAkAAADIAAAAGwEAAAAJAAAA0AAAABsBAAAACQAAANgAAAAbAAAAAAkAAADgAAAACQAAAOgAAAARAP####8BAAD#AAABAQAAAAIAAAAJAAAA6gAAAAARAP####8BAAD#AAABAQAAAAIAAAAJAAAA6wAAAAASAP####8AAAAEAAAA7QAAABMA#####wEAAP8AEAAAAAAAAAAAAAAAQAgAAAAAAAAAAAkAAgAAAO4AAAATAP####8BAAD#ABAAAAAAAAAAAAAAAEAIAAAAAAAAAAAJAAEAAADuAAAAEgD#####AAAABAAAAOwAAAATAP####8BAAD#ABAAAAAAAAAAAAAAAEAIAAAAAAAAAAAJAAIAAADxAAAAEwD#####AQAA#wAQAAAAAAAAAAAAAABACAAAAAAAAAAACQABAAAA8QAAABYA#####wEAAP8AEAAAAQAAAQEAAADyAAAABAAAABYA#####wEAAP8AEAAAAQAAAQEAAADvAAAABAAAABYA#####wEAAP8AEAAAAQAAAQEAAACeAAAAAwAAABYA#####wEAAP8AEAAAAQAAAQEAAACLAAAAAwAAABYA#####wEAAP8AEAAAAQAAAQEAAACIAAAAAwAAABYA#####wEAAP8AEAAAAQAAAQEAAAB8AAAAAwAAABYA#####wEAAP8AEAAAAQAAAQEAAAB4AAAAAwAAABYA#####wEAAP8AEAAAAQAAAQEAAACcAAAAAwAAABUA#####wEAAP8AEAABUAAAAAAAAAAAAEAIAAAAAAAAAAAJAAAAAAMAAAD5AAAAFQD#####AQAA#wAQAAFPAAAAAAAAAAAAQAgAAAAAAAAAAAkAAAAAAwAAAPoAAAAVAP####8BAAD#ABAAAU4AAAAAAAAAAABACAAAAAAAAAAACQAAAAADAAAA+AAAABUA#####wEAAP8AEAABTQAAAAAAAAAAAEAIAAAAAAAAAAAJAAAAAAMAAAD7AAAAFQD#####AQAA#wAQAAFMAAAAAAAAAAAAQAgAAAAAAAAAAAkAAAAAAwAAAPcAAAAVAP####8BAAD#ABAAAUsAAAAAAAAAAABACAAAAAAAAAAACQAAAAADAAAA9gAAAAUA#####wASTWVzdXJlIGRlIGxvbmd1ZXVyAAAABQAAAAIAAAACAAAAAgAAAQEAAAAaAAAAAQIBAAAAABAAAAEAAAABAAAAAgAAAQEAAAAXAAAAAQIBAAAAABAAAAEAAAUAAAAAAgAAAQEAAAARAAAAAQIBAAAAAAAAAQAAAQQAAAABQDAAAAAAAAABAAAAEgAAAAECAAABAwAAAQUAAAATAAAAAQIBAAAAABAAAAEAAAUAAQAAAQYAAAAQAQAAAQIAAAACAAABAQAAAA0BAAABAgEAAP8BAAAAAAEHEQAAAAAAAQAAAAEAAAABAAAAAAAAAAAAAAAAAQAAAQgAAAAFAP####8AEk1lc3VyZSBkZSBsb25ndWV1cgAAAAUAAAACAAAAAgAAAAIAAAEAAAAAGgAAAAEKAQAAAAAQAAABAAAAAQAAAAIAAAEAAAAAFwAAAAEKAQAAAAAQAAABAAAFAAAAAAIAAAEAAAAAEQAAAAEKAQAAAAAAAAEAAAEMAAAAAUAwAAAAAAAAAQAAABIAAAABCgAAAQsAAAENAAAAEwAAAAEKAQAAAAAQAAABAAAFAAEAAAEOAAAAEAEAAAEKAAAAAgAAAQAAAAANAQAAAQoBAAD#AQAAAAABDxEAAAAAAAEAAAABAAAAAQAAAAAAAAAAAAAAAAEAAAEQAAAABQD#####ABJNZXN1cmUgZGUgbG9uZ3VldXIAAAAFAAAAAgAAAAIAAAACAAAA#wAAABoAAAABEgEAAAAAEAAAAQAAAAEAAAACAAAA#wAAABcAAAABEgEAAAAAEAAAAQAABQAAAAACAAAA#wAAABEAAAABEgEAAAAAAAABAAABFAAAAAFAMAAAAAAAAAEAAAASAAAAARIAAAETAAABFQAAABMAAAABEgEAAAAAEAAAAQAABQABAAABFgAAABABAAABEgAAAAIAAAD#AAAADQEAAAESAQAA#wEAAAAAARcRAAAAAAABAAAAAQAAAAEAAAAAAAAAAAAAAAABAAABGAAAAAUA#####wASTWVzdXJlIGRlIGxvbmd1ZXVyAAAABQAAAAIAAAACAAAAAgAAAP4AAAAaAAAAARoBAAAAABAAAAEAAAABAAAAAgAAAP4AAAAXAAAAARoBAAAAABAAAAEAAAUAAAAAAgAAAP4AAAARAAAAARoBAAAAAAAAAQAAARwAAAABQDAAAAAAAAABAAAAEgAAAAEaAAABGwAAAR0AAAATAAAAARoBAAAAABAAAAEAAAUAAQAAAR4AAAAQAQAAARoAAAACAAAA#gAAAA0BAAABGgEAAP8BAAAAAAEfEQAAAAAAAQAAAAEAAAABAAAAAAAAAAAAAAAAAQAAASAAAAAFAP####8AEk1lc3VyZSBkZSBsb25ndWV1cgAAAAUAAAACAAAAAgAAAAIAAAD9AAAAGgAAAAEiAQAAAAAQAAABAAAAAQAAAAIAAAD9AAAAFwAAAAEiAQAAAAAQAAABAAAFAAAAAAIAAAD9AAAAEQAAAAEiAQAAAAAAAAEAAAEkAAAAAUAwAAAAAAAAAQAAABIAAAABIgAAASMAAAElAAAAEwAAAAEiAQAAAAAQAAABAAAFAAEAAAEmAAAAEAEAAAEiAAAAAgAAAP0AAAANAQAAASIBAAD#AQAAAAABJxEAAAAAAAEAAAABAAAAAQAAAAAAAAAAAAAAAAEAAAEoAAAABQD#####ABJNZXN1cmUgZGUgbG9uZ3VldXIAAAAFAAAAAgAAAAIAAAACAAAA#AAAABoAAAABKgEAAAAAEAAAAQAAAAEAAAACAAAA#AAAABcAAAABKgEAAAAAEAAAAQAABQAAAAACAAAA#AAAABEAAAABKgEAAAAAAAABAAABLAAAAAFAMAAAAAAAAAEAAAASAAAAASoAAAErAAABLQAAABMAAAABKgEAAAAAEAAAAQAABQABAAABLgAAABABAAABKgAAAAIAAAD8AAAADQEAAAEqAQAA#wEAAAAAAS8RAAAAAAABAAAAAQAAAAEAAAAAAAAAAAAAAAABAAABMAAAAAQA#####wACUjIAKm1heChDSyxtYXgoQ0wsbWF4KENNLG1heChDTixtYXgoQ08sQ1ApKSkpKQAAABsAAAAACQAAAQgAAAAbAAAAAAkAAAEQAAAAGwAAAAAJAAABGAAAABsAAAAACQAAASAAAAAbAAAAAAkAAAEoAAAACQAAATAAAAAEAP####8AAnIyACptaW4oQ0ssbWluKENMLG1pbihDTSxtaW4oQ04sbWluKENPLENQKSkpKSkAAAAbAQAAAAkAAAEIAAAAGwEAAAAJAAABEAAAABsBAAAACQAAARgAAAAbAQAAAAkAAAEgAAAAGwEAAAAJAAABKAAAAAkAAAEwAAAAEQD#####AQAA#wAAAQEAAAACAAAACQAAATIAAAAAEQD#####AQAA#wAAAQEAAAACAAAACQAAATMAAAAAEgD#####AAAAAwAAATQAAAATAP####8BAAD#ABAAAAAAAAAAAAAAAEAIAAAAAAAAAAAJAAEAAAE2AAAAEwD#####AQAA#wAQAAAAAAAAAAAAAABACAAAAAAAAAAACQACAAABNgAAABIA#####wAAAAMAAAE1AAAAEwD#####AQAA#wAQAAAAAAAAAAAAAABACAAAAAAAAAAACQABAAABOQAAABMA#####wEAAP8AEAAAAAAAAAAAAAAAQAgAAAAAAAAAAAkAAgAAATkAAAAWAP####8BAAD#ABAAAAEAAAEBAAABOAAAAAMAAAAWAP####8BAAD#ABAAAAEAAAEBAAABOwAAAAMAAAAVAP####8BAAD#ABAAAAAAAAAAAAAAAEAIAAAAAAAAAAAJAAAAAPQAAAE9AAAAFQD#####AQAA#wAQAAAAAAAAAAAAAABACAAAAAAAAAAACQAAAAD1AAABPAAAABcA#####wH#AAAAEAAAAAAAAAAAAAAAQAgAAAAAAAAAA01JTAgAAAABPwAAAT7#####AAAAAQAMQ0Jpc3NlY3RyaWNlAP####8BAAB#ABAAAAEAAAEBAAAAeAAAADoAAACcAAAAEgD#####AAABQQAAAJAAAAATAP####8BAAB#ABAAAAAAAAAAAAAAAEAIAAAAAAAAAAAIAAIAAAFCAAAAEwD#####AQAAfwAQAAAAAAAAAAAAAABACAAAAAAAAAAACAABAAABQv####8AAAACAAxDQ29tbWVudGFpcmUA#####wAAAH8BAANucEEAAAFEEAAAAAAAAQAAAAEAAAABAAAAAAAAAAAAAUEAAAAdAP####8AAAB#AQADbnBCAAAAiBAAAAAAAAEAAAABAAAAAQAAAAAAAAAAAAFCAAAAHQD#####AAAAfwEAA25wQwAAAIsQAAAAAAABAAAAAQAAAAEAAAAAAAAAAAABQwAAAB0A#####wAAAH8BAANucEQAAABvEAAAAAAAAQAAAAEAAAABAAAAAAAAAAAAAUQAAAAdAP####8AAAB#AQADbnBFAAAAlRAAAAAAAAEAAAABAAAAAQAAAAAAAAAAAAFFAAAAHQD#####AH8AfwEAAmwxAAAAgRAAAAAAAAEAAAABAAAAAQAAAAAAAAAAAAExAAAAHQD#####AH8AfwEAAmwzAAAAjBAAAAAAAAEAAAABAAAAAQAAAAAAAAAAAAEyAAAAHQD#####AH8AfwEAAmwyAAAAoRAAAAAAAAEAAAABAAAAAQAAAAAAAAAAAAEzAAAAHQD#####AH8AfwEAAmw0AAAAchAAAAAAAAEAAAABAAAAAQAAAAAAAAAAAAE0AAAAHQD#####AH8AfwEAAmw1AAAAlhAAAAAAAAEAAAABAAAAAQAAAAAAAAAAAAE1AAAAHQD#####AH8AfwEAAmw2AAAAaxAAAAAAAAEAAAABAAAAAQAAAAAAAAAAAAE2AAAAQP##########'
      const A = stor.lettres[0]
      const B = stor.lettres[1]
      const C = stor.lettres[2]
      const D = stor.lettres[3]
      const E = stor.lettres[4]
      svgId = j3pGetNewId()
      stor.svgId = svgId
      const tabL1 = addDefaultTable(stor.lesdiv.figure, 1, 1)[0][0]
      j3pCreeSVG(tabL1, { id: svgId, width: 400, height: 350, style: { border: 'solid black 1px' } })
      stor.mtgAppLecteur.removeAllDoc()
      stor.mtgAppLecteur.addDoc(svgId, txtFigure, true)
      stor.mtgAppLecteur.giveFormula2(svgId, 'A1', j3pGetRandomInt(0, 359))
      stor.mtgAppLecteur.giveFormula2(svgId, 'A2', j3pGetRandomInt(0, 10))
      stor.mtgAppLecteur.giveFormula2(svgId, 'A3', j3pGetRandomInt(0, 10))
      stor.mtgAppLecteur.giveFormula2(svgId, 'A4', j3pGetRandomInt(0, 10))
      stor.mtgAppLecteur.setText(svgId, '#npA', B)
      stor.mtgAppLecteur.setText(svgId, '#npB', A)
      stor.mtgAppLecteur.setText(svgId, '#npC', C)
      stor.mtgAppLecteur.setText(svgId, '#npD', D)
      stor.mtgAppLecteur.setText(svgId, '#npE', E)
      stor.mtgAppLecteur.calculateAndDisplayAll(true)

      if (ds.Couleur) {
        stor.mtgAppLecteur.setColor(svgId, '#s4', 255, 0, 0, true)
        stor.mtgAppLecteur.setColor(svgId, '#s5', 255, 0, 0, true)
        stor.mtgAppLecteur.setColor(svgId, '#s6', 255, 0, 0, true)
      } else {
        stor.mtgAppLecteur.setLineStyle(svgId, '#s1', 0, 3, true)
        stor.mtgAppLecteur.setLineStyle(svgId, '#s2', 0, 3, true)
        stor.mtgAppLecteur.setLineStyle(svgId, '#s3', 0, 3, true)
      }

      if (stor.visible[0]) {
        stor.mtgAppLecteur.setText(svgId, '#l4', (stor.l1 + ' cm').replace('.', ','))
      } else {
        stor.nominc.push(lettres[1] + lettres[3])
        stor.mtgAppLecteur.setVisible(svgId, '#l4', false, true)
      }
      if (stor.visible[1]) {
        stor.mtgAppLecteur.setText(svgId, '#l5', (stor.l2 + ' cm').replace('.', ','))
      } else {
        stor.nominc.push(lettres[1] + lettres[4])
        stor.mtgAppLecteur.setVisible(svgId, '#l5', false, true)
      }
      if (stor.visible[2]) {
        stor.mtgAppLecteur.setText(svgId, '#l6', (stor.l3 + ' cm').replace('.', ','))
      } else {
        stor.nominc.push(lettres[3] + lettres[4])
        stor.mtgAppLecteur.setVisible(svgId, '#l6', false, true)
      }
      if (stor.visible[5]) {
        stor.mtgAppLecteur.setText(svgId, '#l3', (stor.l6 + ' cm').replace('.', ','))
      } else {
        stor.nominc.push(lettres[0] + lettres[2])
        stor.mtgAppLecteur.setVisible(svgId, '#l3', false, true)
      }
      if (stor.visible[3]) {
        stor.mtgAppLecteur.setText(svgId, '#l1', (stor.l4 + ' cm').replace('.', ','))
      } else {
        stor.mtgAppLecteur.setVisible(svgId, '#l1', false, true)
        stor.mtgAppLecteur.setVisible(svgId, '#v11', false, true)
        stor.mtgAppLecteur.setVisible(svgId, '#v21', false, true)
        stor.mtgAppLecteur.setVisible(svgId, '#s11', false, true)
        stor.mtgAppLecteur.setVisible(svgId, '#s21', false, true)
        stor.nominc.push(lettres[1] + lettres[0])
      }
      if (stor.visible[4]) {
        stor.mtgAppLecteur.setText(svgId, '#l2', (stor.l5 + ' cm').replace('.', ','))
      } else {
        stor.mtgAppLecteur.setVisible(svgId, '#l2', false, true)
        stor.mtgAppLecteur.setVisible(svgId, '#v12', false, true)
        stor.mtgAppLecteur.setVisible(svgId, '#v22', false, true)
        stor.mtgAppLecteur.setVisible(svgId, '#s12', false, true)
        stor.mtgAppLecteur.setVisible(svgId, '#s22', false, true)
        stor.nominc.push(lettres[1] + lettres[2])
      }

      const p1 = stor.mtgAppLecteur.getPointPosition(svgId, '#MIL')
      const p2 = stor.mtgAppLecteur.getPointPosition(svgId, '#P1')
      const p3 = stor.mtgAppLecteur.getPointPosition(svgId, '#P2')
      const depx = p1.x - 200
      const depy = p1.y - 175
      stor.mtgAppLecteur.setPointPosition(svgId, '#P1', p2.x - depx, p2.y - depy, true)
      stor.mtgAppLecteur.setPointPosition(svgId, '#P2', p3.x - depx, p3.y - depy, true)
    } else {
      j3pAffiche(stor.lesdiv.figure, null, stor.nomtri + ' est un triangle,\n')
    }

    j3pAffiche(stor.lesdiv.figure, null, '$ ' + lettres[3] + ' \\in [' + lettres[1] + lettres[0] + ']$ ;   $' + lettres[4] + ' \\in [' + lettres[1] + lettres[2] + ']$ \n')
    j3pAffiche(stor.lesdiv.figure, null, '$ (' + lettres[3] + lettres[4] + ') // (' + lettres[0] + lettres[2] + ') $ \n')
    if (stor.Lexo.enonc === 'Tex') {
      if (stor.visible[0]) { j3pAffiche(stor.lesdiv.figure, null, '$ ' + lettres[1] + lettres[3] + ' = ' + stor.l1 + '$ cm \n') } else { stor.nominc.push(lettres[1] + lettres[3]) }
      if (stor.visible[1]) { j3pAffiche(stor.lesdiv.figure, null, '$ ' + lettres[1] + lettres[4] + ' = ' + stor.l2 + '$ cm \n') } else { stor.nominc.push(lettres[1] + lettres[4]) }
      if (stor.visible[2]) { j3pAffiche(stor.lesdiv.figure, null, '$ ' + lettres[3] + lettres[4] + ' = ' + stor.l3 + '$ cm \n') } else { stor.nominc.push(lettres[3] + lettres[4]) }
      if (stor.visible[5]) { j3pAffiche(stor.lesdiv.figure, null, '$ ' + lettres[0] + lettres[2] + ' = ' + stor.l6 + '$ cm \n') } else { stor.nominc.push(lettres[0] + lettres[2]) }
      if (stor.visible[3]) { j3pAffiche(stor.lesdiv.figure, null, '$ ' + lettres[1] + lettres[0] + ' = ' + stor.l4 + '$ cm \n') } else { stor.nominc.push(lettres[1] + lettres[0]) }
      if (stor.visible[4]) { j3pAffiche(stor.lesdiv.figure, null, '$ ' + lettres[1] + lettres[2] + ' = ' + stor.l5 + '$ cm \n') } else { stor.nominc.push(lettres[1] + lettres[2]) }
    }
    i = j3pGetRandomInt(0, 5)
    while (stor.visible[i]) {
      i++
      if (i === 6) i = 0
    }
    stor.li = i
    switch (i) {
      case 3: stor.lch = stor.lettres[1] + stor.lettres[0]
        stor.lbas = stor.l1
        break
      case 4: stor.lch = stor.lettres[1] + stor.lettres[2]
        stor.lbas = stor.l2
        break
      case 5: stor.lch = stor.lettres[0] + stor.lettres[2]
        stor.lbas = stor.l3
        break
      case 0: stor.lch = stor.lettres[1] + stor.lettres[3]
        stor.lbas = stor.l4
        break
      case 1: stor.lch = stor.lettres[1] + stor.lettres[4]
        stor.lbas = stor.l5
        break
      case 2: stor.lch = stor.lettres[3] + stor.lettres[4]
        stor.lbas = stor.l6
        break
    }
    if (stor.visible[0] && stor.visible[3]) stor.lhaut = [stor.l1, stor.l4]
    if (stor.visible[1] && stor.visible[4]) stor.lhaut = [stor.l2, stor.l5]
    if (stor.visible[2] && stor.visible[5]) stor.lhaut = [stor.l3, stor.l6]
  }

  function initSection () {
    let i
    initCptPe(ds, stor)
    me.validOnEnter = false // ex donneesSection.touche_entree

    // Construction de la page
    me.construitStructurePage('presentation1bis')

    ds.lesExos = []

    let lp = [1, 2, 3]
    lp = j3pShuffle(lp)

    for (i = 0; i < ds.nbitems; i++) {
      ds.lesExos.push({ prop: lp[i % (lp.length)] })
    }
    ds.lesExos = j3pShuffle(ds.lesExos)

    lp = [1, 2, 3, 4]
    lp = j3pShuffle(lp)
    for (i = 0; i < ds.lesExos.length; i++) {
      ds.lesExos[i].sens = lp[i % 4]
    }
    ds.lesExos = j3pShuffle(ds.lesExos)

    lp = []
    if ((ds.Enonce === 'Figure') || (ds.Enonce === 'Les deux')) lp.push('Fig')
    if ((ds.Enonce === 'Texte') || (ds.Enonce === 'Les deux')) lp.push('Tex')

    lp = j3pShuffle(lp)
    for (i = 0; i < ds.lesExos.length; i++) {
      ds.lesExos[i].enonc = lp[i % lp.length]
    }
    ds.lesExos = j3pShuffle(ds.lesExos)

    me.typederreurs = [0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0]

    let buft = 'Calculer une longueur avec la propriété de Thalès'
    if (!ds.Egalite && !ds.Reponse) { ds.Egalite = true }
    if (ds.Egalite && !ds.Reponse) buft = 'Ecrire l’égalité de Thalès'

    me.afficheTitre(buft)

    // chargement mtg
    getMtgCore({ withMathJax: true })
      .then(
        // success
        (mtgAppLecteur) => {
          stor.mtgAppLecteur = mtgAppLecteur
          stor.listePourCalc = mtgAppLecteur.createList('TWF0aEdyYXBoSmF2YTEuMAAAABI+TMzNAAJmcv###wEA#wEAAAAAAAAAAAQsAAACvQAAAQEAAAAAAAAAAQAAAAP#####AAAAAQAKQ0NhbGNDb25zdAD#####AAJwaQAWMy4xNDE1OTI2NTM1ODk3OTMyMzg0Nv####8AAAABAApDQ29uc3RhbnRlQAkh+1RELRj#####AAAAAQAHQ0NhbGN1bAD#####AAFlAAZleHAoMSn#####AAAAAgAJQ0ZvbmN0aW9uBwAAAAE#8AAAAAAAAAAAAAIA#####wABeAABMAAAAAEAAAAAAAAAAP###############w==')
          enonceMain()
        },
        // failure
        (error) => {
          j3pShowError(error, { message: 'mathgraph n’est pas correctement chargé', mustNotify: true })
        })
      // plantage dans le code de success
      .catch(j3pShowError)
  }

  function yaReponse () {
    if (stor.encours === 'Descrip') {
      if (!stor.listeAp1.changed) {
        stor.listeAp1.focus()
        return false
      }
      if (!stor.listeAp2.changed) {
        stor.listeAp2.focus()
        return false
      }
      if (!stor.listePara.changed) {
        stor.listePara.focus()
        return false
      }
      if (!stor.listeProp.changed) {
        stor.listeProp.focus()
        return false
      }
      return true
    }
    if (stor.encours === 'egalite') {
      if (!stor.listeE1.changed) {
        stor.listeE1.focus()
        return false
      }
      if (!stor.listeE2.changed) {
        stor.listeE2.focus()
        return false
      }
      if (!stor.listeE3.changed) {
        stor.listeE3.focus()
        return false
      }
      if (!stor.listeE4.changed) {
        stor.listeE4.focus()
        return false
      }
      if (!stor.listeE5.changed) {
        stor.listeE5.focus()
        return false
      }
      if (!stor.listeE6.changed) {
        stor.listeE6.focus()
        return false
      }
      return true
    }
    if (stor.encours === 'rempli') {
      if (!(stor.z1.ffalse === true)) {
        if (stor.z1.reponse() === '') {
          stor.z1.focus()
          return false
        }
      }
      if (!(stor.z2.ffalse === true)) {
        if (stor.z2.reponse() === '') {
          stor.z2.focus()
          return false
        }
      }
      if (!(stor.z3.ffalse === true)) {
        if (stor.z3.reponse() === '') {
          stor.z3.focus()
          return false
        }
      }
      if (!(stor.z4.ffalse === true)) {
        if (stor.z4.reponse() === '') {
          stor.z4.focus()
          return false
        }
      }
      if (!(stor.z5.ffalse === true)) {
        if (stor.z5.reponse() === '') {
          stor.z5.focus()
          return false
        }
      }
      if (!(stor.z6.ffalse === true)) {
        if (stor.z6.reponse() === '') {
          stor.z6.focus()
          return false
        }
      }
      return true
    }
    if (stor.encours === 'prod') {
      if (stor.zP1.reponse() === '') {
        stor.zP1.focus()
        return false
      }
      if (stor.zP2.reponse() === '') {
        stor.zP2.focus()
        return false
      }
      if (stor.zP3.reponse() === '') {
        stor.zP3.focus()
        return false
      }
      return true
    }
    if (stor.encours === 'concl') {
      if (stor.zFin.reponse() === '') {
        stor.zFin.focus()
        return false
      }
      return true
    }
  }

  function isRepOk () {
    stor.errAppartient = false
    stor.errPara = false
    stor.errProp = false
    stor.errDouble = false
    stor.errMauvaistri = false
    stor.errPasCoress = false
    stor.errPascote = false
    stor.errCopie = false
    stor.errProduit = false
    stor.errDouble2 = false
    stor.errSimpli = false
    stor.errMalEcrit = false
    stor.errVirgFrac = false
    stor.errAprox = false
    stor.errAprox2 = false

    let ok = true
    let t1, t2, t3, t4, t5, t6
    let n1, n2, n3, n4, n5, n6

    if (stor.encours === 'Descrip') {
      if (stor.listeAp1.reponse !== '[' + stor.lettres[1] + stor.lettres[0] + ']') {
        stor.listeAp1.corrige(false)
        stor.errAppartient = true
        ok = false
      }
      if (stor.listeAp2.reponse !== '[' + stor.lettres[1] + stor.lettres[2] + ']') {
        stor.listeAp2.corrige(false)
        stor.errAppartient = true
        ok = false
      }
      if (stor.listePara.reponse.indexOf(stor.lettres[0] + stor.lettres[2]) === -1) {
        stor.listePara.corrige(false)
        stor.errPara = true
        ok = false
      }
      if (stor.listeProp.reponse !== 'J’utilise la propriété de Thalès,') {
        stor.listeProp.corrige(false)
        stor.errProp = true
        ok = false
      }
      return ok
    }
    if (stor.encours === 'egalite') {
      t1 = stor.listeE1.reponse
      t2 = stor.listeE2.reponse
      t3 = stor.listeE3.reponse
      t4 = stor.listeE4.reponse
      t5 = stor.listeE5.reponse
      t6 = stor.listeE6.reponse
      // verif pas cote
      if ((t1 === stor.lettres[0] + stor.lettres[3]) || (t1 === stor.lettres[2] + stor.lettres[4])) {
        stor.listeE1.corrige(false)
        stor.errPascote = true
        ok = false
      }
      if ((t2 === stor.lettres[0] + stor.lettres[3]) || (t2 === stor.lettres[2] + stor.lettres[4])) {
        stor.listeE2.corrige(false)
        stor.errPascote = true
        ok = false
      }
      if ((t3 === stor.lettres[0] + stor.lettres[3]) || (t3 === stor.lettres[2] + stor.lettres[4])) {
        stor.listeE3.corrige(false)
        stor.errPascote = true
        ok = false
      }
      if ((t4 === stor.lettres[0] + stor.lettres[3]) || (t4 === stor.lettres[2] + stor.lettres[4])) {
        stor.listeE4.corrige(false)
        stor.errPascote = true
        ok = false
      }
      if ((t5 === stor.lettres[0] + stor.lettres[3]) || (t5 === stor.lettres[2] + stor.lettres[4])) {
        stor.listeE5.corrige(false)
        stor.errPascote = true
        ok = false
      }
      if ((t6 === stor.lettres[0] + stor.lettres[3]) || (t6 === stor.lettres[2] + stor.lettres[4])) {
        stor.listeE6.corrige(false)
        stor.errPascote = true
        ok = false
      }
      if (!ok) return false
      // verif pas double
      if (t1 === t2) {
        stor.listeE1.corrige(false)
        stor.listeE2.corrige(false)
        stor.errDouble = true
        ok = false
      }
      if (t1 === t3) {
        stor.listeE1.corrige(false)
        stor.listeE3.corrige(false)
        stor.errDouble = true
        ok = false
      }
      if (t1 === t4) {
        stor.listeE1.corrige(false)
        stor.listeE4.corrige(false)
        stor.errDouble = true
        ok = false
      }
      if (t1 === t5) {
        stor.listeE1.corrige(false)
        stor.listeE5.corrige(false)
        stor.errDouble = true
        ok = false
      }
      if (t1 === t6) {
        stor.listeE1.corrige(false)
        stor.listeE6.corrige(false)
        stor.errDouble = true
        ok = false
      }
      if (t2 === t3) {
        stor.listeE2.corrige(false)
        stor.listeE3.corrige(false)
        stor.errDouble = true
        ok = false
      }
      if (t2 === t4) {
        stor.listeE2.corrige(false)
        stor.listeE4.corrige(false)
        stor.errDouble = true
        ok = false
      }
      if (t2 === t5) {
        stor.listeE2.corrige(false)
        stor.listeE5.corrige(false)
        stor.errDouble = true
        ok = false
      }
      if (t2 === t6) {
        stor.listeE2.corrige(false)
        stor.listeE6.corrige(false)
        stor.errDouble = true
        ok = false
      }
      if (t3 === t4) {
        stor.listeE3.corrige(false)
        stor.listeE4.corrige(false)
        stor.errDouble = true
        ok = false
      }
      if (t3 === t5) {
        stor.listeE3.corrige(false)
        stor.listeE5.corrige(false)
        stor.errDouble = true
        ok = false
      }
      if (t3 === t6) {
        stor.listeE3.corrige(false)
        stor.listeE6.corrige(false)
        stor.errDouble = true
        ok = false
      }
      if (t4 === t5) {
        stor.listeE4.corrige(false)
        stor.listeE5.corrige(false)
        stor.errDouble = true
        ok = false
      }
      if (t4 === t6) {
        stor.listeE4.corrige(false)
        stor.listeE6.corrige(false)
        stor.errDouble = true
        ok = false
      }
      if (t6 === t5) {
        stor.listeE6.corrige(false)
        stor.listeE5.corrige(false)
        stor.errDouble = true
        ok = false
      }
      if (!ok) return false
      // verif bon tri
      n1 = (t1.indexOf(stor.lettres[3]) === -1) && (t1.indexOf(stor.lettres[4]) === -1)
      n2 = (t2.indexOf(stor.lettres[3]) === -1) && (t2.indexOf(stor.lettres[4]) === -1)
      n3 = (t3.indexOf(stor.lettres[3]) === -1) && (t3.indexOf(stor.lettres[4]) === -1)
      n4 = (t4.indexOf(stor.lettres[3]) === -1) && (t4.indexOf(stor.lettres[4]) === -1)
      n5 = (t5.indexOf(stor.lettres[3]) === -1) && (t5.indexOf(stor.lettres[4]) === -1)
      n6 = (t6.indexOf(stor.lettres[3]) === -1) && (t6.indexOf(stor.lettres[4]) === -1)
      if (n1 !== n3 || n3 !== n5) {
        if (n1 === n3) {
          stor.listeE5.corrige(false)
          stor.errMauvaistri = true
          ok = false
        } else if (n1 === n5) {
          stor.listeE3.corrige(false)
          stor.errMauvaistri = true
          ok = false
        } else {
          stor.listeE1.corrige(false)
          stor.errMauvaistri = true
          ok = false
        }
      }
      if (n2 !== n4 || n4 !== n6) {
        if (n2 === n4) {
          stor.listeE6.corrige(false)
          stor.errMauvaistri = true
          ok = false
        } else if (n2 === n6) {
          stor.listeE4.corrige(false)
          stor.errMauvaistri = true
          ok = false
        } else {
          stor.listeE2.corrige(false)
          stor.errMauvaistri = true
          ok = false
        }
      }
      if (!ok) return false
      // verif bien par deux
      if ((t1 === stor.lettres[3] + stor.lettres[4]) || (t1 === stor.lettres[0] + stor.lettres[2])) n1 = 3
      if ((t2 === stor.lettres[3] + stor.lettres[4]) || (t2 === stor.lettres[0] + stor.lettres[2])) n2 = 3
      if ((t3 === stor.lettres[3] + stor.lettres[4]) || (t3 === stor.lettres[0] + stor.lettres[2])) n3 = 3
      if ((t4 === stor.lettres[3] + stor.lettres[4]) || (t4 === stor.lettres[0] + stor.lettres[2])) n4 = 3
      if ((t5 === stor.lettres[3] + stor.lettres[4]) || (t5 === stor.lettres[0] + stor.lettres[2])) n5 = 3
      if ((t6 === stor.lettres[3] + stor.lettres[4]) || (t6 === stor.lettres[0] + stor.lettres[2])) n6 = 3
      if ((t1 === stor.lettres[1] + stor.lettres[0]) || (t1 === stor.lettres[1] + stor.lettres[3])) n1 = 1
      if ((t2 === stor.lettres[1] + stor.lettres[0]) || (t2 === stor.lettres[1] + stor.lettres[3])) n2 = 1
      if ((t3 === stor.lettres[1] + stor.lettres[0]) || (t3 === stor.lettres[1] + stor.lettres[3])) n3 = 1
      if ((t4 === stor.lettres[1] + stor.lettres[0]) || (t4 === stor.lettres[1] + stor.lettres[3])) n4 = 1
      if ((t5 === stor.lettres[1] + stor.lettres[0]) || (t5 === stor.lettres[1] + stor.lettres[3])) n5 = 1
      if ((t6 === stor.lettres[1] + stor.lettres[0]) || (t6 === stor.lettres[1] + stor.lettres[3])) n6 = 1
      if ((t1 === stor.lettres[1] + stor.lettres[2]) || (t1 === stor.lettres[1] + stor.lettres[4])) n1 = 2
      if ((t2 === stor.lettres[1] + stor.lettres[2]) || (t2 === stor.lettres[1] + stor.lettres[4])) n2 = 2
      if ((t3 === stor.lettres[1] + stor.lettres[2]) || (t3 === stor.lettres[1] + stor.lettres[4])) n3 = 2
      if ((t4 === stor.lettres[1] + stor.lettres[2]) || (t4 === stor.lettres[1] + stor.lettres[4])) n4 = 2
      if ((t5 === stor.lettres[1] + stor.lettres[2]) || (t5 === stor.lettres[1] + stor.lettres[4])) n5 = 2
      if ((t6 === stor.lettres[1] + stor.lettres[2]) || (t6 === stor.lettres[1] + stor.lettres[4])) n6 = 2
      if (n1 !== n2) {
        stor.listeE1.corrige(false)
        stor.listeE4.corrige(false)
        stor.errPasCoress = true
        ok = false
      }
      if (n3 !== n4) {
        stor.listeE2.corrige(false)
        stor.listeE5.corrige(false)
        stor.errPasCoress = true
        ok = false
      }
      if (n5 !== n6) {
        stor.listeE6.corrige(false)
        stor.listeE3.corrige(false)
        stor.errPasCoress = true
        ok = false
      }
      return ok
    }
    if (stor.encours === 'rempli') {
      if (!(stor.z6.ffalse === true)) {
        t1 = longueurat(stor.listeE6.reponse)
        t1 = (t1 + '').replace('.', ',')
        if (t1 !== stor.z6.reponse()) {
          stor.errCopie = true
          stor.z6.corrige(false)
          ok = false
        }
      }
      if (!(stor.z4.ffalse === true)) {
        t1 = longueurat(stor.listeE4.reponse)
        t1 = (t1 + '').replace('.', ',')
        if (t1 !== stor.z4.reponse()) {
          stor.errCopie = true
          stor.z4.corrige(false)
          ok = false
        }
      }
      if (!(stor.z3.ffalse === true)) {
        t1 = longueurat(stor.listeE3.reponse)
        t1 = (t1 + '').replace('.', ',')
        if (t1 !== stor.z3.reponse()) {
          stor.errCopie = true
          stor.z3.corrige(false)
          ok = false
        }
      }
      if (!(stor.z2.ffalse === true)) {
        t1 = longueurat(stor.listeE2.reponse)
        t1 = (t1 + '').replace('.', ',')
        if (t1 !== stor.z2.reponse()) {
          stor.errCopie = true
          stor.z2.corrige(false)
          ok = false
        }
      }
      if (!(stor.z5.ffalse === true)) {
        t1 = longueurat(stor.listeE5.reponse)
        t1 = (t1 + '').replace('.', ',')
        if (t1 !== stor.z5.reponse()) {
          stor.errCopie = true
          stor.z5.corrige(false)
          ok = false
        }
      }
      if (!(stor.z1.ffalse === true)) {
        t1 = longueurat(stor.listeE1.reponse)
        t1 = (t1 + '').replace('.', ',')
        if (t1 !== stor.z1.reponse()) {
          stor.errCopie = true
          stor.z1.corrige(false)
          ok = false
        }
      }
      return ok
    }
    if (stor.encours === 'prod') {
      // cherche les deux longueur en haut
      stor.atthaut = [(stor.lbas + '').replace('.', ',')]
      // si stor.lch est en ptittrianglr, on pend stor.lhaut[1]
      if ((stor.lch.indexOf(stor.lettres[0]) !== -1) || (stor.lch.indexOf(stor.lettres[2]) !== -1)) {
        stor.atthaut.push((stor.lhaut[1] + '').replace('.', ','))
        stor.attbas = (stor.lhaut[0] + '').replace('.', ',')
      } else {
        stor.atthaut.push((stor.lhaut[0] + '').replace('.', ','))
        stor.attbas = (stor.lhaut[1] + '').replace('.', ',')
      }
      if (stor.zP2.reponse() === stor.zP1.reponse()) {
        stor.zP2.corrige(false)
        stor.zP1.corrige(false)
        stor.errDouble2 = true
        ok = false
      }
      if (stor.zP2.reponse() === stor.zP3.reponse()) {
        stor.zP2.corrige(false)
        stor.zP3.corrige(false)
        stor.errDouble2 = true
        ok = false
      }
      if (stor.zP1.reponse() === stor.zP3.reponse()) {
        stor.zP1.corrige(false)
        stor.zP3.corrige(false)
        stor.errDouble2 = true
        ok = false
      }
      if (stor.zP3.reponse() !== stor.attbas) {
        stor.zP3.corrige(false)
        stor.errProduit = true
        ok = false
      }
      if ((stor.zP1.reponse() !== stor.atthaut[0]) && (stor.zP1.reponse() !== stor.atthaut[1])) {
        stor.zP1.corrige(false)
        stor.errProduit = true
        ok = false
      }
      if ((stor.zP2.reponse() !== stor.atthaut[0]) && (stor.zP2.reponse() !== stor.atthaut[1])) {
        stor.zP2.corrige(false)
        stor.errProduit = true
        ok = false
      }
      return ok
    }
    if (stor.encours === 'concl') {
      t1 = stor.zFin.reponse()
      t2 = stor.zFin.reponse2()
      t3 = stor.zFin.reponsenb()
      t4 = t2[0].replace(/ /g, '')
      t5 = (t2[1] + '').replace(/ /g, '')
      if ((t4.indexOf('.') === 0) || (t4.indexOf('.') === t4.length - 1) || (t4.indexOf('.') !== t4.lastIndexOf('.')) || (t4 === '')) {
        stor.zFin.corrige(false)
        stor.errMalEcrit = true
        ok = false
      }
      if ((t5.indexOf('.') === 0) || (t4.indexOf('.') === t5.length - 1) || (t5.indexOf('.') !== t5.lastIndexOf('.')) || (t5 === '')) {
        stor.zFin.corrige(false)
        stor.errMalEcrit = true
        ok = false
      }
      if (!ok) return false
      n1 = { num: Math.round(parseFloat(stor.atthaut[0].replace(',', '.')) * 100 * parseFloat(stor.atthaut[1].replace(',', '.'))), den: Math.round(parseFloat(stor.attbas.replace(',', '.')) * 100) }
      n2 = { num: t3[0], den: t3[1] }
      while (n2.num !== Math.round(n2.num)) {
        n2.num = j3pArrondi(n2.num * 10, 10)
        n2.den = j3pArrondi(n2.den * 10, 10)
      }
      if (stor.Yaapprox) {
        if (t4.indexOf('.') !== -1) {
          if (t4.indexOf('.') !== t4.length - 2) {
            ok = false
            stor.zFin.corrige(false)
            stor.errAprox = true
          }
        }
        if (Math.abs(n1.num / n1.den - n2.num / n2.den) > 0.1) {
          ok = false
          stor.zFin.corrige(false)
          stor.errAprox2 = true
        }
      } else if (!egalitefrac(n1, n2)) {
        // ok = false
        stor.zFin.corrige(false)
        return false
      }
      if (t1.indexOf('frac') !== -1) {
        if ((t4.indexOf('.') !== -1) || (t5.indexOf('.') !== -1)) {
          stor.errVirgFrac = true
          ok = false
        } else {
          if ((j3pPGCD(t3[0], t3[1], { negativesAllowed: true, valueIfZero: 1 }) !== 1) && (ds.Simplifie)) {
            stor.errSimpli = true
            ok = false
            stor.zFin.corrige(false)
          }
        }
      }
    }

    return ok
  }
  function egalitefrac (x, y) {
    return (x.num * y.den === x.den * y.num)
  }

  function longueurat (st) {
    switch (st) {
      case stor.lettres[1] + stor.lettres[3]: return stor.l1
      case stor.lettres[1] + stor.lettres[4]: return stor.l2
      case stor.lettres[3] + stor.lettres[4]: return stor.l3
      case stor.lettres[1] + stor.lettres[0]: return stor.l4
      case stor.lettres[1] + stor.lettres[2]: return stor.l5
      case stor.lettres[0] + stor.lettres[2]: return stor.l6
    }
  }

  function desactiveAll () {
    if (stor.encours === 'Descrip') {
      stor.listeAp1.disable()
      stor.listeAp2.disable()
      stor.listePara.disable()
      stor.listeProp.disable()
    }
    if (stor.encours === 'egalite') {
      stor.listeE1.disable()
      stor.listeE2.disable()
      stor.listeE3.disable()
      stor.listeE4.disable()
      stor.listeE5.disable()
      stor.listeE6.disable()
    }
    if (stor.encours === 'rempli') {
      if (!(stor.z6.ffalse === true)) {
        stor.z6.disable()
      }
      if (!(stor.z5.ffalse === true)) {
        stor.z5.disable()
      }
      if (!(stor.z4.ffalse === true)) {
        stor.z4.disable()
      }
      if (!(stor.z3.ffalse === true)) {
        stor.z3.disable()
      }
      if (!(stor.z2.ffalse === true)) {
        stor.z2.disable()
      }
      if (!(stor.z1.ffalse === true)) {
        stor.z1.disable()
      }
    }
    if (stor.encours === 'prod') {
      stor.zP1.disable()
      stor.zP2.disable()
      stor.zP3.disable()
    }
    if (stor.encours === 'concl') {
      stor.zFin.disable()
      stor.brouillon1.disable()
    }
  }

  function passeToutVert () {
    if (stor.encours === 'Descrip') {
      stor.listeAp1.corrige(true)
      stor.listeAp2.corrige(true)
      stor.listePara.corrige(true)
      stor.listeProp.corrige(true)
    }
    if (stor.encours === 'egalite') {
      stor.listeE1.corrige(true)
      stor.listeE2.corrige(true)
      stor.listeE3.corrige(true)
      stor.listeE4.corrige(true)
      stor.listeE5.corrige(true)
      stor.listeE6.corrige(true)
    }
    if (stor.encours === 'rempli') {
      if (!(stor.z6.ffalse === true)) {
        stor.z6.corrige(true)
      }
      if (!(stor.z5.ffalse === true)) {
        stor.z5.corrige(true)
      }
      if (!(stor.z4.ffalse === true)) {
        stor.z4.corrige(true)
      }
      if (!(stor.z3.ffalse === true)) {
        stor.z3.corrige(true)
      }
      if (!(stor.z2.ffalse === true)) {
        stor.z2.corrige(true)
      }
      if (!(stor.z1.ffalse === true)) {
        stor.z1.corrige(true)
      }
    }
    if (stor.encours === 'prod') {
      stor.zP1.corrige(true)
      stor.zP2.corrige(true)
      stor.zP3.corrige(true)
    }
    if (stor.encours === 'concl') {
      stor.zFin.corrige(true)
    }
  }
  function barrelesfo () {
    if (stor.encours === 'Descrip') {
      stor.listeAp1.barreIfKo()
      stor.listeAp2.barreIfKo()
      stor.listePara.barreIfKo()
      stor.listeProp.barreIfKo()
    }
    if (stor.encours === 'egalite') {
      stor.listeE1.barreIfKo()
      stor.listeE2.barreIfKo()
      stor.listeE3.barreIfKo()
      stor.listeE4.barreIfKo()
      stor.listeE5.barreIfKo()
      stor.listeE6.barreIfKo()
    }
    if (stor.encours === 'rempli') {
      if (!(stor.z6.ffalse === true)) {
        stor.z6.barreIfKo()
      }
      if (!(stor.z5.ffalse === true)) {
        stor.z5.barreIfKo()
      }
      if (!(stor.z4.ffalse === true)) {
        stor.z4.barreIfKo()
      }
      if (!(stor.z3.ffalse === true)) {
        stor.z3.barreIfKo()
      }
      if (!(stor.z2.ffalse === true)) {
        stor.z2.barreIfKo()
      }
      if (!(stor.z1.ffalse === true)) {
        stor.z1.barreIfKo()
      }
    }
    if (stor.encours === 'prod') {
      stor.zP1.barreIfKo()
      stor.zP2.barreIfKo()
      stor.zP3.barreIfKo()
    }
    if (stor.encours === 'concl') {
      stor.zFin.barre()
    }
  }

  function affCorrFaux (isFin) {
    /// //affiche indic
    let yaexplik = false
    if (stor.errAppartient) {
      j3pAffiche(stor.lesdiv.explications, null, 'Vérifie sur quel côté se trouvent les points  !\n')
      yaexplik = true
    }
    if (stor.errPara) {
      j3pAffiche(stor.lesdiv.explications, null, 'Des droites parallèles ne se coupent pas !\n')
      yaexplik = true
    }
    if (stor.errProp) {
      j3pAffiche(stor.lesdiv.explications, null, 'Il faut utiliser la bonne propriété !\n')
      yaexplik = true
    }
    if (stor.errDouble) {
      j3pAffiche(stor.lesdiv.explications, null, 'Tu écris plusieurs fois la même longueur !\n')
      yaexplik = true
    }
    if (stor.errMauvaistri) {
      j3pAffiche(stor.lesdiv.explications, null, 'Les trois longueurs d’un même triangle doivent être \nsoit toutes en numérateur, soit toutes en dénominateur !\n')
      yaexplik = true
      stor.compteurPe1++
    }
    if (stor.errPasCoress) {
      j3pAffiche(stor.lesdiv.explications, null, 'Des longueurs ne se correspondent pas !\n')
      yaexplik = true
      stor.compteurPe1++
    }
    if (stor.errPascote) {
      j3pAffiche(stor.lesdiv.explications, null, 'On ne peut utiliser que les longueurs des côtés des deux triangles !\n')
      yaexplik = true
      stor.compteurPe1++
    }
    if (stor.errCopie) {
      j3pAffiche(stor.lesdiv.explications, null, 'Il faut prendre les longueurs de l’énoncé !\n')
      yaexplik = true
    }
    if (stor.errDouble2) {
      j3pAffiche(stor.lesdiv.explications, null, 'Tu écris plusieurs fois la même longueur !\n')
      yaexplik = true
    }
    if (stor.errProduit) {
      j3pAffiche(stor.lesdiv.explications, null, 'On multiplie en diagonale, puis on divise par le troisième !\n')
      yaexplik = true
      stor.compteurPe3++
    }
    if (stor.errSimpli) {
      j3pAffiche(stor.lesdiv.explications, null, 'La réponse doit être simplifiée !\n')
      yaexplik = true
      stor.compteurPe4++
    }
    if (stor.errMalEcrit) {
      j3pAffiche(stor.lesdiv.explications, null, 'La réponse est mal écrite !\n')
      yaexplik = true
      stor.compteurPe2++
    }
    if (stor.errVirgFrac) {
      j3pAffiche(stor.lesdiv.explications, null, 'Il n’y a pas de virgule dans une fraction !\n')
      yaexplik = true
    }
    if (stor.errAprox) {
      j3pAffiche(stor.lesdiv.explications, null, 'Il ne doit y avoir qu’un seul chiffre après la virgule !\n')
      yaexplik = true
    }

    if (isFin) {
      // barre les faux
      barrelesfo()
      desactiveAll()
      let t1, t2, t3, t4, t5, t6
      stor.yaco = true
      // afficheco
      if (stor.encours === 'Descrip') {
        j3pAffiche(stor.lesdiv.solution, null, 'Dans le triangle ' + stor.nomtri + ', \n')
        j3pAffiche(stor.lesdiv.solution, null, 'le point ' + stor.lettres[3] + ' appartient au côté [' + stor.lettres[1] + stor.lettres[0] + '] , \n')
        j3pAffiche(stor.lesdiv.solution, null, 'le point ' + stor.lettres[4] + ' appartient au côté [' + stor.lettres[1] + stor.lettres[2] + '] , \n')
        j3pAffiche(stor.lesdiv.solution, null, 'les droites (' + stor.lettres[3] + stor.lettres[4] + ') et (' + stor.lettres[0] + stor.lettres[2] + ') sont parallèles. \n')
        j3pAffiche(stor.lesdiv.solution, null, 'J’utilise la propriété de Thalès ,')
      }
      if (stor.encours === 'egalite') {
        j3pAffiche(stor.lesdiv.solution, null, 'donc $\\frac{' + stor.lettres[1] + stor.lettres[3] + '}{' + stor.lettres[1] + stor.lettres[0] + '} = \\frac{' + stor.lettres[1] + stor.lettres[4] + '}{' + stor.lettres[1] + stor.lettres[2] + '} = \\frac{' + stor.lettres[3] + stor.lettres[4] + '}{' + stor.lettres[0] + stor.lettres[2] + '}$')
      }
      if (stor.encours === 'rempli') {
        if (stor.z1.ffalse === true) { t1 = stor.listeE1.reponse } else { t1 = (longueurat(stor.listeE1.reponse) + '').replace('.', ',') }
        if (stor.z2.ffalse === true) { t2 = stor.listeE2.reponse } else { t2 = (longueurat(stor.listeE2.reponse) + '').replace('.', ',') }
        if (stor.z3.ffalse === true) { t3 = stor.listeE3.reponse } else { t3 = (longueurat(stor.listeE3.reponse) + '').replace('.', ',') }
        if (stor.z4.ffalse === true) { t4 = stor.listeE4.reponse } else { t4 = (longueurat(stor.listeE4.reponse) + '').replace('.', ',') }
        if (stor.z5.ffalse === true) { t5 = stor.listeE5.reponse } else { t5 = (longueurat(stor.listeE5.reponse) + '').replace('.', ',') }
        if (stor.z6.ffalse === true) { t6 = stor.listeE6.reponse } else { t6 = (longueurat(stor.listeE6.reponse) + '').replace('.', ',') }
        j3pAffiche(stor.lesdiv.solution, null, 'donc $\\frac{' + t1 + '}{' + t2 + '} = \\frac{' + t3 + '}{' + t4 + '} = \\frac{' + t5 + '}{' + t6 + '}$')
      }
      if (stor.encours === 'prod') {
        j3pAffiche(stor.lesdiv.solution, null, '$' + stor.lch + ' = \\frac{' + stor.atthaut[0] + ' \\times ' + stor.atthaut[1] + ' }{' + stor.attbas + '}$')
      }
      if (stor.encours === 'concl') {
        t1 = parseFloat(stor.atthaut[0].replace(',', '.')) * parseFloat(stor.atthaut[1].replace(',', '.'))
        t2 = parseFloat(stor.attbas.replace(',', '.'))
        while ((Math.round(t1) !== t1) || (Math.round(t2) !== t2)) {
          t1 = j3pArrondi(t1 * 10, 8)
          t2 = j3pArrondi(t2 * 10, 8)
        }
        t3 = j3pPGCD(t1, t2, { negativesAllowed: true, valueIfZero: 1 })
        t1 = Math.round(t1 / t3)
        t2 = Math.round(t2 / t3)
        if (t2 === 1) {
          j3pAffiche(stor.lesdiv.solution, null, '$' + stor.lch + ' = ' + t1 + ' $ cm ')
        } else {
          if (stor.Yaapprox) {
            j3pAffiche(stor.lesdiv.solution, null, '$' + stor.lch + ' \\approx ' + (j3pArrondi(t1 / t2, 1) + '').replace('.', ',') + '$ cm')
          } else {
            j3pAffiche(stor.lesdiv.solution, null, '$' + stor.lch + ' = \\frac{' + t1 + ' }{' + t2 + '}$ cm')
          }
        }
      }
      stor.lesdiv.solution.classList.add('correction')
    }
    if (yaexplik) {
      stor.lesdiv.explications.classList.add('explique')
    }
  } // affCorrFaux

  function enonceMain () {
    if (((me.questionCourante % ds.nbetapes) === 1) || (ds.nbetapes === 1)) {
      me.videLesZones()
    }
    creeLesDiv()
    stor.lesdiv.travail.classList.add('travail')
    stor.lesdiv.consigneG.classList.add('enonce')
    stor.lesdiv.figure.classList.add('enonce')
    me.zonesElts.MG.classList.add('fond')
    stor.Lexo = faisChoixExos()
    faiFigure()
    poseQuestion()
    if (ds.Calculatrice) {
      me.fenetresjq = [{ name: 'Calculatrice', title: 'Une calculatrice', left: 600, top: 10 }]
      j3pCreeFenetres(me)
      j3pAfficheCroixFenetres('Calculatrice')
      j3pAjouteBouton(stor.lesdiv.calculatrice, boutonfen, { className: 'MepBoutons', value: 'Calculatrice' })
    }
    if ((stor.Lexo.enonc === 'Tex') && ds.Brouillon) {
      Paint.create(stor.lesdiv.figure, { width: 200, height: 150, textes: stor.lettres })
    }
    me.finEnonce()
  }

  switch (this.etat) {
    case 'enonce':
      if (me.debutDeLaSection) {
        initSection()
      } else {
        enonceMain()
      }
      break // case "enonce":

    case 'correction':
      // On teste si une réponse a été saisie
      stor.lesdiv.explications.innerHTML = ''
      stor.lesdiv.solution.innerHTML = ''

      if (!yaReponse()) {
        // pas de réponse
        stor.lesdiv.correction.style.color = this.styles.cfaux
        stor.lesdiv.correction.innerHTML = reponseIncomplete
        return this.finCorrection()
      }

      if (isRepOk()) {
        // Bonne réponse
        stor.lesdiv.correction.style.color = this.styles.cbien
        stor.lesdiv.correction.innerHTML = cBien

        passeToutVert()
        desactiveAll()
        if (stor.encours === 'Descrip') {
          if (ds.Egalite) {
            poseEgalite()
            stor.niv++
            return
          }
          if (ds.Reponse) {
            rempliEgalite()
            stor.niv++
            return
          }
        }
        if (stor.encours === 'egalite') {
          if (ds.Reponse) {
            poseRempli()
            stor.niv++
            return
          }
        }
        if (stor.encours === 'rempli') {
          poseProduit()
          return
        }
        if (stor.encours === 'prod') {
          poseFin()
          return
        }
        stor.encours = ''

        this.score++
        this.typederreurs[0]++
        return this.finCorrection('navigation', true)
      }

      // Pas de bonne réponse
      stor.lesdiv.correction.style.color = this.styles.cfaux
      stor.lesdiv.correction.innerHTML = cFaux
      if (this.isElapsed) {
        // A cause de la limite de temps :
        stor.lesdiv.correction.innerHTML += tempsDepasse + '<BR>'
        this.typederreurs[10]++
        affCorrFaux(true)
        return this.finCorrection('navigation', true)
      }

      // Réponse fausse sans limite de temps
      if (this.essaiCourant < ds.nbchances) {
        // il reste des essais
        stor.lesdiv.correction.innerHTML += '<br>' + essaieEncore
        affCorrFaux(false)
        this.typederreurs[1]++
        return this.finCorrection()
      }

      // Erreur au dernier essai
      affCorrFaux(true)

      if (stor.encours === 'compare') {
        this.score = j3pArrondi(this.score + 0.1, 1)
      }
      if (stor.encours === 'calcul') {
        this.score = j3pArrondi(this.score + 0.4, 1)
      }
      if (stor.encours === 'concl') {
        this.score = j3pArrondi(this.score + 0.7, 1)
      }

      this.typederreurs[2]++
      this.finCorrection('navigation', true)
      break // case "correction":

    case 'navigation':
      if (this.sectionTerminee()) {
        this.parcours.pe = getPe(ds, stor)
      } else {
        this.etat = 'enonce'
      }
      this.finNavigation()
      break // case "navigation":
  }
}
