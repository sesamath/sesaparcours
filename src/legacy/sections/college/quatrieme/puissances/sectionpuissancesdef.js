import { j3pAddElt, j3pClone, j3pEmpty, j3pGetRandomInt, j3pPGCD, j3pShuffle } from 'src/legacy/core/functions'
import BoutonStyleMathquill from 'src/legacy/outils/boutonsStyleMatquill'
import { j3pAffiche } from 'src/lib/mathquill/functions'
import { addDefaultTable } from 'src/legacy/themes/table'
import ZoneStyleMathquill3 from 'src/legacy/outils/zoneStyleMathquill/ZoneStyleMathquill3'
import { affichepuis } from 'src/legacy/outils/zoneStyleMathquill/functions'
import textesGeneriques from 'src/lib/core/textes'

const { cBien, cFaux, essaieEncore, reponseIncomplete, tempsDepasse } = textesGeneriques

/**
 * Ajouter une description ici
 * @fileOverview
 * @author Tommy Barroy <t.barroy@laposte.net>
 * @since 2017-10
 */
export const params = {
  outils: ['mathquill'],
  /**
   * Ce qui est paramétrable dans l’éditeur de graphe
   * @type {LegacySectionParamsParametres}
   */
  parametres: [
    ['nbrepetitions', 9, 'entier', 'Nombre de répétitions de la section'],
    ['nbchances', 2, 'entier', 'Nombre d’essais possibles.'],
    ['Question', 'Puissance', 'liste', 'Ecriture attendue', ['Puissance', 'Produit', 'les deux']],
    ['Exp_Positifs', 'parfois', 'liste', 'Signe de l’exposant.', ['toujours', 'jamais', 'parfois']],
    ['Fractions', 'parfois', 'liste', 'Possibilité de fractions.', ['toujours', 'jamais', 'parfois']],
    ['Positifs', 'parfois', 'liste', 'Signe du facteur.', ['toujours', 'jamais', 'parfois']],
    // le modèle gère la compatibilité ascendante avec l’ancien paramètre MiseEnPagePlusClaire
    ['theme', 'standard', 'liste', 'Modèle pour l’affichage', ['standard', 'zonesAvecImageDeFond']]
  ]
}

const dummyFn = () => undefined

/**
 * section puissancesdef
 * @this {Parcours}
 */
export default function main () {
  const me = this
  const stor = me.storage
  const ds = me.donneesSection

  function creeLesDiv () {
    stor.lesdiv = {}
    stor.lesdiv.conteneur = j3pAddElt(me.zonesElts.MG, 'div', {
      style: me.styles.etendre('toutpetit.enonce', { padding: '10px' })
    })
    stor.lesdiv.consigneG = addDefaultTable(stor.lesdiv.conteneur, 1, 1)[0][0]
    stor.lesdiv.travail = addDefaultTable(stor.lesdiv.conteneur, 1, 1)[0][0]
    stor.lesdiv.explications = j3pAddElt(stor.lesdiv.conteneur, 'div')
    stor.lesdiv.solution = j3pAddElt(stor.lesdiv.conteneur, 'div')
    stor.lesdiv.calculatrice = j3pAddElt(me.zonesElts.MD, 'div', {
      style: me.styles.etendre('toutpetit.enonce', { padding: '10px' })
    })
    stor.lesdiv.correction = j3pAddElt(me.zonesElts.MD, 'div', {
      style: me.styles.etendre('toutpetit.enonce', { padding: '10px' })
    })
    stor.lesdiv.solution.style.color = me.styles.toutpetit.correction.color
    stor.lesdiv.explications.style.color = me.styles.cfaux
  }

  function initSection () {
    stor.yaeu0 = false
    stor.yaeu1 = false
    let i

    // Construction de la page
    me.construitStructurePage('presentation1bis')

    let lp
    if (ds.Exp_Positifs === 'jamais') lp = [false]
    if (ds.Exp_Positifs === 'toujours') lp = [true]
    if (ds.Exp_Positifs === 'parfois') lp = [false, true]
    lp = j3pShuffle(lp)
    me.donneesSection.nbitems = me.donneesSection.nbetapes * me.donneesSection.nbrepetitions
    ds.lesExos = []
    for (i = 0; i < me.donneesSection.nbitems; i++) {
      ds.lesExos.push({ signen: lp[i % lp.length] })
    }
    ds.lesExos = j3pShuffle(ds.lesExos)
    me.typederreurs = [0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0]

    if (ds.Positifs === 'jamais') lp = [false]
    if (ds.Positifs === 'toujours') lp = [true]
    if (ds.Positifs === 'parfois') lp = [false, true]
    lp = j3pShuffle(lp)
    for (i = 0; i < me.donneesSection.nbitems; i++) {
      ds.lesExos[i].signe = lp[i % lp.length]
    }
    ds.lesExos = j3pShuffle(ds.lesExos)

    if (ds.Fractions === 'jamais') lp = [false]
    if (ds.Fractions === 'toujours') lp = [true]
    if (ds.Fractions === 'parfois') lp = [false, true]
    for (i = 0; i < me.donneesSection.nbitems; i++) {
      ds.lesExos[i].fraction = lp[i % lp.length]
    }
    ds.lesExos = j3pShuffle(ds.lesExos)

    if (ds.Question === 'Puissance') lp = ['Puissance']
    if (ds.Question === 'Produit') lp = ['Produit']
    if (ds.Question === 'les deux') lp = ['Produit', 'Puissance']
    for (i = 0; i < me.donneesSection.nbitems; i++) {
      ds.lesExos[i].question = lp[i % lp.length]
    }
    ds.lesExos = j3pShuffle(ds.lesExos)

    me.score = 0

    me.afficheTitre('Notation puissance')
    enonceMain()
  }
  function faisChoixExo () {
    stor.restric = '0123456789-()'
    stor.Lexo = ds.lesExos.pop()
    stor.nb = []
    stor.a = j3pGetRandomInt(2, 99)
    if (!stor.Lexo.signe) stor.a = -stor.a
    if (stor.Lexo.fraction) {
      do {
        stor.a = { num: j3pGetRandomInt(2, 9) }
        stor.a.den = j3pGetRandomInt(2, 9)
      }
      while (j3pPGCD(stor.a.num, stor.a.den) !== 1)
      if (!stor.Lexo.signe) stor.a.num = -stor.a.num
    }
    stor.na = j3pGetRandomInt(0, 5)
    if (!stor.Lexo.signen && stor.na !== 0) stor.na = -stor.na
    if (stor.yaeum1 && stor.na === -1) stor.na--
    if (stor.yaeu0 && stor.na === 0) stor.na++
    if (stor.yaeu1 && stor.na === 1) stor.na++
    if (stor.na === 0) stor.yaeu0 = true
    if (stor.na === 1) stor.yaeu1 = true
    if (stor.na === -1) stor.yaeum1 = true
    if (stor.Lexo.fraction) { stor.restric += '/' }
  }

  function poseQuestion () {
    let buf, buf2, t, buf3
    let i
    stor.sur = false
    if (stor.Lexo.question === 'Puissance') {
      if (!stor.Lexo.fraction) {
        buf = String(stor.a).replace('.', ',')
        buf2 = String(stor.a).replace('.', ',')
        if (stor.a < 0) buf2 = '(' + buf2 + ')'
        stor.comp = [buf, buf2]
      } else {
        buf = '\\frac{' + Math.abs(stor.a.num) + '}{' + stor.a.den + '}'
        if (stor.a.num < 0) {
          buf = '-' + buf
        }
        buf2 = '(' + buf + ')'
        stor.comp = [buf, buf2]
      }
      for (i = 1; i < Math.abs(stor.na); i++) {
        buf += '\\text{ } \\times ' + buf2
      }
      if (stor.na < 0) {
        buf = '\\frac{1}{' + buf + '}'
      }
      if (stor.na === 0) buf = '1'
      const tabG = addDefaultTable(stor.lesdiv.consigneG, 2, 1)
      j3pAffiche(tabG[0][0], null, '<b>Écris $A$ sous la forme d’une puissance de $' + stor.comp[0] + '$. </b>')
      j3pAffiche(tabG[1][0], null, '$A$ = $' + buf + '$')

      stor.tabAa = addDefaultTable(stor.lesdiv.travail, 1, 3)
      j3pAffiche(stor.tabAa[0][0], null, '$A$')
      j3pAffiche(stor.tabAa[0][1], null, '$=$')
      t = affichepuis(stor.tabAa[0][2])
      if (stor.na === 0) {
        // FIXME ça doit être un vrai objet ZoneStyleMathquill3 et pas ce fake
        stor.zoneRepA = { prevenDefault: dummyFn, barreIfKo: dummyFn, corrige: dummyFn, disable: dummyFn, texta: { elemnum: [buf2] }, reponse: () => buf2 }
        j3pAffiche(t[0], null, '$' + buf2 + '$')
      } else {
        stor.zoneRepA = new ZoneStyleMathquill3(t[0], { restric: stor.restric, enter: me.sectionCourante.bind(me) })
      }
      stor.zoneRep = new ZoneStyleMathquill3(t[2], { restric: stor.restric, bloqueFraction: '*-()', enter: me.sectionCourante.bind(me) })
    } else {
      if (stor.na === 0) buf2 = '1'
      if (!stor.Lexo.fraction) {
        buf2 = String(stor.a).replace('.', ',')
        stor.comp = [buf2]
        buf2 = '(' + buf2 + ')'
        stor.comp.push(buf2)
      } else {
        buf2 = '\\frac{' + Math.abs(stor.a.num) + '}{' + stor.a.den + '}'
        if (stor.a.num < 0) buf2 = '-' + buf2
        stor.comp = [buf2]
        buf2 = '(' + buf2 + ')'
        stor.comp.push(buf2)
        if (stor.na < 0) {
          buf3 = '\\frac{' + stor.a.den + '}{' + Math.abs(stor.a.num) + '}'
          if (stor.a.num < 0) buf3 = '-' + buf3
          stor.comp.push(buf3)
          buf3 = '(' + buf3 + ')'
          stor.comp.push(buf3)
        }
      }
      const tabG = addDefaultTable(stor.lesdiv.consigneG, 2, 1)
      let ttt = '<b>Ecris l’expression correspondant à la puissance donnée. </b> <br> <i>(aucun calcul ou changement de signe n’est attendu.)</i>'
      if (stor.na === 0 || stor.na === 1) {
        ttt = '<b>Ecris la valeur correspondant à la puissance donnée. </b> <br>'
      }
      j3pAffiche(tabG[0][0], null, ttt)
      j3pAffiche(tabG[1][0], null, '$A$ = ${' + buf2 + '}^{' + stor.na + '}$')
      stor.tabAa = addDefaultTable(stor.lesdiv.travail, 1, 4)
      j3pAffiche(stor.tabAa[0][0], null, '$A$')
      j3pAffiche(stor.tabAa[0][1], null, '$=$')
      stor.tabGh = addDefaultTable(stor.tabAa[0][2], 2, 1)
      stor.zoneRep = new ZoneStyleMathquill3(stor.tabGh[1][0], { restric: stor.restric + '*', bloqueFraction: '*-()', enter: me.sectionCourante.bind(me) })
      if (ds.Exp_Positifs === 'parfois' || ds.Exp_Positifs === 'jamais') {
        stor.bout = new BoutonStyleMathquill(stor.tabAa[0][3], 'boivhfe', '$\\frac{1}{x}$', function () { faisbout() }, { mathquill: true })
      }
    }
  }
  function faisbout () {
    j3pEmpty(stor.tabAa[0][3])
    j3pAffiche(stor.tabGh[0][0], null, '$1$')
    stor.tabGh[0][0].style.borderBottom = '1px solid black'
    stor.tabGh[0][0].style.textAlign = 'center'
    stor.bout = new BoutonStyleMathquill(stor.tabAa[0][3], 'boivhfe', 'annuler', function () { faisbout2() }, { mathquill: false })
    stor.sur = true
  }
  function faisbout2 () {
    j3pEmpty(stor.tabAa[0][3])
    j3pEmpty(stor.tabGh[0][0])
    stor.tabGh[0][0].style.borderBottom = ''
    stor.bout = new BoutonStyleMathquill(stor.tabAa[0][3], 'boivhfe', '$\\frac{1}{x}$', function () { faisbout() }, { mathquill: true })
    stor.sur = false
  }

  function yaReponse () {
    if (stor.Lexo.question === 'Puissance') {
      if (stor.zoneRepA.reponse() === '') {
        stor.zoneRepA.focus()
        return false
      }
    }
    if (stor.zoneRep.reponse() === '') {
      stor.zoneRep.focus()
      return false
    }
    return true
  }
  function isRepOk () {
    let i, tabrep2, buf, tabrep, tabrepA, ya1, ya2

    stor.errPasPRod = false
    stor.errFact = false
    stor.errPar = false
    stor.errExp = false
    stor.errSignExp = false
    stor.errCompli = false
    stor.errSignExp2 = false

    stor.errEcrit = false
    stor.errExp2 = false
    stor.errFact1 = false
    stor.errPar1 = false
    stor.errParF = false

    if (stor.Lexo.question === 'Produit') {
      tabrep2 = j3pClone(stor.zoneRep.texta.elemnum)
      tabrep = []
      for (i = 0; i < tabrep2.length; i++) {
        if (Array.isArray(tabrep2[i])) {
          tabrep.push('\\frac{' + tabrep2[i][0] + '}{' + tabrep2[i][1] + '}')
        } else {
          tabrep.push(tabrep2[i])
        }
      }
      tabrep2 = j3pClone(tabrep)
      tabrep = []
      buf = ''
      for (i = 0; i < tabrep2.length; i++) {
        if (tabrep2[i] === '×') {
          tabrep.push(buf)
          tabrep.push('×')
          buf = ''
        } else {
          buf += tabrep2[i]
        }
      }
      tabrep.push(buf)

      // verif produit
      for (i = 1; i < tabrep.length; i += 2) {
        if (tabrep[i] !== '×') {
          stor.errPasPRod = true
          stor.zoneRep.corrige(false)
          return false
        }
      }

      // verif c’est plausible
      // erreur ecriture truc comme ca
      for (i = 0; i < tabrep.length; i += 2) {
        if (stor.comp.indexOf(tabrep[i]) === -1) {
          if (stor.na === 0 && tabrep[i] === '1') continue
          stor.errFact = true
          stor.zoneRep.corrige(false)
          return false
        }
        ya1 = ya1 || stor.comp.indexOf(tabrep[i]) < 2
        ya2 = ya2 || stor.comp.indexOf(tabrep[i]) > 1
      }

      // verif les ()
      for (i = 2; i < tabrep.length; i += 2) {
        if (tabrep[i][0] === '-') {
          stor.errPar = true
          stor.zoneRep.corrige(false)
          return false
        }
      }

      if (tabrep.length !== Math.abs(stor.na) * 2 - 1) {
        if (stor.na === 0 && tabrep.length === 1 && tabrep[0] === '1') return true
        stor.errExp = true
        stor.zoneRep.corrige(false)
        return false
      }

      if (stor.Lexo.fraction) {
        if (stor.na > 0) {
          if (stor.sur) {
            stor.errSignExp = true
            stor.zoneRep.corrige(false)
            return false
          }
        } else {
          if (ya1 && ya2) {
            stor.errCompli = true
            stor.zoneRep.corrige(false)
            return false
          }
          if (ya1 && !stor.sur) {
            stor.errSignExp = true
            stor.zoneRep.corrige(false)
            return false
          }
          if (ya2 && stor.sur) {
            stor.errSignExp2 = true
            stor.zoneRep.corrige(false)
            return false
          }
        }
      } else {
        if (stor.sur !== (stor.na < 0)) {
          stor.errSignExp = true
          stor.zoneRep.corrige(false)
          return false
        }
      }

      return true
    }
    tabrep2 = j3pClone(stor.zoneRep.texta.elemnum)
    tabrep = ''
    for (i = 0; i < tabrep2.length; i++) {
      if (Array.isArray(tabrep2[i])) {
        tabrep += '\\frac{' + tabrep2[i][0] + '}{' + tabrep2[i][1] + '}'
      } else {
        tabrep += tabrep2[i]
      }
    }
    tabrep2 = j3pClone(stor.zoneRepA.texta.elemnum)
    tabrepA = ''
    for (i = 0; i < tabrep2.length; i++) {
      if (Array.isArray(tabrep2[i])) {
        tabrepA += '\\frac{' + tabrep2[i][0] + '}{' + tabrep2[i][1] + '}'
      } else {
        tabrepA += tabrep2[i]
      }
    }
    if (tabrep.indexOf('-') !== 0 && tabrep.indexOf('-') !== -1) {
      stor.zoneRep.corrige(false)
      stor.errEcrit = true
      return false
    }
    if (tabrep.indexOf('-') !== tabrep.lastIndexOf('-')) {
      stor.zoneRep.corrige(false)
      stor.errEcrit = true
      return false
    }
    if (Number(tabrep) !== stor.na) {
      stor.zoneRep.corrige(false)
      stor.errExp2 = true
      return false
    }
    if (stor.comp.indexOf(tabrepA) === -1) {
      stor.zoneRepA.corrige(false)
      stor.errFact1 = true
      return false
    }
    if (stor.Lexo.fraction) {
      if (tabrepA.indexOf('(') === -1) {
        stor.zoneRepA.corrige(false)
        stor.errParF = true
        return false
      }
    } else {
      if (stor.a < 0 && (stor.na % 2 === 0)) {
        if (tabrepA.indexOf('(') === -1) {
          stor.zoneRep.corrige(false)
          stor.errPar1 = true
          return false
        }
      }
    }

    return true
  } // isRepOk

  function desactiveAll () {
    if (stor.Lexo.question === 'Puissance') {
      stor.zoneRepA.disable()
      stor.zoneRep.disable()
    } else {
      j3pEmpty(stor.tabAa[0][3])
      stor.zoneRep.disable()
    }
  } // desactiveAll
  function passeToutVert () {
    if (stor.Lexo.question === 'Puissance') {
      stor.zoneRepA.corrige(true)
      stor.zoneRep.corrige(true)
    } else {
      stor.zoneRep.corrige(true)
    }
  }
  function barrelesfo () {
    if (stor.Lexo.question === 'Puissance') {
      stor.zoneRepA.barreIfKo()
      stor.zoneRep.barreIfKo()
    } else {
      stor.zoneRep.barreIfKo()
    }
  }
  function affCorrFaux (isFin) {
    /// //affiche indic
    let buf, i

    if (stor.errPasPRod) {
      j3pAffiche(stor.lesdiv.explications, null, 'Erreur d’écriture !\n')
      stor.yaexplik = true
    }
    if (stor.errFact && stor.na !== 0) {
      if (stor.na > 0) {
        j3pAffiche(stor.lesdiv.explications, null, 'Tous les facteurs doivent être égaux à $' + stor.comp[0] + '$ !\n')
      } else {
        j3pAffiche(stor.lesdiv.explications, null, 'Tous les diviseurs doivent être égaux à $' + stor.comp[0] + '$ !\n')
      }
      stor.yaexplik = true
    }
    if (stor.errPar) {
      j3pAffiche(stor.lesdiv.explications, null, 'Il manque des parenthèses !\n')
      stor.yaexplik = true
    }
    if (stor.errExp) {
      j3pAffiche(stor.lesdiv.explications, null, 'Le nombre de facteurs est incorrect !\n')
      stor.yaexplik = true
    }
    if (stor.errSignExp) {
      if (stor.na > 0) {
        j3pAffiche(stor.lesdiv.explications, null, 'L’exposant est positif  !\n')
        stor.yaexplik = true
      } else {
        j3pAffiche(stor.lesdiv.explications, null, 'Un exposant est négatif indique une division !\n')
        stor.yaexplik = true
      }
    }
    if (stor.errCompli) {
      j3pAffiche(stor.lesdiv.explications, null, 'Une écriture mélangeant $' + stor.comp[0] + '$ et $' + stor.comp[2] + '$ n’est pas judicieuse ici !\n')
      stor.yaexplik = true
    }

    if (stor.errEcrit) {
      j3pAffiche(stor.lesdiv.explications, null, 'Erreur d’écriture !\n')
      stor.yaexplik = true
    }
    if (stor.errExp2) {
      j3pAffiche(stor.lesdiv.explications, null, 'Erreur d’exposant ! \n')
      stor.yaexplik = true
    }
    if (stor.errPar1) {
      j3pAffiche(stor.lesdiv.explications, null, 'Il faut des parenthèses pour que la puissance concerne le signe - !\n')
      stor.yaexplik = true
    }
    if (stor.errParF) {
      j3pAffiche(stor.lesdiv.explications, null, 'Il faut des parenthèses pour que la puissance concerne la fraction ! \n')
      stor.yaexplik = true
    }
    if (stor.errFact1) {
      j3pAffiche(stor.lesdiv.explications, null, 'Je ne retrouve pas le nombre demandé ! \n')
      stor.yaexplik = true
    }

    if (isFin) {
      // barre les faux
      barrelesfo()
      desactiveAll()
      stor.yaco = true
      if (stor.na < 0) {
        j3pAffiche(stor.lesdiv.explications, null, '$' + stor.comp[Math.min(1, stor.comp.length - 1)] + '^{' + stor.na + '} = \\frac{1}{' + stor.comp[Math.min(1, stor.comp.length - 1)] + '^{' + (-stor.na) + '}}$ \n')
        stor.yaexplik = true
      } else if (stor.na === 0) {
        j3pAffiche(stor.lesdiv.explications, null, '$x^{0} = 1$ pour tout nombre $x$ différent de 0 \n')
        stor.yaexplik = true
      } else if (stor.na !== 1) {
        j3pAffiche(stor.lesdiv.explications, null, '$' + stor.comp[Math.min(1, stor.comp.length - 1)] + '^{' + stor.na + '}$ est égal au produit de $' + stor.na + '$ facteurs égaux à $' + stor.comp[0] + '$ \n')
        stor.yaexplik = true
      } else {
        j3pAffiche(stor.lesdiv.explications, null, '$x^{1} = x$  \n')
        stor.yaexplik = true
      }
      if (stor.Lexo.question === 'Produit') {
        buf = stor.comp[0]
        for (i = 1; i < Math.abs(stor.na); i++) {
          buf += ' \\times ' + stor.comp[Math.min(1, stor.comp.length - 1)]
        }
        if (stor.na < 0) {
          buf = '\\frac{1}{' + buf + '}'
        }
        if (stor.na === 0) buf = '1'
        j3pAffiche(stor.lesdiv.solution, null, '$A = ' + buf + '$')
      }
      if (stor.Lexo.question === 'Puissance') {
        buf = '{' + stor.comp[stor.comp.length - 1] + '}^{' + stor.na + '}'
        if (stor.na === 0) buf = '1'
        j3pAffiche(stor.lesdiv.solution, null, '$A = ' + buf + '$')
      }
    } else { me.afficheBoutonValider() }
  } // affCorrFaux
  function enonceMain () {
    if (me.etapeCourante === 1) {
      // A chaque répétition, on nettoie la scène
      // A commenter si besoin
      me.videLesZones()
    }
    creeLesDiv()
    stor.lesdiv.travail.classList.add('travail')
    stor.lesdiv.consigneG.classList.add('enonce')
    me.zonesElts.MG.classList.add('fond')
    faisChoixExo()
    poseQuestion()
    me.finEnonce()
  }

  switch (this.etat) {
    case 'enonce':
      // code exécuté au lancement de la section
      if (me.debutDeLaSection) {
        initSection()
      } else {
        enonceMain()
      }
      break // case "enonce":

    case 'correction':
      // On teste si une réponse a été saisie
      stor.yaexplik = false
      stor.yaco = false
      stor.lesdiv.explications.innerHTML = ''
      stor.lesdiv.solution.innerHTML = ''
      stor.lesdiv.explications.classList.remove('explique')

      if (yaReponse()) {
        // Bonne réponse
        if (isRepOk()) {
          this._stopTimer()
          stor.lesdiv.correction.style.color = this.styles.cbien
          stor.lesdiv.correction.innerHTML = cBien

          desactiveAll()
          passeToutVert()

          this.score++
          this.typederreurs[0]++
          this.cacheBoutonValider()
          this.etat = 'navigation'
          this.sectionCourante()
        } else {
          // Pas de bonne réponse
          stor.lesdiv.correction.style.color = this.styles.cfaux
          stor.lesdiv.correction.innerHTML = cFaux
          if (this.isElapsed) {
            // A cause de la limite de temps :
            this._stopTimer()

            stor.lesdiv.correction.innerHTML = tempsDepasse + '<BR>'
            this.typederreurs[10]++
            this.cacheBoutonValider()

            affCorrFaux(true)
            stor.yaco = true

            this.etat = 'navigation'
            this.sectionCourante()
          } else {
            // Réponse fausse :
            if (me.essaiCourant < me.donneesSection.nbchances) {
              stor.lesdiv.correction.innerHTML += '<br>' + essaieEncore

              affCorrFaux(false)

              // affCorrFaux.call(this, false)
              this.typederreurs[1]++
            } else {
              // Erreur au nème essai
              this._stopTimer()
              this.cacheBoutonValider()

              affCorrFaux(true)
              stor.yaco = true

              this.typederreurs[2]++
              this.etat = 'navigation'
              this.sectionCourante()
            }
          }
        }
      } else {
        // pas de réponse
        stor.lesdiv.correction.style.color = this.styles.cfaux
        stor.lesdiv.correction.innerHTML = reponseIncomplete
        this.afficheBoutonValider()
      }

      if (stor.yaexplik) stor.lesdiv.explications.classList.add('explique')
      if (stor.yaco) stor.lesdiv.solution.classList.add('correction')
      // Obligatoire
      this.finCorrection()
      break // case "correction":

    case 'navigation':
      if (!this.sectionTerminee()) {
        this.etat = 'enonce'
      }
      this.finNavigation()
      break // case "navigation":
  }
}
