import { j3pAddElt, j3pArrondi, j3pClone, j3pEmpty, j3pShuffle } from 'src/legacy/core/functions'
import BoutonStyleMathquill from 'src/legacy/outils/boutonsStyleMatquill'
import { verifNombreBienEcrit } from 'src/legacy/outils/fonctions/ecritureNombre'
import { j3pAffiche } from 'src/lib/mathquill/functions'
import { affichepuis } from 'src/legacy/outils/zoneStyleMathquill/functions'
import { addDefaultTable } from 'src/legacy/themes/table'
import ZoneStyleMathquill1 from 'src/legacy/outils/zoneStyleMathquill/ZoneStyleMathquill1'
import ZoneStyleMathquill3 from 'src/legacy/outils/zoneStyleMathquill/ZoneStyleMathquill3'
import textesGeneriques from 'src/lib/core/textes'

const { cBien, cFaux, essaieEncore, reponseIncomplete, tempsDepasse } = textesGeneriques

/**
 * Ajouter une description ici
 * @fileOverview
 * @author Tommy Barroy <t.barroy@laposte.net>
 * @since 2017-10
 */
export const params = {
  outils: ['mathquill'],
  /**
   * Ce qui est paramétrable dans l’éditeur de graphe
   * @type {LegacySectionParamsParametres}
   */
  parametres: [
    ['nbrepetitions', 9, 'entier', 'Nombre de répétitions de la section'],
    ['nbchances', 2, 'entier', 'Nombre d’essais possibles.'],
    ['Question', 'Puissance', 'liste', 'Ecriture attendue', ['Puissance', 'Decimale', 'les deux']],
    ['Exp_Positifs', 'parfois', 'liste', 'Signe de l’exposant.', ['toujours', 'jamais', 'parfois']],
    ['Espaces', true, 'boolean', '<u>true</u>: Les espace séparant les groupes de trois sont requis.'],
    // le modèle gère la compatibilité ascendante avec l’ancien paramètre MiseEnPagePlusClaire
    ['theme', 'standard', 'liste', 'Modèle pour l’affichage', ['standard', 'zonesAvecImageDeFond']]
  ]
}

/**
 * section puissancesdef10
 * @this {Parcours}
 */
export default function main () {
  const me = this
  const ds = me.donneesSection
  const stor = me.storage

  function creeLesDiv () {
    stor.lesdiv = {}
    stor.lesdiv.conteneur = j3pAddElt(me.zonesElts.MG, 'div', {
      style: me.styles.etendre('toutpetit.enonce', { padding: '10px' })
    })
    stor.lesdiv.consigneG = addDefaultTable(stor.lesdiv.conteneur, 1, 1)[0][0]
    stor.lesdiv.travail = addDefaultTable(stor.lesdiv.conteneur, 1, 1)[0][0]
    stor.lesdiv.explications = j3pAddElt(stor.lesdiv.conteneur, 'div')
    stor.lesdiv.solution = j3pAddElt(stor.lesdiv.conteneur, 'div')
    stor.lesdiv.calculatrice = j3pAddElt(me.zonesElts.MD, 'div', {
      style: me.styles.etendre('toutpetit.enonce', { padding: '10px' })
    })
    stor.lesdiv.correction = j3pAddElt(me.zonesElts.MD, 'div', {
      style: me.styles.etendre('toutpetit.enonce', { padding: '10px' })
    })
    stor.lesdiv.solution.style.color = me.styles.toutpetit.correction.color
    stor.lesdiv.explications.style.color = me.styles.cfaux
  }

  function initSection () {
    stor.lposbase = []
    let i
    for (i = -15; i < 16; i++) stor.lposbase.push(i)
    stor.lpos = []

    // Construction de la page
    me.construitStructurePage('presentation1bis')

    let lp
    if (ds.Exp_Positifs === 'jamais') lp = [false]
    if (ds.Exp_Positifs === 'toujours') lp = [true]
    if (ds.Exp_Positifs === 'parfois') lp = [false, true]
    lp = j3pShuffle(lp)
    me.donneesSection.nbitems = me.donneesSection.nbetapes * me.donneesSection.nbrepetitions
    ds.lesExos = []
    for (i = 0; i < me.donneesSection.nbitems; i++) {
      ds.lesExos.push({ signen: lp[i % lp.length] })
    }
    ds.lesExos = j3pShuffle(ds.lesExos)
    me.typederreurs = [0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0]

    if (ds.Question === 'Puissance') lp = ['Puissance']
    if (ds.Question === 'Decimale') lp = ['Decimal']
    if (ds.Question === 'les deux') lp = ['Decimal', 'Puissance']
    for (i = 0; i < me.donneesSection.nbitems; i++) {
      ds.lesExos[i].question = lp[i % lp.length]
    }
    ds.lesExos = j3pShuffle(ds.lesExos)

    me.score = 0

    me.afficheTitre('Puissances de 10')
    enonceMain()
  }
  function faisChoixExo () {
    stor.restric = '0123456789-'
    stor.Lexo = ds.lesExos.pop()
    stor.nb = []
    if (stor.lpos.length === 0) {
      stor.lpos = j3pClone(stor.lposbase)
      stor.lpos = j3pShuffle(stor.lpos)
    }
    let cpttt = 0
    do {
      cpttt++
      stor.na = stor.lpos[0]
      if ((stor.na > 0) !== stor.Lexo.signen) stor.lpos.push(stor.na)
      stor.lpos.splice(0, 1)
      if (cpttt === 50) {
        stor.lpos = []
        stor.na = 5
        if (!stor.signen) stor.na = -5
        return
      }
    } while ((stor.na > 0) !== stor.Lexo.signen)
  }

  function poseQuestion () {
    let buf, t, i
    if (stor.Lexo.question === 'Puissance') {
      if (stor.na >= 0) {
        buf = ''
        for (i = 0; i < stor.na; i++) {
          if (i % 3 === 0 && i !== 0) buf = '\\text{ }' + buf
          buf = '0' + buf
        }
        if (i % 3 === 0 && i !== 0) buf = '\\text{ }' + buf
        buf = '1' + buf
      } else {
        buf = '0,'
        for (i = 1; i < -stor.na; i++) {
          if ((i - 1) % 3 === 0 && i !== 1) buf += '\\text{ }'
          buf += '0'
        }
        if ((i - 1) % 3 === 0 && i !== 1) buf += '\\text{ }'
        buf += '1'
      }
      const tabGh = addDefaultTable(stor.lesdiv.consigneG, 2, 1)
      j3pAffiche(tabGh[0][0], null, '<b>Écris $A$ sous la forme d’une puissance de 10.</b>')
      j3pAffiche(tabGh[1][0], null, '$A$ = $' + buf + '$')
      stor.tabAa = addDefaultTable(stor.lesdiv.travail, 1, 3)
      j3pAffiche(stor.tabAa[0][0], null, '$A$')
      j3pAffiche(stor.tabAa[0][1], null, '$=$')
      t = affichepuis(stor.tabAa[0][2])
      j3pAffiche(t[0], null, '$10$')
      stor.zoneRep = new ZoneStyleMathquill3(t[2], { restric: stor.restric, bloqueFraction: '*-()', enter: me.sectionCourante.bind(me) })
    } else {
      const tabGh = addDefaultTable(stor.lesdiv.consigneG, 2, 1)
      j3pAffiche(tabGh[0][0], null, '<b>Calcule $A$. </b> \n')
      j3pAffiche(tabGh[1][0], null, '$A = {10}^{' + stor.na + '}$')
      stor.tabAa = addDefaultTable(stor.lesdiv.travail, 1, 4)
      j3pAffiche(stor.tabAa[0][0], null, '$A$')
      j3pAffiche(stor.tabAa[0][1], null, '$=$')
      stor.tabGh = addDefaultTable(stor.tabAa[0][2], 2, 1)
      stor.zoneRep = new ZoneStyleMathquill1(stor.tabGh[1][0], { restric: stor.restric + '.,  ', enter: me.sectionCourante.bind(me) })
    }
  }
  function faisbout () {
    j3pEmpty(stor.tabAa[0][3])
    j3pAffiche(stor.tabGh[0][0], null, '$1$')
    stor.tabGh[0][0].style.borderBottom = '1px solid black'
    stor.tabGh[0][0].style.textAlign = 'center'
    stor.bout = new BoutonStyleMathquill(stor.tabAa[0][3], 'boivhfe', 'annuler', function () { faisbout2() }, { mathquill: false })
    stor.sur = true
  }
  function faisbout2 () {
    j3pEmpty(stor.tabAa[0][3])
    j3pEmpty(stor.tabGh[0][0])
    stor.tabGh[0][0].style.borderBottom = ''
    stor.bout = new BoutonStyleMathquill(stor.tabAa[0][3], 'boivhfe', '$\\frac{1}{x}$', function () { faisbout() }, { mathquill: true })
    stor.sur = false
  }

  function yaReponse () {
    if (stor.zoneRep.reponse() === '') {
      stor.zoneRep.focus()
      return false
    }
    return true
  }
  function isRepOk () {
    let i, t, tabrep

    stor.errEcrit = false
    stor.errExp2 = false
    stor.errEcritList = []

    if (stor.Lexo.question === 'Decimal') {
      tabrep = stor.zoneRep.reponse()
      // verif c’est un nomb
      t = verifNombreBienEcrit(tabrep, ds.Espaces, true)
      if (!t.good) {
        stor.errEcrit = true
        stor.errEcritList.push(t.remede)
        stor.zoneRep.corrige(false)
        return false
      }
      tabrep = t.nb
      // verif cest le bon

      t = 0
      if (stor.na < 0) t = Math.abs(stor.na)
      if (tabrep !== j3pArrondi(Math.pow(10, stor.na), t)) stor.zoneRep.corrige(false)
      return (tabrep === j3pArrondi(Math.pow(10, stor.na), t))
    }
    const tabrep2 = j3pClone(stor.zoneRep.texta.elemnum)
    tabrep = ''
    for (i = 0; i < tabrep2.length; i++) {
      if (Array.isArray(tabrep2[i])) {
        tabrep += '\\frac{' + tabrep2[i][0] + '}{' + tabrep2[i][1] + '}'
      } else {
        tabrep += tabrep2[i]
      }
    }
    if (tabrep.indexOf('-') !== 0 && tabrep.indexOf('-') !== -1) {
      stor.zoneRep.corrige(false)
      stor.errEcrit = true
      return false
    }
    if (tabrep.indexOf('-') !== tabrep.lastIndexOf('-')) {
      stor.zoneRep.corrige(false)
      stor.errEcrit = true
      return false
    }
    if (Number(tabrep) !== stor.na) {
      stor.zoneRep.corrige(false)
      stor.errExp2 = true
      return false
    }
    return true
  } // isRepOk

  function desactiveAll () {
    stor.zoneRep.disable()
  } // desactiveAll
  function passeToutVert () {
    stor.zoneRep.corrige(true)
  }
  function barrelesfo () {
    stor.zoneRep.barre()
  }
  function affCorrFaux (isFin) {
    /// //affiche indic
    let buf, i

    if (stor.errEcrit) {
      j3pAffiche(stor.lesdiv.explications, null, 'Erreur d’écriture !\n')
      stor.yaexplik = true
    }
    if (stor.errExp2) {
      j3pAffiche(stor.lesdiv.explications, null, 'Erreur d’exposant ! \n')
      stor.yaexplik = true
    }

    if (isFin) {
      // barre les faux
      barrelesfo()
      desactiveAll()
      stor.yaco = true
      if (stor.Lexo.question === 'Decimal') {
        j3pAffiche(stor.lesdiv.solution, null, 'Le résultat doit comporter $' + Math.abs(stor.na) + '$ fois le chiffre $0$,\n')
        if (stor.na >= 0) {
          buf = ''
          for (i = 0; i < stor.na; i++) {
            if (i % 3 === 0 && i !== 0) buf = '\\text{ }' + buf
            buf = '0' + buf
          }
          if (i % 3 === 0 && i !== 0) buf = '\\text{ }' + buf
          buf = '1' + buf
        } else {
          buf = '0,'
          for (i = 1; i < -stor.na; i++) {
            if ((i - 1) % 3 === 0 && i !== 1) buf += '\\text{ }'
            buf += '0'
          }
          if ((i - 1) % 3 === 0 && i !== 1) buf += '\\text{ }'
          buf += '1'
        }
        if (stor.na === 0) buf = '1'
        j3pAffiche(stor.lesdiv.solution, null, '$A = ' + buf + '$')
      }
      if (stor.Lexo.question === 'Puissance') {
        j3pAffiche(stor.lesdiv.solution, null, 'Je compte $' + Math.abs(stor.na) + '$ fois le chiffre $0$,\n')
        buf = '{10}^{' + stor.na + '}'
        j3pAffiche(stor.lesdiv.solution, null, 'donc $A = ' + buf + '$')
      }
    } else { me.afficheBoutonValider() }
  } // affCorrFaux
  function enonceMain () {
    if (me.etapeCourante === 1) {
      me.videLesZones()
    }

    creeLesDiv()
    stor.lesdiv.travail.classList.add('travail')
    stor.lesdiv.consigneG.classList.add('enonce')
    me.zonesElts.MG.classList.add('fond')
    faisChoixExo()
    poseQuestion()
    me.finEnonce()
  }

  switch (this.etat) {
    case 'enonce':
      // code exécuté au lancement de la section
      if (me.debutDeLaSection) {
        initSection()
      } else {
        enonceMain()
      }
      break // case "enonce":

    case 'correction':
      // On teste si une réponse a été saisie
      stor.yaexplik = false
      stor.yaco = false
      stor.lesdiv.explications.innerHTML = ''
      stor.lesdiv.solution.innerHTML = ''
      stor.lesdiv.explications.classList.remove('explique')
      if (yaReponse()) {
        // Bonne réponse
        if (isRepOk()) {
          this._stopTimer()
          stor.lesdiv.correction.style.color = this.styles.cbien
          stor.lesdiv.correction.innerHTML = cBien

          desactiveAll()
          passeToutVert()

          this.score++
          this.typederreurs[0]++
          this.cacheBoutonValider()
          this.etat = 'navigation'
          this.sectionCourante()
        } else {
          // Pas de bonne réponse
          stor.lesdiv.correction.style.color = this.styles.cfaux
          stor.lesdiv.correction.innerHTML = cFaux
          if (this.isElapsed) {
            // A cause de la limite de temps :
            this._stopTimer()

            stor.lesdiv.correction.innerHTML = tempsDepasse + '<BR>'
            this.typederreurs[10]++
            this.cacheBoutonValider()

            affCorrFaux(true)
            stor.yaco = true

            this.etat = 'navigation'
            this.sectionCourante()
          } else {
            // Réponse fausse :
            if (me.essaiCourant < me.donneesSection.nbchances) {
              stor.lesdiv.correction.innerHTML += '<br>' + essaieEncore

              affCorrFaux(false)

              // affCorrFaux.call(this, false)
              this.typederreurs[1]++
            } else {
              // Erreur au nème essai
              this._stopTimer()
              this.cacheBoutonValider()

              affCorrFaux(true)
              stor.yaco = true

              this.typederreurs[2]++
              this.etat = 'navigation'
              this.sectionCourante()
            }
          }
        }
      } else {
        // pas de réponse
        stor.lesdiv.correction.style.color = this.styles.cfaux
        stor.lesdiv.correction.innerHTML = reponseIncomplete
        this.afficheBoutonValider()
      }
      if (stor.yaexplik) stor.lesdiv.explications.classList.add('explique')
      if (stor.yaco) stor.lesdiv.solution.classList.add('correction')
      // Obligatoire
      this.finCorrection()
      break // case "correction":

    case 'navigation':
      if (!this.sectionTerminee()) {
        this.etat = 'enonce'
      }
      this.finNavigation()
      break // case "navigation":
  }
}
