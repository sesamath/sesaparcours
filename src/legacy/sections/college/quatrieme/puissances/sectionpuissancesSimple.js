import { j3pAddElt, j3pAjouteBouton, j3pArrondi, j3pDetruit, j3pEmpty, j3pGetRandomBool, j3pGetRandomInt, j3pShowError, j3pShuffle } from 'src/legacy/core/functions'
import { j3pAffiche } from 'src/lib/mathquill/functions'
import { addDefaultTable } from 'src/legacy/themes/table'
import { j3pCreeFenetres, j3pAfficheCroixFenetres, j3pToggleFenetres } from 'src/legacy/core/functionsJqDialog'

import ZoneStyleAffiche from 'src/legacy/outils/zoneStyleMathquill/zoneStyleAffiche'
import textesGeneriques from 'src/lib/core/textes'

const { cBien, cFaux, essaieEncore, reponseIncomplete, tempsDepasse } = textesGeneriques

/**
 * Ajouter une description ici
 * @fileOverview
 * @author Tommy Barroy <t.barroy@laposte.net>
 * @since 2017-10
*/
export const params = {
  outils: ['mathquill', 'calculatrice'],
  /**
   * Ce qui est paramétrable dans l’éditeur de graphe
   * @type {LegacySectionParamsParametres}
   */
  parametres: [
    ['nbrepetitions', 9, 'entier', 'Nombre de répétitions de la section'],
    ['nbchances', 2, 'entier', 'Nombre d’essais possibles.'],
    ['Formule1', true, 'boolean', '<u>true</u>: Formule a^n * a^p disponible.'],
    ['Formule2', true, 'boolean', '<u>true</u>: Formule a^n / a^p disponible.'],
    ['Formule3', true, 'boolean', '<u>true</u>: Formule (a^n)^p disponible.'],
    ['Fixea', 10, 'entier', 'Valeur de <i>a</i> mettre 0 pour aléatoire'],
    ['Positifs', true, 'boolean', '<u>true</u>: n et p sont positifs.'],
    ['NbOpe', 1, 'entier', 'entre 1 et 3'],
    ['Details', true, 'boolean', '<u>true</u>: Il faut donner les détails'],
    ['Calculatrice', true, 'boolean', '<u>true</u>:  Une calculatrice est disponible'],
    ['theme', 'standard', 'liste', 'Modèle pour l’affichage', ['standard', 'zonesAvecImageDeFond']]
  ]
}

/**
 * section puissancesSimple
 * @this {Parcours}
 */
export default function main () {
  const me = this
  const stor = this.storage
  const ds = this.donneesSection

  function creeLesDiv () {
    stor.lesdiv = {}
    stor.lesdiv.conteneur = j3pAddElt(me.zonesElts.MG, 'div', {
      style: me.styles.etendre('toutpetit.enonce', { padding: '10px' })
    })
    stor.lesdiv.consigneG = addDefaultTable(stor.lesdiv.conteneur, 1, 1)[0][0]
    stor.lesdiv.travail = addDefaultTable(stor.lesdiv.conteneur, 1, 1)[0][0]
    stor.lesdiv.explications = j3pAddElt(stor.lesdiv.conteneur, 'div')
    stor.lesdiv.solution = j3pAddElt(stor.lesdiv.conteneur, 'div')
    stor.lesdiv.calculatrice = j3pAddElt(me.zonesElts.MD, 'div', {
      style: me.styles.etendre('toutpetit.enonce', { padding: '10px' })
    })
    stor.lesdiv.correction = j3pAddElt(me.zonesElts.MD, 'div', {
      style: me.styles.etendre('toutpetit.enonce', { padding: '10px' })
    })
    stor.lesdiv.solution.style.color = me.styles.toutpetit.correction.color
    stor.lesdiv.explications.style.color = me.styles.cfaux
  }

  function initSection () {
    // Construction de la page
    me.construitStructurePage('presentation1bis')

    let lp = []
    if (ds.Formule1) lp.push('1')
    if (ds.Formule2) lp.push('2')
    if (ds.Formule3) lp.push('3')
    if (lp.length === 0) {
      j3pShowError('erreur de paramétrage')
      lp.push(1)
    }
    lp = j3pShuffle(lp)

    let tabuf
    stor.lp = []
    for (let i = 0; i < lp.length; i++) { stor.lp[i] = lp[i] }
    for (let i = 1; i < ds.NbOpe; i++) {
      tabuf = []
      for (let j = 0; j < stor.lp.length; j++) {
        tabuf = tabuf.concat(colle(stor.lp[j], lp))
      }
      stor.lp = []
      for (let j = 0; j < tabuf.length; j++) { stor.lp[j] = tabuf[j] }
    }

    stor.restric = '0123456789'
    if (!ds.Positifs) stor.restric += '()-'

    stor.lp = j3pShuffle(stor.lp)

    me.donneesSection.nbitems = me.donneesSection.nbetapes * me.donneesSection.nbrepetitions
    ds.lesExos = []
    for (let i = 0; i < me.donneesSection.nbitems; i++) {
      ds.lesExos.push({ formule: stor.lp[i % stor.lp.length] })
    }

    me.typederreurs = [0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0]

    me.score = 0
    let tt = 'Opérations sur les puissances'
    if (ds.Fixea !== 0) tt += ' de ' + ds.Fixea

    me.afficheTitre(tt)
    enonceMain()
  } // initSection

  function faisChoixExo () {
    stor.Lexo = ds.lesExos.pop()
    stor.nb = []
    stor.a = ds.Fixea
    if (ds.Fixea === 0) {
      stor.a = j3pGetRandomInt(2, 50)
      if (!ds.Positifs && j3pGetRandomBool()) stor.a = -stor.a
      if (stor.a < 0) stor.a = '(' + stor.a + ')'
    }
    stor.prems = true
  }

  function poseQuestion () {
    stor.compt = 0
    if (ds.Details) { stor.tot = stor.Lexo.formule.length * 2 } else { stor.tot = stor.Lexo.formule.length }
    j3pAffiche(stor.lesdiv.consigneG, null, '<b>Écris l’expression A sous la forme d’une puissance de $' + String(stor.a).replace('(', '').replace(')', '') + '$</b>')
    poseDetails()
    j3pAffiche(stor.lesdiv.consigneG, null, '\n')
  }

  function poseDetails () {
    stor.tabDet = addDefaultTable(stor.lesdiv.travail, ds.NbOpe * 3, 1)
    stor.doukonest = 0
    affPartiel()
  }

  function affPartiel () {
    stor.encours = 'part1'
    if (stor.prems) {
      j3pAffiche(stor.tabDet[stor.doukonest][0], null, '$A = ' + affichemonexp(true) + '$')
      stor.prems = false
    }
    if (ds.Details) {
      affichemonexp2(stor.tabDet[stor.doukonest + 1][0], 1)
    } else {
      posePart2()
    }
  }

  function posePart2 () {
    stor.encours = 'part2'
    if (ds.details) {
      j3pDetruit(stor.tabDet[stor.doukonest + 1][2])
      j3pDetruit(stor.tabDet[stor.doukonest + 1][1])
      stor.tabDet[stor.doukonest + 1][0].setAttribute('colspan', 3)
      j3pEmpty(stor.tabDet[stor.doukonest + 1][0])
      j3pAffiche(stor.tabDet[stor.doukonest + 1][0], null, '$A = ' + affichemonexp(false, true) + '$')
    }
    affichemonexp2(stor.tabDet[stor.doukonest + 2][0], 2)
  }

  function affichemonexp (first, detail) {
    let ok, aret, zu
    do {
      ok = true
      if (first) {
        stor.nb = []
        stor.lbo = []
        if (!ds.Positifs) {
          stor.nb[0] = j3pGetRandomInt(2, 9)
          if (j3pGetRandomBool()) {
            stor.nb[0] = -stor.nb[0]
          }
        } else {
          if (stor.Lexo.formule[0] === '2') {
            stor.nb[0] = j3pGetRandomInt(8, 12)
          } else {
            stor.nb[0] = j3pGetRandomInt(2, 10)
          }
        }
        zu = stor.nb[0]
      }

      aret = stor.a + '^{' + stor.nb[0] + '}'
      let dudeb = 0
      if (detail === true) {
        dudeb = 1
        aret = stor.a + '^{'
        switch (stor.Lexo.formule[0]) {
          case '1':
            if (stor.lbo[0]) {
              if (stor.nb[0] > -1) {
                aret += stor.nb[1] + ' + ' + stor.nb[0] + '}'
              } else {
                aret += stor.nb[1] + ' + (' + stor.nb[0] + ')}'
              }
            } else {
              if (stor.nb[1] > -1) {
                aret += stor.nb[0] + ' + ' + stor.nb[1] + '}'
              } else {
                aret += stor.nb[0] + ' + (' + stor.nb[1] + ')}'
              }
            }

            break
          case '2':
            if (stor.lbo[0]) {
              if (stor.nb[0] > -1) {
                aret += stor.nb[1] + ' - ' + stor.nb[0] + '}'
              } else {
                aret += stor.nb[1] + ' - (' + stor.nb[0] + ')}'
              }
            } else {
              if (stor.nb[1] > -1) {
                aret += stor.nb[0] + ' - ' + stor.nb[1] + '}'
              } else {
                aret += stor.nb[0] + ' - (' + stor.nb[1] + ')}'
              }
            }
            break
          case '3':
            if (stor.nb[1] > -1) {
              aret += stor.nb[0] + ' \\times ' + stor.nb[1] + '}'
            } else {
              aret += stor.nb[0] + ' \\times (' + stor.nb[1] + ')}'
            }
        }
      }
      for (let i = dudeb; i < stor.Lexo.formule.length; i++) {
        switch (stor.Lexo.formule[i]) {
          case '1':
            if (first) {
              stor.lbo.push(j3pGetRandomBool())
              if (!ds.Positifs) {
                stor.nb[i + 1] = j3pGetRandomInt(2, 9)
                if (j3pGetRandomBool()) {
                  stor.nb[i + 1] = -stor.nb[i + 1]
                }
              } else {
                stor.nb[i + 1] = j3pGetRandomInt(2, 9)
              }
              zu = stor.nb[i + 1] + zu
              if ((zu === 0) || (zu === 1) || (zu === 2)) {
                stor.nb[i + 1] += 2
                zu += 2
              }
            }
            if (stor.lbo[i]) {
              aret = stor.a + '^{' + stor.nb[i + 1] + '} \\times ' + aret
            } else {
              aret = aret + '\\times ' + stor.a + '^{' + stor.nb[i + 1] + '}'
            }

            break
          case '2':
            if (first) {
              stor.lbo.push(j3pGetRandomBool())
              if (stor.lbo[i]) {
                if (!ds.Positifs) {
                  stor.nb[i + 1] = j3pGetRandomInt(2, 9)
                  if (j3pGetRandomBool()) {
                    stor.nb[i + 1] = -stor.nb[i + 1]
                  }
                } else {
                  stor.nb[i + 1] = zu + j3pGetRandomInt(2, 9)
                }
                zu = stor.nb[i + 1] - zu
                if ((zu === 0) || (zu === 1) || (zu === 2)) {
                  stor.nb[i + 1] += 3
                  zu += 3
                }
              } else {
                if (!ds.Positifs) {
                  stor.nb[i + 1] = j3pGetRandomInt(2, 9)
                  if (j3pGetRandomBool()) {
                    stor.nb[i + 1] = -stor.nb[i + 1]
                  }
                } else {
                  stor.nb[i + 1] = j3pGetRandomInt(4, zu - 4)
                }
                zu = zu - stor.nb[i + 1]
                if ((zu === 0) || (zu === 1) || (zu === 2)) {
                  stor.nb[i + 1] -= 3
                  zu += 3
                }
              }
            }
            if (stor.lbo[i]) {
              aret = '\\frac{' + stor.a + '^{' + stor.nb[i + 1] + '}}{' + aret + '}'
            } else {
              aret = '\\frac{' + aret + '}{' + stor.a + '^{' + stor.nb[i + 1] + '}}'
            }
            break
          case '3':
            if (first) {
              stor.lbo.push(false)
              if (!ds.Positifs) {
                stor.nb[i + 1] = j3pGetRandomInt(2, 9)
                if (j3pGetRandomBool()) {
                  stor.nb[i + 1] = -stor.nb[i + 1]
                }
              } else {
                stor.nb[i + 1] = j3pGetRandomInt(2, 9)
              }
              zu = zu * stor.nb[i + 1]
            }
            aret = '\\left(' + aret + '\\right)^{' + stor.nb[i + 1] + '}'
        }
        if (ds.Positifs && first && zu < 0) ok = false
      }
    } while (!ok)
    return aret
  } // affichemonexp

  function affichemonexp2 (ou, tot) {
    let aret

    aret = ' µ ^{ ù } '
    let dudeb = 0
    if (tot === 2) dudeb = 1
    if (tot === 1) {
      dudeb = 1
      aret = ' ù ^{'
      switch (stor.Lexo.formule[0]) {
        case '1':
          if (stor.lbo[0]) {
            if (stor.nb[0] > -1) {
              aret += stor.nb[1] + ' µ ' + stor.nb[0] + '}'
            } else {
              aret += stor.nb[1] + ' µ (' + stor.nb[0] + ')}'
            }
          } else {
            if (stor.nb[1] > -1) {
              aret += stor.nb[0] + ' µ ' + stor.nb[1] + '}'
            } else {
              aret += stor.nb[0] + ' µ \\left(' + stor.nb[1] + '\\right)}'
            }
          }

          break
        case '2':
          if (stor.lbo[0]) {
            if (stor.nb[0] > -1) {
              aret += stor.nb[1] + ' µ ' + stor.nb[0] + '}'
            } else {
              aret += stor.nb[1] + ' µ \\left(' + stor.nb[0] + '\\right)}'
            }
          } else {
            if (stor.nb[1] > -1) {
              aret += stor.nb[0] + ' µ ' + stor.nb[1] + '}'
            } else {
              aret += stor.nb[0] + ' µ \\left(' + stor.nb[1] + '\\right)}'
            }
          }
          break
        case '3':
          if (stor.nb[1] > -1) {
            aret += stor.nb[0] + ' µ ' + stor.nb[1] + '}'
          } else {
            aret += stor.nb[0] + ' µ \\left(' + stor.nb[1] + '\\right)}'
          }
      }
    }
    for (let i = dudeb; i < stor.Lexo.formule.length; i++) {
      switch (stor.Lexo.formule[i]) {
        case '1':
          if (stor.lbo[i]) {
            aret = stor.a + '^{' + stor.nb[i + 1] + '} \\times ' + aret
          } else {
            aret = aret + '\\times ' + stor.a + '^{' + stor.nb[i + 1] + '}'
          }
          break
        case '2':
          if (stor.lbo[i]) {
            aret = '\\frac{' + stor.a + '^{' + stor.nb[i + 1] + '}}{' + aret + '}'
          } else {
            aret = '\\frac{' + aret + '}{' + stor.a + '^{' + stor.nb[i + 1] + '}}'
          }
          break
        case '3':
          aret = '\\left(' + aret + '\\right)^{' + stor.nb[i + 1] + '}'
      }
    }

    if (tot === 1) {
      const ll = ['...', '+', '−', '×', '÷']
      stor.zoneStyleAffiche = new ZoneStyleAffiche(ou, '$ A = ' + aret + '$', [
        { type: 'zsm3', symb: 'ù', nom: 'zonerepA', params: { restric: stor.restric, enter: me.sectionCourante.bind(me) } },
        { type: 'ld', symb: 'µ', nom: 'dliste', liste: ll, params: { sansFleche: true, centre: true } }
      ])
    }
    if (tot === 2) {
      stor.zoneStyleAffiche = new ZoneStyleAffiche(ou, '$ A = ' + aret + '$', [
        { type: 'zsm3', symb: 'µ', nom: 'zonerepA', params: { restric: stor.restric, enter: me.sectionCourante.bind(me) } },
        { type: 'zsm3', symb: 'ù', nom: 'zonerepA2', params: { restric: stor.restric, enter: me.sectionCourante.bind(me) } }
      ])
    }
  } // affichemonexp2

  /**
   * retourne le tableau quoi dont tous les éléments seront préfixés par qui
   * @private
   * @param {string} qui préfixe qui sera mis à chaque élément de quoi
   * @param {string[]} quoi tab avec les differents elements possibles a coller
   * @return {string[]}
   */
  function colle (qui, quoi) {
    return quoi.map(str => qui + str)
  }

  function isRepOk () {
    stor.errA = false
    stor.errForm = false
    stor.errCal = false
    stor.errAPar = false
    stor.errEcrit = false

    let ok = true
    let att

    if (stor.encours === 'part1') {
      ok = testEcrit(stor.zoneStyleAffiche.liste[0].reponse())
      if (!ok) {
        stor.errEcrit = true
        stor.zoneStyleAffiche.liste[0].corrige(false)
      }
      if (ok) {
        if (stor.zoneStyleAffiche.liste[0].reponse() !== String(stor.a)) {
          if ((String(stor.a).indexOf('(') !== -1) && (stor.zoneStyleAffiche.liste[0].reponse().indexOf('(') === -1)) {
            stor.errAPar = true
          } else {
            stor.errA = true
          }
          stor.zoneStyleAffiche.liste[0].corrige(false)
          ok = false
        }
      }

      if (stor.Lexo.formule[0] === '1') att = '+'
      if (stor.Lexo.formule[0] === '2') att = '−'
      if (stor.Lexo.formule[0] === '3') att = '×'
      if (att !== stor.zoneStyleAffiche.liste[1].reponse) {
        stor.zoneStyleAffiche.liste[1].corrige(false)
        ok = false
        stor.errForm = true
      }
      return ok
    }
    if (stor.encours === 'part2') {
      if (stor.zoneStyleAffiche.liste[0].reponse() !== String(stor.a)) {
        stor.zoneStyleAffiche.liste[0].corrige(false)
        stor.errA = true
        ok = false
      }
      if (stor.Lexo.formule[0] === '1') stor.att = stor.nb[0] + stor.nb[1]
      if (stor.Lexo.formule[0] === '2') {
        if (!stor.lbo[0]) {
          stor.att = stor.nb[0] - stor.nb[1]
        } else {
          stor.att = stor.nb[1] - stor.nb[0]
        }
      }
      if (stor.Lexo.formule[0] === '3') stor.att = stor.nb[0] * stor.nb[1]
      if (String(stor.att) !== stor.zoneStyleAffiche.liste[1].reponse()) {
        stor.zoneStyleAffiche.liste[1].corrige(false)
        ok = false
        stor.errCal = true
      }
      return ok
    }
  } // isRepOk

  function testEcrit (q) {
    if (q.indexOf('(') !== q.lastIndexOf('(')) return false // plusieurs parenthèses ouvrantes
    if (q.indexOf(')') !== q.lastIndexOf(')')) return false // plusieurs parenthèses fermantes
    if ((q.indexOf(')') === -1) !== (q.indexOf('(') === -1)) return false // pas le même nb (0 ou 1) d’ouvrantes et de fermantes
    if ((q.indexOf('(') !== -1) && (q.indexOf('(') !== 0)) return false // une parenthèse ouvrante pas au début
    if ((q.indexOf(')') !== -1) && (q.indexOf(')') !== q.length - 1)) return false // une parenthèse fermante pas à la fin
    // on retourne false si y’a un - précédé de caractères autres que des parenthèses, true sinon
    return !/^[^()]+-/.test(q)
  }

  function affCorrFaux (isFin) {
  /// //affiche indic

    if (stor.errA) {
      j3pAffiche(stor.lesdiv.explications, null, 'On demande une puissance de $' + String(stor.a).replace('(', '').replace(')', '') + '$ !\n')
      stor.yaexplik = true
    }
    if (stor.errForm) {
      j3pAffiche(stor.lesdiv.explications, null, 'Il y a une erreur de formule !\n')
      stor.yaexplik = true
    }
    if (stor.errCal) {
      j3pAffiche(stor.lesdiv.explications, null, 'Erreur de calcul à l’exposant !\n')
      stor.yaexplik = true
    }
    if (stor.errEcrit) {
      j3pAffiche(stor.lesdiv.explications, null, 'Un nommbre est mal écrit !\n')
      stor.yaexplik = true
    }
    if (stor.errAPar) {
      j3pAffiche(stor.lesdiv.explications, null, 'Il faut garder les parenthèses !\n')
      stor.yaexplik = true
    }

    if (isFin) {
      stor.zoneStyleAffiche.barreLesFaux()
      stor.zoneStyleAffiche.disable()
      stor.yaco = true
      if (stor.encours === 'part1') {
        j3pAffiche(stor.lesdiv.solution, null, '$A = ' + affichemonexp(false, true) + '$')
      }
      if (stor.encours === 'part2') {
        stor.Lexo.formule = stor.Lexo.formule.substring(1)
        stor.nb[1] = stor.att
        stor.nb.splice(0, 1)
        stor.lbo.splice(0, 1)
        j3pAffiche(stor.lesdiv.solution, null, '$A = ' + affichemonexp(false) + '$')
      }

      stor.compt = stor.compt / stor.tot
      me.score = j3pArrondi(me.score + stor.compt, 2)
    }
  } // affCorrFaux

  function enonceMain () {
    if (me.etapeCourante === 1) {
      me.videLesZones()
    }
    creeLesDiv()
    stor.lesdiv.travail.classList.add('travail')
    stor.lesdiv.consigneG.classList.add('enonce')
    me.zonesElts.MG.classList.add('fond')
    faisChoixExo()
    poseQuestion()
    if (ds.Calculatrice) {
      me.fenetresjq = [{ name: 'Calculatrice', title: 'Une calculatrice', left: 600, top: 10 }]
      j3pCreeFenetres(me)
      j3pAfficheCroixFenetres('Calculatrice')
      j3pAjouteBouton(stor.lesdiv.calculatrice, () => j3pToggleFenetres('Calculatrice'), { id: 'Calculatrice', className: 'MepBoutons', value: 'Calculatrice' })
    }
    me.finEnonce()
  }

  switch (this.etat) {
    case 'enonce':
      // code exécuté au lancement de la section
      if (me.debutDeLaSection) {
        initSection()
      } else {
        enonceMain()
      }
      break // case "enonce":

    case 'correction':
      // On teste si une réponse a été saisie
      stor.yaexplik = false
      stor.yaco = false
      stor.lesdiv.explications.innerHTML = ''
      stor.lesdiv.solution.innerHTML = ''

      if (stor.zoneStyleAffiche.hasResponse()) {
        // Bonne réponse
        if (isRepOk()) {
          this._stopTimer()
          stor.lesdiv.correction.style.color = this.styles.cbien
          stor.lesdiv.correction.innerHTML = cBien

          stor.zoneStyleAffiche.corrige()
          stor.zoneStyleAffiche.disable()
          if (stor.encours === 'part1') {
            stor.compt++
            posePart2()
            return
          }
          if (stor.encours === 'part2') {
            stor.compt++
            if (stor.Lexo.formule.length !== 1) {
              stor.Lexo.formule = stor.Lexo.formule.substring(1)
              stor.nb[1] = stor.att
              stor.nb.splice(0, 1)
              stor.lbo.splice(0, 1)
              stor.doukonest += 2
              affPartiel()
              return
            }
          }

          this.score++
          this.typederreurs[0]++
          return this.finCorrection('navigation', true)
        }
        // Pas de bonne réponse
        stor.lesdiv.correction.style.color = this.styles.cfaux
        stor.lesdiv.correction.innerHTML = cFaux
        if (this.isElapsed) {
          // A cause de la limite de temps :
          stor.lesdiv.correction.innerHTML = tempsDepasse + '<BR>'
          this.typederreurs[10]++
          affCorrFaux(true)
          stor.yaco = true
          return this.finCorrection('navigation', true)
        }
        // Réponse fausse :
        if (me.essaiCourant < me.donneesSection.nbchances) {
          stor.lesdiv.correction.innerHTML += '<br>' + essaieEncore
          affCorrFaux(false)
          this.typederreurs[1]++
          return this.finCorrection() // on reste dans l’état correction
        }
        // Erreur au nème essai
        affCorrFaux(true)
        stor.yaco = true
        this.typederreurs[2]++
        return this.finCorrection('navigation', true)
      }

      // pas de réponse
      stor.lesdiv.correction.style.color = this.styles.cfaux
      stor.lesdiv.correction.innerHTML = reponseIncomplete
      if (stor.yaexplik) stor.lesdiv.explications.classList.add('explique')
      if (stor.yaco) stor.lesdiv.solution.classList.add('correction')
      this.finCorrection()
      break // case "correction":

    case 'navigation':
      if (!this.sectionTerminee()) {
        this.etat = 'enonce'
      }
      this.finNavigation()
      break // case "navigation":
  }
}
