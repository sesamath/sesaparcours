import { j3pAddElt, j3pAjouteBouton, j3pArrondi, j3pBarre, j3pGetNewId, j3pGetRandomBool, j3pGetRandomInt, j3pShowError, j3pShuffle } from 'src/legacy/core/functions'
import BoutonStyleMathquill from 'src/legacy/outils/boutonsStyleMatquill'
import Paint from 'src/legacy/outils/brouillon/Paint'
import ListeDeroulante from 'src/legacy/outils/listeDeroulante/ListeDeroulante'
import { j3pAffiche } from 'src/lib/mathquill/functions'
import { addDefaultTable } from 'src/legacy/themes/table'
import ZoneStyleMathquill1 from 'src/legacy/outils/zoneStyleMathquill/ZoneStyleMathquill1'
import { j3pCreeFenetres, j3pAfficheCroixFenetres, j3pToggleFenetres } from 'src/legacy/core/functionsJqDialog'
import { affichefois, afficheFrac } from 'src/legacy/outils/zoneStyleMathquill/functions'
import { j3pCreeSVG } from 'src/legacy/core/functionsSvg'
import { getMtgCore } from 'src/lib/outils/mathgraph'
import { addMaj } from 'src/lib/utils/string'
import textesGeneriques from 'src/lib/core/textes'

const { cBien, cFaux, essaieEncore, reponseIncomplete } = textesGeneriques

/**
 * Ajouter une description ici
 * @fileOverview
 * @author Tommy Barroy <t.barroy@laposte.net>
 * @since 2017-10
 */

export const params = {
  outils: ['mathquill', 'calculatrice'],
  /**
   * Ce qui est paramétrable dans l’éditeur de graphe
   * @type {LegacySectionParamsParametres}
   */
  parametres: [
    ['nbrepetitions', 9, 'entier', 'Nombre de répétitions de la section'],
    ['nbchances', 2, 'entier', 'Nombre d’essais possibles.'],
    ['Type_Enonce', 'Les deux', 'liste', '<u>Figure</u>:  Les données se trouvent sur un schéma.  <br> <br> <u>Texte</u>: Les données sont dans un texte. ', ['Les deux', 'Figure', 'Texte']],
    // ['Raisonnement', true, 'boolean', '<u>true</u>:  Le raisonnement est attendu. '],
    ['Description', true, 'boolean', '<u>true</u>: L’élève doit compléter la description.'],
    ['Calcul', 'Les deux', 'liste', '<u>Formule</u>: L’élève doit juste exprimer une formule. </u><u>Longueur</u>: Il faut calculer une longueur. <br> <br> <u>Angle</u>: Il faut calculer un angle ', ['Formule', 'Longueur', 'Angle', 'Les deux']],
    ['cosinus', true, 'boolean', '<u>true</u>: cosinus possible.'],
    ['sinus', true, 'boolean', '<u>true</u>: sinus possible.'],
    ['tangente', true, 'boolean', '<u>true</u>: tangente possible.'],
    ['Donnees_Inutiles', 'oui', 'liste', '<u>oui</u>: L’énoncé comporte des données inutiles. <br><br> <u>non</u>: non', ['oui', 'non', 'Les deux']],
    ['Eclaire', true, 'boolean', '<i>Uniquement quand on calcule un angle, ou quand on exprime une formule, avec un énoné en schéma</i> <br> <br> <u>true</u>: L’angle est mis en valeur .'],
    ['Brouillon', false, 'boolean', '<u>true</u>: L’élève dispose d’un brouillon quand l’énoncé est un texte'],
    // le modèle gère la compatibilité ascendante avec l’ancien paramètre MiseEnPagePlusClaire
    ['theme', 'standard', 'liste', 'Modèle pour l’affichage', ['standard', 'zonesAvecImageDeFond']]
  ]
}

/**
 * section trigo01
 * @this {Parcours}
 */
export default function main () {
  const me = this
  const stor = me.storage
  const ds = me.donneesSection
  let svgId = stor.svgId

  function boutonfen () {
    j3pToggleFenetres('Calculatrice')
  }
  function creeLesDiv () {
    stor.lesdiv = {}
    stor.lesdiv.conteneur = j3pAddElt(me.zones.MG, 'div', {
      style: me.styles.etendre('toutpetit.enonce', { padding: '10px' })
    })
    const ttab = addDefaultTable(stor.lesdiv.conteneur, 4, 1)
    modif(ttab)
    stor.lesdiv.consigne = ttab[0][0]
    stor.lesdiv.brouillon = ttab[2][0]
    const ttab2 = addDefaultTable(ttab[1][0], 1, 5)
    modif(ttab2)
    stor.lesdiv.figure = ttab2[0][0]
    stor.lesdiv.travail = ttab2[0][2]
    stor.lesdiv.solution = ttab2[0][4]
    ttab2[0][1].style.width = '4px'
    ttab2[0][3].style.width = '4px'
    const ttab3 = addDefaultTable(ttab[3][0], 1, 3)
    modif(ttab3)
    stor.lesdiv.correction = ttab3[0][0]
    stor.lesdiv.explications = ttab3[0][2]
    ttab3[0][1].style.width = '10px'
    me.zonesElts.MG.classList.add('fond')
  }

  function poseQuestion () {
    let bib
    let bob
    if (ds.Calcul === 'Formule') {
      j3pAffiche(stor.lesdiv.consigne, null, '<b>Complète la formule trigonométrique suivante </b>')
    } else {
      switch (stor.Lexo.arrond) {
        // pour réclamer l’arrondi le plus proche, le préciser ici et aller modifier isApeupEgal() (cf commentaires dans cette fct)
        case 1: bob = ' (arrondi au dixième le plus proche).'
          break
        case 2: bob = ' (arrondi au centième le plus proche).'
          break
        case 3: bob = ' (arrondi au millième le plus proche).'
          break
      }
      if (stor.ch.length > 2) { bib = ' l’angle $\\widehat{' + stor.ch + '}$ ' } else { bib = 'la longueur ' + stor.ch }
      j3pAffiche(stor.lesdiv.consigne, null, '<b>Complète la démonstration ci-dessous </b> pour calculer ' + bib + bob)
    }
    AfficheMaphrase()
  }

  function RemakeForm2 () {
    let i
    let lform
    const rep = stor.zoneForm.reponse
    if (rep.indexOf('cos') !== -1) { lform = 'cos' }
    if (rep.indexOf('sin') !== -1) { lform = 'sin' }
    if (rep.indexOf('tan') !== -1) { lform = 'tan' }
    let lc = []
    for (i = 0; i < stor.listmes.length; i++) {
      lc.push('$' + lform + ' (' + String(stor.listmes[i]).replace('.', ',') + ')$')
    }
    lc = j3pShuffle(lc)
    lc.splice(0, 0, '--Choisir--')
    stor.T3[1][0].innerHTML = ''
    stor.zoneForm2 = ListeDeroulante.create(stor.T3[1][0], lc, { center: true })
  }
  function AfficheMaphrase () {
    // opts.id = 'GRO'
    stor.T1 = addDefaultTable(stor.lesdiv.travail, 3, 1)
    modif(stor.T1)
    if (ds.Description) {
      // opts.id = 'DESC'
      stor.T2 = addDefaultTable(stor.T1[0][0], 1, 5)
      modif(stor.T2)
      j3pAffiche(stor.T2[0][0], null, '&nbsp;Dans le triangle &nbsp;')
      j3pAffiche(stor.T2[0][2], null, '&nbsp; rectangle en &nbsp;')
      j3pAffiche(stor.T2[0][4], null, '&nbsp; ,&nbsp;')
      stor.zoneDesc = new ZoneStyleMathquill1(stor.T2[0][1], {
        restric: 'ABCDEFGHIJKLMNOPQRSTUVWXYZabcdefghijklmnopqrstuvwxyz()[],',
        limitenb: 9,
        limite: 3

      })
      stor.zoneDesc2 = new ZoneStyleMathquill1(stor.T2[0][3], {
        restric: 'ABCDEFGHIJKLMNOPQRSTUVWXYZabcdefghijklmnopqrstuvwxyz()[],',
        limitenb: 9,
        limite: 1

      })
    } else {
      j3pAffiche(stor.T1[0][0], null, '&nbsp;Dans le triangle ' + stor.nomtri + ' rectangle en ' + stor.nomsom + ',&nbsp;')
    }
    // opts.id = 'FORM'
    stor.T3 = addDefaultTable(stor.lesdiv.travail, 2, 4)
    modif(stor.T3)
    if (ds.Calcul === 'Formule') {
      stor.T4 = addDefaultTable(stor.T3[0][0], 1, 3)
      modif(stor.T4)
      stor.T4[0][0].style.paddingBottom = '10px'
      stor.T4[0][1].style.paddingBottom = '10px'
      stor.T4[0][2].style.paddingBottom = '10px'
      j3pAffiche(stor.T4[0][0], null, '&nbsp;' + stor.Lexo.formule.substring(0, 3) + '&nbsp;(&nbsp;')
      j3pAffiche(stor.T4[0][1], null, '$\\widehat{' + stor.A + '}$')
      stor.T4[0][0].style.verticalAlign = 'bottom'
      stor.T4[0][2].style.verticalAlign = 'bottom'
      j3pAffiche(stor.T4[0][2], null, '&nbsp;)&nbsp;')
    } else {
      const lc = ['--Choisir--']
      if (ds.sinus) {
        lc.push(' $sin (\\widehat{' + stor.A + '})$ ')
        lc.push(' $sin (\\widehat{' + stor.A2 + '})$ ')
      }
      if (ds.cosinus) {
        lc.push(' $cos (\\widehat{' + stor.A + '})$ ')
        lc.push(' $cos (\\widehat{' + stor.A2 + '})$ ')
      }
      if (ds.tangente) {
        lc.push(' $tan (\\widehat{' + stor.A + '})$ ')
        lc.push(' $tan (\\widehat{' + stor.A2 + '})$ ')
      }
      stor.zoneForm = ListeDeroulante.create(stor.T3[0][0], lc, { center: true, onChange: metLa })
    }
    j3pAffiche(stor.T3[0][1], null, ' $ = $ ')
    stor.divForm = afficheFrac(stor.T3[0][2], false)
    stor.zoneLnum = ListeDeroulante.create(stor.divForm[0], stor.long, { centre: true })
    stor.zoneLden = ListeDeroulante.create(stor.divForm[2], stor.long, { centre: true })
  }
  function metLa () {
    if (stor.zoneForm.reponse.indexOf(stor.A) !== -1) {
      stor.la = stor.A
    } else {
      stor.la = stor.A2
    }
  }
  function affichephrase2 () {
    let lc
    if (stor.etat === 'FORM2') {
      if (ds.Description) {
        stor.zoneDesc.corrige(true)
        stor.zoneDesc2.corrige(true)
        stor.zoneDesc.disable()
        stor.zoneDesc2.disable()
      }
      stor.zoneForm.corrige(true)
      stor.zoneForm.disable()
      stor.zoneLnum.corrige(true)
      stor.zoneLnum.disable()
      stor.zoneLden.corrige(true)
      stor.zoneLden.disable()
      stor.lesdiv.brouillon.style.display = 'none'
      j3pAffiche(stor.T3[1][1], null, ' $ = $ ')
      stor.divForm2 = afficheFrac(stor.T3[1][2], false)
      stor.T3[1][0].style.textAlign = 'center'
      if (stor.Lexo.calcul !== 'Angle') {
        lc = []
        for (let i = 0; i < stor.listmes.length; i++) {
          lc.push(String(stor.listmes[i]).replace('.', ','))
        }
        lc.push(stor.ch)
        lc = j3pShuffle(lc)
        lc.splice(0, 0, '---')
        stor.zoneLnum2 = ListeDeroulante.create(stor.divForm2[0], lc, { centre: true })
        stor.zoneLden2 = ListeDeroulante.create(stor.divForm2[2], lc, { centre: true })
        RemakeForm2()
      } else {
        j3pAffiche(stor.T3[1][0], null, stor.Lexo.formule.substring(0, 3) + ' $(\\widehat{' + stor.ch + '})$')
        lc = []
        for (let i = 0; i < stor.listmes.length; i++) {
          lc.push(String(stor.listmes[i]).replace('.', ','))
        }
        lc = j3pShuffle(lc)
        lc.splice(0, 0, '---')
        stor.zoneLnum2 = ListeDeroulante.create(stor.divForm2[0], lc, { centre: true })
        stor.zoneLden2 = ListeDeroulante.create(stor.divForm2[2], lc, { centre: true })
      }
    } else if (stor.etat === 'CALCUL1') {
      if (stor.Lexo.calcul !== 'Angle') {
        stor.zoneForm2.corrige(true)
        stor.zoneForm2.disable()
      }
      stor.zoneLnum2.corrige(true)
      stor.zoneLnum2.disable()
      stor.zoneLden2.corrige(true)
      stor.zoneLden2.disable()
      // opts.id = 'CONCL'
      stor.T5 = addDefaultTable(stor.lesdiv.travail, 2, 6)
      modif(stor.T5)
      j3pAffiche(stor.T5[0][0], null, ' Donc&nbsp;')
      if (stor.Lexo.calcul === 'Angle') {
        j3pAffiche(stor.T5[0][1], null, '$\\widehat{' + stor.ch + '}$')
        stor.T5[0][1].style.padding = 0
        lc = ['$' + stor.Lexo.formule.substring(0, 3) + '$', '$arc' + stor.Lexo.formule.substring(0, 3) + '$']
        lc = j3pShuffle(lc)
        lc.splice(0, 0, '---')
        stor.T6 = addDefaultTable(stor.T5[0][2], 1, 3)
        modif(stor.T6)
        j3pAffiche(stor.T6[0][0], null, ' $ = $ ')
        stor.zoneC1 = ListeDeroulante.create(stor.T6[0][1], lc, { centre: true })
        j3pAffiche(stor.T6[0][2], null, ' $ (\\frac{' + String(stor.lL1).replace('.', ',') + '}{ ' + String(stor.lL2).replace('.', ',') + ' }) $ ')
      } else {
        stor.T5[0][3].style.textAlign = 'center'
        j3pAffiche(stor.T5[0][1], null, ' $ ' + stor.ch + ' $ ')
        // opts.id = 'CONCLZ'
        stor.T7 = addDefaultTable(stor.T5[0][2], 1, 3)
        modif(stor.T7)
        stor.T7[0][0].style.padding = 0
        stor.T7[0][1].style.padding = 0
        stor.T7[0][2].style.padding = 0
        j3pAffiche(stor.T7[0][0], null, ' $ = $ ')
        j3pAffiche(stor.T7[0][1], null, ' <b>???</b> ')
        stor.zoneC1 = undefined
        stor.bboo = new BoutonStyleMathquill(stor.T5[0][3], 'bouboutCONCL', ['$\\times$', '÷'], [fmult, fdiv], { mathquill: true })
      }
    } else if (stor.etat === 'CONCL') {
      stor.zoneC1.corrige(true)
      stor.zoneC1.disable()
      if (stor.Lexo.calcul !== 'Angle') {
        stor.zoneC2.corrige(true)
        stor.zoneC2.disable()
      }
      j3pAjouteBouton(stor.lesdiv.solution, boutonfen, { className: 'MepBoutons', value: 'Calculatrice' })
      if (stor.Lexo.calcul !== 'Angle') {
        stor.T5[0][3].innerHTML = ''
        j3pAffiche(stor.T5[1][1], null, ' $ ' + stor.ch + ' $ &nbsp;')
      } else {
        j3pAffiche(stor.T5[1][1], null, '$\\widehat{' + stor.ch + '}$&nbsp;')
      }
      // opts.id = 'CONCLFIN'
      stor.T8 = addDefaultTable(stor.T5[1][2], 1, 3)
      modif(stor.T8)
      stor.T8[0][0].style.paddingLeft = '5px'
      stor.T8[0][1].style.paddingLeft = '5px'
      stor.T8[0][2].style.paddingLeft = '5px'
      stor.zoneEgale = ListeDeroulante.create(stor.T8[0][0], ['---', '$=$', '$\\approx$'], { centre: true })
      stor.zoneRep = new ZoneStyleMathquill1(stor.T8[0][1], { restric: '0123456789.,', limitenb: 1, limite: 8 })
      stor.zoneunite = ListeDeroulante.create(stor.T8[0][2], ['---', '°', 'cm', 'km'], { centre: true })
    } else {
      console.error(Error(`etat non géré : ${stor.etat}`))
    }
  }

  function gereEtat (bool) {
    if (!bool) {
      stor.leschances--
      if (stor.leschances === 0) {
        stor.Finish = true
        stor.LarepInt = false
        me.sectionCourante()
      } else {
        affCorrFaux()
      }
    } else {
      switch (stor.etat) {
        case 'FORM': stor.etat = 'FORM2'
          if (ds.Calcul === 'Formule') {
            stor.Finish = true
            stor.LarepInt = true
            stor.zoneLnum.corrige(true)
            stor.zoneLnum.disable()
            stor.zoneLden.corrige(true)
            stor.zoneLden.disable()
            me.sectionCourante()
            return
          }
          break
        case 'FORM2': stor.etat = 'CALCUL1'
          break
        case 'CALCUL1': stor.etat = 'CONCL'
          break
        case 'CONCL': stor.Finish = true
          stor.LarepInt = true
          stor.zoneEgale.corrige(true)
          stor.zoneEgale.disable()
          stor.zoneRep.corrige(true)
          stor.zoneRep.disable()
          stor.zoneunite.corrige(true)
          stor.zoneunite.disable()
          me.sectionCourante()
          return
      }
      affichephrase2()
    }
  }
  function fmult () {
    stor.T7[0][1].innerHTML = ''
    const t = affichefois(stor.T7[0][1], false)
    t[0].style.padding = 0
    t[1].style.padding = 0
    t[2].style.padding = 0
    makeL(t[0], t[2])
    stor.LOP = 'x'
    stor.T7[0][1].style.color = ''
  }
  function fdiv () {
    stor.T7[0][1].innerHTML = ''
    const t = afficheFrac(stor.T7[0][1], false)
    makeL(t[0], t[2])
    stor.LOP = '/'
    stor.T7[0][1].style.color = ''
  }
  function makeL (a, b) {
    let c1
    let c2
    if (stor.zoneC1 !== undefined) {
      c1 = stor.zoneC1.reponse
      c2 = stor.zoneC2.reponse
    } else {
      c1 = '---'
      c2 = '---'
    }

    const lc = ['---']
    lc.push(stor.zoneForm2.reponse)
    lc.push(stor.zoneLnum2.reponse)
    lc.push(stor.zoneLden2.reponse)
    if (lc.indexOf(c1) === -1) { c1 = '---' }
    if (lc.indexOf(c2) === -1) { c2 = '---' }
    if (a) a.innerHTML = ''
    if (b) b.innerHTML = ''
    stor.zoneC1 = ListeDeroulante.create(a, lc, { centre: true, choix: c1 })
    stor.zoneC2 = ListeDeroulante.create(b, lc, { centre: true, choix: c2 })
  }

  function faisChoixExos () {
    stor.zoneC1 = undefined
    stor.zoneC2 = undefined
    stor.zoneForm = undefined
    stor.zoneLnum = undefined
    stor.zoneLden = undefined
    stor.zoneForm2 = undefined
    stor.zoneLnum2 = undefined
    stor.zoneLden2 = undefined
    stor.zoneEgale = undefined
    stor.zoneRep = undefined
    stor.zoneunite = undefined
    stor.zoneDesc = undefined
    stor.zoneDesc2 = undefined
    return ds.lesExos.pop()
  }
  function faisLaFigure () {
    stor.lettres = []
    for (let i = 0; i < 4; i++) {
      addMaj(stor.lettres)
    }
    stor.listmes = []
    const lettres = stor.lettres
    /// determine longueurs et angles
    let va0 = false
    let va1 = false
    let va4 = false
    let va5 = false
    let vl1 = false
    let vl2 = false
    let vl3 = false
    let vl4 = false
    let vl5 = false

    const a0 = j3pGetRandomInt(15, 75)
    const a4 = j3pGetRandomInt(15, 75)

    stor.l1 = j3pArrondi(j3pGetRandomInt(35, 70) / 10, 1)
    stor.l3 = j3pArrondi(stor.l1 / Math.cos(a0 * Math.PI / 180), 1)
    stor.l2 = j3pArrondi(Math.sqrt(stor.l3 * stor.l3 - stor.l1 * stor.l1), 1)
    stor.l4 = j3pArrondi(stor.l3 * Math.cos(a4 * Math.PI / 180), 1)
    stor.l5 = j3pArrondi(Math.sqrt(stor.l3 * stor.l3 - stor.l4 * stor.l4), 1)
    stor.a4 = a4
    stor.a5 = 90 - stor.a4
    stor.a0 = a0
    stor.a1 = 90 - stor.a0

    stor.nomtri = lettres[0] + lettres[1] + lettres[2]
    stor.nomtri2 = lettres[0] + lettres[3] + lettres[1]
    stor.nomsom = lettres[2]
    stor.long = [lettres[0] + lettres[1], lettres[1] + lettres[2], lettres[2] + lettres[0]]

    if (stor.Lexo.inut) {
      stor.long = [lettres[0] + lettres[1], lettres[1] + lettres[3], lettres[3] + lettres[0], lettres[1] + lettres[2], lettres[2] + lettres[0]]
    }
    stor.long = j3pShuffle(stor.long)
    stor.long.splice(0, 0, '---')

    const haz = j3pGetRandomBool()
    switch (stor.Lexo.formule) {
      case 'sinus':
        stor.FOFORMULE = Math.sin
        stor.ArcFOFORMULE = Math.asin
        stor.Fo = lettres[0] + lettres[3]
        stor.lFo = stor.l4
        if (haz) {
          stor.A = lettres[1] + lettres[0] + lettres[2]
          stor.A2 = lettres[0] + lettres[1] + lettres[2]
          stor.L1 = lettres[1] + lettres[2]
          stor.L2 = lettres[0] + lettres[1]
          stor.lL1 = stor.l2
          stor.lL2 = stor.l3
          stor.Ma = stor.a0
        } else {
          stor.A = lettres[0] + lettres[1] + lettres[2]
          stor.A2 = lettres[1] + lettres[0] + lettres[2]
          stor.L1 = lettres[0] + lettres[2]
          stor.L2 = lettres[0] + lettres[1]
          stor.lL1 = stor.l1
          stor.lL2 = stor.l3
          stor.Ma = stor.a1
        }
        break
      case 'cosinus':
        stor.FOFORMULE = Math.cos
        stor.ArcFOFORMULE = Math.acos
        stor.Fo = lettres[0] + lettres[3]
        stor.lFo = stor.l4
        if (haz) {
          stor.A = lettres[0] + lettres[1] + lettres[2]
          stor.A2 = lettres[1] + lettres[0] + lettres[2]
          stor.L1 = lettres[1] + lettres[2]
          stor.L2 = lettres[0] + lettres[1]
          stor.lL1 = stor.l2
          stor.lL2 = stor.l3
          stor.Ma = stor.a1
        } else {
          stor.A = lettres[1] + lettres[0] + lettres[2]
          stor.A2 = lettres[0] + lettres[1] + lettres[2]
          stor.L1 = lettres[0] + lettres[2]
          stor.L2 = lettres[0] + lettres[1]
          stor.lL1 = stor.l1
          stor.lL2 = stor.l3
          stor.Ma = stor.a0
        }
        break
      case 'tangente':
        stor.FOFORMULE = Math.tan
        stor.ArcFOFORMULE = Math.atan
        stor.Fo = lettres[0] + lettres[3]
        stor.lFo = stor.l4
        if (haz) {
          stor.A = lettres[0] + lettres[1] + lettres[2]
          stor.A2 = lettres[1] + lettres[0] + lettres[2]
          stor.L1 = lettres[0] + lettres[2]
          stor.L2 = lettres[2] + lettres[1]
          stor.lL1 = stor.l1
          stor.lL2 = stor.l2
          stor.Ma = stor.a1
        } else {
          stor.A = lettres[1] + lettres[0] + lettres[2]
          stor.A2 = lettres[0] + lettres[1] + lettres[2]
          stor.L1 = lettres[1] + lettres[2]
          stor.L2 = lettres[0] + lettres[2]
          stor.lL1 = stor.l2
          stor.lL2 = stor.l1
          stor.Ma = stor.a0
        }
        break
    }

    const amontre = []
    switch (stor.Lexo.calcul) {
      case 'Longueur1':
        stor.ch = stor.L1
        amontre.push(stor.L2)
        amontre.push(stor.A)
        amontre.push(stor.Fo)
        stor.dl = stor.L2
        stor.dA = stor.A
        stor.dF = stor.Fo
        break
      case 'Longueur2':
        stor.ch = stor.L2
        amontre.push(stor.L1)
        amontre.push(stor.A)
        amontre.push(stor.Fo)
        stor.dl = stor.L1
        stor.dA = stor.A
        stor.dF = stor.Fo
        break
      case 'Angle':
        stor.ch = stor.A
        amontre.push(stor.L1)
        amontre.push(stor.L2)
        amontre.push(stor.Fo)
        stor.dl = stor.L2
        stor.dA = stor.L1
        stor.dF = stor.Fo
        break
    }
    if (ds.Eclaire && amontre.indexOf(stor.A) === -1) amontre.push(stor.A)

    for (let i = 0; i < amontre.length; i++) {
      switch (amontre[i]) {
        case lettres[0] + lettres[1]:
        case lettres[1] + lettres[0]: vl3 = true
          stor.listmes.push(stor.l3)
          break
        case lettres[0] + lettres[2]:
        case lettres[2] + lettres[0]: vl1 = true
          stor.listmes.push(stor.l1)
          break
        case lettres[1] + lettres[2]:
        case lettres[2] + lettres[1]: vl2 = true
          stor.listmes.push(stor.l2)
          break
        case lettres[0] + lettres[3]:
        case lettres[3] + lettres[0]: vl4 = true
          stor.listmes.push(stor.l4)
          break
        case lettres[1] + lettres[3]:
        case lettres[3] + lettres[1]: vl5 = true
          stor.listmes.push(stor.l5)
          break
        case lettres[1] + lettres[0] + lettres[2]:
        case lettres[2] + lettres[0] + lettres[1]: va0 = true
          stor.listmes.push(stor.a0)
          break
        case lettres[0] + lettres[1] + lettres[2]:
        case lettres[2] + lettres[1] + lettres[0]: va1 = true
          stor.listmes.push(stor.a1)
          break
        case lettres[1] + lettres[0] + lettres[3]:
        case lettres[3] + lettres[0] + lettres[1]: va4 = true
          stor.listmes.push(stor.a4)
          break
        case lettres[0] + lettres[1] + lettres[3]:
        case lettres[3] + lettres[1] + lettres[0]: va5 = true
          stor.listmes.push(stor.a5)
          break
      }
    }
    // rajoute test determine qui visible

    if (stor.Lexo.type === 'Figure') {
      stor.svgId = j3pGetNewId('mtg32')
      svgId = stor.svgId
      const hh = addDefaultTable(stor.lesdiv.figure, 1, 1)[0][0]
      modif(hh)
      const txtFigure = 'TWF0aEdyYXBoSmF2YTEuMAAAABM+TMzNAAJmcv###wEA#wEAAAAAAAAAAANLAAACfwAAAQEAAAAAAAAAAQAAAMz#####AAAAAQAKQ0NhbGNDb25zdAD#####AAJwaQAWMy4xNDE1OTI2NTM1ODk3OTMyMzg0Nv####8AAAABAApDQ29uc3RhbnRlQAkh+1RELRj#####AAAAAQAKQ1BvaW50QmFzZQD#####AQAAAAAOAAFVAMAkAAAAAAAAQBAAAAAAAAAAAAUAAEAv#nbItDlYQC#+dsi0OVj#####AAAAAQAUQ0Ryb2l0ZURpcmVjdGlvbkZpeGUA#####wEAAAAAEAAAAQAAAAEAAAABAT#wAAAAAAAA#####wAAAAEAD0NQb2ludExpZURyb2l0ZQD#####AQAAAAAOAAFWAMAAAAAAAAAAQBAAAAAAAAAAAAUAAUA##nbItDlYAAAAAv####8AAAABAAhDU2VnbWVudAD#####AQAAAAAQAAABAAAAAQAAAAEAAAAD#####wAAAAEAB0NNaWxpZXUA#####wEAAAAAEAAAAAAAAAAAAAAAQAgAAAAAAAAAAAUAAAAAAQAAAAP#####AAAAAgAMQ0NvbW1lbnRhaXJlAP####8BAAAAAAAAAAAAAAAAQBgAAAAAAAAAAAAAAAUMAAAAAAABAAAAAAAAAAEAAAAAAAAAAAABMf####8AAAABAAlDTG9uZ3VldXIA#####wAAAAEAAAADAAAAAgD#####AQAAAAAQAAAAAAAAAAAAAABACAAAAAAAAAAABQABQGMgAAAAAABAPeFHrhR64gAAAAIA#####wEAfwAAEAAAAAAAAAAAAAAAQAgAAAAAAAAAAAgAAUBi4AAAAAAAQGKcKPXCj1wAAAACAP####8AAAAAABAAAABACAAAAAAAAAAAAAAAAAAAAAAFAABAWUAAAAAAAEB8ThR64Ueu#####wAAAAEAB0NDYWxjdWwA#####wAFbWluaTEAATAAAAABAAAAAAAAAAAAAAAJAP####8ABW1heGkxAAMzNTkAAAABQHZwAAAAAAD#####AAAAAQAUQ0ltcGxlbWVudGF0aW9uUHJvdG8A#####wAHQ3Vyc2V1cgAAAAUAAAAFAAAAAwAAAAsAAAAMAAAACgAAAAMAAAAADQEAAAAAEAAAAQAAAAEAAAAKAT#wAAAAAAAAAAAABAEAAAANAAAAAAAQAAAAwAgAAAAAAAA#8AAAAAAAAAAABQAAQF1AAAAAAAAAAAAO#####wAAAAEAC0NIb21vdGhldGllAAAAAA0AAAAK#####wAAAAEACkNPcGVyYXRpb24D#####wAAAAEAD0NSZXN1bHRhdFZhbGV1cgAAAAsAAAAMAQAAAA0AAAALAAAADQAAAAz#####AAAAAQALQ1BvaW50SW1hZ2UAAAAADQEAAAAADQACTzEAwBAAAAAAAABAEAAAAAAAAAAABQAAAAAPAAAAEAAAAAsAAAAADQAAAAoAAAAMAwAAAAwBAAAAAT#wAAAAAAAAAAAADQAAAAsAAAAMAQAAAA0AAAAMAAAADQAAAAsAAAAOAAAAAA0BAAAAAA0AAkkxAMAAAAAAAAAAQAgAAAAAAAAAAAUAAAAADwAAABIAAAAFAQAAAA0AAAAAABAAAAEAAAABAAAACgAAAA8AAAAEAQAAAA0AAAAAARAAAmsxAMAAAAAAAAAAQAAAAAAAAAAAAAEAAT#LWbWbWbWbAAAAFP####8AAAACAA9DTWVzdXJlQWJzY2lzc2UBAAAADQACYTEAAAARAAAAEwAAABX#####AAAAAQAPQ1ZhbGV1ckFmZmljaGVlAQAAAA0AAAAAAAAAAAAAAAAAwBgAAAAAAAAAAAAAABUPAAH###8AAAABAAAAAgAAAAEAAAAAAAAAAAAAAAACAAAAFgAAAAkA#####wACQTEAAmExAAAADQAAABYAAAACAP####8AAAAAABAAAABACAAAAAAAAAAAAAAAAAAAAAAFAABAWMAAAAAAAEB#HhR64UeuAAAACQD#####AAVtaW5pMgABMAAAAAEAAAAAAAAAAAAAAAkA#####wAFbWF4aTIAAjEwAAAAAUAkAAAAAAAAAAAACgD#####AAdDdXJzZXVyAAAABQAAAAUAAAADAAAAGgAAABsAAAAZAAAAAwAAAAAcAQAAAAAQAAABAAAAAQAAABkBP#AAAAAAAAAAAAAEAQAAABwAAAAAABAAAADACAAAAAAAAD#wAAAAAAAAAAAFAABAXUAAAAAAAAAAAB0AAAALAAAAABwAAAAZAAAADAMAAAANAAAAGgAAAAwBAAAADQAAABoAAAANAAAAGwAAAA4AAAAAHAEAAAAADQACTzEAwBAAAAAAAABAEAAAAAAAAAAABQAAAAAeAAAAHwAAAAsAAAAAHAAAABkAAAAMAwAAAAwBAAAAAT#wAAAAAAAAAAAADQAAABoAAAAMAQAAAA0AAAAbAAAADQAAABoAAAAOAAAAABwBAAAAAA0AAkkxAMAAAAAAAAAAQAgAAAAAAAAAAAUAAAAAHgAAACEAAAAFAQAAABwAAAAAABAAAAEAAAABAAAAGQAAAB4AAAAEAQAAABwAAAAAARAAAWsAwAAAAAAAAABAAAAAAAAAAAAAAQABP+TJTJTJTJUAAAAjAAAADwEAAAAcAAJhMgAAACAAAAAiAAAAJAAAABABAAAAHAAAAAAAAAAAAAAAAADAGAAAAAAAAAAAAAAAJA8AAf###wAAAAEAAAACAAAAAQAAAAAAAAAAAAAAAAIAAAAlAAAAAgD#####AAAAAAAQAAAAQAgAAAAAAAAAAAAAAAAAAAAABQAAQFoAAAAAAABAgMcKPXCj1wAAAAkA#####wAFbWluaTMAATAAAAABAAAAAAAAAAAAAAAJAP####8ABW1heGkzAAIxMAAAAAFAJAAAAAAAAAAAAAoA#####wAHQ3Vyc2V1cgAAAAUAAAAFAAAAAwAAACgAAAApAAAAJwAAAAMAAAAAKgEAAAAAEAAAAQAAAAEAAAAnAT#wAAAAAAAAAAAABAEAAAAqAAAAAAAQAAAAwAgAAAAAAAA#8AAAAAAAAAAABQAAQF1AAAAAAAAAAAArAAAACwAAAAAqAAAAJwAAAAwDAAAADQAAACgAAAAMAQAAAA0AAAAoAAAADQAAACkAAAAOAAAAACoBAAAAAA0AAk8xAMAQAAAAAAAAQBAAAAAAAAAAAAUAAAAALAAAAC0AAAALAAAAACoAAAAnAAAADAMAAAAMAQAAAAE#8AAAAAAAAAAAAA0AAAAoAAAADAEAAAANAAAAKQAAAA0AAAAoAAAADgAAAAAqAQAAAAANAAJJMQDAAAAAAAAAAEAIAAAAAAAAAAAFAAAAACwAAAAvAAAABQEAAAAqAAAAAAAQAAABAAAAAQAAACcAAAAsAAAABAEAAAAqAAAAAAEQAAJrMgDAAAAAAAAAAEAAAAAAAAAAAAABAAE#2s2s2s2s2wAAADEAAAAPAQAAACoAAmEzAAAALgAAADAAAAAyAAAAEAEAAAAqAAAAAAAAAAAAAAAAAMAYAAAAAAAAAAAAAAAyDwAB####AAAAAQAAAAIAAAABAAAAAAAAAAAAAAAAAgAAADMAAAAJAP####8AAkEyAAJhMgAAAA0AAAAlAAAACQD#####AAJBMwACYTMAAAANAAAAMwAAAAkA#####wABUgADMS42AAAAAT#5mZmZmZmaAAAACQD#####AAJyMgADMC41AAAAAT#gAAAAAAAAAAAACQD#####AAJyMwABMQAAAAE#8AAAAAAAAP####8AAAABAAlDUm90YXRpb24A#####wAAAAkAAAANAAAAGAAAAA4A#####wH#AAAAEAAAAAAAAAAAAAAAQAgAAAAAAAAAAAgAAAAACAAAADr#####AAAAAQARQ1N5bWV0cmllQ2VudHJhbGUA#####wAAAAkAAAAOAP####8B#wAAABAAAAAAAAAAAAAAAEAIAAAAAAAAAAAIAAAAADsAAAA8AAAACQD#####AANMSU0AAjMwAAAAAUA+AAAAAAAAAAAAEQD#####AAAACQAAAAwAAAAADQAAAD4AAAAMAgAAAAwDAAAADAEAAAABQGaAAAAAAAAAAAAMAgAAAAFAAAAAAAAAAAAAAA0AAAA+AAAAAUAkAAAAAAAAAAAADQAAADUAAAAOAP####8B#wAAABAAAAAAAAAAAAAAAEAIAAAAAAAAAAAIAAAAADsAAAA#AAAAEQD#####AAAACQAAAAwAAAAADQAAAD4AAAAMAgAAAAwDAAAADAEAAAABQGaAAAAAAAAAAAAMAgAAAAFAAAAAAAAAAAAAAA0AAAA+AAAAAUAkAAAAAAAAAAAADQAAADYAAAAOAP####8B#wAAABAAAAAAAAAAAAAAAEAIAAAAAAAAAAAIAAAAAD0AAABBAAAABQD#####AAAAAAAQAAABAAAAAwAAADsAAAA9AAAABQD#####AQAAAAAQAAABAAN2djEAAwAAAEIAAAA9AAAABQD#####AAAAAAAQAAABAAAAAwAAAD0AAABAAAAABQD#####AAAAAAAQAAABAAAAAwAAAEAAAAA7AAAABQD#####AQAAAAAQAAABAAN2djIAAwAAADsAAABC#####wAAAAIACUNDZXJjbGVPUgD#####AQAAAAAAAAMAAABCAAAAAT#TMzMzMzMzAAAAABMA#####wEAAAAAAAADAAAAQAAAAAE#0zMzMzMzMwD#####AAAAAQAQQ0ludERyb2l0ZUNlcmNsZQD#####AAAARAAAAEj#####AAAAAQAQQ1BvaW50TGllQmlwb2ludAD#####AQAAAAAQAAAAAAAAAAAAAABACAAAAAAAAAAACAABAAAASgAAABUA#####wAAAAAAEAAAAAAAAAAAAAAAQAgAAAAAAAAAAAgAAgAAAEoAAAAUAP####8AAABHAAAASAAAABUA#####wAAAAAAEAAAAAAAAAAAAAAAQAgAAAAAAAAAAAgAAQAAAE0AAAAVAP####8BAAAAABAAAAAAAAAAAAAAAEAIAAAAAAAAAAAIAAIAAABNAAAAFAD#####AAAARQAAAEkAAAAVAP####8AAAAAABAAAAAAAAAAAAAAAEAIAAAAAAAAAAAIAAEAAABQAAAAFQD#####AQAAAAAQAAAAAAAAAAAAAABACAAAAAAAAAAACAACAAAAUAAAABQA#####wAAAEYAAABJAAAAFQD#####AQAAAAAQAAAAAAAAAAAAAABACAAAAAAAAAAACAABAAAAUwAAABUA#####wAAAAAAEAAAAAAAAAAAAAAAQAgAAAAAAAAAAAgAAgAAAFP#####AAAAAQAWQ0Ryb2l0ZVBlcnBlbmRpY3VsYWlyZQD#####AQAAAAAQAAABAAAAAwAAAE8AAABHAAAAFgD#####AQAAAAAQAAABAAAAAwAAAEsAAABEAAAAFgD#####AQAAAAAQAAABAAAAAwAAAFIAAABFAAAAFgD#####AQAAAAAQAAABAAAAAwAAAFQAAABG#####wAAAAEAEENJbnREcm9pdGVEcm9pdGUA#####wEAAAAAEAAAAAAAAAAAAAAAQAgAAAAAAAAAAAgAAAAAVgAAAFcAAAAXAP####8BAAAAABAAAAAAAAAAAAAAAEAIAAAAAAAAAAAIAAAAAFgAAABZAAAABQD#####AH9#AAAQAAABAAAAAwAAAFIAAABbAAAABQD#####AH9#AAAQAAABAAAAAwAAAFsAAABUAAAABQD#####AX9#AAAQAAABAAN2djMAAwAAAEsAAABaAAAABQD#####AX9#AAAQAAABAAN2djQAAwAAAFoAAABPAAAABgD#####AQB#AAAQAAAAAAAAAAAAAABACAAAAAAAAAAACAAAAAA9AAAAQAAAAAYA#####wEAfwAAEAAAAAAAAAAAAAAAQAgAAAAAAAAAAAgAAAAAQAAAADsAAAAGAP####8BAH8AABAAAAAAAAAAAAAAAEAIAAAAAAAAAAAIAAAAADsAAABCAAAABgD#####AQB#AAAQAAAAAAAAAAAAAABACAAAAAAAAAAACAAAAABCAAAAPQAAABMA#####wEAfwAAAAADAAAAOwAAAA0AAAA5AAAAABMA#####wEAfwAAAAADAAAAPQAAAA0AAAA5AAAAABQA#####wAAAEMAAABlAAAAFQD#####AAAA#wAQAAAAAAAAAAAAAABACAAAAAAAAAAACAABAAAAZgAAABUA#####wEAAP8AEAAAAAAAAAAAAAAAQAgAAAAAAAAAAAgAAgAAAGYAAAAUAP####8AAABEAAAAZQAAABUA#####wAAAP8AEAAAAAAAAAAAAAAAQAgAAAAAAAAAAAgAAQAAAGkAAAAVAP####8BAAD#ABAAAAAAAAAAAAAAAEAIAAAAAAAAAAAIAAIAAABpAAAAFAD#####AAAARQAAAGUAAAAVAP####8BAAD#ABAAAAAAAAAAAAAAAEAIAAAAAAAAAAAIAAEAAABsAAAAFQD#####AAAA#wAQAAAAAAAAAAAAAABACAAAAAAAAAAACAACAAAAbAAAABQA#####wAAAEYAAABkAAAAFQD#####AAAA#wAQAAAAAAAAAAAAAABACAAAAAAAAAAACAABAAAAbwAAABUA#####wEAAP8AEAAAAAAAAAAAAAAAQAgAAAAAAAAAAAgAAgAAAG8AAAAUAP####8AAABDAAAAZAAAABUA#####wEAAP8AEAAAAAAAAAAAAAAAQAgAAAAAAAAAAAgAAQAAAHIAAAAVAP####8AAAD#ABAAAAAAAAAAAAAAAEAIAAAAAAAAAAAIAAIAAAByAAAAFAD#####AAAARwAAAGQAAAAVAP####8BAAD#ABAAAAAAAAAAAAAAAEAIAAAAAAAAAAAIAAEAAAB1AAAAFQD#####AAAA#wAQAAAAAAAAAAAAAABACAAAAAAAAAAACAACAAAAdf####8AAAABAAxDQXJjRGVDZXJjbGUA#####wEAAP8AAAADAAAAPQAAAG0AAABoAAAAGAD#####AQAA#wAAAAMAAAA9AAAAaAAAAGsAAAAYAP####8BAAD#AAAAAwAAADsAAABxAAAAcwAAABgA#####wEAAP8AAAADAAAAOwAAAHMAAAB2#####wAAAAEAGUNTdXJmYWNlU2VjdGV1ckNpcmN1bGFpcmUA#####wF#AAAAA3NhMwAAAAQAAAB5AAAAGQD#####AX8AAAADc2EyAAAABAAAAHgAAAAZAP####8BfwAAAANzYTEAAAAEAAAAegAAABkA#####wF#AAAAA3NhNAAAAAQAAAB7AAAAEwD#####AQAA#wAAAAMAAAA7AAAADQAAADcAAAAAEwD#####AQAA#wAAAAMAAAA9AAAADQAAADcA#####wAAAAEADENCaXNzZWN0cmljZQD#####AQAA#wAQAAABAAAAAwAAAEAAAAA9AAAAOwAAABoA#####wEAAP8AEAAAAQAAAAMAAABAAAAAOwAAAD0AAAAaAP####8BAAD#ABAAAAEAAAADAAAAOwAAAD0AAABCAAAAGgD#####AQAA#wAQAAABAAAAAwAAAD0AAAA7AAAAQgAAABQA#####wAAAIMAAACAAAAAFQD#####AQAA#wAQAAAAAAAAAAAAAABACAAAAAAAAAAACAACAAAAhgAAABUA#####wH###8AEAAAAAAAAAAAAAAAQAgAAAAAAAAAAAcAAQAAAIYAAAAUAP####8AAACFAAAAgAAAABUA#####wH###8AEAAAAAAAAAAAAAAAQAgAAAAAAAAAAAcAAQAAAIkAAAAVAP####8BAAD#ABAAAAAAAAAAAAAAAEAIAAAAAAAAAAAIAAIAAACJAAAAFAD#####AAAAhAAAAIEAAAAVAP####8BAAD#ABAAAAAAAAAAAAAAAEAIAAAAAAAAAAAIAAIAAACMAAAAFQD#####Af###wAQAAAAAAAAAAAAAABACAAAAAAAAAAABwABAAAAjAAAABQA#####wAAAIIAAACBAAAAFQD#####AQAA#wAQAAAAAAAAAAAAAABACAAAAAAAAAAACAACAAAAjwAAABUA#####wH###8AEAAAAAAAAAAAAAAAQAgAAAAAAAAAAAcAAQAAAI8AAAAaAP####8BAAD#ABAAAAEAAAADAAAAPQAAAEAAAAA7AAAAEwD#####AQAA#wAAAAMAAABCAAAADQAAADgAAAAAEwD#####AQAA#wAAAAMAAABAAAAADQAAADgAAAAAFAD#####AAAAkgAAAJQAAAAVAP####8BAAD#ABAAAAAAAAAAAAAAAEAIAAAAAAAAAAAIAAEAAACVAAAAFQD#####Af###wAQAAAAAAAAAAAAAABACAAAAAAAAAAABwACAAAAlQAAABoA#####wEAAP8AEAAAAQAAAAMAAAA9AAAAQgAAADsAAAAUAP####8AAACYAAAAkwAAABUA#####wH###8AEAAAAAAAAAAAAAAAQAgAAAAAAAAAAAcAAgAAAJkAAAAVAP####8BAAD#ABAAAAAAAAAAAAAAAEAIAAAAAAAAAAAIAAEAAACZAAAAEwD#####AQAA#wAAAAMAAAA9AAAADQAAADgAAAAAEwD#####AQAA#wAAAAMAAAA7AAAADQAAADgA#####wAAAAEACUNEcm9pdGVBQgD#####AQAA#wAQAAABAAAAAwAAADsAAAA9AAAAFAD#####AAAAngAAAJwAAAAVAP####8BAAD#ABAAAAAAAAAAAAAAAEAIAAAAAAAAAAAIAAIAAACfAAAAFQD#####Af###wAQAAAAAAAAAAAAAABACAAAAAAAAAAABwABAAAAnwAAABQA#####wAAAJ4AAACdAAAAFQD#####AQAA#wAQAAAAAAAAAAAAAABACAAAAAAAAAAACAABAAAAogAAABUA#####wH###8AEAAAAAAAAAAAAAAAQAgAAAAAAAAAAAcAAgAAAKIAAAAWAP####8BAAD#ABAAAAEAAAADAAAAYgAAAEcAAAAWAP####8BAAD#ABAAAAEAAAADAAAAYwAAAEQAAAAWAP####8BAAD#ABAAAAEAAAADAAAAYAAAAEUAAAAWAP####8BAAD#ABAAAAEAAAADAAAAYQAAAEYAAAAJAP####8AAWEAAzAuNgAAAAE#4zMzMzMzMwAAABMA#####wEAAP8AAAADAAAAYAAAAA0AAACpAAAAABMA#####wEAAP8AAAADAAAAYwAAAA0AAACpAAAAABMA#####wEAAP8AAAADAAAAYgAAAA0AAACpAAAAABMA#####wEAAP8AAAADAAAAYQAAAA0AAACpAAAAABQA#####wAAAKUAAACsAAAAFQD#####AQAA#wAQAAAAAAAAAAAAAABACAAAAAAAAAAACAABAAAArgAAABUA#####wH###8AEAAAAAAAAAAAAAAAQAgAAAAAAAAAAAcAAgAAAK4AAAAUAP####8AAACmAAAAqwAAABUA#####wEAAP8AEAAAAAAAAAAAAAAAQAgAAAAAAAAAAAgAAQAAALEAAAAVAP####8B####ABAAAAAAAAAAAAAAAEAIAAAAAAAAAAAHAAIAAACxAAAAFAD#####AAAApwAAAKoAAAAVAP####8BAAD#ABAAAAAAAAAAAAAAAEAIAAAAAAAAAAAIAAEAAAC0AAAAFQD#####Af###wAQAAAAAAAAAAAAAABACAAAAAAAAAAABwACAAAAtAAAABQA#####wAAAKgAAACtAAAAFQD#####AQAA#wAQAAAAAAAAAAAAAABACAAAAAAAAAAACAABAAAAtwAAABUA#####wH###8AEAAAAAAAAAAAAAAAQAgAAAAAAAAAAAcAAgAAALcAAAAHAP####8BfwB#AQACbDEAAAC2EgAAAAAAAQAAAAEAAAABAAAAAAAAAAAAAmwxAAAABwD#####AX8AfwEAAmwyAAAAsxIAAAAAAAEAAAABAAAAAQAAAAAAAAAAAAJsMgAAAAcA#####wF#AH8BAAJsMwAAALASAAAAAAABAAAAAQAAAAEAAAAAAAAAAAACbDMAAAAHAP####8BfwB#AQACbDQAAAC5EgAAAAAAAQAAAAEAAAABAAAAAAAAAAAAAmw0AAAAEwD#####AX8AfwAAAAMAAAAJAAAADQAAAKkAAAAAFgD#####AX8AfwAQAAABAAAAAwAAAAkAAABDAAAAFAD#####AAAAvwAAAL4AAAAVAP####8BfwB#ABAAAAAAAAAAAAAAAEAIAAAAAAAAAAAIAAEAAADAAAAAFQD#####Af###wAQAAAAAAAAAAAAAABACAAAAAAAAAAABwACAAAAwAAAAAcA#####wF#AH8BAAJsNQAAAMISAAAAAAABAAAAAQAAAAEAAAAAAAAAAAACbDUAAAAHAP####8AAAD#AQABQQAAAJcQAAAAAAABAAAAAQAAAAEAAAAAAAAAAAABQQAAAAcA#####wAAAP8BAAFCAAAAoRAAAAAAAAEAAAABAAAAAQAAAAAAAAAAAAFCAAAABwD#####AQAA#wEAAUMAAACaEAAAAAAAAQAAAAEAAAABAAAAAAAAAAAAAUMAAAAHAP####8AAAD#AQABRAAAAKQQAAAAAAABAAAAAQAAAAEAAAAAAAAAAAABRAAAAAcA#####wF#AAABAAJhMQAAAIgQAAAAAAABAAAAAQAAAAEAAAAAAAAAAAACYTEAAAAHAP####8BfwAAAQACYTIAAACREAAAAAAAAQAAAAEAAAABAAAAAAAAAAAAAmEyAAAABwD#####AX8AAAEAAmEzAAAAjhAAAAAAAAEAAAABAAAAAQAAAAAAAAAAAAJhMwAAAAcA#####wF#AAABAAJhNAAAAIoQAAAAAAABAAAAAQAAAAEAAAAAAAAAAAACYTQAAAAH##########8='
      const yy = j3pCreeSVG(hh, { id: svgId, width: 400, height: 350 })
      yy.style.border = '1px solid black'
      stor.mtgAppLecteur.removeAllDoc()
      stor.mtgAppLecteur.addDoc(svgId, txtFigure, true)
      const A2 = (2 * a0 - 30) / 12
      const A3 = (2 * a4 - 30) / 12
      stor.mtgAppLecteur.giveFormula2(svgId, 'A1', j3pGetRandomInt(1, 359))
      stor.mtgAppLecteur.giveFormula2(svgId, 'A2', A2)
      stor.mtgAppLecteur.giveFormula2(svgId, 'A3', A3)
      stor.mtgAppLecteur.calculateAndDisplayAll(true)
      stor.mtgAppLecteur.setText(svgId, '#A', lettres[2], true)
      stor.mtgAppLecteur.setText(svgId, '#B', lettres[0], true)
      stor.mtgAppLecteur.setText(svgId, '#D', lettres[1], true)

      if (vl3) {
        stor.mtgAppLecteur.setVisible(svgId, '#l5', true, true)
        stor.mtgAppLecteur.setText(svgId, '#l5', String(stor.l3).replace('.', ','), true)
      }
      if (va0) {
        stor.mtgAppLecteur.setVisible(svgId, '#sa2', true, true)
        if ((stor.Lexo.calcul !== 'Angle') && (ds.Calcul !== 'Formule')) {
          stor.mtgAppLecteur.setVisible(svgId, '#a2', true, true)
          stor.mtgAppLecteur.setText(svgId, '#a2', a0 + '°', true)
        }
      }
      if (va1) {
        stor.mtgAppLecteur.setVisible(svgId, '#sa1', true, true)
        if ((stor.Lexo.calcul !== 'Angle') && (ds.Calcul !== 'Formule')) {
          stor.mtgAppLecteur.setVisible(svgId, '#a1', true, true)
          stor.mtgAppLecteur.setText(svgId, '#a1', stor.a1 + '°', true)
        }
      }
      if (vl1) {
        stor.mtgAppLecteur.setVisible(svgId, '#l1', true, true)
        stor.mtgAppLecteur.setText(svgId, '#l1', String(stor.l1).replace('.', ','), true)
      }
      if (vl2) {
        stor.mtgAppLecteur.setVisible(svgId, '#l4', true, true)
        stor.mtgAppLecteur.setText(svgId, '#l4', String(stor.l2).replace('.', ','), true)
      }

      if (stor.Lexo.inut) {
        stor.mtgAppLecteur.setVisible(svgId, '#vv1', true, true)
        stor.mtgAppLecteur.setVisible(svgId, '#vv2', true, true)
        stor.mtgAppLecteur.setVisible(svgId, '#vv3', true, true)
        stor.mtgAppLecteur.setVisible(svgId, '#vv4', true, true)
        stor.mtgAppLecteur.setVisible(svgId, '#C', true, true)
        stor.mtgAppLecteur.setText(svgId, '#C', lettres[3], true)
        if (va4) {
          stor.mtgAppLecteur.setVisible(svgId, '#sa4', true, true)
          if ((stor.Lexo.calcul !== 'Angle') && (ds.Calcul !== 'Formule')) {
            stor.mtgAppLecteur.setVisible(svgId, '#a4', true, true)
            stor.mtgAppLecteur.setText(svgId, '#a4', a4 + '°', true)
          }
        }
        if (va5) {
          stor.mtgAppLecteur.setVisible(svgId, '#sa3', true, true)
          if ((stor.Lexo.calcul !== 'Angle') && (ds.Calcul !== 'Formule')) {
            stor.mtgAppLecteur.setVisible(svgId, '#a3', true, true)
            stor.mtgAppLecteur.setText(svgId, '#a3', stor.a5 + '°', true)
          }
        }
        if (vl4) {
          stor.mtgAppLecteur.setVisible(svgId, '#l2', true, true)
          stor.mtgAppLecteur.setText(svgId, '#l2', String(stor.l4).replace('.', ','), true)
        }
        if (vl5) {
          stor.mtgAppLecteur.setVisible(svgId, '#l3', true, true)
          stor.mtgAppLecteur.setText(svgId, '#l3', String(stor.l5).replace('.', ','), true)
        }
      }

      /// ////
      /// ////

      j3pAffiche(stor.lesdiv.figure, null, 'Les longueurs de cette \nfigure sont exprimées en cm')
    } else {
      let adire = []
      if (vl1) adire.push({ quoi: 'l', qui: lettres[0] + lettres[2], val: stor.l1 })
      if (vl2) adire.push({ quoi: 'l', qui: lettres[1] + lettres[2], val: stor.l2 })
      if (vl3) adire.push({ quoi: 'l', qui: lettres[0] + lettres[1], val: stor.l3 })
      if (vl4) adire.push({ quoi: 'l', qui: lettres[0] + lettres[3], val: stor.l4 })
      if (vl5) adire.push({ quoi: 'l', qui: lettres[1] + lettres[3], val: stor.l5 })
      if (va0) adire.push({ quoi: 'a', qui: lettres[1] + lettres[0] + lettres[2], val: stor.a0 })
      if (va1) adire.push({ quoi: 'a', qui: lettres[0] + lettres[1] + lettres[2], val: stor.a1 })
      if (va4) adire.push({ quoi: 'a', qui: lettres[1] + lettres[0] + lettres[3], val: stor.a4 })
      if (va5) adire.push({ quoi: 'a', qui: lettres[0] + lettres[1] + lettres[3], val: stor.a5 })
      adire = j3pShuffle(adire)
      adire.push({ quoi: 't', qui: lettres[0] + lettres[2] + lettres[1], val: lettres[2] })
      if (stor.Lexo.inut) adire.push({ quoi: 't', qui: lettres[3] + lettres[0] + lettres[1], val: lettres[3] })
      j3pAffiche(stor.lesdiv.figure, null, '<b><u>Données</u></b>\n')
      for (let i = 0; i < adire.length; i++) {
        switch (adire[i].quoi) {
          case 'a': {
            if (stor.Lexo.calcul === 'Angle') break
            const span = j3pAddElt(stor.lesdiv.figure, 'span')
            j3pAffiche(span, null, '$\\widehat{' + adire[i].qui + '}$')
            j3pAffiche(stor.lesdiv.figure, null, ' $ = ' + adire[i].val + ' °$ \n')
            break
          }
          case 'l':
            j3pAffiche(stor.lesdiv.figure, null, ' $ ' + adire[i].qui + ' = ' + adire[i].val + ' $ cm \n')
            break
          case 't':
            j3pAffiche(stor.lesdiv.figure, null, ' Le triangle ' + adire[i].qui + ' est rectangle en  ' + adire[i].val + '  \n')
        }
      }
    }
  }
  function initSection () {
    stor.numc = j3pGetRandomInt(0, 2)
    stor.boolinut = j3pGetRandomBool()
    stor.alphab = 'ABCDEFGHIJKLMNOPQRSTUVWXYZabcdefghijklmnopqrstuvwxyz'
    ds.nbchancesInt = ds.nbchances
    ds.nbchances = 1
    // Construction de la page
    me.construitStructurePage('presentation3')

    ds.lesExos = []

    let lp = []
    if (ds.sinus)lp.push('sinus')
    if (ds.cosinus)lp.push('cosinus')
    if (ds.tangente)lp.push('tangente')
    if (lp.length === 0) {
      console.error('Paramètres: Aucun exercice possible !')
      lp.push('cosinus')
      ds.cosinus = true
    }
    const piz = (lp.length > 1) ? 'la trigonométrie' : lp[0]
    lp = j3pShuffle(lp)
    for (let i = 0; i < ds.nbitems; i++) {
      ds.lesExos.push({ formule: lp[i % (lp.length)] })
    }
    ds.lesExos = j3pShuffle(ds.lesExos)

    lp = []
    if ((ds.Type_Enonce === 'Les deux') || (ds.Type_Enonce === 'Figure'))lp.push('Figure')
    if ((ds.Type_Enonce === 'Les deux') || (ds.Type_Enonce === 'Texte'))lp.push('Texte')
    lp = j3pShuffle(lp)
    for (let i = 0; i < ds.nbitems; i++) {
      ds.lesExos[i].type = lp[i % (lp.length)]
    }
    ds.lesExos = j3pShuffle(ds.lesExos)

    lp = []
    if ((ds.Calcul === 'Les deux') || (ds.Calcul === 'Longueur')) {
      lp.push('Longueur1')
      lp.push('Longueur2')
    }
    if ((ds.Calcul === 'Les deux') || (ds.Calcul === 'Angle'))lp.push('Angle')
    lp = j3pShuffle(lp)
    for (let i = 0; i < ds.nbitems; i++) {
      ds.lesExos[i].calcul = lp[i % (lp.length)]
    }
    ds.lesExos = j3pShuffle(ds.lesExos)

    lp = [1, 2, 3]
    lp = j3pShuffle(lp)
    for (let i = 0; i < ds.nbitems; i++) {
      ds.lesExos[i].arrond = lp[i % (lp.length)]
    }
    ds.lesExos = j3pShuffle(ds.lesExos)

    lp = []
    if ((ds.Donnees_Inutiles === 'oui') || (ds.Donnees_Inutiles === 'Les deux')) lp.push(true)
    if ((ds.Donnees_Inutiles === 'non') || (ds.Donnees_Inutiles === 'Les deux')) lp.push(false)
    lp = j3pShuffle(lp)
    for (let i = 0; i < ds.nbitems; i++) {
      ds.lesExos[i].inut = lp[i % (lp.length)]
    }
    ds.lesExos = j3pShuffle(ds.lesExos)
    me.typederreurs = [0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0]
    let bufdeb = 'Caluler en utilisant '
    if (ds.Calcul === 'Angle') bufdeb = 'Calculer un angle en utilisant '
    if (ds.Calcul === 'Longueur') bufdeb = 'Calculer une longueur en utilisant '
    bufdeb += piz

    me.afficheTitre(bufdeb)
    getMtgCore({ withMathJax: true })
      .then(
        // success
        (mtgAppLecteur) => {
          stor.mtgAppLecteur = mtgAppLecteur
          enonceMain()
        },
        // failure
        (error) => {
          j3pShowError(error, { message: 'mathgraph n’est pas correctement chargé', mustNotify: true })
        })
      // plantage dans enonceMain
      .catch(j3pShowError)
  }
  function yaReponse () {
    if (stor.Finish) return true
    if (stor.etat === 'FORM') {
      if (ds.Description) {
        if (stor.zoneDesc.reponse() === '') {
          stor.zoneDesc.focus()
          return false
        }
        if (stor.zoneDesc2.reponse() === '') {
          stor.zoneDesc2.focus()
          return false
        }
      }
      if (ds.Calcul !== 'Formule') {
        if (!stor.zoneForm.changed) {
          stor.zoneForm.focus()
          return false
        }
      }
      if (!stor.zoneLnum.changed) {
        stor.zoneLnum.focus()
        return false
      }
      if (!stor.zoneLden.changed) {
        stor.zoneLden.focus()
        return false
      }
      return true
    }
    if (stor.etat === 'FORM2') {
      if (stor.Lexo.calcul !== 'Angle') {
        if (!stor.zoneForm2.changed) {
          stor.zoneForm2.focus()
          return false
        }
      }
      if (!stor.zoneLnum2.changed) {
        stor.zoneLnum2.focus()
        return false
      }
      if (!stor.zoneLden2.changed) {
        stor.zoneLden2.focus()
        return false
      }
      return true
    }
    if (stor.etat === 'CALCUL1') {
      if (stor.Lexo.calcul !== 'Angle') {
        if (stor.LOP === 'z') { return false }
        if (!stor.zoneC2.changed) {
          stor.zoneC2.focus()
          return false
        }
      } else {
        if (!stor.zoneC1.changed) {
          stor.zoneC1.focus()
          return false
        }
      }
      return true
    }
    if (!stor.zoneEgale.changed) {
      stor.zoneEgale.focus()
      return false
    }
    if (stor.zoneRep.reponse() === '') {
      stor.zoneRep.focus()
      return false
    }
    if (!stor.zoneunite.changed) {
      stor.zoneunite.focus()
      return false
    }
    return true
  }
  function modif (ki) {
    for (let i = 0; i < ki.length; ki++) {
      for (let j = 0; j < ki[i].length; j++) {
        ki[i][j].style.padding = '0px'
      }
    }
  }
  /*
    function tetang (x) {
      var i
      var j
      var f = x.reponsechap()
      var ff = stor

      // 3 lettres
      if (f[0].length < 3) {
        stor.ErrEcritureAngle3 = true
        x.corrige(false)
        return false
      }
      // majuscules
      for (i = 0; i < 3; i++) {
        if ('abcdefghijklmnopqrstuvwxyz'.indexOf(f[0][i]) !== -1) {
          stor.ErrEcritureAngleMaj = true
          x.corrige(false)
          return false
        }
      }
      // pas deux fois la mm
      for (i = 0; i < 2; i++) {
        for (j = i + 1; j < 3; j++) {
          if (f[0][i] === f[0][j]) {
            stor.ErrEcritureAngleDouble = true
            x.corrige(false)
            return false
          }
        }
      }
      // des lettres presentes
      for (i = 0; i < 3; i++) {
        if (ff.lettres.indexOf(f[0][i]) === -1) {
          stor.ErrEcritureAngleIcon = true
          x.corrige(false)
          return false
        }
      }

      // dan bon tri
      var bug = ff.nomtriangle
      for (i = 0; i < 3; i++) {
        if (bug.indexOf(f[0][i]) === -1) {
          stor.horstri = true
          x.corrige(false)
          return false
        }
      }
      if (f[1] !== 1) {
        stor.ErrEcritureAngleChap = true
        x.corrige(false)
        return false
      }
      return true
    }
    function tetmes (x, b) {
      var a = x.reponse()
      if (b) {
        if (a.indexOf('°') === -1) {
          stor.ErrMesDeg = true
          x.corrige(false)
          return false
        }
      }
      if (a.replace('°', '').indexOf('°') !== -1) {
        stor.ErrMesDegDoub = true
        x.corrige(false)

        return false
      }
      if (a[0] === '0') {
        stor.ErrMesZero = true
        x.corrige(false)
        return false
      }
      return true
    }
    */
  function tetpoint (x) {
    const a = x.reponse()
    /// test que 1
    if (a.length !== 1) {
      stor.errPointNb = true
      x.corrige(false)
      return false
    }
    /// test pas de min
    if ('abcdefghijklmnopqrstuvwxyz'.indexOf(a[0]) !== -1) {
      stor.errPointMaj = true
      x.corrige(false)
      return false
    }
    /// test point existe
    if (stor.lettres.indexOf(a[0]) === -1) {
      stor.errPointIcon = true
      x.corrige(false)
      return false
    }
    return true
  }
  function tettriangle (a, b, x) {
    let i
    const c = x.reponse()

    // verif pas crochets
    if ((c.indexOf('[') !== -1) || (c.indexOf(']') !== -1)) {
      stor.EcrTriCroch = true
      x.corrige(false)
      return false
    }
    // verif pas parent
    if ((c.indexOf(')') !== -1) || (c.indexOf('(') !== -1)) {
      stor.EcrTriPar = true
      x.corrige(false)
      return false
    }
    // verif pas chapeau
    if (x.reponsechap()[1] !== 0) {
      stor.EcrTriChap = true
      x.corrige(false)
      return false
    }
    // test 3 lettres
    if (c.length !== 3) {
      stor.EcrTri3let = true
      x.corrige(false)
      return false
    }
    // test en majuscule
    for (i = 0; i < 3; i++) {
      if ('ABCDEFGHIJKLMNOPQRSTUVWXYZ'.indexOf(c[i]) === -1) {
        stor.EcrTriMaj = true
        x.corrige(false)
        return false
      }
    }
    // test pas de doublons
    if ((c[0] === c[1]) || (c[0] === c[2]) || (c[2] === c[1])) {
      stor.EcrTriDoub = true
      x.corrige(false)
      return false
    }
    // test ya les bonnes
    if ((c.indexOf(a[0]) === -1) || (c.indexOf(a[1]) === -1) || (c.indexOf(a[2]) === -1)) {
      // si ya pas les bonnes , test ya les fause
      if ((c.indexOf(b[0]) === -1) || (c.indexOf(b[1]) === -1) || (c.indexOf(b[2]) === -1)) {
        stor.ErrTriNom2 = true
        x.corrige(false)
        return false
      } else {
        stor.ErrTriNom1 = true
        x.corrige(false)
        return false
      }
    }
    return true
  }
  function isRepOk () {
    return stor.LarepInt
  }
  function isRepOkInt () {
    stor.EcrTri3let = false // 3 let dans un tri
    stor.EcrTriMaj = false // des maj dans un tri
    stor.EcrTriCroch = false // crochets dans un tri
    stor.EcrTriPar = false // parent dans un tri
    stor.EcrTriChap = false // parent dans un tri
    stor.ErrTriNom1 = false // a pris le mauvais tri
    stor.ErrTriNom2 = false // pas le bon de tri
    stor.EcrTriDoub = false // deux mm lettres dans tri
    stor.errSom = false // pas bon rectangle en ...
    stor.errPointIcon = false // point inconnu
    stor.errPointMaj = false // point majuscule
    stor.errPointNb = false // un point 1 lettre
    stor.errForm = false // pas bon choix entre cos sin tan
    stor.errFormAngle = false // pas le bon angle dans formule
    stor.errFormNum = false // pas le bon num dans formule
    stor.errFormden = false // pas le bon den dans formule
    stor.errValAngle = false // la mesure d’angle est pas bonne (2e ligne)
    stor.errValLong = false // erreur longueur dans fomrule nb (2e ligne)
    stor.errValLong2 = false // erreur nom long dans fomrule nb (2e ligne)
    stor.errValLongNOM = false // le nom rech dans 2e ligne (2e ligne)
    stor.errOP = false // 3e Ligne inverse / ou x
    stor.errPREZlign3 = false // manqu un nb en l3
    stor.errNUmLigne3 = false // err nu den L3
    stor.errEgal = false // err = ou approx
    stor.errArrond = false // aval ap pas au bon endroit
    stor.errRepCalcul = false // calcul fo
    stor.errUnit = false // erreur cm
    stor.errcos1 = false // prend pas cos-1 au leu de cos
    stor.ErrAngUnit = false // oubli de degre

    let ok = true
    let t1
    let t2
    let v1
    let v2
    let z

    if (stor.etat === 'FORM') {
      if (ds.Description) {
        ok = tettriangle(stor.nomtri, stor.nomtri2, stor.zoneDesc)
        ok = ok && (tetpoint(stor.zoneDesc2))
        if (ok && stor.zoneDesc2.reponse() !== stor.nomsom) {
          ok = false
          stor.zoneDesc2.corrige(false)
          stor.errSom = true
        }
      }
      if (ds.Calcul !== 'Formule') {
        t1 = stor.zoneForm.reponse
        if (t1.indexOf('cos') !== -1) {
          t2 = 'cosinus'
        }
        if (t1.indexOf('sin') !== -1) {
          t2 = 'sinus'
        }
        if (t1.indexOf('tan') !== -1) {
          t2 = 'tangente'
        }
        if (t2 !== stor.Lexo.formule) {
          stor.errForm = true
          stor.zoneForm.corrige(false)
          ok = false
        }
        if (stor.la !== stor.A) {
          stor.errFormAngle = true
          stor.zoneForm.corrige(false)
          ok = false
        }
      }
      if ((stor.zoneLnum.reponse !== stor.L1) && (stor.zoneLnum.reponse !== stor.L1[1] + stor.L1[0])) {
        stor.errFormNum = true
        stor.zoneLnum.corrige(false)
        ok = false
      }
      if ((stor.zoneLden.reponse !== stor.L2) && (stor.zoneLden.reponse !== stor.L2[1] + stor.L2[0])) {
        stor.errFormden = true
        stor.zoneLden.corrige(false)
        ok = false
      }
      return ok
    }
    if (stor.etat === 'FORM2') {
      if ((stor.Lexo.calcul === 'Longueur2') || (stor.Lexo.calcul === 'Longueur1')) {
        if (stor.zoneForm2.reponse.indexOf(String(stor.Ma)) === -1) {
          ok = false
          stor.zoneForm2.corrige(false)
          stor.errValAngle = true
        }
        if (stor.Lexo.calcul === 'Longueur1') {
          t1 = stor.lL2
          if ((stor.zoneLnum2.reponse !== stor.L1) && (stor.zoneLnum2.reponse !== stor.L1[1] + stor.L1[0])) {
            ok = false
            stor.zoneLnum2.corrige(false)
            stor.errValLongNOM = true
          }
          if (stor.zoneLden2.reponse !== String(t1).replace('.', ',')) {
            ok = false
            stor.zoneLden2.corrige(false)
            stor.errValLong = true
          }
        } else {
          t1 = stor.lL1
          if (stor.zoneLnum2.reponse !== String(t1).replace('.', ',')) {
            ok = false
            stor.zoneLnum2.corrige(false)
            stor.errValLong = true
          }
          if ((stor.zoneLden2.reponse !== stor.L2) && (stor.zoneLden2.reponse !== stor.L2[1] + stor.L2[0])) {
            ok = false
            stor.zoneLden2.corrige(false)
            stor.errValLongNOM = true
          }
        }
      } else {
        t1 = stor.lL2
        if (stor.zoneLden2.reponse !== String(t1).replace('.', ',')) {
          ok = false
          stor.zoneLden2.corrige(false)
          stor.errValLong2 = true
        }
        t1 = stor.lL1
        if (stor.zoneLnum2.reponse !== String(t1).replace('.', ',')) {
          ok = false
          stor.zoneLnum2.corrige(false)
          stor.errValLong2 = true
        }
      }
      return ok
    }
    if (stor.etat === 'CALCUL1') {
      if (stor.Lexo.calcul === 'Longueur1') {
        if (stor.LOP === '/') {
          ok = false
          stor.errOP = true
          stor.T5[0][3].style.color = me.styles.cfaux
        }
        // il faut trouver les deux a gauche ou a droite
        if (!stor.errOP) {
          // test pres deux val
          t2 = prezense([stor.zoneC1.reponse, stor.zoneC2.reponse], [stor.lL2, stor.Ma])
          if (!t2.ya) {
            ok = false
            stor.errPREZlign3 = true
            stor.zoneC1.corrige(false)
            stor.zoneC2.corrige(false)
          }
        }
      }
      if (stor.Lexo.calcul === 'Longueur2') {
        if (stor.LOP === 'x') {
          ok = false
          stor.errOP = true
          stor.T5[0][3].style.color = me.styles.cfaux
        }
        // il faut trouver truc en haut et truc en bas
        if (!stor.errOP) {
          // test pres deux val
          t2 = prezense([stor.zoneC1.reponse, stor.zoneC2.reponse], [stor.lL1, stor.Ma])
          if (!t2.ya) {
            ok = false
            stor.errPREZlign3 = true
            stor.zoneC1.corrige(false)
            stor.zoneC2.corrige(false)
          } else {
            // test le bon en haut
            if (stor.zoneC1.reponse !== String(stor.lL1).replace('.', ',')) {
              stor.errNUmLigne3 = true
              ok = false
              stor.zoneC1.corrige(false)
              stor.zoneC2.corrige(false)
            }
          }
        }
      }
      if (stor.Lexo.calcul === 'Angle') {
        if (!/arc/.test(stor.zoneC1.reponse)) {
          stor.errcos1 = true
          ok = false
          stor.zoneC1.corrige(false)
        }
      }
      return ok
    }
    if (stor.etat === 'CONCL') {
      if (stor.Lexo.calcul === 'Angle') {
        if (stor.zoneunite.reponse !== '°') {
          ok = false
          stor.zoneunite.corrige(false)
          stor.ErrAngUnit = true
        }
        v1 = stor.ArcFOFORMULE(stor.lL1 / stor.lL2) * 180 / Math.PI
        v2 = (j3pArrondi(v1, 10) === j3pArrondi(v1, stor.Lexo.arrond))
        if ((v2 && stor.zoneEgale.reponse !== '$=$') || (!v2 && stor.zoneEgale.reponse === '$=$')) {
          stor.errEgal = true
          ok = false
          stor.zoneEgale.corrige(false)
        }
        z = isApeupEgal(parseFloat(stor.zoneRep.reponse().replace(',', '.')), v1, stor.Lexo.arrond)
        if (!z.ok) {
          switch (z.err) {
            case 'arrond': stor.errArrond = true
              break
            case 'calcul': stor.errRepCalcul = true
              break
          }
          ok = false
          stor.zoneRep.corrige(false)
        }
      } else {
        if (stor.Lexo.calcul === 'Longueur1') {
          v1 = stor.FOFORMULE(stor.Ma * Math.PI / 180) * stor.lL2
        } else {
          v1 = stor.lL1 / stor.FOFORMULE(stor.Ma * Math.PI / 180)
        }
        v2 = (j3pArrondi(v1, 10) === j3pArrondi(v1, stor.Lexo.arrond))
        if ((v2 && stor.zoneEgale.reponse !== '$=$') || (!v2 && stor.zoneEgale.reponse === '$=$')) {
          stor.errEgal = true
          ok = false
          stor.zoneEgale.corrige(false)
        }
        z = isApeupEgal(parseFloat(stor.zoneRep.reponse().replace(',', '.')), v1, stor.Lexo.arrond)
        if (!z.ok) {
          switch (z.err) {
            case 'arrond': stor.errArrond = true
              break
            case 'calcul': stor.errRepCalcul = true
              break
          }
          ok = false
          stor.zoneRep.corrige(false)
        }
        if (stor.zoneunite.reponse !== 'cm') {
          ok = false
          stor.zoneunite.corrige(false)
          stor.errUnit = true
        }
      }
      return ok
    }
    console.error('ya a faire ici ?!')
  }

  /**
   * Vérifie un arrondi, retourne
   * - {ok: true} si valeur est bien un l’arrondi de ref le plus proche à la précision demandée
   * - { ok: false, err: 'arrond' } si c’est un arrondi mais pas à la précision demandée (même partie entière)
   * - { ok: false, err: 'calcul' } sinon
   * @private
   * @param {number} valeur
   * @param {number} ref
   * @param {number} precision
   * @returns {{ok: boolean}|{err: string, ok: boolean}}
   */
  function isApeupEgal (valeur, ref, precision) {
    if (valeur === j3pArrondi(ref, precision)) return { ok: true }
    // erreur d’arrondi si même partie entière
    if (Math.floor(valeur) === Math.floor(ref)) return { ok: false, err: 'arrond' }
    return { ok: false, err: 'calcul' }
  }

  function prezense (a, b) {
    let oki
    for (let j = 0; j < a.length; j++) {
      oki = false
      for (let i = 0; i < b.length; i++) {
        oki = oki || (a[i].indexOf(String(b[j]).replace('.', ',')) !== -1)
      }
      if (!oki) {
        return {
          ya: false,
          qui: b[j]
        }
      }
    }
    return { ya: true }
  }
  function affCorrFaux (isFin) {
    let bb
    let yaexplik = false
    /// //affiche indic
    if (stor.EcrTri3let) {
      j3pAffiche(stor.lesdiv.explications, null, 'Il faut trois lettres pour nommer un triangle !\n')
      yaexplik = true
    }
    if (stor.EcrTriCroch) {
      j3pAffiche(stor.lesdiv.explications, null, 'Pas de crochets dans le nom d’un triangle !\n')
      yaexplik = true
    }
    if (stor.EcrTriPar) {
      j3pAffiche(stor.lesdiv.explications, null, 'Pas de parenthèses dans le nom d’un triangle !\n')
      yaexplik = true
    }
    if (stor.EcrTriChap) {
      j3pAffiche(stor.lesdiv.explications, null, 'Pas de chapeau dans le nom d’un triangle !\n')
      yaexplik = true
    }
    if (stor.ErrTriNom1) {
      j3pAffiche(stor.lesdiv.explications, null, 'Tu ne travailles pas dans le bon triangle !\n')
      yaexplik = true
    }
    if (stor.ErrTriNom2) {
      j3pAffiche(stor.lesdiv.explications, null, 'Ce triangle n’existe pas !\n')
      yaexplik = true
    }
    if (stor.EcrTriDoub) {
      j3pAffiche(stor.lesdiv.explications, null, 'Tu écris deux fois la même lettre !\n')
      yaexplik = true
    }
    if (stor.errPointMaj || stor.EcrTriMaj) {
      j3pAffiche(stor.lesdiv.explications, null, "Le nom d’un point s'écrit en majuscule !\n")
      yaexplik = true
    }
    if (stor.errPointIcon) {
      j3pAffiche(stor.lesdiv.explications, null, 'Tu utilises une lettre qui ne fait pas partie des données !\n')
      yaexplik = true
    }
    if (stor.errPointNb) {
      j3pAffiche(stor.lesdiv.explications, null, 'Une seule lettre pour nommer un point !\n')
      yaexplik = true
    }
    if (stor.ErrSom) {
      j3pAffiche(stor.lesdiv.explications, null, 'Vérifie bien le sommet de l’angle droit !\n')
      yaexplik = true
    }
    if (!stor.errFormAngle && stor.errForm) {
      if (stor.Lexo.calcul === 'Angle') {
        switch (stor.Lexo.formule) {
          case 'sinus': bb = ' de l’hypoténuse et du côté opposé'
            break
          case 'cosinus': bb = ' de l’hypoténuse et du côté adjacent'
            break
          case 'tangente': bb = ' des côtés opposés et adjacents'
            break
        }
        j3pAffiche(stor.lesdiv.explications, null, 'Par rapport à l’angle cherché, les longueurs données sont celles \n' + bb + ', tu dois utiliser une autre formule !\n')
      } else {
        switch (stor.Lexo.formule) {
          case 'sinus': bb = ' l’hypoténuse et le côté opposé'
            break
          case 'cosinus': bb = ' l’hypoténuse et le côté adjacent'
            break
          case 'tangente': bb = ' les côtés opposés et adjacents'
            break
        }
        j3pAffiche(stor.lesdiv.explications, null, 'Par rapport à l’angle donné, la longueur recherchée et la longeur donnée \nsont ' + bb + ', tu dois donc utiliser une autre formule ! \n')
      }
      yaexplik = true
    }
    if (stor.errFormAngle) {
      j3pAffiche(stor.lesdiv.explications, null, 'Tu n’utilises pas le bon angle !\n')
      yaexplik = true
    }
    if (!stor.errFormAngle && !stor.errForm && stor.errFormNum) {
      j3pAffiche(stor.lesdiv.explications, null, 'Le numérateur est faux ! \n')
      yaexplik = true
    }
    if (!stor.errFormAngle && !stor.errForm && stor.errFormden) {
      j3pAffiche(stor.lesdiv.explications, null, 'Le dénominateur est faux ! \n')
      yaexplik = true
    }
    if (stor.errValAngle) {
      j3pAffiche(stor.lesdiv.explications, null, 'Tu dois remplacer l’angle par sa mesure ! \n')
      yaexplik = true
    }
    if (stor.errValLong) {
      j3pAffiche(stor.lesdiv.explications, null, 'Tu dois remplacer la longueur connue par sa mesure ! \n')
      yaexplik = true
    }
    if (stor.errValLong2) {
      j3pAffiche(stor.lesdiv.explications, null, 'Tu dois remplacer les longueurs connues par leur mesure ! \n')
      yaexplik = true
    }
    if (stor.errValLongNOM) {
      j3pAffiche(stor.lesdiv.explications, null, 'Tu ne peux pas remplacer une longeur inconnue par une mesure au hasard ! \n')
      yaexplik = true
    }
    if (stor.errOP || stor.errNUmLigne3) {
      j3pAffiche(stor.lesdiv.explications, null, 'Revois le produit en croix (en mettant 1 sous ' + stor.Lexo.formule + ') ! \n')
      yaexplik = true
    }
    if (stor.errPREZlign3) {
      j3pAffiche(stor.lesdiv.explications, null, 'Tu dois utiliser les deux valeurs données ! \n')
      yaexplik = true
    }
    if (stor.errEgal) {
      j3pAffiche(stor.lesdiv.explications, null, 'On utilise $ \\approx $ pour les valeurs approchées ! \n')
      yaexplik = true
    }
    if (stor.errArrond) {
      j3pAffiche(stor.lesdiv.explications, null, 'Vérifie bien la valeur approchée !\n')
      yaexplik = true
    }
    if (stor.errRepCalcul) {
      j3pAffiche(stor.lesdiv.explications, null, 'Erreur de calcul !\n')
      yaexplik = true
    }
    if (stor.errUnit) {
      j3pAffiche(stor.lesdiv.explications, null, 'Erreur d’unité ! \n')
      yaexplik = true
    }
    if (stor.errcos1) {
      j3pAffiche(stor.lesdiv.explications, null, 'La fonction qui permet de retrouver la mesure d’un angle  \nà partir de son ' + stor.Lexo.formule + ' est $' + stor.Lexo.formule.substring(0, 3) + '^{-1} $  (ou $ Arc' + stor.Lexo.formule.substring(0, 3) + '$) ')
      yaexplik = true
    }
    if (stor.ErrAngUnit) {
      j3pAffiche(stor.lesdiv.explications, null, 'La mesure d’un angle s’exprime en degrés !\n')
      yaexplik = true
    }

    if (isFin) {
      stor.lesdiv.solution.innerHTML = ''
      let v1
      let v2
      let bufap
      let opts
      if (stor.etat === 'FORM') {
        j3pAffiche(stor.lesdiv.solution, null, 'Dans le triangle ' + stor.nomtri + ' rectangle en ' + stor.nomsom + ',')
        // opts.id = 'FORMCO'
        stor.T10 = addDefaultTable(stor.lesdiv.solution, 1, 3)
        modif(stor.T10)
        opts = stor.Lexo.formule.substring(0, 3) + ' $(\\widehat{' + stor.A + '})$'
        j3pAffiche(stor.T10[0][0], null, opts)
        j3pAffiche(stor.T10[0][1], null, ' $ = $ ')
        stor.divForm = afficheFrac(stor.T10[0][2], false)
        j3pAffiche(stor.divForm[0], null, stor.L1)
        j3pAffiche(stor.divForm[2], null, stor.L2)
        if (ds.Calcul !== 'Formule') {
          stor.zoneForm.barreIfKo()
          stor.zoneForm.disable()
        }
        stor.zoneLnum.barreIfKo()
        stor.zoneLnum.disable()
        stor.zoneLden.barreIfKo()
        stor.zoneLden.disable()
        if (ds.Description) {
          stor.zoneDesc.barreIfKo()
          stor.zoneDesc.disable()
          stor.zoneDesc2.barreIfKo()
          stor.zoneDesc2.disable()
        }
        stor.lesdiv.brouillon.innerHTML = ''
      }
      if (stor.etat === 'FORM2') {
        // opts.id = 'FORMCO'
        stor.T11 = addDefaultTable(stor.lesdiv.solution, 1, 3)
        modif(stor.T11)
        j3pAffiche(stor.T11[0][1], null, ' $ = $ ')
        stor.divForm = afficheFrac(stor.T11[0][2], false)
        if (stor.Lexo.calcul === 'Longueur1') {
          opts = stor.Lexo.formule.substring(0, 3) + ' (' + stor.Ma + ')'
          j3pAffiche(stor.T11[0][0], null, opts)
          v1 = stor.L1
          v2 = String(stor.lL2).replace('.', ',')
        }
        if (stor.Lexo.calcul === 'Longueur2') {
          opts = stor.Lexo.formule.substring(0, 3) + ' (' + stor.Ma + ')'
          j3pAffiche(stor.T11[0][0], null, opts)
          v2 = stor.L2
          v1 = String(stor.lL1).replace('.', ',')
        }
        if (stor.Lexo.calcul === 'Angle') {
          opts = stor.Lexo.formule.substring(0, 3) + ' $(\\widehat{' + stor.A + '})$'
          j3pAffiche(stor.T11[0][0], null, opts)
          v1 = String(stor.lL1).replace('.', ',')
          v2 = String(stor.lL2).replace('.', ',')
        }
        j3pAffiche(stor.divForm[0], null, '$' + v1 + '$')
        j3pAffiche(stor.divForm[2], null, '$' + v2 + '$')
        stor.zoneLnum2.barreIfKo()
        stor.zoneLnum2.disable()
        stor.zoneLden2.barreIfKo()
        stor.zoneLden2.disable()
        if (stor.Lexo.calcul !== 'Angle') {
          stor.zoneForm2.barreIfKo()
          stor.zoneForm2.disable()
        }
      }
      if (stor.etat === 'CALCUL1') {
        stor.T12 = addDefaultTable(stor.lesdiv.solution, 1, 3)
        modif(stor.T12)
        j3pAffiche(stor.T12[0][1], null, ' $ = $ ')
        if (stor.Lexo.calcul !== 'Angle') {
          j3pAffiche(stor.T12[0][0], null, ' $ ' + stor.ch + ' $ ')
          if (stor.Lexo.calcul === 'Longueur1') {
            opts = affichefois(stor.T12[0][2], false)
            v1 = String(stor.lL2).replace('.', ',')
          } else {
            opts = afficheFrac(stor.T12[0][2], false)
            v1 = String(stor.lL1).replace('.', ',')
          }
          j3pAffiche(opts[0], null, ' $ ' + v1 + ' $ ')
          j3pAffiche(opts[2], null, stor.Lexo.formule.substring(0, 3) + ' (' + stor.Ma + ') ')
          stor.zoneC2.barreIfKo()
          stor.zoneC2.disable()
          stor.T5[0][3].innerHTML = ''
          if (stor.errOP) j3pBarre(stor.T5[0][2])
        } else {
          j3pAffiche(stor.T12[0][0], null, '$\\widehat{' + stor.ch + '}$')
          // opts.id = 'FORMCOCO'
          stor.T13 = addDefaultTable(stor.T12[0][2], 1, 3)
          modif(stor.T13)
          j3pAffiche(stor.T13[0][0], null, ' $ ' + stor.Lexo.formule.substring(0, 3) + '^{-1}   (\\frac{' + String(stor.lL1).replace('.', ',') + '}{' + String(stor.lL2).replace('.', ',') + '}) $')
        }
        stor.zoneC1.barreIfKo()
        stor.zoneC1.disable()
      }
      if (stor.etat === 'CONCL') {
        stor.zoneEgale.barreIfKo()
        stor.zoneEgale.disable()
        stor.zoneunite.barreIfKo()
        stor.zoneunite.disable()
        stor.zoneRep.barreIfKo()
        stor.zoneRep.disable()
        if (stor.Lexo.calcul === 'Angle') {
          // opts.id = 'ConCLZ'
          stor.T14 = addDefaultTable(stor.lesdiv.solution, 1, 2)
          modif(stor.T14)
          j3pAffiche(stor.T14[0][0], null, '$\\widehat{' + stor.ch + '}$')
          v1 = j3pArrondi(stor.ArcFOFORMULE(stor.lL1 / stor.lL2) * 180 / Math.PI, stor.Lexo.arrond)
          v2 = stor.ArcFOFORMULE(stor.lL1 / stor.lL2) * 180 / Math.PI
          bufap = '\\approx'
          if (v1 === v2) bufap = '='
          j3pAffiche(stor.T14[0][1], null, ' $ ' + bufap + String(v1).replace('.', ',') + ' ° $')
        } else {
          if (stor.Lexo.calcul === 'Longueur1') {
            v1 = j3pArrondi(stor.lL2 * stor.FOFORMULE(stor.Ma * Math.PI / 180), stor.Lexo.arrond)
            v2 = stor.lL2 * stor.FOFORMULE(stor.Ma * Math.PI / 180)
          } else {
            v1 = j3pArrondi(stor.lL1 / stor.FOFORMULE(stor.Ma * Math.PI / 180), stor.Lexo.arrond)
            v2 = stor.lL1 / stor.FOFORMULE(stor.Ma * Math.PI / 180)
          }
          bufap = '\\approx'
          if (v1 === v2) bufap = '='
          j3pAffiche(stor.lesdiv.solution, 'hjkCOlfdsqdsotezr', ' $ ' + stor.ch + ' ' + bufap + ' $ ' + String(v1).replace('.', ',') + ' cm')
        }
      }
      stor.lesdiv.solution.classList.add('correction')
    }
    if (yaexplik) {
      stor.lesdiv.explications.classList.add('explique')
    }
  }
  function brouill () {
    stor.lesdiv.brouillon.innerHTML = ''
    Paint.create(stor.lesdiv.brouillon, { width: 400, height: 150, textes: stor.lettres })
  }

  function enonceMain () {
    if (((me.questionCourante % ds.nbetapes) === 1) || (ds.nbetapes === 1)) {
      me.videLesZones()
    }

    creeLesDiv()
    stor.Lexo = faisChoixExos()
    stor.etat = 'FORM'
    stor.LOP = 'z'
    stor.Finish = false
    stor.leschances = ds.nbchancesInt
    faisLaFigure()
    poseQuestion()
    if (ds.theme === 'zonesAvecImageDeFond') {
      stor.lesdiv.travail.classList.add('travail')
      stor.lesdiv.consigne.classList.add('enonce')
      stor.lesdiv.figure.classList.add('enonce')
    } else {
      if (stor.Lexo.type === 'Texte') {
        stor.lesdiv.travail.style.border = '1px solid'
      }
    }
    me.fenetresjq = [{ name: 'Calculatrice', title: 'Une calculatrice', left: 600, top: 10 }]
    j3pCreeFenetres(me)
    j3pAfficheCroixFenetres('Calculatrice')
    stor.lesdiv.solution.style.color = me.styles.cbien
    stor.lesdiv.explications.style.color = me.styles.cfaux
    if (ds.Brouillon && stor.Lexo.type !== 'Figure') {
      j3pAjouteBouton(stor.lesdiv.brouillon, brouill, { className: 'MepBoutons', value: 'Utiliser un brouillon' })
    }
    me.finEnonce()
  }

  switch (this.etat) {
    case 'enonce':
      // code exécuté au lancement de la section
      if (me.debutDeLaSection) {
        initSection()
      } else {
        enonceMain()
      }
      break // case "enonce":

    case 'correction':
      // On teste si une réponse a été saisie
      stor.lesdiv.explications.innerHTML = ''
      stor.lesdiv.correction.innerHTML = ''

      if (!yaReponse()) {
        // pas de réponse
        stor.lesdiv.correction.style.color = this.styles.cfaux
        stor.lesdiv.correction.innerHTML = reponseIncomplete
        // on reste dans l’état correction
        return this.finCorrection()
      }

      // on a une réponse
      if (!stor.Finish) {
        // mais c’est pas fini
        gereEtat(isRepOkInt())
        return this.finCorrection()
      }
      if (isRepOk()) {
        // Bonne réponse
        this.score++
        stor.lesdiv.correction.style.color = this.styles.cbien
        stor.lesdiv.correction.innerHTML = cBien
        this.typederreurs[0]++
        return this.finCorrection('navigation', true)
      }
      // Pas de bonne réponse
      stor.lesdiv.correction.style.color = this.styles.cfaux
      stor.lesdiv.correction.innerHTML = cFaux

      // Réponse fausse sans pb de temps
      if (this.essaiCourant < ds.nbchances) {
        stor.lesdiv.correction.innerHTML += '<br>' + essaieEncore
        affCorrFaux(false)
        // affCorrFaux.call(this, false)
        this.typederreurs[1]++
        return this.finCorrection()
      }

      // Erreur au dernier essai
      affCorrFaux(true)
      this.typederreurs[2]++
      // on donne la moitié des points pour une erreur d’arrondi
      if (stor.errArrond) this.score += 0.5
      this.finCorrection('navigation', true)
      break // case "correction":

    case 'navigation':
      if (!this.sectionTerminee()) {
        this.etat = 'enonce'
      }
      this.finNavigation()
      break // case "navigation":
  }
}
