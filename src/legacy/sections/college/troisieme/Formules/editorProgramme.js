import { j3pAddContent, j3pAddElt, j3pAjouteBouton, j3pAjouteCaseCoche, j3pDetruit, j3pEmpty } from 'src/legacy/core/functions'
import loadMq from 'src/lib/mathquill/loadMathquill'
import { j3pModale2 } from 'src/legacy/outils/zoneStyleMathquill/functions'
import ListeDeroulante from 'src/legacy/outils/listeDeroulante/ListeDeroulante'
import { addDefaultTable } from 'src/legacy/themes/table'
import './formule02.scss'

export default function editorProgramme (container, values) {
  console.debug('editorProgramme avec les valeurs initiales', values)
  const editor = {}
  const valuesDeb = values
  function creeLesDiv () {
    editor.conteneur = container
    // editor.conteneur.style.background = '#aaa'
    editor.conteneur.style.margin = '10px'
    editor.divUp = j3pAddElt(editor.conteneur, 'div')
    editor.divExo = j3pAddElt(editor.conteneur, 'div')
    editor.divExo.style.border = '5px solid black'
    editor.divExo.style.background = '#ccccff'
    editor.divExo.style.borderRadius = '10px'
    editor.divUp.style.border = '5px solid black'
    editor.divUp.style.background = '#b3efa2'
    editor.divUp.style.borderRadius = '10px'
    editor.divUp.style.padding = '10px'
    editor.divExo.style.padding = '10px'
    menuTotal()
    menuExo()
  }
  function menuTotal () {
    const yuyu = j3pAddElt(editor.divUp, 'span')
    j3pAddContent(yuyu, '<b><u>Titre de la ressource:</u></b>&nbsp;')
    editor.inputTitre = j3pAddElt(yuyu, 'input', '', {
      value: valuesDeb.Liste.titre,
      size: 50
    })
    j3pAddContent(editor.divUp, '\n')
    j3pAddContent(editor.divUp, '\n')

    const ldOptions2 = { dansModale: true, speMod: 10, speMod2: 70, j3pCont: container, choix0: true }

    const yu2 = j3pAddElt(editor.divUp, 'span')
    j3pAddContent(yu2, '<b><u>Nombre d’exercices proposées</u></b>:&nbsp;')
    editor.nbrepetitions = j3pAddElt(editor.divUp, 'input', '', {
      type: 'number',
      value: valuesDeb.nbrepetitions,
      size: 4,
      min: '1',
      max: '10'
    })
    editor.nbrepetitions.addEventListener('change', () => {
      const averif = editor.nbrepetitions.value
      if (averif === '' || isNaN(averif) || (averif < 1) || (averif > 10)) editor.nbrepetitions.value = '1'
    })
    j3pAddContent(editor.divUp, '\n')
    j3pAddContent(editor.divUp, '\n')

    const yu99 = j3pAddElt(editor.divUp, 'span')
    j3pAddContent(editor.divUp, '\n')
    j3pAddContent(editor.divUp, '\n')
    editor.yu99Ap = j3pAddElt(editor.divUp, 'span')
    j3pAddContent(yu99, '<b><u>Approximation décimale demandée:</u></b>&nbsp;')
    const ll99 = ['Exces_Defaut', 'Exces_Defaut_Arrondi', 'Arrondi', 'indéfini', 'Aucune']
    const ldOptions3 = { dansModale: true, speMod: 10, speMod2: 70, j3pCont: container, choix0: true, select: ll99.indexOf(valuesDeb.ApproximationDecimale), onChange: faisAprrox }
    editor.approx = ListeDeroulante.create(yu99, ll99, ldOptions3)

    editor.yu99Ap.style.display = (editor.approx.reponse !== 'Aucune') ? '' : 'none'
    j3pAddContent(editor.yu99Ap, '<b><u>Remplace "Approximation décimale" par "Valeur approchée":</u></b>&nbsp;')
    const ll9914A = (valuesDeb.ValeurApprochee) ? ['Oui', 'Non'] : ['Non', 'Oui']
    editor.approx2 = ListeDeroulante.create(editor.yu99Ap, ll9914A, ldOptions2)
    j3pAddContent(editor.yu99Ap, '\n')
    j3pAddContent(editor.yu99Ap, '\n')

    const yu9941 = j3pAddElt(editor.divUp, 'span')
    j3pAddContent(yu9941, '<b><u>Changement d\'unité:</u></b>&nbsp;')
    const ll9914 = (valuesDeb.ChangementUnite) ? ['Oui', 'Non'] : ['Non', 'Oui']
    editor.changementUnit = ListeDeroulante.create(yu9941, ll9914, ldOptions2)
    j3pAddContent(editor.divUp, '\n')
    j3pAddContent(editor.divUp, '\n')
    const yu99441 = j3pAddElt(editor.divUp, 'span')
    yu99441.style.display = (valuesDeb.ChangementUnite) ? '' : 'none'
    editor.changementUnit.onChange = () => {
      yu99441.style.display = (editor.changementUnit.reponse === 'Oui') ? '' : 'none'
    }

    j3pAddContent(yu99441, '<b><u>Tableau de conversion disponible:</u></b>&nbsp;')
    const ll99214 = (valuesDeb.TableauxConversion === 'Oui') ? ['Oui', 'Non'] : ['Non', 'Oui']
    editor.tabl = ListeDeroulante.create(yu99441, ll99214, ldOptions2)
    j3pAddContent(yu99441, '\n')
    j3pAddContent(yu99441, '\n')

    const yu9G941 = j3pAddElt(editor.divUp, 'span')
    j3pAddContent(yu9G941, '<b><u>Petites valeurs pour calcul mental:</u></b>&nbsp;')
    const ll991F4 = (valuesDeb.CalculMental) ? ['Oui', 'Non'] : ['Non', 'Oui']
    editor.calculL = ListeDeroulante.create(yu9G941, ll991F4, ldOptions2)
    j3pAddContent(editor.divUp, '\n')
    j3pAddContent(editor.divUp, '\n')

    const yu9GNo941 = j3pAddElt(editor.divUp, 'span')
    j3pAddContent(yu9GNo941, '<b><u>Notation de <i><b>aire de ABCD</b></i>:</u></b>&nbsp;')
    const ll99r1F4 = ['$A_{ABCD}$', '$A(ABCD)$', '$Aire_{ABCD}$', '$Aire(ABCD)$']
    const ldOptions4 = { dansModale: true, speMod: 10, speMod2: 70, j3pCont: container, choix0: true, select: ['IndiceCourt', 'FonctionCourt', 'IndiceLong', 'FonctionLong'].indexOf(valuesDeb.NotationGrandeur) }
    editor.notage = ListeDeroulante.create(yu9GNo941, ll99r1F4, ldOptions4)
    j3pAddContent(editor.divUp, '\n')
    j3pAddContent(editor.divUp, '\n')

    const yu991 = j3pAddElt(editor.divUp, 'span')
    j3pAddContent(yu991, '<b><u>Theme:</u></b>&nbsp;')
    const ll991 = (valuesDeb.theme === 'standard') ? ['standard', 'zonesAvecImageDeFond'] : ['zonesAvecImageDeFond', 'standard']
    editor.themeL = ListeDeroulante.create(yu991, ll991, ldOptions2)
    j3pAddContent(editor.divUp, '\n')
    j3pAddContent(editor.divUp, '\n')
  }
  function faisAprrox () {
    editor.yu99Ap.style.display = (editor.approx.reponse !== 'Aucune') ? '' : 'none'
  }
  function verifAutNiv () {
    for (let i = 0; i < editor.listaChek.length; i++) {
      editor.listaChek[i].case.disabled = false
      editor.listaChek[i].case.title = ''
      editor.listaChek[i].aColorie.style.background = editor.listaChek[i].aColorie.def
      switch (editor.listaChek[i].niv) {
        case 6: if (!editor.aut6.checked) {
          editor.listaChek[i].case.disabled = true
          editor.listaChek[i].case.title = 'niveau 6e'
          editor.listaChek[i].aColorie.style.background = '#aaa'
        }
          break
        case 5: if (!editor.aut5.checked) {
          editor.listaChek[i].case.disabled = true
          editor.listaChek[i].aColorie.style.background = '#aaa'
          editor.listaChek[i].case.title = 'niveau 5e'
        }
          break
        case 4: if (!editor.aut4.checked) {
          editor.listaChek[i].case.disabled = true
          editor.listaChek[i].aColorie.style.background = '#aaa'
          editor.listaChek[i].case.title = 'niveau 4e'
        }
          break
        case 3: if (!editor.aut3.checked) {
          editor.listaChek[i].case.disabled = true
          editor.listaChek[i].aColorie.style.background = '#aaa'
          editor.listaChek[i].case.title = 'niveau 3e'
        }
          break
      }
      if (editor.listaChek[i].optionnel) {
        if (editor.yaOpt.reponse === 'Non') {
          editor.listaChek[i].case.disabled = true
          editor.listaChek[i].aColorie.style.background = '#aaa'
          if (editor.listaChek[i].case.title === '') {
            editor.listaChek[i].case.title = 'optionnel'
          } else {
            editor.listaChek[i].case.title += ' -- optionnel'
          }
        }
      }
      if (editor.listaChek[i].exp) {
        if (editor.yaExp.reponse === 'Non') {
          editor.listaChek[i].case.disabled = true
          editor.listaChek[i].aColorie.style.background = '#aaa'
          if (editor.listaChek[i].case.title === '') {
            editor.listaChek[i].case.title = 'notation expert'
          } else {
            editor.listaChek[i].case.title += ' -- notation expert'
          }
        }
      }
    }
  }
  function menuExo () {
    editor.listaChek = []
    j3pEmpty(editor.divExo)
    const yu9913 = j3pAddElt(editor.divExo, 'span')
    const tabNiv = addDefaultTable(yu9913, 2, 5)
    j3pAddContent(tabNiv[0][0], '<b><u>Niveaux Autorisés:</u></b>&nbsp;')
    j3pAddContent(tabNiv[0][1], '&nbsp;6e&nbsp;')
    j3pAddContent(tabNiv[0][2], '&nbsp;5e&nbsp;')
    j3pAddContent(tabNiv[0][3], '&nbsp;4e&nbsp;')
    j3pAddContent(tabNiv[0][4], '&nbsp;3e&nbsp;')
    tabNiv[0][1].style.textAlign = 'center'
    tabNiv[0][2].style.textAlign = 'center'
    tabNiv[0][3].style.textAlign = 'center'
    tabNiv[0][4].style.textAlign = 'center'
    editor.aut6 = j3pAjouteCaseCoche(tabNiv[1][1], { label: '' })
    editor.aut6.checked = values.niveau6e
    editor.aut5 = j3pAjouteCaseCoche(tabNiv[1][2], { label: '' })
    editor.aut5.checked = values.niveau5e
    editor.aut4 = j3pAjouteCaseCoche(tabNiv[1][3], { label: '' })
    editor.aut4.checked = values.niveau4e
    editor.aut3 = j3pAjouteCaseCoche(tabNiv[1][4], { label: '' })
    editor.aut3.checked = values.niveau3e
    editor.aut6.style.cursor = 'pointer'
    editor.aut5.style.cursor = 'pointer'
    editor.aut4.style.cursor = 'pointer'
    editor.aut3.style.cursor = 'pointer'
    editor.aut6.addEventListener('change', verifAutNiv)
    editor.aut5.addEventListener('change', verifAutNiv)
    editor.aut4.addEventListener('change', verifAutNiv)
    editor.aut3.addEventListener('change', verifAutNiv)
    j3pAddContent(editor.divExo, '\n')
    j3pAddContent(editor.divExo, '\n')
    editor.yu9G941E = j3pAddElt(editor.divExo, 'span')

    const ldOptions2Opt = { dansModale: true, speMod: 10, speMod2: 70, j3pCont: container, choix0: true, onChange: verifAutNiv }
    const yu9G941 = j3pAddElt(editor.divExo, 'span')
    j3pAddContent(yu9G941, '<b><u>Formules non exigibles ( hors prog ) possibles:</u></b>&nbsp;')
    const ll991F4 = (valuesDeb.Optionnel) ? ['Oui', 'Non'] : ['Non', 'Oui']
    editor.yaOpt = ListeDeroulante.create(yu9G941, ll991F4, ldOptions2Opt)
    j3pAddContent(editor.divExo, '\n')
    j3pAddContent(editor.divExo, '\n')

    j3pAddContent(editor.yu9G941E, '<b><u>Formules 6e avec Notation expert autorisées:</u></b>&nbsp;')
    const ll991F4E = (valuesDeb.NotationExpert) ? ['Oui', 'Non'] : ['Non', 'Oui']
    editor.yaExp = ListeDeroulante.create(editor.yu9G941E, ll991F4E, ldOptions2Opt)
    j3pAddContent(editor.yu9G941E, '\n')
    j3pAddContent(editor.yu9G941E, '\n')

    if (valuesDeb.Liste.liste.Cercle_P === undefined) {
      // on la crée à partir des params de base
      valuesDeb.Liste.liste.Cercle_P = valuesDeb.Cercle && values.perimetre
      valuesDeb.Liste.liste.Cercle_A = valuesDeb.Cercle && values.aire
      valuesDeb.Liste.liste.Carre_P = valuesDeb.Carre && values.perimetre
      valuesDeb.Liste.liste.Carre_A = valuesDeb.Carre && values.aire
      valuesDeb.Liste.liste.Quadrilatere_P = valuesDeb.Quadrilatere && values.perimetre
      valuesDeb.Liste.liste.Rectangle_P = valuesDeb.Rectangle && values.perimetre
      valuesDeb.Liste.liste.Rectangle_A = valuesDeb.Rectangle && values.aire
      valuesDeb.Liste.liste.Losange_P = valuesDeb.Losange && values.perimetre
      valuesDeb.Liste.liste.Losange_A = valuesDeb.Losange && values.aire
      valuesDeb.Liste.liste.Parallelogramme_P = valuesDeb.Parallelogramme && values.perimetre
      valuesDeb.Liste.liste.Parallelogramme_A = valuesDeb.Parallelogramme && values.aire
      valuesDeb.Liste.liste.TriangleRectangle_A = valuesDeb.TriangleRectangle && values.aire
      valuesDeb.Liste.liste.TriangleQuelconque_P = valuesDeb.TriangleQuelconque && values.perimetre
      valuesDeb.Liste.liste.TriangleQuelconque_A = valuesDeb.TriangleQuelconque && values.aire
      valuesDeb.Liste.liste.TriangleEquilateral_P = valuesDeb.TriangleEquilateral && values.perimetre
      valuesDeb.Liste.liste.TriangleEquilateral_A = valuesDeb.TriangleEquilateral && values.aire
      valuesDeb.Liste.liste.Secteur_A = valuesDeb.Secteur && values.aire
      valuesDeb.Liste.liste.Secteur_P = valuesDeb.Secteur && values.perimetre
      valuesDeb.Liste.liste.Cube_P = valuesDeb.Cube && values.perimetre
      valuesDeb.Liste.liste.Cube_A = valuesDeb.Cube && values.aire
      valuesDeb.Liste.liste.Cube_V = valuesDeb.Cube && values.volume
      valuesDeb.Liste.liste.ParallelepipedeRectangle_P = valuesDeb.ParallelepipedeRectangle && values.perimetre
      valuesDeb.Liste.liste.ParallelepipedeRectangle_A = valuesDeb.ParallelepipedeRectangle && values.aire
      valuesDeb.Liste.liste.ParallelepipedeRectangle_V = valuesDeb.ParallelepipedeRectangle && values.volume
      valuesDeb.Liste.liste.Cylindre_ALat = valuesDeb.Cylindre && values.aireLaterale
      valuesDeb.Liste.liste.Cylindre_A = valuesDeb.Cylindre && values.aire
      valuesDeb.Liste.liste.Cylindre_V = valuesDeb.Cylindre && values.volume
      valuesDeb.Liste.liste.Cylindre_P = valuesDeb.Cylindre && values.perimetre
      valuesDeb.Liste.liste.ConeDeRevolution_V = valuesDeb.ConeDeRevolution && values.volume
      valuesDeb.Liste.liste.ConeDeRevolution_P = valuesDeb.ConeDeRevolution && values.perimetre
      valuesDeb.Liste.liste.Sphere_A = valuesDeb.Sphere && values.aire
      valuesDeb.Liste.liste.Sphere_V = valuesDeb.Sphere && values.volume
      valuesDeb.Liste.liste.Pyramide_V = valuesDeb.Pyramide && values.volume
      valuesDeb.Liste.liste.PrismeDroit_A = valuesDeb.PrismeDroit && values.aire
      valuesDeb.Liste.liste.PrismeDroit_ALat = valuesDeb.PrismeDroit && values.aireLaterale
      valuesDeb.Liste.liste.PrismeDroit_V = valuesDeb.PrismeDroit && values.volume
    } else {
      // on ladapte si y'a eu des changements en dure sur les params
      // si il retire un param
      valuesDeb.Liste.liste.Cercle_P = valuesDeb.Liste.liste.Cercle_P && valuesDeb.Cercle && values.perimetre
      valuesDeb.Liste.liste.Cercle_A = valuesDeb.Liste.liste.Cercle_A && valuesDeb.Cercle && values.aire
      valuesDeb.Liste.liste.Carre_P = valuesDeb.Liste.liste.Carre_P && valuesDeb.Carre && values.perimetre
      valuesDeb.Liste.liste.Carre_A = valuesDeb.Liste.liste.Carre_A && valuesDeb.Carre && values.aire
      valuesDeb.Liste.liste.Quadrilatere_P = valuesDeb.Liste.liste.Quadrilatere_P && valuesDeb.Quadrilatere && values.perimetre
      valuesDeb.Liste.liste.Rectangle_P = valuesDeb.Liste.liste.Rectangle_P && valuesDeb.Rectangle && values.perimetre
      valuesDeb.Liste.liste.Rectangle_A = valuesDeb.Liste.liste.Rectangle_A && valuesDeb.Rectangle && values.aire
      valuesDeb.Liste.liste.Losange_P = valuesDeb.Liste.liste.Losange_P && valuesDeb.Losange && values.perimetre
      valuesDeb.Liste.liste.Losange_A = valuesDeb.Liste.liste.Losange_A && valuesDeb.Losange && values.aire
      valuesDeb.Liste.liste.Parallelogramme_P = valuesDeb.Liste.liste.Parallelogramme_P && valuesDeb.Parallelogramme && values.perimetre
      valuesDeb.Liste.liste.Parallelogramme_A = valuesDeb.Liste.liste.Parallelogramme_A && valuesDeb.Parallelogramme && values.aire
      valuesDeb.Liste.liste.TriangleRectangle_A = valuesDeb.Liste.liste.TriangleRectangle_A && valuesDeb.TriangleRectangle && values.aire
      valuesDeb.Liste.liste.TriangleQuelconque_P = valuesDeb.Liste.liste.TriangleQuelconque_P && valuesDeb.TriangleQuelconque && values.perimetre
      valuesDeb.Liste.liste.TriangleQuelconque_A = valuesDeb.Liste.liste.TriangleQuelconque_A && valuesDeb.TriangleQuelconque && values.aire
      valuesDeb.Liste.liste.TriangleEquilateral_P = valuesDeb.Liste.liste.TriangleEquilateral_P && valuesDeb.TriangleEquilateral && values.perimetre
      valuesDeb.Liste.liste.TriangleEquilateral_A = valuesDeb.Liste.liste.TriangleEquilateral_A && valuesDeb.TriangleEquilateral && values.aire
      valuesDeb.Liste.liste.Secteur_A = valuesDeb.Liste.liste.Secteur_A && valuesDeb.Secteur && values.aire
      valuesDeb.Liste.liste.Secteur_P = valuesDeb.Liste.liste.Secteur_P && valuesDeb.Secteur && values.perimetre
      valuesDeb.Liste.liste.Cube_P = valuesDeb.Liste.liste.Cube_P && valuesDeb.Cube && values.perimetre
      valuesDeb.Liste.liste.Cube_A = valuesDeb.Liste.liste.Cube_A && valuesDeb.Cube && values.aire
      valuesDeb.Liste.liste.Cube_V = valuesDeb.Liste.liste.Cube_V && valuesDeb.Cube && values.volume
      valuesDeb.Liste.liste.ParallelepipedeRectangle_P = valuesDeb.Liste.liste.ParallelepipedeRectangle_P && valuesDeb.ParallelepipedeRectangle && values.perimetre
      valuesDeb.Liste.liste.ParallelepipedeRectangle_A = valuesDeb.Liste.liste.ParallelepipedeRectangle_A && valuesDeb.ParallelepipedeRectangle && values.aire
      valuesDeb.Liste.liste.ParallelepipedeRectangle_V = valuesDeb.Liste.liste.ParallelepipedeRectangle_V && valuesDeb.ParallelepipedeRectangle && values.volume
      valuesDeb.Liste.liste.Cylindre_ALat = valuesDeb.Liste.liste.Cylindre_ALat && valuesDeb.Cylindre && values.aireLaterale
      valuesDeb.Liste.liste.Cylindre_A = valuesDeb.Liste.liste.Cylindre_A && valuesDeb.Cylindre && values.aire
      valuesDeb.Liste.liste.Cylindre_V = valuesDeb.Liste.liste.Cylindre_V && valuesDeb.Cylindre && values.volume
      valuesDeb.Liste.liste.Cylindre_P = valuesDeb.Liste.liste.Cylindre_P && valuesDeb.Cylindre && values.perimetre
      valuesDeb.Liste.liste.ConeDeRevolution_V = valuesDeb.Liste.liste.ConeDeRevolution_V && valuesDeb.ConeDeRevolution && values.volume
      valuesDeb.Liste.liste.ConeDeRevolution_P = valuesDeb.Liste.liste.ConeDeRevolution_P && valuesDeb.ConeDeRevolution && values.perimetre
      valuesDeb.Liste.liste.Sphere_A = valuesDeb.Liste.liste.Sphere_A && valuesDeb.Sphere && values.aire
      valuesDeb.Liste.liste.Sphere_V = valuesDeb.Liste.liste.Sphere_V && valuesDeb.Sphere && values.volume
      valuesDeb.Liste.liste.Pyramide_V = valuesDeb.Liste.liste.Pyramide_V && valuesDeb.Pyramide && values.volume
      valuesDeb.Liste.liste.PrismeDroit_A = valuesDeb.Liste.liste.PrismeDroit_A && valuesDeb.PrismeDroit && values.aire
      valuesDeb.Liste.liste.PrismeDroit_ALat = valuesDeb.Liste.liste.PrismeDroit_ALat && valuesDeb.PrismeDroit && values.aireLaterale
      valuesDeb.Liste.liste.PrismeDroit_V = valuesDeb.Liste.liste.PrismeDroit_V && valuesDeb.PrismeDroit && values.volume
      // si il ajoute un param
      if (valuesDeb.Cercle && !valuesDeb.Liste.liste.Cercle_P && !valuesDeb.Liste.liste.Cercle_A) {
        valuesDeb.Liste.liste.Cercle_P = values.perimetre
        valuesDeb.Liste.liste.Cercle_A = values.aire
      }
      if (valuesDeb.Carre && !valuesDeb.Liste.liste.Carre_P && !valuesDeb.Liste.liste.Carre_A) {
        valuesDeb.Liste.liste.Carre_P = values.perimetre
        valuesDeb.Liste.liste.Carre_A = values.aire
      }
      if (valuesDeb.Quadrilatere && !valuesDeb.Liste.liste.QUADRILATERE) {
        valuesDeb.Liste.liste.QUADRILATERE = values.perimetre
      }
      if (valuesDeb.Rectangle && !valuesDeb.Liste.liste.Rectangle_P && !valuesDeb.Liste.liste.Rectangle_A) {
        valuesDeb.Liste.liste.Rectangle_P = values.perimetre
        valuesDeb.Liste.liste.Rectangle_A = values.aire
      }
      if (valuesDeb.Losange && !valuesDeb.Liste.liste.Losange_P && !valuesDeb.Liste.liste.Losange_A) {
        valuesDeb.Liste.liste.Losange_P = values.perimetre
        valuesDeb.Liste.liste.Losange_A = values.aire
      }
      if (valuesDeb.Parallelogramme && !valuesDeb.Liste.liste.Parallelogramme_P && !valuesDeb.Liste.liste.Parallelogramme_A) {
        valuesDeb.Liste.liste.Parallelogramme_P = values.perimetre
        valuesDeb.Liste.liste.Parallelogramme_A = values.aire
      }
      if (valuesDeb.TriangleRectangle && !valuesDeb.Liste.liste.TriangleRectangle_A) {
        valuesDeb.Liste.liste.TriangleRectangle_A = values.aire
      }
      if (valuesDeb.TriangleQuelconque && !valuesDeb.Liste.liste.TriangleQuelconque_P && !valuesDeb.Liste.liste.TriangleQuelconque_A) {
        valuesDeb.Liste.liste.TriangleQuelconque_P = values.perimetre
        valuesDeb.Liste.liste.TriangleQuelconque_A = values.aire
      }
      if (valuesDeb.TriangleEquilateral && !valuesDeb.Liste.liste.TriangleEquilateral_P && !valuesDeb.Liste.liste.TriangleEquilateral_A) {
        valuesDeb.Liste.liste.TriangleEquilateral_P = values.perimetre
        valuesDeb.Liste.liste.TriangleEquilateral_A = values.aire
      }
      if (valuesDeb.Secteur && !valuesDeb.Liste.liste.Secteur_A && !valuesDeb.Liste.liste.Secteur_P) {
        valuesDeb.Liste.liste.Secteur_A = values.aire
        valuesDeb.Liste.liste.Secteur_P = values.perimetre
      }
      if (valuesDeb.Cube && !valuesDeb.Liste.liste.Cube_P && !valuesDeb.Liste.liste.Cube_A && !valuesDeb.Liste.liste.Cube_V) {
        valuesDeb.Liste.liste.Cube_P = values.perimetre
        valuesDeb.Liste.liste.Cube_A = values.aire
        valuesDeb.Liste.liste.Cube_V = values.volume
      }
      if (valuesDeb.ParallelepipedeRectangle && !valuesDeb.Liste.liste.ParallelepipedeRectangle_P && !valuesDeb.Liste.liste.ParallelepipedeRectangle_A && !valuesDeb.Liste.liste.ParallelepipedeRectangle_V) {
        valuesDeb.Liste.liste.ParallelepipedeRectangle_P = values.perimetre
        valuesDeb.Liste.liste.ParallelepipedeRectangle_A = values.aire
        valuesDeb.Liste.liste.ParallelepipedeRectangle_V = values.volume
      }
      if (valuesDeb.Cylindre && !valuesDeb.Liste.liste.Cylindre_P && !valuesDeb.Liste.liste.Cylindre_A && !valuesDeb.Liste.liste.Cylindre_ALat && !valuesDeb.Liste.liste.Cylindre_V) {
        valuesDeb.Liste.liste.Cylindre_P = values.perimetre
        valuesDeb.Liste.liste.Cylindre_A = values.aire
        valuesDeb.Liste.liste.Cylindre_V = values.volume
        valuesDeb.Liste.liste.Cylindre_ALat = values.aireLaterale
      }
      if (valuesDeb.ConeDeRevolution && !valuesDeb.Liste.liste.ConeDeRevolution_P && !valuesDeb.Liste.liste.ConeDeRevolution_V) {
        valuesDeb.Liste.liste.ConeDeRevolution_P = values.perimetre
        valuesDeb.Liste.liste.ConeDeRevolution_V = values.volume
      }
      if (valuesDeb.Sphere && !valuesDeb.Liste.liste.Sphere_P && !valuesDeb.Liste.liste.Sphere_A && !valuesDeb.Liste.liste.Sphere_V) {
        valuesDeb.Liste.liste.Sphere_A = values.aire
        valuesDeb.Liste.liste.Sphere_V = values.volume
      }
      if (valuesDeb.Pyramide && !valuesDeb.Liste.liste.Pyramide_P && !valuesDeb.Liste.liste.Pyramide_A && !valuesDeb.Liste.liste.Pyramide_V) {
        valuesDeb.Liste.liste.Pyramide_V = values.volume
      }
      if (valuesDeb.PrismeDroit && !valuesDeb.Liste.liste.PrismeDroit_P && !valuesDeb.Liste.liste.PrismeDroit_A && !valuesDeb.Liste.liste.PrismeDroit_ALat && !valuesDeb.Liste.liste.PrismeDroit_V) {
        valuesDeb.Liste.liste.PrismeDroit_A = values.aire
        valuesDeb.Liste.liste.PrismeDroit_V = values.volume
        valuesDeb.Liste.liste.PrismeDroit_ALat = values.aireLaterale
      }
    }
    const tabDep = { acache: editor.divExo, caseCoche: { retour: [], retourFonc: [], adapte: () => {} } }

    const tabfigType = faisChoix(tabDep, ['Figures planes', 'Solides'])
    if (valuesDeb.Liste.liste.Cercle_P || valuesDeb.Liste.liste.Cercle_A || valuesDeb.Liste.liste.Carre_P || valuesDeb.Liste.liste.Carre_A || valuesDeb.Liste.liste.Quadrilatere_P || valuesDeb.Liste.liste.Rectangle_P || valuesDeb.Liste.liste.Rectangle_A || valuesDeb.Liste.liste.Losange_P || valuesDeb.Liste.liste.Losange_A || valuesDeb.Liste.liste.Parallelogramme_P || valuesDeb.Liste.liste.Parallelogramme_A || valuesDeb.Liste.liste.TriangleRectangle_A || valuesDeb.Liste.liste.TriangleQuelconque_P || valuesDeb.Liste.liste.TriangleQuelconque_A || valuesDeb.Liste.liste.TriangleEquilateral_P || valuesDeb.Liste.liste.TriangleEquilateral_A || valuesDeb.Liste.liste.Secteur_A || valuesDeb.Liste.liste.Secteur_P) {
      depliAuto(tabfigType, 0)
    }
    if (valuesDeb.Liste.liste.Cube_P || valuesDeb.Liste.liste.Cube_A || valuesDeb.Liste.liste.Cube_V || valuesDeb.Liste.liste.ParallelepipedeRectangle_P || valuesDeb.Liste.liste.ParallelepipedeRectangle_A || valuesDeb.Liste.liste.ParallelepipedeRectangle_V || valuesDeb.Liste.liste.Cylindre_ALat || valuesDeb.Liste.liste.Cylindre_A || valuesDeb.Liste.liste.Cylindre_V || valuesDeb.Liste.liste.Cylindre_P || valuesDeb.Liste.liste.ConeDeRevolution_V || valuesDeb.Liste.liste.ConeDeRevolution_P || valuesDeb.Liste.liste.Sphere_A || valuesDeb.Liste.liste.Sphere_V || valuesDeb.Liste.liste.Pyramide_V || valuesDeb.Liste.liste.PrismeDroit_A || valuesDeb.Liste.liste.PrismeDroit_ALat || valuesDeb.Liste.liste.PrismeDroit_V) {
      depliAuto(tabfigType, 1)
    }
    const tab2D = faisChoix(tabfigType[0], ['Quadrilatères', 'Triangles', 'Cercle', 'Secteur'])
    if (valuesDeb.Liste.liste.Carre_P || valuesDeb.Liste.liste.Carre_A || valuesDeb.Liste.liste.Quadrilatere_P || valuesDeb.Liste.liste.Rectangle_P || valuesDeb.Liste.liste.Rectangle_A || valuesDeb.Liste.liste.Losange_P || valuesDeb.Liste.liste.Losange_A || valuesDeb.Liste.liste.Parallelogramme_P || valuesDeb.Liste.liste.Parallelogramme_A) {
      depliAuto(tab2D, 0)
    }
    if (valuesDeb.Liste.liste.TriangleRectangle_A || valuesDeb.Liste.liste.TriangleQuelconque_P || valuesDeb.Liste.liste.TriangleQuelconque_A || valuesDeb.Liste.liste.TriangleEquilateral_P || valuesDeb.Liste.liste.TriangleEquilateral_A) {
      depliAuto(tab2D, 1)
    }
    if (valuesDeb.Liste.liste.Cercle_P || valuesDeb.Liste.liste.Cercle_A) {
      depliAuto(tab2D, 2)
    }
    if (valuesDeb.Liste.liste.Secteur_A || valuesDeb.Liste.liste.Secteur_P) {
      depliAuto(tab2D, 3)
    }
    const tabQuad = faisChoix(tab2D[0], ['Quadrilatère quelconque', 'Parallélogramme', 'Losange', 'Rectangle', 'Carré'])
    if (valuesDeb.Liste.liste.Quadrilatere_P) {
      depliAuto(tabQuad, 0)
    }
    if (valuesDeb.Liste.liste.Parallelogramme_P || valuesDeb.Liste.liste.Parallelogramme_A) {
      depliAuto(tabQuad, 1)
    }
    if (valuesDeb.Liste.liste.Losange_P || valuesDeb.Liste.liste.Losange_A) {
      depliAuto(tabQuad, 2)
    }
    if (valuesDeb.Liste.liste.Rectangle_P || valuesDeb.Liste.liste.Rectangle_A) {
      depliAuto(tabQuad, 3)
    }
    if (valuesDeb.Liste.liste.Carre_P || valuesDeb.Liste.liste.Carre_A) {
      depliAuto(tabQuad, 4)
    }
    const tabTri = faisChoix(tab2D[1], ['Triangle quelconque', 'Triangle rectangle', 'Triangle équilatéral'])
    if (valuesDeb.Liste.liste.TriangleQuelconque_P || valuesDeb.Liste.liste.TriangleQuelconque_A) {
      depliAuto(tabTri, 0)
    }
    if (valuesDeb.Liste.liste.TriangleRectangle_A) {
      depliAuto(tabTri, 1)
    }
    if (valuesDeb.Liste.liste.TriangleEquilateral_P || valuesDeb.Liste.liste.TriangleEquilateral_A) {
      depliAuto(tabTri, 2)
    }
    faisTabCase('Cercle', tab2D[2], { p: { exist: true, optionnel: false, niv: 6, exp: false }, a: { exist: true, optionnel: false, niv: 6, exp: false }, aLat: { exist: false }, vol: { exist: false } })
    faisTabCase('Secteur', tab2D[3], { p: { exist: true, optionnel: true, niv: 5, exp: true }, a: { exist: true, optionnel: false, niv: 5, exp: true }, aLat: { exist: false }, vol: { exist: false } })
    faisTabCase('QuadrilatereQuelconque', tabQuad[0], { p: { exist: true, optionnel: false, niv: 6, exp: false }, a: { exist: false }, aLat: { exist: false }, vol: { exist: false } })
    faisTabCase('Parallelogramme', tabQuad[1], { p: { exist: true, optionnel: false, niv: 5, exp: false }, a: { exist: true, optionnel: false, niv: 5, exp: true }, aLat: { exist: false }, vol: { exist: false } })
    faisTabCase('Losange', tabQuad[2], { p: { exist: true, optionnel: false, niv: 6, exp: false }, a: { exist: true, optionnel: true, niv: 6, exp: true }, aLat: { exist: false }, vol: { exist: false } })
    faisTabCase('Rectangle', tabQuad[3], { p: { exist: true, optionnel: false, niv: 6, exp: false }, a: { exist: true, optionnel: false, niv: 6, exp: false }, aLat: { exist: false }, vol: { exist: false } })
    faisTabCase('Carre', tabQuad[4], { p: { exist: true, optionnel: false, niv: 6, exp: false }, a: { exist: true, optionnel: false, niv: 6, exp: false }, aLat: { exist: false }, vol: { exist: false } })
    faisTabCase('TriangleQuelconque', tabTri[0], { p: { exist: true, optionnel: false, niv: 6, exp: false }, a: { exist: true, optionnel: false, niv: 5, exp: true }, aLat: { exist: false }, vol: { exist: false } })
    faisTabCase('TriangleRectangle', tabTri[1], { p: { exist: false }, a: { exist: true, optionnel: false, niv: 6, exp: false }, aLat: { exist: false }, vol: { exist: false } })
    faisTabCase('TriangleEquilateral', tabTri[2], { p: { exist: true, optionnel: false, niv: 6, exp: false }, a: { exist: false }, aLat: { exist: false }, vol: { exist: false } })
    const tabSol = faisChoix(tabfigType[1], ['Cube', 'Pavé droit', 'Prisme droit', 'Cylindre', 'Pyramide', 'Cône', 'Sphère'])
    if (valuesDeb.Liste.liste.Cube_P || valuesDeb.Liste.liste.Cube_A || valuesDeb.Liste.liste.Cube_V) {
      depliAuto(tabSol, 0)
    }
    if (valuesDeb.Liste.liste.ParallelepipedeRectangle_P || valuesDeb.Liste.liste.ParallelepipedeRectangle_A || valuesDeb.Liste.liste.ParallelepipedeRectangle_V) {
      depliAuto(tabSol, 1)
    }
    if (valuesDeb.Liste.liste.PrismeDroit_ALat || valuesDeb.Liste.liste.PrismeDroit_A || valuesDeb.Liste.liste.PrismeDroit_V) {
      depliAuto(tabSol, 2)
    }
    if (valuesDeb.Liste.liste.Cylindre_ALat || valuesDeb.Liste.liste.Cylindre_A || valuesDeb.Liste.liste.Cylindre_V || valuesDeb.Liste.liste.Cylindre_P) {
      depliAuto(tabSol, 3)
    }
    if (valuesDeb.Liste.liste.Pyramide_V) {
      depliAuto(tabSol, 4)
    }
    if (valuesDeb.Liste.liste.ConeDeRevolution_P || valuesDeb.Liste.liste.ConeDeRevolution_V) {
      depliAuto(tabSol, 5)
    }
    if (valuesDeb.Liste.liste.Sphere_V || valuesDeb.Liste.liste.Sphere_A) {
      depliAuto(tabSol, 6)
    }
    faisTabCase('Cube', tabSol[0], { p: { exist: true, optionnel: true, niv: 6, exp: false }, a: { exist: true, optionnel: true, niv: 6, exp: false }, aLat: { exist: false }, vol: { exist: true, optionnel: false, niv: 6, exp: false } })
    faisTabCase('ParallelepipedeRectangle', tabSol[1], { p: { exist: true, optionnel: true, niv: 6, exp: false }, a: { exist: true, optionnel: true, niv: 6, exp: false }, aLat: { exist: false }, vol: { exist: true, optionnel: false, niv: 6, exp: false } })
    faisTabCase('PrismeDroit', tabSol[2], { p: { exist: false }, a: { exist: true, optionnel: true, niv: 5, exp: false }, aLat: { exist: true, optionnel: false, niv: 5, exp: false }, vol: { exist: true, optionnel: false, niv: 5, exp: false } })
    faisTabCase('Cylindre', tabSol[3], { p: { exist: true, optionnel: true, niv: 5, exp: false }, a: { exist: true, optionnel: true, niv: 5, exp: false }, aLat: { exist: true, optionnel: false, niv: 5, exp: false }, vol: { exist: true, optionnel: false, niv: 5, exp: false } })
    faisTabCase('Pyramide', tabSol[4], { p: { exist: false }, a: { exist: false }, aLat: { exist: false }, vol: { exist: true, optionnel: false, niv: 4, exp: true } })
    faisTabCase('ConeDeRevolution', tabSol[5], { p: { exist: true, optionnel: true, niv: 4, exp: false }, a: { exist: false }, aLat: { exist: false }, vol: { exist: true, optionnel: false, niv: 4, exp: true } })
    faisTabCase('Sphere', tabSol[6], { p: { exist: false }, a: { exist: true, optionnel: false, niv: 3, exp: true }, aLat: { exist: false }, vol: { exist: true, optionnel: false, niv: 3, exp: true } })
    verifAutNiv()
  }
  function depliAuto (tab, num) {
    tab[num].acache.style.display = ''
    tab[num].acache2.style.display = ''
    tab[num].acache3.style.display = ''
    tab[num].bouton.classList.remove('fboutonPlus')
    tab[num].bouton.classList.add('fboutonMoins')
    tab[num].bouton.plus = true
  }
  function faisChoix (tabDep, tab) {
    const ou = tabDep.acache
    const tabTot = addDefaultTable(ou, tab.length * 2, 3)
    const arte = []
    for (let i = 0; i < tab.length; i++) {
      const acache = tabTot[i * 2 + 1][2]
      const acache2 = tabTot[i * 2 + 1][1]
      const acache3 = tabTot[i * 2 + 1][0]
      const bouton = tabTot[i * 2][0]
      const casecase = tabTot[i * 2][1]
      const casetitre = tabTot[i * 2][2]
      j3pAddContent(casetitre, tab[i] + '&nbsp;&nbsp;')
      j3pAddContent(casecase, '&nbsp;&nbsp;')

      casetitre.style.border = '1px solid black'
      casetitre.style.borderLeft = '0px solid black'
      bouton.style.border = '1px solid black'
      // bouton.style.borderRight = '0px solid black'
      casecase.style.borderTop = '1px solid black'
      casecase.style.borderBottom = '1px solid black'
      casecase.style.background = '#fff'
      casetitre.style.background = '#fff'

      acache.style.padding = '0px'
      bouton.style.padding = '0px'
      casecase.style.padding = '0px'
      casetitre.style.padding = '0px'
      acache2.style.padding = '0px'
      acache3.style.padding = '0px'

      acache.style.border = '0px'

      acache2.style.visibility = 'hidden'
      acache2.style.display = 'none'
      acache3.style.visibility = 'hidden'
      acache3.style.display = 'none'
      acache.style.display = 'none'
      bouton.classList.add('fboutonPlus')
      bouton.plus = false
      bouton.addEventListener('click', () => {
        bouton.plus = !bouton.plus
        acache.style.display = (bouton.plus) ? '' : 'none'
        acache2.style.display = (bouton.plus) ? '' : 'none'
        acache3.style.display = (bouton.plus) ? '' : 'none'
        if (!bouton.plus) {
          bouton.classList.add('fboutonPlus')
          bouton.classList.remove('fboutonMoins')
        } else {
          bouton.classList.add('fboutonMoins')
          bouton.classList.remove('fboutonPlus')
        }
      })
      const caseCoche = j3pAjouteCaseCoche(casecase, { value: '' })
      caseCoche.retour = []
      caseCoche.retourFonc = []
      arte.push({ acache, acache2, bouton, acache3, caseCoche })
      caseCoche.funcDown = () => {
        for (let j = 0; j < caseCoche.retour.length; j++) {
          if (!caseCoche.retour[j].disabled) {
            caseCoche.retour[j].checked = caseCoche.checked
            caseCoche.retourFonc[j]()
          }
        }
      }
      const functionChange = () => {
        caseCoche.funcDown()
        tabDep.caseCoche.adapte()
      }
      caseCoche.adapte = () => {
        let jesuiscoche = false
        for (let i = 0; i < caseCoche.retour.length; i++) {
          jesuiscoche = jesuiscoche || (caseCoche.retour[i].checked && !caseCoche.retour[i].disabled)
        }
        caseCoche.checked = jesuiscoche
      }
      caseCoche.addEventListener('change', functionChange)
      tabDep.caseCoche.retour.push(caseCoche)
      tabDep.caseCoche.retourFonc.push(caseCoche.funcDown)
    }
    return arte
  }
  function faisTabCase (ki, tab, param) {
    const ou = tab.acache
    const retour = tab.caseCoche.retour
    const retourFonc = tab.caseCoche.retourFonc
    const tabCase = addDefaultTable(ou, 2, 4)
    function chekRetour () {
      let foCh = false
      for (let i = 0; i < retour.length; i++) {
        foCh = foCh || retour[i].checked
      }
      tab.caseCoche.checked = foCh
    }
    for (let i = 0; i < 2; i++) {
      for (let j = 0; j < 4; j++) {
        tabCase[i][j].style.padding = '0px'
        tabCase[i][j].style.border = '1px solid black'
      }
    }
    if (param.p.exist) {
      j3pAddContent(tabCase[0][0], '&nbsp;Périmètre&nbsp;')
      tabCase[0][0].style.background = '#f5b1b1'
      tabCase[1][0].style.background = '#f5b1b1'
      tabCase[1][0].def = '#f5b1b1'
      const laCo = faisUneCoche(ki + '_P', tabCase[1][0])
      laCo.checked = valuesDeb.Liste.liste[ki + '_P']
      tabCase[1][0].style.textAlign = 'center'
      editor.listaChek.push({ aColorie: tabCase[1][0], case: laCo, nom: ki + '_P', optionnel: param.p.optionnel, niv: param.p.niv, exp: param.p.exp })
      retour.push(laCo)
      laCo.addEventListener('change', chekRetour)
      retourFonc.push(() => {})
    } else {
      tabCase[0][0].style.display = 'none'
      tabCase[1][0].style.display = 'none'
    }
    if (param.a.exist) {
      j3pAddContent(tabCase[0][1], '&nbsp;Aire&nbsp;')
      tabCase[0][1].style.background = '#bff188'
      tabCase[1][1].style.background = '#bff188'
      tabCase[1][1].def = '#bff188'
      const laCo = faisUneCoche(ki + '_A', tabCase[1][1])
      laCo.checked = valuesDeb.Liste.liste[ki + '_A']
      tabCase[1][1].style.textAlign = 'center'
      editor.listaChek.push({ aColorie: tabCase[1][1], case: laCo, nom: ki + '_A', optionnel: param.a.optionnel, niv: param.a.niv, exp: param.a.exp })
      retour.push(laCo)
      laCo.addEventListener('change', chekRetour)
      retourFonc.push(() => {})
    } else {
      tabCase[0][1].style.display = 'none'
      tabCase[1][1].style.display = 'none'
    }
    if (param.aLat.exist) {
      j3pAddContent(tabCase[0][2], '&nbsp;Aire latérale&nbsp;')
      tabCase[0][2].style.background = '#6bfdeb'
      tabCase[1][2].style.background = '#6bfdeb'
      tabCase[1][2].def = '#6bfdeb'
      const laCo = faisUneCoche(ki + '_ALat', tabCase[1][2])
      laCo.checked = valuesDeb.Liste.liste[ki + '_ALat']
      tabCase[1][2].style.textAlign = 'center'
      editor.listaChek.push({ aColorie: tabCase[1][2], case: laCo, nom: ki + '_ALat', optionnel: param.aLat.optionnel, niv: param.aLat.niv, exp: param.aLat.exp })
      retour.push(laCo)
      laCo.addEventListener('change', chekRetour)
      retourFonc.push(() => {})
    } else {
      tabCase[0][2].style.display = 'none'
      tabCase[1][2].style.display = 'none'
    }
    if (param.vol.exist) {
      j3pAddContent(tabCase[0][3], '&nbsp;Volume&nbsp;')
      tabCase[0][3].style.background = '#d072ff'
      tabCase[1][3].style.background = '#d072ff'
      tabCase[1][3].def = '#d072ff'
      const laCo = faisUneCoche(ki + '_V', tabCase[1][3])
      laCo.checked = valuesDeb.Liste.liste[ki + '_V']
      tabCase[1][3].style.textAlign = 'center'
      editor.listaChek.push({ aColorie: tabCase[1][3], case: laCo, nom: ki + '_V', optionnel: param.vol.optionnel, niv: param.vol.niv, exp: param.vol.exp })
      retour.push(laCo)
      laCo.addEventListener('change', chekRetour)
      retourFonc.push(() => {})
    } else {
      tabCase[0][3].style.display = 'none'
      tabCase[1][3].style.display = 'none'
    }
  }
  function faisUneCoche (nomParam, ou) {
    const laCo = j3pAjouteCaseCoche(ou, { label: '' })
    laCo.checked = valuesDeb.Liste.liste[nomParam]
    laCo.addEventListener('change', () => {
      valuesDeb.Liste.liste[nomParam] = laCo.checked
    })
    return laCo
  }
  function tradNot (s) {
    switch (s) {
      case '$A_{ABCD}$': return 'IndiceCourt'
      case '$A(ABCD)$': return 'FonctionCourt'
      case '$Aire_{ABCD}$': return 'IndiceLong'
      case '$Aire(ABCD)$': return 'FonctionLong'
    }
    return 'FonctionLong'
  }

  return new Promise((resolve, reject) => {
    function onSaveParams () {
      if (document.fullscreenElement) document.exitFullscreen()
      //
      resolve(
        {
          Parametrage_rapide: JSON.stringify(
            {
              titre: editor.inputTitre.value,
              liste: valuesDeb.Liste.liste
            }
          ),
          nbrepetitions: Number(editor.nbrepetitions.value),
          theme: editor.themeL.reponse,
          ChangementUnite: editor.changementUnit.reponse === 'Oui',
          TableauxConversion: editor.tabl.reponse === 'Oui',
          CalculMental: editor.calculL.reponse === 'Oui',
          ApproximationDecimale: editor.approx.reponse,
          ValeurApprochee: editor.approx2.reponse === 'Oui',
          NotationGrandeur: tradNot(editor.notage.reponse),
          perimetre: valuesDeb.Liste.liste.Cercle_P || valuesDeb.Liste.liste.Carre_P || valuesDeb.Liste.liste.Quadrilatere_P || valuesDeb.Liste.liste.Rectangle_P || valuesDeb.Liste.liste.Losange_P || valuesDeb.Liste.liste.Parallelogramme_P || valuesDeb.Liste.liste.TriangleQuelconque_P || valuesDeb.Liste.liste.TriangleEquilateral_P || valuesDeb.Liste.liste.TriangleEquilateral_A || valuesDeb.Liste.liste.Secteur_P || valuesDeb.Liste.liste.Cube_P || valuesDeb.Liste.liste.ParallelepipedeRectangle_P || valuesDeb.Liste.liste.Cylindre_P || valuesDeb.Liste.liste.ConeDeRevolution_P,
          aire: valuesDeb.Liste.liste.Cercle_A || valuesDeb.Liste.liste.Carre_A || valuesDeb.Liste.liste.Rectangle_A || valuesDeb.Liste.liste.Losange_A || valuesDeb.Liste.liste.Parallelogramme_A || valuesDeb.Liste.liste.TriangleRectangle_A || valuesDeb.Liste.liste.TriangleQuelconque_A || valuesDeb.Liste.liste.Secteur_A || valuesDeb.Liste.liste.Cube_A || valuesDeb.Liste.liste.ParallelepipedeRectangle_A || valuesDeb.Liste.liste.Cylindre_A || valuesDeb.Liste.liste.Sphere_A || valuesDeb.Liste.liste.PrismeDroit_A,
          aireLaterale: valuesDeb.Liste.liste.Cylindre_ALat || valuesDeb.Liste.liste.PrismeDroit_ALat,
          volume: valuesDeb.Liste.liste.Cube_V || valuesDeb.Liste.liste.ParallelepipedeRectangle_V || valuesDeb.Liste.liste.Cylindre_V || valuesDeb.Liste.liste.ConeDeRevolution_V || valuesDeb.Liste.liste.Sphere_V || valuesDeb.Liste.liste.Pyramide_V || valuesDeb.Liste.liste.PrismeDroit_V,
          niveau6e: editor.aut6.checked,
          niveau5e: editor.aut5.checked,
          niveau4e: editor.aut4.checked,
          niveau3e: editor.aut3.checked,
          Cercle: valuesDeb.Liste.liste.Cercle_P || valuesDeb.Liste.liste.Cercle_A,
          Carre: valuesDeb.Liste.liste.Carre_P || valuesDeb.Liste.liste.Carre_A,
          Quadrilatere: valuesDeb.Liste.liste.Quadrilatere_P,
          Rectangle: valuesDeb.Liste.liste.Rectangle_P || valuesDeb.Liste.liste.Rectangle_A,
          Losange: valuesDeb.Liste.liste.Losange_P || valuesDeb.Liste.liste.Losange_A,
          Parallelogramme: valuesDeb.Liste.liste.Parallelogramme_P || valuesDeb.Liste.liste.Parallelogramme_A,
          TriangleRectangle: valuesDeb.Liste.liste.TriangleRectangle_A,
          TriangleEquilateral: valuesDeb.Liste.liste.TriangleEquilateral_P || valuesDeb.Liste.liste.TriangleEquilateral_A,
          TriangleQuelconque: valuesDeb.Liste.liste.TriangleQuelconque_P || valuesDeb.Liste.liste.TriangleQuelconque_A,
          Secteur: valuesDeb.Liste.liste.Secteur_P || valuesDeb.Liste.liste.Secteur_A,
          Cube: valuesDeb.Liste.liste.Cube_P || valuesDeb.Liste.liste.Cube_A || valuesDeb.Liste.liste.Cube_V,
          ParallelepipedeRectangle: valuesDeb.Liste.liste.ParallelepipedeRectangle_P || valuesDeb.Liste.liste.ParallelepipedeRectangle_A || valuesDeb.Liste.liste.ParallelepipedeRectangle_V,
          Cylindre: valuesDeb.Liste.liste.Cylindre_P || valuesDeb.Liste.liste.Cylindre_A || valuesDeb.Liste.liste.Cylindre_V || valuesDeb.Liste.liste.Cylindre_ALat,
          ConeDeRevolution: valuesDeb.Liste.liste.ConeDeRevolution_P || valuesDeb.Liste.liste.ConeDeRevolution_V,
          Sphere: valuesDeb.Liste.liste.Sphere_A || valuesDeb.Liste.liste.Sphere_V,
          Pyramide: valuesDeb.Liste.liste.Sphere_A || valuesDeb.Liste.liste.Pyramide_V,
          PrismeDroit: valuesDeb.Liste.liste.PrismeDroit_A || valuesDeb.Liste.liste.PrismeDroit_ALat || valuesDeb.Liste.liste.PrismeDroit_V,
          Optionnel: editor.yaOpt.reponse === 'Oui',
          NotationExpert: editor.yaExp.reponse === 'Oui'
        })
    }

    loadMq()
      .then(() => {
        const btnsContainer = j3pAddElt(container, 'div', 'Chargement en cours…', {
          style: {
            margin: '1rem auto',
            textAlign: 'center'
          }
        })

        j3pEmpty(btnsContainer)
        // Important, les boutons annuler / valider
        container.parentNode.style.opacity = 1
        const btnProps = { style: { margin: '0.2rem', fontSize: '120%' } }
        j3pAddElt(btnsContainer, 'button', 'Annuler', btnProps)
          .addEventListener('click', () => {
            resolve()
          })
        j3pAddElt(btnsContainer, 'button', 'Valider', btnProps)
          .addEventListener('click', onSaveParams)
        try {
          valuesDeb.Liste = JSON.parse(values.Parametrage_rapide)
        } catch (error) {
          console.error(error)
          const yy = j3pModale2({ titre: 'Avertissement ', contenu: 'Ce fichier comporte une erreur<br>Il faut modifier le JSON' })
          j3pAddContent(yy, '\n')
          j3pAjouteBouton(yy, () => {
            j3pDetruit(yy)
            j3pDetruit(yy.masque)
            resolve()
          }, { value: 'Quitter' })
          return
        }
        creeLesDiv()
      })
      .catch(reject)
  }) // promesse retournée
}
