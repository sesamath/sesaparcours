export default function getDefaultProgramme () {
  return {
    titre: 'Périmètre d\'un cercle',
    liste: {
    }
  }
}

/*
export default function getDefaultProgramme () {
  return {
    titre: 'Périmètre d\'un cercle',
    liste: {
      Cercle_P: true,
      Cercle_A: false,
      Carre_P: false,
      Carre_A: false,
      Quadrilatere_P: false,
      Rectangle_P: false,
      Rectangle_A: false,
      Losange_P: false,
      Losange_A: false,
      Parallelogramme_P: false,
      Parallelogramme_A: false,
      TriangleRectangle_A: false,
      TriangleQuelconque_P: false,
      TriangleQuelconque_A: false,
      TriangleEquilateral_P: false,
      Secteur_A: false,
      Cube_P: false,
      Cube_A: false,
      Cube_V: false,
      ParallelepipedeRectangle_P: false,
      ParallelepipedeRectangle_A: false,
      ParallelepipedeRectangle_V: false,
      Cylindre_A: false,
      Cylindre_V: false,
      ConeDeRevolution_A: false,
      ConeDeRevolution_V: false,
      Sphere_A: false,
      Sphere_V: false,
      Pyramide_A: false,
      Pyramide_V: false,
      PrismeDroit_P: false,
      PrismeDroit_A: false,
      PrismeDroit_V: false
    }
  }
}
 */
