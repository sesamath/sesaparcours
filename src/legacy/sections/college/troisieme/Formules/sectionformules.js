import { j3pDiv, j3pGetRandomInt, j3pShowError, j3pShuffle } from 'src/legacy/core/functions'
import BulleAide from 'src/legacy/outils/bulleAide/BulleAide'
import { j3pAffiche } from 'src/lib/mathquill/functions'
import Zoneclick from 'src/legacy/outils/zoneclick/Zoneclick'
import { addDefaultTable } from 'src/legacy/themes/table'
import textesGeneriques from 'src/lib/core/textes'

const { cBien, cFaux, essaieEncore, regardeCorrection, reponseIncomplete, tempsDepasse } = textesGeneriques

/**
 * Ajouter une description ici
 * @fileOverview
 * @author Tommy Barroy <t.barroy@laposte.net>
 * @since 2017-10
*/

export const params = {
  outils: ['mathquill'],
  /**
   * Ce qui est paramétrable dans l’éditeur de graphe
   * @type {LegacySectionParamsParametres}
   */
  parametres: [
    ['nbrepetitions', 9, 'entier', 'Nombre de répétitions de la section'],
    ['limite', 0, 'entier', 'Temps disponible par répétition, en secondes'],
    ['NbChoix', 6, 'entier', 'Nombre de choix possibles (entre 2 et 12)'],
    ['ObligeToutes', true, 'boolean', 'true : L’élève doit sélectionner toutes les formules <BR> false : Une bonne réponse suffit.'],
    ['niveau6e', true, 'boolean', 'true : L’exercice peut proposer des formules de 6e <BR> false : non.'],
    ['niveau5e', true, 'boolean', 'true : L’exercice peut proposer des formules de 5e <BR> false : non.'],
    ['niveau4e', true, 'boolean', 'true : L’exercice peut proposer des formules de 4e <BR> false : non. <BR> (formules des pyramides et cônes)'],
    ['niveau3e', true, 'boolean', 'true : L’exercice peut proposer des formules de 3e <BR> false : non. <BR> (aire et volume d’une sphère)'],
    ['A_Triangle_Quelconque', false, 'boolean', 'true : Formule aire d’un triangle quelconque proposée <BR> false : non.'],
    ['A_Triangle_Rectangle', false, 'boolean', 'true : Formule aire d’un triangle rectangle proposée <BR> false : non.'],
    ['P_Triangle_Equilateral', false, 'boolean', 'true : Formule périmètre d’un triangle équilatéral proposée <BR> false : non.'],
    ['P_Carre', false, 'boolean', 'true : Formule périmètre d’un carré proposée <BR> false : non.'],
    ['A_Carre', false, 'boolean', 'true : Formule aire d’un carré proposée <BR> false : non.'],
    ['P_Rectangle', false, 'boolean', 'true : Formule périmètre d’un rectangle proposée <BR> false : non.'],
    ['A_Rectangle', false, 'boolean', 'true : Formule aire d’un rectangle proposée <BR> false : non.'],
    ['P_Losange', false, 'boolean', 'true : Formule périmètre d’un losange proposée <BR> false : non.'],
    ['A_Losange', false, 'boolean', 'true : Formule aire d’un losange proposée <BR> false : non.'],
    ['P_Parallelogramme', false, 'boolean', 'true : Formule périmètre d’un parallélogramme proposée <BR> false : non.'],
    ['A_Parallelogramme', false, 'boolean', 'true : Formule aire d’un parallélogramme proposée <BR> false : non.'],
    ['P_Cercle', false, 'boolean', 'true : Formule périmètre d’un cercle proposée <BR> false : non.'],
    ['A_Cercle', false, 'boolean', 'true : Formule aire d’un cercle proposée <BR> false : non.'],
    ['A_Secteur', false, 'boolean', 'true : Formule aire d’un secteur circulaire proposée <BR> false : non.'],
    ['P_Cube', false, 'boolean', 'true : Formule périmètre d’un cube proposée <BR> false : non.'],
    ['A_Cube', false, 'boolean', 'true : Formule aire d’un cube proposée <BR> false : non.'],
    ['V_Cube', false, 'boolean', 'true : Formule volume d’un cube proposée <BR> false : non.'],
    ['P_Pave_Droit', false, 'boolean', 'true : Formule périmètre d’un pavé droit proposée <BR> false : non.'],
    ['A_Pave_Droit', false, 'boolean', 'true : Formule aire d’un pavé droit proposée <BR> false : non.'],
    ['V_Pave_Droit', false, 'boolean', 'true : Formule volume d’un pavé droit proposée <BR> false : non.'],
    ['Al_Prisme_Droit', false, 'boolean', 'true : Formule aire latérale d’un prisme droit proposée <BR> false : non.'],
    ['V_Prisme_Droit', false, 'boolean', 'true : Formule volume d’un prisme droit proposée <BR> false : non.'],
    ['P_Cylindre', false, 'boolean', 'true : Formule périmètre d’un cylindre de révolution proposée <BR> false : non.'],
    ['A_Cylindre', false, 'boolean', 'true : Formule aire d’un cylindre de révolution proposée <BR> false : non.'],
    ['Al_Cylindre', false, 'boolean', 'true : Formule aire latérale d’un cylindre de révolution proposée <BR> false : non.'],
    ['V_Cylindre', false, 'boolean', 'true : Formule volume d’un cylindre de révolution proposée <BR> false : non.'],
    ['V_Pyramide', false, 'boolean', 'true : Formule volume d’une pyramide proposée <BR> false : non.'],
    ['P_Cone', false, 'boolean', 'true : Formule périmètre d’un cône de révolution proposée <BR> false : non.'],
    ['A_Cone', false, 'boolean', 'true : Formule aire d’un cône de révolution proposée <BR> false : non.'],
    ['Al_Cone', false, 'boolean', 'true : Formule aire latérale d’un cône de révolution proposée <BR> false : non.'],
    ['V_Cone', false, 'boolean', 'true : Formule volume d’un cône de révolution proposée <BR> false : non.'],
    ['A_Sphere', false, 'boolean', 'true : Formule aire d’une sphère proposée <BR> false : non.'],
    ['V_Sphere', false, 'boolean', 'true : Formule volume d’une sphère proposée <BR> false : non.'],
    ['NotationCarreCube', true, 'boolean', 'true: L’exercice peut proposer des formules présentées avec les notations carré ² et cube <BR> false: non'],
    // le modèle gère la compatibilité ascendante avec l’ancien paramètre MiseEnPagePlusClaire
    ['theme', 'standard', 'liste', 'Modèle pour l’affichage', ['standard', 'zonesAvecImageDeFond']]
  ]
}

const textes = {
  oubli: 'Tu as oublié cette formule !',
  formules: [
    {
      figure: 'carré',
      genre: 1,
      nbpoints: 4,
      perimetre: [
        { f: '4 \\times \\text{côté}', n: '6e', o: false },
        { f: '\\text{côté} \\times 4', n: '6e', o: false },
        { f: '2 \\times (\\text{longueur} + \\text{largeur})', n: '6e', o: false, p: true },
        { f: '(\\text{longueur} + \\text{largeur}) \\times 2', n: '6e', o: false, p: true },
        { f: '2 \\times \\text{longueur} + 2 \\times \\text{largeur}', n: '6e', o: false, p: true }
      ],
      aire: [
        { f: '\\text{côté} \\times \\text{côté}', n: '6e', o: false },
        { f: '\\text{côté}^{2}', n: '6e', o: false },
        { f: '\\frac{\\text{petite diagonale} \\times \\text{grande diagonale}}{2}', n: '6e', o: true, p: true },
        { f: '\\text{longueur} \\times \\text{largeur}', n: '6e', o: true, p: true },
        { f: '\\text{base} \\times \\text{hauteur}', n: '5e', o: true, p: true }
      ],
      volume: [],
      airelaterale: []
    },
    {
      figure: 'cercle',
      genre: 1,
      nbpoints: undefined,
      perimetre: [
        { f: '2 \\times \\pi \\times \\text{rayon}', n: '6e', o: false },
        { f: '\\pi \\times \\text{diamètre}', n: '6e', o: false },
        { f: '\\text{diamètre} \\times \\pi', n: '6e', o: false },
        { f: '2 \\times \\text{rayon} \\times \\pi', n: '6e', o: false }
      ],
      aire: [
        { f: '\\pi \\times \\text{rayon} \\times \\text{rayon}', n: '6e', o: false },
        { f: '\\pi \\times \\text{rayon}^{2}', n: '6e', o: false }
      ],
      volume: [],
      airelaterale: []
    },
    {
      figure: 'secteur circulaire',
      genre: 1,
      nbpoints: undefined,
      perimetre: [],
      aire: [
        { f: '\\frac{\\pi \\times \\text{rayon} \\times \\text{rayon} \\times \\text{angle}}{360}', n: '5e', o: false },
        { f: '\\frac{\\pi \\times \\text{rayon}^{2} \\times \\text{angle}}{360}', n: '5e', o: false },
        { f: '\\frac{ \\text{rayon}^{2} \\times \\text{angle} \\times \\pi  }{360}', n: '5e', o: false }
      ],
      volume: [],
      airelaterale: []
    },
    {
      figure: 'rectangle',
      genre: 1,
      nbpoints: 4,
      perimetre: [
        { f: '2 \\times (\\text{longueur} + \\text{largeur})', n: '6e', o: false },
        { f: '(\\text{longueur} + \\text{largeur}) \\times 2', n: '6e', o: false },
        { f: '2 \\times \\text{longueur} + 2 \\times \\text{largeur}', n: '6e', o: false }
      ],
      aire: [
        { f: '\\text{longueur} \\times \\text{largeur}', n: '6e', o: false },
        { f: '\\text{base} \\times \\text{hauteur}', n: '5e', o: true, p: true }
      ],
      volume: [],
      airelaterale: []
    },
    {
      figure: 'losange',
      genre: 1,
      nbpoints: 4,
      perimetre: [
        { f: '4 \\times \\text{côté}', n: '6e', o: false },
        { f: '\\text{côté} \\times 4', n: '6e', o: false },
        { f: '2 \\times (\\text{longueur} + \\text{largeur})', n: '6e', o: false, p: true },
        { f: '(\\text{longueur} + \\text{largeur}) \\times 2', n: '6e', o: false, p: true },
        { f: '2 \\times \\text{longueur} + 2 \\times \\text{largeur}', n: '6e', o: false, p: true }
      ],
      aire: [
        { f: '\\frac{\\text{petite diagonale} \\times \\text{grande diagonale}}{2}', n: '6e', o: true },
        { f: '\\text{base} \\times \\text{hauteur}', n: '5e', o: true, p: true }
      ],
      volume: [],
      airelaterale: []
    },
    {
      figure: 'parallélogramme',
      genre: 1,
      nbpoints: 4,
      perimetre: [
        { f: '2 \\times (\\text{longueur} + \\text{largeur})', n: '6e', o: true },
        { f: '(\\text{longueur} + \\text{largeur}) \\times 2', n: '5e', o: true },
        { f: '2 \\times \\text{longueur} + 2 \\times \\text{largeur}', n: '5e', o: true }
      ],
      aire: [
        { f: '\\text{base} \\times \\text{hauteur}', n: '5e', o: false }
      ],
      volume: [],
      airelaterale: []
    },
    {
      figure: 'triangle rectangle',
      genre: 1,
      nbpoints: 3,
      perimetre: [],
      aire: [
        { f: '\\frac{\\text{produit des côtés de l’angle droit}}{2}', n: '6e', o: false },
        { f: '\\frac{ \\text{base} \\times \\text{hauteur}}{2}', n: '5e', o: false }
      ],
      volume: [],
      airelaterale: []
    },
    {
      figure: 'triangle quelconque',
      genre: 1,
      nbpoints: 3,
      perimetre: [],
      aire: [
        { f: '\\frac{ \\text{base} \\times \\text{hauteur}}{2}', n: '6e', o: false }
      ],
      volume: [],
      airelaterale: []
    },
    {
      figure: 'triangle équilatéral',
      genre: 1,
      nbpoints: 3,
      perimetre: [
        { f: '3 \\times \\text{côté}', n: '6e', o: false },
        { f: '\\text{côté} \\times 3', n: '6e', o: false }
      ],
      aire: [],
      volume: [],
      airelaterale: []
    },
    {
      figure: 'cube',
      genre: 1,
      nbpoints: 8,
      perimetre: [
        { f: '12 \\times \\text{côté}', n: '6e', o: true },
        { f: '\\text{côté} \\times 12', n: '6e', o: true }
      ],
      aire: [
        { f: '\\text{côté} \\times \\text{côté} \\times 6', n: '6e', o: true },
        { f: '\\text{côté}^{2} \\times 6', n: '6e', o: true },
        { f: '6 \\times \\text{côté}^{2}', n: '6e', o: true },
        { f: '6 \\times \\text{côté} \\times \\text{côté}', n: '6e', o: true }
      ],
      volume: [
        { f: '\\text{côté} \\times \\text{côté} \\times \\text{côté}', n: '6e', o: false },
        { f: '\\text{côté}^{3}', n: '6e', o: false }
      ],
      airelaterale: []
    },
    {
      figure: 'parallélépipède rectangle',
      genre: 1,
      nbpoints: 8,
      perimetre: [
        { f: '4 \\times \\text{longueur} + 4 \\times \\text{largeur} + 4 \\times \\text{hauteur}', n: '6e', o: true },
        { f: '4 \\times (\\text{longueur} + \\text{largeur} + \\text{hauteur})', n: '6e', o: true }
      ],
      aire: [
        { f: '2\\times\\text{longueur}\\times\\text{largeur}+2\\times\\text{longueur}\\times\\text{hauteur}+2\\times\\text{hauteur}\\times\\text{largeur}', n: '6e', o: true },
        { f: '2 \\times (\\text{longueur}\\times\\text{largeur} + \\text{longueur}\\times\\text{hauteur} + \\text{hauteur}\\times\\text{largeur})', n: '6e', o: true }
      ],
      volume: [
        { f: '\\text{longueur} \\times \\text{largeur} \\times \\text{hauteur}', n: '6e', o: false }
      ],
      airelaterale: []
    },
    {
      figure: 'prisme droit',
      genre: 1,
      nbpoints: undefined,
      perimetre: [
        { f: '2 \\times \\text{périmètre de la base} + \\text{nombre de côtés} \\times \\text{hauteur}', n: '5e', o: true }
      ],
      aire: [
        { f: '\\text{périmètre de la base} \\times \\text{hauteur} + 2 \\times \\text{aire de la base}', n: '5e', o: true }
      ],
      volume: [
        { f: '\\text{aire de la base} \\times \\text{hauteur}', n: '5e', o: false }
      ],
      airelaterale: [
        { f: '\\text{périmètre de la base} \\times \\text{hauteur}', n: '5e', o: false }
      ]
    },
    {
      figure: 'cylindre',
      genre: 1,
      nbpoints: undefined,
      perimetre: [
        { f: '2 \\times \\text{périmètre de la base}', n: '5e', o: true },
        { f: '4 \\times \\pi \\times \\text{rayon}', n: '5e', o: true },
        { f: '2 \\times \\pi \\times \\text{diamètre}', n: '5e', o: true }
      ],
      aire: [
        { f: '\\text{périmètre de la base} \\times \\text{hauteur} + 2 \\times \\text{aire de la base}', n: '5e', o: true },
        { f: '2 \\times \\pi \\times \\text{rayon} \\times \\text{hauteur} + 2 \\times \\pi \\times \\text{rayon}^{2}', n: '5e', o: true }
      ],
      volume: [
        { f: '\\text{aire de la base} \\times \\text{hauteur}', n: '5e', o: false },
        { f: '\\pi \\times \\text{rayon}^{2} \\times \\text{hauteur}', n: '5e', o: false }
      ],
      airelaterale: [
        { f: '\\text{périmètre de la base} \\times \\text{hauteur}', n: '5e', o: false },
        { f: '2 \\times \\pi \\times \\text{rayon} \\times \\text{hauteur}', n: '5e', o: false }
      ]
    },
    {
      figure: 'pyramide',
      genre: 0,
      nbpoints: undefined,
      perimetre: [],
      aire: [],
      volume: [
        { f: '\\frac{\\text{aire de la base} \\times \\text{hauteur}}{3}', n: '4e', o: false }
      ],
      airelaterale: []
    },
    {
      figure: 'cône de révolution',
      genre: 1,
      nbpoints: undefined,
      perimetre: [
        { f: '2 \\times \\pi \\times \\text{rayon}', n: '4e', o: true },
        { f: '\\pi \\times \\text{diamètre}', n: '4e', o: true },
        { f: '\\text{diamètre} \\times \\pi', n: '4e', o: true },
        { f: '2 \\times \\text{rayon} \\times \\pi', n: '4e', o: true }
      ],
      aire: [
        { f: '\\pi \\times \\text{rayon}^{2} + \\pi \\times \\text{rayon} \\times \\sqrt{\\text{rayon}^{2} + \\text{hauteur}^{2}}', n: '4e', o: true }
      ],
      volume: [
        { f: '\\frac{\\text{aire de la base} \\times \\text{hauteur}}{3}', n: '4e', o: false }
      ],
      airelaterale: [
        { f: '\\pi \\times \\text{rayon} \\times \\sqrt{\\text{rayon}^{2} + \\text{hauteur}^{2}}', n: '4e', o: true }
      ]
    },
    {
      figure: 'sphère',
      genre: 0,
      nbpoints: undefined,
      perimetre: [],
      aire: [
        { f: '4 \\times \\pi \\times \\text{rayon}^{2}', n: '3e', o: false }
      ],
      volume: [
        { f: '\\frac{4}{3} \\times \\pi \\times \\text{rayon}^{3}', n: '3e', o: false }
      ],
      airelaterale: []
    }
  ],
  consigneG: 'Sélectionne la (ou les) bonne(s) formule(s).',
  consigne2: 'Pour calculer £a £b £c , on peut utiliser la (ou les) formule(s) suivante(s): ',
  niveaux: ['6e', '5e', '4e', '3e'],
  genres: [' d’une ', ' d’un '],
  grandeurs: ['le périmètre', 'l’aire', 'l’aire latérale', 'le volume'],
  tuaschoiz: 'Cette formule permet de calculer <BR> £a £b £c !',
  obligtext: 'Tu dois sélectionner toutes les formules correctes.'
}

/**
 * section formules
 * @this {Parcours}
 */
export default function main () {
  const me = this
  const stor = me.storage
  const ds = me.donneesSection

  function yareponse () {
    if (me.isElapsed) { return true }
    return (stor.zonesclick[0].reponsenb.length > 0)
  }

  function bonneReponse () {
    // cette fonction renvoie true ou false si du moins une réponse a été saisie
    // (dans le cas contraire, elle n’est pas appelée
    // elle est appelée dans la partie "correction" de la section

    stor.formuleTrouvee = false
    stor.formuleEnTrop = false
    stor.formuleRatee = false
    stor.casesCorrigees = []
    stor.formulesCorrigees = []
    stor.casesRattrappees = []

    // cherche si ya formule trouvee ou entrop
    for (const [i, rep] of stor.zonesclick[0].reponse.entries()) {
      if (stor.reponsesPossibles.includes(rep)) {
        stor.formuleTrouvee = true
      } else {
        stor.formuleEnTrop = true
        stor.casesCorrigees.push(stor.zonesclick[0].reponsenb[i])
        stor.formulesCorrigees.push(rep)
      }
    }

    // cherche si ya formuleRatee
    for (let i = 0; i < stor.zonesclick[0].nb; i++) {
      if ((!stor.zonesclick[0].sel[i]) && ((stor.reponsesPossibles.indexOf(stor.zonesclick[0].montab[i]) !== -1))) {
        if (stor.reponsespossiblesOp.indexOf(stor.zonesclick[0].montab[i]) === -1) {
          stor.formuleRatee = true
        }
        stor.casesRattrappees.push(i)
      }
    }

    if (ds.ObligeToutes) {
      return ((stor.formuleTrouvee) && (!stor.formuleEnTrop) && (!stor.formuleRatee))
    } else {
      return ((stor.formuleTrouvee) && (!stor.formuleEnTrop))
    }
  }

  function initSection () {
    // Construction de la page
    me.construitStructurePage('presentation3')

    if ((!(ds.NbChoix < 13)) && (!(ds.NbChoix > 2))) ds.NbChoix = 6

    // DECLARATION DES VARIABLES
    //
    stor.ToutesformulesPossibles = []
    stor.dejapose = []
    for (let i = 0; i < textes.formules.length; i++) {
      for (let j = 0; j < textes.formules[i].perimetre.length; j++) {
        stor.ToutesformulesPossibles.push({ grandeur: 'le périmètre', figure: textes.formules[i].figure, nbpoints: textes.formules[i].nbpoints, genre: textes.formules[i].genre, f: textes.formules[i].perimetre[j].f, n: textes.formules[i].perimetre[j].n, o: textes.formules[i].perimetre[j].o, p: textes.formules[i].perimetre[j].p })
      }
      for (let j = 0; j < textes.formules[i].aire.length; j++) {
        stor.ToutesformulesPossibles.push({ grandeur: 'l’aire', figure: textes.formules[i].figure, nbpoints: textes.formules[i].nbpoints, genre: textes.formules[i].genre, f: textes.formules[i].aire[j].f, n: textes.formules[i].aire[j].n, o: textes.formules[i].aire[j].o, p: textes.formules[i].aire[j].p })
      }
      for (let j = 0; j < textes.formules[i].airelaterale.length; j++) {
        stor.ToutesformulesPossibles.push({ grandeur: 'l’aire latérale', figure: textes.formules[i].figure, nbpoints: textes.formules[i].nbpoints, genre: textes.formules[i].genre, f: textes.formules[i].airelaterale[j].f, n: textes.formules[i].airelaterale[j].n, o: textes.formules[i].airelaterale[j].o, p: textes.formules[i].airelaterale[j].p })
      }
      for (let j = 0; j < textes.formules[i].volume.length; j++) {
        stor.ToutesformulesPossibles.push({ grandeur: 'le volume', figure: textes.formules[i].figure, nbpoints: textes.formules[i].nbpoints, genre: textes.formules[i].genre, f: textes.formules[i].volume[j].f, n: textes.formules[i].volume[j].n, o: textes.formules[i].volume[j].o, p: textes.formules[i].volume[j].p })
      }
    }

    stor.formulesPossibles = []
    if (ds.A_Triangle_Quelconque) {
      const um = textes.formules[7]
      for (let j = 0; j < um.aire.length; j++) {
        const om = um.aire[j]
        stor.formulesPossibles.push({ grandeur: textes.grandeurs[1], figure: um.figure, nbpoints: um.nbpoints, genre: um.genre, f: om.f, n: om.n, o: om.o, p: om.p })
      }
    }
    if (ds.A_Triangle_Rectangle) {
      const um = textes.formules[6]
      for (let j = 0; j < um.aire.length; j++) {
        const om = um.aire[j]
        stor.formulesPossibles.push({ grandeur: textes.grandeurs[1], figure: um.figure, nbpoints: um.nbpoints, genre: um.genre, f: om.f, n: om.n, o: om.o, p: om.p })
      }
    }
    if (ds.P_Triangle_Equilateral) {
      const um = textes.formules[8]
      for (let j = 0; j < um.perimetre.length; j++) {
        const om = um.perimetre[j]
        stor.formulesPossibles.push({ grandeur: textes.grandeurs[0], figure: um.figure, nbpoints: um.nbpoints, genre: um.genre, f: om.f, n: om.n, o: om.o, p: om.p })
      }
    }
    if (ds.P_Carre) {
      const um = textes.formules[0]
      for (let j = 0; j < um.perimetre.length; j++) {
        const om = um.perimetre[j]
        stor.formulesPossibles.push({ grandeur: textes.grandeurs[0], figure: um.figure, nbpoints: um.nbpoints, genre: um.genre, f: om.f, n: om.n, o: om.o, p: om.p })
      }
    }
    if (ds.A_Carre) {
      const um = textes.formules[0]
      for (let j = 0; j < um.aire.length; j++) {
        const om = um.aire[j]
        stor.formulesPossibles.push({ grandeur: textes.grandeurs[1], figure: um.figure, nbpoints: um.nbpoints, genre: um.genre, f: om.f, n: om.n, o: om.o, p: om.p })
      }
    }
    if (ds.P_Rectangle) {
      const um = textes.formules[3]
      for (let j = 0; j < um.perimetre.length; j++) {
        const om = um.perimetre[j]
        stor.formulesPossibles.push({ grandeur: textes.grandeurs[0], figure: um.figure, nbpoints: um.nbpoints, genre: um.genre, f: om.f, n: om.n, o: om.o, p: om.p })
      }
    }
    if (ds.A_Rectangle) {
      const um = textes.formules[3]
      for (let j = 0; j < um.aire.length; j++) {
        const om = um.aire[j]
        stor.formulesPossibles.push({ grandeur: textes.grandeurs[1], figure: um.figure, nbpoints: um.nbpoints, genre: um.genre, f: om.f, n: om.n, o: om.o, p: om.p })
      }
    }
    if (ds.P_Losange) {
      const um = textes.formules[4]
      for (let j = 0; j < um.perimetre.length; j++) {
        const om = um.perimetre[j]
        stor.formulesPossibles.push({ grandeur: textes.grandeurs[0], figure: um.figure, nbpoints: um.nbpoints, genre: um.genre, f: om.f, n: om.n, o: om.o, p: om.p })
      }
    }
    if (ds.A_Losange) {
      const um = textes.formules[4]
      for (let j = 0; j < um.aire.length; j++) {
        const om = um.aire[j]
        stor.formulesPossibles.push({ grandeur: textes.grandeurs[1], figure: um.figure, nbpoints: um.nbpoints, genre: um.genre, f: om.f, n: om.n, o: om.o, p: om.p })
      }
    }
    if (ds.P_Parallelogramme) {
      const um = textes.formules[5]
      for (let j = 0; j < um.perimetre.length; j++) {
        const om = um.perimetre[j]
        stor.formulesPossibles.push({ grandeur: textes.grandeurs[0], figure: um.figure, nbpoints: um.nbpoints, genre: um.genre, f: om.f, n: om.n, o: om.o, p: om.p })
      }
    }
    if (ds.A_Parallelogramme) {
      const um = textes.formules[5]
      for (let j = 0; j < um.aire.length; j++) {
        const om = um.aire[j]
        stor.formulesPossibles.push({ grandeur: textes.grandeurs[1], figure: um.figure, nbpoints: um.nbpoints, genre: um.genre, f: om.f, n: om.n, o: om.o, p: om.p })
      }
    }
    if (ds.P_Cercle) {
      const um = textes.formules[1]
      for (let j = 0; j < um.perimetre.length; j++) {
        const om = um.perimetre[j]
        stor.formulesPossibles.push({ grandeur: textes.grandeurs[0], figure: um.figure, nbpoints: um.nbpoints, genre: um.genre, f: om.f, n: om.n, o: om.o, p: om.p })
      }
    }
    if (ds.A_Cercle) {
      const um = textes.formules[1]
      for (let j = 0; j < um.aire.length; j++) {
        const om = um.aire[j]
        stor.formulesPossibles.push({ grandeur: textes.grandeurs[1], figure: um.figure, nbpoints: um.nbpoints, genre: um.genre, f: om.f, n: om.n, o: om.o, p: om.p })
      }
    }
    if (ds.A_Secteur) {
      const um = textes.formules[2]
      for (let j = 0; j < um.aire.length; j++) {
        const om = um.aire[j]
        stor.formulesPossibles.push({ grandeur: textes.grandeurs[1], figure: um.figure, nbpoints: um.nbpoints, genre: um.genre, f: om.f, n: om.n, o: om.o, p: om.p })
      }
    }
    if (ds.P_Cube) {
      const um = textes.formules[9]
      for (let j = 0; j < um.perimetre.length; j++) {
        const om = um.perimetre[j]
        stor.formulesPossibles.push({ grandeur: textes.grandeurs[0], figure: um.figure, nbpoints: um.nbpoints, genre: um.genre, f: om.f, n: om.n, o: om.o, p: om.p })
      }
    }
    if (ds.A_Cube) {
      const um = textes.formules[9]
      for (let j = 0; j < um.aire.length; j++) {
        const om = um.aire[j]
        stor.formulesPossibles.push({ grandeur: textes.grandeurs[1], figure: um.figure, nbpoints: um.nbpoints, genre: um.genre, f: om.f, n: om.n, o: om.o, p: om.p })
      }
    }
    if (ds.V_Cube) {
      const um = textes.formules[9]
      for (let j = 0; j < um.volume.length; j++) {
        const om = um.volume[j]
        stor.formulesPossibles.push({ grandeur: textes.grandeurs[3], figure: um.figure, nbpoints: um.nbpoints, genre: um.genre, f: om.f, n: om.n, o: om.o, p: om.p })
      }
    }
    if (ds.P_Pave_Droit) {
      const um = textes.formules[9]
      for (let j = 0; j < um.perimetre.length; j++) {
        const om = um.perimetre[j]
        stor.formulesPossibles.push({ grandeur: textes.grandeurs[0], figure: um.figure, nbpoints: um.nbpoints, genre: um.genre, f: om.f, n: om.n, o: om.o, p: om.p })
      }
    }
    if (ds.A_Pave_Droit) {
      const um = textes.formules[10]
      for (let j = 0; j < um.aire.length; j++) {
        const om = um.aire[j]
        stor.formulesPossibles.push({ grandeur: textes.grandeurs[1], figure: um.figure, nbpoints: um.nbpoints, genre: um.genre, f: om.f, n: om.n, o: om.o, p: om.p })
      }
    }
    if (ds.V_Pave_Droit) {
      const um = textes.formules[10]
      for (let j = 0; j < um.volume.length; j++) {
        const om = um.volume[j]
        stor.formulesPossibles.push({ grandeur: textes.grandeurs[3], figure: um.figure, nbpoints: um.nbpoints, genre: um.genre, f: om.f, n: om.n, o: om.o, p: om.p })
      }
    }
    if (ds.A_Sphere) {
      const um = textes.formules[15]
      for (let j = 0; j < um.aire.length; j++) {
        const om = um.aire[j]
        stor.formulesPossibles.push({ grandeur: textes.grandeurs[1], figure: um.figure, nbpoints: um.nbpoints, genre: um.genre, f: om.f, n: om.n, o: om.o, p: om.p })
      }
    }
    if (ds.V_Sphere) {
      const um = textes.formules[15]
      for (let j = 0; j < um.volume.length; j++) {
        const om = um.volume[j]
        stor.formulesPossibles.push({ grandeur: textes.grandeurs[3], figure: um.figure, nbpoints: um.nbpoints, genre: um.genre, f: om.f, n: om.n, o: om.o, p: om.p })
      }
    }
    if (ds.Al_Prisme_Droit) {
      const um = textes.formules[11]
      for (let j = 0; j < um.airelaterale.length; j++) {
        const om = um.airelaterale[j]
        stor.formulesPossibles.push({ grandeur: textes.grandeurs[2], figure: um.figure, nbpoints: um.nbpoints, genre: um.genre, f: om.f, n: om.n, o: om.o, p: om.p })
      }
    }
    if (ds.V_Prisme_Droit) {
      const um = textes.formules[11]
      for (let j = 0; j < um.volume.length; j++) {
        const om = um.volume[j]
        stor.formulesPossibles.push({ grandeur: textes.grandeurs[3], figure: um.figure, nbpoints: um.nbpoints, genre: um.genre, f: om.f, n: om.n, o: om.o, p: om.p })
      }
    }
    if (ds.V_Pyramide) {
      const um = textes.formules[13]
      for (let j = 0; j < um.volume.length; j++) {
        const om = um.volume[j]
        stor.formulesPossibles.push({ grandeur: textes.grandeurs[3], figure: um.figure, nbpoints: um.nbpoints, genre: um.genre, f: om.f, n: om.n, o: om.o, p: om.p })
      }
    }
    if (ds.P_Cylindre) {
      const um = textes.formules[12]
      for (let j = 0; j < um.perimetre.length; j++) {
        const om = um.perimetre[j]
        stor.formulesPossibles.push({ grandeur: textes.grandeurs[0], figure: um.figure, nbpoints: um.nbpoints, genre: um.genre, f: om.f, n: om.n, o: om.o, p: om.p })
      }
    }
    if (ds.A_Cylindre) {
      const um = textes.formules[12]
      for (let j = 0; j < um.aire.length; j++) {
        const om = um.aire[j]
        stor.formulesPossibles.push({ grandeur: textes.grandeurs[1], figure: um.figure, nbpoints: um.nbpoints, genre: um.genre, f: om.f, n: om.n, o: om.o, p: om.p })
      }
    }
    if (ds.Al_Cylindre) {
      const um = textes.formules[12]
      for (let j = 0; j < um.airelaterale.length; j++) {
        const om = um.airelaterale[j]
        stor.formulesPossibles.push({ grandeur: textes.grandeurs[2], figure: um.figure, nbpoints: um.nbpoints, genre: um.genre, f: om.f, n: om.n, o: om.o, p: om.p })
      }
    }
    if (ds.V_Cylindre) {
      const um = textes.formules[12]
      for (let j = 0; j < um.volume.length; j++) {
        const om = um.volume[j]
        stor.formulesPossibles.push({ grandeur: textes.grandeurs[3], figure: um.figure, nbpoints: um.nbpoints, genre: um.genre, f: om.f, n: om.n, o: om.o, p: om.p })
      }
    }
    if (ds.P_Cone) {
      const um = textes.formules[14]
      for (let j = 0; j < um.perimetre.length; j++) {
        const om = um.perimetre[j]
        stor.formulesPossibles.push({ grandeur: textes.grandeurs[0], figure: um.figure, nbpoints: um.nbpoints, genre: um.genre, f: om.f, n: om.n, o: om.o, p: om.p })
      }
    }
    if (ds.A_Cone) {
      const um = textes.formules[14]
      for (let j = 0; j < um.aire.length; j++) {
        const om = um.aire[j]
        stor.formulesPossibles.push({ grandeur: textes.grandeurs[1], figure: um.figure, nbpoints: um.nbpoints, genre: um.genre, f: om.f, n: om.n, o: om.o, p: om.p })
      }
    }
    if (ds.Al_Cone) {
      const um = textes.formules[14]
      for (let j = 0; j < um.airelaterale.length; j++) {
        const om = um.airelaterale[j]
        stor.formulesPossibles.push({ grandeur: textes.grandeurs[2], figure: um.figure, nbpoints: um.nbpoints, genre: um.genre, f: om.f, n: om.n, o: om.o, p: om.p })
      }
    }
    if (ds.V_Cone) {
      const um = textes.formules[14]
      for (let j = 0; j < um.volume.length; j++) {
        const om = um.volume[j]
        stor.formulesPossibles.push({ grandeur: textes.grandeurs[3], figure: um.figure, nbpoints: um.nbpoints, genre: um.genre, f: om.f, n: om.n, o: om.o, p: om.p })
      }
    }

    if (!ds.Optionnel) {
      for (let i = stor.formulesPossibles.length - 1; i > -1; i--) {
        if (stor.formulesPossibles[i].p) {
          stor.formulesPossibles.splice(i, 1)
        }
      }
    }

    if (!ds.NotationCarreCube) {
      for (let i = stor.formulesPossibles.length - 1; i > -1; i--) {
        if (stor.formulesPossibles[i].f.indexOf('^') !== -1) { stor.formulesPossibles.splice(i, 1) }
      }
      for (let i = stor.ToutesformulesPossibles.length - 1; i > -1; i--) {
        if (stor.ToutesformulesPossibles[i].f.indexOf('^') !== -1) { stor.ToutesformulesPossibles.splice(i, 1) }
      }
    }
    stor.autoriseniv = [
      ds.niveau6e,
      ds.niveau5e,
      ds.niveau4e,
      ds.niveau3e
    ]
    // s’ils sont tous false on les passe tous à true
    if (stor.autoriseniv.every(n => n === false)) {
      stor.autoriseniv = [true, true, true, true]
    }
    for (let i = 0; i < 4; i++) {
      if (!stor.autoriseniv[i]) {
        for (let j = stor.formulesPossibles.length - 1; j > -1; j--) {
          if (stor.formulesPossibles[j].n === textes.niveaux[i]) {
            stor.formulesPossibles.splice(j, 1)
          }
        }
      }
    }
    if (stor.formulesPossibles.length === 0) {
      return j3pShowError(Error('Le paramétrage ne permet pas de construire l’exercice'))
    }

    for (let i = 0; i < stor.formulesPossibles.length; i++) {
      stor.formulesPossibles[i].f = '$' + stor.formulesPossibles[i].f + '$'
    }
    /*
             Par convention,`
            `   me.typederreurs[0] = nombre de bonnes réponses
                me.typederreurs[1] = nombre de fois où l’élève a utilisé une chance supplémentaire
                me.typederreurs[2] = nombre de mauvaises réponses
                me.typederreurs[10] = nombre de fois où l’élève a eu faux à cause d’un temps limite
                LES AUTRES peuvent être utilisées par la détermination de la pe dans le cas d’une section qualitative
             */
    me.typederreurs = [0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0]

    me.score = 0

    me.afficheTitre('Retrouver une formule')
    stor.bull = []
    enonceMain()
  }
  function enonceMain () {
    for (let i = stor.bull.length - 1; i > -1; i--) {
      stor.bull[i].disable()
      stor.bull.pop()
    }
    me.videLesZones()

    /// //////////////////////////////////
    /* LE CODE PRINCIPAL DE LA SECTION */
    /// //////////////////////////////////.

    stor.zoneliste = []
    stor.zonesclick = []
    stor.bull = []

    stor.lesdiv = {}
    stor.lesdiv.conteneur = j3pDiv(me.zones.MG, { style: me.styles.etendre('toutpetit.enonce', { padding: '10px' }) })
    const tt = addDefaultTable(stor.lesdiv.conteneur, 3, 1)
    const pout = addDefaultTable(tt[0][0], 2, 1)
    const tt1 = addDefaultTable(pout[1][0], 1, 3)
    stor.lesdiv.consigne = tt1[0][0]
    tt1[0][1].style.width = '40px'
    stor.lesdiv.correction = tt1[0][2]
    stor.lesdiv.travail = tt[1][0]
    stor.lesdiv.explication = tt[2][0]

    let cpt = 0
    let ok = true
    let i
    do {
      i = j3pGetRandomInt(0, (stor.formulesPossibles.length - 1))
      ok = (stor.dejapose.indexOf(i) === -1)
      cpt++
    }
    while ((!ok) && (cpt < 50))
    stor.dejapose.push(i)
    stor.laformule = stor.formulesPossibles[i]
    stor.reponsesPossibles = []
    stor.reponsespossiblesOp = []
    for (let i = 0; i < textes.formules.length; i++) {
      if (textes.formules[i].figure === stor.laformule.figure) {
        switch (stor.laformule.grandeur) {
          case textes.grandeurs[0]:for (let j = 0; j < textes.formules[i].perimetre.length; j++) {
            stor.reponsesPossibles.push(textes.formules[i].perimetre[j].f)
            if (textes.formules[i].perimetre[j].o) {
              stor.reponsespossiblesOp.push(textes.formules[i].perimetre[j].f)
            }
          }
            break
          case textes.grandeurs[1]:for (let j = 0; j < textes.formules[i].aire.length; j++) {
            stor.reponsesPossibles.push(textes.formules[i].aire[j].f)
            if (textes.formules[i].aire[j].o) {
              stor.reponsespossiblesOp.push(textes.formules[i].aire[j].f)
            }
          }
            break
          case textes.grandeurs[2]:for (let j = 0; j < textes.formules[i].airelaterale.length; j++) {
            stor.reponsesPossibles.push(textes.formules[i].airelaterale[j].f)
            if (textes.formules[i].airelaterale[j].o) {
              stor.reponsespossiblesOp.push(textes.formules[i].airelaterale[j].f)
            }
          }
            break
          case textes.grandeurs[3]:for (let j = 0; j < textes.formules[i].volume.length; j++) {
            stor.reponsesPossibles.push(textes.formules[i].volume[j].f)
            if (textes.formules[i].volume[j].o) {
              stor.reponsespossiblesOp.push(textes.formules[i].volume[j].f)
            }
          }
        }
      }
    }
    for (let i = 0; i < stor.reponsesPossibles.length; i++) {
      stor.reponsesPossibles[i] = '$' + stor.reponsesPossibles[i] + '$'
    }
    for (let i = 0; i < stor.reponsespossiblesOp.length; i++) {
      stor.reponsespossiblesOp[i] = '$' + stor.reponsespossiblesOp[i] + '$'
    }

    const mona = stor.laformule.grandeur
    const monb = textes.genres[stor.laformule.genre]
    const monc = stor.laformule.figure

    stor.lesdiv.travail.classList.add('travail')
    stor.lesdiv.consigne.classList.add('enonce')
    pout[0][0].classList.add('enonce')
    me.zonesElts.MG.classList.add('fond')

    stor.listezoneclik = [stor.laformule.f]
    cpt = 0
    for (let i = 0; i < ds.NbChoix - 1; i++) {
      do {
        cpt++
        ok = true
        const u = j3pGetRandomInt(0, (stor.formulesPossibles.length - 1))
        for (let j = 0; j < stor.listezoneclik.length; j++) {
          if (stor.listezoneclik[j] === stor.formulesPossibles[u].f) {
            ok = false
          }
        }
        ok = ok && stor.formulesPossibles[u].p !== true
        if (ok) {
          stor.listezoneclik.push(stor.formulesPossibles[u].f)
        }
      } while ((!ok) && (cpt < 100))
    }
    const diff = ds.NbChoix - stor.listezoneclik.length
    if (stor.listezoneclik.length < ds.NbChoix) {
      cpt = 0
      for (let i = 0; i < diff; i++) {
        let ok = true
        do {
          cpt++
          const u = j3pGetRandomInt(0, (stor.ToutesformulesPossibles.length - 1))
          for (let j = 0; j < stor.listezoneclik.length; j++) {
            if (stor.listezoneclik[j] === stor.ToutesformulesPossibles[u].f) {
              ok = false
            }
          }
          if (ok) { stor.listezoneclik.push('$' + stor.ToutesformulesPossibles[u].f + '$') }
        } while ((!ok) && (cpt < 10000))
      }
    }
    stor.listezoneclik = j3pShuffle(stor.listezoneclik)

    stor.zonesclick.push(new Zoneclick(stor.lesdiv.travail, stor.listezoneclik, { couleur: '#000000', dispo: 'carre', colonne: 3, reponsesmultiples: true, afaire: '', affiche: true, maxwidthcol: 250 })) // width:240,height:124

    stor.lesbonnesreponses = []
    for (let i = 0; i < stor.zonesclick[0].nb; i++) {
      if (stor.reponsesPossibles.indexOf(stor.zonesclick[0].montab[i]) !== -1) {
        stor.lesbonnesreponses.push(i)
      }
    }

    j3pAffiche(pout[0][0], null, '<b><u>' + textes.consigneG + '</u></b>')

    const mont = textes.consigne2.replace('£a', mona).replace('£b', monb).replace('£c', monc).replace('aire  d’un  cercle', 'aire d’un disque').replace('volume  d’une  sphère', 'volume d’une boule')
    j3pAffiche(stor.lesdiv.consigne, null, mont)
    if (ds.ObligeToutes) {
      j3pAffiche(stor.lesdiv.consigne, null, '\n<b>' + textes.obligtext + '</b>')
    }

    // Obligatoire

    me.finEnonce()
  }

  switch (this.etat) {
    case 'enonce':

      if (this.debutDeLaSection) {
        initSection()
      } else {
        enonceMain()
      }
      break // case "enonce":

    case 'correction':
      // On teste si une réponse a été saisie

      if (!yareponse()) {
        stor.lesdiv.correction.style.color = this.styles.cfaux
        stor.lesdiv.correction.innerHTML = reponseIncomplete
        this.afficheBoutonValider()
      } else {
        // Bonne réponse
        if (bonneReponse()) {
          stor.lesdiv.correction.style.color = this.styles.cbien
          stor.lesdiv.correction.innerHTML = cBien
          this._stopTimer()
          this.score++
          this.typederreurs[0]++
          this.cacheBoutonValider()

          /// /////////////////////////
          stor.zonesclick[0].corrige(stor.lesbonnesreponses)
          stor.zonesclick[0].disable()
          stor.zonesclick.pop()
          /// //////////////////////////

          this.etat = 'navigation'
          this.sectionCourante()
        } else {
          // Pas de bonne réponse
          stor.lesdiv.correction.style.color = this.styles.cfaux

          // A cause de la limite de temps :
          if (this.isElapsed) { // limite de temps
            this._stopTimer()
            stor.lesdiv.correction.innerHTML = tempsDepasse
            stor.lesdiv.correction.innerHTML += '<br>' + cFaux
            stor.lesdiv.correction.innerHTML += '<br>' + '<br>' + regardeCorrection
            this.typederreurs[10]++
            this.cacheBoutonValider()

            /// ///////////////////////
            for (let i = 0; i < stor.casesRattrappees.length; i++) {
              const chaine = textes.oubli
              let place = 1
              if (stor.zonesclick[0].etiqcol[stor.casesRattrappees[i]] === 1) {
                place = 2
              }
              if (stor.zonesclick[0].etiqcol[stor.casesRattrappees[i]] === stor.zonesclick[0].colonne) {
                place = 0
              }
              stor.bull[stor.bull.length] = new BulleAide(stor.zonesclick[0].recupidaide(stor.casesRattrappees[i]), chaine, { place })
            }
            for (const [i, caseCorrigee] of stor.casesCorrigees.entries()) {
              let u
              for (u = 0; u < stor.ToutesformulesPossibles.length; u++) {
                if (stor.ToutesformulesPossibles[u].f === stor.formulesCorrigees[i]) break
              }
              if (u === stor.ToutesformulesPossibles.length) continue
              const chaine = textes.tuaschoiz.replace('£a', stor.ToutesformulesPossibles[u].grandeur).replace('£b', textes.genres[stor.ToutesformulesPossibles[u].genre]).replace('£c', stor.ToutesformulesPossibles[u].figure)
              let place = 1
              if (stor.zonesclick[0].etiqcol[caseCorrigee] === 1) {
                place = 2
              }
              if (stor.zonesclick[0].etiqcol[caseCorrigee] === stor.zonesclick[0].colonne) {
                place = 0
              }
              stor.bull[stor.bull.length] = new BulleAide(stor.zonesclick[0].recupidaide(caseCorrigee), chaine, { place })
            }
            stor.zonesclick[0].corrige(stor.lesbonnesreponses, true)
            stor.zonesclick[0].disable()
            stor.zonesclick.pop()

            /// ///////////////////

            this.etat = 'navigation'
            this.sectionCourante()
          } else {
            stor.lesdiv.correction.innerHTML = cFaux

            if (this.essaiCourant < ds.nbchances) {
              stor.lesdiv.correction.innerHTML += '<br>' + essaieEncore
              this.typederreurs[1]++
              // indication éventuelle ici

              stor.zonesclick[0].corrigelesfaux(stor.lesbonnesreponses)

              /// ///////////////////////////
            } else {
              stor.lesdiv.correction.innerHTML += '<br>' + regardeCorrection
              for (let i = 0; i < stor.casesRattrappees.length; i++) {
                const chaine = textes.oubli
                let place = 1
                if (stor.zonesclick[0].etiqcol[stor.casesRattrappees[i]] === 1) {
                  place = 2
                }
                if (stor.zonesclick[0].etiqcol[stor.casesRattrappees[i]] === stor.zonesclick[0].colonne) {
                  place = 0
                }
                stor.bull[stor.bull.length] = new BulleAide(stor.zonesclick[0].recupidaide(stor.casesRattrappees[i]), chaine, { place })
              }
              for (const [i, caseCorrigee] of stor.casesCorrigees.entries()) {
                let u
                for (u = 0; u < stor.ToutesformulesPossibles.length; u++) {
                  if ('$' + stor.ToutesformulesPossibles[u].f + '$' === stor.formulesCorrigees[i]) {
                    break
                  }
                }
                const chaine = textes.tuaschoiz.replace('£a', stor.ToutesformulesPossibles[u].grandeur).replace('£b', textes.genres[stor.ToutesformulesPossibles[u].genre]).replace('£c', stor.ToutesformulesPossibles[u].figure)
                let place = 1
                if (stor.zonesclick[0].etiqcol[caseCorrigee] === 1) {
                  place = 2
                }
                if (stor.zonesclick[0].etiqcol[caseCorrigee] === stor.zonesclick[0].colonne) {
                  place = 0
                }
                stor.bull[stor.bull.length] = new BulleAide(stor.zonesclick[0].recupidaide(caseCorrigee), chaine, { place })
              }
              stor.zonesclick[0].corrige(stor.lesbonnesreponses, true)
              stor.zonesclick[0].disable()
              stor.zonesclick.pop()

              /// ////////////////////////////////

              this.typederreurs[2]++
              this.etat = 'navigation'
              this.sectionCourante()
            }
          }
        }
      }
      // Obligatoire
      this.finCorrection()
      break // case "correction":

    case 'navigation':

      if (this.sectionTerminee()) {
        // On détermine la phrase d’état renvoyée par la section
        this.parcours.pe = (ds.nbetapes * this.score) / ds.nbitems
        this.afficheBoutonSectionSuivante()
        this.focus('sectioncontinuer')
      } else {
        this.etat = 'enonce'
        this.afficheBoutonSuite()
        this.focus('boutoncontinuer')
      }
      // Obligatoire
      this.finNavigation()

      break // case "navigation":
  }
}
