import loadJqueryDialog from 'src/lib/core/loadJqueryDialog'
import getDefaultProgramme from './defaultProgramme'
import { j3pClone, j3pAjouteBouton, j3pArrondi, j3pEntierBienEcrit, j3pGetRandomBool, j3pGetRandomInt, j3pMathquillXcas, j3pNotify, j3pShowError, j3pPGCD, j3pAddElt, j3pEmpty, j3pStyle } from 'src/legacy/core/functions'
import Algebrite from 'algebrite'
import BoutonStyleMathquill from 'src/legacy/outils/boutonsStyleMatquill'
import { j3pAffiche } from 'src/lib/mathquill/functions'
import ZoneStyleMathquill2 from 'src/legacy/outils/zoneStyleMathquill/ZoneStyleMathquill2'
import BulleAide from 'src/legacy/outils/bulleAide/BulleAide'
import { j3pCreeFenetres, j3pAfficheCroixFenetres, j3pToggleFenetres } from 'src/legacy/core/functionsJqDialog'
import { addDefaultTable } from 'src/legacy/themes/table'
import Repere from 'src/legacy/outils/geometrie/Repere'
import Dialog from 'src/lib/widgets/dialog/Dialog'
import TableauConversionMobile from 'src/legacy/outils/tableauconversion/TableauConversionMobile'
import { getJ3pConteneur } from 'src/lib/core/domHelpers'
import ZoneStyleAffiche from 'src/legacy/outils/zoneStyleMathquill/zoneStyleAffiche'
import textesGeneriques from 'src/lib/core/textes'

const { cBien, cFaux, essaieEncore, regardeCorrection, reponseIncomplete, tempsDepasse } = textesGeneriques

/**
 * Ajouter une description ici
 * @fileOverview
 * @author Tommy Barroy <t.barroy@laposte.net>
 * @since 2017-10
*/
async function initEditor (container, values, btn) {
  const { default: editor } = await import('./editorProgramme.js')
  return editor(container, values, btn) // la fct qui prend un conteneur et les params comme arguments
}

export const params = {
  outils: ['calculatrice', 'mathquill'],
  /**
   * Ce qui est paramétrable dans l’éditeur de graphe
   * @type {LegacySectionParamsParametres}
   */
  parametres: [
    ['nbrepetitions', 9, 'entier', 'Nombre de répétitions de la section'],
    ['Parametrage_rapide', JSON.stringify(getDefaultProgramme()), 'multiEditor', 'Json figure de base et figure attendue.', initEditor],
    ['ChangementUnite', false, 'boolean', '<u>true</u>&nbsp;: Toutes les données n’ont pas forcément la même unité.\n\n <u>false</u>&nbsp;: Toutes les données sont exprimées dans la même unité.'],
    ['TableauxConversion', true, 'boolean', '<u>true</u>&nbsp;: Des tableaux de conversions sont disponibles\n\n <u>false</u>&nbsp;: non.'],
    ['CalculMental', false, 'boolean', '<u>true</u>&nbsp;: Les données sont des nombres simples.\n\n <u>false</u>&nbsp;: non.'],
    ['ApproximationDecimale', 'Aucune', 'liste', '<u>Exces_Defaut</u>&nbsp;: Approximation décimale par défaut ou par excès demandée.\n\n <u>Exces_Defaut_Arrondi</u>&nbsp;: Approximation décimale par défaut, par excès ou arrondi demandée.\n\n <u>Arrondi</u>&nbsp;: Arrondi demandé\n\n <u>indéfini</u>&nbsp;: Approximation décimale simple demandée, les deux réponses (par défaut ou par excès) sont acceptées.\n\n <u>Aucune</u>&nbsp;: Aucune approximation décimale n’est demandée.', ['Exces_Defaut', 'Exces_Defaut_Arrondi', 'Arrondi', 'indéfini', 'Aucune']],
    ['ValeurApprochee', false, 'boolean', '<u>true</u>&nbsp;: Pour les professeurs de collège qui préfèrent l’utilisation du terme «&nbsp;valeur approchée&nbsp;» dont la définition diffère au lycée.'],
    ['NotationGrandeur', 'FonctionLong', 'liste', '<u>IndiceCourt</u> aire de ABCD s’écrit A<sub>ABCD</sub>.\n\n <u>FonctionCourt</u>&nbsp;: elle s’écrit A(ABCD).\n\n <u>IndiceLong</u>&nbsp;: elle s’écrit Aire<sub>ABCD</sub>.\n\n <u>FonctionLong</u>&nbsp;: elle s’écrit Aire(ABCD).', ['IndiceCourt', 'FonctionCourt', 'IndiceLong', 'FonctionLong']],
    ['perimetre', false, 'boolean', '<u>true</u>&nbsp;: L’exercice propose des formules de périmètre\n\n <u>false</u>&nbsp;: non.'],
    ['aire', false, 'boolean', '<u>true</u>&nbsp;: L’exercice propose des formules d’aire\n\n <u>false</u>&nbsp;: non.'],
    ['aireLaterale', false, 'boolean', '<u>true</u>&nbsp;: L’exercice propose des formules d’aire latérale\n\n <u>false</u>&nbsp;: non.'],
    ['volume', false, 'boolean', '<u>true</u>&nbsp;: L’exercice propose des formules de volume\n\n <u>false</u>&nbsp;: non.'],
    ['niveau6e', false, 'boolean', '<u>true</u>&nbsp;: L’exercice propose des formules de niveau 6e.\n\n <u>false</u>&nbsp;: non.'],
    ['niveau5e', false, 'boolean', '<u>true</u>&nbsp;: L’exercice propose des formules de niveau 5e.\n\n <u>false</u>&nbsp;: non.'],
    ['niveau4e', false, 'boolean', '<u>true</u>&nbsp;: L’exercice propose des formules de niveau 4e.\n\n <u>false</u>&nbsp;: non.'],
    ['niveau3e', false, 'boolean', '<u>true</u>&nbsp;: L’exercice propose des formules de niveau 3e.\n\n <u>false</u>&nbsp;: non.'],
    ['Cercle', false, 'boolean', '<u>true</u>&nbsp;: L’exercice propose des formules concernant les cercles.\n\n <u>false</u>&nbsp;: non.'],
    ['Carre', false, 'boolean', '<u>true</u>&nbsp;: L’exercice propose des formules concernant les carrés.\n\n <u>false</u>&nbsp;: non.'],
    ['Quadrilatere', false, 'boolean', '<u>true</u>&nbsp;: L’exercice propose une formule pour un quadrilatère (périmètre uniquement).\n\n <u>false</u>&nbsp;: non.'],
    ['Rectangle', false, 'boolean', '<u>true</u>&nbsp;: L’exercice propose des formules concernant les rectangles.\n\n <u>false</u>&nbsp;: non.'],
    ['Losange', false, 'boolean', '<u>true</u>&nbsp;: L’exercice propose des formules concernant les losanges.\n\n <u>false</u>&nbsp;: non.'],
    ['Parallelogramme', false, 'boolean', '<u>true</u>&nbsp;: L’exercice propose des formules concernant les parallélogrammes.\n\n <u>false</u>&nbsp;: non.'],
    ['TriangleRectangle', false, 'boolean', '<u>true</u>&nbsp;: L’exercice propose des formules concernant les triangles rectangles.\n\n <u>false</u>&nbsp;: non.'],
    ['TriangleQuelconque', false, 'boolean', '<u>true</u>&nbsp;: L’exercice propose des formules concernant les triangles quelconques.\n\n <u>false</u>&nbsp;: non.'],
    ['TriangleEquilateral', false, 'boolean', '<u>true</u>&nbsp;: L’exercice propose des formules concernant les triangles équilatéraux.\n\n <u>false</u>&nbsp;: non.'],
    ['Secteur', false, 'boolean', '<u>true</u>&nbsp;: L’exercice propose des formules concernant les secteurs angulaires.\n\n <u>false</u>: non.'],
    ['Cube', false, 'boolean', '<u>true</u> : L’exercice propose des formules concernant les cubes.\n\n <u>false</u>: non.'],
    ['ParallelepipedeRectangle', false, 'boolean', '<u>true</u>&nbsp;: L’exercice propose des formules concernant les pavés droits.\n\n <u>false</u>: non.'],
    ['Cylindre', false, 'boolean', '<u>true</u> : L’exercice propose des formules concernant les cylindres.\n\n <u>false</u>: non.'],
    ['ConeDeRevolution', false, 'boolean', '<u>true</u>&nbsp;: L’exercice propose des formules concernant les cônes de révolution.\n\n <u>false</u>&nbsp;: non.'],
    ['Sphere', false, 'boolean', '<u>true</u> : L’exercice propose des formules concernant les sphères.\n\n <u>false</u>&nbsp;: non.'],
    ['Pyramide', false, 'boolean', '<u>true</u> : L’exercice propose des formules concernant les pyramides.\n\n <u>false</u>&nbsp;: non.'],
    ['PrismeDroit', false, 'boolean', '<u>true</u> : L’exercice propose des formules concernant les prismes droits.\n\n <u>false</u>&nbsp;: non.'],
    ['Optionnel', false, 'boolean', '<u>true</u>&nbsp;: L’exercice propose des formules non exigibles.\n\n <u>false</u>: non.'],
    ['NotationExpert', true, 'boolean', 'true&nbsp;: L’exercice peut proposer des formules présentées avec les notations carré ², cube ou fraction\n false&nbsp;: non'],
    // le modèle gère la compatibilité ascendante avec l’ancien paramètre MiseEnPagePlusClaire
    ['theme', 'standard', 'liste', 'Modèle pour l’affichage', ['standard', 'zonesAvecImageDeFond']]
  ],
  // dans le cas d’une section QUANTITATIVE
  pe: [
    { pe_1: 'Maitrise' },
    { pe_2: 'Suffisant' },
    { pe_3: 'Suffisant - erreur formule' },
    { pe_4: 'Suffisant - erreur élément de géométrie' },
    { pe_5: 'Suffisant - erreur conversion' },
    { pe_6: 'Suffisant - erreur valeur approchée' },
    { pe_7: 'Suffisant - erreur dans le remplacement des valeurs' },
    { pe_8: 'Suffisant - erreur de calcul' },
    { pe_9: 'Insuffisant' },
    { pe_10: 'Insuffisant - erreur formule' },
    { pe_11: 'Insuffisant - erreur élément de géométrie' },
    { pe_12: 'Insuffisant - erreur conversion' },
    { pe_13: 'Insuffisant - erreur valeur approchée' },
    { pe_14: 'Insuffisant - erreur dans le remplacement des valeurs' },
    { pe_15: 'Insuffisant - erreur de calcul' },
    { pe_16: 'insuffisant' },
    { pe_17: 'insuffisant - erreur formule' },
    { pe_18: 'insuffisant - erreur élément de géométrie' },
    { pe_19: 'insuffisant - erreur conversion' },
    { pe_20: 'insuffisant - erreur valeur approchée' },
    { pe_21: 'insuffisant - erreur dans le remplacement des valeurs' },
    { pe_22: 'insuffisant - erreur de calcul' }
  ]
}

const textes = {
  couleurexplique: '#A9E2F3',
  couleurcorrec: '#A9F5A9',
  couleurenonce: '#D8D8D8',
  couleurenonceg: '#F3E2A9',
  couleuretiquettes: '#A9E2F3',
  formules02: [
    {
      noms: { nom: '0123', cote: ['01', '12', '23', '30'], faux: ['02', '13'] },
      figure: 'carré',
      genre: 1,
      nbpoints: 4,
      perimetre: [{ f: '4 \\times \\text{côté}', n: '6e', o: false }],
      aire: [{ f: '\\text{côté}^{2}', n: '6e', o: false }, { f: '\\text{côté} \\times \\text{côté}', n: '6e', o: false }],
      volume: [],
      airelaterale: []
    },
    {
      noms: { nom: '(0)', rayon: ['12', '13'], diametre: ['23'], faux: ['34', '43'], ajout: 'de centre 1' },
      figure: 'cercle',
      genre: 1,
      nbpoints: 5,
      perimetre: [{
        f: '2 \\times \\pi \\times \\text{rayon}',
        n: '6e',
        o: false
      }, { f: '\\pi \\times \\text{diamètre}', n: '6e', o: true }],
      aire: [{
        f: '\\pi \\times \\text{rayon}^{2}',
        n: '6e',
        o: false
      }, { f: '\\pi \\times \\text{rayon} \\times \\text{rayon}', n: '6e', o: false }],
      volume: [],
      airelaterale: []
    },
    {
      noms: {
        nom: '0123',
        cote1: ['01', '10'],
        cote2: ['12', '21'],
        cote3: ['23', '32'],
        cote4: ['30', '03'],
        faux: ['02', '20', '13', '31']
      },
      figure: 'figure',
      genre: 0,
      nbpoints: 4,
      perimetre: [{ f: '\\text{somme des longueurs des côtés}', n: '6e', o: false }],
      aire: [],
      volume: [],
      airelaterale: []
    },
    {
      noms: { nom: '0123', longueur: ['23', '01'], largeur: ['12', '30'], faux: ['02', '13'] },
      figure: 'rectangle',
      genre: 1,
      nbpoints: 4,
      perimetre: [{ f: '2 \\times (\\text{longueur} + \\text{largeur})', n: '6e', o: false }],
      aire: [{ f: '\\text{longueur} \\times \\text{largeur}', n: '6e', o: false }],
      volume: [],
      airelaterale: []
    },
    {
      noms: {
        nom: '0123',
        cote: ['01', '12', '23', '30'],
        faux: ['14', '04'],
        petitediagonale: ['13'],
        grandediagonale: ['02'],
        base: ['01', '23'],
        hauteur: ['34']
      },
      figure: 'losange',
      genre: 1,
      nbpoints: 5,
      perimetre: [{ f: '4 \\times \\text{côté}', n: '6e', o: false }],
      aire: [{
        f: '\\frac{\\text{petite diagonale} \\times \\text{grande diagonale}}{2}',
        n: '6e',
        o: true
      }, {
        f: '(\\text{petite diagonale} \\times \\text{grande diagonale}) \\div 2',
        n: '6e',
        o: true
      }, { f: '\\text{base} \\times \\text{hauteur}', n: '5e', o: true }],
      volume: [],
      airelaterale: []
    },
    {
      noms: {
        nom: '0123',
        longueur: ['12', '30'],
        largeur: ['23', '01'],
        faux: ['02', '13'],
        base: ['01', '23'],
        hauteur: ['34']
      },
      figure: 'parallélogramme',
      genre: 1,
      nbpoints: 5,
      perimetre: [{ f: '2 \\times (\\text{longueur} + \\text{largeur})', n: '6e', o: true }],
      aire: [{ f: '\\text{base} \\times \\text{hauteur}', n: '5e', o: false }],
      volume: [],
      airelaterale: []
    },
    {
      noms: {
        nom: '012',
        ajout: 'rectangle en 0',
        faux: ['12'],
        fauxspec: ['01', '02'],
        base: ['12'],
        hauteur: ['03'],
        cote1: ['01'],
        cote2: ['02']
      },
      figure: 'triangle',
      figure2: 'triangle rectangle',
      genre: 1,
      nbpoints: 4,
      perimetre: [],
      aire: [{
        f: '\\frac{\\text{produit des côtés de l’angle droit}}{2}',
        n: '6e',
        o: false
      }, {
        f: '\\text{produit des côtés de l’angle droit} \\div 2',
        n: '6e',
        o: false
      }, { f: '\\frac{ \\text{base} \\times \\text{hauteur}}{2}', n: '5e', o: false }],
      volume: [],
      airelaterale: []
    },
    {
      noms: {
        nom: '012',
        cote1: ['01', '10'],
        cote2: ['12', '21'],
        cote3: ['20', '02'],
        faux: ['03', '02'],
        fauxspec: ['31', '03'],
        base: ['01'],
        hauteur: ['23']
      },
      figure: 'triangle quelconque',
      genre: 1,
      nbpoints: 4,
      perimetre: [{ f: '\\text{somme des longueurs des côtés}', n: '6e', o: false }],
      aire: [{ f: '\\frac{ \\text{base} \\times \\text{hauteur}}{2}', n: '5e', o: false }],
      volume: [],
      airelaterale: []
    },
    {
      noms: { nom: '012', cote: ['01', '12', '02'], base: ['01'], faux: ['14', '24'], hauteur: ['23'] },
      figure: 'triangle équilatéral',
      genre: 1,
      nbpoints: 5,
      perimetre: [{ f: '3 \\times \\text{côté}', n: '6e', o: false }],
      aire: [{ f: '\\frac{ \\text{base} \\times \\text{hauteur}}{2}', n: '5e', o: false }],
      volume: [],
      airelaterale: []
    },
    {
      noms: {
        nom: '01234567',
        cote: ['01', '12', '23', '30', '45', '56', '67', '74', '04', '15', '26', '37'],
        faux: ['24', '42', '06', '60', '17', '71']
      },
      figure: 'cube',
      genre: 1,
      nbpoints: 8,
      perimetre: [{ f: '12 \\times \\text{côté}', n: '6e', o: true }],
      aire: [{ f: '6 \\times \\text{côté}^{2}', n: '6e', o: true }],
      volume: [{ f: '\\text{côté}^{3}', n: '6e', o: false }, { f: '\\text{côté} \\times \\text{côté} \\times \\text{côté}', n: '6e', o: false }],
      airelaterale: []
    },
    {
      noms: {
        nom: '01234567',
        longueur: ['12', '30', '56', '74'],
        largeur: ['23', '01', '67', '45'],
        hauteur: ['04', '15', '26', '37'],
        faux: ['24', '42', '06', '60', '17', '71']
      },
      figure: 'parallélépipède rectangle',
      genre: 1,
      nbpoints: 8,
      perimetre: [{ f: '4 \\times (\\text{longueur} + \\text{largeur} + \\text{hauteur})', n: '6e', o: true }],
      aire: [{
        f: '2 \\times (\\text{longueur} \\times \\text{largeur} + \\text{largeur} \\times \\text{hauteur} + \\text{hauteur} \\times \\text{longueur})',
        n: '6e',
        o: true
      }],
      volume: [{ f: '\\text{longueur} \\times \\text{largeur} \\times \\text{hauteur}', n: '6e', o: false }],
      airelaterale: []
    },
    {
      noms: { nom: '(0)', rayon: ['12', '13'], angle: ['312', '213'], faux: ['23'], ajout: ' de centre 1 ' },
      figure: 'secteur de disque',
      genre: 1,
      nbpoints: 4,
      perimetre: [{
        f: '\\frac{2 \\times \\pi \\times \\text{rayon} \\times \\text{angle} }{360} + 2 \\times rayon',
        n: '6e',
        o: true
      }],
      aire: [{ f: '\\frac{ \\pi \\times \\text{rayon}^{2} \\times \\text{angle} }{360}', n: '5e', o: false }],
      volume: [],
      airelaterale: []
    },
    {
      noms: {
        nom: '(0)',
        rayon: ['12', '13'],
        diametre: ['23'],
        hauteur: ['34'],
        faux: ['24'],
        ajout: ', 1 est le centre d’une base,'
      },
      figure: 'cylindre',
      genre: 1,
      nbpoints: 5,
      perimetre: [{
        f: '4 \\times \\pi \\times \\text{rayon}',
        n: '5e',
        o: true
      }, { f: '2 \\times \\pi \\times \\text{diamètre}', n: '5e', o: true }],
      aire: [{
        f: '2 \\times \\pi \\times \\text{rayon} \\times \\text{hauteur} + 2 \\times \\pi \\times \\text{rayon}^{2}',
        n: '5e',
        o: true
      }],
      volume: [{ f: '\\pi \\times \\text{rayon}^{2} \\times \\text{hauteur}', n: '5e', o: false }],
      airelaterale: [{ f: '2 \\times \\pi \\times \\text{rayon} \\times \\text{hauteur}', n: '5e', o: false }]
    },
    {
      noms: {
        nom: '(0)',
        rayon: ['12', '13'],
        diametre: ['23'],
        hauteur: ['14'],
        faux: ['24', '34'],
        ajout: 'de centre 1 et de sommet 4'
      },
      figure: 'cône de révolution',
      genre: 1,
      nbpoints: 5,
      perimetre: [{
        f: '2 \\times \\pi \\times \\text{rayon}',
        n: '4e',
        o: true
      }, { f: '\\pi \\times \\text{diamètre}', n: '4e', o: true }],
      airelaterale: [],
      volume: [{ f: '\\frac{\\pi \\times \\text{rayon}^{2} \\times \\text{hauteur}}{3}', n: '4e', o: false }],
      aire: []
    },
    {
      noms: { nom: '(0)', rayon: ['12', '13'], diametre: ['23'], faux: ['24', '42'], ajout: 'de centre 1' },
      figure: 'sphère',
      genre: 0,
      nbpoints: 5,
      perimetre: [],
      aire: [{ f: '4 \\times \\pi \\times \\text{rayon}^{2}', n: '3e', o: false }],
      volume: [{ f: '\\frac{4}{3} \\times \\pi \\times \\text{rayon}^{3}', n: '3e', o: false }],
      airelaterale: []
    },
    {
      noms: { nom: '(0)', ajout: 'de sommet 1', hauteur: ['67'], faux: ['46', '25', '13', '14'] },
      figure: 'pyramide',
      genre: 0,
      nbpoints: 8,
      perimetre: [],
      aire: [],
      volume: [{ f: '\\frac{\\text{aire de la base} \\times \\text{hauteur}}{3}', n: '4e', o: false }],
      airelaterale: []
    },
    {
      noms: { nom: '(0)', hauteur: ['16', '27', '38', '49'], faux: ['12', '13', '24', '59'] },
      figure: 'prisme droit',
      genre: 1,
      nbpoints: 11,
      perimetre: [],
      aire: [{
        f: '\\text{périmètre de la base} \\times \\text{hauteur} + 2 \\times \\text{aire de la base}',
        n: '5e',
        o: true
      }],
      volume: [{ f: '\\text{aire de la base} \\times \\text{hauteur}', n: '5e', o: false }],
      airelaterale: [{ f: '\\text{périmètre de la base} \\times \\text{hauteur}', n: '5e', o: false }]
    }
  ],
  Mesures: 'Dans cette figure,\n les mesures ne sont\n pas respectées.',
  ConsigneGtex: '<b> On veut calculer £a en £f £b £c $£d$ £e tel que&nbsp;:</b>',
  ConsigneTex: '<b> • $ £a = £b $ £c.</b>',
  Etape1: '<b><u>Étape 1</u>&nbsp;: Écrire la formule avec les noms des grandeurs.</b><br>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;<i>( Utilise les boutons pour écrire des lettres. )</i>',
  Etape2: '<b><u>Étape 2</u>&nbsp;: Remplacer les grandeurs par leur valeur.</b>',
  Etape3: '<b><u>Étape 3</u>&nbsp;: Calculer la valeur exacte.</b>',
  Etape4: ['<b><u>Étape 4</u>&nbsp;: Donner une valeur approchée.</b>', '<b><u>Etape 4</u>&nbsp;: Donner une approximation décimale.</b>'],
  CoFoQuoi1: [
    [
      'Tu as inversé valeur approchée par défaut et valeur approchée par excès&nbsp;!',
      'Tu as inversé valeur approchée par défaut et valeur approchée par excès&nbsp;!',
      'Tu n’as pas choisi l’arrondi&nbsp;!'],
    [
      'Tu as inversé approximation décimale par défaut et approximation décimale par excès&nbsp;!',
      'Tu as inversé approximation décimale par défaut et approximation décimale par excès&nbsp;!',
      'Tu n’as pas choisi l’arrondi&nbsp;!'
    ]
  ],
  Arrond: ['à la dizaine', 'à l’unité', 'au dixième', 'au centième', 'au millième'],
  Vapp1: ['(valeur approchée £b £a)', '(approximation décimale £b £a)'],
  Vapp2: ['(arrondi £a)', '(arrondi £a)'],
  Vapp3: ['(valeur approchée £a)', '(approximation décimale £a)'],
  parquoi: ['par défaut', 'par excès', '', ''],
  debut: '£a $_{ £b }  = $',
  debut1: '£a$(£b)  = $',
  debut2: '£a$ £b   = $',
  debut7: '£a$_{ £b } ≈ $',
  debut8: '£a$ (£b) ≈ $',
  debut9: '£a$  £b  ≈ $',
  aidebouton: 'Clique sur ce bouton \n si tu penses que le résultat est une fraction.',
  unites: ['km', 'hm', 'dam', 'm', 'dm', 'cm', 'mm'],
  Cobon1: 'En effet,  £a =  $£b$.',
  CoFo1: 'On sait que £a =  $£b$,',
  CoFo2: ' $£a$.',
  donc: 'donc ',
  Vexacte: ' (valeur exacte)',
  CoFoconv1: 'Attention aux conversions&nbsp;!',
  aide1: 'L’utilisation de £a n’est vraiment pas pertinente, je simplifie ta formule.',
  Cobon2: 'Le calcul est juste, mais je ne retrouve pas les nombres à leur place. ',
  ConsigneTexte: 'Pour calculer £a £b £c, on peut utiliser la (ou les) formule(s) suivante(s)&nbsp;: ',
  niveau: ['6e', '5e', '4e', '3e'],
  genre: [' de la ', ' du '],
  genre2: [' d’une ', ' d’un '],
  grandeur: ['le périmètre', 'l’aire', 'l’aire latérale', 'le volume'],
  tuaschoiz: 'Cette formule permet de calculer\n £a £b £c&nbsp;!',
  obligtext: 'Tu dois sélectionner toutes les formules02 correctes.'

}

/**
 * section formules02
 * @this {Parcours}
 */
export default function main () {
  // const prefixe = 'formules02'
  const me = this
  const ds = me.donneesSection
  const stor = this.storage
  const caracspe = ['¿', '•', '‡', '¶', '©', '®', '™', '◊', '♠', '♣', '♥']

  function creeLesDiv () {
    stor.lesdiv = {}
    stor.lesdiv.conteneur = j3pAddElt(me.zonesElts.MG, 'div', {
      style: me.styles.etendre('toutpetit.enonce', { padding: '10px' })
    })
    const tt = addDefaultTable(stor.lesdiv.conteneur, 2, 1)
    modif(tt)
    const tt1 = addDefaultTable(tt[0][0], 1, 2)
    modif(tt1)
    const tt2 = addDefaultTable(tt[1][0], 2, 4)
    modif(tt2)
    tt2[1][0].style.height = '7px'
    tt2[0][1].style.width = '10px'
    tt2[0][3].style.width = '10px'
    stor.lesdiv.titre = tt1[0][0]
    stor.lesdiv.aideg = tt1[0][1]
    stor.lesdiv.consigne = tt2[0][0]
    stor.lesdiv.figure = tt2[0][2]
    stor.lesdiv.etape = addDefaultTable(stor.lesdiv.conteneur, 1, 1)[0][0]
    stor.lesdiv.travail = addDefaultTable(stor.lesdiv.conteneur, 1, 1)[0][0]
    stor.lesdiv.l4ligne = addDefaultTable(stor.lesdiv.travail, 4, 1)
    modif(stor.lesdiv.l4ligne)
    stor.lesdiv.explications = j3pAddElt(stor.lesdiv.conteneur, 'div')
    stor.lesdiv.solution = j3pAddElt(stor.lesdiv.conteneur, 'div')
    stor.lesdiv.convers = j3pAddElt(me.zonesElts.MD, 'div', {
      style: me.styles.etendre('toutpetit.enonce', { padding: '10px' })
    })
    stor.lesdiv.calculatrice = j3pAddElt(me.zonesElts.MD, 'div', {
      style: me.styles.etendre('toutpetit.enonce', { padding: '10px' })
    })
    stor.lesdiv.correction = j3pAddElt(me.zonesElts.MD, 'div', {
      style: me.styles.etendre('toutpetit.enonce', { padding: '10px' })
    })
    stor.lesdiv.travail.classList.add('travail')
    tt[0][0].parentNode.parentNode.classList.add('enonce')
    stor.lesdiv.explications.style.color = me.styles.cfaux
    stor.lesdiv.solution.style.color = me.styles.petit.correction.color
    stor.lesdiv.etape.style.color = me.styles.petit.correction.color
  }
  function ressemble (st1, st2) {
    if (st1.length !== st2.length) return false
    let jev
    for (let i = 0; i < st1.length; i++) {
      jev = false
      for (let j = 0; j < st2.length; j++) {
        if (st1[i] === st2[j]) jev = true
      }
      if (!jev) return false
    }
    for (let i = 0; i < st2.length; i++) {
      jev = false
      for (let j = 0; j < st1.length; j++) {
        if (st2[i] === st1[j]) jev = true
      }
      if (!jev) return false
    }
    return true
  }
  function extnum (st1) {
    if ((st1 === undefined) || (typeof st1 !== 'string')) return 'NaN'
    let i
    for (i = 1; i < st1.length; i++) {
      if (st1[i] === 'D') break
    }
    return parseFloat(st1.substring(1, i))
  }

  function extden (st1) {
    if ((st1 === undefined) || (typeof st1 !== 'string')) return 'NaN'
    let i
    for (i = 1; i < st1.length; i++) {
      if (st1[i] === 'D') break
    }
    return parseFloat(st1.substring(i + 1, st1.length - 1))
  }

  function extrait (st3) {
    let monnum = extnum(st3)
    let monden = extden(st3)
    if ((monnum === 'NaN') || (monden === 'NaN') || isNaN(monnum) || isNaN(monden)) return 'NaN'
    const pg = j3pPGCD(monnum, monden, { negativesAllowed: true, valueIfZero: 1 })
    monnum = Math.trunc(monnum / pg)
    monden = Math.trunc(monden / pg)
    const deci = monnum / monden
    let buf = monden
    while ((buf % 2) === 0) buf = Math.trunc(buf / 2)
    while ((buf % 5) === 0) buf = Math.trunc(buf / 5)
    const exa = (buf === 1)
    return { num: monnum, den: monden, decimale: deci, exacte: exa }
  }

  function valp (car) {
    const listeprio = ['UD0123456789.', '+-', '*/', '^', '(']
    let laprio = -1
    for (let j = 0; j < 5; j++) {
      if (listeprio[j].indexOf(car) !== -1) {
        laprio = j
        break
      }
    }

    return laprio
  } // valp
  function modif (ki) {
    for (let i = 0; i < ki.length; i++) {
      for (let j = 0; j < ki[i].length; j++) {
        ki[i][j].style.padding = '0px'
      }
    }
  }
  function chercheprio (chaine) {
    const arenvoie = {
      deb: 0,
      fin: 0,
      prio: -1,
      ope: '',
      operan1: 0,
      operan2: 0
    }
    for (let i = 0; i < chaine.length; i++) {
      const tt = valp(chaine[i])
      if (tt > arenvoie.prio) {
        arenvoie.deb = i
        arenvoie.fin = i
        arenvoie.prio = tt
        arenvoie.ope = chaine[i]
      }
    }

    if (arenvoie.prio === 0) {
      arenvoie.deb = 0
      arenvoie.fin = chaine.length
      arenvoie.ope = ''
      return arenvoie
    }

    if (arenvoie.ope !== '(') {
      for (let i = arenvoie.deb - 1; i > -2; i--) {
        if (i === -1) {
          arenvoie.deb = 0
          arenvoie.operan1 = chaine.substring(arenvoie.deb, arenvoie.fin)
          break
        }
        if (valp(chaine[i]) > 0) {
          arenvoie.deb = i + 1
          arenvoie.operan1 = chaine.substring(arenvoie.deb, arenvoie.fin)
          break
        }
      }
      const garde = arenvoie.fin + 1
      for (let i = arenvoie.fin + 1; i < chaine.length + 1; i++) {
        if (i === chaine.length) {
          arenvoie.fin = chaine.length
          arenvoie.operan2 = chaine.substring(garde, chaine.length)
          break
        }
        if (valp(chaine[i]) > 0) {
          arenvoie.fin = i - 1
          arenvoie.operan2 = chaine.substring(garde, arenvoie.fin + 1)
          break
        }
      }
      return arenvoie
    }
    let comptepar = 0
    for (let i = arenvoie.deb + 1; i < chaine.length; i++) {
      if (chaine[i] === '(') comptepar++
      if (chaine[i] === ')') comptepar--
      if (comptepar === -1) {
        arenvoie.fin = i
        arenvoie.operan1 = chaine.substring(arenvoie.deb + 1, arenvoie.fin)
        return arenvoie
      }
    }
    return arenvoie
  } // chercheprio

  function trans (str) {
    if (str[0] === 'U') return str
    if (str.indexOf('.') === -1) return 'U' + str + 'D1U'
    for (let i = 1; i < 13; i++) {
      if (Math.abs(Math.trunc(parseFloat(str) * Math.pow(10, i)) - (parseFloat(str) * Math.pow(10, i))) < Math.pow(10, -12)) {
        return 'U' + Math.trunc(parseFloat(str) * Math.pow(10, i)) + 'D' + Math.trunc(Math.pow(10, i)) + 'U'
      }
    }
  } // trans

  function moncalc (exp) {
    let prio, num1, den1, num2, den2
    try {
      // mon code qui peut parfois planter
      prio = chercheprio(exp)

      if (prio.ope !== '(' && prio.ope !== '') {
        prio.operan1 = trans(prio.operan1)
        prio.operan2 = trans(prio.operan2)
        num1 = extnum(prio.operan1)
        den1 = extden(prio.operan1)
        num2 = extnum(prio.operan2)
        den2 = extden(prio.operan2)
      }
    } catch (error) {
      // un objet qcq avec les infos utiles pour le débug
      const dataSup = {
        j3p: {
          description: 'Dans la section formules02',
          prio,
          exp,
          formule: stor.laformule,
          resultav: stor.monagarde
        }
      }
      j3pNotify(error, dataSup)
    }

    if (prio.ope === '') return exp
    let val
    switch (prio.ope) {
      case '+' :
        val = 'U' + Math.trunc(num1 * den2 + num2 * den1) + 'D' + Math.trunc(den1 * den2) + 'U'
        break
      case '-' :
        val = 'U' + Math.trunc(num1 * den2 - num2 * den1) + 'D' + Math.trunc(den1 * den2) + 'U'
        break
      case '*' :
        val = 'U' + Math.floor(num1 * num2) + 'D' + Math.floor(den1 * den2) + 'U'
        break
      case '/' :
        val = 'U' + Math.trunc(num1 * den2) + 'D' + Math.trunc(den1 * num2) + 'U'
        break
      case '^' :
        val = 'U' + Math.trunc(Math.pow(num1, num2 / den2)) + 'D' + Math.trunc(Math.pow(den1, num2 / den2)) + 'U'
        break
      case '(' :
        val = moncalc(prio.operan1)
        break
      default:
        console.error(Error('prio.ope non géré : ' + prio.ope))
    }

    const reste = exp.substring(0, prio.deb) + val + exp.substring(prio.fin + 1, exp.length)
    return moncalc(reste)
  }

  function testconv (nb1, nb2) {
    let err = false
    for (let i = -15; i < 15; i++) {
      if (i === 0) continue
      if (Math.abs(nb1 - nb2 * Math.pow(10, i)) < Math.pow(10, -12)) {
        err = true
        break
      }
    }
    return err
  }

  function shuffle (n) {
    if (!n) {
      n = this.length
    }
    if (n > 1) {
      const i1 = j3pGetRandomInt(0, (stor.lesmesures.length - 1))
      const i2 = j3pGetRandomInt(0, (stor.lesmesures.length - 1))
      const tmp1 = stor.lesmesures[i1]
      const tmp2 = stor.lesboutons[i1]
      const tmp3 = stor.lesboutonsfonc[i1]
      const tmp4 = stor.quoi[i1]
      const tmp5 = stor.lesunite[i1]
      stor.lesmesures[i1] = stor.lesmesures[i2]
      stor.lesboutons[i1] = stor.lesboutons[i2]
      stor.lesboutonsfonc[i1] = stor.lesboutonsfonc[i2]
      stor.quoi[i1] = stor.quoi[i2]
      stor.lesunite[i1] = stor.lesunite[i2]
      stor.lesmesures[i2] = tmp1
      stor.lesboutons[i2] = tmp2
      stor.lesboutonsfonc[i2] = tmp3
      stor.quoi[i2] = tmp4
      stor.lesunite[i2] = tmp5
      shuffle(n - 1)
    }
  }

  function boutonfen () {
    j3pToggleFenetres('Calculatrice')
  }

  function affAide () {
    if (!stor.dialog) return console.error(Error('Pas de dialog disponible pour l’afficher'))
    stor.dialog.toggle()
  }

  function fig () {
    const fifig = []

    switch (stor.laformule.figure) {
      case 'carré':
        fifig.push({
          type: 'point',
          nom: stor.leslettres[0],
          par1: 10,
          par2: 2,
          fixe: true,
          visible: true,
          etiquette: true,
          style: { couleur: '#000000', epaisseur: 0, taillepoint: 4, decal: [-20, +20] }
        })
        fifig.push({
          type: 'point',
          nom: stor.leslettres[1],
          par1: 20,
          par2: 3,
          fixe: true,
          visible: true,
          etiquette: true,
          style: { couleur: '#000000', epaisseur: 0, taillepoint: 4, decal: [+15, +10] }
        })
        fifig.push({
          type: 'point',
          nom: stor.leslettres[2],
          par1: 19,
          par2: 13,
          fixe: true,
          visible: true,
          etiquette: true,
          style: { couleur: '#000000', epaisseur: 0, taillepoint: 4, decal: [+15, +15] }
        })
        fifig.push({
          type: 'point',
          nom: stor.leslettres[3],
          par1: 9,
          par2: 12,
          fixe: true,
          visible: true,
          etiquette: true,
          style: { couleur: '#000000', epaisseur: 0, taillepoint: 4, decal: [-20, +20] }
        })
        fifig.push({
          type: 'segment',
          par1: stor.leslettres[0],
          par2: stor.leslettres[1],
          style: { couleur: '#000000', epaisseur: 3 }
        })
        fifig.push({
          type: 'segment',
          par1: stor.leslettres[1],
          par2: stor.leslettres[2],
          style: { couleur: '#000000', epaisseur: 3 }
        })
        fifig.push({
          type: 'segment',
          par1: stor.leslettres[2],
          par2: stor.leslettres[3],
          style: { couleur: '#000000', epaisseur: 3 }
        })
        fifig.push({
          type: 'segment',
          par1: stor.leslettres[3],
          par2: stor.leslettres[0],
          style: { couleur: '#000000', epaisseur: 3 }
        })
        break
      case 'figure':
        fifig.push({
          type: 'point',
          nom: stor.leslettres[0],
          par1: 10,
          par2: 2,
          fixe: true,
          visible: true,
          etiquette: true,
          style: { couleur: '#000000', epaisseur: 0, taillepoint: 4, decal: [-20, +20] }
        })
        fifig.push({
          type: 'point',
          nom: stor.leslettres[1],
          par1: 26,
          par2: 8,
          fixe: true,
          visible: true,
          etiquette: true,
          style: { couleur: '#000000', epaisseur: 0, taillepoint: 4, decal: [+15, +10] }
        })
        fifig.push({
          type: 'point',
          nom: stor.leslettres[2],
          par1: 19,
          par2: 13,
          fixe: true,
          visible: true,
          etiquette: true,
          style: { couleur: '#000000', epaisseur: 0, taillepoint: 4, decal: [+15, +15] }
        })
        fifig.push({
          type: 'point',
          nom: stor.leslettres[3],
          par1: 7,
          par2: 12,
          fixe: true,
          visible: true,
          etiquette: true,
          style: { couleur: '#000000', epaisseur: 0, taillepoint: 4, decal: [-20, +20] }
        })
        fifig.push({
          type: 'segment',
          par1: stor.leslettres[0],
          par2: stor.leslettres[1],
          style: { couleur: '#000000', epaisseur: 3 }
        })
        fifig.push({
          type: 'segment',
          par1: stor.leslettres[1],
          par2: stor.leslettres[2],
          style: { couleur: '#000000', epaisseur: 3 }
        })
        fifig.push({
          type: 'segment',
          par1: stor.leslettres[2],
          par2: stor.leslettres[3],
          style: { couleur: '#000000', epaisseur: 3 }
        })
        fifig.push({
          type: 'segment',
          par1: stor.leslettres[3],
          par2: stor.leslettres[0],
          style: { couleur: '#000000', epaisseur: 3 }
        })
        break
      case 'rectangle' :
        fifig.push({
          type: 'point',
          nom: stor.leslettres[0],
          par1: 5,
          par2: 1,
          fixe: true,
          visible: true,
          etiquette: true,
          style: { couleur: '#000000', epaisseur: 0, taillepoint: 4, decal: [-15, +20] }
        })
        fifig.push({
          type: 'point',
          nom: stor.leslettres[1],
          par1: 25,
          par2: 3,
          fixe: true,
          visible: true,
          etiquette: true,
          style: { couleur: '#000000', epaisseur: 0, taillepoint: 4, decal: [+15, +10] }
        })
        fifig.push({
          type: 'point',
          nom: stor.leslettres[2],
          par1: 24,
          par2: 13,
          fixe: true,
          visible: true,
          etiquette: true,
          style: { couleur: '#000000', epaisseur: 0, taillepoint: 4, decal: [+15, +15] }
        })
        fifig.push({
          type: 'point',
          nom: stor.leslettres[3],
          par1: 4,
          par2: 11,
          fixe: true,
          visible: true,
          etiquette: true,
          style: { couleur: '#000000', epaisseur: 0, taillepoint: 4, decal: [-15, +20] }
        })
        fifig.push({
          type: 'segment',
          par1: stor.leslettres[0],
          par2: stor.leslettres[1],
          style: { couleur: '#000000', epaisseur: 3 }
        })
        fifig.push({
          type: 'segment',
          par1: stor.leslettres[1],
          par2: stor.leslettres[2],
          style: { couleur: '#000000', epaisseur: 3 }
        })
        fifig.push({
          type: 'segment',
          par1: stor.leslettres[2],
          par2: stor.leslettres[3],
          style: { couleur: '#000000', epaisseur: 3 }
        })
        fifig.push({
          type: 'segment',
          par1: stor.leslettres[3],
          par2: stor.leslettres[0],
          style: { couleur: '#000000', epaisseur: 3 }
        })
        break
      case 'cercle' :
        fifig.push({
          type: 'point',
          nom: stor.leslettres[2],
          par1: 20.3,
          par2: 12.3,
          fixe: true,
          visible: true,
          etiquette: true,
          style: { couleur: '#000000', epaisseur: 0, taillepoint: 4, decal: [+20, +20] }
        })
        fifig.push({
          type: 'point',
          nom: stor.leslettres[3],
          par1: 11.7,
          par2: 3.7,
          fixe: true,
          visible: true,
          etiquette: true,
          style: { couleur: '#000000', epaisseur: 0, taillepoint: 4, decal: [-20, +20] }
        })
        fifig.push({
          type: 'point',
          nom: stor.leslettres[4],
          par1: 21.2,
          par2: 4.7,
          fixe: true,
          visible: true,
          etiquette: true,
          style: { couleur: '#000000', epaisseur: 0, taillepoint: 4, decal: [+15, +20] }
        })
        fifig.push({
          type: 'segment',
          par1: stor.leslettres[2],
          par2: stor.leslettres[3],
          style: { couleur: '#000000', epaisseur: 0.5 }
        })
        fifig.push({
          type: 'segment',
          par1: stor.leslettres[4],
          par2: stor.leslettres[3],
          style: { couleur: '#000000', epaisseur: 0.5 }
        })

        fifig.push({
          type: 'point',
          nom: 'c',
          par1: 16,
          par2: 8,
          fixe: true,
          visible: false,
          etiquette: false,
          style: { couleur: '#000000', epaisseur: 0, taillepoint: 4, decal: [-20, +20] }
        })
        fifig.push({
          type: 'point',
          nom: '(' + stor.leslettres[0] + ')',
          par1: 10,
          par2: 8,
          fixe: true,
          visible: true,
          etiquette: true,
          style: { couleur: '#000000', epaisseur: 0, taillepoint: 4, decal: [-30, +20] }
        })
        fifig.push({
          type: 'point',
          nom: stor.leslettres[1],
          par1: 15,
          par2: 9,
          fixe: true,
          visible: true,
          etiquette: true,
          style: { couleur: '#000000', epaisseur: 0, taillepoint: 4, decal: [-10, +20] }
        })
        fifig.push({
          type: 'point',
          nom: 'cdh',
          par1: 15,
          par2: 7,
          fixe: true,
          visible: true,
          etiquette: false,
          style: { couleur: '#000000', epaisseur: 0, taillepoint: 4, decal: [+15, +10] }
        })
        fifig.push({
          type: 'point',
          nom: 'cgb',
          par1: 17,
          par2: 9,
          fixe: true,
          visible: true,
          etiquette: false,
          style: { couleur: '#000000', epaisseur: 0, taillepoint: 4, decal: [+15, +15] }
        })
        fifig.push({
          type: 'point',
          nom: 'cgh',
          par1: 17,
          par2: 7,
          fixe: true,
          visible: true,
          etiquette: false,
          style: { couleur: '#000000', epaisseur: 0, taillepoint: 4, decal: [-20, +20] }
        })
        fifig.push({
          type: 'segment',
          par1: stor.leslettres[1],
          par2: 'cgh',
          style: { couleur: '#000000', epaisseur: 1 }
        })
        fifig.push({ type: 'segment', par1: 'cgb', par2: 'cdh', style: { couleur: '#000000', epaisseur: 1 } })
        fifig.push({
          type: 'cercle',
          par1: 'c',
          par2: '(' + stor.leslettres[0] + ')',
          style: { couleur: '#000000', epaisseur: 3, opaciteRemplissage: 0 }
        })
        break
      case 'losange' :
        fifig.push({
          type: 'point',
          nom: stor.leslettres[0],
          par1: 5,
          par2: 4,
          fixe: true,
          visible: true,
          etiquette: true,
          style: { couleur: '#000000', epaisseur: 0, taillepoint: 4, decal: [-15, +20] }
        })
        fifig.push({
          type: 'point',
          nom: stor.leslettres[1],
          par1: 14,
          par2: 6,
          fixe: true,
          visible: true,
          etiquette: true,
          style: { couleur: '#000000', epaisseur: 0, taillepoint: 4, decal: [+15, +30] }
        })
        fifig.push({
          type: 'point',
          nom: stor.leslettres[2],
          par1: 20,
          par2: 13,
          fixe: true,
          visible: true,
          etiquette: true,
          style: { couleur: '#000000', epaisseur: 0, taillepoint: 4, decal: [+15, +15] }
        })
        fifig.push({
          type: 'point',
          nom: stor.leslettres[3],
          par1: 11,
          par2: 11,
          fixe: true,
          visible: true,
          etiquette: true,
          style: { couleur: '#000000', epaisseur: 0, taillepoint: 4, decal: [-15, +10] }
        })
        fifig.push({
          type: 'segment',
          par1: stor.leslettres[0],
          par2: stor.leslettres[1],
          style: { couleur: '#000000', epaisseur: 3 }
        })
        fifig.push({
          type: 'segment',
          par1: stor.leslettres[1],
          par2: stor.leslettres[2],
          style: { couleur: '#000000', epaisseur: 3 }
        })
        fifig.push({
          type: 'segment',
          par1: stor.leslettres[2],
          par2: stor.leslettres[3],
          style: { couleur: '#000000', epaisseur: 3 }
        })
        fifig.push({
          type: 'segment',
          par1: stor.leslettres[3],
          par2: stor.leslettres[0],
          style: { couleur: '#000000', epaisseur: 3 }
        })
        fifig.push({
          type: 'segment',
          par1: stor.leslettres[2],
          par2: stor.leslettres[0],
          style: { couleur: '#000000', epaisseur: 1 }
        })
        fifig.push({
          type: 'segment',
          par1: stor.leslettres[3],
          par2: stor.leslettres[1],
          style: { couleur: '#000000', epaisseur: 1 }
        })
        fifig.push({
          type: 'point',
          nom: stor.leslettres[4],
          par1: 12.2,
          par2: 5.6,
          fixe: true,
          visible: true,
          etiquette: true,
          style: { couleur: '#000000', epaisseur: 0, taillepoint: 4, decal: [-10, +40] }
        })
        fifig.push({
          type: 'segment',
          par1: stor.leslettres[4],
          par2: stor.leslettres[3],
          style: { couleur: '#000000', epaisseur: 1 }
        })
        if (stor.autoriseniv[1] || stor.autoriseniv[2] || stor.autoriseniv[3]) {
          fifig.push({
            type: 'point',
            nom: 'a1',
            par1: 11,
            par2: 6.4,
            fixe: true,
            visible: false,
            etiquette: false,
            style: { couleur: '#ff0000', epaisseur: 1, taillepoint: 4, decal: [-15, +10] }
          })
          fifig.push({
            type: 'point',
            nom: 'a2',
            par1: 12,
            par2: 6.7,
            fixe: true,
            visible: false,
            etiquette: false,
            style: { couleur: '#00ff00', epaisseur: 1, taillepoint: 4, decal: [-15, +10] }
          })
          fifig.push({
            type: 'point',
            nom: 'a3',
            par1: 11.2,
            par2: 5.4,
            fixe: true,
            visible: false,
            etiquette: false,
            style: { couleur: '#0000ff', epaisseur: 1, taillepoint: 4, decal: [-15, +10] }
          })
          fifig.push({ type: 'segment', par1: 'a1', par2: 'a2', style: { couleur: '#000000', epaisseur: 1 } })
          fifig.push({ type: 'segment', par1: 'a1', par2: 'a3', style: { couleur: '#000000', epaisseur: 1 } })
        }
        break
      case 'triangle' :
        fifig.push({
          type: 'point',
          nom: stor.leslettres[1],
          par1: 5,
          par2: 1,
          fixe: true,
          visible: true,
          etiquette: true,
          style: { couleur: '#000000', epaisseur: 0, taillepoint: 4, decal: [-15, +20] }
        })
        fifig.push({
          type: 'point',
          nom: stor.leslettres[0],
          par1: 25,
          par2: 3,
          fixe: true,
          visible: true,
          etiquette: true,
          style: { couleur: '#000000', epaisseur: 0, taillepoint: 4, decal: [+15, +30] }
        })
        fifig.push({
          type: 'point',
          nom: stor.leslettres[2],
          par1: 24,
          par2: 13,
          fixe: true,
          visible: true,
          etiquette: true,
          style: { couleur: '#000000', epaisseur: 0, taillepoint: 4, decal: [+15, +15] }
        })
        fifig.push({
          type: 'segment',
          par1: stor.leslettres[0],
          par2: stor.leslettres[1],
          style: { couleur: '#000000', epaisseur: 3 }
        })
        fifig.push({
          type: 'segment',
          par1: stor.leslettres[1],
          par2: stor.leslettres[2],
          style: { couleur: '#000000', epaisseur: 3 }
        })
        fifig.push({
          type: 'segment',
          par1: stor.leslettres[2],
          par2: stor.leslettres[0],
          style: { couleur: '#000000', epaisseur: 3 }
        })
        if (stor.autoriseniv[1] || stor.autoriseniv[2] || stor.autoriseniv[3]) {
          fifig.push({
            type: 'point',
            nom: stor.leslettres[3],
            par1: 20.2,
            par2: 10.6,
            fixe: true,
            visible: true,
            etiquette: true,
            style: { couleur: '#000000', epaisseur: 0, taillepoint: 4, decal: [-15, +10] }
          })
          fifig.push({
            type: 'segment',
            nom: '[a5a3]',
            par1: stor.leslettres[3],
            par2: stor.leslettres[0],
            style: { couleur: '#000000', epaisseur: 1 }
          })
          fifig.push({
            type: 'point',
            nom: 'a1',
            par1: 19.7,
            par2: 8.9,
            fixe: true,
            visible: false,
            etiquette: false,
            style: { couleur: '#ff0000', epaisseur: 1, taillepoint: 4, decal: [-15, +10] }
          })
          fifig.push({
            type: 'point',
            nom: 'a2',
            par1: 19.2,
            par2: 9.8,
            fixe: true,
            visible: false,
            etiquette: false,
            style: { couleur: '#00ff00', epaisseur: 1, taillepoint: 4, decal: [-15, +10] }
          })
          fifig.push({
            type: 'point',
            nom: 'a3',
            par1: 20.9,
            par2: 9.7,
            fixe: true,
            visible: false,
            etiquette: false,
            style: { couleur: '#0000ff', epaisseur: 1, taillepoint: 4, decal: [-15, +10] }
          })
          fifig.push({ type: 'segment', nom: 't1', par1: 'a1', par2: 'a2', style: { couleur: '#000000', epaisseur: 1 } })
          fifig.push({ type: 'segment', nom: 't2', par1: 'a1', par2: 'a3', style: { couleur: '#000000', epaisseur: 1 } })
        }
        break
      case 'parallélogramme' :
        fifig.push({
          type: 'point',
          nom: stor.leslettres[0],
          par1: 4,
          par2: 2,
          fixe: true,
          visible: true,
          etiquette: true,
          style: { couleur: '#000000', epaisseur: 0, taillepoint: 4, decal: [-15, +20] }
        })
        fifig.push({
          type: 'point',
          nom: stor.leslettres[1],
          par1: 21,
          par2: 6,
          fixe: true,
          visible: true,
          etiquette: true,
          style: { couleur: '#000000', epaisseur: 0, taillepoint: 4, decal: [+15, +30] }
        })
        fifig.push({
          type: 'point',
          nom: stor.leslettres[2],
          par1: 27,
          par2: 12,
          fixe: true,
          visible: true,
          etiquette: true,
          style: { couleur: '#000000', epaisseur: 0, taillepoint: 4, decal: [+15, +15] }
        })
        fifig.push({
          type: 'point',
          nom: stor.leslettres[3],
          par1: 10,
          par2: 8,
          fixe: true,
          visible: true,
          etiquette: true,
          style: { couleur: '#000000', epaisseur: 0, taillepoint: 4, decal: [-15, +10] }
        })
        fifig.push({
          type: 'segment',
          nom: '[a0a1]',
          par1: stor.leslettres[0],
          par2: stor.leslettres[1],
          style: { couleur: '#000000', epaisseur: 3 }
        })
        fifig.push({
          type: 'segment',
          par1: stor.leslettres[1],
          par2: stor.leslettres[2],
          style: { couleur: '#000000', epaisseur: 3 }
        })
        fifig.push({
          type: 'segment',
          par1: stor.leslettres[2],
          par2: stor.leslettres[3],
          style: { couleur: '#000000', epaisseur: 3 }
        })
        fifig.push({
          type: 'segment',
          par1: stor.leslettres[3],
          par2: stor.leslettres[0],
          style: { couleur: '#000000', epaisseur: 3 }
        })
        fifig.push({
          type: 'segment',
          par1: stor.leslettres[2],
          par2: stor.leslettres[0],
          style: { couleur: '#000000', epaisseur: 1 }
        })
        fifig.push({
          type: 'segment',
          par1: stor.leslettres[3],
          par2: stor.leslettres[1],
          style: { couleur: '#000000', epaisseur: 1 }
        })

        fifig.push({
          type: 'point',
          nom: stor.leslettres[4],
          par1: 11.2,
          par2: 3.6,
          fixe: true,
          visible: true,
          etiquette: true,
          style: { couleur: '#000000', epaisseur: 0, taillepoint: 4, decal: [-10, +40] }
        })
        fifig.push({
          type: 'segment',
          par1: stor.leslettres[4],
          par2: stor.leslettres[3],
          style: { couleur: '#000000', epaisseur: 1 }
        })
        fifig.push({
          type: 'point',
          nom: 'a1',
          par1: 10,
          par2: 4.4,
          fixe: true,
          visible: false,
          etiquette: false,
          style: { couleur: '#ff0000', epaisseur: 1, taillepoint: 4, decal: [-15, +10] }
        })
        fifig.push({
          type: 'point',
          nom: 'a2',
          par1: 11,
          par2: 4.7,
          fixe: true,
          visible: false,
          etiquette: false,
          style: { couleur: '#00ff00', epaisseur: 1, taillepoint: 4, decal: [-15, +10] }
        })
        fifig.push({
          type: 'point',
          nom: 'a3',
          par1: 10.2,
          par2: 3.4,
          fixe: true,
          visible: false,
          etiquette: false,
          style: { couleur: '#0000ff', epaisseur: 1, taillepoint: 4, decal: [-15, +10] }
        })
        fifig.push({ type: 'segment', par1: 'a1', par2: 'a2', style: { couleur: '#000000', epaisseur: 1 } })
        fifig.push({ type: 'segment', par1: 'a1', par2: 'a3', style: { couleur: '#000000', epaisseur: 1 } })

        break
      case 'triangle quelconque' :
        fifig.push({
          type: 'point',
          nom: stor.leslettres[0],
          par1: 7,
          par2: 4,
          fixe: true,
          visible: true,
          etiquette: true,
          style: { couleur: '#000000', epaisseur: 0, taillepoint: 4, decal: [-15, +20] }
        })
        fifig.push({
          type: 'point',
          nom: stor.leslettres[1],
          par1: 24,
          par2: 8,
          fixe: true,
          visible: true,
          etiquette: true,
          style: { couleur: '#000000', epaisseur: 0, taillepoint: 4, decal: [+15, +30] }
        })
        fifig.push({
          type: 'point',
          nom: stor.leslettres[2],
          par1: 13,
          par2: 10,
          fixe: true,
          visible: true,
          etiquette: true,
          style: { couleur: '#000000', epaisseur: 0, taillepoint: 4, decal: [-15, +10] }
        })
        fifig.push({
          type: 'segment',
          par1: stor.leslettres[0],
          par2: stor.leslettres[1],
          style: { couleur: '#000000', epaisseur: 3 }
        })
        fifig.push({
          type: 'segment',
          par1: stor.leslettres[1],
          par2: stor.leslettres[2],
          style: { couleur: '#000000', epaisseur: 3 }
        })
        fifig.push({
          type: 'segment',
          par1: stor.leslettres[2],
          par2: stor.leslettres[0],
          style: { couleur: '#000000', epaisseur: 3 }
        })

        fifig.push({
          type: 'point',
          nom: stor.leslettres[3],
          par1: 14.2,
          par2: 5.6,
          fixe: true,
          visible: true,
          etiquette: true,
          style: { couleur: '#000000', epaisseur: 0, taillepoint: 4, decal: [-10, +40] }
        })
        fifig.push({
          type: 'segment',
          par1: stor.leslettres[3],
          par2: stor.leslettres[2],
          style: { couleur: '#000000', epaisseur: 1 }
        })
        fifig.push({
          type: 'point',
          nom: 'a1',
          par1: 13,
          par2: 6.4,
          fixe: true,
          visible: false,
          etiquette: false,
          style: { couleur: '#ff0000', epaisseur: 1, taillepoint: 4, decal: [-15, +10] }
        })
        fifig.push({
          type: 'point',
          nom: 'a2',
          par1: 14,
          par2: 6.7,
          fixe: true,
          visible: false,
          etiquette: false,
          style: { couleur: '#00ff00', epaisseur: 1, taillepoint: 4, decal: [-15, +10] }
        })
        fifig.push({
          type: 'point',
          nom: 'a3',
          par1: 13.2,
          par2: 5.4,
          fixe: true,
          visible: false,
          etiquette: false,
          style: { couleur: '#0000ff', epaisseur: 1, taillepoint: 4, decal: [-15, +10] }
        })
        fifig.push({ type: 'segment', par1: 'a1', par2: 'a2', style: { couleur: '#000000', epaisseur: 1 } })
        fifig.push({ type: 'segment', par1: 'a1', par2: 'a3', style: { couleur: '#000000', epaisseur: 1 } })

        break
      case 'triangle équilatéral' :
        fifig.push({
          type: 'point',
          nom: stor.leslettres[0],
          par1: 7,
          par2: 1,
          fixe: true,
          visible: true,
          etiquette: true,
          style: { couleur: '#000000', epaisseur: 0, taillepoint: 4, decal: [-15, +20] }
        })
        fifig.push({
          type: 'point',
          nom: stor.leslettres[1],
          par1: 20,
          par2: 3,
          fixe: true,
          visible: true,
          etiquette: true,
          style: { couleur: '#000000', epaisseur: 0, taillepoint: 4, decal: [+15, +20] }
        })
        fifig.push({
          type: 'point',
          nom: stor.leslettres[2],
          par1: 11.75,
          par2: 13.3,
          fixe: true,
          visible: true,
          etiquette: true,
          style: { couleur: '#000000', epaisseur: 0, taillepoint: 4, decal: [-15, +20] }
        })

        fifig.push({
          type: 'point',
          nom: 'h',
          par1: 13.5,
          par2: 10,
          fixe: true,
          visible: false,
          etiquette: false,
          style: { couleur: '#000000', epaisseur: 0, taillepoint: 4, decal: [+15, +20] }
        })
        fifig.push({
          type: 'point',
          nom: stor.leslettres[4],
          par1: 14.5,
          par2: 11,
          fixe: true,
          visible: true,
          etiquette: true,
          style: { couleur: '#000000', epaisseur: 0, taillepoint: 4, decal: [+15, +20] }
        })
        fifig.push({
          type: 'segment',
          par1: 'h',
          par2: stor.leslettres[4],
          style: { couleur: '#000000', epaisseur: 1 }
        })

        fifig.push({
          type: 'segment',
          par1: stor.leslettres[0],
          par2: stor.leslettres[1],
          style: { couleur: '#000000', epaisseur: 3 }
        })
        fifig.push({
          type: 'segment',
          par1: stor.leslettres[1],
          par2: stor.leslettres[2],
          style: { couleur: '#000000', epaisseur: 3 }
        })
        fifig.push({
          type: 'segment',
          par1: stor.leslettres[2],
          par2: stor.leslettres[0],
          style: { couleur: '#000000', epaisseur: 3 }
        })
        if (stor.autoriseniv[1] || stor.autoriseniv[2] || stor.autoriseniv[3]) {
          fifig.push({
            type: 'point',
            nom: stor.leslettres[3],
            par1: 13.5,
            par2: 2,
            fixe: true,
            visible: true,
            etiquette: true,
            style: { couleur: '#000000', epaisseur: 0, taillepoint: 4, decal: [+15, 0] }
          })
          fifig.push({
            type: 'segment',
            par1: stor.leslettres[3],
            par2: stor.leslettres[2],
            style: { couleur: '#000000', epaisseur: 1 }
          })
          fifig.push({
            type: 'point',
            nom: 'a1',
            par1: 12.2,
            par2: 3,
            fixe: true,
            visible: false,
            etiquette: false,
            style: { couleur: '#ff0000', epaisseur: 1, taillepoint: 4, decal: [-15, +10] }
          })
          fifig.push({
            type: 'point',
            nom: 'a2',
            par1: 13.3,
            par2: 3.2,
            fixe: true,
            visible: false,
            etiquette: false,
            style: { couleur: '#00ff00', epaisseur: 1, taillepoint: 4, decal: [-15, +10] }
          })
          fifig.push({
            type: 'point',
            nom: 'a3',
            par1: 12.4,
            par2: 1.9,
            fixe: true,
            visible: false,
            etiquette: false,
            style: { couleur: '#0000ff', epaisseur: 1, taillepoint: 4, decal: [-15, +10] }
          })
          fifig.push({ type: 'segment', par1: 'a1', par2: 'a2', style: { couleur: '#000000', epaisseur: 1 } })
          fifig.push({ type: 'segment', par1: 'a1', par2: 'a3', style: { couleur: '#000000', epaisseur: 1 } })
        }
        break
      // pointilles:"5,5"
      case 'cube':
        fifig.push({
          type: 'point',
          nom: stor.leslettres[0],
          par1: 6,
          par2: 1,
          fixe: true,
          visible: true,
          etiquette: true,
          style: { couleur: '#000000', epaisseur: 0, taillepoint: 4, decal: [-20, +20] }
        })
        fifig.push({
          type: 'point',
          nom: stor.leslettres[1],
          par1: 13,
          par2: 2,
          fixe: true,
          visible: true,
          etiquette: true,
          style: { couleur: '#000000', epaisseur: 0, taillepoint: 4, decal: [+20, +25] }
        })
        fifig.push({
          type: 'point',
          nom: stor.leslettres[2],
          par1: 12,
          par2: 9,
          fixe: true,
          visible: true,
          etiquette: true,
          style: { couleur: '#000000', epaisseur: 0, taillepoint: 4, decal: [+15, +25] }
        })
        fifig.push({
          type: 'point',
          nom: stor.leslettres[3],
          par1: 5,
          par2: 8,
          fixe: true,
          visible: true,
          etiquette: true,
          style: { couleur: '#000000', epaisseur: 0, taillepoint: 4, decal: [-20, +20] }
        })
        fifig.push({
          type: 'segment',
          par1: stor.leslettres[0],
          par2: stor.leslettres[1],
          style: { couleur: '#000000', epaisseur: 3 }
        })
        fifig.push({
          type: 'segment',
          par1: stor.leslettres[1],
          par2: stor.leslettres[2],
          style: { couleur: '#000000', epaisseur: 3 }
        })
        fifig.push({
          type: 'segment',
          par1: stor.leslettres[2],
          par2: stor.leslettres[3],
          style: { couleur: '#000000', epaisseur: 3 }
        })
        fifig.push({
          type: 'segment',
          par1: stor.leslettres[3],
          par2: stor.leslettres[0],
          style: { couleur: '#000000', epaisseur: 3 }
        })

        fifig.push({
          type: 'point',
          nom: stor.leslettres[4],
          par1: 9,
          par2: 5,
          fixe: true,
          visible: true,
          etiquette: true,
          style: { couleur: '#000000', epaisseur: 0, taillepoint: 4, decal: [-15, +20] }
        })
        fifig.push({
          type: 'point',
          nom: stor.leslettres[5],
          par1: 16,
          par2: 6,
          fixe: true,
          visible: true,
          etiquette: true,
          style: { couleur: '#000000', epaisseur: 0, taillepoint: 4, decal: [+15, +10] }
        })
        fifig.push({
          type: 'point',
          nom: stor.leslettres[6],
          par1: 15,
          par2: 13,
          fixe: true,
          visible: true,
          etiquette: true,
          style: { couleur: '#000000', epaisseur: 0, taillepoint: 4, decal: [+15, +15] }
        })
        fifig.push({
          type: 'point',
          nom: stor.leslettres[7],
          par1: 8,
          par2: 12,
          fixe: true,
          visible: true,
          etiquette: true,
          style: { couleur: '#000000', epaisseur: 0, taillepoint: 4, decal: [-15, +10] }
        })
        fifig.push({
          type: 'segment',
          par1: stor.leslettres[4],
          par2: stor.leslettres[5],
          style: { couleur: '#000000', epaisseur: 3, pointilles: '5,5' }
        })
        fifig.push({
          type: 'segment',
          par1: stor.leslettres[5],
          par2: stor.leslettres[6],
          style: { couleur: '#000000', epaisseur: 3 }
        })
        fifig.push({
          type: 'segment',
          par1: stor.leslettres[6],
          par2: stor.leslettres[7],
          style: { couleur: '#000000', epaisseur: 3 }
        })
        fifig.push({
          type: 'segment',
          par1: stor.leslettres[7],
          par2: stor.leslettres[4],
          style: { couleur: '#000000', epaisseur: 3, pointilles: '5,5' }
        })

        fifig.push({
          type: 'segment',
          par1: stor.leslettres[0],
          par2: stor.leslettres[4],
          style: { couleur: '#000000', epaisseur: 3, pointilles: '5,5' }
        })
        fifig.push({
          type: 'segment',
          par1: stor.leslettres[1],
          par2: stor.leslettres[5],
          style: { couleur: '#000000', epaisseur: 3 }
        })
        fifig.push({
          type: 'segment',
          par1: stor.leslettres[2],
          par2: stor.leslettres[6],
          style: { couleur: '#000000', epaisseur: 3 }
        })
        fifig.push({
          type: 'segment',
          par1: stor.leslettres[3],
          par2: stor.leslettres[7],
          style: { couleur: '#000000', epaisseur: 3 }
        })
        break
      case 'parallélépipède rectangle':
        fifig.push({
          type: 'point',
          nom: stor.leslettres[0],
          par1: 6,
          par2: 1,
          fixe: true,
          visible: true,
          etiquette: true,
          style: { couleur: '#000000', epaisseur: 0, taillepoint: 4, decal: [-20, +20] }
        })
        fifig.push({
          type: 'point',
          nom: stor.leslettres[1],
          par1: 19,
          par2: 2,
          fixe: true,
          visible: true,
          etiquette: true,
          style: { couleur: '#000000', epaisseur: 0, taillepoint: 4, decal: [+20, +25] }
        })
        fifig.push({
          type: 'point',
          nom: stor.leslettres[2],
          par1: 18,
          par2: 9,
          fixe: true,
          visible: true,
          etiquette: true,
          style: { couleur: '#000000', epaisseur: 0, taillepoint: 4, decal: [+15, +25] }
        })
        fifig.push({
          type: 'point',
          nom: stor.leslettres[3],
          par1: 5,
          par2: 8,
          fixe: true,
          visible: true,
          etiquette: true,
          style: { couleur: '#000000', epaisseur: 0, taillepoint: 4, decal: [-20, +20] }
        })
        fifig.push({
          type: 'segment',
          par1: stor.leslettres[0],
          par2: stor.leslettres[1],
          style: { couleur: '#000000', epaisseur: 3 }
        })
        fifig.push({
          type: 'segment',
          par1: stor.leslettres[1],
          par2: stor.leslettres[2],
          style: { couleur: '#000000', epaisseur: 3 }
        })
        fifig.push({
          type: 'segment',
          par1: stor.leslettres[2],
          par2: stor.leslettres[3],
          style: { couleur: '#000000', epaisseur: 3 }
        })
        fifig.push({
          type: 'segment',
          par1: stor.leslettres[3],
          par2: stor.leslettres[0],
          style: { couleur: '#000000', epaisseur: 3 }
        })

        fifig.push({
          type: 'point',
          nom: stor.leslettres[4],
          par1: 9,
          par2: 5,
          fixe: true,
          visible: true,
          etiquette: true,
          style: { couleur: '#000000', epaisseur: 0, taillepoint: 4, decal: [-15, +20] }
        })
        fifig.push({
          type: 'point',
          nom: stor.leslettres[5],
          par1: 22,
          par2: 6,
          fixe: true,
          visible: true,
          etiquette: true,
          style: { couleur: '#000000', epaisseur: 0, taillepoint: 4, decal: [+15, +10] }
        })
        fifig.push({
          type: 'point',
          nom: stor.leslettres[6],
          par1: 21,
          par2: 13,
          fixe: true,
          visible: true,
          etiquette: true,
          style: { couleur: '#000000', epaisseur: 0, taillepoint: 4, decal: [+15, +15] }
        })
        fifig.push({
          type: 'point',
          nom: stor.leslettres[7],
          par1: 8,
          par2: 12,
          fixe: true,
          visible: true,
          etiquette: true,
          style: { couleur: '#000000', epaisseur: 0, taillepoint: 4, decal: [-15, +10] }
        })
        fifig.push({
          type: 'segment',
          par1: stor.leslettres[4],
          par2: stor.leslettres[5],
          style: { couleur: '#000000', epaisseur: 3, pointilles: '5,5' }
        })
        fifig.push({
          type: 'segment',
          par1: stor.leslettres[5],
          par2: stor.leslettres[6],
          style: { couleur: '#000000', epaisseur: 3 }
        })
        fifig.push({
          type: 'segment',
          par1: stor.leslettres[6],
          par2: stor.leslettres[7],
          style: { couleur: '#000000', epaisseur: 3 }
        })
        fifig.push({
          type: 'segment',
          par1: stor.leslettres[7],
          par2: stor.leslettres[4],
          style: { couleur: '#000000', epaisseur: 3, pointilles: '5,5' }
        })

        fifig.push({
          type: 'segment',
          par1: stor.leslettres[0],
          par2: stor.leslettres[4],
          style: { couleur: '#000000', epaisseur: 3, pointilles: '5,5' }
        })
        fifig.push({
          type: 'segment',
          par1: stor.leslettres[1],
          par2: stor.leslettres[5],
          style: { couleur: '#000000', epaisseur: 3 }
        })
        fifig.push({
          type: 'segment',
          par1: stor.leslettres[2],
          par2: stor.leslettres[6],
          style: { couleur: '#000000', epaisseur: 3 }
        })
        fifig.push({
          type: 'segment',
          par1: stor.leslettres[3],
          par2: stor.leslettres[7],
          style: { couleur: '#000000', epaisseur: 3 }
        })
        break
      case 'cylindre' :
        fifig.push({
          type: 'point',
          nom: stor.leslettres[2],
          par1: 5,
          par2: 4,
          fixe: true,
          visible: true,
          etiquette: true,
          style: { couleur: '#000000', epaisseur: 0, taillepoint: 4, decal: [-12, +22] }
        })
        fifig.push({
          type: 'point',
          nom: stor.leslettres[3],
          par1: 11,
          par2: 4,
          fixe: true,
          visible: true,
          etiquette: true,
          style: { couleur: '#000000', epaisseur: 0, taillepoint: 4, decal: [+15, +22] }
        })
        fifig.push({
          type: 'segment',
          par1: stor.leslettres[2],
          par2: stor.leslettres[3],
          style: { couleur: '#000000', epaisseur: 0.5 }
        })

        fifig.push({
          type: 'point',
          nom: stor.leslettres[4],
          par1: 12.9,
          par2: 10.5,
          fixe: true,
          visible: true,
          etiquette: true,
          style: { couleur: '#000000', epaisseur: 0, taillepoint: 4, decal: [+15, +22] }
        })
        fifig.push({
          type: 'point',
          nom: '(' + stor.leslettres[0] + ')',
          par1: 7.05,
          par2: 11.6,
          fixe: true,
          visible: true,
          etiquette: true,
          style: { couleur: '#000000', epaisseur: 0, taillepoint: 4, decal: [-35, +45] }
        })
        fifig.push({
          type: 'point',
          nom: 'az',
          par1: 11,
          par2: 3.5,
          fixe: true,
          visible: false,
          etiquette: false,
          style: { couleur: '#000000', epaisseur: 0, taillepoint: 4, decal: [-20, +20] }
        })
        fifig.push({
          type: 'point',
          nom: 'as',
          par1: 5.05,
          par2: 4.5,
          fixe: true,
          visible: true,
          etiquette: false,
          style: { couleur: '#000000', epaisseur: 0, taillepoint: 4, decal: [-25, +20] }
        })

        fifig.push({
          type: 'point',
          nom: 'c',
          par1: 8,
          par2: 4,
          fixe: true,
          visible: false,
          etiquette: false,
          style: { couleur: '#000000', epaisseur: 0, taillepoint: 4, decal: [-20, +20] }
        })
        fifig.push({
          type: 'point',
          nom: 'ay',
          par1: 5,
          par2: 4,
          fixe: true,
          visible: true,
          etiquette: false,
          style: { couleur: '#000000', epaisseur: 0, taillepoint: 4, decal: [-25, +20] }
        })
        fifig.push({
          type: 'point',
          nom: stor.leslettres[1],
          par1: 8.5,
          par2: 3.5,
          fixe: true,
          visible: true,
          etiquette: true,
          style: { couleur: '#000000', epaisseur: 0, taillepoint: 4, decal: [-2, +31] }
        })
        fifig.push({
          type: 'point',
          nom: 'cdh',
          par1: 8.5,
          par2: 4.5,
          fixe: true,
          visible: true,
          etiquette: false,
          style: { couleur: '#000000', epaisseur: 0, taillepoint: 4, decal: [+15, +10] }
        })
        fifig.push({
          type: 'point',
          nom: 'cgb',
          par1: 7.5,
          par2: 3.5,
          fixe: true,
          visible: true,
          etiquette: false,
          style: { couleur: '#000000', epaisseur: 0, taillepoint: 4, decal: [+15, +15] }
        })
        fifig.push({
          type: 'point',
          nom: 'cgh',
          par1: 7.5,
          par2: 4.5,
          fixe: true,
          visible: true,
          etiquette: false,
          style: { couleur: '#000000', epaisseur: 0, taillepoint: 4, decal: [-20, +20] }
        })
        fifig.push({
          type: 'segment',
          par1: stor.leslettres[1],
          par2: 'cgh',
          style: { couleur: '#000000', epaisseur: 1 }
        })
        fifig.push({ type: 'segment', nom: 'c2', par1: 'cgb', par2: 'cdh', style: { couleur: '#000000', epaisseur: 1 } })
        fifig.push({ type: 'cercle', nom: 'ce', par1: 'c', par2: 'ay', style: { couleur: '#000000', epaisseur: 3 } })

        fifig.push({
          type: 'courbe',
          par1: '11-(racine(9-(x-10)^2))',
          par2: 7.1,
          par3: 7.45,
          par4: 10,
          style: { couleur: '#000000', epaisseur: 3 }
        })
        fifig.push({
          type: 'courbe',
          par1: '11-(racine(9-(x-10)^2))',
          par2: 8,
          par3: 8.45,
          par4: 10,
          style: { couleur: '#000000', epaisseur: 3 }
        })
        fifig.push({
          type: 'courbe',
          par1: '11-(racine(9-(x-10)^2))',
          par2: 9,
          par3: 9.45,
          par4: 10,
          style: { couleur: '#000000', epaisseur: 3 }
        })
        fifig.push({
          type: 'courbe',
          par1: '11-(racine(9-(x-10)^2))',
          par2: 10,
          par3: 10.45,
          par4: 10,
          style: { couleur: '#000000', epaisseur: 3 }
        })
        fifig.push({
          type: 'courbe',
          par1: '11-(racine(9-(x-10)^2))',
          par2: 11,
          par3: 11.45,
          par4: 10,
          style: { couleur: '#000000', epaisseur: 3 }
        })
        fifig.push({
          type: 'courbe',
          par1: '11-(racine(9-(x-10)^2))',
          par2: 12.2,
          par3: 13,
          par4: 10,
          style: { couleur: '#000000', epaisseur: 3 }
        })

        fifig.push({
          type: 'courbe',
          par1: '11+racine(9-(x-10)^2)',
          par2: 7.0001,
          par3: 12.9999,
          par4: 50,
          style: { couleur: '#000000', epaisseur: 3 }
        })
        fifig.push({
          type: 'segment',
          par1: stor.leslettres[4],
          par2: 'az',
          style: { couleur: '#000000', epaisseur: 3 }
        })
        fifig.push({
          type: 'segment',
          par1: '(' + stor.leslettres[0] + ')',
          par2: 'as',
          style: { couleur: '#000000', epaisseur: 3 }
        })

        break
      case 'cône de révolution' :
        fifig.push({
          type: 'point',
          nom: stor.leslettres[2],
          par1: 5,
          par2: 4,
          fixe: true,
          visible: true,
          etiquette: true,
          style: { couleur: '#000000', epaisseur: 0, taillepoint: 4, decal: [-12, +22] }
        })
        fifig.push({
          type: 'point',
          nom: stor.leslettres[3],
          par1: 11,
          par2: 4,
          fixe: true,
          visible: true,
          etiquette: true,
          style: { couleur: '#000000', epaisseur: 0, taillepoint: 4, decal: [+15, +22] }
        })
        fifig.push({
          type: 'segment',
          par1: stor.leslettres[2],
          par2: stor.leslettres[3],
          style: { couleur: '#000000', epaisseur: 0.5 }
        })

        fifig.push({
          type: 'point',
          nom: stor.leslettres[4],
          par1: 8,
          par2: 14,
          fixe: true,
          visible: true,
          etiquette: true,
          style: { couleur: '#000000', epaisseur: 0, taillepoint: 4, decal: [+15, +22] }
        })
        fifig.push({
          type: 'point',
          nom: 'az',
          par1: 10.8,
          par2: 5,
          fixe: true,
          visible: false,
          etiquette: false,
          style: { couleur: '#000000', epaisseur: 0, taillepoint: 4, decal: [-20, +20] }
        })
        fifig.push({
          type: 'point',
          nom: 'as',
          par1: 5.2,
          par2: 5,
          fixe: true,
          visible: true,
          etiquette: false,
          style: { couleur: '#000000', epaisseur: 0, taillepoint: 4, decal: [-25, +20] }
        })

        fifig.push({
          type: 'point',
          nom: 'c',
          par1: 8,
          par2: 4,
          fixe: true,
          visible: false,
          etiquette: false,
          style: { couleur: '#000000', epaisseur: 0, taillepoint: 4, decal: [-20, +20] }
        })
        fifig.push({
          type: 'point',
          nom: 'ay',
          par1: 5,
          par2: 4,
          fixe: true,
          visible: true,
          etiquette: false,
          style: { couleur: '#000000', epaisseur: 0, taillepoint: 4, decal: [-25, +20] }
        })
        fifig.push({
          type: 'point',
          nom: stor.leslettres[1],
          par1: 8.5,
          par2: 3.5,
          fixe: true,
          visible: true,
          etiquette: true,
          style: { couleur: '#000000', epaisseur: 0, taillepoint: 4, decal: [-2, +31] }
        })
        fifig.push({
          type: 'point',
          nom: 'cdh',
          par1: 8.5,
          par2: 4.5,
          fixe: true,
          visible: true,
          etiquette: false,
          style: { couleur: '#000000', epaisseur: 0, taillepoint: 4, decal: [+15, +10] }
        })
        fifig.push({
          type: 'point',
          nom: 'cgb',
          par1: 7.5,
          par2: 3.5,
          fixe: true,
          visible: true,
          etiquette: false,
          style: { couleur: '#000000', epaisseur: 0, taillepoint: 4, decal: [+15, +15] }
        })
        fifig.push({
          type: 'point',
          nom: 'cgh',
          par1: 7.5,
          par2: 4.5,
          fixe: true,
          visible: true,
          etiquette: false,
          style: { couleur: '#000000', epaisseur: 0, taillepoint: 4, decal: [-20, +20] }
        })
        fifig.push({
          type: 'segment',
          par1: stor.leslettres[1],
          par2: 'cgh',
          style: { couleur: '#000000', epaisseur: 1 }
        })
        fifig.push({ type: 'segment', par1: 'cgb', par2: 'cdh', style: { couleur: '#000000', epaisseur: 1 } })
        // fifig.push({type:"cercle",nom:'ce',par1:'c',par2:'ay',style:{couleur:"#000000",epaisseur:3}});

        fifig.push({
          type: 'courbe',
          par1: '4-(racine(9-(x-8)^2))',
          par2: 5.0001,
          par3: 10.9999,
          par4: 50,
          style: { couleur: '#000000', epaisseur: 3 }
        })
        fifig.push({
          type: 'courbe',
          par1: '4+(racine(9-(x-8)^2))',
          par2: 5.0001,
          par3: 5.45,
          par4: 10,
          style: { couleur: '#000000', epaisseur: 3 }
        })
        fifig.push({
          type: 'courbe',
          par1: '4+(racine(9-(x-8)^2))',
          par2: 6,
          par3: 6.45,
          par4: 10,
          style: { couleur: '#000000', epaisseur: 3 }
        })
        fifig.push({
          type: 'courbe',
          par1: '4+(racine(9-(x-8)^2))',
          par2: 7,
          par3: 7.45,
          par4: 10,
          style: { couleur: '#000000', epaisseur: 3 }
        })
        fifig.push({
          type: 'courbe',
          par1: '4+(racine(9-(x-8)^2))',
          par2: 8,
          par3: 8.45,
          par4: 10,
          style: { couleur: '#000000', epaisseur: 3 }
        })
        fifig.push({
          type: 'courbe',
          par1: '4+(racine(9-(x-8)^2))',
          par2: 9,
          par3: 9.45,
          par4: 10,
          style: { couleur: '#000000', epaisseur: 3 }
        })
        fifig.push({
          type: 'courbe',
          par1: '4+(racine(9-(x-8)^2))',
          par2: 10.2,
          par3: 10.9999,
          par4: 10,
          style: { couleur: '#000000', epaisseur: 3 }
        })

        fifig.push({
          type: 'segment',
          par1: stor.leslettres[4],
          par2: 'az',
          style: { couleur: '#000000', epaisseur: 3 }
        })
        fifig.push({
          type: 'segment',
          par1: stor.leslettres[4],
          par2: 'as',
          style: { couleur: '#000000', epaisseur: 3 }
        })
        fifig.push({
          type: 'segment',
          par1: stor.leslettres[4],
          par2: 'c',
          style: { couleur: '#000000', epaisseur: 1, pointilles: '5,5' }
        })

        break
      case 'sphère' :
        fifig.push({
          type: 'point',
          nom: stor.leslettres[2],
          par1: 20.3,
          par2: 12.3,
          fixe: true,
          visible: true,
          etiquette: true,
          style: { couleur: '#000000', epaisseur: 0, taillepoint: 4, decal: [+20, +20] }
        })
        fifig.push({
          type: 'point',
          nom: stor.leslettres[3],
          par1: 11.7,
          par2: 3.7,
          fixe: true,
          visible: true,
          etiquette: true,
          style: { couleur: '#000000', epaisseur: 0, taillepoint: 4, decal: [-20, +20] }
        })
        fifig.push({
          type: 'point',
          nom: stor.leslettres[4],
          par1: 11.7,
          par2: 12.3,
          fixe: true,
          visible: true,
          etiquette: true,
          style: { couleur: '#000000', epaisseur: 0, taillepoint: 4, decal: [-20, +20] }
        })
        fifig.push({
          type: 'segment',
          par1: stor.leslettres[2],
          par2: stor.leslettres[3],
          style: { couleur: '#000000', epaisseur: 2, pointilles: '5,5' }
        })
        fifig.push({
          type: 'segment',
          par1: stor.leslettres[2],
          par2: stor.leslettres[4],
          style: { couleur: '#000000', epaisseur: 2, pointilles: '5,5' }
        })

        fifig.push({
          type: 'point',
          nom: 'c',
          par1: 16,
          par2: 8,
          fixe: true,
          visible: false,
          etiquette: false,
          style: { couleur: '#000000', epaisseur: 0, taillepoint: 4, decal: [-20, +20] }
        })
        fifig.push({
          type: 'point',
          nom: '(' + stor.leslettres[0] + ')',
          par1: 10,
          par2: 8,
          fixe: true,
          visible: true,
          etiquette: true,
          style: { couleur: '#000000', epaisseur: 0, taillepoint: 4, decal: [-30, +20] }
        })
        fifig.push({
          type: 'point',
          nom: stor.leslettres[1],
          par1: 15,
          par2: 9,
          fixe: true,
          visible: true,
          etiquette: true,
          style: { couleur: '#000000', epaisseur: 0, taillepoint: 4, decal: [-10, +20] }
        })
        fifig.push({
          type: 'point',
          nom: 'cdh',
          par1: 15,
          par2: 7,
          fixe: true,
          visible: true,
          etiquette: false,
          style: { couleur: '#000000', epaisseur: 0, taillepoint: 4, decal: [+15, +10] }
        })
        fifig.push({
          type: 'point',
          nom: 'cgb',
          par1: 17,
          par2: 9,
          fixe: true,
          visible: true,
          etiquette: false,
          style: { couleur: '#000000', epaisseur: 0, taillepoint: 4, decal: [+15, +15] }
        })
        fifig.push({
          type: 'point',
          nom: 'cgh',
          par1: 17,
          par2: 7,
          fixe: true,
          visible: true,
          etiquette: false,
          style: { couleur: '#000000', epaisseur: 0, taillepoint: 4, decal: [-20, +20] }
        })
        fifig.push({
          type: 'segment',
          par1: stor.leslettres[1],
          par2: 'cgh',
          style: { couleur: '#000000', epaisseur: 1 }
        })
        fifig.push({ type: 'segment', par1: 'cgb', par2: 'cdh', style: { couleur: '#000000', epaisseur: 1 } })
        fifig.push({
          type: 'cercle',
          par1: 'c',
          par2: '(' + stor.leslettres[0] + ')',
          style: { couleur: '#000000', epaisseur: 3 }
        })

        fifig.push({
          type: 'courbe',
          par1: '8+(racine(36-(x-16)^2))/2',
          par2: 10,
          par3: 22,
          par4: 50,
          style: { couleur: '#000000', epaisseur: 3 }
        })
        fifig.push({
          type: 'courbe',
          par1: '8-(racine(36-(x-16)^2))/2',
          par2: 10,
          par3: 10.3,
          par4: 10,
          style: { couleur: '#000000', epaisseur: 3 }
        })
        fifig.push({
          type: 'courbe',
          par1: '8-(racine(36-(x-16)^2))/2',
          par2: 21.8,
          par3: 22,
          par4: 10,
          style: { couleur: '#000000', epaisseur: 3 }
        })
        fifig.push({
          type: 'courbe',
          par1: '8-(racine(36-(x-16)^2))/2',
          par2: 11,
          par3: 12,
          par4: 10,
          style: { couleur: '#000000', epaisseur: 3 }
        })
        fifig.push({
          type: 'courbe',
          par1: '8-(racine(36-(x-16)^2))/2',
          par2: 13,
          par3: 14,
          par4: 10,
          style: { couleur: '#000000', epaisseur: 3 }
        })
        fifig.push({
          type: 'courbe',
          par1: '8-(racine(36-(x-16)^2))/2',
          par2: 15,
          par3: 16,
          par4: 10,
          style: { couleur: '#000000', epaisseur: 3 }
        })
        fifig.push({
          type: 'courbe',
          par1: '8-(racine(36-(x-16)^2))/2',
          par2: 17,
          par3: 18,
          par4: 10,
          style: { couleur: '#000000', epaisseur: 3 }
        })
        fifig.push({
          type: 'courbe',
          par1: '8-(racine(36-(x-16)^2))/2',
          par2: 19,
          par3: 20,
          par4: 10,
          style: { couleur: '#000000', epaisseur: 3 }
        })
        fifig.push({
          type: 'courbe',
          par1: '8-(racine(36-(x-16)^2))/2',
          par2: 20.5,
          par3: 21.2,
          par4: 10,
          style: { couleur: '#000000', epaisseur: 3 }
        })
        break
      case 'pyramide':
        fifig.push({
          type: 'point',
          nom: stor.leslettres[1],
          par1: 6,
          par2: 7.5,
          fixe: true,
          visible: true,
          etiquette: true,
          style: { couleur: '#000000', epaisseur: 0, taillepoint: 4, decal: [-10, +20] }
        })
        fifig.push({
          type: 'point',
          nom: stor.leslettres[2],
          par1: 8,
          par2: 3.5,
          fixe: true,
          visible: true,
          etiquette: true,
          style: { couleur: '#000000', epaisseur: 0, taillepoint: 4, decal: [0, +35] }
        })
        fifig.push({
          type: 'point',
          nom: stor.leslettres[3],
          par1: 11,
          par2: 2.5,
          fixe: true,
          visible: true,
          etiquette: true,
          style: { couleur: '#000000', epaisseur: 0, taillepoint: 4, decal: [+0, +33] }
        })
        fifig.push({
          type: 'point',
          nom: stor.leslettres[4],
          par1: 17,
          par2: 2.5,
          fixe: true,
          visible: true,
          etiquette: true,
          style: { couleur: '#000000', epaisseur: 0, taillepoint: 4, decal: [+5, +33] }
        })
        fifig.push({
          type: 'point',
          nom: stor.leslettres[5],
          par1: 19,
          par2: 6.5,
          fixe: true,
          visible: true,
          etiquette: true,
          style: { couleur: '#000000', epaisseur: 0, taillepoint: 4, decal: [+15, +20] }
        })
        fifig.push({
          type: 'point',
          nom: stor.leslettres[6],
          par1: 14,
          par2: 13.5,
          fixe: true,
          visible: true,
          etiquette: true,
          style: { couleur: '#000000', epaisseur: 0, taillepoint: 4, decal: [+20, +20] }
        })
        fifig.push({
          type: 'segment',
          par1: stor.leslettres[1],
          par2: stor.leslettres[2],
          style: { couleur: '#000000', epaisseur: 3 }
        })
        fifig.push({
          type: 'segment',
          par1: stor.leslettres[2],
          par2: stor.leslettres[3],
          style: { couleur: '#000000', epaisseur: 3 }
        })
        fifig.push({
          type: 'segment',
          par1: stor.leslettres[3],
          par2: stor.leslettres[4],
          style: { couleur: '#000000', epaisseur: 3 }
        })
        fifig.push({
          type: 'segment',
          par1: stor.leslettres[4],
          par2: stor.leslettres[5],
          style: { couleur: '#000000', epaisseur: 3 }
        })
        fifig.push({
          type: 'segment',
          par1: stor.leslettres[5],
          par2: stor.leslettres[1],
          style: { couleur: '#000000', epaisseur: 3, pointilles: '5,5' }
        })
        fifig.push({
          type: 'segment',
          par1: stor.leslettres[1],
          par2: stor.leslettres[6],
          style: { couleur: '#000000', epaisseur: 3 }
        })
        fifig.push({
          type: 'segment',
          par1: stor.leslettres[2],
          par2: stor.leslettres[6],
          style: { couleur: '#000000', epaisseur: 3 }
        })
        fifig.push({
          type: 'segment',
          par1: stor.leslettres[3],
          par2: stor.leslettres[6],
          style: { couleur: '#000000', epaisseur: 3 }
        })
        fifig.push({
          type: 'segment',
          par1: stor.leslettres[4],
          par2: stor.leslettres[6],
          style: { couleur: '#000000', epaisseur: 3 }
        })
        fifig.push({
          type: 'segment',
          par1: stor.leslettres[5],
          par2: stor.leslettres[6],
          style: { couleur: '#000000', epaisseur: 3 }
        })
        fifig.push({
          type: 'point',
          nom: 'b1',
          par1: 14,
          par2: 5,
          fixe: true,
          visible: false,
          etiquette: false,
          style: { couleur: '#000000', epaisseur: 0, taillepoint: 4, decal: [+20, +20] }
        })
        fifig.push({
          type: 'point',
          nom: 'b2',
          par1: 15,
          par2: 6,
          fixe: true,
          visible: false,
          etiquette: false,
          style: { couleur: '#000000', epaisseur: 0, taillepoint: 4, decal: [+20, +20] }
        })
        fifig.push({
          type: 'point',
          nom: 'b3',
          par1: 15,
          par2: 5,
          fixe: true,
          visible: false,
          etiquette: false,
          style: { couleur: '#000000', epaisseur: 0, taillepoint: 4, decal: [+20, +20] }
        })
        fifig.push({
          type: 'point',
          nom: 'b4',
          par1: 14,
          par2: 6,
          fixe: true,
          visible: false,
          etiquette: false,
          style: { couleur: '#000000', epaisseur: 0, taillepoint: 4, decal: [+20, +20] }
        })
        fifig.push({
          type: 'segment',
          par1: stor.leslettres[6],
          par2: 'b1',
          style: { couleur: '#000000', epaisseur: 1 }
        })
        fifig.push({ type: 'segment', par1: 'b2', par2: 'b3', style: { couleur: '#000000', epaisseur: 1 } })
        fifig.push({ type: 'segment', par1: 'b2', par2: 'b4', style: { couleur: '#000000', epaisseur: 1 } })

        fifig.push({
          type: 'point',
          nom: 'c2',
          par1: 13.2,
          par2: 5.5,
          fixe: true,
          visible: false,
          etiquette: false,
          style: { couleur: '#000000', epaisseur: 0, taillepoint: 4, decal: [+20, +20] }
        })
        fifig.push({
          type: 'point',
          nom: 'c3',
          par1: 14,
          par2: 6,
          fixe: true,
          visible: false,
          etiquette: false,
          style: { couleur: '#000000', epaisseur: 0, taillepoint: 4, decal: [+20, +20] }
        })
        fifig.push({
          type: 'point',
          nom: 'c4',
          par1: 13.2,
          par2: 4.5,
          fixe: true,
          visible: false,
          etiquette: false,
          style: { couleur: '#000000', epaisseur: 0, taillepoint: 4, decal: [+20, +20] }
        })
        fifig.push({ type: 'segment', par1: 'c2', par2: 'c3', style: { couleur: '#000000', epaisseur: 1 } })
        fifig.push({ type: 'segment', par1: 'c2', par2: 'c4', style: { couleur: '#000000', epaisseur: 1 } })

        fifig.push({
          type: 'point',
          nom: 'd2',
          par1: 13,
          par2: 4.3,
          fixe: true,
          visible: false,
          etiquette: false,
          style: { couleur: '#000000', epaisseur: 0, taillepoint: 4, decal: [+20, +20] }
        })
        fifig.push({ type: 'segment', par1: 'b1', par2: 'd2', style: { couleur: '#000000', epaisseur: 1 } })
        fifig.push({
          type: 'point',
          nom: stor.leslettres[7],
          par1: 15.5,
          par2: 5,
          fixe: true,
          visible: true,
          etiquette: true,
          style: { couleur: '#000000', epaisseur: 0, taillepoint: 4, decal: [-10, +35] }
        })
        fifig.push({
          type: 'segment',
          par1: 'b1',
          par2: stor.leslettres[7],
          style: { couleur: '#000000', epaisseur: 1 }
        })

        break
      case 'prisme droit':
        fifig.push({
          type: 'point',
          nom: stor.leslettres[1],
          par1: 6,
          par2: 7.5,
          fixe: true,
          visible: true,
          etiquette: true,
          style: { couleur: '#000000', epaisseur: 0, taillepoint: 4, decal: [-10, +20] }
        })
        fifig.push({
          type: 'point',
          nom: stor.leslettres[2],
          par1: 8,
          par2: 3.5,
          fixe: true,
          visible: true,
          etiquette: true,
          style: { couleur: '#000000', epaisseur: 0, taillepoint: 4, decal: [0, +35] }
        })
        fifig.push({
          type: 'point',
          nom: stor.leslettres[3],
          par1: 11,
          par2: 2.5,
          fixe: true,
          visible: true,
          etiquette: true,
          style: { couleur: '#000000', epaisseur: 0, taillepoint: 4, decal: [+0, +33] }
        })
        fifig.push({
          type: 'point',
          nom: stor.leslettres[4],
          par1: 17,
          par2: 2.5,
          fixe: true,
          visible: true,
          etiquette: true,
          style: { couleur: '#000000', epaisseur: 0, taillepoint: 4, decal: [+5, +33] }
        })
        fifig.push({
          type: 'point',
          nom: stor.leslettres[5],
          par1: 19,
          par2: 6.5,
          fixe: true,
          visible: true,
          etiquette: true,
          style: { couleur: '#000000', epaisseur: 0, taillepoint: 4, decal: [+15, +20] }
        })

        fifig.push({
          type: 'segment',
          par1: stor.leslettres[1],
          par2: stor.leslettres[2],
          style: { couleur: '#000000', epaisseur: 3 }
        })
        fifig.push({
          type: 'segment',
          par1: stor.leslettres[2],
          par2: stor.leslettres[3],
          style: { couleur: '#000000', epaisseur: 3 }
        })
        fifig.push({
          type: 'segment',
          par1: stor.leslettres[3],
          par2: stor.leslettres[4],
          style: { couleur: '#000000', epaisseur: 3 }
        })
        fifig.push({
          type: 'segment',
          par1: stor.leslettres[4],
          par2: stor.leslettres[5],
          style: { couleur: '#000000', epaisseur: 3 }
        })
        fifig.push({
          type: 'segment',
          par1: stor.leslettres[5],
          par2: stor.leslettres[1],
          style: { couleur: '#000000', epaisseur: 3, pointilles: '5,5' }
        })

        fifig.push({
          type: 'point',
          nom: stor.leslettres[6],
          par1: 6,
          par2: 13.5,
          fixe: true,
          visible: true,
          etiquette: true,
          style: { couleur: '#000000', epaisseur: 0, taillepoint: 4, decal: [-10, +20] }
        })
        fifig.push({
          type: 'point',
          nom: stor.leslettres[7],
          par1: 8,
          par2: 9.5,
          fixe: true,
          visible: true,
          etiquette: true,
          style: { couleur: '#000000', epaisseur: 0, taillepoint: 4, decal: [+7, +6] }
        })
        fifig.push({
          type: 'point',
          nom: stor.leslettres[8],
          par1: 11,
          par2: 8.5,
          fixe: true,
          visible: true,
          etiquette: true,
          style: { couleur: '#000000', epaisseur: 0, taillepoint: 4, decal: [+5, +5] }
        })
        fifig.push({
          type: 'point',
          nom: stor.leslettres[9],
          par1: 17,
          par2: 8.5,
          fixe: true,
          visible: true,
          etiquette: true,
          style: { couleur: '#000000', epaisseur: 0, taillepoint: 4, decal: [-5, +5] }
        })
        fifig.push({
          type: 'point',
          nom: stor.leslettres[10],
          par1: 19,
          par2: 12.5,
          fixe: true,
          visible: true,
          etiquette: true,
          style: { couleur: '#000000', epaisseur: 0, taillepoint: 4, decal: [+15, +20] }
        })

        fifig.push({
          type: 'segment',
          par1: stor.leslettres[6],
          par2: stor.leslettres[7],
          style: { couleur: '#000000', epaisseur: 3 }
        })
        fifig.push({
          type: 'segment',
          par1: stor.leslettres[7],
          par2: stor.leslettres[8],
          style: { couleur: '#000000', epaisseur: 3 }
        })
        fifig.push({
          type: 'segment',
          par1: stor.leslettres[8],
          par2: stor.leslettres[9],
          style: { couleur: '#000000', epaisseur: 3 }
        })
        fifig.push({
          type: 'segment',
          par1: stor.leslettres[9],
          par2: stor.leslettres[10],
          style: { couleur: '#000000', epaisseur: 3 }
        })
        fifig.push({
          type: 'segment',
          par1: stor.leslettres[10],
          par2: stor.leslettres[6],
          style: { couleur: '#000000', epaisseur: 3 }
        })

        fifig.push({
          type: 'segment',
          par1: stor.leslettres[1],
          par2: stor.leslettres[6],
          style: { couleur: '#000000', epaisseur: 3 }
        })
        fifig.push({
          type: 'segment',
          par1: stor.leslettres[2],
          par2: stor.leslettres[7],
          style: { couleur: '#000000', epaisseur: 3 }
        })
        fifig.push({
          type: 'segment',
          par1: stor.leslettres[3],
          par2: stor.leslettres[8],
          style: { couleur: '#000000', epaisseur: 3 }
        })
        fifig.push({
          type: 'segment',
          par1: stor.leslettres[4],
          par2: stor.leslettres[9],
          style: { couleur: '#000000', epaisseur: 3 }
        })
        fifig.push({
          type: 'segment',
          par1: stor.leslettres[5],
          par2: stor.leslettres[10],
          style: { couleur: '#000000', epaisseur: 3 }
        })

        break
      case 'secteur de disque':
        fifig.push({
          type: 'courbe',
          par1: '2+(racine(100-(x-16)^2))',
          par2: 7,
          par3: 25,
          par4: 50,
          style: { couleur: '#000000', epaisseur: 3 }
        })
        fifig.push({
          type: 'point',
          nom: stor.leslettres[1],
          par1: 16,
          par2: 2,
          fixe: true,
          visible: true,
          etiquette: true,
          style: { couleur: '#000000', epaisseur: 0, taillepoint: 4, decal: [-20, +27] }
        })
        fifig.push({
          type: 'point',
          nom: stor.leslettres[2],
          par1: 7,
          par2: 6.5,
          fixe: true,
          visible: true,
          etiquette: true,
          style: { couleur: '#000000', epaisseur: 0, taillepoint: 4, decal: [-10, +20] }
        })
        fifig.push({
          type: 'point',
          nom: stor.leslettres[3],
          par1: 25,
          par2: 6.5,
          fixe: true,
          visible: true,
          etiquette: true,
          style: { couleur: '#000000', epaisseur: 0, taillepoint: 4, decal: [+15, +20] }
        })
        fifig.push({
          type: 'segment',
          par1: stor.leslettres[1],
          par2: stor.leslettres[2],
          style: { couleur: '#000000', epaisseur: 3 }
        })
        fifig.push({
          type: 'segment',
          par1: stor.leslettres[1],
          par2: stor.leslettres[3],
          style: { couleur: '#000000', epaisseur: 3 }
        })
        break
    }

    //

    stor.repere = new Repere({
      idConteneur: stor.lesdiv.figure,
      visible: false,
      aimantage: true,
      larg: 300,
      haut: 150,

      pasdunegraduationX: 1,
      pixelspargraduationX: 10,
      pasdunegraduationY: 1,
      pixelspargraduationY: 10,
      xO: 0,
      yO: 150,
      debuty: 0,
      negatifs: false,
      trame: false,
      objets: fifig,
      fixe: true
    })
    stor.repere.construit()
    stor.lesdiv.figure.style.background = '#ffffff'
  }

  function changetab () {
    stor.numtab++
    if (stor.numtab === 3) {
      stor.numtab = 0
    }
    const monut = stor.lestab[stor.numtab]
    stor.tablo.disaClav()
    j3pEmpty(stor.zoneTableau[0][0])
    stor.tablo = new TableauConversionMobile(stor.zoneTableau[0][0], {
      unite: monut,
      are: false,
      litre: false,
      tonne: false,
      virgule: true,
      fleches: false,
      clavier: true,
      mepact: getJ3pConteneur(me.zonesElts.MG)
    })
  }

  function yareponse () {
    if (me.isElapsed) return true
    if ((me.questionCourante % ds.nbetapes) === 1) {
      return ((stor.zonem.reponse() !== '') && (stor.zonem.reponse() !== '\\frac{ }{ }'))
    }
    if ((me.questionCourante % ds.nbetapes) === 2) {
      return stor.zoneStyleAffiche.hasResponse()
    }
    if (((me.questionCourante % ds.nbetapes) === 3) || (((me.questionCourante % ds.nbetapes) === 0) && ((!ds.ValeurApprochee2) || (ds.CalculMental)))) {
      return ((stor.zonem.reponse() !== '') && (stor.zonem.reponse() !== '\\frac{ }{ }'))
    }
    if (((me.questionCourante % ds.nbetapes) === 0) && (ds.nbetapes === 4)) {
      return (stor.zonem.reponse() !== '')
    }
  }

  function transformule (formule) {
    return j3pMathquillXcas(transformule2(formule))
  }
  function transformule2 (formu) {
    let repattendue = formu
    let reg = /text{périmètre de la base}/g
    repattendue = repattendue.replace(reg, 'w')
    reg = /text{aire de la base}/g
    repattendue = repattendue.replace(reg, 'u')
    reg = /text{base}/g
    repattendue = repattendue.replace(reg, 'b')
    reg = /text{produit des côtés de l’angle droit}/g
    repattendue = repattendue.replace(reg, 'o \\times p')

    reg = /text{somme des longueurs des côtés}/g
    let am = 'y + x + a + b'
    if (stor.laformule.figure === 'triangle quelconque') {
      am = am.replace('+ b', '')
    }
    repattendue = repattendue.replace(reg, am)

    reg = /text{côté}/g
    repattendue = repattendue.replace(reg, 'k')
    reg = /text{grande diagonale}/g
    repattendue = repattendue.replace(reg, 'n')
    reg = /text{petite diagonale}/g
    repattendue = repattendue.replace(reg, 'd')
    reg = /text{rayon}/g
    repattendue = repattendue.replace(reg, 'q')
    reg = /text{diamètre}/g
    repattendue = repattendue.replace(reg, 'g')
    reg = /text{longueur}/g
    repattendue = repattendue.replace(reg, 'l')
    reg = /text{largeur}/g
    repattendue = repattendue.replace(reg, 'v')
    reg = /text{hauteur}/g
    repattendue = repattendue.replace(reg, 'h')
    reg = /text{angle}/g
    repattendue = repattendue.replace(reg, 'z')
    let cpt = 0
    do {
      cpt++
      repattendue = repattendue.replace('\\', '')
    }
    while ((repattendue.indexOf('\\') !== -1) && (cpt < 1000))

    repattendue = repattendue.replace(/frac/g, '\\frac')
    repattendue = repattendue.replace(/times/g, '\\times')
    repattendue = repattendue.replace(/pi/g, 'π')
    repattendue = repattendue.replace(/div/g, '/')
    return repattendue
  }
  function bonneReponse () {
    // cette fonction renvoie true ou false si du moins une réponse a été saisie
    // (dans le cas contraire, elle n’est pas appelée
    // elle est appelée dans la partie "correction" de la section
    if ((me.questionCourante % ds.nbetapes) === 1) {
      stor.repeleve = stor.zonem.reponse()
      let nb1 = 0
      let nb2 = 0
      for (let i = 0; i < stor.repeleve.length; i++) {
        if (stor.repeleve[i] === ')') { nb1++ }
        if (stor.repeleve[i] === '(') { nb2++ }
      }

      stor.repeleve = stor.repeleve.replace(/\\div/g, '/')
      for (let i = 0; i < stor.lesboutons.length; i++) {
        let up = stor.lesboutons[i]
        if ((up.length > 3) && (up.substring(0, 2) === '\\w')) { up = up.replace('\\', '\\\\') }
        if (up === 'x^{2}') up = 'puissance2'
        if (up === 'x^{3}') up = 'puissance3'
        const reg = new RegExp(up, 'g')
        stor.repeleve = stor.repeleve.replace(reg, stor.quoi[i])
      }

      const notin = ['j', 'k', 'n', 'd', 'b', 'o', 'p', 'q', 'u', 'g', 'l', 'v', 'h', 'w', 'π', ')', 'z', 'x', 'y', 'a', 'b', '}']
      const notin2 = ['j', 'k', 'n', 'd', 'b', 'o', 'p', 'q', 'u', 'g', 'l', 'v', 'h', 'w', 'π', 'z', 'x', 'y', 'a', 'b', '(', '0', '1', '2', '3', '4', '5', '6', '7', '8', '9']
      const notin3 = ['0', '1', '2', '3', '4', '5', '6', '7', '8', '9']
      const notin4 = ['j', 'k', 'n', 'd', 'b', 'o', 'p', 'q', 'u', 'g', 'l', 'v', 'h', 'w', 'π', '(', 'z', 'x', 'y', 'a', 'b']
      for (let i = 0; i < stor.repeleve.length; i++) {
        if (notin.indexOf(stor.repeleve[i]) !== -1) {
          if ((notin2.indexOf(stor.repeleve[i + 1]) !== -1) || (notin2.indexOf(stor.repeleve[i + 2]) !== -1)) {
            stor.repeleve = stor.repeleve.substring(0, i + 1) + ' \\times ' + stor.repeleve.substring(i + 1, stor.repeleve.length)
            i++
          }
        }
        if (notin3.indexOf(stor.repeleve[i]) !== -1) {
          if ((notin4.indexOf(stor.repeleve[i + 1]) !== -1) || (notin4.indexOf(stor.repeleve[i + 2]) !== -1)) {
            stor.repeleve = stor.repeleve.substring(0, i + 1) + ' \\times ' + stor.repeleve.substring(i + 1, stor.repeleve.length)
            i++
          }
        }
      }
      const expEleve = j3pMathquillXcas(stor.repeleve)
      const expResultat = transformule(stor.laformule.f)
      stor.repeleve7 = expEleve
      if (nb1 !== nb2) { return false }
      if (expEleve === '0*') { return false }
      const algbuf = Algebrite.run((expEleve + '-(' + expResultat + ')').replace(/π/g, 'pi'))
      return (algbuf === '0' || algbuf === '-0' || algbuf === '0.0' || algbuf === '0.0...')
      // return j3pEgaliteFormelleWebxcas(expEleve, expResultat)
    }

    if ((me.questionCourante % ds.nbetapes) === 2) {
      stor.repeleve = []
      stor.repeleves = []
      stor.resulteleve = stor.monagarde
      stor.leresult = stor.monagarde

      for (let i = 0; i < stor.zoneStyleAffiche.liste.length; i++) {
        stor.repeleve[i] = stor.zoneStyleAffiche.liste[i].reponse()
        stor.resulteleve = stor.resulteleve.replace(caracspe[i], stor.repeleve[i])
        stor.leresult = stor.leresult.replace(caracspe[i], stor.repatt[i])
      }
      while (stor.resulteleve.indexOf('$') !== -1) {
        stor.resulteleve = stor.resulteleve.replace('$', '')
      }
      while (stor.leresult.indexOf('$') !== -1) {
        stor.leresult = stor.leresult.replace('$', '')
      }
      stor.resulteleve = stor.resulteleve.replace(/π/g, '1')
      stor.leresult = stor.leresult.replace(/π/g, '1')
      stor.resulteleve = j3pMathquillXcas(stor.resulteleve)
      stor.leresult = j3pMathquillXcas(stor.leresult)
      while (stor.resulteleve.indexOf(')1') !== -1) {
        stor.resulteleve = stor.resulteleve.replace(')1', ')*1')
      }
      while (stor.leresult.indexOf(')1') !== -1) {
        stor.leresult = stor.leresult.replace(')1', ')*1')
      }
      stor.resulteleve = extrait(moncalc(stor.resulteleve))
      stor.leresult = extrait(moncalc(stor.leresult))

      return (Math.abs(stor.resulteleve.decimale - stor.leresult.decimale) < Math.pow(10, -12))
    }
    if (((me.questionCourante % ds.nbetapes) === 3) || (((me.questionCourante % ds.nbetapes) === 0) && ((!ds.ValeurApprochee2) || (ds.CalculMental)))) {
      const bub = stor.zonem.reponse2()
      stor.repeleve = j3pArrondi((parseFloat(bub[0].replace(/ /g, '')) / parseFloat(String(bub[1]).replace(/ /g, ''))), 5)
      return (Math.abs(stor.repeleve - stor.resultatt) < Math.pow(10, -4))
    }
    if (((me.questionCourante % ds.nbetapes) === 0) && (ds.nbetapes === 4)) {
      stor.repeleve = stor.zonem.reponse().replace(',', '.').replace(/ /g, '')
      if (stor.parquoi !== 3) {
        return (Math.abs(stor.repeleve - stor.resultatt) < Math.pow(10, -12))
      } else {
        return ((Math.abs(stor.repeleve - stor.resultatt) < Math.pow(10, -12)) || (Math.abs(stor.repeleve - stor.resultattinv) < Math.pow(10, -12)))
      }
    }
  }

  function ecrisbienMAthquill (nb) {
    let monc = j3pEntierBienEcrit(Math.trunc(nb)).replace(/ /g, '\\text{ }')
    if ((nb - Math.trunc(nb)) > 0) {
      let madec = (j3pArrondi((nb - Math.trunc(nb)), 10) + '').replace('0.', '')
      if (madec.length > 3) { madec = madec.substring(0, 3) + '\\text{ }' + madec.substring(3) }
      if (madec.length > 14) { madec = madec.substring(0, 14) + '\\text{ }' + madec.substring(14) }
      if (madec.length > 25) { madec = madec.substring(0, 25) + '\\text{ }' + madec.substring(25) }
      monc += (',' + madec)
    }
    return monc
  }

  async function initSection () {
    // faut s’assurer que jQuery-ui est chargé
    await loadJqueryDialog()

    ds.ValeurApprochee2 = (ds.ApproximationDecimale !== 'Aucune')
    const nbetapes = (!ds.ValeurApprochee2 || ds.CalculMental) ? 3 : 4
    me.surcharge({ nbetapes })
    if (ds.ValeurApprochee) {
      stor.tvp = 0
    } else {
      stor.tvp = 1
    }
    ds.pe = [ds.pe_1,
      ds.pe_2,
      ds.pe_3,
      ds.pe_4,
      ds.pe_5,
      ds.pe_6,
      ds.pe_7,
      ds.pe_8,
      ds.pe_9,
      ds.pe_10,
      ds.pe_11,
      ds.pe_12,
      ds.pe_13,
      ds.pe_14,
      ds.pe_15,
      ds.pe_16,
      ds.pe_17,
      ds.pe_18,
      ds.pe_19,
      ds.pe_20,
      ds.pe_21,
      ds.pe_22]

    stor.dejapose = []
    // Construction de la page
    me.construitStructurePage('presentation1bis')

    stor.liste = stor.Liste = JSON.parse(ds.Parametrage_rapide)
    if (stor.liste.liste.Cercle_P === undefined) {
      // on la crée à partir des params de base
      stor.liste.liste.Cercle_P = ds.Cercle && ds.perimetre
      stor.liste.liste.Cercle_A = ds.Cercle && ds.aire
      stor.liste.liste.Carre_P = ds.Carre && ds.perimetre
      stor.liste.liste.Carre_A = ds.Carre && ds.aire
      stor.liste.liste.Quadrilatere_P = ds.Quadrilatere && ds.perimetre
      stor.liste.liste.Rectangle_P = ds.Rectangle && ds.perimetre
      stor.liste.liste.Rectangle_A = ds.Rectangle && ds.aire
      stor.liste.liste.Losange_P = ds.Losange && ds.perimetre
      stor.liste.liste.Losange_A = ds.Losange && ds.aire
      stor.liste.liste.Parallelogramme_P = ds.Parallelogramme && ds.perimetre
      stor.liste.liste.Parallelogramme_A = ds.Parallelogramme && ds.aire
      stor.liste.liste.TriangleRectangle_A = ds.TriangleRectangle && ds.aire
      stor.liste.liste.TriangleQuelconque_P = ds.TriangleQuelconque && ds.perimetre
      stor.liste.liste.TriangleQuelconque_A = ds.TriangleQuelconque && ds.aire
      stor.liste.liste.TriangleEquilateral_P = ds.TriangleEquilateral && ds.perimetre
      stor.liste.liste.TriangleEquilateral_A = ds.TriangleEquilateral && ds.aire
      stor.liste.liste.Secteur_A = ds.Secteur && ds.aire
      stor.liste.liste.Secteur_P = ds.Secteur && ds.perimetre
      stor.liste.liste.Cube_P = ds.Cube && ds.perimetre
      stor.liste.liste.Cube_A = ds.Cube && ds.aire
      stor.liste.liste.Cube_V = ds.Cube && ds.volume
      stor.liste.liste.ParallelepipedeRectangle_P = ds.ParallelepipedeRectangle && ds.perimetre
      stor.liste.liste.ParallelepipedeRectangle_A = ds.ParallelepipedeRectangle && ds.aire
      stor.liste.liste.ParallelepipedeRectangle_V = ds.ParallelepipedeRectangle && ds.volume
      stor.liste.liste.Cylindre_ALat = ds.Cylindre && ds.aireLaterale
      stor.liste.liste.Cylindre_A = ds.Cylindre && ds.aire
      stor.liste.liste.Cylindre_V = ds.Cylindre && ds.volume
      stor.liste.liste.Cylindre_P = ds.Cylindre && ds.perimetre
      stor.liste.liste.ConeDeRevolution_V = ds.ConeDeRevolution && ds.volume
      stor.liste.liste.ConeDeRevolution_P = ds.ConeDeRevolution && ds.perimetre
      stor.liste.liste.Sphere_A = ds.Sphere && ds.aire
      stor.liste.liste.Sphere_V = ds.Sphere && ds.volume
      stor.liste.liste.Pyramide_V = ds.Pyramide && ds.volume
      stor.liste.liste.PrismeDroit_A = ds.PrismeDroit && ds.aire
      stor.liste.liste.PrismeDroit_ALat = ds.PrismeDroit && ds.aireLaterale
      stor.liste.liste.PrismeDroit_V = ds.PrismeDroit && ds.volume
    } else {
      // on ladapte si y'a eu des changements en dure sur les params
      // si il retire un param
      stor.liste.liste.Cercle_P = stor.liste.liste.Cercle_P && ds.Cercle && ds.perimetre
      stor.liste.liste.Cercle_A = stor.liste.liste.Cercle_A && ds.Cercle && ds.aire
      stor.liste.liste.Carre_P = stor.liste.liste.Carre_P && ds.Carre && ds.perimetre
      stor.liste.liste.Carre_A = stor.liste.liste.Carre_A && ds.Carre && ds.aire
      stor.liste.liste.Quadrilatere_P = stor.liste.liste.Quadrilatere_P && ds.Quadrilatere && ds.perimetre
      stor.liste.liste.Rectangle_P = stor.liste.liste.Rectangle_P && ds.Rectangle && ds.perimetre
      stor.liste.liste.Rectangle_A = stor.liste.liste.Rectangle_A && ds.Rectangle && ds.aire
      stor.liste.liste.Losange_P = stor.liste.liste.Losange_P && ds.Losange && ds.perimetre
      stor.liste.liste.Losange_A = stor.liste.liste.Losange_A && ds.Losange && ds.aire
      stor.liste.liste.Parallelogramme_P = stor.liste.liste.Parallelogramme_P && ds.Parallelogramme && ds.perimetre
      stor.liste.liste.Parallelogramme_A = stor.liste.liste.Parallelogramme_A && ds.Parallelogramme && ds.aire
      stor.liste.liste.TriangleRectangle_A = stor.liste.liste.TriangleRectangle_A && ds.TriangleRectangle && ds.aire
      stor.liste.liste.TriangleQuelconque_P = stor.liste.liste.TriangleQuelconque_P && ds.TriangleQuelconque && ds.perimetre
      stor.liste.liste.TriangleQuelconque_A = stor.liste.liste.TriangleQuelconque_A && ds.TriangleQuelconque && ds.aire
      stor.liste.liste.TriangleEquilateral_P = stor.liste.liste.TriangleEquilateral_P && ds.TriangleEquilateral && ds.perimetre
      stor.liste.liste.TriangleEquilateral_A = stor.liste.liste.TriangleEquilateral_A && ds.TriangleEquilateral && ds.aire
      stor.liste.liste.Secteur_A = stor.liste.liste.Secteur_A && ds.Secteur && ds.aire
      stor.liste.liste.Secteur_P = stor.liste.liste.Secteur_P && ds.Secteur && ds.perimetre
      stor.liste.liste.Cube_P = stor.liste.liste.Cube_P && ds.Cube && ds.perimetre
      stor.liste.liste.Cube_A = stor.liste.liste.Cube_A && ds.Cube && ds.aire
      stor.liste.liste.Cube_V = stor.liste.liste.Cube_V && ds.Cube && ds.volume
      stor.liste.liste.ParallelepipedeRectangle_P = stor.liste.liste.ParallelepipedeRectangle_P && ds.ParallelepipedeRectangle && ds.perimetre
      stor.liste.liste.ParallelepipedeRectangle_A = stor.liste.liste.ParallelepipedeRectangle_A && ds.ParallelepipedeRectangle && ds.aire
      stor.liste.liste.ParallelepipedeRectangle_V = stor.liste.liste.ParallelepipedeRectangle_V && ds.ParallelepipedeRectangle && ds.volume
      stor.liste.liste.Cylindre_ALat = stor.liste.liste.Cylindre_ALat && ds.Cylindre && ds.aireLaterale
      stor.liste.liste.Cylindre_A = stor.liste.liste.Cylindre_A && ds.Cylindre && ds.aire
      stor.liste.liste.Cylindre_V = stor.liste.liste.Cylindre_V && ds.Cylindre && ds.volume
      stor.liste.liste.Cylindre_P = stor.liste.liste.Cylindre_P && ds.Cylindre && ds.perimetre
      stor.liste.liste.ConeDeRevolution_V = stor.liste.liste.ConeDeRevolution_V && ds.ConeDeRevolution && ds.volume
      stor.liste.liste.ConeDeRevolution_P = stor.liste.liste.ConeDeRevolution_P && ds.ConeDeRevolution && ds.perimetre
      stor.liste.liste.Sphere_A = stor.liste.liste.Sphere_A && ds.Sphere && ds.aire
      stor.liste.liste.Sphere_V = stor.liste.liste.Sphere_V && ds.Sphere && ds.volume
      stor.liste.liste.Pyramide_V = stor.liste.liste.Pyramide_V && ds.Pyramide && ds.volume
      stor.liste.liste.PrismeDroit_A = stor.liste.liste.PrismeDroit_A && ds.PrismeDroit && ds.aire
      stor.liste.liste.PrismeDroit_ALat = stor.liste.liste.PrismeDroit_ALat && ds.PrismeDroit && ds.aireLaterale
      stor.liste.liste.PrismeDroit_V = stor.liste.liste.PrismeDroit_V && ds.PrismeDroit && ds.volume
      // si il ajoute un param
      if (ds.Cercle && !stor.liste.liste.Cercle_P && !stor.liste.liste.Cercle_A) {
        stor.liste.liste.Cercle_P = ds.perimetre
        stor.liste.liste.Cercle_A = ds.aire
      }
      if (ds.Carre && !stor.liste.liste.Carre_P && !stor.liste.liste.Carre_A) {
        stor.liste.liste.Carre_P = ds.perimetre
        stor.liste.liste.Carre_A = ds.aire
      }
      if (ds.Quadrilatere && !stor.liste.liste.QUADRILATERE) {
        stor.liste.liste.QUADRILATERE = ds.perimetre
      }
      if (ds.Rectangle && !stor.liste.liste.Rectangle_P && !stor.liste.liste.Rectangle_A) {
        stor.liste.liste.Rectangle_P = ds.perimetre
        stor.liste.liste.Rectangle_A = ds.aire
      }
      if (ds.Losange && !stor.liste.liste.Losange_P && !stor.liste.liste.Losange_A) {
        stor.liste.liste.Losange_P = ds.perimetre
        stor.liste.liste.Losange_A = ds.aire
      }
      if (ds.Parallelogramme && !stor.liste.liste.Parallelogramme_P && !stor.liste.liste.Parallelogramme_A) {
        stor.liste.liste.Parallelogramme_P = ds.perimetre
        stor.liste.liste.Parallelogramme_A = ds.aire
      }
      if (ds.TriangleRectangle && !stor.liste.liste.TriangleRectangle_A) {
        stor.liste.liste.TriangleRectangle_A = ds.aire
      }
      if (ds.TriangleQuelconque && !stor.liste.liste.TriangleQuelconque_P && !stor.liste.liste.TriangleQuelconque_A) {
        stor.liste.liste.TriangleQuelconque_P = ds.perimetre
        stor.liste.liste.TriangleQuelconque_A = ds.aire
      }
      if (ds.TriangleEquilateral && !stor.liste.liste.TriangleEquilateral_P && !stor.liste.liste.TriangleEquilateral_A) {
        stor.liste.liste.TriangleEquilateral_P = ds.perimetre
        stor.liste.liste.TriangleEquilateral_P = ds.aire
      }
      if (ds.Secteur && !stor.liste.liste.Secteur_A && !stor.liste.liste.Secteur_P) {
        stor.liste.liste.Secteur_A = ds.aire
        stor.liste.liste.Secteur_P = ds.perimetre
      }
      if (ds.Cube && !stor.liste.liste.Cube_P && !stor.liste.liste.Cube_A && !stor.liste.liste.Cube_V) {
        stor.liste.liste.Cube_P = ds.perimetre
        stor.liste.liste.Cube_A = ds.aire
        stor.liste.liste.Cube_V = ds.volume
      }
      if (ds.ParallelepipedeRectangle && !stor.liste.liste.ParallelepipedeRectangle_P && !stor.liste.liste.ParallelepipedeRectangle_A && !stor.liste.liste.ParallelepipedeRectangle_V) {
        stor.liste.liste.ParallelepipedeRectangle_P = ds.perimetre
        stor.liste.liste.ParallelepipedeRectangle_A = ds.aire
        stor.liste.liste.ParallelepipedeRectangle_V = ds.volume
      }
      if (ds.Cylindre && !stor.liste.liste.Cylindre_P && !stor.liste.liste.Cylindre_A && !stor.liste.liste.Cylindre_ALat && !stor.liste.liste.Cylindre_V) {
        stor.liste.liste.Cylindre_P = ds.perimetre
        stor.liste.liste.Cylindre_A = ds.aire
        stor.liste.liste.Cylindre_V = ds.volume
        stor.liste.liste.Cylindre_ALat = ds.aireLaterale
      }
      if (ds.ConeDeRevolution && !stor.liste.liste.ConeDeRevolution_P && !stor.liste.liste.ConeDeRevolution_V) {
        stor.liste.liste.ConeDeRevolution_P = ds.perimetre
        stor.liste.liste.ConeDeRevolution_V = ds.volume
      }
      if (ds.Sphere && !stor.liste.liste.Sphere_P && !stor.liste.liste.Sphere_A && !stor.liste.liste.Sphere_V) {
        stor.liste.liste.Sphere_A = ds.aire
        stor.liste.liste.Sphere_V = ds.volume
      }
      if (ds.Pyramide && !stor.liste.liste.Pyramide_P && !stor.liste.liste.Pyramide_A && !stor.liste.liste.Pyramide_V) {
        stor.liste.liste.Pyramide_V = ds.volume
      }
      if (ds.PrismeDroit && !stor.liste.liste.PrismeDroit_P && !stor.liste.liste.PrismeDroit_A && !stor.liste.liste.PrismeDroit_ALat && !stor.liste.liste.PrismeDroit_V) {
        stor.liste.liste.PrismeDroit_A = ds.aire
        stor.liste.liste.PrismeDroit_V = ds.volume
        stor.liste.liste.PrismeDroit_ALat = ds.aireLaterale
      }
    }

    stor.formules02Possibles = []
    for (let i = 0; i < textes.formules02.length; i++) {
      for (let j = 0; j < textes.formules02[i].perimetre.length; j++) {
        stor.formules02Possibles.push({
          g: 0,
          noms: textes.formules02[i].noms,
          grandeur: 'le périmètre',
          figure: textes.formules02[i].figure,
          nbpoints: textes.formules02[i].nbpoints,
          genre: textes.formules02[i].genre,
          f: textes.formules02[i].perimetre[j].f,
          n: textes.formules02[i].perimetre[j].n,
          o: textes.formules02[i].perimetre[j].o
        })
      }
      for (let j = 0; j < textes.formules02[i].aire.length; j++) {
        stor.formules02Possibles.push({
          g: 1,
          noms: textes.formules02[i].noms,
          grandeur: 'l’aire',
          figure: textes.formules02[i].figure,
          nbpoints: textes.formules02[i].nbpoints,
          genre: textes.formules02[i].genre,
          f: textes.formules02[i].aire[j].f,
          n: textes.formules02[i].aire[j].n,
          o: textes.formules02[i].aire[j].o
        })
      }
      for (let j = 0; j < textes.formules02[i].airelaterale.length; j++) {
        stor.formules02Possibles.push({
          g: 2,
          noms: textes.formules02[i].noms,
          grandeur: 'l’aire latérale',
          figure: textes.formules02[i].figure,
          nbpoints: textes.formules02[i].nbpoints,
          genre: textes.formules02[i].genre,
          f: textes.formules02[i].airelaterale[j].f,
          n: textes.formules02[i].airelaterale[j].n,
          o: textes.formules02[i].airelaterale[j].o
        })
      }
      for (let j = 0; j < textes.formules02[i].volume.length; j++) {
        stor.formules02Possibles.push({
          g: 3,
          noms: textes.formules02[i].noms,
          grandeur: 'le volume',
          figure: textes.formules02[i].figure,
          nbpoints: textes.formules02[i].nbpoints,
          genre: textes.formules02[i].genre,
          f: textes.formules02[i].volume[j].f,
          n: textes.formules02[i].volume[j].n,
          o: textes.formules02[i].volume[j].o
        })
      }
    }

    if (!ds.Optionnel) {
      for (let i = stor.formules02Possibles.length - 1; i > -1; i--) {
        if (stor.formules02Possibles[i].o) {
          stor.formules02Possibles.splice(i, 1)
        }
      }
    }
    if (!ds.NotationExpert) {
      for (let i = stor.formules02Possibles.length - 1; i > -1; i--) {
        if (stor.formules02Possibles[i].f.indexOf('^') !== -1 || stor.formules02Possibles[i].f.indexOf('frac') !== -1) {
          stor.formules02Possibles.splice(i, 1)
        }
      }
    }
    stor.autoriseniv = []
    stor.autoriseniv.push(ds.niveau6e)
    stor.autoriseniv.push(ds.niveau5e)
    stor.autoriseniv.push(ds.niveau4e)
    stor.autoriseniv.push(ds.niveau3e)
    if (stor.autoriseniv === [false, false, false, false]) {
      stor.autoriseniv = [true, true, true, true]
    }
    for (let i = 0; i < 4; i++) {
      if (!stor.autoriseniv[i]) {
        for (let j = stor.formules02Possibles.length - 1; j > -1; j--) {
          if (stor.formules02Possibles[j].n === textes.niveau[i]) {
            stor.formules02Possibles.splice(j, 1)
          }
        }
      }
    }
    // Dans stor.formules02Possibles il reste que celles qui
    // correspondent à niv, option et exp

    for (let i = stor.formules02Possibles.length - 1; i > -1; i--) {
      if (stor.formules02Possibles[i].g === 0) {
        if (!stor.liste.liste.Cercle_P && stor.formules02Possibles[i].figure === 'cercle') {
          stor.formules02Possibles.splice(i, 1)
          continue
        }
        if (!stor.liste.liste.Carre_P && stor.formules02Possibles[i].figure === 'carré') {
          stor.formules02Possibles.splice(i, 1)
          continue
        }
        if (!stor.liste.liste.Quadrilatere_P && stor.formules02Possibles[i].figure === 'figure') {
          stor.formules02Possibles.splice(i, 1)
          continue
        }
        if (!stor.liste.liste.Rectangle_P && stor.formules02Possibles[i].figure === 'rectangle') {
          stor.formules02Possibles.splice(i, 1)
          continue
        }
        if (!stor.liste.liste.Losange_P && stor.formules02Possibles[i].figure === 'losange') {
          stor.formules02Possibles.splice(i, 1)
          continue
        }
        if (!stor.liste.liste.Parallelogramme_P && stor.formules02Possibles[i].figure === 'parallélogramme') {
          stor.formules02Possibles.splice(i, 1)
          continue
        }
        if (!stor.liste.liste.TriangleQuelconque_P && stor.formules02Possibles[i].figure === 'triangle quelconque') {
          stor.formules02Possibles.splice(i, 1)
          continue
        }
        if (!stor.liste.liste.TriangleEquilateral_P && stor.formules02Possibles[i].figure === 'triangle équilatéral') {
          stor.formules02Possibles.splice(i, 1)
          continue
        }
        if (!stor.liste.liste.Secteur_P && stor.formules02Possibles[i].figure === 'secteur de disque') {
          stor.formules02Possibles.splice(i, 1)
          continue
        }
        if (!stor.liste.liste.Cube_P && stor.formules02Possibles[i].figure === 'cube') {
          stor.formules02Possibles.splice(i, 1)
          continue
        }
        if (!stor.liste.liste.ParallelepipedeRectangle_P && stor.formules02Possibles[i].figure === 'parallélépipède rectangle') {
          stor.formules02Possibles.splice(i, 1)
          continue
        }
        if (!stor.liste.liste.Cylindre_P && stor.formules02Possibles[i].figure === 'cylindre') {
          stor.formules02Possibles.splice(i, 1)
          continue
        }
        if (!stor.liste.liste.ConeDeRevolution_P && stor.formules02Possibles[i].figure === 'cône de révolution') {
          stor.formules02Possibles.splice(i, 1)
          continue
        }
      } else if (stor.formules02Possibles[i].g === 1) {
        // aire
        if (!stor.liste.liste.TriangleEquilateral_A && stor.formules02Possibles[i].figure === 'triangle équilatéral') {
          stor.formules02Possibles.splice(i, 1)
          continue
        }
        if (!stor.liste.liste.Cercle_A && stor.formules02Possibles[i].figure === 'cercle') {
          stor.formules02Possibles.splice(i, 1)
          continue
        }
        if (!stor.liste.liste.Carre_A && stor.formules02Possibles[i].figure === 'carré') {
          stor.formules02Possibles.splice(i, 1)
          continue
        }
        if (!stor.liste.liste.Rectangle_A && stor.formules02Possibles[i].figure === 'rectangle') {
          stor.formules02Possibles.splice(i, 1)
          continue
        }
        if (!stor.liste.liste.Losange_A && stor.formules02Possibles[i].figure === 'losange') {
          stor.formules02Possibles.splice(i, 1)
          continue
        }
        if (!stor.liste.liste.Parallelogramme_A && stor.formules02Possibles[i].figure === 'parallélogramme') {
          stor.formules02Possibles.splice(i, 1)
          continue
        }
        if (!stor.liste.liste.TriangleRectangle_A && stor.formules02Possibles[i].figure === 'triangle') {
          stor.formules02Possibles.splice(i, 1)
          continue
        }
        if (!stor.liste.liste.TriangleQuelconque_A && stor.formules02Possibles[i].figure === 'triangle quelconque') {
          stor.formules02Possibles.splice(i, 1)
          continue
        }
        if (!stor.liste.liste.Cube_A && stor.formules02Possibles[i].figure === 'cube') {
          stor.formules02Possibles.splice(i, 1)
          continue
        }
        if (!stor.liste.liste.ParallelepipedeRectangle_A && stor.formules02Possibles[i].figure === 'parallélépipède rectangle') {
          stor.formules02Possibles.splice(i, 1)
          continue
        }
        if (!stor.liste.liste.Cylindre_A && stor.formules02Possibles[i].figure === 'cylindre') {
          stor.formules02Possibles.splice(i, 1)
          continue
        }
        if (!stor.liste.liste.Sphere_A && stor.formules02Possibles[i].figure === 'sphère') {
          stor.formules02Possibles.splice(i, 1)
          continue
        }
        if (!stor.liste.liste.Secteur_A && stor.formules02Possibles[i].figure === 'secteur de disque') {
          stor.formules02Possibles.splice(i, 1)
          continue
        }
      } else if (stor.formules02Possibles[i].g === 2) {
        // aire late
        if (!stor.liste.liste.Cylindre_ALat && stor.formules02Possibles[i].figure === 'cylindre') {
          stor.formules02Possibles.splice(i, 1)
          continue
        }
        if (!stor.liste.liste.PrismeDroit_ALat && stor.formules02Possibles[i].figure === 'prisme droit') {
          stor.formules02Possibles.splice(i, 1)
          continue
        } // test
      } else {
        // volume
        if (!stor.liste.liste.Cube_V && stor.formules02Possibles[i].figure === 'cube') {
          stor.formules02Possibles.splice(i, 1)
          continue
        }
        if (!stor.liste.liste.ParallelepipedeRectangle_V && stor.formules02Possibles[i].figure === 'parallélépipède rectangle') {
          stor.formules02Possibles.splice(i, 1)
          continue
        }
        if (!stor.liste.liste.Cylindre_V && stor.formules02Possibles[i].figure === 'cylindre') {
          stor.formules02Possibles.splice(i, 1)
          continue
        }
        if (!stor.liste.liste.ConeDeRevolution_V && stor.formules02Possibles[i].figure === 'cône de révolution') {
          stor.formules02Possibles.splice(i, 1)
          continue
        }
        if (!stor.liste.liste.Sphere_V && stor.formules02Possibles[i].figure === 'sphère') {
          stor.formules02Possibles.splice(i, 1)
          continue
        }
        if (!stor.liste.liste.Pyramide_V && stor.formules02Possibles[i].figure === 'pyramide') {
          stor.formules02Possibles.splice(i, 1)
          continue
        }
        if (!stor.liste.liste.PrismeDroit_V && stor.formules02Possibles[i].figure === 'prisme droit') {
          stor.formules02Possibles.splice(i, 1)
          continue
        }
      }
    }

    if (stor.formules02Possibles.length === 0) {
      j3pShowError('Aucune formule n’est disponible avec ce paramètrage')
      stor.formules02Possibles.push({
        g: 0,
        noms: textes.formules02[0].noms,
        grandeur: textes.grandeur[0],
        figure: textes.formules02[0].figure,
        nbpoints: textes.formules02[0].nbpoints,
        genre: textes.formules02[0].genre,
        f: textes.formules02[0].perimetre[0].f,
        n: textes.formules02[0].perimetre[0].n,
        o: textes.formules02[0].perimetre[0].o
      })
    }

    /*
               Par convention,`
              `   me.typederreurs[0] = nombre de bonnes réponses
                  me.typederreurs[1] = nombre de fois où l’élève a utilisé une chance supplémentaire
                  me.typederreurs[2] = nombre de mauvaises réponses
                  me.typederreurs[10] = nombre de fois où l’élève a eu faux à cause d’un temps limite
                  LES AUTRES peuvent être utilisées par la détermination de la pe dans le cas d’une section qualitative
               */
    // me.typederreurs[3] = erreur formule
    // me.typederreurs[4] = erreur conversion
    // me.typederreurs[5] = confusion par defaut par exces
    me.typederreurs = [0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0]
    me.afficheTitre('Utiliser une formule')
    enonceMain()
  }
  function enonceMain () {
    // A chaque répétition, on nettoie la scène
    if (stor.aide1 !== undefined) {
      stor.aide1.disable()
      stor.aide1 = undefined
    }
    if (stor.bull !== undefined) {
      while (stor.bull.length > 0) stor.bull.pop().disable()
    }

    stor.zoneliste = []
    stor.zoneclick = []
    stor.bull = []
    if ((me.questionCourante % ds.nbetapes) === 1) {
      if (stor.dialog) {
        stor.dialog.destroy()
        stor.dialog = null
      }
      me.videLesZones()
      creeLesDiv()
      let cpt = 0
      let ok = true
      let igarde
      do {
        const i = j3pGetRandomInt(0, (stor.formules02Possibles.length - 1))
        ok = (stor.dejapose.indexOf(i) === -1)
        cpt++
        igarde = i
      } while ((!ok) && (cpt < 50))
      stor.dejapose.push(igarde)
      j3pAffiche(stor.lesdiv.etape, null, textes.Etape1)

      if (!ds.CalculMental) {
        me.fenetresjq = [{ name: 'Calculatrice', title: 'Une calculatrice', left: 600, top: 10 }]
        j3pCreeFenetres(me)
        j3pAfficheCroixFenetres('Calculatrice')
        j3pAjouteBouton(stor.lesdiv.calculatrice, 'Calculatrice', 'MepBoutons', 'Calculatrice', boutonfen)
      }

      stor.laformule = stor.formules02Possibles[igarde]
      stor.reponsespossibles = []
      let ajunit
      for (let i = 0; i < textes.formules02.length; i++) {
        if (textes.formules02[i].figure === stor.laformule.figure) {
          switch (stor.laformule.grandeur) {
            case textes.grandeur[0]:
              for (let j = 0; j < textes.formules02[i].perimetre.length; j++) {
                stor.reponsespossibles.push(textes.formules02[i].perimetre[j].f)
              }
              ajunit = ''
              break
            case textes.grandeur[1]:
              for (let j = 0; j < textes.formules02[i].aire.length; j++) {
                stor.reponsespossibles.push(textes.formules02[i].aire[j].f)
              }
              ajunit = '²'
              break
            case textes.grandeur[2]:
              for (let j = 0; j < textes.formules02[i].airelaterale.length; j++) {
                stor.reponsespossibles.push(textes.formules02[i].airelaterale[j].f)
              }
              ajunit = '²'
              break
            case textes.grandeur[3]:
              for (let j = 0; j < textes.formules02[i].volume.length; j++) {
                stor.reponsespossibles.push(textes.formules02[i].volume[j].f)
              }
              ajunit = '³'
          }
        }
      }
      switch (ds.ApproximationDecimale) {
        case 'indéfini':
          stor.parquoi = 3
          stor.PhraseVal = textes.Vapp3[stor.tvp]
          break
        case 'Exces_Defaut' :
          stor.parquoi = j3pGetRandomInt(0, 1)
          stor.PhraseVal = textes.Vapp1[stor.tvp]
          break
        case 'Exces_Defaut_Arrondi' :
          stor.parquoi = j3pGetRandomInt(0, 2)
          stor.PhraseVal = textes.Vapp1[stor.tvp]
          if (stor.parquoi === 2) {
            stor.PhraseVal = textes.Vapp2[stor.tvp]
          }
          break
        case 'Arrondi' :
          stor.parquoi = 2
          stor.PhraseVal = textes.Vapp2[stor.tvp]
          break
      }
      const tablettres = []
      for (let i = 0; i < stor.laformule.nbpoints; i++) {
        let il
        do {
          il = j3pGetRandomInt(65, 90)
          ok = true
          for (let j = 0; j < i; j++) {
            if (il === tablettres[j]) {
              ok = false
              break
            }
          }
        } while (!ok)
        tablettres.push(il)
      }
      stor.leslettres = []
      for (let i = 0; i < tablettres.length; i++) {
        stor.leslettres.push(String.fromCharCode(tablettres[i]))
        // stor.leslettres.push(i + '')
      }
      stor.lenom = ''
      for (let i = 0; i < stor.laformule.noms.nom.length; i++) {
        if (stor.leslettres[stor.laformule.noms.nom[i]] === undefined) {
          stor.lenom += stor.laformule.noms.nom[i]
        } else {
          stor.lenom += stor.leslettres[stor.laformule.noms.nom[i]]
        }
      }
      stor.ajout = ''
      if (stor.laformule.noms.ajout !== undefined) {
        for (let i = 0; i < stor.laformule.noms.ajout.length; i++) {
          if (stor.leslettres[stor.laformule.noms.ajout[i]] === undefined) {
            stor.ajout += stor.laformule.noms.ajout[i]
          } else {
            stor.ajout += stor.leslettres[stor.laformule.noms.ajout[i]]
          }
        }
      }

      stor.unitei = j3pGetRandomInt(0, 6)
      stor.unitebase = textes.unites[stor.unitei]
      stor.uniterep = stor.unitebase + ajunit
      stor.lesmesures = []
      stor.lesboutons = []
      stor.lesboutonsfonc = []
      stor.lesunite = []
      stor.quoi = []

      /// en fonction de la formule choisie , détermine les boutons possibles
      if ((stor.laformule.f.indexOf('côté') !== -1) && (stor.laformule.f.indexOf('produit') === -1) && (stor.laformule.f.indexOf('somme') === -1)) {
        const i = j3pGetRandomInt(0, (stor.laformule.noms.cote.length - 1))
        stor.lesboutons.push(stor.leslettres[stor.laformule.noms.cote[i][0]] + stor.leslettres[stor.laformule.noms.cote[i][1]])
        stor.lesmesures.push(j3pArrondi((j3pGetRandomInt(11, 35) * 10 + j3pGetRandomInt(1, 9)) / 10, 2))
        if (ds.CalculMental) {
          stor.lesmesures[stor.lesmesures.length - 1] = j3pGetRandomInt(1, 4)
        }
        const cik = stor.leslettres[stor.laformule.noms.cote[i][0]]
        const cuk = stor.leslettres[stor.laformule.noms.cote[i][1]]
        stor.lesboutonsfonc.push(function () {
          stor.zonem.majaffiche(cik + cuk)
          setTimeout(function () { stor.zonem.focus() }, 10)
        })
        stor.quoi.push('k')
        stor.lesunite.push(stor.unitebase)
      }
      if ((stor.laformule.f.indexOf('somme') !== -1)) {
        let i = j3pGetRandomInt(0, (stor.laformule.noms.cote1.length - 1))
        stor.lesboutons.push(stor.leslettres[stor.laformule.noms.cote1[i][0]] + stor.leslettres[stor.laformule.noms.cote1[i][1]])
        stor.lesmesures.push(j3pArrondi((j3pGetRandomInt(11, 35) * 10 + j3pGetRandomInt(1, 9)) / 10, 2))
        if (ds.CalculMental) {
          stor.lesmesures[stor.lesmesures.length - 1] = j3pGetRandomInt(14, 30)
        }
        const b1ik = stor.leslettres[stor.laformule.noms.cote1[i][0]]
        const b1uk = stor.leslettres[stor.laformule.noms.cote1[i][1]]
        stor.lesboutonsfonc.push(function () {
          stor.zonem.majaffiche(b1ik + b1uk)
          setTimeout(function () { stor.zonem.focus() }, 10)
        })
        stor.quoi.push('y')
        stor.lesunite.push(stor.unitebase)

        i = j3pGetRandomInt(0, (stor.laformule.noms.cote2.length - 1))
        stor.lesboutons.push(stor.leslettres[stor.laformule.noms.cote2[i][0]] + stor.leslettres[stor.laformule.noms.cote2[i][1]])
        stor.lesmesures.push(j3pArrondi((j3pGetRandomInt(11, 35) * 10 + j3pGetRandomInt(1, 9)) / 10, 2))
        if (ds.CalculMental) {
          stor.lesmesures[stor.lesmesures.length - 1] = j3pGetRandomInt(14, 30)
        }
        const b2ik = stor.leslettres[stor.laformule.noms.cote2[i][0]]
        const b2uk = stor.leslettres[stor.laformule.noms.cote2[i][1]]
        stor.lesboutonsfonc.push(function () {
          stor.zonem.majaffiche(b2ik + b2uk)
          setTimeout(function () { stor.zonem.focus() }, 10)
        })
        stor.quoi.push('x')
        stor.lesunite.push(stor.unitebase)

        i = j3pGetRandomInt(0, (stor.laformule.noms.cote3.length - 1))
        stor.lesboutons.push(stor.leslettres[stor.laformule.noms.cote3[i][0]] + stor.leslettres[stor.laformule.noms.cote3[i][1]])
        stor.lesmesures.push(j3pArrondi((j3pGetRandomInt(11, 35) * 10 + j3pGetRandomInt(1, 9)) / 10, 2))
        if (ds.CalculMental) {
          stor.lesmesures[stor.lesmesures.length - 1] = j3pGetRandomInt(14, 30)
        }
        const b3ik = stor.leslettres[stor.laformule.noms.cote3[i][0]]
        const b3uk = stor.leslettres[stor.laformule.noms.cote3[i][1]]
        stor.lesboutonsfonc.push(function () {
          stor.zonem.majaffiche(b3ik + b3uk)
          setTimeout(function () { stor.zonem.focus() }, 10)
        })
        stor.quoi.push('a')
        stor.lesunite.push(stor.unitebase)

        if (stor.laformule.figure !== 'triangle quelconque') {
          i = j3pGetRandomInt(0, (stor.laformule.noms.cote4.length - 1))
          stor.lesboutons.push(stor.leslettres[stor.laformule.noms.cote4[i][0]] + stor.leslettres[stor.laformule.noms.cote4[i][1]])
          stor.lesmesures.push(j3pArrondi((j3pGetRandomInt(11, 35) * 10 + j3pGetRandomInt(1, 9)) / 10, 2))
          if (ds.CalculMental) {
            stor.lesmesures[stor.lesmesures.length - 1] = j3pGetRandomInt(14, 30)
          }
          const b4ik = stor.leslettres[stor.laformule.noms.cote4[i][0]]
          const b4uk = stor.leslettres[stor.laformule.noms.cote4[i][1]]
          stor.lesboutonsfonc.push(function () {
            stor.zonem.majaffiche(b4ik + b4uk)
            setTimeout(function () { stor.zonem.focus() }, 10)
          })
          stor.quoi.push('b')
          stor.lesunite.push(stor.unitebase)
        }
      }
      if (stor.laformule.f.indexOf('petite diagonale') !== -1) {
        let i = j3pGetRandomInt(0, (stor.laformule.noms.grandediagonale.length - 1))
        stor.lesboutons.push(stor.leslettres[stor.laformule.noms.grandediagonale[i][0]] + stor.leslettres[stor.laformule.noms.grandediagonale[i][1]])
        stor.lesmesures.push(j3pArrondi((j3pGetRandomInt(50, 89) * 10 + j3pGetRandomInt(1, 9)) / 10, 2))
        if (ds.CalculMental) {
          stor.lesmesures[stor.lesmesures.length - 1] = j3pGetRandomInt(5, 9)
        }
        const dik = stor.leslettres[stor.laformule.noms.grandediagonale[i][0]]
        const duk = stor.leslettres[stor.laformule.noms.grandediagonale[i][1]]
        stor.lesboutonsfonc.push(function () {
          stor.zonem.majaffiche(dik + duk)
          setTimeout(function () { stor.zonem.focus() }, 10)
        })
        stor.quoi.push('n')
        stor.lesunite.push(stor.unitebase)
        i = j3pGetRandomInt(0, (stor.laformule.noms.petitediagonale.length - 1))
        stor.lesboutons.push(stor.leslettres[stor.laformule.noms.petitediagonale[i][0]] + stor.leslettres[stor.laformule.noms.petitediagonale[i][1]])
        stor.lesmesures.push(j3pArrondi((j3pGetRandomInt(36, 49) * 10 + j3pGetRandomInt(1, 9)) / 10, 2))
        if (ds.CalculMental) {
          stor.lesmesures[stor.lesmesures.length - 1] = j3pGetRandomInt(3, 5)
        }
        const gik = stor.leslettres[stor.laformule.noms.petitediagonale[i][0]]
        const guk = stor.leslettres[stor.laformule.noms.petitediagonale[i][1]]
        stor.lesboutonsfonc.push(function () {
          stor.zonem.majaffiche(gik + guk)
          setTimeout(function () { stor.zonem.focus() }, 10)
        })
        stor.quoi.push('d')
        stor.lesunite.push(stor.unitebase)
      }
      if ((stor.laformule.f.indexOf('côté') !== -1) && (stor.laformule.f.indexOf('produit') !== -1)) {
        stor.lesboutons.push(stor.leslettres[stor.laformule.noms.cote1[0][0]] + stor.leslettres[stor.laformule.noms.cote1[0][1]])
        stor.lesmesures.push(j3pArrondi((j3pGetRandomInt(11, 35) * 10 + j3pGetRandomInt(1, 9)) / 10, 2))
        if (ds.CalculMental) {
          stor.lesmesures[stor.lesmesures.length - 1] = j3pGetRandomInt(3, 9)
        }
        const aik = stor.leslettres[stor.laformule.noms.cote1[0][0]]
        const auk = stor.leslettres[stor.laformule.noms.cote1[0][1]]
        stor.lesboutonsfonc.push(function () {
          stor.zonem.majaffiche(aik + auk)
          setTimeout(function () { stor.zonem.focus() }, 10)
        })
        stor.quoi.push('o')
        stor.lesunite.push(stor.unitebase)
        stor.lesboutons.push(stor.leslettres[stor.laformule.noms.cote2[0][0]] + stor.leslettres[stor.laformule.noms.cote2[0][1]])
        stor.lesmesures.push(j3pArrondi((j3pGetRandomInt(11, 35) * 10 + j3pGetRandomInt(1, 9)) / 10, 2))
        if (ds.CalculMental) {
          stor.lesmesures[stor.lesmesures.length - 1] = j3pGetRandomInt(3, 9)
        }
        const bik = stor.leslettres[stor.laformule.noms.cote2[0][0]]
        const buk = stor.leslettres[stor.laformule.noms.cote2[0][1]]
        stor.lesboutonsfonc.push(function () {
          stor.zonem.majaffiche(bik + buk)
          setTimeout(function () { stor.zonem.focus() }, 10)
        })
        stor.quoi.push('p')
        stor.lesunite.push(stor.unitebase)
      }
      if (stor.laformule.f.indexOf('rayon') !== -1) {
        const i = j3pGetRandomInt(0, (stor.laformule.noms.rayon.length - 1))
        stor.lesboutons.push(stor.leslettres[stor.laformule.noms.rayon[i][0]] + stor.leslettres[stor.laformule.noms.rayon[i][1]])
        stor.lesmesures.push(j3pArrondi((j3pGetRandomInt(4, 25) * 3 * 10 + j3pGetRandomInt(1, 9)) / 10, 2))
        if (ds.CalculMental) {
          stor.lesmesures[stor.lesmesures.length - 1] = j3pGetRandomInt(3, 7)
        }
        const eik = stor.leslettres[stor.laformule.noms.rayon[i][0]]
        const euk = stor.leslettres[stor.laformule.noms.rayon[i][1]]
        stor.lesboutonsfonc.push(function () {
          stor.zonem.majaffiche(eik + euk)
          setTimeout(function () { stor.zonem.focus() }, 10)
        })
        stor.quoi.push('q')
        stor.lesunite.push(stor.unitebase)
      }
      if (stor.laformule.f.indexOf('{angle') !== -1) {
        const i = j3pGetRandomInt(0, (stor.laformule.noms.angle.length - 1))
        stor.lesboutons.push('\\widehat{' + stor.leslettres[stor.laformule.noms.angle[i][0]] + stor.leslettres[stor.laformule.noms.angle[i][1]] + stor.leslettres[stor.laformule.noms.angle[i][2]] + '}')
        stor.lesmesures.push(j3pGetRandomInt(10, 170))
        if (ds.CalculMental) {
          stor.lesmesures[stor.lesmesures.length - 1] = j3pArrondi(360 / (j3pGetRandomInt(1, 5)), 0)
        }
        const zzk = stor.leslettres[stor.laformule.noms.angle[i][0]]
        const zzl = stor.leslettres[stor.laformule.noms.angle[i][1]]
        const zzp = stor.leslettres[stor.laformule.noms.angle[i][2]]
        stor.lesboutonsfonc.push(function () {
          stor.zonem.majaffiche('\\widehat{' + zzk + zzl + zzp + '}')
          setTimeout(function () { stor.zonem.focus() }, 10)
        })
        stor.quoi.push('z')
        stor.lesunite.push('°')
      }
      if (stor.laformule.f.indexOf('diamètre') !== -1) {
        const i = j3pGetRandomInt(0, (stor.laformule.noms.diametre.length - 1))
        stor.lesboutons.push(stor.leslettres[stor.laformule.noms.diametre[i][0]] + stor.leslettres[stor.laformule.noms.diametre[i][1]])
        stor.lesmesures.push(j3pArrondi((j3pGetRandomInt(25, 81) * 10 + j3pGetRandomInt(1, 9)) / 10, 2))
        if (ds.CalculMental) {
          stor.lesmesures[stor.lesmesures.length - 1] = j3pGetRandomInt(5, 9)
        }
        const fik = stor.leslettres[stor.laformule.noms.diametre[i][0]]
        const fuk = stor.leslettres[stor.laformule.noms.diametre[i][1]]
        stor.lesboutonsfonc.push(function () {
          stor.zonem.majaffiche(fik + fuk)
          setTimeout(function () { stor.zonem.focus() }, 10)
        })
        stor.quoi.push('g')
        stor.lesunite.push(stor.unitebase)
      }
      if ((stor.laformule.f.indexOf('longueur') !== -1) && (stor.laformule.f.indexOf('longueurs') === -1)) {
        const i = j3pGetRandomInt(0, (stor.laformule.noms.longueur.length - 1))
        stor.lesboutons.push(stor.leslettres[stor.laformule.noms.longueur[i][0]] + stor.leslettres[stor.laformule.noms.longueur[i][1]])
        stor.lesmesures.push(j3pArrondi((j3pGetRandomInt(35, 49) * 10 + j3pGetRandomInt(1, 9)) / 10, 2))
        if (ds.CalculMental) {
          stor.lesmesures[stor.lesmesures.length - 1] = j3pGetRandomInt(3, 6)
        }
        const hik = stor.leslettres[stor.laformule.noms.longueur[i][0]]
        const huk = stor.leslettres[stor.laformule.noms.longueur[i][1]]
        stor.lesboutonsfonc.push(function () {
          stor.zonem.majaffiche(hik + huk)
          setTimeout(function () { stor.zonem.focus() }, 10)
        })
        stor.quoi.push('l')
        stor.lesunite.push(stor.unitebase)
      }
      if (stor.laformule.f.indexOf('largeur') !== -1) {
        const i = j3pGetRandomInt(0, (stor.laformule.noms.largeur.length - 1))
        stor.lesboutons.push(stor.leslettres[stor.laformule.noms.largeur[i][0]] + stor.leslettres[stor.laformule.noms.largeur[i][1]])
        stor.lesmesures.push(j3pArrondi((j3pGetRandomInt(11, 34) * 10 + j3pGetRandomInt(1, 9)) / 10, 2))
        if (ds.CalculMental) {
          switch (j3pGetRandomInt(0, 3)) {
            case 0: stor.lesmesures = [3, 4]
              break
            case 1: stor.lesmesures = [6, 8]
              break
            case 2: stor.lesmesures = [5, 12]
              break
            case 3: stor.lesmesures = [9, 12]
              break
          }
        }
        const kik = stor.leslettres[stor.laformule.noms.largeur[i][0]]
        const kuk = stor.leslettres[stor.laformule.noms.largeur[i][1]]
        stor.lesboutonsfonc.push(function () {
          stor.zonem.majaffiche(kik + kuk)
          setTimeout(function () { stor.zonem.focus() }, 10)
        })
        stor.quoi.push('v')
        stor.lesunite.push(stor.unitebase)
      }
      if (stor.laformule.f.indexOf('hauteur') !== -1) {
        const i = j3pGetRandomInt(0, (stor.laformule.noms.hauteur.length - 1))
        stor.lesboutons.push(stor.leslettres[stor.laformule.noms.hauteur[i][0]] + stor.leslettres[stor.laformule.noms.hauteur[i][1]])
        stor.lesmesures.push(j3pArrondi((j3pGetRandomInt(5, 14) * 3 * 10 + j3pGetRandomInt(1, 9)) / 10, 2))
        if (ds.CalculMental) {
          stor.lesmesures[stor.lesmesures.length - 1] = 6
        }
        const lik = stor.leslettres[stor.laformule.noms.hauteur[i][0]]
        const luk = stor.leslettres[stor.laformule.noms.hauteur[i][1]]
        stor.lesboutonsfonc.push(function () {
          stor.zonem.majaffiche(lik + luk)
          setTimeout(function () { stor.zonem.focus() }, 10)
        })
        stor.quoi.push('h')
        stor.lesunite.push(stor.unitebase)
      }
      if ((stor.laformule.f.indexOf('base') !== -1) && (stor.laformule.f.indexOf('aire') === -1) && (stor.laformule.f.indexOf('périmètre') === -1)) {
        const i = j3pGetRandomInt(0, stor.laformule.noms.base.length - 1)
        stor.lesboutons.push(stor.leslettres[stor.laformule.noms.base[i][0]] + stor.leslettres[stor.laformule.noms.base[i][1]])

        if (stor.laformule.figure === 'triangle') {
          stor.lesmesures.push(j3pArrondi((j3pGetRandomInt(j3pArrondi(stor.lesmesures[0] + 2, 0), 50) * 10 + j3pGetRandomInt(1, 9)) / 10, 1))
        } else if (stor.laformule.figure === 'losange') {
          stor.lesmesures.push(j3pArrondi((j3pGetRandomInt(j3pArrondi(stor.lesmesures[0] + 2, 0), 50) * 10 + j3pGetRandomInt(1, 9)) / 10, 1))
        } else if (stor.laformule.figure === 'triangle équilatéral') {
          stor.lesmesures.push(j3pArrondi(stor.lesmesures[0] * 2 / Math.sqrt(3), 1))
        } else {
          stor.lesmesures.push(j3pArrondi((j3pGetRandomInt(15, 45) * 10 + j3pGetRandomInt(1, 9)) / 10, 2))
        }
        if (ds.CalculMental) {
          stor.lesmesures[stor.lesmesures.length - 1] = j3pArrondi(stor.lesmesures[stor.lesmesures.length - 1], 0)
        }
        const sik = stor.leslettres[stor.laformule.noms.base[i][0]]
        const suk = stor.leslettres[stor.laformule.noms.base[i][1]]
        stor.lesboutonsfonc.push(function () {
          stor.zonem.majaffiche(sik + suk)
          setTimeout(function () { stor.zonem.focus() }, 10)
        })
        stor.quoi.push('b')
        stor.lesunite.push(stor.unitebase)
      }
      if ((stor.laformule.f.indexOf('base') !== -1) && (stor.laformule.f.indexOf('aire') !== -1)) {
        stor.lesboutons.push('A_{base}')
        stor.lesmesures.push(j3pArrondi((j3pGetRandomInt(15, 89) * 10 + j3pGetRandomInt(1, 9)) / 10, 2))
        if (ds.CalculMental) {
          stor.lesmesures[stor.lesmesures.length - 1] = j3pGetRandomInt(1, 5)
        }
        stor.lesboutonsfonc.push(function () {
          stor.zonem.majaffiche('A_{base}')
          setTimeout(function () { stor.zonem.focus() }, 10)
        })
        stor.quoi.push('u')
        stor.lesunite.push(stor.unitebase + '²')
      }
      if ((stor.laformule.f.indexOf('base') !== -1) && (stor.laformule.f.indexOf('périmètre') !== -1)) {
        stor.lesboutons.push('P_{base}')
        stor.lesmesures.push(j3pArrondi((j3pGetRandomInt(15, 89) * 10 + j3pGetRandomInt(1, 9)) / 10, 2))
        if (ds.CalculMental) {
          stor.lesmesures[stor.lesmesures.length - 1] = j3pGetRandomInt(2, 9)
        }
        stor.lesboutonsfonc.push(function () {
          stor.zonem.majaffiche('P_{base}')
          setTimeout(function () { stor.zonem.focus() }, 10)
        })
        stor.quoi.push('w')
        stor.lesunite.push(stor.unitebase)
      }
      if (stor.laformule.f.indexOf('\\pi') !== -1) {
        stor.lesboutons.push('π')
        stor.lesboutonsfonc.push(function () {
          stor.zonem.majaffiche('π')
          setTimeout(function () { stor.zonem.focus() }, 10)
        })
        stor.lesmesures.push('')
        stor.quoi.push('π')
        stor.lesunite.push(stor.unitebase)
      }
      if (stor.laformule.f.indexOf('^{2}') !== -1) {
        stor.lesboutons.push('x^{2}')
        stor.lesboutonsfonc.push(function () {
          stor.zonem.majaffiche('{}^2')
          setTimeout(function () { stor.zonem.focus() }, 10)
        })
        stor.lesmesures.push('')
        stor.quoi.push('^{2}')
        stor.lesunite.push(stor.unitebase)
      }
      if (stor.laformule.f.indexOf('^{3}') !== -1) {
        stor.lesboutons.push('x^{3}')
        stor.lesboutonsfonc.push(function () {
          stor.zonem.majaffiche('{}^3')
          setTimeout(function () { stor.zonem.focus() }, 10)
        })
        stor.lesmesures.push('')
        stor.quoi.push('^{3}')
        stor.lesunite.push(stor.unitebase)
      }

      /// rajoute un bouton faux
      if (((stor.laformule.figure === 'triangle') && (stor.laformule.f === '\\frac{ \\text{base} \\times \\text{hauteur}}{2}')) ||
        ((stor.laformule.figure === 'triangle quelconque') && (stor.laformule.f === '\\text{somme des longueurs des côtés}'))) {
        const i = j3pGetRandomInt(0, (stor.laformule.noms.fauxspec.length - 1))
        stor.lesboutons.push(stor.leslettres[stor.laformule.noms.fauxspec[i][0]] + stor.leslettres[stor.laformule.noms.fauxspec[i][1]])
        if (stor.laformule.figure === 'triangle quelconque') {
          stor.lesmesures.push(j3pArrondi((j3pGetRandomInt(11, j3pArrondi(stor.lesmesures[0] - 2, 0)) * 10 + j3pGetRandomInt(1, 9)) / 10, 1))
          if (ds.CalculMental) {
            stor.lesmesures[stor.lesmesures.length - 1] = j3pArrondi((j3pGetRandomInt(11, j3pArrondi(stor.lesmesures[0] - 2, 0)) * 10 + j3pGetRandomInt(1, 9)) / 10, 0)
          }
        }
        if (stor.laformule.figure === 'triangle') {
          stor.lesmesures.push(j3pArrondi((j3pGetRandomInt(
            j3pArrondi(stor.lesmesures[0] + 2, 0),
            j3pArrondi(stor.lesmesures[1] - 2, 0)
          ) * 10 + j3pGetRandomInt(1, 9)) / 10, 1))
          if (ds.CalculMental) {
            stor.lesmesures[stor.lesmesures.length - 1] = j3pArrondi((j3pGetRandomInt(j3pArrondi(stor.lesmesures[0] + 2, 0), j3pArrondi(stor.lesmesures[1] - 2, 0)) * 10 + j3pGetRandomInt(1, 9)) / 10, 0)
          }
        }
        const mik = stor.leslettres[stor.laformule.noms.fauxspec[i][0]]
        const muk = stor.leslettres[stor.laformule.noms.fauxspec[i][1]]
        stor.lesboutonsfonc.push(function () {
          stor.zonem.majaffiche(mik + muk)
          setTimeout(function () { stor.zonem.focus() }, 10)
        })
        stor.quoi.push('j')
        stor.lesunite.push(stor.unitebase)
      } else {
        const i = j3pGetRandomInt(0, (stor.laformule.noms.faux.length - 1))
        stor.lesboutons.push(stor.leslettres[stor.laformule.noms.faux[i][0]] + stor.leslettres[stor.laformule.noms.faux[i][1]])

        if (stor.laformule.figure === 'figure') {
          stor.lesmesures.push(j3pArrondi((j3pGetRandomInt(15, 30) * 10 + j3pGetRandomInt(1, 9)) / 10, 1))
        }
        if (stor.laformule.figure === 'carré') {
          stor.lesmesures.push(j3pArrondi(Math.sqrt(2) * stor.lesmesures[0], 1))
        }
        if (stor.laformule.figure === 'cercle') {
          stor.lesmesures.push(j3pArrondi((j3pGetRandomInt(2, j3pArrondi(stor.lesmesures[0] - 1, 0)) * 10 + j3pGetRandomInt(1, 4)) / 10, 1))
        }
        if (stor.laformule.figure === 'rectangle') {
          stor.lesmesures.push(j3pArrondi(Math.sqrt(stor.lesmesures[0] * stor.lesmesures[0] + stor.lesmesures[1] * stor.lesmesures[1]), 1))
        }
        if (stor.laformule.figure === 'losange') {
          stor.lesmesures.push(j3pArrondi((j3pGetRandomInt(2, j3pArrondi(stor.lesmesures[0] - 1, 0)) * 10 + j3pGetRandomInt(1, 4)) / 10, 1))
        }
        if (stor.laformule.figure === 'parallélogramme') {
          stor.lesmesures.push(j3pArrondi((j3pGetRandomInt(15, 30) * 10 + j3pGetRandomInt(1, 9)) / 10, 1))
        }
        if (stor.laformule.figure === 'triangle') {
          stor.lesmesures.push(j3pArrondi(Math.sqrt(stor.lesmesures[0] * stor.lesmesures[0] + stor.lesmesures[1] * stor.lesmesures[1]), 1))
        }
        if (stor.laformule.figure === 'triangle quelconque') {
          stor.lesmesures.push(j3pArrondi((j3pGetRandomInt(2, j3pArrondi(Math.min(stor.lesmesures[0], stor.lesmesures[1]) - 1, 0)) * 10 + j3pGetRandomInt(1, 4)) / 10, 1))
        }
        if (stor.laformule.figure === 'triangle équilatéral') {
          stor.lesmesures.push(j3pArrondi((j3pGetRandomInt(2, j3pArrondi(stor.lesmesures[0] - 1, 0)) * 10 + j3pGetRandomInt(1, 4)) / 10, 1))
        }
        if (stor.laformule.figure === 'cube') {
          stor.lesmesures.push(j3pArrondi(Math.sqrt(3) * stor.lesmesures[0], 1))
        }
        if (stor.laformule.figure === 'parallélépipède rectangle') {
          stor.lesmesures.push(j3pArrondi(Math.sqrt(stor.lesmesures[0] * stor.lesmesures[0] + stor.lesmesures[1] * stor.lesmesures[1] + stor.lesmesures[2] * stor.lesmesures[2]), 1))
        }
        if (stor.laformule.figure === 'secteur de disque') {
          stor.lesmesures.push(j3pArrondi(2 * stor.lesmesures[0] * Math.sin(stor.lesmesures[1] * Math.PI / 180), 1))
        }
        if (stor.laformule.figure === 'cylindre') {
          if (stor.quoi.indexOf('h') !== -1) {
            stor.lesmesures.push(j3pArrondi(Math.sqrt(4 * stor.lesmesures[0] * stor.lesmesures[0] + stor.lesmesures[1] * stor.lesmesures[1]), 1))
          } else {
            stor.lesmesures.push(j3pArrondi((j3pGetRandomInt(15, 30) * 10 + j3pGetRandomInt(1, 9)) / 10, 1))
          }
        }
        if (stor.laformule.figure === 'cône de révolution') {
          if (stor.quoi.indexOf('h') !== -1) {
            stor.lesmesures.push(j3pArrondi(Math.sqrt(stor.lesmesures[0] * stor.lesmesures[0] + stor.lesmesures[1] * stor.lesmesures[1]), 1))
          } else {
            stor.lesmesures.push(j3pArrondi((j3pGetRandomInt(15, 30) * 10 + j3pGetRandomInt(1, 9)) / 10, 1))
          }
        }
        if (stor.laformule.figure === 'sphère') {
          stor.lesmesures.push(j3pArrondi((j3pGetRandomInt(2, j3pArrondi(2 * stor.lesmesures[0] - 2, 0)) * 10 + j3pGetRandomInt(1, 4)) / 10, 1))
        }
        if (stor.laformule.figure === 'pyramide') {
          stor.lesmesures.push(j3pArrondi((j3pGetRandomInt(j3pArrondi(stor.lesmesures[0] + 2, 0), 98) * 10 + j3pGetRandomInt(1, 4)) / 10, 1))
        }
        if (stor.laformule.figure === 'prisme droit') {
          stor.lesmesures.push(j3pArrondi((j3pGetRandomInt(j3pArrondi(stor.lesmesures[0] + 2, 0), 98) * 10 + j3pGetRandomInt(1, 4)) / 10, 1))
        }

        if (ds.CalculMental) {
          stor.lesmesures[stor.lesmesures.length - 1] = j3pArrondi(stor.lesmesures[stor.lesmesures.length - 1], 0)
        }
        const mik = stor.leslettres[stor.laformule.noms.faux[i][0]]
        const muk = stor.leslettres[stor.laformule.noms.faux[i][1]]
        stor.lesboutonsfonc.push(function () {
          stor.zonem.majaffiche(mik + muk)
          setTimeout(function () { stor.zonem.focus() }, 10)
        })
        stor.quoi.push('j')
        stor.lesunite.push(stor.unitebase)
      }

      shuffle(20)
      igarde = -1
      for (let i = 0; i < stor.lesboutons.length; i++) {
        if (stor.lesboutons[i].indexOf('\\w') !== -1) {
          igarde = i
          break
        }
      }
      if (igarde !== -1) {
        let azerty = stor.lesmesures[igarde]
        stor.lesmesures.splice(igarde, 1)
        stor.lesmesures.splice(0, 0, azerty)

        azerty = stor.lesboutons[igarde]
        stor.lesboutons.splice(igarde, 1)
        stor.lesboutons.splice(0, 0, azerty)

        azerty = stor.lesboutonsfonc[igarde]
        stor.lesboutonsfonc.splice(igarde, 1)
        stor.lesboutonsfonc.splice(0, 0, azerty)

        azerty = stor.quoi[igarde]
        stor.quoi.splice(igarde, 1)
        stor.quoi.splice(0, 0, azerty)

        azerty = stor.lesunite[igarde]
        stor.lesunite.splice(igarde, 1)
        stor.lesunite.splice(0, 0, azerty)
      }

      const mona = stor.laformule.grandeur
      let monb = textes.genre[stor.laformule.genre]
      let monc = stor.laformule.figure
      const mond = stor.lenom
      const mone = stor.ajout
      const monf = stor.uniterep
      if ((monc === 'sphère') && (mona === 'le volume')) {
        monc = 'boule'
      }
      if ((monc === 'cercle') && (mona === 'l’aire')) {
        monc = 'disque'
        monb = 'du'
      }
      stor.lesdiv.titre.style.whiteSpace = 'normal'
      j3pAffiche(stor.lesdiv.titre, null, textes.ConsigneGtex,
        {
          a: mona,
          b: monb,
          c: monc,
          d: mond,
          e: mone,
          f: monf
        })
      let ff = false
      for (let i = 0; i < stor.lesmesures.length; i++) {
        if (stor.lesmesures[i] !== '') {
          monb = stor.lesmesures[i]
          monc = stor.lesunite[i]
          if (ds.ChangementUnite && (!ff) && (monc !== '°')) {
            ff = true
            let unitebis
            do {
              unitebis = j3pGetRandomInt(0, 6)
            } while (unitebis === stor.unitei)
            if (stor.lesunite[i][stor.lesunite[i].length - 1] === '²') {
              unitebis = stor.unitei + 1
              if (j3pGetRandomBool()) unitebis = stor.unitei - 1
              if (unitebis === 7) unitebis = 5
              if (unitebis === -1) unitebis = 1
            }
            monc = textes.unites[unitebis]
            let mo = 1
            if (stor.lesunite[i][stor.lesunite[i].length - 1] === '²') {
              mo = 2
              monc += '²'
            }
            let appo = 0
            if (stor.unitei > unitebis) {
              appo = (stor.unitei - unitebis) * mo + mo
            }
            monb = j3pArrondi(stor.lesmesures[i] * Math.pow(10, (unitebis - stor.unitei) * mo), appo)
          }
          // monb = (''+monb).replace('.',',')
          monb = ecrisbienMAthquill(monb)
          j3pAffiche(stor.lesdiv.consigne, null, textes.ConsigneTex + '\n',
            {
              a: stor.lesboutons[i],
              b: monb,
              c: monc
            })
        }
      }
      fig()
      stor.Gbuf = ''
      switch (stor.laformule.g) {
        case 0 :
          stor.Gbuf = 'Périmètre'
          break
        case 1 :
          stor.Gbuf = 'Aire'
          break
        case 2 :
          stor.Gbuf = 'Aire latérale'
          break
        case 3 :
          stor.Gbuf = 'Volume'
          break
      }
      stor.dedebuf = textes.debut
      if (ds.NotationGrandeur.indexOf('Fonction') !== -1) {
        if (stor.lenom[0] !== '(') {
          stor.dedebuf = textes.debut1
        } else {
          stor.dedebuf = textes.debut2
        }
      }
      if (ds.NotationGrandeur.indexOf('Court') !== -1) {
        stor.Gbuf = stor.Gbuf[0]
        if (stor.laformule.g === 2) {
          stor.Gbuf = 'Al'
        }
      }
      const kkl = addDefaultTable(stor.lesdiv.l4ligne[0][0], 2, 3)
      modif(kkl)
      stor.ddvBout = kkl[0][1]
      j3pAffiche(kkl[1][0], null, stor.dedebuf,
        {
          a: stor.Gbuf,
          b: stor.lenom
        })
      const hfkdj = addDefaultTable(kkl[1][1], 1, 3)
      modif(hfkdj)
      stor.zonem = new ZoneStyleMathquill2(hfkdj[0][0], {
        id: 'mazone',
        limite: 17,
        limitenb: 4,
        restric: '0123456789/+-*²p()',
        enter: me.sectionCourante.bind(me),
        hasAutoKeyboard: true,
        inverse: true
      })
      stor.oldRep = kkl[1][1]
      stor.bout = []
      stor.lesdiv.aide1 = kkl[1][2]
      stor.ddvBout2 = hfkdj[0][2]
      const cocop = j3pClone(stor.lesboutons)
      for (let i = 0; i < cocop.length; i++) {
        cocop[i] = '$' + cocop[i] + '$'
      }
      let mafoncfrac
      if (!ds.NotationExpert) {
        mafoncfrac = function () {
          stor.zonem.majaffiche('\\div')
          setTimeout(function () { stor.zonem.focus() }, 10)
        }
        cocop.push('$\\div$')
        stor.lesboutonsfonc.push(mafoncfrac)
      } else {
        hfkdj[0][1].style.width = '20px'
        stor.fffunc = function () {
          stor.zonem.majaffiche('/')
          setTimeout(function () { stor.zonem.focus() }, 10)
        }
        stor.unbotM = new BoutonStyleMathquill(hfkdj[0][2], 'bouts', ['$\\frac{a}{b}$'], [stor.fffunc], { mathquill: false })
      }
      stor.bout[stor.bout.length] = new BoutonStyleMathquill(stor.ddvBout, 'bouts', cocop, stor.lesboutonsfonc, { mathquill: false })
      stor.fofo = false

      if ((ds.ChangementUnite) && (ds.TableauxConversion)) {
        stor.dialog = new Dialog({ title: 'Tableau de Conversion', width: '500px' })
        stor.zoneTableau = addDefaultTable(stor.dialog.container, 2, 1)
        modif(stor.zoneTableau)
        j3pAjouteBouton(stor.lesdiv.convers, 'TableauxBOUBOUT', 'MepBoutons', 'Tableaux', affAide)

        stor.lestab = ['m', 'm²', 'm³']
        stor.numtab = j3pGetRandomInt(0, 2)
        const monut = stor.lestab[stor.numtab][0]
        j3pAjouteBouton(stor.zoneTableau[1][0], 'unboutchange', 'MepBoutons', 'Changer de tableau', changetab)
        stor.tablo = new TableauConversionMobile(stor.zoneTableau[0][0], {
          unite: monut,
          are: false,
          litre: false,
          tonne: false,
          virgule: true,
          fleches: false,
          clavier: true,
          mepact: getJ3pConteneur(me.zonesElts.MG)
        })
        j3pStyle(stor.zoneTableau[0][0], { fontSize: '120%' })
      }
    }

    stor.lesdiv.explications.classList.remove('explique')
    stor.lesdiv.solution.classList.remove('correction')
    j3pEmpty(stor.lesdiv.explications)
    j3pEmpty(stor.lesdiv.solution)
    j3pEmpty(stor.lesdiv.correction)

    if ((me.questionCourante % ds.nbetapes) === 2) {
      if (stor.foco) {
        j3pEmpty(stor.oldRep)
        stor.oldRep.style.color = me.styles.petit.correction.color
        let mona = stor.repeleve
        for (let i = 0; i < stor.lesboutons.length; i++) {
          if ((stor.quoi[i] !== 'π') && (stor.quoi[i] !== '^{3}') && (stor.quoi[i] !== '^{2}')) {
            const reg = new RegExp(stor.quoi[i], 'g')
            mona = mona.replace(reg, stor.lesboutons[i])
          }
        }
        mona = mona.replace('x^{2}', ' ^{2}')
        mona = mona.replace('x^{3}', ' ^{3}')
        mona = mona.replace('/', ' \\div ')
        j3pAffiche(stor.oldRep, null, '$' + mona + '$')
      }
      j3pEmpty(stor.lesdiv.etape)
      j3pAffiche(stor.lesdiv.etape, null, textes.Etape2)
      const kkl = addDefaultTable(stor.lesdiv.l4ligne[1][0], 1, 3)
      modif(kkl)
      j3pAffiche(kkl[0][0], null, stor.dedebuf,
        { a: stor.Gbuf, b: stor.lenom })
      let mona = stor.repeleve
      stor.repeleveold = stor.repeleve
      let cpt = 0
      stor.repatt = []
      const tabzone = []
      for (let i = 0; i < stor.lesboutons.length; i++) {
        if ((stor.quoi[i] !== 'π') && (stor.quoi[i] !== '^{2}') && (stor.quoi[i] !== '^{3}')) {
          while (mona.indexOf(stor.quoi[i]) !== -1) {
            const tt = caracspe[cpt]
            mona = mona.replace(stor.quoi[i], tt)
            stor.repatt[cpt] = stor.lesmesures[i]
            tabzone[cpt] = { type: 'zsm3', symb: tt, nom: 'zonerepA' + i + 'A', params: { restric: '0123456789,.', enter: me.sectionCourante.bind(me), limitenb: 1, limite: 6 } }
            cpt++
          }
        }
      }

      stor.monagarde = mona
      mona = mona.replace('/', ' \\div ')
      stor.oldRep = kkl[0][1]
      stor.zoneStyleAffiche = new ZoneStyleAffiche(kkl[0][1], '$' + mona + '$', tabzone)
      j3pAffiche(kkl[0][2], null, '&nbsp;' + stor.uniterep)
      stor.foco = false
    }
    if (((me.questionCourante % ds.nbetapes) === 3) || (((me.questionCourante % ds.nbetapes) === 0) && ((!ds.ValeurApprochee2) || (ds.CalculMental)))) {
      let mona = stor.monagarde
      mona = mona.replace('/', ' \\div ')
      for (let i = 0; i < stor.zoneStyleAffiche.liste.length; i++) {
        mona = mona.replace(caracspe[i], stor.repatt[i])
      }
      if (stor.foco) {
        j3pEmpty(stor.oldRep)
        stor.oldRep.style.color = me.styles.petit.correction.color
        j3pAffiche(stor.oldRep, null, '$' + mona + '$')
      }
      j3pEmpty(stor.lesdiv.etape)
      j3pAffiche(stor.lesdiv.etape, null, textes.Etape3)
      stor.tabWhe = addDefaultTable(stor.lesdiv.l4ligne[3][0], 1, 5)
      modif(stor.tabWhe)
      j3pAffiche(stor.tabWhe[0][0], null, stor.dedebuf,
        { a: stor.Gbuf, b: stor.lenom })
      let buf = ''
      stor.usepi = false
      if (stor.laformule.f.indexOf('pi') !== -1) {
        buf = '$ \\times \\pi $  '
        stor.usepi = true
      }

      let cpt = 0
      while ((mona.indexOf('π') !== -1) && (cpt < 1000)) {
        mona = mona.replace('π', '1')
        cpt++
      }

      mona = mona.replace(/\\div/g, '/')

      stor.resultatt = j3pMathquillXcas(mona)
      while (stor.resultatt.indexOf(')1') !== -1) {
        stor.resultatt = stor.resultatt.replace(')1', ')*1')
      }

      stor.resultatt = extrait(moncalc(stor.resultatt))

      stor.repfrac = !stor.resultatt.exacte

      if (!stor.resultatt.exacte) {
        stor.lafrac = '\\frac{' + stor.resultatt.num + '}{' + stor.resultatt.den + '}'
      }

      if (ds.NotationExpert) {
        stor.tabWhe[0][3].style.width = '20px'
        stor.fffunc = function () {
          stor.zonem.majaffiche('/')
          setTimeout(function () { stor.zonem.focus() }, 10)
        }
        stor.unbotM = new BoutonStyleMathquill(stor.tabWhe[0][4], 'bouts', ['$\\frac{a}{b}$'], [stor.fffunc], { mathquill: false })
      }
      stor.ddvBout2 = stor.tabWhe[0][4]

      stor.resultatt = j3pArrondi(stor.resultatt.decimale, 5)
      stor.oldRep = stor.tabWhe[0][1]
      stor.foco = false
      stor.zonem = new ZoneStyleMathquill2(stor.tabWhe[0][1], {
        id: 'mazone2',
        limite: 17,
        limitenb: 4,
        restric: '0123456789.,/-',
        clavier: '0123456789,/-',
        enter: me.sectionCourante.bind(me),
        hasAutoKeyboard: true,
        inverse: true
      })
      j3pAffiche(stor.tabWhe[0][2], null, '&nbsp;' + buf + ' ' + stor.uniterep + textes.Vexacte)
    }
    if (((me.questionCourante % ds.nbetapes) === 0) && (ds.nbetapes === 4)) {
      stor.resultattold = stor.resultatt
      if (stor.foco) {
        j3pEmpty(stor.oldRep)
        stor.oldRep.style.color = me.styles.petit.correction.color
        let uneco = stor.resultattold
        if (stor.repfrac) { uneco = stor.lafrac }
        j3pAffiche(stor.oldRep, null, '$' + uneco + '$',
          { a: stor.Gbuf, b: stor.lenom })
      }
      j3pEmpty(stor.lesdiv.etape)
      j3pAffiche(stor.lesdiv.etape, null, textes.Etape4[stor.tvp])
      const kkl = addDefaultTable(stor.lesdiv.l4ligne[3][0], 1, 3)
      modif(kkl)
      j3pAffiche(kkl[0][0], null, stor.dedebuf.replace('=', '\\approx'),
        { a: stor.Gbuf, b: stor.lenom })

      if (stor.usepi) { stor.resultatt = stor.resultatt * Math.PI }
      // test si resultat tombe juste
      const tombejuste = function (val, arrondbase) {
        while ((Math.abs(val - (Math.trunc(val * Math.pow(10, arrondbase)) / Math.pow(10, arrondbase)) < Math.pow(10, -12))) && (arrondbase > -2)) {
          arrondbase = arrondbase - 1
        }
        return arrondbase
      }

      stor.numarrond = tombejuste(stor.resultatt, j3pGetRandomInt(0, 3))
      const resultatt1 = j3pArrondi(Math.trunc(stor.resultatt * Math.pow(10, stor.numarrond)) / Math.pow(10, stor.numarrond), stor.numarrond)
      const resultatt2 = j3pArrondi((Math.trunc(stor.resultatt * Math.pow(10, stor.numarrond)) + 1) / Math.pow(10, stor.numarrond), stor.numarrond)

      switch (stor.parquoi) {
        case 0:
          stor.resultatt = resultatt1
          stor.resultattinv = resultatt2
          break
        case 1:
          stor.resultatt = resultatt2
          stor.resultattinv = resultatt1
          break
        case 2:
          if (Math.abs(resultatt1 - stor.resultatt) < Math.abs(resultatt2 - stor.resultatt)) {
            stor.resultatt = resultatt1
            stor.resultattinv = resultatt2
          } else {
            stor.resultatt = resultatt2
            stor.resultattinv = resultatt1
          }
          break
        case 3:
          stor.resultatt = resultatt2
          stor.resultattinv = resultatt1
      }

      stor.zonem = new ZoneStyleMathquill2(kkl[0][1], {
        id: 'mazone3',
        limite: 17,
        limitenb: 4,
        restric: '0123456789.,',
        enter: me.sectionCourante.bind(me),
        inverse: true
      })
      j3pAffiche(kkl[0][2], null, '&nbsp;&nbsp;' + stor.uniterep + '&nbsp;' + stor.PhraseVal.replace('£a', textes.Arrond[stor.numarrond + 1]).replace('£b', textes.parquoi[stor.parquoi]))
    }

    me.zonesElts.MG.classList.add('fond')

    // Obligatoire
    me.finEnonce()
  }

  function mettoutvert () {
    if ((me.questionCourante % ds.nbetapes) === 1) {
      stor.zonem.corrige(true)
    }
    if ((me.questionCourante % ds.nbetapes) === 2) {
      stor.zoneStyleAffiche.corrige(true)
    }
    if (((me.questionCourante % ds.nbetapes) === 3) || (((me.questionCourante % ds.nbetapes) === 0) && ((!ds.ValeurApprochee2) || (ds.CalculMental)))) {
      stor.zonem.corrige(true)
    }
    if (((me.questionCourante % ds.nbetapes) === 0) && (ds.nbetapes === 4)) {
      stor.zonem.corrige(true)
    }
  }

  function disatout () {
    if ((me.questionCourante % ds.nbetapes) === 1) {
      j3pEmpty(stor.ddvBout)
      j3pEmpty(stor.ddvBout2)
      stor.zonem.disable()
    }
    if ((me.questionCourante % ds.nbetapes) === 2) {
      stor.zoneStyleAffiche.disable()
    }
    if (((me.questionCourante % ds.nbetapes) === 3) || (((me.questionCourante % ds.nbetapes) === 0) && ((!ds.ValeurApprochee2) || (ds.CalculMental)))) {
      j3pEmpty(stor.ddvBout2)
      stor.zonem.disable()
    }
    if (((me.questionCourante % ds.nbetapes) === 0) && (ds.nbetapes === 4)) {
      stor.zonem.disable()
    }
  }

  function afcofo () {
    /// ////////////////////////////////
    if ((me.questionCourante % ds.nbetapes) === 1) {
      let mona = stor.laformule.grandeur + textes.genre2[stor.laformule.genre] + stor.laformule.figure
      if (stor.laformule.figure2 !== undefined) mona = stor.laformule.grandeur + textes.genre2[stor.laformule.genre] + stor.laformule.figure2
      if ((stor.laformule.figure === 'sphère') && (stor.laformule.grandeur === 'le volume')) { mona = 'le volume d’une boule' }
      const monb = stor.laformule.f
      j3pAffiche(stor.lesdiv.explications, null, textes.CoFo1,
        {
          a: mona,
          b: monb
        })

      j3pAffiche(stor.lesdiv.solution, null, textes.donc + stor.dedebuf,
        { a: stor.Gbuf, b: stor.lenom })
      mona = transformule2(stor.laformule.f)
      stor.repeleve = mona
      let cpt = 0
      for (let i = 0; i < stor.lesboutons.length; i++) {
        if ((stor.quoi[i] !== '^{2}') && (stor.quoi[i] !== '^{3}')) {
          do {
            mona = mona.replace(stor.quoi[i], stor.lesboutons[i])
            cpt++
          }
          while ((mona.indexOf(stor.quoi[i]) !== -1) && (cpt < 100))
        }
      }
      mona = mona.replace('x^{2}', ' ^{2}')
      mona = mona.replace('x^{3}', ' ^{3}')
      mona = mona.replace('/', ' \\div ')
      j3pAffiche(stor.lesdiv.solution, null, textes.CoFo2,
        { a: mona })
      // faire un test pour voir si c’est une erreur de formule
      // ou pour voir si c’est une erreur d’element
      // si il met 2*pi*cote pour perim carre -> erreur formule
      // di il met 4*(autre chose que cote) -> erreur elem
      // sinon pas d’erreur reportee
      // {noms:{nom:'0123',cote:['01','12','23','30'],faux:['02','13']},figure:'carré',genre:1,nbpoints:4,perimetre:[{f:'4 \\times \\text{côté}',n:'6e',o:false}],aire:[{f:'\\text{côté}^{2}',n:'6e',o:false}],volume:[],airelaterale:[]},

      const burp = function (acomp) {
        acomp = acomp.replace(/u/g, 'w')
        acomp = acomp.replace(/b/g, 'w')
        acomp = acomp.replace(/o/g, 'w')
        acomp = acomp.replace(/p/g, 'w')
        acomp = acomp.replace(/x/g, 'w')
        acomp = acomp.replace(/y/g, 'w')
        acomp = acomp.replace(/O/g, 'w')
        acomp = acomp.replace(/U/g, 'w')
        acomp = acomp.replace(/k/g, 'w')
        acomp = acomp.replace(/n/g, 'w')
        acomp = acomp.replace(/d/g, 'w')
        acomp = acomp.replace(/q/g, 'w')
        acomp = acomp.replace(/g/g, 'w')
        acomp = acomp.replace(/l/g, 'w')
        acomp = acomp.replace(/v/g, 'w')
        acomp = acomp.replace(/h/g, 'w')
        acomp = acomp.replace(/z/g, 'w')
        return acomp
      }
      const burp2 = function (acomp) {
        acomp = burp(acomp)
        acomp = acomp.replace(/j/g, 'w')
        return acomp
      }
      stor.repeleve6 = stor.repeleve7
      stor.repeleve7 = burp(stor.repeleve7)
      const yaconfuseformule = []
      // mmm
      let acomp
      for (let i = 0; i < textes.formules02.length; i++) {
        for (let j = 0; j < textes.formules02[i].perimetre.length; j++) {
          if (!((stor.laformule.grandeur === 'le périmètre') && (stor.laformule.figure === textes.formules02[i].figure))) {
            acomp = burp(transformule(textes.formules02[i].perimetre[j].f))
            if (ressemble(acomp, stor.repeleve7)) {
              yaconfuseformule.push(['le périmètre d’un ' + textes.formules02[i].figure, textes.formules02[i].figure, 'le périmètre'])
            }
          }
        }

        for (let j = 0; j < textes.formules02[i].aire.length; j++) {
          if (!((stor.laformule.grandeur === 'l’aire') && (stor.laformule.figure === textes.formules02[i].figure))) {
            acomp = burp(transformule(textes.formules02[i].aire[j].f))
            if (ressemble(acomp, stor.repeleve7)) {
              yaconfuseformule.push(['l’aire d’un ' + textes.formules02[i].figure, textes.formules02[i].figure, 'l’aire'])
            }
          }
        }
        for (let j = 0; j < textes.formules02[i].volume.length; j++) {
          if (!((stor.laformule.grandeur === 'le volume') && (stor.laformule.figure === textes.formules02[i].figure))) {
            acomp = burp(transformule(textes.formules02[i].volume[j].f))

            if (ressemble(acomp, stor.repeleve7)) {
              yaconfuseformule.push(['le volume d’un ' + textes.formules02[i].figure, textes.formules02[i].figure, 'le volume'])
            }
          }
        }
        for (let j = 0; j < textes.formules02[i].airelaterale.length; j++) {
          if (!((stor.laformule.grandeur === 'l’aire latérale') && (stor.laformule.figure === textes.formules02[i].figure))) {
            acomp = transformule(textes.formules02[i].airelaterale[j].f)
            acomp = burp(acomp)

            if (ressemble(acomp, stor.repeleve7)) {
              yaconfuseformule.push(['l’aire latérale d’un ' + textes.formules02[i].figure, textes.formules02[i].figure, 'l’aire latérale'])
            }
          }
        }
      }
      if (yaconfuseformule.length > 0) {
        // det la meilleure
        // mm fig
        // sinon mm grandeur
        // sinon la premiere
        me.typederreurs[3]++
        let mmfiggrand = false
        let laf = ''
        for (let i = 0; i < yaconfuseformule.length; i++) {
          if (yaconfuseformule[i][1] === stor.laformule.figure) {
            laf = yaconfuseformule[i][0]
            mmfiggrand = true
            break
          }
        }
        if (!mmfiggrand) {
          for (let i = 0; i < yaconfuseformule.length; i++) {
            if (yaconfuseformule[i][2] === stor.laformule.grandeur) {
              laf = yaconfuseformule[i][0]
              mmfiggrand = true
              break
            }
          }
        }
        if (!mmfiggrand) { laf = yaconfuseformule[0][0] }
        const ch = 'Tu as peut-être confondu avec la formule\n qui donne ' + laf
        stor.aide1 = new BulleAide(stor.lesdiv.aide1, ch, { place: 1 })
      }
      if (yaconfuseformule.length < 1) {
        stor.repeleve7 = burp2(stor.repeleve7)
        acomp = burp(transformule(stor.laformule.f))
        if (ressemble(acomp, stor.repeleve7)) {
          me.typederreurs[8]++
        }
      }
      stor.yaco = true
      stor.zonem.corrige(false)
      stor.zonem.barre()
      stor.yaexplik = true
      stor.foco = true
    }
    if ((me.questionCourante % ds.nbetapes) === 2) {
      let errconv = false

      for (let i = 0; i < stor.zoneStyleAffiche.liste.length; i++) {
        const rpa = (stor.repatt[i] + '').replace('.', ',')
        if (rpa !== stor.repeleve[i]) {
          stor.zoneStyleAffiche.liste[i].corrige(false)
          stor.zoneStyleAffiche.liste[i].barre()
          errconv = (errconv || testconv(rpa, stor.repeleve[i]))
          stor.repeleve[i] = rpa
        } else { stor.zoneStyleAffiche.liste[i].corrige(true) }
      }
      if (errconv) {
        me.typederreurs[4]++
        stor.yaco = true
        j3pAffiche(stor.lesdiv.explications, null, textes.CoFoconv1)
      } else { me.typederreurs[6]++ }
      /// /
      const ttab = addDefaultTable(stor.lesdiv.solution, 1, 3)
      modif(ttab)
      j3pAffiche(ttab[0][0], null, stor.dedebuf,
        { a: stor.Gbuf, b: stor.lenom })
      j3pAffiche(ttab[0][2], null, '&nbsp;' + stor.uniterep)

      let mona = stor.repeleveold
      for (let i = 0; i < stor.lesboutons.length; i++) {
        if ((stor.quoi[i] !== 'π') && (stor.quoi[i] !== '^{3}') && (stor.quoi[i] !== '^{2}')) {
          const reg = new RegExp(stor.quoi[i], 'g')
          mona = mona.replace(reg, stor.lesmesures[i])
        }
      }
      mona = mona.replace('x^{2}', ' ^{2}')
      mona = mona.replace('x^{3}', ' ^{3}')
      mona = mona.replace('/', ' \\div ')
      j3pAffiche(ttab[0][1], null, '$' + mona + '$')
      stor.foco = true
      stor.yaco = true
    }
    if (((me.questionCourante % ds.nbetapes) === 3) || (((me.questionCourante % ds.nbetapes) === 0) && ((!ds.ValeurApprochee2) || (ds.CalculMental)))) {
      ///
      const ttab = addDefaultTable(stor.lesdiv.solution, 1, 3)
      modif(ttab)
      j3pAffiche(ttab[0][0], null, stor.dedebuf,
        { a: stor.Gbuf, b: stor.lenom })
      j3pAffiche(ttab[0][2], null, '&nbsp;' + stor.uniterep)
      let buf = ''
      if (stor.usepi) {
        buf = '$ \\times \\pi $  '
      }
      let uneco = ecrisbienMAthquill(stor.resultatt)
      if (stor.repfrac) { uneco = stor.lafrac }
      j3pAffiche(ttab[0][1], null, '$' + uneco + '$' + buf,
        { a: stor.Gbuf, b: stor.lenom })
      ///
      me.typederreurs[7]++
      stor.zonem.corrige(false)
      stor.zonem.barre()
      stor.yaco = true
      stor.foco = true
    }
    if (((me.questionCourante % ds.nbetapes) === 0) && (ds.nbetapes === 4)) {
      const ttab = addDefaultTable(stor.lesdiv.solution, 1, 3)
      modif(ttab)
      j3pAffiche(ttab[0][0], null, stor.dedebuf.replace('=', '\\approx'),
        { a: stor.Gbuf, b: stor.lenom })
      j3pAffiche(ttab[0][2], null, '&nbsp;' + stor.uniterep)

      const ttty = ecrisbienMAthquill(stor.resultatt)
      j3pAffiche(ttab[0][1], null, '$' + ttty + '$',
        { a: stor.Gbuf, b: stor.lenom })
      me.typederreurs[5]++
      stor.yaco = true
      if (Math.abs(stor.repeleve - stor.resultattinv) < Math.pow(10, -12)) {
        stor.yaexplik = true
        j3pAffiche(stor.lesdiv.explications, null, textes.CoFoQuoi1[stor.tvp][stor.parquoi])
      }
      stor.zonem.corrige(false)
      stor.zonem.barre()
    }
    disatout()
  }

  switch (this.etat) {
    case 'enonce':
      if (me.debutDeLaSection) {
        initSection()
      } else {
        enonceMain()
      }
      break // case "enonce":

    case 'correction':
      // On teste si une réponse a été saisie
      stor.lesdiv.explications.classList.remove('explique')
      stor.lesdiv.solution.classList.remove('correction')
      stor.yaco = false
      stor.yaexplik = false

      j3pEmpty(stor.lesdiv.explications)
      if (!yareponse()) {
        stor.lesdiv.correction.style.color = this.styles.cfaux
        stor.lesdiv.correction.innerHTML = reponseIncomplete
        this.afficheBoutonValider()
      } else { // Une réponse a été saisie
        // Bonne réponse
        if (bonneReponse()) {
          stor.lesdiv.correction.style.color = this.styles.cbien
          stor.lesdiv.correction.innerHTML = cBien
          this._stopTimer()
          this.score++
          this.typederreurs[0]++
          this.cacheBoutonValider()

          let affco = false
          if ((this.questionCourante % ds.nbetapes) === 1) {
            let mona = stor.laformule.grandeur + textes.genre2[stor.laformule.genre] + stor.laformule.figure
            if (stor.laformule.figure2 !== undefined) mona = stor.laformule.grandeur + textes.genre2[stor.laformule.genre] + stor.laformule.figure2
            let monb = stor.laformule.f
            if (monb === '2 \\times (\\text{longueur} + \\text{largeur})') monb += '\\text{ ou } 2 \\times \\text{longueur} + 2 \\times  \\text{largeur} '
            if ((stor.laformule.figure === 'sphère') && (stor.laformule.grandeur === 'le volume')) { mona = 'le volume d’une boule' }

            j3pAffiche(stor.lesdiv.solution, null, textes.Cobon1,
              {
                a: mona,
                b: monb
              })
            let gu = ''
            if (stor.repeleve.indexOf('j') !== -1) {
              for (let i = 0; i < stor.quoi.length; i++) {
                if (stor.quoi[i] === 'j') {
                  gu = stor.lesboutons[i]
                  break
                }
              }
              const ch = textes.aide1.replace('£a', gu)
              stor.aide1 = new BulleAide(stor.lesdiv.aide1, ch, { place: 1 })
              stor.repeleve = transformule2(stor.laformule.f)
              stor.foco = true
            }
            if ((stor.repeleve[0] === ' ') && (stor.repeleve[1] === '+')) {
              stor.repeleve = stor.repeleve.substring(2)
            }
            stor.yaco = true
          }
          if ((this.questionCourante % ds.nbetapes) === 2) {
            affco = false
            for (let i = 0; i < stor.zoneStyleAffiche.liste.length; i++) {
              if ((((stor.repatt[i] + '').replace('.', ',')) !== stor.repeleve[i]) && (!affco)) {
                affco = true
                j3pAffiche(stor.lesdiv.solution, null, textes.Cobon2)
              }
              stor.repatt[i] = stor.repeleve[i]
            }
          }
          if (((this.questionCourante % ds.nbetapes) === 3) || (((this.questionCourante % ds.nbetapes) === 0) && ((!ds.ValeurApprochee2) || (ds.CalculMental)))) {
            stor.zonem.corrige(true)
            stor.zonem.disable()
          }

          mettoutvert()
          disatout()

          this.etat = 'navigation'
          this.sectionCourante()
        } else {
          // Pas de bonne réponse
          stor.lesdiv.correction.style.color = this.styles.cfaux

          // A cause de la limite de temps :
          if (me.isElapsed) { // limite de temps
            this._stopTimer()
            stor.lesdiv.correction.innerHTML = tempsDepasse
            stor.lesdiv.correction.innerHTML += '<br>' + cFaux
            stor.lesdiv.correction.innerHTML += '<br>' + '<br>' + regardeCorrection
            this.typederreurs[10]++
            this.cacheBoutonValider()

            afcofo(true)

            this.etat = 'navigation'
            this.sectionCourante()
          } else { // Réponse fausse :
            stor.lesdiv.correction.innerHTML = cFaux

            if (this.essaiCourant < ds.nbchances) {
              stor.lesdiv.correction.innerHTML += '<br>' + essaieEncore
              this.typederreurs[1]++
            } else { // Erreur au nème essai
              me._stopTimer()
              me.cacheBoutonValider()
              stor.lesdiv.correction.innerHTML += '<br>' + regardeCorrection

              afcofo(true)

              this.typederreurs[2]++
              this.etat = 'navigation'
              this.sectionCourante()
            }
          }
        }
      }
      if (stor.yaco) stor.lesdiv.solution.classList.add('correction')
      if (stor.yaexplik) stor.lesdiv.explications.classList.add('explique')
      // Obligatoire
      this.finCorrection()
      break // case "correction":

    case 'navigation':

      if (this.sectionTerminee()) {
        // On détermine la phrase d’état renvoyée par la section
        // (this.typederreurs[3]) erreur formule
        //  (this.typederreurs[4]) erreur conversion
        //   (this.typederreurs[5]) confusion par défaut et par excès
        //    (this.typederreurs[6]) erreur dans le remplacement des valeurs
        //     (this.typederreurs[7]) erreur de calcul
        //   (this.typederreurs[8]) erreur element de geo
        //       this.typederreurs[2] = nombre de mauvaises réponses

        const sco = this.score / ds.nbitems
        let peibase = 0
        if (sco >= 0.8) {
          peibase = 0
        } else if (sco >= 0.55) {
          peibase = 1
        } else if (sco >= 0.3) {
          peibase = 8
        } else {
          peibase = 15
        }
        let peisup = 0
        let compt = this.typederreurs[2] / (3)
        if (this.typederreurs[3] > compt) {
          peisup = 1
          compt = this.typederreurs[3]
        }
        if (this.typederreurs[8] > compt) {
          peisup = 2
          compt = this.typederreurs[8]
        }
        if (this.typederreurs[4] > compt) {
          peisup = 3
          compt = this.typederreurs[4]
        }
        if (this.typederreurs[5] > compt) {
          peisup = 4
          compt = this.typederreurs[5]
        }
        if (this.typederreurs[6] > compt) {
          peisup = 5
          compt = this.typederreurs[6]
        }
        if (this.typederreurs[7] > compt) {
          peisup = 6
          compt = this.typederreurs[7]
        }
        this.parcours.pe = ds.pe[peibase + peisup]

        this.afficheBoutonSectionSuivante()
        this.focus('sectioncontinuer')
      } else {
        this.etat = 'enonce'
        this.afficheBoutonSuite()
        this.focus('boutoncontinuer')
      }
      // Obligatoire
      this.finNavigation()

      break // case "navigation":
  }
}
