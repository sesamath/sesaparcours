import { j3pAddElt, j3pAjouteBouton, j3pArrondi, j3pClone, j3pDetruit, j3pEmpty, j3pGetRandomInt, j3pShowError, j3pShuffle } from 'src/legacy/core/functions'
import { verifNombreBienEcrit } from 'src/legacy/outils/fonctions/ecritureNombre'
import { j3pAffiche } from 'src/lib/mathquill/functions'
import { addDefaultTable } from 'src/legacy/themes/table'
import { ecrisBienMathquill } from 'src/legacy/outils/zoneStyleMathquill/functions'
import ZoneStyleMathquill1 from 'src/legacy/outils/zoneStyleMathquill/ZoneStyleMathquill1'
import ZoneStyleMathquill2 from 'src/legacy/outils/zoneStyleMathquill/ZoneStyleMathquill2'
import { j3pAfficheCroixFenetres, j3pCreeFenetres, j3pToggleFenetres } from 'src/legacy/core/functionsJqDialog'
import ListeDeroulante from 'src/legacy/outils/listeDeroulante/ListeDeroulante'
import textesGeneriques from 'src/lib/core/textes'

import './stat03.scss'

/**
 * Ajouter une description ici
 * @fileOverview
 * @author Tommy Barroy <t.barroy@laposte.net>
 * @since 2017-10
 */

const { cBien, cFaux, essaieEncore, reponseIncomplete, tempsDepasse } = textesGeneriques

export const params = {
  outils: ['mathquill', 'calculatrice'],
  /**
   * Ce qui est paramétrable dans l’éditeur de graphe
   * @type {LegacySectionParamsParametres}
   */
  parametres: [
    ['nbrepetitions', 9, 'entier', 'Nombre de répétitions de la section'],
    ['nbchances', 1, 'entier', 'Nombre de chances'],
    ['Effectif_Total', true, 'boolean', 'La série est présentée sous la forme d’un tableau'],
    ['Frequences', 'oui', 'liste', 'Calcul demandé', ['fraction', 'pourcentage', 'décimale', 'toutes', 'oui', 'non']],
    ['Mediane', true, 'boolean', 'La série est présentée sous la forme d’un tableau'],
    ['Quartiles', true, 'boolean', 'La série est présentée sous la forme d’un tableau'],
    ['Etendue', true, 'boolean', 'La série est présentée sous la forme d’un tableau'],
    ['Moyenne', true, 'boolean', 'La série est présentée sous la forme d’un tableau'],
    ['Classe', 'les deux', 'liste', 'Calcul demandé', ['oui', 'les deux', 'non']],
    ['Calculatrice', true, 'boolean', '<u>true</u>: Une caculatrice est disponible.'],
    ['Justifie', true, 'boolean', '<u>true</u>: Une caculatrice est disponible.'],
    ['Donnees', 'tableau', 'liste', 'Calcul demandé', ['tableau', 'liste', 'les deux']],
    ['theme', 'standard', 'liste', 'Modèle pour l’affichage', ['standard', 'zonesAvecImageDeFond']]
  ]
}

/**
 * section stat03
 * @this {Parcours}
 */
export default function main () {
  const me = this
  const ds = me.donneesSection
  const stor = me.storage
  const listpb = [
    { maxmoy: 13, minmoy: 9, unit: 'ans', et1: 9, et2: 13, cla: false, km: true, nomclass: 'âges (en années)', nomeff: 'nombre de personnes', descrip: "Johann a obtenu une liste d'âges d’un grand nombre de personnes.", classes: [9, 10, 11, 12, 13], liaison: 'ont', cc: [9, 10, 11, 12, 13], descrip2: "Johann a obtenu une liste d'âges de plusieurs personnes." },
    { maxmoy: 5, minmoy: 0, unit: 'écrans', et1: 0, et2: 5, cla: false, km: true, nomclass: 'nombre d’écrans', nomeff: 'nombre de foyers', descrip: 'Un recensement donne le nombre d’écrans par foyer.', classes: [0, 1, 2, 3, 4, 5], liaison: 'ont', cc: [0, 1, 2, 3, 4, 5], descrip2: 'Un sondage donne le nombre d’écrans par foyer.' },
    { cla: false, km: false, nomclass: 'catégories', nomeff: 'nombre de figurines', descrip: 'L’entreprise de Charlie fabrique plusieurs sortes de figurines.', classes: ['soldats', 'monstres', 'pirates', 'cowboys', 'indiens'], liaison: 'représentent des', descrip2: 'Charlie a classé ses figurines.' },
    { cla: false, km: false, nomclass: 'outils utilisés', nomeff: 'nombre de dessins', descrip: 'L’école de Coline a répertorié des dessins d’élèves.', classes: ['feutre', 'crayon', 'fusain'], liaison: 'sont réalisés au', descrip2: 'Coline a classé ses dessins.' },
    { cla: false, km: false, nomclass: 'familles', nomeff: 'nombre de dinosaures', descrip: 'Un musée a classé des os de dinosaures.', classes: ['cératopodes', 'thyréophores', 'sauropodomorphes', 'théropodes'], liaison: 'appartiennent à la famille des', descrip2: 'Ninon a classé ses dinosaures préférés.' }
  ]
  const listepbclasse = [
    { maxmoy: 8, minmoy: 4, unit: 'cm', et1: 4, et2: 8, cla: true, km: true, nomclass: 'longueurs (en cm)', nomeff: 'nombre de morceaux', descrip: 'L’entreprise de Rémi fabrique des batonnets de longueurs différentes.', classes: ['entre 4 et 5 cm', 'entre 5 et 6 cm', 'entre 6 et 7 cm', 'entre 7 et 8 cm'], pc: 10, lc: [[41, 49], [51, 59], [61, 69], [71, 79]], liaison: 'mesurent', cc: [4.5, 5.5, 6.5, 7.5], descrip2: 'Rémi a fabriqué des batonnets de longueurs différentes.' },
    { maxmoy: 2.8, minmoy: 2, unit: 'kg', et1: 2, et2: 2.8, cla: true, km: true, nomclass: 'masse <i>p</i> (en kg)', nomeff: 'nombre de briques', descrip: 'L’entreprise de Llyn fabrique des briques de masses différentes.', classes: ['$2 \\le p \\le 2,2$', '$2,2 < p \\le 2,4$', '$2,4 < p \\le 2,6$', '$2,6 < p \\le 2,8$'], pc: 100, lc: [[200, 220], [221, 240], [241, 260], [261, 280]], liaison: 'pèsent', cc: [2.1, 2.3, 2.5, 2.7], descrip2: 'Lynn a pesé plusieurs briques.' },
    { maxmoy: 180, minmoy: 0, unit: '°', et1: 0, et2: 180, cla: true, km: true, nomclass: 'mesure <i>m</i> (en °)', nomeff: "nombre d’angles'", descrip: 'L’entreprise de Zélie réalise des pièces d’angles différents.', classes: ['$0 \\le m \\le 45$', '$45 < m \\le 90$', '$90 < m \\le 135$', '$135 < m \\le 180$'], pc: 10, lc: [[0, 450], [451, 900], [901, 1350], [1351, 1790]], liaison: 'mesurent', cc: [22.5, 67.5, 112.5, 157.5], descrip2: 'Eve a construit plusieurs angles.' }
  ]

  function creeLesDiv () {
    stor.lesdiv = {}
    stor.lesdiv.conteneur = j3pAddElt(me.zonesElts.MG, 'div', {
      style: me.styles.etendre('toutpetit.enonce', { padding: '10px' })
    })
    const ghj2 = addDefaultTable(stor.lesdiv.conteneur, 1, 3)
    modif(ghj2)
    ghj2[0][1].style.width = '10px'
    const ghj = addDefaultTable(ghj2[0][0], 2, 1)
    modif(ghj)
    stor.lesdiv.donnees = ghj[0][0].parentNode.parentNode
    stor.lesdiv.donnees1 = ghj[0][0]
    stor.lesdiv.donnees2 = ghj[1][0]
    const tt = addDefaultTable(stor.lesdiv.conteneur, 1, 2)
    stor.lesdiv.laCroix = tt[0][0]
    stor.lesdiv.aCons = tt[0][1]
    tt[0][0].style.verticalAlign = 'top'
    stor.lesdiv.etape = addDefaultTable(stor.lesdiv.conteneur, 1, 1)[0][0]
    stor.lesdiv.etape.style.padding = '0px'
    stor.lesdiv.travail = addDefaultTable(stor.lesdiv.conteneur, 1, 1)[0][0]
    stor.lesdiv.travail.style.padding = '0px'
    stor.lesdiv.explications = j3pAddElt(stor.lesdiv.conteneur, 'div')
    stor.lesdiv.solution = j3pAddElt(stor.lesdiv.conteneur, 'div')
    stor.lesdiv.calculatrice = j3pAddElt(ghj2[0][2], 'div', {
      style: me.styles.etendre('toutpetit.enonce', { padding: '10px' })
    })
    stor.lesdiv.correction = j3pAddElt(ghj2[0][2], 'div', {
      style: me.styles.etendre('toutpetit.enonce', { padding: '10px' })
    })
    stor.lesdiv.solution.style.color = me.styles.toutpetit.correction.color
    stor.lesdiv.explications.style.color = me.styles.cfaux
    me.zonesElts.MG.classList.add('fond')
  }
  function modif (ki, b) {
    for (let i = 0; i < ki.length; i++) {
      for (let j = 0; j < ki[i].length; j++) {
        if (!b) {
          ki[i][j].style.padding = '0px'
        } else {
          ki[i][j].style.padding = '0px 3px 0px 3px'
          ki[i][j].style.border = '1px solid black'
          ki[i][j].style.textAlign = 'center'
        }
      }
    }
  }
  function somme () {
    let ret = 0
    for (let i = 0; i < stor.lesfreq.length; i++) {
      ret += stor.lesfreq[i]
    }
    return ret
  }
  function somme2 () {
    let ret = 0
    for (let i = 0; i < stor.leseff.length; i++) {
      ret += stor.leseff[i]
    }
    return ret
  }
  function faisChoixExos () {
    const exo = ds.exos.pop()
    const tot = exo.pb.classes.length
    stor.lesfreq = []
    if (exo.donnees) {
      for (let i = 0; i < tot; i++) stor.lesfreq.push(10)
      do {
        stor.lesfreq[j3pGetRandomInt(0, stor.lesfreq.length - 1)]++
      } while (somme() < 100)
      const mul = j3pShuffle([201, 203, 207, 197, 193, 307, 503, 703, 1017, 1567]).pop()
      stor.leseff = []
      for (let i = 0; i < tot; i++) stor.leseff.push(mul * stor.lesfreq[i])
      stor.tot = 100 * mul
    } else {
      exo.freq = 'fractions'
      stor.leseff = []
      for (let i = 0; i < tot; i++) stor.leseff.push(1)
      stor.tot = j3pGetRandomInt(10, 24)
      do {
        stor.leseff[j3pGetRandomInt(0, stor.leseff.length - 1)]++
      } while (somme2() < stor.tot)
      for (let i = 0; i < tot; i++) stor.lesfreq.push(stor.leseff[i] / stor.tot)
    }
    stor.ival = j3pGetRandomInt(0, stor.leseff.length - 1)
    stor.yadeb = false
    stor.yaeuMed = false
    stor.doukoul = stor.lesdiv.travail
    stor.aAMouv = []
    stor.aTrans = []
    stor.laE = false
    stor.yayaCache = false
    stor.gardeLum = []
    stor.lesvalranggge = false
    return exo
  }
  function initSection () {
    let obb = false
    // Construction de la page
    me.construitStructurePage('presentation3')

    // Le paramètre définit la largeur relative de la première colonne
    stor.afaire = []
    if (ds.Effectif_Total) {
      if (ds.Justifie) stor.afaire.push('JusEffectif_Total')
      stor.afaire.push('Effectif_Total')
    }
    if (ds.Frequences !== 'non') {
      if (ds.Justifie) stor.afaire.push('JusFrequences')
      stor.afaire.push('Frequences')
    }
    if (ds.Mediane) {
      obb = true
      if (ds.Justifie) {
        stor.afaire.push('JusMediane1')
        stor.afaire.push('JusMediane2')
      }
      stor.afaire.push('Mediane')
    }
    if (ds.Quartiles) {
      obb = true
      if (ds.Justifie) {
        if (stor.afaire.indexOf('JusMediane1') === -1) stor.afaire.push('JusMediane1')
        stor.afaire.push('JusQuartiles')
      }
      stor.afaire.push('Quartiles')
    }
    if (ds.Etendue) {
      obb = true
      if (ds.Justifie) stor.afaire.push('JusEtendue')
      stor.afaire.push('Etendue')
    }
    if (ds.Moyenne) {
      obb = true
      if (ds.Justifie) stor.afaire.push('JusMoyenne')
      stor.afaire.push('Moyenne')
    }
    if (stor.afaire.length === 0) {
      j3pShowError('paramétrage')
      if (ds.Justifie) stor.afaire.push('JusEffectif_Total')
      stor.afaire.push('Effectif_Total')
    }

    let titre = 'Etude d’une série statistique'
    if (stor.afaire.length === 1 || (stor.afaire.length === 2 && ds.Justifie)) {
      switch (stor.afaire[0]) {
        case 'Effectif_Total':
          titre = 'Effectif total d’une série statistique'
          break
        case 'Frequences':
          titre = 'Fréquence d’une valeur d’une série statistique'
          break
        case 'Mediane':
          titre = 'Médiane d’une série statistique'
          break
        case 'Quartiles':
          titre = 'Quartile d’une série statistique'
          break
        case 'Etendue':
          titre = 'Etendue d’une série statistique'
          break
        case 'Moyenne':
          titre = 'Moyenne d’une série statistique'
          break
        case 'Representation':
          titre = 'Représentation graphique d’une série statistique'
          break
      }
    }
    me.surcharge({ nbetapes: stor.afaire.length })
    stor.etapeencours = stor.afaire[stor.afaire.length - 1]

    const exos = []
    for (let i = 0; i < ds.nbrepetitions; i++) {
      exos.push({})
    }
    let freq = []
    if (ds.Frequences === 'fraction' || ds.Frequences === 'toutes') freq.push('fractions')
    if (ds.Frequences === 'pourcentage' || ds.Frequences === 'toutes') freq.push('pourcentage')
    if (ds.Frequences === 'décimale' || ds.Frequences === 'toutes') freq.push('decimale')
    if (ds.Frequences === 'non') freq.push('non')
    freq = j3pShuffle(freq)
    for (let i = 0; i < ds.nbrepetitions; i++) {
      exos[i].freq = freq[i % (freq.length)]
    }

    let classe = []
    if (ds.Classe === 'oui' || ds.Classe === 'les deux') classe.push(true)
    if (ds.Classe === 'non' || ds.Classe === 'les deux') classe.push(false)
    classe = j3pShuffle(classe)
    for (let i = 0; i < ds.nbrepetitions; i++) {
      exos[i].classe = classe[i % (classe.length)]
    }

    let donnees = []
    if (ds.Donnees === 'tableau' || ds.Donnees === 'les deux') donnees.push(true)
    if (ds.Donnees === 'liste' || ds.Donnees === 'les deux') donnees.push(false)
    donnees = j3pShuffle(donnees)
    for (let i = 0; i < ds.nbrepetitions; i++) {
      exos[i].donnees = donnees[i % (donnees.length)]
    }

    const rep = j3pShuffle(['diag-graph', 'circ'])
    for (let i = 0; i < ds.nbrepetitions; i++) {
      exos[i].rep = rep[i % (rep.length)]
    }

    const quart = j3pShuffle(['prems', 'deuz'])
    for (let i = 0; i < ds.nbrepetitions; i++) {
      exos[i].quart = quart[i % (quart.length)]
    }

    // gerepb
    const lpb = j3pShuffle(j3pClone(listpb))
    if (obb) {
      for (let i = lpb.length - 1; i > -1; i--) {
        if (!lpb[i].km) lpb.splice(i, 1)
      }
    }
    const lpbC = j3pShuffle(j3pClone(listepbclasse))
    let i1 = 0
    let i2 = 0
    for (let i = 0; i < ds.nbrepetitions; i++) {
      if (exos[i].classe) {
        exos[i].pb = j3pClone(lpbC[i1])
        i1++
        if (i1 === lpbC.length) i1 = 0
      } else {
        exos[i].pb = j3pClone(lpb[i2])
        i2++
        if (i2 === lpb.length) i2 = 0
      }
    }
    ds.exos = j3pShuffle(exos)
    me.afficheTitre(titre)
    enonceMain()
  }

  function changeGG () {
    stor.yayaCache = !stor.yayaCache
    const kl = stor.yayaCache ? 'none' : ''
    const kl1 = stor.yayaCache ? 'boutonAddS' : 'boutonMoinsS'
    const kl2 = stor.yayaCache ? 'boutonMoinsS' : 'boutonAddS'
    stor.lesdiv.aCons.style.display = kl
    stor.LeOuBout.classList.add(kl1)
    stor.LeOuBout.classList.remove(kl2)
  }
  function faisMed () {
    const tr = document.createElement('tr')
    const cells = []
    for (let i = 0; i < stor.leseff.length + 2; i++) {
      cells.push(j3pAddElt(tr, 'td'))
      cells[cells.length - 1].style.border = '1px solid black'
      cells[cells.length - 1].style.padding = '0px 3px 0px 3px'
      cells[cells.length - 1].style.textAlign = 'center'
    }
    stor.letab[0][0].parentNode.parentNode.insertBefore(tr, stor.letab[1][0].parentNode.nextSibling)
    stor.yaeuMed = true
    stor.aVireMed = tr
    j3pAffiche(cells[0], null, '<b>eff. cumul. crois.</b>')
    stor.demCum = []
    for (let i = 0; i < stor.leseff.length; i++) {
      stor.demCum[stor.demCum.length] = new ZoneStyleMathquill1(cells[1 + i], { restric: ' 0123456789', limite: 16, limitenb: 4, enter: () => { me.sectionCourante() } })
    }
    stor.cellMed = cells
    stor.medAsupp = tr
    stor.doukoul = tr
  }
  function poseQuestion () {
    for (let i = 0; i < stor.gardeLum.length; i++) stor.gardeLum[i].style.color = ''
    if (!stor.laE && stor.aAMouv.length > 0) {
      const boutons = addDefaultTable(stor.lesdiv.laCroix, 1, 3)
      modif(boutons)
      boutons[0][1].classList.add('boutonMoinsS')
      boutons[0][1].addEventListener('click', changeGG, false)
      boutons[0][0].classList.add('spacerS')
      boutons[0][2].classList.add('spacerS')
      stor.laE = true
      stor.LeOuBout = boutons[0][1]
    }
    for (let i = stor.aAMouv.length - 1; i > -1; i--) {
      stor.lesdiv.aCons.append(stor.aAMouv.pop())
    }
    stor.doukoul.classList.remove('travail')
    j3pEmpty(stor.lesdiv.solution)
    j3pEmpty(stor.lesdiv.explications)
    j3pEmpty(stor.lesdiv.correction)
    stor.lesdiv.solution.classList.remove('correction')
    stor.lesdiv.explications.classList.remove('explique')
    if (stor.etapeencours === 'JusMediane1') {
      stor.doukoul.classList.remove('correction')
      stor.doukoul.classList.remove('travail')
    }
    stor.etapeencours = suiv()
    if (!stor.yadeb) {
      stor.yadeb = true
      if (stor.Lexo.donnees) {
        j3pAffiche(stor.lesdiv.donnees1, null, stor.Lexo.pb.descrip.replace('£a', '$' + ecrisBienMathquill(stor.tot) + '$'))
        stor.letab = addDefaultTable(stor.lesdiv.donnees2, 2, stor.leseff.length + 2)
        modif(stor.letab, true)
        j3pAffiche(stor.letab[0][0], null, '<b>' + stor.Lexo.pb.nomclass + '</b>')
        j3pAffiche(stor.letab[1][0], null, '<b>' + 'Effectifs' + '</b>')
        j3pAffiche(stor.letab[0][stor.letab[0].length - 1], null, '<b>TOTAL</b>')
        for (let i = 0; i < stor.leseff.length; i++) {
          j3pAffiche(stor.letab[0][1 + i], null, String(stor.Lexo.pb.classes[i]))
          j3pAffiche(stor.letab[1][1 + i], null, '$' + ecrisBienMathquill(stor.leseff[i]) + '$')
        }
      } else {
        j3pAffiche(stor.lesdiv.donnees1, null, stor.Lexo.pb.descrip2)
        stor.listVal = []
        stor.letab = addDefaultTable(stor.lesdiv.donnees2, 3, 21)
        stor.bb1 = j3pAjouteBouton(stor.lesdiv.donnees2, rangeVal, { className: '', value: 'Ranger dans l’ordre croissant' })
        let lili = 0
        const listef = j3pClone(stor.leseff)
        for (let i = 0; i < stor.tot; i++) {
          if (i > 9) lili = 1
          if (i > 19) lili = 2
          let lef = -1
          do {
            const ui = j3pGetRandomInt(0, listef.length - 1)
            if (listef[ui] !== 0) {
              lef = ui
              listef[ui]--
            }
          } while (lef === -1)
          if (stor.Lexo.pb.cla) {
            const val = j3pArrondi(j3pGetRandomInt(stor.Lexo.pb.lc[lef][0], stor.Lexo.pb.lc[lef][1]) / stor.Lexo.pb.pc, 3)
            stor.listVal.push({ val, c: false })
            j3pAffiche(stor.letab[lili][2 * i - 20 * lili], null, '$' + ecrisBienMathquill(val) + '$ ' + stor.Lexo.pb.unit)
          } else {
            const ss = (stor.Lexo.pb.classes[lef] !== 0 && stor.Lexo.pb.classes[lef] !== 1) ? stor.Lexo.pb.unit : stor.Lexo.pb.unit.replace('s', '')
            stor.listVal.push({ val: stor.Lexo.pb.classes[lef], c: false })
            j3pAffiche(stor.letab[lili][2 * i - 20 * lili], null, '$' + ecrisBienMathquill(stor.Lexo.pb.classes[lef]) + '$ ' + ss)
          }
          if (lef === stor.ival) {
            stor.gardeLum.push(stor.letab[lili][2 * i - 20 * lili])
            stor.listVal[stor.listVal.length - 1].c = true
          }
          j3pAffiche(stor.letab[lili][2 * i + 1 - 20 * lili], null, '&nbsp;-&nbsp;')
        }
        stor.letab2 = addDefaultTable(stor.lesdiv.donnees2, 3, 21)
        stor.listVal2 = j3pClone(stor.listVal)
        stor.listVal = stor.listVal.sort((a, b) => { return a.val - b.val })
        lili = 0
        for (let i = 0; i < stor.listVal.length; i++) {
          if (i > 9) lili = 1
          if (i > 19) lili = 2
          j3pAffiche(stor.letab2[lili][2 * i - 20 * lili], null, '$' + ecrisBienMathquill(stor.listVal[i].val) + '$ ' + stor.Lexo.pb.unit)
          j3pAffiche(stor.letab2[lili][2 * i + 1 - 20 * lili], null, '&nbsp;-&nbsp;')
          if (stor.listVal[i].c) stor.gardeLum.push(stor.letab2[lili][2 * i - 20 * lili])
        }
        stor.bb2 = j3pAjouteBouton(stor.lesdiv.donnees2, derangeVal, { className: '', value: 'Remettre dans l’ordre initial' })
        derangeVal()
      }
      stor.lesdiv.donnees1.classList.add('enonce')
      stor.lesdiv.donnees2.classList.add('enonce')
    }
    if (stor.yacoef) {
      j3pEmpty(stor.douko)
      j3pAffiche(stor.douko, null, stor.garderep)
      stor.douko.style.color = me.styles.petit.correction.color
    }
    if (stor.yacoef2) {
      j3pEmpty(stor.douko)
      j3pAffiche(stor.douko, null, '$' + ecrisBienMathquill(stor.tot) + '$')
      stor.douko.style.color = me.styles.petit.correction.color
    }
    if (stor.yacoeffreq) {
      j3pEmpty(stor.douko)
      j3pAffiche(stor.douko, null, '$' + stor.garderep + '$')
      stor.douko.style.color = me.styles.petit.correction.color
    }
    if (stor.yacoeffreq2) {
      j3pEmpty(stor.douko)
      j3pAffiche(stor.douko, null, '$' + stor.garderep + '$')
      stor.douko.style.color = me.styles.petit.correction.color
    }
    if (stor.focoMed1) {
      if (stor.Lexo.donnees) {
        j3pDetruit(stor.medAsupp)
      } else {
        j3pEmpty(stor.douko)
        j3pAffiche(stor.douko, null, stor.garderep)
        stor.douko.style.color = me.styles.petit.correction.color
      }
    }
    if (stor.focoMed2) {
      j3pEmpty(stor.douko)
      j3pAffiche(stor.douko, null, '$' + stor.garderep + '$')
      stor.douko.style.color = me.styles.petit.correction.color
      if (stor.yaMM) {
        j3pEmpty(stor.douko2)
        j3pAffiche(stor.douko2, null, '$' + stor.garderep2 + '$')
        stor.douko2.style.color = me.styles.petit.correction.color
      }
    }
    if (stor.focoMoy2) {
      j3pEmpty(stor.douko)
      j3pAffiche(stor.douko, null, '$' + ecrisBienMathquill(stor.garderep) + '$')
      stor.douko.style.color = me.styles.petit.correction.color
    }
    if (stor.focoMoy1) {
      j3pEmpty(stor.douko)
      j3pAffiche(stor.douko, null, stor.garderep)
      stor.douko.style.color = me.styles.petit.correction.color
    }
    if (stor.focoEten2) {
      j3pEmpty(stor.douko)
      j3pAffiche(stor.douko, null, '$' + ecrisBienMathquill(stor.garderep) + '$')
      stor.douko.style.color = me.styles.petit.correction.color
    }
    if (stor.focoEten1) {
      j3pEmpty(stor.douko)
      j3pAffiche(stor.douko, null, stor.garderep)
      stor.douko.style.color = me.styles.petit.correction.color
    }
    if (stor.focoMed3) {
      j3pEmpty(stor.douko)
      j3pAffiche(stor.douko, null, '$' + ecrisBienMathquill(stor.garderep) + '$')
      stor.douko.style.color = me.styles.petit.correction.color
    }
    if (stor.focoQuart2) {
      j3pEmpty(stor.douko)
      j3pAffiche(stor.douko, null, '$' + stor.garderep + '$')
      stor.douko.style.color = me.styles.petit.correction.color
      j3pEmpty(stor.douko2)
      j3pAffiche(stor.douko2, null, '$' + stor.garderep2 + '$')
      stor.douko2.style.color = me.styles.petit.correction.color
    }
    if (stor.focoQuart1) {
      j3pEmpty(stor.douko)
      j3pAffiche(stor.douko, null, '$' + ecrisBienMathquill(stor.garderep) + '$')
      stor.douko.style.color = me.styles.petit.correction.color
      j3pEmpty(stor.douko2)
      j3pAffiche(stor.douko2, null, '$' + ecrisBienMathquill(stor.garderep2) + '$')
      stor.douko2.style.color = me.styles.petit.correction.color
      if (stor.yaMM) {
        j3pEmpty(stor.douko3)
        j3pAffiche(stor.douko3, null, '$' + ecrisBienMathquill(stor.garderep3) + '$')
        stor.douko3.style.color = me.styles.petit.correction.color
        j3pEmpty(stor.douko4)
        j3pAffiche(stor.douko4, null, '$' + ecrisBienMathquill(stor.garderep4) + '$')
        stor.douko4.style.color = me.styles.petit.correction.color
      }
    }
    stor.yacoeffreq2 = false
    stor.yacoef = false
    stor.yacoef2 = false
    stor.yacoeffreq = false
    stor.focoMed1 = false
    stor.focoMed2 = false
    stor.focoMed3 = false
    stor.focoMoy2 = false
    stor.focoMoy1 = false
    stor.focoEten2 = false
    stor.focoEten1 = false
    stor.focoQuart2 = false
    stor.focoQuart1 = false
    let laquest
    if (stor.etapeencours !== 'JusEffectif_Total' && stor.etapeencours !== 'Effectif_Total' && stor.Lexo.donnees) {
      j3pEmpty(stor.letab[1][stor.letab[0].length - 1])
      j3pAffiche(stor.letab[1][stor.letab[0].length - 1], null, '$' + ecrisBienMathquill(stor.tot) + '$')
    }
    switch (stor.etapeencours) {
      case 'JusEffectif_Total': {
        let rep1, rep2, rep3, rep4
        const tt1 = addDefaultTable(stor.lesdiv.travail, 1, 2)
        modif(tt1)
        let ll
        if (stor.Lexo.donnees) {
          laquest = '<b><u>Choisis le bon calcul pour l’effectif total.</u></b>'
          j3pAffiche(tt1[0][0], null, 'Effectif total =&nbsp;')
          rep1 = rep2 = rep3 = rep4 = ecrisBienMathquill(stor.leseff[0])
          for (let i = 1; i < stor.leseff.length; i++) {
            rep1 += ' + ' + ecrisBienMathquill(stor.leseff[i])
            rep2 += ' - ' + ecrisBienMathquill(stor.leseff[i])
            rep3 += ' \\times ' + ecrisBienMathquill(stor.leseff[i])
            rep4 += ' \\div ' + ecrisBienMathquill(stor.leseff[i])
          }
          ll = j3pShuffle(['$' + rep1 + '$', '$' + rep2 + '$', '$' + rep3 + '$', '$' + rep4 + '$'])
          stor.garderep = '$' + rep1 + '$'
        } else {
          laquest = '<b><u>Choisis la bonne méthode pour obtenir l’effectif total.</u></b>'
          j3pAffiche(tt1[0][0], null, 'Pour obtenir l’effectif total,&nbsp;')
          rep1 = 'je compte le nombre de valeurs.'
          rep2 = 'j’ajoute toutes les valeurs.'
          rep3 = 'je soustrais toutes les valeurs.'
          rep4 = 'je multiplies toutes les valeurs.'
          ll = j3pShuffle([rep1, rep2, rep3, rep4])
          stor.garderep = rep1
        }
        ll.splice(0, 0, 'Choisir')
        stor.lili1 = ListeDeroulante.create(tt1[0][1], ll)
        stor.douko = tt1[0][1]
        stor.doukoul = tt1[0][0].parentNode.parentNode
        stor.aTrans.push(tt1[0][0].parentNode.parentNode)
      }
        break
      case 'Effectif_Total': {
        laquest = '<b><u>Calcule l’effectif total.</u></b>'
        const tt1 = addDefaultTable(stor.lesdiv.travail, 1, 2)
        modif(tt1)
        j3pAffiche(tt1[0][0], null, 'Effectif total =&nbsp;')
        stor.lili1 = new ZoneStyleMathquill1(tt1[0][1], { restric: '0123456789 ', limite: 16, limitenb: 4, enter: () => { me.sectionCourante() } })
        stor.garderep = '$' + stor.tot + '$'
        stor.douko = tt1[0][1]
        stor.doukoul = tt1[0][0].parentNode.parentNode
        stor.aAMouv.push(tt1[0][0].parentNode.parentNode)
        for (let i = stor.aTrans.length - 1; i > -1; i--) {
          stor.aAMouv.push(stor.aTrans.pop())
        }
      }
        break
      case 'JusFrequences': {
        let mod = ''
        let rep1, rep2, rep3, rep4
        switch (stor.Lexo.freq) {
          case 'fractions':
            mod = '<i> (écriture fractionnaire)</i>'
            rep1 = '\\frac{ \\text{effectif} }{ \\text{effectif total} }'
            rep2 = '\\frac{ \\text{effectif total} }{ \\text{effectif} }'
            rep3 = '\\frac{ \\text{effectif total} }{ \\text{effectif} } \\times 100'
            rep4 = '\\frac{ \\text{effectif} }{ \\text{effectif total} } \\times 100'
            break
          case 'pourcentage':
            mod = '<i>(en pourcentage)</i>'
            rep1 = '\\frac{ ' + ecrisBienMathquill(stor.leseff[stor.ival]) + ' }{ ' + ecrisBienMathquill(stor.tot) + '} \\times 100'
            rep2 = '\\frac{ ' + ecrisBienMathquill(stor.tot) + ' }{ ' + ecrisBienMathquill(stor.leseff[stor.ival]) + '} \\times 100'
            rep3 = '\\frac{ ' + ecrisBienMathquill(stor.leseff[stor.ival]) + ' }{ ' + ecrisBienMathquill(stor.tot) + '} \\div 100'
            rep4 = '\\frac{ ' + ecrisBienMathquill(stor.tot) + ' }{ ' + ecrisBienMathquill(stor.leseff[stor.ival]) + '} \\div 100'
            break
          default:
            mod = '<i>(en écriture décimale)</i>'
            rep1 = '\\frac{ ' + ecrisBienMathquill(stor.leseff[stor.ival]) + ' }{ ' + ecrisBienMathquill(stor.tot) + '}'
            rep2 = '\\frac{ ' + ecrisBienMathquill(stor.tot) + ' }{ ' + ecrisBienMathquill(stor.leseff[stor.ival]) + '} \\times 100'
            rep3 = '\\frac{ ' + ecrisBienMathquill(stor.leseff[stor.ival]) + ' }{ ' + ecrisBienMathquill(stor.tot) + '} \\times 100'
            rep4 = '\\frac{ ' + ecrisBienMathquill(stor.tot) + ' }{ ' + ecrisBienMathquill(stor.leseff[stor.ival]) + '}'
            break
        }
        stor.garderep = rep1
        const yacla = (stor.Lexo.pb.cla) ? 'classe' : 'valeur'
        laquest = `<b><u>Choisis le bon calcul pour la fréquence ${mod} de la ${yacla} <i>${stor.Lexo.pb.classes[stor.ival]}</i></u></b>`
        const tt1 = addDefaultTable(stor.lesdiv.travail, 1, 2)
        modif(tt1)
        stor.souv = 'Fréquence de la ' + yacla + ' ' + stor.Lexo.pb.classes[stor.ival]
        j3pAffiche(tt1[0][0], null, 'Fréquence de la ' + yacla + ' ' + stor.Lexo.pb.classes[stor.ival] + ' =&nbsp;')
        const ll = j3pShuffle(['$' + rep1 + '$', '$' + rep2 + '$', '$' + rep3 + '$', '$' + rep4 + '$'])
        ll.splice(0, 0, 'Choisir')
        stor.lili1 = ListeDeroulante.create(tt1[0][1], ll)
        stor.douko = tt1[0][1]
        stor.doukoul = tt1[0][0].parentNode.parentNode
        stor.aTrans.push(tt1[0][0].parentNode.parentNode)
      }
        break
      case 'Frequences': {
        let mod = ''
        let rettt = '0123456789'
        switch (stor.Lexo.freq) {
          case 'fractions':
            rettt += '/'
            mod = '<i> (écriture fractionnaire) </i>'
            stor.garderep = '\\frac{' + stor.leseff[stor.ival] + '}{' + stor.tot + '}'
            break
          case 'pourcentage':
            mod = '<i> (en pourcentage) </i>'
            stor.garderep = j3pArrondi(stor.leseff[stor.ival] / stor.tot * 100, 2)
            break
          default:
            mod = '<i> (en écriture décimale) </i>'
            rettt += '.,'
            stor.garderep = j3pArrondi(stor.leseff[stor.ival] / stor.tot, 2)
            break
        }
        const yacla = (stor.Lexo.pb.cla) ? 'classe' : 'valeur'
        laquest = '<b><u>Calcule la fréquence de la ' + yacla + ' ' + stor.Lexo.pb.classes[stor.ival] + mod + ' .</u></b>'
        const tt1 = addDefaultTable(stor.lesdiv.travail, 1, 3)
        modif(tt1)
        stor.souv = 'Fréquence de la ' + yacla + ' ' + stor.Lexo.pb.classes[stor.ival]
        j3pAffiche(tt1[0][0], null, 'Fréquence de la ' + yacla + ' ' + stor.Lexo.pb.classes[stor.ival] + ' =&nbsp;')
        stor.lili1 = new ZoneStyleMathquill2(tt1[0][1], { id: 'zozone', restric: rettt, limite: 16, limitenb: 4, enter: () => { me.sectionCourante() } })
        stor.douko = tt1[0][1]
        if (stor.Lexo.freq === 'pourcentage') j3pAffiche(tt1[0][2], null, '&nbsp;%')
        stor.doukoul = tt1[0][0].parentNode.parentNode
        stor.aAMouv.push(tt1[0][0].parentNode.parentNode)
        for (let i = stor.aTrans.length - 1; i > -1; i--) {
          stor.aAMouv.push(stor.aTrans.pop())
        }
      }
        break
      case 'JusMediane1':
        if (stor.Lexo.donnees) {
          if (!stor.yaeuMed) faisMed()
          laquest = '<b><u>Complète la ligne <i>effectifs cumulés croissants</i> dans le tableau.</u></b>'
          let cim = 0
          stor.garderep = []
          for (let i = 0; i < stor.leseff.length; i++) {
            cim += stor.leseff[i]
            stor.garderep.push(cim)
          }
        } else {
          const tt1 = addDefaultTable(stor.lesdiv.travail, 2, 1)
          modif(tt1)
          laquest = '<b><u>Choisis la bonne méthode pour obtenir ' + ((stor.tot % 2 === 1) ? 'la' : 'une') + ' médiane (ou les quartiles).</u></b>'
          j3pAffiche(tt1[0][0], null, 'Pour obtenir ' + ((stor.tot % 2 === 1) ? 'la' : 'une') + ' médiane (ou les quartiles), ')
          const rep1 = 'je range les valeurs dans l’ordre croissant.'
          const rep2 = 'je compte le nombre de valeurs.'
          const rep3 = 'je mélange les valeurs.'
          const rep4 = 'je sépare les valeurs paires et les valeurs impaires.'
          const ll = j3pShuffle([rep1, rep2, rep3, rep4])
          stor.garderep = rep1
          ll.splice(0, 0, 'Choisir')
          stor.lili1 = ListeDeroulante.create(tt1[1][0], ll)
          stor.douko = tt1[1][0]
          stor.doukoul = tt1[1][0].parentNode.parentNode
          stor.aTrans.push(tt1[1][0].parentNode.parentNode)
        }
        break
      case 'JusMediane2':
        if (stor.Lexo.donnees || stor.tot % 2 === 1) {
          laquest = '<b><u>Choisis la bonne démarche pour le calcul ' + ((stor.tot % 2 === 1) ? 'de la' : 'd’une') + ' médiane.</u></b>'
          const tt1 = addDefaultTable(stor.lesdiv.travail, 1, 3)
          modif(tt1)
          j3pAffiche(tt1[0][0], null, 'Pour obtenir ' + ((stor.tot % 2 === 1) ? 'la' : 'une') + ' médiane, ' + ((stor.tot % 2 === 1) ? 'il faut' : 'on peut') + ' chercher la&nbsp;')
          stor.lili1 = new ZoneStyleMathquill1(tt1[0][1], { restric: '0123456789 ', limite: 16, limitenb: 4, enter: () => { me.sectionCourante() } })
          stor.garderep = Math.floor(stor.tot / 2)
          if (stor.tot % 2 === 1) stor.garderep++
          stor.douko = tt1[0][1]
          j3pAffiche(tt1[0][2], null, '<sup>ème</sup> valeur.')
          stor.doukoul = tt1[0][0].parentNode.parentNode
          stor.aTrans.push(tt1[0][0].parentNode.parentNode)
          stor.yaMM = false
        } else {
          laquest = '<b><u>Choisis la bonne démarche pour le calcul ' + ((stor.tot % 2 === 1) ? 'de la' : 'd’une') + ' médiane.</u></b>'
          const tt1 = addDefaultTable(stor.lesdiv.travail, 2, 1)
          modif(tt1)
          j3pAffiche(tt1[0][0], null, 'Pour obtenir une médiane, il faut chercher une valeur comprise entre')
          const tt2 = addDefaultTable(tt1[1][0], 1, 5)
          modif(tt2)
          j3pAffiche(tt2[0][0], null, 'la &nbsp;')
          j3pAffiche(tt2[0][2], null, '<sup>ème</sup> et la &nbsp;')
          j3pAffiche(tt2[0][4], null, '<sup>ème</sup> valeur.')
          stor.lili1 = new ZoneStyleMathquill1(tt2[0][1], { restric: '0123456789 ', limite: 16, limitenb: 4, enter: () => { me.sectionCourante() } })
          stor.lili2 = new ZoneStyleMathquill1(tt2[0][3], { restric: '0123456789 ', limite: 16, limitenb: 4, enter: () => { me.sectionCourante() } })
          stor.garderep = Math.floor(stor.tot / 2)
          stor.douko = tt2[0][1]
          stor.garderep2 = Math.floor(stor.tot / 2) + 1
          stor.douko2 = tt2[0][3]
          stor.doukoul = tt1[0][0].parentNode.parentNode
          stor.aTrans.push(tt1[0][0].parentNode.parentNode)
          stor.yaMM = true
        }
        break
      case 'Mediane': {
        laquest = '<b><u>Calcul de la médiane.</u></b>'
        const tt1 = addDefaultTable(stor.lesdiv.travail, 1, 3)
        modif(tt1)
        j3pAffiche(tt1[0][0], null, 'Médiane =&nbsp;')
        j3pAffiche(tt1[0][2], null, '&nbsp;' + stor.Lexo.pb.unit)
        stor.lili1 = new ZoneStyleMathquill1(tt1[0][1], { restric: '0123456789 ,.', limite: 16, limitenb: 4, enter: () => { me.sectionCourante() } })
        let cim = 0
        stor.yaMM = false
        for (let i = 0; i < stor.leseff.length; i++) {
          cim += stor.leseff[i]
          if (cim >= stor.tot / 2) {
            stor.garderep = stor.Lexo.pb.cc[i]
            break
          }
        }
        if (!stor.Lexo.donnees) {
          if (stor.tot % 2 === 1) {
            stor.garderep = stor.listVal[Math.floor(stor.listVal.length / 2)].val
          } else {
            stor.garderep2 = stor.listVal[Math.floor(stor.listVal.length / 2) - 1].val
            stor.garderep3 = stor.listVal[Math.floor(stor.listVal.length / 2)].val
            stor.garderep = j3pArrondi((stor.garderep2 + stor.garderep3) / 2, 3)
            stor.yaMM = true
          }
        }
        stor.douko = tt1[0][1]
        stor.doukoul = tt1[0][0].parentNode.parentNode
        stor.aAMouv.push(tt1[0][0].parentNode.parentNode)
        for (let i = stor.aTrans.length - 1; i > -1; i--) {
          stor.aAMouv.push(stor.aTrans.pop())
        }
      }
        break
      case 'JusQuartiles':
        if (stor.Lexo.donnees || stor.tot % 4 !== 0) {
          laquest = '<b><u>Choisis la bonne démarche pour le calcul des quartiles.</u></b>'
          const tt1 = addDefaultTable(stor.lesdiv.travail, 2, 1)
          modif(tt1)
          const tt2 = addDefaultTable(tt1[0][0], 1, 3)
          modif(tt2)
          const tt3 = addDefaultTable(tt1[1][0], 1, 3)
          modif(tt3)
          j3pAffiche(tt2[0][0], null, 'Pour obtenir le 1<sup>er</sup> quartile, il faut chercher la&nbsp;')
          j3pAffiche(tt3[0][0], null, 'Pour obtenir le 2<sup>ème</sup> quartile, il faut chercher la&nbsp;')
          stor.lili1 = new ZoneStyleMathquill1(tt2[0][1], { restric: '0123456789 ', limite: 16, limitenb: 4, enter: () => { me.sectionCourante() } })
          stor.lili2 = new ZoneStyleMathquill1(tt3[0][1], { restric: '0123456789 ', limite: 16, limitenb: 4, enter: () => { me.sectionCourante() } })
          stor.garderep = Math.ceil(stor.tot / 4)
          stor.garderep2 = Math.ceil(3 * stor.tot / 4)
          stor.douko = tt2[0][1]
          stor.douko2 = tt3[0][1]
          j3pAffiche(tt2[0][2], null, '<sup>ème</sup> valeur.')
          j3pAffiche(tt3[0][2], null, '<sup>ème</sup> valeur.')
          stor.doukoul = tt1[0][0].parentNode.parentNode
          stor.aTrans.push(tt1[0][0].parentNode.parentNode)
          stor.yaMM = false
        } else {
          laquest = '<b><u>Choisis la bonne démarche pour le calcul des quartiles.</u></b>'
          const ttY = addDefaultTable(stor.lesdiv.travail, 2, 1)
          modif(ttY)
          const tt1 = addDefaultTable(ttY[0][0], 2, 1)
          modif(tt1)
          j3pAffiche(tt1[0][0], null, 'Pour obtenir le 1<sup>er</sup> quartile, il faut chercher une valeur comprise entre')
          const tt2 = addDefaultTable(tt1[1][0], 1, 5)
          modif(tt2)
          j3pAffiche(tt2[0][0], null, 'la &nbsp;')
          j3pAffiche(tt2[0][2], null, '<sup>ème</sup> et la &nbsp;')
          j3pAffiche(tt2[0][4], null, '<sup>ème</sup> valeur.')
          stor.lili1 = new ZoneStyleMathquill1(tt2[0][1], { restric: '0123456789 ', limite: 16, limitenb: 4, enter: () => { me.sectionCourante() } })
          stor.lili2 = new ZoneStyleMathquill1(tt2[0][3], { restric: '0123456789 ', limite: 16, limitenb: 4, enter: () => { me.sectionCourante() } })
          stor.garderep = Math.floor(stor.tot / 4)
          stor.douko = tt2[0][1]
          stor.garderep2 = Math.floor(stor.tot / 4) + 1
          stor.douko2 = tt2[0][3]
          stor.doukoul = tt1[0][0].parentNode.parentNode
          stor.aTrans.push(tt1[0][0].parentNode.parentNode)
          stor.yaMM = true

          const tt3 = addDefaultTable(ttY[1][0], 2, 1)
          modif(tt3)
          j3pAffiche(tt3[0][0], null, 'Pour obtenir le 2<sup>ème</sup> quartile, il faut chercher une valeur comprise entre')
          const tt4 = addDefaultTable(tt3[1][0], 1, 5)
          modif(tt4)
          j3pAffiche(tt4[0][0], null, 'la &nbsp;')
          j3pAffiche(tt4[0][2], null, '<sup>ème</sup> et la &nbsp;')
          j3pAffiche(tt4[0][4], null, '<sup>ème</sup> valeur.')
          stor.lili3 = new ZoneStyleMathquill1(tt4[0][1], { restric: '0123456789 ', limite: 16, limitenb: 4, enter: () => { me.sectionCourante() } })
          stor.lili4 = new ZoneStyleMathquill1(tt4[0][3], { restric: '0123456789 ', limite: 16, limitenb: 4, enter: () => { me.sectionCourante() } })
          stor.garderep3 = Math.floor(3 * stor.tot / 4)
          stor.douko3 = tt4[0][1]
          stor.garderep4 = Math.floor(3 * stor.tot / 4) + 1
          stor.douko4 = tt4[0][3]
          stor.aTrans.push(tt3[0][0].parentNode.parentNode)
        }
        break
      case 'Quartiles': {
        laquest = '<b><u>Calcul des quartiles.</u></b>'
        const tt1 = addDefaultTable(stor.lesdiv.travail, 2, 1)
        modif(tt1)
        const tt2 = addDefaultTable(tt1[0][0], 1, 3)
        modif(tt2)
        const tt3 = addDefaultTable(tt1[1][0], 1, 3)
        modif(tt3)
        j3pAffiche(tt2[0][0], null, '1<sup>er</sup> quartile =&nbsp;')
        j3pAffiche(tt3[0][0], null, '2<sup>ème</sup> quartile =&nbsp;')
        j3pAffiche(tt2[0][2], null, '&nbsp;' + stor.Lexo.pb.unit)
        j3pAffiche(tt3[0][2], null, '&nbsp;' + stor.Lexo.pb.unit)
        stor.lili1 = new ZoneStyleMathquill1(tt2[0][1], { restric: '0123456789 ,.', limite: 16, limitenb: 4, enter: () => { me.sectionCourante() } })
        stor.lili2 = new ZoneStyleMathquill1(tt3[0][1], { restric: '0123456789 ,.', limite: 16, limitenb: 4, enter: () => { me.sectionCourante() } })
        let cim = 0
        for (let i = 0; i < stor.leseff.length; i++) {
          cim += stor.leseff[i]
          if (cim >= stor.tot / 4) {
            stor.garderep = stor.Lexo.pb.cc[i]
            break
          }
        }
        cim = 0
        for (let i = 0; i < stor.leseff.length; i++) {
          cim += stor.leseff[i]
          if (cim >= 3 * stor.tot / 4) {
            stor.garderep2 = stor.Lexo.pb.cc[i]
            break
          }
        }
        if (!stor.Lexo.donnees) {
          if (stor.tot % 4 !== 0) {
            stor.garderep = stor.listVal[Math.floor(stor.listVal.length / 4)].val
            stor.garderep2 = stor.listVal[Math.floor(3 * stor.listVal.length / 4)].val
          } else {
            stor.garderep11 = stor.listVal[Math.floor(stor.listVal.length / 4) - 1].val
            stor.garderep12 = stor.listVal[Math.floor(stor.listVal.length / 4)].val
            stor.garderep = j3pArrondi((stor.garderep11 + stor.garderep12) / 2, 3)
            stor.garderep21 = stor.listVal[Math.floor(3 * stor.listVal.length / 4) - 1].val
            stor.garderep22 = stor.listVal[Math.floor(3 * stor.listVal.length / 4)].val
            stor.garderep2 = j3pArrondi((stor.garderep21 + stor.garderep22) / 2, 3)
            stor.yaMM = true
          }
        }
        stor.douko = tt2[0][1]
        stor.douko2 = tt3[0][1]
        stor.doukoul = tt1[0][0].parentNode.parentNode
        stor.aAMouv.push(tt1[0][0].parentNode.parentNode)
        for (let i = stor.aTrans.length - 1; i > -1; i--) {
          stor.aAMouv.push(stor.aTrans.pop())
        }
      }
        break
      case 'JusEtendue': {
        laquest = '<b><u>Choisis le bon calcul pour l’étendue.</u></b>'
        const tt1 = addDefaultTable(stor.lesdiv.travail, 1, 2)
        modif(tt1)
        j3pAffiche(tt1[0][0], null, 'Etendue =&nbsp;')
        const et2 = stor.Lexo.donnees ? stor.Lexo.pb.et2 : stor.listVal[stor.listVal.length - 1].val
        const et1 = stor.Lexo.donnees ? stor.Lexo.pb.et1 : stor.listVal[0].val
        const rep1 = '$' + ecrisBienMathquill(et2) + ' - ' + ecrisBienMathquill(et1) + '$'
        const rep2 = '$' + ecrisBienMathquill(et2) + ' + ' + ecrisBienMathquill(et1) + '$'
        const rep3 = '$' + ecrisBienMathquill(et1) + ' - ' + ecrisBienMathquill(et2) + '$'
        const rep4 = '$' + ecrisBienMathquill(et1) + ' + ' + ecrisBienMathquill(et2) + '$'
        const ll = j3pShuffle([rep1, rep2, rep3, rep4])
        ll.splice(0, 0, 'Choisir')
        stor.lili1 = ListeDeroulante.create(tt1[0][1], ll)
        stor.garderep = rep1
        stor.douko = tt1[0][1]
        stor.doukoul = tt1[0][0].parentNode.parentNode
        stor.aTrans.push(tt1[0][0].parentNode.parentNode)
      }
        break
      case 'Etendue': {
        laquest = '<b><u>Calcule l’étendue.</u></b>'
        const tt1 = addDefaultTable(stor.lesdiv.travail, 1, 3)
        modif(tt1)
        j3pAffiche(tt1[0][0], null, 'Etendue =&nbsp;')
        j3pAffiche(tt1[0][2], null, '&nbsp;' + stor.Lexo.pb.unit)
        stor.lili1 = new ZoneStyleMathquill1(tt1[0][1], { restric: '0123456789 ,.', limite: 16, limitenb: 4, enter: () => { me.sectionCourante() } })
        const et2 = stor.Lexo.donnees ? stor.Lexo.pb.et2 : stor.listVal[stor.listVal.length - 1].val
        const et1 = stor.Lexo.donnees ? stor.Lexo.pb.et1 : stor.listVal[0].val
        stor.garderep = j3pArrondi(et2 - et1, 2)
        stor.douko = tt1[0][1]
        stor.doukoul = tt1[0][0].parentNode.parentNode
        stor.aAMouv.push(tt1[0][0].parentNode.parentNode)
        for (let i = stor.aTrans.length - 1; i > -1; i--) {
          stor.aAMouv.push(stor.aTrans.pop())
        }
      }
        break
      case 'JusMoyenne': {
        laquest = '<b><u>Choisis le bon calcul pour la moyenne.</u></b>'
        const tt1 = addDefaultTable(stor.lesdiv.travail, 1, 2)
        modif(tt1)
        j3pAffiche(tt1[0][0], null, 'Moyenne =&nbsp;')
        stor.tt1 = tt1
        faisListeMoyenne()
      }
        break
      case 'Moyenne': {
        laquest = '<b><u>Calcule la moyenne.</u></b>'
        const tt1 = addDefaultTable(stor.lesdiv.travail, 1, 4)
        modif(tt1)
        j3pAffiche(tt1[0][2], null, '&nbsp;' + stor.Lexo.pb.unit)
        stor.lili1 = new ZoneStyleMathquill1(tt1[0][1], { restric: '0123456789 ,.', limite: 16, limitenb: 4, enter: () => { me.sectionCourante() } })
        let cim
        if (stor.Lexo.donnees) {
          cim = stor.leseff[0] * stor.Lexo.pb.cc[0]
          for (let i = 1; i < stor.leseff.length; i++) {
            cim += stor.leseff[i] * stor.Lexo.pb.cc[i]
          }
        } else {
          cim = stor.listVal[0].val
          for (let i = 1; i < stor.listVal.length; i++) {
            cim += stor.listVal[i].val
          }
        }
        const acomp = j3pArrondi(cim / stor.tot, 10)
        cim = j3pArrondi(cim / stor.tot, 2)
        stor.garderep = cim
        if (acomp === cim) {
          stor.foax = false
          j3pAffiche(tt1[0][0], null, 'Moyenne =&nbsp;')
        } else {
          j3pAffiche(tt1[0][0], null, 'Moyenne $\\approx$&nbsp;')
          j3pAffiche(tt1[0][3], null, '&nbsp;&nbsp;(valeur approchée au centième)')
          stor.foax = true
        }
        stor.douko = tt1[0][1]
        stor.doukoul = tt1[0][0].parentNode.parentNode
        stor.aAMouv.push(tt1[0][0].parentNode.parentNode)
        for (let i = stor.aTrans.length - 1; i > -1; i--) {
          stor.aAMouv.push(stor.aTrans.pop())
        }
      }
        break
    }
    j3pEmpty(stor.lesdiv.etape)
    j3pAffiche(stor.lesdiv.etape, null, laquest)
    stor.doukoul.classList.add('travail')
  }
  function faisListeMoyenne () {
    let rep1, rep2, rep4, rep3
    if (stor.Lexo.donnees) {
      rep1 = ecrisBienMathquill(stor.leseff[0]) + ' \\times ' + ecrisBienMathquill(stor.Lexo.pb.cc[0])
      rep2 = rep1
      rep4 = ecrisBienMathquill(stor.Lexo.pb.cc[0])
      for (let i = 1; i < stor.leseff.length; i++) {
        rep1 += ' + ' + ecrisBienMathquill(stor.leseff[i]) + ' \\times ' + ecrisBienMathquill(stor.Lexo.pb.cc[i])
        rep2 += ' - ' + ecrisBienMathquill(stor.leseff[i]) + ' \\times ' + ecrisBienMathquill(stor.Lexo.pb.cc[i])
        rep4 += ' + ' + ecrisBienMathquill(stor.Lexo.pb.cc[i])
      }
      rep3 = '\\frac{' + rep1 + '}{' + ecrisBienMathquill(stor.leseff.length) + '}'
      rep1 = '\\frac{' + rep1 + '}{' + ecrisBienMathquill(stor.tot) + '}'
      rep2 = '\\frac{' + rep2 + '}{' + ecrisBienMathquill(stor.tot) + '}'
      rep4 = '\\frac{' + rep4 + '}{' + ecrisBienMathquill(stor.leseff.length) + '}'
    } else {
      if (stor.lesvalranggge) {
        // ici
        rep1 = '\\frac{' + ecrisBienMathquill(stor.listVal[0].val) + ' + ' + ecrisBienMathquill(stor.listVal[1].val) + ' + \\text{...} + ' + ecrisBienMathquill(stor.listVal[stor.listVal.length - 2].val) + ' + ' + ecrisBienMathquill(stor.listVal[stor.listVal.length - 1].val) + '}{' + stor.tot + '}'
        rep2 = '\\frac{' + ecrisBienMathquill(stor.listVal[0].val) + ' - ' + ecrisBienMathquill(stor.listVal[1].val) + ' - \\text{...} - ' + ecrisBienMathquill(stor.listVal[stor.listVal.length - 2].val) + ' - ' + ecrisBienMathquill(stor.listVal[stor.listVal.length - 1].val) + '}{' + stor.tot + '}'
        rep3 = '\\frac{' + ecrisBienMathquill(stor.listVal[0].val) + ' + ' + ecrisBienMathquill(stor.listVal[1].val) + ' + \\text{...} + ' + ecrisBienMathquill(stor.listVal[stor.listVal.length - 2].val) + ' + ' + ecrisBienMathquill(stor.listVal[stor.listVal.length - 1].val) + '}{100}'
        rep4 = '\\frac{' + ecrisBienMathquill(stor.listVal[0].val) + ' \\times ' + ecrisBienMathquill(stor.listVal[1].val) + ' \\times \\text{...} \\times ' + ecrisBienMathquill(stor.listVal[stor.listVal.length - 2].val) + ' \\times ' + ecrisBienMathquill(stor.listVal[stor.listVal.length - 1].val) + '}{100}'
      } else {
        rep1 = '\\frac{' + ecrisBienMathquill(stor.listVal2[0].val) + ' + ' + ecrisBienMathquill(stor.listVal2[1].val) + ' + \\text{...} + ' + ecrisBienMathquill(stor.listVal2[stor.listVal2.length - 2].val) + ' + ' + ecrisBienMathquill(stor.listVal2[stor.listVal2.length - 1].val) + '}{' + stor.tot + '}'
        rep2 = '\\frac{' + ecrisBienMathquill(stor.listVal2[0].val) + ' - ' + ecrisBienMathquill(stor.listVal2[1].val) + ' - \\text{...} - ' + ecrisBienMathquill(stor.listVal2[stor.listVal2.length - 2].val) + ' - ' + ecrisBienMathquill(stor.listVal2[stor.listVal2.length - 1].val) + '}{' + stor.tot + '}'
        rep3 = '\\frac{' + ecrisBienMathquill(stor.listVal2[0].val) + ' + ' + ecrisBienMathquill(stor.listVal2[1].val) + ' + \\text{...} + ' + ecrisBienMathquill(stor.listVal2[stor.listVal2.length - 2].val) + ' + ' + ecrisBienMathquill(stor.listVal2[stor.listVal2.length - 1].val) + '}{100}'
        rep4 = '\\frac{' + ecrisBienMathquill(stor.listVal2[0].val) + ' \\times ' + ecrisBienMathquill(stor.listVal2[1].val) + ' \\times \\text{...} \\times ' + ecrisBienMathquill(stor.listVal2[stor.listVal2.length - 2].val) + ' \\times ' + ecrisBienMathquill(stor.listVal2[stor.listVal2.length - 1].val) + '}{100}'
      }
    }
    const ll = j3pShuffle(['$' + rep1 + '$', '$' + rep2 + '$', '$' + rep3 + '$', '$' + rep4 + '$'])
    ll.splice(0, 0, 'Choisir')
    stor.lili1 = ListeDeroulante.create(stor.tt1[0][1], ll)
    stor.garderep = '$' + rep1 + '$'
    stor.douko = stor.tt1[0][1]
    stor.doukoul = stor.tt1[0][0].parentNode.parentNode
    stor.aTrans.push(stor.tt1[0][0].parentNode.parentNode)
  }
  function derangeVal () {
    stor.lesvalranggge = false
    stor.letab2[0][0].parentNode.parentNode.style.display = 'none'
    stor.bb2.style.display = 'none'
    stor.letab[0][0].parentNode.parentNode.style.display = ''
    stor.bb1.style.display = ''
    if (stor.etapeencours === 'JusMoyenne' && me.etat === 'correction') {
      stor.lili1.disable()
      j3pEmpty(stor.tt1[0][1])
      faisListeMoyenne()
    }
  }
  function rangeVal () {
    stor.lesvalranggge = true
    stor.letab2[0][0].parentNode.parentNode.style.display = ''
    stor.bb2.style.display = ''
    stor.letab[0][0].parentNode.parentNode.style.display = 'none'
    stor.bb1.style.display = 'none'
    if (stor.etapeencours === 'JusMoyenne' && me.etat === 'correction') {
      stor.lili1.disable()
      j3pEmpty(stor.tt1[0][1])
      faisListeMoyenne()
    }
  }
  function boutonfen () {
    j3pToggleFenetres('Calculatrice')
  }
  function suiv () {
    let i = stor.afaire.indexOf(stor.etapeencours)
    i++
    if (i >= stor.afaire.length) i = 0
    return stor.afaire[i]
  }

  function yaReponse () {
    switch (stor.etapeencours) {
      case 'JusEffectif_Total':
      case 'JusFrequences':
      case 'JusEtendue':
      case 'JusMoyenne':
        if (!stor.lili1.changed) {
          stor.lili1.focus()
          return false
        }
        return true
      case 'Effectif_Total':
      case 'Frequences':
      case 'Mediane':
      case 'Etendue':
      case 'Moyenne':
        if (stor.lili1.reponse() === '') {
          stor.lili1.focus()
          return false
        }
        return true
      case 'JusMediane1':
        if (stor.Lexo.donnees) {
          for (let i = 0; i < stor.demCum.length; i++) {
            if (stor.demCum[i].reponse() === '') {
              stor.demCum[i].focus()
              return false
            }
          }
        } else {
          if (!stor.lili1.changed) {
            stor.lili1.focus()
            return false
          }
        }
        return true
      case 'JusMediane2':
        if (stor.lili1.reponse() === '') {
          stor.lili1.focus()
          return false
        }
        if (stor.yaMM) {
          if (stor.lili2.reponse() === '') {
            stor.lili2.focus()
            return false
          }
        }
        return true
      case 'JusQuartiles':
        if (stor.lili1.reponse() === '') {
          stor.lili1.focus()
          return false
        }
        if (stor.lili2.reponse() === '') {
          stor.lili2.focus()
          return false
        }
        if (stor.yaMM) {
          if (stor.lili3.reponse() === '') {
            stor.lili3.focus()
            return false
          }
          if (stor.lili4.reponse() === '') {
            stor.lili4.focus()
            return false
          }
        }
        return true
      case 'Quartiles':
        if (stor.lili1.reponse() === '') {
          stor.lili1.focus()
          return false
        }
        if (stor.lili2.reponse() === '') {
          stor.lili2.focus()
          return false
        }
        return true
    }
  }
  function isRepOk () {
    j3pEmpty(stor.lesdiv.solution)
    j3pEmpty(stor.lesdiv.explications)
    stor.lesdiv.solution.classList.remove('correction')
    stor.lesdiv.explications.classList.remove('explique')
    stor.errEcrit = false
    stor.errEcritList = ''
    stor.errCal = false
    stor.errDepMoy = false

    switch (stor.etapeencours) {
      case 'JusEffectif_Total':
        return stor.lili1.reponse === stor.garderep
      case 'Effectif_Total': {
        const hj = stor.lili1.reponse()
        const jj = verifNombreBienEcrit(hj, false, true)
        if (!jj.good) {
          stor.lili1.corrige(false)
          stor.errEcrit = true
          stor.errEcritList = jj.remede
          return false
        }
        if (jj.nb !== stor.tot) {
          stor.lili1.corrige(false)
          return false
        }
        return true
      }
      case 'JusFrequences':
        return stor.lili1.reponse === '$' + stor.garderep + '$'
      case 'Frequences': {
        const hj = stor.lili1.reponse().replace(/ /g, '')
        let jj
        if (hj.indexOf('frac') === -1) {
          jj = verifNombreBienEcrit(hj, false, true)
          if (!jj.good) {
            stor.lili1.corrige(false)
            stor.errEcrit = true
            stor.errEcritList = jj.remede
            return false
          }
        } else {
          jj = { nb: hj }
        }
        if (jj.nb !== stor.garderep) {
          stor.lili1.corrige(false)
          return false
        }
        return true
      }
      case 'JusMediane1': {
        if (stor.Lexo.donnees) {
          const tabrep = []
          for (let i = 0; i < stor.demCum.length; i++) {
            const kk = stor.demCum[i].reponse()
            const jj = verifNombreBienEcrit(kk, false, true)
            if (!jj.good) {
              stor.demCum[i].corrige(false)
              stor.errEcrit = true
              if (stor.errEcritList.indexOf(jj.remede) === -1) stor.errEcritList += jj.remede + '<br>'
            }
            tabrep.push(jj.nb)
          }
          if (stor.errEcrit) return false
          for (let i = 0; i < stor.demCum.length; i++) {
            if (tabrep[i] !== stor.garderep[i]) {
              stor.demCum[i].corrige(false)
              stor.errCal = true
            }
          }
          return !stor.errCal
        } else {
          stor.lili1.corrige(stor.lili1.reponse === stor.garderep)
          return stor.lili1.reponse === stor.garderep
        }
      }
      case 'JusMediane2': {
        const hj = stor.lili1.reponse()
        const jj = verifNombreBienEcrit(hj, false, true)
        if (!jj.good) {
          stor.lili1.corrige(false)
          stor.errEcrit = true
          stor.errEcritList = jj.remede
          return false
        }
        if (jj.nb !== stor.garderep && jj.nb !== stor.garderep + 1) {
          stor.lili1.corrige(false)
          return false
        }
        if (stor.yaMM) {
          const hj2 = stor.lili2.reponse()
          const jj2 = verifNombreBienEcrit(hj2, false, true)
          if (!jj2.good) {
            stor.lili2.corrige(false)
            stor.errEcrit = true
            stor.errEcritList = jj.remede
            return false
          }
          if (jj2.nb !== stor.garderep2) {
            stor.lili2.corrige(false)
            return false
          }
        }
        return true
      }
      case 'Mediane': {
        const hj = stor.lili1.reponse()
        const jj = verifNombreBienEcrit(hj, false, true)
        if (!jj.good) {
          stor.lili1.corrige(false)
          stor.errEcrit = true
          stor.errEcritList = jj.remede
          return false
        }
        if (stor.yaMM) {
          if (jj.nb > stor.garderep3 || jj.nb < stor.garderep2) {
            stor.lili1.corrige(false)
            return false
          }
        } else {
          if (jj.nb !== stor.garderep) {
            stor.lili1.corrige(false)
            return false
          }
        }
        return true
      }
      case 'JusQuartiles': {
        let ok1 = true
        let ok2 = true
        const hj = stor.lili1.reponse()
        const jj = verifNombreBienEcrit(hj, false, true)
        if (!jj.good) {
          stor.lili1.corrige(false)
          stor.errEcrit = true
          stor.errEcritList = jj.remede
          ok1 = false
        }
        const hj2 = stor.lili2.reponse()
        const jj2 = verifNombreBienEcrit(hj2, false, true)
        if (!jj2.good) {
          stor.lili2.corrige(false)
          stor.errEcrit = true
          stor.errEcritList = jj2.remede
          ok2 = false
        }
        if (stor.yaMM) {
          const hj3 = stor.lili3.reponse()
          const jj3 = verifNombreBienEcrit(hj3, false, true)
          if (!jj3.good) {
            stor.lili3.corrige(false)
            stor.errEcrit = true
            stor.errEcritList = jj3.remede
            ok1 = false
          }
          const hj4 = stor.lili4.reponse()
          const jj4 = verifNombreBienEcrit(hj4, false, true)
          if (!jj4.good) {
            stor.lili4.corrige(false)
            stor.errEcrit = true
            stor.errEcritList = jj4.remede
            ok2 = false
          }
          if (ok1) {
            if (jj3.nb !== stor.garderep3) {
              stor.lili3.corrige(false)
              ok1 = false
            }
          }
          if (ok2) {
            if (jj4.nb !== stor.garderep4) {
              stor.lili4.corrige(false)
              ok2 = false
            }
          }
        } else {
          if (ok1) {
            if (jj.nb !== stor.garderep) {
              stor.lili1.corrige(false)
              ok1 = false
            }
          }
          if (ok2) {
            if (jj2.nb !== stor.garderep2) {
              stor.lili2.corrige(false)
              ok2 = false
            }
          }
        }
        return ok1 && ok2
      }
      case 'Quartiles': {
        let ok1 = true
        let ok2 = true
        const hj = stor.lili1.reponse()
        const jj = verifNombreBienEcrit(hj, false, true)
        if (!jj.good) {
          stor.lili1.corrige(false)
          stor.errEcrit = true
          stor.errEcritList = jj.remede
          ok1 = false
        }
        const hj2 = stor.lili2.reponse()
        const jj2 = verifNombreBienEcrit(hj2, false, true)
        if (!jj2.good) {
          stor.lili2.corrige(false)
          stor.errEcrit = true
          stor.errEcritList = jj2.remede
          ok2 = false
        }
        if (stor.yaMM) {
          if (ok1) {
            if (jj.nb > stor.garderep12 || jj.nb < stor.garderep11) {
              stor.lili1.corrige(false)
              ok1 = false
            }
          }
          if (ok2) {
            if (jj2.nb > stor.garderep22 || jj2.nb < stor.garderep21) {
              stor.lili2.corrige(false)
              ok2 = false
            }
          }
        } else {
          if (ok1) {
            if (jj.nb !== stor.garderep) {
              stor.lili1.corrige(false)
              ok1 = false
            }
          }
          if (ok2) {
            if (jj2.nb !== stor.garderep2) {
              stor.lili2.corrige(false)
              ok2 = false
            }
          }
        }
        return ok1 && ok2
      }
      case 'JusEtendue':
        return stor.lili1.reponse === stor.garderep
      case 'Etendue': {
        const hj = stor.lili1.reponse()
        const jj = verifNombreBienEcrit(hj, false, true)
        if (!jj.good) {
          stor.lili1.corrige(false)
          stor.errEcrit = true
          stor.errEcritList = jj.remede
          return false
        }
        if (jj.nb !== stor.garderep) {
          stor.lili1.corrige(false)
          return false
        }
        return true
      }
      case 'JusMoyenne':
        return stor.lili1.reponse === stor.garderep
      case 'Moyenne': {
        const hj = stor.lili1.reponse()
        const jj = verifNombreBienEcrit(hj, false, true)
        if (!jj.good) {
          stor.lili1.corrige(false)
          stor.errEcrit = true
          stor.errEcritList = jj.remede
          return false
        }
        if (jj.nb !== stor.garderep) {
          stor.lili1.corrige(false)
          if (jj.nb < stor.Lexo.pb.minmoy || jj.nb > stor.Lexo.pb.maxmoy) {
            stor.errDepMoy = true
          }
          return false
        }
        return true
      }
    }
  } // isRepOk
  function desactiveAll () {
    switch (stor.etapeencours) {
      case 'JusEffectif_Total':
      case 'Effectif_Total':
      case 'Frequences':
      case 'JusFrequences':
      case 'Moyenne':
      case 'JusEtendue':
      case 'Etendue':
      case 'JusMoyenne':
      case 'Mediane':
        stor.lili1.disable()
        break
      case 'JusMediane1':
        if (stor.Lexo.donnees) {
          for (let i = 0; i < stor.demCum.length; i++) {
            stor.demCum[i].disable()
          }
        } else {
          stor.lili1.disable()
        }
        break
      case 'JusMediane2':
        stor.lili1.disable()
        if (stor.yaMM) stor.lili2.disable()
        break
      case 'JusQuartiles':
        stor.lili1.disable()
        stor.lili2.disable()
        if (stor.yaMM) {
          stor.lili3.disable()
          stor.lili4.disable()
        }
        break
      case 'Quartiles':
        stor.lili1.disable()
        stor.lili2.disable()
        break
    }
  } // desactiveAll
  function passeToutVert () {
    switch (stor.etapeencours) {
      case 'JusEffectif_Total':
      case 'JusFrequences':
      case 'Frequences':
      case 'Mediane':
      case 'JusEtendue':
      case 'Etendue':
      case 'JusMoyenne':
      case 'Moyenne':
      case 'Effectif_Total':
        stor.lili1.corrige(true)
        break
      case 'JusMediane1':
        if (stor.Lexo.donnees) {
          for (let i = 0; i < stor.demCum.length; i++) {
            stor.demCum[i].corrige(true)
          }
        } else {
          stor.lili1.corrige(true)
        }
        break
      case 'JusMediane2':
        stor.lili1.corrige(true)
        if (stor.yaMM) stor.lili2.corrige(true)
        break
      case 'JusQuartiles':
        stor.lili1.corrige(true)
        stor.lili2.corrige(true)
        if (stor.yaMM) {
          stor.lili3.corrige(true)
          stor.lili4.corrige(true)
        }
        break
      case 'Quartiles':
        stor.lili1.corrige(true)
        stor.lili2.corrige(true)
    }
  } // passeToutVert
  function barrelesfo () {
    switch (stor.etapeencours) {
      case 'JusEffectif_Total':
      case 'JusFrequences':
      case 'Frequences':
      case 'Mediane':
      case 'JusEtendue':
      case 'Etendue':
      case 'JusMoyenne':
      case 'Moyenne':
      case 'Effectif_Total':
        stor.lili1.barre()
        break
      case 'JusMediane1':
        if (stor.Lexo.donnees) {
          for (const zone of stor.demCum) zone.barreIfKo()
        } else {
          stor.lili1.barre()
        }
        break
      case 'JusMediane2':
        stor.lili1.barreIfKo()
        if (stor.yaMM) {
          stor.lili2.barreIfKo()
        }
        break
      case 'JusQuartiles':
        stor.lili1.barreIfKo()
        stor.lili2.barreIfKo()
        if (stor.yaMM) {
          stor.lili3.barreIfKo()
          stor.lili4.barreIfKo()
        }
        break
      case 'Quartiles':
        stor.lili1.barreIfKo()
        stor.lili2.barreIfKo()
        break
    }
  }
  function affCorrFaux (isFin) {
    if (stor.errEcrit) {
      j3pAffiche(stor.lesdiv.explications, null, stor.errEcritList)
      stor.yaexplik = true
    }
    if (stor.errDepMoy) {
      stor.yaexplik = true
      j3pAffiche(stor.lesdiv.explications, null, 'La moyenne doit être comprise en entre $' + stor.Lexo.pb.minmoy + '$ et $' + String(stor.Lexo.pb.maxmoy).replace('.', ',') + '$ ' + stor.Lexo.pb.unit)
    }
    switch (stor.etapeencours) {
      case 'JusEffectif_Total':
        stor.lili1.corrige(false)
        stor.yaexplik = true
        if (stor.Lexo.donnees) {
          j3pAffiche(stor.lesdiv.explications, null, 'L’effectif total est la somme des effectifs de la série.')
        } else {
          j3pAffiche(stor.lesdiv.explications, null, 'L’effectif total est égal au nombre de valeurs de la série.')
        }
        break
      case 'JusFrequences':
        stor.lili1.corrige(false)
        stor.yaexplik = true
        if (stor.Lexo.freq !== 'pourcentage') {
          j3pAffiche(stor.lesdiv.explications, null, 'La fréquence d’une valeur est le quotient de son effectif par l’effectif total.')
        } else {
          j3pAffiche(stor.lesdiv.explications, null, 'Pour obtenir la fréquence en pourcentage, il suffit de multiplier la fréquence par 100.')
        }
        break
      case 'JusMediane1':
        if (stor.Lexo.donnees) {
          stor.yaexplik = true
          j3pAffiche(stor.lesdiv.explications, null, 'Pour obtenir les effectifs cumulés, \nil faut ajouter les effectifs des valeurs inférieures ou égales à la valeur donnée.')
        }
        break
      case 'JusMediane2':
        stor.yaexplik = true
        stor.lili1.corrige(false)
        j3pAffiche(stor.lesdiv.explications, null, 'Il faut diviser l’effectif total par 2 !')
        break
      case 'JusEtendue':
        stor.yaexplik = true
        stor.lili1.corrige(false)
        j3pAffiche(stor.lesdiv.explications, null, 'Il faut calculer la différences entre les valeurs extrèmes !')
        break
      case 'JusMoyenne':
        stor.yaexplik = true
        stor.lili1.corrige(false)
        j3pAffiche(stor.lesdiv.explications, null, 'La moyenne est égale à la somme des valeurs divisée par l’effectif total !')
        break
    }
    if (isFin) {
      barrelesfo()
      desactiveAll()
      stor.yaco = true
      switch (stor.etapeencours) {
        case 'JusEffectif_Total':
          if (stor.Lexo.donnees) {
            j3pAffiche(stor.lesdiv.solution, null, 'Effectif total = ' + stor.garderep)
          } else {
            j3pAffiche(stor.lesdiv.solution, null, 'Pour obtenir l’effectif total, ' + stor.garderep)
          }
          stor.yacoef = true
          break
        case 'Effectif_Total':
          if (stor.afaire.indexOf('JusEffectif_Total') === -1) {
            let rep1 = ecrisBienMathquill(stor.leseff[0])
            for (let i = 1; i < stor.leseff.length; i++) {
              rep1 += ' + ' + ecrisBienMathquill(stor.leseff[i])
            }
            j3pAffiche(stor.lesdiv.solution, null, 'Effectif total = $' + rep1 + '$\n')
          }
          j3pAffiche(stor.lesdiv.solution, null, 'Effectif total = $' + ecrisBienMathquill(stor.tot) + '$')
          stor.yacoef2 = true
          break
        case 'JusFrequences':
          j3pAffiche(stor.lesdiv.solution, null, stor.souv + '$ = ' + stor.garderep + '$')
          stor.yacoeffreq = true
          break
        case 'Frequences': {
          if (!stor.Lexo.donnees) {
            for (let i = 0; i < stor.gardeLum.length; i++) stor.gardeLum[i].style.color = '#0F0'
          }
          if (stor.afaire.indexOf('JusFrequences') === -1) {
            if (stor.Lexo.freq === 'pourcentage') {
              j3pAffiche(stor.lesdiv.solution, null, stor.souv = ' = $\\frac{ \\text{effectif} }{ \\text{effectif total} } \\times 100$\n')
            } else {
              j3pAffiche(stor.lesdiv.solution, null, stor.souv = ' = $\\frac{ \\text{effectif} }{ \\text{effectif total} }$\n')
            }
          }
          const bub = (stor.Lexo.freq === 'pourcentage') ? '%' : ''
          j3pAffiche(stor.lesdiv.solution, null, stor.souv + ' = $' + stor.garderep + '$' + bub)
          stor.yacoeffreq2 = true
          break
        }
        case 'JusMediane1':
          if (stor.Lexo.donnees) {
            const tr = document.createElement('tr')
            const cells = []
            for (let i = 0; i < stor.leseff.length + 2; i++) {
              cells.push(j3pAddElt(tr, 'td'))
              cells[cells.length - 1].style.border = '1px solid black'
              cells[cells.length - 1].style.padding = '0px 3px 0px 3px'
              cells[cells.length - 1].style.textAlign = 'center'
              cells[cells.length - 1].style.color = me.styles.petit.correction.color
            }
            stor.cellMed[0].parentNode.parentNode.insertBefore(tr, stor.cellMed[0].parentNode.nextSibling)
            j3pAffiche(cells[0], null, '<b>eff. cumul. crois.</b>')
            let cum = 0
            for (let i = 0; i < stor.leseff.length; i++) {
              cum += stor.leseff[i]
              j3pAffiche(cells[1 + i], null, '$' + ecrisBienMathquill(cum) + '$')
            }
            stor.focoMed1 = true
            stor.doukoul = tr
          } else {
            stor.focoMed1 = true
            j3pAffiche(stor.lesdiv.solution, null, 'Pour obtenir la médiane (ou les quartiles), ' + stor.garderep)
          }
          break
        case 'JusMediane2':
          stor.focoMed2 = true
          j3pAffiche(stor.lesdiv.solution, null, '$' + ecrisBienMathquill(stor.tot) + ' \\div 2 = ' + ecrisBienMathquill(stor.tot / 2) + '$\n')
          if (stor.yaMM) {
            j3pAffiche(stor.lesdiv.solution, null, ' donc une médiane doit être comprise entre la $' + ecrisBienMathquill(Math.ceil(stor.tot / 2)) + '$ <sup>ème</sup> et la $' + ecrisBienMathquill(Math.ceil(stor.tot / 2) + 1) + '$ <sup>ème</sup> valeur.')
          } else {
            j3pAffiche(stor.lesdiv.solution, null, ' donc ' + ((stor.tot % 2 === 1) ? 'la' : 'une valeur ') + ' médiane correspond à la $' + ecrisBienMathquill(Math.ceil(stor.tot / 2)) + '$ <sup>ème</sup> valeur.')
          }
          break
        case 'Mediane':
          stor.focoMed3 = true
          j3pAffiche(stor.lesdiv.solution, null, 'Médiane = $' + ecrisBienMathquill(stor.garderep) + '$ ' + stor.Lexo.pb.unit)
          break
        case 'JusQuartiles':
          stor.focoQuart1 = true
          if (!stor.yaMM) {
            j3pAffiche(stor.lesdiv.solution, null, '$' + stor.tot + ' \\div 4 = ' + (stor.tot / 4) + '$\n')
            j3pAffiche(stor.lesdiv.solution, null, ' donc le 1<sup>er</sup> quartile correspond à la $' + ecrisBienMathquill(Math.ceil(stor.tot / 4)) + '$ <sup>ème</sup> valeur.\n')
            j3pAffiche(stor.lesdiv.solution, null, '$\\frac{3}{4} \\times ' + stor.tot + ' = ' + (3 * stor.tot / 4) + '$\n')
            j3pAffiche(stor.lesdiv.solution, null, ' donc le 2<sup>ème</sup> quartile correspond à la $' + ecrisBienMathquill(Math.ceil(3 * stor.tot / 4)) + '$ <sup>ème</sup> valeur.')
          } else {
            j3pAffiche(stor.lesdiv.solution, null, '$' + stor.tot + ' \\div 4 = ' + (stor.tot / 4) + '$\n')
            const i1 = Math.ceil(stor.tot / 4)
            const i2 = Math.ceil(3 * stor.tot / 4)
            j3pAffiche(stor.lesdiv.solution, null, ' donc le 1<sup>er</sup> quartile est une valeur comprise entre la $' + ecrisBienMathquill(i1) + '$ <sup>ème</sup> et la $' + ecrisBienMathquill(i1 + 1) + '$ <sup>ème</sup> valeur.\n')
            j3pAffiche(stor.lesdiv.solution, null, '$\\frac{3}{4} \\times ' + stor.tot + ' = ' + (3 * stor.tot / 4) + '$\n')
            j3pAffiche(stor.lesdiv.solution, null, ' donc le 2<sup>ème</sup> quartile est une valeur comprise entre la $' + ecrisBienMathquill(i2) + '$ <sup>ème</sup> et la $' + ecrisBienMathquill(i2 + 1) + '$ <sup>ème</sup> valeur.')
          }
          break
        case 'Quartiles':
          stor.focoQuart2 = true
          j3pAffiche(stor.lesdiv.solution, null, '1<sup>er</sup> quartile = $' + ecrisBienMathquill(stor.garderep) + '$ ' + stor.Lexo.pb.unit + '\n')
          j3pAffiche(stor.lesdiv.solution, null, '2<sup>ème</sup> quartile = $' + ecrisBienMathquill(stor.garderep2) + '$ ' + stor.Lexo.pb.unit)
          break
        case 'JusEtendue':
          stor.focoEten1 = true
          j3pAffiche(stor.lesdiv.solution, null, 'Etendue = ' + stor.garderep)
          break
        case 'Etendue':
          stor.focoEten2 = true
          j3pAffiche(stor.lesdiv.solution, null, 'Etendue = $' + ecrisBienMathquill(stor.garderep) + '$ ' + stor.Lexo.pb.unit)
          break
        case 'JusMoyenne':
          stor.focoMoy1 = true
          if (stor.Lexo.donnees) {
            if (stor.Lexo.pb.cla) {
              j3pAffiche(stor.lesdiv.solution, null, 'Il faut utiliser les centres des classes.\n')
            }
          }
          j3pAffiche(stor.lesdiv.solution, null, 'Moyenne = ' + stor.garderep + ' ' + stor.Lexo.pb.unit)
          break
        case 'Moyenne': {
          stor.focoMoy2 = true
          if (stor.afaire.indexOf('Effectif_Total') === -1) {
            j3pAffiche(stor.lesdiv.solution, null, 'Effectif total = $' + stor.tot + '$ ' + '\n')
          }
          if (stor.afaire.indexOf('JusMoyenne') === -1) {
            let rep1
            if (stor.Lexo.donnees) {
              if (stor.Lexo.pb.cla) {
                j3pAffiche(stor.lesdiv.solution, null, 'Il faut utiliser les centres des classes.\n')
              }
              rep1 = ecrisBienMathquill(stor.leseff[0]) + ' \\times ' + stor.Lexo.pb.cc[0]
              for (let i = 1; i < stor.leseff.length; i++) {
                rep1 += ' + ' + ecrisBienMathquill(stor.leseff[i]) + ' \\times ' + stor.Lexo.pb.cc[i]
              }
              rep1 = '\\frac{' + rep1 + '}{' + stor.tot + '}'
            } else {
              rep1 = '\\frac{' + ecrisBienMathquill(stor.listVal2[0].val) + ' + ' + ecrisBienMathquill(stor.listVal2[1].val) + ' + \\text{...} + ' + ecrisBienMathquill(stor.listVal2[stor.listVal2.length - 2].val) + ' + ' + ecrisBienMathquill(stor.listVal2[stor.listVal2.length - 1].val) + '}{' + stor.tot + '}'
            }
            j3pAffiche(stor.lesdiv.solution, null, 'Moyenne = $' + rep1 + '$ ' + '\n')
          }
          const hjhj = stor.foax ? '$\\approx$' : '='
          j3pAffiche(stor.lesdiv.solution, null, 'Moyenne ' + hjhj + ' $' + ecrisBienMathquill(stor.garderep) + '$ ' + stor.Lexo.pb.unit)
          break
        }
      }
    }
  } // affCorrFaux
  function enonceMain () {
    if (me.etapeCourante === 1) {
      me.videLesZones()
      creeLesDiv()
      if (ds.Calculatrice) {
        me.fenetresjq = [{ name: 'Calculatrice', title: 'Une calculatrice', left: 600, top: 10 }]
        j3pCreeFenetres(me)
        j3pAfficheCroixFenetres('Calculatrice')
        j3pAjouteBouton(stor.lesdiv.calculatrice, 'Calculatrice', 'MepBoutons', 'Calculatrice', boutonfen)
      }
      stor.Lexo = faisChoixExos()
    }
    poseQuestion()
    me.afficheBoutonValider()
    me.finEnonce()
  }

  switch (this.etat) {
    case 'enonce':
      // code exécuté au lancement de la section
      if (me.debutDeLaSection) {
        initSection()
      } else {
        enonceMain()
      }
      break // case "enonce":

    case 'correction':
      // On teste si une réponse a été saisie
      stor.yaexplik = false
      stor.yaco = false
      stor.lesdiv.explications.innerHTML = ''
      stor.lesdiv.solution.innerHTML = ''
      stor.lesdiv.explications.classList.remove('explique')
      stor.lesdiv.solution.classList.remove('correction')
      if (yaReponse()) {
        // Bonne réponse
        if (isRepOk()) {
          stor.lesdiv.correction.style.color = this.styles.cbien
          stor.lesdiv.correction.innerHTML = cBien
          desactiveAll()
          passeToutVert()

          this.typederreurs[0]++
          this.score++
          this.sectionCourante('navigation')
        } else {
          // Pas de bonne réponse
          stor.lesdiv.correction.style.color = this.styles.cfaux
          stor.lesdiv.correction.innerHTML = cFaux
          if (this.isElapsed) {
            // A cause de la limite de temps :
            stor.lesdiv.correction.innerHTML += tempsDepasse + '<BR>'
            this.typederreurs[10]++
            affCorrFaux(true)
            this.sectionCourante('navigation')
          } else {
            // Réponse fausse :
            if (me.essaiCourant < me.donneesSection.nbchances) {
              stor.lesdiv.correction.innerHTML += '<br>' + essaieEncore

              affCorrFaux(false)

              // affCorrFaux.call(this, false)
              this.typederreurs[1]++
            } else {
              // Erreur au nème essai
              affCorrFaux(true)

              this.typederreurs[2]++
              this.sectionCourante('navigation')
            }
          }
        }
      } else {
        // pas de réponse
        stor.lesdiv.correction.style.color = this.styles.cfaux
        stor.lesdiv.correction.innerHTML = reponseIncomplete
        this.afficheBoutonValider()
      }

      if (stor.yaexplik) stor.lesdiv.explications.classList.add('explique')
      if (stor.yaco) {
        if (stor.etapeencours === 'JusMediane1') {
          stor.doukoul.classList.add('correction')
        } else {
          stor.lesdiv.solution.classList.add('correction')
        }
      }
      // Obligatoire
      this.finCorrection()
      break // case "correction":

    case 'navigation':
      if (!this.sectionTerminee()) {
        this.etat = 'enonce'
      }
      this.finNavigation()
      break // case "navigation":
  }
}
