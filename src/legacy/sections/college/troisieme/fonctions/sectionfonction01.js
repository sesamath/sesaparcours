import { j3pAddElt, j3pAjouteBouton, j3pEmpty, j3pGetNewId, j3pShowError, j3pShuffle } from 'src/legacy/core/functions'
import ListeDeroulante from 'src/legacy/outils/listeDeroulante/ListeDeroulante'
import { j3pAffiche } from 'src/lib/mathquill/functions'
import { addDefaultTable } from 'src/legacy/themes/table'
import ZoneStyleMathquill1 from 'src/legacy/outils/zoneStyleMathquill/ZoneStyleMathquill1'
import { j3pAfficheCroixFenetres, j3pCreeFenetres, j3pToggleFenetres } from 'src/legacy/core/functionsJqDialog'
import { j3pCreeSVG } from 'src/legacy/core/functionsSvg'
import { getMtgCore } from 'src/lib/outils/mathgraph'
import { figDef, figFonc, figGra, figTab } from './fonction01Datas'
import textesGeneriques from 'src/lib/core/textes'

const { cBien, cFaux, essaieEncore, reponseIncomplete, tempsDepasse } = textesGeneriques

/**
 * Ajouter une description ici
 * @fileOverview
 * @author Tommy Barroy <t.barroy@laposte.net>
 * @since 2017-10
 */
export const params = {
  outils: ['mathquill', 'calculatrice'],
  /**
   * Ce qui est paramétrable dans l’éditeur de graphe
   * @type {LegacySectionParamsParametres}
   */
  parametres: [
    ['nbrepetitions', 9, 'entier', 'Nombre de répétitions de la section'],
    ['nbchances', 2, 'entier', 'Nombre d’essais possibles.'],
    ['limite', 0, 'entier', 'Nombre d’essais possibles.'],
    ['typeDonnees', 'Les trois', 'liste', '<u>Graphe</u>:  On dispose d’un graphe de la fonction.  <br> <br> <u>Tableau</u>: On dispose d’un tableau de valeurs de la fonction. <br> <br> <u>Définition</u>: On dispose d’une définition de la fonction du type <i>f: x -> f(x)</i> . ', ['Les trois', 'Graphe', 'Tableau', 'Définition', 'Graphe_Tableau', 'Graphe_Définition', 'Tableau_Définition']],
    ['typeFonction', 'Tout', 'liste', '<u>Linéaire</u>: <i>f</i> est linéaire.  <br> <br> <u>Affine</u>: <i>f</i> est affine. <br> <br> <u>Tout</u>: <i>f</i> peut être une fonction polynôme.', ['Tout', 'Linéaire', 'Affine']],
    ['question', 'Les deux', 'liste', '<u>Image</u>: Il faut trouver l’image d’un nombre.  <br> <br> <u>Antédédent</u>: Il faut trouver un ou des antécédents d’un nombre.', ['Les deux', 'Image', 'Antécédent']],
    ['theme', 'standard', 'liste', 'Modèle pour l’affichage', ['standard', 'zonesAvecImageDeFond']]
  ]
}

/**
 * section fonction01
 * @this {Parcours}
 */
export default function main () {
  const me = this
  const stor = me.storage
  const ds = me.donneesSection

  function boutonfen () {
    j3pToggleFenetres('Calculatrice')
  }

  function initSection () {
    // Construction de la page
    me.construitStructurePage('presentation1bis')
    ds.lesExos = []

    let lp = []
    if (ds.typeDonnees.includes('Déf') || ds.typeDonnees === 'Les trois') lp.push('de la notation $f: x -> f(x)$')
    if (ds.typeDonnees.includes('Gra') || ds.typeDonnees === 'Les trois') lp.push('de la courbe d’une fonction')
    if (ds.typeDonnees.includes('Tab') || ds.typeDonnees === 'Les trois') lp.push('d’un tableau de valeurs d’une fonction')
    lp = j3pShuffle(lp)
    for (let i = 0; i < ds.nbitems; i++) {
      ds.lesExos.push({ donnee: lp[i % (lp.length)] })
    }
    ds.lesExos = j3pShuffle(ds.lesExos)

    let lp2 = []
    if (ds.question === 'Image' || ds.question === 'Les deux') lp2.push('Image')
    if (ds.question === 'Antécédent' || ds.question === 'Les deux') lp2.push('Antécédent(s)')
    lp2 = j3pShuffle(lp2)
    for (let i = 0; i < ds.nbitems; i++) {
      ds.lesExos[i].quest = lp2[i % (lp2.length)]
    }
    ds.lesExos = j3pShuffle(ds.lesExos)

    let foncPos = []
    switch (ds.typeFonction) {
      case 'Tout':
        for (let i = 1; i < 31; i++) foncPos.push(i)
        break
      case 'Affine':
        foncPos = [1, 2, 3, 4, 5, 6, 7, 8, 9, 10, 11, 12, 13, 14, 15, 16, 17, 19, 20, 21, 22, 28, 29, 30]
        break
      default: foncPos = [2, 3, 4, 5, 6, 28]
    }
    foncPos = j3pShuffle(foncPos)
    for (let i = 0; i < ds.nbitems; i++) {
      ds.lesExos[i].fonc = foncPos[i % (foncPos.length)]
    }
    ds.lesExos = j3pShuffle(ds.lesExos)

    me.typederreurs = [0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0]
    let bufdeb = 'Image et antécédent(s) '
    if (lp2.length === 1) bufdeb = lp2[0] + ' '
    let buffin = ''
    if (lp.length === 1) buffin = 'à partir ' + lp[0]

    me.afficheTitre(bufdeb + buffin)
    getMtgCore({ withMathJax: true })
      .then(
        // success
        (mtgAppLecteur) => {
          stor.mtgAppLecteur = mtgAppLecteur
          enonceMain()
        },
        // failure
        (error) => {
          j3pShowError(error, { message: 'mathgraph n’est pas correctement chargé', mustNotify: true })
        })
      // plantage dans enonceMain
      .catch(j3pShowError)
  }
  function creeLesDiv () {
    stor.lesdiv = {}
    stor.lesdiv.conteneur = j3pAddElt(me.zonesElts.MG, 'div', {
      style: me.styles.etendre('toutpetit.enonce', { padding: '10px' })
    })
    stor.lesdiv.consigne = addDefaultTable(stor.lesdiv.conteneur, 1, 1)[0][0]
    stor.lesdiv.calculatrice = j3pAddElt(me.zonesElts.MD, 'div', {
      style: me.styles.etendre('toutpetit.enonce', { padding: '10px' })
    })
    stor.lesdiv.correction = j3pAddElt(me.zonesElts.MD, 'div', {
      style: me.styles.etendre('toutpetit.enonce', { padding: '10px' })
    })
    stor.lesdiv.travail = addDefaultTable(stor.lesdiv.conteneur, 1, 1)[0][0]
    stor.lesdiv.explications = j3pAddElt(stor.lesdiv.conteneur, 'div')
    stor.lesdiv.solution = j3pAddElt(stor.lesdiv.conteneur, 'div')
    stor.lesdiv.divCache = j3pAddElt(stor.lesdiv.conteneur, 'div')
    stor.lesdiv.divCache.style.display = 'none'
    stor.lesdiv.travail.classList.add('travail')
    stor.lesdiv.consigne.classList.add('enonce')
    me.zonesElts.MG.classList.add('fond')
    stor.lesdiv.solution.style.color = me.styles.cbien
    stor.lesdiv.explications.style.color = me.styles.cfaux
  }
  function faisChoixExos () {
    return ds.lesExos.pop()
  }
  function barreLesFo () {
    if (stor.Lexo.quest !== 'Image') {
      if (stor.liste.bon === false) {
        stor.liste.barre()
      }
      for (let i = 0; i < stor.listRep.length; i++) {
        if (stor.listRep[i].bon === false) {
          stor.listRep[i].barre()
        }
      }
    }
  }
  function enonceMain () {
    me.videLesZones()
    creeLesDiv()
    stor.Lexo = faisChoixExos()
    poseQuestion()
    me.fenetresjq = [{ name: 'Calculatrice', title: 'Une calculatrice', left: 600, top: 10 }]
    j3pCreeFenetres(me)
    j3pAfficheCroixFenetres('Calculatrice')
    j3pAjouteBouton(stor.lesdiv.calculatrice, boutonfen, { value: 'calculatrice', className: 'MepBoutons' })
    me.finEnonce()
  }
  function isRepOk () {
    stor.errDouble = false
    if (stor.Lexo.quest === 'Image') {
      const repEl = stor.zoneI.reponse()
      return Number(repEl) === stor.imAt
    } else {
      let ok1 = true
      if (stor.ya2) {
        if (stor.ya3 && stor.liste.reponse !== '4 (ou plus)') {
          stor.liste.corrige(false)
          ok1 = false
        }
        if (!stor.ya3 && stor.liste.reponse !== '2') {
          stor.liste.corrige(false)
          ok1 = false
        }
      }
      if (!stor.ya2 && stor.liste.reponse !== '1') {
        stor.liste.corrige(false)
        ok1 = false
      }
      if (ok1) stor.liste.corrige(true)
      const tabrep = []
      for (let i = 0; i < stor.listRep.length; i++) {
        tabrep.push(Number(stor.listRep[i].reponse()))
      }
      for (let i = 0; i < tabrep.length; i++) {
        if (!stor.antOk.includes(tabrep[i])) {
          stor.listRep[i].corrige(false)
          ok1 = false
        } else {
          let dej = false
          for (let j = 0; j < i; j++) {
            dej = dej || tabrep[j] === tabrep[i]
          }
          if (dej) {
            ok1 = false
            stor.listRep[i].corrige(false)
            stor.errDouble = true
          } else {
            stor.listRep[i].corrige(true)
          }
        }
      }
      return ok1
    }
  }
  function poseQuestion () {
    const tt = addDefaultTable(stor.lesdiv.consigne, 3, 1)
    let txtFigure = ''
    let wiwi = 250
    let hehe = 80
    // stor.Lexo.donnee = 'de la notation $f: x -> f(x)$'

    switch (stor.Lexo.donnee) {
      case 'de la notation $f: x -> f(x)$':
        j3pAffiche(tt[0][0], null, 'On considère la fonction $f$ définie par la notation suivante:&nbsp;')
        txtFigure = figDef
        break
      case 'de la courbe d’une fonction':
        j3pAffiche(tt[0][0], null, 'On considère la fonction $f$ définie par le graphe suivant:&nbsp;')
        txtFigure = figGra
        wiwi = 300
        hehe = 250
        break
      case 'd’un tableau de valeurs d’une fonction':
        j3pAffiche(tt[0][0], null, 'On considère la fonction $f$ définie par le tableau de valeurs suivant:&nbsp;')
        txtFigure = figTab
        wiwi = 320
        hehe = 170
        break
    }
    const svg2 = j3pGetNewId('mtg')
    const yy = j3pCreeSVG(stor.lesdiv.divCache, {
      id: svg2,
      width: wiwi,
      height: hehe
    })
    yy.style.border = '1px solid black'
    stor.mtgAppLecteur.removeAllDoc()
    stor.mtgAppLecteur.addDoc(svg2, figFonc, true)
    stor.mtgAppLecteur.giveFormula2(svg2, 'A', stor.Lexo.fonc)
    stor.svgId = j3pGetNewId('mtg')
    const yy2 = j3pCreeSVG(tt[1][0], {
      id: stor.svgId,
      width: wiwi,
      height: hehe
    })
    yy2.style.border = '1px solid black'
    stor.mtgAppLecteur.addDoc(stor.svgId, txtFigure, true)
    stor.mtgAppLecteur.giveFormula2(stor.svgId, 'A', stor.Lexo.fonc)
    stor.mtgAppLecteur.calculateAndDisplayAll(true)
    stor.val1 = stor.mtgAppLecteur.valueOf(svg2, 'rep2')
    stor.val2 = stor.mtgAppLecteur.valueOf(svg2, 'rep3')
    stor.val3 = stor.mtgAppLecteur.valueOf(svg2, 'rep4')
    stor.val4 = stor.mtgAppLecteur.valueOf(svg2, 'rep5')
    stor.val5 = stor.mtgAppLecteur.valueOf(svg2, 'rep6')
    let numpos = [-2, -1, 0, 1, 2]
    if (stor.Lexo.donnee === 'de la courbe d’une fonction') {
      numpos = []
      if (Number(stor.val1) >= -3 && Number(stor.val1) <= 3) numpos.push(-2)
      if (Number(stor.val2) >= -3 && Number(stor.val2) <= 3) numpos.push(-1)
      if (Number(stor.val3) >= -3 && Number(stor.val3) <= 3) numpos.push(0)
      if (Number(stor.val4) >= -3 && Number(stor.val4) <= 3) numpos.push(1)
      if (Number(stor.val5) >= -3 && Number(stor.val5) <= 3) numpos.push(2)
    }
    const foncPos = j3pShuffle(numpos)
    stor.Lexo.lenb = foncPos.pop()
    switch (stor.Lexo.lenb) {
      case -2: stor.imAt = stor.val1
        break
      case -1: stor.imAt = stor.val2
        break
      case -0: stor.imAt = stor.val3
        break
      case 1: stor.imAt = stor.val4
        break
      case 2: stor.imAt = stor.val5
        break
    }
    switch (stor.Lexo.quest) {
      case 'Image': {
        j3pAffiche(tt[2][0], null, '<b>Donne l’image de $' + stor.Lexo.lenb + '$ par la fonction $f$.</b><br><i>La réponse est un nombre entier.</i>')
        const tt2 = addDefaultTable(stor.lesdiv.travail, 1, 2)
        j3pAffiche(tt2[0][0], null, '$f(' + stor.Lexo.lenb + ') = $&nbsp;')
        stor.zoneI = new ZoneStyleMathquill1(tt2[0][0], { restric: '-1234567890', limite: 5, enter: () => { me.sectionCourante() } })
      }
        break
      default: {
        stor.antOk = []
        let compt = 0
        if (stor.imAt === stor.val1) {
          compt++
          stor.antOk.push(-2)
        }
        if (stor.imAt === stor.val2) {
          compt++
          stor.antOk.push(-1)
        }
        if (stor.imAt === stor.val3) {
          compt++
          stor.antOk.push(0)
        }
        if (stor.imAt === stor.val4) {
          compt++
          stor.antOk.push(1)
        }
        if (stor.imAt === stor.val5) {
          compt++
          stor.antOk.push(2)
        }
        stor.ya2 = compt > 1
        stor.ya3 = compt > 2
        j3pAffiche(tt[2][0], null, '<b>On cherche le (ou les) antécédent(s) de $' + stor.imAt + '$ par $f$.</b><br><i>(Les réponses sont des nombres entiers).</i><br>')
        const tt3 = addDefaultTable(stor.lesdiv.travail, 1, 4)
        j3pAffiche(tt3[0][0], null, 'Le nombre $' + stor.imAt + '$ a&nbsp;')
        stor.liste = ListeDeroulante.create(tt3[0][1], ['Choisir', '0', '1', '2', '3', '4 (ou plus)'], { onChange: changeList })
        stor.ouaf1 = tt3[0][2]
        j3pAffiche(tt3[0][2], null, '&nbsp;antécédent')
        j3pAffiche(tt3[0][3], null, '&nbsp;par $f$.')
        stor.metAnt = addDefaultTable(stor.lesdiv.travail, 1, 1)[0][0]
        stor.listRep = []
      }
        break
    }
  }
  function changeList () {
    const r = stor.liste.reponse
    j3pEmpty(stor.ouaf1)
    j3pAffiche(stor.ouaf1, null, (r !== '0' && r !== '1') ? '&nbsp;antécédents' : '&nbsp;antécédent')
    const arteient = []
    for (let i = 0; i < stor.listRep.length; i++) {
      arteient.push(stor.listRep[i].reponse())
      stor.listRep[i].disable()
    }
    stor.listRep = []
    j3pEmpty(stor.metAnt)
    if (r === '1') {
      const yu = addDefaultTable(stor.metAnt, 1, 3)
      j3pAffiche(yu[0][0], null, 'L’antécédent de $' + stor.imAt + '$ est&nbsp;')
      stor.listRep[0] = new ZoneStyleMathquill1(yu[0][1], { restric: '-0123456789', contenu: arteient[0] || '', limite: 4 })
      j3pAffiche(yu[0][2], null, '.')
    }
    if (r === '2') {
      const yu = addDefaultTable(stor.metAnt, 1, 5)
      j3pAffiche(yu[0][0], null, 'Les antécédents de $' + stor.imAt + '$ sont&nbsp;')
      stor.listRep[0] = new ZoneStyleMathquill1(yu[0][1], { restric: '-0123456789', contenu: arteient[0] || '', limite: 4 })
      j3pAffiche(yu[0][2], null, '&nbsp;et&nbsp;')
      stor.listRep[1] = new ZoneStyleMathquill1(yu[0][3], { restric: '-0123456789', contenu: arteient[1] || '', limite: 4 })
      j3pAffiche(yu[0][4], null, '.')
    }
    if (r === '3') {
      const yu = addDefaultTable(stor.metAnt, 1, 7)
      j3pAffiche(yu[0][0], null, 'Les antécédents de $' + stor.imAt + '$ sont&nbsp;')
      stor.listRep[0] = new ZoneStyleMathquill1(yu[0][1], { restric: '-0123456789', contenu: arteient[0] || '', limite: 4 })
      j3pAffiche(yu[0][2], null, '&nbsp;,&nbsp;')
      stor.listRep[1] = new ZoneStyleMathquill1(yu[0][3], { restric: '-0123456789', contenu: arteient[1] || '', limite: 4 })
      j3pAffiche(yu[0][4], null, '&nbsp;et&nbsp;')
      stor.listRep[2] = new ZoneStyleMathquill1(yu[0][5], { restric: '-0123456789', contenu: arteient[1] || '', limite: 4 })
      j3pAffiche(yu[0][6], null, '.')
    }
  }
  function disaAll () {
    if (stor.Lexo.quest === 'Image') {
      stor.zoneI.disable()
    } else {
      stor.liste.disable()
      for (let i = 0; i < stor.listRep.length; i++) {
        stor.listRep[i].disable()
      }
    }
  }
  function metTruetAll () {
    if (stor.Lexo.quest === 'Image') {
      stor.zoneI.corrige(true)
    } else {
      stor.liste.corrige(true)
      for (let i = 0; i < stor.listRep.length; i++) {
        stor.listRep[i].corrige(true)
      }
    }
  }

  function yaReponse () {
    if (stor.Lexo.quest === 'Image') {
      if (stor.zoneI.reponse() === '') {
        stor.zoneI.focus()
        return false
      }
    } else {
      if (!stor.liste.changed) {
        stor.liste.focus()
        return false
      }
      for (let i = 0; i < stor.listRep.length; i++) {
        console.error('i', i)
        if (stor.listRep[i].reponse() === '') {
          stor.listRep[i].focus()
          return false
        }
      }
    }
    return true
  }

  function affCorrFaux (isFin) {
    if (stor.Lexo.quest === 'Image') {
      stor.zoneI.corrige(false)
    }
    if (stor.errDouble) {
      stor.yaexplik = true
      j3pAffiche(stor.lesdiv.explications, null, 'Tu a écrit plusiseurs fois le même antécédant !')
    }

    if (isFin) {
      barreLesFo()
      disaAll()
      stor.yaco = true
      if (stor.Lexo.quest === 'Image') {
        stor.zoneI.barre()
        j3pAffiche(stor.lesdiv.solution, null, 'L’image de $' + stor.Lexo.lenb + '$ par $f$ est égale à $' + stor.imAt + '$<br>')
        switch (stor.Lexo.donnee) {
          case 'de la notation $f: x -> f(x)$':
            j3pAffiche(stor.lesdiv.solution, null, '$f(' + stor.Lexo.lenb + ') = ' + donneAffiche() + '$<br>')
            j3pAffiche(stor.lesdiv.solution, null, '$f(' + stor.Lexo.lenb + ') = ' + stor.imAt + '$')
            break
          case 'de la courbe d’une fonction':
            stor.mtgAppLecteur.giveFormula2(stor.svgId, 'Hom1', stor.Lexo.lenb)
            stor.mtgAppLecteur.giveFormula2(stor.svgId, 'Hom3', stor.imAt)
            stor.mtgAppLecteur.calculate(stor.svgId)
            stor.mtgAppLecteur.setVisible(stor.svgId, '#seg1', true)
            stor.mtgAppLecteur.setVisible(stor.svgId, '#seg2', true)
            stor.mtgAppLecteur.setVisible(stor.svgId, '#point1', true)
            j3pAffiche(stor.lesdiv.solution, null, '$f(' + stor.Lexo.lenb + ') = ' + stor.imAt + '$')
            break
          default:
            stor.mtgAppLecteur.setVisible(stor.svgId, '#surf' + (stor.Lexo.lenb + 3), true)
            j3pAffiche(stor.lesdiv.solution, null, '$f(' + stor.Lexo.lenb + ') = ' + stor.imAt + '$')
        }
      } else {
        switch (stor.Lexo.donnee) {
          case 'de la notation $f: x -> f(x)$': {
            j3pAffiche(stor.lesdiv.solution, null, 'On appelle $x$ un antécédant de $' + stor.imAt + '$ par $f$,<br>')
            let laf = stor.mtgAppLecteur.getLatexCode(stor.svgId, '#formule')
            laf = laf.replace(/ /g, '')
            laf = laf.replace(/text{}/g, '')
            laf = laf.replace(/f:/g, '')
            laf = laf.replace(/\\/g, '')
            laf = laf.replace(/xmapsto/g, '')
            laf = laf.replace(/ /g, '')
            j3pAffiche(stor.lesdiv.solution, null, '$x$ est une solution de l’équation $f(x) = ' + stor.imAt + '$, soit $' + laf + ' = ' + stor.imAt + '$,<br>')
            if (!stor.ya2) {
              j3pAffiche(stor.lesdiv.solution, null, 'Cette équation n’admet qu’une seule solution $x = ' + stor.antOk[0] + '$.<br>')
              j3pAffiche(stor.lesdiv.solution, null, '<i>(on vérifie que $f(' + stor.antOk[0] + ') = ' + stor.imAt + '$).</i><br><br>')
              j3pAffiche(stor.lesdiv.solution, null, 'Donc $' + stor.antOk[0] + '$ est l’antécédent de $' + stor.imAt + '$ par $f$.')
            } else {
              if (!stor.ya3) {
                j3pAffiche(stor.lesdiv.solution, null, 'Cette équation admet deux solutions $x = ' + stor.antOk[0] + '$ et $x = ' + stor.antOk[1] + '$.<br>')
                j3pAffiche(stor.lesdiv.solution, null, '<i>On vérifie que $f(' + stor.antOk[0] + ') = ' + stor.imAt + '$ et que $f(' + stor.antOk[1] + ') = ' + stor.imAt + '$.</i><br><br>')
                j3pAffiche(stor.lesdiv.solution, null, 'Donc $' + stor.antOk[0] + '$ et $' + stor.antOk[1] + '$ sont les deux antécédants de $' + stor.imAt + '$ par $f$.')
              } else {
                j3pAffiche(stor.lesdiv.solution, null, 'Cette équation admet une infinité de solutions. <br><br>')
                j3pAffiche(stor.lesdiv.solution, null, 'Donc n’importe quel nombre est un antécédant de $' + stor.imAt + '$ par $f$.')
              }
            }
          }
            break
          case 'de la courbe d’une fonction':
            if (!stor.ya2) {
              stor.mtgAppLecteur.giveFormula2(stor.svgId, 'Hom1', stor.Lexo.lenb)
              stor.mtgAppLecteur.giveFormula2(stor.svgId, 'Hom3', stor.imAt)
              stor.mtgAppLecteur.calculate(stor.svgId)
              stor.mtgAppLecteur.setVisible(stor.svgId, '#seg1', true)
              stor.mtgAppLecteur.setVisible(stor.svgId, '#seg2', true)
              stor.mtgAppLecteur.setVisible(stor.svgId, '#point1', true)
              j3pAffiche(stor.lesdiv.solution, null, '$' + stor.antOk[0] + '$ est l’antécédent de $' + stor.imAt + '$ par $f$.')
            } else {
              if (!stor.ya3) {
                stor.mtgAppLecteur.giveFormula2(stor.svgId, 'Hom1', stor.antOk[0])
                stor.mtgAppLecteur.giveFormula2(stor.svgId, 'Hom2', stor.antOk[1])
                stor.mtgAppLecteur.giveFormula2(stor.svgId, 'Hom3', stor.imAt)
                stor.mtgAppLecteur.calculate(stor.svgId)
                stor.mtgAppLecteur.setVisible(stor.svgId, '#seg1', true)
                stor.mtgAppLecteur.setVisible(stor.svgId, '#seg2', true)
                stor.mtgAppLecteur.setVisible(stor.svgId, '#point1', true)
                stor.mtgAppLecteur.setVisible(stor.svgId, '#seg3', true)
                stor.mtgAppLecteur.setVisible(stor.svgId, '#seg4', true)
                stor.mtgAppLecteur.setVisible(stor.svgId, '#point2', true)
                j3pAffiche(stor.lesdiv.solution, null, '$' + stor.antOk[0] + '$ et $' + stor.antOk[1] + '$ sont les deux antécédants de $' + stor.imAt + '$ par $f$.')
              } else {
                j3pAffiche(stor.lesdiv.solution, null, 'N’importe quel nombre est un antécédant de $' + stor.imAt + '$ par $f$.')
              }
            }
            break
          default:
            for (let i = 0; i < stor.antOk.length; i++) {
              stor.mtgAppLecteur.setVisible(stor.svgId, '#surf' + (stor.antOk[i] + 3), true)
            }
            if (!stor.ya2) {
              j3pAffiche(stor.lesdiv.solution, null, '$' + stor.antOk[0] + '$ est l’antécédent de $' + stor.imAt + '$ par $f$.')
            } else {
              if (!stor.ya3) {
                j3pAffiche(stor.lesdiv.solution, null, '$' + stor.antOk[0] + '$ et $' + stor.antOk[1] + '$ sont les deux antécédants de $' + stor.imAt + '$ par $f$.')
              } else {
                j3pAffiche(stor.lesdiv.solution, null, '$-2$, $-1$, $0$, $1$ et $2$ sont des antécédants de $' + stor.imAt + '$ par $f$.')
              }
            }
        }
      }
    }

    if (stor.yaexplik) stor.lesdiv.explications.classList.add('explique')
    if (stor.yaco) stor.lesdiv.solution.classList.add('correction')
  }
  function donneAffiche () {
    let laf = stor.mtgAppLecteur.getLatexCode(stor.svgId, '#formule')
    laf = laf.replace(/ /g, '')
    laf = laf.replace(/text{}/g, '')
    laf = laf.replace(/f:/g, '')
    laf = laf.replace(/\\/g, '')
    laf = laf.replace(/xmapsto/g, '')
    laf = laf.replace(/ /g, '')
    let ainsere = stor.Lexo.lenb
    if ([26, 27].includes(stor.Lexo.fonc)) {
      ainsere = '(' + ainsere + ')'
    }
    if ([2, 3, 4, 6, 10, 11, 12, 13, 14, 15, 16, 17, 18, 23, 24, 25, 28, 29, 30].includes(stor.Lexo.fonc)) {
      if (stor.Lexo.lenb < 0) ainsere = '(' + ainsere + ')'
    }
    if ([2, 6, 10, 11, 14, 15, 16, 17, 28, 29, 30].includes(stor.Lexo.fonc)) {
      ainsere = ' \\times ' + ainsere
    }
    return laf.replace(/x/g, ainsere)
  }

  switch (this.etat) {
    case 'enonce':
      // code exécuté au lancement de la section
      if (me.debutDeLaSection) {
        initSection()
      } else {
        enonceMain()
      }
      break // case "enonce":

    case 'correction':
      // On teste si une réponse a été saisie
      stor.yaexplik = false
      stor.yaco = false
      stor.lesdiv.explications.innerHTML = ''
      stor.lesdiv.correction.innerHTML = ''

      if (!yaReponse()) {
        // pas de réponse
        stor.lesdiv.correction.style.color = this.styles.cfaux
        stor.lesdiv.correction.innerHTML = reponseIncomplete
        // on reste dans l’état correction
        this.finCorrection()
        return
      }

      if (isRepOk()) {
        // Bonne réponse
        this.score++
        stor.lesdiv.correction.style.color = this.styles.cbien
        stor.lesdiv.correction.innerHTML = cBien
        this.typederreurs[0]++
        metTruetAll()
        disaAll()
        this.finCorrection('navigation', true)
        return
      }
      // Pas de bonne réponse
      stor.lesdiv.correction.style.color = this.styles.cfaux
      stor.lesdiv.correction.innerHTML = cFaux
      if (this.isElapsed) {
        // A cause de la limite de temps :
        stor.lesdiv.correction.innerHTML += tempsDepasse + '<BR>'
        this.typederreurs[10]++
        affCorrFaux(true)
        stor.yaco = true
        this.finCorrection('navigation', true)
        return
      }

      // Réponse fausse sans pb de temps
      if (me.essaiCourant < ds.nbchances) {
        stor.lesdiv.correction.innerHTML += '<br>' + essaieEncore
        affCorrFaux(false)
        // affCorrFaux.call(this, false)
        this.typederreurs[1]++
        this.finCorrection()
        return
      }
      // Erreur au dernier essai
      affCorrFaux(true)
      stor.yaco = true
      this.typederreurs[2]++
      // on donne la moitié des points pour une erreur d’arrondi
      if (stor.errArrond) this.score += 0.5
      this.finCorrection('navigation', true)
      break // case "correction":

    case 'navigation':
      if (!this.sectionTerminee()) this.etat = 'enonce'
      this.finNavigation()
      break // case "navigation":
  }
}
