import { j3pAddElt, j3pAjouteBouton, j3pEmpty, j3pGetRandomInt, j3pShuffle } from 'src/legacy/core/functions'
import ListeDeroulante from 'src/legacy/outils/listeDeroulante/ListeDeroulante'
import { j3pAffiche } from 'src/lib/mathquill/functions'
import { addDefaultTable } from 'src/legacy/themes/table'
import ZoneStyleMathquill1 from 'src/legacy/outils/zoneStyleMathquill/ZoneStyleMathquill1'
import { j3pAfficheCroixFenetres, j3pCreeFenetres, j3pToggleFenetres } from 'src/legacy/core/functionsJqDialog'
import Algebrite from 'algebrite'
import textesGeneriques from 'src/lib/core/textes'

const { cBien, cFaux, essaieEncore, reponseIncomplete, tempsDepasse } = textesGeneriques

/**
 * Ajouter une description ici
 * @fileOverview
 * @author Tommy Barroy <t.barroy@laposte.net>
 * @since 2017-10
 */

export const params = {
  outils: ['mathquill', 'calculatrice'],
  /**
   * Ce qui est paramétrable dans l’éditeur de graphe
   * @type {LegacySectionParamsParametres}
   */
  parametres: [
    ['nbrepetitions', 9, 'entier', 'Nombre de répétitions de la section'],
    ['nbchances', 2, 'entier', 'Nombre d’essais possibles.'],
    ['limite', 0, 'entier', 'Nombre d’essais possibles.'],
    ['theme', 'standard', 'liste', 'Modèle pour l’affichage', ['standard', 'zonesAvecImageDeFond']]
  ]
}

/**
 * section fonction01
 * @this {Parcours}
 */
export default function main () {
  const me = this
  const stor = me.storage
  const ds = me.donneesSection

  function boutonfen () {
    j3pToggleFenetres('Calculatrice')
  }
  function suivEtape () {
    let i = stor.etapes.indexOf(stor.etapeEncours) + 1
    if (i > stor.etapes.length - 1) i = 0
    return stor.etapes[i]
  }
  function initSection () {
    // Construction de la page
    stor.etapes = ['form', 'fonc', 'trouva', 'trouvb', 'concl']
    stor.etapeEncours = 'concl'
    me.construitStructurePage('presentation1bis')
    ds.lesExos = []

    ds.nbetapes = 5
    ds.nbitems = ds.nbetapes * ds.nbrepetitions

    stor.casA = [-10, -9, -8, -7, -6, -5, -4, -3, -2, 2, 3, 4, 5, 6, 7, 8, 9, 10, 100, 20, -100, 50, -50]
    stor.apioche = j3pShuffle(stor.casA)

    me.typederreurs = [0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0]

    me.afficheTitre('Déterminer une fonction affine')
    enonceMain()
  }

  function calcule0 (form) {
    const form2 = String(form)
    const algbuf = Algebrite.run(form2)
    return (algbuf === '0' || algbuf === '-0' || algbuf === '0.0' || algbuf === '0.0...')
  }

  function creeLesDiv () {
    stor.lesdiv = {}
    stor.lesdiv.conteneur = j3pAddElt(me.zonesElts.MG, 'div', {
      style: me.styles.etendre('toutpetit.enonce', { padding: '10px' })
    })
    stor.lesdiv.consigne = addDefaultTable(stor.lesdiv.conteneur, 1, 1)[0][0]
    stor.lesdiv.calculatrice = j3pAddElt(me.zonesElts.MD, 'div', {
      style: me.styles.etendre('toutpetit.enonce', { padding: '10px' })
    })
    stor.lesdiv.correction = j3pAddElt(me.zonesElts.MD, 'div', {
      style: me.styles.etendre('toutpetit.enonce', { padding: '10px' })
    })
    stor.lesdiv.etape = addDefaultTable(stor.lesdiv.conteneur, 1, 1)[0][0]
    stor.lesdiv.travail = addDefaultTable(stor.lesdiv.conteneur, 1, 1)[0][0]
    stor.lesdiv.explications = j3pAddElt(stor.lesdiv.conteneur, 'div')
    stor.lesdiv.solution = j3pAddElt(stor.lesdiv.conteneur, 'div')
    stor.lesdiv.divCache = j3pAddElt(stor.lesdiv.conteneur, 'div')
    stor.lesdiv.divCache.style.display = 'none'
    stor.lesdiv.travail.classList.add('travail')
    stor.lesdiv.consigne.classList.add('enonce')
    me.zonesElts.MG.classList.add('fond')
    stor.lesdiv.etape.style.color = me.styles.colorCorrection
    stor.lesdiv.solution.style.color = me.styles.cbien
    stor.lesdiv.explications.style.color = me.styles.cfaux
  }

  function barreLesFo () {
    if (stor.etapeEncours === 'form') {
      if (stor.liste.isOk() === false) stor.liste.barre()
    }
    if (stor.etapeEncours === 'fonc') {
      if (stor.zonex1.isOk() === false) stor.zonex1.barre()
      if (stor.zonex2.isOk() === false) stor.zonex2.barre()
    }
    if (stor.etapeEncours === 'trouva') {
      if (stor.zonex3.isOk() === false) stor.zonex3.barre()
      if (stor.zonex4.isOk() === false) stor.zonex4.barre()
    }
    if (stor.etapeEncours === 'trouvb') {
      if (stor.zoneA.isOk() === false) stor.zoneA.barre()
      if (stor.zonex3.isOk() === false) stor.zonex3.barre()
      if (stor.zonex4.isOk() === false) stor.zonex4.barre()
    }
    if (stor.etapeEncours === 'concl') {
      if (stor.zoneB.isOk() === false) stor.zoneB.barre()
      if (stor.zonex3.isOk() === false) stor.zonex3.barre()
      if (stor.zonex4.isOk() === false) stor.zonex4.barre()
    }
  }
  function enonceMain () {
    stor.etapeEncours = suivEtape()
    if (stor.etapeEncours === 'form') {
      stor.foco1 = false
      stor.foco2 = false
      stor.foco3 = false
      stor.foco4 = false
      stor.focoA = false
      me.videLesZones()
      creeLesDiv()
      poseQuestion0()
      me.fenetresjq = [{ name: 'Calculatrice', title: 'Une calculatrice', left: 600, top: 10 }]
      j3pCreeFenetres(me)
      j3pAfficheCroixFenetres('Calculatrice')
      j3pAjouteBouton(stor.lesdiv.calculatrice, boutonfen, { value: 'calculatrice', className: 'MepBoutons' })
    }
    poseQuestion()
    me.finEnonce()
  }
  function isRepOk () {
    stor.errDouble = false
    stor.errReduc = false
    if (stor.etapeEncours === 'form') {
      return stor.liste.reponse === stor.listAt
    }
    if (stor.etapeEncours === 'fonc') {
      const res1 = stor.zonex1.reponse()
      const res2 = stor.zonex2.reponse()
      const comp1 = stor.x1 + '*a+b'
      const comp2 = stor.x2 + '*a+b'
      let ok = true
      if (!calcule0(res1 + '-(' + comp1 + ')')) {
        stor.zonex1.corrige(false)
        ok = false
      }
      if (!calcule0(res2 + '-(' + comp2 + ')')) {
        stor.zonex2.corrige(false)
        ok = false
      }
      return ok
    }
    if (stor.etapeEncours === 'trouva') {
      const res1 = stor.zonex3.reponse()
      const res2 = stor.zonex4.reponse()
      const comp1 = (stor.y1 - stor.y2)
      const comp2 = (stor.x1 - stor.x2) + '*a'
      let ok = true
      if (!calcule0(res1 + '-(' + comp1 + ')') && !calcule0(res1 + '-(' + comp2 + ')')) {
        stor.zonex3.corrige(false)
        ok = false
      }
      if (!calcule0(res2 + '-(' + comp2 + ')') && !calcule0(res2 + '-(' + comp1 + ')')) {
        stor.zonex4.corrige(false)
        ok = false
      }
      if (calcule0(res2 + '-(' + res1 + ')')) {
        stor.errDouble = true
        stor.zonex4.corrige(false)
        ok = false
      }
      // vérifie que les réponses soient simples
      const verifA = (res1.includes('a')) ? res1 : res2
      const verifN = (res1.includes('a')) ? res2 : res1
      const zoneA = (res1.includes('a')) ? stor.zonex3 : stor.zonex4
      const zoneN = (res1.includes('a')) ? stor.zonex4 : stor.zonex3
      if (verifA.includes('b')) {
        ok = false
        stor.errReduc = true
        zoneA.corrige(false)
      }
      const count = (verifA.match(/a/g) || []).length
      if (count !== 1) {
        ok = false
        stor.errReduc = true
        zoneA.corrige(false)
      }
      const count2 = (verifN.match(/-/g) || []).length
      if (count2 !== 0) {
        if (count2 !== 1 || verifN[0] !== '-') { ok = false }
        stor.errReduc = true
        zoneN.corrige(false)
      }
      return ok
    }
    if (stor.etapeEncours === 'trouvb') {
      const resA = stor.zoneA.reponse()
      const res1 = stor.zonex3.reponse()
      const res2 = stor.zonex4.reponse()
      const comp1 = stor.y1
      const comp2 = (stor.x1 * stor.a) + '+b'
      let ok = true
      if (Number(resA) !== stor.a) {
        ok = false
        stor.zoneA.corrige(false)
        stor.focoA = true
      }
      if (!calcule0(res1 + '-(' + comp1 + ')') && !calcule0(res1 + '-(' + comp2 + ')')) {
        stor.zonex3.corrige(false)
        ok = false
        stor.foco4 = true
      }
      if (!calcule0(res2 + '-(' + comp2 + ')') && !calcule0(res2 + '-(' + comp1 + ')')) {
        stor.zonex4.corrige(false)
        ok = false
        stor.foco4 = true
      }
      if (calcule0(res2 + '-(' + res1 + ')')) {
        stor.errDouble = true
        stor.zonex4.corrige(false)
        ok = false
        stor.foco4 = true
      }
      return ok
    }
    if (stor.etapeEncours === 'concl') {
      const resB = stor.zoneB.reponse()
      const res1 = stor.zonex3.reponse()
      const res2 = stor.zonex4.reponse()
      let ok = true
      if (Number(resB) !== stor.b) {
        ok = false
        stor.zoneB.corrige(false)
        stor.focoB = true
      }
      if (Number(res1) !== stor.a) {
        ok = false
        stor.zonex3.corrige(false)
        stor.foco4 = true
      }
      if (Number(res2) !== stor.b) {
        ok = false
        stor.zonex4.corrige(false)
        stor.foco4 = true
      }
      if (res2.length > 0) {
        if (res2[0] !== '+' && res2[0] !== '-') {
          ok = false
          stor.zonex4.corrige(false)
          stor.foco4 = true
        }
      }
      return ok
    }
    return true
  }
  function modif (tab) {
    for (let i = 0; i < tab.length; i++) {
      for (let j = 0; j < tab[i].length; j++) {
        tab[i][j].style.padding = '0px'
      }
    }
  }

  function poseQuestion0 () {
    j3pAffiche(stor.lesdiv.consigne, null, 'La fonction $f$ est une fonction affine,<br>')
    stor.x1 = j3pGetRandomInt(-10, 10)
    if (stor.x1 === 0) stor.x1 = 11
    if (stor.x1 === 1) stor.x1 = 12
    if (stor.x1 === -1) stor.x1 = -11
    if (stor.apioche.length === 0) stor.apioche = j3pShuffle(stor.casA)
    stor.a = stor.apioche.pop()
    stor.b = j3pGetRandomInt(-10, 10)
    stor.y1 = stor.x1 * stor.a + stor.b
    j3pAffiche(stor.lesdiv.consigne, null, 'On sait que $f(' + stor.x1 + ')=' + stor.y1 + '$')
    do {
      stor.x2 = j3pGetRandomInt(-10, 10)
      if (stor.x2 === 0) stor.x2 = 11
      if (stor.x2 === 1) stor.x2 = 12
      if (stor.x2 === -1) stor.x2 = -11
    } while (Math.abs(stor.x1 - stor.x2) <= 1)
    stor.y2 = stor.x2 * stor.a + stor.b
    j3pAffiche(stor.lesdiv.consigne, null, ' et que $f(' + stor.x2 + ')=' + stor.y2 + '$')
    j3pAffiche(stor.lesdiv.consigne, null, '<br><b>On veut déterminer la fonction $f$.</b>')
  }
  function poseQuestion () {
    j3pEmpty(stor.lesdiv.solution)
    j3pEmpty(stor.lesdiv.explications)
    stor.lesdiv.explications.classList.remove('explique')
    stor.lesdiv.solution.classList.remove('correction')
    if (stor.foco1) {
      j3pEmpty(stor.douko)
      j3pAffiche(stor.douko, null, '$f:\\text{ }x\\text{ }\\mapsto\\text{ }ax + b$')
      stor.douko.style.color = me.styles.colorCorrection
      stor.foco1 = false
    }
    if (stor.foco2) {
      j3pEmpty(stor.douko1)
      j3pEmpty(stor.douko2)
      j3pAffiche(stor.douko1, null, '$' + stor.x1 + 'a + b$')
      stor.douko1.style.color = me.styles.colorCorrection
      j3pAffiche(stor.douko2, null, '$' + stor.x2 + 'a + b$')
      stor.douko2.style.color = me.styles.colorCorrection

      j3pEmpty(stor.dou1)
      j3pEmpty(stor.dou2)
      j3pAffiche(stor.dou1, null, '$' + stor.x1 + 'a + b$')
      j3pAffiche(stor.dou2, null, '$' + stor.x2 + 'a + b$')
      stor.foco2 = false
    }
    if (stor.foco3) {
      j3pEmpty(stor.douko3)
      j3pEmpty(stor.douko4)
      j3pAffiche(stor.douko3, null, '$' + (stor.y1 - stor.y2) + '$')
      stor.douko3.style.color = me.styles.colorCorrection
      j3pAffiche(stor.douko4, null, '$' + (stor.x1 - stor.x2) + 'a$')
      stor.douko4.style.color = me.styles.colorCorrection

      j3pEmpty(stor.dou3)
      j3pEmpty(stor.dou4)
      j3pAffiche(stor.dou3, null, '$' + (stor.y1 - stor.y2) + '$')
      j3pAffiche(stor.dou4, null, '$' + (stor.x1 - stor.x2) + 'a$')
      stor.foco3 = false
    }
    if (stor.foco4) {
      j3pEmpty(stor.douko3)
      j3pEmpty(stor.douko4)

      j3pAffiche(stor.douko3, null, '$' + stor.y1 + '$')
      stor.douko3.style.color = me.styles.colorCorrection
      j3pAffiche(stor.douko4, null, '$' + (stor.x1 * stor.a) + ' + b$')
      stor.douko4.style.color = me.styles.colorCorrection

      j3pEmpty(stor.dou3)
      j3pEmpty(stor.dou4)
      j3pAffiche(stor.dou3, null, '$' + stor.y1 + '$')
      j3pAffiche(stor.dou4, null, '$' + (stor.x1 * stor.a) + ' + b$')
      stor.foco4 = false
    }
    if (stor.focoA) {
      j3pEmpty(stor.divCoA)
      j3pEmpty(stor.douko2)
      j3pAffiche(stor.douko2, null, '$' + stor.a + '$')
      stor.douko2.style.color = me.styles.colorCorrection
      stor.focoA = false
    }
    if (stor.etapeEncours === 'form') {
      const ll = j3pShuffle(['$f:\\text{ }x\\text{ }\\mapsto\\text{ } ax$', '$f:\\text{ }x\\text{ }\\mapsto\\text{ }ax + b$', '$f:\\text{ }x\\text{ }\\mapsto\\text{ }ax²$'])
      ll.splice(0, 0, 'Choisir')
      j3pAffiche(stor.lesdiv.etape, null, '<b><u>Étape 1</u>: Forme de $f$.</b>')
      const ttabl1 = addDefaultTable(stor.lesdiv.travail, 1, 2)
      modif(ttabl1)
      j3pAffiche(ttabl1[0][0], null, 'Comme la fonction $f$ est affine, elle est de la forme&nbsp;')
      stor.liste = ListeDeroulante.create(ttabl1[0][1], ll)
      stor.douko = ttabl1[0][1]
      stor.listAt = '$f:\\text{ }x\\text{ }\\mapsto\\text{ }ax + b$'
    }
    if (stor.etapeEncours === 'fonc') {
      j3pEmpty(stor.lesdiv.etape)
      j3pAffiche(stor.lesdiv.etape, null, '<b><u>Étape 2</u>: Utilisation la notation $f:\\text{ }x\\text{ }\\mapsto\\text{ }ax + b$.</b>')
      const ttabl1 = addDefaultTable(stor.lesdiv.travail, 2, 1)
      modif(ttabl1)
      j3pAffiche(ttabl1[0][0], null, '<br><b>Exprime $f(' + stor.x1 + ')$ et $f(' + stor.x2 + ')$ en fonction de $a$ et $b$.</b>')
      stor.avire = ttabl1[0][0]
      const ttablx1 = addDefaultTable(ttabl1[1][0], 1, 2)
      modif(ttablx1)
      const ttablx1d = addDefaultTable(ttablx1[0][1], 1, 5)
      modif(ttablx1d)
      j3pAffiche(ttablx1d[0][0], null, '&nbsp;Donc&nbsp;$f(' + stor.x1 + ') - f(' + stor.x2 + ') = $')
      stor.dou1 = ttablx1d[0][1]
      stor.dou2 = ttablx1d[0][3]
      j3pAffiche(ttablx1d[0][2], null, '&nbsp;$-($&nbsp;')
      j3pAffiche(ttablx1d[0][4], null, '&nbsp;$)$&nbsp;')
      j3pAffiche(stor.dou1, null, '$\\text{ _ _ }$')
      j3pAffiche(stor.dou2, null, '$\\text{ _ _ }$')
      const ttablx1g = addDefaultTable(ttablx1[0][0], 2, 3)
      modif(ttablx1g)
      j3pAffiche(ttablx1g[0][0], null, '$f(' + stor.x1 + ') = $&nbsp;')
      ttablx1g[0][0].style.textAlign = 'right'
      ttablx1g[1][0].style.textAlign = 'right'
      j3pAffiche(ttablx1g[1][0], null, '$f(' + stor.x2 + ') = $&nbsp;')
      j3pAffiche(ttablx1g[0][2], null, '&nbsp;&nbsp;')
      stor.zonex1 = new ZoneStyleMathquill1(ttablx1g[0][1], { restric: '0123456789,.-ab+', afaire: afaire1, enter: () => { me.sectionCourante() } })
      stor.zonex2 = new ZoneStyleMathquill1(ttablx1g[1][1], { restric: '0123456789,.-ab+', afaire: afaire2, enter: () => { me.sectionCourante() } })
      ttablx1[0][0].style.borderRight = '2px solid black'
      stor.douko1 = ttablx1g[0][1]
      stor.douko2 = ttablx1g[1][1]
    }
    if (stor.etapeEncours === 'trouva') {
      j3pEmpty(stor.lesdiv.etape)
      j3pAffiche(stor.lesdiv.etape, null, '<b><u>Étape 3</u>: Utilisation des données.</b>')
      const ttabl1 = addDefaultTable(stor.lesdiv.travail, 2, 1)
      modif(ttabl1)
      j3pEmpty(stor.avire)
      j3pAffiche(ttabl1[0][0], null, '<br><b>Exprime de deux manières différentes $f(' + stor.x1 + ') - f(' + stor.x2 + ')$</b><br><i>Écritures simplifiées attendues</i>')
      stor.avire = ttabl1[0][0]
      const ttablx2 = addDefaultTable(ttabl1[1][0], 1, 2)
      modif(ttablx2)
      const ttablx2d = addDefaultTable(ttablx2[0][1], 1, 4)
      modif(ttablx2d)
      j3pAffiche(ttablx2d[0][0], null, '&nbsp;&nbsp;Donc&nbsp;&nbsp;')
      stor.dou3 = ttablx2d[0][1]
      stor.dou4 = ttablx2d[0][3]
      j3pAffiche(ttablx2d[0][2], null, '&nbsp;=&nbsp;')
      j3pAffiche(stor.dou3, null, '$\\text{ _ _ }$')
      j3pAffiche(stor.dou4, null, '$\\text{ _ _ }$')
      const ttablx2g = addDefaultTable(ttablx2[0][0], 2, 3)
      modif(ttablx2g)
      j3pAffiche(ttablx2g[0][0], null, '$f(' + stor.x1 + ') - f(' + stor.x2 + ') = $&nbsp;')
      j3pAffiche(ttablx2g[1][0], null, '$f(' + stor.x1 + ') - f(' + stor.x2 + ') = $&nbsp;')
      j3pAffiche(ttablx2g[0][2], null, '&nbsp;&nbsp;')
      stor.zonex3 = new ZoneStyleMathquill1(ttablx2g[0][1], { restric: '0123456789,.-ab', afaire: afaire3, enter: () => { me.sectionCourante() } })
      stor.zonex4 = new ZoneStyleMathquill1(ttablx2g[1][1], { restric: '0123456789,.-ab', afaire: afaire4, enter: () => { me.sectionCourante() } })
      ttablx2[0][0].style.borderRight = '2px solid black'
      stor.douko3 = ttablx2g[0][1]
      stor.douko4 = ttablx2g[1][1]
    }
    if (stor.etapeEncours === 'trouvb') {
      j3pEmpty(stor.lesdiv.etape)
      j3pAffiche(stor.lesdiv.etape, null, '<b><u>Étape 4</u>: Calcul de $a$.</b>')
      const ttabl1 = addDefaultTable(stor.lesdiv.travail, 3, 1)
      modif(ttabl1)
      j3pEmpty(stor.avire)
      j3pAffiche(stor.avire, null, '<br>')
      j3pAffiche(ttabl1[0][0], null, '<br><b>En déduire $a$ puis exprime de deux manières différentes $f(' + stor.x1 + ')$</b>')
      stor.avire = ttabl1[0][0]
      const tabA = addDefaultTable(ttabl1[1][0], 1, 4)
      modif(tabA)
      j3pAffiche(tabA[0][0], null, '$a =$&nbsp;')
      j3pAffiche(tabA[0][2], null, '&nbsp;')
      stor.divCoA = tabA[0][3]
      stor.zoneA = new ZoneStyleMathquill1(tabA[0][1], { restric: '0123456789,.-', enter: () => { me.sectionCourante() } })
      stor.douko2 = tabA[0][1]
      const ttablx2 = addDefaultTable(ttabl1[2][0], 1, 2)
      modif(ttablx2)
      const ttablx2d = addDefaultTable(ttablx2[0][1], 1, 4)
      modif(ttablx2d)
      j3pAffiche(ttablx2d[0][0], null, '&nbsp;&nbsp;Donc&nbsp;&nbsp;')
      stor.dou3 = ttablx2d[0][1]
      stor.dou4 = ttablx2d[0][3]
      j3pAffiche(ttablx2d[0][2], null, '&nbsp;=&nbsp;')
      j3pAffiche(stor.dou3, null, '$\\text{ _ _ }$')
      j3pAffiche(stor.dou4, null, '$\\text{ _ _ }$')
      const ttablx2g = addDefaultTable(ttablx2[0][0], 2, 3)
      modif(ttablx2g)
      j3pAffiche(ttablx2g[0][0], null, '$f(' + stor.x1 + ') = $&nbsp;')
      j3pAffiche(ttablx2g[1][0], null, '$f(' + stor.x1 + ') = $&nbsp;')
      j3pAffiche(ttablx2g[0][2], null, '&nbsp;&nbsp;')
      stor.zonex3 = new ZoneStyleMathquill1(ttablx2g[0][1], { restric: '0123456789,.-ab+', afaire: afaire3, enter: () => { me.sectionCourante() } })
      stor.zonex4 = new ZoneStyleMathquill1(ttablx2g[1][1], { restric: '0123456789,.-ab+', afaire: afaire4, enter: () => { me.sectionCourante() } })
      ttablx2[0][0].style.borderRight = '2px solid black'
      stor.douko3 = ttablx2g[0][1]
      stor.douko4 = ttablx2g[1][1]
    }
    if (stor.etapeEncours === 'concl') {
      j3pEmpty(stor.lesdiv.etape)
      j3pAffiche(stor.lesdiv.etape, null, '<b><u>Étape 4</u>: Calcul de $b$.</b>')
      const ttabl1 = addDefaultTable(stor.lesdiv.travail, 3, 1)
      modif(ttabl1)
      j3pEmpty(stor.avire)
      j3pAffiche(stor.avire, null, '<br>')
      j3pAffiche(ttabl1[0][0], null, '<br><b>En déduire $b$ , puis détermine $f$.</b>')
      stor.avire = ttabl1[0][0]
      const tabA = addDefaultTable(ttabl1[1][0], 1, 4)
      modif(tabA)
      j3pAffiche(tabA[0][0], null, '$b =$&nbsp;')
      j3pAffiche(tabA[0][2], null, '&nbsp;')
      stor.divCoB = tabA[0][3]
      stor.zoneB = new ZoneStyleMathquill1(tabA[0][1], { restric: '0123456789,.-', enter: () => { me.sectionCourante() } })
      stor.douko2 = tabA[0][1]
      const ttablx2 = addDefaultTable(ttabl1[2][0], 1, 4)
      modif(ttablx2)
      j3pAffiche(ttablx2[0][0], null, 'La fonction affine $f$ est définie par $f:\\text{ }x\\text{ }\\mapsto\\text{ }$')
      stor.zonex3 = new ZoneStyleMathquill1(ttablx2[0][1], { restric: '0123456789,.-+', enter: () => { me.sectionCourante() } })
      j3pAffiche(ttablx2[0][2], null, '$x$&nbsp;')
      stor.zonex4 = new ZoneStyleMathquill1(ttablx2[0][3], { restric: '0123456789,.-+', enter: () => { me.sectionCourante() } })
    }
  }
  function afaire1 () {
    let acrire = stor.zonex1.reponse().replace(/ /g, '')
    if (acrire === '') acrire = '\\text{ _ _ }'
    j3pEmpty(stor.dou1)
    j3pAffiche(stor.dou1, null, '$' + acrire + '$')
  }
  function afaire2 () {
    let acrire = stor.zonex2.reponse().replace(/ /g, '')
    if (acrire === '') acrire = '\\text{ _ _ }'
    j3pEmpty(stor.dou2)
    j3pAffiche(stor.dou2, null, '$' + acrire + '$')
  }
  function afaire3 () {
    let acrire = stor.zonex3.reponse().replace(/ /g, '')
    if (acrire === '') acrire = '\\text{ _ _ }'
    j3pEmpty(stor.dou3)
    j3pAffiche(stor.dou3, null, '$' + acrire + '$')
  }
  function afaire4 () {
    let acrire = stor.zonex4.reponse().replace(/ /g, '')
    if (acrire === '') acrire = '\\text{ _ _ }'
    j3pEmpty(stor.dou4)
    j3pAffiche(stor.dou4, null, '$' + acrire + '$')
  }

  function disaAll () {
    if (stor.etapeEncours === 'form') {
      stor.liste.disable()
    }
    if (stor.etapeEncours === 'fonc') {
      stor.zonex1.disable()
      stor.zonex2.disable()
    }
    if (stor.etapeEncours === 'trouva') {
      stor.zonex3.disable()
      stor.zonex4.disable()
    }
    if (stor.etapeEncours === 'trouvb') {
      stor.zoneA.disable()
      stor.zonex3.disable()
      stor.zonex4.disable()
    }
    if (stor.etapeEncours === 'concl') {
      stor.zoneB.disable()
      stor.zonex3.disable()
      stor.zonex4.disable()
    }
  }
  function metTruetAll () {
    if (stor.etapeEncours === 'form') {
      stor.liste.corrige(true)
    }
    if (stor.etapeEncours === 'fonc') {
      stor.zonex1.corrige(true)
      stor.zonex2.corrige(true)
    }
    if (stor.etapeEncours === 'trouva') {
      stor.zonex3.corrige(true)
      stor.zonex4.corrige(true)
    }
    if (stor.etapeEncours === 'trouvb') {
      stor.zoneA.corrige(true)
      stor.zonex3.corrige(true)
      stor.zonex4.corrige(true)
    }
    if (stor.etapeEncours === 'concl') {
      stor.zoneB.corrige(true)
      stor.zonex3.corrige(true)
      stor.zonex4.corrige(true)
    }
  }

  function yaReponse () {
    if (stor.etapeEncours === 'form') {
      if (!stor.liste.changed) {
        stor.liste.focus()
        return false
      }
    }
    if (stor.etapeEncours === 'fonc') {
      if (stor.zonex1.reponse() === '') {
        stor.zonex1.focus()
        return false
      }
      if (stor.zonex2.reponse() === '') {
        stor.zonex2.focus()
        return false
      }
    }
    if (stor.etapeEncours === 'trouva') {
      if (stor.zonex3.reponse() === '') {
        stor.zonex3.focus()
        return false
      }
      if (stor.zonex4.reponse() === '') {
        stor.zonex4.focus()
        return false
      }
    }
    if (stor.etapeEncours === 'trouvb') {
      if (stor.zoneA.reponse() === '') {
        stor.zoneA.focus()
        return false
      }
      if (stor.zonex3.reponse() === '') {
        stor.zonex3.focus()
        return false
      }
      if (stor.zonex4.reponse() === '') {
        stor.zonex4.focus()
        return false
      }
    }
    if (stor.etapeEncours === 'concl') {
      if (stor.zoneB.reponse() === '') {
        stor.zoneB.focus()
        return false
      }
      if (stor.zonex3.reponse() === '') {
        stor.zonex3.focus()
        return false
      }
      if (stor.zonex4.reponse() === '') {
        stor.zonex4.focus()
        return false
      }
    }
    return true
  }
  function faisSigne (nb) {
    if (nb < 0) { return ' +' + Math.abs(nb) } else { return ' -' + nb }
  }
  function faisParent (nb) {
    if (nb < 0) { return '(' + nb + ')' } else { return nb }
  }
  function forceSigne (nb) {
    if (nb < 0) { return nb } else { return ' +' + nb }
  }
  function affCorrFaux (isFin) {
    if (stor.errDouble) {
      stor.yaexplik = true
      j3pAffiche(stor.lesdiv.explications, null, 'Tu dois procéder de deux manières différentes !')
    }
    if (stor.etapeEncours === 'form') stor.liste.corrige(false)

    if (isFin) {
      stor.yaco = true
      if (stor.etapeEncours === 'form') {
        stor.foco1 = true
        j3pAffiche(stor.lesdiv.solution, null, 'Une fonction affine est de la forme $f:\\text{ }x\\text{ }\\mapsto\\text{ } ax + b$')
      }
      if (stor.etapeEncours === 'fonc') {
        stor.foco2 = true
        j3pAffiche(stor.lesdiv.solution, null, '$f(' + stor.x1 + ') = ' + stor.x1 + ' \\times a + b$ <br>')
        j3pAffiche(stor.lesdiv.solution, null, '$f(' + stor.x2 + ') = ' + stor.x2 + ' \\times a + b$ <br>')
      }
      if (stor.etapeEncours === 'trouva') {
        stor.foco3 = true
        j3pAffiche(stor.lesdiv.solution, null, '$f(' + stor.x1 + ') - f(' + stor.x2 + ') = ' + stor.y1 + faisSigne(stor.y2) + ' = ' + (stor.y1 - stor.y2) + '$ <br><br>')
        j3pAffiche(stor.lesdiv.solution, null, '$f(' + stor.x1 + ') - f(' + stor.x2 + ') = ' + stor.x1 + 'a + b - ( ' + stor.x2 + 'a + b )$ <br>')
        j3pAffiche(stor.lesdiv.solution, null, '$f(' + stor.x1 + ') - f(' + stor.x2 + ') = ' + stor.x1 + 'a + b ' + faisSigne(stor.x2) + 'a - b  $ <br>')
        j3pAffiche(stor.lesdiv.solution, null, '$f(' + stor.x1 + ') - f(' + stor.x2 + ') = ' + (stor.x1 - stor.x2) + 'a$ <i>(après réduction)</i><br>')
      }
      if (stor.etapeEncours === 'trouvb') {
        if (stor.focoA) {
          j3pAffiche(stor.divCoA, null, '$a = \\frac{' + (stor.y1 - stor.y2) + '}{' + (stor.x1 - stor.x2) + '} = ' + stor.a + '$ <br><br>')
          stor.divCoA.style.color = me.styles.colorCorrection
          stor.divCoA.style.verticalAlign = 'middle'
        }
        if (stor.foco4) {
          j3pEmpty(stor.avire)
          j3pAffiche(stor.avire, null, '<br>')
          j3pAffiche(stor.lesdiv.solution, null, '$f(' + stor.x1 + ') = ' + stor.y1 + '$ <i>(Données)</i><br><br>')
          j3pAffiche(stor.lesdiv.solution, null, '$f(' + stor.x1 + ') = ' + stor.x1 + ' \\times ' + faisParent(stor.a) + ' + b $ <br>')
          j3pAffiche(stor.lesdiv.solution, null, '$f(' + stor.x1 + ') = ' + stor.x1 * stor.a + ' + b$')
        }
      }
      if (stor.etapeEncours === 'concl') {
        if (stor.focoB) {
          j3pAffiche(stor.divCoB, null, '$b = ' + stor.y1 + faisSigne(stor.x1 * stor.a) + ' = ' + stor.b + '$ <br><br>')
          stor.divCoB.style.color = me.styles.colorCorrection
        }
        if (stor.foco4) {
          j3pEmpty(stor.avire)
          j3pAffiche(stor.avire, null, '<br>')
          j3pAffiche(stor.lesdiv.solution, null, 'En remplaçant $a$ et $b$, on obtient $f:\\text{ }x\\text{ }\\mapsto\\text{ }' + stor.a + 'x' + forceSigne(stor.b) + '$')
        }
      }
      barreLesFo()
      disaAll()
    }

    if (stor.yaexplik) stor.lesdiv.explications.classList.add('explique')
    if (stor.yaco) stor.lesdiv.solution.classList.add('correction')
  }

  switch (this.etat) {
    case 'enonce':
      // code exécuté au lancement de la section
      if (me.debutDeLaSection) {
        initSection()
      } else {
        enonceMain()
      }
      break // case "enonce":

    case 'correction':
      // On teste si une réponse a été saisie
      stor.yaexplik = false
      stor.yaco = false
      stor.lesdiv.explications.innerHTML = ''
      stor.lesdiv.correction.innerHTML = ''

      if (!yaReponse()) {
        // pas de réponse
        stor.lesdiv.correction.style.color = this.styles.cfaux
        stor.lesdiv.correction.innerHTML = reponseIncomplete
        // on reste dans l’état correction
        this.finCorrection()
        return
      }

      if (isRepOk()) {
        // Bonne réponse
        this.score++
        stor.lesdiv.correction.style.color = this.styles.cbien
        stor.lesdiv.correction.innerHTML = cBien
        this.typederreurs[0]++
        metTruetAll()
        disaAll()
        this.finCorrection('navigation', true)
        return
      }
      // Pas de bonne réponse
      stor.lesdiv.correction.style.color = this.styles.cfaux
      stor.lesdiv.correction.innerHTML = cFaux
      if (this.isElapsed) {
        // A cause de la limite de temps :
        stor.lesdiv.correction.innerHTML += tempsDepasse + '<BR>'
        this.typederreurs[10]++
        affCorrFaux(true)
        stor.yaco = true
        this.finCorrection('navigation', true)
        return
      }

      // Réponse fausse sans pb de temps
      if (me.essaiCourant < ds.nbchances) {
        stor.lesdiv.correction.innerHTML += '<br>' + essaieEncore
        affCorrFaux(false)
        // affCorrFaux.call(this, false)
        this.typederreurs[1]++
        this.finCorrection()
        return
      }
      // Erreur au dernier essai
      affCorrFaux(true)
      stor.yaco = true
      this.typederreurs[2]++
      // on donne la moitié des points pour une erreur d’arrondi
      if (stor.errArrond) this.score += 0.5
      this.finCorrection('navigation', true)
      break // case "correction":

    case 'navigation':
      if (!this.sectionTerminee()) this.etat = 'enonce'
      this.finNavigation()
      break // case "navigation":
  }
}
