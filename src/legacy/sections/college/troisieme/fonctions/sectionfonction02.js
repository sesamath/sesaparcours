import { j3pAddContent, j3pAddElt, j3pAjouteBouton, j3pBarre, j3pEmpty, j3pGetRandomInt, j3pShuffle } from 'src/legacy/core/functions'
import ListeDeroulante from 'src/legacy/outils/listeDeroulante/ListeDeroulante'
import { j3pAffiche } from 'src/lib/mathquill/functions'
import { addDefaultTable } from 'src/legacy/themes/table'
import ZoneStyleMathquill1 from 'src/legacy/outils/zoneStyleMathquill/ZoneStyleMathquill1'
import { j3pAfficheCroixFenetres, j3pCreeFenetres, j3pToggleFenetres } from 'src/legacy/core/functionsJqDialog'
import Algebrite from 'algebrite'
import textesGeneriques from 'src/lib/core/textes'

const { cBien, cFaux, essaieEncore, reponseIncomplete, tempsDepasse } = textesGeneriques

/**
 * Ajouter une description ici
 * @fileOverview
 * @author Tommy Barroy <t.barroy@laposte.net>
 * @since 2017-10
 */
export const params = {
  outils: ['mathquill', 'calculatrice'],
  /**
   * Ce qui est paramétrable dans l’éditeur de graphe
   * @type {LegacySectionParamsParametres}
   */
  parametres: [
    ['nbrepetitions', 9, 'entier', 'Nombre de répétitions de la section'],
    ['nbchances', 2, 'entier', 'Nombre d’essais possibles.'],
    ['limite', 0, 'entier', 'Nombre d’essais possibles.'],
    ['theme', 'standard', 'liste', 'Modèle pour l’affichage', ['standard', 'zonesAvecImageDeFond']]
  ]
}

/**
 * section fonction01
 * @this {Parcours}
 */
export default function main () {
  const me = this
  const stor = me.storage
  const ds = me.donneesSection

  function boutonfen () {
    j3pToggleFenetres('Calculatrice')
  }
  function suivEtape () {
    let i = stor.etapes.indexOf(stor.etapeEncours) + 1
    if (i > stor.etapes.length - 1) i = 0
    return stor.etapes[i]
  }
  function initSection () {
    // Construction de la page
    stor.etapes = ['form', 'trouvab', 'concl']
    stor.etapeEncours = 'concl'
    me.construitStructurePage('presentation1bis')
    ds.lesExos = []

    ds.nbetapes = 3
    ds.nbitems = ds.nbetapes * ds.nbrepetitions

    stor.casA = [-10, -9, -8, -7, -6, -5, -4, -3, -2, 2, 3, 4, 5, 6, 7, 8, 9, 10, 100, 20, -100, 50, -50]
    stor.apioche = j3pShuffle(stor.casA)

    me.typederreurs = [0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0]
    me.afficheTitre('Déterminer une fonction linéaire')
    enonceMain()
  }
  function calcule0 (form) {
    const form2 = String(form)
    const algbuf = Algebrite.run(form2)
    return (algbuf === '0' || algbuf === '-0' || algbuf === '0.0' || algbuf === '0.0...')
  }

  function creeLesDiv () {
    stor.lesdiv = {}
    stor.lesdiv.conteneur = j3pAddElt(me.zonesElts.MG, 'div', {
      style: me.styles.etendre('toutpetit.enonce', { padding: '10px' })
    })
    stor.lesdiv.consigne = addDefaultTable(stor.lesdiv.conteneur, 1, 1)[0][0]
    stor.lesdiv.calculatrice = j3pAddElt(me.zonesElts.MD, 'div', {
      style: me.styles.etendre('toutpetit.enonce', { padding: '10px' })
    })
    stor.lesdiv.correction = j3pAddElt(me.zonesElts.MD, 'div', {
      style: me.styles.etendre('toutpetit.enonce', { padding: '10px' })
    })
    stor.lesdiv.etape = addDefaultTable(stor.lesdiv.conteneur, 1, 1)[0][0]
    stor.lesdiv.travail = addDefaultTable(stor.lesdiv.conteneur, 1, 1)[0][0]
    stor.lesdiv.explications = j3pAddElt(stor.lesdiv.conteneur, 'div')
    stor.lesdiv.solution = j3pAddElt(stor.lesdiv.conteneur, 'div')
    stor.lesdiv.divCache = j3pAddElt(stor.lesdiv.conteneur, 'div')
    stor.lesdiv.divCache.style.display = 'none'
    stor.lesdiv.travail.classList.add('travail')
    stor.lesdiv.consigne.classList.add('enonce')
    me.zonesElts.MG.classList.add('fond')
    stor.lesdiv.etape.style.color = me.styles.colorCorrection
    stor.lesdiv.solution.style.color = me.styles.cbien
    stor.lesdiv.explications.style.color = me.styles.cfaux
  }
  function barreLesFo () {
    if (stor.etapeEncours === 'form') {
      if (stor.liste.isOk() === false) stor.liste.barre()
    }
    if (stor.etapeEncours === 'trouvab') {
      if (stor.zonex1.isOk() === false) stor.zonex1.barre()
      if (stor.zonex2.isOk() === false) stor.zonex2.barre()
    }
    if (stor.etapeEncours === 'concl') {
      stor.zonexx.barre()
      j3pBarre(stor.dou1)
    }
  }
  function enonceMain () {
    stor.etapeEncours = suivEtape()
    if (stor.etapeEncours === 'form') {
      stor.foco1 = false
      stor.foco2 = false
      me.videLesZones()
      creeLesDiv()
      poseQuestion0()
      me.fenetresjq = [{ name: 'Calculatrice', title: 'Une calculatrice', left: 600, top: 10 }]
      j3pCreeFenetres(me)
      j3pAfficheCroixFenetres('Calculatrice')
      j3pAjouteBouton(stor.lesdiv.calculatrice, boutonfen, { value: 'calculatrice', className: 'MepBoutons' })
    }
    poseQuestion()
    me.finEnonce()
  }
  function isRepOk () {
    stor.errDouble = false
    if (stor.etapeEncours === 'form') {
      return stor.liste.reponse === stor.listAt
    }
    if (stor.etapeEncours === 'trouvab') {
      const res1 = stor.zonex1.reponse()
      const res2 = stor.zonex2.reponse()
      const comp1 = String(stor.y1)
      const comp2 = stor.x1 + '*a'
      let ok = true
      if (!calcule0(res1 + '-(' + comp1 + ')') && !calcule0(res1 + '-(' + comp2 + ')')) {
        stor.zonex1.corrige(false)
        ok = false
      }
      if (!calcule0(res2 + '-(' + comp1 + ')') && !calcule0(res2 + '-(' + comp2 + ')')) {
        stor.zonex2.corrige(false)
        ok = false
      }
      if (calcule0(res2 + '-(' + res1 + ')')) {
        stor.zonex2.corrige(false)
        ok = false
        stor.errDouble = true
      }
      return ok
    }
    if (stor.etapeEncours === 'concl') {
      const res1 = stor.zonexx.reponse()
      if (Number(res1) !== stor.a) {
        stor.zonexx.corrige(false)
        return false
      }
      return true
    }
    return true
  }
  function modif (tab) {
    for (let i = 0; i < tab.length; i++) {
      for (let j = 0; j < tab[i].length; j++) {
        tab[i][j].style.padding = '0px'
      }
    }
  }
  function poseQuestion0 () {
    j3pAffiche(stor.lesdiv.consigne, null, 'La fonction $f$ est une fonction linéaire<br>')
    stor.x1 = j3pGetRandomInt(-10, 10)
    if (stor.x1 === 0) stor.x1 = 11
    if (stor.x1 === 1) stor.x1 = 12
    if (stor.x1 === -1) stor.x1 = -11
    if (stor.apioche.length === 0) stor.apioche = j3pShuffle(stor.casA)
    stor.a = stor.apioche.pop()
    stor.y1 = stor.x1 * stor.a
    j3pAffiche(stor.lesdiv.consigne, null, 'On sait que $f(' + stor.x1 + ')=' + stor.y1 + '$')
    j3pAffiche(stor.lesdiv.consigne, null, '<br><b>On veut déterminer la fonction $f$.</b><br><br>')
  }
  function poseQuestion () {
    j3pEmpty(stor.lesdiv.solution)
    j3pEmpty(stor.lesdiv.explications)
    stor.lesdiv.explications.classList.remove('explique')
    stor.lesdiv.solution.classList.remove('correction')
    if (stor.foco1) {
      j3pEmpty(stor.douko)
      j3pAffiche(stor.douko, null, '$f:\\text{ }x\\text{ }\\mapsto\\text{ } ax$')
      stor.douko.style.color = me.styles.colorCorrection
      stor.foco1 = false
    }
    if (stor.foco2) {
      j3pEmpty(stor.douko1)
      j3pEmpty(stor.douko2)
      j3pAffiche(stor.douko1, null, '$' + stor.y1 + '$')
      stor.douko1.style.color = me.styles.colorCorrection
      j3pAffiche(stor.douko2, null, '$' + stor.x1 + 'a$')
      stor.douko2.style.color = me.styles.colorCorrection

      j3pEmpty(stor.dou1)
      j3pEmpty(stor.dou2)
      j3pAffiche(stor.dou1, null, '$' + stor.y1 + '$')
      j3pAffiche(stor.dou2, null, '$' + stor.x1 + 'a$')
      stor.foco2 = false
    }
    if (stor.etapeEncours === 'form') {
      const ll = j3pShuffle(['$f:\\text{ }x\\text{ }\\mapsto\\text{ } ax$', '$f:\\text{ }x\\text{ }\\mapsto\\text{ }ax² + b$', '$f:\\text{ }x\\text{ }\\mapsto\\text{ }ax²$'])
      ll.splice(0, 0, 'Choisir')
      j3pAffiche(stor.lesdiv.etape, null, '<b><u>Étape 1</u>: Forme de $f$.<br><br></b>')
      const ttabl1 = addDefaultTable(stor.lesdiv.travail, 1, 2)
      modif(ttabl1)
      j3pAffiche(ttabl1[0][0], null, 'Comme la fonction $f$ est linéaire, elle est de la forme&nbsp;')
      stor.liste = ListeDeroulante.create(ttabl1[0][1], ll)
      stor.douko = ttabl1[0][1]
      stor.listAt = '$f:\\text{ }x\\text{ }\\mapsto\\text{ } ax$'
    }
    if (stor.etapeEncours === 'trouvab') {
      j3pEmpty(stor.lesdiv.etape)
      j3pAffiche(stor.lesdiv.etape, null, '<b><u>Étape 2</u>: Utilisation des données.<br><br></b>')
      const ttabl1 = addDefaultTable(stor.lesdiv.travail, 5, 1)
      modif(ttabl1)
      j3pAffiche(ttabl1[0][0], null, 'Déterminer la fonction $f$ revient donc à déterminer le nombre $a$.')
      j3pAffiche(ttabl1[1][0], null, '<br><b>Ecris de deux manières différentes $f(' + stor.x1 + ')$</b>')
      stor.avire = ttabl1[1][0]
      const ttablx1 = addDefaultTable(ttabl1[2][0], 1, 2)
      modif(ttablx1)
      const ttablx1d = addDefaultTable(ttablx1[0][1], 1, 4)
      modif(ttablx1d)
      j3pAddContent(ttablx1d[0][0], '&nbsp;Donc&nbsp;&nbsp;&nbsp;')
      stor.dou1 = ttablx1d[0][1]
      stor.dou2 = ttablx1d[0][3]
      j3pAddContent(ttablx1d[0][2], '&nbsp;=&nbsp;')
      j3pAffiche(stor.dou1, null, '$\\text{ _ _ }$')
      j3pAffiche(stor.dou2, null, '$\\text{ _ _ }$')
      const ttablx1g = addDefaultTable(ttablx1[0][0], 2, 3)
      modif(ttablx1g)
      j3pAffiche(ttablx1g[0][0], null, '$f(' + stor.x1 + ') = $&nbsp;')
      j3pAffiche(ttablx1g[1][0], null, '$f(' + stor.x1 + ') = $&nbsp;')
      j3pAffiche(ttablx1g[0][2], null, '&nbsp;&nbsp;')
      stor.zonex1 = new ZoneStyleMathquill1(ttablx1g[0][1], { restric: '0123456789,.-ab', afaire: afaire1, enter: () => { me.sectionCourante() } })
      stor.zonex2 = new ZoneStyleMathquill1(ttablx1g[1][1], { restric: '0123456789,.-ab', afaire: afaire2, enter: () => { me.sectionCourante() } })
      ttablx1[0][0].style.borderRight = '2px solid black'
      stor.douko1 = ttablx1g[0][1]
      stor.douko2 = ttablx1g[1][1]
    }
    if (stor.etapeEncours === 'concl') {
      j3pEmpty(stor.avire)
      j3pAffiche(stor.avire, null, '<br>')
      j3pEmpty(stor.lesdiv.etape)
      j3pAffiche(stor.lesdiv.etape, null, '<b><u>Étape 3</u>: Détermination de $f$.<br><br></b>')
      const ttabl1 = addDefaultTable(stor.lesdiv.travail, 5, 1)
      modif(ttabl1)
      j3pAffiche(ttabl1[1][0], null, '<br>')
      const ttablx1d = addDefaultTable(ttabl1[2][0], 1, 4)
      modif(ttablx1d)
      j3pAffiche(ttablx1d[0][0], null, 'Donc $f$ est la fonction définie par: $f:\\text{ }x\\text{ }\\mapsto\\text{ }$')
      j3pAffiche(ttablx1d[0][2], null, '$x$')
      stor.dou1 = ttablx1d[0][1]
      j3pAffiche(stor.dou1, null, '$\\text{ _ _ }$')
      const ttabl1d = addDefaultTable(ttabl1[0][0], 1, 2)
      modif(ttabl1d)
      j3pAffiche(ttabl1d[0][0], null, 'En utilisant l’équation ci dessus, <b>on trouve</b> $a =$&nbsp;')
      stor.zonexx = new ZoneStyleMathquill1(ttabl1d[0][1], { restric: '-0123456789,.', afaire: afaire3, enter: () => { me.sectionCourante() } })
    }
  }
  function afaire1 () {
    let acrire = stor.zonex1.reponse().replace(/ /g, '')
    if (acrire === '') acrire = '\\text{ _ _ }'
    j3pEmpty(stor.dou1)
    j3pAffiche(stor.dou1, null, '$' + acrire + '$')
  }
  function afaire2 () {
    let acrire = stor.zonex2.reponse().replace(/ /g, '')
    if (acrire === '') acrire = '\\text{ _ _ }'
    j3pEmpty(stor.dou2)
    j3pAffiche(stor.dou2, null, '$' + acrire + '$')
  }
  function afaire3 () {
    let acrire = stor.zonexx.reponse().replace(/ /g, '')
    if (acrire === '') acrire = '\\text{ _ _ }'
    j3pEmpty(stor.dou1)
    j3pAffiche(stor.dou1, null, '$' + acrire + '$')
  }

  function disaAll () {
    if (stor.etapeEncours === 'form') {
      stor.liste.disable()
    }
    if (stor.etapeEncours === 'trouvab') {
      stor.zonex1.disable()
      stor.zonex2.disable()
    }
    if (stor.etapeEncours === 'concl') {
      stor.zonexx.disable()
    }
  }
  function metTruetAll () {
    if (stor.etapeEncours === 'form') {
      stor.liste.corrige(true)
    }
    if (stor.etapeEncours === 'trouvab') {
      stor.zonex1.corrige(true)
      stor.zonex2.corrige(true)
    }
    if (stor.etapeEncours === 'concl') {
      stor.zonexx.corrige(true)
    }
  }

  function yaReponse () {
    if (stor.etapeEncours === 'form') {
      if (!stor.liste.changed) {
        stor.liste.focus()
        return false
      }
    }
    if (stor.etapeEncours === 'trouvab') {
      if (stor.zonex1.reponse() === '') {
        stor.zonex1.focus()
        return false
      }
      if (stor.zonex2.reponse() === '') {
        stor.zonex2.focus()
        return false
      }
    }
    if (stor.etapeEncours === 'concl') {
      if (stor.zonexx.reponse() === '') {
        stor.zonexx.focus()
        return false
      }
    }
    return true
  }

  function affCorrFaux (isFin) {
    if (stor.errDouble) {
      stor.yaexplik = true
      j3pAffiche(stor.lesdiv.explications, null, 'Tu a écrit 2 fois la même chose !')
    }
    if (stor.etapeEncours === 'form') stor.liste.corrige(false)

    if (isFin) {
      barreLesFo()
      disaAll()
      stor.yaco = true
      if (stor.etapeEncours === 'form') {
        stor.foco1 = true
        j3pAffiche(stor.lesdiv.solution, null, 'Une fonction linéaire est de la forme $f:\\text{ }x\\text{ }\\mapsto\\text{ } ax$')
      }
      if (stor.etapeEncours === 'trouvab') {
        stor.foco2 = true
        j3pAffiche(stor.lesdiv.solution, null, '$f(' + stor.x1 + ') = ' + stor.y1 + '$ <i>(dans les données)</i><br>')
        j3pAffiche(stor.lesdiv.solution, null, '$f(' + stor.x1 + ') = ' + stor.x1 + ' \\times a$ <i>(forme de $f$)</i><br>')
      }
      if (stor.etapeEncours === 'concl') {
        j3pAffiche(stor.lesdiv.solution, null, '$a = \\frac{' + stor.y1 + '}{' + stor.x1 + '} = ' + stor.a + '$ <br>')
        j3pAffiche(stor.lesdiv.solution, null, '$f$ est la fonction linéaire définie par $f:\\text{ }x\\text{ }\\mapsto\\text{ } ' + stor.a + 'x$')
      }
    }

    if (stor.yaexplik) stor.lesdiv.explications.classList.add('explique')
    if (stor.yaco) stor.lesdiv.solution.classList.add('correction')
  }

  switch (this.etat) {
    case 'enonce':
      // code exécuté au lancement de la section
      if (me.debutDeLaSection) {
        initSection()
      } else {
        enonceMain()
      }
      break // case "enonce":

    case 'correction':
      // On teste si une réponse a été saisie
      stor.yaexplik = false
      stor.yaco = false
      stor.lesdiv.explications.innerHTML = ''
      stor.lesdiv.correction.innerHTML = ''

      if (!yaReponse()) {
        // pas de réponse
        stor.lesdiv.correction.style.color = this.styles.cfaux
        stor.lesdiv.correction.innerHTML = reponseIncomplete
        // on reste dans l’état correction
        this.finCorrection()
        return
      }

      if (isRepOk()) {
        // Bonne réponse
        this.score++
        stor.lesdiv.correction.style.color = this.styles.cbien
        stor.lesdiv.correction.innerHTML = cBien
        this.typederreurs[0]++
        metTruetAll()
        disaAll()
        this.finCorrection('navigation', true)
        return
      }
      // Pas de bonne réponse
      stor.lesdiv.correction.style.color = this.styles.cfaux
      stor.lesdiv.correction.innerHTML = cFaux
      if (this.isElapsed) {
        // A cause de la limite de temps :
        stor.lesdiv.correction.innerHTML += tempsDepasse + '<BR>'
        this.typederreurs[10]++
        affCorrFaux(true)
        stor.yaco = true
        this.finCorrection('navigation', true)
        return
      }

      // Réponse fausse sans pb de temps
      if (me.essaiCourant < ds.nbchances) {
        stor.lesdiv.correction.innerHTML += '<br>' + essaieEncore
        affCorrFaux(false)
        // affCorrFaux.call(this, false)
        this.typederreurs[1]++
        this.finCorrection()
        return
      }
      // Erreur au dernier essai
      affCorrFaux(true)
      stor.yaco = true
      this.typederreurs[2]++
      // on donne la moitié des points pour une erreur d’arrondi
      if (stor.errArrond) this.score += 0.5
      this.finCorrection('navigation', true)
      break // case "correction":

    case 'navigation':
      if (!this.sectionTerminee()) this.etat = 'enonce'
      this.finNavigation()
      break // case "navigation":
  }
}
