import { j3pAddElt, j3pAjouteBouton, j3pArrondi, j3pEmpty, j3pGetRandomBool, j3pGetRandomInt, j3pShuffle } from 'src/legacy/core/functions'
import ListeDeroulante from 'src/legacy/outils/listeDeroulante/ListeDeroulante'
import { j3pAffiche } from 'src/lib/mathquill/functions'
import { addDefaultTable } from 'src/legacy/themes/table'
import ZoneStyleMathquill1 from 'src/legacy/outils/zoneStyleMathquill/ZoneStyleMathquill1'
import { j3pAfficheCroixFenetres, j3pCreeFenetres, j3pToggleFenetres } from 'src/legacy/core/functionsJqDialog'
import BrouillonCalculs from 'src/legacy/outils/brouillon/BrouillonCalculs'
import textesGeneriques from 'src/lib/core/textes'

const { cBien, cFaux, essaieEncore, reponseIncomplete, tempsDepasse } = textesGeneriques

/**
 * Ajouter une description ici
 * @fileOverview
 * @author Tommy Barroy <t.barroy@laposte.net>
 * @since 2017-10
 */

export const params = {
  outils: ['mathquill', 'calculatrice'],
  /**
   * Ce qui est paramétrable dans l’éditeur de graphe
   * @type {LegacySectionParamsParametres}
   */
  parametres: [
    ['nbrepetitions', 9, 'entier', 'Nombre de répétitions de la section'],
    ['nbchances', 2, 'entier', 'Nombre d’essais possibles.'],
    ['limite', 0, 'entier', 'Nombre d’essais possibles.'],
    ['theme', 'standard', 'liste', 'Modèle pour l’affichage', ['standard', 'zonesAvecImageDeFond']]
  ]
}

/**
 * section fonction04
 * @this {Parcours}
 */
export default function main () {
  const me = this
  const stor = me.storage
  const ds = me.donneesSection

  function boutonfen () {
    j3pToggleFenetres('Calculatrice')
  }
  function suivEtape () {
    let i = stor.etapes.indexOf(stor.etapeEncours) + 1
    if (i > stor.etapes.length - 1) i = 0
    return stor.etapes[i]
  }
  function initSection () {
    // Construction de la page
    stor.etapes = ['action', 'equ', 'concl']
    stor.etapeEncours = 'concl'
    me.construitStructurePage('presentation1bis')
    ds.lesExos = []

    ds.nbetapes = 3
    ds.nbitems = ds.nbetapes * ds.nbrepetitions

    stor.casA = [-10, -9, -8, -7, -6, -5, -4, -3, -2, -1, 1, 2, 3, 4, 5, 6, 7, 8, 9, 10, 100, 20, -100, 50, -50]
    stor.apioche = j3pShuffle(stor.casA)
    stor.casB = ['norm']
    if (ds.nbrepetitions > 1) stor.casB.push('x')
    if (ds.nbrepetitions > 2) stor.casB.push('y')
    if (ds.nbrepetitions > 3) stor.casB.push('norm')
    stor.apioche2 = j3pShuffle(stor.casB)
    me.typederreurs = [0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0]

    me.afficheTitre('Equation de droite et appartenance')
    enonceMain()
  }

  function creeLesDiv () {
    stor.lesdiv = {}
    stor.lesdiv.conteneur = j3pAddElt(me.zonesElts.MG, 'div', {
      style: me.styles.etendre('toutpetit.enonce', { padding: '10px' })
    })
    stor.lesdiv.consigne = addDefaultTable(stor.lesdiv.conteneur, 1, 1)[0][0]
    stor.lesdiv.calculatrice = j3pAddElt(me.zonesElts.MD, 'div', {
      style: me.styles.etendre('toutpetit.enonce', { padding: '10px' })
    })
    stor.lesdiv.correction = j3pAddElt(me.zonesElts.MD, 'div', {
      style: me.styles.etendre('toutpetit.enonce', { padding: '10px' })
    })
    stor.lesdiv.etape = addDefaultTable(stor.lesdiv.conteneur, 1, 1)[0][0]
    stor.lesdiv.travail = addDefaultTable(stor.lesdiv.conteneur, 1, 1)[0][0]
    stor.lesdiv.explications = j3pAddElt(stor.lesdiv.conteneur, 'div')
    stor.lesdiv.solution = j3pAddElt(stor.lesdiv.conteneur, 'div')
    stor.lesdiv.divCache = j3pAddElt(stor.lesdiv.conteneur, 'div')
    stor.lesdiv.divCache.style.display = 'none'
    stor.lesdiv.travail.classList.add('travail')
    stor.lesdiv.consigne.classList.add('enonce')
    me.zonesElts.MG.classList.add('fond')
    stor.lesdiv.etape.style.color = me.styles.colorCorrection
    stor.lesdiv.solution.style.color = me.styles.cbien
    stor.lesdiv.explications.style.color = me.styles.cfaux
  }

  function barreLesFo () {
    if (stor.etapeEncours === 'action') {
      if (stor.liste.isOk() === false) stor.liste.barre()
    }
    if (stor.etapeEncours === 'equ') {
      if (stor.zoneA.isOk() === false) stor.zoneA.barre()
      if (stor.zoneB.isOk() === false) stor.zoneB.barre()
    }
    if (stor.etapeEncours === 'concl') {
      if (stor.liste1.isOk() === false) stor.liste1.barre()
      if (stor.liste2.isOk() === false) stor.liste2.barre()
    }
  }
  function enonceMain () {
    stor.etapeEncours = suivEtape()
    if (stor.etapeEncours === 'action') {
      stor.foco1 = false
      stor.foco2 = false
      stor.foco3 = false
      stor.foco4 = false
      stor.focoA = false
      me.videLesZones()
      creeLesDiv()
      poseQuestion0()
      me.fenetresjq = [{ name: 'Calculatrice', title: 'Une calculatrice', left: 600, top: 10 }]
      j3pCreeFenetres(me)
      j3pAfficheCroixFenetres('Calculatrice')
      j3pAjouteBouton(stor.lesdiv.calculatrice, boutonfen, { value: 'calculatrice', className: 'MepBoutons' })
    }
    poseQuestion()
    me.finEnonce()
  }
  function isRepOk () {
    stor.errSigne = false
    stor.errCal = false
    if (stor.etapeEncours === 'action') {
      return stor.liste.reponse === stor.listAt
    }
    if (stor.etapeEncours === 'equ') {
      let ok = true
      const res1 = stor.zoneA.reponse()
      const res2 = stor.zoneB.reponse()
      stor.at1 = (stor.monCas === 'x') ? stor.x : stor.y
      stor.at2 = (stor.monCas === 'norm') ? j3pArrondi(stor.x * stor.a + stor.b, 0) : stor.b
      if (res1.indexOf('-') !== -1) {
        if (res1.indexOf('-') !== 0) {
          stor.errSigne = true
          ok = false
          stor.zoneA.corrige(false)
        }
        if (res1.lastIndexOf('-') !== 0) {
          stor.errSigne = true
          ok = false
          stor.zoneA.corrige(false)
        }
      }
      if (res2.indexOf('-') !== -1) {
        if (res2.indexOf('-') !== 0) {
          stor.errSigne = true
          ok = false
          stor.zoneB.corrige(false)
        }
        if (res2.lastIndexOf('-') !== 0) {
          stor.errSigne = true
          ok = false
          stor.zoneB.corrige(false)
        }
      }
      if (!ok) return false
      if (Number(res1) !== stor.at1) {
        stor.errCal = true
        ok = false
        stor.zoneA.corrige(false)
      }
      if (Number(res2) !== stor.at2) {
        stor.errCal = true
        ok = false
        stor.zoneB.corrige(false)
      }
      return ok
    }
    if (stor.etapeEncours === 'concl') {
      let ok = true
      const res1 = stor.liste1.reponse.indexOf('pas') === -1
      const res2 = stor.liste2.reponse.indexOf('pas') === -1
      if (stor.cok !== res1) {
        stor.liste1.corrige(false)
        ok = false
      }
      if (stor.cok !== res2) {
        stor.liste2.corrige(false)
        ok = false
      }
      return ok
    }
    return true
  }
  function modif (tab) {
    for (let i = 0; i < tab.length; i++) {
      for (let j = 0; j < tab[i].length; j++) {
        tab[i][j].style.padding = '0px'
      }
    }
  }

  function poseQuestion0 () {
    if (stor.apioche2.length === 0) stor.apioche2 = j3pShuffle(stor.casB)
    if (stor.apioche.length === 0) stor.apioche = j3pShuffle(stor.casA)
    stor.monCas = stor.apioche2.pop()
    stor.cok = j3pGetRandomBool()
    if (stor.monCas === 'norm') {
      stor.a = stor.apioche.pop()
      stor.b = j3pGetRandomInt(-10, 10)
      stor.leq = '$y = ' + fix(stor.a) + 'x ' + forceSigne(stor.b) + '$'
      stor.x = j3pGetRandomInt(-10, 10)
      stor.y = j3pArrondi(stor.x * stor.a + stor.b, 0)
      if (!stor.cok) {
        let dec = j3pGetRandomInt(1, 7)
        if (j3pGetRandomBool()) dec = -dec
        stor.y = j3pArrondi(stor.y + dec, 0)
      }
      stor.finleq = '$' + fix(stor.a) + 'x ' + forceSigne(stor.b) + '$'
    } else {
      stor.b = j3pGetRandomInt(-10, 10)
      stor.leq = '$' + stor.monCas + '= ' + stor.b + '$'
      stor.finleq = '$' + stor.b + '$'
      let dec = 0
      if (!stor.cok) {
        dec = j3pGetRandomInt(1, 7)
        if (j3pGetRandomBool()) dec = -dec
      }
      if (stor.monCas === 'x') {
        stor.x = stor.b + dec
        stor.y = j3pGetRandomInt(-10, 10)
      } else {
        stor.y = stor.b + dec
        stor.x = j3pGetRandomInt(-10, 10)
      }
    }
    j3pAffiche(stor.lesdiv.consigne, null, 'Une droite <i>(D)</i> a pour équation ' + stor.leq + ' ,<br>')
    j3pAffiche(stor.lesdiv.consigne, null, 'on veut déterminer si le point M ($' + stor.x + '$ , $' + stor.y + '$) appartient à  <i>(D)</i>.<br>')
  }
  function fix (a) {
    if (a === 1) return ''
    if (a === -1) return '-'
    return a
  }
  function poseQuestion () {
    j3pEmpty(stor.lesdiv.solution)
    j3pEmpty(stor.lesdiv.explications)
    j3pEmpty(stor.lesdiv.correction)
    stor.lesdiv.explications.classList.remove('explique')
    stor.lesdiv.solution.classList.remove('correction')
    if (stor.foco1) {
      j3pEmpty(stor.douko)
      j3pAffiche(stor.douko, null, stor.listAt)
      stor.douko.style.color = me.styles.colorCorrection
      stor.foco1 = false
    }
    if (stor.foco2) {
      j3pEmpty(stor.douko1)
      j3pEmpty(stor.douko2)
      stor.douko1.style.color = me.styles.colorCorrection
      stor.douko2.style.color = me.styles.colorCorrection
      stor.douko2.style.paddingLeft = '10px'
      stor.douko1.style.paddingRight = '10px'
      stor.douko2.style.borderLeft = '1px solid black'
      j3pAffiche(stor.douko1, null, '<i><u>Membre de gauche</u></i><br><br>')
      const tt = (stor.monCas === 'x') ? '$x=' : '$y='
      const arj = (stor.monCas === 'x') ? stor.x : stor.y
      j3pAffiche(stor.douko1, null, tt + arj + '$')
      stor.douko1.style.verticalAlign = 'top'
      j3pAffiche(stor.douko2, null, '<i><u>Membre de droite</u></i><br><br>')
      if (stor.monCas === 'norm') {
        j3pAffiche(stor.douko2, null, stor.finleq + '$=' + multa(stor.a) + faisParent(stor.x) + forceSigne(stor.b) + '$<br>')
        if (stor.a !== 1) j3pAffiche(stor.douko2, null, stor.finleq + '$=' + j3pArrondi(stor.a * stor.x, 0) + forceSigne(stor.b) + '$<br>')
        j3pAffiche(stor.douko2, null, stor.finleq + '$=' + j3pArrondi(stor.a * stor.x + stor.b, 0) + '$')
      } else {
        j3pAffiche(stor.douko2, null, stor.finleq + '$=' + stor.b + '$')
      }
      stor.foco2 = false
    }
    if (stor.etapeEncours === 'action') {
      const ll = j3pShuffle(['Tester l’équation ' + stor.leq + ' quand $x = ' + stor.x + '$ et $y = ' + stor.y + '$',
        'Tester l’équation ' + stor.leq + ' quand $x = ' + stor.y + '$ et $y = ' + stor.x + '$',
        'Tester l’équation ' + stor.leq])
      ll.splice(0, 0, 'Choisir')
      j3pAffiche(stor.lesdiv.etape, null, '<b><u>Étape 1</u>: Choix de la méthode.</b>')
      const ttabl1 = addDefaultTable(stor.lesdiv.travail, 1, 2)
      modif(ttabl1)
      j3pAffiche(ttabl1[0][0], null, 'Méthode:&nbsp;')
      stor.liste = ListeDeroulante.create(ttabl1[0][1], ll)
      stor.douko = ttabl1[0][1]
      stor.listAt = 'Tester l’équation ' + stor.leq + ' quand $x = ' + stor.x + '$ et $y = ' + stor.y + '$'
    }
    if (stor.etapeEncours === 'equ') {
      j3pEmpty(stor.lesdiv.etape)
      j3pAffiche(stor.lesdiv.etape, null, '<b><u>Étape 2</u>: Test de l’équation de <i>D</i> avec les coordonnées de M.</b>')
      j3pAffiche(stor.lesdiv.travail, null, '<br>')
      const ttabl1 = addDefaultTable(stor.lesdiv.travail, 1, 2)
      modif(ttabl1)
      ttabl1[0][0].style.verticalAlign = 'top'
      j3pAffiche(ttabl1[0][0], null, '<i><u>Membre de gauche:</u></i><br><br>')
      const ttabl2 = addDefaultTable(ttabl1[0][0], 1, 2)
      modif(ttabl2)
      const tt = (stor.monCas === 'x') ? '$x=$&nbsp;' : '$y=$&nbsp;'
      j3pAffiche(ttabl2[0][0], null, tt)
      stor.zoneA = new ZoneStyleMathquill1(ttabl2[0][1], { restric: '0123456789-', enter: () => { me.sectionCourante() } })
      stor.douko1 = ttabl1[0][0]

      ttabl1[0][1].style.paddingLeft = '10px'
      ttabl1[0][0].style.paddingRight = '10px'
      ttabl1[0][1].style.borderLeft = '1px solid black'
      j3pAffiche(ttabl1[0][1], null, '<i><u>Membre de droite:</u></i><br><br>')
      const ttabl3 = addDefaultTable(ttabl1[0][1], 2, 4)
      modif(ttabl3)
      j3pAffiche(ttabl3[0][0], null, stor.finleq + '&nbsp;')
      j3pAffiche(ttabl3[0][1], null, '$=$&nbsp;')
      stor.douko2 = (stor.monCas === 'norm') ? ttabl3[1][2] : ttabl3[0][2]
      stor.zoneB = new ZoneStyleMathquill1(stor.douko2, { restric: '0123456789-', enter: () => { me.sectionCourante() } })
      stor.douko2 = ttabl1[0][1]
      if (stor.monCas === 'norm') {
        j3pEmpty(ttabl3[0][1])
        j3pAffiche(ttabl3[1][0], null, stor.finleq + '&nbsp;')
        j3pAffiche(ttabl3[1][1], null, '$=$&nbsp;')
        stor.brouillon1 = new BrouillonCalculs({
          caseBoutons: ttabl3[0][0],
          caseEgal: ttabl3[0][1],
          caseCalculs: ttabl3[0][2],
          caseApres: ttabl3[0][3]
        },
        { limite: 30, limitenb: 20, restric: '0123456789()+-*/,.', hasAutoKeyboard: true, lmax: 1 })
      }
    }
    if (stor.etapeEncours === 'concl') {
      j3pEmpty(stor.lesdiv.etape)
      j3pAffiche(stor.lesdiv.etape, null, '<b><u>Étape 3</u>: Conclusion.</b>')
      j3pAffiche(stor.lesdiv.travail, null, '<br>')
      const ttabl1 = addDefaultTable(stor.lesdiv.travail, 2, 1)
      modif(ttabl1)
      const ttabl12 = addDefaultTable(ttabl1[0][0], 1, 3)
      modif(ttabl12)
      j3pAffiche(ttabl12[0][0], null, 'Le couple ($' + stor.x + '$ , $' + stor.y + '$)&nbsp;')
      j3pAffiche(ttabl12[0][2], null, '&nbsp;de l’équation de <i>(D)</i> ,<br>')
      const ll = ['Choisir', 'est une solution', 'n’est pas une solution']
      stor.liste1 = ListeDeroulante.create(ttabl12[0][1], ll)
      const ttabl13 = addDefaultTable(ttabl1[1][0], 1, 3)
      modif(ttabl13)
      j3pAffiche(ttabl13[0][0], null, 'donc le point M&nbsp;')
      j3pAffiche(ttabl13[0][2], null, '&nbsp;à la droite <i>(D)</i>.')
      const ll2 = ['Choisir', 'appartient', 'n’appartient pas']
      stor.liste2 = ListeDeroulante.create(ttabl13[0][1], ll2)
    }
  }

  function disaAll () {
    if (stor.etapeEncours === 'action') {
      stor.liste.disable()
    }
    if (stor.etapeEncours === 'equ') {
      stor.zoneA.disable()
      stor.zoneB.disable()
      if (stor.monCas === 'norm') stor.brouillon1.disable()
    }
    if (stor.etapeEncours === 'concl') {
      stor.liste1.disable()
      stor.liste2.disable()
    }
  }
  function metTruetAll () {
    if (stor.etapeEncours === 'action') {
      stor.liste.corrige(true)
    }
    if (stor.etapeEncours === 'equ') {
      stor.zoneA.corrige(true)
      stor.zoneB.corrige(true)
    }
    if (stor.etapeEncours === 'concl') {
      stor.liste1.corrige(true)
      stor.liste2.corrige(true)
    }
  }

  function yaReponse () {
    if (stor.etapeEncours === 'action') {
      if (!stor.liste.changed) {
        stor.liste.focus()
        return false
      }
    }
    if (stor.etapeEncours === 'equ') {
      if (stor.zoneA.reponse() === '') {
        stor.zoneA.focus()
        return false
      }
      if (stor.zoneB.reponse() === '') {
        stor.zoneB.focus()
        return false
      }
    }
    if (stor.etapeEncours === 'concl') {
      if (!stor.liste1.changed) {
        stor.liste1.focus()
        return false
      }
      if (!stor.liste2.changed) {
        stor.liste2.focus()
        return false
      }
    }
    return true
  }
  function faisParent (nb) {
    if (nb < 0) { return '(' + nb + ')' } else { return nb }
  }
  function forceSigne (nb) {
    if (nb < 0) { return nb } else { return ' +' + nb }
  }
  function multa (a) {
    if (a === 1) return ''
    return a + '\\times'
  }

  function affCorrFaux (isFin) {
    if (stor.errSigne) {
      stor.yaexplik = true
      j3pAffiche(stor.lesdiv.explications, null, 'Un nombre est mal écrit')
    }
    if (stor.errCal) {
      stor.yaexplik = true
      j3pAffiche(stor.lesdiv.explications, null, 'Erreur de calcul')
    }
    if (stor.etapeEncours === 'action') stor.liste.corrige(false)

    if (isFin) {
      stor.yaco = true
      if (stor.etapeEncours === 'action') {
        stor.foco1 = true
        j3pAffiche(stor.lesdiv.solution, null, 'Il faut tester l’équation de <i>D</i> en remplaçant $x$ et $y$ par l’abscisse et l’ordonnée de M.')
      }
      if (stor.etapeEncours === 'equ') {
        stor.foco2 = true
        const iuio = addDefaultTable(stor.lesdiv.solution, 1, 2)
        modif(iuio)
        iuio[0][1].style.paddingLeft = '10px'
        iuio[0][0].style.paddingRight = '10px'
        iuio[0][1].style.borderLeft = '1px solid black'
        j3pAffiche(iuio[0][0], null, '<i><u>Membre de gauche</u></i><br><br>')
        const tt = (stor.monCas === 'x') ? '$x=' : '$y='
        const arj = (stor.monCas === 'x') ? stor.x : stor.y
        j3pAffiche(iuio[0][0], null, tt + arj + '$')
        iuio[0][0].style.verticalAlign = 'top'
        j3pAffiche(iuio[0][1], null, '<i><u>Membre de droite</u></i><br><br>')
        if (stor.monCas === 'norm') {
          j3pAffiche(iuio[0][1], null, stor.finleq + '$=' + multa(stor.a) + faisParent(stor.x) + forceSigne(stor.b) + '$<br>')
          if (stor.a !== 1) j3pAffiche(iuio[0][1], null, stor.finleq + '$=' + j3pArrondi(stor.a * stor.x, 0) + forceSigne(stor.b) + '$<br>')
          j3pAffiche(iuio[0][1], null, stor.finleq + '$=' + j3pArrondi(stor.a * stor.x + stor.b, 0) + '$')
        } else {
          j3pAffiche(iuio[0][1], null, stor.finleq + '$=' + stor.b + '$')
        }
      }
      if (stor.etapeEncours === 'concl') {
        const ll = (stor.cok) ? 'est une solution' : 'n’est pas une solution'
        j3pAffiche(stor.lesdiv.solution, null, 'Le couple ($' + stor.x + '$ , $' + stor.y + '$)&nbsp;' + ll + '&nbsp;de l’équation de <i>(D)</i> ,<br>')
        const ll2 = (stor.cok) ? 'appartient' : 'n’appartient pas'
        j3pAffiche(stor.lesdiv.solution, null, 'donc le point M&nbsp;' + ll2 + '&nbsp;à la droite <i>(D)</i>.')
      }
      barreLesFo()
      disaAll()
    }

    if (stor.yaexplik) stor.lesdiv.explications.classList.add('explique')
    if (stor.yaco) stor.lesdiv.solution.classList.add('correction')
  }

  switch (this.etat) {
    case 'enonce':
      // code exécuté au lancement de la section
      if (me.debutDeLaSection) {
        initSection()
      } else {
        enonceMain()
      }
      break // case "enonce":

    case 'correction':
      // On teste si une réponse a été saisie
      stor.yaexplik = false
      stor.yaco = false
      stor.lesdiv.explications.innerHTML = ''
      stor.lesdiv.correction.innerHTML = ''

      if (!yaReponse()) {
        // pas de réponse
        stor.lesdiv.correction.style.color = this.styles.cfaux
        stor.lesdiv.correction.innerHTML = reponseIncomplete
        // on reste dans l’état correction
        this.finCorrection()
        return
      }

      if (isRepOk()) {
        // Bonne réponse
        this.score++
        stor.lesdiv.correction.style.color = this.styles.cbien
        stor.lesdiv.correction.innerHTML = cBien
        this.typederreurs[0]++
        metTruetAll()
        disaAll()
        this.finCorrection('navigation', true)
        return
      }
      // Pas de bonne réponse
      stor.lesdiv.correction.style.color = this.styles.cfaux
      stor.lesdiv.correction.innerHTML = cFaux
      if (this.isElapsed) {
        // A cause de la limite de temps :
        stor.lesdiv.correction.innerHTML += tempsDepasse + '<BR>'
        this.typederreurs[10]++
        affCorrFaux(true)
        stor.yaco = true
        this.finCorrection('navigation', true)
        return
      }

      // Réponse fausse sans pb de temps
      if (me.essaiCourant < ds.nbchances) {
        stor.lesdiv.correction.innerHTML += '<br>' + essaieEncore
        affCorrFaux(false)
        // affCorrFaux.call(this, false)
        this.typederreurs[1]++
        this.finCorrection()
        return
      }
      // Erreur au dernier essai
      affCorrFaux(true)
      stor.yaco = true
      this.typederreurs[2]++
      // on donne la moitié des points pour une erreur d’arrondi
      if (stor.errArrond) this.score += 0.5
      this.finCorrection('navigation', true)
      break // case "correction":

    case 'navigation':
      if (!this.sectionTerminee()) this.etat = 'enonce'
      this.finNavigation()
      break // case "navigation":
  }
}
