import { j3pAddElt, j3pAjouteBouton, j3pArrondi, j3pGetNewId, j3pGetRandomInt, j3pShowError } from 'src/legacy/core/functions'
import { j3pAffiche } from 'src/lib/mathquill/functions'
import { addDefaultTable } from 'src/legacy/themes/table'
import ZoneStyleMathquill2 from 'src/legacy/outils/zoneStyleMathquill/ZoneStyleMathquill2'
import { j3pAfficheCroixFenetres, j3pCreeFenetres, j3pToggleFenetres } from 'src/legacy/core/functionsJqDialog'
import { j3pCreeSVG } from 'src/legacy/core/functionsSvg'
import textesGeneriques from 'src/lib/core/textes'
import { getMtgCore } from 'src/lib/outils/mathgraph'

const { cBien, cFaux, essaieEncore, reponseIncomplete, tempsDepasse } = textesGeneriques

/**
 * Ajouter une description ici
 * @fileOverview
 * @author Tommy Barroy <t.barroy@laposte.net>
 * @since 2017-10
 */

export const params = {
  outils: ['mathquill', 'calculatrice'],
  /**
   * Ce qui est paramétrable dans l’éditeur de graphe
   * @type {LegacySectionParamsParametres}
   */
  parametres: [
    ['nbrepetitions', 9, 'entier', 'Nombre de répétitions de la section'],
    ['nbchances', 2, 'entier', 'Nombre d’essais possibles.'],
    ['limite', 0, 'entier', 'Nombre d’essais possibles.'],
    ['typeFonction', 'Affine', 'liste', '<u>Linéaire</u>: <i>f</i> est linéaire.  <br> <br> <u>Affine</u>: <i>f</i> est affine.', ['Linéaire', 'Affine']],
    ['Ecart_x', '1', 'liste', '<u>1</u>: L’abscisse du deuxième point est 1 ou -1.  <br> <br> <u>autre</u>: L’abscisse du 2e point n’est pas fixée.', ['1', 'autre']],
    ['theme', 'standard', 'liste', 'Modèle pour l’affichage', ['standard', 'zonesAvecImageDeFond']]
  ]
}

/**
 * section fonction01
 * @this {Parcours}
 */
export default function main () {
  const me = this
  const stor = me.storage
  const ds = me.donneesSection

  function boutonfen () {
    j3pToggleFenetres('Calculatrice')
  }
  function modif (tab) {
    for (let i = 0; i < tab.length; i++) {
      for (let j = 0; j < tab[i].length; j++) {
        tab[i][j].style.padding = '0px'
      }
    }
  }
  function initSection () {
    // Construction de la page
    me.construitStructurePage('presentation1bis')
    ds.lesExos = []

    const tt = (ds.typeFonction === 'linéaire') ? 'Retrouver le coefficient à partir du graphe' : 'Retrouver les coefficients à partir du graphe'

    me.afficheTitre(tt)
    getMtgCore({ withMathJax: true })
      .then(
        // success
        (mtgAppLecteur) => {
          stor.mtgAppLecteur = mtgAppLecteur
          enonceMain()
        },
        // failure
        (error) => {
          j3pShowError(error, { message: 'mathgraph n’est pas correctement chargé', mustNotify: true })
        })
      // plantage dans enonceMain
      .catch(j3pShowError)
  }
  function creeLesDiv () {
    stor.lesdiv = {}
    stor.lesdiv.conteneur = j3pAddElt(me.zonesElts.MG, 'div', {
      style: me.styles.etendre('toutpetit.enonce', { padding: '10px' })
    })
    stor.lesdiv.consigne = addDefaultTable(stor.lesdiv.conteneur, 1, 1)[0][0]
    stor.lesdiv.consigne.style.padding = '0px'
    stor.lesdiv.calculatrice = j3pAddElt(me.zonesElts.MD, 'div', {
      style: me.styles.etendre('toutpetit.enonce', { padding: '10px' })
    })
    stor.lesdiv.correction = j3pAddElt(me.zonesElts.MD, 'div', {
      style: me.styles.etendre('toutpetit.enonce', { padding: '10px' })
    })
    stor.lesdiv.travail = addDefaultTable(stor.lesdiv.conteneur, 1, 1)[0][0]
    stor.lesdiv.travail.style.padding = '0px'
    stor.lesdiv.explications = j3pAddElt(stor.lesdiv.conteneur, 'div')
    stor.lesdiv.solution = j3pAddElt(stor.lesdiv.conteneur, 'div')
    stor.lesdiv.divCache = j3pAddElt(stor.lesdiv.conteneur, 'div')
    stor.lesdiv.divCache.style.display = 'none'
    stor.lesdiv.travail.classList.add('travail')
    stor.lesdiv.consigne.classList.add('enonce')
    me.zonesElts.MG.classList.add('fond')
    stor.lesdiv.solution.style.color = me.styles.cbien
    stor.lesdiv.explications.style.color = me.styles.cfaux
  }
  function barreLesFo () {
    if (!stor.zoneA.isOk()) stor.zoneA.barre()
    if (ds.typeFonction !== 'Linéaire') {
      if (!stor.zoneB.isOk()) stor.zoneB.barre()
    }
  }
  function enonceMain () {
    me.videLesZones()
    creeLesDiv()
    poseQuestion()
    me.fenetresjq = [{ name: 'Calculatrice', title: 'Une calculatrice', left: 600, top: 10 }]
    j3pCreeFenetres(me)
    j3pAfficheCroixFenetres('Calculatrice')
    j3pAjouteBouton(stor.lesdiv.calculatrice, boutonfen, { value: 'calculatrice', className: 'MepBoutons' })
    me.finEnonce()
  }
  function isRepOk () {
    stor.errSensA = false
    stor.errSigne = false
    if (ds.typeFonction === 'Linéaire') {
      const repElANUM = String(stor.zoneA.reponse2()[0]).replace(/ /g, '')
      const repElADEN = String(stor.zoneA.reponse2()[1]).replace(/ /g, '')
      let ok = true
      if (!signeOk(repElANUM)) {
        ok = false
        stor.errSigne = true
        stor.zoneA.corrige(false)
      }
      if (!signeOk(repElADEN)) {
        ok = false
        stor.errSigne = true
        stor.zoneA.corrige(false)
      }
      if (Number(repElADEN) === 0) {
        ok = false
        stor.errSigne = true
        stor.zoneA.corrige(false)
      }
      if (!ok) return false
      if (!fracEgal({ num: Number(repElANUM), den: Number(repElADEN) }, { num: stor.y2 - stor.y1, den: stor.x2 })) {
        ok = false
        stor.zoneA.corrige(false)
        if (signe({ num: Number(repElANUM), den: Number(repElADEN) }) !== signe({ num: stor.y2 - stor.y1, den: stor.x2 })) stor.errSensA = true
      }
      return ok
    } else {
      const repElANUM = String(stor.zoneA.reponse2()[0]).replace(/ /g, '')
      const repElADEN = String(stor.zoneA.reponse2()[1]).replace(/ /g, '')
      const repElB = stor.zoneB.reponse().replace(/ /g, '')
      let ok = true
      if (!signeOk(repElANUM)) {
        ok = false
        stor.errSigne = true
        stor.zoneA.corrige(false)
      }
      if (!signeOk(repElADEN)) {
        ok = false
        stor.errSigne = true
        stor.zoneA.corrige(false)
      }
      if (!signeOk(repElB)) {
        ok = false
        stor.errSigne = true
        stor.zoneB.corrige(false)
      }
      if (Number(repElADEN) === 0) {
        ok = false
        stor.errSigne = true
        stor.zoneA.corrige(false)
      }
      if (!ok) return false
      if (!fracEgal({ num: Number(repElANUM), den: Number(repElADEN) }, { num: stor.y2 - stor.y1, den: stor.x2 })) {
        ok = false
        stor.zoneA.corrige(false)
        if (signe({ num: Number(repElANUM), den: Number(repElADEN) }) !== signe({ num: stor.y2 - stor.y1, den: stor.x2 })) stor.errSensA = true
      }
      if (Number(repElB) !== stor.y1) {
        ok = false
        stor.zoneB.corrige(false)
      }
      return ok
    }
  }
  function fracEgal (a, b) {
    return (a.num * b.den === a.den * b.num) && (b.den !== 0)
  }
  function signe (a) {
    if (a.num > 0) {
      return a.den > 0
    } else { return a.den < 0 }
  }
  function signeOk (x) {
    if (x.indexOf('-') === -1) return true
    if (x.indexOf('-') !== 0) return false
    if (x.indexOf('-') !== x.lastIndexOf('-')) return false
    return true
  }
  function poseQuestion () {
    const tt = addDefaultTable(stor.lesdiv.consigne, 3, 1)
    modif(tt)
    stor.y1 = (ds.typeFonction === 'Linéaire') ? 0 : j3pGetRandomInt(-3, 3)
    stor.x1 = 0
    do {
      stor.x2 = (ds.Ecart_x === '1') ? 1 : j3pGetRandomInt(-3, 4)
    } while (stor.x2 === 0)
    stor.y2 = j3pGetRandomInt(-3, 3)
    j3pAffiche(tt[0][0], null, 'On considère la fonction $f$ définie par le graphe suivant:&nbsp;<br>')
    j3pAffiche(tt[0][0], null, 'le graphe de $f$ est la droite bleue.')
    const txtFigure = 'TWF0aEdyYXBoSmF2YTEuMAAAABM+TMzNAAJmcv###wEA#wEAAAAAAAAAAANLAAACfwAAAQEAAAAAAAAAAQAAALv#####AAAAAQAKQ0NhbGNDb25zdAD#####AAJwaQAWMy4xNDE1OTI2NTM1ODk3OTMyMzg0Nv####8AAAABAApDQ29uc3RhbnRlQAkh+1RELRj#####AAAAAQAKQ1BvaW50QmFzZQD#####AQAAAAEQAAFEAAAAAAAAAAAAQAgAAAAAAAAAAAUAAUBdwAAAAAAAQGFcKPXCj1z#####AAAAAQAUQ0Ryb2l0ZURpcmVjdGlvbkZpeGUA#####wEAAAABEAAAAQAAAAEAAAABAD#3Cj1wo9cKAAAAAwD#####AQAAAAEQAAABAAAAAQAAAAEBP#cKPXCj1wr#####AAAAAQAPQ1BvaW50TGllRHJvaXRlAP####8BAAAAARAAAUUAAAAAAAAAAABACAAAAAAAAAAAAgABQDcAAAAAAAAAAAAD#####wAAAAEACUNDZXJjbGVPQQD#####AQAAAAAAAAEAAAABAAAABP####8AAAABABBDSW50RHJvaXRlQ2VyY2xlAP####8AAAACAAAABf####8AAAABABBDUG9pbnRMaWVCaXBvaW50AP####8AAAAAARAAAUYAAAAAAAAAAABACAAAAAAAAAAAAgACAAAABv####8AAAACAAdDUmVwZXJlAP####8A5ubmAAAAAQAAAAEAAAAEAAAABwEBAAAAAAEAAAAAAAAAAAAAAAEAAAAAAAAAAAAAAAE#8AAAAAAAAAAAAAE#8AAAAAAAAAAAAAQA#####wEAAAAAEAAAAAAAAAAAAAAAQAgAAAAAAAAAAAIAAcBWAAAAAAAAAAAAAgAAAAIA#####wEAAAAAEAAAAAAAAAAAAAAAQAgAAAAAAAAAAAIAAUBf6PXCj1woQDeuFHrhR7D#####AAAAAQAIQ1NlZ21lbnQA#####wAAAAAAEAAAAQAAAAEAAAAKAAAACf####8AAAABAA9DU3ltZXRyaWVBeGlhbGUA#####wAAAAL#####AAAAAQALQ1BvaW50SW1hZ2UA#####wEAAAAAEAAAAAAAAAAAAAAAQAgAAAAAAAAAAAIAAAAACgAAAAwAAAAJAP####8AAAAAABAAAAEAAAABAAAACQAAAA3#####AAAAAQAJQ0Ryb2l0ZUFCAP####8BAAAAABAAAAEAAAABAAAAAQAAAAT#####AAAAAQAjQ0F1dHJlUG9pbnRJbnRlcnNlY3Rpb25Ecm9pdGVDZXJjbGUA#####wAAAAAAEAAAAAAAAAAAAAAAQAgAAAAAAAAAAAIAAAAABQAAAA8AAAAE#####wAAAAEACENWZWN0ZXVyAP####8BAAAAABAAAAEAAAABAAAAAQAAABAA#####wAAAAEADUNUcmFuc1BhclZlY3QA#####wAAABEAAAALAP####8AAAAAABAAAAAAAAAAAAAAAEAIAAAAAAAAAAACAAAAABAAAAASAAAACwD#####AAAAAAAQAAAAAAAAAAAAAABACAAAAAAAAAAAAgAAAAATAAAAEgAAAAsA#####wAAAAAAEAAAAAAAAAAAAAAAQAgAAAAAAAAAAAIAAAAAFAAAABIAAAALAP####8AAAAAABAAAAAAAAAAAAAAAEAIAAAAAAAAAAACAAAAABUAAAASAAAABAD#####AQAAAAAQAAAAAAAAAAAAAABACAAAAAAAAAAABQABQFrAAAAAAAAAAAADAAAACQD#####AAAAAAAQAAABAAAAAQAAABYAAAAXAAAABwD#####AAAAAAAQAAAAAAAAAAAAAABACAAAAAAAAAAAAgABAAAABgAAAA4A#####wEAAAAAEAAAAQAAAAEAAAABAAAAGQAAAAAPAP####8AAAAaAAAACwD#####AAAAAAAQAAAAAAAAAAAAAABACAAAAAAAAAAAAgAAAAAZAAAAGwAAAAsA#####wAAAAAAEAAAAAAAAAAAAAAAQAgAAAAAAAAAAAIAAAAAHAAAABsAAAALAP####8AAAAAABAAAAAAAAAAAAAAAEAIAAAAAAAAAAACAAAAAB0AAAAbAAAACwD#####AAAAAAAQAAAAAAAAAAAAAABACAAAAAAAAAAAAgAAAAAeAAAAGwAAAAkA#####wAAAAAAEAAAAQAAAAEAAAAJAAAAH#####8AAAABAA5DT2JqZXREdXBsaXF1ZQD#####AAAAAAAAAAAABAAAABAA#####wAAAAAAAAAAAAf#####AAAAAgAMQ0NvbW1lbnRhaXJlAP####8AAAAAAMAYAAAAAAAAAAAAAAAAAAAAAAAAAAkQAAAAAAACAAAAAQAAAAEAAAAAAAAAAAAII0cjSWYoeCkAAAAQAP####8AAAAAAAAAAAAZAAAAAgD#####AQAAAAAQAAAAQAgAAAAAAAAAAAAAAAAAAAAACAAAQFPAAAAAAABAbzwo9cKPXP####8AAAABAAdDQ2FsY3VsAP####8ABW1pbmkyAAExAAAAAT#wAAAAAAAAAAAAEgD#####AAVtYXhpMgABNQAAAAFAFAAAAAAAAP####8AAAABABRDSW1wbGVtZW50YXRpb25Qcm90bwD#####AAdDdXJzZXVyAAAABQAAAAMAAAADAAAAJgAAACcAAAAlAAAAAwAAAAAoAQAAAAAQAAABAAAAAQAAACUBP#AAAAAAAAAAAAAEAQAAACgB#wAAABAAAADACAAAAAAAAD#wAAAAAAAAAAAFAAFAY0AAAAAAAAAAACn#####AAAAAQALQ0hvbW90aGV0aWUAAAAAKAAAACX#####AAAAAQAKQ09wZXJhdGlvbgP#####AAAAAQAPQ1Jlc3VsdGF0VmFsZXVyAAAAJgAAABUBAAAAFgAAACYAAAAWAAAAJwAAAAsAAAAAKAEAAAAADQACTzEAwBAAAAAAAABAEAAAAAAAAAAABQAAAAAqAAAAKwAAABQAAAAAKAAAACUAAAAVAwAAABUBAAAAAT#wAAAAAAAAAAAAFgAAACYAAAAVAQAAABYAAAAnAAAAFgAAACYAAAALAAAAACgBAAAAAA0AAkkxAMAAAAAAAAAAQAgAAAAAAAAAAAUAAAAAKgAAAC0AAAAJAQAAACgBAAAAABAAAAEAAAABAAAAJQAAACoAAAAEAQAAACgBAH8AARAAAmsxAMAAAAAAAAAAQAAAAAAAAAAAAAgAAT#g1Md7A1MeAAAALwAAABIA#####wAFbWluaTEAATEAAAABP#AAAAAAAAAAAAASAP####8ABW1heGkxAAE1AAAAAUAUAAAAAAAAAAAAAgD#####AQAAAAAQAAAAQAgAAAAAAAAAAAAAAAAAAAAABQABQE9rhR64UexAeq4UeuFHrgAAAAIA#####wEAAAAADgABVQDAJAAAAAAAAEAQAAAAAAAAAAAFAABALzGp++dsi0AvMan752yLAAAAAwD#####AQAAAAAQAAABAAAAAQAAADQBP#cKPXCj1woAAAAEAP####8BAAAAAA4AAVYAwAAAAAAAAABAEAAAAAAAAAAABQABQD8xqfvnbIsAAAA1AAAACQD#####AQAAAAAQAAABAAAAAQAAADQAAAA2#####wAAAAEAB0NNaWxpZXUA#####wEAAAAAEAAAAAAAAAAAAAAAQAgAAAAAAAAAAAUAAAAANAAAADYAAAARAP####8BAAAAAAAAAAAAAAAAQBgAAAAAAAAAAAAAADgMAAAAAAABAAAAAAAAAAEAAAAAAAAAAAABMf####8AAAABAAlDTG9uZ3VldXIA#####wAAADQAAAA2AAAAAgD#####AQAAAAAQAAAAAAAAAAAAAABACAAAAAAAAAAABQABwBNcKPXCj2BAW4UeuFHrhQAAABIA#####wABQQACMzAAAAABQD4AAAAAAAAAAAASAP####8AAUIAA0EqQQAAABUCAAAAFgAAADwAAAAWAAAAPAAAABIA#####wABQwABQgAAABYAAAA9AAAAEQD#####AQB#AAEAAP####8QQDgAAAAAAABAcW4UeuFHrgAAAAAAAAAAAAAAAAABAAAAAAAAAAAABEFMRUEAAAACAP####8BAAAAABAAAABACAAAAAAAAAAAAAAAAAAAAAAFAAFAYKzMzMzMzUByOFHrhR64AAAAEgD#####AAVtaW5pMwABMQAAAAE#8AAAAAAAAAAAABIA#####wAFbWF4aTMAAjEwAAAAAUAkAAAAAAAAAAAAEwD#####AAdDdXJzZXVyAAAABQAAAAUAAAADAAAAQQAAAEIAAABAAAAAAwAAAABDAQAAAAAQAAABAAAAAQAAAEABP#AAAAAAAAAAAAAEAQAAAEMB#wAAABAAAADACAAAAAAAAD#wAAAAAAAAAAAFAAFAXAAAAAAAAAAAAEQAAAAUAAAAAEMAAABAAAAAFQMAAAAWAAAAQQAAABUBAAAAFgAAAEEAAAAWAAAAQgAAAAsAAAAAQwEAAAAADQACTzEAwBAAAAAAAABAEAAAAAAAAAAABQAAAABFAAAARgAAABQAAAAAQwAAAEAAAAAVAwAAABUBAAAAAT#wAAAAAAAAAAAAFgAAAEEAAAAVAQAAABYAAABCAAAAFgAAAEEAAAALAAAAAEMBAAAAAA0AAkkxAMAAAAAAAAAAQAgAAAAAAAAAAAUAAAAARQAAAEgAAAAJAQAAAEMBAAAAABAAAAEAAAABAAAAQAAAAEUAAAAEAQAAAEMB#wD#ARAAAmsyAMAAAAAAAAAAQAAAAAAAAAAAAAgAAT#wAAAAAAAAAAAASv####8AAAACAA9DTWVzdXJlQWJzY2lzc2UBAAAAQwAEZGlteAAAAEcAAABJAAAAS#####8AAAABAA9DVmFsZXVyQWZmaWNoZWUBAAAAQwH#AAAAAAAAAAAAAADAGAAAAAAAAAAAAAAASw8AAf###wAAAAEAAAACAAAAAQAAAAAAAAAAAAAAAAAAAABMAAAAAgD#####AQAAAAAQAAAAQAgAAAAAAAAAAAAAAAAAAAAABQABQHMMKPXCj1tAURR64UeuFQAAABIA#####wAFbWluaTQAATEAAAABP#AAAAAAAAAAAAASAP####8ABW1heGk0AAIxMAAAAAFAJAAAAAAAAAAAABMA#####wAHQ3Vyc2V1cgAAAAUAAAAFAAAAAwAAAE8AAABQAAAATgAAAAMAAAAAUQEAAAAAEAAAAQAAAAEAAABOAT#wAAAAAAAAAAAABAEAAABRAf8AAAAQAAAAwAgAAAAAAAA#8AAAAAAAAAAABQABQFvAAAAAAAAAAABSAAAAFAAAAABRAAAATgAAABUDAAAAFgAAAE8AAAAVAQAAABYAAABPAAAAFgAAAFAAAAALAAAAAFEBAAAAAA0AAk8xAMAQAAAAAAAAQBAAAAAAAAAAAAUAAAAAUwAAAFQAAAAUAAAAAFEAAABOAAAAFQMAAAAVAQAAAAE#8AAAAAAAAAAAABYAAABPAAAAFQEAAAAWAAAAUAAAABYAAABPAAAACwAAAABRAQAAAAANAAJJMQDAAAAAAAAAAEAIAAAAAAAAAAAFAAAAAFMAAABWAAAACQEAAABRAQAAAAAQAAABAAAAAQAAAE4AAABTAAAABAEAAABRAf8A#wEQAAJrMwDAAAAAAAAAAEAAAAAAAAAAAAAIAAE#8AAAAAAAAAAAAFgAAAAZAQAAAFEABGRpbXkAAABVAAAAVwAAAFkAAAAaAQAAAFEB#wAAAAAAAAAAAAAAwBgAAAAAAAAAAAAAAFkPAAH###8AAAABAAAAAgAAAAEAAAAAAAAAAAAAAAAAAAAAWgAAABIA#####wAERElNWAAQaW50KGRpbXgpKjEwKzEwMAAAABUAAAAAFQL#####AAAAAgAJQ0ZvbmN0aW9uAgAAABYAAABMAAAAAUAkAAAAAAAAAAAAAUBZAAAAAAAAAAAAEgD#####AARESU1ZABBpbnQoZGlteSkqMTArMTAwAAAAFQAAAAAVAgAAABsCAAAAFgAAAFoAAAABQCQAAAAAAAAAAAABQFkAAAAAAAD#####AAAAAgAGQ0xhdGV4AP####8B#wAAAD#wAAAAAAAAQCIAAAAAAAAAAAAAAEsQAAAAAAABAAAAAAAAAAEAAAAAAAAAAAAKXFZhbHtESU1YfQAAABwA#####wH#AAAAwAgAAAAAAABAFAAAAAAAAAAAAAAAWRAAAAAAAAEAAAAAAAAAAQAAAAAAAAAAAApcVmFse0RJTVl9AAAAAgD#####AQAA#wAQAAFBAAAAAAAAAAAAQAgAAAAAAAAAAAUAAUBygeuFHrhSQHCCj1wo9cIAAAADAP####8BAAD#ARAAAAEAAAABAAAAYAE#9wo9cKPXCgAAAAQA#####wH#AAAAEAABQgAAAAAAAAAAAEAIAAAAAAAAAAAFAAHAYAAAAAAAAAAAAGEAAAADAP####8BAAD#ARAAAAEAAAABAAAAYAA#9wo9cKPXCgAAAAQA#####wEAAP8AEAABQwAAAAAAAAAAAEAIAAAAAAAAAAAFAAHAVYAAAAAAAAAAAGMAAAAIAP####8A5ubmAAAAAQAAAGAAAABiAAAAZAAAAAAAAAEAAAAAAAAAAAAAAAEAAAAAAAAAAAAAAAE#8AAAAAAAAAAAAAE#8AAAAAAAAAAAAAwA#####wEAAAAAEAAAAQAAAAEAAABgAAAAYgAAAAwA#####wEAAAAAEAAAAQAAAAEAAABgAAAAZP####8AAAABABBDUG9pbnREYW5zUmVwZXJlAP####8BAAD#ABAAAAAAAAAAAAAAAEAIAAAAAAAAAAAFAAAAAGUAAAAVAwAAABUBAAAAAUBpAAAAAAAAAAAAFgAAAFwAAAABQFkAAAAAAAAAAAAVAwAAABUBAAAAAUBpAAAAAAAAAAAAFgAAAF0AAAABQFkAAAAAAAAAAAAOAP####8BAAD#ABAAAAEAAAABAAAAYAAAAGQAAAAADgD#####AQAA#wAQAAABAAAAAQAAAGAAAABiAAAAAA8A#####wAAAGkAAAALAP####8BAAD#ABAAAAAAAAAAAAAAAEAIAAAAAAAAAAAFAAAAAGgAAABrAAAADwD#####AAAAagAAAAsA#####wEAAP8AEAAAAAAAAAAAAAAAQAgAAAAAAAAAAAUAAAAAaAAAAG3#####AAAAAQANQ0RlbWlEcm9pdGVPQQD#####Af8AAAANAAABAAACAwAAAGgAAABsAAAAHgD#####Af8AAAANAAABAAACAwAAAGgAAABuAAAAEQD#####Af8A#wEAAP####8SQFCAAAAAAABAbRwo9cKPXAAAAAAAAAAAAAAAAAABAAAAAAAAAAAACSNHbGFyZ2V1cgAAABEA#####wH#AP8BAAD#####EEBwsAAAAAAAQEfwo9cKPXAAAAAAAAAAAAAAAAAAAQAAAAAAAAAAAAkjR2hhdXRldXIAAAAcAP####8B#wAAAQAA#####xJAcKAAAAAAAEBgfCj1wo9cAAAAAAAAAAAAAAAAAAEAAAAAAAAAAAALQSA9IFxWYWx7QX0AAAAMAP####8BAAAAABAAAAEAAAABAAAAAQAAAAcAAAAOAP####8BAAAAABAAAAEAAAABAAAAAQAAAAQAAAAADgD#####AQAAAAAQAAABAAAAAQAAAAEAAAAHAAAAAA8A#####wAAAHUAAAALAP####8AAAAAABAAAAAAAAAAAAAAAEAIAAAAAAAAAAACAAAAAAQAAAB3AAAACwD#####AAAAAAAQAAAAAAAAAAAAAABACAAAAAAAAAAAAgAAAAB4AAAAdwAAAAsA#####wAAAAAAEAAAAAAAAAAAAAAAQAgAAAAAAAAAAAIAAAAAeQAAAHcAAAALAP####8BAAAAABAAAAAAAAAAAAAAAEAIAAAAAAAAAAAFAAAAAHoAAAB3AAAADwD#####AAAAdgAAAAsA#####wAAAAAAEAAAAAAAAAAAAAAAQAgAAAAAAAAAAAIAAAAABwAAAHwAAAALAP####8AAAAAABAAAAAAAAAAAAAAAEAIAAAAAAAAAAACAAAAAH0AAAB8AAAACwD#####AQAAAAAQAAAAAAAAAAAAAABACAAAAAAAAAAAAgAAAAB+AAAAfAAAABEA#####wAAAAAAAAAAAAAAAABACAAAAAAAAAAAAAAABBAAAAAAAAEAAAAAAAAAAQAAAAAAAAAAAAExAAAAEQD#####AAAAAAAAAAAAAAAAAEAIAAAAAAAAAAAAAAB4EAAAAAAAAQAAAAAAAAABAAAAAAAAAAAAATIAAAARAP####8AAAAAAL#wAAAAAAAAQAgAAAAAAAAAAAAAAHkQAAAAAAABAAAAAAAAAAEAAAAAAAAAAAABMwAAABEA#####wAAAAAAAAAAAAAAAABAEAAAAAAAAAAAAAAAehAAAAAAAAEAAAAAAAAAAQAAAAAAAAAAAAE0AAAAAgD#####AQAAAAAQAAAAAAAAAAAAAABACAAAAAAAAAAABQABQHCHCj1wo9ZAYEeuFHrhSAAAAAoA#####wAAAAMAAAALAP####8BAAAAABAAAAAAAAAAAAAAAEAIAAAAAAAAAAAFAAAAAIQAAACFAAAACQD#####AAAAAAAQAAABAAAAAQAAAIQAAAAXAAAACQD#####AAAAAAAQAAABAAAAAQAAAIYAAAAXAAAAEQD#####AAAAAABACAAAAAAAAMA1AAAAAAAAAAAAAAAXEAAAAAAAAQAAAAAAAAABAAAAAAAAAAAABSNJI0d4AAAAEQD#####AAAAAADACAAAAAAAAEAAAAAAAAAAAAAAAAAQEAAAAAAAAQAAAAAAAAABAAAAAAAAAAAAAi0xAAAAEQD#####AAAAAADAAAAAAAAAAEAIAAAAAAAAAAAAAAATEAAAAAAAAQAAAAAAAAABAAAAAAAAAAAAAi0yAAAAEQD#####AAAAAADAAAAAAAAAAEAAAAAAAAAAAAAAAAAUEAAAAAAAAQAAAAAAAAABAAAAAAAAAAAAAi0zAAAAEQD#####AAAAAADAGAAAAAAAAAAAAAAAAAAAAAJiMQAAAAcQAAAAAAACAAAAAQAAAAEAAAAAAAAAAAABMQAAABEA#####wAAAAAAwBwAAAAAAAA#8AAAAAAAAAACYjIAAAB9EAAAAAAAAgAAAAEAAAABAAAAAAAAAAAAATIAAAARAP####8AAAAAAMAYAAAAAAAAAAAAAAAAAAAAAmIzAAAAfhAAAAAAAAIAAAABAAAAAQAAAAAAAAAAAAEzAAAAEQD#####AAAAAADAIAAAAAAAAAAAAAAAAAAAAANiLTEAAAAZEAAAAAAAAgAAAAEAAAABAAAAAAAAAAAAAi0xAAAAEQD#####AAAAAADAIAAAAAAAAL#wAAAAAAAAAANiLTIAAAAcEAAAAAAAAgAAAAEAAAABAAAAAAAAAAAAAi0yAAAAEQD#####AAAAAADAIAAAAAAAAL#wAAAAAAAAAANiLTMAAAAdEAAAAAAAAgAAAAEAAAABAAAAAAAAAAAAAi0zAAAABAD#####AQAAAAAQAAF4AAAAAAAAAAAAQAgAAAAAAAAAAAUAAUAoKAxin705AAAADwAAABIA#####wACWTEAATMAAAABQAgAAAAAAAAAAAASAP####8AAlgyAAMxLjUAAAABP#gAAAAAAAAAAAASAP####8AAlkyAAMxLjUAAAABP#gAAAAAAAAAAAAUAP####8AAAABAAAAFgAAAJQAAAALAP####8A#wAAABAAAAAAAAAAAAAAAEAIAAAAAAAAAAAJAAAAAAcAAACXAAAAFAD#####AAAAAQAAABYAAACWAAAACwD#####AQAAfwAQAAAAAAAAAAAAAABACAAAAAAAAAAACAAAAAAHAAAAmQAAABQA#####wAAAAEAAAAWAAAAlQAAAAsA#####wEAAH8AEAAAAAAAAAAAAAAAQAgAAAAAAAAAAAgAAAAABAAAAJsAAAADAP####8BAAB#ARAAAAEAAAABAAAAnAA#8AAAAAAAAAAAAAMA#####wEAAH8BEAAAAQAAAAEAAACaAT#wAAAAAAAA#####wAAAAEAEENJbnREcm9pdGVEcm9pdGUA#####wD#AAAAEAAAAAAAAAAAAAAAQAgAAAAAAAAAAAkAAAAAnQAAAJ4AAAAMAP####8AAAD#ABAAAAEAAAACAAAAmAAAAJ8AAAAQAP####8A#wAAAAAAAACfAAAAEAD#####AP8AAAAAAAAAmAAAABcA#####wH#AAAAEAAAAAAAAAAAAAAAQAgAAAAAAAAAAAkAAAAAmgAAAJ8AAAADAP####8B#wAAARAAAAEAAAACAAAAowA#8AAAAAAAAP####8AAAACAAlDQ2VyY2xlT1IA#####wH#AAAAAAACAAAAowAAAAE#yZmZmZmZmgAAAAAGAP####8AAACkAAAApQAAAAcA#####wH#AAAAEAAAAAAAAAAAAAAAQAgAAAAAAAAAAAkAAgAAAKYAAAAHAP####8B#wAAABAAAAAAAAAAAAAAAEAIAAAAAAAAAAAJAAEAAACmAAAAHAD#####AX8AfwEAAmhiAAAAqBAAAAAAAAEAAAABAAAAAQAAAAAAAAAAAAIyYQAAABwA#####wF#AH8BAAJoaAAAAKcQAAAAAAABAAAAAQAAAAEAAAAAAAAAAAACMmEAAAAJAP####8BfwB#ABAAAAEABHNlZ2gBAQAAAJoAAACfAAAAAwD#####Af8AAAEQAAABAAAAAgAAAJgBP#AAAAAAAAAAAAADAP####8B#wAAARAAAAEAAAACAAAAnwA#8AAAAAAAAAAAAB8A#####wH#AAAAEAAAAAAAAAAAAAAAQAgAAAAAAAAAAAkAAAAArAAAAK0AAAAJAP####8BfwB#ABAAAAEAAAEBAAAAmAAAAK4AAAAJAP####8BfwB#ABAAAAEAAAEBAAAArgAAAJ8AAAAXAP####8B#wAAABAAAAAAAAAAAAAAAEAIAAAAAAAAAAAJAAAAAK4AAACfAAAAIAD#####Af8AAAAAAQIAAACxAAAAAT#JmZmZmZmaAAAAAAMA#####wH#AAABEAAAAQAAAQIAAACxAT#wAAAAAAAAAAAABgD#####AAAAswAAALIAAAAHAP####8B#wAAABAAAAAAAAAAAAAAAEAIAAAAAAAAAAAJAAIAAAC0AAAABwD#####Af8AAAAQAAAAAAAAAAAAAABACAAAAAAAAAAACQABAAAAtAAAABwA#####wF#AH8BAAJ2ZwAAALYQAAAAAAABAAAAAQAAAAEAAAAAAAAAAAACMmEAAAAcAP####8BfwB#AQACdmQAAAC1EAAAAAAAAQAAAAEAAAABAAAAAAAAAAAAAjJhAAAADgD#####AX8AfwAQAAABAARzZWd2AQEAAACuAAAAnwAAAAAOAP####8BfwB#ABAAAAEABXNlZ3YyAQEAAACfAAAArgAAAAA6##########8='
    const tt2 = addDefaultTable(tt[1][0], 1, 3)
    modif(tt2)
    stor.svgId = j3pGetNewId('mtg')
    const yy2 = j3pCreeSVG(tt2[0][0], {
      id: stor.svgId,
      width: 300,
      height: 259
    })
    yy2.style.border = '1px solid black'
    stor.mtgAppLecteur.removeAllDoc()
    stor.mtgAppLecteur.addDoc(stor.svgId, txtFigure, true)
    stor.mtgAppLecteur.giveFormula2(stor.svgId, 'Y2', stor.y2)
    stor.mtgAppLecteur.giveFormula2(stor.svgId, 'Y1', stor.y1)
    stor.mtgAppLecteur.giveFormula2(stor.svgId, 'X2', stor.x2)
    stor.mtgAppLecteur.calculateAndDisplayAll(true)

    j3pAffiche(tt2[0][1], null, '&nbsp;&nbsp;')
    j3pAffiche(tt2[0][2], null, '<i>Les coordonnées des<br>deux points rouges sont&nbsp;&nbsp;<br> des nombres entiers.</i>')
    const adire = (ds.typeFonction === 'Linéaire') ? 'le coefficient directeur $a$' : 'le coefficient directeur $a$ et l’ordonnée à l’origine $b$'
    j3pAffiche(tt[2][0], null, '<b>On veut déterminer ' + adire + ' de la fonction $f$</b>&nbsp;')
    const tt3 = addDefaultTable(stor.lesdiv.travail, 2, 2)
    modif(tt3)
    j3pAffiche(tt3[0][0], null, '$a=$&nbsp;')
    stor.zoneA = new ZoneStyleMathquill2(tt3[0][1], { limite: 5, restric: '-0123456789/', enter: () => { me.sectionCourante() } })
    if (ds.typeFonction !== 'Linéaire') {
      j3pAffiche(tt3[1][0], null, '$b=$&nbsp;')
      stor.zoneB = new ZoneStyleMathquill2(tt3[1][1], { limite: 5, restric: '-0123456789', enter: () => { me.sectionCourante() } })
    }
    stor.zoneA.focus()
  }
  function disaAll () {
    stor.zoneA.disable()
    if (ds.typeFonction !== 'Linéaire') {
      stor.zoneB.disable()
    }
  }
  function metTruetAll () {
    stor.zoneA.corrige(true)
    if (ds.typeFonction !== 'Linéaire') {
      stor.zoneB.corrige(true)
    }
  }
  function augmentDim2 () {
    if (stor.x2 > 0) {
      return (stor.y2 - stor.y1 > 0) ? 'augmente' : 'diminue'
    } else {
      return (stor.y2 - stor.y1 < 0) ? 'augmente' : 'diminue'
    }
  }
  function yaReponse () {
    if (stor.zoneA.reponse() === '') {
      stor.zoneA.focus()
      return false
    }
    if (ds.typeFonction !== 'Linéaire') {
      if (!stor.zoneB.reponse() === '') {
        stor.zoneB.focus()
        return false
      }
    }
    return true
  }
  function ajSigne () {
    return ((stor.y2 - stor.y1) / stor.x2 < 0) ? '-' : ''
  }
  function isAEntier () {
    const acomp = j3pArrondi((stor.y1 - stor.y2) / stor.x2, 1)
    const acomp2 = j3pArrondi((stor.y1 - stor.y2) / stor.x2, 0)
    return acomp === acomp2
  }

  function affCorrFaux (isFin) {
    if (stor.errSensA) {
      stor.yaexplik = true
      if ((stor.y2 - stor.y1) / stor.x2 === 0) {
        j3pAffiche(stor.lesdiv.explications, null, '$f$ est une fonction constante, donc $a=0$ !')
      } else if ((stor.y2 - stor.y1) / stor.x2 > 0) {
        j3pAffiche(stor.lesdiv.explications, null, '$f$ est une fonction croissante, donc $a>0$ !')
      } else {
        j3pAffiche(stor.lesdiv.explications, null, '$f$ est une fonction décroissante, donc $a<0$ !')
      }
    }
    if (stor.errSigne) {
      j3pAffiche(stor.lesdiv.explications, null, 'Problème d’écriture !')
    }
    if (!stor.errSigne && !stor.errSensA) {
      j3pAffiche(stor.lesdiv.explications, null, 'Erreur de calcul !')
    }

    if (isFin) {
      barreLesFo()
      disaAll()
      stor.yaco = true
      j3pAffiche(stor.lesdiv.solution, null, 'Quand $x$ augmente de $' + Math.abs(stor.x2) + '$, $y$ ' + augmentDim2() + ' de $' + Math.abs(stor.y1 - stor.y2) + '$,<br>')
      if (stor.y1 !== stor.y2) {
        stor.mtgAppLecteur.setVisible(stor.svgId, '#segh', true)
        if (((stor.y2 - stor.y1) / stor.x2) < 0) {
          if (stor.x2 > 0) {
            stor.mtgAppLecteur.setVisible(stor.svgId, '#segv', true)
          } else {
            stor.mtgAppLecteur.setVisible(stor.svgId, '#segv2', true)
          }
        } else {
          if (stor.x2 > 0) {
            stor.mtgAppLecteur.setVisible(stor.svgId, '#segv', true)
          } else {
            stor.mtgAppLecteur.setVisible(stor.svgId, '#segv2', true)
          }
        }
      }
      if (stor.x2 === 1) {
        j3pAffiche(stor.lesdiv.solution, null, 'donc $a=' + (stor.y2 - stor.y1) + '$<br>')
        if (stor.y2 < stor.y1) {
          stor.mtgAppLecteur.setVisible(stor.svgId, '#hb', true)
          stor.mtgAppLecteur.setText(stor.svgId, '#hb', '1')
        } else if (stor.y1 !== stor.y2) {
          stor.mtgAppLecteur.setVisible(stor.svgId, '#hh', true)
          stor.mtgAppLecteur.setText(stor.svgId, '#hh', '1')
        }
        if (stor.y1 !== stor.y2) {
          stor.mtgAppLecteur.setVisible(stor.svgId, '#vg', true)
          stor.mtgAppLecteur.setText(stor.svgId, '#vg', 'a')
        }
      } else if (stor.x2 === -1) {
        j3pAffiche(stor.lesdiv.solution, null, 'donc $a=' + (stor.y1 - stor.y2) + '$<br>')
        if (stor.y2 < stor.y1) {
          stor.mtgAppLecteur.setVisible(stor.svgId, '#hb', true)
          stor.mtgAppLecteur.setText(stor.svgId, '#hb', '1')
        } else if (stor.y1 !== stor.y2) {
          stor.mtgAppLecteur.setVisible(stor.svgId, '#hh', true)
          stor.mtgAppLecteur.setText(stor.svgId, '#hh', '1')
        }
        if (stor.y1 !== stor.y2) {
          stor.mtgAppLecteur.setVisible(stor.svgId, '#vd', true)
          stor.mtgAppLecteur.setText(stor.svgId, '#vd', 'a')
        }
      } else {
        j3pAffiche(stor.lesdiv.solution, null, 'donc $' + Math.abs(stor.x2) + 'a=' + ajSigne() + Math.abs(stor.y1 - stor.y2) + '$,<br>')
        if (isAEntier()) {
          j3pAffiche(stor.lesdiv.solution, null, 'soit $a=' + ajSigne() + Math.abs(j3pArrondi((stor.y1 - stor.y2) / stor.x2, 0)) + '$,<br>')
        } else {
          j3pAffiche(stor.lesdiv.solution, null, 'soit $a=' + ajSigne() + '\\frac{' + Math.abs(stor.y1 - stor.y2) + '}{' + Math.abs(stor.x2) + '}$,<br>')
        }
        if (stor.x2 > 0) {
          if (stor.y2 < stor.y1) {
            stor.mtgAppLecteur.setVisible(stor.svgId, '#hb', true)
            stor.mtgAppLecteur.setText(stor.svgId, '#hb', String(stor.x2))
          } else if (stor.y1 !== stor.y2) {
            stor.mtgAppLecteur.setVisible(stor.svgId, '#hh', true)
            stor.mtgAppLecteur.setText(stor.svgId, '#hh', String(stor.x2))
          }
          if (stor.y1 !== stor.y2) {
            stor.mtgAppLecteur.setVisible(stor.svgId, '#vg', true)
            stor.mtgAppLecteur.setText(stor.svgId, '#vg', stor.x2 + 'a')
          }
        } else {
          if (stor.y2 < stor.y1) {
            stor.mtgAppLecteur.setVisible(stor.svgId, '#hb', true)
            stor.mtgAppLecteur.setText(stor.svgId, '#hb', String(Math.abs(stor.x2)))
          } else if (stor.y1 !== stor.y2) {
            stor.mtgAppLecteur.setVisible(stor.svgId, '#hh', true)
            stor.mtgAppLecteur.setText(stor.svgId, '#hh', String(Math.abs(stor.x2)))
          }
          if (stor.y1 !== stor.y2) {
            stor.mtgAppLecteur.setVisible(stor.svgId, '#vd', true)
            stor.mtgAppLecteur.setText(stor.svgId, '#vd', Math.abs(stor.x2) + 'a')
          }
        }
      }
      if (ds.typeFonction !== 'Linéaire') {
        j3pAffiche(stor.lesdiv.solution, null, 'L’ordonnée à l’origine $b$ se lit sur l’axe des ordonnées<br>')
        j3pAffiche(stor.lesdiv.solution, null, '<i>(en vert sur la figure)</i> donc $b=' + stor.y1 + '$')
        stor.mtgAppLecteur.setColor(stor.svgId, '#b' + stor.y1, 0, 155, 0, true, 0)
        stor.mtgAppLecteur.setText(stor.svgId, '#b' + stor.y1, '1')
      }
    }

    if (stor.yaexplik) stor.lesdiv.explications.classList.add('explique')
    if (stor.yaco) stor.lesdiv.solution.classList.add('correction')
  }

  switch (this.etat) {
    case 'enonce':
      // code exécuté au lancement de la section
      if (me.debutDeLaSection) {
        initSection()
      } else {
        enonceMain()
      }
      break // case "enonce":

    case 'correction':
      // On teste si une réponse a été saisie
      stor.yaexplik = false
      stor.yaco = false
      stor.lesdiv.explications.innerHTML = ''
      stor.lesdiv.correction.innerHTML = ''
      if (stor.svgId) stor.mtgAppLecteur.setColor(stor.svgId, '#b1', 255, 0, 0, true, 0)

      if (!yaReponse()) {
        // pas de réponse
        stor.lesdiv.correction.style.color = this.styles.cfaux
        stor.lesdiv.correction.innerHTML = reponseIncomplete
        // on reste dans l’état correction
        this.finCorrection()
        return
      }

      if (isRepOk()) {
        // Bonne réponse
        this.score++
        stor.lesdiv.correction.style.color = this.styles.cbien
        stor.lesdiv.correction.innerHTML = cBien
        this.typederreurs[0]++
        metTruetAll()
        disaAll()
        this.finCorrection('navigation', true)
        return
      }
      // Pas de bonne réponse
      stor.lesdiv.correction.style.color = this.styles.cfaux
      stor.lesdiv.correction.innerHTML = cFaux
      if (this.isElapsed) {
        // A cause de la limite de temps :
        stor.lesdiv.correction.innerHTML += tempsDepasse + '<BR>'
        this.typederreurs[10]++
        affCorrFaux(true)
        stor.yaco = true
        this.finCorrection('navigation', true)
        return
      }

      // Réponse fausse sans pb de temps
      if (me.essaiCourant < ds.nbchances) {
        stor.lesdiv.correction.innerHTML += '<br>' + essaieEncore
        affCorrFaux(false)
        // affCorrFaux.call(this, false)
        this.typederreurs[1]++
        this.finCorrection()
        return
      }
      // Erreur au dernier essai
      affCorrFaux(true)
      stor.yaco = true
      this.typederreurs[2]++
      // on donne la moitié des points pour une erreur d’arrondi
      if (stor.errArrond) this.score += 0.5
      this.finCorrection('navigation', true)
      break // case "correction":

    case 'navigation':
      if (!this.sectionTerminee()) this.etat = 'enonce'
      this.finNavigation()
      break // case "navigation":
  }
}
