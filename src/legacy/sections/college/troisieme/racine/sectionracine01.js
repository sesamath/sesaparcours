import { j3pAddElt, j3pAddTxt, j3pAjouteBouton, j3pArrondi, j3pGetRandomBool, j3pGetRandomInt, j3pShuffle } from 'src/legacy/core/functions'
import { verifNombreBienEcrit } from 'src/legacy/outils/fonctions/ecritureNombre'
import { j3pAffiche } from 'src/lib/mathquill/functions'
import ZoneStyleMathquill1 from 'src/legacy/outils/zoneStyleMathquill/ZoneStyleMathquill1'
import { addDefaultTable, addDefaultTableGrid } from 'src/legacy/themes/table'
import './racine.scss'
import { j3pAfficheCroixFenetres, j3pCreeFenetres, j3pToggleFenetres } from 'src/legacy/core/functionsJqDialog'
import textesGeneriques from 'src/lib/core/textes'

const { cBien, cFaux, essaieEncore, reponseIncomplete, tempsDepasse } = textesGeneriques

/**
 * Ajouter une description ici
 * @fileOverview
 * @author Tommy Barroy <t.barroy@laposte.net>
 * @since 2017-10
 */
export const params = {
  outils: ['calculatrice', 'mathquill'],
  /**
   * Ce qui est paramétrable dans l’éditeur de graphe
   * @type {LegacySectionParamsParametres}
   */
  parametres: [
    ['nbrepetitions', 9, 'entier', 'Nombre de répétitions de la section'],
    ['indication', '', 'string', 'Indication pour chaque répétition'],
    ['limite', 0, 'entier', 'Temps disponible par répétition, en secondes'],
    ['nbchances', 1, 'entier', 'Nombre d’essais par répétition'],
    ['Sanctionne_arrondi', false, 'boolean', 'Compte faux pour la mauvaise valeur approchée (défaut / excès)'],
    ['theme', 'standard', 'liste', 'Modèle pour l’affichage', ['standard', 'zonesAvecImageDeFond']]
  ]
}

/**
 * section racine01
 * @this {Parcours}
 */
export default function main () {
  const me = this
  const stor = me.storage
  const ds = me.donneesSection

  function boutonfen () {
    j3pToggleFenetres('Calculatrice')
  }
  function yareponse () {
    if (me.isElapsed) return true
    if (stor.zone.reponse() === '') {
      stor.zone.focus()
      return false
    }
    if (stor.questEncours === 2) {
      if (stor.zone2.reponse() === '') {
        stor.zone2.focus()
        return false
      }
    }
    return true
  }
  function isRepOk () {
    stor.errMauv1 = ''
    stor.errMauv2 = ''
    stor.errCalcul = false
    stor.errDouble = false
    stor.errEcris = ''
    stor.errArrondi = false
    const rep = Number(stor.zone.reponse().replace(/ /g, ''))
    let ok = true
    switch (stor.questEncours) {
      case 1:
        if (stor.repAt !== rep) {
          stor.zone.corrige(false)
          ok = false
          stor.errMauv1 = String(rep)
        }
        break
      case 2: {
        const rep2 = Number(stor.zone2.reponse().replace(/ /g, ''))
        if (rep !== stor.repAt && rep !== -stor.repAt) {
          stor.errCalcul = true
          stor.zone.corrige(false)
          ok = false
        }
        if (rep2 !== stor.repAt && rep2 !== -stor.repAt) {
          stor.errCalcul = true
          stor.zone2.corrige(false)
          ok = false
        }
        if (rep === rep2) {
          stor.errDouble = true
          stor.zone2.corrige(false)
          ok = false
        }
      }
        break
      case 3:
        if (stor.repAt !== rep) {
          stor.zone.corrige(false)
          ok = false
          stor.errMauv2 = String(rep)
        }
        break
      case 4: {
        const tt = verifNombreBienEcrit(stor.zone.reponse(), false, false)
        if (!tt.good) {
          stor.zone.corrige(false)
          stor.errEcris = tt.remede
          return false
        }
        if (tt.nb === stor.repAt) return true
        if (tt.nb === stor.repAt2) {
          if (!ds.Sanctionne_arrondi) {
            j3pAffiche(stor.lesdiv.solution, null, 'Le bon arrondi est $' + stor.repAt + '$')
            stor.lesdiv.solution.classList.add('correction')
            return true
          } else {
            stor.zone.corrige(false)
            stor.errArrondi = true
            return false
          }
        }
        stor.errCalcul = true
        stor.zone.corrige(false)
        return false
      }
    }
    return ok
  }
  function initSection () {
    // Construction de la page
    me.construitStructurePage('presentation1')

    // DECLARATION DES VARIABLES
    // reponse attendue
    ds.nbitems = ds.nbetapes * ds.nbrepetitions
    stor.numerateur = 1
    stor.denominateur = 1

    // contiendra soit consigne1 soit consigne2 (1 fois sur 2)
    stor.consigneaffichee = ''
    /*
             Par convention,`
            `   me.typederreurs[0] = nombre de bonnes réponses
                me.typederreurs[1] = nombre de fois où l’élève a utilisé une chance supplémentaire
                me.typederreurs[2] = nombre de mauvaises réponses
                me.typederreurs[10] = nombre de fois où l’élève a eu faux à cause d’un temps limite
                LES AUTRES peuvent être utilisées par la détermination de la pe dans le cas d’une section qualitative
             */
    me.typederreurs = [0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0]
    const questPos = j3pShuffle([1, 2, 3, 4])
    stor.quest = []
    for (let i = 0; i < ds.nbitems; i++) {
      stor.quest.push(questPos[i % 3])
    }
    stor.quest = j3pShuffle(stor.quest)

    me.score = 0

    me.afficheTitre('Racine carrée')

    if (me.donneesSection.indication) me.indication(me.zones.IG, me.donneesSection.indication)
    enonceMain()
  }
  function enonceMain () {
    me.videLesZones()
    stor.lesdiv = {}
    stor.lesdiv.conteneur = j3pAddElt(me.zonesElts.MG, 'div', {
      style: me.styles.etendre('toutpetit.enonce', { padding: '10px' })
    })
    // Dans ce conteneur, on crée un div par phrase (cela forcera le retour à la ligne)
    stor.lesdiv.consigneG = addDefaultTable(stor.lesdiv.conteneur, 1, 1)[0][0]
    stor.lesdiv.etape = addDefaultTable(stor.lesdiv.conteneur, 1, 1)[0][0]
    stor.lesdiv.travail = addDefaultTable(stor.lesdiv.conteneur, 1, 1)[0][0]
    j3pAddTxt(stor.lesdiv.etape, '&nbsp;')
    stor.lesdiv.calculatrice = j3pAddElt(me.zonesElts.MD, 'div', {
      style: me.styles.etendre('toutpetit.enonce', { padding: '10px' })
    })
    stor.lesdiv.correction = j3pAddElt(me.zonesElts.MD, 'div', {
      style: me.styles.etendre('toutpetit.enonce', { padding: '10px' })
    })
    stor.lesdiv.explications = j3pAddElt(stor.lesdiv.conteneur, 'div')
    stor.lesdiv.solution = j3pAddElt(stor.lesdiv.conteneur, 'div')
    stor.lesdiv.explications.style.color = me.styles.cfaux
    stor.lesdiv.solution.style.color = me.styles.colorCorrection
    me.zonesElts.MG.classList.add('fond')
    stor.lesdiv.travail.classList.add('travail')
    stor.lesdiv.consigneG.classList.add('enonce')
    stor.questEncours = stor.quest.pop()
    me.fenetresjq = [{ name: 'Calculatrice', title: 'Une calculatrice', left: 600, top: 10 }]
    j3pCreeFenetres(me)
    j3pAfficheCroixFenetres('Calculatrice')
    j3pAjouteBouton(stor.lesdiv.calculatrice, boutonfen, { className: 'MepBoutons', value: 'Calculatrice' })
    switch (stor.questEncours) {
      case 1:
        {
          j3pAffiche(stor.lesdiv.consigneG, null, '<b>Complète cette phrase</b>')
          stor.repAt = j3pGetRandomInt(0, 12)
          const tab = addDefaultTableGrid(stor.lesdiv.travail, 1, 3)
          if (j3pGetRandomBool()) {
            j3pAffiche(tab[0][0], null, 'La racine carrée de $' + stor.repAt * stor.repAt + '$ est&nbsp;')
            j3pAffiche(tab[0][2], null, '&nbsp;.')
          } else {
            j3pAffiche(tab[0][0], null, '$\\sqrt{' + stor.repAt * stor.repAt + '} = $&nbsp;')
          }
          stor.zone = new ZoneStyleMathquill1(tab[0][1], { enter: () => { me.sectionCourante() }, restric: '0123456789', limite: 4 })
        }
        break
      case 2:
        {
          j3pAffiche(stor.lesdiv.consigneG, null, '<b>Complète cette phrase</b>')
          stor.repAt = j3pGetRandomInt(1, 12)
          const tab = addDefaultTableGrid(stor.lesdiv.travail, 1, 5)
          j3pAffiche(tab[0][0], null, 'Les nombres&nbsp;')
          j3pAffiche(tab[0][2], null, '&nbsp;et&nbsp;')
          j3pAffiche(tab[0][4], null, '&nbsp;ont pour carré $' + stor.repAt * stor.repAt + '$.')
          stor.zone = new ZoneStyleMathquill1(tab[0][1], { enter: () => { me.sectionCourante() }, restric: '-0123456789', limite: 4 })
          stor.zone2 = new ZoneStyleMathquill1(tab[0][3], { enter: () => { me.sectionCourante() }, restric: '-0123456789', limite: 4 })
        }
        break
      case 3:
        {
          j3pAffiche(stor.lesdiv.consigneG, null, '<b>Complète cette phrase</b>')
          const rep = j3pGetRandomInt(1, 12)
          stor.repAt = rep * rep
          const tab = addDefaultTableGrid(stor.lesdiv.travail, 1, 3)
          stor.laf = j3pGetRandomBool()
          if (stor.laf) {
            j3pAffiche(tab[0][0], null, 'Le nombre&nbsp;')
            j3pAffiche(tab[0][2], null, '&nbsp;a pour racine carrée $' + rep + '$.')
            stor.zone = new ZoneStyleMathquill1(tab[0][1], { enter: () => { me.sectionCourante() }, restric: '0123456789', limite: 4 })
          } else {
            const yy = affficheRacine(tab[0][0])
            stor.zone = new ZoneStyleMathquill1(yy.where, { enter: () => { me.sectionCourante() }, restric: '0123456789', limite: 4 })
            j3pAffiche(tab[0][1], null, '&nbsp; $ = ' + rep + '$.')
            tab[0][1].style.paddingTop = '5px'
          }
        }
        break
      case 4:
        {
          j3pAffiche(stor.lesdiv.consigneG, null, '<b>Calcule avec une calculatrice</b>')
          do {
            stor.repAt = j3pGetRandomInt(1, 99)
          } while ([1, 4, 9, 16, 25, 36, 49, 64, 91].indexOf(stor.repAt) !== -1)
          stor.valDeb = stor.repAt
          const tab = addDefaultTableGrid(stor.lesdiv.travail, 1, 3)
          stor.zone = new ZoneStyleMathquill1(tab[0][1], { enter: () => { me.sectionCourante() }, restric: '0123456789,.', limite: 4 })
          j3pAffiche(tab[0][0], null, '$\\sqrt{' + stor.repAt + '} \\approx$&nbsp;')
          stor.lar = j3pGetRandomInt(1, 3)
          stor.list = ['au dixième', 'au centième', 'au millième']
          j3pAffiche(tab[0][2], null, '&nbsp; <i>(valeur arrondie ' + stor.list[stor.lar - 1] + ')</i>')
          stor.repAt = j3pArrondi(Math.sqrt(stor.valDeb), stor.lar)
          const pass = Math.sqrt(stor.valDeb) * Math.pow(10, stor.lar)
          const v1 = j3pArrondi(Math.ceil(pass) / Math.pow(10, stor.lar), stor.lar)
          const v2 = j3pArrondi(Math.floor(pass) / Math.pow(10, stor.lar), stor.lar)
          stor.repAt2 = (v1 === stor.repAt) ? v2 : v1
        }
        break
    }
    me.finEnonce()
  }
  function affcofo (isFin) {
    let yaexplik = false
    if (stor.errMauv1 !== '') {
      yaexplik = true
      j3pAffiche(stor.lesdiv.explications, null, '$' + stor.errMauv1 + ' \\times ' + stor.errMauv1 + ' \\ne ' + stor.repAt * stor.repAt + '$ !')
    }
    if (stor.errMauv2 !== '') {
      yaexplik = true
      j3pAffiche(stor.lesdiv.explications, null, '$' + j3pArrondi(Math.sqrt(stor.repAt), 0) + '^2 ' + stor.errMauv1 + ' \\ne ' + stor.errMauv2 + '$ !')
    }
    if (stor.errCalcul) {
      yaexplik = true
      j3pAffiche(stor.lesdiv.explications, null, 'Erreur de calcul !')
    }
    if (stor.errDouble) {
      yaexplik = true
      j3pAffiche(stor.lesdiv.explications, null, 'Tu écris deux fois la même valeur !')
    }
    if (stor.errEcris !== '') {
      yaexplik = true
      j3pAffiche(stor.lesdiv.explications, null, stor.errEcris)
    }
    if (stor.errArrondi) {
      yaexplik = true
      j3pAffiche(stor.lesdiv.explications, null, 'Erreur d’arrondi !')
    }
    if (isFin) {
      barrelesfo()
      disaTout()
      switch (stor.questEncours) {
        case 1:
          j3pAffiche(stor.lesdiv.solution, null, 'La réponse attendue est $' + stor.repAt + '$ car $' + stor.repAt + ' \\times ' + stor.repAt + ' = ' + stor.repAt * stor.repAt + '$')
          break
        case 2:
          j3pAffiche(stor.lesdiv.solution, null, 'Les nombres $' + stor.repAt + '$ et $' + -stor.repAt + '$ ont pour carré $' + stor.repAt * stor.repAt + '$.')
          break
        case 3:
          if (stor.laf) {
            j3pAffiche(stor.lesdiv.solution, null, 'Le nombre $' + stor.repAt + '$ a pour racine carrée $' + j3pArrondi(Math.sqrt(stor.repAt), 0) + '$,<br>')
          } else {
            j3pAffiche(stor.lesdiv.solution, null, '$\\sqrt{' + stor.repAt + '} = ' + j3pArrondi(Math.sqrt(stor.repAt), 0) + '$,<br>')
          }
          j3pAffiche(stor.lesdiv.solution, null, 'car $' + j3pArrondi(Math.sqrt(stor.repAt), 0) + ' \\times ' + j3pArrondi(Math.sqrt(stor.repAt), 0) + ' = ' + stor.repAt + '$.')
          break
        case 4:
          j3pAffiche(stor.lesdiv.solution, null, '$\\sqrt{' + stor.valDeb + '} \\approx ' + stor.repAt + '$')
          break
      }
      stor.lesdiv.solution.classList.add('correction')
    }
    if (yaexplik) stor.lesdiv.explications.classList.add('explique')
  }
  function affficheRacine (ou) {
    const aa = addDefaultTable(ou, 1, 3)
    aa[0][0].classList.add('MontreRacine')
    j3pAffiche(aa[0][0], null, '&nbsp;')
    j3pAffiche(aa[0][1], null, '&nbsp;')
    aa[0][1].style.borderTop = '1px solid black'
    aa[0][2].style.borderTop = '1px solid black'
    const aj = j3pAddElt(aa[0][2], 'div')
    return { where: aj }
  }
  function metvert () {
    stor.zone.corrige(true)
    if (stor.questEncours === 2) {
      stor.zone2.corrige(true)
    }
  }
  function disaTout () {
    stor.zone.disable()
    if (stor.questEncours === 2) {
      stor.zone2.disable()
    }
  }
  function barrelesfo () {
    if (stor.zone.isOk() === false) {
      stor.zone.barre()
    }
    if (stor.questEncours === 2) {
      if (stor.zone2.isOk() === false) {
        stor.zone2.barre()
      }
    }
  }

  switch (this.etat) {
    case 'enonce':
      // code exécuté au lancement de la section
      if (this.debutDeLaSection) {
        initSection()
      } else {
        enonceMain()
      }
      break // case "enonce":

    case 'correction':
      // On teste si une réponse a été saisie
      stor.lesdiv.explications.classList.remove('explique')
      if (!yareponse()) {
        // ici on donne le message à afficher si une ou plusieurs zone(s) n’est pas complétée (tout d’abord on met en rouge : styles.cfaux)
        stor.lesdiv.correction.style.color = this.styles.cfaux
        stor.lesdiv.correction.innerHTML = reponseIncomplete
        this.afficheBoutonValider()
      } else {
        // Bonne réponse
        if (isRepOk()) {
          this.score++
          stor.lesdiv.correction.style.color = this.styles.cbien
          stor.lesdiv.correction.innerHTML = cBien

          metvert()
          disaTout()

          this.typederreurs[0]++
          this.cacheBoutonValider()
          this.etat = 'navigation'
          this.sectionCourante()
        } else {
          // Pas de bonne réponse
          stor.lesdiv.correction.style.color = this.styles.cfaux

          // A cause de la limite de temps :
          if (this.isElapsed) { // limite de temps
            this._stopTimer()

            affcofo(true)

            stor.lesdiv.correction.innerHTML = tempsDepasse
            this.typederreurs[10]++
            this.cacheBoutonValider()
            /*
                         *   RECOPIER LA CORRECTION ICI !
                         */
            this.etat = 'navigation'
            this.sectionCourante()
          } else {
            stor.lesdiv.correction.innerHTML = cFaux

            if (this.essaiCourant < this.donneesSection.nbchances) {
              stor.lesdiv.correction.innerHTML += '<br>' + essaieEncore
              affcofo(false)

              this.typederreurs[1]++
              // indication éventuelle ici
            } else {
              this._stopTimer()
              this.cacheBoutonValider()

              affcofo(true)

              this.typederreurs[2]++
              this.etat = 'navigation'
              this.sectionCourante()
            }
          }
        }
      }
      // Obligatoire
      this.finCorrection()
      break // case "correction":

    case 'navigation':

      if (this.sectionTerminee()) {
        // On détermine la phrase d’état renvoyée par la section
        this.parcours.pe = (this.donneesSection.nbetapes * this.score) / this.donneesSection.nbitems
        this.afficheBoutonSectionSuivante()
        this.focus('sectioncontinuer')
      } else {
        this.etat = 'enonce'
        this.afficheBoutonSuite()
        this.focus('boutoncontinuer')
      }
      // Obligatoire
      this.finNavigation()

      break // case "navigation":
  }
}
