import { j3pAddContent, j3pAddElt, j3pAjouteBouton, j3pArrondi, j3pEmpty, j3pGetRandomInt, j3pShuffle } from 'src/legacy/core/functions'
import { j3pCreeFenetres, j3pAfficheCroixFenetres, j3pToggleFenetres } from 'src/legacy/core/functionsJqDialog'
import { addDefaultTable } from 'src/legacy/themes/table'
import ListeDeroulante from 'src/legacy/outils/listeDeroulante/ListeDeroulante'
import ZoneStyleMathquill1 from 'src/legacy/outils/zoneStyleMathquill/ZoneStyleMathquill1'
import { j3pAffiche } from 'src/lib/mathquill/functions'
import textesGeneriques from 'src/lib/core/textes'

const { cBien, cFaux, essaieEncore, reponseIncomplete } = textesGeneriques

/**
 * Ajouter une description ici
 * @fileOverview
 * @author Tommy Barroy <t.barroy@laposte.net>
 * @since 2017-10
*/

export const params = {
  outils: ['mathquill', 'calculatrice'],
  /**
   * Ce qui est paramétrable dans l’éditeur de graphe
   * @type {LegacySectionParamsParametres}
   */
  parametres: [
    ['nbrepetitions', 3, 'entier', 'Nombre de répétitions de la section'],
    ['nbchances', 1, 'entier', 'Nombre d’essais possibles'],
    ['limite', 0, 'entier', 'Temps disponible par répétition, en secondes'],
    ['Cas', 'Les deux', 'liste', 'Augmentation, Diminution ou Les deux', ['Augmentation', 'Diminution', 'Les deux']],
    ['Justifie1', true, 'boolean', '<u>true</u>: L’élève doit écrire la formule (1 + a/100) x nombre de départ .'],
    ['Justifie2', true, 'boolean', '<u>true</u>: L’élève doit donner l’écriture décimale de (1 + a/100).'],
    ['Calculatrice', true, 'boolean', '<u>true</u>: L’élève dispose d’une calculatrice.'],
    ['theme', 'standard', 'liste', 'Modèle pour l’affichage', ['standard', 'zonesAvecImageDeFond']]
  ]
}

/**
 * section fraction16
 * @this {Parcours}
 */
export default function main () {
  const me = this
  const stor = me.storage
  const ds = me.donneesSection
  const structure = 'presentation1bis'
  const grandeurs = j3pShuffle([
    {
      unite: '€',
      phrase1: 'Au cours de l’année passée, le prix d’une table a ',
      complete: { '+': 'augmenté de $£b', '-': 'diminué de $£b' },
      phrase2: '$ %,<br> La table coûtait $£a$ € l’année dernière.<br><b> Calcule son prix actuel.</b>',
      prixmoy: [50, 100],
      acalcul: 'Prix actuel =&nbsp;'
    },
    {
      unite: 'm',
      phrase1: 'à 11h, l’ombre de Johan mesurait $£a$ m.<br>En deux heures, l’ombre a ',
      complete: { '+': 'augmenté de $£b', '-': 'diminué de $£b' },
      phrase2: '$ %,<br> <b> Calcule la taille de l’ombre à 13h.</b>',
      prixmoy: [1, 2],
      acalcul: 'Taille de l’ombre à 13h =&nbsp;'
    },
    {
      unite: 'm²',
      phrase1: 'Au mois de juin, une algue recouvrait £a m² d’un lac.<br>Trois mois plus tard, la surface recouverte a ',
      complete: { '+': 'augmenté de $£b', '-': 'diminué de $£b' },
      phrase2: '$ %,<br> <b> Calcule la surface recouverte par l’algue au mois de septembre.</b>',
      prixmoy: [100, 200],
      acalcul: 'Surface recouverte en septembre =&nbsp;'
    },
    {
      unite: 'tonnes',
      phrase1: 'Au cours des quatre dernières heures, <br>la quantité de maïs contenue dans un silo a ',
      complete: { '+': 'augmenté de $£b', '-': 'diminué de $£b' },
      phrase2: '$ %,<br> Il y a 4 heures, le silo contenait $£a$ tonnes de maïs.<br><b> Calcule la masse actuelle de maïs contenue dans le silo.</b>',
      prixmoy: [10, 20],
      acalcul: 'Masse actuelle de maïs =&nbsp;'
    },
    {
      unite: 'cubes',
      phrase1: 'Dans un jeu vidéo, Laziza a utilisé $£a$ cubes pour un batiment. <br> Daniel a utilisé ',
      complete: { '+': '$£b$ % de cubes en plus que Laziza.', '-': '$£b$ % de cubes en moins que Laziza.' },
      phrase2: '<br><b> Calcule le nombre de cubes utilisés par Daniel.</b>',
      prixmoy: [1000, 2000],
      acalcul: 'Nombre de cubes de Daniel =&nbsp;'
    },
    {
      unite: 'heure(s)',
      phrase1: 'En 2022, Jasmin passait $£a$ heures devant les écrans par jour. <br> en 2023, cette durée ',
      complete: { '+': 'augmenté de $£b', '-': 'diminué de $£b' },
      phrase2: '$ %.<br><b> Calcule la durée que passe Jasmin devant les écrans actuellement.</b>',
      prixmoy: [2, 3],
      acalcul: 'Durée actuelle =&nbsp;'
    }
  ])

  function initSection () {
    me.validOnEnter = false // ex donneesSection.touche_entree

    // Construction de la page
    me.construitStructurePage(structure)

    stor.etapes = []
    if (ds.Justifie1) stor.etapes.push('1')
    if (ds.Justifie2) stor.etapes.push('2')
    stor.etapes.push('3')
    ds.nbetapes = stor.etapes.length
    ds.nbitems = ds.nbrepetitions * ds.nbetapes

    stor.etapeEncours = stor.etapes[stor.etapes.length - 1]

    stor.lesExos = []
    let lp = []
    if (ds.Cas === 'Les deux' || ds.Cas === 'Augmentation') lp.push('+')
    if (ds.Cas === 'Les deux' || ds.Cas === 'Diminution') lp.push('-')
    lp = j3pShuffle(lp)
    for (let i = 0; i < me.donneesSection.nbitems; i++) {
      stor.lesExos.push({ cas: lp[i % lp.length] })
    }
    stor.lesExos = j3pShuffle(stor.lesExos)

    for (let i = 0; i < me.donneesSection.nbitems; i++) {
      stor.lesExos[i].sit = grandeurs[i % grandeurs.length]
    }
    stor.lesExos = j3pShuffle(stor.lesExos)
    me.typederreurs = [0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0]

    me.score = 0
    const tt = (ds.Cas === 'Les deux') ? 'Augmentation et diminution en pourcentage' : (ds.Cas + ' en pourcentage')
    me.afficheTitre(tt)

    enonceMain()
  }

  function faisChoixExos () {
    stor.lexo = stor.lesExos.pop()
    const pas = j3pArrondi((stor.lexo.sit.prixmoy[1] - stor.lexo.sit.prixmoy[0]) / 20, 2)
    stor.nbDep = j3pArrondi(stor.lexo.sit.prixmoy[0] + j3pGetRandomInt(0, 20) * pas, 2)
    stor.varia = (stor.lexo.cas === '+') ? j3pGetRandomInt(1, 120) : j3pGetRandomInt(1, 99)
    stor.multiplik = (stor.lexo.cas === '+') ? j3pArrondi(1 + stor.varia / 100, 2) : j3pArrondi(1 - stor.varia / 100, 2)
    stor.repAt = j3pArrondi(stor.multiplik * stor.nbDep, 4)
  }

  function poseQuestion0 () {
    j3pAffiche(stor.lesdiv.enonce, null, stor.lexo.sit.phrase1 + stor.lexo.sit.complete[stor.lexo.cas] + stor.lexo.sit.phrase2 + '<br><br>', { a: stor.nbDep, b: stor.varia })
    stor.foco1 = false
    stor.foco2 = false
  }
  function poseQuestion () {
    j3pEmpty(stor.lesdiv.solution)
    stor.lesdiv.solution.classList.remove('correction')
    if (stor.foco1) {
      stor.foco1 = false
      j3pEmpty(stor.douko)
      j3pAffiche(stor.douko, null, stor.formAt)
      stor.douko.style.color = me.styles.colorCorrection
    }
    if (stor.foco2) {
      stor.foco2 = false
      j3pEmpty(stor.douko)
      j3pAffiche(stor.douko, null, '$' + stor.multiplik + '$')
      stor.douko.style.color = me.styles.colorCorrection
    }
    switch (stor.etapeEncours) {
      case '1': {
        const ttab = addDefaultTable(stor.lesdiv.travail, 1, 3)
        j3pAddContent(ttab[0][0], stor.lexo.sit.acalcul)
        let mm = j3pShuffle([
          '$( 100 + ' + stor.varia + ')$',
          '$\\left( 100 + \\frac{' + stor.varia + '}{100}\\right)$',
          '$\\left( 1 + \\frac{' + stor.varia + '}{1}\\right)$',
          '$( 100 - ' + stor.varia + ')$',
          '$\\left( 100 - \\frac{' + stor.varia + '}{100}\\right)$',
          '$\\left( 1 - \\frac{' + stor.varia + '}{1}\\right)$'
        ])
        mm.splice(0, 2)
        stor.formAt = '$\\left( 1 ' + stor.lexo.cas + '\\frac{' + stor.varia + '}{100}\\right)$'
        mm.push(stor.formAt)
        mm = j3pShuffle(mm)
        mm.splice(0, 0, 'Choisir')
        stor.liste = ListeDeroulante.create(ttab[0][1], mm)
        j3pAffiche(ttab[0][2], null, '&nbsp;$\\times ' + stor.nbDep + '$ ' + stor.lexo.sit.unite)
        stor.douko = ttab[0][1]
      }
        break
      case '2': {
        const ttab = addDefaultTable(stor.lesdiv.travail, 1, 3)
        j3pAddContent(ttab[0][0], stor.lexo.sit.acalcul)
        stor.zone1 = new ZoneStyleMathquill1(ttab[0][1], { restric: '0123456789,.', limite: 7, enter: () => { me.sectionCourante() } })
        j3pAffiche(ttab[0][2], null, '&nbsp;$\\times ' + stor.nbDep + '$ ' + stor.lexo.sit.unite)
        stor.douko = ttab[0][1]
      }
        break
      case '3': {
        const ttab = addDefaultTable(stor.lesdiv.travail, 1, 3)
        j3pAddContent(ttab[0][0], stor.lexo.sit.acalcul)
        stor.zone2 = new ZoneStyleMathquill1(ttab[0][1], { restric: '0123456789,.', limite: 10, enter: () => { me.sectionCourante() } })
        j3pAffiche(ttab[0][2], null, '&nbsp;' + stor.lexo.sit.unite)
      }
        break
    }
  }

  function creeLesDiv () {
    stor.lesdiv = {}
    stor.lesdiv.conteneur = j3pAddElt(me.zonesElts.MG, 'div', {
      style: me.styles.etendre('toutpetit.enonce', { padding: '10px' })
    })
    stor.lesdiv.enonce = addDefaultTable(stor.lesdiv.conteneur, 1, 1)[0][0]
    stor.lesdiv.travail = addDefaultTable(stor.lesdiv.conteneur, 1, 1)[0][0]
    stor.lesdiv.calculatrice = j3pAddElt(me.zonesElts.MD, 'div', {
      style: me.styles.etendre('toutpetit.enonce', { padding: '10px' })
    })
    stor.lesdiv.correction = j3pAddElt(me.zonesElts.MD, 'div', {
      style: me.styles.etendre('toutpetit.enonce', { padding: '10px' })
    })
    stor.lesdiv.solution = j3pAddElt(stor.lesdiv.conteneur, 'div')
    me.zonesElts.MG.classList.add('fond')
    stor.lesdiv.travail.classList.add('travail')
    stor.lesdiv.enonce.classList.add('enonce')
    stor.lesdiv.solution.style.color = me.styles.colorCorrection
  }

  function boutonfen () {
    j3pToggleFenetres('Calculatrice')
  }

  function yaReponse () {
    if (me.isElapsed) return true
    switch (stor.etapeEncours) {
      case '1':
        if (!stor.liste.changed) {
          stor.liste.focus()
          return false
        }
        break
      case '2':
        if (stor.zone1.reponse() === '') {
          stor.zone1.focus()
          return false
        }
        break
      case '3':
        if (stor.zone2.reponse() === '') {
          stor.zone2.focus()
          return false
        }
        break
    }
    return true
  }
  function disaAll () {
    switch (stor.etapeEncours) {
      case '1':
        stor.liste.disable()
        break
      case '2':
        stor.zone1.disable()
        break
      case '3':
        stor.zone2.disable()
        break
    }
    return true
  }
  function metToutVert () {
    switch (stor.etapeEncours) {
      case '1':
        stor.liste.corrige(true)
        break
      case '2':
        stor.zone1.corrige(true)
        break
      case '3':
        stor.zone2.corrige(true)
        break
    }
    return true
  }
  function metFaux () {
    switch (stor.etapeEncours) {
      case '1':
        stor.liste.corrige(false)
        break
      case '2':
        stor.zone1.corrige(false)
        break
      case '3':
        stor.zone2.corrige(false)
        break
    }
    return true
  }
  function barrelesfaux () {
    switch (stor.etapeEncours) {
      case '1':
        stor.liste.barre()
        break
      case '2':
        stor.zone1.barre()
        break
      case '3':
        stor.zone2.barre()
        break
    }
    return true
  }

  function isRepOk () {
    switch (stor.etapeEncours) {
      case '1':
        return stor.liste.reponse === stor.formAt
      case '2':
        return Number(stor.zone1.reponse().replace(/,/g, '.')) === stor.multiplik
      case '3':
        return Number(stor.zone2.reponse().replace(/,/g, '.')) === stor.repAt
    }
  }

  function suivEtape () {
    let i = stor.etapes.indexOf(stor.etapeEncours) + 1
    if (i > stor.etapes.length - 1) i = 0
    return stor.etapes[i]
  }

  function enonceMain () {
    stor.etapeEncours = suivEtape()
    if (stor.etapeEncours === stor.etapes[0]) {
      me.videLesZones()
      creeLesDiv()
      faisChoixExos()
      if (ds.Calculatrice) {
        me.fenetresjq = [{ name: 'Calculatrice', title: 'Une calculatrice', left: 600, top: 10 }]
        j3pCreeFenetres(me)
        j3pAfficheCroixFenetres('Calculatrice')
        j3pAjouteBouton(stor.lesdiv.calculatrice, 'Calculatrice', 'MepBoutons', 'Calculatrice', boutonfen)
      }
      poseQuestion0()
    }
    poseQuestion()
    me.finEnonce()
  }
  function affCofo (isFin) {
    metFaux()
    if (isFin) {
      barrelesfaux()
      disaAll()
      switch (stor.etapeEncours) {
        case '1':
          j3pAffiche(stor.lesdiv.solution, null, stor.lexo.sit.acalcul + stor.formAt + '&nbsp;$\\times ' + stor.nbDep + '$ ' + stor.lexo.sit.unite)
          stor.foco1 = true
          break
        case '2':
          j3pAffiche(stor.lesdiv.solution, null, stor.lexo.sit.acalcul + '$' + stor.multiplik + '$' + '&nbsp;$\\times ' + stor.nbDep + '$ ' + stor.lexo.sit.unite)
          stor.foco2 = true
          break
        case '3':
          j3pAffiche(stor.lesdiv.solution, null, stor.lexo.sit.acalcul + '&nbsp;$' + stor.repAt + '$ ' + stor.lexo.sit.unite)
          break
      }
      stor.lesdiv.solution.classList.add('correction')
    }
  }

  switch (me.etat) {
    case 'enonce':
      // code exécuté au lancement de la section
      if (me.debutDeLaSection) {
        initSection()
      } else {
        enonceMain()
      }
      break // case "enonce":

    case 'correction':
      // On teste si une réponse a été saisie
      if (!yaReponse()) {
        // pas de réponse
        stor.lesdiv.correction.style.color = me.styles.cfaux
        stor.lesdiv.correction.innerHTML = reponseIncomplete
        return me.finCorrection()
      }

      if (isRepOk()) {
        // Bonne réponse
        stor.lesdiv.correction.style.color = me.styles.cbien
        stor.lesdiv.correction.innerHTML = cBien

        metToutVert()
        disaAll()
        me.score++

        me.typederreurs[0]++
        return me.finCorrection('navigation', true)
      }

      // Pas de bonne réponse
      stor.lesdiv.correction.style.color = me.styles.cfaux
      stor.lesdiv.correction.innerHTML = cFaux
      if (me.isElapsed) {
        // A cause de la limite de temps :
        affCofo(true)
        return me.finCorrection('navigation', true)
      }

      // Réponse fausse :
      if (me.essaiCourant < ds.nbchances) {
        stor.lesdiv.correction.innerHTML += '<br>' + essaieEncore
        affCofo(false)
        me.typederreurs[1]++
        // on reste en correction
        return me.finCorrection()
      }

      // Erreur au dernier essai
      affCofo(true)
      me.typederreurs[2]++
      me.finCorrection('navigation', true)
      break // case "correction":

    case 'navigation':
      if (!me.sectionTerminee()) me.etat = 'enonce'
      me.finNavigation()
      break // case "navigation":
  }
}
