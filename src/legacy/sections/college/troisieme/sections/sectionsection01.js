import { j3pAddContent, j3pAddElt, j3pAjouteBouton, j3pArrondi, j3pEmpty, j3pGetNewId, j3pGetRandomInt, j3pShowError, j3pShuffle } from 'src/legacy/core/functions'
import { verifNombreBienEcrit } from 'src/legacy/outils/fonctions/ecritureNombre'
import { j3pAffiche } from 'src/lib/mathquill/functions'
import { addDefaultTable } from 'src/legacy/themes/table'
import { j3pAfficheCroixFenetres, j3pCreeFenetres, j3pToggleFenetres } from 'src/legacy/core/functionsJqDialog'
import { j3pCreeSVG } from 'src/legacy/core/functionsSvg'
import { getMtgCore } from 'src/lib/outils/mathgraph'
import { addMaj } from 'src/lib/utils/string'
import ListeDeroulante from 'src/legacy/outils/listeDeroulante/ListeDeroulante'
import BrouillonCalculs from 'src/legacy/outils/brouillon/BrouillonCalculs'
import ZoneStyleMathquill1 from 'src/legacy/outils/zoneStyleMathquill/ZoneStyleMathquill1'
import { ecrisBienMathquill } from 'src/legacy/outils/zoneStyleMathquill/functions'
import textesGeneriques from 'src/lib/core/textes'

const { cBien, cFaux, essaieEncore, reponseIncomplete, tempsDepasse } = textesGeneriques

/**
 * Ajouter une description ici
 * @fileOverview
 * @author Tommy Barroy <t.barroy@laposte.net>
 * @since 2017-10
 */
export const params = {
  outils: ['mathquill', 'calculatrice'],
  /**
   * Ce qui est paramétrable dans l’éditeur de graphe
   * @type {LegacySectionParamsParametres}
   */
  parametres: [
    ['nbrepetitions', 9, 'entier', 'Nombre de répétitions de la section'],
    ['nbchances', 2, 'entier', 'Nombre d’essais possibles.'],
    ['limite', 0, 'entier', 'Nombre d’essais possibles.'],
    ['Proprietes', 'Les trois', 'liste', '<u>Trigonométrie</u> ,  <u>Pythagore</u> , et <u>Périmètre</u>.', ['Trigonométrie', 'Pythagore', 'Périmètre', 'Trigonométrie_Pythagore', 'Trigonométrie_Périmètre', 'Pythagore_Périmètre', 'Les trois']],
    ['Erreur_Arrondi', true, 'boolean', '<u>true</u>: Compte faux les erreurs d’arrondi.'],
    ['Aide_Propriete', true, 'boolean', '<u>true</u>: Si l’exercice ne concercne qu’une seule propriété, elle est indiquée.'],
    ['theme', 'standard', 'liste', 'Modèle pour l’affichage', ['standard', 'zonesAvecImageDeFond']]
  ]
}

/**
 * section fonction01
 * @this {Parcours}
 */
export default function main () {
  const me = this
  const stor = me.storage
  const ds = me.donneesSection
  let svgId = stor.svgId

  function boutonfen () {
    j3pToggleFenetres('Calculatrice')
  }
  function poseQuestion0 () {
    stor.donnees = j3pShuffle(stor.donnees)
    const ddon = (stor.donnees.length > 1) ? stor.donnees[0] + ' ; ' + stor.donnees[1] : stor.donnees[0]
    j3pAffiche(stor.lesdiv.consigne, null, 'La sphère ci-dessous est coupée par un plan perpendiculaire au rayon [' + stor.lettres[0] + stor.lettres[1] + '].<br>' + ddon + ' .<br>')
    j3pAffiche(stor.lesdiv.consigne, null, '<b>' + stor.quest + '</b>')

    const txtFigure = 'TWF0aEdyYXBoSmF2YTEuMAAAABM#BR64AAJmcv###wEA#wEAAAAAAAAAAAM7AAACbwAAAQEAAAAAAAAAAQAAAJb#####AAAAAQAKQ0NhbGNDb25zdAD#####AAJwaQAWMy4xNDE1OTI2NTM1ODk3OTMyMzg0Nv####8AAAABAApDQ29uc3RhbnRlQAkh+1RELRj#####AAAAAQAKQ1BvaW50QmFzZQD#####AAAAAAAQAAAAQAgAAAAAAAAAAAAAAAAAAAAACQAAQGCgAAAAAABAf64UeuFHrv####8AAAABAAdDQ2FsY3VsAP####8ABW1pbmkxAAEwAAAAAQAAAAAAAAAAAAAAAwD#####AAVtYXhpMQADMTAwAAAAAUBZAAAAAAAA#####wAAAAEAFENJbXBsZW1lbnRhdGlvblByb3RvAP####8AB0N1cnNldXIAAAAFAAAABQAAAAMAAAACAAAAAwAAAAH#####AAAAAQAUQ0Ryb2l0ZURpcmVjdGlvbkZpeGUAAAAABAEAAAAAEAAAAQAAAAEAAAABAT#wAAAAAAAA#####wAAAAEAD0NQb2ludExpZURyb2l0ZQEAAAAEAQAAAAAQAAAAwAgAAAAAAAA#8AAAAAAAAAAABQAAQF1AAAAAAAAAAAAF#####wAAAAEAC0NIb21vdGhldGllAAAAAAQAAAAB#####wAAAAEACkNPcGVyYXRpb24D#####wAAAAEAD0NSZXN1bHRhdFZhbGV1cgAAAAIAAAAIAQAAAAkAAAACAAAACQAAAAP#####AAAAAQALQ1BvaW50SW1hZ2UAAAAABAEAAAAADQACTzEAwBAAAAAAAABAEAAAAAAAAAAABQAAAAAGAAAABwAAAAcAAAAABAAAAAEAAAAIAwAAAAgBAAAAAT#wAAAAAAAAAAAACQAAAAIAAAAIAQAAAAkAAAADAAAACQAAAAIAAAAKAAAAAAQBAAAAAA0AAkkxAMAAAAAAAAAAQAgAAAAAAAAAAAUAAAAABgAAAAn#####AAAAAQAIQ1NlZ21lbnQBAAAABAEAAAAAEAAAAQAAAAIAAAABAAAABgAAAAYBAAAABAAAAAABEAACazEAwAAAAAAAAABAAAAAAAAAAAAAAQABAAAAAAAAAAAAAAAL#####wAAAAIAD0NNZXN1cmVBYnNjaXNzZQEAAAAEAAFhAAAACAAAAAoAAAAM#####wAAAAEAD0NWYWxldXJBZmZpY2hlZQEAAAAEAQAAAAAAAAAAAAAAAMAYAAAAAAAAAAAAAAAMDwAB####AAAAAQAAAAIAAAABAAAAAAAAAAAAAAAAAgAAAA0AAAADAP####8AAUEAAy01MP####8AAAABAAxDTW9pbnNVbmFpcmUAAAABQEkAAAAAAAAAAAACAP####8BAAAAAA4AAVUAwCQAAAAAAABAEAAAAAAAAAAABQAAQC8xqfvnbItALzGp++dsiwAAAAUA#####wEAAAAAEAAAAQAAAAEAAAAQAT#wAAAAAAAAAAAABgD#####AQAAAAAOAAFWAMAAAAAAAAAAQBAAAAAAAAAAAAUAAUA#Man752yLAAAAEQAAAAsA#####wEAAAAAEAAAAQAAAAEAAAAQAAAAEv####8AAAABAAdDTWlsaWV1AP####8BAAAAABAAAAAAAAAAAAAAAEAIAAAAAAAAAAAFAAAAABAAAAAS#####wAAAAIADENDb21tZW50YWlyZQD#####AQAAAAAAAAAAAAAAAEAYAAAAAAAAAAAAAAAUDAAAAAAAAQAAAAAAAAABAAAAAAAAAAAAATH#####AAAAAQAJQ0xvbmd1ZXVyAP####8AAAAQAAAAEgAAAAIA#####wAAAAAAEAAAAAAAAAAAAAAAQAgAAAAAAAAAAAkAAEBlQAAAAAAAQGT8KPXCj1wAAAACAP####8BAAAAABAAAAAAAAAAAAAAAEAIAAAAAAAAAARhY2EyCQABQHJAAAAAAABAaBwo9cKPXP####8AAAABAAlDQ2VyY2xlT0EA#####wAAAAAAAAABAAAAFwAAABj#####AAAAAQARQ1N5bWV0cmllQ2VudHJhbGUA#####wAAABcAAAAKAP####8BAAAAABAAAAAAAAAAAAAAAEAIAAAAAAAAAAAFAAAAABgAAAAaAAAACwD#####AQAAAAAQAAABAAAAAQAAABgAAAAb#####wAAAAEADENBcmNEZUNlcmNsZQD#####AQAAAAAAAAEAAAAXAAAAGAAAABv#####AAAAAQAPQ1BvaW50TGllQ2VyY2xlAP####8BAAAAABAAAAAAAAAAAAAAAEAIAAAAAAAAAAAFAAE#y4ciqRgIiAAAAB3#####AAAAAQAWQ0Ryb2l0ZVBlcnBlbmRpY3VsYWlyZQD#####AQAAAAAQAAABAAAAAQAAAB4AAAAc#####wAAAAEAEENJbnREcm9pdGVEcm9pdGUA#####wEAAAAAEAAAAAAAAAAAAAAAQAgAAAAAAAAAAAUAAAAAHwAAABwAAAAPAP####8BAAAAABAAAAAAAAAAAAAAAEAIAAAAAAAAAAAFAAAAAB4AAAAgAAAAEwD#####AAAAIAAAAAoA#####wEAAAAAEAAAAAAAAAAAAAAAQAgAAAAAAAAAAAUAAAAAIQAAACIAAAAWAP####8BAAAAABAAAAEAAAABAAAAHgAAAB8AAAAWAP####8BAAAAABAAAAEAAAABAAAAFwAAACQAAAAXAP####8AAAAAABAAAAAAAAAAAAAAAEAIAAAAAAAAAAAJAAAAACQAAAAl#####wAAAAEAEENJbnREcm9pdGVDZXJjbGUA#####wAAACQAAAAZ#####wAAAAEAEENQb2ludExpZUJpcG9pbnQA#####wEAAAAAEAAAAAAAAAAAAAAAQAgAAAAAAAAAAAkAAQAAACcAAAAZAP####8BAAAAABAAAAAAAAAAAAAAAEAIAAAAAAAAAAAFAAIAAAAnAAAAFAD#####AQAAAAAAAAEAAAAmAAAAKAAAACkAAAAVAP####8BAAAAABAAAAAAAAAAAAAAAEAIAAAAAAAAAAAFAAE#5IPkxIwdXgAAACoAAAAWAP####8BAAAAABAAAAEAAAABAAAAKwAAACQAAAAXAP####8BAAAAABAAAAAAAAAAAAAAAEAIAAAAAAAAAAAFAAAAACQAAAAsAAAADwD#####AQAAAAAQAAAAAAAAAAAAAABACAAAAAAAAAAABQAAAAArAAAALQAAAA8A#####wEAAAAAEAAAAAAAAAAAAAAAQAgAAAAAAAAAAAUAAAAALgAAAC0AAAATAP####8AAAAtAAAACgD#####AQAAAAAQAAAAAAAAAAAAAABACAAAAAAAAAAABQAAAAAvAAAAMAAAAA8A#####wEAAAAAEAAAAAAAAAAAAAAAQAgAAAAAAAAAAAUAAAAAIQAAACAAAAAPAP####8BAAAAABAAAAAAAAAAAAAAAEAIAAAAAAAAAAAFAAAAACMAAAAg#####wAAAAIADUNMaWV1RGVQb2ludHMA#####wAAAAAAAAIBAAAAMgAAAMgAAAAAAB4AAAAFAAAAHgAAAB8AAAAgAAAAIQAAADIAAAAaAP####8AAAAAAAAAAQAAADMAAADIAAAAAAAeAAAABwAAAB4AAAAfAAAAIAAAACEAAAAiAAAAIwAAADMAAAAYAP####8AAAAlAAAAGQAAABkA#####wEAAAAAEAAAAAAAAAAAAAAAQAgAAAAAAAAAAAkAAQAAADYAAAAZAP####8AAAAAABAAAAAAAAAAAAAAAEAIAAAAAAAAAAAJAAIAAAA2AAAACwD#####AAAAAAAQAAABAAABAQAAABcAAAA4AAAACwD#####AQAAAAAQAAABAAABAQAAACYAAAAoAAAACwD#####AQAAAAAQAAABAARhY2ExAQEAAAAXAAAAGP####8AAAABAAtDUG9pbnRDbG9uZQD#####AQAAAAAQAAAAAAAAAAAAAABACAAAAAAAAAAACQAAAAAo#####wAAAAEACUNEcm9pdGVBQgD#####AQAAAAAQAAABAAAAAgAAACYAAAAoAAAABgD#####AQAAAAAQAAAAAAAAAAAAAABACAAAAAAAAAAACQABP#qUFy640eUAAAA9AAAAEwD#####AAAAJgAAAAoA#####wEAAAAAEAAAAAAAAAAAAAAAQAgAAAAAAAAAAAkAAAAAPgAAAD8AAAAHAP####8AAAA+AAAACAMAAAAJAAAADwAAAAFAWQAAAAAAAAAAAAoA#####wH#AAAAEAAAAAAAAAAAAAAAQAgAAAAAAAAAAAgAAAAAQAAAAEEAAAACAP####8B#wAAABAAAAAAAAAAAAAAAEAIAAAAAAAAAAAIAAFAd7AAAAAAAEByHhR64UeuAAAAAgD#####Af8AAAAQAAAAAAAAAAAAAABACAAAAAAAAAAACAABQH0gAAAAAABAbnwo9cKPXAAAAAsA#####wH#AAAAEAAAAQAAAAIAAABEAAAAQ#####8AAAABABBDRHJvaXRlUGFyYWxsZWxlAP####8B#wAAABAAAAEAAAACAAAAQgAAAEUAAAACAP####8B#wAAABAAAAAAAAAAAAAAAEAIAAAAAAAAAAAIAAFAgKAAAAAAAEBpvCj1wo9cAAAAAgD#####Af8AAAAQAAAAAAAAAAAAAABACAAAAAAAAAAACAABQIG4AAAAAABAW3hR64UeuAAAAB0A#####wH#AAAAEAAAAQAAAAIAAABHAAAAPQAAAB0A#####wH#AAAAEAAAAQAAAAIAAABIAAAASQAAABcA#####wH#AAAAEAAAAAAAAAAAAAAAQAgAAAAAAAAAAAgAAAAARgAAAEoAAAAXAP####8B#wAAABAAAAAAAAAAAAAAAEAIAAAAAAAAAAAIAAAAAEYAAABJ#####wAAAAEACENWZWN0ZXVyAP####8B#wAAABAAAAEAAAACAAAAQAAAAD4A#####wAAAAEADUNUcmFuc1BhclZlY3QA#####wAAAE0AAAAKAP####8B#wAAABAAAAAAAAAAAAAAAEAIAAAAAAAAAAAIAAAAAEsAAABOAAAACgD#####Af8AAAAQAAAAAAAAAAAAAABACAAAAAAAAAAACAAAAABMAAAATv####8AAAABAAlDUG9seWdvbmUA#####wAAAAAAAAACAAAABQAAAEsAAABPAAAAUAAAAEwAAABL#####wAAAAEAEENTdXJmYWNlUG9seWdvbmUA#####wB#f38AAAAAAAUAAABR#####wAAAAEAI0NBdXRyZVBvaW50SW50ZXJzZWN0aW9uRHJvaXRlQ2VyY2xlAP####8B####ABAAAAAAAAAAAAAAAEAIAAAAAAAAAAAIAAAAABkAAAA9AAAAKAAAABQA#####wAAAAAAAAABAAAAFwAAACgAAABTAAAAFQD#####Af8AAAAQAAAAAAAAAAAAAABACAAAAAAAAAAACAABP8pkZvH5JaMAAABU#####wAAAAEAD0NTeW1ldHJpZUF4aWFsZQD#####AAAAOQAAAAoA#####wH#AAAAEAAAAAAAAAAAAAAAQAgAAAAAAAAAAAgAAAAAVQAAAFYAAAAaAP####8AAAAAAAAAAQAAAFcAAADIAAEAAABVAAAAAgAAAFUAAABXAAAAGgD#####AP8AAAAAAgMAAAAvAAAAyAABAAAAKwAAAAUAAAArAAAALAAAAC0AAAAuAAAALwAAABoA#####wD#AAAAAAADAAAAMQAAAMgAAQAAACsAAAAHAAAAKwAAACwAAAAtAAAALgAAAC8AAAAwAAAAMf####8AAAABABFDU3VyZmFjZURldXhMaWV1eAD#####AP###wAAAAAABQAAAFgAAABaAAAACwD#####Af###wAQAAABAAAAAgAAAFAAAABPAAAAFwD#####Af###wAQAAAAAAAAAAAAAABACAAAAAAAAAAACAAAAAAkAAAAXAAAAAsA#####wH###8AEAAAAQAAAAIAAABCAAAAXQAAABYA#####wEAfwAAEAAAAQAAAAIAAAArAAAAXgAAABcA#####wEAfwAAEAAAAAAAAAAAAAAAQAgAAAAAAAAAAAgAAAAAXwAAAF4AAAAPAP####8BAH8AABAAAAAAAAAAAAAAAEAIAAAAAAAAAAAIAAAAAGAAAAArAAAADwD#####AQB#AAAQAAAAAAAAAAAAAABACAAAAAAAAAAACAAAAABhAAAAYAAAABMA#####wAAAGAAAAAKAP####8B#wAAABAAAAAAAAAAAAAAAEAIAAAAAAAAAAAIAAAAAGIAAABj#####wAAAAEADkNPYmpldER1cGxpcXVlAP####8AAAAAAAAAAAA5AAAAJQD#####AQAAAAAAAAAAOgAAACUA#####wEAAAAAA3BwMQAAACgAAAAQAP####8AAAAAAQADbnBB#####xBAYiAAAAAAAEBkHCj1wo9cAAAAAAAAAAAAAAAAAAEAAAAAAAAAAAABQQAAABAA#####wEAAAABAANucEP#####EEBowAAAAAAAQC#Cj1wo9cAAAAAAAAAAAAAAAAAAAQAAAAAAAAAAAAFDAAAAEAD#####AAAAAAEAA25wRP####8QQHJQAAAAAABAWXhR64UeuAAAAAAAAAAAAAAAAAABAAAAAAAAAAAAAUQAAAAQAP####8BAAAAAQADbnBF#####xBAcwAAAAAAAEBnvCj1wo9cAAAAAAAAAAAAAAAAAAEAAAAAAAAAAAABRQAAAAsA#####wH#AAAAEAAAAQAAAAIAAABLAAAATAAAABcA#####wH#AAAAEAAAAAAAAAAAAAAAQAgAAAAAAAAAAAgAAAAAOgAAAGwAAAALAP####8BAAAAABAAAAEAAAEBAAAAbQAAACgAAAAKAP####8BAAAAABAAAAAAAAAAAAAAAEAIAAAAAAAAAAAIAAAAACgAAAA######wAAAAEADUNEZW1pRHJvaXRlT0EA#####wEAAAAAEAAAAQAAAQEAAAAmAAAAbwAAABcA#####wEAAAAAEAAAAAAAAAAAAAAAQAgAAAAAAAAAAAgAAAAAbAAAAHAAAAAWAP####8BAAAAABAAAAEAAAEBAAAAcQAAADkAAAAXAP####8BAAAAABAAAAAAAAAAAAAAAEAIAAAAAAAAAAAIAAAAADkAAAByAAAACwD#####AQAAAAAQAAABAAABAQAAAHMAAAAoAAAAFgD#####AQAAAAAQAAABAAABAQAAAEIAAAAkAAAAGAD#####AAAAdQAAACoAAAAZAP####8AAAAAABAAAAAAAAAAAAAAAEAIAAAAAAAAAAAIAAEAAAB2AAAAGQD#####AQAAAAAQAAAAAAAAAAAAAABACAAAAAAAAAAACAACAAAAdgAAAA8A#####wEAAAAAEAAAAAAAAAAAAAAAQAgAAAAAAAAAAAgAAAAAeAAAAEIAAAAPAP####8BAAAAABAAAAAAAAAAAAAAAEAIAAAAAAAAAAAIAAAAAEIAAAB5AAAAEwD#####AAAAQgAAAAoA#####wEAAAAAEAAAAAAAAAAAAAAAQAgAAAAAAAAAAAgAAAAAegAAAHsAAAALAP####8B#wAAABAAAAEAAAABAAAAfAAAAHoAAAALAP####8BAAAAABAAAAEABGFtb24BAQAAABcAAAAoAAAAFQD#####AQAAAAAQAAAAAAAAAAAAAABACAAAAAAAAAAABQABQA4yqmoul6UAAAAZAAAACwD#####AQAAAAAQAAABAAABAQAAABcAAAB#AAAAEAD#####AX8AfwEAAmwx#####xBAXcAAAAAAAEBqfCj1wo9cAAAAAAAAAAAAAAAAAAEAAAAAAAAAAAACbDEAAAAQAP####8BAAAAAQACcEH#####EEBmoAAAAAAAQGUcKPXCj1wAAAAAAAAAAAAAAAAAAQAAAAAAAAAAAAFB#####wAAAAIACUNDZXJjbGVPUgD#####AAAAAAAAAAEAAABLAAAAAUBEAAAAAAAAAAAAABgA#####wAAAEoAAACDAAAAGQD#####AQAAAAAQAAAAAAAAAAAAAABACAAAAAAAAAAABQABAAAAhAAAABkA#####wEAAAAAEAAAAAAAAAAAAAAAQAgAAAAAAAAAAAUAAgAAAIQAAAAeAP####8BAAAAABAAAAEAAAABAAAASwAAAEwAAAAAHwD#####AAAAhwAAAAoA#####wEAAAAAEAAAAAAAAAAAAAAAQAgAAAAAAAAAAAUAAAAAhgAAAIgAAAAgAP####8B####AAAAAQAAAAUAAACGAAAASwAAAEwAAACJAAAAhgAAAAsA#####wAAAAAAEAAAAQAAAQEAAAAmAAAAPAAAACEA#####wD###8AAAAAAAQAAACKAAAAEAD#####AAAAAAEAAnBD#####xBAZ+AAAAAAAEArwo9cKPXAAAAAAAAAAAAAAAAAAAEAAAAAAAAAAAABQwAAABAA#####wEAAAABAAJwQv####8QQGRAAAAAAABAU7hR64UeuAAAAAAAAAAAAAAAAAABAAAAAAAAAAAAAUIAAAALAP####8B#wAAABAAAAEAAAADAAAAOAAAACYAAAAlAP####8AAAAAAAAAAAAZAAAAFwD#####AQAAAAAQAAAAAAAAAAAAAABACAAAAAAAAAAABQAAAAAlAAAASQAAAAsA#####wAAAAAAEAAAAQAAAQEAAACRAAAAOAAAACUA#####wAAAAAAA3BwMgAAACYAAAAlAP####8AAAAAAAAAAAA4AAAAJQD#####AQAAAAAAAAAAHgAAABb##########w=='
    const A = stor.lettres[0]
    const C = stor.lettres[1]
    const B = stor.lettres[2]
    const D = stor.lettres[3]
    const E = stor.lettres[4]
    svgId = j3pGetNewId()
    stor.svgId = svgId
    const tabL1 = addDefaultTable(stor.lesdiv.figure, 2, 1)
    j3pCreeSVG(tabL1[0][0], { id: svgId, width: 310, height: 320, style: { border: 'solid black 1px' } })
    stor.mtgAppLecteur.removeAllDoc()
    stor.mtgAppLecteur.addDoc(svgId, txtFigure, true)
    stor.mtgAppLecteur.giveFormula2(svgId, 'a', -50)
    stor.mtgAppLecteur.setText(svgId, '#npA', A)
    stor.mtgAppLecteur.setText(svgId, '#pB', B)
    stor.mtgAppLecteur.setText(svgId, '#pC', C)
    stor.mtgAppLecteur.setText(svgId, '#npD', D)
    stor.mtgAppLecteur.setText(svgId, '#npE', E)
    stor.mtgAppLecteur.calculateAndDisplayAll(true)
    stor.mtgAppLecteur.setVisible(svgId, '#npD', false)
    stor.mtgAppLecteur.setVisible(svgId, '#pB', false)
    stor.mtgAppLecteur.setVisible(svgId, '#pp1', false)
    stor.mtgAppLecteur.setVisible(svgId, '#pp2', false)
    for (let i = 0; i < stor.acache.length; i++) {
      stor.mtgAppLecteur.setVisible(svgId, stor.acache[i], false)
    }
    j3pAjouteBouton(stor.lesdiv.explications, passer, { value: 'Passer l’animation' })
    stor.lea = -40
    stor.annim = 1
    stor.raz = false
    setTimeout(lanceAnim, 50)
    stor.lesdiv.figure.classList.add('enonce')
  }
  function passer () {
    stor.lea = 100
    stor.raz = true
    stor.mtgAppLecteur.setText(svgId, '#npD', stor.lettres[3])
    stor.mtgAppLecteur.setVisible(svgId, '#npD', true)
    stor.mtgAppLecteur.setVisible(svgId, '#pp1', true)
    stor.mtgAppLecteur.setText(svgId, '#pB', stor.lettres[2])
    stor.mtgAppLecteur.setVisible(svgId, '#pB', true)
    stor.mtgAppLecteur.setVisible(svgId, '#pp2', true)
    stor.mtgAppLecteur.giveFormula2(svgId, 'A', stor.lea)
    stor.mtgAppLecteur.updateFigure(svgId)
    for (let i = 0; i < stor.amontre.length; i++) {
      stor.mtgAppLecteur.setVisible(svgId, stor.amontre[i], true)
    }
    j3pEmpty(stor.lesdiv.explications)
  }
  function lanceAnim () {
    if (stor.raz) return
    stor.lea++
    if (stor.lea === 100) {
      stor.annim++
      stor.lea = -40
      if (stor.annim === 3) {
        j3pEmpty(stor.lesdiv.explications)
        setTimeout(() => {
          for (let i = 0; i < stor.amontre.length; i++) {
            stor.mtgAppLecteur.setVisible(svgId, stor.amontre[i], true)
          }
        }, 1000)
        return
      }
      setTimeout(() => {
        stor.mtgAppLecteur.setText(svgId, '#npD', ' ')
        stor.mtgAppLecteur.setText(svgId, '#pB', ' ')
        stor.mtgAppLecteur.setVisible(svgId, '#npD', false)
        stor.mtgAppLecteur.setVisible(svgId, '#pB', false)
        stor.mtgAppLecteur.setVisible(svgId, '#pp1', false)
        stor.mtgAppLecteur.setVisible(svgId, '#pp2', false)
        lanceAnim()
      }, 2000)
      return
    }
    if (stor.lea === 20) {
      stor.mtgAppLecteur.setText(svgId, '#npD', stor.lettres[3])
      stor.mtgAppLecteur.setVisible(svgId, '#npD', true)
      stor.mtgAppLecteur.setVisible(svgId, '#pp1', true)
    }
    if (stor.lea === 51) {
      stor.mtgAppLecteur.setText(svgId, '#pB', stor.lettres[2])
      stor.mtgAppLecteur.setVisible(svgId, '#pB', true)
      stor.mtgAppLecteur.setVisible(svgId, '#pp2', true)
    }
    stor.mtgAppLecteur.giveFormula2(svgId, 'A', stor.lea)
    stor.mtgAppLecteur.updateFigure(svgId)
    setTimeout(lanceAnim, 50)
  }
  function faisChoixExo () {
    const e = ds.lesExos.pop()
    stor.donnees = []
    stor.lettres = []
    for (let i = 0; i < 6; i++) {
      addMaj(stor.lettres)
    }
    stor.acache = []
    stor.amontre = []
    stor.unitQuest = 'cm'
    switch (e[0]) {
      case 'pyth':
        stor.at1 = 'le théorème de Pythagore.'
        stor.acache = ['#aca1', '#aca2', '#npE']
        stor.amontre = ['#amon']
        switch (e[1]) {
          case 1:
            stor.petitRayon = j3pGetRandomInt(2, 98)
            stor.donnees.push('$' + stor.lettres[2] + stor.lettres[3] + ' = ' + stor.petitRayon + '$ cm')
            stor.distanceCentre = j3pGetRandomInt(2, 98)
            stor.donnees.push('$' + stor.lettres[0] + stor.lettres[2] + ' = ' + stor.distanceCentre + '$ cm')
            stor.quest = 'On veut calculer le rayon $' + stor.lettres[0] + stor.lettres[3] + '$ de la sphère.'
            stor.valQuest = '$' + stor.lettres[0] + stor.lettres[3] + '$'
            stor.repAtLong = Math.sqrt(stor.petitRayon * stor.petitRayon + stor.distanceCentre * stor.distanceCentre)
            stor.valCarre = stor.petitRayon * stor.petitRayon + stor.distanceCentre * stor.distanceCentre
            break
          case 2:
            stor.grandRayon = j3pGetRandomInt(10, 98)
            stor.donnees.push('$' + stor.lettres[0] + stor.lettres[3] + ' = ' + stor.grandRayon + '$ cm')
            stor.distanceCentre = j3pGetRandomInt(2, stor.grandRayon - 1)
            stor.donnees.push('$' + stor.lettres[0] + stor.lettres[2] + ' = ' + stor.distanceCentre + '$ cm')
            stor.quest = 'On veut calculer le rayon $' + stor.lettres[2] + stor.lettres[3] + '$ de la section.'
            stor.valQuest = '$' + stor.lettres[2] + stor.lettres[3] + '$'
            stor.repAtLong = Math.sqrt(stor.grandRayon * stor.grandRayon - stor.distanceCentre * stor.distanceCentre)
            stor.valCarre = stor.grandRayon * stor.grandRayon - stor.distanceCentre * stor.distanceCentre
            break
          case 3:
            stor.grandRayon = j3pGetRandomInt(10, 98)
            stor.donnees.push('$' + stor.lettres[0] + stor.lettres[3] + ' = ' + stor.grandRayon + '$ cm')
            stor.petitRayon = j3pGetRandomInt(2, stor.grandRayon - 1)
            stor.donnees.push('$' + stor.lettres[2] + stor.lettres[3] + ' = ' + stor.petitRayon + '$ cm')
            stor.quest = 'On veut calculer la distance $' + stor.lettres[0] + stor.lettres[2] + '$ du plan de la section au centre de la sphère.'
            stor.valQuest = '$' + stor.lettres[0] + stor.lettres[2] + '$'
            stor.repAtLong = Math.sqrt(stor.grandRayon * stor.grandRayon - stor.petitRayon * stor.petitRayon)
            stor.valCarre = stor.grandRayon * stor.grandRayon - stor.petitRayon * stor.petitRayon
            break
        }
        stor.explik1 = 'Le triangle ' + stor.lettres[2] + stor.lettres[0] + stor.lettres[3] + ' est rectangle en ' + stor.lettres[2] + ',<br> on connait la longueur de deux côtés,<br> on peut donc calculer la longueur ' + stor.valQuest + ' du troisième côté,<br> en utilisant le théorème de Pythagore.'
        break
      case 'trigo':
        stor.at1 = 'une formule de trigonométrie.'
        stor.acache = []
        stor.amontre = ['#amon', 'aangle']
        switch (e[1]) {
          case 1:
            stor.angle1 = j3pGetRandomInt(2, 88)
            stor.donnees.push('$\\widehat{' + stor.lettres[3] + stor.lettres[0] + stor.lettres[2] + '} = ' + stor.angle1 + '$ °')
            stor.distanceCentre = j3pGetRandomInt(2, 98)
            stor.donnees.push('$' + stor.lettres[0] + stor.lettres[2] + ' = ' + stor.distanceCentre + '$ cm')
            stor.quest = 'On veut calculer le rayon $' + stor.lettres[0] + stor.lettres[3] + '$ de la sphère.'
            stor.valQuest = '$' + stor.lettres[0] + stor.lettres[3] + '$'
            stor.repAtLong = stor.distanceCentre / Math.cos(stor.angle1 * Math.PI / 180)
            stor.explik1 = 'Le triangle ' + stor.lettres[2] + stor.lettres[0] + stor.lettres[3] + ' est rectangle en ' + stor.lettres[2] + ',<br> on connait la longueur d’un côté et d’un angle,<br> on peut donc calculer la longueur ' + stor.valQuest + ' d’un deuxième côté,<br> en utilisant la trigonométrie.'
            break
          case 2:
            stor.angle1 = j3pGetRandomInt(2, 88)
            stor.donnees.push('$\\widehat{' + stor.lettres[3] + stor.lettres[0] + stor.lettres[2] + '} = ' + stor.angle1 + '$ °')
            stor.distanceCentre = j3pGetRandomInt(2, 98)
            stor.donnees.push('$' + stor.lettres[0] + stor.lettres[2] + ' = ' + stor.distanceCentre + '$ cm')
            stor.quest = 'On veut calculer le rayon $' + stor.lettres[2] + stor.lettres[3] + '$ de la section.'
            stor.valQuest = '$' + stor.lettres[2] + stor.lettres[3] + '$'
            stor.repAtLong = stor.distanceCentre * Math.tan(stor.angle1 * Math.PI / 180)
            stor.explik1 = 'Le triangle ' + stor.lettres[2] + stor.lettres[0] + stor.lettres[3] + ' est rectangle en ' + stor.lettres[2] + ',<br> on connait la longueur d’un côté et d’un angle,<br> on peut donc calculer la longueur ' + stor.valQuest + ' d’un deuxième côté,<br> en utilisant la trigonométrie.'
            break
          case 3:
            stor.angle1 = j3pGetRandomInt(2, 88)
            stor.donnees.push('$\\widehat{' + stor.lettres[3] + stor.lettres[0] + stor.lettres[2] + '} = ' + stor.angle1 + '$ °')
            stor.grandRayon = j3pGetRandomInt(2, 98)
            stor.donnees.push('$' + stor.lettres[0] + stor.lettres[3] + ' = ' + stor.grandRayon + '$ cm')
            stor.quest = 'On veut calculer le rayon $' + stor.lettres[2] + stor.lettres[3] + '$ de la section.'
            stor.valQuest = '$' + stor.lettres[2] + stor.lettres[3] + '$'
            stor.repAtLong = stor.grandRayon * Math.sin(stor.angle1 * Math.PI / 180)
            stor.explik1 = 'Le triangle ' + stor.lettres[2] + stor.lettres[0] + stor.lettres[3] + ' est rectangle en ' + stor.lettres[2] + ',<br> on connait la longueur d’un côté et d’un angle,<br> on peut donc calculer la longueur ' + stor.valQuest + ' d’un deuxième côté,<br> en utilisant la trigonométrie.'
            break
          case 4:
            stor.angle1 = j3pGetRandomInt(2, 88)
            stor.donnees.push('$\\widehat{' + stor.lettres[3] + stor.lettres[0] + stor.lettres[2] + '} = ' + stor.angle1 + '$ °')
            stor.grandRayon = j3pGetRandomInt(2, 98)
            stor.donnees.push('$' + stor.lettres[0] + stor.lettres[3] + ' = ' + stor.grandRayon + '$ cm')
            stor.quest = 'On veut calculer la distance $' + stor.lettres[0] + stor.lettres[2] + '$ du plan de la section au centre de la sphère.'
            stor.valQuest = '$' + stor.lettres[0] + stor.lettres[2] + '$'
            stor.repAtLong = stor.grandRayon * Math.cos(stor.angle1 * Math.PI / 180)
            stor.explik1 = 'Le triangle ' + stor.lettres[2] + stor.lettres[0] + stor.lettres[3] + ' est rectangle en ' + stor.lettres[2] + ',<br> on connait la longueur d’un côté et d’un angle,<br> on peut donc calculer la longueur ' + stor.valQuest + ' d’un deuxième côté,<br> en utilisant la trigonométrie.'
            break
          case 5:
            stor.unitQuest = '°'
            stor.grandRayon = j3pGetRandomInt(10, 98)
            stor.donnees.push('$' + stor.lettres[0] + stor.lettres[3] + ' = ' + stor.grandRayon + '$ cm')
            stor.petitRayon = j3pGetRandomInt(2, stor.grandRayon - 1)
            stor.donnees.push('$' + stor.lettres[2] + stor.lettres[3] + ' = ' + stor.petitRayon + '$ cm')
            stor.quest = 'On veut calculer l’angle $\\widehat{' + stor.lettres[3] + stor.lettres[0] + stor.lettres[2] + '}$.'
            stor.valQuest = '$\\widehat{' + stor.lettres[3] + stor.lettres[0] + stor.lettres[2] + '}$'
            stor.repAtLong = Math.asin(stor.petitRayon / stor.grandRayon) * 180 / Math.PI
            stor.explik1 = 'Le triangle ' + stor.lettres[2] + stor.lettres[0] + stor.lettres[3] + ' est rectangle en ' + stor.lettres[2] + ',<br> on connait la longueur de deux côtés,<br> on peut donc calculer la mesure de l’angle ' + stor.valQuest + ',<br> en utilisant la trigonométrie.'
            break
          case 6:
            stor.unitQuest = '°'
            stor.grandRayon = j3pGetRandomInt(10, 98)
            stor.donnees.push('$' + stor.lettres[0] + stor.lettres[3] + ' = ' + stor.grandRayon + '$ cm')
            stor.distanceCentre = j3pGetRandomInt(2, stor.grandRayon - 1)
            stor.donnees.push('$' + stor.lettres[0] + stor.lettres[2] + ' = ' + stor.distanceCentre + '$ cm')
            stor.quest = 'On veut calculer l’angle $\\widehat{' + stor.lettres[3] + stor.lettres[0] + stor.lettres[2] + '}$.'
            stor.valQuest = '$\\widehat{' + stor.lettres[3] + stor.lettres[0] + stor.lettres[2] + '}$'
            stor.repAtLong = Math.acos(stor.distanceCentre / stor.grandRayon) * 180 / Math.PI
            stor.explik1 = 'Le triangle ' + stor.lettres[2] + stor.lettres[0] + stor.lettres[3] + ' est rectangle en ' + stor.lettres[2] + ',<br> on connait la longueur de deux côtés,<br> on peut donc calculer la mesure de l’angle ' + stor.valQuest + ',<br> en utilisant la trigonométrie.'
            break
        }
        break
      case 'perim':
        stor.at1 = 'la formule du périmètre d’un cercle.'
        stor.acache = []
        stor.amontre = ['#amon', 'aangle']
        switch (e[1]) {
          case 1:
            stor.petitRayon = j3pGetRandomInt(2, 99)
            stor.donnees.push('$' + stor.lettres[2] + stor.lettres[3] + ' = ' + stor.petitRayon + '$ cm')
            stor.quest = 'On veut calculer le périmètre $P(\\text{section})$ de la section.'
            stor.valQuest = '$P(\\text{section})$'
            stor.repAtLong = 2 * Math.PI * stor.petitRayon
            break
          case 2:
            stor.perimetre = j3pGetRandomInt(2, 99)
            stor.donnees.push('$P(\\text{section}) = ' + stor.perimetre + '$ cm')
            stor.quest = 'On veut calculer le rayon $' + stor.lettres[2] + stor.lettres[3] + '$ de la section.'
            stor.valQuest = '$' + stor.lettres[2] + stor.lettres[3] + '$'
            stor.repAtLong = stor.perimetre / (2 * Math.PI)
            break
        }
        stor.explik1 = 'La formule du périmètre d’un cercle permet de <br>retrouver un rayon ou un périmètre<br>en fonction de l’autre.'
        break
    }
    stor.repAt2 = j3pArrondi(stor.repAtLong, 2)
    stor.ceg = stor.repAt2 === stor.repAtLong
    const accomp = j3pArrondi(Math.ceil(100 * stor.repAtLong) / 100, 2)
    const accomp2 = j3pArrondi(Math.floor(100 * stor.repAtLong) / 100, 2)
    stor.arondFake = (stor.repAt2 === accomp2) ? accomp : accomp2
    return e
  }
  function modif (tab) {
    for (let i = 0; i < tab.length; i++) {
      for (let j = 0; j < tab[i].length; j++) {
        tab[i][j].style.padding = '0px'
      }
    }
  }
  function initSection () {
    // Construction de la page
    me.construitStructurePage('presentation3')
    ds.lesExos = []

    const lp = []
    if (ds.Proprietes === 'Les trois' || ds.Proprietes.indexOf('Pyth') !== -1) lp.push('pyth')
    if (ds.Proprietes === 'Les trois' || ds.Proprietes.indexOf('Trigo') !== -1) lp.push('trigo')
    if (ds.Proprietes === 'Les trois' || ds.Proprietes.indexOf('Périm') !== -1) lp.push('perim')
    ds.nbetapes = (lp.length === 1) ? 1 : 2
    ds.nbitems = ds.nbetapes * ds.nbrepetitions
    ds.aFaire = (lp.length === 1) ? ['cal'] : ['prop', 'cal']

    let lp2 = []
    let lp3 = []
    let lp4 = []
    const lpi = []
    if (lp.indexOf('pyth') !== -1) lp2 = j3pShuffle([['pyth', 1], ['pyth', 2], ['pyth', 3]])
    if (lp.indexOf('trigo') !== -1) lp3 = j3pShuffle([['trigo', 1], ['trigo', 2], ['trigo', 3], ['trigo', 4], ['trigo', 5], ['trigo', 6]])
    if (lp.indexOf('perim') !== -1) lp4 = j3pShuffle([['perim', 1], ['perim', 2]])

    do {
      switch (j3pGetRandomInt(0, 2)) {
        case 0:
          if (lp2.length > 0) lpi.push(lp2.pop())
          if (lp3.length > 0) lpi.push(lp3.pop())
          if (lp4.length > 0) lpi.push(lp4.pop())
          break
        case 1:
          if (lp3.length > 0) lpi.push(lp3.pop())
          if (lp4.length > 0) lpi.push(lp4.pop())
          if (lp2.length > 0) lpi.push(lp2.pop())
          break
        case 2:
          if (lp4.length > 0) lpi.push(lp4.pop())
          if (lp2.length > 0) lpi.push(lp2.pop())
          if (lp3.length > 0) lpi.push(lp3.pop())
          break
      }
    } while (lp2.length !== 0 || lp3.length !== 0 || lp4.length !== 0)

    for (let i = 0; i < ds.nbrepetitions; i++) {
      ds.lesExos.push(lpi[i % lpi.length])
    }
    ds.lesExos = j3pShuffle(ds.lesExos)
    stor.etapeEncours = ds.aFaire[ds.aFaire.length - 1]

    me.afficheTitre('Section d’une sphère, calculs associés')
    getMtgCore({ withMathJax: true })
      .then(
        // success
        (mtgAppLecteur) => {
          stor.mtgAppLecteur = mtgAppLecteur
          enonceMain()
        },
        // failure
        (error) => {
          j3pShowError(error, { message: 'mathgraph n’est pas correctement chargé', mustNotify: true })
        })
      // plantage dans enonceMain
      .catch(j3pShowError)
  }
  function creeLesDiv () {
    stor.lesdiv = {}
    stor.lesdiv.conteneur = j3pAddElt(me.zonesElts.MG, 'div', {
      style: me.styles.etendre('toutpetit.enonce', { padding: '10px' })
    })
    const cont1 = addDefaultTable(stor.lesdiv.conteneur, 1, 3)
    modif(cont1)
    stor.lesdiv.consigne = cont1[0][0]
    const contx = addDefaultTable(cont1[0][2], 2, 1)
    stor.lesdiv.calculatrice = contx[0][0]
    stor.lesdiv.correction = contx[1][0]
    const cont2 = addDefaultTable(stor.lesdiv.conteneur, 1, 3)
    modif(cont2)
    j3pAddContent(cont1[0][1], '&nbsp;&nbsp;&nbsp;')
    stor.lesdiv.figure = cont2[0][0]
    cont2[0][2].style.verticalAlign = 'top'
    const cont3 = addDefaultTable(cont2[0][2], 2, 1)
    modif(cont3)
    stor.lesdiv.travail = cont3[0][0]
    stor.lesdiv.solution1 = cont3[1][0]
    stor.lesdiv.explications = j3pAddElt(stor.lesdiv.conteneur, 'div')
    stor.lesdiv.travail.classList.add('travail')
    stor.lesdiv.consigne.classList.add('enonce')
    me.zonesElts.MG.classList.add('fond')
    stor.lesdiv.solution1.style.color = me.styles.cbien
    stor.lesdiv.explications.style.color = me.styles.cfaux
    stor.lesdiv.figure.style.verticalAlign = 'top'
    stor.lesdiv.travail.style.paddingLeft = '5px'
    stor.lesdiv.solution1.style.paddingLeft = '5px'
  }
  function suivEtape () {
    let i = ds.aFaire.indexOf(stor.etapeEncours) + 1
    if (i > ds.aFaire.length - 1) i = 0
    return ds.aFaire[i]
  }
  function enonceMain () {
    stor.etapeEncours = suivEtape()
    if (ds.nbetapes === 1 || stor.etapeEncours === 'prop') {
      me.videLesZones()
      creeLesDiv()
      me.fenetresjq = [{ name: 'Calculatrice', title: 'Une calculatrice', left: 600, top: 10 }]
      j3pCreeFenetres(me)
      j3pAfficheCroixFenetres('Calculatrice')
      j3pAjouteBouton(stor.lesdiv.calculatrice, boutonfen, { value: 'calculatrice', className: 'MepBoutons' })
      stor.lexo = faisChoixExo()
      poseQuestion0()
    }
    poseQuestion()
    me.finEnonce()
  }
  function isRepOk () {
    stor.errEcrit = ''
    stor.errArrond = false
    stor.errCAl = false
    stor.errApprox = false
    if (stor.etapeEncours === 'prop') {
      if (stor.liste.reponse !== stor.at1) {
        stor.liste.corrige(false)
        stor.liste.barre()
        return false
      }
      return true
    } else {
      const rep = stor.zone.reponse().replace(/ /g, '')
      const tt = verifNombreBienEcrit(rep, false, false)
      if (!tt.good) {
        stor.zone.corrige(false)
        stor.errEcrit = tt.remede
        return false
      }
      let ok = true
      if (stor.ceg) {
        if (stor.liste2.reponse !== '$=$') {
          stor.errApprox = true
          stor.liste2.corrige(false)
          ok = false
        }
      } else {
        if (stor.liste2.reponse !== '$\\approx$') {
          stor.errApprox = true
          stor.liste2.corrige(false)
          ok = false
        }
      }
      if (tt.nb !== stor.repAt2) {
        if (tt.nb === stor.arondFake) {
          stor.errArrond = true
          if (ds.Erreur_Arrondi) {
            stor.zone.corrige(false)
            ok = false
          }
        } else {
          stor.zone.corrige(false)
          stor.errCAl = true
          ok = false
        }
      }
      return ok
    }
  }
  function poseQuestion () {
    if (stor.etapeEncours === 'prop') {
      const tab1 = addDefaultTable(stor.lesdiv.travail, 2, 1)
      j3pAffiche(tab1[0][0], null, 'Pour calculer ' + stor.valQuest + ' je vais utiliser')
      const ll = j3pShuffle(['une formule de trigonométrie.', 'le théorème de Pythagore.', 'la formule du périmètre d’un cercle.', 'le théorème de Thalès.'])
      ll.splice(0, 0, 'Choisir')
      stor.liste = ListeDeroulante.create(tab1[1][0], ll, { onChange: passer })
    } else {
      j3pEmpty(stor.lesdiv.travail)
      j3pEmpty(stor.lesdiv.solution1)
      stor.lesdiv.solution1.classList.remove('correction')
      if (ds.aFaire.length === 2 || ds.Aide_Propriete) {
        const tabDeb = addDefaultTable(stor.lesdiv.travail, 1, 1)[0][0]
        switch (stor.lexo[0]) {
          case 'pyth':
            j3pAffiche(tabDeb, null, 'Le triangle ' + stor.lettres[2] + stor.lettres[0] + stor.lettres[3] + ' est rectangle en ' + stor.lettres[2] + ',<br> j’utilise le théorème de Pythagore.')
            break
          case 'trigo':
            j3pAffiche(tabDeb, null, 'Le triangle ' + stor.lettres[2] + stor.lettres[0] + stor.lettres[3] + ' est rectangle en ' + stor.lettres[2] + ',<br>')
            switch (stor.lexo[1]) {
              case 1:
                j3pAffiche(tabDeb, null, 'J’utilise $cos(\\widehat{' + stor.lettres[3] + stor.lettres[0] + stor.lettres[2] + '})$,')
                break
              case 2:
                j3pAffiche(tabDeb, null, 'J’utilise $tan(\\widehat{' + stor.lettres[3] + stor.lettres[0] + stor.lettres[2] + '})$,')
                break
              case 3:
                j3pAffiche(tabDeb, null, 'J’utilise $sin(\\widehat{' + stor.lettres[3] + stor.lettres[0] + stor.lettres[2] + '})$,')
                break
              case 4:
                j3pAffiche(tabDeb, null, 'J’utilise $cos(\\widehat{' + stor.lettres[3] + stor.lettres[0] + stor.lettres[2] + '})$,')
                break
              case 5:
                j3pAffiche(tabDeb, null, 'J’utilise $sin(\\widehat{' + stor.lettres[3] + stor.lettres[0] + stor.lettres[2] + '})$,')
                break
              case 6:
                j3pAffiche(tabDeb, null, 'J’utilise $cos(\\widehat{' + stor.lettres[3] + stor.lettres[0] + stor.lettres[2] + '})$,')
                break
            }
            break
          case 'perim':
            j3pAffiche(tabDeb, null, 'Je sais que $P(\\text{section}) = 2 \\pi ' + stor.lettres[2] + stor.lettres[3] + '$,')
            break
        }
      }
      const tabBrouill = addDefaultTable(stor.lesdiv.travail, 1, 5)
      stor.brouillon1 = new BrouillonCalculs({
        caseBoutons: tabBrouill[0][0],
        caseEgal: tabBrouill[0][1],
        caseCalculs: tabBrouill[0][2],
        caseApres: tabBrouill[0][3]
      }, { titre: ' ', limite: 30, limitenb: 20, restric: '=()sincota0123456789*/,.+-' + stor.lettres, hasAutoKeyboard: false, lmax: 3, justeg: true })
      const tabRep = addDefaultTable(stor.lesdiv.travail, 1, 7)
      j3pAddContent(tabRep[0][1], '&nbsp;')
      j3pAddContent(tabRep[0][3], '&nbsp;')
      stor.rpunit = tabRep[0][6]
      const ll = ['...', '$=$', '$\\approx$']
      stor.liste2 = ListeDeroulante.create(tabRep[0][2], ll, { sansFleche: true, onChange: adapteUnit })
      j3pAffiche(tabRep[0][0], null, stor.valQuest)
      j3pAffiche(tabRep[0][5], null, '&nbsp;' + stor.unitQuest + '&nbsp;')
      stor.zone = new ZoneStyleMathquill1(tabRep[0][4], { restric: '0123456789,.', limite: 8, enter: () => { me.sectionCourante() } })
      stor.zone.modifL = passer
    }
  }
  function adapteUnit () {
    passer()
    j3pEmpty(stor.rpunit)
    if (stor.liste2.reponse === '$\\approx$') {
      j3pAffiche(stor.rpunit, null, '<i>(arrondi à $0,01$ près)</i>')
    }
  }
  function disaAll () {
    if (stor.etapeEncours !== 'prop') {
      stor.zone.disable()
      stor.liste2.disable()
      stor.brouillon1.disable()
    }
  }
  function metTruetAll () {
    if (stor.etapeEncours === 'prop') {
      stor.liste.corrige(true)
    } else {
      stor.zone.corrige(true)
      stor.liste2.corrige(true)
    }
  }
  function yaReponse () {
    if (me.isElapsed) return true
    if (stor.etapeEncours === 'prop') {
      if (!stor.liste.changed) {
        stor.liste.focus()
        return false
      }
    } else {
      if (stor.zone.reponse() === '') {
        stor.zone.focus()
        return false
      }
      if (!stor.liste2.changed) {
        stor.liste2.focus()
        return false
      }
    }
    return true
  }
  function barreLesFo () {
    if (stor.etapeEncours !== 'prop') {
      if (stor.zone.isOk() === false) {
        stor.zone.barre()
      }
      if (stor.liste2.isOk() === false) {
        stor.liste2.barre()
      }
    }
  }

  function afflesco () {
    stor.lesdiv.solution1.style.verticalAlign = 'middle'
    switch (stor.lexo[0]) {
      case 'pyth': {
        j3pAffiche(stor.lesdiv.solution1, null, 'Le triangle ' + stor.lettres[2] + stor.lettres[0] + stor.lettres[3] + ' est rectangle en ' + stor.lettres[2] + ',<br> j’utilise le théorème de Pythagore.')
        const petitb = addDefaultTable(stor.lesdiv.solution1, 2, 6)
        j3pAddContent(petitb[0][0], 'Donc&nbsp;')
        j3pAffiche(petitb[0][1], null, '$' + stor.lettres[0] + stor.lettres[3] + '²$')
        j3pAffiche(petitb[0][2], null, '&nbsp;$=$&nbsp;')
        j3pAffiche(petitb[0][3], null, '$' + stor.lettres[0] + stor.lettres[2] + '²$')
        j3pAffiche(petitb[0][4], null, '&nbsp;$+$&nbsp;')
        j3pAffiche(petitb[0][5], null, '$' + stor.lettres[2] + stor.lettres[3] + '²$')
        j3pAffiche(petitb[1][1], null, '$' + stor.lettres[0] + stor.lettres[3] + '²$')
        j3pAffiche(petitb[1][2], null, '&nbsp;$=$&nbsp;')
        j3pAffiche(petitb[1][3], null, '$' + stor.lettres[0] + stor.lettres[2] + '²$')
        j3pAffiche(petitb[1][4], null, '&nbsp;$+$&nbsp;')
        j3pAffiche(petitb[1][5], null, '$' + stor.lettres[2] + stor.lettres[3] + '²$')
        j3pAddContent(stor.lesdiv.solution1, '&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;........ <br>')
        const petitb2 = addDefaultTable(stor.lesdiv.solution1, 3, 4)
        j3pAddContent(petitb2[1][0], 'Donc&nbsp;')
        j3pAffiche(petitb2[0][1], null, '$' + (stor.valQuest.replace(/\$/g, '')) + '²$')
        j3pAffiche(petitb2[0][2], null, '&nbsp;$=$&nbsp;')
        j3pAffiche(petitb2[0][3], null, '$' + ecrisBienMathquill(stor.valCarre) + '$ cm²')
        j3pAffiche(petitb2[1][1], null, stor.valQuest)
        j3pAffiche(petitb2[1][2], null, '&nbsp;$=$&nbsp;')
        j3pAffiche(petitb2[1][3], null, '$\\sqrt{' + ecrisBienMathquill(stor.valCarre) + '}$')
        j3pAffiche(petitb2[2][1], null, stor.valQuest)
        j3pAffiche(petitb2[2][2], null, '&nbsp;$' + ((stor.ceg) ? '=' : '\\approx') + '$&nbsp;')
        j3pAffiche(petitb2[2][3], null, '$' + stor.repAt2 + '$ cm' + (((stor.ceg) ? '' : ' <i>(arrondi à $0,01$ près)</i>')))
        switch (stor.lexo[1]) {
          case 1:
            j3pEmpty(petitb[1][3])
            j3pAffiche(petitb[1][3], null, '$' + stor.distanceCentre + '²$')
            j3pEmpty(petitb[1][5])
            j3pAffiche(petitb[1][5], null, '$' + stor.petitRayon + '²$')
            break
          case 2:
            j3pEmpty(petitb[1][1])
            j3pAffiche(petitb[1][1], null, '$' + stor.grandRayon + '²$')
            j3pEmpty(petitb[1][3])
            j3pAffiche(petitb[1][3], null, '$' + stor.distanceCentre + '²$')
            break
          case 3:
            j3pEmpty(petitb[1][1])
            j3pAffiche(petitb[1][1], null, '$' + stor.grandRayon + '²$')
            j3pEmpty(petitb[1][5])
            j3pAffiche(petitb[1][5], null, '$' + stor.petitRayon + '²$')
            break
        }
      }
        break
      case 'trigo':
        j3pAffiche(stor.lesdiv.solution1, null, 'Le triangle ' + stor.lettres[2] + stor.lettres[0] + stor.lettres[3] + ' est rectangle en ' + stor.lettres[2] + ',')
        switch (stor.lexo[1]) {
          case 1: {
            const petitb = addDefaultTable(stor.lesdiv.solution1, 4, 1)
            j3pAffiche(petitb[0][0], null, '$cos(\\widehat{' + stor.lettres[3] + stor.lettres[0] + stor.lettres[2] + '})=\\frac{' + stor.lettres[0] + stor.lettres[2] + '}{' + stor.lettres[0] + stor.lettres[3] + '}$')
            j3pAffiche(petitb[1][0], null, '$cos(' + stor.angle1 + '°)=\\frac{' + stor.distanceCentre + '}{' + stor.lettres[0] + stor.lettres[3] + '}$')
            j3pAffiche(petitb[2][0], null, 'Donc $' + stor.lettres[0] + stor.lettres[3] + '=\\frac{' + stor.distanceCentre + '}{cos(' + stor.angle1 + '°)}$')
            j3pAffiche(petitb[3][0], null, 'soit $' + stor.lettres[0] + stor.lettres[3] + ((stor.ceg) ? '=' : '\\approx') + stor.repAt2 + '$ cm' + (((stor.ceg) ? '' : ' <i>(arrondi à $0,01$ près)</i>')))
          }
            break
          case 2: {
            const petitb = addDefaultTable(stor.lesdiv.solution1, 4, 1)
            j3pAffiche(petitb[0][0], null, '$tan(\\widehat{' + stor.lettres[3] + stor.lettres[0] + stor.lettres[2] + '})=\\frac{' + stor.lettres[2] + stor.lettres[3] + '}{' + stor.lettres[0] + stor.lettres[2] + '}$')
            j3pAffiche(petitb[1][0], null, '$tan(' + stor.angle1 + '°)=\\frac{' + stor.lettres[2] + stor.lettres[3] + '}{' + stor.distanceCentre + '}$')
            j3pAffiche(petitb[2][0], null, 'Donc $' + stor.lettres[2] + stor.lettres[3] + '=' + stor.distanceCentre + ' \\times tan(' + stor.angle1 + '°)$')
            j3pAffiche(petitb[3][0], null, 'soit $' + stor.lettres[2] + stor.lettres[3] + ((stor.ceg) ? '=' : '\\approx') + stor.repAt2 + '$ cm' + (((stor.ceg) ? '' : ' <i>(arrondi à $0,01$ près)</i>')))
          }
            break
          case 3: {
            const petitb = addDefaultTable(stor.lesdiv.solution1, 4, 1)
            j3pAffiche(petitb[0][0], null, '$sin(\\widehat{' + stor.lettres[3] + stor.lettres[0] + stor.lettres[2] + '})=\\frac{' + stor.lettres[2] + stor.lettres[3] + '}{' + stor.lettres[0] + stor.lettres[3] + '}$')
            j3pAffiche(petitb[1][0], null, '$sin(' + stor.angle1 + '°)=\\frac{' + stor.lettres[2] + stor.lettres[3] + '}{' + stor.grandRayon + '}$')
            j3pAffiche(petitb[2][0], null, 'Donc $' + stor.lettres[2] + stor.lettres[3] + '=' + stor.grandRayon + ' \\times sin(' + stor.angle1 + '°)$')
            j3pAffiche(petitb[3][0], null, 'soit $' + stor.lettres[2] + stor.lettres[3] + ((stor.ceg) ? '=' : '\\approx') + stor.repAt2 + '$ cm' + (((stor.ceg) ? '' : ' <i>(arrondi à $0,01$ près)</i>')))
          }
            break
          case 4: {
            const petitb = addDefaultTable(stor.lesdiv.solution1, 4, 1)
            j3pAffiche(petitb[0][0], null, '$cos(\\widehat{' + stor.lettres[3] + stor.lettres[0] + stor.lettres[2] + '})=\\frac{' + stor.lettres[0] + stor.lettres[2] + '}{' + stor.lettres[0] + stor.lettres[3] + '}$')
            j3pAffiche(petitb[1][0], null, '$cos(' + stor.angle1 + '°)=\\frac{' + stor.lettres[0] + stor.lettres[2] + '}{' + stor.grandRayon + '}$')
            j3pAffiche(petitb[2][0], null, 'Donc $' + stor.lettres[0] + stor.lettres[2] + '=' + stor.grandRayon + ' \\times cos(' + stor.angle1 + '°)$')
            j3pAffiche(petitb[3][0], null, 'soit $' + stor.lettres[0] + stor.lettres[2] + ((stor.ceg) ? '=' : '\\approx') + stor.repAt2 + '$ cm' + (((stor.ceg) ? '' : ' <i>(arrondi à $0,01$ près)</i>')))
          }
            break
          case 5: {
            const petitb = addDefaultTable(stor.lesdiv.solution1, 4, 1)
            j3pAffiche(petitb[0][0], null, '$sin(\\widehat{' + stor.lettres[3] + stor.lettres[0] + stor.lettres[2] + '})=\\frac{' + stor.lettres[2] + stor.lettres[3] + '}{' + stor.lettres[0] + stor.lettres[3] + '}$')
            j3pAffiche(petitb[1][0], null, '$sin(\\widehat{' + stor.lettres[3] + stor.lettres[0] + stor.lettres[2] + '})=\\frac{' + stor.petitRayon + '}{' + stor.grandRayon + '}$')
            j3pAffiche(petitb[2][0], null, 'Donc $\\widehat{' + stor.lettres[3] + stor.lettres[0] + stor.lettres[2] + '} = \\arcsin\\left(\\frac{' + stor.petitRayon + '}{' + stor.grandRayon + '}\\right)$')
            j3pAffiche(petitb[3][0], null, 'soit $\\widehat{' + stor.lettres[3] + stor.lettres[0] + stor.lettres[2] + '}' + ((stor.ceg) ? '=' : '\\approx') + stor.repAt2 + '$ °' + (((stor.ceg) ? '' : ' <i>(arrondi à $0,01$ près)</i>')))
          }
            break
          case 6: {
            const petitb = addDefaultTable(stor.lesdiv.solution1, 4, 1)
            j3pAffiche(petitb[0][0], null, '$cos(\\widehat{' + stor.lettres[3] + stor.lettres[0] + stor.lettres[2] + '})=\\frac{' + stor.lettres[0] + stor.lettres[2] + '}{' + stor.lettres[0] + stor.lettres[3] + '}$')
            j3pAffiche(petitb[1][0], null, '$cos(\\widehat{' + stor.lettres[3] + stor.lettres[0] + stor.lettres[2] + '})=\\frac{' + stor.distanceCentre + '}{' + stor.grandRayon + '}$')
            j3pAffiche(petitb[2][0], null, 'Donc $\\widehat{' + stor.lettres[3] + stor.lettres[0] + stor.lettres[2] + '} = \\arccos\\left(\\frac{' + stor.distanceCentre + '}{' + stor.grandRayon + '}\\right)$')
            j3pAffiche(petitb[3][0], null, 'soit $\\widehat{' + stor.lettres[3] + stor.lettres[0] + stor.lettres[2] + '}' + ((stor.ceg) ? '=' : '\\approx') + stor.repAt2 + '$ °' + (((stor.ceg) ? '' : ' <i>(arrondi à $0,01$ près)</i>')))
          }
            break
        }
        break
      case 'perim':
        switch (stor.lexo[1]) {
          case 1: {
            const petitb = addDefaultTable(stor.lesdiv.solution1, 4, 1)
            j3pAffiche(petitb[0][0], null, '$P(\\text{section})= 2 \\pi ' + stor.lettres[0] + stor.lettres[3] + '$')
            j3pAffiche(petitb[1][0], null, '$P(\\text{section})= 2 \\pi \\times ' + stor.petitRayon + '$')
            j3pAffiche(petitb[2][0], null, '$P(\\text{section})= ' + (2 * stor.petitRayon) + '\\pi$ cm')
            j3pAffiche(petitb[3][0], null, '$P(\\text{section}) \\approx ' + stor.repAt2 + '$ cm <i>(arrondi à $0,01$ près)</i>')
          }
            break
          case 2: {
            const petitb = addDefaultTable(stor.lesdiv.solution1, 4, 1)
            j3pAffiche(petitb[0][0], null, '$P(\\text{section})= 2 \\pi ' + stor.lettres[0] + stor.lettres[3] + '$')
            j3pAffiche(petitb[1][0], null, 'donc $' + stor.lettres[0] + stor.lettres[3] + '= \\frac{P(\\text{section})}{2 \\pi}$')
            j3pAffiche(petitb[2][0], null, '&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp; $' + stor.lettres[0] + stor.lettres[3] + '= \\frac{' + stor.perimetre + '}{2 \\pi}$')
            j3pAffiche(petitb[3][0], null, '&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp; $' + stor.lettres[0] + stor.lettres[3] + ' \\approx ' + stor.repAt2 + '$ cm <i>(arrondi à $0,01$ près)</i>')
          }
            break
        }
        break
    }
  }
  function affCorrFaux (isFin) {
    if (stor.errEcrit !== '') {
      stor.yaexplik = true
      j3pAffiche(stor.lesdiv.explications, null, stor.errEcrit)
    }
    if (stor.errArrond) {
      stor.yaexplik = true
      j3pAffiche(stor.lesdiv.explications, null, 'Erreur d’arrondi !<br>')
    }
    if (stor.errCAl) {
      stor.yaexplik = true
      j3pAffiche(stor.lesdiv.explications, null, 'Erreur de calcul !')
    }
    if (stor.errApprox && !stor.errCAl) {
      stor.yaexplik = true
      if (stor.ceg) {
        j3pAffiche(stor.lesdiv.explications, null, 'Il faut utiliser le symbole $=$ quand<br>le résultat est exact !')
      } else {
        j3pAffiche(stor.lesdiv.explications, null, 'Il faut utiliser le symbole $\\approx$ pour<br>une valeur approchée !')
      }
    }
    stor.lesdiv.solution1.classList.add('correction')
    if (isFin) {
      barreLesFo()
      disaAll()
      if (stor.etapeEncours === 'prop') {
        j3pAffiche(stor.lesdiv.solution1, null, stor.explik1)
      } else {
        afflesco()
      }
    }

    if (stor.yaexplik) stor.lesdiv.explications.classList.add('explique')
  }

  switch (this.etat) {
    case 'enonce':
      // code exécuté au lancement de la section
      if (me.debutDeLaSection) {
        initSection()
      } else {
        enonceMain()
      }
      break // case "enonce":

    case 'correction':
      // On teste si une réponse a été saisie
      stor.yaexplik = false
      stor.yaco = false
      stor.lesdiv.explications.innerHTML = ''
      stor.lesdiv.correction.innerHTML = ''

      if (!yaReponse()) {
        // pas de réponse
        stor.lesdiv.correction.style.color = this.styles.cfaux
        stor.lesdiv.correction.innerHTML = reponseIncomplete
        // on reste dans l’état correction
        this.finCorrection()
        return
      }

      if (isRepOk()) {
        // Bonne réponse
        this.score++
        stor.lesdiv.correction.style.color = this.styles.cbien
        stor.lesdiv.correction.innerHTML = cBien
        this.typederreurs[0]++
        metTruetAll()
        disaAll()
        if (stor.etapeEncours !== 'prop') {
          j3pAffiche(stor.lesdiv.solution1, null, 'En effet, <br>')
          stor.lesdiv.solution1.classList.add('correction')
          afflesco()
          if (stor.errArrond) {
            j3pAffiche(stor.lesdiv.explications, null, 'Erreur d’arrondi')
            stor.lesdiv.explications.classList.add('explique')
          }
        }
        this.finCorrection('navigation', true)
        return
      }
      // Pas de bonne réponse
      stor.lesdiv.correction.style.color = this.styles.cfaux
      stor.lesdiv.correction.innerHTML = cFaux
      if (this.isElapsed) {
        // A cause de la limite de temps :
        stor.lesdiv.correction.innerHTML += tempsDepasse + '<BR>'
        this.typederreurs[10]++
        affCorrFaux(true)
        stor.yaco = true
        this.finCorrection('navigation', true)
        return
      }

      // Réponse fausse sans pb de temps
      if (me.essaiCourant < ds.nbchances && stor.etapeEncours !== 'prop') {
        stor.lesdiv.correction.innerHTML += '<br>' + essaieEncore
        affCorrFaux(false)
        // affCorrFaux.call(this, false)
        this.typederreurs[1]++
        this.finCorrection()
        return
      }
      // Erreur au dernier essai
      affCorrFaux(true)
      stor.yaco = true
      this.typederreurs[2]++
      // on donne la moitié des points pour une erreur d’arrondi
      if (stor.errArrond) this.score += 0.8
      this.finCorrection('navigation', true)
      break // case "correction":

    case 'navigation':
      if (!this.sectionTerminee()) this.etat = 'enonce'
      this.finNavigation()
      break // case "navigation":
  }
}
