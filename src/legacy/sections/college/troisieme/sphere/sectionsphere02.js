import { j3pAddContent, j3pAddElt, j3pGetNewId, j3pShowError, j3pShuffle } from 'src/legacy/core/functions'
import { addDefaultTable } from 'src/legacy/themes/table'
import { getMtgCore } from 'src/lib/outils/mathgraph'
import { j3pCreeSVG } from 'src/legacy/core/functionsSvg'
import { j3pAffiche } from 'src/lib/mathquill/functions'
import ZoneStyleMathquill1 from 'src/legacy/outils/zoneStyleMathquill/ZoneStyleMathquill1'
import ListeDeroulante from 'src/legacy/outils/listeDeroulante/ListeDeroulante'
import textesGeneriques from 'src/lib/core/textes'

import fig from './FigSph'
import rose from './rose.png'

const { cBien, cFaux, essaieEncore, reponseIncomplete, tempsDepasse } = textesGeneriques

export const params = {
  outils: ['mathquill'],
  /**
   * Ce qui est paramétrable dans l’éditeur de graphe
   * @type {LegacySectionParamsParametres}
   */
  parametres: [
    ['nbrepetitions', 4, 'entier', 'Nombre de répétitions de la section'],
    ['nbchances', 2, 'entier', 'Nombre d’essais possibles (0  signifie illimité)'],
    ['limite', 0, 'entier', 'Temps disponible par répétition, en secondes (0  signifie illimité)'],
    ['Question', 'Les deux', 'liste', '<u><b>Retrouver</b></u> ou <u><b>Placer</b></u> un point en utilisant les coordonnées terrestres.', ['Retrouver', 'Placer', 'Les deux']],
    ['theme', 'standard', 'liste', 'Modèle pour l’affichage', ['standard', 'zonesAvecImageDeFond']]
  ]
}

/**
 * section sphere02
 * @this {Parcours}
 */
export default function main () {
  const me = this
  const stor = me.storage
  const ds = me.donneesSection
  const listVille = [
    {
      nom: 'Paris',
      lat: 48,
      ns: 'N',
      long: 2,
      eo: 'E',
      int1: [90, 271],
      int2: [0, 0]
    },
    {
      nom: 'Dallas',
      lat: 32,
      ns: 'N',
      long: 96,
      eo: 'W',
      int1: [186, 361],
      int2: [0, 0]
    },
    {
      nom: 'Mexico',
      lat: 23,
      ns: 'N',
      long: 102,
      eo: 'W',
      int1: [-1, 20],
      int2: [190, 361]
    },
    {
      nom: 'Brasilia',
      lat: 14,
      ns: 'S',
      long: 51,
      eo: 'W',
      int1: [-1, 2],
      int2: [140, 321]
    },
    {
      nom: 'Rio grande',
      lat: 32,
      ns: 'S',
      long: 52,
      eo: 'W',
      int1: [143, 324],
      int2: [361, 361]
    },
    {
      nom: 'New Delhi',
      lat: 28,
      ns: 'N',
      long: 77,
      eo: 'E',
      int1: [13, 194],
      int2: [358, 361]
    },
    {
      nom: 'Prétoria',
      lat: 25,
      ns: 'S',
      long: 28,
      eo: 'E',
      int1: [60, 245],
      int2: [361, 361]
    },
    {
      nom: 'Bamako',
      lat: 12,
      ns: 'N',
      long: 8,
      eo: 'W',
      int1: [95, 281],
      int2: [0, 0]
    },
    {
      nom: 'Bagdad',
      lat: 33,
      ns: 'N',
      long: 44,
      eo: 'E',
      int1: [45, 226],
      int2: [0, 0]
    },
    {
      nom: 'Pékin',
      lat: 39,
      ns: 'N',
      long: 116,
      eo: 'E',
      int1: [-1, 155],
      int2: [333, 361]
    },
    {
      nom: 'Moscou',
      lat: 55,
      ns: 'N',
      long: 37,
      eo: 'E',
      int1: [50, 235],
      int2: [355, 361]
    },
    {
      nom: 'Sidney',
      lat: 32,
      ns: 'S',
      long: 140,
      eo: 'E',
      int1: [-1, 130],
      int2: [305, 361]
    }
  ]
  const acache = ['lacircleS', 'surfAfri', 'surfAm1', 'surfAm2', 'surfAm3', 'surfAm4', 'surfAm5', 'surfAm6', 'surfAm7', 'surfAm8', 'surfAm9', 'surfAm15', 'Ville1', 'Ville2', 'Ville3', 'Ville4', 'Ville5', 'Ville6', 'Ville7', 'Ville8', 'Ville9', 'Ville10', 'Ville11', 'Ville12']
  const amontre = [6800, 6799]

  function initSection () {
    // let callParent = () => undefined
    stor.achcheNom = []
    me.validOnEnter = false // ex donneesSection.touche_entree

    me.construitStructurePage('presentation3')
    me.stockage = [0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0]

    me.afficheTitre('Corrdonnées terrestres')

    stor.exoPos = []
    let lp = []
    if (ds.Question === 'Les deux' || ds.Question === 'Retrouver') lp.push('Ret')
    if (ds.Question === 'Les deux' || ds.Question === 'Placer') lp.push('Pla')
    lp = j3pShuffle(lp)
    for (let i = 0; i < ds.nbitems; i++) {
      stor.exoPos.push({ quest: lp[i % lp.length] })
    }
    stor.exoPos = j3pShuffle(stor.exoPos)

    lp = j3pShuffle(listVille)
    for (let i = 0; i < ds.nbitems; i++) {
      stor.exoPos[i].ville = lp[i % lp.length]
    }
    stor.exoPos = j3pShuffle(stor.exoPos)

    getMtgCore({ withMathJax: true })
      .then(
        // success
        (mtgAppLecteur) => {
          stor.mtgApp = mtgAppLecteur
          enonceMain()
        },
        // failure
        (error) => {
          j3pShowError(error, { message: 'mathgraph n’est pas correctement chargé', mustNotify: true })
        })
      // plantage dans enonceMain
      .catch(j3pShowError)
  }
  function modif (tab) {
    for (let i = 0; i < tab.length; i++) {
      for (let j = 0; j < tab[i].length; j++) {
        tab[i][j].style.padding = '0px'
      }
    }
  }
  function creeLesDiv () {
    stor.lesdiv = {}
    stor.lesdiv.conteneur = j3pAddElt(me.zonesElts.MG, 'div', {
      style: me.styles.etendre('toutpetit.enonce', { padding: '10px' })
    })
    const cont1 = addDefaultTable(stor.lesdiv.conteneur, 1, 1)
    modif(cont1)
    stor.lesdiv.consigne = cont1[0][0]
    const cont2 = addDefaultTable(stor.lesdiv.conteneur, 1, 2)
    modif(cont2)
    stor.lesdiv.figure = cont2[0][0]
    cont2[0][1].style.verticalAlign = 'top'
    const cont3 = addDefaultTable(cont2[0][1], 5, 1)
    modif(cont3)
    stor.lesdiv.correction = cont3[0][0]
    stor.lesdiv.travail = cont3[1][0]
    stor.lesdiv.solution1 = cont3[3][0]
    stor.lesdiv.explications = cont3[2][0]
    stor.lesdiv.rose = cont3[4][0]
    stor.lesdiv.rose.style.height = '100%'
    stor.lesdiv.correction.style.paddingLeft = '5px'
    stor.lesdiv.solution1.style.paddingLeft = '5px'
    stor.lesdiv.explications.style.paddingLeft = '5px'
    stor.lesdiv.travail.style.paddingLeft = '5px'
    stor.lesdiv.travail.classList.add('travail')
    stor.lesdiv.consigne.classList.add('enonce')
    me.zonesElts.MG.classList.add('fond')
    stor.lesdiv.solution1.style.color = me.styles.cbien
    stor.lesdiv.explications.style.color = me.styles.cfaux
    stor.lesdiv.figure.style.verticalAlign = 'top'
    stor.lesdiv.rose.style.paddingTop = '80px'
    stor.lesdiv.travail.style.paddingLeft = '5px'
    j3pAddElt(stor.lesdiv.rose, 'img', '', { src: rose, alt: 'Charlie', border: '1px solid black' })
  }
  function enonceMain () {
    stor.tag = []
    me.videLesZones()
    creeLesDiv()
    stor.exo = stor.exoPos.pop()
    const quest = (stor.exo.quest === 'Ret') ? ('<b>Retrouve les coordonnées terrestres de ' + stor.exo.ville.nom + '</b>') : ('<b>Quelle ville a pour coordonnées terrestres</b>  $' + stor.exo.ville.lat + '°\\text{ }' + stor.exo.ville.ns + '\\text{ }' + stor.exo.ville.long + '°\\text{ }' + stor.exo.ville.eo.replace('W', 'O') + '$ <b>?</b>')
    j3pAffiche(stor.lesdiv.consigne, null, quest)
    stor.svgId = j3pGetNewId()
    stor.mtgApp.removeAllDoc()
    stor.acache = []
    const wi = 400
    const lar = 400
    stor.lesdiv.travail.style.paddingLeft = '5px'
    stor.lafig = fig()
    if (stor.exo.quest === 'Ret') {
      const tabh = addDefaultTable(stor.lesdiv.travail, 1, 7)
      stor.zoneLat = new ZoneStyleMathquill1(tabh[0][0], { restric: '0123456789', enter: () => { me.sectionCourante() }, limite: 2 })
      j3pAddContent(tabh[0][1], '°&nbsp;')
      stor.listeNS = ListeDeroulante.create(tabh[0][2], ['...', '$N$', '$S$'], { sansFleche: true })
      j3pAddContent(tabh[0][3], '&nbsp;&nbsp;')
      stor.zoneLong = new ZoneStyleMathquill1(tabh[0][4], { restric: '0123456789', enter: () => { me.sectionCourante() }, limite: 2 })
      j3pAddContent(tabh[0][5], '°&nbsp;')
      stor.listeEO = ListeDeroulante.create(tabh[0][6], ['...', '$E$', '$O$'], { sansFleche: true })
    } else {
      const tabh = addDefaultTable(stor.lesdiv.travail, 1, 2)
      j3pAddContent(tabh[0][0], 'La ville est&nbsp;')
      const vpos = j3pShuffle(listVille)
      let ll = []
      for (let i = 0; i < 7; i++) { ll.push(vpos[i].nom) }
      if (ll.indexOf(stor.exo.ville.nom) === -1) {
        ll.pop()
        ll.push(stor.exo.ville.nom)
        ll = j3pShuffle(ll)
      }
      ll.splice(0, 0, 'Choisir')
      stor.listeNS = ListeDeroulante.create(tabh[0][1], ll)
    }
    j3pCreeSVG(stor.lesdiv.figure, { id: stor.svgId, width: wi, height: lar, style: { border: 'solid black 1px' } })
    stor.mtgApp.addDoc(stor.svgId, stor.lafig, true)
    stor.mtgApp.calculateAndDisplayAll()
    stor.mtgApp.addCallBackToSVGListener(stor.svgId, 'mousemove', () => {
      if (stor.mtgApp.getDoc(stor.svgId).pointCapture) {
        const tag = stor.mtgApp.getDoc(stor.svgId).pointCapture.tag
        if (tag === 'POINTlat' || tag === 'POINTlong') {
          cacheEtMontre()
        } else {
          ontreEtCache()
        }
      } else {
        ontreEtCache()
        replaceLesCurseurs()
      }
    })
    stor.mtgApp.addEventListener(stor.svgId, '#NgfdC2', 'mouseover', () => {
      stor.mtgApp.getDoc(stor.svgId).defaultCursor = 'pointer'
    })
    stor.mtgApp.addEventListener(stor.svgId, '#NgfdC2', 'mouseout', () => {
      stor.mtgApp.getDoc(stor.svgId).defaultCursor = ''
    })
    stor.mtgApp.addEventListener(stor.svgId, '#NgfdC2', 'click', () => {
      cacheEtMontre()
      const b = stor.mtgApp.getPointPosition(stor.svgId, '#potCursLo')
      stor.mtgApp.setPointPosition(stor.svgId, '#POINTlong', Math.min(b.x + 0.69, 360), b.y, true)
    })
    stor.mtgApp.addEventListener(stor.svgId, '#NgfdC3', 'mouseover', () => {
      stor.mtgApp.getDoc(stor.svgId).defaultCursor = 'pointer'
    })
    stor.mtgApp.addEventListener(stor.svgId, '#NgfdC3', 'mouseout', () => {
      stor.mtgApp.getDoc(stor.svgId).defaultCursor = ''
    })
    stor.mtgApp.addEventListener(stor.svgId, '#NgfdC3', 'click', () => {
      cacheEtMontre()
      const b = stor.mtgApp.getPointPosition(stor.svgId, '#potCursLo')
      stor.mtgApp.setPointPosition(stor.svgId, '#POINTlong', Math.max(b.x - 0.69, 110), b.y, true)
    })
    stor.mtgApp.addEventListener(stor.svgId, '#NgfdC', 'mouseover', () => {
      stor.mtgApp.getDoc(stor.svgId).defaultCursor = 'pointer'
    })
    stor.mtgApp.addEventListener(stor.svgId, '#NgfdC', 'mouseout', () => {
      stor.mtgApp.getDoc(stor.svgId).defaultCursor = ''
    })
    stor.mtgApp.addEventListener(stor.svgId, '#NgfdC', 'click', () => {
      const b = stor.mtgApp.getPointPosition(stor.svgId, '#potCursL')
      cacheEtMontre()
      stor.mtgApp.setPointPosition(stor.svgId, '#POINTlat', Math.min(b.x + 1.38, 360), b.y, true)
    })
    stor.mtgApp.addEventListener(stor.svgId, '#NgfdC4', 'mouseover', () => {
      stor.mtgApp.getDoc(stor.svgId).defaultCursor = 'pointer'
    })
    stor.mtgApp.addEventListener(stor.svgId, '#NgfdC4', 'mouseout', () => {
      stor.mtgApp.getDoc(stor.svgId).defaultCursor = ''
    })
    stor.mtgApp.addEventListener(stor.svgId, '#NgfdC4', 'click', () => {
      cacheEtMontre()
      const b = stor.mtgApp.getPointPosition(stor.svgId, '#potCursL')
      stor.mtgApp.setPointPosition(stor.svgId, '#POINTlat', Math.max(b.x - 1.38, 110), b.y, true)
    })
    ontreEtCache()
    stor.lesdiv.figure.classList.add((stor.exo === 'Ret') ? 'enonce' : 'travail')
    me.finEnonce()
  }

  function cacheEtMontre () {
    for (let i = 0; i < acache.length; i++) {
      stor.mtgApp.setVisible(stor.svgId, '#' + acache[i], false)
    }
    for (let i = 0; i < amontre.length; i++) {
      stor.mtgApp.setVisible(stor.svgId, amontre[i], true)
    }
  }
  function ontreEtCache () {
    for (let i = 0; i < acache.length; i++) {
      stor.mtgApp.setVisible(stor.svgId, '#' + acache[i], true)
    }
    for (let i = 0; i < amontre.length; i++) {
      stor.mtgApp.setVisible(stor.svgId, amontre[i], false)
    }
  }
  function replaceLesCurseurs () {
    const a = stor.mtgApp.getPointPosition(stor.svgId, '#potCursL')
    stor.mtgApp.setPointPosition(stor.svgId, '#POINTlat', a.x, a.y, true)
    const b = stor.mtgApp.getPointPosition(stor.svgId, '#potCursLo')
    stor.mtgApp.setPointPosition(stor.svgId, '#POINTlong', b.x, b.y, true)
  }

  function yaReponse () {
    if (me.isElapsed) return true
    if (stor.exo.quest === 'Ret') {
      if (stor.zoneLat.reponse() === '') {
        stor.zoneLat.focus()
        return false
      }
      if (!stor.listeNS.changed) {
        stor.listeNS.focus()
        return false
      }
      if (stor.zoneLong.reponse() === '') {
        stor.zoneLong.focus()
        return false
      }
      if (!stor.listeEO.changed) {
        stor.listeEO.focus()
        return false
      }
    } else {
      if (!stor.listeNS.changed) {
        stor.listeNS.focus()
        return false
      }
    }
    return true
  }
  function isRepOk () {
    stor.errLAt = false
    stor.errLong = false
    stor.errNS = false
    stor.errEO = false
    if (stor.exo.quest === 'Ret') {
      let ok = true
      const repLat = Number(stor.zoneLat.reponse())
      const repNS = stor.listeNS.reponse
      const repLong = Number(stor.zoneLong.reponse())
      const repEO = stor.listeEO.reponse
      if (Math.abs(repLat - stor.exo.ville.lat) > 1) {
        stor.errLAt = true
        ok = false
        stor.zoneLat.corrige(false)
      }
      if (Math.abs(repLong - stor.exo.ville.long) > 1) {
        stor.errLong = true
        ok = false
        stor.zoneLong.corrige(false)
      }
      if (repNS !== '$' + stor.exo.ville.ns + '$') {
        stor.errNS = true
        ok = false
        stor.listeNS.corrige(false)
      }
      if (repEO !== '$' + stor.exo.ville.eo.replace('W', 'O') + '$') {
        stor.errEO = true
        ok = false
        stor.listeEO.corrige(false)
      }
      return ok
    } else {
      const repVille = stor.listeNS.reponse
      if (repVille !== stor.exo.ville.nom) {
        stor.listeNS.corrige(false)
        return false
      }
      return true
    }
  }
  function metTruetAll () {
    if (stor.exo.quest === 'Ret') {
      stor.zoneLat.corrige(true)
      stor.listeNS.corrige(true)
      stor.zoneLong.corrige(true)
      stor.listeEO.corrige(true)
    } else {
      stor.listeNS.corrige(true)
    }
  }
  function disaAll () {
    if (stor.exo.quest === 'Ret') {
      stor.zoneLat.disable()
      stor.listeNS.disable()
      stor.zoneLong.disable()
      stor.listeEO.disable()
    } else {
      stor.listeNS.disable()
    }
  }
  function barreLesFo () {
    if (stor.exo.quest === 'Ret') {
      if (stor.zoneLat.isOk() === false) {
        stor.zoneLat.barre()
      }
      if (stor.listeNS.isOk() === false) {
        stor.listeNS.barre()
      }
      if (stor.zoneLong.isOk() === false) {
        stor.zoneLong.barre()
      }
      if (stor.listeEO.isOk() === false) {
        stor.listeEO.barre()
      }
    } else {
      if (stor.listeNS.isOk() === false) {
        stor.listeNS.barre()
      }
    }
  }
  function metAuBonEndroit () {
    let am = (stor.exo.ville.eo === 'E') ? (180 - stor.exo.ville.long + 20) : (stor.exo.ville.long + 180 - 20)
    if (am < 0) am += 360
    if (am > 360) am -= 360
    const bm = (stor.exo.ville.eo === 'E') ? (-stor.exo.ville.long) : (stor.exo.ville.long)
    const lm = (stor.exo.ville.ns === 'N') ? (stor.exo.ville.lat) : (-stor.exo.ville.lat)
    stor.mtgApp.setVisible(stor.svgId, '#POINTlong', false)
    stor.mtgApp.setVisible(stor.svgId, '#POINTlat', false)
    stor.mtgApp.setVisible(stor.svgId, '#NgfdC3', false)
    stor.mtgApp.setVisible(stor.svgId, '#NgfdC4', false)
    stor.mtgApp.setVisible(stor.svgId, '#NgfdC3S', false)
    stor.mtgApp.setVisible(stor.svgId, '#NgfdC4S', false)
    stor.mtgApp.setVisible(stor.svgId, '#fhdsk2', false)
    stor.mtgApp.setVisible(stor.svgId, '#fhdsk', false)
    stor.mtgApp.setVisible(stor.svgId, '#segcursL', false)
    stor.mtgApp.setVisible(stor.svgId, '#potCursL', false)
    stor.mtgApp.setVisible(stor.svgId, '#peticursL', false)
    stor.mtgApp.setVisible(stor.svgId, '#segcursLo', false)
    stor.mtgApp.setVisible(stor.svgId, '#potCursLo', false)
    stor.mtgApp.setVisible(stor.svgId, '#peticursLo', false)
    stor.mtgApp.setVisible(stor.svgId, '#surface987', false)
    stor.mtgApp.setVisible(stor.svgId, '#surfacgfde987', false)
    stor.mtgApp.setText(stor.svgId, '#LatitudeText', 'Latitude: ' + stor.exo.ville.lat + '° ' + ((stor.exo.ville.ns === 'N') ? 'Nord' : 'Sud'))
    stor.mtgApp.setText(stor.svgId, '#LongitudeText', 'Longitude: ' + stor.exo.ville.long + '° ' + ((stor.exo.ville.eo === 'E') ? 'Est' : 'Ouest'))
    stor.mtgApp.giveFormula2(stor.svgId, 'valCurseurb', am, true)
    stor.mtgApp.giveFormula2(stor.svgId, 'latiP', lm, true)
    stor.mtgApp.giveFormula2(stor.svgId, 'longiP', bm, true)
    stor.mtgApp.updateFigure(stor.svgId)
    setTimeout(() => { stor.mtgApp.giveFormula2(stor.svgId, 'valCurseurb', 'C1potCurs/C1C2*360', true) }, 10)
  }

  function affCorrFaux (isFin) {
    if (stor.errLAt) {
      stor.yaexplik = true
      j3pAffiche(stor.lesdiv.explications, null, 'Erreur sur la latitude. <br>')
    }
    if (stor.errLong) {
      stor.yaexplik = true
      j3pAffiche(stor.lesdiv.explications, null, 'Erreur sur la longitude. <br>')
    }
    if (stor.errEO) {
      stor.yaexplik = true
      j3pAffiche(stor.lesdiv.explications, null, 'L\' Est est vers la droite. <br>')
    }
    if (stor.errNS) {
      stor.yaexplik = true
      j3pAffiche(stor.lesdiv.explications, null, 'Le Nord est vers le haut. <br>')
    }
    if (isFin) {
      if (stor.yaexplik) stor.lesdiv.explications.classList.add('explique')
      barreLesFo()
      disaAll()
      if (stor.exo.quest === 'Ret') {
        j3pAffiche(stor.lesdiv.solution1, null, 'Les coordonnées terrestres de ' + stor.exo.ville.nom + ' sont<br>')
        j3pAffiche(stor.lesdiv.solution1, null, '$' + stor.exo.ville.lat + '°\\text{ }' + stor.exo.ville.ns + '\\text{ }' + stor.exo.ville.long + '°\\text{ }' + stor.exo.ville.eo.replace('W', 'O') + '$')
      } else {
        j3pAffiche(stor.lesdiv.solution1, null, 'Il fallait trouver ' + stor.exo.ville.nom)
      }
      metAuBonEndroit()
    }
    if (stor.yaexplik) stor.lesdiv.explications.classList.add('explique')
  }

  switch (this.etat) {
    case 'enonce':
      if (this.debutDeLaSection) {
        initSection()
      } else {
        enonceMain()
      }
      break // case "enonce":

    case 'correction':
      // On teste si une réponse a été saisie
      stor.yaexplik = false
      stor.yaco = false
      stor.lesdiv.explications.innerHTML = ''
      stor.lesdiv.correction.innerHTML = ''

      if (!yaReponse()) {
        // pas de réponse
        stor.lesdiv.correction.style.color = this.styles.cfaux
        stor.lesdiv.correction.innerHTML = reponseIncomplete
        // on reste dans l’état correction
        this.finCorrection()
        return
      }

      if (isRepOk()) {
        // Bonne réponse
        this.score++
        stor.lesdiv.correction.style.color = this.styles.cbien
        stor.lesdiv.correction.innerHTML = cBien
        this.typederreurs[0]++
        metTruetAll()
        disaAll()
        metAuBonEndroit()
        this.finCorrection('navigation', true)
        return
      }
      // Pas de bonne réponse
      stor.lesdiv.correction.style.color = this.styles.cfaux
      stor.lesdiv.correction.innerHTML = cFaux
      if (this.isElapsed) {
        // A cause de la limite de temps :
        stor.lesdiv.correction.innerHTML += tempsDepasse + '<BR>'
        this.typederreurs[10]++
        affCorrFaux(true)
        stor.yaco = true
        this.finCorrection('navigation', true)
        return
      }

      // Réponse fausse sans pb de temps
      if (me.essaiCourant < ds.nbchances && stor.exo.quest !== 'Pla') {
        stor.lesdiv.correction.innerHTML += '<br>' + essaieEncore
        affCorrFaux(false)
        // affCorrFaux.call(this, false)
        this.typederreurs[1]++
        this.finCorrection()
        return
      }
      // Erreur au dernier essai
      affCorrFaux(true)
      stor.yaco = true
      this.typederreurs[2]++
      // on donne la moitié des points pour une erreur d’arrondi
      if (stor.errArrond) this.score += 0.8
      this.finCorrection('navigation', true)
      break // case "correction":

    case 'navigation':
      if (!this.sectionTerminee()) this.etat = 'enonce'
      this.finNavigation()
      break // case "navigation":
  }
}
