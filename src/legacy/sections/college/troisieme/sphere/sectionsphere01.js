import { j3pAddContent, j3pAddElt, j3pGetNewId, j3pShuffle } from 'src/legacy/core/functions'
import { addDefaultTable } from 'src/legacy/themes/table'
import { getMtgCore } from 'src/lib/outils/mathgraph'
import { j3pCreeSVG } from 'src/legacy/core/functionsSvg'

export const params = {
  outils: ['mathquill'],
  /**
   * Ce qui est paramétrable dans l’éditeur de graphe
   * @type {LegacySectionParamsParametres}
   */
  parametres: [
    ['nbrepetitions', 4, 'entier', 'Nombre de répétitions de la section'],
    ['nbchances', 2, 'entier', 'Nombre d’essais possibles (0  signifie illimité)'],
    ['limite', 0, 'entier', 'Temps disponible par répétition, en secondes (0  signifie illimité)'],
    ['Question', 'Les deux', 'liste', '<u><b>Retrouver</b></u> ou <u><b>Placer</b></u> un point avec coordonnées terrestres.', ['Retrouver', 'Placer', 'Les deux']],
    ['theme', 'standard', 'liste', 'Modèle pour l’affichage', ['standard', 'zonesAvecImageDeFond']]
  ]
}

// @todo Copier le code base64 de la figure construite par src/dev/mappemonde/buildMappemonde.js et l’utiliser

/**
 * section sphere01
 * @this {Parcours}
 */
export default function main () {
  const me = this
  const stor = me.storage
  const ds = me.donneesSection
  const listVille = [
    {
      nom: 'Paris',
      lat: 48,
      ns: 'N',
      long: 2,
      eo: 'E',
      int1: [90, 271],
      int2: [0, 0]
    },
    {
      nom: 'Dallas',
      lat: 32,
      ns: 'N',
      long: 96,
      eo: 'W',
      int1: [186, 361],
      int2: [0, 0]
    },
    {
      nom: 'Mexico',
      lat: 23,
      ns: 'N',
      long: 102,
      eo: 'W',
      int1: [-1, 20],
      int2: [190, 361]
    },
    {
      nom: 'Brasilia',
      lat: 14,
      ns: 'S',
      long: 51,
      eo: 'W',
      int1: [-1, 2],
      int2: [140, 321]
    },
    {
      nom: 'Rio grande',
      lat: 32,
      ns: 'S',
      long: 52,
      eo: 'W',
      int1: [143, 324],
      int2: [361, 361]
    },
    {
      nom: 'New Delhi',
      lat: 28,
      ns: 'N',
      long: 77,
      eo: 'E',
      int1: [13, 194],
      int2: [358, 361]
    },
    {
      nom: 'Prétoria',
      lat: 25,
      ns: 'S',
      long: 28,
      eo: 'E',
      int1: [60, 245],
      int2: [361, 361]
    },
    {
      nom: 'Bamako',
      lat: 12,
      ns: 'N',
      long: 8,
      eo: 'W',
      int1: [95, 281],
      int2: [0, 0]
    },
    {
      nom: 'Bagdad',
      lat: 33,
      ns: 'N',
      long: 44,
      eo: 'E',
      int1: [45, 226],
      int2: [0, 0]
    },
    {
      nom: 'Pékin',
      lat: 39,
      ns: 'N',
      long: 116,
      eo: 'E',
      int1: [-1, 155],
      int2: [333, 361]
    },
    {
      nom: 'Moscou',
      lat: 55,
      ns: 'N',
      long: 37,
      eo: 'E',
      int1: [50, 235],
      int2: [355, 361]
    },
    {
      nom: 'Sidney',
      lat: 32,
      ns: 'S',
      long: 140,
      eo: 'E',
      int1: [-1, 130],
      int2: [305, 361]
    }
  ]

  function newtag () {
    const aret = 'T' + stor.tag.length
    stor.tag.push(aret)
    return aret
  }
  async function initSection () {
    // let callParent = () => undefined
    me.validOnEnter = false // ex donneesSection.touche_entree

    me.construitStructurePage('presentation3')
    me.stockage = [0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0]

    me.afficheTitre('Corrdonnées terrestres')

    stor.exoPos = []
    let lp = []
    if (ds.Question === 'Les deux' || ds.Question === 'Retrouver') lp.push('Ret')
    if (ds.Question === 'Les deux' || ds.Question === 'Placer') lp.push('Pla')
    lp = j3pShuffle(lp)
    for (let i = 0; i < ds.nbitems; i++) {
      stor.exoPos.push({ quest: lp[i % lp.length] })
    }
    stor.exoPos = j3pShuffle(stor.exoPos)

    lp = j3pShuffle(listVille)
    for (let i = 0; i < ds.nbitems; i++) {
      stor.exoPos[i].ville = lp[i % lp.length]
    }
    stor.exoPos = j3pShuffle(stor.exoPos)

    stor.mtgApp = await getMtgCore({ withApiPromise: true })
    enonceMain()
  }
  function modif (tab) {
    for (let i = 0; i < tab.length; i++) {
      for (let j = 0; j < tab[i].length; j++) {
        tab[i][j].style.padding = '0px'
      }
    }
  }
  function creeLesDiv () {
    stor.lesdiv = {}
    stor.lesdiv.conteneur = j3pAddElt(me.zonesElts.MG, 'div', {
      style: me.styles.etendre('toutpetit.enonce', { padding: '10px' })
    })
    const cont1 = addDefaultTable(stor.lesdiv.conteneur, 1, 3)
    modif(cont1)
    stor.lesdiv.consigne = cont1[0][0]
    const contx = addDefaultTable(cont1[0][2], 2, 1)
    stor.lesdiv.calculatrice = contx[0][0]
    stor.lesdiv.correction = contx[1][0]
    const cont2 = addDefaultTable(stor.lesdiv.conteneur, 1, 3)
    modif(cont2)
    j3pAddContent(cont1[0][1], '&nbsp;&nbsp;&nbsp;')
    const conty = addDefaultTable(cont2[0][0], 2, 1)
    modif(conty)
    stor.lesdiv.figure = conty[0][0]
    cont2[0][2].style.verticalAlign = 'top'
    const cont3 = addDefaultTable(cont2[0][2], 2, 1)
    modif(cont3)
    stor.lesdiv.travail = cont3[0][0]
    stor.lesdiv.solution1 = cont3[1][0]
    stor.lesdiv.explications = j3pAddElt(stor.lesdiv.conteneur, 'div')
    stor.lesdiv.travail.classList.add('travail')
    stor.lesdiv.consigne.classList.add('enonce')
    me.zonesElts.MG.classList.add('fond')
    stor.lesdiv.solution1.style.color = me.styles.cbien
    stor.lesdiv.explications.style.color = me.styles.cfaux
    stor.lesdiv.figure.style.verticalAlign = 'top'
    stor.lesdiv.travail.style.paddingLeft = '5px'
  }
  function enonceMain () {
    stor.tag = []
    me.videLesZones()
    creeLesDiv()
    /*
    stor.exo = stor.exoPos.pop()
    const quest = (stor.exo.quest === 'Ret') ? ('<b>Retrouve les coordonnées terrestres de ' + stor.exo.ville.nom + '</b>') : ('<b>Quelle ville a pour coordonnées terrestres</b>  $' + stor.exo.ville.lat + '°\\text{ }' + stor.exo.ville.ns + '\\text{ }' + stor.exo.ville.long + '°\\text{ }' + stor.exo.ville.eo + '$ <b>?</b>')
    j3pAffiche(stor.lesdiv.consigne, null, quest)
     */
    stor.svgId = j3pGetNewId()
    stor.mtgApp.removeAllDoc()
    stor.acache = []
    const wi = 400
    const lar = 400
    const marg = 50

    j3pCreeSVG(stor.lesdiv.figure, { id: stor.svgId, width: wi, height: lar, style: { border: 'solid black 1px' } })
    stor.mtgApp.addDoc(stor.svgId, 'TWF0aEdyYXBoSmF2YTEuMAAAABM+TMzNAAJmcv###wEA#wEAAAAAAAAAAANLAAACfwAAAQEAAAAAAAAAAQAAAAH#####AAAAAQAKQ0NhbGNDb25zdAD#####AAJwaQAWMy4xNDE1OTI2NTM1ODk3OTMyMzg0Nv####8AAAABAApDQ29uc3RhbnRlQAkh+1RELRj###############8=', true)
    stor.mtgApp.setApiDoc(stor.svgId)
    stor.mtgApp.calculateFirstTime(stor.svgId)

    // curseur
    stor.mtgApp.addFreePoint({
      absCoord: true,
      x: marg,
      y: 15,
      name: 'C1',
      tag: 'C1',
      hidden: true,
      pointStyle: 'bigmult'
    })
    stor.mtgApp.fixPoint({ a: 'C1' })
    stor.mtgApp.addFreePoint({
      absCoord: true,
      x: wi - marg,
      y: 15,
      name: 'C2',
      tag: 'C2',
      hidden: true,
      pointStyle: 'bigmult'
    })
    stor.mtgApp.fixPoint({ a: 'C2' })
    stor.mtgApp.setUnity({ a: 'C1', b: 'C2' })
    stor.mtgApp.addSegment({
      b: 'C1',
      a: 'C2',
      tag: 'segcurs',
      lineStyle: 'dashdash'
    })
    stor.mtgApp.addLinkedPointLine({
      absCoord: true,
      d: 'segcurs',
      x: wi / 2 + 20,
      y: lar / 2,
      name: 'potCurs',
      tag: 'potCurs',
      color: 'white',
      hiddenName: true
    })
    stor.mtgApp.addCircleOr({
      o: 'potCurs',
      r: 0.035,
      tag: 'peticurs',
      thickness: 3
    })
    stor.mtgApp.addSurfaceCircle({
      c: 'peticurs',
      color: 'black'
    })
    stor.mtgApp.addLengthMeasure({
      a: 'C1',
      b: 'potCurs'
    })
    stor.mtgApp.addLengthMeasure({
      a: 'C1',
      b: 'C2'
    })
    stor.mtgApp.addCalc({
      nameCalc: 'valCurseurb',
      formula: 'C1potCurs/C1C2*360'
    })
    stor.mtgApp.addCalc({
      nameCalc: 'valCurseur',
      formula: 'si(valCurseurb=0,0.01,si(valCurseurb=-180,-179.9,si(valCurseurb=180,179.9,valCurseurb)))'
    })

    // potCurs C1potCurs  et C1C2
    stor.mtgApp.addFreePoint({
      absCoord: true,
      x: 100,
      y: 1,
      name: 'PPP1',
      tag: 'PPP1',
      hidden: true,
      pointStyle: 'bigmult'
    })
    stor.mtgApp.addFreePoint({
      absCoord: true,
      x: 200,
      y: 1,
      name: 'PPP2',
      tag: 'PPP2',
      hidden: true,
      pointStyle: 'bigmult'
    })
    stor.mtgApp.addSegment({
      b: 'PPP1',
      a: 'PPP2',
      tag: 'PPPsegcurs',
      lineStyle: 'dashdash',
      hidden: true
    })
    stor.mtgApp.addLinkedPointLine({
      absCoord: true,
      d: 'PPPsegcurs',
      x: 150,
      y: 1,
      name: 'ppppp',
      tag: 'ppppp',
      color: 'white',
      hiddenName: true,
      hidden: true
    })
    stor.mtgApp.addLengthMeasure({
      a: 'PPP1',
      b: 'ppppp'
    })
    stor.mtgApp.addLengthMeasure({
      a: 'PPP1',
      b: 'PPP2'
    })

    // curseur lattitude
    stor.mtgApp.addFreePoint({
      absCoord: true,
      x: 2 * marg,
      y: 380,
      name: 'L1',
      tag: 'L1',
      hidden: true,
      pointStyle: 'bigmult'
    })
    stor.mtgApp.fixPoint({ a: 'L1' })
    stor.mtgApp.addFreePoint({
      absCoord: true,
      x: wi - marg,
      y: 380,
      name: 'L2',
      tag: 'L2',
      hidden: true,
      pointStyle: 'bigmult'
    })
    stor.mtgApp.fixPoint({ a: 'L2' })
    stor.mtgApp.addSegment({
      b: 'L1',
      a: 'L2',
      tag: 'segcursL',
      lineStyle: 'dashdash',
      color: 'blue',
      thickness: 2
    })
    stor.mtgApp.addLinkedPointLine({
      absCoord: true,
      d: 'segcursL',
      x: 300,
      y: lar / 2,
      name: 'potCursL',
      tag: 'potCursL',
      color: 'white',
      hiddenName: true
    })
    stor.mtgApp.addCircleOr({
      o: 'potCursL',
      r: 0.035,
      tag: 'peticursL',
      thickness: 3
    })
    stor.mtgApp.addSurfaceCircle({
      c: 'peticursL',
      color: 'blue',
      fillStyle: 'fill',
      tag: 'surface987'
    })
    stor.mtgApp.addLengthMeasure({
      a: 'L1',
      b: 'potCursL'
    })
    stor.mtgApp.addLengthMeasure({
      a: 'L1',
      b: 'L2'
    })
    stor.mtgApp.addCalc({
      nameCalc: 'latiP',
      formula: '(L1potCursL/L1L2)*180-90'
    })
    stor.mtgApp.addCalc({
      nameCalc: 'latiP2',
      formula: 'int(abs(latiP))'
    })

    // curseur longitude
    stor.mtgApp.addFreePoint({
      absCoord: true,
      x: 2 * marg,
      y: 350,
      name: 'Lo1',
      tag: 'Lo1',
      hidden: true,
      pointStyle: 'bigmult'
    })
    stor.mtgApp.fixPoint({ a: 'Lo1' })
    stor.mtgApp.addFreePoint({
      absCoord: true,
      x: wi - marg,
      y: 350,
      name: 'Lo2',
      tag: 'Lo2',
      hidden: true,
      pointStyle: 'bigmult'
    })
    stor.mtgApp.fixPoint({ a: 'Lo2' })
    stor.mtgApp.addSegment({
      b: 'Lo1',
      a: 'Lo2',
      tag: 'segcursLo',
      lineStyle: 'dashdash',
      color: 'green',
      thickness: 2
    })
    stor.mtgApp.addLinkedPointLine({
      absCoord: true,
      d: 'segcursLo',
      x: 200,
      y: lar / 2,
      name: 'potCursLo',
      tag: 'potCursLo',
      color: 'white',
      hiddenName: true
    })
    stor.mtgApp.addCircleOr({
      o: 'potCursLo',
      r: 0.035,
      tag: 'peticursLo',
      thickness: 3
    })
    stor.mtgApp.addSurfaceCircle({
      c: 'peticursLo',
      color: 'green',
      fillStyle: 'fill',
      tag: 'surfacgfde987'
    })
    stor.mtgApp.addLengthMeasure({
      a: 'Lo1',
      b: 'potCursLo'
    })
    stor.mtgApp.addLengthMeasure({
      a: 'Lo1',
      b: 'Lo2'
    })
    stor.mtgApp.addCalc({
      nameCalc: 'longiP',
      formula: '-((Lo1potCursLo/Lo1Lo2)*360-180)'
    })
    stor.mtgApp.addCalc({
      nameCalc: 'longiP2',
      formula: 'int(abs(longiP))'
    })

    // sphere
    stor.mtgApp.addFreePoint({
      absCoord: true,
      x: 700,
      y: 700,
      name: 'R',
      tag: 'R',
      hidden: false,
      pointStyle: 'bigmult'
    })
    stor.mtgApp.addFreePoint({
      absCoord: true,
      x: wi / 2,
      y: (lar - marg) / 2,
      name: 'O',
      tag: 'O',
      hidden: false,
      pointStyle: 'bigmult'
    })
    stor.mtgApp.addFreePoint({
      absCoord: true,
      x: wi / 2,
      y: (lar - marg) / 2,
      name: 'O2',
      tag: 'O2',
      hidden: false,
      pointStyle: 'bigmult'
    })
    stor.mtgApp.addFreePoint({
      absCoord: true,
      x: wi / 2,
      y: marg,
      name: 'N',
      tag: 'N',
      pointStyle: 'bigmult',
      hiddenName: true
    })
    stor.mtgApp.fixPoint({ a: 'N' })
    stor.mtgApp.addFreePoint({
      absCoord: true,
      x: wi / 2,
      y: 300,
      name: 'S',
      tag: 'S',
      pointStyle: 'bigmult',
      hiddenName: true
    })
    stor.mtgApp.fixPoint({ a: 'S' })
    stor.mtgApp.addCircleOA({
      o: 'O',
      a: 'N',
      tag: 'lecircle'
    })
    stor.mtgApp.addSurfaceCircle({
      c: 'lecircle',
      color: '#6683ee',
      tag: 'lacircleS',
      fillStyle: 'fill'
    })
    stor.acache.push('lecircleS')
    stor.mtgApp.addLengthMeasure({
      a: 'O',
      b: 'N'
    })
    // repere
    stor.mtgApp.addFreePoint({
      absCoord: true,
      x: wi - marg * 1.5,
      y: (lar - marg) / 2,
      name: 'I',
      tag: 'I',
      hidden: true,
      pointStyle: 'bigmult'
    })
    stor.mtgApp.addFreePoint({
      absCoord: true,
      x: marg * 1.5,
      y: (lar - marg) / 2,
      name: 'I7',
      tag: 'I7',
      hidden: true,
      pointStyle: 'bigmult'
    })
    stor.mtgApp.addLengthMeasure({
      a: 'O',
      b: 'I'
    })
    stor.mtgApp.addLengthMeasure({
      a: 'O',
      b: 'N'
    })
    stor.mtgApp.addSegment({
      b: 'O',
      a: 'I',
      tag: 'axeab1',
      color: 'red',
      hidden: true
    })
    stor.mtgApp.addSegment({
      b: 'O',
      a: 'N',
      tag: 'axeord1',
      color: 'red',
      hidden: true
    })
    // repoer o i n
    stor.mtgApp.addSystemOfAxis({
      o: 'O',
      a: 'I',
      b: 'N',
      hidden: false,
      tag: 'Repere2'
    })

    // place I' et J'
    /*
    stor.mtgApp.addCalc({
      nameCalc: 'abI',
      formula: 'cos(valCurseur)'
    })
    stor.mtgApp.addCalc({
      nameCalc: 'ordI',
      formula: 'sin(valCurseur)*0.2'
    })
    stor.mtgApp.addImPointDilation({
      o: 'O',
      a: 'I',
      x: 'abI',
      tag: 'ibuf',
      name: 'ibuf',
      hidden: true
    })
    stor.mtgApp.addLinePar({
      d: 'axeord1',
      a: 'ibuf',
      tag: 'parIab',
      hidden: true,
      color: 'green'
    })
    stor.mtgApp.addImPointDilation({
      o: 'O',
      a: 'N',
      x: 'ordI',
      tag: 'ibuf2',
      name: 'ibuf2',
      hidden: true
    })
    stor.mtgApp.addLinePar({
      d: 'axeab1',
      a: 'ibuf2',
      tag: 'parIord',
      hidden: true,
      color: 'green'
    })
    stor.mtgApp.addIntLineLine({
      d: 'parIord',
      d2: 'parIab',
      name: 'J1',
      tag: 'J1',
      hidden: true
    })
    */
    stor.mtgApp.addPointXY({
      x: 'cos(valCurseur)',
      y: 'sin(valCurseur)*0.2',
      rep: 'Repere2',
      tag: 'J1',
      name: 'J1',
      hidden: true
    })
    /*
    stor.mtgApp.addCalc({
      nameCalc: 'abJ',
      formula: 'sin(valCurseur)'
    })
    stor.mtgApp.addCalc({
      nameCalc: 'ordJ',
      formula: '-cos(valCurseur)*0.2'
    })
    stor.mtgApp.addImPointDilation({
      o: 'O',
      a: 'I',
      x: 'abJ',
      tag: 'jbuf',
      name: 'jbuf',
      hidden: true
    })
    stor.mtgApp.addLinePar({
      d: 'axeord1',
      a: 'jbuf',
      tag: 'parJab',
      hidden: true,
      color: 'green'
    })
    stor.mtgApp.addImPointDilation({
      o: 'O',
      a: 'N',
      x: 'ordJ',
      tag: 'jbuf2',
      name: 'jbuf2',
      hidden: true
    })
    stor.mtgApp.addLinePar({
      d: 'axeab1',
      a: 'jbuf2',
      tag: 'parJord',
      hidden: true,
      color: 'green'
    })
    stor.mtgApp.addIntLineLine({
      d: 'parJord',
      d2: 'parJab',
      name: 'I1',
      tag: 'I1',
      hidden: true
    })
    */
    stor.mtgApp.addPointXY({
      x: 'sin(valCurseur)',
      y: '-cos(valCurseur)*0.2',
      rep: 'Repere2',
      tag: 'I1',
      name: 'I1',
      hidden: true
    })
    stor.mtgApp.addPointXY({
      x: '-sin(valCurseur)',
      y: '+cos(valCurseur)*0.2',
      rep: 'Repere2',
      tag: 'I2',
      name: 'I2',
      hidden: true
    })

    for (let i = -90; i < 91; i++) {
      stor.mtgApp.addPointXY({
        x: Math.cos(i * Math.PI / 180),
        y: Math.sin(i * Math.PI / 180),
        rep: 'Repere2',
        tag: 'vers' + i + '1',
        name: 'vers' + i + '1',
        hidden: true
      })
      stor.mtgApp.addPointXY({
        x: -Math.cos(i * Math.PI / 180),
        y: Math.sin(i * Math.PI / 180),
        rep: 'Repere2',
        tag: 'vers' + i + '2',
        name: 'vers' + i + '2',
        hidden: true
      })
    }

    // textes
    stor.mtgApp.addText({
      absCoord: true,
      text: 'Pôle Nord',
      x: 170,
      y: 27
    })
    stor.mtgApp.addText({
      absCoord: true,
      text: 'Pôle Sud',
      x: 170,
      y: 310
    })
    stor.mtgApp.addText({
      absCoord: true,
      text: 'Latitude:',
      x: 20,
      y: 370,
      color: 'blue',
      tag: 'LatitudeText'
    })
    stor.mtgApp.addText({
      absCoord: true,
      text: 'Longitude:',
      x: 20,
      y: 340,
      color: 'green',
      tag: 'LongitudeText'
    })

    // repoer o i1 J1
    stor.mtgApp.addSystemOfAxis({
      o: 'O',
      a: 'I1',
      b: 'J1',
      hidden: false,
      tag: 'Repere1'
    })

    stor.mtgApp.addPointXY({
      x: '-cos(longiP)',
      y: 'sin(longiP)',
      rep: 'Repere1',
      tag: 'tagLo',
      name: 'tagLo',
      hidden: true,
      hiddenName: true,
      color: 'red'
    })
    const tagRep = newtag()
    stor.mtgApp.addSystemOfAxis({
      o: 'O',
      a: 'tagLo',
      b: 'N',
      hidden: false,
      tag: tagRep
    })
    const tagIII = newtag()
    stor.mtgApp.addPointXY({
      x: 'cos(latiP)',
      y: 'sin(latiP)',
      rep: tagRep,
      tag: tagIII,
      name: tagIII,
      hidden: true,
      hiddenName: true,
      color: 'blue'
    })

    stor.mtgApp.addCalc({ nameCalc: 'pourV', formula: '(longiP+180-valCurseur)' })
    stor.mtgApp.addCalc({ nameCalc: 'pourV2', formula: 'si(pourV<-180,pourV+360,si(pourV>180,pourV-360,pourV))' })
    const ff = 'si((pourV2)<-90,1000,si((pourV2)>90,1000,1))'

    const tagRet = newtag()
    stor.mtgApp.addImPointDilation({
      o: 'O',
      a: tagIII,
      x: ff,
      tag: tagRet,
      name: tagRet,
      hidden: true,
      color: 'black',
      hiddenName: true,
      pointStyle: 'biground'
    })

    stor.mtgApp.addCalc({ nameCalc: 'miLn', formula: 'min(longiP,0)' })
    stor.mtgApp.addCalc({ nameCalc: 'maLn', formula: 'max(longiP,0)' })
    stor.mtgApp.addCalc({ nameCalc: 'dx', formula: 'miLn+(L1potCursL/L1L2)*(maLn-miLn)' })
    stor.mtgApp.addCalc({ nameCalc: 'd1', formula: '-cos(dx)' })
    stor.mtgApp.addCalc({ nameCalc: 'd2', formula: '+sin(dx)' })

    stor.mtgApp.addCalc({ nameCalc: 'miLn2', formula: 'min(latiP,0)' })
    stor.mtgApp.addCalc({ nameCalc: 'maLn2', formula: 'max(latiP,0)' })
    stor.mtgApp.addCalc({ nameCalc: 'dx2', formula: 'miLn2+(PPP1ppppp/PPP1PPP2)*(maLn2-miLn2)' })
    stor.mtgApp.addCalc({ nameCalc: 'd12', formula: 'cos(dx2)' })
    stor.mtgApp.addCalc({ nameCalc: 'd22', formula: 'sin(dx2)' })

    stor.mtgApp.addSegment({
      b: 'O',
      a: 'I2',
      tag: 'seg11',
      color: 'black',
      hidden: false
    })
    stor.mtgApp.addSegment({
      b: 'O',
      a: 'tagLo',
      tag: 'seg22',
      color: 'black',
      hidden: false
    })
    stor.mtgApp.addSegment({
      b: 'O',
      a: tagIII,
      tag: 'seg33',
      color: 'black',
      hidden: false
    })
    stor.mtgApp.addPointXY({
      x: 'd1',
      y: 'd2',
      rep: 'Repere1',
      tag: 'pLomp',
      name: 'pLomp',
      hidden: true,
      hiddenName: true,
      color: 'black',
      pointStyle: 'biground'
    })
    stor.mtgApp.addPointLocus({
      a: 'potCursL',
      b: 'pLomp',
      x: 200,
      color: 'black',
      thickness: 1,
      tag: 'jdslkjflskq'
    })

    stor.mtgApp.addPointXY({
      x: 'd12',
      y: 'd22',
      rep: tagRep,
      tag: 'pLomp2',
      name: 'pLomp2',
      hidden: true,
      hiddenName: true,
      color: 'black',
      pointStyle: 'biground'
    })
    stor.mtgApp.addPointLocus({
      a: 'ppppp',
      b: 'pLomp2',
      x: 200,
      color: 'black',
      thickness: 1,
      tag: 'jdslkjhgfhflskq'
    })

    dessineAfrique()
    dessineAmeriqueN()
    dessineAmeriqueS()
    dessineEurasie()
    dessineOceane()
    ajEquateur()
    ajGreenMer()
    ajPara()
    ajMeridien()

    for (let i = 0; i < listVille.length; i++) {
      const condi = 'si(valCurseurb>' + listVille[i].int1[0] + ',si(valCurseurb<' + listVille[i].int1[1] + ',1,si(valCurseurb>' + listVille[i].int2[0] + ',si(valCurseurb<' + listVille[i].int2[1] + ',1,0),0)),0)'
      const jj = ajPoint(listVille[i].lat, listVille[i].ns, listVille[i].long, listVille[i].eo, 'black', 'biground', false, condi)
      stor.mtgApp.addLinkedText({
        text: listVille[i].nom,
        a: jj,
        offsetX: 16,
        offsetY: -10,
        color: 'black',
        fontSize: 15
      })
    }
    // pour cerif
    stor.mtgApp.addLinkedText({
      text: '#G#Val(latiP2)°',
      a: tagRet,
      offsetX: 16,
      offsetY: -20,
      color: 'blue'
    })

    stor.mtgApp.addLinkedText({
      text: '#G#Val(valCurseurb)°',
      a: 'O2',
      offsetX: 16,
      offsetY: -20,
      color: 'black'
    })

    stor.lesdiv.figure.classList.add((stor.exo === 'Ret') ? 'enonce' : 'travail')
    const yy = stor.mtgApp.getBase64Code()
    console.error(yy)
    me.finEnonce()
  }
  function dessineAfrique () {
    const i = stor.acache.length
    stor.mtgApp.addCalc({ nameCalc: 'kklmop', formula: 'si(valCurseurb>280,0,si(valCurseurb<20,0,1))' })
    stor.acache = stor.acache.concat(ajSegTerre(30, 'S', 25, 'E', 20, 'S', 33, 'E', '#523703', true, 'kklmop'))
    stor.acache = stor.acache.concat(ajSegTerre(20, 'S', 33, 'E', 7, 'S', 38, 'E', '#523703', true, 'kklmop'))
    stor.acache = stor.acache.concat(ajSegTerre(7, 'S', 38, 'E', 1, 'N', 41, 'E', '#523703', true, 'kklmop'))
    stor.acache = stor.acache.concat(ajSegTerre(1, 'N', 41, 'E', 8, 'N', 46, 'E', '#523703', true, 'kklmop'))
    stor.acache = stor.acache.concat(ajSegTerre(8, 'N', 46, 'E', 18, 'N', 34, 'E', '#523703', true, 'kklmop'))
    stor.acache = stor.acache.concat(ajSegTerre(18, 'N', 34, 'E', 27, 'N', 27, 'E', '#523703', true, 'kklmop'))
    stor.acache = stor.acache.concat(ajSegTerre(27, 'N', 27, 'E', 31, 'N', 13, 'E', '#523703', true, 'kklmop'))
    stor.acache = stor.acache.concat(ajSegTerre(31, 'N', 13, 'E', 35, 'N', 2, 'E', '#523703', true, 'kklmop'))
    stor.acache = stor.acache.concat(ajSegTerre(35, 'N', 2, 'E', 26, 'N', 11, 'W', '#523703', true, 'kklmop'))
    stor.acache = stor.acache.concat(ajSegTerre(26, 'N', 11, 'W', 6, 'N', 8, 'W', '#523703', true, 'kklmop'))
    stor.acache = stor.acache.concat(ajSegTerre(6, 'N', 8, 'W', 7, 'N', 6, 'E', '#523703', true, 'kklmop'))
    stor.acache = stor.acache.concat(ajSegTerre(7, 'N', 6, 'E', 5, 'S', 13, 'E', '#523703', true, 'kklmop'))
    stor.acache = stor.acache.concat(ajSegTerre(5, 'S', 13, 'E', 18, 'S', 14, 'E', '#523703', true, 'kklmop'))
    stor.acache = stor.acache.concat(ajSegTerre(18, 'S', 14, 'E', 25, 'S', 17, 'E', '#523703', true, 'kklmop'))
    stor.acache = stor.acache.concat(ajSegTerre(25, 'S', 17, 'E', 30, 'S', 25, 'E', '#523703', true, 'kklmop'))
    const p = []
    for (let j = i; j < stor.acache.length; j++) {
      p.push(stor.acache[j].tag)
    }
    stor.mtgApp.addPolygon({
      points: p,
      tag: 'polaf',
      hidden: true
    })
    stor.mtgApp.addSurfacePoly({
      poly: 'polaf',
      color: '#9b6220',
      hidden: false,
      fillStyle: 'fill',
      tag: 'surfAfri'
    })
  }
  function dessineAmeriqueN () {
    let i = stor.acache.length
    stor.mtgApp.addCalc({ nameCalc: 'Zkklmop', formula: 'si(valCurseurb>160,1,0)' })
    stor.acache = stor.acache.concat(ajSegTerre(79, 'N', 90, 'W', 65, 'N', 69, 'W', '#523703', true, 'Zkklmop'))
    stor.acache = stor.acache.concat(ajSegTerre(65, 'N', 69, 'W', 47, 'N', 53, 'W', '#523703', true, 'Zkklmop'))
    stor.acache = stor.acache.concat(ajSegTerre(47, 'N', 53, 'W', 42, 'N', 74, 'W', '#523703', true, 'Zkklmop'))
    stor.acache = stor.acache.concat(ajSegTerre(42, 'N', 74, 'W', 31, 'N', 82, 'W', '#523703', true, 'Zkklmop'))
    stor.acache = stor.acache.concat(ajSegTerre(31, 'N', 82, 'W', 22, 'N', 79, 'W', '#000000', true, 'Zkklmop'))
    stor.acache = stor.acache.concat(ajSegTerre(22, 'N', 79, 'W', 29, 'N', 82, 'W', '#000000', true, 'Zkklmop'))
    stor.acache = stor.acache.concat(ajSegTerre(29, 'N', 82, 'W', 28, 'N', 96, 'W', '#523703', true, 'Zkklmop'))
    stor.acache = stor.acache.concat(ajSegTerre(28, 'N', 96, 'W', 17, 'N', 96, 'W', '#523703', true, 'Zkklmop'))
    stor.acache = stor.acache.concat(ajSegTerre(17, 'N', 96, 'W', 79, 'N', 90, 'W', '#523703', true, 'Zkklmop'))
    let p = []
    for (let j = i; j < stor.acache.length; j++) {
      p.push(stor.acache[j].tag)
    }
    stor.mtgApp.addPolygon({
      points: p,
      tag: 'polafee',
      hidden: true
    })
    stor.mtgApp.addSurfacePoly({
      poly: 'polafee',
      color: '#9b6220',
      fillStyle: 'fill',
      tag: 'surfAm1'
    })

    stor.mtgApp.addCalc({ nameCalc: 'fdsfsdkljfidos', formula: 'si(valCurseurb<160,si(valCurseurb<60,1,0),1)' })
    i = stor.acache.length
    stor.acache = stor.acache.concat(ajSegTerre(17, 'N', 96, 'W', 25, 'N', 110, 'W', '#523703', true, 'fdsfsdkljfidos'))
    stor.acache = stor.acache.concat(ajSegTerre(25, 'N', 110, 'W', 39, 'N', 123, 'W', '#523703', true, 'fdsfsdkljfidos'))
    stor.acache = stor.acache.concat(ajSegTerre(39, 'N', 123, 'W', 49, 'N', 124, 'W', '#523703', true, 'fdsfsdkljfidos'))
    stor.acache = stor.acache.concat(ajSegTerre(49, 'N', 124, 'W', 59, 'N', 160, 'W', '#523703', true, 'fdsfsdkljfidos'))
    stor.acache = stor.acache.concat(ajSegTerre(59, 'N', 160, 'W', 69, 'N', 160, 'W', '#523703', true, 'fdsfsdkljfidos'))
    stor.acache = stor.acache.concat(ajSegTerre(69, 'N', 160, 'W', 69, 'N', 134, 'W', '#523703', true, 'fdsfsdkljfidos'))
    stor.acache = stor.acache.concat(ajSegTerre(69, 'N', 134, 'W', 80, 'N', 88, 'W', '#523703', true, 'fdsfsdkljfidos'))
    stor.acache = stor.acache.concat(ajSegTerre(80, 'N', 88, 'W', 65, 'N', 69, 'W', '#523703', true, 'fdsfsdkljfidos'))
    stor.acache = stor.acache.concat(ajSegTerre(65, 'N', 69, 'W', 17, 'N', 96, 'W', '#523703', true, 'fdsfsdkljfidos'))
    p = []
    for (let j = i; j < stor.acache.length; j++) {
      p.push(stor.acache[j].tag)
    }
    stor.mtgApp.addPolygon({
      points: p,
      tag: 'gfgfdgfdsgfd',
      hidden: true
    })
    stor.mtgApp.addSurfacePoly({
      poly: 'gfgfdgfdsgfd',
      color: '#9b6220',
      fillStyle: 'fill',
      tag: 'surfAm15'
    })
  }
  function dessineOceane () {
    const i = stor.acache.length
    stor.mtgApp.addCalc({ nameCalc: 'Zkklmottp', formula: 'si(valCurseurb>150,si(valCurseurb<310,0,1),1)' })
    stor.acache = stor.acache.concat(ajSegTerre(35, 'S', 117, 'E', 31, 'S', 133, 'E', '#523703', true, 'Zkklmottp'))
    stor.acache = stor.acache.concat(ajSegTerre(31, 'S', 133, 'E', 43, 'S', 146, 'E', '#523703', true, 'Zkklmottp'))
    stor.acache = stor.acache.concat(ajSegTerre(43, 'S', 146, 'E', 28, 'S', 152, 'E', '#523703', true, 'Zkklmottp'))
    stor.acache = stor.acache.concat(ajSegTerre(28, 'S', 152, 'E', 11, 'S', 142, 'E', '#523703', true, 'Zkklmottp'))
    stor.acache = stor.acache.concat(ajSegTerre(11, 'S', 142, 'E', 18, 'S', 140, 'E', '#523703', true, 'Zkklmottp'))
    stor.acache = stor.acache.concat(ajSegTerre(18, 'S', 140, 'E', 13, 'S', 133, 'E', '#523703', true, 'Zkklmottp'))
    stor.acache = stor.acache.concat(ajSegTerre(13, 'S', 133, 'E', 22, 'S', 114, 'E', '#523703', true, 'Zkklmottp'))
    stor.acache = stor.acache.concat(ajSegTerre(22, 'S', 114, 'E', 35, 'S', 117, 'E', '#523703', true, 'Zkklmottp'))
    const p = []
    for (let j = i; j < stor.acache.length; j++) {
      p.push(stor.acache[j].tag)
    }
    stor.mtgApp.addPolygon({
      points: p,
      tag: 'poladfsffee',
      hidden: true
    })
    stor.mtgApp.addSurfacePoly({
      poly: 'poladfsffee',
      color: '#9b6220',
      fillStyle: 'fill',
      tag: 'surfAm2'
    })
  }
  function dessineAmeriqueS () {
    const i = stor.acache.length
    stor.mtgApp.addCalc({ nameCalc: 'Zkklrmop', formula: 'si(valCurseurb<140,0,1)' })
    stor.acache = stor.acache.concat(ajSegTerre(17, 'N', 96, 'W', 22, 'N', 87, 'W', '#523703', true, 'Zkklrmop'))
    stor.acache = stor.acache.concat(ajSegTerre(22, 'N', 87, 'W', 10, 'N', 84, 'W', '#523703', true, 'Zkklrmop'))
    stor.acache = stor.acache.concat(ajSegTerre(10, 'N', 84, 'W', 17, 'N', 96, 'W', '#523703', true, 'Zkklrmop'))
    stor.acache = stor.acache.concat(ajSegTerre(10, 'N', 84, 'W', 6, 'N', 76, 'W', '#523703', true, 'Zkklrmop'))
    stor.acache = stor.acache.concat(ajSegTerre(6, 'N', 76, 'W', 5, 'S', 79, 'W', '#523703', true, 'Zkklrmop'))
    stor.acache = stor.acache.concat(ajSegTerre(5, 'S', 79, 'W', 18, 'S', 69, 'W', '#523703', true, 'Zkklrmop'))
    stor.acache = stor.acache.concat(ajSegTerre(18, 'S', 69, 'W', 51, 'S', 72, 'W', '#523703', true, 'Zkklrmop'))
    stor.acache = stor.acache.concat(ajSegTerre(51, 'S', 72, 'W', 20, 'S', 42, 'W', '#523703', true, 'Zkklrmop'))
    stor.acache = stor.acache.concat(ajSegTerre(20, 'S', 42, 'W', 5, 'S', 36, 'W', '#523703', true, 'Zkklrmop'))
    stor.acache = stor.acache.concat(ajSegTerre(5, 'S', 36, 'W', 10, 'N', 62, 'W', '#523703', true, 'Zkklrmop'))
    stor.acache = stor.acache.concat(ajSegTerre(10, 'N', 62, 'W', 6, 'N', 76, 'W', '#523703', true, 'Zkklrmop'))
    const p = []
    for (let j = i; j < stor.acache.length; j++) {
      p.push(stor.acache[j].tag)
    }
    stor.mtgApp.addPolygon({
      points: p,
      tag: 'ttpolafee',
      hidden: true
    })
    stor.mtgApp.addSurfacePoly({
      poly: 'ttpolafee',
      color: '#9b6220',
      fillStyle: 'fill',
      tag: 'surfAm3'
    })
  }
  function dessineEurasie () {
    let i, p

    i = stor.acache.length
    stor.mtgApp.addCalc({ nameCalc: 'Zkklrggmop', formula: 'si(valCurseurb>182,si(valCurseurb>284,1,0),1)' })

    stor.acache = stor.acache.concat(ajSegTerre(19, 'N', 105, 'E', 25, 'N', 116, 'E', '#523703', true, 'Zkklrggmop'))
    stor.acache = stor.acache.concat(ajSegTerre(25, 'N', 116, 'E', 31, 'N', 121, 'E', '#523703', true, 'Zkklrggmop'))
    stor.acache = stor.acache.concat(ajSegTerre(31, 'N', 121, 'E', 40, 'N', 119, 'E', '#523703', true, 'Zkklrggmop'))
    stor.acache = stor.acache.concat(ajSegTerre(40, 'N', 119, 'E', 39, 'N', 105, 'E', '#523703', true, 'Zkklrggmop'))
    stor.acache = stor.acache.concat(ajSegTerre(39, 'N', 105, 'E', 19, 'N', 105, 'E', '#523703', true, 'Zkklrggmop'))
    p = []
    for (let j = i; j < stor.acache.length; j++) {
      p.push(stor.acache[j].tag)
    }
    stor.mtgApp.addPolygon({
      points: p,
      tag: 'ttpffolafee',
      hidden: true
    })
    stor.mtgApp.addSurfacePoly({
      poly: 'ttpffolafee',
      color: '#9b6220',
      fillStyle: 'fill',
      tag: 'surfAm4'
    })
    i = stor.acache.length
    stor.acache = stor.acache.concat(ajSegTerre(40, 'N', 119, 'E', 37, 'N', 127, 'E', '#523703', true, 'Zkklrggmop'))
    stor.acache = stor.acache.concat(ajSegTerre(37, 'N', 127, 'E', 42, 'N', 127, 'E', '#523703', true, 'Zkklrggmop'))
    stor.acache = stor.acache.concat(ajSegTerre(42, 'N', 127, 'E', 45, 'N', 135, 'E', '#523703', true, 'Zkklrggmop'))
    stor.acache = stor.acache.concat(ajSegTerre(45, 'N', 135, 'E', 72, 'N', 150, 'E', '#523703', true, 'Zkklrggmop'))
    stor.acache = stor.acache.concat(ajSegTerre(72, 'N', 150, 'E', 76, 'N', 102, 'E', '#523703', true, 'Zkklrggmop'))
    stor.acache = stor.acache.concat(ajSegTerre(76, 'N', 102, 'E', 39, 'N', 105, 'E', '#523703', true, 'Zkklrggmop'))
    stor.acache = stor.acache.concat(ajSegTerre(39, 'N', 105, 'E', 40, 'N', 119, 'E', '#523703', true, 'Zkklrggmop'))
    p = []
    for (let j = i; j < stor.acache.length; j++) {
      p.push(stor.acache[j].tag)
    }
    stor.mtgApp.addPolygon({
      points: p,
      tag: 'ttpffola3fee',
      hidden: true
    })
    stor.mtgApp.addSurfacePoly({
      poly: 'ttpffola3fee',
      color: '#9b6220',
      fillStyle: 'fill',
      tag: 'surfAm5'
    })
    i = stor.acache.length
    stor.acache = stor.acache.concat(ajSegTerre(55, 'N', 133, 'E', 61, 'N', 148, 'E', '#523703', true, 'Zkklrggmop'))
    stor.acache = stor.acache.concat(ajSegTerre(61, 'N', 148, 'E', 63, 'N', 176, 'E', '#523703', true, 'Zkklrggmop'))
    stor.acache = stor.acache.concat(ajSegTerre(63, 'N', 176, 'E', 67, 'N', 173, 'E', '#523703', true, 'Zkklrggmop'))
    stor.acache = stor.acache.concat(ajSegTerre(67, 'N', 173, 'E', 72, 'N', 150, 'E', '#523703', true, 'Zkklrggmop'))
    p = []
    for (let j = i; j < stor.acache.length; j++) {
      p.push(stor.acache[j].tag)
    }
    stor.mtgApp.addPolygon({
      points: p,
      tag: '3ttpffola3fee',
      hidden: true
    })
    stor.mtgApp.addSurfacePoly({
      poly: '3ttpffola3fee',
      color: '#9b6220',
      fillStyle: 'fill',
      tag: 'surfAm6'
    })

    i = stor.acache.length
    stor.mtgApp.addCalc({ nameCalc: 'Zkklrggmop2', formula: 'si(valCurseurb>204,si(valCurseurb>340,1,0),1)' })
    stor.acache = stor.acache.concat(ajSegTerre(21, 'N', 73, 'E', 8, 'N', 77, 'E', '#ff0000', true, 'Zkklrggmop2'))
    stor.acache = stor.acache.concat(ajSegTerre(8, 'N', 77, 'E', 16, 'N', 80, 'E', '#ff0000', true, 'Zkklrggmop2'))
    stor.acache = stor.acache.concat(ajSegTerre(16, 'N', 80, 'E', 21, 'N', 85, 'E', '#ff0000', true, 'Zkklrggmop2'))
    stor.acache = stor.acache.concat(ajSegTerre(21, 'N', 85, 'E', 20, 'N', 94, 'E', '#ff0000', true, 'Zkklrggmop2'))
    stor.acache = stor.acache.concat(ajSegTerre(20, 'N', 94, 'E', 15, 'N', 95, 'E', '#ff0000', true, 'Zkklrggmop2'))
    stor.acache = stor.acache.concat(ajSegTerre(15, 'N', 95, 'E', 10, 'N', 104, 'E', '#ff0000', true, 'Zkklrggmop2'))
    stor.acache = stor.acache.concat(ajSegTerre(10, 'N', 104, 'E', 13, 'N', 108, 'E', '#ff0000', true, 'Zkklrggmop2'))
    stor.acache = stor.acache.concat(ajSegTerre(13, 'N', 108, 'E', 19, 'N', 105, 'E', '#ff0000', true, 'Zkklrggmop2'))
    stor.acache = stor.acache.concat(ajSegTerre(19, 'N', 105, 'E', 50, 'N', 115, 'E', '#ff0000', true, 'Zkklrggmop2'))
    stor.acache = stor.acache.concat(ajSegTerre(50, 'N', 115, 'E', 76, 'N', 102, 'E', '#ff0000', true, 'Zkklrggmop2'))
    stor.acache = stor.acache.concat(ajSegTerre(76, 'N', 102, 'E', 68, 'N', 66, 'E', '#523703', true, 'Zkklrggmop2'))
    stor.acache = stor.acache.concat(ajSegTerre(68, 'N', 66, 'E', 21, 'N', 73, 'E', '#523703', true, 'Zkklrggmop2'))
    p = []
    for (let j = i; j < stor.acache.length; j++) {
      p.push(stor.acache[j].tag)
    }
    stor.mtgApp.addPolygon({
      points: p,
      tag: '3ttpffolaff3fee',
      hidden: true
    })
    stor.mtgApp.addSurfacePoly({
      poly: '3ttpffolaff3fee',
      color: '#9b6220',
      fillStyle: 'fill',
      tag: 'surfAm7'
    })

    stor.mtgApp.addCalc({ nameCalc: 'Zkklrggmop3', formula: 'si(valCurseurb>230,0,1)' })
    i = stor.acache.length
    stor.acache = stor.acache.concat(ajSegTerre(68, 'N', 66, 'E', 63, 'N', 34, 'E', '#523703', true, 'Zkklrggmop3'))
    stor.acache = stor.acache.concat(ajSegTerre(63, 'N', 34, 'E', 28, 'N', 47, 'E', '#523703', true, 'Zkklrggmop3'))
    stor.acache = stor.acache.concat(ajSegTerre(28, 'N', 47, 'E', 21, 'N', 73, 'E', '#ff0000', true, 'Zkklrggmop3'))
    stor.acache = stor.acache.concat(ajSegTerre(21, 'N', 73, 'E', 50, 'N', 80, 'E', '#ff0000', true, 'Zkklrggmop3'))
    stor.acache = stor.acache.concat(ajSegTerre(50, 'N', 80, 'E', 68, 'N', 66, 'E', '#ff0000', true, 'Zkklrggmop3'))
    p = []
    for (let j = i; j < stor.acache.length; j++) {
      p.push(stor.acache[j].tag)
    }
    stor.mtgApp.addPolygon({
      points: p,
      tag: '3ttp4ffolaff3fee',
      hidden: true
    })
    stor.mtgApp.addSurfacePoly({
      poly: '3ttp4ffolaff3fee',
      color: '#9b6220',
      fillStyle: 'fill',
      tag: 'surfAm8'
    })

    stor.mtgApp.addCalc({ nameCalc: 'Zkklrggmop4', formula: 'si(valCurseurb>270,0,si(valCurseurb<33,0,1))' })
    i = stor.acache.length
    stor.acache = stor.acache.concat(ajSegTerre(63, 'N', 34, 'E', 70, 'N', 27, 'E', '#000000', true, 'Zkklrggmop4'))
    stor.acache = stor.acache.concat(ajSegTerre(70, 'N', 27, 'E', 60, 'N', 4, 'E', '#ffffff', true, 'Zkklrggmop4'))
    stor.acache = stor.acache.concat(ajSegTerre(60, 'N', 4, 'E', 53, 'N', 8, 'E', '#2a4d11', true, 'Zkklrggmop4'))
    stor.acache = stor.acache.concat(ajSegTerre(53, 'N', 8, 'E', 48, 'N', 0, 'W', '#000000', true, 'Zkklrggmop4'))

    stor.acache = stor.acache.concat(ajSegTerre(48, 'N', 0, 'W', 48, 'N', 4, 'W', '#ff0000', true, 'Zkklrggmop4'))
    stor.acache = stor.acache.concat(ajSegTerre(48, 'N', 4, 'W', 47, 'N', 1, 'W', '#ff0000', true, 'Zkklrggmop4'))
    stor.acache = stor.acache.concat(ajSegTerre(47, 'N', 1, 'W', 43, 'N', 1, 'W', '#ff0000', true, 'Zkklrggmop4'))
    stor.acache = stor.acache.concat(ajSegTerre(43, 'N', 1, 'W', 43, 'N', 8, 'W', '#ff0000', true, 'Zkklrggmop4'))
    stor.acache = stor.acache.concat(ajSegTerre(43, 'N', 8, 'W', 37, 'N', 8, 'W', '#0000FF', true, 'Zkklrggmop4'))
    stor.acache = stor.acache.concat(ajSegTerre(37, 'N', 8, 'W', 36, 'N', 5, 'W', '#0000FF', true, 'Zkklrggmop4'))
    stor.acache = stor.acache.concat(ajSegTerre(36, 'N', 5, 'W', 37, 'N', 1, 'W', '#0000FF', true, 'Zkklrggmop4'))
    stor.acache = stor.acache.concat(ajSegTerre(37, 'N', 1, 'W', 45, 'N', 11, 'E', '#0000FF', true, 'Zkklrggmop4'))

    stor.acache = stor.acache.concat(ajSegTerre(45, 'N', 11, 'E', 38, 'N', 15, 'E', '#000000', true, 'Zkklrggmop4'))
    stor.acache = stor.acache.concat(ajSegTerre(38, 'N', 15, 'E', 40, 'N', 18, 'E', '#000000', true, 'Zkklrggmop4'))
    stor.acache = stor.acache.concat(ajSegTerre(40, 'N', 18, 'E', 42, 'N', 14, 'E', '#000000', true, 'Zkklrggmop4'))
    stor.acache = stor.acache.concat(ajSegTerre(42, 'N', 14, 'E', 45, 'N', 13, 'E', '#000000', true, 'Zkklrggmop4'))
    stor.acache = stor.acache.concat(ajSegTerre(45, 'N', 13, 'E', 45, 'N', 14, 'E', '#d91212', true, 'Zkklrggmop4'))
    stor.acache = stor.acache.concat(ajSegTerre(45, 'N', 14, 'E', 41, 'N', 19, 'E', '#d91212', true, 'Zkklrggmop4'))
    stor.acache = stor.acache.concat(ajSegTerre(41, 'N', 19, 'E', 40, 'N', 19, 'E', '#d91212', true, 'Zkklrggmop4'))
    stor.acache = stor.acache.concat(ajSegTerre(40, 'N', 19, 'E', 36, 'N', 22, 'E', '#d91212', true, 'Zkklrggmop4'))
    stor.acache = stor.acache.concat(ajSegTerre(36, 'N', 22, 'E', 40, 'N', 23, 'E', '#000000', true, 'Zkklrggmop4'))
    stor.acache = stor.acache.concat(ajSegTerre(40, 'N', 23, 'E', 36, 'N', 28, 'E', '#000000', true, 'Zkklrggmop4'))
    stor.acache = stor.acache.concat(ajSegTerre(36, 'N', 28, 'E', 37, 'N', 36, 'E', '#000000', true, 'Zkklrggmop4'))
    stor.acache = stor.acache.concat(ajSegTerre(37, 'N', 36, 'E', 31, 'N', 34, 'E', '#000000', true, 'Zkklrggmop4'))
    stor.acache = stor.acache.concat(ajSegTerre(31, 'N', 34, 'E', 29, 'N', 33, 'E', '#000000', true, 'Zkklrggmop4'))

    stor.acache = stor.acache.concat(ajSegTerre(29, 'N', 33, 'E', 22, 'N', 40, 'E', '#000000', true, 'Zkklrggmop4'))
    stor.acache = stor.acache.concat(ajSegTerre(22, 'N', 40, 'E', 15, 'N', 45, 'E', '#000000', true, 'Zkklrggmop4'))
    stor.acache = stor.acache.concat(ajSegTerre(15, 'N', 45, 'E', 17, 'N', 51, 'E', '#000000', true, 'Zkklrggmop4'))
    stor.acache = stor.acache.concat(ajSegTerre(17, 'N', 51, 'E', 21, 'N', 59, 'E', '#000000', true, 'Zkklrggmop4'))
    stor.acache = stor.acache.concat(ajSegTerre(21, 'N', 59, 'E', 24, 'N', 55, 'E', '#000000', true, 'Zkklrggmop4'))
    stor.acache = stor.acache.concat(ajSegTerre(24, 'N', 55, 'E', 23, 'N', 43, 'E', '#000000', true, 'Zkklrggmop4'))
    stor.acache = stor.acache.concat(ajSegTerre(23, 'N', 43, 'E', 30, 'N', 50, 'E', '#000000', true, 'Zkklrggmop4'))
    stor.acache = stor.acache.concat(ajSegTerre(30, 'N', 50, 'E', 63, 'N', 34, 'E', '#ffffff', true, 'Zkklrggmop4'))

    p = []
    for (let j = i; j < stor.acache.length; j++) {
      p.push(stor.acache[j].tag)
    }
    stor.mtgApp.addPolygon({
      points: p,
      tag: 'ttpffofdfdlafee2',
      hidden: true
    })
    stor.mtgApp.addSurfacePoly({
      poly: 'ttpffofdfdlafee2',
      color: '#9b6220',
      fillStyle: 'fill',
      tag: 'surfAm9'
    })
  }
  function ajEquateur () {
    stor.mtgApp.addPointXY({
      x: '-cos(Lo1potCursLo/Lo1Lo2*180)',
      y: '-sin(Lo1potCursLo/Lo1Lo2*180)*0.2',
      rep: 'Repere2',
      tag: 'PtMoEq',
      name: 'PtMoEq',
      hiddenName: true,
      pointStyle: 'biground',
      color: 'red',
      hidden: true
    })
    stor.mtgApp.addPointLocus({
      a: 'potCursLo',
      b: 'PtMoEq',
      x: 200,
      color: 'red',
      thickness: 4
    })
  }
  function ajGreenMer () {
    for (let i = -90; i < 90; i++) {
      const j = (i < 0) ? 'S' : 'N'
      const jj = ajPoint(Math.abs(i), j, 0, 'E', 'red', 'littleround')
      if (i === 0) {
        stor.tagLongPtOrigine = jj
      }
    }
  }
  function ajPara () {
    for (let i = 0; i < 180; i++) {
      const jj = ajPointF('latiP', 'N', i, 'E', 'blue', 'littleround')
      if (i === 90) stor.tagLatPt = jj
    }
  }
  function ajMeridien () {
    for (let i = -90; i < 90; i++) {
      const j = (i < 0) ? 'S' : 'N'
      const jj = ajPoint(Math.abs(i), j, 'longiP', 'E', 'green', 'littleround')
      if (i === 0) stor.tagLongPt = jj
    }
    stor.mtgApp.addCircleOA({
      o: 'O',
      a: 'N',
      thickness: 3
    })
    stor.mtgApp.addLinkedText({
      text: '#G#Val(longiP2)°',
      a: stor.tagLongPt,
      offsetX: 10,
      offsetY: 10,
      color: 'green'
    })
  }
  function radians (h) {
    return h * Math.PI / 180
  }
  function ajSegTerre (lat1, ns, long1, eo, lat2, ns2, long2, eo2, cool, visible, condition) {
    const aret = []
    const long = (ns === 'N') ? lat1 : -lat1
    const lat = (eo === 'E') ? -long1 : long1

    const longBis = (ns2 === 'N') ? lat2 : -lat2
    const latBis = (eo2 === 'E') ? -long2 : long2

    const lalong = 350 * Math.acos(Math.sin(radians(long)) * Math.sin(radians(longBis)) + Math.cos(radians(long)) * Math.cos(radians(longBis)) * Math.cos(radians(lat - latBis)))

    for (let i = 0; i < Math.ceil(lalong / 10); i++) {
      const latE = lat + i * (latBis - lat) / Math.ceil(lalong / 10)
      const longE = long + i * (longBis - long) / Math.ceil(lalong / 10)
      aret.push({ tag: ajPoint(longE, 'N', latE, 'W', cool, 'round', visible, condition), lat: latE })
    }
    return aret
  }
  function ajPoint (lat1, ns, long1, eo, cool, pointStyle, visible, condition) {
    const long = (ns === 'N') ? lat1 : -lat1
    let lat, aa
    if (typeof long1 === 'string') {
      lat = long1
      aa = '(' + long1 + '+180-valCurseur)'
    } else {
      lat = (eo === 'E') ? -long1 : long1
      aa = '(' + (lat + 180) + '-valCurseur)'
    }
    const rem = '(si(' + aa + '<-180,' + aa + '+360,si(' + aa + '>180,' + aa + '-360,' + aa + ')))'
    /*
    const tag1 = newtag()
    stor.mtgApp.addCalc({
      nameCalc: tag1,
      formula: '-cos(' + lat + ')'
    })
    const tag2 = newtag()
    stor.mtgApp.addCalc({
      nameCalc: tag2,
      formula: 'sin(' + lat + ')'
    })
    */
    const tag3 = newtag()
    stor.mtgApp.addPointXY({
      x: '-cos(' + lat + ')',
      y: 'sin(' + lat + ')',
      rep: 'Repere1',
      tag: tag3,
      name: tag3,
      hidden: true,
      hiddenName: true,
      color: 'red'
    })
    const tagRep = newtag()
    stor.mtgApp.addSystemOfAxis({
      o: 'O',
      a: tag3,
      b: 'N',
      hidden: false,
      tag: tagRep
    })
    /*
    const tagSeg = newtag()
    stor.mtgApp.addSegment({
      a: tag3,
      b: 'O',
      tag: tagSeg,
      hidden: true
    })

    const tagAbI = newtag()
    stor.mtgApp.addCalc({
      nameCalc: tagAbI,
      formula: 'cos(' + long + ')'
    })
    const tagOrdI = newtag()
    stor.mtgApp.addCalc({
      nameCalc: tagOrdI,
      formula: 'sin(' + long + ')'
    })
    const tagBuf1 = newtag()
    stor.mtgApp.addImPointDilation({
      o: 'O',
      a: tag3,
      x: tagAbI,
      tag: tagBuf1,
      name: tagBuf1,
      hidden: true
    })
    const tagparI1 = newtag()
    stor.mtgApp.addLinePar({
      d: 'axeord1',
      a: tagBuf1,
      tag: tagparI1,
      hidden: true,
      color: 'green'
    })
    const tagBuf2 = newtag()
    stor.mtgApp.addImPointDilation({
      o: 'O',
      a: 'N',
      x: tagOrdI,
      tag: tagBuf2,
      name: tagBuf2,
      hidden: true
    })
    const tagparI2 = newtag()
    stor.mtgApp.addLinePar({
      d: tagSeg,
      a: tagBuf2,
      tag: tagparI2,
      hidden: true,
      color: 'green'
    })
    */
    const tagIII = newtag()
    stor.mtgApp.addPointXY({
      x: 'cos(' + long + ')',
      y: 'sin(' + long + ')',
      rep: tagRep,
      tag: tagIII,
      name: tagIII,
      hidden: true,
      hiddenName: true,
      color: 'blue'
    })
    /*
    stor.mtgApp.addIntLineLine({
      d: tagparI2,
      d2: tagparI1,
      name: tagIII,
      pointStyle: 'bigmult',
      color: 'red',
      hidden: true
    })
    const tagCalBuf = newtag()
    stor.mtgApp.addCalc({
      nameCalc: tagCalBuf,
      formula: rem
    })
    const tagCal = newtag()
    stor.mtgApp.addCalc({
      nameCalc: tagCal,
      formula: 'si(' + tagCalBuf + '<-90,1000,si(' + tagCalBuf + '>90,1000,1))'
    })
    */
    const ff = 'si(' + rem + '<-90,0,si(' + rem + '>90,0,1))'
    const ff2 = 'si(' + rem + '>90,0,1)'
    const tagRet = newtag()
    stor.mtgApp.addImPointDilation({
      o: 'vers' + Math.round(long) + '1',
      a: tagIII,
      x: ff,
      tag: tagRet,
      name: tagRet,
      hidden: true,
      color: cool,
      hiddenName: true,
      pointStyle
    })
    const tagRet2 = newtag()
    stor.mtgApp.addImPointDilation({
      o: 'vers' + Math.round(long) + '2',
      a: tagRet,
      x: ff2,
      tag: tagRet2,
      name: tagRet2,
      hidden: visible || (condition),
      color: cool,
      hiddenName: true,
      pointStyle
    })
    if (condition) {
      const tagRet3 = newtag()
      stor.mtgApp.addImPointDilation({
        o: 'R',
        a: tagRet2,
        x: condition,
        tag: tagRet3,
        name: tagRet3,
        hidden: visible,
        color: cool,
        hiddenName: true,
        pointStyle
      })
      return tagRet3
    } else {
      return tagRet2
    }
    // O et O7 dependent de la lat
  }
  function ajPointF (lat1, ns, long1, eo, cool, pointStyle) {
    const long = (ns === 'N') ? lat1 : -lat1
    const lat = (eo === 'E') ? -long1 : long1

    /*
    const tag1 = newtag()
    stor.mtgApp.addCalc({
      nameCalc: tag1,
      formula: '-cos(' + lat + ')'
    })
    const tag2 = newtag()
    stor.mtgApp.addCalc({
      nameCalc: tag2,
      formula: 'sin(' + lat + ')*0.2'
    })
    const tag3 = newtag()
    stor.mtgApp.addImPointDilation({
      o: 'O',
      a: 'I',
      x: tag1,
      tag: tag3,
      name: tag3,
      hidden: true
    })
    const tag4 = newtag()
    stor.mtgApp.addLinePar({
      d: 'axeord1',
      a: tag3,
      tag: tag4,
      hidden: true,
      color: 'green'
    })
    const tag5 = newtag()
    stor.mtgApp.addImPointDilation({
      o: 'O',
      a: 'N',
      x: tag2,
      tag: tag5,
      name: tag5,
      hidden: true
    })
    const tag6 = newtag()
    stor.mtgApp.addLinePar({
      d: 'axeab1',
      a: tag5,
      tag: tag6,
      hidden: true,
      color: 'green'
    })
    const tag7 = newtag()
    stor.mtgApp.addIntLineLine({
      d: tag6,
      d2: tag4,
      name: tag7,
      // pointStyle: 'bigmult',
      color: cool,
      hidden: true,
      hiddenName: true
    })
    const tagSeg = newtag()
    stor.mtgApp.addSegment({
      a: tag7,
      b: 'O',
      tag: tagSeg,
      hidden: true
    })
    */

    const tag7 = newtag()
    stor.mtgApp.addPointXY({
      x: '-cos(' + lat + ')',
      y: 'sin(' + lat + ')*0.2',
      rep: 'Repere2',
      tag: tag7,
      name: tag7,
      hidden: true,
      hiddenName: true
    })
    const tagRep = newtag()
    stor.mtgApp.addSystemOfAxis({
      o: 'O',
      a: tag7,
      b: 'N',
      hidden: false,
      tag: tagRep
    })
    const tagIII = newtag()
    stor.mtgApp.addPointXY({
      x: 'cos(' + long + ')',
      y: 'sin(' + long + ')',
      rep: tagRep,
      tag: tagIII,
      name: tagIII,
      pointStyle,
      color: cool,
      hiddenName: true
    })
    return tagIII

    /*
    const tagAbI = newtag()
    stor.mtgApp.addCalc({
      nameCalc: tagAbI,
      formula: 'cos(' + long + ')'
    })
    const tagOrdI = newtag()
    stor.mtgApp.addCalc({
      nameCalc: tagOrdI,
      formula: 'sin(' + long + ')'
    })
    const tagBuf1 = newtag()
    stor.mtgApp.addImPointDilation({
      o: 'O',
      a: tag7,
      x: tagAbI,
      tag: tagBuf1,
      name: tagBuf1,
      hidden: true,
      hiddenName: true
    })
    const tagparI1 = newtag()
    stor.mtgApp.addLinePar({
      d: 'axeord1',
      a: tagBuf1,
      tag: tagparI1,
      hidden: true,
      color: 'green'
    })
    const tagBuf2 = newtag()
    stor.mtgApp.addImPointDilation({
      o: 'O',
      a: 'N',
      x: tagOrdI,
      tag: tagBuf2,
      name: tagBuf2,
      hidden: true,
      hiddenName: true
    })
    const tagparI2 = newtag()
    stor.mtgApp.addLinePar({
      d: tagSeg,
      a: tagBuf2,
      tag: tagparI2,
      hidden: true,
      color: 'green'
    })
    const tagIII = newtag()
    stor.mtgApp.addIntLineLine({
      d: tagparI2,
      d2: tagparI1,
      name: tagIII,
      tag: tagIII,
      pointStyle,
      color: cool,
      hidden: false,
      hiddenName: true
    })
    return tagIII
    */
  }

  switch (this.etat) {
    case 'enonce':
      if (this.debutDeLaSection) {
        initSection()
      } else {
        enonceMain()
      }
      break // case "enonce":

    case 'correction':
      try {
        // on veut être rappelé dans l’état navigation
        this.finCorrection('navigation', true)
      } catch (error) {
        console.error(error)
      }
      break // case "correction":

    case 'navigation':
      if (this.sectionTerminee()) {
        stor.scratch.lesdivs.BoutonsSuite.appendChild(this.buttonsElts.sectionSuivante)
      } else {
        this.etat = 'enonce'
        stor.scratch.lesdivs.BoutonsSuite.appendChild(this.buttonsElts.suite)
      }
      this.finNavigation()
      break // case "navigation":
  }
}
