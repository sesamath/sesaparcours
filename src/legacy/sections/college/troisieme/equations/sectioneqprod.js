import { j3pAddContent, j3pAddElt, j3pAddTxt, j3pAjouteBouton, j3pArrondi, j3pDetruit, j3pEmpty, j3pGetRandomBool, j3pGetRandomInt, j3pPGCD, j3pShuffle } from 'src/legacy/core/functions'
import { j3pAffiche } from 'src/lib/mathquill/functions'
import { addDefaultTable } from 'src/legacy/themes/table'
import { j3pAfficheCroixFenetres, j3pCreeFenetres, j3pToggleFenetres } from 'src/legacy/core/functionsJqDialog'
import ListeDeroulante from 'src/legacy/outils/listeDeroulante/ListeDeroulante'
import ZoneStyleMathquill2 from 'src/legacy/outils/zoneStyleMathquill/ZoneStyleMathquill2'
import BrouillonCalculs from 'src/legacy/outils/brouillon/BrouillonCalculs'
import textesGeneriques from 'src/lib/core/textes'

const { cBien, cFaux, essaieEncore, reponseIncomplete, tempsDepasse } = textesGeneriques

/**
 * Ajouter une description ici
 * @fileOverview
 * @author Tommy Barroy <t.barroy@laposte.net>
 * @since 2017-10
 */
export const params = {
  outils: ['calculatrice', 'mathquill'],
  /**
   * Ce qui est paramétrable dans l’éditeur de graphe
   * @type {LegacySectionParamsParametres}
   */
  parametres: [
    ['nbrepetitions', 9, 'entier', 'Nombre de répétitions de la section'],
    ['limite', 0, 'entier', 'Temps disponible par répétition, en secondes'],
    ['nbchances', 1, 'entier', 'Nombre d’essais par répétition'],
    ['entiers', true, 'boolean', 'coefficients entiers'],
    ['theme', 'standard', 'liste', 'Modèle pour l’affichage', ['standard', 'zonesAvecImageDeFond']]

  ]

}

/**
 * section racine01
 * @this {Parcours}
 */
export default function main () {
  const me = this
  const stor = me.storage
  const ds = me.donneesSection

  function creeLesDiv () {
    stor.lesdiv = {}
    stor.lesdiv.conteneur = j3pAddElt(me.zonesElts.MG, 'div', {
      style: me.styles.etendre('toutpetit.enonce', { padding: '10px' })
    })
    // Dans ce conteneur, on crée un div par phrase (cela forcera le retour à la ligne)
    const tab = addDefaultTable(stor.lesdiv.conteneur, 1, 5)
    modif(tab)
    stor.lesdiv.consigneG = tab[0][0]
    j3pAddContent(tab[0][1], '&nbsp;&nbsp;&nbsp;')
    stor.lesdiv.calculatrice = tab[0][2]
    j3pAddContent(tab[0][3], '&nbsp;&nbsp;&nbsp;')
    stor.lesdiv.correction = tab[0][4]
    stor.lesdiv.etape = addDefaultTable(stor.lesdiv.conteneur, 1, 1)[0][0]
    stor.lesdiv.travail = addDefaultTable(stor.lesdiv.conteneur, 1, 1)[0][0]
    j3pAddTxt(stor.lesdiv.etape, '&nbsp;')
    stor.lesdiv.explications = j3pAddElt(stor.lesdiv.conteneur, 'div')
    stor.lesdiv.solution = j3pAddElt(stor.lesdiv.conteneur, 'div')
    stor.lesdiv.explications.style.color = me.styles.cfaux
    stor.lesdiv.solution.style.color = me.styles.colorCorrection
    me.zonesElts.MG.classList.add('fond')
    stor.lesdiv.travail.classList.add('travail')
    stor.lesdiv.consigneG.classList.add('enonce')
  }
  function boutonfen () {
    j3pToggleFenetres('Calculatrice')
  }
  function isRepOk () {
    stor.errSigne = false
    stor.errDouble = false
    stor.errSimpli = false
    if (stor.etapeEncours === 1) {
      metvert()
      let ok = true
      if (stor.l1.reponse !== stor.at1) {
        stor.l1.corrige(false)
        ok = false
      }
      if (stor.l2.reponse !== stor.at2) {
        stor.l2.corrige(false)
        ok = false
      }
      return ok
    }
    if (stor.etapeEncours === 2) {
      metvert()
      let ok1 = true
      let ok2 = true
      const repElANUM = String(stor.zone1.reponse2()[0]).replace(/ /g, '')
      const repElADEN = String(stor.zone1.reponse2()[1]).replace(/ /g, '')
      if (!signeOk(repElANUM)) {
        ok1 = false
        stor.errSigne = true
        stor.zone1.corrige(false)
      }
      if (!signeOk(repElADEN)) {
        ok1 = false
        stor.errSigne = true
        stor.zone1.corrige(false)
      }
      if (Number(repElADEN) === 0) {
        ok1 = false
        stor.errSigne = true
        stor.zone1.corrige(false)
      }
      if (ok1) {
        if (!fracEgal({ num: Number(repElANUM), den: Number(repElADEN) }, { num: -stor.b.num * stor.a.den, den: stor.b.den * stor.a.num })) {
          ok1 = false
          stor.zone1.corrige(false)
        }
      }
      const repElANUM2 = String(stor.zone2.reponse2()[0]).replace(/ /g, '')
      const repElADEN2 = String(stor.zone2.reponse2()[1]).replace(/ /g, '')
      if (!signeOk(repElANUM2)) {
        ok2 = false
        stor.errSigne = true
        stor.zone2.corrige(false)
      }
      if (!signeOk(repElADEN2)) {
        ok2 = false
        stor.errSigne = true
        stor.zone2.corrige(false)
      }
      if (Number(repElADEN2) === 0) {
        ok2 = false
        stor.errSigne = true
        stor.zone2.corrige(false)
      }
      if (ok2) {
        if (!fracEgal({ num: Number(repElANUM2), den: Number(repElADEN2) }, { num: -stor.d.num * stor.c.den, den: stor.d.den * stor.c.num })) {
          ok2 = false
          stor.zone2.corrige(false)
        }
      }
      return ok1 && ok2
    }
    if (stor.etapeEncours === 0) {
      if (stor.double) {
        if (stor.listFin.reponse !== 'admet une solution:') {
          stor.errDouble = true
          stor.listFin.corrige(false)
          return false
        }
      } else {
        if (stor.listFin.reponse !== 'admet deux solutions:') {
          stor.errDouble = true
          stor.listFin.corrige(false)
          return false
        }
      }
      metvert()
      let ok1 = true
      let ok2 = true
      let okta = false
      let oktc = false
      const repElANUM = String(stor.zone1.reponse2()[0]).replace(/ /g, '')
      const repElADEN = String(stor.zone1.reponse2()[1]).replace(/ /g, '')
      if (!signeOk(repElANUM)) {
        ok1 = false
        stor.errSigne = true
        stor.zone1.corrige(false)
      }
      if (!signeOk(repElADEN)) {
        ok1 = false
        stor.errSigne = true
        stor.zone1.corrige(false)
      }
      if (Number(repElADEN) === 0) {
        ok1 = false
        stor.errSigne = true
        stor.zone1.corrige(false)
      }
      if (ok1) {
        ok1 = false
        if (fracEgal({ num: Number(repElANUM), den: Number(repElADEN) }, { num: -stor.b.num * stor.a.den, den: stor.b.den * stor.a.num })) {
          okta = true
          ok1 = true
        }
        if (fracEgal({ num: Number(repElANUM), den: Number(repElADEN) }, { num: -stor.d.num * stor.c.den, den: stor.d.den * stor.c.num })) {
          oktc = true
          ok1 = true
        }
        if (!ok1) {
          stor.zone1.corrige(false)
        } else {
          // test si c’est simplifié
          const pgg = j3pPGCD(Math.abs(Number(repElANUM)), Math.abs(Number(repElADEN)))
          if (pgg !== 1) {
            ok1 = false
            stor.zone1.corrige(false)
            stor.errSimpli = true
          } else {
            if (stor.zone1.reponse().indexOf('frac') !== -1 && Math.abs(Number(repElADEN)) === 1) {
              ok1 = false
              stor.zone1.corrige(false)
              stor.errSimpli = true
            }
          }
        }
      }
      if (stor.zone2) {
        const repElANUM2 = String(stor.zone2.reponse2()[0]).replace(/ /g, '')
        const repElADEN2 = String(stor.zone2.reponse2()[1]).replace(/ /g, '')
        if (!signeOk(repElANUM2)) {
          ok2 = false
          stor.errSigne = true
          stor.zone2.corrige(false)
        }
        if (!signeOk(repElADEN2)) {
          ok2 = false
          stor.errSigne = true
          stor.zone2.corrige(false)
        }
        if (Number(repElADEN2) === 0) {
          ok2 = false
          stor.errSigne = true
          stor.zone2.corrige(false)
        }
        if (ok2) {
          ok2 = false
          if (fracEgal({ num: Number(repElANUM2), den: Number(repElADEN2) }, { num: -stor.b.num * stor.a.den, den: stor.b.den * stor.a.num })) {
            okta = true
            ok2 = true
          }
          if (fracEgal({ num: Number(repElANUM2), den: Number(repElADEN2) }, { num: -stor.d.num * stor.c.den, den: stor.d.den * stor.c.num })) {
            oktc = true
            ok2 = true
          }
          if (!ok2 || (ok1 && (!okta || !oktc))) {
            stor.zone2.corrige(false)
          } else {
            const pgg = j3pPGCD(Math.abs(Number(repElANUM2)), Math.abs(Number(repElADEN2)))
            if (pgg !== 1) {
              ok2 = false
              stor.zone2.corrige(false)
              stor.errSimpli = true
            } else {
              if (stor.zone2.reponse().indexOf('frac') !== -1 && Math.abs(Number(repElADEN2)) === 1) {
                ok2 = false
                stor.zone2.corrige(false)
                stor.errSimpli = true
              }
            }
          }
        }
        return ok1 && ok2 && okta && oktc
      } else {
        return ok1
      }
    }
  }

  function fracEgal (a, b) {
    return (a.num * b.den === a.den * b.num) && (b.den !== 0)
  }
  function signeOk (x) {
    if (x.indexOf('-') === -1) return true
    if (x.indexOf('-') !== 0) return false
    if (x.indexOf('-') !== x.lastIndexOf('-')) return false
    return true
  }
  function modif (tab) {
    for (let i = 0; i < tab.length; i++) {
      for (let j = 0; j < tab[i].length; j++) {
        tab[i][j].style.padding = '0px'
      }
    }
  }
  function affMembre (a, b) {
    let toret = (a.num === 0) ? '' : (afficheMoi(a, false) + 'x')
    toret += afficheMoi(b, true)
    return toret
  }
  function afficheMoi (s, forceSigne) {
    if (s.num === 0) return ''
    if (s.den === 1) {
      if (s.num === 1) {
        return (forceSigne) ? '+1' : ''
      }
      if (s.num === -1) return (forceSigne) ? '-1' : '-'
      if (s.num < 0) return String(s.num)
      return ((forceSigne) ? '+' : '') + String(s.num)
    } else {
      if (s.num < 0) return '-\\frac{' + Math.abs(s.num) + '}{' + s.den + '}'
      return ((forceSigne) ? '+' : '') + '\\frac{' + s.num + '}{' + s.den + '}'
    }
  }
  function poseQuestion () {
    j3pAffiche(stor.lesdiv.consigneG, null, '<b>On veut résoudre l’équation $' + stor.par1 + affMembre(stor.a, stor.b) + stor.par2 + '\\left(' + affMembre(stor.c, stor.d) + '\\right) = 0$</b>')
    stor.tab = addDefaultTable(stor.lesdiv.travail, 2, 4)
    modif(stor.tab)
    j3pAddContent(stor.tab[0][0], 'Soit&nbsp;')
    j3pAddContent(stor.tab[0][2], '&nbsp;, soit&nbsp;')
    let l1 = []
    l1.push('$' + affMembre(stor.a, stor.b) + '=0$')
    l1.push('$' + affMembre(stor.a, stor.b) + '=' + affMembre(stor.c, stor.d) + '$')
    if (stor.b.num !== 0) {
      l1.push('$' + afficheMoi(stor.a, false) + 'x=' + afficheMoi(stor.b, false) + '$')
    } else {
      l1.push('$' + afficheMoi(stor.a, false) + 'x=1$')
    }
    l1.push('$x=' + afficheMoi(stor.a, false) + '$')
    l1 = j3pShuffle(l1)
    l1.splice(0, 0, 'Choisir')
    stor.l1 = ListeDeroulante.create(stor.tab[0][1], l1)
    let l2 = []
    l2.push('$' + affMembre(stor.c, stor.d) + '=0$')
    l2.push('$' + affMembre(stor.a, stor.b) + '=' + affMembre(stor.c, stor.d) + '$')
    l2.push('$' + afficheMoi(stor.c, false) + 'x=' + afficheMoi(stor.d, false) + '$')
    l2.push('$x=' + afficheMoi(stor.c, false) + '$')
    l2 = j3pShuffle(l2)
    l2.splice(0, 0, 'Choisir')
    stor.l2 = ListeDeroulante.create(stor.tab[0][3], l2)
    stor.at1 = '$' + affMembre(stor.a, stor.b) + '=0$'
    stor.at2 = '$' + affMembre(stor.c, stor.d) + '=0$'
  }
  function poseQuestion2 () {
    j3pEmpty(stor.lesdiv.correction)
    j3pEmpty(stor.lesdiv.solution)
    j3pEmpty(stor.lesdiv.explications)
    stor.lesdiv.solution.classList.remove('correction')
    stor.lesdiv.explications.classList.remove('explique')
    const g1 = stor.l1.isOk()
    const g2 = stor.l2.isOk()
    j3pEmpty(stor.tab[0][1])
    j3pEmpty(stor.tab[0][3])
    stor.tab[0][0].style.verticalAlign = 'top'
    stor.tab[0][1].style.verticalAlign = 'top'
    stor.tab[0][2].style.verticalAlign = 'top'
    stor.tab[0][3].style.verticalAlign = 'top'
    const tb1 = addDefaultTable(stor.tab[0][1], 3, 3)
    modif(tb1)
    tb1[0][1].style.color = (g1) ? me.styles.cbien : me.styles.colorCorrection
    tb1[0][0].style.color = (g1) ? me.styles.cbien : me.styles.colorCorrection
    tb1[0][2].style.color = (g1) ? me.styles.cbien : me.styles.colorCorrection
    j3pAffiche(tb1[0][0], null, '&nbsp;&nbsp;&nbsp;$' + affMembre(stor.a, stor.b) + '$')
    j3pAffiche(tb1[0][1], null, '&nbsp;$=$&nbsp;')
    j3pAffiche(tb1[0][2], null, '$0$&nbsp;&nbsp;')
    j3pDetruit(tb1[1][1])
    j3pDetruit(tb1[1][2])
    j3pAffiche(tb1[2][0], null, '$x$')
    tb1[2][0].style.textAlign = 'right'
    j3pAffiche(tb1[2][1], null, '&nbsp;$=$&nbsp;')
    stor.zone1 = new ZoneStyleMathquill2(tb1[2][2], { restric: '0123456789/-,.', enter: () => { me.sectionCourante() } })
    tb1[1][0].setAttribute('colspan', 3)
    tb1[1][0].style.textAlign = 'center'
    const ttabl3 = addDefaultTable(tb1[1][0], 1, 5)
    j3pAddContent(ttabl3[0][0], '&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;')
    stor.brouillon1 = new BrouillonCalculs({
      caseBoutons: ttabl3[0][1],
      caseEgal: ttabl3[0][2],
      caseCalculs: ttabl3[0][3],
      caseApres: ttabl3[0][4]
    },
    { limite: 30, limitenb: 20, restric: '0123456789/x+-=', hasAutoKeyboard: true, lmax: 1, justeg: true, titre: '' })
    tb1[1][0].classList.add('caseBrouillon')
    stor.dez1 = tb1[0][0].parentNode.parentNode

    const tb2 = addDefaultTable(stor.tab[0][3], 3, 3)
    modif(tb2)
    tb2[0][1].style.color = (g2) ? me.styles.cbien : me.styles.colorCorrection
    tb2[0][0].style.color = (g2) ? me.styles.cbien : me.styles.colorCorrection
    tb2[0][2].style.color = (g2) ? me.styles.cbien : me.styles.colorCorrection
    j3pAffiche(tb2[0][0], null, '&nbsp;&nbsp;&nbsp;$' + affMembre(stor.c, stor.d) + '$')
    j3pAffiche(tb2[0][1], null, '&nbsp;$=$&nbsp;')
    j3pAffiche(tb2[0][2], null, '$0$&nbsp;&nbsp;')
    j3pDetruit(tb2[1][1])
    j3pDetruit(tb2[1][2])
    j3pAffiche(tb2[2][0], null, '$x$')
    tb2[2][0].style.textAlign = 'right'
    j3pAffiche(tb2[2][1], null, '&nbsp;$=$&nbsp;')
    stor.zone2 = new ZoneStyleMathquill2(tb2[2][2], { restric: '0123456789/-,.', enter: () => { me.sectionCourante() } })
    tb2[1][0].setAttribute('colspan', 3)
    tb2[1][0].style.textAlign = 'center'
    const ttabl4 = addDefaultTable(tb2[1][0], 1, 5)
    j3pAddContent(ttabl4[0][0], '&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;')
    stor.brouillon2 = new BrouillonCalculs({
      caseBoutons: ttabl4[0][1],
      caseEgal: ttabl4[0][2],
      caseCalculs: ttabl4[0][3],
      caseApres: ttabl4[0][4]
    },
    { limite: 30, limitenb: 20, restric: '0123456789/x+-=', hasAutoKeyboard: true, lmax: 1, justeg: true, titre: '' })
    tb2[1][0].classList.add('caseBrouillon')
    stor.dez2 = tb2[0][0].parentNode.parentNode
  }
  function poseQuestion3 () {
    j3pEmpty(stor.lesdiv.correction)
    j3pEmpty(stor.lesdiv.solution)
    j3pEmpty(stor.lesdiv.explications)
    if (stor.yaCo7) {
      for (let i = 0; i < stor.pourCo7.length; i++) {
        stor.pourCo7[i].classList.remove('correction')
      }
      for (let i = 0; i < stor.pourDezCo7.length; i++) {
        stor.pourDezCo7[i].style.display = 'none'
      }
    }
    stor.lesdiv.solution.classList.remove('correction')
    stor.lesdiv.explications.classList.remove('explique')
    const tabFin = addDefaultTable(stor.lesdiv.travail, 1, 6)
    j3pAffiche(tabFin[0][0], null, 'L’équation $' + stor.par1 + affMembre(stor.a, stor.b) + stor.par2 + '\\left(' + affMembre(stor.c, stor.d) + '\\right) = 0$&nbsp;')
    stor.listFin = ListeDeroulante.create(tabFin[0][1], ['Choisir', 'admet une solution:', 'admet deux solutions:'], { onChange: changeNbRep })
    j3pAddContent(tabFin[0][2], '&nbsp;')
    stor.douPrev = addDefaultTable(stor.lesdiv.travail, 1, 1)[0][0]
    stor.dou1 = tabFin[0][3]
    stor.dou2 = tabFin[0][4]
    stor.dou3 = tabFin[0][5]
    stor.zone1 = undefined
    stor.zone2 = undefined
  }
  function changeNbRep () {
    if (stor.zone1) {
      stor.zone1.disable()
      stor.zone1 = undefined
    }
    if (stor.zone2) {
      stor.zone2.disable()
      stor.zone2 = undefined
    }
    j3pEmpty(stor.dou1)
    j3pEmpty(stor.dou2)
    j3pEmpty(stor.dou3)
    if (stor.listFin.reponse === 'admet une solution:') {
      stor.zone1 = new ZoneStyleMathquill2(stor.dou1, { restric: '0123456789.,/-', enter: () => { me.sectionCourante() } })
      j3pEmpty(stor.douPrev)
      j3pAffiche(stor.douPrev, null, '<b><i>La solution doit être notée sous forme simplifiée.</i></b>')
    } else {
      stor.zone1 = new ZoneStyleMathquill2(stor.dou1, { restric: '0123456789.,/-', enter: () => { me.sectionCourante() } })
      j3pAddContent(stor.dou2, '&nbsp;et&nbsp;')
      stor.zone2 = new ZoneStyleMathquill2(stor.dou3, { restric: '0123456789.,/-', enter: () => { me.sectionCourante() } })
      j3pEmpty(stor.douPrev)
      j3pAffiche(stor.douPrev, null, '<b><i>Les solutions doivent être notées sous forme simplifiée.</i></b>')
    }
  }

  function faisChoixExo () {
    const yy = stor.lesExos.pop()
    do {
      stor.a = newFrac(true)
      stor.b = newFrac(true)
      stor.c = newFrac(true)
      stor.d = newFrac(true)
      stor.double = false
      if (yy === 'b0') stor.b.num = 0
      if (yy === 'b1double') {
        stor.double = true
        const mul = j3pGetRandomInt(2, 10)
        stor.c.num = stor.a.num * mul
        stor.c.den = stor.a.den
        stor.d.num = stor.b.num * mul
        stor.d.den = stor.b.den
        const pg1 = j3pPGCD(stor.c.num, stor.c.den, { negativesAllowed: true })
        stor.c.num = j3pArrondi(stor.c.num / pg1, 0)
        stor.c.den = j3pArrondi(stor.c.den / pg1, 0)
        const pg2 = j3pPGCD(stor.d.num, stor.d.den, { negativesAllowed: true })
        stor.d.num = j3pArrondi(stor.d.num / pg2, 0)
        stor.d.den = j3pArrondi(stor.d.den / pg2, 0)
      }
      if (yy === 'b1') {
        if (fracEgal({ num: -stor.b.num * stor.a.den, den: stor.b.den * stor.a.num }, { num: -stor.d.num * stor.c.den, den: stor.d.den * stor.c.num })) {
          stor.c.num++
          const pg1 = j3pPGCD(stor.c.num, stor.c.den, { negativesAllowed: true })
          stor.c.num = j3pArrondi(stor.c.num / pg1, 0)
          stor.c.den = j3pArrondi(stor.c.den / pg1, 0)
        }
      }
    } while (Math.abs(stor.c.num) > 100 || Math.abs(stor.d.num) > 100)
    stor.yaCo7 = false
  }
  function initSection () {
    // Construction de la page
    me.construitStructurePage('presentation3')

    ds.nbetapes = 3
    ds.nbitems = ds.nbetapes * ds.nbrepetitions
    stor.numerateur = 1
    stor.denominateur = 1
    stor.etapeEnCours = 1
    // contiendra soit consigne1 soit consigne2 (1 fois sur 2)
    stor.consigneaffichee = ''
    /*
             Par convention,`
            `   me.typederreurs[0] = nombre de bonnes réponses
                me.typederreurs[1] = nombre de fois où l’élève a utilisé une chance supplémentaire
                me.typederreurs[2] = nombre de mauvaises réponses
                me.typederreurs[10] = nombre de fois où l’élève a eu faux à cause d’un temps limite
                LES AUTRES peuvent être utilisées par la détermination de la pe dans le cas d’une section qualitative
             */
    me.typederreurs = [0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0]
    stor.etapeEncours = 0
    me.score = 0

    const lp = j3pShuffle(['b0', 'b1', 'b1double', 'b1'])
    stor.lesExos = []
    for (let i = 0; i < ds.nbrepetitions; i++) {
      stor.lesExos.push(lp[i % lp.length])
    }
    stor.lesExos = j3pShuffle(stor.lesExos)

    me.afficheTitre('Résoudre une équation produit')
    enonceMain()
  }
  function suivEtape () {
    stor.etapeEncours = (stor.etapeEncours + 1) % 3
  }
  function enonceMain () {
    suivEtape()
    if (stor.etapeEncours === 1) {
      me.videLesZones()
      creeLesDiv()
      me.fenetresjq = [{ name: 'Calculatrice', title: 'Une calculatrice', left: 600, top: 10 }]
      j3pCreeFenetres(me)
      j3pAfficheCroixFenetres('Calculatrice')
      j3pAjouteBouton(stor.lesdiv.calculatrice, boutonfen, { className: 'MepBoutons', value: 'Calculatrice' })
      faisChoixExo()
      stor.par1 = (stor.b.num === 0) ? '' : '\\left('
      stor.par2 = (stor.b.num === 0) ? '' : '\\right)'
      poseQuestion()
    } else if (stor.etapeEncours === 2) {
      poseQuestion2()
    } else {
      poseQuestion3()
    }
    me.finEnonce()
  }

  function newFrac (pasZero) {
    let a = j3pGetRandomInt((pasZero) ? 1 : 0, 50)
    let b
    do {
      b = (ds.entiers) ? 1 : j3pGetRandomInt(1, 50)
    } while (j3pPGCD(a, b, { valueIfZero: 1 }) !== 1)
    if (j3pGetRandomBool()) a = -a
    return { num: a, den: b }
  }
  function affcofo (isFin) {
    let yaexplik = false
    if (stor.errSigne) {
      yaexplik = true
      j3pAffiche(stor.lesdiv.explications, null, 'Problème d’écriture !')
    }
    if (stor.errDouble) {
      yaexplik = true
      if (stor.double) {
        j3pAffiche(stor.lesdiv.explications, null, 'Les deux équations ont la même solution !')
      } else {
        j3pAffiche(stor.lesdiv.explications, null, 'Les deux équations ont une solution différente !')
      }
    }
    if (stor.errSimpli) {
      yaexplik = true
      j3pAffiche(stor.lesdiv.explications, null, 'Tu peux simplifier l’écriture !')
    }
    if (isFin) {
      barrelesfo()
      disaTout()
      switch (stor.etapeEncours) {
        case 0:
          if (stor.double) {
            let rep = ''
            const rep1 = { num: -stor.d.num * stor.c.den, den: stor.d.den * stor.c.num }
            const pg = j3pPGCD(rep1.num, rep1.den, { negativesAllowed: true })
            if (j3pArrondi(rep1.den / pg, 6) === 1 || j3pArrondi(rep1.den / pg, 6) === -1) {
              rep = '$' + j3pArrondi(rep1.num / rep1.den, 0) + '$'
            } else {
              const signe = (rep1.num / rep1.den < 0) ? '-' : ''
              rep = '$' + signe + '\\frac{' + Math.abs(j3pArrondi(rep1.num / pg, 6)) + '}{' + Math.abs(j3pArrondi(rep1.den / pg, 6)) + '}$'
            }
            j3pAffiche(stor.lesdiv.solution, null, 'L’équation $' + stor.par1 + affMembre(stor.a, stor.b) + stor.par2 + '\\left(' + affMembre(stor.c, stor.d) + '\\right) = 0$ admet une solution: ' + rep)
          } else {
            let rep = ''
            const rep1 = { num: -stor.d.num * stor.c.den, den: stor.d.den * stor.c.num }
            const pg = j3pPGCD(rep1.num, rep1.den, { negativesAllowed: true })
            if (j3pArrondi(rep1.den / pg, 6) === 1 || j3pArrondi(rep1.den / pg, 6) === -1) {
              rep = '$' + j3pArrondi(rep1.num / rep1.den, 0) + '$'
            } else {
              const signe = (rep1.num / rep1.den < 0) ? '-' : ''
              rep = '$' + signe + '\\frac{' + Math.abs(j3pArrondi(rep1.num / pg, 6)) + '}{' + Math.abs(j3pArrondi(rep1.den / pg, 6)) + '}$'
            }

            let repbis = ''
            const rep2 = { num: -stor.b.num * stor.a.den, den: stor.b.den * stor.a.num }
            const pg2 = j3pPGCD(rep2.num, rep2.den, { negativesAllowed: true })
            if (j3pArrondi(rep2.den / pg2, 6) === 1 || j3pArrondi(rep2.den / pg2, 6) === -1) {
              repbis = '$' + j3pArrondi(rep2.num / rep2.den, 0) + '$'
            } else {
              const signe = (rep2.num / rep2.den < 0) ? '-' : ''
              repbis = '$' + signe + '\\frac{' + Math.abs(j3pArrondi(rep2.num / pg2, 6)) + '}{' + Math.abs(j3pArrondi(rep2.den / pg2, 6)) + '}$'
            }

            j3pAffiche(stor.lesdiv.solution, null, 'L’équation $' + stor.par1 + affMembre(stor.a, stor.b) + stor.par2 + '\\left(' + affMembre(stor.c, stor.d) + '\\right) = 0$ admet deux solutions: ' + repbis + ' et ' + rep + ' .')
          }
          stor.lesdiv.solution.classList.add('correction')
          break
        case 1:
          stor.lesdiv.solution.classList.add('correction')
          j3pAffiche(stor.lesdiv.solution, null, 'Si un produit est nul, alors l’un au moins de ses facteurs est nul,<br>')
          j3pAffiche(stor.lesdiv.solution, null, 'donc soit ' + stor.at1 + ', soit ' + stor.at2 + ' .')
          break
        case 2:
          stor.yaCo7 = true
          stor.pourCo7 = []
          stor.pourDezCo7 = []
          if (stor.zone1.isOk() === false) {
            stor.tab[1][1].style.color = me.styles.colorCorrection
            stor.tab[1][1].style.verticalAlign = 'top'
            const tabco1 = addDefaultTable(stor.tab[0][1], 3, 3)
            modif(tabco1)
            tabco1[0][0].parentNode.parentNode.style.color = me.styles.colorCorrection
            tabco1[0][0].parentNode.parentNode.style.verticalAlign = 'top'
            tabco1[0][0].style.textAlign = 'right'
            tabco1[1][0].style.textAlign = 'right'
            tabco1[2][0].style.textAlign = 'right'
            tabco1[0][2].style.textAlign = 'left'
            tabco1[1][2].style.textAlign = 'left'
            tabco1[2][2].style.textAlign = 'left'
            tabco1[0][0].parentNode.parentNode.classList.add('correction')
            stor.pourCo7.push(tabco1[0][0].parentNode.parentNode)
            stor.pourDezCo7.push(stor.dez1)
            j3pAffiche(tabco1[0][0], null, '$' + affMembre(stor.a, stor.b) + '$')
            j3pAffiche(tabco1[0][1], null, '&nbsp;$=$&nbsp;')
            j3pAffiche(tabco1[0][2], null, '$0$')
            if (stor.b.num !== 0) {
              j3pAffiche(tabco1[1][0], null, '$' + affMembre(stor.a, { num: 0, den: 1 }) + '$')
              j3pAffiche(tabco1[1][1], null, '&nbsp;$=$&nbsp;')
              j3pAffiche(tabco1[1][2], null, '$' + affMembre({ num: 0, den: 1 }, { num: -stor.b.num, den: stor.b.den }) + '$')
            }
            if (!fracEgal(stor.a, { num: 1, den: 1 })) {
              j3pAffiche(tabco1[2][0], null, '$x$')
              j3pAffiche(tabco1[2][1], null, '&nbsp;$=$&nbsp;')
              const rep1 = { num: -stor.b.num * stor.a.den, den: stor.b.den * stor.a.num }
              const pg = j3pPGCD(rep1.num, rep1.den, { negativesAllowed: true })
              if (j3pArrondi(rep1.den / pg, 6) === 1 || j3pArrondi(rep1.den / pg, 6) === -1) {
                j3pAffiche(tabco1[2][2], null, '$' + j3pArrondi(rep1.num / rep1.den, 0) + '$')
              } else {
                const signe = (rep1.num / rep1.den < 0) ? '-' : ''
                j3pAffiche(tabco1[2][2], null, '$' + signe + '\\frac{' + Math.abs(j3pArrondi(rep1.num / pg, 6)) + '}{' + Math.abs(j3pArrondi(rep1.den / pg, 6)) + '}$')
              }
            }
          }
          if (stor.zone2.isOk() === false) {
            const tabco1 = addDefaultTable(stor.tab[0][3], 3, 3)
            modif(tabco1)
            tabco1[0][0].parentNode.parentNode.style.color = me.styles.colorCorrection
            tabco1[0][0].parentNode.parentNode.style.verticalAlign = 'top'
            tabco1[0][0].style.textAlign = 'right'
            tabco1[1][0].style.textAlign = 'right'
            tabco1[2][0].style.textAlign = 'right'
            tabco1[0][2].style.textAlign = 'left'
            tabco1[1][2].style.textAlign = 'left'
            tabco1[2][2].style.textAlign = 'left'
            tabco1[0][0].parentNode.parentNode.classList.add('correction')
            stor.pourCo7.push(tabco1[0][0].parentNode.parentNode)
            stor.pourDezCo7.push(stor.dez2)
            j3pAffiche(tabco1[0][0], null, '$' + affMembre(stor.c, stor.d) + '$')
            j3pAffiche(tabco1[0][1], null, '&nbsp;$=$&nbsp;')
            j3pAffiche(tabco1[0][2], null, '$0$')
            j3pAffiche(tabco1[1][0], null, '$' + affMembre(stor.c, { num: 0, den: 1 }) + '$')
            j3pAffiche(tabco1[1][1], null, '&nbsp;$=$&nbsp;')
            j3pAffiche(tabco1[1][2], null, '$' + affMembre({ num: 0, den: 1 }, { num: -stor.d.num, den: stor.d.den }) + '$')
            if (!fracEgal(stor.c, { num: 1, den: 1 })) {
              j3pAffiche(tabco1[2][0], null, '$x$')
              j3pAffiche(tabco1[2][1], null, '&nbsp;$=$&nbsp;')
              const rep1 = { num: -stor.d.num * stor.c.den, den: stor.d.den * stor.c.num }
              const pg = j3pPGCD(rep1.num, rep1.den, { negativesAllowed: true })
              if (j3pArrondi(rep1.den / pg, 6) === 1 || j3pArrondi(rep1.den / pg, 6) === -1) {
                j3pAffiche(tabco1[2][2], null, '$' + j3pArrondi(rep1.num / rep1.den, 0) + '$')
              } else {
                const signe = (rep1.num / rep1.den < 0) ? '-' : ''
                j3pAffiche(tabco1[2][2], null, '$' + signe + '\\frac{' + Math.abs(j3pArrondi(rep1.num / pg, 6)) + '}{' + Math.abs(j3pArrondi(rep1.den / pg, 6)) + '}$')
              }
            }
          }
          break
      }
    }
    if (yaexplik) stor.lesdiv.explications.classList.add('explique')
  }

  function yareponse () {
    if (me.isElapsed) return true
    if (stor.etapeEncours === 1) {
      if (!stor.l1.changed) {
        stor.l1.focus()
        return false
      }
      if (!stor.l2.changed) {
        stor.l2.focus()
        return false
      }
    }
    if (stor.etapeEncours === 2) {
      if (stor.zone1.reponse() === '') {
        stor.zone1.focus()
        return false
      }
      if (stor.zone2.reponse() === '') {
        stor.zone2.focus()
        return false
      }
    }
    if (stor.etapeEncours === 0) {
      if (!stor.listFin.changed) {
        stor.listFin.focus()
        return false
      }
      if (stor.zone1) {
        if (stor.zone1.reponse() === '') {
          stor.zone1.focus()
          return false
        }
      }
      if (stor.zone2) {
        if (stor.zone2.reponse() === '') {
          stor.zone2.focus()
          return false
        }
      }
    }
    return true
  }
  function metvert () {
    if (stor.etapeEncours === 1) {
      stor.l1.corrige(true)
      stor.l2.corrige(true)
    }
    if (stor.etapeEncours === 2) {
      stor.zone1.corrige(true)
      stor.zone2.corrige(true)
    }
    if (stor.etapeEncours === 0) {
      stor.listFin.corrige(true)
      if (stor.zone1) {
        stor.zone1.corrige(true)
      }
      if (stor.zone2) {
        stor.zone2.corrige(true)
      }
    }
  }
  function disaTout () {
    if (stor.etapeEncours === 1) {
      stor.l1.disable()
      stor.l2.disable()
    }
    if (stor.etapeEncours === 2) {
      stor.zone1.disable()
      stor.zone2.disable()
      stor.brouillon2.disable()
      stor.brouillon1.disable()
    }
    if (stor.etapeEncours === 0) {
      stor.listFin.disable()
      if (stor.zone1) {
        stor.zone1.disable()
      }
      if (stor.zone2) {
        stor.zone2.disable()
      }
    }
  }
  function barrelesfo () {
    if (stor.etapeEncours === 1) {
      if (stor.l1.isOk() === false) {
        stor.l1.barre()
      }
      if (stor.l2.isOk() === false) {
        stor.l2.barre()
      }
    }
    if (stor.etapeEncours === 2) {
      if (stor.zone1.isOk() === false) {
        stor.zone1.barre()
      }
      if (stor.zone2.isOk() === false) {
        stor.zone2.barre()
      }
    }
    if (stor.etapeEncours === 0) {
      if (stor.listFin.isOk() === false) {
        stor.listFin.barre()
      }
      if (stor.zone1) {
        if (stor.zone1.isOk() === false) {
          stor.zone1.barre()
        }
      }
      if (stor.zone2) {
        if (stor.zone2.isOk() === false) {
          stor.zone2.barre()
        }
      }
    }
  }

  switch (this.etat) {
    case 'enonce':
      // code exécuté au lancement de la section
      if (this.debutDeLaSection) {
        initSection()
      } else {
        enonceMain()
      }
      break // case "enonce":

    case 'correction':
      // On teste si une réponse a été saisie
      stor.lesdiv.explications.classList.remove('explique')
      if (!yareponse()) {
        // ici on donne le message à afficher si une ou plusieurs zone(s) n’est pas complétée (tout d’abord on met en rouge : styles.cfaux)
        stor.lesdiv.correction.style.color = this.styles.cfaux
        stor.lesdiv.correction.innerHTML = reponseIncomplete
        this.afficheBoutonValider()
      } else {
        // Bonne réponse
        if (isRepOk()) {
          if (stor.etapeEncours === 1) {
            this.score = j3pArrondi(this.score + 0.5, 1)
          }
          if (stor.etapeEncours === 2) {
            this.score = j3pArrondi(this.score + 2, 1)
          }
          if (stor.etapeEncours === 0) {
            this.score = j3pArrondi(this.score + 0.5, 1)
          }
          stor.lesdiv.correction.style.color = this.styles.cbien
          stor.lesdiv.correction.innerHTML = cBien

          metvert()
          disaTout()

          this.typederreurs[0]++
          this.cacheBoutonValider()
          this.etat = 'navigation'
          this.sectionCourante()
        } else {
          // Pas de bonne réponse
          stor.lesdiv.correction.style.color = this.styles.cfaux

          // A cause de la limite de temps :
          if (this.isElapsed) { // limite de temps
            this._stopTimer()

            affcofo(true)

            stor.lesdiv.correction.innerHTML = tempsDepasse
            this.typederreurs[10]++
            this.cacheBoutonValider()
            /*
                         *   RECOPIER LA CORRECTION ICI !
                         */
            this.etat = 'navigation'
            this.sectionCourante()
          } else {
            stor.lesdiv.correction.innerHTML = cFaux

            if (this.essaiCourant < this.donneesSection.nbchances && stor.etapeEncours === 2) {
              stor.lesdiv.correction.innerHTML += '<br>' + essaieEncore
              affcofo(false)

              this.typederreurs[1]++
              // indication éventuelle ici
            } else {
              this._stopTimer()
              this.cacheBoutonValider()

              affcofo(true)

              this.typederreurs[2]++
              this.etat = 'navigation'
              this.sectionCourante()
            }
          }
        }
      }
      // Obligatoire
      this.finCorrection()
      break // case "correction":

    case 'navigation':

      if (this.sectionTerminee()) {
        // On détermine la phrase d’état renvoyée par la section
        this.parcours.pe = (this.donneesSection.nbetapes * this.score) / this.donneesSection.nbitems
        this.afficheBoutonSectionSuivante()
        this.focus('sectioncontinuer')
      } else {
        this.etat = 'enonce'
        this.afficheBoutonSuite()
        this.focus('boutoncontinuer')
      }
      // Obligatoire
      this.finNavigation()

      break // case "navigation":
  }
}
