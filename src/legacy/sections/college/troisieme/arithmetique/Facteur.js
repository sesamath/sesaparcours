class Facteur {
  constructor (val, fac1, fac2, id, coul) {
    this.valeur = val
    this.facteur1 = fac1
    this.facteur2 = fac2
    this.id = id
    this.coul = coul
  }

  nett () {
    if (this.valeur === -1) {
      this.valeur = 0
    } else if (this.facteur1 != null) {
      this.facteur1.nett()
      this.facteur2.nett()
    }
  }

  suppr () {
    this.destroy(1, this.length())
  }

  destroy (niv, rang) {
    if (niv === rang - 1) {
      this.facteur1 = null
      this.facteur2 = null
    } else {
      if (this.facteur1 != null) {
        this.facteur1.destroy(niv + 1, rang)
      }
      if (this.facteur2 != null) {
        this.facteur2.destroy(niv + 1, rang)
      }
    }
  }

  length () {
    let long = 1
    if (!this.estlast()) {
      long = long + Math.max(this.facteur1.length(), this.facteur2.length())
    }
    return long
  }

  profaux () {
    let tabresult = []
    if (!this.estlast()) {
      if (this.valeur !== this.facteur1.valeur * this.facteur2.valeur) {
        this.facteur1.coul = -1
        this.facteur2.coul = -1
        tabresult.push([this.valeur, this.facteur1.id, this.facteur1.valeur, this.facteur2.id, this.facteur2.valeur])
      } else {
        this.facteur1.coul = 1
        this.facteur2.coul = 1
      }
      let cam = this.facteur1.profaux()
      if (cam.length !== 0) {
        tabresult = tabresult.concat(cam)
      }
      cam = this.facteur2.profaux()
      if (cam.length !== 0) {
        tabresult = tabresult.concat(cam)
      }
    }
    return tabresult
  }

  decomp (id) {
    if (this.id === id) {
      this.coul = 0
      this.facteur1 = new Facteur(-1, null, null, id + this.valeur + id + 1, 0)
      this.facteur2 = new Facteur(-1, null, null, id + this.valeur + id + 2, 0)
      return true
    }
    if (this.facteur1 != null) {
      if (this.facteur1.decomp(id)) return true
      if (this.facteur2.decomp(id)) return true
    }
    return false
  }

  modif (id, val) {
    if (this.id === id) {
      this.valeur = val
      this.coul = 0
      return true
    }
    if (this.facteur1 != null) {
      if (this.facteur1.modif(id, val)) return true
      if (this.facteur2.modif(id, val)) return true
    }
    return false
  }

  modifcoul (id, couleur) {
    if (this.id === id) {
      this.coul = couleur
      return true
    }
    if (this.facteur1 != null) {
      if (this.facteur1.modifcoul(id, couleur)) return true
      if (this.facteur2.modifcoul(id, couleur)) return true
    }
    return false
  }

  estlast () {
    return ((this.facteur1 == null) && (this.facteur2 == null))
  }

  faistab () {
    let facteurs = []
    facteurs[0] = [this]
    let ok = this.estlast()
    while (!ok) {
      facteurs = this.colletabenfant(facteurs)
      ok = true
      for (let i = 0; i < facteurs[facteurs.length - 1].length; i++) {
        ok = ok && facteurs[facteurs.length - 1][i].estlast()
      }
    }

    /// construit val à partir de facteurs
    const col = []
    const val = []
    const id = []
    const coul = []
    for (let i = 0; i < facteurs.length; i++) {
      val.push([])
      col.push([])
      id.push([])
      coul.push([])
      for (let j = 0; j < facteurs[i].length; j++) {
        val[i].push(facteurs[i][j].valeur)
        col[i].push(facteurs[i][j].colspan())
        id[i].push(facteurs[i][j].id)
        coul[i].push(facteurs[i][j].coul)
      }
    }
    return { val, col, id, coul }
  }

  colletabenfant (facteurs) {
    facteurs.push([])
    for (let i = 0; i < facteurs[facteurs.length - 2].length; i++) {
      if (facteurs[facteurs.length - 2][i].estlast()) {
        facteurs[facteurs.length - 1].push(facteurs[facteurs.length - 2][i])
      } else {
        facteurs[facteurs.length - 1].push(facteurs[facteurs.length - 2][i].facteur1)
        facteurs[facteurs.length - 1].push(facteurs[facteurs.length - 2][i].facteur2)
      }
    }
    return facteurs
  }

  colspan () {
    return this.estlast() ? 1 : this.facteur1.colspan() + this.facteur2.colspan() + 1
  }
}

export default Facteur
