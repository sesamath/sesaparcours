import { j3pClone, j3pAjouteBouton, j3pArrondi, j3pBarre, j3pDetruit, j3pEmpty, j3pGetRandomInt, j3pAddElt } from 'src/legacy/core/functions'
import BulleAide from 'src/legacy/outils/bulleAide/BulleAide'
import { initSectionDecompose, textes } from 'src/legacy/sections/college/troisieme/arithmetique/sectiondecompose01'
import { j3pAffiche } from 'src/lib/mathquill/functions'
import ZoneStyleMathquill2 from 'src/legacy/outils/zoneStyleMathquill/ZoneStyleMathquill2'
import ZoneStyleMathquill1 from 'src/legacy/outils/zoneStyleMathquill/ZoneStyleMathquill1'
import { addDefaultTable } from 'src/legacy/themes/table'
import { j3pCreeFenetres, j3pAfficheCroixFenetres, j3pToggleFenetres, j3pDetruitFenetres } from 'src/legacy/core/functionsJqDialog'
import textesGeneriques from 'src/lib/core/textes'

const { cBien, cFaux, essaieEncore, regardeCorrection, reponseIncomplete, tempsDepasse } = textesGeneriques

/**
 * Ajouter une description ici
 * @fileOverview
 * @author Tommy Barroy <t.barroy@laposte.net>
 * @since 2017-10
*/

export const params = {
  outils: ['calculatrice', 'mathquill'],
  /**
   * Ce qui est paramétrable dans l’éditeur de graphe
   * @type {LegacySectionParamsParametres}
   */
  parametres: [
    ['nbrepetitions', 5, 'entier', 'Nombre de répétitions de la section'],
    ['limite', 0, 'entier', 'Temps disponible par répétition, en secondes'],
    ['nbchances', 2, 'entier', 'Nombre d’essais par répétition'],
    ['Reduction', true, 'boolean', '<u>true</u>: Il faut écire la décomposition sous forme réduite en deuxième étape.'],
    ['borne_facteur', 29, 'entier', 'Valeur max des facteurs (entre 7 et 97)'],
    ['Min_nb_facteurs', 3, 'entier', 'Valeur minimum du nombre de facteurs (supérieure à 3)'],
    ['Max_nb_facteurs', 7, 'entier', 'Valeur minimum du nombre de facteurs (inférieur à 7)'],
    ['CubeMax', true, 'boolean', 'Les exposants ne dépassent pas 3'],
    ['difficulte', 'Progressive', 'liste', 'Gestion de la progression (entre min et max)', ['Aléatoire', 'Progressive']],
    ['PetitsFacteurs', true, 'boolean', '<u>true</u>: 3 facteurs sont toujours petits.'],
    ['Ordre', true, 'boolean', 'Les diviseurs doivent être testés par ordre croissant'],
    ['Aide', true, 'boolean', 'La liste des nombres premiers inférieurs à la borne est donnée.'],
    ['theme', 'standard', 'liste', 'Modèle pour l’affichage', ['standard', 'zonesAvecImageDeFond']]

  ],
  // dans le cas d’une section QUANTITATIVE
  pe: [
    { pe_1: 'nombres premiers' },
    { pe_2: 'puissances' }
  ]
}

/**
 * section decompose02
 * @this {Parcours}
 */
export default function main () {
  const me = this
  const prefixe = 'decompose02'
  const ds = me.donneesSection
  const stor = me.storage

  function vir () {
    for (let i = stor.MenuContextuel.length - 1; i > -1; i--) {
      stor.MenuContextuel[i].destroy()
      stor.MenuContextuel.pop()
    }
  }
  function decomp (id) {
    vir()
    cons2()
    stor.montab0.decomp(id)
    const xy = stor.montab0.faistab()
    stor.montab = xy.val
    stor.montab2 = xy.col
    stor.montab6 = xy.id
    stor.montab12 = xy.coul
    affichetab()
  }
  function cons2 () {
    vir()
    let nbfirst = 0
    for (let i = 0; i < stor.montab[stor.montab.length - 1].length; i++) {
      if (stor.montab[stor.montab.length - 1][i] === -1) {
        const cont = stor.listezone[nbfirst].reponse()
        nbfirst++
        if (cont !== '') { stor.montab0.modif(stor.montab6[stor.montab.length - 1][i], parseFloat(cont.replace(/ /g, ''))) }
      }
    }
  }
  function action2 (i, j, nb) {
    /// modification exposant
    if (nb === 1) {
      vir()
      cons2()

      stor.montab0.modif(stor.montab6[i][j], -1)
      if (stor.montab6[i][j][stor.montab6[i][j].length - 1] === '1') { stor.montab0.modifcoul(stor.montab6[i][j].substring(0, (stor.montab6[i][j].length - 1)) + '2', 0) } else { stor.montab0.modifcoul(stor.montab6[i][j].substring(0, (stor.montab6[i][j].length - 1)) + '1', 0) }
      const xy = stor.montab0.faistab()
      stor.montab = xy.val
      stor.montab2 = xy.col
      stor.montab6 = xy.id
      stor.montab12 = xy.coul
      affichetab()
    }
    if (nb === 0) {
      decomp(stor.montab6[i][j])
    }
  }
  function affichetab (bool1) {
    stor.zoneRep = undefined
    let dou = stor.lesdiv.expliK
    if (!bool1) {
      if (stor.zoneRest !== undefined) stor.zoneRest.disable()
      if (stor.zoneDiv !== undefined) stor.zoneDiv.disable()
      j3pEmpty(stor.lesdiv.zoneRep)
      dou = stor.lesdiv.zoneRep
    }
    const verver = addDefaultTable(dou, 1, 6)
    const tabtra = addDefaultTable(verver[0][0], 10, 2)
    for (let i = 0; i < tabtra.length; i++) {
      tabtra[i][0].style.borderRight = '1px solid black'
    }
    for (let i = 0; i < stor.listeRest.length; i++) {
      j3pAffiche(tabtra[i][0], null, '$' + stor.listeRest[i] + '$&nbsp;')
    }
    if (!bool1) stor.zoneRest = new ZoneStyleMathquill1(tabtra[stor.listeRest.length][0], { restric: '0123456789', enter: verif })
    for (let i = 0; i < stor.listDvi.length; i++) {
      j3pAffiche(tabtra[i][1], null, '&nbsp;$' + stor.listDvi[i] + '$')
    }
    if (!bool1) stor.zoneDiv = new ZoneStyleMathquill1(tabtra[stor.listDvi.length][1], { restric: '0123456789', enter: verif })
    verver[0][1].style.width = '50px'
    if (!bool1) {
      verver[0][5].style.width = '10px'
      verver[0][3].style.width = '50px'
      j3pAjouteBouton(verver[0][2], 'hkdfhdkql', '', 'Valider cette étape', verif)
      j3pAjouteBouton(verver[0][4], 'hkdfdsfdsfhdkql', '', 'Conclure', verifConcl)
      stor.avir1 = verver[0][2]
      stor.avir2 = verver[0][4]
    } else {
      stor.dou = verver[0][2]
    }
  }
  function poseFin1 () {
    stor.avir1 = stor.avir2 = undefined
    if (stor.zoneRest !== undefined) stor.zoneRest.disable()
    if (stor.zoneDiv !== undefined) stor.zoneDiv.disable()
    j3pEmpty(stor.lesdiv.zoneRep)
    const verver = addDefaultTable(stor.lesdiv.zoneRep, 1, 3)
    const tabtra = addDefaultTable(verver[0][0], 10, 2)
    for (let i = 0; i < tabtra.length; i++) {
      tabtra[i][0].style.borderRight = '1px solid black'
    }
    for (let i = 0; i < stor.listeRest.length; i++) {
      j3pAffiche(tabtra[i][0], null, '$' + stor.listeRest[i] + '$&nbsp;')
    }
    for (let i = 0; i < stor.listDvi.length; i++) {
      j3pAffiche(tabtra[i][1], null, '&nbsp;$' + stor.listDvi[i] + '$')
    }
    const tabRep = addDefaultTable(verver[0][2], 1, 2)
    j3pAffiche(tabRep[0][0], null, '$' + stor.nombre + ' = $&nbsp;')
    stor.zoneRep = new ZoneStyleMathquill2(tabRep[0][1], { id: 'huifdffffdssfdsdiu', restric: '0123456789*', enter: function () { me.sectionCourante() } })
    verver[0][1].style.width = '50px'
  }
  function verif () {
    j3pEmpty(stor.lesdiv.expliK2)
    stor.lesdiv.expliK2.classList.remove('explique')
    if (stor.zoneRest.reponse() === '') {
      stor.zoneRest.focus()
      affIncomp()
      return
    }
    if (stor.zoneDiv.reponse() === '') {
      stor.zoneDiv.focus()
      affIncomp()
      return
    }
    const repRest = parseInt(stor.zoneRest.reponse())
    stor.repDiv = parseInt(stor.zoneDiv.reponse())

    let ok = true

    stor.errDivSpe = (stor.repDiv === 0 || stor.repDiv === 1)
    if (!stor.errDivSpe) {
      // verif div est bien un diviseur
      stor.errEstDiv = stor.listeRest[stor.listeRest.length - 1] % stor.repDiv !== 0
      if (!stor.errEstDiv) {
        // si ordre verif c’est bien le plus petit dans l’ordre
        if (ds.Ordre) {
          stor.errOrdre = stor.repDiv !== plusPetitDiv(stor.listeRest[stor.listeRest.length - 1])
          if (stor.errOrdre) {
            ok = false
            stor.zoneDiv.corrige(false)
          }
        }
        // verif quotient reste est bon
        stor.errRest = false
        if (stor.listeRest[stor.listeRest.length - 1] / stor.repDiv !== repRest) {
          stor.errRest = true
          ok = false
          stor.zoneRest.corrige(false)
        }
        stor.errPrems = estPasPrems(stor.repDiv)
        if (stor.errPrems) {
          ok = false
          stor.zoneDiv.corrige(false)
        }
      } else {
        ok = false
        stor.zoneDiv.corrige(false)
      }
    } else {
      ok = false
      stor.zoneDiv.corrige(false)
      stor.parki = stor.repDiv
    }

    if (ok) {
      stor.listeRest.push(repRest)
      stor.listDvi.push(stor.repDiv)
      affichetab()
    } else {
      stor.nbchances--
      if (stor.nbchances === 0) {
        me.essaiCourant = ds.nbchances
        me.sectionCourante()
      } else {
        affcofo(false)
      }
    }

    // si pas ok dis et choisi
  }
  function verifConcl () {
    j3pEmpty(stor.lesdiv.expliK2)
    stor.lesdiv.expliK2.classList.remove('explique')
    if (stor.zoneRest.reponse() !== '') {
      stor.zoneRest.focus()
      affIncompConcl()
      return
    }
    if (stor.zoneDiv.reponse() !== '') {
      stor.zoneDiv.focus()
      affIncompConcl()
      return
    }
    const dern = stor.listeRest[stor.listeRest.length - 1]
    stor.errPasFin = (dern !== 1 && estPasPrems(dern))

    if (stor.errPasFin) {
      stor.nbchances--
      if (stor.nbchances === 0) {
        me.sectionCourante()
      } else {
        affcofo(false)
      }
    } else {
      poseFin1()
    }
  }
  function estPasPrems (n) {
    if (n === 0 || n === 1) return false
    for (let i = 2; i <= Math.sqrt(n); i++) {
      if (n % i === 0) return true
    }
    return false
  }
  function plusPetitDiv (n) {
    if (n < 2) return 1
    for (let i = 2; i < n + 1; i++) {
      if (n % i === 0) return i
    }
  }
  function affIncomp () {
    stor.lesdiv.correction.style.color = me.styles.cfaux
    stor.lesdiv.correction.innerHTML = reponseIncomplete
  }
  function affIncompConcl () {
    stor.lesdiv.correction.style.color = me.styles.cfaux
    stor.lesdiv.correction.innerHTML = 'Il faut valider l’étape <br> en cours avant de valider !'
  }
  function bonneReponse () {
    // cette fonction renvoie true ou false si du moins une réponse a été saisie
    // (dans le cas contraire, elle n’est pas appelée
    // elle est appelée dans la partie "correction" de la section
    stor.errEcr = false
    stor.errYatout = false
    stor.errTrop = false
    if (stor.encours === 'Decomp') {
      if (stor.zoneRep !== undefined) {
        const rrrep = stor.zoneRep.reponse()
        const rety = rrrep.split('\\times')
        for (let i = 0; i < rety.length; i++) {
          rety[i] = rety[i].replace(/ /g, '')
          if (rety[i] === '') {
            stor.errEcr = true
            stor.zoneRep.corrige(false)
            return false
          }
          rety[i] = parseInt(rety[i])
        }

        // verif ya tout
        const vyat = j3pClone(stor.listeNb)
        const cprety = j3pClone(rety)
        for (let i = 0; i < vyat.length; i++) {
          let okya = false
          for (let j = 0; j < cprety.length; j++) {
            if (cprety[j] === vyat[i]) {
              okya = true
              cprety.splice(j, 1)
              break
            }
          }
          if (!okya) {
            stor.errYatout = true
            stor.zoneRep.corrige(false)
            stor.leq = vyat[i]
            return false
          }
        }
        if (rety.length > vyat.length) {
          stor.errTrop = true
          stor.zoneRep.corrige(false)
          return false
        }
        return true
      }
      return stor.repEl
    } else {
      const machainesplit = stor.zonem.reponse().split('\\times')

      stor.montab4 = []
      stor.montab5 = []
      for (let i = 0; i < machainesplit.length; i++) {
        if (machainesplit[i].indexOf('^') === -1) {
          stor.montab4[i] = parseInt(machainesplit[i].replace(/ /g, ''))
          stor.montab5[i] = 1
        } else {
          stor.montab4[i] = parseInt(machainesplit[i].substring(0, machainesplit[i].indexOf('^')).replace(/ /g, ''))
          stor.montab5[i] = parseInt(machainesplit[i].substring(machainesplit[i].indexOf('^') + 1).replace(/{/g, '').replace(/}/g, '').replace('^', '').replace(/ /g, ''))
        }
      }

      let cbon = true
      stor.facteurdouble = []
      stor.facteurmanquant = []
      stor.exposantfaux = []
      stor.facteurentrop = []

      // teste pas de facteur en double
      for (let i = 0; i < stor.montab4.length; i++) {
        for (let j = 1; j < stor.montab4.length - i; j++) {
          if (stor.montab4[i] === stor.montab4[j + i]) {
            cbon = false
            stor.facteurdouble.push(stor.montab4[i])
          }
        }
      }
      // teste presence facteur
      for (let i = 0; i < stor.listeNb.length; i++) {
        let ya = false
        for (let j = 0; j < stor.montab4.length; j++) {
          if (stor.montab4[j] === stor.listeNb[i]) { ya = true }
        }
        if (!ya) {
          cbon = false
          stor.facteurmanquant.push(stor.listeNb[i])
        }
      }
      // test facteur en trop
      for (let i = 0; i < stor.montab4.length; i++) {
        if (stor.listeNb.indexOf(stor.montab4[i]) === -1) {
          cbon = false
          stor.facteurentrop.push(stor.montab4[i])
        }
      }
      // test bon exposant
      const occurrences = stor.listeNb.reduce(function (obj, item) {
        obj[item] = (obj[item] || 0) + 1
        return obj
      }, {})
      for (let i = 0; i < stor.montab4.length; i++) {
        if (stor.montab5[i] !== occurrences[stor.montab4[i]]) cbon = false
      }

      if (!cbon) { stor.zonem.corrige(false) }
      return cbon
    }
  }

  function yareponse () {
    if (stor.encours === 'Decomp') {
      if (stor.zoneRep !== undefined) {
        if (stor.zoneRep.reponse() === '') {
          stor.zoneRep.focus()
          return false
        }
      }
      return true
    } else {
      if (me.isElapsed) return true
      if (stor.zonem.reponse() === '') {
        stor.zonem.focus()
        return false
      } else {
        return true
      }
    }
  }
  function boutonfen () {
    j3pToggleFenetres('Calculatrice')
  }
  function boutonaide () {
    j3pToggleFenetres('ListeMmB')
  }

  function affcofo (bool) {
    j3pEmpty(stor.lesdiv.expliK2)
    stor.lesdiv.expliK2.classList.remove('explique')
    if (stor.errDivSpe) {
      stor.yaexplik = true
      j3pAffiche(stor.lesdiv.expliK2, null, 'On ne divise pas par $' + stor.repDiv + '$ !\n')
    }
    if (stor.errOrdre) {
      stor.yaexplik = true
      j3pAffiche(stor.lesdiv.expliK2, null, 'Il existe un diviseur premier plus petit !\n')
    }
    if (stor.errPrems) {
      stor.yaexplik = true
      j3pAffiche(stor.lesdiv.expliK2, null, 'Tu dois forcément diviser en utilisant des nombres premiers !\n')
    }
    if (stor.errRest) {
      stor.yaexplik = true
      j3pAffiche(stor.lesdiv.expliK2, null, 'Le quotient est incorrect !\n')
    }
    if (stor.errYatout) {
      stor.yaexplik = true
      j3pAffiche(stor.lesdiv.expliK2, null, 'Je ne retrouve pas le facteur $' + stor.leq + '$ !\n')
    }
    if (stor.errTrop) {
      stor.yaexplik = true
      j3pAffiche(stor.lesdiv.expliK2, null, 'Il y a trop de facteurs !\n')
    }
    if (stor.errEcr) {
      stor.yaexplik = true
      j3pAffiche(stor.lesdiv.expliK2, null, 'Il y a une erreur d’écriture !\n')
    }
    if (stor.errEstDiv) {
      stor.yaexplik = true
      j3pAffiche(stor.lesdiv.expliK2, null, '$' + stor.repDiv + '$ n’est pas un diviseur de $' + stor.listeRest[stor.listeRest.length - 1] + '$ !\n')
    }
    if (stor.errPasFin) {
      stor.yaexplik = true
      j3pAffiche(stor.lesdiv.expliK2, null, 'La décomposition n’est pas terminée ! \n<i>($ ' + stor.listeRest[stor.listeRest.length - 1] + ' $ n’est pas un nombre premier.)</i>\n')
    }
    if (stor.facteurdouble.length > 0) {
      stor.yaexplik = true
      j3pAffiche(stor.lesdiv.expliK2, null, 'Le même facteur ne peut pas apparaître deux fois !\n')
    }
    if (stor.facteurmanquant.length > 0) {
      stor.yaexplik = true
      j3pAffiche(stor.lesdiv.expliK2, null, 'Je ne retrouve pas tous les facteurs attendus !\n')
    }
    if (stor.exposantfaux.length > 0) {
      stor.yaexplik = true
      j3pAffiche(stor.lesdiv.expliK2, null, 'Vérifie bien les exposants !\n')
    }
    if (stor.facteurentrop.length > 0) {
      stor.yaexplik = true
      j3pAffiche(stor.lesdiv.expliK2, null, 'Le facteur $' + stor.facteurentrop[0] + '$ ne fais pas partie de la décomposition !\n')
    }
    if (bool) {
      stor.yaco = true
      if (stor.encours === 'Decomp') {
        if (stor.zoneRest !== undefined) {
          stor.zoneRest.barreIfKo()
          stor.zoneDiv.barreIfKo()
          stor.zoneRest.disable()
          stor.zoneDiv.disable()
        }
        if (stor.zoneRep !== undefined) {
          stor.zoneRep.barreIfKo()
          stor.zoneRep.disable()
        }
        if (stor.avir1 !== undefined) j3pEmpty(stor.avir1)
        if (stor.avir2 !== undefined) j3pEmpty(stor.avir2)
        const koi = deComp()
        affichetab(true)
        j3pAffiche(stor.dou, null, '$' + stor.nombre + ' = ' + koi + '$')
      } else {
        const occurrences = stor.listeNb.reduce(function (obj, item) {
          obj[item] = (obj[item] || 0) + 1
          return obj
        }, {})
        let jj = ''
        const monTableau = Object.keys(occurrences).map(function (cle) {
          return [Number(cle), occurrences[cle]]
        })
        for (let i = 0; i < monTableau.length; i++) {
          jj += ' \\times ' + monTableau[i][0]
          if (monTableau[i][1] > 1) jj += '^{' + monTableau[i][1] + '} '
        }
        jj = jj.replace(' \\times ', '')
        j3pAffiche(stor.lesdiv.expliK, null, '$' + stor.nombre + ' = ' + jj + '$')
        stor.zonem.barre()
        stor.zonem.disable()
      }
    }
    if (stor.yaexplik && ds.theme === 'zonesAvecImageDeFond') {
      stor.lesdiv.expliK2.classList.add('explique')
    }
  }
  function clikclik () {
    const kl = j3pBarre(this)
    kl.style.cursor = 'pointer'
    kl.addEventListener('click', function () { j3pDetruit(this) }, false)
  }
  function deComp () {
    stor.listeNb.sort(function (a, b) { return a - b })
    stor.listDvi = j3pClone(stor.listeNb)
    stor.listeRest = [stor.nombre]
    let dou = stor.nombre
    for (let i = 0; i < stor.listDvi.length; i++) {
      dou = dou / stor.listDvi[i]
      stor.listeRest.push(dou)
    }
    let aret = stor.listeNb[0]
    for (let i = 1; i < stor.listeNb.length; i++) {
      aret += ' \\times ' + stor.listeNb[i]
    }
    return aret
  }
  function nbdeunb (x) {
    let cpt = 0
    for (let i = 0; i < stor.listeNb.length; i++) {
      if (stor.listeNb[i] === x) cpt++
    }
    return cpt
  }
  function yaaumoinsundouble () {
    for (let i = 0; i < stor.listeNb.length; i++) {
      for (let j = i + 1; j < stor.listeNb.length; j++) {
        if (stor.listeNb[i] === stor.listeNb[j]) return true
      }
    }
    return false
  }

  function enonceMain () {
    stor.facteurdouble = []
    stor.facteurmanquant = []
    stor.exposantfaux = []
    stor.facteurentrop = []
    me.videLesZones()

    stor.MenuContextuel = []
    stor.bullaide2 = undefined
    stor.bullaide1 = undefined
    stor.listezone = []
    stor.repEl = false

    if (((me.questionCourante % ds.nbetapes) === 1) || (ds.nbetapes === 1)) {
      me.validOnEnter = false // ex donneesSection.touche_entree

      let nbfact = j3pGetRandomInt(ds.Min_nb_facteurs, ds.Max_nb_facteurs)
      if (ds.difficulte === 'Progressive') {
        nbfact = ds.Min_nb_facteurs + Math.round((me.questionCourante - 1) * (ds.Max_nb_facteurs - ds.Min_nb_facteurs) / (ds.nbrepetitions * ds.nbetapes - 1))
      }
      const nbprems = [2, 3, 5, 7, 11, 13, 17, 19, 23, 29, 31, 37, 41, 43, 47, 53, 59, 61, 67, 71, 73, 79, 83, 89, 97]
      const nbprems1 = [2, 3, 5]
      const nbprems2 = [2, 3, 5, 7, 11]
      const nbprems3 = [2, 3, 5, 7, 11, 13, 17]

      for (let i = nbprems.length - 1; i > -1; i--) {
        if (nbprems[i] > ds.borne_facteur) nbprems.splice(i, 1)
        if (nbprems1[i] > ds.borne_facteur) nbprems1.splice(i, 1)
        if (nbprems2[i] > ds.borne_facteur) nbprems2.splice(i, 1)
        if (nbprems3[i] > ds.borne_facteur) nbprems3.splice(i, 1)
      }
      stor.listeNb = []
      while (stor.listeNb.length < nbfact) {
        let unb
        if (ds.PetitsFacteurs && stor.listeNb.length < 3) {
          switch (stor.listeNb.length) {
            case 0:
              unb = nbprems1[j3pGetRandomInt(0, nbprems1.length - 1)]
              break
            case 1:
              unb = nbprems2[j3pGetRandomInt(0, nbprems2.length - 1)]
              break
            case 2:
              unb = nbprems3[j3pGetRandomInt(0, nbprems3.length - 1)]
              break
          }
        } else {
          unb = nbprems[j3pGetRandomInt(0, nbprems.length - 1)]
        }
        if (ds.CubeMax) {
          if (nbdeunb(unb) < 3) stor.listeNb.push(unb)
        } else {
          stor.listeNb.push(unb)
        }
      }
      if (!yaaumoinsundouble()) {
        stor.listeNb[0] = stor.listeNb[1]
      }
      stor.nombre = 1
      for (let i = 0; i < stor.listeNb.length; i++) stor.nombre *= stor.listeNb[i]

      stor.lesdiv = {}
      stor.lesdiv.conteneur = j3pAddElt(me.zones.MG, 'div', {
        style: me.styles.etendre('toutpetit.enonce', { padding: '10px' })
      })

      stor.lesdiv.cons1 = addDefaultTable(stor.lesdiv.conteneur, 1, 1)[0][0]
      const tt = addDefaultTable(stor.lesdiv.conteneur, 1, 3)
      stor.lesdiv.zoneRep = tt[0][0]
      stor.lesdiv.expliK = tt[0][2]
      tt[0][1].style.width = '20px'
      stor.lesdiv.expliK2 = addDefaultTable(stor.lesdiv.conteneur, 1, 1)[0][0]
      stor.lesdiv.expliK2.style.color = me.styles.cfaux
      stor.lesdiv.expliK.style.color = me.styles.petit.correction.color
      j3pAffiche(stor.lesdiv.cons1, null, '<b>' + textes.consigne1 + '</b>\n',
        {
          a: stor.nombre
        })
      if (ds.Ordre) {
        j3pAffiche(stor.lesdiv.cons1, null, '<i>Dans cet exercice, tu dois trouver les facteurs premiers dans l’ordre croissant.</i>\n')
      }
      stor.listeRest = [stor.nombre]
      stor.listDvi = []
      affichetab()
      stor.lesdiv.calculette = j3pAddElt(me.zones.MD, 'div', {
        style: me.styles.etendre('toutpetit.enonce', { padding: '10px' })
      })
      stor.lesdiv.aidez = j3pAddElt(me.zones.MD, 'div', {
        style: me.styles.etendre('toutpetit.enonce', { padding: '10px' })
      })
      stor.lesdiv.correction = j3pAddElt(me.zones.MD, 'div', {
        style: me.styles.etendre('toutpetit.enonce', { padding: '10px' })
      })
      const untableau = [{ name: 'Calculatrice', title: 'Une calculatrice', left: 600, top: 10 }]
      if (ds.Aide) {
        untableau.push({ name: 'ListeMmB', title: 'Nombres premiers connus', left: 500, top: 10 })
      }
      me.fenetresjq = untableau
      j3pCreeFenetres(me)
      j3pAfficheCroixFenetres('Calculatrice')
      j3pAfficheCroixFenetres('ListeMmB')
      j3pAjouteBouton(stor.lesdiv.calculette, 'BCalculatrice', 'MepBoutons', 'Calculatrice', boutonfen)
      if (ds.Aide) {
        j3pAjouteBouton(stor.lesdiv.aidez, 'Baidee', 'MepBoutons', 'Aide', boutonaide)
        const tyu = addDefaultTable('dialogListeMmB', 10, 10)
        for (let i = 0; i < nbprems.length; i++) {
          j3pAffiche(tyu[Math.trunc((i * 2) / 10)][(i * 2) % 10], null, '$' + nbprems[i] + '$')
          tyu[Math.trunc((i * 2) / 10)][(i * 2) % 10].addEventListener('click', clikclik, false)
          tyu[Math.trunc((i * 2) / 10)][(i * 2) % 10 + 1].style.width = '5px'
          tyu[Math.trunc((i * 2) / 10)][(i * 2) % 10].nb = nbprems[i]
          tyu[Math.trunc((i * 2) / 10)][(i * 2) % 10].style.cursor = 'pointer'
        }
      }
      // Obligatoire
      me.cacheBoutonValider()
      stor.encours = 'Decomp'
      stor.nbchances = ds.nbchances
    }

    if ((((me.questionCourante % ds.nbetapes) === 0) && (ds.nbetapes === 2))) {
      stor.errDivSpe = stor.errOrdre = stor.errPrems = stor.errRest = stor.errYatout = stor.errTrop = stor.errEcr = stor.errEstDiv = stor.errPasFin = false
      stor.lesdiv.conteneur = j3pAddElt(me.zones.MG, 'div', {
        style: me.styles.etendre('toutpetit.enonce', { padding: '10px' })
      })

      stor.lesdiv.cons1 = addDefaultTable(stor.lesdiv.conteneur, 1, 1)[0][0]
      const tt = addDefaultTable(stor.lesdiv.conteneur, 2, 1)
      stor.lesdiv.zoneRep = tt[0][0]
      stor.lesdiv.zoneCoRep = tt[1][0]

      me.validOnEnter = true // ex donneesSection.touche_entree
      stor.encours = 'Red'
      const tt2 = addDefaultTable(stor.lesdiv.cons1, 2, 1)

      stor.lesdiv.cons12 = tt2[0][0]

      j3pAffiche(stor.lesdiv.cons12, null, textes.onsaitque, { decomp: stor.nombre + ' = ' + deComp() })
      stor.lesdiv.cons2 = tt2[1][0]

      j3pAffiche(tt2[1][0], null, '<b><u>' + textes.cons2 + '</u></b>')
      stor.bullaide2 = new BulleAide(tt2[1][0], textes.aide3)

      stor.lesdiv.zoneRep = j3pAddElt(stor.lesdiv.conteneur, 'div')
      stor.cip = addDefaultTable(stor.lesdiv.zoneRep, 2, 2)

      stor.lesdiv.expliK2 = addDefaultTable(stor.lesdiv.conteneur, 1, 1)[0][0]

      stor.lesdiv.expliK = addDefaultTable(stor.lesdiv.conteneur, 1, 1)[0][0]
      stor.lesdiv.expliK2.style.color = me.styles.cfaux
      stor.lesdiv.expliK.style.color = me.styles.petit.correction.color

      j3pAffiche(stor.cip[1][0], null, '$' + stor.nombre + ' = $')
      stor.zonem = new ZoneStyleMathquill2(stor.cip[1][1], {
        id: prefixe + 'mazone',
        limite: 17,
        limitenb: 20,
        restric: '0123456789*^',
        hasAutoKeyboard: true,
        enter: function () { me.sectionCourante() }
      })
      /// ////////////////////

      stor.lesdiv.correction = j3pAddElt(me.zones.MD, 'div', {
        style: me.styles.etendre('toutpetit.enonce', { padding: '10px' })
      })
    }
    stor.lesdiv.cons1.classList.add('enonce')
    stor.lesdiv.zoneRep.classList.add('travail')
    me.zonesElts.MG.classList.add('fond')

    me.finEnonce()
  }

  switch (this.etat) {
    case 'enonce':
      // code exécuté au lancement de la section

      if (this.debutDeLaSection) {
        initSectionDecompose(me)
        stor.lesch = [
          {
            name: 'op1col',
            label: 'Décomposer',
            callback: function (a) {
              action2(a.infos.j, a.infos.i, 0)
            }
          },
          {
            name: 'op2col',
            label: 'Modifier',
            callback: function (a) {
              action2(a.infos.j, a.infos.i, 1)
            }
          }
        ]
      }
      enonceMain()
      break // case "enonce":

    case 'correction':
      // On teste si une réponse a été saisie
      if (!yareponse()) {
        affIncomp()
        return this.finCorrection()
      }
      if (bonneReponse()) {
        // Bonne réponse
        stor.lesdiv.correction.style.color = this.styles.cbien
        stor.lesdiv.correction.innerHTML = cBien

        if (stor.encours === 'Decomp') {
          j3pDetruitFenetres('Calculatrice')
          j3pDetruit('BCalculatrice')
          if (ds.nbetapes === 2) {
            this.score = j3pArrondi(this.score + 1.5, 1)
          } else {
            this.score++
          }
          stor.zoneRep.corrige(true)
          stor.zoneRep.disable()
          /// //
        } else {
          this.score = j3pArrondi(this.score + 0.5, 1)
          j3pDetruit(stor.lesdiv.expliK)
          j3pEmpty(stor.lesdiv.zoneCorrec)
          stor.bullaide2.disable()
          stor.zonem.corrige(true)
          stor.zonem.disable()
          j3pDetruit(stor.cip[0][1])
        }

        this.typederreurs[0]++
        return this.finCorrection('navigation', true)
      }

      // Pas de bonne réponse
      stor.lesdiv.correction.style.color = this.styles.cfaux

      // A cause de la limite de temps :
      if (me.isElapsed) { // limite de temps
        stor.lesdiv.correction.innerHTML = tempsDepasse
        this.typederreurs[10]++
        affcofo(true)

        if (ds.theme === 'zonesAvecImageDeFond') {
          stor.lesdiv.expliK.classList.add('correction')
          if (me.afco) {
            stor.lesdiv.zoneCorrec.classList('explique')
          }
        }
        return this.finCorrection('navigation', true)
      }

      // Réponse fausse sans limite de temps
      stor.lesdiv.correction.innerHTML = cFaux

      if (this.essaiCourant < ds.nbchances) {
        stor.lesdiv.correction.innerHTML += '<br>' + essaieEncore
        affcofo(false)
        this.typederreurs[1]++
        // on reste en correction
        return this.finCorrection()
      }

      // Erreur au dernier essai
      stor.lesdiv.correction.innerHTML += '<br>' + regardeCorrection
      affcofo(true)
      if (ds.theme === 'zonesAvecImageDeFond') {
        stor.lesdiv.expliK.classList.add('correction')
        if (me.afco) {
          stor.lesdiv.zoneCorrec.classList.add('explique')
        }
      }

      this.typederreurs[2]++
      this.finCorrection('navigation', true)
      break // case "correction":

    case 'navigation':
      if (this.sectionTerminee()) {
        // On détermine la phrase d’état renvoyée par la section
        // this.typederreurs[4] c’est pe_2 puissances
        // this.typederreurs[3] c’est pe_1 premiers
        let peisup = ''
        let compt = this.typederreurs[2] / 4
        if (this.typederreurs[3] > compt) {
          peisup = ds.pe_1
          compt = this.typederreurs[3]
        }
        if (this.typederreurs[4] > compt) {
          peisup = ds.pe_2
        }
        this.parcours.pe = peisup
      } else {
        this.etat = 'enonce'
      }
      this.finNavigation()
      break // case "navigation":
  }
}
