import { j3pAddElt, j3pAjouteBouton, j3pArrondi, j3pBarre, j3pDetruit, j3pEmpty, j3pGetRandomBool, j3pGetRandomInt } from 'src/legacy/core/functions'
import BulleAide from 'src/legacy/outils/bulleAide/BulleAide'
import MenuContextuel from 'src/legacy/outils/menuContextuel'
import Facteur from 'src/legacy/sections/college/troisieme/arithmetique/Facteur'
import { j3pAffiche } from 'src/lib/mathquill/functions'
import { addDefaultTable } from 'src/legacy/themes/table'
import ZoneStyleMathquill2 from 'src/legacy/outils/zoneStyleMathquill/ZoneStyleMathquill2'
import { j3pCreeFenetres, j3pAfficheCroixFenetres, j3pToggleFenetres, j3pDetruitFenetres } from 'src/legacy/core/functionsJqDialog'
import textesGeneriques from 'src/lib/core/textes'

const { cBien, cFaux, essaieEncore, regardeCorrection, reponseIncomplete, tempsDepasse } = textesGeneriques

/**
 * Ajouter une description ici
 * @fileOverview
 * @author Tommy Barroy <t.barroy@laposte.net>
 * @since 2017-10
*/

export const params = {
  outils: ['calculatrice', 'mathquill'],
  /**
   * Ce qui est paramétrable dans l’éditeur de graphe
   * @type {LegacySectionParamsParametres}
   */
  parametres: [
    ['nbrepetitions', 5, 'entier', 'Nombre de répétitions de la section'],
    ['limite', 0, 'entier', 'Temps disponible par répétition, en secondes'],
    ['nbchances', 2, 'entier', 'Nombre d’essais par répétition'],
    ['Reduction', true, 'boolean', '<u>true</u>: Il faut écire la décomposition sous forme réduite en deuxième étape.'],
    ['borne_facteur', 29, 'entier', 'Valeur max des facteurs (entre 7 et 97)'],
    ['Min_nb_facteurs', 3, 'entier', 'Valeur minimum du nombre de facteurs (supérieure à 3)'],
    ['Max_nb_facteurs', 7, 'entier', 'Valeur minimum du nombre de facteurs (inférieur à 7)'],
    ['CubeMax', true, 'boolean', 'Les exposants ne dépassent pas 3'],
    ['difficulte', 'Progressive', 'liste', 'Gestion de la progression (entre min et max)', ['Aléatoire', 'Progressive']],
    ['Calculatrice', true, 'boolean', '<u>true</u>: L’élève dispose d’une calculatrice.'],
    // le modèle gère la compatibilité ascendante avec l’ancien paramètre MiseEnPagePlusClaire
    ['theme', 'standard', 'liste', 'Modèle pour l’affichage', ['standard', 'zonesAvecImageDeFond']]
  ],
  // dans le cas d’une section QUANTITATIVE
  pe: [
    { pe_1: 'nombres premiers' },
    { pe_2: 'puissances' }
  ]
}

export const textes = {
  consigne1: 'Nous voulons décomposer le nombre $£a$ en un produit de facteurs premiers.',
  suppr: '🗑 ligne',
  valider: 'Valider la ligne',
  pasprod: '$ £a \\times £b \\neq £c $',
  paspremsing: 'Le facteur £a n’est pas un nombre premier.',
  paspremplur: 'Les facteurs £a ne sont pas des nombres premiers.',
  et: 'et',
  correction: 'Correction :',
  onsaitque: 'Nous savons que : $£{decomp}$.',
  aide0: 'Clique sur le nombre pour le décomposer.',
  aide1: 'Clique sur un facteur pour le décomposer.',
  aide3: 'Un facteur ne peut apparaitre qu’une fois. Il faut utiliser les exposants.',
  cons2: 'Ecris la décomposition sous forme réduite.',
  facteurdouble: 'Le facteur £a ne peut pas apparaitre plusieurs fois.',
  facteurmanquant: 'Le facteur £a manque.',
  facteurentrop: 'Le facteur £a ne fait pas partie de la décomposition.',
  exposantfaux: 'L’exposant du facteur £a est £b .',
  tropgrand: 'trop grand',
  troppetit: 'trop petit'
}

// fct
export function initSectionDecompose (parcours) {
  parcours.validOnEnter = false // ex donneesSection.touche_entree

  // Construction de la page
  parcours.construitStructurePage('presentation1bis')

  const ds = parcours.donneesSection
  ds.Min_nb_facteurs = Math.max(3, ds.Min_nb_facteurs)
  ds.Min_nb_facteurs = Math.min(7, ds.Min_nb_facteurs)
  ds.Max_nb_facteurs = Math.min(7, ds.Max_nb_facteurs)
  ds.Max_nb_facteurs = Math.max(3, ds.Max_nb_facteurs)
  if (ds.Min_nb_facteurs > ds.Max_nb_facteurs) ds.Max_nb_facteurs = ds.Min_nb_facteurs
  ds.borne_facteur = Math.max(7, ds.borne_facteur)

  if (ds.Reduction) {
    parcours.surcharge({ nbetapes: 2 })
  } // sinon ça reste à 1, pas besoin de surcharge()

  parcours.afficheTitre('Décomposer un nombre en facteurs premiers')
}

/**
 * section decompose01
 * @this {Parcours}
 */
export default function main () {
  const me = this
  const ds = me.donneesSection
  const stor = me.storage

  function vir () {
    for (let i = stor.MenuContextuel.length - 1; i > -1; i--) {
      stor.MenuContextuel[i].destroy()
      stor.MenuContextuel.pop()
    }
  }
  function decomp (id) {
    vir()
    cons2()
    stor.montab0.decomp(id)
    const xy = stor.montab0.faistab()
    stor.montab = xy.val
    stor.montab2 = xy.col
    stor.montab6 = xy.id
    stor.montab12 = xy.coul
    affichetab()
  }
  function suppr () {
    vir()
    stor.montab0.suppr()
    const xy = stor.montab0.faistab()
    stor.montab = xy.val
    stor.montab2 = xy.col
    stor.montab6 = xy.id
    stor.montab12 = xy.coul
    affichetab()
  }
  function cons2 () {
    vir()
    let nbfirst = 0
    for (let i = 0; i < stor.montab[stor.montab.length - 1].length; i++) {
      if (stor.montab[stor.montab.length - 1][i] === -1) {
        const cont = stor.listezone[nbfirst].reponse()
        nbfirst++
        if (cont !== '') { stor.montab0.modif(stor.montab6[stor.montab.length - 1][i], parseFloat(cont.replace(/ /g, ''))) }
      }
    }
  }
  function action2 (i, j, nb) {
    /// modification exposant
    if (nb === 1) {
      vir()
      cons2()

      stor.montab0.modif(stor.montab6[i][j], -1)
      if (stor.montab6[i][j][stor.montab6[i][j].length - 1] === '1') { stor.montab0.modifcoul(stor.montab6[i][j].substring(0, (stor.montab6[i][j].length - 1)) + '2', 0) } else { stor.montab0.modifcoul(stor.montab6[i][j].substring(0, (stor.montab6[i][j].length - 1)) + '1', 0) }
      const xy = stor.montab0.faistab()
      stor.montab = xy.val
      stor.montab2 = xy.col
      stor.montab6 = xy.id
      stor.montab12 = xy.coul
      affichetab()
    }
    if (nb === 0) {
      decomp(stor.montab6[i][j])
    }
  }
  function enter (e) {
    if ((e.code === 'NumpadEnter') || (e.code === 'Enter')) {
      cons2()
      const xy = stor.montab0.faistab()
      stor.montab = xy.val
      stor.montab2 = xy.col
      stor.montab6 = xy.id
      stor.montab12 = xy.coul
      affichetab()
    }
  }
  function affichetab () {
    stor.listezone.forEach(el => el.disable())
    stor.listezone = []
    j3pEmpty(stor.lesdiv.zoneRep)
    let clikable = true

    let max = 0
    for (let u = 0; u < stor.montab.length; u++) {
      max = Math.max(max, stor.montab[u].length)
    }
    const tabaf = addDefaultTable(stor.lesdiv.zoneRep, stor.montab.length + 1, max * 2 + 3)
    for (let u = 0; u < stor.montab.length + 1; u++) {
      for (let m = 0; m < 2 * max + 3; m++) {
        tabaf[u][m].style.padding = 0
        tabaf[u][m].style.textAlign = 'center'
      }
    }
    for (let i = 0; i < stor.montab.length; i++) {
      for (let j = 0; j < stor.montab[i].length - 1; j++) {
        if (stor.montab[i][j] === -1) { clikable = false }
        tabaf[i][j * 2 + 1].setAttribute('colspan', stor.montab2[i][j])
        j3pAffiche(tabaf[i][j * 2 + 2], null, '&nbsp;×&nbsp;')
      }
      const gj = stor.montab[i].length - 1
      if (stor.montab[i][gj] === -1) { clikable = false }
      tabaf[i][gj * 2 + 1].setAttribute('colspan', stor.montab2[i][gj])
      for (let x = max * 2 + 2; x > gj * 2 + 3; x--) {
        j3pDetruit(tabaf[i][x])
        tabaf[i].splice(x, 1)
        if (i === stor.montab.length - 1) {
          j3pDetruit(tabaf[i + 1][x])
          tabaf[i + 1].splice(x, 1)
        }
      }
    }
    const gii = stor.montab.length - 1
    for (let j = 0; j < stor.montab[gii].length - 1; j++) {
      tabaf[gii][j * 2 + 2].setAttribute('colspan', stor.montab2[gii][j])
      tabaf[gii + 1][j * 2 + 2].setAttribute('colspan', stor.montab2[gii][j])
    }

    // ajout bouton supprime ligne
    if ((stor.montab.length - 1) !== 0) {
      j3pAjouteBouton(tabaf[tabaf.length - 2][tabaf[tabaf.length - 2].length - 1], suppr, { className: 'MepBoutons', value: textes.suppr })
      if (!clikable) { j3pAjouteBouton(tabaf[tabaf.length - 1][tabaf[tabaf.length - 1].length - 1], function () { enter({ code: 'Enter' }) }, { className: 'MepBoutons', value: textes.valider }) }
    }

    // ajout les onclick et input
    let yafocus = false
    for (let i = 0; i < stor.montab[stor.montab.length - 1].length; i++) {
      if (stor.montab[stor.montab.length - 1][i] !== -1) {
        if (stor.montab.length === 1) {
          stor.MenuContextuel[stor.MenuContextuel.length] = new MenuContextuel(tabaf[stor.montab.length - 1][i * 2 + 1], [stor.lesch[0]], { infos: { i, j: stor.montab.length - 1 } })
        } else {
          stor.MenuContextuel[stor.MenuContextuel.length] = new MenuContextuel(tabaf[stor.montab.length - 1][i * 2 + 1], stor.lesch, { infos: { i, j: stor.montab.length - 1 } })
        }
      } else {
        stor.listezone[stor.listezone.length] = new ZoneStyleMathquill2(tabaf[stor.montab.length - 1][i * 2 + 1], {
          id: 'um' + (stor.montab.length - 1) + 'ma' + i + 'zone',
          limite: 17,
          limitenb: 4,
          restric: '0123456789',
          enter: function () { enter({ code: 'Enter' }) }
        })
        if (!yafocus) {
          yafocus = true
        }
        ///
      }
    }

    for (let i = 0; i < stor.montab.length; i++) {
      j3pAffiche(tabaf[i][0], null, '$' + stor.nombre + ' = $')
      for (let j = 0; j < stor.montab[i].length; j++) {
        if (stor.montab[i][j] !== -1) {
          const hjkl = j3pAffiche(tabaf[i][j * 2 + 1], null, '$' + stor.montab[i][j] + '$')
          if (i === stor.montab.length - 1) {
            for (let uu = 0; uu < hjkl.mqList.length; uu++) hjkl.mqList[uu].style.cursor = 'pointer'
          }
        }
      }
    }

    // ajoutcool
    for (let i = 0; i < stor.montab.length - 1; i++) {
      for (let j = 0; j < stor.montab[i].length; j++) {
        if (stor.montab12[i][j] === -1) { tabaf[i][j * 2 + 1].style.color = me.styles.cfaux }
        if (stor.montab12[i][j] === 1) { tabaf[i][j * 2 + 1].style.color = me.styles.cbien }
      }
    }
    for (let j = 0; j < stor.montab[stor.montab12.length - 1].length; j++) {
      if (stor.montab12[stor.montab12.length - 1][j] === -1) { tabaf[stor.montab12.length - 1][j * 2 + 1].setAttribute('class', 'klikf') }
      if (stor.montab12[stor.montab12.length - 1][j] === 1) { tabaf[stor.montab12.length - 1][j * 2 + 1].setAttribute('class', 'klikb') }
    }

    stor.AcouP = tabaf[0][2]
    if (clikable) {
      tabaf[0][3].innerHTML = (tabaf.length === 2) ? '&nbsp;' + textes.aide0 : '&nbsp;' + textes.aide1
      tabaf[0][3].style.color = me.styles.petit.correction.color
    }

    if (yafocus) {
      setTimeout(() => { stor.listezone[0].focus() }, 10)
    } else {
      me.buttonsElts.valider.focus()
    }

    stor.tabAf = tabaf
  }
  function hexToR (h) { return parseInt((cutHex(h)).substring(0, 2), 16) }
  function hexToG (h) { return parseInt((cutHex(h)).substring(2, 4), 16) }
  function hexToB (h) { return parseInt((cutHex(h)).substring(4, 6), 16) }
  function cutHex (h) { return (h.charAt(0) === '#') ? h.substring(1, 7) : h }
  function bonneReponse () {
    // cette fonction renvoie true ou false si du moins une réponse a été saisie
    // (dans le cas contraire, elle n’est pas appelée
    // elle est appelée dans la partie "correction" de la section
    let i, j, k, buf, phrasebuf, ya
    if (stor.encours === 'Decomp') {
      cons2()
      vir()
      stor.montab0.nett()
      let bouboul = true
      const prodfo = stor.montab0.profaux()
      /// test si les facteurs de la ligne n sont premiers
      let premfo = []
      premfo = []
      for (i = 0; i < stor.montab[stor.montab.length - 1].length; i++) {
        if (!premier(stor.montab[stor.montab.length - 1][i])) {
          bouboul = false
          premfo.push(stor.montab[stor.montab.length - 1][i])
          stor.montab0.modifcoul(stor.montab6[stor.montab.length - 1][i], -1)
        }
      }
      const xy = stor.montab0.faistab()
      stor.montab = xy.val
      stor.montab2 = xy.col
      stor.montab6 = xy.id
      stor.montab12 = xy.coul
      affichetab()

      if (prodfo.length > 0) { bouboul = false }
      j3pEmpty(stor.AcouP)

      for (i = 0; i < stor.montab.length; i++) {
        j3pEmpty(stor.tabAf[i][stor.tabAf[i].length - 2])
      }

      for (let t = 0; t < prodfo.length; t++) {
        let trouve = false
        for (i = 0; i < stor.montab.length; i++) {
          for (j = 0; j < stor.montab[i].length; j++) {
            buf = false
            if (stor.montab6[i][j] === prodfo[t][1]) {
              if (!trouve) { /// affichephrase co
                const aj = buf ? '&nbsp;,&nbsp;' : '&nbsp;'
                j3pAffiche(stor.tabAf[i][stor.tabAf[i].length - 2], null, aj + textes.pasprod,
                  {
                    a: prodfo[t][2],
                    b: prodfo[t][4],
                    c: prodfo[t][0]
                  })
                stor.tabAf[i][stor.tabAf[i].length - 2].style.color = me.styles.petit.correction.color
                trouve = true
              }
            }
          }
        }
      }

      stor.lesdiv.zoneCoRep.innerHTML = ''
      stor.lesdiv.zoneCoRep.classList.remove('explique')
      if (premfo.length > 0) {
        me.typederreurs[3]++
        if (premfo.length > 1) {
          buf = ''
          phrasebuf = textes.paspremplur
          for (k = 0; k < premfo.length - 1; k++) {
            buf = buf + ' ' + premfo[k] + ','
          }
          buf = buf.substring(0, buf.length - 1)

          buf = buf + ' ' + textes.et + ' ' + premfo[k]
        } else {
          buf = premfo[0]
          phrasebuf = textes.paspremsing
        }
        j3pAffiche(stor.lesdiv.zoneCoRep, null, phrasebuf + '\n',
          {
            a: buf
          })
        stor.lesdiv.zoneCoRep.classList.add('explique')
      }

      return [bouboul]
    } else {
      // dans montab4 list facteur
      // dans montab5 liste expo
      const machainesplit = stor.zonem.reponse().split('\\times')

      stor.montab4 = []
      stor.montab5 = []
      for (i = 0; i < machainesplit.length; i++) {
        if (machainesplit[i].indexOf('^') === -1) {
          stor.montab4[i] = parseInt(machainesplit[i].replace(/ /g, ''))
          stor.montab5[i] = 1
        } else {
          stor.montab4[i] = parseInt(machainesplit[i].substring(0, machainesplit[i].indexOf('^')).replace(/ /g, ''))
          stor.montab5[i] = parseInt(machainesplit[i].substring(machainesplit[i].indexOf('^') + 1).replace(/{/g, '').replace(/}/g, '').replace('^', '').replace(/ /g, ''))
        }
      }

      let cbon = true
      const facteurdouble = []
      const facteurmanquant = []
      const exposantfaux = []
      const facteurentrop = []
      // test bon exposant

      for (i = 0; i < stor.montab4.length; i++) {
        ya = true
        switch (stor.montab4[i]) {
          case 2 :
            if (stor.nombrede2 !== stor.montab5[i]) {
              me.typederreurs[4]++
              ya = false
            }
            break
          case 3 :
            if (stor.nombrede3 !== stor.montab5[i]) {
              me.typederreurs[4]++
              ya = false
            }
            break
          case 5 :
            if (stor.nombrede5 !== stor.montab5[i]) {
              me.typederreurs[4]++
              ya = false
            }
            break
          default : if (stor.montab5[i] !== 1) ya = false
        }
        if (!ya) {
          cbon = false
          exposantfaux.push([stor.montab4[i], stor.montab5[i]])
        }
      }
      // teste pas de facteur en double
      for (i = 0; i < stor.montab4.length; i++) {
        for (j = 1; j < stor.montab4.length - i; j++) {
          if (stor.montab4[i] === stor.montab4[j + i]) {
            cbon = false
            facteurdouble.push(stor.montab4[i])
          }
        }
      }
      // teste presence facteur
      if (stor.nombrede2 > 0) {
        ya = false
        for (i = 0; i < stor.montab4.length; i++) {
          if (stor.montab4[i] === 2) { ya = true }
        }
        if (!ya) {
          cbon = false
          facteurmanquant.push(2)
        }
      }
      if (stor.nombrede3 > 0) {
        ya = false
        for (i = 0; i < stor.montab4.length; i++) {
          if (stor.montab4[i] === 3) { ya = true }
        }
        if (!ya) {
          cbon = false
          facteurmanquant.push(3)
        }
      }
      if (stor.nombrede5 > 0) {
        ya = false
        for (i = 0; i < stor.montab4.length; i++) {
          if (stor.montab4[i] === 5) { ya = true }
        }
        if (!ya) {
          cbon = false
          facteurmanquant.push(5)
        }
      }
      ya = false
      for (i = 0; i < stor.montab4.length; i++) {
        if (stor.montab4[i] === stor.nombrede7) { ya = true }
      }
      if (!ya) {
        cbon = false
        facteurmanquant.push(stor.nombrede7)
      }
      // test facteur en trop
      for (i = 0; i < stor.montab4.length; i++) {
        ya = true
        switch (stor.montab4[i]) {
          case 2 :
            if (stor.nombrede2 < 1) { ya = false }
            break
          case 3 :
            if (stor.nombrede3 < 1) { ya = false }
            break
          case 5 :
            if (stor.nombrede5 < 1) { ya = false }
            break
          default: if (stor.montab4[i] !== stor.nombrede7) { ya = false }
        }
        if (!ya) {
          cbon = false
          facteurentrop.push(stor.montab4[i])
        }
      }

      if (!cbon) { stor.zonem.corrige(false) }
      return [cbon, facteurdouble, facteurmanquant, facteurentrop, exposantfaux]
    }
  }
  function premier (nb) {
    let i
    for (i = 2; i <= Math.sqrt(Math.abs(nb)); i++) {
      if (nb % i === 0) return false
    }
    return true
  }
  function yareponse () {
    if (stor.encours === 'Decomp') {
      if (me.isElapsed) {
        for (let i = 0; i < stor.montab.length; i++) {
          for (let j = 0; j < stor.montab[i].length; j++) {
            if (stor.montab[i][j] === -1) { stor.montab[i][j] = 0 }
          }
        }
      }
      return true
    } else {
      if (stor.zonem.reponse() === '') {
        stor.zonem.focus()
        return false
      }
      return true
    }
  }
  function boutonfen () {
    j3pToggleFenetres('Calculatrice')
  }
  function affcofo (repeleve) {
    if (stor.encours === 'Decomp') {
      if (ds.Calculatrice) {
        j3pDetruitFenetres('Calculatrice')
        j3pDetruit('BCalculatrice')
      }
      // vire les nb clikables
      for (let i = 0; i < stor.montab[stor.montab.length - 1].length; i++) {
        if (stor.tabAf[stor.montab.length - 1][i * 2 + 1].getAttribute('class') === 'klikf') { stor.tabAf[stor.montab.length - 1][i * 2 + 1].style.color = me.styles.cfaux } else { stor.tabAf[stor.montab.length - 1][i * 2 + 1].style.color = me.styles.cbien }
      }
      stor.tabAf[stor.montab.length - 1][stor.tabAf[stor.montab.length - 1].length - 1].innerHTML = ''
      stor.tabAf[0][stor.tabAf[0].length - 1].innerHTML = ''
      vir()
      const mu = 'rgb(' + hexToR(me.styles.cfaux) + ', ' + hexToG(me.styles.cfaux) + ', ' + hexToB(me.styles.cfaux) + ')'

      // affiche la co
      let phraserep = stor.nombre + ' = '
      for (let k = 0; k < 4; k++) {
        if (stor.nombrede2 > k) phraserep += ' 2 \\times'
        if (stor.nombrede3 > k) phraserep += ' 3 \\times'
        if (stor.nombrede5 > k) phraserep += ' 5 \\times'
      }
      phraserep += stor.nombrede7

      stor.lesdiv.expliK1.innerHTML = '<b><u>' + textes.correction + '</u></b>'
      j3pAffiche(stor.lesdiv.expliK2, null, '$' + phraserep + '$')
      me.decompok = phraserep
      for (let i = 0; i < stor.montab.length; i++) {
        for (let j = 0; j < stor.montab[i].length; j++) {
          if (stor.tabAf[i][j * 2 + 1].style.color === mu) { j3pBarre(stor.tabAf[i][j * 2 + 1]) }
        }
      }
    } else {
      stor.lesdiv.zoneCorrec.style.backgroundColor = ''
      j3pEmpty(stor.lesdiv.zoneCorrec)
      stor.bullaide2.disable()
      stor.zonem.corrige(false)
      stor.zonem.disable()
      stor.zonem.barre()

      /// /cas des facteurs doubles
      let listedit = []
      stor.afco = false
      for (let i = 0; i < repeleve[1].length; i++) {
        if (listedit.indexOf(repeleve[1][i]) === -1) {
          stor.afco = true
          j3pAffiche(stor.lesdiv.zoneCorrec, null, textes.facteurdouble + '\n',
            {
              a: repeleve[1][i]
            })
          listedit.push(repeleve[1][i])
        }
      }
      // cas des facteurs manquants
      for (let i = 0; i < repeleve[2].length; i++) {
        stor.afco = true
        j3pAffiche(stor.lesdiv.zoneCorrec, null, textes.facteurmanquant + '\n',
          {
            a: repeleve[2][i]
          })
      }
      // cas des facteurs en trop
      listedit = []
      for (let i = 0; i < repeleve[3].length; i++) {
        if (listedit.indexOf(repeleve[3][i]) === -1) {
          stor.afco = true
          j3pAffiche(stor.lesdiv.zoneCorrec, null, textes.facteurentrop + '\n',
            {
              a: repeleve[3][i]
            })
          listedit.push(repeleve[3][i])
        }
      }
      // cas des exposants faux
      for (let i = 0; i < repeleve[4].length; i++) {
        let ya = true
        let buff = textes.tropgrand
        let comp = 0
        switch (repeleve[4][i][0]) {
          case 2 :
            comp = stor.nombrede2
            break
          case 3 :
            comp = stor.nombrede3
            break
          case 5 :
            comp = stor.nombrede5
            break
          default: { ya = false }
        }
        if ((ya) & (comp !== 0)) {
          if (repeleve[4][i][1] < comp) { buff = textes.troppetit }
          stor.afco = true
          j3pAffiche(stor.lesdiv.zoneCorrec, null, textes.exposantfaux + '\n',
            {
              a: repeleve[4][i][0],
              b: buff
            })
        }
      }

      j3pAffiche(stor.lesdiv.expliK2, null, '<b><u>' + textes.correction + '</u></b>\n')
      let phraserep = stor.nombre + ' = '
      let fotimes = false
      if (stor.nombrede2 > 0) {
        phraserep += '2'
        fotimes = true
        if (stor.nombrede2 > 1) phraserep += '^' + stor.nombrede2
      }
      if (stor.nombrede3 > 0) {
        if (fotimes) phraserep += ' \\times '
        phraserep += '3'
        fotimes = true
        if (stor.nombrede3 > 1) phraserep += '^' + stor.nombrede3
      }
      if (stor.nombrede5 > 0) {
        if (fotimes) phraserep += ' \\times '
        phraserep += '5'
        fotimes = true
        if (stor.nombrede5 > 1) phraserep += '^' + stor.nombrede5
      }
      phraserep += ' \\times ' + stor.nombrede7
      j3pAffiche(stor.lesdiv.expliK1, null, '$ ' + phraserep + ' $')
    }
  } // affcofo

  function unnbpremierinfa (j) {
    let i
    const lp = [7, 11, 13, 17, 19, 23, 29, 31, 37, 41, 43, 47, 53, 59, 61, 67, 71, 73, 79, 83, 89, 97]
    for (i = lp.length - 1; i > -1; i--) {
      if (lp[i] > j) lp.splice(i, 1)
    }
    return lp[j3pGetRandomInt(0, lp.length - 1)]
  }

  function enonceMain () {
    me.videLesZones()

    stor.MenuContextuel = []
    stor.bullaide2 = undefined
    stor.bullaide1 = undefined
    stor.listezone = []

    if (((me.questionCourante % ds.nbetapes) === 1) || (ds.nbetapes === 1)) {
      me.validOnEnter = false // ex donneesSection.touche_entree

      let nbfact = j3pGetRandomInt(ds.Min_nb_facteurs, ds.Max_nb_facteurs)
      if (ds.difficulte === 'Progressive') {
        nbfact = ds.Min_nb_facteurs + Math.round((me.questionCourante - 1) * (ds.Max_nb_facteurs - ds.Min_nb_facteurs) / (ds.nbrepetitions * ds.nbetapes - 1))
      }

      stor.nombrede7 = unnbpremierinfa(ds.borne_facteur)
      stor.nombrede2 = 0
      stor.nombrede3 = 0
      stor.nombrede5 = 0
      for (let i = 1; i < nbfact; i++) {
        switch (j3pGetRandomInt(0, 2)) {
          case 0:
            stor.nombrede2++
            if (ds.CubeMax && stor.nombrede2 > 3) {
              stor.nombrede2--
              i--
            }
            break
          case 1:
            stor.nombrede3++
            if (ds.CubeMax && stor.nombrede3 > 3) {
              stor.nombrede3--
              i--
            }
            break
          case 2:
            stor.nombrede5++
            if (ds.CubeMax && stor.nombrede5 > 3) {
              stor.nombrede5--
              i--
            }
            break
        }
      }
      if (ds.Reduction) {
        while (Math.max(stor.nombrede2, stor.nombrede3, stor.nombrede5) < 2) {
          switch (j3pGetRandomInt(0, 2)) {
            case 0: stor.nombrede2++
              if (stor.nombrede3 === 0) { stor.nombrede5-- } else if (stor.nombrede5 === 0) { stor.nombrede3-- } else {
                if (j3pGetRandomBool()) { stor.nombrede5-- } else { stor.nombrede3-- }
              }
              break
            case 1: stor.nombrede3++
              if (stor.nombrede2 === 0) { stor.nombrede5-- } else if (stor.nombrede5 === 0) { stor.nombrede2-- } else {
                if (j3pGetRandomBool()) { stor.nombrede5-- } else { stor.nombrede2-- }
              }
              break
            case 2: stor.nombrede5++
              if (stor.nombrede3 === 0) { stor.nombrede2-- } else if (stor.nombrede2 === 0) { stor.nombrede3-- } else {
                if (j3pGetRandomBool()) { stor.nombrede2-- } else { stor.nombrede3-- }
              }
              break
          }
        }
      }

      stor.nombre = Math.pow(2, stor.nombrede2) * Math.pow(3, stor.nombrede3) * Math.pow(5, stor.nombrede5) * stor.nombrede7

      stor.montab0 = new Facteur(stor.nombre, null, null, 'deb', 0)
      stor.montab = []
      stor.montab2 = []
      const xy = stor.montab0.faistab()
      stor.montab = xy.val
      stor.montab2 = xy.col
      stor.montab6 = xy.id
      stor.montab12 = xy.coul
      stor.montab3 = []
      // stor.montab = [[me.Sectiondecompose01.nombre]];
      stor.montab2 = [[1]]
      stor.montab3.push(stor.montab2)

      stor.lesdiv = {}
      stor.lesdiv.conteneur = j3pAddElt(me.zonesElts.MG, 'div', {
        style: me.styles.etendre('toutpetit.enonce', { padding: '10px' })
      })

      stor.lesdiv.cons1 = addDefaultTable(stor.lesdiv.conteneur, 1, 1)[0][0]
      const tt = addDefaultTable(stor.lesdiv.conteneur, 2, 1)
      stor.lesdiv.zoneRep = tt[0][0]
      stor.lesdiv.zoneCoRep = tt[1][0]
      stor.lesdiv.zoneCoRep.style.color = me.styles.cfaux
      j3pAffiche(stor.lesdiv.cons1, null, textes.consigne1,
        {
          a: stor.nombre
        })

      affichetab()
      if (ds.Calculatrice) {
        stor.lesdiv.calculatrice = j3pAddElt(me.zonesElts.MD, 'div', {
          style: me.styles.etendre('toutpetit.enonce', { padding: '10px' })
        })
        me.fenetresjq = [{ name: 'Calculatrice', title: 'Une calculatrice', left: 600, top: 10 }]
        j3pCreeFenetres(me)
        j3pAfficheCroixFenetres('Calculatrice')
        j3pAjouteBouton(stor.lesdiv.calculatrice, boutonfen, { className: 'MepBoutons', value: 'Calculatrice' })
      }
      stor.lesdiv.correction = j3pAddElt(me.zonesElts.MD, 'div', {
        style: me.styles.etendre('toutpetit.enonce', { padding: '10px' })
      })
      stor.lesdiv.expliK = j3pAddElt(stor.lesdiv.conteneur, 'div')
      stor.lesdiv.expliK1 = j3pAddElt(stor.lesdiv.conteneur, 'div')
      stor.lesdiv.expliK2 = j3pAddElt(stor.lesdiv.conteneur, 'div')
      stor.lesdiv.zoneCorrec = stor.lesdiv.expliK2
      stor.encours = 'Decomp'
    }

    if ((((me.questionCourante % ds.nbetapes) === 0) && (ds.nbetapes === 2))) {
      stor.lesdiv.conteneur = j3pAddElt(me.zonesElts.MG, 'div', {
        style: me.styles.etendre('toutpetit.enonce', { padding: '10px' })
      })

      stor.lesdiv.cons1 = addDefaultTable(stor.lesdiv.conteneur, 1, 1)[0][0]
      const tt = addDefaultTable(stor.lesdiv.conteneur, 2, 1)
      stor.lesdiv.zoneRep = tt[0][0]
      stor.lesdiv.zoneCoRep = tt[1][0]

      me.validOnEnter = true // ex donneesSection.touche_entree
      stor.encours = 'Red'
      stor.lesdiv.cons12 = j3pAddElt(stor.lesdiv.cons1, 'div')

      j3pAffiche(stor.lesdiv.cons12, null, textes.onsaitque, { decomp: me.decompok })
      stor.lesdiv.cons2 = addDefaultTable(stor.lesdiv.cons1, 1, 2)

      j3pAffiche(stor.lesdiv.cons2[0][0], null, '<b><u>' + textes.cons2 + '</u></b>')
      stor.bullaide2 = new BulleAide(stor.lesdiv.cons2[0][1], textes.aide3)

      stor.lesdiv.zoneRep = j3pAddElt(stor.lesdiv.conteneur, 'div')
      stor.cip = addDefaultTable(stor.lesdiv.zoneRep, 2, 2)

      stor.lesdiv.zRep2 = j3pAddElt(stor.lesdiv.zoneRep, 'div')
      stor.lesdiv.zoneCorrec = j3pAddElt(stor.lesdiv.conteneur, 'div')
      stor.lesdiv.expliK = j3pAddElt(stor.lesdiv.conteneur, 'div')
      stor.lesdiv.expliK2 = j3pAddElt(stor.lesdiv.expliK, 'div')
      stor.lesdiv.expliK1 = j3pAddElt(stor.lesdiv.expliK, 'div')
      stor.lesdiv.zoneCorrec.style.color = me.styles.cfaux

      j3pAffiche(stor.cip[1][0], null, '$' + stor.nombre + ' = $')
      stor.zonem = new ZoneStyleMathquill2(stor.cip[1][1], {
        id: 'mazone',
        limite: 17,
        limitenb: 20,
        restric: '0123456789*^',
        clavier: '0123456789*^',
        hasAutoKeyboard: true,
        enter: me.sectionCourante.bind(me)
      })

      stor.lesdiv.correction = j3pAddElt(me.zonesElts.MD, 'div', {
        style: me.styles.etendre('toutpetit.enonce', { padding: '10px' })
      })
      stor.zonem.focus()
    }
    stor.lesdiv.cons1.classList.add('enonce')
    stor.lesdiv.zoneRep.classList.add('travail')
    me.zonesElts.MG.classList.add('fond')
    me.finEnonce()
  }

  switch (this.etat) {
    case 'enonce':
      if (this.debutDeLaSection) {
        initSectionDecompose(this)
        stor.lesch = [
          {
            name: 'op1col',
            label: 'Décomposer',
            callback: function (a) {
              action2(a.infos.j, a.infos.i, 0)
            }
          },
          {
            name: 'op2col',
            label: 'Modifier',
            callback: function (a) {
              action2(a.infos.j, a.infos.i, 1)
            }
          }
        ]
      }
      enonceMain()
      break // case "enonce":

    case 'correction':
      // On teste si une réponse a été saisie
      if (!yareponse()) {
        stor.lesdiv.correction.style.color = this.styles.cfaux
        stor.lesdiv.correction.innerHTML = reponseIncomplete
      } else {
        // on a une réponse
        const repeleve = bonneReponse()

        if (repeleve[0]) {
          // Bonne réponse
          stor.lesdiv.correction.style.color = this.styles.cbien
          stor.lesdiv.correction.innerHTML = cBien

          if (stor.encours === 'Decomp') {
            if (ds.Calculatrice) {
              j3pDetruitFenetres('Calculatrice')
              j3pEmpty(stor.lesdiv.calculatrice)
            }
            if (ds.nbetapes === 2) {
              this.score = j3pArrondi(this.score + 1.5, 1)
            } else {
              this.score++
            }
            // nettoyage
            stor.lesdiv.zoneCoRep.innerHTML = ''
            stor.tabAf[stor.tabAf.length - 2][stor.tabAf[stor.tabAf.length - 1].length - 1].innerHTML = ''
            stor.tabAf[0][stor.tabAf[0].length - 1].innerHTML = ''
            // vire les nb clikables
            vir()
            for (let i = 0; i < stor.montab[stor.montab.length - 1].length; i++) {
              stor.tabAf[stor.montab.length - 1][i * 2 + 1].style.color = me.styles.cbien
            }

            /// // prepare phrase etape 2
            me.decompok = stor.nombre + ' = ' + stor.montab[stor.montab.length - 1][0]
            for (let i = 1; i < stor.montab[stor.montab.length - 1].length; i++) {
              me.decompok = me.decompok + ' \\times ' + stor.montab[stor.montab.length - 1][i]
            }
          } else {
            this.score = j3pArrondi(this.score + 0.5, 1)
            j3pDetruit(stor.lesdiv.expliK)
            j3pEmpty(stor.lesdiv.zoneCorrec)
            stor.bullaide2.disable()
            stor.zonem.corrige(true)
            stor.zonem.disable()
            j3pDetruit(stor.cip[0][1])
          }

          this.typederreurs[0]++
          return this.finCorrection('navigation', true)
        }

        // Pas de bonne réponse
        stor.lesdiv.correction.style.color = this.styles.cfaux

        // A cause de la limite de temps :
        if (me.isElapsed) { // limite de temps
          stor.lesdiv.correction.innerHTML = tempsDepasse
          this.typederreurs[10]++
          affcofo(repeleve)
          stor.lesdiv.expliK.classList.add('correction')
          if (stor.afco) {
            stor.lesdiv.zoneCorrec.classList.add('explique')
          }
          return this.finCorrection('navigation', true)
        }

        // Réponse fausse :
        stor.lesdiv.correction.innerHTML = cFaux

        if (me.essaiCourant < ds.nbchances) {
          stor.lesdiv.correction.innerHTML += '<br>' + essaieEncore
          if (stor.encours !== 'Decomp') {
            stor.afco = false
            stor.lesdiv.zoneCorrec.innerHTML = ''
            /// /cas des facteurs doubles
            let listedit = []
            for (let i = 0; i < repeleve[1].length; i++) {
              if (listedit.indexOf(repeleve[1][i]) === -1) {
                j3pAffiche(stor.lesdiv.zoneCorrec, null, textes.facteurdouble + '\n',
                  {
                    a: repeleve[1][i]
                  })
                listedit.push(repeleve[1][i])
              }
            }
            // cas des facteurs manquants
            for (let i = 0; i < repeleve[2].length; i++) {
              stor.afco = true
              j3pAffiche(stor.lesdiv.zoneCorrec, null, textes.facteurmanquant + '\n',
                {
                  a: repeleve[2][i]
                })
            }
            // cas des facteurs en trop
            listedit = []
            for (let i = 0; i < repeleve[3].length; i++) {
              if (listedit.indexOf(repeleve[3][i]) === -1) {
                stor.afco = true
                j3pAffiche(stor.lesdiv.zoneCorrec, null, textes.facteurentrop + '\n',
                  {
                    a: repeleve[3][i]
                  })
                listedit.push(repeleve[3][i])
              }
            }
            // cas des exposants faux
            for (let i = 0; i < repeleve[4].length; i++) {
              let ya = true
              let buff = textes.tropgrand
              let comp = 0
              switch (repeleve[4][i][0]) {
                case 2 :
                  comp = stor.nombrede2
                  break
                case 3 :
                  comp = stor.nombrede3
                  break
                case 5 :
                  comp = stor.nombrede5
                  break
                default: { ya = false }
              }
              if (ya && comp !== 0) {
                if (repeleve[4][i][1] < comp) { buff = textes.troppetit }
                stor.afco = true
                j3pAffiche(stor.lesdiv.zoneCorrec, null, textes.exposantfaux + '\n',
                  {
                    a: repeleve[4][i][0],
                    b: buff
                  })
              }
            }

            if (stor.afco) {
              if (ds.theme === 'zonesAvecImageDeFond') {
                stor.lesdiv.zoneCorrec.classList.add('explique')
              }
            }
          }

          this.typederreurs[1]++
          // on reste en état correction
          return this.finCorrection()
        }

        // Erreur au dernier essai
        stor.lesdiv.correction.innerHTML += '<br>' + regardeCorrection
        affcofo(repeleve)
        stor.lesdiv.expliK.classList.add('correction')
        if (stor.afco) {
          stor.lesdiv.zoneCorrec.classList.add('explique')
        }

        this.typederreurs[2]++
        this.finCorrection('navigation', true)
      }
      break // case "correction":

    case 'navigation':
      if (this.sectionTerminee()) {
        // On détermine la phrase d’état renvoyée par la section
        // this.typederreurs[4] c’est pe_2 puissances
        // this.typederreurs[3] c’est pe_1 premiers
        let peisup = ''
        let compt = this.typederreurs[2] / 4
        if (this.typederreurs[3] > compt) {
          peisup = ds.pe_1
          compt = this.typederreurs[3]
        }
        if (this.typederreurs[4] > compt) {
          peisup = ds.pe_2
        }
        this.parcours.pe = peisup
      } else {
        this.etat = 'enonce'
      }
      this.finNavigation()
      break // case "navigation":
  }
}
