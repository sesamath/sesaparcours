import { j3pAddElt, j3pAjouteBouton, j3pArrondi, j3pDetruit, j3pEmpty, j3pGetRandomBool, j3pGetRandomInt, j3pModale, j3pSpan } from 'src/legacy/core/functions'
import BulleAide from 'src/legacy/outils/bulleAide/BulleAide'
import ListeDeroulante from 'src/legacy/outils/listeDeroulante/ListeDeroulante'
import { j3pAffiche } from 'src/lib/mathquill/functions'
import { addDefaultTable } from 'src/legacy/themes/table'
import ZoneStyleMathquill1 from 'src/legacy/outils/zoneStyleMathquill/ZoneStyleMathquill1'
import { BoutonRadio } from 'src/legacy/outils/zoneStyleMathquill/functions'
import textesGeneriques from 'src/lib/core/textes'

const { cBien, cFaux, essaieEncore, regardeCorrection, reponseIncomplete, tempsDepasse } = textesGeneriques

/**
 * Ajouter une description ici
 * @fileOverview
 * @author Tommy Barroy <t.barroy@laposte.net>
 * @since 2017-10
*/

const minMin = 2 // valeur minimale acceptable pour min
const maxMax = 1000 // valeur maximale acceptable pour max

const pe_1 = 'Maitrise'
const pe_2 = 'Suffisant'
const pe_3 = 'Suffisant - Erreur critères de divisibilité'
const pe_4 = 'Insuffisant'
const pe_5 = 'Insuffisant - Erreur critères de divisibilité'
const pe_6 = 'Insuffisant'
const pe_7 = 'Insuffisant - Erreur critères de divisibilité'

// les premiers < 1000
const premiers = [2, 3, 5, 7, 11, 13, 17, 19, 23, 29, 31, 37, 41, 43, 47, 53, 59, 61, 67, 71, 73, 79, 83, 89, 97, 101, 103, 107, 109, 113, 127, 131, 137, 139, 149, 151, 157, 163, 167, 173, 179, 181, 191, 193, 197, 199, 211, 223, 227, 229, 233, 239, 241, 251, 257, 263, 269, 271, 277, 281, 283, 293, 307, 311, 313, 317, 331, 337, 347, 349, 353, 359, 367, 373, 379, 383, 389, 397, 401, 409, 419, 421, 431, 433, 439, 443, 449, 457, 461, 463, 467, 479, 487, 491, 499, 503, 509, 521, 523, 541, 547, 557, 563, 569, 571, 577, 587, 593, 599, 601, 607, 613, 617, 619, 631, 641, 643, 647, 653, 659, 661, 673, 677, 683, 691, 701, 709, 719, 727, 733, 739, 743, 751, 757, 761, 769, 773, 787, 797, 809, 811, 821, 823, 827, 829, 839, 853, 857, 859, 863, 877, 881, 883, 887, 907, 911, 919, 929, 937, 941, 947, 953, 967, 971, 977, 983, 991, 997]
// les premiers < sqrt(1000), 37 devrait pas y être, on le laisse là
const listnbpremiers = [2, 3, 5, 7, 11, 13, 17, 19, 23, 29, 31, 37]

export const params = {
  outils: ['mathquill'],
  /**
   * Ce qui est paramétrable dans l’éditeur de graphe
   * @type {LegacySectionParamsParametres}
   */
  parametres: [
    ['nbrepetitions', 5, 'entier', 'Nombre de répétitions de la section'],
    ['limite', 0, 'entier', 'Temps disponible par répétition, en secondes'],
    ['nbchances', 2, 'entier', 'Nombre d’essais par répétition'],
    ['est_premier', 'les deux', 'liste', '"oui" , "non" ou "les deux".', ['les deux', 'oui', 'non']],
    ['min', 100, 'entier', 'Valeur min du nombre (au moins 10)'],
    ['max', maxMax, 'entier', 'Valeur max du nombre (au plus 1000)'],
    ['Aide', true, 'boolean', '<u>true</u>: Une liste des premiers nombres premiers est disponible. '],
    // le modèle gère la compatibilité ascendante avec l’ancien paramètre MiseEnPagePlusClaire
    ['theme', 'standard', 'liste', 'Modèle pour l’affichage', ['standard', 'zonesAvecImageDeFond']]
  ],
  // dans le cas d’une section QUANTITATIVE
  pe: [
    { pe_1 },
    { pe_2 },
    { pe_3 },
    { pe_4 },
    { pe_5 },
    { pe_6 },
    { pe_7 }
  ]
}

const textes = {
  couleurexplique: '#A9E2F3',
  couleurcorrec: '#A9F5A9',
  couleurenonce: '#D8D8D8',
  couleurenonceg: '#F3E2A9',
  couleuretiquettes: '#A9E2F3',
  consigne1: 'Nous voulons déterminer si le nombre $£a$ est premier.',
  boutondivise: ' J’essaie une division par ',
  boutonrepondre: ' Je veux répondre. ',
  boutonracine: 'Je veux connaitre la racine carrée de £a .',
  racine: 'Racine de $£a$',
  listerep: ['Choisir.', 'est un nombre premier.', 'n’est pas un nombre premier.'],
  division: 'Essai d’une division',
  division2: ' $ £a \\div £b KK  £c $',
  saisdiv: 'Je sais que $£a$ est divisible par $£b$',
  divisible: 'divisible',
  pasdivisible: 'pas divisible',
  saisdiv2: 'Je sais que $£a$ n’est pas divisible par $£b$.',
  divjuste: ', donc $£a$ est divisible par $£b$ .',
  divpasjuste: ', donc $£a$ n’est pas divisible par $£b$ .',
  conseilordre: 'Tu ferais mieux d’effectuer les divisions dans l’ordre croissant.',
  conseildivmanqu: 'Ta réponse est trop rapide, nous ne savons pas si $£a$ est divisible par $£b$ .',
  conseilracine: 'Tu aurais pu faire moins de divisions. ($ \\sqrt{£a} = £b $).',
  conseildivisioninutile: 'Il ne servait à rien de diviser par $£a$ , car ce n’est pas un nombre premier.',
  conseildivisioninutile2: 'Il ne servait à rien de diviser par $£a$.',
  conseildivisioninutiles: 'Il ne servait à rien de diviser par $£a$ , car ce ne sont pas des nombres premiers.',
  conseildiviseurtrouve: 'Tu as trouvé que $£a$ est divisible par $£b$, donc $£a$ n’est pas un nombre premier',
  correc1: '$£a$ est divisible par $£b$ , donc $£a$ n’est pas un nombre premier',
  raisonnement: 'Raisonnement',
  conclusion: 'Conclusion',
  mank: 'Tu n’as pas vérifié si $£a$ est divisible par $£b$',
  fauxaveccalcul: 'Si le quotient de a par b est un nombre entier, alors a est divisible par b',
  aide: 'AIDE',
  estdoncpremier: '$£a$ n’a pas de diviseur premier inférieur à sa racine, donc $£a$ est un nombre premier',
  estdoncpaspremier: '$£b$ et $£c$ sont des diviseurs de $£a$, donc $£a$ n’est pas un nombre premier.',
  granddiviseur: 'Ce diviseur est plus grand que la racine de $£a$ .',
  mank2: 'Il faut trouver au moins un diviseur différent de $1$ ou de $£a$ pour conclure que $£a$ n’est pas un nombre premier.',
  voir: 'Je veux voir la division',
  utilisepremier: 'Il vaut mieux rechercher des diviseurs premiers.',
  inutilez: 'Cette ligne est inutile dans le raisonnement, tu as déjà trouvé un diviseur.',
  dejafait: 'Tu as déjà répondu pour ce diviseur.',
  fauxsanscalculbase: 'Ce résultat n’est pas démontré.',
  divisible2: 'Les nombres divisibles par $2$ sont les nombres pairs.',
  divisible3: 'La somme des chiffres d’un nombre divisible par $3$ est aussi divisible par $3$.',
  divisible4: 'Le nombre formé par les deux derniers chiffres d un nombre divisible par $4$ est aussi divisible par $4$.',
  divisible5: 'Les nombres divisibles par $5$ se terminent par $0$ ou $5$.',
  divisible9: 'La somme des chiffres d’un nombre divisible par $9$ est aussi divisible par $9$.',
  divisible10: 'Les nombres divisibles par $10$ se terminent par $0$.',
  raisonnement2: 'Le raisonnement est incomplet !'
}

/**
 * section premier01
 * @this {Parcours}
 */
export default function main () {
  const me = this
  const stor = me.storage
  const ds = me.donneesSection

  function onMouseOver () {
    // sans bind, notre this est l’élément sur lequel ce listener a été ajouté
    this.style.backgroundColor = 'grey'
  }

  function onMouseOut () {
    // sans bind, notre this est l’élément sur lequel ce listener a été ajouté
    this.style.backgroundColor = null
  }

  function onClick () {
    if (stor.disabled) { return }
    annuleAffi(this.coid)
  }

  function hasReponse () {
    let yaya = stor.listes.changed
    if (!yaya) stor.listes.focus()
    if (me.isElapsed) yaya = true
    return yaya
  }

  function addBulle (container, content, numligne) {
    const span = j3pSpan(container, {
      style: me.styles.toutpetit.correction
    })
    const bulle = new BulleAide(span, content, { place: 1 })
    bulle.numligne = numligne
    stor.bulles.push(bulle)
  }

  function bonneReponse () {
    // cette fonction renvoie true ou false si du moins une réponse a été saisie
    // (dans le cas contraire, elle n’est pas appelée
    // elle est appelée dans la partie "correction" de la section

    if (document.getElementById('modale') !== null) {
      j3pDetruit('j3pmasque')
      j3pDetruit('modale')
    }
    j3pEmpty(stor.zones.explik1)
    stor.zones.Explik = j3pAddElt(stor.zones.explik1, 'div')
    stor.zones.Explik.style.color = me.styles.cfaux

    let i, j
    for (i = stor.bulles.length - 1; i > -1; i--) {
      stor.bulles[i].disable()
      stor.bulles.pop()
    }
    let repOk = true
    for (i = 0; i < stor.rep1eleve.length; i++) {
      let booll = false
      const bulleCtId = stor.rep1eleve[i].divAide
      const elt = stor.rep1eleve[i].laligne
      if ((stor.nombre % stor.rep1eleve[i].div === 0) === stor.rep1eleve[i].affirmeDiv) {
        elt.style.color = me.styles.cbien
      } else {
        repOk = false
        elt.style.color = me.styles.cfaux
        booll = true
      }
      /// fausses avec calculs ->  avert
      if (booll && stor.rep1eleve[i].avuDiv) {
        addBulle(bulleCtId, textes.fauxaveccalcul, stor.rep1eleve[i].coid)
      }

      // fausses sans calculs ->  avert
      if (booll && (!stor.rep1eleve[i].avuDiv)) {
        let avert = textes.fauxsanscalculbase
        if (stor.rep1eleve[i].div === 2) {
          avert = textes.divisible2
          stor.typeerreurs[3]++
        }
        if (stor.rep1eleve[i].div === 3) {
          avert = textes.divisible3
          stor.typeerreurs[3]++
        }
        if (stor.rep1eleve[i].div === 5) {
          avert = textes.divisible5
          stor.typeerreurs[3]++
        }
        if (stor.rep1eleve[i].div === 4) {
          avert = textes.divisible4
          stor.typeerreurs[3]++
        }
        if (stor.rep1eleve[i].div === 9) {
          avert = textes.divisible9
          stor.typeerreurs[3]++
        }
        if (stor.rep1eleve[i].div === 10) {
          avert = textes.divisible10
          stor.typeerreurs[3]++
        }
        addBulle(bulleCtId, avert, stor.rep1eleve[i].coid)
      }

      /// juste avec calculs 2,3,5 ->  avert
      if ((!booll) && (stor.rep1eleve[i].avuDiv)) {
        let foprev = false
        let avert
        if (stor.rep1eleve[i].div === 2) {
          avert = textes.divisible2
          stor.typeerreurs[3]++
          foprev = true
        }
        if (stor.rep1eleve[i].div === 3) {
          avert = textes.divisible3
          stor.typeerreurs[3]++
          foprev = true
        }
        if (stor.rep1eleve[i].div === 5) {
          avert = textes.divisible5
          stor.typeerreurs[3]++
          foprev = true
        }
        if (stor.rep1eleve[i].div === 4) {
          avert = textes.divisible4
          stor.typeerreurs[3]++
          foprev = true
        }
        if (stor.rep1eleve[i].div === 9) {
          avert = textes.divisible9
          stor.typeerreurs[3]++
          foprev = true
        }
        if (stor.rep1eleve[i].div === 10) {
          avert = textes.divisible10
          stor.typeerreurs[3]++
          foprev = true
        }

        if (foprev) {
          addBulle(bulleCtId, avert, stor.rep1eleve[i].coid)
        }
      }
    }

    if (repOk) {
      // si toutes lignes bonnes
      // verifier calculs manquants  -> envoie faux
      // okk est true si un diviseur a été trouvé
      let okk = false
      for (j = 0; j < stor.rep1eleve.length; j++) {
        if (stor.rep1eleve[j].affirmeDiv) {
          if ((stor.rep1eleve[j].div > 1) && (stor.rep1eleve[j].div < stor.nombre)) {
            okk = true
          }
        }
      }
      if (!estPremier(stor.nombre)[0]) {
        // si pas premier, faut au moins un diviseur
        if (!okk) repOk = false
      }
      // si affirme pas premier
      if (!stor.rep2eleve) { // si pas premier, faut un diviseur au moins un diviseur
        if (!okk) { // alert il faut trouver au moins un diviseur
          j3pAffiche(stor.zones.Explik, 'explikmank' + i, textes.mank2 + '\n',
            {
              a: stor.nombre,
              b: '1'
            })
          if (ds.theme === 'zonesAvecImageDeFond') {
            stor.zones.Explik.classList.add('explique')
          }
        }
      }

      /// si premier
      if (estPremier(stor.nombre)[0] || (!repOk)) {
        let dej = false
        for (i = 0; listnbpremiers[i] < Math.sqrt(stor.nombre); i++) {
          let calculi = false
          for (j = 0; j < stor.rep1eleve.length; j++) {
            if ((stor.rep1eleve[j].div === listnbpremiers[i])) {
              calculi = true
            }
          }
          if (!calculi) {
            repOk = false
            let buf = ', $£b$'
            if (!dej) {
              dej = true
              buf = textes.mank
            }

            j3pAffiche(stor.zones.Explik, 'explikmankz' + i, buf,
              {
                a: stor.nombre,
                b: listnbpremiers[i]
              })
            if (ds.theme === 'zonesAvecImageDeFond') {
              stor.zones.Explik.classList.add('explique')
            }
          }
        }
      }

      // verifier lignes redondantes -> envoie avert
      const tabdej = []
      let ltabdej = 0
      for (j = 0; j < stor.rep1eleve.length; j++) {
        let yep = false
        const val = stor.rep1eleve[j].div
        for (i = 0; i < ltabdej; i++) {
          if (val === tabdej[i]) {
            yep = true
            addBulle(stor.rep1eleve[j].divAide, textes.dejafait, stor.rep1eleve[j].coid)
          }
        }
        if (!yep) {
          tabdej[ltabdej] = val
          ltabdej++
        }
      }
      // verifier calculs inutiles -> envoie avert
      /// pas premiers
      for (j = 0; j < stor.rep1eleve.length; j++) {
        const val = stor.rep1eleve[j].div
        const premier = estPremier(val)[0]
        if ((!premier) && (!stor.rep1eleve[j].affirmeDiv)) {
          addBulle(stor.rep1eleve[j].divAide, textes.utilisepremier, stor.rep1eleve[j].coid)
        }
      }
      // nb ou 1
      for (j = 0; j < stor.rep1eleve.length; j++) {
        if ((stor.rep1eleve[j].div === 1) || (stor.rep1eleve[j].div === stor.nombre)) {
          const bulleContent = textes.conseildivisioninutile2.replace('£a', stor.rep1eleve[j].div)
          addBulle(stor.rep1eleve[j].divAide, bulleContent, stor.rep1eleve[j].coid)
        }
      }
      // plus grands que racine
      for (j = 0; j < stor.rep1eleve.length; j++) {
        if ((!stor.rep1eleve[j].affirmeDiv) && (stor.rep1eleve[j].div > Math.sqrt(stor.nombre))) {
          const bulleContent = textes.granddiviseur.replace('£a', stor.nombre)
          addBulle(stor.rep1eleve[j].divAide, bulleContent, stor.rep1eleve[j].coid)
        }
      }
      /// suite à un diviseur trouvé
      let dejatrouv = false
      for (j = 0; j < stor.rep1eleve.length; j++) {
        if (stor.rep1eleve[j].affirmeDiv) {
          if (dejatrouv) {
            addBulle(stor.rep1eleve[j].divAide, textes.inutilez, stor.rep1eleve[j].coid)
          } else {
            dejatrouv = true
          }
        }
      }
      // verifier ordre calculs -> envoie avert
    } // fin if repOk

    stor.unerep = true
    if (estPremier(stor.nombre)[0] === stor.rep2eleve) {
      stor.listes.corrige(true)
    } else {
      repOk = false
      stor.listes.corrige(false)
      stor.unerep = false
    }
    return (repOk)
  } // bonneReponse

  function fermeDiv (texte, bon, bb) {
    const coid = newIdPerso()
    let maphrase = texte
    if (bb === undefined) {
      bb = true
      if (bon) {
        maphrase += textes.divjuste
      } else {
        maphrase += textes.divpasjuste
      }
    }
    const lnb = parseInt(stor.zoneDiv.reponse().replace(/ /g, ''))
    const Maligne = j3pAddElt(stor.zones.rep1, 'div')
    const lint = addDefaultTable(Maligne, 1, 2)
    j3pAffiche(lint[0][0], null, maphrase,
      {
        a: stor.nombre,
        b: lnb,
        c: j3pArrondi(stor.nombre / lnb, 0)
      })
    lint[0][0].addEventListener('mouseover', onMouseOver, false)
    lint[0][0].addEventListener('mouseout', onMouseOut, false)
    lint[0][0].addEventListener('click', onClick, false)
    lint[0][0].style.cursor = 'not-allowed'
    lint[0][0].coid = coid
    j3pDetruit('j3pmasque')
    j3pDetruit('modale')
    stor.rep1eleve.push({
      div: lnb,
      affirmeDiv: bon,
      laligne: Maligne,
      laligne2: lint[0][0],
      avuDiv: bb,
      divAide: lint[0][1],
      coid
    })
    stor.zoneDiv.disable()
    j3pEmpty(stor.doudiv)
    stor.zoneDiv = new ZoneStyleMathquill1(stor.doudiv, { restric: '0123456789' })
    stor.zoneDiv.modifL = changeInput
    changeInput()
    stor.zoneDiv.focus()
  }

  function afficheDiv () {
    fermeDiv('Je sais que $' + stor.nombre + '$ est divisible par $' + stor.zoneDiv.reponse().replace(/ /g, '') + '$.', true, false)
  }

  function affichePasDiv () {
    fermeDiv('Je sais que $' + stor.nombre + '$ n’est pas divisible par $' + stor.zoneDiv.reponse().replace(/ /g, '') + '$.', false, false)
  }

  function annuleAffi (coid) {
    for (let i = stor.bulles.length - 1; i > -1; i--) {
      if (stor.bulles[i].numligne === coid) {
        stor.bulles[i].disable()
        stor.bulles.splice(i, 1)
      }
    }
    for (let i = 0; i < stor.rep1eleve.length; i++) {
      if (stor.rep1eleve[i].coid === coid) {
        j3pDetruit(stor.rep1eleve[i].laligne)
        stor.rep1eleve.splice(i, 1)
      }
    }
  }

  function affimodale (texte, titre) {
    const yy = j3pModale({ titre, contenu: '' })
    j3pAffiche(yy, null, texte)
    yy.style.color = me.styles.toutpetit.correction.color
  }

  function changeInput () {
    j3pEmpty(stor.jklm[0][0])
    j3pEmpty(stor.jklm[1][0])
    j3pEmpty(stor.jklm[2][0])
    // FIXME ce truc arrive dès le chargement sur ?rid=sesabibli/5c2df981e816a24d19b6d9df
    if (!stor.zoneDiv) return console.error(Error('zoneDiv n’a pas encore été initialisé'))
    if (stor.zoneDiv.reponse() === '') {
      stor.divdiv[0][0].style.borderRight = '0px solid'
      return
    }
    const nb = parseInt(stor.zoneDiv.reponse().replace(/ /g, ''))
    if (nb === 0) { return }
    stor.radio1 = BoutonRadio(stor.jklm[0][0], 'ensemble', 0)
    stor.radio2 = BoutonRadio(stor.jklm[1][0], 'ensemble', 0)
    stor.radio3 = BoutonRadio(stor.jklm[2][0], 'ensemble', 0)
    stor.divdiv[0][0].style.borderRight = '1px solid'

    j3pAffiche(stor.radio1.label, null, textes.voir)
    j3pAffiche(stor.radio2.label, null, textes.saisdiv,
      { a: stor.nombre, b: nb })
    j3pAffiche(stor.radio3.label, null, textes.saisdiv2,
      { a: stor.nombre, b: nb })

    stor.radio1.input.addEventListener('change', essaieDivision.bind(null, nb))
    stor.radio2.input.addEventListener('change', afficheDiv)
    stor.radio3.input.addEventListener('change', affichePasDiv)
  }

  function estPremier (nb) {
    let prems = true
    let i
    for (i = 2; i <= Math.sqrt(nb); i++) {
      if ((nb % i) === 0) {
        prems = false
        break
      }
    }

    return [prems, i]
  }

  function gereZone (rep) {
    stor.rep2eleve = (rep === textes.listerep[1])
  }

  function afficheRacine () {
    const racine = j3pArrondi(Math.sqrt(stor.nombre), 6)
    const buf = '='
    // à virer
    // if (Math.abs(racine - Math.sqrt(stor.nombre)) < (Math.pow(10, -12))) {var buf = '=';} else {var buf = '≈';}
    j3pDetruit('bouton3')
    j3pAffiche(stor.zones.rep1, null, '$ \\sqrt{£a}' + buf + ' £b $', {
      a: stor.nombre,
      b: racine
    })
  }

  function essaieDivision (nb) {
    j3pModale({ titre: textes.division, contenu: '' })
    const result = j3pArrondi((stor.nombre / nb), 2)
    let buf
    if (Math.abs((stor.nombre / nb) - result) < Math.pow(10, -12)) { buf = '=' } else { buf = '≈' }
    const maphrase = textes.division2.replace('KK', buf).replace('£a', stor.nombre).replace('£b', nb).replace('£c', result)
    // FIXME pourquoi affecter ici undefined à me.affichemodale, qu’on utilise nulle part
    j3pAffiche('modale', 'affichemodale', '\n' + maphrase + '\n\n')
    j3pAjouteBouton('modale', 'boutonval1', 'MepBoutons', textes.divisible, fermeDiv.bind(null, maphrase, true))
    j3pAjouteBouton('modale', 'boutonval2', 'MepBoutons', textes.pasdivisible, fermeDiv.bind(null, maphrase, false))
  }

  function initSection () {
    me.validOnEnter = false // ex donneesSection.touche_entree

    // check min/max
    if (!Number.isInteger(ds.min) || ds.min < minMin) {
      console.error(`Paramètre min invalide => ${minMin} imposé`)
      ds.min = minMin
    }
    if (!Number.isInteger(ds.max) || ds.max > maxMax) {
      console.error(`Paramètre max invalide => ${maxMax} imposé`)
      ds.max = maxMax
    }
    if (ds.max < ds.min) {
      console.error(`Paramètre max invalide, il doit être supérieur au min => ${maxMax} imposé`)
      ds.max = maxMax
    }
    // et faut aussi vérifier qu’on pourra tirer assez de nombres différents
    if (ds.nbitems > ds.max - ds.min) {
      // faut rester dans les bornes acceptables
      if (ds.min + ds.nbitems < maxMax) {
        console.error(`max ${ds.max} trop petit pour le nombre de questions => ${ds.min + ds.nbitems} imposé`)
        ds.max = ds.min + ds.nbitems
      } else {
        console.error(`max ${ds.min} trop grand pour le nombre de questions => ${ds.max - ds.nbitems} imposé`)
        ds.min = ds.max - ds.nbitems
      }
    }

    // les nb premiers dans l’intervalle demandé
    const nbPremiers = []
    // les autres
    const nbPasPremiers = []
    for (let i = ds.min; i <= ds.max; i++) {
      if (premiers.includes(i)) nbPremiers.push(i)
      else nbPasPremiers.push(i)
    }
    // les nombres aléatoires à mettre dans les énoncés
    stor.nombres = []
    while (stor.nombres.length < ds.nbitems) {
      let premier
      if (ds.est_premier === 'oui') {
        premier = true
      } else if (ds.est_premier === 'non') {
        premier = false
      } else {
        premier = j3pGetRandomBool()
      }
      let pioche = premier ? nbPremiers : nbPasPremiers
      if (!pioche.length) {
        console.error(`Pas assez de nombres ${premier ? 'premiers' : 'non premiers'} dans l’intervalle fixé`)
        pioche = premier ? nbPasPremiers : nbPremiers
      }
      // on peut prendre ce qu’on a demandé
      const index = j3pGetRandomInt(0, pioche.length - 1)
      stor.nombres.push(pioche[index])
      // et on le vire
      pioche.splice(index, 1)
    }

    // Construction de la page
    me.construitStructurePage('presentation1bis')

    // DECLARATION DES VARIABLES
    ds.pe = [pe_1, pe_2, pe_3, pe_4, pe_5, pe_6, pe_7]

    /*
             Par convention,`
            `   stor.typeerreurs[0] = nombre de bonnes réponses
                stor.typeerreurs[1] = nombre de fois où l’élève a utilisé une chance supplémentaire
                stor.typeerreurs[2] = nombre de mauvaises réponses
                stor.typeerreurs[10] = nombre de fois où l’élève a eu faux à cause d’un temps limite
                LES AUTRES peuvent être utilisées par la détermination de la pe dans le cas d’une section qualitative
             */
    stor.typeerreurs = [0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0]
    me.afficheTitre('Déterminer si un nombre est premier')
    stor.bulles = []
    enonceMain()
  }

  function newIdPerso () {
    let aret
    do {
      aret = j3pGetRandomInt(1, 1000)
    } while (stor.mesidperso.indexOf(aret) !== -1)
    return aret
  }

  function enonceMain () {
    for (let i = stor.bulles.length - 1; i > -1; i--) {
      stor.bulles[i].disable()
      stor.bulles.pop()
    }
    me.videLesZones()

    stor.disabled = false
    stor.zones = {}
    stor.nombre = 1
    stor.rep1eleve = []
    stor.rep2eleve = false
    stor.mesidperso = []

    stor.nombre = stor.nombres[me.questionCourante - 1]

    // création du conteneur dans lequel se trouveront toutes les consignes
    stor.zones.elCt = j3pAddElt(me.zonesElts.MG, 'div', {
      style: me.styles.etendre('toutpetit.enonce', { padding: '10px' })
    })
    // Dans ce conteneur, on crée un div par phrase (cela forcera le retour à la ligne)
    const elCons1 = addDefaultTable(stor.zones.elCt, 1, 1)[0][0]

    const elRep = addDefaultTable(stor.zones.elCt, 1, 1)[0][0]
    stor.zones.contBout = addDefaultTable(elRep, 1, 1)[0][0]
    stor.uio = addDefaultTable(stor.zones.contBout, 2, 1)
    stor.divdiv = addDefaultTable(stor.uio[1][0], 1, 2)
    stor.jklm = addDefaultTable(stor.divdiv[0][1], 3, 1)
    stor.zones.rep1 = j3pAddElt(elRep, 'div')

    stor.zones.Raiz = addDefaultTable(stor.zones.rep1, 1, 1)[0][0]
    j3pAffiche(stor.zones.Raiz, null, '<b><u>' + textes.raisonnement + '</u></b>\n')
    stor.zones.rep2 = addDefaultTable(elRep, 1, 1)[0][0]
    stor.zoneConcl = j3pAddElt(stor.zones.rep2, 'div')
    j3pAffiche(stor.zoneConcl, null, '<b><u>' + textes.conclusion + '</u></b>\n')

    j3pAffiche(elCons1, null, '<b>' + textes.consigne1 + '</b>', { a: stor.nombre })

    stor.nbligne = 0

    const ldiv = addDefaultTable(stor.divdiv[0][0], 1, 3)
    j3pAffiche(ldiv[0][0], null, textes.boutondivise + '&nbsp;')
    j3pAffiche(ldiv[0][2], null, '&nbsp;.')
    stor.doudiv = ldiv[0][1]
    stor.zoneDiv = new ZoneStyleMathquill1(stor.doudiv, { restric: '0123456789', limite: 6 })
    stor.zoneDiv.modifL = changeInput

    j3pAjouteBouton(stor.uio[0][0], 'bouton3', 'MepBoutons', textes.boutonracine.replace('£a', stor.nombre), afficheRacine)

    const liste = textes.listerep
    j3pAffiche(stor.zones.rep2, null, '£a ', {
      a: stor.nombre
    })
    stor.listes = ListeDeroulante.create(stor.zones.rep2, liste, {
      onChange: gereZone,
      centre: true,
      alignmathquill: true
    })

    if (ds.Aide) {
      const zAide = j3pAddElt(me.zonesElts.MD, 'div', {
        style: me.styles.etendre('toutpetit.enonce', { padding: '10px' })
      })
      j3pAjouteBouton(zAide, montreprems, { className: 'MepBoutons', value: 'Aide' })
    }

    stor.zones.correction = j3pAddElt(me.zonesElts.MD, 'div', {
      style: me.styles.etendre('toutpetit.enonce', { padding: '10px' })
    })

    stor.zones.explik1 = j3pAddElt(stor.zones.elCt, 'div', { style: me.styles.toutpetit.correction })
    // Obligatoire

    elCons1.classList.add('enonce')
    stor.zones.contBout.classList.add('travail')
    stor.zones.rep1.style.marginTop = '2px'
    stor.zones.rep1.style.marginBottom = '2px'
    stor.zones.rep1.style.background = '#cccccc'
    stor.zones.rep1.style.padding = '5px'
    stor.zones.rep2.classList.add('travail')
    me.zonesElts.MG.classList.add('fond')

    me.finEnonce()
  }

  function montreprems () {
    let tt = ''
    const laliste = [2, 3, 5, 7, 11, 13, 17, 19, 23, 29, 31, 37]
    for (let i = 0; i < laliste.length; i++) {
      tt += laliste[i] + '<br>'
    }
    affimodale(tt, 'Nombres premiers connus')
  }

  function affCorFaux (bool) {
    if (bool) {
      stor.zoneDiv.disable()
      for (let j = 1; j < stor.rep1eleve.length; j++) {
        const elt = stor.rep1eleve[j].laligne2
        elt.removeEventListener('mouseover', onMouseOver)
        elt.removeEventListener('mouseout', onMouseOut)
        elt.removeEventListener('click', onClick)
        elt.style.cursor = null
      }
      stor.disabled = true
      /// dezingue tout
      j3pDetruit(stor.zones.contBout)

      stor.listes.disable()
      if (!stor.unerep) stor.listes.barre()

      const eltRepCo = j3pAddElt(stor.zones.explik1, 'div')
      eltRepCo.classList.add('correction')

      const eltRep1Co = j3pAddElt(eltRepCo, 'div')
      const eltRaisCo = j3pAddElt(eltRep1Co, 'div')
      j3pAffiche(eltRaisCo, null, '<b><u>' + textes.raisonnement + '</u></b>\n')
      const eltRep2Co = j3pAddElt(eltRepCo, 'div')
      const eltConclCo = j3pAddElt(eltRep2Co, 'div')
      j3pAffiche(eltConclCo, null, '<b><u>' + textes.conclusion + '</u></b>\n')

      if (estPremier(stor.nombre)[0]) {
        const racine = j3pArrondi(Math.sqrt(stor.nombre), 6)
        j3pAffiche(eltRep1Co, null, '$ \\sqrt{£a} ≈ £b $',
          {
            a: stor.nombre,
            b: racine
          })
        let signeEgal
        for (let i = 0; listnbpremiers[i] < Math.sqrt(stor.nombre); i++) {
          const lico = j3pAddElt(eltRep1Co, 'div')
          if ([2, 3, 5].indexOf(listnbpremiers[i]) !== -1) {
            j3pAffiche(
              lico,
              null,
              'Je sais que $' + stor.nombre + '$ n’est pas divisible par $' + listnbpremiers[i] + '$.')
          } else {
            const result = j3pArrondi((stor.nombre / listnbpremiers[i]), 2)
            signeEgal = (Math.abs((stor.nombre / listnbpremiers[i]) - result) < Math.pow(10, -12))
              ? '='
              : '≈'

            j3pAffiche(
              lico,
              null,
              '$ £a \\div £b ' + signeEgal + ' £c $ ' + textes.divpasjuste,
              {
                a: stor.nombre,
                b: listnbpremiers[i],
                c: j3pArrondi(stor.nombre / listnbpremiers[i], 2)
              })
          }
        }
        const lico2 = j3pAddElt(eltRep2Co, 'div')
        j3pAffiche(lico2, null, textes.estdoncpremier,
          {
            a: stor.nombre
          })
      } else {
        let lico = j3pAddElt(eltRep1Co, 'div')
        const mondiv = estPremier(stor.nombre)[1]
        j3pAffiche(lico, null, '$ £a \\div £b = £c $ ' + textes.divjuste,
          {
            a: stor.nombre,
            b: mondiv,
            c: j3pArrondi(stor.nombre / mondiv, 0)
          })
        lico = j3pAddElt(eltRep2Co, 'div')
        j3pAffiche(lico, null, textes.estdoncpaspremier,
          {
            a: stor.nombre,
            b: mondiv,
            c: j3pArrondi(stor.nombre / mondiv, 0)
          })
      }
    }
  }

  switch (this.etat) {
    case 'enonce':
      // code exécuté au lancement de la section
      if (this.debutDeLaSection) {
        initSection()
      } else {
        enonceMain()
      }
      break // case "enonce":

    case 'correction':
      // On teste si une réponse a été saisie
      if (!hasReponse()) {
        // pas de réponse
        stor.zones.correction.style.color = this.styles.cfaux
        stor.zones.correction.innerHTML = reponseIncomplete
      } else if (bonneReponse()) {
        // Bonne réponse
        this.score += j3pArrondi(1 / ds.nbetapes, 1)
        stor.zones.correction.style.color = this.styles.cbien
        stor.zones.correction.innerHTML = cBien

        j3pDetruit(stor.zones.contBout)
        stor.zoneDiv.disable()

        stor.listes.disable()
        stor.disabled = true

        for (let j = 1; j < stor.rep1eleve.length; j++) {
          const elt = stor.rep1eleve[j].laligne2
          elt.removeEventListener('mouseover', onMouseOver)
          elt.removeEventListener('mouseout', onMouseOut)
          elt.removeEventListener('click', onClick)
          elt.style.cursor = null
        }

        stor.typeerreurs[0]++
        return this.finCorrection('navigation', true)
      }

      // Une réponse, mais pas la bonne
      stor.zones.correction.style.color = this.styles.cfaux

      // A cause de la limite de temps :
      if (me.isElapsed) { // limite de temps
        stor.zones.correction.innerHTML = tempsDepasse
        stor.zones.correction.innerHTML += '<br>' + '<br>' + regardeCorrection
        stor.typeerreurs[10]++
        affCorFaux(true)
        return this.finCorrection('navigation', true)
      }

      // Réponse fausse sans limite de temps
      if (estPremier(stor.nombre)[0] === stor.rep2eleve) {
        stor.zones.correction.innerHTML = textes.raisonnement2
      } else {
        stor.zones.correction.innerHTML = cFaux
      }

      if (me.essaiCourant < ds.nbchances) {
        stor.zones.correction.innerHTML += '<br>' + essaieEncore
        stor.typeerreurs[1]++
        // on reste en correction
        return this.finCorrection()
      }

      // Erreur au dernier essai
      stor.zones.correction.innerHTML += '<br>' + regardeCorrection
      affCorFaux(true)
      stor.typeerreurs[2]++
      this.finCorrection('navigation', true)
      break // case "correction":

    case 'navigation':
      if (this.sectionTerminee()) {
        // On détermine la phrase d’état renvoyée par la section
        const sco = this.score / ds.nbitems
        let peibase = 0
        if (sco >= 0.8) peibase = 0
        if ((sco >= 0.55) && (sco < 0.8)) peibase = 1
        if ((sco >= 0.3) && (sco < 0.55)) peibase = 3
        if (sco < 0.3) peibase = 5

        let peisup = 0
        const compt = stor.typeerreurs[2] / 4
        if (stor.typeerreurs[3] > compt) {
          peisup = 1
        }
        this.parcours.pe = ds.pe[peibase + peisup]
      } else {
        this.etat = 'enonce'
      }
      this.finNavigation()
      break // case "navigation"
  }
}
