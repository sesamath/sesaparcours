import $ from 'jquery'
import { j3pAddElt, j3pAjouteBouton, j3pDiv, j3pElement, j3pEmpty, j3pFocus, j3pPaletteMathquill, j3pShowError, j3pValeurde } from 'src/legacy/core/functions'
import { j3pAffiche, j3pMathsAjouteDans, mqRestriction, unLatexify } from 'src/lib/mathquill/functions'
import { getMtgApp } from 'src/lib/outils/mathgraph'
import textesGeneriques from 'src/lib/core/textes'

const { cBien, cFaux, essaieEncore, tempsDepasse } = textesGeneriques

/*
    Yves Biton
    2020
    Section demandant d’abord de décomposer un entier en produit de facteurs premiers puis de donner tous ses diviseurs
 */

export const params = {
  outils: ['mathquill'],
  /**
   * Ce qui est paramétrable dans l’éditeur de graphe
   * @type {LegacySectionParamsParametres}
   */
  parametres: [
    ['nbrepetitions', 5, 'entier', 'aucun'],
    ['limite', 0, 'entier', 'Temps disponible par répétition, en secondes'],
    ['aDecomposer', '[20;23;24;27;28;29;30;32;36;37;38;40;41;42,44;45;48;50;54;56;64;70;72;75;88;98;100;110;120;140;150;220;240;250;280;300;320;350;360;400]', 'string',
      'Liste des nombres dont on demandera la décomposition et les diviseurs. Si vide, on choisit un nombre entre min et max au hasard'],
    ['min', 6, 'entier', 'Valeur mini du nombre dont on demande les diviseurs'],
    ['max', 100, 'entier', 'Valeur mini du nombre dont on demande les diviseurs'],
    ['nbEssais', 5, 'entier', 'Nombre d’essais maximum autorisés pour la décomposition en produit de facteurs premeirs']
  ]
}

const textes = {
  consigneInit: 'On demande de trouver la décomposition de $£a$ en produit de puissances de facteurs premiers.' +
        '<br>Elle doit être écrite sous la forme la plus simple possible (ne pas utiliser d’exposant égal à 1). Si le nombre est premier, entrer le nombre comme réponse.',
  recopier: 'Recopier réponse précédente',
  essai1: 'Il reste un essai.',
  essai2: 'Il reste £e essais.',
  validation: 'Il reste une validation.',
  rep1: 'Réponse exacte&nbsp;: ',
  rep2: 'Réponse fausse&nbsp;: ',
  rep3: 'Exact pas fini&nbsp;: ',
  lasol: 'La solution se trouve ci-contre.',
  pbDecomp: 'Le nombre n’est pas décomposé comme demandé',
  divAbsents: 'Il manque des diviseurs.',
  rep4_1: 'Les diviseurs positifs de £a sont £b',
  rep4_2: 'Les diviseurs positifs de £a ne sont pas £b',
  corr1: '£a est un  nombre premier. Ses deux seuls diviseurs dans $\\N$ sont 1 et £a.',
  corr2: 'Les diviseurs positifs de £a sont&nbsp;: ',
  corr3: 'Rangés dans l’ordre croissant&nbsp;: ',
  corr4: 'On peut obtenir ces solutions à l’aide d’un arbre.',
  nextQuest: 'On passe à la question suivante.',
  decompFournie: 'Voici la décomposition demandée :',
  estPrem: 'est un nombre premier.'
}

/**
 * section decompositionLibre
 * @this {Parcours}
 */
export default function main () {
  const parcours = this
  const sq = this.storage
  let nbe
  function onkeyup () {
    if (sq.marked) { demarqueEditeurPourErreur() }
  }

  function traiteMathQuill (ch) {
    ch = unLatexify(ch)
    return sq.listePourCalc.addImplicitMult(ch) // Traitement des multiplications implicites
  }

  function montrePaletteBoutons (bvisible) {
    $('#boutonsmathquill').css('display', bvisible ? 'block' : 'none')
  }

  function zero (x) {
    return Math.abs(x) < 1e-9
  }

  function entier (x) {
    return zero(x - Math.floor(x))
  }

  /*
  Fonction ui, si n est divisible par q, renvoie le plus grand exposant de q qui divise n et sinon renvoie 0
   */
  function divPar (x, q) {
    if (entier(x / q)) {
      let exp = 1
      let k = q
      while (entier(x / (q * k))) {
        exp++
        k = k * q
      }
      return exp
    } else return 0
  }

  function listeADecomposer () {
    let st = parcours.donneesSection.aDecomposer
    if (st === '') {
      return []
    } else {
      const res = []
      // On retire les crochets de début et de fin
      st = st.replace(/\s*\[\s*/g, '').replace(/\s*]\s*/g, '').replace(/,/g, ';')
      const tab = st.split(';')
      for (let i = 0; i < tab.length; i++) {
        if (sq.listePourCalc.verifieSyntaxe(tab[i])) {
          sq.listePourCalc.giveFormula2('rep', tab[i])
          sq.listePourCalc.calculateNG()
          const nb = sq.listePourCalc.valueOf('rep')
          if (nb !== 1) res.push(nb)
        }
      }
      return res
    }
  }

  function prepareNombre () {
    let n
    if (sq.listeADecomposerInit.length !== 0) {
      if (sq.listeADecomposer.length === 0) sq.listeADecomposer = sq.listeADecomposerInit
      // On tire un nombre au hasard parmi les nombres à décomposer restants
      const ind = Math.floor(Math.random() * sq.listeADecomposer.length)
      n = sq.listeADecomposer[ind]
      sq.listeADecomposer.splice(ind, 1)
    } else {
      const min = parcours.donneesSection.min
      const max = parcours.donneesSection.max
      n = min + Math.floor(Math.random() * (max - min + 1))
    }
    let nb = n
    const factprem = []
    const exposants = []
    let exp = divPar(n, 2)
    if (exp > 0) {
      factprem.push(2)
      exposants.push(exp)
      nb = nb / Math.pow(2, exp)
    }
    exp = divPar(n, 3)
    if (exp > 0) {
      factprem.push(3)
      exposants.push(exp)
      nb = nb / Math.pow(3, exp)
    }
    let k = 5
    let pas = 4
    while (k * k <= nb) {
      exp = divPar(nb, k)
      if (exp > 0) {
        factprem.push(k)
        exposants.push(exp)
        nb = nb / Math.pow(k, exp)
      }
      pas = 6 - pas
      k = k + pas
    }
    if (nb !== 1) {
      factprem.push(nb)
      exposants.push(1)
    }
    let st = ''
    for (let i = 0; i < factprem.length; i++) {
      if (i > 0) st += '*'
      st += String(factprem[i])
      if (exposants[i] > 1) st += '^' + String(exposants[i])
    }
    sq.n = n
    sq.factprem = factprem
    sq.exposants = exposants
    sq.decomp = st
    let nbdiv = 1
    for (let i = 0; i < exposants.length; i++) {
      nbdiv *= (exposants[i] + 1)
    }
    sq.nbdiv = nbdiv
  }

  function getLatexDecomp () {
    sq.listePourCalc.giveFormula2('calc', sq.decomp)
    sq.listePourCalc.calculateNG()
    return sq.listePourCalc.getLatexCode(0)
  }

  function marqueEditeurPourErreur () {
    sq.marked = true
    $('#expressioninputmq1').css('background-color', '#FF9999')
  }

  function demarqueEditeurPourErreur () {
    if (!sq.marked) return
    sq.marked = false // Par précaution si appel via bouton recopier réponse trop tôt
    $('#expressioninputmq1').css('background-color', '#FFFFFF')
  }

  function validationEditeur () {
    let res
    const rep = j3pValeurde('expressioninputmq1')
    const chcalcul = traiteMathQuill(rep)
    const valide = sq.listePourCalc.verifieSyntaxe(chcalcul)
    if (valide) return true
    else {
      marqueEditeurPourErreur()
      res = false
    }
    if (!res) {
      marqueEditeurPourErreur()
      j3pFocus('expressioninputmq1')
    }
    return res
  }

  function montreEditeur (bVisible) {
    if (bVisible) $('#info').css('display', 'block')
    $('#editeur').css('display', bVisible ? 'block' : 'none')
    $('#boutonsmathquill').css('display', bVisible ? 'block' : 'none')
    // On vide le contenu de l’éditeur MathQuill et on lui donne le focus.
    if (bVisible) {
      $('#expressioninputmq1').mathquill('latex', ' ')
      j3pFocus('expressioninputmq1')
    }
  }

  function videEditeur () {
    $('#expressioninputmq1').mathquill('latex', ' ')
    j3pFocus('expressioninputmq1')
  }

  function creeEditeur () {
    j3pAffiche('editeur', 'expression', '$£a$ = &1&', { a: sq.n }) // sq.formulaire contient ou plusieurs éditeurs MathQuill
    // On affecte un indice à chaque éditeur ainsi qu’un écouteur de focus
    mqRestriction('expressioninputmq1', '\\d²\\^\\*', {
      boundingContainer: parcours.zonesElts.MG
    })
    j3pElement('expressioninputmq1').onkeyup = function () {
      onkeyup.call(this)
    }
  }

  function afficheReponse (bilan, depasse) {
    let ch, coul
    if (bilan === 'exact') {
      ch = textes.rep1
      coul = parcours.styles.cbien
    } else {
      if (bilan === 'faux') {
        ch = textes.rep2
        coul = parcours.styles.cfaux
      } else { // Exact mais pas fini
        ch = textes.rep3
        if (depasse) coul = parcours.styles.cfaux
        else coul = '#0000FF'
      }
    }
    const num = sq.numEssai
    const idrep = 'exp' + String(num)
    if (num > 2) ch = '<br>' + ch
    j3pAffiche('formules', idrep + 'deb', ch, { style: { color: coul } })
    ch = '$' + '\\textcolor{' + String(coul) + '}{' + String(sq.n) + ((bilan === 'faux') ? '\\ne' : '=') + sq.rep + '}$'
    j3pMathsAjouteDans('formules', { id: idrep, content: ch, parametres: { style: { couleur: coul } } })
  }

  function afficheSolution (bexact) {
    let stsol = textes.decompFournie + '<br>$' + sq.n + '=' + getLatexDecomp() + '$'
    if (sq.factprem.length === 1 && sq.exposants[0] === 1) stsol += '<br>' + sq.factprem[0] + ' ' + textes.estPrem
    const style = parcours.styles.petit.correction
    style.color = bexact ? 'green' : 'blue'
    style.marginLeft = '0'
    j3pAffiche('divsolution', 'textedecomp', stsol, { style })
  }

  function validation () {
    if (sq.numEssai >= 1) {
      j3pElement('boutonrecopier').style.display = 'block'
    }
    sq.rep = j3pValeurde('expressioninputmq1')
    const chcalcul = traiteMathQuill(sq.rep)
    sq.listePourCalc.giveFormula2('rep', chcalcul)
    sq.listePourCalc.giveFormula2('calc', sq.decomp)
    sq.listePourCalc.calculateNG()
    sq.resolu = sq.listePourCalc.valueOf('resolu') === 1
    sq.exact = sq.listePourCalc.valueOf('exact') === 1
  }

  function recopierReponse () {
    demarqueEditeurPourErreur()
    $('#expressioninputmq1').mathquill('latex', sq.rep)
    j3pFocus('expressioninputmq1')
  }

  function depart () {
    const mtgOptions = {
      // player only, sans figure initiale ni svg, on gère ça dans depart et modif_fig
      loadCoreWithMathJax: true
    }

    getMtgApp('mtg32svg', {}, mtgOptions).then((mtgAppLecteur) => {
      sq.mtgAppLecteur = mtgAppLecteur
      sq.listePourCalc = mtgAppLecteur.createList('TWF0aEdyYXBoSmF2YTEuMAAAABM+TMzNAAJmcv###wEA#wEAAAAAAAAAAARZAAACpQAAAQEAAAAAAAAAAQAAAAf#####AAAAAQAKQ0NhbGNDb25zdAD#####AAJwaQAWMy4xNDE1OTI2NTM1ODk3OTMyMzg0Nv####8AAAABAApDQ29uc3RhbnRlQAkh+1RELRj#####AAAAAQAHQ0NhbGN1bAD#####AANyZXAAATAAAAABAAAAAAAAAAAAAAACAP####8ABGNhbGMAATAAAAABAAAAAAAAAAD#####AAAAAQAFQ0ZvbmMA#####wAEemVybwASYWJzKHgpPDAuMDAwMDAwMDAx#####wAAAAEACkNPcGVyYXRpb24E#####wAAAAIACUNGb25jdGlvbgD#####AAAAAgARQ1ZhcmlhYmxlRm9ybWVsbGUAAAAAAAAAAT4RLgvoJtaVAAF4AAAAAgD#####AAVleGFjdAAOemVybyhyZXAtY2FsYyn#####AAAAAQAOQ0FwcGVsRm9uY3Rpb24AAAADAAAABAH#####AAAAAQAPQ1Jlc3VsdGF0VmFsZXVyAAAAAQAAAAgAAAAC#####wAAAAMAEENUZXN0RXF1aXZhbGVuY2UA#####wAGcmVzb2x1AAAAAQAAAAIAAAAAAAE#8AAAAAAAAAH#####AAAAAgAGQ0xhdGV4AP####8AAAAAAQAA#####xBAOgAAAAAAAEA2ZmZmZmZmAAAAAAAAAAAAAAAAAAEAAAAAAAAAAAAOXEZvclNpbXB7Y2FsY33###############8=')
      sq.listeADecomposer = listeADecomposer()
      sq.listeADecomposerInit = sq.listeADecomposer
      prepareNombre()
      sq.marked = false
      j3pAffiche('enonce', 'texte', textes.consigneInit, { a: sq.n })
      creeEditeur()
      j3pPaletteMathquill('boutonsmathquill', 'expressioninputmq1', { liste: ['puissance'], nomdiv: 'palette' })
      // Ligne suivante pour un bon alignement
      $('#palette').css('display', 'inline-block')
      j3pFocus('expressioninputmq1')
    }).catch(j3pShowError)
  }

  switch (this.etat) {
    case 'enonce':
      // code exécuté au lancement de la section
      if (this.debutDeLaSection) {
        // Construction de la page
        parcours.construitStructurePage('presentation1bis')
        // compteur.
        sq.numEssai = 1
        sq.nbEssais = parseInt(parcours.donneesSection.nbEssais)
        if (parcours.donneesSection.max < parcours.donneesSection.min) parcours.donneesSection.max = parcours.donneesSection.min

        parcours.afficheTitre('Trouver les diviseurs d’un entier')
        j3pDiv(parcours.zones.MG, { id: 'conteneur', contenu: '', style: parcours.styles.etendre('toutpetit.enonce', { padding: '6px' }) })
        j3pDiv('conteneur', 'enonce', '')
        j3pDiv('conteneur', {
          id: 'formules',
          contenu: '',
          style: parcours.styles.petit.enonce
        }) // Contient le formules entrées par l’élève
        j3pDiv('conteneur', 'divsolution', '') // Le div qui contiendra la correction
        j3pDiv('conteneur', 'info', '')
        nbe = parcours.donneesSection.nbEssais
        if (nbe === 1) j3pAffiche('info', 'texteinfo', textes.essai1, { style: { color: '#7F007F' } })
        else j3pAffiche('info', 'texteinfo', textes.essai2, { style: { color: '#7F007F' }, e: nbe })
        j3pDiv('conteneur', 'conteneurbouton', '')
        j3pAjouteBouton('conteneurbouton', 'boutonrecopier', 'MepBoutonsRepere', textes.recopier,
          recopierReponse)
        j3pElement('boutonrecopier').style.display = 'none'
        j3pDiv('conteneur', 'editeur', '') // Le div qui contiendra la chaine avec le ou les éditeurs MathQuill
        $('#editeur').css('padding-top', '10px') // Pour laisser un peu de place au-dessus de l’éditeur
        j3pDiv('conteneur', 'boutonsmathquill', '')
        $('#boutonsmathquill').css('display', 'inline-block').css('padding-top', '10px') // Pour laisser un peu de place au-dessus des boutons
        j3pDiv('conteneur', 'divcorrection', '') // Le div qui contiendra la correction
        depart()
        const style = parcours.styles.petit.correction
        style.marginTop = '1.5em'
        style.marginLeft = '0.5em'
        j3pAddElt(parcours.zonesElts.MD, 'div', '', { id: 'correction', style })
      } else {
        j3pEmpty('correction')
        this.afficheBoutonValider()
        sq.numEssai = 1
      }
      if (this.questionCourante > 1) {
        sq.marked = false
        prepareNombre()
        // videEditeur()
        j3pEmpty('enonce')
        j3pEmpty('formules')
        j3pEmpty('info')
        j3pEmpty('divsolution')
        j3pEmpty('divcorrection')
        j3pAffiche('enonce', 'texte', textes.consigneInit, { a: sq.n })
        nbe = parcours.donneesSection.nbEssais - sq.numEssai + 1
        if (nbe === 1) j3pAffiche('info', 'texteinfo', textes.validation, { styletexte: { couleur: '#7F007F' } })
        else j3pAffiche('info', 'texteinfo', textes.essai2, { styletexte: { couleur: '#7F007F' }, e: nbe })
        j3pElement('info').style.display = 'block'
        j3pEmpty('editeur')
        creeEditeur()
        montreEditeur(true)
        montrePaletteBoutons(true)
        // Pour revenir en haut de page
        try {
          window.location.hash = 'MepMG'
          parcours.zonesElts.MG.scrollIntoView()
        } catch (e) {}
        j3pFocus('expressioninputmq1')
      }
      this.finEnonce()

      break // case "enonce":

    case 'correction':
    {
      let bilanReponse = ''
      if (validationEditeur()) {
        validation()
        if (sq.resolu) bilanReponse = 'exact'
        else {
          if (sq.exact) {
            bilanReponse = 'exactPasFini'
          } else bilanReponse = 'faux'
        }
      } else {
        bilanReponse = 'incorrect'
      }
      // A cause de la limite de temps :
      if (this.isElapsed) { // limite de temps
        this._stopTimer()
        montreEditeur(false)
        j3pElement('correction').style.color = this.styles.cfaux
        j3pElement('boutonrecopier').style.display = 'none'
        j3pElement('info').style.display = 'none'
        afficheSolution(false)
        j3pElement('correction').innerHTML = tempsDepasse
        j3pElement('correction').innerHTML += '<br>' + textes.lasol
        this.etat = 'navigation'
        this.sectionCourante()
      } else {
        if (bilanReponse === 'incorrect') {
          j3pElement('correction').style.color = this.styles.cfaux
          j3pElement('correction').innerHTML = 'Réponse incorrecte'
          this.afficheBoutonValider()
        } else {
          // Une réponse a été saisie
          // Bonne réponse
          if (bilanReponse === 'exact') {
            sq.mtgAppLecteur.setActive('mtg32svg', false)
            this.score += 1
            j3pElement('correction').style.color = this.styles.cbien
            j3pElement('correction').innerHTML = cBien
            montreEditeur(false)
            sq.numEssai++ // Pour un affichage correct dans afficheReponse
            afficheReponse('exact', false)
            afficheSolution(true)
            this._stopTimer()
            j3pElement('boutonrecopier').style.display = 'none'
            j3pElement('info').style.display = 'none'
            this.etat = 'navigation'
            this.sectionCourante()
          } else {
            sq.numEssai++
            if (bilanReponse === 'exactPasFini') {
              j3pElement('correction').style.color =
                (sq.numEssai <= this.donneesSection.nbEssais) ? '#0000FF' : this.styles.cfaux
              j3pElement('correction').innerHTML = textes.pbDecomp
            } else {
              j3pElement('correction').style.color = this.styles.cfaux
              j3pElement('correction').innerHTML = cFaux + (sq.diviseursExacts ? '<br>' + textes.divAbsents : '')
            }
            j3pEmpty('info')
            if (sq.numeroessai <= this.donneesSection.nbEssais) {
              nbe = parcours.donneesSection.nbEssais - sq.numeroessai + 1
              if (nbe === 1) j3pAffiche('info', 'texteinfo', textes.essai1, { styletexte: { couleur: '#7F007F' } })
              else {
                j3pAffiche('info', 'texteinfo', textes.essai2, {
                  style: { color: '#7F007F' },
                  e: nbe
                })
              }
              videEditeur()
              afficheReponse(bilanReponse, false)
              // S’il y a plus de deux chances,on appelle à nouveau le bouton Valider
              j3pElement('correction').innerHTML += '<br>' + '<br>' + essaieEncore
            } else {
              // Erreur au nème essai
              this._stopTimer()
              sq.mtgAppLecteur.setActive('mtg32svg', false)
              montreEditeur(false)
              j3pElement('boutonrecopier').style.display = 'none'
              j3pElement('info').style.display = 'none'
              afficheSolution(false)
              afficheReponse(bilanReponse, true)
              j3pElement('correction').innerHTML += '<br><br>' + textes.lasol
              this.etat = 'navigation'
              this.sectionCourante()
            }
          }
        }
      }
      // Obligatoire
      this.finCorrection()
      break // case "correction":
    }
    case 'navigation':

      if (this.sectionTerminee()) {
        // On détermine la phrase d’état renvoyée par la section
        this.parcours.pe = this.score / this.donneesSection.nbitems
        this.afficheBoutonSectionSuivante()
        this.focus('sectioncontinuer')
      } else {
        this.etat = 'enonce'
        this.afficheBoutonSuite()
        this.focus('boutoncontinuer')
      }
      // Obligatoire
      this.finNavigation()

      break // case "navigation":
  }
}
