import { j3pAddContent, j3pAddElt, j3pAjouteBouton, j3pClone, j3pEmpty, j3pGetNewId, j3pShowError, j3pShuffle } from 'src/legacy/core/functions'
import { addDefaultTable } from 'src/legacy/themes/table'
import { getMtgCore } from 'src/lib/outils/mathgraph'
import getDefaultProgramme from './defaultProgramme'
import { j3pAffiche } from 'src/lib/mathquill/functions'
import { j3pCreeSVG } from 'src/legacy/core/functionsSvg'
import { placeJouerSon } from 'src/legacy/outils/zoneStyleMathquill/functions'
import textesGeneriques from 'src/lib/core/textes'
import './resMake.css'
const { cBien, cFaux, tempsDepasse } = textesGeneriques

async function initeditor (container, values, btn) {
  const { default: stor } = await import('./editorProgramme.js')
  return stor(container, values, btn) // la fct qui prend un conteneur et les params comme arguments
}

/**
 * Ajouter une description ici
 * @fileOverview
 * @author Tommy Barroy <t.barroy@laposte.net>
 * @since 2017-10
 */
export const params = {
  outils: ['calculatrice', 'mathquill'],
  /**
   * Ce qui est paramétrable dans l’éditeur de graphe
   * @type {LegacySectionParamsParametres}
   */
  parametres: [
    ['nbrepetitions', 9, 'entier', 'Nombre de répétitions de la section'],
    ['limite', 0, 'entier', 'Temps disponible par répétition, en secondes'],
    ['nbchances', 1, 'entier', 'Nombre d’essais par répétition'],
    ['Liste', JSON.stringify(getDefaultProgramme()), 'multiEditor', 'Json figure de base et figure attendue.', initeditor],
    ['ordonne', 'Oui', 'liste', '<u>Oui</u>:  Exercices dans l’ordre', ['Oui', 'Non']],
    ['theme', 'zonesAvecImageDeFond', 'liste', 'Modèle pour l’affichage', ['standard', 'zonesAvecImageDeFond']]
  ]
}

/**
 * section choix
 * @this {Parcours}
 */
export default function main () {
  const me = this
  const ds = me.donneesSection
  const stor = me.storage

  function initSection () {
    // chargement mtg
    me.construitStructurePage('presentation3')
    try {
      stor.Liste = JSON.parse(ds.Liste)
      const aComp = []
      //
      for (let prog = 0; prog < stor.Liste.liste.length; prog++) {
        let aCompte = true
        if (stor.Liste.liste[prog].image !== 'string') {
          if (stor.Liste.liste[prog].image.type === 'clone') {
            aCompte = false
            stor.Liste.liste[prog].image = j3pClone(aComp[stor.Liste.liste[prog].image.num])
          }
        }
        if (aCompte) aComp.push(j3pClone(stor.Liste.liste[prog].image))
        aCompte = true
        if (stor.Liste.liste[prog].yaSon) {
          if (stor.Liste.liste[prog].son !== 'string') {
            if (stor.Liste.liste[prog].son.type === 'clone') {
              aCompte = false
              stor.Liste.liste[prog].son = j3pClone(aComp[stor.Liste.liste[prog].son.num])
            }
          }
          if (aCompte) aComp.push(j3pClone(stor.Liste.liste[prog].son))
        }
        aCompte = true
        if (stor.Liste.liste[prog].txtFigure !== 'string') {
          if (stor.Liste.liste[prog].txtFigure.type === 'clone') {
            aCompte = false
            stor.Liste.liste[prog].txtFigure = j3pClone(aComp[stor.Liste.liste[prog].txtFigure.num])
          }
        }
        if (aCompte) aComp.push(j3pClone(stor.Liste.liste[prog].txtFigure))
      }
    } catch (error) {
      console.error(error)
      j3pShowError('Parametres invalide')
      stor.Legendes = getDefaultProgramme()
    }
    let titre = stor.Liste.titre
    if (!titre || titre === '') titre = ' '
    me.afficheTitre(titre)
    stor.lesExos = []
    if (ds.ordonne === 'Non') {
      const exPos = j3pShuffle(stor.Liste.liste)
      for (let i = 0; i < ds.nbrepetitions; i++) {
        stor.lesExos.push(exPos[i % exPos.length])
      }
    } else {
      for (let i = 0; i < ds.nbrepetitions; i++) {
        stor.lesExos.splice(0, 0, stor.Liste.liste[i % stor.Liste.liste.length])
      }
    }
    getMtgCore({ withMathJax: true, withApiPromise: true })
      .then(
        // success
        (mtgAppLecteur) => {
          stor.mtgAppLecteur = mtgAppLecteur
          enonceMain()
        },
        // failure
        (error) => {
          j3pShowError(error, { message: 'mathgraph n’est pas correctement chargé', mustNotify: true })
        })
      // plantage dans le code de success
      .catch(j3pShowError)
  }
  function creeLesDiv () {
    stor.lesdiv = {}
    stor.lesdiv.conteneur = j3pAddElt(me.zonesElts.MG, 'div', {
      style: me.styles.etendre('toutpetit.enonce', { padding: '10px' })
    })
    stor.concot = addDefaultTable(stor.lesdiv.conteneur, 1, 1)[0][0]
    const tab2 = addDefaultTable(stor.concot, 1, 5)
    stor.lesdiv.enonceG = addDefaultTable(tab2[0][0], 1, 1)[0][0]
    stor.lesdiv.son = addDefaultTable(tab2[0][0], 1, 1)[0][0]
    stor.lesdiv.son.style.display = 'none'
    stor.lesdiv.travail = j3pAddElt(stor.lesdiv.conteneur, 'div')
    stor.lesdiv.explications = j3pAddElt(stor.lesdiv.conteneur, 'div')
    stor.lesdiv.solution = j3pAddElt(stor.lesdiv.conteneur, 'div')
    stor.lesdiv.correction = tab2[0][3]
    stor.lesdiv.explications.style.color = me.styles.cfaux
    stor.lesdiv.solution.style.color = me.styles.colorCorrection
    tab2[0][0].classList.add('enonce')
    stor.lesdiv.zoneBout1 = tab2[0][1]
    stor.lesdiv.zoneBout2 = tab2[0][2]
    stor.lesdiv.zoneBout1.style.paddingLeft = '30px'
    stor.lesdiv.zoneBout2.style.paddingLeft = '30px'
    stor.lesdiv.correction.style.paddingLeft = '30px'
    stor.lesdiv.zoneBout2.style.display = 'none'
    stor.lesdiv.travail.classList.add('travail')
    me.zonesElts.MG.classList.add('fond')
    stor.tab2 = tab2
  }
  function agrandiMax () {
    stor.isAgrand = true
    stor.lesdiv.zoneBout1.style.display = 'none'
    stor.lesdiv.zoneBout2.style.display = ''
    me.zonesElts.HG.style.display = 'none'
    me.zonesElts.HD.style.display = 'none'
    const taillEnonce = stor.concot.getBoundingClientRect()
    stor.tailllimm = j3pClone(stor.limmm3.getBoundingClientRect())
    stor.tailltravail = j3pClone(stor.lesdiv.travail.getBoundingClientRect())
    stor.tailleMG = j3pClone(me.zonesElts.MG.getBoundingClientRect())
    const multMaxX = stor.tailleMG.width / stor.tailltravail.width
    const multMaxy = (stor.tailleMG.height + 100 - taillEnonce.height) / stor.tailltravail.height
    const aMul = (Math.min(multMaxX, multMaxy) - 0.05)
    stor.lesdiv.travail.style.width = stor.tailltravail.width * aMul + 'px'
    stor.limmm3.style.width = stor.tailllimm.width * aMul + 'px'
    stor.lesdiv.travail.style.height = stor.tailltravail.height * aMul + 'px'
    stor.limmm3.style.height = stor.tailllimm.height * aMul + 'px'
    me.zonesElts.MG.style.height = (stor.tailleMG.height + 120) + 'px'
    stor.aMul = aMul
    stor.lesdiv.travail.style.minWidth = stor.tailltravail.width * aMul + 'px'
    stor.lesdiv.travail.style.maxWidth = stor.tailltravail.width * aMul + 'px'
    stor.lesdiv.travail.style.minHeight = stor.tailltravail.height * aMul + 'px'
    stor.lesdiv.travail.style.maxHeight = stor.tailltravail.height * aMul + 'px'
    stor.leMG2.style.top = -stor.tailllimm.height * aMul + 'px'
    stor.leMG2.style.width = stor.tailltravail.width * aMul + 'px'
    stor.leMG2.style.height = stor.tailltravail.height * aMul + 'px'
    replaceMezZones(aMul)
    me.cacheBoutonValider()
  }
  function agrandiMin () {
    stor.isAgrand = false
    stor.lesdiv.zoneBout1.style.display = ''
    stor.lesdiv.zoneBout2.style.display = 'none'
    me.zonesElts.HG.style.display = ''
    me.zonesElts.HD.style.display = ''
    stor.lesdiv.travail.style.width = stor.tailltravail.width + 'px'
    stor.limmm3.style.width = stor.tailllimm.width + 'px'
    stor.lesdiv.travail.style.height = stor.tailltravail.height + 'px'
    stor.limmm3.style.height = stor.tailllimm.height + 'px'
    me.zonesElts.MG.style.height = stor.tailleMG.height + 'px'
    stor.lesdiv.travail.style.minWidth = stor.tailltravail.width + 'px'
    stor.lesdiv.travail.style.maxWidth = stor.tailltravail.width + 'px'
    stor.lesdiv.travail.style.minHeight = stor.tailltravail.height + 'px'
    stor.lesdiv.travail.style.maxHeight = stor.tailltravail.height + 'px'
    stor.leMG2.style.top = -stor.tailllimm.height + 'px'
    stor.leMG2.style.width = stor.tailltravail.width + 'px'
    stor.leMG2.style.height = stor.tailltravail.height + 'px'
    replaceMezZones(1)
  }
  function enonceMain () {
    stor.listCoulZone = []
    me.videLesZones()
    creeLesDiv()
    stor.lexo = stor.lesExos.pop()
    if (stor.lexo.consigneG !== '') {
      j3pAffiche(stor.lesdiv.enonceG, null, stor.lexo.consigneG)
      stor.lesdiv.enonceG.style.whiteSpace = 'normal'
    } else {
      stor.lesdiv.enonceG.style.display = 'none'
    }
    j3pAddContent(stor.lesdiv.enonceG, '\n')
    if (stor.lexo.yaSon) {
      stor.lesdiv.son.style.display = ''
      const contui = addDefaultTable(stor.lesdiv.son, 1, 1)[0][0]
      const ui = j3pAddElt(contui, 'div')
      ui.style.border = '1px solid black'
      ui.style.borderRadius = '5px'
      ui.style.padding = '5px'
      ui.style.paddingTop = '0px'
      ui.style.background = '#aeef59'
      ui.style.textAlign = 'center'
      placeJouerSon(ui, stor.lexo, stor, 'son')
    }
    if (stor.lexo.consigneG === '' && !stor.lexo.yaSon) stor.tab2[0][0].style.display = 'none'
    stor.idSvg2 = j3pGetNewId()
    stor.limmm3 = j3pAddElt(stor.lesdiv.travail, 'img', '', { src: stor.lexo.image })
    stor.lesdiv.travail.style.padding = 'Opx'
    stor.lesdiv.travail.style.margin = 'Opx'
    stor.limmm3.style.position = 'relative'
    stor.limmm3.style.top = '0px'
    stor.limmm3.style.left = '0px'
    stor.lesdiv.travail.style.minWidth = (287 + stor.lexo.width * 355) + 'px'
    stor.lesdiv.travail.style.maxWidth = (287 + stor.lexo.width * 355) + 'px'
    stor.lesdiv.travail.style.minHeight = (149 + stor.lexo.height * 280) + 'px'
    stor.lesdiv.travail.style.maxHeight = (149 + stor.lexo.height * 280) + 'px'
    stor.leMG2 = j3pCreeSVG(stor.lesdiv.travail, { id: stor.idSvg2, width: (287 + stor.lexo.width * 355), height: (149 + stor.lexo.height * 280) })
    stor.leMG2.style.position = 'relative'
    stor.mtgAppLecteur.removeAllDoc()
    stor.mtgAppLecteur.addDoc(stor.idSvg2, stor.lexo.txtFigure, false)
    stor.mtgAppLecteur.setApiDoc(stor.idSvg2)
    stor.mtgAppLecteur.calculate(stor.idSvg2)
    const aef = stor.mtgAppLecteur.setTag(stor.idSvg2, stor.idSvg2 + '#17', 'ss1')
    stor.mtgAppLecteur.setHidden({ elt: aef })
    try {
      for (let i = 1; i < 9; i++) {
        stor.mtgAppLecteur.setHidden({ elt: 'ss' + i })
      }
    } catch (e) {
      const el1 = []
      el1.push(stor.mtgAppLecteur.setTag(stor.idSvg2, stor.idSvg2 + '#71', 'ss1'))
      el1.push(stor.mtgAppLecteur.setTag(stor.idSvg2, stor.idSvg2 + '#70', '#ss2'))
      el1.push(stor.mtgAppLecteur.setTag(stor.idSvg2, stor.idSvg2 + '#69', '#ss3'))
      el1.push(stor.mtgAppLecteur.setTag(stor.idSvg2, stor.idSvg2 + '#68', '#ss4'))
      el1.push(stor.mtgAppLecteur.setTag(stor.idSvg2, stor.idSvg2 + '#67', '#ss5'))
      el1.push(stor.mtgAppLecteur.setTag(stor.idSvg2, stor.idSvg2 + '#65', '#ss6'))
      el1.push(stor.mtgAppLecteur.setTag(stor.idSvg2, stor.idSvg2 + '#64', '#ss7'))
      el1.push(stor.mtgAppLecteur.setTag(stor.idSvg2, stor.idSvg2 + '#63', '#ss8'))
      for (let i = 0; i < 8; i++) {
        stor.mtgAppLecteur.setHidden({ elt: el1[i] })
      }
    }
    stor.mtgAppLecteur.display(stor.idSvg2).then(() => {
      stor.leMG2.childNodes[1].setAttribute('fill', 'rgb(255,0,0,0)')
      stor.limmm3.style.width = (287 + stor.lexo.width * 355) + 'px'
      stor.limmm3.style.height = (149 + stor.lexo.height * 280) + 'px'
      stor.leMG2.style.position = 'relative'
      stor.leMG2.style.top = (-149 - stor.lexo.height * 280) + 'px'
      stor.leMG2.style.left = '0px'
      stor.lesdiv.travail.style.height = (149 + stor.lexo.height * 280 + 1) + 'px'
      stor.lesdiv.travail.style.maxHeight = (149 + stor.lexo.height * 280 + 1) + 'px'
      stor.lesdiv.travail.style.width = (287 + stor.lexo.width * 355) + 'px'
      stor.lesdiv.travail.style.border = '1px solid black'
      stor.leMG2.style.cursor = 'pointer'
      stor.lesdiv.travail.style.overflow = 'hidden'
      let tailleavrire = (149 + stor.lexo.height * 280) * 2
      if (stor.lexo.fond !== 'n') stor.leMG2.addEventListener('click', (event) => { makeEvent(event, -1) })
      stor.mesPtAbouge = []
      stor.listMondiv = []
      for (let i = 0; i < stor.lexo.listZone.length; i++) {
        if (stor.lexo.listZone[i].type !== 'label' && stor.lexo.listZone[i].type !== 'cache') {
          let aEve
          stor.mtgAppLecteur.setApiDoc(stor.idSvg2)
          for (let k = 0; k < stor.lexo.listZone[i].surface.length; k++) {
            stor.mtgAppLecteur.setHidden({ elt: stor.lexo.listZone[i].surface[k] })
          }
          stor.mtgAppLecteur.setHidden({ elt: stor.lexo.listZone[i].texte })
          switch (stor.lexo.listZone[i].type) {
            case 'cercle':
              stor.mtgAppLecteur.setHidden({ elt: stor.lexo.listZone[i].c })
              stor.mtgAppLecteur.setHidden({ elt: stor.lexo.listZone[i].p })
              stor.mtgAppLecteur.setVisible({ elt: stor.lexo.listZone[i].cercle })
              stor.mtgAppLecteur.setColor({ elt: stor.lexo.listZone[i].cercle, opacity: 0, color: '#000' })
              aEve = stor.lexo.listZone[i].cercle
              stor.mesPtAbouge.push({ name: stor.lexo.listZone[i].c, coo: stor.mtgAppLecteur.getPointPosition({ a: stor.lexo.listZone[i].c }) })
              stor.mesPtAbouge.push({ name: stor.lexo.listZone[i].p, coo: stor.mtgAppLecteur.getPointPosition({ a: stor.lexo.listZone[i].p }) })
              break
            case 'rectangle':
              stor.mtgAppLecteur.setHidden({ elt: stor.lexo.listZone[i].p1 })
              stor.mtgAppLecteur.setHidden({ elt: stor.lexo.listZone[i].p2 })
              stor.mtgAppLecteur.setHidden({ elt: stor.lexo.listZone[i].p3 })
              stor.mtgAppLecteur.setVisible({ elt: stor.lexo.listZone[i].poly })
              stor.mtgAppLecteur.setColor({ elt: stor.lexo.listZone[i].poly, opacity: 0, color: '#000' })
              stor.mesPtAbouge.push({ name: stor.lexo.listZone[i].p1, coo: stor.mtgAppLecteur.getPointPosition({ a: stor.lexo.listZone[i].p1 }) })
              stor.mesPtAbouge.push({ name: stor.lexo.listZone[i].p2, coo: stor.mtgAppLecteur.getPointPosition({ a: stor.lexo.listZone[i].p2 }) })
              stor.mesPtAbouge.push({ name: stor.lexo.listZone[i].p3, coo: stor.mtgAppLecteur.getPointPosition({ a: stor.lexo.listZone[i].p3 }) })
              aEve = stor.lexo.listZone[i].poly
              break
            case 'poly':
              stor.mtgAppLecteur.setHidden({ elt: stor.lexo.listZone[i].p1 })
              stor.mtgAppLecteur.setHidden({ elt: stor.lexo.listZone[i].p2 })
              stor.mtgAppLecteur.setHidden({ elt: stor.lexo.listZone[i].p3 })
              stor.mtgAppLecteur.setHidden({ elt: stor.lexo.listZone[i].p4 })
              stor.mtgAppLecteur.setVisible({ elt: stor.lexo.listZone[i].poly })
              stor.mtgAppLecteur.setColor({ elt: stor.lexo.listZone[i].poly, opacity: 0, color: '#000' })
              stor.mesPtAbouge.push({ name: stor.lexo.listZone[i].p1, coo: stor.mtgAppLecteur.getPointPosition({ a: stor.lexo.listZone[i].p1 }) })
              stor.mesPtAbouge.push({ name: stor.lexo.listZone[i].p2, coo: stor.mtgAppLecteur.getPointPosition({ a: stor.lexo.listZone[i].p2 }) })
              stor.mesPtAbouge.push({ name: stor.lexo.listZone[i].p3, coo: stor.mtgAppLecteur.getPointPosition({ a: stor.lexo.listZone[i].p3 }) })
              stor.mesPtAbouge.push({ name: stor.lexo.listZone[i].p4, coo: stor.mtgAppLecteur.getPointPosition({ a: stor.lexo.listZone[i].p4 }) })
              aEve = stor.lexo.listZone[i].poly
              break
            case 'circ':
              stor.mtgAppLecteur.setHidden({ elt: stor.lexo.listZone[i].p1 })
              stor.mtgAppLecteur.setHidden({ elt: stor.lexo.listZone[i].p2 })
              stor.mtgAppLecteur.setHidden({ elt: stor.lexo.listZone[i].p3 })
              stor.mtgAppLecteur.setVisible({ elt: stor.lexo.listZone[i].poly })
              stor.mtgAppLecteur.setColor({ elt: stor.lexo.listZone[i].poly, opacity: 0, color: '#000' })
              stor.mtgAppLecteur.setVisible({ elt: stor.lexo.listZone[i].c1 })
              stor.mtgAppLecteur.setColor({ elt: stor.lexo.listZone[i].c1, opacity: 0, color: '#000' })
              stor.mtgAppLecteur.setVisible({ elt: stor.lexo.listZone[i].c2 })
              stor.mtgAppLecteur.setColor({ elt: stor.lexo.listZone[i].c2, opacity: 0, color: '#000' })
              aEve = stor.lexo.listZone[i].poly
              stor.mtgAppLecteur.addEltListener({ elt: stor.lexo.listZone[i].c1, callBack: (event) => { makeEvent(event, i) }, eventName: 'click' })
              stor.mtgAppLecteur.addEltListener({ elt: stor.lexo.listZone[i].c2, callBack: (event) => { makeEvent(event, i) }, eventName: 'click' })
              stor.mesPtAbouge.push({ name: stor.lexo.listZone[i].p1, coo: stor.mtgAppLecteur.getPointPosition({ a: stor.lexo.listZone[i].p1 }) })
              stor.mesPtAbouge.push({ name: stor.lexo.listZone[i].p2, coo: stor.mtgAppLecteur.getPointPosition({ a: stor.lexo.listZone[i].p2 }) })
              stor.mesPtAbouge.push({ name: stor.lexo.listZone[i].p3, coo: stor.mtgAppLecteur.getPointPosition({ a: stor.lexo.listZone[i].p3 }) })
              break
            case 'polyg':
              for (let j = 0; j < stor.lexo.listZone[i].listSom.length; j++) {
                stor.mtgAppLecteur.setHidden({ elt: stor.lexo.listZone[i].listSom[j] })
                stor.mesPtAbouge.push({ name: stor.lexo.listZone[i].listSom[j], coo: stor.mtgAppLecteur.getPointPosition({ a: stor.lexo.listZone[i].listSom[j] }) })
              }
              stor.mtgAppLecteur.setVisible({ elt: stor.lexo.listZone[i].poly })
              stor.mtgAppLecteur.setColor({ elt: stor.lexo.listZone[i].poly, opacity: 0, color: '#000' })
              aEve = stor.lexo.listZone[i].poly
              break
          }
          stor.mtgAppLecteur.addEltListener({ elt: aEve, callBack: (event) => { makeEvent(event, i) }, eventName: 'click' })
          if (stor.lexo.survol) {
            stor.mtgAppLecteur.addEltListener({ elt: aEve, callBack: (event) => { makeEventOv(event, i) }, eventName: 'mouseover' })
            stor.mtgAppLecteur.addEltListener({ elt: aEve, callBack: (event) => { makeEventOu(event, i) }, eventName: 'mouseout' })
          }
        }
        if (stor.lexo.listZone[i].type === 'label') {
          const mondiv = j3pAddElt(stor.lesdiv.travail, 'div')
          mondiv.classList.add('pourFitContent')
          mondiv.style.position = 'relative'
          mondiv.style.verticalAlign = 'middle'
          mondiv.style.textAlign = 'center'
          mondiv.style.fontFamily = 'Bitstream Véracité Sans'
          mondiv.style.pointerEvents = 'none'
          mondiv.style.fontSize = stor.lexo.listZone[i].taille + 'px'
          j3pAffiche(mondiv, null, stor.lexo.listZone[i].texte)
          const p1 = stor.mtgAppLecteur.getPointPosition({ a: stor.lexo.listZone[i].p1 })
          mondiv.style.top = (p1.y - tailleavrire) + 'px'
          mondiv.style.left = p1.x + 'px'
          const aaj = mondiv.getBoundingClientRect()
          tailleavrire += aaj.height
          stor.mtgAppLecteur.setHidden({ elt: stor.lexo.listZone[i].p1 })
          stor.listMondiv.push(mondiv)
          mondiv.x = p1.x
          mondiv.y = p1.y
          mondiv.larg = aaj.width
          mondiv.height = aaj.height
        }
        if (stor.lexo.listZone[i].type === 'cache') {
          stor.mtgAppLecteur.setHidden({ elt: stor.lexo.listZone[i].texte })
          stor.mtgAppLecteur.setHidden({ elt: stor.lexo.listZone[i].p2 })
          stor.mtgAppLecteur.setHidden({ elt: stor.lexo.listZone[i].p1 })
          stor.mtgAppLecteur.setVisible({ elt: stor.lexo.listZone[i].poly })
          stor.mtgAppLecteur.setColor({ elt: stor.lexo.listZone[i].poly, opacity: 0, color: '#000' })
          stor.mesPtAbouge.push({ name: stor.lexo.listZone[i].p1, coo: stor.mtgAppLecteur.getPointPosition({ a: stor.lexo.listZone[i].p1 }) })
          stor.mesPtAbouge.push({ name: stor.lexo.listZone[i].p2, coo: stor.mtgAppLecteur.getPointPosition({ a: stor.lexo.listZone[i].p2 }) })
        }
      }
    })
    j3pAjouteBouton(stor.lesdiv.zoneBout1, agrandiMax, { value: '⬜' })
    j3pAjouteBouton(stor.lesdiv.zoneBout2, agrandiMin, { value: '◽' })
    me.finEnonce()
    me.cacheBoutonValider()
  }
  function replaceMezZones (mul) {
    const tailleLim = j3pClone(stor.limmm3.getBoundingClientRect())
    let tailleavrire = 2 * tailleLim.height
    for (let i = 0; i < stor.mesPtAbouge.length; i++) {
      stor.mtgAppLecteur.setPointPosition(stor.idSvg2, stor.mesPtAbouge[i].name, stor.mesPtAbouge[i].coo.x * mul, stor.mesPtAbouge[i].coo.y * mul, true)
    }

    for (let i = 0; i < stor.listMondiv.length; i++) {
      stor.listMondiv[i].style.top = (stor.listMondiv[i].y * mul - tailleavrire) + 'px'
      stor.listMondiv[i].style.left = (stor.listMondiv[i].x * mul) + 'px'
      stor.listMondiv[i].style.width = stor.listMondiv[i].larg * mul + 'px'
      stor.listMondiv[i].style.height = stor.listMondiv[i].height * mul + 'px'
      tailleavrire += stor.listMondiv[i].height * mul
    }
    for (let i = 0; i < stor.lexo.listZone.length; i++) {
      if (stor.lexo.listZone[i].surface && stor.lexo.listZone[i].type !== 'cache') {
        if (stor.listCoulZone[i]) {
          for (let k = 0; k < stor.lexo.listZone[i].surface.length; k++) {
            stor.mtgAppLecteur.setColor({ elt: stor.lexo.listZone[i].surface[k], color: stor.listCoulZone[i], opacity: 0.4 })
          }
        }
      }
    }
  }
  function makeEvent (ev, i) {
    ev.preventDefault()
    ev.stopPropagation()
    if (me.etat !== 'correction') return
    stor.repEl = i
    me.sectionCourante()
  }
  function makeEventOv (ev, i) {
    ev.preventDefault()
    ev.stopPropagation()
    if (me.etat !== 'correction') return
    if (stor.lexo.listZone[i].clicked) return
    for (let k = 0; k < stor.lexo.listZone[i].surface.length; k++) {
      stor.mtgAppLecteur.setVisible({ elt: stor.lexo.listZone[i].surface[k] })
      stor.mtgAppLecteur.setColor({ elt: stor.lexo.listZone[i].surface[k], color: '#aaa', opacity: 0.4 })
    }
  }
  function makeEventOu (ev, i) {
    ev.preventDefault()
    ev.stopPropagation()
    if (me.etat !== 'correction') return
    if (stor.lexo.listZone[i].clicked) return
    for (let k = 0; k < stor.lexo.listZone[i].surface.length; k++) {
      stor.mtgAppLecteur.setHidden({ elt: stor.lexo.listZone[i].surface[k] })
    }
  }
  function isRepOk () {
    j3pEmpty(stor.lesdiv.explications)
    stor.lesdiv.explications.classList.remove('explique')
    j3pEmpty(stor.lesdiv.solution)
    stor.lesdiv.solution.classList.remove('correction')
    if (stor.repEl === -1) {
      j3pAffiche(stor.lesdiv.explications, null, stor.lexo.avertFond)
      stor.lesdiv.explications.classList.add('explique')
      return false
    } else {
      stor.lexo.listZone[stor.repEl].clicked = true
      for (let k = 0; k < stor.lexo.listZone[stor.repEl].surface.length; k++) {
        stor.mtgAppLecteur.setVisible({ elt: stor.lexo.listZone[stor.repEl].surface[k] })
      }
      if (stor.lexo.listZone[stor.repEl].bon) {
        stor.listCoulZone[stor.repEl] = '#258f06'
        for (let k = 0; k < stor.lexo.listZone[stor.repEl].surface.length; k++) {
          stor.mtgAppLecteur.setColor({ elt: stor.lexo.listZone[stor.repEl].surface[k], color: '#258f06', opacity: 0.4 })
        }
        j3pAffiche(stor.lesdiv.solution, null, stor.lexo.listZone[stor.repEl].avert)
        stor.lesdiv.solution.classList.add('correction')
      } else {
        for (let k = 0; k < stor.lexo.listZone[stor.repEl].surface.length; k++) {
          stor.mtgAppLecteur.setColor({ elt: stor.lexo.listZone[stor.repEl].surface[k], color: '#d92500', opacity: 0.4 })
        }
        j3pAffiche(stor.lesdiv.explications, null, stor.lexo.listZone[stor.repEl].avert)
        stor.lesdiv.explications.classList.add('explique')
      }
      return stor.lexo.listZone[stor.repEl].bon === true
    }
  }

  function affcofo (isFin) {
    if (isFin) {
      for (let i = 0; i < stor.lexo.listZone.length; i++) {
        if (stor.lexo.listZone[i].surface && stor.lexo.listZone[i].type !== 'cache') {
          const el = stor.lexo.listZone[i]
          if (el.bon) {
            if (!el.clicked) {
              stor.listCoulZone[i] = '#023194'
              for (let k = 0; k < stor.lexo.listZone[i].surface.length; k++) {
                stor.mtgAppLecteur.setVisible({ elt: stor.lexo.listZone[i].surface[k] })
                stor.mtgAppLecteur.setColor({ elt: stor.lexo.listZone[i].surface[k], color: '#023194', opacity: 0.4 })
              }
              let aEv
              switch (el.type) {
                case 'cercle':
                  aEv = stor.lexo.listZone[i].cercle
                  break
                case 'rectangle':
                  aEv = stor.lexo.listZone[i].poly
                  break
                case 'poly':
                  aEv = stor.lexo.listZone[i].poly
                  break
                case 'circ':
                  aEv = stor.lexo.listZone[i].poly
                  stor.mtgAppLecteur.addEltListener({
                    elt: stor.lexo.listZone[i].c1,
                    eventName: 'mouseover',
                    callBack: () => {
                      j3pEmpty(stor.lesdiv.solution)
                      j3pAffiche(stor.lesdiv.solution, null, stor.lexo.listZone[i].avert)
                    }
                  })
                  stor.mtgAppLecteur.addEltListener({
                    elt: stor.lexo.listZone[i].c1,
                    eventName: 'click',
                    callBack: () => {
                      j3pEmpty(stor.lesdiv.solution)
                      j3pAffiche(stor.lesdiv.solution, null, stor.lexo.listZone[i].avert)
                    }
                  })
                  stor.mtgAppLecteur.addEltListener({
                    elt: stor.lexo.listZone[i].c1,
                    eventName: 'mouseout',
                    callBack: () => {
                      j3pEmpty(stor.lesdiv.solution)
                    }
                  })
                  stor.mtgAppLecteur.addEltListener({
                    elt: stor.lexo.listZone[i].c2,
                    eventName: 'mouseover',
                    callBack: () => {
                      j3pEmpty(stor.lesdiv.solution)
                      j3pAffiche(stor.lesdiv.solution, null, stor.lexo.listZone[i].avert)
                    }
                  })
                  stor.mtgAppLecteur.addEltListener({
                    elt: stor.lexo.listZone[i].c2,
                    eventName: 'click',
                    callBack: () => {
                      j3pEmpty(stor.lesdiv.solution)
                      j3pAffiche(stor.lesdiv.solution, null, stor.lexo.listZone[i].avert)
                    }
                  })
                  stor.mtgAppLecteur.addEltListener({
                    elt: stor.lexo.listZone[i].c2,
                    eventName: 'mouseout',
                    callBack: () => {
                      j3pEmpty(stor.lesdiv.solution)
                    }
                  })
                  break
                case 'polyg':
                  aEv = stor.lexo.listZone[i].poly
              }
              stor.mtgAppLecteur.addEltListener({
                elt: aEv,
                eventName: 'mouseover',
                callBack: () => {
                  j3pEmpty(stor.lesdiv.solution)
                  j3pAffiche(stor.lesdiv.solution, null, stor.lexo.listZone[i].avert)
                }
              })
              stor.mtgAppLecteur.addEltListener({
                elt: aEv,
                eventName: 'click',
                callBack: () => {
                  j3pEmpty(stor.lesdiv.solution)
                  j3pAffiche(stor.lesdiv.solution, null, stor.lexo.listZone[i].avert)
                }
              })
              stor.mtgAppLecteur.addEltListener({
                elt: aEv,
                eventName: 'mouseout',
                callBack: () => {
                  j3pEmpty(stor.lesdiv.solution)
                }
              })
            } else {
              stor.listCoulZone[i] = '#258f06'
            }
          } else {
            if (!el.clicked) {
              for (let k = 0; k < stor.lexo.listZone[i].surface.length; k++) {
                stor.mtgAppLecteur.setHidden({ elt: stor.lexo.listZone[i].surface[k] })
              }
            } else {
              stor.listCoulZone[i] = '#d92500'
            }
          }
        }
      }
    }
  }
  function yatout () {
    if (!stor.lexo.fautTout) return true
    for (let i = 0; i < stor.lexo.listZone.length; i++) {
      if (stor.lexo.listZone[i].bon) {
        if (!stor.lexo.listZone[i].clicked) return false
      }
    }
    return true
  }

  switch (this.etat) {
    case 'enonce':
      // code exécuté au lancement de la section
      if (me.debutDeLaSection) {
        initSection()
      } else {
        enonceMain()
      }
      break // case "enonce":

    case 'correction':
      // On teste si une réponse a été saisie

      if (me.isElapsed) {
        me._stopTimer()
        stor.lesdiv.correction.innerHTML += '<br>' + tempsDepasse + '<BR>'
        me.typederreurs[10]++

        affcofo(true)

        me.etat = 'navigation'
        me.sectionCourante()
      }

      if (isRepOk()) {
        if (yatout()) {
          me._stopTimer()
          stor.lesdiv.correction.style.color = me.styles.cbien
          stor.lesdiv.correction.innerHTML = cBien
          for (let i = 0; i < stor.lexo.listZone.length; i++) {
            if (stor.lexo.listZone[i].surface && stor.lexo.listZone[i].type !== 'cache') {
              const el = stor.lexo.listZone[i]
              if (el.bon) {
                if (!el.clicked) {
                  for (let k = 0; k < stor.lexo.listZone[i].surface.length; k++) {
                    stor.mtgAppLecteur.setVisible({ elt: stor.lexo.listZone[i].surface[k] })
                    stor.mtgAppLecteur.setColor({ elt: stor.lexo.listZone[i].surface[k], color: '#023194', opacity: 0.4 })
                  }
                  let aEv
                  switch (el.type) {
                    case 'cercle':
                      aEv = stor.lexo.listZone[i].cercle
                      break
                    case 'poly':
                      aEv = stor.lexo.listZone[i].poly
                      break
                    case 'rectangle':
                      aEv = stor.lexo.listZone[i].poly
                      break
                    case 'circ':
                      aEv = stor.lexo.listZone[i].poly
                      stor.mtgAppLecteur.addEltListener({
                        elt: stor.lexo.listZone[i].c1,
                        eventName: 'mouseover',
                        callBack: () => {
                          j3pEmpty(stor.lesdiv.solution)
                          j3pAffiche(stor.lesdiv.solution, null, stor.lexo.listZone[i].avert)
                        }
                      })
                      stor.mtgAppLecteur.addEltListener({
                        elt: stor.lexo.listZone[i].c1,
                        eventName: 'click',
                        callBack: () => {
                          j3pEmpty(stor.lesdiv.solution)
                          j3pAffiche(stor.lesdiv.solution, null, stor.lexo.listZone[i].avert)
                        }
                      })
                      stor.mtgAppLecteur.addEltListener({
                        elt: stor.lexo.listZone[i].c1,
                        eventName: 'mouseout',
                        callBack: () => {
                          j3pEmpty(stor.lesdiv.solution)
                        }
                      })

                      stor.mtgAppLecteur.addEltListener({
                        elt: stor.lexo.listZone[i].c2,
                        eventName: 'mouseover',
                        callBack: () => {
                          j3pEmpty(stor.lesdiv.solution)
                          j3pAffiche(stor.lesdiv.solution, null, stor.lexo.listZone[i].avert)
                        }
                      })
                      stor.mtgAppLecteur.addEltListener({
                        elt: stor.lexo.listZone[i].c2,
                        eventName: 'click',
                        callBack: () => {
                          j3pEmpty(stor.lesdiv.solution)
                          j3pAffiche(stor.lesdiv.solution, null, stor.lexo.listZone[i].avert)
                        }
                      })
                      stor.mtgAppLecteur.addEltListener({
                        elt: stor.lexo.listZone[i].c2,
                        eventName: 'mouseout',
                        callBack: () => {
                          j3pEmpty(stor.lesdiv.solution)
                        }
                      })
                      break
                  }
                  stor.mtgAppLecteur.addEltListener({
                    elt: aEv,
                    eventName: 'mouseover',
                    callBack: () => {
                      j3pEmpty(stor.lesdiv.solution)
                      j3pAffiche(stor.lesdiv.solution, null, stor.lexo.listZone[i].avert)
                    }
                  })
                  stor.mtgAppLecteur.addEltListener({
                    elt: aEv,
                    eventName: 'click',
                    callBack: () => {
                      j3pEmpty(stor.lesdiv.solution)
                      j3pAffiche(stor.lesdiv.solution, null, stor.lexo.listZone[i].avert)
                    }
                  })
                  stor.mtgAppLecteur.addEltListener({
                    elt: aEv,
                    eventName: 'mouseout',
                    callBack: () => {
                      j3pEmpty(stor.lesdiv.solution)
                    }
                  })
                }
              } else {
                if (!el.clicked) {
                  for (let k = 0; k < stor.lexo.listZone[i].surface.length; k++) {
                    stor.mtgAppLecteur.setHidden({ elt: stor.lexo.listZone[i].surface[k] })
                  }
                }
              }
            }
          }
          me.score++
          me.typederreurs[0]++
          me.etat = 'navigation'
          me.sectionCourante()
        } else {
          return
        }
      } else {
        // Pas de bonne réponse
        stor.lesdiv.correction.style.color = me.styles.cfaux
        stor.lesdiv.correction.innerHTML = cFaux
        if (me.essaiCourant >= me.donneesSection.nbchances) {
          // Erreur au nème essai
          me._stopTimer()
          me.cacheBoutonValider()
          stor.lesdiv.correction.innerHTML += '<br> Clique/Survole la bonne zone,<br> pour une indication.'
          affcofo(true)

          me.typederreurs[2]++
          me.etat = 'navigation'
          me.sectionCourante()
        }
      }

      // Obligatoire
      me.finCorrection()
      break // case "correction":

    case 'navigation':

      if (me.sectionTerminee()) {
        // On détermine la phrase d’état renvoyée par la section
        me.afficheBoutonSectionSuivante()
        me.focus('sectioncontinuer')
      } else {
        me.etat = 'enonce'
        me.afficheBoutonSuite()
        me.focus('boutoncontinuer')
      }
      // Obligatoire
      me.finNavigation()
      break // case "navigation":
  }
}
