import { j3pAddContent, j3pAddElt, j3pAjouteBouton, j3pAjouteCaseCoche, j3pClone, j3pDetruit, j3pEmpty, j3pFocus, j3pGetNewId, j3pShowError, j3pValeurde } from 'src/legacy/core/functions'
import { faitSon, importImage, j3pModale2, j3pPaletteMathquill2, placeJouerSon } from 'src/legacy/outils/zoneStyleMathquill/functions'
import loadMq from 'src/lib/mathquill/loadMathquill'
import { getMtgCore } from 'src/lib/outils/mathgraph'
import { j3pCreeSVG } from 'src/legacy/core/functionsSvg'
import ListeDeroulante from 'src/legacy/outils/listeDeroulante/ListeDeroulante'
import { addDefaultTable } from 'src/legacy/themes/table'
import { j3pAffiche, mqRestriction } from 'src/lib/mathquill/functions'
import './resMake.css'
import rondImg from './rond.png'
import quadImg from './quad.png'
import rectangleImg from './rect.png'
import circImg from './circ.png'

const genMGEx = 'TWF0aEdyYXBoSmF2YTEuMAAAABUAAmZy####AQD#AQAAAAAAAAAAA1wAAAKDAAABAQAAAAAAAAAAAAAASP####8AAAABAApDQ2FsY0NvbnN0AP####8AAnBpABYzLjE0MTU5MjY1MzU4OTc5MzIzODQ2#####wAAAAEACkNDb25zdGFudGVACSH7VEQtGP####8AAAABAApDUG9pbnRCYXNlAP####8BgAD#P4AAAAAQAAFIAAAAAAAAAAAAQAgAAAAAAAAAAAkAAUCEGAAAAAAAQHvj1wo9cKQAAAACAP####8BgAD#P4AAAAAQAAFDAAAAAAAAAAAAQAgAAAAAAAAAAAkAAUCEsAAAAAAAQGMHrhR64Uj#####AAAAAQAUQ0Ryb2l0ZURpcmVjdGlvbkZpeGUA#####wGAAP8#gAAAARAAAAEAAAIEAAAAAQE#8AAAAAAAAAAAAAMA#####wGAAP8#gAAAARAAAAEAAAIEAAAAAgA#8AAAAAAAAP####8AAAABAA9DUG9pbnRMaWVEcm9pdGUA#####wGAAP8#gAAAABAAAUUAAAAAAAAAAABACAAAAAAAAAAACQABQHFAAAAAAAAAAAAEAAAABAD#####AYAA#z+AAAAAEAABRgAAAAAAAAAAAEAIAAAAAAAAAAAJAAHAdiAAAAAAAAAAAAP#####AAAAAQAIQ1NlZ21lbnQA#####wCAAP8#gAAAABAAAAEAAAAEAAAAAgAAAAUAAAAFAP####8AgAD#P4AAAAAQAAABAAAABAAAAAEAAAAGAAAABAD#####AP8AAD+AAAABEAABRwAAAAAAAAAAAEAIAAAAAAAAAAAIAAE#0YlmsHO0ygAAAAgAAAAEAP####8A#wAAP4AAAAEQAAFEAAAAAAAAAAAAQAgAAAAAAAAAAAgAAT#mQshZCyFkAAAABwAAAAMA#####wH#AAA#gAAAARAAAAEABnZlcnRpYwAEAAAACQA#8AAAAAAAAAAAAAMA#####wH#AAA#gAAAARAAAAEAAAAEAAAACgE#8AAAAAAAAP####8AAAABABBDSW50RHJvaXRlRHJvaXRlAP####8A#wAAP4AAAAAQAAAAAAAAAAAAAABACAAAAAAAAAADbGltCAAAAAALAAAADAAAAAQA#####wD#AAA#gAAAABAAAAAAAAAAAAAAAEAIAAAAAAAAAAAIAAHAfIAAAAAAAAAAAAsAAAAEAP####8A#wAAP4AAAAAQAAAAAAAAAAAAAABACAAAAAAAAAAACAABwIUYAAAAAAAAAAAMAAAABQD#####AP8AAD+AAAAAEAAAAQAAAAQAAAANAAAADgAAAAUA#####wD#AAA#gAAAABAAAAEAAAAEAAAADQAAAA8AAAACAP####8B#wAAP4AAAAAQAAFBAAAAAAAAAAAAQAgAAAAAAAAAAAgAAUCAGAAAAAAAQGDcKPXCj1wAAAACAP####8B#wAAP4AAAAAQAAFCAAAAAAAAAAAAQAgAAAAAAAAAAAgAAUCA+AAAAAAAQGDcKPXCj1z#####AAAAAQAJQ0xvbmd1ZXVyAP####8AAAASAAAAE#####8AAAABABRDSW1wbGVtZW50YXRpb25Qcm90bwD#####ABJNZXN1cmUgZGUgbG9uZ3VldXIAAAAFAAAAAgAAAAIAAAACAAAACv####8AAAABAAtDTWVkaWF0cmljZQAAAAAVAQAAAD+AAAAAEAAAAQAAAAEAAAACAAAACv####8AAAABAAdDTWlsaWV1AAAAABUBAAAAP4AAAAAQAAABAAAFAAAAAAIAAAAK#####wAAAAIACUNDZXJjbGVPUgAAAAAVAQAAAD+AAAAAAAABAAAAFwAAAAFAMAAAAAAAAAH#####AAAAAQAQQ0ludERyb2l0ZUNlcmNsZQAAAAAVAAAAFgAAABj#####AAAAAQAQQ1BvaW50TGllQmlwb2ludAAAAAAVAQAAAD+AAAAAEAAAAQAABQABAAAAGQAAAAcBAAAAFQAAAAIAAAAK#####wAAAAEAD0NWYWxldXJBZmZpY2hlZQEAAAAVAf8AAD+AAAABAAAAAAAaEQAAAAAAAQAAAAEAAAABAAAAAAAAAAAAAAAAAAEAAAAbAAAACAD#####ABJNZXN1cmUgZGUgbG9uZ3VldXIAAAAFAAAAAgAAAAIAAAACAAAABQAAAAkAAAAAHQEAAAA#gAAAABAAAAEAAAABAAAAAgAAAAUAAAAKAAAAAB0BAAAAP4AAAAAQAAABAAAFAAAAAAIAAAAFAAAACwAAAAAdAQAAAD+AAAAAAAABAAAAHwAAAAFAMAAAAAAAAAEAAAAMAAAAAB0AAAAeAAAAIAAAAA0AAAAAHQEAAAA#gAAAABAAAAEAAAUAAQAAACEAAAAHAQAAAB0AAAACAAAABQAAAA4BAAAAHQH#AAA#gAAAAQAAAAAAIhEAAAAAAAEAAAABAAAAAQAAAAAAAAAAAAAAAAABAAAAIwAAAAgA#####wASTWVzdXJlIGRlIGxvbmd1ZXVyAAAABQAAAAIAAAACAAAABgAAAAkAAAAJAAAAACUBAAAAP4AAAAAQAAABAAAAAQAAAAYAAAAJAAAACgAAAAAlAQAAAD+AAAAAEAAAAQAABQAAAAAGAAAACQAAAAsAAAAAJQEAAAA#gAAAAAAAAQAAACcAAAABQDAAAAAAAAABAAAADAAAAAAlAAAAJgAAACgAAAANAAAAACUBAAAAP4AAAAAQAAABAAAFAAEAAAApAAAABwEAAAAlAAAABgAAAAkAAAAOAQAAACUB#wAAP4AAAAEAAAAAACoRAAAAAAABAAAAAQAAAAEAAAAAAAAAAAAAAAAAAQAAACsAAAAIAP####8AEk1lc3VyZSBkZSBsb25ndWV1cgAAAAUAAAACAAAAAgAAAAYAAAABAAAACQAAAAAtAQAAAD+AAAAAEAAAAQAAAAEAAAAGAAAAAQAAAAoAAAAALQEAAAA#gAAAABAAAAEAAAUAAAAABgAAAAEAAAALAAAAAC0BAAAAP4AAAAAAAAEAAAAvAAAAAUAwAAAAAAAAAQAAAAwAAAAALQAAAC4AAAAwAAAADQAAAAAtAQAAAD+AAAAAEAAAAQAABQABAAAAMQAAAAcBAAAALQAAAAYAAAABAAAADgEAAAAtAf8AAD+AAAABAAAAAAAyEQAAAAAAAQAAAAEAAAABAAAAAAAAAAAAAAAAAAEAAAAz#####wAAAAEAB0NDYWxjdWwA#####wABQQAFQ0QvQ0X#####AAAAAQAKQ09wZXJhdGlvbgP#####AAAAAQAPQ1Jlc3VsdGF0VmFsZXVyAAAAGwAAABEAAAAjAAAADwD#####AAFCAAVGRy9GSAAAABADAAAAEQAAACsAAAARAAAAMwAAAAIA#####wEAAAA#gAAAABAAAAAAAAAAAAAAAEAIAAAAAAAAAAAIAAFAaMAAAAAAAECDaeuFHrhSAAAAAwD#####AQAAAD+AAAABEAAAAQAAAAEAAAA3AT#wAAAAAAAAAAAABgD#####AQAAAD+AAAAAEAAAAAAAAAAAAAAAQAgAAAAAAAAAAAgAAAAACwAAADgAAAAEAP####8BAAAAP4AAAAAQAAAAAAAAAAAAAABACAAAAAAAAAAACAABQIPgAAAAAAAAAAA4AAAAAwD#####AQAAAD+AAAABEAAAAQAAAAEAAAAOAT#wAAAAAAAAAAAAAwD#####AQAAAD+AAAABEAAAAQAAAAEAAAA6AD#wAAAAAAAAAAAABgD#####AQAAAD+AAAAAEAAAAAAAAAAAAAAAQAgAAAAAAAAAAAgAAAAAOwAAADz#####AAAAAQAJQ1BvbHlnb25lAP####8BAAAAP4AAAAAAAAEAAAAFAAAAPQAAADoAAAA5AAAADgAAAD3#####AAAAAQAQQ1N1cmZhY2VQb2x5Z29uZQD#####AP###z5MzM0AA3NzOAAAAAQAAAA+AAAAAwD#####Af###z+AAAABEAAAAQAAAAEAAAAPAD#wAAAAAAAAAAAABgD#####AP###z+AAAAAEAAAAAAAAAAAAAAAQAgAAAAAAAAAA3NzNwgAAAAAOAAAAEAAAAASAP####8B####P4AAAAAAAAEAAAAFAAAADwAAAEEAAAA5AAAADQAAAA8AAAATAP####8A####PkzMzQADc3MxAAAABAAAAEL#####AAAAAQAOQ09iamV0RHVwbGlxdWUA#####wCAAP8#gAAAAANzczIAAAAIAAAAFAD#####AIAA#z+AAAAAA3NzMwAAAAcAAAAUAP####8A#wAAP4AAAAADc3M0AAAACgAAABQA#####wD#AAA#gAAAAANzczUAAAAJAAAAFP##########'
const genIm = 'data:image/png;base64,iVBORw0KGgoAAAANSUhEUgAAAFwAAABKCAYAAAA2YDPeAAAAAXNSR0IArs4c6QAAAARnQU1BAACxjwv8YQUAAAAJcEhZcwAADsMAAA7DAcdvqGQAAAKmSURBVHhe7ZTBkuIwDAXnz/l0tnTwzFvTJHYiy0riruoTIEt94Oe9CGUFD2YFD2YFD2YFD2YFD+Yj+Ov1+nXhz3/BNfaKPobd4MWFD83BzcV5Nv/DycUnPX0+ghs6gFz80dsHgxdomPpkqEdxi83gBg1Unwh1ULfYDW7QUPVJ0P3qHk3BDRquPgG6W22hOXiBHlLvCN2p9tAd3KBH1TtB96m9HApu0OPqHaC71CMcDm7QEupVoVvUM5wKXqCl1CtB+6tncQlu0HLqFaC9VQ/cghu0pJoV2rXWC9fgBi2rZoN2VL1xD16g5dUM0F7qCIYFN+gIdRa0S+0ohgY36Bg1GtpBHc3w4AYdVhsBvatGEBK8QEeqo6C3aqMIDW7Qsao39IYaTXhwgw5XvaDZ6gymBC9QBPUoNKt2FlODGxRD7YVmqLOZHtygMGor9Fs1AymCGxRI3YN+o2YhTfACxVJr6DtqNtIFNyicWqDP1IykDG5QwB6zkja4QSH3zE7q4AUKS16BSwQ3KLB6FdIHp7jfvALrPzyYtMEpZo9ZSRec4tUW6DM1I6mCUzSVoO+p2UgTnGKpW9D3a7MwPTjFqW2FfqtmYGpwiqIegeaos5kWnGKoZ6B56kzCg1OAWi9otjqD0OB0tDoCekeNJiw4HauOhN5TIwkJTkeqEdC7ahRDg9Nh6gxoD3U0w4LTMepMaB91JEOC0xFqBmgvdRSuwWlxNRu0ozoCt+C0sJoZ2lf1xCU4LaleAdpb9eJUcFqs9krQ/qoHh4PTQupVoVvUsxwKTouod4DuUo/SFZwerr0TdJ96hObg9KB6V+hWtZem4PSQenfo5mIvm8Hpgdon4XH/1+A0XH0qZxtgcB1KLo7THXxxjubgCx+a/sMXfnwNvhjDCh7K+/0PVwF0/feJqwcAAAAASUVORK5CYII='
const listColor = ['#FF0', '#2d5d00', '#00F', '#fd4200', '#f00', '#000']

export default function editorProgramme (container, values) {
  console.debug('editorProgramme avec les valeurs initiales', values)
  const editor = {}
  const valuesDeb = values
  function creeLesDiv () {
    editor.conteneur = container
    editor.divUp = j3pAddElt(editor.conteneur, 'div')
    editor.divExo = j3pAddElt(editor.conteneur, 'div')
    editor.figDep = j3pAddElt(editor.conteneur, 'div')
    editor.divUp.style.minWidth = '900px'
    editor.divUp.style.border = '5px solid black'
    editor.divUp.style.borderRadius = '10px'
    editor.divUp.style.padding = '10px'
    editor.divUp.style.background = '#47ffff'
    editor.figDep.style.border = '5px solid black'
    editor.figDep.style.borderRadius = '10px'
    editor.figDep.style.background = '#ffFFFF'
    editor.figDep.classList.add('pourFitContent')
    editor.divExo.style.border = '5px solid black'
    editor.divExo.style.background = '#ccccff'
    editor.divExo.style.borderRadius = '10px'

    editor.div7 = j3pAddElt(container, 'div', '', {
      style: {
        background: '#e54949',
        border: '7px solid black',
        borderRadius: '10px',
        padding: '10px'
      }
    })
    j3pAddContent(editor.div7, '\n')
    editor.div7.style.fontSize = '40px'
    editor.div7.style.verticalAlign = 'middle'
    editor.div7.style.textAlign = 'center'
    editor.div7.style.color = 'white'
    j3pAddContent(editor.div7, 'Ne pas cliquer sur le bouton enregistrer ci dessous, <br> (mais sur le bouton valider plus haut)')
    j3pAddContent(editor.div7, '\n')
    j3pAddContent(editor.div7, '\n')
    editor.div7.style.display = 'none'
  }
  function menuTotal () {
    const yuyu = j3pAddElt(editor.divUp, 'span')
    j3pAddContent(yuyu, '<b><u>Titre de la ressource:</u></b>&nbsp;')
    editor.inputTitre = j3pAddElt(yuyu, 'input', '', {
      value: valuesDeb.Liste.titre,
      size: 50
    })
    j3pAddContent(editor.divUp, '\n')
    j3pAddContent(editor.divUp, '\n')

    const yuyu7 = j3pAddElt(editor.divUp, 'span')
    j3pAddContent(yuyu7, '<b><u>Exercice chronométré:</u></b>&nbsp;')
    const ldOptions2 = { dansModale: true, speMod: 10, speMod2: 70, j3pCont: container, choix0: true, onChange: faisLimite }
    const ll2 = (valuesDeb.limite === 0 || valuesDeb.limite === '') ? ['Non', 'Oui'] : ['Oui', 'Non']
    editor.listeparamLimite = ListeDeroulante.create(editor.divUp, ll2, ldOptions2)
    editor.chronoCahc2 = j3pAddElt(editor.divUp, 'span')
    j3pAddContent(editor.chronoCahc2, '&nbsp;Temps (en secondes):&nbsp;')
    editor.inputLimite = j3pAddElt(editor.chronoCahc2, 'input', '', {
      type: 'number',
      value: (valuesDeb.limite !== '' && valuesDeb.limite !== 0) ? valuesDeb.limite : 20,
      size: 3,
      min: '1',
      max: '999'
    })
    editor.inputLimite.addEventListener('change', () => {
      const averif = editor.inputLimite.value
      if (averif === '' || isNaN(averif) || (Number(averif) < 0) || (Number(averif) > 1000)) editor.inputLimite.value = '20'
    })
    faisLimite()
    j3pAddContent(editor.divUp, '\n')
    j3pAddContent(editor.divUp, '\n')

    const yu2 = j3pAddElt(editor.divUp, 'span')
    j3pAddContent(yu2, '<b><u>Nombre d’exercices proposées</u></b>:&nbsp;')
    editor.nbrepetitions = j3pAddElt(editor.divUp, 'input', '', {
      type: 'number',
      value: valuesDeb.nbrepetitions,
      size: 4,
      min: '1',
      max: '10'
    })
    editor.nbrepetitions.addEventListener('change', () => {
      const averif = editor.nbrepetitions.value
      if (averif === '' || isNaN(averif) || (Number(averif) < 1) || (Number(averif) > 10)) editor.nbrepetitions.value = '1'
    })
    j3pAddContent(editor.divUp, '\n')
    j3pAddContent(editor.divUp, '\n')

    const yu9921 = j3pAddElt(editor.divUp, 'span')
    j3pAddContent(yu9921, '<b><u>Exercices ordonnés:</u></b>&nbsp;')
    const ll9921 = (valuesDeb.ordonne === 'Oui') ? ['Oui', 'Non'] : ['Non', 'Oui']
    editor.ordonneL = ListeDeroulante.create(yu9921, ll9921, ldOptions2)
    j3pAddContent(editor.divUp, '\n')
    j3pAddContent(editor.divUp, '\n')

    const yu991 = j3pAddElt(editor.divUp, 'span')
    j3pAddContent(yu991, '<b><u>Theme:</u></b>&nbsp;')
    const ll991 = (valuesDeb.theme === 'standard') ? ['standard', 'zonesAvecImageDeFond'] : ['zonesAvecImageDeFond', 'standard']
    editor.themeL = ListeDeroulante.create(yu991, ll991, ldOptions2)
    j3pAddContent(editor.divUp, '\n')
    j3pAddContent(editor.divUp, '\n')
  }

  function menuExo () {
    editor.okVire = false
    editor.figDep.style.display = 'none'
    editor.yaBlok = false
    j3pEmpty(editor.figDep)
    j3pEmpty(editor.divExo)
    const tab3 = addDefaultTable(editor.divExo, 3, 3)
    j3pAddContent(tab3[0][0], '&nbsp;')
    j3pAddContent(tab3[0][2], '&nbsp;')
    const tab4 = addDefaultTable(tab3[0][1], 1, 7)
    j3pAddContent(tab4[0][0], '<b>Liste des exercices disponibles</b>')
    tab3[0][0].style.textAlign = 'center'
    j3pAddContent(tab4[0][1], '&nbsp;&nbsp;')
    j3pAddContent(tab4[0][3], '&nbsp;&nbsp;')
    j3pAddContent(tab4[0][5], '&nbsp;&nbsp;')
    editor.divExoliste = tab3[1][1]
    j3pAjouteBouton(tab4[0][2], importExo, { value: 'Dupliquer l’ensemble des exercices' })

    const tab = addDefaultTable(editor.divExoliste, editor.ProgrammeS.length + 2, 11)
    for (let i = 0; i < editor.ProgrammeS.length; i++) {
      tab[i + 1][0].style.borderBottom = '1px solid black'
      tab[i + 1][0].style.borderTop = '1px solid black'
      tab[i + 1][0].style.borderLeft = '1px solid black'
      tab[i + 1][1].style.borderBottom = '1px solid black'
      tab[i + 1][1].style.borderTop = '1px solid black'
      tab[i + 1][3].style.borderBottom = '1px solid black'
      tab[i + 1][3].style.borderTop = '1px solid black'
      tab[i + 1][2].style.borderBottom = '1px solid black'
      tab[i + 1][2].style.borderTop = '1px solid black'
      tab[i + 1][4].style.borderBottom = '1px solid black'
      tab[i + 1][4].style.borderTop = '1px solid black'
      tab[i + 1][5].style.borderBottom = '1px solid black'
      tab[i + 1][5].style.borderTop = '1px solid black'
      tab[i + 1][6].style.borderBottom = '1px solid black'
      tab[i + 1][6].style.borderTop = '1px solid black'
      tab[i + 1][6].style.borderRight = '1px solid black'
      tab[i + 1][6].style.padding = '5px 5px 5px 5px'
      tab[i + 1][2].style.padding = '5px 5px 5px 5px'
      tab[i + 1][4].style.padding = '5px 5px 5px 5px'
      j3pAddContent(tab[i + 1][1], '&nbsp;')
      j3pAddContent(tab[i + 1][3], '&nbsp;')
      j3pAddContent(tab[i + 1][5], '&nbsp;')
      j3pAddContent(tab[i + 1][7], '&nbsp;')
      j3pAddContent(tab[i + 1][9], '&nbsp;')
      const bufadd = (editor.ProgrammeS[i].nom) ? ' (' + editor.ProgrammeS[i].nom + ')' : ''
      j3pAffiche(tab[i + 1][0], null, '&nbsp;Exercice ' + (i + 1) + bufadd + '&nbsp;')
      j3pAjouteBouton(tab[i + 1][2], () => { modifExo(i) }, { value: 'Modifier' })
      j3pAjouteBouton(tab[i + 1][4], () => { supExo(i) }, { value: 'Supprimer' })
      j3pAjouteBouton(tab[i + 1][6], () => { DupExo(i) }, { value: 'Dupliquer' })
      if (i !== 0) {
        j3pAjouteBouton(tab[i + 1][8], () => { makeUp(i) }, { value: '↑' })
      } else {
        j3pAddContent(tab[i + 1][8], '&nbsp;&nbsp;&nbsp;')
      }
      if (i !== editor.ProgrammeS.length - 1) j3pAjouteBouton(tab[i + 1][10], () => { MakeDown(i) }, { value: '↓' })
    }
    j3pAddContent(tab[tab.length - 2][0], '&nbsp;')
    j3pAjouteBouton(tab[tab.length - 1][0], ajouteExo, { value: '+' })
  }
  function makeUp (i) {
    const acons = editor.ProgrammeS[i]
    editor.ProgrammeS.splice(i, 1)
    editor.ProgrammeS.splice(i - 1, 0, acons)
    menuExo()
  }
  function MakeDown (i) {
    const acons = editor.ProgrammeS[i]
    editor.ProgrammeS.splice(i, 1)
    editor.ProgrammeS.splice(i + 1, 0, acons)
    menuExo()
  }
  function importExo () {
    const aaj = j3pClone(editor.ProgrammeS)
    const nomsPris = []
    for (let i = 0; i < aaj.length; i++) {
      nomsPris.push(aaj[i].nom)
      // je sais Daniel y’a un truc pour faire ca mais je m’en rappelle plus
    }
    for (let i = 0; i < aaj.length; i++) {
      do {
        aaj[i].nom += '2'
      } while (nomsPris.indexOf(aaj[i].nom) !== -1)
      editor.ProgrammeS.push(j3pClone(aaj[i]))
      nomsPris.push(aaj[i].nom)
    }
    menuExo()
  }
  function DupExo (num) {
    const aaj = j3pClone(editor.ProgrammeS[num])
    const nomsPris = []
    for (let i = 0; i < editor.ProgrammeS.length; i++) {
      nomsPris.push(editor.ProgrammeS[i].nom)
      // je sais Daniel y’a un truc pour faire ca mais je m’en rappelle plus
    }
    do {
      aaj.nom += '2'
    } while (nomsPris.indexOf(aaj.nom) !== -1)
    editor.ProgrammeS.push(j3pClone(aaj))
    menuExo()
  }

  function ajouteExo () {
    editor.ProgrammeS.push(getDefaultProgrammeElement())
    menuExo()
  }
  function faisId (s) {
    if (typeof s === 'string') return s
    return s.content.txtFigure
  }
  function getDefaultProgrammeElement () {
    return {
      consigneG: 'Clique',
      correction: '',
      repPrev: [],
      yaSon: false,
      yaImage: false,
      image: genIm,
      txtFigure: genMGEx,
      avertFond: '',
      nom: 'nouvel exercice',
      survol: true,
      fond: 'n',
      fautTout: false,
      listZone: []
    }
  }
  function modifExo (i) {
    editor.listAffiche = []
    editor.yaBlok = true
    editor.objAvire = []
    editor.okVire = false
    editor.figDep.style.display = ''
    j3pEmpty(editor.figDep)
    editor.exoEncours = j3pClone(editor.ProgrammeS[i])
    j3pEmpty(editor.divExo)
    const tab = addDefaultTable(editor.divExo, 5, 3)
    j3pAddContent(tab[0][0], '&nbsp;')
    j3pAddContent(tab[2][2], '&nbsp;')
    j3pAddContent(tab[4][0], '&nbsp;')
    const tadd = addDefaultTable(tab[1][1], 1, 7)
    editor.cellModifExo = tadd[0][0]
    j3pAddContent(tadd[0][1], '&nbsp;&nbsp;&nbsp;&nbsp;')
    j3pAddContent(tadd[0][2], '&nbsp;nom:&nbsp;')
    j3pAddContent(tadd[0][4], '&nbsp;&nbsp;&nbsp;&nbsp;')
    editor.inputNouveauNom = j3pAddElt(tadd[0][3], 'input', '', { value: editor.exoEncours.nom, size: 15 })
    editor.numExo = i + 1
    editor.inputNouveauNom.addEventListener('change', chgNom)
    chgNom()
    const tabou = addDefaultTable(tab[3][1], 1, 3)
    j3pAddContent(tabou[0][1], '&nbsp;&nbsp;&nbsp;&nbsp;')
    editor.btnValiderNom = j3pAjouteBouton(tabou[0][0], () => { valModifExo(i) }, { value: 'Valider' })
    j3pAjouteBouton(tabou[0][2], menuExo, { value: 'Annuler' })
    editor.exoEncoursi = i

    j3pEmpty(editor.figDep)
    const divEn = j3pAddElt(editor.figDep, 'div')
    divEn.style.padding = '10px'
    divEn.style.borderRadius = '10px'
    divEn.style.backgroundImage = 'linear-gradient(to left, rgba(245, 169, 107, 0.8) 30%, rgba(245, 220, 187, 0.4))'

    faitAreaFacile(divEn, 'Consigne', 'consigneG')
    afficheExplik(divEn, 'La consigne s’affiche tout en haut de l’exercice.')

    /// ///
    const yuyu712 = j3pAddElt(divEn, 'span')
    editor.achcaSon = j3pAddElt(divEn, 'div')
    const tabvson = addDefaultTable(yuyu712, 1, 2)
    j3pAddContent(tabvson[0][0], '<b><u>Fichier son l’énoncé:</u></b>&nbsp;')
    const ldOptions34 = { dansModale: true, speMod: 10, speMod2: 70, j3pCont: container, choix0: true, onChange: changeSon }
    const ll34 = (!editor.exoEncours.yaSon) ? ['Non', 'Oui'] : ['Oui', 'Non']
    editor.yaFigureLSon = ListeDeroulante.create(tabvson[0][1], ll34, ldOptions34)
    editor.Cahc2yaIMageSon = addDefaultTable(editor.achcaSon, 2, 1)
    editor.whereFigSon = editor.Cahc2yaIMageSon[0][0]
    faitSon(editor.whereFigSon, editor.exoEncours, editor, 'son')
    changeSon()
    afficheExplik(divEn, 'Le fichier son doit rester disponible en ligne', true)
    /// ///

    const divTrav = j3pAddElt(editor.figDep, 'div')
    divTrav.style.padding = '10px'
    divTrav.style.borderRadius = '10px'
    divTrav.style.backgroundImage = 'linear-gradient(to right, rgba(187, 187, 187, 0.8) 30%, rgba(224, 224, 224, 0.5))'
    divTrav.style.borderTop = '1px dashed black'

    /// ///
    const yuyu71 = j3pAddElt(divTrav, 'span')
    editor.achcaIm = j3pAddElt(divTrav, 'div')
    const tabvim = addDefaultTable(yuyu71, 1, 2)
    j3pAddContent(tabvim[0][0], '<b><u>Image de fond:</u></b>&nbsp;')
    editor.Cahc2yaIMageL = addDefaultTable(editor.achcaIm, 2, 1)
    editor.whereFigIm = editor.Cahc2yaIMageL[0][0]
    faitIm()
    afficheExplik(divTrav, 'L’image doit rester disponible en ligne', false)
    /// ///

    initTag()
    const yuyu7 = j3pAddElt(divTrav, 'span')
    editor.achca = j3pAddElt(divTrav, 'div')
    j3pAddContent(yuyu7, '<b><u>Zones et taille:</u></b>')
    const tbBoutt = addDefaultTable(yuyu7, 1, 13)
    const boutonLabel = j3pAddElt(tbBoutt[0][0], 'button')
    boutonLabel.addEventListener('mousedown', (event) => { ajZoneLabel(event) })
    j3pAddContent(boutonLabel, '&nbsp;<b>Label</b>&nbsp;')
    j3pAddContent(tbBoutt[0][1], '&nbsp;&nbsp;')

    const boutonCache = j3pAddElt(tbBoutt[0][2], 'button')
    boutonCache.addEventListener('mousedown', (event) => { ajZoneCache(event) })
    const ttr = addDefaultTable(boutonCache, 1, 2)
    j3pAddContent(ttr[0][0], '&nbsp;<b>Cache</b>&nbsp;')
    j3pAddContent(tbBoutt[0][3], '&nbsp;&nbsp;')

    const boutonCirc = j3pAddElt(tbBoutt[0][4], 'button')
    boutonCirc.addEventListener('mousedown', (event) => { ajZone(event) })
    const tt = addDefaultTable(boutonCirc, 1, 2)
    j3pAddContent(tt[0][0], '&nbsp;<b>Zone</b>&nbsp;')
    const immm = j3pAddElt(tt[0][1], 'img', '', { src: rondImg, width: 30, height: 30 })
    immm.style.width = '30px'
    immm.style.height = '30px'
    j3pAddContent(tbBoutt[0][5], '&nbsp;&nbsp;')

    const boutonR = j3pAddElt(tbBoutt[0][6], 'button')
    boutonR.addEventListener('mousedown', ajZonepr)
    const ttR = addDefaultTable(boutonR, 1, 2)
    j3pAddContent(ttR[0][0], '&nbsp;<b>Zone</b>&nbsp;')
    const immmR = j3pAddElt(ttR[0][1], 'img', '', { src: rectangleImg, width: 30, height: 30 })
    immmR.style.width = '30px'
    immmR.style.height = '30px'
    j3pAddContent(tbBoutt[0][7], '&nbsp;&nbsp;')

    const boutonDF = j3pAddElt(tbBoutt[0][8], 'button')
    boutonDF.addEventListener('mousedown', ajZonepdf)
    const ttDF = addDefaultTable(boutonDF, 1, 2)
    j3pAddContent(ttDF[0][0], '&nbsp;<b>Zone</b>&nbsp;')
    const immmDF = j3pAddElt(ttDF[0][1], 'img', '', { src: circImg, width: 30, height: 30 })
    immmDF.style.width = '30px'
    immmDF.style.height = '30px'
    j3pAddContent(tbBoutt[0][9], '&nbsp;&nbsp;')

    const boutonQ = j3pAddElt(tbBoutt[0][10], 'button')
    boutonQ.addEventListener('mousedown', ajZonepq)
    const ttQ = addDefaultTable(boutonQ, 1, 2)
    j3pAddContent(ttQ[0][0], '&nbsp;<b>Zone</b>&nbsp;')
    const immmQ = j3pAddElt(ttQ[0][1], 'img', '', { src: quadImg, width: 30, height: 30 })
    immmQ.style.width = '30px'
    immmQ.style.height = '30px'
    j3pAddContent(tbBoutt[0][11], '&nbsp;&nbsp;')

    const boutonPoly = j3pAddElt(tbBoutt[0][12], 'button')
    boutonPoly.addEventListener('mousedown', ajZonepoly)
    const ttOly = addDefaultTable(boutonPoly, 1, 2)
    j3pAddContent(ttOly[0][0], '&nbsp;<b>Zone Polyg</b>&nbsp;')
    const inputnbSom = j3pAddElt(tbBoutt[0][12], 'input', '', {
      type: 'number',
      value: '5',
      size: 3,
      min: '5',
      max: '14'
    })
    inputnbSom.addEventListener('input', () => {
      const averif = inputnbSom.value
      if (averif === '' || isNaN(averif) || Number(averif) < 5 || Number(averif) > 14) inputnbSom.value = '5'
    })
    inputnbSom.addEventListener('change', () => {
      const averif = inputnbSom.value
      if (averif === '' || isNaN(averif) || Number(averif) < 5 || Number(averif) > 14) inputnbSom.value = '5'
    })
    editor.inputnbSom = inputnbSom

    editor.spanConst = j3pAddElt(yuyu7, 'span')
    editor.spanConst.style.color = '#0844c4'
    j3pAddContent(editor.spanConst, '&nbsp;')

    editor.whereFig = j3pAddElt(editor.achca, 'div')
    faitMG()
    afficheExplik(divTrav, 'Placer sur cette figure les différentes zones.<br>Leur paramètres sont à compléter ci-dessous.', false)

    const divListeZone = j3pAddElt(divTrav, 'div')
    const tabContZone = addDefaultTable(divListeZone, 2, 1)
    editor.zoneList = tabContZone[1][0]
    j3pAddContent(tabContZone[0][0], '<b><u>Zones</u></b>')
    tabContZone[0][0].style.paddingBottom = '10px'
    affListeZone()
    afficheExplik(divTrav, 'Utiliser le bouton <b>[Suppr]</b> pour supprimer la zone.<br><br> Cocher la <b>case "Réponse juste"</b> pour que cette zone soit considérée comme une réponse valide.<br><br>Utiliser le bouton <b>[Fixer]</b> ou <b>[Débloquer]</b> pour masquer/afficher les petits points qui permettent de déplacer/déformer la zone.<br><br> Utiliser les boutons <b>[↓]</b> et <b>[↑]</b> pour modifier l’ordre de création de la zone.<br>&nbsp;&nbsp;<i>(Les dernières zones sont affichées au dessus des premières, ce sont elles qui auront le "click" en cas de recouvrement )</i>')

    const divListeZone3 = addDefaultTable(divTrav, 1, 2)
    j3pAddContent(divListeZone3[0][0], 'Les zones apparaissent au survol de la souris.')
    editor.cocheSurvol = j3pAjouteCaseCoche(divListeZone3[0][1])
    editor.cocheSurvol.checked = editor.exoEncours.survol
    editor.cocheSurvol.onchange = () => {
      editor.exoEncours.survol = editor.cocheSurvol.checked
    }
    afficheExplik(divTrav, 'Si cette case est cochée, les zones apparaissent au survol de la souris', false)

    const divListeZone2 = addDefaultTable(divTrav, 1, 2)
    j3pAddContent(divListeZone2[0][0], 'Toutes les zones "bonnes" doivent être validées.')
    editor.cocheToute = j3pAjouteCaseCoche(divListeZone2[0][1])
    editor.cocheToute.checked = editor.exoEncours.fautTout
    editor.cocheToute.onchange = () => {
      editor.exoEncours.fautTout = editor.cocheToute.checked
    }
    afficheExplik(divTrav, 'Si cette case est cochée, et que plusieurs zones ont la case "Réponse juste" cochée,<br> L’éléve doit sélectionner toutes ces zones pour valider l’exercice.', true)

    const divTest = j3pAddElt(editor.figDep, 'div')
    divTest.style.padding = '10px'
    divTest.style.borderTop = '1px dashed black'
    divTest.style.borderRadius = '10px'
    divTest.style.background = '#6e6666'
    j3pAjouteBouton(divTest, testExo, { value: 'Tester cet exercice' })
    afficheExplik(divTest, 'Permet de prévisualiser l’exercice', true)
    editor.divTest = addDefaultTable(divTest, 1, 1)[0][0]
  }
  function initTag () {
    editor.nomPos = []
    editor.listTag = []
    for (let i = 0; i < editor.exoEncours.listZone.length; i++) {
      const el = editor.exoEncours.listZone[i]
      editor.nomPos.push(el.nom)
      switch (el.type) {
        case 'cercle':
          editor.listTag.push(el.c, el.p, el.cercle, el.droite, el.texte, el.surface[0])
          break
        case 'poly':
          editor.listTag.push(el.m1, el.m2, el.m3, el.p1, el.p2, el.p3, el.p4, el.s1, el.s2, el.s3, el.s4, el.g, el.g1, el.g2, el.poly, el.texte, el.surface[0])
          break
        case 'rectangle':
          editor.listTag.push(el.p1, el.p2, el.p3, el.p4, el.p5, el.p6, el.s1, el.m1, el.per1, el.poly, el.texte, el.surface[0])
          break
        case 'circ':
          editor.listTag.push(el.p1, el.p2, el.p3, el.p4, el.p5, el.p6, el.p7, el.p8, el.p9, el.m1, el.cer1, el.cer2, el.c1, el.c2, el.s1, el.per1, el.poly, el.texte, el.surface[0], el.surface[1], el.surface[2])
          break
        case 'polyg':
          for (let i = 0; i < el.listSom.length; i++) {
            editor.listTag.push(el.listSom[i])
          }
          for (let i = 0; i < el.mimid.length; i++) {
            editor.listTag.push(el.mimid[i])
          }
          editor.listTag.push(el.poly, el.surface[0], el.texte)
          break
        case 'label':
          editor.listTag.push(el.p1)
          break
        case 'cache':
          editor.listTag.push(el.p1, el.p2, el.p3, el.p4, el.d1, el.d2, el.d3, el.d4, el.m1, el.poly, el.texte, el.surface[0])
          break
      }
    }
  }
  function faitAreaFacile (divEn, titre, editorConsigne) {
    const tab1 = addDefaultTable(divEn, 3, 1)
    const yuyuu = addDefaultTable(tab1[2][0], 1, 3)
    j3pAddContent(yuyuu[0][1], '&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;')
    tab1[1][0].style.verticalAlign = 'top'
    j3pAddContent(tab1[0][0], '<b><u>' + titre + ':</u></b>&nbsp;')
    yuyuu[0][0].style.verticalAlign = 'top'
    yuyuu[0][2].style.verticalAlign = 'top'
    const consigneG = j3pAddElt(yuyuu[0][0], 'textarea', '', {
      value: editor.exoEncours[editorConsigne].replace(/<br>/g, '\r\n').replace(/<BR>/g, '\r\n'),
      cols: 60,
      rows: 7
    })
    const lamodaleG = j3pAddElt(yuyuu[0][0], 'div')
    const sortie = j3pAddElt(yuyuu[0][2], 'div')
    sortie.style.border = '1px solid black'
    sortie.style.borderRadius = '5px'
    sortie.style.minWidth = '400px'
    sortie.style.minHeight = '100px'
    sortie.style.background = '#b2afaf'
    sortie.style.padding = '5px'
    consigneG.style.resize = 'none'
    consigneG.addEventListener('input', () => {
      editor.exoEncours[editorConsigne] = consigneG.value
      j3pEmpty(sortie)
      j3pAffiche(sortie, null, consigneG.value)
    })
    j3pAffiche(sortie, null, consigneG.value)
    const boutonIG = j3pAddElt(tab1[1][0], 'button')
    boutonIG.addEventListener('click', faitItalG)
    boutonIG.entree = consigneG
    boutonIG.sortie = sortie
    boutonIG.editorConsigne = editorConsigne
    j3pAddElt(boutonIG, 'em', '&nbsp;I&nbsp;')
    const boutonGG = j3pAddElt(tab1[1][0], 'button')
    boutonGG.addEventListener('click', faitBoldG)
    boutonGG.entree = consigneG
    boutonGG.sortie = sortie
    boutonGG.editorConsigne = editorConsigne
    j3pAddElt(boutonGG, 'strong', '&nbsp;G&nbsp;')
    const boutonUG = j3pAddElt(tab1[1][0], 'button')
    boutonUG.addEventListener('click', faitUnderG)
    boutonUG.entree = consigneG
    boutonUG.sortie = sortie
    boutonUG.editorConsigne = editorConsigne
    j3pAddElt(boutonUG, 'u', '&nbsp;U&nbsp;')
    j3pAddContent(tab1[1][0], '&nbsp;&nbsp;')
    const boutonSS = j3pAddElt(tab1[1][0], 'button')
    boutonSS.addEventListener('click', faitSupG)
    boutonSS.entree = consigneG
    boutonSS.sortie = sortie
    boutonSS.editorConsigne = editorConsigne
    j3pAddContent(boutonSS, '&nbsp;Supprimer la mise en forme&nbsp;')
    j3pAddContent(tab1[1][0], '&nbsp;&nbsp;')
    const boutonFonc = j3pAddElt(tab1[1][0], 'button')
    boutonFonc.addEventListener('click', faitFonc)
    boutonFonc.entree = consigneG
    boutonFonc.sortie = sortie
    boutonFonc.lamodale = lamodaleG
    boutonFonc.exp = editorConsigne
    boutonFonc.acache = tab1[1][0]
    boutonFonc.editorConsigne = editorConsigne
    lamodaleG.style.display = 'none'
    j3pAddContent(boutonFonc, '&nbsp;Formule&nbsp;')
  }
  function faitFonc () {
    const liste = ['racine', 'fraction', 'pi', 'puissance', 'infegal', 'supegal', '\\neq']
    const liste2 = [
      'vecteur', 'prosca', 'abs', 'Un', 'sigma'
    ]
    const liste3 = [
      'inf', 'equivaut', 'indice', 'conj', 'bracket'
    ]
    const liste4 = [
      'inter', 'union', 'vide', 'barre', 'ℕ', 'ℝ', 'ℚ', 'ℤ'
    ]
    const liste5 = [
      'ln', 'log', 'sin', 'cos', 'tan', 'exp'
    ]
    const liste6 = [
      'integ', 'prim', 'k_parmi_n'
    ]
    this.acache.style.visibility = 'hidden'
    const rememberIndex = this.entree.selectionStart
    this.lamodale.style.display = ''
    this.lamodale.style.width = '400px'
    this.entree.style.display = 'none'
    const exp = this.exp
    j3pEmpty(this.lamodale)
    const divCont = j3pAddElt(this.lamodale, 'div')
    divCont.style.border = '1px solid black'
    divCont.style.borderRadius = '5px'
    divCont.style.background = '#a49999'
    divCont.style.padding = '5px'
    const tabCont = addDefaultTable(divCont, 5, 1)
    tabCont[1][0].style.height = '30px'
    const tabBas = addDefaultTable(tabCont[3][0], 1, 3)
    tabBas[0][1].style.width = '100%'
    j3pAjouteBouton(tabBas[0][2], makeFaisAnnuleFormule(this.entree, this.lamodale, this.acache), { value: 'Annuler' })
    const dernLigne = addDefaultTable(tabCont[2][0], 1, 2)
    j3pAddContent(dernLigne[0][0], 'Formule:&nbsp;&nbsp;')
    const whereAF = j3pAddElt(dernLigne[0][1], 'div')
    whereAF.style.background = '#fff'
    whereAF.classList.add('pourFitContent')
    const laf = j3pAffiche(whereAF, exp, '&1&', { inputmq1: {} })
    mqRestriction(exp + 'inputmq1', '0123456789^,.+-*/abcdefghijklmnopqrstuvwxyzABCDEFGHIJKLMNOPQRSTUVWXYZ{}= ', {
      commandes: [],
      boundingContainer: container
    })
    const bbout = j3pAddElt(tabCont[1][0], 'div', '', { id: exp + 'boutonsmathquill' })
    j3pPaletteMathquill2(bbout, exp + 'inputmq1', { liste })
    j3pPaletteMathquill2(bbout, exp + 'inputmq1', { liste: liste2 })
    j3pPaletteMathquill2(bbout, exp + 'inputmq1', { liste: liste3 })
    j3pPaletteMathquill2(bbout, exp + 'inputmq1', { liste: liste4 })
    j3pPaletteMathquill2(bbout, exp + 'inputmq1', { liste: liste5 })
    j3pPaletteMathquill2(bbout, exp + 'inputmq1', { liste: liste6, big: true })
    j3pAjouteBouton(tabBas[0][0], makeFaisOKFormule(laf, this.entree, this.sortie, rememberIndex, this.lamodale, this.acache, this.editorConsigne), { value: 'OK' })
    j3pFocus(exp + 'inputmq1')
  }
  function ajZoneCache (event) {
    if (editor.okVire) vireConstEncours()
    editor.okVire = true
    event.stopPropagation()
    event.preventDefault()
    editor.mtgAppLecteur.getDoc(editor.idSvg).defaultCursor = 'crosshair'
    j3pEmpty(editor.spanConst)
    j3pAddContent(editor.spanConst, 'Cliquer sur la figure pour placer un sommet de la zone')
    editor.funcClick = (x, y) => {
      editor.mtgAppLecteur.setApiDoc(editor.idSvg)
      const p1 = newTag()
      const p2 = newTag()
      editor.mtgAppLecteur.addFreePoint({
        absCoord: true,
        x,
        y,
        name: p1,
        tag: p1,
        pointStyle: 'biground',
        hiddenName: true,
        color: listColor[editor.exoEncours.listZone.length % listColor.length]
      })
      editor.mtgAppLecteur.addFreePoint({
        absCoord: true,
        x: x + 1,
        y: y + 1,
        name: p2,
        tag: p2,
        pointStyle: 'biground',
        hiddenName: true,
        color: listColor[editor.exoEncours.listZone.length % listColor.length]
      })
      const d1 = newTag()
      const d2 = newTag()
      const d3 = newTag()
      const d4 = newTag()
      const p3 = newTag()
      const m1 = newTag()
      const p4 = newTag()
      const poly = newTag()
      const surface = newTag()
      editor.mtgAppLecteur.addLinePerp({
        absCoord: true,
        a: p1,
        x,
        y,
        d: 'vertic',
        name: d1,
        tag: d1,
        pointStyle: 'bigmult',
        hiddenName: true,
        hidden: true
      })
      editor.mtgAppLecteur.addLinePerp({
        absCoord: true,
        a: p1,
        x,
        y,
        d: d1,
        name: d2,
        tag: d2,
        pointStyle: 'bigmult',
        hiddenName: true,
        hidden: true
      })
      editor.mtgAppLecteur.addLinePerp({
        absCoord: true,
        a: p2,
        x,
        y,
        d: 'vertic',
        name: d3,
        tag: d3,
        pointStyle: 'bigmult',
        hiddenName: true,
        hidden: true
      })
      editor.mtgAppLecteur.addLinePerp({
        absCoord: true,
        a: p2,
        x,
        y,
        d: d1,
        name: d4,
        tag: d4,
        pointStyle: 'bigmult',
        hiddenName: true,
        hidden: true
      })
      editor.mtgAppLecteur.addIntLineLine({
        d: d2,
        d2: d3,
        name: p3,
        tag: p3,
        hidden: true
      })
      editor.mtgAppLecteur.addIntLineLine({
        d: d1,
        d2: d4,
        name: p4,
        tag: p4,
        hidden: true
      })
      editor.mtgAppLecteur.addMidpoint({
        a: p1,
        b: p2,
        name: m1,
        tag: m1,
        hidden: true
      })
      editor.mtgAppLecteur.addPolygon({
        points: [p1, p3, p2, p4],
        tag: poly,
        hidden: true,
        opacity: 1
      })
      editor.mtgAppLecteur.addSurfacePoly({
        poly,
        tag: surface,
        color: 'white',
        thickness: 3,
        fillStyle: 'fill',
        opacity: 1
      })
      editor.objAvire.push(p1, p2)
      j3pEmpty(editor.spanConst)
      j3pAddContent(editor.spanConst, 'Cliquer sur la figure pour placer le sommet opposé de la zone')
      editor.funcMov = (x, y) => {
        editor.mtgAppLecteur.setPointPosition(editor.idSvg, '#' + p2, x, y, true)
      }
      editor.funcClick = (x, y) => {
        editor.mtgAppLecteur.getDoc(editor.idSvg).defaultCursor = undefined
        editor.okVire = false
        editor.objAvire = []
        editor.funcMov = () => {}
        editor.funcClick = () => {}
        const texte = newTag()
        const nom = newNom()
        editor.mtgAppLecteur.addLinkedText({
          a: m1,
          text: nom.replace('Zone', 'Cache'),
          tag: texte,
          color: listColor[editor.exoEncours.listZone.length % listColor.length],
          thickness: 3,
          vAlign: 'middle',
          hAlign: 'center'
        })
        editor.exoEncours.listZone.push({
          color: listColor[editor.exoEncours.listZone.length % listColor.length],
          type: 'cache',
          nom,
          p1,
          p2,
          p3,
          p4,
          d1,
          d2,
          d3,
          d4,
          m1,
          poly,
          surface: [surface],
          texte
        })
        affListeZone()
      }
    }
  }
  function ajZoneLabel (event) {
    if (editor.okVire) vireConstEncours()
    editor.okVire = true
    event.stopPropagation()
    event.preventDefault()
    editor.mtgAppLecteur.getDoc(editor.idSvg).defaultCursor = 'crosshair'
    j3pEmpty(editor.spanConst)
    j3pAddContent(editor.spanConst, 'Cliquer sur la figure pour placer le point de départ')
    editor.funcClick = (x, y) => {
      j3pEmpty(editor.spanConst)
      j3pAddContent(editor.spanConst, '&nbsp;')
      editor.funcClick = () => {}
      editor.mtgAppLecteur.getDoc(editor.idSvg).defaultCursor = undefined
      editor.mtgAppLecteur.setApiDoc(editor.idSvg)
      const p1 = newTag()
      editor.okVire = false
      editor.mtgAppLecteur.addFreePoint({
        absCoord: true,
        x,
        y,
        name: p1,
        tag: p1,
        pointStyle: 'biground',
        hiddenName: true,
        color: listColor[editor.exoEncours.listZone.length % listColor.length]
      })
      editor.funcMov = () => {}
      const nom = newNom()
      editor.exoEncours.listZone.push({
        color: listColor[editor.exoEncours.listZone.length % listColor.length],
        type: 'label',
        nom,
        p1,
        texte: '',
        taille: 15
      })
      affListeZone()
    }
  }
  function ajZone (event) {
    if (editor.okVire) vireConstEncours()
    editor.okVire = true
    event.stopPropagation()
    event.preventDefault()
    editor.mtgAppLecteur.getDoc(editor.idSvg).defaultCursor = 'crosshair'
    j3pEmpty(editor.spanConst)
    j3pAddContent(editor.spanConst, 'Cliquer sur la figure pour placer le centre du cercle')
    editor.funcClick = (x, y) => {
      editor.mtgAppLecteur.setApiDoc(editor.idSvg)
      const c = newTag()
      editor.mtgAppLecteur.addFreePoint({
        absCoord: true,
        x,
        y,
        name: c,
        tag: c,
        pointStyle: 'biground',
        hiddenName: true,
        color: listColor[editor.exoEncours.listZone.length % listColor.length]
      })
      const droite = newTag()
      editor.mtgAppLecteur.addLinePerp({
        a: c,
        d: 'vertic',
        tag: droite,
        color: 'red',
        hidden: true
      })
      const ctemp = newTag()
      editor.mtgAppLecteur.addFreePoint({
        absCoord: true,
        x: x + 1,
        y,
        name: ctemp,
        tag: ctemp,
        hidden: true
      })
      const droitTemp = newTag()
      editor.mtgAppLecteur.addLinePerp({
        a: ctemp,
        d: droite,
        tag: droitTemp,
        color: 'red',
        hidden: true
      })
      const pTemp = newTag()
      editor.mtgAppLecteur.addIntLineLine({
        d: droite,
        d2: droitTemp,
        name: pTemp,
        tag: pTemp,
        hidden: true
      })
      const cercTemp = newTag()
      editor.mtgAppLecteur.addCircleOA({
        o: c,
        a: pTemp,
        tag: cercTemp,
        hidden: true,
        opacity: 0
      })
      const surfTemp = newTag()
      editor.mtgAppLecteur.addSurfaceCircle({
        c: cercTemp,
        tag: surfTemp,
        color: listColor[editor.exoEncours.listZone.length % listColor.length],
        thickness: 3,
        fillStyle: 'transp',
        opacity: 0.6
      })
      editor.objAvire.push(c, ctemp)
      j3pEmpty(editor.spanConst)
      j3pAddContent(editor.spanConst, 'Cliquer sur la figure pour terminer la zone')
      editor.funcMov = (x, y) => {
        editor.mtgAppLecteur.setPointPosition(editor.idSvg, '#' + ctemp, x, y, true)
      }
      editor.funcClick = (x, y) => {
        j3pEmpty(editor.spanConst)
        j3pAddContent(editor.spanConst, '&nbsp;')
        editor.mtgAppLecteur.getDoc(editor.idSvg).defaultCursor = undefined
        editor.okVire = false
        editor.funcMov = () => {}
        editor.funcClick = () => {}
        editor.mtgAppLecteur.deleteElt({ elt: ctemp })
        editor.objAvire = []
        const p = newTag()
        const cercle = newTag()
        const texte = newTag()
        const surface = newTag()
        const nom = newNom()
        editor.mtgAppLecteur.addLinkedPointLine({
          absCoord: true,
          d: droite,
          x,
          y,
          name: p,
          tag: p,
          pointStyle: 'bigmult',
          hiddenName: true,
          color: listColor[editor.exoEncours.listZone.length % listColor.length]
        })
        editor.mtgAppLecteur.addCircleOA({
          o: c,
          a: p,
          tag: cercle,
          hidden: true,
          opacity: 0
        })
        editor.mtgAppLecteur.addSurfaceCircle({
          c: cercle,
          tag: surface,
          color: listColor[editor.exoEncours.listZone.length % listColor.length],
          thickness: 3,
          fillStyle: 'transp',
          opacity: 0.6
        })
        editor.mtgAppLecteur.addLinkedText({
          a: c,
          text: nom,
          tag: texte,
          thickness: 3,
          vAlign: 'top',
          hAlign: 'center'
        })
        editor.exoEncours.listZone.push({
          color: listColor[editor.exoEncours.listZone.length % listColor.length],
          type: 'cercle',
          nom,
          c,
          p,
          cercle,
          droite,
          texte,
          surface: [surface],
          bon: false,
          avert: ''
        })
        affListeZone()
      }
    }
  }
  function newNom () {
    let i = 0
    do {
      i++
    } while (editor.nomPos.indexOf('Zone ' + i) !== -1)
    editor.nomPos.push('Zone ' + i)
    return 'Zone ' + i
  }
  function newTag () {
    let j = 0
    let ntag
    do {
      ntag = 't' + j
      j++
    } while (editor.listTag.includes(ntag))
    editor.listTag.push(ntag)
    return ntag
  }
  function ajZonepoly () {
    if (editor.okVire) vireConstEncours()
    const nbSom = Number(editor.inputnbSom.value)
    editor.okVire = true
    event.stopPropagation()
    event.preventDefault()
    editor.mtgAppLecteur.getDoc(editor.idSvg).defaultCursor = 'crosshair'
    j3pEmpty(editor.spanConst)
    j3pAddContent(editor.spanConst, 'Cliquer sur la figure pour placer un sommet du polygone')
    const listSom = []
    editor.funcClick = (x, y) => {
      editor.mtgAppLecteur.setApiDoc(editor.idSvg)
      listSom.push(newTag())
      listSom.push(newTag())
      const listSeg = newTag()
      editor.mtgAppLecteur.addFreePoint({
        absCoord: true,
        x,
        y,
        name: listSom[0],
        tag: listSom[0],
        pointStyle: 'biground',
        hiddenName: true,
        color: listColor[editor.exoEncours.listZone.length % listColor.length]
      })
      editor.mtgAppLecteur.addFreePoint({
        absCoord: true,
        x: x + 1,
        y: y + 1,
        name: listSom[1],
        tag: listSom[1],
        pointStyle: 'biground',
        hiddenName: true,
        color: listColor[editor.exoEncours.listZone.length % listColor.length]
      })
      editor.mtgAppLecteur.addSegment({
        a: listSom[0],
        b: listSom[1],
        tag: listSeg,
        hidden: false,
        thickness: 7,
        color: listColor[editor.exoEncours.listZone.length % listColor.length]
      })
      editor.objAvire.push(listSom[0], listSom[1])
      j3pEmpty(editor.spanConst)
      j3pAddContent(editor.spanConst, 'Cliquer sur la figure pour placer le 2e sommet du polygone')
      editor.funcMov = makeFuncMov(listSom)
      editor.funcClick = makeFuncClick(listSom, nbSom, listSeg)
    }
  }
  function makeFuncClick (listSom, nbSom, polyTempO, surf) {
    return (x, y) => {
      if (listSom.length === nbSom) {
        editor.objAvire = []
        editor.funcMov = () => {}
        editor.funcClick = () => {}

        const mimid = [newTag()]
        editor.mtgAppLecteur.addMidpoint({
          a: listSom[0],
          b: listSom[1],
          name: mimid[0],
          hidden: true
        })
        for (let i = 2; i < nbSom; i++) {
          mimid.push(newTag())
          editor.mtgAppLecteur.addImPointDilation({
            o: listSom[i],
            a: mimid[mimid.length - 2],
            name: mimid[mimid.length - 1],
            x: i / (i + 1),
            hidden: true
          })
        }

        const nom = newNom()
        const texte = newTag()
        editor.mtgAppLecteur.addLinkedText({
          a: mimid[mimid.length - 1],
          text: nom,
          tag: texte,
          thickness: 3,
          vAlign: 'middle',
          hAlign: 'center'
        })
        editor.exoEncours.listZone.push({
          color: listColor[editor.exoEncours.listZone.length % listColor.length],
          type: 'polyg',
          nom,
          listSom,
          poly: polyTempO,
          surface: [surf],
          texte,
          bon: false,
          avert: '',
          mimid
        })
        affListeZone()
      } else {
        j3pEmpty(editor.spanConst)
        j3pAddContent(editor.spanConst, 'Cliquer sur la figure pour placer le sommet suivant (' + listSom.length + ' sur ' + nbSom + ' )')
        listSom.push(newTag())
        const Aenv = j3pClone(listSom)
        editor.mtgAppLecteur.addFreePoint({
          absCoord: true,
          x,
          y,
          name: Aenv[Aenv.length - 1],
          tag: Aenv[Aenv.length - 1],
          pointStyle: 'biground',
          hiddenName: true,
          color: listColor[editor.exoEncours.listZone.length % listColor.length]
        })
        if (polyTempO) {
          editor.mtgAppLecteur.deleteElt({ elt: polyTempO })
        }
        const polyTemp = newTag()
        editor.mtgAppLecteur.addPolygon({
          points: listSom,
          tag: polyTemp,
          hidden: true,
          opacity: 0
        })
        const surfaceTemp = newTag()
        editor.mtgAppLecteur.addSurfacePoly({
          poly: polyTemp,
          tag: surfaceTemp,
          color: listColor[editor.exoEncours.listZone.length % listColor.length],
          thickness: 3,
          fillStyle: 'transp',
          opacity: 0.2
        })
        editor.objAvire.push(listSom[listSom.length - 1])
        editor.funcMov = makeFuncMov(Aenv)
        editor.funcClick = makeFuncClick(Aenv, nbSom, polyTemp, surfaceTemp)
      }
    }
  }
  function makeFuncMov (listSom) {
    return (x, y) => {
      editor.mtgAppLecteur.setPointPosition(editor.idSvg, '#' + listSom[listSom.length - 1], x, y, true)
    }
  }
  function ajZonepq () {
    if (editor.okVire) vireConstEncours()
    editor.okVire = true
    event.stopPropagation()
    event.preventDefault()
    editor.mtgAppLecteur.getDoc(editor.idSvg).defaultCursor = 'crosshair'
    j3pEmpty(editor.spanConst)
    j3pAddContent(editor.spanConst, 'Cliquer sur la figure pour placer un sommet du quadrilatère')
    editor.funcClick = (x, y) => {
      editor.mtgAppLecteur.setApiDoc(editor.idSvg)
      const p1 = newTag()
      const p2 = newTag()
      const s1 = newTag()
      editor.mtgAppLecteur.addFreePoint({
        absCoord: true,
        x,
        y,
        name: p1,
        tag: p1,
        pointStyle: 'biground',
        hiddenName: true,
        color: listColor[editor.exoEncours.listZone.length % listColor.length]
      })
      editor.mtgAppLecteur.addFreePoint({
        absCoord: true,
        x: x + 1,
        y: y + 1,
        name: p2,
        tag: p2,
        pointStyle: 'biground',
        hiddenName: true,
        color: listColor[editor.exoEncours.listZone.length % listColor.length]
      })
      editor.mtgAppLecteur.addSegment({
        a: p1,
        b: p2,
        tag: s1,
        hidden: false,
        thickness: 7,
        color: listColor[editor.exoEncours.listZone.length % listColor.length]
      })
      editor.objAvire.push(p1, p2)
      j3pEmpty(editor.spanConst)
      j3pAddContent(editor.spanConst, 'Cliquer sur la figure pour placer le 2e sommet du quadrilatère')
      editor.funcMov = (x, y) => {
        editor.mtgAppLecteur.setPointPosition(editor.idSvg, '#' + p2, x, y, true)
      }
      editor.funcClick = (x, y) => {
        j3pEmpty(editor.spanConst)
        j3pAddContent(editor.spanConst, 'Cliquer sur la figure pour placer le 3e sommet du quadrilatère')
        const pTemp = newTag()
        editor.mtgAppLecteur.addFreePoint({
          absCoord: true,
          x,
          y,
          name: pTemp,
          tag: pTemp,
          hidden: true
        })
        const polyTemp = newTag()
        const surfaceTemp = newTag()
        editor.mtgAppLecteur.addPolygon({
          points: [p1, p2, pTemp],
          tag: polyTemp,
          hidden: true,
          opacity: 0
        })
        editor.mtgAppLecteur.addSurfacePoly({
          poly: polyTemp,
          tag: surfaceTemp,
          color: listColor[editor.exoEncours.listZone.length % listColor.length],
          thickness: 3,
          fillStyle: 'transp',
          opacity: 0.2
        })
        editor.objAvire.push(pTemp)
        editor.mtgAppLecteur.setHidden({ elt: s1 })
        editor.funcMov = (x, y) => {
          editor.mtgAppLecteur.setPointPosition(editor.idSvg, '#' + pTemp, x, y, true)
        }
        editor.funcClick = (x, y) => {
          j3pEmpty(editor.spanConst)
          j3pAddContent(editor.spanConst, 'Cliquer sur la figure pour terminer le quadrilatère')
          const p3 = newTag()
          editor.objAvire.push(p3)
          editor.mtgAppLecteur.addFreePoint({
            absCoord: true,
            x,
            y,
            name: p3,
            tag: p3,
            pointStyle: 'biground',
            hiddenName: true,
            color: listColor[editor.exoEncours.listZone.length % listColor.length]
          })
          editor.mtgAppLecteur.deleteElt({ elt: polyTemp })
          const polyTemp2 = newTag()
          const surfaceTemp2 = newTag()
          editor.mtgAppLecteur.addPolygon({
            points: [p1, p2, p3, pTemp],
            tag: polyTemp2,
            hidden: true,
            opacity: 0
          })
          editor.mtgAppLecteur.addSurfacePoly({
            poly: polyTemp2,
            tag: surfaceTemp2,
            color: listColor[editor.exoEncours.listZone.length % listColor.length],
            thickness: 3,
            fillStyle: 'transp',
            opacity: 0.2
          })
          editor.funcClick = (x, y) => {
            j3pEmpty(editor.spanConst)
            j3pAddContent(editor.spanConst, '&nbsp;')
            editor.mtgAppLecteur.getDoc(editor.idSvg).defaultCursor = undefined
            editor.okVire = false
            editor.mtgAppLecteur.deleteElt({ elt: pTemp })
            editor.objAvire = []
            editor.funcMov = () => {}
            editor.funcClick = () => {}

            const m1 = newTag()
            const m2 = newTag()
            const m3 = newTag()
            const p4 = newTag()
            const s1 = newTag()
            const s2 = newTag()
            const s3 = newTag()
            const s4 = newTag()
            const g = newTag()
            const g1 = newTag()
            const g2 = newTag()
            const poly = newTag()
            const texte = newTag()
            const surface = newTag()
            const nom = newNom()
            editor.mtgAppLecteur.addFreePoint({
              absCoord: true,
              x,
              y,
              name: p4,
              tag: p4,
              pointStyle: 'biground',
              hiddenName: true,
              color: listColor[editor.exoEncours.listZone.length % listColor.length]
            })
            editor.mtgAppLecteur.addMidpoint({
              a: p1,
              b: p2,
              name: m1,
              hidden: true
            })
            editor.mtgAppLecteur.addMidpoint({
              a: p3,
              b: p4,
              name: m2,
              hidden: true
            })
            editor.mtgAppLecteur.addMidpoint({
              a: m1,
              b: m2,
              name: m3,
              hidden: true
            })
            editor.mtgAppLecteur.addPolygon({
              points: [p1, p2, p3, p4],
              tag: poly,
              hidden: true,
              opacity: 0
            })
            editor.mtgAppLecteur.addSurfacePoly({
              poly,
              tag: surface,
              color: listColor[editor.exoEncours.listZone.length % listColor.length],
              thickness: 3,
              fillStyle: 'transp',
              opacity: 0.2
            })
            editor.mtgAppLecteur.addLinkedText({
              a: m3,
              text: nom,
              tag: texte,
              thickness: 3,
              vAlign: 'middle',
              hAlign: 'center'
            })
            editor.exoEncours.listZone.push({
              color: listColor[editor.exoEncours.listZone.length % listColor.length],
              type: 'poly',
              nom,
              p1,
              p2,
              p3,
              p4,
              m3,
              m1,
              m2,
              s1,
              s2,
              s3,
              s4,
              poly,
              surface: [surface],
              g,
              g1,
              g2,
              texte,
              bon: false,
              avert: ''
            })
            affListeZone()
          }
        }
      }
    }
  }
  function ajZonepr (event) {
    if (editor.okVire) vireConstEncours()
    editor.okVire = true
    event.stopPropagation()
    event.preventDefault()
    editor.mtgAppLecteur.getDoc(editor.idSvg).defaultCursor = 'crosshair'
    j3pEmpty(editor.spanConst)
    j3pAddContent(editor.spanConst, 'Cliquer sur la figure pour placer un bord du rectangle')
    editor.funcClick = (x, y) => {
      editor.mtgAppLecteur.setApiDoc(editor.idSvg)
      const p1 = newTag()
      const p2 = newTag()
      const s1 = newTag()
      editor.mtgAppLecteur.addFreePoint({
        absCoord: true,
        x,
        y,
        name: p1,
        tag: p1,
        pointStyle: 'biground',
        hiddenName: true,
        color: listColor[editor.exoEncours.listZone.length % listColor.length]
      })
      editor.mtgAppLecteur.addFreePoint({
        absCoord: true,
        x: x + 1,
        y: y + 1,
        name: p2,
        tag: p2,
        pointStyle: 'biground',
        hiddenName: true,
        color: listColor[editor.exoEncours.listZone.length % listColor.length]
      })
      editor.mtgAppLecteur.addSegment({
        a: p1,
        b: p2,
        tag: s1,
        hidden: false,
        thickness: 7,
        color: listColor[editor.exoEncours.listZone.length % listColor.length]
      })
      editor.objAvire.push(p1, p2)
      j3pEmpty(editor.spanConst)
      j3pAddContent(editor.spanConst, 'Cliquer sur la figure pour placer le 2e bord du rectangle')
      editor.funcMov = (x, y) => {
        editor.mtgAppLecteur.setPointPosition(editor.idSvg, '#' + p2, x, y, true)
      }
      editor.funcClick = (x, y) => {
        j3pEmpty(editor.spanConst)
        j3pAddContent(editor.spanConst, 'Cliquer sur la figure pour terminer le rectangle')
        const per1 = newTag()
        editor.mtgAppLecteur.addLinePerp({
          a: p1,
          d: s1,
          tag: per1,
          hidden: true
        })
        const pTemp = newTag()
        editor.mtgAppLecteur.addFreePoint({
          absCoord: true,
          x,
          y,
          name: pTemp,
          tag: pTemp,
          hidden: true
        })
        const droiteTemp = newTag()
        editor.mtgAppLecteur.addLinePerp({
          a: pTemp,
          d: 'vertic',
          tag: droiteTemp,
          hidden: true
        })
        const p2Temp = newTag()
        editor.mtgAppLecteur.addIntLineLine({
          d: droiteTemp,
          d2: per1,
          tag: p2Temp,
          name: p2Temp,
          hidden: true
        })
        const m1 = newTag()
        editor.mtgAppLecteur.addMidpoint({
          a: p1,
          b: p2,
          name: m1,
          hidden: true
        })
        editor.objAvire.push(pTemp)
        editor.mtgAppLecteur.setHidden({ elt: s1 })
        const p4Temp = newTag()
        const p5Temp = newTag()
        const p6Temp = newTag()
        const polyTemp = newTag()
        const surfaceTemp = newTag()
        editor.mtgAppLecteur.addImPointSymCent({
          o: p1,
          a: p2Temp,
          tag: p5Temp,
          name: p5Temp,
          hidden: true
        })
        editor.mtgAppLecteur.addImPointSymCent({
          o: m1,
          a: p2Temp,
          tag: p4Temp,
          name: p4Temp,
          hidden: true
        })
        editor.mtgAppLecteur.addImPointSymCent({
          o: m1,
          a: p5Temp,
          tag: p6Temp,
          name: p6Temp,
          hidden: true
        })
        editor.mtgAppLecteur.addPolygon({
          points: [p2Temp, p5Temp, p4Temp, p6Temp],
          tag: polyTemp,
          hidden: true,
          opacity: 0
        })
        editor.mtgAppLecteur.addSurfacePoly({
          poly: polyTemp,
          tag: surfaceTemp,
          color: listColor[editor.exoEncours.listZone.length % listColor.length],
          thickness: 3,
          fillStyle: 'transp',
          opacity: 0.2
        })

        editor.funcMov = (x, y) => {
          editor.mtgAppLecteur.setPointPosition(editor.idSvg, '#' + pTemp, x, y, true)
        }
        editor.funcClick = (x, y) => {
          editor.mtgAppLecteur.getDoc(editor.idSvg).defaultCursor = undefined
          editor.okVire = false
          editor.mtgAppLecteur.deleteElt({ elt: pTemp })
          editor.objAvire = []
          editor.funcMov = () => {}
          editor.funcClick = () => {}
          const p3 = newTag()
          const p4 = newTag()
          const p5 = newTag()
          const p6 = newTag()
          const poly = newTag()
          const texte = newTag()
          const surface = newTag()
          const nom = newNom()
          editor.mtgAppLecteur.addLinkedPointLine({
            absCoord: true,
            x,
            y,
            d: per1,
            name: p3,
            tag: p3,
            pointStyle: 'bigmult',
            hiddenName: true,
            color: listColor[editor.exoEncours.listZone.length % listColor.length]
          })

          editor.mtgAppLecteur.addImPointSymCent({
            o: p1,
            a: p3,
            tag: p5,
            name: p5,
            hidden: true
          })
          editor.mtgAppLecteur.addImPointSymCent({
            o: m1,
            a: p3,
            tag: p4,
            name: p4,
            hidden: true
          })
          editor.mtgAppLecteur.addImPointSymCent({
            o: m1,
            a: p5,
            tag: p6,
            name: p6,
            hidden: true
          })

          editor.mtgAppLecteur.addPolygon({
            points: [p3, p5, p4, p6],
            tag: poly,
            hidden: true,
            opacity: 0
          })
          editor.mtgAppLecteur.addSurfacePoly({
            poly,
            tag: surface,
            color: listColor[editor.exoEncours.listZone.length % listColor.length],
            thickness: 3,
            fillStyle: 'transp',
            opacity: 0.2
          })
          editor.mtgAppLecteur.addLinkedText({
            a: m1,
            text: nom,
            tag: texte,
            thickness: 3,
            vAlign: 'middle',
            hAlign: 'center'
          })
          editor.exoEncours.listZone.push({
            color: listColor[editor.exoEncours.listZone.length % listColor.length],
            type: 'rectangle',
            nom,
            p1,
            p2,
            p3,
            p4,
            p5,
            p6,
            per1,
            m1,
            s1,
            poly,
            surface: [surface],
            texte,
            bon: false,
            avert: ''
          })
          affListeZone()
        }
      }
    }
  }
  function ajZonepdf (event) {
    if (editor.okVire) vireConstEncours()
    editor.okVire = true
    event.stopPropagation()
    event.preventDefault()
    editor.mtgAppLecteur.getDoc(editor.idSvg).defaultCursor = 'crosshair'
    j3pEmpty(editor.spanConst)
    j3pAddContent(editor.spanConst, 'Cliquer sur la figure pour placer un point de la zone')
    editor.funcClick = (x, y) => {
      editor.mtgAppLecteur.setApiDoc(editor.idSvg)
      const p1 = newTag()
      const p2 = newTag()
      const s1 = newTag()
      editor.mtgAppLecteur.addFreePoint({
        absCoord: true,
        x,
        y,
        name: p1,
        tag: p1,
        pointStyle: 'biground',
        hiddenName: true,
        color: listColor[editor.exoEncours.listZone.length % listColor.length]
      })
      editor.mtgAppLecteur.addFreePoint({
        absCoord: true,
        x: x + 1,
        y: y + 1,
        name: p2,
        tag: p2,
        pointStyle: 'biground',
        hiddenName: true,
        color: listColor[editor.exoEncours.listZone.length % listColor.length]
      })
      editor.mtgAppLecteur.addSegment({
        a: p1,
        b: p2,
        tag: s1,
        hidden: false,
        thickness: 7,
        color: listColor[editor.exoEncours.listZone.length % listColor.length]
      })
      editor.objAvire.push(p1, p2)
      j3pEmpty(editor.spanConst)
      j3pAddContent(editor.spanConst, 'Cliquer sur la figure pour placer le 2e point de la zone')
      editor.funcMov = (x, y) => {
        editor.mtgAppLecteur.setPointPosition(editor.idSvg, '#' + p2, x, y, true)
      }
      editor.funcClick = (x, y) => {
        j3pEmpty(editor.spanConst)
        j3pAddContent(editor.spanConst, 'Cliquer sur la figure pour terminer la zone')
        const per1 = newTag()
        editor.mtgAppLecteur.addLinePerp({
          a: p1,
          d: s1,
          tag: per1,
          hidden: true
        })
        const pTemp = newTag()
        editor.mtgAppLecteur.addFreePoint({
          absCoord: true,
          x,
          y,
          name: pTemp,
          tag: pTemp,
          hidden: true
        })
        const droiteTemp = newTag()
        editor.mtgAppLecteur.addLinePerp({
          a: pTemp,
          d: 'vertic',
          tag: droiteTemp,
          hidden: true
        })
        const p2Temp = newTag()
        editor.mtgAppLecteur.addIntLineLine({
          d: droiteTemp,
          d2: per1,
          tag: p2Temp,
          name: p2Temp,
          hidden: true
        })
        const m1 = newTag()
        editor.mtgAppLecteur.addMidpoint({
          a: p1,
          b: p2,
          name: m1,
          hidden: true
        })
        editor.objAvire.push(pTemp)
        editor.mtgAppLecteur.setHidden({ elt: s1 })
        const p4Temp = newTag()
        const p5Temp = newTag()
        const p6Temp = newTag()
        editor.mtgAppLecteur.addImPointSymCent({
          o: p1,
          a: p2Temp,
          tag: p5Temp,
          name: p5Temp,
          hidden: true
        })
        editor.mtgAppLecteur.addImPointSymCent({
          o: m1,
          a: p2Temp,
          tag: p4Temp,
          name: p4Temp,
          hidden: true
        })
        editor.mtgAppLecteur.addImPointSymCent({
          o: m1,
          a: p5Temp,
          tag: p6Temp,
          name: p6Temp,
          hidden: true
        })

        const cer1Temp = newTag()
        const cer2Temp = newTag()
        const c1Temp = newTag()
        const c2Temp = newTag()
        const p7Temp = newTag()
        const p8Temp = newTag()
        const p9Temp = newTag()
        const polyTemp = newTag()
        const surfaceTemp = newTag()
        const surface2Temp = newTag()
        const surface3Temp = newTag()

        editor.mtgAppLecteur.addPolygon({
          points: [p2Temp, p5Temp, p4Temp, p6Temp],
          tag: polyTemp,
          hidden: true,
          opacity: 0
        })
        editor.mtgAppLecteur.addSurfacePoly({
          poly: polyTemp,
          tag: surfaceTemp,
          color: listColor[editor.exoEncours.listZone.length % listColor.length],
          thickness: 3,
          fillStyle: 'transp',
          opacity: 0.2
        })

        editor.mtgAppLecteur.addCircleOr({
          o: p1,
          r: 0.01,
          hidden: true,
          tag: cer1Temp
        })
        editor.mtgAppLecteur.addIntLineCircle({
          d: s1,
          c: cer1Temp,
          tag: p7Temp,
          name: p7Temp,
          hidden: true
        })
        editor.mtgAppLecteur.addArcOAB({
          o: p7Temp,
          a: p2Temp,
          b: p5Temp,
          tag: c1Temp,
          hidden: true
        })
        editor.mtgAppLecteur.addSurfaceCircle({
          c: c1Temp,
          tag: surface2Temp,
          color: listColor[editor.exoEncours.listZone.length % listColor.length],
          thickness: 3,
          fillStyle: 'transp',
          opacity: 0.2
        })

        editor.mtgAppLecteur.addCircleOr({
          o: p2,
          r: 0.01,
          hidden: false,
          tag: cer2Temp
        })
        editor.mtgAppLecteur.addIntLineCircle({
          d: s1,
          c: cer2Temp,
          tag: p8Temp,
          name: p8Temp,
          tag2: p9Temp,
          name2: p9Temp,
          hidden: true,
          pointStyle: 'bigmult'
        })
        editor.mtgAppLecteur.addArcOAB({
          o: p9Temp,
          a: p4Temp,
          b: p6Temp,
          tag: c2Temp,
          hidden: true
        })
        editor.mtgAppLecteur.addSurfaceCircle({
          c: c2Temp,
          tag: surface3Temp,
          color: listColor[editor.exoEncours.listZone.length % listColor.length],
          thickness: 3,
          fillStyle: 'transp',
          opacity: 0.2
        })

        editor.funcMov = (x, y) => {
          editor.mtgAppLecteur.setPointPosition(editor.idSvg, '#' + pTemp, x, y, true)
        }
        editor.funcClick = (x, y) => {
          editor.mtgAppLecteur.getDoc(editor.idSvg).defaultCursor = undefined
          editor.okVire = false
          editor.mtgAppLecteur.deleteElt({ elt: pTemp })
          editor.objAvire = []
          editor.funcMov = () => {}
          editor.funcClick = () => {}

          const cer1 = newTag()
          const cer2 = newTag()
          const c1 = newTag()
          const c2 = newTag()
          const p3 = newTag()
          const p4 = newTag()
          const p5 = newTag()
          const p6 = newTag()
          const p7 = newTag()
          const p8 = newTag()
          const p9 = newTag()
          const poly = newTag()
          const texte = newTag()
          const surface = newTag()
          const surface2 = newTag()
          const surface3 = newTag()
          const nom = newNom()

          editor.mtgAppLecteur.addLinkedPointLine({
            absCoord: true,
            x,
            y,
            d: per1,
            name: p3,
            tag: p3,
            pointStyle: 'bigmult',
            hiddenName: true,
            color: listColor[editor.exoEncours.listZone.length % listColor.length]
          })
          editor.mtgAppLecteur.addImPointSymCent({
            o: p1,
            a: p3,
            tag: p5,
            name: p5,
            hidden: true
          })
          editor.mtgAppLecteur.addImPointSymCent({
            o: m1,
            a: p3,
            tag: p4,
            name: p4,
            hidden: true
          })
          editor.mtgAppLecteur.addImPointSymCent({
            o: m1,
            a: p5,
            tag: p6,
            name: p6,
            hidden: true
          })
          editor.mtgAppLecteur.addPolygon({
            points: [p3, p5, p4, p6],
            tag: poly,
            hidden: true,
            opacity: 0
          })
          editor.mtgAppLecteur.addSurfacePoly({
            poly,
            tag: surface,
            color: listColor[editor.exoEncours.listZone.length % listColor.length],
            thickness: 3,
            fillStyle: 'transp',
            opacity: 0.2
          })

          editor.mtgAppLecteur.addCircleOr({
            o: p1,
            r: 0.01,
            hidden: true,
            tag: cer1
          })
          editor.mtgAppLecteur.addIntLineCircle({
            d: s1,
            c: cer1,
            tag: p7,
            name: p7,
            hidden: true
          })
          editor.mtgAppLecteur.addArcOAB({
            o: p7,
            a: p3,
            b: p5,
            tag: c1,
            hidden: true
          })
          editor.mtgAppLecteur.addSurfaceCircle({
            c: c1,
            tag: surface2,
            color: listColor[editor.exoEncours.listZone.length % listColor.length],
            thickness: 3,
            fillStyle: 'transp',
            opacity: 0.2
          })

          editor.mtgAppLecteur.addCircleOr({
            o: p2,
            r: 0.01,
            hidden: false,
            tag: cer2
          })
          editor.mtgAppLecteur.addIntLineCircle({
            d: s1,
            c: cer2,
            tag: p8,
            name: p8,
            tag2: p9,
            name2: p9,
            hidden: true,
            pointStyle: 'bigmult'
          })
          editor.mtgAppLecteur.addArcOAB({
            o: p9,
            a: p4,
            b: p6,
            tag: c2,
            hidden: true
          })
          editor.mtgAppLecteur.addSurfaceCircle({
            c: c2,
            tag: surface3,
            color: listColor[editor.exoEncours.listZone.length % listColor.length],
            thickness: 3,
            fillStyle: 'transp',
            opacity: 0.2
          })

          editor.mtgAppLecteur.addLinkedText({
            a: m1,
            text: nom,
            tag: texte,
            thickness: 3,
            vAlign: 'middle',
            hAlign: 'center'
          })
          editor.exoEncours.listZone.push({
            color: listColor[editor.exoEncours.listZone.length % listColor.length],
            type: 'circ',
            nom,
            p1,
            p2,
            p3,
            p4,
            p5,
            p6,
            p7,
            p8,
            p9,
            m1,
            cer1,
            cer2,
            c1,
            c2,
            s1,
            per1,
            poly,
            surface: [surface, surface2, surface3],
            texte,
            bon: false,
            avert: ''
          })
          affListeZone()
        }
      }
    }
  }
  function makeFaisOKFormule (zone, entree, sortie, index, modale, acache, editorConsigne) {
    return () => {
      if (j3pValeurde(zone.inputmqList[0]).length > 0) {
        const startSelection = entree.value.substring(0, index)
        const endSelection = entree.value.substring(index)
        entree.value = startSelection + '$' + j3pValeurde(zone.inputmqList[0]) + '$' + endSelection
        editor.exoEncours[editorConsigne] = entree.value
      }
      modale.style.display = 'none'
      entree.style.display = ''
      acache.style.visibility = 'visible'
      j3pEmpty(sortie)
      j3pAffiche(sortie, null, entree.value)
    }
  }
  function makeFaisAnnuleFormule (ent, mod, acache) {
    return () => {
      ent.style.display = ''
      mod.style.display = 'none'
      acache.style.visibility = 'visible'
    }
  }
  function faitSupG () {
    const field = this.entree
    field.value = field.value.replace(/<strong>/g, '')
    field.value = field.value.replace(/<\/strong>/g, '')
    field.value = field.value.replace(/<em>/g, '')
    field.value = field.value.replace(/<\/em>/g, '')
    field.value = field.value.replace(/<u>/g, '')
    field.value = field.value.replace(/<\/u>/g, '')
    field.value = field.value.replace(/<i>/g, '')
    field.value = field.value.replace(/<\/i>/g, '')
    field.value = field.value.replace(/<b>/g, '')
    field.value = field.value.replace(/<\/b>/g, '')
    editor.exoEncours[this.editorConsigne] = field.value
    j3pEmpty(this.sortie)
    j3pAffiche(this.sortie, null, field.value)
  }

  function changeSon () {
    editor.exoEncours.yaSon = editor.yaFigureLSon.reponse === 'Oui'
    editor.achcaSon.style.display = (editor.exoEncours.yaSon) ? '' : 'none'
  }
  function afficheExplik (ou, txt, bool) {
    const tomuch = addDefaultTable(ou, 1, 2)
    j3pAjouteBouton(tomuch[0][0], () => { faiv(tomuch[0][1]) }, { value: ' ? ' })
    tomuch[0][1].style.color = '#850593'
    tomuch[0][1].style.background = '#b0ee7f'
    tomuch[0][1].style.padding = '5px'
    tomuch[0][1].style.border = '1px solid black'
    j3pAddContent(tomuch[0][1], txt)
    tomuch[0][1].style.display = 'none'
    j3pAddContent(ou, '\n')
    const liligne = j3pAddElt(ou, 'div')
    if (!bool) {
      liligne.style.border = '1px dashed black'
      j3pAddContent(ou, '\n')
    }
  }
  function faiv (el) {
    el.style.display = (el.style.display === '') ? 'none' : ''
  }
  function faitItalG () {
    insertTag(this.entree, 'em', this.sortie, this.editorConsigne)
  }
  function faitBoldG () {
    insertTag(this.entree, 'strong', this.sortie, this.editorConsigne)
  }
  function faitUnderG () {
    insertTag(this.entree, 'u', this.sortie, this.editorConsigne)
  }
  function insertTag (textareaId, tagType, sortie, editorConsigne) {
    const field = textareaId
    field.focus() // On remet le focus sur la zone de texte, suivant les navigateurs, on perd le focus en appelant la fonction.
    const startSelection = field.value.substring(0, field.selectionStart)
    const currentSelection = field.value.substring(field.selectionStart, field.selectionEnd)
    const endSelection = field.value.substring(field.selectionEnd)
    field.value = startSelection + '<' + tagType + '>' + currentSelection + '</' + tagType + '>' + endSelection
    field.focus() // On remet le focus sur la zone de texte
    editor.exoEncours[editorConsigne] = textareaId.value
    j3pEmpty(sortie)
    j3pAffiche(sortie, null, field.value)
  }
  function sauve () {
    editor.mtgAppLecteur.setApiDoc(editor.idSvg)
    editor.exoEncours.height = editor.mtgAppLecteur.valueOf(editor.idSvg, 'A')
    editor.exoEncours.width = editor.mtgAppLecteur.valueOf(editor.idSvg, 'B')
    editor.exoEncours.txtFigure = editor.mtgAppLecteur.getBase64Code(editor.idSvg)
  }

  function testExo () {
    sauve()
    if (editor.idSvg2) {
      editor.mtgAppLecteur.removeDoc(editor.idSvg2)
    }
    j3pEmpty(editor.divTest)
    editor.divTest.style.background = '#efe6e6'
    editor.divTest.style.width = '550px'
    editor.divTest.style.padding = '10px'
    const eeen = j3pAddElt(editor.divTest, 'div')
    eeen.style.padding = '5px'
    eeen.style.borderRadius = '5px'
    eeen.style.backgroundImage = 'linear-gradient(to left, rgba(245, 169, 107, 0.8) 30%, rgba(245, 220, 187, 0.4))'
    j3pAffiche(eeen, null, editor.exoEncours.consigneG)
    j3pAddContent(eeen, '\n')
    if (editor.yaFigureLSon.reponse === 'Oui') {
      const contui = addDefaultTable(eeen, 1, 1)[0][0]
      const ui = j3pAddElt(contui, 'div')
      ui.style.border = '1px solid black'
      ui.style.borderRadius = '5px'
      ui.style.padding = '5px'
      ui.style.paddingTop = '0px'
      ui.style.background = '#aeef59'
      ui.style.textAlign = 'center'
      placeJouerSon(ui, editor.exoEncours, editor, 'son')
    }

    const tttn = j3pAddElt(editor.divTest, 'div')
    tttn.style.padding = '5px'
    tttn.style.borderRadius = '5px'
    tttn.style.backgroundImage = 'linear-gradient(to right, rgba(187, 187, 187, 0.8) 30%, rgba(224, 224, 224, 0.5))'

    editor.idSvg2 = j3pGetNewId()
    editor.limmm3 = j3pAddElt(tttn, 'img', '', { src: editor.exoEncours.image, width: editor.exoEncours.widthImage, height: editor.exoEncours.heightImage })
    editor.limmm3.style.width = (287 + editor.exoEncours.width * 355) + 'px'
    editor.limmm3.style.height = (149 + editor.exoEncours.height * 280) + 'px'
    editor.limmm3.style.position = 'relative'
    editor.limmm3.style.top = '0px'
    editor.limmm3.style.left = '0px'
    editor.leMG2 = j3pCreeSVG(tttn, { id: editor.idSvg2, width: (287 + editor.exoEncours.width * 355), height: (149 + editor.exoEncours.height * 280) })
    editor.mtgAppLecteur.addDoc(editor.idSvg2, editor.exoEncours.txtFigure, false)
    editor.mtgAppLecteur.setApiDoc(editor.idSvg2)
    editor.mtgAppLecteur.calculate(editor.idSvg2)
    editor.mtgAppLecteur.display(editor.idSvg2).then(() => {
      editor.leMG2.childNodes[1].setAttribute('fill', 'rgb(0,0,0,0)')
      editor.leMG2.style.position = 'relative'
      editor.leMG2.style.top = -(149 + editor.exoEncours.height * 280) + 'px'
      editor.leMG2.style.left = '0px'
      editor.limmm3.style.width = (287 + editor.exoEncours.width * 355) + 'px'
      editor.limmm3.style.height = (149 + editor.exoEncours.height * 280) + 'px'
      tttn.style.height = (149 + editor.exoEncours.height * 280) + 'px'
      let tailleavrire = (149 + editor.exoEncours.height * 280) * 2
      editor.leMG2.addEventListener('click', () => {
        j3pEmpty(editor.divAvert)
        j3pAddContent(editor.divAvert, editor.exoEncours.avertFond)
        editor.divAvert.style.color = '#bd2e03'
      })
      for (let i = 0; i < editor.exoEncours.listZone.length; i++) {
        editor.mtgAppLecteur.setApiDoc(editor.idSvg2)
        if (editor.exoEncours.listZone[i].type !== 'label') editor.mtgAppLecteur.setHidden({ elt: editor.exoEncours.listZone[i].texte })
        if (editor.exoEncours.listZone[i].type !== 'label' && editor.exoEncours.listZone[i].type !== 'cache') {
          let aEve
          switch (editor.exoEncours.listZone[i].type) {
            case 'cercle':
              editor.mtgAppLecteur.setHidden({ elt: editor.exoEncours.listZone[i].c })
              editor.mtgAppLecteur.setHidden({ elt: editor.exoEncours.listZone[i].p })
              editor.mtgAppLecteur.setVisible({ elt: editor.exoEncours.listZone[i].cercle })
              editor.mtgAppLecteur.setColor({ elt: editor.exoEncours.listZone[i].cercle, opacity: 0, color: '#000' })
              aEve = editor.exoEncours.listZone[i].cercle
              break
            case 'poly':
              editor.mtgAppLecteur.setHidden({ elt: editor.exoEncours.listZone[i].p1 })
              editor.mtgAppLecteur.setHidden({ elt: editor.exoEncours.listZone[i].p2 })
              editor.mtgAppLecteur.setHidden({ elt: editor.exoEncours.listZone[i].p3 })
              editor.mtgAppLecteur.setHidden({ elt: editor.exoEncours.listZone[i].p4 })
              editor.mtgAppLecteur.setVisible({ elt: editor.exoEncours.listZone[i].poly })
              editor.mtgAppLecteur.setColor({ elt: editor.exoEncours.listZone[i].poly, opacity: 0, color: '#000' })
              aEve = editor.exoEncours.listZone[i].poly
              break
            case 'rectangle':
              editor.mtgAppLecteur.setHidden({ elt: editor.exoEncours.listZone[i].p1 })
              editor.mtgAppLecteur.setHidden({ elt: editor.exoEncours.listZone[i].p2 })
              editor.mtgAppLecteur.setHidden({ elt: editor.exoEncours.listZone[i].p3 })
              editor.mtgAppLecteur.setVisible({ elt: editor.exoEncours.listZone[i].poly })
              editor.mtgAppLecteur.setColor({ elt: editor.exoEncours.listZone[i].poly, opacity: 0, color: '#000' })
              aEve = editor.exoEncours.listZone[i].poly
              break
            case 'circ':
              editor.mtgAppLecteur.setHidden({ elt: editor.exoEncours.listZone[i].p1 })
              editor.mtgAppLecteur.setHidden({ elt: editor.exoEncours.listZone[i].p2 })
              editor.mtgAppLecteur.setHidden({ elt: editor.exoEncours.listZone[i].p3 })
              editor.mtgAppLecteur.setVisible({ elt: editor.exoEncours.listZone[i].poly })
              editor.mtgAppLecteur.setVisible({ elt: editor.exoEncours.listZone[i].c1 })
              editor.mtgAppLecteur.setVisible({ elt: editor.exoEncours.listZone[i].c2 })
              editor.mtgAppLecteur.setColor({ elt: editor.exoEncours.listZone[i].poly, opacity: 0, color: '#000' })
              editor.mtgAppLecteur.setColor({ elt: editor.exoEncours.listZone[i].c1, opacity: 0, color: '#000' })
              editor.mtgAppLecteur.setColor({ elt: editor.exoEncours.listZone[i].c2, opacity: 0, color: '#000' })
              editor.mtgAppLecteur.addEltListener({ elt: editor.exoEncours.listZone[i].c2, callBack: makeEvent(i), eventName: 'click' })
              editor.mtgAppLecteur.addEltListener({ elt: editor.exoEncours.listZone[i].c2, callBack: makeEvent(i), eventName: 'click' })
              aEve = editor.exoEncours.listZone[i].poly
              break
            case 'polyg':
              for (let j = 0; j < editor.exoEncours.listZone[i].listSom.length; j++) {
                editor.mtgAppLecteur.setHidden({ elt: editor.exoEncours.listZone[i].listSom[j] })
              }
              editor.mtgAppLecteur.setVisible({ elt: editor.exoEncours.listZone[i].poly })
              editor.mtgAppLecteur.setColor({ elt: editor.exoEncours.listZone[i].poly, opacity: 0, color: '#000' })
              aEve = editor.exoEncours.listZone[i].poly
              break
          }
          for (let k = 0; k < editor.exoEncours.listZone[i].surface.length; k++) {
            editor.mtgAppLecteur.setColor({
              elt: editor.exoEncours.listZone[i].surface[k],
              color: '#aaa',
              opacity: 0.5
            })
            editor.mtgAppLecteur.setHidden({
              elt: editor.exoEncours.listZone[i].surface[k]
            })
          }
          editor.mtgAppLecteur.addEltListener({ elt: aEve, callBack: makeEvent(i), eventName: 'click' })
          if (editor.exoEncours.survol) {
            editor.mtgAppLecteur.addEltListener({
              elt: aEve,
              callBack: () => {
                for (let k = 0; k < editor.exoEncours.listZone[i].surface.length; k++) {
                  editor.mtgAppLecteur.setVisible({ elt: editor.exoEncours.listZone[i].surface[k] })
                }
              },
              eventName: 'mouseover'
            })
            editor.mtgAppLecteur.addEltListener({
              elt: aEve,
              callBack: () => {
                for (let k = 0; k < editor.exoEncours.listZone[i].surface.length; k++) {
                  editor.mtgAppLecteur.setHidden({ elt: editor.exoEncours.listZone[i].surface[k] })
                }
              },
              eventName: 'mouseout'
            })
          }
        }
        if (editor.exoEncours.listZone[i].type === 'label') {
          const mondiv = j3pAddElt(tttn, 'div')
          mondiv.classList.add('pourFitContent')
          mondiv.style.position = 'relative'
          mondiv.style.verticalAlign = 'middle'
          mondiv.style.textAlign = 'center'
          mondiv.style.fontFamily = 'Bitstream Véracité Sans'
          mondiv.style.pointerEvents = 'none'
          mondiv.style.fontSize = editor.exoEncours.listZone[i].taille + 'px'
          j3pAffiche(mondiv, null, editor.exoEncours.listZone[i].texte)
          const p1 = editor.mtgAppLecteur.getPointPosition({ a: editor.exoEncours.listZone[i].p1 })
          mondiv.style.top = (p1.y - tailleavrire) + 'px'
          mondiv.style.left = p1.x + 'px'
          const aaj = mondiv.getBoundingClientRect()
          tailleavrire += aaj.height
          editor.mtgAppLecteur.setHidden({ elt: editor.exoEncours.listZone[i].p1 })
        }
      }
    })

    const exn = j3pAddElt(editor.divTest, 'div')
    exn.style.padding = '5px'
    exn.style.borderRadius = '5px'
    exn.style.backgroundImage = 'linear-gradient(to left, rgba(196, 223, 252, 0.8) 30%, rgba(128, 216, 253, 0.5))'
    const yu3 = addDefaultTable(exn, 1, 2)
    j3pAddContent(yu3[0][0], 'Avert:&nbsp;')
    editor.divAvert = yu3[0][1]
    j3pAddContent(editor.divAvert, 'Aucun')
    editor.divAvert.style.color = '#bd2e03'

    const con = j3pAddElt(editor.divTest, 'div')
    con.style.padding = '5px'
    con.style.borderRadius = '5px'
    con.style.backgroundImage = 'linear-gradient(to left, rgba(170, 255, 141, 0.5) 30%, rgba(209, 248, 194, 0.8))'
    const yu2 = addDefaultTable(con, 1, 2)
    j3pAjouteBouton(yu2[0][0], voirCo, { value: 'voir la correction' })
  }
  function voirCo () {
    for (let i = 0; i < editor.exoEncours.listZone.length; i++) {
      if (editor.exoEncours.listZone[i].surface) {
        if (editor.exoEncours.listZone[i].bon) {
          for (let k = 0; k < editor.exoEncours.listZone[i].surface.length; k++) {
            editor.mtgAppLecteur.setVisible({ elt: editor.exoEncours.listZone[i].surface[k] })
            editor.mtgAppLecteur.setColor({ elt: editor.exoEncours.listZone[i].surface[k], color: '#228500', opacity: 0.5 })
          }
        } else {
          for (let k = 0; k < editor.exoEncours.listZone[i].surface.length; k++) {
            editor.mtgAppLecteur.setVisible({ elt: editor.exoEncours.listZone[i].surface[k] })
            editor.mtgAppLecteur.setColor({ elt: editor.exoEncours.listZone[i].surface[k], color: '#f14b4b', opacity: 0.5 })
          }
        }
      }
    }
  }
  function makeEvent (i) {
    return (event) => {
      event.stopPropagation()
      event.preventDefault()
      setTimeout(() => {
        j3pEmpty(editor.divAvert)
        j3pAffiche(editor.divAvert, null, editor.exoEncours.listZone[i].avert)
        editor.divAvert.style.color = (editor.exoEncours.listZone[i].bon) ? '#289a08' : '#bd2e03'
      }, 10)
    }
  }
  function affListeZone () {
    j3pEmpty(editor.zoneList)
    j3pEmpty(editor.divTest)
    for (let i = 0; i < editor.listAffiche.length; i++) {
      j3pDetruit(editor.listAffiche[i])
    }
    editor.listAffiche = []
    editor.nomPos = []
    editor.listCoche = []
    const tab = addDefaultTable(editor.zoneList, editor.exoEncours.listZone.length + 1, 1)
    tab[0][0].style.border = '1px solid black'
    tab[0][0].style.padding = '5px'
    const tabList = addDefaultTable(tab[0][0], 2, 1)
    const tabContList1 = addDefaultTable(tabList[0][0], 1, 7)
    tabContList1[0][0].style.padding = 'Opx 5px 5px 5px'
    tabContList1[0][1].style.padding = 'Opx 5px 5px 5px'
    tabContList1[0][2].style.padding = 'Opx 5px 5px 5px'
    tabContList1[0][3].style.padding = 'Opx 5px 5px 5px'
    tabContList1[0][4].style.padding = 'Opx 5px 5px 5px'
    tabContList1[0][5].style.padding = 'Opx 5px 5px 5px'
    j3pAddContent(tabContList1[0][0], '<b>Fond</b>')
    j3pAddContent(tabContList1[0][3], '&nbsp;&nbsp;<u>Réponse fausse</u>:&nbsp;')
    editor.lacoheFondF = j3pAjouteCaseCoche(tabContList1[0][4])
    j3pAddContent(tabContList1[0][5], '&nbsp;&nbsp;<u>Réponse non comptée</u>:&nbsp;')
    editor.lacoheFondN = j3pAjouteCaseCoche(tabContList1[0][6])
    editor.lacoheFondF.checked = editor.exoEncours.fond === 'faux'
    editor.lacoheFondN.checked = editor.exoEncours.fond === 'n'
    editor.lacoheFondF.onchange = () => {
      if (editor.lacoheFondF.checked) {
        editor.lacoheFondN.checked = false
      }
      verifles3()
    }
    editor.lacoheFondN.onchange = () => {
      if (editor.lacoheFondN.checked) {
        editor.lacoheFondF.checked = false
      }
      verifles3()
    }
    function verifles3 () {
      if (!editor.lacoheFondN.checked && !editor.lacoheFondF.checked) {
        editor.lacoheFondN.checked = true
      }
      if (editor.lacoheFondN.checked) editor.exoEncours.fond = 'n'
      if (editor.lacoheFondF.checked) editor.exoEncours.fond = 'faux'
    }
    verifles3()
    const tabContList2 = addDefaultTable(tabList[1][0], 1, 2)
    j3pAddContent(tabContList2[0][0], '<u>Avertissement:</u>&nbsp;&nbsp;')
    const inputi = j3pAddElt(tabContList2[0][1], 'input', '', {
      size: 40,
      value: editor.exoEncours.avertFond
    })
    tabContList2[0][0].style.paddingBottom = '5px'
    tabContList2[0][1].style.paddingBottom = '5px'
    inputi.addEventListener('input', () => {
      editor.exoEncours.avertFond = inputi.value
    })
    for (let i = 0; i < editor.exoEncours.listZone.length; i++) {
      tab[i + 1][0].style.border = '1px solid black'
      tab[i + 1][0].style.padding = '5px'
      const tabList = addDefaultTable(tab[i + 1][0], 2, 1)
      const tabContList1 = addDefaultTable(tabList[0][0], 1, 7)
      tabContList1[0][0].style.padding = 'Opx 5px 5px 5px'
      tabContList1[0][1].style.padding = 'Opx 5px 5px 5px'
      tabContList1[0][2].style.padding = 'Opx 5px 5px 5px'
      tabContList1[0][3].style.padding = 'Opx 5px 5px 5px'
      tabContList1[0][4].style.padding = 'Opx 5px 5px 5px'
      tabContList1[0][5].style.padding = 'Opx 5px 5px 5px'
      let nnnom
      switch (editor.exoEncours.listZone[i].type) {
        case 'label':
          nnnom = editor.exoEncours.listZone[i].nom.replace('Zone', 'Label')
          break
        case 'cache':
          nnnom = editor.exoEncours.listZone[i].nom.replace('Zone', 'Cache')
          break
        default: nnnom = editor.exoEncours.listZone[i].nom
      }
      nnnom += '&nbsp;&nbsp;'
      j3pAddContent(tabContList1[0][0], '<b>' + nnnom + '</b>')
      tabContList1[0][0].style.color = editor.exoEncours.listZone[i].color
      if (editor.exoEncours.listZone[i].type === 'label') {
        j3pAddContent(tabContList1[0][2], 'taille:&nbsp;')

        const titaille = j3pAddElt(tabContList1[0][3], 'input', '', {
          type: 'number',
          value: editor.exoEncours.listZone[i].taille,
          size: 3,
          min: '15',
          max: '40'
        })
        titaille.addEventListener('change', () => {
          const averif = titaille.value
          if (averif === '' || isNaN(averif) || (Number(averif) < 15) || (Number(averif) > 40)) titaille.value = '15'
          editor.exoEncours.listZone[i].taille = Number(titaille.value)
          majAffiche(editor.exoEncours.listZone[i])
        })
        titaille.addEventListener('input', () => {
          const averif = titaille.value
          if (averif === '' || isNaN(averif) || (Number(averif) < 15) || (Number(averif) > 40)) titaille.value = '15'
          editor.exoEncours.listZone[i].taille = Number(titaille.value)
          majAffiche(editor.exoEncours.listZone[i])
        })

        afficheLab(editor.exoEncours.listZone[i], tabList[1][0])
      }
      if (editor.exoEncours.listZone[i].type !== 'label' && editor.exoEncours.listZone[i].type !== 'cache') {
        j3pAddContent(tabContList1[0][1], '&nbsp;&nbsp;<u>Réponse juste</u>:&nbsp;')
        editor.listCoche[i] = j3pAjouteCaseCoche(tabContList1[0][2])
        editor.listCoche[i].checked = editor.exoEncours.listZone[i].bon
        editor.listCoche[i].onchange = () => {
          editor.exoEncours.listZone[i].bon = editor.listCoche[i].checked
        }
        const tabContList2 = addDefaultTable(tabList[1][0], 1, 2)
        j3pAddContent(tabContList2[0][0], '<u>Avertissement:</u>&nbsp;&nbsp;')
        const inputi = j3pAddElt(tabContList2[0][1], 'input', '', {
          size: 40,
          value: editor.exoEncours.listZone[i].avert
        })
        tabContList2[0][0].style.paddingBottom = '5px'
        tabContList2[0][1].style.paddingBottom = '5px'
        inputi.addEventListener('input', () => {
          editor.exoEncours.listZone[i].avert = inputi.value
        })
        inputi.addEventListener('change', () => {
          editor.exoEncours.listZone[i].avert = inputi.value
        })
      }
      j3pAjouteBouton(tabContList1[0][3], makeSup(i), { value: 'Suppr.' })
      editor.nomPos.push(editor.exoEncours.listZone[i].nom)
      j3pAddContent(tabContList1[0][3], '&nbsp;&nbsp;')
      const boutFix = j3pAjouteBouton(tabContList1[0][3], () => {
        const el = editor.exoEncours.listZone[i]
        el.bloque = !el.bloque
        boutFix.value = el.bloque ? 'Débloquer' : 'Fixer'
        let aMod = []
        switch (el.type) {
          case 'cercle':
            aMod.push(el.c, el.p)
            break
          case 'poly':
            aMod.push(el.p1, el.p2, el.p3, el.p4)
            break
          case 'rectangle':
            aMod.push(el.p1, el.p2, el.p3)
            break
          case 'circ':
            aMod.push(el.p1, el.p2, el.p3)
            break
          case 'polyg':
            aMod = j3pClone(el.listSom)
            break
          case 'cache':
            aMod.push(el.p1, el.p2)
            break
          case 'label':
            aMod.push(el.p1)
            break
        }
        editor.mtgAppLecteur.setApiDoc(editor.idSvg)
        if (editor.exoEncours.listZone[i].bloque) {
          for (let j = 0; j < aMod.length; j++) {
            editor.mtgAppLecteur.setHidden({ elt: aMod[j] })
          }
        } else {
          for (let j = 0; j < aMod.length; j++) {
            editor.mtgAppLecteur.setVisible({ elt: aMod[j] })
          }
        }
      }, { value: editor.exoEncours.listZone[i].bloque ? 'Débloquer' : 'Fixer' })
      if (i !== editor.exoEncours.listZone.length - 1) {
        j3pAddContent(tabContList1[0][5], '&nbsp;&nbsp;')
        j3pAjouteBouton(tabContList1[0][5], () => { makeSurfDown(i) }, { value: '↓' })
      }
      if (i !== 0) {
        j3pAddContent(tabContList1[0][6], '&nbsp;&nbsp;')
        j3pAjouteBouton(tabContList1[0][6], () => { makeSurfUp(i) }, { value: '↑' })
      }
    }
    gereMove()
  }
  function afficheLab (el, ou) {
    creeAffiche(el)
    const tabRep = addDefaultTable(ou, 1, 2)
    j3pAddContent(tabRep[0][0], '<b><u>Texte:</u></b>&nbsp;&nbsp;')
    const input = j3pAddElt(tabRep[0][1], 'input', {
      size: 80,
      value: el.texte
    })
    input.addEventListener('change', () => {
      el.texte = input.value
      majAffiche(el)
    })
    input.addEventListener('input', () => {
      el.texte = input.value
      majAffiche(el)
    })
  }
  function creeAffiche (el) {
    const mondiv = j3pAddElt(editor.whereFig, 'span')
    mondiv.style.position = 'relative'
    mondiv.style.verticalAlign = 'middle'
    mondiv.style.textAlign = 'center'
    mondiv.el = el
    editor.listAffiche.push(mondiv)
    mondiv.style.fontFamily = 'Bitstream Véracité Sans'
    mondiv.style.pointerEvents = 'none'
    mondiv.style.fontSize = el.taille + 'px'
    j3pAffiche(mondiv, null, el.texte)
  }
  function majAffiche (el) {
    for (let i = 0; i < editor.listAffiche.length; i++) {
      if (editor.listAffiche[i].el.nom === el.nom) {
        j3pEmpty(editor.listAffiche[i])
        editor.listAffiche[i].style.fontSize = el.taille + 'px'
        j3pAffiche(editor.listAffiche[i], null, el.texte)
      }
    }
    gereMove()
  }
  function makeSurfDown (i) {
    const ag = editor.exoEncours.listZone[i]
    editor.exoEncours.listZone.splice(i, 1)
    editor.exoEncours.listZone.splice(i + 1, 0, ag)
    remetOrdre()
    affListeZone()
  }
  function makeSurfUp (i) {
    const ag = editor.exoEncours.listZone[i]
    editor.exoEncours.listZone.splice(i, 1)
    editor.exoEncours.listZone.splice(i - 1, 0, ag)
    remetOrdre()
    affListeZone()
  }
  function remetOrdre () {
    for (let i = 0; i < editor.exoEncours.listZone.length; i++) {
      if (editor.exoEncours.listZone[i].surface) {
        for (let k = 0; k < editor.exoEncours.listZone[i].surface.length; k++) {
          editor.mtgAppLecteur.reclassMax({ elt: editor.exoEncours.listZone[i].surface[k] })
        }
      }
    }
  }
  function makeSup (i) {
    return () => {
      const av = editor.exoEncours.listZone[i]
      editor.exoEncours.listZone.splice(i, 1)
      editor.mtgAppLecteur.setApiDoc(editor.idSvg)
      switch (av.type) {
        case 'cercle':
          editor.mtgAppLecteur.deleteElt({ elt: av.c })
          break
        case 'poly':
          editor.mtgAppLecteur.deleteElt({ elt: av.p1 })
          editor.mtgAppLecteur.deleteElt({ elt: av.p2 })
          editor.mtgAppLecteur.deleteElt({ elt: av.p3 })
          editor.mtgAppLecteur.deleteElt({ elt: av.p4 })
          break
        case 'rectangle':
          editor.mtgAppLecteur.deleteElt({ elt: av.p1 })
          editor.mtgAppLecteur.deleteElt({ elt: av.p2 })
          break
        case 'circ':
          editor.mtgAppLecteur.deleteElt({ elt: av.p1 })
          editor.mtgAppLecteur.deleteElt({ elt: av.p2 })
          break
        case 'polyg':
          for (let i = 0; i < av.listSom.length; i++) {
            editor.mtgAppLecteur.deleteElt({ elt: av.listSom[i] })
          }
          break
      }
      affListeZone()
    }
  }

  function faitMG () {
    j3pEmpty(editor.whereFig)
    editor.whereFig.style.height = '500px'
    editor.whereFig.style.border = '7px solid black'
    editor.idSvg = j3pGetNewId()
    editor.limmm2 = j3pAddElt(editor.whereFig, 'img', '', { src: editor.exoEncours.image, width: editor.exoEncours.widthImage, height: editor.exoEncours.heightImage })
    editor.limmm2.style.position = 'relative'
    editor.limmm2.style.top = '0px'
    editor.limmm2.style.left = '0px'
    editor.leMG = j3pCreeSVG(editor.whereFig, { id: editor.idSvg, width: 800, height: 500 })
    editor.mtgAppLecteur.removeAllDoc()
    editor.mtgAppLecteur.addDoc(editor.idSvg, editor.exoEncours.txtFigure || genMGEx, true)
    editor.mtgAppLecteur.setApiDoc(editor.idSvg)
    editor.mtgAppLecteur.calculateFirstTime(editor.idSvg)
    editor.mtgAppLecteur.display(editor.idSvg).then(() => {
      editor.leMG.childNodes[1].setAttribute('fill', 'rgb(0,0,0,0)')
      editor.leMG.style.position = 'relative'
      editor.leMG.addEventListener('mousemove', gereMove, false)
      editor.leMG.style.top = '0px'
      editor.leMG.style.left = '0px'
      editor.mtgAppLecteur.addSvgListener({
        eventName: 'mousedown',
        callBack: (event, x, y) => {
          event.preventDefault()
          event.stopPropagation()
          editor.funcClick(x, y)
        }
      })
      editor.mtgAppLecteur.addSvgListener({
        eventName: 'mousemove',
        callBack: (event, x, y) => {
          editor.funcMov(x, y)
        }
      })
      editor.leMG.addEventListener('mousedown', (event) => {
        event.preventDefault()
        event.stopPropagation()
      })
      gereMove()
    })
    editor.whereFig.style.overflow = 'hidden'
  }
  function gereMove () {
    const a = editor.mtgAppLecteur.valueOf(editor.idSvg, 'A')
    const b = editor.mtgAppLecteur.valueOf(editor.idSvg, 'B')
    editor.limmm2.style.width = (287 + b * 355) + 'px'
    editor.limmm2.style.height = (149 + a * 280) + 'px'
    editor.leMG.style.top = '-' + editor.limmm2.style.height
    const tailleaVire = (149 + a * 280) + 500
    let tailleaVireX = 0
    for (let i = 0; i < editor.listAffiche.length; i++) {
      const p1 = editor.mtgAppLecteur.getPointPosition({ a: editor.listAffiche[i].el.p1 })
      editor.listAffiche[i].style.top = (p1.y - tailleaVire) + 'px'
      editor.listAffiche[i].style.left = (p1.x - tailleaVireX) + 'px'
      const aaj = editor.listAffiche[i].getBoundingClientRect()
      tailleaVireX += aaj.width
    }
  }
  function faitIm () {
    editor.exoEncours.image = editor.exoEncours.image || genIm
    editor.exoEncours.widthImage = 200
    editor.exoEncours.heightImage = 200
    function majIm () {
      editor.limmm2.src = editor.exoEncours.image
    }
    importImage(editor.whereFigIm, editor.exoEncours, 'image', 'widthImage', 'heightImage', true, editor, null, majIm)
  }
  function affiErr (ou) {
    j3pEmpty(ou)
    j3pAddContent(ou, '<b><u>Erreurs à discerner:</u></b>&nbsp;')
    // ou.style.border = '1px solid black'
    for (let i = 0; i < editor.exoEncours.errPrev.length; i++) {
      const aj = addDefaultTable(ou, 2, 4)
      j3pAffiche(aj[0][0], null, (i + 1) + '. à repérer:&nbsp;')
      j3pAffiche(aj[1][0], null, 'remède:&nbsp;')
      aj[1][0].style.textAlign = 'right'
      const yu = j3pAddElt(aj[0][1], 'input', '', {
        value: editor.exoEncours.errPrev[i].rep,
        size: 50
      })
      yu.addEventListener('change', makeChange(i, yu))
      const yu2 = j3pAddElt(aj[1][1], 'input', '', {
        value: editor.exoEncours.errPrev[i].remede,
        size: 50
      })
      yu2.addEventListener('change', makeChange2(i, yu2))
      j3pAddContent(aj[0][2], '&nbsp;&nbsp;')
      j3pAjouteBouton(aj[0][3], makeSup(i, ou), { value: '✄' })
    }
    j3pAddContent(ou, '\n')
    j3pAjouteBouton(ou, () => { addEr(ou) }, { value: '+' })
  }
  function addEr (ou) {
    editor.exoEncours.errPrev.push({ rep: '', remede: '' })
    affiErr(ou)
  }
  function makeChange (i, t) {
    return () => {
      editor.exoEncours.errPrev[i].rep = t.value
    }
  }
  function makeChange2 (i, t) {
    return () => {
      editor.exoEncours.errPrev[i].remede = t.value
    }
  }
  function valModifExo () {
    editor.okVire = false
    sauve()
    editor.ProgrammeS[editor.exoEncoursi].image = editor.exoEncours.image
    editor.ProgrammeS[editor.exoEncoursi].yaSon = editor.exoEncours.yaSon
    if (editor.ProgrammeS[editor.exoEncoursi].yaSon) {
      editor.ProgrammeS[editor.exoEncoursi].son = editor.exoEncours.son
    } else {
      editor.ProgrammeS[editor.exoEncoursi].son = undefined
    }
    editor.ProgrammeS[editor.exoEncoursi].nom = editor.exoEncours.nom
    editor.ProgrammeS[editor.exoEncoursi].survol = editor.exoEncours.survol
    editor.ProgrammeS[editor.exoEncoursi].consigneG = editor.exoEncours.consigneG
    editor.ProgrammeS[editor.exoEncoursi].txtFigure = editor.exoEncours.txtFigure
    editor.ProgrammeS[editor.exoEncoursi].width = editor.exoEncours.width
    editor.ProgrammeS[editor.exoEncoursi].height = editor.exoEncours.height
    editor.ProgrammeS[editor.exoEncoursi].fond = editor.exoEncours.fond
    editor.ProgrammeS[editor.exoEncoursi].fautTout = editor.exoEncours.fautTout
    editor.ProgrammeS[editor.exoEncoursi].listZone = j3pClone(editor.exoEncours.listZone)
    editor.ProgrammeS[editor.exoEncoursi].avertFond = editor.exoEncours.avertFond
    menuExo()
  }
  function vireConstEncours () {
    for (let i = 0; i < editor.objAvire.length; i++) {
      editor.mtgAppLecteur.deleteElt({ elt: editor.objAvire[i] })
    }
    editor.objAvire = []
    editor.mtgAppLecteur.getDoc(editor.idSvg).defaultCursor = undefined
    if (editor.spanConst) {
      j3pEmpty(editor.spanConst)
      j3pAddContent(editor.spanConst, '&nbsp;')
    }
  }
  function chgNom () {
    editor.exoEncours.nom = editor.inputNouveauNom.value
    const bufadd = (editor.exoEncours.nom) ? ' (' + editor.exoEncours.nom + ') ' : ''
    j3pEmpty(editor.cellModifExo)
    j3pAddContent(editor.cellModifExo, '<b>Modification de l’exercice ' + editor.numExo + bufadd + '</b>.')
  }

  function supExo (i) {
    editor.ProgrammeS.splice(i, 1)
    menuExo()
  }
  function faisLimite () {
    const viz = (editor.listeparamLimite.reponse === 'Oui') ? '' : 'none'
    editor.chronoCahc2.style.display = viz
  }
  return new Promise((resolve, reject) => {
    function onSaveParams () {
      // on affecte nos nouvelles valeurs (à récupérer dans l’éditeur mis ci-dessus)
      if (document.fullscreenElement) document.exitFullscreen()
      const aComp = []
      for (let prog = 0; prog < editor.ProgrammeS.length; prog++) {
        let idMod = faisId(editor.ProgrammeS[prog].image)
        let iMod = aComp.indexOf(idMod)
        if (iMod !== -1) {
          editor.ProgrammeS[prog].image = { type: 'clone', num: iMod }
        } else {
          aComp.push(idMod)
        }
        if (editor.ProgrammeS[prog].yaSon) {
          idMod = faisId(editor.ProgrammeS[prog].son)
          iMod = aComp.indexOf(idMod)
          if (iMod !== -1) {
            editor.ProgrammeS[prog].son = { type: 'clone', num: iMod }
          } else {
            aComp.push(idMod)
          }
        }
        idMod = faisId(editor.ProgrammeS[prog].txtFigure)
        iMod = aComp.indexOf(idMod)
        if (iMod !== -1) {
          editor.ProgrammeS[prog].txtFigure = { type: 'clone', num: iMod }
        } else {
          aComp.push(idMod)
        }
      }
      resolve(
        {
          Liste: JSON.stringify(
            {
              titre: editor.inputTitre.value,
              liste: editor.ProgrammeS
            }
          ),
          limite: (editor.listeparamLimite.reponse === 'Non') ? 0 : Number(editor.inputLimite.value),
          nbrepetitions: Number(editor.nbrepetitions.value),
          theme: editor.themeL.reponse,
          ordonne: editor.ordonneL.reponse
        })
    }

    function onSaveParamsT () {
      if (editor.yaBlok) {
        const yy = j3pModale2({ titre: 'Avertissement ', contenu: 'Un exercice est en cours\n de modification.', divparent: container })
        j3pAddContent(yy, '\n')
        j3pAjouteBouton(yy, () => {
          j3pDetruit(yy)
          j3pDetruit(yy.masque)
        }, { value: 'Annuler' })
        j3pAddContent(yy, '\n')
        j3pAddContent(yy, '\n')
        j3pAjouteBouton(yy, () => {
          j3pDetruit(yy)
          j3pDetruit(yy.masque)
          onSaveParams()
        }, { value: 'Tant pis' })
        return
      }
      onSaveParams()
    }

    // on ajoute notre éditeur dans container
    const btnsContainer = j3pAddElt(container, 'div', 'Chargement en cours…', {
      style: {
        margin: '1rem auto',
        textAlign: 'center'
      }
    })

    j3pEmpty(btnsContainer)
    // Important, les boutons annuler / valider
    loadMq()
      .then(() => {
        container.parentNode.style.opacity = 1
        const btnProps = { style: { margin: '0.2rem', fontSize: '120%' } }
        j3pAddElt(btnsContainer, 'button', 'Annuler', btnProps)
          .addEventListener('click', () => {
            if (document.fullscreenElement) document.exitFullscreen()
            resolve()
          })
        j3pAddElt(btnsContainer, 'button', 'Valider', btnProps)
          .addEventListener('click', onSaveParamsT)
        try {
          valuesDeb.Liste = JSON.parse(values.Liste)
          editor.ProgrammeS = j3pClone(valuesDeb.Liste.liste)
          const aComp = []
          for (let prog = 0; prog < editor.ProgrammeS.length; prog++) {
            let aCompte = true
            if (editor.ProgrammeS[prog].image !== 'string') {
              if (editor.ProgrammeS[prog].image.type === 'clone') {
                aCompte = false
                editor.ProgrammeS[prog].image = j3pClone(aComp[editor.ProgrammeS[prog].image.num])
              }
            }
            if (aCompte) aComp.push(j3pClone(editor.ProgrammeS[prog].image))
            aCompte = true
            if (editor.ProgrammeS[prog].yaSon) {
              if (editor.ProgrammeS[prog].son !== 'string') {
                if (editor.ProgrammeS[prog].son.type === 'clone') {
                  aCompte = false
                  editor.ProgrammeS[prog].son = j3pClone(aComp[editor.ProgrammeS[prog].son.num])
                }
              }
              if (aCompte) aComp.push(j3pClone(editor.ProgrammeS[prog].son))
            }
            aCompte = true
            if (editor.ProgrammeS[prog].txtFigure !== 'string') {
              if (editor.ProgrammeS[prog].txtFigure.type === 'clone') {
                aCompte = false
                editor.ProgrammeS[prog].txtFigure = j3pClone(aComp[editor.ProgrammeS[prog].txtFigure.num])
              }
            }
            if (aCompte) aComp.push(j3pClone(editor.ProgrammeS[prog].txtFigure))
          }
        } catch (error) {
          console.error(error)
        }
        getMtgCore({ withMathJax: true, withApiPromise: true })
          .then(
            // success
            (mtgAppLecteur) => {
              editor.mtgAppLecteur = mtgAppLecteur
              editor.funcClick = () => {
              }
              container.addEventListener('mousedown', () => {
                editor.funcClick = () => {}
                editor.funcMov = () => {}
                if (editor.okVire) vireConstEncours()
              })
              creeLesDiv()
              menuExo()
              menuTotal()
            },
            // failure
            (error) => {
              j3pShowError(error, { message: 'mathgraph n’est pas correctement chargé', mustNotify: true })
            })
          // plantage dans le code de success
          .catch(j3pShowError)
      })
      .catch(reject)
  }) // promesse retournée
}
