import { j3pAddElt, j3pAjouteBouton, j3pClone, j3pEmpty, j3pGetNewId, j3pShowError, j3pShuffle } from 'src/legacy/core/functions'
import ZoneStyleMathquill3 from 'src/legacy/outils/zoneStyleMathquill/ZoneStyleMathquill3'
import { addDefaultTable } from 'src/legacy/themes/table'
import { getMtgCore } from 'src/lib/outils/mathgraph'
import getDefaultProgramme from './defaultProgramme'
import { j3pAffiche } from 'src/lib/mathquill/functions'
import { j3pCreeSVG } from 'src/legacy/core/functionsSvg'
import { placeJouerSon } from 'src/legacy/outils/zoneStyleMathquill/functions'
import './resMake.css'

async function initeditor (container, values, btn) {
  const { default: stor } = await import('./editorProgramme.js')
  return stor(container, values, btn) // la fct qui prend un conteneur et les params comme arguments
}

const larestr = 'abcdefghijklmnopqrstuvwxyzABCDEFGHIJKLMNOPQRSTUVWXYZ0123456789.,*-+()@[]àéèù/%'

/**
 * Ajouter une description ici
 * @fileOverview
 * @author Tommy Barroy <t.barroy@laposte.net>
 * @since 2017-10
 */
export const params = {
  outils: ['calculatrice', 'mathquill'],
  /**
   * Ce qui est paramétrable dans l’éditeur de graphe
   * @type {LegacySectionParamsParametres}
   */
  parametres: [
    ['nbrepetitions', 1, 'entier', 'Nombre de répétitions de la section'],
    ['limite', 0, 'entier', 'Temps disponible par répétition, en secondes'],
    ['Liste', JSON.stringify(getDefaultProgramme()), 'multiEditor', 'Json figure de base et figure attendue.', initeditor],
    ['ordonne', 'Oui', 'liste', '<u>Oui</u>:  Exercices dans l’ordre', ['Oui', 'Non']],
    ['theme', 'zonesAvecImageDeFond', 'liste', 'Modèle pour l’affichage', ['standard', 'zonesAvecImageDeFond']]
  ]
}

/**
 * section choix
 * @this {Parcours}
 */
export default function main () {
  const me = this
  const ds = me.donneesSection
  const stor = me.storage

  function initSection () {
    // chargement mtg
    me.construitStructurePage('presentation3')
    try {
      stor.Liste = JSON.parse(ds.Liste)
      const aComp = []
      //
      for (let prog = 0; prog < stor.Liste.liste.length; prog++) {
        let aCompte = true
        if (stor.Liste.liste[prog].image !== 'string') {
          if (stor.Liste.liste[prog].image.type === 'clone') {
            aCompte = false
            stor.Liste.liste[prog].image = j3pClone(aComp[stor.Liste.liste[prog].image.num])
          }
        }
        if (aCompte) aComp.push(j3pClone(stor.Liste.liste[prog].image))
        aCompte = true
        if (stor.Liste.liste[prog].yaSon) {
          if (stor.Liste.liste[prog].son !== 'string') {
            if (stor.Liste.liste[prog].son.type === 'clone') {
              aCompte = false
              stor.Liste.liste[prog].son = j3pClone(aComp[stor.Liste.liste[prog].son.num])
            }
          }
          if (aCompte) aComp.push(j3pClone(stor.Liste.liste[prog].son))
        }
        aCompte = true
        if (stor.Liste.liste[prog].txtFigure !== 'string') {
          if (stor.Liste.liste[prog].txtFigure.type === 'clone') {
            aCompte = false
            stor.Liste.liste[prog].txtFigure = j3pClone(aComp[stor.Liste.liste[prog].txtFigure.num])
          }
        }
        if (aCompte) aComp.push(j3pClone(stor.Liste.liste[prog].txtFigure))
      }
    } catch (error) {
      console.error(error)
      j3pShowError('Parametres invalide')
      stor.Legendes = getDefaultProgramme()
    }
    let titre = stor.Liste.titre
    if (!titre || titre === '') titre = ' '
    me.afficheTitre(titre)
    stor.lesExos = []
    if (ds.ordonne === 'Non') {
      const exPos = j3pShuffle(stor.Liste.liste)
      for (let i = 0; i < ds.nbrepetitions; i++) {
        stor.lesExos.push(exPos[i % exPos.length])
      }
    } else {
      for (let i = 0; i < ds.nbrepetitions; i++) {
        stor.lesExos.splice(0, 0, stor.Liste.liste[i % stor.Liste.liste.length])
      }
    }
    getMtgCore({ withMathJax: true, withApiPromise: true })
      .then(
        // success
        (mtgAppLecteur) => {
          stor.mtgAppLecteur = mtgAppLecteur
          enonceMain()
        },
        // failure
        (error) => {
          j3pShowError(error, { message: 'mathgraph n’est pas correctement chargé', mustNotify: true })
        })
      // plantage dans le code de success
      .catch(j3pShowError)
  }
  function creeLesDiv () {
    stor.isAgrand = false
    stor.lesdiv = {}
    stor.lesdiv.conteneur = j3pAddElt(me.zonesElts.MG, 'div', {
      style: me.styles.etendre('toutpetit.enonce', { padding: '10px' })
    })
    stor.concot = addDefaultTable(stor.lesdiv.conteneur, 1, 1)[0][0]
    const tab2 = addDefaultTable(stor.concot, 1, 4)
    stor.lesdiv.enonceG = addDefaultTable(tab2[0][0], 1, 1)[0][0]
    stor.lesdiv.son = addDefaultTable(tab2[0][0], 1, 1)[0][0]
    stor.lesdiv.son.style.display = 'none'
    stor.lesdiv.travail = j3pAddElt(stor.lesdiv.conteneur, 'div')
    stor.lesdiv.travail.style.overflow = 'hidden'
    tab2[0][0].classList.add('enonce')
    stor.lesdiv.zoneBout1 = tab2[0][1]
    stor.lesdiv.zoneBout2 = tab2[0][2]
    stor.lesdiv.zoneBout3 = tab2[0][3]
    stor.lesdiv.zoneBout1.style.paddingLeft = '30px'
    stor.lesdiv.zoneBout2.style.paddingLeft = '30px'
    stor.lesdiv.zoneBout3.style.paddingLeft = '30px'
    stor.lesdiv.zoneBout2.style.display = 'none'
    stor.lesdiv.zoneBout3.style.display = 'none'
    stor.lesdiv.travail.classList.add('travail')
    me.zonesElts.MG.classList.add('fond')
    stor.tab2 = tab2
  }
  function fauxOK () {
    me.sectionCourante()
  }
  function agrandiMax () {
    stor.isAgrand = true
    stor.lesdiv.zoneBout1.style.display = 'none'
    stor.lesdiv.zoneBout2.style.display = ''
    stor.lesdiv.zoneBout3.style.display = ''
    me.zonesElts.HG.style.display = 'none'
    me.zonesElts.HD.style.display = 'none'
    const taillEnonce = stor.concot.getBoundingClientRect()
    stor.tailllimm = j3pClone(stor.limmm3.getBoundingClientRect())
    stor.tailltravail = j3pClone(stor.lesdiv.travail.getBoundingClientRect())
    stor.tailleMG = j3pClone(me.zonesElts.MG.getBoundingClientRect())
    const multMaxX = stor.tailleMG.width / stor.tailltravail.width
    const multMaxy = (stor.tailleMG.height + 100 - taillEnonce.height) / stor.tailltravail.height
    const aMul = (Math.min(multMaxX, multMaxy) - 0.05)
    stor.lesdiv.travail.style.width = stor.tailltravail.width * aMul + 'px'
    stor.limmm3.style.width = stor.tailllimm.width * aMul + 'px'
    stor.lesdiv.travail.style.height = stor.tailltravail.height * aMul + 'px'
    stor.limmm3.style.height = stor.tailllimm.height * aMul + 'px'
    me.zonesElts.MG.style.height = (stor.tailleMG.height + 120) + 'px'
    stor.aMul = aMul
    replaceMezZones(aMul)
    me.cacheBoutonValider()
  }
  function agrandiMin () {
    stor.isAgrand = false
    stor.lesdiv.zoneBout1.style.display = ''
    stor.lesdiv.zoneBout2.style.display = 'none'
    stor.lesdiv.zoneBout3.style.display = 'none'
    me.zonesElts.HG.style.display = ''
    me.zonesElts.HD.style.display = ''
    stor.lesdiv.travail.style.width = stor.tailltravail.width + 'px'
    stor.limmm3.style.width = stor.tailllimm.width + 'px'
    stor.lesdiv.travail.style.height = stor.tailltravail.height + 'px'
    stor.limmm3.style.height = stor.tailllimm.height + 'px'
    me.zonesElts.MG.style.height = stor.tailleMG.height + 'px'
    replaceMezZones(1)
    if (me.etat === 'correction') me.afficheBoutonValider()
  }

  function enonceMain () {
    me.videLesZones()
    creeLesDiv()
    stor.monDivCo = []
    stor.lexo = stor.lesExos.pop()
    if (stor.lexo.consigneG !== '') {
      j3pAffiche(stor.lesdiv.enonceG, null, stor.lexo.consigneG)
      stor.lesdiv.enonceG.style.whiteSpace = 'normal'
    } else {
      stor.lesdiv.enonceG.style.display = 'none'
    }
    if (stor.lexo.yaSon) {
      stor.lesdiv.son.style.display = ''
      const contui = addDefaultTable(stor.lesdiv.son, 1, 1)[0][0]
      const ui = j3pAddElt(contui, 'div')
      ui.style.border = '1px solid black'
      ui.style.borderRadius = '5px'
      ui.style.padding = '5px'
      ui.style.paddingTop = '0px'
      ui.style.background = '#aeef59'
      ui.style.textAlign = 'center'
      placeJouerSon(ui, stor.lexo, stor, 'son')
    }
    if (stor.lexo.consigneG === '' && !stor.lexo.yaSon) stor.tab2[0][0].style.display = 'none'
    stor.idSvg2 = j3pGetNewId()
    stor.limmm3 = j3pAddElt(stor.lesdiv.travail, 'img', '', { src: stor.lexo.image })
    stor.limmm3.style.position = 'relative'
    stor.limmm3.style.top = '0px'
    stor.limmm3.style.left = '0px'
    stor.lesdiv.travail.style.minWidth = (287 + stor.lexo.width * 355) + 'px'
    stor.leMG2 = j3pCreeSVG(stor.lesdiv.travail, { id: stor.idSvg2, width: (287 + stor.lexo.width * 355), height: (149 + stor.lexo.height * 280) })
    // stor.leMG2.style.border = '1px solid black'
    stor.mtgAppLecteur.removeAllDoc()
    stor.mtgAppLecteur.addDoc(stor.idSvg2, stor.lexo.txtFigure, false)
    stor.mtgAppLecteur.setApiDoc(stor.idSvg2)
    stor.mtgAppLecteur.calculate(stor.idSvg2)
    stor.mtgAppLecteur.display(stor.idSvg2).then(() => {
      stor.mesZone = []
      stor.leMG2.childNodes[1].setAttribute('fill', 'rgb(0,0,0,0)')
      stor.leMG2.style.position = 'relative'
      stor.leMG2.style.top = -(149 + stor.lexo.height * 280) + 'px'
      stor.leMG2.style.left = '0px'
      stor.limmm3.style.width = (287 + stor.lexo.width * 355) + 'px'
      stor.limmm3.style.height = (149 + stor.lexo.height * 280) + 'px'
      stor.lesdiv.travail.style.height = (149 + stor.lexo.height * 280 + 1) + 'px'
      stor.lesdiv.travail.style.width = (287 + stor.lexo.width * 355) + 'px'
      let tailleavrire = (149 + stor.lexo.height * 280) * 2
      stor.tailleavrireBase = 149 + stor.lexo.height * 280
      stor.listMondiv = []
      for (let i = 0; i < stor.lexo.listZone.length; i++) {
        if (stor.lexo.listZone[i].type === 'rectangle') {
          for (let k = 0; k < stor.lexo.listZone[i].surface.length; k++) {
            stor.mtgAppLecteur.setHidden({ elt: stor.lexo.listZone[i].surface[k] })
          }
          const p1 = stor.mtgAppLecteur.getPointPosition({ a: stor.lexo.listZone[i].p1 })
          const p2 = stor.mtgAppLecteur.getPointPosition({ a: stor.lexo.listZone[i].p2 })
          const p3 = stor.mtgAppLecteur.getPointPosition({ a: stor.lexo.listZone[i].p3 })
          const p4 = stor.mtgAppLecteur.getPointPosition({ a: stor.lexo.listZone[i].p4 })
          const x = Math.min(p1.x, p2.x, p3.x, p4.x)
          const y = Math.min(p1.y, p2.y, p3.y, p4.y)
          const x2 = Math.max(p1.x, p2.x, p3.x, p4.x)
          const y2 = Math.max(p1.y, p2.y, p3.y, p4.y)
          const mondiv = j3pAddElt(stor.lesdiv.travail, 'div')
          mondiv.style.position = 'relative'
          mondiv.style.top = y - tailleavrire + 'px'
          mondiv.y = y
          mondiv.x = x
          mondiv.larg = (x2 - x)
          mondiv.height = (y2 - y)
          mondiv.style.left = x + 'px'
          mondiv.style.width = (x2 - x) + 'px'
          mondiv.style.height = (y2 - y) + 'px'
          mondiv.style.background = '#fff'
          tailleavrire += (y2 - y)
          mondiv.style.verticalAlign = 'middle'
          mondiv.style.textAlign = 'center'
          mondiv.style.cursor = 'text'
          const maZ = new ZoneStyleMathquill3(mondiv, { restric: larestr, sansBord: true, petiteFont: true })
          maZ.lediv = mondiv
          maZ.zone = stor.lexo.listZone[i]
          stor.mesZone.push(maZ)
          mondiv.addEventListener('click', () => {
            setTimeout(() => {
              maZ.focus()
            }, 10)
          })
          if (stor.lexo.listZone[i].encadre) mondiv.style.border = '1px solid black'
          stor.listMondiv.push(mondiv)
        }
        if (stor.lexo.listZone[i].type === 'cache') {
          for (let k = 0; k < stor.lexo.listZone[i].surface.length; k++) {
            stor.mtgAppLecteur.setHidden({ elt: stor.lexo.listZone[i].surface[k] })
          }
          const p1 = stor.mtgAppLecteur.getPointPosition({ a: stor.lexo.listZone[i].p1 })
          const p2 = stor.mtgAppLecteur.getPointPosition({ a: stor.lexo.listZone[i].p2 })
          const p3 = stor.mtgAppLecteur.getPointPosition({ a: stor.lexo.listZone[i].p3 })
          const p4 = stor.mtgAppLecteur.getPointPosition({ a: stor.lexo.listZone[i].p4 })
          const x = Math.min(p1.x, p2.x, p3.x, p4.x)
          const y = Math.min(p1.y, p2.y, p3.y, p4.y)
          const x2 = Math.max(p1.x, p2.x, p3.x, p4.x)
          const y2 = Math.max(p1.y, p2.y, p3.y, p4.y)
          const mondiv = j3pAddElt(stor.lesdiv.travail, 'div')
          mondiv.y = y
          mondiv.x = x
          mondiv.larg = (x2 - x)
          mondiv.height = (y2 - y)
          mondiv.style.position = 'relative'
          mondiv.style.top = y - tailleavrire + 'px'
          mondiv.style.left = x + 'px'
          mondiv.style.width = (x2 - x) + 'px'
          mondiv.style.height = (y2 - y) + 'px'
          mondiv.style.background = '#fff'
          tailleavrire += (y2 - y)
          stor.listMondiv.push(mondiv)
        }
        if (stor.lexo.listZone[i].type === 'label') {
          const mondiv = j3pAddElt(stor.lesdiv.travail, 'div')
          mondiv.classList.add('pourFitContent')
          mondiv.style.position = 'relative'
          mondiv.style.verticalAlign = 'middle'
          mondiv.style.textAlign = 'center'
          mondiv.style.fontFamily = 'Bitstream Véracité Sans'
          mondiv.style.pointerEvents = 'none'
          mondiv.style.fontSize = stor.lexo.listZone[i].taille + 'px'
          j3pAffiche(mondiv, null, stor.lexo.listZone[i].texte)
          const p1 = stor.mtgAppLecteur.getPointPosition({ a: stor.lexo.listZone[i].p1 })
          mondiv.style.top = (p1.y - tailleavrire) + 'px'
          mondiv.style.left = p1.x + 'px'
          mondiv.y = p1.y
          mondiv.x = p1.x
          const aaj = mondiv.getBoundingClientRect()
          mondiv.larg = aaj.width
          mondiv.height = aaj.height
          tailleavrire += aaj.height
          stor.listMondiv.push(mondiv)
        }
        stor.mtgAppLecteur.setHidden({ elt: stor.lexo.listZone[i].p1 })
        if (stor.lexo.listZone[i].type !== 'label') {
          stor.mtgAppLecteur.setHidden({ elt: stor.lexo.listZone[i].texte })
          stor.mtgAppLecteur.setHidden({ elt: stor.lexo.listZone[i].p2 })
          stor.mtgAppLecteur.setHidden({ elt: stor.lexo.listZone[i].p3 })
          stor.mtgAppLecteur.setVisible({ elt: stor.lexo.listZone[i].poly })
          stor.mtgAppLecteur.setColor({ elt: stor.lexo.listZone[i].poly, opacity: 0, color: '#000' })
        }
      }
    })
    j3pAjouteBouton(stor.lesdiv.zoneBout1, agrandiMax, { value: '⬜' })
    j3pAjouteBouton(stor.lesdiv.zoneBout2, agrandiMin, { value: '◽' })
    stor.boutFauxOK = j3pAjouteBouton(stor.lesdiv.zoneBout3, fauxOK, { value: 'ok', className: 'big ok' })
    me.finEnonce()
  }

  function replaceMezZones (mul) {
    const tailleLim = j3pClone(stor.limmm3.getBoundingClientRect())
    let tailleavrire = 149 + stor.lexo.height * 280 + tailleLim.height
    for (let i = 0; i < stor.listMondiv.length; i++) {
      stor.listMondiv[i].style.top = (stor.listMondiv[i].y * mul - tailleavrire) + 'px'
      stor.listMondiv[i].style.left = (stor.listMondiv[i].x * mul) + 'px'
      stor.listMondiv[i].style.width = stor.listMondiv[i].larg * mul + 'px'
      stor.listMondiv[i].style.height = stor.listMondiv[i].height * mul + 'px'
      tailleavrire += stor.listMondiv[i].height * mul
    }
    for (let i = 0; i < stor.monDivCo.length; i++) {
      stor.monDivCo[i].style.width = (stor.monDivCo[i].width * mul) + 'px'
      stor.monDivCo[i].style.height = (stor.monDivCo[i].height * mul) + 'px'
    }
  }
  function isRepOk () {
    stor.repEl = []
    let ok = true
    for (let i = 0; i < stor.mesZone.length; i++) {
      if (stor.mesZone[i]) stor.repEl[i] = stor.mesZone[i].reponse()
    }
    for (let i = 0; i < stor.mesZone.length; i++) {
      if (stor.mesZone[i]) {
        if (okrep(i)) {
          stor.mesZone[i].corrige(true)
        } else {
          const monDivCo = j3pAddElt(stor.mesZone[i].lediv, 'div')
          const coo = stor.mesZone[i].lediv.getBoundingClientRect()
          monDivCo.style.top = '0px'
          monDivCo.style.left = '0px'
          monDivCo.style.width = coo.width - 3 + 'px'
          monDivCo.style.height = coo.height - 3 + 'px'
          monDivCo.style.background = '#8bc1fa'
          monDivCo.style.borderRadius = '5px'
          monDivCo.style.color = '#4a8a30'
          monDivCo.width = (!stor.isAgrand) ? (coo.width - 3) : (coo.width - 3) / stor.aMul
          monDivCo.height = (!stor.isAgrand) ? (coo.height - 3) : (coo.height - 3) / stor.aMul
          j3pEmpty(monDivCo)
          const aj = (stor.mesZone[i].zone.repAt.length > 0) ? stor.mesZone[i].zone.repAt[0].text : ''
          j3pAffiche(monDivCo, null, '$' + aj + '$')
          monDivCo.style.position = 'relative'
          ok = false
          stor.mesZone[i].corrige(false)
          stor.mesZone[i].lediv.addEventListener('mouseover', () => {
            monDivCo.style.display = ''
            stor.mesZone[i].eLemCoNtz.style.display = 'none'
          })
          stor.mesZone[i].lediv.addEventListener('mouseout', () => {
            monDivCo.style.display = 'none'
            stor.mesZone[i].eLemCoNtz.style.display = ''
          })
          monDivCo.style.display = 'none'
          stor.monDivCo.push(monDivCo)
        }
        stor.mesZone[i].disable()
        stor.mesZone[i].eLemCoNtz.style.backgroundColor = 'rgba(117, 190, 218, 0)'
        stor.mesZone[i].textaCont.style.backgroundColor = 'rgba(117, 190, 218, 0)'
      }
    }
    return ok
  }

  function okrep (num) {
    const acomp = stor.repEl[num]
    let ok = false
    for (let i = 0; i < stor.mesZone[num].zone.repAt.length; i++) {
      ok = ok || acomp.replace(/ /g, '') === stor.mesZone[num].zone.repAt[i].text.replace(/ /g, '')
    }
    return ok
  }

  switch (this.etat) {
    case 'enonce':
      // code exécuté au lancement de la section
      if (me.debutDeLaSection) {
        initSection()
      } else {
        enonceMain()
      }
      break // case "enonce":

    case 'correction':
      if (isRepOk()) me.score++
      me._stopTimer()
      me.cacheBoutonValider()
      me.etat = 'navigation'
      me.sectionCourante()
      stor.boutFauxOK.style.display = 'none'
      // Obligatoire
      me.finCorrection()
      break // case "correction":

    case 'navigation':

      if (me.sectionTerminee()) {
        // On détermine la phrase d’état renvoyée par la section
        me.afficheBoutonSectionSuivante()
        me.focus('sectioncontinuer')
      } else {
        me.etat = 'enonce'
        me.afficheBoutonSuite()
        me.focus('boutoncontinuer')
      }
      // Obligatoire
      me.finNavigation()
      break // case "navigation":
  }
}
