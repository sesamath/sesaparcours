import { j3pAddContent, j3pAddElt, j3pAjouteBouton, j3pClone, j3pDetruit, j3pEmpty, j3pGetNewId, j3pShowError } from 'src/legacy/core/functions'
import { faitAreaFacile, faitSon, importImage, j3pModale2, placeJouerSon } from 'src/legacy/outils/zoneStyleMathquill/functions'
import ZoneStyleMathquill3 from 'src/legacy/outils/zoneStyleMathquill/ZoneStyleMathquill3'
import loadMq from 'src/lib/mathquill/loadMathquill'
import { getMtgCore } from 'src/lib/outils/mathgraph'
import { j3pCreeSVG } from 'src/legacy/core/functionsSvg'
import ListeDeroulante from 'src/legacy/outils/listeDeroulante/ListeDeroulante'
import { addDefaultTable } from 'src/legacy/themes/table'
import { j3pAffiche } from 'src/lib/mathquill/functions'
import './resMake.css'
import rectangleImg from './rect.png'

const genMGEx = 'TWF0aEdyYXBoSmF2YTEuMAAAABM+TMzNAAJmcv###wEA#wEAAAAAAAAAAAM2AAACbQAAAQEAAAAAAAAAAAAAAEj#####AAAAAQAKQ0NhbGNDb25zdAD#####AAJwaQAWMy4xNDE1OTI2NTM1ODk3OTMyMzg0Nv####8AAAABAApDQ29uc3RhbnRlQAkh+1RELRj#####AAAAAQAKQ1BvaW50QmFzZQD#####AYAA#wAQAAFIAAAAAAAAAAAAQAgAAAAAAAAAAAkAAUCEGAAAAAAAQHvj1wo9cKQAAAACAP####8BgAD#ABAAAUMAAAAAAAAAAABACAAAAAAAAAAACQABQISwAAAAAABAYweuFHrhSP####8AAAABABRDRHJvaXRlRGlyZWN0aW9uRml4ZQD#####AYAA#wEQAAABAAACBAAAAAEBP#AAAAAAAAAAAAADAP####8BgAD#ARAAAAEAAAIEAAAAAgA#8AAAAAAAAP####8AAAABAA9DUG9pbnRMaWVEcm9pdGUA#####wGAAP8AEAABRQAAAAAAAAAAAEAIAAAAAAAAAAAJAAFAcUAAAAAAAAAAAAQAAAAEAP####8BgAD#ABAAAUYAAAAAAAAAAABACAAAAAAAAAAACQABwHYgAAAAAAAAAAAD#####wAAAAEACENTZWdtZW50AP####8AgAD#ABAAAAEAAAAEAAAAAgAAAAUAAAAFAP####8AgAD#ABAAAAEAAAAEAAAAAQAAAAYAAAAEAP####8A#wAAARAAAUcAAAAAAAAAAABACAAAAAAAAAAACAABP9GJZrBztMoAAAAIAAAABAD#####AP8AAAEQAAFEAAAAAAAAAAAAQAgAAAAAAAAAAAgAAT#mQshZCyFkAAAABwAAAAMA#####wH#AAABEAAAAQAGdmVydGljAAQAAAAJAD#wAAAAAAAAAAAAAwD#####Af8AAAEQAAABAAAABAAAAAoBP#AAAAAAAAD#####AAAAAQAQQ0ludERyb2l0ZURyb2l0ZQD#####AP8AAAAQAAAAAAAAAAAAAABACAAAAAAAAAADbGltCAAAAAALAAAADAAAAAQA#####wD#AAAAEAAAAAAAAAAAAAAAQAgAAAAAAAAAAAgAAcB8gAAAAAAAAAAACwAAAAQA#####wD#AAAAEAAAAAAAAAAAAAAAQAgAAAAAAAAAAAgAAcCFGAAAAAAAAAAADAAAAAUA#####wD#AAAAEAAAAQAAAAQAAAANAAAADgAAAAUA#####wD#AAAAEAAAAQAAAAQAAAANAAAADwAAAAIA#####wH#AAAAEAABQQAAAAAAAAAAAEAIAAAAAAAAAAAIAAFAgBgAAAAAAEBg3Cj1wo9cAAAAAgD#####Af8AAAAQAAFCAAAAAAAAAAAAQAgAAAAAAAAAAAgAAUCA+AAAAAAAQGDcKPXCj1z#####AAAAAQAJQ0xvbmd1ZXVyAP####8AAAASAAAAE#####8AAAABABRDSW1wbGVtZW50YXRpb25Qcm90bwD#####ABJNZXN1cmUgZGUgbG9uZ3VldXIAAAAFAAAAAgAAAAIAAAACAAAACv####8AAAABAAtDTWVkaWF0cmljZQAAAAAVAQAAAAAQAAABAAAAAQAAAAIAAAAK#####wAAAAEAB0NNaWxpZXUAAAAAFQEAAAAAEAAAAQAABQAAAAACAAAACv####8AAAACAAlDQ2VyY2xlT1IAAAAAFQEAAAAAAAABAAAAFwAAAAFAMAAAAAAAAAH#####AAAAAQAQQ0ludERyb2l0ZUNlcmNsZQAAAAAVAAAAFgAAABj#####AAAAAQAQQ1BvaW50TGllQmlwb2ludAAAAAAVAQAAAAAQAAABAAAFAAEAAAAZAAAABwEAAAAVAAAAAgAAAAr#####AAAAAQAPQ1ZhbGV1ckFmZmljaGVlAQAAABUB#wAAAQAAAAAAGhEAAAAAAAEAAAABAAAAAQAAAAAAAAAAAAAAAAEAAAAbAAAACAD#####ABJNZXN1cmUgZGUgbG9uZ3VldXIAAAAFAAAAAgAAAAIAAAACAAAABQAAAAkAAAAAHQEAAAAAEAAAAQAAAAEAAAACAAAABQAAAAoAAAAAHQEAAAAAEAAAAQAABQAAAAACAAAABQAAAAsAAAAAHQEAAAAAAAABAAAAHwAAAAFAMAAAAAAAAAEAAAAMAAAAAB0AAAAeAAAAIAAAAA0AAAAAHQEAAAAAEAAAAQAABQABAAAAIQAAAAcBAAAAHQAAAAIAAAAFAAAADgEAAAAdAf8AAAEAAAAAACIRAAAAAAABAAAAAQAAAAEAAAAAAAAAAAAAAAABAAAAIwAAAAgA#####wASTWVzdXJlIGRlIGxvbmd1ZXVyAAAABQAAAAIAAAACAAAABgAAAAkAAAAJAAAAACUBAAAAABAAAAEAAAABAAAABgAAAAkAAAAKAAAAACUBAAAAABAAAAEAAAUAAAAABgAAAAkAAAALAAAAACUBAAAAAAAAAQAAACcAAAABQDAAAAAAAAABAAAADAAAAAAlAAAAJgAAACgAAAANAAAAACUBAAAAABAAAAEAAAUAAQAAACkAAAAHAQAAACUAAAAGAAAACQAAAA4BAAAAJQH#AAABAAAAAAAqEQAAAAAAAQAAAAEAAAABAAAAAAAAAAAAAAAAAQAAACsAAAAIAP####8AEk1lc3VyZSBkZSBsb25ndWV1cgAAAAUAAAACAAAAAgAAAAYAAAABAAAACQAAAAAtAQAAAAAQAAABAAAAAQAAAAYAAAABAAAACgAAAAAtAQAAAAAQAAABAAAFAAAAAAYAAAABAAAACwAAAAAtAQAAAAAAAAEAAAAvAAAAAUAwAAAAAAAAAQAAAAwAAAAALQAAAC4AAAAwAAAADQAAAAAtAQAAAAAQAAABAAAFAAEAAAAxAAAABwEAAAAtAAAABgAAAAEAAAAOAQAAAC0B#wAAAQAAAAAAMhEAAAAAAAEAAAABAAAAAQAAAAAAAAAAAAAAAAEAAAAz#####wAAAAEAB0NDYWxjdWwA#####wABQQAFQ0QvQ0X#####AAAAAQAKQ09wZXJhdGlvbgP#####AAAAAQAPQ1Jlc3VsdGF0VmFsZXVyAAAAGwAAABEAAAAjAAAADwD#####AAFCAAVGRy9GSAAAABADAAAAEQAAACsAAAARAAAAMwAAAAIA#####wEAAAAAEAAAAAAAAAAAAAAAQAgAAAAAAAAAAAgAAUBowAAAAAAAQINp64UeuFIAAAADAP####8BAAAAARAAAAEAAAABAAAANwE#8AAAAAAAAAAAAAYA#####wEAAAAAEAAAAAAAAAAAAAAAQAgAAAAAAAAAAAgAAAAACwAAADgAAAAEAP####8BAAAAABAAAAAAAAAAAAAAAEAIAAAAAAAAAAAIAAFAg+AAAAAAAAAAADgAAAADAP####8BAAAAARAAAAEAAAABAAAADgE#8AAAAAAAAAAAAAMA#####wEAAAABEAAAAQAAAAEAAAA6AD#wAAAAAAAAAAAABgD#####AQAAAAAQAAAAAAAAAAAAAABACAAAAAAAAAAACAAAAAA7AAAAPP####8AAAABAAlDUG9seWdvbmUA#####wEAAAAAAAABAAAABQAAAD0AAAA6AAAAOQAAAA4AAAA9#####wAAAAEAEENTdXJmYWNlUG9seWdvbmUA#####wD###8AAAAAAAQAAAA+AAAAAwD#####Af###wEQAAABAAAAAQAAAA8AP#AAAAAAAAAAAAAGAP####8A####ABAAAAAAAAAAAAAAAEAIAAAAAAAAAAAIAAAAADgAAABAAAAAEgD#####Af###wAAAAEAAAAFAAAADwAAAEEAAAA5AAAADQAAAA8AAAATAP####8A####AAAAAAAEAAAAQv####8AAAABAA5DT2JqZXREdXBsaXF1ZQD#####AIAA#wAAAAAACAAAABQA#####wCAAP8AAAAAAAcAAAAUAP####8A#wAAAAAAAAAKAAAAFAD#####AP8AAAAAAAAACQAAABT##########w=='
const genIm = 'data:image/png;base64,iVBORw0KGgoAAAANSUhEUgAAAFwAAABKCAYAAAA2YDPeAAAAAXNSR0IArs4c6QAAAARnQU1BAACxjwv8YQUAAAAJcEhZcwAADsMAAA7DAcdvqGQAAAKmSURBVHhe7ZTBkuIwDAXnz/l0tnTwzFvTJHYiy0riruoTIEt94Oe9CGUFD2YFD2YFD2YFD2YFD+Yj+Ov1+nXhz3/BNfaKPobd4MWFD83BzcV5Nv/DycUnPX0+ghs6gFz80dsHgxdomPpkqEdxi83gBg1Unwh1ULfYDW7QUPVJ0P3qHk3BDRquPgG6W22hOXiBHlLvCN2p9tAd3KBH1TtB96m9HApu0OPqHaC71CMcDm7QEupVoVvUM5wKXqCl1CtB+6tncQlu0HLqFaC9VQ/cghu0pJoV2rXWC9fgBi2rZoN2VL1xD16g5dUM0F7qCIYFN+gIdRa0S+0ohgY36Bg1GtpBHc3w4AYdVhsBvatGEBK8QEeqo6C3aqMIDW7Qsao39IYaTXhwgw5XvaDZ6gymBC9QBPUoNKt2FlODGxRD7YVmqLOZHtygMGor9Fs1AymCGxRI3YN+o2YhTfACxVJr6DtqNtIFNyicWqDP1IykDG5QwB6zkja4QSH3zE7q4AUKS16BSwQ3KLB6FdIHp7jfvALrPzyYtMEpZo9ZSRec4tUW6DM1I6mCUzSVoO+p2UgTnGKpW9D3a7MwPTjFqW2FfqtmYGpwiqIegeaos5kWnGKoZ6B56kzCg1OAWi9otjqD0OB0tDoCekeNJiw4HauOhN5TIwkJTkeqEdC7ahRDg9Nh6gxoD3U0w4LTMepMaB91JEOC0xFqBmgvdRSuwWlxNRu0ozoCt+C0sJoZ2lf1xCU4LaleAdpb9eJUcFqs9krQ/qoHh4PTQupVoVvUsxwKTouod4DuUo/SFZwerr0TdJ96hObg9KB6V+hWtZem4PSQenfo5mIvm8Hpgdon4XH/1+A0XH0qZxtgcB1KLo7THXxxjubgCx+a/sMXfnwNvhjDCh7K+/0PVwF0/feJqwcAAAAASUVORK5CYII='
const listColor = ['#FF0', '#2d5d00', '#00F', '#fd4200', '#f00', '#000']
const larestr = 'abcdefghijklmnopqrstuvwxyzABCDEFGHIJKLMNOPQRSTUVWXYZ0123456789.,*-+()@[]àéèù/%'

export default function editorProgramme (container, values) {
  console.debug('editorProgramme avec les valeurs initiales', values)
  const editor = {}
  const valuesDeb = values
  function creeLesDiv () {
    editor.conteneur = container
    editor.divUp = j3pAddElt(editor.conteneur, 'div')
    editor.divExo = j3pAddElt(editor.conteneur, 'div')
    editor.figDep = j3pAddElt(editor.conteneur, 'div')
    editor.divUp.style.minWidth = '900px'
    editor.divUp.style.border = '5px solid black'
    editor.divUp.style.borderRadius = '10px'
    editor.divUp.style.padding = '10px'
    editor.divUp.style.background = '#47ffff'
    editor.figDep.style.border = '5px solid black'
    editor.figDep.style.borderRadius = '10px'
    editor.figDep.style.background = '#ffFFFF'
    editor.figDep.classList.add('pourFitContent')
    editor.figDep.style.minWidth = '1000px'
    editor.divExo.style.border = '5px solid black'
    editor.divExo.style.background = '#ccccff'
    editor.divExo.style.borderRadius = '10px'

    editor.div7 = j3pAddElt(container, 'div', '', {
      style: {
        background: '#e54949',
        border: '7px solid black',
        borderRadius: '10px',
        padding: '10px'
      }
    })
    j3pAddContent(editor.div7, '\n')
    editor.div7.style.fontSize = '40px'
    editor.div7.style.verticalAlign = 'middle'
    editor.div7.style.textAlign = 'center'
    editor.div7.style.color = 'white'
    j3pAddContent(editor.div7, 'Ne pas cliquer sur le bouton enregistrer ci dessous, <br> (mais sur le bouton valider plus haut)')
    j3pAddContent(editor.div7, '\n')
    j3pAddContent(editor.div7, '\n')
    editor.div7.style.display = 'none'
  }
  function menuTotal () {
    const yuyu = j3pAddElt(editor.divUp, 'span')
    j3pAddContent(yuyu, '<b><u>Titre de la ressource:</u></b>&nbsp;')
    editor.inputTitre = j3pAddElt(yuyu, 'input', '', {
      value: valuesDeb.Liste.titre,
      size: 50
    })
    j3pAddContent(editor.divUp, '\n')
    j3pAddContent(editor.divUp, '\n')

    const yuyu7 = j3pAddElt(editor.divUp, 'span')
    j3pAddContent(yuyu7, '<b><u>Exercice chronométré:</u></b>&nbsp;')
    const ldOptions2 = { dansModale: true, speMod: 10, speMod2: 70, j3pCont: container, choix0: true, onChange: faisLimite }
    const ll2 = (valuesDeb.limite === 0 || valuesDeb.limite === '') ? ['Non', 'Oui'] : ['Oui', 'Non']
    editor.listeparamLimite = ListeDeroulante.create(editor.divUp, ll2, ldOptions2)
    editor.chronoCahc2 = j3pAddElt(editor.divUp, 'span')
    j3pAddContent(editor.chronoCahc2, '&nbsp;Temps (en secondes):&nbsp;')
    editor.inputLimite = j3pAddElt(editor.chronoCahc2, 'input', '', {
      type: 'number',
      value: (valuesDeb.limite !== '' && valuesDeb.limite !== 0) ? valuesDeb.limite : 20,
      size: 3,
      min: '1',
      max: '999'
    })
    editor.inputLimite.addEventListener('change', () => {
      const averif = editor.inputLimite.value
      if (averif === '' || isNaN(averif) || (Number(averif) < 0) || (Number(averif) > 1000)) editor.inputLimite.value = '20'
    })
    faisLimite()
    j3pAddContent(editor.divUp, '\n')
    j3pAddContent(editor.divUp, '\n')

    const yu2 = j3pAddElt(editor.divUp, 'span')
    j3pAddContent(yu2, '<b><u>Nombre d’exercices proposées</u></b>:&nbsp;')
    editor.nbrepetitions = j3pAddElt(editor.divUp, 'input', '', {
      type: 'number',
      value: valuesDeb.nbrepetitions,
      size: 4,
      min: '1',
      max: '10'
    })
    editor.nbrepetitions.addEventListener('change', () => {
      const averif = editor.nbrepetitions.value
      if (averif === '' || isNaN(averif) || (Number(averif) < 1) || (Number(averif) > 10)) editor.nbrepetitions.value = '1'
    })
    editor.nbrepetitions.addEventListener('input', () => {
      const averif = editor.nbrepetitions.value
      if (averif === '' || isNaN(averif) || (Number(averif) < 1) || (Number(averif) > 10)) editor.nbrepetitions.value = '1'
    })
    j3pAddContent(editor.divUp, '\n')
    j3pAddContent(editor.divUp, '\n')

    const yu9921 = j3pAddElt(editor.divUp, 'span')
    j3pAddContent(yu9921, '<b><u>Exercices ordonnés:</u></b>&nbsp;')
    const ll9921 = (valuesDeb.ordonne === 'Oui') ? ['Oui', 'Non'] : ['Non', 'Oui']
    editor.ordonneL = ListeDeroulante.create(yu9921, ll9921, ldOptions2)
    j3pAddContent(editor.divUp, '\n')
    j3pAddContent(editor.divUp, '\n')

    const yu991 = j3pAddElt(editor.divUp, 'span')
    j3pAddContent(yu991, '<b><u>Theme:</u></b>&nbsp;')
    const ll991 = (valuesDeb.theme === 'standard') ? ['standard', 'zonesAvecImageDeFond'] : ['zonesAvecImageDeFond', 'standard']
    editor.themeL = ListeDeroulante.create(yu991, ll991, ldOptions2)
    j3pAddContent(editor.divUp, '\n')
    j3pAddContent(editor.divUp, '\n')
  }

  function menuExo () {
    if (editor.maZASouv) {
      for (let i = 0; i < editor.maZASouv.length; i++) {
        if (editor.maZASouv[i]) {
          editor.maZASouv[i].disable()
        }
      }
    }
    editor.maZASouv = []
    editor.okVire = false
    editor.figDep.style.display = 'none'
    editor.yaBlok = false
    j3pEmpty(editor.figDep)
    j3pEmpty(editor.divExo)
    const tab3 = addDefaultTable(editor.divExo, 3, 3)
    j3pAddContent(tab3[0][0], '&nbsp;')
    j3pAddContent(tab3[0][2], '&nbsp;')
    const tab4 = addDefaultTable(tab3[0][1], 1, 7)
    j3pAddContent(tab4[0][0], '<b>Liste des exercices disponibles</b>')
    tab3[0][0].style.textAlign = 'center'
    j3pAddContent(tab4[0][1], '&nbsp;&nbsp;')
    j3pAddContent(tab4[0][3], '&nbsp;&nbsp;')
    j3pAddContent(tab4[0][5], '&nbsp;&nbsp;')
    editor.divExoliste = tab3[1][1]
    j3pAjouteBouton(tab4[0][2], importExo, { value: 'Dupliquer l’ensemble des exercices' })

    const tab = addDefaultTable(editor.divExoliste, editor.ProgrammeS.length + 2, 11)
    for (let i = 0; i < editor.ProgrammeS.length; i++) {
      tab[i + 1][0].style.borderBottom = '1px solid black'
      tab[i + 1][0].style.borderTop = '1px solid black'
      tab[i + 1][0].style.borderLeft = '1px solid black'
      tab[i + 1][1].style.borderBottom = '1px solid black'
      tab[i + 1][1].style.borderTop = '1px solid black'
      tab[i + 1][3].style.borderBottom = '1px solid black'
      tab[i + 1][3].style.borderTop = '1px solid black'
      tab[i + 1][2].style.borderBottom = '1px solid black'
      tab[i + 1][2].style.borderTop = '1px solid black'
      tab[i + 1][4].style.borderBottom = '1px solid black'
      tab[i + 1][4].style.borderTop = '1px solid black'
      tab[i + 1][5].style.borderBottom = '1px solid black'
      tab[i + 1][5].style.borderTop = '1px solid black'
      tab[i + 1][6].style.borderBottom = '1px solid black'
      tab[i + 1][6].style.borderTop = '1px solid black'
      tab[i + 1][6].style.borderRight = '1px solid black'
      tab[i + 1][6].style.padding = '5px 5px 5px 5px'
      tab[i + 1][2].style.padding = '5px 5px 5px 5px'
      tab[i + 1][4].style.padding = '5px 5px 5px 5px'
      j3pAddContent(tab[i + 1][1], '&nbsp;')
      j3pAddContent(tab[i + 1][3], '&nbsp;')
      j3pAddContent(tab[i + 1][5], '&nbsp;')
      j3pAddContent(tab[i + 1][7], '&nbsp;')
      j3pAddContent(tab[i + 1][9], '&nbsp;')
      const bufadd = (editor.ProgrammeS[i].nom) ? ' (' + editor.ProgrammeS[i].nom + ')' : ''
      j3pAffiche(tab[i + 1][0], null, '&nbsp;Exercice ' + (i + 1) + bufadd + '&nbsp;')
      j3pAjouteBouton(tab[i + 1][2], () => { modifExo(i) }, { value: 'Modifier' })
      j3pAjouteBouton(tab[i + 1][4], () => { supExo(i) }, { value: 'Supprimer' })
      j3pAjouteBouton(tab[i + 1][6], () => { DupExo(i) }, { value: 'Dupliquer' })
      if (i !== 0) {
        j3pAjouteBouton(tab[i + 1][8], () => { makeUp(i) }, { value: '↑' })
      } else {
        j3pAddContent(tab[i + 1][8], '&nbsp;&nbsp;&nbsp;')
      }
      if (i !== editor.ProgrammeS.length - 1) j3pAjouteBouton(tab[i + 1][10], () => { MakeDown(i) }, { value: '↓' })
    }
    j3pAddContent(tab[tab.length - 2][0], '&nbsp;')
    j3pAjouteBouton(tab[tab.length - 1][0], ajouteExo, { value: '+' })
  }
  function makeUp (i) {
    const acons = editor.ProgrammeS[i]
    editor.ProgrammeS.splice(i, 1)
    editor.ProgrammeS.splice(i - 1, 0, acons)
    menuExo()
  }
  function MakeDown (i) {
    const acons = editor.ProgrammeS[i]
    editor.ProgrammeS.splice(i, 1)
    editor.ProgrammeS.splice(i + 1, 0, acons)
    menuExo()
  }
  function importExo () {
    const aaj = j3pClone(editor.ProgrammeS)
    const nomsPris = []
    for (let i = 0; i < aaj.length; i++) {
      nomsPris.push(aaj[i].nom)
      // je sais Daniel y’a un truc pour faire ca mais je m’en rappelle plus
    }
    for (let i = 0; i < aaj.length; i++) {
      do {
        aaj[i].nom += '2'
      } while (nomsPris.indexOf(aaj[i].nom) !== -1)
      editor.ProgrammeS.push(j3pClone(aaj[i]))
      nomsPris.push(aaj[i].nom)
    }
    menuExo()
  }
  function DupExo (num) {
    const aaj = j3pClone(editor.ProgrammeS[num])
    const nomsPris = []
    for (let i = 0; i < editor.ProgrammeS.length; i++) {
      nomsPris.push(editor.ProgrammeS[i].nom)
      // je sais Daniel y’a un truc pour faire ca mais je m’en rappelle plus
    }
    do {
      aaj.nom += '2'
    } while (nomsPris.indexOf(aaj.nom) !== -1)
    editor.ProgrammeS.push(j3pClone(aaj))
    menuExo()
  }

  function ajouteExo () {
    editor.ProgrammeS.push(getDefaultProgrammeElement())
    menuExo()
  }
  function faisId (s) {
    if (typeof s === 'string') return s
    return s.content.txtFigure
  }
  function getDefaultProgrammeElement () {
    return {
      consigneG: '',
      repPrev: [],
      yaSon: false,
      yaImage: false,
      image: genIm,
      txtFigure: genMGEx,
      nom: 'nouvel exercice',
      survol: true,
      fautTout: false,
      listZone: []
    }
  }

  function modifExo (i) {
    editor.listAffiche = []
    editor.yaBlok = true
    editor.objAvire = []
    editor.okVire = false
    editor.figDep.style.display = ''
    j3pEmpty(editor.figDep)
    editor.exoEncours = j3pClone(editor.ProgrammeS[i])
    j3pEmpty(editor.divExo)
    const tab = addDefaultTable(editor.divExo, 5, 3)
    j3pAddContent(tab[0][0], '&nbsp;')
    j3pAddContent(tab[2][2], '&nbsp;')
    j3pAddContent(tab[4][0], '&nbsp;')
    const tadd = addDefaultTable(tab[1][1], 1, 7)
    editor.cellModifExo = tadd[0][0]
    j3pAddContent(tadd[0][1], '&nbsp;&nbsp;&nbsp;&nbsp;')
    j3pAddContent(tadd[0][2], '&nbsp;nom:&nbsp;')
    j3pAddContent(tadd[0][4], '&nbsp;&nbsp;&nbsp;&nbsp;')
    editor.inputNouveauNom = j3pAddElt(tadd[0][3], 'input', '', { value: editor.exoEncours.nom, size: 15 })
    editor.numExo = i + 1
    editor.inputNouveauNom.addEventListener('change', chgNom)
    chgNom()
    const tabou = addDefaultTable(tab[3][1], 1, 3)
    j3pAddContent(tabou[0][1], '&nbsp;&nbsp;&nbsp;&nbsp;')
    editor.btnValiderNom = j3pAjouteBouton(tabou[0][0], () => { valModifExo(i) }, { value: 'Valider' })
    j3pAjouteBouton(tabou[0][2], menuExo, { value: 'Annuler' })
    editor.exoEncoursi = i

    j3pEmpty(editor.figDep)
    const divEn = j3pAddElt(editor.figDep, 'div')
    divEn.style.padding = '10px'
    divEn.style.borderRadius = '10px'
    divEn.style.backgroundImage = 'linear-gradient(to left, rgba(245, 169, 107, 0.8) 30%, rgba(245, 220, 187, 0.4))'
    faitAreaFacile(divEn, editor.exoEncours, 'consigneG', container)
    afficheExplik(divEn, 'La consigne s’affiche tout en haut de l’exercice.')

    /// ///
    const yuyu712 = j3pAddElt(divEn, 'span')
    editor.achcaSon = j3pAddElt(divEn, 'div')
    const tabvson = addDefaultTable(yuyu712, 1, 2)
    j3pAddContent(tabvson[0][0], '<b><u>Fichier son l’énoncé:</u></b>&nbsp;')
    const ldOptions34 = { dansModale: true, speMod: 10, speMod2: 70, j3pCont: container, choix0: true, onChange: changeSon }
    const ll34 = (!editor.exoEncours.yaSon) ? ['Non', 'Oui'] : ['Oui', 'Non']
    editor.yaFigureLSon = ListeDeroulante.create(tabvson[0][1], ll34, ldOptions34)
    editor.Cahc2yaIMageSon = addDefaultTable(editor.achcaSon, 2, 1)
    editor.whereFigSon = editor.Cahc2yaIMageSon[0][0]
    faitSon(editor.whereFigSon, editor.exoEncours, editor, 'son')
    changeSon()
    afficheExplik(divEn, 'Le fichier son doit rester disponible en ligne', true)
    /// ///

    const divTrav = j3pAddElt(editor.figDep, 'div')
    divTrav.style.padding = '10px'
    divTrav.style.borderRadius = '10px'
    divTrav.style.backgroundImage = 'linear-gradient(to right, rgba(187, 187, 187, 0.8) 30%, rgba(224, 224, 224, 0.5))'
    divTrav.style.borderTop = '1px dashed black'

    /// ///
    const yuyu71 = j3pAddElt(divTrav, 'span')
    editor.achcaIm = j3pAddElt(divTrav, 'div')
    const tabvim = addDefaultTable(yuyu71, 1, 2)
    j3pAddContent(tabvim[0][0], '<b><u>Image de fond:</u></b>&nbsp;')
    editor.Cahc2yaIMageL = addDefaultTable(editor.achcaIm, 2, 1)
    editor.whereFigIm = editor.Cahc2yaIMageL[0][0]
    faitIm()
    afficheExplik(divTrav, 'L’image doit rester disponible en ligne', false)
    /// ///

    initTag()
    const yuyu7 = j3pAddElt(divTrav, 'div')
    j3pAddContent(yuyu7, '<b><u>Zones et taille:</u></b><br>')
    const tbBoutt = addDefaultTable(yuyu7, 1, 5)
    const boutonLabel = j3pAddElt(tbBoutt[0][0], 'button')
    boutonLabel.addEventListener('mousedown', (event) => { ajZoneLabel(event) })
    j3pAddContent(boutonLabel, '&nbsp;<b>Label</b>&nbsp;')
    j3pAddContent(tbBoutt[0][1], '&nbsp;&nbsp;&nbsp;&nbsp;')

    const boutonCirc = j3pAddElt(tbBoutt[0][2], 'button')
    boutonCirc.addEventListener('mousedown', (event) => { ajZone(event) })
    const tt = addDefaultTable(boutonCirc, 1, 2)
    j3pAddContent(tt[0][0], '&nbsp;<b>Cache</b>&nbsp;')

    j3pAddContent(tbBoutt[0][3], '&nbsp;&nbsp;&nbsp;&nbsp;')
    const boutonR = j3pAddElt(tbBoutt[0][4], 'button')
    boutonR.addEventListener('mousedown', ajZonepr)
    const ttR = addDefaultTable(boutonR, 1, 2)
    j3pAddContent(ttR[0][0], '&nbsp;<b>Zone de texte</b>&nbsp;')
    const immmR = j3pAddElt(ttR[0][1], 'img', '', { src: rectangleImg, width: 30, height: 30 })
    immmR.style.width = '30px'
    immmR.style.height = '30px'

    tbBoutt[0][0].style.verticalAlign = 'middle'
    tbBoutt[0][2].style.verticalAlign = 'middle'
    tbBoutt[0][4].style.verticalAlign = 'middle'

    editor.spanConst = j3pAddElt(divTrav, 'span')
    editor.spanConst.style.color = '#0844c4'
    j3pAddContent(editor.spanConst, '&nbsp;')
    const Cahc2yaFigureL = addDefaultTable(divTrav, 1, 1)[0][0]
    editor.whereFig = j3pAddElt(Cahc2yaFigureL, 'div')
    faitMG()
    afficheExplik(divTrav, 'Placer sur cette figure les différentes zones.<br>Leur paramètres sont à compléter ci-dessous.', false)

    const divListeZone = j3pAddElt(divTrav, 'div')
    const tabContZone = addDefaultTable(divListeZone, 2, 1)
    editor.zoneList = tabContZone[1][0]
    j3pAddContent(tabContZone[0][0], '<b><u>Zones</u></b>')
    tabContZone[0][0].style.paddingBottom = '10px'
    affListeZone()
    afficheExplik(divTrav, 'Utiliser le bouton <b>[Suppr]</b> pour supprimer la zone.<br><br> Utiliser le bouton <b>[Fixer]</b> ou <b>[Débloquer]</b> pour masquer/afficher les petits points qui permettent de déplacer/déformer la zone.<br><br> Utiliser le bouton <b>[Bordure]</b> ou <b>[Sans bordure]</b> pour que la zone de saisie soit encadrée.<br><i>( à éviter si on est dans un tableau )</i><br><br>')

    const divTest = j3pAddElt(editor.figDep, 'div')
    divTest.style.padding = '10px'
    divTest.style.borderTop = '1px dashed black'
    divTest.style.borderRadius = '10px'
    divTest.style.background = '#6e6666'
    j3pAjouteBouton(divTest, testExo, { value: 'Tester cet exercice' })
    afficheExplik(divTest, 'Permet de prévisualiser l’exercice', true)
    editor.divTest = addDefaultTable(divTest, 1, 1)[0][0]
  }
  function initTag () {
    editor.nomPos = []
    editor.listTag = []
    for (let i = 0; i < editor.exoEncours.listZone.length; i++) {
      const el = editor.exoEncours.listZone[i]
      editor.nomPos.push(el.nom)
      editor.listTag.push(el.p1)
      if (el.type !== 'label') {
        editor.listTag.push(el.p2, el.p3, el.p4, el.d1, el.d2, el.d3, el.d4, el.poly, el.texte, el.surface[0], el.m1)
      }
    }
  }

  function ajZone (event) {
    if (editor.okVire) vireConstEncours()
    editor.okVire = true
    event.stopPropagation()
    event.preventDefault()
    editor.mtgAppLecteur.getDoc(editor.idSvg).defaultCursor = 'crosshair'
    j3pEmpty(editor.spanConst)
    j3pAddContent(editor.spanConst, 'Cliquer sur la figure pour placer un sommet de la zone')
    editor.funcClick = (x, y) => {
      editor.mtgAppLecteur.setApiDoc(editor.idSvg)
      const p1 = newTag()
      const p2 = newTag()
      editor.mtgAppLecteur.addFreePoint({
        absCoord: true,
        x,
        y,
        name: p1,
        tag: p1,
        pointStyle: 'biground',
        hiddenName: true,
        color: listColor[editor.exoEncours.listZone.length % listColor.length]
      })
      editor.mtgAppLecteur.addFreePoint({
        absCoord: true,
        x: x + 1,
        y: y + 1,
        name: p2,
        tag: p2,
        pointStyle: 'biground',
        hiddenName: true,
        color: listColor[editor.exoEncours.listZone.length % listColor.length]
      })
      const d1 = newTag()
      const d2 = newTag()
      const d3 = newTag()
      const d4 = newTag()
      const p3 = newTag()
      const m1 = newTag()
      const p4 = newTag()
      const poly = newTag()
      const surface = newTag()
      editor.mtgAppLecteur.addLinePerp({
        absCoord: true,
        a: p1,
        x,
        y,
        d: 'vertic',
        name: d1,
        tag: d1,
        pointStyle: 'bigmult',
        hiddenName: true,
        hidden: true
      })
      editor.mtgAppLecteur.addLinePerp({
        absCoord: true,
        a: p1,
        x,
        y,
        d: d1,
        name: d2,
        tag: d2,
        pointStyle: 'bigmult',
        hiddenName: true,
        hidden: true
      })
      editor.mtgAppLecteur.addLinePerp({
        absCoord: true,
        a: p2,
        x,
        y,
        d: 'vertic',
        name: d3,
        tag: d3,
        pointStyle: 'bigmult',
        hiddenName: true,
        hidden: true
      })
      editor.mtgAppLecteur.addLinePerp({
        absCoord: true,
        a: p2,
        x,
        y,
        d: d1,
        name: d4,
        tag: d4,
        pointStyle: 'bigmult',
        hiddenName: true,
        hidden: true
      })
      editor.mtgAppLecteur.addIntLineLine({
        d: d2,
        d2: d3,
        name: p3,
        tag: p3,
        hidden: true
      })
      editor.mtgAppLecteur.addIntLineLine({
        d: d1,
        d2: d4,
        name: p4,
        tag: p4,
        hidden: true
      })
      editor.mtgAppLecteur.addMidpoint({
        a: p1,
        b: p2,
        name: m1,
        tag: m1,
        hidden: true
      })
      editor.mtgAppLecteur.addPolygon({
        points: [p1, p3, p2, p4],
        tag: poly,
        hidden: true,
        opacity: 1
      })
      editor.mtgAppLecteur.addSurfacePoly({
        poly,
        tag: surface,
        color: 'white',
        thickness: 3,
        fillStyle: 'fill',
        opacity: 1
      })
      editor.objAvire.push(p1, p2)
      j3pEmpty(editor.spanConst)
      j3pAddContent(editor.spanConst, 'Cliquer sur la figure pour placer le sommet opposé de la zone')
      editor.funcMov = (x, y) => {
        editor.mtgAppLecteur.setPointPosition(editor.idSvg, '#' + p2, x, y, true)
      }
      editor.funcClick = (x, y) => {
        editor.mtgAppLecteur.getDoc(editor.idSvg).defaultCursor = undefined
        editor.okVire = false
        editor.objAvire = []
        editor.funcMov = () => {}
        editor.funcClick = () => {}
        const texte = newTag()
        const nom = newNom()
        editor.mtgAppLecteur.addLinkedText({
          a: m1,
          text: nom.replace('Zone', 'Cache'),
          tag: texte,
          color: listColor[editor.exoEncours.listZone.length % listColor.length],
          thickness: 3,
          vAlign: 'middle',
          hAlign: 'center'
        })
        editor.exoEncours.listZone.push({
          color: listColor[editor.exoEncours.listZone.length % listColor.length],
          type: 'cache',
          nom,
          p1,
          p2,
          p3,
          p4,
          d1,
          d2,
          d3,
          d4,
          m1,
          poly,
          surface: [surface],
          texte
        })
        affListeZone()
      }
    }
  }
  function ajZoneLabel (event) {
    if (editor.okVire) vireConstEncours()
    editor.okVire = true
    event.stopPropagation()
    event.preventDefault()
    editor.mtgAppLecteur.getDoc(editor.idSvg).defaultCursor = 'crosshair'
    j3pEmpty(editor.spanConst)
    j3pAddContent(editor.spanConst, 'Cliquer sur la figure pour placer le point de départ')
    editor.funcClick = (x, y) => {
      j3pEmpty(editor.spanConst)
      j3pAddContent(editor.spanConst, '&nbsp;')
      editor.funcClick = () => {}
      editor.mtgAppLecteur.getDoc(editor.idSvg).defaultCursor = undefined
      editor.mtgAppLecteur.setApiDoc(editor.idSvg)
      const p1 = newTag()
      editor.okVire = false
      editor.mtgAppLecteur.addFreePoint({
        absCoord: true,
        x,
        y,
        name: p1,
        tag: p1,
        pointStyle: 'biground',
        hiddenName: true,
        color: listColor[editor.exoEncours.listZone.length % listColor.length]
      })
      editor.funcMov = () => {}
      const nom = newNom()
      editor.exoEncours.listZone.push({
        color: listColor[editor.exoEncours.listZone.length % listColor.length],
        type: 'label',
        nom,
        p1,
        texte: '',
        taille: 15
      })
      affListeZone()
    }
  }
  function newNom () {
    let i = 0
    do {
      i++
    } while (editor.nomPos.indexOf('Zone ' + i) !== -1)
    editor.nomPos.push('Zone ' + i)
    return 'Zone ' + i
  }
  function newTag () {
    let j = 0
    let ntag
    do {
      ntag = 't' + j
      j++
    } while (editor.listTag.includes(ntag))
    editor.listTag.push(ntag)
    return ntag
  }
  function ajZonepr (event) {
    if (editor.okVire) vireConstEncours()
    editor.okVire = true
    event.stopPropagation()
    event.preventDefault()
    editor.mtgAppLecteur.getDoc(editor.idSvg).defaultCursor = 'crosshair'
    j3pEmpty(editor.spanConst)
    j3pAddContent(editor.spanConst, 'Cliquer sur la figure pour placer un sommet de la zone')
    editor.funcClick = (x, y) => {
      editor.mtgAppLecteur.setApiDoc(editor.idSvg)
      const p1 = newTag()
      const p2 = newTag()
      editor.mtgAppLecteur.addFreePoint({
        absCoord: true,
        x,
        y,
        name: p1,
        tag: p1,
        pointStyle: 'biground',
        hiddenName: true,
        color: listColor[editor.exoEncours.listZone.length % listColor.length]
      })
      editor.mtgAppLecteur.addFreePoint({
        absCoord: true,
        x: x + 1,
        y: y + 1,
        name: p2,
        tag: p2,
        pointStyle: 'biground',
        hiddenName: true,
        color: listColor[editor.exoEncours.listZone.length % listColor.length]
      })
      const d1 = newTag()
      const d2 = newTag()
      const d3 = newTag()
      const d4 = newTag()
      const p3 = newTag()
      const m1 = newTag()
      const p4 = newTag()
      const poly = newTag()
      const texte = newTag()
      const surface = newTag()
      const nom = newNom()
      editor.mtgAppLecteur.addLinePerp({
        absCoord: true,
        a: p1,
        x,
        y,
        d: 'vertic',
        name: d1,
        tag: d1,
        pointStyle: 'bigmult',
        hiddenName: true,
        hidden: true
      })
      editor.mtgAppLecteur.addLinePerp({
        absCoord: true,
        a: p1,
        x,
        y,
        d: d1,
        name: d2,
        tag: d2,
        pointStyle: 'bigmult',
        hiddenName: true,
        hidden: true
      })
      editor.mtgAppLecteur.addLinePerp({
        absCoord: true,
        a: p2,
        x,
        y,
        d: 'vertic',
        name: d3,
        tag: d3,
        pointStyle: 'bigmult',
        hiddenName: true,
        hidden: true
      })
      editor.mtgAppLecteur.addLinePerp({
        absCoord: true,
        a: p2,
        x,
        y,
        d: d1,
        name: d4,
        tag: d4,
        pointStyle: 'bigmult',
        hiddenName: true,
        hidden: true
      })
      editor.mtgAppLecteur.addIntLineLine({
        d: d2,
        d2: d3,
        name: p3,
        tag: p3,
        hidden: true
      })
      editor.mtgAppLecteur.addIntLineLine({
        d: d1,
        d2: d4,
        name: p4,
        tag: p4,
        hidden: true
      })
      editor.mtgAppLecteur.addMidpoint({
        a: p1,
        b: p2,
        name: m1,
        tag: m1,
        hidden: true
      })
      editor.mtgAppLecteur.addPolygon({
        points: [p1, p3, p2, p4],
        tag: poly,
        hidden: true,
        opacity: 1
      })
      editor.mtgAppLecteur.addSurfacePoly({
        poly,
        tag: surface,
        color: listColor[editor.exoEncours.listZone.length % listColor.length],
        thickness: 3,
        fillStyle: 'transp',
        opacity: 0.2
      })
      editor.objAvire.push(p1, p2)
      j3pEmpty(editor.spanConst)
      j3pAddContent(editor.spanConst, 'Cliquer sur la figure pour placer le sommet opposé de la zone')
      editor.funcMov = (x, y) => {
        editor.mtgAppLecteur.setPointPosition(editor.idSvg, '#' + p2, x, y, true)
      }
      editor.funcClick = (x, y) => {
        editor.mtgAppLecteur.getDoc(editor.idSvg).defaultCursor = undefined
        editor.okVire = false
        editor.objAvire = []
        editor.funcMov = () => {}
        editor.funcClick = () => {}
        editor.mtgAppLecteur.addLinkedText({
          a: m1,
          text: nom,
          tag: texte,
          thickness: 3,
          vAlign: 'middle',
          hAlign: 'center'
        })
        editor.exoEncours.listZone.push({
          color: listColor[editor.exoEncours.listZone.length % listColor.length],
          type: 'rectangle',
          nom,
          p1,
          p2,
          p3,
          p4,
          d1,
          d2,
          d3,
          d4,
          m1,
          poly,
          surface: [surface],
          texte,
          bon: false,
          repAt: []
        })
        affListeZone()
      }
    }
  }

  function changeSon () {
    editor.exoEncours.yaSon = editor.yaFigureLSon.reponse === 'Oui'
    editor.achcaSon.style.display = (editor.exoEncours.yaSon) ? '' : 'none'
  }
  function afficheExplik (ou, txt, bool) {
    const tomuch = addDefaultTable(ou, 1, 2)
    j3pAjouteBouton(tomuch[0][0], () => { faiv(tomuch[0][1]) }, { value: ' ? ' })
    tomuch[0][1].style.color = '#850593'
    tomuch[0][1].style.background = '#b0ee7f'
    tomuch[0][1].style.padding = '5px'
    tomuch[0][1].style.border = '1px solid black'
    j3pAddContent(tomuch[0][1], txt)
    tomuch[0][1].style.display = 'none'
    j3pAddContent(ou, '\n')
    const liligne = j3pAddElt(ou, 'div')
    if (!bool) {
      liligne.style.border = '1px dashed black'
      j3pAddContent(ou, '\n')
    }
  }
  function faiv (el) {
    el.style.display = (el.style.display === '') ? 'none' : ''
  }
  function sauve () {
    editor.mtgAppLecteur.setApiDoc(editor.idSvg)
    editor.exoEncours.height = editor.mtgAppLecteur.valueOf(editor.idSvg, 'A')
    editor.exoEncours.width = editor.mtgAppLecteur.valueOf(editor.idSvg, 'B')
    editor.exoEncours.txtFigure = editor.mtgAppLecteur.getBase64Code(editor.idSvg)
  }

  function testExo () {
    sauve()
    if (editor.maZASouv) {
      for (let i = 0; i < editor.maZASouv.length; i++) {
        if (editor.maZASouv[i]) {
          editor.maZASouv[i].disable()
        }
      }
    }
    editor.maZASouv = []
    if (editor.idSvg2) {
      editor.mtgAppLecteur.removeDoc(editor.idSvg2)
    }
    j3pEmpty(editor.divTest)
    editor.divTest.style.background = '#efe6e6'
    editor.divTest.style.width = '550px'
    editor.divTest.style.padding = '10px'
    const eeen = j3pAddElt(editor.divTest, 'div')
    eeen.style.padding = '5px'
    eeen.style.borderRadius = '5px'
    eeen.style.backgroundImage = 'linear-gradient(to left, rgba(245, 169, 107, 0.8) 30%, rgba(245, 220, 187, 0.4))'
    if (editor.exoEncours.consigneG !== '') {
      j3pAffiche(eeen, null, editor.exoEncours.consigneG)
      j3pAddContent(eeen, '\n')
    }
    if (editor.yaFigureLSon.reponse === 'Oui') {
      const contui = addDefaultTable(eeen, 1, 1)[0][0]
      const ui = j3pAddElt(contui, 'div')
      ui.style.border = '1px solid black'
      ui.style.borderRadius = '5px'
      ui.style.padding = '5px'
      ui.style.paddingTop = '0px'
      ui.style.background = '#aeef59'
      ui.style.textAlign = 'center'
      placeJouerSon(ui, editor.exoEncours, editor, 'son')
    }

    const tttn = j3pAddElt(editor.divTest, 'div')
    tttn.style.padding = '5px'
    tttn.style.borderRadius = '5px'
    tttn.style.backgroundImage = 'linear-gradient(to right, rgba(187, 187, 187, 0.8) 30%, rgba(224, 224, 224, 0.5))'

    editor.idSvg2 = j3pGetNewId()
    editor.limmm3 = j3pAddElt(tttn, 'img', '', { src: editor.exoEncours.image, width: editor.exoEncours.widthImage, height: editor.exoEncours.heightImage })
    editor.limmm3.style.top = '0px'
    editor.limmm3.style.left = '0px'
    editor.limmm3.style.position = 'relative'
    editor.tttn = tttn
    editor.leMG2 = j3pCreeSVG(tttn, { id: editor.idSvg2, width: (287 + editor.exoEncours.width * 355), height: (149 + editor.exoEncours.height * 280) })
    editor.mtgAppLecteur.addDoc(editor.idSvg2, editor.exoEncours.txtFigure, false)
    editor.mtgAppLecteur.setApiDoc(editor.idSvg2)
    editor.mtgAppLecteur.calculate(editor.idSvg2)
    editor.mtgAppLecteur.display(editor.idSvg2).then(() => {
      editor.mesZone = []
      editor.leMG2.childNodes[1].setAttribute('fill', 'rgb(0,0,0,0)')
      editor.leMG2.style.position = 'relative'
      editor.leMG2.style.top = '-' + (149 + editor.exoEncours.height * 280) + 'px'
      editor.leMG2.style.left = '0px'
      editor.limmm3.style.width = (287 + editor.exoEncours.width * 355) + 'px'
      editor.limmm3.style.height = (149 + editor.exoEncours.height * 280) + 'px'
      tttn.style.height = (149 + editor.exoEncours.height * 280) + 'px'
      let tailleavrire = (149 + editor.exoEncours.height * 280) * 2
      // let tailleaVireX = 0
      editor.tailleAsouv = []
      for (let i = 0; i < editor.exoEncours.listZone.length; i++) {
        if (editor.exoEncours.listZone[i].type === 'rectangle') {
          for (let k = 0; k < editor.exoEncours.listZone[i].surface.length; k++) {
            editor.mtgAppLecteur.setHidden({ elt: editor.exoEncours.listZone[i].surface[k] })
          }
          const p1 = editor.mtgAppLecteur.getPointPosition({ a: editor.exoEncours.listZone[i].p1 })
          const p2 = editor.mtgAppLecteur.getPointPosition({ a: editor.exoEncours.listZone[i].p2 })
          const p3 = editor.mtgAppLecteur.getPointPosition({ a: editor.exoEncours.listZone[i].p3 })
          const p4 = editor.mtgAppLecteur.getPointPosition({ a: editor.exoEncours.listZone[i].p4 })
          const x = Math.min(p1.x, p2.x, p3.x, p4.x)
          const y = Math.min(p1.y, p2.y, p3.y, p4.y)
          const x2 = Math.max(p1.x, p2.x, p3.x, p4.x)
          const y2 = Math.max(p1.y, p2.y, p3.y, p4.y)
          const mondiv = j3pAddElt(tttn, 'div')
          mondiv.style.position = 'relative'
          mondiv.style.top = y - tailleavrire + 'px'
          mondiv.style.left = x + 'px'
          mondiv.style.width = (x2 - x) + 'px'
          mondiv.style.height = (y2 - y) + 'px'
          mondiv.style.background = '#fff'
          tailleavrire += (y2 - y)
          mondiv.style.verticalAlign = 'middle'
          mondiv.style.textAlign = 'center'
          const maZ = new ZoneStyleMathquill3(mondiv, { restric: larestr, sansBord: true, petiteFont: true })
          maZ.lediv = mondiv
          maZ.zone = editor.exoEncours.listZone[i]
          editor.mesZone.push(maZ)
          mondiv.addEventListener('click', () => {
            setTimeout(() => {
              maZ.focus()
            }, 10)
          })
          editor.tailleAsouv[i] = mondiv
          editor.maZASouv[i] = maZ
          if (editor.exoEncours.listZone[i].encadre) mondiv.style.border = '1px solid black'
        }
        if (editor.exoEncours.listZone[i].type === 'label') {
          const mondiv = j3pAddElt(tttn, 'div')
          mondiv.classList.add('pourFitContent')
          mondiv.style.position = 'relative'
          mondiv.style.verticalAlign = 'middle'
          mondiv.style.textAlign = 'center'
          mondiv.style.fontFamily = 'Bitstream Véracité Sans'
          mondiv.style.pointerEvents = 'none'
          mondiv.style.fontSize = editor.exoEncours.listZone[i].taille + 'px'
          j3pAffiche(mondiv, null, editor.exoEncours.listZone[i].texte)
          const p1 = editor.mtgAppLecteur.getPointPosition({ a: editor.exoEncours.listZone[i].p1 })
          mondiv.style.top = (p1.y - tailleavrire) + 'px'
          mondiv.style.left = p1.x + 'px'
          const aaj = mondiv.getBoundingClientRect()
          tailleavrire += aaj.height
        }
        editor.mtgAppLecteur.setHidden({ elt: editor.exoEncours.listZone[i].p1 })
        if (editor.exoEncours.listZone[i].type !== 'label') {
          editor.mtgAppLecteur.setHidden({ elt: editor.exoEncours.listZone[i].texte })
          editor.mtgAppLecteur.setHidden({ elt: editor.exoEncours.listZone[i].p2 })
          editor.mtgAppLecteur.setVisible({ elt: editor.exoEncours.listZone[i].poly })
          editor.mtgAppLecteur.setColor({ elt: editor.exoEncours.listZone[i].poly, opacity: 0, color: '#000' })
        }
      }
      // tttn.style.overflow = 'hidden'
    })

    const con = j3pAddElt(editor.divTest, 'div')
    con.style.padding = '5px'
    con.style.borderRadius = '5px'
    con.style.backgroundImage = 'linear-gradient(to left, rgba(170, 255, 141, 0.5) 30%, rgba(209, 248, 194, 0.8))'
    const yu2 = addDefaultTable(con, 1, 2)
    j3pAjouteBouton(yu2[0][0], voirCo, { value: 'voir la correction' })
    editor.divAsup = yu2[0][0]

    const con2 = j3pAddElt(editor.divTest, 'div')
    con.style.padding = '5px'
    con.style.borderRadius = '5px'
    con.style.backgroundImage = 'linear-gradient(to left, rgba(170, 255, 141, 0.5) 30%, rgba(209, 248, 194, 0.8))'
    const yu22 = addDefaultTable(con2, 1, 3)
    const maZ3 = new ZoneStyleMathquill3(yu22[0][0], { restric: larestr })
    j3pAddContent(yu22[0][1], '&nbsp;&nbsp;')
    yu22[0][2].style.background = '#aaa'
    maZ3.onchange = () => {
      j3pEmpty(yu22[0][2])
      j3pAddContent(yu22[0][2], maZ3.reponse())
    }
    j3pAddContent(con2, 'Utilisez cette zone de saisie pour copier-coller les résultats attendus.<br><i>( utile pour les formules mathématiques. )</i>')
  }
  function voirCo () {
    j3pEmpty(editor.divAsup)
    for (let i = 0; i < editor.exoEncours.listZone.length; i++) {
      if (editor.exoEncours.listZone[i].type === 'rectangle') {
        editor.maZASouv[i].disable()
        editor.maZASouv[i] = undefined
        j3pEmpty(editor.tailleAsouv[i])
        const mondiv = j3pAddElt(editor.tailleAsouv[i], 'div')
        mondiv.style.position = 'relative'
        mondiv.style.top = '0px'
        mondiv.style.left = '0px'
        mondiv.style.width = '100%'
        mondiv.style.height = '100%'
        mondiv.style.background = '#43e2e5'
        mondiv.style.verticalAlign = 'middle'
        mondiv.style.textAlign = 'center'
        const tt = (editor.exoEncours.listZone[i].repAt[0]) ? ('$' + editor.exoEncours.listZone[i].repAt[0].text + '$') : 'à prévoir'
        j3pAffiche(mondiv, null, tt)
      }
    }
  }

  function affListeZone () {
    j3pEmpty(editor.zoneList)
    j3pEmpty(editor.divTest)
    if (editor.maZASouv) {
      for (let i = 0; i < editor.maZASouv.length; i++) {
        if (editor.maZASouv[i]) {
          editor.maZASouv[i].disable()
        }
      }
    }
    editor.maZASouv = []
    for (let i = 0; i < editor.listAffiche.length; i++) {
      j3pDetruit(editor.listAffiche[i])
    }
    editor.listAffiche = []
    editor.nomPos = []
    editor.listCoche = []
    const tab = addDefaultTable(editor.zoneList, editor.exoEncours.listZone.length + 1, 1)
    tab[0][0].style.border = '1px solid black'
    tab[0][0].style.padding = '5px'
    const tabList = addDefaultTable(tab[0][0], 2, 1)
    const tabContList1 = addDefaultTable(tabList[0][0], 1, 6)
    tabContList1[0][0].style.padding = 'Opx 5px 5px 5px'
    tabContList1[0][1].style.padding = 'Opx 5px 5px 5px'
    tabContList1[0][2].style.padding = 'Opx 5px 5px 5px'
    tabContList1[0][3].style.padding = 'Opx 5px 5px 5px'
    tabContList1[0][4].style.padding = 'Opx 5px 5px 5px'
    tabContList1[0][5].style.padding = 'Opx 5px 5px 5px'
    for (let i = 0; i < editor.exoEncours.listZone.length; i++) {
      tab[i + 1][0].style.border = '1px solid black'
      tab[i + 1][0].style.padding = '5px'
      const tabList = addDefaultTable(tab[i + 1][0], 2, 1)
      const tabContList1 = addDefaultTable(tabList[0][0], 1, 7)
      tabContList1[0][0].style.padding = 'Opx 5px 5px 5px'
      tabContList1[0][1].style.padding = 'Opx 5px 5px 5px'
      tabContList1[0][2].style.padding = 'Opx 5px 5px 5px'
      tabContList1[0][3].style.padding = 'Opx 5px 5px 5px'
      tabContList1[0][4].style.padding = 'Opx 5px 5px 5px'
      tabContList1[0][5].style.padding = 'Opx 5px 5px 5px'
      let nnnom
      switch (editor.exoEncours.listZone[i].type) {
        case 'label':
          nnnom = editor.exoEncours.listZone[i].nom.replace('Zone', 'Label')
          break
        case 'cache':
          nnnom = editor.exoEncours.listZone[i].nom.replace('Zone', 'Cache')
          break
        default: nnnom = editor.exoEncours.listZone[i].nom
      }
      nnnom += '&nbsp;&nbsp;'
      j3pAddContent(tabContList1[0][0], '<b>' + nnnom + '</b>')
      tabContList1[0][0].style.color = editor.exoEncours.listZone[i].color
      if (editor.exoEncours.listZone[i].type === 'rectangle') {
        afficheRep(editor.exoEncours.listZone[i], tabList[1][0])
        const boutEnc = j3pAjouteBouton(tabContList1[0][5], () => {
          const el = editor.exoEncours.listZone[i]
          el.encadre = !el.encadre
          boutEnc.value = el.encadre ? 'Sans bordure' : 'Bordure'
          if (!el.encadre) {
            editor.mtgAppLecteur.setHidden({ elt: el.poly })
          } else {
            editor.mtgAppLecteur.setVisible({ elt: el.poly })
          }
        }, { value: !editor.exoEncours.listZone[i].encadre ? 'Bordure' : 'Sans bordure' })
      }
      if (editor.exoEncours.listZone[i].type === 'label') {
        j3pAddContent(tabContList1[0][2], 'taille:&nbsp;')

        const titaille = j3pAddElt(tabContList1[0][3], 'input', '', {
          type: 'number',
          value: editor.exoEncours.listZone[i].taille,
          size: 3,
          min: '15',
          max: '40'
        })
        titaille.addEventListener('change', () => {
          const averif = titaille.value
          if (averif === '' || isNaN(averif) || (Number(averif) < 15) || (Number(averif) > 40)) titaille.value = '15'
          editor.exoEncours.listZone[i].taille = Number(titaille.value)
          majAffiche(editor.exoEncours.listZone[i])
        })
        titaille.addEventListener('input', () => {
          const averif = titaille.value
          if (averif === '' || isNaN(averif) || (Number(averif) < 15) || (Number(averif) > 40)) titaille.value = '15'
          editor.exoEncours.listZone[i].taille = Number(titaille.value)
          majAffiche(editor.exoEncours.listZone[i])
        })

        afficheLab(editor.exoEncours.listZone[i], tabList[1][0])
      }
      editor.nomPos.push(editor.exoEncours.listZone[i].nom)
      j3pAjouteBouton(tabContList1[0][3], makeSup(i), { value: 'Suppr.' })
      j3pAddContent(tabContList1[0][3], '&nbsp;&nbsp;')
      j3pAddContent(tabContList1[0][4], '&nbsp;&nbsp;')
      const boutFix = j3pAjouteBouton(tabContList1[0][3], () => {
        const el = editor.exoEncours.listZone[i]
        el.bloque = !el.bloque
        boutFix.value = el.bloque ? 'Débloquer' : 'Fixer'
        const aMod = [el.p1]
        if (el.type !== 'label') aMod.push(el.p2)
        editor.mtgAppLecteur.setApiDoc(editor.idSvg)
        if (editor.exoEncours.listZone[i].bloque) {
          for (let j = 0; j < aMod.length; j++) {
            editor.mtgAppLecteur.setHidden({ elt: aMod[j] })
          }
        } else {
          for (let j = 0; j < aMod.length; j++) {
            editor.mtgAppLecteur.setVisible({ elt: aMod[j] })
          }
        }
      }, { value: editor.exoEncours.listZone[i].bloque ? 'Débloquer' : 'Fixer' })
    }
    gereMove()
  }

  function afficheLab (el, ou) {
    creeAffiche(el)
    const tabRep = addDefaultTable(ou, 1, 2)
    j3pAddContent(tabRep[0][0], '<b><u>Texte:</u></b>&nbsp;&nbsp;')
    const input = j3pAddElt(tabRep[0][1], 'input', {
      size: 80,
      value: el.texte
    })
    input.addEventListener('change', () => {
      el.texte = input.value
      majAffiche(el)
    })
    input.addEventListener('input', () => {
      el.texte = input.value
      majAffiche(el)
    })
  }
  function creeAffiche (el) {
    const mondiv = j3pAddElt(editor.whereFig, 'span')
    mondiv.style.position = 'relative'
    mondiv.style.verticalAlign = 'middle'
    mondiv.style.textAlign = 'center'
    mondiv.el = el
    editor.listAffiche.push(mondiv)
    mondiv.style.fontFamily = 'Bitstream Véracité Sans'
    mondiv.style.pointerEvents = 'none'
    mondiv.style.fontSize = el.taille + 'px'
    j3pAffiche(mondiv, null, el.texte)
  }
  function majAffiche (el) {
    for (let i = 0; i < editor.listAffiche.length; i++) {
      if (editor.listAffiche[i].el.nom === el.nom) {
        j3pEmpty(editor.listAffiche[i])
        editor.listAffiche[i].style.fontSize = el.taille + 'px'
        j3pAffiche(editor.listAffiche[i], null, el.texte)
      }
    }
    gereMove()
  }
  function afficheRep (tab, ou) {
    const tabRep = addDefaultTable(ou, tab.repAt.length + 2, 1)
    j3pAddContent(tabRep[0][0], '<b><u>Réponse acceptées</u></b>')
    for (let i = 0; i < tab.repAt.length; i++) {
      const titti = addDefaultTable(tabRep[i + 1][0], 1, 3)
      j3pAddContent(titti[0][1], '&nbsp;&nbsp;')
      const input = j3pAddElt(titti[0][0], 'input', {
        size: 80,
        value: tab.repAt[i].text
      })
      input.addEventListener('change', () => {
        tab.repAt[i].text = input.value
      })
      input.addEventListener('input', () => {
        tab.repAt[i].text = input.value
      })
      j3pAjouteBouton(titti[0][2], () => {
        tab.repAt.splice(i, 1)
        affListeZone()
      }, { value: 'suppr.' })
    }
    j3pAjouteBouton(tabRep[tab.repAt.length + 1][0], () => {
      tab.repAt.push({ text: '' })
      affListeZone()
    }, { value: '+' })
  }
  function makeSup (i) {
    return () => {
      const av = editor.exoEncours.listZone[i]
      editor.exoEncours.listZone.splice(i, 1)
      editor.mtgAppLecteur.setApiDoc(editor.idSvg)
      editor.mtgAppLecteur.deleteElt({ elt: av.p1 })
      if (av.type !== 'label') editor.mtgAppLecteur.deleteElt({ elt: av.p2 })
      affListeZone()
    }
  }

  function faitMG () {
    j3pEmpty(editor.whereFig)
    editor.whereFig.style.height = '500px'
    editor.whereFig.style.border = '7px solid black'
    editor.idSvg = j3pGetNewId()
    editor.limmm2 = j3pAddElt(editor.whereFig, 'img', '', { src: editor.exoEncours.image, width: editor.exoEncours.widthImage, height: editor.exoEncours.heightImage })
    editor.limmm2.style.position = 'relative'
    editor.limmm2.style.top = '0px'
    editor.limmm2.style.left = '0px'
    editor.leMG = j3pCreeSVG(editor.whereFig, { id: editor.idSvg, width: 800, height: 500 })
    editor.mtgAppLecteur.removeAllDoc()
    editor.mtgAppLecteur.addDoc(editor.idSvg, editor.exoEncours.txtFigure || genMGEx, true)
    editor.mtgAppLecteur.setApiDoc(editor.idSvg)
    editor.mtgAppLecteur.calculateFirstTime(editor.idSvg)
    editor.mtgAppLecteur.display(editor.idSvg).then(() => {
      editor.leMG.childNodes[1].setAttribute('fill', 'rgb(0,0,0,0)')
      editor.leMG.style.position = 'relative'
      editor.leMG.addEventListener('mousemove', gereMove, false)
      editor.leMG.style.left = '0px'
      editor.mtgAppLecteur.addSvgListener({
        eventName: 'mousedown',
        callBack: (event, x, y) => {
          event.preventDefault()
          event.stopPropagation()
          editor.funcClick(x, y)
        }
      })
      editor.mtgAppLecteur.addSvgListener({
        eventName: 'mousemove',
        callBack: (event, x, y) => {
          editor.funcMov(x, y)
        }
      })
      editor.leMG.addEventListener('mousedown', (event) => {
        event.preventDefault()
        event.stopPropagation()
      })
    })
    gereMove()
    editor.whereFig.style.overflow = 'hidden'
  }
  function gereMove () {
    editor.mtgAppLecteur.setApiDoc(editor.idSvg)
    const a = editor.mtgAppLecteur.valueOf(editor.idSvg, 'A')
    const b = editor.mtgAppLecteur.valueOf(editor.idSvg, 'B')
    editor.limmm2.style.width = (287 + b * 355) + 'px'
    editor.limmm2.style.height = (149 + a * 280) + 'px'
    editor.leMG.style.top = '-' + editor.limmm2.style.height
    const tailleaVire = (149 + a * 280) + 500
    let tailleaVireX = 0
    for (let i = 0; i < editor.listAffiche.length; i++) {
      const p1 = editor.mtgAppLecteur.getPointPosition({ a: editor.listAffiche[i].el.p1 })
      editor.listAffiche[i].style.top = (p1.y - tailleaVire) + 'px'
      editor.listAffiche[i].style.left = (p1.x - tailleaVireX) + 'px'
      const aaj = editor.listAffiche[i].getBoundingClientRect()
      tailleaVireX += aaj.width
    }
  }
  function faitIm () {
    editor.exoEncours.image = editor.exoEncours.image || genIm
    editor.exoEncours.widthImage = 200
    editor.exoEncours.heightImage = 200
    function majIm () {
      editor.limmm2.src = editor.exoEncours.image
    }
    importImage(editor.whereFigIm, editor.exoEncours, 'image', 'widthImage', 'heightImage', true, editor, null, majIm)
  }
  function affiErr (ou) {
    j3pEmpty(ou)
    j3pAddContent(ou, '<b><u>Erreurs à discerner:</u></b>&nbsp;')
    // ou.style.border = '1px solid black'
    for (let i = 0; i < editor.exoEncours.errPrev.length; i++) {
      const aj = addDefaultTable(ou, 2, 4)
      j3pAffiche(aj[0][0], null, (i + 1) + '. à repérer:&nbsp;')
      j3pAffiche(aj[1][0], null, 'remède:&nbsp;')
      aj[1][0].style.textAlign = 'right'
      const yu = j3pAddElt(aj[0][1], 'input', '', {
        value: editor.exoEncours.errPrev[i].rep,
        size: 50
      })
      yu.addEventListener('change', makeChange(i, yu))
      const yu2 = j3pAddElt(aj[1][1], 'input', '', {
        value: editor.exoEncours.errPrev[i].remede,
        size: 50
      })
      yu2.addEventListener('change', makeChange2(i, yu2))
      j3pAddContent(aj[0][2], '&nbsp;&nbsp;')
      j3pAjouteBouton(aj[0][3], makeSup(i, ou), { value: '✄' })
    }
    j3pAddContent(ou, '\n')
    j3pAjouteBouton(ou, () => { addEr(ou) }, { value: '+' })
  }
  function addEr (ou) {
    editor.exoEncours.errPrev.push({ rep: '', remede: '' })
    affiErr(ou)
  }
  function makeChange (i, t) {
    return () => {
      editor.exoEncours.errPrev[i].rep = t.value
    }
  }
  function makeChange2 (i, t) {
    return () => {
      editor.exoEncours.errPrev[i].remede = t.value
    }
  }
  function valModifExo () {
    editor.okVire = false
    if (editor.maZASouv) {
      for (let i = 0; i < editor.maZASouv.length; i++) {
        if (editor.maZASouv[i]) {
          editor.maZASouv[i].disable()
        }
      }
    }
    editor.maZASouv = []
    sauve()
    editor.ProgrammeS[editor.exoEncoursi].image = editor.exoEncours.image
    editor.ProgrammeS[editor.exoEncoursi].yaSon = editor.exoEncours.yaSon
    if (editor.ProgrammeS[editor.exoEncoursi].yaSon) {
      editor.ProgrammeS[editor.exoEncoursi].son = editor.exoEncours.son
    } else {
      editor.ProgrammeS[editor.exoEncoursi].son = undefined
    }
    editor.ProgrammeS[editor.exoEncoursi].nom = editor.exoEncours.nom
    editor.ProgrammeS[editor.exoEncoursi].survol = editor.exoEncours.survol
    editor.ProgrammeS[editor.exoEncoursi].consigneG = editor.exoEncours.consigneG
    editor.ProgrammeS[editor.exoEncoursi].txtFigure = editor.exoEncours.txtFigure
    editor.ProgrammeS[editor.exoEncoursi].width = editor.exoEncours.width
    editor.ProgrammeS[editor.exoEncoursi].height = editor.exoEncours.height
    editor.ProgrammeS[editor.exoEncoursi].fautTout = editor.exoEncours.fautTout
    editor.ProgrammeS[editor.exoEncoursi].listZone = j3pClone(editor.exoEncours.listZone)
    editor.ProgrammeS[editor.exoEncoursi].avertFond = editor.exoEncours.avertFond
    menuExo()
  }
  function vireConstEncours () {
    for (let i = 0; i < editor.objAvire.length; i++) {
      editor.mtgAppLecteur.deleteElt({ elt: editor.objAvire[i] })
    }
    editor.objAvire = []
    editor.mtgAppLecteur.getDoc(editor.idSvg).defaultCursor = undefined
    if (editor.spanConst) {
      j3pEmpty(editor.spanConst)
      j3pAddContent(editor.spanConst, '&nbsp;')
    }
  }
  function chgNom () {
    editor.exoEncours.nom = editor.inputNouveauNom.value
    const bufadd = (editor.exoEncours.nom) ? ' (' + editor.exoEncours.nom + ') ' : ''
    j3pEmpty(editor.cellModifExo)
    j3pAddContent(editor.cellModifExo, '<b>Modification de l’exercice ' + editor.numExo + bufadd + '</b>.')
  }

  function supExo (i) {
    editor.ProgrammeS.splice(i, 1)
    menuExo()
  }
  function faisLimite () {
    const viz = (editor.listeparamLimite.reponse === 'Oui') ? '' : 'none'
    editor.chronoCahc2.style.display = viz
  }
  return new Promise((resolve, reject) => {
    function onSaveParams () {
      // on affecte nos nouvelles valeurs (à récupérer dans l’éditeur mis ci-dessus)
      if (document.fullscreenElement) document.exitFullscreen()
      const aComp = []
      for (let prog = 0; prog < editor.ProgrammeS.length; prog++) {
        let idMod = faisId(editor.ProgrammeS[prog].image)
        let iMod = aComp.indexOf(idMod)
        if (iMod !== -1) {
          editor.ProgrammeS[prog].image = { type: 'clone', num: iMod }
        } else {
          aComp.push(idMod)
        }
        if (editor.ProgrammeS[prog].yaSon) {
          idMod = faisId(editor.ProgrammeS[prog].son)
          iMod = aComp.indexOf(idMod)
          if (iMod !== -1) {
            editor.ProgrammeS[prog].son = { type: 'clone', num: iMod }
          } else {
            aComp.push(idMod)
          }
        }
        idMod = faisId(editor.ProgrammeS[prog].txtFigure)
        iMod = aComp.indexOf(idMod)
        if (iMod !== -1) {
          editor.ProgrammeS[prog].txtFigure = { type: 'clone', num: iMod }
        } else {
          aComp.push(idMod)
        }
      }
      resolve(
        {
          Liste: JSON.stringify(
            {
              titre: editor.inputTitre.value,
              liste: editor.ProgrammeS
            }
          ),
          limite: (editor.listeparamLimite.reponse === 'Non') ? 0 : Number(editor.inputLimite.value),
          nbrepetitions: Number(editor.nbrepetitions.value),
          theme: editor.themeL.reponse,
          ordonne: editor.ordonneL.reponse
        })
    }

    function onSaveParamsT () {
      if (editor.yaBlok) {
        const yy = j3pModale2({ titre: 'Avertissement ', contenu: 'Un exercice est en cours\n de modification.', divparent: container })
        j3pAddContent(yy, '\n')
        j3pAjouteBouton(yy, () => {
          j3pDetruit(yy)
          j3pDetruit(yy.masque)
        }, { value: 'Annuler' })
        j3pAddContent(yy, '\n')
        j3pAddContent(yy, '\n')
        j3pAjouteBouton(yy, () => {
          j3pDetruit(yy)
          j3pDetruit(yy.masque)
          onSaveParams()
        }, { value: 'Tant pis' })
        return
      }
      onSaveParams()
    }

    // on ajoute notre éditeur dans container
    const btnsContainer = j3pAddElt(container, 'div', 'Chargement en cours…', {
      style: {
        margin: '1rem auto',
        textAlign: 'center'
      }
    })

    j3pEmpty(btnsContainer)
    // Important, les boutons annuler / valider
    loadMq()
      .then(() => {
        container.parentNode.style.opacity = 1
        const btnProps = { style: { margin: '0.2rem', fontSize: '120%' } }
        j3pAddElt(btnsContainer, 'button', 'Annuler', btnProps)
          .addEventListener('click', () => {
            if (document.fullscreenElement) document.exitFullscreen()
            resolve()
          })
        j3pAddElt(btnsContainer, 'button', 'Valider', btnProps)
          .addEventListener('click', onSaveParamsT)
        try {
          valuesDeb.Liste = JSON.parse(values.Liste)
          editor.ProgrammeS = j3pClone(valuesDeb.Liste.liste)
          const aComp = []
          for (let prog = 0; prog < editor.ProgrammeS.length; prog++) {
            let aCompte = true
            if (editor.ProgrammeS[prog].image !== 'string') {
              if (editor.ProgrammeS[prog].image.type === 'clone') {
                aCompte = false
                editor.ProgrammeS[prog].image = j3pClone(aComp[editor.ProgrammeS[prog].image.num])
              }
            }
            if (aCompte) aComp.push(j3pClone(editor.ProgrammeS[prog].image))
            aCompte = true
            if (editor.ProgrammeS[prog].yaSon) {
              if (editor.ProgrammeS[prog].son !== 'string') {
                if (editor.ProgrammeS[prog].son.type === 'clone') {
                  aCompte = false
                  editor.ProgrammeS[prog].son = j3pClone(aComp[editor.ProgrammeS[prog].son.num])
                }
              }
              if (aCompte) aComp.push(j3pClone(editor.ProgrammeS[prog].son))
            }
            aCompte = true
            if (editor.ProgrammeS[prog].txtFigure !== 'string') {
              if (editor.ProgrammeS[prog].txtFigure.type === 'clone') {
                aCompte = false
                editor.ProgrammeS[prog].txtFigure = j3pClone(aComp[editor.ProgrammeS[prog].txtFigure.num])
              }
            }
            if (aCompte) aComp.push(j3pClone(editor.ProgrammeS[prog].txtFigure))
          }
        } catch (error) {
          console.error(error)
        }
        getMtgCore({ withMathJax: true, withApiPromise: true })
          .then(
            // success
            (mtgAppLecteur) => {
              editor.mtgAppLecteur = mtgAppLecteur
              editor.funcClick = () => {
              }
              container.addEventListener('mousedown', () => {
                editor.funcClick = () => {}
                editor.funcMov = () => {}
                if (editor.okVire) vireConstEncours()
              })
              creeLesDiv()
              menuExo()
              menuTotal()
            },
            // failure
            (error) => {
              j3pShowError(error, { message: 'mathgraph n’est pas correctement chargé', mustNotify: true })
            })
          // plantage dans le code de success
          .catch(j3pShowError)
      })
      .catch(reject)
  }) // promesse retournée
}
