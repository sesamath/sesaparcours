```{r data generation, echo = FALSE, results = "hide"}
## parameters
a <- sample(2:9, 1)
b <- sample(seq(2, 4, 0.1), 1)
c <- sample(seq(0.5, 0.8, 0.01), 1)
## solution
res <- exp(b * c) * (a * c^(a-1) + b * c^a)
```

Question
========
Quelle est la dérivée de $f(x) = x^{`r a`} e^{`r b` x}$, évaluée en $x = `r c`$?


Solution
========
En utilisant la règle du produit pour $f(x) = g(x) \cdot h(x)$, où $g(x) = x^{`r a`}$ et $h(x) = e^{`r b` x}$, nous obtenons
$$
\begin{aligned}
f'(x) &= [g(x) \cdot h(x)]' = g'(x) \cdot h(x) + g(x) \cdot h'(x) \\
      &= `r a` x^{`r a` - 1} \cdot e^{`r b` x} + x^{`r a`} \cdot e^{`r b` x} \cdot `r b` \\
      &= e^{`r b` x} \cdot(`r a` x^`r a-1` + `r b` x^{`r a`}) \\
      &= e^{`r b` x} \cdot x^`r a-1` \cdot (`r a` + `r b` x).
\end{aligned}
$$
Pour $x = `r c`$, la réponse est
$$ e^{`r b` \cdot `r c`} \cdot `r c`^`r a-1` \cdot (`r a` + `r b` \cdot `r c`) \approx `r paste("!!!",fmt(res),"!!!",sep="")`. $$

Meta-information
================
extype: num
exsolution: `r fmt(res)`
exname: derivative exp
extol: 0.01
