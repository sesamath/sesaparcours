Question
========
Indiquez les villes qui sont la capitale du pays correspondant.

Answerlist
----------
* Lagos (Nigéria)
* São Paulo (Brésil)
* Toronto (Canada)
* Auckland (Nouvelle-Zélande)
* Istanbul (Turquie)
* Zurich (Suisse)
* Tokyo (Japon)
* New Delhi (Inde)
* Astana (Kazakhstan)
* Varsovie (Pologne)
* Riyad (Arabie saoudite)

Solution
========

Answerlist
----------
* False. La capitale du Nigéria est Abuja.
* False. La capitale du Brésil est Brasilia.
* False. La capitale du Canada est Ottawa.
* False. La capitale de la Nouvelle-Zélande est Wellington.
* False. La capitale de la Turquie est Ankara.
* False. La capitale de facto de la Suisse est Berne.
* True. Tokyo est la capitale du Japon.
* True. New Delhi est la capitale de l'Inde.
* True. Astana est la capitale du Kazakhstan.
* True. Varsovie est la capitale de la Pologne.
* True. Riyad est la capitale de l'Arabie saoudite.


Meta-information
================
exname: Capitals
extype: mchoice
exsolution: 00000011111
exshuffle: 5
