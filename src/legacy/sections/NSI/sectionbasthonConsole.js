import { j3pAddElt, j3pShowError, j3pAddContent } from 'src/legacy/core/functions'
import { addBasthonConsole } from 'src/lib/outils/basthon/console'
// import { getDiagMessage } from 'src/lib/outils/basthon/analyse'
const getDiagMessage = async () => undefined

const pcMinHeight = 20
const pcMaxHeight = 80
const pcDefaultHeight = 50

export const params = {
  // pas d’outils
  /**
   * Ce qui est paramétrable dans l’éditeur de graphe
   * @type {LegacySectionParamsParametres}
   */
  parametres: [
    ['enonce', 'Un texte d’énoncé\nIl faut afficher "Hello World" puis le double de a', 'string', 'L’énoncé à afficher avant la console (\\n pour retour à la ligne)'],
    ['enonceApres', 'Un peu de texte après.', 'string', 'Un complément d’énoncé à afficher après la console (\\n pour retour à la ligne)'],
    ['programmeInitial', 'print("Hello World")\n', 'string', 'Le programme initial (\\n pour retour à la ligne)'],
    ['hauteurConsole', pcDefaultHeight, 'number', 'La hauteur initiale de la console'],
    ['indication', '', 'string', 'Un indice éventuel']
  ]
  // pe à définir
}

/**
 * section basthonConsole
 * @this {Parcours}
 */
export default function main () {
  const me = this
  const stor = this.storage

  function init () {
    // donneesSection
    const ds = me.donneesSection = {}
    // on init avec ce qu’on exporte comme valeur par défaut des paramètres
    for (const [prop, initValue] of params.parametres) ds[prop] = initValue
    // et on surcharge d’après le graphe
    me.surcharge()
    // mais faut toujours imposer ça, sinon taper <entrée> dans l’éditeur valide
    me.validOnEnter = false // ex donneesSection.touche_entree

    // vérif de la hauteur passée via les params
    if (ds.hauteurConsole < pcMinHeight || ds.hauteurConsole > pcMaxHeight) ds.hauteurConsole = pcDefaultHeight
    // page
    me.construitStructurePage('presentation2')
    if (ds.indication) me.indication(me.zonesElts.IG, ds.indication)

    // me.afficheTitre(ds.textes.titre_exo)
    // enoncé
    const divEnonceAv = j3pAddElt(me.zonesElts.MG, 'div', ds.enonce)

    // l’input de resize de la console
    const p = j3pAddElt(divEnonceAv, 'p', 'Hauteur de la console')
    const input = j3pAddElt(p, 'input', { type: 'range', value: ds.hauteurConsole, style: { margin: '1rem' }, min: pcMinHeight, max: pcMaxHeight, step: 5 })
    const spanH = j3pAddElt(p, 'span', `${ds.hauteurConsole}%`)
    input.addEventListener('input', () => {
      if (stor.basthonConsole) stor.basthonConsole.setHeight(Number(input.value))
      j3pAddContent(spanH, `${input.value}%`, { replace: true })
    })

    // la console
    const divConsole = j3pAddElt(me.zonesElts.MG, 'div')

    // on met un peu de texte sous la console
    const divEnonceAp = j3pAddElt(me.zonesElts.MG, 'div')
    if (ds.enonceApres) j3pAddElt(divEnonceAp, 'p', ds.enonceApres)

    addBasthonConsole(divConsole).then(basthonConsole => {
      stor.basthonConsole = basthonConsole
      // y’a plus qu'à causer avec cette console…

      // On peut aussi démarrer l’exercice avec du code fourni
      if (ds.programmeInitial) stor.basthonConsole.setContent(ds.programmeInitial)

      // changer la hauteur
      stor.basthonConsole.setHeight(ds.hauteurConsole)

      /* exemples de boutons
      // Ajouter un bouton pour récupérer le code
      const getCodeBtn = j3pAddElt(divEnonceAp, 'button', 'Récupérer le code')
      getCodeBtn.addEventListener('click', () => {
        const currentCode = stor.basthonConsole.getContent()
        console.log('Le code récupéré dans l’éditeur', currentCode)
      })

      // un autre pour effacer
      const clearBtn = j3pAddElt(divEnonceAp, 'button', 'Réinitialiser')
      const restart = () => {
        stor.basthonConsole.restart()
        stor.basthonConsole.setContent(startingCode)
      }
      clearBtn.addEventListener('click', restart)
      /* fin exemples de boutons */

      // le code solution
      stor.codeSolution = 'print("Hello World")\nprint(2*a)'

      // enoncé terminé
      me.finEnonce()
    }).catch(j3pShowError)
  }

  switch (this.etat) {
    case 'enonce':
      if (this.debutDeLaSection) {
        init()
      }
      break

    case 'correction':
      stor.basthonConsole.editor.setReadOnly()
      getDiagMessage(stor.basthonConsole, stor.codeSolution, { a: 42 }).then(diag => {
        if (!diag) diag = 'C’est tout bon !'
        j3pAddElt(me.zonesElts.MG, 'p', diag)
        this.finCorrection('navigation')
      }).catch(j3pShowError)
      break

    case 'navigation':
      // @todo mettre le code python dans le résultat pour que le prof puisse le récupérer dans les bilans
      this.finNavigation()
      break
  }
}
