import $ from 'jquery'
import { j3pAddElt, j3pAjouteBouton, j3pDetruit, j3pElement, j3pEmpty, j3pFocus, j3pImporteAnnexe, j3pNotify, j3pPaletteMathquill, j3pShowError } from 'src/legacy/core/functions'
import { addElement, focusIfExists } from 'src/lib/utils/dom/main'
import { unLatexify } from 'src/lib/mathquill/functions'
import { getMtgApp } from 'src/lib/outils/mathgraph'
import { j3pCreeSVG } from 'src/legacy/core/functionsSvg'
import { MathfieldElement, renderMathInDocument } from 'mathlive'
import { setMathliveCss } from 'src/lib/outils/mathlive/mathlivecss'
import { getFirstIndexParExcluded, getIndexFermant } from 'src/lib/utils/string'
import { cleanLatexForMl, mathliveRestrict } from 'src/lib/outils/mathlive/utils'
import { afficheMathliveDans, getMathliveValue } from 'src/lib/outils/mathlive/display'
import textesGeneriques from 'src/lib/core/textes'

const { cBien, cFaux, essaieEncore, tempsDepasse } = textesGeneriques

/*
    Yves Biton
    Juillet 2014. Refondu en janvier 2024 pour passage à Mathlive

    http://localhost:8081/?graphe=[1,"squelettemtg32_Calc_Param_1_Edit",[{pe:">=0",nn:"fin",conclusion:"fin"},{f:"Fact_ax_Carre_Moins_cCarre_Ex1"}]];
    squelettemtg32_Calc_Param_1_Edit
    Squelette demandant de calculer une expression.
    L’élève valide ses réponses aussi bien en appuyant sur la touche Entrée que sur le bouton OK.
    On peut éventuellement spécifier des paramètres  a,b,c,d,e,f,g,h,j,k,n,n,p.

    Si la figure contient un paramètre nbvar , alors dans le cas ou nbrepetitions > 1, on donne pour i compris entre 1 et nbvar
    au calcul de la figure nommé "r"+i (par exemple r1 et r2 si nbParamAleat vaut 2) des valeurs comprises entre 0 et "nbcas"+i - 1
    toutes différentes (sauf si nbrepetitions > b=nbcas auquel cas au bout de nbcas répétitions il y aura nécessairement une valeur de r déjà prise.

*/

// on définit le dossier des fonts mathlive (il faut le faire très tôt,
// ici c’est bon avec pnpm start mais trop tard pour un build (cf src/lib/player/index.js de la branche draftPlayer)
if (MathfieldElement) { // undefined dans les tests avec happy-dom
  MathfieldElement.fontsDirectory = '/static/mathlive/fonts'
  MathfieldElement.soundsDirectory = '/static/mathlive/sounds'
}

export const params = {
  /**
   * Ce qui est paramétrable dans l’éditeur de graphe
   * @type {LegacySectionParamsParametres}
   */
  parametres: [
    ['nbrepetitions', 1, 'entier', 'aucun'],
    ['limite', 0, 'entier', 'Temps disponible par répétition, en secondes'],
    ['nbEssais', 6, 'entier', 'Nombre d’essais maximum autorisés'],
    ['indicationfaute', false, 'boolean', 'true si on veut que certaines fautes soient expliquées après appui sur Entrée'],
    ['simplifier', true, 'boolean', 'true si on exige un forme donnée, false sinon'],
    ['infoParam', 'Information sur le rôle des paramètres', 'string', 'Information sur l’utilisation des paramètres suivants'],
    ['a', 'random', 'string', 'Valeur de a'],
    ['b', 'random', 'string', 'Valeur de b'],
    ['c', 'random', 'string', 'Valeur de c'],
    ['d', 'random', 'string', 'Valeur de d'],
    ['e', 'random', 'string', 'Valeur de e'],
    ['f', 'random', 'string', 'Valeur de f'],
    ['g', 'random', 'string', 'Valeur de g'],
    ['h', 'random', 'string', 'Valeur de h'],
    ['j', 'random', 'string', 'Valeur de j'],
    ['k', 'random', 'string', 'Valeur de k'],
    ['l', 'random', 'string', 'Valeur de l'],
    ['m', 'random', 'string', 'Valeur de m'],
    ['n', 'random', 'string', 'Valeur de n'],
    ['p', 'random', 'string', 'Valeur de p'],
    ['q', 'random', 'string', 'Valeur de q'],
    ['r', 'random', 'string', 'Valeur de r'],
    ['ex', 'TerminaleSTMG/Calcul_Exp_Basea_Ex3', 'string', 'Nom du fichier annexe (xxx => squelettes-mtg32/xxx.js et xxx/yyy => squelettes-mtg32-xxx/yyy.js)']
  ]
}

const inputId = 'expressioninputmq1'

export default function squeletteMtg32CalcParam1Edit () {
  const parcours = this
  const sq = this.storage

  function onkeyup () {
    if (sq.marked) demarqueEditeurPourErreur()
  }

  function afficheNombreESsaisRestants () {
    j3pEmpty('info')
    const nbe = sq.nbEssais - sq.numEssai + 1
    if (nbe === 1) {
      afficheMathliveDans('info', 'texteinfo', 'Il reste un essai.', { style: { color: '#7F007F' } })
    } else {
      afficheMathliveDans('info', 'texteinfo', 'Il reste ' + nbe + ' essais.', { style: { color: '#7F007F' } })
    }
  }

  // Attention : Les }{ on été remplacés par des }/{
  function traiteIntegrales (st) {
    const faux = { valide: false, st }
    let ind
    // Traitement différent pour mathlive
    while ((ind = st.indexOf('\\int_')) !== -1) {
      const ind1 = getFirstIndexParExcluded(st, '^', ind + 5)
      if (ind1 === -1) return -1
      const a = st.substring(ind + 5, ind1)
      // Si le ^ est suivi d’une parenthèse on recherche la parenthèse fermante associée
      // Sinon c’est que la borne supérieure est formée d’un seul caractère
      let ind2, ind3, indsuiv
      if (st.charAt(ind1 + 1) === '(') {
        ind2 = ind1 + 2
        ind3 = getIndexFermant(st, ind1 + 1)
        indsuiv = ind3 + 1
      } else {
        ind2 = ind1 + 1
        ind3 = ind1 + 2
        indsuiv = ind3
      }
      if (ind3 === 0 || indsuiv >= st.length) return faux
      const b = st.substring(ind2, ind3)
      // Attention : les {}{} ont pu donner des ()/().
      if (st.charAt(indsuiv) === '/') indsuiv++
      // On recherche maintenant le d d’intégration en sautant les éventuelles parenthèses
      let ind4 = getFirstIndexParExcluded(st, '\\differentialD', indsuiv)
      if (ind4 === -1) return faux
      const fonc = st.substring(indsuiv, ind4)
      ind4 += 14
      let ind5
      if (st.charAt(ind4) === '(') {
        ind5 = getIndexFermant(st, ind4)
        ind4++
      } else {
        ind5 = ind4 + 1
      }
      if (ind5 === -1) return faux
      const varfor = st.substring(ind4, ind5) // Le caractère représentant la variable d’intégration
      st = st.substring(0, ind) + 'integrale(' + fonc + ',' + varfor + ',' + a + ',' + b + ')' + st.substring(ind5)
    }
    return { valide: true, res: st }
  }

  function traitePrimitives (st) {
    const faux = { valide: false, st }
    if (sq.variables === '') return { valide: true, res: st }
    let ind, tab
    while ((ind = st.indexOf('\\left[')) !== -1) {
      // On cherche le \right] correspondant
      const ind1 = getIndexFermant(st, ind + 5)
      if (ind1 === -1) return faux
      const fonc = st.substring(ind + 6, ind1 - 6)
      tab = variablesUtilisees(fonc)
      if (tab.length > 1) return faux
      const varfor = (tab.length === 0) ? sq.variables[0] : tab[0] // On traite le cas d’une fonction constante
      // Le crochet fermant doit être suivi d’un _
      if (st.charAt(ind1 + 1) !== '_') return faux
      const ind2 = getFirstIndexParExcluded(st, '^', ind1 + 2)
      if (ind2 === -1) return faux
      const a = st.substring(ind1 + 2, ind2)
      // Soit le caractère suivant n’est pas une parenthèse et c’est un seul caractère
      // qui est l’argument soit c’est le contenu de la parenthèse
      let ind3, ind4
      if (st.charAt(ind2 + 1) === '(') {
        ind3 = ind2 + 2
        ind4 = getIndexFermant(st, ind3)
      } else {
        ind3 = ind2 + 1
        ind4 = ind2 + 2
      }
      if (ind4 === -1 || ind4 > st.length) return faux
      const b = st.substring(ind3, ind4)
      st = st.substring(0, ind) + 'primitive(' + fonc + ',' + varfor + ',' + a + ',' + b + ')' + st.substring(ind4 + 1)
    }
    return { valide: true, res: st }
  }

  /**
   * Fonction renvoyant un tableau formé des noms de variables utilisées par le calcul contenu dans ch
   * Si le tableau renvoyé a une longueur au moins égale à 2 on considérera que la primitive est incohérente
   * @param {string} ch
   * @return {[]}
   */
  function variablesUtilisees (ch) {
    function utiliseVariable (nomvar) {
      let res
      res = ch.search(new RegExp('\\(' + nomvar + '[\\+\\-\\*\\/\\^\\)]')) !== -1
      res = res || ch.search(new RegExp(nomvar + '[\\+\\-\\*\\/\\^\\)]')) !== -1
      res = res || (ch.lastIndexOf(nomvar) === ch.length - 1)
      res = res || ch === nomvar
      return res
    }
    const tab = []
    for (let i = 0; i < sq.variables.length; i++) {
      const car = sq.variables.charAt(i)
      if (utiliseVariable(car)) tab.push(car)
    }
    return tab
  }

  function traiteMathlive (ch) {
    // Mathlive ne met pas d’accolades pour les fractions dont le numérateur et le dénominateur sont des entiers
    ch = ch.replace(/\\frac(\d)(\d)/g, '\\frac{$1' + '}{' + '$2}')
    // Ni pour les racines carrées d’un entier
    ch = ch.replace(/\\sqrt(\d)/g, '\\sqrt{$1}')
    // Pour les puissances dont l’exposant est un chiffre, Mathlive ne met pas d’accolades
    ch = ch.replace(/\^([^{])/g, '^{$1}')
    ch = unLatexify(ch)
    // mathlive peut mettre des {} vides
    ch = ch.replace(/\\{\\}/g, '')
    ch = ch.replace(/\^(\d)(\d)/g, '^$1*$2') // On peut avoir par exemple 2^35^4 pour 2^3*5^4
    let { res, valide } = traiteIntegrales(ch)
    if (valide) {
      const resul = traitePrimitives(res)
      res = resul.res
      valide = resul.valide
    }
    if (valide) ch = sq.mtgAppLecteur.addImplicitMult('mtg32svg', res)// Traitement des multiplications implicites
    const contientIntegOuPrim = ch.includes('integrale') || ch.includes('primitive')
    return { valide, res: ch, contientIntegOuPrim }
  }

  /**
   * Fonction extrayant d’un affichage LaTeX de la figure les lignes de texte et les transformant
   * en une chaîne affichable par MathQuill
   * @param tagLatex : le tag de l’affichage LaTeX de la figure
   * @return {string}
   */
  function extraitDeLatexPourMathlive (tagLatex) {
    const list = sq.mtgAppLecteur.getList('mtg32svg')
    const latex = list.getByTag(tagLatex)
    if (latex === null || latex.className !== 'CLatex') return ''
    return cleanLatexForMl(latex.chaineLatex)
  }

  /**
   * Fonction affichant la solution s’il existe un affichage LaTeX de tag solution
   * @param{boolean} bexact true si l’élève a entré la bonne réponse, false sinon
   */
  function afficheSolution (bexact) {
    $('#divSolution').css('display', 'block').html('')
    const ch = extraitDeLatexPourMathlive('solution')
    if (ch !== '') {
      afficheMathliveDans('divSolution', 'solution', ch, { style: { color: bexact ? 'green' : 'blue' } })
    }
  }

  function cacheSolution () {
    j3pEmpty('divSolution')
  }

  function marqueEditeurPourErreur () {
    sq.marked = true
    const mf = j3pElement(inputId)
    if (!mf) return
    mf.style.backgroundColor = '#FF9999'
    mf.demarqueErreur = () => {
      if (sq.marked) {
        demarqueEditeurPourErreur()
      }
    }
  }

  function demarqueEditeurPourErreur () {
    sq.marked = false
    j3pEmpty('correction')
    const mf = j3pElement(inputId)
    if (!mf) return
    mf.style.backgroundColor = 'white'
  }

  function validationEditeur () {
    sq.signeEgalManquant = false
    let res
    const rep = getMathliveValue(inputId)
    if (rep === '') {
      res = false
    } else if (sq.equation && !rep.includes('=')) {
      sq.signeEgalManquant = true
      res = false
    } else {
      const resul = traiteMathlive(rep)
      res = resul.valide && sq.mtgAppLecteur.syntaxValidation('mtg32svg', 'rep', resul.res, true)
    }
    if (!res) {
      marqueEditeurPourErreur()
      focusIfExists(inputId)
    }
    return res
  }

  function montreEditeur (bVisible) {
    if (bVisible) $('#info').css('display', 'block')
    $('#editeur').css('display', bVisible ? 'block' : 'none')
    if (sq.boutonsMathQuill) $('#boutonsmathquill').css('display', bVisible ? 'block' : 'none')
    // On vide le contenu de l’éditeur MathQuill et on lui donne le focus.
    j3pElement(inputId).value = ''
  }

  function videEditeur () {
    j3pElement(inputId).value = ''
  }

  function afficheReponse (bilan, depasse) {
    let coul
    if (bilan === 'exact') {
      coul = parcours.styles.cbien
    } else {
      if (bilan === 'faux') {
        coul = parcours.styles.cfaux
      } else { // Réponse exacte mais calcul pas fini
        if (depasse) {
          coul = parcours.styles.cfaux
        } else {
          coul = '#0000FF'
        }
      }
    }
    const num = sq.numEssai
    const idrep = 'exp' + num
    let chdeb = ''
    if (num > 2) chdeb = '<br>'

    if (bilan === 'faux') {
      afficheMathliveDans('formules', idrep, chdeb + '$' + (sq.entete === '' ? sq.aCalculer : sq.entete) + ' ' + sq.symbnonexact +
        ' ' + sq.rep + '$', {
        style: {
          color: coul
        }
      })
    } else {
      afficheMathliveDans('formules', idrep, chdeb + '$' + (sq.entete === '' ? sq.aCalculer : sq.entete) + ' ' + sq.symbexact +
        ' ' + sq.rep + '$', {
        style: {
          color: coul
        }
      })
    }
    const texteFaute = sq.indicationfaute ? extraitDeLatexPourMathlive('faute') : ''
    if (texteFaute) {
      afficheMathliveDans('formules', idrep + 'faute', '<br>' + texteFaute, {
        style: {
          color: parcours.styles.cfaux
        }
      })
    }
  }

  function validation () {
    if (sq.numEssai >= 1) {
      j3pElement('boutonrecopier').style.display = 'block'
    }
    sq.rep = getMathliveValue(inputId)
    sq.repMathQuill = traiteMathlive(sq.rep)
    const chcalcul = sq.repMathQuill.res
    sq.mtgAppLecteur.giveFormula2('mtg32svg', 'rep', chcalcul)
    sq.mtgAppLecteur.calculate('mtg32svg', false)
    if (sq.indicationfaute) {
      const faute = sq.mtgAppLecteur.valueOf('mtg32svg', 'faute')
      if (faute === 1) {
        sq.mtgAppLecteur.executeMacro('mtg32svg', 'voirFaute')
      } else {
        sq.mtgAppLecteur.executeMacro('mtg32svg', 'masquerFaute')
      }
    }
    const reponse = sq.mtgAppLecteur.valueOf('mtg32svg', 'reponse')
    sq.resolu = reponse === 1
    sq.exact = reponse === 2
  }

  function recopierReponse () {
    demarqueEditeurPourErreur()
    j3pElement(inputId).value = sq.rep
    focusIfExists(inputId)
  }

  // les fcts pour découper le chargement, initDatas (récupère les données du fichier annexe) => initMtg => initDom => initEnonce
  function initDatas (datas) {
    if (typeof datas !== 'object' || !datas.txt || typeof datas.txt !== 'string' || !Number.isInteger(datas.nbLatex)) {
      console.error(Error(`Le fichier annexe ${parcours.donneesSection.ex} ne remonte pas les données au format attendu`), datas)
      return j3pShowError('Paramètres incorrects')
    }
    sq.txt = datas.txt
    sq.entete = datas.entete ?? ''
    sq.equation = datas.equation ?? false
    sq.symbexact = datas.symbexact ?? '='
    sq.symbnonexact = datas.symbnonexact ?? '\\ne'
    sq.width = Number(datas.width ?? 780)
    sq.height = Number(datas.height ?? 900)

    sq.nbLatex = Number(datas.nbLatex ?? 0) // Le nombre de paramètres LaTeX dans le texte
    const listeBoutons = []
    // boutons avec propriétés à true par défaut
    const tabBoutons1 = ['puissance', 'fraction']
    ;['btnPuis', 'btnFrac'].forEach((p, i) => {
      const b = Boolean(datas[p] ?? true)
      if (b) listeBoutons.push(tabBoutons1[i])
    })
    // boutons avec false par défaut
    const tabBoutons2 = ['racine', 'pi', 'exp', 'ln', 'sin', 'cos', 'tan', 'abs', 'conj', 'integ', 'prim', 'differential']
    ;['btnRac', 'btnPi', 'btnExp', 'btnLn', 'btnSin', 'btnCos', 'btnTan', 'btnAbs', 'btnConj', 'btnInteg', 'btnPrim', 'btnDiff'].forEach((p, i) => {
      const b = Boolean(datas[p] ?? false)
      if (b) listeBoutons.push(tabBoutons2[i])
    })
    sq.listeBoutons = listeBoutons
    sq.boutonsMathQuill = sq.listeBoutons.length !== 0

    sq.consigne1 = datas.consigne1 ?? ''
    sq.consigne2 = datas.consigne2 ?? ''
    sq.consigne3 = datas.consigne3 ?? ''
    sq.consigne4 = datas.consigne4 ?? ''
    sq.titre = datas.titre ?? ''
    sq.bigSize = Boolean(datas.bigSize ?? false)
    sq.variables = datas.variables ?? ''
    sq.cbienmais = datas.cbienmais || 'C’est bien mais on pouvait simplifier la réponse'
    // Chaine contenant les paramètres autorisés
    sq.param = datas.param ?? ''
    // Le set de caractères utilisés dans l’éditeur
    sq.charset = datas.charset ?? ''
    initMtg()
  }

  function initMtg () {
    setMathliveCss('MepMG')
    const mtgOptions = {
      // player only, sans figure initiale ni svg, on gère ça dans initMtg et modif_fig
      loadCoreWithMathJax: true
    }
    getMtgApp('mtg32svg', {}, mtgOptions).then((mtgAppLecteur) => {
      sq.mtgAppLecteur = mtgAppLecteur
      initDom()
    }).catch(j3pShowError)
  }

  // appelé par initMtg, appellera initEnonce
  function initDom () {
    renderMathInDocument({ renderAccessibleContent: '' })
    if (sq.titre) parcours.afficheTitre(sq.titre)
    const conteneur = addElement(parcours.zonesElts.MG, 'div', { style: parcours.styles.etendre('toutpetit.enonce', { padding: '6px' }) })
    addElement(conteneur, 'div', { id: 'enonce' })
    const formules = addElement(conteneur, 'div', { id: 'formules', style: parcours.styles.petit.enonce }) // Contient le formules entrées par l’élève
    formules.style.fontSize = sq.bigSize ? '26px' : '22px'
    formules.style.paddingBottom = '4px'
    addElement(conteneur, 'div', { id: 'info' })
    afficheNombreESsaisRestants()
    const btns = addElement(conteneur, 'div')
    j3pAjouteBouton(btns, recopierReponse, { id: 'boutonrecopier', className: 'MepBoutonsRepere', value: 'Recopier réponse précédente', style: { display: 'none' } })
    const editeur = addElement(conteneur, 'div', { id: 'editeur' }) // Le div qui contiendra l’éditeur MathQuill
    const $editeur = $(editeur)
    if (sq.bigSize) $editeur.css('font-size', '30px')
    $editeur.css('padding-top', '10px') // Pour laisser un peu de place au-dessus de l’éditeur
    // j3pAffiche('editeur', 'debut', '', {})
    j3pAddElt('editeur', 'span', '', {
      id: 'debut'
    })
    afficheMathliveDans('editeur', 'expression', '&1&', {
      charset: sq.charset,
      listeBoutons: sq.listeBoutons
    })
    if (sq.charset !== '') {
      mathliveRestrict(inputId, sq.charset)
    }
    if (sq.boutonsMathQuill) {
      // padding pour laisser un peu de place au-dessus des boutons
      const boutonsmathquill = addElement(conteneur, 'div', { id: 'boutonsmathquill', style: { display: 'inline-block', paddingTop: '10px' } })
      // inline-block pour un bon alignement
      j3pPaletteMathquill(boutonsmathquill, inputId, { liste: sq.listeBoutons, nomdiv: 'palette', style: { display: 'inline-block' } })
    } else {
      j3pDetruit('boutonsmathquill')
    }
    j3pAddElt(conteneur, 'div', '', { id: 'divSolution' })
    $('#divSolution').css('display', 'none')
    j3pElement('divSolution').style.fontSize = sq.bigSize ? '22px' : '20px'
    const divMtg32 = addElement(conteneur, 'div')
    j3pCreeSVG(divMtg32, {
      id: 'mtg32svg',
      width: sq.width,
      height: sq.height
    })
    const style = parcours.styles.petit.correction
    style.marginTop = '1.5em'
    style.marginLeft = '0.5em'
    addElement(parcours.zonesElts.MD, 'div', { id: 'correction', style })
    // cette fct est sync donc le code qui suit pourrait en faire partie, mais on garde ce découpage "logique"
    initEnonce()
  }

  // appelé par initDom (sq.mtgAppLecteur est récupéré)
  function initEnonce () {
    sq.marked = false
    sq.mtgAppLecteur.removeAllDoc()
    sq.mtgAppLecteur.addDoc('mtg32svg', sq.txt, true)
    sq.mtgAppLecteur.calculate('mtg32svg', false)
    sq.nbexp = 0
    sq.reponse = -1
    const ch = 'abcdefghjklmnpqr'
    if (sq.param !== undefined) {
      for (const car of ch) {
        if (sq.param.includes(car)) {
          const par = parcours.donneesSection[car]
          if (par !== 'random') sq.mtgAppLecteur.giveFormula2('mtg32svg', car, par)
        }
      }
      sq.mtgAppLecteur.calculate('mtg32svg', true)
    }
    const nbvar = sq.mtgAppLecteur.valueOf('mtg32svg', 'nbvar')
    if (nbvar !== -1) {
      const nbrep = parcours.donneesSection.nbrepetitions
      sq.aleat = true
      sq.nbParamAleat = nbvar
      for (let i = 1; i <= nbvar; i++) {
        /** type {number} */
        const nbcas = sq.mtgAppLecteur.valueOf('mtg32svg', 'nbcas' + i)
        const nb = Math.max(nbrep, nbcas)
        const ar = []
        for (let j = 0; j < nb; j++) ar.push(j % nbcas)
        sq['tab' + i] = []
        for (let k = 0; k < nbrep; k++) {
          const tir = Math.floor(Math.random() * ar.length)
          sq['tab' + i].push(ar[tir])
          ar.splice(tir, 1)
        }
        sq.mtgAppLecteur.giveFormula2('mtg32svg', 'r' + i, sq['tab' + i][0])
      }
    } else {
      sq.aleat = false
    }

    sq.mtgAppLecteur.calculateAndDisplayAll(true)
    j3pElement(inputId).onkeyup = onkeyup

    const nbParam = sq.nbLatex
    const t = ['a', 'b', 'c', 'd']
    const param = {}
    for (let i = 0; i < nbParam; i++) {
      const code = sq.mtgAppLecteur.getLatexCode('mtg32svg', i)
      if (i === 0) sq.aCalculer = code.replaceAll('\n', '')
      param[t[i]] = code
    }
    param.e = String(sq.nbEssais)
    const st = sq.consigne1 + (sq.simplifier ? sq.consigne3 : sq.consigne2) + sq.consigne4
    afficheMathliveDans('enonce', 'texte', st, param)
    if (sq.entete !== '') {
      afficheMathliveDans('debut', 'acalc', '$' + sq.entete + '$' + (sq.equation ? ' ' : ' = '), param)
    } else {
      afficheMathliveDans('debut', 'acalc', '$' + sq.aCalculer + '=$ ', param)
    }

    sq.indicationfaute = parcours.donneesSection.indicationfaute // true si indication quand certaines fautes sont faites et appui sur Entrée

    // $("#expressioninputmq1").focus();
    focusIfExists(inputId)
    parcours.finEnonce()
  }

  // appelé à chaque répétition
  function resetSection () {
    j3pEmpty('correction')
    j3pEmpty('formules')
    cacheSolution()
    sq.numEssai = 1
    videEditeur()
    montreEditeur(true)
    sq.mtgAppLecteur.removeDoc('mtg32svg')
    sq.mtgAppLecteur.addDoc('mtg32svg', sq.txt, true)

    if (sq.param !== undefined) {
      const ch = 'abcdefghjklmnpqr'
      for (let i = 0; i < ch.length; i++) {
        const car = ch.charAt(i)
        if (sq.param.indexOf(car) !== -1) {
          const par = parcours.donneesSection[car]
          if (par !== 'random') sq.mtgAppLecteur.giveFormula2('mtg32svg', car, par)
        }
      }
      sq.mtgAppLecteur.calculate('mtg32svg', true) // true pour que les calculs alèatoires soient réinitialisés
    }

    if (sq.aleat) {
      for (let i = 1; i <= sq.nbParamAleat; i++) {
        sq.mtgAppLecteur.giveFormula2('mtg32svg', 'r' + i, sq['tab' + i][parcours.questionCourante - 1])
      }
    }

    sq.mtgAppLecteur.calculate('mtg32svg', true) // true pour que les calculs alèatoires soient réinitialisés
    ;['enonce', 'debut', 'formules', 'info'].forEach(j3pEmpty)
    sq.nbexp = 0
    sq.reponse = -1
    const nbParam = sq.nbLatex
    const t = ['a', 'b', 'c', 'd']
    const param = {}
    for (let i = 0; i < nbParam; i++) {
      const code = sq.mtgAppLecteur.getLatexCode('mtg32svg', i)
      if (i === 0) sq.aCalculer = code.replaceAll('\n', '')
      param[t[i]] = code
    }
    param.e = String(sq.nbEssais)
    const st = sq.consigne1 + (sq.simplifier ? sq.consigne3 : sq.consigne2) + sq.consigne4
    afficheMathliveDans('enonce', 'texte', st, param)
    if (sq.entete !== '') {
      afficheMathliveDans('debut', 'acalc', '$' + sq.entete + '$' + (sq.equation ? ' ' : ' = '), param)
    } else {
      afficheMathliveDans('debut', 'acalc', '$' + sq.aCalculer + '=$ ', param)
    }
    afficheNombreESsaisRestants()
    j3pElement('info').style.display = 'block'
    sq.mtgAppLecteur.display('mtg32svg') // Pour tout réafficher
    // Pour revenir en haut de page
    try {
      window.location.hash = 'MepMG'
      j3pElement('MepMG').scrollIntoView()
    } catch (e) {
      console.error(e)
    }
    j3pFocus(inputId)
    parcours.finEnonce()
  }

  switch (this.etat) {
    case 'enonce':
      // code exécuté au lancement de la section
      if (this.debutDeLaSection) {
        // Construction de la page
        parcours.construitStructurePage('presentation1bis')

        sq.nbEssais = parseInt(parcours.donneesSection.nbEssais)
        // compteur.
        sq.numEssai = 1
        sq.simplifier = parcours.donneesSection.simplifier ?? true
        // faut gérer sous-dossier ou pas…
        const sep = this.donneesSection.ex.includes('/') ? '-' : '/'
        j3pImporteAnnexe('squelettes-mtg32' + sep + this.donneesSection.ex + '.js')
          .then(initDatas)
          .catch(error => {
            console.error(error)
            j3pShowError('Impossible de charger les données de cette exercice')
          })
      } else {
        if (!sq.mtgAppLecteur) {
          j3pNotify('case enonce rappelé avant finEnonce')
          j3pShowError('Problème de chargement, il est encore en cours, c’est trop tôt pour rappeller un deuxième essai.')
          return
        }
        resetSection()
      }

      break // case "enonce":

    case 'correction': {
      let bilanReponse = ''
      let simplificationPossible = false
      if (validationEditeur()) {
        validation()
        if (sq.simplifier) {
          if (sq.resolu) {
            bilanReponse = 'exact'
          } else {
            if (sq.exact) {
              bilanReponse = 'exactPasFini'
              simplificationPossible = true
            } else {
              bilanReponse = 'faux'
            }
          }
        } else {
          if (sq.resolu || sq.exact) {
            if (sq.resolu) { bilanReponse = 'exact' } else {
              simplificationPossible = true
              if ((sq.exact) && sq.repMathQuill.contientIntegOuPrim) {
                bilanReponse = 'exactPasFini'
              } else { bilanReponse = 'exact' }
            }
          } else {
            bilanReponse = 'faux'
          }
        }
      } else {
        bilanReponse = 'incorrect'
      }
      // A cause de la limite de temps :
      if (this.isElapsed) { // limite de temps
        montreEditeur(false)
        j3pElement('correction').style.color = this.styles.cfaux
        j3pElement('boutonrecopier').style.display = 'none'
        j3pElement('info').style.display = 'none'
        if (sq.indicationfaute) {
          sq.mtgAppLecteur.giveFormula2('mtg32svg', 'faute', '0')
          sq.mtgAppLecteur.calculate('mtg32svg', false)
          sq.mtgAppLecteur.executeMacro('mtg32svg', 'masquerFaute')
        }
        afficheSolution(false)
        sq.mtgAppLecteur.executeMacro('mtg32svg', 'solution')
        j3pElement('correction').innerHTML = tempsDepasse
        j3pElement('correction').innerHTML += '<br>La solution se trouve ci-contre.'
        return this.finCorrection('navigation', true)
      }

      if (bilanReponse === 'incorrect') {
        if (getMathliveValue(inputId) !== '') {
          j3pElement('correction').style.color = this.styles.cfaux
          j3pElement('correction').innerHTML = sq.signeEgalManquant ? 'Il faut utiliser le signe = ' : 'Réponse incorrecte'
        } // else pas de saisie on fait rien
        return this.finCorrection() // on reste dans l’état correction
      }

      // Une réponse a été saisie
      // Bonne réponse
      if (bilanReponse === 'exact') {
        sq.mtgAppLecteur.setActive('mtg32svg', false)
        this.score++
        j3pElement('correction').style.color = this.styles.cbien
        if (simplificationPossible) {
          j3pElement('correction').innerHTML = sq.cbienmais
        } else {
          j3pElement('correction').innerHTML = cBien
        }
        j3pElement('boutonrecopier').style.display = 'none'
        j3pElement('info').style.display = 'none'
        if (sq.indicationfaute) {
          sq.mtgAppLecteur.executeMacro('mtg32svg', 'masquerFaute')
        }
        afficheSolution(true)
        sq.mtgAppLecteur.executeMacro('mtg32svg', 'solution')
        montreEditeur(false)
        sq.numEssai++ // Pour un affichage correct dans afficheReponse
        afficheReponse('exact', false)
        return this.finCorrection('navigation', true)
      }

      // pas exact
      sq.numEssai++
      if (bilanReponse === 'exactPasFini') {
        j3pElement('correction').style.color =
                (sq.numEssai <= sq.nbEssais) ? '#0000FF' : this.styles.cfaux
        j3pElement('correction').innerHTML = 'Le calcul est bon mais pas écrit sous la forme demandée.'
      } else {
        j3pElement('correction').style.color = this.styles.cfaux
        j3pElement('correction').innerHTML = cFaux
      }
      j3pEmpty('info')
      if (sq.numEssai <= sq.nbEssais) {
        afficheNombreESsaisRestants()
        videEditeur()
        afficheReponse(bilanReponse, false)
        focusIfExists(inputId)
        // Il reste des chances, on reste dans l’état correction (en attendant le prochain clic sur Valider)
        j3pElement('correction').innerHTML += '<br>' + '<br>' + essaieEncore
        return this.finCorrection()
      }

      // Erreur au dernier essai
      sq.mtgAppLecteur.setActive('mtg32svg', false)
      j3pElement('boutonrecopier').style.display = 'none'
      j3pElement('info').style.display = 'none'
      if (sq.indicationfaute) {
        sq.mtgAppLecteur.executeMacro('mtg32svg', 'masquerFaute')
      }
      afficheSolution(false)
      sq.mtgAppLecteur.executeMacro('mtg32svg', 'solution')
      montreEditeur(false)
      afficheReponse(bilanReponse, true)
      j3pElement('correction').innerHTML += '<br><br>La solution se trouve ci-contre.'
      this.finCorrection('navigation', true)
      break // case "correction":
    }

    case 'navigation':
      if (!this.sectionTerminee()) {
        this.etat = 'enonce'
      }
      this.finNavigation()
      break // case "navigation":
  }
}
