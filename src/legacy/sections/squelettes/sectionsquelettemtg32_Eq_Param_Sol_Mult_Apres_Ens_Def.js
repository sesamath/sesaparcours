import $ from 'jquery'
import { MathfieldElement, renderMathInDocument } from 'mathlive'

import { j3pAddElt, j3pAjouteBouton, j3pDiv, j3pElement, j3pEmpty, j3pImporteAnnexe, j3pPaletteMathquill, j3pShowError } from 'src/legacy/core/functions'
import { unLatexify } from 'src/lib/mathquill/functions'
import { unLatexifyAndAddImplicitMult } from 'src/legacy/sections/squelettes/helpersMtg'
import { getMtgApp } from 'src/lib/outils/mathgraph'
import { j3pCreeSVG } from 'src/legacy/core/functionsSvg'
import { setMathliveCss } from 'src/lib/outils/mathlive/mathlivecss'
import { cleanLatexForMl, mathliveRestrict } from 'src/lib/outils/mathlive/utils'
import { afficheMathliveDans, getMathliveValue } from 'src/lib/outils/mathlive/display'
import textesGeneriques from 'src/lib/core/textes'
import { focusIfExists } from 'src/lib/utils/dom/main'

const { cBien, cFaux, essaieEncore, tempsDepasse } = textesGeneriques

/*
  Yves Biton
  2019
  squelettemtg32_Eq_Param_Sol_Mult_Apres_Ens_Def
  Squelette demandant de calculer une expression ou plusieurs expressions
  On peut éventuellement spécifier des paramètres  a,b,c,d,e,f,g,h,j,k,n,n,p.
  Utilise une fichier annexe dont le nom est contenu dans le paramètre ex.
  Ce fichier annexe contient dans la variable txt le code Base 64 de la figure mtg32 associée et dans la variable param
  le nom des calculs à une lettre qui peuvent être paramétrés par l’utilisateur à choisir parmi les lettres abcdefghijklmnopq.
  Ce fichier annexe contient une variable nbCalc qui contient le nombre d’expressions que l’élève doit calculer simultanément.
  Le fichier annexe contient deux variables chaînes formulaire1 et formulaire2 contenant la question posée à l’étape 1 ou 2
  Chacune des ces variables contient la sous-chaîne edit qui correspondra à l’éditeur (pour l’ensemble de définition
  à l’étape 1 et pour les solutions à l’étape 2).
  Il contient aussi les variables btnFrac, btnPuis, btnRac, btnExp, btnLn, btnPi, btnSin, btnCos, btnTan dont me nom est suffixé
  par le numéro de l’étape (1 ou 2) correspondant aux boutons permis pour l’éditeur mathquill à l’étape 1 ou 2.
  Pour l’étape 1, la figure mtg32 contenue dans le fichier annexe doit contenir les éléments suivants :
  Si l’ensemble des solutions contient -∞, la figure doit contenir un calcul nommé moinsInfSol qui vaut 1 et 0 sinon.
  Si l’ensemble des solutions contient +∞, la figure doit contenir un calcul nommé plusInfSol qui vaut 1 et 0 sinon.
  Si l’ensemble des solutions est R la figure doit contenir un calcul nommé toutReelSol qui vaut 1 et 0 sinon.
  Si l’ensemble des solutions est vide la figure doit contenir un calcul nommé vide qui vaut 1 et 0 sinon
  La figure doit comprendre un calcul nommé xTest et un calcul nommé estSolution qui prend pour valeur 1 si xTest est solution
  de l’inéquation et zéro sinon.
  Elle doit contenir une fonction nommée zeroBorne de la variable x qui a comme formule abs(x)<0.00000000001 (10^-11)
  Elle doit contenir une fonction de la variable x nommée rep qui correspond à la réponse proposée par l’élève.
  Elle doit contenir une fonction de la variable x nommée repPourBornes qui correspond à la réponse proposée par l’élève avec un test supplémentaire pour les bornes fermées
  proposées par l’élève.
  Elle doit contenir une fonction nommée repBornesFermees qui correspond à la réponse proposée par l’élève mais avec toutes les bornes fermées
  Elle doit comprendre un calcul nommé estBorne qui vaut 1 si xTest est une borne de l’ensemble des solutions ou 0 sinon.
  Elle doit aussi comprendre un calcul nommé repContientSol qui vaut 1 si l’ensemble des solutions contient celui qui a été entré
  Elle doit ontenir un calcul nommé resolu qui vaut 1 si la fonction rep est exactement équivalente à la bonne réponse attendue
  et un calcul nommé presqueResolu si la réponse de l’élève n’est pas la bonne mais si repPourBornes est équivalent à la solution
  mais avec toutes les bornes fermées (c’est-à-dire s’il y a une erreur sur les bornes de l’intervalle).
  Elle doit contenir une fonction nommée fonctionTest d’une ou plusieurs variables dont une au moins a pour nom x
  et un calcul nommé contientBorne qui sera chargé de dire si une des bornes finies de l’ensembe des soluions est comprise
  dans un intervalle contenant au calcul qui sera mis par le squelette dans la fonction fonctionTest.
  Imaginons pas exemple que l’ensemble des solutions est [1;2] U ]3;+∞[
  Une première fois, le squelette donnera comme formule à fonctionTest "x>1&x<2" et la seconde fois "x>3".
  Le  calcul contientBorne contiendra alors comme formule "fonctionTest(1)|fonctionTest(2)|fonctionTest(3)"
  Les bornes isolées ne doivent pas être prises en compte dans contientBorne. Si, par exemple, l’ensemble des solutions
  est [1;2[U]2;+∞[, contientBorne devra contenir comme formule "fonctionTest(1)".
  Pour vérification de la réponse de l’élève à l’étape 2 :
  Pour savoir quel est le nombre de solution on interroge la valeur du calcul nbSol de la figure (O s’il n’y a pas de solution).
  Pour savoir si une réponse est exacte, c’est-à-dire contient une formule du type x= ou =x donnant une des solutions,
  on met sa formule dans le calcul rep et on interroge, après recalcul de la figure, la valeur
  du calcul exact2 qui vaut 0 si la formule proposée ne correspond pas à une des solutions et sinon renvoie le n° correspondant de la solution (de 1 à nbsol).
  Pour savoir si une réponse est exacte et bien écrite sous la forme demandée, on interroge la valeur du calcul resolu2 qui vaut 0
  Si le calcul ne correspond pas à une forme attendue et sinon renvoie le n° correspondant de la solution (de 1 à nbsol).
  L’élève peut aussi entrer des équations intermédiaires. Si, par exemple, nbSol vaut 2, la figure mtg32 doit contenir un calcul nommé racine1
  qui vaut 1 si la première solution est racine de l’équation proposée et 0 sinon et un calcul racine2 contenant 1 si la deuxième solution
  est racine de l’équation proposée et 0 sinon. Une réponse intermédiaire (avec des équations séparées par des ;) est acceptée si toutes les
  solutions de l’équation sont racines d’au moins une des équations proposées entre les;

  Si la figure contient un paramètre nbvar , alors dans le cas ou nbrepetitions > 1, on donne pour i compris entre 1 et nbvar
  au calcul de la figure nommé "r"+i (par exemple r1 et r2 si nbParamAleat vaut 2) des valeurs comprises entre 0 et "nbcas"+i - 1
  toutes différentes (sauf si nbrepetitions > b=nbcas auquel cas au bout de nbcas répétitions il y aura nécessairement une valeur de r déjà prise.

*/

// on définit le dossier des fonts mathlive (il faut le faire très tôt,
// ici c’est bon avec pnpm start mais trop tard pour un build (cf src/lib/player/index.js de la branche draftPlayer)
if (MathfieldElement) { // undefined dans les tests avec happy-dom
  MathfieldElement.fontsDirectory = '/static/mathlive/fonts'
  MathfieldElement.soundsDirectory = '/static/mathlive/sounds'
}

export const params = {
  /**
   * Ce qui est paramétrable dans l’éditeur de graphe
   * @type {LegacySectionParamsParametres}
   */
  parametres: [
    ['nbrepetitions', 1, 'entier', 'aucun'],
    ['limite', 0, 'entier', 'Temps disponible par répétition, en secondes'],
    ['nbEssais1', 3, 'entier', 'Nombre d’essais maximum autorisés pour le premier calcul'],
    ['nbEssais2', 6, 'entier', 'Nombre d’essais maximum autorisés pour le deuxième calcul'],
    ['coefEtape1', '0.5', 'string', 'Nombre strictement compris entre 0 et 1 à rajouter au score\npour une réussite à l’étape 1'],
    ['simplifier1', true, 'boolean', 'true si on exige un forme donnée pour le premier calcul, false sinon'],
    ['simplifier2', true, 'boolean', 'true si on exige un forme donnée pour le deuxième calcul, false sinon'],
    ['indicationErreurCrochet', true, 'boolean', 'true si on indique une erreur de sens de crochet pour l’ensemble de définition'],
    ['a', 'random', 'string', 'Valeur de a'],
    ['b', 'random', 'string', 'Valeur de b'],
    ['c', 'random', 'string', 'Valeur de c'],
    ['d', 'random', 'string', 'Valeur de d'],
    ['e', 'random', 'string', 'Valeur de e'],
    ['f', 'random', 'string', 'Valeur de f'],
    ['g', 'random', 'string', 'Valeur de g'],
    ['h', 'random', 'string', 'Valeur de h'],
    ['j', 'random', 'string', 'Valeur de j'],
    ['k', 'random', 'string', 'Valeur de k'],
    ['l', 'random', 'string', 'Valeur de l'],
    ['m', 'random', 'string', 'Valeur de m'],
    ['n', 'random', 'string', 'Valeur de n'],
    ['p', 'random', 'string', 'Valeur de p'],
    ['q', 'random', 'string', 'Valeur de q'],
    ['r', 'random', 'string', 'Valeur de r'],
    ['ex', 'terminale/Ln_Equation_Apres_Ens_Def_5', 'string', 'Nom du fichier annexe (xxx => squelettes-mtg32/xxx.js et xxx/yyy => squelettes-mtg32-xxx/yyy.js)']
  ]
}

const inputId = 'expressioninputmq1'

/**
 * section squelettemtg32_Eq_Param_Sol_Mult_Apres_Ens_Def
 * @this {Parcours}
 */
export default function main () {
  const parcours = this
  const sq = this.storage

  function onkeyup (ev) {
    if (sq.marked) demarqueEditeurPourErreur()
    if (ev.keyCode === 13) { // Touche Enter
      if (!validationEditeur()) marqueEditeurPourErreur()
    }
  }

  /**
   * Fonction extrayant d’un affichage LaTeX de la figure les lignes de texte et les transformant
   * en une chaîne affichable par MathQuill
   * @param tagLatex : le tag de l’affichage LaTeX de la figure
   * @return {string}
   */
  function extraitDeLatexPourMathlive (tagLatex) {
    const list = sq.mtgAppLecteur.getList('mtg32svg')
    const latex = list.getByTag(tagLatex)
    if (latex === null || latex.className !== 'CLatex') return ''
    return cleanLatexForMl(latex.chaineLatex)
  }

  function traiteMathlive (ch) {
    // La première ligne est nécessaire pour cette section
    ch = ch.replace(/\\R/g, '_R').replace(/\\emptyset/g, '∅')
    // Mathlive ne met pas d’accolades pour les fractions dont le numérateur et le dénominateur sont des entiers
    ch = ch.replace(/\\frac(\d)(\d)/g, '\\frac{$1' + '}{' + '$2}')
    // Ni pour les racines carrées d’un entier
    ch = ch.replace(/\\sqrt(\d)/g, '\\sqrt{$1}')
    // Pour les puissances dont l’exposant est un chiffre, Mathlive ne met pas d’accolades
    ch = ch.replace(/\^([^{])/g, '^{$1}')
    ch = unLatexify(ch)
    // mathlive peut mettre des {} vides
    ch = ch.replace(/\\{\\}/g, '')
    ch = ch.replace(/\^(\d)(\d)/g, '^$1*$2') // On peut avoir par exemple 2^35^4 pour 2^3*5^4
    return sq.mtgAppLecteur.addImplicitMult('mtg32svg', ch)// Traitement des multiplications implicites
  }

  function marqueEditeurPourErreur () {
    sq.marked = true
    const mf = j3pElement(inputId)
    if (!mf) return
    mf.style.backgroundColor = '#FF9999'
    mf.demarqueErreur = () => {
      if (sq.marked) {
        demarqueEditeurPourErreur()
      }
    }
  }

  function demarqueEditeurPourErreur () {
    sq.marked = false
    j3pEmpty('correction')
    const mf = j3pElement(inputId)
    if (!mf) return
    mf.style.backgroundColor = 'white'
  }

  /**
   * Fonction affichant la solution s’il existe un affichage LaTeX de tag solution
   * @param{boolean} bexact true si l’élève a entré la bonne réponse, false sinon
   */
  function afficheSolution (bexact) {
    if (sq.solution) {
      sq.mtgAppLecteur.executeMacro('mtg32svg', 'solution')
      $('#divSolution').css('display', 'block').html('')
      const ch = extraitDeLatexPourMathlive('solution')
      if (ch !== '') {
        const styles = parcours.styles
        afficheMathliveDans('divSolution', 'solution', ch, { style: { color: bexact ? styles.cbien : styles.colorCorrection } })
      }
    }
  }

  function cacheSolution () {
    $('#divSolution').css('display', 'none').html('')
  }

  function afficheNombreEssaisRestants () {
    j3pEmpty('info')
    const nbe = parcours.donneesSection['nbEssais' + sq.etape] - sq.numEssai + 1
    if (nbe === 1) afficheMathliveDans('info', 'texteinfo', 'Il reste un essai.', { style: { color: '#7F007F' } })
    else afficheMathliveDans('info', 'texteinfo', 'Il reste ' + nbe + ' essais.', { style: { color: '#7F007F' } })
  }

  /**
   * Fonction renvoyant un tableau formé de deux tableaux réordonnés suivant les valeurs du premier tableau
   * Les deux tableaux doivent avoir la même dimension
   * @param tabval
   * @param tabfor
   */
  function ordonne (tabval, tabfor) {
    let i, j
    const tabvalaux = []
    const tabforaux = []
    const tabvalres = []
    const tabforres = []
    for (i = 0; i < tabval.length; i++) {
      tabvalaux.push(tabval[i])
      tabforaux.push(tabfor[i])
    }
    while (tabvalaux.length >= 1) {
      let valmin = tabvalaux[0]
      let indmin = 0
      for (j = 1; j < tabvalaux.length; j++) {
        if (tabvalaux[j] < valmin) {
          indmin = j
          valmin = tabvalaux[j]
        }
      }
      const val = tabvalaux[indmin]
      const form = tabforaux[indmin]
      tabvalres.push(val)
      tabforres.push(form)
      tabvalaux.splice(indmin, 1)
      tabforaux.splice(indmin, 1)
    }
    return [tabvalres, tabforres]
  }

  function validationEnsembleSol (ch) {
    let i, j
    const listePourCalc = sq.listePourCalc
    sq.reponseContientR = false // Sera true si un des intervalles de la réunion est ]-∞;+∞[
    if (ch === '_R' || ch === ']-∞;+∞[') {
      sq.reponseEstR = true
      return true
    } else sq.reponseEstR = false
    if (ch.charAt(ch.length - 1) === '|') return false
    if (ch.indexOf('[∞') !== -1 || ch.indexOf('∞]') !== -1 || ch.indexOf('[-∞') !== -1) return false
    sq.bornesGauches = []
    sq.valBornesGauches = []
    sq.bornesGauchesFermees = []
    sq.bornesDroites = []
    sq.valBornesDroites = []
    sq.bornesDroitesFermees = []
    sq.bornesGauchesInf = []
    sq.bornesDroitesInf = []
    // On regarde s la réponse a été entrée sous la forme R-{}
    if (ch.indexOf('_R-\\(') === 0) {
      const indpf = parentheseFermante(ch, 5)
      if (indpf < ch.length - 1) return false // On ne permet que R-{} sans autre opérateur
      const chaux = ch.substring(5, indpf)
      if (chaux === '') return false
      const tabcal = []
      const tabval = []
      const tab = chaux.split(';')
      for (i = 0; i < tab.length; i++) {
        const ch2 = tab[i]
        if (ch2 === '') return -1
        if (!listePourCalc.verifieSyntaxe(ch2)) return false
        listePourCalc.giveFormula2('x', ch2)
        listePourCalc.calculateNG()
        tabcal.push(ch2)
        tabval.push(listePourCalc.valueOf('x'))
      }
      // On ordonne les valeurs trouvées entre les accolades
      const res = ordonne(tabval, tabcal)
      const tabvalord = res[0]
      const tabforord = res[1]
      // On regarde s’il, y a deux valeurs ou plus répétées. Si oui on refuse
      for (i = 0; i < tabvalord.length; i++) {
        for (j = i + 1; j < tabvalord.length - 1; j++) {
          if (tabvalord[i] === tabvalord[j]) return false
        }
      }
      for (i = 0; i < tabvalord.length; i++) {
        if (i === 0) { // Intervalle de - ∞ à la plus petite valeur
          sq.bornesGauchesInf.push(true)
          sq.bornesGauches.push('0') // Valeur sans importance
          sq.bornesGauchesFermees.push(false)
          sq.valBornesGauches.push(0)
          sq.bornesDroitesInf.push(false)
          sq.bornesDroitesFermees.push(false)
          sq.bornesDroites.push(tabforord[0])
          sq.valBornesDroites.push(tabvalord[0])
        } else {
          sq.bornesGauchesInf.push(false)
          sq.bornesGauches.push(tabforord[i - 1])
          sq.bornesGauchesFermees.push(false)
          sq.valBornesGauches.push(tabvalord[i - 1])
          sq.bornesDroitesInf.push(false)
          sq.bornesDroitesFermees.push(false)
          sq.bornesDroites.push(tabforord[i])
          sq.valBornesDroites.push(tabvalord[i])
        }
        if (i === tabvalord.length - 1) {
          sq.bornesGauchesInf.push(false)
          sq.bornesGauches.push(tabforord[i])
          sq.bornesGauchesFermees.push(false)
          sq.valBornesGauches.push(tabvalord[i])
          sq.bornesDroitesInf.push(true)
          sq.bornesDroitesFermees.push(false)
          sq.bornesDroites.push('0') // Valeru sans importance
          sq.valBornesDroites.push(0)
        }
      }
    } else {
      let ind = 0
      while (ind !== -1 && ind <= ch.length - 1) {
        ind = validationIntervalle(ch, ind)
        if (ind === -1) return false
      }
    }
    // On crée maintenant une chaîne de caractères dont le texte est celui d’une fonction mtg32 testant si un x est solution
    let st = ''
    for (i = 0; i < sq.bornesGauches.length; i++) {
      if (i !== 0) st += '|'
      if (sq.bornesGauchesInf[i]) {
        if (sq.bornesDroitesInf[i]) st += '(0=0)' // N’importe quel test vrai fait l’affaire
        else {
          st += '(' + sq.bornesDroites[i]
          st += (sq.bornesDroitesFermees[i] ? '>=' : '>') + 'x)'
        }
      } else {
        if (sq.bornesDroitesInf[i]) {
          st += '(' + sq.bornesGauches[i]
          st += (sq.bornesGauchesFermees[i] ? '<=' : '<') + 'x)'
        } else {
          if (sq.bornesGauchesFermees[i] && sq.bornesDroitesFermees[i] && sq.bornesGauches[i] === sq.bornesDroites[i]) {
            st += '(x=' + sq.bornesGauches[i] + ')'
          } else {
            st += '(' + sq.bornesGauches[i]
            st += (sq.bornesGauchesFermees[i] ? '<=' : '<') + 'x)&('
            st += sq.bornesDroites[i]
            st += (sq.bornesDroitesFermees[i] ? '>=' : '>') + 'x)'
          }
        }
      }
    }
    sq.chaineFonctionTest = st
    sq.chaineFonctionTestBornesFermees = st.replace(/<([^=])/g, '<=$1').replace(/>([^=])/g, '>=$1') // Pour tester une réponse presque exacte avec une confusion sur les bornes
    // Une autre chaîne qui sera attribuée à repPourBornes de la figure
    st = ''
    for (i = 0; i < sq.bornesGauches.length; i++) {
      if (sq.bornesGauchesFermees[i] && sq.bornesDroitesFermees[i] && sq.bornesGauches[i] === sq.bornesDroites[i]) {
        st += '(x=' + sq.bornesGauches[i] + ')'
      } else {
        if (!sq.bornesGauchesInf[i]) {
          st += '(' + sq.bornesGauches[i]
          st += (sq.bornesGauchesFermees[i] ? '<=' : '<') + 'x)&('
        } else st += '('
        if (!sq.bornesDroitesInf[i]) {
          st += sq.bornesDroites[i]
          st += (sq.bornesDroitesFermees[i] ? '>=' : '>') + 'x)'
        } else st += '0=0)'
      }
      if (sq.bornesGauchesFermees[i]) st += '|' + 'zeroBorne(x - (' + sq.bornesGauches[i] + '))'
      if (sq.bornesDroitesFermees[i]) st += '|' + 'zeroBorne(x - (' + sq.bornesDroites[i] + '))'
      if (i !== sq.bornesGauches.length - 1) st += '|'
    }
    sq.chaineFonctionTestPourBornes = st
    return true
  }

  function parentheseFermante (ch, inddeb) {
    let i = inddeb
    let somme = 1
    while (i < ch.length) {
      const car = ch.charAt(i)
      if (car === '\\') return -1 // Pas d’accolades imbriquées
      if (car === '(') somme++
      else {
        if (car === ')') somme--
      }
      if (somme === 0) return i
      else i++
    }
    return -1
  }

  function validationIntervalle (ch, indeb) {
    let indcr
    let valGauche
    let valDroite
    const listePourCalc = sq.listePourCalc
    const car = ch.charAt(indeb)
    if (car !== '[' && car !== ']' && car !== '\\') return -1
    const crochet1 = ch.charAt(indeb)
    if (crochet1 === '\\') { // Traitement des valeurs isolées entre accolades
      indcr = parentheseFermante(ch, indeb + 2) // Le \ est forcéménet suivi d’une (
      if (indcr === -1) return -1
      const st = ch.substring(indeb + 2, indcr)
      const tab = st.split(';')
      for (let j = 0; j < tab.length; j++) {
        const ch2 = tab[j]
        if (ch2 === '') return -1
        if (!listePourCalc.verifieSyntaxe(ch2)) return -1
        sq.bornesGauches.push(ch2)
        listePourCalc.giveFormula2('x', ch2)
        listePourCalc.calculateNG()
        valGauche = listePourCalc.valueOf('x')
        sq.valBornesGauches.push(valGauche)
        sq.valBornesDroites.push(valGauche)
        sq.bornesDroites.push(ch2)
        sq.bornesGauchesFermees.push(true)
        sq.bornesGauchesInf.push(false)
        sq.bornesDroitesFermees.push(true)
        sq.bornesDroitesInf.push(false)
      }
    } else {
      const indpv = ch.indexOf(';', indeb + 1)
      const ind1 = ch.indexOf('[', indeb + 1)
      const ind2 = ch.indexOf(']', indeb + 1)
      if (indpv === -1 || (ind1 === -1 && ind2 === -1)) return -1
      if (ind1 === -1) indcr = ind2
      else if (ind2 === -1) indcr = ind1
      else indcr = Math.min(ind1, ind2)
      if (indcr < indpv) return -1
      const crochet2 = ch.charAt(indcr)
      const left = ch.substring(indeb + 1, indpv)
      const right = ch.substring(indpv + 1, indcr)
      if (left === '' || right === '') return -1
      sq.bornesGauches.push(left)
      sq.bornesDroites.push(right)
      const borneGaucheInf = left === '-∞'
      const borneDroiteInf = right === '+∞'
      sq.bornesGauchesInf.push(borneGaucheInf)
      sq.bornesDroitesInf.push(borneDroiteInf)
      if (borneGaucheInf && borneDroiteInf) sq.reponseContientR = true
      if ((!borneGaucheInf && !listePourCalc.verifieSyntaxe(left)) || (!borneDroiteInf && !listePourCalc.verifieSyntaxe(right))) return -1
      if (!borneGaucheInf) {
        listePourCalc.giveFormula2('x', left)
        listePourCalc.calculateNG()
        valGauche = listePourCalc.valueOf('x')
      }
      if (!borneDroiteInf) {
        listePourCalc.giveFormula2('x', right)
        listePourCalc.calculateNG()
        valDroite = listePourCalc.valueOf('x')
      }
      if ((!borneGaucheInf && !borneDroiteInf) && (valGauche > valDroite)) return -1
      sq.valBornesGauches.push(valGauche)
      sq.valBornesDroites.push(valDroite)
      sq.bornesGauchesFermees.push(!borneGaucheInf && (crochet1 === '['))
      sq.bornesDroitesFermees.push(!borneDroiteInf && (crochet2 === ']'))
    }
    if (indcr < ch.length - 1) {
      if (ch.charAt(indcr + 1) !== '|') return -1
      else return indcr + 2
    } else return indcr + 1
  }

  /**
   * Fonction renvoyant true, si, à 10^-10 près, le nombre xtest est une des bornes fermées de l’intervalla
   * ou la réunion d’intervalles proposées par l’élève
   * @param xtest
   */
  function estBorne (xtest) {
    const eps = 0.0000000001
    for (let i = 0; i < sq.bornesGauches.length; i++) {
      if ((!sq.bornesGauchesInf[i] && ((sq.bornesGauchesFermees[i] && Math.abs(xtest - sq.valBornesGauches[i]) <= eps))) ||
        (!sq.bornesDroitesInf[i] && (sq.bornesDroitesFermees[i] && Math.abs(xtest - sq.valBornesDroites[i]) <= eps))) return true
    }
    return false
  }

  function reponseIncluseDansSol () {
    let xTest
    let xTest2
    let res, estBorneIsolee
    const eps = 0.0000000001
    const eps2 = 0.00000001
    res = sq.mtgAppLecteur.valueOf('mtg32svg', 'toutReelSol')
    if (res === 1) return true
    // On commence par regarder si un des intervalles de la réunion proposée contient une borne limitante
    // de l’intervalle solution.
    // Pour cela on donne à la fonction fonctionTest de la figure une formule et on regarde si cette formule
    // est vérifiée par une borne limitante de la figure (hors valeur exclue isolée) en interrogeant
    // la valeur du calcul contientBorne de la figure
    for (let i = 0; i < sq.bornesGauches.length; i++) {
      let form = ''
      if (!sq.bornesGauchesInf[i]) form += sq.valBornesGauches[i] + '<x'
      if (!sq.bornesDroitesInf[i]) {
        if (form !== '') form += '&'
        form += 'x<' + sq.valBornesDroites[i]
      }
      if (form !== '') {
        sq.mtgAppLecteur.giveFormula2('mtg32svg', 'fonctionTest', form)
        sq.mtgAppLecteur.calculate('mtg32svg', false)
        if (sq.mtgAppLecteur.valueOf('mtg32svg', 'contientBorne') === 1) return false
      }
    }

    for (let i = 0; i < sq.bornesGauches.length; i++) {
      // On étudie le cas d’une valeur isolée représentée par un intervalle dont les bornes sont confondues
      if (sq.bornesGauchesFermees[i] && sq.bornesDroitesFermees[i] && (sq.valBornesGauches[i] === sq.valBornesDroites[i])) {
        xTest = sq.valBornesGauches[i]
        sq.mtgAppLecteur.giveFormula2('mtg32svg', 'xTest', xTest.toFixed(16))
        sq.mtgAppLecteur.calculate('mtg32svg', false)
        if ((sq.mtgAppLecteur.valueOf('mtg32svg', 'estBorne') !== 1) &&
          (sq.mtgAppLecteur.valueOf('mtg32svg', 'estSolution') !== 1)) return false
      } else {
        if (sq.bornesGauchesInf[i]) {
          res = sq.mtgAppLecteur.valueOf('mtg32svg', 'moinsInfSolution')
          if (res !== 1) return false
        } else {
          const gauche = sq.valBornesGauches[i]
          if (!sq.bornesGauchesFermees[i]) {
            if (!appartientAuMoinsUnInt(gauche)) {
              xTest = gauche
              sq.mtgAppLecteur.giveFormula2('mtg32svg', 'xTest', xTest.toFixed(16))
              sq.mtgAppLecteur.calculate('mtg32svg', false)
              res = sq.mtgAppLecteur.valueOf('mtg32svg', 'estBorne')
              if (res === 1) return false
              estBorneIsolee = (sq.mtgAppLecteur.valueOf('mtg32svg', 'estBorneIsolee') === 1)
              if (!estBorneIsolee) {
                xTest2 = gauche - eps
                sq.mtgAppLecteur.giveFormula2('mtg32svg', 'xTest', xTest2.toFixed(16))
                sq.mtgAppLecteur.calculate('mtg32svg', false)
                res = sq.mtgAppLecteur.valueOf('mtg32svg', 'estSolution')
                if (res === 1 && !estBorneFermee(xTest)) return false
              }
            }
          }
          if (estBorne(gauche)) xTest = gauche; else xTest = gauche + eps
          sq.mtgAppLecteur.giveFormula2('mtg32svg', 'xTest', xTest.toFixed(16))
          sq.mtgAppLecteur.calculate('mtg32svg', false)
          res = sq.mtgAppLecteur.valueOf('mtg32svg', 'estSolution')
          if (res !== 1) return false
          xTest = gauche - eps2
          if (!appartientAuMoinsUnInt(xTest)) {
            sq.mtgAppLecteur.giveFormula2('mtg32svg', 'xTest', xTest.toFixed(16))
            sq.mtgAppLecteur.calculate('mtg32svg', false)
            res = sq.mtgAppLecteur.valueOf('mtg32svg', 'estSolution')
            if (res === 1) return false
          }
        }
        if (sq.bornesDroitesInf[i]) {
          res = sq.mtgAppLecteur.valueOf('mtg32svg', 'plusInfSolution')
          if (res !== 1) return false
        } else {
          const droite = sq.valBornesDroites[i]
          if (!sq.bornesDroitesFermees[i]) {
            if (!appartientAuMoinsUnInt(droite)) {
              xTest = droite
              sq.mtgAppLecteur.giveFormula2('mtg32svg', 'xTest', xTest.toFixed(16))
              sq.mtgAppLecteur.calculate('mtg32svg', false)
              res = sq.mtgAppLecteur.valueOf('mtg32svg', 'estBorne')
              if (res === 1) return false
              estBorneIsolee = (sq.mtgAppLecteur.valueOf('mtg32svg', 'estBorneIsolee') === 1)
              if (!estBorneIsolee) {
                xTest2 = droite + eps
                sq.mtgAppLecteur.giveFormula2('mtg32svg', 'xTest', xTest2.toFixed(16))
                sq.mtgAppLecteur.calculate('mtg32svg', false)
                res = sq.mtgAppLecteur.valueOf('mtg32svg', 'estSolution')
                if (res === 1 && !estBorneFermee(xTest)) return false
              }
            }
          }
          if (estBorne(droite)) xTest = droite; else xTest = droite - eps
          sq.mtgAppLecteur.giveFormula2('mtg32svg', 'xTest', xTest.toFixed(16))
          sq.mtgAppLecteur.calculate('mtg32svg', false)
          res = sq.mtgAppLecteur.valueOf('mtg32svg', 'estSolution')
          if (res !== 1) return false
          xTest = droite + eps2
          if (!appartientAuMoinsUnInt(xTest)) {
            sq.mtgAppLecteur.giveFormula2('mtg32svg', 'xTest', xTest.toFixed(16))
            sq.mtgAppLecteur.calculate('mtg32svg', false)
            res = sq.mtgAppLecteur.valueOf('mtg32svg', 'estSolution')
            if (res === 1) return false
          }
        }
      }
    }
    return true
  }

  function appartientAuMoinsUnInt (x) {
    if (sq.reponseContientR) return true
    for (let i = 0; i < sq.bornesGauches.length; i++) {
      if (sq.bornesGauchesInf[i]) {
        if (sq.bornesDroitesInf[i]) return true
        else {
          if (sq.bornesDroitesFermees[i]) {
            if (x <= sq.valBornesDroites[i]) return true
          } else {
            if (x < sq.valBornesDroites[i]) return true
          }
        }
      } else {
        if (sq.bornesDroitesInf[i]) {
          if (sq.bornesGauchesFermees[i]) {
            if (x >= sq.valBornesGauches[i]) return true
          } else {
            if (x > sq.valBornesGauches[i]) return true
          }
        } else {
          if (sq.bornesGauchesFermees[i]) {
            if (sq.bornesDroitesFermees[i]) {
              if ((x >= sq.valBornesGauches[i]) && (x <= sq.valBornesDroites[i])) return true
            } else {
              if ((x >= sq.valBornesGauches[i]) && (x < sq.valBornesDroites[i])) return true
            }
          } else {
            if (sq.bornesDroitesFermees[i]) {
              if ((x > sq.valBornesGauches[i]) && (x <= sq.valBornesDroites[i])) return true
            } else {
              if ((x > sq.valBornesGauches[i]) && (x < sq.valBornesDroites[i])) return true
            }
          }
        }
      }
    }
    return false
  }

  function estBorneFermee (x) {
    for (let i = 0; i < sq.bornesGauches.length; i++) {
      if (!sq.bornesGauchesInf[i] && sq.bornesGauches[i] === x) return true
      if (!sq.bornesDroitesInf[i] && sq.bornesDroites[i] === x) return true
    }
  }

  function validationEditeur () {
    let res = true; let valide; let chcalcul
    let rep = getMathliveValue(inputId)
    if (rep === '') {
      marqueEditeurPourErreur()
      return false
    }
    if (sq.etape === 1) {
      sq.vide = false
      chcalcul = traiteMathlive(rep)
      if (chcalcul.indexOf('∅') !== -1) {
        res = chcalcul.length === 1
        sq.vide = res
      } else {
        valide = validationEnsembleSol(chcalcul)
        if (!valide) res = false
      }
      if (!res) marqueEditeurPourErreur()
    } else {
      sq.signeEgalOublie = false
      rep = getMathliveValue(inputId)
      const tab = rep.split(';')
      for (let i = 0; (i < tab.length) && res; i++) {
        if (tab[i] === '') {
          res = false
        } else {
          chcalcul = traiteMathlive(tab[i])
          if (chcalcul === '∅') res = true
          else {
            if (chcalcul.indexOf('=') === -1) {
              res = false
              sq.signeEgalOublie = true
            } else {
              valide = sq.mtgAppLecteur.syntaxValidation('mtg32svg', 'rep', chcalcul, true)
              if (!valide) res = false
            }
          }
        }
      }
    }
    if (!res) {
      marqueEditeurPourErreur()
      focusIfExists(inputId)
    }
    return res
  }

  function montreEditeur (bVisible) {
    if (bVisible) $('#info').css('display', 'block')
    $('#editeur').css('display', bVisible ? 'block' : 'none')
    if (sq.boutonsMathQuill) $('#boutonsmathquill').css('display', bVisible ? 'block' : 'none')
    // On vide le contenu de l’éditeur MathQuill et on lui donne le focus.
    j3pElement(inputId).value = ''
    focusIfExists(inputId)
  }

  function videEditeur () {
    j3pElement(inputId).value = ''
  }

  function creeEditeur () {
    let i, code, formulaire
    if (sq.etape === 1) {
      formulaire = sq['formulaire' + sq.etape]
      formulaire = formulaire.replace('edit', '&1&')
    } else formulaire = 'Réponse proposée : &1&'
    const t = 'abcdefghijklmnopq'
    const nbParam = parseInt(sq.nbLatex)
    const obj = {}
    sq.paramFormulaire = {}
    for (i = 0; i < nbParam; i++) {
      code = sq.mtgAppLecteur.getLatexCode('mtg32svg', i)
      obj[t.charAt(i)] = code
      sq.paramFormulaire[t[i]] = code
    }
    obj.charset = sq['charset' + sq.etape]
    // On affecte un indice à chaque éditeur ainsi qu’un écouteur de focus
    let liste = []
    const etape = sq.etape
    if (sq['btnFrac' + etape]) liste.push('fraction')
    if (sq['btnPuis' + etape]) liste.push('puissance')
    if (sq['btnRac' + etape]) liste.push('racine')
    if (sq['btnExp' + etape]) liste.push('exp')
    if (sq['btnLn' + etape]) liste.push('ln')
    if (sq['btnPi' + etape]) liste.push('pi')
    if (sq['btnSin' + etape]) liste.push('sin')
    if (sq['btnCos' + etape]) liste.push('cos')
    if (sq['btnTan' + etape]) liste.push('tan')
    // liste.push(';') // Pas accepté par le clavier virtuel
    if (etape === 1) {
      liste.push('inf')
      liste.push('bracket')
      // liste.push('[') // Les crochets ne sont pas des commandes : exclues des commandes du clavier virtuel
      // liste.push(']')
      liste.push('union')
      liste.push('R')
    }
    liste.push('vide')
    sq.listeBoutons = liste
    sq.boutonsMathQuill = liste.length !== 0
    obj.listeBoutons = sq.listeBoutons
    afficheMathliveDans('editeur', 'expression', formulaire, obj) // sq.formulaire contient ou plusieurs éditeurs MathQuill
    if (sq['charset' + sq.etape] !== '') {
      mathliveRestrict(inputId, sq['charset' + sq.etape])
    }
    j3pElement(inputId).onkeyup = function (ev) {
      onkeyup.call(this, ev)
    }

    j3pEmpty('boutonsmathquill')
    // On ajoute les boutons qui ne peuvent pas être dans le clavier virtuel
    if (sq.etape === 1) liste = liste.concat([';', '[', ']'])
    j3pPaletteMathquill('boutonsmathquill', inputId, {
      liste,
      nomdiv: 'palette'
    })
    // Ligne suivante pour un bon alignement
    $('#palette').css('display', 'inline-block')
    focusIfExists(inputId)
  }

  function afficheReponse (bilan, depasse) {
    let ch, coul, idrep, num
    num = sq.numEssai
    idrep = 'exp' + num
    if (num >= 2) afficheMathliveDans('formules', idrep + 'br', '<br>')
    if (sq.etape === 1) {
      if (bilan === 'exact') {
        ch = 'Réponse exacte : '
        coul = parcours.styles.cbien
      } else {
        if (bilan === 'faux') {
          ch = 'Réponse fausse : '
          coul = parcours.styles.cfaux
        } else { // Exact mais pas fini
          ch = 'Exact pas fini : '
          if (depasse) coul = parcours.styles.cfaux
          else coul = '#0000FF'
        }
      }
      afficheMathliveDans(sq.etape === 1 ? 'formules' : 'formulesSuite', idrep + 'deb', ch, {
        style: {
          color: coul
        }
      })
      ch = sq['formulaire' + sq.etape]
      const st = '\\textcolor{' + (sq.resolu ? parcours.styles.cbien : (sq.exact ? '#0000FF' : parcours.styles.cfaux)) + '}{'
      ch = ch.replace('edit', '$' + st + sq.rep + '}$')
      afficheMathliveDans('formules', idrep + String(sq.etape), ch, sq.paramFormulaire)
    } else {
      if ((bilan === 'exact')) {
        ch = 'Exact : '
        coul = parcours.styles.cbien
      } else {
        if (bilan === 'correct') {
          ch = 'Vérifié par la ou les ' + sq.nomSolutions + ' : '
          coul = depasse ? parcours.styles.cfaux : '#0000FF'
        } else {
          if (bilan === 'faux') {
            ch = 'Faux : '
            coul = parcours.styles.cfaux
          } else { // exact pas fini
            ch = 'Exact pas fini : '
            coul = depasse ? parcours.styles.cfaux : '#0000FF'
          }
        }
      }
      num = sq.numEssai
      if (num > 2) ch = '<br>' + ch
      idrep = 'exp' + num
      afficheMathliveDans('formulesSuite', idrep, ch + '$' + sq.rep + '$', {
        style: {
          color: coul
        }
      })
    }
  }

  function validation () {
    let j, chcalcul, k, ind

    function tabContient (tab, ent) {
      for (let i = 1; i <= tab.length; i++) {
        if (tab[i - 1] === ent) return true
      }
      return false
    }

    if (sq.numEssai >= 1) {
      j3pElement('boutonrecopier').style.display = 'block'
    }
    if (sq.etape === 1) {
      sq.rep = getMathliveValue(inputId).replace(/\\mathbb\{R}/g, '\\R')
      const vide = sq.mtgAppLecteur.valueOf('mtg32svg', 'vide')
      if (sq.vide) {
        if (vide) {
          sq.resolu = true
        } else {
          sq.resolu = false
          sq.exact = false
        }
      } else {
        const toutReelSol = sq.mtgAppLecteur.valueOf('mtg32svg', 'toutReelSol') === 1
        if (toutReelSol) {
          if (sq.reponseEstR) sq.resolu = true
          else {
            if (sq.reponseContientR) {
              sq.exact = true
              sq.resolu = false
            } else {
              sq.exact = false
              sq.resolu = false
            }
          }
        } else {
          if (sq.reponseEstR || sq.reponseContientR) {
            sq.resolu = false
            sq.exact = false
          } else {
            // On regarde d’abord pour chaque borne des intervalles entrés :
            // Si l’intervalle est fermé en cette borne ou en une autre borne équivalente, si la borne est solution de l’inéquation
            // Sinon si un nombre très proche de cette borne et dans l’intervalle est solution.
            sq.mtgAppLecteur.giveFormula2('mtg32svg', 'rep', sq.chaineFonctionTest)
            sq.mtgAppLecteur.giveFormula2('mtg32svg', 'repPourBornes', sq.chaineFonctionTestPourBornes)
            sq.mtgAppLecteur.giveFormula2('mtg32svg', 'repBornesFermees', sq.chaineFonctionTestBornesFermees)
            sq.mtgAppLecteur.calculate('mtg32svg', false)
            sq.presqueResolu = sq.mtgAppLecteur.valueOf('mtg32svg', 'presqueResolu') === 1
            if (!reponseIncluseDansSol()) {
              sq.exact = false
              sq.resolu = false
            } else {
              if (sq.mtgAppLecteur.valueOf('mtg32svg', 'resolu' + sq.etape) === 1) {
                sq.resolu = true
              } else {
                sq.resolu = false
                sq.exact = sq.mtgAppLecteur.valueOf('mtg32svg', 'repContientSol') === 1
              }
            }
          }
        }
      }
    } else {
      if (sq.rep === '∅') {
        if (sq.mtgAppLecteur.valueOf('nbSol') === 0) sq.resolu = true
        else {
          sq.resolu = false
          sq.exact = false
          sq.correct = false
        }
      } else {
        sq.rep = [] // Tableau destiné à recevoir les réponses dans les champs d’édition des solutions
        sq.reponseExacte = []
        sq.reponseResolue = []
        sq.reponseCorrecte = []
        const rep = getMathliveValue(inputId)
        sq.rep = rep
        const tab = rep.split(';')
        sq.correct = true
        for (j = 0; j < tab.length; j++) {
          chcalcul = traiteMathlive(tab[j])
          sq.mtgAppLecteur.giveFormula2('mtg32svg', 'rep', chcalcul)
          sq.mtgAppLecteur.calculate('mtg32svg', false)
          let auMoinsUneCorrecte = false
          for (k = 1; k <= sq.nbSol; k++) {
            const estrac = sq.mtgAppLecteur.valueOf('mtg32svg', 'racine' + k)
            auMoinsUneCorrecte = auMoinsUneCorrecte || (estrac === 1)
            sq.reponseCorrecte.push(estrac === 1 ? k : 0)
          }
          sq.correct = sq.correct && auMoinsUneCorrecte
        }
        if (sq.correct) {
          for (ind = 1; ind <= sq.nbSol; ind++) {
            sq.correct = sq.correct && tabContient(sq.reponseCorrecte, ind)
          }
        }
        if (tab.length !== sq.nbSol) {
          sq.exact = false
          sq.resolu = false
        } else {
          for (j = 0; j < tab.length; j++) {
            chcalcul = unLatexifyAndAddImplicitMult(sq.mtgAppLecteur, tab[j])
            sq.mtgAppLecteur.giveFormula2('mtg32svg', 'rep', chcalcul)
            sq.mtgAppLecteur.calculate('mtg32svg', false)
            sq.reponseExacte.push(sq.mtgAppLecteur.valueOf('mtg32svg', 'exact2'))
            sq.reponseResolue.push(sq.mtgAppLecteur.valueOf('mtg32svg', 'resolu2'))
          }
          sq.exact = true
          sq.resolu = true
          for (ind = 1; ind <= sq.nbSol; ind++) {
            sq.exact = sq.exact && tabContient(sq.reponseExacte, ind)
            sq.resolu = sq.resolu && tabContient(sq.reponseResolue, ind)
          }
        }
      }
    }
  }

  function recopierReponse () {
    demarqueEditeurPourErreur()
    j3pElement(inputId).value = sq.rep
    focusIfExists(inputId)
  }

  function etapeSuivante () {
    sq.etape = 2
    j3pEmpty('editeur')
    j3pEmpty('enonce')
    j3pEmpty('info')
    const st = sq.consigneSuite1 + (sq['simplifier' + sq.etape] ? sq.consigneSuite3 : sq.consigneSuite2) + sq.consigneSuite4
    sq.parametres.e = sq['nbEssais' + sq.etape]
    afficheMathliveDans('enonceSuite', 'texte', st, sq.parametres)
    sq.numEssai = 1
    afficheNombreEssaisRestants()
    creeEditeur()
    montreEditeur(true)
  }

  function initMtg () {
    setMathliveCss('MepMG')
    const mtgOptions = {
      // player only, sans figure initiale ni svg, on gère ça dans initMtg et modif_fig
      loadCoreWithMathJax: true
    }
    getMtgApp('mtg32svg', {}, mtgOptions).then((mtgAppLecteur) => {
      sq.mtgAppLecteur = mtgAppLecteur
      initDom()
      sq.listePourCalc = sq.mtgAppLecteur.createList('TWF0aEdyYXBoSmF2YTEuMAAAABI+TMzNAAJmcv###wEA#wEAAAAAAAAAAAQsAAACvQAAAQEAAAAAAAAAAQAAAAP#####AAAAAQAKQ0NhbGNDb25zdAD#####AAJwaQAWMy4xNDE1OTI2NTM1ODk3OTMyMzg0Nv####8AAAABAApDQ29uc3RhbnRlQAkh+1RELRj#####AAAAAQAHQ0NhbGN1bAD#####AAFlAAZleHAoMSn#####AAAAAgAJQ0ZvbmN0aW9uBwAAAAE#8AAAAAAAAAAAAAIA#####wABeAABMAAAAAEAAAAAAAAAAP###############w==')
      let code, par, car, i, j, nbrep, ar, tir, nb
      sq.mtgAppLecteur.removeAllDoc()
      sq.mtgAppLecteur.addDoc('mtg32svg', sq.txt, true)
      sq.mtgAppLecteur.calculate('mtg32svg', false)
      const ch = 'abcdefghjklmnpqr'
      const nbvar = sq.mtgAppLecteur.valueOf('mtg32svg', 'nbvar')
      if (nbvar !== -1) {
        nbrep = parcours.donneesSection.nbrepetitions
        sq.aleat = true
        sq.nbParamAleat = nbvar
        for (i = 1; i <= nbvar; i++) {
          const nbcas = sq.mtgAppLecteur.valueOf('mtg32svg', 'nbcas' + i)
          nb = Math.max(nbrep, nbcas)
          ar = []
          for (j = 0; j < nb; j++) ar.push(j % nbcas)
          sq['tab' + i] = []
          for (let k = 0; k < nbrep; k++) {
            tir = Math.floor(Math.random() * ar.length)
            sq['tab' + i].push(ar[tir])
            ar.splice(tir, 1)
          }
          sq.mtgAppLecteur.giveFormula2('mtg32svg', 'r' + i, sq['tab' + i][0])
        }
      } else sq.aleat = false

      if (sq.param !== undefined) {
        for (i = 0; i < ch.length; i++) {
          car = ch.charAt(i)
          if (sq.param.indexOf(car) !== -1) {
            par = parcours.donneesSection[car]
            if (par !== 'random') sq.mtgAppLecteur.giveFormula2('mtg32svg', car, par)
          }
        }
      }
      sq.mtgAppLecteur.calculateAndDisplayAll(true)
      sq.nbSol = sq.mtgAppLecteur.valueOf('mtg32svg', 'nbSol') // Le nombre de solutions de l’équation.
      const nbParam = parseInt(sq.nbLatex)
      const t = 'abcdefghijklmnopq'
      const param = {}
      for (i = 0; i < nbParam; i++) {
        code = sq.mtgAppLecteur.getLatexCode('mtg32svg', i)
        param[t.charAt(i)] = code
      }
      sq.parametres = param // Mémorisation pour l’étape 2
      const st = sq.consigne1 + (sq['simplifier' + sq.etape] ? sq.consigne3 : sq.consigne2) + sq.consigne4
      afficheMathliveDans('enonce', 'texte', st, param)
      creeEditeur()
      parcours.finEnonce()
    }).catch(j3pShowError)
  }

  function initDom () {
    renderMathInDocument({ renderAccessibleContent: '' })
    if (sq.titre) parcours.afficheTitre(sq.titre)
    j3pDiv(parcours.zones.MG, {
      id: 'conteneur',
      contenu: '',
      style: parcours.styles.etendre('toutpetit.enonce', { padding: '6px' })
    })
    j3pDiv('conteneur', 'enonce', '')
    j3pDiv('conteneur', {
      id: 'formules',
      contenu: '',
      style: parcours.styles.petit.enonce
    }) // Contient le formules entrées par l’élève
    j3pDiv('conteneur', 'enonceSuite', '')
    j3pDiv('conteneur', {
      id: 'formulesSuite',
      contenu: '',
      style: parcours.styles.petit.enonce
    }) // Contient le formules entrées par l’élève

    j3pDiv('conteneur', 'info', '')
    afficheNombreEssaisRestants()
    j3pDiv('conteneur', 'conteneurbouton', '')
    j3pAjouteBouton('conteneurbouton', 'boutonrecopier', 'MepBoutonsRepere', 'Recopier réponse précédente',
      recopierReponse)
    j3pElement('boutonrecopier').style.display = 'none'
    j3pDiv('conteneur', 'editeur', '') // Le div qui contiendra la chaine avec le ou les éditeurs MathQuill
    $('#editeur').css('padding-top', '10px') // Pour laisser un peu de place au-dessus de l’éditeur
    $('#editeur').css('font-size', '24px')
    j3pDiv('conteneur', 'boutonsmathquill', '')
    $('#boutonsmathquill').css('display', 'inline-block').css('padding-top', '10px') // Pour laisser un peu de place au-dessus des boutons
    j3pDiv('conteneur', 'divSolution', '')
    $('#divSolution').css('display', 'none')
    j3pDiv('conteneur', 'divmtg32', '')
    j3pCreeSVG('divmtg32', {
      id: 'mtg32svg',
      width: sq.width,
      height: sq.height
    })
    const style = parcours.styles.petit.correction
    style.marginTop = '1.5em'
    style.marginLeft = '0.5em'
    j3pAddElt(parcours.zonesElts.MD, 'div', '', { id: 'correction', style })
  }

  switch (this.etat) {
    case 'enonce':
      // code exécuté au lancement de la section
      if (this.debutDeLaSection) {
        // Construction de la page
        parcours.construitStructurePage('presentation1bis')

        // compteur.
        sq.numEssai = 1
        sq.etape = 1
        sq.nbEssais1 = parseInt(parcours.donneesSection.nbEssais1)
        sq.nbEssais2 = parseInt(parcours.donneesSection.nbEssais2)
        sq.coefEtape1 = parseFloat(parcours.donneesSection.coefEtape1)
        sq.simplifier1 = parcours.donneesSection.simplifier1
        if (sq.simplifier1 === undefined) sq.simplifier1 = true
        sq.simplifier2 = parcours.donneesSection.simplifier2
        if (sq.simplifier2 === undefined) sq.simplifier2 = true
        sq.indicationErreurCrochet = parcours.donneesSection.indicationErreurCrochet
        // faut gérer sous-dossier ou pas…
        const sep = this.donneesSection.ex.includes('/') ? '-' : '/'
        j3pImporteAnnexe('squelettes-mtg32' + sep + this.donneesSection.ex + '.js').then(function (datasSection) {
          // console.log("initsection");
          // Attention, le this devient window...
          sq.txt = datasSection.txt
          // Préparation des boutons pour chaque étape
          sq.nbLatex = Number(datasSection.nbLatex ?? 0) // Le nombre de paramètres LaTeX dans le texte
          for (let etape = 1; etape <= 2; etape++) {
            ;['btnPuis', 'btnFrac'].forEach((p) => {
              sq[p + etape] = Boolean(datasSection[p + etape] ?? true)
            })
            ;['btnPi', 'btnRac', 'btnExp', 'btnLn', 'btnSin', 'btnCos', 'btnTan', 'btnAbs'].forEach((p) => {
              sq[p + etape] = Boolean(datasSection[p + etape] ?? false)
            })
          }
          sq.consigne1 = datasSection.consigne1 ?? ''
          sq.consigne2 = datasSection.consigne2 ?? ''
          sq.consigne3 = datasSection.consigne3 ?? ''
          sq.consigne4 = datasSection.consigne4 ?? ''
          sq.titre = datasSection.titre ?? ''
          sq.width = Number(datasSection.width ?? 700)
          sq.height = Number(datasSection.height ?? 900)
          sq.nomSolutions = datasSection.nomSolutions ?? 'solutions'
          sq.nbCalc1 = datasSection.nbCalc1
          sq.formulaire1 = datasSection.formulaire1 // Contient la chaine avec l’éditeur pour l’étape 1.
          sq.param = datasSection.param ?? '' // Chaine contenant les paramètres autorisés
          sq.consigneSuite1 = datasSection.consigneSuite1 ?? ''
          sq.consigneSuite2 = datasSection.consigneSuite2 ?? ''
          sq.consigneSuite3 = datasSection.consigneSuite3 ?? ''
          sq.consigneSuite4 = datasSection.consigneSuite4 ?? ''
          sq.nbCalc2 = datasSection.nbCalc2
          sq.formulaire2 = datasSection.formulaire2 // Contient la chaine avec l’éditeur pour l’étape 2.
          sq.charset1 = datasSection.charset1 ?? ''
          sq.charset2 = datasSection.charset2 ?? ''
          sq.marked = []
          for (let k = 0; k < sq.nbCalc; k++) {
            sq.marked[k] = false
          }
          initMtg()
        }).catch(error => {
          console.error(error)
          j3pShowError('Impossible de charger les données de cette exercice')
        })
        // return;
      } else {
        j3pEmpty('correction')
        cacheSolution()
        sq.numEssai = 1
        sq.etape = 1
        videEditeur()
        montreEditeur(true)
        sq.mtgAppLecteur.removeDoc('mtg32svg')
        sq.mtgAppLecteur.addDoc('mtg32svg', sq.txt, true)
        if (sq.aleat) {
          for (let i = 1; i <= sq.nbParamAleat; i++) {
            sq.mtgAppLecteur.giveFormula2('mtg32svg', 'r' + i, sq['tab' + i][this.questionCourante - 1])
          }
        }
        if (sq.param !== undefined) {
          const ch = 'abcdefghjklmnpqr'
          for (let i = 0; i < ch.length; i++) {
            const car = ch.charAt(i)
            if (sq.param.indexOf(car) !== -1) {
              const par = parcours.donneesSection[car]
              if (par !== 'random') sq.mtgAppLecteur.giveFormula2('mtg32svg', car, par)
            }
          }
        }
        sq.mtgAppLecteur.calculate('mtg32svg', true) // true pour que les calculs aléatoires soient réinitialisés
        sq.nbSol = sq.mtgAppLecteur.valueOf('mtg32svg', 'nbSol') // Le nombre de solutions de l’équation.
        j3pEmpty('enonce')
        j3pEmpty('formules')
        j3pEmpty('info')
        j3pEmpty('enonceSuite')
        j3pEmpty('formulesSuite')

        const nbParam = parseInt(sq.nbLatex)
        const t = ['a', 'b', 'c', 'd', 'e', 'f', 'g', 'h', 'i', 'j', 'k', 'l', 'm', 'n', 'o', 'p', 'q']
        const param = {}
        for (let i = 0; i < nbParam; i++) {
          param[t[i]] = sq.mtgAppLecteur.getLatexCode('mtg32svg', i)
        }
        sq.parametres = param
        const st = sq.consigne1 + (sq.simplifier1 ? sq.consigne3 : sq.consigne2) + sq.consigne4
        afficheMathliveDans('enonce', 'texte', st, param)
        afficheNombreEssaisRestants()
        j3pElement('info').style.display = 'block'

        j3pEmpty('editeur')
        creeEditeur()
        sq.mtgAppLecteur.display('mtg32svg') // Pour tout réafficher
        // Pour revenir en haut de page
        try {
          window.location.hash = 'MepMG'
          j3pElement('MepMG').scrollIntoView()
        } catch (e) {
          console.warn(e) // pas normal mais pas grave
        }
        focusIfExists(inputId)
        this.finEnonce()
      }

      break // case "enonce":

    case 'correction': {
      let bilanReponse = ''
      let simplificationPossible = false
      if (validationEditeur()) {
        validation()
        if (sq.etape === 1) {
          if (sq.simplifier1) {
            if (sq.resolu) bilanReponse = 'exact'
            else {
              if (sq.exact) {
                bilanReponse = 'exactPasFini'
                simplificationPossible = true
              } else bilanReponse = 'faux'
            }
          } else {
            if (sq.resolu || sq.exact) {
              bilanReponse = 'exact'
              if (!sq.resolu) simplificationPossible = true
            } else bilanReponse = 'faux'
          }
        } else {
          if (sq.simplifier2) {
            if (sq.resolu) bilanReponse = 'exact'
            else {
              if (sq.exact) {
                bilanReponse = 'exactPasFini'
                simplificationPossible = true
              } else {
                if (sq.correct) bilanReponse = 'correct'
                else bilanReponse = 'faux'
              }
            }
          } else {
            if (sq.resolu || sq.exact) {
              bilanReponse = 'exact'
              if (!sq.resolu) simplificationPossible = true
            } else {
              if (sq.correct) bilanReponse = 'correct'
              else bilanReponse = 'faux'
            }
          }
        }
      } else {
        bilanReponse = 'incorrect'
      }
      // A cause de la limite de temps :
      if (this.isElapsed) { // limite de temps
        this._stopTimer()
        montreEditeur(false)
        j3pElement('correction').style.color = this.styles.cfaux
        j3pElement('boutonrecopier').style.display = 'none'
        j3pElement('info').style.display = 'none'

        sq.mtgAppLecteur.executeMacro('mtg32svg', 'solution')
        afficheSolution(false)
        j3pElement('correction').innerHTML = tempsDepasse
        j3pElement('correction').innerHTML += '<br>La solution se trouve ci-contre.'
        this.etat = 'navigation'
        this.sectionCourante()
      } else {
        if (bilanReponse === 'incorrect') {
          j3pElement('correction').style.color = this.styles.cfaux
          j3pElement('correction').innerHTML = 'Réponse incorrecte'
          this.afficheBoutonValider()
        } else {
          // Une réponse a été saisie
          // Bonne réponse
          if (bilanReponse === 'exact') {
            sq.mtgAppLecteur.setActive('mtg32svg', false)
            this.score += sq.etape === 1 ? sq.coefEtape1 : (1 - sq.coefEtape1)
            j3pElement('correction').style.color = this.styles.cbien
            let mess = simplificationPossible ? 'C’est bien mais on pouvait  simplifier la réponse' : cBien
            if (sq.etape === 1) mess += '<br>On passe à la question suivante.'
            j3pElement('correction').innerHTML = mess
            montreEditeur(false)
            sq.numEssai++ // Pour un affichage correct dans afficheReponse
            afficheReponse('exact', false)
            if (sq.etape === 2) {
              this._stopTimer()
              j3pElement('boutonrecopier').style.display = 'none'
              j3pElement('info').style.display = 'none'
              sq.mtgAppLecteur.executeMacro('mtg32svg', 'solution')
              afficheSolution(true)
              this.etat = 'navigation'
              this.sectionCourante()
            } else {
              etapeSuivante()
            }
          } else {
            sq.numEssai++
            if (bilanReponse === 'exactPasFini') {
              j3pElement('correction').style.color =
                (sq.numEssai <= this.donneesSection['nbEssais' + sq.etape]) ? '#0000FF' : this.styles.cfaux
              j3pElement('correction').innerHTML = 'Le calcul est bon mais pas écrit sous la forme demandée.'
            } else {
              if (sq.etape === 1) {
                j3pElement('correction').style.color = this.styles.cfaux
                if (sq.indicationErreurCrochet && sq.presqueResolu) {
                  j3pElement('correction').innerHTML = 'La réponse est fausse à cause d’un problème de crochets'
                } else j3pElement('correction').innerHTML = cFaux
              } else {
                if (bilanReponse === 'faux') {
                  j3pElement('correction').style.color = this.styles.cfaux
                  j3pElement('correction').innerHTML = cFaux
                }
              }
            }
            j3pEmpty('info')
            afficheReponse(bilanReponse, false)
            if (sq.numEssai <= this.donneesSection['nbEssais' + sq.etape]) {
              afficheNombreEssaisRestants()
              videEditeur()
              // S’il y a plus de deux chances,on appelle à nouveau le bouton Valider
              j3pElement('correction').innerHTML += '<br>' + '<br>' + essaieEncore
              focusIfExists(inputId)
            } else {
              // Erreur au nème essai
              if (sq.etape === 2) {
                this._stopTimer()
                sq.mtgAppLecteur.setActive('mtg32svg', false)
                montreEditeur(false)
                j3pElement('boutonrecopier').style.display = 'none'
                j3pElement('info').style.display = 'none'
                sq.mtgAppLecteur.executeMacro('mtg32svg', 'solution')
                afficheSolution(false)
                afficheReponse(bilanReponse, true)
                j3pElement('correction').innerHTML += '<br><br>La solution se trouve ci-contre.'
                this.etat = 'navigation'
                this.sectionCourante()
              } else {
                j3pElement('correction').innerHTML += '<br><br>On passe à la question suivante.'
                etapeSuivante()
              }
            }
          }
        }
      }
      // Obligatoire
      this.finCorrection()
      break // case "correction":
    }

    case 'navigation':
      if (!this.sectionTerminee()) {
        this.etat = 'enonce'
      }
      this.finNavigation()
      break // case "navigation":
  }
}
