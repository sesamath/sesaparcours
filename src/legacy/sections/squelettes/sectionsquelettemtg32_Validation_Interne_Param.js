import { j3pAddElt, j3pDetruit, j3pDiv, j3pElement, j3pEmpty, j3pImporteAnnexe, j3pShowError } from 'src/legacy/core/functions'
import { j3pAffiche } from 'src/lib/mathquill/functions'
import { getMtgApp } from 'src/lib/outils/mathgraph'
import { j3pCreeSVG } from 'src/legacy/core/functionsSvg'
import textesGeneriques from 'src/lib/core/textes'

const { cBien, cFaux, essaieEncore, tempsDepasse } = textesGeneriques

/**
 Yves Biton
 Mars 2019

 squelettemtg32_Validation_Interne_Param
 Squelette demandant d’agir sur une figure pour répondre à une question.
 On peut éventuellement spécifier des paramètres  a,b,c,d,e,f,g,h,j,k,n,n,p.
 ex est la référence à un fichier annexe qui comprend entre autres une figure mtg32 chargée de proposée l’exercice et de vérifier la validité des solutions proposées
 Pour savoir la réponse est exacte, on interroge la valeur reponse qui vaut 1 si l’exercice a été résolu et 0 sinon.
 Si la figure contenue dans ex contient un paramètre nbvar , alors dans le cas ou nbrepetitions > 1, on donne pour i compris entre 1 et nbvar
 au calcul de la figure nommé "r"+i (par exemple r1 et r2 si nbParamAleat vaut 2) des valeurs comprises entre 0 et "nbcas"+i - 1
 toutes différentes (sauf si nbrepetitions > b=nbcas auquel cas au bout de nbcas répétitions il y aura nécessairement une valeur de r déjà prise.
 */

export const params = {
  outils: ['mathquill'],
  /**
   * Ce qui est paramétrable dans l’éditeur de graphe
   * @type {LegacySectionParamsParametres}
   */
  parametres: [
    ['nbrepetitions', 1, 'entier', 'aucun'],
    ['limite', 0, 'entier', 'Temps disponible par répétition, en secondes'],
    ['nbEssais', 4, 'entier', 'Nombre d’essais maximum autorisés'],
    ['infoParam', 'Information sur le rôle des paramètres', 'string', 'Information sur l’utilisation des paramètres suivants'],
    ['a', 'random', 'string', 'Valeur de a'],
    ['b', 'random', 'string', 'Valeur de b'],
    ['c', 'random', 'string', 'Valeur de c'],
    ['d', 'random', 'string', 'Valeur de d'],
    ['e', 'random', 'string', 'Valeur de e'],
    ['f', 'random', 'string', 'Valeur de f'],
    ['g', 'random', 'string', 'Valeur de g'],
    ['h', 'random', 'string', 'Valeur de h'],
    ['j', 'random', 'string', 'Valeur de j'],
    ['k', 'random', 'string', 'Valeur de k'],
    ['l', 'random', 'string', 'Valeur de l'],
    ['m', 'random', 'string', 'Valeur de m'],
    ['n', 'random', 'string', 'Valeur de n'],
    ['p', 'random', 'string', 'Valeur de p'],
    ['q', 'random', 'string', 'Valeur de q'],
    ['r', 'random', 'string', 'Valeur de r'],
    ['x', 'random', 'string', 'Valeur de x'],
    ['y', 'random', 'string', 'Valeur de y'],
    ['ex', 'College_Placer_Nombre_Sur_Axe_Niv3', 'string', 'Nom du fichier annexe (xxx => squelettes-mtg32/xxx.js et xxx/yyy => squelettes-mtg32-xxx/yyy.js)']
  ]
}

/**
 * section squelettemtg32_Validation_Interne_Param
 * @this {Parcours}
 */
export default function main () {
  const parcours = this
  const sq = this.storage
  let i, code
  sq.validation = function () {
    sq.resolu = sq.mtgAppLecteur.valueOf('mtg32svg', 'reponse') === 1
  }

  function afficheNombreEssaisRestants () {
    j3pEmpty('divinfo')
    const nbe = sq.nbEssais - sq.numEssai + 1
    if (nbe === 1) {
      j3pAffiche('divInfo', 'texteinfo', 'Il reste un essai.', { style: { color: '#7F007F' } })
    } else {
      j3pAffiche('divInfo', 'texteinfo', 'Il reste ' + nbe + ' essais.', { style: { color: '#7F007F' } })
    }
  }

  function initMtg () {
    const mtgOptions = {
      // player only, sans figure initiale ni svg, on gère ça dans initMtg et modif_fig
      loadCoreWithMathJax: true
    }
    getMtgApp('mtg32svg', {}, mtgOptions).then((mtgAppLecteur) => {
      sq.mtgAppLecteur = mtgAppLecteur
      initDom()
      let par, car, i, j, nbrep, ar, tir, nb, nbcas, code
      // On crée provisoirement le DIV ci-dessous pour pouvoir calculer la figure.
      // On le détruira ensuite pour le récréer à la bonne position
      j3pDiv('conteneur', 'divmtg32', '')
      j3pCreeSVG('divmtg32', {
        id: 'mtg32svg',
        width: sq.width,
        height: sq.height
      })
      sq.mtgAppLecteur.removeAllDoc()
      // on ajoute la figure que le fichier annexe contient
      sq.mtgAppLecteur.addDoc('mtg32svg', sq.txt, false)
      sq.mtgAppLecteur.calculate('mtg32svg', true)
      const ch = 'abcdefghjklmnpqrxy'
      const nbvar = sq.mtgAppLecteur.valueOf('mtg32svg', 'nbvar')
      if (nbvar !== -1) {
        nbrep = parcours.donneesSection.nbrepetitions
        sq.aleat = true
        sq.nbParamAleat = nbvar
        for (i = 1; i <= nbvar; i++) {
          nbcas = sq.mtgAppLecteur.valueOf('mtg32svg', 'nbcas' + i)
          nb = Math.max(nbrep, nbcas)
          ar = []
          for (j = 0; j < nb; j++) ar.push(j % nbcas)
          sq['tab' + i] = []
          for (let k = 0; k < nbrep; k++) {
            tir = Math.floor(Math.random() * ar.length)
            sq['tab' + i].push(ar[tir])
            ar.splice(tir, 1)
          }
          sq.mtgAppLecteur.giveFormula2('mtg32svg', 'r' + i, sq['tab' + i][0])
        }
      } else {
        sq.aleat = false
      }

      if (sq.param) {
        for (i = 0; i < ch.length; i++) {
          car = ch.charAt(i)
          if (sq.param.indexOf(car) !== -1) {
            par = parcours.donneesSection[car]
            if (par !== 'random') sq.mtgAppLecteur.giveFormula2('mtg32svg', car, par)
          }
        }
      }
      sq.mtgAppLecteur.calculate('mtg32svg', true)
      sq.mtgAppLecteur.display('mtg32svg')

      const nbLatex = parseInt(sq.nbLatex)
      // La consigne peut contenir jusqu'à 4 paramètres a, b,c et d récupérés comme affichages LaTeX de la figure.
      const t = 'abcd'
      const param = {}
      for (let k = 0; k < nbLatex; k++) {
        code = sq.mtgAppLecteur.getLatexCode('mtg32svg', k)
        param[t.charAt(k)] = code
      }
      const st = sq.consigne1 + sq.consigne2 + sq.consigne3 + sq.consigne4
      j3pAffiche('divEnonce', 'texte', st, param)
      parcours.finEnonce()
    }).catch(j3pShowError)
  }

  function ajouteFigureSolution () {
    if (sq.figSol !== null) {
      let j, car, par
      const width = sq.widthSol
      const height = sq.heightSol
      j3pDiv('conteneur', 'divmtg32sol', '')
      j3pDiv('divmtg32sol', 'explication', '')
      // j3pElement("explication").style.color = color;
      j3pAffiche('explication', 'titreexp', 'Solution :\n', { style: { color: 'brown' } })
      j3pCreeSVG('divmtg32sol', { id: 'mtg32svgsol', width, height })
      sq.mtgAppLecteur.addDoc('mtg32svgsol', sq.figSol, false)
      // On remplace dans la figure de solution les paramètres par les valeurs correspondant dans la figure de texte
      const ch = 'abcdefghjklmnpqrxy'
      for (j = 0; j < ch.length; j++) {
        car = ch.charAt(j)
        if (sq.param.indexOf(car) !== -1) {
          par = sq.mtgAppLecteur.valueOf('mtg32svg', car)
          sq.mtgAppLecteur.giveFormula2('mtg32svgsol', car, par)
        }
      }
      sq.mtgAppLecteur.calculate('mtg32svgsol', true)
      sq.mtgAppLecteur.display('mtg32svgsol')
    }
  }

  function initDom () {
    if (sq.titre) parcours.afficheTitre(sq.titre)

    // Trois lignes suivantes déplacées ici depuis initMtg pour éviter que j3pAffiche("", "divInfo" ne soit appelé avant la création du div divInfo
    j3pDiv(parcours.zones.MG, {
      id: 'conteneur',
      contenu: '',
      style: parcours.styles.etendre('toutpetit.enonce', { padding: '6px' })
    })
    j3pDiv('conteneur', 'divEnonce', '')
    j3pDiv('conteneur', 'divInfo', '')
    const style = parcours.styles.petit.correction
    style.marginTop = '1.5em'
    style.marginLeft = '0.5em'
    j3pAddElt(parcours.zonesElts.MD, 'div', '', { id: 'correction', style })
    afficheNombreEssaisRestants()
  }

  function _Donnees () {
    // "primaire" || "lycee" || "college"
    // Actuellement les dimensions sont identiques
    this.typesection = 'lycee'//

    // Nombre de répétitions de l’exercice
    this.nbrepetitions = 1
    this.nbetapes = 1
    this.nbitems = this.nbetapes * this.nbrepetitions

    this.ex = 'College_Placer_Nombre_Sur_Axe_Niv3'
    // Si indication != "" alors un lien "Indication" s’affiche dans la zone Inférieure Gauche (this.zones.IG
    // une indication<br>de deux lignes...";
    this.indication = ''
    const st = 'abcdefghjklmnpqrxy'
    for (let i = 0; i < st.length; i++) this[st.charAt(i)] = 'random'
    this.nbEssais = 4
    this.simplifier = true

    this.textes = {}
    this.titre = 'Titre de l’activité'

    this.pe = 0
  }

  switch (this.etat) {
    case 'enonce':
      // code exécuté au lancement de la section
      if (this.debutDeLaSection) {
        this.donneesSection = new _Donnees()
        // Construction de la page
        this.construitStructurePage('presentation1bis')

        this.surcharge()

        // compteur. Si numEssai > nbEssais, on corrige
        sq.numEssai = 1
        sq.nbEssais = parseInt(parcours.donneesSection.nbEssais)
        // faut gérer sous-dossier ou pas…
        const sep = this.donneesSection.ex.includes('/') ? '-' : '/'
        j3pImporteAnnexe('squelettes-mtg32' + sep + this.donneesSection.ex + '.js').then(function (datasSection) {
          // console.log("initsection");
          // Attention, le this devient window...
          sq.txt = datasSection.txt

          sq.nomInconnue1 = datasSection.nomInconnue1
          sq.nomInconnue2 = datasSection.nomInconnue2
          sq.nbLatex = datasSection.nbLatex // Le nombre de paramètres LaTeX dans le texte
          if (datasSection.btnPuis !== undefined) sq.btnPuis = datasSection.btnPuis; else sq.btnPuis = true
          if (datasSection.btnFrac !== undefined) sq.btnFrac = datasSection.btnFrac; else sq.btnFrac = true
          if (datasSection.btnPi !== undefined) sq.btnPi = datasSection.btnPi; else sq.btnPi = false
          if (datasSection.btnRac !== undefined) sq.btnRac = datasSection.btnRac; else sq.btnRac = false
          if (datasSection.btnExp !== undefined) sq.btnExp = datasSection.btnExp; else sq.btnExp = false
          if (datasSection.btnLn !== undefined) sq.btnLn = datasSection.btnLn; else sq.btnLn = false
          sq.consigne1 = datasSection.consigne1 || ''
          sq.consigne2 = datasSection.consigne2 || ''
          sq.consigne3 = datasSection.consigne3 || ''
          sq.consigne4 = datasSection.consigne4 || ''
          sq.titre = datasSection.titre || 'Titre'
          sq.width = datasSection.width || 700
          sq.height = datasSection.height || 900
          sq.param = datasSection.param // Chaine contenant les paramètres autorisés
          sq.figSol = datasSection.figSol || null
          sq.widthSol = datasSection.widthSol || 700
          sq.heightSol = datasSection.heightSol || 900
          initMtg()
        }).catch(error => {
          console.error(error)
          j3pShowError('Impossible de charger les données de cette exercice')
        })
        // return;
      } else {
        j3pEmpty('correction')
        sq.numEssai = 1
        // On détruit la figure de correction précédente s’il y en avait une
        if (sq.figSol !== null) {
          sq.mtgAppLecteur.removeDoc('mtg32svgsol')
          const div = j3pElement('divmtg32sol')
          while (div.childNodes.length !== 0) div.removeChild(div.childNodes[0])
          j3pDetruit('divmtg32sol')
        }
        sq.mtgAppLecteur.removeDoc('mtg32svg')
        sq.mtgAppLecteur.addDoc('mtg32svg', sq.txt, false)
        sq.mtgAppLecteur.calculate('mtg32svg', true)
        if (sq.aleat) {
          for (i = 1; i <= sq.nbParamAleat; i++) {
            sq.mtgAppLecteur.giveFormula2('mtg32svg', 'r' + i, sq['tab' + i][this.questionCourante - 1])
          }
        }
        if (sq.param !== undefined) {
          const ch = 'abcdefghjklmnpqrxy'
          for (i = 0; i < ch.length; i++) {
            const car = ch.charAt(i)
            if (sq.param.indexOf(car) !== -1) {
              const par = parcours.donneesSection[car]
              if (par !== 'random') sq.mtgAppLecteur.giveFormula2('mtg32svg', car, par)
            }
          }
        }
        sq.mtgAppLecteur.calculate('mtg32svg', true)
        j3pEmpty('divEnonce')
        j3pElement('divInfo').style.display = 'block'
        j3pEmpty('divInfo')
        // La consigne peut contenir jusqu'à 4 paramètres a, b,c et d récupérés comme affichages LaTeX de la figure.
        const nbLatex = parseInt(sq.nbLatex)
        // La consigne peut contenir jusqu'à 4 paramètres a, b,c et d récupérés comme affichages LaTeX de la figure.
        const t = 'abcd'
        const param = {}
        for (let k = 0; k < nbLatex; k++) {
          code = sq.mtgAppLecteur.getLatexCode('mtg32svg', k)
          param[t.charAt(k)] = code
        }
        sq.mtgAppLecteur.display('mtg32svg')
        const st = sq.consigne1 + sq.consigne2 + sq.consigne3 + sq.consigne4
        j3pAffiche('divEnonce', 'texte', st, param)
        afficheNombreEssaisRestants()
        // Pour revenir en haut de page
        try {
          window.location.hash = 'MepMG'
          j3pElement('MepMG').scrollIntoView()
        } catch (e) {
          console.error(e)
        }
        this.finEnonce()
      }

      break // case "enonce":

    case 'correction':
      // A cause de la limite de temps :
      sq.validation()
      if (this.isElapsed) { // limite de temps
        this._stopTimer()
        j3pElement('correction').style.color = this.styles.cfaux
        j3pElement('divInfo').style.display = 'none'
        sq.mtgAppLecteur.executeMacro('mtg32svg', 'voirSolution')
        j3pElement('correction').innerHTML = tempsDepasse
        j3pElement('correction').innerHTML += '<br>La solution se trouve ci-contre.'
        ajouteFigureSolution()
        this.etat = 'navigation'
        this.sectionCourante()
      } else {
        sq.numEssai++
        // Bonne réponse
        if (sq.resolu) {
          this.score++
          this._stopTimer()
          sq.mtgAppLecteur.setActive('mtg32svg', false)
          j3pElement('correction').style.color = this.styles.cbien
          j3pElement('correction').innerHTML = cBien + '<br>Voir la solution ci-contre'
          j3pElement('divInfo').style.display = 'none'
          sq.mtgAppLecteur.executeMacro('mtg32svg', 'voirSolution')
          ajouteFigureSolution()
          this.etat = 'navigation'
          this.sectionCourante()
        } else {
          j3pElement('correction').style.color = this.styles.cfaux
          j3pElement('correction').innerHTML = cFaux
          j3pEmpty('divInfo')

          if (sq.numEssai <= sq.nbEssais) {
            afficheNombreEssaisRestants()
            j3pElement('correction').innerHTML += '<br>' + '<br>' + essaieEncore
          } else {
            // Erreur au nème essai
            this._stopTimer()
            sq.mtgAppLecteur.setActive('mtg32svg', false)
            j3pElement('divInfo').style.display = 'none'
            sq.mtgAppLecteur.executeMacro('mtg32svg', 'voirSolution')
            ajouteFigureSolution()
            j3pElement('correction').innerHTML += '<br><br>La solution se trouve ci-contre.'
            this.etat = 'navigation'
            this.sectionCourante()
          }
        }
      }
      // Obligatoire
      this.finCorrection()
      break // case "correction":

    case 'navigation':

      if (this.sectionTerminee()) {
        // On détermine la phrase d’état renvoyée par la section
        this.parcours.pe = this.score / this.donneesSection.nbitems
        this.afficheBoutonSectionSuivante()
        this.focus('sectioncontinuer')
      } else {
        this.etat = 'enonce'
        this.afficheBoutonSuite()
        this.focus('boutoncontinuer')
      }
      // Obligatoire
      this.finNavigation()

      break // case "navigation":
  }
}
