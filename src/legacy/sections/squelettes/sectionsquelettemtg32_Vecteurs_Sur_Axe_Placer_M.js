import { j3pAddElt, j3pDiv, j3pElement, j3pEmpty, j3pImporteAnnexe, j3pShowError } from 'src/legacy/core/functions'
import { j3pAffiche } from 'src/lib/mathquill/functions'
import { getMtgApp } from 'src/lib/outils/mathgraph'
import { j3pCreeSVG } from 'src/legacy/core/functionsSvg'
import textesGeneriques from 'src/lib/core/textes'

const { cBien, cFaux, essaieEncore, regardeCorrection, tempsDepasse } = textesGeneriques

/*
    Yves Biton
    Juin 2014

    sectionsquelettemtg32_Vecteurs_Sur_Axe_Placer_M
    On demande de placer un point M sur un axe avec deux points A et B et une égalité vectorielle
    L’axe est muni d’une graduation régulière. Le point A est fixe.
    Le point B est situé à droite de A et la longueur AB peut être donnée en paramètre.
    On peut aussi passer en paramètre l’abscisse du point M solution.

*/

export const params = {
  outils: ['mathquill'],
  /**
   * Ce qui est paramétrable dans l’éditeur de graphe
   * @type {LegacySectionParamsParametres}
   */
  parametres: [
    ['nbrepetitions', 1, 'entier', 'aucun'],
    ['limite', 0, 'entier', 'Temps disponible par répétition, en secondes'],
    ['nbchances', 1, 'entier', 'Nombre de validations permises'],
    ['AB', 'random', 'string', 'Distance AB avec B à droite de A (de 1 à 5, entre 2 et 5 si random)'],
    ['xM', 'random', 'string', 'Abscisse du point M vérifiant l’égalité du texte (origine A) ,de -13 à 18 ']
  ]
}

/**
 * section squelettemtg32_Vecteurs_Sur_Axe_Placer_M
 * @this {Parcours}
 */
export default function main () {
  const parcours = this
  const sq = this.storage
  function initMtg () {
    const mtgOptions = {
      // player only, sans figure initiale ni svg, on gère ça dans initMtg et modif_fig
      loadCoreWithMathJax: true
    }
    getMtgApp('mtg32svg', {}, mtgOptions).then((mtgAppLecteur) => {
      sq.mtgAppLecteur = mtgAppLecteur
      sq.mtgAppLecteur.removeAllDoc()
      sq.mtgAppLecteur.addDoc('mtg32svg', sq.txt, true)
      let par = parcours.donneesSection.AB
      if (par !== 'random') sq.mtgAppLecteur.giveFormula2('mtg32svg', 'long', par)
      par = parcours.donneesSection.xM
      if (par !== 'random') sq.mtgAppLecteur.giveFormula2('mtg32svg', 'ab', par)
      sq.mtgAppLecteur.calculateAndDisplayAll(true)
      j3pAffiche('enonce', 'texte', sq.consigne1)
      parcours.finEnonce()
    }).catch(j3pShowError)
  }

  function initDom () {
    if (sq.titre) parcours.afficheTitre(sq.titre)
    j3pDiv(parcours.zones.MG, {
      id: 'conteneur',
      contenu: '',
      style: parcours.styles.etendre('toutpetit.enonce', { padding: '6px' })
    })
    j3pDiv('conteneur', 'enonce', '')
    j3pDiv('conteneur', 'divmtg32', '')
    j3pCreeSVG('divmtg32', { id: 'mtg32svg', width: 750, height: 450 })
    const style = parcours.styles.petit.correction
    style.marginTop = '1.5em'
    style.marginLeft = '0.5em'
    j3pAddElt(parcours.zonesElts.MD, 'div', '', { id: 'correction', style })
    initMtg()
  }

  function _Donnees () {
    // "primaire" || "lycee" || "college"
    // Actuellement les dimensions sont identiques
    this.typesection = 'lycee'//
    // Nombre de répétitions de l’exercice
    this.nbrepetitions = 1
    this.nbetapes = 1
    this.nbitems = this.nbetapes * this.nbrepetitions

    this.f = 'Vecteurs_Placer_M_VectAM_Egal_kVectAB'
    this.AB = 'random'
    this.xM = 'random'
    // Si indication != "" alors un lien "Indication" s’affiche dans la zone Inférieure Gauche (this.zones.IG
    // une indication<br>de deux lignes...";

    // pour le cas de presentation1bis
    // En effet pas d’indication possible pour cette présentation
    // this.surchargeindication = false;

    // Nombre de chances dont dispose l’él§ve pour répondre à la question
    this.nbchances = 1

    this.textes = {
    }
    this.titre = 'Titre de l’activité'

    this.pe = 0
  }

  switch (this.etat) {
    case 'enonce':
      // code exécuté au lancement de la section
      if (this.debutDeLaSection) {
        this.donneesSection = new _Donnees()
        // Construction de la page
        this.construitStructurePage('presentation1bis')

        this.surcharge()
        this.validOnEnter = false // ex donneesSection.touche_entree

        sq.nbchances = parseInt(parcours.donneesSection.nbchances)
        // compteur. Si   numEssai > nbchances, on corrige
        sq.numEssai = 1
        // faut gérer sous-dossier ou pas…
        const sep = this.donneesSection.f.includes('/') ? '-' : '/'
        j3pImporteAnnexe('squelettes-mtg32' + sep + this.donneesSection.f + '.js').then(function (datasSection) {
          // Attention, le this devient window...
          sq.txt = datasSection.txt
          sq.consigne1 = datasSection.consigne1
          sq.titre = datasSection.titre
          initDom()
        }).catch(error => {
          console.error(error)
          j3pShowError('Impossible de charger les données de cette exercice')
        })
      } else {
        j3pEmpty('correction')
        sq.numEssai = 1
        sq.mtgAppLecteur.removeDoc('mtg32svg')
        sq.mtgAppLecteur.addDoc('mtg32svg', sq.txt, true)
        sq.mtgAppLecteur.calculate('mtg32svg', true) // true pour que les calculs aléaoires soient réinitialisés
        sq.mtgAppLecteur.display('mtg32svg') // Pour tout réafficher
        sq.mtgAppLecteur.setActive('mtg32svg', true)
        this.finEnonce()
      }

      break // case "enonce":

    case 'correction': {
      let bilanreponse = ''
      const res1 = sq.mtgAppLecteur.valueOf('mtg32svg', 'reponse')
      if (res1 === 1) {
        bilanreponse = 'exact'
      } else {
        bilanreponse = 'erreur'
      }

      // Bonne réponse
      if (bilanreponse === 'exact') {
        this._stopTimer()
        sq.mtgAppLecteur.setActive('mtg32svg', false)
        this.score++
        j3pElement('correction').style.color = this.styles.cbien
        j3pElement('correction').innerHTML = cBien
        sq.mtgAppLecteur.executeMacro('mtg32svg', 'solutionVrai')
        this.etat = 'navigation'
        this.sectionCourante()
      } else {
        // Pas de bonne réponse
        // A cause de la limite de temps :
        if (this.isElapsed) { // limite de temps
          this._stopTimer()
          sq.mtgAppLecteur.setActive('mtg32svg', false)
          j3pElement('correction').style.color = this.styles.cfaux
          sq.mtgAppLecteur.executeMacro('mtg32svg', 'solutionFaux')
          j3pElement('correction').innerHTML = tempsDepasse
          j3pElement('correction').innerHTML += '<br>' + regardeCorrection
          this.etat = 'navigation'
          this.sectionCourante()
        } else {
          // Réponse fausse :
          j3pElement('correction').style.color = this.styles.cfaux
          j3pElement('correction').innerHTML = cFaux
          sq.numEssai++
          // S’il y a plus de deux chances,on appelle à nouveau le bouton Valider
          if (sq.numEssai <= sq.nbchances) {
            j3pElement('correction').innerHTML += '<br>' + essaieEncore
          } else {
            // Erreur au nème essai
            this._stopTimer()
            sq.mtgAppLecteur.setActive('mtg32svg', false)
            j3pElement('correction').style.color = this.styles.cfaux
            sq.mtgAppLecteur.executeMacro('mtg32svg', 'solutionFaux')
            j3pElement('correction').innerHTML += '<br>' + regardeCorrection
            this.etat = 'navigation'
            this.sectionCourante()
          }
        }
      }
      // Obligatoire
      this.finCorrection()
      break // case "correction":
    }

    case 'navigation':

      if (this.sectionTerminee()) {
        // On détermine la phrase d’état renvoyée par la section
        this.parcours.pe = this.score
        this.afficheBoutonSectionSuivante()
        this.focus('sectioncontinuer')
      } else {
        this.etat = 'enonce'
        this.afficheBoutonSuite()
        this.focus('boutoncontinuer')
      }
      // Obligatoire
      this.finNavigation()

      break // case "navigation":
  }
}
