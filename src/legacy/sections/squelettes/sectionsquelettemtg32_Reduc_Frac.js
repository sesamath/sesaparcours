import $ from 'jquery'
import { j3pAddContent, j3pAddElt, j3pAjouteBouton, j3pElement, j3pEmpty, j3pFocus, j3pImporteAnnexe, j3pPaletteMathquill, j3pShowError } from 'src/legacy/core/functions'
import { MathfieldElement, renderMathInDocument } from 'mathlive'
import { getMtgApp } from 'src/lib/outils/mathgraph'
import { j3pCreeSVG } from 'src/legacy/core/functionsSvg'
import { cleanLatexForMl, mathliveRestrict } from 'src/lib/outils/mathlive/utils'
import { afficheMathliveDans, getMathliveValue } from 'src/lib/outils/mathlive/display'
import { unLatexify } from 'src/lib/mathquill/functions'
import { setMathliveCss } from 'src/lib/outils/mathlive/mathlivecss'
import { resetKeyboardPosition } from 'src/lib/widgets/mlVirtualKeyboard/VirtualKeyboard'
import textesGeneriques from 'src/lib/core/textes'
import { focusIfExists } from 'src/lib/utils/dom/main'

const { cBien, cFaux, tempsDepasse } = textesGeneriques

/*
    Yves Biton
    Octobre 2018. REvu en décembre 2023 pour passage à Mathlive

    http://localhost:8081/?graphe=[1,"squelettemtg32_Reduc_Frac",[{pe:">=0",nn:"fin",conclusion:"fin"},{f:"Fact_ax_Carre_Moins_cCarre_Ex1"}]];
    squelettemtg32_Reduc_Frac
    Squelette demandant de réduire une frcation
    On peut éventuellement spécifier des paramètres  n,d,a,b et c.

    Si ex contient un paramètre nbParamAleat , alors dans le cas ou nbrepetitions > 1, on donne pour i compris entre 1 et npParamAleat
    au calcul de la figure nommé "r"+i (par exemple r1 et r2 si nbParamAleat vaut 2) des valeurs comprises entre 0 et "nbcas"+i - 1
    toutes différentes (sauf si nbrepetitions > b=nbcas auquel cas au bout de nbcas répétitions il y aura nécessairement une valeur de r déjà prise.

*/
// on définit le dossier des fonts mathlive (il faut le faire très tôt,
// ici c’est bon avec pnpm start mais trop tard pour un build (cf src/lib/player/index.js de la branche draftPlayer)
if (MathfieldElement) { // undefined dans les tests avec happy-dom
  MathfieldElement.fontsDirectory = '/static/mathlive/fonts'
  MathfieldElement.soundsDirectory = '/static/mathlive/sounds'
}

export const params = {
  /**
   * Ce qui est paramétrable dans l’éditeur de graphe
   * @type {LegacySectionParamsParametres}
   */
  parametres: [
    ['nbrepetitions', 1, 'entier', 'aucun'],
    ['limite', 0, 'entier', 'Temps disponible par répétition, en secondes'],
    ['nbEssais', 6, 'entier', 'Nombre d’essais maximum autorisés'],
    ['boutonRecopie', true, 'boolean', 'true pour avoir un bouton de recopie de la réponse précédente'],
    ['n', 'random', 'string', 'Le numérateur de la fraction à simplifier'],
    ['d', 'random', 'string', 'Le dénominateur de la fraction à simplifier'],
    ['k', 'random', 'string', 'L’entier par lequel la fraction sera simplifiable (n et d doivent être à random)'],
    ['a', 'random', 'string', 'Premier nombre premier utilisé dans la fraction réduite (si n et d sont à random)'],
    ['b', 'random', 'string', 'Deuxième nombre premier utilisé dans la fraction réduite (si n et d sont à random)'],
    ['c', 'random', 'string', 'Troisième nombre premier utilisé dans la fraction réduite (si n et d sont à random)'],
    ['ex', 'College_ReducFrac_Progressif1', 'string', 'Nom du fichier annexe (xxx => squelettes-mtg32/xxx.js et xxx/yyy => squelettes-mtg32-xxx/yyy.js)']
  ]
}

const inputId = 'expressioninputmq1'

/**
 * section squelettemtg32_Reduc_Frac
 * @this {Parcours}
 */
export default function squelettemtg32ReducFrac () {
  const parcours = this
  const sq = this.storage

  function marqueEditeurPourErreur () {
    sq.marked = true
    const mf = j3pElement(inputId)
    if (!mf) return
    mf.style.backgroundColor = '#FF9999'
    mf.demarqueErreur = () => {
      if (sq.marked) {
        demarqueEditeurPourErreur()
      }
    }
  }

  function demarqueEditeurPourErreur () {
    sq.marked = false
    j3pEmpty('correction')
    const mf = j3pElement(inputId)
    if (!mf) return
    mf.style.backgroundColor = 'white'
  }

  function traiteMathlive (ch) {
    // Mathlive ne met pas d’accolades pour les fractions dont le numérateur et le dénominateur sont des entiers
    ch = ch.replace(/\\frac(\d)(\d)/g, '\\frac{$1' + '}{' + '$2}')
    // Ni pour les racines carrées d’un entier
    ch = ch.replace(/\\sqrt(\d)/g, '\\sqrt{$1}')
    // Pour les puissances dont l’exposant est un chiffre, Mathlive ne met pas d’accolades
    ch = ch.replace(/\^([^{])/g, '^{$1}')
    ch = unLatexify(ch)
    // mathlive peut mettre des {} vides
    ch = ch.replace(/\\{\\}/g, '')
    ch = ch.replace(/\^(\d)(\d)/g, '^$1*$2') // On peut avoir par exemple 2^35^4 pour 2^3*5^4
    return sq.mtgAppLecteur.addImplicitMult('mtg32svg', ch)// Traitement des multiplications implicites
  }

  /**
   * Fonction extrayant d’un affichage LaTeX de la figure les lignes de texte et les transformant
   * en une chaîne affichable par MathQuill
   * @param tagLatex : le tag de l’affichage LaTeX de la figure
   * @return {string}
   */
  function extraitDeLatexPourMathlive (tagLatex) {
    const list = sq.mtgAppLecteur.getList('mtg32svg')
    const latex = list.getByTag(tagLatex, true)
    if (latex === null || latex.className !== 'CLatex') return ''
    return cleanLatexForMl(latex.chaineLatex, sq.charset)
  }

  /**
   * Fonction affichant la solution s’il existe un affichage LaTeX de tag solution
   * @param{boolean} bexact true si l’élève a entré la bonne réponse, false sinon
   */
  function afficheSolution (bexact) {
    $('#divSolution').css('display', 'block').html('')
    const ch = extraitDeLatexPourMathlive('solution')
    if (ch !== '') {
      afficheMathliveDans('divSolution', 'solution', ch, { style: { color: bexact ? 'green' : 'blue' } })
    }
  }

  function cacheSolution () {
    $('#divSolution').css('display', 'none').html('')
  }

  function validationEditeur () {
    const rep = getMathliveValue(sq.inputMq)
    if (rep === '') return false
    const repcalcul = traiteMathlive(rep)
    const valide = sq.mtgAppLecteur.syntaxValidation('mtg32svg', 'rep', repcalcul, true)
    if (!valide) {
      marqueEditeurPourErreur()
      focusIfExists(inputId)
      return false
    }
    return true
  }

  function montreEditeur (bVisible) {
    if (bVisible) sq.info.display = 'block'
    sq.editeur.style.display = bVisible ? 'block' : 'none'
    sq.boutonsMathquill.style.display = bVisible ? 'block' : 'none'
    // On vide le contenu de l’éditeur MathQuill et on lui donne le focus.
    sq.inputMq.value = ''
    j3pFocus(sq.inputMq)
  }

  function videEditeur () {
    sq.inputMq.value = ''
    focusIfExists(inputId) // focus nécessaire sinon éditeur riquiqui
  }

  function afficheReponse (bilan, depasse) {
    let couleur
    const num = parcours.essaiCourant
    const idrep = 'exp' + num
    if (num >= 2) afficheMathliveDans(sq.formules, idrep + 'br', '<br>')

    if (bilan === 'exact') {
      couleur = parcours.styles.cbien
    } else {
      if (bilan === 'faux') {
        couleur = parcours.styles.cfaux
      } else { // Réponse exacte mais calcul pas fini
        if (depasse) {
          couleur = parcours.styles.cfaux
        } else {
          couleur = '#0000FF'
        }
      }
    }
    const latex = '$' + sq.aCalculer + ' ' + (bilan === 'faux' ? '\\ne' : '=') + ' ' + sq.rep + '$'
    afficheMathliveDans(sq.formules, idrep, latex, { style: { color: couleur } })
    resetKeyboardPosition()
  }

  function validation () {
    if (parcours.essaiCourant > 0 && sq.btnRecopier) {
      sq.btnRecopier.style.display = 'block'
    }
    sq.rep = getMathliveValue(sq.inputMq)
    sq.repMathQuill = traiteMathlive(sq.rep)
    sq.mtgAppLecteur.giveFormula2('mtg32svg', 'rep', sq.repMathQuill)
    sq.mtgAppLecteur.calculate('mtg32svg', false)
    sq.reponse = sq.mtgAppLecteur.valueOf('mtg32svg', 'reponse')
  }

  function recopierReponse () {
    demarqueEditeurPourErreur()
    const mf = j3pElement(inputId)
    if (!mf) return
    mf.value = sq.rep
    focusIfExists(inputId)
  }

  function initMtg () {
    setMathliveCss('MepMG')
    const mtgOptions = {
      // player only, sans figure initiale ni svg, on gère ça dans initMtg et modif_fig
      loadCoreWithMathJax: true
    }
    getMtgApp('mtg32svg', {}, mtgOptions).then((mtgAppLecteur) => {
      initDom()
      sq.mtgAppLecteur = mtgAppLecteur
      sq.mtgAppLecteur.removeAllDoc()
      sq.mtgAppLecteur.addDoc('mtg32svg', sq.txt, true)
      sq.mtgAppLecteur.calculate('mtg32svg', false)
      const ch = 'abcdefghjklmnpqr'
      const nbvar = sq.mtgAppLecteur.valueOf('mtg32svg', 'nbvar')
      if (nbvar !== -1) {
        const nbrep = parcours.donneesSection.nbrepetitions
        sq.aleat = true
        sq.nbParamAleat = nbvar
        for (let i = 1; i <= nbvar; i++) {
          const nbcas = sq.mtgAppLecteur.valueOf('mtg32svg', 'nbcas' + i)
          const nb = Math.max(nbrep, nbcas)
          const ar = []
          for (let j = 0; j < nb; j++) ar.push(j % nbcas)
          sq['tab' + i] = []
          for (let k = 0; k < nbrep; k++) {
            const tir = Math.floor(Math.random() * ar.length)
            sq['tab' + i].push(ar[tir])
            ar.splice(tir, 1)
          }
          sq.mtgAppLecteur.giveFormula2('mtg32svg', 'r' + i, sq['tab' + i][0])
        }
      } else {
        sq.aleat = false
      }

      if (sq.param !== undefined) {
        for (let i = 0; i < ch.length; i++) {
          const car = ch.charAt(i)
          if (sq.param.indexOf(car) !== -1) {
            const par = parcours.donneesSection[car]
            if (par !== 'random') sq.mtgAppLecteur.giveFormula2('mtg32svg', car, par)
          }
        }
      }
      sq.mtgAppLecteur.calculateAndDisplayAll(true)
      refreshEnonce()
      // Ce listener ne devait être ajouté qu’après sq.mtgAppLecteur, mais pas pour cette section où il n’est pas utilisé
      const liste = ['fraction']
      afficheMathliveDans(sq.editeur, 'expression', '$' + sq.aCalculer + '$ = &1&', {
        charset: '0123456789/*',
        listeBoutons: liste
      })
      sq.inputMq = j3pElement(inputId)
      mathliveRestrict(sq.inputMq, '0123456789/*')
      sq.boutonsMathquill = j3pAddElt(sq.conteneur, 'div', '', { style: { display: 'inline-block', paddingTop: '10px' } })
      j3pPaletteMathquill(sq.boutonsMathquill, inputId, { liste })
      sq.inputMq.onkeyup = () => {
        if (sq.marked) demarqueEditeurPourErreur()
      }
      focusIfExists(inputId)
    }).catch(j3pShowError)
  }

  function refreshEnonce () {
    j3pEmpty(sq.enonce)
    j3pEmpty(sq.formules)
    sq.reponse = -1
    const nbParam = sq.numero
    const t = ['a', 'b', 'c', 'd']
    const param = {}
    for (let i = 0; i < nbParam; i++) {
      const code = sq.mtgAppLecteur.getLatexCode('mtg32svg', i)
      if (i === 0) sq.aCalculer = code
      param[t[i]] = code
    }
    param.e = String(sq.nbEssais) // au cas où y’aurait une variable e dans la figure mtg ?
    const st = 'Il faut simplifier au maximum la fraction $A=£a$' + '<br>Appuie sur la touche <B>Entrée</B> pour valider chaque étape.'
    afficheMathliveDans(sq.enonce, 'texte', st, param)
    refreshTexteInfo()
    parcours.finEnonce()
  }

  function refreshTexteInfo () {
    sq.info.style.display = 'block'
    const reste = sq.nbEssais - parcours.essaiCourant
    let txt = (reste > 1)
      ? 'Il reste ' + reste + ' étapes'
      : 'Il reste une étape'
    txt += ' maximum pour finir le calcul.'
    j3pAddContent(sq.texteInfo, txt, { replace: true })
  }

  function initDom () {
    renderMathInDocument({ renderAccessibleContent: '' })
    sq.conteneur = j3pAddElt(parcours.zonesElts.MG, 'div', { style: parcours.styles.etendre('petit.enonce', { padding: '6px' }) })
    sq.enonce = j3pAddElt(sq.conteneur, 'div')
    sq.formules = j3pAddElt(sq.conteneur, 'div', '', { style: parcours.styles.petit.enonce })
    sq.info = j3pAddElt(sq.conteneur, 'div')
    sq.texteInfo = j3pAddElt(sq.info, 'div', '', { style: { color: '#7F007F' } })
    sq.conteneurBouton = j3pAddElt(sq.conteneur, 'div')
    if (parcours.donneesSection.boutonRecopie) sq.btnRecopier = j3pAjouteBouton(sq.conteneurBouton, recopierReponse, { className: 'MepBoutonsRepere', value: 'Recopier réponse précédente', style: { display: 'none' } })
    sq.editeur = j3pAddElt(sq.conteneur, 'div', '', { style: { paddingTop: '10px' } }) // Le div qui contiendra l’éditeur MathQuill
    j3pAddElt(sq.conteneur, 'div', '', { id: 'divSolution' })
    $('#divSolution').css('display', 'none')
    sq.divMtg = j3pAddElt(sq.conteneur, 'div')
    // La figure n’est plus affichée tout se fait en html
    sq.mtg32svg = j3pCreeSVG(sq.divMtg, {
      id: 'mtg32svg',
      width: 0,
      height: 0
    })
    const style = parcours.styles.petit.correction
    style.marginTop = '1.5em'
    style.marginLeft = '0.5em'
    sq.correction = j3pAddElt(parcours.zonesElts.MD, 'div', '', { id: 'correction', style })
  }

  switch (this.etat) {
    case 'enonce':
      // code exécuté au lancement de la section
      if (this.debutDeLaSection) {
        // init sq.nbEssais, on s’assure que c’est bien un entier >= 1
        if (typeof parcours.donneesSection.nbEssais === 'string') {
          // y’a un pb qq part car on précisait entier dans nos params
          sq.nbEssais = parseInt(parcours.donneesSection.nbEssais)
        } else {
          sq.nbEssais = parcours.donneesSection.nbEssais
        }
        if (!Number.isInteger(sq.nbEssais) || sq.nbEssais < 1) {
          console.error(Error('Nombre d’essais invalides => 6 imposé'))
          sq.nbEssais = 6
        }
        // Construction de la page
        parcours.construitStructurePage('presentation1bis')

        // on charge le fichier annexe
        // faut gérer sous-dossier ou pas…
        const sep = this.donneesSection.ex.includes('/') ? '-' : '/'
        j3pImporteAnnexe('squelettes-mtg32' + sep + this.donneesSection.ex + '.js').then(function (datasSection) {
          // Attention, le this est ici window...
          sq.txt = datasSection.txt
          sq.symbexact = datasSection.symbexact ?? '='
          sq.symbnonexact = datasSection.symbnonexact ?? '\\ne'
          sq.numero = Number(datasSection.numero ?? 0) // Le nombre de paramètres LaTeX dans le texte
          if (datasSection.titre) parcours.afficheTitre(datasSection.titre)
          sq.param = datasSection.param ?? '' // Chaine contenant les paramètres autorisés
          initMtg()
        }).catch(error => {
          console.error(error)
          j3pShowError('Impossible de charger les données de cette exercice')
        })
      } else {
        // on ne réinitialise que MD, pas MG car on veut conserver ce qu’a créé initDom
        j3pEmpty('correction')
        j3pEmpty(sq.editeur)
        cacheSolution()
        sq.editeur.style.display = 'block'
        sq.boutonsMathquill.style.display = 'inline-block'
        // On vide le contenu de l’éditeur MathQuill et on lui donne le focus.
        sq.inputMq.value = ''

        sq.mtgAppLecteur.removeDoc('mtg32svg')
        sq.mtgAppLecteur.addDoc('mtg32svg', sq.txt, true)

        if (sq.aleat) {
          for (let i = 1; i <= sq.nbParamAleat; i++) {
            sq.mtgAppLecteur.giveFormula2('mtg32svg', 'r' + i, sq['tab' + i][this.questionCourante - 1])
          }
        }
        if (sq.param !== undefined) {
          const ch = 'abcdefghjklmnpqr'
          for (let i = 0; i < ch.length; i++) {
            const car = ch.charAt(i)
            if (sq.param.indexOf(car) !== -1) {
              const par = parcours.donneesSection[car]
              if (par !== 'random') sq.mtgAppLecteur.giveFormula2('mtg32svg', car, par)
            }
          }
        }

        sq.mtgAppLecteur.calculate('mtg32svg', true) // true pour que les calculs alèatoires soient réinitialisés
        refreshEnonce()
        // Ce listener ne devait être ajouté qu’après sq.mtgAppLecteur, mais pas pour cette section où il n’est pas utilisé
        const liste = ['fraction']
        afficheMathliveDans(sq.editeur, 'expression', '$' + sq.aCalculer + '$ = &1&', {
          charset: '0123456789/*',
          listeBoutons: liste
        })
        sq.inputMq = j3pElement(inputId)
        mathliveRestrict(sq.inputMq, '0123456789/*')
        sq.boutonsMathquill = j3pAddElt(sq.conteneur, 'div', '', { style: { display: 'inline-block', paddingTop: '10px' } })
        j3pPaletteMathquill(sq.boutonsMathquill, inputId, { liste })
        sq.inputMq.onkeyup = () => {
          if (sq.marked) demarqueEditeurPourErreur()
        }
        focusIfExists(inputId)
        // sq.mtgAppLecteur.display('mtg32svg') // Pour tout réafficher
        this.finEnonce()
      }

      break // case "enonce":

    case 'correction': {
      let bilanReponse = ''
      if (validationEditeur()) {
        validation()
        switch (sq.reponse) {
          case 1:
            bilanReponse = 'exact'
            break
          case 2:
            bilanReponse = 'exactPasFini'
            break
          case 3:
            bilanReponse = 'exactNonSimplifie'
            break
          default:
            bilanReponse = 'faux'
        }
      } else {
        bilanReponse = 'incorrect'
        focusIfExists(inputId)
      }

      // A cause de la limite de temps :
      if (this.isElapsed) { // limite de temps
        montreEditeur(false)
        sq.correction.style.color = this.styles.cfaux
        if (sq.btnRecopier) sq.btnRecopier.style.display = 'none'
        sq.info.style.display = 'none'
        afficheSolution(false)
        sq.correction.innerHTML = tempsDepasse
        sq.correction.innerHTML += '<br>La solution se trouve ci-contre.'
        return this.finCorrection('navigation', true)
      }

      if (bilanReponse === 'incorrect') {
        if (getMathliveValue(sq.inputMq) !== '') {
          sq.correction.style.color = this.styles.cfaux
          sq.correction.innerHTML = 'Réponse incorrecte'
        } // else c’est vide on fait rien
        // on reste dans l’état correction et on attend une nouvelle validation
        return this.finCorrection()
      }

      // Une réponse a été saisie (sinon on serait passé dans le if précédent car vide => incorrect
      if (bilanReponse === 'exact') {
        // Bonne réponse
        sq.mtgAppLecteur.setActive('mtg32svg', false)
        this.score++
        sq.correction.style.color = this.styles.cbien
        sq.correction.innerHTML = cBien
        if (sq.btnRecopier) sq.btnRecopier.style.display = 'none'
        sq.info.style.display = 'none'
        montreEditeur(false)
        afficheReponse('exact', false)
        afficheSolution(true)
        return this.finCorrection('navigation', true)
      }

      if (bilanReponse === 'exactPasFini') {
        sq.correction.style.color = (this.essaiCourant <= sq.nbEssais) ? '#0000FF' : this.styles.cfaux
        sq.correction.innerHTML = 'La fraction peut encore être simplifiée.'
      } else if (bilanReponse === 'exactNonSimplifie') {
        sq.correction.style.color = this.styles.cfaux
        sq.correction.innerHTML = 'La fraction n’est pas simplifiée.'
      } else {
        sq.correction.style.color = this.styles.cfaux
        sq.correction.innerHTML = cFaux
      }
      if (this.essaiCourant < sq.nbEssais) {
        refreshTexteInfo()
        videEditeur()
        afficheReponse(bilanReponse, false)
        // il reste des essais, on reste dans l’état correction et on attend la validation suivante
        return this.finCorrection()
      }

      // Erreur au dernier essai
      sq.mtgAppLecteur.setActive('mtg32svg', false)
      if (sq.btnRecopier) sq.btnRecopier.style.display = 'none'
      sq.info.style.display = 'none'
      afficheSolution(false)
      montreEditeur(false)
      afficheReponse(bilanReponse, true)
      sq.correction.innerHTML += '<br><br>La solution se trouve ci-contre.'
      this.finCorrection('navigation', true)
      break
    } // case "correction":

    case 'navigation':
      if (this.sectionTerminee()) {
        // On détermine la phrase d’état renvoyée par la section, ici c’est un score…
        this.parcours.pe = this.score / this.donneesSection.nbitems
      } else {
        this.etat = 'enonce'
      }
      this.finNavigation(true)
      break // case "navigation":
  }
}
