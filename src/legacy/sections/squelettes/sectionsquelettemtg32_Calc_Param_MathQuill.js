import $ from 'jquery'
import { MathfieldElement, renderMathInDocument } from 'mathlive'

import { j3pAddContent, j3pAddElt, j3pAjouteBouton, j3pDetruit, j3pElement, j3pEmpty, j3pImporteAnnexe, j3pNotify, j3pPaletteMathquill, j3pShowError } from 'src/legacy/core/functions'
import { unLatexify } from 'src/lib/mathquill/functions'
import { getMtgApp } from 'src/lib/outils/mathgraph'
import { j3pCreeSVG } from 'src/legacy/core/functionsSvg'
import { cleanLatexForMl, mathliveRestrict } from 'src/lib/outils/mathlive/utils'
import { setMathliveCss } from 'src/lib/outils/mathlive/mathlivecss'
import { afficheMathliveDans, getMathliveValue } from 'src/lib/outils/mathlive/display'
import { getFirstIndexParExcluded, getIndexFermant } from 'src/lib/utils/string'
import { resetKeyboardPosition } from 'src/lib/widgets/mlVirtualKeyboard/VirtualKeyboard'
import textesGeneriques from 'src/lib/core/textes'
import { focusIfExists } from 'src/lib/utils/dom/main'

const { cBien, cFaux, essaieEncore, tempsDepasse } = textesGeneriques

/*
    Yves Biton
    Juillet 2014. Refondu en 2023 pour adaptation à Mathlive au lieu de Mathquill
    http://localhost:8081/?graphe=[1,"squelettemtg32_Calc_Param_MathQuill",[{pe:">=0",nn:"fin",conclusion:"fin"},{f:"Fact_ax_Carre_Moins_cCarre_Ex1"}]];
    squelettemtg32_Calc_Param_MathQuill
    Squelette demandant de calculer une expression et de l’écrire sous la forme la plus simple possible
    On peut éventuellement spécifier des paramètres  a,b,c,d,e,f,g,h,j,k,n,n,p.

    Si ex contient un paramètre nbParamAleat , alors dans le cas ou nbrepetitions > 1, on donne pour i compris entre 1 et npParamAleat
    au calcul de la figure nommé "r"+i (par exemple r1 et r2 si nbParamAleat vaut 2) des valeurs comprises entre 0 et "nbcas"+i - 1
    toutes différentes (sauf si nbrepetitions > b=nbcas auquel cas au bout de nbcas répétitions il y aura nécessairement une valeur de r déjà prise.

    Le fichier annexe peut contenir un membre variable contenant les lettres autorisées dans un crochet ppur le calcil de primitives
    Pour les calculs d’intégrales, quand on demande un résultat non simplifié :
      Si la valeur renvoyée par reponse est de 2 et que le calcul
      entré par l’lève contient une ou plusieurs intégrales ou un ou plusieurs crochets de primitive,
      le calcul sera considéré comme exact mais non fini.
      Si le fichier annexe contient un paramètre reponseSansPrim, si la valeur renvoyée par reponse est de  1 et que le calcul
      entré par l’élève contient un ou plusieurs crochets de primitive, le calcul sera considéré comme non fini.

*/

// on définit le dossier des fonts mathlive (il faut le faire très tôt,
// ici c’est bon avec pnpm start mais trop tard pour un build (cf src/lib/player/index.js de la branche draftPlayer)
if (MathfieldElement) { // undefined dans les tests avec happy-dom
  MathfieldElement.fontsDirectory = '/static/mathlive/fonts'
  MathfieldElement.soundsDirectory = '/static/mathlive/sounds'
}

export const params = {
  /**
   * Ce qui est paramétrable dans l’éditeur de graphe
   * @type {LegacySectionParamsParametres}
   */
  parametres: [
    ['nbrepetitions', 1, 'entier', 'aucun'],
    ['limite', 0, 'entier', 'Temps disponible par répétition, en secondes'],
    ['nbchances', 1, 'entier', 'Nombre d’erreurs possibles (pour tenter de les corriger)'],
    ['nbEssais', 6, 'entier', 'Nombre d’essais maximum autorisés'],
    ['indicationfaute', true, 'boolean', 'true si on veut que certaines fautes soient expliquées après appui sur Entrée'],
    ['simplifier', true, 'boolean', 'true si on exige un forme donnée, false sinon'],
    ['a', 'random', 'string', 'Valeur de a'],
    ['b', 'random', 'string', 'Valeur de b'],
    ['c', 'random', 'string', 'Valeur de c'],
    ['d', 'random', 'string', 'Valeur de d'],
    ['e', 'random', 'string', 'Valeur de e'],
    ['f', 'random', 'string', 'Valeur de f'],
    ['g', 'random', 'string', 'Valeur de g'],
    ['h', 'random', 'string', 'Valeur de h'],
    ['j', 'random', 'string', 'Valeur de j'],
    ['k', 'random', 'string', 'Valeur de k'],
    ['l', 'random', 'string', 'Valeur de l'],
    ['m', 'random', 'string', 'Valeur de m'],
    ['n', 'random', 'string', 'Valeur de n'],
    ['p', 'random', 'string', 'Valeur de p'],
    ['q', 'random', 'string', 'Valeur de q'],
    ['r', 'random', 'string', 'Valeur de r'],
    ['x', 'random', 'string', 'Valeur de x'],
    ['y', 'random', 'string', 'Valeur de y'],
    ['ex', 'Devel_ax_Fois_bx+c_Niv2', 'string', 'Nom du fichier annexe (xxx => squelettes-mtg32/xxx.js et xxx/yyy => squelettes-mtg32-xxx/yyy.js)']
  ]
}

// les lettres (variables à random par défaut dans donneesSection)
const lettres = 'abcdefghjklmnpqrxy'
const inputId = 'expressioninputmq1'

/**
 * section squelettemtg32_Calc_Param_MathQuill
 * @this {Parcours}
 */
export default function squelettemtg32CalcParamMathQuill () {
  /** @type {Object} */
  const sq = this.storage
  const textes = {
    ilReste: 'Il reste ',
    essai: 'essai',
    essaisEt: 'essais et',
    validation: 'validation',
    validations: 'validations',
    une: 'une',
    fauteSyn: 'Faute de syntaxe',
    solutionIci: '\nLa solution se trouve ci-contre.',
    bienMais: 'C’est bien  mais on pouvait simplifier la réponse.',
    exactPasFini: 'La réponse est bonne mais pas écrite sous la forme demandée.'
  }

  function onkeyup (ev) {
    if (sq.marked) {
      demarqueEditeurPourErreur()
    }
    if (ev.keyCode === 13) {
      const rep = getMathliveValue(inputId)
      if (!rep) return
      const resul = traiteMathlive(rep)
      const chcalcul = resul.res
      const valide = resul.valide && sq.mtgAppLecteur.syntaxValidation('mtg32svg', 'rep', chcalcul, true)
      if (valide) {
        validation()
      } else {
        marqueEditeurPourErreur()
        j3pElement('correction').style.color = parcours.styles.cfaux
        j3pAddContent('correction', textes.fauteSyn, { replace: true })
        focusIfExists(inputId)
      }
    }
  }

  // Attention : Les }{ on été remplacés par des }/{
  function traiteIntegrales (st) {
    const faux = { valide: false, st }
    let ind
    // Traitement différent pour mathlive
    while ((ind = st.indexOf('\\int_')) !== -1) {
      const ind1 = getFirstIndexParExcluded(st, '^', ind + 5)
      if (ind1 === -1) return -1
      const a = st.substring(ind + 5, ind1)
      // Si le ^ est suivi d’une parenthèse on recherche la parenthèse fermante associée
      // Sinon c’est que la borne supérieure est formée d’un seul caractère
      let ind2, ind3, indsuiv
      if (st.charAt(ind1 + 1) === '(') {
        ind2 = ind1 + 2
        ind3 = getIndexFermant(st, ind1 + 1)
        indsuiv = ind3 + 1
      } else {
        ind2 = ind1 + 1
        ind3 = ind1 + 2
        indsuiv = ind3
      }
      if (ind3 === 0 || indsuiv >= st.length) return faux
      const b = st.substring(ind2, ind3)
      // Attention : les {}{} ont pu donner des ()/().
      if (st.charAt(indsuiv) === '/') indsuiv++
      // On recherche maintenant le d d’intégration en sautant les éventuelles parenthèses
      let ind4 = getFirstIndexParExcluded(st, '\\differentialD', indsuiv)
      if (ind4 === -1) return faux
      const fonc = st.substring(indsuiv, ind4)
      ind4 += 14
      let ind5
      if (st.charAt(ind4) === '(') {
        ind5 = getIndexFermant(st, ind4)
        ind4++
      } else {
        ind5 = ind4 + 1
      }
      if (ind5 === -1) return faux
      const varfor = st.substring(ind4, ind5) // Le caractère représentant la variable d’intégration
      st = st.substring(0, ind) + 'integrale(' + fonc + ',' + varfor + ',' + a + ',' + b + ')' + st.substring(ind5)
    }
    return { valide: true, res: st }
  }

  function traitePrimitives (st) {
    const faux = { valide: false, st }
    if (sq.variables === '') return { valide: true, res: st }
    let ind, tab
    while ((ind = st.indexOf('\\left[')) !== -1) {
      // On cherche le \right] correspondant
      const ind1 = getIndexFermant(st, ind + 5)
      if (ind1 === -1) return faux
      const fonc = st.substring(ind + 6, ind1 - 6)
      tab = variablesUtilisees(fonc)
      if (tab.length > 1) return faux
      const varfor = (tab.length === 0) ? sq.variables[0] : tab[0] // On traite le cas d’une fonction constante
      // Le crochet fermant doit être suivi d’un _
      if (st.charAt(ind1 + 1) !== '_') return faux
      const ind2 = getFirstIndexParExcluded(st, '^', ind1 + 2)
      if (ind2 === -1) return faux
      const a = st.substring(ind1 + 2, ind2)
      // Soit le caractère suivant n’est pas une parenthèse et c’est un seul caractère
      // qui est l’argument soit c’est le contenu de la parenthèse
      let ind3, ind4
      if (st.charAt(ind2 + 1) === '(') {
        ind3 = ind2 + 2
        ind4 = getIndexFermant(st, ind3)
      } else {
        ind3 = ind2 + 1
        ind4 = ind2 + 2
      }
      if (ind4 === -1 || ind4 > st.length) return faux
      const b = st.substring(ind3, ind4)
      st = st.substring(0, ind) + 'primitive(' + fonc + ',' + varfor + ',' + a + ',' + b + ')' + st.substring(ind4 + 1)
    }
    return { valide: true, res: st }
  }

  /**
     * Fonction renvoyant un tableau formé des noms de variables utilisées par le calcul contenu dans ch
     * Si le tableau renvoyé a une longueur au moins égale à 2 on considérera que la primitive est incohérente
     * @param {string} ch
     * @return {[]}
     */
  function variablesUtilisees (ch) {
    function utiliseVariable (nomvar) {
      let res
      res = ch.search(new RegExp('\\(' + nomvar + '[\\+\\-\\*\\/\\^\\)]')) !== -1
      res = res || ch.search(new RegExp(nomvar + '[\\+\\-\\*\\/\\^\\)]')) !== -1
      res = res || (ch.lastIndexOf(nomvar) === ch.length - 1)
      res = res || ch === nomvar
      return res
    }

    const tab = []
    for (let i = 0; i < sq.variables.length; i++) {
      const car = sq.variables.charAt(i)
      if (utiliseVariable(car)) tab.push(car)
    }
    return tab
  }

  function traiteMathlive (ch) {
    // Mathlive ne met pas d’accolades pour les fractions dont le numérateur et le dénominateur sont des entiers
    ch = ch.replace(/\\frac(\d)(\d)/g, '\\frac{$1' + '}{' + '$2}')
    // Ni pour les racines carrées d’un entier
    ch = ch.replace(/\\sqrt(\d)/g, '\\sqrt{$1}')
    // Pour les puissances dont l’exposant est un chiffre, Mathlive ne met pas d’accolades
    ch = ch.replace(/\^([^{])/g, '^{$1}')
    ch = unLatexify(ch)
    // mathlive peut mettre des {} vides
    ch = ch.replace(/\\{\\}/g, '')
    ch = ch.replace(/\^(\d)(\d)/g, '^$1*$2') // On peut avoir par exemple 2^35^4 pour 2^3*5^4
    let { res, valide } = traiteIntegrales(ch)
    if (valide) {
      const resul = traitePrimitives(res)
      res = resul.res
      valide = resul.valide
    }
    if (valide) ch = sq.mtgAppLecteur.addImplicitMult('mtg32svg', res)// Traitement des multiplications implicites
    const contientIntegOuPrim = ch.includes('integrale') || ch.includes('primitive')
    return { valide, res: ch, contientIntegOuPrim }
  }

  /**
     * Fonction extrayant d’un affichage LaTeX de la figure les lignes de texte et les transformant
     * en une chaîne affichable par MathQuill
     * @param tagLatex : le tag de l’affichage LaTeX de la figure
     * @return {string}
     */
  function extraitDeLatexPourMathlive (tagLatex) {
    const list = sq.mtgAppLecteur.getList('mtg32svg')
    const latex = list.getByTag(tagLatex)
    if (latex === null || latex.className !== 'CLatex') return ''
    return cleanLatexForMl(latex.chaineLatex)
  }

  /**
     * Fonction affichant le nombre d’essais restants nbe et le nombre de chances restantes nbc
     */
  function afficheNombreEssaisRestants () {
    j3pEmpty('info')
    const nbe = sq.nbEssais - sq.nbexp
    const nbc = Math.min(sq.nbchances - sq.numEssai + 1, nbe)
    if ((nbe === 1) && (nbc === 1)) {
      afficheMathliveDans('info', 'texteinfo', textes.ilReste + 'une' + ' ' + textes.validation, { style: { color: '#7F007F' } })
    } else {
      const valid = nbc === 1 ? ' ' + textes.validation + '.' : ' ' + textes.validations + '.'
      afficheMathliveDans('info', 'texteinfo', textes.ilReste + ' ' + nbe + ' ' + textes.essaisEt + ' ' + nbc + valid, { style: { color: '#7F007F' } })
    }
  }

  /**
     * Fonction affichant la solution s’il existe un affichage LaTeX de tag solution
     * @param{boolean} bexact true si l’élève a entré la bonne réponse, false sinon
     */
  function afficheSolution (bexact) {
    $('#divSolution').css('display', 'block').html('')
    const ch = extraitDeLatexPourMathlive('solution')
    if (ch !== '') {
      afficheMathliveDans('divSolution', 'solution', ch, { style: { color: bexact ? 'green' : 'blue' } })
    }
  }

  function cacheSolution () {
    $('#divSolution').css('display', 'none').html('')
  }

  function marqueEditeurPourErreur () {
    sq.marked = true
    const mf = j3pElement(inputId)
    if (!mf) return
    mf.style.backgroundColor = '#FF9999'
    mf.demarqueErreur = () => {
      if (sq.marked) {
        demarqueEditeurPourErreur()
      }
    }
  }

  function demarqueEditeurPourErreur () {
    sq.marked = false
    j3pEmpty('correction')
    const mf = j3pElement(inputId)
    if (!mf) return
    mf.style.backgroundColor = 'white'
  }

  // Le paramètre est à false seulement quand elle est appelée depuis l’étape de correction
  // Sinon la fonction est appelée par l’éditeur quand l’utilisateur appuie sur la touche entrée avec un seul paramètre
  // Modification Sésaparcours : cette fonction renvoie un booléen.
  // Elle renvoie true si parcours.sectionCourante() a dû être appelé et false sinon
  function validation (continuer) {
    if (arguments.length <= 1) continuer = true
    if (sq.nbexp === 0) {
      j3pElement('boutonrecopier').style.display = 'block'
    }
    sq.rep = getMathliveValue(inputId)
    sq.repMathQuill = traiteMathlive(sq.rep)
    const chcalcul = sq.repMathQuill.res
    sq.mtgAppLecteur.giveFormula2('mtg32svg', 'rep', chcalcul)
    j3pEmpty('correction')
    sq.mtgAppLecteur.calculate('mtg32svg', false)
    if (sq.indicationfaute) {
      const faute = sq.mtgAppLecteur.valueOf('mtg32svg', 'faute')
      if (faute === 1) {
        sq.mtgAppLecteur.executeMacro('mtg32svg', 'voirFaute')
      } else {
        sq.mtgAppLecteur.executeMacro('mtg32svg', 'masquerFaute')
      }
    }

    sq.reponse = sq.mtgAppLecteur.valueOf('mtg32svg', 'reponse')
    const idrep = 'exp' + sq.nbexp
    sq.nbexp++
    const chdeb = sq.nbexp === 1 ? '' : '<br>'
    if (sq.reponse >= 1) {
      sq.params.style = { color: parcours.styles.colorCorrection }
      afficheMathliveDans('formules', idrep, chdeb + '$' + (sq.entete === '' ? sq.aCalculer : sq.entete) + ' ' + sq.symbexact +
                ' ' + sq.rep + sq.postchaine + '$', sq.params)
    } else {
      const texteFaute = sq.indicationfaute ? extraitDeLatexPourMathlive('faute') : ''
      const chFaute = texteFaute !== '' ? '<br>' + texteFaute : ''
      sq.params.style = { color: parcours.styles.cfaux }
      afficheMathliveDans('formules', idrep, chdeb + '$' + (sq.entete === '' ? sq.aCalculer : sq.entete) + ' ' + sq.symbnonexact +
                ' ' + sq.rep + sq.postchaine + '$' + chFaute, sq.params)
    }
    // On vide le contenu de l’éditeur et on lui donne le focus.
    j3pElement(inputId).value = ''
    resetKeyboardPosition()
    // Si le nombre maximum d’essais est atteint on passe en mode correction
    if (sq.nbexp >= sq.nbEssais) {
      // sq.mtgAppLecteur.setActive("mtg32svg", false);
      if (continuer) {
        parcours.etat = 'correction'
        parcours.sectionCourante()
        $('#editeur').css('display', 'none')
        $('#boutonsmathquill').css('display', 'none')
        return true
      }
    } else {
      afficheNombreEssaisRestants()
    }
    return false
  }

  function recopierReponse () {
    demarqueEditeurPourErreur()
    const mf = j3pElement(inputId)
    if (!mf) return
    mf.value = sq.rep
    focusIfExists(inputId)
  }

  function initMtg () {
    const mtgOptions = {
      // player only, sans figure initiale ni svg, on gère ça dans initMtg et modif_fig
      loadCoreWithMathJax: true
    }
    getMtgApp('mtg32svg', {}, mtgOptions).then((mtgAppLecteur) => {
      sq.mtgAppLecteur = mtgAppLecteur
      initDom()
      let code, par, nbrep, ar
      sq.mtgAppLecteur.removeAllDoc()
      sq.mtgAppLecteur.addDoc('mtg32svg', sq.txt, true)
      sq.mtgAppLecteur.calculate('mtg32svg', false)
      const nbvar = sq.mtgAppLecteur.valueOf('mtg32svg', 'nbvar')
      if (nbvar !== -1) {
        nbrep = parcours.donneesSection.nbrepetitions
        sq.aleat = true
        sq.nbParamAleat = nbvar
        for (let i = 1; i <= nbvar; i++) {
          const nbcas = sq.mtgAppLecteur.valueOf('mtg32svg', 'nbcas' + i)
          const nb = Math.max(nbrep, nbcas)
          ar = []
          for (let j = 0; j < nb; j++) ar.push(j % nbcas)
          sq['tab' + i] = []
          for (let k = 0; k < nbrep; k++) {
            const tir = Math.floor(Math.random() * ar.length)
            sq['tab' + i].push(ar[tir])
            ar.splice(tir, 1)
          }
          sq.mtgAppLecteur.giveFormula2('mtg32svg', 'r' + i, sq['tab' + i][0])
        }
      } else {
        sq.aleat = false
      }

      if (sq.param !== undefined) {
        for (const lettre of lettres) {
          if (sq.param.indexOf(lettre) !== -1) {
            par = parcours.donneesSection[lettre]
            if (par !== 'random') sq.mtgAppLecteur.giveFormula2('mtg32svg', lettre, par)
          }
        }
      }
      sq.mtgAppLecteur.calculateAndDisplayAll(true)
      const nbParam = parseInt(sq.numero)
      const t = 'abcd'
      const param = {}
      for (let i = 0; i < nbParam; i++) {
        code = sq.mtgAppLecteur.getLatexCode('mtg32svg', i)
        if (i === 0) sq.aCalculer = code.replace(/\n/g, ' ') // Mathlive n’aime pas les retours à la ligne
        param[t.charAt(i)] = code
      }
      param.e = String(sq.nbEssais)
      sq.params = param
      const chaine = sq.consigne1 + (sq.simplifier ? sq.consigne3 : sq.consigne2) + sq.consigne4
      afficheMathliveDans('enonce', 'texte', chaine, param)
      const debut = sq.entete !== '' ? '$' + sq.entete + sq.symbexact + '$ ' : '$' + sq.aCalculer + sq.symbexact + '$ '
      // sq.params.inputmq1 = {}
      // sq.params.styletexte = {}
      sq.params.style = { fontSize: '24px' }
      sq.params.listeBoutons = sq.listeBoutons
      sq.params.charset = sq.charset // Doit être passé pour l clavier virtuel mathlive
      // j3pAffiche('editeur', 'expression', '&1&', { inputmq1: {}})
      const st = debut + '&1&' + (sq.postchaine !== '' ? '$' + sq.postchaine + '$' : '')
      afficheMathliveDans('editeur', 'expression', st, sq.params)
      // Les 3 lignes c-dessous déplacées ici pour que sq.mtgAppLecteur soit prêt
      // lors de l’appel de onkeyup
      $('#expressioninputmq1').keyup(onkeyup)
      if (sq.charset !== '') {
        mathliveRestrict(inputId, sq.charset)
      }
      focusIfExists(inputId)

      renderMathInDocument({ renderAccessibleContent: '' }) // Indispensable
      parcours.finEnonce()
    }).catch(j3pShowError)
  }

  function onLoadDatas (datasSection) {
    // Attention, this est window...
    sq.txt = datasSection.txt
    sq.entete = datasSection.entete || ''
    sq.width = Number(datasSection.width ?? 780)
    sq.height = Number(datasSection.height ?? 900)
    sq.postchaine = datasSection.postchaine || ''
    sq.symbexact = datasSection.symbexact
    sq.symbnonexact = datasSection.symbnonexact
    sq.numero = datasSection.numero // Le nombre de paramètres LaTeX dans le texte
    // ?? retourne l’opérande de gauche si !== undefined && !== null, celle de droite sinon
    const listeBoutons = []
    // boutons avec propriétés à true par défaut
    const tabBoutons1 = ['puissance', 'fraction']
        ;['btnPuis', 'btnFrac'].forEach((p, i) => {
      const b = Boolean(datasSection[p] ?? true)
      if (b) listeBoutons.push(tabBoutons1[i])
    })
    // boutons avec false par défaut
    const tabBoutons2 = ['pi', 'racine', 'exp', 'ln', 'log', 'sin', 'cos', 'tan', 'abs', 'conj', 'integ', 'differential', 'prim']
        ;['btnPi', 'btnRac', 'btnExp', 'btnLn', 'btnLog', 'btnSin', 'btnCos', 'btnTan', 'btnAbs', 'btnConj', 'btnInteg', 'btnDiff', 'btnPrim'].forEach((p, i) => {
      const b = Boolean(datasSection[p] ?? false)
      if (b) listeBoutons.push(tabBoutons2[i])
    })
    sq.listeBoutons = listeBoutons
    sq.boutonsMathQuill = listeBoutons.length !== 0
    sq.bigSize = Boolean(datasSection.bigSize) ?? false
    // string vide par défaut
    ;['entete', 'consigne1', 'consigne2', 'consigne3', 'consigne4', 'variables', 'charset'].forEach(p => {
      sq[p] = datasSection[p] ?? ''
    })
    sq.titre = datasSection.titre
    sq.param = datasSection.param // Chaine contenant les paramètres autorisés
    initMtg()
  }

  function initDom () {
    setMathliveCss('MepMG')
    if (sq.titre) parcours.afficheTitre(sq.titre)

    j3pAddElt(parcours.zones.MG, 'div', '', {
      id: 'conteneur',
      style: parcours.styles.etendre('toutpetit.enonce', { padding: '6px' })
    })
    j3pAddElt('conteneur', 'div', '', { id: 'enonce' })
    j3pAddElt('conteneur', 'div', '', {
      id: 'formules',
      style: parcours.styles.petit.enonce
    })
    if (sq.bigSize) $('#formules').css('font-size', '26px')
    j3pAddElt('conteneur', 'div', '', { id: 'info' })
    afficheNombreEssaisRestants()
    j3pAddElt('conteneur', 'div', '', { id: 'conteneurbouton' })
    j3pAjouteBouton('conteneurbouton', 'boutonrecopier', 'MepBoutonsRepere', 'Recopier réponse précédente',
      recopierReponse)
    j3pElement('boutonrecopier').style.display = 'none'
    j3pAddElt('conteneur', 'div', '', { id: 'editeur' }) // Le div qui contiendra l’éditeur MathQuill
    if (sq.bigSize) $('#editeur').css('font-size', '30px')
    $('#editeur').css('padding-top', '10px') // Pour laisser un peu de place au-dessus de l’éditeur
    j3pAddElt('conteneur', 'div', '', { id: 'boutonsmathquill' })
    $('#boutonsmathquill').css('display', 'inline-block')
    $('#boutonsmathquill').css('padding-top', '10px') // Pour laisser un peu de place au-dessus des boutons
    if (sq.boutonsMathQuill) j3pPaletteMathquill('boutonsmathquill', inputId, { liste: sq.listeBoutons })
    j3pAddElt('conteneur', 'div', '', { id: 'divSolution' })
    $('#divSolution').css('display', 'none')
    j3pAddElt('conteneur', 'div', '', { id: 'divmtg32' })
    j3pCreeSVG('divmtg32', {
      id: 'mtg32svg',
      width: sq.width,
      height: sq.height
    })
    const style = parcours.styles.petit.correction
    style.marginTop = '1.5em'
    style.marginLeft = '0.5em'
    j3pAddElt(parcours.zonesElts.MD, 'div', '', { id: 'correction', style })
  }

  // début du code principal de la section

  /** @type {Parcours} */
  const parcours = this

  switch (this.etat) {
    case 'enonce': {
      // code exécuté au lancement de la section
      if (this.debutDeLaSection) {
        this.validOnEnter = false // ex donneesSection.touche_entree
        // compteur. Si   numEssai > nbchances, on corrige
        sq.numEssai = 1

        sq.nbexp = 0
        sq.reponse = -1
        sq.nbEssais = parseInt(parcours.donneesSection.nbEssais)
        sq.nbchances = parseInt(parcours.donneesSection.nbchances)
        sq.simplifier = parcours.donneesSection.simplifier
        if (sq.simplifier === undefined) {
          sq.simplifier = true
        }
        sq.indicationfaute = parcours.donneesSection.indicationfaute // true si indication quand certaines fautes sont faites et appui sur Entrée
        sq.variables = 'xt' // Le paramètre variables n’est jamais utilisé pour cette section
        // Construction de la page
        this.construitStructurePage({
          structure: 'presentation1bis',
          ratioGauche: 0.75
        })

        if (sq.nbchances > sq.nbEssais) sq.nbchances = sq.nbEssais
        // faut gérer sous-dossier ou pas…
        const sep = this.donneesSection.ex.includes('/') ? '-' : '/'
        j3pImporteAnnexe('squelettes-mtg32' + sep + this.donneesSection.ex + '.js')
        // onLoadDatas appelera initDom qui appelera initMtg qui appelera finEnonce
          .then(onLoadDatas)
          .catch(j3pShowError)
      } else {
        if (!sq.mtgAppLecteur) {
          // ça devrait plus arriver, mais au cas où
          j3pNotify('case enonce rappelé avant finEnonce du premier appel')
          j3pShowError('Problème de chargement, il est encore en cours, c’est trop tôt pour rappeller un deuxième essai.')
          return
        }
        j3pEmpty('correction')
        sq.numEssai = 1
        cacheSolution()
        $('#editeur').css('display', 'block')
        $('#boutonsmathquill').css('display', 'inline-block')

        sq.mtgAppLecteur.removeDoc('mtg32svg')
        sq.mtgAppLecteur.addDoc('mtg32svg', sq.txt, true)

        if (sq.aleat) {
          for (let i = 1; i <= sq.nbParamAleat; i++) {
            sq.mtgAppLecteur.giveFormula2('mtg32svg', 'r' + i, sq['tab' + i][this.questionCourante - 1])
          }
        }
        if (sq.param !== undefined) {
          for (const lettre of lettres) {
            if (sq.param.indexOf(lettre) !== -1) {
              const par = this.donneesSection[lettre]
              if (par !== 'random') sq.mtgAppLecteur.giveFormula2('mtg32svg', lettre, par)
            }
          }
        }

        sq.mtgAppLecteur.calculate('mtg32svg', true) // true pour que les calculs alèatoires soient réinitialisés
        j3pEmpty('enonce')
        j3pEmpty('formules')
        j3pEmpty('info')
        sq.nbexp = 0
        sq.reponse = -1
        const nbParam = parseInt(sq.numero)
        const t = 'abcd'
        const param = {}
        for (let i = 0; i < nbParam; i++) {
          const code = sq.mtgAppLecteur.getLatexCode('mtg32svg', i)
          if (i === 0) sq.aCalculer = code.replace(/\n/g, ' ')
          param[t.charAt(i)] = code
        }
        param.e = String(sq.nbEssais)
        sq.params = param
        // j3pAffiche('editeur', 'expression', '&1&', { inputmq1: {}})
        const chaine = sq.consigne1 + (sq.simplifier ? sq.consigne3 : sq.consigne2) + sq.consigne4
        afficheMathliveDans('enonce', 'texte', chaine, param)
        j3pDetruit('expression')
        const debut = sq.entete !== '' ? '$' + sq.entete + sq.symbexact + '$ ' : '$' + sq.aCalculer + sq.symbexact + '$ '
        sq.params.style = { fontSize: '24px' }
        sq.params.listeBoutons = sq.listeBoutons
        sq.params.charset = sq.charset // Doit être passé pour l clavier virtuel mathlive
        const st = debut + '&1&' + (sq.postchaine !== '' ? '$' + sq.postchaine + '$' : '')
        afficheMathliveDans('editeur', 'expression', st, sq.params)
        $('#expressioninputmq1').keyup(onkeyup)
        if (sq.charset !== '') {
          mathliveRestrict(inputId, sq.charset)
        }
        afficheNombreEssaisRestants()
        j3pElement('info').style.display = 'block'
        sq.mtgAppLecteur.display('mtg32svg') // Pour tout réafficher
        // Pour revenir en haut de page
        try {
          window.location.hash = 'MepMG'
          j3pElement('MepMG').scrollIntoView()
        } catch (e) {
          console.warn(e) // pas normal mais pas grave
        }
        // On vide le contenu de l’éditeur MathQuill et on lui donne le focus.
        j3pElement(inputId).value = ''
        focusIfExists(inputId)
        this.finEnonce()
      }

      break // case "enonce":
    }
    case 'correction': {
      let bilanreponse = ''
      let simplifieri = false
      // On regarde si le champ d’édition est rempli même si l’utilisateur
      // n’a pas appuyé sur la touche Entrée.
      const ch = getMathliveValue(inputId)
      if (ch !== '') {
        const resul = traiteMathlive(ch)
        const repcalcul = resul.res
        const valide = resul.valide && sq.mtgAppLecteur.syntaxValidation('mtg32svg', 'rep', repcalcul, true)
        if (valide) {
          const returnNecessaire = validation(false)
          if (returnNecessaire) return // Sésaparcours : Ajout ici d’un return indispensable depuis les modifs faites par Daniel
        } else {
          bilanreponse = 'incorrect'
        }
      } else {
        if (sq.nbexp === 0) bilanreponse = 'incorrect'
      }
      const res1 = sq.reponse
      if (bilanreponse !== 'incorrect') {
        if (sq.simplifier) { // Si on demande le calcul simplifié
          if (res1 === 1) {
            bilanreponse = 'exact'
            simplifieri = true
          } else {
            if (res1 === 2) {
              bilanreponse = 'exactpasfini'
            } else {
              bilanreponse = 'erreur'
            }
          }
        } else {
          if ((res1 === 1) || (res1 === 2)) {
            if ((res1 === 2) && sq.repMathQuill.contientIntegOuPrim) {
              bilanreponse = 'exactpasfini'
            } else {
              bilanreponse = 'exact'
            }
            if (res1 === 1) {
              simplifieri = true
            }
          } else {
            bilanreponse = 'erreur'
          }
        }
      }

      // A cause de la limite de temps :
      if (this.isElapsed) { // limite de temps
        j3pElement('correction').style.color = this.styles.cfaux
        j3pElement('boutonrecopier').style.display = 'none'
        j3pElement('info').style.display = 'none'
        if (sq.indicationfaute) {
          sq.mtgAppLecteur.giveFormula2('mtg32svg', 'faute', '0')
          sq.mtgAppLecteur.calculate('mtg32svg', false)
          sq.mtgAppLecteur.executeMacro('mtg32svg', 'masquerFaute')
        }
        $('#editeur').css('display', 'none')
        $('#boutonsmathquill').css('display', 'none')
        afficheSolution(false)
        sq.mtgAppLecteur.executeMacro('mtg32svg', 'solution')
        j3pAddContent('correction', tempsDepasse, { replace: true })
        j3pAddContent('correction', textes.solutionIci)
        return this.finCorrection('navigation', true)
      }

      if (bilanreponse === 'incorrect') {
        j3pElement('correction').style.color = this.styles.cfaux
        j3pAddContent('correction', textes.fauteSyn, { replace: true })
        marqueEditeurPourErreur()
        focusIfExists(inputId)
        // on reste en correction
        return this.finCorrection()
      }

      // Une réponse a été saisie
      if (bilanreponse === 'exact') {
        // Bonne réponse
        // sq.mtgAppLecteur.setActive("mtg32svg", false);
        this.score++
        j3pElement('correction').style.color = this.styles.cbien
        if (simplifieri) {
          j3pAddContent('correction', cBien, { replace: true })
        } else {
          j3pAddContent('correction', textes.bienMais, { replace: true })
        }
        j3pElement('boutonrecopier').style.display = 'none'
        j3pElement('info').style.display = 'none'
        if (sq.indicationfaute) {
          sq.mtgAppLecteur.executeMacro('mtg32svg', 'masquerFaute')
        }
        afficheSolution(true)
        sq.mtgAppLecteur.executeMacro('mtg32svg', 'solution')
        $('#editeur').css('display', 'none')
        $('#boutonsmathquill').css('display', 'none')
        return this.finCorrection('navigation', true)
      }

      // mauvaise réponse
      j3pElement('correction').style.color = this.styles.cfaux
      sq.numEssai++
      if (bilanreponse === 'exactpasfini') {
        j3pAddContent('correction', textes.exactPasFini, { replace: true })
      } else {
        j3pAddContent('correction', cFaux, { replace: true })
      }
      afficheNombreEssaisRestants()
      // On donne le focus à l’éditeur MathQuill
      focusIfExists(inputId)
      // S’il y a plus de deux chances,on appelle à nouveau le bouton Valider

      if ((sq.nbexp < sq.nbEssais) && (sq.numEssai <= sq.nbchances)) {
        j3pAddContent('correction', '<br>' + '<br>' + essaieEncore)
        // indication éventuelle ici
        // this.Sectionsquelettemtg32_Calc_Param_MathQuill.nbExp += 1;
        // on reste en correction
        return this.finCorrection()
      }

      // Erreur au dernier essai
      // sq.mtgAppLecteur.setActive("mtg32svg", false);
      j3pElement('boutonrecopier').style.display = 'none'
      j3pElement('info').style.display = 'none'
      if (sq.indicationfaute) {
        sq.mtgAppLecteur.executeMacro('mtg32svg', 'masquerFaute')
      }
      afficheSolution(false)
      sq.mtgAppLecteur.executeMacro('mtg32svg', 'solution')
      $('#editeur').css('display', 'none')
      $('#boutonsmathquill').css('display', 'none')
      j3pAddContent('correction', textes.solutionIci)
      this.finCorrection('navigation', true)
      break
    } // case correction

    case 'navigation':
      if (!this.sectionTerminee()) {
        this.etat = 'enonce'
      }
      this.finNavigation()
      break // case "navigation":
  }
}
