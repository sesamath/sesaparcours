/**
 * @author Yves Biton
 * @since 2019
 * @fileOverview
 * section squelettemtg32_Calc_Param_Multi_Edit_Apres_Interm
 * Squelette demandant de calculer une expression ou plusieurs expressions.
 * On peut éventuellement spécifier des paramètres a,b,c,d,e,f,g,h,j,k,n,n,p.
 * Utilise un fichier annexe dont le nom est contenu dans le paramètre ex.
 * Ce fichier annexe contient une variable nbCalc qui contient le nombre d’expressions que l’élève doit calculer simultanément.
 * Il contient aussi une variable formulaire qui est une chaîne de caractères contenant éventuellement du LaTeX et contenant
 * une référence aux éditeurs MathQuill utilisés. Par exemple, si nbCalc  vaut 2, la chaine devra contenir deux sous-chaînes
 * "edit1" et "edit2" qui correspondront aux éditeurs MathQuill.
 * Quand on vérifiera la validité de la réponse de l’élève, on mettra (dabns le cas où nbCalc vaut 2, par exemple, la réponse
 * contenue dans l’éditeur 1 dans le calcul rep1 et, après recalcul de la figure mtg32, le calcul resolu1 contiendra 1
 * si la réponse contenue dans le premier éditeur est bonne, 2 si elle est bonne mais le calcul n’est pas fini et 0 si la réponse est fausse.
 * Si la paramètre simplifier est à false, un calcul exact mais pas fini sera accepté.
 * Si la figure contenue  dans ex contient un paramètre nbVar , alors dans le cas ou nbrepetitions > 1, on donne pour i compris entre 1 et npParamAleat
 * au calcul de la figure nommé "r"+i (par exemple r1 et r2 si nbParamAleat vaut 2) des valeurs comprises entre 0 et "nbcas"+i - 1
 * toutes différentes (sauf si nbrepetitions > b=nbcas auquel cas au bout de nbcas répétitions il y aura nécessairement une valeur de r déjà prise.
 * Le paramètre nbLatex contient le nombre d’affichages LaTeX qui doivent être récupérés de la figure pour affichage dans l’énoncé.
 */
import $ from 'jquery'
import { j3pAddElt, j3pAjouteBouton, j3pDetruit, j3pDiv, j3pElement, j3pEmpty, j3pImporteAnnexe, j3pPaletteMathquill, j3pShowError } from 'src/legacy/core/functions'
import { MathfieldElement, renderMathInDocument } from 'mathlive'
import { j3pCreeFenetres, j3pToggleFenetres } from 'src/legacy/core/functionsJqDialog'
import { getMtgApp } from 'src/lib/outils/mathgraph'
import { j3pCreeSVG } from 'src/legacy/core/functionsSvg'
import { cleanLatexForMl, mathliveRestrict } from 'src/lib/outils/mathlive/utils'
import { afficheMathliveDans, getMathliveValue } from 'src/lib/outils/mathlive/display'
import { unLatexify } from 'src/lib/mathquill/functions'
import textesGeneriques from 'src/lib/core/textes'

const { cBien, cFaux, essaieEncore, tempsDepasse } = textesGeneriques

const structure = 'presentation1bis'

// on définit le dossier des fonts mathlive (il faut le faire très tôt,
// ici c’est bon avec pnpm start mais trop tard pour un build (cf src/lib/player/index.js de la branche draftPlayer)
MathfieldElement.fontsDirectory = '/static/mathlive/fonts'
MathfieldElement.soundsDirectory = '/static/mathlive/sounds'

export const params = {
  outils: ['calculatrice'],
  /**
   * Ce qui est paramétrable dans l’éditeur de graphe
   * @type {LegacySectionParamsParametres}
   */
  parametres: [
    ['nbrepetitions', 2, 'entier', 'aucun'],
    ['limite', 0, 'entier', 'Temps disponible par répétition, en secondes'],
    ['calculatrice', false, 'boolean', 'true pour avoir une calculatrice disponible'],
    ['nbEssais1', 6, 'entier', 'Nombre maximum d’égalités intermédiaires autorisées avant de conclure'],
    ['nbEssais2', 2, 'entier', 'Nombre maximum d’essais  autorisés pour le deuxième calcul'],
    ['simplifier', true, 'boolean', 'true si on exige un forme donnée pour le deuxième calcul, false sinon'],
    ['infoParam', 'Information sur le rôle des paramètres', 'string', 'Information sur l’utilisation des paramètres suivants'],
    ['a', 'random', 'string', 'Valeur de a'],
    ['b', 'random', 'string', 'Valeur de b'],
    ['c', 'random', 'string', 'Valeur de c'],
    ['d', 'random', 'string', 'Valeur de d'],
    ['e', 'random', 'string', 'Valeur de e'],
    ['f', 'random', 'string', 'Valeur de f'],
    ['g', 'random', 'string', 'Valeur de g'],
    ['h', 'random', 'string', 'Valeur de h'],
    ['j', 'random', 'string', 'Valeur de j'],
    ['k', 'random', 'string', 'Valeur de k'],
    ['l', 'random', 'string', 'Valeur de l'],
    ['m', 'random', 'string', 'Valeur de m'],
    ['n', 'random', 'string', 'Valeur de n'],
    ['p', 'random', 'string', 'Valeur de p'],
    ['q', 'random', 'string', 'Valeur de q'],
    ['r', 'random', 'string', 'Valeur de r'],
    ['ex', 'quatrieme/College_Pythagore_Progressif_1', 'string', 'Nom du fichier annexe (xxx/yyy => sectionsAnnexes/squelettes-mtg32-xxx/yyy.js)']
  ]
}

// le nom de cette fonction ne sert que pour les stacktrace des erreurs js
export default function squeletteMtg32CalcParamMultiEditApresInterm () {
  const parcours = this
  const sq = this.storage
  /**
   * Chaîne formée des caractères représentant les affichages LaTeX que l’on peut récupérer (ou pas)
   * depuis la figure (le nombre de ces paramètres étant mis dans la propriété nbLatex du fichier annexe)
   * @type {string}
   */
  const caracteresRecupLatex = 'abcdefghijklmnopq'
  /**
   * Chaîne formée des caractères représentant les paramètres qui peuvent être imposés à la figure par l’utilisateur
   * (paramètres à une lettre déclarés dans les parametres de cette section)
   * @type {string}
   */
  const caracteresParametres = 'abcdefghjklmnpqr'

  function onkeyup () {
    const indice = this.indice
    if (sq.marked[indice - 1]) {
      demarqueEditeurPourErreur(indice)
      j3pEmpty('correction')
    }
  }

  function traiteMathlive (ch) {
    // Mathlive ne met pas d’accolades pour les fractions dont le numérateur et le dénominateur sont des entiers
    ch = ch.replace(/\\frac(\d)(\d)/g, '\\frac{$1' + '}{' + '$2}')
    // Ni pour les racines carrées d’un entier
    ch = ch.replace(/\\sqrt(\d)/g, '\\sqrt{$1}')
    // Pour les puissances dont l’exposant est un chiffre, Mathlive ne met pas d’accolades
    ch = ch.replace(/\^([^{])/g, '^{$1}')
    ch = unLatexify(ch)
    // mathlive peut mettre des {} vides
    ch = ch.replace(/\\{\\}/g, '')
    ch = ch.replace(/\^(\d)(\d)/g, '^$1*$2') // On peut avoir par exemple 2^35^4 pour 2^3*5^4
    return sq.mtgAppLecteur.addImplicitMult('mtg32svg', ch)// Traitement des multiplications implicites
  }

  /**
   * Fonction extrayant d’un affichage LaTeX de la figure les lignes de texte et les transformant
   * en une chaîne affichable par MathQuill
   * @param tagLatex : le tag de l’affichage LaTeX de la figure
   * @return {string}
   */
  function extraitDeLatexPourMathlive (tagLatex) {
    const list = sq.mtgAppLecteur.getList('mtg32svg')
    const latex = list.getByTag(tagLatex, true)
    if (latex === null || latex.className !== 'CLatex') return ''
    return cleanLatexForMl(latex.chaineLatex, sq.charset)
  }

  function afficheNombreEssaisRestants () {
    j3pEmpty('info')
    const nbe = sq['nbEssais' + sq.etape] - sq.numEssai + 1
    const chinterm1 = sq.etape === 2 ? 'essai' : 'égalité intermédiaire autorisée'
    const chinterm2 = sq.etape === 2 ? 'essais' : 'égalités intermédiaires autorisées'
    if (nbe === 1) afficheMathliveDans('info', 'texteinfo', 'Il reste un ' + chinterm1, { style: { color: '#7F007F' } })
    else afficheMathliveDans('info', 'texteinfo', 'Il reste ' + nbe + ' ' + chinterm2, { style: { color: '#7F007F' } })
  }

  function boutonfen () {
    j3pToggleFenetres('Calculatrice')
  }

  function marqueEditeurPourErreur (indice) {
    sq.marked[indice - 1] = true
    const mf = j3pElement('expressioninputmq' + indice)
    mf.style.backgroundColor = '#FF9999'
    mf.demarqueErreur = () => {
      if (sq.marked[indice - 1]) {
        demarqueEditeurPourErreur(indice)
      }
    }
  }

  function demarqueEditeurPourErreur (indice) {
    if (!sq.marked[indice - 1]) return
    sq.marked[indice - 1] = false // Par précaution si appel via bouton recopier réponse trop tôt
    const mf = j3pElement('expressioninputmq' + indice)
    mf.style.backgroundColor = 'white'
  }

  function validationEditeurInterm () {
    let res = true
    const rep = getMathliveValue('expressioninputmq1')
    if (rep === '') {
      setTimeout(function () {
        j3pElement('expressioninputmq1').focus()
      })
      return false
    }
    sq.rep = rep
    sq.signeEgalManquant = false
    const tab = rep.split(';')
    if (tab.length + sq.nbCalcInterm > sq.nbEssais1) {
      sq.tropDeCalculsInterm = true
      res = false
    } else {
      for (let j = 0; j < tab.length; j++) {
        const ch = tab[j]
        let nbegal = 0
        for (let k = 0; k < ch.length; k++) {
          if (ch.charAt(k) === '=') nbegal++
        }
        if (nbegal !== 1) {
          res = false
          if (nbegal === 0) sq.signeEgalManquant = true
          break
        } else {
          const chcalcul = traiteMathlive(tab[j])
          const valide = sq.mtgAppLecteur.syntaxValidation('mtg32svg', 'interm', chcalcul, false)
          if (!valide) {
            res = false
          }
        }
      }
      sq.tropDeCalculsInterm = false
    }
    if (!res) {
      marqueEditeurPourErreur(1)
      setTimeout(function () {
        j3pElement('expressioninputmq1').focus()
      })
    } else { sq.nbCalcInterm += tab.length }
    return res
  }

  function validationEditeurs () {
    sq.signeEgalManquant = false
    let res = true
    for (let ind = 1; ind <= sq['nbCalc' + sq.etape]; ind++) {
      const rep = getMathliveValue('expressioninputmq' + ind)
      if (rep === '') {
        res = false
      } else {
        const chcalcul = traiteMathlive(rep)
        const valide = sq.mtgAppLecteur.syntaxValidation('mtg32svg', 'rep' + ind, chcalcul, true)
        if (!valide) {
          res = res && false
          marqueEditeurPourErreur(ind)
          setTimeout(function () {
            j3pElement('expressioninputmq1').focus()
          })
        }
      }
    }
    return res
  }

  function montreEditeurs (bVisible) {
    if (bVisible) $('#info').css('display', 'block')
    $('#editeur').css('display', bVisible ? 'block' : 'none')
    if (sq.boutonsMathQuill) $('#boutonsmathquill').css('display', bVisible ? 'block' : 'none')
    // On vide le contenu de l’éditeur MathQuill et on lui donne le focus.
    if (bVisible) j3pElement('expressioninputmq1').value = ''
    j3pElement('expressioninputmq1').focus()
  }

  function videEditeurs () {
    for (let i = 1; i <= sq['nbCalc' + sq.etape]; i++) {
      j3pElement('expressioninputmq' + i).value = ''
    }
  }

  function creeEditeurs () {
    // sq.formulaire contient ou plusieurs éditeurs MathQuill
    let contenu = sq['formulaire' + sq.etape]
    for (let k = 1; k <= sq['nbCalc' + sq.etape]; k++) contenu = contenu.replace('edit' + k, '&' + k + '&')
    const options = {}
    for (let k = 1; k <= sq['nbCalc' + sq.etape]; k++) options['inputmq' + k] = {}
    const nbParam = parseInt(sq.nbLatex)
    for (let i = 0; i < nbParam; i++) {
      options[caracteresRecupLatex.charAt(i)] = sq.parametres[caracteresRecupLatex.charAt(i)]
    }
    options.charset = sq['charset' + sq.etape]
    options.listeBoutons = sq.listeBoutons
    afficheMathliveDans('editeur', 'expression', contenu, options)
    // On affecte un indice à chaque éditeur ainsi qu’un écouteur de focus
    for (let k = 1; k <= sq['nbCalc' + sq.etape]; k++) {
      j3pElement('expressioninputmq' + k).indice = k
      if (sq['charset' + sq.etape] !== '') {
        mathliveRestrict('expressioninputmq' + k, sq['charset' + sq.etape])
      }
      $('#expressioninputmq' + k).focusin(function () {
        ecoute.call(this)
      })
      j3pElement('expressioninputmq' + k).onkeyup = function (ev) {
        onkeyup.call(this, ev, parcours)
      }
    }
  }

  function afficheReponse (bilan, depasse) {
    const num = sq.numEssai
    const idrep = 'exp' + num
    // let ch = (num > 2) ? '<br>' : ''
    // Avec MathliveAffiche il ne faut pas envoyer une chaîne commençant par un <br> car le premier affichage
    // se fait inline (celui du <br> mais le suivant est dans un <p> ce qui provoque un retour à la ligne)
    // et donc on a par exemple Réponse exacte : suivi d’un retour à la ligne suivi de la réponse
    // alors qu’on veut que tout soit sur une même ligne
    if (num > 2) afficheMathliveDans(sq.etape === 1 ? 'formules' : 'formulesSuite', idrep + 'br', '<br>')
    let contenu
    const style = {} // pour les options de j3pAffiche
    if (sq.etape !== 1) {
      if (bilan === 'exact') {
        contenu = 'Réponse exacte : '
        style.color = parcours.styles.cbien
      } else {
        if (bilan === 'faux') {
          contenu = 'Réponse fausse : '
          style.color = parcours.styles.cfaux
        } else { // Exact mais pas fini
          contenu = 'Exact pas fini : '
          if (depasse) style.couleur = depasse ? parcours.styles.cfaux : '#0000FF'
        }
      }
      afficheMathliveDans(sq.etape === 1 ? 'formules' : 'formulesSuite', idrep + 'deb', contenu, { style })
    }

    let chaine
    if (sq.etape === 1) {
      chaine = ''
      for (let i = 0; i < sq.rep.length; i++) {
        chaine += '$\\textcolor{' + (sq.repExact[i] ? '#0000FF' : parcours.styles.cfaux) + '}{' +
        sq.rep[i] + '}$'
        if (i !== sq.rep.length - 1) chaine += ';'
      }
      afficheMathliveDans('formules', idrep + String(sq.etape),
        j3pElement('formules').innerHTML === '' ? chaine : '<br>' + chaine,
        sq.parametres)
    } else {
      chaine = sq['formulaire' + sq.etape]
      for (let i = 1; i <= sq['nbCalc' + sq.etape]; i++) {
        const st = '\\textcolor{' + (sq.repResolu[i - 1] ? parcours.styles.cbien : (sq.repExact[i - 1] ? '#0000FF' : parcours.styles.cfaux)) + '}{'
        chaine = chaine.replace('edit' + i, '$' + st + sq.rep[i - 1] + '}$')
      }
      afficheMathliveDans('formulesSuite', idrep + String(sq.etape), chaine, sq.parametres)
    }
  }

  /**
   * Fonction affichant la solution s’il existe un affichage LaTeX de tag solution
   * @param{boolean} bexact true si l’élève a entré la bonne réponse, false sinon
   */
  function afficheSolution (bexact) {
    const ch = extraitDeLatexPourMathlive('solution')
    if (ch !== '') {
      $('#divSolution').css('display', 'block').html('')
      afficheMathliveDans('divSolution', 'solution', ch, { style: { color: bexact ? 'green' : 'blue' } })
    }
  }

  function cacheSolution () {
    $('#divSolution').css('display', 'none').html('')
  }

  /**
   * Listener de l’événement focusin
   * @this {HTMLInputElement}
   * @private
   */
  function ecoute () {
    if (sq.boutonsMathQuill) {
      j3pEmpty('boutonsmathquill')
      const indice = this.indice
      j3pPaletteMathquill('boutonsmathquill', 'expressioninputmq' + indice, { liste: sq.listeBoutons, nomdiv: 'palette' })
      // Ligne suivante pour un bon alignement
      $('#palette').css('display', 'inline-block')
    }
  }

  function validation () {
    let rep, j, chcalcul, exact, ch, resolu
    sq.rep = []
    sq.repExact = []
    sq.repResolu = []
    if (sq.etape === 1) {
      rep = getMathliveValue('expressioninputmq1')
      sq.repInterm = rep
      const tab = rep.split(';')
      sq.correct = true
      for (j = 0; j < tab.length; j++) {
        chcalcul = traiteMathlive(tab[j])
        sq.mtgAppLecteur.giveFormula2('mtg32svg', 'interm', chcalcul)
        sq.mtgAppLecteur.calculate('mtg32svg', false)
        exact = sq.mtgAppLecteur.valueOf('mtg32svg', 'vrai') === 1
        // On récupère l’affichage LaTeX de la formule intermédiaire qui doit être le dernier de la figure
        ch = sq.mtgAppLecteur.getLatexCode('mtg32svg', -1)
        if (!exact) ch = ch.replace(/=/g, '\\ne')
        sq.rep.push(ch)
        sq.repExact.push(exact)
      }
      sq.numEssai += tab.length
      if (sq.numEssai < sq.nbEssais1) j3pElement('boutonrecopier').style.display = 'block'
    } else {
      if (sq.numEssai >= 1) {
        j3pElement('boutonrecopier').style.display = 'block'
      }

      for (let ind = 1; ind <= sq['nbCalc' + sq.etape]; ind++) {
        rep = getMathliveValue('expressioninputmq' + ind)
        sq.rep.push(rep)
        chcalcul = traiteMathlive(rep)
        sq.mtgAppLecteur.giveFormula2('mtg32svg', 'rep' + ind, chcalcul)
      }
      sq.mtgAppLecteur.calculate('mtg32svg', false)
      exact = true
      resolu = true
      let auMoinsUnExact = false
      for (let ind = 1; ind <= sq.nbCalc2; ind++) {
        const res = sq.mtgAppLecteur.valueOf('mtg32svg', 'resolu' + ind)
        const ex = sq.mtgAppLecteur.valueOf('mtg32svg', 'exact' + ind)
        resolu = resolu && (res === 1)
        exact = exact && ((res === 1) || (ex === 1))
        sq.repResolu.push(res === 1)
        sq.repExact.push((res === 1) || (ex === 1))
        auMoinsUnExact = auMoinsUnExact || (ex === 1)
      }
      sq.resolu = resolu
      sq.exact = exact && auMoinsUnExact
    }
  }

  function recopierReponse () {
    for (let ind = sq['nbCalc' + sq.etape]; ind > 0; ind--) {
      demarqueEditeurPourErreur(ind)
      j3pElement('expressioninputmq' + ind).value = sq.etape === 1 ? sq.repInterm : sq.rep[ind - 1]
      if (ind === 1) j3pElement('expressioninputmq1').focus()
    }
  }

  function etapeSuivante () {
    sq.etape = 2
    j3pEmpty('correction')
    j3pEmpty('editeur')
    j3pEmpty('enonce')
    j3pEmpty('info')
    j3pElement('boutonetape2').style.display = 'none'
    j3pElement('boutonrecopier').style.display = 'none'
    const st = sq.consigne1 + '<br>' + sq.consigneSuite1 + (sq.simplifier ? sq.consigneSuite3 : sq.consigneSuite2) + sq.consigne4
    // sq.parametres.e = sq['nbEssais' + sq.etape]
    afficheMathliveDans('enonceSuite', 'texte', st, sq.parametres)
    sq.numEssai = 1
    afficheNombreEssaisRestants()
    creeEditeurs()
    montreEditeurs(true)
    j3pElement('expressioninputmq1').focus()
  }

  function initMtg () {
    const mtgOptions = {
      // player only, sans figure initiale ni svg, on gère ça dans initMtg et modif_fig
      loadCoreWithMathJax: true
    }
    getMtgApp('mtg32svg', {}, mtgOptions).then((mtgAppLecteur) => {
      sq.mtgAppLecteur = mtgAppLecteur
      initDom()
      sq.etape = 1
      sq.nbCalcInterm = 0
      let code, par, car, nbrep, ar, tir, nb
      sq.mtgAppLecteur.removeAllDoc()
      sq.mtgAppLecteur.addDoc('mtg32svg', sq.txt, true)
      sq.mtgAppLecteur.calculate('mtg32svg', false)
      const nbvar = sq.mtgAppLecteur.valueOf('mtg32svg', 'nbvar')
      if (nbvar !== -1) {
        nbrep = parcours.donneesSection.nbrepetitions
        sq.aleat = true
        sq.nbParamAleat = nbvar
        for (let i = 1; i <= nbvar; i++) {
          const nbcas = sq.mtgAppLecteur.valueOf('mtg32svg', 'nbcas' + i)
          nb = Math.max(nbrep, nbcas)
          ar = []
          for (let j = 0; j < nb; j++) ar.push(j % nbcas)
          sq['tab' + i] = []
          for (let k = 0; k < nbrep; k++) {
            tir = Math.floor(Math.random() * ar.length)
            sq['tab' + i].push(ar[tir])
            ar.splice(tir, 1)
          }
          sq.mtgAppLecteur.giveFormula2('mtg32svg', 'r' + i, sq['tab' + i][0])
        }
      } else { sq.aleat = false }

      if (sq.param !== undefined) {
        for (let i = 0; i < caracteresParametres.length; i++) {
          car = caracteresParametres.charAt(i)
          if (sq.param.indexOf(car) !== -1) {
            par = parcours.donneesSection[car]
            if (par !== 'random') sq.mtgAppLecteur.giveFormula2('mtg32svg', car, par)
          }
        }
      }

      sq.mtgAppLecteur.calculateAndDisplayAll(true)
      sq.mtgAppLecteur.setActive('mtg32svg', false)
      const nbParam = sq.nbLatex
      const param = {}
      for (let i = 0; i < nbParam; i++) {
        code = sq.mtgAppLecteur.getLatexCode('mtg32svg', i)
        param[caracteresRecupLatex.charAt(i)] = code
      }
      sq.parametres = param // Mémorisation pour l’étape 2

      const st = sq.consigne1 + sq.consigne2 + sq.consigne3 + sq.consigne4
      // j3pEmpty(enonce)
      afficheMathliveDans('enonce', 'texte', st, param)
      creeEditeurs()
      if (sq.boutonsMathQuill) {
        j3pPaletteMathquill('boutonsmathquill', 'expressioninputmq1', { liste: sq.listeBoutons, nomdiv: 'palette' })
        // Ligne suivante pour un bon alignement
        $('#palette').css('display', 'inline-block')
      } else {
        j3pDetruit('boutonsmathquill')
      }
      j3pElement('boutonetape2').style.display = 'block'
      afficheNombreEssaisRestants()
      j3pElement('expressioninputmq1').focus()
      parcours.finEnonce()
    }).catch(j3pShowError)
  }

  function initDom () {
    renderMathInDocument({ renderAccessibleContent: '' })
    if (sq.titre) parcours.afficheTitre(sq.titre)

    j3pDiv(parcours.zones.MG, { id: 'conteneur', contenu: '', style: parcours.styles.etendre('toutpetit.enonce', { padding: '6px' }) })
    j3pDiv('conteneur', 'enonce', '')
    // var enonce = j3pElement('enonce')
    // j3pAddContent(enonce, 'Chargement en cours…', { replace: true }) // on vide et remplace
    j3pDiv('conteneur', {
      id: 'formules',
      contenu: '',
      style: parcours.styles.petit.enonce
    }) // Contient le formules entrées par l’élève
    if (sq.bigSize) $('#formules').css('font-size', '26px')
    j3pDiv('conteneur', 'enonceSuite', '')
    j3pDiv('conteneur', {
      id: 'formulesSuite',
      contenu: '',
      style: parcours.styles.petit.enonce
    }) // Contient le formules entrées par l’élève

    j3pDiv('conteneur', 'info', '')
    j3pDiv('conteneur', 'conteneurbouton', '')
    j3pAjouteBouton('conteneurbouton', 'boutonrecopier', 'MepBoutonsRepere', 'Recopier réponse précédente',
      recopierReponse)
    j3pElement('boutonrecopier').style.display = 'none'
    j3pDiv('conteneur', 'editeur', '') // Le div qui contiendra la chaine avec le ou les éditeurs MathQuill
    if (sq.bigSize) $('#editeur').css('font-size', '30px')
    $('#editeur').css('padding-top', '10px') // Pour laisser un peu de place au-dessus de l’éditeur
    j3pDiv('conteneur', 'boutonsmathquill', '')
    $('#boutonsmathquill').css('display', 'inline-block').css('padding-top', '10px') // Pour laisser un peu de place au-dessus des boutons
    j3pDiv('conteneur', 'conteneurboutonetape2', '')
    j3pAjouteBouton('conteneurboutonetape2', 'boutonetape2', 'MepBoutonsRepere', 'Passer à la réponse finale',
      etapeSuivante)
    // On n’affiche le bouton de passage à l’étape 2 que quand la figure est prête dponc dans initMtg
    $('#boutonetape2').css('display', 'none').css('margin-top', '5px')
    j3pDiv('conteneur', 'divSolution', '')
    $('#divSolution').css('display', 'none')
    j3pDiv('conteneur', 'divmtg32', '')
    j3pCreeSVG('divmtg32', {
      id: 'mtg32svg',
      width: 800,
      height: 900
    })
    const style = parcours.styles.petit.correction
    style.marginTop = '1.5em'
    style.marginLeft = '0.5em'
    j3pAddElt(parcours.zonesElts.MD, 'div', '', { id: 'correction', style })
  }

  switch (this.etat) {
    case 'enonce':
      // code exécuté au lancement de la section
      if (this.debutDeLaSection) {
        // Construction de la page
        parcours.construitStructurePage(structure)

        // compteur.
        sq.numEssai = 1
        sq.nbEssais1 = parseInt(parcours.donneesSection.nbEssais1)
        sq.nbEssais2 = parseInt(parcours.donneesSection.nbEssais2)
        sq.simplifier = parcours.donneesSection.simplifier
        sq.nbexp = 0
        sq.reponse = -1
        if (sq.simplifier === undefined) sq.simplifier = true
        if (parcours.donneesSection.calculatrice) {
          j3pDiv('MepMD', 'emplacecalc', '')
          this.fenetresjq = [{ name: 'Calculatrice', title: 'Une calculatrice', left: 600, top: 10 }]
          j3pCreeFenetres(this)
          j3pAjouteBouton('emplacecalc', 'BCalculatrice', 'MepBoutons', 'Calculatrice', boutonfen)
        }
        // faut gérer sous-dossier ou pas…
        const sep = this.donneesSection.ex.includes('/') ? '-' : '/'
        j3pImporteAnnexe('squelettes-mtg32' + sep + this.donneesSection.ex + '.js').then(function (datasSection) {
          // Attention, le this est ici window...
          sq.txt = datasSection.txt
          sq.nbLatex = Number(datasSection.nbLatex ?? 0) // Le nombre de paramètres LaTeX dans le texte
          if (!Number.isInteger(sq.nbLatex) || sq.nbLatex < 0) throw Error(`Données du fichier annexe ${parcours.donneesSection.ex} invalide`)
          // ?? retourne l’opérande de gauche si !== undefined && !== null, celle de droite sinon
          const listeBoutons = []
          // boutons avec propriétés à true par défaut
          const tabBoutons1 = ['puissance', 'fraction']
          ;['btnPuis', 'btnFrac'].forEach((p, i) => {
            const b = Boolean(datasSection[p] ?? true)
            if (b) listeBoutons.push(tabBoutons1[i])
          })
          // boutons avec false par défaut
          const tabBoutons2 = ['pi', 'racine', 'exp', 'ln', 'sin', 'cos', 'tan', 'abs']
          ;['btnPi', 'btnRac', 'btnExp', 'btnLn', 'btnSin', 'btnCos', 'btnTan', 'btnAbs'].forEach((p, i) => {
            const b = Boolean(datasSection[p] ?? false)
            if (b) listeBoutons.push(tabBoutons2[i])
          })
          sq.listeBoutons = listeBoutons
          sq.boutonsMathQuill = listeBoutons.length !== 0
          sq.consigne1 = datasSection.consigne1 ?? ''
          sq.consigne2 = datasSection.consigne2 ?? ''
          sq.consigne3 = datasSection.consigne3 ?? ''
          sq.consigne4 = datasSection.consigne4 ?? ''
          sq.titre = datasSection.titre ?? ''
          sq.bigSize = Boolean(datasSection.bigSize ?? false)
          sq.nbCalc1 = 1
          sq.formulaire1 = datasSection.formulaire1 // Contient la chaine avec les éditeurs.
          sq.param = datasSection.param ?? '' // Chaine contenant les paramètres autorisés
          sq.consigneSuite1 = datasSection.consigneSuite1 ?? ''
          sq.consigneSuite2 = datasSection.consigneSuite2 ?? ''
          sq.consigneSuite3 = datasSection.consigneSuite3 ?? ''
          sq.consigneSuite4 = datasSection.consigneSuite4 ?? ''
          sq.nbCalc2 = datasSection.nbCalc2
          sq.formulaire2 = datasSection.formulaire2 // Contient la chaine avec les éditeurs.
          sq.charset1 = datasSection.charset1 ?? '' // Le set de caractères utilisés dans l’éditeur
          sq.charset2 = datasSection.charset2 ?? '' // Le set de caractères utilisés dans l’éditeur
          sq.marked = []
          for (let k = 0; k < sq.nbCalc; k++) {
            sq.marked[k] = false
          }
          initMtg()
        }).catch(error => {
          console.error(error)
          j3pShowError('Impossible de charger les données de cette exercice')
        })
      } else {
        cacheSolution()
        j3pEmpty('correction')
        sq.numEssai = 1
        sq.etape = 1
        sq.nbCalcInterm = 0
        videEditeurs()
        j3pElement('boutonetape2').style.display = 'block'
        montreEditeurs(true)
        sq.mtgAppLecteur.removeDoc('mtg32svg')
        sq.mtgAppLecteur.addDoc('mtg32svg', sq.txt, true)
        if (sq.aleat) {
          for (let i = 1; i <= sq.nbParamAleat; i++) {
            sq.mtgAppLecteur.giveFormula2('mtg32svg', 'r' + i, sq['tab' + i][this.questionCourante - 1])
          }
        }
        if (sq.param !== undefined) {
          for (let i = 0; i < caracteresParametres.length; i++) {
            const car = caracteresParametres.charAt(i)
            if (sq.param.indexOf(car) !== -1) {
              const par = parcours.donneesSection[car]
              if (par !== 'random') sq.mtgAppLecteur.giveFormula2('mtg32svg', car, par)
            }
          }
        }

        sq.mtgAppLecteur.calculate('mtg32svg', true) // true pour que les calculs aléatoires soient réinitialisés
        j3pEmpty('enonce')
        j3pEmpty('formules')
        j3pEmpty('info')
        j3pEmpty('enonceSuite')
        j3pEmpty('formulesSuite')

        sq.nbexp = 0
        sq.reponse = -1
        const nbParam = sq.nbLatex
        const param = {}
        for (let i = 0; i < nbParam; i++) {
          param[caracteresRecupLatex.charAt(i)] = sq.mtgAppLecteur.getLatexCode('mtg32svg', i)
        }
        sq.parametres = param
        const st = sq.consigne1 + sq.consigne2 + sq.consigne3 + sq.consigne4
        afficheMathliveDans('enonce', 'texte', st, param)
        afficheNombreEssaisRestants()
        j3pElement('info').style.display = 'block'

        j3pEmpty('editeur')
        creeEditeurs()
        sq.mtgAppLecteur.display('mtg32svg') // Pour tout réafficher
        sq.mtgAppLecteur.setActive('mtg32svg', false)
        try {
          j3pElement('MepMG').scrollIntoView()
        } catch (e) {
          console.error(e)
          // peu de chance d’atterrir ici, faudrait tomber sur un navigateur qui plante là-dessus mais il doit plus y en avoir des masses
          // cf https://developer.mozilla.org/fr/docs/Web/API/Element/scrollIntoView#compatibilit%C3%A9_des_navigateurs
          // au cas où on laisse ça, le hash positionne le scroll pour afficher à l’écran l’élément html ayant pour id le hash,
          // si de début de #MepMG était sorti en haut ça remonte pour l’afficher, si le bas est sorti ça descend
          // Mais dans j3p ça sert vraiment à rien car #MepMG est toujours à l’écran, et ce qu’on veut c’est que le scroll à l’intérieur de MepMG
          window.location.hash = 'MepMG'
        }
        j3pElement('expressioninputmq1').focus()
        this.finEnonce()
      }
      break // case "enonce":

    case 'correction': {
      let bilanReponse = ''
      let simplificationPossible = false
      if (sq.etape === 1 ? validationEditeurInterm() : validationEditeurs()) {
        validation()
        if (sq.etape === 1) {
          bilanReponse = 'exact'
        } else {
          if (sq.simplifier) {
            if (sq.resolu) { bilanReponse = 'exact' } else {
              if (sq.exact) {
                bilanReponse = 'exactPasFini'
                simplificationPossible = true
              } else { bilanReponse = 'faux' }
            }
          } else {
            if (sq.resolu || sq.exact) {
              bilanReponse = 'exact'
              if (!sq.resolu) simplificationPossible = true
            } else { bilanReponse = 'faux' }
          }
        }
      } else {
        bilanReponse = 'incorrect'
      }
      // A cause de la limite de temps :
      if (this.isElapsed) { // limite de temps
        montreEditeurs(false)
        j3pElement('correction').style.color = this.styles.cfaux
        j3pElement('boutonrecopier').style.display = 'none'
        j3pElement('info').style.display = 'none'

        sq.mtgAppLecteur.executeMacro('mtg32svg', 'solution')
        afficheSolution()
        j3pElement('correction').innerHTML = tempsDepasse
        j3pElement('correction').innerHTML += '<br>La solution se trouve ci-contre.'
        this.etat = 'navigation'
        this.sectionCourante()
      } else {
        if (bilanReponse === 'incorrect') {
          j3pElement('correction').style.color = this.styles.cfaux
          j3pElement('correction').innerHTML = sq.tropDeCalculsInterm
            ? 'Trop d’égalités intermédiaires'
            : (sq.signeEgalManquant ? 'Il faut écrire une ou des égalités <br>séparées par des ;' : 'Réponse incorrecte')
        } else {
          // Bonne réponse
          if (bilanReponse === 'exact') {
            if (sq.etape !== 1) {
              this.score += 1
              j3pElement('correction').style.color = this.styles.cbien
              j3pElement('correction').innerHTML = simplificationPossible ? 'C’est bien mais on pouvait  simplifier la réponse' : cBien
              montreEditeurs(false)
              sq.numEssai++ // Pour un affichage correct dans afficheReponse
            }
            afficheReponse('exact', false)
            if (sq.etape === 2) {
              this._stopTimer()
              j3pElement('boutonrecopier').style.display = 'none'
              j3pElement('info').style.display = 'none'
              sq.mtgAppLecteur.executeMacro('mtg32svg', 'solution')
              afficheSolution()
              this.etat = 'navigation'
              this.sectionCourante()
            } else {
              if (sq.numEssai > sq.nbEssais1) { etapeSuivante() } else {
                j3pEmpty('info')
                afficheNombreEssaisRestants()
                videEditeurs()
                setTimeout(function () {
                  j3pElement('expressioninputmq1').focus()
                })
              }
            }
          } else {
            sq.numEssai++
            if (bilanReponse === 'exactPasFini') {
              j3pElement('correction').style.color =
                (sq.numEssai <= this.donneesSection['nbEssais' + sq.etape]) ? '#0000FF' : this.styles.cfaux
              j3pElement('correction').innerHTML = 'Le calcul est bon mais pas écrit sous la forme demandée.'
            } else {
              j3pElement('correction').style.color = this.styles.cfaux
              j3pElement('correction').innerHTML = cFaux
            }
            j3pEmpty('info')
            if (sq.numEssai <= this.donneesSection['nbEssais' + sq.etape]) {
              afficheNombreEssaisRestants()
              videEditeurs()
              afficheReponse(bilanReponse, false)
              // S’il y a plus de deux chances,on appelle à nouveau le bouton Valider
              j3pElement('correction').innerHTML += '<br>' + '<br>' + essaieEncore
              setTimeout(function () {
                j3pElement('expressioninputmq1').focus()
              })
            } else {
              // Erreur au nème essai
              if (sq.etape === 2) {
                this._stopTimer()
                montreEditeurs(false)
                j3pElement('boutonrecopier').style.display = 'none'
                j3pElement('info').style.display = 'none'
                sq.mtgAppLecteur.executeMacro('mtg32svg', 'solution')
                afficheSolution()
                afficheReponse(bilanReponse, true)
                j3pElement('correction').innerHTML += '<br><br>La solution se trouve ci-contre.'
                this.etat = 'navigation'
                this.sectionCourante()
              } else {
                j3pElement('correction').innerHTML += '<br><br>On passe à la question suivante.'
                etapeSuivante()
              }
            }
          }
        }
      }
      this.finCorrection()
      break // case "correction":
    }

    case 'navigation':
      if (!this.sectionTerminee()) {
        this.etat = 'enonce'
      }
      this.finNavigation()
      break // case "navigation":
  }
}
