import $ from 'jquery'
import { MathfieldElement, renderMathInDocument } from 'mathlive'

import { j3pAddElt, j3pAjouteBouton, j3pDetruit, j3pDiv, j3pElement, j3pEmpty, j3pFocus, j3pImporteAnnexe, j3pPaletteMathquill, j3pShowError } from 'src/legacy/core/functions'
import { j3pCreeSVG } from 'src/legacy/core/functionsSvg'

import textesGeneriques from 'src/lib/core/textes'
import { unLatexify } from 'src/lib/mathquill/functions'
import { getMtgApp } from 'src/lib/outils/mathgraph'
import { afficheMathliveDans, getMathliveValue } from 'src/lib/outils/mathlive/display'
import { cleanLatexForMl, mathliveRestrict } from 'src/lib/outils/mathlive/utils'
import { setMathliveCss } from 'src/lib/outils/mathlive/mathlivecss'
import { focusIfExists } from 'src/lib/utils/dom/main'

const { cBien, cFaux, essaieEncore, tempsDepasse } = textesGeneriques

/*
    Yves Biton
    Juillet 2014

    http://localhost:8081/?graphe=[1,"squelettemtg32_Calc_Param_2_Edit",[{pe:">=0",nn:"fin",conclusion:"fin"},{f:"Fact_ax_Carre_Moins_cCarre_Ex1"}]];
    squelettemtg32_Calc_Param_2_Edit
    Squelette demandant de calculer deux expressions.
    L’élève valide ses réponses aussi bien en appuyant sur la touche Entrée que sur le bouton OK.
    On peut éventuellement spécifier des paramètres  a,b,c,d,e,f,g,h,j,k,n,n,p.

    Si la figure contient un calcul nbvar , alors dans le cas ou nbrepetitions > 1, on donne pour i compris entre 1 et nbvar
    au calcul de la figure nommé "r"+i (par exemple r1 et r2 si nbParamAleat vaut 2) des valeurs comprises entre 0 et "nbcas"+i - 1
    toutes différentes (sauf si nbrepetitions > b=nbcas auquel cas au bout de nbcas répétitions il y aura nécessairement une valeur de r déjà prise.

*/
// on définit le dossier des fonts mathlive (il faut le faire très tôt,
// ici c’est bon avec pnpm start mais trop tard pour un build (cf src/lib/player/index.js de la branche draftPlayer)
if (MathfieldElement) { // undefined dans les tests avec happy-dom
  MathfieldElement.fontsDirectory = '/static/mathlive/fonts'
  MathfieldElement.soundsDirectory = '/static/mathlive/sounds'
}

export const params = {
  /**
   * Ce qui est paramétrable dans l’éditeur de graphe
   * @type {LegacySectionParamsParametres}
   */
  parametres: [
    ['nbrepetitions', 1, 'entier', 'aucun'],
    ['limite', 0, 'entier', 'Temps disponible par répétition, en secondes'],
    ['nbEssais', 6, 'entier', 'Nombre d’essais maximum autorisés'],
    ['indicationfaute', false, 'boolean', 'true si on veut que certaines fautes soient expliquées après appui sur Entrée'],
    ['simplifier', true, 'boolean', 'true si on exige un forme donnée, false sinon'],
    ['a', 'random', 'string', 'Valeur de a'],
    ['b', 'random', 'string', 'Valeur de b'],
    ['c', 'random', 'string', 'Valeur de c'],
    ['d', 'random', 'string', 'Valeur de d'],
    ['e', 'random', 'string', 'Valeur de e'],
    ['f', 'random', 'string', 'Valeur de f'],
    ['g', 'random', 'string', 'Valeur de g'],
    ['h', 'random', 'string', 'Valeur de h'],
    ['j', 'random', 'string', 'Valeur de j'],
    ['k', 'random', 'string', 'Valeur de k'],
    ['l', 'random', 'string', 'Valeur de l'],
    ['m', 'random', 'string', 'Valeur de m'],
    ['n', 'random', 'string', 'Valeur de n'],
    ['p', 'random', 'string', 'Valeur de p'],
    ['q', 'random', 'string', 'Valeur de q'],
    ['r', 'random', 'string', 'Valeur de r'],
    ['ex', 'Lire_Coord_2', 'string', 'Nom du fichier annexe (xxx => squelettes-mtg32/xxx.js et xxx/yyy => squelettes-mtg32-xxx/yyy.js)']
  ]
}

/**
 * section squelettemtg32_Calc_Param_2_Edit
 * @this {Parcours}
 */
export default function main () {
  const parcours = this
  const sq = this.storage
  sq.onkeyup = function (ev, parcours) {
    const indice = this.indice
    if (sq.marked[indice]) {
      demarqueEditeurPourErreur(indice)
    }
    if (ev.keyCode === 13) {
      const rep = getMathliveValue('expression' + indice + 'inputmq1')
      const chcalcul = traiteMathlive(rep)
      const valide = sq.mtgAppLecteur.syntaxValidation('mtg32svg', 'rep1', chcalcul, true)
      if (valide) {
        validation(parcours)
        if (indice === 1) j3pFocus('expression2inputmq1')
      } else {
        marqueEditeurPourErreur(indice)
        j3pElement('correction').style.color = parcours.styles.cfaux
        j3pElement('correction').innerHTML = 'Réponse incorrecte'
      }
    }
  }

  function afficheNombreEssaisRestants () {
    j3pEmpty('info')
    const nbe = sq.nbEssais - sq.numEssai + 1
    if (nbe === 1) {
      afficheMathliveDans('info', 'texteinfo', 'Il reste un essai.', { style: { color: '#7F007F' } })
    } else {
      afficheMathliveDans('info', 'texteinfo', 'Il reste ' + nbe + ' essais.', { style: { color: '#7F007F' } })
    }
  }

  // Désactive le focus clavier pour les boutons d’une palette MathQuill
  function desactiveBoutonsEnfants (nomPalette) {
    const palette = j3pElement(nomPalette)
    for (let i = 0; i < palette.childNodes.length; i++) {
      const btn = palette.childNodes[i]
      $(btn).attr('tabIndex', '-1')
    }
  }

  /**
   * Fonction extrayant d’un affichage LaTeX de la figure les lignes de texte et les transformant
   * en une chaîne affichable par MathQuill
   * @param tagLatex : le tag de l’affichage LaTeX de la figure
   * @return {string}
   */
  function extraitDeLatexPourMathlive (tagLatex) {
    const list = sq.mtgAppLecteur.getList('mtg32svg')
    const latex = list.getByTag(tagLatex)
    if (latex === null || latex.className !== 'CLatex') return ''
    return cleanLatexForMl(latex.chaineLatex)
  }

  /**
   * Fonction affichant la solution s’il existe un affichage LaTeX de tag solution
   * @param{boolean} bexact true si l’élève a entré la bonne réponse, false sinon
   */
  function afficheSolution (bexact) {
    $('#divSolution').css('display', 'block').html('')
    const ch = extraitDeLatexPourMathlive('solution')
    if (ch !== '') {
      afficheMathliveDans('divSolution', 'solution', ch, { style: { color: bexact ? 'green' : 'blue' } })
    }
  }

  function cacheSolution () {
    j3pEmpty('divSolution')
  }

  function marqueEditeurPourErreur (indice) {
    sq.marked[indice] = true
    const mf = j3pElement('expression' + indice + 'inputmq1')
    mf.style.backgroundColor = '#FF9999'
    mf.demarqueErreur = () => {
      if (sq.marked[indice]) {
        demarqueEditeurPourErreur(indice)
      }
    }
  }

  function demarqueEditeurPourErreur (indice) {
    sq.marked[indice] = false
    j3pEmpty('correction')
    const mf = j3pElement('expression' + indice + 'inputmq1')
    mf.style.backgroundColor = 'white'
  }

  function traiteMathlive (ch) {
    // Mathlive ne met pas d’accolades pour les fractions dont le numérateur et le dénominateur sont des entiers
    ch = ch.replace(/\\frac(\d)(\d)/g, '\\frac{$1' + '}{' + '$2}')
    // Ni pour les racines carrées d’un entier
    ch = ch.replace(/\\sqrt(\d)/g, '\\sqrt{$1}')
    // Pour les puissances dont l’exposant est un chiffre, Mathlive ne met pas d’accolades
    ch = ch.replace(/\^([^{])/g, '^{$1}')
    ch = unLatexify(ch)
    // mathlive peut mettre des {} vides
    ch = ch.replace(/\\{\\}/g, '')
    ch = ch.replace(/\^(\d)(\d)/g, '^$1*$2') // On peut avoir par exemple 2^35^4 pour 2^3*5^4
    return sq.mtgAppLecteur.addImplicitMult('mtg32svg', ch)// Traitement des multiplications implicites
  }

  function validationEditeurs () {
    let res = true
    let focusSurErreurDonne = false
    for (let ind = 1; ind <= 2; ind++) {
      const id = 'expression' + ind + 'inputmq1'
      const rep = getMathliveValue(id)
      if (rep === '') {
        res = false
        marqueEditeurPourErreur(ind)
        if (!focusSurErreurDonne) {
          focusIfExists(id)
          focusSurErreurDonne = true
        }
      } else {
        const chcalcul = traiteMathlive(rep)
        const valide = sq.mtgAppLecteur.syntaxValidation('mtg32svg', 'rep' + ind, chcalcul, true)
        if (!valide) {
          res = false
          marqueEditeurPourErreur(ind)
          if (!focusSurErreurDonne) {
            focusIfExists(id)
            focusSurErreurDonne = true
          }
        }
      }
    }
    return res
  }

  function montreEditeurs (bVisible) {
    if (bVisible) $('#info').css('display', 'block')
    for (let i = 1; i <= 2; i++) {
      $('#editeur' + i).css('display', bVisible ? 'block' : 'none')
      if (sq.boutonsMathQuill) $('#boutonsmathquill' + i).css('display', bVisible ? 'block' : 'none')
      // On vide le contenu de l’éditeur MathQuill et on lui donne le focus.
      j3pElement('expression' + i + 'inputmq1').value = ''
    }
  }

  function videEditeurs () {
    for (let i = 1; i <= 2; i++) {
      j3pElement('expression' + i + 'inputmq1').value = ''
    }
  }

  function afficheReponse (bilan, depasse) {
    let ch, coul
    if (bilan === 'exact') {
      ch = 'Réponse exacte : '
      coul = parcours.styles.cbien
    } else {
      if (bilan === 'faux') {
        ch = 'Réponse fausse : '
        coul = parcours.styles.cfaux
      } else { // Réponse exacte mais pas fini
        ch = 'Exact pas fini : '
        if (depasse) {
          coul = parcours.styles.cfaux
        } else {
          coul = '#0000FF'
        }
      }
    }
    const tab = [sq.entete1, sq.entete2]
    for (let i = 1; i <= 2; i++) {
      ch += '$' + tab[i - 1] + '=' + sq.rep[i - 1] + '$'
      if (i === 1) ch += ' , '
    }
    const num = sq.numEssai
    if (num > 2) ch = '<br>' + ch
    const idrep = 'exp' + num
    afficheMathliveDans('formules', idrep, ch, {
      style: {
        color: coul
      }
    })
  }

  function validation () {
    if (sq.numEssai >= 1) {
      j3pElement('boutonrecopier').style.display = 'block'
    }
    sq.rep = []
    for (let ind = 1; ind <= 2; ind++) {
      const rep = getMathliveValue('expression' + ind + 'inputmq1')
      sq.rep.push(rep)
      const chcalcul = traiteMathlive(rep)
      sq.mtgAppLecteur.giveFormula2('mtg32svg', 'rep' + ind, chcalcul)
    }
    sq.mtgAppLecteur.calculate('mtg32svg', false)
    if (sq.indicationfaute) {
      const faute = sq.mtgAppLecteur.valueOf('mtg32svg', 'faute')
      if (faute === 1) {
        sq.mtgAppLecteur.executeMacro('mtg32svg', 'voirFaute')
      } else {
        sq.mtgAppLecteur.executeMacro('mtg32svg', 'masquerFaute')
      }
    }
    const reponse = sq.mtgAppLecteur.valueOf('mtg32svg', 'reponse')
    sq.resolu = reponse === 1
    sq.exact = reponse === 2
  }

  function recopierReponse () {
    demarqueEditeurPourErreur(1)
    demarqueEditeurPourErreur(2)
    j3pElement('expression2inputmq1').value = sq.rep[1]
    j3pElement('expression1inputmq1').value = sq.rep[0]
    focusIfExists('expression1inputmq1')
  }

  function initMtg () {
    setMathliveCss('MepMG')
    const mtgOptions = {
      // player only, sans figure initiale ni svg, on gère ça dans initMtg et modif_fig
      loadCoreWithMathJax: true
    }
    getMtgApp('mtg32svg', {}, mtgOptions).then((mtgAppLecteur) => {
      sq.mtgAppLecteur = mtgAppLecteur
      initDom()

      let code, par, car, i, j, nbrep, ar, tir, nb
      sq.mtgAppLecteur.removeAllDoc()
      sq.mtgAppLecteur.addDoc('mtg32svg', sq.txt, true)
      sq.mtgAppLecteur.calculate('mtg32svg', false)
      sq.nbexp = 0
      sq.reponse = -1
      const ch = 'abcdefghjklmnpqr'
      if (sq.param !== undefined) {
        for (i = 0; i < ch.length; i++) {
          car = ch.charAt(i)
          if (sq.param.indexOf(car) !== -1) {
            par = parcours.donneesSection[car]
            if (par !== 'random') sq.mtgAppLecteur.giveFormula2('mtg32svg', car, par)
          }
        }
        sq.mtgAppLecteur.calculate('mtg32svg', true)
      }
      const nbvar = sq.mtgAppLecteur.valueOf('mtg32svg', 'nbvar')
      if (nbvar !== -1) {
        nbrep = parcours.donneesSection.nbrepetitions
        sq.aleat = true
        sq.nbParamAleat = nbvar
        for (i = 1; i <= nbvar; i++) {
          const nbcas = sq.mtgAppLecteur.valueOf('mtg32svg', 'nbcas' + i)
          nb = Math.max(nbrep, nbcas)
          ar = []
          for (j = 0; j < nb; j++) ar.push(j % nbcas)
          sq['tab' + i] = []
          for (let k = 0; k < nbrep; k++) {
            tir = Math.floor(Math.random() * ar.length)
            sq['tab' + i].push(ar[tir])
            ar.splice(tir, 1)
          }
          sq.mtgAppLecteur.giveFormula2('mtg32svg', 'r' + i, sq['tab' + i][0])
        }
      } else {
        sq.aleat = false
      }
      sq.mtgAppLecteur.calculateAndDisplayAll(true)
      const nbParam = parseInt(sq.nbLatex)
      const t = ['a', 'b', 'c', 'd']
      const param = {}
      for (i = 0; i < nbParam; i++) {
        code = sq.mtgAppLecteur.getLatexCode('mtg32svg', i)
        if (i === 0) {
          sq.aCalculer1 = code
        } else if (i === 1) sq.aCalculer2 = code
        param[t[i]] = code
      }
      param.e = String(sq.nbEssais)
      param.charset = sq.charset
      const st = sq.consigne1 + (sq.simplifier ? sq.consigne3 : sq.consigne2) + sq.consigne4
      afficheMathliveDans('enonce', 'texte', st, param)
      if (sq.entete1 !== '') {
        afficheMathliveDans('debut1', 'acalc1', '$' + sq.entete1 + '$ = ', param)
      } else {
        afficheMathliveDans('debut1', 'acalc1', '$' + sq.aCalculer1 + '=$ ', param)
      }
      if (sq.entete2 !== '') {
        afficheMathliveDans('debut2', 'acalc2', '$' + sq.entete2 + '$ = ', param)
      } else {
        afficheMathliveDans('debut2', 'acalc2', '$' + sq.aCalculer2 + '=$ ', param)
      }
      sq.indicationfaute = parcours.donneesSection.indicationfaute // true si indication quand certaines fautes sont faites et appui sur Entrée

      focusIfExists('expression1inputmq1')
      parcours.finEnonce()
    }).catch(j3pShowError)
  }

  function initDom () {
    renderMathInDocument({ renderAccessibleContent: '' })
    if (sq.titre) parcours.afficheTitre(sq.titre)

    j3pDiv(parcours.zones.MG, {
      id: 'conteneur',
      contenu: '',
      style: parcours.styles.etendre('toutpetit.enonce', { padding: '6px' })
    })
    const style = parcours.styles.petit.correction
    style.marginTop = '1.5em'
    style.marginLeft = '0.5em'
    j3pAddElt(parcours.zonesElts.MD, 'div', '', { id: 'correction', style })
    j3pDiv('conteneur', 'enonce', '')
    j3pDiv('conteneur', {
      id: 'formules',
      contenu: '',
      style: parcours.styles.petit.enonce
    }) // Contient le formules entrées par l’élève
    j3pDiv('conteneur', 'info', '')
    afficheNombreEssaisRestants()
    j3pDiv('conteneur', 'conteneurbouton', '')
    j3pAjouteBouton('conteneurbouton', 'boutonrecopier', 'MepBoutonsRepere', 'Recopier réponse précédente',
      recopierReponse)
    j3pElement('boutonrecopier').style.display = 'none'
    j3pDiv('conteneur', 'editeur1', '') // Le div qui contiendra le premier éditeur MathQuill
    $('#editeur1').css('padding-top', '10px') // Pour laisser un peu de place au-dessus de l’éditeur
    j3pAddElt('editeur1', 'span', '', { id: 'debut1' })
    // j3pAffiche('editeur1', 'debut1', '', {})
    afficheMathliveDans('editeur1', 'expression1', '&1&', {
      charset: sq.charset,
      listeBoutons: sq.listeBoutons
    })
    j3pElement('expression1inputmq1').indice = 1 // Pour connaître dans quel éditeur on est lors de onkeyup
    if (sq.charset !== '') {
      mathliveRestrict('expression1inputmq1', sq.charset)
    }
    j3pDiv('conteneur', 'boutonsmathquill1', '')
    $('#boutonsmathquill1').css('display', 'inline-block').css('padding-top', '10px') // Pour laisser un peu de place au-dessus des boutons
    j3pDiv('conteneur', 'editeur2', '') // Le div qui contiendra le premier éditeur MathQuill
    j3pAddElt('editeur2', 'span', '', { id: 'debut2' })
    // j3pAffiche('editeur2', 'debut2', '', {})
    $('#editeur2').css('padding-top', '10px') // Pour laisser un peu de place au-dessus de l’éditeur
    afficheMathliveDans('editeur2', 'expression2', '&1&', {
      charset: sq.charset,
      listeBoutons: sq.listeBoutons
    })
    j3pElement('expression2inputmq1').indice = 2
    if (sq.charset !== '') {
      mathliveRestrict('expression2inputmq1', sq.charset)
    }
    j3pDiv('conteneur', 'boutonsmathquill2', '')
    $('#boutonsmathquill2').css('display', 'inline-block').css('padding-top', '10px') // Pour laisser un peu de place au-dessus des boutons

    if (sq.boutonsMathQuill) {
      j3pPaletteMathquill('boutonsmathquill1', 'expression1inputmq1', { liste: sq.listeBoutons, nomdiv: 'palette1' })
      // Ligne suivante pour un bon alignement
      $('#palette1').css('display', 'inline-block')
      desactiveBoutonsEnfants('palette1')
    } else {
      j3pDetruit('boutonsmathquill1')
      j3pDetruit('boutonsmathquill2')
    }
    j3pElement('expression1inputmq1').onkeyup = function (ev) {
      sq.onkeyup.call(this, ev, parcours)
    }
    j3pElement('expression1inputmq1').onkeydown = onkeydown
    if (sq.boutonsMathQuill) {
      j3pPaletteMathquill('boutonsmathquill2', 'expression2inputmq1', { liste: sq.listeBoutons, nomdiv: 'palette2' })
      // Ligne suivante pour un bon alignement
      $('#palette2').css('display', 'inline-block')
      desactiveBoutonsEnfants('palette2')
    }
    j3pElement('expression2inputmq1').onkeyup = function (ev) {
      sq.onkeyup.call(this, ev, parcours)
    }
    j3pElement('expression2inputmq1').onkeydown = onkeydown
    j3pAddElt('conteneur', 'div', '', { id: 'divSolution' })
    $('#divSolution').css('display', 'none')
    j3pElement('divSolution').style.fontSize = '24px'

    j3pDiv('conteneur', 'divmtg32', '')
    j3pCreeSVG('divmtg32', {
      id: 'mtg32svg',
      width: sq.width,
      height: sq.height
    })
  }

  switch (this.etat) {
    case 'enonce': {
      // code exécuté au lancement de la section
      if (this.debutDeLaSection) {
        // Construction de la page
        parcours.construitStructurePage('presentation1bis')

        sq.nbEssais = parseInt(parcours.donneesSection.nbEssais)
        // compteur.
        sq.numEssai = 1
        sq.simplifier = parcours.donneesSection.simplifier
        if (sq.simplifier === undefined) {
          sq.simplifier = true
        }
        // faut gérer sous-dossier ou pas…
        const sep = this.donneesSection.ex.includes('/') ? '-' : '/'
        j3pImporteAnnexe('squelettes-mtg32' + sep + this.donneesSection.ex + '.js').then(function (datasSection) {
          // console.log("initsection");
          // Attention, le this devient window...
          sq.txt = datasSection.txt
          sq.entete1 = datasSection.entete1 ?? ''
          sq.entete2 = datasSection.entete2 ?? ''
          sq.symbexact = datasSection.symbexact ?? '='
          sq.symbnonexact = datasSection.symbnonexact ?? '\\ne'
          sq.nbLatex = Number(datasSection.nbLatex ?? 0) // Le nombre de paramètres LaTeX dans le texte
          const listeBoutons = []
          // boutons avec propriétés à true par défaut
          const tabBoutons1 = ['puissance', 'fraction']
          ;['btnPuis', 'btnFrac'].forEach((p, i) => {
            const b = Boolean(datasSection[p] ?? true)
            if (b) listeBoutons.push(tabBoutons1[i])
          })
          // boutons avec false par défaut
          const tabBoutons2 = ['racine', 'pi', 'exp', 'ln', 'sin', 'cos', 'tan', 'abs']
          ;['btnRac', 'btnPi', 'btnExp', 'btnLn', 'btnSin', 'btnCos', 'btnTan', 'btnAbs'].forEach((p, i) => {
            const b = Boolean(datasSection[p] ?? false)
            if (b) listeBoutons.push(tabBoutons2[i])
          })
          sq.listeBoutons = listeBoutons
          sq.boutonsMathQuill = sq.listeBoutons.length !== 0
          sq.consigne1 = datasSection.consigne1 ?? ''
          sq.consigne2 = datasSection.consigne2 ?? ''
          sq.consigne3 = datasSection.consigne3 ?? ''
          sq.consigne4 = datasSection.consigne4 ?? ''
          sq.titre = datasSection.titre ?? ''
          sq.width = Number(datasSection.width ?? 780)
          sq.height = Number(datasSection.height ?? 900)
          sq.param = datasSection.param ?? ''// Chaine contenant les paramètres autorisés
          sq.charset = datasSection.charset ?? ''
          // Important d’initialiser sq.marked avant d’ajouter le clavier virtuel dans le dom,
          // sinon on peut avoir du onkeyup qui plante
          sq.marked = []
          for (let k = 1; k < 3; k++) {
            sq.marked[k] = false
          }
          initMtg()
        }).catch(error => {
          console.error(error)
          j3pShowError('Impossible de charger les données de cette exercice')
        })
        // return;
      } else {
        j3pEmpty('correction')
        cacheSolution()
        sq.numEssai = 1
        videEditeurs()
        montreEditeurs(true)
        sq.mtgAppLecteur.removeDoc('mtg32svg')
        sq.mtgAppLecteur.addDoc('mtg32svg', sq.txt, true)
        if (sq.param !== undefined) {
          const ch = 'abcdefghjklmnpqr'
          for (let i = 0; i < ch.length; i++) {
            const car = ch.charAt(i)
            if (sq.param.indexOf(car) !== -1) {
              const par = parcours.donneesSection[car]
              if (par !== 'random') sq.mtgAppLecteur.giveFormula2('mtg32svg', car, par)
            }
          }
          sq.mtgAppLecteur.calculate('mtg32svg', true)
        }
        if (sq.aleat) {
          for (let i = 1; i <= sq.nbParamAleat; i++) {
            sq.mtgAppLecteur.giveFormula2('mtg32svg', 'r' + i, sq['tab' + i][this.questionCourante - 1])
          }
        }

        sq.mtgAppLecteur.calculate('mtg32svg', true) // true pour que les calculs alèatoires soient réinitialisés
        j3pEmpty('enonce')
        j3pEmpty('debut1')
        j3pEmpty('debut2')
        j3pEmpty('formules')
        j3pEmpty('info')
        sq.nbexp = 0
        sq.reponse = -1
        const nbParam = parseInt(sq.nbLatex)
        const t = ['a', 'b', 'c', 'd']
        const param = {}
        for (let i = 0; i < nbParam; i++) {
          const code = sq.mtgAppLecteur.getLatexCode('mtg32svg', i)
          if (i === 0) {
            sq.aCalculer1 = code
          } else if (i === 1) sq.aCalculer2 = code
          param[t[i]] = code
        }
        param.e = String(sq.nbEssais)
        const st = sq.consigne1 + (sq.simplifier ? sq.consigne3 : sq.consigne2) + sq.consigne4
        afficheMathliveDans('enonce', 'texte', st, param)
        if (sq.entete1 !== '') {
          afficheMathliveDans('debut1', 'acalc1', '$' + sq.entete1 + '$ = ', param)
        } else {
          afficheMathliveDans('debut1', 'acalc1', '$' + sq.aCalculer + '=$ ', param)
        }
        if (sq.entete2 !== '') {
          afficheMathliveDans('debut2', 'acalc2', '$' + sq.entete2 + '$ = ', param)
        } else {
          afficheMathliveDans('debut2', 'acalc2', '$' + sq.aCalculer2 + '=$ ', param)
        }
        afficheNombreEssaisRestants()
        j3pElement('info').style.display = 'block'
        sq.mtgAppLecteur.display('mtg32svg') // Pour tout réafficher
        // Pour revenir en haut de page
        try {
          window.location.hash = 'MepMG'
          j3pElement('MepMG').scrollIntoView()
        } catch (e) {
          console.warn(e) // pas normal mais pas grave
        }
        $('#expression1inputmq1')
        focusIfExists('expression1inputmq1')
        this.finEnonce()
      }

      break // case "enonce":
    }

    case 'correction': {
      let bilanReponse = ''
      let simplificationPossible = false
      if (validationEditeurs()) {
        validation()
        if (sq.simplifier) {
          if (sq.resolu) {
            bilanReponse = 'exact'
          } else {
            if (sq.exact) {
              bilanReponse = 'exactPasFini'
              simplificationPossible = true
            } else {
              bilanReponse = 'faux'
            }
          }
        } else {
          if (sq.resolu || sq.exact) {
            bilanReponse = 'exact'
            if (!sq.resolu) simplificationPossible = true
          } else {
            bilanReponse = 'faux'
          }
        }
      } else {
        bilanReponse = 'incorrect'
      }
      // A cause de la limite de temps :
      if (this.isElapsed) { // limite de temps
        montreEditeurs(false)
        this._stopTimer()
        j3pElement('correction').style.color = this.styles.cfaux
        j3pElement('boutonrecopier').style.display = 'none'
        j3pElement('info').style.display = 'none'
        if (sq.indicationfaute) {
          sq.mtgAppLecteur.giveFormula2('mtg32svg', 'faute', '0')
          sq.mtgAppLecteur.calculate('mtg32svg', false)
          sq.mtgAppLecteur.executeMacro('mtg32svg', 'masquerFaute')
        }
        sq.mtgAppLecteur.executeMacro('mtg32svg', 'solution')
        afficheSolution(true)
        j3pElement('correction').innerHTML = tempsDepasse
        j3pElement('correction').innerHTML += '<br>La solution se trouve ci-contre.'
        this.etat = 'navigation'
        this.sectionCourante()
      } else {
        if (bilanReponse === 'incorrect') {
          j3pElement('correction').style.color = this.styles.cfaux
          j3pElement('correction').innerHTML = 'Réponse incorrecte'
          this.afficheBoutonValider()
        } else {
          // Une réponse a été saisie
          // Bonne réponse
          if (bilanReponse === 'exact') {
            this._stopTimer()
            sq.mtgAppLecteur.setActive('mtg32svg', false)
            this.score++
            j3pElement('correction').style.color = this.styles.cbien
            if (simplificationPossible) {
              j3pElement('correction').innerHTML = 'C’est bien mais on pouvait  simplifier la réponse'
            } else {
              j3pElement('correction').innerHTML = cBien
            }
            j3pElement('boutonrecopier').style.display = 'none'
            j3pElement('info').style.display = 'none'
            if (sq.indicationfaute) {
              sq.mtgAppLecteur.executeMacro('mtg32svg', 'masquerFaute')
            }
            sq.mtgAppLecteur.executeMacro('mtg32svg', 'solution')
            afficheSolution(true)
            montreEditeurs(false)
            sq.numEssai++ // Pour un affichage correct dans afficheReponse
            afficheReponse('exact', false)
            this.etat = 'navigation'
            this.sectionCourante()
          } else {
            sq.numEssai++
            if (bilanReponse === 'exactPasFini') {
              j3pElement('correction').style.color =
                (sq.numEssai <= sq.nbEssais) ? '#0000FF' : this.styles.cfaux
              j3pElement('correction').innerHTML = 'Le calcul est bon mais pas écrit sous la forme demandée.'
            } else {
              j3pElement('correction').style.color = this.styles.cfaux
              j3pElement('correction').innerHTML = cFaux
            }
            j3pEmpty('info')
            if (sq.numEssai <= sq.nbEssais) {
              afficheNombreEssaisRestants()
              videEditeurs()
              afficheReponse(bilanReponse, false)
              // S’il y a plus de deux chances,on appelle à nouveau le bouton Valider
              j3pElement('correction').innerHTML += '<br>' + '<br>' + essaieEncore
              focusIfExists('expression1inputmq1')
            } else {
              // Erreur au nème essai
              this._stopTimer()
              sq.mtgAppLecteur.setActive('mtg32svg', false)
              j3pElement('boutonrecopier').style.display = 'none'
              j3pElement('info').style.display = 'none'
              if (sq.indicationfaute) {
                sq.mtgAppLecteur.executeMacro('mtg32svg', 'masquerFaute')
              }
              sq.mtgAppLecteur.executeMacro('mtg32svg', 'solution')
              afficheSolution(false)
              montreEditeurs(false)
              afficheReponse(bilanReponse, true)
              j3pElement('correction').innerHTML += '<br><br>La solution se trouve ci-contre.'
              this.etat = 'navigation'
              this.sectionCourante()
            }
          }
        }
      }
      // Obligatoire
      this.finCorrection()
      break // case "correction":
    }

    case 'navigation':
      if (!this.sectionTerminee()) {
        this.etat = 'enonce'
      }
      this.finNavigation()
      break // case "navigation":
  }
}
