import $ from 'jquery'
import { j3pAddElt, j3pAjouteBouton, j3pDiv, j3pElement, j3pEmpty, j3pImporteAnnexe, j3pPaletteMathquill, j3pShowError } from 'src/legacy/core/functions'
import { MathfieldElement, renderMathInDocument } from 'mathlive'
import { unLatexify } from 'src/lib/mathquill/functions'
import { getMtgApp } from 'src/lib/outils/mathgraph'
import { j3pCreeSVG } from 'src/legacy/core/functionsSvg'
import { cleanLatexForMl, mathliveRestrict } from 'src/lib/outils/mathlive/utils'
import { afficheMathliveDans, getMathliveValue } from 'src/lib/outils/mathlive/display'
import { setMathliveCss } from 'src/lib/outils/mathlive/mathlivecss'
import { resetKeyboardPosition } from 'src/lib/widgets/mlVirtualKeyboard/VirtualKeyboard'
import textesGeneriques from 'src/lib/core/textes'
import { focusIfExists } from 'src/lib/utils/dom/main'

const { cBien, cFaux, essaieEncore, tempsDepasse } = textesGeneriques

/*
    Yves Biton
    Janvier 2020.  Revu en 2023 pour massage à Mathlive.

    Squelette demandant de calculer une expressionv ectorielle de l’écrire sous la forme la plus simple possible
    On peut éventuellement spécifier des paramètres  a,b,c,d,e,f,g,h,j,k,n,n,p.

    Le fichier annexe doit contenir un membre nomsPoints constenant les noms des points autorisés pour le calcul
    et un membre nomsVect contenant les noms des vecteurs autorisés par le calcul.

    La figure mtg32 doit contenir des calculs complexes ayant le même nom que les points ou vecteurs autorisés
    mais un vecteur nommé i dans l’exercice devra être associé à un calcul complexe nommé i' dans la figure.
    La figure doit contenir un calcul complexe nommé rep et un calcul nommé reponse qui doit valeur 1 si rep contient
    la (ou une des) formule attendue, 2 si le calcul correspond à la réponse mais n’est pas fini et 0 sinon.
    Elle doit aussi contenit un calcul complexe nommé vect0 ayant pour valeur 0 et défini avant le calcul complexe rep.
    Ce calcul représente le vecteur nul.

    Si la figure contient un calcul nommé nbVar , alors dans le cas ou nbrepetitions > 1, on donne pour i compris entre 1 et nbVar
    au calcul de la figure nommé "r"+i (par exemple r1 et r2 si nbParamAleat vaut 2) des valeurs comprises entre 0 et "nbcas"+i - 1
    toutes différentes (sauf si nbrepetitions > nbcas auquel cas au bout de nbcas répétitions il y aura nécessairement une valeur de r déjà prise.

*/

// on définit le dossier des fonts mathlive (il faut le faire très tôt,
// ici c’est bon avec pnpm start mais trop tard pour un build (cf src/lib/player/index.js de la branche draftPlayer)
if (MathfieldElement) { // undefined dans les tests avec happy-dom
  MathfieldElement.fontsDirectory = '/static/mathlive/fonts'
  MathfieldElement.soundsDirectory = '/static/mathlive/sounds'
}

export const params = {
  outils: ['mathquill'],
  /**
   * Ce qui est paramétrable dans l’éditeur de graphe
   * @type {LegacySectionParamsParametres}
   */
  parametres: [
    ['nbrepetitions', 1, 'entier', 'aucun'],
    ['limite', 0, 'entier', 'Temps disponible par répétition, en secondes'],
    ['nbchances', 1, 'entier', 'Nombre d’erreurs possibles (pour tenter de les corriger)'],
    ['nbEssais', 6, 'entier', 'Nombre d’essais maximum autorisés'],
    ['a', 'random', 'string', 'Valeur de a'],
    ['b', 'random', 'string', 'Valeur de b'],
    ['c', 'random', 'string', 'Valeur de c'],
    ['d', 'random', 'string', 'Valeur de d'],
    ['e', 'random', 'string', 'Valeur de e'],
    ['f', 'random', 'string', 'Valeur de f'],
    ['g', 'random', 'string', 'Valeur de g'],
    ['h', 'random', 'string', 'Valeur de h'],
    ['j', 'random', 'string', 'Valeur de j'],
    ['k', 'random', 'string', 'Valeur de k'],
    ['l', 'random', 'string', 'Valeur de l'],
    ['m', 'random', 'string', 'Valeur de m'],
    ['n', 'random', 'string', 'Valeur de n'],
    ['p', 'random', 'string', 'Valeur de p'],
    ['q', 'random', 'string', 'Valeur de q'],
    ['r', 'random', 'string', 'Valeur de r'],
    ['ex', 'seconde/Vecteurs_Eq_Chasles_1', 'string', 'Nom du fichier annexe (xxx => squelettes-mtg32/xxx.js et xxx/yyy => squelettes-mtg32-xxx/yyy.js)']
  ]
}

const inputId = 'expressioninputmq1'

/**
 * section squelettemtg32_Eq_Param_Vect
 * @this {Parcours}
 */
export default function main () {
  const parcours = this
  const sq = this.storage

  function onkeyup (ev, parcours) {
    if (sq.marked) demarqueEditeurPourErreur()
    // touche entrée
    if (ev.keyCode === 13) {
      sq.rep = getMathliveValue(inputId)
      const resMathQuill = traiteMathlive(sq.rep)
      const chcalcul = resMathQuill.res
      const tabNames = []
      for (let k = 0; k < sq.nomsVect.length; k++) {
        let nom = sq.nomsVect[k]
        if (nom === 'i') nom = "i'"
        tabNames.push(nom)
      }
      for (let k = 0; k < sq.nomsPoints.length; k++) {
        let nom = sq.nomsPoints[k]
        if (nom === 'i') nom = "i'"
        tabNames.push(nom)
      }
      sq.resultatSyntaxe = sq.mtgAppLecteur.calcVectOK('mtg32svg', chcalcul, tabNames)
      const valide = resMathQuill.valid && sq.resultatSyntaxe.syntaxOK
      if (valide) {
        if (chcalcul.indexOf('=') === -1) {
          marqueEditeurPourErreur()
          j3pElement('correction').style.color = parcours.styles.cfaux
          j3pElement('correction').innerHTML = 'Il faut utiliser le signe ='
          focusIfExists(inputId)
        } else {
          if (chcalcul.lastIndexOf('=') !== chcalcul.indexOf('=')) {
            marqueEditeurPourErreur()
            j3pElement('correction').style.color = parcours.styles.cfaux
            j3pElement('correction').innerHTML = 'Il ne faut qu’une seule égalité par réponse'
            focusIfExists(inputId)
          } else {
            validation(parcours)
          }
        }
      } else {
        marqueEditeurPourErreur()
        j3pElement('correction').style.color = parcours.styles.cfaux
        j3pElement('correction').inncreesverHTML = 'Faute de syntaxe'
        focusIfExists(inputId)
      }
    }
  } // onkeyup

  function afficheNombreEssaisRestants () {
    j3pEmpty('info')
    const nbe = sq.nbEssais - sq.nbexp
    const nbc = Math.min(sq.nbchances - sq.numEssai + 1, nbe)
    if ((nbe === 1) && (nbc === 1)) {
      afficheMathliveDans('info', 'texteinfo', 'Il reste une validation.', { style: { color: '#7F007F' } })
    } else {
      afficheMathliveDans('info', 'texteinfo', 'Il reste ' + nbe + ' essai(s) et ' + nbc + ' validation(s).', { style: { color: '#7F007F' } })
    }
  }

  /**
   * Fonction extrayant d’un affichage LaTeX de la figure les lignes de texte et les transformant
   * en une chaîne affichable par MathQuill
   * @param tagLatex : le tag de l’affichage LaTeX de la figure
   * @return {string}
   */
  function extraitDeLatexPourMathlive (tagLatex) {
    const list = sq.mtgAppLecteur.getList('mtg32svg')
    const latex = list.getByTag(tagLatex)
    if (latex === null || latex.className !== 'CLatex') return ''
    return cleanLatexForMl(latex.chaineLatex)
  }

  /**
   * Fonction affichant la solution s’il existe un affichage LaTeX de tag solution
   * @param{boolean} bexact true si l’élève a entré la bonne réponse, false sinon
   */
  function afficheSolution (bexact) {
    $('#divSolution').css('display', 'block').html('')
    const ch = extraitDeLatexPourMathlive('solution')
    if (ch !== '') {
      afficheMathliveDans('divSolution', 'solution', ch, { style: { color: bexact ? 'green' : 'blue' } })
    }
  }

  function cacheSolution () {
    $('#divSolution').css('display', 'none').html('')
  }

  function contientSomOuDifPoints (ch) {
    let i, j
    for (i = 0; i < sq.nomsPoints.length; i++) {
      for (j = 0; j < sq.nomsPoints.length; j++) {
        if (ch.indexOf(sq.nomsPoints[i] + '-' + sq.nomsPoints[j]) !== -1) return true
      }
    }
    for (i = 0; i < sq.nomsPoints.length; i++) {
      for (j = i; j < sq.nomsPoints.length; j++) {
        if (ch.indexOf(sq.nomsPoints[i] + '+' + sq.nomsPoints[j]) !== -1) return true
      }
    }
    return false
  }
  function contientVectSousVect (ch) {
    let i, j
    for (i = 0; i < sq.nomsVect.length; i++) {
      for (j = 0; j < sq.nomsVect.length; j++) {
        if (ch.indexOf('\\####{' + sq.nomsVect[i] + sq.nomsVect[j]) !== -1) return true
      }
    }
    return false
  }

  function traiteMathlive (ch) {
    if (ch === '\\overrightarrow{0}') return { res: '0', valid: true, vectNul: true, contientVecsNuls: false }
    ch = ch.replace(/\\frac(\d)(\d)/g, '\\frac{$1' + '}{' + '$2}')
    // Ni pour les racines carrées d’un entier
    ch = ch.replace(/\\sqrt(\d)/g, '\\sqrt{$1}')
    // Pour les puissances dont l’exposant est un chiffre, Mathlive ne met pas d’accolades
    ch = ch.replace(/\^([^{])/g, '^{$1}')
    // mathlive peut mettre des {} vides
    ch = ch.replace(/\\{\\}/g, '')
    ch = ch.replace(/\^(\d)(\d)/g, '^$1*$2') // On peut avoir par exemple 2^35^4 pour 2^3*5^4
    // On regarde si la réponse contient des vecteurs nuls du type vec(AA).
    // Si oui une réponse ne sera pas accepté comme simplifiée
    let contientVecsNuls = false
    let st = sq.nomsPoints
    // ON remplace tous les \overrightarrow par des \#### de façon qu’il n’y ait pas dedans de lettre
    // succeptible d'être un nom de vecteur
    ch = ch.replace(/\\overrightarrow/g, '\\####')

    if (ch.indexOf('\\####{0}') !== -1) contientVecsNuls = true
    else {
      for (let i = 0; i < st.length; i++) {
        const nom = st[i]
        if (ch.indexOf('\\####{' + nom + nom + '}') !== -1) {
          contientVecsNuls = true
          break
        }
      }
    }
    ch = ch.replace(/\\####\{0}/g, '(v000)') // à cause de l’appel à addImplicitMult, on met v000 qui sera remplacé à la fin par vect0
    // Attention : Les parenthèses entourant le v0 sont indispensables
    st = sq.nomsVect
    for (let i = 0; i < st.length; i++) {
      if (st[i] !== 'i') ch = ch.replace(new RegExp('\\\\####{(' + st[i] + ')}', 'g'), '$1')
    }
    const bsomOuDifPt = contientSomOuDifPoints(ch)
    const bvectDeVect = contientVectSousVect(ch)
    ch = ch.replace(/\\####\{i}/g, "i'") // i n’est pas un nom correct dans mtg32
    ch = ch.replace(/\\####\{([a-zA-Z]['"]?)([a-zA-Z]['"]?)\}/g, '($2-$1)')
    const correct = (ch.indexOf('\\####(') === -1) && !bsomOuDifPt && !bvectDeVect
    ch = unLatexify(ch)
    // S’il reste des \vecteur{ c’est que l’expression est incorrecte
    if (correct) ch = sq.mtgAppLecteur.addImplicitMult('mtg32svg', ch)// Traitement des multiplications implicites
    ch = ch.replace(/v000/g, 'vect0')
    return { res: ch, valid: correct, vectNul: false, contientVecsNuls }
  }

  function marqueEditeurPourErreur () {
    sq.marked = true
    const mf = j3pElement(inputId)
    if (!mf) return
    mf.style.backgroundColor = '#FF9999'
    mf.demarqueErreur = () => {
      if (sq.marked) {
        demarqueEditeurPourErreur()
      }
    }
  }

  function demarqueEditeurPourErreur () {
    sq.marked = false
    j3pEmpty('correction')
    const mf = j3pElement(inputId)
    if (!mf) return
    mf.style.backgroundColor = 'white'
  }

  // Le deuxième paramètre est à false seulement quand elle est appelée depuis l’étape de correction
  // Sinon la fonction est appelée par l’éditeur quand l’utilisateur appuie sur la touche entrée avec un seul paramètre
  function validation (parcours, continuer) {
    if (arguments.length <= 1) continuer = true
    if (sq.nbexp === 0) {
      j3pElement('boutonrecopier').style.display = 'block'
    }
    sq.rep = getMathliveValue(inputId)
    // FIXME sq.resultatSyntaxe est parfois undefined (cf https://app.bugsnag.com/sesamath/j3p/errors/64301fd4424e9b000860d141)
    if (!sq.resultatSyntaxe) sq.resultatSyntaxe = {} // pour que la suite ne plante pas
    if (sq.resultatSyntaxe.syntaxVecOK && sq.resultatSyntaxe.isVec) {
      const resMathQuill = traiteMathlive(sq.rep)
      const chcalcul = resMathQuill.res
      sq.mtgAppLecteur.giveFormula2('mtg32svg', 'rep', chcalcul)
      j3pEmpty('correction')
      sq.mtgAppLecteur.calculate('mtg32svg', false)
      sq.reponse = sq.mtgAppLecteur.valueOf('mtg32svg', 'reponse')
      if ((sq.reponse === 1) && resMathQuill.contientVecsNuls) sq.reponse = 2 // Pour ne pas accepter une réponse avec des vec(AA)
    } else sq.reponse = 4 // Considéré comme faux si erreur de syntaxe sur les vecteurs
    const idrep = 'exp' + sq.nbexp
    sq.nbexp++
    const chdeb = sq.nbexp === 1 ? '' : '<br>'
    if (sq.resultatSyntaxe.syntaxVecOK && sq.resultatSyntaxe.isVec && sq.reponse >= 1) {
      afficheMathliveDans('formules', idrep, chdeb + '$' + ' ' + sq.symbexact +
          ' ' + sq.rep + '$', {
        style: {
          color: '#0000FF'
        }
      })
    } else {
      if (sq.resultatSyntaxe.syntaxVecOK && sq.resultatSyntaxe.isVec) {
        afficheMathliveDans('formules', idrep, chdeb + '$' + ' ' + sq.symbnonexact +
            ' ' + sq.rep + '$', {
          style: {
            color: '#FF0000'
          }
        })
      } else { // On compte une  écriture incorrecte sur les vecteurs comme une faute
        afficheMathliveDans('formules', idrep, chdeb + '$' + '\\text{Invalide : }' + ' ' + sq.rep + '$', {
          style: {
            color: '#FF0000'
          }
        })
      }
    }
    // On vide le contenu de l’éditeur MathQuill
    j3pElement(inputId).value = ''
    resetKeyboardPosition()
    // Si le nombre maximum d’essais est atteint on passe en mode correction
    if (sq.nbexp >= sq.nbEssais) {
      // sq.mtgAppLecteur.setActive("mtg32svg", false);
      if (continuer) {
        parcours.etat = 'correction'
        parcours.sectionCourante()
        $('#editeur').css('display', 'none')
        $('#boutonsmathquill').css('display', 'none')
      }
    } else {
      afficheNombreEssaisRestants()
    }
  }

  function recopierReponse () {
    demarqueEditeurPourErreur()
    const mf = j3pElement(inputId)
    if (!mf) return
    mf.value = sq.rep
    focusIfExists(inputId)
  }

  function initMtg () {
    setMathliveCss('MepMG')
    const mtgOptions = {
      // player only, sans figure initiale ni svg, on gère ça dans initMtg et modif_fig
      loadCoreWithMathJax: true
    }
    getMtgApp('mtg32svg', {}, mtgOptions).then((mtgAppLecteur) => {
      sq.mtgAppLecteur = mtgAppLecteur
      initDom()
      let code, par, car, i, j, k, nbrep, ar, tir, nb
      sq.mtgAppLecteur.removeAllDoc()
      sq.mtgAppLecteur.addDoc('mtg32svg', sq.txt, true)
      sq.mtgAppLecteur.calculate('mtg32svg', false)
      const ch = 'abcdefghjklmnpqr'
      const nbvar = sq.mtgAppLecteur.valueOf('mtg32svg', 'nbvar')
      if (nbvar !== -1) {
        nbrep = parcours.donneesSection.nbrepetitions
        sq.aleat = true
        sq.nbParamAleat = nbvar
        for (i = 1; i <= nbvar; i++) {
          const nbcas = sq.mtgAppLecteur.valueOf('mtg32svg', 'nbcas' + i)
          nb = Math.max(nbrep, nbcas)
          ar = []
          for (j = 0; j < nb; j++) ar.push(j % nbcas)
          sq['tab' + i] = []
          for (k = 0; k < nbrep; k++) {
            tir = Math.floor(Math.random() * ar.length)
            sq['tab' + i].push(ar[tir])
            ar.splice(tir, 1)
          }
          sq.mtgAppLecteur.giveFormula2('mtg32svg', 'r' + i, sq['tab' + i][0])
        }
      } else {
        sq.aleat = false
      }

      if (sq.param !== undefined) {
        for (i = 0; i < ch.length; i++) {
          car = ch.charAt(i)
          if (sq.param.indexOf(car) !== -1) {
            par = parcours.donneesSection[car]
            if (par !== 'random') sq.mtgAppLecteur.giveFormula2('mtg32svg', car, par)
          }
        }
      }
      sq.mtgAppLecteur.calculateAndDisplayAll(true)
      const nbParam = parseInt(sq.nbLatex)
      const t = ['a', 'b', 'c', 'd']
      const param = {}
      for (i = 0; i < nbParam; i++) {
        code = sq.mtgAppLecteur.getLatexCode('mtg32svg', i)
        if (i === 0) sq.aCalculer = code
        param[t[i]] = code
      }
      sq.paramLatex = param

      const st = sq.consigne1 + sq.consigne2 + sq.consigne3 + sq.consigne4
      afficheMathliveDans('enonce', 'texte', st, param)
      // j3pAffiche('debut', 'acalc', '$' + sq.entete + '$ ', param)
      // Les 3 lignes suivantes déplacées ici pour que sq.mtgAppLecteur soit prêt quand onkeyup est appelé
      j3pElement(inputId).onkeyup = function (ev) {
        onkeyup(ev, parcours)
      }
      focusIfExists(inputId)
      parcours.finEnonce()
    }).catch(j3pShowError)
  }

  function initDom () {
    renderMathInDocument({ renderAccessibleContent: '' })
    if (sq.titre) parcours.afficheTitre(sq.titre)

    j3pDiv(parcours.zones.MG, {
      id: 'conteneur',
      contenu: '',
      style: parcours.styles.etendre('toutpetit.enonce', { padding: '6px' })
    })
    j3pDiv('conteneur', 'enonce', '')
    j3pDiv('conteneur', {
      id: 'formules',
      contenu: '',
      style: parcours.styles.petit.enonce
    }) // Contient le formules entrées par l’élève
    if (sq.bigSize) $('#formules').css('font-size', '26px')
    j3pDiv('conteneur', 'info', '')
    afficheNombreEssaisRestants()
    j3pDiv('conteneur', 'conteneurbouton', '')
    j3pAjouteBouton('conteneurbouton', 'boutonrecopier', 'MepBoutonsRepere', 'Recopier réponse précédente',
      recopierReponse)
    j3pElement('boutonrecopier').style.display = 'none'

    j3pDiv('conteneur', 'editeur', '') // Le div qui contiendra l’éditeur MathQuill
    if (sq.bigSize) $('#editeur').css('font-size', '30px')
    $('#editeur').css('padding-top', '10px') // Pour laisser un peu de place au-dessus de l’éditeur
    afficheMathliveDans('editeur', 'expression', '$' + sq.entete + '$ &1&', {
      charset: sq.charset,
      listeBoutons: sq.listeBoutons
    })
    if (sq.charset !== '') {
      mathliveRestrict(inputId, sq.charset)
    }
    j3pDiv('conteneur', 'boutonsmathquill', '')
    $('#boutonsmathquill').css('display', 'inline-block')
    $('#boutonsmathquill').css('padding-top', '10px') // Pour laisser un peu de place au-dessus des boutons
    if (sq.boutonsMathQuill) j3pPaletteMathquill('boutonsmathquill', inputId, { liste: sq.listeBoutons })
    j3pDiv('conteneur', 'divSolution', '')
    $('#divSolution').css('display', 'none')
    j3pDiv('conteneur', 'divmtg32', '')
    j3pCreeSVG('divmtg32', {
      id: 'mtg32svg',
      width: 780,
      height: 900
    })
    const style = parcours.styles.petit.correction
    style.marginTop = '1.5em'
    style.marginLeft = '0.5em'
    j3pAddElt(parcours.zonesElts.MD, 'div', '', { id: 'correction', style })
  }

  switch (this.etat) {
    case 'enonce':
      // code exécuté au lancement de la section
      if (this.debutDeLaSection) {
        // Construction de la page
        parcours.construitStructurePage('presentation1bis')
        this.validOnEnter = false // ex donneesSection.touche_entree
        // compteur. Si   numEssai > nbchances, on corrige
        sq.numEssai = 1

        sq.nbexp = 0
        sq.reponse = -1
        sq.nbEssais = parseInt(parcours.donneesSection.nbEssais)
        sq.nbchances = parseInt(parcours.donneesSection.nbchances)
        if (sq.nbchances > sq.nbEssais) sq.nbchances = sq.nbEssais
        // faut gérer sous-dossier ou pas…
        const sep = this.donneesSection.ex.includes('/') ? '-' : '/'
        j3pImporteAnnexe('squelettes-mtg32' + sep + this.donneesSection.ex + '.js').then(function (datasSection) {
          // console.log("initsection");
          // Attention, le this devient window...
          sq.txt = datasSection.txt
          sq.entete = datasSection.entete ?? ''
          sq.symbexact = datasSection.symbexact ?? '='
          sq.symbnonexact = datasSection.symbnonexact ?? '\\ne'
          sq.nbLatex = datasSection.nbLatex // Le nombre de paramètres LaTeX dans le texte
          // ?? retourne l’opérande de gauche si !== undefined && !== null, celle de droite sinon
          const listeBoutons = ['vecteur'] // Bouotn vecteur toujours présent
          // boutons avec propriétés à true par défaut
          const tabBoutons1 = ['puissance', 'fraction']
          ;['btnPuis', 'btnFrac'].forEach((p, i) => {
            const b = Boolean(datasSection[p] ?? true)
            if (b) listeBoutons.push(tabBoutons1[i])
          })
          // boutons avec false par défaut
          const tabBoutons2 = ['pi', 'racine', 'exp', 'ln', 'sin', 'cos', 'tan', 'abs']
          ;['btnPi', 'btnRac', 'btnExp', 'btnLn', 'btnSin', 'btnCos', 'btnTan', 'btnAbs'].forEach((p, i) => {
            const b = Boolean(datasSection[p] ?? false)
            if (b) listeBoutons.push(tabBoutons2[i])
          })
          sq.listeBoutons = listeBoutons
          sq.boutonsMathQuill = listeBoutons.length !== 0
          sq.consigne1 = datasSection.consigne1 ?? ''
          sq.consigne2 = datasSection.consigne2 ?? ''
          sq.consigne3 = datasSection.consigne3 ?? ''
          sq.consigne4 = datasSection.consigne4 ?? ''
          sq.bigSize = Boolean(datasSection.bigSize ?? false)
          sq.titre = datasSection.titre ?? ''
          sq.param = datasSection.param ?? '' // Chaine contenant les paramètres autorisés
          sq.nomsVect = datasSection.nomsVect
          sq.nomsPoints = datasSection.nomsPoints
          sq.charset = datasSection.charset ?? ''
          initMtg()
        }).catch(error => {
          console.error(error)
          j3pShowError('Impossible de charger les données de cette exercice')
        })
        // return;
      } else {
        j3pEmpty('correction')
        cacheSolution()
        sq.numEssai = 1
        $('#editeur').css('display', 'block')
        $('#boutonsmathquill').css('display', 'inline-block')
        sq.mtgAppLecteur.removeDoc('mtg32svg')
        sq.mtgAppLecteur.addDoc('mtg32svg', sq.txt, true)
        if (sq.aleat) {
          for (let i = 1; i <= sq.nbParamAleat; i++) {
            sq.mtgAppLecteur.giveFormula2('mtg32svg', 'r' + i, sq['tab' + i][this.questionCourante - 1])
          }
        }
        if (sq.param !== undefined) {
          const ch = 'abcdefghjklmnpqr'
          for (let i = 0; i < ch.length; i++) {
            const car = ch.charAt(i)
            if (sq.param.indexOf(car) !== -1) {
              const par = parcours.donneesSection[car]
              if (par !== 'random') sq.mtgAppLecteur.giveFormula2('mtg32svg', car, par)
            }
          }
        }

        sq.mtgAppLecteur.calculate('mtg32svg', true) // true pour que les calculs alèatoires soient réinitialisés
        j3pEmpty('enonce')
        j3pEmpty('formules')
        j3pEmpty('info')
        sq.nbexp = 0
        sq.reponse = -1
        const nbParam = parseInt(sq.nbLatex)
        const t = ['a', 'b', 'c', 'd']
        const param = {}
        for (let i = 0; i < nbParam; i++) {
          const code = sq.mtgAppLecteur.getLatexCode('mtg32svg', i)
          if (i === 0) sq.aCalculer = code
          param[t[i]] = code
        }

        const st = sq.consigne1 + sq.consigne2 + sq.consigne3 + sq.consigne4
        afficheMathliveDans('enonce', 'texte', st, param)
        // j3pAffiche('debut', 'acalc', '$' + sq.entete + '$ ', param)
        afficheNombreEssaisRestants()
        j3pElement('info').style.display = 'block'
        sq.mtgAppLecteur.display('mtg32svg') // Pour tout réafficher
        // Pour revenir en haut de page
        try {
          window.location.hash = 'MepMG'
          j3pElement('MepMG').scrollIntoView()
        } catch (e) {}
        // On vide le contenu de l’éditeur MathQuill et on lui donne le focus.
        j3pElement(inputId).value = ''
        focusIfExists(inputId)
        this.finEnonce()
      }

      break // case "enonce":

    case 'correction': {
      let bilanreponse = ''
      // On regarde si le champ d’édition est rempli même si l’utilisateur
      // n’a pas appuyé sur la touche Entrée.
      const ch = getMathliveValue(inputId)
      if (ch !== '') {
        const rep = traiteMathlive(ch)
        const repcalcul = rep.res
        const tabNames = []
        let valide
        if (rep.vectNul) valide = true
        else {
          for (let k = 0; k < sq.nomsVect.length; k++) {
            let nom = sq.nomsVect[k]
            if (nom === 'i') nom = "i'"
            tabNames.push(nom)
          }
          for (let k = 0; k < sq.nomsPoints.length; k++) {
            let nom = sq.nomsPoints[k]
            if (nom === 'i') nom = "i'"
            tabNames.push(nom)
          }
          sq.resultatSyntaxe = sq.mtgAppLecteur.calcVectOK('mtg32svg', repcalcul, tabNames)
          valide = rep.valid && sq.resultatSyntaxe.syntaxOK
        }
        if (valide) {
          validation(this, false)
        } else {
          bilanreponse = 'incorrect'
        }
      } else {
        if (sq.nbexp === 0) bilanreponse = 'incorrect'
      }
      const res1 = sq.reponse
      if (bilanreponse !== 'incorrect') {
        if (res1 === 1) {
          bilanreponse = 'exact'
        } else {
          if (res1 === 2) {
            bilanreponse = 'exactpasfini'
          } else {
            if (res1 === 3) {
              bilanreponse = 'erreursyntaxe'
            } else bilanreponse = 'erreur'
          }
        }
      }
      // A cause de la limite de temps :
      if (this.isElapsed) { // limite de temps
        this._stopTimer()
        j3pElement('correction').style.color = this.styles.cfaux
        j3pElement('boutonrecopier').style.display = 'none'
        j3pElement('info').style.display = 'none'
        $('#editeur').css('display', 'none')
        $('#boutonsmathquill').css('display', 'none')
        sq.mtgAppLecteur.executeMacro('mtg32svg', 'solution')
        afficheSolution(false)
        j3pElement('correction').innerHTML = tempsDepasse
        j3pElement('correction').innerHTML += '<br>La solution se trouve ci-contre.'
        this.etat = 'navigation'
        this.sectionCourante()
      } else {
        if (bilanreponse === 'incorrect') {
          j3pElement('correction').style.color = this.styles.cfaux
          j3pElement('correction').innerHTML = 'Réponse incorrecte'
          marqueEditeurPourErreur()
          focusIfExists(inputId)
        } else {
          // Une réponse a été saisie
          // Bonne réponse
          if (bilanreponse === 'exact') {
            this._stopTimer()
            // sq.mtgAppLecteur.setActive("mtg32svg", false);
            this.score++
            j3pElement('correction').style.color = this.styles.cbien
            j3pElement('correction').innerHTML = cBien
            j3pElement('boutonrecopier').style.display = 'none'
            j3pElement('info').style.display = 'none'
            sq.mtgAppLecteur.executeMacro('mtg32svg', 'solution')
            afficheSolution(true)
            $('#editeur').css('display', 'none')
            $('#boutonsmathquill').css('display', 'none')
            this.etat = 'navigation'
            this.sectionCourante()
          } else {
            j3pElement('correction').style.color = this.styles.cfaux
            sq.numEssai++
            if (bilanreponse === 'exactpasfini') {
              j3pElement('correction').innerHTML = 'Le calcul est bon mais pas écrit sous la forme demandée.'
            } else {
              if (bilanreponse === 'erreursyntaxe') j3pElement('correction').innerHTML = 'Calcul vectoriel invalide'
              else j3pElement('correction').innerHTML = cFaux
            }
            afficheNombreEssaisRestants()
            // On donne le focus à l’éditeur MathQuill
            focusIfExists(inputId)
            // S’il y a plus de deux chances,on appelle à nouveau le bouton Valider

            if ((sq.nbexp < sq.nbEssais) && (sq.numEssai <= sq.nbchances)) {
              j3pElement('correction').innerHTML += '<br>' + '<br>' + essaieEncore
              // indication éventuelle ici
              // parcours.Sectionsquelettemtg32_Eq_Param_Vect.nbExp += 1;
            } else {
              // Erreur au nème essai
              this._stopTimer()
              // sq.mtgAppLecteur.setActive("mtg32svg", false);
              j3pElement('boutonrecopier').style.display = 'none'
              j3pElement('info').style.display = 'none'
              sq.mtgAppLecteur.executeMacro('mtg32svg', 'solution')
              afficheSolution(false)
              $('#editeur').css('display', 'none')
              $('#boutonsmathquill').css('display', 'none')

              // j3pElement("correction").innerHTML+='<br>' + regardeCorrection;
              j3pElement('correction').innerHTML += '<br><br>La solution se trouve ci-contre.'
              this.etat = 'navigation'
              this.sectionCourante()
            }
          }
        }
      }
      // Obligatoire
      this.finCorrection()
      break // case "correction":
    }

    case 'navigation':
      if (!this.sectionTerminee()) {
        this.etat = 'enonce'
      }
      this.finNavigation()
      break // case "navigation":
  }
}
