import { j3pAddElt, j3pDiv, j3pElement, j3pEmpty, j3pImporteAnnexe, j3pShowError } from 'src/legacy/core/functions'
import { j3pAffiche } from 'src/lib/mathquill/functions'
import { getMtgApp } from 'src/lib/outils/mathgraph'
import { j3pCreeSVG } from 'src/legacy/core/functionsSvg'
import textesGeneriques from 'src/lib/core/textes'

const { cBien, cFaux, essaieEncore, regardeCorrection, tempsDepasse } = textesGeneriques

/*
   Yves Biton
   Octobre 2013

   squelettemtg32_Tracer_Droite2
   La figure comprend deux points libres à coordonnées entières A et B.
   L’élève doit déplacer A et B pour que l’équation de la droite soit celle demandée.
   Dans certains cas, un conseil est donné pour calculer un point de la droite.
   Pour obtenir par exemple 4 répétitions aléatoires, utiliser la commande :
   ?graphe=[1,"squelettemtg32_Tracer_Droite2",[{pe:">=0",nn:"fin",conclusion:"fin"},{nbrepetitions:4}]];
   Pour obtenir un seul exercice avec par exemple une droite d’équation x=3, utiliser la commande :
   ?graphe=[1,"squelettemtg32_Tracer_Droite2",[{pe:">=0",nn:"fin",conclusion:"fin"},{param:"x=3"}]];
   Ne pas demander une droite horizontale
*/

export const params = {
  outils: ['mathquill'],
  /**
   * Ce qui est paramétrable dans l’éditeur de graphe
   * @type {LegacySectionParamsParametres}
   */
  parametres: [
    ['nbrepetitions', 1, 'entier', 'aucun'],
    ['limite', 0, 'entier', 'Temps disponible par répétition, en secondes'],
    ['nbchances', 1, 'entier', 'Nombre d’erreurs possibles (pour tenter de les corriger)'],
    ['f', 'Tracer_Droite_Ex1', 'string', 'Nom du fichier annexe (xxx => squelettes-mtg32/xxx.js et xxx/yyy => squelettes-mtg32-xxx/yyy.js)'],
    ['param', 'random', 'string', 'random pour exercice aléatoire ou x=k pour droite d’équation x=k'],
    ['titre', 'Titre de l’activité', 'string', 'Titre de l’activité']
  ]
}

/**
 * section squelettemtg32_Tracer_Droite2
 * @this {Parcours}
 */
export default function main () {
  const parcours = this
  let code
  const sq = this.storage
  function initMtg () {
    const mtgOptions = {
      // player only, sans figure initiale ni svg, on gère ça dans initMtg et modif_fig
      loadCoreWithMathJax: true
    }
    getMtgApp('mtg32svg', {}, mtgOptions).then((mtgAppLecteur) => {
      sq.mtgAppLecteur = mtgAppLecteur
      sq.mtgAppLecteur.removeAllDoc()
      sq.mtgAppLecteur.addDoc('mtg32svg', sq.txt, true)
      const par = parcours.donneesSection.param
      if (par !== 'random') {
        const ind = par.indexOf('x=')
        if (ind !== -1) {
          const k = par.substring(ind + 2)
          sq.mtgAppLecteur.giveFormula2('mtg32svg', 'k', '0')
          sq.mtgAppLecteur.giveFormula2('mtg32svg', 'x2', k)
        }
      }
      sq.mtgAppLecteur.calculateAndDisplayAll(true)
      j3pAffiche('enonce', 'texte', sq.consigne1)
      let code = sq.mtgAppLecteur.getLatexCode('mtg32svg', 0)
      j3pAffiche('equation', 'eq', '$' + code + '$')
      if (sq.conseil1 !== '') {
        code = sq.mtgAppLecteur.getLatexCode('mtg32svg', 2)
        if (code !== 'vide') {
          j3pAffiche('indication', 'texteindication', sq.conseil1 + '$' + code + '$')
        } else j3pElement('indication').style.visibiliy = 'none'
      }
      parcours.finEnonce()
    }).catch(j3pShowError)
  }

  function _Donnees () {
    // "primaire" || "lycee" || "college"
    // Actuellement les dimensions sont identiques
    this.typesection = 'lycee'//
    // Nombre de répétitions de l’exercice
    this.nbrepetitions = 1
    this.nbetapes = 1
    this.nbitems = this.nbetapes * this.nbrepetitions

    this.f = 'Tracer_Droite_Ex2'
    this.param = 'random'
    // Si indication != "" alors un lien "Indication" s’affiche dans la zone Inférieure Gauche (this.zones.IG
    // une indication<br>de deux lignes...";

    // pour le cas de presentation1bis
    // En effet pas d’indication possible pour cette présentation
    // this.surchargeindication = false;

    // Nombre de chances dont dispose l’él§ve pour répondre à la question
    this.nbchances = 1

    this.textes = {
    }
    this.titre = 'Titre de l’activité'

    this.pe = 0
  }

  function initDom () {
    if (sq.titre) parcours.afficheTitre(sq.titre)

    j3pDiv(parcours.zones.MG, {
      id: 'conteneur',
      contenu: '',
      style: parcours.styles.etendre('toutpetit.enonce', { padding: '6px' })
    })
    j3pDiv('conteneur', 'enonce', '')
    j3pDiv('conteneur', 'equation', '')
    j3pDiv('conteneur', 'indication', '')
    let div = j3pElement('equation')
    div.align = 'center'
    div.style.color = 'blue'
    j3pDiv('conteneur', 'explicationsifaux', '')
    div = j3pDiv('conteneur', 'equationsifaux', '')
    div.align = 'center'
    div.style.color = 'red'
    j3pDiv('conteneur', 'divmtg32', '')
    j3pCreeSVG('divmtg32', { id: 'mtg32svg', width: 600, height: 650 })
    const style = parcours.styles.petit.correction
    style.marginTop = '1.5em'
    style.marginLeft = '0.5em'
    j3pAddElt(parcours.zonesElts.MD, 'div', '', { id: 'correction', style })
    initMtg()
  }

  switch (this.etat) {
    case 'enonce':
      // code exécuté au lancement de la section
      if (this.debutDeLaSection) {
        this.donneesSection = new _Donnees()
        // Construction de la page
        this.construitStructurePage('presentation1bis')

        this.surcharge()
        this.validOnEnter = false // ex donneesSection.touche_entree
        sq.nbchances = parseInt(parcours.donneesSection.nbchances)
        // compteur. Si   numEssai > nbchances, on corrige
        sq.numEssai = 1
        // faut gérer sous-dossier ou pas…
        const sep = this.donneesSection.f.includes('/') ? '-' : '/'
        j3pImporteAnnexe('squelettes-mtg32' + sep + this.donneesSection.f + '.js').then(function (datasSection) {
          // console.log("initsection");
          // Attention, le this devient window...
          // console.log(arguments);
          sq.txt = datasSection.txt
          sq.consigne1 = datasSection.consigne1 // Le nombre de paramètres LaTeX dans le texte
          sq.consigne2 = datasSection.consigne2
          sq.conseil1 = datasSection.conseil
          sq.titre = datasSection.titre
          initDom()
        }).catch(error => {
          console.error(error)
          j3pShowError('Impossible de charger les données de cette exercice')
        })
      } else {
        j3pEmpty('correction')
        sq.numEssai = 1
        sq.mtgAppLecteur.removeDoc('mtg32svg')
        sq.mtgAppLecteur.addDoc('mtg32svg', sq.txt, true)
        sq.mtgAppLecteur.calculate('mtg32svg', true) // true pour que les calculs aléaoires soient réinitialisés
        sq.mtgAppLecteur.display('mtg32svg') // Pour tout réafficher
        sq.mtgAppLecteur.setActive('mtg32svg', true)
        j3pEmpty('enonce')
        j3pEmpty('equation')
        j3pEmpty('indication')
        code = sq.mtgAppLecteur.getLatexCode('mtg32svg', 0)
        j3pAffiche('enonce', 'texte', sq.consigne1)
        j3pAffiche('equation', 'eq', '$' + code + '$')
        if (sq.conseil1 !== '') {
          code = sq.mtgAppLecteur.getLatexCode('mtg32svg', 2)
          if (code !== 'vide') {
            j3pAffiche('indication', 'texteindication', sq.conseil1 + '$' + code + '$')
            j3pElement('indication').style.visibiliy = 'block'
          } else j3pElement('indication').style.visibiliy = 'none'
        } else j3pElement('indication').style.visibiliy = 'none'
        j3pEmpty('explicationsifaux')
        j3pEmpty('equationsifaux')
        this.finEnonce()
      }

      break // case "enonce":

    case 'correction': {
      let bilanreponse = ''
      const res1 = sq.mtgAppLecteur.valueOf('mtg32svg', 'reponse')
      if (res1 === 1) {
        bilanreponse = 'exact'
      } else {
        bilanreponse = 'erreur'
      }

      // Bonne réponse
      if (bilanreponse === 'exact') {
        this._stopTimer()
        sq.mtgAppLecteur.setActive('mtg32svg', false)
        this.score++
        j3pElement('correction').style.color = this.styles.cbien
        j3pElement('correction').innerHTML = cBien
        sq.mtgAppLecteur.executeMacro('mtg32svg', 'solution')
        this.etat = 'navigation'
        this.sectionCourante()
      } else {
        // Pas de bonne réponse

        // A cause de la limite de temps :
        if (this.isElapsed) { // limite de temps
          this._stopTimer()
          j3pAffiche('explicationsifaux', 'expli', sq.consigne2)
          code = sq.mtgAppLecteur.getLatexCode('mtg32svg', 1)
          j3pAffiche('equationsifaux', 'formulesifaux', '$' + code + '$')
          j3pElement('formulesifaux').style.color = '$FF0000'
          sq.mtgAppLecteur.executeMacro('mtg32svg', 'solution')
          j3pElement('correction').style.color = this.styles.cfaux
          j3pElement('correction').innerHTML = tempsDepasse
          j3pElement('correction').innerHTML += '<br>' + regardeCorrection
          this.etat = 'navigation'
          this.sectionCourante()
        } else {
          // Réponse fausse :
          j3pElement('correction').style.color = this.styles.cfaux
          j3pElement('correction').innerHTML = cFaux
          sq.numEssai++

          // S’il y a plus de deux chances,on appelle à nouveau le bouton Valider
          if (sq.numEssai <= sq.nbchances) {
            j3pElement('correction').innerHTML += '<br>' + essaieEncore
          } else {
            // Erreur au nème essai
            this._stopTimer()
            sq.mtgAppLecteur.setActive('mtg32svg', false)
            j3pAffiche('explicationsifaux', 'expli', sq.consigne2)
            code = sq.mtgAppLecteur.getLatexCode('mtg32svg', 1)
            j3pAffiche('equationsifaux', 'formulesifaux', '$' + code + '$')
            j3pElement('formulesifaux').style.color = '$FF0000'
            sq.mtgAppLecteur.executeMacro('mtg32svg', 'solution')
            j3pElement('correction').innerHTML += '<br>' + regardeCorrection
            this.etat = 'navigation'
            this.sectionCourante()
          }
        }
      }
      // Obligatoire
      this.finCorrection()
      break // case "correction":
    }

    case 'navigation':

      if (this.sectionTerminee()) {
        // On détermine la phrase d’état renvoyée par la section
        this.parcours.pe = this.score / this.donneesSection.nbitems
        this.afficheBoutonSectionSuivante()
        this.focus('sectioncontinuer')
      } else {
        this.etat = 'enonce'
        this.afficheBoutonSuite()
        this.focus('boutoncontinuer')
      }
      // Obligatoire
      this.finNavigation()

      break // case "navigation":
  }
}
