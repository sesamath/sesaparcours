import $ from 'jquery'
import { j3pAddElt, j3pBoutonRadio, j3pBoutonRadioChecked, j3pDiv, j3pElement, j3pEmpty, j3pImporteAnnexe, j3pShowError } from 'src/legacy/core/functions'
import { j3pDesactive } from 'src/lib/mathquill/functions'
import { getMtgApp } from 'src/lib/outils/mathgraph'
import { j3pCreeSVG } from 'src/legacy/core/functionsSvg'
import { afficheMathliveDans } from 'src/lib/outils/mathlive/display'
import { MathfieldElement, renderMathInDocument } from 'mathlive'
import { cleanLatexForMl } from 'src/lib/outils/mathlive/utils'
import { setMathliveCss } from 'src/lib/outils/mathlive/mathlivecss'
import textesGeneriques from 'src/lib/core/textes'

const { cBien, cFaux, tempsDepasse } = textesGeneriques

/*
    Yves Biton
    Juillet 2014

    http://localhost:8081/?graphe=[1,"squelettemtg32_Question_Oui_Non_Param",[{pe:">=0",nn:"fin",conclusion:"fin"},{f:"Fact_ax_Carre_Moins_cCarre_Ex1"}]];
    squelettemtg32_Question_Oui_Non_Param
    Squelette demandant de répondre par oui ou non à une question.

    Si la figure contient un paramètre nbvar , alors dans le cas ou nbrepetitions > 1, on donne pour i compris entre 1 et nbvar
    au calcul de la figure nommé "r"+i (par exemple r1 et r2 si nbParamAleat vaut 2) des valeurs comprises entre 0 et "nbcas"+i - 1
    toutes différentes (sauf si nbrepetitions > b=nbcas auquel cas au bout de nbcas répétitions il y aura nécessairement une valeur de r déjà prise.

*/

// on définit le dossier des fonts mathlive (il faut le faire très tôt,
// ici c’est bon avec pnpm start mais trop tard pour un build (cf src/lib/player/index.js de la branche draftPlayer)
if (MathfieldElement) { // undefined dans les tests avec happy-dom
  MathfieldElement.fontsDirectory = '/static/mathlive/fonts'
  MathfieldElement.soundsDirectory = '/static/mathlive/sounds'
}

export const params = {
  outils: ['mathquill'],
  /**
   * Ce qui est paramétrable dans l’éditeur de graphe
   * @type {LegacySectionParamsParametres}
   */
  parametres: [
    ['nbrepetitions', 1, 'entier', 'aucun'],
    ['limite', 0, 'entier', 'Temps disponible par répétition, en secondes'],
    ['a', 'random', 'string', 'Valeur de a'],
    ['b', 'random', 'string', 'Valeur de b'],
    ['c', 'random', 'string', 'Valeur de c'],
    ['d', 'random', 'string', 'Valeur de d'],
    ['e', 'random', 'string', 'Valeur de e'],
    ['f', 'random', 'string', 'Valeur de f'],
    ['g', 'random', 'string', 'Valeur de g'],
    ['h', 'random', 'string', 'Valeur de h'],
    ['j', 'random', 'string', 'Valeur de j'],
    ['k', 'random', 'string', 'Valeur de k'],
    ['l', 'random', 'string', 'Valeur de l'],
    ['m', 'random', 'string', 'Valeur de m'],
    ['n', 'random', 'string', 'Valeur de n'],
    ['p', 'random', 'string', 'Valeur de p'],
    ['q', 'random', 'string', 'Valeur de q'],
    ['r', 'random', 'string', 'Valeur de r'],
    ['ex', 'Lycee_Produit_Scalaire_Test_Vect_Ortho_1', 'string', 'Nom du fichier annexe (xxx => squelettes-mtg32/xxx.js et xxx/yyy => squelettes-mtg32-xxx/yyy.js)']
  ]
}

/**
 * section squelettemtg32_Question_Oui_Non_Param
 * @this {Parcours}
 */
export default function main () {
  const parcours = this
  const sq = this.storage
  let i

  /**
   * Fonction extrayant d’un affichage LaTeX de la figure les lignes de texte et les transformant
   * en une chaîne affichable par Mathlive
   * @param tagLatex : le tag de l’affichage LaTeX de la figure
   * @return {string}
   */
  function extraitDeLatexPourMathlive (tagLatex) {
    const list = sq.mtgAppLecteur.getList('mtg32svg')
    const latex = list.getByTag(tagLatex)
    if (latex === null || latex.className !== 'CLatex') return ''
    return cleanLatexForMl(latex.chaineLatex)
  }

  /**
   * Fonction affichant la solution s’il existe un affichage LaTeX de tag solution
   * @param{boolean} bexact true si l’élève a entré la bonne réponse, false sinon
   */
  function afficheSolution (bexact) {
    $('#divSolution').css('display', 'block').html('')
    const ch = extraitDeLatexPourMathlive('solution')
    if (ch !== '') {
      afficheMathliveDans('divSolution', 'solution', ch, { style: { color: bexact ? 'green' : 'blue' } })
    }
  }

  function cacheSolution () {
    $('#divSolution').css('display', 'none').html('')
  }

  function initMtg () {
    setMathliveCss()
    const mtgOptions = {
      // player only, sans figure initiale ni svg, on gère ça dans initMtg et modif_fig
      loadCoreWithMathJax: true
    }
    getMtgApp('mtg32svg', {}, mtgOptions).then((mtgAppLecteur) => {
      sq.mtgAppLecteur = mtgAppLecteur
      initDom()
      let code, par, car, i, j, k, nbrep, ar, tir, nb
      sq.mtgAppLecteur.removeAllDoc()
      sq.mtgAppLecteur.addDoc('mtg32svg', sq.txt, true)
      sq.mtgAppLecteur.calculate('mtg32svg', false)
      const ch = 'abcdefghjklmnpqr'
      const nbvar = sq.mtgAppLecteur.valueOf('mtg32svg', 'nbvar')
      if (nbvar !== -1) {
        nbrep = parcours.donneesSection.nbrepetitions
        sq.aleat = true
        sq.nbParamAleat = nbvar
        for (i = 1; i <= nbvar; i++) {
          const nbcas = sq.mtgAppLecteur.valueOf('mtg32svg', 'nbcas' + i)
          nb = Math.max(nbrep, nbcas)
          ar = []
          for (j = 0; j < nb; j++) ar.push(j % nbcas)
          sq['tab' + i] = []
          for (k = 0; k < nbrep; k++) {
            tir = Math.floor(Math.random() * ar.length)
            sq['tab' + i].push(ar[tir])
            ar.splice(tir, 1)
          }
          sq.mtgAppLecteur.giveFormula2('mtg32svg', 'r' + i, sq['tab' + i][0])
        }
      } else sq.aleat = false

      if (sq.param !== undefined) {
        for (i = 0; i < ch.length; i++) {
          car = ch.charAt(i)
          if (sq.param.indexOf(car) !== -1) {
            par = parcours.donneesSection[car]
            if (par !== 'random') sq.mtgAppLecteur.giveFormula2('mtg32svg', car, par)
          }
        }
      }

      sq.mtgAppLecteur.calculateAndDisplayAll(true)
      const nbParam = parseInt(sq.nbLatex)
      const t = ['a', 'b', 'c', 'd']
      const param = {}
      for (i = 0; i < nbParam; i++) {
        code = sq.mtgAppLecteur.getLatexCode('mtg32svg', i)
        if (i === 0) sq.aCalculer = code
        param[t[i]] = code
      }
      const st = sq.consigne1 + sq.consigne2 + sq.consigne3 + sq.consigne4
      param.replacefracdfrac = true
      afficheMathliveDans('enonce', 'texte', st, param)
      parcours.finEnonce()
    }).catch(j3pShowError)
  }

  function initDom () {
    renderMathInDocument({ renderAccessibleContent: '' }) // Indispensable
    if (sq.titre) parcours.afficheTitre(sq.titre)

    j3pDiv(parcours.zones.MG, { id: 'conteneur', contenu: '', style: parcours.styles.etendre('toutpetit.enonce', { padding: '6px' }) })
    j3pDiv('conteneur', 'enonce', '')
    j3pDiv('conteneur', 'divsaisie', '')
    j3pBoutonRadio('divsaisie', 'radio1', 'ensemble', 0, sq.oui)
    afficheMathliveDans('divsaisie', 'para1', '&nbsp; &nbsp; &nbsp;', {})
    j3pBoutonRadio('divsaisie', 'radio2', 'ensemble', 1, sq.non)
    j3pDiv('conteneur', 'divSolution', '')
    j3pDiv('conteneur', 'divmtg32', '')
    j3pCreeSVG('divmtg32', {
      id: 'mtg32svg',
      width: sq.width,
      height: sq.height
    })
    const style = parcours.styles.petit.correction
    style.marginTop = '1.5em'
    style.marginLeft = '0.5em'
    j3pAddElt(parcours.zonesElts.MD, 'div', '', { id: 'correction', style })
  }

  switch (this.etat) {
    case 'enonce':
      // code exécuté au lancement de la section
      if (this.debutDeLaSection) {
        // Construction de la page
        parcours.construitStructurePage('presentation1bis')

        // faut gérer sous-dossier ou pas…
        const sep = this.donneesSection.ex.includes('/') ? '-' : '/'
        j3pImporteAnnexe('squelettes-mtg32' + sep + this.donneesSection.ex + '.js').then(function (datasSection) {
          // console.log("initsection");
          // Attention, le this devient window...
          sq.txt = datasSection.txt
          let s = datasSection.entete
          sq.entete = s || ''
          sq.nbLatex = datasSection.nbLatex // Le nombre de paramètres LaTeX dans le texte
          s = datasSection.consigne1
          sq.consigne1 = (s !== undefined) ? s : ''
          s = datasSection.consigne2
          sq.consigne2 = (s !== undefined) ? s : ''
          s = datasSection.consigne3
          sq.consigne3 = (s !== undefined) ? s : ''
          s = datasSection.consigne4
          sq.consigne4 = (s !== undefined) ? s : ''
          s = datasSection.oui
          sq.oui = (s !== undefined) ? s : 'OUI'
          s = datasSection.non
          sq.non = (s !== undefined) ? s : 'NON'
          sq.titre = datasSection.titre
          sq.width = Number(datasSection.width ?? 780)
          sq.height = Number(datasSection.height ?? 900)
          sq.param = datasSection.param // Chaine contenant les paramètres autorisés
          initMtg()
        }).catch(error => {
          console.error(error)
          j3pShowError('Impossible de charger les données de cette exercice')
        })
        // return;
      } else {
        j3pEmpty('correction')
        cacheSolution()
        $('#editeur').css('display', 'block')

        sq.mtgAppLecteur.removeDoc('mtg32svg')
        sq.mtgAppLecteur.addDoc('mtg32svg', sq.txt, true)

        if (sq.aleat) {
          for (i = 1; i <= sq.nbParamAleat; i++) {
            sq.mtgAppLecteur.giveFormula2('mtg32svg', 'r' + i, sq['tab' + i][this.questionCourante - 1])
          }
        }
        if (sq.param !== undefined) {
          const ch = 'abcdefghjklmnpqr'
          for (i = 0; i < ch.length; i++) {
            const car = ch.charAt(i)
            if (sq.param.indexOf(car) !== -1) {
              const par = parcours.donneesSection[car]
              if (par !== 'random') sq.mtgAppLecteur.giveFormula2('mtg32svg', car, par)
            }
          }
        }

        sq.mtgAppLecteur.calculate('mtg32svg', true) // true pour que les calculs alèatoires soient réinitialisés
        j3pEmpty('enonce')
        const nbParam = parseInt(sq.nbLatex)
        const t = ['a', 'b', 'c', 'd']
        const param = {}
        for (i = 0; i < nbParam; i++) {
          const code = sq.mtgAppLecteur.getLatexCode('mtg32svg', i)
          if (i === 0) sq.aCalculer = code
          param[t[i]] = code
        }
        const st = sq.consigne1 + sq.consigne2 + sq.consigne3 + sq.consigne4
        param.replacefracdfrac = true
        afficheMathliveDans('enonce', 'texte', st, param)
        const divsaisie = j3pElement('divsaisie')
        j3pEmpty(divsaisie)
        j3pBoutonRadio(divsaisie, 'radio1', 'ensemble', 0, sq.oui)
        afficheMathliveDans(divsaisie, 'para1', '&nbsp; &nbsp; &nbsp;', {})
        j3pBoutonRadio(divsaisie, 'radio2', 'ensemble', 1, sq.non)
        sq.mtgAppLecteur.display('mtg32svg') // Pour tout réafficher
        // Pour revenir en haut de page
        try {
          window.location.hash = 'MepMG'
          j3pElement('MepMG').scrollIntoView()
        } catch (e) {}
        this.finEnonce()
      }

      break // case "enonce":

    case 'correction': {
      let bilanreponse = ''
      const rep = j3pBoutonRadioChecked('ensemble')[0]
      if (rep > -1) {
        const oui = sq.mtgAppLecteur.valueOf('mtg32svg', 'oui')
        if (oui === 1) {
          if (rep === 0) bilanreponse = 'exact'; else bilanreponse = 'erreur'
        } else {
          if (rep === 0) bilanreponse = 'erreur'; else bilanreponse = 'exact'
        }
      } else {
        bilanreponse = 'incorrect'
      }
      // A cause de la limite de temps :
      if (this.isElapsed) { // limite de temps
        this._stopTimer()
        j3pElement('correction').style.color = this.styles.cfaux
        sq.mtgAppLecteur.executeMacro('mtg32svg', 'solution')
        afficheSolution(false)
        j3pElement('correction').innerHTML = tempsDepasse
        j3pElement('correction').innerHTML += '<br>La solution se trouve ci-contre.'
        this.etat = 'navigation'
        this.sectionCourante()
      } else {
        if (bilanreponse === 'incorrect') {
          j3pElement('correction').style.color = this.styles.cfaux
          j3pElement('correction').innerHTML = 'Il faut répondre'
          return
        } else {
          // Une réponse a été saisie
          // Bonne réponse
          j3pDesactive('radio1')
          j3pDesactive('radio2')
          if (bilanreponse === 'exact') {
            this._stopTimer()
            // sq.mtgAppLecteur.setActive("mtg32svg", false);
            this.score++
            j3pElement('correction').style.color = this.styles.cbien
            j3pElement('correction').innerHTML = cBien
            sq.mtgAppLecteur.executeMacro('mtg32svg', 'solution')
            afficheSolution(true)
            this.etat = 'navigation'
            this.sectionCourante()
          } else {
            j3pElement('correction').style.color = this.styles.cfaux
            j3pElement('correction').innerHTML = cFaux

            this._stopTimer()
            sq.mtgAppLecteur.executeMacro('mtg32svg', 'solution')
            afficheSolution(false)
            j3pElement('correction').innerHTML += '<br><br>La solution se trouve ci-contre.'
            this.etat = 'navigation'
            this.sectionCourante()
          }
        }
      }
      // Obligatoire
      this.finCorrection()
      break // case "correction":
    }

    case 'navigation':
      if (!this.sectionTerminee()) {
        this.etat = 'enonce'
      }
      this.finNavigation()
      break // case "navigation":
  }
}
