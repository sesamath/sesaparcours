import $ from 'jquery'

import { j3pAddElt, j3pAjouteBouton, j3pDiv, j3pElement, j3pEmpty, j3pImporteAnnexe, j3pPaletteMathquill, j3pShowError } from 'src/legacy/core/functions'
import { getMtgApp } from 'src/lib/outils/mathgraph'
import { j3pCreeSVG } from 'src/legacy/core/functionsSvg'
import { MathfieldElement, renderMathInDocument } from 'mathlive'
import { setMathliveCss } from 'src/lib/outils/mathlive/mathlivecss'
import { afficheMathliveDans, getMathliveValue } from 'src/lib/outils/mathlive/display'
import { cleanLatexForMl, mathliveRestrict } from 'src/lib/outils/mathlive/utils'
import { unLatexify } from 'src/lib/mathquill/functions'
import textesGeneriques from 'src/lib/core/textes'
import { focusIfExists } from 'src/lib/utils/dom/main'

const { cBien, cFaux, essaieEncore, tempsDepasse } = textesGeneriques

/*
    Yves Biton
    Juillet 2014. Revu en janvier 2024 pour passage à Mathlive

    http://localhost:8081/?graphe=[1,"squelettemtg32_Calc_Param_Affine_MathQuill",[{pe:">=0",nn:"fin",conclusion:"fin"},{f:"Fact_ax_Carre_Moins_cCarre_Ex1"}]];
    squelettemtg32_Calc_Param_Affine_MathQuill
    Squelette demandant de calculer une expression et de l’écrire sous la forme la plus simple possible
    On peut éventuellement spécifier des paramètres  a,b,c,d,e,f,g,h,j,k,n,n,p.

    Si ex contient un paramètre nbParamAleat , alors dans le cas ou nbrepetitions > 1, on donne pour i compris entre 1 et npParamAleat
    au calcul de la figure nommé "r"+i (par exemple r1 et r2 si nbParamAleat vaut 2) des valeurs comprises entre 0 et "nbcas"+i - 1
    toutes différentes (sauf si nbrepetitions > b=nbcas auquel cas au bout de nbcas répétitions il y aura nécessairement une valeur de r déjà prise.
    Le paramètre k sert à choisir une formule au hasard parmi plusieurs. IL doit être à "random"

*/

// on définit le dossier des fonts mathlive (il faut le faire très tôt,
// ici c’est bon avec pnpm start mais trop tard pour un build (cf src/lib/player/index.js de la branche draftPlayer)
if (MathfieldElement) { // undefined dans les tests avec happy-dom
  MathfieldElement.fontsDirectory = '/static/mathlive/fonts'
  MathfieldElement.soundsDirectory = '/static/mathlive/sounds'
}

export const params = {
  /**
   * Ce qui est paramétrable dans l’éditeur de graphe
   * @type {LegacySectionParamsParametres}
   */
  parametres: [
    ['nbrepetitions', 1, 'entier', 'aucun'],
    ['limite', 0, 'entier', 'Temps disponible par répétition, en secondes'],
    ['nbchances', 1, 'entier', 'Nombre d’erreurs possibles (pour tenter de les corriger)'],
    ['nbEssais', 6, 'entier', 'Nombre d’essais maximum autorisés'],
    ['indicationfaute', true, 'boolean', 'true si on veut que certaines fautes soient expliquées après appui sur Entrée'],
    ['simplificationMaximale', false, 'boolean', 'true si on n’accepte pas des réponses\nde la forme 1*x+b ou ax+0, false sinon'],
    ['a', 'random', 'string', 'Valeur de a'],
    ['b', 'random', 'string', 'Valeur de b'],
    ['c', 'random', 'string', 'Valeur de c'],
    ['d', 'random', 'string', 'Valeur de d'],
    ['e', 'random', 'string', 'Valeur de e'],
    ['f', 'random', 'string', 'Valeur de f'],
    ['k', 'random', 'string', 'Valeur de k'],
    ['p', 'random', 'string', 'Valeur de p'],
    ['q', 'random', 'string', 'Valeur de q'],
    ['ex', 'College_Reduction_Deg1', 'string', 'Nom du fichier annexe (xxx => squelettes-mtg32/xxx.js et xxx/yyy => squelettes-mtg32-xxx/yyy.js)']
  ]
}

const inputId = 'expressioninputmq1'

/**
 * section squelettemtg32_Calc_Param_Affine_MathQuill
 * @this {Parcours}
 */
export default function main () {
  const parcours = this
  const sq = this.storage
  sq.onkeyup = function (ev, parcours) {
    if (sq.marked) {
      demarqueEditeurPourErreur()
    }
    if (ev.keyCode === 13) {
      const rep = getMathliveValue(inputId)
      if (rep !== '') {
        const chcalcul = traiteMathlive(rep)
        const valide = sq.mtgAppLecteur.syntaxValidation('mtg32svg', 'rep', chcalcul, true)
        if (valide) {
          validation(parcours)
        } else {
          marqueEditeurPourErreur()
          j3pElement('correction').style.color = parcours.styles.cfaux
          j3pElement('correction').innerHTML = 'Réponse incorrecte'
        }
      }
    }
  }

  /**
   * Fonction extrayant d’un affichage LaTeX de la figure les lignes de texte et les transformant
   * en une chaîne affichable par MathQuill
   * @param tagLatex : le tag de l’affichage LaTeX de la figure
   * @return {string}
   */
  function extraitDeLatexPourMathlive (tagLatex) {
    const list = sq.mtgAppLecteur.getList('mtg32svg')
    const latex = list.getByTag(tagLatex, true)
    if (latex === null || latex.className !== 'CLatex') return ''
    return cleanLatexForMl(latex.chaineLatex, sq.charset)
  }

  /**
   * Fonction affichant la solution s’il existe un affichage LaTeX de tag solution
   * @param{boolean} bexact true si l’élève a entré la bonne réponse, false sinon
   */
  function afficheSolution (bexact) {
    $('#divSolution').css('display', 'block').html('')
    const ch = extraitDeLatexPourMathlive(sq.factMax ? 'solutionMax' : 'solution')
    if (ch !== '') {
      afficheMathliveDans('divSolution', 'solution', ch, { style: { color: bexact ? 'green' : 'blue' } })
    }
  }

  function cacheSolution () {
    $('#divSolution').css('display', 'none').html('')
  }

  function afficheNombreEssaisRestants () {
    j3pEmpty('info')
    const nbe = sq.nbEssais - sq.nbexp
    const nbc = Math.min(sq.nbchances - sq.numEssai + 1, nbe)
    if ((nbe === 1) && (nbc === 1)) {
      j3pElement('info').innerHTML = 'Il reste une validation'
    } else {
      j3pElement('info').innerHTML = 'Il reste ' + nbe + ' essai(s) et ' + nbc + ' validation(s).'
    }
  }

  function marqueEditeurPourErreur () {
    sq.marked = true
    const mf = j3pElement(inputId)
    if (!mf) return
    mf.style.backgroundColor = '#FF9999'
    mf.demarqueErreur = () => {
      if (sq.marked) {
        demarqueEditeurPourErreur()
      }
    }
  }

  function demarqueEditeurPourErreur () {
    sq.marked = false
    j3pEmpty('correction')
    const mf = j3pElement(inputId)
    if (!mf) return
    mf.style.backgroundColor = 'white'
  }

  function traiteMathlive (ch) {
    // Mathlive ne met pas d’accolades pour les fractions dont le numérateur et le dénominateur sont des entiers
    ch = ch.replace(/\\frac(\d)(\d)/g, '\\frac{$1' + '}{' + '$2}')
    // Ni pour les racines carrées d’un entier
    ch = ch.replace(/\\sqrt(\d)/g, '\\sqrt{$1}')
    // Pour les puissances dont l’exposant est un chiffre, Mathlive ne met pas d’accolades
    ch = ch.replace(/\^([^{])/g, '^{$1}')
    ch = unLatexify(ch)
    // mathlive peut mettre des {} vides
    ch = ch.replace(/\\{\\}/g, '')
    ch = ch.replace(/\^(\d)(\d)/g, '^$1*$2') // On peut avoir par exemple 2^35^4 pour 2^3*5^4
    return sq.mtgAppLecteur.addImplicitMult('mtg32svg', ch)// Traitement des multiplications implicites
  }

  // Le deuxième paramètre est à false seulement quand elle est appelée depuis l’étape de correction
  // Sinon la fonction est appelée par l’éditeur quand l’utilisateur appuie sur la touche entrée avec un seul paramètre
  function validation (parcours, continuer) {
    if (arguments.length <= 1) continuer = true
    if (sq.nbexp === 0) {
      j3pElement('boutonrecopier').style.display = 'block'
    }
    sq.rep = getMathliveValue(inputId)
    const chcalcul = traiteMathlive(sq.rep)
    sq.mtgAppLecteur.giveFormula2('mtg32svg', 'rep', chcalcul)
    j3pEmpty('correction')
    sq.mtgAppLecteur.calculate('mtg32svg', false)
    if (sq.indicationfaute) {
      const faute = sq.mtgAppLecteur.valueOf('mtg32svg', 'faute')
      if (faute === 1) {
        sq.mtgAppLecteur.executeMacro('mtg32svg', 'voirFaute')
      } else {
        sq.mtgAppLecteur.executeMacro('mtg32svg', 'masquerFaute')
      }
    }

    sq.reponse = sq.mtgAppLecteur.valueOf('mtg32svg', 'reponse')
    if (sq.reponse === 2 && !sq.simplificationMaximale) {
      // On regarde si l’élève a pu entre 1*x+b ou ax+0 ou x+0 etc
      // On compte le nomfre de * et de +
      let i = 0
      let nb1 = 0
      while ((i = chcalcul.indexOf('+', i)) !== -1) {
        nb1++
        i++
      }
      i = 0
      let nb2 = 0
      while ((i = chcalcul.indexOf('*', i)) !== -1) {
        nb2++
        i++
      }
      if (nb1 <= 1 && nb2 <= 1) {
        let chc = chcalcul.replace(/(\D)1\*/, '$1').replace(/\*1(\D)/, '$1')
        if (chc.startsWith('1*')) chc = chc.substring(2)
        if (chc.endsWith('*1')) chc = chc.substring(0, chc.length - 2)
        if (chc.startsWith('x*0+')) chc = chc.substring(4)
        if (chc.startsWith('x*0-')) chc = chc.substring(3)
        if (chc.startsWith('0*x')) chc = chc.substring(3)
        if (chc.endsWith('+0*x')) chc = chc.substring(0, chc.length - 4)
        if (chc.endsWith('+x*0')) chc = chc.substring(0, chc.length - 4)
        if (chc.endsWith('+0')) chc = chc.substring(0, chc.length - 2)
        if (chc.startsWith('0+')) chc = chc.substring(2)
        if (chc.startsWith('0-')) chc = chc.substring(1)
        if (chc === '') chc = '0'
        sq.mtgAppLecteur.giveFormula2('mtg32svg', 'rep', chc)
        sq.mtgAppLecteur.calculate('mtg32svg', false)
        if (sq.mtgAppLecteur.valueOf('mtg32svg', 'reponse') === 1) sq.reponse = 1
      }
    }

    const idrep = 'exp' + sq.nbexp
    sq.nbexp++
    const chdeb = sq.nbexp === 1 ? '' : '<br>'
    if (sq.reponse >= 1) {
      afficheMathliveDans('formules', idrep, chdeb + '$' + sq.entete + ' ' + sq.symbexact +
          ' ' + sq.rep + '$', {
        style: {
          color: '#0000FF'
        }
      })
    } else {
      const texteFaute = sq.indicationfaute ? extraitDeLatexPourMathlive('faute') : ''
      const chFaute = texteFaute !== '' ? '<br>' + texteFaute : ''
      afficheMathliveDans('formules', idrep, chdeb + '$' + sq.entete + ' ' + sq.symbnonexact +
          ' ' + sq.rep + '$' + chFaute, {
        style: { color: parcours.styles.cfaux }
      })
    }
    // On vide le contenu de l’éditeur MathQuill
    j3pElement(inputId).value = ''

    // Si le nombre maximum d’essais est atteint on passe en mode correction
    if (sq.nbexp >= sq.nbEssais) {
      sq.mtgAppLecteur.setActive('mtg32svg', false)
      if (continuer) {
        parcours.etat = 'correction'
        parcours.sectionCourante()
        $('#editeur').css('display', 'none')
        $('#boutonsmathquill').css('display', 'none')
      }
    } else {
      afficheNombreEssaisRestants()
    }
  }

  function recopierReponse () {
    demarqueEditeurPourErreur()
    j3pElement(inputId).value = sq.rep
    focusIfExists(inputId)
  }

  function initMtg () {
    setMathliveCss()
    const mtgOptions = {
      // player only, sans figure initiale ni svg, on gère ça dans initMtg et modif_fig
      loadCoreWithMathJax: true
    }
    getMtgApp('mtg32svg', {}, mtgOptions).then((mtgAppLecteur) => {
      sq.mtgAppLecteur = mtgAppLecteur
      sq.nbexp = 0
      sq.reponse = -1
      initDom()
      let code, par, car, i, j, k, nbrep, ar, tir, nb
      sq.mtgAppLecteur.removeAllDoc()
      sq.mtgAppLecteur.addDoc('mtg32svg', sq.txt, true)
      sq.mtgAppLecteur.calculate('mtg32svg', false)
      const ch = 'abcdefpqk'
      const nbvar = sq.mtgAppLecteur.valueOf('mtg32svg', 'nbvar')
      if (nbvar !== -1) {
        nbrep = parcours.donneesSection.nbrepetitions
        sq.aleat = true
        sq.nbParamAleat = nbvar
        for (i = 1; i <= nbvar; i++) {
          const nbcas = sq.mtgAppLecteur.valueOf('mtg32svg', 'nbcas' + i)
          nb = Math.max(nbrep, nbcas)
          ar = []
          for (j = 0; j < nb; j++) ar.push(j % nbcas)
          sq['tab' + i] = []
          for (k = 0; k < nbrep; k++) {
            tir = Math.floor(Math.random() * ar.length)
            sq['tab' + i].push(ar[tir])
            ar.splice(tir, 1)
          }
          sq.mtgAppLecteur.giveFormula2('mtg32svg', 'r' + i, sq['tab' + i][0])
        }
      } else {
        sq.aleat = false
      }

      if (sq.param !== undefined) {
        for (i = 0; i < ch.length; i++) {
          car = ch.charAt(i)
          if (sq.param.indexOf(car) !== -1) {
            par = parcours.donneesSection[car]
            if (par !== 'random') sq.mtgAppLecteur.giveFormula2('mtg32svg', car, par)
          }
        }
      }
      sq.mtgAppLecteur.calculateAndDisplayAll(true)
      const nbParam = parseInt(sq.numero)
      const t = ['a', 'b', 'c', 'd']
      const param = {}
      for (i = 0; i < nbParam; i++) {
        code = sq.mtgAppLecteur.getLatexCode('mtg32svg', i)
        param[t[i]] = code
      }
      param.e = String(sq.nbEssais)
      let st = sq.consigne1 + sq.consigne2 + ((sq.nbEssais === 1) ? sq.consigne5 : sq.consigne4)
      if (sq.simplificationMaximale) st += sq.consigne3
      afficheMathliveDans('enonce', 'texte', st, param)
      j3pElement(inputId).onkeyup = function (ev) {
        sq.onkeyup(ev, parcours)
      }

      focusIfExists(inputId)
      parcours.finEnonce()
    }).catch(j3pShowError)
  }

  function initDom () {
    renderMathInDocument({ renderAccessibleContent: '' }) // Indispensable
    if (sq.titre) parcours.afficheTitre(sq.titre)

    j3pDiv(parcours.zones.MG, {
      id: 'conteneur',
      contenu: '',
      style: parcours.styles.etendre('toutpetit.enonce', { padding: '6px' })
    })
    j3pDiv('conteneur', 'enonce', '')
    j3pDiv('conteneur', {
      id: 'formules',
      contenu: '',
      style: parcours.styles.petit.enonce
    }) // Contient le formules entrées par l’élève
    j3pDiv('conteneur', 'info', '')
    afficheNombreEssaisRestants()
    j3pDiv('conteneur', 'conteneurbouton', '')
    j3pAjouteBouton('conteneurbouton', 'boutonrecopier', 'MepBoutonsRepere', 'Recopier réponse précédente',
      recopierReponse)
    j3pElement('boutonrecopier').style.display = 'none'

    j3pDiv('conteneur', 'editeur', '') // Le div qui contiendra l’éditeur MathQuill
    $('#editeur').css('padding-top', '10px') // Pour laisser un peu de place au-dessus de l’éditeur
    j3pAddElt('editeur', 'span', {
      id: 'debut'
    })
    afficheMathliveDans('debut', 'spandeb', '$' + sq.entete + '$ = ')
    afficheMathliveDans('editeur', 'expression', '&1&', {
      charset: sq.charset,
      listeBoutons: sq.listeBoutons
    })
    if (sq.charset !== '') {
      mathliveRestrict(inputId, sq.charset)
    }
    j3pDiv('conteneur', 'boutonsmathquill', '')
    $('#boutonsmathquill').css('display', 'inline-block')
    $('#boutonsmathquill').css('padding-top', '10px') // Pour laisser un peu de place au-dessus des boutons
    if (sq.boutonsMathQuill) j3pPaletteMathquill('boutonsmathquill', inputId, { liste: sq.listeBoutons })
    j3pAddElt('conteneur', 'div', '', { id: 'divSolution' })
    $('#divSolution').css('display', 'none')
    j3pDiv('conteneur', 'divmtg32', '')
    j3pCreeSVG('divmtg32', {
      id: 'mtg32svg',
      width: sq.width,
      height: sq.height
    })
    const style = parcours.styles.petit.correction
    style.marginTop = '1.5em'
    style.marginLeft = '0.5em'
    j3pAddElt(parcours.zonesElts.MD, 'div', '', { id: 'correction', style })
  }

  switch (this.etat) {
    case 'enonce':
      // code exécuté au lancement de la section
      if (this.debutDeLaSection) {
        this.validOnEnter = false // ex donneesSection.touche_entree

        sq.nbEssais = parseInt(parcours.donneesSection.nbEssais)
        // compteur
        sq.numEssai = 1
        sq.nbchances = parseInt(parcours.donneesSection.nbchances)
        sq.simplificationMaximale = parcours.donneesSection.simplificationMaximale
        if (sq.simplificationMaximale === undefined) sq.simplificationMaximale = false
        sq.indicationfaute = parcours.donneesSection.indicationfaute // true si indication quand certaines fautes sont faites et appui sur Entrée
        // Construction de la page
        this.construitStructurePage('presentation1bis')
        if (sq.nbchances > sq.nbEssais) sq.nbchances = sq.nbEssais
        // faut gérer sous-dossier ou pas…
        const sep = this.donneesSection.ex.includes('/') ? '-' : '/'
        j3pImporteAnnexe('squelettes-mtg32' + sep + this.donneesSection.ex + '.js').then(function (datasSection) {
          // console.log("initsection");
          // Attention, le this devient window...
          sq.txt = datasSection.txt
          sq.entete = datasSection.entete ?? ''
          sq.symbexact = datasSection.symbexact ?? '='
          sq.symbnonexact = datasSection.symbnonexact ?? '\\ne'
          sq.numero = Number(datasSection.numero ?? 0) // Le nombre de paramètres LaTeX dans le texte

          const listeBoutons = []
          // boutons avec propriétés à true par défaut
          const tabBoutons1 = ['puissance', 'fraction']
          ;['btnPuis', 'btnFrac'].forEach((p, i) => {
            const b = Boolean(datasSection[p] ?? true)
            if (b) listeBoutons.push(tabBoutons1[i])
          })
          // boutons avec false par défaut
          const tabBoutons2 = ['racine', 'pi', 'exp', 'ln', 'sin', 'cos', 'tan', 'abs']
          ;['btnRac', 'btnPi', 'btnExp', 'btnLn', 'btnSin', 'btnCos', 'btnTan', 'btnAbs'].forEach((p, i) => {
            const b = Boolean(datasSection[p] ?? false)
            if (b) listeBoutons.push(tabBoutons2[i])
          })
          sq.listeBoutons = listeBoutons
          sq.boutonsMathQuill = listeBoutons.length !== 0
          sq.consigne1 = datasSection.consigne1 ?? ''
          sq.consigne2 = datasSection.consigne2 ?? ''
          sq.consigne3 = datasSection.consigne3 ?? ''
          sq.consigne4 = datasSection.consigne4 ?? ''
          sq.consigne5 = datasSection.consigne5 ?? ''
          sq.titre = datasSection.titre ?? ''
          sq.width = Number(datasSection.width ?? 780)
          sq.height = Number(datasSection.height ?? 520)
          sq.param = datasSection.param ?? '' // Chaine contenant les paramètres autorisés
          sq.charset = datasSection.charset ?? ''
          initMtg()
        }).catch(error => {
          console.error(error)
          j3pShowError('Impossible de charger les données de cette exercice')
        })
        // return;
      } else {
        j3pEmpty('correction')
        cacheSolution()
        sq.numEssai = 1
        $('#editeur').css('display', 'block')
        $('#boutonsmathquill').css('display', 'inline-block')
        // On vide le contenu de l’éditeur MathQuill et on lui donne le focus.
        j3pElement(inputId).value = ''

        sq.mtgAppLecteur.removeDoc('mtg32svg')
        sq.mtgAppLecteur.addDoc('mtg32svg', sq.txt, true)

        if (sq.aleat) {
          for (let i = 1; i <= sq.nbParamAleat; i++) {
            sq.mtgAppLecteur.giveFormula2('mtg32svg', 'r' + i, sq['tab' + i][this.questionCourante - 1])
          }
        }
        if (sq.param !== undefined) {
          const ch = 'abcdefpqk'
          for (let i = 0; i < ch.length; i++) {
            const car = ch.charAt(i)
            if (sq.param.indexOf(car) !== -1) {
              const par = parcours.donneesSection[car]
              if (par !== 'random') sq.mtgAppLecteur.giveFormula2('mtg32svg', car, par)
            }
          }
        }

        sq.mtgAppLecteur.calculate('mtg32svg', true) // true pour que les calculs alèatoires soient réinitialisés
        j3pEmpty('enonce')
        j3pEmpty('formules')
        sq.nbexp = 0
        sq.reponse = -1
        const nbParam = parseInt(sq.numero)
        const t = ['a', 'b', 'c', 'd']
        const param = {}
        for (let i = 0; i < nbParam; i++) {
          const code = sq.mtgAppLecteur.getLatexCode('mtg32svg', i)
          param[t[i]] = code
        }
        param.e = String(sq.nbEssais)
        let st = sq.consigne1 + sq.consigne2 + ((sq.nbEssais === 1) ? sq.consigne5 : sq.consigne4)
        if (sq.simplificationMaximale) st += sq.consigne3
        afficheMathliveDans('enonce', 'texte', st, param)
        afficheNombreEssaisRestants()
        j3pElement('info').style.display = 'block'
        sq.mtgAppLecteur.display('mtg32svg') // Pour tout réafficher
        focusIfExists(inputId)
        this.finEnonce()
      }

      break // case "enonce":

    case 'correction': {
      let bilanreponse = ''
      // On regarde si le champ d’édition est rempli même si l’utilisateur
      // n’a pas appuyé sur la touche Entrée.
      const ch = getMathliveValue(inputId)
      if (ch !== '') {
        const repcalcul = traiteMathlive(ch)
        const valide = sq.mtgAppLecteur.syntaxValidation('mtg32svg', 'rep', repcalcul, true)
        if (valide) {
          validation(this, false)
        } else {
          bilanreponse = 'incorrect'
        }
      } else {
        if (sq.nbexp === 0) bilanreponse = 'incorrect'
      }
      const res1 = sq.reponse
      if (bilanreponse !== 'incorrect') {
        if (res1 === 1) {
          bilanreponse = 'exact'
        } else {
          if (res1 === 2) {
            bilanreponse = 'exactpasfini'
          } else {
            bilanreponse = 'erreur'
          }
        }
      }
      // A cause de la limite de temps :
      if (this.isElapsed) { // limite de temps
        j3pElement('correction').style.color = this.styles.cfaux
        j3pElement('boutonrecopier').style.display = 'none'
        j3pElement('info').style.display = 'none'
        if (sq.indicationfaute) {
          sq.mtgAppLecteur.giveFormula2('mtg32svg', 'faute', '0')
          sq.mtgAppLecteur.calculate('mtg32svg', false)
          sq.mtgAppLecteur.executeMacro('mtg32svg', 'masquerFaute')
        }
        $('#editeur').css('display', 'none')
        $('#boutonsmathquill').css('display', 'none')
        sq.mtgAppLecteur.executeMacro('mtg32svg', 'solution')
        afficheSolution(false)
        j3pElement('correction').innerHTML = tempsDepasse
        j3pElement('correction').innerHTML += '<br>La solution se trouve ci-contre.'
        this.etat = 'navigation'
        this.sectionCourante()
      } else {
        if (bilanreponse === 'incorrect') {
          j3pElement('correction').style.color = this.styles.cfaux
          j3pElement('correction').innerHTML = 'Réponse incorrecte'
          marqueEditeurPourErreur()
          focusIfExists(inputId)
        } else {
          // Bonne réponse
          if (bilanreponse === 'exact') {
            sq.mtgAppLecteur.setActive('mtg32svg', false)
            this.score++
            j3pElement('correction').style.color = this.styles.cbien
            j3pElement('correction').innerHTML = cBien
            j3pElement('boutonrecopier').style.display = 'none'
            j3pElement('info').style.display = 'none'
            if (sq.indicationfaute) {
              sq.mtgAppLecteur.executeMacro('mtg32svg', 'masquerFaute')
            }
            sq.mtgAppLecteur.executeMacro('mtg32svg', 'solution')
            afficheSolution(true)
            $('#editeur').css('display', 'none')
            $('#boutonsmathquill').css('display', 'none')
            return this.finCorrection('navigation', true)
          } else {
            j3pElement('correction').style.color = this.styles.cfaux
            sq.numEssai++
            if (bilanreponse === 'exactpasfini') {
              j3pElement('correction').innerHTML = 'Le calcul est bon mais pas écrit sous la forme demandée.'
            } else {
              j3pElement('correction').innerHTML = cFaux
            }
            afficheNombreEssaisRestants()
            // S’il y a plus de deux chances,on appelle à nouveau le bouton Valider

            if ((sq.nbexp < sq.nbEssais) && (sq.numEssai <= sq.nbchances)) {
              j3pElement('correction').innerHTML += '<br>' + '<br>' + essaieEncore
              // indication éventuelle ici
              // parcours.Sectionsquelettemtg32_Calc_Param_Affine_MathQuill.nbExp += 1;
              sq.mtgAppLecteur.setActive('mtg32svg', true)
              focusIfExists(inputId)
              return this.finCorrection()
            } else {
              // Erreur au nème essai
              sq.mtgAppLecteur.setActive('mtg32svg', false)
              j3pElement('boutonrecopier').style.display = 'none'
              j3pElement('info').style.display = 'none'
              if (sq.indicationfaute) {
                sq.mtgAppLecteur.executeMacro('mtg32svg', 'masquerFaute')
              }
              sq.mtgAppLecteur.executeMacro('mtg32svg', 'solution')
              $('#editeur').css('display', 'none')
              $('#boutonsmathquill').css('display', 'none')
              afficheSolution(false)

              // j3pElement("correction").innerHTML+='<br>' + regardeCorrection;
              j3pElement('correction').innerHTML += '<br><br>La solution se trouve ci-contre.'
              this.finCorrection('navigation', true)
            }
          }
        }
      }
      break // case "correction":
    }

    case 'navigation':
      if (!this.sectionTerminee()) {
        this.etat = 'enonce'
      }
      this.finNavigation()
      break // case "navigation":
  }
}
