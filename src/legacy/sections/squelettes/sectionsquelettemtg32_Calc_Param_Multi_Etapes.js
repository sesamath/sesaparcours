import $ from 'jquery'
import { j3pAddElt, j3pAjouteBouton, j3pDetruit, j3pDiv, j3pElement, j3pEmpty, j3pImporteAnnexe, j3pPaletteMathquill, j3pShowError } from 'src/legacy/core/functions'
import { unLatexify } from 'src/lib/mathquill/functions'
import { getMtgApp } from 'src/lib/outils/mathgraph'
import { j3pCreeSVG } from 'src/legacy/core/functionsSvg'
import { MathfieldElement, renderMathInDocument } from 'mathlive'
import { setMathliveCss } from 'src/lib/outils/mathlive/mathlivecss'
import { getFirstIndexParExcluded, getIndexFermant } from 'src/lib/utils/string'
import { cleanLatexForMl, mathliveRestrict, replaceOldMQCodes } from 'src/lib/outils/mathlive/utils'
import { afficheMathliveDans, getMathliveValue } from 'src/lib/outils/mathlive/display'
import textesGeneriques from 'src/lib/core/textes'

import { setFocusById, setFocusToFirstInput } from 'src/legacy/sections/squelettes/helpersMtg'

const { cBien, cFaux, essaieEncore, tempsDepasse } = textesGeneriques

/*
    Yves Biton
    2019. Revu en janvier 2024 pour passage à Mathlive

    squelettemtg32_Calc_Param_Multi_Etapes
    Squelette demandant de calculer une expression ou plusieurs expressions
    On peut éventuellement spécifier des paramètres  a,b,c,d,e,f,g,h,j,k,n,n,p.
    Utilise une fichier annexe dont le nom est contenu dans le paramètre ex.
    Ce fichier contient :
    Une variable titre pour le titre de l’activité
    une variable nbEtapes qui contient le nombre d’étapes successives (maximum 5 étapes).
    Une variable nbLatex qui contient le nombre de paramètres LaTeX récupérés depuis la figure mtg32 associée.
    Une variable coefEtape1 comprise entre 0 et 1 qui contient le coefficient à attribuer à la réponse à al première étape,
      le reste étant partagé entre les étapes suivantes.
    Une variable consigne_init qui contient la consigne à afficher au début de l’activité.
    Supposons par exemple que nbEtapes vaut 2.
    nbcalc1 contient le nombre de calculs demandés à l’étape 1 (nbcalc2 pour l’étape 2).
    consigne1_1 contient le début de la consigne à l’étape 1 (consigne1_2 pour l’étape 2)
    consigne2_1 contient la suite la consigne à l’étape 1 dans le cas ou simplifier1 est à true sinon c’est consigne3_1 qui est affiché
    consigne4_1 contient la fin de la consigne à l’étape 1 etc ...
    formulaire1 est une chaîne de caractères qui contient le formulaire à calculer à l’étape 1 (formulaire2 pour l’étape2)
    Par exemple, si nbcalc2 vaut 3, on pourra avoir dans formulaire2 la chaîne "Le centre de $C$ a pour coordonnées (edit1;edit2) et le rayon de $C$ est edit3"

    Il contient aussi une variable formulaire qui est une chaîne de caractères contenant éventuellement du LaTeX et contenant
    une référence aux éditeurs MathQuill utilisés. Par exemple, si nbCalc  vaut 2, la chaine devra contenir deux sous-chaînes
    "edit1" et "edit2" qui correspondront aux éditeurs MathQuill.
    Quand on vérifiera la validité de la réponse de l’élève à l’tape 1 par exemple, on mettra (dans le cas où nbCalc vaut 2, par exemple), la réponse
    contenue dans l’éditeur 1 dans le calcul rep1 et, après recalcul de la figure mtg32, le calcul resolu1 contiendra 1
    si la réponse contenue dans le premier éditeur est bonne, 2 si elle est bonne mais le calcul n’est pas fini et 0 si la réponse est fausse.
    Si la variable simplifier1 est à false, un calcul exact mais pas fini sera accepté.
    Si la figure contenue  dans ex contient un paramètre nbVar , alors dans le cas ou nbrepetitions > 1, on donne pour i compris entre 1 et npParamAleat
    au calcul de la figure nommé "r"+i (par exemple r1 et r2 si nbParamAleat vaut 2) des valeurs comprises entre 0 et "nbcas"+i - 1
    toutes différentes (sauf si nbrepetitions > b=nbcas auquel cas au bout de nbcas répétitions il y aura nécessairement une valeur de r déjà prise.
    Le paramètre nbLatex contient le nombre d’affichages LaTeX qui doivent être récupérés de la figure pour affichage dans l’énoncé.

*/

// on définit le dossier des fonts mathlive (il faut le faire très tôt,
// ici c’est bon avec pnpm start mais trop tard pour un build (cf src/lib/player/index.js de la branche draftPlayer)
MathfieldElement.fontsDirectory = '/static/mathlive/fonts'
MathfieldElement.soundsDirectory = '/static/mathlive/sounds'

export const params = {
  /**
   * Ce qui est paramétrable dans l’éditeur de graphe
   * @type {LegacySectionParamsParametres}
   */
  parametres: [
    ['nbrepetitions', 2, 'entier', 'aucun'],
    ['limite', 0, 'entier', 'Temps disponible par répétition, en secondes'],
    ['nbEssais1', 6, 'entier', 'Nombre d’essais maximum autorisés pour le premier calcul'],
    ['nbEssais2', 6, 'entier', 'Nombre d’essais maximum autorisés pour le deuxième calcul'],
    ['nbEssais3', 6, 'entier', 'Nombre d’essais maximum autorisés pour le troisième calcul (si au moins 3 étapes)'],
    ['nbEssais4', 6, 'entier', 'Nombre d’essais maximum autorisés pour le quatrième calcul (si au moins 4 étapes)'],
    ['nbEssais5', 6, 'entier', 'Nombre d’essais maximum autorisés pour le cinquième calcul (si au moins 5 étapes)'],
    ['simplifier1', true, 'boolean', 'true si on exige un forme donnée à l’étape 1, false sinon'],
    ['simplifier2', true, 'boolean', 'true si on exige un forme donnée à l’étape 2, false sinon'],
    ['simplifier3', true, 'boolean', 'true si on exige un forme donnée à l’étape 3, false sinon'],
    ['simplifier4', true, 'boolean', 'true si on exige un forme donnée à l’étape 4, false sinon'],
    ['simplifier5', true, 'boolean', 'true si on exige un forme donnée à l’étape 5, false sinon'],
    ['coefEtape1', '0.5', 'string', 'Nombre strictement compris entre 0 et 1 à rajouter au score\npour une réussite à l’étape 1'],
    ['indicationfaute', true, 'boolean', 'true si on veut que certaines fautes soient expliquées après appui sur Entrée'],
    ['a', 'random', 'string', 'Valeur de a'],
    ['b', 'random', 'string', 'Valeur de b'],
    ['c', 'random', 'string', 'Valeur de c'],
    ['d', 'random', 'string', 'Valeur de d'],
    ['e', 'random', 'string', 'Valeur de e'],
    ['f', 'random', 'string', 'Valeur de f'],
    ['g', 'random', 'string', 'Valeur de g'],
    ['h', 'random', 'string', 'Valeur de h'],
    ['j', 'random', 'string', 'Valeur de j'],
    ['k', 'random', 'string', 'Valeur de k'],
    ['l', 'random', 'string', 'Valeur de l'],
    ['m', 'random', 'string', 'Valeur de m'],
    ['n', 'random', 'string', 'Valeur de n'],
    ['p', 'random', 'string', 'Valeur de p'],
    ['q', 'random', 'string', 'Valeur de q'],
    ['r', 'random', 'string', 'Valeur de r'],
    ['ex', 'cinquieme/Francais_Vers_Calcul_2', 'string', 'Nom du fichier annexe (xxx => squelettes-mtg32/xxx.js et xxx/yyy => squelettes-mtg32-xxx/yyy.js)']
  ]
}

/**
 * section squelettemtg32_Calc_Param_Multi_Etapes
 * @this {Parcours}
 */
export default function main () {
  const parcours = this
  const sq = this.storage

  function onkeyup () {
    const id = this.id
    if (sq.marked[id]) {
      demarqueEditeurPourErreur(id)
    }
  }

  function traiteMathlive (ch) {
    // Mathlive ne met pas d’accolades pour les fractions dont le numérateur et le dénominateur sont des entiers
    ch = ch.replace(/\\frac(\d)(\d)/g, '\\frac{$1' + '}{' + '$2}')
    // Ni pour les racines carrées d’un entier
    ch = ch.replace(/\\sqrt(\d)/g, '\\sqrt{$1}')
    // Pour les puissances dont l’exposant est un chiffre, Mathlive ne met pas d’accolades
    ch = ch.replace(/\^([^{])/g, '^{$1}')
    ch = unLatexify(ch)
    // mathlive peut mettre des {} vides
    ch = ch.replace(/\\{\\}/g, '')
    ch = ch.replace(/\^(\d)(\d)/g, '^$1*$2') // On peut avoir par exemple 2^35^4 pour 2^3*5^4
    ch = ch.replace(/\\infty/g, '∞')
    if (ch === '+∞' || ch === '-∞') {
      let ret = 'infinity'
      if (ch === '-∞') ret = '-' + ret
      return { valide: true, res: ret, contientIntegOuPrim: false }
    }
    if (ch.indexOf('∞') !== -1) {
      return { valide: false, res: ch, contientIntegOuPrim: false }
    }
    let { res, valide } = traiteIntegrales(ch)
    if (valide) {
      const resul = traitePrimitives(res)
      res = resul.res
      valide = resul.valide
    }
    if (valide) ch = sq.mtgAppLecteur.addImplicitMult('mtg32svg', res)// Traitement des multiplications implicites
    const contientIntegOuPrim = (ch.indexOf('integrale') !== -1) || (ch.indexOf('primitive') !== -1)
    return { valide, res: ch, contientIntegOuPrim }
  }

  /**
   * Fonction extrayant d’un affichage LaTeX de la figure les lignes de texte et les transformant
   * en une chaîne affichable par MathQuill
   * @param tagLatex : le tag de l’affichage LaTeX de la figure
   * @return {string}
   */
  function extraitDeLatexPourMathlive (tagLatex) {
    const list = sq.mtgAppLecteur.getList('mtg32svg')
    const latex = list.getByTag(tagLatex, true)
    if (latex === null || latex.className !== 'CLatex') return ''
    return cleanLatexForMl(latex.chaineLatex)
  }

  /**
   * Fonction affichant la solution s’il existe un affichage LaTeX de tag solution
   * @param{boolean} bexact true si l’élève a entré la bonne réponse, false sinon
   */
  function afficheSolution (bexact) {
    j3pEmpty('editeur')
    j3pEmpty('enonce')
    j3pEmpty('enonceSuite')
    j3pEmpty('info')
    $('#divSolution').css('display', 'block').html('')
    const ch = extraitDeLatexPourMathlive('solution')
    if (ch !== '') {
      const styles = parcours.styles
      afficheMathliveDans('divSolution', 'solution', ch, { style: { color: bexact ? styles.cbien : styles.colorCorrection } })
    }
  }

  function cacheSolution () {
    $('#divSolution').css('display', 'none').html('')
    if (sq.figSol !== '') {
      $('#divExplications').css('display', 'none').html('')
      $('#mtg32svgsol').css('display', 'none')
    }
  }

  function afficheNombreEssaisRestants () {
    j3pEmpty('info')
    const nbe = parcours.donneesSection['nbEssais' + sq.etape] - sq.numEssai + 1
    if (nbe === 1) {
      afficheMathliveDans('info', 'texteinfo', 'Il reste un essai.', { style: { color: '#7F007F' } })
    } else {
      afficheMathliveDans('info', 'texteinfo', 'Il reste ' + nbe + ' essais.', { style: { color: '#7F007F' } })
    }
  }

  // Attention : Les }{ on été remplacés par des }/{
  function traiteIntegrales (st) {
    const faux = { valide: false, st }
    let ind
    // Traitement différent pour mathlive
    while ((ind = st.indexOf('\\int_')) !== -1) {
      const ind1 = getFirstIndexParExcluded(st, '^', ind + 5)
      if (ind1 === -1) return -1
      const a = st.substring(ind + 5, ind1)
      // Si le ^ est suivi d’une parenthèse on recherche la parenthèse fermante associée
      // Sinon c’est que la borne supérieure est formée d’un seul caractère
      let ind2, ind3, indsuiv
      if (st.charAt(ind1 + 1) === '(') {
        ind2 = ind1 + 2
        ind3 = getIndexFermant(st, ind1 + 1)
        indsuiv = ind3 + 1
      } else {
        ind2 = ind1 + 1
        ind3 = ind1 + 2
        indsuiv = ind3
      }
      if (ind3 === 0 || indsuiv >= st.length) return faux
      const b = st.substring(ind2, ind3)
      // Attention : les {}{} ont pu donner des ()/().
      if (st.charAt(indsuiv) === '/') indsuiv++
      // On recherche maintenant le d d’intégration en sautant les éventuelles parenthèses
      let ind4 = getFirstIndexParExcluded(st, '\\differentialD', indsuiv)
      if (ind4 === -1) return faux
      const fonc = st.substring(indsuiv, ind4)
      ind4 += 14
      let ind5
      if (st.charAt(ind4) === '(') {
        ind5 = getIndexFermant(st, ind4)
        ind4++
      } else {
        ind5 = ind4 + 1
      }
      if (ind5 === -1) return faux
      const varfor = st.substring(ind4, ind5) // Le caractère représentant la variable d’intégration
      st = st.substring(0, ind) + 'integrale(' + fonc + ',' + varfor + ',' + a + ',' + b + ')' + st.substring(ind5)
    }
    return { valide: true, res: st }
  }

  /**
   * Fonction renvoyant un tableau formé des noms de variables utilisées par le calcul contenu dans ch
   * Si le tableau renvoyé a une longueur au moins égale à 2 on considérera que la primitive est incohérente
   * @param {string} ch
   * @return {[]}
   */
  function variablesUtilisees (ch) {
    function utiliseVariable (nomvar) {
      let res
      res = ch.search(new RegExp('\\(' + nomvar + '[\\+\\-\\*\\/\\^\\)]')) !== -1
      res = res || ch.search(new RegExp(nomvar + '[\\+\\-\\*\\/\\^\\)]')) !== -1
      res = res || (ch.lastIndexOf(nomvar) === ch.length - 1)
      res = res || ch === nomvar
      return res
    }
    const tab = []
    for (let i = 0; i < sq.variables.length; i++) {
      const car = sq.variables.charAt(i)
      if (utiliseVariable(car)) tab.push(car)
    }
    return tab
  }

  function traitePrimitives (st) {
    const faux = { valide: false, st }
    if (sq.variables === '') return { valide: true, res: st }
    let ind, tab
    while ((ind = st.indexOf('\\left[')) !== -1) {
      // On cherche le \right] correspondant
      const ind1 = getIndexFermant(st, ind + 5)
      if (ind1 === -1) return faux
      const fonc = st.substring(ind + 6, ind1 - 6)
      tab = variablesUtilisees(fonc)
      if (tab.length > 1) return faux
      const varfor = (tab.length === 0) ? sq.variables[0] : tab[0] // On traite le cas d’une fonction constante
      // Le crochet fermant doit être suivi d’un _
      if (st.charAt(ind1 + 1) !== '_') return faux
      const ind2 = getFirstIndexParExcluded(st, '^', ind1 + 2)
      if (ind2 === -1) return faux
      const a = st.substring(ind1 + 2, ind2)
      // Soit le caractère suivant n’est pas une parenthèse et c’est un seul caractère
      // qui est l’argument soit c’est le contenu de la parenthèse
      let ind3, ind4
      if (st.charAt(ind2 + 1) === '(') {
        ind3 = ind2 + 2
        ind4 = getIndexFermant(st, ind3)
      } else {
        ind3 = ind2 + 1
        ind4 = ind2 + 2
      }
      if (ind4 === -1 || ind4 > st.length) return faux
      const b = st.substring(ind3, ind4)
      st = st.substring(0, ind) + 'primitive(' + fonc + ',' + varfor + ',' + a + ',' + b + ')' + st.substring(ind4 + 1)
    }
    return { valide: true, res: st }
  }

  /* Abandonné
  sq.onkeydown = function(ev) {
    if (ev.keyCode === 9) { // Touche Tab
      var indice = this.indice; // Indice de l’éditeur qui a appelé la fonction
      var indnext = (indice % sq["nbCalc" + sq.etape]) + 1;
      $("#expressioninputmq" + indnext).focus();
      ev.preventDefault();
    }
  };
  */

  function marqueEditeurPourErreur (id) {
    // Il faut regarder si l’erreur a été rencontrée dans un placeHolder ou dans un éditeur Mathlive classique
    let editor // Peut être aussi bien un éditeur de texte qu’un éditeur Mathlive
    if (id.includes('ph')) {
      const ind = id.indexOf('ph')
      const idmf = id.substring(0, ind) // L’id de l’éditeur MathLive contenant le placeHolder
      editor = j3pElement(idmf)
      sq.marked[idmf] = true
      editor.setPromptState(id, 'incorrect')
    } else {
      editor = j3pElement(id)
      sq.marked[id] = true
      editor.style.backgroundColor = '#FF9999'
    }
    // On donne à l’éditeur une fonction demarquePourError qui lui permettra de se démarquer quand
    // on utilise le clavier virtuel pour le modifier
    editor.demarqueErreur = () => {
      demarqueEditeurPourErreur(editor.id)
    }
  }

  function demarqueEditeurPourErreur (id) {
    if (JSON.stringify(sq.marked) === '{}') return // On teste si l’objet est vide
    // Si id est l’id d’un palceholder on ma repmplace par celle du mathfield qui le contient
    const indph = id.search(/ph\d+/) // On recherche la chaîne ph suivi d’un chiffre
    if (indph !== -1) id = id.substring(0, indph)
    // if ($.isEmptyObject(sq.marked)) return
    sq.marked[id] = false // Par précaution si appel via bouton recopier réponse trop tôt
    // On regarde si l’éditeur où a été trouvée l’erreur est un placeHolder
    const editor = j3pElement(id) // Peut être aussi bien un éditeur Mathlive qu’un éditeur de texte
    if (editor.classList.contains('PhBlock')) {
      editor.getPrompts().forEach((id) => {
        editor.setPromptState(id, 'undefined')
      })
    } else {
      editor.style.backgroundColor = 'white'
    }
  }

  function validationEditeurs () {
    let res = true
    const etapeAvecEditable = sq['isEditableField' + sq.etape]
    let editeurFocusedPourErreur = false
    for (let ind = 1; ind <= sq['nbCalc' + sq.etape]; ind++) {
      let id, rep
      if (etapeAvecEditable) {
        const idmf = 'expressionPhBlock1'
        id = idmf + 'ph' + ind
        rep = getMathliveValue(idmf, { placeholderId: id })
      } else {
        id = 'expressioninputmq' + ind
        rep = getMathliveValue(id)
      }
      if (rep === '' || (sq['equation' + sq.etape] && rep.indexOf('=') === -1)) {
        res = false
        marqueEditeurPourErreur(id)
        if (!editeurFocusedPourErreur) setFocusById(id)
        editeurFocusedPourErreur = true
      } else {
        const resul = traiteMathlive(rep)
        const repcalcul = resul.res
        let aux = 0
        for (let k = 1; k < sq.etape; k++) aux += sq['nbCalc' + k]
        const valide = resul.valide && sq.mtgAppLecteur.syntaxValidation('mtg32svg', 'rep' + String(ind + aux), repcalcul, true)
        if (!valide) {
          res = false
          marqueEditeurPourErreur(id)
          if (!editeurFocusedPourErreur) setFocusById(id)
          editeurFocusedPourErreur = true
        }
      }
    }
    return res
  }

  function montreEditeurs (bVisible) {
    if (bVisible) $('#info').css('display', 'block')
    $('#editeur').css('display', bVisible ? 'block' : 'none')
    if (sq.boutonsMathQuill) $('#boutonsmathquill').css('display', bVisible ? 'block' : 'none')
  }

  function videEditeurs () {
    const el = j3pElement('expression')
    el.querySelectorAll('math-field').forEach(mf => {
      if (!mf.id.includes('PhBlock')) mf.value = ''
      demarqueEditeurPourErreur(mf.id)
    })
    el.querySelectorAll('.PhBlock').forEach((mf) => {
      mf.getPrompts().forEach((id) => {
        mf.setPromptValue(id, '', 'latex')
      })
      // mf.stripPromptContent() // Quand on vide l’apparence du placeholder n’est plus visible
      const idFirstEdit = mf.getPrompts()[0]
      mf.setPromptValue(idFirstEdit, '')
    })
  }

  function creeEditeurs () {
    let i, k, code
    $('#editeur').css('display', 'block')
    let formulaire = sq['formulaire' + sq.etape]
    const t = ['a', 'b', 'c', 'd', 'e', 'f', 'g', 'h', 'i', 'j', 'k', 'l', 'm', 'n', 'o', 'p', 'q']
    const nbParam = parseInt(sq.nbLatex)
    for (k = 1; k <= sq['nbCalc' + sq.etape]; k++) formulaire = formulaire.replace('edit' + k, '&' + k + '&')
    const obj = {}
    sq.paramFormulaire = {}
    for (k = 1; k <= sq['nbCalc' + sq.etape]; k++) obj['inputmq' + k] = {}
    for (i = 0; i < nbParam; i++) {
      code = sq.mtgAppLecteur.getLatexCode('mtg32svg', i)
      obj[t[i]] = code
      sq.paramFormulaire[t[i]] = code
    }
    obj.charset = sq['charset' + sq.etape]
    obj.listeBoutons = sq.listeBoutons
    obj.transposeMat = true
    afficheMathliveDans('editeur', 'expression', formulaire, obj) // sq.formulaire contient ou plusieurs éditeurs MathQuill
    // On affecte un indice à chaque éditeur ainsi qu’un écouteur de focus
    const isEditableField = formulaire.indexOf('{\\editable{') !== -1
    sq['isEditableField' + sq.etape] = isEditableField
    if (isEditableField) {
      j3pElement('expression').querySelectorAll('.PhBlock').forEach((mf) => {
        const id = mf.id
        if (sq.charset !== '') {
          mathliveRestrict(id, sq.charset)
        }
        mf.addEventListener('focusin', () => {
          ecoute.call(mf)
        })
        mf.addEventListener('keyup', () => {
          onkeyup.call(mf)
        })
      })
    } else {
      for (k = 1; k <= sq['nbCalc' + sq.etape]; k++) {
        const mf = j3pElement('expressioninputmq' + k)
        if (sq['charset' + sq.etape] !== '') {
          mathliveRestrict(mf.id, sq['charset' + sq.etape])
        }
        mf.addEventListener('focusin', () => {
          ecoute.call(mf)
        })
        mf.addEventListener('keyup', () => {
          onkeyup.call(mf)
        })
      }
    }
  }

  function afficheReponse (bilan, depasse) {
    const num = sq.numEssai
    const idrep = 'exp' + String(sq.etape) + num
    if (num >= 2 || sq.etape > 1) afficheMathliveDans('formules', idrep + 'br', '<br>')
    let ch, coul, st, i
    if (bilan === 'exact') {
      ch = 'Réponse exacte : '
      coul = parcours.styles.cbien
    } else {
      if (bilan === 'faux') {
        ch = 'Réponse fausse : '
        coul = parcours.styles.cfaux
      } else { // Exact mais pas fini
        ch = 'Exact pas fini : '
        if (depasse) {
          coul = parcours.styles.cfaux
        } else {
          coul = '#0000FF'
        }
      }
    }
    afficheMathliveDans('formules', idrep + 'deb', ch, {
      style: {
        color: coul
      }
    })

    ch = sq['formulaire' + sq.etape]
    const isEditableField = sq['isEditableField' + sq.etape]
    if (isEditableField) {
      for (i = 1; i <= sq['nbCalc' + sq.etape]; i++) {
        let ind = i
        if (sq['estMatrice' + sq.etape]) {
          // Si c’est une matrice on envoie les résultats par ligne alors qu’ils sont
          // enregistrés dans la pile en colonnes
          const nbcol = sq['nbcol' + sq.etape]
          const nblig = sq['nblig' + sq.etape]
          const col = (ind - 1) % nbcol
          const lig = (ind - 1 - (ind - 1) % nbcol) / nbcol
          ind = col * nblig + lig + 1
        }
        st = '\\textcolor{' + (sq.repResolu[ind - 1] ? parcours.styles.cbien : (sq.repExact[ind - 1] ? '#0000FF' : parcours.styles.cfaux)) + '}{'
        const inddeb = ch.indexOf('\\editable{')
        const indfin = ch.indexOf('}', inddeb)
        ch = ch.substring(0, inddeb) + '{' + st + sq.rep[ind - 1] + '}' + ch.substring(indfin)
      }
    } else {
      for (i = 1; i <= sq['nbCalc' + sq.etape]; i++) {
        st = '\\textcolor{' + (sq.repResolu[i - 1] ? parcours.styles.cbien : (sq.repExact[i - 1] ? '#0000FF' : parcours.styles.cfaux)) + '}{'
        ch = ch.replace('edit' + i, '$' + st + sq.rep[i - 1] + '}$')
      }
    }
    afficheMathliveDans('formules', idrep + String(sq.etape), ch, sq.paramFormulaire)
    const texteFaute = sq.indicationfaute ? extraitDeLatexPourMathlive('faute') : ''
    if (texteFaute) {
      afficheMathliveDans('formules', idrep + 'faute', '<br>' + texteFaute, {
        style: {
          color: parcours.styles.cfaux
        }
      })
    }
  }

  function ecoute () {
    if (sq.boutonsMathQuill && (this.id.indexOf('inputmq') !== -1 || this.id.indexOf('PhBlock') !== -1)) {
      j3pEmpty('boutonsmathquill')
      j3pPaletteMathquill('boutonsmathquill', this.id, { liste: sq.listeBoutons, nomdiv: 'palette' })
      // Ligne suivante pour un bon alignement
      $('#palette').css('display', 'inline-block')
    }
  }

  function validation () {
    let aux, k
    if (sq.numEssai >= 1) {
      j3pElement('boutonrecopier').style.display = 'block'
    }
    sq.rep = []
    sq.repExact = []
    sq.repResolu = []
    sq.contientInteg = false
    const etapeAvecEditable = sq['isEditableField' + sq.etape]
    for (let ind = 1; ind <= sq['nbCalc' + sq.etape]; ind++) {
      // Il faut regarder si dans la formulaire correspondant on édite des champs
      // MathQuill ou des champs editable
      let id, rep
      if (etapeAvecEditable) {
        const idmf = 'expressionPhBlock1'
        id = idmf + 'ph' + ind
        rep = getMathliveValue(idmf, { placeholderId: id })
      } else {
        id = 'expressioninputmq' + ind
        rep = getMathliveValue(id)
      }
      sq.rep.push(rep)
      const repMathQuill = traiteMathlive(rep)
      sq.contientInteg = sq.contientInteg || repMathQuill.contientIntegOuPrim
      const chcalcul = repMathQuill.res
      aux = 0
      for (k = 1; k < sq.etape; k++) aux += sq['nbCalc' + k]
      sq.mtgAppLecteur.giveFormula2('mtg32svg', 'rep' + String(ind + aux), chcalcul)
    }
    sq.mtgAppLecteur.calculate('mtg32svg', false)
    if (sq.indicationfaute) {
      const faute = sq.mtgAppLecteur.valueOf('mtg32svg', 'faute')
      if (faute === 1) {
        sq.mtgAppLecteur.executeMacro('mtg32svg', 'voirFaute')
      } else {
        sq.mtgAppLecteur.executeMacro('mtg32svg', 'masquerFaute')
      }
    }
    let exact = true
    let resolu = true
    let auMoinsUnExact = false
    for (let ind = 1; ind <= sq['nbCalc' + sq.etape]; ind++) {
      aux = 0
      for (k = 1; k < sq.etape; k++) aux += sq['nbCalc' + k]
      const res = sq.mtgAppLecteur.valueOf('mtg32svg', 'resolu' + String(ind + aux))
      const ex = sq.mtgAppLecteur.valueOf('mtg32svg', 'exact' + String(ind + aux))
      resolu = resolu && (res === 1)
      exact = exact && ((res === 1) || (ex === 1))
      sq.repResolu.push(res === 1)
      sq.repExact.push((res === 1) || (ex === 1))
      auMoinsUnExact = auMoinsUnExact || (ex === 1)
    }
    sq.resolu = resolu
    sq.exact = exact && auMoinsUnExact
  }

  function recopierReponse () {
    const etapeAvecEditable = sq['isEditableField' + sq.etape]
    for (let ind = sq['nbCalc' + sq.etape]; ind > 0; ind--) {
      const id = (etapeAvecEditable ? 'expressionPhBlock1ph' : 'expressioninputmq') + ind
      demarqueEditeurPourErreur(id)
      if (etapeAvecEditable) {
        const mf = j3pElement('expressionPhBlock1')
        mf.setPromptValue(id, sq.rep[ind - 1])
      } else {
        const mf = j3pElement(id)
        mf.value = sq.rep[ind - 1]
      }
    }
    setFocusToFirstInput('expression')
  }

  function etapeSuivante () {
    j3pElement('boutonrecopier').style.display = 'none'
    sq.etape++
    j3pEmpty('editeur')
    j3pEmpty('enonce')
    j3pEmpty('enonceSuite')
    j3pEmpty('info')
    const k = sq.etape
    const st = sq['consigne1_' + k] + (sq['simplifier' + sq.etape] ? sq['consigne3_' + k] : sq['consigne2_' + k]) + sq['consigne4_' + k]
    afficheMathliveDans('enonceSuite', 'texte', st, sq.parametres)
    sq.numEssai = 1
    afficheNombreEssaisRestants()
    creeEditeurs()
    montreEditeurs(true)
    videEditeurs() // Nécessaire sinon plusieurs éditeurs peuvent clignoter
    setFocusToFirstInput('expression')
  }

  function initMtg () {
    setMathliveCss()
    const mtgOptions = {
      // player only, sans figure initiale ni svg, on gère ça dans initMtg et modif_fig
      loadCoreWithMathJax: true
    }
    getMtgApp('mtg32svg', {}, mtgOptions).then((mtgAppLecteur) => {
      sq.mtgAppLecteur = mtgAppLecteur
      initDom()
      let code, par, car, i, j, nbrep, ar, tir, nb
      sq.mtgAppLecteur.removeAllDoc()
      sq.mtgAppLecteur.addDoc('mtg32svg', sq.txt, true)
      sq.mtgAppLecteur.calculate('mtg32svg', false)
      sq.nbexp = 0
      sq.reponse = -1
      const ch = 'abcdefghjklmnpqr'
      const nbvar = sq.mtgAppLecteur.valueOf('mtg32svg', 'nbvar')
      if (nbvar !== -1) {
        nbrep = parcours.donneesSection.nbrepetitions
        sq.aleat = true
        sq.nbParamAleat = nbvar
        for (i = 1; i <= nbvar; i++) {
          const nbcas = sq.mtgAppLecteur.valueOf('mtg32svg', 'nbcas' + i)
          nb = Math.max(nbrep, nbcas)
          ar = []
          for (j = 0; j < nb; j++) ar.push(j % nbcas)
          sq['tab' + i] = []
          for (let k = 0; k < nbrep; k++) {
            tir = Math.floor(Math.random() * ar.length)
            sq['tab' + i].push(ar[tir])
            ar.splice(tir, 1)
          }
          sq.mtgAppLecteur.giveFormula2('mtg32svg', 'r' + i, sq['tab' + i][0])
        }
      } else {
        sq.aleat = false
      }

      if (sq.param !== undefined) {
        for (i = 0; i < ch.length; i++) {
          car = ch.charAt(i)
          if (sq.param.indexOf(car) !== -1) {
            par = parcours.donneesSection[car]
            if (par !== 'random') sq.mtgAppLecteur.giveFormula2('mtg32svg', car, par)
          }
        }
      }
      for (let k = 1; k <= sq.nbEtapes; k++) sq['nbEssais' + k] = parseInt(parcours.donneesSection['nbEssais' + k])

      sq.coefEtape1 = parseFloat(parcours.donneesSection.coefEtape1)
      sq.coefEtapesSuivantes = (1 - sq.coefEtape1) / (sq.nbEtapes - 1)

      sq.mtgAppLecteur.calculateAndDisplayAll(true)
      const nbParam = parseInt(sq.nbLatex)
      const t = ['a', 'b', 'c', 'd', 'e', 'f', 'g', 'h', 'i', 'j', 'k', 'l', 'm', 'n', 'o', 'p', 'q']
      const param = {}
      for (i = 0; i < nbParam; i++) {
        // Attention MathGraph32 traite les accents d’une façon non reconnue par MathQuill
        code = sq.mtgAppLecteur.getLatexCode('mtg32svg', i).replace(/\\acute{e}/g, '\\text{é}').replace(/\\agrave{e}/g, '\\text{è}')
        param[t[i]] = code
      }
      sq.parametres = param // Mémorisation pour l’étape 2

      const st = sq.consigne1_1 + (sq['simplifier' + sq.etape] ? sq.consigne3_1 : sq.consigne2_1) + sq.consigne4_1
      afficheMathliveDans('enonceInit', 'texteInit', sq.consigne_init, param)
      afficheMathliveDans('enonce', 'texte', replaceOldMQCodes(st), param)
      sq.indicationfaute = parcours.donneesSection.indicationfaute // true si indication quand certaines fautes sont faites et appui sur Entrée
      creeEditeurs()
      if (sq.boutonsMathQuill) {
        j3pPaletteMathquill('boutonsmathquill', 'expressioninputmq1', { liste: sq.listeBoutons, nomdiv: 'palette' })
        // Ligne suivante pour un bon alignement
        $('#palette').css('display', 'inline-block')
      } else {
        j3pDetruit('boutonsmathquill')
      }

      j3pAjouteBouton('conteneurbouton', 'boutonrecopier', 'MepBoutonsRepere', 'Recopier réponse précédente',
        recopierReponse)
      j3pElement('boutonrecopier').style.display = 'none'
      setFocusToFirstInput('expression')
      // $("#expressioninputmq1").focus();
      parcours.finEnonce()
    }).catch(j3pShowError)
  }

  function initDom () {
    renderMathInDocument({ renderAccessibleContent: '' })
    if (sq.titre) parcours.afficheTitre(sq.titre)

    j3pDiv(parcours.zones.MG, {
      id: 'conteneur',
      contenu: '',
      style: parcours.styles.etendre('toutpetit.enonce', { padding: '6px' })
    })
    const divEnonceInit = j3pAddElt('conteneur', 'div', { id: 'enonceInit' })
    divEnonceInit.style.fontSize = sq.taillePoliceEnonce + 'px'
    const divEnonce = j3pAddElt('conteneur', 'div', { id: 'enonce' })
    divEnonce.style.fontSize = sq.taillePoliceEnonce + 'px'
    const formules = j3pAddElt('conteneur', 'div', { id: 'formules' })
    formules.style.fontSize = sq.taillePoliceReponses + 'px'
    // formules contient le formules entrées par l’élève
    const divEnonceSuite = j3pAddElt('conteneur', 'div', { id: 'enonceSuite' })
    divEnonceSuite.style.fontSize = sq.taillePoliceEnonce + 'px'

    j3pDiv('conteneur', 'info', '')
    afficheNombreEssaisRestants()
    j3pDiv('conteneur', 'conteneurbouton', '')
    const editeur = j3pAddElt('conteneur', 'div', { id: 'editeur', style: { paddingTop: '10px' } }) // Le div qui contiendra la chaine avec le ou les éditeurs MathQuill
    editeur.style.fontSize = sq.taillePoliceFormulaire + 'px'
    $('#editeur').css('padding-top', '10px') // Pour laisser un peu de place au-dessus de l’éditeur
    j3pDiv('conteneur', 'boutonsmathquill', '')
    $('#boutonsmathquill').css('display', 'inline-block').css('padding-top', '10px') // Pour laisser un peu de place au-dessus des boutons
    const divsol = j3pAddElt('conteneur', 'div', { id: 'divSolution', style: { display: 'none' } })
    divsol.style.fontSize = sq.taillePoliceSolution + 'px'
    $('#divSolution').css('display', 'none')
    j3pDiv('conteneur', 'divmtg32', '')
    j3pCreeSVG('divmtg32', {
      id: 'mtg32svg',
      width: sq.width,
      height: sq.height
    })
    const style = parcours.styles.petit.correction
    style.marginTop = '1.5em'
    style.marginLeft = '0.5em'
    j3pAddElt(parcours.zonesElts.MD, 'div', '', { id: 'correction', style })
  }

  switch (this.etat) {
    case 'enonce': {
      // code exécuté au lancement de la section
      if (this.debutDeLaSection) {
        // Construction de la page
        parcours.construitStructurePage('presentation1bis')

        sq.etape = 1
        // compteur.
        sq.numEssai = 1

        for (let i = 1; i <= 5; i++) {
          const s = parcours.donneesSection['simplifier' + i]
          sq['simplifier' + i] = (s === undefined) ? true : s
        }
        // faut gérer sous-dossier ou pas…
        const sep = this.donneesSection.ex.includes('/') ? '-' : '/'
        j3pImporteAnnexe('squelettes-mtg32' + sep + this.donneesSection.ex + '.js').then(function (datasSection) {
          let s
          // console.log("initsection");
          // Attention, le this devient window...
          sq.txt = datasSection.txt
          sq.width = Number(datasSection.width ?? 700)
          sq.height = Number(datasSection.height ?? 900)
          sq.nbEtapes = Number(datasSection.nbEtapes)
          sq.nbLatex = Number(datasSection.nbLatex ?? 0) // Le nombre de paramètres LaTeX dans le texte
          // ?? retourne l’opérande de gauche si !== undefined && !== null, celle de droite sinon
          const listeBoutons = []
          // boutons avec propriétés à true par défaut
          const tabBoutons1 = ['puissance', 'fraction']
          ;['btnPuis', 'btnFrac'].forEach((p, i) => {
            const b = Boolean(datasSection[p] ?? true)
            if (b) listeBoutons.push(tabBoutons1[i])
          })
          // boutons avec false par défaut
          const tabBoutons2 = ['pi', 'racine', 'exp', 'ln', 'sin', 'cos', 'tan', 'abs', 'conj', 'integ', 'differential', 'prim']
          ;['btnPi', 'btnRac', 'btnExp', 'btnLn', 'btnSin', 'btnCos', 'btnTan', 'btnAbs', 'btnConj', 'btnInteg', 'btnDiff', 'btnPrim'].forEach((p, i) => {
            const b = Boolean(datasSection[p] ?? false)
            if (b) listeBoutons.push(tabBoutons2[i])
          })
          sq.listeBoutons = listeBoutons
          sq.boutonsMathQuill = listeBoutons.length !== 0
          sq.consigne_init = datasSection.consigne_init
          for (let k = 1; k <= sq.nbEtapes; k++) {
            s = datasSection['equation' + k]
            sq['equation' + k] = (s !== undefined) ? s : false
            s = datasSection['consigne1_' + k]
            sq['consigne1_' + k] = (s !== undefined) ? s : ''
            s = datasSection['consigne2_' + k]
            sq['consigne2_' + k] = (s !== undefined) ? s : ''
            s = datasSection['consigne3_' + k]
            sq['consigne3_' + k] = (s !== undefined) ? s : ''
            s = datasSection['consigne4_' + k]
            sq['consigne4_' + k] = (s !== undefined) ? s : ''
            sq['nbCalc' + k] = datasSection['nbCalc' + k]
            sq['formulaire' + k] = datasSection['formulaire' + k] // Contient la chaine avec les éditeurs.
            // Information pour savoir si à l’étape n on édite une matrice  et si oui son
            // nombre de lignes et de colonnes
            s = datasSection['estMatrice' + k]
            sq['estMatrice' + k] = (s !== undefined) ? s : false
            s = datasSection['nbcol' + k]
            sq['nbcol' + k] = (s !== undefined) ? s : 0
            s = datasSection['nblig' + k]
            sq['nblig' + k] = (s !== undefined) ? s : 0
            s = datasSection['charset' + k] // Le set de caractères utilisés dans l’éditeur
            sq['charset' + k] = (s !== undefined) ? s : ''
          }
          sq.variables = datasSection.variables ?? ''
          sq.titre = datasSection.titre ?? ''
          const bigSize = datasSection.bigSize
          if (bigSize && !datasSection.taillePoliceFormulaire) {
            sq.taillePoliceFormulaire = 30
            sq.taillePoliceReponses = 26
            sq.taillePoliceSolution = 26
            sq.tailleEnonce = 26
          } else {
            let taillePoliceFormulaire = Number(datasSection.taillePoliceFormulaire ?? 24)
            if (taillePoliceFormulaire < 10 || taillePoliceFormulaire > 50) taillePoliceFormulaire = 24
            sq.taillePoliceFormulaire = taillePoliceFormulaire
            let taillePoliceReponses = Number(datasSection.taillePoliceReponses ?? 22)
            if (taillePoliceReponses < 10 || taillePoliceReponses > 50) taillePoliceReponses = 22
            sq.taillePoliceReponses = taillePoliceReponses
            let taillePoliceSolution = Number(datasSection.taillePoliceSolution ?? 22)
            if (taillePoliceSolution < 10 || taillePoliceSolution > 50) taillePoliceSolution = 22
            sq.taillePoliceSolution = taillePoliceSolution
            let taillePoliceEnonce = Number(datasSection.taillePoliceEnonce ?? 22)
            if (taillePoliceEnonce < 10 || taillePoliceEnonce > 50) taillePoliceEnonce = 22
            sq.taillePoliceEnonce = taillePoliceEnonce
          }
          sq.param = datasSection.param ?? '' // Chaine contenant les paramètres autorisés
          sq.marked = []
          for (let k = 0; k < sq.nbCalc; k++) {
            sq.marked[k] = false
          }
          initMtg()
        }).catch(error => {
          console.error(error)
          j3pShowError('Impossible de charger les données de cette exercice')
        })
        // return;
      } else {
        j3pEmpty('correction')
        cacheSolution()
        sq.numEssai = 1
        sq.etape = 1
        // sq.videEditeurs();
        sq.mtgAppLecteur.removeDoc('mtg32svg')
        sq.mtgAppLecteur.addDoc('mtg32svg', sq.txt, true)

        if (sq.aleat) {
          for (let i = 1; i <= sq.nbParamAleat; i++) {
            sq.mtgAppLecteur.giveFormula2('mtg32svg', 'r' + i, sq['tab' + i][this.questionCourante - 1])
          }
        }
        if (sq.param !== undefined) {
          const ch = 'abcdefghjklmnpqr'
          for (let i = 0; i < ch.length; i++) {
            const car = ch.charAt(i)
            if (sq.param.indexOf(car) !== -1) {
              const par = parcours.donneesSection[car]
              if (par !== 'random') sq.mtgAppLecteur.giveFormula2('mtg32svg', car, par)
            }
          }
        }

        sq.mtgAppLecteur.calculate('mtg32svg', true) // true pour que les calculs aléatoires soient réinitialisés
        j3pEmpty('enonceInit')
        j3pEmpty('enonce')
        j3pEmpty('formules')
        j3pEmpty('info')
        j3pEmpty('enonceSuite')

        sq.nbexp = 0
        sq.reponse = -1
        const nbParam = parseInt(sq.nbLatex)
        const t = ['a', 'b', 'c', 'd', 'e', 'f', 'g', 'h', 'i', 'j', 'k', 'l', 'm', 'n', 'o', 'p', 'q']
        const param = {}
        for (let i = 0; i < nbParam; i++) {
          const code = sq.mtgAppLecteur.getLatexCode('mtg32svg', i).replace(/\\acute{e}/g, '\\text{é}').replace(/\\agrave{e}/g, '\\text{è}')
          param[t[i]] = code
        }
        sq.parametres = param
        const st = sq.consigne1_1 + (sq['simplifier' + sq.etape] ? sq.consigne3_1 : sq.consigne2_1) + sq.consigne4_1
        afficheMathliveDans('enonceInit', 'texteInit', sq.consigne_init, param)
        afficheMathliveDans('enonce', 'texte', replaceOldMQCodes(st), param)
        afficheNombreEssaisRestants()
        j3pElement('info').style.display = 'block'

        j3pEmpty('editeur')
        creeEditeurs()
        montreEditeurs(true)
        sq.mtgAppLecteur.display('mtg32svg') // Pour tout réafficher
        // Pour revenir en haut de page
        try {
          window.location.hash = 'MepMG'
          j3pElement('MepMG').scrollIntoView()
        } catch (e) {
          console.error(e)
        }
        setFocusToFirstInput('expression')
        this.finEnonce()
      }

      break // case "enonce":
    }

    case 'correction': {
      let bilanReponse = ''
      let simplificationPossible = false
      if (validationEditeurs()) {
        validation()
        if (sq['simplifier' + sq.etape]) {
          if (sq.resolu) {
            bilanReponse = 'exact'
          } else {
            if (sq.exact) {
              bilanReponse = 'exactPasFini'
              simplificationPossible = true
            } else {
              bilanReponse = 'faux'
            }
          }
        } else {
          if (sq.resolu || sq.exact) {
            if (sq.resolu) { bilanReponse = 'exact' } else {
              simplificationPossible = true
              if (sq.exact && sq.contientInteg) {
                bilanReponse = 'exactPasFini'
              } else { bilanReponse = 'exact' }
            }
          } else {
            bilanReponse = 'faux'
          }
        }
      } else {
        bilanReponse = 'incorrect'
      }
      // A cause de la limite de temps :
      if (this.isElapsed) { // limite de temps
        this._stopTimer()
        montreEditeurs(false)
        j3pElement('correction').style.color = this.styles.cfaux
        j3pElement('boutonrecopier').style.display = 'none'
        j3pElement('info').style.display = 'none'
        if (sq.indicationfaute) sq.mtgAppLecteur.executeMacro('mtg32svg', 'masquerFaute')

        sq.mtgAppLecteur.executeMacro('mtg32svg', 'solution')
        afficheSolution(false)
        j3pElement('correction').innerHTML = tempsDepasse
        j3pElement('correction').innerHTML += '<br>La solution se trouve ci-contre.'
        this.etat = 'navigation'
        this.sectionCourante()
      } else {
        if (bilanReponse === 'incorrect') {
          j3pElement('correction').style.color = this.styles.cfaux
          j3pElement('correction').innerHTML = 'Réponse incorrecte'
          this.afficheBoutonValider()
        } else {
          // Une réponse a été saisie
          // Bonne réponse
          if (bilanReponse === 'exact') {
            sq.mtgAppLecteur.setActive('mtg32svg', false)
            this.score += (sq.etape === 1 ? sq.coefEtape1 : sq.coefEtapesSuivantes)
            j3pElement('correction').style.color = this.styles.cbien
            let mess = simplificationPossible ? 'C’est bien mais on pouvait  simplifier la réponse' : cBien
            if (sq.etape === 1) mess += '<br>On passe à la question suivante.'
            j3pElement('correction').innerHTML = mess
            montreEditeurs(false)
            sq.numEssai++ // Pour un affichage correct dans afficheReponse
            afficheReponse('exact', false)
            if (sq.etape === sq.nbEtapes) {
              j3pEmpty('enonceSuite')
              this._stopTimer()
              j3pElement('boutonrecopier').style.display = 'none'
              j3pElement('info').style.display = 'none'
              if (sq.indicationfaute) sq.mtgAppLecteur.executeMacro('mtg32svg', 'masquerFaute')
              sq.mtgAppLecteur.executeMacro('mtg32svg', 'solution')
              afficheSolution(true)
              this.etat = 'navigation'
              this.sectionCourante()
            } else {
              etapeSuivante()
            }
          } else {
            sq.numEssai++
            if (bilanReponse === 'exactPasFini') {
              j3pElement('correction').style.color =
                  (sq.numEssai <= this.donneesSection['nbEssais' + sq.etape]) ? '#0000FF' : this.styles.cfaux
              j3pElement('correction').innerHTML = 'Le calcul est bon mais pas écrit sous la forme demandée.'
            } else {
              j3pElement('correction').style.color = this.styles.cfaux
              j3pElement('correction').innerHTML = cFaux
            }
            j3pEmpty('info')
            afficheReponse(bilanReponse, false)
            if (sq.numEssai <= this.donneesSection['nbEssais' + sq.etape]) {
              afficheNombreEssaisRestants()
              videEditeurs()
              // S’il y a plus de deux chances,on appelle à nouveau le bouton Valider
              j3pElement('correction').innerHTML += '<br>' + '<br>' + essaieEncore
              setFocusToFirstInput('expression')
            } else {
              if (sq.etape === sq.nbEtapes) {
                afficheSolution(false)
                j3pEmpty('enonceSuite')
                this._stopTimer()
                sq.mtgAppLecteur.setActive('mtg32svg', false)
                montreEditeurs(false)
                j3pElement('boutonrecopier').style.display = 'none'
                j3pElement('info').style.display = 'none'

                if (sq.indicationfaute) sq.mtgAppLecteur.executeMacro('mtg32svg', 'masquerFaute')
                sq.mtgAppLecteur.executeMacro('mtg32svg', 'solution')
                j3pElement('correction').innerHTML += '<br><br>La solution se trouve ci-contre.'
                this.etat = 'navigation'
                this.sectionCourante()
              } else {
                j3pElement('correction').innerHTML += '<br><br>On passe à la question suivante.'
                etapeSuivante()
              }
            }
          }
        }
      }
      // Obligatoire
      this.finCorrection()
      break // case "correction":
    }

    case 'navigation':
      if (!this.sectionTerminee()) {
        this.etat = 'enonce'
      }
      this.finNavigation()
      break // case "navigation":
  }
}
