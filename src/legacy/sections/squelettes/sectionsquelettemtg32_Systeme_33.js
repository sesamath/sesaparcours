import $ from 'jquery'
import { MathfieldElement, renderMathInDocument } from 'mathlive'

import { j3pAddElt, j3pAjouteBouton, j3pDiv, j3pElement, j3pEmpty, j3pImporteAnnexe, j3pPaletteMathquill, j3pShowError } from 'src/legacy/core/functions'
import { getMtgApp } from 'src/lib/outils/mathgraph'
import { j3pCreeSVG } from 'src/legacy/core/functionsSvg'
import { setMathliveCss } from 'src/lib/outils/mathlive/mathlivecss'
import { afficheMathliveDans, getMathliveValue } from 'src/lib/outils/mathlive/display'
import { resetKeyboardPosition } from 'src/lib/widgets/mlVirtualKeyboard/VirtualKeyboard'
import { cleanLatexForMl, focusPlaceholder, mathliveRestrict } from 'src/lib/outils/mathlive/utils'
import { unLatexify } from 'src/lib/mathquill/functions'
import textesGeneriques from 'src/lib/core/textes'

const { cBien, cFaux, essaieEncore, tempsDepasse } = textesGeneriques

/**
 * @fileOverview
 * @author Yves Biton <yves.biton@sesamath.net>
 * @since août 2021. Revu en janvier 2024 pour passage à Mathlive
 * squelettemtg32_Systeme_33 est un squelette demandant de résoudre un système de trois équations linéaires à trois inconnues.
 * On peut éventuellement spécifier des paramètres  a,b,c,d,e,f,g,h,j,k,n,n,p.
 * ex est la référence à un fichier annexe qui comprend entre autres une figure mtg32 chargée de proposer l’exercice et d’afficher la solution détaillée
 * Pour savoir la réponse est exacte, c’est-à-dire contient une formule du type x= ou =x donnant une des solutions,
 * on met la formule donnée pour la première inconnue dans dans le calcul rep1, la formule donnée pour la seconde
 * inconnue dans rep2 la formule donnée pour la 3e inconnue dans rep3 et on interroge.
 * Après recalcul de la figure, la valeur du calcul exact qui vaut 1 si la réponse est exacte et 0 si elle est fausse.
 * Pour savoir si une réponse est exacte et bien écrite sous la forme demandée, on interroge la valeur du calcul resolu qui vaut 0
 * Si le calcul ne correspond pas à une forme attendue et sinon renvoie 1.
 * L’élève peut aussi entrer de équations intermédiaires. Dans ce cas le calcul prequeresolu renvoie 1
 * si les valeurs cherchées vérifient bien les deux équations correspondantes et 0 sinon.
 * Si la figure contenue dans ex contient un paramètre nbvar , alors dans le cas ou nbrepetitions > 1,
 * on donne pour i compris entre 1 et nbvar au calcul de la figure nommé "r"+i (par exemple r1 et r2 si nbParamAleat vaut 2)
 * des valeurs comprises entre 0 et "nbcas"+i - 1 toutes différentes (sauf si nbrepetitions > nbcas auquel cas au bout
 * de nbcas répétitions il y aura nécessairement une valeur de r déjà prise.
 */
// on définit le dossier des fonts mathlive (il faut le faire très tôt,
// ici c’est bon avec pnpm start mais trop tard pour un build (cf src/lib/player/index.js de la branche draftPlayer)
if (MathfieldElement) { // undefined dans les tests avec happy-dom
  MathfieldElement.fontsDirectory = '/static/mathlive/fonts'
  MathfieldElement.soundsDirectory = '/static/mathlive/sounds'
}

export const params = {
  /**
   * Ce qui est paramétrable dans l’éditeur de graphe
   * @type {LegacySectionParamsParametres}
   */
  parametres: [
    ['nbrepetitions', 2, 'entier', 'aucun'],
    ['limite', 0, 'entier', 'Temps disponible par répétition, en secondes'],
    ['nbEssais', 8, 'entier', 'Nombre d’essais maximum autorisés'],
    ['simplifier', true, 'boolean', 'true si on exige un forme donnée, false sinon'],
    ['a', 'random', 'string', 'Valeur de a'],
    ['b', 'random', 'string', 'Valeur de b'],
    ['c', 'random', 'string', 'Valeur de c'],
    ['d', 'random', 'string', 'Valeur de d'],
    ['e', 'random', 'string', 'Valeur de e'],
    ['f', 'random', 'string', 'Valeur de f'],
    ['g', 'random', 'string', 'Valeur de g'],
    ['h', 'random', 'string', 'Valeur de h'],
    ['j', 'random', 'string', 'Valeur de j'],
    ['k', 'random', 'string', 'Valeur de k'],
    ['l', 'random', 'string', 'Valeur de l'],
    ['m', 'random', 'string', 'Valeur de m'],
    ['n', 'random', 'string', 'Valeur de n'],
    ['p', 'random', 'string', 'Valeur de p'],
    ['q', 'random', 'string', 'Valeur de q'],
    ['r', 'random', 'string', 'Valeur de r'],
    ['ex', 'premiere/Systeme_Lineaire_33_1', 'string', 'Nom du fichier annexe (xxx => squelettes-mtg32/xxx.js et xxx/yyy => squelettes-mtg32-xxx/yyy.js)']
  ]
}

/**
 * section Systeme33
 * @this {Parcours}
 */
export default function main () {
  const parcours = this
  const sq = this.storage

  function onkeyup () {
    for (let i = 1; i <= 3; i++) demarqueEditeurPourErreur(i)
  }

  function afficheNombreEssaisRestants () {
    j3pEmpty('divinfo')
    const nbe = sq.nbEssais - sq.numEssai + 1
    if (nbe === 1) afficheMathliveDans('divInfo', 'texteinfo', 'Il reste un essai.', { style: { color: '#7F007F' } })
    else afficheMathliveDans('divInfo', 'texteinfo', 'Il reste ' + nbe + ' essais.', { style: { color: '#7F007F' } })
  }

  /**
   * Fonction extrayant d’un affichage LaTeX de la figure les lignes de texte et les transformant
   * en une chaîne affichable par MathQuill
   * @param tagLatex : le tag de l’affichage LaTeX de la figure
   * @return {string}
   */
  function extraitDeLatexPourMathlive (tagLatex) {
    const list = sq.mtgAppLecteur.getList('mtg32svg')
    const latex = list.getByTag(tagLatex, true)
    if (latex === null || latex.className !== 'CLatex') return ''
    return cleanLatexForMl(latex.chaineLatex, sq.charset)
  }

  /**
   * Fonction affichant la solution s’il existe un affichage LaTeX de tag solution
   * @param{boolean} bexact true si l’élève a entré la bonne réponse, false sinon
   */
  function afficheSolution (bexact) {
    $('#divSolution').css('display', 'block').html('')
    const ch = extraitDeLatexPourMathlive('solution').replace(/\\frac/g, '\\dfrac')
    if (ch !== '') {
      afficheMathliveDans('divSolution', 'solution', ch, { style: { color: bexact ? 'green' : 'blue' } })
      $('#divsysteme').css('display', 'none')
    }
  }

  function cacheSolution () {
    $('#divSolution').css('display', 'none').html('')
    $('#divsysteme').css('display', 'block')
  }

  function traiteMathlive (ch) {
    // Mathlive ne met pas d’accolades pour les fractions dont le numérateur et le dénominateur sont des entiers
    ch = ch.replace(/\\frac(\d)(\d)/g, '\\frac{$1' + '}{' + '$2}')
    // Ni pour les racines carrées d’un entier
    ch = ch.replace(/\\sqrt(\d)/g, '\\sqrt{$1}')
    // Pour les puissances dont l’exposant est un chiffre, Mathlive ne met pas d’accolades
    ch = ch.replace(/\^([^{])/g, '^{$1}')
    ch = unLatexify(ch)
    // mathlive peut mettre des {} vides
    ch = ch.replace(/\\{\\}/g, '')
    ch = ch.replace(/\^(\d)(\d)/g, '^$1*$2') // On peut avoir par exemple 2^35^4 pour 2^3*5^4
    return sq.mtgAppLecteur.addImplicitMult('mtg32svg', ch)// Traitement des multiplications implicites
  }

  function focusEditeur (numEdit) {
    const idmf = 'expressionPhBlock1'
    const idPrompt = idmf + 'ph' + numEdit
    focusPlaceholder(idmf, idPrompt)
  }

  function validationEditeurs () {
    let res = true
    let focusDonne = false
    for (let ind = 1; ind <= 3; ind++) {
      const idBlock = 'expressionPhBlock1'
      const id = idBlock + 'ph' + ind
      const rep = getMathliveValue(idBlock, { placeholderId: id })
      if (rep === '') {
        res = false
        marqueEditeurPourErreur(ind)
        if (!focusDonne) focusEditeur(ind)
        focusDonne = true
      } else {
        const chcalcul = traiteMathlive(rep)
        if (chcalcul.indexOf('=') === -1) {
          res = false
          sq.signeEgalOublie = true
          marqueEditeurPourErreur(ind)
          if (!focusDonne) focusEditeur(ind)
          focusDonne = true
        } else {
          const valide = sq.listeCalc.syntaxValidation('rep' + ind, chcalcul, true)
          if (!valide) {
            res = false
            marqueEditeurPourErreur(ind)
            if (!focusDonne) focusEditeur(ind)
            focusDonne = true
          }
        }
      }
    }
    return res
  }

  function validation () {
    sq.rep = [] // Tableau destiné à recevoir les réponses dans les champs d’édition des solutions
    for (let ind = 1; ind <= 3; ind++) {
      const idBlock = 'expressionPhBlock1'
      const id = idBlock + 'ph' + ind
      const rep = getMathliveValue(idBlock, { placeholderId: id })
      const chcalcul = traiteMathlive(rep)
      sq.listeCalc.giveFormula2('rep' + ind, chcalcul)
      sq.rep.push(rep)
    }
    sq.listeCalc.calculateNG(false)
    sq.resolu = sq.listeCalc.valueOf('resolu') === 1
    sq.exact = sq.listeCalc.valueOf('exact') === 1
    sq.presqueresolu = sq.listeCalc.valueOf('presqueResolu') === 1
    sq.refuse = sq.listeCalc.valueOf('refuse') === 1
  }

  function marqueEditeurPourErreur (indice) {
    sq.marked[indice] = true
    const idEditor = 'expressionPhBlock1'
    const editor = j3pElement(idEditor)
    editor.setPromptState(idEditor + 'ph' + indice, 'incorrect')
    editor.demarqueErreur = () => {
      demarqueEditeurPourErreur(indice)
    }
  }

  function demarqueEditeurPourErreur (indice) {
    sq.marked[indice] = false
    const idEditor = 'expressionPhBlock1'
    const editor = j3pElement(idEditor)
    editor.getPrompts().forEach((id) => {
      editor.setPromptState(id, 'undefined')
    })
  }

  function afficheReponse (bilan) {
    const num = sq.numEssai
    const idrep = 'exp' + num
    if (num >= 2) afficheMathliveDans('formules', idrep + 'br', '<br>')
    let ch, coul
    let equivalent = true
    if (bilan === 'refuse') {
      ch = 'Calcul refusé : &nbsp; &nbsp;'
      coul = '#FF0000'
    } else {
      if ((bilan === 'resolu')) {
        ch = 'Resolu : &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; '
        coul = parcours.styles.cbien
      } else {
        if (bilan === 'exact') {
          ch = 'Equivalent : &nbsp; &nbsp; &nbsp; &nbsp;'
          coul = '#0000FF'
        } else {
          if (bilan === 'exactPasFini') {
            ch = 'Presque résolu : &nbsp; &nbsp; &nbsp; &nbsp;'
            coul = '#0000FF'
          } else {
            if (bilan === 'faux') {
              equivalent = false
              ch = 'Non équivalent : '
              coul = '#FF0000'
            } else { // Réponse exacte mais nombre d’essais dépassé
              ch = 'Calcul pas fini : <br>'
              coul = '#FF0000'
            }
          }
        }
      }
    }
    let reponse = sq.finFormulaire
    for (let i = 0; i < 3; i++) {
      const lig = sq.rep[i].replaceAll('\\frac', '\\dfrac')
      reponse = reponse.replace('\\editable{}', lig)
    }
    if (bilan === 'refuse') {
      afficheMathliveDans('formules', idrep + 'sys', ch + '$' + reponse + '$', {
        style: { color: coul }
      })
      return
    }
    afficheMathliveDans('formules', idrep + 'deb', ch, {
      style: { color: coul }
    })
    afficheMathliveDans('formules', idrep + 'suite', '$' + sq.debFormulaire + '$', {
      style: { color: 'black' }
    })
    const fin = (equivalent ? '\\' : '\\n') + 'Leftrightarrow' + reponse
    afficheMathliveDans('formules', idrep + 'fin', '$' + fin + '$', {
      style: { color: coul }
    })

    resetKeyboardPosition()
  }

  function montreEditeurs (bVisible) {
    $('#editeur').css('display', bVisible ? 'block' : 'none')
    if (bVisible) {
      $('#divInfo').css('display', 'block')
      $('#boutonsmathquill').css('display', 'block')
      setTimeout(function () {
        focusEditeur(1)
      })
    } else {
      $('#boutonsmathquill').css('display', 'none')
    }
  }

  function videEditeurs () {
    const idmf = 'expressionPhBlock1'
    const mf = j3pElement(idmf)
    for (let i = 1; i <= 3; i++) {
      mf.setPromptValue(idmf + 'ph' + i, '')
    }
  }

  function recopierReponse (indEdit) {
    const ch = sq.numEssai > 1 ? sq.rep[indEdit - 1] : sq['equation' + indEdit]
    const idEditor = 'expressionPhBlock1'
    const editor = j3pElement(idEditor)
    editor.setPromptValue(idEditor + 'ph' + indEdit, ch)
    demarqueEditeurPourErreur(indEdit)
    focusEditeur(indEdit)
  }

  function initMtg () {
    setMathliveCss()
    const mtgOptions = {
      // player only, sans figure initiale ni svg, on gère ça dans initMtg et modif_fig
      loadCoreWithMathJax: true
    }
    getMtgApp('mtg32svg', {}, mtgOptions).then((mtgAppLecteur) => {
      sq.mtgAppLecteur = mtgAppLecteur
      initDom()
      sq.marked = [false, false, false]
      let par, nbrep, ar, tir, nb, nbcas, code
      // On crée provisoirement le DIV ci-dessous pour pouvoir calculer la figure.
      // On le détruira ensuite pour le récréer à la bonne position
      j3pDiv('conteneur', {
        id: 'formules',
        contenu: '',
        style: parcours.styles.toutpetit.enonce
      }) // Contient le formules entrées par l’élève
      j3pAddElt('conteneur', 'div', '', { id: 'divSolution' })
      $('#divSolution').css('display', 'none')
      j3pDiv('conteneur', 'divmtg32', '')
      j3pCreeSVG('divmtg32', {
        id: 'mtg32svg',
        width: sq.width,
        height: sq.height
      })
      // On crée une liste contenant la figure chargée de vérifier les calculs
      sq.listeCalc = mtgAppLecteur.createList('TWF0aEdyYXBoSmF2YTEuMAAAABM+TMzNAAJmcvb6#gEA#wEAAAAAAAAAAAUqAAAC4AAAAQEAAAAAAAAAAQAAAKz#####AAAAAQAKQ0NhbGNDb25zdAD#####AAJwaQAWMy4xNDE1OTI2NTM1ODk3OTMyMzg0Nv####8AAAABAApDQ29uc3RhbnRlQAkh+1RELRj#####AAAAAQAHQ0NhbGN1bAD#####AAFhAAExAAAAAT#wAAAAAAAAAAAAAgD#####AAFiAAExAAAAAT#wAAAAAAAAAAAAAgD#####AAFjAAExAAAAAT#wAAAAAAAAAAAAAgD#####AAFkAAExAAAAAT#wAAAAAAAAAAAAAgD#####AAFlAAExAAAAAT#wAAAAAAAAAAAAAgD#####AAFmAAExAAAAAT#wAAAAAAAAAAAAAgD#####AAFnAAExAAAAAT#wAAAAAAAAAAAAAgD#####AAFoAAExAAAAAT#wAAAAAAAAAAAAAgD#####AAFqAAExAAAAAT#wAAAAAAAAAAAAAgD#####AAFrAAExAAAAAT#wAAAAAAAAAAAAAgD#####AAFsAAExAAAAAT#wAAAAAAAAAAAAAgD#####AAFtAAExAAAAAT#wAAAAAAAAAAAAAgD#####AAFuAAExAAAAAT#wAAAAAAAAAAAAAgD#####AAFvAAExAAAAAT#wAAAAAAAAAAAAAgD#####AAFwAAExAAAAAT#wAAAAAAAAAAAAAgD#####AAFxAAExAAAAAT#wAAAAAAAAAAAAAgD#####AAFyAAExAAAAAT#wAAAAAAAA#####wAAAAEACUNGb25jTlZhcgD#####AANlcTEAB3greSt6PTH#####AAAAAQAKQ09wZXJhdGlvbggAAAAEAAAAAAQA#####wAAAAIAEUNWYXJpYWJsZUZvcm1lbGxlAAAAAAAAAAUAAAABAAAABQAAAAIAAAABP#AAAAAAAAAAAAADAAF4AAF5AAF6AAAAAwD#####AANlcTIABzMqeT0yKnoAAAAECAAAAAQCAAAAAUAIAAAAAAAAAAAABQAAAAEAAAAEAgAAAAFAAAAAAAAAAAAAAAUAAAACAAAAAwABeAABeQABegAAAAMA#####wADZXEzAAV6PTMqeAAAAAQIAAAABQAAAAIAAAAEAgAAAAFACAAAAAAAAAAAAAUAAAAAAAAAAwABeAABeQABegAAAAMA#####wACZDEAJGdhdWNoZShlcTEoeCx5LHopKS1kcm9pdChlcTEoeCx5LHopKQAAAAQB#####wAAAAIACUNGb25jdGlvbhT#####AAAAAQASQ0FwcGVsRm9uY3Rpb25OVmFyAAAAAwAAABIAAAAFAAAAAAAAAAUAAAABAAAABQAAAAIAAAAGFQAAAAcAAAADAAAAEgAAAAUAAAAAAAAABQAAAAEAAAAFAAAAAgAAAAMAAXgAAXkAAXoAAAADAP####8AAmQyACRnYXVjaGUoZXEyKHgseSx6KSktZHJvaXQoZXEyKHgseSx6KSkAAAAEAQAAAAYUAAAABwAAAAMAAAATAAAABQAAAAAAAAAFAAAAAQAAAAUAAAACAAAABhUAAAAHAAAAAwAAABMAAAAFAAAAAAAAAAUAAAABAAAABQAAAAIAAAADAAF4AAF5AAF6AAAAAwD#####AAJkMwAkZ2F1Y2hlKGVxMyh4LHkseikpLWRyb2l0KGVxMyh4LHkseikpAAAABAEAAAAGFAAAAAcAAAADAAAAFAAAAAUAAAAAAAAABQAAAAEAAAAFAAAAAgAAAAYVAAAABwAAAAMAAAAUAAAABQAAAAAAAAAFAAAAAQAAAAUAAAACAAAAAwABeAABeQABev####8AAAABABFDRGVyaXZlZVBhcnRpZWxsZQD#####AANkeDEAAAAVAAAAAAAAAAgA#####wADZHkxAAAAFQAAAAEAAAAIAP####8AA2R6MQAAABUAAAACAAAACAD#####AANkeDIAAAAWAAAAAAAAAAgA#####wADZHkyAAAAFgAAAAEAAAAIAP####8AA2R6MgAAABYAAAACAAAACAD#####AANkeDMAAAAXAAAAAAAAAAgA#####wADZHkzAAAAFwAAAAEAAAAIAP####8AA2R6MwAAABcAAAACAAAAAgD#####AANhMTEACmR4MSgwLDAsMCkAAAAHAAAAAwAAABgAAAABAAAAAAAAAAAAAAABAAAAAAAAAAAAAAABAAAAAAAAAAAAAAACAP####8AA2ExMgAKZHkxKDAsMCwwKQAAAAcAAAADAAAAGQAAAAEAAAAAAAAAAAAAAAEAAAAAAAAAAAAAAAEAAAAAAAAAAAAAAAIA#####wADYTEzAApkejEoMCwwLDApAAAABwAAAAMAAAAaAAAAAQAAAAAAAAAAAAAAAQAAAAAAAAAAAAAAAQAAAAAAAAAAAAAAAgD#####AANhMjEACmR4MigwLDAsMCkAAAAHAAAAAwAAABsAAAABAAAAAAAAAAAAAAABAAAAAAAAAAAAAAABAAAAAAAAAAAAAAACAP####8AA2EyMgAKZHkyKDAsMCwwKQAAAAcAAAADAAAAHAAAAAEAAAAAAAAAAAAAAAEAAAAAAAAAAAAAAAEAAAAAAAAAAAAAAAIA#####wADYTIzAApkejIoMCwwLDApAAAABwAAAAMAAAAdAAAAAQAAAAAAAAAAAAAAAQAAAAAAAAAAAAAAAQAAAAAAAAAAAAAAAgD#####AANhMzEACmR4MygwLDAsMCkAAAAHAAAAAwAAAB4AAAABAAAAAAAAAAAAAAABAAAAAAAAAAAAAAABAAAAAAAAAAAAAAACAP####8AA2EzMgAKZHkzKDAsMCwwKQAAAAcAAAADAAAAHwAAAAEAAAAAAAAAAAAAAAEAAAAAAAAAAAAAAAEAAAAAAAAAAAAAAAIA#####wADYTMzAApkejMoMCwwLDApAAAABwAAAAMAAAAgAAAAAQAAAAAAAAAAAAAAAQAAAAAAAAAAAAAAAQAAAAAAAAAAAAAAAgD#####AAJiMQAKLWQxKDAsMCwwKf####8AAAABAAxDTW9pbnNVbmFpcmUAAAAHAAAAAwAAABUAAAABAAAAAAAAAAAAAAABAAAAAAAAAAAAAAABAAAAAAAAAAAAAAACAP####8AAmIyAAotZDIoMCwwLDApAAAACQAAAAcAAAADAAAAFgAAAAEAAAAAAAAAAAAAAAEAAAAAAAAAAAAAAAEAAAAAAAAAAAAAAAIA#####wACYjMACi1kMygwLDAsMCkAAAAJAAAABwAAAAMAAAAXAAAAAQAAAAAAAAAAAAAAAQAAAAAAAAAAAAAAAQAAAAAAAAAA#####wAAAAEACENNYXRyaWNlAP####8AAUEAAAADAAAAA#####8AAAABAA9DUmVzdWx0YXRWYWxldXIAAAAhAAAACwAAACIAAAALAAAAIwAAAAsAAAAkAAAACwAAACUAAAALAAAAJgAAAAsAAAAnAAAACwAAACgAAAALAAAAKQAAAAoA#####wABQgAAAAMAAAABAAAACwAAACoAAAALAAAAKwAAAAsAAAAs#####wAAAAEAFENJbXBsZW1lbnRhdGlvblByb3RvAP####8ADETDqXRlcm1pbmFudAAAAAEAAAABAAAAAQAAAC3#####AAAAAQAIQ0NhbGNNYXQAAAAALwAGZGV0TWF0AAhkZXRlcihBKQAAAAYXAAAACwAAAC0AAAACAQAAAC8ABGRldEEAC2RldE1hdCgxLDEp#####wAAAAEACENUZXJtTWF0AAAAMAAAAAE#8AAAAAAAAAAAAAE#8AAAAAAAAAAAAA0A#####wABWAAIQV4oLTEpKkIAAAAEAv####8AAAABAApDUHVpc3NhbmNlAAAACwAAAC0AAAAJAAAAAT#wAAAAAAAAAAAACwAAAC4AAAACAP####8AAngwAAZYKDEsMSkAAAAOAAAAMgAAAAE#8AAAAAAAAAAAAAE#8AAAAAAAAAAAAAIA#####wACeTAABlgoMiwxKQAAAA4AAAAyAAAAAUAAAAAAAAAAAAAAAT#wAAAAAAAAAAAAAgD#####AAJ6MAAGWCgzLDEpAAAADgAAADIAAAABQAgAAAAAAAAAAAABP#AAAAAAAAAAAAADAP####8ABHJlcDEAA3g9MQAAAAQIAAAABQAAAAAAAAABP#AAAAAAAAAAAAADAAF4AAF5AAF6AAAAAwD#####AARyZXAyAAN5PTEAAAAECAAAAAUAAAABAAAAAT#wAAAAAAAAAAAAAwABeAABeQABegAAAAMA#####wAEcmVwMwADej0xAAAABAgAAAAFAAAAAgAAAAE#8AAAAAAAAAAAAAMAAXgAAXkAAXoAAAADAP####8ABGRpZjEAJmdhdWNoZShyZXAxKHgseSx6KSktZHJvaXQocmVwMSh4LHkseikpAAAABAEAAAAGFAAAAAcAAAADAAAANgAAAAUAAAAAAAAABQAAAAEAAAAFAAAAAgAAAAYVAAAABwAAAAMAAAA2AAAABQAAAAAAAAAFAAAAAQAAAAUAAAACAAAAAwABeAABeQABegAAAAMA#####wAEZGlmMgAmZ2F1Y2hlKHJlcDIoeCx5LHopKS1kcm9pdChyZXAyKHgseSx6KSkAAAAEAQAAAAYUAAAABwAAAAMAAAA3AAAABQAAAAAAAAAFAAAAAQAAAAUAAAACAAAABhUAAAAHAAAAAwAAADcAAAAFAAAAAAAAAAUAAAABAAAABQAAAAIAAAADAAF4AAF5AAF6AAAAAwD#####AARkaWYzACZnYXVjaGUocmVwMyh4LHkseikpLWRyb2l0KHJlcDMoeCx5LHopKQAAAAQBAAAABhQAAAAHAAAAAwAAADgAAAAFAAAAAAAAAAUAAAABAAAABQAAAAIAAAAGFQAAAAcAAAADAAAAOAAAAAUAAAAAAAAABQAAAAEAAAAFAAAAAgAAAAMAAXgAAXkAAXr#####AAAAAQAFQ0ZvbmMA#####wAEemVybwASYWJzKHgpPDAuMDAwMDAwMDAxAAAABAQAAAAGAAAAAAUAAAAAAAAAAT4RLgvoJtaVAAF4AAAACAD#####AAVkZXJ4MQAAADkAAAAAAAAACAD#####AAVkZXJ5MQAAADkAAAABAAAACAD#####AAVkZXJ6MQAAADkAAAACAAAACAD#####AAVkZXJ4MgAAADoAAAAAAAAACAD#####AAVkZXJ5MgAAADoAAAABAAAACAD#####AAVkZXJ6MgAAADoAAAACAAAACAD#####AAVkZXJ4MwAAADsAAAAAAAAACAD#####AAVkZXJ5MwAAADsAAAABAAAACAD#####AAVkZXJ6MwAAADsAAAACAAAAAgD#####AAJ4MQAJMStyYW5kKDApAAAABAAAAAABP#AAAAAAAAAAAAAGEQAAAAEAAAAAAAAAAD#QX5jBKxZwAAAAAgD#####AAJ4MgALMS4xK3JhbmQoMCkAAAAEAAAAAAE#8ZmZmZmZmgAAAAYRAAAAAQAAAAAAAAAAP+UY7ApMHJYAAAACAP####8AAngzAAsxLjIrcmFuZCgwKQAAAAQAAAAAAT#zMzMzMzMzAAAABhEAAAABAAAAAAAAAAA#2nJbQDgNUAAAAAIA#####wACeTEACTErcmFuZCgwKQAAAAQAAAAAAT#wAAAAAAAAAAAABhEAAAABAAAAAAAAAAA#3NbG2Sq7VAAAAAIA#####wACeTIACzEuMStyYW5kKDApAAAABAAAAAABP#GZmZmZmZoAAAAGEQAAAAEAAAAAAAAAAD+Xc+D6dBhAAAAAAgD#####AAJ5MwALMS4yK3JhbmQoMCkAAAAEAAAAAAE#8zMzMzMzMwAAAAYRAAAAAQAAAAAAAAAAP89FOkgNrIgAAAACAP####8AAnoxAAkxK3JhbmQoMCkAAAAEAAAAAAE#8AAAAAAAAAAAAAYRAAAAAQAAAAAAAAAAP95LHKE9ccQAAAACAP####8AAnoyAAsxLjErcmFuZCgwKQAAAAQAAAAAAT#xmZmZmZmaAAAABhEAAAABAAAAAAAAAAA#y2QJcW43OAAAAAIA#####wACejMACzEuMityYW5kKDApAAAABAAAAAABP#MzMzMzMzMAAAAGEQAAAAEAAAAAAAAAAD#oHB7jwSwgAAAAAgD#####AARkZWcxAON6ZXJvKGRlcngxKHgxLHkxLHoxKS1kZXJ4MSh4Mix5Mix6MikpJnplcm8oZGVyeDEoeDIseTIsejIpLWRlcngxKHgzLHkzLHozKSkmemVybyhkZXJ5MSh4MSx5MSx6MSktZGVyeTEoeDIseTIsejIpKSZ6ZXJvKGRlcnkxKHgyLHkyLHoyKS1kZXJ5MSh4Myx5Myx6MykpJnplcm8oZGVyejEoeDEseTEsejEpLWRlcnoxKHgyLHkyLHoyKSkmemVybyhkZXJ6MSh4Mix5Mix6MiktZGVyejEoeDMseTMsejMpKQAAAAQKAAAABAoAAAAECgAAAAQKAAAABAr#####AAAAAQAOQ0FwcGVsRm9uY3Rpb24AAAA8AAAABAEAAAAHAAAAAwAAAD0AAAALAAAARgAAAAsAAABJAAAACwAAAEwAAAAHAAAAAwAAAD0AAAALAAAARwAAAAsAAABKAAAACwAAAE0AAAARAAAAPAAAAAQBAAAABwAAAAMAAAA9AAAACwAAAEcAAAALAAAASgAAAAsAAABNAAAABwAAAAMAAAA9AAAACwAAAEgAAAALAAAASwAAAAsAAABOAAAAEQAAADwAAAAEAQAAAAcAAAADAAAAPgAAAAsAAABGAAAACwAAAEkAAAALAAAATAAAAAcAAAADAAAAPgAAAAsAAABHAAAACwAAAEoAAAALAAAATQAAABEAAAA8AAAABAEAAAAHAAAAAwAAAD4AAAALAAAARwAAAAsAAABKAAAACwAAAE0AAAAHAAAAAwAAAD4AAAALAAAASAAAAAsAAABLAAAACwAAAE4AAAARAAAAPAAAAAQBAAAABwAAAAMAAAA#AAAACwAAAEYAAAALAAAASQAAAAsAAABMAAAABwAAAAMAAAA#AAAACwAAAEcAAAALAAAASgAAAAsAAABNAAAAEQAAADwAAAAEAQAAAAcAAAADAAAAPwAAAAsAAABHAAAACwAAAEoAAAALAAAATQAAAAcAAAADAAAAPwAAAAsAAABIAAAACwAAAEsAAAALAAAATgAAAAIA#####wAEZGVnMgDjemVybyhkZXJ4Mih4MSx5MSx6MSktZGVyeDIoeDIseTIsejIpKSZ6ZXJvKGRlcngyKHgyLHkyLHoyKS1kZXJ4Mih4Myx5Myx6MykpJnplcm8oZGVyeTIoeDEseTEsejEpLWRlcnkyKHgyLHkyLHoyKSkmemVybyhkZXJ5Mih4Mix5Mix6MiktZGVyeTIoeDMseTMsejMpKSZ6ZXJvKGRlcnoyKHgxLHkxLHoxKS1kZXJ6Mih4Mix5Mix6MikpJnplcm8oZGVyejIoeDIseTIsejIpLWRlcnoyKHgzLHkzLHozKSkAAAAECgAAAAQKAAAABAoAAAAECgAAAAQKAAAAEQAAADwAAAAEAQAAAAcAAAADAAAAQAAAAAsAAABGAAAACwAAAEkAAAALAAAATAAAAAcAAAADAAAAQAAAAAsAAABHAAAACwAAAEoAAAALAAAATQAAABEAAAA8AAAABAEAAAAHAAAAAwAAAEAAAAALAAAARwAAAAsAAABKAAAACwAAAE0AAAAHAAAAAwAAAEAAAAALAAAASAAAAAsAAABLAAAACwAAAE4AAAARAAAAPAAAAAQBAAAABwAAAAMAAABBAAAACwAAAEYAAAALAAAASQAAAAsAAABMAAAABwAAAAMAAABBAAAACwAAAEcAAAALAAAASgAAAAsAAABNAAAAEQAAADwAAAAEAQAAAAcAAAADAAAAQQAAAAsAAABHAAAACwAAAEoAAAALAAAATQAAAAcAAAADAAAAQQAAAAsAAABIAAAACwAAAEsAAAALAAAATgAAABEAAAA8AAAABAEAAAAHAAAAAwAAAEIAAAALAAAARgAAAAsAAABJAAAACwAAAEwAAAAHAAAAAwAAAEIAAAALAAAARwAAAAsAAABKAAAACwAAAE0AAAARAAAAPAAAAAQBAAAABwAAAAMAAABCAAAACwAAAEcAAAALAAAASgAAAAsAAABNAAAABwAAAAMAAABCAAAACwAAAEgAAAALAAAASwAAAAsAAABOAAAAAgD#####AARkZWczAON6ZXJvKGRlcngzKHgxLHkxLHoxKS1kZXJ4Myh4Mix5Mix6MikpJnplcm8oZGVyeDMoeDIseTIsejIpLWRlcngzKHgzLHkzLHozKSkmemVybyhkZXJ5Myh4MSx5MSx6MSktZGVyeTMoeDIseTIsejIpKSZ6ZXJvKGRlcnkzKHgyLHkyLHoyKS1kZXJ5Myh4Myx5Myx6MykpJnplcm8oZGVyejMoeDEseTEsejEpLWRlcnozKHgyLHkyLHoyKSkmemVybyhkZXJ6Myh4Mix5Mix6MiktZGVyejMoeDMseTMsejMpKQAAAAQKAAAABAoAAAAECgAAAAQKAAAABAoAAAARAAAAPAAAAAQBAAAABwAAAAMAAABDAAAACwAAAEYAAAALAAAASQAAAAsAAABMAAAABwAAAAMAAABDAAAACwAAAEcAAAALAAAASgAAAAsAAABNAAAAEQAAADwAAAAEAQAAAAcAAAADAAAAQwAAAAsAAABHAAAACwAAAEoAAAALAAAATQAAAAcAAAADAAAAQwAAAAsAAABIAAAACwAAAEsAAAALAAAATgAAABEAAAA8AAAABAEAAAAHAAAAAwAAAEQAAAALAAAARgAAAAsAAABJAAAACwAAAEwAAAAHAAAAAwAAAEQAAAALAAAARwAAAAsAAABKAAAACwAAAE0AAAARAAAAPAAAAAQBAAAABwAAAAMAAABEAAAACwAAAEcAAAALAAAASgAAAAsAAABNAAAABwAAAAMAAABEAAAACwAAAEgAAAALAAAASwAAAAsAAABOAAAAEQAAADwAAAAEAQAAAAcAAAADAAAARQAAAAsAAABGAAAACwAAAEkAAAALAAAATAAAAAcAAAADAAAARQAAAAsAAABHAAAACwAAAEoAAAALAAAATQAAABEAAAA8AAAABAEAAAAHAAAAAwAAAEUAAAALAAAARwAAAAsAAABKAAAACwAAAE0AAAAHAAAAAwAAAEUAAAALAAAASAAAAAsAAABLAAAACwAAAE4AAAADAP####8ABGVnYWwAA3g9eQAAAAQIAAAABQAAAAAAAAAFAAAAAQAAAAMAAXgAAXkAAXoAAAADAP####8AA2lkeAABeAAAAAUAAAAAAAAAAwABeAABeQABegAAAAMA#####wADaWR5AAF5AAAABQAAAAEAAAADAAF4AAF5AAF6AAAAAwD#####AANpZHoAAXoAAAAFAAAAAgAAAAMAAXgAAXkAAXr#####AAAAAQAMQ1Rlc3RFcU5hdE9wAP####8AB2VnYWxyZXAAAAA2AAAAUgAAABIA#####wAIZWdhbHJlcDIAAAA3AAAAUgAAABIA#####wAIZWdhbHJlcDMAAAA4AAAAUgAAAAMA#####wAHZ2F1Y2hlMQATZ2F1Y2hlKHJlcDEoeCx5LHopKQAAAAYUAAAABwAAAAMAAAA2AAAABQAAAAAAAAAFAAAAAQAAAAUAAAACAAAAAwABeAABeQABegAAAAMA#####wAGZHJvaXQxABJkcm9pdChyZXAxKHgseSx6KSkAAAAGFQAAAAcAAAADAAAANgAAAAUAAAAAAAAABQAAAAEAAAAFAAAAAgAAAAMAAXgAAXkAAXoAAAADAP####8AB2dhdWNoZTIAE2dhdWNoZShyZXAyKHgseSx6KSkAAAAGFAAAAAcAAAADAAAANwAAAAUAAAAAAAAABQAAAAEAAAAFAAAAAgAAAAMAAXgAAXkAAXoAAAADAP####8ABmRyb2l0MgASZHJvaXQocmVwMih4LHkseikpAAAABhUAAAAHAAAAAwAAADcAAAAFAAAAAAAAAAUAAAABAAAABQAAAAIAAAADAAF4AAF5AAF6AAAAAwD#####AAdnYXVjaGUzABNnYXVjaGUocmVwMyh4LHkseikpAAAABhQAAAAHAAAAAwAAADgAAAAFAAAAAAAAAAUAAAABAAAABQAAAAIAAAADAAF4AAF5AAF6AAAAAwD#####AAZkcm9pdDMAEmRyb2l0KHJlcDMoeCx5LHopKQAAAAYVAAAABwAAAAMAAAA4AAAABQAAAAAAAAAFAAAAAQAAAAUAAAACAAAAAwABeAABeQABev####8AAAADABBDVGVzdEVxdWl2YWxlbmNlAP####8AC2dhdWNoZWVzdHgxAAAAWQAAAFMBAAAAAAE#8AAAAAAAAAEAAAATAP####8AC2dhdWNoZWVzdHkxAAAAWQAAAFQBAAAAAAE#8AAAAAAAAAEAAAATAP####8AC2dhdWNoZWVzdHoxAAAAWQAAAFUBAAAAAAE#8AAAAAAAAAEAAAATAP####8ACmRyb2l0ZXN0eDEAAABaAAAAUwEAAAAAAT#wAAAAAAAAAQAAABMA#####wAKZHJvaXRlc3R5MQAAAFoAAABUAQAAAAABP#AAAAAAAAABAAAAEwD#####AApkcm9pdGVzdHoxAAAAWgAAAFUBAAAAAAE#8AAAAAAAAAEAAAATAP####8AC2dhdWNoZWVzdHgyAAAAWwAAAFMBAAAAAAE#8AAAAAAAAAEAAAATAP####8AC2dhdWNoZWVzdHkyAAAAWwAAAFQBAAAAAAE#8AAAAAAAAAEAAAATAP####8AC2dhdWNoZWVzdHoyAAAAWwAAAFUBAAAAAAE#8AAAAAAAAAEAAAATAP####8ACmRyb2l0ZXN0eDIAAABcAAAAUwEAAAAAAT#wAAAAAAAAAQAAABMA#####wAKZHJvaXRlc3R5MgAAAFwAAABUAQAAAAABP#AAAAAAAAABAAAAEwD#####AApkcm9pdGVzdHoyAAAAXAAAAFUBAAAAAAE#8AAAAAAAAAEAAAATAP####8AC2dhdWNoZWVzdHgzAAAAXQAAAFMBAAAAAAE#8AAAAAAAAAEAAAATAP####8AC2dhdWNoZWVzdHkzAAAAXQAAAFQBAAAAAAE#8AAAAAAAAAEAAAATAP####8AC2dhdWNoZWVzdHozAAAAXQAAAFUBAAAAAAE#8AAAAAAAAAEAAAATAP####8ACmRyb2l0ZXN0eDMAAABeAAAAUwEAAAAAAT#wAAAAAAAAAQAAABMA#####wAKZHJvaXRlc3R5MwAAAF4AAABUAQAAAAABP#AAAAAAAAABAAAAEwD#####AApkcm9pdGVzdHozAAAAXgAAAFUBAAAAAAE#8AAAAAAAAAEAAAANAP####8AB3NvbGZyYWMAB2ZyYWMoWCkAAAAGGQAAAAsAAAAyAAAAAgD#####AANueDAADHNvbGZyYWMoMSwxKQAAAA4AAABxAAAAAT#wAAAAAAAAAAAAAT#wAAAAAAAAAAAAAgD#####AANkeDAADHNvbGZyYWMoMSwyKQAAAA4AAABxAAAAAT#wAAAAAAAAAAAAAUAAAAAAAAAAAAAAAgD#####AANueTAADHNvbGZyYWMoMiwxKQAAAA4AAABxAAAAAUAAAAAAAAAAAAAAAT#wAAAAAAAAAAAAAgD#####AANkeTAADHNvbGZyYWMoMiwyKQAAAA4AAABxAAAAAUAAAAAAAAAAAAAAAUAAAAAAAAAAAAAAAgD#####AANuejAADHNvbGZyYWMoMywxKQAAAA4AAABxAAAAAUAIAAAAAAAAAAAAAT#wAAAAAAAAAAAAAgD#####AANkejAADHNvbGZyYWMoMywyKQAAAA4AAABxAAAAAUAIAAAAAAAAAAAAAUAAAAAAAAAAAAAAAwD#####AARzb2x4AAl4PW54MC9keDAAAAAECAAAAAUAAAAAAAAABAMAAAALAAAAcgAAAAsAAABzAAAAAwABeAABeQABegAAAAMA#####wAEc29seQAJeT1ueTAvZHkwAAAABAgAAAAFAAAAAQAAAAQDAAAACwAAAHQAAAALAAAAdQAAAAMAAXgAAXkAAXoAAAADAP####8ABHNvbHoACXo9bnowL2R6MAAAAAQIAAAABQAAAAIAAAAEAwAAAAsAAAB2AAAACwAAAHcAAAADAAF4AAF5AAF6AAAAEwD#####AAxzb2x4ZGFuc3JlcDEAAAB4AAAANgEAAAAAAT#wAAAAAAAAAQAAABMA#####wAMc29seGRhbnNyZXAyAAAAeAAAADcBAAAAAAE#8AAAAAAAAAEAAAATAP####8ADHNvbHhkYW5zcmVwMwAAAHgAAAA4AQAAAAABP#AAAAAAAAABAAAAEwD#####AAxzb2x5ZGFuc3JlcDEAAAB5AAAANgEAAAAAAT#wAAAAAAAAAQAAABMA#####wAMc29seWRhbnNyZXAyAAAAeQAAADcBAAAAAAE#8AAAAAAAAAEAAAATAP####8ADHNvbHlkYW5zcmVwMwAAAHkAAAA4AQAAAAABP#AAAAAAAAABAAAAEwD#####AAxzb2x6ZGFuc3JlcDEAAAB6AAAANgEAAAAAAT#wAAAAAAAAAQAAABMA#####wAMc29semRhbnNyZXAyAAAAegAAADcBAAAAAAE#8AAAAAAAAAEAAAATAP####8ADHNvbHpkYW5zcmVwMwAAAHoAAAA4AQAAAAABP#AAAAAAAAABAAAAAgD#####AAZyZXNvbHUA6XNvbHhkYW5zcmVwMSZzb2x5ZGFuc3JlcDImc29semRhbnNyZXAzfHNvbHhkYW5zcmVwMSZzb2x5ZGFuc3JlcDMmc29semRhbnNyZXAyfHNvbHhkYW5zcmVwMiZzb2x5ZGFuc3JlcDEmc29semRhbnNyZXAzfHNvbHhkYW5zcmVwMiZzb2x5ZGFuc3JlcDMmc29semRhbnNyZXAxfHNvbHhkYW5zcmVwMyZzb2x5ZGFuc3JlcDEmc29semRhbnNyZXAyfHNvbHhkYW5zcmVwMyZzb2x5ZGFuc3JlcDImc29semRhbnNyZXAxAAAABAsAAAAECwAAAAQLAAAABAsAAAAECwAAAAQKAAAABAoAAAALAAAAewAAAAsAAAB#AAAACwAAAIMAAAAECgAAAAQKAAAACwAAAHsAAAALAAAAgAAAAAsAAACCAAAABAoAAAAECgAAAAsAAAB8AAAACwAAAH4AAAALAAAAgwAAAAQKAAAABAoAAAALAAAAfAAAAAsAAACAAAAACwAAAIEAAAAECgAAAAQKAAAACwAAAH0AAAALAAAAfgAAAAsAAACCAAAABAoAAAAECgAAAAsAAAB9AAAACwAAAH8AAAALAAAAgQAAAAoA#####wACQScAAAADAAAAAwAAAAcAAAADAAAAPQAAAAEAAAAAAAAAAAAAAAEAAAAAAAAAAAAAAAEAAAAAAAAAAAAAAAcAAAADAAAAPgAAAAEAAAAAAAAAAAAAAAEAAAAAAAAAAAAAAAEAAAAAAAAAAAAAAAcAAAADAAAAPwAAAAEAAAAAAAAAAAAAAAEAAAAAAAAAAAAAAAEAAAAAAAAAAAAAAAcAAAADAAAAQAAAAAEAAAAAAAAAAAAAAAEAAAAAAAAAAAAAAAEAAAAAAAAAAAAAAAcAAAADAAAAQQAAAAEAAAAAAAAAAAAAAAEAAAAAAAAAAAAAAAEAAAAAAAAAAAAAAAcAAAADAAAAQgAAAAEAAAAAAAAAAAAAAAEAAAAAAAAAAAAAAAEAAAAAAAAAAAAAAAcAAAADAAAAQwAAAAEAAAAAAAAAAAAAAAEAAAAAAAAAAAAAAAEAAAAAAAAAAAAAAAcAAAADAAAARAAAAAEAAAAAAAAAAAAAAAEAAAAAAAAAAAAAAAEAAAAAAAAAAAAAAAcAAAADAAAARQAAAAEAAAAAAAAAAAAAAAEAAAAAAAAAAAAAAAEAAAAAAAAAAAAAAAwA#####wAMRMOpdGVybWluYW50AAAAAQAAAAEAAAABAAAAhQAAAA0AAAAAhgAGZGV0TWF0AAlkZXRlcihBJykAAAAGFwAAAAsAAACFAAAAAgEAAACGAAVkZXRBJwALZGV0TWF0KDEsMSkAAAAOAAAAhwAAAAE#8AAAAAAAAAAAAAE#8AAAAAAAAP####8AAAABABdDVGVzdERlcGVuZGFuY2VWYXJpYWJsZQD#####AAtnYXVjaGVkZXB4MQAAAFkAAAAAAAAAFAD#####AAtnYXVjaGVkZXB5MQAAAFkAAAABAAAAFAD#####AAtnYXVjaGVkZXB6MQAAAFkAAAACAAAAFAD#####AApkcm9pdGRlcHgxAAAAWgAAAAAAAAAUAP####8ACmRyb2l0ZGVweTEAAABaAAAAAQAAABQA#####wAKZHJvaXRkZXB6MQAAAFoAAAACAAAAFAD#####AAtnYXVjaGVkZXB4MgAAAFsAAAAAAAAAFAD#####AAtnYXVjaGVkZXB5MgAAAFsAAAABAAAAFAD#####AAtnYXVjaGVkZXB6MgAAAFsAAAACAAAAFAD#####AApkcm9pdGRlcHgyAAAAXAAAAAAAAAAUAP####8ACmRyb2l0ZGVweTIAAABcAAAAAQAAABQA#####wAKZHJvaXRkZXB6MgAAAFwAAAACAAAAFAD#####AAtnYXVjaGVkZXB4MwAAAF0AAAAAAAAAFAD#####AAtnYXVjaGVkZXB5MwAAAF0AAAABAAAAFAD#####AAtnYXVjaGVkZXB6MwAAAF0AAAACAAAAFAD#####AApkcm9pdGRlcHgzAAAAXgAAAAAAAAAUAP####8ACmRyb2l0ZGVweTMAAABeAAAAAQAAABQA#####wAKZHJvaXRkZXB6MwAAAF4AAAACAAAACgD#####AAJCJwAAAAMAAAABAAAACQAAAAcAAAADAAAAOQAAAAEAAAAAAAAAAAAAAAEAAAAAAAAAAAAAAAEAAAAAAAAAAAAAAAkAAAAHAAAAAwAAADoAAAABAAAAAAAAAAAAAAABAAAAAAAAAAAAAAABAAAAAAAAAAAAAAAJAAAABwAAAAMAAAA7AAAAAQAAAAAAAAAAAAAAAQAAAAAAAAAAAAAAAQAAAAAAAAAAAAAADQD#####AAJYJwAKQSdeKC0xKSpCJwAAAAQCAAAADwAAAAsAAACFAAAACQAAAAE#8AAAAAAAAAAAAAsAAACbAAAAAgD#####AAVleGFjdABWZGVnMSZkZWcyJmRlZzMmZGV0QSc8PjAmemVybyhYKDEsMSktWCcoMSwxKSkmemVybyhYKDIsMSktWCcoMiwxKSkmemVybyhYKDMsMSktWCcoMywxKSkAAAAECgAAAAQKAAAABAoAAAAECgAAAAQKAAAABAoAAAALAAAATwAAAAsAAABQAAAACwAAAFEAAAAECQAAAAsAAACIAAAAAQAAAAAAAAAAAAAAEQAAADwAAAAEAQAAAA4AAAAyAAAAAT#wAAAAAAAAAAAAAT#wAAAAAAAAAAAADgAAAJwAAAABP#AAAAAAAAAAAAABP#AAAAAAAAAAAAARAAAAPAAAAAQBAAAADgAAADIAAAABQAAAAAAAAAAAAAABP#AAAAAAAAAAAAAOAAAAnAAAAAFAAAAAAAAAAAAAAAE#8AAAAAAAAAAAABEAAAA8AAAABAEAAAAOAAAAMgAAAAFACAAAAAAAAAAAAAE#8AAAAAAAAAAAAA4AAACcAAAAAUAIAAAAAAAAAAAAAT#wAAAAAAAAAAAAAgD#####AAZlZ2FseDEAUmV4YWN0JmdhdWNoZWVzdHgxJjEtZHJvaXRkZXB5MSYxLWRyb2l0ZGVwejF8ZHJvaXRlc3R4MSYxLWdhdWNoZWRlcHkxJjEtZ2F1Y2hlZGVwejEAAAAECwAAAAQKAAAABAoAAAAECgAAAAsAAACdAAAACwAAAF8AAAAEAQAAAAE#8AAAAAAAAAAAAAsAAACNAAAABAEAAAABP#AAAAAAAAAAAAALAAAAjgAAAAQKAAAABAoAAAALAAAAYgAAAAQBAAAAAT#wAAAAAAAAAAAACwAAAIoAAAAEAQAAAAE#8AAAAAAAAAAAAAsAAACLAAAAAgD#####AAZlZ2FseTEAUmV4YWN0JmdhdWNoZWVzdHkxJjEtZHJvaXRkZXB4MSYxLWRyb2l0ZGVwejF8ZHJvaXRlc3R5MSYxLWdhdWNoZWRlcHgxJjEtZ2F1Y2hlZGVwejEAAAAECwAAAAQKAAAABAoAAAAECgAAAAsAAACdAAAACwAAAGAAAAAEAQAAAAE#8AAAAAAAAAAAAAsAAACMAAAABAEAAAABP#AAAAAAAAAAAAALAAAAjgAAAAQKAAAABAoAAAALAAAAYwAAAAQBAAAAAT#wAAAAAAAAAAAACwAAAIkAAAAEAQAAAAE#8AAAAAAAAAAAAAsAAACLAAAAAgD#####AAZlZ2FsejEAUmV4YWN0JmdhdWNoZWVzdHoxJjEtZHJvaXRkZXB4MSYxLWRyb2l0ZGVweTF8ZHJvaXRlc3R6MSYxLWdhdWNoZWRlcHgxJjEtZ2F1Y2hlZGVweTEAAAAECwAAAAQKAAAABAoAAAAECgAAAAsAAACdAAAACwAAAGEAAAAEAQAAAAE#8AAAAAAAAAAAAAsAAACMAAAABAEAAAABP#AAAAAAAAAAAAALAAAAjQAAAAQKAAAABAoAAAALAAAAZAAAAAQBAAAAAT#wAAAAAAAAAAAACwAAAIkAAAAEAQAAAAE#8AAAAAAAAAAAAAsAAACKAAAAAgD#####AAZlZ2FseDIAUmV4YWN0JmdhdWNoZWVzdHgyJjEtZHJvaXRkZXB5MiYxLWRyb2l0ZGVwejJ8ZHJvaXRlc3R4MiYxLWdhdWNoZWRlcHkyJjEtZ2F1Y2hlZGVwejIAAAAECwAAAAQKAAAABAoAAAAECgAAAAsAAACdAAAACwAAAGUAAAAEAQAAAAE#8AAAAAAAAAAAAAsAAACTAAAABAEAAAABP#AAAAAAAAAAAAALAAAAlAAAAAQKAAAABAoAAAALAAAAaAAAAAQBAAAAAT#wAAAAAAAAAAAACwAAAJAAAAAEAQAAAAE#8AAAAAAAAAAAAAsAAACRAAAAAgD#####AAZlZ2FseTIAUmV4YWN0JmdhdWNoZWVzdHkyJjEtZHJvaXRkZXB4MiYxLWRyb2l0ZGVwejJ8ZHJvaXRlc3R5MiYxLWdhdWNoZWRlcHgyJjEtZ2F1Y2hlZGVwejIAAAAECwAAAAQKAAAABAoAAAAECgAAAAsAAACdAAAACwAAAGYAAAAEAQAAAAE#8AAAAAAAAAAAAAsAAACSAAAABAEAAAABP#AAAAAAAAAAAAALAAAAlAAAAAQKAAAABAoAAAALAAAAaQAAAAQBAAAAAT#wAAAAAAAAAAAACwAAAI8AAAAEAQAAAAE#8AAAAAAAAAAAAAsAAACRAAAAAgD#####AAZlZ2FsejIAUmV4YWN0JmdhdWNoZWVzdHoyJjEtZHJvaXRkZXB4MiYxLWRyb2l0ZGVweTJ8ZHJvaXRlc3R6MiYxLWdhdWNoZWRlcHgyJjEtZ2F1Y2hlZGVweTIAAAAECwAAAAQKAAAABAoAAAAECgAAAAsAAACdAAAACwAAAGcAAAAEAQAAAAE#8AAAAAAAAAAAAAsAAACSAAAABAEAAAABP#AAAAAAAAAAAAALAAAAkwAAAAQKAAAABAoAAAALAAAAagAAAAQBAAAAAT#wAAAAAAAAAAAACwAAAI8AAAAEAQAAAAE#8AAAAAAAAAAAAAsAAACQAAAAAgD#####AAZlZ2FseDMAUmV4YWN0JmdhdWNoZWVzdHgzJjEtZHJvaXRkZXB5MyYxLWRyb2l0ZGVwejN8ZHJvaXRlc3R4MyYxLWdhdWNoZWRlcHkzJjEtZ2F1Y2hlZGVwejMAAAAECwAAAAQKAAAABAoAAAAECgAAAAsAAACdAAAACwAAAGsAAAAEAQAAAAE#8AAAAAAAAAAAAAsAAACZAAAABAEAAAABP#AAAAAAAAAAAAALAAAAmgAAAAQKAAAABAoAAAALAAAAbgAAAAQBAAAAAT#wAAAAAAAAAAAACwAAAJYAAAAEAQAAAAE#8AAAAAAAAAAAAAsAAACXAAAAAgD#####AAZlZ2FseTMAUmV4YWN0JmdhdWNoZWVzdHkzJjEtZHJvaXRkZXB4MyYxLWRyb2l0ZGVwejN8ZHJvaXRlc3R5MyYxLWdhdWNoZWRlcHgzJjEtZ2F1Y2hlZGVwejMAAAAECwAAAAQKAAAABAoAAAAECgAAAAsAAACdAAAACwAAAGwAAAAEAQAAAAE#8AAAAAAAAAAAAAsAAACYAAAABAEAAAABP#AAAAAAAAAAAAALAAAAmgAAAAQKAAAABAoAAAALAAAAbwAAAAQBAAAAAT#wAAAAAAAAAAAACwAAAJUAAAAEAQAAAAE#8AAAAAAAAAAAAAsAAACXAAAAAgD#####AAZlZ2FsejMAUmV4YWN0JmdhdWNoZWVzdHozJjEtZHJvaXRkZXB4MyYxLWRyb2l0ZGVweTN8ZHJvaXRlc3R6MyYxLWdhdWNoZWRlcHgzJjEtZ2F1Y2hlZGVweTMAAAAECwAAAAQKAAAABAoAAAAECgAAAAsAAACdAAAACwAAAG0AAAAEAQAAAAE#8AAAAAAAAAAAAAsAAACYAAAABAEAAAABP#AAAAAAAAAAAAALAAAAmQAAAAQKAAAABAoAAAALAAAAcAAAAAQBAAAAAT#wAAAAAAAAAAAACwAAAJUAAAAEAQAAAAE#8AAAAAAAAAAAAAsAAACWAAAAAgD#####AA1wcmVzcXVlcmVzb2x1AIVleGFjdCYoZWdhbHgxJmVnYWx5MiZlZ2FsejN8ZWdhbHgxJmVnYWx6MiZlZ2FseTN8ZWdhbHgyJmVnYWx5MSZlZ2FsejN8ZWdhbHgyJmVnYWx6MSZlZ2FseTN8ZWdhbHgzJmVnYWx5MSZlZ2FsejJ8ZWdhbHgzJmVnYWx6MSZlZ2FseTIpAAAABAoAAAALAAAAnQAAAAQLAAAABAsAAAAECwAAAAQLAAAABAsAAAAECgAAAAQKAAAACwAAAJ4AAAALAAAAogAAAAsAAACmAAAABAoAAAAECgAAAAsAAACeAAAACwAAAKMAAAALAAAApQAAAAQKAAAABAoAAAALAAAAoQAAAAsAAACfAAAACwAAAKYAAAAECgAAAAQKAAAACwAAAKEAAAALAAAAoAAAAAsAAAClAAAABAoAAAAECgAAAAsAAACkAAAACwAAAJ8AAAALAAAAowAAAAQKAAAABAoAAAALAAAApAAAAAsAAACgAAAACwAAAKIAAAACAP####8ABnJlZnVzZQAQMS1kZWcxJmRlZzImZGVnMwAAAAQKAAAABAoAAAAEAQAAAAE#8AAAAAAAAAAAAAsAAABPAAAACwAAAFAAAAALAAAAUf####8AAAACAAZDTGF0ZXgA#####wAAAAABAAD#####EEA4gAAAAAAAQDGFHrhR64YAAAAAAAAAAAAAAAAAAQAAAAAAAAAAAA1cRm9yU2ltcHtlcTF9AAAAFQD#####AAAAAAEAAP####8QQDiAAAAAAABASsKPXCj1xAAAAAAAAAAAAAAAAAABAAAAAAAAAAAADVxGb3JTaW1we2VxMn0AAAAVAP####8AAAAAAQAA#####xBAN4AAAAAAAEBV4UeuFHriAAAAAAAAAAAAAAAAAAEAAAAAAAAAAAANXEZvclNpbXB7ZXEzff###############w==')
      sq.mtgAppLecteur.removeAllDoc()
      sq.mtgAppLecteur.addDoc('mtg32svg', sq.txt, true)
      sq.mtgAppLecteur.calculate('mtg32svg', true)
      const nbvar = sq.mtgAppLecteur.valueOf('mtg32svg', 'nbvar')
      if (nbvar !== -1) {
        nbrep = parcours.donneesSection.nbrepetitions
        sq.aleat = true
        sq.nbParamAleat = nbvar
        for (let i = 1; i <= nbvar; i++) {
          nbcas = sq.mtgAppLecteur.valueOf('mtg32svg', 'nbcas' + i)
          nb = Math.max(nbrep, nbcas)
          ar = []
          for (let j = 0; j < nb; j++) ar.push(j % nbcas)
          sq['tab' + i] = []
          for (let k = 0; k < nbrep; k++) {
            tir = Math.floor(Math.random() * ar.length)
            sq['tab' + i].push(ar[tir])
            ar.splice(tir, 1)
          }
          sq.mtgAppLecteur.giveFormula2('mtg32svg', 'r' + i, sq['tab' + i][0])
        }
      } else {
        sq.aleat = false
      }

      const chars = 'abcdefghjklmnpqr'
      if (sq.param) {
        for (const char of chars) {
          if (sq.param.includes(char)) {
            par = parcours.donneesSection[char]
            if (par !== 'random') sq.mtgAppLecteur.giveFormula2('mtg32svg', char, par)
          }
        }
      }
      sq.mtgAppLecteur.calculate('mtg32svg', true)
      // On donne à tous les paramètres de la figure de calcul les mêmes valeurs que celles qu’ils ont
      // dans la figure du fichier annexe
      if (sq.param) {
        for (const char of chars) {
          if (sq.param.includes(char)) {
            sq.listeCalc.giveFormula2(char, sq.mtgAppLecteur.valueOf('mtg32svg', char))
          }
        }
      }
      // On informe la liste chargée des calculs des équations du système qui sont dans trois
      // fonctions de 3 variables avec égalité nommées eq1, eq2 et eq3
      for (let i = 1; i < 4; i++) {
        const nomcalc = 'eq' + i
        sq.listeCalc.giveFormula2(nomcalc, sq.mtgAppLecteur.getFormula('mtg32svg', nomcalc))
      }
      sq.listeCalc.calculateNG(false)
      sq.equation1 = sq.listeCalc.getLatexCode(0)
      sq.equation2 = sq.listeCalc.getLatexCode(1)
      sq.equation3 = sq.listeCalc.getLatexCode(2)
      sq.debFormulaire = '\\left\\{ \\begin{array}{l} ' + sq.equation1 + '\\\\' + sq.equation2 + '\\\\' + sq.equation3 + '\\end{array} \\right.'
      sq.finFormulaire = '\\left\\{ \\begin{array}{l} \\editable{} \\\\ \\editable{} \\\\ \\editable{} \\end{array} \\right.'
      sq.formulaire = sq.debFormulaire + '\\Leftrightarrow ' + sq.finFormulaire

      j3pDiv('conteneur', 'conteneurbouton', '')
      j3pAjouteBouton('conteneurbouton', 'boutonrecopierEd1', 'MepBoutonsRepere', 'Recopier équation 1',
        function () { recopierReponse(1) })
      $('#boutonrecopierEd1').css('margin-bottom', '5px') // Pour laisser un peu de place au-dessous du bouton
      j3pAjouteBouton('conteneurbouton', 'boutonrecopierEd2', 'MepBoutonsRepere', 'Recopier équation 2',
        function () { recopierReponse(2) })
      $('#boutonrecopierEd2').css('margin-bottom', '5px').css('margin-left', '10px') // Pour laisser un peu de place au-dessous du bouton
      j3pAjouteBouton('conteneurbouton', 'boutonrecopierEd3', 'MepBoutonsRepere', 'Recopier équation 3',
        function () { recopierReponse(3) })
      $('#boutonrecopierEd3').css('margin-bottom', '5px').css('margin-left', '10px') // Pour laisser un peu de place au-dessous du bouton
      j3pDiv('conteneur', 'editeur', '')
      afficheMathliveDans('editeur', 'expression', sq.formulaire, {
        charset: sq.charset,
        listeBoutons: sq.listeBoutons
      })
      if (sq.charset !== '') {
        mathliveRestrict('expressionPhBlock1', sq.charset)
      }
      j3pDiv('conteneur', 'boutonsmathquill', '')
      $('#editeur').css('font-size', '24px')
      $('#boutonsmathquill').css('padding-top', '10px') // Pour laisser un peu de place au-dessus des boutons
      if (sq.boutonsMathQuill) j3pPaletteMathquill('boutonsmathquill', 'expressionPhBlock1', { liste: sq.listeBoutons, nomdiv: 'palette' })
      // Ligne suivante pour un bon alignement
      $('#palette').css('display', 'inline-block')
      // On empêche les boutons MathQuill de pouvoir avoir le focus clavier
      const pal = j3pElement('palette')
      for (let m = 0; m < pal.childNodes.length; m++) {
        pal.childNodes[m].setAttribute('tabindex', '-1')
      }
      j3pElement('expressionPhBlock1').onkeyup = function () {
        onkeyup.call()
      }
      // On récupère le div de la figure pour le reclasser après les autres div
      const div = j3pElement('conteneur').removeChild(j3pElement('divmtg32'))
      j3pElement('conteneur').appendChild(div)
      sq.mtgAppLecteur.display('mtg32svg')
      const nbLatex = parseInt(sq.nbLatex)
      // La consigne peut contenir jusqu'à 4 paramètres a, b,c et d récupérés comme affichages LaTeX de la figure.
      const t = 'abcd'
      const param = {}
      for (let k = 0; k < nbLatex; k++) {
        code = sq.mtgAppLecteur.getLatexCode('mtg32svg', k)
        param[t.charAt(k)] = code
      }
      const st = sq.consigne1 + (sq.simplifier ? sq.consigne3 : sq.consigne2) + sq.consigne4
      afficheMathliveDans('divEnonce', 'texte', st, param)
      focusEditeur(1)
      parcours.finEnonce()
    }).catch(j3pShowError)
  }

  function initDom () {
    renderMathInDocument({ renderAccessibleContent: '' })
    if (sq.titre) parcours.afficheTitre(sq.titre)

    // Trois lignes suivantes déplacées ici depuis initMtg pour éviter que j3pAffiche("", "divInfo" ne soit appelé avant la création du div divInfo
    j3pDiv(parcours.zones.MG, { id: 'conteneur', contenu: '', style: parcours.styles.etendre('toutpetit.enonce', { padding: '6px' }) })
    j3pDiv('conteneur', 'divEnonce', '')
    j3pDiv('conteneur', 'divInfo', '')
    const style = parcours.styles.petit.correction
    style.marginTop = '1.5em'
    style.marginLeft = '0.5em'
    j3pAddElt(parcours.zonesElts.MD, 'div', '', { id: 'correction', style })
    afficheNombreEssaisRestants()
  }

  switch (this.etat) {
    case 'enonce':
      // code exécuté au lancement de la section
      if (this.debutDeLaSection) {
        // Construction de la page
        parcours.construitStructurePage('presentation1bis')

        // compteur. Si   numEssai > nbEssais, on corrige
        sq.numEssai = 1
        sq.nbEssais = parseInt(parcours.donneesSection.nbEssais)
        sq.simplifier = parcours.donneesSection.simplifier

        // faut gérer sous-dossier ou pas…
        const sep = this.donneesSection.ex.includes('/') ? '-' : '/'
        j3pImporteAnnexe('squelettes-mtg32' + sep + this.donneesSection.ex + '.js').then(function (datasSection) {
          // console.log("initsection");
          // Attention, le this devient window...
          sq.txt = datasSection.txt
          sq.nbLatex = Number(datasSection.nbLatex ?? 0) // Le nombre de paramètres LaTeX dans le texte
          // ?? retourne l’opérande de gauche si !== undefined && !== null, celle de droite sinon
          const listeBoutons = []
          // boutons avec propriétés à true par défaut
          const tabBoutons1 = ['puissance', 'fraction']
          ;['btnPuis', 'btnFrac'].forEach((p, i) => {
            const b = Boolean(datasSection[p] ?? true)
            if (b) listeBoutons.push(tabBoutons1[i])
          })
          // boutons avec false par défaut
          const tabBoutons2 = ['pi', 'racine', 'exp', 'ln', 'sin', 'cos', 'tan', 'abs']
          ;['btnPi', 'btnRac', 'btnExp', 'btnLn', 'btnSin', 'btnCos', 'btnTan', 'btnAbs'].forEach((p, i) => {
            const b = Boolean(datasSection[p] ?? false)
            if (b) listeBoutons.push(tabBoutons2[i])
          })
          sq.listeBoutons = listeBoutons
          sq.boutonsMathQuill = listeBoutons.length !== 0
          sq.consigne1 = datasSection.consigne1 ?? ''
          sq.consigne2 = datasSection.consigne2 ?? ''
          sq.consigne3 = datasSection.consigne3 ?? ''
          sq.consigne4 = datasSection.consigne4 ?? ''
          sq.titre = datasSection.titre ?? ''
          sq.width = Number(datasSection.width ?? 700)
          sq.height = Number(datasSection.height ?? 900)
          sq.param = datasSection.param ?? '' // Chaine contenant les paramètres autorisés
          sq.charset = datasSection.charset ?? ''
          initMtg()
        }).catch(error => {
          console.error(error)
          j3pShowError('Impossible de charger les données de cette exercice')
        })
        // return;
      } else {
        j3pEmpty('correction')
        cacheSolution()
        sq.numEssai = 1
        sq.mtgAppLecteur.removeDoc('mtg32svg')
        sq.mtgAppLecteur.addDoc('mtg32svg', sq.txt, true)

        if (sq.aleat) {
          for (let i = 1; i <= sq.nbParamAleat; i++) {
            sq.mtgAppLecteur.giveFormula2('mtg32svg', 'r' + i, sq['tab' + i][this.questionCourante - 1])
          }
        }

        if (sq.param) {
          for (const char of 'abcdefghjklmnpqr') {
            if (sq.param.includes(char)) {
              const par = parcours.donneesSection[char]
              if (par !== 'random') sq.mtgAppLecteur.giveFormula2('mtg32svg', char, par)
            }
          }
        }

        sq.mtgAppLecteur.calculate('mtg32svg', true) // true pour que les calculs alèatoires soient réinitialisés
        // On donne à tous les paramètres de la figure de calcul les mêmes valeurs que celles qu’ils ont
        // dans la figure du fichier annexe
        if (sq.param) {
          for (const char of 'abcdefghjklmnpqr') {
            if (sq.param.includes(char)) {
              sq.listeCalc.giveFormula2(char, sq.mtgAppLecteur.valueOf('mtg32svg', char))
            }
          }
        }
        // On informe la liste chargée des calculs des équations du système qui sont dans trois
        // fonctions de 3 variables avec égalité nommées eq1, eq2 et eq3
        for (let i = 1; i < 4; i++) {
          const nomcalc = 'eq' + i
          sq.listeCalc.giveFormula2(nomcalc, sq.mtgAppLecteur.getFormula('mtg32svg', nomcalc))
        }
        sq.listeCalc.calculateNG(false)

        j3pEmpty('divEnonce')
        j3pEmpty('formules')
        j3pEmpty('divInfo')
        j3pEmpty('editeur')
        // La consigne peut contenir jusqu'à 4 paramètres a, b,c et d récupérés comme affichages LaTeX de la figure.
        const nbLatex = parseInt(sq.nbLatex)
        // La consigne peut contenir jusqu'à 4 paramètres a, b,c et d récupérés comme affichages LaTeX de la figure.
        const t = 'abcd'
        const param = {}
        for (let k = 0; k < nbLatex; k++) {
          const code = sq.listeCalc.getLatexCode(k)
          param[t.charAt(k)] = code
        }

        const st = sq.consigne1 + (sq.simplifier ? sq.consigne3 : sq.consigne2) + sq.consigne4
        afficheMathliveDans('divEnonce', 'texte', st, param)
        sq.equation1 = sq.listeCalc.getLatexCode(0)
        sq.equation2 = sq.listeCalc.getLatexCode(1)
        sq.equation3 = sq.listeCalc.getLatexCode(2)
        sq.debFormulaire = '\\left\\{ \\begin{array}{l} ' + sq.equation1 + '\\\\' + sq.equation2 + '\\\\' + sq.equation3 + '\\end{array} \\right.'
        sq.finFormulaire = '\\left\\{ \\begin{array}{l} \\editable{} \\\\ \\editable{} \\\\ \\editable{} \\end{array} \\right.'
        sq.formulaire = sq.debFormulaire + '\\Leftrightarrow ' + sq.finFormulaire
        afficheMathliveDans('editeur', 'expression', sq.formulaire, {
          charset: sq.charset,
          listeBoutons: sq.listeBoutons
        })
        if (sq.charset !== '') {
          mathliveRestrict('expressionPhBlock1', sq.charset)
        }
        j3pElement('expressionPhBlock1').onkeyup = function () {
          onkeyup.call()
        }
        j3pElement('boutonrecopierEd1').style.display = 'inline-block'
        j3pElement('boutonrecopierEd2').style.display = 'inline-block'
        j3pElement('boutonrecopierEd3').style.display = 'inline-block'

        sq.mtgAppLecteur.display('mtg32svg')
        // Pour revenir en haut de page
        try {
          window.location.hash = 'MepMG'
          j3pElement('MepMG').scrollIntoView()
        } catch (e) {}
        montreEditeurs(true)
        afficheNombreEssaisRestants()
        j3pElement('divInfo').style.display = 'block'
        this.finEnonce()
      }

      break // case "enonce":

    case 'correction': {
      let bilanReponse = ''
      let simplificationPossible = false
      // On regarde d’abord si un des éditeurs a un contenu vide ou incorrect
      if (validationEditeurs()) {
        validation()
        if (sq.simplifier) {
          if (sq.resolu) { bilanReponse = 'resolu' } else {
            if (sq.presqueresolu) {
              bilanReponse = 'presqueResolu'
              simplificationPossible = true
            } else {
              if (sq.exact) bilanReponse = 'exact'
              else bilanReponse = 'faux'
            }
          }
        } else {
          if (sq.resolu || sq.presqueresolu) {
            bilanReponse = 'resolu'
            if (!sq.resolu) simplificationPossible = true
          } else { bilanReponse = 'faux' }
        }
      } else {
        bilanReponse = 'incorrect'
      }
      // A cause de la limite de temps :
      if (this.isElapsed) { // limite de temps
        this._stopTimer()
        j3pElement('correction').style.color = this.styles.cfaux
        j3pElement('divInfo').style.display = 'none'
        j3pElement('boutonrecopierEd1').style.display = 'none'
        j3pElement('boutonrecopierEd2').style.display = 'none'
        montreEditeurs(false)
        sq.mtgAppLecteur.executeMacro('mtg32svg', 'solution')
        afficheSolution(false)
        j3pElement('correction').innerHTML = tempsDepasse
        j3pElement('correction').innerHTML += '<br>La solution se trouve ci-contre.'
        this.etat = 'navigation'
        this.sectionCourante()
      } else {
        if (bilanReponse === 'incorrect') {
          j3pElement('correction').style.color = this.styles.cfaux
          if (sq.signeEgalOublie) j3pElement('correction').innerHTML = 'Ne pas oublier d’entrer les réponses avec des égalités.'
          else j3pElement('correction').innerHTML = 'Réponse incorrecte'
        } else {
          // Une réponse a été saisie
          sq.numEssai++
          if (sq.refuse) {
            j3pElement('correction').style.color = this.styles.cfaux
            j3pElement('correction').innerHTML = 'Calcul refusé (système non linéaire)'
            afficheReponse('refuse')
            videEditeurs()
            focusEditeur(1)
          } else {
            // Bonne réponse
            if (bilanReponse === 'resolu') {
              this.score++
              this._stopTimer()
              sq.mtgAppLecteur.setActive('mtg32svg', false)
              afficheReponse('resolu')
              j3pElement('boutonrecopierEd1').style.display = 'none'
              j3pElement('boutonrecopierEd2').style.display = 'none'
              j3pElement('boutonrecopierEd3').style.display = 'none'
              j3pElement('correction').style.color = this.styles.cbien
              if (simplificationPossible) j3pElement('correction').innerHTML = 'C’est bien mais on pouvait  simplifier la réponse' + '<br>Voir la solution ci-contre'
              else j3pElement('correction').innerHTML = cBien + '<br>Voir la solution ci-contre'
              j3pElement('divInfo').style.display = 'none'
              sq.mtgAppLecteur.executeMacro('mtg32svg', 'solution')
              afficheSolution(true)
              montreEditeurs(false)
              this.etat = 'navigation'
              this.sectionCourante()
            } else {
              if (bilanReponse === 'presqueResolu') {
                j3pElement('correction').style.color =
                  (sq <= sq.nbEssais) ? '#0000FF' : this.styles.cbien
                j3pElement('correction').innerHTML = 'La solution est bonne mais pas écrite sous la forme demandée.'
              } else {
                if (bilanReponse === 'exact') {
                  j3pElement('correction').style.color =
                    (sq.numEssai <= sq.nbEssais) ? '#0000FF' : this.styles.cfaux
                  j3pElement('correction').innerHTML = 'Résolution à poursuivre.'
                } else {
                  j3pElement('correction').style.color = this.styles.cfaux
                  j3pElement('correction').innerHTML = cFaux
                }
              }
              j3pEmpty('divInfo')
              if (sq.numEssai <= sq.nbEssais) {
                afficheNombreEssaisRestants()
                videEditeurs()
                // afficheReponse((bilanReponse === 'exactPasFini') ? 'exact' : ((bilanReponse === 'presqueResolu') ? 'presqueResolu' : 'faux'))
                afficheReponse(bilanReponse)
                // S’il y a plus de deux chances,on appelle à nouveau le bouton Valider

                j3pElement('correction').innerHTML += '<br>' + '<br>' + essaieEncore
                focusEditeur(1)
                // sq.mtgAppLecteur.setActive("mtg32svg", true);
              } else {
                // Erreur au nème essai
                this._stopTimer()
                sq.mtgAppLecteur.setActive('mtg32svg', false)
                j3pElement('boutonrecopierEd1').style.display = 'none'
                j3pElement('boutonrecopierEd2').style.display = 'none'
                j3pElement('boutonrecopierEd3').style.display = 'none'
                j3pElement('divInfo').style.display = 'none'
                sq.mtgAppLecteur.executeMacro('mtg32svg', 'solution')
                afficheSolution(false)
                montreEditeurs(false)
                afficheReponse('faux')
                j3pElement('correction').innerHTML += '<br><br>La solution se trouve ci-contre.'
                this.etat = 'navigation'
                this.sectionCourante()
              }
            }
          }
        }
      }
      // Obligatoire
      this.finCorrection()
      break // case "correction":
    }

    case 'navigation':

      if (this.sectionTerminee()) {
        // On détermine la phrase d’état renvoyée par la section
        this.parcours.pe = this.score / this.donneesSection.nbitems
        this.afficheBoutonSectionSuivante()
        this.focus('sectioncontinuer')
      } else {
        this.etat = 'enonce'
        this.afficheBoutonSuite()
        this.focus('boutoncontinuer')
      }
      // Obligatoire
      this.finNavigation()

      break // case "navigation":
  }
}
