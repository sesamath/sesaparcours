import $ from 'jquery'
import { MathfieldElement, renderMathInDocument } from 'mathlive'

import { j3pAddContent, j3pAddElt, j3pAjouteBouton, j3pDetruit, j3pDiv, j3pElement, j3pEmpty, j3pImporteAnnexe, j3pPaletteMathquill, j3pRandomTab, j3pShowError } from 'src/legacy/core/functions'
import { unLatexify } from 'src/lib/mathquill/functions'
import { pgcd } from 'src/lib/utils/number'
import { j3pCreeFenetres, j3pToggleFenetres } from 'src/legacy/core/functionsJqDialog'
import { getMtgApp } from 'src/lib/outils/mathgraph'
import { j3pCreeSVG } from 'src/legacy/core/functionsSvg'
import { setMathliveCss } from 'src/lib/outils/mathlive/mathlivecss'
import { afficheMathliveDans, getMathliveValue } from 'src/lib/outils/mathlive/display'
import { cleanLatexForMl, mathliveRestrict } from 'src/lib/outils/mathlive/utils'
import { resetKeyboardPosition } from 'src/lib/widgets/mlVirtualKeyboard/VirtualKeyboard'
// import { addConsoleOnScreen } from 'src/lib/utils/debug'
import textesGeneriques from 'src/lib/core/textes'
import { focusIfExists } from 'src/lib/utils/dom/main'

import { setFocusById } from 'src/legacy/sections/squelettes/helpersMtg'

const { cBien, tempsDepasse } = textesGeneriques

/*
    Yves Biton
    Octobre 2018. Revus en 2023 pour adaptation à Mathlive

    http://localhost:8081/?graphe=[1,"squelettemtg32_Somme_Frac",[{pe:">=0",nn:"fin",conclusion:"fin"},{f:"Fact_ax_Carre_Moins_cCarre_Ex1"}]];
    squelettemtg32_Somme_Frac
    Squelette demandant d’additionner deux fractions a/b + c/d et de donner le résultat sous une forme simplifiée.
    On peut éventuellement spécifier les paramètres  a, b,c et d mais a et b ainsi que ce et de doivent être premiers entre eux.

    Si nbrepetitions > 1, on donne pour i compris entre 1 et npParamAleat
    au calcul de la figure nommé "r"+i (par exemple r1 et r2 si nbParamAleat vaut 2) des valeurs comprises entre 0 et "nbcas"+i - 1
    toutes différentes (sauf si nbrepetitions > b=nbcas auquel cas au bout de nbcas répétitions il y aura nécessairement une valeur de r déjà prise.
    A utiliser avec les fichiers annexes College_Somme_Frac.js ou College_Dif8frac.js

*/

// on définit le dossier des fonts mathlive (il faut le faire très tôt,
// ici c’est bon avec pnpm start mais trop tard pour un build (cf src/lib/player/index.js de la branche draftPlayer)
if (MathfieldElement) { // undefined dans les tests avec happy-dom
  MathfieldElement.fontsDirectory = '/static/mathlive/fonts'
  MathfieldElement.soundsDirectory = '/static/mathlive/sounds'
}

export const params = {
  outils: ['calculatrice'],
  /**
   * Ce qui est paramétrable dans l’éditeur de graphe
   * @type {LegacySectionParamsParametres}
   */
  parametres: [
    ['nbrepetitions', 1, 'entier', 'Nombre de répétitions de l’exercice'],
    ['limite', 0, 'entier', 'Temps disponible par répétition, en secondes'],
    ['nbEssais', 6, 'entier', 'Nombre d’essais maximum autorisé'],
    ['premiereEtapeImposee', true, 'boolean', 'true si la première étape du calcul est imposée\navec le dénominateur fourni'],
    ['calculatrice', true, 'boolean', 'true pour avoir une calculatrice disponible'],
    ['boutonRecopie', true, 'boolean', 'true pour avoir un bouton de recopie de la réponse précédente'],
    ['niveau', 1, 'entier', 'Niveau (1 ou 2). Le niveau 1 donne des dénominateurs plus petits'],
    ['simplifier', false, 'boolean', 'true si on exige que le résultat soit une fraction irréductible'],
    ['aideDenominateur', false, 'boolean', 'true si on veut que le dénominateur de la réduction soit fourni\nNon pris en compte si premiereEtapeImposee est à true'],
    ['b', 'random', 'string', 'Calcul de a/b + c /d : Valeur de b. Peut être un nombre choisi parmi une liste : Par exemple [2;5;7]'],
    ['d', 'random', 'string', 'Calcul de a/b + c /d : Valeur de d (Peut dépendre de b : Par exemple 2*b ou être un nombre choisi parmi une liste : Par exemple [2;5;7]'],
    ['a', 'random', 'string', 'Calcul de a/b + c /d : Valeur de a.\nPeut être un nombre choisi au hasard dans une liste : Par exemple [2;5;7]\nSi a/b n’est pas irréductible, elle sera réduite'],
    ['c', 'random', 'string', 'Calcul de a/b + c /d : Valeur de c.\nPeut être un nombre choisi au hasard dans une liste : Par exemple [2;5;7]\nSi c/d n’est pas irréductible, elle sera réduite'],
    ['ex', 'College_Somme_Frac', 'string', 'Nom du fichier annexe (xxx => squelettes-mtg32/xxx.js et xxx/yyy => squelettes-mtg32-xxx/yyy.js)']
  ]
}

const textes = {
  txt1: 'Il reste ',
  txt2: 'Il reste une étape',
  txt3: 'C’est juste et on peut encore simplifier.',
  txt4: 'C’est juste et on pouvait simplifier le résultat par ',
  txt5: '<br>La solution se trouve ci-contre.',
  txt6: 'Le résultat est faux',
  txt7: '<br> mais on pouvait simplifier le résultat.',
  txt8: 'On pouvait simplifier par ',
  txt9: '<br>On peut encore simplifier le résutat.',
  txt10: '<br/>Première étape refusée : ',
  txt11: ' : Pas écrit sous la forme demandée',
  etape1: '<br>Tu dois d’abord compléter la formule ci-dessous.',
  fin: ' maximum pour finir le calcul.',
  conseil: '<br> Conseil : Réduire au même dénominateur $£b$.',
  etapes: ' étapes',
  pasfini: 'Le calcul n’est pas fini.',
  incorrect: 'Réponse incorrecte',
  faux: 'C’est faux.',
  pasfini2: 'Le calcul n’a pas été fini.'
}

const inputId = 'expressioninputmq1'

/**
 * section SommeFrac
 * @this {Parcours}
 */
export default function main () {
  const parcours = this
  const sq = this.storage

  // Fonction appelée quand le paramètre st commence par un crochet [ et contient une liste de nombres et renvoyant un
  // des nombres choisi au hasard.
  function nombreAleat (st) {
    // On retire les crochets de début et de fin
    st = st.replace(/\s*\[\s*/g, '').replace(/\s*]\s*/g, '').replace(/,/g, ';')
    const t = st.split(';')
    const len = t.length
    const freq = Math.round(100 / len) / 100
    let somme = 0
    const tab1 = []
    const tab2 = []
    for (let i = 0; i < len; i++) {
      tab1.push(Number(t[i]))
      if (i < len - 1) {
        somme += freq
        tab2.push(freq)
      } else tab2.push(Math.round((1 - somme) * 100) / 100)
    }
    return j3pRandomTab(tab1, tab2).toString()
  }

  // listener mis sur les input mathquill pour détecter la touche entrée ou les saisies
  function onkeyup (ev) {
    // (this est bien l’élément html concerné)
    let rep, valide
    let unEditeurAEuFocus = false
    const id = this.getAttribute('id')
    if (sq.marked[id]) demarqueEditeurPourErreur(id)
    if (sq.premiereEtapeImposee && !sq.premierEtapeResolue) {
      if (ev.keyCode === 13) {
        let chinit = sq.latexEtape1
        let indedit = 0
        valide = true
        while (chinit.indexOf('\\editable{') !== -1) {
          indedit++
          rep = getPromptValue('expressionPhBlock1ph' + indedit)
          if (rep === '') {
            valide = false
            marqueEditeurPourErreur('expressionPhBlock1ph' + indedit)
            j3pElement('correction').style.color = parcours.styles.cfaux
            j3pElement('correction').innerHTML = textes.incorrect
            if (!unEditeurAEuFocus) setFocusById('expressionPhBlock1ph' + indedit)
            unEditeurAEuFocus = true
            break
          }
          chinit = chinit.replace('\\editable{}', '{' + rep + '}')
        }
        if (valide) validation(parcours)
      }
    } else {
      rep = getMathliveValue(inputId)
      // @todo ajouter un petit commentaire car ce qui suit n’est pas très clair
      if (!sq.simplifier) {
        if ((rep === '') && sq.nonSimplifie) parcours.afficheBoutonValider()
        else parcours.cacheBoutonValider()
      }
      if (ev.keyCode === 13) {
        const chcalcul = traiteMathlive(rep)
        valide = sq.mtgAppLecteur.syntaxValidation('mtg32svg', 'rep', chcalcul, true)
        if (valide) {
          validation(parcours)
        } else {
          marqueEditeurPourErreur(id)
          j3pElement('correction').style.color = parcours.styles.cfaux
          j3pElement('correction').innerHTML = textes.incorrect
          focusIfExists(inputId)
        }
      }
    }
  }

  /**
   * Fonction extrayant d’un affichage LaTeX de la figure les lignes de texte et les transformant
   * en une chaîne affichable par MathQuill
   * @param tagLatex : le tag de l’affichage LaTeX de la figure
   * @return {string}
   */
  function extraitDeLatexPourMathlive (tagLatex) {
    const list = sq.mtgAppLecteur.getList('mtg32svg')
    const latex = list.getByTag(tagLatex)
    if (latex === null || latex.className !== 'CLatex') return ''
    return cleanLatexForMl(latex.chaineLatex)
  }

  function afficheNombreEssaisRestants () {
    j3pEmpty('info')
    const nbe = sq.nbEssais - sq.nbexp
    if (nbe === 1) {
      afficheMathliveDans('info', 'texteinfo', textes.txt2 + textes.fin,
        { style: { color: '#7F007F' } })
    } else {
      afficheMathliveDans('info', 'texteinfo', textes.txt1 + nbe + textes.etapes + textes.fin,
        { style: { color: '#7F007F' } })
    }
  }

  /**
   * Fonction affichant la solution s’il existe un affichage LaTeX de tag solution
   * @param{boolean} bexact true si l’élève a entré la bonne réponse, false sinon
   */
  function afficheSolution (bexact) {
    $('#divSolution').css('display', 'block').html('')
    const ch = extraitDeLatexPourMathlive('solution')
    if (ch !== '') {
      afficheMathliveDans('divSolution', 'solution', ch, { style: { color: bexact ? 'green' : 'blue' } })
    }
  }

  function cacheSolution () {
    $('#divSolution').css('display', 'none').html('')
  }

  /**
   * Ajoutera onkeyup à l’élément dans 100ms (pour ne pas se déclencher dès l’affichage de l’énoncé en cas de touche entrée qui déclenche suite)
   * @private
   * @param {string} id
   */
  function addKeyupListener (id) {
    // on attend 100ms avant d’écouter, pour "rater" un éventuel événement keyup venant de la répétition précédente
    // (car un appui sur entrée lors de la correction déclenche un événement click sur le bouton suite (ça c’est le navigateur qui le gère)
    // et on récupère l’événement clavier juste après l’affichage de l’énoncé, pas terrible d’afficher "réponse incorrecte" dès l’affichage de l’énoncé)
    setTimeout(function () { j3pElement(id).onkeyup = onkeyup }, 100)
  }

  function marqueEditeurPourErreur (id) {
    // Il faut regarder si l’erreur a été rencontrée dans un placeHolder ou dans un éditeur Mathlive classique
    let editor // Peut être aussi bien un éditeur de texte qu’un éditeur Mathlive
    if (id.includes('ph')) {
      const ind = id.indexOf('ph')
      const idmf = id.substring(0, ind) // L’id de l’éditeur MathLive contenant le placeHolder
      editor = j3pElement(idmf)
      sq.marked[idmf] = true
      editor.setPromptState(id, 'incorrect')
    } else {
      editor = j3pElement(id)
      sq.marked[id] = true
      editor.style.backgroundColor = '#FF9999'
    }
    // On donne à l’éditeur une fonction demarquePourError qui lui permettra de se démarquer quand
    // on utilise le clavier virtuel pour le modifier
    editor.demarqueErreur = () => {
      demarqueEditeurPourErreur(editor.id)
    }
  }

  function demarqueEditeurPourErreur (id) {
    // if ($.isEmptyObject(sq.marked)) return
    if (JSON.stringify(sq.marked) === '{}') return // On teste si l’objet est vide
    sq.marked[id] = false // Par précaution si appel via bouton recopier réponse trop tôt
    // On regarde si l’éditeur où a été trouvée l’erreur est un placeHolder
    const editor = j3pElement(id) // Peut être aussi bien un éditeur Mathlive qu’un éditeur de texte
    if (editor.classList.contains('PhBlock')) {
      editor.getPrompts().forEach((id) => {
        editor.setPromptState(id, 'undefined')
      })
    } else {
      editor.style.backgroundColor = 'white'
    }
  }

  function getPromptValue (id) {
    // On n’utilise pas de forEach qui ne permete pas de sortir de la boucle
    const sel = j3pElement('expression').querySelectorAll('math-field')
    for (const value of sel.entries()) {
      const mf = value[1]
      const prompts = mf.getPrompts()
      const ind = prompts.indexOf(id)
      if (ind !== -1) {
        return mf.getPromptValue(id, 'latex-unstyled').replace(/\\placeholder{}/g, '')
      }
    }
    console.error('id prompt invalide : ' + id)
    return ''
  }

  function traiteMathlive (ch) {
    // Mathlive ne met pas d’accolades pour les fractions dont le numérateur et le dénominateur sont des entiers
    ch = ch.replace(/\\frac(\d)(\d)/g, '\\frac{$1' + '}{' + '$2}')
    // Ni pour les racines carrées d’un entier
    ch = ch.replace(/\\sqrt(\d)/g, '\\sqrt{$1}')
    // Pour les puissances dont l’exposant est un chiffre, Mathlive ne met pas d’accolades
    ch = ch.replace(/\^([^{])/g, '^{$1}')
    ch = unLatexify(ch)
    // mathlive peut mettre des {} vides
    ch = ch.replace(/\\{\\}/g, '')
    ch = ch.replace(/\^(\d)(\d)/g, '^$1*$2') // On peut avoir par exemple 2^35^4 pour 2^3*5^4
    return sq.mtgAppLecteur.addImplicitMult('mtg32svg', ch)// Traitement des multiplications implicites
  }

  function nombreEditables (ch) {
    let nbedit = 0
    while (ch.indexOf('\\editable{') !== -1) {
      nbedit++
      ch = ch.replace('\\editable{}', '')
    }
    return nbedit
  }

  function videEditeursEtape1 () {
    let chinit = sq.latexEtape1
    let indedit = 0
    while (chinit.indexOf('\\editable{') !== -1) {
      indedit++
      const $elt = j3pElement('expressionPhBlock1')
      $elt.setPromptValue('expressionPhBlock1ph' + indedit, '')
      chinit = chinit.replace('\\editable{}', '')
    }
    // $('#expressioninputmqef1').focus()
    // j3pFocus('expressioninputmqef1')
    setFocusById('expressionPhBlock1ph1')
  }

  // Le deuxième paramètre est à false seulement quand elle est appelée depuis l’étape de correction
  // Sinon la fonction est appelée par l’éditeur quand l’utilisateur appuie sur la touche entrée avec un seul paramètre
  function validation (parcours) {
    let idrep, st, rep
    sq.resolu = false
    /*
    if (sq.boutonRecopie && ((sq.nbexp === 0) || (sq.premiereEtapeImposee && sq.premierEtapeResolue))) {
      j3pElement('boutonrecopier').style.display = 'block'
    }
     */
    let indedit = 0
    if (sq.premiereEtapeImposee && !sq.premierEtapeResolue) {
      rep = sq.latexEtape1
      while (rep.indexOf('\\editable{}') !== -1) {
        indedit++
        rep = rep.replace('\\editable{}', '{' + getPromptValue('expressionPhBlock1ph' + indedit) + '}')
      }
    } else rep = getMathliveValue(inputId)
    sq.rep = rep
    const chcalcul = traiteMathlive(sq.rep)
    sq.mtgAppLecteur.giveFormula2('mtg32svg', 'rep', chcalcul)
    j3pEmpty('correction')
    sq.mtgAppLecteur.calculate('mtg32svg', false)
    sq.reponse = sq.mtgAppLecteur.valueOf('mtg32svg', 'reponse')
    if (sq.premiereEtapeImposee && !sq.premierEtapeResolue) {
      // On regarde si la réponse de l’élève correspond à la première réponse imposée
      const correct = sq.mtgAppLecteur.valueOf('mtg32svg', 'resoluEtape1')
      idrep = 'exp' + sq.nbexp
      sq.nbexp++
      if (correct) {
        sq.premierEtapeResolue = true
        afficheMathliveDans('formules', idrep, '\n$' + sq.aCalculer + ' = ' + sq.rep + '$', {
          style: {
            color: parcours.styles.cbien
          }
        })
        j3pDetruit('expression')
        afficheMathliveDans('editeur', 'expression', '$' + sq.aCalculer + '$ = &1&', {
          charset: sq.charset,
          listeBoutons: ['fraction']
        })
        if (sq.charset !== '') {
          mathliveRestrict(inputId, sq.charset)
        }
        addKeyupListener(inputId)
        const $btn = $('#boutonsmathquill')
        $btn.css('display', 'inline-block')
        $btn.css('padding-top', '10px') // Pour laisser un peu de place au-dessus des boutons
        const liste = ['fraction']
        j3pPaletteMathquill('boutonsmathquill', inputId, { liste })
        focusIfExists(inputId)
        if (sq.boutonRecopie) j3pElement('boutonrecopier').style.display = 'block'
      } else {
        st = textes.txt10 + '$' + sq.aCalculer + '$'
        st += (sq.reponse === 1 || sq.reponse === 2)
          ? ('=$' + sq.rep + '$' + textes.txt11)
          : ('$\\ne ' + sq.rep + '$')
        afficheMathliveDans('formules', idrep, st, {
          style: {
            color: parcours.styles.cfaux
          }
        })
        videEditeursEtape1()
      }
    } else {
      if (!sq.premiereEtapeImposee && (sq.boutonRecopie && (sq.nbexp === 0))) j3pElement('boutonrecopier').style.display = 'block'
      sq.nonSimplifie = false
      if (sq.reponse === 2) {
        // Si on ne demande pas de simplifier on accepte toute fraction du moment que le résultat est bon
        // Pour cela il faut que le nombre de divisions soit au plus de 1
        // Il faut aussi tester qu’il n’y a pas de point décimal car un élève a pu copier-coller depuis la calculatrice
        if ((chcalcul.indexOf('+') === -1 && chcalcul.indexOf('-') === -1 &&
            chcalcul.indexOf('*') === -1 && chcalcul.indexOf('.') === -1) &&
          ((chcalcul.indexOf('/') === -1) || (chcalcul.lastIndexOf('/') === chcalcul.indexOf('/')))) {
          // sq.reponse = 1;
          const chc = chcalcul.replace(/\(/g, '').replace(/\)/g, '')
          st = chc.split('/')
          sq.simpPar = pgcd(Number(st[0]), Number(st[1]))
          sq.nonSimplifie = true
        }
      }
      idrep = 'exp' + sq.nbexp
      sq.nbexp++
      const chdeb = sq.nbexp === 1 ? '' : '<br>'
      switch (sq.reponse) {
        case 0 :
        case -1 : // Au cas où l’élève a répondu une réponse qui n’existe pas comme 1/0
          if (sq.nbexp < sq.nbEssais) {
            afficheMathliveDans('formules', idrep, chdeb + '$' + sq.aCalculer + ' \\ne ' + sq.rep + '$', {
              style: {
                color: parcours.styles.cfaux
              }
            })
            j3pElement('correction').style.color = parcours.styles.cfaux
            j3pElement('correction').innerHTML = textes.faux
            resetKeyboardPosition()
            if (sq.reponse === -1) j3pAddContent('correction', '\nLe résultat n’existe pas')
          }
          break
        case 2 :
          if (sq.nbexp < sq.nbEssais) {
            if (!sq.simplifier && sq.nonSimplifie) parcours.afficheBoutonValider()
            else parcours.cacheBoutonValider()
            afficheMathliveDans('formules', idrep, chdeb + '$' + sq.aCalculer + ' = ' + sq.rep + '$', {
              style: {
                color: parcours.styles.colorCorrection
              }
            })
            j3pElement('correction').style.color = parcours.styles.cbien
            if (sq.simplifier) {
              let s = textes.pasfini
              if (sq.nonSimplifie) s += textes.txt9
              j3pElement('correction').innerHTML = s
            } else {
              j3pElement('correction').innerHTML = sq.nonSimplifie ? textes.txt3 : textes.pasfini
            }
          }
          break
        case 1: // Le résultat est bon et écrit sous forme réduite
          // On vide le contenu de l’éditeur MathQuill
          j3pElement(inputId).value = ''
          focusIfExists(inputId)
          parcours.etat = 'correction'
          sq.resolu = true
          parcours.sectionCourante()
          return
      }
      // Si on arrive ici la réponse est soit fausse soit incomplète
      // On vide le contenu de l’éditeur MathQuill
      j3pElement(inputId).value = ''
      focusIfExists(inputId)
    }
    // Si le nombre maximum d’essais est atteint on passe en mode correction
    if (sq.nbexp >= sq.nbEssais) {
      sq.mtgAppLecteur.setActive('mtg32svg', false)
      $('#editeur').css('display', 'none')
      $('#boutonsmathquill').css('display', 'none')
      parcours.etat = 'correction'
      sq.resolu = false
      parcours.sectionCourante()
    } else {
      afficheNombreEssaisRestants()
    }
  }

  function recopierReponse () {
    demarqueEditeurPourErreur(inputId)
    const mf = j3pElement(inputId)
    if (!mf) return
    mf.value = sq.rep
    focusIfExists(inputId)
  }

  function boutonfen () {
    j3pToggleFenetres('Calculatrice')
  }

  // Fonction renvoyant le k ième nombre premier avec n à partir de 1 (les indices pour k commencent à 0)
  function nbprem (n, k) {
    let nb = 1
    let i = -1
    while (i < k) {
      if (pgcd(nb, n) === 1) i++
      nb++
    }
    return nb - 1
  }

  function initMtg () {
    setMathliveCss('MepMG')
    const mtgOptions = {
      // player only, sans figure initiale ni svg, on gère ça dans initMtg et modif_fig
      loadCoreWithMathJax: true
    }
    getMtgApp('mtg32svg', {}, mtgOptions).then((mtgAppLecteur) => {
      sq.mtgAppLecteur = mtgAppLecteur
      initDom()
      let code, i, j, k, ar, tir, nb, vald, valb
      sq.mtgAppLecteur.removeAllDoc()
      sq.mtgAppLecteur.addDoc('mtg32svg', sq.txt, true)
      sq.mtgAppLecteur.calculate('mtg32svg', false)
      // var nbvar = sq.mtgAppLecteur.valueOf("mtg32svg","nbvar");
      const nbvar = 4 // 4 paramètres aléatoires
      if (parcours.donneesSection.niveau === 1) {
        vald = [2, 3, 4, 5, 6, 8, 9, 10]
        valb = [2, 3, 4, 5, 6, 8, 9, 10]
      } else {
        vald = [4, 5, 6, 7, 8, 9, 10, 12, 14, 15, 20, 30]
        valb = [4, 5, 6, 7, 8, 9, 10, 12, 14, 15, 20, 30]
      }
      const nbcas = [valb.length, vald.length - 1, 5, 5]
      const nbrep = parcours.donneesSection.nbrepetitions
      for (i = 1; i <= nbvar; i++) {
        nb = Math.max(nbrep, nbcas[i - 1])
        ar = []
        for (j = 0; j < nb; j++) ar.push(j % nbcas[i - 1])
        sq['tab' + i] = []
        for (k = 0; k < nbrep; k++) {
          tir = Math.floor(Math.random() * ar.length)
          sq['tab' + i].push(ar[tir])
          ar.splice(tir, 1)
        }
      }
      let b, d, a, c
      // On tire un premier dénominateur au hasard pour b
      const ind0 = sq.tab1[0]
      const paramb = parcours.donneesSection.b.trim()
      if (paramb === 'random') b = valb[ind0]
      else {
        if (paramb.indexOf('[') === 0) b = nombreAleat(paramb)
        else b = paramb
      }
      sq.mtgAppLecteur.giveFormula2('mtg32svg', 'b', b)

      // Comme b peut contenir une formule utilisant rand(0) il faut recalculer la figure.
      sq.mtgAppLecteur.calculate('mtg32svg', false)
      b = sq.mtgAppLecteur.valueOf('mtg32svg', 'b')
      sq.mtgAppLecteur.giveFormula2('mtg32svg', 'b', String(b))

      // On retire la valeur de b pour choisir la valeur de d
      vald.splice(ind0, 1)
      const ind1 = sq.tab2[0]
      const paramd = parcours.donneesSection.d.trim()
      if (paramd === 'random') d = vald[ind1]
      else {
        if (paramd.indexOf('[') === 0) d = nombreAleat(paramd)
        else d = paramd
      }
      sq.mtgAppLecteur.giveFormula2('mtg32svg', 'd', d)

      // Comme d peut contenir une formule utilisant rand(0) il faut recalculer la figure.
      sq.mtgAppLecteur.calculate('mtg32svg', false)
      d = sq.mtgAppLecteur.valueOf('mtg32svg', 'd')
      sq.mtgAppLecteur.giveFormula2('mtg32svg', 'd', String(d))

      // On affecte à a un nombre premier avec b choisi à partir de 1 (le sq.tab2[0] ième)
      const parama = parcours.donneesSection.a.trim()
      if (parama === 'random') a = nbprem(b, sq.tab3[0])
      else {
        if (parama.indexOf('[') === 0) a = nombreAleat(parama)
        else a = parama
      }
      sq.mtgAppLecteur.giveFormula2('mtg32svg', 'a', a)

      // Idem pour c
      const paramc = parcours.donneesSection.c
      if (paramc === 'random') c = nbprem(d, sq.tab4[0])
      else {
        if (paramc.indexOf('[') === 0) c = nombreAleat(paramc)
        else c = paramc
      }
      sq.mtgAppLecteur.giveFormula2('mtg32svg', 'c', c)
      sq.mtgAppLecteur.calculateAndDisplayAll(true)
      if (sq.mtgAppLecteur.valueOf('mtg32svg', 'begald') === 1) sq.premiereEtapeImposee = false
      if (parcours.donneesSection.premiereEtapeImposee) sq.premierEtapeResolue = false

      sq.latexEtape1 = sq.mtgAppLecteur.getLatexCode('mtg32svg', 2)
      if (sq.premiereEtapeImposee) sq.nombreEditables = nombreEditables(sq.latexEtape1)

      const nbParam = sq.aideDenominateur ? 2 : 1
      const t = ['a', 'b']
      const param = {}
      for (i = 0; i < nbParam; i++) {
        code = sq.mtgAppLecteur.getLatexCode('mtg32svg', i)
        if (i === 0) sq.aCalculer = code
        param[t[i]] = code
      }
      sq.param = param
      let st = (sq.simplifier ? sq.consigne1 : sq.consigne2) + sq.consigne3
      if (sq.aideDenominateur) st += textes.conseil
      if (sq.premiereEtapeImposee) st += textes.etape1
      afficheMathliveDans('enonce', 'texteenonce', st, param)
      // debug
      // addConsoleOnScreen(parcours.zonesElts.MG)

      // Les 3 lignes suivantes pour que sq.mtgAppLecteur soit prêt quand onkeyup est appelé
      if (sq.premiereEtapeImposee) {
        afficheMathliveDans('editeur', 'expression', '$' + sq.aCalculer + '=' + sq.latexEtape1 + '$', {
          charset: sq.charset
        })
        if (sq.latexEtape1.indexOf('\\editable{}') !== -1) {
          addKeyupListener('expressionPhBlock1')
          // Seulement des chiggres permis pour l’étape 1
          mathliveRestrict('expressionPhBlock1', '\\d')
        }
        setFocusById('expressionPhBlock1ph1')
      } else {
        afficheMathliveDans('editeur', 'expression', '$' + sq.aCalculer + '$ = &1&', {
          charset: sq.charset,
          listeBoutons: ['fraction']
        })
        if (sq.charset !== '') {
          mathliveRestrict(inputId, sq.charset)
        }
        addKeyupListener(inputId)
        const $btn = $('#boutonsmathquill')
        $btn.css('display', 'inline-block')
        $btn.css('padding-top', '10px') // Pour laisser un peu de place au-dessus des boutons
        const liste = ['fraction']
        j3pPaletteMathquill('boutonsmathquill', inputId, { liste })
        focusIfExists(inputId)
      }
      const style = parcours.styles.petit.correction
      style.marginTop = '1.5em'
      style.marginLeft = '0.5em'
      j3pAddElt(parcours.zonesElts.MD, 'div', '', { id: 'correction', style })
      parcours.finEnonce()
      // on veut forcer l’usage de la touche entrée, faut appeler ça après finEnonce (sinon il remet le bouton)
      parcours.cacheBoutonValider()
    }).catch(j3pShowError)
  }

  function initDom () {
    renderMathInDocument({ renderAccessibleContent: '' })

    if (sq.titre) parcours.afficheTitre(sq.titre)

    j3pDiv(parcours.zones.MG, { id: 'conteneur', contenu: '', style: parcours.styles.etendre('toutpetit.enonce', { padding: '6px' }) })
    j3pDiv('conteneur', 'enonce', '')
    j3pDiv('conteneur', {
      id: 'formules',
      contenu: '',
      style: parcours.styles.toutpetit.enonce
    }) // Contient le formules entrées par l’élève
    j3pDiv('conteneur', 'info', '')
    afficheNombreEssaisRestants()
    const divConteneur = j3pDiv('conteneur', 'conteneurbouton', '')
    if (sq.boutonRecopie) {
      j3pAjouteBouton('conteneurbouton', 'boutonrecopier', 'MepBoutonsRepere', 'Recopier réponse précédente',
        recopierReponse)
      j3pElement('boutonrecopier').style.display = 'none'
    }
    const divEditeur = j3pDiv(divConteneur, 'editeur', '') // Le div qui contiendra l’éditeur MathQuill
    divEditeur.style.fontSize = '24px' // Une taille plus grosse pour l’éditeur
    j3pDiv(divConteneur, 'conteneurboutonInitEtape1', '')
    divEditeur.style.paddingTop = '10px' // Pour laisser un peu de place au-dessus de l’éditeur
    j3pDiv(divConteneur, 'boutonsmathquill', '')
    j3pAddElt(divConteneur, 'div', '', { id: 'divSolution' })
    $('#divSolution').css('display', 'none')
    const divMtg = j3pDiv(divConteneur, 'divmtg32', '')
    j3pCreeSVG(divMtg, {
      id: 'mtg32svg',
      width: sq.width,
      height: sq.height
    })
  }

  switch (this.etat) {
    case 'enonce':
      // code exécuté au lancement de la section
      sq.nonSimplifie = true
      if (this.debutDeLaSection) {
        // Construction de la page
        this.construitStructurePage('presentation1bis')

        this.validOnEnter = false // ex donneesSection.touche_entree
        sq.nbexp = 0
        sq.reponse = -1
        sq.marked = {} // Sert à sevoir si les éditeurs sont sur fond rouge ou non
        sq.nbEssais = parseInt(parcours.donneesSection.nbEssais)
        sq.niveau = parcours.donneesSection.niveau
        sq.simplifier = parcours.donneesSection.simplifier
        sq.premiereEtapeImposee = parcours.donneesSection.premiereEtapeImposee
        sq.aideDenominateur = sq.premiereEtapeImposee ? false : parcours.donneesSection.aideDenominateur
        if (parcours.donneesSection.calculatrice) {
          j3pDiv('MepMD', 'emplacecalc', '')
          this.fenetresjq = [{ name: 'Calculatrice', title: 'Une calculatrice', left: 600, top: 10 }]
          j3pCreeFenetres(this)
          j3pAjouteBouton('emplacecalc', 'BCalculatrice', 'MepBoutons', 'Calculatrice', boutonfen)
        }

        sq.boutonRecopie = parcours.donneesSection.boutonRecopie
        // faut gérer sous-dossier ou pas…
        const sep = this.donneesSection.ex.includes('/') ? '-' : '/'
        j3pImporteAnnexe('squelettes-mtg32' + sep + this.donneesSection.ex + '.js').then(function (datasSection) {
          // Attention, le this devient window...
          sq.txt = datasSection.txt
          sq.symbexact = datasSection.symbexact
          sq.symbnonexact = datasSection.symbnonexact
          sq.titre = datasSection.titre
          sq.width = Number(datasSection.width ?? 700)
          sq.height = Number(datasSection.height ?? 520)
          let s = datasSection.consigne1
          sq.consigne1 = (s !== undefined) ? s : ''
          s = datasSection.consigne2
          sq.consigne2 = (s !== undefined) ? s : ''
          s = datasSection.consigne3
          sq.consigne3 = (s !== undefined) ? s : ''
          s = datasSection.charset // Le set de caractères utilisés dans l’éditeur
          sq.charset = (s !== undefined) ? s : ''
          initMtg()
        }).catch(error => {
          console.error(error)
          j3pShowError('Impossible de charger les données de cette exercice')
        })
        // return;
      } else {
        j3pEmpty('correction')
        j3pEmpty('enonce')
        cacheSolution()
        $('#editeur').css('display', 'block')
        $('#boutonsmathquill').css('display', 'inline-block')

        sq.mtgAppLecteur.removeDoc('mtg32svg')
        sq.mtgAppLecteur.addDoc('mtg32svg', sq.txt, true)

        let valb, vald
        if (sq.niveau === 1) {
          vald = [2, 3, 4, 5, 6, 8, 9, 10]
          valb = [2, 3, 4, 5, 6, 8, 9, 10]
        } else {
          vald = [2, 3, 4, 5, 6, 7, 8, 9, 10, 12, 14, 15, 20]
          valb = [2, 3, 4, 5, 6, 7, 8, 9, 10, 12, 14, 15, 20]
        }
        const indice = this.questionCourante - 1
        let b, d, a, c
        // On tire un premier dénominateur au hasard pour b

        const ind0 = sq.tab1[indice]

        const paramb = this.donneesSection.b.trim()
        if (paramb === 'random') b = valb[ind0]
        else {
          if (paramb.indexOf('[') === 0) b = nombreAleat(paramb)
          else b = paramb
        }
        sq.mtgAppLecteur.giveFormula2('mtg32svg', 'b', b)

        // Comme b peut contenir une formule utilisant rand(0) il faut recalculer la figure.
        sq.mtgAppLecteur.calculate('mtg32svg', false)
        b = sq.mtgAppLecteur.valueOf('mtg32svg', 'b')
        sq.mtgAppLecteur.giveFormula2('mtg32svg', 'b', String(b))

        // On retire la valeur de b pour choisir la valeur de d
        vald.splice(ind0, 1)
        const ind1 = sq.tab2[indice]
        const paramd = this.donneesSection.d.trim()
        if (paramd === 'random') d = vald[ind1]
        else {
          if (paramd.indexOf('[') === 0) d = nombreAleat(paramd)
          else d = paramd
        }
        sq.mtgAppLecteur.giveFormula2('mtg32svg', 'd', d)

        // Comme d peut contenir une formule utilisant rand(0) il faut recalculer la figure.
        sq.mtgAppLecteur.calculate('mtg32svg', false)
        d = sq.mtgAppLecteur.valueOf('mtg32svg', 'd')
        sq.mtgAppLecteur.giveFormula2('mtg32svg', 'd', String(d))

        // On affecte à a un nombre premier avec b choisi à partir de 1 (le sq.tab2[0] ième)
        const parama = this.donneesSection.a.trim()
        if (parama === 'random') a = nbprem(b, sq.tab3[indice])
        else {
          if (parama.indexOf('[') === 0) a = nombreAleat(parama)
          else a = parama
        }
        sq.mtgAppLecteur.giveFormula2('mtg32svg', 'a', a)

        // Idem pour c
        const paramc = this.donneesSection.c.trim()
        if (paramc === 'random') c = nbprem(d, sq.tab4[indice])
        else {
          if (paramc.indexOf('[') === 0) c = nombreAleat(paramc)
          else c = paramc
        }
        sq.mtgAppLecteur.giveFormula2('mtg32svg', 'c', c)

        sq.mtgAppLecteur.calculate('mtg32svg', true) // true pour que les calculs alèatoires soient réinitialisés
        sq.premiereEtapeImposee = parcours.donneesSection.premiereEtapeImposee
        if (sq.mtgAppLecteur.valueOf('mtg32svg', 'begald') === 1) sq.premiereEtapeImposee = false

        j3pEmpty('enonce')
        j3pEmpty('formules')
        sq.nbexp = 0
        sq.reponse = -1
        const nbParam = sq.aideDenominateur ? 2 : 1
        const t = ['a', 'b']
        const param = {}
        for (let i = 0; i < nbParam; i++) {
          const code = sq.mtgAppLecteur.getLatexCode('mtg32svg', i)
          if (i === 0) sq.aCalculer = code
          param[t[i]] = code
        }
        let st = (sq.simplifier ? sq.consigne1 : sq.consigne2) + sq.consigne3
        if (sq.aideDenominateur) st += textes.conseil
        if (sq.premiereEtapeImposee) st += textes.etape1
        afficheMathliveDans('enonce', 'texte', st, param)
        j3pDetruit('expression')
        if (sq.premiereEtapeImposee) {
          sq.premierEtapeResolue = false
          sq.latexEtape1 = sq.mtgAppLecteur.getLatexCode('mtg32svg', 2)

          afficheMathliveDans('editeur', 'expression', '$' + sq.aCalculer + ' = ' + sq.latexEtape1 + '$', {
            charset: sq.charset
          })
          if (sq.latexEtape1.indexOf('\\editable{}') !== -1) {
            addKeyupListener('expressionPhBlock1')
            // Seulement des chiffres permis pour l’étape 1
            mathliveRestrict('expressionPhBlock1', '\\d')
          }
          $('#boutonsmathquill').css('display', 'none')
          setFocusById('expressionPhBlock1ph1')
        } else {
          afficheMathliveDans('editeur', 'expression', '$' + sq.aCalculer + '$ = &1&', {
            charset: sq.charset,
            listeBoutons: ['fraction']
          })
          if (sq.charset !== '') {
            mathliveRestrict(inputId, sq.charset)
          }
          addKeyupListener(inputId)
          focusIfExists(inputId)
        }
        afficheNombreEssaisRestants()
        j3pElement('info').style.display = 'block'
        sq.mtgAppLecteur.display('mtg32svg') // Pour tout réafficher
        this.finEnonce()
        // on veut forcer l’usage de la touche entrée, faut appeler ça après finEnonce (sinon il remet le bouton)
        parcours.cacheBoutonValider()
      }

      break // case "enonce":

    case 'correction':
      this._stopTimer() // pas très utile car fait par le modèle à l’appel de finCorrection() si on sort de l’état correction
      j3pEmpty('enonce')
      afficheMathliveDans('enonce', 'texte', sq.simplifier ? sq.consigne1 : sq.consigne2, sq.param)
      // A cause de la limite de temps :
      if (this.isElapsed) { // limite de temps
        j3pElement('correction').style.color = this.styles.cfaux
        if (sq.boutonRecopie) j3pElement('boutonrecopier').style.display = 'none'
        j3pElement('info').style.display = 'none'
        $('#editeur').css('display', 'none')
        $('#boutonsmathquill').css('display', 'none')
        sq.mtgAppLecteur.executeMacro('mtg32svg', 'solution')
        afficheSolution(false)
        j3pElement('correction').innerHTML = tempsDepasse
        j3pElement('correction').innerHTML += textes.txt5
      } else {
        const idrep = 'exp' + sq.nbexp
        const chdeb = sq.nbexp === 1 ? '' : '<br>'
        switch (sq.reponse) {
          case 0 :
          case -1 : // Pour le cas où la réponse était incohérente comme 1/0
            afficheMathliveDans('formules', idrep, chdeb + '$' + sq.aCalculer + ' \\ne ' + sq.rep + '$', {
              style: {
                color: this.styles.cfaux
              }
            })
            j3pElement('correction').style.color = this.styles.cfaux
            j3pElement('correction').innerHTML = textes.txt6
            break
          case 2 :
            if (sq.simplifier) {
              afficheMathliveDans('formules', idrep, chdeb + '$' + sq.aCalculer + ' = ' + sq.rep + '$', {
                style: {
                  color: this.styles.colorCorrection
                }
              })
            }
            j3pElement('correction').style.color = sq.simplifier ? this.styles.cfaux : this.styles.colorCorrection
            if (sq.simplifier) {
              let st = textes.pasfini2
              if (sq.nonSimplifie) st += '<br>' + textes.txt8 + sq.simpPar + '.'
              j3pElement('correction').innerHTML = st
            } else {
              j3pElement('correction').innerHTML = textes.txt4 + sq.simpPar + '.'
              this.score++
            }
            break
          case 1: // La fraction a été bien simplifiée
            this.score++
            afficheMathliveDans('formules', idrep, chdeb + '$' + sq.aCalculer + ' = ' + sq.rep + '$', {
              style: {
                color: this.styles.cbien
              }
            })
            j3pElement('correction').style.color = this.styles.cbien
            j3pElement('correction').innerHTML = cBien + (sq.nonSimplifie ? textes.txt7 : '')
        }
        if (sq.boutonRecopie) j3pElement('boutonrecopier').style.display = 'none'
        j3pElement('info').style.display = 'none'
        j3pElement('correction').innerHTML += textes.txt5
        sq.mtgAppLecteur.executeMacro('mtg32svg', 'solution')
        afficheSolution(sq.resolu)
        $('#editeur').css('display', 'none')
        $('#boutonsmathquill').css('display', 'none')
      }
      this.finCorrection('navigation', true)
      // Obligatoire
      break // case "correction":

    case 'navigation':
      if (!this.sectionTerminee()) {
        this.etat = 'enonce'
      }
      this.finNavigation()
      break // case "navigation":
  }
}
