import $ from 'jquery'
import { j3pAddElt, j3pAjouteBouton, j3pBoutonRadio, j3pBoutonRadioChecked, j3pDetruit, j3pDiv, j3pElement, j3pEmpty, j3pFocus, j3pImporteAnnexe, j3pPaletteMathquill, j3pShowError, j3pValeurde } from 'src/legacy/core/functions'
import { j3pAffiche, mqRestriction, unLatexify } from 'src/lib/mathquill/functions'
import { getMtgApp } from 'src/lib/outils/mathgraph'
import { j3pCreeSVG } from 'src/legacy/core/functionsSvg'
import textesGeneriques from 'src/lib/core/textes'
import { getIndexFermant } from 'src/lib/utils/string'

const { cBien, cFaux, essaieEncore, tempsDepasse } = textesGeneriques

/*
    Yves Biton
    2019

    squelettemtg32_Calc_Param_Par_Etapes
    Squelette demandant de calculer une expression ou plusieurs expressions
    On peut éventuellement spécifier des paramètres  a,b,c,d,e,f,g,h,j,k,n,n,p.
    Utilise une fichier annexe dont le nom est contenu dans le paramètre ex.
    Ce fichier contient :
    Une variable titre pour le titre de l’activité
    une variable nbEtapes qui contient le nombre d’étapes successives (maximum 5 étapes).
    Une variable nbLatex qui contient le nombre de paramètres LaTeX récupérés depuis la figure mtg32 associée.
    Une variable coefEtape1 comprise entre 0 et 1 qui contient le coefficient à attribuer à la réponse à al première étape,
      le reste étant partagé entre les étapes suivantes.
    Une variable consigne_init qui contient la consigne à afficher au début de l’activité.
    Supposons par exemple que nbEtapes vaut 2.
    nbcalc1 contient le nombre de calculs demandés à l’étape 1 (nbcalc2 pour l’étape 2).
    consigne1_1 contient le début de la consigne à l’étape 1 (consigne1_2 pour l’étape 2)
    consigne2_1 contient la initDom la consigne à l’étape 1 dans le cas ou simplifier1 est à true sinon c’est consigne3_1 qui est affiché
    consigne4_1 contient la fin de la consigne à l’étape 1 etc ...
    formulaire1 est une chaîne de caractères qui contient le formulaire à calculer à l’étape 1 (formulaire2 pour l’étape2)
    Par exemple, si nbcalc2 vaut 3, on pourra avoir dans formulaire2 la chaîne "Le centre de $C$ a pour coordonnées (edit1;edit2) et le rayon de $C$ est edit3"

    Il contient aussi des variable formulaire1, formulaire2 etc qui sont des chaîne de caractères contenant éventuellement du LaTeX et contenant
    une référence aux éditeurs MathQuill utilisés. Par exemple, si nbCalc1 vaut 2, la chaine devra contenir deux sous-chaînes
    'edit1' et 'edit2' qui correspondront aux éditeurs MathQuill. Elle peut aussi contenir une formule LaTeX (entre deux $) contenant
    des champs \editable{} qui servent à avoir des éditeurs MathQuill à l’intérieur même d’un calcul ou même d’une matrice (dans le cas
    d’une matrice, les \editable{} sont entrés ligne pr ligne.
    Quand on vérifiera la validité de la réponse de l’élève à l’étape 1 par exemple, on mettra (dans le cas où nbCalc vaut 2, par exemple), la réponse
    contenue dans l’éditeur 1 dans le calcul rep1 et, après recalcul de la figure mtg32, le calcul resolu1 contiendra 1
    si la réponse contenue dans le premier éditeur est la réponse finale attendue et 0 sinon. La figure  devra aussi contenir
    un calcul exact1 qui contiendra 1 si la résultat contenu dans exact est bon, 0 sinon. Si nbCalc1 contient 2, la figure devra
    aussi contenir deux calculs resolu2 et exact2.
    Si nbEtapes = 2, nbCalc1 = 2 et nbCalc2 = 3, les calculs servant à évaluer la réponse à l’étape 2 seront dans resolu3, resolu4
    et resolu5, exact3, exact4 et exact5 et ainsi de initDom.
    Si la variable simplifier1 est à false, un calcul exact mais pas fini sera accepté.
    Si la figure contenue  dans ex contient un paramètre nbVar , alors dans le cas ou nbrepetitions > 1, on donne pour i compris entre 1 et npParamAleat
    au calcul de la figure nommé "r"+i (par exemple r1 et r2 si nbParamAleat vaut 2) des valeurs comprises entre 0 et "nbcas"+i - 1
    toutes différentes (sauf si nbrepetitions > b=nbcas auquel cas au bout de nbcas répétitions il y aura nécessairement une valeur de r déjà prise.
    Le paramètre nbLatex contient le nombre d’affichages LaTeX qui doivent être récupérés de la figure pour affichage dans l’énoncé.

*/

export const params = {
  outils: ['mathquill'],
  /**
   * Ce qui est paramétrable dans l’éditeur de graphe
   * @type {LegacySectionParamsParametres}
   */
  parametres: [
    ['nbrepetitions', 2, 'entier', 'aucun'],
    ['limite', 0, 'entier', 'Temps disponible par répétition, en secondes'],
    ['nbEssais1', 1, 'entier', 'Nombre d’essais maximum autorisés pour le premier calcul'],
    ['nbEssais2', 3, 'entier', 'Nombre d’essais maximum autorisés pour le deuxième calcul'],
    ['nbEssais3', 6, 'entier', 'Nombre d’essais maximum autorisés pour le troisième calcul (si au moins 3 étapes)'],
    ['nbEssais4', 6, 'entier', 'Nombre d’essais maximum autorisés pour le quatrième calcul (si au moins 4 étapes)'],
    ['nbEssais5', 6, 'entier', 'Nombre d’essais maximum autorisés pour le cinquième calcul (si au moins 5 étapes)'],
    ['simplifier1', true, 'boolean', 'true si on exige un forme donnée à l’étape 1, false sinon'],
    ['simplifier2', true, 'boolean', 'true si on exige un forme donnée à l’étape 2, false sinon'],
    ['simplifier3', true, 'boolean', 'true si on exige un forme donnée à l’étape 3, false sinon'],
    ['simplifier4', true, 'boolean', 'true si on exige un forme donnée à l’étape 4, false sinon'],
    ['simplifier5', true, 'boolean', 'true si on exige un forme donnée à l’étape 5, false sinon'],
    ['coefEtape1', '0.5', 'string', 'Nombre strictement compris entre 0 et 1 à rajouter au score\npour une réussite à l’étape 1'],
    ['indicationfaute', false, 'boolean', 'true si on veut que certaines fautes soient expliquées après appui sur Entrée'],
    ['a', 'random', 'string', 'Valeur de a'],
    ['b', 'random', 'string', 'Valeur de b'],
    ['c', 'random', 'string', 'Valeur de c'],
    ['d', 'random', 'string', 'Valeur de d'],
    ['e', 'random', 'string', 'Valeur de e'],
    ['f', 'random', 'string', 'Valeur de f'],
    ['g', 'random', 'string', 'Valeur de g'],
    ['h', 'random', 'string', 'Valeur de h'],
    ['j', 'random', 'string', 'Valeur de j'],
    ['k', 'random', 'string', 'Valeur de k'],
    ['l', 'random', 'string', 'Valeur de l'],
    ['m', 'random', 'string', 'Valeur de m'],
    ['n', 'random', 'string', 'Valeur de n'],
    ['p', 'random', 'string', 'Valeur de p'],
    ['q', 'random', 'string', 'Valeur de q'],
    ['r', 'random', 'string', 'Valeur de r'],
    ['ex', 'terminale_expert/Produit_Mat22_Autre_Mat_Existe_Ou_Non', 'string', 'Nom du fichier annexe (xxx => squelettes-mtg32/xxx.js et xxx/yyy => squelettes-mtg32-xxx/yyy.js)']
  ]
}

/**
 * section squelettemtg32_Calc_Param_Par_Etapes
 * @this {Parcours}
 */
export default function main () {
  const parcours = this
  const sq = this.storage

  function onkeyup (ev, parcours) {
    const indice = this.indice
    if (sq.marked[indice - 1]) {
      demarqueEditeurPourErreur(indice)
    }
    if (ev.keyCode === 13) {
      const postString = sq['isEditableField' + sq.etape] ? 'ef' : ''
      const rep = j3pValeurde('expressioninputmq' + postString + indice)
      const chcalcul = traiteMathQuill(rep)
      const valide = sq.mtgAppLecteur.syntaxValidation('mtg32svg', 'rep1', chcalcul, true)
      if (valide) {
        validation(parcours)
        if (indice === 1) j3pFocus('expressioninputmq' + postString + indice)
      } else {
        marqueEditeurPourErreur()
        j3pElement('correction').style.color = parcours.styles.cfaux
        j3pElement('correction').innerHTML = 'Réponse incorrecte'
        j3pFocus('expressioninputmq' + postString + indice)
      }
    }
  }

  function afficheNombreEssaisRestants () {
    j3pEmpty('info')
    const nbe = parcours.donneesSection['nbEssais' + sq.etape] - sq.numEssai + 1
    if (nbe === 1) {
      j3pAffiche('info', 'texteinfo', 'Il reste un essai.', { style: { color: '#7F007F' } })
    } else {
      j3pAffiche('info', 'texteinfo', 'Il reste ' + nbe + ' essais.', { style: { color: '#7F007F' } })
    }
  }

  // Attention : Les }{ on été remplacés par des }/{
  function traiteIntegrales (st) {
    let ind
    while ((ind = st.indexOf('\\integ(')) !== -1) {
      const indpf1 = getIndexFermant(st, ind + 6)
      const fonc = st.substring(ind + 7, indpf1)
      const indpf2 = getIndexFermant(st, indpf1 + 2)
      const varfor = st.substring(indpf1 + 3, indpf2)
      const indpf3 = getIndexFermant(st, indpf2 + 2)
      const a = st.substring(indpf2 + 3, indpf3)
      const indpf4 = getIndexFermant(st, indpf3 + 2)
      const b = st.substring(indpf3 + 3, indpf4)
      st = st.substring(0, ind) + 'integrale(' + fonc + ',' + varfor + ',' + a + ',' + b + ')' + st.substring(indpf4 + 1)
    }
    return st
  }

  /**
   * Fonction renvoyant un tableau formé des noms de variables utilisées par le calcul contenu dans ch
   * Si le tableau renvoyé a une longueur au moins égale à 2 on considérera que la primitive est incohérente
   * @param {string} ch
   * @return {[]}
   */
  function variablesUtilisees (ch) {
    function utiliseVariable (nomvar) {
      let res
      res = ch.search(new RegExp('\\(' + nomvar + '[\\+\\-\\*\\/\\^\\)]')) !== -1
      res = res || ch.search(new RegExp(nomvar + '[\\+\\-\\*\\/\\^\\)]')) !== -1
      res = res || (ch.lastIndexOf(nomvar) === ch.length - 1)
      res = res || ch === nomvar
      return res
    }
    const tab = []
    for (let i = 0; i < sq.variables.length; i++) {
      const car = sq.variables.charAt(i)
      if (utiliseVariable(car)) tab.push(car)
    }
    return tab
  }

  function traitePrimitives (st) {
    if (sq.variables === '') return { valide: true, res: st }
    let ind, tab
    while ((ind = st.indexOf('\\prim(')) !== -1) {
      const indpf1 = getIndexFermant(st, ind + 5)
      const fonc = st.substring(ind + 6, indpf1)
      tab = variablesUtilisees(fonc)
      if (tab.length > 1) return { valide: false, st }
      const varfor = (tab.length === 0) ? sq.variables[0] : tab[0] // On traite le cas d’une fonction constante
      const indpf2 = getIndexFermant(st, indpf1 + 2)
      const a = st.substring(indpf1 + 3, indpf2)
      const indpf3 = getIndexFermant(st, indpf2 + 2)
      const b = st.substring(indpf2 + 3, indpf3)
      st = st.substring(0, ind) + 'primitive(' + fonc + ',' + varfor + ',' + a + ',' + b + ')' + st.substring(indpf3 + 1)
    }
    return { valide: true, res: st }
  }

  /* Abandonné
  sq.onkeydown = function(ev) {
    if (ev.keyCode === 9) { // Touche Tab
      var indice = this.indice; // Indice de l’éditeur qui a appelé la fonction
      var indnext = (indice % sq["nbCalc" + sq.etape]) + 1;
      $("#expressioninputmq" + indnext).focus();
      ev.preventDefault();
    }
  };
  */

  function traiteMathQuill (ch) {
    ch = unLatexify(ch)
    ch = traiteIntegrales(ch)
    const resul = traitePrimitives(ch)
    if (resul.valide) ch = sq.mtgAppLecteur.addImplicitMult('mtg32svg', resul.res)// Traitement des multiplications implicites
    const contientIntegOuPrim = (ch.indexOf('integrale') !== -1) || (ch.indexOf('primitive') !== -1)
    return { valide: resul.valide, res: ch, contientIntegOuPrim }
  }

  function marqueEditeurPourErreur (indice) {
    sq.marked[indice - 1] = true
    const postString = sq['isEditableField' + sq.etape] ? 'ef' : ''
    $('#expressioninputmq' + postString + indice).css('background-color', '#FF9999')
  }
  function demarqueEditeurPourErreur (indice) {
    if (!sq.marked) return
    sq.marked[indice - 1] = false // Par précaution si appel via bouton recopier réponse trop tôt
    const postString = sq['isEditableField' + sq.etape] ? 'ef' : ''
    $('#expressioninputmq' + postString + indice).css('background-color', '#FFFFFF')
  }

  function validationEditeurs () {
    let res = true
    const postString = sq['isEditableField' + sq.etape] ? 'ef' : ''
    let editeurFocusedSurErreur = false
    for (let ind = 1; ind <= sq['nbCalc' + sq.etape]; ind++) {
      const id = 'expressioninputmq' + postString + ind
      const rep = j3pValeurde(id)
      if (rep === '') {
        res = res && false
        marqueEditeurPourErreur(ind)
        if (!editeurFocusedSurErreur) j3pFocus(id)
        editeurFocusedSurErreur = true
      } else {
        const resul = traiteMathQuill(rep)
        const repcalcul = resul.res
        const valide = resul.valide && sq.mtgAppLecteur.syntaxValidation('mtg32svg', 'rep' + String(ind), repcalcul, true)
        if (!valide) {
          res = res && false
          marqueEditeurPourErreur(ind)
          if (!editeurFocusedSurErreur) j3pFocus(id)
          editeurFocusedSurErreur = true
        }
      }
    }
    return res
  }

  function montreEditeurs (bVisible) {
    if (bVisible) $('#info').css('display', 'block')
    $('#editeur').css('display', bVisible ? 'block' : 'none')
    if (sq.boutonsMathQuill) $('#boutonsmathquill').css('display', bVisible ? 'block' : 'none')
    // On vide le contenu de l’éditeur MathQuill et on lui donne le focus.
    const postString = sq['isEditableField' + sq.etape] ? 'ef' : ''
    if (bVisible) $('#expressioninputmq' + postString + '1').mathquill('latex', ' ')
    j3pFocus('expressioninputmq' + postString + '1')
  }

  function videEditeurs () {
    let postString = sq['isEditableField' + sq.etape] ? 'ef' : ''
    for (let i = 1; i <= sq['nbCalc' + sq.etape]; i++) {
      $('#expressioninputmq' + postString + i).mathquill('latex', ' ')
    }
    // On donne le focus au premier éditeur MathQuill
    postString = sq['isEditableField' + sq.etape] ? 'ef' : ''
    j3pFocus('expressioninputmq' + postString + '1')
  }

  function creeEditeurs () {
    let ind, nbCalc
    j3pEmpty('editeur')
    $('#editeur').css('display', 'block')
    let formulaire = sq['formulaire' + sq.etape]
    const t = 'abcdefghijklmnopq'
    const obj = sq.parametres
    for (const props in sq.parametres) {
      for (let j = 0; j < t.length; j++) {
        if (t.charAt(j) === props) {
          formulaire = formulaire.replace(new RegExp('£' + props, 'g'), sq.parametres[props])
        }
      }
    }
    obj.styletexte = {}
    let isEditableField = false
    nbCalc = 0
    if (formulaire.indexOf('#edit') !== -1) {
      let k = 1
      while (formulaire.indexOf('#edit') !== -1) {
        nbCalc++
        formulaire = formulaire.replace('#edit' + k, '&' + k + '&')
        k++ // Rajouté le 10/8/2021
      }
    } else {
      isEditableField = true
      ind = 0
      while (formulaire.indexOf('\\editable{', ind) !== -1) {
        nbCalc++
        ind = formulaire.indexOf('\\editable{', ind) + 1
      }
    }
    sq['nbCalc' + sq.etape] = nbCalc

    sq['isEditableField' + sq.etape] = isEditableField
    for (let k = 1; k <= nbCalc; k++) obj['inputmq' + k] = {}

    j3pAffiche('editeur', 'expression', formulaire, obj) // sq.formulaire contient ou plusieurs éditeurs MathQuill
    // On affecte un indice à chaque éditeur ainsi qu’un écouteur de focus
    const postString = isEditableField ? 'ef' : ''
    for (let k = 1; k <= nbCalc; k++) {
      j3pElement('expressioninputmq' + postString + k).indice = k
      if (sq['charset' + sq.etape] !== '') {
        mqRestriction('expressioninputmq' + postString + k, sq['charset' + sq.etape], {
          commandes: sq.listeBoutons,
          boundingContainer: parcours.zonesElts.MG
        })
      }
      $('#expressioninputmq' + postString + k).focusin(function () {
        ecoute.call(this)
      })
      j3pElement('expressioninputmq' + postString + k).onkeyup = function (ev) {
        onkeyup.call(this, ev, parcours)
      }
      // j3pElement("expressioninputmq" + k).onkeydown = sq.onkeydown;
    }
  }

  function afficheReponse (bilan, depasse) {
    let ch, coul, st, i
    if (bilan === 'exact') {
      ch = 'Réponse exacte : '
      coul = parcours.styles.cbien
    } else {
      if (bilan === 'faux') {
        ch = 'Réponse fausse : '
        coul = parcours.styles.cfaux
      } else { // Exact mais pas fini
        ch = 'Exact pas fini : '
        if (depasse) {
          coul = parcours.styles.cfaux
        } else {
          coul = '#0000FF'
        }
      }
    }
    const num = sq.numEssai
    const idrep = 'exp' + String(sq.etape) + String(num)
    if (num > 2 || sq.etape > 1) ch = '<br>' + ch
    j3pAffiche('formules', idrep + 'deb', ch, {
      styletexte: {
        couleur: coul
      }
    })

    ch = sq['formulaire' + sq.etape]
    // Le formulaire peut contenir des \editable via les paramètres
    const t = 'abcdefghijklmnopq'
    for (const props in sq.parametres) {
      for (let j = 0; j < t.length; j++) {
        if (t.charAt(j) === props) {
          ch = ch.replace(new RegExp('£' + props, 'g'), sq.parametres[props])
        }
      }
    }
    const isEditableField = sq['isEditableField' + sq.etape]
    if (isEditableField) {
      for (i = 1; i <= sq['nbCalc' + sq.etape]; i++) {
        let ind = i
        if (sq['estMatrice' + sq.etape]) {
          // Si c’est une matrice on envoie les résultats par ligne alors qu’ils sont
          // enregistrés dans la pile en colonnes
          const nbcol = sq.mtgAppLecteur.valueOf('mtg32svg', 'nbcol' + sq.etape)
          const nblig = sq.mtgAppLecteur.valueOf('mtg32svg', 'nblig' + sq.etape)
          const col = (i - 1) % nbcol
          const lig = (i - 1 - (i - 1) % nbcol) / nbcol
          ind = col * nblig + lig + 1
        }
        st = '\\textcolor{' + (sq.repResolu[ind - 1] ? parcours.styles.cbien : (sq.repExact[ind - 1] ? '#0000FF' : parcours.styles.cfaux)) + '}{'
        const inddeb = ch.indexOf('\\editable{')
        const indfin = ch.indexOf('}', inddeb)
        ch = ch.substring(0, inddeb) + '{' + st + sq.rep[ind - 1] + '}' + ch.substring(indfin)
      }
    } else {
      for (i = 1; i <= sq['nbCalc' + sq.etape]; i++) {
        st = '\\textcolor{' + (sq.repResolu[i - 1] ? parcours.styles.cbien : (sq.repExact[i - 1] ? '#0000FF' : parcours.styles.cfaux)) + '}{'
        ch = ch.replace('#edit' + i, '$' + st + sq.rep[i - 1] + '}$')
      }
    }
    j3pAffiche('formules', idrep + String(sq.etape), ch, sq.parametres)
  }

  function ecoute () {
    if (sq.boutonsMathQuill) {
      j3pEmpty('boutonsmathquill')
      const indice = this.indice

      j3pPaletteMathquill('boutonsmathquill', 'expressioninputmq' + indice, {
        liste: sq.listeBoutons,
        nomdiv: 'palette'
      })
      // Ligne suivante pour un bon alignement
      $('#palette').css('display', 'inline-block')
    }
  }

  function validation () {
    if (sq.numEssai >= 1) {
      j3pElement('boutonrecopier').style.display = 'block'
    }
    // Il faut informer la figure de l’étape
    sq.mtgAppLecteur.giveFormula2('mtg32svg', 'etape', sq.etape)
    sq.rep = []
    sq.repExact = []
    sq.repResolu = []
    sq.contientInteg = false
    const postString = sq['isEditableField' + sq.etape] ? 'ef' : ''
    for (let ind = 1; ind <= sq['nbCalc' + sq.etape]; ind++) {
      // Il faut regarder si dans la formulaire correspondant on édite des champs
      // MathQuill ou des champs editable
      const rep = j3pValeurde('expressioninputmq' + postString + ind)
      sq.rep.push(rep)
      const repMathQuill = traiteMathQuill(rep)
      sq.contientInteg = sq.contientInteg || repMathQuill.contientIntegOuPrim
      const chcalcul = repMathQuill.res
      sq.mtgAppLecteur.giveFormula2('mtg32svg', 'rep' + String(ind), chcalcul)
    }
    sq.mtgAppLecteur.calculate('mtg32svg', false)
    if (sq.indicationfaute) {
      const faute = sq.mtgAppLecteur.valueOf('mtg32svg', 'faute')
      if (faute === 1) {
        sq.mtgAppLecteur.executeMacro('mtg32svg', 'voirFaute')
      } else {
        sq.mtgAppLecteur.executeMacro('mtg32svg', 'masquerFaute')
      }
    }
    let exact = true
    let resolu = true
    let auMoinsUnExact = false
    for (let ind = 1; ind <= sq['nbCalc' + sq.etape]; ind++) {
      const res = sq.mtgAppLecteur.valueOf('mtg32svg', 'resolu' + String(ind))
      const ex = sq.mtgAppLecteur.valueOf('mtg32svg', 'exact' + String(ind))
      resolu = resolu && (res === 1)
      exact = exact && ((res === 1) || (ex === 1))
      sq.repResolu.push(res === 1)
      sq.repExact.push((res === 1) || (ex === 1))
      auMoinsUnExact = auMoinsUnExact || (ex === 1)
    }
    sq.resolu = resolu
    sq.exact = exact && auMoinsUnExact
  }

  function recopierReponse () {
    const postString = sq['isEditableField' + sq.etape] ? 'ef' : ''
    for (let ind = sq['nbCalc' + sq.etape]; ind > 0; ind--) {
      demarqueEditeurPourErreur(ind)
      $('#expressioninputmq' + postString + ind).mathquill('latex', sq.rep[ind - 1]).blur()
      if (ind === 1) j3pFocus('expressioninputmq' + postString + ind)
    }
  }

  function etapeSuivante () {
    j3pElement('boutonrecopier').style.display = 'none'
    sq.etape++
    j3pEmpty('editeur')
    j3pEmpty('enonce')
    j3pEmpty('enonceSuite')
    j3pEmpty('info')
    const k = sq.etape
    const st = sq['consigne1_' + k] + (sq['simplifier' + sq.etape] ? sq['consigne3_' + k] : sq['consigne2_' + k]) + sq['consigne4_' + k]
    j3pAffiche('enonceSuite', 'texte', st, sq.parametres)
    sq.numEssai = 1
    afficheNombreEssaisRestants()
    creeEditeurs()
    montreEditeurs(true)
    videEditeurs() // Nécessaire sinon plusieurs éditeurs peuvent clignoter
  }

  function initMtg () {
    const mtgOptions = {
      // player only, sans figure initiale ni svg, on gère ça dans initMtg et modif_fig
      loadCoreWithMathJax: true
    }
    getMtgApp('mtg32svg', {}, mtgOptions).then((mtgAppLecteur) => {
      sq.mtgAppLecteur = mtgAppLecteur
      initDom()
      sq.questionOuiNonResolue = false
      sq.etape = 1
      let code, par, car, i, j, nbrep, ar, tir, nb
      sq.mtgAppLecteur.removeAllDoc()
      sq.mtgAppLecteur.addDoc('mtg32svg', sq.txt, true)
      sq.mtgAppLecteur.calculate('mtg32svg', false)
      sq.nbexp = 0
      sq.reponse = -1
      const ch = 'abcdefghjklmnpqr'
      const nbvar = sq.mtgAppLecteur.valueOf('mtg32svg', 'nbvar')
      if (nbvar !== -1) {
        nbrep = parcours.donneesSection.nbrepetitions
        sq.aleat = true
        sq.nbParamAleat = nbvar
        for (i = 1; i <= nbvar; i++) {
          const nbcas = sq.mtgAppLecteur.valueOf('mtg32svg', 'nbcas' + i)
          nb = Math.max(nbrep, nbcas)
          ar = []
          for (j = 0; j < nb; j++) ar.push(j % nbcas)
          sq['tab' + i] = []
          for (let k = 0; k < nbrep; k++) {
            tir = Math.floor(Math.random() * ar.length)
            sq['tab' + i].push(ar[tir])
            ar.splice(tir, 1)
          }
          sq.mtgAppLecteur.giveFormula2('mtg32svg', 'r' + i, sq['tab' + i][0])
        }
      } else {
        sq.aleat = false
      }

      if (sq.param !== undefined) {
        for (i = 0; i < ch.length; i++) {
          car = ch.charAt(i)
          if (sq.param.indexOf(car) !== -1) {
            par = parcours.donneesSection[car]
            if (par !== 'random') sq.mtgAppLecteur.giveFormula2('mtg32svg', car, par)
          }
        }
      }
      for (let k = 1; k <= sq.nbEtapes; k++) sq['nbEssais' + k] = parseInt(parcours.donneesSection['nbEssais' + k])

      sq.coefEtape1 = parseFloat(parcours.donneesSection.coefEtape1)
      sq.coefEtapesSuivantes = (1 - sq.coefEtape1) / (sq.nbEtapes - 1)

      sq.mtgAppLecteur.calculateAndDisplayAll(true)
      const nbParam = parseInt(sq.nbLatex)
      const t = 'abcdefghijklmnopq'
      const param = {}
      for (i = 0; i < nbParam; i++) {
        // Attention MathGraph32 traite les accents d’une façon non reconnue par MathQuill
        code = sq.mtgAppLecteur.getLatexCode('mtg32svg', i).replace(/\\acute{e}/g, '\\text{é}').replace(/\\agrave{e}/g, '\\text{è}')
        param[t.charAt(i)] = code
      }
      sq.parametres = param // Mémorisation pour l’étape 2
      if (sq.questionOuiNon) {
        j3pAffiche('enoncequestionouinon', 'texteInit', sq.consigne_QuestionOuiNon, param)
        j3pBoutonRadio('questionouinon', 'radio1', 'ensemble', '0', sq.oui)
        j3pAffiche('questionouinon', 'para1', '&nbsp; &nbsp; &nbsp;', {})
        j3pBoutonRadio('questionouinon', 'radio2', 'ensemble', '1', sq.non)
        $('#editeur').css('display', 'none')
        $('#boutonsmathquill').css('display', 'none')
      } else {
        afficheNombreEssaisRestants()
        j3pAffiche('enonceInit', 'texteInit', sq.consigne_init, param)
        const st = sq.consigne1_1 + (sq['simplifier' + sq.etape] ? sq.consigne3_1 : sq.consigne2_1) + sq.consigne4_1
        j3pAffiche('enonce', 'texte', st, param)
        sq.indicationfaute = parcours.donneesSection.indicationfaute // true si indication quand certaines fautes sont faites et appui sur Entrée
        creeEditeurs()
        if (sq.boutonsMathQuill) {
          j3pPaletteMathquill('boutonsmathquill', 'expressioninputmq1', { liste: sq.listeBoutons, nomdiv: 'palette' })
          // Ligne suivante pour un bon alignement
          $('#palette').css('display', 'inline-block')
        } else {
          j3pDetruit('boutonsmathquill')
        }
        const postString = sq['isEditableField' + sq.etape] ? 'ef' : ''
        j3pFocus('expressioninputmq' + postString + '1')
        // $("#expressioninputmq1").focus();
      }
      parcours.finEnonce()
    }).catch(j3pShowError)
  }

  function initDom () {
    if (sq.titre) parcours.afficheTitre(sq.titre)

    j3pDiv(parcours.zones.MG, {
      id: 'conteneur',
      contenu: '',
      style: parcours.styles.etendre('toutpetit.enonce', { padding: '6px' })
    })
    j3pDiv('conteneur', 'enonceInit', '')
    if (sq.questionOuiNon) {
      j3pDiv('conteneur', 'enoncequestionouinon', '')
      j3pDiv('conteneur', 'questionouinon', '')
    }
    j3pDiv('conteneur', 'enonce', '')
    j3pDiv('conteneur', {
      id: 'formules',
      contenu: '',
      style: parcours.styles.petit.enonce
    }) // Contient le formules entrées par l’élève
    j3pDiv('conteneur', 'enonceSuite', '')

    j3pDiv('conteneur', 'info', '')
    j3pDiv('conteneur', 'conteneurbouton', '')
    j3pAjouteBouton('conteneurbouton', 'boutonrecopier', 'MepBoutonsRepere', 'Recopier réponse précédente',
      recopierReponse)
    j3pElement('boutonrecopier').style.display = 'none'
    j3pDiv('conteneur', 'editeur', '') // Le div qui contiendra la chaine avec le ou les éditeurs MathQuill
    if (sq.bigSize) $('#editeur').css('font-size', '30px')
    $('#editeur').css('padding-top', '10px') // Pour laisser un peu de place au-dessus de l’éditeur
    j3pDiv('conteneur', 'boutonsmathquill', '')
    $('#boutonsmathquill').css('display', 'inline-block').css('padding-top', '10px') // Pour laisser un peu de place au-dessus des boutons

    const liste = []
    if (sq.btnFrac) liste.push('fraction')
    if (sq.btnPuis) liste.push('puissance')
    if (sq.btnRac) liste.push('racine')
    if (sq.btnExp) liste.push('exp')
    if (sq.btnLn) liste.push('ln')
    if (sq.btnPi) liste.push('pi')
    if (sq.btnCos) liste.push('cos')
    if (sq.btnSin) liste.push('sin')
    if (sq.btnTan) liste.push('tan')
    if (sq.btnInteg) liste.push('integ')
    if (sq.btnPrim) liste.push('prim')

    if (sq.ln) liste.push('ln')
    sq.listeBoutons = liste
    sq.boutonsMathQuill = liste.length !== 0
    j3pDiv('conteneur', 'divmtg32', '')
    j3pCreeSVG('divmtg32', {
      id: 'mtg32svg',
      width: 700,
      height: 900
    })

    // OBLIGATOIRE
    parcours.cacheBoutonSuite()
    const style = parcours.styles.petit.correction
    style.marginTop = '1.5em'
    style.marginLeft = '0.5em'
    j3pAddElt(parcours.zonesElts.MD, 'div', '', { id: 'correction', style })
  }

  function _Donnees () {
    // "primaire" || "lycee" || "college"
    // Actuellement les dimensions sont identiques
    this.typesection = 'lycee'//

    // Nombre de répétitions de l’exercice
    this.nbrepetitions = 2
    this.nbetapes = 1
    this.nbitems = this.nbetapes * this.nbrepetitions

    this.ex = 'terminale_expert/Produit_Mat22_Autre_Mat_Existe_Ou_Non'
    // Si indication != "" alors un lien "Indication" s’affiche dans la zone Inférieure Gauche (this.zones.IG
    // une indication<br>de deux lignes...";
    this.indication = ''
    const st = 'abcdefghjklmnpqr'
    for (let i = 0; i < st.length; i++) this[st.charAt(i)] = 'random'
    this.nbEssais1 = 1
    this.nbEssais2 = 3
    this.nbEssais3 = 6
    this.nbEssais4 = 6
    this.nbEssais5 = 6
    this.coefEtape1 = 0.5
    this.simplifier1 = true
    this.simplifier2 = true
    this.simplifier3 = true
    this.simplifier4 = true
    this.simplifier5 = true

    this.indicationfaute = true

    this.textes = {}
    this.titre = 'Titre de l’activité'

    this.pe = 0
  }

  switch (this.etat) {
    case 'enonce':
      // code exécuté au lancement de la section
      if (this.debutDeLaSection) {
        this.donneesSection = new _Donnees()
        // Construction de la page
        parcours.construitStructurePage('presentation1bis')

        this.surcharge()
        // compteur.
        sq.numEssai = 1

        for (let i = 1; i <= 5; i++) {
          const s = parcours.donneesSection['simplifier' + i]
          sq['simplifier' + i] = (s === undefined) ? true : s
        }
        // faut gérer sous-dossier ou pas…
        const sep = this.donneesSection.ex.includes('/') ? '-' : '/'
        j3pImporteAnnexe('squelettes-mtg32' + sep + this.donneesSection.ex + '.js').then(function (datasSection) {
          // console.log("initsection");
          // Attention, le this devient window...
          sq.txt = datasSection.txt
          sq.nbEtapes = datasSection.nbEtapes
          sq.nbLatex = Number(datasSection.nbLatex ?? 0) // Le nombre de paramètres LaTeX dans le texte
          // ?? retourne l’opérande de gauche si !== undefined && !== null, celle de droite sinon
          const listeBoutons = []
          // boutons avec propriétés à true par défaut
          const tabBoutons1 = ['puissance', 'fraction']
          ;['btnPuis', 'btnFrac'].forEach((p, i) => {
            const b = Boolean(datasSection[p] ?? true)
            if (b) listeBoutons.push(tabBoutons1[i])
          })
          // boutons avec false par défaut
          const tabBoutons2 = ['pi', 'racine', 'exp', 'ln', 'sin', 'cos', 'tan', 'abs', 'conj', 'integ', 'prim']
          ;['btnPi', 'btnRac', 'btnExp', 'btnLn', 'btnSin', 'btnCos', 'btnTan', 'btnAbs', 'btnConj', 'btnInteg', 'btnPrim'].forEach((p, i) => {
            const b = Boolean(datasSection[p] ?? false)
            if (b) listeBoutons.push(tabBoutons2[i])
          })
          sq.listeBoutons = listeBoutons
          sq.boutonsMathQuill = listeBoutons.length !== 0
          sq.consigne_init = datasSection.consigne_init ?? ''
          sq.questionOuiNon = Boolean(datasSection.questionOuiNon ?? false)
          if (sq.questionOuiNon) {
            sq.consigne_QuestionOuiNon = datasSection.consigne_QuestionOuiNon ?? ''
            sq.oui = datasSection.oui ?? 'Oui'
            sq.non = datasSection.non ?? 'non'
          }
          for (let k = 1; k <= sq.nbEtapes; k++) {
            sq['isEditableField' + k] = Boolean(datasSection['isEditableField' + k] ?? false)
            sq['simplifier' + k] = Boolean(datasSection['simplifier' + k] ?? true)
            sq['consigne1_' + k] = datasSection['consigne1_' + k] ?? ''
            sq['consigne2_' + k] = datasSection['consigne2_' + k] ?? ''
            sq['consigne3_' + k] = datasSection['consigne3_' + k] ?? ''
            sq['consigne4_' + k] = datasSection['consigne4_' + k] ?? ''
            sq['nbCalc' + k] = datasSection['nbCalc' + k]
            sq['formulaire' + k] = datasSection['formulaire' + k] // Contient la chaine avec les éditeurs.
            // Information pour savoir si à l’étape n on édite une matrice  et si oui son
            // nombre de lignes et de colonnes
            sq['estMatrice' + k] = Boolean(datasSection['estMatrice' + k] ?? false)
            sq['nbcol' + k] = Number(datasSection['nbcol' + k] ?? 0)
            sq['nblig' + k] = Number(datasSection['nblig' + k] ?? 0)
            sq['charset' + k] = datasSection['charset' + k] ?? ''
          }
          sq.variables = datasSection.variables ?? ''
          sq.titre = datasSection.titre ?? ''
          sq.bigSize = Boolean(datasSection.bigSize ?? false)
          sq.param = datasSection.param ?? '' // Chaine contenant les paramètres autorisés
          sq.marked = []
          for (let k = 0; k < sq.nbCalc; k++) {
            sq.marked[k] = false
          }

          initMtg()
        }).catch(error => {
          console.error(error)
          j3pShowError('Impossible de charger les données de cette exercice')
        })
        // return;
      } else {
        j3pEmpty('correction')
        sq.numEssai = 1
        sq.questionOuiNonResolue = false
        sq.etape = 1
        sq.mtgAppLecteur.removeDoc('mtg32svg')
        sq.mtgAppLecteur.addDoc('mtg32svg', sq.txt, true)

        if (sq.aleat) {
          for (let i = 1; i <= sq.nbParamAleat; i++) {
            sq.mtgAppLecteur.giveFormula2('mtg32svg', 'r' + i, sq['tab' + i][this.questionCourante - 1])
          }
        }
        if (sq.param !== undefined) {
          const ch = 'abcdefghjklmnpqr'
          for (let i = 0; i < ch.length; i++) {
            const car = ch.charAt(i)
            if (sq.param.indexOf(car) !== -1) {
              const par = parcours.donneesSection[car]
              if (par !== 'random') sq.mtgAppLecteur.giveFormula2('mtg32svg', car, par)
            }
          }
        }

        sq.mtgAppLecteur.calculate('mtg32svg', true) // true pour que les calculs aléatoires soient réinitialisés
        if (sq.questionOuiNon) {
          j3pEmpty('questionouinon')
          j3pEmpty('enoncequestionouinon')
        }
        j3pEmpty('enonceInit')
        j3pEmpty('enonce')
        j3pEmpty('formules')
        j3pEmpty('info')
        j3pEmpty('enonceSuite')

        sq.nbexp = 0
        sq.reponse = -1
        const nbParam = parseInt(sq.nbLatex)
        const t = 'abcdefghijklmnopq'
        const param = {}
        for (let i = 0; i < nbParam; i++) {
          const code = sq.mtgAppLecteur.getLatexCode('mtg32svg', i).replace(/\\acute{e}/g, '\\text{é}').replace(/\\agrave{e}/g, '\\text{è}')
          param[t.charAt(i)] = code
        }
        sq.parametres = param
        if (sq.questionOuiNon) {
          j3pAffiche('enoncequestionouinon', 'texteInit', sq.consigne_QuestionOuiNon, param)
          j3pBoutonRadio('questionouinon', 'radio1', 'ensemble', '0', sq.oui)
          j3pAffiche('questionouinon', 'para1', '&nbsp; &nbsp; &nbsp;', {})
          j3pBoutonRadio('questionouinon', 'radio2', 'ensemble', '1', sq.non)
          $('#editeur').css('display', 'none')
          $('#boutonsmathquill').css('display', 'none')
        } else {
          const st = sq.consigne1_1 + (sq['simplifier' + sq.etape] ? sq.consigne3_1 : sq.consigne2_1) + sq.consigne4_1
          j3pAffiche('enonceInit', 'texteInit', sq.consigne_init, param)
          j3pAffiche('enonce', 'texte', st, param)
          afficheNombreEssaisRestants()
          j3pElement('info').style.display = 'block'

          j3pEmpty('editeur')
          creeEditeurs()
        }
        sq.mtgAppLecteur.display('mtg32svg') // Pour tout réafficher
        // Pour revenir en haut de page
        try {
          window.location.hash = 'MepMG'
          j3pElement('MepMG').scrollIntoView()
        } catch (e) {
          console.error(e)
        }
        // $('#expressioninputmq1').focus()
        this.finEnonce()
      }

      break // case "enonce":

    case 'correction': {
      let bilanReponse = ''
      let simplificationPossible = false
      if (sq.questionOuiNon && !sq.questionOuiNonResolue) {
        const rep = j3pBoutonRadioChecked('ensemble')[0]
        if (rep > -1) {
          const oui = sq.mtgAppLecteur.valueOf('mtg32svg', 'vrai')
          if (oui === 1) {
            if (rep === 0) bilanReponse = 'exact'; else bilanReponse = 'fauxouinon'
          } else {
            if (rep === 0) bilanReponse = 'fauxouinon'; else bilanReponse = 'exactouinon'
          }
        } else {
          bilanReponse = 'incorrect'
        }
      } else {
        if (validationEditeurs()) {
          validation()
          if (sq['simplifier' + sq.etape]) {
            if (sq.resolu) {
              bilanReponse = 'exact'
            } else {
              if (sq.exact) {
                bilanReponse = 'exactPasFini'
                simplificationPossible = true
              } else {
                bilanReponse = 'faux'
              }
            }
          } else {
            if (sq.resolu || sq.exact) {
              if (sq.resolu) bilanReponse = 'exact'
              else {
                simplificationPossible = true
                if (sq.exact && sq.contientInteg) {
                  bilanReponse = 'exactPasFini'
                } else bilanReponse = 'exact'
              }
            } else {
              bilanReponse = 'faux'
            }
          }
        } else {
          bilanReponse = 'incorrect'
        }
      }
      // A cause de la limite de temps :
      if (this.isElapsed) { // limite de temps
        this._stopTimer()
        montreEditeurs(false)
        j3pElement('correction').style.color = this.styles.cfaux
        j3pElement('boutonrecopier').style.display = 'none'
        j3pElement('info').style.display = 'none'
        if (sq.indicationfaute) sq.mtgAppLecteur.executeMacro('mtg32svg', 'masquerFaute')

        sq.mtgAppLecteur.executeMacro('mtg32svg', 'solution')
        j3pElement('correction').innerHTML = tempsDepasse
        j3pElement('correction').innerHTML += '<br>La solution se trouve ci-contre.'
        this.etat = 'navigation'
        this.sectionCourante()
      } else {
        if (bilanReponse === 'incorrect') {
          j3pElement('correction').style.color = this.styles.cfaux
          j3pElement('correction').innerHTML = 'Réponse incorrecte'
          this.afficheBoutonValider()
        } else {
          // Une réponse a été saisie
          // Bonne réponse
          if (bilanReponse === 'exact') {
            sq.mtgAppLecteur.setActive('mtg32svg', false)
            if (!sq.questionOuiNon || sq.questionOuiNonResolue) {
              this.score += (sq.etape === 1 ? sq.coefEtape1 : sq.coefEtapesSuivantes)
            }
            j3pElement('correction').style.color = this.styles.cbien
            let mess = simplificationPossible ? 'C’est bien mais on pouvait  simplifier la réponse' : cBien
            if (sq.etape === 1) mess += '<br>On passe à la question suivante.'
            j3pElement('correction').innerHTML = mess
            if (!sq.questionOuiNon || sq.questionOuiNonResolue) {
              montreEditeurs(false)
              sq.numEssai++ // Pour un affichage correct dans afficheReponse
              afficheReponse('exact', false)
              if (sq.etape === sq.nbEtapes) {
                j3pEmpty('enonceSuite')
                this._stopTimer()
                j3pElement('boutonrecopier').style.display = 'none'
                j3pElement('info').style.display = 'none'
                if (sq.indicationfaute) sq.mtgAppLecteur.executeMacro('mtg32svg', 'masquerFaute')
                sq.mtgAppLecteur.executeMacro('mtg32svg', 'solution')
                this.etat = 'navigation'
                this.sectionCourante()
              } else {
                etapeSuivante()
              }
            } else {
              sq.questionOuiNonResolue = true
              $('#radio1').attr('disabled', true)
              $('#radio2').attr('disabled', true)
              j3pAffiche('enonceInit', 'texteInit', sq.consigne_init, sq.parametres)
              const st = sq.consigne1_1 + (sq['simplifier' + sq.etape] ? sq.consigne3_1 : sq.consigne2_1) + sq.consigne4_1
              j3pAffiche('enonce', 'texte', st, sq.parametres)
              sq.indicationfaute = parcours.donneesSection.indicationfaute // true si indication quand certaines fautes sont faites et appui sur Entrée
              creeEditeurs()
              montreEditeurs(true)
              if (sq.questionOuiNon) {
                $('#editeur').css('display', 'block')
                $('#boutonsmathquill').css('display', 'block')
                afficheNombreEssaisRestants()
              }

              if (sq.boutonsMathQuill) {
                j3pPaletteMathquill('boutonsmathquill', 'expressioninputmq1', {
                  liste: sq.listeBoutons,
                  nomdiv: 'palette'
                })
                // Ligne suivante pour un bon alignement
                $('#palette').css('display', 'inline-block')
              } else {
                j3pDetruit('boutonsmathquill')
              }
            }
          } else {
            $('#radio1').attr('disabled', true)
            $('#radio2').attr('disabled', true)
            if (bilanReponse === 'fauxouinon' || bilanReponse === 'exactouinon') { // Cas où la réponse est faux
              sq.mtgAppLecteur.executeMacro('mtg32svg', 'solution')
              if (bilanReponse === 'fauxouinon') {
                j3pElement('correction').style.color = this.styles.cfaux
                j3pElement('correction').innerHTML = cFaux
              } else {
                j3pElement('correction').style.color = this.styles.cbien
                j3pElement('correction').innerHTML = cBien
                this.score += 1
              }
              this.etat = 'navigation'
              this.sectionCourante()
            } else {
              sq.numEssai++
              if (bilanReponse === 'exactPasFini') {
                j3pElement('correction').style.color =
                  (sq.numEssai <= this.donneesSection['nbEssais' + sq.etape]) ? '#0000FF' : this.styles.cfaux
                j3pElement('correction').innerHTML = 'Le calcul est bon mais pas écrit sous la forme demandée.'
              } else {
                j3pElement('correction').style.color = this.styles.cfaux
                j3pElement('correction').innerHTML = cFaux
              }
              j3pEmpty('info')
              if (sq.numEssai <= this.donneesSection['nbEssais' + sq.etape]) {
                afficheNombreEssaisRestants()
                videEditeurs()
                afficheReponse(bilanReponse, false)
                // S’il y a plus de deux chances,on appelle à nouveau le bouton Valider
                j3pElement('correction').innerHTML += '<br>' + '<br>' + essaieEncore
              } else {
                afficheReponse(bilanReponse, true)
                if (sq.etape === sq.nbEtapes) {
                  j3pEmpty('enonceSuite')
                  this._stopTimer()
                  sq.mtgAppLecteur.setActive('mtg32svg', false)
                  montreEditeurs(false)
                  j3pElement('boutonrecopier').style.display = 'none'
                  j3pElement('info').style.display = 'none'

                  if (sq.indicationfaute) sq.mtgAppLecteur.executeMacro('mtg32svg', 'masquerFaute')
                  sq.mtgAppLecteur.executeMacro('mtg32svg', 'solution')
                  j3pElement('correction').innerHTML += '<br><br>La solution se trouve ci-contre.'
                  this.etat = 'navigation'
                  this.sectionCourante()
                } else {
                  j3pElement('correction').innerHTML += '<br><br>On passe à la question suivante.'
                  etapeSuivante()
                }
              }
            }
          }
        }
      }
      // Obligatoire
      this.finCorrection()
      break // case "correction":
    }

    case 'navigation':

      if (this.sectionTerminee()) {
        // On détermine la phrase d’état renvoyée par la section
        const newscore = this.score / this.donneesSection.nbitems
        this.parcours.pe = newscore.toFixed(2)
        this.afficheBoutonSectionSuivante()
        this.focus('sectioncontinuer')
      } else {
        this.etat = 'enonce'
        this.afficheBoutonSuite()
        this.focus('boutoncontinuer')
      }
      // Obligatoire
      this.finNavigation()

      break // case "navigation":
  }
}
