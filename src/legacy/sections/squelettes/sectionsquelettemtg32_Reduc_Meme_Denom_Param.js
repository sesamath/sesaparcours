import $ from 'jquery'
import { MathfieldElement, renderMathInDocument } from 'mathlive'

import { j3pAddElt, j3pAjouteBouton, j3pDiv, j3pElement, j3pEmpty, j3pImporteAnnexe, j3pPaletteMathquill, j3pShowError } from 'src/legacy/core/functions'
import { unLatexify } from 'src/lib/mathquill/functions'
import { getMtgApp } from 'src/lib/outils/mathgraph'
import { j3pCreeSVG } from 'src/legacy/core/functionsSvg'
import { cleanLatexForMl, mathliveRestrict } from 'src/lib/outils/mathlive/utils'
import { afficheMathliveDans, getMathliveValue } from 'src/lib/outils/mathlive/display'
import { resetKeyboardPosition } from 'src/lib/widgets/mlVirtualKeyboard/VirtualKeyboard'
import { setMathliveCss } from 'src/lib/outils/mathlive/mathlivecss'
import textesGeneriques from 'src/lib/core/textes'
import { focusIfExists } from 'src/lib/utils/dom/main'

const { cBien, cFaux, essaieEncore, tempsDepasse } = textesGeneriques

/*
    Yves Biton
    Septembre 2014

    http://localhost:8081/?graphe=[1,"squelettemtg32_Reduc_Meme_Denom_Param",[{pe:">=0",nn:"fin",conclusion:"fin"},{f:"Fact_ax_Carre_Moins_cCarre_Ex1"}]];
    squelettemtg32_Reduc_Meme_Denom_Param
    Squelette demandant de réduire une somme de fractions rationnelles au même dénominateur.
    On peut éventuellement spécifier des paramètres  a,b,c,d,e,f,g,h,i,j,k.
    Peut être utilisé avec les fichiers suivant pour le paramètre ex :
    Fact_cxPlusd_axPlusb_Plus_k_exPlusf_axPlusb

*/

// on définit le dossier des fonts mathlive (il faut le faire très tôt,
// ici c’est bon avec pnpm start mais trop tard pour un build (cf src/lib/player/index.js de la branche draftPlayer)
if (MathfieldElement) { // undefined dans les tests avec happy-dom
  MathfieldElement.fontsDirectory = '/static/mathlive/fonts'
  MathfieldElement.soundsDirectory = '/static/mathlive/sounds'
}

export const params = {
  /**
   * Ce qui est paramétrable dans l’éditeur de graphe
   * @type {LegacySectionParamsParametres}
   */
  parametres: [
    ['nbrepetitions', 1, 'entier', 'aucun'],
    ['limite', 0, 'entier', 'Temps disponible par répétition, en secondes'],
    ['nbchances', 1, 'entier', 'Nombre d’erreurs possibles (pour tenter de les corriger)'],
    ['nbEssais', 6, 'entier', 'Nombre d’essais maximum autorisé (8 par défaut)'],
    ['indicationDenom', true, 'boolean', 'true si on veut que le dénominateur commun le meilleur soit indiqué'],
    ['a', 'random', 'string', 'Valeur de a'],
    ['b', 'random', 'string', 'Valeur de b'],
    ['c', 'random', 'string', 'Valeur de c'],
    ['d', 'random', 'string', 'Valeur de d'],
    ['e', 'random', 'string', 'Valeur de e'],
    ['f', 'random', 'string', 'Valeur de f'],
    ['g', 'random', 'string', 'Valeur de g'],
    ['h', 'random', 'string', 'Valeur de h'],
    ['j', 'random', 'string', 'Valeur de j'],
    ['k', 'random', 'string', 'Valeur de k'],
    ['l', 'random', 'string', 'Valeur de l'],
    ['m', 'random', 'string', 'Valeur de m'],
    ['n', 'random', 'string', 'Valeur de n'],
    ['p', 'random', 'string', 'Valeur de p'],
    ['q', 'random', 'string', 'Valeur de q'],
    ['r', 'random', 'string', 'Valeur de r'],
    ['ex', 'Reduc_a_Sur_e(x-c)_Plus_b_Sur_f', 'string', 'Nom du fichier annexe (xxx => squelettes-mtg32/xxx.js et xxx/yyy => squelettes-mtg32-xxx/yyy.js)']
  ]
}

const inputId = 'expressioninputmq1'

/**
 * section squelettemtg32_Reduc_Meme_Denom_Param
 * @this {Parcours}
 */
export default function main () {
  const parcours = this
  const sq = this.storage

  function onkeyup (ev, parcours) {
    if (sq.marked) { demarqueEditeurPourErreur() }
    if (ev.keyCode === 13) {
      const rep = getMathliveValue(inputId)
      if (rep !== '') {
        const repcalcul = traiteMathlive(rep)
        const valide = sq.mtgAppLecteur.syntaxValidation('mtg32svg', 'rep', repcalcul, false)
        if (valide) {
          validation(parcours)
        } else {
          marqueEditeurPourErreur()
          j3pElement('correction').style.color = parcours.styles.cfaux
          j3pElement('correction').innerHTML = 'Faute de syntaxe'
          focusIfExists(inputId)
        }
      } else focusIfExists(inputId)
    }
  }

  function traiteMathlive (ch) {
    // Mathlive ne met pas d’accolades pour les fractions dont le numérateur et le dénominateur sont des entiers
    ch = ch.replace(/\\frac(\d)(\d)/g, '\\frac{$1' + '}{' + '$2}')
    // Ni pour les racines carrées d’un entier
    ch = ch.replace(/\\sqrt(\d)/g, '\\sqrt{$1}')
    // Pour les puissances dont l’exposant est un chiffre, Mathlive ne met pas d’accolades
    ch = ch.replace(/\^([^{])/g, '^{$1}')
    ch = unLatexify(ch)
    // mathlive peut mettre des {} vides
    ch = ch.replace(/\\{\\}/g, '')
    ch = ch.replace(/\^(\d)(\d)/g, '^$1*$2') // On peut avoir par exemple 2^35^4 pour 2^3*5^4
    return sq.mtgAppLecteur.addImplicitMult('mtg32svg', ch)// Traitement des multiplications implicites
  }

  /**
   * Fonction extrayant d’un affichage LaTeX de la figure les lignes de texte et les transformant
   * en une chaîne affichable par MathQuill
   * @param tagLatex : le tag de l’affichage LaTeX de la figure
   * @return {string}
   */
  function extraitDeLatexPourMathlive (tagLatex) {
    const list = sq.mtgAppLecteur.getList('mtg32svg')
    const latex = list.getByTag(tagLatex)
    if (latex === null || latex.className !== 'CLatex') return ''
    return cleanLatexForMl(latex.chaineLatex)
  }

  function afficheNombreEssaisRestants () {
    j3pEmpty('info')
    const nbe = sq.nbEssais - sq.nbexp
    const nbc = Math.min(sq.nbchances - sq.numEssai + 1, nbe)
    if ((nbe === 1) && (nbc === 1)) {
      afficheMathliveDans('info', 'texteinfo', 'Il reste une validation.', { style: { color: '#7F007F' } })
    } else {
      afficheMathliveDans('info', 'texteinfo', 'Il reste ' + nbe + ' essai(s) et ' + nbc + ' validation(s).', { style: { color: '#7F007F' } })
    }
  }

  /**
   * Fonction affichant la solution s’il existe un affichage LaTeX de tag solution
   * @param{boolean} bexact true si l’élève a entré la bonne réponse, false sinon
   */
  function afficheSolution (bexact) {
    $('#divSolution').css('display', 'block').html('')
    const ch = extraitDeLatexPourMathlive('solution')
    if (ch !== '') {
      afficheMathliveDans('divSolution', 'solution', ch, { style: { color: bexact ? 'green' : 'blue' } })
    }
  }

  function cacheSolution () {
    $('#divSolution').css('display', 'none').html('')
  }

  function marqueEditeurPourErreur () {
    sq.marked = true
    const mf = j3pElement(inputId)
    if (!mf) return
    mf.style.backgroundColor = '#FF9999'
    mf.demarqueErreur = () => {
      if (sq.marked) {
        demarqueEditeurPourErreur()
      }
    }
  }

  function demarqueEditeurPourErreur () {
    sq.marked = false
    j3pEmpty('correction')
    const mf = j3pElement(inputId)
    if (!mf) return
    mf.style.backgroundColor = 'white'
  }

  function validation (parcours, continuer) {
    if (arguments.length <= 1) continuer = true
    if (sq.nbexp === 0) {
      j3pElement('boutonrecopier').style.display = 'block'
    }
    sq.rep = getMathliveValue(inputId)
    sq.mtgAppLecteur.giveFormula2('mtg32svg', 'rep', traiteMathlive(sq.rep))
    j3pEmpty('correction')
    sq.mtgAppLecteur.calculate('mtg32svg', false)
    sq.reponse = sq.mtgAppLecteur.valueOf('mtg32svg', 'reponse')
    const idrep = 'exp' + sq.nbexp
    const ch = sq.nbexp === 0 ? '' : '<br>'
    sq.nbexp++
    if (sq.reponse >= 1) {
      afficheMathliveDans('formules', idrep, ch + '$f(x)' + ' ' + sq.symbexact +
        ' ' + sq.mtgAppLecteur.getLatexFormula('mtg32svg', 'rep') + '$', { style: { color: '#0000FF' } })
    } else {
      afficheMathliveDans('formules', idrep, ch + '$f(x)' + ' ' + sq.symbnonexact +
        ' ' + sq.mtgAppLecteur.getLatexFormula('mtg32svg', 'rep') + '$', { style: { color: '#FF0000' } })
    }
    // On vide le contenu de l’éditeur MathQuill
    j3pElement(inputId).value = ''
    focusIfExists(inputId)

    // Si le nombre maximum d’essais est atteint on désactive la figure
    if (sq.nbexp === sq.nbEssais) {
      sq.mtgAppLecteur.setActive('mtg32svg', false)
      if (continuer) {
        parcours.etat = 'correction'
        parcours.sectionCourante()
      }
    } else {
      afficheNombreEssaisRestants()
      resetKeyboardPosition()
    }
  }

  function recopierReponse () {
    demarqueEditeurPourErreur()
    j3pElement(inputId).value = sq.rep
    focusIfExists(inputId)
  }

  function initMtg () {
    setMathliveCss('MepMG')
    const mtgOptions = {
      // player only, sans figure initiale ni svg, on gère ça dans initMtg et modif_fig
      loadCoreWithMathJax: true
    }
    getMtgApp('mtg32svg', {}, mtgOptions).then((mtgAppLecteur) => {
      sq.mtgAppLecteur = mtgAppLecteur
      initDom()
      let code, par, car, i, j, k, nb, nbrep, nbcas, ar, tir
      sq.mtgAppLecteur.removeAllDoc()
      sq.mtgAppLecteur.addDoc('mtg32svg', sq.txt, true)
      sq.mtgAppLecteur.calculate('mtg32svg', false)
      const ch = 'abcdefghjklmnpqr'
      const nbvar = sq.mtgAppLecteur.valueOf('mtg32svg', 'nbvar')
      if (nbvar !== -1) {
        nbrep = parcours.donneesSection.nbrepetitions
        sq.aleat = true
        sq.nbParamAleat = nbvar
        for (i = 1; i <= nbvar; i++) {
          nbcas = sq.mtgAppLecteur.valueOf('mtg32svg', 'nbcas' + i)
          nb = Math.max(nbrep, nbcas)
          ar = []
          for (j = 0; j < nb; j++) ar.push(j % nbcas)
          sq['tab' + i] = []
          for (k = 0; k < nbrep; k++) {
            tir = Math.floor(Math.random() * ar.length)
            sq['tab' + i].push(ar[tir])
            ar.splice(tir, 1)
          }
          sq.mtgAppLecteur.giveFormula2('mtg32svg', 'r' + i, sq['tab' + i][0])
        }
      } else { sq.aleat = false }

      for (i = 0; i < ch.length; i++) {
        car = ch.charAt(i)
        if (sq.param.indexOf(car) !== -1) {
          par = parcours.donneesSection[car]
          if (par !== 'random') sq.mtgAppLecteur.giveFormula2('mtg32svg', car, par)
        }
      }
      sq.mtgAppLecteur.calculateAndDisplayAll(true)
      const nbParam = parseInt(sq.numero)
      const t = ['a', 'b', 'c', 'd']
      const param = {}
      for (i = 0; i < nbParam; i++) {
        code = sq.mtgAppLecteur.getLatexCode('mtg32svg', i)
        param[t[i]] = code
      }
      sq.indicationDenom = parcours.donneesSection.indicationDenom

      param.e = String(sq.nbEssais)
      const st = (sq.indicationDenom ? sq.consigne2 : sq.consigne3) + sq.consigne4
      afficheMathliveDans('enonce', 'texte', sq.consigne1 + st, param)
      // Les 3 lignes suivantes pour que sq.mtgAppLecteur soit prêt quand onkeyup est appelé
      j3pElement(inputId).onkeyup = function (ev) {
        onkeyup(ev, parcours)
      }
      focusIfExists(inputId)
      parcours.finEnonce()
    }).catch(j3pShowError)
  }

  function initDom () {
    renderMathInDocument({ renderAccessibleContent: '' })
    if (sq.titre) parcours.afficheTitre(sq.titre)

    j3pDiv(parcours.zones.MG, {
      id: 'conteneur',
      contenu: '',
      style: parcours.styles.etendre('petit.enonce', { padding: '6px' })
    })
    j3pDiv('conteneur', 'enonce', '')
    j3pDiv('conteneur', { id: 'formules', contenu: '', style: parcours.styles.petit.enonce }) // Contient le formules entrées par l’élève
    j3pDiv('conteneur', 'info', '')
    afficheNombreEssaisRestants()
    j3pDiv('conteneur', 'conteneurbouton', '')
    j3pAjouteBouton('conteneurbouton', 'boutonrecopier', 'MepBoutonsRepere', 'Recopier réponse précédente',
      recopierReponse)
    j3pElement('boutonrecopier').style.display = 'none'

    j3pDiv('conteneur', 'editeur', '') // Le div qui contiendra l’éditeur MathQuill
    $('#editeur').css('padding-top', '10px') // Pour laisser un peu de place au-dessus de l’éditeur
    afficheMathliveDans('editeur', 'expression', '$f(x)$ = &1&', {
      charset: sq.charset,
      listeBoutons: sq.listeBoutons
    })
    if (sq.charset !== '') {
      mathliveRestrict(inputId, sq.charset)
    }
    j3pDiv('conteneur', 'boutonsmathquill', '')
    $('#boutonsmathquill').css('display', 'inline-block')
    $('#boutonsmathquill').css('padding-top', '10px') // Pour laisser un peu de place au-dessus des boutons

    j3pPaletteMathquill('boutonsmathquill', inputId, { liste: sq.listeBoutons })
    j3pAddElt('conteneur', 'div', '', { id: 'divSolution' })
    $('#divSolution').css('display', 'none')
    j3pDiv('conteneur', 'divmtg32', '')
    j3pCreeSVG('divmtg32', { id: 'mtg32svg', width: sq.width, height: sq.height })
    const style = parcours.styles.petit.correction
    style.marginTop = '1.5em'
    style.marginLeft = '0.5em'
    j3pAddElt(parcours.zonesElts.MD, 'div', '', { id: 'correction', style })
  }
  switch (this.etat) {
    case 'enonce':
      // code exécuté au lancement de la section
      if (this.debutDeLaSection) {
        // Construction de la page
        this.construitStructurePage('presentation1bis')
        this.validOnEnter = false // ex donneesSection.touche_entree
        sq.nbexp = 0
        sq.reponse = -1
        // compteur. Si   numEssai > nbchances, on corrige
        sq.numEssai = 1
        sq.nbEssais = parseInt(parcours.donneesSection.nbEssais)
        sq.nbchances = parseInt(parcours.donneesSection.nbchances)
        if (sq.nbchances > sq.nbEssais) sq.nbchances = sq.nbEssais

        sq.listeBoutons = ['fraction', 'puissance']
        // faut gérer sous-dossier ou pas…
        const sep = this.donneesSection.ex.includes('/') ? '-' : '/'
        j3pImporteAnnexe('squelettes-mtg32' + sep + this.donneesSection.ex + '.js').then(function (datasSection) {
          // console.log("initsection");
          // Attention, le this devient window...
          sq.txt = datasSection.txt
          sq.entete = datasSection.entete
          sq.symbexact = datasSection.symbexact
          sq.symbnonexact = datasSection.symbnonexact
          sq.numero = datasSection.numero // Le nombre de paramètres LaTeX dans le texte
          let s = datasSection.consigne1
          sq.consigne1 = (s !== undefined) ? s : ''
          s = datasSection.consigne2
          sq.consigne2 = (s !== undefined) ? s : ''
          s = datasSection.consigne3
          sq.consigne3 = (s !== undefined) ? s : ''
          s = datasSection.consigne4
          sq.consigne4 = (s !== undefined) ? s : ''
          sq.titre = datasSection.titre
          sq.width = Number(datasSection.width ?? 750)
          sq.height = Number(datasSection.height ?? 500)
          sq.param = datasSection.param // Chaine contenant les paramètres autorisés
          s = datasSection.charset // Le set de caractères utilisés dans l’éditeur
          sq.charset = (s !== undefined) ? s : ''
          initMtg()
        }).catch(error => {
          console.error(error)
          j3pShowError('Impossible de charger les données de cette exercice')
        })
        // return;
      } else {
        j3pEmpty('correction')
        cacheSolution()
        sq.numEssai = 1
        sq.mtgAppLecteur.removeDoc('mtg32svg')
        sq.mtgAppLecteur.addDoc('mtg32svg', sq.txt, true)
        if (sq.aleat) {
          for (let i = 1; i <= sq.nbParamAleat; i++) {
            sq.mtgAppLecteur.giveFormula2('mtg32svg', 'r' + i, sq['tab' + i][this.questionCourante - 1])
          }
        }
        if (sq.param !== undefined) {
          const ch = 'abcdefghjklmnpqr'
          for (let i = 0; i < ch.length; i++) {
            const car = ch.charAt(i)
            if (sq.param.indexOf(car) !== -1) {
              const par = parcours.donneesSection[car]
              if (par !== 'random') sq.mtgAppLecteur.giveFormula2('mtg32svg', car, par)
            }
          }
        }

        sq.mtgAppLecteur.calculate('mtg32svg', true) // true pour que les calculs alèatoires soient rꪮitialisés
        j3pEmpty('enonce')
        j3pEmpty('formules')
        sq.nbexp = 0
        sq.reponse = -1
        const nbParam = parseInt(sq.numero)
        const t = ['a', 'b', 'c', 'd']
        const param = {}
        for (let i = 0; i < nbParam; i++) {
          const code = sq.mtgAppLecteur.getLatexCode('mtg32svg', i)
          param[t[i]] = code
        }

        param.e = String(sq.nbEssais)
        const st = (sq.indicationDenom ? sq.consigne2 : sq.consigne3) + sq.consigne4
        afficheMathliveDans('enonce', 'texte', sq.consigne1 + st, param)
        $('#editeur').css('display', 'block')
        $('#boutonsmathquill').css('display', 'inline-block')

        // On vide le contenu de l’éditeur MathQuill et on lui donne le focus.
        j3pElement(inputId).value = ''
        focusIfExists(inputId)
        sq.mtgAppLecteur.display('mtg32svg')
        j3pElement('info').style.display = 'block'
        afficheNombreEssaisRestants()
        this.finEnonce()
      }

      break // case "enonce":

    case 'correction': {
      let bilanreponse = ''
      // On regarde si le champ d’édition est rempli même si l’utilisateur
      // n’a pas appuyé sur la touche Entrée.
      const ch = getMathliveValue(inputId)
      if (ch !== '') {
        const repcalcul = traiteMathlive(ch)
        const valide = sq.mtgAppLecteur.syntaxValidation('mtg32svg', 'rep', repcalcul, true)
        if (valide) validation(this, false)
        else bilanreponse = 'incorrect'
      }
      const res1 = sq.reponse
      if (bilanreponse !== 'incorrect') {
        if (res1 === 1) {
          bilanreponse = 'exact'
        } else {
          if (res1 === 2) bilanreponse = 'reduitpasfini'
          else if (res1 === 3) bilanreponse = 'pasreduit'
          else bilanreponse = 'erreur'
        }
      }
      // A cause de la limite de temps :
      if (this.isElapsed) { // limite de temps
        this._stopTimer()
        j3pElement('correction').style.color = this.styles.cfaux
        j3pElement('boutonrecopier').style.display = 'none'
        $('#editeur').css('display', 'none')
        $('#boutonsmathquill').css('display', 'none')
        sq.mtgAppLecteur.executeMacro('mtg32svg', 'solution')
        afficheSolution(false)
        j3pElement('correction').innerHTML = tempsDepasse
        j3pElement('correction').innerHTML += '<br>La solution se trouve ci-contre.'
        this.etat = 'navigation'
        this.sectionCourante()
      } else {
        if (bilanreponse === 'incorrect') {
          j3pElement('correction').style.color = this.styles.cfaux
          j3pElement('correction').innerHTML = 'Réponse incorrecte'
        } else {
          // Bonne réponse
          if (bilanreponse === 'exact') {
            this._stopTimer()
            sq.mtgAppLecteur.setActive('mtg32svg', false)
            this.score++
            j3pElement('correction').style.color = this.styles.cbien
            j3pElement('correction').innerHTML = cBien
            j3pElement('boutonrecopier').style.display = 'none'
            sq.mtgAppLecteur.executeMacro('mtg32svg', 'solution')
            afficheSolution(true)
            j3pElement('info').style.display = 'none'
            $('#editeur').css('display', 'none')
            $('#boutonsmathquill').css('display', 'none')
            this.etat = 'navigation'
            this.sectionCourante()
          } else {
            j3pElement('correction').style.color = this.styles.cfaux
            sq.numEssai++
            if (bilanreponse === 'reduitpasfini') {
              j3pElement('correction').innerHTML = 'La réduction est bonne mais la réponse n’est pas donnée sous la forme demandée.'
            } else {
              if (bilanreponse === 'pasreduit') j3pElement('correction').innerHTML = 'L’expression n’est pas écrite sous la forme demandée.'
              else j3pElement('correction').innerHTML = cFaux
            }
            // S’il y a plus de deux chances,on appelle à nouveau le bouton Valider
            if ((sq.nbexp < sq.nbEssais) &&
              (sq.numEssai <= sq.nbchances)) {
              j3pElement('correction').innerHTML += '<br>' + '<br>' + essaieEncore
              sq.mtgAppLecteur.setActive('mtg32svg', true)
              afficheNombreEssaisRestants()
            } else {
              // Erreur au nème essai
              this._stopTimer()
              j3pElement('info').style.display = 'none'
              j3pElement('boutonrecopier').style.display = 'none'
              sq.mtgAppLecteur.executeMacro('mtg32svg', 'solution')
              afficheSolution(false)
              $('#editeur').css('display', 'none')
              $('#boutonsmathquill').css('display', 'none')
              sq.mtgAppLecteur.setActive('mtg32svg', false)
              // j3pElement("correction").innerHTML+='<br>' + regardeCorrection;
              j3pElement('correction').innerHTML += '<br><br>La solution se trouve ci-contre.'
              this.etat = 'navigation'
              this.sectionCourante()
            }
          }
        }
      }
      // Obligatoire
      this.finCorrection()
      break // case "correction":
    }

    case 'navigation':
      if (this.sectionTerminee()) {
        // On détermine la phrase d’état renvoyée par la section, ici c’est un score…
        this.parcours.pe = this.score / this.donneesSection.nbitems
      } else {
        this.etat = 'enonce'
      }
      this.finNavigation(true)
      break // case "navigation":
  }
}
