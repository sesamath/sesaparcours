import { j3pAddElt, j3pDiv, j3pElement, j3pEmpty, j3pImporteAnnexe, j3pShowError } from 'src/legacy/core/functions'
import { getMtgApp } from 'src/lib/outils/mathgraph'
import { j3pCreeSVG } from 'src/legacy/core/functionsSvg'
import { MathfieldElement, renderMathInDocument } from 'mathlive'
import { setMathliveCss } from 'src/lib/outils/mathlive/mathlivecss'
import { afficheMathliveDans } from 'src/lib/outils/mathlive/display'
import textesGeneriques from 'src/lib/core/textes'

const { cBien, cFaux, essaieEncore, regardeCorrection, tempsDepasse } = textesGeneriques

/*
    Yves Biton
    Janvier 2019. Revu janvier 2004 pour passage Mathlive

    squelettemtg32_Placer_Point_Dans_Rep
    Squelette demandant de déplacer un point dans un repère pour qu’il ait des coordonnées données.
    On peut éventuellement spécifier des paramètres  x et y pour imposer une des coordonnées ou les deux.

    Si la figure contient un calcul nbvar , alors dans le cas ou nbrepetitions > 1, on donne pour i compris entre 1 et npParamAleat
    au calcul de la figure nommé "r"+i (par exemple r1 et r2 si nbParamAleat vaut 2) des valeurs comprises entre 0 et "nbcas"+i - 1
    toutes différentes (sauf si nbrepetitions > b=nbcas auquel cas au bout de nbcas répétitions il y aura nécessairement une valeur de r déjà prise.
    Le fichier annexe doit contenir les variables suivantes :
    txt: Chaîne de caractères contenant le code Base64 de la figure.
    consigne1 : Consigne à afficher si on a demandé un repère sans vecteurs.
    consigne2 : Consigne à afficher si on a demandé un repère avec vecteurs.

*/

// on définit le dossier des fonts mathlive (il faut le faire très tôt,
// ici c’est bon avec pnpm start mais trop tard pour un build (cf src/lib/player/index.js de la branche draftPlayer)
if (MathfieldElement) { // undefined dans les tests avec happy-dom
  MathfieldElement.fontsDirectory = '/static/mathlive/fonts'
  MathfieldElement.soundsDirectory = '/static/mathlive/sounds'
}

export const params = {
  /**
   * Ce qui est paramétrable dans l’éditeur de graphe
   * @type {LegacySectionParamsParametres}
   */
  parametres: [
    ['nbrepetitions', 4, 'entier', 'aucun'],
    ['limite', 0, 'entier', 'Temps disponible par répétition, en secondes'],
    ['nbchances', 1, 'entier', 'Nombre d’erreurs possibles (pour tenter de les corriger)'],
    ['x', 'random', 'string', 'Valeur de a'],
    ['y', 'random', 'string', 'Valeur de b'],
    ['typeRepere', 1, 'entier', 'Type du repère utilisé :\n1 pour repère orthonormal\n2 pour repère orthogonal\n3 pour repère oblique\n4 pour repère oblique normé'],
    ['avecGraduations', true, 'boolean', 'true pour que le repère soit gradué'],
    ['avecNomsIJ', true, 'boolean', 'true pour que les noms des points I et J sur les axes soient visibles'],
    ['avecVecteurs', false, 'boolean', 'true pour que le repère ait des vecteurs sur chaque axe'],
    ['ex', 'Placer_M_Dans_Rep', 'string', 'Nom du fichier annexe (xxx => squelettes-mtg32/xxx.js et xxx/yyy => squelettes-mtg32-xxx/yyy.js)']
  ]
}

/**
 * section squelettemtg32_Placer_Point_Dans_Rep
 * @this {Parcours}
 */
export default function main () {
  const parcours = this
  const sq = this.storage
  let code
  function initMtg () {
    setMathliveCss()
    const mtgOptions = {
      // player only, sans figure initiale ni svg, on gère ça dans initMtg et modif_fig
      loadCoreWithMathJax: true
    }
    getMtgApp('mtg32svg', {}, mtgOptions).then((mtgAppLecteur) => {
      sq.mtgAppLecteur = mtgAppLecteur
      let par, car, i, j, k, nbrep, ar, tir, nb
      sq.mtgAppLecteur.removeAllDoc()
      sq.mtgAppLecteur.addDoc('mtg32svg', sq.txt, true)
      sq.mtgAppLecteur.calculate('mtg32svg', false)
      const ch = 'xy'
      const nbvar = sq.mtgAppLecteur.valueOf('mtg32svg', 'nbvar')
      if (nbvar !== -1) {
        nbrep = parcours.donneesSection.nbrepetitions
        sq.aleat = true
        sq.nbParamAleat = nbvar
        for (i = 1; i <= nbvar; i++) {
          const nbcas = sq.mtgAppLecteur.valueOf('mtg32svg', 'nbcas' + i)
          nb = Math.max(nbrep, nbcas)
          ar = []
          for (j = 0; j < nb; j++) ar.push(j % nbcas)
          sq['tab' + i] = []
          for (k = 0; k < nbrep; k++) {
            tir = Math.floor(Math.random() * ar.length)
            sq['tab' + i].push(ar[tir])
            ar.splice(tir, 1)
          }
          sq.mtgAppLecteur.giveFormula2('mtg32svg', 'r' + i, sq['tab' + i][0])
        }
      } else { sq.aleat = false }

      for (i = 0; i < ch.length; i++) {
        car = ch.charAt(i)
        par = parcours.donneesSection[car]
        if (par !== 'random') sq.mtgAppLecteur.giveFormula2('mtg32svg', car, par)
      }

      sq.mtgAppLecteur.calculate('mtg32svg', true) // true pour que les calculs aléatoires soient réinitialisés
      sq.mtgAppLecteur.display('mtg32svg')
      if (sq.avecNomsIJ) { sq.mtgAppLecteur.executeMacro('mtg32svg', 'avecNomsIJ') } else {
        sq.mtgAppLecteur.executeMacro('mtg32svg', 'sansNomsIJ')
        if (!sq.avecGraduations) sq.mtgAppLecteur.executeMacro('mtg32svg', 'voirUnites')
      }
      if (sq.avecVecteurs) sq.mtgAppLecteur.executeMacro('mtg32svg', 'avecVecteurs')
      else sq.mtgAppLecteur.executeMacro('mtg32svg', 'sansVecteurs')
      if (sq.avecGraduations) sq.mtgAppLecteur.executeMacro('mtg32svg', 'avecGraduations')
      else sq.mtgAppLecteur.executeMacro('mtg32svg', 'sansGraduations')
      sq.mtgAppLecteur.executeMacro('mtg32svg', 'repere' + sq.typeRepere)
      j3pEmpty('enonce')
      j3pEmpty('complement')
      const code = sq.mtgAppLecteur.getLatexCode('mtg32svg', 0)
      const param = { a: code }
      afficheMathliveDans('enonce', 'texte',
        sq.avecVecteurs
          ? sq.consigne2
          : sq.consigne1 + (sq.avecNomsIJ ? '(O;I,J)' : 'ci-dessous'),
        param
      )
      parcours.finEnonce()
    }).catch(j3pShowError)
  }

  function initDom () {
    renderMathInDocument({ renderAccessibleContent: '' })
    if (sq.titre) parcours.afficheTitre(sq.titre)

    j3pDiv(parcours.zones.MG, {
      id: 'conteneur',
      contenu: '',
      style: parcours.styles.etendre('toutpetit.enonce', { padding: '6px' })
    })
    j3pDiv('conteneur', 'enonce', '')
    j3pDiv('conteneur', 'complement', '') // Pour éventuellement des compléments d’explication
    // j3pDiv("conteneur", "explicationFaux", "");
    j3pDiv('conteneur', 'explicationFaux', '')

    j3pDiv('conteneur', 'divmtg32', '')
    j3pCreeSVG('divmtg32', {
      id: 'mtg32svg',
      width: 700,
      height: 560
    })

    initMtg()
    const style = parcours.styles.petit.correction
    style.marginTop = '1.5em'
    style.marginLeft = '0.5em'
    j3pAddElt(parcours.zonesElts.MD, 'div', '', { id: 'correction', style })
  }

  switch (this.etat) {
    case 'enonce':
      // code exécuté au lancement de la section
      if (this.debutDeLaSection) {
        // Construction de la page
        this.construitStructurePage('presentation1bis')
        this.validOnEnter = false // ex donneesSection.touche_entree

        sq.reponse = -1
        sq.avecVecteurs = parcours.donneesSection.avecVecteurs
        sq.avecGraduations = parcours.donneesSection.avecGraduations
        sq.avecNomsIJ = parcours.donneesSection.avecNomsIJ
        sq.typeRepere = parcours.donneesSection.typeRepere
        // compteur. Si   numEssai > nbchances, on corrige
        sq.numEssai = 1
        sq.nbchances = parseInt(parcours.donneesSection.nbchances)
        // faut gérer sous-dossier ou pas…
        const sep = this.donneesSection.ex.includes('/') ? '-' : '/'
        j3pImporteAnnexe('squelettes-mtg32' + sep + this.donneesSection.ex + '.js').then(function (datasSection) {
          // console.log("initsection");
          // Attention, le this devient window...
          sq.txt = datasSection.txt
          let s = datasSection.consigne1
          sq.consigne1 = (s !== undefined) ? s : ''
          s = datasSection.consigne2
          sq.consigne2 = (s !== undefined) ? s : ''
          s = datasSection.explicationFaux
          sq.explicationFaux = (s !== undefined) ? s : ''
          s = datasSection.confusionCoord
          sq.confusionCoord = (s !== undefined) ? s : ''
          s = datasSection.complement
          sq.complement = (s !== undefined) ? s : ''
          sq.titre = datasSection.titre
          sq.param = 'xy' // Chaine contenant les paramètres autorisés
          initDom()
        }).catch(error => {
          console.error(error)
          j3pShowError('Impossible de charger les données de cette exercice')
        })
        // return;
      } else {
        j3pEmpty('correction')
        sq.numEssai = 1
        sq.mtgAppLecteur.removeDoc('mtg32svg')
        sq.mtgAppLecteur.addDoc('mtg32svg', sq.txt, true)

        if (sq.aleat) {
          for (let i = 1; i <= sq.nbParamAleat; i++) {
            sq.mtgAppLecteur.giveFormula2('mtg32svg', 'r' + i, sq['tab' + i][this.questionCourante - 1])
          }
        }
        const ch = 'xy'
        for (let i = 0; i < ch.length; i++) {
          const car = ch.charAt(i)
          const par = parcours.donneesSection[car]
          if (par !== 'random') sq.mtgAppLecteur.giveFormula2('mtg32svg', car, par)
        }

        sq.mtgAppLecteur.calculate('mtg32svg', true) // true pour que les calculs aléatoires soient réinitialisés
        sq.mtgAppLecteur.display('mtg32svg')
        if (sq.avecNomsIJ) { sq.mtgAppLecteur.executeMacro('mtg32svg', 'avecNomsIJ') } else {
          sq.mtgAppLecteur.executeMacro('mtg32svg', 'sansNomsIJ')
          if (!sq.avecGraduations) sq.mtgAppLecteur.executeMacro('mtg32svg', 'voirUnites')
        }
        if (sq.avecVecteurs) sq.mtgAppLecteur.executeMacro('mtg32svg', 'avecVecteurs')
        else sq.mtgAppLecteur.executeMacro('mtg32svg', 'sansVecteurs')
        if (sq.avecGraduations) sq.mtgAppLecteur.executeMacro('mtg32svg', 'avecGraduations')
        else sq.mtgAppLecteur.executeMacro('mtg32svg', 'sansGraduations')
        sq.mtgAppLecteur.executeMacro('mtg32svg', 'repere' + sq.typeRepere)
        j3pEmpty('enonce')
        j3pEmpty('complement')
        j3pEmpty('explicationFaux')
        code = sq.mtgAppLecteur.getLatexCode('mtg32svg', 0)
        const param = { a: code }
        afficheMathliveDans('enonce', 'texte',
          sq.avecVecteurs
            ? sq.consigne2
            : sq.consigne1 + (sq.avecNomsIJ ? '(O;I,J)' : 'ci-dessous'),
          param
        )
        this.finEnonce()
      }

      break // case "enonce":

    case 'correction': {
      let bilanreponse = ''
      const res1 = sq.mtgAppLecteur.valueOf('mtg32svg', 'reponse')
      if (res1 === 1) {
        bilanreponse = 'exact'
      } else {
        bilanreponse = 'erreur'
      }

      // Bonne réponse
      if (bilanreponse === 'exact') {
        this._stopTimer()
        sq.mtgAppLecteur.setActive('mtg32svg', false)
        this.score++
        j3pElement('correction').style.color = this.styles.cbien
        j3pElement('correction').innerHTML = cBien
        code = sq.mtgAppLecteur.getLatexCode('mtg32svg', 2) // Le troisième LaTeX de la figure
        afficheMathliveDans('complement', 'comp', sq.complement,
          {
            style: { color: this.styles.cbien },
            a: code
          })
        sq.mtgAppLecteur.executeMacro('mtg32svg', 'masquerNomM')
        sq.mtgAppLecteur.executeMacro('mtg32svg', 'solution')

        this.etat = 'navigation'
        this.sectionCourante()
      } else {
        // Pas de bonne réponse

        // A cause de la limite de temps :
        if (this.isElapsed) { // limite de temps
          this._stopTimer()
          sq.mtgAppLecteur.setActive('mtg32svg', false)
          j3pElement('correction').style.color = this.styles.cfaux
          sq.mtgAppLecteur.executeMacro('mtg32svg', 'solution')
          j3pElement('correction').innerHTML = tempsDepasse
          j3pElement('correction').innerHTML += '<br>' + regardeCorrection
          // j3pElement("correction").innerHTML+="<br>La solution était "+this.stockage[0]
          /*
           *   RECOPIER LA CORRECTION ICI !
           */
          this.etat = 'navigation'
          this.sectionCourante()
        } else {
          // Réponse fausse :
          j3pElement('correction').style.color = this.styles.cfaux
          j3pElement('correction').innerHTML = cFaux
          sq.numEssai++
          const confusionCoord = sq.mtgAppLecteur.valueOf('mtg32svg', 'confusionCoord')
          if (confusionCoord) j3pElement('correction').innerHTML += '<br>' + sq.confusionCoord
          // S’il y a plus de deux chances,on appelle à nouveau le bouton Valider
          if (sq.numEssai <= sq.nbchances) {
            j3pElement('correction').innerHTML += '<br>' + essaieEncore
          } else {
            // Erreur au nème essai
            this._stopTimer()
            sq.mtgAppLecteur.setActive('mtg32svg', false)
            if (sq.complement !== '') {
              code = sq.mtgAppLecteur.getLatexCode('mtg32svg', 2) // Le troisième LaTeX de la figure
              afficheMathliveDans('complement', 'comp', sq.complement,
                {
                  style: { color: this.styles.cfaux },
                  a: code
                })
            }
            code = sq.mtgAppLecteur.getLatexCode('mtg32svg', 1)

            afficheMathliveDans('explicationFaux', 'explication', sq.explicationFaux,
              {
                style: { color: this.styles.cfaux },
                a: code
              })

            sq.mtgAppLecteur.executeMacro('mtg32svg', 'voirMFaux')
            sq.mtgAppLecteur.executeMacro('mtg32svg', 'solution')
            j3pElement('correction').innerHTML += '<br>' + regardeCorrection
            this.etat = 'navigation'
            this.sectionCourante()
          }
        }
      }
      // Obligatoire
      this.finCorrection()
      break // case "correction":
    }

    case 'navigation':

      if (!this.sectionTerminee()) {
        this.etat = 'enonce'
      }
      this.finNavigation()
      break // case "navigation":
  }
}
