import $ from 'jquery'
import { j3pAddElt, j3pAjouteBouton, j3pDetruit, j3pDiv, j3pElement, j3pEmpty, j3pFocus, j3pImporteAnnexe, j3pNotify, j3pPaletteMathquill, j3pShowError, j3pValeurde } from 'src/legacy/core/functions'
import { cleanLatexForMq, j3pAffiche, mqRestriction } from 'src/lib/mathquill/functions'
import { unLatexifyAndAddImplicitMult } from 'src/legacy/sections/squelettes/helpersMtg'
import { getMtgApp } from 'src/lib/outils/mathgraph'
import { j3pCreeSVG } from 'src/legacy/core/functionsSvg'
import textesGeneriques from 'src/lib/core/textes'

const { cBien, cFaux, essaieEncore, tempsDepasse } = textesGeneriques

/*
    Yves Biton
    Octobre 2018

    http://localhost:8081/?graphe=[1,"squelettemtg32_Calc_Frac",[{pe:">=0",nn:"fin",conclusion:"fin"},{f:"Fact_ax_Carre_Moins_cCarre_Ex1"}]];
    squelettemtg32_Calc_Frac
    Squelette demandant de faire un calcul fractionnaire et de le donner sous une forme simplifiée.
    On peut éventuellement spécifier des paramètres  n,d,a,b et c.

    Si ex contient un paramètre nbParamAleat , alors dans le cas ou nbrepetitions > 1, on donne pour i compris entre 1 et npParamAleat
    au calcul de la figure nommé "r"+i (par exemple r1 et r2 si nbParamAleat vaut 2) des valeurs comprises entre 0 et "nbcas"+i - 1
    toutes différentes (sauf si nbrepetitions > b=nbcas auquel cas au bout de nbcas répétitions il y aura nécessairement une valeur de r déjà prise.

*/

export const params = {
  outils: ['mathquill'],
  /**
   * Ce qui est paramétrable dans l’éditeur de graphe
   * @type {LegacySectionParamsParametres}
   */
  parametres: [
    ['nbrepetitions', 2, 'entier', 'aucun'],
    ['limite', 0, 'entier', 'Temps disponible par répétition, en secondes'],
    ['nbEssais', 6, 'entier', 'Nombre d’essais maximum autorisés'],
    ['boutonRecopie', true, 'boolean', 'true pour avoir un bouton de recopie de la réponse précédente'],
    ['infoParam', 'Information sur le rôle des paramètres', 'string', 'Information sur l’utilisation des paramètres suivants'],
    ['a', 'random', 'string', 'Valeur de a'],
    ['b', 'random', 'string', 'Valeur de b'],
    ['c', 'random', 'string', 'Valeur de c'],
    ['d', 'random', 'string', 'Valeur de d'],
    ['e', 'random', 'string', 'Valeur de e'],
    ['f', 'random', 'string', 'Valeur de f'],
    ['ex', 'College_Div_Frac_Frac', 'string', 'Nom du fichier annexe (xxx => squelettes-mtg32/xxx.js et xxx/yyy => squelettes-mtg32-xxx/yyy.js)']
  ]
}

/**
 * section CalcFrac
 * @this {Parcours}
 */
export default function main () {
  const parcours = this
  const sq = this.storage

  const onkeyup = () => {
    if (sq.marked) demarqueEditeurPourErreur()
  }

  /**
   * Fonction extrayant d’un affichage LaTeX de la figure les lignes de texte et les transformant
   * en une chaîne affichable par MathQuill
   * @param tagLatex : le tag de l’affichage LaTeX de la figure
   * @return {string}
   */
  function extraitDeLatexPourMathQuill (tagLatex) {
    const list = sq.mtgAppLecteur.getList('mtg32svg')
    const latex = list.getByTag(tagLatex)
    if (latex === null || latex.className !== 'CLatex') return ''
    return cleanLatexForMq(latex.chaineLatex)
  }

  function afficheNombreEssaisRestants () {
    j3pEmpty('info')
    const nbe = sq.nbEssais - sq.numEssai + 1
    if (nbe === 1) {
      j3pAffiche('info', 'texteinfo', 'Il reste un essai.', { style: { color: '#7F007F' } })
    } else {
      j3pAffiche('info', 'texteinfo', 'Il reste ' + nbe + ' essais.', { style: { color: '#7F007F' } })
    }
  }

  /**
   * Fonction affichant la solution s’il existe un affichage LaTeX de tag solution
   * @param{boolean} bexact true si l’élève a entré la bonne réponse, false sinon
   */
  function afficheSolution (bexact) {
    $('#divSolution').css('display', 'block').html('')
    const ch = extraitDeLatexPourMathQuill('solution')
    if (ch !== '') {
      j3pAffiche('divSolution', 'solution', ch, { style: { color: bexact ? 'green' : 'blue' } })
    }
  }

  function cacheSolution () {
    $('#divSolution').css('display', 'none').html('')
  }

  function marqueEditeurPourErreur () {
    sq.marked = true
    $('#expressioninputmq1').css('background-color', '#FF9999')
  }
  function demarqueEditeurPourErreur () {
    sq.marked = false
    $('#expressioninputmq1').css('background-color', '#FFFFFF')
  }

  function validationEditeur () {
    let res = true
    const rep = j3pValeurde('expressioninputmq1')
    if (rep === '') {
      res = false
    } else {
      const repcalcul = unLatexifyAndAddImplicitMult(sq.mtgAppLecteur, rep)
      const valide = sq.mtgAppLecteur.syntaxValidation('mtg32svg', 'rep', repcalcul, true)
      if (!valide) res = false
    }
    if (!res && rep !== '') {
      marqueEditeurPourErreur()
      j3pFocus('expressioninputmq1')
    }
    return res
  }

  function montreEditeur (bVisible) {
    if (bVisible) $('#info').css('display', 'block')
    $('#editeur').css('display', bVisible ? 'block' : 'none')
    if (sq.btnFrac) $('#boutonsmathquill').css('display', bVisible ? 'block' : 'none')
    // On vide le contenu de l’éditeur MathQuill et on lui donne le focus.
    $('#expressioninputmq1').mathquill('latex', ' ')
    j3pFocus('expressioninputmq1')
  }

  function videEditeur () {
    $('#expressioninputmq1').mathquill('latex', ' ')
    j3pFocus('expressioninputmq1') // focus nécessaire sinon éditeur riquiqui
  }

  function afficheReponse (bilan, depasse) {
    let coul
    if (bilan === 'exact') {
      coul = parcours.styles.cbien
    } else {
      if (bilan === 'faux') {
        coul = parcours.styles.cfaux
      } else { // Réponse exacte mais calcul pas fini
        if (depasse) {
          coul = parcours.styles.cfaux
        } else {
          coul = '#0000FF'
        }
      }
    }
    const num = sq.numEssai
    const idrep = 'exp' + num
    let chdeb = ''
    if (num > 2) chdeb = '<br>'

    if (bilan === 'faux') {
      j3pAffiche('formules', idrep, chdeb + '$' + sq.aCalculer + ' ' + '\\ne' +
        ' ' + sq.rep + '$', {
        styletexte: {
          couleur: coul
        }
      })
    } else {
      j3pAffiche('formules', idrep, chdeb + '$' + sq.aCalculer + ' ' + ' = ' +
        ' ' + sq.rep + '$', {
        styletexte: {
          couleur: coul
        }
      })
    }
    if (sq.texteFaute) {
      j3pAffiche('formules', idrep + 'faute', '<br>' + sq.texteFaute, {
        styletexte: {
          couleur: coul
        }
      })
    }
  }

  function validation () {
    if (sq.numEssai >= 1) {
      j3pElement('boutonrecopier').style.display = 'block'
    }
    sq.rep = j3pValeurde('expressioninputmq1')
    sq.repMathQuill = unLatexifyAndAddImplicitMult(sq.mtgAppLecteur, sq.rep)
    const contientMult = sq.repMathQuill.indexOf('*') !== -1
    sq.mtgAppLecteur.giveFormula2('mtg32svg', 'rep', sq.repMathQuill)
    sq.mtgAppLecteur.calculate('mtg32svg', false)
    if (sq.indicationfaute) {
      const faute = sq.mtgAppLecteur.valueOf('mtg32svg', 'faute')
      if (faute === 1) {
        sq.mtgAppLecteur.executeMacro('mtg32svg', 'voirFaute')
      } else {
        sq.mtgAppLecteur.executeMacro('mtg32svg', 'masquerFaute')
      }
    }
    const reponse = sq.mtgAppLecteur.valueOf('mtg32svg', 'reponse')
    sq.resolu = (reponse === 1) && !contientMult
    sq.exact = reponse === 2 || ((reponse === 1) && contientMult)
    sq.texteFaute = ''
    if (sq.indicationfaute && !sq.exact && !sq.resolu) {
      sq.texteFaute = extraitDeLatexPourMathQuill('faute')
    }
  }

  function recopierReponse () {
    demarqueEditeurPourErreur()
    $('#expressioninputmq1').mathquill('latex', sq.rep)
    j3pFocus('expressioninputmq1')
  }

  function initMtg () {
    const mtgOptions = {
      // player only, sans figure initiale ni svg, on gère ça dans initMtg et modif_fig
      loadCoreWithMathJax: true
    }
    getMtgApp('mtg32svg', {}, mtgOptions).then((mtgAppLecteur) => {
      sq.mtgAppLecteur = mtgAppLecteur
      initDom()
      sq.marked = false
      let code, par, car, i, j, k, nbrep, ar, tir, nb
      sq.mtgAppLecteur.removeAllDoc()
      sq.mtgAppLecteur.addDoc('mtg32svg', sq.txt, true)
      sq.mtgAppLecteur.calculate('mtg32svg', false)
      sq.nbexp = 0
      sq.reponse = -1
      const ch = 'abcdefghjklmnpqr'
      if (sq.param !== undefined) {
        for (i = 0; i < ch.length; i++) {
          car = ch.charAt(i)
          if (sq.param.indexOf(car) !== -1) {
            par = parcours.donneesSection[car]
            if (par !== 'random') sq.mtgAppLecteur.giveFormula2('mtg32svg', car, par)
          }
        }
        sq.mtgAppLecteur.calculate('mtg32svg', true)
      }
      const nbvar = sq.mtgAppLecteur.valueOf('mtg32svg', 'nbvar')
      if (nbvar !== -1) {
        nbrep = parcours.donneesSection.nbrepetitions
        sq.aleat = true
        sq.nbParamAleat = nbvar
        for (i = 1; i <= nbvar; i++) {
          const nbcas = sq.mtgAppLecteur.valueOf('mtg32svg', 'nbcas' + i)
          nb = Math.max(nbrep, nbcas)
          ar = []
          for (j = 0; j < nb; j++) ar.push(j % nbcas)
          sq['tab' + i] = []
          for (k = 0; k < nbrep; k++) {
            tir = Math.floor(Math.random() * ar.length)
            sq['tab' + i].push(ar[tir])
            ar.splice(tir, 1)
          }
          sq.mtgAppLecteur.giveFormula2('mtg32svg', 'r' + i, sq['tab' + i][0])
        }
      } else {
        sq.aleat = false
      }

      sq.mtgAppLecteur.calculateAndDisplayAll(true)
      j3pElement('expressioninputmq1').onkeyup = onkeyup
      const nbParam = parseInt(sq.nbLatex)
      const t = ['a', 'b', 'c', 'd']
      const param = {}
      for (i = 0; i < nbParam; i++) {
        code = sq.mtgAppLecteur.getLatexCode('mtg32svg', i)
        if (i === 0) sq.aCalculer = code
        param[t[i]] = code
      }
      param.e = sq.nbEssais
      const st = sq.consigne1 + sq.consigne2 + sq.consigne3
      j3pAffiche('enonce', 'texte', st, param)
      j3pAffiche('debut', 'acalc', '$' + sq.aCalculer + '=$ ', param)
      sq.indicationfaute = parcours.donneesSection.indicationfaute // true si indication quand certaines fautes sont faites et appui sur Entrée
      j3pFocus('expressioninputmq1')
      parcours.finEnonce()
    }).catch(j3pShowError)
  }

  function initDom () {
    if (sq.titre) parcours.afficheTitre(sq.titre)

    j3pDiv(parcours.zones.MG, {
      id: 'conteneur',
      contenu: '',
      style: parcours.styles.etendre('toutpetit.enonce', { padding: '6px' })
    })
    j3pDiv('conteneur', 'enonce', '')
    j3pDiv('conteneur', {
      id: 'formules',
      contenu: '',
      style: parcours.styles.petit.enonce
    }) // Contient le formules entrées par l’élève
    if (sq.bigSize) $('#formules').css('font-size', '26px')
    j3pDiv('conteneur', 'info', '')
    afficheNombreEssaisRestants()
    j3pDiv('conteneur', 'conteneurbouton', '')
    j3pAjouteBouton('conteneurbouton', 'boutonrecopier', 'MepBoutonsRepere', 'Recopier réponse précédente',
      recopierReponse)
    j3pElement('boutonrecopier').style.display = 'none'
    j3pDiv('conteneur', 'editeur', '') // Le div qui contiendra l’éditeur MathQuill
    const $editeur = $('#editeur')
    if (sq.bigSize) $editeur.css('font-size', '30px')
    $editeur.css('padding-top', '10px') // Pour laisser un peu de place au-dessus de l’éditeur
    j3pAffiche('editeur', 'debut', '', {})
    j3pAffiche('editeur', 'expression', '&1&', { inputmq1: {} })
    if (sq.charset !== '') {
      mqRestriction('expressioninputmq1', sq.charset, {
        commandes: ['fraction'],
        boundingContainer: parcours.zonesElts.MG
      })
    }
    j3pDiv('conteneur', 'boutonsmathquill', '')
    $('#boutonsmathquill').css('display', 'inline-block').css('padding-top', '10px') // Pour laisser un peu de place au-dessus des boutons

    if (sq.btnFrac) {
      j3pPaletteMathquill('boutonsmathquill', 'expressioninputmq1', { liste: ['fraction'], nomdiv: 'palette' })
      // Ligne suivante pour un bon alignement
      $('#palette').css('display', 'inline-block')
    } else {
      j3pDetruit('boutonsmathquill')
    }
    j3pAddElt('conteneur', 'div', '', { id: 'divSolution' })
    $('#divSolution').css('display', 'none')
    j3pDiv('conteneur', 'divmtg32', '')
    j3pCreeSVG('divmtg32', {
      id: 'mtg32svg',
      width: 700,
      height: 900
    })
    const style = parcours.styles.petit.correction
    style.marginTop = '1.5em'
    style.marginLeft = '0.5em'
    j3pAddElt(parcours.zonesElts.MD, 'div', '', { id: 'correction', style })
  }

  function _Donnees () {
    // "primaire" || "lycee" || "college"
    // Actuellement les dimensions sont identiques
    this.typesection = 'college'//

    // Nombre de répétitions de l’exercice
    this.nbrepetitions = 2
    this.nbetapes = 1
    this.nbitems = this.nbetapes * this.nbrepetitions

    this.ex = 'College_Div_Frac_Frac'
    // Si indication != "" alors un lien "Indication" s’affiche dans la zone Inférieure Gauche (this.zones.IG
    // une indication<br>de deux lignes...";
    this.indication = ''
    const st = 'abcdef'
    for (let i = 0; i < st.length; i++) this[st.charAt(i)] = 'random'
    this.nbEssais = 6
    this.boutonRecopie = true
    this.indicationfaute = true
    this.structure = 'presentation1bis'//  || "presentation2" || "presentation3"  || "presentation1bis"

    this.textes = {
      phrase1: 'Ecris '
    }
    this.titre = 'Titre de l’activité'

    this.pe = 0
  }

  switch (this.etat) {
    case 'enonce':
      // code exécuté au lancement de la section
      if (this.debutDeLaSection) {
        this.donneesSection = new _Donnees()
        // Construction de la page
        parcours.construitStructurePage('presentation1bis')

        this.surcharge()

        // Nombre d’essais dont dispose l’él§ve pour répondre à la question
        sq.numEssai = 1
        sq.nbEssais = parseInt(parcours.donneesSection.nbEssais)
        sq.simplifier = parcours.donneesSection.simplifier
        if (sq.simplifier === undefined) {
          sq.simplifier = true
        }
        // faut gérer sous-dossier ou pas…
        const sep = this.donneesSection.ex.includes('/') ? '-' : '/'
        j3pImporteAnnexe('squelettes-mtg32' + sep + this.donneesSection.ex + '.js').then(function (datasSection) {
          // console.log("initsection");
          // Attention, le this devient window...
          sq.txt = datasSection.txt
          sq.nbLatex = Number(datasSection.nbLatex ?? 0) // Le nombre de paramètres LaTeX dans le texte
          sq.btnFrac = Boolean(datasSection.btnFrac ?? true) // Bouton fraction par défaut
          sq.titre = datasSection.titre ?? ''
          sq.param = datasSection.param ?? '' // Chaine contenant les paramètres autorisés
          sq.consigne1 = datasSection.consigne1 ?? ''
          sq.consigne2 = datasSection.consigne2 ?? ''
          sq.consigne3 = datasSection.consigne3 ?? ''
          sq.charset = datasSection.charset ?? ''
          initMtg()
        }).catch(error => {
          console.error(error)
          j3pShowError('Impossible de charger les données de cette exercice')
        })
      } else {
        if (!sq.mtgAppLecteur) {
          j3pNotify('case enonce rappelé avant finEnonce')
          j3pShowError('Problème de chargement, il est encore en cours, c’est trop tôt pour rappeller un deuxième essai.')
          return
        }
        j3pEmpty('correction')
        cacheSolution()
        sq.numEssai = 1
        videEditeur()
        montreEditeur(true)
        sq.mtgAppLecteur.removeDoc('mtg32svg')
        sq.mtgAppLecteur.addDoc('mtg32svg', sq.txt, true)

        if (sq.param !== undefined) {
          const ch = 'abcdefghjklmnpqr'
          for (let i = 0; i < ch.length; i++) {
            const car = ch.charAt(i)
            if (sq.param.indexOf(car) !== -1) {
              const par = parcours.donneesSection[car]
              if (par !== 'random') sq.mtgAppLecteur.giveFormula2('mtg32svg', car, par)
            }
          }
          sq.mtgAppLecteur.calculate('mtg32svg', true) // true pour que les calculs alèatoires soient réinitialisés
        }

        if (sq.aleat) {
          for (let i = 1; i <= sq.nbParamAleat; i++) {
            sq.mtgAppLecteur.giveFormula2('mtg32svg', 'r' + i, sq['tab' + i][this.questionCourante - 1])
          }
        }

        sq.mtgAppLecteur.calculate('mtg32svg', true) // true pour que les calculs alèatoires soient réinitialisés
        j3pEmpty('enonce')
        j3pEmpty('debut')
        j3pEmpty('formules')
        j3pEmpty('info')
        sq.nbexp = 0
        sq.reponse = -1
        const nbParam = parseInt(sq.nbLatex)
        const t = ['a', 'b', 'c', 'd']
        const param = {}
        for (let i = 0; i < nbParam; i++) {
          const code = sq.mtgAppLecteur.getLatexCode('mtg32svg', i)
          if (i === 0) sq.aCalculer = code
          param[t[i]] = code
        }
        param.e = sq.nbEssais
        const st = sq.consigne1 + sq.consigne2 + sq.consigne3
        j3pAffiche('enonce', 'texte', st, param)
        j3pAffiche('debut', 'acalc', '$' + sq.aCalculer + '=$ ', param)
        afficheNombreEssaisRestants()
        j3pElement('info').style.display = 'block'
        sq.mtgAppLecteur.display('mtg32svg') // Pour tout réafficher
        // Pour revenir en haut de page
        try {
          window.location.hash = 'MepMG'
          j3pElement('MepMG').scrollIntoView()
        } catch (e) {
          console.error(e)
        }
        j3pFocus('expressioninputmq1')
        this.finEnonce()
      }

      break // case "enonce":

    case 'correction': {
      let bilanReponse = ''
      let simplificationPossible = false
      if (validationEditeur()) {
        validation()
        if (sq.resolu) {
          bilanReponse = 'exact'
        } else {
          if (sq.exact) {
            bilanReponse = 'exactPasFini'
            simplificationPossible = true
          } else {
            bilanReponse = 'faux'
          }
        }
      } else {
        bilanReponse = 'incorrect'
      }
      // A cause de la limite de temps :
      if (this.isElapsed) { // limite de temps
        montreEditeur(false)
        j3pElement('correction').style.color = this.styles.cfaux
        j3pElement('boutonrecopier').style.display = 'none'
        j3pElement('info').style.display = 'none'
        if (sq.indicationfaute) {
          sq.mtgAppLecteur.giveFormula2('mtg32svg', 'faute', '0')
          sq.mtgAppLecteur.calculate('mtg32svg', false)
          sq.mtgAppLecteur.executeMacro('mtg32svg', 'masquerFaute')
        }
        sq.mtgAppLecteur.executeMacro('mtg32svg', 'solution')
        afficheSolution(false)
        j3pElement('correction').innerHTML = tempsDepasse
        j3pElement('correction').innerHTML += '<br>La solution se trouve ci-contre.'
        this.etat = 'navigation'
        this.sectionCourante()
      } else {
        if (bilanReponse === 'incorrect') {
          j3pElement('correction').style.color = this.styles.cfaux
          if (j3pValeurde('expressioninputmq1') !== '') {
            j3pElement('correction').innerHTML = sq.signeEgalManquant ? 'Il faut utiliser le signe = ' : 'Réponse incorrecte'
          } else {
            j3pElement('correction').innerHTML = 'Il faut entrer une réponse'
          }
          j3pFocus('expressioninputmq1')
          // this.afficheBoutonValider()
        } else {
          // Une réponse a été saisie
          // Bonne réponse
          if (bilanReponse === 'exact') {
            this._stopTimer()
            sq.mtgAppLecteur.setActive('mtg32svg', false)
            this.score++
            j3pElement('correction').style.color = this.styles.cbien
            if (simplificationPossible) {
              j3pElement('correction').innerHTML = sq.cbienmais
            } else {
              j3pElement('correction').innerHTML = cBien
            }
            j3pElement('boutonrecopier').style.display = 'none'
            j3pElement('info').style.display = 'none'
            if (sq.indicationfaute) {
              sq.mtgAppLecteur.executeMacro('mtg32svg', 'masquerFaute')
            }
            sq.mtgAppLecteur.executeMacro('mtg32svg', 'solution')
            afficheSolution(true)
            montreEditeur(false)
            sq.numEssai++ // Pour un affichage correct dans afficheReponse
            afficheReponse('exact', false)
            this.etat = 'navigation'
            this.sectionCourante()
          } else {
            sq.numEssai++
            if (bilanReponse === 'exactPasFini') {
              j3pElement('correction').style.color =
                (sq.numEssai <= sq.nbEssais) ? '#0000FF' : this.styles.cfaux
              j3pElement('correction').innerHTML = 'Le calcul est bon mais pas fini.'
            } else {
              j3pElement('correction').style.color = this.styles.cfaux
              j3pElement('correction').innerHTML = cFaux
            }
            j3pEmpty('info')
            if (sq.numEssai <= sq.nbEssais) {
              afficheNombreEssaisRestants()
              videEditeur()
              afficheReponse(bilanReponse, false)
              // S’il y a plus de deux chances,on appelle à nouveau le bouton Valider
              j3pElement('correction').innerHTML += '<br>' + '<br>' + essaieEncore
            } else {
              // Erreur au nème essai
              this._stopTimer()
              sq.mtgAppLecteur.setActive('mtg32svg', false)
              j3pElement('boutonrecopier').style.display = 'none'
              j3pElement('info').style.display = 'none'
              if (sq.indicationfaute) {
                sq.mtgAppLecteur.executeMacro('mtg32svg', 'masquerFaute')
              }
              sq.mtgAppLecteur.executeMacro('mtg32svg', 'solution')
              afficheSolution(false)
              montreEditeur(false)
              afficheReponse(bilanReponse, true)
              j3pElement('correction').innerHTML += '<br><br>La solution se trouve ci-contre.'
              this.etat = 'navigation'
              this.sectionCourante()
            }
          }
        }
      }
      // Obligatoire
      this.finCorrection()
      break
    } // case "correction":

    case 'navigation':
      if (this.sectionTerminee()) {
        // On détermine la phrase d’état renvoyée par la section
        this.parcours.pe = this.score / this.donneesSection.nbitems
        this.afficheBoutonSectionSuivante()
        this.focus('sectioncontinuer')
      } else {
        this.etat = 'enonce'
        this.afficheBoutonSuite()
        this.focus('boutoncontinuer')
      }
      // Obligatoire
      this.finNavigation()

      break // case "navigation":
  }
}
