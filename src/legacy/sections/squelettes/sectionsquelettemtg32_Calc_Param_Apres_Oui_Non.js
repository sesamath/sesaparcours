import $ from 'jquery'
import { j3pAddElt, j3pAjouteBouton, j3pBoutonRadio, j3pBoutonRadioChecked, j3pDetruit, j3pDiv, j3pElement, j3pEmpty, j3pFocus, j3pImporteAnnexe, j3pPaletteMathquill, j3pShowError, j3pValeurde } from 'src/legacy/core/functions'
import { cleanLatexForMq, j3pAffiche, mqRestriction, unLatexify } from 'src/lib/mathquill/functions'
import { getMtgApp } from 'src/lib/outils/mathgraph'
import { j3pCreeSVG } from 'src/legacy/core/functionsSvg'
import textesGeneriques from 'src/lib/core/textes'
import { getIndexFermant } from 'src/lib/utils/string'

const { cBien, cFaux, essaieEncore, tempsDepasse } = textesGeneriques

/*
    Yves Biton
    Octobre 2020

    Squelette demandant d’abord de répondre à une question oui-non puis de calculer une expression
    On peut éventuellement spécifier des paramètres  a,b,c,d,e,f,g,h,j,k,n,n,p.

    Si la figure contient un calcul nbVar , alors dans le cas ou nbrepetitions > 1, on donne pour i compris entre 1 et npParamAleat
    au calcul de la figure nommé "r"+i (par exemple r1 et r2 si nbParamAleat vaut 2) des valeurs comprises entre 0 et "nbcas"+i - 1
    toutes différentes (sauf si nbrepetitions > b=nbcas auquel cas au bout de nbcas répétitions il y aura nécessairement une valeur de r déjà prise.
    Pour savoir si la réponse à la question oui-non onn interroga le calcul nommé vrai de la figure qui contient 1 si la réponse
    est oui et 0 sinon.
    Pour savoir si le calcul proposé par l’élève ets bon, on interroge le calcul de la figure mtg32 nommé reponse qui vaut
    1 si le calcul est bon et considéré comme réponse finale, 2 si le calcul est exact mais pas considéré comme réponse finale
    et 0 si le calcul est faux.
    Le fichier annexe doit contenir les membres suivants :
    titre : Le titre le l’activité
    param : chaine de caractères contenant les noms des paramètres que l’utilisateur peut changer (chaine vide par défaut)
    nbLatex : Le nombre d’affichage LaTeX que le squelette doit récupérer de la figure pour l’affichage de la consigne.
    On fait ensuite réfrence au premier affichage LaTeX par $£a$, au second par $b$ dans j3pAffiche et j3pMathAjouteDans
    consigne_QuestionOuiNon : Chaine de caractères qui est la consigne initiale pour la question oui-non
    oui : La chaine de caractère pour le bouton radio correspondant au oui ('oui' par défaut)
    non : La chaine de caractère pour le bouton radio correspondant au non ('non' par défaut)
    consigneSiFauxOui : Chaine de caractères donnant la consigne à afficher si l’élève a choisi oui alors que la réponse est non
    consigneLigne1 : Chaine de caractères contenant la première ligne de la consigne à affficher une fois que
    l’élève a répondu à la question oui-non (après consigneSiFauxOui)
    consigneLigne2SiNonSimplifier : Chaine de caractères contenant la consigne à afficher après consigneLigne1
    si on n’exige pas une réponse simplifiée
    consigneLigne2SiSimplifier :  à afficher après consigneLigne1
    si on exige une réponse simplifiée
    consigneLigne3 : Chaine de caractères contenant la consigne finale (typiquement comment utiliser la touche Entrée te le bouton OK)
    Le fichier annexe peut contenir les membres suivants :
    symbexact : Le symbole en LaTeX utilisé pour afficher un bon calcul (signe = par défaut)
    symbnonexact : Le symbole en LaTeX utilisé pour afficher un calcul faux ('\ne' par défaut)
    bigSize : booléen qui vaut true si on veut un éditeur de grande taille (false par défaut)
    btnFrac : true si on veut un bouton fraction pour l’éditeur (true par défaut)
    btnPuis : true si on veut un bouton fraction pour l’éditeur (true par défaut)
    btnRac : true si on veut un bouton racine carrée pour l’éditeur (false par défaut)
    btnExp : true si on veut un bouton exponentielle (e^) pour l’éditeur (false par défaut)
    btnLn : true si on veut un bouton ln pour l’éditeur (false par défaut)
    btnPi : true si on veut un bouton lettre grecque pi pour l’éditeur (false par défaut)
    btnSin : true si on veut un bouton sin pour l’éditeur (false par défaut)
    btnCos : true si on veut un bouton cos pour l’éditeur (false par défaut)
    btnTan : true si on veut un bouton tan pour l’éditeur (false par défaut)
    btnAbs : true si on veut un bouton valeur absolue pour l’éditeur (false par défaut)
    btnConj : true si on veut un bouton conjugué pour l’éditeur (false par défaut)
    btnInteg : true si on veut un bouton intégrale pour l’éditeur (false par défaut)
    btnPrim : true si on veut un bouton insérant des crochets de calcul de primitives pour l’éditeur (false par défaut)
    Le fichier annexe peut contenir un membre variables contenant les lettres autorisées dans un crochet pour le calcul de primitives
    Pour les calculs d’intégrales, quand on demande un résultat non simplifié :
      Si la valeur renvoyée par reponse est de 2 et que le calcul
      entré par l’lève contient une ou plusieurs intégrales ou un ou plusieurs crochets de primitive,
      le calcul sera considéré comme exact mais non fini.
      Si le fichier annexe contient un paramètre reponseSansPrim, si la valeur renvoyée par reponse est de  1 et que le calcul
      entré par l’lève contient un ou plusieurs crochets de primitive, le calcul sera considéré comme non fini.

*/
export const params = {
  outils: ['mathquill'],
  /**
   * Ce qui est paramétrable dans l’éditeur de graphe
   * @type {LegacySectionParamsParametres}
   */
  parametres: [
    ['nbrepetitions', 2, 'entier', 'aucun'],
    ['limite', 0, 'entier', 'Temps disponible par répétition, en secondes'],
    ['validationAuto', true, 'boolean', `Si false (par défaut)&nbsp;:
L’élève valide ses calculs intermédiaires par la touche Entrée et, quand il estime avoir répondu à la question, clique sur le bouton OK pour valider sa réponse.
Le nombre de calculs intermédiaires permis est nbEssais et le nombre de validations est nbchances.

Si true&nbsp;:
L’élève valide ses calculs en appuyant sur la touche Entrée ou en cliquant sur le bouton OK.
Dès qu’une des réponses intermédiaires est acceptée comme réponse finale, la réponse est acceptée.
L’élève peut faire nbEssais propositions et le paramètre nbchances n’est ici pas utilisé.`],
    ['nbchances', 2, 'entier', 'Nombre d’essais autorisés à l’élève en validant par OK (seulement si validationAuto est false)'],
    ['nbEssais', 6, 'entier', 'Nombre d’essais maximum autorisés'],
    ['indicationfaute', true, 'boolean', 'true si on veut que certaines fautes soient expliquées après appui sur Entrée'],
    ['simplifier', true, 'boolean', 'true si on exige un forme donnée, false sinon'],
    ['coefEtape1', '0.5', 'string', 'Nombre strictement compris entre 0 et 1 à rajouter au score\npour une réussite à l’étape 1'],
    ['continuerSiFaux', true, 'boolean', 'true si on demande à l’élève de faire quand même le calcul \nquand il a répondu non à la question alors que la réponse était oui'],
    ['infoParam', 'Information sur le rôle des paramètres', 'string', 'Information sur l’utilisation des paramètres suivants'],
    ['a', 'random', 'string', 'Valeur de a'],
    ['b', 'random', 'string', 'Valeur de b'],
    ['c', 'random', 'string', 'Valeur de c'],
    ['d', 'random', 'string', 'Valeur de d'],
    ['e', 'random', 'string', 'Valeur de e'],
    ['f', 'random', 'string', 'Valeur de f'],
    ['g', 'random', 'string', 'Valeur de g'],
    ['h', 'random', 'string', 'Valeur de h'],
    ['j', 'random', 'string', 'Valeur de j'],
    ['k', 'random', 'string', 'Valeur de k'],
    ['l', 'random', 'string', 'Valeur de l'],
    ['m', 'random', 'string', 'Valeur de m'],
    ['n', 'random', 'string', 'Valeur de n'],
    ['p', 'random', 'string', 'Valeur de p'],
    ['q', 'random', 'string', 'Valeur de q'],
    ['r', 'random', 'string', 'Valeur de r'],
    ['x', 'random', 'string', 'Valeur de x'],
    ['y', 'random', 'string', 'Valeur de y'],
    ['ex', 'seconde/Simplification_Quotient_Formel_3', 'string', 'Nom du fichier annexe (xxx => squelettes-mtg32/xxx.js et xxx/yyy => squelettes-mtg32-xxx/yyy.js)']
  ]
}

export default function mySection () {
  const parcours = this
  const sq = this.storage
  function onkeyup (ev, parcours) {
    if (sq.marked) {
      demarqueEditeurPourErreur()
    }
    if (!sq.validationAuto && ev.keyCode === 13) {
      const rep = j3pValeurde('expressioninputmq1')
      if (rep !== '') {
        const resul = traiteMathQuill(rep)
        const chcalcul = resul.res
        const valide = resul.valide && sq.mtgAppLecteur.syntaxValidation('mtg32svg', 'rep', chcalcul, true)
        if (valide) {
          validation(parcours)
        } else {
          marqueEditeurPourErreur()
          j3pElement('correction').style.color = parcours.styles.cfaux
          j3pElement('correction').innerHTML = 'Réponse incorrecte'
        }
      } else j3pFocus('expressioninputmq1')
    }
  }

  // Attention : Les }{ on été remplacés par des }/{
  function traiteIntegrales (st) {
    let ind
    while ((ind = st.indexOf('\\integ(')) !== -1) {
      const indpf1 = getIndexFermant(st, ind + 6)
      const fonc = st.substring(ind + 7, indpf1)
      const indpf2 = getIndexFermant(st, indpf1 + 2)
      const varfor = st.substring(indpf1 + 3, indpf2)
      const indpf3 = getIndexFermant(st, indpf2 + 2)
      const a = st.substring(indpf2 + 3, indpf3)
      const indpf4 = getIndexFermant(st, indpf3 + 2)
      const b = st.substring(indpf3 + 3, indpf4)
      st = st.substring(0, ind) + 'integrale(' + fonc + ',' + varfor + ',' + a + ',' + b + ')' + st.substring(indpf4 + 1)
    }
    return st
  }

  /**
   * Fonction renvoyant un tableau formé des noms de variables utilisées par le calcul contenu dans ch
   * Si le tableau renvoyé a une longueur au moins égale à 2 on considérera que la primitive est incohérente
   * @param {string} ch
   * @return {[]}
   */
  function variablesUtilisees (ch) {
    function utiliseVariable (nomvar) {
      let res
      res = ch.search(new RegExp('\\(' + nomvar + '[\\+\\-\\*\\/\\^\\)]')) !== -1
      res = res || ch.search(new RegExp(nomvar + '[\\+\\-\\*\\/\\^\\)]')) !== -1
      res = res || (ch.lastIndexOf(nomvar) === ch.length - 1)
      res = res || ch === nomvar
      return res
    }
    const tab = []
    for (let i = 0; i < sq.variables.length; i++) {
      const car = sq.variables.charAt(i)
      if (utiliseVariable(car)) tab.push(car)
    }
    return tab
  }

  function traitePrimitives (st) {
    if (sq.variables === '') return { valide: true, res: st }
    let ind, tab
    while ((ind = st.indexOf('\\prim(')) !== -1) {
      const indpf1 = getIndexFermant(st, ind + 5)
      const fonc = st.substring(ind + 6, indpf1)
      tab = variablesUtilisees(fonc)
      if (tab.length > 1) return { valide: false, st }
      const varfor = (tab.length === 0) ? sq.variables[0] : tab[0] // On traite le cas d’une fonction constante
      const indpf2 = getIndexFermant(st, indpf1 + 2)
      const a = st.substring(indpf1 + 3, indpf2)
      const indpf3 = getIndexFermant(st, indpf2 + 2)
      const b = st.substring(indpf2 + 3, indpf3)
      st = st.substring(0, ind) + 'primitive(' + fonc + ',' + varfor + ',' + a + ',' + b + ')' + st.substring(indpf3 + 1)
    }
    return { valide: true, res: st }
  }

  function traiteMathQuill (ch) {
    ch = unLatexify(ch)
    ch = traiteIntegrales(ch)
    const resul = traitePrimitives(ch)
    if (resul.valide) ch = sq.mtgAppLecteur.addImplicitMult('mtg32svg', resul.res)// Traitement des multiplications implicites
    const contientIntegOuPrim = (ch.indexOf('integrale') !== -1) || (ch.indexOf('primitive') !== -1)
    return { valide: resul.valide, res: ch, contientIntegOuPrim }
  }

  function marqueEditeurPourErreur () {
    sq.marked = true
    $('#expressioninputmq1').css('background-color', '#FF9999')
  }
  function demarqueEditeurPourErreur () {
    sq.marked = false
    j3pEmpty('correction')

    $('#expressioninputmq1').css('background-color', '#FFFFFF')
  }

  /**
   * Fonction extrayant d’un affichage LaTeX de la figure les lignes de texte et les transformant
   * en une chaîne affichable par MathQuill
   * @param tagLatex : le tag de l’affichage LaTeX de la figure
   * @return {string}
   */
  function extraitDeLatexPourMathQuill (tagLatex) {
    const list = sq.mtgAppLecteur.getList('mtg32svg')
    const latex = list.getByTag(tagLatex)
    if (latex === null || latex.className !== 'CLatex') return ''
    return cleanLatexForMq(latex.chaineLatex)
  }

  /**
   * Fonction affichant la solution s’il existe un affichage LaTeX de tag solution
   * @param{boolean} bexact true si l’élève a entré la bonne réponse, false sinon
   */
  function afficheSolution (bexact) {
    $('#divSolution').css('display', 'block').html('')
    const ch = extraitDeLatexPourMathQuill('solution')
    if (ch !== '') {
      j3pAffiche('divSolution', 'solution', ch, { style: { color: bexact ? 'green' : 'blue' } })
    }
  }

  function cacheSolution () {
    $('#divSolution').css('display', 'none').html('')
  }

  // Le deuxième paramètre est à false seulement quand elle est appelée depuis l’étape de correction
  // Sinon la fonction est appelée par l’éditeur quand l’utilisateur appuie sur la touche entrée avec un seul paramètre
  function validation (parcours, continuer) {
    if (arguments.length <= 1) continuer = true
    if (sq.nbexp === 0) {
      j3pElement('boutonrecopier').style.display = 'block'
    }
    sq.rep = j3pValeurde('expressioninputmq1')
    sq.repMathQuill = traiteMathQuill(sq.rep)
    const chcalcul = sq.repMathQuill.res
    sq.mtgAppLecteur.giveFormula2('mtg32svg', 'rep', chcalcul)
    j3pEmpty('correction')
    sq.mtgAppLecteur.calculate('mtg32svg', false)
    if (sq.indicationfaute) {
      const faute = sq.mtgAppLecteur.valueOf('mtg32svg', 'faute')
      if (faute === 1) {
        sq.mtgAppLecteur.executeMacro('mtg32svg', 'voirFaute')
      } else {
        sq.mtgAppLecteur.executeMacro('mtg32svg', 'masquerFaute')
      }
    }

    sq.reponse = sq.mtgAppLecteur.valueOf('mtg32svg', 'reponse')
    const idrep = 'exp' + sq.nbexp
    sq.nbexp++
    const chdeb = sq.nbexp === 1 ? '' : '<br>'
    if (sq.reponse >= 1) {
      sq.params.styletexte = { couleur: sq.reponse === 1 ? parcours.styles.cbien : parcours.styles.colorCorrection }
      j3pAffiche('formules', idrep, chdeb + '$' + (sq.entete === '' ? sq.aCalculer : sq.entete) + ' ' + sq.symbexact +
        ' ' + sq.rep + sq.postchaine + '$', sq.params)
    } else {
      const texteFaute = sq.indicationfaute ? extraitDeLatexPourMathQuill('faute') : ''
      const chFaute = texteFaute !== '' ? '<br>' + texteFaute : ''
      sq.params.styletexte = { couleur: parcours.styles.cfaux }
      j3pAffiche('formules', idrep, chdeb + '$' + (sq.entete === '' ? sq.aCalculer : sq.entete) + ' ' + sq.symbnonexact +
        ' ' + sq.rep + sq.postchaine + '$' + chFaute, sq.params)
    }
    // On vide le contenu de l’éditeur MathQuill
    $('#expressioninputmq1').mathquill('latex', ' ')
    j3pFocus('expressioninputmq1')

    // Si le nombre maximum d’essais est atteint on passe en mode correction
    if (sq.nbexp >= sq.nbEssais) {
      // sq.mtgAppLecteur.setActive("mtg32svg", false);
      if (continuer) {
        parcours.etat = 'correction'
        parcours.sectionCourante()
        $('#editeur').css('display', 'none')
        $('#boutonsmathquill').css('display', 'none')
      }
    } else {
      afficheInfo()
    }
  }

  function afficheInfo () {
    j3pEmpty('info')
    const nbe = sq.nbEssais - sq.nbexp
    let nbc = sq.nbchances - sq.numEssai + 1
    if (!sq.validationAuto && nbc > nbe) {
      nbc = nbe
    }
    if (sq.validationAuto) {
      if (nbe === 1) {
        j3pAffiche('info', 'texteinfo', 'Il reste un essai.', { style: { color: '#7F007F' } })
      } else {
        j3pAffiche('info', 'texteinfo', 'Il reste ' + nbe + ' essais.', { style: { color: '#7F007F' } })
      }
    } else {
      if ((nbe === 1) && (nbc === 1)) {
        j3pAffiche('info', 'texteinfo', 'Il reste une validation.', { style: { color: '#7F007F' } })
      } else {
        j3pAffiche('info', 'texteinfo', 'Il reste ' + nbe + ' essai(s) et ' + nbc + ' validation(s).', { style: { color: '#7F007F' } })
      }
    }
  }
  function recopierReponse () {
    demarqueEditeurPourErreur()
    $('#expressioninputmq1').mathquill('latex', sq.rep)
    j3pFocus('expressioninputmq1')
  }

  function montreEditeur (bVisible) {
    if (bVisible) $('#info').css('display', 'block')
    $('#editeur').css('display', bVisible ? 'block' : 'none')
    if (sq.boutonsMathQuill) $('#boutonsmathquill').css('display', bVisible ? 'block' : 'none')
    // On vide le contenu de l’éditeur MathQuill et on lui donne le focus.
    if (bVisible) $('#expressioninputmq1').mathquill('latex', ' ')
    j3pFocus('expressioninputmq1')
  }

  function creeEditeur (bAfficheFaux) {
    j3pEmpty('editeur')
    j3pEmpty('enonce')
    j3pEmpty('enonceSuite')
    j3pEmpty('info')
    $('#info').css('display', 'block')
    $('#editeur').css('display', 'block')
    $('#boutonsmathquill').css('display', 'inline-block')

    let st = bAfficheFaux ? sq.consigneSiFauxOui + '<br>' : ''
    st += sq.consigneLigne1 + (sq.simplifier ? sq.consigneLigne2SiSimplifier : sq.consigneLigne2SiNonSimplifier) + sq.consigneLigne3
    j3pAffiche('enonceSuite', 'texte', st, sq.params)
    sq.numEssai = 1

    if (sq.entete !== '') {
      j3pAffiche('editeur', 'debut', '$' + sq.aCalculer + sq.symbexact + '$ ', sq.params)
    } else {
      j3pAffiche('editeur', 'debut', '$' + sq.aCalculer + sq.symbexact + '$ ', sq.params)
    }

    j3pAffiche('editeur', 'expression', '&1&' + '$' + sq.postchaine + '$', sq.params)
    // Les 3 lignes c-dessous déplacées ici pour que sq.mtgAppLecteur soit prêt
    // lors de l’appel de onkeyup
    $('#expressioninputmq1').keyup(function (ev) {
      onkeyup(ev, parcours)
    })
    if (sq.charset !== '') {
      mqRestriction('expressioninputmq1', sq.charset, {
        commandes: sq.listeBoutons,
        boundingContainer: parcours.zonesElts.MG
      })
    }
    if (sq.boutonsMathQuill) j3pPaletteMathquill('boutonsmathquill', 'expressioninputmq1', { liste: sq.listeBoutons })
    afficheInfo()
    j3pFocus('expressioninputmq1')
  }

  function initMtg () {
    const mtgOptions = {
      // player only, sans figure initiale ni svg, on gère ça dans initMtg et modif_fig
      loadCoreWithMathJax: true
    }
    getMtgApp('mtg32svg', {}, mtgOptions).then((mtgAppLecteur) => {
      sq.mtgAppLecteur = mtgAppLecteur
      initDom()
      let code, par, car, i, j, k, nbrep, ar, tir, nb
      sq.marked = false
      sq.etape = 1
      sq.mtgAppLecteur.removeAllDoc()
      sq.mtgAppLecteur.addDoc('mtg32svg', sq.txt, true)
      sq.mtgAppLecteur.calculate('mtg32svg', false)
      sq.nbexp = 0
      sq.reponse = -1
      const ch = 'abcdefghjklmnpqrxy'
      const nbvar = sq.mtgAppLecteur.valueOf('mtg32svg', 'nbvar')
      if (nbvar !== -1) {
        nbrep = parcours.donneesSection.nbrepetitions
        sq.aleat = true
        sq.nbParamAleat = nbvar
        for (i = 1; i <= nbvar; i++) {
          const nbcas = sq.mtgAppLecteur.valueOf('mtg32svg', 'nbcas' + i)
          nb = Math.max(nbrep, nbcas)
          ar = []
          for (j = 0; j < nb; j++) ar.push(j % nbcas)
          sq['tab' + i] = []
          for (k = 0; k < nbrep; k++) {
            tir = Math.floor(Math.random() * ar.length)
            sq['tab' + i].push(ar[tir])
            ar.splice(tir, 1)
          }
          sq.mtgAppLecteur.giveFormula2('mtg32svg', 'r' + i, sq['tab' + i][0])
        }
      } else {
        sq.aleat = false
      }

      if (sq.param !== undefined) {
        for (i = 0; i < ch.length; i++) {
          car = ch.charAt(i)
          if (sq.param.indexOf(car) !== -1) {
            par = parcours.donneesSection[car]
            if (par !== 'random') sq.mtgAppLecteur.giveFormula2('mtg32svg', car, par)
          }
        }
      }
      sq.mtgAppLecteur.calculateAndDisplayAll(true)
      const nbParam = parseInt(sq.nbLatex)
      const t = 'abcd'
      const param = {}
      for (i = 0; i < nbParam; i++) {
        code = sq.mtgAppLecteur.getLatexCode('mtg32svg', i)
        if (i === 0) sq.aCalculer = code
        param[t.charAt(i)] = code
      }
      sq.params = param
      j3pAffiche('enoncequestionouinon', 'texteInit', sq.consigne_QuestionOuiNon, param)
      j3pBoutonRadio('questionouinon', 'radio1', 'ensemble', '0', sq.oui)
      j3pAffiche('questionouinon', 'para1', '&nbsp; &nbsp; &nbsp;', {})
      j3pBoutonRadio('questionouinon', 'radio2', 'ensemble', '1', sq.non)
      $('#editeur').css('display', 'none')
      $('#boutonsmathquill').css('display', 'none')

      sq.params.inputmq1 = {}
      sq.params.styletexte = {}
      // j3pAffiche('editeur', 'expression', '&1&', { inputmq1: {}})
      j3pAffiche('editeur', 'expression', '&1&' + '$' + sq.postchaine + '$', sq.params)
      // Les 3 lignes c-dessous déplacées ici pour que sq.mtgAppLecteur soit prêt
      // lors de l’appel de onkeyup
      $('#expressioninputmq1').keyup(function (ev) {
        onkeyup(ev, parcours)
      })
      if (sq.charset !== '') {
        mqRestriction('expressioninputmq1', sq.charset, {
          commandes: sq.listeBoutons,
          boundingContainer: parcours.zonesElts.MG
        })
      }
      parcours.finEnonce()
    }).catch(j3pShowError)
  }

  function initDom () {
    if (sq.titre) parcours.afficheTitre(sq.titre)

    j3pDiv(parcours.zones.MG, {
      id: 'conteneur',
      contenu: '',
      style: parcours.styles.etendre('toutpetit.enonce', { padding: '6px' })
    })
    j3pDiv('conteneur', 'enoncequestionouinon', '')
    j3pDiv('conteneur', 'questionouinon', '')
    j3pDiv('conteneur', 'enonce', '')
    j3pDiv('conteneur', {
      id: 'formules',
      contenu: '',
      style: parcours.styles.petit.enonce
    }) // Contient le formules entrées par l’élève
    if (sq.bigSize) $('#formules').css('font-size', '26px')
    j3pDiv('conteneur', 'enonceSuite', '')

    j3pDiv('conteneur', 'info', '')
    j3pDiv('conteneur', 'conteneurbouton', '')
    j3pAjouteBouton('conteneurbouton', 'boutonrecopier', 'MepBoutonsRepere', 'Recopier réponse précédente',
      recopierReponse)
    j3pElement('boutonrecopier').style.display = 'none'

    j3pDiv('conteneur', 'editeur', '') // Le div qui contiendra l’éditeur MathQuill
    if (sq.bigSize) $('#editeur').css('font-size', '30px')
    $('#editeur').css('padding-top', '10px') // Pour laisser un peu de place au-dessus de l’éditeur
    j3pDiv('conteneur', 'boutonsmathquill', '')
    $('#boutonsmathquill').css('display', 'inline-block')
    $('#boutonsmathquill').css('padding-top', '10px') // Pour laisser un peu de place au-dessus des boutons
    j3pAddElt('conteneur', 'div', '', { id: 'divSolution' })
    $('#divSolution').css('display', 'none')
    j3pDiv('conteneur', 'divmtg32', '')
    j3pCreeSVG('divmtg32', {
      id: 'mtg32svg',
      width: 780,
      height: 900
    })
    const style = parcours.styles.petit.correction
    style.marginTop = '1.5em'
    style.marginLeft = '0.5em'
    j3pAddElt(parcours.zonesElts.MD, 'div', '', { id: 'correction', style })
  }

  function _Donnees () {
    // "primaire" || "lycee" || "college"
    // Actuellement les dimensions sont identiques
    this.typesection = 'lycee'//

    // Si TRUE alors la touche ENTREE appelle la correction
    // Ne fonctionne QUE SI la scène dispose d’une zone de saisie

    // Nombre de répétitions de l’exercice
    this.nbrepetitions = 2
    this.nbetapes = 1
    this.nbitems = this.nbetapes * this.nbrepetitions

    this.ex = 'seconde/Simplification_Quotient_Formel_3'
    // Si indication != "" alors un lien "Indication" s’affiche dans la zone Inférieure Gauche (this.zones.IG
    // une indication<br>de deux lignes...";
    this.indication = ''
    const st = 'abcdefghjklmnpqrxy'
    for (let i = 0; i < st.length; i++) this[st.charAt(i)] = 'random'
    this.validationAuto = true
    // Nombre de chances dont dispose l’él§ve pour répondre à la question
    this.nbchances = 2
    this.nbEssais = 6

    this.simplifier = true
    this.indicationfaute = true
    this.coefEtape1 = 0.5
    this.continuerSiFaux = true

    // pour le cas de presentation1bis
    // En effet pas d’indication possible pour cette présentation
    // this.surchargeindication = false;
    this.structure = 'presentation1bis'//  || "presentation2" || "presentation3"  || "presentation1bis"

    this.textes = {}
    this.titre = 'Titre de l’activité'

    this.pe = 0
  }

  switch (this.etat) {
    case 'enonce':
      // code exécuté au lancement de la section
      if (this.debutDeLaSection) {
        this.donneesSection = new _Donnees()
        // Construction de la page
        parcours.construitStructurePage('presentation1bis', 0.75)

        this.surcharge()
        sq.questionOuiNonTraitee = false
        sq.nbEssais = parseInt(parcours.donneesSection.nbEssais)
        // compteur. Si   numEssai > nbchances, on corrige
        sq.numEssai = 1
        sq.nbchances = parseInt(parcours.donneesSection.nbchances)
        sq.simplifier = parcours.donneesSection.simplifier
        if (sq.simplifier === undefined) {
          sq.simplifier = true
        }
        sq.coefEtape1 = parseFloat(parcours.donneesSection.coefEtape1)
        sq.continuerSiFaux = parcours.donneesSection.continuerSiFaux
        sq.indicationfaute = parcours.donneesSection.indicationfaute // true si indication quand certaines fautes sont faites et appui sur Entrée
        sq.validationAuto = parcours.donneesSection.validationAuto
        this.validOnEnter = sq.validationAuto

        if (!sq.validationAuto && (sq.nbchances > sq.nbEssais)) sq.nbchances = sq.nbEssais
        // faut gérer sous-dossier ou pas…
        const sep = this.donneesSection.ex.includes('/') ? '-' : '/'
        j3pImporteAnnexe('squelettes-mtg32' + sep + this.donneesSection.ex + '.js').then(function (datasSection) {
          // console.log("initsection");
          // Attention, le this devient window...
          sq.txt = datasSection.txt
          sq.entete = datasSection.entete ?? ''
          sq.postchaine = datasSection.postchaine ?? ''
          sq.consigne_QuestionOuiNon = datasSection.consigne_QuestionOuiNon ?? ''
          sq.oui = datasSection.oui ?? 'Oui'
          sq.non = datasSection.non ?? 'non'
          sq.symbexact = datasSection.symbexact ?? '='
          sq.symbnonexact = datasSection.symbnonexact ?? '\\ne'
          sq.nbLatex = Number(datasSection.nbLatex ?? 0) // Le nombre de paramètres LaTeX dans le texte
          // ?? retourne l’opérande de gauche si !== undefined && !== null, celle de droite sinon
          const listeBoutons = []
          // boutons avec propriétés à true par défaut
          const tabBoutons1 = ['puissance', 'fraction']
          ;['btnPuis', 'btnFrac'].forEach((p, i) => {
            const b = Boolean(datasSection[p] ?? true)
            if (b) listeBoutons.push(tabBoutons1[i])
          })
          // boutons avec false par défaut
          const tabBoutons2 = ['pi', 'racine', 'exp', 'ln', 'sin', 'cos', 'tan', 'abs', 'conj', 'integ', 'prim']
          ;['btnPi', 'btnRac', 'btnExp', 'btnLn', 'btnSin', 'btnCos', 'btnTan', 'btnAbs', 'btnConj', 'btnInteg', 'btnPrim'].forEach((p, i) => {
            const b = Boolean(datasSection[p] ?? false)
            if (b) listeBoutons.push(tabBoutons2[i])
          })
          sq.listeBoutons = listeBoutons
          sq.boutonsMathQuill = listeBoutons.length !== 0
          sq.consigneSiFauxOui = datasSection.consigneSiFauxOui ?? ''
          sq.consigneLigne1 = datasSection.consigneLigne1 ?? ''
          sq.consigneLigne2SiNonSimplifier = datasSection.consigneLigne2SiNonSimplifier ?? ''
          sq.consigneLigne2SiSimplifier = datasSection.consigneLigne2SiSimplifier ?? ''
          sq.consigneLigne3 = datasSection.consigneLigne3 ?? ''
          sq.bigSize = Boolean(datasSection.bigSize ?? false)
          sq.variables = datasSection.variables ?? ''
          sq.titre = datasSection.titre ?? ''
          sq.param = datasSection.param ?? '' // Chaine contenant les paramètres autorisés
          sq.charset = datasSection.charset ?? '' // Le set de caractères utilisés dans l’éditeur
          initMtg()
        }).catch(error => {
          console.error(error)
          j3pShowError('Impossible de charger les données de cette exercice')
        })
      } else {
        j3pEmpty('correction')
        cacheSolution()
        sq.numEssai = 1
        $('#editeur').css('display', 'none')
        $('#boutonsmathquill').css('display', 'none')
        $('#questionouinon').css('color', 'black')

        sq.mtgAppLecteur.removeDoc('mtg32svg')
        sq.mtgAppLecteur.addDoc('mtg32svg', sq.txt, true)
        sq.marked = false
        sq.etape = 1
        sq.questionOuiNonTraitee = false

        if (sq.aleat) {
          for (let i = 1; i <= sq.nbParamAleat; i++) {
            sq.mtgAppLecteur.giveFormula2('mtg32svg', 'r' + i, sq['tab' + i][this.questionCourante - 1])
          }
        }
        if (sq.param !== undefined) {
          const ch = 'abcdefghjklmnpqrxy'
          for (let i = 0; i < ch.length; i++) {
            const car = ch.charAt(i)
            if (sq.param.indexOf(car) !== -1) {
              const par = parcours.donneesSection[car]
              if (par !== 'random') sq.mtgAppLecteur.giveFormula2('mtg32svg', car, par)
            }
          }
        }

        sq.mtgAppLecteur.calculate('mtg32svg', true) // true pour que les calculs alèatoires soient réinitialisés

        j3pEmpty('questionouinon')
        j3pEmpty('enoncequestionouinon')

        j3pEmpty('enonce')
        j3pEmpty('enonceSuite')
        // j3pEmpty('debut')
        j3pEmpty('formules')
        j3pEmpty('info')
        sq.nbexp = 0
        sq.reponse = -1
        const nbParam = parseInt(sq.nbLatex)
        const t = 'abcd'
        const param = {}
        for (let i = 0; i < nbParam; i++) {
          const code = sq.mtgAppLecteur.getLatexCode('mtg32svg', i)
          if (i === 0) sq.aCalculer = code
          param[t.charAt(i)] = code
        }
        sq.params = param
        sq.params.inputmq1 = {}
        sq.params.styletexte = {}
        // j3pAffiche('editeur', 'expression', '&1&', { inputmq1: {}})
        j3pDetruit('expression')
        j3pAffiche('enoncequestionouinon', 'texteInit', sq.consigne_QuestionOuiNon, param)
        j3pBoutonRadio('questionouinon', 'radio1', 'ensemble', '0', sq.oui)
        j3pAffiche('questionouinon', 'para1', '&nbsp; &nbsp; &nbsp;', {})
        j3pBoutonRadio('questionouinon', 'radio2', 'ensemble', '1', sq.non)
        $('#editeur').css('display', 'none')
        $('#boutonsmathquill').css('display', 'none')

        sq.mtgAppLecteur.display('mtg32svg') // Pour tout réafficher
        // Pour revenir en haut de page
        try {
          window.location.hash = 'MepMG'
          j3pElement('MepMG').scrollIntoView()
        } catch (e) {}
        // On vide le contenu de l’éditeur MathQuill et on lui donne le focus.
        this.finEnonce()
      }

      break // case "enonce":

    case 'correction': {
      let bilanReponse = ''
      let simplificationPossible = false
      if (!sq.questionOuiNonTraitee) {
        const rep = j3pBoutonRadioChecked('ensemble')[0]
        if (rep > -1) {
          const oui = sq.mtgAppLecteur.valueOf('mtg32svg', 'vrai')
          if (oui === 1) {
            if (rep === 0) {
              bilanReponse = 'exact'
              $('#questionouinon').css('color', parcours.styles.cbien)
            } else {
              bilanReponse = 'fauxnon'
              $('#questionouinon').css('color', parcours.styles.cfaux)
            }
          } else {
            if (rep === 0) {
              bilanReponse = 'fauxoui'
              $('#questionouinon').css('color', parcours.styles.cfaux)
            } else {
              bilanReponse = 'exactouinon'
              $('#questionouinon').css('color', parcours.styles.cbien)
            }
          }
        } else {
          bilanReponse = 'incorrect'
        }
      } else {
        // On regarde si le champ d’édition est rempli même si l’utilisateur
        // n’a pas appuyé sur la touche Entrée.
        const ch = j3pValeurde('expressioninputmq1')
        if (ch !== '') {
          const resul = traiteMathQuill(ch)
          const repcalcul = resul.res
          const valide = resul.valide && sq.mtgAppLecteur.syntaxValidation('mtg32svg', 'rep', repcalcul, true)
          if (valide) {
            validation(this, false)
          } else {
            bilanReponse = 'incorrect'
          }
        } else {
          if (sq.nbexp === 0) bilanReponse = 'incorrect'
        }
        const res1 = sq.reponse
        if (bilanReponse !== 'incorrect') {
          if (sq.simplifier) { // Si on demande le calcul simplifié
            if (res1 === 1) {
              bilanReponse = 'exact'
            } else {
              if (res1 === 2) {
                bilanReponse = 'exactpasfini'
              } else {
                bilanReponse = 'erreur'
              }
            }
          } else {
            if ((res1 === 1) || (res1 === 2)) {
              if ((res1 === 2) && sq.repMathQuill.contientIntegOuPrim) {
                bilanReponse = 'exactpasfini'
              } else { bilanReponse = 'exact' }
              if (res1 === 2) {
                simplificationPossible = true
              }
            } else {
              bilanReponse = 'erreur'
            }
          }
        }
      }
      // A cause de la limite de temps :
      if (this.isElapsed) { // limite de temps
        this._stopTimer()
        j3pElement('correction').style.color = this.styles.cfaux
        j3pElement('boutonrecopier').style.display = 'none'
        j3pElement('info').style.display = 'none'
        if (sq.indicationfaute) {
          sq.mtgAppLecteur.giveFormula2('mtg32svg', 'faute', '0')
          sq.mtgAppLecteur.calculate('mtg32svg', false)
          sq.mtgAppLecteur.executeMacro('mtg32svg', 'masquerFaute')
        }
        $('#editeur').css('display', 'none')
        $('#boutonsmathquill').css('display', 'none')
        sq.mtgAppLecteur.executeMacro('mtg32svg', 'solution')
        afficheSolution(false)
        j3pElement('correction').innerHTML = tempsDepasse
        j3pElement('correction').innerHTML += '<br>La solution se trouve ci-contre.'
        this.etat = 'navigation'
        this.sectionCourante()
      } else {
        if (bilanReponse === 'incorrect') {
          j3pElement('correction').style.color = this.styles.cfaux
          j3pElement('correction').innerHTML = 'Réponse incorrecte'
          marqueEditeurPourErreur()
          j3pFocus('expressioninputmq1')
          this.afficheBoutonValider()
        } else {
          // Une réponse a été saisie
          // Bonne réponse
          if (bilanReponse === 'exact') {
            if (sq.questionOuiNonTraitee) {
              this.score += 1 - sq.coefEtape1
            } else { this.score += sq.coefEtape1 }
            j3pElement('correction').style.color = this.styles.cbien
            let mess = simplificationPossible ? 'C’est bien mais on pouvait  simplifier la réponse' : cBien
            if (sq.etape === 1) mess += '<br>On passe à la question suivante.'
            j3pElement('correction').innerHTML = mess
            if (sq.questionOuiNonTraitee) {
              sq.numEssai++ // Pour un affichage correct dans afficheReponse
              // afficheReponse('exact', false)
              if (sq.etape === 2) {
                j3pEmpty('enonceSuite')
                this._stopTimer()
                j3pElement('boutonrecopier').style.display = 'none'
                j3pElement('info').style.display = 'none'
                if (sq.indicationfaute) sq.mtgAppLecteur.executeMacro('mtg32svg', 'masquerFaute')
                sq.mtgAppLecteur.executeMacro('mtg32svg', 'solution')
                afficheSolution(true)
                montreEditeur(false)
                this.etat = 'navigation'
                this.sectionCourante()
              } else {
                sq.etape++
                creeEditeur()
                // montreEditeur(true)
                // videEditeur() // Nécessaire sinon plusieurs éditeurs peuvent clignoter
              }
            } else {
              sq.questionOuiNonTraitee = true
              j3pElement('boutonrecopier').style.display = 'none'
              sq.etape++
              $('#radio1').attr('disabled', true)
              $('#radio2').attr('disabled', true)
              j3pEmpty('editeur')
              j3pEmpty('enonce')
              j3pEmpty('enonceSuite')
              j3pEmpty('info')
              $('#info').css('display', 'block')
              $('#editeur').css('display', 'block')
              $('#boutonsmathquill').css('display', 'inline-block')
              // sq.mtgAppLecteur.setActive("mtg32svg", false);
              const st = sq.consigneLigne1 + (sq.simplifier ? sq.consigneLigne2SiSimplifier : sq.consigneLigne2SiNonSimplifier) + sq.consigneLigne3
              j3pAffiche('enonce', 'texte', st, sq.params)
              j3pAffiche('editeur', 'debut', '', {})
              creeEditeur()
              // this.etat = 'navigation'
              // this.sectionCourante()
            }
          } else {
            $('#radio1').attr('disabled', true)
            $('#radio2').attr('disabled', true)
            if (bilanReponse === 'fauxoui' || bilanReponse === 'exactouinon' || (bilanReponse === 'fauxnon' && !sq.continuerSiFaux)) {
              sq.mtgAppLecteur.executeMacro('mtg32svg', 'solution')
              if (bilanReponse === 'fauxoui' || bilanReponse === 'fauxnon') {
                j3pElement('correction').style.color = this.styles.cfaux
                j3pElement('correction').innerHTML = cFaux
                afficheSolution(false)
              } else {
                j3pElement('correction').style.color = this.styles.cbien
                j3pElement('correction').innerHTML = cBien
                afficheSolution(true)
                this.score += 1
              }
              this.etat = 'navigation'
              this.sectionCourante()
            } else {
              if (bilanReponse === 'fauxnon') {
                // L’élève a répondu non alors que la réponse était oui et sq.continuerSiFaux est true
                // On demande quand même de simplifier
                j3pElement('correction').style.color = this.styles.cfaux
                j3pElement('correction').innerHTML = cFaux
                sq.questionOuiNonTraitee = true
                j3pElement('boutonrecopier').style.display = 'none'
                sq.etape++
                creeEditeur(true)
              } else {
                j3pElement('correction').style.color = this.styles.cfaux
                sq.numEssai++
                if (bilanReponse === 'exactpasfini') {
                  j3pElement('correction').innerHTML = 'Le calcul est bon mais pas écrit sous la forme demandée.'
                } else {
                  j3pElement('correction').innerHTML = cFaux
                }
                j3pEmpty('info')
                afficheInfo()
                // On donne le focus à l’éditeur MathQuill
                j3pFocus('expressioninputmq1')
                // S’il y a plus de deux chances,on appelle à nouveau le bouton Valider

                if ((sq.nbexp < sq.nbEssais) && (sq.validationAuto
                  ? true
                  : (sq.numEssai <= sq.nbchances))) {
                  j3pElement('correction').innerHTML += '<br>' + '<br>' + essaieEncore
                  // sq.mtgAppLecteur.setActive("mtg32svg", true);
                } else {
                  // Erreur au nème essai
                  this._stopTimer()
                  // sq.mtgAppLecteur.setActive("mtg32svg", false);
                  j3pElement('boutonrecopier').style.display = 'none'
                  j3pElement('info').style.display = 'none'
                  if (sq.indicationfaute) {
                    sq.mtgAppLecteur.executeMacro('mtg32svg', 'masquerFaute')
                  }
                  sq.mtgAppLecteur.executeMacro('mtg32svg', 'solution')
                  afficheSolution(false)
                  $('#editeur').css('display', 'none')
                  $('#boutonsmathquill').css('display', 'none')

                  // j3pElement("correction").innerHTML+='<br>' + regardeCorrection;
                  j3pElement('correction').innerHTML += '<br><br>La solution se trouve ci-contre.'
                  this.etat = 'navigation'
                  this.sectionCourante()
                }
              }
            }
          }
        }
      }
      // Obligatoire
      this.finCorrection()
      break // case "correction":
    }

    case 'navigation':

      if (this.sectionTerminee()) {
        // On détermine la phrase d’état renvoyée par la section
        this.parcours.pe = this.score / this.donneesSection.nbitems
        this.afficheBoutonSectionSuivante()
        this.focus('sectioncontinuer')
      } else {
        this.etat = 'enonce'
        this.afficheBoutonSuite()
        this.focus('boutoncontinuer')
      }
      // Obligatoire
      this.finNavigation()

      break // case "navigation":
  }
}
