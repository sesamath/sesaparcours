import { j3pAddElt, j3pDiv, j3pElement, j3pEmpty, j3pImporteAnnexe, j3pShowError } from 'src/legacy/core/functions'
import { getMtgApp } from 'src/lib/outils/mathgraph'
import { j3pCreeSVG } from 'src/legacy/core/functionsSvg'
import textesGeneriques from 'src/lib/core/textes'

const { cBien, cFaux, essaieEncore, regardeCorrection, tempsDepasse } = textesGeneriques

/*
    Yves Biton
    Juin 2014

    sectionsquelettemtg32_Vecteurs_Placer_M_Par_CL
    On demande de placer un point M sur uj quadrillage tel que Vect(AM) = aVect(u)+bVect(v)
*/

export const params = {
  outils: ['mathquill'],
  /**
   * Ce qui est paramétrable dans l’éditeur de graphe
   * @type {LegacySectionParamsParametres}
   */
  parametres: [
    ['nbrepetitions', 1, 'entier', 'aucun'],
    ['limite', 0, 'entier', 'Temps disponible par répétition, en secondes'],
    ['nbchances', 1, 'entier', 'Nombre d’erreurs possibles (pour tenter de les corriger)'],
    ['xu', 'random', 'string', 'Abscisse de l’origine du vecteur u (origine A)'],
    ['yu', 'random', 'string', 'Ordonnée de l’origine du vecteur u (origine A)'],
    ['au', 'random', 'string', 'Première coordonnée du vecteur u'],
    ['bu', 'random', 'string', 'Deuxième coordonnée du vecteur u'],
    ['xv', 'random', 'string', 'Abscisse de l’origine du vecteur v (origine A)'],
    ['yv', 'random', 'string', 'Ordonnée de l’origine du vecteur v (origine A)'],
    ['av', 'random', 'string', 'Première coordonnée du vecteur v'],
    ['bv', 'random', 'string', 'Deuxième coordonnée du vecteur v'],
    ['a', 'random', 'string', 'On demande que Vect(AM) = a Vect(u) + b Vect(v)'],
    ['b', 'random', 'string', 'On demande que Vect(AM) = a Vect(u) + b Vect(v)']
  ]
}
/**
 * section squelettemtg32_Vecteurs_Placer_M_Par_CL
 * @this {Parcours}
 */
export default function main () {
  const parcours = this
  const sq = this.storage

  function initMtg () {
    const mtgOptions = {
      // player only, sans figure initiale ni svg, on gère ça dans initMtg et modif_fig
      loadCoreWithMathJax: true
    }
    getMtgApp('mtg32svg', {}, mtgOptions).then((mtgAppLecteur) => {
      sq.mtgAppLecteur = mtgAppLecteur
      initDom()
      sq.mtgAppLecteur.removeAllDoc()
      sq.mtgAppLecteur.addDoc('mtg32svg', sq.txt, true)
      let par = parcours.donneesSection.xu
      if (par !== 'random') sq.mtgAppLecteur.giveFormula2('mtg32svg', 'xu', par)
      par = parcours.donneesSection.yu
      if (par !== 'random') sq.mtgAppLecteur.giveFormula2('mtg32svg', 'yu', par)
      par = parcours.donneesSection.au
      if (par !== 'random') sq.mtgAppLecteur.giveFormula2('mtg32svg', 'au', par)
      par = parcours.donneesSection.bu
      if (par !== 'random') sq.mtgAppLecteur.giveFormula2('mtg32svg', 'bu', par)
      par = parcours.donneesSection.xv
      if (par !== 'random') sq.mtgAppLecteur.giveFormula2('mtg32svg', 'xv', par)
      par = parcours.donneesSection.yv
      if (par !== 'random') sq.mtgAppLecteur.giveFormula2('mtg32svg', 'yv', par)
      par = parcours.donneesSection.av
      if (par !== 'random') sq.mtgAppLecteur.giveFormula2('mtg32svg', 'av', par)
      par = parcours.donneesSection.bv
      if (par !== 'random') sq.mtgAppLecteur.giveFormula2('mtg32svg', 'bv', par)
      par = parcours.donneesSection.a
      if (par !== 'random') sq.mtgAppLecteur.giveFormula2('mtg32svg', 'a', par)
      par = parcours.donneesSection.b
      if (par !== 'random') sq.mtgAppLecteur.giveFormula2('mtg32svg', 'b', par)
      sq.mtgAppLecteur.calculateAndDisplayAll(true)
      parcours.finEnonce()
    }).catch(j3pShowError)
  }

  function initDom () {
    if (sq.titre) parcours.afficheTitre(sq.titre)

    j3pDiv(parcours.zones.MG, {
      id: 'conteneur',
      contenu: '',
      style: parcours.styles.etendre('toutpetit.enonce', { padding: '6px' })
    })
    j3pDiv('conteneur', 'divmtg32', '')
    j3pCreeSVG('divmtg32', { id: 'mtg32svg', width: 750, height: 535 })
    const style = parcours.styles.petit.correction
    style.marginTop = '1.5em'
    style.marginLeft = '0.5em'
    j3pAddElt(parcours.zonesElts.MD, 'div', '', { id: 'correction', style })
  }

  function _Donnees () {
    // "primaire" || "lycee" || "college"
    // Actuellement les dimensions sont identiques
    this.typesection = 'lycee'//
    // Nombre de répétitions de l’exercice
    this.nbrepetitions = 1
    this.nbetapes = 1
    this.nbitems = this.nbetapes * this.nbrepetitions

    this.f = 'Vecteurs_Placer_M_Par_CL'
    this.xu = 'random'
    this.yu = 'random'
    this.au = 'random'
    this.bu = 'random'
    this.xv = 'random'
    this.yv = 'random'
    this.av = 'random'
    this.bv = 'random'
    this.a = 'random'
    this.b = 'random'
    // Si indication != "" alors un lien "Indication" s’affiche dans la zone Inférieure Gauche (this.zones.IG
    // une indication<br>de deux lignes...";

    // pour le cas de presentation1bis
    // En effet pas d’indication possible pour cette présentation
    // this.surchargeindication = false;

    // Nombre de chances dont dispose l’él§ve pour répondre à la question
    this.nbchances = 1

    this.textes = {
    }
    this.titre = 'Titre de l’activité'

    this.pe = 0
  }

  switch (this.etat) {
    case 'enonce':
      // code exécuté au lancement de la section
      if (this.debutDeLaSection) {
        this.donneesSection = new _Donnees()
        // Construction de la page
        this.construitStructurePage('presentation1bis')

        this.surcharge()
        this.validOnEnter = false // ex donneesSection.touche_entree
        sq.nbchances = parseInt(parcours.donneesSection.nbchances)
        // compteur. Si   numEssai > nbchances, on corrige
        sq.numEssai = 1
        // faut gérer sous-dossier ou pas…
        const sep = this.donneesSection.f.includes('/') ? '-' : '/'
        j3pImporteAnnexe('squelettes-mtg32' + sep + this.donneesSection.f + '.js').then(function (datasSection) {
          // console.log("initsection");
          // Attention, le this devient window...
          // console.log(arguments);
          sq.txt = datasSection.txt
          sq.titre = datasSection.titre
          initMtg()
        }).catch(error => {
          console.error(error)
          j3pShowError('Impossible de charger les données de cette exercice')
        })
      } else {
        j3pEmpty('correction')
        sq.numEssai = 1
        sq.mtgAppLecteur.removeDoc('mtg32svg')
        sq.mtgAppLecteur.addDoc('mtg32svg', sq.txt, true)
        let par = parcours.donneesSection.xu
        if (par !== 'random') sq.mtgAppLecteur.giveFormula2('mtg32svg', 'xu', par)
        par = parcours.donneesSection.yu
        if (par !== 'random') sq.mtgAppLecteur.giveFormula2('mtg32svg', 'yu', par)
        par = parcours.donneesSection.au
        if (par !== 'random') sq.mtgAppLecteur.giveFormula2('mtg32svg', 'au', par)
        par = parcours.donneesSection.bu
        if (par !== 'random') sq.mtgAppLecteur.giveFormula2('mtg32svg', 'bu', par)
        par = parcours.donneesSection.xv
        if (par !== 'random') sq.mtgAppLecteur.giveFormula2('mtg32svg', 'xv', par)
        par = parcours.donneesSection.yv
        if (par !== 'random') sq.mtgAppLecteur.giveFormula2('mtg32svg', 'yv', par)
        par = parcours.donneesSection.av
        if (par !== 'random') sq.mtgAppLecteur.giveFormula2('mtg32svg', 'av', par)
        par = parcours.donneesSection.bv
        if (par !== 'random') sq.mtgAppLecteur.giveFormula2('mtg32svg', 'bv', par)
        par = parcours.donneesSection.a
        if (par !== 'random') sq.mtgAppLecteur.giveFormula2('mtg32svg', 'a', par)
        par = parcours.donneesSection.b
        if (par !== 'random') sq.mtgAppLecteur.giveFormula2('mtg32svg', 'b', par)
        sq.mtgAppLecteur.calculate('mtg32svg', true) // true pour que les calculs aléaoires soient réinitialisés
        sq.mtgAppLecteur.display('mtg32svg') // Pour tout réafficher
        sq.mtgAppLecteur.setActive('mtg32svg', true)
        this.finEnonce()
      }

      break // case "enonce":

    case 'correction': {
      let bilanreponse = ''
      const res1 = sq.mtgAppLecteur.valueOf('mtg32svg', 'reponse')
      if (res1 === 1) {
        bilanreponse = 'exact'
      } else {
        bilanreponse = 'erreur'
      }

      // Bonne réponse
      if (bilanreponse === 'exact') {
        this._stopTimer()
        sq.mtgAppLecteur.setActive('mtg32svg', false)
        this.score++
        j3pElement('correction').style.color = this.styles.cbien
        j3pElement('correction').innerHTML = cBien
        sq.mtgAppLecteur.executeMacro('mtg32svg', 'solutionVrai')
        this.etat = 'navigation'
        this.sectionCourante()
      } else {
        // Pas de bonne réponse
        // A cause de la limite de temps :
        if (this.isElapsed) { // limite de temps
          this._stopTimer()
          sq.mtgAppLecteur.setActive('mtg32svg', false)
          j3pElement('correction').style.color = this.styles.cfaux
          sq.mtgAppLecteur.executeMacro('mtg32svg', 'solutionFaux')
          j3pElement('correction').innerHTML = tempsDepasse
          j3pElement('correction').innerHTML += '<br>' + regardeCorrection
          this.etat = 'navigation'
          this.sectionCourante()
        } else {
          // Réponse fausse :
          j3pElement('correction').style.color = this.styles.cfaux
          j3pElement('correction').innerHTML = cFaux
          sq.numEssai++
          // S’il y a plus de deux chances,on appelle à nouveau le bouton Valider
          if (sq.numEssai <= sq.nbchances) {
            j3pElement('correction').innerHTML += '<br>' + essaieEncore
            // indication éventuelle ici
          } else {
            // Erreur au nème essai
            this._stopTimer()
            sq.mtgAppLecteur.setActive('mtg32svg', false)
            j3pElement('correction').style.color = this.styles.cfaux
            sq.mtgAppLecteur.executeMacro('mtg32svg', 'solutionFaux')
            j3pElement('correction').innerHTML += '<br>' + regardeCorrection
            this.etat = 'navigation'
            this.sectionCourante()
          }
        }
      }
      // Obligatoire
      this.finCorrection()
      break // case "correction":
    }

    case 'navigation':

      if (this.sectionTerminee()) {
        // On détermine la phrase d’état renvoyée par la section
        this.parcours.pe = this.score
        this.afficheBoutonSectionSuivante()
        this.focus('sectioncontinuer')
      } else {
        this.etat = 'enonce'
        this.afficheBoutonSuite()
        this.focus('boutoncontinuer')
      }
      // Obligatoire
      this.finNavigation()

      break // case "navigation":
  }
}
