import { j3pAjouteArea, j3pAjouteZoneTexte, j3pDiv, j3pElement, j3pImporteAnnexe, j3pRandom, j3pRemplace, j3pShowError } from 'src/legacy/core/functions'
import { j3pCreeFenetres, j3pToggleFenetres, j3pCreeBoutonsFenetre } from 'src/legacy/core/functionsJqDialog'
import Psylvia from 'src/legacy/outils/psylvia/Psylvia'
import textesGeneriques from 'src/lib/core/textes'

const { cBien, cFaux, essaieEncore, tempsDepasse } = textesGeneriques

/*
    JP Vanroyen
    décembre 2013
*/

export const params = {
  outils: ['calculatrice'],
  /**
   * Ce qui est paramétrable dans l’éditeur de graphe
   * @type {LegacySectionParamsParametres}
   */
  parametres: [
    ['nbrepetitions', 2, 'entier', 'Nombre de répétitions de la section'],
    ['indication', '', 'string', 'Indication pour chaque répétition'],
    ['limite', 0, 'entier', 'Temps disponible par répétition, en secondes'],
    ['nbchances', 2, 'entier', 'Nombre d’essais par répétition'],
    ['f', 'psylvia1', 'string', 'Fichier contenant l’exercice algo']
  ]
}

/**
 * section squelettepsylvia
 * @this {Parcours}
 */
export default function main () {
  function verification () {
    let cbon = true
    // on vérifie l’algo saisi à l’aide de 5 valeurs aléatoires de la variable d’entrée
    for (let k = 1; k <= 5; k++) {
      const forcer = {}
      for (let u = 0; u < stor.variablesentree.length; u++) { forcer[stor.variablesentree[u]] = j3pRandom(stor.verif) }

      const erreurs = psy.compareAvecSecret(forcer, stor.variablessortie[0])

      if (typeof (erreurs[stor.variablessortie[0]]) !== 'undefined') {
        if (erreurs[stor.variablessortie[0]].attendue != erreurs.x.obtenue) {
          cbon = false
        }
      }
    }

    // console.log(erreurs)
    return cbon
  }
  function lancement (init) {
    window.ALeffacer = function () {}

    // demander n\n0  &rarr;  x\npour k de 1 à n\n  x+k  &rarr;  x\nfin pour\necrire x\n

    psy = new Psylvia()

    // FIXME ecritAlgoSecret n’existe pas
    // psy.ecritAlgoSecret(stor.algosolution, false)

    //  surveille si la variable x de l’utilisateur prend la valeur 1

    // psy.surveiller('x',0);
    // introduit deux démos dans la liste déroulante
    // psy.ajouteDemo('essai','écrire 1\nécrire 2');
    // psy.ajouteDemo('boucle','0→a\npour k de 1 à 15\na+k→a\nfin pour\necrire a');

    // masque l’enveloppe et la liste des repas de la tortue
    psy.masquer('courrier', 'repas')
    // fixe la précision à 0.001
    psy.precision(6)
    // définit la hauteur des cadres en pixels
    psy.hauteur(450)

    // appelle la fonction faire() après chaque clic sur la fusée
    // psy.actionFusee(function(){ document.getElementById("s").value=psy.verifier("x")});

    // psy.actionFusee(verification);

    // stocke un algorithme dans une chaîne de caractères
    // var algo="afficher 1/3\n1  &rarr;  x\nsi x&le;2\n  afficher x\nfin si\n";
    const algo = init
    // installe psylvia et affiche l’algorithme précédent
    psy.installer(me.zones.MG, 'divsortie', algo)
    setTimeout(function () {
      j3pElement('ALlisteDemo').style.display = 'none'
      j3pElement('ALicones').style.display = 'none'
      j3pElement('ALprecis').style.display = 'none'
      j3pElement('ALdroite').style.marginTop = '150px'
      j3pElement('ALdroite').style.paddingTop = '0px'
      j3pElement('divsortie').style.height = '320px'
    }, 10)
  }

  function _Donnees () {
    this.typesection = 'primaire'//
    this.nbrepetitions = 1
    this.nbetapes = 1
    this.indication = ''
    this.nbchances = 1
    this.structure = 'presentation1'//  || "presentation2" || "presentation3"  || "presentation1bis"
    this.f = 'exercice1'
    this.titre = 'un titre'
    this.textes = {
      phrase1: 'Ecris ',
      phrase2: 'La solution était ',
      phrase3: 'On te demande d’écrire '
    }
  }

  function onContinue () {
    const o = stor.data[me.questionCourante]
    stor.algoinit = o.algoinit
    stor.algosolution = o.algosolution
    stor.enonce = o.enonce
    stor.variablesentree = o.variablesentree
    stor.variablessortie = o.variablessortie
    stor.verif = o.verif

    j3pDiv(me.zonesElts.MD, {
      id: 'question',
      contenu: stor.enonce,
      style: me.styles.toutpetit.enonce
    }
    )
    j3pElement('question').style.padding = '10px'
    j3pElement('question').style.color = '#00A'

    j3pDiv(me.zonesElts.MD, {
      id: 'divsortie',
      contenu: '',
      style: me.styles.toutpetit.enonce
    }
    )
    lancement(stor.algoinit)

    j3pDiv(me.zonesElts.MD, { id: 'correction', contenu: '', coord: [10, 120], style: me.styles.petit.correction })

    me.finEnonce()
  }

  const me = this
  const stor = this.storage
  // une var globale à notre code
  let psy

  switch (this.etat) {
    case 'enonce':
      // code exécuté au lancement de la section
      if (this.debutDeLaSection) {
        this.donneesSection = new _Donnees()

        this.surcharge()
        this.validOnEnter = false // ex donneesSection.touche_entree
        // Construction de la page
        this.construitStructurePage({ structure: this.donneesSection.structure, ratioGauche: 0.5 })
        // par convention, this.stockage[0] contient la solution
        // les autres peuvent contenir ce que vous voulez, en général des valeurs que l’on doit mémoriser entre les différents appels de la section
        this.stockage = [0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0]
        /*
                 Par convention,`
                `   this.typederreurs[0] = nombre de bonnes réponses
                    this.typederreurs[1] = nombre de fois où l’élève a utilisé une chance supplémentaire
                    this.typederreurs[2] = nombre de mauvaises réponses
                    this.typederreurs[10] = nombre de fois où l’élève a eu faux à cause d’un temps limite
                    LES AUTRES peuvent être utilisées par la détermination de la pe dans le cas d’une section qualitative
                 */
        this.typederreurs = [0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0]

        this.score = 0

        if (this.donneesSection.indication) this.indication(this.zones.IG, this.donneesSection.indication)

        j3pDiv(this.zones.IG, {
          id: 'undiv',
          contenu: '',
          coord: [20, 20],
          style: this.styles.moyen.enonce
        }
        )
        j3pAjouteZoneTexte('undiv', { id: 's', texte: '', tailletexte: 18, width: 180 })
        j3pElement('s').style.display = 'none'

        this.fenetresjq = [
          { name: 'Boutons', title: 'Les algos', left: 700, top: 0, id: 'fenetreBoutons' },
          { name: 'solution', title: 'Algo solution', top: 170, left: 90, width: 500, id: 'fenetresolution' },
          { name: 'algoeleve', title: 'Ton algo ', top: 170, left: 90, width: 500, id: 'fenetrealgoeleve' }
        ]
        j3pCreeFenetres(this)
        j3pCreeBoutonsFenetre('fenetreBoutons', [['solution', 'Algo solution'], ['algoeleve', 'Ton algo']])
        j3pAjouteArea('fenetresolution', { id: 'zonesolution', cols: 30, rows: 7, readonly: false, taillepolice: 14, police: 'Arial', couleurpolice: '#003366' })
        j3pAjouteArea('fenetrealgoeleve', { id: 'zoneeleve', cols: 30, rows: 7, readonly: false, taillepolice: 14, police: 'Arial', couleurpolice: '#003366' })

        j3pImporteAnnexe('psylvia/' + me.donneesSection.f + '.js').then(({ titre, exos }) => {
          // console.log('récup', me.donneesSection.f, titre, exos)
          me.donneesSection.nbrepetitions = exos.length - 1
          me.donneesSection.nbitems = me.donneesSection.nbetapes * me.donneesSection.nbrepetitions
          stor.data = exos
          me.afficheTitre(titre)
          onContinue()
        }).catch(error => {
          console.error(error)
          j3pShowError('Impossible de charger les données de cette exercice')
        })
      } else {
        j3pCreeFenetres(this)
        j3pCreeBoutonsFenetre('fenetreBoutons', [['solution', 'Algo solution'], ['algoeleve', 'Ton algo']])
        j3pAjouteArea('fenetresolution', { id: 'zonesolution', cols: 30, rows: 7, readonly: false, taillepolice: 14, police: 'Arial', couleurpolice: '#003366' })
        j3pAjouteArea('fenetrealgoeleve', { id: 'zoneeleve', cols: 30, rows: 7, readonly: false, taillepolice: 14, police: 'Arial', couleurpolice: '#003366' })
        this.videLesZones()
        onContinue()
      }

      // OBLIGATOIRE

      break // case "enonce":

    case 'correction':

      var cbon = verification()
      if (cbon) {
        // Bonne réponse
        this.score++
        j3pElement('correction').style.color = this.styles.cbien
        j3pElement('correction').innerHTML = cBien

        this.typederreurs[0]++
        this.cacheBoutonValider()
        this.etat = 'navigation'
        this.sectionCourante()
      } else {
        // Pas de bonne réponse
        j3pElement('correction').style.color = this.styles.cfaux

        // A cause de la limite de temps :
        if (this.isElapsed) { // limite de temps
          this._stopTimer()

          j3pElement('correction').innerHTML = tempsDepasse
          this.typederreurs[10]++
          this.cacheBoutonValider()
          j3pElement('correction').innerHTML += '<br>' + this.donneesSection.textes.phrase2 + this.stockage[0]
          /*
                         *   RECOPIER LA CORRECTION ICI !
                         */
          this.etat = 'navigation'
          this.sectionCourante()
        } else {
          // Réponse fausse :
          j3pElement('correction').innerHTML = cFaux

          if (this.essaiCourant < this.donneesSection.nbchances) {
            j3pElement('correction').innerHTML += '<br>' + essaieEncore
            this.typederreurs[1]++
            // indication éventuelle ici
          } else {
          // Erreur au nème essai
            j3pToggleFenetres('Boutons')
            j3pElement('zonesolution').value = stor.algosolution
            function nettoie (ch) {
              while (ch.indexOf('<span') != -1) {
                ch = j3pRemplace(ch, ch.indexOf('<span'), ch.indexOf('>', ch.indexOf('<span')), '')
              }
              while (ch.indexOf('</span>') != -1) {
                ch = j3pRemplace(ch, ch.indexOf('</span>'), ch.indexOf('</span>') + 6, '')
              }
              while (ch.indexOf('<br>') != -1) {
                ch = j3pRemplace(ch, ch.indexOf('<br>'), ch.indexOf('<br>') + 3, '\n')
              }
              return ch
            }

            j3pElement('zoneeleve').value = nettoie(j3pElement('ALgauche').firstChild.innerHTML)

            this.typederreurs[2]++
            this.etat = 'navigation'
            this.sectionCourante()
          }
        }
      }
      // Obligatoire
      this.finCorrection()
      break // case "correction":

    case 'navigation':

      if (this.sectionTerminee()) {
        // On détermine la phrase d’état renvoyée par la section
        this.parcours.pe = this.score / this.donneesSection.nbitems
        this.afficheBoutonSectionSuivante()

        this.focus('sectioncontinuer')
      } else {
        this.etat = 'enonce'
        this.afficheBoutonSuite()
        this.focus('boutoncontinuer')
      }
      // Obligatoire
      this.finNavigation()

      break // case "navigation":
  }
  // console.log("fin du code : ",this.etat)
}
