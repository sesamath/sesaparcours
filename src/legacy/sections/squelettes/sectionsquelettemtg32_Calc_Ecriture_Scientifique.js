import $ from 'jquery'
import { j3pAddContent, j3pAddElt, j3pAjouteBouton, j3pElement, j3pEmpty, j3pFocus, j3pImporteAnnexe, j3pPaletteMathquill, j3pShowError, j3pValeurde } from 'src/legacy/core/functions'
import { cleanLatexForMq, j3pAffiche, mqRestriction } from 'src/lib/mathquill/functions'
import { unLatexifyAndAddImplicitMult } from 'src/legacy/sections/squelettes/helpersMtg'
import { getMtgApp } from 'src/lib/outils/mathgraph'
import { j3pCreeSVG } from 'src/legacy/core/functionsSvg'
import textesGeneriques from 'src/lib/core/textes'

const { cBien, cFaux, essaieEncore, tempsDepasse } = textesGeneriques

/*
    Yves Biton
    Mars2019

    http://localhost:8081/?graphe=[1,"squelettemtg32_Calc_Ecriture_Scientifique",[{pe:">=0",nn:"fin",conclusion:"fin"},{f:"Fact_ax_Carre_Moins_cCarre_Ex1"}]];
    squelettemtg32_Calc_Ecriture_Scientifique
    Squelette demandant de calculer une expression et de l’écrire sous forme scientifique
    Un squellete sépcial a été nécessaire car pour mtg32 10^1 n’est pas une écriture simplifiée.
    Sinon fonctionne comme sectionsquelettemtg32_Calc_Param_Mathquill_Sans_Entete
    On peut éventuellement spécifier des paramètres  a,b,c,d,e,f,g,h,j,k,n,n,p.

    Si ex contient un paramètre nbParamAleat , alors dans le cas ou nbrepetitions > 1, on donne pour i compris entre 1 et npParamAleat
    au calcul de la figure nommé "r"+i (par exemple r1 et r2 si nbParamAleat vaut 2) des valeurs comprises entre 0 et "nbcas"+i - 1
    toutes différentes (sauf si nbrepetitions > b=nbcas auquel cas au bout de nbcas répétitions il y aura nécessairement une valeur de r déjà prise.

*/

export const params = {
  outils: ['calculatrice', 'mathquill'],
  /**
   * Ce qui est paramétrable dans l’éditeur de graphe
   * @type {LegacySectionParamsParametres}
   */
  parametres: [
    ['nbrepetitions', 3, 'entier', 'aucun'],
    ['limite', 0, 'entier', 'Temps disponible par répétition, en secondes'],
    ['nbchances', 2, 'entier', 'Nombre d’erreurs possibles (pour tenter de les corriger)'],
    ['nbEssais', 4, 'entier', 'Nombre d’essais maximum autorisés'],
    ['indicationfaute', true, 'boolean', 'true si on veut que certaines fautes soient expliquées après appui sur Entrée'],
    ['simplifier', true, 'boolean', 'true si on exige un forme donnée, false sinon'],
    ['a', 'random', 'string', 'Valeur de a'],
    ['b', 'random', 'string', 'Valeur de b'],
    ['c', 'random', 'string', 'Valeur de c'],
    ['d', 'random', 'string', 'Valeur de d'],
    ['e', 'random', 'string', 'Valeur de e'],
    ['f', 'random', 'string', 'Valeur de f'],
    ['g', 'random', 'string', 'Valeur de g'],
    ['h', 'random', 'string', 'Valeur de h'],
    ['j', 'random', 'string', 'Valeur de j'],
    ['k', 'random', 'string', 'Valeur de k'],
    ['l', 'random', 'string', 'Valeur de l'],
    ['m', 'random', 'string', 'Valeur de m'],
    ['n', 'random', 'string', 'Valeur de n'],
    ['p', 'random', 'string', 'Valeur de p'],
    ['q', 'random', 'string', 'Valeur de q'],
    ['r', 'random', 'string', 'Valeur de r'],
    ['ex', 'Lycee_Ecriture_Scientifique_1', 'string', 'Nom du fichier annexe (xxx => squelettes-mtg32/xxx.js et xxx/yyy => squelettes-mtg32-xxx/yyy.js)']
  ]
}

/**
 * section squelettemtg32_Calc_Ecriture_Scientifique
 * @this {Parcours}
 */
export default function main () {
  const parcours = this // idem var globale j3p, que l’on évite d’utiliser
  const sq = this.storage
  let i, ch

  function onkeyup (ev) {
    if (sq.marked) {
      demarqueEditeurPourErreur()
    }
    if (ev.keyCode === 13) {
      const rep = j3pValeurde('expressioninputmq1')
      if (rep !== '') {
        const chcalcul = unLatexifyAndAddImplicitMult(sq.mtgAppLecteur, rep)
        const resultatSyntaxe = validationSyntaxe(chcalcul)
        // const valide = sq.mtgAppLecteur.syntaxValidation('mtg32svg', 'rep', chcalcul, true)
        if (resultatSyntaxe.correct) {
          validation(parcours)
        } else {
          marqueEditeurPourErreur()
          j3pElement('correction').style.color = parcours.styles.cfaux
          j3pAddContent('correction', resultatSyntaxe.tropDeDec ? 'Trop de décimales dans la réponse' : 'Réponse incorrecte',
            { replace: true }
          )
          j3pFocus('expressioninputmq1')
        }
      } else j3pFocus('expressioninputmq1')
    }
  }

  function afficheNombreEssaisRestants () {
    j3pEmpty('info')
    const nbe = sq.nbEssais - sq.nbexp
    const nbc = Math.min(sq.nbchances - sq.numEssai + 1, nbe)
    if ((nbe === 1) && (nbc === 1)) {
      j3pAffiche('info', 'texteinfo', 'Il reste une validation.', { style: { color: '#7F007F' } })
    } else {
      j3pAffiche('info', 'texteinfo', 'Il reste ' + nbe + ' essai(s) et ' + nbc + ' validation(s).', { style: { color: '#7F007F' } })
    }
  }
  function marqueEditeurPourErreur () {
    sq.marked = true
    $('#expressioninputmq1').css('background-color', '#FF9999')
  }
  function demarqueEditeurPourErreur () {
    sq.marked = false
    j3pEmpty('correction')

    $('#expressioninputmq1').css('background-color', '#FFFFFF')
  }

  function validationSyntaxe (ch) {
    const correct = sq.mtgAppLecteur.syntaxValidation('mtg32svg', 'rep', ch, true)
    if (correct) {
      let st = ch
      let indptdc
      // On regarde si l’élève n’a pas utilisé trop de déciamles car une réponse fausse à epsilon près
      // pourrait être donnée comme juste
      while ((indptdc = st.indexOf('.')) !== -1) {
        st = st.substring(indptdc + 1)
        let i = 0
        while (i < st.length && st.charAt(i).search(/\d/) !== -1) i++
        if (i > 10) return { correct: false, tropDeDec: true }
      }
      return { correct: true, tropDeDec: false }
    } else {
      return { correct: false, tropDeDec: false }
    }
  }

  // Le deuxième paramètre est à false seulement quand elle est appelée depuis l’étape de correction
  // Sinon la fonction est appelée par l’éditeur quand l’utilisateur appuie sur la touche entrée avec un seul paramètre
  function validation (parcours, continuer) {
    if (arguments.length <= 1) continuer = true
    if (sq.nbexp === 0) {
      j3pElement('boutonrecopier').style.display = 'block'
    }
    sq.rep = j3pValeurde('expressioninputmq1')
    const chcalcul = unLatexifyAndAddImplicitMult(sq.mtgAppLecteur, sq.rep)
    // Pour évaluer la validité de la réponse il ne faut pas de 10^1 car pour mtg32 ce n’est pas simplifie
    let chcalculaux = chcalcul.replace('^1', '')
    let ind1 = chcalculaux.indexOf('1*')
    // 1*10^n ne sera pas accepté comme bon si on ne le tronque pas
    if (ind1 === 0) {
      chcalculaux = chcalculaux.substring(chcalculaux, 2)
    } else {
      ind1 = chcalculaux.indexOf('*1')
      if (ind1 === chcalculaux.length - 2) chcalculaux = chcalculaux.substring(chcalculaux, 0, chcalculaux.length - 2)
    }
    sq.mtgAppLecteur.giveFormula2('mtg32svg', 'rep', chcalculaux)
    j3pEmpty('correction')
    sq.mtgAppLecteur.calculate('mtg32svg', false)
    if (sq.indicationfaute) {
      const faute = sq.mtgAppLecteur.valueOf('mtg32svg', 'faute')
      if (faute === 1) {
        sq.mtgAppLecteur.executeMacro('mtg32svg', 'voirFaute')
      } else {
        sq.mtgAppLecteur.executeMacro('mtg32svg', 'masquerFaute')
      }
    }

    sq.reponse = sq.mtgAppLecteur.valueOf('mtg32svg', 'reponse')
    const idrep = 'exp' + sq.nbexp
    sq.nbexp++
    const chdeb = sq.nbexp === 1 ? '' : '<br>'
    if (sq.reponse >= 1) {
      j3pAffiche('formules', idrep, chdeb + '$' + sq.aCalculer + ' ' + sq.symbexact +
        ' ' + sq.rep + '$', {
        styletexte: {
          couleur: '#0000FF'
        }
      })
    } else {
      j3pAffiche('formules', idrep, chdeb + '$' + sq.aCalculer + ' ' + sq.symbnonexact +
        ' ' + sq.rep + '$', {
        styletexte: {
          couleur: '#FF0000'
        }
      })
    }
    // On vide le contenu de l’éditeur MathQuill
    $('#expressioninputmq1').mathquill('latex', ' ')
    j3pFocus('expressioninputmq1')

    // Si le nombre maximum d’essais est atteint on passe en mode correction
    if (sq.nbexp >= sq.nbEssais) {
      sq.mtgAppLecteur.setActive('mtg32svg', false)
      if (continuer) {
        parcours.etat = 'correction'
        parcours.sectionCourante()
        $('#editeur').css('display', 'none')
        $('#boutonsmathquill').css('display', 'none')
      }
    } else {
      afficheNombreEssaisRestants()
    }
  }

  function recopierReponse () {
    demarqueEditeurPourErreur()
    $('#expressioninputmq1').mathquill('latex', sq.rep)
    j3pFocus('expressioninputmq1')
  }

  /**
   * Fonction extrayant d’un affichage LaTeX de la figure les lignes de texte et les transformant
   * en une chaîne affichable par MathQuill
   * @param {string} tagLatex tag de l’affichage LaTeX de la figure
   * @return {string}
   */
  function extraitDeLatexPourMathQuill (tagLatex) {
    const list = sq.mtgAppLecteur.getList('mtg32svg')
    const latex = list.getByTag(tagLatex)
    if (latex === null || latex.className !== 'CLatex') return ''
    return cleanLatexForMq(latex.chaineLatex)
  }

  /**
   * Fonction affichant la solution s’il existe un affichage LaTeX de tag solution
   * @param{boolean} bexact true si l’élève a entré la bonne réponse, false sinon
   */
  function afficheSolution (bexact) {
    $('#divSolution').css('display', 'block').html('')
    const ch = extraitDeLatexPourMathQuill('solution')
    if (ch !== '') {
      j3pAffiche('divSolution', 'solution', ch, { style: { color: bexact ? 'green' : 'blue' } })
    }
  }

  function cacheSolution () {
    $('#divSolution').css('display', 'none').html('')
  }

  function initDom () {
    if (sq.titre) parcours.afficheTitre(sq.titre)

    j3pAddElt(parcours.zones.MG, 'div', '',
      {
        id: 'conteneur',
        contenu: '',
        style: parcours.styles.etendre('toutpetit.enonce', { padding: '6px' })
      })
    j3pAddElt('conteneur', 'div', '', { id: 'enonce' })
    j3pAddElt('conteneur', 'div', '', {
      id: 'formules',
      style: parcours.styles.petit.enonce
    })
    j3pAddElt('conteneur', 'div', {
      id: 'info'
    })
    afficheNombreEssaisRestants()
    j3pAddElt('conteneur', 'div', '', { id: 'conteneurbouton' })
    j3pAjouteBouton('conteneurbouton', 'boutonrecopier', 'MepBoutonsRepere', 'Recopier réponse précédente',
      recopierReponse)
    j3pElement('boutonrecopier').style.display = 'none'
    j3pAddElt('conteneur', 'div', '', { id: 'editeur' })
    $('#editeur').css('padding-top', '10px') // Pour laisser un peu de place au-dessus de l’éditeur
    j3pAffiche('editeur', 'debut', '', {})
    j3pAffiche('editeur', 'expression', '&1&', { inputmq1: {} })
    if (sq.charset !== '') {
      mqRestriction('expressioninputmq1', sq.charset, {
        commandes: sq.listeBoutons,
        boundingContainer: parcours.zonesElts.MG
      })
    }
    j3pAddElt('conteneur', 'div', '', { id: 'boutonsmathquill' })
    $('#boutonsmathquill').css('display', 'inline-block').css('padding-top', '10px') // Pour laisser un peu de place au-dessus des boutons
    j3pPaletteMathquill('boutonsmathquill', 'expressioninputmq1', { liste: sq.listeBoutons })
    j3pAddElt('conteneur', 'div', '', { id: 'divSolution' })
    $('#divSolution').css('display', 'none')
    j3pAddElt('conteneur', 'div', '', { id: 'divmtg32' })
    // SVG de dimensions nulles car l’affichage de la solution est délégué à MathQuill
    j3pCreeSVG('divmtg32', {
      id: 'mtg32svg',
      width: 0,
      height: 0
    })

    const style = parcours.styles.petit.correction
    style.marginTop = '1.5em'
    style.marginLeft = '0.5em'
    j3pAddElt(parcours.zonesElts.MD, 'div', '', { id: 'correction', style })

    j3pFocus('expressioninputmq1')
  } // initDom

  function initMtg () {
    // on charge mathgraph
    const mtgOptions = {
      // player only, sans figure initiale ni svg, on gère ça dans initMtg et modif_fig
      loadCoreWithMathJax: true
    }
    getMtgApp('mtg32svg', {}, mtgOptions).then(mtgAppLecteur => {
      sq.mtgAppLecteur = mtgAppLecteur
      sq.nbexp = 0
      sq.reponse = -1
      initDom()
      let code, par, car, i, j, k, nbrep, ar, tir, nb
      sq.mtgAppLecteur.removeAllDoc()
      sq.mtgAppLecteur.addDoc('mtg32svg', sq.txt, true)
      sq.mtgAppLecteur.calculate('mtg32svg', false)
      const ch = 'abcdefghjklmnpqr'
      const nbvar = sq.mtgAppLecteur.valueOf('mtg32svg', 'nbvar')
      if (nbvar !== -1) {
        nbrep = parcours.donneesSection.nbrepetitions
        sq.aleat = true
        sq.nbParamAleat = nbvar
        for (i = 1; i <= nbvar; i++) {
          const nbcas = sq.mtgAppLecteur.valueOf('mtg32svg', 'nbcas' + i)
          nb = Math.max(nbrep, nbcas)
          ar = []
          for (j = 0; j < nb; j++) ar.push(j % nbcas)
          sq['tab' + i] = []
          for (k = 0; k < nbrep; k++) {
            tir = Math.floor(Math.random() * ar.length)
            sq['tab' + i].push(ar[tir])
            ar.splice(tir, 1)
          }
          sq.mtgAppLecteur.giveFormula2('mtg32svg', 'r' + i, sq['tab' + i][0])
        }
      } else {
        sq.aleat = false
      }

      if (sq.param !== undefined) {
        for (i = 0; i < ch.length; i++) {
          car = ch.charAt(i)
          if (sq.param.indexOf(car) !== -1) {
            par = parcours.donneesSection[car]
            if (par !== 'random') sq.mtgAppLecteur.giveFormula2('mtg32svg', car, par)
          }
        }
      }
      sq.mtgAppLecteur.calculateAndDisplayAll(true)
      const nbParam = parseInt(sq.numero)
      const t = ['a', 'b', 'c', 'd']
      const param = {}
      for (i = 0; i < nbParam; i++) {
        code = sq.mtgAppLecteur.getLatexCode('mtg32svg', i)
        if (i === 0) sq.aCalculer = code
        param[t[i]] = code
      }
      param.e = sq.nbEssais
      const st = sq.consigne1 + (sq.simplifier ? sq.consigne3 : sq.consigne2) + sq.consigne4
      // j3pAffiche("editeur","debut", "$"  + sq.aCalculer + "$ = ", {});
      // $(debut).html("$"  + sq.aCalculer + "$ = ");
      j3pAffiche('enonce', 'texte', st, param)
      j3pAffiche('debut', 'acalc', '$' + sq.aCalculer + '$ = ')
      // Les trois lignee suivantes déplacées ici pour ne pas risquer l’appel de onkeyup avant que
      // sq.mtgAppLecteur soit prêt
      j3pElement('expressioninputmq1').onkeyup = function (ev) {
        onkeyup(ev)
      }
      parcours.finEnonce()
    }).catch(j3pShowError)
  } // initMtg

  function _Donnees () {
    // "primaire" || "lycee" || "college"
    // Actuellement les dimensions sont identiques
    this.typesection = 'lycee'//
    // Nombre de répétitions de l’exercice
    this.nbrepetitions = 3
    this.nbetapes = 1
    this.nbitems = this.nbetapes * this.nbrepetitions

    this.ex = 'Lycee_Ecriture_Scientifique_1'
    // Si indication != "" alors un lien "Indication" s’affiche dans la zone Inférieure Gauche (this.zones.IG
    // une indication<br>de deux lignes...";
    this.indication = ''
    const st = 'abcdefghjklmnpqr'
    for (let i = 0; i < st.length; i++) this[st.charAt(i)] = 'random'
    this.nbEssais = 4
    this.simplifier = true
    this.indicationfaute = true

    // pour le cas de presentation1bis
    // En effet pas d’indication possible pour cette présentation
    // this.surchargeindication = false;

    // Nombre de chances dont dispose l’él§ve pour répondre à la question
    this.nbchances = 2

    this.structure = 'presentation1bis'//  || "presentation2" || "presentation3"  || "presentation1bis"

    this.textes = {}
    this.titre = 'Titre de l’activité'

    this.pe = 0
  }

  switch (this.etat) {
    case 'enonce':
      // code exécuté au lancement de la section
      if (this.debutDeLaSection) {
        this.donneesSection = new _Donnees()
        // Construction de la page
        parcours.construitStructurePage('presentation1bis')

        this.surcharge()
        this.validOnEnter = false // ex donneesSection.touche_entree
        sq.numEssai = 1
        sq.nbEssais = parseInt(parcours.donneesSection.nbEssais)
        sq.nbchances = parseInt(parcours.donneesSection.nbchances)
        sq.simplifier = parcours.donneesSection.simplifier
        if (sq.simplifier === undefined) {
          sq.simplifier = true
        }
        sq.indicationfaute = parcours.donneesSection.indicationfaute // true si indication quand certaines fautes sont faites et appui sur Entrée

        if (sq.nbchances > parcours.donneesSection.nbEssais) sq.nbchances = sq.nbEssais
        // faut gérer sous-dossier ou pas…
        const sep = this.donneesSection.ex.includes('/') ? '-' : '/'
        j3pImporteAnnexe('squelettes-mtg32' + sep + this.donneesSection.ex + '.js').then(function (datasSection) {
          // console.log("initsection");
          // Attention, le this devient window...
          sq.txt = datasSection.txt
          sq.symbexact = datasSection.symbexact ?? '='
          sq.symbnonexact = datasSection.symbnonexact ?? '\\ne'
          sq.numero = Number(datasSection.numero ?? 0) // Le nombre de paramètres LaTeX dans le texte
          const liste = []
          const tab1 = ['fraction', 'puissance']
          ;['btnFrac', 'btnPuis'].forEach((p, i) => {
            const b = Boolean(datasSection[p] ?? true)
            if (b) liste.push(tab1[i])
          })
          const tab2 = ['racine', 'pi', 'exp']
          ;['btnRac', 'btnPi', 'btnExp'].forEach((p, i) => {
            const b = Boolean(datasSection[p] ?? false)
            if (b) liste.push(tab2[i])
          })
          sq.listeBoutons = liste
          let s = datasSection.consigne1
          sq.consigne1 = (s !== undefined) ? s : ''
          s = datasSection.consigne2
          sq.consigne2 = (s !== undefined) ? s : ''
          s = datasSection.consigne3
          sq.consigne3 = (s !== undefined) ? s : ''
          s = datasSection.consigne4
          sq.consigne4 = (s !== undefined) ? s : ''
          sq.titre = datasSection.titre
          sq.param = datasSection.param // Chaine contenant les paramètres autorisés
          s = datasSection.charset // Le set de caractères utilisés dans l’éditeur
          sq.charset = (s !== undefined) ? s : ''
          initMtg()
        }).catch(error => {
          console.error(error)
          j3pShowError('Impossible de charger les données de cette exercice')
        })
      } else {
        // pas debutDeLaSection
        j3pEmpty('correction')
        this.afficheBoutonValider()
        sq.numEssai = 1
        cacheSolution()
        $('#editeur').css('display', 'block')
        $('#boutonsmathquill').css('display', 'inline-block')
        // On vide le contenu de l’éditeur MathQuill et on lui donne le focus.
        $('#expressioninputmq1').mathquill('latex', ' ')
        j3pFocus('expressioninputmq1')

        sq.mtgAppLecteur.removeDoc('mtg32svg')
        sq.mtgAppLecteur.addDoc('mtg32svg', sq.txt, true)

        if (sq.aleat) {
          for (i = 1; i <= sq.nbParamAleat; i++) {
            sq.mtgAppLecteur.giveFormula2('mtg32svg', 'r' + i, sq['tab' + i][this.questionCourante - 1])
          }
        }
        if (sq.param !== undefined) {
          ch = 'abcdefghjklmnpqr'
          for (i = 0; i < ch.length; i++) {
            const car = ch.charAt(i)
            if (sq.param.indexOf(car) !== -1) {
              const par = parcours.donneesSection[car]
              if (par !== 'random') sq.mtgAppLecteur.giveFormula2('mtg32svg', car, par)
            }
          }
        }

        sq.mtgAppLecteur.calculate('mtg32svg', true) // true pour que les calculs alèatoires soient réinitialisés
        j3pEmpty('enonce')
        j3pEmpty('debut')
        j3pEmpty('formules')
        j3pEmpty('info')
        sq.nbexp = 0
        sq.reponse = -1
        const nbParam = parseInt(sq.numero)
        const t = ['a', 'b', 'c', 'd']
        const param = {}
        for (i = 0; i < nbParam; i++) {
          const code = sq.mtgAppLecteur.getLatexCode('mtg32svg', i)
          if (i === 0) sq.aCalculer = code
          param[t[i]] = code
        }
        param.e = sq.nbEssais
        const st = sq.consigne1 + (sq.simplifier ? sq.consigne3 : sq.consigne2) + sq.consigne4
        j3pAffiche('enonce', 'texte', st, param)
        j3pAffiche('debut', 'acalc', '$' + sq.aCalculer + '$ = ', param)
        afficheNombreEssaisRestants()
        j3pElement('info').style.display = 'block'
        sq.mtgAppLecteur.display('mtg32svg') // Pour tout réafficher
        this.finEnonce()
      }
      break // case "enonce":

    case 'correction': {
      let bilanreponse = ''
      let simplifieri = false
      // On regarde si le champ d’édition est rempli même si l’utilisateur
      // n’a pas appuyé sur la touche Entrée.
      ch = j3pValeurde('expressioninputmq1')
      if (ch !== '') {
        const repcalcul = unLatexifyAndAddImplicitMult(sq.mtgAppLecteur, ch)
        sq.resultatSyntaxe = validationSyntaxe(repcalcul)
        const valide = sq.resultatSyntaxe.correct
        if (valide) {
          validation(this, false)
        } else {
          bilanreponse = 'incorrect'
        }
      } else {
        sq.resultatSyntaxe = { correct: false, tropDeDec: false }
        if (sq.nbexp === 0) bilanreponse = 'incorrect'
      }
      const res1 = sq.reponse
      if (bilanreponse !== 'incorrect') {
        if (sq.simplifier) { // Si on demande le calcul simplifié
          if (res1 === 1) {
            bilanreponse = 'exact'
            simplifieri = true
          } else {
            if (res1 === 2) {
              bilanreponse = 'exactpasfini'
            } else {
              bilanreponse = 'erreur'
            }
          }
        } else {
          if ((res1 === 1) || (res1 === 2)) {
            bilanreponse = 'exact'
            if (res1 === 1) {
              simplifieri = true
            }
          } else {
            bilanreponse = 'erreur'
          }
        }
      }
      // A cause de la limite de temps :
      if (this.isElapsed) { // limite de temps
        this._stopTimer()
        j3pElement('correction').style.color = this.styles.cfaux
        j3pElement('boutonrecopier').style.display = 'none'
        j3pElement('info').style.display = 'none'
        if (sq.indicationfaute) {
          sq.mtgAppLecteur.giveFormula2('mtg32svg', 'faute', '0')
          sq.mtgAppLecteur.calculate('mtg32svg', false)
          sq.mtgAppLecteur.executeMacro('mtg32svg', 'masquerFaute')
        }
        $('#editeur').css('display', 'none')
        $('#boutonsmathquill').css('display', 'none')
        afficheSolution(false)
        sq.mtgAppLecteur.executeMacro('mtg32svg', 'solution')
        j3pAddContent('correction', tempsDepasse, { replace: true })
        j3pAddContent('correction', '<br>La solution se trouve ci-contre.')
        /*
                 *   RECOPIER LA CORRECTION ICI !
                 */
        this.etat = 'navigation'
        this.sectionCourante()
      } else {
        if (bilanreponse === 'incorrect') {
          j3pElement('correction').style.color = this.styles.cfaux
          j3pAddContent('correction', sq.resultatSyntaxe.tropDeDec
            ? 'Trop de décimales dans la réponse'
            : 'Réponse incorrecte', { replace: true })
          marqueEditeurPourErreur()
          j3pFocus('expressioninputmq1')
          this.afficheBoutonValider()
        } else {
          // Bonne réponse
          if (bilanreponse === 'exact') {
            this._stopTimer()
            sq.mtgAppLecteur.setActive('mtg32svg', false)
            this.score++
            j3pElement('correction').style.color = this.styles.cbien
            if (simplifieri) {
              j3pAddContent('correction', cBien, { replace: true })
            } else {
              j3pAddContent('correction', 'C’est bien mais on pouvait  simplifier la réponse', { replace: true })
            }
            j3pElement('boutonrecopier').style.display = 'none'
            j3pElement('info').style.display = 'none'
            if (sq.indicationfaute) {
              sq.mtgAppLecteur.executeMacro('mtg32svg', 'masquerFaute')
            }
            afficheSolution(true)
            sq.mtgAppLecteur.executeMacro('mtg32svg', 'solution')
            $('#editeur').css('display', 'none')
            $('#boutonsmathquill').css('display', 'none')
            this.etat = 'navigation'
            this.sectionCourante()
          } else {
            j3pElement('correction').style.color = this.styles.cfaux
            sq.numEssai++
            if (bilanreponse === 'exactpasfini') {
              j3pAddContent('correction', 'Le calcul est bon mais pas écrit sous la forme demandée.',
                { replace: true })
            } else {
              j3pAddContent('correction', cFaux, { replace: true })
            }
            afficheNombreEssaisRestants()
            // On donne le focus à l’éditeur MathQuill
            j3pFocus('expressioninputmq1')
            // S’il y a plus de deux chances,on appelle à nouveau le bouton Valider

            if ((sq.nbexp < sq.nbEssais) && (sq.numEssai <= sq.nbchances)) {
              j3pElement('correction').innerHTML += '<br>' + '<br>' + essaieEncore
              this.typederreurs[1]++
              // indication éventuelle ici
              // parcours.Sectionsquelettemtg32_Calc_Ecriture_Scientifique.nbExp += 1;
              sq.mtgAppLecteur.setActive('mtg32svg', true)
            } else {
              // Erreur au nème essai
              this._stopTimer()
              sq.mtgAppLecteur.setActive('mtg32svg', false)
              j3pElement('boutonrecopier').style.display = 'none'
              j3pElement('info').style.display = 'none'
              if (sq.indicationfaute) {
                sq.mtgAppLecteur.executeMacro('mtg32svg', 'masquerFaute')
              }
              afficheSolution(false)
              sq.mtgAppLecteur.executeMacro('mtg32svg', 'solution')
              $('#editeur').css('display', 'none')
              $('#boutonsmathquill').css('display', 'none')

              // j3pElement("correction").innerHTML+='<br>' + regardeCorrection;
              j3pAddContent('correction', '\n\nLa solution se trouve ci-contre.')
              this.etat = 'navigation'
              this.sectionCourante()
            }
          }
        }
      }
      this.finCorrection()
      break // case "correction":
    }

    case 'navigation':
      if (this.sectionTerminee()) {
        // On détermine la phrase d’état renvoyée par la section
        this.parcours.pe = this.score / this.donneesSection.nbitems
        this.afficheBoutonSectionSuivante()
        this.focus('sectioncontinuer')
      } else {
        this.etat = 'enonce'
        this.afficheBoutonSuite()
        this.focus('boutoncontinuer')
      }
      this.finNavigation()
      break // case "navigation":
  }
}
