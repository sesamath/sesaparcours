import $ from 'jquery'

import { j3pAddElt, j3pAjouteBouton, j3pDiv, j3pElement, j3pEmpty, j3pFocus, j3pImporteAnnexe, j3pMathquillXcas, j3pPaletteMathquill, j3pShowError, j3pValeurde } from 'src/legacy/core/functions'
import { j3pAffiche, mqRestriction } from 'src/lib/mathquill/functions'

import { unLatexifyAndAddImplicitMult } from 'src/legacy/sections/squelettes/helpersMtg'
import { getMtgApp } from 'src/lib/outils/mathgraph'
import { j3pCreeSVG } from 'src/legacy/core/functionsSvg'
import textesGeneriques from 'src/lib/core/textes'

const { cBien, cFaux, essaieEncore, tempsDepasse } = textesGeneriques

/*
    Yves Biton
    Août 2017

    http://localhost:8081/?graphe=[1,"squelettemtg32_Eq_Calcul_Formel",[{pe:">=0",nn:"2"},{ex:"Calcul_xSura_Plus_b_Egal_1Sura","simplifier":true,"nbEssais":"6",nbchances:"3"}]];;
    squelettemtg32_Eq_Calcul_Formel
    Squelette demandant résoudre de façon formelle une équation avec une iconnue x et des paramètres formels.
    On peut éventuellement spécifier des paramètres  a,b,c,d,e,f,g,h,i,j,k.
    Peut être utilisé avec les fichiers suivant pour le paramètre ex :
    Calcul_xSura_Plus_b_Egal_1Sura
    Calcul_ax_Plus_aSurb_Egal_1Surb
    Calcul_ax_Plus_bx_Egal_aCarre_Moins_bCarre
    Calcul_xSura_Plus_xSurb_Egal_1
    Calcul_1Surx_Egal_1surb_Moins_1Sura
    Calcul_x_Sur_ab_Plus_aSurb_Egal_1Surb
    Calcul_b_Fois_xMoins1Sura_Egal_a
    Calcul_xSurbCarre_Moins_xSuraCarre_Egal_aPlusb_Sur aMoinsb
    Calcul_aCarre_Plus_aPlusb_Sur_x_Egal_bCarre
    Calcul_b_Sur_xPlusb_Egal_1_Sur_aPlus1
    Calcul_1_Sur_xSuraPlus1Surb_Egal_a
    Calcul_ab_Sur_xSuraPlusb_Egal_aPlusb

*/

export const params = {
  outils: ['mathquill'],
  /**
   * Ce qui est paramétrable dans l’éditeur de graphe
   * @type {LegacySectionParamsParametres}
   */
  parametres: [
    ['nbrepetitions', 1, 'entier', 'aucun'],
    ['limite', 0, 'entier', 'Temps disponible par répétition, en secondes'],
    ['nbchances', 1, 'entier', 'Nombre d’erreurs possibles (pour tenter de les corriger)'],
    ['nbEssais', 6, 'entier', 'Nombre d’essais maximum autorisé (8 par défaut)'],
    ['simplifier', true, 'boolean', 'true si on demande la solution simplifiée au maximum'],
    ['ex', 'Eq_Deg1_ax_Egal_b', 'string', 'Nom du fichier annexe (xxx => squelettes-mtg32/xxx.js et xxx/yyy => squelettes-mtg32-xxx/yyy.js)']
  ]
}

/**
 * section squelettemtg32_Eq_Calcul_Formel
 * @this {Parcours}
 */
export default function main () {
  const parcours = this
  const sq = this.storage

  function onkeyup (ev, parcours) {
    if (sq.marked) { demarqueEditeurPourErreur() }
    if (ev.keyCode === 13) {
      const rep = j3pValeurde('expressioninputmq1')
      if (rep !== '') {
        const chcalcul = unLatexifyAndAddImplicitMult(sq.mtgAppLecteur, rep)
        const valide = sq.mtgAppLecteur.syntaxValidation('mtg32svg', 'rep', chcalcul, true)
        let erreurMarquee = false
        if (valide) {
          if (chcalcul.indexOf('=') === -1) {
            erreurMarquee = true
          } else {
            if (chcalcul.lastIndexOf('=') === chcalcul.indexOf('=')) {
              validation(parcours)
            }
          }
        }
        if (!valide || erreurMarquee) {
          marqueEditeurPourErreur()
          j3pElement('correction').style.color = parcours.styles.cfaux
          j3pElement('correction').innerHTML = erreurMarquee ? 'Il faut utiliser le signe =' : 'Syntaxe incorrecte'
          j3pFocus('expressioninputmq1')
        }
      } else j3pFocus('expressioninputmq1')
    }
  }

  function afficheNombreEssaisRestants () {
    j3pEmpty('info')
    const nbe = sq.nbEssais - sq.nbexp
    const nbc = Math.min(sq.nbchances - sq.numEssai + 1, nbe)
    if ((nbe === 1) && (nbc === 1)) {
      j3pAffiche('info', 'texteinfo', 'Il reste une validation.', { style: { color: '#7F007F' } })
    } else {
      j3pAffiche('info', 'texteinfo', 'Il reste ' + nbe + ' essai(s) et ' + nbc + ' validation(s).', { style: { color: '#7F007F' } })
    }
  }

  function marqueEditeurPourErreur () {
    sq.marked = true
    $('#expressioninputmq1').css('background-color', '#FF9999')
  }
  function demarqueEditeurPourErreur () {
    sq.marked = false
    j3pEmpty('correction')
    $('#expressioninputmq1').css('background-color', '#FFFFFF')
  }

  function validation (parcours, continuer) {
    if (arguments.length <= 1) continuer = true
    if (sq.nbexp === 0) {
      j3pElement('boutonrecopier').style.display = 'block'
    }
    sq.rep = j3pValeurde('expressioninputmq1')
    const chcalcul = unLatexifyAndAddImplicitMult(sq.mtgAppLecteur, sq.rep)
    sq.mtgAppLecteur.giveFormula2('mtg32svg', 'rep', chcalcul)
    j3pEmpty('correction')
    sq.mtgAppLecteur.calculate('mtg32svg', false)
    sq.reponse = sq.mtgAppLecteur.valueOf('mtg32svg', 'reponse')
    const idrep = 'exp' + sq.nbexp
    sq.nbexp++
    const chdeb = sq.nbexp === 1 ? '' : '<br>'
    if (sq.reponse >= 1) {
      j3pAffiche('formules', idrep, chdeb + '$' + ' ' + sq.symbexact +
        ' ' + sq.rep + '$', {
        styletexte: {
          couleur: '#0000FF'
        }
      })
    } else {
      j3pAffiche('formules', idrep, chdeb + '$' + ' ' + sq.symbnonexact +
        ' ' + sq.rep + '$', {
        styletexte: {
          couleur: '#FF0000'
        }
      })
    }
    // On vide le contenu de l’éditeur MathQuill
    $('#expressioninputmq1').mathquill('latex', ' ')
    j3pFocus('expressioninputmq1')

    // Si le nombre maximum d’essais est atteint on passe en mode correction
    if (sq.nbexp >= sq.nbEssais) {
      sq.mtgAppLecteur.setActive('mtg32svg', false)
      if (continuer) {
        parcours.etat = 'correction'
        parcours.sectionCourante()
        $('#editeur').css('display', 'none')
        $('#boutonsmathquill').css('display', 'none')
      }
    } else {
      afficheNombreEssaisRestants()
    }
  }

  function recopierReponse () {
    demarqueEditeurPourErreur()
    $('#expressioninputmq1').mathquill('latex', sq.rep)
    j3pFocus('expressioninputmq1')
  }
  function initMtg () {
    const mtgOptions = {
      // player only, sans figure initiale ni svg, on gère ça dans initMtg et modif_fig
      loadCoreWithMathJax: true
    }
    getMtgApp('mtg32svg', {}, mtgOptions).then((mtgAppLecteur) => {
      sq.mtgAppLecteur = mtgAppLecteur
      initDom()
      let code, i
      sq.mtgAppLecteur.removeAllDoc()
      sq.mtgAppLecteur.addDoc('mtg32svg', sq.txt, true)
      sq.mtgAppLecteur.calculateAndDisplayAll(true)
      const nbParam = sq.numero
      const t = 'abcd'
      const param = {}
      for (i = 0; i < nbParam; i++) {
        code = sq.mtgAppLecteur.getLatexCode('mtg32svg', i)
        param[t.charAt(i)] = code
      }
      param.e = String(sq.nbEssais)
      const st = (sq.simplifier ? sq.consigne3 : sq.consigne2) + sq.consigne4
      j3pAffiche('enonce', 'texte', sq.consigne1 + st, param)
      // Les 3 lignes suivnates déplacées ici pour que sq.mtgAppLecteur soit prêt quand onkeyup est appelé
      j3pElement('expressioninputmq1').onkeyup = function (ev) {
        onkeyup(ev, parcours)
      }
      j3pFocus('expressioninputmq1')
      parcours.finEnonce()
    }).catch(j3pShowError)
  }

  function initDom () {
    if (sq.titre) parcours.afficheTitre(sq.titre)

    j3pDiv(parcours.zones.MG, {
      id: 'conteneur',
      contenu: '',
      style: parcours.styles.etendre('toutpetit.enonce', { padding: '6px' })
    })
    j3pDiv('conteneur', 'enonce', '')
    j3pDiv('conteneur', { id: 'formules', contenu: '', style: parcours.styles.petit.enonce }) // Contient le formules entrées par l’élève
    j3pDiv('conteneur', 'info', '')
    afficheNombreEssaisRestants()
    j3pDiv('conteneur', 'conteneurbouton', '')
    j3pAjouteBouton('conteneurbouton', 'boutonrecopier', 'MepBoutonsRepere', 'Recopier réponse précédente',
      recopierReponse)
    j3pElement('boutonrecopier').style.display = 'none'

    j3pDiv('conteneur', 'editeur', '') // Le div qui contiendra l’éditeur MathQuill
    $('#editeur').css('padding-top', '10px') // Pour laisser un peu de place au-dessus de l’éditeur
    j3pAffiche('editeur', 'debut', '$' + '$Proposition :  ', {})
    j3pAffiche('editeur', 'expression', '&1&', { inputmq1: {} })
    sq.listeBoutons = ['fraction', 'puissance', 'racine']
    if (sq.charset !== '') {
      mqRestriction('expressioninputmq1', sq.charset, {
        commandes: sq.listeBoutons,
        boundingContainer: parcours.zonesElts.MG
      })
    }
    j3pDiv('conteneur', 'boutonsmathquill', '')
    $('#boutonsmathquill').css('display', 'inline-block')
    $('#boutonsmathquill').css('padding-top', '10px') // Pour laisser un peu de place au-dessus des boutons
    j3pPaletteMathquill('boutonsmathquill', 'expressioninputmq1', { liste: sq.listeBoutons })

    j3pDiv('conteneur', 'divmtg32', '')
    j3pCreeSVG('divmtg32', { id: 'mtg32svg', width: 640, height: 480 })
    const style = parcours.styles.petit.correction
    style.marginTop = '1.5em'
    style.marginLeft = '0.5em'
    j3pAddElt(parcours.zonesElts.MD, 'div', '', { id: 'correction', style })
  }

  function _Donnees () {
    // "primaire" || "lycee" || "college"
    // Actuellement les dimensions sont identiques
    this.typesection = 'lycee'//
    // Nombre de répétitions de l’exercice
    this.nbrepetitions = 1
    this.nbetapes = 1
    this.nbitems = this.nbetapes * this.nbrepetitions

    this.ex = 'Calcul_xSura_Plus_b_Egal_1Sura'
    // Si indication != "" alors un lien "Indication" s’affiche dans la zone Inférieure Gauche (this.zones.IG
    // une indication<br>de deux lignes...";
    this.nbEssais = 6
    this.simplifier = true

    // pour le cas de presentation1bis
    // En effet pas d’indication possible pour cette présentation
    // this.surchargeindication = false;

    // Nombre de chances dont dispose l’él§ve pour répondre à la question
    this.nbchances = 1

    this.textes = {
    }
    this.titre = 'Titre de l’activité'

    this.pe = 0
  }

  switch (this.etat) {
    case 'enonce':
      // code exécuté au lancement de la section
      if (this.debutDeLaSection) {
        this.donneesSection = new _Donnees()

        this.surcharge()
        this.validOnEnter = false // ex donneesSection.touche_entree
        // compteur. Si   numEssai > nbchances, on corrige
        sq.numEssai = 1

        sq.nbEssais = parseInt(parcours.donneesSection.nbEssais)
        sq.nbchances = parseInt(parcours.donneesSection.nbchances)
        sq.nbexp = 0
        sq.reponse = -1
        sq.simplifier = parcours.donneesSection.simplifier

        // Construction de la page
        this.construitStructurePage('presentation1bis')
        if (sq.nbchances > sq.nbEssais) sq.nbchances = sq.nbEssais
        // faut gérer sous-dossier ou pas…
        const sep = this.donneesSection.ex.includes('/') ? '-' : '/'
        j3pImporteAnnexe('squelettes-mtg32' + sep + this.donneesSection.ex + '.js').then(function (datasSection) {
          // console.log("initsection");
          // Attention, le this devient window...
          sq.txt = datasSection.txt
          sq.symbexact = datasSection.symbexact
          sq.symbnonexact = datasSection.symbnonexact
          sq.numero = parseInt(datasSection.numero) // Le nombre de paramètres LaTeX dans le texte
          let s = datasSection.consigne1
          sq.consigne1 = (s !== undefined) ? s : ''
          s = datasSection.consigne2
          sq.consigne2 = (s !== undefined) ? s : ''
          s = datasSection.consigne3
          sq.consigne3 = (s !== undefined) ? s : ''
          s = datasSection.consigne4
          sq.consigne4 = (s !== undefined) ? s : ''
          sq.titre = datasSection.titre
          s = datasSection.charset // Le set de caractères utilisés dans l’éditeur
          sq.charset = (s !== undefined) ? s : ''
          initMtg()
        }).catch(error => {
          console.error(error)
          j3pShowError('Impossible de charger les données de cette exercice')
        })
      } else {
        j3pEmpty('correction')
        sq.numEssai = 1
        $('#editeur').css('display', 'block')
        $('#boutonsmathquill').css('display', 'inline-block')
        sq.mtgAppLecteur.removeDoc('mtg32svg')
        sq.mtgAppLecteur.addDoc('mtg32svg', sq.txt, true)
        const parc = this
        sq.mtgAppLecteur.setEditorCallBackOK('mtg32svg', 'rep', function () {
          validation(parc)
        })
        sq.mtgAppLecteur.setEditorsSize('mtg32svg', '17px')
        sq.mtgAppLecteur.calculate('mtg32svg', true) // true pour que les calculs alèatoires soient rꪮitialisés
        j3pEmpty('enonce')
        j3pEmpty('formules')
        sq.nbexp = 0
        sq.reponse = -1
        const nbParam = sq.numero
        const t = 'abcd'
        const param = {}
        for (let i = 0; i < nbParam; i++) {
          const code = sq.mtgAppLecteur.getLatexCode('mtg32svg', i)
          param[t.charAt(i)] = code
        }
        param.e = String(sq.nbEssais)
        // Consigne différente suivant qu’on demande une solution simplifiée ou non.
        const st = (sq.simplifier ? sq.consigne3 : sq.consigne2) + sq.consigne4
        j3pAffiche('enonce', 'texte', sq.consigne1 + st, param)
        sq.mtgAppLecteur.display('mtg32svg') // Pour tout réafficher
        afficheNombreEssaisRestants()
        j3pElement('info').style.display = 'block'
        // On vide le contenu de l’éditeur MathQuill et on lui donne le focus.
        $('#expressioninputmq1').mathquill('latex', ' ')
        j3pFocus('expressioninputmq1')
        this.finEnonce()
      }

      break // case "enonce":

    case 'correction': {
      let bilanreponse = ''
      let simplifie = false
      // On regarde si le champ d’édition est rempli même si l’utilisateur
      // n’a pas appuyé sur la touche Entrée.
      const ch = j3pValeurde('expressioninputmq1')
      if (ch !== '') {
        const repcalcul = j3pMathquillXcas(ch)
        const valide = sq.mtgAppLecteur.syntaxValidation('mtg32svg', 'rep', repcalcul, true)
        if (valide) validation(this, false)
        else bilanreponse = 'incorrect'
      }
      const res1 = sq.reponse
      if (bilanreponse !== 'incorrect') {
        if (sq.simplifier) { // Si on demande une solution simplifiée
          if (res1 === 1) {
            bilanreponse = 'exact'
            simplifie = true
          } else {
            if (res1 === 2) bilanreponse = 'exactpasfini'
            else bilanreponse = 'erreur'
          }
        } else {
          if ((res1 === 1) || (res1 === 2)) {
            bilanreponse = 'exact'
            if (res1 === 1) simplifie = true
          } else {
            bilanreponse = 'erreur'
          }
        }
      }
      // A cause de la limite de temps :
      if (this.isElapsed) { // limite de temps
        this._stopTimer()
        j3pElement('correction').style.color = this.styles.cfaux
        j3pElement('boutonrecopier').style.display = 'none'
        j3pElement('info').style.display = 'none'
        sq.mtgAppLecteur.executeMacro('mtg32svg', 'solution')
        $('#editeur').css('display', 'none')
        $('#boutonsmathquill').css('display', 'none')
        j3pElement('correction').innerHTML = tempsDepasse
        j3pElement('correction').innerHTML += '<br>La solution se trouve ci-contre.'
        this.etat = 'navigation'
        this.sectionCourante()
      } else {
        if (bilanreponse === 'incorrect') {
          j3pElement('correction').style.color = this.styles.cfaux
          j3pElement('correction').innerHTML = 'Réponse incorrecte'
          marqueEditeurPourErreur()
          j3pFocus('expressioninputmq1')
          this.afficheBoutonValider()
        } else {
          // Une réponse a été saisie
          // Bonne réponse
          if (bilanreponse === 'exact') {
            this._stopTimer()
            sq.mtgAppLecteur.setActive('mtg32svg', false)
            this.score++
            j3pElement('correction').style.color = this.styles.cbien
            if (simplifie) j3pElement('correction').innerHTML = cBien
            else j3pElement('correction').innerHTML = 'C’est bien  mais on pouvait simplifier la solution.'
            j3pElement('boutonrecopier').style.display = 'none'
            j3pElement('info').style.display = 'none'
            sq.mtgAppLecteur.executeMacro('mtg32svg', 'solution')
            $('#editeur').css('display', 'none')
            $('#boutonsmathquill').css('display', 'none')
            this.etat = 'navigation'
            this.sectionCourante()
          } else {
            j3pElement('correction').style.color = this.styles.cfaux
            sq.numEssai++
            if (bilanreponse === 'exactpasfini') {
              j3pElement('correction').innerHTML = 'La solution est bonne mais pas écrite sous la forme demandée.'
            } else {
              if (res1 === 3) j3pElement('correction').innerHTML = 'L’équation n’est pas résolue.'
              else j3pElement('correction').innerHTML = cFaux
            }
            afficheNombreEssaisRestants()
            // On donne le focus à l’éditeur MathQuill
            j3pFocus('expressioninputmq1')

            // S’il y a plus de deux chances,on appelle à nouveau le bouton Valider
            if ((sq.nbexp < sq.nbEssais) && (sq.numEssai <= sq.nbchances)) {
              j3pElement('correction').innerHTML += '<br>' + '<br>' + essaieEncore
              sq.mtgAppLecteur.setActive('mtg32svg', true)
            } else {
              // Erreur au nème essai
              this._stopTimer()
              j3pElement('boutonrecopier').style.display = 'none'
              j3pElement('info').style.display = 'none'
              sq.mtgAppLecteur.executeMacro('mtg32svg', 'solution')
              sq.mtgAppLecteur.setActive('mtg32svg', false)
              // j3pElement("correction").innerHTML+='<br>' + regardeCorrection;
              j3pElement('correction').innerHTML += '<br><br>La solution se trouve ci-contre.'
              this.etat = 'navigation'
              this.sectionCourante()
            }
          }
        }
      }
      // Obligatoire
      this.finCorrection()
      break // case "correction":
    }

    case 'navigation':

      if (this.sectionTerminee()) {
        // On détermine la phrase d’état renvoyée par la section
        this.parcours.pe = this.score / this.donneesSection.nbitems
        this.afficheBoutonSectionSuivante()
        this.focus('sectioncontinuer')
      } else {
        this.etat = 'enonce'
        this.afficheBoutonSuite()
        this.focus('boutoncontinuer')
      }
      // Obligatoire
      this.finNavigation()

      break // case "navigation":
  }
}
