import $ from 'jquery'

import { j3pAddElt, j3pAjouteBouton, j3pDetruit, j3pDiv, j3pElement, j3pEmpty, j3pImporteAnnexe, j3pPaletteMathquill, j3pShowError } from 'src/legacy/core/functions'
import { unLatexify } from 'src/lib/mathquill/functions'
import { MathfieldElement, renderMathInDocument } from 'mathlive'
import { getMtgApp } from 'src/lib/outils/mathgraph'
import { j3pCreeSVG } from 'src/legacy/core/functionsSvg'
import { cleanLatexForMl, mathliveRestrict, replaceOldMQCodes } from 'src/lib/outils/mathlive/utils'
import { afficheMathliveDans, getMathliveValue } from 'src/lib/outils/mathlive/display'
import { setMathliveCss } from 'src/lib/outils/mathlive/mathlivecss'
import textesGeneriques from 'src/lib/core/textes'

import { setFocusById, setFocusToFirstInput } from 'src/legacy/sections/squelettes/helpersMtg'

const { cBien, cFaux, essaieEncore, tempsDepasse } = textesGeneriques

/*
    Yves Biton
    Juillet 2019

    squelettemtg32_Op_Matrices_Param
    Squelette demandant de calculer le produit de deux matrices
    On peut éventuellement spécifier des paramètres  a,b,c,d,e,f,g,h,j,k,n,n,p.
    Utilise une fichier annexe dont le nom est contenu dans le paramètre ex.
    Il contient aussi une variable formulaire qui est une chaîne de caractères contenant éventuellement du LaTeX et contenant
    par exemple comme sous-chaîne contenant la chaine matrix.
    Si, par exemple, on attend comme résultat une matrice 2x2, matrix sera remplacé par la commenade MathQuill
    \matrixtwotwo{\editable}{\editable}{\editable}{\editable}
    Dans ce squelette on ne doit utiliser que des editable donc pas de &1& par exemple.
    La figure mtg32 associée doit contenir deux calculs nommés nbcol et nblig contenant respectivement le nombre de colonnes
    et le nombre de lignes de la matrice à calculer
    Quand on vérifiera la validité de la réponse de l’élève, on mettra la réponse
    contenue dans l’éditeur 1 dans le calcul rep1 et, après recalcul de la figure mtg32, le calcul resolu1 contiendra 1
    si la réponse contenue dans le premier éditeur est la réponse finale, 0 sinon.
    Le calcul exact1 contiendra lui 1 si la réponse est bonne à epsilon près.
    Si la paramètre simplifier est à false, un calcul exact mais pas fini sera accepté.
    Si, par exemple, il s’agit d’un produit de deux matrices 2*2 donc si le résultat est une matrice 2x2, la figure mtg32
    devra contenir quatre calculs rep1, rep2, rep3 et rep4, rep1 et rep2 correspondant à la première ligne de la matrice
    et rep3 et rep4 à la seconde.
    Si la figure contenue  dans ex contient un paramètre nbvar , alors dans le cas ou nbrepetitions > 1, on donne pour i compris entre 1 et npParamAleat
    au calcul de la figure nommé "r"+i (par exemple r1 et r2 si nbParamAleat vaut 2) des valeurs comprises entre 0 et "nbcas"+i - 1
    toutes différentes (sauf si nbrepetitions > b=nbcas auquel cas au bout de nbcas répétitions il y aura nécessairement une valeur de r déjà prise.
    Le paramètre nbLatex contient le nombre d’affichages LaTeX qui doivent être récupérés de la figure pour affichage dans l’énoncé.

*/

// on définit le dossier des fonts mathlive (il faut le faire très tôt,
// ici c’est bon avec pnpm start mais trop tard pour un build (cf src/lib/player/index.js de la branche draftPlayer)
if (MathfieldElement) { // undefined dans les tests avec happy-dom
  MathfieldElement.fontsDirectory = '/static/mathlive/fonts'
  MathfieldElement.soundsDirectory = '/static/mathlive/sounds'
}

export const params = {
  /**
   * Ce qui est paramétrable dans l’éditeur de graphe
   * @type {LegacySectionParamsParametres}
   */
  parametres: [
    ['nbrepetitions', 2, 'entier', 'aucun'],
    ['limite', 0, 'entier', 'Temps disponible par répétition, en secondes'],
    ['nbEssais', 6, 'entier', 'Nombre d’essais maximum autorisés'],
    ['indicationfaute', false, 'boolean', 'true si on veut que certaines fautes soient expliquées après appui sur Entrée'],
    ['simplifier', true, 'boolean', 'true si on exige un forme donnée, false sinon'],
    ['a', 'random', 'string', 'Valeur de a'],
    ['b', 'random', 'string', 'Valeur de b'],
    ['c', 'random', 'string', 'Valeur de c'],
    ['d', 'random', 'string', 'Valeur de d'],
    ['e', 'random', 'string', 'Valeur de e'],
    ['f', 'random', 'string', 'Valeur de f'],
    ['g', 'random', 'string', 'Valeur de g'],
    ['h', 'random', 'string', 'Valeur de h'],
    ['j', 'random', 'string', 'Valeur de j'],
    ['k', 'random', 'string', 'Valeur de k'],
    ['l', 'random', 'string', 'Valeur de l'],
    ['m', 'random', 'string', 'Valeur de m'],
    ['n', 'random', 'string', 'Valeur de n'],
    ['o', 'random', 'string', 'Valeur de o'],
    ['p', 'random', 'string', 'Valeur de p'],
    ['q', 'random', 'string', 'Valeur de q'],
    ['r', 'random', 'string', 'Valeur de r'],
    ['s', 'random', 'string', 'Valeur de s'],
    ['x', 'random', 'string', 'Valeur de x'],
    ['y', 'random', 'string', 'Valeur de y'],
    ['z', 'random', 'string', 'Valeur de z'],
    ['ex', 'terminale_expert/Matrices_Produit_Mat_2_2', 'string', 'Nom du fichier annexe (xxx => squelettes-mtg32/xxx.js et xxx/yyy => squelettes-mtg32-xxx/yyy.js)']
  ]
}

/**
 * section squelettemtg32_Op_Matrices_Param
 * @this {Parcours}
 */
export default function main () {
  const parcours = this
  const sq = this.storage

  function onkeyup () {
    const indice = this.id // Id de l’éditeur
    if (sq.marked[indice]) {
      demarqueEditeurPourErreur(indice)
    }
  }

  function getPromptValue (id) {
    // On n’utilise pas de forEach qui ne permete pas de sortir de la boucle
    const sel = j3pElement('expression').querySelectorAll('math-field')
    for (const value of sel.entries()) {
      const mf = value[1]
      const prompts = mf.getPrompts()
      const ind = prompts.indexOf(id)
      if (ind !== -1) {
        return mf.getPromptValue(id, 'latex-unstyled').replace(/\\placeholder{}/g, '')
      }
    }
    console.error('id prompt invalide : ' + id)
    return ''
  }

  function traiteMathlive (ch) {
    // La première ligne est nécessaire pour cette section
    ch = ch.replace(/\\R/g, '_R').replace(/\\emptyset/g, '∅')
    // Mathlive ne met pas d’accolades pour les fractions dont le numérateur et le dénominateur sont des entiers
    ch = ch.replace(/\\frac(\d)(\d)/g, '\\frac{$1' + '}{' + '$2}')
    // Ni pour les racines carrées d’un entier
    ch = ch.replace(/\\sqrt(\d)/g, '\\sqrt{$1}')
    // Pour les puissances dont l’exposant est un chiffre, Mathlive ne met pas d’accolades
    ch = ch.replace(/\^([^{])/g, '^{$1}')
    ch = unLatexify(ch)
    // mathlive peut mettre des {} vides
    ch = ch.replace(/\\{\\}/g, '')
    ch = ch.replace(/\^(\d)(\d)/g, '^$1*$2') // On peut avoir par exemple 2^35^4 pour 2^3*5^4
    return sq.mtgAppLecteur.addImplicitMult('mtg32svg', ch)// Traitement des multiplications implicites
  }

  /**
   * Fonction extrayant d’un affichage LaTeX de la figure les lignes de texte et les transformant
   * en une chaîne affichable par MathQuill
   * @param tagLatex : le tag de l’affichage LaTeX de la figure
   * @return {string}
   */
  function extraitDeLatexPourMathlive (tagLatex) {
    const list = sq.mtgAppLecteur.getList('mtg32svg')
    const latex = list.getByTag(tagLatex, true)
    if (latex === null || latex.className !== 'CLatex') return ''
    return cleanLatexForMl(latex.chaineLatex, sq.charset)
  }

  function afficheNombreEssaisRestants () {
    j3pEmpty('info')
    const nbe = sq.nbEssais - sq.numEssai + 1
    if (nbe === 1) {
      afficheMathliveDans('info', 'texteinfo', 'Il reste une validation.', { style: { color: '#7F007F' } })
    } else {
      afficheMathliveDans('info', 'texteinfo', 'Il reste ' + nbe + ' essais.', { style: { color: '#7F007F' } })
    }
  }

  function marqueEditeurPourErreur (id) {
    // Il faut regarder si l’erreur a été rencontrée dans un placeHolder ou dans un éditeur Mathlive classique
    let editor // Peut être aussi bien un éditeur de texte qu’un éditeur Mathlive
    if (id.includes('ph')) {
      const ind = id.indexOf('ph')
      const idmf = id.substring(0, ind) // L’id de l’éditeur MathLive contenant le placeHolder
      editor = j3pElement(idmf)
      sq.marked[idmf] = true
      editor.setPromptState(id, 'incorrect')
    } else {
      editor = j3pElement(id)
      sq.marked[id] = true
      editor.style.backgroundColor = '#FF9999'
    }
    // On donne à l’éditeur une fonction demarquePourError qui lui permettra de se démarquer quand
    // on utilise le clavier virtuel pour le modifier
    editor.demarqueErreur = () => {
      demarqueEditeurPourErreur(editor.id)
    }
  }

  function demarqueEditeurPourErreur (id) {
    // if ($.isEmptyObject(sq.marked)) return
    if (JSON.stringify(sq.marked) === '{}') return // On teste si l’objet est vide
    sq.marked[id] = false // Par précaution si appel via bouton recopier réponse trop tôt
    // On regarde si l’éditeur où a été trouvée l’erreur est un placeHolder
    const editor = j3pElement(id) // Peut être aussi bien un éditeur Mathlive qu’un éditeur de texte
    if (editor.classList.contains('PhBlock')) {
      editor.getPrompts().forEach((id) => {
        editor.setPromptState(id, 'undefined')
      })
    } else {
      editor.style.backgroundColor = 'white'
    }
  }

  function traiteFormulaire (form) {
    // On traite d’abord les champs MathQuill normaux
    for (let k = 1; k <= sq.nbCalc; k++) form = form.replace('#edit' + k, '&' + k + '&')
    // Puis les champs d’édition des cellules de la matrice
    for (let k = 1; k <= sq.nbMatrices; k++) {
      let ch = ''
      for (let i = 0; i < sq['nblig' + k]; i++) {
        for (let j = 0; j < sq['nbcol' + k]; j++) {
          ch += '{\\editable{}}'
        }
      }
      form = form.replace('#matrice' + k, controlSequence(k) + ch)
    }
    return replaceOldMQCodes(form)
  }

  // Fonctioon renvoyant la séquence de contrôle LaTeX pour la matrice n°k de bonnes dimensions
  function controlSequence (k) {
    const rowValue = sq['nblig' + k]
    const colValue = sq['nbcol' + k]
    switch (rowValue) {
      case 1 :
        switch (colValue) {
          case 2 :
            return '\\matrixonetwo'
          case 3 :
            return '\\matrixonethree'
          default:
            return console.error(Error('cas non prévu avec nbcol' + k + ' qui vaut ni 2 ni 3 : ' + colValue))
        }
      case 2 :
        switch (colValue) {
          case 1 :
            return '\\matrixtwoone'
          case 2 :
            return '\\matrixtwotwo'
          case 3 :
            return '\\matrixtwothree'
          default:
            return console.error(Error('cas non prévu avec nbcol' + k + ' qui vaut ni 1 ni 2 ni 3 : ' + colValue))
        }
      case 3 :
        switch (sq['nbcol' + k]) {
          case 1 :
            return '\\matrixthreeone'
          case 2 :
            return '\\matrixthreetwo'
          case 3 :
            return '\\matrixthreethree'
          default:
            return console.error(Error('cas non prévu avec nbcol' + k + ' qui vaut ni 1 ni 2 ni 3 : ' + colValue))
        }
      default:
        return console.error(Error('cas non prévu avec nblig' + k + ' qui vaut ni 1 ni 2 ni 3 : ' + rowValue))
    }
  }

  /**
   * Fonction affichant la solution s’il existe un affichage LaTeX de tag solution
   * @param{boolean} bexact true si l’élève a entré la bonne réponse, false sinon
   */
  function afficheSolution (bexact) {
    $('#divSolution').css('display', 'block').html('')
    const ch = extraitDeLatexPourMathlive('solution')
    if (ch !== '') {
      afficheMathliveDans('divSolution', 'solution', ch, {
        style: { color: bexact ? 'green' : 'blue' },
        replacefracdfrac: true
      })
    }
  }

  function cacheSolution () {
    $('#divSolution').css('display', 'none').html('')
  }

  /*
  function prepareEditeursMatrices () {
    // var select = $('#expression').find('.mq-editable-field')
    // Select contient un tableau des éditeurs editable
    sq.tabConversionIndices = [] // Tabelua de conversion car les réponses sont vérifiées par ligne
    for (let k = 1; k <= sq.nbMatrices; k++) {
      for (let i = 0; i < sq['nblig' + k]; i++) {
        for (let j = 0; j < sq['nbcol' + k]; j++) {
          // var edit = select[i + j * sq['nblig' + k] + sq['decalageEditeurMatrice' + k]]
          sq.tabConversionIndices.push(i + j * sq['nblig' + k] + nbCellules(k - 1))
          const indice = i * sq['nbcol' + k] + j
          const idEditeur = 'expressioninputmqef' + String(nbCellules(k - 1) + indice + 1)
          const edit = j3pElement(idEditeur)
          sq.marked[idEditeur] = false

          if (sq.charset !== '') {
            mqRestriction(edit, sq.charset, {
              commandes: sq.listeBoutons,
              boundingContainer: parcours.zonesElts.MG
            })
          }
          $(edit).focusin(function () {
            ecoute.call(this)
          })
          $(edit).keyup(function () {
            onkeyup.call(this)
          })
        }
      }
    }
  }
   */

  function prepareEditeursMatrices () {
    sq.tabConversionIndices = [] // Tableau de conversion car les réponses sont vérifiées par ligne
    for (let k = 1; k <= sq.nbMatrices; k++) {
      for (let i = 0; i < sq['nblig' + k]; i++) {
        for (let j = 0; j < sq['nbcol' + k]; j++) {
          // var edit = select[i + j * sq['nblig' + k] + sq['decalageEditeurMatrice' + k]]
          sq.tabConversionIndices.push(i + j * sq['nblig' + k] + nbCellules(k - 1))
          const indice = i * sq['nbcol' + k] + j
          const idEditeur = 'expressionPhBlock' + k + 'ph' + String(nbCellules(k - 1) + indice + 1)
          sq.marked[idEditeur] = false
        }
      }
    }

    j3pElement('expression').querySelectorAll('.PhBlock').forEach((mf) => {
      const id = mf.id
      if (sq.charset !== '') {
        mathliveRestrict(id, sq.charset)
      }
      mf.addEventListener('focusin', () => {
        ecoute.call(mf)
      })
      mf.addEventListener('keyup', () => {
        onkeyup.call(mf)
      })
    })
  }

  function prepareEditeursMathQuill () {
    for (let k = 1; k <= sq.nbCalc; k++) {
      // Dans ce squelette indice est en fait l’id de l’éditeur
      const id = 'expressioninputmq' + k
      if (sq.charset !== '') {
        mathliveRestrict(id, sq.charset)
      }
      $('#' + id).focusin(function () {
        ecoute.call(this)
      })
      j3pElement(id).onkeyup = function () {
        onkeyup.call(this)
      }
    }
  }

  /**
   * Fonction renvoyant le nombre de cellules de la matrice n°k
   * @param k
   */
  function nbCellules (k) {
    if (k === 0) return 0
    else return sq['nblig' + k] * sq['nbcol' + k]
  }

  function validationEditeurs () {
    let ind, chcalcul, valide, rep, k
    let res = true
    let focusDonne = false
    // On commence par traiter les champs MathQuill normaux s’il y en a
    for (ind = 1; ind <= sq.nbCalc; ind++) {
      const idEditeur = 'expressioninputmq' + ind
      rep = getMathliveValue('expressioninputmq' + ind)
      if (rep === '') {
        res = res && false
        marqueEditeurPourErreur(idEditeur)
        if (!focusDonne) {
          focusDonne = true
          setFocusById(idEditeur)
        }
      } else {
        chcalcul = traiteMathlive(rep)
        valide = sq.mtgAppLecteur.syntaxValidation('mtg32svg', 'repmq' + ind, chcalcul, true)
        if (!valide) {
          res = false
          marqueEditeurPourErreur(idEditeur)
          if (!focusDonne) {
            focusDonne = true
            setFocusById(idEditeur)
          }
        }
      }
    }
    if (!res) return false
    // Puis les champs d’édition des matrices
    let inded = 0
    for (k = 1; k <= sq.nbMatrices; k++) {
      for (ind = 1; ind <= nbCellules(k); ind++) {
        inded++
        // var rep = j3pValeurde('expressioninputcell' + ind)
        // idEditeur = 'expressioninputmqef' + String(nbCellules(k - 1) + ind)
        const idPrompt = 'expressionPhBlock' + k + 'ph' + inded
        // rep = $('#' + idEditeur).mathquill('latex')
        rep = getPromptValue(idPrompt)
        if (rep === '') {
          res = res && false
          marqueEditeurPourErreur(idPrompt)
          if (!focusDonne) {
            focusDonne = true
            setFocusById(idPrompt)
          }
        } else {
          chcalcul = traiteMathlive(rep)
          valide = sq.mtgAppLecteur.syntaxValidation('mtg32svg', 'rep' + ind, chcalcul, true)
          if (!valide) {
            res = false
            marqueEditeurPourErreur(idPrompt)
            if (!focusDonne) {
              focusDonne = true
              setFocusById(idPrompt)
            }
          }
        }
      }
    }
    return res
  }

  function montreEditeurs (bVisible) {
    if (bVisible) $('#info').css('display', 'block')
    $('#editeur').css('display', bVisible ? 'block' : 'none')
    if (sq.boutonsMathQuill) $('#boutonsmathquill').css('display', bVisible ? 'block' : 'none')
    // On vide le contenu de l’éditeur MathQuill et on lui donne le focus.
    if (bVisible) {
      setFocusToFirstInput('expression')
    }
  }

  /*
  function videEditeurs () {
    let i, k, idEditeur
    for (i = 1; i <= sq.nbCalc; i++) {
      demarqueEditeurPourErreur('expressioninputmq' + i)
      $('#expressioninputmq' + i).mathquill('latex', ' ')
    }
    for (k = 1; k <= sq.nbMatrices; k++) {
      for (i = 1; i <= nbCellules(k); i++) {
        idEditeur = 'expressioninputmqef' + String(nbCellules(k - 1) + i)
        demarqueEditeurPourErreur(idEditeur)
        // Ci-dessous plus nécessaire avec nouveau MathQuill
        // $('#' + idEDiteur).mathquill('latex', ' ').focus().blur() // focus nécessaire sinon éditeur riquiqui
        $('#' + idEditeur).mathquill('latex', ' ')
      }
    }
    // On donne le focus au premier éditeur MathQuill
    j3pFocus(idPremierEditeur())
  }
   */

  function videEditeurs () {
    /*
      for (let i = 1; i <= sq.nbCalc; i++) {
        // $('#expressioninputmq' + i).mathquill('latex', ' ')
        j3pElement('expressioninputmq' + i).value = ''
      }
      for (let i = 1; i <= sq.nbEditable; i++) {
        j3pElement('expressioninputmqef' + i).value = ''
      }
                                                 */
    // On vide les éditeurs simples mais on ne vide dans les blocs de editable que les editable
    const el = j3pElement('expression')
    el.querySelectorAll('math-field').forEach(mf => {
      if (!mf.id.includes('PhBlock')) mf.value = ''
    })
    el.querySelectorAll('.PhBlock').forEach((mf) => {
      mf.getPrompts().forEach((id) => {
        mf.setPromptValue(id, '', 'latex')
      })
      // mf.stripPromptContent() // Quand on vide l’apparence du placeholder n’est plus visible
      const idFirstEdit = mf.getPrompts()[0]
      mf.setPromptValue(idFirstEdit, '')
    })
    for (let i = 1; i <= sq.nbList; i++) {
      if (sq.listesLatex) {
        sq['listeDeroulante' + i].reset()
      } else {
        j3pElement('expressionliste' + i).selectedIndex = 0
      }
    }
    for (let i = 1; i <= sq.nbTextEdit; i++) {
      $('#expressioninput' + i).val('')
    }
    // On donne le focus au premier éditeur MathQuill
    setFocusToFirstInput('expression')
  }

  function afficheReponse (bilan, depasse) {
    let coul, ch
    const num = sq.numEssai
    const idrep = 'exp' + num
    if (num > 2) afficheMathliveDans('formules', idrep + 'br', '<br>')

    if (bilan === 'exact') {
      ch = 'Réponse exacte : '
      coul = parcours.styles.cbien
    } else {
      if (bilan === 'faux') {
        ch = 'Réponse fausse : '
        coul = parcours.styles.cfaux
      } else { // Exact mais pas fini
        ch = 'Exact pas fini : '
        if (depasse) {
          coul = parcours.styles.cfaux
        } else {
          coul = '#0000FF'
        }
      }
    }
    afficheMathliveDans('formules', idrep + 'deb', ch, {
      style: {
        color: coul
      }
    })

    ch = sq.formulaire
    for (let i = 1; i <= sq.nbCalc; i++) {
      const str = '\\textcolor{' + (sq.repResolumq[i - 1] ? parcours.styles.cbien : (sq.repExactmq[i - 1] ? '#0000FF' : parcours.styles.cfaux)) + '}{'
      ch = ch.replace('#edit' + i, '$' + str + sq.repmq[i - 1] + '}$')
    }

    let k = 0
    for (let m = 1; m <= sq.nbMatrices; m++) {
      let st = ''
      for (let i = 0; i < nbCellules(m); i++) {
        st += '{\\textcolor{' + (sq.repResolu[k] ? parcours.styles.cbien : (sq.repExact[k] ? '#0000FF' : parcours.styles.cfaux)) + '}{' + sq.rep[sq.tabConversionIndices[k]] + '}}'
        k++
      }
      ch = ch.replace('#matrice' + m, controlSequence(m) + st)
    }
    afficheMathliveDans('formules', idrep, ch, sq.paramFormulaire)
  }

  function ecoute () {
    if (sq.boutonsMathQuill) {
      j3pEmpty('boutonsmathquill')
      j3pPaletteMathquill('boutonsmathquill', this.id, {
        liste: sq.listeBoutons,
        nomdiv: 'palette'
      })
      // Ligne suivante pour un bon alignement
      $('#palette').css('display', 'inline-block')
    }
  }

  function validation () {
    let exact, resolu, auMoinsUnExact, ind, rep, res, ex, faute, chcalcul, k, decalageCell
    if (sq.numEssai >= 1) {
      j3pElement('boutonrecopier').style.display = 'block'
    }
    // Pour la matrice
    sq.rep = []
    sq.repExact = []
    sq.repResolu = []
    // Pour les autres chmps MathQuill
    sq.repmq = []
    sq.repExactmq = []
    sq.repResolumq = []

    for (ind = 1; ind <= sq.nbCalc; ind++) {
      rep = getMathliveValue('expressioninputmq' + ind)
      sq.repmq.push(rep)
      chcalcul = traiteMathlive(rep)
      sq.mtgAppLecteur.giveFormula2('mtg32svg', 'repmq' + ind, chcalcul)
    }
    decalageCell = 0
    let inded = 0
    for (k = 1; k <= sq.nbMatrices; k++) {
      for (ind = 1; ind <= nbCellules(k); ind++) {
        inded++
        // const idEditeur = 'expressioninputmqef' + String(nbCellules(k - 1) + ind)
        const idEditeur = 'expressionPhBlock' + k + 'ph' + inded
        rep = getPromptValue(idEditeur)
        sq.rep.push(rep)
        chcalcul = traiteMathlive(rep)
        sq.mtgAppLecteur.giveFormula2('mtg32svg', 'rep' + String(sq.tabConversionIndices[decalageCell + ind - 1] + 1), chcalcul)
      }
      decalageCell += nbCellules(k)
    }
    sq.mtgAppLecteur.calculate('mtg32svg', false)
    if (sq.indicationfaute) {
      faute = sq.mtgAppLecteur.valueOf('mtg32svg', 'faute')
      if (faute === 1) {
        sq.mtgAppLecteur.executeMacro('mtg32svg', 'voirFaute')
      } else {
        sq.mtgAppLecteur.executeMacro('mtg32svg', 'masquerFaute')
      }
    }
    exact = true
    resolu = true
    auMoinsUnExact = false
    for (ind = 1; ind <= sq.nbCalc; ind++) {
      res = sq.mtgAppLecteur.valueOf('mtg32svg', 'resolumq' + ind)
      ex = sq.mtgAppLecteur.valueOf('mtg32svg', 'exactmq' + ind)
      resolu = resolu && (res === 1)
      exact = exact && ((res === 1) || (ex === 1))
      sq.repResolumq.push(res === 1)
      sq.repExactmq.push((res === 1) || (ex === 1))
      auMoinsUnExact = auMoinsUnExact || (ex === 1)
    }
    decalageCell = 0
    for (k = 1; k <= sq.nbMatrices; k++) {
      for (ind = 1; ind <= nbCellules(k); ind++) {
        res = sq.mtgAppLecteur.valueOf('mtg32svg', 'resolu' + String(ind + decalageCell))
        ex = sq.mtgAppLecteur.valueOf('mtg32svg', 'exact' + String(ind + decalageCell))
        resolu = resolu && (res === 1)
        exact = exact && ((res === 1) || (ex === 1))
        sq.repResolu.push(res === 1)
        sq.repExact.push((res === 1) || (ex === 1))
        auMoinsUnExact = auMoinsUnExact || (ex === 1)
      }
      decalageCell += nbCellules(k)
    }
    sq.resolu = resolu
    sq.exact = exact && auMoinsUnExact
  }

  function recopierReponse () {
    let ind, k
    for (ind = sq.nbCalc; ind > 0; ind--) {
      demarqueEditeurPourErreur('expressioninputmq' + ind)
      j3pElement('expressioninputmq' + ind).value = sq.repmq[ind - 1]
    }
    let inded = 0
    for (k = 1; k <= sq.nbMatrices; k++) {
      for (ind = nbCellules(k); ind > 0; ind--) {
        // const idEditeur = 'expressioninputmqef' + String(nbCellules(k - 1) + ind)
        inded++
        const idEditeur = 'expressionPhBlock' + k
        const idPrompt = idEditeur + 'ph' + inded
        demarqueEditeurPourErreur(idEditeur)
        j3pElement(idEditeur).setPromptValue(idPrompt, sq.rep[inded - 1])
      }
    }
    setFocusToFirstInput('expression')
  }

  function initMtg () {
    setMathliveCss('MepMG')
    const mtgOptions = {
      // player only, sans figure initiale ni svg, on gère ça dans initMtg et modif_fig
      loadCoreWithMathJax: true
    }
    getMtgApp('mtg32svg', {}, mtgOptions).then((mtgAppLecteur) => {
      sq.mtgAppLecteur = mtgAppLecteur
      initDom()
      sq.marked = {} // Objet dont les membres seron associés aux ide dess éditeurs avec valeur boolean
      let code, par, car, i, j, nbrep, ar, tir, nb, k
      sq.mtgAppLecteur.removeAllDoc()
      sq.mtgAppLecteur.addDoc('mtg32svg', sq.txt, true)
      sq.mtgAppLecteur.calculate('mtg32svg', false)
      const ch = 'abcdefghjklmnopqrsxyz'
      const nbvar = sq.mtgAppLecteur.valueOf('mtg32svg', 'nbvar')
      if (nbvar !== -1) {
        nbrep = parcours.donneesSection.nbrepetitions
        sq.aleat = true
        sq.nbParamAleat = nbvar
        for (i = 1; i <= nbvar; i++) {
          const nbcas = sq.mtgAppLecteur.valueOf('mtg32svg', 'nbcas' + i)
          nb = Math.max(nbrep, nbcas)
          ar = []
          for (j = 0; j < nb; j++) ar.push(j % nbcas)
          sq['tab' + i] = []
          for (k = 0; k < nbrep; k++) {
            tir = Math.floor(Math.random() * ar.length)
            sq['tab' + i].push(ar[tir])
            ar.splice(tir, 1)
          }
          sq.mtgAppLecteur.giveFormula2('mtg32svg', 'r' + i, sq['tab' + i][0])
        }
      } else {
        sq.aleat = false
      }

      if (sq.param !== undefined) {
        for (i = 0; i < ch.length; i++) {
          car = ch.charAt(i)
          if (sq.param.indexOf(car) !== -1) {
            par = parcours.donneesSection[car]
            if (par !== 'random') sq.mtgAppLecteur.giveFormula2('mtg32svg', car, par)
          }
        }
      }
      sq.mtgAppLecteur.calculate('mtg32svg', true)
      for (k = 1; k <= sq.nbMatrices; k++) {
        sq['nbcol' + k] = sq.mtgAppLecteur.valueOf('mtg32svg', 'nbcol' + k)
        sq['nblig' + k] = sq.mtgAppLecteur.valueOf('mtg32svg', 'nblig' + k)
      }
      const nbParam = parseInt(sq.nbLatex)
      const t = 'abcdefghijklmnopq'
      const param = {}
      for (i = 0; i < nbParam; i++) {
        code = sq.mtgAppLecteur.getLatexCode('mtg32svg', i)
        param[t.charAt(i)] = code
      }

      const st = replaceOldMQCodes(sq.consigne1 + (sq.simplifier ? sq.consigne3 : sq.consigne2) + sq.consigne4)
      afficheMathliveDans('enonce', 'texte', st, param)
      sq.indicationfaute = parcours.donneesSection.indicationfaute // true si indication quand certaines fautes sont faites et appui sur Entrée
      const formulaire = traiteFormulaire(sq.formulaire)
      const obj = {}
      sq.paramFormulaire = {}
      for (k = 1; k <= sq.nbCalc; k++) obj['inputmq' + k] = {}
      for (i = 0; i < nbParam; i++) {
        code = sq.mtgAppLecteur.getLatexCode('mtg32svg', i)
        obj[t[i]] = code
        sq.paramFormulaire[t[i]] = code
      }
      obj.styletexte = {}
      obj.charset = sq.charset
      obj.listeBoutons = sq.listeBoutons
      obj.transposeMat = true
      afficheMathliveDans('editeur', 'expression', formulaire, obj) // sq.formulaire contient ou plusieurs éditeurs MathQuill
      // On affecte un indice à chaque éditeur ainsi qu’un écouteur de focus
      prepareEditeursMatrices()
      prepareEditeursMathQuill()
      sq.mtgAppLecteur.display('mtg32svg')
      if (sq.boutonsMathQuill) {
        j3pPaletteMathquill('boutonsmathquill', 'expressioninputcell1', { liste: sq.listeBoutons, nomdiv: 'palette' })
        // Ligne suivante pour un bon alignement
        $('#palette').css('display', 'inline-block')
      } else {
        j3pDetruit('boutonsmathquill')
      }
      // j3pFocus('expressioninputmq1')
      // Ne pas appeler ici j3pFocus car les id des éditeurs de la matrice ne sont pas du type inputmq
      setFocusToFirstInput('expression')
      parcours.finEnonce()
    }).catch(j3pShowError)
  }

  function initDom () {
    renderMathInDocument({ renderAccessibleContent: '' }) // Indispensable
    if (sq.titre) parcours.afficheTitre(sq.titre)

    j3pDiv(parcours.zones.MG, {
      id: 'conteneur',
      contenu: '',
      style: parcours.styles.etendre('toutpetit.enonce', { padding: '6px' })
    })
    j3pDiv('conteneur', 'enonce', '')
    j3pDiv('conteneur', {
      id: 'formules',
      contenu: '',
      style: parcours.styles.petit.enonce
    }) // Contient le formules entrées par l’élève
    j3pDiv('conteneur', 'info', '')
    afficheNombreEssaisRestants()
    j3pDiv('conteneur', 'conteneurbouton', '')
    j3pAjouteBouton('conteneurbouton', 'boutonrecopier', 'MepBoutonsRepere', 'Recopier réponse précédente',
      recopierReponse)
    j3pElement('boutonrecopier').style.display = 'none'
    j3pDiv('conteneur', 'editeur', '') // Le div qui contiendra la chaine avec le ou les éditeurs MathQuill
    $('#editeur').css('padding-top', '10px') // Pour laisser un peu de place au-dessus de l’éditeur
    j3pDiv('conteneur', 'boutonsmathquill', '')
    $('#boutonsmathquill').css('display', 'inline-block').css('padding-top', '10px') // Pour laisser un peu de place au-dessus des boutons
    j3pAddElt('conteneur', 'div', '', { id: 'divSolution' })
    $('#divSolution').css('display', 'none')
    j3pDiv('conteneur', 'divmtg32', '')
    j3pCreeSVG('divmtg32', {
      id: 'mtg32svg',
      width: sq.width,
      height: sq.height
    })
    const style = parcours.styles.petit.correction
    style.marginTop = '1.5em'
    style.marginLeft = '0.5em'
    j3pAddElt(parcours.zonesElts.MD, 'div', '', { id: 'correction', style })
    // Ne pas appeler ici j3pFocus car les id des éditeurs de la matrice ne sont pas du type inputmq
  }

  switch (this.etat) {
    case 'enonce':
      // code exécuté au lancement de la section
      if (this.debutDeLaSection) {
        // Construction de la page
        parcours.construitStructurePage('presentation1bis')
        // compteur.
        sq.numEssai = 1
        sq.nbexp = 0

        sq.nbEssais = parseInt(parcours.donneesSection.nbEssais)
        sq.simplifier = parcours.donneesSection.simplifier ?? true
        // faut gérer sous-dossier ou pas…
        const sep = this.donneesSection.ex.includes('/') ? '-' : '/'
        j3pImporteAnnexe('squelettes-mtg32' + sep + this.donneesSection.ex + '.js').then(function (datasSection) {
          // console.log("initsection");
          // Attention, le this devient window...
          sq.txt = datasSection.txt
          sq.nbLatex = Number(datasSection.nbLatex ?? 0) // Le nombre de paramètres LaTeX dans le texte
          // On crée une liste formée des id des boutons autorisés
          const listeBoutons = []
          // On commence par les boutons qui sont activés par défaut
          const tab1 = ['puissance', 'fraction']
          ;['btnPuis', 'btnFrac'].forEach((p, i) => {
            const b = Boolean(datasSection[p] ?? true)
            if (b) listeBoutons.push(tab1[i])
          })
          // Puis les boutons désactivés par défaut
          const tab2 = ['pi', 'racine', 'exp', 'ln', 'sin', 'cos', 'tan', 'abs', 'conj']
          ;['btnPi', 'btnRac', 'btnExp', 'btnLn', 'btnSin', 'btnCos', 'btnTan', 'btnAbs', 'btnConj'].forEach((p, i) => {
            const b = Boolean(datasSection[p] ?? false)
            if (b) listeBoutons.push(tab2[i])
          })
          sq.listeBoutons = listeBoutons
          sq.boutonsMathQuill = listeBoutons.length !== 0
          sq.consigne1 = datasSection.consigne1 ?? ''
          sq.consigne2 = datasSection.consigne2 ?? ''
          sq.consigne3 = datasSection.consigne3 ?? ''
          sq.consigne4 = datasSection.consigne4 ?? ''
          sq.titre = datasSection.titre ?? ''
          sq.width = Number(datasSection.width ?? 900)
          sq.height = Number(datasSection.height ?? 900)
          sq.nbMatrices = datasSection.nbMatrices ?? 1
          sq.nbCalc = datasSection.nbCalc ?? 0
          for (let i = 1; i <= sq.nbMatrices; i++) {
            sq['decalageEditeurMatrice' + i] = datasSection['decalageEditeurMatrice' + i] ?? 0
          }
          sq.formulaire = datasSection.formulaire // Contient la chaine avec les éditeurs.
          sq.param = datasSection.param ?? ''// Chaine contenant les paramètres autorisés
          sq.charset = datasSection.charset ?? ''
          initMtg()
        }).catch(error => {
          console.error(error)
          j3pShowError('Impossible de charger les données de cette exercice')
        })
        // return;
      } else {
        j3pEmpty('correction')
        sq.numEssai = 1
        videEditeurs()
        cacheSolution()
        montreEditeurs(true)
        sq.mtgAppLecteur.removeDoc('mtg32svg')
        sq.mtgAppLecteur.addDoc('mtg32svg', sq.txt, true)

        if (sq.aleat) {
          for (let i = 1; i <= sq.nbParamAleat; i++) {
            sq.mtgAppLecteur.giveFormula2('mtg32svg', 'r' + i, sq['tab' + i][this.questionCourante - 1])
          }
        }
        if (sq.param !== undefined) {
          const ch = 'abcdefghjklmnopqrsxyz'
          for (let i = 0; i < ch.length; i++) {
            const car = ch.charAt(i)
            if (sq.param.indexOf(car) !== -1) {
              const par = parcours.donneesSection[car]
              if (par !== 'random') sq.mtgAppLecteur.giveFormula2('mtg32svg', car, par)
            }
          }
        }

        sq.mtgAppLecteur.calculate('mtg32svg', true) // true pour que les calculs alèatoires soient réinitialisés
        for (let k = 1; k <= sq.nbMatrices; k++) {
          sq['nbcol' + k] = sq.mtgAppLecteur.valueOf('mtg32svg', 'nbcol' + k)
          sq['nblig' + k] = sq.mtgAppLecteur.valueOf('mtg32svg', 'nblig' + k)
        }
        j3pEmpty('enonce')
        j3pEmpty('formules')
        j3pEmpty('info')
        sq.nbexp = 0
        const nbParam = parseInt(sq.nbLatex)
        const t = 'abcdefghijklmnopq'
        const param = {}
        for (let i = 0; i < nbParam; i++) {
          const code = sq.mtgAppLecteur.getLatexCode('mtg32svg', i)
          param[t.charAt(i)] = code
        }

        const st = replaceOldMQCodes(sq.consigne1 + (sq.simplifier ? sq.consigne3 : sq.consigne2) + sq.consigne4)
        afficheMathliveDans('enonce', 'texte', st, param)
        afficheNombreEssaisRestants()
        j3pElement('info').style.display = 'block'

        j3pEmpty('editeur')
        const formulaire = traiteFormulaire(sq.formulaire)
        const obj = {}
        sq.paramFormulaire = {}
        for (let k = 1; k <= nbCellules(k); k++) obj['inputmq' + k] = {}
        for (let i = 0; i < nbParam; i++) {
          const code = sq.mtgAppLecteur.getLatexCode('mtg32svg', i)
          obj[t[i]] = code
          sq.paramFormulaire[t[i]] = code
        }
        obj.styletexte = {}
        obj.charset = sq.charset
        obj.listeBoutons = sq.listeBoutons
        obj.transposeMat = true
        afficheMathliveDans('editeur', 'expression', formulaire, obj) // sq.formulaire contient ou plusieurs éditeurs MathQuill
        prepareEditeursMatrices()
        prepareEditeursMathQuill()
        sq.mtgAppLecteur.display('mtg32svg') // Pour tout réafficher
        // Pour revenir en haut de page
        try {
          window.location.hash = 'MepMG'
          j3pElement('MepMG').scrollIntoView()
        } catch (e) {
          console.error(e)
        }
        setFocusToFirstInput('expression')
        this.finEnonce()
      }

      break // case "enonce":

    case 'correction': {
      let bilanReponse = ''
      let simplificationPossible = false
      if (validationEditeurs()) {
        validation()
        if (sq.simplifier) {
          if (sq.resolu) {
            bilanReponse = 'exact'
          } else {
            if (sq.exact) {
              bilanReponse = 'exactPasFini'
              simplificationPossible = true
            } else {
              bilanReponse = 'faux'
            }
          }
        } else {
          if (sq.resolu || sq.exact) {
            bilanReponse = 'exact'
            if (!sq.resolu) simplificationPossible = true
          } else {
            bilanReponse = 'faux'
          }
        }
      } else {
        bilanReponse = 'incorrect'
      }
      // A cause de la limite de temps :
      if (this.isElapsed) { // limite de temps
        this._stopTimer()
        montreEditeurs(false)
        j3pElement('correction').style.color = this.styles.cfaux
        j3pElement('boutonrecopier').style.display = 'none'
        j3pElement('info').style.display = 'none'
        if (sq.indicationfaute) sq.mtgAppLecteur.executeMacro('mtg32svg', 'masquerFaute')

        sq.mtgAppLecteur.executeMacro('mtg32svg', 'solution')
        afficheSolution(false)
        j3pElement('correction').innerHTML = tempsDepasse
        j3pElement('correction').innerHTML += '<br>La solution se trouve ci-contre.'
        this.etat = 'navigation'
        this.sectionCourante()
      } else {
        if (bilanReponse === 'incorrect') {
          j3pElement('correction').style.color = this.styles.cfaux
          j3pElement('correction').innerHTML = 'Réponse incorrecte'
        } else {
          // Une réponse a été saisie
          // Bonne réponse
          if (bilanReponse === 'exact') {
            this._stopTimer()
            this.score++
            j3pElement('correction').style.color = this.styles.cbien
            if (simplificationPossible) {
              j3pElement('correction').innerHTML = 'C’est bien mais on pouvait  simplifier la réponse'
            } else {
              j3pElement('correction').innerHTML = cBien
            }
            j3pElement('boutonrecopier').style.display = 'none'
            j3pElement('info').style.display = 'none'
            if (sq.indicationfaute) sq.mtgAppLecteur.executeMacro('mtg32svg', 'masquerFaute')
            sq.mtgAppLecteur.executeMacro('mtg32svg', 'solution')
            afficheSolution(true)
            montreEditeurs(false)
            sq.numEssai++ // Pour un affichage correct dans afficheReponse
            afficheReponse('exact', false)
            this.etat = 'navigation'
            this.sectionCourante()
          } else {
            sq.numEssai++
            if (bilanReponse === 'exactPasFini') {
              j3pElement('correction').style.color =
                (sq.numEssai <= sq.nbEssais) ? '#0000FF' : this.styles.cfaux
              j3pElement('correction').innerHTML = 'Le calcul est bon mais pas écrit sous la forme demandée.'
            } else {
              j3pElement('correction').style.color = this.styles.cfaux
              j3pElement('correction').innerHTML = cFaux
            }
            j3pEmpty('info')
            if (sq.numEssai <= sq.nbEssais) {
              afficheNombreEssaisRestants()
              videEditeurs()
              afficheReponse(bilanReponse, false)
              // S’il y a plus de deux chances,on appelle à nouveau le bouton Valider
              j3pElement('correction').innerHTML += '<br>' + '<br>' + essaieEncore
            } else {
              // Erreur au nème essai
              this._stopTimer()
              montreEditeurs(false)
              j3pElement('boutonrecopier').style.display = 'none'
              j3pElement('info').style.display = 'none'
              if (sq.indicationfaute) sq.mtgAppLecteur.executeMacro('mtg32svg', 'masquerFaute')
              sq.mtgAppLecteur.executeMacro('mtg32svg', 'solution')
              afficheSolution(false)
              afficheReponse(bilanReponse, true)
              j3pElement('correction').innerHTML += '<br><br>La solution se trouve ci-contre.'
              this.etat = 'navigation'
              this.sectionCourante()
            }
          }
        }
      }
      // Obligatoire
      this.finCorrection()
      break // case "correction":
    }

    case 'navigation':
      if (!this.sectionTerminee()) {
        this.etat = 'enonce'
      }
      this.finNavigation()
      break // case "navigation":
  }
}
