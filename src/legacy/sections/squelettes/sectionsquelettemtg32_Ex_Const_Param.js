import $ from 'jquery'
import { j3pAddElt, j3pDiv, j3pElement, j3pEmpty, j3pImporteAnnexe, j3pShowError } from 'src/legacy/core/functions'
import { j3pAffiche } from 'src/lib/mathquill/functions'
import { getMtgApp } from 'src/lib/outils/mathgraph'
import { j3pCreeSVG } from 'src/legacy/core/functionsSvg'
import textesGeneriques from 'src/lib/core/textes'

const { cBien, cFaux, essaieEncore, regardeCorrection, tempsDepasse } = textesGeneriques

/*
    Yves Biton
    Juillet 2017

    squelettemtg32_Ex_Const_Param
    Sert à faire faire un exercice de construction à l’aide de MathGraph32
    La figure utilisée doit comprendre une macro d’apparition d’objets d’intitulé #Solution# dont les objets qu’elle
    fait apparaître sont les objets à construire par l’élève.
    Le commentaire de la macro peut préciser quels sont les objets de type numériques que l’élève a le droit
    d’utiliser. Par exemple {a} et {f} pour utiliser un calcul nommé a et une fonction nommée f.
    Les outils permis pour la construction sont contenus dans la figure elle-même via le menu Options-
    Figure en cours - Personnalisation Menu

    Pour obtenir par exemples 4 répétitions aléatoires, utiliser la commande :
    ?graphe=[1,"squelettemtg32_Ex_Cons",[{pe:">=0",nn:"fin",conclusion:"fin"},{ex:"RetrouverCentreCercle", nbrepetitions:4}]];
    Si ex contient un paramètre nbParamAleat , alors dans le cas ou nbrepetitions > 1, on donne pour i compris entre 1 et npParamAleat
    au calcul de la figure nommé "r"+i (par exemple r1 et r2 si nbParamAleat vaut 2) des valeurs comprises entre 0 et "nbcas"+i - 1
    toutes différentes (sauf si nbrepetitions > b=nbcas auquel cas au bout de nbcas répétitions il y aura nécessairement une valeur de r déjà prise.

*/

export const params = {
  outils: ['mathquill'],
  /**
   * Ce qui est paramétrable dans l’éditeur de graphe
   * @type {LegacySectionParamsParametres}
   */
  parametres: [
    ['nbrepetitions', 1, 'entier', 'aucun'],
    ['limite', 0, 'entier', 'Temps disponible par répétition, en secondes'],
    ['nbchances', 2, 'entier', 'Nombre d’essais autorisés à l’élève'],
    ['correction', true, 'boolean', 'true si on veut que la solution soit donnée à la fin en cas d’échec'],
    ['modeDys', false, 'boolean', 'true si on veut que MathGraph32 fonctionne en mode "dys"'],
    ['infoParam', 'Information sur le rôle des paramètres', 'string', 'Information sur l’utilisation des paramètres suivants'],
    ['a', 'random', 'string', 'Valeur éventuelle à affecter à un calcul a'],
    ['b', 'random', 'string', 'Valeur éventuelle à affecter à un calcul b'],
    ['c', 'random', 'string', 'Valeur éventuelle à affecter à un calcul  c'],
    ['d', 'random', 'string', 'Valeur éventuelle à affecter à un calcul  d'],
    ['e', 'random', 'string', 'Valeur éventuelle à affecter à un calcul e'],
    ['f', 'random', 'string', 'Valeur éventuelle à affecter à un calcul f'],
    ['g', 'random', 'string', 'Valeur éventuelle à affecter à un calcul g'],
    ['h', 'random', 'string', 'Valeur éventuelle à affecter à un calcul h'],
    ['j', 'random', 'string', 'Valeur éventuelle à affecter à un calcul j'],
    ['k', 'random', 'string', 'Valeur éventuelle à affecter à un calcul k'],
    ['n', 'random', 'string', 'Valeur éventuelle à affecter à un calcul n'],
    ['ex', 'Const_Trigo_Point_Par_Mesure_Angle', 'string', 'Nom du fichier annexe (xxx => squelettes-mtg32/xxx.js et xxx/yyy => squelettes-mtg32-xxx/yyy.js)']
  ]
}

/**
 * section squelettemtg32_Ex_Const_Param
 * @this {Parcours}
 */
export default function main () {
  const parcours = this
  const sq = this.storage

  function cacheSolution () {
    $('#solution').css('display', 'none')
  }

  function afficheSolutions (color) {
    let car, par, t, param, code
    $('#solution').css('display', 'block')
    const player = sq.player
    for (let i = 1; i <= sq.nbsol; i++) {
      j3pEmpty('explication' + i)
      j3pElement('explication' + i).style.color = color
      j3pAffiche('explication' + i, 'titreexp' + i, 'Solution ' + ((sq.nbsol > 1) ? ('n°' + i) : '') + ':\n', { style: { color: 'brown' } })
      player.removeDoc('mtg32svgsol' + i) // Ne fait rien lors du premier affichage de correction
      player.addDoc('mtg32svgsol' + i, sq['figsol' + i], true)
      // On remplace dans la figure de solution les paramètres par les valeurs correspondant dans la figure de texte
      const ch = 'abcdefghijkmnopqr'
      for (let j = 0; j < ch.length; j++) {
        car = ch.charAt(j)
        if (sq.param.indexOf(car) !== -1) {
          par = sq.mtgApp.valueOf(car)
          player.giveFormula2('mtg32svgsol' + i, car, par)
        }
      }
      player.calculate('mtg32svgsol' + i, false) // false suite à rapport LaboMep
      t = 'abcdefghijk'
      param = {}
      for (let k = 0; k < sq.nbLatex + i; k++) {
        code = player.getLatexCode('mtg32svgsol' + i, k)
        param[t.charAt(k)] = code
      }
      j3pAffiche('explication' + i, 'solution' + i, sq['explication' + i] + '\n', param)
      player.display('mtg32svgsol' + i)
    }
  }

  function initDom () {
    if (sq.titre) parcours.afficheTitre(sq.titre)

    j3pDiv(parcours.zones.MG, {
      id: 'conteneur',
      contenu: '',
      style: parcours.styles.etendre('toutpetit.enonce', { padding: '6px' })
    })
    j3pDiv('conteneur', 'enonce', '')
    j3pDiv('conteneur', 'infoNbEssais', '')
    j3pDiv('conteneur', 'divmtg32', '')
    j3pDiv('conteneur', 'solution', '')
    for (let i = 1; i <= sq.nbsol; i++) {
      j3pDiv('solution', 'explication' + i, '')
      j3pDiv('solution', 'divmtg32sol' + i, '')
      j3pCreeSVG('divmtg32sol' + i, { id: 'mtg32svgsol' + i, width: sq['width' + i], height: sq['height' + i] })
    }
    const style = parcours.styles.petit.correction
    style.marginTop = '1.5em'
    style.marginLeft = '0.5em'
    j3pAddElt(parcours.zonesElts.MD, 'div', '', { id: 'correction', style })
    cacheSolution()
    const svgOptions = { idSvg: 'mtg32svg', width: sq.width, height: sq.height }
    const mtgOptions = {
      fig: sq.fig,
      dys: sq.modeDys,
      local: false,
      translatable: false,
      displayOnLoad: false,
      bplayer: true,
      save: false,
      stylePointCroix: sq.stylePointCroix,
      isEditable: true
    }
    getMtgApp('divmtg32', svgOptions, mtgOptions).then(mtgApp => {
      sq.player = mtgApp.player
      sq.mtgApp = mtgApp
      // mtg32App.removeAllDoc();
      // Ci-dessous, employer local:true pour déboguer en local et false pour version en ligne
      sq.correction = parcours.donneesSection.correction
      mtgApp.calculate(true)
      const nbvar = mtgApp.valueOf('nbvar')
      if (nbvar !== -1) {
        const nbrep = parcours.donneesSection.nbrepetitions
        sq.aleat = true
        sq.nbParamAleat = nbvar
        for (let i = 1; i <= nbvar; i++) {
          const nbcas = mtgApp.valueOf('nbcas' + i)
          const nb = Math.max(nbrep, nbcas)
          const ar = []
          for (let j = 0; j < nb; j++) ar.push(j % nbcas)
          sq['tab' + i] = []
          for (let k = 0; k < nbrep; k++) {
            const tir = Math.floor(Math.random() * ar.length)
            sq['tab' + i].push(ar[tir])
            ar.splice(tir, 1)
          }
          mtgApp.giveFormula2('r' + i, sq['tab' + i][0])
        }
      } else {
        sq.aleat = false
      }

      if (sq.param !== undefined) {
        const ch = 'abcdefghijkmnopqr'
        for (let i = 0; i < ch.length; i++) {
          const car = ch.charAt(i)
          if (sq.param.indexOf(car) !== -1) {
            const par = parcours.donneesSection[car]
            if (par !== 'random') mtgApp.giveFormula2(car, par)
          }
        }
      }

      mtgApp.calculate(false) // false au lieu de true corrigé initDom à rapport erreur
      // Important la ligne suivante est indispensable car il faut que lorsqu’on annule les actions faites
      // on retrouve la figure telle qu’elle a été modifiée par les lignes ci-dessus
      mtgApp.gestionnaire.initialise()
      // La figure mtg32 peut avoir jusque 11 affichages Latex qu’on peut récupérer commes paramètres dans l’énoncé
      const t = 'abcdefghijk'
      const param = {}
      for (let i = 0; i < sq.nbLatex; i++) {
        param[t.charAt(i)] = mtgApp.getLatexCode(i)
      }
      j3pAffiche('enonce', 'texte', sq.enonce + '\n', param)
      j3pAffiche('infoNbEssais', 'infoessais', 'Il reste ' + sq.nbchances + ' validation(s) possibles.', { style: { color: 'black' } })
      mtgApp.display()
      parcours.finEnonce()
    }).catch(j3pShowError)
  }

  switch (this.etat) {
    case 'enonce':
      // code exécuté au lancement de la section
      if (this.debutDeLaSection) {
        // Construction de la page
        this.construitStructurePage('presentation1bis')
        this.validOnEnter = false // ex donneesSection.touche_entree

        // compteur. Si   numEssai > nbchances, on corrige
        sq.numEssai = 1
        sq.nbchances = parseInt(parcours.donneesSection.nbchances)
        sq.modeDys = parcours.donneesSection.modeDys
        // faut gérer sous-dossier ou pas…
        const sep = this.donneesSection.ex.includes('/') ? '-' : '/'
        j3pImporteAnnexe('squelettes-mtg32' + sep + this.donneesSection.ex + '.js').then(function (datasSection) {
          // Attention, le this devient window...
          sq.fig = datasSection.fig
          sq.width = datasSection.width
          sq.height = datasSection.height
          sq.titre = datasSection.titre
          let s = datasSection.stylePointCroix
          sq.stylePointCroix = (s !== undefined) ? s : true
          s = datasSection.exigerObjetTypeAttendu // Si true, on ne valide pas la réponse tant que les objets de type attendus ont été créés
          sq.exigerObjetTypeAttendu = (s !== undefined) ? s : true
          const param = datasSection.param
          sq.param = param || ''
          sq.nbLatex = datasSection.nbLatex
          sq.nbParamAleat = datasSection.nbParamAleat
          if (sq.nbParamAleat !== undefined) {
            for (let i = 1; i <= sq.nbParamAleat; i++) { sq['nbcas' + i] = datasSection['nbcas' + i] }
          }
          sq.enonce = datasSection.enonce
          sq.nbsol = datasSection.nbsol
          for (let nbsol = 1; nbsol <= datasSection.nbsol; nbsol++) {
            sq['explication' + nbsol] = datasSection['explication' + nbsol]
            sq['nbLatex' + nbsol] = datasSection['nbLatex' + nbsol]
            sq['width' + nbsol] = datasSection['width' + nbsol]
            sq['height' + nbsol] = datasSection['height' + nbsol]
            sq['figsol' + nbsol] = datasSection['solution' + nbsol]
          }
          sq.param = datasSection.param // Chaine contenant les paramètres autorisés
          initDom()
        }).catch(error => {
          console.error(error)
          j3pShowError('Impossible de charger les données de cette exercice')
        })
      } else {
        j3pEmpty('correction')
        this.afficheBoutonValider()
        sq.numEssai = 1
        cacheSolution()
        j3pEmpty('enonce')
        j3pEmpty('infoNbEssais')
        // On recharge la même figure pour tout réinitialiser
        sq.mtgApp.setFigByCode(sq.fig, false) // false pour ne pas réafficher
        sq.mtgApp.calculate(true)
        if (sq.aleat) {
          for (let i = 1; i <= sq.nbParamAleat; i++) {
            sq.mtgApp.giveFormula2('r' + i, sq['tab' + i][this.questionCourante - 1])
          }
        }
        if (sq.param !== undefined) {
          const ch = 'abcdefghijkmnopqr'
          for (let i = 0; i < ch.length; i++) {
            const car = ch.charAt(i)
            if (sq.param.indexOf(car) !== -1) {
              const par = parcours.donneesSection[car]
              if (par !== 'random') sq.mtgApp.giveFormula2(car, par)
            }
          }
        }
        sq.mtgApp.calculate(false)
        // Important la ligne suivante est indispensable car il faut que lorsqu’on annule les actions faites
        // on retrouve la figure telle qu’elle a été modifiée par les lignes ci-dessus
        sq.mtgApp.gestionnaire.initialise()
        // La figure mtg32 peut avoir jusque 11 affichages Latex qu’on peut récupérer commes paramètres dans l’énoncé
        const t = 'abcdefghijk'
        const param = {}
        for (let i = 0; i < sq.nbLatex; i++) {
          param[t.charAt(i)] = sq.mtgApp.getLatexCode(i)
        }
        j3pAffiche('enonce', 'texte', sq.enonce + '\n', param)
        j3pAffiche('infoNbEssais', 'infoessais', 'Il reste ' + sq.nbchances + ' validation(s) possibles.', { style: { color: 'black' } })
        sq.mtgApp.display()
        this.finEnonce()
      }
      // Pour revenir en haut de page
      try {
        window.location.hash = 'MepMG'
        j3pElement('MepMG').scrollIntoView()
      } catch (e) {}

      break // case "enonce":

    case 'correction': {
      if (!this.isElapsed) { // Vérications à faire seulement si le temps limite a pas été dépassé)
        if (!sq.mtgApp.objectConstructed()) {
          j3pElement('correction').style.color = this.styles.cfaux
          j3pElement('correction').innerHTML = 'Aucun objet n’a été construit.'
          return
        }
        if (sq.exigerObjetTypeAttendu) {
          const missingTypes = sq.mtgApp.getMissingTypes()
          if (missingTypes.length !== 0) {
            let ch = 'Il manque un ou des éléments de type : '
            j3pElement('correction').style.color = this.styles.cfaux
            for (let k = 0; k < missingTypes.length; k++) {
              ch = ch + missingTypes[k]
              if (k !== missingTypes.length - 1) ch = ch + ', '; else ch = ch + '.'
            }
            j3pElement('correction').innerHTML = ch
            return
          }
        }
        const missingNames = sq.mtgApp.getMissingNames()
        if (missingNames.length !== 0) {
          let ch = 'Il faut nommer '
          j3pElement('correction').style.color = this.styles.cfaux
          for (let k = 0; k < missingNames.length; k++) {
            ch = ch + missingNames[k]
            if (k !== missingNames.length - 1) ch = ch + ', '; else ch = ch + '.'
          }
          j3pElement('correction').innerHTML = ch
          return
        }
      }
      let bilanreponse = ''
      const res1 = sq.mtgApp.validateAnswer()
      if (res1) {
        bilanreponse = 'exact'
      } else {
        bilanreponse = 'erreur'
      }

      // Bonne réponse
      if (bilanreponse === 'exact') {
        sq.mtgApp.setActive(false)
        this._stopTimer()
        // mtg32App.setActive("mtg32svg", false);
        this.score++
        j3pElement('correction').style.color = this.styles.cbien
        j3pElement('correction').innerHTML = cBien + '<br>Voir la solution au-dessous de la figure.'
        afficheSolutions(this.styles.cbien)
        this.etat = 'navigation'
        this.sectionCourante()
      } else {
        // Pas de bonne réponse
        // A cause de la limite de temps :
        if (this.isElapsed) { // limite de temps
          sq.mtgApp.setActive(false)
          this._stopTimer()
          if (sq.correction) afficheSolutions(parcours.styles.toutpetit.correction.color)
          j3pElement('correction').innerHTML = tempsDepasse
          j3pElement('correction').innerHTML += '<br>' + regardeCorrection
          this.etat = 'navigation'
          this.sectionCourante()
        } else {
          // Réponse fausse :
          j3pElement('correction').style.color = this.styles.cfaux
          j3pElement('correction').innerHTML = cFaux
          sq.numEssai++

          // S’il y a plus de deux chances,on appelle à nouveau le bouton Valider
          if (sq.numEssai <= sq.nbchances) {
            j3pElement('correction').innerHTML += '<br>' + essaieEncore
            j3pElement('infoessais').innerHTML = 'Il reste ' + String(sq.numEssai - 1) + ' validation(s) possible(s).'
          } else {
            sq.mtgApp.setActive(false)
            // Erreur au nème essai
            this._stopTimer()
            j3pElement('infoNbEssais').removeChild(j3pElement('infoessais'))
            if (sq.correction) {
              afficheSolutions(parcours.styles.toutpetit.correction.color)
            }
            if (sq.correction) j3pElement('correction').innerHTML += '<br>Regarder la correction sous la figure.'
            this.etat = 'navigation'
            this.sectionCourante()
          }
        }
      }
      // Obligatoire
      this.finCorrection()
      break // case "correction":
    }

    case 'navigation':

      if (this.sectionTerminee()) {
        // On détermine la phrase d’état renvoyée par la section
        this.parcours.pe = this.score / this.donneesSection.nbitems
        this.afficheBoutonSectionSuivante()
        this.focus('sectioncontinuer')
      } else {
        this.etat = 'enonce'
        this.afficheBoutonSuite()
        this.focus('boutoncontinuer')
      }
      // Obligatoire
      this.finNavigation()

      break // case "navigation":
  }
}
