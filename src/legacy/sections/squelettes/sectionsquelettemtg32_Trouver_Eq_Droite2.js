import { j3pDiv, j3pElement, j3pEmpty, j3pImporteAnnexe, j3pShowError } from 'src/legacy/core/functions'
import { j3pMathsAjouteDans } from 'src/lib/mathquill/functions'
import { j3pCreeSVG } from 'src/legacy/core/functionsSvg'
import { getMtgCore } from 'src/lib/outils/mathgraph'
import textesGeneriques from 'src/lib/core/textes'

const { cBien, cFaux, essaieEncore, tempsDepasse } = textesGeneriques

/*
    Yves Biton
    Octobre 2013

    http://localhost:8081/?graphe=[1,"squelettemtg32_Trouver_Eq_Droite",[{pe:">=0",nn:"fin",conclusion:"fin"},{f:"Trouver_Eq_Droite_Ex1"}]];
    squelettemtg32_Trouver_Eq_Droite
    Trace une droite (BC) avec B(0; yb) et C(xc; yx) choisis aléatoirement entiers
    Les valeurs de xb, xc et de yc peuvent être passées comme paramètres.
*/

export const params = {
  outils: ['mathquill'],
  /**
   * Ce qui est paramétrable dans l’éditeur de graphe
   * @type {LegacySectionParamsParametres}
   */
  parametres: [
    ['nbrepetitions', 1, 'entier', 'aucun'],
    ['limite', 0, 'entier', 'Temps disponible par répétition, en secondes'],
    ['nbchances', 1, 'entier', 'Nombre d’erreurs possibles (pour tenter de les corriger)'],
    // ["f","Trouver_Eq_Droite_Ex2.txt","string", 'Nom du fichier annexe (xxx => squelettes-mtg32/xxx.js et xxx/yyy => squelettes-mtg32-xxx/yyy.js)'],
    ['yb', 'random', 'string', "random pour exercice aléatoire ou ordonnée entière de B.\nDemande l’équation de (BC) avec B(0;yB) et C(xC;yC).\nPar exemple, yb:'int(rand(0)*9)-4' pour une valeur entre -4 et 4"],
    ['xc', 'random', 'string', "random pour exercice aléatoire ou abscisse de C.\nDemande l’équation de (BC) avec B(0;yB) et C(xC;yC).\nPar exemple, xc:'int(rand(0)*9)-4' pour une valeur entre -4 et 4"],
    ['yc', 'random', 'string', "random pour exercice aléatoire ou ordonnée de C.\nDemande l’équation de (BC) avec B(0;yB) et C(xC;yC).\nPar exemple, yc:'int(rand(0)*9)-4' pour une valeur entre -4 et 4"],
    ['titre', 'Titre de l’activité', 'string', 'Titre de l’activité']
  ]
}

/**
 * section squelettemtg32_Trouver_Eq_Droite2
 * @this {Parcours}
 */
export default function main () {
  const parcours = this
  const sq = this.storage
  let code
  function initMtg () {
    // chargement mtg
    getMtgCore({ withMathJax: true })
      .then(
        // success
        (mtgAppLecteur) => {
          sq.mtgAppLecteur = mtgAppLecteur
          sq.mtgAppLecteur.removeAllDoc()
          sq.mtgAppLecteur.addDoc('mtg32svg', sq.txt, true)
          const par = parcours.donneesSection
          if (par.yb !== 'random') sq.mtgAppLecteur.giveFormula2('mtg32svg', 'yb', par.yb)
          if (par.xc !== 'random') sq.mtgAppLecteur.giveFormula2('mtg32svg', 'xc', par.xc)
          if (par.yc !== 'random') sq.mtgAppLecteur.giveFormula2('mtg32svg', 'n', par.yc + '-' + par.yb)
          sq.mtgAppLecteur.calculateAndDisplayAll(true)
          // j3pMathsAjouteDans("enonce",{id:"texte", content: sq.numero});
          const nbParam = parseInt(sq.numero)
          const t = ['a', 'b', 'c', 'd']
          const param = {}
          let code
          for (let i = 0; i < nbParam; i++) {
            code = sq.mtgAppLecteur.getLatexCode('mtg32svg', i)
            param[t[i]] = code
          }
          sq.titre = nbParam
          j3pMathsAjouteDans('enonce', { id: 'texte', content: sq.consigne1, parametres: param })
          parcours.finEnonce()
        },
        // failure
        (error) => {
          j3pShowError(error, { message: 'mathgraph n’est pas correctement chargé', mustNotify: true })
        })
      // plantage dans le code de success
      .catch(j3pShowError)
  }

  function initDom () {
    if (sq.titre) parcours.afficheTitre(sq.titre)

    j3pDiv(parcours.zones.MG, {
      id: 'conteneur',
      contenu: '',
      style: parcours.styles.etendre('toutpetit.enonce', { padding: '6px' })
    })
    j3pDiv('conteneur', 'enonce', '')
    let div = j3pDiv('conteneur', 'solution', '')
    div.align = 'center'
    div.style.color = 'blue'
    div.style.visibility = 'hidden'
    div = j3pDiv('conteneur', 'explicationsifaux', '')
    div.align = 'center'
    div.style.color = 'red'
    div.style.visibility = 'hidden'
    /*
    j3pDiv("conteneur","divmtg32","",parcours.styles.etendre("toutpetit.enonce",
      {width:"600px",height:"700px"}));
    */
    j3pDiv('conteneur', 'divmtg32', '')
    j3pCreeSVG('divmtg32', { id: 'mtg32svg', width: 600, height: 700 })

    initMtg()
    // OBLIGATOIRE
    parcours.cacheBoutonSuite()

    j3pDiv(parcours.zones.MD, { id: 'correction', contenu: '', coord: [20, 150], style: parcours.styles.petit.correction })
  }

  function _Donnees () {
    // "primaire" || "lycee" || "college"
    // Actuellement les dimensions sont identiques
    this.typesection = 'lycee'//
    // Nombre de répétitions de l’exercice
    this.nbrepetitions = 1
    this.nbetapes = 1
    this.nbitems = this.nbetapes * this.nbrepetitions

    this.f = 'Trouver_Eq_Droite_Ex2'
    this.yb = 'random'
    this.xc = 'random'
    this.yc = 'random'
    // Si indication != "" alors un lien "Indication" s’affiche dans la zone Inférieure Gauche (this.zones.IG
    // une indication<br>de deux lignes...";

    // pour le cas de presentation1bis
    // En effet pas d’indication possible pour cette présentation
    // this.surchargeindication = false;

    // Nombre de chances dont dispose l’él§ve pour répondre à la question
    this.nbchances = 1

    this.structure = 'presentation1bis'//  || "presentation2" || "presentation3"  || "presentation1bis"

    this.textes = {
    }
    this.titre = 'Titre de l’activité'

    this.pe = 0
  }

  switch (this.etat) {
    case 'enonce':
      // code exécuté au lancement de la section
      if (this.debutDeLaSection) {
        this.donneesSection = new _Donnees()
        // Construction de la page
        parcours.construitStructurePage(parcours.donneesSection.structure)

        this.surcharge()
        this.validOnEnter = false // ex donneesSection.touche_entree
        // faut gérer sous-dossier ou pas…
        const sep = this.donneesSection.f.includes('/') ? '-' : '/'
        j3pImporteAnnexe('squelettes-mtg32' + sep + this.donneesSection.f + '.js').then(function (datasSection) {
          // console.log("initsection");
          // Attention, le this devient window...
          // console.log(arguments);
          sq.txt = datasSection.txt
          sq.numero = datasSection.numero // Le nombre de paramètres LaTeX dans le texte
          sq.consigne1 = datasSection.consigne1
          sq.consigne2 = datasSection.consigne2
          sq.correction = datasSection.correction
          sq.titre = datasSection.titre
          initDom()
        }).catch(error => {
          console.error(error)
          j3pShowError('Impossible de charger les données de cette exercice')
        })
      } else {
        if ((this.donneesSection.structure === 'presentation1') || (this.donneesSection.structure === 'presentation1bis')) {
          j3pEmpty(this.zonesElts.MD)
        }
        j3pDiv(this.zones.MD, { id: 'correction', contenu: '', coord: [20, 150], style: this.styles.petit.correction })
        sq.mtgAppLecteur.setEditorsEmpty('mtg32svg')
        sq.mtgAppLecteur.executeMacro('mtg32svg', 'initialiser')
        sq.mtgAppLecteur.calculate('mtg32svg', true) // true pour que les calculs alꢴoires soient rꪮitialis곍
        sq.mtgAppLecteur.display('mtg32svg') // Pour tout réafficher
        sq.mtgAppLecteur.setActive('mtg32svg', true)
        j3pElement('enonce').innerHTML = ''
        const t = ['a', 'b', 'c', 'd']
        const param = {}
        for (let i = 0; i < sq.titre; i++) {
          code = sq.mtgAppLecteur.getLatexCode('mtg32svg', i)
          param[t[i]] = code
        }
        j3pMathsAjouteDans('enonce', { id: 'texte', content: sq.consigne1, parametres: param })
        j3pElement('solution').innerHTML = ''
        j3pElement('explicationsifaux').innerHTML = ''
        this.finEnonce()
      }

      break // case "enonce":

    case 'correction': {
      let bilanreponse = ''
      const verifa = sq.mtgAppLecteur.fieldValidation('mtg32svg', 'z1')
      const verifb = sq.mtgAppLecteur.fieldValidation('mtg32svg', 'z2')
      if (verifa && verifb) {
        const res1 = sq.mtgAppLecteur.valueOf('mtg32svg', 'reponse')
        if (res1 === 1) {
          bilanreponse = 'exact'
        } else {
          bilanreponse = 'erreur'
          sq.mtgAppLecteur.activateFirstInvalidField('mtg32svg')
        }
      } else {
        bilanreponse = 'incorrect'
        sq.mtgAppLecteur.activateFirstInvalidField('mtg32svg')
      }
      // A cause de la limite de temps :
      if (this.isElapsed) { // limite de temps
        this._stopTimer()
        j3pElement('correction').style.color = this.styles.cfaux
        code = sq.mtgAppLecteur.getLatexCode('mtg32svg', sq.titre) // L’équation fausse proposée
        j3pMathsAjouteDans('solution', {
          id: 'solution',
          content: sq.correction,
          parametres: { a: code }
        })
        j3pElement('solution').style.visibility = 'visible'
        sq.mtgAppLecteur.executeMacro('mtg32svg', 'voirSolution2')
        j3pElement('correction').innerHTML = tempsDepasse
        this.cacheBoutonValider()
        this.etat = 'navigation'
        this.sectionCourante()
      } else {
        if (bilanreponse === 'incorrect') {
          j3pElement('correction').style.color = this.styles.cfaux
          j3pElement('correction').innerHTML = 'Réponse illisible'
          this.afficheBoutonValider()
        } else {
          // Une réponse a été saisie
          if (bilanreponse === 'exact') {
            this._stopTimer()
            sq.mtgAppLecteur.setActive('mtg32svg', false)
            code = sq.mtgAppLecteur.getLatexCode('mtg32svg', sq.titre) // L’équation fausse proposée
            sq.mtgAppLecteur.executeMacro('mtg32svg', 'voirSolution')
            code = sq.mtgAppLecteur.getLatexCode('mtg32svg', sq.titre) // L’équation fausse proposée
            j3pMathsAjouteDans('solution', {
              id: 'solution',
              content: sq.correction,
              parametres: { a: code }
            })
            j3pElement('solution').style.visibility = 'visible'
            sq.mtgAppLecteur.executeMacro('mtg32svg', 'voirSolution')
            this.score++
            j3pElement('correction').style.color = this.styles.cbien
            j3pElement('correction').innerHTML = cBien
            this.cacheBoutonValider()
            this.etat = 'navigation'
            this.sectionCourante()
          } else {
            // Réponse fausse :
            j3pElement('correction').innerHTML = cFaux

            // S’il y a plus de deux chances,on appelle à nouveau le bouton Valider
            if (this.essaiCourant < this.donneesSection.nbchances) {
              j3pElement('correction').innerHTML += '<br>' + essaieEncore
            } else {
              // Erreur au nème essai
              this._stopTimer()
              this.cacheBoutonValider()
              sq.mtgAppLecteur.setActive('mtg32svg', false)
              j3pMathsAjouteDans('explicationsifaux', {
                id: 'expli',
                content: sq.consigne2,
                parametres: {}
              })
              j3pElement('explicationsifaux').style.visibility = 'visible'
              code = sq.mtgAppLecteur.getLatexCode('mtg32svg', sq.titre) // L’équation fausse proposée
              j3pMathsAjouteDans('solution', {
                id: 'solution',
                content: sq.correction,
                parametres: { a: code }
              })
              j3pElement('solution').style.visibility = 'visible'
              sq.mtgAppLecteur.executeMacro('mtg32svg', 'voirSolution')
              j3pElement('correction').style.color = this.styles.cfaux
              // j3pElement("correction").innerHTML+='<br>' + regardeCorrection;
              j3pElement('correction').innerHTML = '<br>C’est faux.<br><br>La solution se trouve en bas de la fenêtre ci-contre.'
              this.etat = 'navigation'
              this.sectionCourante()
            }
          }
        }
      }
      // Obligatoire
      this.finCorrection()
      break // case "correction":
    }

    case 'navigation':

      if (this.sectionTerminee()) {
        // On détermine la phrase d’état renvoyée par la section
        this.parcours.pe = this.score
        this.afficheBoutonSectionSuivante()
        this.focus('sectioncontinuer')
      } else {
        this.etat = 'enonce'
        this.afficheBoutonSuite()
        this.focus('boutoncontinuer')
      }
      // Obligatoire
      this.finNavigation()

      break // case "navigation":
  }
}
