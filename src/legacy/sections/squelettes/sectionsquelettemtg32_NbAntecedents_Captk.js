import { j3pAddElt, j3pDiv, j3pElement, j3pEmpty, j3pImporteAnnexe, j3pShowError } from 'src/legacy/core/functions'
import { j3pAffiche } from 'src/lib/mathquill/functions'
import { getMtgApp } from 'src/lib/outils/mathgraph'
import { j3pCreeSVG } from 'src/legacy/core/functionsSvg'
import textesGeneriques from 'src/lib/core/textes'

const { cBien, cFaux, essaieEncore, regardeCorrection, tempsDepasse } = textesGeneriques

/*
    Yves Biton
    Octobre 2013

    http://localhost:8081/?graphe=[1,"squelettemtg32_NbAntecedents_Captk.initMtg",[{pe:">=0",nn:"fin",conclusion:"fin"},{f:"NbAntecedents_Captk1.txt"}]];

    Ce squelette contient une figure MathGraph32 dans laquelle on demande à l’utilisateur d’agir sur la figure contenant
    une courbe de fonction et une droite d’équation f(x)=k.
    L’utilisateur doit capturer k pour que l’équation ait le nombre de solutions demandé.
    Le nombre de solutions que doit avoir l’équation pour que la réponse soit juste est dans le calcul n.
    Le calcul reponse contient 1 si a réponse est exacte et 0 si elle est fausse.
    Le nombre de solutions proposé par l’élève est dans le calcul nbsol
    Chaque utilisation est liée à un fichier texte dont la première ligne contient la figure de l’exercice et la deuxième
    une figure contenant le corrigé.
    La troisième ligne contient la consigne dans laquelle est innsérée la valeur de n.
    La quatrième contient un texte d’explications en cas d’erreur de la correction dans lequel est inséré
    le nombre faux de solutions proposé.
    Au début de l’exercice, la corrigé est dans un div caché et affiché seulement lors de la correction.

*/

export const params = {
  outils: ['mathquill'],
  /**
   * Ce qui est paramétrable dans l’éditeur de graphe
   * @type {LegacySectionParamsParametres}
   */
  parametres: [
    ['limite', 0, 'entier', 'Temps disponible par répétition, en secondes'],
    ['nbchances', 2, 'entier', 'Nombre d’erreurs possibles (pour tenter de les corriger)'],

    ['f', 'NbAntecedents_Captk1', 'string', 'Nom du fichier annexe (xxx => squelettes-mtg32/xxx.js et xxx/yyy => squelettes-mtg32-xxx/yyy.js)'],
    ['titre', 'Titre de l’activité', 'string', 'Titre de l’activité']
  ]
}

/**
 * section squelettemtg32_NbAntecedents_Captk
 * @this {Parcours}
 */
export default function main () {
  const parcours = this
  const sq = this.storage
  function initMtg () {
    const mtgOptions = {
      // player only, sans figure initiale ni svg, on gère ça dans initMtg et modif_fig
      loadCoreWithMathJax: true
    }
    getMtgApp('mtg32svg', {}, mtgOptions).then((mtgAppLecteur) => {
      sq.mtgAppLecteur = mtgAppLecteur
      sq.mtgAppLecteur.removeAllDoc()
      sq.mtgAppLecteur.addDoc('mtg32svg', sq.fig, true)
      sq.mtgAppLecteur.addDoc('mtg32svgsol', sq.figsol, false)
      sq.mtgAppLecteur.calculate('mtg32svg', true)
      sq.mtgAppLecteur.display('mtg32svg')
      const code = sq.mtgAppLecteur.valueOf('mtg32svg', 'n')
      j3pAffiche('enonce', 'texte', sq.consigne1, { n: code })
      parcours.finEnonce()
    }).catch(j3pShowError)
  }

  function _Donnees () {
    // "primaire" || "lycee" || "college"
    // Actuellement les dimensions sont identiques
    this.typesection = 'lycee'//
    // Nombre de répétitions de l’exercice
    this.nbrepetitions = 1
    this.nbetapes = 1
    this.nbitems = this.nbetapes * this.nbrepetitions

    this.f = 'NbAntecedents_Captk1'
    // Si indication != "" alors un lien "Indication" s’affiche dans la zone Inférieure Gauche (this.zones.IG
    // une indication<br>de deux lignes...";

    // pour le cas de presentation1bis
    // En effet pas d’indication possible pour cette présentation
    // this.surchargeindication = false;

    // Nombre de chances dont dispose l’élève pour répondre à la question
    this.nbchances = 1

    this.textes = {
    }
    this.titre = 'Titre de l’activité'

    this.pe = 0
  }

  function initDom () {
    if (sq.titre) parcours.afficheTitre(sq.titre)

    j3pDiv(parcours.zones.MG, {
      id: 'conteneur',
      contenu: '',
      style: parcours.styles.etendre('toutpetit.enonce', { padding: '6px' })
    })
    j3pDiv('conteneur', 'enonce', '')
    j3pDiv('conteneur', 'explicationsifaux', '')
    j3pElement('explicationsifaux').style.display = 'hidden'
    j3pDiv('conteneur', 'divmtg32', '')
    j3pCreeSVG('divmtg32', { id: 'mtg32svg', width: 570, height: 425 })
    j3pDiv('conteneur', 'divmtg32sol', '')
    j3pCreeSVG('divmtg32sol', { id: 'mtg32svgsol', width: 570, height: 425 })
    j3pElement('divmtg32sol').style.display = 'none'
    const style = parcours.styles.petit.correction
    style.marginTop = '1.5em'
    style.marginLeft = '0.5em'
    j3pAddElt(parcours.zonesElts.MD, 'div', '', { id: 'correction', style })
    initMtg()
  }
  switch (this.etat) {
    case 'enonce':
      // code exécuté au lancement de la section
      if (this.debutDeLaSection) {
        this.donneesSection = new _Donnees()
        // Construction de la page
        parcours.construitStructurePage('presentation1bis')

        this.surcharge()
        this.validOnEnter = false // ex donneesSection.touche_entree
        // compteur. Si   numEssai > nbchances, on corrige
        sq.numEssai = 1
        sq.nbchances = parcours.donneesSection.nbchances
        // faut gérer sous-dossier ou pas…
        const sep = this.donneesSection.f.includes('/') ? '-' : '/'
        j3pImporteAnnexe('squelettes-mtg32' + sep + this.donneesSection.f + '.js').then(function (datasSection) {
          // console.log("initsection");
          // Attention, le this devient window...
          // console.log(arguments);
          sq.fig = datasSection.txt // La figure de l’énoncé
          sq.figsol = datasSection.figure // La figure de la correction
          sq.consigne1 = datasSection.consigne1// L’énoncé
          sq.consigne2 = datasSection.consigne2// L’explication en cas d’erreur
          sq.titre = datasSection.titre
          initDom()
        }).catch(error => {
          console.error(error)
          j3pShowError('Impossible de charger les données de cette exercice')
        })
      } else {
        j3pEmpty('correction')
        sq.numEssai = 1
        this.finEnonce()
      }

      break // case "enonce":

    case 'correction': {
      let bilanreponse = ''
      const res1 = sq.mtgAppLecteur.valueOf('mtg32svg', 'reponse')
      if (res1 === 1) {
        bilanreponse = 'exact'
      } else {
        bilanreponse = 'erreur'
      }

      sq.mtgAppLecteur.executeMacro('mtg32svg', 'solution')
      j3pElement('divmtg32sol').style.display = 'block'
      sq.mtgAppLecteur.calculate('mtg32svgsol', true)
      sq.mtgAppLecteur.display('mtg32svgsol')
      sq.mtgAppLecteur.setActive('mtg32svg', false)
      if (bilanreponse === 'exact') {
        this._stopTimer()
        this.score++
        j3pElement('correction').style.color = this.styles.cbien
        j3pElement('correction').innerHTML = cBien
        this.etat = 'navigation'
        this.sectionCourante()
      } else {
        // Pas de bonne réponse
        j3pElement('correction').style.color = this.styles.cfaux

        // A cause de la limite de temps :
        if (this.isElapsed) { // limite de temps
          this._stopTimer()
          j3pElement('correction').innerHTML = tempsDepasse
          j3pElement('correction').innerHTML += '<br>' + '<br>' + regardeCorrection
          /*
           *   RECOPIER LA CORRECTION ICI !
           */
          this.etat = 'navigation'
          this.sectionCourante()
        } else {
          // Réponse fausse :
          j3pElement('correction').innerHTML = cFaux
          sq.numEssai++

          // S’il y a plus de deux chances,on appelle à nouveau le bouton Valider
          if (sq.numEssai <= sq.nbchances) {
            j3pElement('correction').innerHTML += '<br>' + essaieEncore
          } else {
            // Erreur au nème essai
            this._stopTimer()
            j3pElement('correction').innerHTML += '<br>' + regardeCorrection
            // j3pElement("correction").innerHTML+="<br>La solution était "+this.stockage[0]
            const code = sq.mtgAppLecteur.valueOf('mtg32svg', 'nbsol')
            j3pAffiche('explicationsifaux', 'expli', sq.consigne2, { m: code })
            j3pElement('explicationsifaux').style.visibility = 'visible'
            this.etat = 'navigation'
            this.sectionCourante()
          }
        }
      }
      // Obligatoire
      this.finCorrection()
      break // case "correction":
    }

    case 'navigation':

      if (this.sectionTerminee()) {
        // On détermine la phrase d’état renvoyée par la section
        this.parcours.pe = this.score
        this.afficheBoutonSectionSuivante()
        this.focus('sectioncontinuer')
      } else {
        this.etat = 'enonce'
        this.afficheBoutonSuite()
        this.focus('boutoncontinuer')
      }
      // Obligatoire
      this.finNavigation()

      break // case "navigation":
  }
}
