import $ from 'jquery'
import { MathfieldElement, renderMathInDocument } from 'mathlive'

import { j3pAddElt, j3pAjouteBouton, j3pDiv, j3pElement, j3pEmpty, j3pImporteAnnexe, j3pPaletteMathquill, j3pShowError } from 'src/legacy/core/functions'
import { j3pCreeSVG } from 'src/legacy/core/functionsSvg'
import { cleanLatexForMq, unLatexify } from 'src/lib/mathquill/functions'
import { getMtgApp } from 'src/lib/outils/mathgraph'
import textesGeneriques from 'src/lib/core/textes'
import { setMathliveCss } from 'src/lib/outils/mathlive/mathlivecss'
import { mathliveRestrict } from 'src/lib/outils/mathlive/utils'
import { afficheMathliveDans, getMathliveValue } from 'src/lib/outils/mathlive/display'
import { focusIfExists } from 'src/lib/utils/dom/main'

const { cBien, cFaux, essaieEncore, tempsDepasse } = textesGeneriques

/*
    Yves Biton
    Janvier 2020. Revu en 2023 pour pasage à Mathlive
    Squelette demandant de calculer une expressionv ectorielle de l’écrire sous la forme la plus simple possible
    On peut éventuellement spécifier des paramètres  a,b,c,d,e,f,g,h,j,k,n,n,p.

    Le fichier annexe doit contenir un membre nomsPoints constenant les noms des points autorisés pour le calcul
    et un membre nomsVect contenant les noms des vecteurs autorisés par le calcul.

    La figure mtg32 doit contenir des calculs complexes ayant le même nom que les points ou vecteurs autorisés
    mais un vecteur nommé i dans l’exercice devra être associé à un calcul complexe nommé i' dans la figure.
    La figure doit contenir un calcul complexe nommé rep et un calcul nommé reponse qui doit valeur 1 si rep contient
    la (ou une des) formule attendue, 2 si le calcul correspond à la réponse mais n’est pas fini et 0 sinon.
    Elle doit aussi contenit un calcul complexe nommé vect0 ayant pour valeur 0 et défini avant le calcul complexe rep.
    Ce calcul représente le vecteur nul.

    Si la figure contient un calcul nommé nbVar , alors dans le cas ou nbrepetitions > 1, on donne pour i compris entre 1 et nbVar
    au calcul de la figure nommé "r"+i (par exemple r1 et r2 si nbParamAleat vaut 2) des valeurs comprises entre 0 et "nbcas"+i - 1
    toutes différentes (sauf si nbrepetitions > nbcas auquel cas au bout de nbcas répétitions il y aura nécessairement une valeur de r déjà prise.

*/
// on définit le dossier des fonts mathlive (il faut le faire très tôt,
// ici c’est bon avec pnpm start mais trop tard pour un build (cf src/lib/player/index.js de la branche draftPlayer)
if (MathfieldElement) { // undefined dans les tests avec happy-dom
  MathfieldElement.fontsDirectory = '/static/mathlive/fonts'
  MathfieldElement.soundsDirectory = '/static/mathlive/sounds'
}

export const params = {
  /**
   * Ce qui est paramétrable dans l’éditeur de graphe
   * @type {LegacySectionParamsParametres}
   */
  parametres: [
    ['nbrepetitions', 1, 'entier', 'aucun'],
    ['limite', 0, 'entier', 'Temps disponible par répétition, en secondes'],
    ['validationAuto', false, 'boolean', `Si false (par défaut) :
L’élève valide ses calculs intermédiaires par la touche Entrée et, quand il estime avoir répondu à la question, clique sur le bouton OK pour valider sa réponse
Le nombre de calculs intermédiaires permis est nbEssais et le nombre de validations est nbchances
Si true :
L’élève valide ses calculs en appuyant sur la touche Entrée ou en cliquant sur le bouton OK.
Dès qu’une des réponses intermédiaires est acceptée comme réponse finale, la réponse est acceptée.
L’élève peut faire nbEssais propositions et le paramètre nbchances n’est ici pas utilisé.`],
    ['nbchances', 1, 'entier', 'Nombre d’erreurs possibles (pour tenter de les corriger)'],
    ['nbEssais', 6, 'entier', 'Nombre d’essais maximum autorisés'],
    ['indicationfaute', true, 'boolean', 'true si on veut que certaines fautes soient expliquées après appui sur Entrée'],
    ['simplifier', true, 'boolean', 'true si on exige un forme donnée, false sinon'],
    ['a', 'random', 'string', 'Valeur de a'],
    ['b', 'random', 'string', 'Valeur de b'],
    ['c', 'random', 'string', 'Valeur de c'],
    ['d', 'random', 'string', 'Valeur de d'],
    ['e', 'random', 'string', 'Valeur de e'],
    ['f', 'random', 'string', 'Valeur de f'],
    ['g', 'random', 'string', 'Valeur de g'],
    ['h', 'random', 'string', 'Valeur de h'],
    ['j', 'random', 'string', 'Valeur de j'],
    ['k', 'random', 'string', 'Valeur de k'],
    ['l', 'random', 'string', 'Valeur de l'],
    ['m', 'random', 'string', 'Valeur de m'],
    ['n', 'random', 'string', 'Valeur de n'],
    ['p', 'random', 'string', 'Valeur de p'],
    ['q', 'random', 'string', 'Valeur de q'],
    ['r', 'random', 'string', 'Valeur de r'],
    ['ex', 'seconde/Calcul_Vecteurs_1', 'string', 'Nom du fichier annexe (xxx => squelettes-mtg32/xxx.js et xxx/yyy => squelettes-mtg32-xxx/yyy.js)']
  ]
}

const inputId = 'expressioninputmq1'

/**
 * section squelettemtg32_Calc_Param_Vect
 * @this {Parcours}
 */
export default function main () {
  const parcours = this
  const sq = this.storage

  function onkeyup (ev, parcours) {
    if (sq.marked) {
      demarqueEditeurPourErreur()
    }
    if (!sq.validationAuto && ev.keyCode === 13) {
      let valide
      let rep = getMathliveValue(inputId)
      if (rep !== '') {
        rep = traiteMathlive(rep)
        const chcalcul = rep.res
        if (rep.vectNul) {
          valide = true
          sq.resultatSyntaxe = {
            syntaxOK: true,
            syntaxVecOK: true,
            isVec: true
          }
        } else {
          const tabNames = []
          for (let k = 0; k < sq.nomsVect.length; k++) {
            let nom = sq.nomsVect[k]
            if (nom === 'i') nom = "i'"
            tabNames.push(nom)
          }
          for (let k = 0; k < sq.nomsPoints.length; k++) {
            let nom = sq.nomsPoints[k]
            if (nom === 'i') nom = "i'"
            tabNames.push(nom)
          }
          sq.resultatSyntaxe = sq.mtgAppLecteur.calcVectOK('mtg32svg', chcalcul, tabNames)
          valide = rep.valid && sq.resultatSyntaxe.syntaxOK
        }
        if (valide) {
          validation(parcours)
        } else {
          marqueEditeurPourErreur()
          j3pElement('correction').style.color = parcours.styles.cfaux
          j3pElement('correction').innerHTML = 'Réponse incorrecte'
          focusIfExists(inputId)
        }
      } else {
        focusIfExists(inputId)
      }
    }
  }
  function contientSomOuDifPoints (ch) {
    if (sq.nomsPoints.length === 0) return false
    // avec les points "A", "B" et "point1" ça donne la regex /(A|B|point1)[+-](A|B|point1)/
    // soit le nom d’un des points, suivi du signe + ou -, suivi du nom d’un des points
    const listeChoixPoints = '(' + sq.nomsPoints.join('|') + ')'
    const regexp = new RegExp(listeChoixPoints + '[+-]' + listeChoixPoints)
    return regexp.test(ch)
  }
  function contientVectSousVect (ch) {
    let i, j
    for (i = 0; i < sq.nomsVect.length; i++) {
      let st1 = sq.nomsVect[i]
      if (st1 === 'i') st1 = "i'"
      for (j = 0; j < sq.nomsVect.length; j++) {
        let st2 = sq.nomsVect[j]
        if (st2 === 'i') st2 = "i'"
        if (ch.indexOf('\\####{' + st1 + st2) !== -1) return true
      }
      // Correction version Sésaparcours : il aut aussi refuser un vecteur sous une flèche avec un point
      for (j = 0; j < sq.nomsPoints.length; j++) {
        if ((ch.indexOf('\\####{' + st1 + sq.nomsPoints[j]) !== -1) ||
          (ch.indexOf('\\####{' + sq.nomsPoints[i] + st1) !== -1)) return true
      }
    }
    return false
  }
  // Fonction qui renvoie false si la chaîne ch contient des noms de vecteurs qui n’ont pas été inclus dans des parenthèses
  // Utilisée dans traiteMathquill
  function contientVectSansFleche (ch) {
    for (let i = 0; i < sq.nomsVect.length; i++) {
      const st = sq.nomsVect[i]
      let k = ch.indexOf(st)
      if (k !== -1) {
        if (k === 0) return true
        else {
          if (!ch.substring(0, k).trim().endsWith('\\####{')) return true
          k = ch.indexOf(st, k + 1)
        }
      }
    }
    return false
  }

  /**
   * Fonction extrayant d’un affichage LaTeX de la figure les lignes de texte et les transformant
   * en une chaîne affichable par MathQuill
   * @param tagLatex : le tag de l’affichage LaTeX de la figure
   * @return {string}
   */
  function extraitDeLatexPourMathQuill (tagLatex) {
    const list = sq.mtgAppLecteur.getList('mtg32svg')
    const latex = list.getByTag(tagLatex)
    if (latex === null || latex.className !== 'CLatex') return ''
    return cleanLatexForMq(latex.chaineLatex)
  }

  /**
   * Fonction affichant la solution s’il existe un affichage LaTeX de tag solution
   * @param{boolean} bexact true si l’élève a entré la bonne réponse, false sinon
   */
  function afficheSolution (bexact) {
    $('#divSolution').css('display', 'block').html('')
    const ch = extraitDeLatexPourMathQuill('solution').replace(/\\frac/g, '\\dfrac')
    if (ch !== '') {
      afficheMathliveDans('divSolution', 'solution', ch, { style: { color: bexact ? 'green' : 'blue' } })
    }
  }

  function cacheSolution () {
    $('#divSolution').css('display', 'none').html('')
  }

  function traiteMathlive (ch) {
    if (ch === '\\overrightarrow{0}') return { res: '0', valid: true, vectNul: true, contientVecsNuls: false }
    ch = ch.replace(/\\frac(\d)(\d)/g, '\\frac{$1' + '}{' + '$2}')
    // Ni pour les racines carrées d’un entier
    ch = ch.replace(/\\sqrt(\d)/g, '\\sqrt{$1}')
    // Pour les puissances dont l’exposant est un chiffre, Mathlive ne met pas d’accolades
    ch = ch.replace(/\^([^{])/g, '^{$1}')
    // mathlive peut mettre des {} vides
    ch = ch.replace(/\\{\\}/g, '')
    ch = ch.replace(/\^(\d)(\d)/g, '^$1*$2') // On peut avoir par exemple 2^35^4 pour 2^3*5^4
    // On regarde si la réponse contient des vecteurs nuls du type vec(AA).
    // Si oui une réponse ne sera pas accepté comme simplifiée
    let contientVecsNuls = false
    let st = sq.nomsPoints
    // ON remplace tous les \overrightarrow par des \#### de façon qu’il n’y ait pas dedans de lettre
    // succeptible d'être un nom de vecteur
    ch = ch.replace(/\\overrightarrow/g, '\\####')
    if (ch.indexOf('\\####{0}') !== -1) contientVecsNuls = true
    else {
      for (let i = 0; i < st.length; i++) {
        const nom = st[i]
        if (ch.indexOf('\\####{' + nom + nom + '}') !== -1) {
          contientVecsNuls = true
          break
        }
      }
    }
    ch = ch.replace(/\\####\{0}/g, '(v000)') // à cause de l’appel à addImplicitMult, on met v000 qui sera remplacé à la fin par vect0
    // Attention : Les parenthèses entourant le v0 sont indispensables
    const bcontientVectSansFleche = contientVectSansFleche(ch)
    st = sq.nomsVect
    for (let i = 0; i < st.length; i++) {
      if (st[i] !== 'i') ch = ch.replace(new RegExp('\\\\####{(' + st[i] + ')}', 'g'), '$1')
    }
    const bsomOuDifPt = contientSomOuDifPoints(ch)
    const bvectDeVect = contientVectSousVect(ch)
    //  = ch.replace(/\\vecteur{i}/g, "i'") // i n’est pas un nom correct dans mtg32
    ch = ch.replace(/\\####\{i}/g, "i'") // i n’est pas un nom correct dans mtg32
    ch = ch.replace(/\\####\{([a-zA-Z]['"]?)([a-zA-Z]['"]?)}/g, '($2-$1)')

    const correct = (ch.indexOf('\\####{') === -1) && !bsomOuDifPt && !bvectDeVect && !bcontientVectSansFleche
    ch = unLatexify(ch)
    // S’il reste des \vecteur{ c’est que l’expression est incorrecte
    if (correct) ch = sq.mtgAppLecteur.addImplicitMult('mtg32svg', ch)// Traitement des multiplications implicites
    ch = ch.replace(/v000/g, 'vect0')
    return { res: ch, valid: correct, vectNul: false, contientVecsNuls }
  }

  function marqueEditeurPourErreur () {
    sq.marked = true
    const mf = j3pElement(inputId)
    if (!mf) return
    mf.style.backgroundColor = '#FF9999'
    mf.demarqueErreur = () => {
      if (sq.marked) {
        demarqueEditeurPourErreur()
      }
    }
  }

  function demarqueEditeurPourErreur () {
    sq.marked = false
    j3pEmpty('correction')
    const mf = j3pElement(inputId)
    if (!mf) return
    mf.style.backgroundColor = 'white'
  }

  // Le deuxième paramètre est à false seulement quand elle est appelée depuis l’étape de correction
  // Sinon la fonction est appelée par l’éditeur quand l’utilisateur appuie sur la touche entrée avec un seul paramètre
  // Cette fonction renvoie true si parcours.sectionCourante() a dû être appelé et false sinon
  function validation (parcours, continuer) {
    if (arguments.length <= 1) continuer = true
    if (sq.nbexp === 0) {
      j3pElement('boutonrecopier').style.display = 'block'
    }
    sq.rep = getMathliveValue(inputId)
    if (sq.resultatSyntaxe.syntaxVecOK && sq.resultatSyntaxe.isVec) {
      const resMathQuill = traiteMathlive(sq.rep)
      const chcalcul = resMathQuill.res
      sq.mtgAppLecteur.giveFormula2('mtg32svg', 'rep', chcalcul)
      j3pEmpty('correction')
      sq.mtgAppLecteur.calculate('mtg32svg', false)
      if (sq.indicationfaute) {
        const faute = sq.mtgAppLecteur.valueOf('mtg32svg', 'faute')
        if (faute === 1) {
          sq.mtgAppLecteur.executeMacro('mtg32svg', 'voirFaute')
        } else {
          sq.mtgAppLecteur.executeMacro('mtg32svg', 'masquerFaute')
        }
      }
      sq.reponse = sq.mtgAppLecteur.valueOf('mtg32svg', 'reponse')
      if ((sq.reponse === 1) && resMathQuill.contientVecsNuls) sq.reponse = 2 // Pour ne pas accepter une réponse avec des vec(AA)
    } else sq.reponse = 3 // Considéré comme faux si erreur de syntaxe sur les vecteurs
    const idrep = 'exp' + sq.nbexp
    sq.nbexp++
    const chdeb = sq.nbexp === 1 ? '' : '<br>'
    if (sq.resultatSyntaxe.syntaxVecOK && sq.resultatSyntaxe.isVec && sq.reponse >= 1) {
      afficheMathliveDans('formules', idrep, chdeb + '$' + (sq.entete === '' ? sq.aCalculer : sq.entete) + '$ $' + sq.symbexact +
        ' ' + sq.rep + '$', {
        style: {
          color: '#0000FF'
        }
      })
    } else {
      if (sq.resultatSyntaxe.syntaxVecOK && sq.resultatSyntaxe.isVec) {
        afficheMathliveDans('formules', idrep, chdeb + '$' + (sq.entete === '' ? sq.aCalculer : sq.entete) + '$ $' + sq.symbnonexact +
            ' ' + sq.rep + '$', {
          style: {
            color: '#FF0000'
          }
        })
      } else { // On compte une  écriture incorrecte sur les vecteurs comme une faute
        afficheMathliveDans('formules', idrep, chdeb + '$' + '\\text{Invalide : }' + (sq.entete === '' ? sq.aCalculer : sq.entete) + ' ' + sq.symbexact +
            ' ' + sq.rep + '$', {
          style: {
            color: '#FF0000'
          }
        })
      }
      const texteFaute = sq.indicationfaute ? extraitDeLatexPourMathQuill('faute') : ''
      if (texteFaute) {
        afficheMathliveDans('formules', idrep + 'faute', '<br> + faute', {
          style: {
            color: '#FF0000'
          }
        })
      }
    }
    // On vide le contenu de l’éditeur MathQuill
    j3pElement(inputId).value = ''

    // Si le nombre maximum d’essais est atteint on passe en mode correction
    if (sq.nbexp >= sq.nbEssais) {
      // sq.mtgAppLecteur.setActive("mtg32svg", false);
      if (continuer) {
        parcours.etat = 'correction'
        parcours.sectionCourante()
        $('#editeur').css('display', 'none')
        $('#boutonsmathquill').css('display', 'none')
        return true
      }
    } else {
      j3pEmpty('info')
      afficheInfo()
    }
    return false
  }

  function afficheInfo () {
    j3pEmpty('info')
    const nbe = sq.nbEssais - sq.nbexp
    let nbc = sq.nbchances - sq.numEssai + 1
    if (!sq.validationAuto && nbc > nbe) {
      nbc = nbe
    }
    if (sq.validationAuto) {
      if (nbe === 1) {
        afficheMathliveDans('info', 'texteinfo', 'Il reste un essai.', { style: { color: '#7F007F' } })
      } else {
        afficheMathliveDans('info', 'texteinfo', 'Il reste ' + nbe + ' essais.', { style: { color: '#7F007F' } })
      }
    } else {
      if ((nbe === 1) && (nbc === 1)) {
        afficheMathliveDans('info', 'texteinfo', 'Il reste une validation.', { style: { color: '#7F007F' } })
      } else {
        afficheMathliveDans('info', 'texteinfo', 'Il reste ' + nbe + ' essai(s) et ' + nbc + ' validation(s).', { style: { color: '#7F007F' } })
      }
    }
  }

  function recopierReponse () {
    demarqueEditeurPourErreur()
    j3pElement(inputId).value = sq.rep
    focusIfExists(inputId)
  }

  function initMtg () {
    setMathliveCss('MepMG')
    const mtgOptions = {
      // player only, sans figure initiale ni svg, on gère ça dans initMtg et modif_fig
      loadCoreWithMathJax: true
    }
    getMtgApp('mtg32svg', {}, mtgOptions).then((mtgAppLecteur) => {
      sq.mtgAppLecteur = mtgAppLecteur
      initDom()
      let code, par, car, nbrep, ar, tir, nb
      sq.mtgAppLecteur.removeAllDoc()
      sq.mtgAppLecteur.addDoc('mtg32svg', sq.txt, true)
      sq.mtgAppLecteur.calculate('mtg32svg', false)
      sq.nbexp = 0
      sq.reponse = -1
      const ch = 'abcdefghjklmnpqr'
      const nbvar = sq.mtgAppLecteur.valueOf('mtg32svg', 'nbvar')
      if (nbvar !== -1) {
        nbrep = parcours.donneesSection.nbrepetitions
        sq.aleat = true
        sq.nbParamAleat = nbvar
        for (let i = 1; i <= nbvar; i++) {
          const nbcas = sq.mtgAppLecteur.valueOf('mtg32svg', 'nbcas' + i)
          nb = Math.max(nbrep, nbcas)
          ar = []
          for (let j = 0; j < nb; j++) ar.push(j % nbcas)
          sq['tab' + i] = []
          for (let k = 0; k < nbrep; k++) {
            tir = Math.floor(Math.random() * ar.length)
            sq['tab' + i].push(ar[tir])
            ar.splice(tir, 1)
          }
          sq.mtgAppLecteur.giveFormula2('mtg32svg', 'r' + i, sq['tab' + i][0])
        }
      } else {
        sq.aleat = false
      }

      if (sq.param !== undefined) {
        for (let i = 0; i < ch.length; i++) {
          car = ch.charAt(i)
          if (sq.param.indexOf(car) !== -1) {
            par = parcours.donneesSection[car]
            if (par !== 'random') sq.mtgAppLecteur.giveFormula2('mtg32svg', car, par)
          }
        }
      }
      sq.mtgAppLecteur.calculateAndDisplayAll(true)
      const nbParam = parseInt(sq.nbLatex)
      const t = ['a', 'b', 'c', 'd']
      const param = {}
      for (let i = 0; i < nbParam; i++) {
        code = sq.mtgAppLecteur.getLatexCode('mtg32svg', i)
        if (i === 0) sq.aCalculer = code.replaceAll('\n', '')
        param[t[i]] = code
      }
      sq.paramLatex = param

      const st = sq.consigne1 + (sq.simplifier ? sq.consigne3 : sq.consigne2) + sq.consigne4
      afficheMathliveDans('enonce', 'texte', st, param)
      const deb = sq.entete !== '' ? '$' + sq.entete + '$ = ' : '$' + sq.aCalculer + sq.symbexact + '$ '
      afficheMathliveDans('editeur', 'expression', deb + '&1&', {
        charset: sq.charset,
        listeBoutons: sq.listeBoutons
      })
      if (sq.charset !== '') {
        mathliveRestrict(inputId, sq.charset)
      }

      sq.indicationfaute = parcours.donneesSection.indicationfaute // true si indication quand certaines fautes sont faites et appui sur Entrée
      // Les 3 lignes suivantes déplacées ici pour que sq.mtgAppLecteur soit prêt quand onkeyup est appelé
      j3pElement(inputId).onkeyup = function (ev) {
        onkeyup(ev, parcours)
      }
      afficheInfo()
      focusIfExists(inputId)
      parcours.finEnonce()
    }).catch(j3pShowError)
  }

  function initDom () {
    renderMathInDocument({ renderAccessibleContent: '' })
    if (sq.titre) parcours.afficheTitre(sq.titre)

    j3pDiv(parcours.zones.MG, {
      id: 'conteneur',
      contenu: '',
      style: parcours.styles.etendre('toutpetit.enonce', { padding: '6px' })
    })
    j3pDiv('conteneur', 'enonce', '')
    j3pDiv('conteneur', {
      id: 'formules',
      contenu: '',
      style: parcours.styles.petit.enonce
    }) // Contient le formules entrées par l’élève
    if (sq.bigSize) $('#formules').css('font-size', '26px')
    j3pDiv('conteneur', 'info', '')
    j3pDiv('conteneur', 'conteneurbouton', '')
    j3pAjouteBouton('conteneurbouton', 'boutonrecopier', 'MepBoutonsRepere', 'Recopier réponse précédente',
      recopierReponse)
    j3pElement('boutonrecopier').style.display = 'none'

    j3pDiv('conteneur', 'editeur', '') // Le div qui contiendra l’éditeur MathQuill
    if (sq.bigSize) $('#editeur').css('font-size', '30px')
    $('#editeur').css('padding-top', '10px') // Pour laisser un peu de place au-dessus de l’éditeur
    j3pDiv('conteneur', 'boutonsmathquill', '')
    $('#boutonsmathquill').css('display', 'inline-block')
    $('#boutonsmathquill').css('padding-top', '10px') // Pour laisser un peu de place au-dessus des boutons
    j3pPaletteMathquill('boutonsmathquill', inputId, { liste: sq.listeBoutons })
    j3pDiv('conteneur', 'divSolution', '')
    $('#divSolution').css('display', 'none')
    j3pDiv('conteneur', 'divmtg32', '')
    j3pCreeSVG('divmtg32', {
      id: 'mtg32svg',
      width: sq.width,
      height: sq.height
    })
    const style = parcours.styles.petit.correction
    style.marginTop = '1.5em'
    style.marginLeft = '0.5em'
    j3pAddElt(parcours.zonesElts.MD, 'div', '', { id: 'correction', style })
  }

  switch (this.etat) {
    case 'enonce': {
      // code exécuté au lancement de la section
      if (this.debutDeLaSection) {
        // Construction de la page
        parcours.construitStructurePage('presentation1bis')
        sq.nbEssais = parseInt(parcours.donneesSection.nbEssais)
        sq.nbchances = parseInt(parcours.donneesSection.nbchances)
        sq.simplifier = parcours.donneesSection.simplifier
        if (sq.simplifier === undefined) {
          sq.simplifier = true
        }
        // compteur. Si   numEssai > nbchances, on corrige
        sq.numEssai = 1
        sq.validationAuto = parcours.donneesSection.validationAuto
        this.validOnEnter = sq.validationAuto
        if (!sq.validationAuto && (sq.nbchances > sq.nbEssais)) sq.nbchances = sq.nbEssais
        // faut gérer sous-dossier ou pas…
        const sep = this.donneesSection.ex.includes('/') ? '-' : '/'
        j3pImporteAnnexe('squelettes-mtg32' + sep + this.donneesSection.ex + '.js').then(function (datasSection) {
          // console.log("initsection");
          // Attention, le this devient window...
          sq.txt = datasSection.txt
          sq.entete = datasSection.entete ?? ''
          sq.symbexact = datasSection.symbexact ?? '='
          sq.symbnonexact = datasSection.symbnonexact ?? '\\ne'
          sq.nbLatex = Number(datasSection.nbLatex ?? 0) // Le nombre de paramètres LaTeX dans le texte
          const listeBoutons = ['vecteur']
          // boutons avec propriétés à true par défaut
          const tabBoutons1 = ['puissance', 'fraction']
          ;['btnPuis', 'btnFrac'].forEach((p, i) => {
            const b = Boolean(datasSection[p] ?? true)
            if (b) listeBoutons.push(tabBoutons1[i])
          })
          // boutons avec false par défaut
          const tabBoutons2 = ['racine', 'pi', 'exp', 'ln', 'sin', 'cos', 'tan', 'abs']
          ;['btnRac', 'btnPi', 'btnExp', 'btnLn', 'btnSin', 'btnCos', 'btnTan', 'btnAbs'].forEach((p, i) => {
            const b = Boolean(datasSection[p] ?? false)
            if (b) listeBoutons.push(tabBoutons2[i])
          })
          sq.listeBoutons = listeBoutons
          sq.consigne1 = datasSection.consigne1 ?? ''
          sq.consigne2 = datasSection.consigne2 ?? ''
          sq.consigne3 = datasSection.consigne3 ?? ''
          if (!sq.validationAuto) {
            sq.consigne4 = datasSection.consigne4 ?? ''
          } else {
            const s = datasSection.consigne4SiValidationAuto
            if (s !== undefined) {
              sq.consigne4 = datasSection.consigne4SiValidationAuto
            } else {
              sq.consigne4 = datasSection.consigne4 ?? ''
            }
          }
          sq.bigSize = Boolean(datasSection.bigSize ?? false)
          sq.titre = datasSection.titre ?? ''
          sq.width = Number(datasSection.width ?? 799)
          sq.height = Number(datasSection.height ?? 800)
          sq.param = datasSection.param ?? '' // Chaine contenant les paramètres autorisés
          sq.nomsVect = datasSection.nomsVect
          sq.nomsPoints = datasSection.nomsPoints
          sq.charset = datasSection.charset ?? ''
          initMtg()
        }).catch(error => {
          console.error(error)
          j3pShowError('Impossible de charger les données de cette exercice')
        })
        // return;
      } else {
        j3pEmpty('correction')
        cacheSolution()
        sq.numEssai = 1
        $('#editeur').css('display', 'block')
        $('#boutonsmathquill').css('display', 'inline-block')
        sq.mtgAppLecteur.removeDoc('mtg32svg')
        sq.mtgAppLecteur.addDoc('mtg32svg', sq.txt, true)
        if (sq.aleat) {
          for (let i = 1; i <= sq.nbParamAleat; i++) {
            sq.mtgAppLecteur.giveFormula2('mtg32svg', 'r' + i, sq['tab' + i][this.questionCourante - 1])
          }
        }
        if (sq.param !== undefined) {
          const ch = 'abcdefghjklmnpqr'
          for (let i = 0; i < ch.length; i++) {
            const car = ch.charAt(i)
            if (sq.param.indexOf(car) !== -1) {
              const par = parcours.donneesSection[car]
              if (par !== 'random') sq.mtgAppLecteur.giveFormula2('mtg32svg', car, par)
            }
          }
        }

        sq.mtgAppLecteur.calculate('mtg32svg', true) // true pour que les calculs alèatoires soient réinitialisés
        j3pEmpty('enonce')
        j3pEmpty('formules')
        j3pEmpty('info')
        j3pEmpty('editeur')
        sq.nbexp = 0
        sq.reponse = -1
        const nbParam = parseInt(sq.nbLatex)
        const t = ['a', 'b', 'c', 'd']
        const param = {}
        for (let i = 0; i < nbParam; i++) {
          const code = sq.mtgAppLecteur.getLatexCode('mtg32svg', i)
          if (i === 0) sq.aCalculer = code.replaceAll('\n', '')
          param[t[i]] = code
        }

        const st = sq.consigne1 + (sq.simplifier ? sq.consigne3 : sq.consigne2) + sq.consigne4
        afficheMathliveDans('enonce', 'texte', st, param)
        const deb = sq.entete !== '' ? '$' + sq.entete + '$ = ' : '$' + sq.aCalculer + sq.symbexact + '$ '
        afficheMathliveDans('editeur', 'expression', deb + '&1&', {
          charset: sq.charset,
          listeBoutons: sq.listeBoutons
        })
        if (sq.charset !== '') {
          mathliveRestrict(inputId, sq.charset)
        }
        j3pElement(inputId).onkeyup = function (ev) {
          onkeyup(ev, parcours)
        }
        afficheInfo()
        j3pElement('info').style.display = 'block'
        sq.mtgAppLecteur.display('mtg32svg') // Pour tout réafficher
        // Pour revenir en haut de page
        try {
          window.location.hash = 'MepMG'
          j3pElement('MepMG').scrollIntoView()
        } catch (e) {
          console.warn(e) // pas normal mais pas grave
        }
        // On vide le contenu de l’éditeur MathQuill et on lui donne le focus.
        j3pElement(inputId).value = ''
        focusIfExists(inputId)
        this.finEnonce()
      }

      break // case "enonce":
    } // case 'enonce'
    case 'correction': {
      let bilanreponse = ''
      let simplifieri = false
      // On regarde si le champ d’édition est rempli même si l’utilisateur
      // n’a pas appuyé sur la touche Entrée.
      const ch = getMathliveValue(inputId)
      if (ch !== '') {
        const rep = traiteMathlive(ch)
        const repcalcul = rep.res
        const tabNames = []
        let valide
        if (rep.vectNul) {
          valide = true
          sq.resultatSyntaxe = { syntaxOK: true, syntaxVecOK: true, isVec: true }
        } else {
          for (let k = 0; k < sq.nomsVect.length; k++) {
            let nom = sq.nomsVect[k]
            if (nom === 'i') nom = "i'"
            tabNames.push(nom)
          }
          for (let k = 0; k < sq.nomsPoints.length; k++) {
            let nom = sq.nomsPoints[k]
            if (nom === 'i') nom = "i'"
            tabNames.push(nom)
          }
          sq.resultatSyntaxe = sq.mtgAppLecteur.calcVectOK('mtg32svg', repcalcul, tabNames)
          valide = rep.valid && sq.resultatSyntaxe.syntaxOK
        }
        if (valide) {
          const returnNecessaire = validation(this, false)
          if (returnNecessaire) return
        } else {
          bilanreponse = 'incorrect'
        }
      } else {
        if (sq.nbexp === 0) bilanreponse = 'incorrect'
        // else on a un bilanreponse qui reste vide et resultatSyntaxe undefined
      }
      const res1 = sq.reponse
      if (bilanreponse !== 'incorrect') {
        if (sq.simplifier) { // Si on demande le calcul simplifié
          if (res1 === 1) {
            bilanreponse = 'exact'
            simplifieri = true
          } else {
            if (res1 === 2) {
              bilanreponse = 'exactpasfini'
            } else {
              if (res1 === 3) {
                bilanreponse = 'erreursyntaxe'
              } else bilanreponse = 'erreur'
            }
          }
        } else {
          if ((res1 === 1) || (res1 === 2)) {
            bilanreponse = 'exact'
            if (res1 === 1) {
              simplifieri = true
            }
          } else {
            if (res1 === 3) {
              bilanreponse = 'erreursyntaxe'
            } else bilanreponse = 'erreur'
          }
        }
      }
      // A cause de la limite de temps :
      if (this.isElapsed) { // limite de temps
        j3pElement('correction').style.color = this.styles.cfaux
        j3pElement('boutonrecopier').style.display = 'none'
        j3pElement('info').style.display = 'none'
        if (sq.indicationfaute) {
          sq.mtgAppLecteur.giveFormula2('mtg32svg', 'faute', '0')
          sq.mtgAppLecteur.calculate('mtg32svg', false)
          sq.mtgAppLecteur.executeMacro('mtg32svg', 'masquerFaute')
        }
        $('#editeur').css('display', 'none')
        $('#boutonsmathquill').css('display', 'none')
        sq.mtgAppLecteur.executeMacro('mtg32svg', 'solution')
        afficheSolution(false)
        j3pElement('correction').innerHTML = tempsDepasse
        j3pElement('correction').innerHTML += '<br>La solution se trouve ci-contre.'
        this.etat = 'navigation'
        this.sectionCourante()
      } else {
        if (bilanreponse === 'incorrect') {
          j3pElement('correction').style.color = this.styles.cfaux
          j3pElement('correction').innerHTML = 'Réponse incorrecte'
          marqueEditeurPourErreur()
          focusIfExists(inputId)
        } else {
          // Une réponse a été saisie
          // Bonne réponse
          if (bilanreponse === 'exact') {
            // sq.mtgAppLecteur.setActive("mtg32svg", false);
            this.score++
            j3pElement('correction').style.color = this.styles.cbien
            if (simplifieri) {
              j3pElement('correction').innerHTML = cBien
            } else {
              j3pElement('correction').innerHTML = 'C’est bien mais on pouvait  simplifier la réponse'
            }
            j3pElement('boutonrecopier').style.display = 'none'
            j3pElement('info').style.display = 'none'
            if (sq.indicationfaute) {
              sq.mtgAppLecteur.executeMacro('mtg32svg', 'masquerFaute')
            }
            sq.mtgAppLecteur.executeMacro('mtg32svg', 'solution')
            afficheSolution(true)
            $('#editeur').css('display', 'none')
            $('#boutonsmathquill').css('display', 'none')
            return this.finCorrection('navigation', true)
          } else {
            j3pElement('correction').style.color = this.styles.cfaux
            sq.numEssai++
            if (bilanreponse === 'exactpasfini') {
              j3pElement('correction').innerHTML = 'Le calcul est bon mais pas écrit sous la forme demandée.'
            } else {
              if (bilanreponse === 'erreursyntaxe') j3pElement('correction').innerHTML = 'Calcul vectoriel invalide'
              else j3pElement('correction').innerHTML = cFaux
            }
            j3pEmpty('info')
            afficheInfo()
            // S’il y a plus de deux chances,on appelle à nouveau le bouton Valider

            if ((sq.nbexp < sq.nbEssais) && (sq.validationAuto
              ? true
              : (sq.numEssai <= sq.nbchances))) {
              j3pElement('correction').innerHTML += '<br>' + '<br>' + essaieEncore
              // On donne le focus à l’éditeur MathQuill
              focusIfExists(inputId)
              // on reste en correction
              return this.finCorrection()
              // indication éventuelle ici
              // parcours.Sectionsquelettemtg32_Calc_Param_Vect.nbExp += 1;
            } else {
              // Erreur au nème essai
              // sq.mtgAppLecteur.setActive("mtg32svg", false);
              j3pElement('boutonrecopier').style.display = 'none'
              j3pElement('info').style.display = 'none'
              if (sq.indicationfaute) {
                sq.mtgAppLecteur.executeMacro('mtg32svg', 'masquerFaute')
              }
              sq.mtgAppLecteur.executeMacro('mtg32svg', 'solution')
              afficheSolution(false)
              $('#editeur').css('display', 'none')
              $('#boutonsmathquill').css('display', 'none')

              // j3pElement("correction").innerHTML+='<br>' + regardeCorrection;
              j3pElement('correction').innerHTML += '<br><br>La solution se trouve ci-contre.'
              this.finCorrection('navigation', true)
            }
          }
        }
      }
      // Obligatoire
      this.finCorrection()
      break // case "correction":
    } // case 'correction'

    case 'navigation':
      if (!this.sectionTerminee()) {
        this.etat = 'enonce'
      }
      this.finNavigation()
      break // case "navigation":
  }
}
