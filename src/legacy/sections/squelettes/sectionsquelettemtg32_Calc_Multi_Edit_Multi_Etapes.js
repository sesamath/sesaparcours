import $ from 'jquery'
import { j3pAddElt, j3pAjouteBouton, j3pDetruit, j3pDiv, j3pElement, j3pEmpty, j3pImporteAnnexe, j3pPaletteMathquill, j3pRestriction, j3pShowError, j3pValeurde } from 'src/legacy/core/functions'
import ListeDeroulante from 'src/legacy/outils/listeDeroulante/ListeDeroulante'
import { unLatexify } from 'src/lib/mathquill/functions'
import { j3pCreeFenetres, j3pToggleFenetres } from 'src/legacy/core/functionsJqDialog'
import { getMtgApp } from 'src/lib/outils/mathgraph'
import { j3pCreeSVG } from 'src/legacy/core/functionsSvg'
import { getFirstIndexParExcluded, getIndexFermant } from 'src/lib/utils/string'
import { MathfieldElement, renderMathInDocument } from 'mathlive'
import { setMathliveCss } from 'src/lib/outils/mathlive/mathlivecss'
import { cleanLatexForMl, mathliveRestrict, transposeMatrices } from 'src/lib/outils/mathlive/utils'
import { afficheMathliveDans, getMathliveValue } from 'src/lib/outils/mathlive/display'
import { hideCurrentVirtualKeyboard, resetKeyboardPosition } from 'src/lib/widgets/mlVirtualKeyboard/VirtualKeyboard'
import textesGeneriques from 'src/lib/core/textes'

import { setFocusById, setFocusToFirstInput } from 'src/legacy/sections/squelettes/helpersMtg'

const { cBien, cFaux, essaieEncore, tempsDepasse } = textesGeneriques

/*
    Yves Biton
    2021. Revu en 2023 pour passage à Mathlive

    squelettemtg32_Calc_Multi_Edit_Multi_Etapes
    Squelette permettant de poser des questions successives sous forme de formulaires.
    Utilise une fichier annexe dont le nom est contenu dans le paramètre ex.
    Ce fichier annexe contient une variable txt qui contient le code base 64 de la figure
    MathGraph32 associée.
    Le nombre d’étapes est renvoyé par la figure mtg32 associée dans le calcul nbEtapes (8 maxi).
    Chaque formulaire (qui est affiché en mode texte) peut contenir des listes de choix déroulantes,
    des éditeurs en mode texte (répérés par les caractères edit1, edit2, ...), des éditeurs en mode
    maths repérés par des \editable{} (sans numérotation). Ces derniers permettent qu’une partie d’une
    formule LaTeX soit un champ d’édition.
    Le formulaire à l’étape i est renvoyé par un affichage LaTeX (formé éventuellement d’un tableau de plusieurs lignes,
    chaque ligne étant formée d’un \text{}) dont la tag doit être formulaire suivi du n° de l’étape.
    A chaque étape, le formulaire est précédé d’un affichage de consigne qui doit reprendre les résultats des quesions précédentes.
    Cette consigne est données par un affichage LaTeX qui est en général un tableau formé de plusieurs lignes,
    chaque ligne étant formée d’un \text{} dont le contenu fourni la ligne correspondante.
    Cet affichage LaTeX de la consigne à l’étape i doit avoir pour tag consigne suivi du n° i.
    On peut éventuellement spécifier des paramètres  a,b,c,d,e,f,g,h,j,k,n,n,p.
    Se référer au fichier annexe paramétré par défaut pour les voir.
    Ce formulaire peut contenir, en mode texte, des éditeurs mathquill, repérés dans le formulaire par edit suivi
    du numéro de l’éditeur (les n°s commençant à 1) et, en mode maths, des \editable{} ce qui permet
    de mettre des éditeurs directement à l’intérieur de formules, comme par exemple un éditeur sous une racine carrée.
    Il faut aussi contenir des contenir des choix à liste déroulantes repérés par list suivi de n° de la liste.
    Les \editable{} eux n’ont pas à être numérotés.
    Imaginons par exemple que le LaTeX de tag formulaire2 contienne ceci (nous sommes à l’étape 2) :
    \text{La fonction $f$ est list1 sur $\R$, \Val{a} a edit1 antécédents par $f$ et $f(\Val{b})=\editable{}\sqrt{2}+\editable{}$}
    Ce formulaire a un choix de liste. La figure mtg32 doit donc contenir un affichage LaTeX de tag list21, formé d’une tableau
    de deux lignes dont les deux lignes sont par exemple \text{croissante} et \text{décroissante}. Elle doi aussi contenir
    un calcul nommé reslist21 dont la valeur est l’indice de la bonne réponse (1 ou 2 ici).
    Le premier chiffre du nom du calcul (reslist21 par exemple) est le n° de l’étape et le second le n° de la liste
    dans le formulaire.
    Si la figure contient un calcul nommé listaleat21 qui vaut 1, les éléments de la liste seront présentés
    mélangés au hasard
    Si plusieurs items de la liste peuvent être acceptés, la figure doit contenir un calcul nommé par exemple repList21
    qui doit être initalisé à une valeur autre que -1.
    Si ce calcul existe, la figure doit contenir un autre calcul nommé exactList21 qui doit valoir 1 si la réponse
    de l’élève est une des réponses acceptées et 0 sinon.
    Il contient un éditeur mathquill en mmode texte repéré par edit1.
    Il doit donc contenir un calcul (ou fonction) nommé rep21 et deux calculs nommés resolu21 et exact21.
    resolu21 doit valoir 1 si la réponse entrée par l’élève dans ce champ d’édition est une réponse acceptée commme
    réponse finale et 0 sinon. exact21 doit lui renvoyer 1 si la réponse contenue dans le champ est exacte et 0 sinon.
    Le formulaire contient deux \editable{}. La figure doit donc contenir deux calculs (ou fonctions) nommés repEditable21
    et repEditable22 destinés à contenir les réponses contenues dans ces champs.
    La figure doit donc aussi contenir des calculs nommés resoluEditable21, exactEditable21, resoluEditable22, exactEditable22.
    Par exemple, resoluEditable21 doit renvoyer 1 si la réponse dans le premier editable est acceptée comme réponse finale
    et 0 sinon, et exactEditable21 lui doit renvoyer 1 si la réponse dans le premier editable est exacte et 0 sinon.
    Si le paramètre bigSize du fichier annexe est présent et à true, l’éditeur de formulaire et les réponses de l’élève utiliseront
    une taille plus grande que la taille standard.
    Si, par exemple à l’étape 2, on a deux champ d’éditon MathQuill dont le deuxième doit contenir une égalité, la figure MathGraph32,
    la figure doit contenir un calcul nommé estEgalie22 qui revoie la valeur 1.
    Il et possible de faire afficher des lignes lors de la correction. Ces lignes doivent alors être les lignes d’une affichage LaTEX
    de tag solution de la figure.
    Si on ne souhaite pas que la figure mtg32 soit affichée, il faut mettre les paramètres width et height du fichier annexe
    à 0 Sinon ces variables doivent contenir la largeur et la hauteur de la figure en pixels.
    On peut aussi faire afficher à la correction une figure dont le code base 64 est contenu dans le paramètre figSol
    du fichier annexe, les paramètres widthSol et heightSol désignant la largeur et la hauteur de cette figure.
    Avant d’afficher cette figure de correction, on donne aux calculs ou fonctions dont le nom figure dans la paramètre de nom
    param les formules qu’ils avaient dans la figure principale.
    Avant la figure de correction, on peut afficher des epxlications contenues dans le paramètre explications.
    Ces explications peuvent utiliser des affichages LaTeX de la figure de correction, leu nombre étant dans le paramètre
    nbLatexSol. On fait référence à eux via des $£a$, $£b$, etc ...
    Si la figure est affichée, elle peut contenir une macro d’intitulé solution qui sera exécutée lors de la correction.
    Si la figure contenue  dans ex contient un paramètre nbVar , alors dans le cas ou nbrepetitions > 1, on donne pour i compris entre 1 et npParamAleat
    au calcul de la figure nommé "r"+i (par exemple r1 et r2 si nbParamAleat vaut 2) des valeurs comprises entre 0 et "nbcas"+i - 1
    toutes différentes (sauf si nbrepetitions > b=nbcas auquel cas au bout de nbcas répétitions il y aura nécessairement une valeur de r déjà prise.
    Si on veut accepter une réponse +∞ ou -∞ dans un champ d’édition, le bouton correspondant doit être activé dans le fichier annexe (btnInf)
    et la figure mtg32 doit contenir un calcul de nom infinity qui ne soit pas égal à -1
*/
// on définit le dossier des fonts mathlive (il faut le faire très tôt,
// ici c’est bon avec pnpm start mais trop tard pour un build (cf src/lib/player/index.js de la branche draftPlayer)
MathfieldElement.fontsDirectory = '/static/mathlive/fonts'
MathfieldElement.soundsDirectory = '/static/mathlive/sounds'

export const params = {
  outils: ['calculatrice', 'mathquill'],
  /**
   * Ce qui est paramétrable dans l’éditeur de graphe
   * @type {LegacySectionParamsParametres}
   */
  parametres: [
    ['nbrepetitions', 1, 'entier', 'aucun'],
    ['limite', 0, 'entier', 'Temps disponible par répétition, en secondes'],
    ['calculatrice', false, 'boolean', 'true pour avoir une calculatrice disponible'],
    ['nbEssais1', 3, 'entier', 'Nombre maximum d’essais  autorisés pour les calculs à l’étape 1'],
    ['nbEssais2', 3, 'entier', 'Nombre maximum d’essais  autorisés pour les calculs à l’étape 2 (si présente)'],
    ['nbEssais3', 3, 'entier', 'Nombre maximum d’essais  autorisés pour les calculs à l’étape 3 (si présente)'],
    ['nbEssais4', 3, 'entier', 'Nombre maximum d’essais  autorisés pour les calculs à l’étape 4 (si présente)'],
    ['nbEssais5', 3, 'entier', 'Nombre maximum d’essais  autorisés pour les calculs à l’étape 5 (si présente)'],
    ['nbEssais6', 3, 'entier', 'Nombre maximum d’essais  autorisés pour les calculs à l’étape 6 (si présente)'],
    ['nbEssais7', 3, 'entier', 'Nombre maximum d’essais  autorisés pour les calculs à l’étape 7 (si présente)'],
    ['nbEssais8', 3, 'entier', 'Nombre maximum d’essais  autorisés pour les calculs à l’étape 8 (si présente)'],
    ['coefEtape1', 0.5, 'number', 'Nombre strictement compris entre 0 et 1 à rajouter au score\npour une réussite à l’étape 1, si nul même répartition pour toutes les étapes'],
    ['coefEtapeFin', -1, 'number', 'Nombre strictement compris entre 0 et 1 à rajouter au score\npour une réussite à la dernière étape, ignoré si égal à -1 ou si le nombre d’étapes est 2'],
    ['infoParam', 'Information sur le rôle des paramètres', 'string', 'Information sur l’utilisation des paramètres suivants'],
    ['a', 'random', 'string', 'Valeur de a'],
    ['b', 'random', 'string', 'Valeur de b'],
    ['c', 'random', 'string', 'Valeur de c'],
    ['d', 'random', 'string', 'Valeur de d'],
    ['e', 'random', 'string', 'Valeur de e'],
    ['f', 'random', 'string', 'Valeur de f'],
    ['g', 'random', 'string', 'Valeur de g'],
    ['h', 'random', 'string', 'Valeur de h'],
    ['j', 'random', 'string', 'Valeur de j'],
    ['k', 'random', 'string', 'Valeur de k'],
    ['l', 'random', 'string', 'Valeur de l'],
    ['m', 'random', 'string', 'Valeur de m'],
    ['n', 'random', 'string', 'Valeur de n'],
    ['p', 'random', 'string', 'Valeur de p'],
    ['q', 'random', 'string', 'Valeur de q'],
    ['r', 'random', 'string', 'Valeur de r'],
    ['ex', 'terminale/Integrale_Linearite_Progressif_1', 'string', 'Nom du fichier annexe (xxx => squelettes-mtg32/xxx.js et xxx/yyy => squelettes-mtg32-xxx/yyy.js)']
  ]
}

const textes = {
  ilReste: 'Il reste ',
  essais: ' essais.',
  unEssai: ' un essai.',
  solutionIci: '\n\nLa solution se trouve ci-contre.',
  solDesact: '\nL’affichage de la solution a été désactivé par le professeur.',
  repIncorrecte: 'Réponse incorrecte',
  egaliteNecess: '\nIl faut écrire une égalité (et une seule)',
  inegaliteNecess: '\nIl faut écrire une inégalité',
  bonPasFini: 'Le calcul est bon mais pas écrit sous la forme demandée.',
  passQuestSuiv: '\n\nOn passe à la question suivante.',
  alertNbEt: 'La figure doit contenir un calcul nbEtapes de valeur entière au moins égale à 1',
  repExacte: 'Réponse exacte : ',
  repFausse: 'Reponse fausse : ',
  repExactPasFini: 'Exact pas fini : ',
  errNbEtapes: 'La figure doit contenir un calcul nommé nbEtapes contenant le nombre d’étapes de l’exercice',
  ilManque: 'Il manque : ',
  unAffLat: 'un affichage LaTeX de tag ',
  avert1: 'L’affichage LaTeX de tag ',
  avert2: ' doit contenir au moins un code LaTeX \\text{}.',
  aide1: 'C’est le contenu du ou des \\text{} qui fourni le contenu à afficher.',
  avert3: ' doit contenir au moins un éditeur repéré par edit, edittext ou list suivi du numéro de l’éditeur' +
      '\n ou un ou plusieurs \\editable{}',
  unCalcNom: 'un calcul (ou une fonction) nommé ',
  pourEtape: 'Pour l’étape ',
  ilManqueMin: ' il manque :',
  deuxCalcNom: 'deux calculs nommés',
  et: 'et',
  ou: 'ou',
  uneSeuleConst: 'La figure ne peut contenir qu’une seule étape de construction'
}

/**
 * section squelettemtg32_Calc_Multi_Edit_Multi_Etapes
 * @this {Parcours}
 */
export default function main () {
  const parcours = this
  const sq = this.storage

  function onkeyup () {
    // Attention : indice est en fait l’id de l’éditeur
    const indice = this.indice
    if (sq.marked[indice]) {
      demarqueEditeurPourErreur(indice)
      j3pEmpty('correction')
    }
  }

  function afficheNombreEssaisRestants () {
    j3pEmpty('info')
    const nbe = parcours.donneesSection['nbEssais' + sq.etape] - sq.numEssai + 1
    if (nbe === 1) afficheMathliveDans('info', 'texteinfo', textes.ilReste + textes.unEssai, { style: { color: '#7F007F' } })
    else afficheMathliveDans('info', 'texteinfo', textes.ilReste + nbe + textes.essais, { style: { color: '#7F007F' } })
  }

  function etablitNombreEtapesEtCoeff () {
    // Dans cette section le nombre d’étapes est dynamique et renvoyé par la figure mtg32
    sq.nbEtapes = valueOf('nbEtapes', true)
    if (!Number.isInteger(sq.nbEtapes) || sq.nbEtapes < 1) {
      j3pShowError('La figure doit contenir un calcul nbEtapes de valeur entière au moins égale à 1')
      sq.nbEtapes = 1
    }
    if (sq.nbEtapes === 1) {
      sq.coefEtape1 = 1
    } else {
      sq.coefEtape1 = parcours.donneesSection.coefEtape1
      if (sq.coefEtape1 === 0) {
        sq.coefEtape1 = 1 / sq.nbEtapes
        sq.coefEtapeFin = sq.coefEtape1
        sq.coefEtapesSuivantes = sq.coefEtape1
      }
      if (!Number.isFinite(sq.coefEtape1) || sq.coefEtape1 <= 0 || sq.coefEtape1 >= 1) {
        j3pShowError('Paramétrage invalide, coefEtape1 doit être un nombre dans ]0;1[')
        sq.coefEtape1 = 1 / sq.nbEtapes
      }
    }
    sq.coefEtapeFin = parcours.donneesSection.coefEtapeFin
    if (!Number.isFinite(sq.coefEtapeFin) || sq.coefEtapeFin < 0 || (sq.coefEtape1 + sq.coefEtapeFin) > 1) {
      // -1 est la valeur par défaut pour un réglage auto
      if (sq.coefEtapeFin !== -1) j3pShowError(`Paramétrage invalide, coefEtapeFin doit être un nombre dans [0 ; 1-coefEtape1] ${sq.coefEtapeFin} fourni`)
      sq.coefEtapesSuivantes = (1 - sq.coefEtape1) / (sq.nbEtapes - 1)
      sq.coefEtapeFin = sq.coefEtapesSuivantes
    } else if (sq.nbEtapes === 2) {
      sq.coefEtapeFin = 1 - sq.coefEtape1
    } else {
      sq.coefEtapesSuivantes = (1 - sq.coefEtape1 - sq.coefEtapeFin) / (sq.nbEtapes - 2)
      // on pourrait avoir sq.coefEtape1 + sq.coefEtapeFin === 1, on le signale
      if (sq.coefEtapesSuivantes < 1e-10) console.warn(`coefEtape1=${sq.coefEtape1} et coefEtapeFin=${sq.coefEtapeFin}, avec nbEtapes=${sq.nbEtapes} ça donne un coef nul pour le score des étapes intermédiaires`)
    }
  }

  // Attention : Les }{ on été remplacés par des }/{
  function traiteIntegrales (st) {
    const faux = { valide: false, st }
    let ind
    // Traitement différent pour mathlive
    while ((ind = st.indexOf('\\int_')) !== -1) {
      const ind1 = getFirstIndexParExcluded(st, '^', ind + 5)
      if (ind1 === -1) return -1
      const a = st.substring(ind + 5, ind1)
      // Si le ^ est suivi d’une parenthèse on recherche la parenthèse fermante associée
      // Sinon c’est que la borne supérieure est formée d’un seul caractère
      let ind2, ind3, indsuiv
      if (st.charAt(ind1 + 1) === '(') {
        ind2 = ind1 + 2
        ind3 = getIndexFermant(st, ind1 + 1)
        indsuiv = ind3 + 1
      } else {
        ind2 = ind1 + 1
        ind3 = ind1 + 2
        indsuiv = ind3
      }
      if (ind3 === 0 || indsuiv >= st.length) return faux
      const b = st.substring(ind2, ind3)
      // Attention : les {}{} ont pu donner des ()/().
      if (st.charAt(indsuiv) === '/') indsuiv++
      // On recherche maintenant le d d’intégration en sautant les éventuelles parenthèses
      let ind4 = getFirstIndexParExcluded(st, '\\differentialD', indsuiv)
      if (ind4 === -1) return faux
      const fonc = st.substring(indsuiv, ind4)
      ind4 += 14
      let ind5
      if (st.charAt(ind4) === '(') {
        ind5 = getIndexFermant(st, ind4)
        ind4++
      } else {
        ind5 = ind4 + 1
      }
      if (ind5 === -1) return faux
      const varfor = st.substring(ind4, ind5) // Le caractère représentant la variable d’intégration
      st = st.substring(0, ind) + 'integrale(' + fonc + ',' + varfor + ',' + a + ',' + b + ')' + st.substring(ind5)
    }
    return { valide: true, res: st }
  }

  /**
   * Fonction renvoyant un tableau formé des noms de variables utilisées par le calcul contenu dans ch
   * Si le tableau renvoyé a une longueur au moins égale à 2 on considérera que la primitive est incohérente
   * @param {string} ch
   * @return {[]}
   */
  function variablesUtilisees (ch) {
    function utiliseVariable (nomvar) {
      let res
      res = ch.search(new RegExp('\\(' + nomvar + '[\\+\\-\\*\\/\\^\\)]')) !== -1
      res = res || ch.search(new RegExp(nomvar + '[\\+\\-\\*\\/\\^\\)]')) !== -1
      res = res || (ch.lastIndexOf(nomvar) === ch.length - 1)
      res = res || ch === nomvar
      return res
    }

    const tab = []
    for (let i = 0; i < sq.variables.length; i++) {
      const car = sq.variables.charAt(i)
      if (utiliseVariable(car)) tab.push(car)
    }
    return tab
  }

  /**
   * Fonction remplaçant dans la chaîne st les indices (caractère _) par des appels de suites
   * ou de fonctions (pour MathGraph32)
   * @param st
   * @returns {string}
   */
  function traiteIndices (st) {
    // Le nom d’une suite peut être une lettre ou un lettre suivi du caractère '
    st = st.replace(/(\w)_(\w+)/g, '$1' + '(' + '$2' + ')')
    st = st.replace(/(\w')_(\w+)/g, '$1' + '(' + '$2' + ')')
    let ind
    while ((ind = st.indexOf('_(')) !== -1) {
      const indpf = getIndexFermant(st, ind + 1)
      st = st.substring(0, ind) + '(' + st.substring(ind + 2, indpf) + ')' + st.substring(indpf + 1)
    }
    return st
  }

  function traitePrimitives (st) {
    const faux = { valide: false, st }
    if (sq.variables === '') return { valide: true, res: st }
    let ind, tab
    while ((ind = st.indexOf('\\left[')) !== -1) {
      // On cherche le \right] correspondant
      const ind1 = getIndexFermant(st, ind + 5)
      if (ind1 === -1) return faux
      const fonc = st.substring(ind + 6, ind1 - 6)
      tab = variablesUtilisees(fonc)
      if (tab.length > 1) return faux
      const varfor = (tab.length === 0) ? sq.variables[0] : tab[0] // On traite le cas d’une fonction constante
      // Le crochet fermant doit être suivi d’un _
      if (st.charAt(ind1 + 1) !== '_') return faux
      const ind2 = getFirstIndexParExcluded(st, '^', ind1 + 2)
      if (ind2 === -1) return faux
      const a = st.substring(ind1 + 2, ind2)
      // Soit le caractère suivant n’est pas une parenthèse et c’est un seul caractère
      // qui est l’argument soit c’est le contenu de la parenthèse
      let ind3, ind4
      if (st.charAt(ind2 + 1) === '(') {
        ind3 = ind2 + 2
        ind4 = getIndexFermant(st, ind3)
      } else {
        ind3 = ind2 + 1
        ind4 = ind2 + 2
      }
      if (ind4 === -1 || ind4 > st.length) return faux
      const b = st.substring(ind3, ind4)
      st = st.substring(0, ind) + 'primitive(' + fonc + ',' + varfor + ',' + a + ',' + b + ')' + st.substring(ind4 + 1)
    }
    return { valide: true, res: st }
  }

  function traiteMathlive (ch) {
    // Mathlive ne met pas d’accolades pour les fractions dont le numérateur et le dénominateur sont des entiers
    ch = ch.replace(/\\frac(\d)(\d)/g, '\\frac{$1' + '}{' + '$2}')
    // Ni pour les racines carrées d’un entier
    ch = ch.replace(/\\sqrt(\d)/g, '\\sqrt{$1}')
    // Pour les puissances dont l’exposant est un chiffre, Mathlive ne met pas d’accolades
    ch = ch.replace(/\^([^{])/g, '^{$1}')
    ch = unLatexify(ch)
    // mathlive peut mettre des {} vides
    ch = ch.replace(/\\{\\}/g, '')
    ch = ch.replace(/\^(\d)(\d)/g, '^$1*$2') // On peut avoir par exemple 2^35^4 pour 2^3*5^4
    ch = ch.replace(/\\infty/g, '∞')
    if (ch === '+∞' || ch === '-∞') {
      let ret = 'infinity'
      if (ch === '-∞') ret = '-' + ret
      return { valide: true, res: ret, contientIntegOuPrim: false }
    }
    if (ch.indexOf('∞') !== -1) {
      return { valide: false, res: ch, contientIntegOuPrim: false }
    }
    let { res, valide } = traiteIntegrales(ch)
    if (valide) {
      const resul = traitePrimitives(res)
      res = resul.res
      valide = resul.valide
    }
    if (valide) ch = sq.mtgAppLecteur.addImplicitMult('mtg32svg', res)// Traitement des multiplications implicites
    ch = traiteIndices(ch)
    const contientIntegOuPrim = (ch.indexOf('integrale') !== -1) || (ch.indexOf('primitive') !== -1)
    return { valide, res: ch, contientIntegOuPrim }
  }

  function boutonfen () {
    j3pToggleFenetres('Calculatrice')
  }

  /**
   * Fonction renvoyant la valeur du calcul de nom nomCalc contenu dans la figure de l’exercice
   * @param {string} nomCalc
   * @param {boolean} bNoCase Si true on recherche le nom du calcul sans tenir compte de la casse
   * @return {number}
   */
  function valueOf (nomCalc, bNoCase) {
    return sq.mtgAppLecteur.valueOf('mtg32svg', nomCalc, bNoCase)
  }

  function giveFormula2 (nomCalc, formula) {
    sq.mtgAppLecteur.giveFormula2('mtg32svg', nomCalc, formula)
  }

  function calculate (brandom) {
    sq.mtgAppLecteur.calculate('mtg32svg', brandom)
  }

  function marqueEditeurPourErreur (id) {
    // Il faut regarder si l’erreur a été rencontrée dans un placeHolder ou dans un éditeur Mathlive classique
    let editor // Peut être aussi bien un éditeur de texte qu’un éditeur Mathlive
    if (id.includes('ph')) {
      const ind = id.indexOf('ph')
      const idmf = id.substring(0, ind) // L’id de l’éditeur MathLive contenant le placeHolder
      editor = j3pElement(idmf)
      sq.marked[idmf] = true
      editor.setPromptState(id, 'incorrect')
    } else {
      editor = j3pElement(id)
      sq.marked[id] = true
      editor.style.backgroundColor = '#FF9999'
    }
    // On donne à l’éditeur une fonction demarquePourError qui lui permettra de se démarquer quand
    // on utilise le clavier virtuel pour le modifier
    editor.demarqueErreur = () => {
      demarqueEditeurPourErreur(editor.id)
    }
  }

  function demarqueEditeurPourErreur (id) {
    // if ($.isEmptyObject(sq.marked)) return
    if (JSON.stringify(sq.marked) === '{}') return // On teste si l’objet est vide
    sq.marked[id] = false // Par précaution si appel via bouton recopier réponse trop tôt
    // On regarde si l’éditeur où a été trouvée l’erreur est un placeHolder
    const editor = j3pElement(id) // Peut être aussi bien un éditeur Mathlive qu’un éditeur de texte
    if (editor.classList.contains('PhBlock')) {
      editor.getPrompts().forEach((id) => {
        editor.setPromptState(id, 'undefined')
      })
    } else {
      editor.style.backgroundColor = 'white'
    }
  }

  function validationEditeurs () {
    function verifieEgal (ind, chEstEgal, repcalcul, id) {
      let valideEq = true
      if (sq[chEstEgal + ind]) {
        const indeq1 = repcalcul.indexOf('=')
        const indeq2 = repcalcul.indexOf('=', indeq1 + 1)
        if (indeq1 === -1 || (indeq1 !== -1 && indeq2 !== -1)) {
          valideEq = false
          sq.signeEgalManquant = true
          marqueEditeurPourErreur(id)
          listeIdEditeursAvecErreur.push(id)
        }
      }
      return valideEq
    }

    sq.signeEgalManquant = false
    const listeIdEditeursAvecErreur = []
    for (let ind = 1; ind <= sq.nbCalc; ind++) {
      const id = 'expressioninputmq' + ind
      const rep = getMathliveValue(id)
      if (rep === '') {
        marqueEditeurPourErreur(id)
        listeIdEditeursAvecErreur.push(id)
      } else {
        const resul = traiteMathlive(rep)
        const repcalcul = resul.res
        // On regarde si l’éditeur n° ind doit contenir une égalité ou non
        const valideEq = verifieEgal(ind, 'estEgalite', repcalcul, id)
        if (valideEq) {
          // Ligne suivante dernier paramètre à false car addImplicitmult a déjà été appelé par traiteMathQuill
          // et si une réponse est +infini ou -infini elle ne serait pas acceptée
          const valide = resul.valide && sq.mtgAppLecteur.syntaxValidation('mtg32svg', 'rep' + String(sq.etape) + ind, repcalcul, false)
          if (!valide) {
            marqueEditeurPourErreur(id)
            listeIdEditeursAvecErreur.push(id)
          }
        }
      }
    }
    let indEditable = 0
    let indPhBlock = 0
    const span = j3pElement('expression')
    span.querySelectorAll('.PhBlock').forEach((mf) => {
      indPhBlock++
      for (let i = 0; i < mf.getPrompts().length; i++) {
        indEditable++
        const idmf = 'expressionPhBlock' + indPhBlock
        const id = idmf + 'ph' + indEditable
        const rep = getMathliveValue(idmf, { placeholderId: id })
        if (rep === '') {
          marqueEditeurPourErreur(id)
          listeIdEditeursAvecErreur.push(id)
        } else {
          const resul = traiteMathlive(rep)
          const repcalcul = resul.res
          const valideEq = verifieEgal(indEditable, 'estEgaliteEditable', repcalcul, id)
          if (valideEq) {
            const resul = traiteMathlive(rep)
            const repcalcul = resul.res
            // Ligne suivante dernier paramètre à false car addImplicitmult a déjà été appelé par traiteMathQuill
            // et si une réponse est +infini ou -infini elle ne serait pas acceptée
            const valide = resul.valide && sq.mtgAppLecteur.syntaxValidation('mtg32svg', 'repEditable' + String(sq.etape) + indEditable, repcalcul, false)
            if (!valide) {
              marqueEditeurPourErreur(id)
              listeIdEditeursAvecErreur.push(id)
            }
          }
        }
      }
    })
    for (let ind = 1; ind <= sq.nbTextEdit; ind++) {
      const id = 'expressioninput' + ind
      const rep = j3pValeurde(id)
      if (rep === '') {
        marqueEditeurPourErreur(id)
        listeIdEditeursAvecErreur.push(id)
      }
    }

    if (listeIdEditeursAvecErreur.length) {
      setFocusById(listeIdEditeursAvecErreur[0])
      return false
    }
    for (let ind = 1; ind <= sq.nbList; ind++) {
      const resliste = sq.listesEnLatex[sq.etape - 1] ? sq['listeDeroulante' + ind].getReponseIndex() : j3pElement('expression' + 'liste' + ind).selectedIndex
      if (resliste === 0) return false
    }
    return true
  }

  /**
   * Fonction renvoyant le nombre d’éditeurs contenus dans la chaîne ch et dont le type est contenu
   * dans la chaîne type
   * @param {string} type chaine de caractères qui peut être la chaîne 'edit' pour un éditeur MathQuill,
   * 'list' pour une liste déroulante et 'string' pour une chaîne de caractères.
   * Si, par exemple, il y a trois éditeurs MathQuill la chaîne doit contenir edit1, edit2 et edit3.
   * @param {string} ch
   * @returns {string}
   */
  function nbEdit (type, ch) {
    let res = 0
    let i = 1
    while (ch.indexOf(type + i) !== -1) {
      res++
      i++
    }
    return res
  }

  /**
   * Fonction renvoyant le nombre de `\editable{} contenus dans la chaîne ch
   * @param ch
   * @return {string|number}
   */
  function nbEditable (ch) {
    let res = 0
    let i = ch.indexOf('\\editable{')
    if (i === -1) return ''
    while (i !== -1) {
      res++
      ch = ch.substring(i + 10)
      i = ch.indexOf('\\editable{')
    }
    return res
  }

  /**
   * Fonction filtrant dans la chaîne ch les éléments LaTeX qui ne sont pas générés par MathQuill
   * comme les \displaystyle et renvoyant la chaîne traitée
   * @param ch
   */
  function filtrePourMathquill (ch) {
    return ch.replace(/\n/g, '').replace(/\\displaystyle/g, '').replace(/\\,/g, ' ').replace(/\\left\[/g, '[').replace(/\\right]/g, ']')
  }

  function extraitDonneesListe (indListe) {
    const list = sq.mtgAppLecteur.getList('mtg32svg')
    // On cherche dans la liste le LaTeX qui a pour tag 'list' suivi du chiffre indListe
    const latex = list.getByTag('list' + String(sq.etape) + indListe)
    if (latex === null || latex.className !== 'CLatex') return []
    // On met dans le tableau qu’on va renvoyer le contenu des \text{} successifs rencontrés dans le LaTeX
    const chaineLatex = filtrePourMathquill(latex.chaineLatex)
    const res = ['...']
    let i = chaineLatex.indexOf('\\text{')
    while (i !== -1) {
      const idaf = getIndexFermant(chaineLatex, i + 5)
      if (idaf === -1) return res
      else res.push(chaineLatex.substring(i + 6, idaf))
      i = chaineLatex.indexOf('\\text{', idaf + 1)
    }
    // Amélioration : Si la figure contient un calcul nommé listaleat suivi du numéro indListe et qui vaut 1,
    // alors on mélange les items de la liste au hasard.
    // On crée un tableau pour connaître le vrai numéro de l’item choisi dans la liste
    const tabnum = []
    const n = res.length - 1
    for (let i = 0; i < n; i++) tabnum.push(i + 1)
    let tabres
    let tabresitem
    if (valueOf('listaleat' + sq.etape + String(indListe), true) === 1) { // On mélange les items au hasard
      tabresitem = ['...']
      tabres = []
      for (let i = 0; i < n; i++) {
        const tir = Math.floor(Math.random() * tabnum.length)
        const num = tabnum[tir]
        tabres.push(num)
        tabnum.splice(tir, 1)
        tabresitem.push(res[num])
      }
    } else {
      tabres = tabnum
      tabresitem = res
    }
    sq['listaleat' + indListe] = tabres
    return { texte: tabresitem }
  }

  /**
   * Fonction extrayant d’un affichage LaTeX de la figure les lignes de texte et les transformant
   * en une chaîne affichable par MathQuill
   * @param {string} tagLatex tag de l’affichage LaTeX de la figure
   * @return {string}
   */
  function extraitDeLatexPourMathlive (tagLatex) {
    const list = sq.mtgAppLecteur.getList('mtg32svg')
    const latex = list.getByTag(tagLatex)
    if (latex === null || latex.className !== 'CLatex') return ''
    return cleanLatexForMl(latex.chaineLatex)
  }

  function extraitSolutionsEditeursTexte () {
    function affErr () {
      console.error('Les données pour les résultats de champ texte sont incorrectes')
    }

    const list = sq.mtgAppLecteur.getList('mtg32svg')
    const latex = list.getByTag('restext' + sq.etape)
    if (latex === null || latex.className !== 'CLatex') return []
    // Il faut se débarrasser de ce qui n’est pas géré par MathQuill
    let ch = latex.chaineLatex.replace(/\n/g, '').replace(/\\displaystyle/g, ' ').replace(/\\,/g, ' ')
    ch = ch.replace(/\\left\[/g, '[').replace(/\\right]/g, ']')
    // On remplace les . décimaux par des virgules
    ch = ch.replace(/(\d)\.(\d)/g, '$1,$2')
    const res = []
    let i = ch.indexOf('\\text{')
    if (i === -1) {
      affErr()
      return res
    } else {
      while (i !== -1) {
        const idaf = getIndexFermant(ch, i + 5)
        if (idaf !== -1) {
          // Les résultats possibles pour les différents éditeurs sont séparés par des **
          // et s’il y a plusieurs réponses possibles pour un même éditeur elles sont séparées par des //
          const tab = []
          const st = ch.substring(i + 6, idaf)
          const soustab = st.split('**')
          if (soustab.length !== sq.nbTextEdit) {
            affErr()
            return []
          }
          soustab.forEach(function (el) {
            tab.push(el.split('//'))
          })
          res.push(tab)
        }
        i = ch.indexOf('\\text{', idaf + 1)
      }
    }
    return res
  }

  function montreEditeurs (bVisible) {
    if (bVisible) $('#info').css('display', 'block')
    $('#editeur').css('display', bVisible ? 'block' : 'none')
    if (sq.boutonsMathQuill) $('#boutonsmathquill').css('display', bVisible ? 'block' : 'none')
  }

  function videEditeurs () {
    const el = j3pElement('expression')
    el.querySelectorAll('math-field').forEach(mf => {
      if (!mf.id.includes('PhBlock')) mf.value = ''
    })
    el.querySelectorAll('.PhBlock').forEach((mf) => {
      mf.getPrompts().forEach((id) => {
        mf.setPromptValue(id, '', 'latex')
      })
      // mf.stripPromptContent() // Quand on vide l’apparence du placeholder n’est plus visible
      const idFirstEdit = mf.getPrompts()[0]
      mf.setPromptValue(idFirstEdit, '')
    })
    for (let i = 1; i <= sq.nbList; i++) {
      if (sq.listesLatex) {
        sq['listeDeroulante' + i].reset()
      } else {
        j3pElement('expressionliste' + i).selectedIndex = 0
      }
    }
    for (let i = 1; i <= sq.nbTextEdit; i++) {
      $('#expressioninput' + i).val('')
    }
    // On donne le focus au premier éditeur MathQuill
    setFocusToFirstInput('expression')
  }

  function creeEditeurs () {
    let k, i, edit
    j3pEmpty('editeur')
    // Dans cette section, le formulaire est fourni par la figure mtg32 à chaque étape
    // par exemple dans le LaTeX de tag formulaire1 pour l’étape 1
    sq.formulaire = extraitDeLatexPourMathlive('formulaire' + sq.etape)
    sq.nbCalc = nbEdit('edit', sq.formulaire)
    sq.nbList = nbEdit('list', sq.formulaire)
    sq.nbTextEdit = nbEdit('edittext', sq.formulaire)
    sq.solEditeursTextes = extraitSolutionsEditeursTexte()
    sq.nbEditable = nbEditable(sq.formulaire)
    sq.marked = {}
    let formulaire = sq.formulaire
    for (k = 1; k <= sq.nbCalc; k++) {
      formulaire = formulaire.replace('edit' + k, '&' + k + '&')
      // On interroge la figure pour savoir si l’éditeur k de l’étape correspondante
      // doit contenir une egalité (calcul nommé estEgalité suivi du n° d’étape suivi de l’indice de l’éditeur)
      sq['estEgalite' + k] = valueOf('estEgalite' + sq.etape + String(k), true) === 1
    }
    for (k = 1; k <= sq.nbList; k++) formulaire = formulaire.replace('list' + k, '#' + k + '#')
    for (k = 1; k <= sq.nbTextEdit; k++) formulaire = formulaire.replace('edittext' + k, '@' + k + '@')
    const obj = {}
    for (k = 1; k <= sq.nbCalc; k++) obj['inputmq' + k] = {}
    for (k = 1; k <= sq.nbList; k++) {
      const donnees = extraitDonneesListe(k)
      sq['tabItemsLatex' + k] = donnees.texte // Mémorisé pour afficheReponse
      obj['liste' + k] = donnees
    }
    for (k = 1; k <= sq.nbTextEdit; k++) obj['input' + k] = { texte: '', dynamique: true }
    obj.charset = sq['charset' + sq.etape]
    obj.listeBoutons = sq.listeBoutons
    obj.charsetText = sq['charsetText' + sq.etape]
    afficheMathliveDans('editeur', 'expression', formulaire, obj) // sq.formulaire contient ou plusieurs éditeurs MathQuill
    // On donne un margin left et right aux listes déroulantes et aus éditeurs
    for (k = 1; k <= sq.nbList; k++) {
      $('#expressionliste' + k).css('margin-left', '5px').css('margin-right', '5px')
    }
    if (sq.listesLatex) {
      for (k = 1; k <= sq.nbList; k++) {
        // $('#expressionliste' + k).css('margin-left', '5px').css('margin-right', '5px')
        const select = j3pElement('expressionliste' + k)
        const pn = select.parentNode.parentNode
        const div = document.createElement('div')
        $(div).attr('id', 'expressiondivliste' + k)
        j3pElement('expression').replaceChild(div, pn)
        const tab = sq['tabItemsLatex' + k]
        // Rectification suite aux modifs de liste Déroulante : S’il y a un $ en début et en fin on laisse
        // S’il n’y en a pas et que sq.listesLaTeX est à true on les rajoutes
        for (i = 1; i < tab.length; i++) {
          let ch = tab[i].trim()
          const debdol = ch.charAt(0) === '$'
          const findol = ch.charAt(ch.length - 1) === '$'
          if (!debdol || !findol) {
            ch = (debdol ? '' : '$') + ch + (findol ? '' : '$')
            tab[i] = ch
          }
        }
        sq['listeDeroulante' + k] = ListeDeroulante.create('expressiondivliste' + k, tab)
        $(div).css('display', 'inline')
      }
    }
    for (k = 1; k <= sq.nbCalc; k++) {
      $('#expressioninputmq' + k).css('margin-left', '5px').css('margin-right', '5px')
    }
    // On affecte un indice à chaque éditeur ainsi qu’un écouteur de focus
    for (k = 1; k <= sq.nbCalc; k++) {
      edit = j3pElement('expressioninputmq' + k)
      edit.indice = 'expressioninputmq' + k
      if (sq['charset' + sq.etape] !== '') {
        mathliveRestrict('expressioninputmq' + k, sq['charset' + sq.etape])
      }
      const self = this
      $('#expressioninputmq' + k).focusin(function () {
        ecoute.call(this, self)
      })

      edit.onkeyup = function (ev) {
        onkeyup.call(edit)
      }
    }
    // On met les restrictions et les listeners sur chaque bloc contenant des editable (palceholders)
    j3pElement('expression').querySelectorAll('.PhBlock').forEach((mf) => {
      const id = mf.id
      if (sq['charset' + sq.etape] !== '') {
        mathliveRestrict(id, sq['charset' + sq.etape])
      }
      mf.addEventListener('focusin', () => {
        ecoute.call(mf)
      })
      mf.addEventListener('keyup', () => {
        onkeyup.call(mf)
      })
    })
    for (k = 1; k <= sq.nbTextEdit; k++) {
      edit = j3pElement('expressioninput' + k)
      edit.indice = 'expressioninput' + k
      const nbcar = valueOf('nbcar' + String(sq.etape) + k, true)
      if (nbcar !== -1) $(edit).attr('maxlength', nbcar)
      if (sq['charsetText' + sq.etape] !== '') j3pRestriction('expressioninput' + k, sq['charsetText' + sq.etape])

      $('#expressioninput' + k).focusin(function () {
        ecoute.call(this)
      })

      edit.onkeyup = function (ev) {
        onkeyup.call(this, ev, parcours)
      }
    }
    setFocusToFirstInput('expression')
  }

  function afficheReponse (bilan, depasse) {
    let coul
    const num = sq.numEssai
    const idrep = 'exp' + sq.etape + String(num)
    // let ch = (num > 2) ? '<br>' : ''
    // Avec MathliveAffiche il ne faut pas envoyer une chaîne commençant par un <br> car le premier affichage
    // se fait inline (celui du <br> mais le suivant est dans un <p> ce qui provoque un retour à la ligne)
    // et donc on a par exemple Réponse exacte : suivi d’un retour à la ligne suivi de la réponse
    // alors qu’on veut que tout soit sur une même ligne
    if (num >= 2) afficheMathliveDans('formules', idrep + 'br', '<br>')
    let ch

    if (bilan === 'exact') {
      ch = textes.repExacte
      coul = parcours.styles.cbien
    } else {
      if (bilan === 'faux') {
        ch = textes.repFausse
        coul = parcours.styles.cfaux
      } else { // Exact mais pas fini
        ch = textes.repExactPasFini
        if (depasse) coul = parcours.styles.cfaux
        else coul = parcours.styles.colorCorrection
      }
    }
    afficheMathliveDans('formules', idrep + 'deb', ch, {
      style: {
        color: coul
      }
    })
    ch = sq.formulaire
    for (let i = 1; i <= sq.nbCalc; i++) {
      const st = '\\textcolor{' + (sq.repResolu[i - 1] ? parcours.styles.cbien : (sq.repExact[i - 1] ? '#0000FF' : parcours.styles.cfaux)) + '}{'
      ch = ch.replace('edit' + i, '$' + st + sq.rep[i - 1] + '}$')
    }
    // Il se peut qu’il y ait une matrice ou plusieurs dont les termes soient des editable.
    // Dans ce cas, lors de la création des éditeurs, les indices des editable ont été créés
    // par ordre croissant colonne par colonne mais lors du replacement il faut donner le contenu des
    // cellules ligne par ligne
    let indmqef = 1
    const hasMatrixEditable = ch.includes('\\begin{matrix}') && ch.includes('\\editable')
    if (hasMatrixEditable) ch = transposeMatrices(ch)
    let indexEditable = ch.indexOf('\\editable{')
    while (indexEditable !== -1) {
      const st = '\\textcolor{' + (sq.repResoluEditable[indmqef - 1] ? parcours.styles.cbien : (sq.repExactEditable[indmqef - 1] ? '#0000FF' : parcours.styles.cfaux)) + '}{'
      ch = ch.replace('\\editable', st + sq.repEditable[indmqef - 1] + '}') // On laisse des {} vides derrière ce qui ne change rien à l’affichage
      indmqef += 1
      indexEditable = ch.indexOf('\\editable{', indexEditable + 1)
    }
    if (hasMatrixEditable) ch = transposeMatrices(ch)
    for (let i = 1; i <= sq.nbList; i++) {
      const st = '\\textcolor{' + (sq.repResoluListe[i - 1] ? parcours.styles.cbien : parcours.styles.cfaux) + '}{'
      let chrep = sq['tabItemsLatex' + i][sq['replist' + i]]
      if (sq.listesLatex) {
        // chrep = chrep.substring(1, chrep.length - 1)
        chrep = chrep.replaceAll('$', '') // Modifié pour Mathlive car il forunit des espaces avant le $ et après
        ch = ch.replace('list' + i, '$' + st + chrep + '}$')
      } else {
        ch = ch.replace('list' + i, '$' + st + '\\text{' + chrep + '}}$')
      }
    }
    for (let i = 1; i <= sq.nbTextEdit; i++) {
      const st = '\\textcolor{' + (sq.repResoluText ? parcours.styles.cbien : parcours.styles.cfaux) + '}{'
      ch = ch.replace('edittext' + i, '$' + st + '\\text{' + sq.repText[i - 1] + '}}$')
    }
    afficheMathliveDans('formules', idrep, ch, sq.parametres)
    resetKeyboardPosition()
  }

  function ecoute () {
    if (sq.boutonsMathQuill && (this.id.indexOf('inputmq') !== -1 || this.id.indexOf('PhBlock') !== -1)) {
      j3pEmpty('boutonsmathquill')
      j3pPaletteMathquill('boutonsmathquill', this.id, { liste: sq.listeBoutons, nomdiv: 'palette' })
      // Ligne suivante pour un bon alignement
      $('#palette').css('display', 'inline-block')
    }
  }

  function validation () {
    let rep, ind, chcalcul, exact, resolu, res, ex, tab, j
    sq.rep = []
    sq.repExact = []
    sq.repEditable = []
    sq.repText = []
    sq.repExactEditable = []
    sq.repResolu = []
    sq.repExactEditable = []
    sq.repResoluEditable = []
    sq.repResoluListe = []
    sq.repResoluText = true

    if (sq.numEssai >= 1) {
      j3pElement('boutonrecopier').style.display = 'block'
      for (ind = 1; ind <= sq.nbCalc; ind++) {
        rep = getMathliveValue('expressioninputmq' + ind)
        sq.rep.push(rep)
        chcalcul = traiteMathlive(rep).res
        giveFormula2('rep' + String(sq.etape) + ind, chcalcul)
      }
      // Traitement des editable (placeholders)
      let indPhBlock = 0
      let indEditable = 0
      j3pElement('expression').querySelectorAll('.PhBlock').forEach((mf) => {
        indPhBlock++
        for (let i = 0; i < mf.getPrompts().length; i++) {
          indEditable++
          const idmf = 'expressionPhBlock' + indPhBlock
          const id = idmf + 'ph' + indEditable
          const rep = getMathliveValue(idmf, { placeholderId: id })
          sq.repEditable.push(rep)
          chcalcul = traiteMathlive(rep).res
          giveFormula2('repEditable' + String(sq.etape) + indEditable, chcalcul)
        }
      })
      calculate(false)
      exact = true
      resolu = true
      let auMoinsUnExact = false
      // On regarde d’abord les champs MathQuill s’il y en a
      for (ind = 1; ind <= sq.nbCalc; ind++) {
        res = valueOf('resolu' + String(sq.etape) + ind, true)
        ex = valueOf('exact' + String(sq.etape) + ind, true)
        resolu = resolu && (res === 1)
        exact = exact && ((res === 1) || (ex === 1))
        sq.repResolu.push(res === 1)
        sq.repExact.push((res === 1) || (ex === 1))
        auMoinsUnExact = auMoinsUnExact || (ex === 1)
      }
      // On regarde ensuite les champs Editable s’il y en a
      for (ind = 1; ind <= sq.nbEditable; ind++) {
        res = valueOf('resoluEditable' + String(sq.etape) + ind, true)
        ex = valueOf('exactEditable' + String(sq.etape) + ind, true)
        resolu = resolu && (res === 1)
        exact = exact && ((res === 1) || (ex === 1))
        sq.repResoluEditable.push(res === 1)
        sq.repExactEditable.push((res === 1) || (ex === 1))
        auMoinsUnExact = auMoinsUnExact || (ex === 1)
      }
      let listesOK = true
      for (ind = 1; ind <= sq.nbList; ind++) {
        const indresliste = sq.listesEnLatex[sq.etape - 1] ? sq['listeDeroulante' + ind].getReponseIndex() : j3pElement('expression' + 'liste' + ind).selectedIndex
        // Les éléments de la liste peuvent avoir été mélangés au hasard
        // On gère les choix multiples ou les choix uniques dans la liste.
        // Si les choix multiples sont acceptés, on donne au calcul repListe suivi du n° d’étape et du n° de liste
        // la valeur choisie dans la liste et on interroge le calcul exactList suivi du n° d’étape et du n° de liste
        // qui doit valeur 1 si la réponse est une des réponses acceptées et 0 sinon.
        // Si le choix est unique on interroge la calcul resliste suivi du n° d’étape et du n° de liste qui
        // contient l’indice de la bonne réponse
        const resliste = sq['listaleat' + ind][indresliste - 1]
        let reslisteexact
        if (valueOf('repList' + String(sq.etape) + ind, true) !== -1) {
          giveFormula2('repList' + String(sq.etape) + ind, String(resliste))
          calculate(false)
          reslisteexact = valueOf('exactList' + String(sq.etape) + ind, true) === 1
        } else {
          reslisteexact = resliste === valueOf('reslist' + String(sq.etape) + ind, true)
        }
        listesOK = listesOK && reslisteexact
        // On mémorise le choix de la sélection des listes déroulantes pour pouvoir recopier
        // la réponse précédente
        sq['replist' + ind] = indresliste
        sq.repResoluListe.push(reslisteexact)
      }
      resolu = resolu && listesOK
      // On vérifie maintenant la réponse contenue dans les champs de texte
      for (ind = 1; ind <= sq.nbTextEdit; ind++) {
        rep = j3pValeurde('expressioninput' + ind)
        sq.repText.push(rep)
      }
      for (let i = 0; i < sq.solEditeursTextes.length; i++) {
        tab = sq.solEditeursTextes[i]
        res = true
        for (j = 0; j < tab.length && res; j++) { // tab.length doit être égal à sq.nbTextEdit
          const chcasse = sq.textesSensiblesCasse ? sq.repText[j] : sq.repText[j].toLowerCase()
          res = res && tab[j].includes(chcasse)
        }
        if (res) {
          sq.repResoluText = true
          break
        } else {
          sq.repResoluText = false
        }
      }
      sq.resolu = resolu && sq.repResoluText
      sq.exact = exact && auMoinsUnExact && listesOK && sq.repResoluText
    }
  }

  function recopierReponse () {
    for (let ind = sq.nbCalc; ind > 0; ind--) {
      const ided = 'expressioninputmq' + ind
      // demarqueEditeurPourErreur(ided)
      // $('#' + ided).mathquill('latex', sq.rep[ind - 1]).blur()
      const mf = j3pElement(ided)
      mf.value = sq.rep[ind - 1]
      if (ind === 0) mf.focus()
    }
    let indiceEditable = 0
    let firstEdit = null
    let idFirstEdit = ''
    let contentFirstEdit = ''
    j3pElement('expression').querySelectorAll('.PhBlock').forEach((mf, index) => {
      // On n’utilise pas mf.getPromps() pour parcourir ses placeholders à cause du problème des matrices
      // Pour garder la compatbilité avec mathquill les indices vont croissants par colonne et pas par ligne
      const indPhBlock = index + 1
      const nbPrompts = mf.getPrompts().length
      for (let i = 0; i < nbPrompts; i++) {
        const content = sq.repEditable[indiceEditable]
        indiceEditable++
        const id = 'expressionPhBlock' + indPhBlock + 'ph' + indiceEditable
        mf.setPromptValue(id, content)
        demarqueEditeurPourErreur(mf.id)
        if (index === 0 && indiceEditable === 1) {
          firstEdit = mf
          idFirstEdit = id
          contentFirstEdit = content
        }
      }
      // On récrit dans le premier placeHolder
      firstEdit.setPromptValue(idFirstEdit, contentFirstEdit)
    })
    for (let ind = sq.nbTextEdit; ind > 0; ind--) {
      const ided = 'expressioninput' + ind
      demarqueEditeurPourErreur(ided)
      $('#' + ided).val(sq.repText[ind - 1]).blur()
    }
    for (let ind = 1; ind <= sq.nbList; ind++) {
      if (sq.listesLatex) {
        sq['listeDeroulante' + ind].select(sq['replist' + ind])
      } else {
        j3pElement('expressionliste' + ind).selectedIndex = sq['replist' + ind]
      }
    }
    setFocusToFirstInput('expression')
  }

  function etapeSuivante () {
    j3pElement('boutonrecopier').style.display = 'none'
    sq.etape++
    // On laisse la possibilité à l’utilisateur d’exécuter une macro de la figure au début de chaque étape
    sq.mtgAppLecteur.executeMacro('mtg32svg', 'macroEtape' + sq.etape)
    j3pEmpty('editeur')
    j3pEmpty('enonce')
    j3pEmpty('enonceSuite')
    j3pEmpty('info')
    const st = extraitDeLatexPourMathlive('enonce' + sq.etape)
    afficheMathliveDans('enonceSuite', 'texte', st)
    sq.numEssai = 1
    afficheNombreEssaisRestants()
    creeEditeurs()
  }

  /**
   * Fonction affichant la solution s’il existe un affichage LaTeX de tag solution
   * @param{boolean} bexact true si l’élève a entré la bonne réponse, false sinon
   */
  function afficheSolution (bexact) {
    hideCurrentVirtualKeyboard()
    sq.mtgAppLecteur.executeMacro('mtg32svg', 'solution')
    let ch, j, car, k, code
    j3pEmpty('editeur')
    j3pEmpty('enonce')
    j3pEmpty('enonceSuite')
    j3pEmpty('info')
    $('#divSolution').css('display', 'block').html('')
    ch = extraitDeLatexPourMathlive('solution')
    if (ch !== '') {
      const styles = parcours.styles
      afficheMathliveDans('divSolution', 'solution', ch, { style: { color: bexact ? styles.cbien : styles.colorCorrection } })
    }
    if (sq.figSol !== '') {
      const mtg32App = sq.mtgAppLecteur
      $('#divExplications').css('display', 'block').html('')
      $('#mtg32svgsol').css('display', 'block')
      mtg32App.removeDoc('mtg32svgsol')
      mtg32App.addDoc('mtg32svgsol', sq.figSol)
      // On remplace dans la figure de solution les paramètres par les valeurs correspondant dans la figure de texte
      ch = 'abcdefghjklmnpqr'
      for (j = 0; j < ch.length; j++) {
        car = ch.charAt(j)
        if (sq.param.indexOf(car) !== -1) {
          const par = valueOf(car) // La valeur du calcul dans la figure principale
          // On donne cette même valeur au calcul de même nom dans la figure de correction
          // Attntion ici n epas appelelr giveFormula2 tout court
          mtg32App.giveFormula2('mtg32svgsol', car, par)
        }
      }
      mtg32App.calculate('mtg32svgsol', false)
      const param = {}
      for (k = 0; k < sq.nbLatexSol; k++) {
        code = mtg32App.getLatexCode('mtg32svgsol', k)
        param[ch.charAt(k)] = code
      }
      afficheMathliveDans('divExplications', 'explications', sq.explicationSol, param)
      mtg32App.display('mtg32svgsol')
    }
  }

  function cacheSolution () {
    $('#divSolution').css('display', 'none').html('')
    if (sq.figSol !== '') {
      $('#divExplications').css('display', 'none').html('')
      $('#mtg32svgsol').css('display', 'none')
    }
  }

  function initMtg () {
    setMathliveCss()
    const mtgOptions = {
      // player only, sans figure initiale ni svg, on gère ça dans initMtg et modif_fig
      loadCoreWithMathJax: true
    }
    getMtgApp('mtg32svg', {}, mtgOptions).then((mtgAppLecteur) => {
      sq.mtgAppLecteur = mtgAppLecteur
      sq.marked = {}
      let par, car, i, j, k, nbrep, ar, tir, nb
      sq.mtgAppLecteur.removeAllDoc()
      sq.mtgAppLecteur.addDoc('mtg32svg', sq.fig, true)
      sq.mtgAppLecteur.calculateFirstTime('mtg32svg', false)
      const ch = 'abcdefghjklmnpqr'
      const nbvar = sq.mtgAppLecteur.valueOf('mtg32svg', 'nbvar', true)
      if (nbvar !== -1) {
        nbrep = parcours.donneesSection.nbrepetitions
        sq.aleat = true
        sq.nbParamAleat = nbvar
        for (i = 1; i <= nbvar; i++) {
          const nbcas = sq.mtgAppLecteur.valueOf('mtg32svg', 'nbcas' + i, true)
          nb = Math.max(nbrep, nbcas)
          ar = []
          for (j = 0; j < nb; j++) ar.push(j % nbcas)
          sq['tab' + i] = []
          for (k = 0; k < nbrep; k++) {
            tir = Math.floor(Math.random() * ar.length)
            sq['tab' + i].push(ar[tir])
            ar.splice(tir, 1)
          }
          giveFormula2('r' + i, sq['tab' + i][0])
        }
      } else { sq.aleat = false }

      if (sq.param !== undefined) {
        for (i = 0; i < ch.length; i++) {
          car = ch.charAt(i)
          if (sq.param.indexOf(car) !== -1) {
            par = parcours.donneesSection[car]
            if (par !== 'random') giveFormula2(car, par)
          }
        }
      }

      mtgAppLecteur.calculate('mtg32svg', true)
      etablitNombreEtapesEtCoeff()
      mtgAppLecteur.display('mtg32svg')
      mtgAppLecteur.setActive('mtg32svg', false)

      // j3pEmpty(enonce)
      afficheMathliveDans('enonce', 'texte', extraitDeLatexPourMathlive('enonce1'))
      creeEditeurs()
      if (!sq.boutonsMathQuill) {
        j3pDetruit('boutonsmathquill')
      }
      afficheNombreEssaisRestants()
      parcours.finEnonce()
    }).catch(j3pShowError)
  }

  function initDom () {
    renderMathInDocument({ renderAccessibleContent: '' })
    if (sq.titre) parcours.afficheTitre(sq.titre)

    j3pDiv(parcours.zones.MG, { id: 'conteneur', style: parcours.styles.etendre('toutpetit.enonce', { padding: '6px' }) })
    j3pDiv('conteneur', 'enonce', '')
    const formules = j3pAddElt('conteneur', 'div', { id: 'formules', style: parcours.styles.petit.enonce }) // Contient le formules entrées par l’élève
    formules.style.fontSize = sq.taillePoliceReponses + 'px'
    // formules contient les formules entrées par l’élève
    j3pDiv('conteneur', 'enonceSuite', '')

    if (sq.bigSize) $('#formules').css('font-size', '26px')
    j3pDiv('conteneur', 'info', '')
    j3pDiv('conteneur', 'conteneurbouton', '')
    j3pAjouteBouton('conteneurbouton', 'boutonrecopier', 'MepBoutonsRepere', 'Recopier réponse précédente',
      recopierReponse)
    j3pElement('boutonrecopier').style.display = 'none'
    const editeur = j3pAddElt('conteneur', 'div', { id: 'editeur', style: { paddingTop: '10px' } }) // Le div qui contiendra la chaine avec le ou les éditeurs MathQuill
    editeur.style.fontSize = sq.taillePoliceFormulaire + 'px'
    $('#editeur').css('padding-top', '10px') // Pour laisser un peu de place au-dessus de l’éditeur
    j3pDiv('conteneur', 'boutonsmathquill', '')
    $('#boutonsmathquill').css('display', 'inline-block').css('padding-top', '10px') // Pour laisser un peu de place au-dessus des boutons
    const divsol = j3pAddElt('conteneur', 'div', { id: 'divSolution', style: { display: 'none' } })
    divsol.style.fontSize = sq.taillePoliceSolution + 'px'
    j3pDiv('conteneur', 'divmtg32', '')
    j3pCreeSVG('divmtg32', {
      id: 'mtg32svg',
      width: sq.width,
      height: sq.height
    })
    if (sq.figSol !== '') {
      j3pDiv('conteneur', 'divExplications', '')
      $('#divExplications').css('display', 'none')
      j3pDiv('conteneur', 'divFigSol', '')
      j3pCreeSVG('divFigSol', {
        id: 'mtg32svgsol',
        width: sq.widthSol,
        height: sq.heightSol
      })
      $('#mtg32svgsol').css('display', 'none')
    }
    const style = parcours.styles.petit.correction
    style.marginTop = '1.5em'
    style.marginLeft = '0.5em'
    j3pAddElt(parcours.zonesElts.MD, 'div', '', { id: 'correction', style })
    initMtg()
  }

  switch (this.etat) {
    case 'enonce':
      // code exécuté au lancement de la section
      sq.scoreInit = parcours.score
      if (this.debutDeLaSection) {
        parcours.construitStructurePage('presentation1bis')
        // compteur.
        sq.numEssai = 1
        sq.etape = 1
        for (let i = 1; i <= 8; i++) {
          sq['nbEssais' + i] = parseInt(parcours.donneesSection['nbEssais' + i])
        }
        sq.nbexp = 0
        sq.reponse = -1
        if (parcours.donneesSection.calculatrice) {
          j3pDiv('MepMD', 'emplacecalc', '')
          this.fenetresjq = [{ name: 'Calculatrice', title: 'Une calculatrice', left: 600, top: 10 }]
          j3pCreeFenetres(this)
          j3pAjouteBouton('emplacecalc', 'BCalculatrice', 'MepBoutons', 'Calculatrice', boutonfen)
        }
        // faut gérer sous-dossier ou pas…
        const sep = this.donneesSection.ex.includes('/') ? '-' : '/'
        j3pImporteAnnexe('squelettes-mtg32' + sep + this.donneesSection.ex + '.js').then(function (datasSection) {
          // Attention, le this devient window...
          sq.fig = datasSection.txt
          sq.width = Number(datasSection.width ?? 800)
          sq.height = Number(datasSection.height ?? 600)
          sq.listesLatex = Boolean(datasSection.listesLatex ?? false)
          // Gestion de la liste des boutons autorisés
          const liste = []
          // Boutons qui sont inclus par défaut
          const tab1 = ['fraction', 'puissance']
          ;['btnFrac', 'btnPuis'].forEach((p, i) => {
            const b = Boolean(datasSection[p] ?? true)
            if (b) liste.push(tab1[i])
          })
          // Boutons qui sont exclus par défaut
          const tab2 = ['racine', 'exp', 'ln', 'pi', 'sin', 'cos', 'tan', 'abs', 'conj', 'integ', 'differential', 'prim', 'inf']
          ;['btnRac', 'btnExp', 'btnLn', 'btnPi', 'btnSin', 'btnCos',
            'btnTan', 'btnAbs', 'btnConj', 'btnInteg', 'btnDif', 'btnPrim', 'btnInf'].forEach((p, i) => {
            const b = Boolean(datasSection[p] ?? false)
            if (b) liste.push(tab2[i])
          })
          sq.listeBoutons = liste
          sq.boutonsMathQuill = liste.length !== 0

          sq.consigne_init = datasSection.consigne_init ?? ''
          for (let k = 1; k <= 8; k++) {
            let s = datasSection['charset' + k] // Le set de caractères utilisés dans l’éditeur
            sq['charset' + k] = (s !== undefined) ? s : ''
            s = datasSection['charsetText' + k] // Le set de caractères utilisés dans l’éditeur pour les éditeurs de chaînes
            sq['charsetText' + k] = (s !== undefined) ? s : ''
          }
          sq.variables = datasSection.variables ?? ''
          sq.titre = datasSection.titre ?? ''
          sq.bigSize = Boolean(datasSection.bigSize ?? false)
          sq.figSol = datasSection.figSol ?? ''
          sq.nbLatexSol = Number(datasSection.nbLatexSol ?? 0)
          sq.widthSol = Number(datasSection.widthSol) || 700
          sq.heightSol = Number(datasSection.heightSol) || 500
          sq.explicationSol = datasSection.explicationSol ?? ''
          sq.param = datasSection.param ?? '' // Chaine contenant les paramètres autorisés
          initDom()
        })

        // return;
      } else {
        j3pEmpty('correction')
        sq.numEssai = 1
        cacheSolution()
        sq.etape = 1
        sq.mtgAppLecteur.removeDoc('mtg32svg')
        sq.mtgAppLecteur.addDoc('mtg32svg', sq.fig, true)
        if (sq.aleat) {
          for (let i = 1; i <= sq.nbParamAleat; i++) {
            giveFormula2('r' + i, sq['tab' + i][this.questionCourante - 1])
          }
        }
        if (sq.param !== undefined) {
          const ch = 'abcdefghjklmnpqr'
          for (let i = 0; i < ch.length; i++) {
            const car = ch.charAt(i)
            if (sq.param.indexOf(car) !== -1) {
              const par = parcours.donneesSection[car]
              if (par !== 'random') giveFormula2(car, par)
            }
          }
        }

        calculate(true) // true pour que les calculs aléatoires soient réinitialisés
        etablitNombreEtapesEtCoeff()
        j3pEmpty('enonce')
        j3pEmpty('enonceSuite')
        j3pEmpty('formules')
        j3pEmpty('info')

        sq.nbexp = 0
        sq.reponse = -1
        afficheMathliveDans('enonce', 'texte', extraitDeLatexPourMathlive('enonce1'))
        afficheNombreEssaisRestants()
        j3pElement('info').style.display = 'block'
        montreEditeurs(true)
        creeEditeurs()
        sq.mtgAppLecteur.display('mtg32svg') // Pour tout réafficher
        sq.mtgAppLecteur.setActive('mtg32svg', false)
        // Pour revenir en haut de page
        try {
          window.location.hash = 'MepMG'
          j3pElement('MepMG').scrollIntoView()
        } catch (e) {}
        setFocusToFirstInput('expression')
        this.finEnonce()
      }

      break // case "enonce":

    case 'correction': {
      let bilanReponse = ''
      if (validationEditeurs()) {
        validation()
        if (sq.resolu) { bilanReponse = 'exact' } else {
          if (sq.exact) {
            bilanReponse = 'exactPasFini'
          } else { bilanReponse = 'faux' }
        }
      } else {
        bilanReponse = 'incorrect'
      }
      // A cause de la limite de temps :
      if (this.isElapsed) { // limite de temps
        montreEditeurs(false)
        j3pElement('correction').style.color = this.styles.cfaux
        j3pElement('boutonrecopier').style.display = 'none'
        j3pElement('info').style.display = 'none'
        sq.mtgAppLecteur.setActive('mtg32svg', true)
        sq.mtgAppLecteur.executeMacro('mtg32svg', 'solution')
        afficheSolution(false)
        j3pElement('correction').innerHTML = tempsDepasse
        j3pElement('correction').innerHTML += '<br>La solution se trouve ci-contre.'
        return this.finCorrection('navigation', true)
      }
      if (bilanReponse === 'incorrect') {
        j3pElement('correction').style.color = this.styles.cfaux
        let ch = 'Réponse incorrecte'
        if (sq.signeEgalManquant) ch += '<br> Il faut écrire une égalité'
        j3pElement('correction').innerHTML = ch
        // on reste dans l’état correction
        return this.finCorrection()
      }

      // Bonne réponse
      if (bilanReponse === 'exact') {
        if (sq.nbEtapes === 1) {
          this.score += 1
        } else {
          const scoreprec = sq.scoreInit
          let add
          if (sq.etape === 1) {
            add = sq.coefEtape1
          } else if (sq.etape === sq.nbEtapes) {
            add = sq.coefEtapeFin
          } else {
            add = sq.coefEtapesSuivantes
          }
          // add = Math.floor(add * 100 + 0.5) / 100 On n’arrondit plus à 0,01 près car sinon le score final peut être tronqué
          let newscore = this.score + add
          if (newscore > scoreprec + 1) newscore = scoreprec + 1
          this.score = newscore
        }
        j3pElement('correction').style.color = this.styles.cbien
        j3pElement('correction').innerHTML = cBien
        sq.numEssai++ // Pour un affichage correct dans afficheReponse
        afficheReponse('exact', false)

        if (sq.etape === sq.nbEtapes) {
          // dernière étape
          const score = this.score
          // Si le score est quasi entier c’est sans doute du à des erreurs d’arrondi
          if (Math.abs(score - Math.round(score)) < 0.000001) {
            this.score = Math.round(score)
          }
          j3pEmpty('enonceSuite')
          montreEditeurs(false)
          j3pElement('boutonrecopier').style.display = 'none'
          j3pElement('info').style.display = 'none'
          sq.mtgAppLecteur.setActive('mtg32svg', true)
          sq.mtgAppLecteur.executeMacro('mtg32svg', 'solution')
          afficheSolution(true)
          return this.finCorrection('navigation', true)
        }

        // il reste des étapes, on reste en état correction (pas génial d’appeler
        // finCorrection après avoir lancé etapeSuivante mais on a pas trop le choix)
        etapeSuivante()
        return this.finCorrection()
      }

      // mauvaise réponse
      sq.numEssai++
      if (bilanReponse === 'exactPasFini') {
        j3pElement('correction').style.color =
                (sq.numEssai <= this.donneesSection['nbEssais' + sq.etape]) ? '#0000FF' : this.styles.cfaux
        j3pElement('correction').innerHTML = 'Le calcul est bon mais pas écrit sous la forme demandée.'
      } else {
        j3pElement('correction').style.color = this.styles.cfaux
        j3pElement('correction').innerHTML = cFaux
      }
      j3pEmpty('info')
      if (sq.numEssai <= this.donneesSection['nbEssais' + sq.etape]) {
        afficheNombreEssaisRestants()
        afficheReponse(bilanReponse, false)
        videEditeurs()
        // S’il y a plus de deux chances,on appelle à nouveau le bouton Valider
        j3pElement('correction').innerHTML += '<br>' + '<br>' + essaieEncore
        // et on reste dans l’état correction
        return this.finCorrection()
      }

      // Erreur au dernier essai
      afficheReponse(bilanReponse, true)
      if (sq.etape === sq.nbEtapes) {
        // dernière étape
        montreEditeurs(false)
        j3pElement('boutonrecopier').style.display = 'none'
        j3pElement('info').style.display = 'none'
        sq.mtgAppLecteur.setActive('mtg32svg', true)
        sq.mtgAppLecteur.executeMacro('mtg32svg', 'solution')
        afficheSolution(false)
        j3pElement('correction').innerHTML += '<br><br>La solution se trouve ci-contre.'
        return this.finCorrection('navigation', true)
      }

      // dernier essai mais pas la dernière étape
      j3pElement('correction').innerHTML += '<br><br>On passe à la question suivante.'
      etapeSuivante()
      this.finCorrection()
      break // case "correction":
    }

    case 'navigation':
      if (!this.sectionTerminee()) {
        this.etat = 'enonce'
      }
      this.finNavigation()
      break // case "navigation":
  }
}
