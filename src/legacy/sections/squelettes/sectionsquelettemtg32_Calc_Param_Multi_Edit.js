import $ from 'jquery'

import { j3pAddElt, j3pAjouteBouton, j3pDetruit, j3pElement, j3pEmpty, j3pImporteAnnexe, j3pPaletteMathquill, j3pShowError } from 'src/legacy/core/functions'
import { unLatexify } from 'src/lib/mathquill/functions'
import { MathfieldElement, renderMathInDocument } from 'mathlive'
import { getMtgApp } from 'src/lib/outils/mathgraph'
import { j3pCreeSVG } from 'src/legacy/core/functionsSvg'
import { cleanLatexForMl, mathliveRestrict } from 'src/lib/outils/mathlive/utils'
import { afficheMathliveDans, getMathliveValue } from 'src/lib/outils/mathlive/display'
import { resetKeyboardPosition } from 'src/lib/widgets/mlVirtualKeyboard/VirtualKeyboard'
import textesGeneriques from 'src/lib/core/textes'

const { cBien, cFaux, essaieEncore, tempsDepasse } = textesGeneriques

/*
    Yves Biton
    Juillet 2019

    squelettemtg32_Calc_Param_Multi_Edit
    Squelette demandant de calculer une expression ou plusieurs expressions
    On peut éventuellement spécifier des paramètres  a,b,c,d,e,f,g,h,j,k,n,n,p.
    Utilise une fichier annexe dont le nom est contenu dans le paramètre ex.
    Ce fichier annexe contient une variable nbCalc qui contient le nombre d’expressions que l’élève doit calculer simultanément.
    Il contient aussi une variable formulaire qui est une chaîne de caractères contenant éventuellement du LaTeX et contenant
    une référence aux éditeurs MathQuill utilisés. Par exemple, si nbCalc  vaut 2, la chaine devra contenir deux sous-chaînes
    "edit1" et "edit2" qui correspondront aux éditeurs MathQuill.
    Quand on vérifiera la validité de la réponse de l’élève, on mettra la réponse
    contenue dans l’éditeur 1 dans le calcul rep1 et, après recalcul de la figure mtg32, le calcul resolu1 contiendra 1
    si la réponse contenue dans le premier éditeur est la réponse finale, 0 sinon.
    Le calcul exact1 contiendra lui 1 si la réponse est bonne à epsilon près.
    Si la paramètre simplifier est à false, un calcul exact mais pas fini sera accepté.
    Si la figure contenue  dans ex contient un paramètre nbvar , alors dans le cas ou nbrepetitions > 1, on donne pour i compris entre 1 et npParamAleat
    au calcul de la figure nommé "r"+i (par exemple r1 et r2 si nbParamAleat vaut 2) des valeurs comprises entre 0 et "nbcas"+i - 1
    toutes différentes (sauf si nbrepetitions > b=nbcas auquel cas au bout de nbcas répétitions il y aura nécessairement une valeur de r déjà prise.
    Le paramètre nbLatex contient le nombre d’affichages LaTeX qui doivent être récupérés de la figure pour affichage dans l’énoncé.

*/
// on définit le dossier des fonts mathlive (il faut le faire très tôt,
// ici c’est bon avec pnpm start mais trop tard pour un build (cf src/lib/player/index.js de la branche draftPlayer)
MathfieldElement.fontsDirectory = '/static/mathlive/fonts'
MathfieldElement.soundsDirectory = '/static/mathlive/sounds'

export const params = {
  /**
   * Ce qui est paramétrable dans l’éditeur de graphe
   * @type {LegacySectionParamsParametres}
   */
  parametres: [
    ['nbrepetitions', 2, 'entier', 'aucun'],
    ['limite', 0, 'entier', 'Temps disponible par répétition, en secondes'],
    ['nbEssais', 6, 'entier', 'Nombre d’essais maximum autorisés'],
    ['indicationfaute', true, 'boolean', 'true si on veut que certaines fautes soient expliquées après appui sur Entrée'],
    ['simplifier', true, 'boolean', 'true si on exige un forme donnée, false sinon'],
    ['a', 'random', 'string', 'Valeur de a'],
    ['b', 'random', 'string', 'Valeur de b'],
    ['c', 'random', 'string', 'Valeur de c'],
    ['d', 'random', 'string', 'Valeur de d'],
    ['e', 'random', 'string', 'Valeur de e'],
    ['f', 'random', 'string', 'Valeur de f'],
    ['g', 'random', 'string', 'Valeur de g'],
    ['h', 'random', 'string', 'Valeur de h'],
    ['j', 'random', 'string', 'Valeur de j'],
    ['k', 'random', 'string', 'Valeur de k'],
    ['l', 'random', 'string', 'Valeur de l'],
    ['m', 'random', 'string', 'Valeur de m'],
    ['n', 'random', 'string', 'Valeur de n'],
    ['p', 'random', 'string', 'Valeur de p'],
    ['q', 'random', 'string', 'Valeur de q'],
    ['r', 'random', 'string', 'Valeur de r'],
    ['ex', 'Vecteurs_Lire_Coord_Vect_Oblique', 'string', 'Nom du fichier annexe (xxx => squelettes-mtg32/xxx.js et xxx/yyy => squelettes-mtg32-xxx/yyy.js)']
  ]
}

/**
 * section squelettemtg32_Calc_Param_Multi_Edit
 * @this {Parcours}
 */
export default function main () {
  const parcours = this
  const sq = this.storage
  function onkeyup (ev, parcours) {
    const indice = this.indice
    if (sq.marked[indice - 1]) {
      demarqueEditeurPourErreur(indice)
    }
    if (ev.keyCode === 13) {
      const rep = getMathliveValue('expressioninputmq' + indice)
      if (rep === '') return
      const chcalcul = traiteMathlive(rep)
      const valide = sq.mtgAppLecteur.syntaxValidation('mtg32svg', 'rep1', chcalcul, true)
      if (valide) {
        validation(parcours)
        if (indice === 1) j3pElement('expressioninputmq' + indice).focus()
      } else {
        marqueEditeurPourErreur(indice)
        j3pElement('correction').style.color = parcours.styles.cfaux
        j3pElement('correction').innerHTML = 'Réponse incorrecte'
      }
    }
  }

  function traiteMathlive (ch) {
    // Mathlive ne met pas d’accolades pour les fractions dont le numérateur et le dénominateur sont des entiers
    ch = ch.replace(/\\frac(\d)(\d)/g, '\\frac{$1' + '}{' + '$2}')
    // Ni pour les racines carrées d’un entier
    ch = ch.replace(/\\sqrt(\d)/g, '\\sqrt{$1}')
    // Pour les puissances dont l’exposant est un chiffre, Mathlive ne met pas d’accolades
    ch = ch.replace(/\^([^{])/g, '^{$1}')
    ch = unLatexify(ch)
    // mathlive peut mettre des {} vides
    ch = ch.replace(/\\{\\}/g, '')
    ch = ch.replace(/\^(\d)(\d)/g, '^$1*$2') // On peut avoir par exemple 2^35^4 pour 2^3*5^4
    return sq.mtgAppLecteur.addImplicitMult('mtg32svg', ch)// Traitement des multiplications implicites
  }

  /**
   * Fonction extrayant d’un affichage LaTeX de la figure les lignes de texte et les transformant
   * en une chaîne affichable par MathQuill
   * @param tagLatex : le tag de l’affichage LaTeX de la figure
   * @return {string}
   */
  function extraitDeLatexPourMathlive (tagLatex) {
    const list = sq.mtgAppLecteur.getList('mtg32svg')
    const latex = list.getByTag(tagLatex, true)
    if (latex === null || latex.className !== 'CLatex') return ''
    return cleanLatexForMl(latex.chaineLatex, sq.charset)
  }

  /**
   * Fonction affichant la solution s’il existe un affichage LaTeX de tag solution
   * @param{boolean} bexact true si l’élève a entré la bonne réponse, false sinon
   */
  function afficheSolution (bexact) {
    $('#divSolution').css('display', 'block').html('')
    const ch = extraitDeLatexPourMathlive('solution')
    if (ch !== '') {
      afficheMathliveDans('divSolution', 'solution', ch, { style: { color: bexact ? 'green' : 'blue' } })
    }
  }

  function cacheSolution () {
    $('#divSolution').css('display', 'none').html('')
  }

  function afficheNombreEssaisRestants () {
    j3pEmpty('info')
    const nbe = sq.nbEssais - sq.numEssai + 1
    if (nbe === 1) {
      afficheMathliveDans('info', 'texteinfo', 'Il reste une validation.', { style: { color: '#7F007F' } })
    } else {
      afficheMathliveDans('info', 'texteinfo', 'Il reste ' + nbe + ' essais.', { style: { color: '#7F007F' } })
    }
  }

  function marqueEditeurPourErreur (indice) {
    sq.marked[indice - 1] = true
    const mf = j3pElement('expressioninputmq' + indice)
    mf.style.backgroundColor = '#FF9999'
    mf.demarqueErreur = () => {
      if (sq.marked[indice - 1]) {
        demarqueEditeurPourErreur(indice)
      }
    }
  }

  function demarqueEditeurPourErreur (indice) {
    if (!sq.marked[indice - 1]) return
    sq.marked[indice - 1] = false // Par précaution si appel via bouton recopier réponse trop tôt
    const mf = j3pElement('expressioninputmq' + indice)
    mf.style.backgroundColor = 'white'
  }

  function validationEditeurs () {
    let res = true
    let focusDonne = false
    for (let ind = 1; ind <= sq.nbCalc; ind++) {
      const rep = getMathliveValue('expressioninputmq' + ind)
      if (rep === '') {
        res = false
        if (!focusDonne) {
          focusDonne = true
          setTimeout(function () {
            j3pElement('expressioninputmq' + ind).focus()
          })
        }
      } else {
        const chcalcul = traiteMathlive(rep)
        const valide = sq.mtgAppLecteur.syntaxValidation('mtg32svg', 'rep' + ind, chcalcul, true)
        if (!valide) {
          res = false
          marqueEditeurPourErreur(ind)
          if (!focusDonne) {
            focusDonne = true
            setTimeout(function () {
              j3pElement('expressioninputmq' + ind).focus()
            })
          }
        }
      }
    }
    return res
  }

  function montreEditeurs (bVisible) {
    if (bVisible) $('#info').css('display', 'block')
    $('#editeur').css('display', bVisible ? 'block' : 'none')
    if (sq.boutonsMathQuill) $('#boutonsmathquill').css('display', bVisible ? 'block' : 'none')
    // On vide le contenu de l’éditeur MathQuill et on lui donne le focus.
    if (bVisible) j3pElement('expressioninputmq1').value = ''
    j3pElement('expressioninputmq1').focus()
  }

  function videEditeurs () {
    for (let i = 1; i <= sq.nbCalc; i++) {
      j3pElement('expressioninputmq' + i).value = ''
    }
  }

  function afficheReponse (bilan, depasse) {
    let coul
    const num = sq.numEssai
    const idrep = 'exp' + num
    // let ch = (num > 2) ? '<br>' : ''
    // Avec MathliveAffiche il ne faut pas envoyer une chaîne commençant par un <br> car le premier affichage
    // se fait inline (celui du <br> mais le suivant est dans un <p> ce qui provoque un retour à la ligne)
    // et donc on a par exemple Réponse exacte : suivi d’un retour à la ligne suivi de la réponse
    // alors qu’on veut que tout soit sur une même ligne
    if (num > 2) afficheMathliveDans('formules', idrep + 'br', '<br>')
    let ch

    if (bilan === 'exact') {
      ch = 'Réponse exacte : '
      coul = parcours.styles.cbien
    } else {
      if (bilan === 'faux') {
        ch = 'Réponse fausse : '
        coul = parcours.styles.cfaux
      } else { // Exact mais pas fini
        ch = 'Exact pas fini : '
        if (depasse) {
          coul = parcours.styles.cfaux
        } else {
          coul = '#0000FF'
        }
      }
    }
    afficheMathliveDans('formules', idrep + 'deb', ch, {
      style: {
        color: coul
      }
    })

    ch = cleanLatexForMl(sq.formulaire)

    for (let i = 1; i <= sq.nbCalc; i++) {
      const st = '\\textcolor{' + (sq.repResolu[i - 1] ? parcours.styles.cbien : (sq.repExact[i - 1] ? '#0000FF' : parcours.styles.cfaux)) + '}{'
      ch = ch.replace('edit' + i, '$' + st + sq.rep[i - 1] + '}$')
    }
    afficheMathliveDans('formules', idrep + 'fin', ch, sq.paramFormulaire)
    if (sq.texteFaute) {
      afficheMathliveDans('formules', idrep + 'faute', '<br>' + sq.texteFaute, {
        style: {
          color: coul
        }
      })
    }
    resetKeyboardPosition()
  }

  function ecoute () {
    if (sq.boutonsMathQuill) {
      j3pEmpty('boutonsmathquill')
      const indice = this.indice

      j3pPaletteMathquill('boutonsmathquill', 'expressioninputmq' + indice, {
        liste: sq.listeBoutons,
        nomdiv: 'palette'
      })
      // Ligne suivante pour un bon alignement
      $('#palette').css('display', 'inline-block')
    }
  }

  function validation () {
    let ind
    if (sq.numEssai >= 1) {
      j3pElement('boutonrecopier').style.display = 'block'
    }
    sq.rep = []
    sq.repExact = []
    sq.repResolu = []

    for (ind = 1; ind <= sq.nbCalc; ind++) {
      const rep = getMathliveValue('expressioninputmq' + ind)
      sq.rep.push(rep)
      const chcalcul = traiteMathlive(rep)
      sq.mtgAppLecteur.giveFormula2('mtg32svg', 'rep' + ind, chcalcul)
    }
    sq.mtgAppLecteur.calculate('mtg32svg', false)
    if (sq.indicationfaute) {
      const faute = sq.mtgAppLecteur.valueOf('mtg32svg', 'faute')
      if (faute === 1) {
        sq.mtgAppLecteur.executeMacro('mtg32svg', 'voirFaute')
      } else {
        sq.mtgAppLecteur.executeMacro('mtg32svg', 'masquerFaute')
      }
    }
    let exact = true
    let resolu = true
    let auMoinsUnExact = false
    for (ind = 1; ind <= sq.nbCalc; ind++) {
      const res = sq.mtgAppLecteur.valueOf('mtg32svg', 'resolu' + ind)
      const ex = sq.mtgAppLecteur.valueOf('mtg32svg', 'exact' + ind)
      resolu = resolu && (res === 1)
      exact = exact && ((res === 1) || (ex === 1))
      sq.repResolu.push(res === 1)
      sq.repExact.push((res === 1) || (ex === 1))
      auMoinsUnExact = auMoinsUnExact || (ex === 1)
    }
    sq.resolu = resolu
    sq.exact = exact && auMoinsUnExact
    sq.texteFaute = ''
    if (sq.indicationfaute && !sq.exact && !sq.resolu) {
      sq.texteFaute = extraitDeLatexPourMathlive('faute')
    }
  }

  function recopierReponse () {
    for (let ind = sq.nbCalc; ind > 0; ind--) {
      demarqueEditeurPourErreur(ind)
      j3pElement('expressioninputmq' + ind).value = sq.rep[ind - 1]
      if (ind === 1) {
        j3pElement('expressioninputmq' + ind).focus()
      }
    }
  }

  function initMtg () {
    sq.nbEssais = parseInt(parcours.donneesSection.nbEssais)
    sq.simplifier = parcours.donneesSection.simplifier
    if (sq.simplifier === undefined) sq.simplifier = true
    const mtgOptions = {
      // player only, sans figure initiale ni svg, on gère ça dans initMtg et modif_fig
      loadCoreWithMathJax: true
    }
    getMtgApp('mtg32svg', {}, mtgOptions).then((mtgAppLecteur) => {
      sq.mtgAppLecteur = mtgAppLecteur
      initDom()
      let code, par, car, i, j, nbrep, ar, tir, nb
      sq.mtgAppLecteur.removeAllDoc()
      sq.mtgAppLecteur.addDoc('mtg32svg', sq.txt, true)
      sq.mtgAppLecteur.calculate('mtg32svg', false)
      sq.nbexp = 0
      const ch = 'abcdefghjklmnpqr'
      const nbvar = sq.mtgAppLecteur.valueOf('mtg32svg', 'nbvar')
      if (nbvar !== -1) {
        nbrep = parcours.donneesSection.nbrepetitions
        sq.aleat = true
        sq.nbParamAleat = nbvar
        for (i = 1; i <= nbvar; i++) {
          const nbcas = sq.mtgAppLecteur.valueOf('mtg32svg', 'nbcas' + i)
          nb = Math.max(nbrep, nbcas)
          ar = []
          for (j = 0; j < nb; j++) ar.push(j % nbcas)
          sq['tab' + i] = []
          for (let k = 0; k < nbrep; k++) {
            tir = Math.floor(Math.random() * ar.length)
            sq['tab' + i].push(ar[tir])
            ar.splice(tir, 1)
          }
          sq.mtgAppLecteur.giveFormula2('mtg32svg', 'r' + i, sq['tab' + i][0])
        }
      } else {
        sq.aleat = false
      }

      if (sq.param !== undefined) {
        for (i = 0; i < ch.length; i++) {
          car = ch.charAt(i)
          if (sq.param.indexOf(car) !== -1) {
            par = parcours.donneesSection[car]
            if (par !== 'random') sq.mtgAppLecteur.giveFormula2('mtg32svg', car, par)
          }
        }
      }
      sq.mtgAppLecteur.calculateAndDisplayAll(true)
      const nbParam = parseInt(sq.nbLatex)
      const t = 'abcdefghijklmnopq'
      const param = {}
      for (i = 0; i < nbParam; i++) {
        code = sq.mtgAppLecteur.getLatexCode('mtg32svg', i)
        param[t.charAt(i)] = code
      }

      const st = cleanLatexForMl(sq.consigne1 + (sq.simplifier ? sq.consigne3 : sq.consigne2) + sq.consigne4)
      afficheMathliveDans('enonce', 'texte', st, param)
      sq.indicationfaute = parcours.donneesSection.indicationfaute // true si indication quand certaines fautes sont faites et appui sur Entrée
      let formulaire = cleanLatexForMl(sq.formulaire)
      for (let k = 1; k <= sq.nbCalc; k++) formulaire = formulaire.replace('edit' + k, '&' + k + '&')
      const obj = {}
      sq.paramFormulaire = {}
      for (let k = 1; k <= sq.nbCalc; k++) obj['inputmq' + k] = {}
      for (i = 0; i < nbParam; i++) {
        code = sq.mtgAppLecteur.getLatexCode('mtg32svg', i)
        obj[t[i]] = code
        sq.paramFormulaire[t[i]] = code
      }
      // obj.styletexte = {}
      obj.charset = sq.charset
      obj.listeBoutons = sq.listeBoutons
      afficheMathliveDans('editeur', 'expression', formulaire, obj) // sq.formulaire contient ou plusieurs éditeurs MathQuill
      // On affecte un indice à chaque éditeur ainsi qu’un écouteur de focus
      for (let k = 1; k <= sq.nbCalc; k++) {
        j3pElement('expressioninputmq' + k).indice = k
        if (sq.charset !== '') {
          mathliveRestrict('expressioninputmq' + k, sq.charset)
        }
        $('#expressioninputmq' + k).focusin(function () {
          ecoute.call(this)
        })
        j3pElement('expressioninputmq' + k).onkeyup = function (ev) {
          onkeyup.call(this, ev, parcours)
        }
      }
      if (sq.boutonsMathQuill) {
        j3pPaletteMathquill('boutonsmathquill', 'expressioninputmq1', { liste: sq.listeBoutons, nomdiv: 'palette' })
        // Ligne suivante pour un bon alignement
        $('#palette').css('display', 'inline-block')
      } else {
        j3pDetruit('boutonsmathquill')
      }

      j3pAjouteBouton('conteneurbouton', 'boutonrecopier', 'MepBoutonsRepere', 'Recopier réponse précédente',
        recopierReponse)
      j3pElement('boutonrecopier').style.display = 'none'

      j3pElement('expressioninputmq1').focus()
      parcours.finEnonce()
    }).catch(j3pShowError)
  }

  function initDom () {
    renderMathInDocument({ renderAccessibleContent: '' })
    if (sq.titre) parcours.afficheTitre(sq.titre)

    const conteneur = j3pAddElt(parcours.zonesElts.MG, 'div', '', {
      id: 'conteneur',
      style: parcours.styles.etendre('toutpetit.enonce', { padding: '6px' })
    })
    j3pAddElt(conteneur, 'div', '', { id: 'enonce' })
    // Contient les formules entrées par l’élève
    j3pAddElt(conteneur, 'div', '', { id: 'formules', style: parcours.styles.petit.enonce })
    j3pAddElt(conteneur, 'div', '', { id: 'info' })
    afficheNombreEssaisRestants()
    j3pAddElt(conteneur, 'div', '', { id: 'conteneurbouton' })
    // Le div qui contiendra la chaine avec le ou les éditeurs MathQuill
    j3pAddElt(conteneur, 'div', '', { id: 'editeur', style: { paddingTop: '10px' } })
    j3pAddElt(conteneur, 'div', { id: 'boutonsmathquill', style: { display: 'inline-block', paddingTop: '10px' } })
    j3pAddElt('conteneur', 'div', '', { id: 'divSolution' })
    $('#divSolution').css('display', 'none')
    j3pElement('divSolution').style.fontSize = '21px'
    const divMtg = j3pAddElt(conteneur, 'div', '', { id: 'divmtg32' })
    j3pCreeSVG(divMtg, {
      id: 'mtg32svg',
      width: sq.width,
      height: sq.height
    })
    const style = parcours.styles.petit.correction
    style.marginTop = '1.5em'
    style.marginLeft = '0.5em'
    j3pAddElt(parcours.zonesElts.MD, 'div', '', { id: 'correction', style })
  }

  switch (this.etat) {
    case 'enonce':
      // code exécuté au lancement de la section
      if (this.debutDeLaSection) {
        // Construction de la page
        parcours.construitStructurePage('presentation1bis')

        // compteur.
        sq.numEssai = 1
        // faut gérer sous-dossier ou pas…
        const sep = this.donneesSection.ex.includes('/') ? '-' : '/'
        j3pImporteAnnexe('squelettes-mtg32' + sep + this.donneesSection.ex + '.js').then(function (datasSection) {
          // console.log("initsection");
          // Attention, le this devient window...
          sq.txt = datasSection.txt
          sq.nbLatex = Number(datasSection.nbLatex ?? 0) // Le nombre de paramètres LaTeX dans le texte
          const listeBoutons = []
          // boutons avec propriétés à true par défaut
          const tabBoutons1 = ['puissance', 'fraction']
          ;['btnPuis', 'btnFrac'].forEach((p, i) => {
            const b = Boolean(datasSection[p] ?? true)
            if (b) listeBoutons.push(tabBoutons1[i])
          })
          // boutons avec false par défaut
          const tabBoutons2 = ['racine', 'pi', 'exp', 'ln', 'sin', 'cos', 'tan', 'abs']
          ;['btnRac', 'btnPi', 'btnExp', 'btnLn', 'btnSin', 'btnCos', 'btnTan', 'btnAbs'].forEach((p, i) => {
            const b = Boolean(datasSection[p] ?? false)
            if (b) listeBoutons.push(tabBoutons2[i])
          })
          sq.listeBoutons = listeBoutons
          sq.boutonsMathQuill = listeBoutons.length !== 0
          sq.consigne1 = datasSection.consigne1 ?? ''
          sq.consigne2 = datasSection.consigne2 ?? ''
          sq.consigne3 = datasSection.consigne3 ?? ''
          sq.consigne4 = datasSection.consigne4 ?? ''
          sq.titre = datasSection.titre ?? ''
          sq.width = Number(datasSection.width ?? 700)
          sq.height = Number(datasSection.height ?? 900)
          sq.nbCalc = datasSection.nbCalc
          sq.formulaire = datasSection.formulaire // Contient la chaine avec les éditeurs.
          sq.param = datasSection.param ?? '' // Chaine contenant les paramètres autorisés
          sq.charset = datasSection.charset ?? ''
          sq.marked = []
          for (let k = 0; k < sq.nbCalc; k++) {
            sq.marked[k] = false
          }
          initMtg()
        }).catch(error => {
          console.error(error)
          j3pShowError('Impossible de charger les données de cette exercice')
        })
        // return;
      } else {
        j3pEmpty('correction')
        cacheSolution()
        sq.numEssai = 1
        sq.mtgAppLecteur.removeDoc('mtg32svg')
        sq.mtgAppLecteur.addDoc('mtg32svg', sq.txt, true)

        if (sq.aleat) {
          for (let i = 1; i <= sq.nbParamAleat; i++) {
            sq.mtgAppLecteur.giveFormula2('mtg32svg', 'r' + i, sq['tab' + i][this.questionCourante - 1])
          }
        }
        if (sq.param !== undefined) {
          const ch = 'abcdefghjklmnpqr'
          for (let i = 0; i < ch.length; i++) {
            const car = ch.charAt(i)
            if (sq.param.indexOf(car) !== -1) {
              const par = parcours.donneesSection[car]
              if (par !== 'random') sq.mtgAppLecteur.giveFormula2('mtg32svg', car, par)
            }
          }
        }

        sq.mtgAppLecteur.calculate('mtg32svg', true) // true pour que les calculs alèatoires soient réinitialisés
        j3pEmpty('enonce')
        j3pEmpty('formules')
        j3pEmpty('info')
        sq.nbexp = 0
        const nbParam = parseInt(sq.nbLatex)
        const t = 'abcdefghijklmnopq'
        const param = {}
        for (let i = 0; i < nbParam; i++) {
          const code = sq.mtgAppLecteur.getLatexCode('mtg32svg', i)
          param[t.charAt(i)] = code
        }

        const st = cleanLatexForMl(sq.consigne1 + (sq.simplifier ? sq.consigne3 : sq.consigne2) + sq.consigne4)
        afficheMathliveDans('enonce', 'texte', st, param)
        afficheNombreEssaisRestants()
        j3pElement('info').style.display = 'block'

        j3pEmpty('editeur')
        let formulaire = cleanLatexForMl(sq.formulaire)
        for (let k = 1; k <= sq.nbCalc; k++) formulaire = formulaire.replace('edit' + k, '&' + k + '&')
        const obj = {}
        sq.paramFormulaire = {}
        for (let k = 1; k <= sq.nbCalc; k++) obj['inputmq' + k] = {}
        for (let i = 0; i < nbParam; i++) {
          const code = sq.mtgAppLecteur.getLatexCode('mtg32svg', i)
          obj[t[i]] = code
          sq.paramFormulaire[t[i]] = code
        }
        // obj.styletexte = {}
        obj.charset = sq.charset
        obj.listeBoutons = sq.listeBoutons
        afficheMathliveDans('editeur', 'expression', formulaire, obj) // sq.formulaire contient ou plusieurs éditeurs MathQuill
        for (let k = 1; k <= sq.nbCalc; k++) {
          j3pElement('expressioninputmq' + k).indice = k
          if (sq.charset !== '') {
            mathliveRestrict('expressioninputmq' + k, sq.charset)
          }
          $('#expressioninputmq' + k).focusin(function () {
            ecoute.call(this)
          })
          j3pElement('expressioninputmq' + k).onkeyup = function (ev) {
            onkeyup.call(this, ev, parcours)
          }
          // j3pElement("expressioninputmq" + k).onkeydown = sq.onkeydown;
        }

        sq.mtgAppLecteur.display('mtg32svg') // Pour tout réafficher
        videEditeurs()
        montreEditeurs(true)
        // Pour revenir en haut de page
        try {
          window.location.hash = 'MepMG'
          j3pElement('MepMG').scrollIntoView()
        } catch (e) {
          console.error(e)
        }
        j3pElement('expressioninputmq1').focus()
        this.finEnonce()
      }

      break // case "enonce":

    case 'correction': {
      let bilanReponse = ''
      let simplificationPossible = false
      if (validationEditeurs()) {
        validation()
        if (sq.simplifier) {
          if (sq.resolu) {
            bilanReponse = 'exact'
          } else {
            if (sq.exact) {
              bilanReponse = 'exactPasFini'
              simplificationPossible = true
            } else {
              bilanReponse = 'faux'
            }
          }
        } else {
          if (sq.resolu || sq.exact) {
            bilanReponse = 'exact'
            if (!sq.resolu) simplificationPossible = true
          } else {
            bilanReponse = 'faux'
          }
        }
      } else {
        bilanReponse = 'incorrect'
      }
      // A cause de la limite de temps :
      if (this.isElapsed) { // limite de temps
        this._stopTimer()
        montreEditeurs(false)
        j3pElement('correction').style.color = this.styles.cfaux
        j3pElement('boutonrecopier').style.display = 'none'
        j3pElement('info').style.display = 'none'
        afficheSolution(false)
        if (sq.indicationfaute) sq.mtgAppLecteur.executeMacro('mtg32svg', 'masquerFaute')

        sq.mtgAppLecteur.executeMacro('mtg32svg', 'solution')
        j3pElement('correction').innerHTML = tempsDepasse
        j3pElement('correction').innerHTML += '<br>La solution se trouve ci-contre.'

        this.etat = 'navigation'
        this.sectionCourante()
      } else {
        if (bilanReponse === 'incorrect') {
          j3pElement('correction').style.color = this.styles.cfaux
          j3pElement('correction').innerHTML = 'Réponse incorrecte'
        } else {
          // Une réponse a été saisie
          // Bonne réponse
          if (bilanReponse === 'exact') {
            this._stopTimer()
            sq.mtgAppLecteur.setActive('mtg32svg', false)
            this.score++
            j3pElement('correction').style.color = this.styles.cbien
            if (simplificationPossible) {
              j3pElement('correction').innerHTML = 'C’est bien mais on pouvait  simplifier la réponse'
            } else {
              j3pElement('correction').innerHTML = cBien
            }
            j3pElement('boutonrecopier').style.display = 'none'
            j3pElement('info').style.display = 'none'
            if (sq.indicationfaute) sq.mtgAppLecteur.executeMacro('mtg32svg', 'masquerFaute')
            afficheSolution(true)
            sq.mtgAppLecteur.executeMacro('mtg32svg', 'solution')
            montreEditeurs(false)
            sq.numEssai++ // Pour un affichage correct dans afficheReponse
            afficheReponse('exact', false)
            this.etat = 'navigation'
            this.sectionCourante()
          } else {
            sq.numEssai++
            if (bilanReponse === 'exactPasFini') {
              j3pElement('correction').style.color =
                (sq.numEssai <= sq.nbEssais) ? '#0000FF' : this.styles.cfaux
              j3pElement('correction').innerHTML = 'Le calcul est bon mais pas écrit sous la forme demandée.'
            } else {
              j3pElement('correction').style.color = this.styles.cfaux
              j3pElement('correction').innerHTML = cFaux
            }
            j3pEmpty('info')
            if (sq.numEssai <= sq.nbEssais) {
              afficheNombreEssaisRestants()
              videEditeurs()
              afficheReponse(bilanReponse, false)
              // S’il y a plus de deux chances,on appelle à nouveau le bouton Valider
              j3pElement('correction').innerHTML += '<br>' + '<br>' + essaieEncore
              setTimeout(function () {
                j3pElement('expressioninputmq1').focus()
              })
            } else {
              // Erreur au nème essai
              this._stopTimer()
              sq.mtgAppLecteur.setActive('mtg32svg', false)
              montreEditeurs(false)
              j3pElement('boutonrecopier').style.display = 'none'
              j3pElement('info').style.display = 'none'
              if (sq.indicationfaute) sq.mtgAppLecteur.executeMacro('mtg32svg', 'masquerFaute')
              afficheSolution(false)
              sq.mtgAppLecteur.executeMacro('mtg32svg', 'solution')
              afficheReponse(bilanReponse, true)
              j3pElement('correction').innerHTML += '<br><br>La solution se trouve ci-contre.'
              this.etat = 'navigation'
              this.sectionCourante()
            }
          }
        }
      }
      // Obligatoire
      this.finCorrection()
      break // case "correction":
    }

    case 'navigation':
      if (!this.sectionTerminee()) {
        this.etat = 'enonce'
      }
      this.finNavigation()
      break // case "navigation":
  }
}
