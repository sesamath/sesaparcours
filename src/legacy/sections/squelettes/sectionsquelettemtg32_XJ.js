import $ from 'jquery'

import { j3pAjouteBouton, j3pDiv, j3pElement, j3pEmpty, j3pFocus, j3pImporteAnnexe, j3pMathquillXcas, j3pPaletteMathquill, j3pRestriction, j3pShowError, j3pValeurde } from 'src/legacy/core/functions'
import { j3pAffiche, j3pMathsAjouteDans } from 'src/lib/mathquill/functions'
import { j3pCreeSVG } from 'src/legacy/core/functionsSvg'
import { getMtgCore } from 'src/lib/outils/mathgraph'
import textesGeneriques from 'src/lib/core/textes'

const { cBien, cFaux, essaieEncore, tempsDepasse } = textesGeneriques

/*
    Yves Biton
    Juillet 2014

    Modifié par Xavier JEROME
    Août 2020

    http://localhost:8081/?graphe=[1,"squelettemtg32_XJ",[{pe:">=0",nn:"fin",conclusion:"fin"},{f:"Fact_ax_Carre_Moins_cCarre_Ex1"}]];
    squelettemtg32_XJ
    Squelette demandant de calculer une expression et de l’écrire sous la forme la plus simple possible
    On peut éventuellement spécifier des paramètres  a,b,c,d,e,f,g,h,j,k,n,n,p.

    Si ex contient un paramètre nbParamAleat , alors dans le cas ou nbrepetitions > 1, on donne pour i compris entre 1 et npParamAleat
    au calcul de la figure nommé "r"+i (par exemple r1 et r2 si nbParamAleat vaut 2) des valeurs comprises entre 0 et "nbcas"+i - 1
    toutes différentes (sauf si nbrepetitions > b=nbcas auquel cas au bout de nbcas répétitions il y aura nécessairement une valeur de r déjà prise.
    Le paramètre k sert à choisir une formule au hasard parmi plusieurs. IL doit être à "random"

*/

export const params = {
  outils: ['calculatrice', 'mathquill'],
  /**
   * Ce qui est paramétrable dans l’éditeur de graphe
   * @type {LegacySectionParamsParametres}
   */
  parametres: [
    ['nbrepetitions', 1, 'entier', 'aucun'],
    ['limite', 0, 'entier', 'Temps disponible par répétition, en secondes'],
    ['nbchances', 1, 'entier', 'Nombre d’erreurs possibles'],
    ['nbEssais', 6, 'entier', 'Nombre d’essais maximum autorisés'],
    ['indicationfaute', true, 'boolean', 'true si on veut que certaines fautes soient expliquées après appui sur Entrée'],
    ['simplificationMaximale', false, 'boolean', 'true si on n’accepte pas des réponses\nde la forme 1*x+b ou ax+0, false sinon'],
    ['a', 'random', 'string', 'Valeur de a'],
    ['b', 'random', 'string', 'Valeur de b'],
    ['c', 'random', 'string', 'Valeur de c'],
    ['d', 'random', 'string', 'Valeur de d'],
    ['e', 'random', 'string', 'Valeur de e'],
    ['f', 'random', 'string', 'Valeur de f'],
    ['P', 'random', 'string', 'Valeur de p'],
    ['q', 'random', 'string', 'Valeur de q'],
    ['ex', 'College_Reduction_Deg1', 'string', 'Nom du fichier annexe (xxx => squelettes-mtg32/xxx.js et xxx/yyy => squelettes-mtg32-xxx/yyy.js)']
  ]
}

/**
 * section squelettemtg32_XJ
 * @this {Parcours}
 */
export default function main () {
  const parcours = this
  const sq = this.storage

  function onkeyup (ev) {
    if (sq.marked) { demarqueEditeurPourErreur() }
    if (ev.keyCode === 13) {
      const rep = j3pValeurde('expressioninputmq1')
      const chcalcul = traiteMathQuill(rep)
      const valide = sq.mtgAppLecteur.syntaxValidation('mtg32svg', 'rep', chcalcul, true)
      if (valide) { validation(parcours) } else {
        marqueEditeurPourErreur()
        j3pElement('correction').style.color = parcours.styles.cfaux
        j3pElement('correction').innerHTML = 'Réponse incorrecte'
      }
    }
  }

  function traiteMathQuill (ch) {
    ch = ch.replace(/}{/g, '}/{') // Pour traiter les \frac
    ch = ch.replace(/\\times/g, '*') // POur traiter les signes de multiplication
    ch = ch.replace(/\\left\(/g, '(') // Pour traiter les parenthèses ouvrantes
    ch = ch.replace(/\\right\)/g, ')') // Pour traiter les parenthèses fermantes
    ch = ch.replace(/\\left\|/g, 'abs(') // Traitement des valeurs absolues
    ch = ch.replace(/\\right\|/g, ')')
    ch = ch.replace(/\\frac/g, '') // Les fractions ont déja été remplacées par des divisions
    ch = ch.replace(/\\sqrt/g, 'sqrt') // Traitement des racines carrées
    ch = ch.replace(/\\ln/g, 'ln') // Traitement des ln
    ch = ch.replace(/\\le/g, '<=') // Traitement des signes <=
    ch = ch.replace(/\\ge/g, '>=') // Traitement des signes >=
    ch = ch.replace(/{/g, '(') // Les accolades deviennet des parenthèses
    ch = ch.replace(/}/g, ')')
    ch = ch.replace(/\)(\d)/g, ')*$1') // Remplace les parenthèses ")" suivies d’un chiffre en ajoutant un *
    ch = ch.replace(/\^(\d)(\d)/g, '^$1*$2') // On peut avoir par exemple 2^35^4 pour 2^3*5^4
    ch = ch.replace(/,/g, '.') // Remplacement des virgules par des points décimaux
    ch = ch.replace(/PI/g, 'pi')
    ch = ch.replace(/\\pi[ ]*/g, 'pi')
    ch = ch.replace(/ /g, '') // ON retire les espaces inutiles
    ch = sq.mtgAppLecteur.addImplicitMult('mtg32svg', ch)// Traitement des multiplications implicites
    return ch
  }

  function marqueEditeurPourErreur () {
    sq.marked = true
    $('#expressioninputmq1').css('background-color', '#FF9999')
  }
  function demarqueEditeurPourErreur () {
    sq.marked = false
    j3pElement('correction').innerHTML = ''

    $('#expressioninputmq1').css('background-color', '#FFFFFF')
  }

  // Le deuxième paramètre est à false seulement quand elle est appelée depuis l’étape de correction
  // Sinon la fonction est appelée par l’éditeur quand l’utilisateur appuie sur la touche entrée avec un seul paramètre
  function validation (parcours, continuer) {
    let i
    if (arguments.length <= 1) continuer = true
    if (sq.nbexp === 0) {
      j3pElement('boutonrecopier').style.display = 'block'
    }
    sq.rep = j3pValeurde('expressioninputmq1')
    const chcalcul = traiteMathQuill(sq.rep)
    sq.mtgAppLecteur.giveFormula2('mtg32svg', 'rep', chcalcul)
    j3pElement('correction').innerHTML = ''
    sq.mtgAppLecteur.calculate('mtg32svg', false)
    if (sq.indicationfaute) {
      const faute = sq.mtgAppLecteur.valueOf('mtg32svg', 'faute')
      if (faute === 1) sq.mtgAppLecteur.executeMacro('mtg32svg', 'voirFaute')
      else sq.mtgAppLecteur.executeMacro('mtg32svg', 'masquerFaute')
    }

    sq.reponse = sq.mtgAppLecteur.valueOf('mtg32svg', 'reponse')
    if (sq.reponse === 2 && !sq.simplificationMaximale) {
      // On regarde si l’élève a pu entre 1*x+b ou ax+0 ou x+0 etc
      // On compte le nomfre de * et de +
      i = 0
      let nb1 = 0
      while ((i = chcalcul.indexOf('+', i)) !== -1) {
        nb1++
        i++
      }
      i = 0
      let nb2 = 0
      while ((i = chcalcul.indexOf('*', i)) !== -1) {
        nb2++
        i++
      }
      if (nb1 <= 1 && nb2 <= 1) {
        let chc = chcalcul.replace(/(\D)1\*/, '$1').replace(/\*1(\D)/, '$1')
        if (chc.startsWith('1*')) chc = chc.substring(2)
        if (chc.endsWith('*1')) chc = chc.substring(0, chc.length - 2)
        if (chc.startsWith('x*0+')) chc = chc.substring(4)
        if (chc.startsWith('x*0-')) chc = chc.substring(3)
        if (chc.startsWith('0*x')) chc = chc.substring(3)
        if (chc.endsWith('+0*x')) chc = chc.substring(0, chc.length - 4)
        if (chc.endsWith('+x*0')) chc = chc.substring(0, chc.length - 4)
        if (chc.endsWith('+0')) chc = chc.substring(0, chc.length - 2)
        if (chc.startsWith('0+')) chc = chc.substring(2)
        if (chc.startsWith('0-')) chc = chc.substring(1)
        if (chc === '') chc = '0'
        sq.mtgAppLecteur.giveFormula2('mtg32svg', 'rep', chc)
        sq.mtgAppLecteur.calculate('mtg32svg', false)
        if (sq.mtgAppLecteur.valueOf('mtg32svg', 'reponse') === 1) sq.reponse = 1
      }
    }
    const nbParam = parseInt(sq.numero)
    const t = ['a', 'b', 'c', 'd']
    sq.parametres = {}
    let code
    for (i = 0; i < nbParam; i++) {
      code = sq.mtgAppLecteur.getLatexCode('mtg32svg', i)
      sq.parametres[t[i]] = code
    }
    sq.parametres.e = sq.nbEssais

    const idrep = 'exp' + sq.nbexp
    sq.nbexp++
    const chdeb = sq.nbexp === 1 ? '' : '<br>'
    sq.parametres.r = sq.rep
    const st = chdeb + (sq.reponse >= 1 ? sq.symbexact : sq.symbnonexact)
    j3pMathsAjouteDans('formules', { id: idrep, content: st, parametres: sq.parametres })
    if (sq.reponse === 1) j3pElement(idrep).style.color = '#0000FF'
    else j3pElement(idrep).style.color = '#FF0000'
    // On vide le contenu de l’éditeur MathQuill
    $('#expressioninputmq1').mathquill('latex', ' ').focus()

    // Si le nombre maximum d’essais est atteint on passe en mode correction
    if (sq.nbexp >= sq.nbEssais) {
      sq.mtgAppLecteur.setActive('mtg32svg', false)
      if (continuer) {
        parcours.etat = 'correction'
        parcours.sectionCourante()
        $('#editeur').css('display', 'none')
        $('#boutonsmathquill').css('display', 'none')
      }
    } else {
      j3pElement('info').innerHTML = ''
      const nbe = parcours.donneesSection.nbEssais - sq.nbexp
      let nbc = parcours.donneesSection.nbchances - parcours.essaiCourant
      if (nbc > nbe) {
        nbc = nbe
      }
      if ((nbe === 1) && (nbc === 1)) j3pAffiche('info', 'texteinfo', 'Il reste une validation.', { style: { color: '#7F007F' } })
      else j3pAffiche('info', 'texteinfo', 'Il reste ' + nbe + ' essai(s) et ' + nbc + ' validation(s).', { style: { color: '#7F007F' } })
    }
  }

  function recopierReponse () {
    $('#expressioninputmq1').mathquill('latex', sq.rep).focus()
  }

  function initMtg () {
    // chargement mtg
    getMtgCore({ withMathJax: true })
      .then(
        // success
        (mtgAppLecteur) => {
          sq.mtgAppLecteur = mtgAppLecteur
          let code, par, car, i, j, k, nbrep, ar, tir, nb
          sq.mtgAppLecteur.removeAllDoc()
          sq.mtgAppLecteur.addDoc('mtg32svg', sq.txt, true)
          sq.mtgAppLecteur.calculate('mtg32svg', false)
          sq.nbexp = 0
          sq.reponse = -1
          const ch = 'abcdefpqk'
          const nbvar = sq.mtgAppLecteur.valueOf('mtg32svg', 'nbvar')
          if (nbvar !== -1) {
            nbrep = parcours.donneesSection.nbrepetitions
            sq.aleat = true
            sq.nbParamAleat = nbvar
            let nbcas
            for (i = 1; i <= nbvar; i++) {
              nbcas = sq.mtgAppLecteur.valueOf('mtg32svg', 'nbcas' + i)
              nb = Math.max(nbrep, nbcas)
              ar = []
              for (j = 0; j < nb; j++) ar.push(j % nbcas)
              sq['tab' + i] = []
              for (k = 0; k < nbrep; k++) {
                tir = Math.floor(Math.random() * ar.length)
                sq['tab' + i].push(ar[tir])
                ar.splice(tir, 1)
              }
              sq.mtgAppLecteur.giveFormula2('mtg32svg', 'r' + i, sq['tab' + i][0])
            }
          } else sq.aleat = false

          if (sq.param !== undefined) {
            for (i = 0; i < ch.length; i++) {
              car = ch.charAt(i)
              if (sq.param.indexOf(car) !== -1) {
                par = parcours.donneesSection[car]
                if (par !== 'random') sq.mtgAppLecteur.giveFormula2('mtg32svg', car, par)
              }
            }
          }

          sq.nbEssais = parseInt(parcours.donneesSection.nbEssais)
          sq.simplificationMaximale = parcours.donneesSection.simplificationMaximale
          if (sq.simplificationMaximale === undefined) sq.simplificationMaximale = false

          sq.mtgAppLecteur.calculateAndDisplayAll(true)
          const nbParam = parseInt(sq.numero)
          const t = ['a', 'b', 'c', 'd']
          sq.parametres = {}
          for (i = 0; i < nbParam; i++) {
            code = sq.mtgAppLecteur.getLatexCode('mtg32svg', i)
            sq.parametres[t[i]] = code
          }
          sq.parametres.e = sq.nbEssais
          let st = sq.consigne1 + sq.consigne2 + ((sq.nbEssais === 1) ? sq.consigne5 : sq.consigne4)
          if (sq.simplificationMaximale) st += sq.consigne3
          j3pMathsAjouteDans('enonce', { id: 'texte', content: st, parametres: sq.parametres })
          j3pMathsAjouteDans('debut', { id: 'debutquestion', content: sq.entete, parametres: sq.parametres })
          j3pMathsAjouteDans('fin', { id: 'finquestion', content: sq.fin, parametres: sq.parametres })

          sq.indicationfaute = parcours.donneesSection.indicationfaute // true si indication quand certaines fautes sont faites et appui sur Entrée

          // $("#expressioninputmq1").focus();
          parcours.finEnonce()
        },
        // failure
        (error) => {
          j3pShowError(error, { message: 'mathgraph n’est pas correctement chargé', mustNotify: true })
        })
      // plantage dans le code de success
      .catch(j3pShowError)
  }

  function initDom () {
    parcours.typederreurs = [0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0]

    if (sq.titre) parcours.afficheTitre(sq.titre)

    j3pDiv(parcours.zones.MG, { id: 'conteneur', contenu: '', style: parcours.styles.etendre('toutpetit.enonce', { padding: '2px' }) })
    j3pDiv('conteneur', 'enonce', '')
    j3pDiv('conteneur', {
      id: 'formules',
      contenu: '',
      style: parcours.styles.petit.enonce
    }) // Contient le formules entrées par l’élève
    j3pDiv('conteneur', 'info', '')
    const nbe = parcours.donneesSection.nbEssais
    const nbc = parcours.donneesSection.nbchances
    if ((nbe === 1) && (nbc === 1)) j3pAffiche('info', 'texteinfo', 'Il reste une validation.', { style: { color: '#7F007F' } })
    else j3pAffiche('info', 'texteinfo', 'Il reste ' + nbe + ' essai(s) et ' + nbc + ' validation(s).', { style: { color: '#7F007F' } })
    j3pDiv('conteneur', 'conteneurbouton', '')
    j3pAjouteBouton('conteneurbouton', recopierReponse, { id: 'boutonrecopier', className: 'MepBoutonsRepere', value: 'Recopier réponse précédente' })
    j3pElement('boutonrecopier').style.display = 'none'

    j3pDiv('conteneur', 'editeur', '') // Le div qui contiendra l’éditeur MathQuill
    $('#editeur').css('padding-top', '10px') // Pour laisser un peu de place au-dessus de l’éditeur
    j3pAffiche('editeur', 'debut', '', {})
    j3pAffiche('editeur', 'expression', '&1&', { inputmq1: {} })
    j3pAffiche('editeur', 'fin', '', {})
    if (sq.charset !== '') j3pRestriction('expressioninputmq1', sq.charset)
    j3pDiv('conteneur', 'boutonsmathquill', '')
    $('#boutonsmathquill').css('display', 'inline-block')
    $('#boutonsmathquill').css('padding-top', '10px') // Pour laisser un peu de place au-dessus des boutons
    const liste = []
    if (sq.btnFrac) liste.push('fraction')
    if (sq.btnPuis) liste.push('puissance')
    if (sq.btnRac) liste.push('racine')
    if (sq.btnExp) liste.push('exp')
    if (sq.btnLn) liste.push('ln')
    if (sq.btnPi) liste.push('pi')
    if (sq.ln) liste.push('ln')
    j3pPaletteMathquill('boutonsmathquill', 'expressioninputmq1', { liste })
    j3pElement('expressioninputmq1').onkeyup = onkeyup

    j3pDiv('conteneur', 'divmtg32', '')
    j3pCreeSVG('divmtg32', {
      id: 'mtg32svg',
      width: 700,
      height: 520
    })

    initMtg()
    // OBLIGATOIRE
    parcours.cacheBoutonSuite()

    j3pDiv(parcours.zones.MD, {
      id: 'correction',
      contenu: '',
      coord: [20, 150],
      style: parcours.styles.petit.correction
    })
    j3pFocus('expressioninputmq1')
  }

  function _Donnees () {
    // Nombre de répétitions de l’exercice
    this.nbrepetitions = 1
    this.nbetapes = 1
    this.nbitems = this.nbetapes * this.nbrepetitions

    this.ex = 'College_Reduction_Deg1'
    // Si indication != "" alors un lien "Indication" s’affiche dans la zone Inférieure Gauche (this.zones.IG
    // une indication<br>de deux lignes...";
    this.indication = ''
    // Attention : Le paramètre k n’est pas modifiable par l’utilisateur. Il correspond au choix d’une formule au hasard
    // et doit être laissé à random
    const st = 'abcdefpqk'
    for (let i = 0; i < st.length; i++) this[st.charAt(i)] = 'random'
    this.nbEssais = 6
    this.indicationfaute = true
    this.simplificationMaximale = false

    // pour le cas de presentation1bis
    // En effet pas d’indication possible pour cette présentation
    // this.surchargeindication = false;

    // Nombre de chances dont dispose l’él§ve pour répondre à la question
    this.nbchances = 1

    this.structure = 'presentation1bis'//  || "presentation2" || "presentation3"  || "presentation1bis"

    this.textes = {
    }
    this.titre = 'Titre de l’activité'

    this.pe = 0
  }

  let ch, nbe, nbc
  switch (this.etat) {
    case 'enonce':
      // code exécuté au lancement de la section
      if (this.debutDeLaSection) {
        this.donneesSection = new _Donnees()
        // Construction de la page
        parcours.construitStructurePage(parcours.donneesSection.structure)

        this.surcharge()
        this.validOnEnter = false // ex donneesSection.touche_entree
        if (parcours.donneesSection.nbchances > parcours.donneesSection.nbEssais) parcours.donneesSection.nbchances = parcours.donneesSection.nbEssais

        this.stockage = [0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0]
        // faut gérer sous-dossier ou pas…
        const sep = this.donneesSection.ex.includes('/') ? '-' : '/'
        j3pImporteAnnexe('squelettes-mtg32' + sep + this.donneesSection.ex + '.js').then(function (datasSection) {
          // console.log('initsection', datasSection)
          // Attention, le this devient window...
          sq.txt = datasSection.txt
          sq.entete = datasSection.entete
          // FIXME squelettes-mtg32/College_Reduction_Deg1.js n’exporte pas de propriété fin
          sq.fin = datasSection.fin || ''
          sq.symbexact = datasSection.symbexact
          sq.symbnonexact = datasSection.symbnonexact
          sq.numero = datasSection.numero // Le nombre de paramètres LaTeX dans le texte
          if (datasSection.btnPuis !== undefined) sq.btnPuis = datasSection.btnPuis; else sq.btnPuis = true
          if (datasSection.btnFrac !== undefined) sq.btnFrac = datasSection.btnFrac; else sq.btnFrac = true
          if (datasSection.btnPi !== undefined) sq.btnPi = datasSection.btnPi; else sq.btnPi = false
          if (datasSection.btnRac !== undefined) sq.btnRac = datasSection.btnRac; else sq.btnRac = false
          if (datasSection.btnExp !== undefined) sq.btnExp = datasSection.btnExp; else sq.btnExp = false
          if (datasSection.btnLn !== undefined) sq.btnLn = datasSection.btnLn; else sq.btnLn = false
          let s = datasSection.consigne1
          sq.consigne1 = (s !== undefined) ? s : ''
          s = datasSection.consigne2
          sq.consigne2 = (s !== undefined) ? s : ''
          s = datasSection.consigne3
          sq.consigne3 = (s !== undefined) ? s : ''
          s = datasSection.consigne4
          sq.consigne4 = (s !== undefined) ? s : ''
          s = datasSection.consigne5
          sq.consigne5 = (s !== undefined) ? s : ''
          sq.titre = datasSection.titre
          sq.param = datasSection.param // Chaine contenant les paramètres autorisés
          s = datasSection.charset // Le set de caractères utilisés dans l’éditeur
          sq.charset = (s !== undefined) ? s : ''
          initDom()
        }).catch(error => {
          console.error(error)
          j3pShowError('Impossible de charger les données de cette exercice')
        })
      } else {
        if ((this.donneesSection.structure === 'presentation1') || (this.donneesSection.structure === 'presentation1bis')) {
          j3pEmpty(this.zonesElts.MD)
        }
        j3pDiv(this.zones.MD, {
          id: 'correction',
          contenu: '',
          coord: [20, 150],
          style: this.styles.petit.correction
        })

        $('#editeur').css('display', 'block')
        $('#boutonsmathquill').css('display', 'inline-block')
        // On vide le contenu de l’éditeur MathQuill et on lui donne le focus.
        $('#expressioninputmq1').mathquill('latex', ' ').focus()

        sq.mtgAppLecteur.removeDoc('mtg32svg')
        sq.mtgAppLecteur.addDoc('mtg32svg', sq.txt, true)

        if (sq.aleat) {
          for (let i = 1; i <= sq.nbParamAleat; i++) {
            sq.mtgAppLecteur.giveFormula2('mtg32svg', 'r' + i, sq['tab' + i][this.questionCourante - 1])
          }
        }
        if (sq.param !== undefined) {
          ch = 'abcdefpqk'
          for (let i = 0; i < ch.length; i++) {
            const car = ch.charAt(i)
            if (sq.param.indexOf(car) !== -1) {
              const par = parcours.donneesSection[car]
              if (par !== 'random') sq.mtgAppLecteur.giveFormula2('mtg32svg', car, par)
            }
          }
        }

        sq.mtgAppLecteur.calculate('mtg32svg', true) // true pour que les calculs alèatoires soient réinitialisés
        j3pElement('enonce').innerHTML = ''
        j3pElement('formules').innerHTML = ''
        j3pElement('debut').innerHTML = ''
        j3pElement('fin').innerHTML = ''
        //        j3pElement("info").innerHTML = "";
        sq.nbexp = 0
        sq.reponse = -1
        const nbParam = parseInt(sq.numero)
        const t = ['a', 'b', 'c', 'd']
        sq.parametres = {}
        let code
        for (let i = 0; i < nbParam; i++) {
          code = sq.mtgAppLecteur.getLatexCode('mtg32svg', i)
          sq.parametres[t[i]] = code
        }
        sq.parametres.e = sq.nbEssais
        let st = sq.consigne1 + sq.consigne2 + ((sq.nbEssais === 1) ? sq.consigne5 : sq.consigne4)
        if (sq.simplificationMaximale) st += sq.consigne3
        j3pMathsAjouteDans('enonce', { id: 'texte', content: st, parametres: sq.parametres })
        j3pMathsAjouteDans('debut', { id: 'debutquestion', content: sq.entete, parametres: sq.parametres })
        j3pMathsAjouteDans('fin', { id: 'finquestion', content: sq.fin, parametres: sq.parametres })

        nbe = parcours.donneesSection.nbEssais - sq.nbexp
        nbc = parcours.donneesSection.nbchances - parcours.essaiCourant
        if ((nbe === 1) && (nbc === 1)) j3pElement('texteinfo').innerHTML = 'Il reste une validation'
        else j3pElement('texteinfo').innerHTML = 'Il reste ' + nbe + ' essai(s) et ' + nbc + ' validation(s).'
        j3pElement('info').style.display = 'block'
        sq.mtgAppLecteur.display('mtg32svg') // Pour tout réafficher
        this.finEnonce()
      }

      break // case "enonce":

    case 'correction': {
      let bilanreponse = ''
      // On regarde si le champ d’édition est rempli même si l’utilisateur
      // n’a pas appuyé sur la touche Entrée.
      ch = j3pValeurde('expressioninputmq1')
      if (ch !== '') {
        const repcalcul = j3pMathquillXcas(ch)
        const valide = sq.mtgAppLecteur.syntaxValidation('mtg32svg', 'rep', repcalcul, true)
        if (valide) validation(this, false)
        else bilanreponse = 'incorrect'
      } else {
        if (sq.nbexp === 0) bilanreponse = 'incorrect'
      }
      const res1 = sq.reponse
      if (bilanreponse !== 'incorrect') {
        if (res1 === 1) {
          bilanreponse = 'exact'
        } else {
          if (res1 === 2) { bilanreponse = 'exactpasfini' } else { bilanreponse = 'erreur' }
        }
      }
      // A cause de la limite de temps :
      if (this.isElapsed) { // limite de temps
        this._stopTimer()
        j3pElement('correction').style.color = this.styles.cfaux
        j3pElement('boutonrecopier').style.display = 'none'
        j3pElement('info').style.display = 'none'
        if (sq.indicationfaute) {
          sq.mtgAppLecteur.giveFormula2('mtg32svg', 'faute', '0')
          sq.mtgAppLecteur.calculate('mtg32svg', false)
          sq.mtgAppLecteur.executeMacro('mtg32svg', 'masquerFaute')
        }
        $('#editeur').css('display', 'none')
        $('#boutonsmathquill').css('display', 'none')
        sq.mtgAppLecteur.executeMacro('mtg32svg', 'solution')
        j3pElement('correction').innerHTML = tempsDepasse
        j3pElement('correction').innerHTML += '<br>La solution se trouve ci-contre.'

        this.typederreurs[10]++
        this.cacheBoutonValider()
        this.etat = 'navigation'
        this.sectionCourante()
      } else {
        if (bilanreponse === 'incorrect') {
          j3pElement('correction').style.color = this.styles.cfaux
          j3pElement('correction').innerHTML = 'Réponse incorrecte'
          marqueEditeurPourErreur()
          $('#expressioninputmq1').focus()
          this.afficheBoutonValider()
        } else {
          // Une réponse a été saisie
          if (bilanreponse === 'exact') {
            // Bonne réponse
            this._stopTimer()
            sq.mtgAppLecteur.setActive('mtg32svg', false)
            this.score++
            j3pElement('correction').style.color = this.styles.cbien
            j3pElement('correction').innerHTML = cBien
            this.typederreurs[0]++
            this.cacheBoutonValider()
            j3pElement('boutonrecopier').style.display = 'none'
            j3pElement('info').style.display = 'none'
            if (sq.indicationfaute) { sq.mtgAppLecteur.executeMacro('mtg32svg', 'masquerFaute') }
            sq.mtgAppLecteur.executeMacro('mtg32svg', 'solution')
            $('#editeur').css('display', 'none')
            $('#boutonsmathquill').css('display', 'none')
            this.etat = 'navigation'
            this.sectionCourante()
          } else {
            j3pElement('correction').style.color = this.styles.cfaux
            if (bilanreponse === 'exactpasfini') {
              j3pElement('correction').innerHTML = 'Le calcul est bon mais pas écrit sous la forme demandée.'
            } else {
              j3pElement('correction').innerHTML = cFaux
            }
            j3pElement('info').innerHTML = ''
            nbe = parcours.donneesSection.nbEssais - sq.nbexp
            nbc = parcours.donneesSection.nbchances - parcours.essaiCourant
            j3pAffiche('info', 'texteinfo', 'Il reste ' + nbe + ' essai(s) et ' + nbc + ' validation(s).', {
              styletexte: {
                couleur: '#7F007F'
              }
            })
            // On donne le focus à l’éditeur MathQuill
            $('#expressioninputmq1').focus()
            // S’il y a plus de deux chances,on appelle à nouveau le bouton Valider

            if ((sq.nbexp < sq.nbEssais) &&
              (this.essaiCourant < this.donneesSection.nbchances)) {
              j3pElement('correction').innerHTML += '<br>' + '<br>' + essaieEncore
              this.typederreurs[1]++
              // indication éventuelle ici
              // parcours.Sectionsquelettemtg32_XJ.nbExp += 1;
              sq.mtgAppLecteur.setActive('mtg32svg', true)
              sq.mtgAppLecteur.activateField('mtg32svg', 'rep')
            } else {
              // Erreur au nème essai
              this._stopTimer()
              sq.mtgAppLecteur.setActive('mtg32svg', false)
              this.cacheBoutonValider()
              j3pElement('boutonrecopier').style.display = 'none'
              j3pElement('info').style.display = 'none'
              if (sq.indicationfaute) { sq.mtgAppLecteur.executeMacro('mtg32svg', 'masquerFaute') }
              sq.mtgAppLecteur.executeMacro('mtg32svg', 'solution')
              $('#editeur').css('display', 'none')
              $('#boutonsmathquill').css('display', 'none')

              // j3pElement("correction").innerHTML+='<br>' + regardeCorrection;
              j3pElement('correction').innerHTML += '<br><br>La solution se trouve ci-contre.'
              this.typederreurs[2]++
              this.etat = 'navigation'
              this.sectionCourante()
            }
          }
        }
      }
      // Obligatoire
      this.finCorrection()
      break // case "correction":
    }

    case 'navigation':
      if (this.sectionTerminee()) {
        // On détermine la phrase d’état renvoyée par la section
        this.parcours.pe = this.score / this.donneesSection.nbitems
        this.afficheBoutonSectionSuivante()
        this.focus('sectioncontinuer')
      } else {
        this.etat = 'enonce'
        this.afficheBoutonSuite()
        this.focus('boutoncontinuer')
      }
      // Obligatoire
      this.finNavigation()

      break // case "navigation":
  }
}
