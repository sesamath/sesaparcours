import $ from 'jquery'

import { j3pAddContent, j3pAddElt, j3pAjouteBouton, j3pDiv, j3pElement, j3pEmpty, j3pImporteAnnexe, j3pPaletteMathquill, j3pShowError } from 'src/legacy/core/functions'
import { MathfieldElement, renderMathInDocument } from 'mathlive'
import { getMtgApp } from 'src/lib/outils/mathgraph'
import { j3pCreeSVG } from 'src/legacy/core/functionsSvg'
import { cleanLatexForMl, mathliveRestrict } from 'src/lib/outils/mathlive/utils'
import { afficheMathliveDans, getMathliveValue } from 'src/lib/outils/mathlive/display'
import { unLatexify } from 'src/lib/mathquill/functions'
import { setMathliveCss } from 'src/lib/outils/mathlive/mathlivecss'
import { resetKeyboardPosition } from 'src/lib/widgets/mlVirtualKeyboard/VirtualKeyboard'
import textesGeneriques from 'src/lib/core/textes'
import { focusIfExists } from 'src/lib/utils/dom/main'

const { cBien, cFaux, essaieEncore, tempsDepasse } = textesGeneriques

/*
 Yves Biton
 Juillet 2014. Revu en 2023 pour passage à mathlive au lieu de Mathquill
 */

// on définit le dossier des fonts mathlive (il faut le faire très tôt,
// ici c’est bon avec pnpm start mais trop tard pour un build (cf src/lib/player/index.js de la branche draftPlayer)
if (MathfieldElement) { // undefined dans les tests avec happy-dom
  MathfieldElement.fontsDirectory = '/static/mathlive/fonts'
  MathfieldElement.soundsDirectory = '/static/mathlive/sounds'
}

export const params = {
  outils: [],
  /**
   * Ce qui est paramétrable dans l’éditeur de graphe
   * @type {LegacySectionParamsParametres}
   */
  parametres: [
    ['nbrepetitions', 1, 'entier', 'aucun'],
    ['limite', 0, 'entier', 'Temps disponible par répétition, en secondes'],
    ['nbchances', 1, 'entier', 'Nombre d’erreurs possibles (pour tenter de les corriger)'],
    ['nbEssais', 6, 'entier', 'Nombre d’essais maximum autorisés'],
    ['simplifier', true, 'boolean', 'true si on demande la solution simplifiée au maximum'],
    ['a', 'random', 'string', 'Valeur de a'],
    ['b', 'random', 'string', 'Valeur de b'],
    ['c', 'random', 'string', 'Valeur de c'],
    ['d', 'random', 'string', 'Valeur de d'],
    ['e', 'random', 'string', 'Valeur de e'],
    ['f', 'random', 'string', 'Valeur de f'],
    ['g', 'random', 'string', 'Valeur de g'],
    ['h', 'random', 'string', 'Valeur de h'],
    ['j', 'random', 'string', 'Valeur de j'],
    ['k', 'random', 'string', 'Valeur de k'],
    ['l', 'random', 'string', 'Valeur de l'],
    ['m', 'random', 'string', 'Valeur de m'],
    ['n', 'random', 'string', 'Valeur de n'],
    ['p', 'random', 'string', 'Valeur de p'],
    ['q', 'random', 'string', 'Valeur de q'],
    ['r', 'random', 'string', 'Valeur de r'],
    ['ex', 'terminale_expert/Complexes_Conjugue_1', 'string', 'Nom du fichier annexe (xxx => squelettes-mtg32/xxx.js et xxx/yyy => squelettes-mtg32-xxx/yyy.js)']
  ]
}

const inputId = 'expressioninputmq1'

/**
 * section squelettemtg32_Eq_Param_MathQuill
 * @this {Parcours}
 */
export default function main () {
  // cette variable globale à la section sert à rendre global des propriétés entre les ≠ callback
  // (dont le this n’est plus le parcours courant)
  const parcours = this
  const sq = this.storage
  const textes = {
    ilReste: 'Il reste ',
    essai: 'essai',
    essaisEt: 'essais et',
    validation: 'validation',
    validations: 'validations',
    une: 'une',
    signeEgalNec: 'Il faut utiliser le signe =',
    uneSeuleEg: 'Il ne faut qu’une seule égalité par réponse',
    fauteSyn: 'Faute de syntaxe',
    solutionIci: '\nLa solution se trouve ci-contre.',
    repIllis: 'Réponse illisible',
    bienMais: 'C’est bien  mais on pouvait simplifier la solution.',
    exactPasFini: 'La solution est bonne mais pas écrite sous la forme demandée.',
    nonRes: 'L’équation n’est pas résolue.'
  }

  /**
     * Fonction extrayant d’un affichage LaTeX de la figure les lignes de texte et les transformant
     * en une chaîne affichable par MathQuill
     * @param tagLatex : le tag de l’affichage LaTeX de la figure
     * @return {string}
     */
  function extraitDeLatexPourMathlive (tagLatex) {
    const list = sq.mtgAppLecteur.getList('mtg32svg')
    const latex = list.getByTag(tagLatex, true)
    if (latex === null || latex.className !== 'CLatex') return ''
    return cleanLatexForMl(latex.chaineLatex, sq.charset)
  }

  // nos fonctions
  function onkeyup (ev, parcours) {
    if (sq.marked) demarqueEditeurPourErreur()
    // touche entrée
    if (ev.keyCode === 13) {
      const rep = getMathliveValue(inputId)
      if (rep !== '') {
        const chcalcul = traiteMathlive(rep)
        const valide = sq.mtgAppLecteur.syntaxValidation('mtg32svg', 'rep', chcalcul, true)
        if (valide) {
          if (chcalcul.indexOf('=') === -1) {
            focusIfExists(inputId)
            marqueEditeurPourErreur()
            j3pElement('correction').style.color = parcours.styles.cfaux
            j3pAddContent('correction', textes.signeEgalNec, { replace: true })
          } else {
            if (chcalcul.lastIndexOf('=') !== chcalcul.indexOf('=')) {
              marqueEditeurPourErreur()
              j3pElement('correction').style.color = parcours.styles.cfaux
              j3pAddContent('correction', textes.uneSeuleEg, { replace: true })
            } else {
              validation(parcours)
            }
          }
        } else {
          marqueEditeurPourErreur()
          j3pElement('correction').style.color = parcours.styles.cfaux
          focusIfExists(inputId)
          j3pAddContent('correction', textes.fauteSyn, { replace: true })
        }
      } else {
        focusIfExists(inputId)
      }
    }
  } // onkeyup

  function marqueEditeurPourErreur () {
    sq.marked = true
    const mf = j3pElement(inputId)
    if (!mf) return
    mf.style.backgroundColor = '#FF9999'
    mf.demarqueErreur = () => {
      if (sq.marked) {
        demarqueEditeurPourErreur()
      }
    }
  }

  function demarqueEditeurPourErreur () {
    sq.marked = false
    j3pEmpty('correction')
    const mf = j3pElement(inputId)
    if (!mf) return
    mf.style.backgroundColor = 'white'
  }

  /**
     * Fonction affichant le nombre d’essais restants nbe et le nombre de chances restantes nbc
     */
  function afficheNombreEssaisRestants () {
    j3pEmpty('info')
    const nbe = sq.nbEssais - sq.nbexp
    const nbc = Math.min(sq.nbchances - sq.numEssai + 1, nbe)
    if ((nbe === 1) && (nbc === 1)) {
      afficheMathliveDans('info', 'texteinfo', textes.ilReste + 'une' + ' ' + textes.validation, { style: { color: '#7F007F' } })
    } else {
      const valid = nbc === 1 ? ' ' + textes.validation + '.' : ' ' + textes.validations + '.'
      afficheMathliveDans('info', 'texteinfo', textes.ilReste + ' ' + nbe + ' ' + textes.essaisEt + ' ' + nbc + valid, { style: { color: '#7F007F' } })
    }
  }

  // Cette fonction renvoie true si parcours.sectionCourante() a dû être appelé et false sinon
  function validation (parcours, continuer) {
    if (arguments.length <= 1) continuer = true
    if (sq.nbexp === 0) {
      j3pElement('boutonrecopier').style.display = 'block'
    }
    sq.rep = getMathliveValue(inputId)
    const chcalcul = traiteMathlive(sq.rep)
    sq.mtgAppLecteur.giveFormula2('mtg32svg', 'rep', chcalcul)
    j3pEmpty('correction')
    sq.mtgAppLecteur.calculate('mtg32svg', false)
    sq.reponse = sq.mtgAppLecteur.valueOf('mtg32svg', 'reponse')
    const idrep = 'exp' + sq.nbexp
    sq.nbexp++
    const chdeb = sq.nbexp === 1 ? '' : '<br>'
    if (sq.reponse >= 1) {
      afficheMathliveDans('formules', idrep, chdeb + '$' + sq.entete + ' ' + sq.symbexact +
                ' ' + sq.rep + '$', {
        style: {
          color: '#0000FF'
        }
      })
    } else {
      afficheMathliveDans('formules', idrep, chdeb + '$' + sq.entete + ' ' + sq.symbnonexact +
                ' ' + sq.rep + '$', {
        style: {
          color: '#FF0000'
        }
      })
    }

    j3pElement(inputId).value = ''
    resetKeyboardPosition()

    // Si le nombre maximum d’essais est atteint on passe en mode correction
    if (sq.nbexp >= sq.nbEssais) {
      sq.mtgAppLecteur.setActive('mtg32svg', false)
      if (continuer) {
        parcours.etat = 'correction'
        parcours.sectionCourante()
        $('#editeur').css('display', 'none')
        $('#boutonsmathquill').css('display', 'none')
        return true
      }
    } else {
      afficheNombreEssaisRestants()
    }
    return false
  } // validation

  function recopierReponse () {
    demarqueEditeurPourErreur()
    const mf = j3pElement(inputId)
    if (!mf) return
    mf.value = sq.rep
    focusIfExists(inputId)
  }

  // charge mtgAppLecteur puis appelle enonceInitFirst
  function loadMtg () {
    // on charge mathgraph
    const mtgOptions = {
      // player only, sans figure initiale ni svg, on gère ça dans initMtg et modif_fig
      loadCoreWithMathJax: true
    }
    getMtgApp('mtg32svg', {}, mtgOptions).then((mtgAppLecteur) => {
      sq.mtgAppLecteur = mtgAppLecteur
      enonceInitFirst()
    }).catch(j3pShowError)
  }

  // ce code est celui qu’il y avait avec l’outil mtg32 dans le if (this.debutDeLaSection)
  // il appelle enonceMain à la fin, et initDom dans la callback de chargement du fichier annexe (et les deux vont modifier le svg)
  function enonceInitFirst () {
    // Construction de la page
    parcours.construitStructurePage('presentation1bis')
    parcours.validOnEnter = false // ex donneesSection.touche_entree
    sq.nbexp = 0
    sq.reponse = -1
    // compteur. Si   numEssai > nbchances, on corrige
    sq.numEssai = 1
    sq.simplifier = parcours.donneesSection.simplifier
    sq.nbEssais = parseInt(parcours.donneesSection.nbEssais)
    sq.nbchances = parseInt(parcours.donneesSection.nbchances)
    if (sq.nbchances > sq.nbEssais) sq.nbchances = sq.nbEssais
    // on charge le fichier annexe et on traite ses données
    // faut gérer sous-dossier ou pas…
    const sep = parcours.donneesSection.ex.includes('/') ? '-' : '/'
    j3pImporteAnnexe('squelettes-mtg32' + sep + parcours.donneesSection.ex + '.js').then(function (datasSection) {
      // Attention, ici le this est window
      // datasSection est l’objet passé à la fct j3pcallback dans le js qu’on vient de charger
      sq.txt = datasSection.txt
      sq.entete = datasSection.entete
      sq.symbexact = datasSection.symbexact
      sq.symbnonexact = datasSection.symbnonexact
      sq.numero = datasSection.numero // Le nombre de paramètres LaTeX dans le texte
      // Le paramètre suivant est true si on veut un éditeur avec une grosse police de caractères
      sq.bigSize = Boolean(datasSection.bigSize) ?? false
      sq.width = Number(datasSection.width ?? 780)
      sq.height = Number(datasSection.height ?? 900)
      // On crée une liste formée des id des boutons autorisés
      const listeBoutons = []
      // On commence par les boutons qui sont activés par défaut
      const tab1 = ['puissance', 'fraction']
            ;['btnPuis', 'btnFrac'].forEach((p, i) => {
        const b = Boolean(datasSection[p] ?? true)
        if (b) listeBoutons.push(tab1[i])
      })
      // Puis les boutons désactivés par défaut
      const tab2 = ['pi', 'racine', 'exp', 'ln', 'sin', 'cos', 'tan', 'abs', 'conj']
            ;['btnPi', 'btnRac', 'btnExp', 'btnLn', 'btnSin', 'btnCos', 'btnTan', 'btnAbs', 'btnConj'].forEach((p, i) => {
        const b = Boolean(datasSection[p] ?? false)
        if (b) listeBoutons.push(tab2[i])
      })
      sq.listeBoutons = listeBoutons
      sq.boutonsMathQuill = listeBoutons.length !== 0
      // string vide par défaut
      ;['consigne1', 'consigne2', 'consigne3', 'consigne4', 'charset'].forEach((p) => {
        sq[p] = datasSection[p] ?? ''
      })
      sq.titre = datasSection.titre
      sq.param = datasSection.param // Chaine contenant les paramètres autorisés
      initDom(parcours)
    }).catch(error => {
      console.error(error)
      j3pShowError('Impossible de charger les données de cette exercice')
    })
  }

  function initDom () {
    renderMathInDocument({ renderAccessibleContent: '' })
    if (sq.titre) parcours.afficheTitre(sq.titre)

    j3pDiv(parcours.zones.MG, {
      id: 'conteneur',
      contenu: '',
      style: parcours.styles.etendre('toutpetit.enonce', { padding: '6px' })
    })
    j3pDiv('conteneur', 'enonce', '')
    j3pDiv('conteneur', { id: 'formules', contenu: '', style: parcours.styles.petit.enonce }) // Contient le formules entrées par l’élève
    j3pDiv('conteneur', 'info', '')
    afficheNombreEssaisRestants()
    j3pDiv('conteneur', 'conteneurbouton', '')
    j3pAjouteBouton('conteneurbouton', 'boutonrecopier', 'MepBoutonsRepere', 'Recopier réponse précédente',
      recopierReponse)
    j3pElement('boutonrecopier').style.display = 'none'

    j3pDiv('conteneur', 'editeur', '') // Le div qui contiendra l’éditeur MathQuill
    if (sq.bigSize) $('#editeur').css('font-size', '30px')
    $('#editeur').css('padding-top', '10px') // Pour laisser un peu de place au-dessus de l’éditeur
    const options = {
      charset: sq.charset,
      listeBoutons: sq.listeBoutons,
      style: { fontSize: '24px' }
    }
    afficheMathliveDans('editeur', 'expression', 'Proposition : &1&', options)
    mathliveRestrict(inputId, sq.charset)
    j3pDiv('conteneur', 'boutonsmathquill', '')
    $('#boutonsmathquill').css('display', 'inline-block')
    $('#boutonsmathquill').css('padding-top', '10px') // Pour laisser un peu de place au-dessus des boutons
    if (sq.boutonsMathQuill) j3pPaletteMathquill('boutonsmathquill', inputId, { liste: sq.listeBoutons })
    j3pAddElt('conteneur', 'div', '', { id: 'divSolution' })
    $('#divSolution').css('display', 'none')
    j3pDiv('conteneur', 'divmtg32', '')
    j3pCreeSVG('divmtg32', {
      id: 'mtg32svg',
      width: sq.width,
      height: sq.height
    })
    const style = parcours.styles.petit.correction
    style.marginTop = '1.5em'
    style.marginLeft = '0.5em'
    j3pAddElt(parcours.zonesElts.MD, 'div', '', { id: 'correction', style })
    initMtg()
  } // initDom

  function traiteMathlive (ch) {
    // Mathlive ne met pas d’accolades pour les fractions dont le numérateur et le dénominateur sont des entiers
    ch = ch.replace(/\\frac(\d)(\d)/g, '\\frac{$1' + '}{' + '$2}')
    // Ni pour les racines carrées d’un entier
    ch = ch.replace(/\\sqrt(\d)/g, '\\sqrt{$1}')
    // Pour les puissances dont l’exposant est un chiffre, Mathlive ne met pas d’accolades
    ch = ch.replace(/\^([^{])/g, '^{$1}')
    ch = unLatexify(ch)
    // mathlive peut mettre des {} vides
    ch = ch.replace(/\\{\\}/g, '')
    ch = ch.replace(/\^(\d)(\d)/g, '^$1*$2') // On peut avoir par exemple 2^35^4 pour 2^3*5^4
    return sq.mtgAppLecteur.addImplicitMult('mtg32svg', ch)// Traitement des multiplications implicites
  }

  /**
     * Fonction affichant la solution s’il existe un affichage LaTeX de tag solution
     * @param{boolean} bexact true si l’élève a entré la bonne réponse, false sinon
     */
  function afficheSolution (bexact) {
    $('#divSolution').css('display', 'block').html('')
    const ch = extraitDeLatexPourMathlive('solution')
    if (ch !== '') {
      afficheMathliveDans('divSolution', 'solution', ch, { style: { color: bexact ? 'green' : 'blue' } })
    }
  }

  function cacheSolution () {
    $('#divSolution').css('display', 'none').html('')
  }

  function initMtg () {
    setMathliveCss('MepMG')
    let code, par, car, nbrep, ar, tir, nb
    sq.mtgAppLecteur.removeAllDoc()
    sq.mtgAppLecteur.addDoc('mtg32svg', sq.txt, true)
    sq.mtgAppLecteur.calculate('mtg32svg', false)
    // sq.param est une chaîne contenant les noms des paramètres autorisés
    const ch = 'abcdefghjklmnpqr'
    const nbvar = sq.mtgAppLecteur.valueOf('mtg32svg', 'nbvar')
    if (nbvar !== -1) {
      nbrep = parcours.donneesSection.nbrepetitions
      sq.aleat = true
      sq.nbParamAleat = nbvar
      for (let i = 1; i <= nbvar; i++) {
        const nbcas = sq.mtgAppLecteur.valueOf('mtg32svg', 'nbcas' + i)
        nb = Math.max(nbrep, nbcas)
        ar = []
        for (let j = 0; j < nb; j++) ar.push(j % nbcas)
        sq['tab' + i] = []
        for (let k = 0; k < nbrep; k++) {
          tir = Math.floor(Math.random() * ar.length)
          sq['tab' + i].push(ar[tir])
          ar.splice(tir, 1)
        }
        sq.mtgAppLecteur.giveFormula2('mtg32svg', 'r' + i, sq['tab' + i][0])
      }
    } else {
      sq.aleat = false
    }

    if (sq.param !== undefined) {
      for (let i = 0; i < ch.length; i++) {
        car = ch.charAt(i)
        if (sq.param.indexOf(car) !== -1) {
          par = parcours.donneesSection[car]
          if (par !== 'random') {
            sq.mtgAppLecteur.giveFormula2('mtg32svg', car, par)
          }
        }
      }
    }
    sq.mtgAppLecteur.calculateAndDisplayAll(true)
    const nbParam = parseInt(sq.numero)
    const t = ['a', 'b', 'c', 'd']
    const param = {}
    for (let i = 0; i < nbParam; i++) {
      code = sq.mtgAppLecteur.getLatexCode('mtg32svg', i)
      param[t[i]] = code
    }
    param.e = String(sq.nbEssais)
    const st = (sq.simplifier ? sq.consigne3 : sq.consigne2) + sq.consigne4
    afficheMathliveDans('enonce', 'texte', sq.consigne1 + st, param)
    // $("#expressioninputmq1").focus();
    // Les 3 lignes suivantes déplacées ici pour que sq.mtgAppLecteur soit prêt quand onkeyup est appelé
    j3pElement(inputId).addEventListener('keyup', function (ev) {
      onkeyup(ev, parcours)
    })
    if (sq.charset !== '') {
      mathliveRestrict(inputId, sq.charset)
    }
    focusIfExists(inputId)
    parcours.finEnonce()
  } // initMtg

  function enonceInitAfter () {
    j3pEmpty('correction')
    cacheSolution()
    sq.numEssai = 1
    sq.mtgAppLecteur.removeDoc('mtg32svg')
    $('#editeur').css('display', 'block')
    $('#boutonsmathquill').css('display', 'inline-block')

    // on ajoute la figure sq.txt
    sq.mtgAppLecteur.addDoc('mtg32svg', sq.txt, true)

    if (sq.aleat) {
      for (let i = 1; i <= sq.nbParamAleat; i++) {
        sq.mtgAppLecteur.giveFormula2('mtg32svg', 'r' + i, sq['tab' + i][parcours.questionCourante - 1])
      }
    }
    if (sq.param !== undefined) {
      const ch = 'abcdefghjklmnpqr'
      for (let i = 0; i < ch.length; i++) {
        const car = ch.charAt(i)
        if (sq.param.indexOf(car) !== -1) {
          const par = parcours.donneesSection[car]
          if (par !== 'random') {
            sq.mtgAppLecteur.giveFormula2('mtg32svg', car, par)
          }
        }
      }
    }
    sq.mtgAppLecteur.calculate('mtg32svg', true) // true pour que les calculs alèatoires soient rꪮitialisés
    j3pEmpty('enonce')
    j3pEmpty('formules')
    sq.nbexp = 0
    sq.reponse = -1
    const nbParam = parseInt(sq.numero)
    const t = ['a', 'b', 'c', 'd']
    const param = {}
    for (let i = 0; i < nbParam; i++) {
      const code = sq.mtgAppLecteur.getLatexCode('mtg32svg', i)
      param[t[i]] = code
    }
    param.e = String(sq.nbEssais)
    // Consigne différente suivant qu’on demande une solution simplifiée ou non.
    const st = (sq.simplifier ? sq.consigne3 : sq.consigne2) + sq.consigne4
    param.listeBoutons = sq.listeBoutons
    param.charset = sq.charset // Doit être passé pour l clavier virtuel mathlive
    afficheMathliveDans('enonce', 'texte', sq.consigne1 + st, param)
    sq.mtgAppLecteur.display('mtg32svg') // Pour tout réafficher
    // Pour revenir en haut de page
    try {
      window.location.hash = 'MepMG'
      parcours.zonesElts.MG.scrollIntoView()
    } catch (e) {
      // pas gravev
    }
    afficheNombreEssaisRestants()
    j3pElement('info').style.display = 'block'
    // On vide le contenu de l’éditeur MathQuill et on lui donne le focus.
    j3pElement(inputId).value = ''
    focusIfExists(inputId)
    parcours.finEnonce()
  } // enonceInitAfter

  switch (this.etat) {
    case 'enonce':
      // code exécuté au lancement de la section
      if (this.debutDeLaSection) {
        loadMtg()
      } else {
        enonceInitAfter()
      }
      break // case "enonce":

    case 'correction': {
      let bilanreponse = ''
      let simplifie = false
      // On regarde si le champ d’édition est rempli même si l’utilisateur
      // n’a pas appuyé sur la touche Entrée.
      const ch = getMathliveValue(inputId)
      if (ch.length !== 0) {
        const repcalcul = traiteMathlive(ch)
        const valide = sq.mtgAppLecteur.syntaxValidation('mtg32svg', 'rep', repcalcul, true)
        if (valide) {
          const returnNecessaire = validation(this, false)
          if (returnNecessaire) return
        } else {
          bilanreponse = 'incorrect'
        }
      }
      const res1 = sq.reponse
      if (bilanreponse !== 'incorrect') {
        if (sq.simplifier) { // Si on demande une solution simplifiée
          if (res1 === 1) {
            bilanreponse = 'exact'
            simplifie = true
          } else {
            if (res1 === 2) {
              bilanreponse = 'exactpasfini'
            } else {
              bilanreponse = 'erreur'
            }
          }
        } else {
          if ((res1 === 1) || (res1 === 2)) {
            bilanreponse = 'exact'
            if (res1 === 1) {
              simplifie = true
            }
          } else {
            bilanreponse = 'erreur'
          }
        }
      }
      // A cause de la limite de temps :
      if (this.isElapsed) { // limite de temps
        this._stopTimer()
        j3pElement('correction').style.color = this.styles.cfaux
        j3pElement('boutonrecopier').style.display = 'none'
        j3pElement('info').style.display = 'none'
        afficheSolution(false)
        sq.mtgAppLecteur.executeMacro('mtg32svg', 'solution')
        $('#editeur').css('display', 'none')
        $('#boutonsmathquill').css('display', 'none')

        j3pAddContent('correction', tempsDepasse, { replace: true })
        j3pAddContent('correction', textes.solutionIci)
        return this.finCorrection('navigation', true)
      } else {
        if (bilanreponse === 'incorrect') {
          j3pElement('correction').style.color = this.styles.cfaux
          j3pAddContent('correction', textes.repIllis, { replace: true })
        } else {
          // Une réponse a été saisie
          if (bilanreponse === 'exact') {
            // Bonne réponse
            this._stopTimer()
            sq.mtgAppLecteur.setActive('mtg32svg', false)
            this.score++
            j3pElement('correction').style.color = this.styles.cbien
            if (simplifie) {
              j3pAddContent('correction', cBien, { replace: true })
            } else {
              j3pAddContent('correction', textes.bienMais, { replace: true })
            }
            j3pElement('boutonrecopier').style.display = 'none'
            j3pElement('info').style.display = 'none'
            afficheSolution(true)
            sq.mtgAppLecteur.executeMacro('mtg32svg', 'solution')
            $('#editeur').css('display', 'none')
            $('#boutonsmathquill').css('display', 'none')
            return this.finCorrection('navigation', true)
          }
          j3pElement('correction').style.color = this.styles.cfaux
          sq.numEssai++
          if (bilanreponse === 'exactpasfini') {
            j3pAddContent('correction', textes.exactPasFini, { replace: true })
          } else {
            if (res1 === 3) {
              j3pAddContent('correction', textes.nonRes, { replace: true })
            } else {
              j3pAddContent('correction', cFaux, { replace: true })
            }
          }
          afficheNombreEssaisRestants()

          // S’il y a plus de deux chances,on appelle à nouveau le bouton Valider
          if ((sq.nbexp < sq.nbEssais) && (sq.numEssai <= sq.nbchances)) {
            j3pAddContent('correction', '\n' + '<br>' + essaieEncore)
            sq.mtgAppLecteur.setActive('mtg32svg', true)
            focusIfExists(inputId)
            return this.finCorrection()
          } else {
            // Erreur au nème essai
            j3pElement('boutonrecopier').style.display = 'none'
            j3pElement('info').style.display = 'none'
            afficheSolution(false)
            sq.mtgAppLecteur.executeMacro('mtg32svg', 'solution')
            $('#editeur').css('display', 'none')
            $('#boutonsmathquill').css('display', 'none')
            sq.mtgAppLecteur.setActive('mtg32svg', false)
            j3pAddContent('correction', textes.solutionIci)
            this.finCorrection('navigation', true)
          }
        }
      }
      // Obligatoire
      this.finCorrection()
      break // case "correction":
    } // case 'correction'

    case 'navigation':
      if (!this.sectionTerminee()) {
        this.etat = 'enonce'
      }
      this.finNavigation()
      break // case "navigation":
  }
}
