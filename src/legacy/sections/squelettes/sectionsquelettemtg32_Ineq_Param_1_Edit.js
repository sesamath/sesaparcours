import $ from 'jquery'
import { j3pAddElt, j3pAjouteBouton, j3pDiv, j3pElement, j3pEmpty, j3pImporteAnnexe, j3pPaletteMathquill, j3pShowError } from 'src/legacy/core/functions'
import { getMtgApp } from 'src/lib/outils/mathgraph'
import { j3pCreeSVG } from 'src/legacy/core/functionsSvg'
import { MathfieldElement, renderMathInDocument } from 'mathlive'
import { cleanLatexForMl, mathliveRestrict } from 'src/lib/outils/mathlive/utils'
import { afficheMathliveDans, getMathliveValue } from 'src/lib/outils/mathlive/display'
import { unLatexify } from 'src/lib/mathquill/functions'
import { resetKeyboardPosition } from 'src/lib/widgets/mlVirtualKeyboard/VirtualKeyboard'
import { setMathliveCss } from 'src/lib/outils/mathlive/mathlivecss'
import textesGeneriques from 'src/lib/core/textes'
import { focusIfExists } from 'src/lib/utils/dom/main'

const { cBien, cFaux, essaieEncore, tempsDepasse } = textesGeneriques

/**
 Yves Biton
 Juillet 2019

 http://localhost:8081/?graphe=[1,"squelettemtg32_Ineq_Param_1_Edit",[{pe:">=0",nn:"fin",conclusion:"fin"},{f:"Fact_ax_Carre_Moins_cCarre_Ex1"}]];
 squelettemtg32_Ineq_Param_1_Edit
 Squelette demandant de résoudre une inéquation.
 L’élève valide ses réponses aussi bien en appuyant sur la touche Entrée que sur le bouton OK.
 On peut éventuellement spécifier des paramètres  a,b,c,d,e,f,g,h,j,k,n,n,p.
 Si l’ensemble des solutions contient -∞, la figure doit contenir un calcul nommé moinsInfSol qui vaut 1 et 0 sinon.
 Si l’ensemble des solutions contient +∞, la figure doit contenir un calcul nommé plusInfSol qui vaut 1 et 0 sinon.
 Si l’ensemble des solutions est R la figure doit contenir un calcul nommé toutReelSol qui vaut 1 et 0 sinon.
 Si l’ensemble des solutions est vide la figure doit contenir un calcul nommé vide qui vaut 1 et 0 sinon

 La figure doit comprendre un calcul nommé xTest et un calcul nommé estSolution qui prend pour valeur 1 si xTest est solution
 de l’inéquation et zéro sinon.
 Elle doit contenir une fonction nommée zeroBorne de la variable x qui a comme formule abs(x)<0.00000000001 (10^-11)
 Elle doit contenir une fonction de la variable x nommée rep qui correspond à la réponse proposée par l’élève.
 Elle doit contenir une fonction de la variable x nommée repPourBornes qui correspond à la réponse proposée par l’élève avec un test supplémentaire pour les bornes fermées
 proposées par l’élève.
 Elle doit contenir une fonction nommée repBornesFermees qui correspond à la réponse proposée par l’élève mais avec toutes les bornes fermées
 Elle doit comprendre un calcul nommé estBorne qui vaut 1 si xTest est une borne de l’ensemble des solutions ou 0 sinon.
 Elle doit aussi comprendre un calcul nommé repContientSol qui vaut 1 si l’ensemble des solutions contient celui qui a été entré
 Elle doit ontenir un calcul nommé resolu qui vaut 1 si la fonction rep est exactement équivalente à la bonne réponse attendue
 et un calcul nommé presqueResolu si la réponse de l’élève n’est pas la bonne mais si repPourBornes est équivalent à la solution
 mais avec toutes les bornes fermées (c’est-à-dire s’il y a une erreur sur les bornes de l’intervalle).
 Elle doit contenir une fonction nommée fonctionTest d’une ou plusieurs variables dont une au moins a pour nom x
 et un calcul nommé contientBorne qui sera chargé de dire si une des bornes finies de l’ensembLe des soluions est comprise
 dans un intervalle contenant au calcul qui sera mis par le squelette dans la fonction fonctionTest.
 Imaginons pas exemple que l’ensemble des solutions est [1;2] U ]3;+∞[
 Une première fois, le squelette donnera comme formule à fonctionTest "x>1&x<2" et la seconde fois "x>3".
 Le  calcul contientBorne contiendra alors comme formule "fonctionTest(1)|fonctionTest(2)|fonctionTest(3)"
 Les bornes isolées ne doivent pas être prises en compte dans contientBorne. Si, par exemple, l’ensemble des solutions
 est [1;2[U]2;+∞[, contientBorne devra contenir comme formule "fonctionTest(1)".

 Si la figure contient un paramètre nbvar , alors dans le cas ou nbrepetitions > 1, on donne pour i compris entre 1 et nbvar
 au calcul de la figure nommé "r"+i (par exemple r1 et r2 si nbParamAleat vaut 2) des valeurs comprises entre 0 et "nbcas"+i - 1
 toutes différentes (sauf si nbrepetitions > b=nbcas auquel cas au bout de nbcas répétitions il y aura nécessairement une valeur de r déjà prise.
 */

// on définit le dossier des fonts mathlive (il faut le faire très tôt,
// ici c’est bon avec pnpm start mais trop tard pour un build (cf src/lib/player/index.js de la branche draftPlayer)
if (MathfieldElement) { // undefined dans les tests avec happy-dom
  MathfieldElement.fontsDirectory = '/static/mathlive/fonts'
  MathfieldElement.soundsDirectory = '/static/mathlive/sounds'
}

export const params = {
  /**
   * Ce qui est paramétrable dans l’éditeur de graphe
   * @type {LegacySectionParamsParametres}
   */
  parametres: [
    ['nbrepetitions', 1, 'entier', 'aucun'],
    ['limite', 0, 'entier', 'Temps disponible par répétition, en secondes'],
    ['nbEssais', 6, 'entier', 'Nombre d’essais maximum autorisés'],
    ['simplifier', true, 'boolean', 'true si on exige un forme donnée, false sinon'],
    ['indicationErreurCrochets', true, 'boolean', 'true si on indique quand la réponse est fausse pour une erreur de crochets'],
    ['a', 'random', 'string', 'Valeur de a'],
    ['b', 'random', 'string', 'Valeur de b'],
    ['c', 'random', 'string', 'Valeur de c'],
    ['d', 'random', 'string', 'Valeur de d'],
    ['e', 'random', 'string', 'Valeur de e'],
    ['f', 'random', 'string', 'Valeur de f'],
    ['g', 'random', 'string', 'Valeur de g'],
    ['h', 'random', 'string', 'Valeur de h'],
    ['j', 'random', 'string', 'Valeur de j'],
    ['k', 'random', 'string', 'Valeur de k'],
    ['l', 'random', 'string', 'Valeur de l'],
    ['m', 'random', 'string', 'Valeur de m'],
    ['n', 'random', 'string', 'Valeur de n'],
    ['p', 'random', 'string', 'Valeur de p'],
    ['q', 'random', 'string', 'Valeur de q'],
    ['r', 'random', 'string', 'Valeur de r'],
    ['ex', 'premiere/Inequation_Degre2_1', 'string', 'Nom du fichier annexe (xxx => squelettes-mtg32/xxx.js et xxx/yyy => squelettes-mtg32-xxx/yyy.js)']
  ]
}

const inputId = 'expressioninputmq1'

/**
 * section squelettemtg32_Ineq_Param_1_Edit
 * @this {Parcours}
 */
export default function main () {
  const parcours = this
  const sq = this.storage

  function onkeyup () {
    if (sq.marked) demarqueEditeurPourErreur()
  }

  function afficheNombreObjetsRestants () {
    j3pEmpty('info')
    const nbe = sq.nbEssais - sq.numEssai + 1
    if (nbe === 1) {
      afficheMathliveDans('info', 'texteinfo', 'Il reste un essai.', { style: { color: '#7F007F' } })
    } else {
      afficheMathliveDans('info', 'texteinfo', 'Il reste ' + nbe + ' essais.', { style: { color: '#7F007F' } })
    }
  }

  function marqueEditeurPourErreur () {
    sq.marked = true
    const mf = j3pElement(inputId)
    if (!mf) return
    mf.style.backgroundColor = '#FF9999'
    mf.demarqueErreur = () => {
      if (sq.marked) {
        demarqueEditeurPourErreur()
      }
    }
  }

  function demarqueEditeurPourErreur () {
    sq.marked = false
    j3pEmpty('correction')
    const mf = j3pElement(inputId)
    if (!mf) return
    mf.style.backgroundColor = 'white'
  }

  /**
   * Fonction extrayant d’un affichage LaTeX de la figure les lignes de texte et les transformant
   * en une chaîne affichable par MathQuill
   * @param tagLatex : le tag de l’affichage LaTeX de la figure
   * @return {string}
   */
  function extraitDeLatexPourMathlive (tagLatex) {
    const list = sq.mtgAppLecteur.getList('mtg32svg')
    const latex = list.getByTag(tagLatex)
    if (latex === null || latex.className !== 'CLatex') return ''
    return cleanLatexForMl(latex.chaineLatex)
  }

  function traiteMathlive (ch) {
    // La première ligne est nécessaire pour cette section
    ch = ch.replace(/\\R/g, '_R').replace(/\\emptyset/g, '∅')
    // Mathlive ne met pas d’accolades pour les fractions dont le numérateur et le dénominateur sont des entiers
    ch = ch.replace(/\\frac(\d)(\d)/g, '\\frac{$1' + '}{' + '$2}')
    // Ni pour les racines carrées d’un entier
    ch = ch.replace(/\\sqrt(\d)/g, '\\sqrt{$1}')
    // Pour les puissances dont l’exposant est un chiffre, Mathlive ne met pas d’accolades
    ch = ch.replace(/\^([^{])/g, '^{$1}')
    // Si on tape { au clavier dans les résolutions d’inéquations le code LaTeX n’est pas le même que celui
    // obtenu via le bouton MathQuill. La ligne ci-dessous corrige ça.
    ch = ch.replaceAll('lbrace', '{').replaceAll('rbrace', '}')
    ch = unLatexify(ch)
    // mathlive peut mettre des {} vides
    ch = ch.replace(/\\{\\}/g, '')
    ch = ch.replace(/\^(\d)(\d)/g, '^$1*$2') // On peut avoir par exemple 2^35^4 pour 2^3*5^4
    return sq.mtgAppLecteur.addImplicitMult('mtg32svg', ch)// Traitement des multiplications implicites
  }

  /**
   * Fonction utilisée pour améliorer le rendu des crochets quand on affiche la réponse rpécédente de l’élève.
   * @param {string} ch la chaîne traitée
   * @returns {string}
   */
  function traiteCrochets (ch) {
    const st = ch.split('\\cup')
    const res = []
    st.forEach((value) => {
      value = value.replace(/]([^;]+;[^[\]]+)\[/g, '\\left]$1\\right[')
      value = value.replace(/]([^;]+;[^[\]]+)]/g, '\\left]$1\\right]')
      value = value.replace(/\[([^;]+;[^[\]]+)\[/g, '\\left[$1\\right[')
      value = value.replace(/\[([^;]+;[^[\]]+)]/g, '\\left[$1\\right]')
      res.push(value)
    })
    return res.join('\\cup')
  }

  function validationEditeur () {
    sq.vide = false
    sq.erreurOrdreBornes = false // Pourra être mis à true dans validationIntervalle
    // quand deux bornes ne sont pas rangées dans le bon ordre
    let res = true
    const rep = getMathliveValue(inputId)
    if (rep === '') {
      res = false
    } else {
      const chcalcul = traiteMathlive(rep)
      if (chcalcul.indexOf('∅') !== -1) {
        res = chcalcul.length === 1
        sq.vide = res
      } else {
        const valide = validationEnsembleSol(chcalcul)
        if (!valide) res = false
      }
    }
    if (!res) {
      marqueEditeurPourErreur()
      focusIfExists(inputId)
    }
    return res
  }

  /**
   * Fonction renvoyant un tableau formé de deux tableaux réordonnés suivant les valeurs du premier tableau
   * Les deux tableaux doivent avoir la même dimension
   * @param tabval
   * @param tabfor
   */
  function ordonne (tabval, tabfor) {
    let i, j
    const tabvalaux = []
    const tabforaux = []
    const tabvalres = []
    const tabforres = []
    for (i = 0; i < tabval.length; i++) {
      tabvalaux.push(tabval[i])
      tabforaux.push(tabfor[i])
    }
    while (tabvalaux.length >= 1) {
      let valmin = tabvalaux[0]
      let indmin = 0
      for (j = 1; j < tabvalaux.length; j++) {
        if (tabvalaux[j] < valmin) {
          indmin = j
          valmin = tabvalaux[j]
        }
      }
      const val = tabvalaux[indmin]
      const form = tabforaux[indmin]
      tabvalres.push(val)
      tabforres.push(form)
      tabvalaux.splice(indmin, 1)
      tabforaux.splice(indmin, 1)
    }
    return [tabvalres, tabforres]
  }

  function validationEnsembleSol (ch) {
    let i, j
    const listePourCalc = sq.listePourCalc
    sq.reponseContientR = false // Sera true si un des intervalles de la réunion est ]-∞;+∞[
    if (ch === '_R' || ch === ']-∞;+∞[') {
      sq.reponseEstR = true
      return true
    } else { sq.reponseEstR = false }
    if (ch.charAt(ch.length - 1) === '|') return false
    if (ch.indexOf('[∞') !== -1 || ch.indexOf('∞]') !== -1 || ch.indexOf('[-∞') !== -1) return false
    sq.bornesGauches = []
    sq.valBornesGauches = []
    sq.bornesGauchesFermees = []
    sq.bornesDroites = []
    sq.valBornesDroites = []
    sq.bornesDroitesFermees = []
    sq.bornesGauchesInf = []
    sq.bornesDroitesInf = []
    // On regarde s la réponse a été entrée sous la forme R-{}
    if (ch.indexOf('_R-\\(') === 0) {
      const indpf = parentheseFermante(ch, 5)
      if (indpf < ch.length - 1) return false // On ne permet que R-{} sans autre opérateur
      const chaux = ch.substring(5, indpf)
      if (chaux === '') return false
      const tabcal = []
      const tabval = []
      const tab = chaux.split(';')
      for (i = 0; i < tab.length; i++) {
        const ch2 = tab[i]
        if (ch2 === '') return -1
        if (!listePourCalc.verifieSyntaxe(ch2)) return false
        listePourCalc.giveFormula2('x', ch2)
        listePourCalc.calculateNG()
        tabcal.push(ch2)
        tabval.push(listePourCalc.valueOf('x'))
      }
      // On ordonne les valeurs trouvées entre les accolades
      const res = ordonne(tabval, tabcal)
      const tabvalord = res[0]
      const tabforord = res[1]
      // On regarde s’il, y a deux valeurs ou plus répétées. Si oui on refuse
      for (i = 0; i < tabvalord.length; i++) {
        for (j = i + 1; j < tabvalord.length - 1; j++) {
          if (tabvalord[i] === tabvalord[j]) return false
        }
      }
      for (i = 0; i < tabvalord.length; i++) {
        if (i === 0) { // Intervalle de - ∞ à la plus petite valeur
          sq.bornesGauchesInf.push(true)
          sq.bornesGauches.push('0') // Valeur sans importance
          sq.bornesGauchesFermees.push(false)
          sq.valBornesGauches.push(0)
          sq.bornesDroitesInf.push(false)
          sq.bornesDroitesFermees.push(false)
          sq.bornesDroites.push(tabforord[0])
          sq.valBornesDroites.push(tabvalord[0])
        } else {
          sq.bornesGauchesInf.push(false)
          sq.bornesGauches.push(tabforord[i - 1])
          sq.bornesGauchesFermees.push(false)
          sq.valBornesGauches.push(tabvalord[i - 1])
          sq.bornesDroitesInf.push(false)
          sq.bornesDroitesFermees.push(false)
          sq.bornesDroites.push(tabforord[i])
          sq.valBornesDroites.push(tabvalord[i])
        }
        if (i === tabvalord.length - 1) {
          sq.bornesGauchesInf.push(false)
          sq.bornesGauches.push(tabforord[i])
          sq.bornesGauchesFermees.push(false)
          sq.valBornesGauches.push(tabvalord[i])
          sq.bornesDroitesInf.push(true)
          sq.bornesDroitesFermees.push(false)
          sq.bornesDroites.push('0') // Valeru sans importance
          sq.valBornesDroites.push(0)
        }
      }
    } else {
      let ind = 0
      while (ind !== -1 && ind <= ch.length - 1) {
        ind = validationIntervalle(ch, ind)
        if (ind === -1) return false
      }
    }
    // On crée maintenant une chaîne de caractères dont le texte est celui d’une fonction mtg32 testant si un x est solution
    let st = ''
    for (i = 0; i < sq.bornesGauches.length; i++) {
      if (i !== 0) st += '|'
      if (sq.bornesGauchesInf[i]) {
        if (sq.bornesDroitesInf[i]) {
          st += '(0=0)'
        } else { // N’importe quel test vrai fait l’affaire
          st += '(' + sq.bornesDroites[i]
          st += (sq.bornesDroitesFermees[i] ? '>=' : '>') + 'x)'
        }
      } else {
        if (sq.bornesDroitesInf[i]) {
          st += '(' + sq.bornesGauches[i]
          st += (sq.bornesGauchesFermees[i] ? '<=' : '<') + 'x)'
        } else {
          if (sq.bornesGauchesFermees[i] && sq.bornesDroitesFermees[i] && sq.bornesGauches[i] === sq.bornesDroites[i]) {
            st += '(x=' + sq.bornesGauches[i] + ')'
          } else {
            st += '(' + sq.bornesGauches[i]
            st += (sq.bornesGauchesFermees[i] ? '<=' : '<') + 'x)&('
            st += sq.bornesDroites[i]
            st += (sq.bornesDroitesFermees[i] ? '>=' : '>') + 'x)'
          }
        }
      }
    }
    sq.chaineFonctionTest = st
    sq.chaineFonctionTestBornesFermees = st.replace(/<([^=])/g, '<=$1').replace(/>([^=])/g, '>=$1') // Pour tester une réponse presque exacte avec une confusion sur les bornes
    // Une autre chaîne qui sera attribuée à repPourBornes de la figure
    st = ''
    for (i = 0; i < sq.bornesGauches.length; i++) {
      if (sq.bornesGauchesFermees[i] && sq.bornesDroitesFermees[i] && sq.bornesGauches[i] === sq.bornesDroites[i]) {
        st += '(x=' + sq.bornesGauches[i] + ')'
      } else {
        if (!sq.bornesGauchesInf[i]) {
          st += '(' + sq.bornesGauches[i]
          st += (sq.bornesGauchesFermees[i] ? '<=' : '<') + 'x)&('
        } else { st += '(' }
        if (!sq.bornesDroitesInf[i]) {
          st += sq.bornesDroites[i]
          st += (sq.bornesDroitesFermees[i] ? '>=' : '>') + 'x)'
        } else { st += '0=0)' }
      }
      if (sq.bornesGauchesFermees[i]) st += '|' + 'zeroBorne(x - (' + sq.bornesGauches[i] + '))'
      if (sq.bornesDroitesFermees[i]) st += '|' + 'zeroBorne(x - (' + sq.bornesDroites[i] + '))'
      if (i !== sq.bornesGauches.length - 1) st += '|'
    }
    sq.chaineFonctionTestPourBornes = st
    // On doit savoir si la réponse contient R pour cela elle doit contenir au moins un intervalle non borné à gauche,
    // au moins un intervalle non borné à droite
    if (!sq.reponseContientR) {
      let repContientR = sq.bornesGauchesInf.length > 0 && sq.bornesDroitesInf.length > 0
      if (repContientR) {
        for (i = 0; (i < sq.bornesGauches.length) && repContientR; i++) {
          if (!sq.bornesGauchesInf[i]) {
            if (sq.bornesGauchesFermees[i]) {
              repContientR = repContientR && (estBorneOuverteDroiteAutreIntQue(sq.bornesGauches[i], i) || appartientAuMoinsUnIntAutreQue(sq.bornesGauches[i], i))
            } else {
              repContientR = repContientR && (estBorneFermeeDroiteAutreIntQue(sq.bornesGauches[i], i) || appartientAuMoinsUnIntAutreQue(sq.bornesGauches[i], i))
            }
          }
          if (!sq.bornesDroitesInf[i]) {
            if (sq.bornesDroitesFermees[i]) {
              repContientR = repContientR && (estBorneOuverteGaucheAutreIntQue(sq.bornesDroites[i], i) || appartientAuMoinsUnIntAutreQue(sq.bornesDroites[i], i))
            } else {
              repContientR = repContientR && (estBorneFermeeGaucheAutreIntQue(sq.bornesDroites[i], i) || appartientAuMoinsUnIntAutreQue(sq.bornesDroites[i], i))
            }
          }
        }
      }
      sq.reponseContientR = repContientR
    }
    return true
  }

  function parentheseFermante (ch, inddeb) {
    let i = inddeb
    let somme = 1
    while (i < ch.length) {
      const car = ch.charAt(i)
      if (car === '\\') return -1 // Pas d’accolades imbriquées
      if (car === '(') { somme++ } else {
        if (car === ')') somme--
      }
      if (somme === 0) return i
      else i++
    }
    return -1
  }

  function validationIntervalle (ch, indeb) {
    let indcr
    let valGauche
    let valDroite
    const listePourCalc = sq.listePourCalc
    const car = ch.charAt(indeb)
    if (car !== '[' && car !== ']' && car !== '\\') return -1
    const crochet1 = ch.charAt(indeb)
    if (crochet1 === '\\') { // Traitement des valeurs isolées entre accolades
      indcr = parentheseFermante(ch, indeb + 2) // Le \ est forcéménet suivi d’une (
      if (indcr === -1) return -1
      const st = ch.substring(indeb + 2, indcr)
      const tab = st.split(';')
      for (let j = 0; j < tab.length; j++) {
        const ch2 = tab[j]
        if (ch2 === '') return -1
        if (!listePourCalc.verifieSyntaxe(ch2)) return -1
        sq.bornesGauches.push(ch2)
        listePourCalc.giveFormula2('x', ch2)
        listePourCalc.calculateNG()
        valGauche = listePourCalc.valueOf('x')
        sq.valBornesGauches.push(valGauche)
        sq.valBornesDroites.push(valGauche)
        sq.bornesDroites.push(ch2)
        sq.bornesGauchesFermees.push(true)
        sq.bornesGauchesInf.push(false)
        sq.bornesDroitesFermees.push(true)
        sq.bornesDroitesInf.push(false)
      }
    } else {
      const indpv = ch.indexOf(';', indeb + 1)
      const ind1 = ch.indexOf('[', indeb + 1)
      const ind2 = ch.indexOf(']', indeb + 1)
      if (indpv === -1 || (ind1 === -1 && ind2 === -1)) return -1
      if (ind1 === -1) indcr = ind2
      else if (ind2 === -1) indcr = ind1
      else indcr = Math.min(ind1, ind2)
      if (indcr < indpv) return -1
      const crochet2 = ch.charAt(indcr)
      const left = ch.substring(indeb + 1, indpv)
      const right = ch.substring(indpv + 1, indcr)
      if (left === '' || right === '') return -1
      sq.bornesGauches.push(left)
      sq.bornesDroites.push(right)
      const borneGaucheInf = left === '-∞'
      const borneDroiteInf = right === '+∞'
      sq.bornesGauchesInf.push(borneGaucheInf)
      sq.bornesDroitesInf.push(borneDroiteInf)
      if (borneGaucheInf && borneDroiteInf) sq.reponseContientR = true
      if ((!borneGaucheInf && !listePourCalc.verifieSyntaxe(left)) || (!borneDroiteInf && !listePourCalc.verifieSyntaxe(right))) return -1
      if (!borneGaucheInf) {
        listePourCalc.giveFormula2('x', left)
        listePourCalc.calculateNG()
        valGauche = listePourCalc.valueOf('x')
      }
      if (!borneDroiteInf) {
        listePourCalc.giveFormula2('x', right)
        listePourCalc.calculateNG()
        valDroite = listePourCalc.valueOf('x')
      }
      if ((!borneGaucheInf && !borneDroiteInf) && (valGauche > valDroite)) {
        sq.erreurOrdreBornes = true
        return -1
      }
      sq.valBornesGauches.push(valGauche)
      sq.valBornesDroites.push(valDroite)
      sq.bornesGauchesFermees.push(!borneGaucheInf && (crochet1 === '['))
      sq.bornesDroitesFermees.push(!borneDroiteInf && (crochet2 === ']'))
    }
    if (indcr < ch.length - 1) {
      if (ch.charAt(indcr + 1) !== '|') return -1
      else return indcr + 2
    } else { return indcr + 1 }
  }

  /**
   * Fonction renvoyant true, si, à 10^-10 près, le nombre xtest est une des bornes fermées de l’intervalle
   * ou la réunion d’intervalles proposées par l’élève
   * @param xtest
   */
  function estBorne (xtest) {
    const eps = 0.0000000001
    for (let i = 0; i < sq.bornesGauches.length; i++) {
      if ((!sq.bornesGauchesInf[i] && ((sq.bornesGauchesFermees[i] && Math.abs(xtest - sq.valBornesGauches[i]) <= eps))) ||
        (!sq.bornesDroitesInf[i] && (sq.bornesDroitesFermees[i] && Math.abs(xtest - sq.valBornesDroites[i]) <= eps))) return true
    }
    return false
  }

  /**
   * Fonction renvoyant true si xTest est une borne gauche fermée d’un intervalle autre que l’intervalle d’indice indIntervalle
   * @param xtest
   * @param indIntervalle
   * @return {boolean}
   */
  function estBorneFermeeGaucheAutreIntQue (xtest, indIntervalle) {
    const eps = 0.0000000001
    for (let i = 0; i < sq.bornesGauches.length; i++) {
      if (i !== indIntervalle) {
        if (!sq.bornesGauchesInf[i] && sq.bornesGauchesFermees[i] && Math.abs(xtest - sq.valBornesGauches[i]) <= eps) return true
      }
    }
    return false
  }

  /**
   * Fonction renvoyant true si xTest est une borne gauche ouverte d’un intervalle autre que l’intervalle d’indice indIntervalle
   * @param xtest
   * @param indIntervalle
   * @return {boolean}
   */
  function estBorneOuverteGaucheAutreIntQue (xtest, indIntervalle) {
    const eps = 0.0000000001
    for (let i = 0; i < sq.bornesGauches.length; i++) {
      if (i !== indIntervalle) {
        if (!sq.bornesGauchesInf[i] && !sq.bornesGauchesFermees[i] && Math.abs(xtest - sq.valBornesGauches[i]) <= eps) return true
      }
    }
    return false
  }

  /**
   * Fonction renvoyant true si xTest est une borne droite fermée d’un intervalle autre que l’intervalle d’indice indIntervalle
   * @param xtest
   * @param indIntervalle
   * @return {boolean}
   */
  function estBorneFermeeDroiteAutreIntQue (xtest, indIntervalle) {
    const eps = 0.0000000001
    for (let i = 0; i < sq.bornesGauches.length; i++) {
      if (i !== indIntervalle) {
        if (!sq.bornesDroitesInf[i] && sq.bornesDroitesFermees[i] && Math.abs(xtest - sq.valBornesDroites[i]) <= eps) return true
      }
    }
    return false
  }

  /**
   * Fonction renvoyant true si xTest est une borne droite ouverte d’un intervalle autre que l’intervalle d’indice indIntervalle
   * @param xtest
   * @param indIntervalle
   * @return {boolean}
   */
  function estBorneOuverteDroiteAutreIntQue (xtest, indIntervalle) {
    const eps = 0.0000000001
    for (let i = 0; i < sq.bornesGauches.length; i++) {
      if (i !== indIntervalle) {
        if (!sq.bornesDroitesInf[i] && !sq.bornesDroitesFermees[i] && Math.abs(xtest - sq.valBornesDroites[i]) <= eps) return true
      }
    }
    return false
  }

  function reponseIncluseDansSol () {
    let xTest
    let xTest2
    let res, estBorneIsolee
    const eps = 0.0000000001
    const eps2 = 0.00000001
    res = sq.mtgAppLecteur.valueOf('mtg32svg', 'toutReelSol')
    if (res === 1) return true
    // On commence par regarder si un des intervalles de la réunion proposée contient une borne limitante
    // de l’intervalle solution.
    // Pour cela on donne à la fonction fonctionTest de la figure une formule et on regarde si cette formule
    // est vérifiée par une borne limitante de la figure (hors valeur exclue isolée) en interrogeant
    // la valeur du calcul contientBorne de la figure
    for (let i = 0; i < sq.bornesGauches.length; i++) {
      let form = ''
      if (!sq.bornesGauchesInf[i]) form += sq.valBornesGauches[i] + '<x'
      if (!sq.bornesDroitesInf[i]) {
        if (form !== '') form += '&'
        form += 'x<' + sq.valBornesDroites[i]
      }
      if (form !== '') {
        sq.mtgAppLecteur.giveFormula2('mtg32svg', 'fonctionTest', form)
        sq.mtgAppLecteur.calculate('mtg32svg', false)
        if (sq.mtgAppLecteur.valueOf('mtg32svg', 'contientBorne') === 1) return false
      }
    }
    for (let i = 0; i < sq.bornesGauches.length; i++) {
      // On étudie le cas d’une valeur isolée représentée par un intervalle dont les bornes sont confondues
      if (sq.bornesGauchesFermees[i] && sq.bornesDroitesFermees[i] && (sq.valBornesGauches[i] === sq.valBornesDroites[i])) {
        xTest = sq.valBornesGauches[i]
        sq.mtgAppLecteur.giveFormula2('mtg32svg', 'xTest', xTest.toFixed(16))
        sq.mtgAppLecteur.calculate('mtg32svg', false)
        if ((sq.mtgAppLecteur.valueOf('mtg32svg', 'estBorne') !== 1) &&
          (sq.mtgAppLecteur.valueOf('mtg32svg', 'estSolution') !== 1)) return false
      } else {
        if (sq.bornesGauchesInf[i]) {
          res = sq.mtgAppLecteur.valueOf('mtg32svg', 'moinsInfSolution')
          if (res !== 1) return false
        } else {
          const gauche = sq.valBornesGauches[i]
          if (!sq.bornesGauchesFermees[i]) {
            if (!appartientAuMoinsUnInt(gauche)) {
              xTest = gauche
              sq.mtgAppLecteur.giveFormula2('mtg32svg', 'xTest', xTest.toFixed(16))
              sq.mtgAppLecteur.calculate('mtg32svg', false)
              res = sq.mtgAppLecteur.valueOf('mtg32svg', 'estBorne')
              if (res === 1) return false
              estBorneIsolee = (sq.mtgAppLecteur.valueOf('mtg32svg', 'estBorneIsolee') === 1)
              if (!estBorneIsolee) {
                xTest2 = gauche - eps
                sq.mtgAppLecteur.giveFormula2('mtg32svg', 'xTest', xTest2.toFixed(16))
                sq.mtgAppLecteur.calculate('mtg32svg', false)
                res = sq.mtgAppLecteur.valueOf('mtg32svg', 'estSolution')
                if (res === 1 && !estBorneFermee(xTest)) return false
              }
            }
          }
          if (estBorne(gauche)) xTest = gauche; else xTest = gauche + eps
          sq.mtgAppLecteur.giveFormula2('mtg32svg', 'xTest', xTest.toFixed(16))
          sq.mtgAppLecteur.calculate('mtg32svg', false)
          res = sq.mtgAppLecteur.valueOf('mtg32svg', 'estSolution')
          if (res !== 1) return false
          xTest = gauche - eps2
          if (!appartientAuMoinsUnInt(xTest)) {
            sq.mtgAppLecteur.giveFormula2('mtg32svg', 'xTest', xTest.toFixed(16))
            sq.mtgAppLecteur.calculate('mtg32svg', false)
            res = sq.mtgAppLecteur.valueOf('mtg32svg', 'estSolution')
            if (res === 1) return false
          }
        }
        if (sq.bornesDroitesInf[i]) {
          res = sq.mtgAppLecteur.valueOf('mtg32svg', 'plusInfSolution')
          if (res !== 1) return false
        } else {
          const droite = sq.valBornesDroites[i]
          if (!sq.bornesDroitesFermees[i]) {
            if (!appartientAuMoinsUnInt(droite)) {
              xTest = droite
              sq.mtgAppLecteur.giveFormula2('mtg32svg', 'xTest', xTest.toFixed(16))
              sq.mtgAppLecteur.calculate('mtg32svg', false)
              res = sq.mtgAppLecteur.valueOf('mtg32svg', 'estBorne')
              if (res === 1) return false
              estBorneIsolee = (sq.mtgAppLecteur.valueOf('mtg32svg', 'estBorneIsolee') === 1)
              if (!estBorneIsolee) {
                xTest2 = droite + eps
                sq.mtgAppLecteur.giveFormula2('mtg32svg', 'xTest', xTest2.toFixed(16))
                sq.mtgAppLecteur.calculate('mtg32svg', false)
                res = sq.mtgAppLecteur.valueOf('mtg32svg', 'estSolution')
                if (res === 1 && !estBorneFermee(xTest)) return false
              }
            }
          }
          if (estBorne(droite)) xTest = droite; else xTest = droite - eps
          sq.mtgAppLecteur.giveFormula2('mtg32svg', 'xTest', xTest.toFixed(16))
          sq.mtgAppLecteur.calculate('mtg32svg', false)
          res = sq.mtgAppLecteur.valueOf('mtg32svg', 'estSolution')
          if (res !== 1) return false
          xTest = droite + eps2
          if (!appartientAuMoinsUnInt(xTest)) {
            sq.mtgAppLecteur.giveFormula2('mtg32svg', 'xTest', xTest.toFixed(16))
            sq.mtgAppLecteur.calculate('mtg32svg', false)
            res = sq.mtgAppLecteur.valueOf('mtg32svg', 'estSolution')
            if (res === 1) return false
          }
        }
      }
    }
    return true
  }

  function appartientAuMoinsUnInt (x) {
    if (sq.reponseContientR) return true
    for (let i = 0; i < sq.bornesGauches.length; i++) {
      if (sq.bornesGauchesInf[i]) {
        if (sq.bornesDroitesInf[i]) { return true } else {
          if (sq.bornesDroitesFermees[i]) {
            if (x <= sq.valBornesDroites[i]) return true
          } else {
            if (x < sq.valBornesDroites[i]) return true
          }
        }
      } else {
        if (sq.bornesDroitesInf[i]) {
          if (sq.bornesGauchesFermees[i]) {
            if (x >= sq.valBornesGauches[i]) return true
          } else {
            if (x > sq.valBornesGauches[i]) return true
          }
        } else {
          if (sq.bornesGauchesFermees[i]) {
            if (sq.bornesDroitesFermees[i]) {
              if ((x >= sq.valBornesGauches[i]) && (x <= sq.valBornesDroites[i])) return true
            } else {
              if ((x >= sq.valBornesGauches[i]) && (x < sq.valBornesDroites[i])) return true
            }
          } else {
            if (sq.bornesDroitesFermees[i]) {
              if ((x > sq.valBornesGauches[i]) && (x <= sq.valBornesDroites[i])) return true
            } else {
              if ((x > sq.valBornesGauches[i]) && (x < sq.valBornesDroites[i])) return true
            }
          }
        }
      }
    }
    return false
  }

  /**
   * Fonction renvoyant true si x appratient à au moins un intervalle autre que celui d’indice ind
   * @param x
   * @param indIntervalle
   * @return {boolean}
   */
  function appartientAuMoinsUnIntAutreQue (x, indIntervalle) {
    for (let i = 0; i < sq.bornesGauches.length; i++) {
      if (i !== indIntervalle) {
        if (sq.bornesGauchesInf[i]) {
          if (sq.bornesDroitesInf[i]) { return true } else {
            if (sq.bornesDroitesFermees[i]) {
              if (x <= sq.valBornesDroites[i]) return true
            } else {
              if (x < sq.valBornesDroites[i]) return true
            }
          }
        } else {
          if (sq.bornesDroitesInf[i]) {
            if (sq.bornesGauchesFermees[i]) {
              if (x >= sq.valBornesGauches[i]) return true
            } else {
              if (x > sq.valBornesGauches[i]) return true
            }
          } else {
            if (sq.bornesGauchesFermees[i]) {
              if (sq.bornesDroitesFermees[i]) {
                if ((x >= sq.valBornesGauches[i]) && (x <= sq.valBornesDroites[i])) return true
              } else {
                if ((x >= sq.valBornesGauches[i]) && (x < sq.valBornesDroites[i])) return true
              }
            } else {
              if (sq.bornesDroitesFermees[i]) {
                if ((x > sq.valBornesGauches[i]) && (x <= sq.valBornesDroites[i])) return true
              } else {
                if ((x > sq.valBornesGauches[i]) && (x < sq.valBornesDroites[i])) return true
              }
            }
          }
        }
      }
    }
    return false
  }

  function estBorneFermee (x) {
    for (let i = 0; i < sq.bornesGauches.length; i++) {
      if (!sq.bornesGauchesInf[i] && sq.bornesGauches[i] === x) return true
      if (!sq.bornesDroitesInf[i] && sq.bornesDroites[i] === x) return true
    }
  }

  function montreEditeur (bVisible) {
    if (bVisible) $('#info').css('display', 'block')
    $('#editeur').css('display', bVisible ? 'block' : 'none')
    $('#boutonsmathquill').css('display', bVisible ? 'block' : 'none')
    // On vide le contenu de l’éditeur MathQuill et on lui donne le focus.
    if (bVisible) {
      j3pElement(inputId).value = ''
      focusIfExists(inputId)
    }
  }

  function videEditeur () {
    j3pElement(inputId).value = ''
  }

  function afficheReponse (bilan, depasse) {
    let coul
    const num = sq.numEssai
    const idrep = 'exp' + num
    if (num > 2) afficheMathliveDans('formules', idrep + 'br', '<br>')

    if (bilan === 'exact') {
      coul = parcours.styles.cbien
    } else {
      if (bilan === 'faux') {
        coul = parcours.styles.cfaux
      } else { // Réponse exacte mais calcul pas fini
        if (depasse) coul = parcours.styles.cfaux
        else coul = '#0000FF'
      }
    }
    if (bilan === 'faux') {
      afficheMathliveDans('formules', idrep, '$' + (sq.entete === '' ? sq.aCalculer : sq.entete) + ' ' + sq.symbnonexact +
        ' ' + traiteCrochets(sq.rep) + '$', {
        style: {
          color: coul
        }
      })
    } else {
      afficheMathliveDans('formules', idrep, '$' + (sq.entete === '' ? sq.aCalculer : sq.entete) + ' ' + sq.symbexact +
        ' ' + traiteCrochets(sq.rep) + '$', {
        style: {
          color: coul
        }
      })
    }
    resetKeyboardPosition()
  }

  function validation () {
    if (sq.numEssai >= 1) {
      j3pElement('boutonrecopier').style.display = 'block'
    }
    sq.rep = getMathliveValue(inputId)
    sq.presqueResolu = false
    const vide = sq.mtgAppLecteur.valueOf('mtg32svg', 'vide')
    if (sq.vide) {
      if (vide) {
        sq.resolu = true
      } else {
        sq.resolu = false
        sq.exact = false
      }
    } else {
      const toutReelSol = sq.mtgAppLecteur.valueOf('mtg32svg', 'toutReelSol') === 1
      if (toutReelSol) {
        if (sq.reponseEstR) { sq.resolu = true } else {
          if (sq.reponseContientR) {
            sq.exact = true
            sq.resolu = false
          } else {
            sq.exact = false
            sq.resolu = false
          }
        }
      } else {
        if (sq.reponseEstR || sq.reponseContientR) {
          sq.resolu = false
          sq.exact = false
        } else {
          // On regarde d’abord pour chaque borne des intervalles entrés :
          // Si l’intervalle est fermé en cette borne ou en une autre borne équivalente, si la borne est solution de l’inéquation
          // Sinon si un nombre très proche de cette borne et dans l’intervalle est solution.
          sq.mtgAppLecteur.giveFormula2('mtg32svg', 'rep', sq.chaineFonctionTest)
          sq.mtgAppLecteur.giveFormula2('mtg32svg', 'repPourBornes', sq.chaineFonctionTestPourBornes)
          sq.mtgAppLecteur.giveFormula2('mtg32svg', 'repBornesFermees', sq.chaineFonctionTestBornesFermees)
          sq.mtgAppLecteur.calculate('mtg32svg', false)
          sq.presqueResolu = sq.mtgAppLecteur.valueOf('mtg32svg', 'presqueResolu') === 1
          if (!reponseIncluseDansSol()) {
            sq.exact = false
            sq.resolu = false
          } else {
            if (sq.mtgAppLecteur.valueOf('mtg32svg', 'resolu') === 1) {
              sq.resolu = true
            } else {
              sq.resolu = false
              sq.exact = sq.mtgAppLecteur.valueOf('mtg32svg', 'repContientSol') === 1
            }
          }
        }
      }
    }
  }

  function recopierReponse () {
    demarqueEditeurPourErreur()
    j3pElement(inputId).value = sq.rep
    focusIfExists(inputId)
  }

  /**
   * Fonction affichant la solution s’il existe un affichage LaTeX de tag solution
   * @param{boolean} bexact true si l’élève a entré la bonne réponse, false sinon
   */
  function afficheSolution (bexact) {
    if (sq.solution) {
      sq.mtgAppLecteur.executeMacro('mtg32svg', 'solution')
      $('#divSolution').css('display', 'block').html('')
      const ch = extraitDeLatexPourMathlive('solution')
      if (ch !== '') {
        const styles = parcours.styles
        afficheMathliveDans('divSolution', 'solution', ch, { style: { color: bexact ? styles.cbien : styles.colorCorrection } })
      }
    }
  }

  function cacheSolution () {
    $('#divSolution').css('display', 'none').html('')
  }

  function initMtg () {
    setMathliveCss('MepMG')
    const mtgOptions = {
      // player only, sans figure initiale ni svg, on gère ça dans initMtg et modif_fig
      loadCoreWithMathJax: true
    }
    getMtgApp('mtg32svg', {}, mtgOptions).then((mtgAppLecteur) => {
      sq.mtgAppLecteur = mtgAppLecteur
      initDom()
      sq.marked = false
      // Une liste qui servira pour le calcul des valerus des bornes des intervalles.
      // Elle contiendra un calcul x auquel on affectera des valeurs pour tester la valeur de la syntaxe et de la valeur d’une borne d’intervalle.
      sq.listePourCalc = sq.mtgAppLecteur.createList('TWF0aEdyYXBoSmF2YTEuMAAAABI+TMzNAAJmcv###wEA#wEAAAAAAAAAAAQsAAACvQAAAQEAAAAAAAAAAQAAAAP#####AAAAAQAKQ0NhbGNDb25zdAD#####AAJwaQAWMy4xNDE1OTI2NTM1ODk3OTMyMzg0Nv####8AAAABAApDQ29uc3RhbnRlQAkh+1RELRj#####AAAAAQAHQ0NhbGN1bAD#####AAFlAAZleHAoMSn#####AAAAAgAJQ0ZvbmN0aW9uBwAAAAE#8AAAAAAAAAAAAAIA#####wABeAABMAAAAAEAAAAAAAAAAP###############w==')
      let code, par, car, i, j, k, nbrep, ar, tir, nb
      sq.mtgAppLecteur.removeAllDoc()
      sq.mtgAppLecteur.addDoc('mtg32svg', sq.txt, true)
      sq.mtgAppLecteur.calculate('mtg32svg', false)
      sq.nbexp = 0
      sq.reponse = -1
      const ch = 'abcdefghjklmnpqr'
      const nbvar = sq.mtgAppLecteur.valueOf('mtg32svg', 'nbvar')
      if (nbvar !== -1) {
        nbrep = parcours.donneesSection.nbrepetitions
        sq.aleat = true
        sq.nbParamAleat = nbvar
        for (i = 1; i <= nbvar; i++) {
          const nbcas = sq.mtgAppLecteur.valueOf('mtg32svg', 'nbcas' + i)
          nb = Math.max(nbrep, nbcas)
          ar = []
          for (j = 0; j < nb; j++) ar.push(j % nbcas)
          sq['tab' + i] = []
          for (k = 0; k < nbrep; k++) {
            tir = Math.floor(Math.random() * ar.length)
            sq['tab' + i].push(ar[tir])
            ar.splice(tir, 1)
          }
          sq.mtgAppLecteur.giveFormula2('mtg32svg', 'r' + i, sq['tab' + i][0])
        }
      } else { sq.aleat = false }

      if (sq.param !== undefined) {
        for (i = 0; i < ch.length; i++) {
          car = ch.charAt(i)
          if (sq.param.indexOf(car) !== -1) {
            par = parcours.donneesSection[car]
            if (par !== 'random') sq.mtgAppLecteur.giveFormula2('mtg32svg', car, par)
          }
        }
      }
      sq.mtgAppLecteur.calculateAndDisplayAll(true)
      const nbParam = parseInt(sq.nbLatex)
      const t = ['a', 'b', 'c', 'd']
      const param = {}
      for (i = 0; i < nbParam; i++) {
        code = sq.mtgAppLecteur.getLatexCode('mtg32svg', i)
        if (i === 0) sq.aCalculer = code
        param[t[i]] = code
      }
      param.e = String(sq.nbEssais)
      const st = sq.consigne1 + (sq.simplifier ? sq.consigne3 : sq.consigne2) + sq.consigne4
      afficheMathliveDans('enonce', 'texte', st, param)
      if (sq.entete !== '') {
        afficheMathliveDans('debut', 'acalc', '$' + sq.entete + '$' + ' = ', param)
      } else {
        afficheMathliveDans('debut', 'acalc', '$' + sq.aCalculer + '=$ ', param)
      }
      focusIfExists(inputId)
      parcours.finEnonce()
    }).catch(j3pShowError)
  }

  function initDom () {
    renderMathInDocument({ renderAccessibleContent: '' }) // Indispensable
    if (sq.titre) parcours.afficheTitre(sq.titre)

    j3pDiv(parcours.zones.MG, {
      id: 'conteneur',
      contenu: '',
      style: parcours.styles.etendre('toutpetit.enonce', { padding: '6px' })
    })
    j3pDiv('conteneur', 'enonce', '')
    j3pDiv('conteneur', {
      id: 'formules',
      contenu: '',
      style: parcours.styles.petit.enonce
    }) // Contient le formules entrées par l’élève
    $('#formules').css('padding-bottom', '5px')
    if (sq.bigSize) $('#formules').css('font-size', '26px')
    j3pDiv('conteneur', 'info', '')
    afficheNombreObjetsRestants()
    j3pDiv('conteneur', 'conteneurbouton', '')
    j3pAjouteBouton('conteneurbouton', 'boutonrecopier', 'MepBoutonsRepere', 'Recopier réponse précédente',
      recopierReponse)
    j3pElement('boutonrecopier').style.display = 'none'
    j3pDiv('conteneur', 'editeur', '') // Le div qui contiendra l’éditeur MathQuill
    if (sq.bigSize) $('#editeur').css('font-size', '30px')
    $('#editeur').css('padding-top', '10px') // Pour laisser un peu de place au-dessus de l’éditeur
    j3pAddElt('editeur', 'span', '', { id: 'debut' })
    // j3pAffiche('editeur', 'debut', '', {})
    afficheMathliveDans('editeur', 'expression', '&1&', {
      charset: sq.charset,
      listeBoutons: sq.listeBoutons
    })
    if (sq.charset !== '') {
      mathliveRestrict(inputId, sq.charset)
    }
    j3pDiv('conteneur', 'boutonsmathquill', '')
    $('#boutonsmathquill').css('display', 'inline-block').css('padding-top', '10px') // Pour laisser un peu de place au-dessus des boutons
    j3pPaletteMathquill('boutonsmathquill', inputId, {
      liste: sq.listeBoutons.concat(['[', ']', ';']), nomdiv: 'palette'
    })
    // Ligne suivante pour un bon alignement
    $('#palette').css('display', 'inline-block')

    j3pElement(inputId).onkeyup = function (ev) {
      onkeyup.call(this, ev, parcours)
    }
    j3pDiv('conteneur', 'divSolution', '')
    $('#divSolution').css('display', 'none').css('padding-bottom', '5px')
    j3pDiv('conteneur', 'divmtg32', '')
    j3pCreeSVG('divmtg32', {
      id: 'mtg32svg',
      width: sq.width,
      height: sq.height
    })
    const style = parcours.styles.petit.correction
    style.marginTop = '1.5em'
    style.marginLeft = '0.5em'
    j3pAddElt(parcours.zonesElts.MD, 'div', '', { id: 'correction', style })
  }

  switch (this.etat) {
    case 'enonce':
      // code exécuté au lancement de la section
      if (this.debutDeLaSection) {
        // Construction de la page
        parcours.construitStructurePage('presentation1bis')
        // compteur.
        sq.numEssai = 1
        sq.nbEssais = parseInt(parcours.donneesSection.nbEssais)
        sq.simplifier = parcours.donneesSection.simplifier
        sq.indicationErreurCrochets = parcours.donneesSection.indicationErreurCrochets
        // faut gérer sous-dossier ou pas…
        const sep = this.donneesSection.ex.includes('/') ? '-' : '/'
        j3pImporteAnnexe('squelettes-mtg32' + sep + this.donneesSection.ex + '.js').then(function (datasSection) {
          // console.log("initsection");
          // Attention, le this devient window...
          sq.txt = datasSection.txt
          sq.entete = datasSection.entete ?? ''
          sq.symbexact = datasSection.symbexact ?? '='
          sq.symbnonexact = datasSection.symbnonexact ?? '\\ne'
          sq.nbLatex = Number(datasSection.nbLatex ?? 0) // Le nombre de paramètres LaTeX dans le texte
          // ?? retourne l’opérande de gauche si !== undefined && !== null, celle de droite sinon
          const listeBoutons = []
          // boutons avec propriétés à true par défaut
          const tabBoutons1 = ['puissance', 'fraction', 'union']
          ;['btnPuis', 'btnFrac', 'BtnUnion'].forEach((p, i) => {
            const b = Boolean(datasSection[p] ?? true)
            if (b) listeBoutons.push(tabBoutons1[i])
          })
          // boutons avec false par défaut
          const tabBoutons2 = ['pi', 'racine', 'exp', 'ln', 'sin', 'cos', 'tan', 'abs', 'inf', 'R', 'bracket']
          ;['btnPi', 'btnRac', 'btnExp', 'btnLn', 'btnSin', 'btnCos', 'btnTan', 'btnAbs', 'btnInf', 'btnR', 'btnAcc'].forEach((p, i) => {
            const b = Boolean(datasSection[p] ?? false)
            if (b) listeBoutons.push(tabBoutons2[i])
          })
          listeBoutons.push('vide')
          sq.listeBoutons = listeBoutons
          sq.consigne1 = datasSection.consigne1 ?? ''
          sq.consigne2 = datasSection.consigne2 ?? ''
          sq.consigne3 = datasSection.consigne3 ?? ''
          sq.consigne4 = datasSection.consigne4 ?? ''
          sq.titre = datasSection.titre ?? ''
          sq.bigSize = Boolean(datasSection.bigSize ?? false)
          sq.width = Number(datasSection.width ?? 700)
          sq.height = Number(datasSection.height ?? 900)
          sq.cbienmais = datasSection.cbienmais ?? 'C’est bien mais on pouvait  simplifier la réponse'
          sq.param = datasSection.param ?? '' // Chaine contenant les paramètres autorisés
          sq.charset = datasSection.charset ?? ''
          initMtg()
        }).catch(error => {
          console.error(error)
          j3pShowError('Impossible de charger les données de cette exercice')
        })
        // return;
      } else {
        j3pEmpty('correction')
        sq.numEssai = 1
        cacheSolution()
        videEditeur()
        montreEditeur(true)
        sq.mtgAppLecteur.removeDoc('mtg32svg')
        sq.mtgAppLecteur.addDoc('mtg32svg', sq.txt, true)

        if (sq.aleat) {
          for (let i = 1; i <= sq.nbParamAleat; i++) {
            sq.mtgAppLecteur.giveFormula2('mtg32svg', 'r' + i, sq['tab' + i][this.questionCourante - 1])
          }
        }
        if (sq.param !== undefined) {
          const ch = 'abcdefghjklmnpqr'
          for (let i = 0; i < ch.length; i++) {
            const car = ch.charAt(i)
            if (sq.param.indexOf(car) !== -1) {
              const par = parcours.donneesSection[car]
              if (par !== 'random') sq.mtgAppLecteur.giveFormula2('mtg32svg', car, par)
            }
          }
        }

        sq.mtgAppLecteur.calculate('mtg32svg', true) // true pour que les calculs alèatoires soient réinitialisés
        j3pEmpty('enonce')
        j3pEmpty('debut')
        j3pEmpty('formules')
        j3pEmpty('info')
        sq.nbexp = 0
        sq.reponse = -1
        const nbParam = parseInt(sq.nbLatex)
        const t = ['a', 'b', 'c', 'd']
        const param = {}
        for (let i = 0; i < nbParam; i++) {
          const code = sq.mtgAppLecteur.getLatexCode('mtg32svg', i)
          if (i === 0) sq.aCalculer = code
          param[t[i]] = code
        }
        param.e = String(sq.nbEssais)
        const st = sq.consigne1 + (sq.simplifier ? sq.consigne3 : sq.consigne2) + sq.consigne4
        afficheMathliveDans('enonce', 'texte', st, param)
        if (sq.entete !== '') {
          afficheMathliveDans('debut', 'acalc', '$' + sq.entete + '$' + ' = ', param)
        } else {
          afficheMathliveDans('debut', 'acalc', '$' + sq.aCalculer + '=$ ', param)
        }
        afficheNombreObjetsRestants()
        j3pElement('info').style.display = 'block'
        sq.mtgAppLecteur.display('mtg32svg') // Pour tout réafficher
        // Pour revenir en haut de page
        try {
          window.location.hash = 'MepMG'
          j3pElement('MepMG').scrollIntoView()
        } catch (e) {
          console.warn(e) // pas normal mais pas grave
        }
        focusIfExists(inputId)
        this.finEnonce()
      }

      break // case "enonce":

    case 'correction': {
      let bilanReponse = ''
      let simplificationPossible = false
      if (validationEditeur()) {
        validation()
        if (sq.simplifier) {
          if (sq.resolu) { bilanReponse = 'exact' } else {
            if (sq.exact) {
              bilanReponse = 'exactPasFini'
              simplificationPossible = true
            } else { bilanReponse = 'faux' }
          }
        } else {
          if (sq.resolu || sq.exact) {
            bilanReponse = 'exact'
            if (!sq.resolu) simplificationPossible = true
          } else { bilanReponse = 'faux' }
        }
      } else {
        bilanReponse = 'incorrect'
      }
      // A cause de la limite de temps :
      if (this.isElapsed) { // limite de temps
        montreEditeur(false)
        this._stopTimer()
        j3pElement('correction').style.color = this.styles.cfaux
        j3pElement('boutonrecopier').style.display = 'none'
        j3pElement('info').style.display = 'none'
        afficheSolution(false)
        sq.mtgAppLecteur.executeMacro('mtg32svg', 'solution')
        j3pElement('correction').innerHTML = tempsDepasse
        j3pElement('correction').innerHTML += '<br>La solution se trouve ci-contre.'
        this.etat = 'navigation'
        this.sectionCourante()
      } else {
        if (bilanReponse === 'incorrect') {
          j3pElement('correction').style.color = this.styles.cfaux
          j3pElement('correction').innerHTML = 'Réponse incorrecte' + (sq.erreurOrdreBornes ? '<br>Une borne gauche est supérieure<br>à une borne droite ' : '')
        } else {
          // Une réponse a été saisie
          // Bonne réponse
          if (bilanReponse === 'exact') {
            this._stopTimer()
            this.score++
            j3pElement('correction').style.color = this.styles.cbien
            if (simplificationPossible) j3pElement('correction').innerHTML = sq.cbienmais
            else j3pElement('correction').innerHTML = cBien
            j3pElement('boutonrecopier').style.display = 'none'
            j3pElement('info').style.display = 'none'
            afficheSolution(true)
            sq.mtgAppLecteur.executeMacro('mtg32svg', 'solution')
            montreEditeur(false)
            sq.numEssai++ // Pour un affichage correct dans afficheReponse
            afficheReponse('exact', false)
            this.etat = 'navigation'
            this.sectionCourante()
          } else {
            sq.numEssai++
            if (bilanReponse === 'exactPasFini') {
              j3pElement('correction').style.color =
                (sq.numEssai <= sq.nbEssais) ? '#0000FF' : this.styles.cfaux
              j3pElement('correction').innerHTML = 'Le résultat est bon mais pas écrit sous la forme demandée.'
            } else {
              j3pElement('correction').style.color = this.styles.cfaux
              j3pElement('correction').innerHTML = sq.presqueResolu ? 'La réponse est fausse à cause d’un problème de crochets' : cFaux
            }
            j3pEmpty('info')
            if (sq.numEssai <= sq.nbEssais) {
              afficheNombreObjetsRestants()
              videEditeur()
              afficheReponse(bilanReponse, false)
              // S’il y a plus de deux chances,on appelle à nouveau le bouton Valider
              j3pElement('correction').innerHTML += '<br>' + '<br>' + essaieEncore
              focusIfExists(inputId)
            } else {
              // Erreur au nème essai
              this._stopTimer()
              j3pElement('boutonrecopier').style.display = 'none'
              j3pElement('info').style.display = 'none'
              afficheSolution(false)
              sq.mtgAppLecteur.executeMacro('mtg32svg', 'solution')
              montreEditeur(false)
              afficheReponse(bilanReponse, true)
              j3pElement('correction').innerHTML += '<br><br>La solution se trouve ci-contre.'
              this.etat = 'navigation'
              this.sectionCourante()
            }
          }
        }
      }
      // Obligatoire
      this.finCorrection()
      break // case "correction":
    }

    case 'navigation':
      if (!this.sectionTerminee()) {
        this.etat = 'enonce'
      }
      this.finNavigation()
      break // case "navigation":
  }
}
