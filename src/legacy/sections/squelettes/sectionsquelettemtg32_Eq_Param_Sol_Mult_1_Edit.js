import $ from 'jquery'

import { j3pAddElt, j3pAjouteBouton, j3pDiv, j3pElement, j3pEmpty, j3pImporteAnnexe, j3pPaletteMathquill, j3pShowError } from 'src/legacy/core/functions'
import ValidationZones from 'src/legacy/outils/calculatrice/ValidationZones'
import { unLatexify } from 'src/lib/mathquill/functions'
import { MathfieldElement, renderMathInDocument } from 'mathlive'
import { getMtgApp } from 'src/lib/outils/mathgraph'
import { j3pCreeSVG } from 'src/legacy/core/functionsSvg'
import { cleanLatexForMl, mathliveRestrict } from 'src/lib/outils/mathlive/utils'
import { afficheMathliveDans, getMathliveValue } from 'src/lib/outils/mathlive/display'
import { setMathliveCss } from 'src/lib/outils/mathlive/mathlivecss'
import { resetKeyboardPosition } from 'src/lib/widgets/mlVirtualKeyboard/VirtualKeyboard'
import textesGeneriques from 'src/lib/core/textes'
import { focusIfExists } from 'src/lib/utils/dom/main'

const { cBien, cFaux, essaieEncore, tempsDepasse } = textesGeneriques

/**
 Yves Biton
 Mars 2019. REvu en 2023 pour utiliser Mathlive au lieu de Mathquill

 squelettemtg32_Eq_Param_Sol_Mult_1_Edit
 Squelette demandant s’il y a ou non au moins une solution à un problème (le plus souvent une équation) et ensuite demandant la valeur exacte
 des solutions. Un seul éditeur est utilisé et les solutions doivent être séparées par des ;
 Utilise une fichier annexe dont le nom est contenu dans le paramètre ex.
 Ce fichier annexe contient dans la variable txt le code Base 64 de la figure mtg32 associée et dans la variable param
 le nom des calculs à une lettre qui peuvent être paramétrés par l’utilisateur à choisir parmi les lettres abcdefghijklmnopq.
 Pour savoir quel est le nombre de solution on interroge la valeur du calcul nbSol de la figure (0 s’il n’y a pas de solution).
 Pour savoir si une réponse est exacte et bien écrite sous la forme demandée, on interroge la valeur du calcul resolu qui vaut 0
 Si le calcul ne correspond pas à une forme attendue et sinon renvoie le n° correspondant de la solution (de 1 à nbsol).
 Pour savoir si une réponse est exacte, c’est-à-dire contient une formule du type x= ou =x donnant une des solutions,
 on met sa formule dans le calcul rep et on interroge, après recalcul de la figure, la valeur
 du calcul exact qui vaut 0 si la formule proposée ne correspond pas à une des solutions et sinon renvoie le n° correspondant de la solution (de 1 à nbsol).
 Par exemple, exact contiendra 1 si dans la réponse on a une réponse de la forme x = valeur de la solution 1.
 Si, par exemple exact vaut 1 et resolu ne vaut pas 1, c’estq ue l’élève a bien écrit x = valeur de la solution 1 mais ne l’a
 pas écrit sous la forme la plus simple demandée.
 L’élève peut aussi entrer des équations intermédiaires. Si, par exemple, nbSol vaut 2, la figure mtg32 doit contenir un calcul nommé racine1
 qui vaut 1 si la première solution est racine de l’équation proposée et 0 sinon et un calcul racine2 contenant 1 si la deuxième solution
 est racine de l’équation proposée et 0 sinon. Une réponse intermédiaire (avec des équations séparées par des ;) est acceptée si toutes les
 solutions de l’équation sont racines d’au moins une des équations proposées entre les;
 Si la figure contenue dans ex contient un paramètre nbvar , alors dans le cas ou nbrepetitions > 1, on donne pour i compris entre 1 et nbvar
 au calcul de la figure nommé "r"+i (par exemple r1 et r2 si nbParamAleat vaut 2) des valeurs comprises entre 0 et "nbcas"+i - 1
 toutes différentes (sauf si nbrepetitions > b=nbcas auquel cas au bout de nbcas répétitions il y aura nécessairement une valeur de r déjà prise.
 */

// on définit le dossier des fonts mathlive (il faut le faire très tôt,
// ici c’est bon avec pnpm start mais trop tard pour un build (cf src/lib/player/index.js de la branche draftPlayer)
if (MathfieldElement) { // undefined dans les tests avec happy-dom
  MathfieldElement.fontsDirectory = '/static/mathlive/fonts'
  MathfieldElement.soundsDirectory = '/static/mathlive/sounds'
}

export const params = {
  /**
   * Ce qui est paramétrable dans l’éditeur de graphe
   * @type {LegacySectionParamsParametres}
   */
  parametres: [
    ['nbrepetitions', 1, 'entier', 'aucun'],
    ['limite', 0, 'entier', 'Temps disponible par répétition, en secondes'],
    ['nbEssais', 4, 'entier', 'Nombre d’essais maximum autorisés'],
    ['simplifier', true, 'boolean', 'true si on exige un forme donnée, false sinon'],
    ['infoParam', 'Information sur le rôle des paramètres', 'string', 'Information sur l’utilisation des paramètres suivants'],
    ['a', 'random', 'string', 'Valeur de a'],
    ['b', 'random', 'string', 'Valeur de b'],
    ['c', 'random', 'string', 'Valeur de c'],
    ['d', 'random', 'string', 'Valeur de d'],
    ['e', 'random', 'string', 'Valeur de e'],
    ['f', 'random', 'string', 'Valeur de f'],
    ['g', 'random', 'string', 'Valeur de g'],
    ['h', 'random', 'string', 'Valeur de h'],
    ['j', 'random', 'string', 'Valeur de j'],
    ['k', 'random', 'string', 'Valeur de k'],
    ['l', 'random', 'string', 'Valeur de l'],
    ['m', 'random', 'string', 'Valeur de m'],
    ['n', 'random', 'string', 'Valeur de n'],
    ['p', 'random', 'string', 'Valeur de p'],
    ['q', 'random', 'string', 'Valeur de q'],
    ['r', 'random', 'string', 'Valeur de r'],
    ['ex', 'terminale/Equation_Ln_Ou_Exp_Simple_2', 'string', 'Nom du fichier annexe (xxx => squelettes-mtg32/xxx.js et xxx/yyy => squelettes-mtg32-xxx/yyy.js)']
  ]
}

const inputId = 'expressioninputmq1'

/**
 * section squelettemtg32_Eq_Param_Sol_Mult_1_Edit
 * @this {Parcours}
 */
export default function main () {
  const parcours = this
  const sq = this.storage

  function onkeyup (ev) {
    if (sq.marked) demarqueEditeurPourErreur()
    if (ev.keyCode === 13) { // Touche Enter
      if (!validationEditeur()) marqueEditeurPourErreur()
    }
  }

  function afficheNombreEssaisRestants () {
    j3pEmpty('divInfo')
    const nbe = sq.nbEssais - sq.numEssai + 1
    if (nbe === 1) afficheMathliveDans('divInfo', 'texteinfo', 'Il reste un essai.', { style: { color: '#7F007F' } })
    else afficheMathliveDans('divInfo', 'texteinfo', 'Il reste ' + nbe + ' essais.', { style: { color: '#7F007F' } })
  }

  function validationEditeur () {
    sq.signeEgalOublie = false
    sq.auMoins2Eg = false
    let res = true
    const rep = getMathliveValue(inputId)
    if (rep === '') {
      res = false
    } else {
      const tab = rep.split(';')
      for (let i = 0; (i < tab.length) && res; i++) {
        if (tab[i] === '') {
          res = false
        } else {
          const chcalcul = traiteMathlive(tab[i])
          const ch = chcalcul
          let nbegal = 0
          let k = ch.indexOf('=')
          while (k !== -1) {
            nbegal++
            k = ch.indexOf('=', k + 1)
          }
          if (nbegal === 0) {
            res = false
            sq.signeEgalOublie = true
          } else {
            if (nbegal > 1) {
              res = false
              sq.auMoins2Eg = true
            } else {
              const valide = sq.mtgAppLecteur.syntaxValidation('mtg32svg', 'rep', chcalcul, true)
              if (!valide) {
                res = false
              }
            }
          }
        }
      }
    }
    if (!res) {
      marqueEditeurPourErreur()
      focusIfExists(inputId)
    }
    return res
  }

  function validation () {
    let j, chcalcul, k, ind

    function tabContient (tab, ent) {
      for (let i = 1; i <= tab.length; i++) {
        if (tab[i - 1] === ent) return true
      }
      return false
    }

    if (sq.numEssai >= 1) {
      j3pElement('boutonrecopier').style.display = 'block'
    }

    sq.rep = [] // Tableau destiné à recevoir les réponses dans les champs d’édition des solutions
    sq.reponseExacte = []
    sq.reponseResolue = []
    sq.reponseCorrecte = []
    const rep = getMathliveValue(inputId)
    sq.rep = rep
    const tab = rep.split(';')
    sq.correct = true
    for (j = 0; j < tab.length; j++) {
      chcalcul = traiteMathlive(tab[j])
      sq.mtgAppLecteur.giveFormula2('mtg32svg', 'rep', chcalcul)
      sq.mtgAppLecteur.calculate('mtg32svg', false)
      let auMoinsUneCorrecte = false
      for (k = 1; k <= sq.nbSol; k++) {
        const estrac = sq.mtgAppLecteur.valueOf('mtg32svg', 'racine' + k)
        auMoinsUneCorrecte = auMoinsUneCorrecte || (estrac === 1)
        sq.reponseCorrecte.push(estrac === 1 ? k : 0)
      }
      sq.correct = sq.correct && auMoinsUneCorrecte
    }
    if (sq.correct) {
      for (ind = 1; ind <= sq.nbSol; ind++) {
        sq.correct = sq.correct && tabContient(sq.reponseCorrecte, ind)
      }
    }
    if (tab.length !== sq.nbSol) {
      sq.exact = false
      sq.resolu = false
    } else {
      for (j = 0; j < tab.length; j++) {
        chcalcul = traiteMathlive(tab[j])
        sq.mtgAppLecteur.giveFormula2('mtg32svg', 'rep', chcalcul)
        sq.mtgAppLecteur.calculate('mtg32svg', false)
        sq.reponseExacte.push(sq.mtgAppLecteur.valueOf('mtg32svg', 'exact'))
        sq.reponseResolue.push(sq.mtgAppLecteur.valueOf('mtg32svg', 'resolu'))
      }
      sq.exact = true
      sq.resolu = true
      for (ind = 1; ind <= sq.nbSol; ind++) {
        sq.exact = sq.exact && tabContient(sq.reponseExacte, ind)
        sq.resolu = sq.resolu && tabContient(sq.reponseResolue, ind)
      }
    }
  }

  function marqueEditeurPourErreur () {
    sq.marked = true
    const mf = j3pElement(inputId)
    if (!mf) return
    mf.style.backgroundColor = '#FF9999'
    mf.demarqueErreur = () => {
      if (sq.marked) {
        demarqueEditeurPourErreur()
      }
    }
  }

  function demarqueEditeurPourErreur () {
    sq.marked = false
    j3pEmpty('correction')
    const mf = j3pElement(inputId)
    if (!mf) return
    mf.style.backgroundColor = 'white'
  }

  function traiteMathlive (ch) {
    // Mathlive ne met pas d’accolades pour les fractions dont le numérateur et le dénominateur sont des entiers
    ch = ch.replace(/\\frac(\d)(\d)/g, '\\frac{$1' + '}{' + '$2}')
    // Ni pour les racines carrées d’un entier
    ch = ch.replace(/\\sqrt(\d)/g, '\\sqrt{$1}')
    // Pour les puissances dont l’exposant est un chiffre, Mathlive ne met pas d’accolades
    ch = ch.replace(/\^([^{])/g, '^{$1}')
    ch = unLatexify(ch)
    // mathlive peut mettre des {} vides
    ch = ch.replace(/\\{\\}/g, '')
    ch = ch.replace(/\^(\d)(\d)/g, '^$1*$2') // On peut avoir par exemple 2^35^4 pour 2^3*5^4
    return sq.mtgAppLecteur.addImplicitMult('mtg32svg', ch)// Traitement des multiplications implicites
  }

  /**
     * Fonction extrayant d’un affichage LaTeX de la figure les lignes de texte et les transformant
     * en une chaîne affichable par MathQuill
     * @param tagLatex : le tag de l’affichage LaTeX de la figure
     * @return {string}
     */
  function extraitDeLatexPourMathlive (tagLatex) {
    const list = sq.mtgAppLecteur.getList('mtg32svg')
    const latex = list.getByTag(tagLatex, true)
    if (latex === null || latex.className !== 'CLatex') return ''
    return cleanLatexForMl(latex.chaineLatex, sq.charset)
  }

  function afficheReponse (bilan, depasse) {
    let ch, coul
    if ((bilan === 'exact')) {
      ch = 'Exact : '
      coul = parcours.styles.cbien
    } else {
      if (bilan === 'correct') {
        ch = 'Vérifié par les ' + sq.nomSolutions + ' : '
        coul = depasse ? parcours.styles.cfaux : '#0000FF'
      } else {
        if (bilan === 'faux') {
          ch = 'Faux : '
          coul = parcours.styles.cfaux
        } else { // exact pas fini
          ch = 'Exact pas fini : '
          coul = depasse ? parcours.styles.cfaux : '#0000FF'
        }
      }
    }
    const num = sq.numEssai
    if (num > 2) ch = '<br>' + ch
    const idrep = 'exp' + num
    afficheMathliveDans('formules', idrep, ch + '$' + sq.rep + '$', {
      style: {
        color: coul
      }
    })
    resetKeyboardPosition()
  }

  function montreEditeur (bVisible) {
    if (bVisible) $('#divInfo').css('display', 'block')
    $('#editeur').css('display', bVisible ? 'block' : 'none')
    if (bVisible) {
      $('#boutonsmathquill').css('display', 'block')
      // On vide le contenu de l’éditeur MathQuill et on lui donne le focus.
      j3pElement(inputId).value = ''
      focusIfExists(inputId)
    }
  }

  function videEditeur () {
    j3pElement(inputId).value = ''
    demarqueEditeurPourErreur()
    focusIfExists(inputId)
  }

  function recopierReponse () {
    demarqueEditeurPourErreur()
    const mf = j3pElement(inputId)
    if (!mf) return
    mf.value = sq.rep
    focusIfExists(inputId)
  }

  function afficheSolution (bexact) {
    $('#divSolution').css('display', 'block').html('')
    const ch = extraitDeLatexPourMathlive('solution')
    if (ch !== '') {
      afficheMathliveDans('divSolution', 'solution', ch, { style: { color: bexact ? 'green' : 'blue' } })
    }
  }

  function cacheSolution () {
    $('#divSolution').css('display', 'none').html('')
  }

  function initMtg () {
    setMathliveCss('MepMG')
    const mtgOptions = {
      // player only, sans figure initiale ni svg, on gère ça dans initMtg et modif_fig
      loadCoreWithMathJax: true
    }
    getMtgApp('mtg32svg', {}, mtgOptions).then((mtgAppLecteur) => {
      sq.mtgAppLecteur = mtgAppLecteur
      initDom()
      sq.marked = false
      sq.nbSolEntre = false // Sera mis à true quand le nombre de solutions aura été proposé par l’élève.
      // sous la forme demandée. Ne sert que si sq.simplifier est à true;
      let par, car, i, j, k, nbrep, ar, tir, nb, nbcas
      // On crée provisoirement le DIV ci-dessous pour pouvoir calculer la figure.
      // On le détruira ensuite pour le récréer à la bonne position
      j3pDiv('conteneur', {
        id: 'formules',
        contenu: '',
        style: parcours.styles.petit.enonce
      }) // Contient le formules entrées par l’élève
      j3pDiv('conteneur', 'divSolution', '')
      $('#divSolution').css('display', 'none')
      $('#divSolution').css('fontSize', '22px')
      j3pDiv('conteneur', 'divmtg32', '')
      j3pCreeSVG('divmtg32', {
        id: 'mtg32svg',
        width: sq.width,
        height: sq.height
      })
      sq.mtgAppLecteur.removeAllDoc()
      sq.mtgAppLecteur.addDoc('mtg32svg', sq.txt, true)
      sq.mtgAppLecteur.calculate('mtg32svg', true)
      const ch = 'abcdefghjklmnpqr'
      const nbvar = sq.mtgAppLecteur.valueOf('mtg32svg', 'nbvar')
      if (nbvar !== -1) {
        nbrep = parcours.donneesSection.nbrepetitions
        sq.aleat = true
        sq.nbParamAleat = nbvar
        for (i = 1; i <= nbvar; i++) {
          nbcas = sq.mtgAppLecteur.valueOf('mtg32svg', 'nbcas' + i)
          nb = Math.max(nbrep, nbcas)
          ar = []
          for (j = 0; j < nb; j++) ar.push(j % nbcas)
          sq['tab' + i] = []
          for (k = 0; k < nbrep; k++) {
            tir = Math.floor(Math.random() * ar.length)
            sq['tab' + i].push(ar[tir])
            ar.splice(tir, 1)
          }
          sq.mtgAppLecteur.giveFormula2('mtg32svg', 'r' + i, sq['tab' + i][0])
        }
      } else {
        sq.aleat = false
      }

      if (sq.param) {
        for (i = 0; i < ch.length; i++) {
          car = ch.charAt(i)
          if (sq.param.indexOf(car) !== -1) {
            par = parcours.donneesSection[car]
            if (par !== 'random') sq.mtgAppLecteur.giveFormula2('mtg32svg', car, par)
          }
        }
      }
      sq.mtgAppLecteur.calculate('mtg32svg', true)

      sq.nbSol = sq.mtgAppLecteur.valueOf('mtg32svg', 'nbSol') // Le nombre de solutions de l’équation.

      const tab = ['Choisir ci-dessous s’il y a ou non des ' + sq.nomSolutions, sq.consigneNbSol0, sq.consigneNbSol1]
      const nbLatex = parseInt(sq.nbLatex)
      // La consigne peut contenir jusqu'à 4 paramètres a, b,c et d récupérés comme affichages LaTeX de la figure.
      const param = []
      for (i = 0; i < nbLatex; i++) {
        param.push(sq.mtgAppLecteur.getLatexCode('mtg32svg', i))
      }
      afficheMathliveDans('divConsigneNbSol', 'consigneNbSol', sq.consigne1 + '<br>#1#',
        {
          liste1: { texte: tab },
          a: param[0],
          b: param[1],
          c: param[2],
          d: param[3]
        })
      j3pElement('consigneNbSolliste1').reponse = (sq.nbSol === 0) ? 1 : 2
      sq.fcts_valid = new ValidationZones({ parcours, zones: ['consigneNbSolliste1'] })

      // Le div qui contiendra l’éditeur MathQuill.
      // Cet éditeur sera masqué au début.
      j3pDiv('conteneur', 'conteneurbouton', '')
      j3pAjouteBouton('conteneurbouton', 'boutonrecopier', 'MepBoutonsRepere', 'Recopier réponse précédente',
        recopierReponse)
      j3pElement('boutonrecopier').style.display = 'none'

      j3pDiv('conteneur', 'editeur', '')
      j3pElement('editeur').style.fontSize = '24px'
      afficheMathliveDans('editeur', 'expression', 'Réponse proposée : &1&',
        {
          charset: sq.charset,
          listeBoutons: sq.listeBoutons
        })
      if (sq.charset !== '') {
        mathliveRestrict(inputId, sq.charset)
      }
      j3pDiv('editeur', 'boutonsmathquill', '')
      // Au départ les boutons MathQuill sont masqués
      $('#boutonsmathquill').css('display', 'none').css('padding-top', '10px') // Pour laisser un peu de place au-dessus des boutons
      if (sq.boutonsMathQuill) {
        j3pPaletteMathquill('boutonsmathquill', inputId, {
          liste: sq.listeBoutons,
          nomdiv: 'palette'
        })
      }
      // Ligne suivante pour un bon alignement
      $('#palette').css('display', 'inline-block')
      j3pElement(inputId).onkeyup = function (ev) {
        onkeyup.call(this, ev, parcours)
      }
      // Au départ les éditeurs sont masqués
      $('#editeur').css('padding-top', '10px').css('display', 'none')

      // ON récupère le div de la figure pour le reclasser après les autres div
      const div = j3pElement('conteneur').removeChild(j3pElement('divmtg32'))
      j3pElement('conteneur').appendChild(div)
      sq.mtgAppLecteur.display('mtg32svg')
      parcours.finEnonce()
    }).catch(j3pShowError)
  }

  function initDom () {
    renderMathInDocument({ renderAccessibleContent: '' })
    if (sq.titre) parcours.afficheTitre(sq.titre)

    j3pDiv(parcours.zones.MG, {
      id: 'conteneur',
      contenu: '',
      style: parcours.styles.etendre('toutpetit.enonce', { padding: '6px' })
    })
    // Quatre lignes suivantes déplacées ici depuis initMtg pour éviter que j3pAffiche("", "divInfo" ne soit appelé avant la création du div divInfo
    j3pDiv('conteneur', 'divConsigneNbSol', '') // Le div qui contiendra la liste déroulante pour le nombre de solutions
    j3pDiv('conteneur', 'divSuiteEnonce', '')
    j3pDiv('conteneur', 'divInfo', '')
    const style = parcours.styles.petit.correction
    style.marginTop = '1.5em'
    style.marginLeft = '0.5em'
    j3pAddElt(parcours.zonesElts.MD, 'div', '', { id: 'correction', style })
  }

  switch (this.etat) {
    case 'enonce':
      // code exécuté au lancement de la section
      if (this.debutDeLaSection) {
        // Construction de la page
        parcours.construitStructurePage('presentation1bis')
        // compteur. Si   numEssai > nbEssais, on corrige
        sq.numEssai = 1

        sq.nbEssais = parseInt(parcours.donneesSection.nbEssais)
        sq.simplifier = parcours.donneesSection.simplifier
        // faut gérer sous-dossier ou pas…
        const sep = this.donneesSection.ex.includes('/') ? '-' : '/'
        j3pImporteAnnexe('squelettes-mtg32' + sep + this.donneesSection.ex + '.js').then(function (datasSection) {
          // console.log("initsection");
          // Attention, le this devient window...
          sq.txt = datasSection.txt
          sq.width = Number(datasSection.width ?? 750)
          sq.height = Number(datasSection.height ?? 900)
          sq.nomSolutions = datasSection.nomSolutions ?? 'solutions'
          sq.consigneNbSol0 = datasSection.consigneNbSol0
          sq.consigneNbSol1 = datasSection.consigneNbSol1
          sq.phraseSolution0 = datasSection.phraseSolution0
          sq.phraseSolution1 = datasSection.phraseSolution1
          sq.faux = datasSection.faux ?? 'C’est faux'
          sq.juste = datasSection.juste ?? 'C’est juste'
          sq.nbLatex = Number(datasSection.nbLatex ?? 0) // Le nombre de paramètres LaTeX dans le texte
          // ?? retourne l’opérande de gauche si !== undefined && !== null, celle de droite sinon
          const listeBoutons = []
          // boutons avec propriétés à true par défaut
          const tabBoutons1 = ['puissance', 'fraction']
                    ;['btnPuis', 'btnFrac'].forEach((p, i) => {
            const b = Boolean(datasSection[p] ?? true)
            if (b) listeBoutons.push(tabBoutons1[i])
          })
          // boutons avec false par défaut
          const tabBoutons2 = ['pi', 'racine', 'exp', 'ln', 'sin', 'cos', 'tan', 'abs']
                    ;['btnPi', 'btnRac', 'btnExp', 'btnLn', 'btnSin', 'btnCos', 'btnTan', 'btnAbs'].forEach((p, i) => {
            const b = Boolean(datasSection[p] ?? false)
            if (b) listeBoutons.push(tabBoutons2[i])
          })
          sq.listeBoutons = listeBoutons
          sq.boutonsMathQuill = listeBoutons.length !== 0
          sq.consigne1 = datasSection.consigne1 ?? ''
          sq.consigne2 = datasSection.consigne2 ?? ''
          sq.consigne3 = datasSection.consigne3 ?? ''
          sq.consigne4 = datasSection.consigne4 ?? ''
          sq.consigne5 = datasSection.consigne5 ?? ''
          sq.titre = datasSection.titre ?? ''
          sq.param = datasSection.param ?? '' // Chaine contenant les paramètres autorisés
          sq.charset = datasSection.charset ?? ''
          initMtg()
        }).catch(error => {
          console.error(error)
          j3pShowError('Impossible de charger les données de cette exercice')
        })
        // return;
      } else {
        j3pEmpty('correction')
        sq.numEssai = 1
        cacheSolution()
        sq.nbSolEntre = false
        montreEditeur(false)
        sq.mtgAppLecteur.removeDoc('mtg32svg')
        sq.mtgAppLecteur.addDoc('mtg32svg', sq.txt, true)

        if (sq.aleat) {
          for (let i = 1; i <= sq.nbParamAleat; i++) {
            sq.mtgAppLecteur.giveFormula2('mtg32svg', 'r' + i, sq['tab' + i][this.questionCourante - 1])
          }
        }
        if (sq.param !== undefined) {
          const ch = 'abcdefghjklmnpqr'
          for (let i = 0; i < ch.length; i++) {
            const car = ch.charAt(i)
            if (sq.param.indexOf(car) !== -1) {
              const par = parcours.donneesSection[car]
              if (par !== 'random') sq.mtgAppLecteur.giveFormula2('mtg32svg', car, par)
            }
          }
        }

        sq.mtgAppLecteur.calculate('mtg32svg', true) // true pour que les calculs alèatoires soient réinitialisés

        j3pEmpty('divConsigneNbSol')
        j3pEmpty('divSuiteEnonce')
        j3pEmpty('formules')
        j3pEmpty('divInfo')
        // La consigne peut contenir jusqu'à 4 paramètres a, b,c et d récupérés comme affichages LaTeX de la figure.
        sq.nbSol = sq.mtgAppLecteur.valueOf('mtg32svg', 'nbSol') // Le nombre de solutions de l’équation.
        const tab = ['Choisir ci-dessous s’il y a ou non des ' + sq.nomSolutions, sq.consigneNbSol0, sq.consigneNbSol1]
        const nbLatex = parseInt(sq.nbLatex)
        // La consigne peut contenir jusqu'à 4 paramètres a, b,c et d récupérés comme affichages LaTeX de la figure.
        const param = []
        for (let i = 0; i < nbLatex; i++) {
          param.push(sq.mtgAppLecteur.getLatexCode('mtg32svg', i))
        }
        afficheMathliveDans('divConsigneNbSol', 'consigneNbSol', sq.consigne1 + '<br>#1#',
          {
            liste1: { texte: tab },
            a: param[0],
            b: param[1],
            c: param[2],
            d: param[3]
          })
        j3pElement('consigneNbSolliste1').typeReponse = [true]
        j3pElement('consigneNbSolliste1').reponse = (sq.nbSol === 0) ? 1 : 2
        sq.fcts_valid = new ValidationZones({ parcours, zones: ['consigneNbSolliste1'] })
        sq.mtgAppLecteur.display('mtg32svg') // Pour tout réafficher

        this.finEnonce()
      }

      break // case "enonce":

    case 'correction': {
      let bilanReponse = ''
      let simplificationPossible = false
      if (!sq.nbSolEntre) {
        sq.fcts_valid.validationGlobale()
        const rep = j3pElement('consigneNbSolliste1').selectedIndex - 1
        if (rep === -1) {
          bilanReponse = 'nbSolPasEntre'
        } else {
          if (rep === ((sq.nbSol >= 1) ? 1 : 0)) {
            if (rep === 0) bilanReponse = 'nbSolExactFini' // Cas où il n’y a pas de solution
            else bilanReponse = 'nbSolExact'
          } else {
            bilanReponse = 'nbSolFaux'
          }
        }
      } else {
        // On regarde d’abord l’éditeur a un contenu vide ou incorrect
        if (validationEditeur()) {
          validation()
          if (sq.simplifier) {
            if (sq.resolu) {
              bilanReponse = 'exact'
            } else {
              if (sq.exact) {
                bilanReponse = 'exactPasFini'
                simplificationPossible = true
              } else {
                if (sq.correct) bilanReponse = 'correct'
                else bilanReponse = 'faux'
              }
            }
          } else {
            if (sq.resolu || sq.exact) {
              bilanReponse = 'exact'
              if (!sq.resolu) simplificationPossible = true
            } else {
              if (sq.correct) bilanReponse = 'correct'
              else bilanReponse = 'faux'
            }
          }
        } else {
          bilanReponse = 'incorrect'
        }
      }
      // A cause de la limite de temps :
      if (this.isElapsed) { // limite de temps
        this._stopTimer()
        j3pElement('correction').style.color = this.styles.cfaux
        j3pElement('divInfo').style.display = 'none'
        j3pElement('boutonrecopier').style.display = 'none'
        montreEditeur(false)
        afficheSolution(false)
        sq.mtgAppLecteur.executeMacro('mtg32svg', 'solution')
        j3pElement('correction').innerHTML = tempsDepasse
        j3pElement('correction').innerHTML += '<br>La solution se trouve ci-contre.'
        this.finCorrection('navigation', true)
      }

      if (!sq.nbSolEntre) {
        if (bilanReponse !== 'nbSolPasEntre') {
          sq.nbSolEntre = true
          if (bilanReponse === 'nbSolExactFini') {
            sq.nbSolExact = true
            this._stopTimer()
            sq.mtgAppLecteur.setActive('mtg32svg', false)
            this.score++
            j3pElement('correction').style.color = this.styles.cbien
            j3pElement('correction').innerHTML = cBien
            afficheSolution(true)
            sq.mtgAppLecteur.executeMacro('mtg32svg', 'solution')
            // return this.finCorrection('navigation')
          } else {
            if (bilanReponse === 'nbSolExact') {
              this.score += 0.5
              sq.nbSolExact = true
              j3pElement('correction').style.color = this.styles.cbien
              j3pElement('correction').innerHTML = sq.juste + '<br>' + sq.phraseSolution1
              const nbParam = parseInt(sq.nbLatex)
              const t = 'abcd'
              const param = {}
              for (let i = 0; i < nbParam; i++) {
                param[t.charAt(i)] = sq.mtgAppLecteur.getLatexCode('mtg32svg', i)
              }
              param.e = String(sq.nbSol)
              const st = sq.consigne2 + (sq.simplifier ? sq.consigne3 : '') +
                  (sq.nbEssais > 1 ? sq.consigne4 : '') + sq.consigne5
              $('#divSuiteEnonce').css('color', 'black')
              afficheMathliveDans('divSuiteEnonce', 'texte', st, param)
            } else {
              sq.nbSolExact = false
              j3pElement('correction').style.color = this.styles.cfaux
              if (sq.nbSol === 0) { // Faux alors qu’il n’y a pas de solution
                j3pElement('correction').innerHTML = sq.faux + '<br>' + sq.phraseSolution0
              } else {
                j3pElement('correction').innerHTML = sq.faux + '<br>' + sq.phraseSolution1
                const nbParam = parseInt(sq.nbLatex)
                const t = 'abcd'
                const param = {}
                for (let i = 0; i < nbParam; i++) {
                  param[t.charAt(i)] = sq.mtgAppLecteur.getLatexCode('mtg32svg', i)
                }
                param.e = String(sq.nbSol)
                const st = sq.consigne2 + (sq.simplifier ? sq.consigne3 : '') +
                    (sq.nbEssais > 1 ? sq.consigne4 : '') + sq.consigne5
                $('#divSuiteEnonce').css('color', 'black')
                afficheMathliveDans('divSuiteEnonce', 'texte', st, param)
              }
            }
          }
          // On fait apparaître les éditeurs pour les solutions
          if (sq.nbSol === 0) {
            afficheSolution(false)
            sq.mtgAppLecteur.executeMacro('mtg32svg', 'solution')
            return this.finCorrection('navigation', true)
          } else {
            // On fait apparaître l’éditeur pour les solutions
            afficheNombreEssaisRestants()
            montreEditeur(true)
          }
        }
      } else {
        if (bilanReponse === 'incorrect') {
          j3pElement('correction').style.color = this.styles.cfaux
          if (sq.signeEgalOublie) {
            j3pElement('correction').innerHTML = 'Ne pas oublier d’entrer les réponses avec des égalités.'
          } else {
            if (sq.auMoins2Eg) {
              j3pElement('correction').innerHTML = 'Il faut une seule égalité par équation.'
            } else {
              j3pElement('correction').innerHTML = 'Réponse incorrecte'
            }
          }
        } else {
          // Une réponse a été saisie
          sq.numEssai++
          if (bilanReponse === 'exact') {
            this.score += 0.5
            this._stopTimer()
            sq.mtgAppLecteur.setActive('mtg32svg', false)
            afficheReponse('exact', false)
            j3pElement('boutonrecopier').style.display = 'none'
            j3pElement('correction').style.color = this.styles.cbien
            if (simplificationPossible) j3pElement('correction').innerHTML = 'C’est bien mais on pouvait  simplifier la réponse'
            else j3pElement('correction').innerHTML = cBien
            j3pElement('divInfo').style.display = 'none'
            afficheSolution(true)
            sq.mtgAppLecteur.executeMacro('mtg32svg', 'solution')
            montreEditeur(false)
            return this.finCorrection('navigation', true)
          }

          if (bilanReponse === 'exactPasFini') {
            j3pElement('correction').style.color =
                            (sq.numEssai <= sq.nbEssais) ? '#0000FF' : this.styles.cfaux
            j3pElement('correction').innerHTML = 'Le calcul est bon mais pas écrit sous la forme demandée.'
          } else {
            if (bilanReponse === 'correct') {
              j3pElement('correction').style.color =
                                (sq.numEssai <= sq.nbEssais) ? '#0000FF' : this.styles.cfaux
              j3pElement('correction').innerHTML = 'Calcul exact mais pas résolu.'
            } else {
              j3pElement('correction').style.color = this.styles.cfaux
              j3pElement('correction').innerHTML = cFaux
            }
          }
          j3pEmpty('divInfo')

          if (sq.numEssai <= sq.nbEssais) {
            afficheNombreEssaisRestants()
            videEditeur()
            afficheReponse(bilanReponse, false)
            // S’il y a plus de deux chances,on appelle à nouveau le bouton Valider
            j3pElement('correction').innerHTML += '<br>' + '<br>' + essaieEncore
            // sq.mtgAppLecteur.setActive("mtg32svg", true);
          } else {
            // Erreur au nème essai
            if (sq.nbSolExact) this.score -= 0.5// si nbSolExact on avait aujouté 1 ou 0.5 précédemment
            this._stopTimer()
            sq.mtgAppLecteur.setActive('mtg32svg', false)
            j3pElement('boutonrecopier').style.display = 'none'
            j3pElement('divInfo').style.display = 'none'
            afficheSolution(false)
            sq.mtgAppLecteur.executeMacro('mtg32svg', 'solution')
            montreEditeur(false)
            afficheReponse(bilanReponse, true)
            j3pElement('correction').innerHTML += '<br><br>La solution se trouve ci-contre.'
            return this.finCorrection('navigation', true)
          }
        }
      }
      // Obligatoire
      this.finCorrection()
      break // case "correction":
    }

    case 'navigation':
      if (!this.sectionTerminee()) {
        this.etat = 'enonce'
      }
      this.finNavigation()
      break // case "navigation":
  }
}
