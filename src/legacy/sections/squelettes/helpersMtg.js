import { unLatexify } from 'src/lib/mathquill/functions'
import { focusPlaceholder } from 'src/lib/outils/mathlive/utils'
import { focusIfExists } from 'src/lib/utils/dom/main'
import { j3pElement } from 'src/legacy/core/functions'

/**
 * Traitement des multiplications implicites par mtgAppLecteur (applique unLatexify et passe le résultat à mtgAppLecteur.addImplicitMult)
 * @param {MtgAppLecteur} mtgAppLecteur
 * @param {string} latex
 * @param {string} [svgId=mtg32svg]
 * @returns {string}
 */
export function unLatexifyAndAddImplicitMult (mtgAppLecteur, latex, svgId = 'mtg32svg') {
  return mtgAppLecteur.addImplicitMult(svgId, unLatexify(latex))
}

/**
 * Donne le focus à l’élément id ou à son placeholder si id contient 'ph' (avec id=XXXphYYY on considère que c’est l’id du placeholder et que l’élément mathfield est XXX)
 * @param id
 */
export function setFocusById (id) {
  if (id.includes('ph')) { // Cas d’un placeholder dans un éditeur mathfield
    const idph = id
    const ind = id.indexOf('ph')
    id = id.substring(0, ind)
    // id devient l’id de l’éditeur Mathlive qui contient le placeholder
    focusPlaceholder(id, idph)
  } else {
    focusIfExists(id)
  }
}

/**
 * Si l’input trouvé a la classe css PhBlock on prendra son premier placeholder pour lui donner le focus (sinon on donne le focus à l’input)
 * Ne fait rien si parent n’existe pas (ou plus) ou qu’il ne contient aucun input ni mathfield
 * @param {HTMLElement|string} parent l’élément dans lequel on prendra le premier mathfield ou le premier input texte
 */
export function setFocusToFirstInput (parent) {
  if (typeof parent === 'string') parent = j3pElement(parent, null)
  const editor = parent?.querySelector('math-field, input[type=text]')
  if (!editor) return
  if (editor.classList.contains('PhBlock')) {
    const idfirstPrompt = editor.getPrompts()[0]
    focusPlaceholder(editor, idfirstPrompt)
  } else {
    focusIfExists(editor)
  }
}
