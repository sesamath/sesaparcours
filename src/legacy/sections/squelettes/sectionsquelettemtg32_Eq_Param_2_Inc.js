import $ from 'jquery'
import { j3pAddElt, j3pAjouteBouton, j3pDiv, j3pElement, j3pEmpty, j3pImporteAnnexe, j3pPaletteMathquill, j3pShowError } from 'src/legacy/core/functions'
import { MathfieldElement, renderMathInDocument } from 'mathlive'
import { unLatexify } from 'src/lib/mathquill/functions'
import { getMtgApp } from 'src/lib/outils/mathgraph'
import { j3pCreeSVG } from 'src/legacy/core/functionsSvg'
import { afficheMathliveDans, getMathliveValue } from 'src/lib/outils/mathlive/display'
import { cleanLatexForMl, mathliveRestrict } from 'src/lib/outils/mathlive/utils'
import { setMathliveCss } from 'src/lib/outils/mathlive/mathlivecss'
import { resetKeyboardPosition } from 'src/lib/widgets/mlVirtualKeyboard/VirtualKeyboard'
import textesGeneriques from 'src/lib/core/textes'
import { focusIfExists } from 'src/lib/utils/dom/main'

const { cBien, cFaux, essaieEncore, tempsDepasse } = textesGeneriques

/**
 Yves Biton
 Mars 2019. Revu en 2023 pour massage à Mathlive.

 squelettemtg32_Eq_Param_2_Inc
 Squelette demandant de calcuer deux inconnues indépendantes l’une de l’autre
 On peut éventuellement spécifier des paramètres  a,b,c,d,e,f,g,h,j,k,n,n,p.
 ex est la référence à un fichier annexe qui comprend entre autres une figure mtg32 chargée de proposée l’exercice et de vérifier la validité des solutions proposées
 Pour savoir la réponse est exacte, c’est-à-dire contient une formule du type x= ou =x donnant une des solutions,
 on met la formule donnée pour la première inconnue dans dans le calcul rep1 et la formule donnée pour la seconde inconnu dans rep2
 et on interroge, après recalcul de la figure, la valeur du calcul exact qui vaut 1 la réponse est exacte et 0 si elle est fausse.
 Pour savoir si une réponse est exacte et bien écrite sous la forme demandée, on interroge la valeur du calcul resolu qui vaut 0
 Si le calcul ne correspond pas à une forme attendue et sinon renvoie 1.
 L’élève peut aussi entrer de équations intermédiaires. Dans ce cas le calcul correct renvoie 1 si les valeurs cherchées
 vérifient bien les deux équations correspondantes et 0 sinon;
 Si la figure contenue dans ex contient un paramètre nbvar , alors dans le cas ou nbrepetitions > 1, on donne pour i compris entre 1 et nbvar
 au calcul de la figure nommé "r"+i (par exemple r1 et r2 si nbParamAleat vaut 2) des valeurs comprises entre 0 et "nbcas"+i - 1
 toutes différentes (sauf si nbrepetitions > b=nbcas auquel cas au bout de nbcas répétitions il y aura nécessairement une valeur de r déjà prise.
 */

// on définit le dossier des fonts mathlive (il faut le faire très tôt,
// ici c’est bon avec pnpm start mais trop tard pour un build (cf src/lib/player/index.js de la branche draftPlayer)
if (MathfieldElement) { // undefined dans les tests avec happy-dom
  MathfieldElement.fontsDirectory = '/static/mathlive/fonts'
  MathfieldElement.soundsDirectory = '/static/mathlive/sounds'
}

export const params = {
  /**
   * Ce qui est paramétrable dans l’éditeur de graphe
   * @type {LegacySectionParamsParametres}
   */
  parametres: [
    ['nbrepetitions', 1, 'entier', 'aucun'],
    ['limite', 0, 'entier', 'Temps disponible par répétition, en secondes'],
    ['nbEssais', 4, 'entier', 'Nombre d’essais maximum autorisés'],
    ['simplifier', true, 'boolean', 'true si on exige un forme donnée, false sinon'],
    ['a', 'random', 'string', 'Valeur de a'],
    ['b', 'random', 'string', 'Valeur de b'],
    ['c', 'random', 'string', 'Valeur de c'],
    ['d', 'random', 'string', 'Valeur de d'],
    ['e', 'random', 'string', 'Valeur de e'],
    ['f', 'random', 'string', 'Valeur de f'],
    ['g', 'random', 'string', 'Valeur de g'],
    ['h', 'random', 'string', 'Valeur de h'],
    ['j', 'random', 'string', 'Valeur de j'],
    ['k', 'random', 'string', 'Valeur de k'],
    ['l', 'random', 'string', 'Valeur de l'],
    ['m', 'random', 'string', 'Valeur de m'],
    ['n', 'random', 'string', 'Valeur de n'],
    ['p', 'random', 'string', 'Valeur de p'],
    ['q', 'random', 'string', 'Valeur de q'],
    ['r', 'random', 'string', 'Valeur de r'],
    ['ex', 'Lycee_Vecteurs_Calc_Coord_Point_3', 'string', 'Nom du fichier annexe (xxx => squelettes-mtg32/xxx.js et xxx/yyy => squelettes-mtg32-xxx/yyy.js)']
  ]
}

const inputId = 'expressioninputmq1'

/**
 * section squelettemtg32_Eq_Param_2_Inc
 * @this {Parcours}
 */
export default function main () {
  const parcours = this
  const sq = this.storage

  function onkeyup () {
    const indice = this.indice // Indice de l’éditeur qui a appelé la fonction
    if (sq.marked[indice]) { demarqueEditeurPourErreur(indice) }
  }

  /**
   * Fonction extrayant d’un affichage LaTeX de la figure les lignes de texte et les transformant
   * en une chaîne affichable par MathQuill
   * @param {string} tagLatex le tag de l’affichage LaTeX de la figure
   * @return {string}
   */
  function extraitDeLatexPourMathQuill (tagLatex) {
    const list = sq.mtgAppLecteur.getList('mtg32svg')
    const latex = list.getByTag(tagLatex)
    if (latex === null || latex.className !== 'CLatex') return ''
    return cleanLatexForMl(latex.chaineLatex)
  }

  function afficheNombreEssaisRestants () {
    j3pEmpty('info')
    const nbe = sq.nbEssais - sq.numEssai + 1
    if (nbe === 1) afficheMathliveDans('divInfo', 'texteinfo', 'Il reste un essai.', { style: { color: '#7F007F' } })
    else afficheMathliveDans('divInfo', 'texteinfo', 'Il reste ' + nbe + ' essais.', { style: { color: '#7F007F' } })
  }

  function validationEditeurs () {
    let res = true
    let editorFocusedonError = false
    for (let ind = 1; ind <= 2; ind++) {
      const id = 'expression' + ind + 'inputmq1'
      const rep = getMathliveValue(id)
      if (rep === '') {
        res = false
        if (!editorFocusedonError) {
          marqueEditeurPourErreur(ind)
          editorFocusedonError = true
          setTimeout(function () {
            j3pElement(id).focus()
          })
        }
      } else {
        const chcalcul = traiteMathlive(rep)
        if (chcalcul.indexOf('=') === -1) {
          res = false
          sq.signeEgalOublie = true
          if (!editorFocusedonError) {
            marqueEditeurPourErreur(ind)
            editorFocusedonError = true
            setTimeout(function () {
              j3pElement(id).focus()
            })
          }
        } else {
          const valide = sq.mtgAppLecteur.syntaxValidation('mtg32svg', 'rep' + ind, chcalcul, true)
          if (!valide) {
            res = false
            if (!editorFocusedonError) {
              marqueEditeurPourErreur(ind)
              editorFocusedonError = true
              setTimeout(function () {
                j3pElement(id).focus()
              })
            }
          }
        }
      }
    }
    return res
  }

  function validation () {
    sq.rep = [] // Tableau destiné à recevoir les réponses dans les champs d’édition des solutions
    for (let ind = 1; ind <= 2; ind++) {
      const rep = getMathliveValue('expression' + ind + 'inputmq1')
      const chcalcul = traiteMathlive(rep)
      sq.mtgAppLecteur.giveFormula2('mtg32svg', 'rep' + ind, chcalcul)
      sq.rep.push(rep)
    }
    sq.mtgAppLecteur.calculate('mtg32svg', false)
    sq.resolu = sq.mtgAppLecteur.valueOf('mtg32svg', 'resolu') === 1
    sq.exact = sq.mtgAppLecteur.valueOf('mtg32svg', 'exact') === 1
    sq.correct = sq.mtgAppLecteur.valueOf('mtg32svg', 'correct') === 1
    if (sq.numEssai >= 1) {
      j3pElement('boutonrecopier').style.display = 'block'
    }
  }

  /**
   * Fonction affichant la solution s’il existe un affichage LaTeX de tag solution
   * @param{boolean} bexact true si l’élève a entré la bonne réponse, false sinon
   */
  function afficheSolution (bexact) {
    $('#divSolution').css('display', 'block').html('')
    const ch = extraitDeLatexPourMathQuill('solution')
    if (ch !== '') {
      afficheMathliveDans('divSolution', 'solution', ch, { style: { color: bexact ? 'green' : 'blue' } })
    }
  }

  function cacheSolution () {
    $('#divSolution').css('display', 'none').html('')
  }

  function traiteMathlive (ch) {
    // Mathlive ne met pas d’accolades pour les fractions dont le numérateur et le dénominateur sont des entiers
    ch = ch.replace(/\\frac(\d)(\d)/g, '\\frac{$1' + '}{' + '$2}')
    // Ni pour les racines carrées d’un entier
    ch = ch.replace(/\\sqrt(\d)/g, '\\sqrt{$1}')
    // Pour les puissances dont l’exposant est un chiffre, Mathlive ne met pas d’accolades
    ch = ch.replace(/\^([^{])/g, '^{$1}')
    ch = unLatexify(ch)
    // mathlive peut mettre des {} vides
    ch = ch.replace(/\\{\\}/g, '')
    ch = ch.replace(/\^(\d)(\d)/g, '^$1*$2') // On peut avoir par exemple 2^35^4 pour 2^3*5^4
    return sq.mtgAppLecteur.addImplicitMult('mtg32svg', ch)// Traitement des multiplications implicites
  }

  function marqueEditeurPourErreur (indice) {
    sq.marked[indice] = true
    $('#expression' + indice + 'inputmq1').css('background-color', '#FF9999')
    const mf = j3pElement('expression' + indice + 'inputmq1')
    mf.style.backgroundColor = '#FF9999'
    mf.demarqueErreur = () => {
      if (sq.marked) {
        demarqueEditeurPourErreur(indice)
      }
    }
  }

  function demarqueEditeurPourErreur (indice) {
    sq.marked[indice] = false
    j3pEmpty('correction')
    const mf = j3pElement('expression' + indice + 'inputmq1')
    mf.style.backgroundColor = 'white'
  }

  function afficheReponse (bilan) {
    let ch = ''; let coul
    if ((bilan === 'exact')) {
      coul = '#0000FF'
    } else {
      if (bilan === 'correct') {
        coul = '#0000FF'
      } else {
        if (bilan === 'faux') {
          coul = '#FF0000'
        } else { // Réponse exacte mais nombre d’essais dépassé
          ch = 'Calcul pas fini : <br>'
          coul = '#FF0000'
        }
      }
    }
    for (let i = 0; i < 2; i++) {
      ch += '$' + sq.rep[i] + '$'
      if (i === 0) ch += ',   '
    }
    const num = sq.numEssai
    if (num > 2) ch = '<br>' + ch
    const idrep = 'exp' + num
    afficheMathliveDans('formules', idrep, ch, {
      style: {
        color: coul
      }
    })
    resetKeyboardPosition()
  }

  function montreEditeurs (bVisible) {
    if (bVisible) $('#divInfo').css('display', 'block')
    for (let i = 1; i <= 2; i++) {
      $('#editeur' + i).css('display', bVisible ? 'block' : 'none')
      $('#boutonsmathquill' + i).css('display', 'block')
    }
    if (bVisible) focusIfExists(inputId)
  }

  function videEditeurs () {
    for (let i = 1; i <= 2; i++) {
      j3pElement('expression' + i + 'inputmq1').value = ''
    }
    // On donne le focus au premier éditeur MathQuill
    focusIfExists(inputId)
  }

  function recopierReponse () {
    let mf = j3pElement('expression2inputmq1')
    mf.value = sq.rep[1]
    demarqueEditeurPourErreur(2)
    mf = j3pElement('expression1inputmq1')
    mf.value = sq.rep[0]
    demarqueEditeurPourErreur(1)
    focusIfExists(inputId)
  }

  function initMtg () {
    setMathliveCss('MepMG')
    const mtgOptions = {
      // player only, sans figure initiale ni svg, on gère ça dans initMtg et modif_fig
      loadCoreWithMathJax: true
    }
    getMtgApp('mtg32svg', {}, mtgOptions).then((mtgAppLecteur) => {
      sq.mtgAppLecteur = mtgAppLecteur
      initDom()
      let par, car, i, j, k, nbrep, ar, tir, nb, nbcas, code
      // On crée provisoirement le DIV ci-dessous pour pouvoir calculer la figure.
      // On le détruira ensuite pour le récréer à la bonne position
      j3pDiv('conteneur', {
        id: 'formules',
        contenu: '',
        style: parcours.styles.petit.enonce
      }) // Contient le formules entrées par l’élève
      j3pDiv('conteneur', 'divSolution', '')
      $('#divSolution').css('display', 'none')
      j3pDiv('conteneur', 'divmtg32', '')
      j3pCreeSVG('divmtg32', {
        id: 'mtg32svg',
        width: 700,
        height: 500
      })
      sq.mtgAppLecteur.removeAllDoc()
      sq.mtgAppLecteur.addDoc('mtg32svg', sq.txt, true)
      sq.mtgAppLecteur.calculate('mtg32svg', true)
      const ch = 'abcdefghjklmnpqr'
      const nbvar = sq.mtgAppLecteur.valueOf('mtg32svg', 'nbvar')
      if (nbvar !== -1) {
        nbrep = parcours.donneesSection.nbrepetitions
        sq.aleat = true
        sq.nbParamAleat = nbvar
        for (i = 1; i <= nbvar; i++) {
          nbcas = sq.mtgAppLecteur.valueOf('mtg32svg', 'nbcas' + i)
          nb = Math.max(nbrep, nbcas)
          ar = []
          for (j = 0; j < nb; j++) ar.push(j % nbcas)
          sq['tab' + i] = []
          for (k = 0; k < nbrep; k++) {
            tir = Math.floor(Math.random() * ar.length)
            sq['tab' + i].push(ar[tir])
            ar.splice(tir, 1)
          }
          sq.mtgAppLecteur.giveFormula2('mtg32svg', 'r' + i, sq['tab' + i][0])
        }
      } else { sq.aleat = false }

      if (sq.param) {
        for (i = 0; i < ch.length; i++) {
          car = ch.charAt(i)
          if (sq.param.indexOf(car) !== -1) {
            par = parcours.donneesSection[car]
            if (par !== 'random') sq.mtgAppLecteur.giveFormula2('mtg32svg', car, par)
          }
        }
      }
      sq.mtgAppLecteur.calculate('mtg32svg', true)

      // Le div qui contiendra l’éditeur MathQuill.
      // Cet éditeur sera masqué au début.
      j3pDiv('conteneur', 'conteneurbouton', '')
      j3pAjouteBouton('conteneurbouton', 'boutonrecopier', 'MepBoutonsRepere', 'Recopier réponse précédente',
        recopierReponse)
      j3pElement('boutonrecopier').style.display = 'none'
      for (let ind = 1; ind <= 2; ind++) {
        j3pDiv('conteneur', 'editeur' + ind, '')
        afficheMathliveDans('editeur' + ind, 'expression' + ind, 'Calcul de ' + sq['nomInconnue' + ind] + ' : ' + '&1&', {
          charset: sq['charset' + ind],
          listeBoutons: sq.listeBoutons
        })
        if (sq['charset' + ind] !== '') {
          mathliveRestrict('expression' + ind + 'inputmq1', sq['charset' + ind])
        }
        j3pElement('expression' + ind + 'inputmq1').indice = ind // Pour connaître dans quel éditeur on est lors de onkeyup
        j3pDiv('editeur' + ind, 'boutonsmathquill' + ind, '')
        $('#boutonsmathquill' + ind).css('padding-top', '10px') // Pour laisser un peu de place au-dessus des boutons

        if (sq.listeBoutons) j3pPaletteMathquill('boutonsmathquill' + ind, 'expression' + ind + 'inputmq1', { liste: sq.listeBoutons, nomdiv: 'palette' + ind })
        // Ligne suivante pour un bon alignement
        $('#palette' + ind).css('display', 'inline-block')
        // On empêche les boutons MathQuill de pouvoir avoir le focus clavier
        const pal = j3pElement('palette' + ind)
        for (let m = 0; m < pal.childNodes.length; m++) {
          pal.childNodes[m].setAttribute('tabindex', '-1')
        }
        j3pElement('expression' + ind + 'inputmq1').onkeyup = function (ev) {
          onkeyup.call(this, ev, parcours)
        }
        // j3pElement("expression" + ind + "inputmq1").onkeydown = sq.onkeydown;
      }
      // On récupère le div de la figure pour le reclasser après les autres div
      const div = j3pElement('conteneur').removeChild(j3pElement('divmtg32'))
      j3pElement('conteneur').appendChild(div)
      sq.mtgAppLecteur.display('mtg32svg')
      const nbLatex = parseInt(sq.nbLatex)
      // La consigne peut contenir jusqu'à 4 paramètres a, b,c et d récupérés comme affichages LaTeX de la figure.
      const t = ['a', 'b', 'c', 'd']
      const param = {}
      for (k = 0; k < nbLatex; k++) {
        code = sq.mtgAppLecteur.getLatexCode('mtg32svg', k)
        param[t[k]] = code
      }
      const st = sq.consigne1 + (sq.simplifier ? sq.consigne3 : sq.consigne2) + sq.consigne4
      afficheMathliveDans('divEnonce', 'texte', st, param)
      focusIfExists(inputId)
      parcours.finEnonce()
    }).catch(j3pShowError)
  }

  function initDom () {
    renderMathInDocument({ renderAccessibleContent: '' })
    if (sq.titre) parcours.afficheTitre(sq.titre)

    // Trois lignes suivantes déplacées ici depuis initMtg pour éviter que j3pAffiche("", "divInfo" ne soit appelé avant la création du div divInfo
    j3pDiv(parcours.zones.MG, { id: 'conteneur', contenu: '', style: parcours.styles.etendre('toutpetit.enonce', { padding: '6px' }) })
    j3pDiv('conteneur', 'divEnonce', '')
    j3pDiv('conteneur', 'divInfo', '')
    const style = parcours.styles.petit.correction
    style.marginTop = '1.5em'
    style.marginLeft = '0.5em'
    j3pAddElt(parcours.zonesElts.MD, 'div', '', { id: 'correction', style })
    afficheNombreEssaisRestants()
  }

  switch (this.etat) {
    case 'enonce':
      // code exécuté au lancement de la section
      if (this.debutDeLaSection) {
        // Construction de la page
        parcours.construitStructurePage('presentation1bis')

        // compteur. Si   numEssai > nbEssais, on corrige
        sq.numEssai = 1

        sq.nbEssais = parseInt(parcours.donneesSection.nbEssais)
        const simplifier = parcours.donneesSection.simplifier
        sq.simplifier = (simplifier === undefined) ? true : simplifier
        // faut gérer sous-dossier ou pas…
        const sep = this.donneesSection.ex.includes('/') ? '-' : '/'
        j3pImporteAnnexe('squelettes-mtg32' + sep + this.donneesSection.ex + '.js').then(function (datasSection) {
          // console.log("initsection");
          // Attention, le this devient window...
          sq.txt = datasSection.txt

          sq.nomInconnue1 = datasSection.nomInconnue1
          sq.nomInconnue2 = datasSection.nomInconnue2
          sq.nbLatex = Number(datasSection.nbLatex ?? 0) // Le nombre de paramètres LaTeX dans le texte
          // ?? retourne l’opérande de gauche si !== undefined && !== null, celle de droite sinon
          const listeBoutons = []
          // boutons avec propriétés à true par défaut
          const tabBoutons1 = ['puissance', 'fraction']
          ;['btnPuis', 'btnFrac'].forEach((p, i) => {
            const b = Boolean(datasSection[p] ?? true)
            if (b) listeBoutons.push(tabBoutons1[i])
          })
          // boutons avec false par défaut
          const tabBoutons2 = ['pi', 'racine', 'exp', 'ln', 'sin', 'cos', 'tan', 'abs']
          ;['btnPi', 'btnRac', 'btnExp', 'btnLn', 'btnSin', 'btnCos', 'btnTan', 'btnAbs'].forEach((p, i) => {
            const b = Boolean(datasSection[p] ?? false)
            if (b) listeBoutons.push(tabBoutons2[i])
          })
          sq.listeBoutons = listeBoutons
          sq.boutonsMathQuill = listeBoutons.length !== 0
          sq.consigne1 = datasSection.consigne1 ?? ''
          sq.consigne2 = datasSection.consigne2 ?? ''
          sq.consigne3 = datasSection.consigne3 ?? ''
          sq.consigne4 = datasSection.consigne4 ?? ''
          sq.titre = datasSection.titre ?? ''
          sq.param = datasSection.param ?? '' // Chaine contenant les paramètres autorisés
          sq.charset1 = datasSection.charset1 ?? ''// Le set de caractères utilisés dans l’éditeur
          sq.charset2 = datasSection.charset2 ?? '' // Le set de caractères utilisés dans l’éditeur
          sq.marked = []
          for (let k = 0; k < 2; k++) {
            sq.marked[k] = false
          }
          initMtg()
        }).catch(error => {
          console.error(error)
          j3pShowError('Impossible de charger les données de cette exercice')
        })
        // return;
      } else {
        j3pEmpty('correction')
        cacheSolution()
        sq.numEssai = 1
        sq.mtgAppLecteur.removeDoc('mtg32svg')
        sq.mtgAppLecteur.addDoc('mtg32svg', sq.txt, true)

        if (sq.aleat) {
          for (let i = 1; i <= sq.nbParamAleat; i++) {
            sq.mtgAppLecteur.giveFormula2('mtg32svg', 'r' + i, sq['tab' + i][this.questionCourante - 1])
          }
        }
        if (sq.param !== undefined) {
          const ch = 'abcdefghjklmnpqr'
          for (let i = 0; i < ch.length; i++) {
            const car = ch.charAt(i)
            if (sq.param.indexOf(car) !== -1) {
              const par = parcours.donneesSection[car]
              if (par !== 'random') sq.mtgAppLecteur.giveFormula2('mtg32svg', car, par)
            }
          }
        }

        sq.mtgAppLecteur.calculate('mtg32svg', true) // true pour que les calculs alèatoires soient réinitialisés
        sq.mtgAppLecteur.display('mtg32svg')
        j3pEmpty('divEnonce')
        j3pEmpty('formules')
        j3pEmpty('divInfo')
        // La consigne peut contenir jusqu'à 4 paramètres a, b,c et d récupérés comme affichages LaTeX de la figure.
        sq.nbSol = sq.mtgAppLecteur.valueOf('mtg32svg', 'nbSol') // Le nombre de solutions de l’équation.
        // La consigne peut contenir jusqu'à 4 paramètres a, b,c et d récupérés comme affichages LaTeX de la figure.
        const nbLatex = parseInt(sq.nbLatex)
        // La consigne peut contenir jusqu'à 4 paramètres a, b,c et d récupérés comme affichages LaTeX de la figure.
        const t = ['a', 'b', 'c', 'd']
        const param = {}
        for (let k = 0; k < nbLatex; k++) {
          const code = sq.mtgAppLecteur.getLatexCode('mtg32svg', k)
          param[t[k]] = code
        }

        const st = sq.consigne1 + (sq.simplifier ? sq.consigne3 : sq.consigne2) + sq.consigne4
        afficheMathliveDans('divEnonce', 'texte', st, param)
        // Pour revenir en haut de page
        try {
          window.location.hash = 'MepMG'
          j3pElement('MepMG').scrollIntoView()
        } catch (e) {}
        montreEditeurs(true)
        videEditeurs()
        afficheNombreEssaisRestants()
        this.finEnonce()
      }

      break // case "enonce":

    case 'correction': {
      let bilanReponse = ''
      let simplificationPossible = false
      // On regarde d’abord si un des éditeurs a un contenu vide ou incorrect
      if (validationEditeurs()) {
        validation()
        if (sq.simplifier) {
          if (sq.resolu) { bilanReponse = 'exact' } else {
            if (sq.exact) {
              bilanReponse = 'exactPasFini'
              simplificationPossible = true
            } else {
              if (sq.correct) bilanReponse = 'correct'
              else bilanReponse = 'faux'
            }
          }
        } else {
          if (sq.resolu || sq.exact) {
            bilanReponse = 'exact'
            if (!sq.resolu) simplificationPossible = true
          } else { bilanReponse = 'faux' }
        }
      } else {
        bilanReponse = 'incorrect'
      }
      // A cause de la limite de temps :
      if (this.isElapsed) { // limite de temps
        this._stopTimer()
        j3pElement('correction').style.color = this.styles.cfaux
        j3pElement('divInfo').style.display = 'none'
        j3pElement('boutonrecopier').style.display = 'none'
        montreEditeurs(false)
        sq.mtgAppLecteur.executeMacro('mtg32svg', 'solution')
        afficheSolution(false)
        j3pElement('correction').innerHTML = tempsDepasse
        j3pElement('correction').innerHTML += '<br>La solution se trouve ci-contre.'
        this.etat = 'navigation'
        this.sectionCourante()
      } else {
        if (bilanReponse === 'incorrect') {
          j3pElement('correction').style.color = this.styles.cfaux
          if (sq.signeEgalOublie) j3pElement('correction').innerHTML = 'Ne pas oublier d’entrer les réponses avec des égalités.'
          else j3pElement('correction').innerHTML = 'Réponse incorrecte'
        } else {
          // Une réponse a été saisie
          sq.numEssai++
          // Bonne réponse
          if (bilanReponse === 'exact') {
            this.score++
            this._stopTimer()
            sq.mtgAppLecteur.setActive('mtg32svg', false)
            afficheReponse('exact')
            j3pElement('boutonrecopier').style.display = 'none'
            j3pElement('correction').style.color = this.styles.cbien
            if (simplificationPossible) j3pElement('correction').innerHTML = 'C’est bien mais on pouvait  simplifier la réponse' + '<br>Voir la solution ci-contre'
            else j3pElement('correction').innerHTML = cBien + '<br>Voir la solution ci-contre'
            j3pElement('divInfo').style.display = 'none'
            sq.mtgAppLecteur.executeMacro('mtg32svg', 'solution')
            afficheSolution(true)
            montreEditeurs(false)
            this.etat = 'navigation'
            this.sectionCourante()
          } else {
            if (bilanReponse === 'exactPasFini') {
              j3pElement('correction').style.color =
                (sq.numEssai <= sq.nbEssais) ? '#0000FF' : this.styles.cfaux
              j3pElement('correction').innerHTML = 'Le calcul est bon mais pas écrit sous la forme demandée.'
            } else {
              if (bilanReponse === 'correct') {
                j3pElement('correction').style.color =
                  (sq.numEssai <= sq.nbEssais) ? '#0000FF' : this.styles.cfaux
                j3pElement('correction').innerHTML = 'Pas résolu.'
              } else {
                j3pElement('correction').style.color = this.styles.cfaux
                j3pElement('correction').innerHTML = cFaux
              }
            }
            j3pEmpty('divInfo')

            if (sq.numEssai <= sq.nbEssais) {
              afficheNombreEssaisRestants()
              videEditeurs()
              afficheReponse((bilanReponse === 'exactPasFini') ? 'exact' : ((bilanReponse === 'correct') ? 'correct' : 'faux'))
              // S’il y a plus de deux chances,on appelle à nouveau le bouton Valider

              j3pElement('correction').innerHTML += '<br>' + '<br>' + essaieEncore
              // sq.mtgAppLecteur.setActive("mtg32svg", true);
            } else {
              // Erreur au nème essai
              this._stopTimer()
              sq.mtgAppLecteur.setActive('mtg32svg', false)
              j3pElement('boutonrecopier').style.display = 'none'
              j3pElement('divInfo').style.display = 'none'
              sq.mtgAppLecteur.executeMacro('mtg32svg', 'solution')
              afficheSolution(false)
              montreEditeurs(false)
              afficheReponse('faux')
              j3pElement('correction').innerHTML += '<br><br>La solution se trouve ci-contre.'
              this.etat = 'navigation'
              this.sectionCourante()
            }
          }
        }
      }
      // Obligatoire
      this.finCorrection()
      break // case "correction":
    }

    case 'navigation':
      if (!this.sectionTerminee()) {
        this.etat = 'enonce'
      }
      this.finNavigation()
      break // case "navigation":
  }
}
