import { j3pAddElt, j3pDiv, j3pShowError } from 'src/legacy/core/functions'
/*
Section atome :
Dev : Alexis Lecomte
Date : janvier 2015
Affiche un atome (image de mep-outils)
Modifié en mai 2021 par Rémi
 */

export const params = {
  outils: [],
  /**
   * Ce qui est paramétrable dans l’éditeur de graphe
   * @type {LegacySectionParamsParametres}
   */
  parametres: [
    ['indication', '', 'string', 'Une consigne à ajouter'],
    ['atome', 0, 'entier', 'identifiant de l’atome'],
    ['numero', 1, 'entier', 'Première ou seconde partie de la ressource (mettre 2 pour un changement de page sur le manuel)'],
    ['letitre', '', 'string', 'Titre à afficher']
  ]
}
/**
 * section squeletteatome
 * @this {Parcours}
 */
export default function main () {
  if (this.etat === 'enonce') {
    if (this.donneesSection.atome === 0) {
      j3pShowError('Vous devez paramétrer ce nœud pour indiquer l’atome à afficher')
      // on remet celui là qui était celui indiqué par défaut avant le 08/04/2021
      this.donneesSection.atome = 65430
    }
    this.construitStructurePage({ structure: 'presentation3', ratioGauche: 0.6 })
    this.setPassive()

    if (this.donneesSection.letitre) this.afficheTitre(this.donneesSection.letitre)
    const divGeneral = j3pDiv(this.zones.MG, 'div_general', '', this.styles.etendre('toutpetit.enonce', { textAlign: 'center' }))
    const img = j3pAddElt(divGeneral, 'img')
    img.setAttribute('src', 'https://manuel.sesamath.net/numerique/diapo.php?atome=' + this.donneesSection.atome + '&ordre=' + this.donneesSection.numero + '&env=j3p')
    this.finEnonce()
    // faut afficher ça après finEnonce (car il nettoie HD avant de mettre l’état dedans)
    if (this.donneesSection.indication) {
      j3pAddElt(this.zonesElts.HD, 'p', this.donneesSection.indication)
    }
  }
  // section passive, pas de case correction ni navigation
}
