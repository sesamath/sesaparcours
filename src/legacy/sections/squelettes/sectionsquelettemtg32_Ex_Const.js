import { j3pAddElt, j3pDiv, j3pElement, j3pEmpty, j3pImporteAnnexe, j3pShowError } from 'src/legacy/core/functions'
import { j3pAffiche } from 'src/lib/mathquill/functions'
import { getMtgApp } from 'src/lib/outils/mathgraph'
import { j3pCreeSVG } from 'src/legacy/core/functionsSvg'
import textesGeneriques from 'src/lib/core/textes'

const { cBien, cFaux, essaieEncore, regardeCorrection, tempsDepasse } = textesGeneriques

/*
    Yves Biton
    Juillet 2017

    squelettemtg32_Ex_Const
    Sert à faire faire un exercice de construction à l’aide de MathGraph32
    La figure utilisée doit comprendre une macro d’apparition d’objets d’intitulé #Solution# dont les objets qu’elle
    fait apparaître sont les objets à construire par l’élève.
    Le commentaire de la macro peut préciser quels sont les objets de type numériques que l’élève a le droit
    d’utiliser. Par exemple {a} et {f} pour utiliser un calcul nommé a et une fonction nommée f.
    Les outils permis pour la construction sont contenus dans la figure elle-même via le menu Options-
    Figure en cours - Personnalisation Menu

    Pour obtenir par exemples 4 répétitions aléatoires, utiliser la commande :
    ?graphe=[1,"squelettemtg32_Ex_Cons",[{pe:">=0",nn:"fin",conclusion:"fin"},{ex:"RetrouverCentreCercle", nbrepetitions:4}]];

*/

export const params = {
  outils: ['mathquill'],
  /**
   * Ce qui est paramétrable dans l’éditeur de graphe
   * @type {LegacySectionParamsParametres}
   */
  parametres: [
    ['limite', 0, 'entier', 'Temps disponible par répétition, en secondes'],
    ['nbchances', 2, 'entier', 'Nombre d’essais autorisés à l’élève'],
    ['ex', 'Const_CerclesTangentsADroite', 'string', 'Nom du fichier annexe (xxx => squelettes-mtg32/xxx.js et xxx/yyy => squelettes-mtg32-xxx/yyy.js)'],
    ['correction', true, 'boolean', 'true si on veut que la solution soit donnée à la fin en cas d’échec'],
    ['modeDys', false, 'boolean', 'true si on veut que MathGraph32 fonctionne en mode "dys"'],
    ['infoParam', 'Information sur le rôle des paramètres', 'string', 'Information sur l’utilisation des paramètres suivants'],
    ['a', 'random', 'string', 'Valeur éventuelle à affecter à un calcul a'],
    ['b', 'random', 'string', 'Valeur éventuelle à affecter à un calcul b'],
    ['c', 'random', 'string', 'Valeur éventuelle à affecter à un calcul c'],
    ['d', 'random', 'string', 'Valeur éventuelle à affecter à un calcul d'],
    ['e', 'random', 'string', 'Valeur éventuelle à affecter à un calcul e'],
    ['f', 'random', 'string', 'Valeur éventuelle à affecter à un calcul f'],
    ['g', 'random', 'string', 'Valeur éventuelle à affecter à un calcul g'],
    ['h', 'random', 'string', 'Valeur éventuelle à affecter à un calcul h'],
    ['j', 'random', 'string', 'Valeur éventuelle à affecter à un calcul h'],
    ['k', 'random', 'string', 'Valeur éventuelle à affecter à un calcul h']
  ]
}

/**
 * section squelettemtg32_Ex_Const
 * @this {Parcours}
 */
export default function main () {
  const parcours = this
  const sq = this.storage

  function ajouteFiguresSolutions (bCacherSolution, color) {
    if (bCacherSolution) j3pElement('divmtg32').style.display = 'none'
    for (let i = 1; i <= sq.nbsol; i++) {
      j3pDiv('conteneur', 'explication' + i, '')
      j3pElement('explication' + i).style.color = color
      j3pAffiche('explication' + i, 'titreexp' + i, sq['titreSolution' + i] + '\n', { style: { color: 'brown' } })

      const width = sq['width' + i]
      const height = sq['height' + i]
      j3pDiv('conteneur', 'divmtg32sol' + i, '')
      j3pCreeSVG('divmtg32sol' + i, { id: 'mtg32svgsol' + i, width, height })
      sq.player.addDoc('mtg32svgsol' + i, sq['figsol' + i], true)
      const ch = 'abcdefghjk'
      for (let j = 0; j < ch.length; j++) {
        const car = ch.charAt(j)
        if (sq.param.indexOf(car) !== -1) {
          const par = sq.mtgApp.valueOf(car)
          sq.player.giveFormula2('mtg32svgsol' + i, car, par)
        }
      }
      sq.player.calculate('mtg32svgsol' + i, true)
      sq.player.display('mtg32svgsol' + i)
      // La figure mtg32 peut avoir jusque 6 affichages Latex qu’on peut récupérer commes paramètres dans l’énoncé
      const t = ['a', 'b', 'c', 'd', 'e', 'f']
      const param = {}
      for (let k = 0; k < sq['nbLatex' + i]; k++) {
        param[t[k]] = sq.player.getLatexCode('mtg32svgsol' + i, k)
      }
      // j3pMathsAjouteDans("enonce", {id: "texte", content: sq.enonce + "<br>", parametres: param});
      j3pAffiche('explication' + i, 'solution' + i, sq['explication' + i] + '\n', param)
    }
  }

  function initDom () {
    if (sq.titre) parcours.afficheTitre(sq.titre)

    // Construction des boutons
    j3pDiv(parcours.zones.MG, {
      id: 'conteneur',
      contenu: '',
      style: parcours.styles.etendre('toutpetit.enonce', { padding: '6px' })
    })
    j3pDiv('conteneur', 'enonce', '')
    j3pDiv('conteneur', 'infoNbEssais', '')
    j3pDiv('conteneur', 'divmtg32', '')
    const style = parcours.styles.petit.correction
    style.marginTop = '1.5em'
    style.marginLeft = '0.5em'
    j3pAddElt(parcours.zonesElts.MD, 'div', '', { id: 'correction', style })
    const svgOptions = { idSvg: 'mtg32svg', width: sq.width, height: sq.height }
    const mtgOptions = {
      fig: sq.fig,
      local: false,
      translatable: false,
      stylePointCroix: sq.stylePointCroix,
      dys: sq.modeDys,
      options: false,
      newFig: false,
      save: false,
      displayOnLoad: false,
      bplayer: true,
      isEditable: true
    }
    getMtgApp('divmtg32', svgOptions, mtgOptions).then(mtgApp => {
      sq.mtgApp = mtgApp
      // mtg32App.removeAllDoc();
      // Ci-dessous, employer local:true pour déboguer en local et false pour version en ligne
      sq.correction = parcours.donneesSection.correction
      sq.player = mtgApp.player
      const ch = 'abcdefghjk'
      for (let i = 0; i < ch.length; i++) {
        const car = ch.charAt(i)
        if (sq.param.indexOf(car) !== -1) {
          const par = parcours.donneesSection[car]
          if (par !== 'random') mtgApp.giveFormula2(car, par)
        }
      }

      mtgApp.calculate(true)
      mtgApp.gestionnaire.initialise()
      // La figure mtg32 peut avoir jusque 6 affichages Latex qu’on peut récupérer commes paramètres dans l’énoncé
      const t = ['a', 'b', 'c', 'd', 'e', 'f']
      const param = {}
      for (let i = 0; i < sq.nbLatex; i++) {
        param[t[i]] = mtgApp.getLatexCode(i)
      }
      j3pAffiche('enonce', 'texte', sq.enonce + '\n', param)
      j3pAffiche('infoNbEssais', 'infoessais', 'Il te reste ' + sq.nbchances + ' validation(s) possibles.', { style: { color: 'black' } })
      mtgApp.display()
      parcours.finEnonce()
    }).catch(j3pShowError)
  }

  switch (this.etat) {
    case 'enonce':
      // code exécuté au lancement de la section
      if (this.debutDeLaSection) {
        // Construction de la page
        parcours.construitStructurePage('presentation1bis')
        this.validOnEnter = false // ex donneesSection.touche_entree
        // compteur. Si   numEssai > nbchances, on corrige
        sq.numEssai = 1
        sq.nbchances = parseInt(parcours.donneesSection.nbchances)
        sq.modeDys = parcours.donneesSection.modeDys
        // faut gérer sous-dossier ou pas…
        const sep = this.donneesSection.ex.includes('/') ? '-' : '/'
        j3pImporteAnnexe('squelettes-mtg32' + sep + this.donneesSection.ex + '.js').then(function (datasSection) {
          // Attention, le this devient window...
          sq.fig = datasSection.fig
          sq.width = datasSection.width
          sq.height = datasSection.height
          sq.titre = datasSection.titre
          let nbl = datasSection.nbLatex
          sq.nbLatex = nbl || 0 // Le nombre d’affichages LaTeX à récupérer dans la figure pour l’énoncé
          // S’il existe param est une chaîne contenant les noms des calculs choisis aléatoirement par la figure d’énoncé
          const param = datasSection.param
          sq.param = param || ''
          sq.enonce = datasSection.enonce
          sq.nbsol = datasSection.nbsol
          for (let nbsol = 1; nbsol <= sq.nbsol; nbsol++) {
            const ts = datasSection['titreSolution' + nbsol]
            sq['titreSolution' + nbsol] = ts || ((sq.nbsol === 1) ? 'Solution :' : ('Solution n°' + nbsol + '/'))
            sq['explication' + nbsol] = datasSection['explication' + nbsol]
            nbl = datasSection['nbLatex' + nbsol]
            sq['nbLatex' + nbsol] = nbl ? datasSection['nbLatex' + nbsol] : 0
            sq['width' + nbsol] = datasSection['width' + nbsol]
            sq['height' + nbsol] = datasSection['height' + nbsol]
            sq['figsol' + nbsol] = datasSection['solution' + nbsol]
          }
          sq.titre = datasSection.titre
          const s = datasSection.stylePointCroix
          sq.stylePointCroix = s || true
          initDom()
        }).catch(error => {
          console.error(error)
          j3pShowError('Impossible de charger les données de cette exercice')
        })
      } else {
        j3pEmpty('correction')
        this.afficheBoutonValider()
        sq.numEssai = 1
        this.finEnonce()
      }

      break // case "enonce":

    case 'correction': {
      let ch, k
      if (!this.isElapsed) { // Vérications à faire seulement si le temps limite a pas été dépassé)
        if (!sq.mtgApp) return
        if (!sq.mtgApp.objectConstructed()) {
          j3pElement('correction').style.color = this.styles.cfaux
          j3pElement('correction').innerHTML = 'Aucun objet n’a été construit.'
          return
        }
        const missingTypes = sq.mtgApp.getMissingTypes()
        if (missingTypes.length !== 0) {
          ch = 'Il manque un ou des éléments de type : '
          j3pElement('correction').style.color = this.styles.cfaux
          for (k = 0; k < missingTypes.length; k++) {
            ch = ch + missingTypes[k]
            if (k !== missingTypes.length - 1) ch = ch + ', '; else ch = ch + '.'
          }
          j3pElement('correction').innerHTML = ch
          return
        }
        const missingNames = sq.mtgApp.getMissingNames()
        if (missingNames.length !== 0) {
          ch = 'Il faut nommer '
          j3pElement('correction').style.color = this.styles.cfaux
          for (k = 0; k < missingNames.length; k++) {
            ch = ch + missingNames[k]
            if (k !== missingNames.length - 1) ch = ch + ', '; else ch = ch + '.'
          }
          j3pElement('correction').innerHTML = ch
          return
        }
      }
      let bilanreponse = ''
      const res1 = sq.mtgApp.validateAnswer()
      if (res1) {
        bilanreponse = 'exact'
      } else {
        bilanreponse = 'erreur'
      }

      // Bonne réponse
      if (bilanreponse === 'exact') {
        this._stopTimer()
        // mtg32App.setActive("mtg32svg", false);
        sq.mtgApp.setActive('false')
        sq.mtgApp.activateForCor()
        this.score++
        j3pElement('correction').style.color = this.styles.cbien
        let phrase = cBien
        if (sq.correction) {
          if (sq.correction) phrase += '<br>Tu peux regarder la solution et des compléments sous la figure.'
          ajouteFiguresSolutions(false, this.styles.cbien)
        }
        j3pElement('correction').innerHTML = phrase
        this.etat = 'navigation'
        this.sectionCourante()
      } else {
        // Pas de bonne réponse
        // A cause de la limite de temps :
        if (this.isElapsed) { // limite de temps
          sq.mtgApp.setActive('false')
          sq.mtgApp.activateForCor()
          this._stopTimer()
          if (sq.correction) {
            ajouteFiguresSolutions(false, parcours.styles.toutpetit.correction.color)
          }
          j3pElement('correction').innerHTML = tempsDepasse
          j3pElement('correction').innerHTML += '<br>' + regardeCorrection
          this.etat = 'navigation'
          this.sectionCourante()
        } else {
          // Réponse fausse :
          j3pElement('correction').style.color = this.styles.cfaux
          j3pElement('correction').innerHTML = cFaux
          sq.numEssai++

          // S’il y a plus de deux chances,on appelle à nouveau le bouton Valider
          if (sq.numEssai <= sq.nbchances) {
            j3pElement('correction').innerHTML += '<br>' + essaieEncore
            j3pElement('infoessais').innerHTML = 'Il te reste ' + String(sq.numEssai - 1) + ' validation(s) possible(s).'
          } else {
            // Erreur au nème essai
            sq.mtgApp.setActive('false')
            sq.mtgApp.activateForCor()
            this._stopTimer()
            j3pElement('infoNbEssais').removeChild(j3pElement('infoessais'))
            if (sq.correction) {
              ajouteFiguresSolutions(false, parcours.styles.toutpetit.correction.color)
            }
            if (sq.correction) j3pElement('correction').innerHTML += '<br>Regarde la correction sous la figure.'
            this.etat = 'navigation'
            this.sectionCourante()
          }
        }
      }
      // Obligatoire
      this.finCorrection()
      break // case "correction":
    }

    case 'navigation':

      if (this.sectionTerminee()) {
        // On détermine la phrase d’état renvoyée par la section
        this.parcours.pe = this.score / this.donneesSection.nbitems
        this.afficheBoutonSectionSuivante()
        this.focus('sectioncontinuer')
      } else {
        this.etat = 'enonce'
        this.afficheBoutonSuite()
        this.focus('boutoncontinuer')
      }
      // Obligatoire
      this.finNavigation()

      break // case "navigation":
  }
}
