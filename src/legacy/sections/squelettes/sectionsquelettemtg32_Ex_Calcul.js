import { j3pDiv, j3pElement, j3pEmpty, j3pImporteAnnexe, j3pShowError } from 'src/legacy/core/functions'
import { j3pMathsAjouteDans } from 'src/lib/mathquill/functions'
import { j3pCreeSVG } from 'src/legacy/core/functionsSvg'
import { getMtgCore } from 'src/lib/outils/mathgraph'
import textesGeneriques from 'src/lib/core/textes'

const { cBien, cFaux, essaieEncore, tempsDepasse } = textesGeneriques

/*
    Yves Biton
    Octobre 2013

    http://localhost:8081/?graphe=[1,"squelettemtg32_Ex_Calcul",[{pe:">=0",nn:"fin",conclusion:"fin"},{f:"Eq10"}]];
    squelettemtg32_Ex_Calcul
*/

export const params = {
  outils: ['mathquill'],
  /**
   * Ce qui est paramétrable dans l’éditeur de graphe
   * @type {LegacySectionParamsParametres}
   */
  parametres: [
    ['nbrepetitions', 2, 'entier', 'aucun'],
    ['limite', 0, 'entier', 'Temps disponible par répétition, en secondes'],
    ['nbchances', 1, 'entier', 'Nombre d’erreurs possibles (pour tenter de les corriger)'],

    ['f', 'eq10', 'string', 'Nom du fichier annexe (xxx => squelettes-mtg32/xxx.js et xxx/yyy => squelettes-mtg32-xxx/yyy.js)'],
    ['titre', 'Titre de l’activité', 'string', 'Titre de l’activité']
  ]
}

export default function squelettemtg32ExCalcul () {
  const parcours = this
  const sq = this.storage
  function initMtg () {
    // chargement mtg
    getMtgCore({ withMathJax: true })
      .then(
        // success
        (mtgAppLecteur) => {
          sq.mtgAppLecteur = mtgAppLecteur
          let code
          sq.mtgAppLecteur.removeAllDoc()
          sq.mtgAppLecteur.addDoc('mtg32svg', parcours.stockage[5], true)
          sq.mtgAppLecteur.setEditorsSize('mtg32svg', '17px')
          sq.mtgAppLecteur.calculateAndDisplayAll(true)
          const nbParam = parseInt(parcours.stockage[6])
          const t = ['a', 'b', 'c', 'd']
          const param = {}
          for (let i = 0; i < nbParam; i++) {
            code = sq.mtgAppLecteur.getLatexCode('mtg32svg', i)
            param[t[i]] = code
          }
          // console.log('chaine=' + (parcours.stockage[7] + parcours.stockage[8]))
          // console.log('param=' + param.a + ' et ' + param.b + ' et ' + param.c + ' et ' + param.d)
          j3pMathsAjouteDans('enonce', { id: 'texte', content: parcours.stockage[7] + parcours.stockage[8], parametres: param })
          parcours.finEnonce()
        },
        // failure
        (error) => {
          j3pShowError(error, { message: 'mathgraph n’est pas correctement chargé', mustNotify: true })
        })
      // plantage dans le code de success
      .catch(j3pShowError)
  }

  function initDom () {
    parcours.afficheTitre(parcours.donneesSection.titre)

    j3pDiv(parcours.zones.MG, {
      id: 'conteneur',
      contenu: '',
      style: parcours.styles.etendre('toutpetit.enonce', { padding: '6px' })
    })
    j3pDiv('conteneur', 'enonce', '')
    j3pDiv('conteneur', 'divmtg32', '')
    j3pCreeSVG('divmtg32', { id: 'mtg32svg', width: 600, height: 800 })
    j3pDiv(parcours.zones.MD, { id: 'correction', contenu: '', coord: [20, 150], style: parcours.styles.petit.correction })
    initMtg()
  }

  function getDonnees () {
    return {
      // Nombre de répétitions de l’exercice
      nbrepetitions: 2,
      nbetapes: 1,
      f: 'eq10',
      // Si indication != "" alors un lien "Indication" s’affiche dans la zone Inférieure Gauche (this.zones.IG
      // une indication<br>de deux lignes...";

      // pour le cas de presentation1bis
      // En effet pas d’indication possible pour cette présentation
      // surchargeindication: false;,

      // Nombre de chances dont dispose l’él§ve pour répondre à la question
      nbchances: 1,

      titre: 'Titre de l’activité'
    }
  }

  switch (this.etat) {
    case 'enonce':
      // code exécuté au lancement de la section
      if (this.debutDeLaSection) {
        this.donneesSection = getDonnees()

        this.surcharge()
        this.validOnEnter = false // ex donneesSection.touche_entree
        // Construction de la page
        this.construitStructurePage('presentation1bis')
        this.stockage = [0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0]
        // faut gérer sous-dossier ou pas…
        const sep = this.donneesSection.f.includes('/') ? '-' : '/'
        j3pImporteAnnexe('squelettes-mtg32' + sep + this.donneesSection.f + '.js').then(function (datasSection) {
          parcours.stockage[5] = datasSection.txt
          parcours.stockage[6] = datasSection.numero // Le nombre de paramètres LaTeX dans le texte
          parcours.stockage[7] = datasSection.consigne1
          parcours.stockage[8] = datasSection.consigne2
          initDom()
        }).catch(error => {
          console.error(error)
          j3pShowError('Impossible de charger les données de cette exercice')
        })
      } else {
        j3pEmpty(this.zonesElts.MD)
        j3pDiv(this.zones.MD, { id: 'correction', contenu: '', coord: [20, 150], style: this.styles.petit.correction })
        sq.mtgAppLecteur.setEditorsEmpty('mtg32svg')
        // sq.mtgAppLecteur.executeMacro("mtg32svg","initialiser");
        sq.mtgAppLecteur.removeDoc('mtg32svg')
        sq.mtgAppLecteur.addDoc('mtg32svg', parcours.stockage[5], true)
        sq.mtgAppLecteur.setEditorsSize('mtg32svg', '17px')
        sq.mtgAppLecteur.calculate('mtg32svg', true) // true pour que les calculs alèatoires soient rꪮitialisés
        j3pElement('enonce').innerHTML = ''
        const nbParam = parseInt(parcours.stockage[6])
        const t = ['a', 'b', 'c', 'd']
        const param = {}
        for (let i = 0; i < nbParam; i++) {
          param[t[i]] = sq.mtgAppLecteur.getLatexCode('mtg32svg', i)
        }
        j3pMathsAjouteDans('enonce', { id: 'texte', content: parcours.stockage[7] + parcours.stockage[8], parametres: param })
        sq.mtgAppLecteur.display('mtg32svg') // Pour tout réafficher
        // sq.mtgAppLecteur.setActive("mtg32svg",true);
        this.finEnonce()
      }

      break // case "enonce":

    case 'correction':
      { // faut un bloc car on déclare des variables
        let bilanreponse = ''
        if (sq.mtgAppLecteur.fieldValidation('mtg32svg', 'rep')) {
          const res1 = sq.mtgAppLecteur.valueOf('mtg32svg', 'reponse')
          if (res1 === 1) {
            bilanreponse = 'exact'
          } else {
            bilanreponse = 'erreur'
          }
        } else {
          bilanreponse = 'incorrect'
          sq.mtgAppLecteur.activateField('mtg32svg', 'rep') // Ne donne pas le focus sur tous les navigateurs.
        }

        // A cause de la limite de temps :
        if (this.isElapsed) { // limite de temps
          this._stopTimer()
          j3pElement('correction').style.color = this.styles.cfaux
          sq.mtgAppLecteur.executeMacro('mtg32svg', 'solution')
          // j3pElement("correction").innerHTML+='<br>' + regardeCorrection;
          j3pElement('correction').innerHTML = tempsDepasse
          j3pElement('correction').innerHTML += '<br>La solution se trouve en bas de la fenêtre ci-contre.'

          this.cacheBoutonValider()
          this.etat = 'navigation'
          this.sectionCourante()
        } else {
          if (bilanreponse === 'incorrect') {
            j3pElement('correction').style.color = this.styles.cfaux
            j3pElement('correction').innerHTML = 'Réponse illisible'
            this.afficheBoutonValider()
          } else {
          // Une réponse a été saisie
          // Bonne réponse
            if (bilanreponse === 'exact') {
              sq.mtgAppLecteur.setActive('mtg32svg', false)
              this._stopTimer()
              this.score++
              j3pElement('correction').style.color = this.styles.cbien
              j3pElement('correction').innerHTML = cBien
              this.cacheBoutonValider()
              sq.mtgAppLecteur.executeMacro('mtg32svg', 'solution')
              this.etat = 'navigation'
              this.sectionCourante()
            } else {
            // Pas de bonne réponse
              j3pElement('correction').style.color = this.styles.cfaux

              // Réponse fausse :

              j3pElement('correction').innerHTML = cFaux

              // S’il y a plus de deux chances,on appelle à nouveau le bouton Valider
              if (this.essaiCourant < this.donneesSection.nbchances) {
                j3pElement('correction').innerHTML += '<br>' + essaieEncore
              } else {
              // Erreur au nème essai
                this._stopTimer()
                this.cacheBoutonValider()
                sq.mtgAppLecteur.executeMacro('mtg32svg', 'solution')
                sq.mtgAppLecteur.setActive('mtg32svg', false)
                // j3pElement("correction").innerHTML+='<br>' + regardeCorrection;
                j3pElement('correction').innerHTML = '<br>C’est faux.<br><br>La solution se trouve en bas de la fenêtre ci-contre.'
                this.etat = 'navigation'
                this.sectionCourante()
              }
            }
          }
        }
        // Obligatoire
        this.finCorrection()
      }
      break // case "correction":

    case 'navigation':

      if (this.sectionTerminee()) {
        // On détermine la phrase d’état renvoyée par la section
        this.parcours.pe = this.score
        this.afficheBoutonSectionSuivante()
        this.focus('sectioncontinuer')
      } else {
        this.etat = 'enonce'
        this.afficheBoutonSuite()
        this.focus('boutoncontinuer')
      }
      // Obligatoire
      this.finNavigation()

      break // case "navigation":
  }
}
