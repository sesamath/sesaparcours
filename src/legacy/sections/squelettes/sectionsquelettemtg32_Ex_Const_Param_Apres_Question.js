import $ from 'jquery'
import { j3pAddElt, j3pDiv, j3pElement, j3pEmpty, j3pImporteAnnexe, j3pShowError } from 'src/legacy/core/functions'
import ValidationZones from 'src/legacy/outils/calculatrice/ValidationZones'
import { j3pAffiche } from 'src/lib/mathquill/functions'
import { getMtgApp } from 'src/lib/outils/mathgraph'
import { j3pCreeSVG } from 'src/legacy/core/functionsSvg'
import textesGeneriques from 'src/lib/core/textes'

const { cBien, cFaux, essaieEncore, regardeCorrection, tempsDepasse } = textesGeneriques

/*
    Yves Biton
    Juillet 2017

    squelettemtg32_Ex_Const_Param_Apres_Question
    Sert à faire faire un exercice de construction à l’aide de MathGraph32
    La figure utilisée doit comprendre une macro d’apparition d’objets d’intitulé #Solution# dont les objets qu’elle
    fait apparaître sont les objets à construire par l’élève.
    Le commentaire de la macro peut préciser quels sont les objets de type numériques que l’élève a le droit
    d’utiliser. Par exemple {a} et {f} pour utiliser un calcul nommé a et une fonction nommée f.
    Les outils permis pour la construction sont contenus dans la figure elle-même via le menu Options-
    Figure en cours - Personnalisation Menu

    Pour obtenir par exemples 4 répétitions aléatoires, utiliser la commande :
    ?graphe=[1,"squelettemtg32_Ex_Cons",[{pe:">=0",nn:"fin",conclusion:"fin"},{ex:"RetrouverCentreCercle", nbrepetitions:4}]];
    Si ex contient un paramètre nbParamAleat , alors dans le cas ou nbrepetitions > 1, on donne pour i compris entre 1 et npParamAleat
    au calcul de la figure nommé "r"+i (par exemple r1 et r2 si nbParamAleat vaut 2) des valeurs comprises entre 0 et "nbcas"+i - 1
    toutes différentes (sauf si nbrepetitions > b=nbcas auquel cas au bout de nbcas répétitions il y aura nécessairement une valeur de r déjà prise.

*/

export const params = {
  outils: ['mathquill'],
  /**
   * Ce qui est paramétrable dans l’éditeur de graphe
   * @type {LegacySectionParamsParametres}
   */
  parametres: [
    ['nbrepetitions', 4, 'entier', 'aucun'],
    ['limite', 0, 'entier', 'Temps disponible par répétition, en secondes'],
    ['nbchances', 2, 'entier', 'Nombre d’essais autorisés à l’élève'],
    ['ex', 'Lycee_Eq_Cercle_Const_1', 'string', 'Nom du fichier annexe (xxx => squelettes-mtg32/xxx.js et xxx/yyy => squelettes-mtg32-xxx/yyy.js)'],
    ['correction', true, 'boolean', 'true si on veut que la solution soit donnée à la fin en cas d’échec'],
    ['modeDys', false, 'boolean', 'true si on veut que MathGraph32 fonctionne en mode "dys"'],
    ['infoParam', 'Information sur le rôle des paramètres', 'string', 'Information sur l’utilisation des paramètres suivants'],
    ['a', 'random', 'string', 'Valeur éventuelle à affecter à un calcul a'],
    ['b', 'random', 'string', 'Valeur éventuelle à affecter à un calcul b'],
    ['c', 'random', 'string', 'Valeur éventuelle à affecter à un calcul  c'],
    ['d', 'random', 'string', 'Valeur éventuelle à affecter à un calcul  d'],
    ['e', 'random', 'string', 'Valeur éventuelle à affecter à un calcul e'],
    ['f', 'random', 'string', 'Valeur éventuelle à affecter à un calcul f'],
    ['g', 'random', 'string', 'Valeur éventuelle à affecter à un calcul g'],
    ['h', 'random', 'string', 'Valeur éventuelle à affecter à un calcul h'],
    ['j', 'random', 'string', 'Valeur éventuelle à affecter à un calcul j'],
    ['k', 'random', 'string', 'Valeur éventuelle à affecter à un calcul k'],
    ['n', 'random', 'string', 'Valeur éventuelle à affecter à un calcul n']
  ]
}

/**
 * section squelettemtg32_Ex_Const_Param_Apres_Question
 * @this {Parcours}
 */
export default function main () {
  const parcours = this
  const sq = this.storage

  function cacheSolution () {
    $('#solution').css('display', 'none')
  }

  function afficheSolutions (color) {
    let i, j, ch, car, par, t, param, code, k
    $('#solution').css('display', 'block')
    const player = sq.player
    for (i = 1; i <= sq.nbsol; i++) {
      j3pEmpty('explication' + i)
      j3pElement('explication' + i).style.color = color
      j3pAffiche('explication' + i, 'titreexp' + i, 'Solution ' + ((sq.nbsol > 1) ? ('n°' + i) : '') + ':\n', { style: { color: 'brown' } })
      // j3pMathsAjouteDans("explication" + i,{id:"texte", content: parcours.Sectionsquelettemtg32_Ex_Const_Param["explication" + i]});
      player.removeDoc('mtg32svgsol' + i) // Ne fait rien lors du premier affichage de correction
      player.addDoc('mtg32svgsol' + i, sq['figsol' + i], true)
      // On remplace dans la figure de solution les paramètres par les valeurs correspondant dans la figure de texte
      ch = 'abcdefghijkmnopqr'
      for (j = 0; j < ch.length; j++) {
        car = ch.charAt(j)
        if (sq.param.indexOf(car) !== -1) {
          par = sq.mtgApp.valueOf(car)
          player.giveFormula2('mtg32svgsol' + i, car, par)
        }
      }
      player.calculate('mtg32svgsol' + i, false) // false suite à rapport LaboMep
      t = 'abcdefghijk'
      param = {}
      for (k = 0; k < sq.nbLatex + i; k++) {
        code = player.getLatexCode('mtg32svgsol' + i, k)
        param[t.charAt(k)] = code
      }
      j3pAffiche('explication' + i, 'solution' + i, sq['explication' + i] + '\n', param)
      player.display('mtg32svgsol' + i)
    }
  }

  function initDom () {
    if (sq.titre) parcours.afficheTitre(sq.titre)

    j3pDiv(parcours.zones.MG, {
      id: 'conteneur',
      contenu: '',
      style: parcours.styles.etendre('toutpetit.enonce', { padding: '6px' })
    })
    j3pDiv('conteneur', 'divConsigneNbSol', '') // Le div qui contiendra la liste déroulante pour l’existence ou non de solutions
    $('#divConsigneNbSol').css('padding-bottom', '10px') // Pour laisser un peu de place au-dessus de l’éditeur
    j3pDiv('conteneur', 'enonce', '')
    j3pDiv('conteneur', 'infoNbEssais', '')
    j3pDiv('conteneur', 'divmtg32', '')
    j3pDiv('conteneur', 'solution', '')
    for (let i = 1; i <= sq.nbsol; i++) {
      j3pDiv('solution', 'explication' + i, '')
      j3pDiv('solution', 'divmtg32sol' + i, '')
      j3pCreeSVG('divmtg32sol' + i, { id: 'mtg32svgsol' + i, width: sq['width' + i], height: sq['height' + i] })
    }
    const style = parcours.styles.petit.correction
    style.marginTop = '1.5em'
    style.marginLeft = '0.5em'
    j3pAddElt(parcours.zonesElts.MD, 'div', '', { id: 'correction', style })
    cacheSolution()
    const svgOptions = { idSvg: 'mtg32svg', width: sq.width, height: sq.height }
    const mtgOptions = {
      fig: sq.fig,
      dys: sq.modeDys,
      local: false,
      translatable: false,
      displayOnLoad: false,
      bplayer: true,
      save: false,
      stylePointCroix: sq.stylePointCroix,
      isEditable: true
    }
    getMtgApp('divmtg32', svgOptions, mtgOptions).then(mtgApp => {
      sq.mtgApp = mtgApp
      sq.player = mtgApp.player
      // Indispensable ici d’utiliser la queue de MathJax
      sq.existenceSolEntre = false // Sera mis à true quand le nombre de solutions aura été proposé par l’élève.
      let code, ch, par, car, i, j, k, nbrep, ar, tir, nb
      mtgApp.calculate(true)
      const nbvar = mtgApp.valueOf('nbvar')
      if (nbvar !== -1) {
        nbrep = parcours.donneesSection.nbrepetitions
        sq.aleat = true
        sq.nbParamAleat = nbvar
        for (i = 1; i <= nbvar; i++) {
          const nbcas = mtgApp.valueOf('nbcas' + i)
          nb = Math.max(nbrep, nbcas)
          ar = []
          for (j = 0; j < nb; j++) ar.push(j % nbcas)
          sq['tab' + i] = []
          for (k = 0; k < nbrep; k++) {
            tir = Math.floor(Math.random() * ar.length)
            sq['tab' + i].push(ar[tir])
            ar.splice(tir, 1)
          }
          mtgApp.giveFormula2('r' + i, sq['tab' + i][0])
        }
      } else {
        sq.aleat = false
      }

      if (sq.param !== undefined) {
        ch = 'abcdefghijkn'
        for (i = 0; i < ch.length; i++) {
          car = ch.charAt(i)
          if (sq.param.indexOf(car) !== -1) {
            par = parcours.donneesSection[car]
            if (par !== 'random') mtgApp.giveFormula2(car, par)
          }
        }
      }

      mtgApp.calculate(true)
      mtgApp.gestionnaire.initialise()
      sq.auMoinsUneSol = mtgApp.valueOf('nbSol') === 1
      const tab = [sq.listeTitre, sq.listeItem1, sq.listeItem2]
      const nbLatex = parseInt(sq.nbLatex)

      // La figure mtg32 peut avoir jusque nbLatex affichages Latex qu’on peut récupérer commes paramètres dans l’énoncé
      const t = ['a', 'b', 'c', 'd', 'e', 'f', 'g', 'h', 'i', 'j', 'k']
      const obj = {}
      sq.paramLatex = {}
      for (i = 0; i < nbLatex; i++) {
        code = mtgApp.getLatexCode(i)
        obj[t[i]] = code
        sq.paramLatex[t[i]] = code
      }
      obj.styletexte = { couleur: '#000000' }
      obj.liste1 = { texte: tab }

      j3pAffiche('divConsigneNbSol', 'consigneNbSol', sq.consigneTitre.replace('#liste#', '#1#'), obj)
      j3pElement('consigneNbSolliste1').reponse = sq.auMoinsUneSol ? 2 : 1
      sq.fcts_valid = new ValidationZones({ parcours, zones: ['consigneNbSolliste1'] })

      // j3pMathsAjouteDans("enonce", {id: "texte", content: sq.enonce + "<br>", parametres: sq.paramLatex});
      // j3pAffiche("", "infoNbEssais", "infoessais", "Il reste " + parcours.donneesSection.nbchances + " validation(s) possibles.", {styletexte: {couleur: "black"}});
      mtgApp.display()
      // $("#enonce").css("visibility", "hidden");
      // $("#infoNbEssais").css("visibility", "hidden");
      sq.mtgApp.setActive(false)
      $('#mtg32svg').css('display', 'none')
      parcours.finEnonce()
    }).catch(j3pShowError)
  }

  switch (this.etat) {
    case 'enonce': {
      // code exécuté au lancement de la section
      if (this.debutDeLaSection) {
        // Construction de la page
        parcours.construitStructurePage('presentation1bis')

        // compteur. Si   numEssai > nbchances, on corrige
        sq.numEssai = 1
        sq.nbchances = parseInt(parcours.donneesSection.nbchances)
        sq.correction = parcours.donneesSection.correction
        sq.modeDys = parcours.donneesSection.modeDys
        // faut gérer sous-dossier ou pas…
        const sep = this.donneesSection.ex.includes('/') ? '-' : '/'
        j3pImporteAnnexe('squelettes-mtg32' + sep + this.donneesSection.ex + '.js').then(function (datasSection) {
          // Attention, le this devient window...
          sq.fig = datasSection.fig
          sq.width = datasSection.width
          sq.height = datasSection.height
          sq.titre = datasSection.titre
          sq.consigneTitre = datasSection.consigneTitre
          sq.listeTitre = datasSection.listeTitre
          sq.listeItem1 = datasSection.listeItem1
          sq.listeItem2 = datasSection.listeItem2
          let s = datasSection.stylePointCroix
          sq.stylePointCroix = (s !== undefined) ? s : true
          s = datasSection.exigerObjetTypeAttendu // Si true, on ne valide pas la réponse tant que les objets de type attendus ont été créés
          sq.exigerObjetTypeAttendu = (s !== undefined) ? s : true
          const param = datasSection.param
          sq.param = param || ''
          sq.nbLatex = datasSection.nbLatex
          sq.nbParamAleat = datasSection.nbParamAleat
          if (sq.nbParamAleat !== undefined) {
            for (let i = 1; i <= sq.nbParamAleat; i++) {
              sq['nbcas' + i] = datasSection['nbcas' + i]
            }
          }
          sq.enonce = datasSection.enonce
          sq.nbsol = datasSection.nbsol
          for (let nbsol = 1; nbsol <= datasSection.nbsol; nbsol++) {
            sq['explication' + nbsol] = datasSection['explication' + nbsol]
            sq['nbLatex' + nbsol] = datasSection['nbLatex' + nbsol]
            sq['width' + nbsol] = datasSection['width' + nbsol]
            sq['height' + nbsol] = datasSection['height' + nbsol]
            sq['figsol' + nbsol] = datasSection['solution' + nbsol]
          }
          sq.param = datasSection.param // Chaine contenant les paramètres autorisés
          initDom()
        }).catch(error => {
          console.error(error)
          j3pShowError('Impossible de charger les données de cette exercice')
        })
      } else {
        j3pEmpty('correction')
        sq.numEssai = 1
        cacheSolution()
        sq.existenceSolEntre = false // Sera mis à true quand le nombre de solutions aura été proposé par l’élève.
        j3pEmpty('divConsigneNbSol')
        j3pEmpty('enonce')
        j3pEmpty('infoNbEssais')
        // On recharge la même figure pour tout réinitialiser
        sq.mtgApp.setFigByCode(sq.fig, false) // false pour ne pas réafficher

        if (sq.aleat) {
          for (let i = 1; i <= sq.nbParamAleat; i++) {
            sq.mtgApp.giveFormula2('r' + i, sq['tab' + i][this.questionCourante - 1])
          }
        }
        if (sq.param !== undefined) {
          const ch = 'abcdefghjk'
          for (let i = 0; i < ch.length; i++) {
            const car = ch.charAt(i)
            if (sq.param.indexOf(car) !== -1) {
              const par = parcours.donneesSection[car]
              if (par !== 'random') sq.mtgApp.giveFormula2(car, par)
            }
          }
        }
        sq.mtgApp.calculate(true)
        // Important la ligne suivante est indispensable car il faut que lorsqu’on annule les actions faites
        // on retrouve la figure telle qu’elle a été modifiée par les lignes ci-dessus
        sq.mtgApp.gestionnaire.initialise()
        // Important la ligne suivante est indispensable car il faut que lorsqu’on annule les actions faites
        // on retrouve la figure telle qu’elle a été modifiée par les lignes ci-dessus
        sq.auMoinsUneSol = sq.mtgApp.valueOf('nbSol') === 1

        const tab = [sq.listeTitre, sq.listeItem1, sq.listeItem2]
        const nbLatex = parseInt(sq.nbLatex)

        // La figure mtg32 peut avoir jusque nbLatex affichages Latex qu’on peut récupérer commes paramètres dans l’énoncé
        const t = ['a', 'b', 'c', 'd', 'e', 'f', 'g', 'h', 'i', 'j', 'k']
        const obj = {}
        sq.paramLatex = {}
        for (let i = 0; i < nbLatex; i++) {
          const code = sq.mtgApp.getLatexCode(i)
          obj[t[i]] = code
          sq.paramLatex[t[i]] = code
        }
        obj.styletexte = { couleur: '#000000' }
        obj.liste1 = { texte: tab }

        j3pAffiche('divConsigneNbSol', 'consigneNbSol', sq.consigneTitre.replace('#liste#', '#1#'), obj)
        j3pElement('consigneNbSolliste1').reponse = sq.auMoinsUneSol ? 2 : 1
        sq.fcts_valid = new ValidationZones({
          parcours,
          zones: ['consigneNbSolliste1']
        })
        sq.mtgApp.display()
        sq.mtgApp.setActive(false)
        $('#mtg32svg').css('display', 'none')
        this.finEnonce()
      }
      // Pour revenir en haut de page
      try {
        window.location.hash = 'MepMG'
        j3pElement('MepMG').scrollIntoView()
      } catch (e) {
        console.error(e)
      }

      break // case "enonce":
    }

    case 'correction': {
      let bilanreponse, k
      if (!sq.existenceSolEntre && (!this.isElapsed)) {
        sq.fcts_valid.validationGlobale()
        const rep = j3pElement('consigneNbSolliste1').selectedIndex - 1
        if (rep === -1) {
          bilanreponse = 'existenceSolPasEntre'
        } else {
          if (rep === (sq.auMoinsUneSol ? 1 : 0)) {
            if (rep === 0) {
              bilanreponse = 'exactPasDeSol'
            } else {
              // Cas où il n’y a pas de solution
              bilanreponse = 'exactAvecSol'
              $('#mtg32svg').css('display', 'block')
            }
          } else {
            bilanreponse = 'nbSolFaux'
          }
        }
      } else {
        if (!this.isElapsed) {
          if (!sq.mtgApp.objectConstructed()) {
            j3pElement('correction').style.color = this.styles.cfaux
            j3pElement('correction').innerHTML = 'Aucun objet n’a été construit.'
            return
          }
          if (sq.exigerObjetTypeAttendu) {
            const missingTypes = sq.mtgApp.getMissingTypes()
            if (missingTypes.length !== 0) {
              let ch = 'Il manque un ou des éléments de type : '
              j3pElement('correction').style.color = this.styles.cfaux
              for (k = 0; k < missingTypes.length; k++) {
                ch = ch + missingTypes[k]
                if (k !== missingTypes.length - 1) ch = ch + ', '; else ch = ch + '.'
              }
              j3pElement('correction').innerHTML = ch
              return
            }
          }
          const missingNames = sq.mtgApp.getMissingNames()
          if (missingNames.length !== 0) {
            let ch = 'Il faut nommer '
            j3pElement('correction').style.color = this.styles.cfaux
            for (k = 0; k < missingNames.length; k++) {
              ch = ch + missingNames[k]
              if (k !== missingNames.length - 1) ch = ch + ', '; else ch = ch + '.'
            }
            j3pElement('correction').innerHTML = ch
            return
          }
        }
        const res1 = sq.mtgApp.validateAnswer()
        if (res1) {
          bilanreponse = 'exact'
        } else {
          bilanreponse = 'erreur'
        }
      }

      // A cause de la limite de temps :
      if (this.isElapsed) { // limite de temps
        this._stopTimer()
        if (sq.correction) afficheSolutions(parcours.styles.toutpetit.correction.color)
        j3pElement('correction').innerHTML = tempsDepasse
        j3pElement('correction').innerHTML += '<br>' + regardeCorrection
        this.etat = 'navigation'
        this.sectionCourante()
      } else {
        if (!sq.existenceSolEntre) {
          if (bilanreponse !== 'existenceSolPasEntre') {
            sq.existenceSolEntre = true
            if (bilanreponse === 'exactPasDeSol') {
              this._stopTimer()
              sq.mtgApp.setActive(false)
              this.score++
              afficheSolutions(this.styles.cbien)
              j3pElement('correction').style.color = this.styles.cbien
              j3pElement('correction').innerHTML = 'C’est juste. Il n’y a pas de solution.<br> Voir les explications ci-contre, en bas.'
              this.etat = 'navigation'
              this.sectionCourante()
            } else {
              if (bilanreponse === 'exactAvecSol') {
                sq.mtgApp.setActive(true)
                j3pElement('correction').style.color = this.styles.cbien
                j3pElement('correction').innerHTML = 'C’est juste. On continue.'
                // $("#enonce").css("visibility", "visible");
                // $("#infoNbEssais").css("visibility", "visible");
                j3pAffiche('enonce', 'texte', sq.enonce + '\n', sq.paramLatex)
                j3pAffiche('infoNbEssais', 'infoessais', 'Il reste ' + sq.nbchances + ' validation(s) possibles.', { style: { color: 'black' } })
              } else {
                this._stopTimer()
                j3pElement('correction').style.color = this.styles.cfaux
                j3pElement('correction').innerHTML = sq.auMoinsUneSol
                  ? 'C’est faux, il y a des points solutions<br> Voir les explications ci-contre, en bas.'
                  : 'C’est faux, il n’y a pas de solution<br> Voir les explications ci-contre, en bas.'
                afficheSolutions(this.styles.cbien)
                this.etat = 'navigation'
                this.sectionCourante()
              }
            }
          }
        } else {
          // Bonne réponse
          if (bilanreponse === 'exact') {
            this._stopTimer()
            sq.mtgApp.setActive(false)
            this.score++
            j3pElement('correction').style.color = this.styles.cbien
            j3pElement('correction').innerHTML = cBien + '<br>Voir la solution au-dessous de la figure.'
            afficheSolutions(this.styles.cbien)
            // mtg32App.executeMacro("mtg32svg","solution");
            this.etat = 'navigation'
            this.sectionCourante()
          } else {
            // Pas de bonne réponse
            // Réponse fausse :
            j3pElement('correction').style.color = this.styles.cfaux
            j3pElement('correction').innerHTML = cFaux
            sq.numEssai++

            // S’il y a plus de deux chances,on appelle à nouveau le bouton Valider
            if (sq.numEssai <= sq.nbchances) {
              sq.mtgApp.setActive(true)
              j3pElement('correction').innerHTML += '<br>' + essaieEncore
              j3pElement('infoessais').innerHTML = 'Il reste ' + String(sq.numEssai - 1) + ' validation(s) possible(s).'
            } else {
              // Erreur au nème essai
              this._stopTimer()
              j3pElement('infoNbEssais').removeChild(j3pElement('infoessais'))
              if (sq.correction) {
                afficheSolutions(parcours.styles.toutpetit.correction.color)
              }
              if (sq.correction) j3pElement('correction').innerHTML += '<br>Regarder la correction sous la figure.'
              this.etat = 'navigation'
              this.sectionCourante()
            }
          }
        }
      }
      // Obligatoire
      this.finCorrection()
      break // case "correction":
    }

    case 'navigation':

      if (this.sectionTerminee()) {
        // On détermine la phrase d’état renvoyée par la section
        this.parcours.pe = this.score / this.donneesSection.nbitems
        this.afficheBoutonSectionSuivante()
        this.focus('sectioncontinuer')
      } else {
        this.etat = 'enonce'
        this.afficheBoutonSuite()
        this.focus('boutoncontinuer')
      }
      // Obligatoire
      this.finNavigation()

      break // case "navigation":
  }
}
