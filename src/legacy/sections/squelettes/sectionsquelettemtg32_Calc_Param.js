import { j3pAddElt, j3pAjouteBouton, j3pDiv, j3pElement, j3pEmpty, j3pImporteAnnexe, j3pShowError } from 'src/legacy/core/functions'
import { j3pAffiche } from 'src/lib/mathquill/functions'
import { getMtgApp } from 'src/lib/outils/mathgraph'
import { j3pCreeSVG } from 'src/legacy/core/functionsSvg'
import textesGeneriques from 'src/lib/core/textes'

const { cBien, cFaux, essaieEncore, tempsDepasse } = textesGeneriques

/*
    Yves Biton
    Juillet 2014

    http://localhost:8081/?graphe=[1,"squelettemtg32_Calc_Param",[{pe:">=0",nn:"fin",conclusion:"fin"},{f:"Fact_ax_Carre_Moins_cCarre_Ex1"}]];
    squelettemtg32_Calc_Param
    Squelette demandant de calculer une expression et de l’écrire sous la forme la plus simple possible
    On peut éventuellement spécifier des paramètres  a,b,c,d,e,f,g,h,j,k,n,n,p.

    Si la figure contient un paramètre nbvar , alors dans le cas ou nbrepetitions > 1, on donne pour i compris entre 1 et nbvar
    au calcul de la figure nommé "r"+i (par exemple r1 et r2 si nbParamAleat vaut 2) des valeurs comprises entre 0 et "nbcas"+i - 1
    toutes différentes (sauf si nbrepetitions > b=nbcas auquel cas au bout de nbcas répétitions il y aura nécessairement une valeur de r déjà prise.

*/

export const params = {
  outils: ['mathquill'],
  /**
   * Ce qui est paramétrable dans l’éditeur de graphe
   * @type {LegacySectionParamsParametres}
   */
  parametres: [
    ['nbrepetitions', 1, 'entier', 'aucun'],
    ['limite', 0, 'entier', 'Temps disponible par répétition, en secondes'],
    ['nbchances', 1, 'entier', 'Nombre d’erreurs possibles (pour tenter de les corriger)'],
    ['nbEssais', 6, 'entier', 'Nombre d’essais maximum autorisé (8 par défaut)'],
    ['indicationfaute', true, 'boolean', 'true si on veut que certaines fautes soient expliquées après appui sur Entrée'],
    ['a', 'random', 'string', 'Valeur de a'],
    ['b', 'random', 'string', 'Valeur de b'],
    ['c', 'random', 'string', 'Valeur de c'],
    ['d', 'random', 'string', 'Valeur de d'],
    ['e', 'random', 'string', 'Valeur de e'],
    ['f', 'random', 'string', 'Valeur de f'],
    ['g', 'random', 'string', 'Valeur de g'],
    ['h', 'random', 'string', 'Valeur de h'],
    ['j', 'random', 'string', 'Valeur de j'],
    ['k', 'random', 'string', 'Valeur de k'],
    ['l', 'random', 'string', 'Valeur de l'],
    ['m', 'random', 'string', 'Valeur de m'],
    ['n', 'random', 'string', 'Valeur de n'],
    ['p', 'random', 'string', 'Valeur de p'],
    ['q', 'random', 'string', 'Valeur de q'],
    ['r', 'random', 'string', 'Valeur de r'],
    ['ex', 'Complexes_(a+ib)(c+id)', 'string', 'Nom du fichier annexe (xxx => squelettes-mtg32/xxx.js et xxx/yyy => squelettes-mtg32-xxx/yyy.js)']
  ]
}

/**
 * section squelettemtg32_Calc_Param
 * @this {Parcours}
 */
export default function main () {
  const parcours = this
  const sq = this.storage
  let i, ch, nbe, nbc
  sq.validation = function (parcours, continuer) {
    if (arguments.length <= 1) continuer = true
    if (sq.nbexp === 0) {
      j3pElement('boutonrecopier').style.display = 'block'
    }
    sq.rep = sq.mtgAppLecteur.getFieldValue('mtg32svg', 'rep')
    j3pElement('correction').innerHTML = ''
    sq.mtgAppLecteur.calculate('mtg32svg', false)
    if (sq.indicationfaute) {
      const faute = sq.mtgAppLecteur.valueOf('mtg32svg', 'faute')
      if (faute === 1) {
        sq.mtgAppLecteur.executeMacro('mtg32svg', 'voirFaute')
      } else {
        sq.mtgAppLecteur.executeMacro('mtg32svg', 'masquerFaute')
      }
    }
    sq.reponse = sq.mtgAppLecteur.valueOf('mtg32svg', 'reponse')
    const idrep = 'exp' + sq.nbexp
    sq.nbexp++
    if (sq.reponse >= 1) {
      j3pAffiche('formules', idrep, '\n' + '$' + sq.entete + ' ' + sq.symbexact +
        ' ' + sq.mtgAppLecteur.getLatexFormula('mtg32svg', 'rep') + '$', {
        styletexte: {
          couleur: '#0000FF'
        }
      })
    } else {
      j3pAffiche('formules', idrep, '\n' + '$' + sq.entete + ' ' + sq.symbnonexact +
        ' ' + sq.mtgAppLecteur.getLatexFormula('mtg32svg', 'rep') + '$', {
        styletexte: {
          couleur: '#FF0000'
        }
      })
    }
    sq.mtgAppLecteur.setEditorValue('mtg32svg', 'rep', '')
    // Si le nombre maximum d’essais est atteint on passe en mode correction
    if (sq.nbexp >= sq.nbEssais) {
      sq.mtgAppLecteur.setActive('mtg32svg', false)
      if (continuer) {
        parcours.etat = 'correction'
        parcours.sectionCourante()
      }
    } else {
      j3pElement('info').innerHTML = ''
      const nbe = parcours.donneesSection.nbEssais - sq.nbexp
      const nbc = parcours.donneesSection.nbchances - sq.numEssai + 1
      j3pAffiche('info', 'texteinfo', 'Il reste ' + nbe + ' essai(s) et ' + nbc + ' validation(s).', {
        styletexte: {
          couleur: '#7F007F'
        }
      })
    }
  }
  sq.recopierReponse = function () {
    sq.mtgAppLecteur.setEditorValue('mtg32svg', 'rep', sq.rep)
  }

  function initMtg () {
    const mtgOptions = {
      // player only, sans figure initiale ni svg, on gère ça dans initMtg et modif_fig
      loadCoreWithMathJax: true
    }
    getMtgApp('mtg32svg', {}, mtgOptions).then((mtgAppLecteur) => {
      sq.mtgAppLecteur = mtgAppLecteur
      let code, par, car, i, j, k, nbrep, ar, tir
      sq.mtgAppLecteur.removeAllDoc()
      sq.mtgAppLecteur.addDoc('mtg32svg', sq.txt, true)
      sq.nbexp = 0
      sq.reponse = -1
      sq.mtgAppLecteur.setEditorCallBackOK('mtg32svg', 'rep', function () {
        sq.validation(parcours)
      })
      sq.mtgAppLecteur.setEditorCharset('mtg32svg', 'rep', sq.charset)
      sq.mtgAppLecteur.setEditorsSize('mtg32svg', '17px')
      const ch = 'abcdefghjklmnpqr'
      if (sq.nbParamAleat !== undefined) {
        nbrep = parcours.donneesSection.nbrepetitions
        sq.aleat = true
        for (i = 1; i <= sq.nbParamAleat; i++) {
          const nb = Math.max(nbrep, sq['nbcas' + i])
          ar = []
          for (j = 0; j < nb; j++) ar.push(j % sq['nbcas' + i])
          sq['tab' + i] = []
          for (k = 0; k < nbrep; k++) {
            tir = Math.floor(Math.random() * ar.length)
            sq['tab' + i].push(ar[tir])
            ar.splice(tir, 1)
          }
          sq.mtgAppLecteur.giveFormula2('mtg32svg', 'r' + i, sq['tab' + i][0])
        }
      } else {
        sq.aleat = false
      }

      if (sq.param !== undefined) {
        for (i = 0; i < ch.length; i++) {
          car = ch.charAt(i)
          if (sq.param.indexOf(car) !== -1) {
            par = parcours.donneesSection[car]
            if (par !== 'random') sq.mtgAppLecteur.giveFormula2('mtg32svg', car, par)
          }
        }
      }
      sq.mtgAppLecteur.calculateAndDisplayAll(true)
      const nbParam = parseInt(sq.numero)
      const t = ['a', 'b', 'c', 'd']
      const param = {}
      for (i = 0; i < nbParam; i++) {
        code = sq.mtgAppLecteur.getLatexCode('mtg32svg', i)
        param[t[i]] = code
      }
      param.e = sq.nbEssais
      const st = sq.consigne1 + sq.consigne2 + sq.consigne3 + sq.consigne4
      j3pAffiche('enonce', 'texte', st, param)
      sq.mtgAppLecteur.activateField('mtg32svg', 'rep')
      parcours.finEnonce()
    }).catch(j3pShowError)
  }

  function initDom () {
    if (sq.titre) parcours.afficheTitre(sq.titre)

    j3pDiv(parcours.zones.MG, {
      id: 'conteneur',
      contenu: '',
      style: parcours.styles.etendre('toutpetit.enonce', { padding: '6px' })
    })
    j3pDiv('conteneur', 'enonce', '')
    j3pDiv('conteneur', {
      id: 'formules',
      contenu: '',
      style: parcours.styles.petit.enonce
    }) // Contient le formules entrées par l’élève
    j3pDiv('conteneur', 'info', '')
    const nbe = parcours.donneesSection.nbEssais
    const nbc = parcours.donneesSection.nbchances
    j3pAffiche('info', 'texteinfo', 'Il reste ' + nbe + ' essai(s) et ' + nbc + ' validation(s).', {
      styletexte: {
        couleur: '#7F007F'
      }
    })
    j3pDiv('conteneur', 'conteneurbouton', '')
    j3pAjouteBouton('conteneurbouton', 'boutonrecopier', 'MepBoutonsRepere', 'Recopier réponse précédente',
      'parcours.Sectionsquelettemtg32_Calc_Param.recopierReponse()')
    j3pElement('boutonrecopier').style.display = 'none'
    /*
    j3pDiv("conteneur","divmtg32","",parcours.styles.etendre("toutpetit.enonce",
    {
      width:"700px",
      height:"520px"
    }));
      */
    j3pDiv('conteneur', 'divmtg32', '')
    j3pCreeSVG('divmtg32', {
      id: 'mtg32svg',
      width: 780,
      height: 520
    })
    const style = parcours.styles.petit.correction
    style.marginTop = '1.5em'
    style.marginLeft = '0.5em'
    j3pAddElt(parcours.zonesElts.MD, 'div', '', { id: 'correction', style })
    initMtg()
  }

  function _Donnees () {
    // "primaire" || "lycee" || "college"
    // Actuellement les dimensions sont identiques
    this.typesection = 'lycee'//
    // Nombre de répétitions de l’exercice
    this.nbrepetitions = 1
    this.nbetapes = 1
    this.nbitems = this.nbetapes * this.nbrepetitions

    this.ex = 'Fact_cxPlusd_axPlusb_Plus_k_exPlusf_axPlusb'
    // Si indication != "" alors un lien "Indication" s’affiche dans la zone Inférieure Gauche (this.zones.IG
    // une indication<br>de deux lignes...";
    this.indication = ''
    const st = 'abcdefghjklmnpqr'
    for (let i = 0; i < st.length; i++) this[st.charAt(i)] = 'random'
    this.nbEssais = 6
    this.indicationfaute = true

    // pour le cas de presentation1bis
    // En effet pas d’indication possible pour cette présentation
    // this.surchargeindication = false;

    // Nombre de chances dont dispose l’él§ve pour répondre à la question
    this.nbchances = 1
    this.structure = 'presentation1bis'//  || "presentation2" || "presentation3"  || "presentation1bis"

    this.textes = {
      phrase1: 'Ecris '
    }
    this.titre = 'Titre de l’activité'

    this.pe = 0
  }

  switch (this.etat) {
    case 'enonce':
      // code exécuté au lancement de la section
      if (this.debutDeLaSection) {
        this.donneesSection = new _Donnees()
        // Construction de la page
        parcours.construitStructurePage('presentation1bis')

        this.surcharge()
        this.validOnEnter = false // ex donneesSection.touche_entree
        // compteur. Si   numEssai > nbchances, on corrige
        sq.numEssai = 1
        sq.nbEssais = parseInt(parcours.donneesSection.nbEssais)
        sq.nbchances = parseInt(parcours.donneesSection.nbchances)
        sq.indicationfaute = parcours.donneesSection.indicationfaute // true si indication quand certaines fautes sont faites et appui sur Entrée
        // faut gérer sous-dossier ou pas…
        const sep = this.donneesSection.ex.includes('/') ? '-' : '/'
        j3pImporteAnnexe('squelettes-mtg32' + sep + this.donneesSection.ex + '.js').then(function (datasSection) {
          // console.log("initsection");
          // Attention, le this devient window...
          sq.txt = datasSection.txt
          sq.entete = datasSection.entete
          sq.symbexact = datasSection.symbexact
          sq.symbnonexact = datasSection.symbnonexact
          sq.nbParamAleat = datasSection.nbParamAleat
          if (sq.nbParamAleat !== undefined) {
            for (let i = 1; i <= sq.nbParamAleat; i++) { sq['nbcas' + i] = datasSection['nbcas' + i] }
          }
          sq.numero = datasSection.numero // Le nombre de paramètres LaTeX dans le texte
          let s = datasSection.consigne1
          sq.consigne1 = (s !== undefined) ? s : ''
          s = datasSection.consigne2
          sq.consigne2 = (s !== undefined) ? s : ''
          s = datasSection.consigne3
          sq.consigne3 = (s !== undefined) ? s : ''
          s = datasSection.consigne4
          sq.consigne4 = (s !== undefined) ? s : ''
          sq.titre = datasSection.titre
          sq.param = datasSection.param // Chaine contenant les paramètres autorisés
          s = datasSection.charset // Le set de caractères utilisés dans l’éditeur
          sq.charset = (s !== undefined) ? s : ''
          initDom()
        }).catch(error => {
          console.error(error)
          j3pShowError('Impossible de charger les données de cette exercice')
        })
        // return;
      } else {
        j3pEmpty('correction')
        this.afficheBoutonValider()
        sq.numEssai = 1
        this.cacheBoutonSuite()
        sq.mtgAppLecteur.setEditorsEmpty('mtg32svg')
        // sq.mtgAppLecteur.executeMacro("mtg32svg","initialiser");
        sq.mtgAppLecteur.removeDoc('mtg32svg')
        sq.mtgAppLecteur.addDoc('mtg32svg', sq.txt, true)
        const parc = this
        sq.mtgAppLecteur.setEditorCallBackOK('mtg32svg', 'rep', function () {
          sq.validation(parc)
        })
        sq.mtgAppLecteur.setEditorsSize('mtg32svg', '17px')
        if (sq.aleat) {
          for (i = 1; i <= sq.nbParamAleat; i++) {
            sq.mtgAppLecteur.giveFormula2('mtg32svg', 'r' + i, sq['tab' + i][this.questionCourante - 1])
          }
        }
        if (sq.param !== undefined) {
          ch = 'abcdefghjklmnpqr'
          for (i = 0; i < ch.length; i++) {
            const car = ch.charAt(i)
            if (sq.param.indexOf(car) !== -1) {
              const par = parcours.donneesSection[car]
              if (par !== 'random') sq.mtgAppLecteur.giveFormula2('mtg32svg', car, par)
            }
          }
        }

        sq.mtgAppLecteur.calculate('mtg32svg', true) // true pour que les calculs alèatoires soient rꪮitialisés
        j3pElement('enonce').innerHTML = ''
        j3pElement('formules').innerHTML = ''
        sq.nbexp = 0
        sq.reponse = -1
        const nbParam = parseInt(sq.numero)
        const t = ['a', 'b', 'c', 'd']
        const param = {}
        for (i = 0; i < nbParam; i++) {
          param[t[i]] = sq.mtgAppLecteur.getLatexCode('mtg32svg', i)
        }
        param.e = sq.nbEssais
        const st = sq.consigne1 + sq.consigne2 + sq.consigne3 + sq.consigne4
        j3pAffiche('enonce', 'texte', st, param)
        nbe = parcours.donneesSection.nbEssais - sq.nbexp
        nbc = parcours.donneesSection.nbchances - parcours.essaiCourant
        j3pElement('texteinfo').innerHTML = 'Il reste ' + nbe + ' essai(s) et ' + nbc + ' validation(s).'
        j3pElement('info').style.display = 'block'
        sq.mtgAppLecteur.display('mtg32svg') // Pour tout réafficher
        sq.mtgAppLecteur.activateField('mtg32svg', 'rep')
        // sq.mtgAppLecteur.setActive("mtg32svg",true);
        this.finEnonce()
      }

      break // case "enonce":

    case 'correction': {
      let bilanreponse = ''
      let simplifieri = false
      // On regarde si le champ d’édition est rempli même si l’utilisateur
      // n’a pas appuyé sur la touche Entrée.
      ch = sq.mtgAppLecteur.getFieldValue('mtg32svg', 'rep')
      if (ch.length !== 0) {
        if (sq.mtgAppLecteur.fieldValidation('mtg32svg', 'rep')) {
          sq.validation(this, false)
        } else {
          bilanreponse = 'incorrect'
        }
      }
      const res1 = sq.reponse
      if (bilanreponse !== 'incorrect') {
        if (res1 === 1) {
          bilanreponse = 'exact'
          simplifieri = true
        } else {
          if (res1 === 2) {
            bilanreponse = 'exactpasfini'
          } else {
            bilanreponse = 'erreur'
          }
        }
      }
      // A cause de la limite de temps :
      if (this.isElapsed) { // limite de temps
        this._stopTimer()
        j3pElement('correction').style.color = this.styles.cfaux
        j3pElement('boutonrecopier').style.display = 'none'
        j3pElement('info').style.display = 'none'
        if (sq.indicationfaute) {
          sq.mtgAppLecteur.giveFormula2('mtg32svg', 'faute', '0')
          sq.mtgAppLecteur.calculate('mtg32svg', false)
          sq.mtgAppLecteur.executeMacro('mtg32svg', 'masquerFaute')
        }
        sq.mtgAppLecteur.executeMacro('mtg32svg', 'solution')
        j3pElement('correction').innerHTML = tempsDepasse
        j3pElement('correction').innerHTML += '<br>La solution se trouve ci-contre.'

        this.cacheBoutonValider()
        /*
                 *   RECOPIER LA CORRECTION ICI !
                 */
        this.etat = 'navigation'
        this.sectionCourante()
      } else {
        if (bilanreponse === 'incorrect') {
          j3pElement('correction').style.color = this.styles.cfaux
          j3pElement('correction').innerHTML = 'Réponse illisible'
          this.afficheBoutonValider()
          sq.mtgAppLecteur.activateField('mtg32svg', 'rep')
        } else {
          // Bonne réponse
          if (bilanreponse === 'exact') {
            this._stopTimer()
            sq.mtgAppLecteur.setActive('mtg32svg', false)
            this.score++
            j3pElement('correction').style.color = this.styles.cbien
            if (simplifieri) {
              j3pElement('correction').innerHTML = cBien
            } else {
              j3pElement('correction').innerHTML = 'C’est bien mais on pouvait  simplifier la réponse'
            }
            this.cacheBoutonValider()
            j3pElement('boutonrecopier').style.display = 'none'
            j3pElement('info').style.display = 'none'
            if (sq.indicationfaute) {
              sq.mtgAppLecteur.executeMacro('mtg32svg', 'masquerFaute')
            }
            sq.mtgAppLecteur.executeMacro('mtg32svg', 'solution')
            this.etat = 'navigation'
            this.sectionCourante()
          } else {
            j3pElement('correction').style.color = this.styles.cfaux
            sq.numEssai++
            if (bilanreponse === 'exactpasfini') {
              j3pElement('correction').innerHTML = 'Le calcul est bon mais pas écrit sous la forme demandée.'
            } else {
              j3pElement('correction').innerHTML = cFaux
            }
            j3pElement('info').innerHTML = ''
            nbe = parcours.donneesSection.nbEssais - sq.nbexp
            nbc = parcours.donneesSection.nbchances - parcours.essaiCourant
            j3pAffiche('info', 'texteinfo', 'Il reste ' + nbe + ' essai(s) et ' + nbc + ' validation(s).', {
              styletexte: {
                couleur: '#7F007F'
              }
            })

            // S’il y a plus de deux chances,on appelle à nouveau le bouton Valider
            if ((sq.nbexp < sq.nbEssais) &&
              (sq.numEssai <= sq.nbchances)) {
              j3pElement('correction').innerHTML += '<br>' + '<br>' + essaieEncore
              sq.mtgAppLecteur.setActive('mtg32svg', true)
              sq.mtgAppLecteur.activateField('mtg32svg', 'rep')
            } else {
              // Erreur au nème essai
              this._stopTimer()
              sq.mtgAppLecteur.setActive('mtg32svg', false)
              this.cacheBoutonValider()
              j3pElement('boutonrecopier').style.display = 'none'
              j3pElement('info').style.display = 'none'
              if (sq.indicationfaute) {
                sq.mtgAppLecteur.executeMacro('mtg32svg', 'masquerFaute')
              }
              sq.mtgAppLecteur.executeMacro('mtg32svg', 'solution')

              // j3pElement("correction").innerHTML+='<br>' + regardeCorrection;
              j3pElement('correction').innerHTML += '<br><br>La solution se trouve ci-contre.'
              this.etat = 'navigation'
              this.sectionCourante()
            }
          }
        }
      }
      // Obligatoire
      this.finCorrection()
      break // case "correction":
    }

    case 'navigation':

      if (this.sectionTerminee()) {
        // On détermine la phrase d’état renvoyée par la section
        this.parcours.pe = this.score / this.donneesSection.nbitems
        this.afficheBoutonSectionSuivante()
        this.focus('sectioncontinuer')
      } else {
        this.etat = 'enonce'
        this.afficheBoutonSuite()
        this.focus('boutoncontinuer')
      }
      // Obligatoire
      this.finNavigation()

      break // case "navigation":
  }
}
