import $ from 'jquery'
import { MathfieldElement, renderMathInDocument } from 'mathlive'

import { j3pAddElt, j3pAjouteBouton, j3pDiv, j3pElement, j3pEmpty, j3pImporteAnnexe, j3pPaletteMathquill, j3pShowError } from 'src/legacy/core/functions'
import { j3pCreeSVG } from 'src/legacy/core/functionsSvg'
import textesGeneriques from 'src/lib/core/textes'
import { unLatexify } from 'src/lib/mathquill/functions'

import { getMtgApp } from 'src/lib/outils/mathgraph'
import { afficheMathliveDans, getMathliveValue } from 'src/lib/outils/mathlive/display'
import { setMathliveCss } from 'src/lib/outils/mathlive/mathlivecss'
import { cleanLatexForMl, mathliveRestrict } from 'src/lib/outils/mathlive/utils'
import { focusIfExists } from 'src/lib/utils/dom/main'
import { resetKeyboardPosition } from 'src/lib/widgets/mlVirtualKeyboard/VirtualKeyboard'

const { cBien, cFaux, essaieEncore, tempsDepasse } = textesGeneriques

/*
 Yves Biton
 Juillet 2014 - Revu en 2023 pour utiliser Mathlive
*/

// on définit le dossier des fonts mathlive (il faut le faire très tôt,
// ici c’est bon avec pnpm start mais trop tard pour un build (cf src/lib/player/index.js de la branche draftPlayer)
if (MathfieldElement) { // undefined dans les tests avec happy-dom
  MathfieldElement.fontsDirectory = '/static/mathlive/fonts'
  MathfieldElement.soundsDirectory = '/static/mathlive/sounds'
}

export const params = {
  /**
   * Ce qui est paramétrable dans l’éditeur de graphe
   * @type {LegacySectionParamsParametres}
   */
  parametres: [
    ['nbrepetitions', 1, 'entier', 'aucun'],
    ['limite', 0, 'entier', 'Temps disponible par répétition, en secondes'],
    ['nbchances', 2, 'entier', 'Nombre d’erreurs possibles (pour tenter de les corriger)'],
    ['nbEssais', 6, 'entier', 'Nombre d’essais maximum autorisés'],
    ['factMax', false, 'boolean', 'true si on exige une factorisation maximale, false sinon'],
    ['infoParam', 'Information sur le rôle des paramètres', 'string', 'Information sur l’utilisation des paramètres suivants'],
    ['a', 'random', 'string', 'Valeur de a'],
    ['b', 'random', 'string', 'Valeur de b'],
    ['c', 'random', 'string', 'Valeur de c'],
    ['d', 'random', 'string', 'Valeur de d'],
    ['e', 'random', 'string', 'Valeur de e'],
    ['f', 'random', 'string', 'Valeur de f'],
    ['g', 'random', 'string', 'Valeur de g'],
    ['h', 'random', 'string', 'Valeur de h'],
    ['j', 'random', 'string', 'Valeur de j'],
    ['k', 'random', 'string', 'Valeur de k'],
    ['l', 'random', 'string', 'Valeur de l'],
    ['m', 'random', 'string', 'Valeur de m'],
    ['n', 'random', 'string', 'Valeur de n'],
    ['p', 'random', 'string', 'Valeur de p'],
    ['q', 'random', 'string', 'Valeur de q'],
    ['r', 'random', 'string', 'Valeur de r'],
    ['ex', 'Fact_bxCarre_Plus_cx', 'string', 'Nom du fichier annexe (xxx => squelettes-mtg32/xxx.js et xxx/yyy => squelettes-mtg32-xxx/yyy.js)']
  ]
}

const inputId = 'expressioninputmq1'

/**
 * section squelettemtg32_Fact_Param
 * @this {Parcours}
 */
export default function main () {
  const parcours = this
  const sq = this.storage

  function remplaceNomsVariables (chcalc) {
    const indVar = sq.mtgAppLecteur.valueOf('mtg32svg', 'indiceNomVar')
    if (indVar !== -1 && indVar !== 0) {
      const ch = sq.nomVarRemp.charAt(indVar - 1)
      chcalc = chcalc.replace(new RegExp(ch, 'g'), sq.nomVar)
    }
    return chcalc
  }

  function onkeyup (ev) {
    if (sq.marked) demarqueEditeurPourErreur()
    if (ev.keyCode === 13) {
      const rep = getMathliveValue(inputId)
      if (!rep) return
      let chcalcul = traiteMathlive(rep)
      if (sq.nomVar !== '') chcalcul = remplaceNomsVariables(chcalcul)

      const valide = sq.mtgAppLecteur.syntaxValidation('mtg32svg', 'rep', chcalcul, true)
      if (valide) {
        validation()
      } else {
        marqueEditeurPourErreur()
        j3pElement('correction').style.color = parcours.styles.cfaux
        j3pElement('correction').innerHTML = 'Réponse incorrecte'
        focusIfExists(inputId)
      }
    }
  }

  /**
   * Fonction extrayant d’un affichage LaTeX de la figure les lignes de texte et les transformant
   * en une chaîne affichable par MathQuill
   * @param tagLatex : le tag de l’affichage LaTeX de la figure
   * @return {string}
   */
  function extraitDeLatexPourMathlive (tagLatex) {
    const list = sq.mtgAppLecteur.getList('mtg32svg')
    const latex = list.getByTag(tagLatex, true)
    if (latex === null || latex.className !== 'CLatex') return ''
    return cleanLatexForMl(latex.chaineLatex, sq.charset)
  }

  function traiteMathlive (ch) {
    // Mathlive ne met pas d’accolades pour les fractions dont le numérateur et le dénominateur sont des entiers
    ch = ch.replace(/\\frac(\d)(\d)/g, '\\frac{$1' + '}{' + '$2}')
    // Ni pour les racines carrées d’un entier
    ch = ch.replace(/\\sqrt(\d)/g, '\\sqrt{$1}')
    // Pour les puissances dont l’exposant est un chiffre, Mathlive ne met pas d’accolades
    ch = ch.replace(/\^([^{])/g, '^{$1}')
    ch = unLatexify(ch)
    // mathlive peut mettre des {} vides
    ch = ch.replace(/\\{\\}/g, '')
    ch = ch.replace(/\^(\d)(\d)/g, '^$1*$2') // On peut avoir par exemple 2^35^4 pour 2^3*5^4
    return sq.mtgAppLecteur.addImplicitMult('mtg32svg', ch)// Traitement des multiplications implicites
  }

  /**
   * Fonction affichant la solution s’il existe un affichage LaTeX de tag solution
   * @param{boolean} bexact true si l’élève a entré la bonne réponse, false sinon
   */
  function afficheSolution (bexact) {
    $('#divSolution').css('display', 'block').html('')
    const ch = extraitDeLatexPourMathlive(sq.factMax ? 'solutionMax' : 'solution')
    if (ch !== '') {
      afficheMathliveDans('divSolution', 'solution', ch, { style: { color: bexact ? 'green' : 'blue' } })
    }
  }

  function cacheSolution () {
    $('#divSolution').css('display', 'none').html('')
  }

  function afficheNombreEssaisRestants () {
    j3pEmpty('info')
    const nbe = sq.nbEssais - sq.nbexp
    const nbc = Math.min(sq.nbchances - sq.numEssai + 1, nbe)
    if ((nbe === 1) && (nbc === 1)) {
      afficheMathliveDans('info', 'texteinfo', 'Il reste une validation.', { style: { color: '#7F007F' } })
    } else {
      afficheMathliveDans('info', 'texteinfo', 'Il reste ' + nbe + ' essai(s) et ' + nbc + ' validation(s).', { style: { color: '#7F007F' } })
    }
  }

  function marqueEditeurPourErreur () {
    sq.marked = true
    const mf = j3pElement(inputId)
    if (!mf) return
    mf.style.backgroundColor = '#FF9999'
    mf.demarqueErreur = () => {
      if (sq.marked) {
        demarqueEditeurPourErreur()
      }
    }
  }

  function demarqueEditeurPourErreur () {
    sq.marked = false
    j3pEmpty('correction')
    const mf = j3pElement(inputId)
    if (!mf) return
    mf.style.backgroundColor = 'white'
  }

  function variableValidation (st) {
    if (sq.nomVar === '') return true
    else {
      const indVar = sq.mtgAppLecteur.valueOf('mtg32svg', 'indiceNomVar')
      if (indVar !== -1 && indVar !== 0) {
        const noms = sq.nomVar + sq.nomVarRemp
        // On regarde si l’utilisateur n’a pas utilisé un autre nom devariable que celui permis
        for (let j = 0; j < noms.length; j++) {
          if ((j !== indVar) && (st.indexOf(noms.charAt(j)) !== -1)) return false
        }
      }
    }
    return true
  }

  function validation (continuer) {
    if (arguments.length <= 1) continuer = true
    if (sq.nbexp === 0) {
      j3pElement('boutonrecopier').style.display = 'block'
    }
    sq.rep = getMathliveValue(inputId)
    // Ci dessous on ajoute un \displaystyle pour que la réponse soit affichée comme dans l’éditeur
    let chcalcul = traiteMathlive(sq.rep)
    sq.rep = '\\displaystyle ' + sq.rep

    if (sq.nomVar !== '') {
      chcalcul = remplaceNomsVariables(chcalcul)
    }
    sq.mtgAppLecteur.giveFormula2('mtg32svg', 'rep', chcalcul)
    j3pEmpty('correction')
    sq.mtgAppLecteur.calculate('mtg32svg', false)
    sq.reponse = sq.mtgAppLecteur.valueOf('mtg32svg', 'reponse')
    const idrep = 'exp' + sq.nbexp
    sq.nbexp++
    const chdeb = sq.nbexp === 1 ? '' : '<br>'
    if (sq.reponse >= 1) {
      afficheMathliveDans('formules', idrep, chdeb + '$' + sq.entete + ' ' + sq.symbexact + sq.rep + '$', { style: { color: '#0000FF' } })
    } else {
      afficheMathliveDans('formules', idrep, chdeb + '$' + sq.entete + ' ' + sq.symbnonexact + sq.rep + '$', { style: { color: '#FF0000' } })
    }
    // On vide le contenu de l’éditeur
    const mf = j3pElement(inputId)
    if (!mf) return
    mf.value = ''
    // Si le nombre maximum d’essais est atteint on désactive la figure
    if (sq.nbexp >= sq.nbEssais) {
      sq.mtgAppLecteur.setActive('mtg32svg', false)
      if (continuer) {
        parcours.etat = 'correction'
        parcours.sectionCourante()
        $('#editeur').css('display', 'none')
        $('#boutonsmathquill').css('display', 'none')
      }
    } else {
      afficheNombreEssaisRestants()
      resetKeyboardPosition()
    }
  } // validation

  function recopierReponse () {
    demarqueEditeurPourErreur()
    const mf = j3pElement(inputId)
    if (!mf) return
    mf.value = sq.rep
    focusIfExists(inputId)
  }

  function initMtg () {
    setMathliveCss('MepMG')
    const mtgOptions = {
      // player only, sans figure initiale ni svg, on gère ça dans initMtg et modif_fig
      loadCoreWithMathJax: true
    }
    getMtgApp('mtg32svg', {}, mtgOptions).then((mtgAppLecteur) => {
      sq.mtgAppLecteur = mtgAppLecteur
      initDom()
      let code, par, car, i, j, k, nbrep, ar, tir, nbcas, nb
      sq.mtgAppLecteur.removeAllDoc()
      sq.mtgAppLecteur.addDoc('mtg32svg', sq.txt, true)
      sq.mtgAppLecteur.calculate('mtg32svg', false)
      const ch = 'abcdefghjklmnpqr'
      const nbvar = sq.mtgAppLecteur.valueOf('mtg32svg', 'nbvar')
      if (nbvar !== -1) {
        nbrep = parcours.donneesSection.nbrepetitions
        sq.aleat = true
        sq.nbParamAleat = nbvar
        for (i = 1; i <= nbvar; i++) {
          nbcas = sq.mtgAppLecteur.valueOf('mtg32svg', 'nbcas' + i)
          nb = Math.max(nbrep, nbcas)
          ar = []
          for (j = 0; j < nb; j++) {
            ar.push(j % nbcas)
          }
          sq['tab' + i] = []
          for (k = 0; k < nbrep; k++) {
            tir = Math.floor(Math.random() * ar.length)
            sq['tab' + i].push(ar[tir])
            ar.splice(tir, 1)
          }
          sq.mtgAppLecteur.giveFormula2('mtg32svg', 'r' + i, sq['tab' + i][0])
        }
      } else sq.aleat = false
      if (sq.param !== undefined) {
        for (i = 0; i < ch.length; i++) {
          car = ch.charAt(i)
          if (sq.param.indexOf(car) !== -1) {
            par = parcours.donneesSection[car]
            if (par !== 'random') {
              sq.mtgAppLecteur.giveFormula2('mtg32svg', car, par)
            }
          }
        }
      }
      sq.mtgAppLecteur.calculateAndDisplayAll(true)
      const nbParam = sq.numero // Le nombre de LaTeX fournis pas la figure mtg32
      const t = ['a', 'b', 'c', 'd']
      const param = {}
      for (i = 0; i < nbParam; i++) {
        code = sq.mtgAppLecteur.getLatexCode('mtg32svg', i)
        param[t[i]] = code
      }
      param.e = String(sq.nbEssais)
      const st = sq.consigne1 + (sq.factMax ? sq.consigne3 : sq.consigne2) + sq.consigne4
      afficheMathliveDans('enonce', 'texte', st, param)
      // Les 3 lignes suivantes pour que sq.mtgAppLecteur soit prêt quand onkeyup est appelé
      j3pElement(inputId).onkeyup = onkeyup
      focusIfExists(inputId)
      renderMathInDocument({ renderAccessibleContent: '' }) // Indispensable
      parcours.finEnonce()
    }).catch(j3pShowError)
  } // initMtg

  function initDom () {
    if (sq.titre) parcours.afficheTitre(sq.titre)

    j3pDiv(parcours.zones.MG, {
      id: 'conteneur',
      contenu: '',
      style: parcours.styles.etendre('toutpetit.enonce', { padding: '6px' })
    })
    j3pDiv('conteneur', 'enonce', '')
    j3pDiv('conteneur', { id: 'formules', contenu: '', style: parcours.styles.petit.enonce }) // Contient le formules entrées par l’élève
    j3pDiv('conteneur', 'info', '')
    afficheNombreEssaisRestants()
    j3pDiv('conteneur', 'conteneurbouton', '')
    j3pAjouteBouton('conteneurbouton', 'boutonrecopier', 'MepBoutonsRepere', 'Recopier réponse précédente', recopierReponse)
    j3pElement('boutonrecopier').style.display = 'none'
    j3pDiv('conteneur', 'editeur', '') // Le div qui contiendra l’éditeur MathQuill
    $('#editeur').css('padding-top', '10px') // Pour laisser un peu de place au-dessus de l’éditeur
    // j3pAffiche('editeur', 'debut', '$' + sq.entete + '$ = ', {})
    const options = {
      charset: sq.charset,
      listeBoutons: sq.listeBoutons,
      style: { fontSize: '24px' }
    }
    afficheMathliveDans('editeur', 'expression', '$' + sq.entete + '$ = &1&', options)
    if (sq.charset !== '') {
      mathliveRestrict(inputId, sq.charset)
    }
    j3pDiv('conteneur', 'boutonsmathquill', '')
    $('#boutonsmathquill').css('display', 'inline-block').css('padding-top', '10px')// Pour laisser un peu de place au-dessus des boutons
    if (sq.boutonsMathQuill) j3pPaletteMathquill('boutonsmathquill', inputId, { liste: sq.listeBoutons })
    j3pAddElt('conteneur', 'div', '', { id: 'divSolution' })
    $('#divSolution').css('display', 'none')
    j3pDiv('conteneur', 'divmtg32', '')
    j3pCreeSVG('divmtg32', { id: 'mtg32svg', width: 640, height: 480 })
    const style = parcours.styles.petit.correction
    style.marginTop = '1.5em'
    style.marginLeft = '0.5em'
    j3pAddElt(parcours.zonesElts.MD, 'div', '', { id: 'correction', style })
  }

  switch (this.etat) {
    case 'enonce':
      // code exécuté au lancement de la section
      if (this.debutDeLaSection) {
        this.validOnEnter = false // ex donneesSection.touche_entree
        // compteur. Si   numEssai > nbchances, on corrige
        sq.numEssai = 1
        sq.nbexp = 0
        sq.reponse = -1
        sq.factMax = parcours.donneesSection.factMax
        sq.nbEssais = parseInt(parcours.donneesSection.nbEssais)
        sq.nbchances = parseInt(parcours.donneesSection.nbchances)
        // Construction de la page
        this.construitStructurePage('presentation1bis', 0.75)
        if (sq.nbchances > sq.nbEssais) sq.nbchances = sq.nbEssais
        const sep = this.donneesSection.ex.includes('/') ? '-' : '/'
        j3pImporteAnnexe('squelettes-mtg32' + sep + this.donneesSection.ex + '.js').then(function (datasSection) {
          // console.log("initsection");
          // Attention, le this devient window...
          sq.txt = datasSection.txt
          sq.entete = datasSection.entete
          sq.symbexact = datasSection.symbexact
          sq.symbnonexact = datasSection.symbnonexact
          sq.numero = datasSection.numero // Le nombre de paramètres LaTeX dans le texte
          // ?? retourne l’opérande de gauche si !== undefined && !== null, celle de droite sinon
          const listeBoutons = []
          // boutons avec propriétés à true par défaut
          const tabBoutons1 = ['puissance', 'fraction']
          ;['btnPuis', 'btnFrac'].forEach((p, i) => {
            const b = Boolean(datasSection[p] ?? true)
            if (b) listeBoutons.push(tabBoutons1[i])
          })
          // boutons avec false par défaut
          const tabBoutons2 = ['pi', 'racine', 'exp', 'ln', 'sin', 'cos', 'tan', 'abs']
          ;['btnPi', 'btnRac', 'btnExp', 'btnLn', 'btnSin', 'btnCos', 'btnTan', 'btnAbs'].forEach((p, i) => {
            const b = Boolean(datasSection[p] ?? false)
            if (b) listeBoutons.push(tabBoutons2[i])
          })
          sq.listeBoutons = listeBoutons
          sq.boutonsMathQuill = listeBoutons.length !== 0
          sq.nomVar = datasSection.nomVar ?? ''
          sq.nomVarRemp = datasSection.nomVarRemp ?? ''
          sq.consigne1 = datasSection.consigne1 ?? ''
          sq.consigne2 = datasSection.consigne2 ?? ''
          sq.consigne3 = datasSection.consigne3 ?? ''
          sq.consigne4 = datasSection.consigne4 ?? ''
          sq.titre = datasSection.titre ?? ''
          sq.param = datasSection.param ?? '' // Chaine contenant les paramètres autorisés
          sq.charset = datasSection.charset ?? ''
          initMtg()
        }).catch(error => {
          console.error(error)
          j3pShowError('Impossible de charger les données de cette exercice')
        })
      } else {
        j3pEmpty('correction')
        cacheSolution()
        sq.numEssai = 1
        $('#editeur').css('display', 'block')
        $('#boutonsmathquill').css('display', 'inline-block')
        sq.mtgAppLecteur.removeDoc('mtg32svg')
        sq.mtgAppLecteur.addDoc('mtg32svg', sq.txt, true)

        if (sq.aleat) {
          for (let i = 1; i <= sq.nbParamAleat; i++) {
            sq.mtgAppLecteur.giveFormula2('mtg32svg', 'r' + i, sq['tab' + i][this.questionCourante - 1])
          }
        }
        if (sq.param !== undefined) {
          const ch = 'abcdefghjklmnpqr'
          for (let i = 0; i < ch.length; i++) {
            const car = ch.charAt(i)
            if (sq.param.indexOf(car) !== -1) {
              const par = this.donneesSection[car]
              if (par !== 'random') sq.mtgAppLecteur.giveFormula2('mtg32svg', car, par)
            }
          }
        }
        sq.mtgAppLecteur.calculate('mtg32svg', true) // true pour que les calculs alèatoires soient rꪮitialisés
        j3pEmpty('enonce')
        j3pEmpty('formules')
        sq.nbexp = 0
        sq.reponse = -1
        const nbParam = sq.numero
        const t = ['a', 'b', 'c', 'd']
        const param = {}
        for (let i = 0; i < nbParam; i++) {
          param[t[i]] = sq.mtgAppLecteur.getLatexCode('mtg32svg', i) // le code
        }
        param.e = String(sq.nbEssais)
        // Consigne différente suivant qu’on demande une factorisation maximale ou non.
        const st = sq.consigne1 + (sq.factMax ? sq.consigne3 : sq.consigne2) + sq.consigne4
        afficheMathliveDans('enonce', 'texte', st, param)
        sq.mtgAppLecteur.display('mtg32svg') // Pour tout réafficher
        // Pour revenir en haut de page
        try {
          window.location.hash = 'MepMG'
          j3pElement('MepMG').scrollIntoView()
        } catch (e) {
        }
        j3pEmpty('info')
        afficheNombreEssaisRestants()
        j3pElement('info').style.display = 'block'
        // On vide le contenu de l’éditeur et on lui donne le focus.
        j3pElement(inputId).value = ''
        focusIfExists(inputId)
        this.finEnonce()
      }
      break // case "enonce":

    case 'correction': {
      let bilanreponse = ''
      let factmaxi = false
      // On regarde si le champ d’édition est rempli même si l’utilisateur
      // n’a pas appuyé sur la touche Entrée.
      const ch = getMathliveValue(inputId)
      sq.rep = getMathliveValue(inputId)
      if (ch !== '') {
        const repcalcul = traiteMathlive(sq.rep)
        const valide = variableValidation(repcalcul) &&
          sq.mtgAppLecteur.syntaxValidation('mtg32svg', 'rep', remplaceNomsVariables(repcalcul), true)
        if (valide) validation(false)
        else bilanreponse = 'incorrect'
      }
      const res1 = sq.reponse
      if (bilanreponse !== 'incorrect') {
        if (sq.factMax) { // Si on demande une factorisation maximale
          if (res1 === 1) {
            bilanreponse = 'exact'
            factmaxi = true
          } else {
            if (res1 === 2) {
              bilanreponse = 'exactpasfini'
            } else if (res1 === 3) bilanreponse = 'pasfini'
            else bilanreponse = 'erreur'
          }
        } else {
          if ((res1 === 1) || (res1 === 2)) {
            bilanreponse = 'exact'
            if (res1 === 1) {
              factmaxi = true
            }
          } else {
            if (res1 === 3) bilanreponse = 'pasfini'
            else bilanreponse = 'erreur'
          }
        }
      }
      // A cause de la limite de temps :
      if (this.isElapsed) { // limite de temps
        this._stopTimer()
        j3pElement('correction').style.color = this.styles.cfaux
        j3pElement('boutonrecopier').style.display = 'none'
        j3pElement('info').style.display = 'none'
        $('#editeur').css('display', 'none')
        $('#boutonsmathquill').css('display', 'none')
        afficheSolution(false)
        sq.mtgAppLecteur.executeMacro('mtg32svg', sq.factMax ? 'solutionMax' : 'solution')
        j3pElement('correction').innerHTML = tempsDepasse
        j3pElement('correction').innerHTML += '<br>La solution se trouve ci-contre.'
        this.etat = 'navigation'
        this.sectionCourante()
      } else {
        if (bilanreponse === 'incorrect') {
          j3pElement('correction').style.color = this.styles.cfaux
          j3pElement('correction').innerHTML = 'Réponse illisible'
          this.afficheBoutonValider()
          marqueEditeurPourErreur()
          focusIfExists(inputId)
        } else {
          // Bonne réponse
          if (bilanreponse === 'exact') {
            this._stopTimer()
            sq.mtgAppLecteur.setActive('mtg32svg', false)
            this.score++
            j3pElement('correction').style.color = this.styles.cbien
            if (factmaxi) {
              j3pElement('correction').innerHTML = cBien
            } else {
              j3pElement('correction').innerHTML = 'C’est bien mais on pouvait  factoriser plus.'
            }
            j3pElement('boutonrecopier').style.display = 'none'
            j3pElement('info').style.display = 'none'
            $('#editeur').css('display', 'none')
            $('#boutonsmathquill').css('display', 'none')
            afficheSolution(true)
            sq.mtgAppLecteur.executeMacro('mtg32svg', sq.factMax ? 'solutionMax' : 'solution')
            this.etat = 'navigation'
            this.sectionCourante()
          } else {
            j3pElement('correction').style.color = this.styles.cfaux
            sq.numEssai++
            if (bilanreponse === 'pasfini') {
              j3pElement('correction').innerHTML = 'Le calcul n’est pas fini.'
            } else {
              if (bilanreponse === 'exactpasfini') {
                j3pElement('correction').innerHTML = 'La factorisation est bonne mais pas maximale.'
              } else {
                if (res1 === 3) {
                  j3pElement('correction').innerHTML = 'L’expression n’est pas écrite sous la forme demandée.'
                } else {
                  j3pElement('correction').innerHTML = cFaux
                }
              }
            }
            afficheNombreEssaisRestants()
            // On donne le focus à l’éditeur MathQuill
            focusIfExists(inputId)

            // S’il y a plus de deux chances,on appelle à nouveau le bouton Valider
            if ((sq.nbexp < sq.nbEssais) && (sq.numEssai <= sq.nbchances)) {
              j3pElement('correction').innerHTML += '<br>' + '<br>' + essaieEncore
              sq.mtgAppLecteur.setActive('mtg32svg', true)
            } else {
              // Erreur au nème essai
              this._stopTimer()
              j3pElement('boutonrecopier').style.display = 'none'
              j3pElement('info').style.display = 'none'
              afficheSolution(false)
              sq.mtgAppLecteur.executeMacro('mtg32svg', sq.factMax ? 'solutionMax' : 'solution')
              $('#editeur').css('display', 'none')
              $('#boutonsmathquill').css('display', 'none')
              sq.mtgAppLecteur.setActive('mtg32svg', false)
              j3pElement('correction').innerHTML += '<br><br>La solution se trouve ci-contre.'
              this.etat = 'navigation'
              this.sectionCourante()
            }
          }
        }
      }
      // Obligatoire
      this.finCorrection()
      break // case "correction":
    }

    case 'navigation':
      if (this.sectionTerminee()) {
        // On détermine la phrase d’état renvoyée par la section
        this.parcours.pe = this.score / this.donneesSection.nbitems
        // this.afficheBoutonSectionSuivante()
        // this.desactiveReturn()
        // this.focus('sectioncontinuer')
      } else {
        this.etat = 'enonce'
        // this.afficheBoutonSuite()
        // this.desactiveReturn()
        // this.focus('boutoncontinuer')
      }
      // Obligatoire
      this.finNavigation()

      break // case "navigation":
  }
}
