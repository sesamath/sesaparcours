import $ from 'jquery'
import { j3pAddElt, j3pAjouteBouton, j3pDiv, j3pElement, j3pEmpty, j3pFocus, j3pImporteAnnexe, j3pPaletteMathquill, j3pShowError, j3pValeurde } from 'src/legacy/core/functions'
import { cleanLatexForMq, j3pAffiche, mqRestriction, unLatexify } from 'src/lib/mathquill/functions'
import { getMtgApp } from 'src/lib/outils/mathgraph'
import { j3pCreeSVG } from 'src/legacy/core/functionsSvg'
import textesGeneriques from 'src/lib/core/textes'

const { cBien, cFaux, essaieEncore, tempsDepasse } = textesGeneriques

/*
    Yves Biton
    2018

    squelettemtg32_Calc_Vect_Multi_Edit
    Squelette demandant de calculer des résultats vectoriels
    Utilise une fichier annexe dont le nom est contenu dans le paramètre ex.
    Ce fichier annexe contient dans txt le code Base64 d’une figure mtg32 qui contient la figure utilisée.
    IL contient une variable nbCalc qui contient le nombre d’éditeurs que l’élève doit remplir
    Il contient aussi une variable formulaire qui est une chaîne de caractères contenant éventuellement du LaTeX et contenant
    une référence aux éditeurs MathQuill utilisés. Par exemple, si nbCalc  vaut 2, la chaine devra contenir deux sous-chaînes
    "edit1" et "edit2" qui correspondront aux éditeurs MathQuill.
    Chaque éditeur accepte au maximum l’entrée de 2 caractères (il peut y en avoir un seul pour le vecteur nul).
    Les réponses attendues sont vectorielles et l’exercice n’est pas aléatoire.
    Le fichier annexe ex contient une variable solutions qui est un tableau de taille nbCalc contenant les bonnes réponses.
    Chaque élément de ce tableau est lui-même un tableau contenant toutes les bonnes réponses admissibles pour l’éditeur asscoié.
    Par exemple, si nbCalc vaut 2, solution pourrait âtre égal à [[AB,PQ],[BC]] il y aurait alors deux bonnes réponses pour
    le premier éditeur et une seule pour le second.

*/

export const params = {
  outils: ['mathquill'],
  /**
   * Ce qui est paramétrable dans l’éditeur de graphe
   * @type {LegacySectionParamsParametres}
   */
  parametres: [
    ['limite', 0, 'entier', 'Temps disponible par répétition, en secondes'],
    ['nbEssais', 6, 'entier', 'Nombre d’essais maximum autorisés'],
    ['simplifier', true, 'boolean', 'true si on exige un forme donnée, false sinon'],
    ['ex', 'Lycee_Vecteurs_Chasles_2', 'string', 'Nom du fichier annexe (xxx => squelettes-mtg32/xxx.js et xxx/yyy => squelettes-mtg32-xxx/yyy.js)']
  ]
}
/**
 * section squelettemtg32_Calc_Vect_Multi_Edit
 * @this {Parcours}
 */
export default function main () {
  const parcours = this
  const sq = this.storage
  function onkeyup () {
    const indice = this.indice
    if (sq.marked[indice - 1]) { demarqueEditeurPourErreur(indice) }
    if (sq.vecteurs[indice - 1]) {
      const txt = $('#' + sq.idEdit[indice - 1]).text()
      const txtTab = txt.match(/[A-Z]/g)
      let newTxt = ''
      if (txtTab !== null) {
        for (let k = 0; k < txtTab.length; k++) {
          newTxt += txtTab[k]
        }
        if (newTxt.length > 2) {
          $('#' + sq.idEdit[indice - 1]).mathquill('latex', newTxt.substring(0, 2))
        }
      }
    }
  }

  /**
   * Fonction extrayant d’un affichage LaTeX de la figure les lignes de texte et les transformant
   * en une chaîne affichable par MathQuill
   * @param tagLatex : le tag de l’affichage LaTeX de la figure
   * @return {string}
   */
  function extraitDeLatexPourMathQuill (tagLatex) {
    const list = sq.mtgAppLecteur.getList('mtg32svg')
    const latex = list.getByTag(tagLatex)
    if (latex === null || latex.className !== 'CLatex') return ''
    return cleanLatexForMq(latex.chaineLatex)
  }

  /**
   * Fonction affichant la solution s’il existe un affichage LaTeX de tag solution
   * @param{boolean} bexact true si l’élève a entré la bonne réponse, false sinon
   */
  function afficheSolution (bexact) {
    $('#divSolution').css('display', 'block').html('')
    const ch = extraitDeLatexPourMathQuill('solution')
    if (ch !== '') {
      j3pAffiche('divSolution', 'solution', ch, { style: { color: bexact ? 'green' : 'blue' } })
    }
  }

  function cacheSolution () {
    $('#divSolution').css('display', 'none').html('')
  }

  function afficheNombreEssaisRestants () {
    j3pEmpty('info')
    const nbe = sq.nbEssais - sq.numEssai + 1
    if (nbe === 1) j3pAffiche('info', 'texteinfo', 'Il reste un essai.', { style: { color: '#7F007F' } })
    else j3pAffiche('info', 'texteinfo', 'Il reste ' + nbe + ' essais.', { style: { color: '#7F007F' } })
  }

  function traiteMathQuill (ch) {
    ch = unLatexify(ch)
    if (!sq.longueursPermises) ch = sq.mtgAppLecteur.addImplicitMult('mtg32svg', ch)// Traitement des multiplications implicites Pas pour cet exercice (longueur)
    return ch
  }

  function marqueEditeurPourErreur (indice) {
    sq.marked[indice - 1] = true
    $('#' + sq.idEdit[indice - 1]).css('background-color', '#FF9999')
  }

  function demarqueEditeurPourErreur (indice) {
    sq.marked[indice - 1] = false
    $('#' + sq.idEdit[indice - 1]).css('background-color', '#FFFFFF')
  }

  function validationEditeurs () {
    let rep; let res = true
    let focusDonne = false
    for (let ind = 1; ind <= sq.nbCalc; ind++) {
      const idEdit = sq.idEdit[ind - 1]
      if (sq.vecteurs[ind - 1]) {
        rep = $('#' + idEdit).text()
        if (rep.length <= 1 && rep !== '0') {
          res = false
          marqueEditeurPourErreur(ind)
          if (!focusDonne) {
            focusDonne = true
            j3pFocus(idEdit)
          }
        }
      } else {
        rep = j3pValeurde(idEdit)
        if (rep === '') {
          res = false
          marqueEditeurPourErreur(ind)
          if (!focusDonne) {
            focusDonne = true
            j3pFocus(idEdit)
          }
        } else {
          const chcalcul = traiteMathQuill(rep)
          // Dernier rparamètre false pour ne pas avoir de multiplications implicites si sq.longueursPermises est à true
          const valide = sq.mtgAppLecteur.syntaxValidation('mtg32svg', 'rep' + ind, chcalcul, !sq.longueursPermises)
          if (!valide) {
            res = false
            marqueEditeurPourErreur(ind)
            if (!focusDonne) {
              focusDonne = true
              j3pFocus(idEdit)
            }
          }
        }
      }
    }
    return res
  }

  function montreEditeurs (bVisible) {
    if (bVisible) $('#info').css('display', 'block')
    $('#editeur').css('display', bVisible ? 'block' : 'none')
    if (sq.boutonsMathQuill) $('#boutonsmathquill').css('display', bVisible ? 'block' : 'none')
    // On vide le contenu de l’éditeur MathQuill et on lui donne le focus.
    if (bVisible) $('#' + sq.idEdit[0]).mathquill('latex', ' ')
    j3pFocus(sq.idEdit[0])
  }

  function videEditeurs () {
    for (let i = 1; i <= sq.nbCalc; i++) {
      $('#' + sq.idEdit[i - 1]).mathquill('latex', ' ')
    }
    // On donne le focus au premier éditeur
    j3pFocus(sq.idEdit[0])
  }

  function determineIndicesEditeurs () {
    sq.idEdit = []
    let indmq = 0
    let indmqef = 0
    for (let k = 1; k <= sq.nbCalc; k++) {
      if (sq.vecteurs[k - 1]) {
        indmqef++
        sq.idEdit.push('expressioninputmqef' + indmqef)
      } else {
        indmq++
        sq.idEdit.push('expressioninputmq' + indmq)
      }
    }
  }

  function afficheReponse (bilan, depasse) {
    let ch, coul, st
    if (bilan === 'exact') {
      ch = 'Réponse exacte : '
      coul = parcours.styles.cbien
    } else {
      if (bilan === 'faux') {
        ch = 'Réponse fausse : '
        coul = parcours.styles.cfaux
      } else { // Exact mais pas fini
        ch = 'Exact pas fini : '
        if (depasse) coul = parcours.styles.cfaux
        else coul = '#0000FF'
      }
    }
    const num = sq.numEssai
    const idrep = 'exp' + num
    if (num > 2) ch = '<br>' + ch
    j3pAffiche('formules', idrep + 'deb', ch, {
      styletexte: {
        couleur: coul
      }
    })

    ch = sq.formulaire

    for (let i = 1; i <= sq.nbCalc; i++) {
      st = '\\textcolor{' + (sq.repResolu[i - 1] ? parcours.styles.cbien : (sq.repExact[i - 1] ? '#0000FF' : parcours.styles.cfaux)) + '}{'
      if (sq.vecteurs[i - 1]) ch = ch.replace('edit' + i, '$\\vecteur{\\mathrm{' + st + sq.rep[i - 1] + '}}}$')
      else ch = ch.replace('edit' + i, '$' + st + sq.rep[i - 1] + '}$')
    }
    j3pAffiche('formules', idrep, ch, sq.paramFormulaire)
    const texteFaute = sq.indicationfaute ? extraitDeLatexPourMathQuill('faute') : ''
    if (bilan === 'faux' && texteFaute) {
      j3pAffiche('formules', idrep + 'faute', '<br>' + sq.texteFaute, {
        styletexte: {
          couleur: coul
        }
      })
    }
  }

  function nettoieChaineMathQuill (ch) {
    let st = ''
    for (let i = 0; i < ch.length; i++) {
      const car = ch.charAt(i)
      if (car.search(/[A-Z\d]/g) !== -1) st = st + car
      // else if (car.search(/\d/g) !== -1) st = st + car;
    }
    return st
  }

  function ecoute () {
    if (sq.boutonsMathQuill) {
      const indice = this.indice
      if (sq.vecteurs[indice - 1]) $('#palette').css('display', 'none')
      else {
        j3pEmpty('boutonsmathquill')
        j3pPaletteMathquill('boutonsmathquill', sq.idEdit[indice - 1], {
          liste: sq.listeBoutons,
          nomdiv: 'palette'
        })
        // Ligne suivante pour un bon alignement
        $('#palette').css('display', 'inline-block')
      }
    }
  }

  function validation () {
    let rep, exac, resol
    if (sq.numEssai >= 1) {
      j3pElement('boutonrecopier').style.display = 'block'
    }
    sq.rep = []
    sq.repExact = []
    sq.repResolu = []
    for (let ind = 1; ind <= sq.nbCalc; ind++) {
      if (sq.vecteurs[ind - 1]) {
        rep = nettoieChaineMathQuill($('#' + sq.idEdit[ind - 1]).text())
      } else {
        rep = j3pValeurde(sq.idEdit[ind - 1])
        const chcalcul = traiteMathQuill(rep)
        sq.mtgAppLecteur.giveFormula2('mtg32svg', 'rep' + ind, chcalcul)
      }
      sq.rep.push(rep)
    }
    sq.mtgAppLecteur.calculate('mtg32svg', false)
    let auMoinsUnExact = false
    let exact = true
    let resolu = true
    for (let ind = 1; ind <= sq.nbCalc; ind++) {
      if (sq.vecteurs[ind - 1]) {
        const sol = sq['solutions' + sq.form][ind - 1]
        rep = sq.rep[ind - 1]
        if ((sol.length === 1) && (sol[0] === '0') && (rep.length === 2)) {
          if (rep.charAt(0) === rep.charAt(1)) {
            resol = false
            exac = true
          } else {
            resol = false
            exac = false
          }
        } else {
          exac = false
          for (let i = 0; (i < sol.length && !exac); i++) {
            exac = exac || (sol[i] === rep)
          }
          resol = exac
        }
        sq.repResolu.push(resol)
        sq.repExact.push(exac)
        resolu = resolu && resol
        exact = exact && exac
      } else {
        const res = sq.mtgAppLecteur.valueOf('mtg32svg', 'resolu' + ind)
        const ex = sq.mtgAppLecteur.valueOf('mtg32svg', 'exact' + ind)
        resolu = resolu && (res === 1)
        exact = exact && ((res === 1) || (ex === 1))
        sq.repResolu.push(res === 1)
        sq.repExact.push((res === 1) || (ex === 1))
        auMoinsUnExact = auMoinsUnExact || (ex === 1)
      }
    }
    sq.resolu = resolu
    sq.exact = exact && auMoinsUnExact
  }

  function recopierReponse () {
    let sel, ind
    for (ind = 1; ind <= sq.nbCalc; ind++) {
      demarqueEditeurPourErreur(ind)
      // var sel = $("#expressioninputmq" + ind);
      sel = $('#' + sq.idEdit[ind - 1])
      sel.mathquill('latex', sq.rep[ind - 1])
    }
    // On donne le focus au premeir éditeur où la réponse n’est pas résolue
    for (ind = 1; ind <= sq.nbCalc; ind++) {
      if (!sq.repResolu[ind - 1]) {
        j3pFocus(sq.idEdit[ind - 1])
        break
      }
    }
  }

  function initMtg () {
    const mtgOptions = {
      // player only, sans figure initiale ni svg, on gère ça dans initMtg et modif_fig
      loadCoreWithMathJax: true
    }
    getMtgApp('mtg32svg', {}, mtgOptions).then((mtgAppLecteur) => {
      sq.mtgAppLecteur = mtgAppLecteur
      initDom()
      let code, i, k
      sq.mtgAppLecteur.removeAllDoc()
      sq.mtgAppLecteur.addDoc('mtg32svg', sq.txt, true)
      sq.mtgAppLecteur.calculate('mtg32svg', true)
      const form = sq.mtgAppLecteur.valueOf('mtg32svg', 'form') // Le n° de formule utilis s’il existe
      sq.form = (form === -1) ? 1 : form
      sq.mtgAppLecteur.display('mtg32svg')
      const nbParam = parseInt(sq.nbLatex)
      const t = ['a', 'b', 'c', 'd', 'e', 'f', 'g', 'h', 'i', 'j', 'k', 'l', 'm', 'n', 'o', 'p', 'q']
      const param = {}
      for (i = 0; i < nbParam; i++) {
        code = sq.mtgAppLecteur.getLatexCode('mtg32svg', i)
        param[t[i]] = code
      }
      const st = sq.consigne1 + (sq.simplifier ? sq.consigne3 : sq.consigne2) + sq.consigne4
      j3pAffiche('enonce', 'texte', st, param)
      sq.indicationfaute = parcours.donneesSection.indicationfaute // true si indication quand certaines fautes sont faites et appui sur Entrée
      let formulaire = sq.formulaire
      let idmq = 0
      for (k = 1; k <= sq.nbCalc; k++) {
        if (sq.vecteurs[k - 1]) formulaire = formulaire.replace('edit' + k, '$\\vecteur{\\mathrm{\\editable{}}}$')
        else {
          idmq++
          formulaire = formulaire.replace('edit' + k, '&' + idmq + '&')
        }
      }
      const obj = {}
      sq.paramFormulaire = {}
      for (k = 1; k <= sq.nbCalc; k++) {
        if (!sq.vecteurs[k - 1]) obj['inputmq' + k] = {}
      }
      for (i = 0; i < nbParam; i++) {
        code = sq.mtgAppLecteur.getLatexCode('mtg32svg', i)
        obj[t[i]] = code
        sq.paramFormulaire[t[i]] = code
      }
      obj.styletexte = {}
      j3pAffiche('editeur', 'expression', formulaire, obj) // sq.formulaire contient ou plusieurs éditeurs MathQuill
      // On affecte un indice à chaque éditeur ainsi qu’un écouteur de focus
      determineIndicesEditeurs()
      for (k = 1; k <= sq.nbCalc; k++) {
        const idEditeur = sq.idEdit[k - 1]
        if (sq.vecteurs[k - 1]) {
          j3pElement(idEditeur).indice = k
          if (sq.charsetvect !== '') {
            mqRestriction(idEditeur, sq.charsetvect, {
              commandes: sq.listeBoutons,
              boundingContainer: parcours.zonesElts.MG
            })
          }
          j3pElement(idEditeur).onkeyup = function (ev) {
            onkeyup.call(this, ev, parcours)
          }
          $('#' + idEditeur).focusin(function () {
            ecoute.call(this)
          })
          // j3pElement("expression" + sq.indiceEditeurVect(k)).onkeydown = sq.onkeydown;
        } else {
          j3pElement(idEditeur).indice = k
          mqRestriction(idEditeur, sq.charsetmq, {
            boundingContainer: parcours.zonesElts.MG
          })
          j3pElement(idEditeur).onkeyup = function (ev) {
            onkeyup.call(this, ev, parcours)
          }
          $('#' + idEditeur).focusin(function () {
            ecoute.call(this)
          })
        }
      }
      j3pFocus(sq.idEdit[0])
      parcours.finEnonce()
    }).catch(j3pShowError)
  }

  function initDom () {
    if (sq.titre) parcours.afficheTitre(sq.titre)

    j3pDiv(parcours.zones.MG, { id: 'conteneur', contenu: '', style: parcours.styles.etendre('toutpetit.enonce', { padding: '6px' }) })
    j3pDiv('conteneur', 'enonce', '')
    j3pDiv('conteneur', {
      id: 'formules',
      contenu: '',
      style: parcours.styles.petit.enonce
    }) // Contient le formules entrées par l’élève
    j3pDiv('conteneur', 'info', '')
    afficheNombreEssaisRestants()
    j3pDiv('conteneur', 'conteneurbouton', '')
    j3pAjouteBouton('conteneurbouton', 'boutonrecopier', 'MepBoutonsRepere', 'Recopier réponse précédente',
      recopierReponse)
    j3pElement('boutonrecopier').style.display = 'none'
    j3pDiv('conteneur', 'editeur', '') // Le div qui contiendra la chaine avec le ou les éditeurs MathQuill
    $('#editeur').css('padding-top', '10px') // Pour laisser un peu de place au-dessus de l’éditeur
    j3pDiv('conteneur', 'boutonsmathquill', '')
    $('#boutonsmathquill').css('display', 'inline-block').css('padding-top', '10px') // Pour laisser un peu de place au-dessus des boutons
    j3pDiv('conteneur', 'divSolution', '')
    $('#divSolution').css('display', 'none')
    j3pDiv('conteneur', 'divmtg32', '')
    j3pCreeSVG('divmtg32', {
      id: 'mtg32svg',
      width: 700,
      height: 900
    })
    const style = parcours.styles.petit.correction
    style.marginTop = '1.5em'
    style.marginLeft = '0.5em'
    j3pAddElt(parcours.zonesElts.MD, 'div', '', { id: 'correction', style })
  }

  function _Donnees () {
    // "primaire" || "lycee" || "college"
    // Actuellement les dimensions sont identiques
    this.typesection = 'lycee'//

    // Nombre de répétitions de l’exercice
    this.nbrepetitions = 1
    this.nbetapes = 1
    this.nbitems = this.nbetapes * this.nbrepetitions

    this.ex = 'Lycee_Vecteurs_Chasles_2'
    // Si indication != "" alors un lien "Indication" s’affiche dans la zone Inférieure Gauche (this.zones.IG
    // une indication<br>de deux lignes...";
    this.indication = ''
    const st = 'abcdefghjklmnpqr'
    for (let i = 0; i < st.length; i++) this[st.charAt(i)] = 'random'
    this.nbEssais = 6
    this.simplifier = true
    this.indicationfaute = true

    this.textes = {
    }
    this.titre = 'Titre de l’activité'

    this.pe = 0
  }

  switch (this.etat) {
    case 'enonce':
      // code exécuté au lancement de la section
      if (this.debutDeLaSection) {
        this.donneesSection = new _Donnees()
        // Construction de la page
        parcours.construitStructurePage('presentation1bis')

        this.surcharge()

        // compteur.
        sq.numEssai = 1
        sq.nbEssais = parseInt(parcours.donneesSection.nbEssais)
        sq.simplifier = parcours.donneesSection.simplifier ?? true

        // faut gérer sous-dossier ou pas…
        const sep = this.donneesSection.ex.includes('/') ? '-' : '/'
        j3pImporteAnnexe('squelettes-mtg32' + sep + this.donneesSection.ex + '.js').then(function (datasSection) {
          // console.log("initsection");
          // Attention, le this devient window...
          sq.txt = datasSection.txt
          sq.nbLatex = datasSection.nbLatex // Le nombre de paramètres LaTeX dans le texte
          // ?? retourne l’opérande de gauche si !== undefined && !== null, celle de droite sinon
          const listeBoutons = []
          // boutons avec propriétés à true par défaut
          const tabBoutons1 = ['puissance', 'fraction']
          ;['btnPuis', 'btnFrac'].forEach((p, i) => {
            const b = Boolean(datasSection[p] ?? true)
            if (b) listeBoutons.push(tabBoutons1[i])
          })
          // boutons avec false par défaut
          const tabBoutons2 = ['pi', 'racine', 'exp', 'ln', 'sin', 'cos', 'tan', 'abs']
          ;['btnPi', 'btnRac', 'btnExp', 'btnLn', 'btnSin', 'btnCos', 'btnTan', 'btnAbs'].forEach((p, i) => {
            const b = Boolean(datasSection[p] ?? false)
            if (b) listeBoutons.push(tabBoutons2[i])
          })
          sq.listeBoutons = listeBoutons
          sq.boutonsMathQuill = listeBoutons.length !== 0
          sq.consigne1 = datasSection.consigne1 ?? ''
          sq.consigne2 = datasSection.consigne2 ?? ''
          sq.consigne3 = datasSection.consigne3 ?? ''
          sq.consigne4 = datasSection.consigne4 ?? ''

          sq.titre = datasSection.titre ?? ''
          sq.nbCalc = datasSection.nbCalc
          sq.longueursPermises = Boolean(datasSection.longueursPermises ?? false)
          sq.formulaire = datasSection.formulaire // Contient la chaine avec les éditeurs.
          sq.nbform = datasSection.nbform ?? 1
          for (let k = 1; k <= sq.nbform; k++) sq['solutions' + k] = datasSection['solutions' + k] // Chaine contenant les paramètres autorisés
          sq.vecteurs = datasSection.vecteurs // Tableau contenant true quand la réponse attendue est un vecteur et false sinon
          sq.charsetvect = datasSection.charsetvect ?? ''
          sq.charsetmq = datasSection.charsetmq ?? ''
          sq.marked = []
          for (let k = 0; k < sq.nbCalc; k++) {
            sq.marked[k] = false
          }
          initMtg()
        }).catch(error => {
          console.error(error)
          j3pShowError('Impossible de charger les données de cette exercice')
        })
        // return;
      } else {
        j3pEmpty('correction')
        cacheSolution()
        this.afficheBoutonValider()
        sq.numEssai = 1
        this.finEnonce()
      }

      break // case "enonce":

    case 'correction': {
      let bilanReponse = ''
      let simplificationPossible = false
      if (validationEditeurs()) {
        validation()
        if (sq.simplifier) {
          if (sq.resolu) {
            bilanReponse = 'exact'
          } else {
            if (sq.exact) {
              bilanReponse = 'exactPasFini'
              simplificationPossible = true
            } else {
              bilanReponse = 'faux'
            }
          }
        } else {
          if (sq.resolu || sq.exact) {
            bilanReponse = 'exact'
            if (!sq.resolu) simplificationPossible = true
          } else {
            bilanReponse = 'faux'
          }
        }
      } else {
        bilanReponse = 'incorrect'
      }
      const eltCorrection = j3pElement('correction')
      // A cause de la limite de temps :
      if (this.isElapsed) { // limite de temps
        this._stopTimer()
        montreEditeurs(false)
        eltCorrection.style.color = this.styles.cfaux
        j3pElement('boutonrecopier').style.display = 'none'
        j3pElement('info').style.display = 'none'
        if (sq.indicationfaute) sq.mtgAppLecteur.executeMacro('mtg32svg', 'masquerFaute')

        sq.mtgAppLecteur.executeMacro('mtg32svg', 'solution')
        afficheSolution(false)
        eltCorrection.innerHTML = tempsDepasse
        eltCorrection.innerHTML += '<br>La solution se trouve ci-contre.'
        this.etat = 'navigation'
        this.sectionCourante()
      } else {
        if (bilanReponse === 'incorrect') {
          eltCorrection.style.color = this.styles.cfaux
          eltCorrection.innerHTML = 'Réponse incorrecte'
          this.afficheBoutonValider()
        } else {
          // Une réponse a été saisie
          // Bonne réponse
          if (bilanReponse === 'exact') {
            this._stopTimer()
            sq.mtgAppLecteur.setActive('mtg32svg', false)
            this.score++
            eltCorrection.style.color = this.styles.cbien
            if (simplificationPossible) eltCorrection.innerHTML = 'C’est bien mais on pouvait  simplifier la réponse'
            else eltCorrection.innerHTML = cBien
            j3pElement('boutonrecopier').style.display = 'none'
            j3pElement('info').style.display = 'none'
            if (sq.indicationfaute) sq.mtgAppLecteur.executeMacro('mtg32svg', 'masquerFaute')
            sq.mtgAppLecteur.executeMacro('mtg32svg', 'solution')
            afficheSolution(true)
            montreEditeurs(false)
            sq.numEssai++ // Pour un affichage correct dans afficheReponse
            afficheReponse('exact', false)
            this.etat = 'navigation'
            this.sectionCourante()
          } else {
            sq.numEssai++
            if (bilanReponse === 'exactPasFini') {
              eltCorrection.style.color = (sq.numEssai <= sq.nbEssais) ? '#0000FF' : this.styles.cfaux
              eltCorrection.innerHTML = 'Le calcul est bon mais pas écrit sous la forme demandée.'
            } else {
              eltCorrection.style.color = this.styles.cfaux
              eltCorrection.innerHTML = cFaux
            }

            j3pEmpty('info')
            if (sq.numEssai <= sq.nbEssais) {
              afficheNombreEssaisRestants()
              videEditeurs()
              afficheReponse(bilanReponse, false)
              // S’il y a plus de deux chances,on appelle à nouveau le bouton Valider
              eltCorrection.innerHTML += '<br>' + '<br>' + essaieEncore
            } else {
              // Erreur au nème essai
              this._stopTimer()
              sq.mtgAppLecteur.setActive('mtg32svg', false)
              montreEditeurs(false)
              this.cacheBoutonValider()
              j3pElement('boutonrecopier').style.display = 'none'
              j3pElement('info').style.display = 'none'
              if (sq.indicationfaute) sq.mtgAppLecteur.executeMacro('mtg32svg', 'masquerFaute')
              sq.mtgAppLecteur.executeMacro('mtg32svg', 'solution')
              afficheSolution(false)
              afficheReponse(bilanReponse, true)
              eltCorrection.innerHTML += '<br><br>La solution se trouve ci-contre.'
              this.etat = 'navigation'
              this.sectionCourante()
            }
          }
        }
      }
      // Obligatoire
      this.finCorrection()
      break // case "correction":
    }

    case 'navigation':

      if (this.sectionTerminee()) {
        // On détermine la phrase d’état renvoyée par la section
        this.parcours.pe = this.score / this.donneesSection.nbitems
        this.afficheBoutonSectionSuivante()
        this.focus('sectioncontinuer')
      } else {
        this.etat = 'enonce'
        this.afficheBoutonSuite()
        this.focus('boutoncontinuer')
      }
      // Obligatoire
      this.finNavigation()

      break // case "navigation":
  }
}
