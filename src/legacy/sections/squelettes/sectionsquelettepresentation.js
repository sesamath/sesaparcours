import { j3pAddContent, j3pAddElt, j3pImporteAnnexe, j3pShowError } from 'src/legacy/core/functions'
import { j3pBaseUrl } from 'src/lib/core/constantes'

/**
 * section squelettepresentation
 * @author JP Vanroyen
 * @since juin 2013
 * @fileOverview
*/

export const params = {
  outils: ['calculatrice'],
  /**
   * Ce qui est paramétrable dans l’éditeur de graphe
   * @type {LegacySectionParamsParametres}
   */
  parametres: [
    ['titre', 'Information', 'string', 'Titre à afficher'],
    ['f', 'presentationbouquet', 'string', 'Nom du fichier annexe (xxx => sectionsAnnexes/presentation/xxx.js)']
  ]
}

/**
 * Retourne le code html pour insérer une vidéo
 * @private
 * @param {HTMLElement} conteneur
 * @param {Object} options
 * @param {string} options.fichier Nom du fichier dans /static/videos/
 * @param {number} options.width
 * @param {number} options.height
 * @return {string}
 */
function addVideo (conteneur, { fichier, width, height }) {
  // on ajoute le préfixe url
  fichier = `${publicPath}static/videos/${fichier}`
  const props = {}
  if (typeof width === 'number') props.width = `${width}px`
  if (typeof height === 'number') props.height = `${height}px`
  const video = j3pAddElt(conteneur, 'video', '', { ...props, controls: true })
  j3pAddElt(video, 'source', '', { src: `${fichier}.mp4`, type: 'video/mp4' })
  j3pAddElt(video, 'source', '', { src: `${fichier}.webm`, type: 'video/webm;codecs=vp8,vorbis' })
}

/**
 * Charge le fichier annexe et retourne son contenu
 * @param {string} fichier le fichier js à charger dans legacy/sectionsAnnexes/presentation/, sans son extension
 * @return {Promise<Object>} Attention, toujours null en cas de pb
 */
async function loadAnnexePresentation (fichier) {
  try {
    const datas = await j3pImporteAnnexe(`presentation/${fichier}.js`)
    if (!datas || typeof datas !== 'object') {
      console.error(Error(`Le chargement de ${fichier} a réussi mais le contenu n’est pas celui attendu`), datas)
      return null
    }
    return datas
  } catch (error) {
    console.error(error)
    return null
  }
}

const publicPath = j3pBaseUrl

/**
 * section squelettepresentation
 * @this {Parcours}
 */
export default function main () {
  const me = this

  if (this.etat === 'enonce') {
    // code exécuté au lancement de la section, ici pas besoin de tester debutDeLaSection vu qu’il n’y a pas de répétition
    // toujours un seul affichage pour la présentation, ce fut paramétrable à une époque, d’où l’écrasement ici
    this.surcharge({ nbetapes: 1, nbrepetitions: 1 })
    this.setPassive()
    // Construction de la page (avec presentation3 le ratioGauche ne sert que pour le haut, y’a pas de bas et le milieu est toujours à 100%)
    this.construitStructurePage({ structure: 'presentation3', ratioGauche: 0.6 })
    // on prépare le dom
    const style = me.styles.etendre('toutpetit.enonce', {
      border: '2px solid #000',
      background: '#f0f0f0',
      padding: '10px',
      margin: '40px'
    })
    const divConteneur = j3pAddElt(me.zonesElts.MG, 'div', '', { style })
    j3pAddElt(divConteneur, 'h2', me.donneesSection.titre, { className: 'j3ptitre' })
    // on récupère la liste de fichiers à charger
    const fichiers = this.donneesSection.f.split(',').filter(f => {
      if (/^[\w_-]+$/.test(f)) return true // lettres, chiffres - _
      j3pShowError(`paramètre f invalide (${f}), ignoré`)
      return false
    })
    if (!fichiers.length) return me.finEnonce()
    // et on charge les fichiers
    // on lance le chargement (asynchrone)
    Promise.all(fichiers.map(loadAnnexePresentation))
      .then(results => {
        for (const { html, txt, video } of results) {
          let str = txt || ''
          if (html) str += html
          if (str) j3pAddContent(divConteneur, str)
          if (video) addVideo(divConteneur, video)
        }
        me.finEnonce()
      })
      .catch(j3pShowError)
  }
}
