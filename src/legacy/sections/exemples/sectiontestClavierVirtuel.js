import { j3pAffiche, mqRestriction } from 'src/lib/mathquill/functions'
import { j3pAddElt, j3pFocus } from 'src/legacy/core/functions'
import { addConsoleOnScreen } from 'src/lib/utils/debug'

export const params = {
  outils: ['mathquill'],
  /**
   * Ce qui est paramétrable dans l’éditeur de graphe
   * @type {LegacySectionParamsParametres}
   */
  parametres: []

}

const structure = 'presentation1'

export default function testClavierVirtuel () {
  const parcours = this

  switch (this.etat) {
    case 'enonce':
      // code exécuté au lancement de la section
      if (this.debutDeLaSection) {
        this.donneesSection = {}
        // Construction de la page
        parcours.construitStructurePage(structure)
        this.surcharge()

        const container = this.zonesElts.MG

        // debug
        const divConsole = addConsoleOnScreen(parcours.zonesElts.MG)

        const enonce = 'Du blabla avec &1& un champ mathquill\nLorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam, quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea commodo consequat.\nDuis aute irure dolor in reprehenderit &2& in voluptate velit esse cillum dolore eu fugiat nulla pariatur. Excepteur sint occaecat cupidatat non proident, sunt in culpa qui officia deserunt mollit anim id est laborum.'
        const { inputmqList } = j3pAffiche(container, '', enonce, {})
        const defaultCommands = ['puissance', 'racine', 'fraction', 'pi', 'abs', 'conj']
        const moreCommands = defaultCommands.concat(['exp', 'ln', 'sin'])
        Array.from(inputmqList).forEach((inputMq, i) => {
          const commandes = i ? moreCommands : defaultCommands
          mqRestriction(inputMq, /[0-9a-cx-z./^*()]/, { commandes, boundingContainer: this.zonesElts.MG })
        })
        // le focus sur le 1er inputMq
        j3pFocus(inputmqList[0]) // inutile, le dépliement ci-dessous va mettre le focus dessus
        // et on déplie d’office le clavier sur
        // inputmqList[0].virtualKeyboard.show()

        // on remet la console à la fin
        j3pAddElt(container, 'br')
        container.appendChild(divConsole)
      }
      this.finEnonce()
      break // case "enonce":

    case 'correction':
      this.finCorrection('enonce')
      break // case "correction":

    case 'navigation':
      // on passe jamais là dans cette section de test
      this.finNavigation()
      break // case "navigation":
  }
}
