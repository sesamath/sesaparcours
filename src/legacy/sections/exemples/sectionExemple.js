import { j3pAddElt, j3pFocus, j3pGetRandomInt, j3pRestriction, j3pStyle } from 'src/legacy/core/functions'
import { afficheBloc, phraseBloc } from 'src/legacy/core/functionsPhrase'
import ValidationZones from 'src/legacy/outils/calculatrice/ValidationZones'
import textesGeneriques from 'src/lib/core/textes'

const { essaieEncore, tempsDepasse } = textesGeneriques

/**
 * Mettre ici une description de la section
 * @author votre nom <votre mail>
 * @since date
 * @fileOverview
 */

/* Ce fichier est un exemple de section, à utiliser comme base de départ pour en créer une nouvelle
 * - remplacer Exemple par le nom de la section dans ce fichier
 * - enregistrer ce fichier sous le nom sectionxxx.js dans le dossier voulu
 * - indiquer le chemin dans legacy/core/adresses.js
 * - supprimer ce commentaire
 * - ajouter votre code
 */

// On peut déclarer ici tout ce qu’on veut, des constantes, des fonctions, etc.

// Tout ce qui est déclaré ici est local à ce fichier (ça ne va rien mettre en global,
// si on veut déclarer un truc utilisé ailleurs alors il faut l’exporter).

// Attention, le code de ce fichier est exécuté une fois au chargement du fichier, plus jamais ensuite,
// il est donc fortement déconseillé d’y mettre des variables (dont la valeur pourrait changer
// au fil de l’exécution), car en cas de rappel de la même section dans un autre nœud du graphe,
// la valeur de cette variable ne sera pas réinitialisée !
// (idem s’il y avait plusieurs instances simultanées d’un parcours,
// cette variable aurait la même valeur pour tout le monde)
// Pour conserver des valeurs entre les différents appels de la fct principale, utiliser plutôt me.storage
// (qui lui est spécifique à chaque instance de Parcours, et initialisé avec un objet vide à chaque démarrage d’un nœud)

// nos constantes
const structure = 'presentation1'
// const ratioGauche = 0.75 C’est la valeur par défaut du ratio de la partie gauche dans presentation1
// N’utiliser cette variable que si ce ratio est différent
// (utilisé dans ce cas dans me.construitStructurePage(structure, ratioGauche) de initSection()

export const params = {
  // seuls les outils qui ne peuvent pas être importés doivent être déclarés ici (pour que le loader les charge),
  // sinon c’est à nous de les importer au début
  outils: ['calculatrice', 'mathquill'],
  /**
   * Ce qui est paramétrable dans l’éditeur de graphe
   * @type {LegacySectionParamsParametres}
   */
  parametres: [
    ['nbrepetitions', 3, 'entier', 'Nombre de répétitions de la section'],
    ['indication', '', 'string', 'Indication pour chaque répétition'],
    ['limite', 0, 'entier', 'Temps disponible par répétition, en secondes'],
    ['nbchances', 1, 'entier', 'Nombre d’essais par répétition']
    // les paramètres peuvent être du type 'entier', 'string', 'boolean', 'liste'
  ],
  // la liste des pe qui peuvent être retournés par une section quantitative
  pe: [
    { pe_1: 'Une pe' },
    { pe_2: 'Une autre pe' }
  ]
}

/**
 * Un objet pour regrouper les textes affichés (ce sera plus pratique pour internationaliser ensuite)
 * @private
 */
const textes = {
  // le mieux est de mettre également le texte correspondant au titre de l’exo ici
  titreExo: 'Ma section exemple de base',
  // on donne les phrases de la consigne
  consigne1: 'Saisis le nombre £n.', // n est une variable définie comme propriété d’un objet lors de la constructiond e la consigne
  // On peut utiliser une variable contenant plusieurs caractères, mais il faut alors mettre des accolades : £{nb} par exemple
  consigne2: 'Ma réponse&nbsp;: @1@.',
  // les différents commentaires envisagés suivant les réponses fausses de l’élève
  comment1: 'Pas loin...',
  // et les phrases utiles pour les explications de la réponse
  corr1: 'Il fallait saisir £n.'
}

/**
 * La fonction principale, rappelée à chaque étape
 * Son this sera l’instance courante de Parcours
 * @this {Parcours}
 */
export default function maSection () {
  // suivant l’état courant on avise
  switch (this.etat) {
    case 'enonce':
      if (this.debutDeLaSection) {
        initSection(this)
        enonceStart(this)
      } else {
        enonceRestart(this)
      }
      // attention à bien appeler finEnonce() quand tout est fini (en général dans enonceEnd(me)), sinon on va rester coincé là
      // car le modèle l’attend avant de déploquer le passage à la suite
      break

    case 'correction':
      correction(this)
      break

    case 'navigation':
      navigation(this)
      break
  }
}

// ///////////////////////////////////////////////
// FONCTIONS Privées, déclarées une seule fois
// au chargement du fichier js
// ///////////////////////////////////////////////

// on décide arbitrairement de nommer "me" la variable qui correspond à l’instance courante de Parcours (objet this de la fct principale)

/**
 * Appelé au début de la section
 * @private
 * @param {Parcours} me
 */
function initSection (me) {
  const stor = me.storage
  // Construction de la page
  me.construitStructurePage(structure) // L’argument devient {structure:structure, ratioGauche:ratioGauche} si ratioGauche est défini comme variable globale
  me.afficheTitre(textes.titreExo)
  // on initialise un objet persistant pour garder une référence sur les éléments qu’on crée
  stor.elts = {}
  // et un autre pour nos listeners
  stor.listeners = {}
}

/**
 * Appelé au début de la section, après l’init
 * @private
 * @param {Parcours} me
 */
function enonceStart (me) {
  // on peut faire ici du chargement asynchrone

  // ou stocker des variables, par exemples celles qui serviraient pour la pe

  // puis énoncé
  enonceEnd(me)
}

/**
 * Appelé au case enonce dans le cas d’une répétition
 * @private
 * @param {Parcours} me
 */
function enonceRestart (me) {
  // le nettoyage au restart
  me.videLesZones()
  enonceEnd(me)
}
/**
 * Retourne les données de l’exercice
 * @private
 * @return {Object}
 */
function genereAlea () {
  /**
   * données de l’exercice qui peuvent être les variables présentes dans les textes (consignes ou corrections)
   * ou autre données utiles à la validation
   * @private
   * @type {Object} obj
   */
  const obj = {}
  obj.n = j3pGetRandomInt(30, 60)
  // Pourquoi nommer cette variable n ? C’est parce que dans l’énoncé (consigne1), apparait £n qui sera remplacé par la valeur ainsi déterminée
  return obj
}
/**
 * Appelé après enonceStart ou enonceRestart
 * @private
 * @param me
 */
function enonceEnd (me) {
  const stor = me.storage
  // on affiche l’énoncé
  // La style de base (ici petit.enonce) peut être complété par d’autres propriétés css. C’est fait ici avec j3pStyle
  stor.elts.divEnonce = j3pAddElt(me.zonesElts.MG, 'div', '', { style: me.styles.petit.enonce })
  j3pStyle(stor.elts.divEnonce, { padding: '10px' })
  // On génère les données de l’exo (stockées dans un objet dont on se sert entre autre pour les phrases de l’énoncé)
  stor.objVariables = genereAlea()
  // On écrit la première phrase (dans un bloc pour assurer un retour à la ligne)
  phraseBloc(stor.elts.divEnonce, textes.consigne1, stor.objVariables)
  const eltCons2 = afficheBloc(stor.elts.divEnonce, textes.consigne2, {
    input1: { texte: '', dynamique: true, width: '12px' }
  })
  stor.zoneInput = eltCons2.inputList[0] // Une seule zone à valider
  stor.zoneInput.typeReponse = ['nombre', 'exact']
  stor.zoneInput.reponse = [stor.objVariables.n]
  j3pRestriction(stor.zoneInput, /\d+/g)
  stor.fctsValid = new ValidationZones({
    zones: [stor.zoneInput],
    parcours: me
  })
  j3pFocus(stor.zoneInput)
  // On crée ici le div qui contiendra toutes les remarques sur ce qu’on pense de la réponse de l’élève (le plus souvent dans le cadre MD)
  // Il n’est ainsi construit qu’une seule fois et vidé à chaque passage dans correction()
  stor.zoneCorr = j3pAddElt(me.zonesElts.MD, 'div', '', { style: me.styles.moyen.correction })
  j3pStyle(stor.zoneCorr, { padding: '10px' })
  // et on appelle finEnonce()
  me.finEnonce()
}

function afficheCorrection (me, bonneReponse = false) {
  const stor = me.storage
  // const ds = me.donneesSection
  const zoneExpli = j3pAddElt(stor.elts.divEnonce, 'div', '', { style: me.styles.petit.correction })
  // La couleur de la correction sera fonction de la qualité de la réponse de l’élève
  j3pStyle(zoneExpli, { paddingTop: '10px', color: (bonneReponse) ? me.styles.cbien : me.styles.toutpetit.correction.color })
  phraseBloc(zoneExpli, textes.corr1, stor.objVariables)
}

/**
 * Vérifie la réponse (retourne null si pas répondu)
 * @param {Parcours} me
 * @return {number|null}
 */
function getScore (me) {
  // check de la réponse, on retourne
  // - null si pas répondu
  // - 1 si ok
  // - un nombre entre 0 et 1 si c’est pas complètement bon
  const stor = me.storage
  const reponse = stor.fctsValid.validationGlobale()
  if (reponse.aRepondu) {
    if (reponse.bonneReponse) return 1
    // s’il en est proche, je lui donne un demi-point
    return (Math.abs(stor.zoneInput.reponse[0] - stor.fctsValid.zones.reponseSaisie[0]) < 2) ? 0.5 : 0
    // Pour le score, voici ci-dessous ce qu’on aura le plus souvent (pas de demi-point ou autre)
    // return (reponse.bonneReponse) ? 1 : 0
  }
  return null
}

function correction (me) {
  const stor = me.storage
  const ds = me.donneesSection
  const score = getScore(me) // null si pas répondu
  if ((score === 1) || (me.essaiCourant >= ds.nbchances)) me.score += score
  // pas de réponse sans limite de temps
  if (score === null && !me.isElapsed) {
    // on arrête là (on reste donc dans l’état correction avec le bouton valider actif)
    me.reponseManquante(stor.zoneCorr)
    return me.finCorrection()
  }
  if (score !== 1) {
    // On écrit c’est faux
    me.reponseKo(stor.zoneCorr)
    if (score === 0.5) {
      j3pAddElt(stor.zoneCorr, 'br')
      me.reponseKo(stor.zoneCorr, textes.comment1, true)
    }
  }
  // le seul cas où on reste en correction, réponse fausse avec un autre essai
  if ((score !== 1) && me.essaiCourant < me.donneesSection.nbchances) {
    me.reponseKo(stor.zoneCorr, '<br>' + essaieEncore, true)
    return me.finCorrection()
  }

  // pour tout le reste on a terminé avec cette question, on affiche commentaire & correction, puis, au choix
  // 1) on choisit ici le prochain état, enonce ou navigation pour la fin de la section, et on met le bon bouton
  // 2) on affiche les commentaires et on passe d’office en navigation sans clic de l’élève, avec un sectionCourante('navigation'),
  //    et c’est là-bas que le choix se décide
  // 3) on a rien de spécial à afficher en fin de section, on fait comme pour 1) mais on active directement ici le bouton sectionSuivante()
  //    => on ne sera jamais appelé en méthode navigation
  // ci dessous l’option 2)
  if (score === 1) {
    // ça peut arriver aussi en temps limité (si le temps expire après la saisie mais avant que l’élève n’ait pu cliquer sur ok)
    // pour ça qu’on le teste en premier
    me.reponseOk(stor.zoneCorr)
    // Même dans le cas d’une bonne réponse, on peut afficher la correction qui aurait dans ce cas d’éventuels compléments d’information
    // afficheCorrection(me, true)
  } else {
    // ko, temps dépassé ou dernier essai
    if (me.isElapsed) {
      j3pAddElt(stor.zoneCorr, 'br')
      me.reponseKo(stor.zoneCorr, tempsDepasse, true)
    }
    afficheCorrection(me)
  }
  // passe l’état en navigation et nous rappelle
  me.finCorrection('navigation', true)
}

function navigation (me) {
  if (!me.sectionTerminee()) {
    me.etat = 'enonce'
  }
  me.finNavigation(true)
}
