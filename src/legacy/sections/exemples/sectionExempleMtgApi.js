import { j3pAddElt, j3pBoutonRadio, j3pDistance, j3pGetRandomElts, j3pGetRandomInt } from 'src/legacy/core/functions'
import { getMtgCore } from 'src/lib/outils/mathgraph'

/**
 * Mettre ici une description de la section
 * @author votre nom <votre mail>
 * @since date de création
 * @fileOverview
 */

/* Ce fichier est un exemple de section qui utilise l’api mathgraph */

export const params = {
  // ce qui est paramétrable
  /**
   * Ce qui est paramétrable dans l’éditeur de graphe
   * @type {LegacySectionParamsParametres}
   */
  parametres: [
    ['nbrepetitions', 3, 'entier', 'Nombre de répétitions de la section'],
    ['indication', '', 'string', 'Indication pour chaque répétition'],
    ['limite', 0, 'entier', 'Temps disponible par répétition, en secondes'],
    ['titre', 'Exemple d’usage de l’api mathgraph', 'string', 'le titre de l’exercice']
  ]
}

/**
 * section ExempleMtgApi
 * La fonction principale, rappelée à chaque étape
 * Son this sera l’instance courante de Parcours
 * @this {Parcours}
 */
export default function main () {
  const me = this
  const ds = this.donneesSection
  const stor = this.storage

  async function initSection () {
    try {
    // Construction de la page
      me.construitStructurePage('presentation1')
      me.afficheTitre(ds.titre)
      // on charge mathgraph
      stor.mtgApp = await getMtgCore({ withApiPromise: true })
      enonce()
    } catch (error) {
      me.notif(error)
    }
  }

  function enonce () {
    me.videLesZones()
    const lettres = j3pGetRandomElts(Array.from('ABCDEFGHIJKLMN'), 4)
    const [a, b, c, d] = lettres
    j3pAddElt(me.zonesElts.MG, 'p', `Voici les segments [${a}${b}] et [${c}${d}]`)
    // il faut lui mettre une hauteur identique à celle du svg, sinon mtg le passera en relatif et les div suivants se retrouveront sous le svg généré plus tard
    const mtg = j3pAddElt(me.zonesElts.MG, 'div', '', { style: { height: '500px' } })
    // il faut lui passer l’option purge car on a viré le svg sans lui dire (il référence donc toujours les objets créés précédemment)
    stor.mtgApp.setFig({ container: mtg, width: 500, height: 500, purge: true })
    /* Attention, au 2023-06-16 setFig n’est pas encore déployé, il faut remplacer la ligne précédente par
    const fig =
        'TWF0aEdyYXBoSmF2YTEuMAAAABM+TMzNAAJmcv###wEA#wEAAAAAAAAAAANaAAACOAAAAQEAAAAAAAAAAAAAAAj#####AAAAAQAKQ0NhbGNDb25zdAD#####AAJwaQAWMy4xNDE1OTI2NTM1ODk3OTMyMzg0Nv####8AAAABAApDQ29uc3RhbnRlQAkh+1RELRj#####AAAAAQAKQ1BvaW50QmFzZQD#####AAAAAAAOAAFVAMAkAAAAAAAAQBAAAAAAAAAAAAUAAEAsZmZmZmZmQCxmZmZmZmb#####AAAAAQAUQ0Ryb2l0ZURpcmVjdGlvbkZpeGUA#####wEAAAAAEAAAAQAAAAEAAAABAT#wAAAAAAAA#####wAAAAEAD0NQb2ludExpZURyb2l0ZQD#####AAAAAAAOAAFWAMAAAAAAAAAAQBAAAAAAAAAAAAUAAUA8ZmZmZmZmAAAAAv####8AAAABAAhDU2VnbWVudAD#####AAAAAAAQAAABAAAAAQAAAAEAAAAD#####wAAAAEAB0NNaWxpZXUA#####wEAAAAAEAAAAAAAAAAAAAAAQAgAAAAAAAAAAAUAAAAAAQAAAAP#####AAAAAgAMQ0NvbW1lbnRhaXJlAP####8AAAAAAAAAAAAAAAAAQBgAAAAAAAAAAAAAAAUMAAAAAAABAAAAAAAAAAEAAAAAAAAAAAABMf####8AAAABAAlDTG9uZ3VldXIA#####wAAAAEAAAADAAAAB###########'
    const svgId = j3pGetNewId('mtg')
    j3pCreeSVG(mtg, { id: svgId, width: 500, height: 500 })
    stor.mtgApp.removeAllDoc()
    stor.mtgApp.addDoc(svgId, fig)
    // faut dire à l’api qu’on a changé de doc, même si c’est le même id
    stor.mtgApp.setApiDoc(svgId)
    */
    // @todo virer le commentaire précédent quand setFig sera en prod
    // On affiche les points
    const points = lettres.map(l => ({
      x: j3pGetRandomInt(50, 450),
      y: j3pGetRandomInt(50, 450),
      name: l
    }))
    for (const point of points) {
      stor.mtgApp.addFreePoint(point)
    }
    // et les segments
    stor.mtgApp.addSegment({ a, b })
    stor.mtgApp.addSegment({ a: c, b: d })
    // on en a besoin en correction
    stor.lettres = lettres
    const divReponse = j3pAddElt(me.zonesElts.MG, 'div')
    // on passe par stor pour récupérer ces boutons en correction
    stor.btnOui = j3pBoutonRadio(divReponse, '', 'reponse', 'oui', `${a}${b} > ${c}${d}`)
    j3pAddElt(divReponse, 'br')
    stor.btnNon = j3pBoutonRadio(divReponse, '', 'reponse', 'non', `${a}${b} < ${c}${d}`)
    j3pAddElt(divReponse, 'p', '(bouger un point en cas de doute)')
    me.finEnonce()
  }

  /**
   * Récupère les positions actuelles des points et regarde si la réponse cochée est la bonne
   * @private
   */
  function correction () {
    if (!stor.btnOui.checked && !stor.btnNon.checked) {
      me.reponseManquante(me.zonesElts.MD)
      return me.finCorrection()
    }
    // on récupère la position actuelle des points, qui a pu changer c’est des points libres
    // const [{ x: xa, y: ya }, { x: xb, y: yb }, { x: xc, y: yc }, { x: xd, y: yd }] = stor.lettres.map(a => stor.mtgApp.getPointPosition({ absCoord: true, a }))
    const [a, b, c, d] = stor.lettres.map(a => stor.mtgApp.getPointPosition({ absCoord: true, a }))
    const longueur1 = j3pDistance(a, b)
    const longueur2 = j3pDistance(c, d)
    const isOk = longueur1 > longueur2 ? stor.btnOui.checked : stor.btnNon.checked
    if (isOk) {
      me.score++
      me.reponseOk(me.zonesElts.MD)
    } else {
      me.reponseKo(me.zonesElts.MD)
    }
    me.finCorrection('navigation', true)
  }

  // suivant l’état courant on avise
  switch (this.etat) {
    case 'enonce':
      if (this.debutDeLaSection) {
        initSection()
      } else {
        enonce()
      }
      break

    case 'correction':
      correction()
      break

    case 'navigation':
      if (!me.sectionTerminee()) me.etat = 'enonce'
      me.finNavigation()
      break
  }
}
