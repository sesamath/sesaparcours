import { j3pAddElt, j3pArrondi, j3pGetRandomElts, j3pDetruit, j3pFocus, j3pPaletteMathquill, j3pRestriction, j3pStyle } from 'src/legacy/core/functions'
import { mqRestriction } from 'src/lib/mathquill/functions'
import ValidationZones from 'src/legacy/outils/calculatrice/ValidationZones'
import { phraseBloc, afficheBloc } from 'src/legacy/core/functionsPhrase'
import textesGeneriques from 'src/lib/core/textes'

const { essaieEncore, tempsDepasse } = textesGeneriques

/**
 * Mettre ici une description de la section
 * @author Rémi DENIAUD
 * @since juin 2022
 * @fileOverview
 */

// nos constantes
const structure = 'presentation1'

/**
 * Les paramètres de la section
 */
export const params = {
  // seuls les outils qui ne peuvent pas être importés doivent être déclarés ici (pour que le loader les charge),
  // sinon c’est à nous de les importer au début
  outils: ['calculatrice', 'mathquill'],
  // ce qui est paramétrable
  /**
   * Ce qui est paramétrable dans l’éditeur de graphe
   * @type {LegacySectionParamsParametres}
   */
  parametres: [
    ['nbrepetitions', 3, 'entier', 'Nombre de répétitions de la section'],
    ['indication', '', 'string', 'Indication pour chaque répétition'],
    ['limite', 0, 'entier', 'Temps disponible par répétition, en secondes'],
    ['nbchances', 2, 'entier', 'Nombre d’essais par répétition']
  ],
  // la liste des pe qui peuvent être retournés par une section quantitative (liste vide pour une section quantitative car ce sera géré par score)
  pe: [
    { pe_1: 'C’est bien' },
    { pe_2: 'Revoir la configuration du parallélogramme' },
    { pe_3: 'Problème avec le calcul fractionnaire' },
    { pe_4: 'Insuffisant' }
  ]
}

const textes = {
  // le mieux est de mettre également le texte correspondant au titre de l’exo ici
  titreExo: 'Exemple avec plusieurs zones de validation',
  // on donne les phrases de la consigne
  consigne1: '£A£B£C£D est un parallélogramme.',
  consigne2: 'Alors ses #1# [@1@] et [@2@] se coupent en leur @3@.',
  consigne3: 'Écris $1+\\frac{2}{7}$ sous forme réduite&nbsp;: &1&.',
  diagonales: 'diagonales',
  cotes: 'côtés',
  milieu: 'milieu|millieu', // on peut avoir une tolérance sur l’orthographe (le 1er est la bonne orthographe)
  // les différents commentaires envisagés suivant les réponses fausses de l’élève
  erreurs: 'La réponse est en partie fausse&nbsp;!',
  comment1: 'Fais une figure en respectant l’ordre des sommets pour t’aider à répondre&nbsp;!',
  comment2: 'La fraction doit être irréductible&nbsp;!',
  comment3: 'Simplifie l’écriture de ta réponse&nbsp;!',
  // et les phrases utiles pour les explications de la réponse
  corr1: '[£A£C] et [£B£D] sont les diagonales du parallélogramme £A£B£C£D. De plus elles se coupent en leur milieu.',
  corr2: 'En mettant au même dénominateur, on obtient&nbsp;:'
}

/**
 * section exValidationZones
 *
 * La fonction principale, rappelée à chaque étape
 * Son this sera l’instance courante de Parcours
 * @this {Parcours}
 */
export default function main () {
  // suivant l’état courant on avise
  switch (this.etat) {
    case 'enonce':
      if (this.debutDeLaSection) {
        initSection(this)
        enonceStart(this)
      } else {
        enonceRestart(this)
      }
      // attention à bien appeler finEnonce() quand tout est fini, sinon on va rester coincé là
      // car le modèle l’attend avant de déploquer le passage à la suite
      break

    case 'correction':
      correction(this)
      break

    case 'navigation':
      navigation(this)
      break
  }
}

/**
 * Appelé au début de la section
 * @private
 * @param {Parcours} me
 */
function initSection (me) {
  const stor = me.storage
  // Construction de la page
  me.construitStructurePage(structure) // L’argument devient {structure:structure, ratioGauche:ratioGauche} si ratioGauche est défini comme variable globale
  me.afficheTitre(textes.titreExo)
  // on initialise un objet persistant pour garder une référence sur les éléments qu’on crée
  stor.elts = {}
  // et un autre pour nos listeners
  stor.listeners = {}
}

/**
 * Appelé au début de la section, après l’init
 * @private
 * @param {Parcours} me
 */
function enonceStart (me) {
  // on peut faire ici du chargement asynchrone

  // stockage de variables, par exemples celles qui vont servir pour la pe
  me.storage.nbFautesPara = 0
  me.storage.nbFautesFraction = 0
  enonceEnd(me)
}

/**
 * Appelé au case enonce dans le cas d’une répétition
 * @private
 * @param {Parcours} me
 */
function enonceRestart (me) {
  // le nettoyage au restart
  me.videLesZones()
  enonceEnd(me)
}

/**
 * Retourne les données de l’exercice
 * @private
 * @return {Object}
 */
function genereAlea () {
  /**
   * données de l’exercice qui peuvent être les variables présentes dans les textes (consignes ou corrections)
   * ou autre données utiles à la validation
   * @private
   * @type {Object} obj
   */
  const obj = {}
  // Je choisis les noms des quatre points au hasard parmi ceux ci-dessous
  const lettres = j3pGetRandomElts(Array.from('ABCDEFGHIJK'), 4)
  const nomsInit = 'ABCD' // A, B, C et D seront les propriétés de l’objet (pour remplacer £A, £B, £C et £D des consignes)
  Array.from(nomsInit).forEach((lettre, index) => { obj[lettre] = lettres[index] })
  // Je mets aussi les propriétés utiles pour le calcul fractionnaire
  obj.r = '\\frac{7}{7}+\\frac{2}{7}'
  obj.t = '\\frac{9}{7}'
  return obj
}
// on décide arbitrairement de nommer "me" la variable qui correspond à l’instance
// courante de Parcours (objet global j3p ou this de la fct principale)

/**
 * Appelé après enonceStart ou enonceRestart
 * @private
 * @param me
 */
function enonceEnd (me) {
  const stor = me.storage
  // on affiche l’énoncé
  // La style de base (ici petit.enonce) peut être complété par d’autres propriétés css. On peut aussi utiliser j3pStyle
  stor.elts.divEnonce = j3pAddElt(me.zonesElts.MG, 'div', '', { style: me.styles.petit.enonce })
  j3pStyle(stor.elts.divEnonce, { padding: '10px' })
  // On génère les données de l’exo
  stor.objVariables = genereAlea()
  // dans la première consigne, il y a 3 zones de saisie : input1, input2, input3, chacune ayant une taille dynamique, cad qu’elle évolue en fonction du contenu
  // ces zones input ne sont pas des zones mathquill.
  // Dans le texte cons1, elles correspondent à @1@, @2@ et @3@
  // Pour chacune d’elles, au début, elle sont vides, d’où texte="". La largeur de 12px est juste pour qu’elles soient un peu plus large au départ
  phraseBloc(stor.elts.divEnonce, textes.consigne1, stor.objVariables)
  const objVariablesZones = {
    ...stor.objVariables, // on récupère toutes les propriétés de cet objet
    // et on ajoute tout ça
    input1: { texte: '', dynamique: true, width: '12px', maxchars: 2 },
    input2: { texte: '', dynamique: true, width: '12px', maxchars: 2 },
    input3: { texte: '', dynamique: true, width: '12px' },
    liste1: { texte: ['', textes.diagonales, textes.cotes] }
  }

  // objVariablesZones a a lors récupéré le contenu de stor.objVariables auquel j’ai ajouté les propriétés ci-dessus
  const eltCons2 = afficheBloc(stor.elts.divEnonce, textes.consigne2, objVariablesZones)
  // Dans cette section, j’ai une deuxième phrase à compléter
  // cette fois-ci, je n’ai qu’une zone de saisie à compléter, c’est une zone mathquill : inputmq1
  const eltCons3 = afficheBloc(stor.elts.divEnonce, textes.consigne3, { inputmq1: { texte: '' } })
  // On stocke les zones de saisie (et liste) dans un array stor.zonesInput
  // Si on n’a qu’une seule zone, on utilisera plutôt stor.zoneInput égal à cette zone
  // C’est ce que je fais pour nommer stor.zoneSelect (que j’aurais pu mettre dans l’array précédent, c’est au goût de chacun)
  stor.zonesInput = [...eltCons2.inputList]
  stor.zonesInput.push(eltCons3.inputmqList[0])
  stor.zoneSelect = eltCons2.selectList[0]
  eltCons2.inputList.forEach((zone, index) => {
    // On aura du texte dans les 3 premières zones de saisie
    zone.typeReponse = ['texte']
    if (index === 2) {
      j3pRestriction(zone, j3pRestriction.lettresAccents)
    } else {
      j3pRestriction(zone, 'A-Za-z')
      // Dans les noms des segments, j’autorise les minuscules, mais je les convertis en majuscule lors de la saisie
      zone.addEventListener('input', (e) => { e.target.value = e.target.value.toUpperCase() })
    }
  })
  stor.zoneSelect.reponse = [1] // position de la bonne réponse de la liste déroulante
  // Pour la troisième zone, la réponse est "milieu", on la met donc en position [0] (c’est géré dans textes.milieu)
  // On peut avoir une certaine tolérance sur l’orthographe, d’où l’acceptation de "millieu" qui sera accepté puis modifié à la validation
  stor.zonesInput[2].reponse = textes.milieu.split('|')
  // Dans la quatrième zone de saisie, la valeur attendue est un nombre dont on attend la valeur exacte
  stor.zonesInput[3].typeReponse = ['nombre', 'exact']
  // Pour ce nombre, on peut être plus ou moins exigent sur le résultat donné par l’élève :
  // Dans certains cas, on voudra que l’élève simplifie au maximum le nombre. D’autre fois, il pourra laisser un calcul réduit mais dont une fraction ne serait pas simplifiée
  // Pour ces simplification, on peut soit compter faux s’il ne l’a pas fait, soit considérer la réponse comme non valide et lui redonner la main pour qu’il simplifie.
  // Tout ceci est géré pas la propriété solutionSimplifiee
  stor.zonesInput[3].solutionSimplifiee = ['reponse fausse', 'non valide']// on peut aussi mettre "non valide" ou "reponse fausse"
  // On donne également la réponse attendue dans un array qui ne contient qu’un seul élément
  stor.zonesInput[3].reponse = [9 / 7]
  // Pour cette dernière zone qui est une zone mathquill, on utilise mqRestriction qui va créer la clavier virtuel
  // On ajoute également l’outil 'fraction' (dans le clavier virtuel, mais aussi comme palette dans l’exo)
  mqRestriction(stor.zonesInput[3], '\\d.,-/+', { commandes: ['fraction'] })
  stor.elts.laPalette = j3pAddElt(stor.elts.divEnonce, 'div')
  j3pStyle(stor.elts.laPalette, { paddingTop: '10px' })
  j3pPaletteMathquill(stor.elts.laPalette, stor.zonesInput[3], { liste: ['fraction'] })
  // On définit les zones de saisie (ou select) validées par ValidationZones
  // ValidationZones gère aussi zones comme éléments HTML que leurs ids
  // stor.fctsValid est un objet permettant la validation. Il contient le nom des zones (array zonesSaisie) et un deuxième paramètre validePerso
  // validePerso est souvent un array vide
  // Mais parfois certaines zones ne peuvent pas être validée de manière automatique car comme ici les inputs 1 et 2 peuvent aussi bien accueillir les noms des segment [AC] et [BD] que [BD] et [AC]
  // Donc la validation de ces zones devra se faire "à la main". On vérifiera si elles sont remplies ou non.
  const zonesSaisie = [stor.zoneSelect].concat(stor.zonesInput)
  // L’ordre des zones a tout de même de l’importance pour vérifier si elles ont été complétées et dans la validation.
  // En effet, le focus sera redonné à la première zone posant pb
  stor.fctsValid = new ValidationZones({
    zones: zonesSaisie,
    validePerso: zonesSaisie.slice(1, 3),
    parcours: me
  })
  // Je donne le focus à la première zone à compléter (la liste déroulante dans notre cas de figure)
  j3pFocus(stor.zoneSelect)
  // On crée ici le div qui contiendra toutes les remarques sur ce qu’on pense de la réponse de l’élève (le plus souvent dans le cadre MD)
  // Il n’est ainsi construit qu’une seule fois et vidé à chaque passage dans correction()
  stor.zoneCorr = j3pAddElt(me.zonesElts.MD, 'div', '', { style: me.styles.moyen.correction })
  j3pStyle(stor.zoneCorr, { padding: '10px' })
  // et on appelle finEnonce()
  me.finEnonce()
}

function afficheCorrection (me, bonneReponse = false) {
  // on affiche la correction
  const stor = me.storage
  j3pDetruit(stor.elts.laPalette)
  const zoneExpli = j3pAddElt(stor.elts.divEnonce, 'div', '', { style: me.styles.petit.correction })
  // La couleur de la correction sera fonction de la qualité de la réponse de l’élève
  j3pStyle(zoneExpli, { paddingTop: '10px', color: (bonneReponse) ? me.styles.cbien : me.styles.toutpetit.correction.color })
  ;[1, 2].forEach(i => {
    phraseBloc(zoneExpli, textes['corr' + i], stor.objVariables)
  })
  afficheBloc(zoneExpli, '$1+\\frac{2}{7}=£r=£t$.', stor.objVariables)
}

/**
 * Vérifie la réponse (retourne null si pas répondu)
 * @param {Parcours} me
 * @return {number|null}
 */
function getScore (me) {
  // check de la réponse, on retourne
  // - null si pas répondu
  // - 1 si ok
  // - un nombre entre 0 et 1 si c’est pas complètement bon
  const stor = me.storage
  const reponse = stor.fctsValid.validationGlobale()
  // A cet instant reponse contient deux éléments :
  // reponse.aRepondu qui me dit si la liste et les 4 zones ont bien été complétées ou non
  // reponse.bonneReponse qui me dit si les zones qui ne sont pas dans le tableau validePerso ont des réponses correctes ou non.
  // Si reponse.aRepondu vaut true, la fonction met aussi en bleu ou rouge les réponses bonnes ou fausses.
  // Si c’est la dernière tentative de répondre, on barre également les zones fausses
  if (reponse.aRepondu) {
    // il faut aussi que je teste les deux zones dont la validation est perso
    let bonneRep2 = false
    const tabRepPossibles = [
      [[stor.objVariables.A + stor.objVariables.C, stor.objVariables.C + stor.objVariables.A], [stor.objVariables.B + stor.objVariables.D, stor.objVariables.D + stor.objVariables.B]],
      [[stor.objVariables.B + stor.objVariables.D, stor.objVariables.D + stor.objVariables.B], [stor.objVariables.A + stor.objVariables.C, stor.objVariables.C + stor.objVariables.A]]
    ]
    tabRepPossibles.forEach(tab => {
      bonneRep2 = bonneRep2 || (stor.fctsValid.valideUneZone(stor.zonesInput[0], tab[0]).bonneReponse && stor.fctsValid.valideUneZone(stor.zonesInput[1], tab[1]).bonneReponse)
    })
    reponse.bonneReponse = (reponse.bonneReponse && bonneRep2)
    // bonneRep2 ne prend en compte que les zones dans validePerso
    if (!bonneRep2) {
      stor.fctsValid.zones.bonneReponse[1] = false
      stor.fctsValid.zones.bonneReponse[2] = false
      // si les 2 zones de validePerso ne sont pas bonnes, on met false dans stor.fctsValid.zones.bonneReponse[i] où i est la position de la zone dans le tableau zonesSaisie déclaré lors de la construction de stor.fctsValid
    }
    // on appelle alors la fonction qui va mettre en couleur les réponses bonnes ou fausses de ces 2 zones
    stor.fctsValid.coloreUneZone(stor.zonesInput[0])
    stor.fctsValid.coloreUneZone(stor.zonesInput[1])
    // Pour le score, ci-dessous ce qu’on aura le plus souvent :
    // return (reponse.bonneReponse) ? 1 : 0
    // Mais on peut très bien donner des points (score restant inférieur ou égal à 1) si une partie de la réponse est bonne
    // Par exemple, 0.8 pour la première phrase et 0.2 pour le calcul fractionnaire
    // Je stocke erreurLigne1 pour l’utiliser dans un commentaire dans la correction
    stor.erreurLigne1 = stor.fctsValid.zones.bonneReponse.slice(0, stor.fctsValid.zones.bonneReponse.length - 1).includes(false)
    // Si la seule faute est dans la liste déroulante, je vais lui donner le score de 0.9 puis dans correction, je ne lui laisserai pas la possibilité de modifier
    if (!stor.fctsValid.zones.bonneReponse[0] && !stor.fctsValid.zones.bonneReponse.slice(1).includes(false)) return 0.9
    stor.score = (stor.erreurLigne1) ? 0 : 0.8
    stor.score += (stor.fctsValid.zones.bonneReponse[stor.fctsValid.zones.bonneReponse.length - 1]) ? 0.2 : 0
    return j3pArrondi(stor.score, 2) // attention à la gestion des flottants
  }
  return null
}

function correction (me) {
  const stor = me.storage
  const ds = me.donneesSection
  const score = getScore(me) // null si pas répondu
  if ((score === 1) || (me.essaiCourant >= ds.nbchances)) me.score += score
  // pas de réponse sans limite de temps
  if (score === null && !me.isElapsed) {
    // on arrête là (on reste donc dans l’état correction avec le bouton valider actif)
    let msgReponseManquante
    if (!stor.fctsValid.zones.reponseSimplifiee[4][1]) {
      // c’est que la fraction de la dernière zone de saisie est réductible
      msgReponseManquante = textes.comment2
    }
    me.reponseManquante(stor.zoneCorr, msgReponseManquante)
    return
  }
  // ici, je peux gérer le cas où seule la liste déroulante est fausse et auquel cas, je l’empêche de faire une modification
  if (score === 0.9 && me.essaiCourant < ds.nbchances) {
    me.essaiCourant = ds.nbchances
    me.score += score
    // On barre alors la liste et on la rend inactive
    stor.fctsValid.coloreUneZone(stor.zoneSelect)
  }
  if (score !== 1) {
    // On écrit c’est faux
    // Cas particulier ici où on modifie le commentaire suivant que la réponse est totalement ou partiellement fausse
    if (score === 0) me.reponseKo(stor.zoneCorr)
    else me.reponseKo(stor.zoneCorr, textes.erreurs)
    // On peut ajouter des commentaires expliquant la ou les erreur(s) commise(s)
    if (stor.erreurLigne1) {
      j3pAddElt(stor.zoneCorr, 'br')
      me.reponseKo(stor.zoneCorr, textes.comment1, true)
    }
  }
  // le seul cas où on reste en correction, réponse fausse avec un autre essai
  // score pouvant être un nombre entre 0 et 1, !score n’est pas équivalent à (score !== 1)
  if ((score !== 1) && me.essaiCourant < ds.nbchances) {
    me.reponseKo(stor.zoneCorr, '<br>' + essaieEncore, true)
    return
  }

  // pour tout le reste on a terminé avec cette question, on affiche commentaire & correction, puis, au choix
  // 1) on choisit ici le prochain état, enonce ou navigation pour la fin de la section, et on met le bon bouton
  // 2) on affiche les commentaires et on passe d’office en navigation sans clic de l’élève, avec un sectionCourante('navigation'),
  //    et c’est là-bas que le choix se décide
  // 3) on a rien de spécial à afficher en fin de section, on fait comme pour 1) mais on active directement ici le bouton sectionSuivante()
  //    => on ne sera jamais appelé en méthode navigation
  // ci dessous l’option 2)
  if (score === 1) {
    // ça peut arriver aussi en temps limité (si le temps expire après la saisie mais avant que l’élève n’ait pu cliquer sur ok)
    // pour ça qu’on le teste en premier
    me.reponseOk(stor.zoneCorr)
    // Même dans le cas d’une bonne réponse, on peut afficher la correction qui aurait dans ce cas d’éventuels compléments d’information
    afficheCorrection(me, true)
  } else {
    // ko, temps dépassé ou dernier essai
    if (me.isElapsed) {
      j3pAddElt(stor.zoneCorr, 'br')
      me.reponseKo(stor.zoneCorr, tempsDepasse, true)
    }
    // pour la pe, je fais des tests sur les problèmes éventuels
    if (stor.erreurLigne1) stor.nbFautesPara++
    if (!stor.fctsValid.zones.bonneReponse[stor.fctsValid.zones.bonneReponse.length - 1]) stor.nbFautesFraction++
    afficheCorrection(me)
  }
  // passe l’état en navigation et nous rappelle
  me.finCorrection('navigation', true)
}

function navigation (me) {
  const ds = me.donneesSection
  const stor = me.storage
  if (me.sectionTerminee()) {
    // fin de section, on calcule la pe retournée
    if (me.score >= 0.7) {
      me.pe = ds.pe_1
    } else if (stor.nbFautesPara / ds.nbitems >= 0.7) {
      me.pe = ds.pe_2
    } else if (stor.nbFautesFraction / ds.nbitems >= 0.7) {
      me.pe = ds.pe_3
    } else {
      me.pe = ds.pe_4
    }
  } else {
    me.etat = 'enonce'
  }
  // Ici on a rien à afficher de particulier après la dernière répétition, donc on passe true à finNavigation
  // pour que le modèle affiche directement le bouton "section suivante" (sans devoir cliquer sur 'suite' avant)
  me.finNavigation(true)
}
