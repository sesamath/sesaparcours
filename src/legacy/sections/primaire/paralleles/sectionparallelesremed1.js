import { j3pAddElt, j3pEmpty, j3pGetBornesIntervalle, j3pGetNewId, j3pGetRandomInt, j3pIntersection, j3pStyle } from 'src/legacy/core/functions'
import { j3pDistancepointdroite, j3pEquation } from 'src/legacy/outils/geometrie/functions'
import Repere from 'src/legacy/outils/geometrie/Repere'
import textesGeneriques from 'src/lib/core/textes'

const { cBien, cFaux, essaieEncore, tempsDepasse } = textesGeneriques

/*
    JP Vanroyen
    2012
*/

export const params = {
  outils: ['calculatrice'],

  /**
   * Ce qui est paramétrable dans l’éditeur de graphe
   * @type {LegacySectionParamsParametres}
   */
  parametres: [
    ['nbrepetitions', 2, 'entier', 'aucun'],
    ['indication', '', 'string', 'aucun'],
    ['limite', 0, 'entier', 'Temps disponible par répétition, en secondes'],
    ['inclinaison', '[15;40]', 'string', 'Angle de la droite avec l’horizontale']
  ]
}

/**
 * section parallelesremed1
 * @this {Parcours}
 */
export default function main () {
  const me = this
  let ds = me.donneesSection
  const stor = this.storage

  function getDonnees () {
    return {
      typesection: 'primaire',
      nbrepetitions: 2,
      nbetapes: 1,
      unparametre: 0,
      structure: 'presentation1',
      inclinaison: '[15;40]',
      nbchances: 1,
      indication: '',

      seuilbien: 0.75, // 75% pour la pe "bien" renvoyée
      seuilprobleme: 0.5, // plus de 50% d’erreur après seconde chance
      pe_1: 'bien',
      pe_2: 'moyen', // autres cas
      pe_3: 'probleme',
      textes: {
        titre_exo: 'Parallélisme',
        cons1: 'Les droites bleues et rouges sont parallèles',
        cons2: 'Déplace le point C afin que la droite bleue passe par le point P.',
        comment1: 'La droite bleue devait être confondue avec la droite noire&nbsp;!'
      }
    }
  }
  function enonceMain () {
    let fini = false
    const xA = -3
    const xB = xA + 1
    let yA, yB, xC, yC, xD, yD
    while (!fini) {
      yA = j3pGetRandomInt(-5, 5)
      const [intMin, intMax] = j3pGetBornesIntervalle(ds.inclinaison)
      const inclinaison = j3pGetRandomInt(intMin, intMax)
      yB = yA + Math.tan((inclinaison / 180) * Math.PI)

      xC = j3pGetRandomInt(-6, 6)
      yC = j3pGetRandomInt(-6, 6)

      xD = j3pGetRandomInt(-7, 7)
      while (xD === xC)xD = j3pGetRandomInt(-7, 7)

      yD = j3pGetRandomInt(-5, 5)

      if (me.questionCourante <= 2) {
        const tab = j3pIntersection(j3pEquation(xA, yA, xB, yB), j3pEquation(xC, yC, xD, yD))
        const coef = (tab[0] - xD) / (xC - xD)
        if ((j3pDistancepointdroite(xC, yC, j3pEquation(xA, yA, xB, yB)) > 3) &&
          (j3pDistancepointdroite(xD, yD, j3pEquation(xA, yA, xB, yB)) > 3) &&
          (Math.abs((j3pDistancepointdroite(xD, yD, j3pEquation(xA, yA, xB, yB)) - j3pDistancepointdroite(xC, yC, j3pEquation(xA, yA, xB, yB)))) > 1) &&
          (Math.abs(coef) < 1)
        ) { fini = true }
      } else {
        if ((j3pDistancepointdroite(xC, yC, j3pEquation(xA, yA, xB, yB)) > 3) &&
          (j3pDistancepointdroite(xD, yD, j3pEquation(xA, yA, xB, yB)) > 3) &&
          (Math.abs((j3pDistancepointdroite(xD, yD, j3pEquation(xA, yA, xB, yB)) - j3pDistancepointdroite(xC, yC, j3pEquation(xA, yA, xB, yB)))) > 1)
        ) { fini = true }
      }
    }

    stor.unRepere = j3pGetNewId('unrepere')
    stor.conteneur = j3pAddElt(me.zonesElts.MG, 'div', '', { style: me.styles.etendre('petit.enonce', { padding: '15px' }) })
    stor.repere = new Repere({
      idConteneur: stor.conteneur,
      idDivRepere: stor.unRepere,
      visible: false,
      aimantage: false,
      larg: 500,
      haut: 400,

      pasdunegraduationX: 1,
      pixelspargraduationX: 25,
      pasdunegraduationY: 1,
      pixelspargraduationY: 25,
      xO: 250,
      yO: 250,
      debuty: 0,
      negatifs: true,
      objets: [
        { type: 'point', nom: 'A', par1: xA, par2: yA, fixe: true, visible: false, etiquette: false, style: { couleur: '#00F', epaisseur: 2, taillepoint: 6 } },
        { type: 'point', nom: 'B', par1: xB, par2: yB, fixe: true, visible: false, etiquette: false, style: { couleur: '#00F', epaisseur: 2, taillepoint: 6 } },
        { type: 'droite', nom: 'd1', par1: 'A', par2: 'B', style: { couleur: '#F00', epaisseur: 2 } },
        { type: 'point', nom: 'C', par1: xC, par2: yC, fixe: false, visible: true, etiquette: true, style: { couleur: '#00F', epaisseur: 2, taillepoint: 6 } },
        { type: 'droiteparallele', nom: 'd2', par1: 'C', par2: 'd1', style: { couleur: '#00F', epaisseur: 2 } },
        { type: 'point', nom: 'P', par1: xD, par2: yD, fixe: true, visible: true, etiquette: true, style: { couleur: '#000', epaisseur: 3, taillepoint: 6 } }
      ]
    })
    stor.repere.construit()
    stor.conteneurD = j3pAddElt(me.zonesElts.MD, 'div', '', { style: me.styles.etendre('petit.enonce', { padding: '10px' }) })

    ;['1', '2'].forEach(i => { stor['zoneCons' + i] = j3pAddElt(stor.conteneurD, 'div') })
    j3pAddElt(stor.zoneCons1, 'div', ds.textes.cons1)
    j3pStyle(stor.zoneCons1, { fontWeight: 'bold', color: '#00F', paddingBottom: '30px' })
    j3pAddElt(stor.zoneCons2, 'div', ds.textes.cons2)
    stor.zoneCorr = j3pAddElt(me.zonesElts.MD, 'div', '', { style: me.styles.etendre('petit.correction', { padding: '10px' }) })

    me.finEnonce()
  }
  switch (me.etat) {
    case 'enonce':
      if (me.debutDeLaSection) {
        me.donneesSection = getDonnees()
        ds = me.donneesSection
        me.surcharge()
        me.validOnEnter = false // ex donneesSection.touche_entree
        me.construitStructurePage({ structure: ds.structure, ratioGauche: 0.6 })

        me.typederreurs = [0, 0, 0, 0, 0, 0]
        me.score = 0

        me.afficheTitre(ds.textes.titre_exo, true)

        if (ds.indication) me.indication(me.zonesElts.IG, ds.indication)
      } else {
        me.videLesZones()
      }
      enonceMain()
      break // case enonce

    case 'correction': {
      stor.zoneCorr.innerHTML = ''
      let cbon = false
      if (stor.repere.distancepointdroite('P', 'd2') < 0.1) cbon = true

      if (cbon) {
        me.score++
        stor.zoneCorr.style.color = me.styles.cbien
        stor.zoneCorr.innerHTML = cBien
        me.typederreurs[0]++
        stor.repere.getObjet('C').fixe = true
        return this.finCorrection('navigation', true)
      }

      stor.zoneCorr.style.color = me.styles.cfaux

      if (me.isElapsed) { // limite de temps
        stor.zoneCorr.innerHTML = tempsDepasse
        me.typederreurs[10]++
        return this.finCorrection('navigation', true)
      }

      stor.zoneCorr.innerHTML = cFaux

      if (me.essaiCourant < ds.nbchances) {
        stor.zoneCorr.innerHTML += '<br>' + essaieEncore
        document.getElementById(stor.unRepere).innerHTML = ''
        stor.repere.add({
          type: 'droiteparallele',
          nom: 'dP',
          par1: 'P',
          par2: 'd1',
          style: { couleur: '#000', epaisseur: 2 }
        })
        stor.repere.construit()
        stor.repere.getObjet('C').fixe = true
        me.typederreurs[1]++
        return this.finCorrection() // on reste en correction
      }

      stor.zoneCorr.innerHTML += '<br>' + ds.textes.comment1
      j3pEmpty(stor.unRepere)
      stor.repere.add({
        type: 'droiteparallele',
        nom: 'dP',
        par1: 'P',
        par2: 'd1',
        style: { couleur: '#000', epaisseur: 2 }
      })
      stor.repere.construit()
      stor.repere.getObjet('C').fixe = true
      me.typederreurs[2]++

      this.finCorrection('navigation', true)
      break // case "correction":
    }

    case 'navigation':
      if (!me.sectionTerminee()) {
        me.etat = 'enonce'
      }
      me.finNavigation(true)
      break // case "navigation":
  }
}
