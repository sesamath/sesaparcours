import { j3pAddElt, j3pGetBornesIntervalle, j3pGetNewId, j3pGetRandomInt, j3pStyle } from 'src/legacy/core/functions'
import { j3pAngledroites, j3pDistancepointdroite, j3pEquation } from 'src/legacy/outils/geometrie/functions'
import Repere from 'src/legacy/outils/geometrie/Repere'
import textesGeneriques from 'src/lib/core/textes'

const { cBien, cFaux, essaieEncore, regardeCorrection, tempsDepasse } = textesGeneriques

/*
    JP Vanroyen
    2012
*/

export const params = {
  outils: ['calculatrice'],
  /**
   * Ce qui est paramétrable dans l’éditeur de graphe
   * @type {LegacySectionParamsParametres}
   */
  parametres: [
    ['nbrepetitions', 3, 'entier', 'aucun'],
    ['indication', '', 'string', 'aucun'],
    ['limite', 0, 'entier', 'Temps disponible par répétition, en secondes'],
    ['seuilbien', 0.75, 'number', 'pour avoir la pe "bien" renvoyée'],
    ['seuilprobleme', 45, 'entier', 'si moyenne des erreurs >seuilprobleme pe "probleme" renvoyée'],
    ['tolerance', 5, 'number', 'Erreur maximale tolérée'],
    ['inclinaison', '[15;40]', 'string', 'Angles de la droite avec l’horizontale']
  ],
  pe: [
    { pe_1: 'bien' },
    { pe_2: 'moyen' },
    { pe_3: 'probleme' }
  ]
}

/**
 * section paralleles
 * @this {Parcours}
 */
export default function main () {
  const me = this
  let ds = me.donneesSection
  const stor = this.storage

  function getDonnees () {
    return {
      typesection: 'primaire',
      nbrepetitions: 5,
      nbetapes: 1,
      unparametre: 0,
      structure: 'presentation1',
      indication: '',
      inclinaison: '[15;40]',
      nbchances: 1,
      tolerance: 5,
      seuilbien: 0.75, // 75% pour la pe "bien" renvoyée
      seuilprobleme: 45, // moyenne des erreurs > 45°
      //
      pe_1: 'bien',
      pe_2: 'moyen',
      pe_3: 'probleme',
      textes: {
        titre_exo: 'Parallélisme',
        consigne1: 'Déplace le point C afin que les droites te semblent "presque" parallèles'
      }
    }
  }
  function enonceMain () {
    let fini = false
    const xA = -3
    const xB = xA + 1
    let yA, yB, xC, yC, xD, yD, inclinaison
    while (!fini) {
      yA = j3pGetRandomInt(-5, 5)
      const [intMin, intMax] = j3pGetBornesIntervalle(ds.inclinaison)
      inclinaison = j3pGetRandomInt(intMin, intMax)
      yB = yA + Math.tan((inclinaison / 180) * Math.PI)

      xC = j3pGetRandomInt(-7, 7)
      yC = j3pGetRandomInt(-7, 7)
      xD = j3pGetRandomInt(-7, 7)
      yD = j3pGetRandomInt(-7, 7)
      if (
        (j3pDistancepointdroite(xC, yC, j3pEquation(xA, yA, xB, yB)) > 3) &&
        (j3pAngledroites(j3pEquation(xA, yA, xB, yB), j3pEquation(xC, yC, xD, yD)) > 15)
      ) {
        fini = true
      }
    }

    me.stockage[1] = [xC + 1, yC + Math.tan((inclinaison / 180) * Math.PI)]

    me.cacheBoutonSuite()
    stor.idRepere = j3pGetNewId('unrepere')
    stor.repere = new Repere({
      idConteneur: me.zonesElts.MG,
      idDivRepere: stor.idRepere,
      visible: false,
      aimantage: false,
      larg: 500,
      haut: 400,

      pasdunegraduationX: 1,
      pixelspargraduationX: 25,
      pasdunegraduationY: 1,
      pixelspargraduationY: 25,
      xO: 250,
      yO: 250,
      debuty: 0,
      negatifs: true,
      objets: [
        { type: 'point', nom: 'A', par1: xA, par2: yA, fixe: true, visible: false, etiquette: false, style: { couleur: '#00F', epaisseur: 2, taillepoint: 6 } },
        { type: 'point', nom: 'B', par1: xB, par2: yB, fixe: true, visible: false, etiquette: false, style: { couleur: '#00F', epaisseur: 2, taillepoint: 6 } },
        { type: 'droite', nom: 'd1', par1: 'A', par2: 'B', style: { couleur: '#F00', epaisseur: 2 } },
        { type: 'point', nom: 'C', par1: xC, par2: yC, fixe: false, visible: true, etiquette: true, style: { couleur: '#00F', epaisseur: 2, taillepoint: 6 } },
        { type: 'point', nom: 'D', par1: xD, par2: yD, fixe: true, visible: false, etiquette: false, style: { couleur: '#00F', epaisseur: 2, taillepoint: 6 } },
        { type: 'droite', nom: 'd2', par1: 'C', par2: 'D', style: { couleur: '#00F', epaisseur: 2 } }
      ]
    })
    stor.repere.construit()
    const ch = ds.textes.consigne1
    stor.conteneur = j3pAddElt(me.zonesElts.MD, 'div', '', { style: me.styles.etendre('moyen.enonce', { padding: '10px' }) })
    j3pAddElt(stor.conteneur, 'div', ch)
    stor.zoneCorr = j3pAddElt(stor.conteneur, 'div')
    j3pStyle(stor.zoneCorr, { paddingTop: '15px' })

    me.finEnonce()
  }
  switch (me.etat) {
    case 'enonce':
      if (me.debutDeLaSection) {
        me.donneesSection = getDonnees()
        ds = me.donneesSection
        me.surcharge()
        me.validOnEnter = false // ex donneesSection.touche_entree
        me.construitStructurePage({ structure: ds.structure, ratioGauche: 0.6 })

        me.stockage = [0, 0, 0, 0]
        me.typederreurs = [0, 0, 0, 0, 0, 0]
        me.score = 0

        me.afficheTitre(ds.textes.titre_exo)

        if (ds.indication) me.indication(me.zonesElts.IG, ds.indication)
      } else {
        me.videLesZones()
      }

      enonceMain()
      break // case enonce

    case 'correction':
      {
        me.cacheBoutonValider()
        stor.zoneCorr.innerHTML = ''
        let cbon = false
        if (j3pAngledroites(j3pEquation(stor.repere.abs('A'), stor.repere.ord('A'), stor.repere.abs('B'), stor.repere.ord('B')), j3pEquation(stor.repere.abs('C'), stor.repere.ord('C'), stor.repere.abs('D'), stor.repere.ord('D'))) < ds.tolerance) cbon = true

        if (cbon) {
          me.score++
          stor.zoneCorr.style.color = me.styles.cbien
          stor.zoneCorr.innerHTML = cBien
          me.typederreurs[0]++
          stor.repere.getObjet('C').fixe = true
          me.etat = 'navigation'
          me.sectionCourante()
        } else {
          stor.zoneCorr.style.color = me.styles.cfaux

          if (me.isElapsed) { // limite de temps
            stor.zoneCorr.innerHTML = tempsDepasse
            me.typederreurs[10]++
            me.typederreurs[3] += j3pAngledroites(j3pEquation(stor.repere.abs('A'), stor.repere.ord('A'), stor.repere.abs('B'), stor.repere.ord('B')), j3pEquation(stor.repere.abs('C'), stor.repere.ord('C'), stor.repere.abs('D'), stor.repere.ord('D')))
            // console.log("me.typederreurs[3]="+me.typederreurs[3])
            document.getElementById(stor.idRepere).innerHTML = ''
            stor.repere.add({
              type: 'droiteparallele',
              nom: 'dC',
              par1: 'C',
              par2: 'd1',
              style: { couleur: '#000', epaisseur: 2 }
            })
            stor.repere.construit()
            stor.repere.getObjet('C').fixe = true
            me.etat = 'navigation'
            me.sectionCourante()
          } else {
            stor.zoneCorr.innerHTML = cFaux

            if (me.essaiCourant < ds.nbchances) {
              stor.zoneCorr.innerHTML += '<br>' + essaieEncore
              me.typederreurs[1]++
              me.etat = 'correction'
              me.afficheBoutonValider()
            } else {
              stor.zoneCorr.innerHTML += '<br>' + regardeCorrection

              me.typederreurs[2]++
              me.typederreurs[3] += j3pAngledroites(j3pEquation(stor.repere.abs('A'), stor.repere.ord('A'), stor.repere.abs('B'), stor.repere.ord('B')), j3pEquation(stor.repere.abs('C'), stor.repere.ord('C'), stor.repere.abs('D'), stor.repere.ord('D')))
              document.getElementById(stor.idRepere).innerHTML = ''
              stor.repere.add({
                type: 'droiteparallele',
                nom: 'dC',
                par1: 'C',
                par2: 'd1',
                style: { couleur: '#000', epaisseur: 2 }
              })
              stor.repere.construit()
              stor.repere.getObjet('C').fixe = true
              me.etat = 'navigation'
              me.sectionCourante()
            }
          }
        }
      }
      break // case "correction":

    case 'navigation':
      if (me.sectionTerminee()) {
        if (me.score / ds.nbitems >= ds.seuilbien) {
          me.parcours.pe = ds.pe_1
        } else if (me.typederreurs[3] / ds.nbitems > ds.seuilprobleme) {
          me.parcours.pe = ds.pe_3
        } else { me.parcours.pe = ds.pe_2 }
      } else {
        me.etat = 'enonce'
      }
      this.finNavigation()
      break // case "navigation":
  }
}
