import { j3pAddContent, j3pAddElt, j3pChaine, j3pElement, j3pEmpty, j3pGetNewId, j3pGetRandomInt, j3pShuffle } from 'src/legacy/core/functions'
import { j3pAngledroites, j3pDistancepointdroite, j3pEquation } from 'src/legacy/outils/geometrie/functions'
import Repere from 'src/legacy/outils/geometrie/Repere'
import textesGeneriques from 'src/lib/core/textes'

const { cBien, essaieEncore, tempsDepasse } = textesGeneriques

/*
    JP Vanroyen
    2012
*/

const couleurs = {
  bleue: '#3366FF',
  orange: '#FF6633',
  verte: '#339933',
  marron: '#664D33',
  rouge: '#FF0000',
  violette: '#8000FF',
  rose: '#FF33CC',
  'bleu foncé': '#2E2E8A'
}
const nbMax = Object.keys(couleurs).length

export const params = {
  /**
   * Ce qui est paramétrable dans l’éditeur de graphe
   * @type {LegacySectionParamsParametres}
   */
  parametres: [
    ['nbrepetitions', 5, 'entier', 'aucun'],
    ['indication', '', 'string', 'aucun'],
    ['limite', 0, 'entier', 'Temps disponible par répétition, en secondes'],
    ['nbdroites', nbMax, 'entier', `Nombre de droites (${nbMax} max)`],
    ['inclinaison', '[-90;90]', 'string', 'Angles des droites avec l’horizontale']
  ]
}

const textes = {
  titre_exo: 'Droites visiblement parallèles',
  cons1: 'la droite ..... et la droite .....',
  cons2: 'Clique sur les deux droites qui te semblent parallèles.',
  comment1: 'Il faut sélectionner deux droites&nbsp;!',
  comment2: 'Il faut sélectionner deux droites distinctes&nbsp;!',
  comment3: 'C’est faux !<br>Les deux droites se coupent en T.<br><br><i>Si le point T n’apparaît pas, cherche-le en déplaçant la figure à l’aide de la souris</i>',
  comment4: "la droite <span style='color:£{c1}'>£{c2}</span> et la droite <span style='color:£{c3}'>£{c4}</span>"
}

/**
 * section parallelesfaisceau
 * @this {Parcours}
 */
export default function main () {
  const me = this
  const stor = this.storage
  const ds = me.donneesSection

  const getCodeCouleur = (ch) => couleurs[ch]

  function onClickDroite (evt) {
    // Je desactive la sélection des droites si nous ne sommes pas en droit de le faire
    if (me.etat !== 'correction') return
    const id = evt.target.getAttribute('id')
    const obj = stor.repere.getObjet(id.substring(6))
    for (let k = 0; k < ds.nbdroites; k++) {
      if (stor.codesCouleurs[k] === obj.style.couleur) {
        stor.place++
        if (stor.place === 3) stor.place = 1
        stor.droitesSelectionnees[stor.place - 1] = id
        stor.couleursSelectionnees[stor.place - 1] = stor.nomsCouleurs[k]
        stor.maPhrase.innerHTML = j3pChaine(textes.comment4, { c1: getCodeCouleur(stor.couleursSelectionnees[0]), c2: stor.couleursSelectionnees[0], c3: getCodeCouleur(stor.couleursSelectionnees[1]), c4: stor.couleursSelectionnees[1] })
      }
    }
  }

  function enonceMain () {
    stor.place = 0
    // on tire un ordre de couleurs aléatoire
    // les noms
    stor.nomsCouleurs = j3pShuffle(Object.keys(couleurs))
    // les codes
    stor.codesCouleurs = stor.nomsCouleurs.map(couleur => couleurs[couleur])
    stor.couleursSelectionnees = ['....', '....']
    stor.droitesSelectionnees = []

    let fini = false
    // les coords des points des droites, initialisées à 0
    const coord = []
    while (coord.length < ds.nbdroites * 2) coord.push({ x: 0, y: 0 })
    const points = []
    const droites = []
    // raccourcis pour les 4 premières coords
    const [c0, c1, c2, c3] = coord
    while (!fini) {
      c0.x = j3pGetRandomInt(-5, 5)
      c0.y = j3pGetRandomInt(-5, 5)

      c1.x = c0.x + 1
      let inclinaison = 0

      while (Math.abs(inclinaison) < 10) {
        inclinaison = j3pGetRandomInt(ds.inclinaison)
      }
      c1.y = c0.y + Math.tan((inclinaison / 180) * Math.PI)

      c2.x = j3pGetRandomInt(-7, 7)
      c2.y = j3pGetRandomInt(-7, 7)
      c3.x = c2.x + 1
      c3.y = c2.y + Math.tan((inclinaison / 180) * Math.PI)

      if (j3pDistancepointdroite(c2.x, c2.y, j3pEquation(c0.x, c0.y, c1.x, c1.y)) > 5) {
        fini = true
      }
    }
    // on place ces 4 points
    points[0] = {
      type: 'point',
      nom: 'A0',
      par1: c0.x,
      par2: c0.y,
      fixe: true,
      visible: false,
      etiquette: false,
      style: { couleur: '#00F', epaisseur: 2, taille: 18 }
    }
    points[1] = {
      type: 'point',
      nom: 'A1',
      par1: c1.x,
      par2: c1.y,
      fixe: true,
      visible: false,
      etiquette: false,
      style: { couleur: '#00F', epaisseur: 2, taille: 18 }
    }
    points[2] = {
      type: 'point',
      nom: 'A2',
      par1: c2.x,
      par2: c2.y,
      fixe: true,
      visible: false,
      etiquette: false,
      style: { couleur: '#00F', epaisseur: 2, taille: 18 }
    }
    points[3] = {
      type: 'point',
      nom: 'A3',
      par1: c3.x,
      par2: c3.y,
      fixe: true,
      visible: false,
      etiquette: false,
      style: { couleur: '#00F', epaisseur: 2, taille: 18 }
    }
    // et deux droites passant par ces points
    droites[0] = {
      type: 'droite',
      nom: 'A0A1',
      par1: 'A0',
      par2: 'A1',
      style: { couleur: stor.codesCouleurs[0], epaisseur: 5, opacite: 1 }
    }
    droites[1] = {
      type: 'droite',
      nom: 'A2A3',
      par1: 'A2',
      par2: 'A3',
      style: { couleur: stor.codesCouleurs[1], epaisseur: 5 }
    }

    // on place les droites suivantes
    for (let k = 2; k < ds.nbdroites; k++) {
    // les coords des deux points de cette droite k
      const c0 = coord[2 * k]
      const c1 = coord[2 * k + 1]

      // chaque droite coupe la droite précédente sur l’écran
      fini = false
      while (!fini) {
        const coef = (j3pGetRandomInt(0, 100) / 100)

        c0.x = coord[2 * k - 2].x + coef * (coord[2 * k - 1].x - coord[2 * k - 2].x)
        c0.y = coord[2 * k - 2].y + coef * (coord[2 * k - 1].y - coord[2 * k - 2].y)
        c1.x = j3pGetRandomInt(-7, 7)
        c1.y = j3pGetRandomInt(-7, 7)

        while (Math.abs(j3pAngledroites(j3pEquation(c0.x, c0.y, c1.x, c1.y), j3pEquation(0, 1, 1, 1))) < 10) {
          c0.x = coord[2 * k - 2].x + coef * (coord[2 * k - 1].x - coord[2 * k - 2].x)
          c0.y = coord[2 * k - 2].y + coef * (coord[2 * k - 1].y - coord[2 * k - 2].y)
          c1.x = j3pGetRandomInt(-7, 7)
          c1.y = j3pGetRandomInt(-7, 7)
        }

        let valide = true
        for (let j = 0; j < k; j++) {
        // la droite créée ne doit être parallèle à aucune des droites précédentes
          if (j3pAngledroites(j3pEquation(c0.x, c0.y, c1.x, c1.y), j3pEquation(coord[2 * j].x, coord[2 * j].y, coord[2 * j + 1].x, coord[2 * j + 1].y)) < 10) {
            valide = false
          }
        }

        fini = valide
      }
      points[2 * k] = {
        type: 'point',
        nom: 'A' + (2 * k),
        par1: c0.x,
        par2: c0.y,
        fixe: false,
        visible: false,
        etiquette: false,
        style: { couleur: '#00F', epaisseur: 2, taille: 18 }
      }
      points[2 * k + 1] = {
        type: 'point',
        nom: 'A' + (2 * k + 1),
        par1: c1.x,
        par2: c1.y,
        fixe: true,
        visible: false,
        etiquette: false,
        style: { couleur: '#00F', epaisseur: 2, taille: 18 }
      }
      droites[k] = {
        type: 'droite',
        nom: 'A' + (2 * k) + 'A' + (2 * k + 1),
        par1: 'A' + (2 * k),
        par2: 'A' + (2 * k + 1),
        style: { couleur: stor.codesCouleurs[k], epaisseur: 5 }
      }
    } // fin du for pour les droites 2 à nbDroites
    stor.idRepere = j3pGetNewId('unrepere')
    stor.repere = new Repere({
      keepWeaksIds: true, // @todo virer ça dès qu’on pourra (plus de j3pElement sur les ids foireux mis par Repere)
      idConteneur: me.zonesElts.MG,
      idDivRepere: stor.idRepere,
      visible: false,
      fixe: true,
      aimantage: false,
      larg: 500,
      haut: 400,

      pasdunegraduationX: 1,
      pixelspargraduationX: 25,
      pasdunegraduationY: 1,
      pixelspargraduationY: 25,
      xO: 250,
      yO: 250,
      debuty: 0,
      negatifs: true,
      objets: points.concat(droites)
    })
    stor.repere.construit()

    for (let k = 0; k < ds.nbdroites; k++) {
      j3pElement('droiteA' + (2 * k) + 'A' + (2 * k + 1)).addEventListener('click', onClickDroite)
    }

    const ch = textes.cons2
    stor.conteneur = j3pAddElt(me.zonesElts.MD, 'div', '', { style: me.styles.etendre('moyen.enonce', { padding: '10px' }) })
    j3pAddElt(stor.conteneur, 'div', ch)
    stor.maPhrase = j3pAddElt(stor.conteneur, 'div', textes.cons1)

    stor.zoneCorr = j3pAddElt(stor.conteneur, 'div', '', { style: me.styles.etendre('moyen.correction', { paddingTop: '30px' }) })

    me.finEnonce()
  }
  switch (me.etat) {
    case 'enonce':
      if (me.debutDeLaSection) {
        me.validOnEnter = false // ex donneesSection.touche_entree
        if (ds.nbdroites > nbMax) {
          console.error(Error(`Trop de droites, on ramène à ${nbMax}`))
          ds.nbdroites = nbMax
        }
        me.construitStructurePage({ structure: 'presentation1', ratioGauche: 0.5 })
        me.typederreurs = [0, 0, 0, 0, 0, 0]
        me.afficheTitre(textes.titre_exo)
        if (ds.indication) me.indication(me.zonesElts.IG, ds.indication)
      } else {
        me.videLesZones()
      }

      enonceMain()
      break // case enonce

    case 'correction': {
      j3pEmpty(stor.zoneCorr)
      const [d0, d1] = stor.droitesSelectionnees

      // on teste si y’a bien deux réponses
      if (!d0 || !d1) {
        // il en manque
        stor.zoneCorr.style.color = me.styles.cfaux
        j3pAddContent(stor.zoneCorr, textes.comment1)
        j3pAddElt(stor.zoneCorr, 'br')
        return this.finCorrection()
      }

      if (d0 === d1) {
        // 2 × la même
        stor.zoneCorr.style.color = me.styles.cfaux
        j3pAddContent(stor.zoneCorr, textes.comment2)
        j3pAddElt(stor.zoneCorr, 'br')
        return this.finCorrection()
      }

      const cbon = (d0 === 'droiteA0A1' && d1 === 'droiteA2A3') || (d1 === 'droiteA0A1' && d0 === 'droiteA2A3')
      if (cbon) {
        me.score++
        stor.zoneCorr.style.color = me.styles.cbien
        j3pAddContent(stor.zoneCorr, cBien)
        me.typederreurs[0]++
        return this.finCorrection('navigation', true)
      }

      stor.zoneCorr.style.color = me.styles.cfaux
      if (me.isElapsed) { // limite de temps
        stor.zoneCorr.innerHTML = tempsDepasse
        me.typederreurs[10]++
        return this.finCorrection('navigation', true)
      }
      // on vire le préfixe "droite"
      const droiteOk0 = (d0.substring(6))
      const droiteOk1 = (d1.substring(6))
      j3pEmpty(stor.idRepere)

      stor.repere.add({
        type: 'pointintersection',
        nom: 'T',
        par1: droiteOk0,
        par2: droiteOk1,
        visible: true,
        etiquette: true,
        style: { couleur: '#000', epaisseur: 3, taille: 24 }
      })

      // on baisse l’opacité des droites non sélectionnées
      stor.repere.objets
        .filter(o => o.type === 'droite' && ![droiteOk0, droiteOk1].includes(o.nom))
        .forEach(droite => {
          droite.style.opacite = 0.2
        })
      stor.repere.fixe = false
      stor.repere.construit()
      j3pAddContent(stor.zoneCorr, textes.comment3)

      if (me.essaiCourant < ds.nbchances) {
        j3pAddContent(stor.zoneCorr, '<br>' + essaieEncore)
        me.typederreurs[1]++
        return this.finCorrection()
      }
      // c’est foireux et terminé
      me.typederreurs[2]++
      this.finCorrection('navigation', true)
      break // case "correction":
    }

    case 'navigation':
      if (!me.sectionTerminee()) {
        me.etat = 'enonce'
      }
      this.finNavigation()
      break
  }
}
