import { j3pAddContent, j3pAddElt, j3pBarre, j3pFocus, j3pFreezeElt, j3pGetRandomInt, j3pNombreBienEcrit, j3pRandomdec, j3pRemplacePoint, j3pRestriction, j3pStyle, j3pValeurde } from 'src/legacy/core/functions'
import { j3pAffiche, j3pDesactive } from 'src/lib/mathquill/functions'
import textesGeneriques from 'src/lib/core/textes'

const { cBien, cFaux, essaieEncore, tempsDepasse } = textesGeneriques

export const params = { outils: ['calculatrice', 'mathquill'] }

/**
 * Ajouter une description ici
 * @fileOverview
 * @author Tommy Barroy <t.barroy@laposte.net>
 * @since 2017-10
*/
export default function main () {
  const me = this
  let ds = me.donneesSection
  const stor = me.storage
  function compare (a1, a2) { // compare 2 chaines
    if (a1.length !== a2.length) {
      return false
    } else {
      for (let j = 1; j < a1.length - 1; j++) {
        if (a1[j] !== a2[j]) return false
      }
    }
    return true
  }

  function getDonnees () {
    return {

      typesection: 'primaire',
      nbrepetitions: 5,
      nbetapes: 1,
      indication: '',
      seuil: 0.6,
      nbchances: 1,
      structure: 'presentation2',

      textes: {
        titreExo: 'Zéros inutiles - (N4 F4 ex1)',
        phrase1: 'Réécris le nombre en supprimant les zéros inutiles (lorsqu’il y en a) :',
        phrase3: 'Conserve également les espaces nécessaires.',
        cons1: 'Réponse :  @1@',
        phrase2: '<br>La solution est : '
      }
    }
  }
  function enonceMain () {
    const st1 = me.styles.etendre('grand.enonce', { padding: '20px' })
    stor.conteneur = j3pAddElt(me.zonesElts.MG, 'div', '', { style: st1 })

    let tempo = j3pRandomdec('[0;9999],5')

    stor.stockage[0] = j3pNombreBienEcrit(tempo) // stockage de la solution
    tempo = j3pNombreBienEcrit(tempo)
    if (j3pGetRandomInt(0, 1)) {
      tempo = ((tempo.split(',')[0].replace(/\s/g, '').length % 3 === 0) ? '0 ' : '0') + tempo
    } // on ajoute un zéro aléatoirement au début
    if (tempo.split(',')[1].replace(/\s/g, '').length % 3 === 0) tempo += ' '
    if (j3pGetRandomInt(0, 1)) tempo = tempo + '0'
    if (tempo.split(',')[1].replace(/\s/g, '').length % 3 === 0) tempo += ' '
    if (j3pGetRandomInt(0, 1)) tempo = tempo + '0' // on ajoute 0 1 2 zéro aléatoirement à la fin

    for (let j = 1; j <= 3; j++) stor['zoneCons' + j] = j3pAddElt(stor.conteneur, 'div')
    j3pAddContent(stor.zoneCons1, ds.textes.phrase1)
    j3pAddElt(stor.zoneCons1, 'br')
    const spanNb = j3pAddElt(stor.zoneCons1, 'span', tempo)
    j3pStyle(spanNb, { color: 'red' })
    j3pAffiche(stor.zoneCons2, '', ds.textes.phrase3)
    stor.zoneCons2.style.fontStyle = 'italic'
    stor.zoneCons2.style.fontSize = '0.9em'
    stor.zoneCons2.style.paddingBottom = '25px'
    const elt = j3pAffiche(stor.zoneCons3, '', ds.textes.cons1, { input1: { texte: '', dynamique: true } })
    stor.zoneInput = elt.inputList[0]
    j3pRestriction(stor.zoneInput, '0-9,. ')
    stor.zoneInput.addEventListener('input', j3pRemplacePoint)
    j3pFocus(stor.zoneInput)
    stor.zoneCorr = j3pAddElt(stor.conteneur, 'div', '', { style: me.styles.etendre('petit.correction', { padding: '10px' }) })

    me.finEnonce()
  }
  switch (me.etat) {
    case 'enonce':
      if (me.debutDeLaSection) {
        me.donneesSection = getDonnees()
        ds = me.donneesSection
        me.surcharge()
        me.construitStructurePage({ structure: ds.structure, ratioGauche: 0.6 })
        stor.stockage = [0, 0, 0, 0]
        me.typederreurs = [0, 0, 0, 0, 0, 0]
        me.score = 0

        me.videLesZones()
        me.afficheTitre(ds.textes.titreExo)
        if (ds.indication) me.indication(me.zonesElts.IG, ds.indication)
      } else {
        me.videLesZones()
      }
      enonceMain()
      break // case "enonce":

    case 'correction':

      me.cacheBoutonValider()
      // on teste si une réponse a été saisie
      if (stor.zoneInput.value === '') {
        me.reponseManquante(stor.zoneCorr)
        j3pFocus(stor.zoneInput)
      } else { // une réponse a été saisie
        const a1 = stor.stockage[0]
        const a2 = j3pValeurde(stor.zoneInput)
        const cbon = (compare(a1, a2))
        if (cbon) {
          me.score++
          stor.zoneInput.style.color = me.styles.cbien
          j3pDesactive(stor.zoneInput)
          j3pFreezeElt(stor.zoneInput)
          stor.zoneCorr.style.color = me.styles.cbien
          stor.zoneCorr.innerHTML = cBien
          me.typederreurs[0]++
          me.etat = 'navigation'
          me.sectionCourante()
        } else {
          // Pas de bonne réponse
          stor.zoneCorr.style.color = me.styles.cfaux
          stor.zoneInput.style.color = me.styles.cfaux
          // à cause de la limite de temps :
          if (me.isElapsed) { // limite de temps
            stor.zoneCorr.innerHTML = tempsDepasse
            me.typederreurs[10]++
            j3pDesactive(stor.zoneInput)
            j3pFreezeElt(stor.zoneInput)
            me.etat = 'navigation'
            me.sectionCourante()
          } else {
            // réponse fausse :
            stor.zoneCorr.innerHTML = cFaux
            // S’il y a deux chances,on appelle à nouveau le bouton Valider

            if (me.essaiCourant < ds.nbchances) {
              stor.zoneCorr.innerHTML += '<br>' + essaieEncore
              me.typederreurs[1]++
              // indication éventuelle ici
              me.etat = 'correction'
              j3pFocus(stor.zoneInput)
              me.afficheBoutonValider()// c’est la partie "correction" de la section qui sera exécutée
            } else {
              me.typederreurs[2]++
              j3pDesactive(stor.zoneInput)
              j3pBarre(stor.zoneInput)
              stor.zoneCorr.innerHTML += ds.textes.phrase2 + stor.stockage[0]
              me.etat = 'navigation'
              me.sectionCourante()
            }
          }
        }
      }
      me.finCorrection()
      break // case "correction":

    case 'navigation':
      if (!me.sectionTerminee()) {
        me.etat = 'enonce'
      }
      me.finNavigation()
      break // case "navigation":
  }
}
