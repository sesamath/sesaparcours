import { j3pAddContent, j3pAddElt, j3pAjouteBouton, j3pBarre, j3pBasculeAffichage, j3pDetruit, j3pElement, j3pFocus, j3pFreezeElt, j3pGetNewId, j3pStyle, j3pValeurde } from 'src/legacy/core/functions'
import { j3pAffiche, j3pDesactive, mqRestriction } from 'src/lib/mathquill/functions'
import { j3pCreeTexte } from 'src/legacy/core/functionsSvg'
import textesGeneriques from 'src/lib/core/textes'

const { cBien, cFaux, essaieEncore, regardeCorrection, tempsDepasse } = textesGeneriques

/*
        Bruno Meunier
        Mai 2013
        Section adaptée au clavier virtuel par Yves en 10-2021
 */

export const params = {
  outils: ['calculatrice', 'mathquill'],
  /**
   * Ce qui est paramétrable dans l’éditeur de graphe
   * @type {LegacySectionParamsParametres}
   */
  parametres: [
    ['nbrepetitions', 4, 'entier', 'aucun'],
    ['indication', '', 'string', 'aucun'],
    ['limite', 0, 'entier', 'Temps disponible par répétition, en secondes'],
    ['nbchances', 2, 'entier', 'Nombre d’erreurs possibles (pour tenter de les corriger)'],
    ['affichageAide', 'manuel', 'liste', 'Affichage de l’ aide.', ['aucun', 'auto', 'manuel']],
    ['graduation1', [0, 0, 290, 975], 'array', 'Valeur de la première graduation (le 1er élément correspond à la 1re répétition)'],
    ['pas', [10, 100, 10, 5], 'array', 'Pas d’une graduation'],
    ['etiquettes', ['consecutives', 'non-consecutives', 'consecutives', 'non-consecutives'], 'array', 'Les deux étiquettes affichées se suivent ou pas.', ['non-consecutives', 'consecutives']],
    ['etiquetteOrigine', [true, true, false, false], 'array', 'La première étiquette affichée n’est pas l’origine de l’axe.']
  ]

}

/**
 * Ajouter une description ici
 * @fileOverview
 * @author Tommy Barroy <t.barroy@laposte.net>
 * @since 2017-10
*/
export default function main () {
  const me = this
  const ds = me.donneesSection
  const stor = me.storage
  function basculerAffichagePas () {
    j3pBasculeAffichage(stor.idGroupe)
  }

  function enonceInitFirst () {
    me.construitStructurePage('presentation1')
    stor.stockage = []
    me.typederreurs = []
    me.afficheTitre('Graduer un axe - (N1 F3 ex1)')
    if (ds.indication) me.indication(me.zonesElts.IG, ds.indication)
  }

  function enonceMain () {
    // Creation de la consigne
    stor.conteneur = j3pAddElt(me.zonesElts.MG, 'div', '', { style: me.styles.etendre('petit.enonce', { padding: '20px' }) })
    j3pAddElt(stor.conteneur, 'div', 'Complète avec les nombres qui conviennent.')

    // La droite graduée
    stor.idGroupe = j3pGetNewId('groupeDroite') // aucune idée ce à quoi cela peut servir... C’est pour la droite
    const q = me.questionCourante - 1
    stor.droite = j3pAddElt(stor.conteneur, 'div')
    j3pStyle(stor.droite, { paddingTop: '70px', paddingLeft: '30px' })
    const droiteG = creerDroiteGraduee(
      ds.graduation1[q],
      ds.pas[q],
      ds.etiquettes[q],
      ds.etiquetteOrigine[q]
    )

    stor.stockage.soluce = droiteG.aSoluce
    droiteG.conteneur.style.position = 'absolute'
    stor.droite.appendChild(droiteG.conteneur)
    if (ds.affichageAide !== 'auto') {
      basculerAffichagePas()
    }

    // les etiquettes de saisie
    stor.blockSaisie = j3pAddElt(stor.droite, 'div')
    j3pStyle(stor.blockSaisie, { position: 'relative' })
    // j3pDiv('n1-31-conteneur-droite', 'n1-31-block-saisie')
    // j3pElement('n1-31-block-saisie').style.position = 'relative'
    // stor.stockage.aTfSaisie = []
    let j = 0
    let index
    stor.zoneInput = []
    for (let i = 0; i < 7; i++) {
      if (droiteG.aEtiquetteSaisie.indexOf(i) > -1) {
        const posX = i * droiteG.largeurGrad + droiteG.margeGauche - 15
        const eltSaisie = j3pAddElt(stor.blockSaisie, 'div')
        j3pStyle(eltSaisie, { position: 'absolute', top: -12, left: posX, fontSize: me.styles.toutpetit.enonce.fontSize })
        // mise en valeur du chiffre qui change pour un pas puissance de 10
        let sCor = String(droiteG.aSoluce[j])
        const aChar = sCor.split('')
        const sPas = String(ds.pas[q])
        const expre = /10+/

        if (sPas.match(expre)) index = sCor.length - sPas.length
        sCor = ''
        for (let k = 0; k < aChar.length; k++) {
          k === index ? sCor += '<b>' + aChar[k] + '</b>' : sCor += aChar[k]
        }

        const elt = j3pAffiche(eltSaisie, '', '&1&',
          {
            inputmq1: { width: '50px', taillepolice: 19, correction: sCor, maxchars: 4 }
          })
        stor.zoneInput.push(elt.inputmqList[0])
        elt.inputmqList[0].leSpan = eltSaisie // Pour récupérer facilement l’elt parent
        elt.inputmqList[0].reponse = [sCor]
        mqRestriction(elt.inputmqList[0], '\\d')
        elt.inputmqList[0].style.textAlign = 'center'
        j++
      }
    }
    stor.zoneD = j3pAddElt(me.zonesElts.MD, 'div', '', { style: me.styles.etendre('moyen.explications', { padding: '20px' }) })

    // le bouton Aide
    stor.divBtnAide = j3pAddElt(stor.zoneD, 'div')
    if (ds.affichageAide === 'manuel') {
      stor.idBtn = j3pGetNewId('boutonAide')
      j3pAjouteBouton(stor.divBtnAide, stor.idBtn, 'MepBoutons', 'Aide', basculerAffichagePas)
    }
    stor.zoneCorr = j3pAddElt(stor.zoneD, 'div', '', { style: me.styles.etendre('petit.correction', { padding: '10px' }) })
    j3pFocus(stor.zoneInput[0])
    me.finEnonce()
  }
  switch (me.etat) {
    case 'enonce':
      if (me.debutDeLaSection) {
        enonceInitFirst()
      } else {
        me.videLesZones()
      }
      enonceMain()
      break // case "enonce":

    case 'correction': {
      // on teste si toutes les réponses ont été saisies
      const aTestReponse = []
      for (let i = stor.zoneInput.length - 1; i >= 0; i--) {
        aTestReponse[i] = (j3pValeurde(stor.zoneInput[i]) !== '')
        if (!aTestReponse[i]) j3pFocus(stor.zoneInput[i])
      }

      if ((aTestReponse.includes(false)) && (!me.isElapsed)) {
        me.reponseManquante(stor.zoneCorr)
        return me.finCorrection()
      }

      // une réponse a été saisie
      const aRepEleve = []
      for (let i = 0; i < stor.zoneInput.length; i++) {
        const repEleve = j3pValeurde(stor.zoneInput[i])
        const soluce = stor.stockage.soluce[i]
        aRepEleve.push(Number(repEleve) === soluce)
        if (aRepEleve[i]) {
          stor.zoneInput[i].style.color = me.styles.cbien
          j3pDesactive(stor.zoneInput[i])
        } else {
          stor.zoneInput[i].style.color = me.styles.cfaux
        }
      }
      if (!aRepEleve.includes(false)) {
        me.score++
        stor.zoneCorr.style.color = me.styles.cbien
        stor.zoneCorr.innerHTML = cBien
        me.typederreurs[0]++
        return me.finCorrection('navigation', true)
      }

      // Pas de bonne réponse
      stor.zoneCorr.style.color = me.styles.cfaux
      // à cause de la limite de temps :
      if (me.isElapsed) { // limite de temps
        stor.zoneCorr.innerHTML = tempsDepasse
        me.typederreurs[10]++
        const droiteCorr = j3pAddElt(stor.droite, 'div', '', { style: me.styles.toutpetit.correction })
        j3pStyle(droiteCorr, { position: 'relative' })
        for (let i = 0; i < stor.zoneInput.length; i++) {
          if (!aRepEleve[i]) {
            j3pDesactive(stor.zoneInput[i])
            j3pFreezeElt(stor.zoneInput[i])
            const eltCorr = j3pAddElt(droiteCorr, 'span')
            const posTop = (stor.zoneInput[i].leSpan.style.top.split('px')[0] - 25) + 'px'
            j3pStyle(eltCorr, { position: 'absolute', left: stor.zoneInput[i].leSpan.style.left, top: posTop })
            j3pAddContent(eltCorr, stor.zoneInput[i].reponse[0])
          }
        }
        j3pDetruit(stor.divBtnAide)
        return me.finCorrection('navigation', true)
      }

      // réponse fausse :
      stor.zoneCorr.innerHTML = cFaux
      // S’il y a deux chances,on appelle à nouveau le bouton Valider
      if (me.essaiCourant < ds.nbchances) {
        stor.zoneCorr.innerHTML += '<br>' + essaieEncore
        me.typederreurs[1]++
        return me.finCorrection() // on reste en correction
      }

      // Erreur au dernier essai
      const droiteCorr = j3pAddElt(stor.droite, 'div', '', { style: me.styles.toutpetit.correction })
      j3pStyle(droiteCorr, { position: 'relative' })
      for (let i = 0; i < stor.zoneInput.length; i++) {
        if (!aRepEleve[i]) {
          j3pDesactive(stor.zoneInput[i])
          j3pBarre(stor.zoneInput[i])
          const eltCorr = j3pAddElt(droiteCorr, 'span')
          const posTop = (stor.zoneInput[i].leSpan.style.top.split('px')[0] - 25) + 'px'
          j3pStyle(eltCorr, { position: 'absolute', left: stor.zoneInput[i].leSpan.style.left, top: posTop })
          j3pAddContent(eltCorr, stor.zoneInput[i].reponse[0])
        }
        j3pFreezeElt(stor.zoneInput[i])
      }
      j3pDetruit(stor.divBtnAide)
      if (j3pElement(stor.idGroupe).style.display === 'none') basculerAffichagePas()
      // le pas est une puissance de 10
      stor.zoneCorr.innerHTML += '<br>' + regardeCorrection
      me.typederreurs[2]++
      me.finCorrection('navigation', true)
      break // case "correction":
    }

    case 'navigation':
      if (!me.sectionTerminee()) {
        me.etat = 'enonce'
      }
      me.finNavigation()
      break // case "navigation":
  }

  // retourne un objet qui trois props : conteneur,valeurSoluce, etiquetteSoluce
  function creerDroiteGraduee (graduation1, pas, etiquettes, etiquetteOrigine) {
    // const largeurDroiteGraduee = 480
    const largeurDroiteGraduee = 600
    const largeurGrad = Math.floor(largeurDroiteGraduee / 7)
    const corps = 16
    const hauteurGrad = 20
    const margeGauche = 20
    const margeHaut = corps + hauteurGrad
    const aSoluce = []

    // la droite
    const svgns = 'http://www.w3.org/2000/svg'
    const svgContainer = document.createElementNS(svgns, 'svg')
    const d = document.createElementNS(svgns, 'line')
    const oStyleDroite = {
      stroke: '#666666',
      'stroke-width': '1',
      x1: margeGauche,
      y1: margeHaut,
      x2: largeurDroiteGraduee + margeGauche,
      y2: margeHaut
    }
    for (const prop in oStyleDroite) {
      d.setAttribute(prop, oStyleDroite[prop])
    }
    svgContainer.appendChild(d)
    const t = document.createElementNS(svgns, 'polygon')
    const aPoints = [
      largeurDroiteGraduee + margeGauche,
      margeHaut - 10,
      largeurDroiteGraduee + margeGauche + 10,
      margeHaut,
      largeurDroiteGraduee + margeGauche,
      margeHaut + 10,
      largeurDroiteGraduee + margeGauche,
      margeHaut - 10
    ]
    const sPoints = aPoints.join(',')
    const oStyleTriangle = {
      points: sPoints,
      fill: '#666666'
    }
    for (const prop in oStyleTriangle) {
      t.setAttribute(prop, oStyleTriangle[prop])
    }
    svgContainer.appendChild(t)

    // choix des etiquettes à afficher et des etiquettes saisie
    let aEtiquetteVisible = []
    const aEtiquetteSaisie = []
    let aleaA
    if (etiquetteOrigine && etiquettes === 'consecutives') {
      aEtiquetteVisible = [0, 1]
    } else if (etiquetteOrigine && etiquettes === 'non-consecutives') {
      aleaA = 2 + Math.floor(Math.random() * (5 - 2 + 1))
      aEtiquetteVisible = [0, aleaA]
    } else if (!etiquetteOrigine && etiquettes === 'consecutives') {
      aleaA = 2 + Math.floor(Math.random() * (3 - 2 + 1))
      aEtiquetteVisible = [aleaA, aleaA + 1]
    } else if (!etiquetteOrigine && etiquettes === 'non-consecutives') {
      aleaA = 2 + Math.floor(Math.random() * (3 - 2 + 1))
      aEtiquetteVisible = [aleaA, aleaA + 2]
    }
    const groupe = document.createElementNS(svgns, 'g')
    groupe.setAttribute('id', stor.idGroupe)
    let label
    for (let i = 0; i < 7; i++) {
      // les graduations
      const oStyleGrad = {
        stroke: '#666666',
        'stroke-width': '1',
        x1: i * largeurGrad + margeGauche,
        y1: corps + 5,
        x2: i * largeurGrad + margeGauche,
        y2: margeHaut
      }
      const grad = document.createElementNS(svgns, 'line')
      for (const prop in oStyleGrad) {
        grad.setAttribute(prop, oStyleGrad[prop])
      }
      if (etiquetteOrigine || i > 0) {
        svgContainer.appendChild(grad)
      }
      // les etiquettes
      const oStyleLabel = {
        'font-family': 'Arial',
        'font-size': '16',
        fill: '#000000',
        'text-anchor': 'middle',
        x: i * largeurGrad + margeGauche,
        y: corps
      }
      label = document.createElementNS(svgns, 'text')
      const nombre = graduation1 + (i * pas)
      const texte = document.createTextNode(nombre)
      label.appendChild(texte)
      for (const prop in oStyleLabel) {
        label.setAttribute(prop, oStyleLabel[prop])
      }
      if (aEtiquetteVisible.indexOf(i) > -1) {
        svgContainer.appendChild(label)
      } else if (etiquetteOrigine || i > 0) {
        aEtiquetteSaisie.push(i)
        aSoluce.push(nombre)
      }
      // les indications de pas

      const boite = document.createElementNS(svgns, 'marker')
      let oAttr = {
        id: 't-' + i,
        viewBox: '0 0 10 10',
        refX: '0',
        refY: '5',
        markerWidth: '10',
        markerHeight: '10',
        orient: 'auto'
      }
      for (const prop in oAttr) {
        boite.setAttribute(prop, oAttr[prop])
      }
      const pointe = document.createElementNS(svgns, 'path')
      oAttr = {
        d: 'M 0 0 L 10 5 L 0 10 z',
        fill: '#3D93C6'
      }
      for (const prop in oAttr) {
        pointe.setAttribute(prop, oAttr[prop])
      }
      boite.appendChild(pointe)
      groupe.appendChild(boite)

      const x1 = i * largeurGrad + margeGauche
      const y1 = margeHaut + 5
      const x2 = largeurGrad / 2
      const y2 = 40
      const x3 = largeurGrad - 6
      const y3 = 5
      const oStyleCourbe = {
        stroke: '#3D93C6',
        fill: 'transparent',
        d: 'M' + x1 + ' ' + y1 + ' q ' + x2 + ' ' + y2 + ' ' + x3 + ' ' + y3,
        'marker-end': 'url(#t-' + i + ')'
      }
      const courbe = document.createElementNS(svgns, 'path')
      for (const prop in oStyleCourbe) {
        courbe.setAttribute(prop, oStyleCourbe[prop])
      }

      if ((etiquetteOrigine || i > 0) && i < 6) {
        groupe.appendChild(courbe)
        label = j3pCreeTexte(groupe, { id: 'text' + i, texte: '+ ' + pas, x: x1 + largeurGrad / 2, y: 80, couleur: '#3D93C6', taille: 12 })
        label.setAttribute('font-family', 'Arial')
        label.setAttribute('text-anchor', 'middle')
      }
      svgContainer.appendChild(groupe)
    }

    // $(svgContainer).css({width:largeurDroiteGraduee+2*margeGauche,height:50});
    svgContainer.style.width = largeurDroiteGraduee + 2 * margeGauche + 'px'
    svgContainer.style.height = '150px'
    return {
      conteneur: svgContainer,
      aEtiquetteSaisie,
      margeGauche,
      largeurGrad,
      aSoluce,
      groupePas: groupe
    }
  }
}
