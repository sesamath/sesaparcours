import { j3pAddContent, j3pAddElt, j3pBarre, j3pChaine, j3pFocus, j3pFreezeElt, j3pGetBornesIntervalle, j3pGetRandomBool, j3pGetRandomInt, j3pNombreBienEcrit, j3pRestriction, j3pStyle, j3pValeurde } from 'src/legacy/core/functions'
import Boulier from 'src/legacy/outils/boulier/boulier'
import { j3pAffiche, j3pDesactive } from 'src/lib/mathquill/functions'
import textesGeneriques from 'src/lib/core/textes'

const { cBien, cFaux, essaieEncore, tempsDepasse } = textesGeneriques

/*
    JP Vanroyen
    2012-2013
*/

export const params = {
  outils: ['calculatrice'],
  /**
   * Ce qui est paramétrable dans l’éditeur de graphe
   * @type {LegacySectionParamsParametres}
   */
  parametres: [
    ['nbrepetitions', 5, 'entier', 'aucun'],
    ['indication', '', 'string', 'aucun'],
    ['limite', 0, 'entier', 'aucun'],
    ['nbchances', 1, 'entier', 'Nombre d’erreurs possibles (pour tenter de les corriger)'],
    ['nbtiges', 6, 'entier', 'nombre de tiges : de 1 à 8'],
    ['type', 'chinois', 'liste', 'type de boulier', ['japonais', 'chinois', 'lesdeux']],
    ['nombre', '[10;9999]', 'intervalle', 'intervalle d’appartenance du nombre demandé'],
    ['espacesImposes', true, 'boolean', 'savoir si on impose les espaces ou non dans l’écritude du nombre']
  ]
}

/**
 * Ajouter une description ici
 * @fileOverview
 * @author Tommy Barroy <t.barroy@laposte.net>
 * @since 2017-10
*/
export default function main () {
  const me = this
  let ds = me.donneesSection
  const stor = me.storage

  function getDonnees () {
    return {

      typesection: 'primaire',
      nbrepetitions: 10,
      nbetapes: 1,
      indication: '',
      nbchances: 2,
      espacesImposes: true,
      structure: 'presentation1', //  || "presentation2" || "presentation3"
      // limite=5;

      textes: {
        titre_exo: 'Boulier - (A1 F1 ex2)',
        phrase1: 'Trouve le nombre inscrit sur le boulier.',
        phrase2: 'Ecris ce nombre avec les espaces éventuels bien placés.',
        phrase3: 'Ce n’est pas ce qui était inscrit sur le boulier&nbsp;!',
        phrase4: 'Il fallait lire £n.',
        comment1: 'Attention aux espaces dans l’écriture du nombre&nbsp;'
      },

      L: 600,
      ep: 15,
      nbtiges: 6, // de 1 à 6
      diametre: 30,
      ept: 4,

      type: 'chinois', // "japonais" ||"lesdeux" ||"chinois"
      economique: false, // pour le chinois
      nombre: '[10;9999]',
      // nombre: "000000000000";
      boutonnombre: true,
      affichagenombre: false,
      surchargeaffichagenombre: false
    }
  }
  function enonceMain () {
    const o1 = me.styles.etendre('petit.enonce', { padding: '20px' })
    stor.conteneur = j3pAddElt(me.zonesElts.MG, 'div', '', { style: o1 })
    stor.conteneurD = j3pAddElt(me.zonesElts.MD, 'div', '', { style: o1 })
    const [intMin, intMax] = j3pGetBornesIntervalle(ds.nombre)
    stor.stockage[0] = j3pGetRandomInt(intMin, intMax) // réponse attendue
    j3pAddElt(stor.conteneurD, 'p', ds.textes.phrase1)
    const zoneCons2 = j3pAddElt(stor.conteneurD, 'div')
    const elt = j3pAffiche(zoneCons2, '', '@1@', { input1: { texte: '', dynamique: true } })
    stor.zoneInput = elt.inputList[0]
    if (ds.espacesImposes) {
      j3pAddElt(stor.conteneurD, 'div', ds.textes.phrase2, {
        style: {
          padding: '5px 0',
          fontStyle: 'italic',
          fontSize: '0.9em'
        }
      })
    }

    const element = j3pAddElt(stor.conteneur, 'div')

    let isChinese
    if (ds.type === 'lesdeux') {
      isChinese = j3pGetRandomBool()
    } else {
      isChinese = ds.type !== 'japonais'
    }

    stor.boulier = new Boulier(
      {
        isChinese,
        width: ds.L,
        epCadre: ds.ep,
        nbTiges: ds.nbtiges,
        diametre: ds.diametre,
        epTige: ds.ept,
        fige: true
      },
      element)
    let ch = stor.boulier.nbToEtat(stor.stockage[0])
    for (let i = ch.length / 2; i < ds.nbtiges; i++) ch = '00' + ch
    stor.boulier.affiche(ch)
    stor.zoneCorr = j3pAddElt(stor.conteneurD, 'div', '', me.styles.etendre('moyen.explications', { paddingTop: '30px' }))
    stor.zoneExpli = j3pAddElt(stor.conteneurD, 'div', '', { style: me.styles.petit.correction })
    j3pStyle(stor.zoneExpli, { paddingTop: '10px' })
    j3pFocus(stor.zoneInput)
    j3pRestriction(stor.zoneInput, '0-9 ')
    // console.log('réponse:', stor.stockage[0])
    me.finEnonce()
  }
  switch (me.etat) {
    case 'enonce':
      if (me.debutDeLaSection) {
        me.donneesSection = getDonnees()
        ds = me.donneesSection
        me.surcharge()
        if (ds.nbtiges > 8) ds.nbtiges = 8
        me.construitStructurePage({ structure: ds.structure, ratioGauche: 0.7 })
        stor.stockage = [0, 0, 0, 0]
        me.typederreurs = [0, 0, 0, 0, 0, 0]
        me.score = 0

        // me.videLesZones();
        me.afficheTitre(ds.textes.titre_exo)

        if (ds.indication) {
          me.indication(me.zonesElts.IG, ds.indication)
        }
      } else {
        me.videLesZones()
      }
      enonceMain()
      break // case "enonce":

    case 'correction':

      me.cacheBoutonValider()
      // on teste si une réponse a été saisie
      {
        const repEleve = j3pValeurde(stor.zoneInput)
        let pbEspaces = false
        if (!repEleve && !me.isElapsed) {
        // ne se produira jamais
          me.reponseManquante(stor.zoneCorr)
          j3pFocus(stor.zoneInput)

          me.afficheBoutonValider()
        } else { // une réponse a été saisie
          let cbon = repEleve === j3pNombreBienEcrit(stor.stockage[0])
          if (!cbon) {
            pbEspaces = repEleve === String(stor.stockage[0])
            if (!ds.espacesImposes) cbon = pbEspaces
          }
          if (cbon) {
            me.score++
            stor.zoneCorr.style.color = me.styles.cbien
            stor.zoneCorr.innerHTML = cBien
            me.typederreurs[0]++
            stor.zoneInput.valeur = j3pNombreBienEcrit(stor.stockage[0])
            stor.zoneInput.style.color = me.styles.cbien
            j3pDesactive(stor.zoneInput)
            j3pFreezeElt(stor.zoneInput)
            me.etat = 'navigation'
            me.sectionCourante()
          } else {
          // Pas de bonne réponse
            stor.zoneCorr.style.color = me.styles.cfaux
            stor.zoneInput.style.color = me.styles.cfaux
            // à cause de la limite de temps :
            if (me.isElapsed) { // limite de temps
              stor.zoneCorr.innerHTML = tempsDepasse
              me.typederreurs[10]++
              j3pDesactive(stor.zoneInput)
              j3pFreezeElt(stor.zoneInput)
              me.etat = 'navigation'
              me.sectionCourante()
            } else {
            // réponse fausse :
              stor.zoneCorr.innerHTML = cFaux
              if (pbEspaces) stor.zoneCorr.innerHTML += '<br/>' + ds.textes.comment1
              // S’il y a deux chances,on appelle à nouveau le bouton Valider
              if (me.essaiCourant < ds.nbchances) {
                stor.zoneCorr.innerHTML += '<br>' + essaieEncore
                me.typederreurs[1]++
                // indication éventuelle ici
                me.etat = 'correction'
                me.afficheBoutonValider()// c’est la partie "correction" de la section qui sera exécutée
              } else {
              // Erreur au second essai
                stor.zoneCorr.innerHTML += '<br>' + ds.textes.phrase3
                j3pAddContent(stor.zoneExpli, j3pChaine(ds.textes.phrase4, { n: j3pNombreBienEcrit(stor.stockage[0]) }))
                me.typederreurs[2]++
                j3pDesactive(stor.zoneInput)
                j3pBarre(stor.zoneInput)
                me.etat = 'navigation'
                me.sectionCourante()
              }
            }
          }
        }
      }
      me.finCorrection()
      break // case "correction":

    case 'navigation':
      if (me.sectionTerminee()) {
        me.parcours.pe = me.score / ds.nbitems
        me.afficheBoutonSectionSuivante()
        me.focus('sectioncontinuer')
      } else {
        me.etat = 'enonce'
        me.afficheBoutonSuite()
        me.focus('boutoncontinuer')
      }
      break // case "navigation":
  }
}
