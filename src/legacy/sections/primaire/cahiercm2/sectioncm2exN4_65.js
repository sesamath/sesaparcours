import $ from 'jquery'
import { j3pAddContent, j3pAddElt, j3pEmpty, j3pGetRandomInt, j3pNombreBienEcrit, j3pRandomdec } from 'src/legacy/core/functions'
import textesGeneriques from 'src/lib/core/textes'

const { cBien, cFaux } = textesGeneriques

/*
 * Auteur : Dominique Chiariello
 * Date : Janvier 2013
 * Modifications : JP Vanroyen
 */
export const params = {
  outils: ['calculatrice', 'mathquill'],
  /**
   * Ce qui est paramétrable dans l’éditeur de graphe
   * @type {LegacySectionParamsParametres}
   */
  parametres: [
    ['nbrepetitions', 5, 'entier', 'aucun'],
    ['indication', '', 'string', 'aucun'],
    ['limite', 0, 'entier', 'Temps disponible par répétition, en secondes'],
    ['nb_nombre', 7, 'entier', 'description'],
    ['nb_chiffre_maxi', 100000, 'entier', 'aucun'],
    ['nb_max_decimal', 4, 'entier', 'aucun'],
    ['negatif', true, 'boolean', 'presence de nombre negatif'],
    ['croissant', true, 'boolean', 'dans un ordre croissant']
  ]

}

/**
 * Ajouter une description ici
 * @fileOverview
 * @author Tommy Barroy <t.barroy@laposte.net>
 * @since 2017-10
*/
export default function main () {
  const me = this
  let ds = me.donneesSection
  const stor = me.storage
  function compare (a1, a2) { // compare 2 tableaux  de chaines de caracteres
    let a, b
    if (a1.length !== a2.length) { return false } else {
      let i
      for (i = 0; i <= a1.length - 1; i++) {
        a = j3pNombreBienEcrit(a1[i]); b = a2[i]
        if (!a.match(b)) { return false }
      }
    }
    return true
  }

  function getDonnees () {
    return {

      nbrepetitions: 5,
      nbetapes: 1,
      indication: '',
      seuil: 0.6,
      pe_1: 'ras',
      pe_2: 'cours à revoir',
      typesection: 'primaire',
      nb_nombre: 7,
      nb_chiffre_maxi: 100000,
      nb_max_decimal: 4,
      negatif: true, // relatif       true
      croissant: true, // décroissant   false
      textes: {
        titre_exo: 'Rangement de nombres - (N4 F6 ex5)',
        cons1_1: 'Classer les nombres en les déplaçant par ordre croissant.',
        cons1_2: 'Classer les nombres en les déplaçant par ordre décroissant.',
        donnees: 'Les données',
        tasol: 'Ta solution',
        laCorr: 'La correction'
      }
    }
  }

  async function init () {
    // il faut mettre jQuery en global avant de charger sortable
    window.jQuery = $
    await import('jquery-ui/ui/widgets/sortable')
    enonceMain()
  }

  function enonceMain () {
    stor.donne_hasard1 = []
    stor.donne_hasard2 = []
    stor.donne_utilisateur = []
    stor.donne_solution = []

    stor.zoneCorr = j3pAddElt(me.zonesElts.MD, 'div', '', { style: me.styles.etendre('petit.correction', { padding: '10px' }) })

    stor.conteneur = j3pAddElt(me.zonesElts.MG, 'div', '', { style: me.styles.etendre('petit.enonce', { padding: '10px' }) })
    for (let i = 1; i <= 2; i++) stor['zoneCons' + i] = j3pAddElt(stor.conteneur, 'div')
    j3pAddContent(stor.zoneCons1, (ds.croissant) ? ds.textes.cons1_1 : ds.textes.cons1_2)
    const table = j3pAddElt(stor.zoneCons2, 'table')
    const ligne = j3pAddElt(table, 'tr')
    const td = j3pAddElt(ligne, 'td', { align: 'right' })
    const ul = j3pAddElt(td, 'ul')

    let tempo
    const sortableJquery = $(ul)
    for (let i = 0; i < ds.nb_nombre; i++) {
      const ch = '[0;' + ds.nb_chiffre_maxi + '],' + ds.nb_max_decimal
      tempo = j3pRandomdec(ch)
      if (ds.negatif) {
        if (j3pGetRandomInt(0, 1)) tempo = -tempo
      }

      stor.donne_hasard1[i] = tempo
      stor.donne_hasard2[i] = tempo
      sortableJquery.append('<li>' + j3pNombreBienEcrit(tempo) + '</li>')
    }

    // creation de  stor.donne_solution
    stor.donne_solution = stor.donne_hasard2.sort(function (x, y) {
      return x - y
    })
    if (!ds.croissant) {
      stor.donne_solution = stor.donne_solution.reverse()
    }

    sortableJquery.sortable({ cursor: 'move' })
    $(me.zonesElts.MG)
      .find('li')
      .css({
        margin: '3px',
        'padding-right': '5px',
        border: '1px solid silver',
        width: '300px',
        'list-style-type': 'none'
      })
      .hover(
        function () {
          $(this).css('color', 'silver')
        },
        function () {
          $(this).css('color', 'black')
        }
      )

    me.finEnonce()
  }

  switch (me.etat) {
    case 'enonce':
      if (me.debutDeLaSection) {
        me.donneesSection = getDonnees()
        ds = me.donneesSection
        me.surcharge()
        me.construitStructurePage('presentation1')
        me.typederreurs = [0, 0, 0, 0, 0, 0]

        me.afficheTitre(ds.textes.titre_exo)

        if (ds.indication) me.indication(me.zonesElts.IG, ds.indication)
        // faut importer jquery d’abord puis jquery-ui/ui/widgets/sortable
        init()
      } else {
        me.videLesZones()
        enonceMain()
      }
      break // case "enonce":

    case 'correction': {
      // creation de  me.donne_utilisateur
      stor.donne_utilisateur = []
      for (const li of me.zonesElts.MG.querySelectorAll('li')) {
        stor.donne_utilisateur.push(li.innerText)
      }

      // comparaison de  stor.donne_utilisateur et stor.donne_solution

      const cbon = compare(stor.donne_solution, stor.donne_utilisateur)

      if (cbon) {
        stor.zoneCorr.style.color = me.styles.cbien
        stor.zoneCorr.innerHTML = cBien
        me.score++
        me.typederreurs[0]++
      } else {
        me.typederreurs[1]++
        stor.zoneCorr.style.color = me.styles.cfaux
        stor.zoneCorr.innerHTML = cFaux
        j3pEmpty(stor.zoneCons2)
        const table = j3pAddElt(stor.zoneCons2, 'table')
        const ligne1 = j3pAddElt(table, 'tr')
        j3pAddElt(ligne1, 'th', ds.textes.donnees)
        j3pAddElt(ligne1, 'th', ds.textes.tasol)
        j3pAddElt(ligne1, 'th', ds.textes.laCorr)
        const ligne2 = j3pAddElt(table, 'tr')
        const tabUl = []
        for (let i = 1; i <= 3; i++) {
          const td = j3pAddElt(ligne2, 'td')
          tabUl.push(j3pAddElt(td, 'ul'))
        }

        let tempo1 = 0
        let tempo3 = 0
        for (let i = 0; i < ds.nb_nombre; i++) {
          tempo1 = j3pNombreBienEcrit(stor.donne_hasard1[i])
          const tempo2 = stor.donne_utilisateur[i]
          tempo3 = j3pNombreBienEcrit(stor.donne_solution[i])

          $(tabUl[0]).append('<li>' + tempo1 + '</li>')
          $(tabUl[1]).append('<li>' + tempo2 + '</li>')
          if (tempo2 === tempo3) {
            $(tabUl[2]).append('<li>' + tempo3 + '</li>')
          } else {
            $(tabUl[2]).append('<li class="rouge">' + tempo3 + '</li>')
          }
        }
        $(me.zonesElts.MG)
          .find('li')
          .css({ margin: '3px', 'padding-right': '5px', border: '1px solid silver', 'list-style-type': 'none' })
      }
      me.finCorrection('navigation', true)
      break // case "correction":
    }

    case 'navigation':
      if (me.sectionTerminee()) {
        if (me.parcours.pe < ds.seuil) {
          me.parcours.pe = ds.pe_2
        } else {
          me.parcours.pe = ds.pe_1
        }
      } else {
        me.etat = 'enonce'
      }
      me.finNavigation()
      break // case "navigation":
  }
}
