import { j3pAddElt, j3pAjouteBouton, j3pBasculeAffichage, j3pDetruit, j3pGetNewId } from 'src/legacy/core/functions'
import { j3pAffiche } from 'src/lib/mathquill/functions'
import { j3pCreeTexte } from 'src/legacy/core/functionsSvg'
import textesGeneriques from 'src/lib/core/textes'

const { cBien, cFaux, essaieEncore, regardeCorrection, tempsDepasse } = textesGeneriques

// CODE SANS COMMENTAIRE

//
//        Bruno Meunier
//        Juin 2013
//        Remarques

//   Exemple de Graphe de test
//   j3p.html?graphe=[1,"modele",[{pe:">=0",nn:"2",conclusion:"Etape 2"}]];[2,"modele",[{pe:">=0",nn:"fin",conclusion:"fin"}]];

// NE PAS OUBLIER DE CHANGER LE NOM DE LA VARIABLE
export const params = {
  outils: ['mathquill'],
  /**
   * Ce qui est paramétrable dans l’éditeur de graphe
   * @type {LegacySectionParamsParametres}
   */
  parametres: [
    ['nbrepetitions', 4, 'entier', 'aucun'],
    ['indication', '', 'string', 'aucun'],
    ['limite', 0, 'entier', 'Temps disponible par répétition, en secondes'],
    ['nbchances', 2, 'entier', 'Nombre d’erreurs possibles (pour tenter de les corriger)'],
    ['graduation1', 200, 'entier', 'Abscisse de l’origine de l’axe affiché.'],
    ['pas', 100, 'entier', 'Pas d’une graduation'],
    ['etiquettes', 'non-consecutives', 'liste', 'Les deux étiquettes affichées se suivent ou non.', ['non-consecutives', 'consecutives']],
    ['etiquetteOrigine', false, 'boolean', 'La première étiquette affichée n’est pas l’origine de l’axe.']
  ]

}

/**
 * section
 * @this {Parcours}
 */
export default function main () {
  const me = this
  let ds = me.donneesSection
  const stor = me.storage
  function basculerAffichageAide () {
    j3pBasculeAffichage(stor.idDGoupeAide)
  }

  function afficheCorr () {
    j3pDetruit(stor.divBtnAide)
    stor.stockage.droiteG.groupeAide.style.visibility = 'hidden'

    stor.stockage.droiteG.groupeCorrection.style.display = 'block'
    stor.stockage.droiteG.groupeAide.style.display = 'none'

    stor.stockage.etiquetteReponse.setAttribute('opacity', '0.5')
    stor.stockage.rature.setAttribute('visibility', 'visible')
    stor.stockage.droiteG.etiquetteSoluce.setAttribute('visibility', 'visible')

    stor.stockage.droiteG.pastilleSoluce.setAttribute('fill', '#ff0000')
    stor.stockage.droiteG.gradSoluce.setAttribute('stroke', '#ff0000')
  }
  // retourne un objet qui trois props : conteneur,valeurSoluce, etiquetteSoluce
  const creerDroiteGraduee = function (graduation1, pas, etiquettes, etiquetteOrigine) {
    const largeurDroiteGraduee = 480
    const largeurGrad = largeurDroiteGraduee / 7
    const rayonPastille = 7
    const corps = 16
    const hauteurGrad = 20
    const margeGauche = 20
    const margeHaut = 20 + corps + 2 * rayonPastille + hauteurGrad
    const epaisseurDroite = 3
    const epaisseurGrad = 1
    let etiquetteSoluce
    let pastilleSoluce
    let gradSoluce
    let valeurSoluce
    let pastilleReponse
    let aEtiquetteVisible = []
    const aEtiquette = []
    const aGrad = []
    const aPastille = []
    // var pas  = 100;
    // var graduation1 = 50;
    // var etiquetteOrigine = false;
    // var etiquettes = "non-consecutives";

    // la droite
    const svgns = 'http://www.w3.org/2000/svg'
    const svgContainer = document.createElementNS(svgns, 'svg')
    svgContainer.setAttribute('width', largeurDroiteGraduee + 2 * margeGauche)
    svgContainer.setAttribute('height', 150)
    const d = document.createElementNS(svgns, 'line')
    const oStyleDroite = 'stroke: #666666;' + 'stroke-width:' + epaisseurDroite + ';'
    d.setAttribute('x1', margeGauche)
    d.setAttribute('y1', (margeHaut + epaisseurDroite / 2))
    d.setAttribute('x2', largeurDroiteGraduee + margeGauche)
    d.setAttribute('y2', (margeHaut + epaisseurDroite / 2))
    d.setAttribute('style', oStyleDroite)
    svgContainer.appendChild(d)
    const t = document.createElementNS(svgns, 'polygon')
    const aPoints = [
      largeurDroiteGraduee + margeGauche,
      margeHaut + epaisseurDroite / 2 - 10,
      largeurDroiteGraduee + margeGauche + 10,
      margeHaut + epaisseurDroite / 2,
      largeurDroiteGraduee + margeGauche,
      margeHaut + epaisseurDroite / 2 + 10,
      largeurDroiteGraduee + margeGauche,
      margeHaut + epaisseurDroite / 2 - 10
    ]
    const sPoints = aPoints.join(',')
    t.setAttribute('points', sPoints)
    t.setAttribute('style', 'fill:#666666;')
    svgContainer.appendChild(t)

    // choix des etiquettes à afficher et de la garduation rouge
    let graduationCible = 0
    let aleaA, aleaB
    if (etiquetteOrigine && etiquettes === 'consecutives') {
      aEtiquetteVisible = [0, 1]
      graduationCible = 3 + Math.floor(Math.random() * (6 - 3 + 1))
    } else if (etiquetteOrigine && etiquettes === 'non-consecutives') {
      aleaA = 1 + Math.floor(Math.random() * (6 - 1 + 1))
      aEtiquetteVisible = [0, aleaA]
      while (aEtiquetteVisible.indexOf(graduationCible) > -1) {
        graduationCible = 1 + Math.floor(Math.random() * (6 - 1 + 1))
      }
    } else if (!etiquetteOrigine && etiquettes === 'consecutives') {
      aleaA = 1 + Math.floor(Math.random() * (6 - 1 + 1))
      aEtiquetteVisible = [aleaA, aleaA + 1]
      while (graduationCible === 0 || aEtiquetteVisible.indexOf(graduationCible) > -1) {
        graduationCible = 1 + Math.floor(Math.random() * (6 - 1 + 1))
      }
    } else if (!etiquetteOrigine && etiquettes === 'non-consecutives') {
      aleaA = 1 + Math.floor(Math.random() * (3 - 1 + 1))
      aleaB = aleaA + 2 + Math.floor(Math.random() * (6 - (aleaA + 2) + 1))
      aEtiquetteVisible = [aleaA, aleaB]
      while (graduationCible === 0 || aEtiquetteVisible.indexOf(graduationCible) > -1) {
        graduationCible = 1 + Math.floor(Math.random() * (6 - 1 + 1))
      }
    }

    const groupeAide = document.createElementNS(svgns, 'g')
    stor.idDGoupeAide = j3pGetNewId('groupeAide')
    groupeAide.setAttribute('id', stor.idDGoupeAide)
    const groupeCorrection = document.createElementNS(svgns, 'g')
    let i, prop, oStyleLabel
    for (i = 0; i < 7; i++) {
      // les pastilles
      const pastille = document.createElementNS(svgns, 'circle')
      const oAttribute = {
        cx: i * largeurGrad + margeGauche,
        cy: 20 + corps + rayonPastille + 5,
        r: rayonPastille,
        style: 'fill:#666666;',
        cursor: 'pointer'
      }
      for (const attr in oAttribute) {
        pastille.setAttribute(attr, oAttribute[attr])
      }
      aPastille.push(pastille)
      // les graduations
      const oStyleGrad = {
        stroke: '#666666',
        'stroke-width': epaisseurGrad
      }

      const grad = document.createElementNS(svgns, 'line')
      grad.setAttribute('x1', i * largeurGrad + margeGauche)
      grad.setAttribute('y1', 20 + corps + 2 * rayonPastille + 5)
      grad.setAttribute('x2', i * largeurGrad + margeGauche)
      grad.setAttribute('y2', margeHaut)
      for (const prop in oStyleGrad) {
        grad.setAttribute(prop, oStyleGrad[prop])
      }
      if (etiquetteOrigine || i > 0) {
        svgContainer.appendChild(pastille)
        svgContainer.appendChild(grad)
      }
      if (i === graduationCible) {
        valeurSoluce = graduation1 + i * pas
        pastilleSoluce = pastille
        gradSoluce = grad
      }

      aGrad.push(grad)
      // les etiquettes
      oStyleLabel = {
        'font-family': 'Arial',
        'font-size': '16',
        fill: '#000000',
        'text-anchor': 'middle',
        visibility: 'hidden'
      }
      const label = document.createElementNS(svgns, 'text')
      const texte = document.createTextNode(graduation1 + i * pas)
      label.appendChild(texte)
      // var largeurEtiquette = label.offsetWidth // Supprimé par Yves. Inutile
      label.setAttribute('x', i * largeurGrad + margeGauche)
      label.setAttribute('y', 20 + corps)
      for (prop in oStyleLabel) {
        label.setAttribute(prop, oStyleLabel[prop])
      }
      svgContainer.appendChild(label)
      if (aEtiquetteVisible.indexOf(i) > -1) {
        label.setAttribute('visibility', 'visible')
      }

      if (i === graduationCible) {
        label.setAttribute('fill', '#ff0000')
        label.setAttribute('font-weight', 'bold')
        label.setAttribute('font-size', '22px')
        etiquetteSoluce = label
      }
      aEtiquette.push(label)
      // l’aide
      if (i >= aEtiquetteVisible[0] && i < aEtiquetteVisible[1]) {
        creerFlechePlus('#3D93C6', groupeAide)
      }
      // la correction
      if (
        graduationCible < aEtiquetteVisible[0] &&
                i >= graduationCible &&
                i < aEtiquetteVisible[0]) {
        creerFlecheMoins('#ff0000', groupeCorrection)
      } else if (
        graduationCible < aEtiquetteVisible[1] &&
                graduationCible <= aEtiquetteVisible[0] / 2 + aEtiquetteVisible[1] / 2 &&
                i >= aEtiquetteVisible[0] &&
                i < graduationCible) {
        creerFlechePlus('#ff0000', groupeCorrection)
      } else if (
        graduationCible < aEtiquetteVisible[1] &&
                graduationCible > aEtiquetteVisible[0] / 2 + aEtiquetteVisible[1] / 2 &&
                i < aEtiquetteVisible[1] &&
                i >= graduationCible) {
        creerFlecheMoins('#ff0000', groupeCorrection)
      } else if (
        graduationCible >= aEtiquetteVisible[1] &&
                i >= aEtiquetteVisible[1] &&
                i < graduationCible) {
        creerFlechePlus('#ff0000', groupeCorrection)
      }
    }
    svgContainer.appendChild(groupeAide)
    svgContainer.appendChild(groupeCorrection)
    groupeAide.style.display = 'none'
    groupeCorrection.style.display = 'none'

    function creerFlechePlus (sCouleur, groupeConteneur) {
      const marqueur = document.createElementNS(svgns, 'marker')
      let oAttr = {
        id: 'tr-' + sCouleur.substr(1) + '-' + i,
        viewBox: '0 0 10 10',
        refX: '0',
        refY: '5',
        markerWidth: '10',
        markerHeight: '10',
        orient: 'auto'
      }
      for (const prop in oAttr) {
        marqueur.setAttribute(prop, oAttr[prop])
      }
      const pointe = document.createElementNS(svgns, 'path')
      oAttr = {
        d: 'M 0 0 L 10 5 L 0 10 z',
        fill: sCouleur
      }
      for (prop in oAttr) {
        pointe.setAttribute(prop, oAttr[prop])
      }
      marqueur.appendChild(pointe)
      groupeConteneur.appendChild(marqueur)

      const x1 = i * largeurGrad + margeGauche
      const y1 = margeHaut + 5
      const x2 = largeurGrad / 2
      const y2 = 40
      const x3 = largeurGrad - 6
      const y3 = 5
      const oStyleCourbe = {
        stroke: sCouleur,
        fill: 'transparent',
        d: 'M' + x1 + ' ' + y1 + ' q ' + x2 + ' ' + y2 + ' ' + x3 + ' ' + y3,
        'marker-end': 'url(#tr-' + sCouleur.substr(1) + '-' + i + ')'
      }
      const courbe = document.createElementNS(svgns, 'path')
      for (prop in oStyleCourbe) {
        courbe.setAttribute(prop, oStyleCourbe[prop])
      }
      groupeConteneur.appendChild(courbe)
      const label = j3pCreeTexte(groupeConteneur, { id: 'text' + i, texte: '+ ' + pas, x: x1 + largeurGrad / 2, y: 120, couleur: sCouleur, taille: 12 })
      label.setAttribute('font-family', 'Arial')
      label.setAttribute('text-anchor', 'middle')
    }

    function creerFlecheMoins (sCouleur, groupeConteneur) {
      const marqueur = document.createElementNS(svgns, 'marker')
      let oAttr = {
        id: 'tr-' + sCouleur.substr(1) + '-' + i,
        viewBox: '0 0 10 10',
        refX: '0',
        refY: '5',
        markerWidth: '10',
        markerHeight: '10',
        orient: 'auto'
      }
      for (prop in oAttr) {
        marqueur.setAttribute(prop, oAttr[prop])
      }
      const pointe = document.createElementNS(svgns, 'path')
      oAttr = {
        d: 'M 10 0 L 10 10 L 0 5 z',
        fill: sCouleur
      }
      for (prop in oAttr) {
        pointe.setAttribute(prop, oAttr[prop])
      }
      marqueur.appendChild(pointe)
      groupeConteneur.appendChild(marqueur)

      const x1 = i * largeurGrad + margeGauche
      const y1 = margeHaut + 5
      const x2 = largeurGrad / 2
      const y2 = 40
      const x3 = largeurGrad
      const y3 = 5
      const oStyleCourbe = {
        stroke: sCouleur,
        fill: 'transparent',
        d: 'M' + x1 + ' ' + y1 + ' q ' + x2 + ' ' + y2 + ' ' + x3 + ' ' + y3,
        'marker-start': 'url(#tr-' + sCouleur.substr(1) + '-' + i + ')'
      }
      const courbe = document.createElementNS(svgns, 'path')
      for (prop in oStyleCourbe) {
        courbe.setAttribute(prop, oStyleCourbe[prop])
      }
      groupeConteneur.appendChild(courbe)
      const label = j3pCreeTexte(groupeConteneur, { id: 'text' + i, texte: '- ' + pas, x: x1 + largeurGrad / 2, y: 120, couleur: sCouleur, taille: 12 })
      label.setAttribute('font-family', 'Arial')
      label.setAttribute('text-anchor', 'middle')
    }

    // gestion du click sur les pastilles
    let labelEleve
    // je veux récupérer les pastilles pour les rendre inactives à la correction
    stor.pastillesActives = true
    for (i = 0; i < aPastille.length; i++) {
      aPastille[i].addEventListener('click', gestionClickPastille, false)
    }

    function gestionClickPastille (e) {
      // on passe les graduations en gris on enleve l’étiquette eleve
      if (!stor.pastillesActives) return
      const indice = aPastille.indexOf(e.target)
      for (let j = 0; j < aEtiquette.length; j++) {
        if (aEtiquetteVisible.indexOf(j) < 0) {
          aPastille[j].setAttribute('fill', '#666666')
          aGrad[j].setAttribute('stroke', '#666666')
          aEtiquette[j].setAttribute('visibility', 'hidden')
          if (labelEleve !== undefined) {
            labelEleve.remove()
          }
        }
      }
      // on passe la graduation cliquee en orange on affiche la valeur demandee
      if (aEtiquetteVisible.indexOf(indice) < 0) {
        aPastille[indice].setAttribute('fill', '#ff9c00')
        aGrad[indice].setAttribute('stroke', '#ff9c00')

        labelEleve = document.createElementNS(svgns, 'text')
        const texte = document.createTextNode(valeurSoluce)
        stor.stockage[1] = graduation1 + indice * pas
        stor.stockage.etiquetteReponse = labelEleve
        labelEleve.appendChild(texte)
        // var largeurEtiquette = label.offsetWidth  // Inutile supprimé par Yves le 9/4/2020
        labelEleve.setAttribute('x', indice * largeurGrad + margeGauche)
        labelEleve.setAttribute('y', 20 + corps)
        for (prop in oStyleLabel) {
          labelEleve.setAttribute(prop, oStyleLabel[prop])
        }
        svgContainer.appendChild(labelEleve)
        labelEleve.setAttribute('fill', '#ff9c00')
        labelEleve.setAttribute('font-weight', 'bold')
        labelEleve.setAttribute('font-size', '22px')
        labelEleve.setAttribute('visibility', 'visible')

        const rature = document.createElementNS(svgns, 'line')
        /* console.log('labelEleve.offsetLeft', labelEleve.offsetLeft, labelEleve)
        console.log('labelEleve', labelEleve) */
        const bb = labelEleve.getBBox()
        const oAttribut = {
          x1: String(bb.x) + 'px',
          y1: String(bb.y) + 'px',
          x2: String(bb.x + bb.width) + 'px',
          y2: String(bb.y + bb.height + 5) + 'px',
          style: 'stroke: #ff0000;stroke-width: 5;stroke-linecap: round;opacity: 0.5'
        }
        for (prop in oAttribut) {
          rature.setAttribute(prop, oAttribut[prop])
        }
        svgContainer.appendChild(rature)
        rature.setAttribute('visibility', 'hidden')
        stor.stockage.rature = rature
        pastilleReponse = e.target
        // Modifié par Yves le 9/4/2020
        // var etiquetteReponse = stor.stockage.droiteG.aEtiquette[indice]
        stor.stockage.etiquetteReponse = stor.stockage.droiteG.aEtiquette[indice]
      } else {
        pastilleReponse = undefined
      }
    }

    return {
      conteneur: svgContainer,
      valeurSoluce,
      etiquetteSoluce,
      gradSoluce,
      pastilleSoluce,
      pastilleReponse,
      aEtiquette,
      aEtiquetteVisible,
      aPastille,
      aGrad,
      groupeAide,
      groupeCorrection
    }
  }

  function getDonnees () {
    return {
      typeme: 'primaire',

      nbrepetitions: 2,
      nbetapes: 1,
      indication: '',
      nbchances: 2,
      structure: 'presentation1', //  || "presentation2" || "presentation3"  || "presentation1bis"
      // Paramètres de la me, avec leurs valeurs par défaut.
      graduation1: 200,
      pas: 100,
      etiquettes: 'non-consecutives',
      etiquetteOrigine: false,
      affichageAide: 'manuel',

      textes: {
        titre_exo: 'Cliquer sur la graduation qui convient - (N1 F3 ex3)',
        phrase1: 'Clique sur la graduation qui correspond au nombre £n.',
        aide: 'Aide'
      },

      // Par défaut, pas de temps limite. Décommentez pour avoir un temps limite par défaut. En secondes.
      // limite =5;

      pe: 0
    }
  }
  function enonceMain () {
  // Mon code
    stor.conteneur = j3pAddElt(me.zonesElts.MG, 'div', '', { style: me.styles.etendre('petit.enonce', { padding: '25px' }) })
    // La consigne
    const zoneCons1 = j3pAddElt(stor.conteneur, 'div')
    const zoneCons2 = j3pAddElt(stor.conteneur, 'div')
    // Creation de la droite graduee
    stor.stockage.droiteG = creerDroiteGraduee(
      ds.graduation1,
      ds.pas,
      ds.etiquettes,
      ds.etiquetteOrigine
    )

    stor.stockage[0] = stor.stockage.droiteG.valeurSoluce
    zoneCons2.appendChild(stor.stockage.droiteG.conteneur)
    zoneCons2.paddingTop = '30px'
    // Gestion du click sur les graduations

    j3pAffiche(zoneCons1, '', ds.textes.phrase1, { n: stor.stockage[0] })

    stor.zoneD = j3pAddElt(me.zonesElts.MD, 'div', '', { style: me.styles.etendre('moyen.explications', { padding: '20px' }) })
    stor.divBtnAide = j3pAddElt(stor.zoneD, 'div')
    j3pAjouteBouton(stor.divBtnAide, 'boutonAide', 'MepBoutons', ds.textes.aide, basculerAffichageAide)
    stor.zoneCorr = j3pAddElt(stor.zoneD, 'div', '', { style: me.styles.etendre('petit.correction', { padding: '10px' }) })

    me.finEnonce()
  }
  switch (me.etat) {
    case 'enonce':
      // code exécuté au lancement de la me
      if (me.debutDeLaSection) {
        me.donneesSection = getDonnees()
        ds = me.donneesSection
        me.surcharge()
        me.construitStructurePage({ structure: ds.structure, ratioGauche: 0.6 })
        stor.stockage = [0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0]
        me.typederreurs = [0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0]
        me.score = 0
        me.afficheTitre(ds.textes.titre_exo)

        if (ds.indication) me.indication(me.zonesElts.IG, ds.indication)
      } else {
        me.videLesZones()
      }

      enonceMain()
      break // case "enonce":

    case 'correction':

      // On teste si une réponse a été saisie
      {
        const repEleve = stor.stockage[1]
        // console.log(stor.stockage.droiteG)
        if (!repEleve) {
          me.reponseManquante(stor.zoneCorr)
          // j3pFocus("reponse1");
          me.afficheBoutonValider()
        } else {
        // Une réponse a été saisie
          if (repEleve === stor.stockage[0]) {
          // Bonne réponse
            me._stopTimer()
            me.score++
            stor.zoneCorr.style.color = me.styles.cbien
            stor.zoneCorr.innerHTML = cBien
            stor.pastillesActives = false
            me.typederreurs[0]++
            me.cacheBoutonValider()
            me.etat = 'navigation'
            me.sectionCourante()
          } else {
          // Pas de bonne réponse
            stor.zoneCorr.style.color = me.styles.cfaux

            // A cause de la limite de temps :
            if (me.isElapsed) { // limite de temps
              me._stopTimer()

              stor.zoneCorr.innerHTML = tempsDepasse
              me.typederreurs[10]++
              me.cacheBoutonValider()
              afficheCorr()
              // RECOPIER LA CORRECTION ICI !
              stor.pastillesActives = false
              me.etat = 'navigation'
              me.sectionCourante()
            } else {
            // Réponse fausse :
              stor.zoneCorr.innerHTML = cFaux

              // S’il y a plus de deux chances,on appelle à nouveau le bouton Valider
              if (me.essaiCourant < ds.nbchances) {
                stor.zoneCorr.innerHTML += '<br>' + essaieEncore
                me.typederreurs[1]++
              // indication éventuelle ici
              } else {
              // Erreur au nème essai
                me._stopTimer()
                me.cacheBoutonValider()
                stor.pastillesActives = false
                stor.zoneCorr.innerHTML += '<br>' + regardeCorrection
                me.typederreurs[2]++
                afficheCorr()

                me.etat = 'navigation'
                me.sectionCourante()
              }
            }
          }
        }
      }
      me.finCorrection()
      break // case "correction":

    case 'navigation':

      if (me.sectionTerminee()) {
        // On détermine la phrase d’état renvoyée par la me
        me.parcours.pe = me.score / ds.nbitems
        me.afficheBoutonSectionSuivante()
        me.focus('sectioncontinuer')
      } else {
        me.etat = 'enonce'
        me.afficheBoutonSuite()
        me.focus('boutoncontinuer')
      }
      me.finNavigation()

      break // case "navigation":
  }
}
