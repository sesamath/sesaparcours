import { j3pAddContent, j3pAddElt, j3pAddTxt, j3pArrondi, j3pBarre, j3pEmpty, j3pFocus, j3pFreezeElt, j3pGetRandomFloat, j3pGetRandomInt, j3pShowError, j3pStyle, j3pValeurde, j3pVirgule } from 'src/legacy/core/functions'
import { j3pAffiche, j3pDesactive, mqRestriction } from 'src/lib/mathquill/functions'
import textesGeneriques from 'src/lib/core/textes'

const { cBien, cFaux, essaieEncore, regardeCorrection, tempsDepasse } = textesGeneriques

/*
    Auteur : Rémi ANGOT
    Date : 13/01/2013
    Version : 0.1
    Reste à faire : valider la réponse en appuyant sur entrée, scénario à améliorer, correction, type d’erreur 3,1<3,10
    indications...
    Section adaptée au clavier virtuel par Yves en 10-2021
*/

export const params = {
  outils: ['calculatrice', 'mathquill'],
  /**
   * Ce qui est paramétrable dans l’éditeur de graphe
   * @type {LegacySectionParamsParametres}
   */
  parametres: [
    ['nbrepetitions', 5, 'entier', 'aucun'],
    ['indication', '', 'string', 'aucun'],
    ['limite', 0, 'entier', 'Temps disponible par répétition, en secondes'],
    ['nbchances', 2, 'entier', 'Nombre d’essai(s) possible(s)']
  ]
}

/**
 * Ajouter une description ici
 * @fileOverview
 * @author Tommy Barroy <t.barroy@laposte.net>
 * @since 2017-10
*/
export default function main () {
  const me = this
  let ds = me.donneesSection
  const stor = me.storage
  function getDonnees () {
    return {

      typesection: 'primaire',
      nbrepetitions: 5,
      nbetapes: 1,
      indication: '',
      nbchances: 2,
      unparametre: 0,
      structure: 'presentation1',
      nbdecimales: '[0;2]',
      partie_entiere: '[1;100]',
      textes: {
        titre_exo: 'Intercaler un nombre décimal - (N4 F7 ex6)',
        phrase1: 'Complète avec un nombre décimal :',
        pbSyntaxe: 'Faute de syntaxe'
      }
    }
  }
  function enonceMain () {
    const typeQuestion = j3pGetRandomInt(1, 3)
    // type 1 il y a un écart de 2 ou 3 entre les 2 derniers chiffres
    // type 2 il y a un écart de 1 entre les 2 chiffres
    // type 3 un des nombres a une décimale de plus
    let min = 0
    let max = 2
    let chunks = /^\[([-0-9]);([-0-9])]$/.exec(ds.nbdecimales)
    if (chunks) {
      min = Number(chunks[1])
      max = Number(chunks[2])
    } else {
      j3pShowError('Paramètre nbdecimales invalide : ' + ds.nbdecimales + ' => [0;2] imposé')
    }
    const nbDecimales = (min === max) ? min : j3pGetRandomInt(min, max)
    // idem pour partie_entiere
    chunks = /^\[([-0-9]+);([-0-9]+)]$/.exec(ds.partie_entiere)
    if (chunks) {
      min = Number(chunks[1])
      max = Number(chunks[2])
    } else {
      j3pShowError('Paramètre partie_entiere invalide : ' + ds.partie_entiere + ' => [1;100] imposé')
      min = 1
      max = 100
    }
    let a, b

    if (nbDecimales === 0) {
      a = j3pGetRandomInt(min, max) // borne inférieure
      b = a + 1 // borne supérieure
    } else {
      const ordre = Math.pow(10, nbDecimales)
      // un float entre min et max avec nbDecimales
      a = j3pArrondi(j3pGetRandomFloat(min, max), nbDecimales)
      switch (typeQuestion) {
        case 1 :
          b = j3pArrondi(a + j3pGetRandomInt(2, 3) / ordre, nbDecimales)
          break
        case 2 :
          b = j3pArrondi(a + 1 / ordre, nbDecimales)
          break
        case 3 :
          b = j3pArrondi(a + j3pGetRandomInt(1, 9) / ordre, nbDecimales)
          break
      }
    }

    stor.min = a
    stor.max = b
    stor.exempleBonneReponse = j3pVirgule(j3pArrondi(a + (b - a) / 2, nbDecimales + 1))
    stor.conteneur = j3pAddElt(me.zonesElts.MG, 'div', '', { style: me.styles.etendre('petit.enonce', { padding: '25px' }) })
    j3pAddElt(stor.conteneur, 'p', ds.textes.phrase1)
    const zoneCons2 = j3pAddElt(stor.conteneur, 'div')
    j3pStyle(zoneCons2, { paddingTop: '20px', paddingLeft: '50px' })
    const elt = j3pAffiche(zoneCons2, '', '£x < &1& < £y', {
      x: j3pVirgule(a),
      y: j3pVirgule(b),
      inputmq1: { texte: '' }
    })
    stor.a = a
    stor.b = b
    stor.zoneInput = elt.inputmqList[0]
    mqRestriction(stor.zoneInput, '\\d,.')
    j3pFocus(stor.zoneInput)
    stor.zoneCorr = j3pAddElt(me.zonesElts.MD, 'div', '', { style: me.styles.etendre('petit.correction', { padding: '10px' }) })
    me.finEnonce()
  }
  switch (me.etat) {
    case 'enonce':
      if (me.debutDeLaSection) {
        me.donneesSection = getDonnees()
        ds = me.donneesSection
        me.surcharge()
        me.construitStructurePage({ structure: ds.structure, ratioGauche: 0.6 })
        me.afficheTitre(ds.textes.titre_exo)
        if (ds.indication) me.indication(me.zonesElts.IG, ds.indication)
      } else {
        me.videLesZones()
      }
      // ecriture de l’énoncé
      enonceMain()
      break // case "enonce":

    case 'correction': {
      const ch = j3pValeurde(stor.zoneInput)
      const test = ch.split(',')
      const fauteSyntaxe = test.length > 2
      const rep = (ch === '' || test.length > 2) ? null : Number(ch.replace(',', '.'))
      // rep est un number ou null
      const cbon = rep && rep > stor.min && rep < stor.max
      stor.zoneCorr.style.color = cbon ? me.styles.cbien : me.styles.cfaux
      // on teste si une réponse a été saisie
      // le cas pas de réponse sans temps dépassé vite expédié
      if (!rep && !me.isElapsed) {
        if (!fauteSyntaxe) me.reponseManquante(stor.zoneCorr)
        else stor.zoneCorr.innerHTML = ds.textes.pbSyntaxe
        j3pFocus(stor.zoneInput)
        // on reste sur l’état correction et on arrête là
        return
      }
      // le cas réponse ko avec encore une chance où on reste en correction
      j3pEmpty(stor.zoneCorr)
      if (rep && !cbon && me.essaiCourant < ds.nbchances) {
        j3pAddContent(stor.zoneCorr, '<br>' + essaieEncore) // y’a du html dedans…
        j3pFocus(stor.zoneInput)
        return
      }
      // pour tout le reste on affiche un feedback et on passera en navigation avec le bouton suite

      me.etat = 'navigation'
      me.afficheBoutonSuite(true)
      // le feedback
      if (cbon) {
        me.score++
        j3pAddTxt(stor.zoneCorr, cBien)
        stor.zoneInput.style.color = me.styles.cbien
        j3pDesactive(stor.zoneInput)
        j3pFreezeElt(stor.zoneInput)
      } else {
        // Pas de bonne réponse (et c'était la dernière chance)
        stor.zoneInput.style.color = me.styles.cfaux
        if (me.isElapsed) {
          // à cause de la limite de temps :
          j3pAddTxt(stor.zoneCorr, tempsDepasse)
          j3pAddElt(stor.zoneCorr, 'br')
        } else {
          j3pAddTxt(stor.zoneCorr, cFaux)
        }
        j3pAddContent(stor.zoneCorr, '<br>' + regardeCorrection) // y’a du html dedans…
        // on corrige ici
        j3pDesactive(stor.zoneInput)
        if (rep) j3pBarre(stor.zoneInput)
        const zoneSol = j3pAddElt(stor.conteneur, 'div', { style: me.styles.petit.correction })
        zoneSol.style.paddingLeft = '50px'
        j3pAffiche(zoneSol, '', '£x < £z < £y', { x: j3pVirgule(stor.a), y: j3pVirgule(stor.b), z: stor.exempleBonneReponse })
      }
    }
      me.finCorrection()
      break // case "correction":

    case 'navigation':
      if (me.sectionTerminee()) {
        me.parcours.pe = me.score / ds.nbitems
        me.afficheBoutonSectionSuivante()
        me.focus('sectioncontinuer')
      } else {
        me.etat = 'enonce'
        me.sectionCourante()
      }
      break // case "navigation":
  }
}
