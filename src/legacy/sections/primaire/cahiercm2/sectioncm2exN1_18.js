import $ from 'jquery'
import { j3pAddElt, j3pAjouteDans, j3pArrondi, j3pBarre, j3pElement, j3pFindpos, j3pFocus, j3pGetPositionZone, j3pGetRandomElt, j3pRandomdec, j3pStyle } from 'src/legacy/core/functions'

import { j3pAffiche, j3pDesactive } from 'src/lib/mathquill/functions'
import textesGeneriques from 'src/lib/core/textes'
import { formateNombre } from 'src/legacy/outils/fonctions/ecritureNombre'

const { cBien, cFaux, essaieEncore, regardeCorrection, tempsDepasse } = textesGeneriques

/*
Section modifiée par Yves en novembre 2017 : Ne gérait pas bien le temps limite (pas de solution si temps dépassé)
Risque de boucle infinie
Paramètre typeNombres remplacé par nbDecimales
 */

const defaultRaisons = [100, 200]
const defaultMotifs = [false, true, true, false, false, false]

export const params = {
  outils: ['calculatrice'],
  /**
   * Ce qui est paramétrable dans l’éditeur de graphe
   * @type {LegacySectionParamsParametres}
   */
  parametres: [
    ['nbrepetitions', 5, 'entier', 'Nombre de répétitions de la section'],
    ['indication', '', 'string', 'Indication éventuelle'],
    ['limite', 0, 'entier', 'Temps disponible par répétition, en secondes'],
    ['nbchances', 2, 'entier', 'Nombre d’essais par répétition'],
    ['motifs', defaultMotifs, 'array', 'Les termes de la suite qu’il faut trouver (le nombre est affiché si true et il faut le trouver si false). Ce tableau peut être réduit à 3, 4 ou 5 élèments, il doit comporter deux true consécutifs'],
    ['n', '[100;999]', 'intervalle', 'l’intervalle d’appartenance du premier terme de la suite. Peut être négatif.'],
    ['raisons', defaultRaisons, 'array', 'les différentes raisons possibles. Nombres décimaux et nombres négatifs possibles.'],
    // ["nbDecimales", [0, false], 'array', '[nombre de décimales,false||true], false pour avoir des nombres positifs. ']
    ['nbDecimales', 0, 'entier', 'nombre de décimales.']
  ]
}

const textes = {
  titre_exo: 'Suite de nombres entiers - (N1 F1 ex8)',
  phrase1: 'Observe et complète la suite de nombres : ',
  phrase2: '</p> Mais un nombre est mal écrit !',
  phrase3: '</p>Mais il y a des nombres  mal écrits !',
  phrase4: 'Il faut compléter toutes les cases ! ',
  phrase5: 'Vous devez donner au moins deux termes consécutifs de la suite !'
}

/**
 * section cm2exN1_18
 * @this {Parcours}
 */
export default function main () {
  const me = this
  const ds = me.donneesSection
  const stor = me.storage

  /**
   * ex fct j3pCorrige un peu simplifiée (c'était la seule section à l’utiliser)
   * @param {HTMLElement} container
   * @param texte
   * @param left
   * @param top
   * @returns {HTMLDivElement}
   */
  function corrige (container, texte, left, top) {
    const pos = { x: 0, y: 0 }
    const pos2 = j3pFindpos(container) // contient les coordonnées par rapport à Mepact

    pos.x = pos2.x
    pos.y = pos2.y // - 115;//hauteur de HG
    let elementparent = container.parentNode
    // on cherche l’ancetre MepMG ou MepMD
    let elementparent1 = elementparent
    while (elementparent1 && elementparent1.id !== 'MepMG' && elementparent1.id !== 'MepMD') {
      elementparent1 = elementparent1.parentNode
    }
    // et le 1er div (ou elt sans id) parent
    while (elementparent && (!elementparent.id || elementparent.tagName !== 'DIV')) {
      elementparent = elementparent.parentNode
    }
    // si le parent, par rapport auquel sera calculée la position de la barre n’a pas de position, le positionnement de la barre
    // ne fonctionnera pas
    if (elementparent && !elementparent.style.position) {
      elementparent.style.position = 'relative'
    }

    const tab = j3pGetPositionZone(container, elementparent)
    pos.x = tab[0]
    pos.y = tab[1]

    const props = {
      style: {
        color: '#FF0000',
        position: 'absolute',
        // on peut laisser number pour ces propriétés, j3pAjouteDiv ajoutera px
        top: pos.y + top,
        left: pos.x + left,
        boxShadow: '0 0 0'
      }
    }
    return j3pAddElt(elementparent, 'div', texte, props)
  }

  function fleche (idZone, texte, position) {
    let posX = parseFloat(j3pElement(idZone).offsetLeft) + parseFloat(j3pElement(idZone).offsetWidth / 2)// offset à adapter pour les autres navigateurs
    let posY = parseFloat(j3pElement(idZone).offsetTop)// seul fireFox semble l’interpreter
    let longueur = parseFloat(j3pElement(idZone).offsetWidth) - 10

    let textPosX, pas1, pas2, textPosY, styleSvg, couleur
    // postion et sens de la flèche
    if (position === 'basD') {
      textPosX = 0
      pas1 = 10
      pas2 = 5
      posY = posY + parseFloat(3 * j3pElement(idZone).offsetHeight / 2) - 5
      textPosY = 2 * (pas1 + pas2)
      styleSvg = 'style="fill:none;stroke:green; stroke-width:2px"'
      couleur = 'fill:green'
    } else if (position === 'basDR') {
      textPosX = 0
      pas1 = 10
      pas2 = 5
      posX = posX + 10
      posY = posY + parseFloat(3 * j3pElement(idZone).offsetHeight / 2) - 5
      textPosY = 2 * (pas1 + pas2)
      styleSvg = 'style="fill:none;stroke:red; stroke-width:2px"'
      couleur = 'fill:red'
    } else if (position === 'basG') {
      textPosX = -longueur + 15
      longueur = -1 * parseFloat(j3pElement(idZone).offsetWidth)
      pas1 = 10
      pas2 = 5
      textPosY = 2 * (pas1 + pas2)
      posY = posY + parseFloat(3 * j3pElement(idZone).offsetHeight / 2) - 5
      styleSvg = 'style="fill:none;stroke:green; stroke-width:2px"'
      couleur = 'fill:green'
    } else if (position === 'basGR') {
      textPosX = -longueur
      longueur = -1 * parseFloat(j3pElement(idZone).offsetWidth)
      pas1 = 10
      pas2 = 5
      textPosY = 2 * (pas1 + pas2)
      posY = posY + parseFloat(3 * j3pElement(idZone).offsetHeight / 2) - 5
      posX = posX - 10
      styleSvg = 'style="fill:none;stroke:red; stroke-width:2px"'
      couleur = 'fill:red'
    } else if (position === 'hautGV') {
      textPosX = -longueur
      longueur = -1 * parseFloat(j3pElement(idZone).offsetWidth)
      pas1 = -10
      pas2 = -5
      posY = posY - 5
      textPosY = (pas1 + pas2)
      styleSvg = 'style="fill:none;stroke:green; stroke-width:2px"'
      couleur = 'fill:green'
    }
    let svgTxt = '<line x1="' + posX + '" y1="' + posY + '" x2="' + posX + '" y2="' + posY + '" ' + styleSvg + '>' +
      '<animate attributeName="y2" attributeType="XML" id="' + idZone + 's1"' +
      'from="' + posY + '" to="' + parseFloat(posY + pas1) + '" begin="100ms" dur="300ms"  fill="freeze"/></line>' +
      '<line x1="' + posX + '" y1="' + parseFloat(posY + pas1) + '" x2="' + posX + '" y2="' + parseFloat(posY + pas1) + '" ' + styleSvg + '>' +
      '<animate attributeName="x2" attributeType="XML" id="' + idZone + 's2"' +
      'from="' + posX + '" to="' + parseFloat(posX + longueur) + '" begin="' + idZone + 's1.end" dur="500ms" fill="freeze" /> </line>' +
      '<line x1="' + parseFloat(posX + longueur) + '" y1="' + parseFloat(posY + pas1) + '" x2="' + parseFloat(posX + longueur) + '" y2="' + parseFloat(posY + pas1) + '" ' + styleSvg + '>' +
      '<animate attributeName="y2" attributeType="XML" id="' + idZone + 's3"' +
      'from="' + parseFloat(posY + pas1) + '" to="' + posY + '" begin="' + idZone + 's2.end" dur="500ms" fill="freeze" /> </line>' +
      '<line x1="' + parseFloat(posX + longueur) + '" y1="' + posY + '" x2="' + parseFloat(posX + longueur) + '" y2="' + posY + '" ' + styleSvg + '>' +
      '<animate attributeName="y2" attributeType="XML" id="' + idZone + 's4"' +
      'from="' + posY + '" to="' + parseFloat(posY + pas2) + '" begin="' + idZone + 's3.end" dur="500ms" fill="freeze" />' +
      '<animate attributeName="x2" attributeType="XML" id="' + idZone + 's5"' +
      'from="' + parseFloat(posX + longueur) + '" to="' + parseFloat(posX + (longueur - pas2)) + '" begin="' + idZone + 's3.end" dur="500ms" fill="freeze" /> </line>' +
      '<line x1="' + parseFloat(posX + longueur) + '" y1="' + posY + '" x2="' + parseFloat(posX + longueur) + '" y2="' + posY + '" ' + styleSvg + '>' +
      '<animate attributeName="y2" attributeType="XML" id="' + idZone + 's6"' +
      'from="' + posY + '" to="' + parseFloat(posY + pas2) + '" begin="' + idZone + 's3.end" dur="500ms" fill="freeze" />' +
      '<animate attributeName="x2" attributeType="XML" id="' + idZone + 's7"' +
      'from="' + parseFloat(posX + longueur) + '" to="' + parseFloat(posX + longueur + pas2) + '" begin="' + idZone + 's3.end" dur="500ms" fill="freeze" /> </line>' +
      '<text id = "' + idZone + 'ecart" x="' + parseFloat(posX + textPosX + 50) + '" y="' + parseFloat(posY + textPosY) + '" style="font-size: 22px;font-family: Verdana ;' + couleur + '; stroke-width:0 ;text-anchor:middle;visibility:hidden">' + texte +
      '<set attributeName="visibility" attributeType="CSS" to="visible" begin="' + idZone + 's7.end" dur="3s"  fill="freeze"/>' +
      '</text>'

    if (position === 'basGR' || position === 'basDR') {
      svgTxt = svgTxt + '<line x1="' + parseFloat(posX + textPosX + 20) + '" y1="' + parseFloat(posY + textPosY - 20) + '" x2="' + parseFloat(posX + textPosX + 75) + '" y2="' + parseFloat(posY + textPosY) + '" style="fill:none;stroke:red; stroke-width:2px;stroke-opacity:0">' +
        '<animate attributeName="stroke-opacity" attributeType="CSS" from="0" to="1" begin="' + idZone + 's7.end" dur="1s"  fill="freeze"/></line>'
    }

    return svgTxt
  }

  function remplaceVirgule (nbChaine) {
    let pEntiere = ''
    let decimale = ''
    let nombre
    // nbChaine=nbChaine+"";
    if (nbChaine.indexOf(',') !== -1) {
      // le nombre contient une virgule
      pEntiere = nbChaine.split(',')[0]
      decimale = nbChaine.split(',')[1]
      nombre = pEntiere + '.' + decimale
    } else {
      nombre = nbChaine
    }
    return nombre
  }

  function trim (sString) { // supprime les espaces avant et après la chaine (trouvé sur internet)
    while (sString.substring(0, 1) === ' ' || sString.substring(0, 1) === '\t' ||
    sString.substring(0, 1) === '\r' || sString.substring(0, 1) === '\n') {
      sString = sString.substring(1, sString.length)
    }
    while (sString.substring(sString.length - 1, sString.length) === ' ' ||
    sString.substring(sString.length - 1, sString.length) === '\t' ||
    sString.substring(sString.length - 1, sString.length) === '\r' ||
    sString.substring(sString.length - 1, sString.length) === '\n') {
      sString = sString.substring(0, sString.length - 1)
    }
    return sString
  }// fin trim()

  function enleveEspaceMoinsPlus (ch) {
    const chaine = trim(ch)
    let temp, chaineSansEspaces
    if (chaine.indexOf('-') !== -1) {
      temp = chaine.substring(1)
      chaineSansEspaces = '-' + trim(temp)
    } else if (chaine.indexOf('+') !== -1) {
      temp = chaine.substring(1)
      chaineSansEspaces = '+' + trim(temp)
    } else {
      chaineSansEspaces = chaine
    }

    return chaineSansEspaces
  } // fin enleveEspaceMoinsPlus()

  function enleveEspaces (ch) {
    // supprime les espaces à l’interieur de la chaine
    const chaine = trim(ch)
    let chaineSansEspaces = ''
    if (chaine.indexOf(' ') !== -1) {
      const tab = chaine.split(' ')
      for (let i = 0; i < tab.length; i++) {
        chaineSansEspaces = chaineSansEspaces + tab[i]
      }
    } else chaineSansEspaces = chaine
    return chaineSansEspaces
  }

  // retourne un tableau d’entiers compris entre 0 et nbrepetitions (exclus)
  function getIndexes () {
    const { motifs, raisons, nbrepetitions } = ds
    // indice du premier nombre des deux consécutifs
    let pos = 100 // si pos = 100, la suite n’est pas définie, 2 true successifs doivent être saisis
    // le premier elt sera l’index de motifs de la première valeur true suivie d’une autre valeur true
    // le reste correspond à un entier entre 0 et nbrepetitions (exclus), pas compris comment c’est construit
    const tableau = []
    for (let i = 0; i < motifs.length; i++) {
      if (motifs[i] && motifs[i + 1]) {
        // on est sur un true avec un suivant à true aussi
        pos = i
        tableau[0] = i
        break
      }
    }

    // tableau des raisons en fonctions du nombre de répétitions
    if (raisons.length >= nbrepetitions) {
      const index = Math.floor(Math.random() * nbrepetitions)
      tableau.push(index) // on stocke des index de raisons pris au hasard
      let k = 1
      while (k < nbrepetitions) {
        let onContinue = true
        const index = Math.floor(Math.random() * nbrepetitions)
        for (let j = 1; j < tableau.length; j++) {
          onContinue = onContinue && (tableau[j] !== index)
        }
        if (onContinue) {
          tableau.push(index)
          k++
        }
      }
    } else {
      // raisons.length < nbrepetitions
      const q = Math.floor(nbrepetitions / raisons.length)
      const reste = nbrepetitions % raisons.length
      for (let i = 1; i < q + 1; i++) {
        for (let k = 0; k < raisons.length; k++) {
          tableau.push(k)
        }
      }
      if (reste !== 0) {
        for (let k = 0; k < reste + 1; k++) {
          tableau.push(k)
        }
      }
    }

    if (pos === 100) {
      throw Error('Paramétrage invalide, il faut deux valeurs true consécutives dans les motifs')
    }
    return tableau
  }

  function enonceMain () {
    const raison = j3pGetRandomElt(ds.raisons)
    const partieDec = raison - Math.floor(raison)
    const nbDecimale = partieDec > 0
      ? String(partieDec).length
      : 0

    const decimale = (nbDecimale < ds.nbDecimales) ? ds.nbDecimales : nbDecimale
    stor.stockage[6] = decimale

    stor.stockage[3] = j3pRandomdec(ds.n + ',' + ds.nbDecimales)
    stor.stockage[4] = raison

    // l’enoncé
    stor.conteneur = j3pAddElt(me.zonesElts.MG, 'div', '', { style: me.styles.etendre('petit.enonce', { padding: '20px' }) })
    stor.fleches = j3pAddElt(me.zonesElts.MG, 'div')
    j3pStyle(stor.fleches, { position: 'absolute', top: 0, left: 0 })
    stor.zoneCons1 = j3pAddElt(stor.conteneur, 'p', textes.phrase1)
    stor.zoneCons2 = j3pAddElt(stor.conteneur, 'div')
    j3pStyle(stor.zoneCons2, { paddingTop: '40px' })
    let nbSaisies = 0
    stor.zoneDeSaisies = []
    const largeurzonedetexte = 110
    let b, n1
    stor.zoneInput = []
    stor.case = []

    for (let j = 0; j < ds.motifs.length; j++) {
      if (ds.motifs[j]) {
        if (j < stor.fait[0]) {
          b = (ds.nbDecimales === 0)
            ? stor.stockage[3] - (stor.fait[0] - j) * stor.stockage[4]
            : j3pArrondi(stor.stockage[3] - (stor.fait[0] - j) * stor.stockage[4], decimale)
          n1 = formateNombre(b)
        } else if (j > stor.fait[0]) {
          b = (ds.nbDecimales === 0)
            ? stor.stockage[3] + (j - stor.fait[0]) * stor.stockage[4]
            : j3pArrondi(stor.stockage[3] + (j - stor.fait[0]) * stor.stockage[4], decimale)

          n1 = formateNombre(b)
        } else if (j === stor.fait[0]) {
          n1 = formateNombre(stor.stockage[3])
        }
      } else {
        stor.zoneDeSaisies.push(j)
        if (j < stor.fait[0]) {
          b = (ds.nbDecimales === 0)
            ? stor.stockage[3] - (stor.fait[0] - j) * stor.stockage[4]
            : j3pArrondi(stor.stockage[3] - (stor.fait[0] - j) * stor.stockage[4], decimale)
          n1 = formateNombre(b)
        } else if (j > stor.fait[0]) {
          b = (ds.nbDecimales === 0)
            ? stor.stockage[3] + (j - stor.fait[0]) * stor.stockage[4]
            : j3pArrondi(stor.stockage[3] + (j - stor.fait[0]) * stor.stockage[4], decimale)

          n1 = formateNombre(b)
        }
        stor.stockage[nbSaisies] = n1
        nbSaisies++
      }
      const elt = j3pAffiche(stor.zoneCons2, '', '@1@', {
        input1: { texte: (ds.motifs[j]) ? String(n1) : '', maxchars: '7', restrict: /[0-9 ,.-]/, tailletexte: 22, width: largeurzonedetexte }
      })
      stor.case.push(elt.inputList[0])
      elt.inputList[0].style.textAlign = 'center'
      elt.inputList[0].style.marginRight = '5px'
      if (ds.motifs[j]) {
        j3pDesactive(elt.inputList[0])
        j3pStyle(elt.inputList[0], { textAlign: 'center', marginRight: '2px', backgroundColor: 'rgba(80, 80, 80, 0.3)' })
      } else {
        stor.zoneInput.push(elt.inputList[0])
        j3pStyle(elt.inputList[0], { textAlign: 'center', marginRight: '2px' })
      }
    }
    j3pFocus(stor.zoneInput[0])
    stor.zoneInput.forEach(zone => {
      zone.onkeyup = function () {
        const selStart = this.selectionStart
        const selEnd = this.selectionEnd
        this.value = this.value.replace(/\./g, ',')
        this.selectionStart = selStart
        this.selectionEnd = selEnd
      }
    })
    stor.zoneCorr = j3pAddElt(me.zonesElts.MG, 'div', '', { style: me.styles.etendre('moyen.explications', { padding: '35px' }) })
    me.finEnonce()
  }

  switch (me.etat) {
    case 'enonce':
      if (me.debutDeLaSection) {
        // on contrôle les types de raisons et motifs
        if (typeof ds.raisons === 'string') {
          // l’éditeur de paramètres nous retourne une string avec [a,b] dedans
          try {
            ds.raisons = JSON.parse(ds.raisons)
          } catch (error) {
            console.error(error)
            ds.raisons = defaultRaisons
          }
        }
        if (!Array.isArray(ds.raisons)) {
          console.error(Error('le paramètre raisons n’est pas une liste', ds.raisons, 'il sera remplacé par', defaultRaisons))
          ds.raisons = defaultRaisons
        }
        // on cast les raisons en number, c'était souvent des strings
        ds.raisons = ds.raisons.map(r => Number(r)).filter(r => Number.isFinite(r))
        if (!ds.raisons.length) {
          console.error(Error('paramètre raisons invalide, remplacé par', defaultRaisons))
          ds.raisons = defaultRaisons
        }
        // idem pour motifs
        if (typeof ds.motifs === 'string') {
          // l’éditeur de paramètres nous retourne une string avec [a,b] dedans
          try {
            ds.motifs = JSON.parse(ds.motifs)
          } catch (error) {
            console.error(error)
            ds.motifs = defaultMotifs
          }
        }
        if (!Array.isArray(ds.motifs)) {
          console.error(Error('le paramètre motifs n’est pas une liste', ds.motifs, 'il sera remplacé par', defaultMotifs))
          ds.motifs = defaultMotifs
        }
        ds.motifs = ds.motifs.map(Boolean)
        if (!ds.motifs.length) {
          console.error(Error('paramètre motifs invalide, remplacé par', defaultMotifs))
          ds.motifs = defaultMotifs
        }
        // fin de la vérification des types

        me.construitStructurePage('presentation2')
        stor.stockage = [0, 0, 0, 0]
        me.typederreurs = [0, 0, 0, 0, 0, 0]
        me.afficheTitre(textes.titre_exo)
        stor.fait = getIndexes()
        if (ds.indication) me.indication(me.zonesElts.IG, ds.indication)
      } else {
        me.videLesZones()
      }

      // Deuxième étape d’une répétition  si il y a deux étapes
      // OU première étape s’il n’y a qu’une seule étape
      enonceMain()
      break // case "enonce":

    case 'correction': {
      const indicePremier = stor.fait[0]
      /** issu de ds.raisons, forcément un entier */
      const raison = stor.stockage[4]
      let reponseVide = 0
      let reponseJuste = true // true : tout est juste; false : au moins une réponse est fausse
      let nbMalEcrit = 0
      const indMalEcrit = [] // Indices des réponses bonnes mais mal écrites (ajout de Yves)

      // j3pDiv(me.zonesElts.MG, 'conteneur', '', [40, 40]) //
      const texte = (raison > 0)
        ? '+' + formateNombre(raison)
        : formateNombre(raison)
      const texteG = (raison > 0)
        ? '-' + formateNombre(raison)
        : '+' + texte.split('-')[1]
      let figSvg = '<svg id="figureSvg" width="800" height="400" >'
      let e, f, diff, dec
      // console.log('cases: ', stor.case, stor.zoneDeSaisies)

      for (let i = 0; i < stor.zoneDeSaisies.length; i++) {
        let repEleve = trim(stor.case[stor.zoneDeSaisies[i]].value) // on enlève les espaces avant et après la réponse
        // Modif Yves : On remplace les . par des ,
        repEleve = repEleve.replace(/\./g, ',')
        if (stor.case[stor.zoneDeSaisies[i]].value.indexOf('-') !== -1) {
          repEleve = enleveEspaceMoinsPlus(stor.case[stor.zoneDeSaisies[i]].value)
        } else if (stor.case[stor.zoneDeSaisies[i]].value.indexOf('+') !== -1) {
          repEleve = enleveEspaceMoinsPlus(stor.case[stor.zoneDeSaisies[i]].value).substring(1)
        }
        if (repEleve === stor.stockage[i]) { // réponse juste
          stor.case[stor.zoneDeSaisies[i]].style.color = me.styles.cbien
          j3pDesactive(stor.case[stor.zoneDeSaisies[i]])
          stor.case[stor.zoneDeSaisies[i]].reponse = true
          reponseJuste = reponseJuste && stor.case[stor.zoneDeSaisies[i]].reponse
        } else { // réponse fausse ou nombre mal ecrit ou pas de réponse
          // nombre mal écrit
          if (enleveEspaces(repEleve) === enleveEspaces(stor.stockage[i])) {
            nbMalEcrit++
            indMalEcrit.push(stor.zoneDeSaisies[i])
            stor.case[stor.zoneDeSaisies[i]].style.color = me.styles.cfaux
          } else if ((stor.case[stor.zoneDeSaisies[i]].value === '') && !me.isElapsed) {
            reponseVide++
          } else {
            // réponse vraiment fausse
            if (ds.nbDecimales !== 0) {
              // nombres à virgule
              if (indicePremier < stor.zoneDeSaisies[i]) {
                e = (stor.case[stor.zoneDeSaisies[i] - 1].value.indexOf('-') !== -1)
                  ? enleveEspaces('-' + remplaceVirgule(stor.case[stor.zoneDeSaisies[i] - 1].value.substring(1)))
                  : enleveEspaces(remplaceVirgule(stor.case[stor.zoneDeSaisies[i] - 1].value))
                f = (stor.case[stor.zoneDeSaisies[i]].value.indexOf('-') !== -1)
                  ? enleveEspaces('-' + remplaceVirgule(enleveEspaceMoinsPlus(stor.case[stor.zoneDeSaisies[i]].value).substring(1)))
                  : enleveEspaces(remplaceVirgule(stor.case[stor.zoneDeSaisies[i]].value))
                diff = (j3pArrondi(parseFloat(f - e), stor.stockage[6]) > 0)
                  ? '+' + formateNombre(j3pArrondi((f - e), stor.stockage[6]))
                  : formateNombre(j3pArrondi((f - e), stor.stockage[6]))
              } else if (indicePremier > stor.zoneDeSaisies[i]) {
                e = (stor.case[stor.zoneDeSaisies[i] + 1].value.indexOf('-') !== -1)
                  ? enleveEspaces('-' + remplaceVirgule(stor.case[stor.zoneDeSaisies[i] + 1].value.substring(1)))
                  : enleveEspaces(remplaceVirgule(stor.case[stor.zoneDeSaisies[i] + 1].value))
                f = (stor.case[stor.zoneDeSaisies[i]].value.indexOf('-') !== -1)
                  ? enleveEspaces('-' + remplaceVirgule(enleveEspaceMoinsPlus(stor.case[stor.zoneDeSaisies[i]].value).substring(1)))
                  : enleveEspaces(remplaceVirgule(stor.case[stor.zoneDeSaisies[i]].value))

                diff = (j3pArrondi(parseFloat(f - e), stor.stockage[6]) > 0)
                  ? '+' + formateNombre(j3pArrondi((f - e), stor.stockage[6]))
                  : formateNombre(j3pArrondi((f - e), stor.stockage[6]))
              }
            } else {
              // pour les entiers
              if (indicePremier < stor.zoneDeSaisies[i]) {
                if (!Number.isInteger(raison)) {
                  dec = raison.split('.')[1].length // dec pour récupérer le nombre de décimales
                  e = enleveEspaces(remplaceVirgule(stor.case[stor.zoneDeSaisies[i] - 1].value))
                  f = enleveEspaces(remplaceVirgule(stor.case[stor.zoneDeSaisies[i]].value))
                  diff = (j3pArrondi(parseFloat(f - e), dec) > 0)
                    ? '+' + formateNombre(j3pArrondi(parseFloat(f - e), dec))
                    : formateNombre(j3pArrondi(parseFloat(f - e), dec))
                } else {
                  e = enleveEspaces(stor.case[stor.zoneDeSaisies[i] - 1].value)
                  f = enleveEspaces(stor.case[stor.zoneDeSaisies[i]].value)

                  diff = (parseFloat(f - e) > 0)
                    ? '+' + formateNombre(parseFloat(f - e))
                    : formateNombre(parseFloat(f - e))
                }
              } else if (indicePremier > stor.zoneDeSaisies[i]) {
                if (!Number.isInteger(raison)) {
                  dec = raison.split('.')[1].length
                  e = enleveEspaces(remplaceVirgule(stor.case[stor.zoneDeSaisies[i] + 1].value))
                  f = enleveEspaces(remplaceVirgule(stor.case[stor.zoneDeSaisies[i]].value))
                  diff = (j3pArrondi(parseFloat(f - e), dec) > 0)
                    ? '+' + formateNombre(j3pArrondi(parseFloat(f - e), dec))
                    : formateNombre(j3pArrondi(parseFloat(f - e), dec))
                } else {
                  e = enleveEspaces(stor.case[stor.zoneDeSaisies[i] + 1].value)
                  f = enleveEspaces(stor.case[stor.zoneDeSaisies[i]].value)
                  diff = (parseFloat(f - e) > 0)
                    ? '+' + formateNombre(parseFloat(f - e))
                    : formateNombre(parseFloat(f - e))
                }
              }
            }
            stor.case[stor.zoneDeSaisies[i]].reponse = false
            reponseJuste = reponseJuste && stor.case[stor.zoneDeSaisies[i]].reponse
            stor.case[stor.zoneDeSaisies[i]].style.color = '#FF0000'
            // les flèches pour la correction
            // console.log('DIFF=', typeof diff)
            if (isNaN(diff)) {
              diff = '?'
            }
            if (!stor.case[stor.zoneDeSaisies[i]].reponse && stor.zoneDeSaisies[i] !== stor.fait.length - 1) {
              if (ds.nbDecimales === 0 && Number.isInteger(raison)) {
                if (indicePremier < stor.zoneDeSaisies[i] && j3pArrondi(parseFloat(f - e), stor.stockage[6]) !== raison) {
                  figSvg = figSvg + fleche(stor.case[stor.zoneDeSaisies[i] - 1].id, diff, 'basDR')
                } else if (indicePremier > stor.zoneDeSaisies[i] && j3pArrondi(parseFloat(e - f), stor.stockage[6]) !== raison) {
                  figSvg = figSvg + fleche(stor.case[stor.zoneDeSaisies[i] + 1].id, diff, 'basGR')// +fleche("reponse"+(stor.fait[0]+1),texteG,"hautGV");
                }
              } else {
                if (indicePremier < stor.zoneDeSaisies[i] && j3pArrondi(parseFloat(f - e), stor.stockage[6]) !== raison) {
                  figSvg = figSvg + fleche(stor.case[stor.zoneDeSaisies[i] - 1].id, diff, 'basDR')
                } else if (indicePremier > stor.zoneDeSaisies[i] && j3pArrondi(parseFloat(e - f), stor.stockage[6]) !== raison) {
                  figSvg = figSvg + fleche(stor.case[stor.zoneDeSaisies[i] + 1].id, diff, 'basGR')// +fleche("reponse"+(stor.fait[0]+1),texteG,"hautGV");
                }
              }
            }
          }
        }
      } // fin analyse des réponses

      // on teste si une réponse a été saisie
      if (reponseVide > 0 && !me.isElapsed) {
        let msgReponseManquante
        if (reponseVide !== 1 || stor.zoneDeSaisies.length !== 1) {
          msgReponseManquante = textes.phrase4 + '<br><br>'
        }
        me.reponseManquante(stor.zoneCorr, msgReponseManquante)
        return this.finCorrection() // on reste en état correction
      }

      // une réponse a été saisie
      const cbon = reponseJuste
      if (cbon) {
        // tout est juste
        me.score++
        stor.zoneCorr.style.color = me.styles.cbien
        stor.zoneCorr.innerHTML = cBien
        me.typederreurs[0]++
        figSvg = figSvg + fleche(stor.case[stor.fait[0]].id, texte, 'basD')
        $(stor.fleches).html(figSvg + '</svg>')
        if (nbMalEcrit > 0) { // C’est bon mais des nombres sont mal écrits
          for (let k = 0; k < indMalEcrit.length; k++) {
            for (let j = 0; j < stor.zoneDeSaisies.length; j++) {
              if (indMalEcrit[k] === stor.zoneDeSaisies[j]) corrige(stor.case[stor.zoneDeSaisies[j]], stor.stockage[j], 30, -30)
            }
          }
          j3pAjouteDans(stor.zoneCorr, nbMalEcrit === 1 ? textes.phrase2 : textes.phrase3)
        }
        return me.finCorrection('navigation', true)
      }

      // Pas de bonne réponse
      stor.zoneCorr.style.color = me.styles.cfaux
      stor.zoneCorr.innerHTML = cFaux

      if (me.isElapsed) { // limite de temps
        stor.zoneCorr.innerHTML = tempsDepasse
        me.typederreurs[10]++
        return me.finCorrection('navigation', true)
      }

      if (me.essaiCourant < ds.nbchances) {
        // Il reste des essais
        stor.zoneCorr.innerHTML += '<br>' + essaieEncore
        me.typederreurs[1]++
        return this.finCorrection() // on reste en correction
      }

      // c'était le dernier essai
      stor.zoneCorr.innerHTML += '<br>' + regardeCorrection
      for (let j = 0; j < stor.zoneDeSaisies.length; j++) {
        if (!stor.case[stor.zoneDeSaisies[j]].reponse) {
          j3pDesactive(stor.case[stor.zoneDeSaisies[j]])
          j3pBarre(stor.case[stor.zoneDeSaisies[j]])
          corrige(stor.case[stor.zoneDeSaisies[j]], stor.stockage[j], 30, -30, 20)
        }
      }
      if (indicePremier !== 0) {
        $(stor.fleches).html(figSvg + fleche(stor.case[stor.fait[0]].id, texte, 'basD') + figSvg + fleche(stor.case[stor.fait[0] + 1].id, texteG, 'hautGV') + '</svg>')
      } else {
        $(stor.fleches).html(figSvg + fleche(stor.case[stor.fait[0]].id, texte, 'basD') + '</svg>')
      }

      me.typederreurs[2]++
      me.finCorrection('navigation', true)
      break // case "correction":
    }

    case 'navigation':
      if (!me.sectionTerminee()) {
        this.etat = 'enonce'
      }
      this.finNavigation()
      break // case "navigation":
  }
}
