import { j3pAddContent, j3pAddElt, j3pDetruit, j3pFocus, j3pGetBornesIntervalle, j3pGetNewId, j3pGetRandomInt, j3pNombre, j3pNombreBienEcrit, j3pRemplacePoint, j3pRestriction, j3pStyle } from 'src/legacy/core/functions'
import ValidationZones from 'src/legacy/outils/calculatrice/ValidationZones'
import { j3pAffiche } from 'src/lib/mathquill/functions'
import TableauConversion from 'src/legacy/outils/tableauconversion/TableauConversion'
import textesGeneriques from 'src/lib/core/textes'

const { cBien, cFaux, essaieEncore, regardeCorrection, tempsDepasse } = textesGeneriques

/*
        JP Vanroyen
        Mai 2013
        modifié en octobre 2018 par Rémi DENIAUD
        Modifié en novembre 2018 par Yves pour validation par OK.
 */

export const params = {
  outils: ['calculatrice', 'mathquill'],
  /**
   * Ce qui est paramétrable dans l’éditeur de graphe
   * @type {LegacySectionParamsParametres}
   */
  parametres: [
    ['nbrepetitions', 5, 'entier', 'nombre de répétitions.'],
    ['indication', '', 'string', 'indication (éventuelle) donnée à l’élève'],
    ['limite', 0, 'entier', 'Temps disponible par répétition, en secondes'],
    ['nbchances', 2, 'entier', 'nombre de chances donnée à chaque répétition (2 par défaut).'],
    ['membregauche1', ['unités', 'dizaines', 'centaines', 'milliers'], 'array', 'Tableau contenant les différents choix possibles dans la présentation du premier nombre (par exemple 35 dizaines).'],
    ['membregauche2', ['unités', 'dizaines', 'centaines', 'milliers'], 'array', 'Tableau contenant les différents choix possibles dans la présentation du deuxième nombre (par exemple 35 dizaines).<br/>Paramètre utile si le paramètre deuxTermes vaut true.'],
    ['deuxTermes', true, 'boolean', '- true signifie qu’on aura une somme de deux termes ;<br/>- false signifie qu’on n’a qu’un nombre à convertir'],
    ['nbchiffresgauche1', '[2;3]', 'string', 'Intervalle dans lequel on choisit de manière aléatoire le nombre de chiffres du premier terme dans l’écriture avec l’unité de la réponse (ne peut dépasser 5).'],
    ['nbchiffresgauche2', '[1;2]', 'string', 'Intervalle dans lequel on choisit de manière aléatoire le nombre de chiffres du deuxième terme dans l’écriture avec l’unité de la réponse (ne peut dépasser 4), si deuxTermes vaut true.'],
    ['membredroite', ['unités', 'dizaines', 'centaines', 'milliers'], 'array', 'Tableau contenant les différents choix possibles dans la présentation du membre de droite (par exemple 35 dizaines).'],
    ['type', 2, 'entier', 'choix entre 1, 2 et 3 pour la présence d’un tableau de conversion<br>1: pas de tableau<br>2: tableau statique<br>3: tableau dynamique.'],
    ['affichetableau', true, 'boolean', 'Si ce paramètre vaut true, alors le tableau de conversion est affiché systématiquement à la correction, même si la réponse est exacte.'],
    ['sansEspaceAccepte', true, 'boolean', 'true signifie qu’on accepte les réponses où l’élève n’aurait pas mis les espaces.']
  ]

}

/**
 * Section cm2exN1_26
 * @fileOverview
 * @author Tommy Barroy <t.barroy@laposte.net>
 * @since 2017-10
*/
export default function main () {
  const me = this
  let ds = me.donneesSection

  const stor = me.storage
  const intervalleReg = /^\[(-?\d+[.,]?\d*);(-?\d+[.,]?\d*)]$/

  function intervalleValide (intervalle, borneMin, borneMax, intDefaut) {
    // intervalle est l’intervalle de départ
    // On veut que sa borne inf ne soit pas inférieure à borneMin et que sa borne sup ne soit pas supérieure à borneMax
    // si ce n’est pas le cas, on modifie la borne inf et/ou la borne sup
    // si on n’a pas renseigné un intervalle, on donne l’intervalle intDefaut
    // console.log('intervalle:', intervalle)
    const chunks = intervalleReg.exec(intervalle)
    if (chunks) {
      // on a bien un intervalle
      let borneInf = j3pNombre(chunks[1])
      let borneSup = j3pNombre(chunks[2])
      if (borneInf < borneMin) borneInf = borneMin
      if (borneSup > borneMax) borneSup = borneMax
      return '[' + borneInf + ';' + borneSup + ']'
    }
    console.error(Error('intervalle ' + intervalle + ' invalide, on impose ' + intDefaut))
    return intDefaut
  }

  function afficheCorr (bonneReponse) {
    // fonction appelée pour afficher la correction (que la réponse soit bonne ou fausse
    // le style n’est pas le même suivant que la réponse soit bonne ou fausse'

    j3pStyle(stor.zoneExpli, { paddingTop: '15px', color: (bonneReponse) ? me.styles.cbien : me.styles.toutpetit.correction.color })
    if (ds.type > 1) {
      j3pDetruit(stor.leTableau)
      j3pDetruit(stor.info)
    }
    let untab
    let untab2
    stor.leTableauCorr = j3pAddElt(stor.zoneTabCorr, 'div', { style: me.styles.moyen.explications })
    // stor.leTableauCorr.style.setProperty('position', 'relative')
    if (!bonneReponse) stor.zoneExpli.style.display = 'block'
    const corrTabId = j3pGetNewId('tableauCorr')
    if (!ds.deuxTermes) {
      untab = TableauConversion.creeLigne(stor.nbG, 11 - stor.indiceGauche, false, [1, 3])
      untab2 = TableauConversion.creeLigne(stor.solution.replace(/\s/g, ''), 11 - stor.indiceDroit, true, [1, 3])
      stor.tab = new TableauConversion(stor.leTableauCorr,
        {

          idDIV: corrTabId,
          format: {
            virgule: [false, false],
            epaisseurs: {},
            couleurs: {}
          },
          lignes: [untab, untab2]
        }
      )
    } else {
      untab = TableauConversion.creeLigne(stor.nbG0, 11 - stor.indiceGauche, false, [1, 3])
      untab2 = TableauConversion.creeLigne(stor.nbG1, 11 - stor.indiceDroit, false, [1, 3])
      const untab3 = TableauConversion.creeLigne(stor.solution.replace(/\s/g, ''), 11 - stor.nbG, false, [1, 3])
      stor.tab = new TableauConversion(stor.leTableauCorr,
        {

          idDIV: corrTabId,
          format: {
            virgule: [false, false, false],
            epaisseurs: {},
            couleurs: {}
          },
          lignes: [untab, untab2, untab3]
        }
      )
      if (!bonneReponse) {
        j3pAddContent(stor.zoneCorr, '<br>')
        j3pAddContent(stor.zoneCorr, stor.correction)
      }
    }
  }
  function getDonnees () {
    return {

      typesection: 'primaire',
      nbrepetitions: 5,
      nbetapes: 1,
      indication: '',
      // surchargeindication: false;  pour le cas de presentation1bis
      nbchances: 2,
      positionNombreDemande: ['g', 'd', 'g', 'g', 'g', 'd', 'd', 'g'],
      sansEspaceAccepte: true,
      membregauche1: ['unités', 'dizaines', 'centaines', 'milliers'],
      membregauche2: ['unités', 'dizaines', 'centaines', 'milliers'],
      deuxTermes: true,
      nbchiffresgauche1: '[2;3]',
      nbchiffresgauche2: '[1;2]',
      membredroite: ['unités', 'dizaines', 'centaines', 'milliers'],

      affichetableau: true,
      type: 2,
      // type = 1;
      // le tableau est proposé lors de la correction si c’est faux
      // OU si c’est bon et que affichetableau = true;

      // type = 2;
      // le tableau est proposé vide, non-interactif
      // Affichage de la correction dans ce tableau
      // Affichage systématique si affichetableau = true;

      // type = 3;
      // un tableau de conversion est proposé comportant des zones de saisies

      structure: 'presentation1', //  || "presentation2" || "presentation3"  || "presentation1bis"
      // limite=5;

      textes: {
        titreExo: 'Les différents chiffres - (N1 F2 ex6)',
        phrase1: 'Complète :',
        phrase2: 'Complète avec l’entier qui précède :',
        phrase3: 'Il y a une erreur, corrige-la&nbsp;!',
        phrase4: 'Il faut donner une réponse&nbsp;!',
        info1: 'Tu peux t’aider en regardant le tableau et en y plaçant mentalement les nombres.',
        info2: 'Tu peux t’aider en plaçant correctement les nombres dans le tableau.<br/>Le contenu de ce tableau ne sera pas vérifié par le logiciel.',
        comment1: 'Attention à bien écrire le nombre&nbsp;!',
        comment2: 'Attention tout de même à mettre l’espace pour bien écrire le nombre&nbsp;!'
      }
    }
  }
  function enonceMain () {
    // construction des données
    // on remplace les stockage[n] par du storage.propEnClair
    // 0 => solution
    // 3 => indiceGauche
    // 4 => indiceDroit
    // 5 => nbG (mais parfois c’est une variable indicedroit qui est mise dedans…)
    // 6 => nbG0 (premier elt de l’array nombregauche)
    // 7 => nbG1 (2e elt de l’array nombregauche)
    // 8 => correction (en string avec les espaces)
    let termegauche, termedroit, indicegauche, indicedroit, nbchiffresgauche, puissance10, puissance10inf,
      nombregauche
    if (!ds.deuxTermes) {
      ds.membregauche = ds.membregauche1
      do {
        // ces termes
        termegauche = ds.membregauche[j3pGetRandomInt(0, (ds.membregauche.length - 1))]
        termedroit = ds.membredroite[j3pGetRandomInt(0, (ds.membredroite.length - 1))]
      } while (termegauche === termedroit)
      indicegauche = stor.propositions[termegauche]
      indicedroit = stor.propositions[termedroit]
      stor.indiceGauche = indicegauche
      stor.indiceDroit = indicedroit
      const [intMin, intMax] = j3pGetBornesIntervalle(ds.nbchiffresgauche1)
      const nbchiffres = j3pGetRandomInt(intMin, intMax)
      puissance10 = Math.pow(10, nbchiffres) - 1
      puissance10inf = Math.pow(10, nbchiffres - 1) - 1
      nombregauche = j3pGetRandomInt(puissance10inf, puissance10)

      // dans le cas où on a nombregauche dizaines = ? centaines
      // il faut que nombregauche se termine par 0
      if (indicegauche < indicedroit) {
        nombregauche = nombregauche * Math.pow(10, indicedroit - indicegauche)
      }
      stor.nbG = nombregauche
      stor.solution = nombregauche * Math.pow(10, indicegauche - indicedroit)
    } else {
      stor.membregauche = [ds.membregauche1, ds.membregauche2]
      termegauche = []
      do {
        termegauche[0] = stor.membregauche[0][j3pGetRandomInt(0, (stor.membregauche[0].length - 1))]
        termegauche[1] = stor.membregauche[1][j3pGetRandomInt(0, (stor.membregauche[1].length - 1))]
      } while (termegauche[0] === termegauche[1])
      termedroit = ds.membredroite[j3pGetRandomInt(0, (ds.membredroite.length - 1))]
      indicegauche = []
      indicegauche[0] = stor.propositions[termegauche[0]]
      indicegauche[1] = stor.propositions[termegauche[1]]
      stor.indiceGauche = indicegauche[0]
      stor.indiceDroit = indicegauche[1]
      indicedroit = stor.propositions[termedroit]
      stor.nbG = indicedroit
      nbchiffresgauche = [ds.nbchiffresgauche1, ds.nbchiffresgauche2]
      const [intMin1, intMax1] = j3pGetBornesIntervalle(nbchiffresgauche[0])
      const [intMin2, intMax2] = j3pGetBornesIntervalle(nbchiffresgauche[1])
      const nbchiffres1 = j3pGetRandomInt(intMin1, intMax1)
      const nbchiffres2 = j3pGetRandomInt(intMin2, intMax2)
      nombregauche = []
      puissance10 = Math.pow(10, nbchiffres1) - 1
      puissance10inf = Math.pow(10, nbchiffres1 - 1) - 1
      do {
        nombregauche[0] = j3pGetRandomInt(puissance10inf, puissance10)
      } while (nombregauche[0] <= 1)
      puissance10 = Math.pow(10, nbchiffres2) - 1
      puissance10inf = Math.pow(10, nbchiffres2 - 1) - 1
      do {
        nombregauche[1] = j3pGetRandomInt(puissance10inf, puissance10)
      } while (nombregauche[1] <= 1)

      for (let k = 0; k <= 1; k++) {
        if (indicegauche[k] < indicedroit) {
          nombregauche[k] = nombregauche[k] * Math.pow(10, indicedroit - indicegauche[k])
        }
      }

      stor.nbG0 = nombregauche[0]
      stor.nbG1 = nombregauche[1]

      stor.solution = nombregauche[0] * Math.pow(10, indicegauche[0] - indicedroit) + nombregauche[1] * Math.pow(10, indicegauche[1] - indicedroit)
      stor.correction = j3pNombreBienEcrit(nombregauche[0] * Math.pow(10, indicegauche[0] - indicedroit)) + ' + ' + j3pNombreBienEcrit(nombregauche[1] * Math.pow(10, indicegauche[1] - indicedroit)) + ' = ' + j3pNombreBienEcrit(stor.solution)
    } // fin else deux termes

    stor.NombreSol = stor.solution
    stor.solution = j3pNombreBienEcrit(stor.solution)

    // construction de la scène
    stor.conteneur = j3pAddElt(me.zonesElts.MG, 'div', '', { style: me.styles.etendre('petit.enonce', { padding: '10px' }) })
    for (let i = 1; i <= 2; i++)stor['zoneCons' + i] = j3pAddElt(stor.conteneur, 'div')
    stor.zoneCorr = j3pAddElt(stor.conteneur, 'div', '', {
      style: me.styles.etendre('petit.correction', {
        padding: '10px'
      })
    })
    j3pAddContent(stor.zoneCons1, ds.textes.phrase1)
    let chaine, elt
    if (!ds.deuxTermes) {
      chaine = '<span class="grasrouge">' + j3pNombreBienEcrit(nombregauche) + '</span>'

      elt = j3pAffiche(stor.zoneCons2, '', '£a ' + termegauche + ' = @1@ ' + termedroit,
        {
          a: chaine,
          input1: { texte: '', dynamique: true, correction: String(stor.solution), width: '12px' }
        }
      )
    } else {
      chaine = []
      chaine[0] = '<span class="grasrouge">' + j3pNombreBienEcrit(nombregauche[0]) + '</span>'
      chaine[1] = '<span class="grasrouge">' + j3pNombreBienEcrit(nombregauche[1]) + '</span>'
      elt = j3pAffiche(stor.zoneCons2, '', '£a ' + termegauche[0] + ' et £b ' + termegauche[1] + '= @1@ ' + termedroit,
        {
          a: chaine[0],
          b: chaine[1],
          input1: { texte: '', dynamique: true, correction: String(stor.solution), width: '12px' }
        }
      )
    }
    stor.zoneInput = elt.inputList[0]
    stor.info = j3pAddElt(stor.conteneur, 'div', '', { style: me.styles.etendre('petit.explications', { background: '#CCC', margin: '15px 0' }) })
    const tabUnites = (ds.type === 2) ? ['.', '.', '.'] : ['?_', '?_', '?_']
    const tabMilliers = (ds.type === 2) ? ['.', '.', '.'] : ['?_', '?_', '?_']
    const tabMillions = (stor.NombreSol * Math.pow(10, indicedroit) >= Math.pow(10, 6)) ? (ds.type === 2) ? ['.', '.', '.'] : ['?_', '?_', '?_'] : []
    stor.zoneExpli = j3pAddElt(stor.conteneur, 'div', '', { style: me.styles.petit.correction })
    stor.zoneInput.typeReponse = ['texte']
    stor.zoneInput.reponse = (ds.sansEspaceAccepte) ? [stor.solution, stor.solution.replace(/\s/g, '')] : [stor.solution]
    stor.laCorr = String(stor.zoneInput.reponse[0]) + ' ' + termedroit
    j3pAddContent(stor.zoneExpli, stor.laCorr)
    stor.zoneExpli.style.display = 'none'
    me.logIfDebug('réponse : ', stor.zoneInput.reponse)
    j3pRestriction(stor.zoneInput, '0-9,\\. ')
    stor.zoneInput.addEventListener('input', j3pRemplacePoint)
    if (ds.affichetableau) {
      stor.leTableau = j3pAddElt(stor.conteneur, 'div')
      stor.idTab = j3pGetNewId('tableauId')
      if (ds.type === 2) {
        j3pAddContent(stor.info, ds.textes.info1)
        stor.tab = new TableauConversion(stor.leTableau,
          {

            idDIV: stor.idTab,
            classes: [3, 4],
            format: {
              epaisseurs: {},
              couleurs: {}
            },
            lignes: [[[], tabMillions, tabMilliers, tabUnites, []]]
          }
        )
      }
      if (ds.type === 3) {
        j3pAddContent(stor.info, ds.textes.info2)
        const leslignes = ds.deuxTermes
          ? [[[], tabMillions, tabMilliers, tabUnites, []], [[], tabMillions, tabMilliers, tabUnites, []], [[], tabMillions, tabMilliers, tabUnites, []]]
          : [[[], tabMillions, tabMilliers, tabUnites, []]]
        // if (!ds.deuxTermes) { var leslignes = [[[], tabMillions, tabMilliers, tabUnites, []]] } else { var leslignes = [[[], tabMillions, tabMilliers, tabUnites, []], [[], tabMillions, tabMilliers, tabUnites, []], [[], tabMillions, tabMilliers, tabUnites, []]] }
        stor.tab = new TableauConversion(stor.leTableau,
          {

            idDIV: stor.idTab,
            classes: [3, 4],
            format: {
              epaisseurs: {},
              couleurs: {}
            },
            lignes: leslignes
          }
        )
      }
      stor.leTableau.style.paddingTop = '10px'
    }

    stor.zoneCorr = j3pAddElt(me.zonesElts.MD, 'div', '', { style: me.styles.etendre('moyen.explications', { padding: '10px' }) })
    // stor.leTableau.style.setProperty('position', 'relative')
    j3pFocus(stor.zoneInput)
    // Paramétrage de la validation
    // mesZonesSaisie est un tableau contenant le nom de chacune des zones (on pourrait aussi y mettre une liste déroulante)
    const mesZonesSaisie = [stor.zoneInput.id]
    // fctsValid est un objet permettant la validation. Il contient le nom des zones (tableau précédent) et un deuxième paramètre validePerso
    stor.fctsValid = new ValidationZones({ parcours: me, zones: mesZonesSaisie })
    stor.zoneTabCorr = j3pAddElt(stor.conteneur, 'div')

    me.finEnonce()
  }
  switch (me.etat) {
    case 'enonce':
      if (me.debutDeLaSection) {
        me.donneesSection = getDonnees()
        ds = me.donneesSection
        me.surcharge()
        me.construitStructurePage({ structure: ds.structure, ratioGauche: 0.7 })
        ds.nbchiffresgauche1 = intervalleValide(ds.nbchiffresgauche1, 1, 5, '[2;3]')
        ds.nbchiffresgauche2 = intervalleValide(ds.nbchiffresgauche2, 1, 4, '[1;2]')
        me.typederreurs = [0, 0, 0, 0, 0, 0]
        me.afficheTitre(ds.textes.titreExo)
        if (ds.indication !== '') me.indication(me.zonesElts.IG, ds.indication)
        stor.propositions = { unités: 0, dizaines: 1, centaines: 2, milliers: 3 }
        // Il me faut gérer des ressources mal construites où il serait écrit "unites" et non "unités", d’où les 3 boucles suivantes
        for (let k = 0; k < ds.membregauche1.length; k++) {
          if (ds.membregauche1[k].indexOf('unite') > -1) ds.membregauche1[k] = 'unités'
        }
        for (let k = 0; k < ds.membregauche2.length; k++) {
          if (ds.membregauche2[k].indexOf('unite') > -1) ds.membregauche2[k] = 'unités'
        }
        for (let k = 0; k < ds.membredroite.length; k++) {
          if (ds.membredroite[k].indexOf('unite') > -1) ds.membredroite[k] = 'unités'
        }
      } else {
        me.videLesZones()
      }
      enonceMain()

      break // case "enonce":

    case 'correction':
      // On teste si une réponse a été saisie
      {
        const fctsValid = stor.fctsValid// ici je crée juste une variable pour raccourcir le nom de l’oject fctsValid
        const reponse = fctsValid.validationGlobale()

        me.cacheBoutonValider()
        // on teste si une réponse a été saisie

        if ((!reponse.aRepondu) && (!me.isElapsed)) {
          this.reponseManquante(stor.zoneCorr)
          j3pFocus(stor.zoneInput)
        } else { // une réponse a été saisie
          if (reponse.bonneReponse) {
            me.score++
            stor.zoneCorr.style.color = me.styles.cbien
            stor.zoneCorr.innerHTML = cBien
            me.typederreurs[0]++
            if (fctsValid.zones.reponseSaisie[0] !== fctsValid.zones.inputs[0].reponse[0]) {
              stor.zoneCorr.innerHTML += '<br/>' + ds.textes.comment2
            }
            afficheCorr(true)
            me.etat = 'navigation'
            me.sectionCourante()
          } else {
          // Pas de bonne réponse
            stor.zoneCorr.style.color = me.styles.cfaux
            // à cause de la limite de temps :
            if (me.isElapsed) { // limite de temps
              stor.zoneCorr.innerHTML = tempsDepasse
              me.typederreurs[10]++
              /*
             *   RECOPIER LA CORRECTION ICI !
             */
              afficheCorr(false)
              me.etat = 'navigation'
              me.sectionCourante()
            } else {
            // réponse fausse :
              stor.zoneCorr.innerHTML = cFaux
              if (stor.solution.replace(/\s/g, '') === fctsValid.zones.reponseSaisie[0]) {
                stor.zoneCorr.innerHTML += '<br/>' + ds.textes.comment1
              }
              // S’il y a deux chances,on appelle à nouveau le bouton Valider
              if (me.essaiCourant < ds.nbchances) {
                stor.zoneCorr.innerHTML += '<br>' + essaieEncore
                me.typederreurs[1]++
                // indication éventuelle ici
                me.etat = 'correction'
                // j3pFocus("affiche1input1");
                me.afficheBoutonValider()// c’est la partie "correction" de la section qui sera exécutée
              } else {
              // Erreur au second essai
                stor.zoneCorr.innerHTML += '<br>' + regardeCorrection
                me.typederreurs[2]++

                afficheCorr(false)

                me.etat = 'navigation'
                me.sectionCourante()
              }
            }
          }
        }
      }
      me.finCorrection()
      break // case "correction":

    case 'navigation':
      if (!me.sectionTerminee()) {
        me.etat = 'enonce'
      }
      me.finNavigation()
      break // case "navigation":
  }
}
