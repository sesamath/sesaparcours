import { j3pAddContent, j3pAddElt, j3pBarre, j3pChaine, j3pElement, j3pFocus, j3pFreezeElt, j3pGetNewId, j3pNombreBienEcrit, j3pValeurde } from 'src/legacy/core/functions'
import { j3pAffiche, j3pDesactive, mqRestriction } from 'src/lib/mathquill/functions'
import { j3pCreeTexte } from 'src/legacy/core/functionsSvg'
import textesGeneriques from 'src/lib/core/textes'

const { cBien, cFaux, essaieEncore, tempsDepasse } = textesGeneriques

/*
    Bruno Meunier
    juin 2013
    Section adaptée au clavier virtuel par Yves en 10-2021
*/

/*
    Cette variable renseigne :
        - les outils utilisés par la section
        - les paramètres de la section (nom,valeur par défaut,type,description

    A RENSEIGNER SOIGNEUSEMENT

*/

export const params = {
  outils: ['mathquill'],
  /**
   * Ce qui est paramétrable dans l’éditeur de graphe
   * @type {LegacySectionParamsParametres}
   */
  parametres: [
    ['nbrepetitions', 5, 'entier', 'Nombre de questions'],
    ['nbchances', 2, 'entier', 'Nombre d’erreurs possibles (pour tenter de les corriger)'],
    ['graduation1', 200, 'entier', 'Valeur de la première graduation'],
    ['pas', 100, 'entier', 'Valeur du pas'],
    // @etiquettes : "consecutives" ou "non-consecutives"
    ['etiquettes', 'non-consecutives', 'liste', 'Place des étiquettes, consecutives ou non-consecutives', ['consecutives', 'non-consecutives']],
    ['etiquetteOrigine', false, 'boolean', 'La première graduation comporte une etiquette']
  ]

}

/**
 * Ajouter une description ici
 * @fileOverview
 * @author Tommy Barroy <t.barroy@laposte.net>
 * @since 2017-10
*/
export default function main () {
  const me = this
  let ds = me.donneesSection
  const stor = me.storage
  function creerDroiteGraduee (graduation1, pas, etiquettes, etiquetteOrigine) {
    const largeurDroiteGraduee = 480
    const largeurGrad = largeurDroiteGraduee / 7
    const corps = 16
    const hauteurGrad = 20
    const margeGauche = 20
    const margeHaut = corps + hauteurGrad
    let etiquetteSoluce
    // la droite
    const svgns = 'http://www.w3.org/2000/svg'
    const svgContainer = document.createElementNS(svgns, 'svg')
    const d = document.createElementNS(svgns, 'line')
    const oAttribut = {
      x1: margeGauche,
      y1: margeHaut,
      x2: largeurDroiteGraduee + margeGauche,
      stroke: '#666666',
      y2: margeHaut,
      'stroke-width': '1'
    }
    for (const attribut in oAttribut) {
      d.setAttribute(attribut, oAttribut[attribut])
    }
    svgContainer.appendChild(d)
    const t = document.createElementNS(svgns, 'polygon')
    const aPoints = [
      largeurDroiteGraduee + margeGauche,
      margeHaut - 10,
      largeurDroiteGraduee + margeGauche + 10,
      margeHaut,
      largeurDroiteGraduee + margeGauche,
      margeHaut + 10,
      largeurDroiteGraduee + margeGauche,
      margeHaut - 10
    ]
    const sPoints = aPoints.join(',')
    t.setAttribute('points', sPoints)
    t.setAttribute('fill', '#666666')
    svgContainer.appendChild(t)

    // choix des etiquettes à afficher et de la graduation rouge
    let aEtiquetteVisible = []
    let graduationCible = 0
    let aleaA, aleaB
    if (etiquetteOrigine && etiquettes === 'consecutives') {
      aEtiquetteVisible = [0, 1]
      graduationCible = 3 + Math.floor(Math.random() * (6 - 3 + 1))
    } else if (etiquetteOrigine && etiquettes === 'non-consecutives') {
      aleaA = 1 + Math.floor(Math.random() * (6 - 1 + 1))
      aEtiquetteVisible = [0, aleaA]
      while (aEtiquetteVisible.indexOf(graduationCible) > -1) {
        graduationCible = 1 + Math.floor(Math.random() * (6 - 1 + 1))
      }
    } else if (!etiquetteOrigine && etiquettes === 'consecutives') {
      aleaA = 1 + Math.floor(Math.random() * (6 - 1 + 1))
      aEtiquetteVisible = [aleaA, aleaA + 1]
      while (graduationCible === 0 || aEtiquetteVisible.indexOf(graduationCible) > -1) {
        graduationCible = 1 + Math.floor(Math.random() * (6 - 1 + 1))
      }
    } else if (!etiquetteOrigine && etiquettes === 'non-consecutives') {
      aleaA = 1 + Math.floor(Math.random() * (3 - 1 + 1))
      aleaB = aleaA + 2 + Math.floor(Math.random() * (6 - (aleaA + 2) + 1))
      aEtiquetteVisible = [aleaA, aleaB]
      while (graduationCible === 0 || aEtiquetteVisible.indexOf(graduationCible) > -1) {
        graduationCible = 1 + Math.floor(Math.random() * (6 - 1 + 1))
      }
    }
    const valeurSoluce = graduation1 + graduationCible * pas
    const groupeAide = document.createElementNS(svgns, 'g')
    const groupeCorrection = document.createElementNS(svgns, 'g')
    let prop, i
    for (i = 0; i < 7; i++) {
      // les graduations
      let oStyleGrad
      if (i === graduationCible) {
        oStyleGrad = {
          stroke: '#ff0000',
          'stroke-width': '3'
        }
      } else {
        oStyleGrad = {
          stroke: '#666666',
          'stroke-width': '1'
        }
      }
      const grad = document.createElementNS(svgns, 'line')
      grad.setAttribute('x1', i * largeurGrad + margeGauche)
      grad.setAttribute('y1', corps + 5)
      grad.setAttribute('x2', i * largeurGrad + margeGauche)
      grad.setAttribute('y2', margeHaut)
      for (prop in oStyleGrad) {
        grad.setAttribute(prop, oStyleGrad[prop])
      }
      if (etiquetteOrigine || i > 0) {
        svgContainer.appendChild(grad)
      }
      // les etiquettes
      const oStyleLabel = {
        'font-family': 'Arial',
        'font-size': '16',
        fill: '#000000',
        'text-anchor': 'middle',
        x: i * largeurGrad + margeGauche,
        y: corps
      }
      const oStyleLabelSoluce = {
        'font-family': 'Arial',
        fill: '#ff0000',
        'text-anchor': 'middle',
        'font-size': '22px',
        'font-weight': 'bold',
        x: i * largeurGrad + margeGauche,
        y: corps
      }
      const label = document.createElementNS(svgns, 'text')
      const nombre = graduation1 + (i * pas)
      let texte
      if (i === graduationCible) {
        stor.idCible = j3pGetNewId('laCible')
        texte = document.createTextNode('?')
        label.setAttribute('id', stor.idCible)
        etiquetteSoluce = document.createElementNS(svgns, 'text')
        etiquetteSoluce.appendChild(document.createTextNode(nombre))

        for (prop in oStyleLabelSoluce) {
          label.setAttribute(prop, oStyleLabelSoluce[prop])
          etiquetteSoluce.setAttribute(prop, oStyleLabelSoluce[prop])
        }
      } else {
        texte = document.createTextNode(nombre)
        for (prop in oStyleLabel) {
          label.setAttribute(prop, oStyleLabel[prop])
        }
      }
      label.appendChild(texte)
      if (aEtiquetteVisible.indexOf(i) > -1 || i === graduationCible) {
        svgContainer.appendChild(label)
      }
      // le groupe Aide
      if (i >= aEtiquetteVisible[0] && i < aEtiquetteVisible[1]) {
        creerFlechePlus('#3D93C6', groupeAide)
      }
      // le groupe Correction
      if (
        graduationCible < aEtiquetteVisible[0] &&
        i >= graduationCible &&
        i < aEtiquetteVisible[0]) {
        creerFlecheMoins('#ff0000', groupeCorrection)
      } else if (
        graduationCible < aEtiquetteVisible[1] &&
        graduationCible <= aEtiquetteVisible[0] / 2 + aEtiquetteVisible[1] / 2 &&
        i >= aEtiquetteVisible[0] &&
        i < graduationCible) {
        creerFlechePlus('#ff0000', groupeCorrection)
      } else if (
        graduationCible < aEtiquetteVisible[1] &&
        graduationCible > aEtiquetteVisible[0] / 2 + aEtiquetteVisible[1] / 2 &&
        i < aEtiquetteVisible[1] &&
        i >= graduationCible) {
        creerFlecheMoins('#ff0000', groupeCorrection)
      } else if (
        graduationCible >= aEtiquetteVisible[1] &&
        i >= aEtiquetteVisible[1] &&
        i < graduationCible) {
        creerFlechePlus('#ff0000', groupeCorrection)
      }
      svgContainer.appendChild(groupeAide)
      groupeAide.style.display = 'none'
      svgContainer.appendChild(groupeCorrection)
      groupeCorrection.style.display = 'none'
    }
    const oStyle = { width: (largeurDroiteGraduee + 2 * margeGauche) + 'px', height: 100 + 'px' }
    for (prop in oStyle) {
      svgContainer.style[prop] = oStyle[prop]
    }

    function creerFlechePlus (sCouleur, groupeConteneur) {
      const marqueur = document.createElementNS(svgns, 'marker')
      let oAttr = {
        id: 'tr-' + sCouleur.substr(1) + '-' + i,
        viewBox: '0 0 10 10',
        refX: '0',
        refY: '5',
        markerWidth: '10',
        markerHeight: '10',
        orient: 'auto'
      }
      let prop
      for (prop in oAttr) {
        marqueur.setAttribute(prop, oAttr[prop])
      }
      const pointe = document.createElementNS(svgns, 'path')
      oAttr = {
        d: 'M 0 0 L 10 5 L 0 10 z',
        fill: sCouleur
      }
      for (prop in oAttr) {
        pointe.setAttribute(prop, oAttr[prop])
      }
      marqueur.appendChild(pointe)
      groupeConteneur.appendChild(marqueur)

      const x1 = i * largeurGrad + margeGauche
      const y1 = margeHaut + 5
      const x2 = largeurGrad / 2
      const y2 = 40
      const x3 = largeurGrad - 6
      const y3 = 5
      const oStyleCourbe = {
        stroke: sCouleur,
        fill: 'transparent',
        d: 'M' + x1 + ' ' + y1 + ' q ' + x2 + ' ' + y2 + ' ' + x3 + ' ' + y3,
        'marker-end': 'url(#tr-' + sCouleur.substr(1) + '-' + i + ')'
      }
      const courbe = document.createElementNS(svgns, 'path')
      for (prop in oStyleCourbe) {
        courbe.setAttribute(prop, oStyleCourbe[prop])
      }
      groupeConteneur.appendChild(courbe)
      const label = j3pCreeTexte(groupeConteneur, {
        id: 'text' + i,
        texte: '+ ' + pas,
        x: x1 + largeurGrad / 2,
        y: 80,
        couleur: sCouleur,
        taille: 12
      })
      label.setAttribute('font-family', 'Arial')
      label.setAttribute('text-anchor', 'middle')
    }

    function creerFlecheMoins (sCouleur, groupeConteneur) {
      const marqueur = document.createElementNS(svgns, 'marker')
      let oAttr = {
        id: 'tr-' + sCouleur.substr(1) + '-' + i,
        viewBox: '0 0 10 10',
        refX: '0',
        refY: '5',
        markerWidth: '10',
        markerHeight: '10',
        orient: 'auto'
      }
      let prop
      for (prop in oAttr) {
        marqueur.setAttribute(prop, oAttr[prop])
      }
      const pointe = document.createElementNS(svgns, 'path')
      oAttr = {
        d: 'M 10 0 L 10 10 L 0 5 z',
        fill: sCouleur
      }
      for (prop in oAttr) {
        pointe.setAttribute(prop, oAttr[prop])
      }
      marqueur.appendChild(pointe)
      groupeConteneur.appendChild(marqueur)

      const x1 = i * largeurGrad + margeGauche
      const y1 = margeHaut + 5
      const x2 = largeurGrad / 2
      const y2 = 40
      const x3 = largeurGrad
      const y3 = 5
      const oStyleCourbe = {
        stroke: sCouleur,
        fill: 'transparent',
        d: 'M' + x1 + ' ' + y1 + ' q ' + x2 + ' ' + y2 + ' ' + x3 + ' ' + y3,
        'marker-start': 'url(#tr-' + sCouleur.substr(1) + '-' + i + ')'
      }
      const courbe = document.createElementNS(svgns, 'path')
      for (prop in oStyleCourbe) {
        courbe.setAttribute(prop, oStyleCourbe[prop])
      }
      groupeConteneur.appendChild(courbe)
      const label = j3pCreeTexte(groupeConteneur, {
        id: 'text' + i,
        texte: '- ' + pas,
        x: x1 + largeurGrad / 2,
        y: 80,
        couleur: sCouleur,
        taille: 12
      })
      label.setAttribute('font-family', 'Arial')
      label.setAttribute('text-anchor', 'middle')
    }

    return {
      conteneur: svgContainer,
      valeurSoluce,
      etiquetteSoluce,
      groupeAide,
      groupeCorrection
    }
  }

  function getDonnees () {
    return {
      typesection: 'primaire', //

      nbrepetitions: 5,
      nbetapes: 1,
      indication: '',
      nbchances: 2,
      structure: 'presentation1', //  || "presentation2" || "presentation3"  || "presentation1bis"

      // Paramètres de la section, avec leurs valeurs par défaut.
      graduation1: 200,
      pas: 100,
      etiquettes: 'non-consecutives',
      etiquetteOrigine: false,
      textes: {
        titre_exo: 'Trouver le nombre qui correspond à une graduation - (N1 F3 ex2)',
        phrase1: 'Ecris le nombre qui correspond à la graduation rouge : ',
        corr1: 'La solution était £s.'
      }
    }
  }
  function enonceInitFirst () {
    me.donneesSection = getDonnees()
    ds = me.donneesSection

    me.surcharge()

    // Construction de la page
    me.construitStructurePage({ structure: ds.structure, ratioGauche: 0.7 })

    // par convention, stor.stockage[0] contient la solution
    // les autres peuvent contenir ce que vous voulez, en général des valeurs que l’on doit mémoriser entre les différents appels de la section
    stor.stockage = [0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0]

    /*
     Par convention,`
    `   me.typederreurs[0] = nombre de bonnes réponses
        me.typederreurs[1] = nombre de fois où l’élève a utilisé une chance supplémentaire
        me.typederreurs[2] = nombre de mauvaises réponses
        me.typederreurs[10] = nombre de fois où l’élève a eu faux à cause d’un temps limite
        LES AUTRES peuvent être utilisées par la détermination de la pe dans le cas d’une section qualitative
     */
    me.typederreurs = [0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0]

    me.score = 0

    me.afficheTitre(ds.textes.titre_exo)

    if (ds.indication) me.indication(me.zonesElts.IG, ds.indication)
  }
  function enonceMain () {
    // LE CODE
    // Creation de la droite graduee
    stor.conteneur = j3pAddElt(me.zonesElts.MG, 'div', '', { style: me.styles.etendre('petit.enonce', { padding: '20px' }) })
    // La droite
    stor.idDroite = j3pGetNewId('idDroite')
    j3pAddElt(stor.conteneur, 'div', '', { id: stor.idDroite, style: { paddingTop: '30px' } })
    const droiteG = creerDroiteGraduee(
      ds.graduation1,
      ds.pas,
      ds.etiquettes,
      ds.etiquetteOrigine
    )
    // La consigne
    const zoneCons1 = j3pAddElt(stor.conteneur, 'div')
    const zoneCons2 = j3pAddElt(stor.conteneur, 'div', '', { style: { padding: '10px 0' } })
    stor.stockage[0] = droiteG.valeurSoluce
    j3pElement(stor.idDroite).appendChild(droiteG.conteneur)
    stor.stockage.droiteG = droiteG

    j3pAddContent(zoneCons1, ds.textes.phrase1)
    const elt = j3pAffiche(zoneCons2, '', '&1&', {
      inputmq1: { dynamique: true, correction: String(droiteG.valeurSoluce) }
    })
    stor.zoneInput = elt.inputmqList[0]
    mqRestriction(stor.zoneInput, '\\d')
    j3pFocus(stor.zoneInput)
    stor.zoneD = j3pAddElt(me.zonesElts.MD, 'div', '', { style: me.styles.etendre('moyen.explications', { padding: '20px' }) })

    stor.zoneCorr = j3pAddElt(stor.zoneD, 'div', '', { style: me.styles.etendre('petit.correction', { padding: '10px' }) })
    stor.zoneExpli = j3pAddElt(stor.conteneur, 'div', '', { style: me.styles.petit.correction })
    // Obligatoire
    me.finEnonce()
  }
  switch (me.etat) {
    case 'enonce':
      // code exécuté au lancement de la section
      if (me.debutDeLaSection) {
        enonceInitFirst()
      } else {
        // A chaque répétition, on nettoie la scène
        me.videLesZones()
      }

      enonceMain()

      break // case "enonce":

    case 'correction':

      // On teste si une réponse a été saisie
      {
        const repEleve = j3pValeurde(stor.zoneInput)
        if (!repEleve && !me.isElapsed) {
          me.reponseManquante(stor.zoneCorr)
          j3pFocus(stor.zoneInput)
          me.afficheBoutonValider()
        } else {
        // Une réponse a été saisie
        // Bonne réponse
          if (repEleve === j3pNombreBienEcrit(stor.stockage[0]) || repEleve === String(stor.stockage[0])) {
            me._stopTimer()
            me.score++
            stor.zoneCorr.style.color = me.styles.cbien
            stor.zoneCorr.innerHTML = cBien
            stor.zoneInput.style.color = me.styles.cbien
            j3pDesactive(stor.zoneInput)
            j3pFreezeElt(stor.zoneInput)
            me.typederreurs[0]++
            me.cacheBoutonValider()
            me.etat = 'navigation'
            me.sectionCourante()
          } else {
          // Pas de bonne réponse
            stor.zoneCorr.style.color = me.styles.cfaux
            stor.zoneInput.style.color = me.styles.cfaux
            // A cause de la limite de temps :
            if (me.isElapsed) { // limite de temps
              me._stopTimer()

              stor.zoneCorr.innerHTML = tempsDepasse
              me.typederreurs[10]++
              me.cacheBoutonValider()
              stor.zoneCorr.innerHTML += '<br/>' + j3pChaine(ds.textes.corr1, { s: stor.stockage[0] })
              /*
             *   RECOPIER LA CORRECTION ICI !
             */
              j3pDesactive(stor.zoneInput)
              j3pFreezeElt(stor.zoneInput)
              me.etat = 'navigation'
              me.sectionCourante()
            } else {
            // Réponse fausse :
              stor.zoneCorr.innerHTML = cFaux

              // S’il y a plus de deux chances,on appelle à nouveau le bouton Valider
              if (me.essaiCourant < ds.nbchances) {
                stor.zoneCorr.innerHTML += '<br>' + essaieEncore
                me.typederreurs[1]++
              // indication éventuelle ici
              } else {
              // Erreur au nème essai
                j3pDesactive(stor.zoneInput)
                j3pBarre(stor.zoneInput)
                j3pFreezeElt(stor.zoneInput)
                const dG = stor.stockage.droiteG
                dG.groupeCorrection.style.display = 'block'
                dG.groupeAide.style.display = 'none'
                j3pElement(stor.idCible).parentNode.removeChild(j3pElement(stor.idCible))
                dG.conteneur.appendChild(dG.etiquetteSoluce)
                j3pAddContent(stor.zoneExpli, String(stor.stockage.droiteG.valeurSoluce))
                me._stopTimer()
                me.cacheBoutonValider()
                me.typederreurs[2]++
                me.etat = 'navigation'
                me.sectionCourante()
              }
            }
          }
        }
      }
      // Obligatoire
      me.finCorrection()
      break // case "correction":

    case 'navigation':

      if (me.sectionTerminee()) {
        // On détermine la phrase d’état renvoyée par la section
        me.parcours.pe = me.score / ds.nbitems
        me.afficheBoutonSectionSuivante()
        me.focus('sectioncontinuer')
      } else {
        me.etat = 'enonce'
        me.afficheBoutonSuite()
        me.focus('boutoncontinuer')
      }
      // Obligatoire
      me.finNavigation()

      break // case "navigation":
  }
}
