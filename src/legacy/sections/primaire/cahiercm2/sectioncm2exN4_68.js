import { j3pAddElt, j3pBarre, j3pChaine, j3pFocus, j3pFreezeElt, j3pGetBornesIntervalle, j3pGetRandomInt, j3pStyle, j3pValeurde, j3pVirgule } from 'src/legacy/core/functions'
import { j3pAffiche, j3pDesactive, mqRestriction } from 'src/lib/mathquill/functions'
import textesGeneriques from 'src/lib/core/textes'

const { cBien, cFaux, essaieEncore, regardeCorrection, tempsDepasse } = textesGeneriques

/*

    Auteur : Rémi ANGOT
    Date : 12/01/2013 crée le 10/01/2013
    Version : 0.1.3
    Reste à faire : valider la réponse en appuyant sur entrée, taille du champ texte
    Adapté au clavier virtule par Yves en 10-2021
*/

export const params = {
  outils: ['calculatrice', 'mathquill'],
  /**
   * Ce qui est paramétrable dans l’éditeur de graphe
   * @type {LegacySectionParamsParametres}
   */
  parametres: [
    ['nbrepetitions', 5, 'entier', 'Nombre de questions'],
    ['indication', '', 'string', 'Indice éventuel'],
    ['limite', 0, 'entier', 'Temps disponible par répétition, en secondes'],
    ['nbchances', 2, 'entier', 'Nombre d’essai(s) possible(s)']
  ]
}
/**
 * Ajouter une description ici
 * @fileOverview
 * @author Tommy Barroy <t.barroy@laposte.net>
 * @since 2017-10
*/
export default function main () {
  const me = this
  let ds = me.donneesSection
  const stor = me.storage
  function getDonnees () {
    return {

      nbrepetitions: 5,
      typesection: 'primaire',
      nbetapes: 1,
      indication: '',
      nbchances: 2,
      unparametre: 0,
      structure: 'presentation1',
      nbdecimales: '[0;2]',
      partie_entiere: '[1;100]',
      textes: {
        titre_exo: 'L’entier qui suit ou qui précède - (N4 F6 ex8)',
        phrase1: 'Complète avec l’entier qui suit ou qui précède :'
      }
    }
  }
  function enonceMain () {
    const [intMin0, intMax0] = j3pGetBornesIntervalle(ds.nbdecimales)
    const nombreDecimales = j3pGetRandomInt(intMin0, intMax0)
    const [intMin1, intMax1] = j3pGetBornesIntervalle(ds.partie_entiere)
    let a = j3pGetRandomInt(intMin1, intMax1)
    if (nombreDecimales !== 0) {
      const maxDecimales = Math.pow(10, nombreDecimales) - 1
      a += j3pGetRandomInt(1, maxDecimales) / Math.pow(10, nombreDecimales)
    }

    const quisuit = j3pGetRandomInt(0, 1) // booléen 1 si on demande le nombre qui suit, 0 si on demande le nombre qui précède
    stor.conteneur = j3pAddElt(me.zonesElts.MG, 'div', '', { style: me.styles.etendre('petit.enonce', { padding: '20px' }) })
    j3pAddElt(stor.conteneur, 'div', ds.textes.phrase1)
    const zoneCons2 = j3pAddElt(stor.conteneur, 'div')
    let elt
    if (quisuit === 1) {
      stor.stockage[0] = parseInt(a) + 1
      elt = j3pAffiche(zoneCons2, '', '£x < &1&', { x: j3pVirgule(a), inputmq1: { texte: '', dynamique: true } })
      stor.solution = '£x < £y'
    } else {
      if (a === parseInt(a)) stor.stockage[0] = parseInt(a) - 1
      else stor.stockage[0] = parseInt(a)
      elt = j3pAffiche(zoneCons2, '', '&1& < £x', { x: j3pVirgule(a), inputmq1: { texte: '', dynamique: true } })
      stor.solution = '£y < £x'
    }
    stor.a = a
    stor.zoneInput = elt.inputmqList[0]
    mqRestriction(stor.zoneInput, '\\d')
    // fin de l’énoncé

    me.etat = 'correction'
    j3pFocus(stor.zoneInput)

    stor.zoneCorr = j3pAddElt(me.zonesElts.MD, 'div', '', { style: me.styles.etendre('petit.correction', { padding: '10px' }) })
    me.finEnonce()
  }
  switch (me.etat) {
    case 'enonce':
      if (me.debutDeLaSection) {
        me.donneesSection = getDonnees()
        ds = me.donneesSection
        me.surcharge()
        me.construitStructurePage({ structure: ds.structure, ratioGauche: 0.6 })
        stor.stockage = [0, 0, 0, 0]
        me.typederreurs = [0, 0, 0, 0, 0, 0]
        me.score = 0

        me.afficheTitre(ds.textes.titre_exo)
        if (ds.indication) me.indication(me.zonesElts.IG, ds.indication)
      } else {
        me.videLesZones()
      }

      // ecriture de l’énoncé
      enonceMain()
      break // case "enonce":

    case 'correction':

      // me.cacheBoutonValider()
      {
        let cbon
        // on teste si une réponse a été saisie
        if (j3pValeurde(stor.zoneInput) === '') {
          me.reponseManquante(stor.zoneCorr)
          j3pFocus(stor.zoneInput)
        } else { // une réponse a été saisie
          if ((me.questionCourante % ds.nbetapes) === 1) {
            cbon = true
          }

          if ((me.questionCourante % ds.nbetapes) === 0) {
            cbon = (Number(j3pValeurde(stor.zoneInput)) === Number(stor.stockage[0]))
          }

          if (cbon) {
            me.score++
            stor.zoneCorr.style.color = me.styles.cbien
            stor.zoneCorr.innerHTML = cBien
            stor.zoneInput.style.color = me.styles.cbien
            j3pDesactive(stor.zoneInput)
            j3pFreezeElt(stor.zoneInput)
            me.typederreurs[0]++
            me.etat = 'navigation'
            me.sectionCourante()
          } else {
          // Pas de nonne réponse
            stor.zoneCorr.style.color = me.styles.cfaux
            stor.zoneInput.style.color = me.styles.cfaux
            // à cause de la limite de temps :
            if (me.isElapsed) { // limite de temps
              stor.zoneCorr.innerHTML = tempsDepasse
              me.typederreurs[10]++
              j3pDesactive(stor.zoneInput)
              j3pFreezeElt(stor.zoneInput)
              me.etat = 'navigation'
              me.sectionCourante()
            } else {
            // réponse fausse :
              stor.zoneCorr.innerHTML = cFaux
              // S’il y a deux chances,on appelle à nouveau le bouton Valider
              if (me.essaiCourant < ds.nbchances) {
                stor.zoneCorr.innerHTML += '<br>' + essaieEncore
                me.typederreurs[1]++
                // indication éventuelle ici
                me.etat = 'correction'
                j3pFocus(stor.zoneInput)
                me.afficheBoutonValider()// c’est la partie "correction" de la section qui sera exécutée
              } else {
              // Erreur au second essai
                me.etat = 'navigation'
                stor.zoneCorr.innerHTML += '<br>' + regardeCorrection
                me.typederreurs[2]++
                // on corrige ici
                j3pDesactive(stor.zoneInput)
                j3pBarre(stor.zoneInput)
                const laSol = j3pAddElt(stor.conteneur, 'div', j3pChaine(stor.solution, { x: j3pVirgule(stor.a), y: stor.stockage[0] }))
                j3pStyle(laSol, { color: me.styles.petit.correction.color })
                me.sectionCourante()// c’est la partie "navigation" de la section qui sera exécutée
              }
            }
          }
        }
      }
      me.finCorrection()
      break // case "correction":

    case 'navigation':
      if (!me.sectionTerminee()) me.etat = 'enonce'
      // rien à faire quand la section est terminée.
      me.finNavigation(true)

      break // case "navigation":
  }
}
