import { j3pAddContent, j3pAddElt, j3pChaine, j3pCurseur, j3pGetRandomBool, j3pGetRandomInt, j3pParseInter, j3pStyle } from 'src/legacy/core/functions'
import Horloge from 'src/legacy/outils/horloge/Horloge'
import textesGeneriques from 'src/lib/core/textes'

const { cBien } = textesGeneriques

/*
 * Auteur : Dominique Chiariello
 * Date : Janvier 2013
 * Modifications : JP Vanroyen
 */

export const params = {
  outils: [],
  /**
   * Ce qui est paramétrable dans l’éditeur de graphe
   * @type {LegacySectionParamsParametres}
   */
  parametres: [
    ['nbrepetitions', 5, 'entier', 'aucun'],
    ['indication', '', 'string', 'aucun'],
    ['limite', 0, 'entier', 'aucun'],
    ['h', '[0;11]', 'string', 'intervalle d’appartenance de l’heure demandée'],
    ['m', '[0;59]', 'string', 'intervalle d’appartenance de la minute demandée'],
    ['couleurpetiteaiguille', 'rouge', 'string', ' "rouge" || "bleu" || "noir"'],
    ['couleurgrandeaiguille', 'bleu', 'string', ' "rouge" || "bleu" || "noir"'],
    ['type', 'jour', 'liste', 'matin||jour <br> Si type=jour, alors on précise si on est le matin ou l’après-midi.<br>Sinon on précise toujours qu’on est le matin', ['matin', 'jour']],
    ['debutant', false, 'boolean', 'Si TRUE alors on est toujours le matin et on NE demande QUE les minutes']
  ]
}

const textes = {
  titre_exo: 'Lire l’heure',
  matin: 'Nous sommes le matin.',
  apresmidi: 'Nous sommes l’après-midi.',
  heure: 'Quelle heure est-il ?',
  laSol: 'La solution est :',
  lApresMidi: 'C’est exact mais nous sommes l’après-midi et non le matin&nbsp;!',
  leMatin: 'C’est exact mais nous sommes le matin et non l’après-midi  !',
  pbHeures: 'Erreur sur les heures\n',
  pbMinutes: 'Erreur sur les minutes\n',
  erreur: 'La grande aiguille est entre £{i1} et £{i2}.\nDonc le nombre de minutes est compris entre £{i3} et £{i4}.'
}

/**
 * section cm2exM3_11
 * @this {Parcours}
 */
export default function main () {
  const me = this
  const ds = me.donneesSection
  const stor = me.storage

  function desactiveCurseur () {
    stor.lesInput.forEach(function (nom) {
      nom.disabled = true
    })
  }
  switch (me.etat) {
    case 'enonce':
      if (me.debutDeLaSection) {
        me.construitStructurePage('presentation1')
        if (ds.debutant) ds.type = 'matin'
        me.typederreurs = [0, 0, 0, 0, 0, 0]
        me.afficheTitre(textes.titre_exo)
        if (ds.indication) me.indication(me.zonesElts.IG, ds.indication)
        stor.couleurs = { rouge: 'red', bleu: 'blue', noir: 'black' }
      } else {
        me.videLesZones()
      }
      // creation de l’horloge
      stor.divHorloge = j3pAddElt(me.zonesElts.MG, 'div', '', {
        style: {
          padding: '30px',
          margin: '0 auto',
          width: '360px'
        }
      })
      stor.horloge = Horloge.create(stor.divHorloge, {
        echelle: 3,
        couleurs: {
          grande: stor.couleurs[ds.couleurgrandeaiguille],
          petite: stor.couleurs[ds.couleurpetiteaiguille],
          cadran: '#000000',
          chiffres: '#000000',
          fondInt: '#F6FAFE',
          fondExt: '#F6FAFE'
        },
        tailleChiffres: 11
      })

      switch (ds.type) {
        case 'jour':
          stor.periode = j3pGetRandomBool() ? 'AM' : 'PM'
          break
        default:
          stor.periode = 'AM'
      }
      {
        let consigne = (stor.periode === 'AM') ? textes.matin : textes.apresmidi
        consigne += ' ' + textes.heure
        j3pAddElt(me.zonesElts.MG, 'div', consigne, { style: me.styles.etendre('petit.enonce', { paddingLeft: '20px' }) })

        stor.divReponses = j3pAddElt(me.zonesElts.MG, 'div', '', { style: me.styles.petit.enonce })
        const repStyle = { display: 'inline-block', margin: '1em' }
        const divRepH = j3pAddElt(stor.divReponses, 'div', '', { style: repStyle })
        const divRepM = j3pAddElt(stor.divReponses, 'div', '', { style: repStyle })

        let bornes = j3pParseInter(ds.h)
        stor.heure = j3pGetRandomInt(bornes.min, bornes.max)
        bornes = j3pParseInter(ds.m)
        stor.minute = j3pGetRandomInt(bornes.min, bornes.max)

        if (ds.type === 'jour') {
          stor.curseurH = j3pCurseur(divRepH, { label: 'Heures', min: 0, max: 23, valeur: 0 })
          if (stor.periode === 'PM') stor.heure += 12
        } else {
          if (ds.debutant) {
            stor.curseurH = j3pCurseur(divRepH, { label: 'Heures', min: 0, max: 11, valeur: stor.heure })
            stor.curseurH.style.display = 'none'
          } else {
            stor.curseurH = j3pCurseur(divRepH, { label: 'Heures', min: 0, max: 11, valeur: 0 })
          }
        }
        stor.curseurM = j3pCurseur(divRepM, { label: 'Minutes', min: 0, max: 59, valeur: 0 })
        stor.lesInput = stor.divReponses.querySelectorAll('input')
        stor.lesInput.forEach(function (nom) {
          nom.style.fontSize = me.styles.petit.enonce.fontSize
        })
        stor.horloge.change(stor.heure, stor.minute)
        stor.zoneCorr = j3pAddElt(me.zonesElts.MD, 'div', '', { style: me.styles.etendre('petit.explications', { padding: '15px' }) })
        stor.zoneExpli = j3pAddElt(me.zonesElts.MD, 'div', '', { style: me.styles.petit.correction })
        j3pStyle(stor.zoneExpli, { padding: '15px' })
      }
      me.finEnonce()
      break // case "enonce":

    case 'correction': {
      j3pAddElt(me.zonesElts.MD, 'div', '', { id: 'correction' })
      const hr = Number(stor.curseurH.value)
      const mr = Number(stor.curseurM.value)
      let cbon = true
      if (hr !== stor.heure) {
        cbon = false
        if (ds.type === 'jour') {
          if (Math.abs(hr - stor.heure) === 12) {
            j3pAddContent(stor.zoneCorr, (stor.periode === 'PM') ? textes.lApresMidi : textes.leMatin)
          } else {
            j3pAddContent(stor.zoneCorr, textes.pbHeures)
          }
        } else {
          j3pAddContent(stor.zoneCorr, textes.pbHeures)
        }
      }
      if (Math.abs(mr - stor.minute) > 1) {
        cbon = false
        if (ds.debutant) {
          const inter = Math.floor(stor.minute / 5)
          j3pAddContent(stor.zoneCorr, j3pChaine(textes.erreur, { i1: inter, i2: inter + 1, i3: inter * 5, i4: 5 * (inter + 1) }))
        } else {
          j3pAddContent(stor.zoneCorr, textes.pbMinutes)
        }
      }
      if (cbon) {
        me.score++
        me.typederreurs[0]++
        j3pAddContent(stor.zoneCorr, cBien, { replace: true })
        stor.zoneCorr.style.color = me.styles.cbien
      } else {
        stor.zoneCorr.style.color = me.styles.cfaux
        j3pAddElt(stor.zoneCorr, 'br')
        j3pAddElt(stor.zoneCorr, 'br')
        j3pAddElt(stor.zoneExpli, 'span', textes.laSol, { style: { color: '#000' } })
        j3pAddElt(stor.zoneExpli, 'br')
        j3pAddElt(stor.zoneExpli, 'br')
        j3pAddElt(stor.zoneExpli, 'span', stor.heure + ' h', { style: { color: '#' + stor.couleurs[ds.couleurpetiteaiguille] } })
        j3pAddContent(stor.zoneExpli, ' ')
        j3pAddElt(stor.zoneExpli, 'span', stor.minute + ' min', { style: { color: '#' + stor.couleurs[ds.couleurgrandeaiguille] } })
      }
      desactiveCurseur()
      me.finCorrection('navigation', true)
      break // case "correction":
    }

    case 'navigation':
      if (!me.sectionTerminee()) {
        me.etat = 'enonce'
      }
      me.finNavigation()
      break // case "navigation":
  }
}
