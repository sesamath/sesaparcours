import { j3pAddElt, j3pNombre, j3pDetruit, j3pDiv, j3pFocus, j3pGetRandomInt, j3pNombreBienEcrit, j3pRandomdec, j3pRestriction, j3pVirgule, j3pStyle } from 'src/legacy/core/functions'
import ValidationZones from 'src/legacy/outils/calculatrice/ValidationZones'
import { j3pAffiche } from 'src/lib/mathquill/functions'
import TableauConversion from 'src/legacy/outils/tableauconversion/TableauConversion'
import textesGeneriques from 'src/lib/core/textes'

const { cBien, cFaux, essaieEncore, regardeCorrection, tempsDepasse } = textesGeneriques

/*
    Auteur : Rémi ANGOT
    Date : 12/01/2013
    Version : 0.1.1
    Reste à faire : valider la réponse en appuyant sur entrée, taille du champ texte

    CoAuteur : JP Vanroyen
    Correction à l’aide de l’outil tableau de conversion
    MAJ avec nouvelle structure
    Janvier 2013
    Revu Par Yves Biton le 7-11-2018 pour évier positions absolues (saut pour le tableau)
    Repris enfin par Rémi pour gérer le paramètre nbdecimales pour qu’on ne mette pas tout ce qu’on veut
*/

export const params = {
  outils: ['calculatrice', 'mathquill'],
  /**
   * Ce qui est paramétrable dans l’éditeur de graphe
   * @type {LegacySectionParamsParametres}
   */
  parametres: [
    ['nbrepetitions', 4, 'entier', 'aucun'],
    ['indication', '', 'string', 'aucun'],
    ['limite', 0, 'entier', 'Temps disponible par répétition, en secondes'],
    ['nbchances', 2, 'entier', 'Nombre d’erreurs possibles (pour tenter de les corriger)'],
    ['nbdecimales', '[2;3]', 'string', 'Intervalle dans lequel se trouvera la puissance de 10 par laquelle on divise (ne peut être supérieure à 6)'],
    ['nombre', '[9.9;10.1]', 'string', 'L’intervalle auquel appartient le nombre'],
    ['affichetableau', true, 'boolean', 'Le tableau est affiché systématiquement lors de la correction, même en cas de bonne réponse'],
    ['type', 3, 'entier', '1 || 2 || 3 || 4<br>1 : pas de tableau<br>2 : tableau statique<br>3 : tableau dynamique<br>4: tableau statique comportant le nombre'],
    ['sansEspaceAccepte', true, 'boolean', 'true signifie qu’on accepte les réponses où l’élève n’aurait pas mis les espaces.']
  ]
}
const textes = {
  titre_exo: 'Fraction décimale - (N4 F2 ex2)',
  phrase1: 'Complète avec l’écriture décimale :',
  phrase2: 'Supprime les zéros inutiles.',
  info1: 'Tu peux t’aider en regardant le tableau et en y plaçant mentalement le nombre £a.',
  info2: 'Tu peux t’aider en plaçant correctement dans le tableau le nombre £a.',
  info3: 'Tu peux t’aider en regardant le tableau dans lequel a été placé correctement le nombre £a.',
  comment1: 'Attention à bien écrire le nombre !',
  comment2: 'Attention tout de même à mettre l’espace pour bien écrire le nombre !',
  corr1: 'Donc $£f$=£s.'
}

/**
 * section
 * @this {Parcours}
 */
export default function main () {
  const me = this
  const stor = me.storage
  const ds = me.donneesSection

  // fonction appelée pour afficher la correction (que la réponse soit bonne ou fausse
  // le style n’est pas le même suivant que la réponse soit bonne ou fausse'
  function afficheCorrection (bonneReponse) {
    const zoneExpli = j3pAddElt(stor.conteneur, 'div', '', { style: me.styles.petit.correction })
    j3pStyle(zoneExpli, { paddingTop: '15px', color: (bonneReponse) ? me.styles.cbien : me.styles.toutpetit.correction.color })
    if (ds.type > 1) {
      j3pDetruit(stor.leTableau)
      j3pDetruit(stor.info)
    }
    const divSolution = j3pAddElt(zoneExpli, 'div', '', { style: { margin: '5px' } })
    j3pAffiche(divSolution, '', textes.corr1,
      {
        f: stor.fraction,
        s: j3pVirgule(stor.zoneInput.reponse[0])
      })
    const divTabConv = j3pAddElt(zoneExpli, 'div', '', { style: { position: 'relative' } })
    const unit = 11 + stor.nbDecimales
    // eslint-disable-next-line no-new
    new TableauConversion(divTabConv,
      {
        classes: [3, 4],
        format: {
          virgule: [false, true],
          epaisseurs: {},
          couleurs: {}
        },
        lignes: [
          TableauConversion.creeLigne(stor.numerateur, unit, false, [3, 4]),
          TableauConversion.creeLigne(stor.numerateur, unit, true, [3, 4])
        ]
      }
    )
  }

  function enonceMain () {
    stor.numEssai = 1
    // ecriture de l’énoncé
    const nbDecimales = j3pGetRandomInt(stor.nbDecimalesMin, stor.nbDecimalesMax)
    const unnombre = j3pRandomdec(ds.nombre + ',' + nbDecimales)
    const numerateur = Math.round(unnombre * Math.pow(10, nbDecimales))
    stor.conteneur = j3pDiv(me.zonesElts.MG, { style: me.styles.etendre('moyen.enonce', { padding: '10px' }) })
    j3pAddElt(stor.conteneur, 'p', textes.phrase1)
    const zoneCons2 = j3pAddElt(stor.conteneur, 'div')
    const elt = j3pAffiche(zoneCons2, '', '$\\frac{£n}{£d}$ = @1@', {
      n: numerateur,
      d: Math.pow(10, nbDecimales),
      input1: { dynamique: true }
    })
    stor.zoneInput = elt.inputList[0]
    stor.numerateur = numerateur
    stor.nbDecimales = nbDecimales // on stocke les deux pour ne pas avoir à multiplier des nombres décimaux
    stor.fraction = '\\frac{' + numerateur + '}{' + Math.pow(10, nbDecimales) + '}'
    j3pRestriction(stor.zoneInput, '0-9,. ')
    j3pFocus(stor.zoneInput)
    stor.zoneInput.typeReponse = ['texte']
    const laRep = stor.numerateur / Math.pow(10, stor.nbDecimales)
    stor.zoneInput.reponse = (ds.sansEspaceAccepte)
      ? [j3pNombreBienEcrit(laRep), laRep, j3pVirgule(laRep)]
      : [j3pNombreBienEcrit(laRep)]
    stor.validationZones = new ValidationZones({ parcours: me, zones: [stor.zoneInput.id] })
    stor.zoneInput.onkeyup = function () {
      const selStart = this.selectionStart
      const selEnd = this.selectionEnd
      this.value = this.value.replace(/\./g, ',')
      this.selectionStart = selStart
      this.selectionEnd = selEnd
    }

    stor.zoneCorr = j3pAddElt(me.zonesElts.MD, 'div', '', { style: me.styles.etendre('petit.correction', { padding: '10px' }) })

    if (ds.type !== 1) {
      stor.info = j3pAddElt(stor.conteneur, 'div', '', {
        style: me.styles.etendre('moyen.explications', {
          background: '#CCC',
          padding: '5px',
          margin: '5px'
        })
      })
    }
    stor.leTableau = j3pAddElt(stor.conteneur, 'div', '', me.styles.moyen.explications)
    let tabUnites, tabjusqueMilliemes, tabjusqueMillioniemes
    if ((ds.type === 2) || (ds.type === 3)) {
      tabUnites = (ds.type === 2) ? ['.', '.', '.'] : ['?_', '?_', '?_']
      tabjusqueMilliemes = (ds.type === 2) ? ['.', '.', '.'] : ['?_', '?_', '?_']
      tabjusqueMillioniemes = (nbDecimales > 3) ? (ds.type === 2) ? ['.', '.', '.'] : ['?_', '?_', '?_'] : []
    }
    if (ds.type === 2) {
      j3pAffiche(stor.info, '', textes.info1, { a: numerateur })
      // eslint-disable-next-line no-new
      new TableauConversion(stor.leTableau,
        {
          classes: [3, 4],
          format: {
            epaisseurs: {},
            couleurs: {},
            largeurcol: 60,
            hcol: 60,
            labelscolsdecimaleslettres: true

          },
          lignes: [[[], [], [], tabUnites, tabjusqueMilliemes, tabjusqueMillioniemes]]
        }
      )
    } else if (ds.type === 3) {
      j3pAffiche(stor.info, '', textes.info2, { a: numerateur })
      // eslint-disable-next-line no-new
      new TableauConversion(stor.leTableau,
        {
          classes: [3, 4],
          format: {
            epaisseurs: {},
            couleurs: {},
            largeurcol: 60,
            hcol: 60,
            labelscolsdecimaleslettres: true
          },
          lignes: [[[], [], [], tabUnites, tabjusqueMilliemes, tabjusqueMillioniemes]]
        }
      )
    } else if (ds.type === 4) {
      j3pAffiche(stor.info, '', textes.info3, { a: numerateur })
      let unit
      switch (stor.nbDecimales) {
        case 1:
          unit = 12
          break
        case 2:
          unit = 13
          break
        case 3:
          unit = 14
          break
      }
      const untab = TableauConversion.creeLigne(stor.numerateur, unit, false, [3, 4])
      // eslint-disable-next-line no-new
      new TableauConversion(stor.leTableau,
        {
          classes: [3, 4],
          format: {
            virgule: [false],
            epaisseurs: {},
            couleurs: {},
            largeurcol: 60,
            hcol: 60,
            labelscolsdecimaleslettres: true
          },
          lignes: [untab]
        }
      )
    }
    me.finEnonce()
  }
  switch (me.etat) {
    case 'enonce':
      if (me.debutDeLaSection) {
        me.construitStructurePage('presentation1')
        me.afficheTitre(textes.titre_exo)
        // on accepte un intervalle d’entiers entre 1 et 6
        const chunks = /^\[([1-6]);([1-6])]$/.exec(ds.nbdecimales)
        if (chunks && chunks[1] < chunks[2]) {
          stor.nbDecimalesMin = chunks[1]
          stor.nbDecimalesMax = chunks[2]
        } else {
          console.error(Error('Paramètre nbdecimales invalide (doit être un intervalle d’entiers entre 1 et 6 max)'))
          stor.nbDecimalesMin = 2
          stor.nbDecimalesMax = 3
        }
        if (ds.indication) me.indication(me.zonesElts.IG, ds.indication)
      } else {
        me.videLesZones()
      }
      enonceMain()
      break // case "enonce":
    case 'correction': {
      const validationZones = stor.validationZones
      let reponse = { aRepondu: false, bonneReponse: false }
      reponse.aRepondu = validationZones.valideReponses()
      const isLastChance = stor.numEssai === ds.nbchances
      let pbZeroInutile = false
      if (reponse.aRepondu) {
        // on vérifie qu’il n’y a pas de zéro inutile
        let repDonnee = validationZones.zones.reponseSaisie[0]
        let repAttendue = stor.numerateur / Math.pow(10, stor.nbDecimales)
        while (repDonnee.includes(' ')) {
          repDonnee = repDonnee.replace(' ', '')
        }
        repAttendue = String(repAttendue).replace('.', ',')
        pbZeroInutile = ((Math.abs(j3pNombre(repDonnee) - j3pNombre(repAttendue)) < Math.pow(10, -10)) && (repAttendue !== repDonnee))
        reponse.aRepondu = !pbZeroInutile
      }
      if (reponse.aRepondu) {
        reponse = validationZones.validationGlobale(isLastChance)
      }
      // on teste si une réponse a été saisie
      if ((!reponse.aRepondu) && (!me.isElapsed)) {
        let msgReponseManquante
        if (pbZeroInutile) {
          msgReponseManquante = textes.phrase2
        }
        me.reponseManquante(stor.zoneCorr, msgReponseManquante)
      } else { // une réponse a été saisie
        if (reponse.bonneReponse) {
          me._stopTimer()
          me.cacheBoutonValider()
          me.score++
          stor.zoneCorr.style.color = me.styles.cbien
          stor.zoneCorr.innerHTML = cBien
          // Même si la réponse est bonne on peut afficher des éléments de correction/explication :
          if (validationZones.zones.reponseSaisie[0] !== validationZones.zones.inputs[0].reponse[0]) {
            stor.zoneCorr.innerHTML += '<br/>' + textes.comment2
          }
          if (ds.affichetableau) {
            afficheCorrection(true)
          }

          me.etat = 'navigation'
          me.sectionCourante()
        } else {
          // Pas de bonne réponse
          stor.zoneCorr.style.color = me.styles.cfaux
          // à cause de la limite de temps :
          if (me.isElapsed) { // limite de temps
            me._stopTimer()

            stor.zoneCorr.innerHTML = tempsDepasse
            // L’élève avait un temps limite mais ne l’a pas respecté. On affiche alors la correction
            afficheCorrection(false)
            me.etat = 'navigation'
            me.sectionCourante()
          } else {
            // réponse fausse :
            stor.zoneCorr.innerHTML = cFaux
            if (stor.numerateur / Math.pow(10, stor.nbDecimales) === Number(validationZones.zones.reponseSaisie[0])) {
              stor.zoneCorr.innerHTML += '<br/>' + textes.comment1
            }
            if (isLastChance) {
              // Erreur au dernier essai
              me._stopTimer()
              me.cacheBoutonValider()
              stor.zoneCorr.innerHTML += '<br>' + regardeCorrection
              // Là il ne peut plus se corriger. On lui affiche alors la solution
              afficheCorrection(false)
              me.etat = 'navigation'
              me.sectionCourante()
            } else {
              stor.zoneCorr.innerHTML += '<br>' + essaieEncore
              stor.numEssai++
              // on laisse l’utilisateur corriger et rappeler correction en cliquant ok
            }
          }
        }
      }
      break // case "correction":
    }

    case 'navigation':
      if (me.sectionTerminee()) {
        // On détermine la phrase d’état renvoyée par la section
        me.parcours.pe = me.score / ds.nbitems
        me.afficheBoutonSectionSuivante()
        me.focus('sectioncontinuer')
      } else {
        me.etat = 'enonce'
        me.afficheBoutonSuite()
        me.focus('boutoncontinuer')
      }
      me.finNavigation()
      break // case "navigation":
  }
}
