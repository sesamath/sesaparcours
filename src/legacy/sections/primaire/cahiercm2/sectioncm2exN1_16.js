import { j3pAddContent, j3pAddElt, j3pBarre, j3pChaine, j3pFocus, j3pFreezeElt, j3pNombreBienEcrit, j3pRestriction, j3pStyle, j3pSupprimeEspaces, j3pValeurde } from 'src/legacy/core/functions'
import { j3pAffiche, j3pDesactive } from 'src/lib/mathquill/functions'
import textesGeneriques from 'src/lib/core/textes'

const { cBien, essaieEncore, regardeCorrection, tempsDepasse } = textesGeneriques

/*
        Dominique Chiarello
        2012-2013
        Modififé par Yves Biton le 7-11-2018 (utilisation finEnonce() et plus de couleurs différentes dans la solution)
 */

const pe_1 = 'Bien'
const pe_2 = 'Confusion, tranches effectuées en commençant par la gauche'
const pe_3 = 'Insuffisant'

export const params = {
  outils: ['calculatrice', 'mathquill'],
  /**
   * Ce qui est paramétrable dans l’éditeur de graphe
   * @type {LegacySectionParamsParametres}
   */
  parametres: [
    ['nbrepetitions', 3, 'entier', 'Nombre de répétitions de la section (max 6)'],
    ['indication', '', 'string', 'Indication pour chaque répétition'],
    ['limite', 0, 'entier', 'Temps disponible par répétition, en secondes'],
    ['nbchances', 2, 'entier', 'Nombre d’essais par répétition'],
    ['avectext', true, 'boolean', 'Si false l’élève doit recopier le nombre (sinon le nombre est déjà dans la zone de saisie et il doit ajouter les espaces)'],
    ['seuilreussite', 0.7, 'number', 'decimal entre 0 et 1, seuil de réussite global'],
    ['seuiltrancheagauche', 0.4, 'number', 'decimal entre 0 et 1, seuil de confusion pour les tranches D/G']
  ],
  pe: [
    { pe_1 },
    { pe_2 },
    { pe_3 }
  ]
}

const nbChiffres = [5, 6, 7, 8, 9, 11]

const textes = {
  titre_exo: 'Espaces dans un nombre - (N1 F1 ex6)',
  phrase1: 'Recopie le nombre ci-dessous en plaçant <br>correctement les espaces :',
  phrase2: 'Il y a une erreur, corrige-la',
  phrase3: 'Il y a des erreurs, corrige-les',
  phrase5: 'Insère correctement les espaces :',
  phrase6: 'Il faut compter les chiffres pour placer les espaces à partir de la droite! ',
  phrase7: 'Attention tu t’es trompé en recopiant les chiffres&nbsp;!',
  phrase8: 'Attention tu t’es trompé en recopiant les chiffres&nbsp;! <br>Les espaces ne sont pas corrects.',
  phrase9: 'Les espaces ne sont pas corrects',
  corr1: 'La solution est : £s.',
  corr2: 'La solution est : £s.'
}

/**
 * Ajouter une description ici
 * @fileOverview
 * @author Tommy Barroy <t.barroy@laposte.net>
 * @since 2017-10
*/
export default function main () {
  const me = this
  const ds = me.donneesSection
  const stor = me.storage

  function enonceInitFirst () {
    if (ds.avectext === 'true') ds.avectext = true
    else if (ds.avectext === 'false') ds.avectext = false
    if (ds.nbrepetitions > 6) {
      console.error(Error('Cet exercice ne peut pas avoir plus de 6 questions'))
      ds.nbrepetitions = 6
    }
    me.construitStructurePage('presentation1')
    me.typederreurs = [0, 0, 0, 0, 0, 0]
    me.afficheTitre(textes.titre_exo)
    if (ds.indication) me.indication(me.zonesElts.IG, ds.indication)
  }

  switch (me.etat) {
    case 'enonce': {
      if (me.debutDeLaSection) {
        enonceInitFirst()
      } else {
        me.videLesZones()
      }
      // Donnees numériques
      const q = me.questionCourante - 1
      const nb = nbChiffres[q]
      const min = Math.pow(10, (nb - 1))
      const max = Math.pow(10, nb) - 1
      // console.log('tirage', q, nb, min, max)
      const nombre = min + Math.floor(Math.random() * (max - min + 1))

      stor.soluce = j3pNombreBienEcrit(nombre)
      // la consigne
      const st1 = me.styles.etendre('petit.enonce', { padding: '20px' })
      stor.conteneur = j3pAddElt(me.zonesElts.MG, 'div', '', { style: st1 })
      for (let i = 1; i <= 3; i++) stor['zoneCons' + i] = j3pAddElt(stor.conteneur, 'div')
      j3pStyle(stor.zoneCons2, { paddingTop: '40px' })

      // l’énoncé
      let elt
      if (!ds.avectext) {
        j3pAddContent(stor.zoneCons1, textes.phrase1)
        elt = j3pAffiche(stor.zoneCons2, '', nombre + ' ; @1@',
          {
            input1: {
              texte: '',
              dynamique: true,
              correction: j3pNombreBienEcrit(stor.soluce)
            }
          }
        )
      } else {
        j3pAddContent(stor.zoneCons1, textes.phrase5)
        elt = j3pAffiche(stor.zoneCons2, '', ' @1@',
          {
            input1: {
              texte: nombre,
              correction: stor.soluce, // type string requis donc c’est bon
              width: ((String(nombre).length + 2) * 18) + 'px'
            }
          }
        )
      }
      stor.zoneInput = elt.inputList[0]
      j3pRestriction(stor.zoneInput, '0-9 ')
      j3pFocus(stor.zoneInput)
      // La zone de feedback
      stor.zoneCorr = j3pAddElt(me.zonesElts.MD, 'div', '', { style: me.styles.etendre('petit.explications', { padding: '20px' }) })
      stor.zoneExpli = j3pAddElt(stor.conteneur, 'div', '', { style: me.styles.etendre('petit.correction', { paddingTop: '20px' }) })
      me.finEnonce()
      break // case "enonce":
    }

    case 'correction': {
      // on teste si une réponse a été saisie
      const repEleve = j3pValeurde(stor.zoneInput).trim()
      if (!repEleve && !me.isElapsed) {
        // pas de réponse
        me.reponseManquante(stor.zoneCorr)
      } else {
        const repelevecompacte = j3pSupprimeEspaces(repEleve)
        const resultcompacte = j3pSupprimeEspaces(stor.soluce)
        const motif = /^\d{0,3}(\s{1,5}\d{3}){1,5}$/

        if (repelevecompacte === resultcompacte && repEleve.match(motif)) {
          stor.zoneInput.style.color = me.styles.cbien
          j3pDesactive(stor.zoneInput)
          j3pFreezeElt(stor.zoneInput)
          me.score++
          stor.zoneCorr.style.color = me.styles.cbien
          stor.zoneCorr.innerHTML = cBien
          stor.zoneCorr.innerHTML += '<br/>' + j3pChaine(textes.corr1, { s: stor.soluce })
          me.typederreurs[0]++
          me.etat = 'navigation'
          me.sectionCourante()
        } else {
          // réponse fausse :
          stor.zoneCorr.style.color = me.styles.cfaux
          stor.zoneInput.style.color = me.styles.cfaux
          // S’il y a deux chances,on appelle à nouveau le bouton Valider
          if (me.essaiCourant < ds.nbchances) {
            stor.zoneCorr.innerHTML += '<br>' + essaieEncore
            me.typederreurs[1]++
            j3pFocus(stor.zoneInput)
            me.afficheBoutonValider()// c’est la partie "correction" de la section qui sera exécutée
          } else {
            // A cause de la limite de temps :
            if (me.isElapsed) { // limite de temps
              me._stopTimer()

              stor.zoneCorr.innerHTML = tempsDepasse
              me.typederreurs[10]++
              me.cacheBoutonValider()
              stor.zoneCorr.innerHTML += '<br/>' + j3pChaine(textes.corr2, { s: stor.soluce })
              /*
               *   RECOPIER LA CORRECTION ICI !
               */
              j3pDesactive(stor.zoneInput)
              j3pFreezeElt(stor.zoneInput)
              me.etat = 'navigation'
              me.sectionCourante()
            } else {
              if ((repelevecompacte !== resultcompacte) && repEleve.match(motif)) {
                // ne se realise pas normalement
                stor.zoneCorr.innerHTML = textes.phrase7
                me.typederreurs[3]++
              }
              const motif2 = /^(\d{3}\s{1,5}){1,4}\d{1,2}$/
              if (repEleve.match(motif2)) {
                stor.zoneCorr.innerHTML = textes.phrase6
                me.typederreurs[2]++
              }
              // Erreur au second essai

              j3pDesactive(stor.zoneInput)
              j3pBarre(stor.zoneInput)
              stor.zoneCorr.innerHTML += '<br>' + regardeCorrection
              stor.zoneExpli.innerHTML += j3pChaine(textes.corr2, { s: stor.soluce })

              me.typederreurs[2]++
              me.etat = 'navigation'
              me.sectionCourante()
            }
          }
        }
      } // fin
      me.finCorrection()
      break // case "correction":
    }

    case 'navigation':
      if (me.sectionTerminee()) {
        const pourcentagereussite = me.score / ds.nbitems
        if (pourcentagereussite >= ds.seuilreussite) {
          me.parcours.pe = pe_1
        } else {
          me.parcours.pe = pe_3
        }

        if ((me.typederreurs[2] / ds.nbitems) >= ds.seuiltrancheagauche) me.parcours.pe += '  ' + pe_2
      } else {
        me.etat = 'enonce'
      }
      me.finNavigation(true)
      break // case "navigation":
  }
}
