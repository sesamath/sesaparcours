import { j3pAddContent, j3pAddElt, j3pChaine, j3pEmpty, j3pGetBornesIntervalle, j3pGetRandomBool, j3pGetRandomInt, j3pStyle } from 'src/legacy/core/functions'
import Boulier from 'src/legacy/outils/boulier/boulier'
import textesGeneriques from 'src/lib/core/textes'

const { cBien, cFaux, essaieEncore, tempsDepasse } = textesGeneriques

/*
    JP Vanroyen
    2012-2013

*/

export const params = {
  outils: ['calculatrice'],
  /**
   * Ce qui est paramétrable dans l’éditeur de graphe
   * @type {LegacySectionParamsParametres}
   */
  parametres: [
    ['nbrepetitions', 5, 'entier', 'Nombre de répétitions'],
    ['indication', '', 'string', 'Indication éventuelle'],
    ['limite', 0, 'entier', 'Temps limite'],
    ['nbchances', 2, 'entier', 'Nombre de chances'],
    ['nbtiges', 6, 'entier', 'nombre de tiges : de 1 à 8'],
    ['type', 'lesdeux', 'liste', 'type de boulier', ['japonais', 'chinois', 'lesdeux']],
    ['nombre', '[10;999]', 'intervalle', 'intervalle d’appartenance du nombre demandé'],
    ['affichagenombre', false, 'boolean', 'nombre boulier affiché ou non']
  ]
}

/**
 * Ajouter une description ici
 * @fileOverview
 * @author Tommy Barroy <t.barroy@laposte.net>
 * @since 2017-10
*/
export default function main () {
  const me = this
  let ds = me.donneesSection
  const stor = me.storage

  function getDonnees () {
    return {
      typesection: 'primaire',
      nbrepetitions: 5,
      nbetapes: 1,
      indication: '',
      nbchances: 2,
      structure: 'presentation1',
      textes: {
        titre_exo: 'Boulier - (A1 F1 ex1)',
        phrase1: 'Inscris sur le boulier le nombre :',
        phrase2: 'Tu as inscrit £n.',
        phrase3: 'ce n’est pas ce qui était demandé !',
        phrase4: 'Voici la solution :'
      },
      L: 494,
      ep: 15,
      nbtiges: 4,
      diametre: 30,
      ept: 4,
      type: 'lesdeux', // "japonais" ||"lesdeux" ||"chinois"
      nombre: '[10;999]',
      boutonnombre: true,
      affichagenombre: false
    }
  } // getDonnees()
  function afficheCorr () {
    j3pAddContent(stor.zoneExpli, ds.textes.phrase4)
    me.typederreurs[2]++
    const boulier2 = new Boulier(
      {
        isChinese: stor.boulier.isChinese,
        width: 370,
        height: 270,
        epCadre: 10,
        nbTiges: ds.nbtiges,
        diametre: 23,
        epTige: 4,
        fige: true
      },
      stor.boulier2,
      { isBlue: true }
    )
    let laRep = stor.boulier.nbToEtat(stor.reponseAttendue)
    for (let i = String(stor.reponseAttendue).length; i < ds.nbtiges; i++) laRep = '00' + laRep
    boulier2.affiche(laRep)
    stor.boulier.fige = true
  }

  function enonceMain () {
    stor.conteneur = j3pAddElt(me.zonesElts.MG, 'div', '', { style: me.styles.etendre('moyen.enonce', { padding: '20px' }) })
    const conteneur = j3pAddElt(stor.conteneur, 'div')

    if (ds.affichagenombre) stor.leNombre = j3pAddElt(stor.conteneur, 'span')
    const [intMin, intMax] = j3pGetBornesIntervalle(ds.nombre)
    stor.reponseAttendue = j3pGetRandomInt(intMin, intMax) // réponse attendue
    stor.conteneurD = j3pAddElt(me.zonesElts.MD, 'div', '', { style: me.styles.etendre('petit.enonce', { padding: '15px' }) })
    stor.zoneCons1 = j3pAddElt(stor.conteneurD, 'div', ds.textes.phrase1)
    stor.zoneCons2 = j3pAddElt(stor.conteneurD, 'div')
    stor.zoneCons2.style.paddingLeft = '50px'
    const spanRep = j3pAddElt(stor.zoneCons2, 'span', stor.reponseAttendue)
    j3pStyle(spanRep, { color: 'red', fontWeight: 'bold' })

    let isChinese
    if (ds.type === 'lesdeux') {
      isChinese = j3pGetRandomBool()
    } else {
      isChinese = ds.type !== 'japonais'
    }

    stor.boulier = new Boulier(
      {
        isChinese,
        outputElement: stor.leNombre,
        width: ds.L,
        epCadre: ds.ep,
        nbTiges: ds.nbtiges,
        diametre: ds.diametre,
        epTige: ds.ept
      },
      conteneur,
      { hasResetButton: true }
    )
    let chch = ''
    for (let k = 1; k <= ds.nbtiges; k++) chch += '00'
    stor.boulier.affiche(chch, stor.leNombre)
    stor.zoneCorr = j3pAddElt(stor.conteneurD, 'div', '', { style: me.styles.etendre('petit.explications', { paddingTop: '30px' }) })
    stor.zoneExpli = j3pAddElt(stor.conteneurD, 'div', '', { style: me.styles.petit.correction })
    j3pStyle(stor.zoneExpli, { paddingTop: '10px' })
    stor.boulier2 = j3pAddElt(stor.conteneurD, 'div')
    j3pStyle(stor.boulier2, { paddingTop: '10px' })
    me.finEnonce()
  }
  switch (me.etat) {
    case 'enonce':
      if (me.debutDeLaSection) {
        me.donneesSection = getDonnees()
        ds = me.donneesSection
        me.surcharge()
        me.validOnEnter = false // ex donneesSection.touche_entree
        // on limite à 8…
        if (ds.nbtiges > 8) {
          console.warn('8 tiges max')
          ds.nbtiges = 8
        }

        me.construitStructurePage({ structure: ds.structure, ratioGauche: 0.55 })
        me.typederreurs = [0, 0, 0, 0, 0, 0]
        me.score = 0

        // me.videLesZones();
        me.afficheTitre(ds.textes.titre_exo)
        if (ds.indication) me.indication(me.zonesElts.IG, ds.indication)
      } else {
        me.videLesZones()
      }
      enonceMain()
      break // case "enonce":

    case 'correction': {
      j3pEmpty(stor.zoneCorr)
      // on teste si une réponse a été saisie
      const repEleve = stor.boulier.getNumber()
      if ((String(repEleve) === '') && (!me.isElapsed)) {
        // ne se produira jamais
        me.reponseManquante(stor.zoneCorr)
      } else { // une réponse a été saisie
        if (repEleve === stor.reponseAttendue) {
          me.score++
          stor.zoneCorr.style.color = me.styles.cbien
          stor.zoneCorr.innerHTML = cBien
          stor.boulier.fige = true
          me.typederreurs[0]++
          me.etat = 'navigation'
          me.sectionCourante()
        } else {
          // Pas de bonne réponse
          stor.zoneCorr.style.color = me.styles.cfaux
          // à cause de la limite de temps :
          if (me.isElapsed) { // limite de temps
            stor.zoneCorr.innerHTML = tempsDepasse
            me.typederreurs[10]++
            /*
                         *   RECOPIER LA CORRECTION ICI !
                         */
            afficheCorr()
            me.etat = 'navigation'
            me.sectionCourante()
          } else {
            // réponse fausse :
            stor.zoneCorr.innerHTML = cFaux
            stor.zoneCorr.innerHTML += '<br>' + j3pChaine(ds.textes.phrase2, { n: repEleve })

            // S’il y a deux chances,on appelle à nouveau le bouton Valider
            if (me.essaiCourant < ds.nbchances) {
              stor.zoneCorr.innerHTML += '<br>' + essaieEncore
              me.typederreurs[1]++
            } else {
              // Erreur au second essai
              afficheCorr()
              me.sectionCourante('navigation')
            }
          }
        }
      }
    }
      // Obligatoire
      me.finCorrection()
      break // case "correction":

    case 'navigation':
      if (!this.sectionTerminee()) {
        this.etat = 'enonce'
      }
      this.finNavigation()
      break // case "navigation":
  }
}
