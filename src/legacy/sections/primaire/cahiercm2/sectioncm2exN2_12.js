import { j3pAddContent, j3pAddElt, j3pAjouteDans, j3pEntierBienEcrit, j3pFocus, j3pGetNewId, j3pGetRandomInt, j3pNombreBienEcrit, j3pStyle, j3pStylepolice } from 'src/legacy/core/functions'
import ValidationZones from 'src/legacy/outils/calculatrice/ValidationZones'
import { j3pAffiche, mqRestriction } from 'src/lib/mathquill/functions'
import textesGeneriques from 'src/lib/core/textes'

const { cBien, cFaux, essaieEncore, regardeCorrection, tempsDepasse } = textesGeneriques

/*
        Auteur Alexis Lecomte
        Date mai 2013
        Remarques /*
 * Complément du cahier CM2
 * chapitre N2 exercice 2
 * Développeur : Alexis Lecomte
 * Section adaptée au clavier virtuel par Yves en 102021
 */

export const params = {
  outils: ['mathquill'],
  /**
   * Ce qui est paramétrable dans l’éditeur de graphe
   * @type {LegacySectionParamsParametres}
   */
  parametres: [
    ['nbrepetitions', 5, 'entier', 'aucun'],
    ['indication', '', 'string', 'aucun'],
    ['limite', 0, 'entier', 'Temps disponible par répétition, en secondes'],
    ['nbchances', 2, 'entier', 'Nombre d’erreurs possibles (pour tenter de les corriger)'],
    ['operation', '+', 'liste', 'opérations proposées', ['+', '*']],
    ['nbtermes', 3, 'entier', 'Nombre de termes (ou de facteurs), maximum 8 et minimum 2'],
    ['supersympathiques', true, 'boolean', 'Si, lorsqu’on a 4 termes, on veut avoir deux couples dont les sommes forment un couple de somme multiple de 100'],
    ['max_du_dernier', 9, 'entier', 'Lors d’un nombre impair de termes (ou facteurs), maximum du terme seul'],
    ['chamboule', true, 'boolean', 'Pour savoir si les nombres apparaissent dans le désordre'],
    ['aide_seconde_chance', true, 'boolean', 'Si nbchances>=2, en cas de seconde erreur, on colore les regroupements astucieux'],
    ['sommes', [20, 30, 40, 50, 60, 70, 80, 90, 100], 'array', 'liste des sommes (ou produits) possibles de deux nombres']
  ]

}

/**
 * section
 * @this {Parcours}
 */
export default function main () {
  const me = this
  let ds = me.donneesSection
  const stor = me.storage
  // pour colorer les nombres qui vont bien ensemble, pour aider en seconde étape...
  function coloreNombres () {
    for (let i = 0; i < stor.stockage[6].length - 1; i = i + 2) {
      j3pStylepolice(stor.tabIdCalc[2 * stor.stockage[4][i]], { couleur: 'rgb(' + (255 - 30 * i) + ',' + (30 * i) + ',' + (30 * i) + ')' })
      j3pStylepolice(stor.tabIdCalc[2 * stor.stockage[4][i + 1]], { couleur: 'rgb(' + (255 - 30 * i) + ',' + (30 * i) + ',' + (30 * i) + ')' })
    }
  }

  function afficheConsigne (style) {
    const tab = stor.stockage[6]
    stor.conteneur = j3pAddElt(me.zonesElts.MG, 'div', '', { style: me.styles.etendre('moyen.enonce', { padding: '20px' }) })
    const divCons0 = j3pAddElt(stor.conteneur, 'div')
    j3pAddContent(divCons0, ds.textes.phrase1)
    const divCons1 = j3pAddElt(stor.conteneur, 'div', '', { style: { paddingTop: '20px' } })
    const divLeCalcul = j3pAddElt(divCons1, 'div')
    stor.tabIdCalc = [j3pGetNewId('calcul0')]
    stor.calcul0 = j3pAddElt(divLeCalcul, 'span', { id: stor.tabIdCalc[0] })
    j3pAffiche(stor.calcul0, '', j3pEntierBienEcrit(tab[stor.stockage[3][0]]))
    if (tab.length > 1) {
      for (let i = 1; i < (2 * tab.length - 1); i++) {
        // pour pouvoir gérer les styles différemment
        stor.tabIdCalc.push(j3pGetNewId('calcul' + i))
        const spanCalc = j3pAddElt(divLeCalcul, 'span', { id: stor.tabIdCalc[i] })
        if (i % 2 === 1) {
          if (ds.operation === '+') {
            j3pAffiche(spanCalc, '', ' + ')
          }
          if (ds.operation === '*') {
            j3pAffiche(spanCalc, '', ' $\\times$ ')
          }
        } else {
          j3pAffiche(spanCalc, '', j3pEntierBienEcrit(tab[stor.stockage[3][i / 2]]))
        }
      }
    }
    j3pAffiche(divLeCalcul, '', ' = ')
    const elt = j3pAffiche(divLeCalcul, '', '&1&', {
      inputmq1: { maxchars: 3, dynamique: true, texte: '' }
    })
    stor.zoneInput = elt.inputmqList[0]
    mqRestriction(stor.zoneInput, '\\d')
    j3pFocus(stor.zoneInput)
    // pour la correction :
    stor.idcalcul0Bis = j3pGetNewId('calcul0Bis')
    stor.calcul0Bis = j3pAddElt(stor.conteneur, 'div', { id: stor.idcalcul0Bis })
  }

  function afficheCorrection (tab) {
    const autreCalcul = []
    for (let i = 0; i < (2 * tab.length - 1); i++) {
      // pour pouvoir gérer les styles différemmen
      const idAutreCalc = j3pGetNewId('autreCalc' + i)
      autreCalcul.push(j3pAddElt(stor.calcul0Bis, 'span', { id: idAutreCalc }))
      if (i % 2 === 1) {
        if (ds.operation === '+') j3pAjouteDans(autreCalcul[i], ' + ')
        if (ds.operation === '*') j3pAjouteDans(autreCalcul[i], ' × ')
      } else {
        j3pAjouteDans(autreCalcul[i], j3pEntierBienEcrit(stor.stockage[5][i / 2]))
      }
    }

    // }
    j3pAjouteDans(stor.calcul0Bis, ' = ' + j3pEntierBienEcrit(stor.solution))
    j3pStylepolice(stor.idcalcul0Bis, { couleur: 'blue' })
    for (let i = 0; i < (tab.length - 1) / 2; i++) {
      // on colore comme dans l’énoncé
      j3pStylepolice(autreCalcul[4 * i].id, { couleur: 'rgb(' + (255 - 60 * i) + ',' + (60 * i) + ',' + (60 * i) + ')' })
      j3pStylepolice(autreCalcul[4 * i + 1].id, { couleur: 'rgb(' + (255 - 60 * i) + ',' + (60 * i) + ',' + (60 * i) + ')' })
      j3pStylepolice(autreCalcul[4 * i + 2].id, { couleur: 'rgb(' + (255 - 60 * i) + ',' + (60 * i) + ',' + (60 * i) + ')' })
      // on souligne les opérations :
      autreCalcul[4 * i].style.borderBottom = 'thick solid rgb(' + (255 - 60 * i) + ',' + (60 * i) + ',' + (60 * i) + ''
      autreCalcul[4 * i + 1].style.borderBottom = 'thick solid rgb(' + (255 - 60 * i) + ',' + (60 * i) + ',' + (60 * i) + ''
      autreCalcul[4 * i + 2].style.borderBottom = 'thick solid rgb(' + (255 - 60 * i) + ',' + (60 * i) + ',' + (60 * i) + ''
      // je mets les résultats des opérations en dessous :
      // récupération du positionnement
      const coord1 = autreCalcul[4 * i].getBoundingClientRect()
      const coord2 = autreCalcul[4 * i + 2].getBoundingClientRect()
      const longueur = coord2.width
      const ref = stor.calcul0Bis.getBoundingClientRect()
      const ref2 = stor.conteneur.getBoundingClientRect()
      const leX = (coord1.x + longueur + coord2.x) / 2 - ref.x
      const leY = coord1.y - ref2.y + 40
      // l’affichage :
      const regroupement = (ds.operation === '*')
        ? j3pEntierBienEcrit(stor.stockage[5][2 * i] * stor.stockage[5][2 * i + 1])
        : j3pEntierBienEcrit(stor.stockage[5][2 * i] + stor.stockage[5][2 * i + 1])
      // création du DIV :
      const idRegroupement = j3pGetNewId('regroupement')
      const divRegroupement = j3pAddElt(stor.calcul0Bis, 'div', regroupement + '', { style: me.styles.petit.enonce, id: idRegroupement })
      j3pStyle(divRegroupement, { position: 'absolute', left: leX, top: leY })
      j3pStylepolice(idRegroupement, { couleur: 'rgb(' + (255 - 60 * i) + ',' + (60 * i) + ',' + (60 * i) + ')' })
      // on peut améliorer en centrant :
      const decal = divRegroupement.getBoundingClientRect().width
      divRegroupement.style.left = (leX - decal / 2) + 'px'
    }
  }

  function derange (tab) {
    stor.stockage[3] = []
    stor.stockage[4] = []
    if (ds.chamboule) {
      for (let i = 0; i <= tab.length - 1; i++) {
        stor.stockage[3][i] = j3pGetRandomInt(0, (tab.length - 1))
        for (let j = 0; j < i; j++) {
          if (stor.stockage[3][i] === stor.stockage[3][j]) {
            i--
          }
        }
      }
      // détermination de la permutation inverse
      for (let i = 0; i <= tab.length - 1; i++) {
        stor.stockage[4][stor.stockage[3][i]] = i
      }
    } else {
      for (let i = 0; i <= tab.length - 1; i++) {
        stor.stockage[3][i] = i
        stor.stockage[4][i] = i
      }
    }
  }

  function sommeTableau (tab) {
    let maSomme = (tab.length === 0) ? 0 : tab[0]
    for (let i = 1; i < tab.length; i++) {
      maSomme += tab[i]
    }
    return maSomme
  }

  function produitTableau (tab) {
    let monProduit = (tab.length === 0) ? 0 : tab[0]
    for (let i = 1; i < tab.length; i++) {
      monProduit = monProduit * tab[i]
    }
    return monProduit
  }

  function getDonnees () {
    return {
      typesection: 'primaire',

      // Nombre de répétitions de l’exercice
      nbrepetitions: 5,
      nbetapes: 1,
      indication: '',
      nbchances: 2,
      // nbtermes est minimum 3 (max 8 ?). Si impair, on se sert de max_du_dernier pour le dernier nombre sinon on a que des couples de nb sympathiques
      nbtermes: 3,
      // Dans le cas où le nbtermes vaut 4, on peut avoir deux couples de nb sympathiques
      // dont les sommes forment un couple de nombre supersympathiques (somme multiple de 100)
      supersympathiques: true,
      // sommes - ou produits -  possibles pour les deux nb sympathiques, surchargeables
      sommes: [20, 30, 40, 50, 60, 70, 80, 90, 100],
      // maximum pour le troisième nombre, surchargeable
      max_du_dernier: 9,
      // 1 ou deux essais, paramétrable, inopérant si le calcul s’efface (cad si temps_affichage existe)
      // affichage dans le desordre :
      chamboule: true,
      // pour savoir si l’on affiche les regroupements en cas de seconde chance :
      aide_seconde_chance: true,
      // pourcentage de réussite envoyé
      // temps limite si besoin, paramétrable : (non implémenté)
      // temps_affichage=2;
      // type d’opération, + ou *
      operation: '+',
      // limite: 5;
      structure: 'presentation1',
      // ptet à supprimer :
      // changement: false;
      // Boolean précisant que les coordonnées doivent être changées par le prog afin d'être adaptées à la nouvelle strucuture.
      // Concerne les sections développées avec l’ancienne structure et passées dans la nouvelle donc implique structure="presentation1";
      // L’alternative consiste à changer les coordonnées dans le code.
      // Pour les sections développées avec la nouvelle structure (structure="presentation1";) ce boolean doit valoir False
      limite: '',
      // nbdecimales: "[1;3]";
      textes: {
        titre_exo: 'Calculs astucieux  - (N2 F1 ex2)',
        phrase1: 'Calcule (pense à regrouper !):<br>',
        phrase2: 'Observe bien ce calcul, il va disparaître !<br>'

      }
    }
  }
  function enonceMain () {
  // Garde-fou :
    if (ds.nbtermes < 2) {
      ds.nbtermes = 2
    }
    if (ds.nbtermes > 8) {
      ds.nbtermes = 8
    }
    const tabNombres = []
    let alea, sommePossible, nb1, nb2, sommePossible2, nb3, nb4
    // Cas particulier si on a 4 termes avec des couples supersympathiques
    if (ds.operation === '+' && ds.nbtermes === 4 && ds.supersympathiques) {
      do {
        alea = j3pGetRandomInt(0, (ds.sommes.length - 1))
        sommePossible = ds.sommes[alea]
        nb1 = j3pGetRandomInt(1, (sommePossible - 1))
        nb2 = sommePossible - nb1
        const alea2 = j3pGetRandomInt(0, (ds.sommes.length - 1))
        sommePossible2 = ds.sommes[alea2]
        nb3 = j3pGetRandomInt(1, (sommePossible2 - 1))
        nb4 = sommePossible2 - nb3
      } while (Math.abs(Math.round((sommePossible + sommePossible2) / 100) - (sommePossible + sommePossible2) / 100) > Math.pow(10, -7))
      tabNombres.push(nb1, nb2, nb3, nb4)
    } else {
      for (let i = 0; i < Math.floor(ds.nbtermes / 2); i++) {
        // sommes ou produits possibles :
        if (ds.operation === '+') {
          do {
            alea = j3pGetRandomInt(0, (ds.sommes.length - 1))
            sommePossible = ds.sommes[alea]
            nb1 = j3pGetRandomInt(1, (sommePossible - 1))
            nb2 = sommePossible - nb1
            // conditions : nb2 entier, nb1 et nb2 non multiple de  10
          } while (Math.abs(nb1 / 10 - Math.round(nb1 / 10)) < Math.pow(10, -7))
        }
        if (ds.operation === '*') {
          do {
            alea = j3pGetRandomInt(0, (ds.sommes.length - 1))
            sommePossible = ds.sommes[alea]
            nb1 = j3pGetRandomInt(2, (sommePossible - 1))
            nb2 = sommePossible / nb1
            // conditions : nb2 entier, nb1 et nb2 non multiple de  10
          } while (Math.abs(nb2 - Math.round(nb2)) > Math.pow(10, -7) || Math.abs(nb1 / 10 - Math.round(nb1 / 10)) < Math.pow(10, -7) || Math.abs(nb2 / 10 - Math.round(nb2 / 10)) < Math.pow(10, -7))
        }
        tabNombres.push(nb1, nb2)
      }
    }
    if (ds.nbtermes % 2 === 1) {
      // nb de termes impairs, j’en ajoute donc un :
      alea = (ds.operation === '*')
        ? j3pGetRandomInt(2, ds.max_du_dernier)
        : j3pGetRandomInt(1, ds.max_du_dernier)
      tabNombres.push(alea)
    }
    if (ds.operation === '+') {
      stor.solution = sommeTableau(tabNombres)
    }
    if (ds.operation === '*') {
      stor.solution = produitTableau(tabNombres)
    }
    // je garde la mémoire du tableau non dérangé...
    stor.stockage[5] = tabNombres
    // je dérange (ou pas suivant chamboule) l’ordre du tableau :
    derange(tabNombres)
    // tableau dérangé :
    stor.stockage[6] = tabNombres

    afficheConsigne('enonce')

    // pour la correction :
    stor.stockage[2] = tabNombres
    //   console.log("TABLEAU NON DERANGE="+stor.stockage[5])
    stor.zoneCorr = j3pAddElt(me.zonesElts.MD, 'div', '', { style: me.styles.etendre('moyen.explications', { padding: '20px' }) })
    stor.zoneInput.typeReponse = ['texte']
    stor.zoneInput.reponse = [j3pNombreBienEcrit(stor.solution), String(stor.solution)]
    stor.fctsValid = new ValidationZones({ parcours: me, zones: [stor.zoneInput.id] })
    // Obligatoire
    me.finEnonce()
  }
  switch (me.etat) {
    case 'enonce':
      // code exécuté au lancement de la section
      if (me.debutDeLaSection) {
        me.donneesSection = getDonnees()
        ds = me.donneesSection
        me.surcharge()
        me.construitStructurePage({ structure: ds.structure, ratioGauche: 0.7 })
        stor.stockage = [0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0]
        /*
         Par convention,`
        `   me.typederreurs[0] = nombre de bonnes réponses
            me.typederreurs[1] = nombre de fois où l’élève a utilisé une chance supplémentaire
            me.typederreurs[2] = nombre de mauvaises réponses
            me.typederreurs[10] = nombre de fois où l’élève a eu faux à cause d’un temps limite
            LES AUTRES peuvent être utilisées par la détermination de la pe dans le cas d’une section qualitative
         */
        me.typederreurs = [0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0]
        me.score = 0
        me.afficheTitre(ds.textes.titre_exo)

        stor.stockage[20] = 'true'
        if (ds.indication) me.indication(me.zonesElts.IG, ds.indication)
      } else {
        // A chaque répétition, on nettoie la scène
        me.videLesZones()
      }

      enonceMain()

      break // case "enonce":

    case 'correction':

      // On teste si une réponse a été saisie
      {
        const fctsValid = stor.fctsValid
        const reponse = fctsValid.validationGlobale()
        if (!reponse.aRepondu && (!me.isElapsed)) {
          me.reponseManquante(stor.zoneCorr)
          j3pFocus(stor.zoneInput)
          me.afficheBoutonValider()
        } else {
        // Bonne réponse
          if (reponse.bonneReponse) {
            me._stopTimer()
            afficheCorrection(stor.stockage[2])

            me.score++
            stor.zoneCorr.style.color = me.styles.cbien
            stor.zoneCorr.innerHTML = cBien
            me.cacheBoutonValider()
            me.typederreurs[0]++
            me.etat = 'navigation'
            me.sectionCourante()
          } else {
          // Pas de bonne réponse
            stor.zoneCorr.style.color = me.styles.cfaux
            // A cause de la limite de temps :
            if (me.isElapsed) { // limite de temps
              me._stopTimer()
              stor.zoneCorr.innerHTML = tempsDepasse
              me.typederreurs[10]++
              me.cacheBoutonValider()
              afficheCorrection(stor.stockage[2])
              me.etat = 'navigation'
              me.sectionCourante()
            } else {
              stor.zoneCorr.innerHTML = cFaux
              // S’il y a plus de deux chances,on appelle à nouveau le bouton Valider
              if (me.essaiCourant < ds.nbchances) {
              // Cas particulier de cette section : au cas où aide_seconde_chance est à true, on colore le regroupement astucieux (cad qu’on recréé l’affichage)
                if (ds.aide_seconde_chance)coloreNombres()
                stor.zoneCorr.innerHTML += '<br>' + essaieEncore
                me.typederreurs[1]++
              // indication éventuelle ici
              } else {
                me._stopTimer()
                me.cacheBoutonValider()
                stor.zoneCorr.innerHTML += '<br>' + regardeCorrection
                me.typederreurs[2]++
                coloreNombres()
                afficheCorrection(stor.stockage[2])
                me.etat = 'navigation'
                me.sectionCourante()
              }
            }
          }
        }
      }
      // Obligatoire
      me.finCorrection()
      break // case "correction":

    case 'navigation':

      if (me.sectionTerminee()) {
        // On détermine la phrase d’état renvoyée par la section
        me.parcours.pe = me.score / ds.nbitems
        me.afficheBoutonSectionSuivante()
        me.focus('sectioncontinuer')
      } else {
        me.etat = 'enonce'
        me.afficheBoutonSuite()
        me.focus('boutoncontinuer')
      }
      // Obligatoire
      me.finNavigation()

      break // case "navigation":
  }
}
