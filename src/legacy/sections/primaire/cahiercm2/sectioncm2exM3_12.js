import $ from 'jquery'
import { j3pAddContent, j3pAddElt, j3pAjouteBouton, j3pChaine, j3pDetruit, j3pEspaces, j3pFocus, j3pGetBornesIntervalle, j3pGetRandomInt, j3pStyle, j3pValeurde } from 'src/legacy/core/functions'
import ValidationZones from 'src/legacy/outils/calculatrice/ValidationZones'
import Horloge from 'src/legacy/outils/horloge/Horloge'
import { j3pAffiche, mqRestriction } from 'src/lib/mathquill/functions'
import textesGeneriques from 'src/lib/core/textes'

const { cBien, cFaux, essaieEncore } = textesGeneriques

/*
 * Auteur : Dominique Chiariello
 * Date :  2013
 * REvu par Yves et Rémi en novemvre 2018
 */

const couleurs = { rouge: 'red', bleu: 'blue', noir: 'black' }

export const params = {
  outils: ['calculatrice', 'mathquill'],
  /**
   * Ce qui est paramétrable dans l’éditeur de graphe
   * @type {LegacySectionParamsParametres}
   */
  parametres: [
    ['nbrepetitions', 5, 'entier', 'aucun'],
    ['indication', '', 'string', 'aucun'],
    ['limite', 0, 'entier', 'aucun'],
    ['nbchances', 2, 'entier', 'Nombre d’erreurs possibles (pour tenter de les corriger)'],
    ['h1', '[0;11]', 'intervalle', 'intervalle d’appartenance du nombre d’heures de la première horloge'],
    ['h2', '[0;11]', 'intervalle', 'intervalle d’appartenance du nombre d’heures de la seconde horloge'],
    ['m1', '[0;59]', 'intervalle', 'intervalle d’appartenance du nombre de minutes de la première horloge'],
    ['m2', '[0;59]', 'intervalle', 'intervalle d’appartenance du nombre de minutes de la seconde horloge'],
    ['couleurPetiteAiguille', 'noir', 'liste', 'Couleur de la petite aiguille', Object.keys(couleurs)],
    ['couleurGrandeAiguille', 'noir', 'liste', 'Couleur de la grande aiguille', Object.keys(couleurs)],
    ['precision', 5, 'entier', 'Tolérance en nombre de minutes']
  ]
}

const textes = {
  titre_exo: 'Calcul d’une durée ',
  consigne1: 'Indique la durée écoulée entre les 2 horloges (tolérance = £a min).',
  consigne2: 'Durée écoulée : &1& h et &2& min',
  horloge1: 'Début',
  horloge2: 'Fin',
  comment1: 'La réponse aurait cependant pu être écrite plus simplement !',
  corr1_1: 'Il s’est écoulé £h heures et £m minutes.',
  corr1_2: 'Il s’est écoulé £h heure et £m minutes.',
  btn1: 'Suite de la solution',
  btn2: 'Voir la solution',
  // affichages successifs
  corr1: 'De £{h1} h £{m1} min à £{h2} h :<br>£e il s’est écoulé £v min.',
  corr2: 'De £{h1} h à £{h2} h :<br>£e il s’est écoulé £d h.',
  corr3: 'De £{h2} h à £{h2} h £{m2} min:<br>£e il s’est écoulé £v min.',
  corr4: 'De £{h1} h £{m1} min à £{h2} h :<br>£e il s’est écoulé £v min.',
  corr5: 'De £{h2} h à £{h2} h £{m2} min:<br>£e il s’est écoulé £v min.',
  corr6: 'De £{h1} h £{m1} min à £{h1} h £{m2} min, il s’est écoulé £v min.',
  corr7_1: 'La solution est £h h et £m min.',
  corr7_2: 'La solution est £m min.',
  corr7_3: 'Au final, on obtient £h et £m min.',
  corr7_4: 'Au final, on obtient £h.'
}

/**
 * section cm2exM3_12
 * @this {Parcours}
 */
export default function main () {
  const me = this
  const ds = me.donneesSection
  const stor = me.storage

  function rouge (nb) {
    return "<span style='color:#f00'>" + nb + '</span>'
  }

  function violet (nb) {
    return "<span style='color:#800080'>" + nb + '</span>'
  }

  function fa (a) {
    const m1 = stor.stockage[1]
    j3pAddContent(stor.zoneExpli1, a)
    j3pAddElt(stor.zoneExpli1, 'br')
    secteurm1(m1)
  }

  function fb (b) {
    const h1 = stor.stockage[0]
    const m1 = stor.stockage[1]
    let h2 = stor.stockage[2]
    const m2 = stor.stockage[3]
    if (h1 * 60 + m1 > h2 * 60 + m2) {
      h2 = h2 + 12
    }
    j3pAddContent(stor.zoneExpli1, b)
    j3pAddElt(stor.zoneExpli1, 'br')
    secteurh(h1 + 1, h2)
  }

  function fc (c) {
    const m2 = stor.stockage[3]
    j3pAddContent(stor.zoneExpli1, c)
    j3pAddElt(stor.zoneExpli1, 'br')
    secteurm2(m2)
  }

  function fd (d) {
    const m1 = stor.stockage[1]
    j3pAddContent(stor.zoneExpli1, d)
    j3pAddElt(stor.zoneExpli1, 'br')
    secteurm1(m1)
  }

  function fe (e) {
    const m2 = stor.stockage[3]
    j3pAddContent(stor.zoneExpli1, e)
    j3pAddElt(stor.zoneExpli1, 'br')
    secteurm2(m2)
  }

  function ff (f) {
    const m1 = stor.stockage[1]
    const m2 = stor.stockage[3]
    j3pAddContent(stor.zoneExpli1, f)
    j3pAddElt(stor.zoneExpli1, 'br')
    secteurm(m1, m2)
  }

  function fg (g) {
    j3pAddContent(stor.zoneExpli1, g)
    j3pAddElt(stor.zoneExpli1, 'br')
  }

  function afficheCorrection () {
    const zoneExpliL = j3pAddElt(stor.conteneur, 'div', '', { style: me.styles.petit.correction })
    j3pStyle(zoneExpliL, { paddingTop: '10px', color: (stor.score === 1) ? me.styles.cbien : me.styles.toutpetit.correction.color })
    const texteCorr = (stor.zoneInput[0].reponse[0] === 1) ? textes.corr1_2 : textes.corr1_1
    const zoneExpliL1 = j3pAddElt(zoneExpliL, 'div')
    j3pAffiche(zoneExpliL1, '', texteCorr, {
      h: stor.zoneInput[0].reponse[0],
      m: stor.zoneInput[1].reponse[0]
    })
    let h1 = stor.stockage[0]
    const m1 = stor.stockage[1]
    let h2 = stor.stockage[2]
    const m2 = stor.stockage[3]
    if (h1 * 60 + m1 > h2 * 60 + m2) {
      h2 = h2 + 12
    }

    const durees = (h2 * 60 + m2) - (h1 * 60 + m1)

    let minute1 = 0
    let minute2 = 0
    let heure = 0
    let a = ''
    let b = ''
    let c = ''
    let d = ''
    let e = ''
    let f = ''
    let g = ''
    let h = ''
    stor.liste = []

    if (durees >= 60) {
      if (m1 !== 0) {
        a = j3pChaine(textes.corr1, { h1, m1, h2: h1 + 1, e: j3pEspaces(9), v: violet(60 - m1) })
        stor.liste.push([fa, a])
        minute1 = 60 - m1
        h1++
      }
      if (h1 !== h2) {
        b = j3pChaine(textes.corr2, { h1, h2, e: j3pEspaces(9), d: rouge(h2 - h1) })
        stor.liste.push([fb, b])
        heure = h2 - h1
      }
      if (m2 !== 0) {
        c = j3pChaine(textes.corr3, { h2, m2, e: j3pEspaces(9), v: violet(m2) })
        stor.liste.push([fc, c])
        minute2 = m2
      }
    } else {
      if (h1 !== h2) {
        if (m1 !== 0) {
          d = j3pChaine(textes.corr4, { h1, m1, h2: (h1 + 1), e: j3pEspaces(9), v: violet(60 - m1) })
          stor.liste.push([fd, d])
          minute1 = 60 - m1
        }
        if (m2 !== 0) {
          e = j3pChaine(textes.corr5, { h2, m2, e: j3pEspaces(9), v: violet(m2) })
          stor.liste.push([fe, e])
          minute2 = m2
        }
      } else {
        f = j3pChaine(textes.corr6, { h1, m1, m2, v: violet(m2 - m1) })
        stor.liste.push([ff, f])
        minute1 = m2 - m1
        minute2 = 0
      }
    }

    if (minute1 + minute2 >= 60) {
      g = (heure === 0)
        ? j3pChaine(textes.corr7_2, { m: violet(minute1 + minute2) })
        : j3pChaine(textes.corr7_1, { h: rouge(heure), m: violet(minute1 + minute2) })
      g += '<br>'
      g += (minute1 + minute2 - 60 === 0)
        ? j3pChaine(textes.corr7_4, { h: rouge(heure + 1) })
        : j3pChaine(textes.corr7_3, { h: rouge(heure + 1), m: violet(minute1 + minute2 - 60) })
      stor.liste.push([fg, g])
    } else {
      h = (heure === 0)
        ? j3pChaine(textes.corr7_2, { m: violet(minute1 + minute2) })
        : j3pChaine(textes.corr7_1, { h: rouge(heure), m: violet(minute1 + minute2) })
      stor.liste.push([fg, h])
    }
    stor.zoneBtn = j3pAddElt(stor.zoneExpli, 'div')
    stor.zoneExpli1 = j3pAddElt(stor.zoneExpli, 'div')
    j3pAjouteBouton(stor.zoneBtn, afficheSuite.bind(this), { value: textes.btn2, className: 'MepBoutons' })
  }
  function afficheSuite (elt) {
    if (elt.target.value !== textes.btn1) elt.target.value = textes.btn1
    stor.liste[0][0](stor.liste[0][1])
    stor.liste.shift()
    if (stor.liste.length === 0) {
      j3pDetruit(elt.target)
    }
  }
  function secteurm1 (min) {
    let S = 37 * (Math.sin((2 * Math.PI * min) / 60)); let C = 37 * (Math.cos((2 * Math.PI * min) / 60)); let a = -S
    let b = -37 + C
    S = S.toFixed(2)
    C = C.toFixed(2)
    a = a.toFixed(2)
    b = b.toFixed(2)
    const ch = (min < 30)
      ? 'M50,50 l' + S + ' ' + (-C) + ' a37,37 0 1,1 ' + a + ',' + b + ' z'
      : 'M50,50 l' + S + ' ' + (-C) + ' a37,37 0 0,1 ' + a + ',' + b + ' z'
    const path = document.createElementNS('http://www.w3.org/2000/svg', 'path')
    path.setAttribute('d', ch)
    path.setAttribute('style', 'fill:#00f;fill-opacity:0.2')
    $('td > svg > g').append(path)
  }

  function secteurm2 (min) {
    const S = 37 * (Math.sin((2 * Math.PI * min) / 60)); const C = 37 * (Math.cos((2 * Math.PI * min) / 60)); let a = S; let b = 37 - C
    a = a.toFixed(2)
    b = b.toFixed(2)
    const ch = (min < 30)
      ? 'M50,50 v-37 a37,37 0 0,1  ' + a + ' ' + b + ' z'
      : 'M50,50 v-37 a37,37 0 1,1  ' + a + ' ' + b + ' z'
    const path = document.createElementNS('http://www.w3.org/2000/svg', 'path')
    path.setAttribute('d', ch)
    path.setAttribute('style', 'fill:#00f;fill-opacity:0.2')
    $('td > svg > g').append(path)
  }

  function secteurm (a1, a2) {
    const c = a1
    if (a2 < a1) {
      a1 = a2
      a2 = c
    }
    let S1 = 37 * (Math.sin((2 * Math.PI * a1) / 60)); let C1 = 37 * (Math.cos((2 * Math.PI * a1) / 60))
    S1 = S1.toFixed(2)
    C1 = C1.toFixed(2)
    let S2 = 37 * (Math.sin((2 * Math.PI * a2) / 60)); let C2 = 37 * (Math.cos((2 * Math.PI * a2) / 60))
    S2 = S2.toFixed(2)
    C2 = C2.toFixed(2)

    const ch = ((a2 - a1) <= 30)
      ? 'M50,50 l' + S1 + ' ' + (-C1) + ' a37,37 0 0,1 ' + (S2 - S1) + '  ' + (C1 - C2) + '  z'
      : 'M50,50 l' + S1 + ' ' + (-C1) + '   a37,37 0 1,1  ' + (S2 - S1) + '  ' + (C1 - C2) + '  z'
    const path = document.createElementNS('http://www.w3.org/2000/svg', 'path')
    path.setAttribute('d', ch)
    path.setAttribute('style', 'fill:#00f;fill-opacity:0.2')
    $('td > svg > g').append(path)
  }

  function secteurh (a1, a2) {
    const c = a1
    if (a2 < a1) {
      a1 = a2
      a2 = c
    }
    let S1 = 27 * (Math.sin((2 * Math.PI * a1) / 12)); let C1 = 27 * (Math.cos((2 * Math.PI * a1) / 12))
    S1 = S1.toFixed(2)
    C1 = C1.toFixed(2)
    let S2 = 27 * (Math.sin((2 * Math.PI * a2) / 12)); let C2 = 27 * (Math.cos((2 * Math.PI * a2) / 12))
    S2 = S2.toFixed(2)
    C2 = C2.toFixed(2)

    const ch = ((a2 - a1) <= 6)
      ? 'M50,50 l' + S1 + ' ' + (-C1) + ' a27,27 0 0,1 ' + (S2 - S1) + '  ' + (C1 - C2) + '  z'
      : 'M50,50 l' + S1 + ' ' + (-C1) + '   a27,27 0 1,1  ' + (S2 - S1) + '  ' + (C1 - C2) + '  z'
    const path = document.createElementNS('http://www.w3.org/2000/svg', 'path')
    path.setAttribute('d', ch)
    path.setAttribute('style', 'fill:#f00;fill-opacity:0.4')
    $('td > svg > g').append(path)
  }

  function enonceMain () {
    const terme = ds.precision
    stor.conteneur = j3pAddElt(me.zonesElts.MG, 'div', '', { style: me.styles.etendre('petit.enonce', { padding: '20px' }) })
    for (let i = 1; i <= 3; i++) stor['zoneCons' + i] = j3pAddElt(stor.conteneur, 'div')
    j3pAffiche(stor.zoneCons1, '', textes.consigne1, { a: terme })

    // creation des 2 horloges
    j3pStyle(stor.zoneCons2, { padding: '10 0' })
    const table = j3pAddElt(stor.zoneCons2, 'table', { cellPadding: 2 })
    const ligne1 = j3pAddElt(table, 'tr')
    j3pAddElt(ligne1, 'td', textes.horloge1, { align: 'center' })
    j3pAddElt(ligne1, 'td', textes.horloge2, { align: 'center' })
    const ligne2 = j3pAddElt(table, 'tr')
    const ho1 = j3pAddElt(ligne2, 'td')
    const ho2 = j3pAddElt(ligne2, 'td')
    const editeurs = j3pAddElt(stor.zoneCons3, 'div')
    // Horloge H1
    stor.horloge1 = Horloge.create(ho1, {
      echelle: 2.4,
      couleurs: {
        grande: couleurs[ds.couleurGrandeAiguille],
        petite: couleurs[ds.couleurPetiteAiguille],
        cadran: '#000000',
        chiffres: '#000000',
        fondInt: '#F6FAFE',
        fondExt: '#F6FAFE'
      },
      tailleChiffres: 11
    })
    // Horloge H2
    stor.horloge2 = Horloge.create(ho2, {
      echelle: 2.4,
      couleurs: {
        grande: couleurs[ds.couleurGrandeAiguille],
        petite: couleurs[ds.couleurPetiteAiguille],
        cadran: '#000000',
        chiffres: '#000000',
        fond_int: '#F6FAFE',
        fond_ext: '#F6FAFE'
      }
    })
    // mise à  l’heure de H1
    const [intMin1, intMax1] = j3pGetBornesIntervalle(ds.h1)
    const [intMin2, intMax2] = j3pGetBornesIntervalle(ds.m1)
    let h = j3pGetRandomInt(intMin1, intMax1) // h=9;
    let m = j3pGetRandomInt(intMin2, intMax2) // m=0;
    stor.stockage[0] = h
    stor.stockage[1] = m
    stor.horloge1.change(h, m)

    // mise à  l’heure de H2
    const [intMin3, intMax3] = j3pGetBornesIntervalle(ds.h2)
    const [intMin4, intMax4] = j3pGetBornesIntervalle(ds.m2)
    h = j3pGetRandomInt(intMin3, intMax3) // h=11;
    m = j3pGetRandomInt(intMin4, intMax4) // m=0;
    stor.stockage[2] = h
    stor.stockage[3] = m
    stor.horloge2.change(h, m)
    me.logIfDebug(stor.stockage[0], stor.stockage[1], '  ', stor.stockage[2], stor.stockage[3])

    const elt = j3pAffiche(editeurs, '', textes.consigne2,
      {
        inputmq1: { maxlength: 2, texte: '', dynamique: true, couleur: couleurs[ds.couleurpetiteaiguille] },
        inputmq2: { maxlength: 2, texte: '', dynamique: true, couleur: couleurs[ds.couleurpetiteaiguille] }
      })
    stor.zoneInput = [...elt.inputmqList]
    stor.zoneInput.forEach(zone => mqRestriction(zone, '\\d'))
    j3pFocus(stor.zoneInput[0])
    stor.zoneCorr = j3pAddElt(me.zonesElts.MD, 'div', '', { style: me.styles.etendre('petit.explications', { padding: '10px' }) })
    stor.zoneExpli = j3pAddElt(me.zonesElts.MD, 'div', '', { style: me.styles.etendre('petit.explications', { padding: '10px' }) })
    stor.zoneInput[0].typeReponse = ['nombre', 'exact']
    stor.zoneInput[1].typeReponse = ['nombre', 'approche', terme]
    if (stor.stockage[0] * 60 + stor.stockage[1] > stor.stockage[2] * 60 + stor.stockage[3]) {
      stor.stockage[2] = stor.stockage[2] + 12
    }
    const durees = (stor.stockage[2] * 60 + stor.stockage[3]) - (stor.stockage[0] * 60 + stor.stockage[1])
    stor.zoneInput[0].reponse = [Math.floor(durees / 60)]
    stor.zoneInput[1].reponse = [durees % 60]
    me.logIfDebug('heure:', Math.floor(durees / 60), ' minutes:', durees % 60, '  duree:', durees)
    const mesZonesSaisie = stor.zoneInput.map(elt => elt.id)
    stor.fctsValid = new ValidationZones({ parcours: me, zones: mesZonesSaisie })
    me.finEnonce()
  }
  switch (me.etat) {
    case 'correction': {
      // on teste si une réponse a été saisie
      // recup de la reponse élève
      const reponse = { aRepondu: false, bonneReponse: false }
      const fctsValid = stor.fctsValid
      reponse.aRepondu = fctsValid.valideReponses()

      let hr = j3pValeurde(stor.zoneInput[0])
      let mr = j3pValeurde(stor.zoneInput[1])

      // if (hr == "" && mr == "") {
      if (!reponse.aRepondu) {
        me.reponseManquante(stor.zoneCorr)
        // j3pFocus("reponsesinput1");
        me.afficheBoutonValider()
      } else { // une réponse a été saisie
        // calcul de la durée en min
        if (hr === '') hr = 0
        if (mr === '') mr = 0
        hr = parseInt(hr)
        mr = parseInt(mr)
        const dureer = hr * 60 + mr
        // recup de la solution

        const h1 = stor.stockage[0]
        const m1 = stor.stockage[1]
        let h2 = stor.stockage[2]
        const m2 = stor.stockage[3]
        if (h1 * 60 + m1 > h2 * 60 + m2) {
          h2 = h2 + 12
        }

        const durees = (h2 * 60 + m2) - (h1 * 60 + m1)
        reponse.bonneReponse = (Math.abs(dureer - durees) <= ds.precision)
        fctsValid.zones.bonneReponse[0] = reponse.bonneReponse
        fctsValid.zones.bonneReponse[1] = reponse.bonneReponse
        fctsValid.coloreUneZone(stor.zoneInput[0].id)
        fctsValid.coloreUneZone(stor.zoneInput[1].id)
        stor.bonneRep = reponse.bonneReponse
        const bonneRepSimple = ((hr === stor.zoneInput[0].reponse[0]) && (Math.abs(Number(mr) - stor.zoneInput[1].reponse[0]) <= ds.precision))
        if (reponse.bonneReponse) { // reponse juste
          me.score++
          stor.zoneCorr.style.color = me.styles.cbien
          stor.zoneCorr.innerHTML = cBien
          if (!bonneRepSimple) {
            stor.zoneCorr.innerHTML += '<br/>' + textes.comment1
          }
          stor.zoneExpli.style.color = me.styles.cbien
          afficheCorrection.call(this)
          me.typederreurs[0]++
          me.etat = 'navigation'
          me.sectionCourante()
        } else {
          stor.zoneCorr.style.color = me.styles.cfaux
          stor.zoneCorr.innerHTML = cFaux
          stor.zoneExpli.style.color = me.styles.toutpetit.correction.color
          // S’il y a deux chances,on appelle à  nouveau le bouton Valider
          if (me.essaiCourant < ds.nbchances) {
            stor.zoneCorr.innerHTML += '<br>' + essaieEncore
            me.typederreurs[1]++
            // indication éventuelle ici
            me.etat = 'correction'
            // j3pFocus("affiche1input1");
            me.afficheBoutonValider()// c’est la partie "correction" de la section qui sera exÃ©cutÃ©e
          } else { // debut de la correction
            afficheCorrection.call(this)
            me._stopTimer()
            me.typederreurs[2]++

            me.etat = 'navigation'
            me.sectionCourante()
          }// fin de la correction
        }
      }
    }
      me.finCorrection()
      break // case "correction":

    case 'enonce':
      if (me.debutDeLaSection) {
        me.construitStructurePage('presentation1')
        stor.stockage = [0, 0, 0, 0]
        me.typederreurs = [0, 0, 0, 0, 0, 0]
        me.afficheTitre(textes.titre_exo)
        if (ds.indication) me.indication(me.zonesElts.IG, ds.indication)
      } else {
        me.videLesZones()
      }
      enonceMain()
      break // case "enonce":

    case 'navigation':
      if (!me.sectionTerminee()) {
        me.etat = 'enonce'
      }
      me.finNavigation()
      break // case "navigation":
  }
}
