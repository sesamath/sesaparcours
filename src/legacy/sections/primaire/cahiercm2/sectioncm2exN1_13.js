import $ from 'jquery'
import { j3pAddElt, j3pDetruit, j3pDiv, j3pEmpty, j3pEntierBienEcrit, j3pFocus, j3pGetNewId, j3pGetRandomInt, j3pRestriction } from 'src/legacy/core/functions'
import ValidationZones from 'src/legacy/outils/calculatrice/ValidationZones'
import { j3pAffiche } from 'src/lib/mathquill/functions'
import TableauConversion from 'src/legacy/outils/tableauconversion/TableauConversion'
import { entierEnMot } from 'src/lib/outils/conversion/nombreEnMots'
import textesGeneriques from 'src/lib/core/textes'

const { cBien, cFaux, essaieEncore, regardeCorrection, tempsDepasse } = textesGeneriques

/*
    Dominique Chiarello
    2012-2013
    Modifié par Yves en Novembre 2018 : Position relative pour les dic et le tableau
    Paramètre afficheTableau non utilisé supprimé.
    Si on dépasait le temps limite on n’avait pas la solution.
    Repris par Rémi D pour avoir le style commun
 */

export const params = {
  outils: ['calculatrice', 'mathquill'],
  /**
   * Ce qui est paramétrable dans l’éditeur de graphe
   * @type {LegacySectionParamsParametres}
   */
  parametres: [
    ['nbrepetitions', 5, 'entier', 'Nombre de répétitions de la section'],
    ['indication', '', 'string', 'Indication pour chaque répétition'],
    ['limite', 0, 'entier', 'Temps disponible par répétition, en secondes'],
    ['nbchances', 3, 'entier', 'Nombre d’essais par répétition'],

    ['type', 2, 'entier', 'Prend les valeurs 1 ; 2 ou 3<br/>1 : pas de tableau<br/>2 : tableau statique<br/>3 : tableau dynamique'],
    ['lancer', '[0;999999]', 'string', 'Nombre de chiffres du nombre proposé.<br>Maximum 999 milliards'],
    ['sansEspaceAccepte', true, 'boolean', 'true signifie qu’on accepte les réponses où l’élève n’aurait pas mis les espaces.']
  ]
}

/**
 * Ajouter une description ici
 * @fileOverview
 * @author Tommy Barroy <t.barroy@laposte.net>
 * @since 2017-10
*/

/**
 * section cm2exN1_13
 * @this {Parcours}
 */
export default function main () {
  const me = this
  let ds = me.donneesSection
  const stor = me.storage

  function afficheCorr (bonneReponse) {
    // fonction appelée pour afficher la correction (que la réponse soit bonne ou fausse
    const zoneExpli = j3pAddElt(stor.conteneur, 'div', '', { style: me.styles.etendre('moyen.explications', { paddingTop: '15px', color: (bonneReponse) ? me.styles.cbien : me.styles.toutpetit.correction.color }) })
    j3pAffiche(zoneExpli, '', ds.textes.corr1, {
      s: j3pEntierBienEcrit(stor.terme)
    })
    tableauSol(stor.terme)
  }

  function tableauSol (nombre) {
    j3pEmpty(stor.tableauConv)
    if (ds.type !== 1) j3pDetruit(stor.info)
    j3pDiv(stor.conteneur, stor.tableauConv, '')
    const ligne = TableauConversion.creeLigne(nombre, 11, false, [0, 3])
    stor.tab = new TableauConversion(stor.tableauConv,
      {

        idDIV: stor.idTableau,
        classes: [3, 4],
        format: {
          epaisseurs: {},
          couleurs: {},
          largeurcol: 45
        },
        lignes: [ligne]
      }
    )
    $('#' + stor.idTableau).css('position', 'relative')
  }

  function getDonnees () {
    return {

      nbrepetitions: 5,
      nbetapes: 1,
      nbchances: 3,
      indication: '',
      seuil: 0.6,
      structure: 'presentation1',
      lancer: '[0;999999]', // max 999999999999999
      sansEspaceAccepte: true,
      typesection: 'primaire',
      type: 2,
      textes: {
        titre_exo: 'Ecriture de nombres- (N1 F1 ex3)',
        // le mieux est de mettre également le texte correspondant au titre de l’exo ici
        consigne1: 'Ecris le nombre <br> £a <br>en chiffres : @1@.',
        espace: 'Place correctement les espaces dans l’écriture du nombre.',
        info1: 'Tu peux t’aider en regardant le tableau et en y plaçant mentalement les nombres.',
        info2: 'Tu peux t’aider en plaçant correctement les nombres dans le tableau.',
        comment1: 'Attention à bien écrire le nombre&nbsp;!',
        comment2: 'Attention tout de même à mettre l’espace pour bien écrire le nombre&nbsp;!',
        comment3: 'Dernière tentative&nbsp;!',
        corr1: 'La solution est : £s.'
      }
    }
  }

  // variables internes
  const termeMin = 0
  const termeMax = 999999999999
  const termeMaxDefault = 999999

  switch (me.etat) {
    case 'enonce':

      if (me.debutDeLaSection) {
        me.donneesSection = getDonnees()
        ds = me.donneesSection
        me.surcharge()
        me.construitStructurePage({ structure: ds.structure, ratioGauche: 0.65 })
        me.typederreurs = [0, 0, 0, 0, 0, 0]
        me.score = 0
        // me.videLesZones();
        me.afficheTitre(ds.textes.titre_exo)

        if (ds.indication !== '') me.indication(me.zonesElts.IG, ds.indication)
      } else {
        me.videLesZones()
      }

      // on vérifie le paramétrage
      {
        const bornes = /^\[([0-9]+);([0-9]+)]$/.exec(ds.lancer)
        let borneInf, borneSup
        if (bornes) {
        // check borneInf
          borneInf = Number(bornes[1])
          borneSup = Number(bornes[2])
          // à priori pas possible si termeMin = 0, au cas où on le passerait à 1 ou plus
          let errMsg
          if (borneInf < termeMin) errMsg = 'Terme minimal invalide (' + borneInf + ' < ' + termeMin + ') => ' + termeMin
          else if (borneInf > termeMax) errMsg = 'Terme minimal invalide (' + borneInf + ' > ' + termeMax + ') => ' + termeMin
          else if (borneInf > borneSup) errMsg = 'Terme minimal invalide (supérieur à la borne sup : ' + borneInf + ' > ' + borneSup + ') => ' + termeMin
          if (errMsg) {
            console.error(errMsg)
            borneInf = termeMin
          }

          // check borneSup
          if (borneSup < termeMin) {
            console.error('Terme maximum invalide (' + borneSup + ' < ' + termeMin + ') => ' + termeMaxDefault)
            borneSup = termeMaxDefault
          } else if (borneSup > termeMax) {
            console.error('Terme maximum invalide (' + borneSup + ' > ' + termeMax + ') => ' + termeMax)
            borneSup = termeMax
          }
        } else {
          console.error('Donnée "lancer" de la section invalide (' + ds.lancer + ') => [0;999999]')
          borneInf = termeMin
          borneSup = termeMaxDefault
        }
        const terme = j3pGetRandomInt(borneInf, borneSup)

        stor.terme = terme

        stor.conteneur = j3pAddElt(me.zonesElts.MG, 'div', '', { style: me.styles.etendre('moyen.enonce', { padding: '10px' }) })
        const paveEnonce = j3pAddElt(stor.conteneur, 'div')
        const elt = j3pAffiche(paveEnonce, '', ds.textes.consigne1, {
          a: entierEnMot(terme),
          input1: { dynamique: true, maxlength: 12 }
        })
        if (!ds.sansEspaceAccepte) {
          const zoneInfo = j3pAddElt(stor.conteneur, 'div', ds.textes.espace)
          zoneInfo.style.fontStyle = 'italic'
          zoneInfo.style.fontSize = '0.9em'
        }
        stor.zoneInput = elt.inputList[0]
        j3pRestriction(stor.zoneInput, '0-9, ')
        stor.tableauConv = j3pAddElt(me.zonesElts.MG, 'div', '', { style: me.styles.etendre('moyen.explications', { padding: '15px' }) })
        if (ds.type !== 1) {
          stor.info = j3pAddElt(me.zonesElts.MG, 'div', '', {
            style: me.styles.etendre('petit.explications', {
              background: '#CCC',
              padding: '10px',
              margin: '20px'
            })
          })
        }
        stor.idTableau = j3pGetNewId('tableau')
        if (ds.type === 2) {
          stor.info.innerHTML = ds.textes.info1
          stor.tab = new TableauConversion(stor.tableauConv,
            {

              idDIV: stor.idTableau,
              classes: [3, 4],
              format: {
                epaisseurs: {},
                couleurs: {},
                largeurcol: 45
              },
              lignes: [[['.', '.', '.'], ['.', '.', '.'], ['.', '.', '.'], ['.', '.', '.'], []]]
            }
          )
          $('#' + stor.idTableau).css('position', 'relative')
        } else if (ds.type === 3) {
          stor.info.innerHTML = ds.textes.info2
          const leslignes = (!ds.et)
            ? [[['.', '.', '.'], ['.', '.', '.'], ['.', '.', '.'], ['.', '.', '.'], []]]
            : [[['?_', '?_', '?_'], ['?_', '?_', '?_'], ['?_', '?_', '?_'], ['?_', '?_', '?_'], []]]
          stor.tab = new TableauConversion(stor.tableauConv,
            {

              idDIV: stor.idTableau,
              classes: [3, 4],
              format: {
                epaisseurs: {},
                couleurs: {},
                largeurcol: 45
              },
              lignes: leslignes
            }
          )
          $('#tableau').css('position', 'relative')
        }

        j3pFocus(stor.zoneInput)
        stor.zoneInput.typeReponse = ['texte']
        stor.zoneInput.reponse = (ds.sansEspaceAccepte) ? [j3pEntierBienEcrit(stor.terme), String(stor.terme)] : [j3pEntierBienEcrit(stor.terme)]
        me.logIfDebug('reponse:', stor.zoneInput.reponse)
        stor.fctsValid = new ValidationZones({ parcours: me, zones: [stor.zoneInput.id] })
        stor.zoneCorr = j3pAddElt(me.zonesElts.MD, 'div', '', { style: me.styles.etendre('moyen.explications', { padding: '10px' }) })
      }
      me.finEnonce()

      break // case "enonce":

    case 'correction':

      // me.cacheBoutonValider();
      {
        const fctsValid = stor.fctsValid
        const reponse = fctsValid.validationGlobale()

        stor.zoneCorr.innerHTML = ''
        if (!reponse.aRepondu && (!me.isElapsed)) {
          me.reponseManquante(stor.zoneCorr)
        } else {
          if (reponse.bonneReponse) {
            me._stopTimer()
            me.score++
            stor.zoneCorr.style.color = me.styles.cbien
            stor.zoneCorr.innerHTML = cBien
            if (fctsValid.zones.reponseSaisie[0] !== fctsValid.zones.inputs[0].reponse[0]) {
              stor.zoneCorr.innerHTML += '<br/>' + ds.textes.comment2
            }
            me.typederreurs[0]++
            me.cacheBoutonValider()
            me.etat = 'navigation'
            me.sectionCourante()
          } else {
          // Pas de bonne réponse
          // à cause de la limite de temps :
            if (me.isElapsed) { // limite de temps
              stor.zoneCorr.innerHTML = tempsDepasse
              afficheCorr(false)
              me.typederreurs[10]++
              me.etat = 'navigation'
              me.sectionCourante()
            } else {
            // réponse fausse :
              stor.zoneCorr.style.color = me.styles.cfaux
              stor.zoneCorr.innerHTML = cFaux
              if (stor.terme === fctsValid.zones.reponseSaisie[0]) {
                stor.zoneCorr.innerHTML += '<br/>' + ds.textes.comment1
              }
              // S’il y a deux chances,on appelle à nouveau le bouton Valider
              if (me.essaiCourant < ds.nbchances) {
                if (me.essaiCourant === ds.nbchances - 1) {
                  stor.zoneCorr.innerHTML += '<br/>' + ds.textes.comment3
                } else {
                  stor.zoneCorr.innerHTML += '<br>' + essaieEncore
                }
                me.typederreurs[1]++
              } else {
                me._stopTimer()
                me.cacheBoutonValider()
                stor.zoneCorr.innerHTML += '<br>' + regardeCorrection
                // Là il ne peut plus se corriger. On lui affiche alors la solution
                afficheCorr(false)
                me.typederreurs[2]++
                me.etat = 'navigation'
                me.sectionCourante()
              }
            }
          }
        }
      }
      me.finCorrection()
      break // case "correction":

    case 'navigation':

      if (me.sectionTerminee()) {
        me.parcours.pe = me.score / ds.nbitems
        // if (me.parcours.pe < ds.seuil)  {  me.parcours.pe = ds.pe_2 }  else {me.parcours.pe = ds.pe_1};
        me.afficheBoutonSectionSuivante()
        me.focus('sectioncontinuer')
      } else {
        me.etat = 'enonce'
        me.afficheBoutonSuite()
        me.focus('boutoncontinuer')
      }

      break // case "navigation":
  }
}
