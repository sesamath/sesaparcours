import { j3pAddContent, j3pAddElt, j3pAjouteBouton, j3pBarre, j3pBasculeAffichage, j3pElement, j3pGetNewId, j3pNombreBienEcrit, j3pValeurde } from 'src/legacy/core/functions'
import { j3pAffiche, j3pDesactive } from 'src/lib/mathquill/functions'
import TableauConversion from 'src/legacy/outils/tableauconversion/TableauConversion'
import textesGeneriques from 'src/lib/core/textes'

const { cBien, cFaux, essaieEncore, regardeCorrection, tempsDepasse } = textesGeneriques

/*
        Bruno Meunier
        2013
 */

export const params = {
  outils: ['calculatrice'],
  /**
   * Ce qui est paramétrable dans l’éditeur de graphe
   * @type {LegacySectionParamsParametres}
   */
  parametres: [
    ['nbrepetitions', 5, 'entier', 'aucun'],
    ['indication', '', 'string', 'aucun'],
    ['limite', 0, 'entier', 'Temps disponible par répétition, en secondes'],
    ['nbchances', 2, 'entier', 'Nombre d’erreurs possibles (pour tenter de les corriger)'],

    ['nbreChiffre', [3, 4, 5, 6, 7], 'array', 'Le nombre de chiffres du nombre sur lequel l’élève travaille'],
    ['rangDemande', [1, 3, 5, 4, 2], 'array', 'Le rang du chiffre sur lequel porte la question (rang 1 = chiffre des unités rang 6 = chiffre des centaines de mille'],
    ['aideTableau', 'auto', 'liste', 'Un tableau de conversion remplissable par l’élève sera affiché quand on clique sur "Aide".<br>Il pourra être complet, vide ou éditable.<br/>"auto" signifie qu’il est directement affiché.', ['auto', 'manuel', 'manuel-vide', 'manuel-editable']],
    ['propositionsOrdonnees', true, 'boolean', "Les propositions de réponses sont dans l’ordre du tableau de numération 'unités dizaines centaines etc"]
  ]

}

/**
 * Ajouter une description ici
 * @fileOverview
 * @author Tommy Barroy <t.barroy@laposte.net>
 * @since 2017-10
*/
export default function main () {
  const me = this
  let ds = me.donneesSection
  const stor = me.storage

  function basculerAffichageTableau () {
    j3pBasculeAffichage(stor.idTableau)
  }

  function getDonnees () {
    return {

      typesection: 'primaire',
      nbrepetitions: 5,
      nbetapes: 1,
      indication: '',
      nbchances: 2,
      structure: 'presentation1',
      // nbreChiffre :
      // le nombre de chiffres du nombre sur lequel l’élève travaille
      nbreChiffre: [3, 4, 5, 6, 7],
      // rangDemande :
      // le rang du chiffre sur lequel porte la question (rang 1: chiffre des unités rang 6: chiffre des centaines de mille
      rangDemande: [1, 3, 5, 4, 2],
      // aideTableau :
      // si "auto" le tableau de numeration est toujours affiché
      // si "manuel" le tableau de numération avec le nombre sera affiché quand on clique sur "Aide"
      // si "manuel-vide" un tableau de numération vide sera affiché quand on clique sur "Aide"
      // // aideTableau: "manuel-editable" un tableau de numération remplisssable par l’élève sera affiché quand on clique sur "Aide"
      aideTableau: 'auto',
      // propositionsOrdonnees :
      // si true les propositions de réponses sont dans l’ordre du tableau de numération 'unités dizaines centaines etc.
      // si false les propositions de réponses ne sont pas dans l’ordre : unités, unités de mille, unités de millions etc.
      propositionsOrdonnees: true,

      textes: {
        titre_exo: 'Chiffre et nombre entier - (N1 F2 ex3)',
        consigne1: 'Dans <b>£n</b> le chiffre <span class=\'grasrouge\'>£c</span> est celui des : #1#',
        phrase1: 'Observe le tableau et choisis la bonne proposition.',
        phrase11: 'Choisis la bonne proposition.',
        phrase2: 'Il y a un erreur, corrige-la',
        phrase3: 'Il y a des erreurs, corrige-les',
        phrase4: 'Il faut donner une réponse.',
        phrase5: 'unités,dizaines,centaines,unités de mille,dizaines de mille,centaines de mille,unités de millions,dizaines de millions,centaines de millions,unités de milliards,dizaines de milliards,centaines de milliards',
        phrase6: 'unités,unités de mille,unités de millions,unités de milliards,dizaines,dizaines de mille,dizaines de millions,dizaines de milliards,centaines,centaines de mille,centaines de millions,centaines de milliards',
        phrase7: '',
        comment1: 'Il faut donner une réponse&nbsp;!',
        corr1: '£c est le chiffre des £r.'
      }
    }
  }
  function enonceInitFirst () {
    me.donneesSection = getDonnees()
    ds = me.donneesSection
    me.surcharge()
    me.construitStructurePage({ structure: ds.structure, ratioGauche: 0.7 })
    stor.stockage = [0, 0, 0, 0]
    me.typederreurs = [0, 0, 0, 0, 0, 0]
    me.score = 0
    me.videLesZones()
    me.afficheTitre(ds.textes.titre_exo)
    if (ds.indication) me.indication(me.zonesElts.IG, ds.indication)
  }
  function enonceMain () {
    // Creation des donnees numeriques
    // nbreChiffre:[3,4,6,6,7],rangDemande:[1,3,5,4,6]
    q = me.questionCourante - 1
    let nbreChiffre = ds.nbreChiffre[q]
    let inf, sup
    if (typeof (nbreChiffre) === 'string') {
      inf = parseInt(nbreChiffre.split('-')[0])
      sup = parseInt(nbreChiffre.split('-')[1])
      nbreChiffre = inf + Math.floor(Math.random() * (sup - inf + 1))
      ds.nbreChiffre[q] = nbreChiffre
    }

    nChiffreMax = Math.max.apply(null, ds.nbreChiffre)
    // var nbreEspace = Math.floor(nbreChiffre/3);
    const min = Math.pow(10, (nbreChiffre - 1))
    const max = Math.pow(10, (nbreChiffre)) - 1
    let unique = false
    while (!unique) {
      stor.stockage.nombre = min + Math.floor(Math.random() * (max - min + 1))
      const aChiffre = String(stor.stockage.nombre).split('').sort()
      for (let i = 1; i < aChiffre.length; i++) {
        if (aChiffre[i] === aChiffre[i - 1]) {
          unique = false
          break
        } else {
          unique = true
        }
      }
    }
    let rangDemande = ds.rangDemande[q]

    if (typeof (rangDemande) === 'string') {
      inf = parseInt(rangDemande.split('-')[0])
      sup = parseInt(rangDemande.split('-')[1])
      if (inf > nbreChiffre) {
        inf = 1
      }
      if (sup > nbreChiffre) {
        sup = nbreChiffre
      }
      rangDemande = inf + Math.floor(Math.random() * (sup - inf + 1))
    }
    stor.stockage.rangDemande = rangDemande
    let sValeur = ds.textes.phrase5
    let aValeur = sValeur.split(',').slice(0, nChiffreMax)
    const chiffreDemande = String(stor.stockage.nombre).charAt(String(stor.stockage.nombre).length - rangDemande)
    nClasse = Math.ceil(nChiffreMax / 3)
    // si rangDemande = 1, et si stor.stockage.nombre=963 alors chiffreDemande = 3
    stor.stockage.soluce = aValeur[rangDemande - 1]
    if (!ds.propositionsOrdonnees) {
      sValeur = ds.textes.phrase6
      aValeur = sValeur.split(',')
    }
    aValeur.unshift(' ')
    stor.repEleve = ''

    // conteneur
    const st1 = me.styles.etendre('petit.enonce', { padding: '20px' })
    stor.conteneur = j3pAddElt(me.zonesElts.MG, 'div', '', { style: st1 })
    for (let i = 1; i <= 4; i++)stor['zoneCons' + i] = j3pAddElt(stor.conteneur, 'div')
    // la consigne
    const sConsigne = (ds.aideTableau === 'auto') ? ds.textes.phrase1 : ds.textes.phrase11
    j3pAddContent(stor.zoneCons1, sConsigne)

    // l’enonce
    const sNombre = j3pNombreBienEcrit(stor.stockage.nombre)
    const indiceSoluce = aValeur.indexOf(stor.stockage.soluce)
    const sEnonce = ds.textes.consigne1
    const elt = j3pAffiche(stor.zoneCons2, '', sEnonce,
      {
        c: chiffreDemande,
        n: sNombre,
        liste1: {
          texte: aValeur,
          correction: indiceSoluce
        }
      }
    )
    stor.zoneSelect = elt.selectList[0]
    stor.chiffreDemande = chiffreDemande
    stor.zoneExpli = j3pAddElt(stor.zoneCons3, 'div', '', { style: me.styles.etendre('petit.correction', { padding: '20px 0' }) })

    // le tableau
    stor.idTableau = j3pGetNewId('conteneurTab')
    stor.tableau = j3pAddElt(stor.zoneCons4, 'div', '', { id: stor.idTableau, style: st1 })
    let tabLignes
    // le nombre de classes qu’on va afficher dépend du plus grand nombre de chiffres de la série

    startClasse = 4 - nClasse
    if (ds.aideTableau === 'auto') {
      tabLignes = TableauConversion.creeLigne(stor.stockage.nombre, 11, false, [startClasse, 3])
    } else if (ds.aideTableau === 'manuel') {
      tabLignes = TableauConversion.creeLigne(stor.stockage.nombre, 11, false, [startClasse, 3])
      j3pBasculeAffichage(stor.idTableau)
    } else if (ds.aideTableau === 'manuel-vide') {
      tabLignes = [[], [], [], [], [], []]
      for (let i = 3; i >= (4 - nClasse); i--) {
        tabLignes[i] = ['.', '.', '.']
      }
      j3pBasculeAffichage(stor.idTableau)
    } else if (ds.aideTableau === 'manuel-editable') {
      tabLignes = [[], [], [], [], [], []]
      for (let i = 3; i >= (4 - nClasse); i--) {
        tabLignes[i] = ['?_', '?_', '?_']
      }
      j3pBasculeAffichage(stor.idTableau)
    }

    stor.tab = new TableauConversion(stor.tableau,
      {

        idDIV: 'Tab' + stor.idTableau,
        format: { epaisseurs: {}, couleurs: {} },
        lignes: [tabLignes]
      }
    )
    stor.zoneD = j3pAddElt(me.zonesElts.MD, 'div', '', { style: me.styles.etendre('petit.explications', { padding: '15px' }) })
    // le bouton Aide
    stor.btnAide = j3pGetNewId('boutonAide')
    if (ds.aideTableau !== 'auto') {
      const zoneBtn = j3pAddElt(stor.zoneD, 'div')
      j3pAjouteBouton(zoneBtn, stor.btnAide, 'MepBoutons', 'Aide', basculerAffichageTableau)
    }

    stor.zoneCorr = j3pAddElt(stor.zoneD, 'div')
    stor.zoneCorr.style.paddingTop = '15px'
    me.finEnonce()
  }
  let nClasse
  let nChiffreMax
  let startClasse
  let q
  switch (me.etat) {
    case 'enonce':

      if (me.debutDeLaSection) {
        enonceInitFirst()
      } else {
        me.videLesZones()
      }

      enonceMain()
      break // case "enonce":

    case 'correction':

      me.cacheBoutonValider()
      {
      // on teste si une réponse a été saisie
        const repEleve = j3pValeurde(stor.zoneSelect)
        let newLine
        if (repEleve === '') {
          stor.zoneCorr.style.color = me.styles.cfaux
          stor.zoneCorr.innerHTML = ds.textes.comment1
          me.afficheBoutonValider()
        } else { // une réponse a été saisie
          if (repEleve === stor.stockage.soluce) {
            me.score++
            // si tableau vide on recrée un tableau plein;
            if (ds.aideTableau === 'manuel-vide' || ds.aideTableau === 'manuel-editable') {
              nChiffreMax = Math.max.apply(null, ds.nbreChiffre)
              nClasse = Math.ceil(nChiffreMax / 3)
              startClasse = 4 - nClasse
              newLine = TableauConversion.creeLigne(stor.stockage.nombre, 11, false, [startClasse, 3])
              j3pElement(stor.idTableau).innerHTML = ''
              stor.tab = new TableauConversion(stor.idTableau,
                {

                  idDIV: 'Tab' + stor.idTableau,
                  format: { epaisseurs: {}, couleurs: {} },
                  lignes: [newLine]
                }
              )
            }

            // on colorie en vert la réponse
            q = me.questionCourante - 1
            // on affiche le tableau;
            document.getElementById(stor.idTableau).style.display = 'block'
            stor.zoneSelect.style.color = me.styles.cbien
            j3pDesactive(stor.zoneSelect)
            // on cache le bouton aide s’il est affiché'
            if (ds.aideTableau === 'manuel' || ds.aideTableau === 'manuel-vide' || ds.aideTableau === 'manuel-editable') j3pBasculeAffichage(stor.btnAide)

            stor.zoneCorr.style.color = me.styles.cbien
            stor.zoneCorr.innerHTML = cBien
            me.typederreurs[0]++
            me.etat = 'navigation'
            me.sectionCourante()
          } else {
          // Pas de bonne réponse
            stor.zoneCorr.style.color = me.styles.cfaux
            stor.zoneSelect.style.color = me.styles.cfaux
            // à cause de la limite de temps :
            if (me.isElapsed) { // limite de temps
              stor.zoneCorr.innerHTML = tempsDepasse
              me.typederreurs[10]++
              j3pDesactive(stor.zoneSelect)
              j3pAffiche(stor.zoneExpli, '', ds.textes.corr1, { c: stor.chiffreDemande, r: stor.stockage.soluce })

              me.etat = 'navigation'
              me.sectionCourante()
            } else {
            // réponse fausse :
              stor.zoneCorr.innerHTML = cFaux
              // S’il y a deux chances,on appelle à nouveau le bouton Valider
              if (me.essaiCourant < ds.nbchances) {
                stor.zoneCorr.innerHTML += '<br>' + essaieEncore
                me.typederreurs[1]++
                // indication éventuelle ici
                me.etat = 'correction'
                me.afficheBoutonValider()// c’est la partie "correction" de la section qui sera exécutée
              } else {
              // Erreur au second essai
                stor.zoneCorr.innerHTML += '<br>' + regardeCorrection
                me.typederreurs[2]++
                j3pAffiche(stor.zoneExpli, '', ds.textes.corr1, { c: stor.chiffreDemande, r: stor.stockage.soluce })
                j3pDesactive(stor.zoneSelect)
                j3pBarre(stor.zoneSelect)
                // si tableau vide on recrée un tableau plein;
                if (ds.aideTableau === 'manuel-vide' || ds.aideTableau === 'manuel-editable') {
                  nChiffreMax = Math.max.apply(null, ds.nbreChiffre)
                  nClasse = Math.ceil(nChiffreMax / 3)
                  startClasse = 4 - nClasse
                  newLine = TableauConversion.creeLigne(stor.stockage.nombre, 11, false, [startClasse, 3])
                  j3pElement(stor.idTableau).innerHTML = ''
                  stor.tab = new TableauConversion(stor.idTableau,
                    {

                      idDIV: 'Tab' + stor.idTableau,
                      format: { epaisseurs: {}, couleurs: {} },
                      lignes: [newLine]
                    }
                  )
                }

                // on met en rouge le chiffre et la colonne pertinente
                q = me.questionCourante - 1
                // rang = stor.stockage.rangDemande
                // me.tab.colorierColonne(rang, 0, '#ff0000')
                // on affiche le tableau
                document.getElementById(stor.idTableau).style.display = 'block'
                // on cache le bouton Aide
                if (ds.aideTableau === 'manuel' || ds.aideTableau === 'manuel-vide' || ds.aideTableau === 'manuel-editable') j3pBasculeAffichage(stor.btnAide)

                // j3pDesactive("affiche1liste1");
                me.etat = 'navigation'
                me.sectionCourante()
              }
            }
          }
        }
      }
      me.finCorrection()
      break // case "correction":

    case 'navigation':
      if (!me.sectionTerminee()) {
        me.etat = 'enonce'
      }
      me.finNavigation()
      break // case "navigation":
  }
}
