import { j3pAddElt, j3pBarre, j3pChaine, j3pFocus, j3pFreezeElt, j3pGetRandomFixed, j3pNombre, j3pParseArray, j3pRemplacePoint, j3pShowError, j3pVirgule } from 'src/legacy/core/functions'
import Droitegraduee from 'src/legacy/outils/droitegraduee/droitegraduee'
import { j3pAffiche, j3pDesactive } from 'src/lib/mathquill/functions'
import textesGeneriques from 'src/lib/core/textes'

const { cBien, cFaux, essaieEncore, tempsDepasse } = textesGeneriques

export const params = {
  outils: [],
  /**
   * Ce qui est paramétrable dans l’éditeur de graphe
   * @type {LegacySectionParamsParametres}
   */
  parametres: [
    ['nbrepetitions', 3, 'entier', 'aucun'],
    ['indication', '', 'string', 'aucun'],
    ['limite', 0, 'entier', 'Temps disponible par répétition, en secondes'],
    ['nbchances', 2, 'entier', 'Nombre d’erreurs possibles (pour tenter de les corriger)'],
    ['lepoint', '[1;5],3', 'string', 'génère un réel compris entre 1 et 5, à 3 décimales. OU 3.34  OU  4/3'],
    ['tolerance', 0.1, 'number', 'L’erreur tolérée'],
    ['negatifs', false, 'boolean', 'Afficher les négatifs'],
    ['etiquettes', true, 'boolean', 'Affichage ou non des étiquettes des graduations'],
    ['listeetiquettes', '[]', 'string', 'Les étiquettes affichées. N’a de sens que si etiquettes = false'],
    ['zoomable', false, 'boolean', 'barre de zoom'],
    ['pasdunegraduation', 1, 'entier', 'Pas de chaque graduation'],
    ['pixelspargraduation', 60, 'entier', 'Nombre de pixels d’une graduation'],
    ['nbmaxzoomdecimal', 0, 'entier', 'Zoom max'],
    ['positionO', -350, 'entier', 'En pixels. Position de 0 par rapport au centre de l’axe'],
    ['graduationsecondaires', true, 'boolean', 'Affichage ou non des graduations secondaires']
  ]

}

/**
 * section
 * @this {Parcours}
 */
export default function main () {
  const me = this
  const stor = me.storage
  let ds = me.donneesSection
  function getDonnees () {
    return {
      typesection: 'primaire',

      nbrepetitions: 3,
      nbetapes: 1,
      nbchances: 2,
      structure: 'presentation3',

      zoomable: false,
      deplacable: true,
      negatifs: false,
      positionO: -350,
      pixelspargraduation: 100,
      pixelsminpourgraduations: 40,
      pasdunegraduation: 1,
      graduationsecondaires: true,
      etiquettes: true,
      listeetiquettes: '[]',
      lepoint: '[1;5],3',
      nbmaxzoomdecimal: 0,

      hasZoomDecimal: true,
      tolerance: 0.1,
      textes: {
        titreExo: 'Lire l’abscisse d’un point ',
        cons1: 'Donne la valeur de l’abscisse du point M qui te semble la plus précise possible',
        cFaux1: 'C’est faux<br>Une réponse correcte est £a.',
        cFaux2: 'C’est faux<br>La bonne réponse est £a.'
      }
    }
  }

  // pour éviter de coller des trucs spécifiques à cette section directement sur this (l’objet global j3p)
  function enonceMain () {
  // on regarde si c’est de la forme [a;b],c
    const chunks = /^\[([0-9.-]+);([0-9.-]+)\],([0-9])/.exec(ds.lepoint)
    if (chunks) {
      stor.br = j3pGetRandomFixed(chunks[1], chunks[2], chunks[3])
    } else {
      stor.br = Number(ds.lepoint)
      if (isNaN(stor.br)) return j3pShowError(Error('Paramétrage invalide'))
    }
    if (ds.listeetiquettes === 'autourdupoint') {
      const bi = Math.floor(stor.br)
      const bs = bi + 1
      ds.listeetiquettes = '[' + bi + ',' + bs + ']'
    }

    const conteneur = j3pAddElt(me.zonesElts.MG, 'div', '', { style: me.styles.etendre('petit.enonce', { padding: '15px' }) })

    // eslint-disable-next-line no-new
    new Droitegraduee(conteneur, {
      positionO: ds.positionO,
      pixelsParGraduation: ds.pixelspargraduation,
      pas: ds.pasdunegraduation,
      pixelsminpourgraduations: ds.pixelsminpourgraduations,
      pixelsminpouretiquette: 20,
      tailleetiquetteprincipale: 14,
      tailleetiquette: 10,
      taillesegmentprincipal: 16,
      taillesegment: 8,
      taillesegmentpetit: 6,
      couleurgraduationprincipale: 'black',
      couleurgraduation: 'blue',
      couleurgraduationpetite: 'green',
      epaisseurgraduationprincipale: 2,
      epaisseurgraduation: 1,
      couleuraxe: 'black',
      epaisseuraxe: 1,
      graduationsecondaires: ds.graduationsecondaires,
      etiquettes: ds.etiquettes,
      listeetiquettes: j3pParseArray(ds.listeetiquettes),
      zoomable: ds.zoomable,
      deplacable: ds.deplacable,
      negatifs: ds.negatifs,
      listePoints: [{
        nom: 'M',
        abscisse: stor.br,
        nbdecimales: 3,
        etiquette: false,
        taillepoint: 10,
        taillepolice: 18,
        police: 'times new roman',
        couleur: '#006600',
        epaisseur: 2,
        fixe: true
      }],
      hasZoomDecimal: ds.boutonszoomdecimal,
      boutonplacerpoint: false,
      maxZoomDecimal: ds.nbmaxzoomdecimal
    })
    // énoncé
    j3pAddElt(conteneur, 'p', ds.textes.cons1)
    const zoneCons2 = j3pAddElt(conteneur, 'div')
    const elt = j3pAffiche(zoneCons2, '', '@1@', {
      input1: { texte: '', maxchars: 6, restrict: /[0-9,.]/, dynamique: true }
    })
    stor.zoneInput = elt.inputList[0]
    stor.zoneInput.addEventListener('input', j3pRemplacePoint)
    stor.zoneCorr = j3pAddElt(conteneur, 'div', '', { style: me.styles.etendre('petit.correction', { paddingTop: '30px' }) })

    j3pFocus(stor.zoneInput)
    me.finEnonce()
  }
  switch (me.etat) {
    case 'enonce':
      if (me.debutDeLaSection) {
        me.donneesSection = getDonnees()
        ds = me.donneesSection
        me.surcharge()
        me.construitStructurePage({ structure: ds.structure, ratioGauche: 0.6 })

        if (ds.nbmaxzoomdecimal === 0) ds.boutonszoomdecimal = false
        me.typederreurs = [0, 0, 0, 0, 0, 0]
        me.score = 0

        me.afficheTitre(ds.textes.titreExo)
      } else {
        me.videLesZones()
      }
      enonceMain()
      break // case enonce

    case 'correction':

      {
        const reponseeleve = stor.zoneInput.value.trim()
        const reponseEleveNb = j3pNombre(reponseeleve) // remplace la virgule et virera les 0 non significatifs

        if (!reponseeleve && !me.isElapsed) {
        // pas de réponse sans limite de temps
          me.reponseManquante(stor.zoneCorr)
          j3pFocus(stor.zoneInput)
        } else {
          const cbon = Math.abs(reponseEleveNb - Number(stor.br)) < ds.tolerance
          if (cbon) {
            me.score++
            stor.zoneCorr.style.color = me.styles.cbien
            stor.zoneCorr.innerHTML = cBien
            stor.zoneInput.style.color = me.styles.cbien
            j3pDesactive(stor.zoneInput)
            j3pFreezeElt(stor.zoneInput)
            me.typederreurs[0]++
            me.etat = 'navigation'
            me.sectionCourante()
          } else {
          // réponse incorrecte
            stor.zoneCorr.style.color = me.styles.cfaux
            stor.zoneInput.style.color = me.styles.cfaux
            if (!me.isElapsed) {
            // sans limite de temps
              stor.zoneCorr.innerHTML = cFaux
              if (me.essaiCourant < ds.nbchances) {
              // on peut relancer un essai supplémentaire
                stor.zoneCorr.innerHTML += '<br>' + essaieEncore
                me.typederreurs[1]++
                // indication éventuelle ici
                me.etat = 'correction'
                me.afficheBoutonValider()
              // c’est la partie "correction" de la section qui sera exécutée
              } else {
              // pas d’autre essai
                if (ds.tolerance) {
                  const n = Math.round(Math.log(1 / ds.tolerance) / Math.log(10))
                  stor.zoneCorr.innerHTML = j3pChaine(ds.textes.cFaux1, { a: j3pVirgule(stor.br, n) })
                } else {
                  stor.zoneCorr.innerHTML = j3pChaine(ds.textes.cFaux1, { a: j3pVirgule(stor.br) })
                }
                j3pDesactive(stor.zoneInput)
                j3pBarre(stor.zoneInput)
                j3pFreezeElt(stor.zoneInput)
                me.typederreurs[2]++
                me.etat = 'navigation'
                me.sectionCourante()
              }
            } else {
            // limite de temps
              stor.zoneCorr.innerHTML = tempsDepasse
              me.typederreurs[10]++
              me.etat = 'navigation'
              me.sectionCourante()
            }
          }
        }
      }
      me.finCorrection()
      break // case "correction":

    case 'navigation':
      if (!me.sectionTerminee()) me.etat = 'enonce'
      // rien à faire quand la section est terminée.
      me.finNavigation(true)
      break
  } // switch etat
}
