import { j3pAddContent, j3pAddElt, j3pArrondi, j3pBarre, j3pChaine, j3pEmpty, j3pFocus, j3pFreezeElt, j3pGetRandomInt, j3pParseArray, j3pRemplacePoint, j3pRestriction, j3pShowError, j3pVirgule } from 'src/legacy/core/functions'
import Droitegraduee from 'src/legacy/outils/droitegraduee/droitegraduee'
import { j3pAffiche, j3pDesactive } from 'src/lib/mathquill/functions'
import textesGeneriques from 'src/lib/core/textes'

const { cBien, tempsDepasse } = textesGeneriques

export const params = {
  outils: [],
  /**
   * Ce qui est paramétrable dans l’éditeur de graphe
   * @type {LegacySectionParamsParametres}
   */
  parametres: [
    ['nbrepetitions', 5, 'entier', 'aucun'],
    ['indication', '', 'string', 'aucun'],
    ['limite', 0, 'entier', 'Temps disponible par répétition, en secondes'],
    // ['nbchances',2,'entier',""],
    ['lepoint', '[0;50],3', 'string', 'génère un réel compris entre 1 et 50, à 3 décimales'],
    ['etiquettes', true, 'boolean', 'Affichage ou non des étiquettes des graduations'],
    ['listeetiquettes', '[]', 'string', 'Les étiquettes affichées. N’a de sens que si etiquettes = false'],
    ['zoomable', false, 'boolean', 'barre de zoom (en plus des boutons du zoom décimal)'],
    ['negatifs', false, 'boolean', 'Affichage ou non des nombres négatifs'],
    ['pasdunegraduation', 10, 'entier', 'Le pas d’une graduation'],
    ['pixelspargraduation', 100, 'entier', 'Nombre de pixels par graduation'],
    ['positionO', -300, 'entier', 'En pixels. Position de 0 par rapport au centre de l’axe'],
    ['graduationsecondaires', true, 'boolean', 'Affichage ou non des graduations secondaires']
  ]

}

/**
 * Ajouter une description ici
 * @fileOverview
 * @author Tommy Barroy <t.barroy@laposte.net>
 * @since 2017-10
*/
export default function main () {
  const me = this
  let ds = me.donneesSection
  const stor = me.storage
  function getDonnees () {
    return {
      typesection: 'primaire',

      nbrepetitions: 5,
      nbetapes: 1,
      nbitems: 5, // nbetapes * nbrepetitions
      structure: 'presentation3',

      zoomable: false,
      deplacable: true,
      negatifs: false,
      positionO: -300,
      pixelspargraduation: 100,
      pasdunegraduation: 10,
      graduationsecondaires: true,
      etiquettes: true,
      listeetiquettes: '[]',
      lepoint: '[0;50],3',
      textes: {
        titre_exo: 'Lire l’abscisse d’un point',
        consigne1: 'Donner une valeur approchée de l’abscisse du point M.<br> Sois le plus précis possible.',
        consigne2: 'On ne peut zoomer qu’une seule fois.',
        consigne3: 'On ne peut zoomer que £n fois.',
        comment1: 'Il faut donner une réponse numérique !',
        corr1: 'C’est faux&nbsp;!<br>La bonne réponse était £r.',
        corr2: 'C’est faux&nbsp;!<br>Une bonne réponse était £r ou £s ou tout nombre compris entre ces deux nombres.'
      }
    }
  }
  function enonceMain () {
  // tirage d’un nouveau point, on détermine l’intervalle
    const chunks = /^\[([0-9.-]+);([0-9.-]+)\],([0-9])/.exec(ds.lepoint)
    if (chunks) {
      const nbDecimales = Number(chunks[3])
      const ordre = Math.pow(10, nbDecimales)
      const borneinf = Number(chunks[1]) * ordre
      const bornesup = Number(chunks[2]) * ordre
      // bonneReponse en number
      stor.br = j3pGetRandomInt(borneinf, bornesup) / ordre
      // faudrait expliquer ces calculs…
      const offset = Math.log(ds.pasdunegraduation) / Math.log(10)
      ds.maxZoomDecimal = j3pArrondi(nbDecimales - 2 + offset, 5)
      const puissance2 = j3pArrondi(Math.pow(10, ds.maxZoomDecimal + 1 - offset), 5)
      stor.brInf = Math.floor(stor.br * puissance2) / puissance2
      stor.brSup = Math.ceil(stor.br * puissance2) / puissance2
    } else {
      return j3pShowError(Error('Paramétrage invalide'))
    }

    let question = ds.textes.consigne1
    if (!ds.zoomlibre && ds.maxZoomDecimal > 0) {
      if (ds.maxZoomDecimal === 1) {
        question += '<br>' + ds.textes.consigne2
      } else {
        question += '<br>' + j3pChaine(ds.textes.consigne3, { n: ds.maxZoomDecimal })
      }
    }
    const conteneur = j3pAddElt(me.zonesElts.MG, 'div', '', { style: me.styles.etendre('petit.enonce', { padding: '20px' }) })
    j3pAddElt(conteneur, 'div', question)
    const divZone = j3pAddElt(conteneur, 'div')
    const elt = j3pAffiche(divZone, '', '@1@', {
      input1: { texte: '', maxchars: 6, dynamique: true }
    })
    stor.zoneInput = elt.inputList[0]
    j3pRestriction(stor.zoneInput, '0-9,.')
    stor.zoneInput.addEventListener('input', j3pRemplacePoint)
    const div = j3pAddElt(conteneur, 'div', '', { style: { margin: '20px 10px' } })
    // eslint-disable-next-line no-new
    new Droitegraduee(div, {
      height: 140,
      positionO: -350,
      pixelsParGraduation: ds.pixelspargraduation,
      pas: ds.pasdunegraduation,
      pixelsminpourgraduations: 40,
      pixelsminpouretiquette: 20,
      tailleetiquetteprincipale: 14,
      tailleetiquette: 10,
      taillesegmentprincipal: 14,
      taillesegment: 8,
      taillesegmentpetit: 4,
      couleurgraduationprincipale: 'black',
      couleurgraduation: 'blue',
      couleurgraduationpetite: 'green',
      epaisseurgraduationprincipale: 2,
      epaisseurgraduation: 1,
      couleuraxe: 'black',
      epaisseuraxe: 1,
      zoomable: ds.zoomable,
      deplacable: ds.deplacable,
      negatifs: ds.negatifs,
      graduationsecondaires: ds.graduationsecondaires,
      etiquettes: ds.etiquettes,
      // faut transformer la string en array…
      listeetiquettes: j3pParseArray(ds.listeetiquettes),
      listePoints: [{
        nom: 'M',
        abscisse: stor.br,
        nbdecimales: 3,
        etiquette: false,
        taillepoint: 16,
        taillepolice: 18,
        police: 'times new roman',
        couleur: '#006600',
        epaisseur: 2,
        fixe: true
      }],
      hasZoomDecimal: true,
      zoomCenter: stor.br,
      boutonplacerpoint: false,
      maxZoomDecimal: ds.maxZoomDecimal
    })
    // pour le retrouver à la correction
    stor.zoneCorr = j3pAddElt(conteneur, 'div', '', { style: me.styles.etendre('petit.correction', { paddingTop: '30px' }) })
    j3pFocus(stor.zoneInput)
    me.finEnonce()
  }
  switch (me.etat) {
    case 'enonce':
      if (me.debutDeLaSection) {
        me.donneesSection = getDonnees()
        ds = me.donneesSection
        me.surcharge()
        me.construitStructurePage({ structure: ds.structure, ratioGauche: 0.6 })
        me.typederreurs = [0, 0, 0, 0, 0, 0]
        me.afficheTitre(ds.textes.titre_exo)
      } else {
        me.videLesZones()
      }
      enonceMain()
      break // case enonce

    case 'correction':

      {
        const reponse = Number(stor.zoneInput.value.replace(',', '.'))
        j3pEmpty(stor.zoneCorr)
        if (stor.zoneInput.value === '' && !me.isElapsed) {
          me.reponseManquante(stor.zoneCorr)
          j3pFocus(stor.zoneInput)
        } else if (isNaN(reponse) && !me.isElapsed) {
          stor.zoneCorr.style.color = me.styles.cfaux
          j3pAddContent(stor.zoneCorr, ds.textes.comment1)
        } else {
          const cbon = (reponse >= stor.brInf && reponse <= stor.brSup)

          if (cbon) {
            me.score++
            j3pAddContent(stor.zoneCorr, cBien)
            stor.zoneCorr.style.color = me.styles.cbien
            me.typederreurs[0]++
            stor.zoneInput.style.color = me.styles.cbien
            j3pDesactive(stor.zoneInput)
            j3pFreezeElt(stor.zoneInput)
            me.etat = 'navigation'
            me.sectionCourante()
          } else {
            stor.zoneInput.style.color = me.styles.cfaux
            stor.zoneCorr.style.color = me.styles.cfaux
            if (me.isElapsed) {
            // limite de temps
              j3pAddContent(stor.zoneCorr, tempsDepasse)
              me.typederreurs[10]++
            } else {
              const rInf = j3pVirgule(stor.brInf)
              const rSup = j3pVirgule(stor.brSup)

              stor.zoneCorr.innerHTML = (rInf === rSup)
                ? j3pChaine(ds.textes.corr1, { r: rInf })
                : j3pChaine(ds.textes.corr1, { r: rInf, s: rSup })
              j3pDesactive(stor.zoneInput)
              j3pBarre(stor.zoneInput)
              j3pFreezeElt(stor.zoneInput)
              me.typederreurs[1]++
            }
            me.etat = 'navigation'
            me.sectionCourante()
          }
        }
      }
      me.finCorrection()
      break // case 'correction':

    case 'navigation':
      if (me.sectionTerminee()) {
        me.parcours.pe = me.score / ds.nbitems
        me.afficheBoutonSectionSuivante()
        me.focus('sectioncontinuer')
      } else {
        me.etat = 'enonce'
        me.afficheBoutonSuite()
        me.focus('boutoncontinuer')
      }
      break // case 'navigation':
  }
}
