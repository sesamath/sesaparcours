import $ from 'jquery'
import { j3pAddContent, j3pAddElt, j3pArrondi, j3pChaine, j3pEmpty, j3pGetRandomInt, j3pParseArray } from 'src/legacy/core/functions'
import Droitegraduee from 'src/legacy/outils/droitegraduee/droitegraduee'
import textesGeneriques from 'src/lib/core/textes'

const { cBien, cFaux, essaieEncore, tempsDepasse } = textesGeneriques

export const params = {
  outils: [],
  /**
   * Ce qui est paramétrable dans l’éditeur de graphe
   * @type {LegacySectionParamsParametres}
   */
  parametres: [
    ['nbrepetitions', 5, 'entier', 'aucun'],
    ['indication', '', 'string', 'aucun'],
    ['limite', 0, 'entier', 'Temps disponible par répétition, en secondes'],
    ['nbchances', 1, 'entier', 'Nombre d’erreurs possibles (pour tenter de les corriger)'],
    ['zoomable', false, 'boolean', 'Zoom continu possible'],
    ['deplacable', true, 'boolean', 'Possibilité de déplacer l’axe'],
    ['negatifs', false, 'boolean', 'Affichage ou non des nombres négatifs'],
    ['positionO', -350, 'entier', 'En pixels. Position de 0 par rapport au centre de l’axe'],
    ['pixelspargraduation', 70, 'entier', 'Nombre de pixels par graduation'],
    ['pasdunegraduation', 1, 'number', 'Le pas d’une graduation'],
    ['graduationsecondaires', true, 'boolean', 'Affichage ou non des graduations secondaires'],
    ['etiquettes', true, 'boolean', 'Affichage ou non des étiquettes des graduations'],
    ['listeetiquettes', '[]', 'string', 'Les étiquettes affichées. N’a de sens que si etiquettes = false'],
    ['lepoint', '[1;8],1', 'string', 'Intervalle dans lequel va varier l’abscisse du point suivi du nombre de décimales, ce paramètre doit donc être écrit sous la forme [a;b],c']

  ]

}
export default function main () {
  const me = this
  const stor = me.storage
  let ds = me.donneesSection
  function getDonnees () {
    return {
      typesection: 'primaire',

      nbrepetitions: 3, //
      nbchances: 1,
      nbetapes: 1,
      structure: 'presentation3',

      zoomable: false,
      deplacable: true,
      negatifs: false,
      positionO: -350,
      pixelspargraduation: 70,
      pasdunegraduation: 1,
      graduationsecondaires: true,
      etiquettes: true,
      listeetiquettes: '[]', // [2,5]
      lepoint: '[1;8],1',
      nbzoommaxdecimal: -1,
      indication: '',
      textes: {
        titreExo: 'Placer un point ',
        cons1: 'Déplace le point M pour que son abscisse soit égale à £a.',
        cfaux: 'Voici l’emplacement correct.',
        comment1: 'Cela manque de précision&nbsp;!',
        ici: 'ici'
      }
    }
  }
  function enonceMain () {
    const intervalle = ds.lepoint.substring(0, ds.lepoint.indexOf(','))
    const puissance = Number(ds.lepoint.substring(ds.lepoint.indexOf(',') + 1))
    const borneinf = Number(intervalle.substring(1, intervalle.indexOf(';'))) * Math.pow(10, puissance)
    const bornesup = Number(intervalle.substring(intervalle.indexOf(';') + 1, intervalle.indexOf(']'))) * Math.pow(10, puissance)
    const abscisseM = (borneinf + bornesup) / (2 * Math.pow(10, puissance))
    do {
      stor.abscissePt = j3pGetRandomInt(borneinf, bornesup)
    } while (Math.abs(stor.abscissePt - (borneinf + bornesup) / 2) < 2 * Math.pow(10, puissance))
    stor.abscissePt = String(stor.abscissePt / Math.pow(10, puissance))
    const chainequestion = j3pChaine(ds.textes.cons1, { a: stor.abscissePt.replace('.', ',') })
    const conteneur = j3pAddElt(me.zonesElts.MG, 'div', '', { style: me.styles.etendre('petit.enonce', { padding: '10px' }) })
    j3pAddContent(conteneur, chainequestion)

    stor.dg = new Droitegraduee(conteneur, {
      height: 180,
      positionO: ds.positionO,
      pixelsParGraduation: ds.pixelspargraduation,
      pas: ds.pasdunegraduation,
      pixelsminpourgraduations: 40,
      pixelsminpouretiquette: 20,
      tailleetiquetteprincipale: 14,
      tailleetiquette: 10,
      taillesegmentprincipal: 14,
      taillesegment: 8,
      taillesegmentpetit: 4,
      couleurgraduationprincipale: 'black',
      couleurgraduation: 'blue',
      couleurgraduationpetite: 'green',
      epaisseurgraduationprincipale: 2,
      epaisseurgraduation: 1,
      couleuraxe: 'black',
      epaisseuraxe: 1,
      zoomable: ds.zoomable,
      deplacable: ds.deplacable,
      negatifs: ds.negatifs,
      graduationsecondaires: ds.graduationsecondaires,
      etiquettes: ds.etiquettes,
      listeetiquettes: j3pParseArray(ds.listeetiquettes), // listeetiquettes est une string et on veut un array ! Le pb est que c’est une string entrée par l’utilisateur dans les paramètres…
      listePoints: [{
        nom: 'M',
        abscisse: abscisseM,
        nbdecimales: 3,
        etiquette: false,
        taillepoint: 10,
        taillepolice: 20,
        couleur: '#00F',
        epaisseur: 3,
        fixe: false
      }],
      boutonszoomdecimal: true,
      boutonplacerpoint: false,
      maxZoomDecimal: ds.nbzoommax
    })

    stor.dg.construire()
    stor.zoneCorr = j3pAddElt(conteneur, 'div', '', { style: me.styles.etendre('petit.correction', { paddingTop: '30px' }) })
    me.finEnonce()
  }
  switch (me.etat) {
    case 'enonce':

      if (me.debutDeLaSection) {
        me.donneesSection = getDonnees()
        ds = me.donneesSection
        me.construitStructurePage({ structure: ds.structure, ratioGauche: 0.6 })
        me.surcharge()

        if (ds.nbzoommaxdecimal !== -1) {
          // pas de calcul automatique
          ds.nbzoommax = ds.nbzoommaxdecimal
          ds.nbdecimales = Number(ds.lepoint.substring(ds.lepoint.indexOf(',') + 1))
        } else {
          ds.nbdecimales = Number(ds.lepoint.substring(ds.lepoint.indexOf(',') + 1))
          ds.nbzoommax = ds.nbdecimales - 1 + Math.log(ds.pasdunegraduation) / Math.log(10)
        }

        me.typederreurs = [0, 0, 0, 0, 0, 0]
        me.afficheTitre(ds.textes.titreExo)
        if (ds.indication) me.indication(me.zonesElts.IG, ds.indication)
      } else {
        me.videLesZones()
      }

      enonceMain()
      break // case enonce

    case 'correction':
      me.cacheBoutonValider()
      {
        j3pEmpty(stor.zoneCorr)

        const reponseeleve = j3pArrondi(stor.dg.points.M.abscisse, ds.nbdecimales)
        const cbon = (reponseeleve === Number(stor.abscissePt))
        if (cbon) {
          me.score++
          stor.zoneCorr.style.color = me.styles.cbien
          stor.zoneCorr.innerHTML = cBien
          $('rect').first().remove()
          me.typederreurs[0]++
          me.etat = 'navigation'
          me.sectionCourante()
        } else {
          stor.zoneCorr.style.color = me.styles.cfaux
          stor.zoneCorr.innerHTML = cFaux
          j3pAddElt(stor.zoneCorr, 'br')
          if (me.isElapsed) {
          // limite de temps
            stor.zoneCorr.innerHTML = tempsDepasse
            me.typederreurs[10]++
            j3pAddContent(stor.zoneCorr, ds.textes.cfaux, { replace: true })
            $('rect').first().remove()
            stor.dg.addPoint({
              nom: ds.textes.ici,
              abscisse: stor.abscissePt,
              nbdecimales: 3,
              etiquette: false,
              taillepoint: 12,
              taillepolice: 18,
              couleur: 'red',
              epaisseur: 3,
              fixe: true
            })
            me.etat = 'navigation'
            me.sectionCourante()
          } else {
            if (Math.abs(stor.dg.points.M.abscisse - Number(stor.abscissePt)) < 2 * Math.pow(10, -ds.nbdecimales)) {
              j3pAddContent(stor.zoneCorr, ds.textes.comment1)
              j3pAddElt(stor.zoneCorr, 'br')
            }
            if (me.essaiCourant < ds.nbchances) {
              stor.zoneCorr.innerHTML += '<br>' + essaieEncore
              me.typederreurs[1]++
              // indication éventuelle ici
              me.etat = 'correction'
              me.afficheBoutonValider() // c’est la partie "correction" de la section qui sera exécutée
            } else {
            // Erreur au nème essai
              j3pAddContent(stor.zoneCorr, ds.textes.cfaux, { replace: false })
              stor.dg.points.M.fixe = true
              stor.dg.addPoint({
                nom: ds.textes.ici,
                abscisse: stor.abscissePt,
                nbdecimales: 3,
                etiquette: true,
                taillepoint: 12,
                taillepolice: 18,
                couleur: 'red',
                epaisseur: 3,
                fixe: true
              })
              $('rect').first().remove()
              me.typederreurs[2]++
              me.etat = 'navigation'
              me.sectionCourante()
            }
          }
        }
      }
      me.finCorrection()
      break // case "correction":

    case 'navigation':
      if (me.sectionTerminee()) {
        me.parcours.pe = me.score / ds.nbitems
        me.afficheBoutonSectionSuivante()
        me.focus('sectioncontinuer')
      } else {
        me.etat = 'enonce'
        me.afficheBoutonSuite()
      }

      break // case "navigation":
  }
}
