import { j3pAddElt, j3pGetPositionZone, j3pParseArray } from 'src/legacy/core/functions'
import Droitegraduee from 'src/legacy/outils/droitegraduee/droitegraduee'
export const params = {
  outils: [],
  /**
   * Ce qui est paramétrable dans l’éditeur de graphe
   * @type {LegacySectionParamsParametres}
   */
  parametres: [
    ['nbrepetitions', 5, 'entier', 'aucun'],
    ['indication', '', 'string', 'aucun'],
    ['limite', 0, 'entier', 'Temps disponible par répétition, en secondes'],
    ['nbchances', 1, 'entier', 'Nombre d’erreurs possibles (pour tenter de les corriger)'],
    ['nompoint', 'M', 'string', 'Nom de l’éventuel point à construire'],
    ['abscisse', '2.31', 'string', 'Abscisse du point éventuel'],
    ['etiquette', true, 'boolean', 'Etiquette du point éventuel'],
    ['boutonconstruire', false, 'boolean', 'Bouton de construction d’un point'],
    ['zoomdecimal', true, 'boolean', 'Afficher les boutons de zoom décimal'],
    ['zoomable', true, 'boolean', 'Zoom continu possible'],
    ['deplacable', true, 'boolean', 'Possibilité de déplacer l’axe'],
    ['negatifs', true, 'boolean', 'Affichage ou non des nombres négatifs'],
    ['positionO', -300, 'entier', 'Position de l’origine par rapport au centre de l’axe (en pixels)'],
    ['pixelspargraduation', 100, 'entier', 'Nombre de pixels par graduation'],
    ['pasdunegraduation', 1, 'reel', 'Le pas d’une graduation'],
    ['graduationsecondaires', true, 'boolean', 'Affichage ou non des graduations secondaires'],
    ['etiquettes', true, 'boolean', 'Affichage ou non des étiquettes des graduations'],
    ['listeetiquettes', '[]', 'string', 'Les étiquettes affichées. N’a de sens que si etiquettes = false'],
    ['nbmaxzoomdecimal', 3, 'entier', 'Nombre maximal de zoom décimal'],
    ['maxNbPoints', 1, 'entier', 'Nombre maximal de points constructibles'],
    ['aide', true, 'boolean', 'Affichage ou non d’une aide sur la manipulation de l’outil']
  ]

}

/**
 * section
 * @this {Parcours}
 */
export default function main () {
  const me = this
  let ds = me.donneesSection
  function getDonnees () {
    return {
      typesection: 'primaire',
      nbrepetitions: 1,
      nbetapes: 1,
      unparametre: 0,
      structure: 'presentation3',
      nompoint: 'M',
      abscisse: '2.31',
      etiquette: true,
      boutonconstruire: false,
      zoomdecimal: true,
      zoomable: true,
      deplacable: true,
      negatifs: true,
      positionO: -300,
      pixelspargraduation: 100,
      pas: 1,
      graduationsecondaires: true,
      etiquettes: true,
      listeetiquettes: '[]',
      nbmaxzoomdecimal: 3,
      aide: true,
      textes: {
        titreExo: 'OUTIL : Droite graduée',
        deplacable: 'Vous pouvez déplacer l’axe avec la souris.',
        zoomDecimal: '<strong>ZOOM DECIMAL</strong><br>Pour <strong>zoomer</strong>, cliquer sur un bouton <strong>puis</strong> sur la règle',
        zoomContinu: '<strong>ZOOM CONTINU</strong><br>Déplacer le curseur.'
      }
    }
  }

  switch (this.etat) {
    case 'enonce':
      if (this.debutDeLaSection) {
        this.donneesSection = getDonnees()
        ds = me.donneesSection
        this.surcharge()
        this.construitStructurePage({ structure: ds.structure, ratioGauche: 0.6 })
        this.afficheTitre(ds.textes.titreExo)
      } else {
        me.videLesZones()
      }

      {
        const dgData = {
          pixelsParGraduation: 150,
          pas: 1,
          pixelsminpourgraduations: 40,
          pixelsminpouretiquette: 20,
          tailleetiquetteprincipale: 14,
          tailleetiquette: 10,
          taillesegmentprincipal: 14,
          taillesegment: 8,
          taillesegmentpetit: 4,
          couleurgraduationprincipale: 'black',
          couleurgraduation: 'blue',
          couleurgraduationpetite: 'green',
          epaisseurgraduationprincipale: 2,
          epaisseurgraduation: 1,
          couleuraxe: 'black',
          epaisseuraxe: 1,
          positionO: ds.positionO,
          zoomable: ds.zoomable,
          deplacable: ds.deplacable,
          negatifs: ds.negatifs,
          etiquettes: ds.etiquettes,
          listeetiquettes: j3pParseArray(ds.listeetiquettes),
          graduationsecondaires: ds.graduationsecondaires,
          pointconstruit: {
            nom: 'F',
            nbdecimales: 3,
            etiquette: true,
            taillepoint: 10,
            taillepolice: 18,
            couleur: 'red',
            epaisseur: 2,
            fixe: false
          },
          hasZoomDecimal: ds.zoomdecimal,
          boutonplacerpoint: ds.boutonconstruire,
          maxNbPoints: ds.maxNbPoints,
          maxZoomDecimal: ds.nbmaxzoomdecimal
        }

        if (ds.nompoint) {
          dgData.listePoints = [{
            nom: ds.nompoint,
            abscisse: ds.abscisse,
            nbdecimales: 3,
            etiquette: ds.etiquette,
            taillepoint: 10,
            taillepolice: 18,
            couleur: 'red',
            epaisseur: 2,
            fixe: false
          }]
        }
        const container = j3pAddElt(me.zonesElts.MG, 'div', '', { style: { margin: '1em' } })
        const dg = new Droitegraduee(container, dgData)
        if (ds.aide) {
        // on aligne le cadre sur le div du svg, avec sa largeur
          const pos = j3pGetPositionZone(dg.div, container)
          const aideProps = {
            style: {
              margin: '1em 1em 1em ' + pos[0] + 'px',
              border: '#222 1px solid',
              padding: '0.5em 1em',
              backgroundColor: '#fff',
              width: dg.div.offsetWidth + 'px'
            }
          }
          const aide = j3pAddElt(container, 'div', '', aideProps)
          const pProps = { style: { padding: '0.3em' } }
          if (dgData.deplacable) {
            j3pAddElt(aide, 'p', ds.textes.deplacable, pProps)
          }
          if (dgData.hasZoomDecimal) {
            j3pAddElt(aide, 'p', ds.textes.zoomDecimal, pProps)
          }
          if (dgData.zoomable) {
            j3pAddElt(aide, 'p', ds.textes.zoomContinu, pProps)
          }
        }
        this.finEnonce()
      }
      break // case "enonce"

    case 'correction':

      this.cacheBoutonValider()
      this.score++
      this.etat = 'navigation'
      this.sectionCourante()
      break // case "correction":

    case 'navigation':
      if (!this.sectionTerminee()) {
        this.etat = 'enonce'
      }
      this.finNavigation(true)
      break // case "navigation":
  }
}
