import { j3pArrondi, j3pNombre, j3pVirgule, j3pRandomdec, j3pGetBornesIntervalle, j3pGetRandomInt, j3pAddElt, j3pChaine, j3pRemplacePoint, j3pFocus, j3pFreezeElt, j3pBarre, j3pAddContent, j3pEmpty } from 'src/legacy/core/functions'
import { j3pAffiche, j3pDesactive } from 'src/lib/mathquill/functions'
import textesGeneriques from 'src/lib/core/textes'

const { cBien, cFaux, tempsDepasse } = textesGeneriques

export const params = {
  outils: ['calculatrice'],
  /**
   * Ce qui est paramétrable dans l’éditeur de graphe
   * @type {LegacySectionParamsParametres}
   */
  parametres: [
    ['nbrepetitions', 3, 'entier', 'aucun'],
    ['indication', '', 'string', 'aucun'],
    ['limite', 0, 'entier', 'Temps disponible par répétition, en secondes'],
    ['nbchances', 1, 'entier', 'Nombre d’erreurs possibles (pour tenter de les corriger)'],
    ['b', '[1;7]', 'string', 'Utilisé dans la première étape de l’exercice'],
    ['ecart', 1, 'number', 'Écart'], // FIXME donner un peu plus d’indication
    ['c', '[1;7],1', 'string', 'Utilisé dans la seconde étape de l’exercice'],
    ['ecart2', 0.1, 'number', 'Écart bis…'] // FIXME donner un peu plus d’indication
  ]

}

/**
 * section
 * @this {Parcours}
 */
export default function main () {
  const me = this
  const stor = me.storage
  let ds = me.donneesSection
  function getDonnees () {
    return {

      typesection: 'primaire',
      nbrepetitions: 3,
      nbetapes: 2,
      nbchances: 1,
      structure: 'presentation1',
      b: '[1;7]',
      ecart: 1,
      c: '[1;7],1',
      ecart2: 0.1,
      indication: '',
      textes: {
        titreExo: 'Suite de nombres décimaux',
        cons1: 'Donner un nombre compris entre les nombres £a et £b.',
        cons2: 'Compléter la série de nombres suivante :',
        corr1: 'Une possibilité de réponse était £a.',
        corr2: 'Regarde la solution'
      }
    }
  }
  function enonceMain () {
    stor.conteneur = j3pAddElt(me.zonesElts.MG, 'div', '', { style: me.styles.etendre('petit.enonce', { padding: '30px' }) })
    if ((me.questionCourante % ds.nbetapes) === 1) {
      const [intMin, intMax] = j3pGetBornesIntervalle(ds.b)
      const b = j3pGetRandomInt(intMin, intMax)
      const a = b - ds.ecart
      me.stockage[0] = a
      me.stockage[1] = b
      j3pAddElt(stor.conteneur, 'div', j3pChaine(ds.textes.cons1, { a, b }))
      const zoneCons2 = j3pAddElt(stor.conteneur, 'div')
      zoneCons2.style.paddingTop = '20px'
      const elt = j3pAffiche(zoneCons2, '', '@1@', {
        input1: { texte: '', restrict: /[0-9,.]/, maxchars: 6, dynamique: true }
      })
      stor.zoneInput = [elt.inputList[0]]
    } else {
      const prem = j3pRandomdec(ds.c)
      stor.sol1 = j3pVirgule(j3pArrondi(prem + 3 * ds.ecart2, 6))
      stor.sol2 = j3pVirgule(j3pArrondi(prem + 4 * ds.ecart2, 6))
      j3pAddElt(stor.conteneur, 'div', ds.textes.cons2)
      const zoneCons2 = j3pAddElt(stor.conteneur, 'div')
      zoneCons2.style.paddingTop = '20px'
      const elt = j3pAffiche(zoneCons2, '', '£a - £b - £c - @1@ - @2@ ',
        {
          a: j3pVirgule(prem),
          b: j3pVirgule(j3pArrondi(prem + ds.ecart2, 6)),
          c: j3pVirgule(j3pArrondi(prem + 2 * ds.ecart2, 6)),
          input1: { texte: '', restrict: /[0-9,.]/, maxchars: 6, dynamique: true },
          input2: { texte: '', restrict: /[0-9,.]/, maxchars: 6, dynamique: true }
        }
      )
      stor.zoneInput = [...elt.inputList]
      me.stockage[9] = stor.sol1 + ' - ' + stor.sol2
      stor.solutionQ2 = j3pVirgule(prem) + ' - ' + j3pVirgule(j3pArrondi(prem + ds.ecart2, 6)) + ' - ' + j3pVirgule(j3pArrondi(prem + 2 * ds.ecart2, 6))
      stor.solutionQ2 += ' - ' + me.stockage[9]
    }
    stor.zoneInput.forEach(zone => zone.addEventListener('input', j3pRemplacePoint))
    j3pFocus(stor.zoneInput[0])
    stor.zoneCorr = j3pAddElt(me.zonesElts.MD, 'div', '', { style: me.styles.etendre('petit.correction', { padding: '10px' }) })

    me.finEnonce()
  }

  switch (me.etat) {
    case 'enonce':
      if (me.debutDeLaSection) {
        me.donneesSection = getDonnees()
        ds = me.donneesSection
        me.surcharge()
        me.construitStructurePage({ structure: ds.structure, ratioGauche: 0.6 })
        me.stockage = [0, 0, 0, 0]
        me.typederreurs = [0, 0, 0, 0, 0, 0]
        me.afficheTitre(ds.textes.titreExo)
        me.stockage[8] = []
        for (let k = 0; k < ds.nbrepetitions; k++) {
          me.stockage[8].push(0)
        }
        // [si 3 répétitions : = [a,b,c] où a note sur 2 à la première répétition
        if (ds.indication) me.indication(me.zonesElts.IG, ds.indication)
      } else {
        me.videLesZones()
      }
      // Première étape d’une répétition DANS LE CAS où y a plus de 2 étapes
      enonceMain()
      break // case "enonce":

    case 'correction':

      // me.cacheBoutonValider()
      j3pEmpty(stor.zoneCorr)
      {
        const aRepondu = ((me.questionCourante % ds.nbetapes) === 1)
          ? stor.zoneInput[0].value !== ''
          : stor.zoneInput[0].value !== '' && stor.zoneInput[1].value !== ''

        if (!aRepondu) {
          me.reponseManquante(stor.zoneCorr)
          for (let i = stor.zoneInput.length - 1; i >= 0; i--) {
            if (stor.zoneInput[i].value === '') j3pFocus(stor.zoneInput[i])
          }
        } else {
          let cbon
          if ((me.questionCourante % ds.nbetapes) === 1) {
            const s = j3pNombre(stor.zoneInput[0].value)

            cbon = (s < me.stockage[1] && s > me.stockage[0])
            if (cbon) {
              me.stockage[8][Math.floor((me.questionCourante - 0.1) / ds.nbetapes)]++
              me.score++
              stor.zoneCorr.style.color = me.styles.cbien
              stor.zoneCorr.innerHTML = cBien
              me.typederreurs[0]++
              stor.zoneInput[0].style.color = me.styles.cbien
              j3pDesactive(stor.zoneInput[0])
              j3pFreezeElt(stor.zoneInput[0])
            } else {
              stor.zoneCorr.style.color = me.styles.cfaux
              stor.zoneInput[0].style.color = me.styles.cfaux
              j3pAddContent(stor.zoneCorr, cFaux)
              if (me.isElapsed) { // limite de temps
                stor.zoneCorr.innerHTML = tempsDepasse
                me.typederreurs[10]++
              } else {
                j3pDesactive(stor.zoneInput[0])
                j3pBarre(stor.zoneInput[0])
                j3pFreezeElt(stor.zoneInput[0])
                const ch = j3pVirgule((me.stockage[1] + me.stockage[0]) / 2)
                j3pAddElt(stor.zoneCorr, 'br')
                j3pAddContent(stor.zoneCorr, j3pChaine(ds.textes.corr1, { a: ch }))
                me.typederreurs[1]++
              }
            }
            me.sectionCourante('navigation')
          } else if ((me.questionCourante % ds.nbetapes) === 0) {
            const bonneRep = [
              stor.zoneInput[0].value === stor.sol1,
              stor.zoneInput[1].value === stor.sol2
            ]
            cbon = bonneRep[0] && bonneRep[1]

            if (cbon) {
              me.stockage[8][Math.floor((me.questionCourante - 0.1) / ds.nbetapes)]++
              me.score++
              stor.zoneCorr.style.color = me.styles.cbien
              stor.zoneCorr.innerHTML = cBien
              me.typederreurs[0]++
              stor.zoneInput.forEach(elt => {
                elt.style.color = me.styles.cbien
                j3pDesactive(elt)
                j3pFreezeElt(elt)
              })
            } else {
              stor.zoneCorr.style.color = me.styles.cfaux
              stor.zoneCorr.innerHTML = cFaux
              if (me.isElapsed) { // limite de temps
                stor.zoneCorr.innerHTML = tempsDepasse
                me.typederreurs[10]++
              } else {
                j3pAddElt(stor.zoneCorr, 'br')
                j3pAddContent(stor.zoneCorr, ds.textes.corr2)
                me.typederreurs[1]++
              }
              stor.zoneInput.forEach((elt, index) => {
                j3pDesactive(elt)
                if (bonneRep[index]) {
                  elt.style.color = me.styles.cbien
                } else {
                  elt.style.color = me.styles.cfaux
                  j3pBarre(elt)
                }
                j3pFreezeElt(elt)
              })
              j3pAddElt(stor.conteneur, 'p', stor.solutionQ2, { style: me.styles.petit.correction })
            }

            me.etat = 'navigation'
          }
        }
      }
      me.finCorrection()
      break // case "correction":

    case 'navigation':
      if (me.sectionTerminee()) {
        for (let k = 0; k < me.stockage[8].length; k++) {
          if (me.stockage[8][k] === ds.nbetapes) { me.parcours.pe++ }
        }
        me.parcours.pe = me.parcours.pe / ds.nbrepetitions
      } else {
        me.etat = 'enonce'
      }
      break // case "navigation":
  }
}
