export const html = `L’objectif de ce graphe, qui comporte 5 noeuds, est de travailler les tables de multiplication.<br>
<br>
<span class='rouge'>A chaque noeud, pour passer au noeud suivant, trois bonnes réponses sur cinq sont demandées.</span><br>
<br>
Si tu réussis à aller au bout de ce graphe, tu auras atteint le <span class='rouge'>niveau 4</span> dans le calcul mental du type a &times b !`
