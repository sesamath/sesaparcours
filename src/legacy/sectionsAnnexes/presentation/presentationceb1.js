import { j3pBaseUrl } from 'src/lib/core/constantes'

const ceb1image1 = j3pBaseUrl + 'static/presentation/ceb1image1.png'
const ceb1image2 = j3pBaseUrl + 'static/presentation/ceb1image2.png'

export const html = `Ce parcours de 6 noeuds demande de trouver le compte en utilisant les étiquettes présentes dans la première ligne (ci-dessous 5,25,3,7,100,1).<br>
<br>
Chaque étape comporte un nombre variable de questions et chaque question est notée sur 1 point. Si l’étape comporte 4 questions, pour passer à l’étape suivante, il faut obtenir au minimumun total de 2 points.<br>
<br>
Ci-dessous, il faut trouver 215. On commence par effectuer 5×25 et on obtient alors 125 au début de la seconde ligne. Puis on effectue 125+100 et on obtient alors 225 au début de la troisième ligne.<br>
<img src="${ceb1image1}"><br>
À chaque étape, les étiquettes non utilisées sont recopiées dans la nouvelle ligne.<br>
Les couleurs permettent de retrouver le cheminement du calcul.<br>
Ci-dessus, il reste à calculer 225-10 pour trouver le compte 215.<br>
<br>
Quand le compte est trouvé, le programme demande d’écrire l’expression donnant le compte à partir des nombres de la première ligne&nbsp;:<br>
<img src="${ceb1image2}"><br>
<br>
A tout moment de la recherche du compte, vous pouvez cliquer sur le bouton OK pour voir une solution. Mais attention les points obtenus à la question diminueront !`
