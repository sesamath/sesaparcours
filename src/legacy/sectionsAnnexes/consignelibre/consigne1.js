export default {
  // nom du fichier vidéo éventuel pour l’étape 1, a priori inutile car paramètre général d’un parcours
  // "video" :"integrales_2",
  consigne: 'On pose $f(x)=x^3+4x^2+4x$.<br>Démontre que l’équation $f(x)=-1$ admet exactement trois solutions dans $\\R$.<br>',
  variables: [['a', '[2;15]'], ['b', '[2;15]'], ['c', '[2;15]']]
}
