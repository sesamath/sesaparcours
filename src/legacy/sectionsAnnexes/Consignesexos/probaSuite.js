export default {
  /* Dans toutes les consignes de ce type d’exercices, £a permet de déclarer la variable a. Il faut alors préciser l’intervalle dans lequel se trouve cette valeur
    Les consignes seront le plus souvent sur plusieurs lignes, d’où l’idée de créer un tableau
    la propriété variables de chaque énoncé est un tableau où chaque élément est un tableau de deux ou trois éléments
    Par exemple ['a','[5;15]'] signifie que a sera un entier de l’intervalle [5;15]
    ['a','[5;15]',100] signifie que a prend une valeur décimale de [0,05;0,15] avec 2 chiffres après la virgule
    */
  sujets: [
    {
      nomSujet: 'sujet inventé 1', // numéro 1
      consignes: ['Un élève arrive en classe de Terminale. Ses capacités lui ont permis de s’en sortir en mathématiques jusqu’à présent mais son travail irrégulier lui a été reproché en classe de Première.',
        'Un peu trop sûr de lui, s’il a la moyenne à un devoir, la probabilité qu’il ait la moyenne au devoir suivant vaut calcule[£a].',
        'Par contre, s’il n’obtient pas la moyenne, il consent à des efforts et cette fois-ci la probabilité d’avoir la moyenne au devoir suivant vaut calcule[£b].',
        'La probabilité qu’il ait la moyenne au premier devoir de mathématiques est égale à calcule[£c].',
        'Pour tout entier naturel non nul $n$, on note $M_n$ l’événement : « l’élève a eu la moyenne au $n$-ième devoir de l’année » et $p_n$ sa probabilité.'],
      variables: [['a', '[30;37]', 100], ['b', '[60;65]', 100], ['c', '[45;55]', 100]],
      // événements et probabilités de l’arbre
      // je retiens les différents événements pour un message d’alerte si l’élève donne un événement qui n’est pas dans cette liste
      listeEvt: ['M_n', 'M_{n+1}'],
      // la longueur de evtsProba correspond au nombre de premières branches
      // evstProba[i][2] est un tableau de couples evt/proba correspondant aux secondes branches pour chaque premier evt
      evtsProba: [['M_n', 'p_n', [['M_{n+1}', '£a'], ['\\overline{M_{n+1}}', '1-£a']]],
        ['\\overline{M_n}', '1-p_n', [['M_{n+1}', '£b'], ['\\overline{M_{n+1}}', '1-£b']]]],
      prbEnonce: ['p_n', '£a', '£b', '£c'], // ce sont les proba données dans l’énoncé au bon format (décimal ou fractionnaire)
      // correctionProba est la phrase qui traduit les proba de l’énoncé
      // les proba de l’énoncé doivent nécessairement être repérées par £a, £b, ... et cela correspond aux éléments de prbEnonce dans l’ordre
      correctionProba: 'D’après l’énoncé, $P(M_n)=£a$;<br/>$P_{M_n}(M_{n+1})=£b$;<br/>$P_{\\overline{M_n}}(M_{n+1})=£c$<br/>et $P(M_1)=£d$.',
      // différentes proba calculables avec la formule des proba totales
      questionPrbTotale: ['De plus, pour tout entier naturel $n\\geq 1$, on souhaite exprimer $p_{n+1}$ en fonction de $p_n$.',
        'Le faire sous forme simplifiée !',
        'p_{n+1}'],
      // réponses associées
      // p_{n+1} est de la forme ap_n+b où a est en position 1 et b en position 2. Le terme général de la suite est en position 3
      // Je donne également le rang initial en position 4 et le terme initial en position 5
      reponsePrbTotale: ['(£a-£b)p_n+£b', '£a-£b', '£b', 'p_n', '1', '£c'],
      // Correction associée à la proba à calculer
      correctionPrbTotale: '$p_{n+1}=P(M_{n+1})=P(M_n\\cap M_{n+1})+P(\\overline{M_n}\\cap M_{n+1})$ d’après la formule des probabilités totales<br/>$p_{n+1}=P(M_n)\\times P_{M_n}(M_{n+1})+P(\\overline{M_n})\\times P_{\\overline{M_n}}(M_{n+1})$<br/>$p_{n+1}=p_n\\times calcule[£a]+(1-p_n)\\times calcule[£b]=calcule[£a-£b]p_n+calcule[£b]$'
    },
    {
      nomSujet: 'Pondichéry avril 2013 S', // numéro 2
      consignes: ['Dans une entreprise, on s’intéresse à la probabilité qu’un salarié soit absent durant une période d’épidémie de grippe.',
        '- Un salarié malade est absent;',
        '- la première semaine de travail, le salarié n’est pas malade;',
        '- si la semaine $n$ le salarié n’est pas malade, il tombe malade la semaine $n+1$ avec une probabilité égale à $£a$;',
        '- si la semaine $n$ le salarié est malade, il reste malade la semaine $n+1$ avec une probabilité égale à $£b$.',
        'On désigne, pour tout entier naturel $n$ supérieur ou égal à 1, par $E_n$ l’évènement «le salarié est absent pour cause de maladie la $n$-ième semaine». On note $p_n$ la probabilité de l’évènement $E_n$.',
        'On a ainsi : $p_1=0$ et, pour tout entier naturel $n$ supérieur ou égal à 1 : $0\\leq p_n<1$.'],
      variables: [['a', '[3;9]', 100], ['b', '[21;29]', 100]],
      // événements et probabilités de l’arbre
      // je retiens les différents événements pour un message d’alerte si l’élève donne un événement qui n’est pas dans cette liste
      listeEvt: ['E_n', 'E_{n+1}'],
      // la longueur de evtsProba correspond au nombre de premières branches
      // evstProba[i][2] est un tableau de couples evt/proba correspondant aux secondes branches pour chaque premier evt
      evtsProba: [['E_n', 'p_n', [['E_{n+1}', '£b'], ['\\overline{E_{n+1}}', '1-£b']]],
        ['\\overline{E_n}', '1-p_n', [['E_{n+1}', '£a'], ['\\overline{E_{n+1}}', '1-£a']]]],
      prbEnonce: ['p_n', '£a', '£b', '0'], // ce sont les proba données dans l’énoncé au bon format (décimal ou fractionnaire)
      // correctionProba est la phrase qui traduit les proba de l’énoncé
      // les proba de l’énoncé doivent nécessairement être repérées par £a, £b, ... et cela correspond aux éléments de prbEnonce dans l’ordre
      correctionProba: 'D’après l’énoncé, $P(E_n)=£a$;<br/>$P_{\\overline{E_n}}(E_{n+1})=£b$;<br/>$P_{E_n}(E_{n+1})=£c$<br/>et $P(E_1)=£d$.',
      // différentes proba calculables avec la formule des proba totales
      questionPrbTotale: ['De plus, pour tout entier naturel $n\\geq 1$, on souhaite exprimer $p_{n+1}$ en fonction de $p_n$.',
        'Le faire sous forme simplifiée !',
        'p_{n+1}'],
      // réponses associées
      // p_{n+1} est de la forme ap_n+b où a est en position 1 et b en position 2. Le terme général de la suite est en position 3
      // Je donne également le rang initial en position 4 et le terme initial en position 5
      reponsePrbTotale: ['(£b-£a)p_n+£a', '£b-£a', '£a', 'p_n', '1', '0'],
      // Correction associée à la proba à calculer
      correctionPrbTotale: '$p_{n+1}=P(E_{n+1})=P(E_n\\cap E_{n+1})+P(\\overline{E_n}\\cap E_{n+1})$ d’après la formule des probabilités totales<br/>$p_{n+1}=P(E_n)\\times P_{E_n}(E_{n+1})+P(\\overline{E_n})\\times P_{\\overline{E_n}}(E_{n+1})$<br/>$p_{n+1}=p_n\\times calcule[£b]+(1-p_n)\\times calcule[£a]=calcule[£b-£a]p_n+calcule[£a]$'
    },
    {
      nomSujet: 'Polynésie juin 2011 S', // numéro 3
      consignes: ['Un joueur débute un jeu vidéo et effectue plusieurs parties successives.',
        'On admet que:',
        '- la probabilité qu’il gagne la première partie est de $£a$;',
        '- s’il gagne une partie, la probabilité de gagner la suivante est égale à $£b$;',
        '- s’il perd une partie, la probabilité de gagner la suivante est égale à $£c$.',
        'On note, pour tout entier naturel $n$ non nul:',
        '$G_n$ l’évènement «le joueur gagne la $n$-ième partie»;',
        '$p_n$ la probabilité de l’évènement $G_n$·',
        'On a donc $p_1=£a$.'],
      variables: [['a', '[1;2]', 10], ['b', '[7;8]', 10], ['c', '[4;6]', 10]],
      // événements et probabilités de l’arbre
      // je retiens les différents événements pour un message d’alerte si l’élève donne un événement qui n’est pas dans cette liste
      listeEvt: ['G_n', 'G_{n+1}'],
      // la longueur de evtsProba correspond au nombre de premières branches
      // evstProba[i][2] est un tableau de couples evt/proba correspondant aux secondes branches pour chaque premier evt
      evtsProba: [['G_n', 'p_n', [['G_{n+1}', '£b'], ['\\overline{G_{n+1}}', '1-£b']]],
        ['\\overline{G_n}', '1-p_n', [['G_{n+1}', '£c'], ['\\overline{G_{n+1}}', '1-£c']]]],
      prbEnonce: ['p_n', '£a', '£b', '£c'], // ce sont les proba données dans l’énoncé au bon format (décimal ou fractionnaire)
      // correctionProba est la phrase qui traduit les proba de l’énoncé
      // les proba de l’énoncé doivent nécessairement être repérées par £a, £b, ... et cela correspond aux éléments de prbEnonce dans l’ordre
      correctionProba: 'D’après l’énoncé, $P(G_n)=£a$;<br/>$P(G_1)=£b$;<br/>$P_{G_n}(G_{n+1})=£c$<br/>et $P_{\\overline{G_n}}(G_{n+1})=£d$.',
      // différentes proba calculables avec la formule des proba totales
      questionPrbTotale: ['De plus, pour tout entier naturel $n\\geq 1$, on souhaite exprimer $p_{n+1}$ en fonction de $p_n$.',
        'Le faire sous forme simplifiée !',
        'p_{n+1}'],
      // réponses associées
      // p_{n+1} est de la forme ap_n+b où a est en position 1 et b en position 2. Le terme général de la suite est en position 3
      // Je donne également le rang initial en position 4 et le terme initial en position 5
      reponsePrbTotale: ['(£b-£c)p_n+£c', '£b-£c', '£c', 'p_n', '1', '£a'],
      // Correction associée à la proba à calculer
      correctionPrbTotale: '$p_{n+1}=P(G_{n+1})=P(G_n\\cap G_{n+1})+P(\\overline{G_n}\\cap G_{n+1})$ d’après la formule des probabilités totales<br/>$p_{n+1}=P(G_n)\\times P_{G_n}(G_{n+1})+P(\\overline{G_n})\\times P_{\\overline{G_n}}(G_{n+1})$<br/>$p_{n+1}=p_n\\times calcule[£b]+(1-p_n)\\times calcule[£c]=calcule[£b-£c]p_n+calcule[£c]$'
    },
    {
      nomSujet: 'Antilles-Guyane septembre 2011 S', // numéro 4
      consignes: ['Un site internet propose un jeu en ligne. Pour ce jeu :',
        '- si l’internaute gagne une partie, la probabilité qu’il gagne la partie suivante est égale à $\\frac{£a}{5}$;',
        '- si l’internaute perd une partie, la probabilité qu’il perde la partie suivante est égale à $\\frac{£b}{5}$.',
        'Pour tout entier naturel non nul $n$, on désigne par $G_n$ l’évènement «l’internaute gagne la $n$-ième partie» et on note $p_n$ la probabilité de l’évènement $G_n$.',
        'L’internaute gagne toujours la première partie et donc $p_1=1$.'],
      variables: [['a', '[1;2]'], ['b', '[1;2]']],
      // événements et probabilités de l’arbre
      // je retiens les différents événements pour un message d’alerte si l’élève donne un événement qui n’est pas dans cette liste
      listeEvt: ['G_n', 'G_{n+1}'],
      // la longueur de evtsProba correspond au nombre de premières branches
      // evstProba[i][2] est un tableau de couples evt/proba correspondant aux secondes branches pour chaque premier evt
      evtsProba: [['G_n', 'p_n', [['G_{n+1}', '\\frac{£a}{5}'], ['\\overline{G_{n+1}}', '\\frac{5-£a}{5}']]],
        ['\\overline{G_n}', '1-p_n', [['G_{n+1}', '\\frac{5-£b}{5}'], ['\\overline{G_{n+1}}', '\\frac{£b}{5}']]]],
      prbEnonce: ['p_n', '\\frac{£a}{5}', '\\frac{£b}{5}', '1'], // ce sont les proba données dans l’énoncé au bon format (décimal ou fractionnaire)
      // correctionProba est la phrase qui traduit les proba de l’énoncé
      // les proba de l’énoncé doivent nécessairement être repérées par £a, £b, ... et cela correspond aux éléments de prbEnonce dans l’ordre
      correctionProba: 'D’après l’énoncé, $P(G_n)=£a$;<br/>$P_{G_n}(G_{n+1})=£b$;<br/>$P_{\\overline{G_n}}(\\overline{G_{n+1}})=£c$<br/>et $P(G_1)=1$.',
      // différentes proba calculables avec la formule des proba totales
      questionPrbTotale: ['De plus, pour tout entier naturel $n\\geq 1$, on souhaite exprimer $p_{n+1}$ en fonction de $p_n$.',
        'Le faire sous forme simplifiée !',
        'p_{n+1}'],
      // réponses associées
      // p_{n+1} est de la forme ap_n+b où a est en position 1 et b en position 2. Le terme général de la suite est en position 3
      // Je donne également le rang initial en position 4 et le terme initial en position 5
      reponsePrbTotale: ['\\frac{£a-(5-£b)}{5}p_n+\\frac{5-£b}{5}', '(£a-5+£b)/5', '(5-£b)/5', 'p_n', '1', '1'],
      // Correction associée à la proba à calculer
      correctionPrbTotale: '$p_{n+1}=P(G_{n+1})=P(G_n\\cap G_{n+1})+P(\\overline{G_n}\\cap G_{n+1})$ d’après la formule des probabilités totales<br/>$p_{n+1}=P(G_n)\\times P_{G_n}(G_{n+1})+P(\\overline{G_n})\\times P_{\\overline{G_n}}(G_{n+1})$<br/>$p_{n+1}=p_n\\times \\frac{calcule[£a]}{5}+(1-p_n)\\times \\frac{calcule[5-£b]}{5}=\\frac{calcule[£a-5+£b]}{5}p_n+\\frac{calcule[5-£b]}{5}$'
    }
  ]
}
