export default {
  /* Dans toutes les consignes de ce type d’exercices, £a permet de déclarer la variable a. Il faut alors préciser l’intervalle dans lequel se trouve cette valeur
        Les consignes seront le plus souvent sur plusieurs lignes, d’où l’idée de créer un tableau
        la propriété variables de chaque énoncé est un tableau où chaque élément est un tableau de deux ou trois éléments
        Par exemple ['a','[5;15]'] signifie que a sera un entier de l’intervalle [5;15]
        ['a','[5;15]',100] signifie que a prend une valeur décimale de [0,05;0,15] avec 2 chiffres après la virgule
        */
  sujets: [
    {
      nomSujet: '', // numéro 0
      consignes: ['Le chiffre d’affaires d’une entreprise de vente par internet s’élevait à calcule[£a*10000] € en 2014.'],
      variables: [['a', '[17;29]'], ['b', '[11;17]'], ['c', '[30;50]', 10], ['d', '[9;14]']],
      questionVA: ['L’année suivante, ce chiffre d’affaires a augmenté de £b %.',
        'En 2015, il valait ainsi £e&1& €.'], // £e vaudra "environ" ou "" suivant que la valeur à calculer soit exacte ou non
      questionTaux1: ['L’année suivante, il valait calcule[(£a+£c)*10000] €.',
        'De 2014 à 2015, le chiffre d’affaires de cette entreprise a donc #1# £e &1& %.'], // questionTaux1 sera utilisée si questionVA n’est pas posée. Dans cette question #1# est une liste prenant les valeurs "augmenté" ou "diminué"
      questionTaux2: ['En 2015, il valait ainsi £f € puis calcule[£f+£c*10000] € en 2016.', // La variable £f sera la réponse à questionVA
        'De 2015 à 2016, le chiffre d’affaires de cette entreprise a donc #1# £e &1& %.'], // si questionVA est posée, on demandera le taux d’évolution de 2015 à 2016
      questionVD: ['Sachant qu’entre 2013 et 2014 le chiffre d’affaires de cette entreprise avait augmenté de £d %, en 2013 il valait £e:',
        '&1& €.'],
      arrondisTxt: ['Arrondir à 100 €.', 'Arrondir à 0,1 %.', 'Arrondir à 100 €.'], // le premier arrondi est pour questionVA, le 2ème pour le taux et le troisième pour question VD
      arrondis: [100, 0.1, 100],
      valeurs: ['£a*10000', '£b', '£d'], // la 1ère valeur est la valeur de départ pour q1 et la valeur finale pour q3, la 2ème, est le taux d’évolution dans q1 et la troisième le taux d’évolution dans q3
      // réponses associées
      reponses: { va: '£a*10000*(1+£b/100)', tx1: '£c/£a', tx2: '£c*10000/£f', vd: '£a*10000/(1+£d/100)' },
      // Correction associée à la proba à calculer
      correctionVA: ['Or $calcule[£a*10000]\\times (1+\\frac{£b}{100})£s£{r3}$.',
        "Donc en 2015, le chiffre d’affaires de cette entreprise s'élevait à £e£r&nbsp;€."],
      correctionTaux1: ['Or $\\frac{calcule[(£a+£c)*10000]-calcule[£a*10000]}{calcule[£a*10000]}£s£t\\text{ soit £{e1}}£r$&nbsp;%.',
        'Donc le chiffre d’affaires a augmenté £e $£r$ % de 2014 à 2015.'],
      correctionTaux2: ['Or $\\frac{calcule[£f+£c*10000]-calcule[£f]}{calcule[£f]}£s£t\\text{ soit £{e1}}£r$&nbsp;%.',
        'Donc le chiffre d’affaires a augmenté £e $£r$ % de 2015 à 2016.'],
      correctionVD: ['Or $\\frac{calcule[£a*10000]}{1+\\frac{£d}{100}}£s£{r3}$.',
        "Donc en 2013, le chiffre d’affaires de cette entreprise s'élevait à £e£r&nbsp;€."]
    },
    {
      nomSujet: '', // numéro 1
      consignes: ["Dans une commune, le prix moyen d’un hectare de terre s'élevait en 2007 à calcule[£a*100] €."],
      variables: [['a', '[49;55]'], ['b', '[21;29]'], ['c', '[30;50]'], ['d', '[12;17]']],
      questionVA: ['Entre 2007 et 2012, ce prix a baissé de £b %.',
        'En 2012, il valait ainsi £e&1& €.'], // £e vaudra "environ" ou "" suivant que la valeur à calculer soit exacte ou non
      questionTaux1: ['En 2012, il valait calcule[£a*100-£c*10] €.',
        'De 2007 à 2012, dans cette commune, le prix d’un hectare de terre a donc #1# £e &1& %.'], // questionTaux1 sera utilisée si questionVA n’est pas posée. Dans cette question #1# est une liste prenant les valeurs "augmenté" ou "diminué"
      questionTaux2: ['En 2012, il valait ainsi £f € puis calcule[£f-£c*5] € en 2017.', // La variable £f sera la réponse à questionVA
        'De 2012 à 2017, dans cette commune, le prix d’un hectare de terre a donc #1# £e &1& %.'], // si questionVA est posée, on demandera le taux d’évolution de 2015 à 2016
      questionVD: ['Sachant qu’entre 2002 et 2007 le prix d’un hectare de terre de cette commune avait augmenté de £d %, en 2002 il valait £e:',
        '&1& €.'],
      arrondisTxt: ['Arrondir à l’unité.', 'Arrondir à 0,1 %.', 'Arrondir à l’unité.'], // le premier arrondi est pour questionVA, le 2ème pour le taux et le troisième pour question VD
      arrondis: [1, 0.1, 1],
      valeurs: ['£a*100', '-£b', '£d'], // la 1ère valeur est la valeur de départ pour q1 et la valeur finale pour q3, la 2ème, est le taux d’évolution dans q1 et la troisième le taux d’évolution dans q3
      // réponses associées
      reponses: { va: '£a*100*(1-£b/100)', tx1: '-£c*10/(£a*100)', tx2: '-£c*5/£f', vd: '£a*100/(1+£d/100)' },
      // Correction associée à la proba à calculer
      correctionVA: ['Or $calcule[£a*100]\\times (1-\\frac{£b}{100})£s£{r3}$.',
        "Donc en 2012, le prix d’un hectare dans cette commune s'élevait à £e£r&nbsp;€."],
      correctionTaux1: ['Or $\\frac{calcule[£a*100-£c*10]-calcule[£a*100]}{calcule[£a*100]}£s£t\\text{ soit £{e1}}£r$&nbsp;%.',
        'Donc le prix d’un hectare dans cette commune a diminué £e $calcule[abs(£r)]$ % de 2007 à 2012.'],
      correctionTaux2: ['Or $\\frac{calcule[£f-£c*5]-calcule[£f]}{calcule[£f]}£s£t\\text{ soit £{e1}}£r$&nbsp;%.',
        'Donc le prix d’un hectare dans cette commune a diminué £e $calcule[abs(£r)]$ % de 2012 à 2017.'],
      correctionVD: ['Or $\\frac{calcule[£a*100]}{1+\\frac{£d}{100}}£s£{r3}$.',
        "Donc en 2002, le prix d’un hectare dans cette commune s'élevait à £e£r&nbsp;€."]
    },
    {
      nomSujet: '', // numéro 2
      consignes: ['Un développeur informatique s’intéresse au nombre de courriels échangés par semaine pour travailler sur un projet d’équipe.',
        'La semaine du 8 septembre au 14 septembre, il en a compté £a.'],
      variables: [['a', '[210;262]'], ['b', '[27;45]', 10], ['c', '[15;32]'], ['d', '[28;43]', 10]],
      questionVA: ['La semaine suivante, le nombre de courriels a augmenté de $£b$&nbsp;%.',
        'Du 15 au 21 septembre, il en a ainsi échangés £e&1&.'], // £e vaudra "environ" ou "" suivant que la valeur à calculer soit exacte ou non
      questionTaux1: ['Du 15 au 21 septembre, il a échangé calcule[£a+£c] courriels.',
        'De la première à la deuxième semaine, le nombre de courriels échangés a donc #1# £e &1& %.'], // questionTaux1 sera utilisée si questionVA n’est pas posée. Dans cette question #1# est une liste prenant les valeurs "augmenté" ou "diminué"
      questionTaux2: ['Du 15 au 21 septembre, il a ainsi échangé £f courriels puis calcule[£f+£c] du 22 au 28.', // La variable £f sera la réponse à questionVA
        'De la deuxième à la troisième semaine, le nombre de courriels échangés a donc #1# £e &1& %.'], // si questionVA est posée, on demandera le taux d’évolution de 2015 à 2016
      questionVD: ['Sachant qu’entre la première semaine de septembre (du 1er au 7) et la deuxième le nombre de courriels échangés avait baissé de $£d$&nbsp;%, entre le 1er et le 7 septembre il avait échangé £e:',
        '&1& courriels.'],
      arrondisTxt: ['Arrondir à l’unité.', 'Arrondir à 0,1 %.', 'Arrondir à l’unité.'], // le premier arrondi est pour questionVA, le 2ème pour le taux et le troisième pour question VD
      arrondis: [1, 0.1, 1],
      valeurs: ['£a', '£b', '-£d'], // la 1ère valeur est la valeur de départ pour q1 et la valeur finale pour q3, la 2ème, est le taux d’évolution dans q1 et la troisième le taux d’évolution dans q3
      // réponses associées
      reponses: { va: '£a*(1+£b/100)', tx1: '£c/£a', tx2: '£c/£f', vd: '£a/(1-£d/100)' },
      // Correction associée à la proba à calculer
      correctionVA: ['Or $£a\\times (1+\\frac{£b}{100})£s£{r3}$.',
        'Donc entre le 15 et le 21 septembre, il a échangé £e£r courriels.'],
      correctionTaux1: ['Or $\\frac{calcule[£a+£c]-£a}{£a}£s£t\\text{ soit £{e1}}£r$&nbsp;%.',
        'Donc le nombre de courriels échangés entre ces deux semaines a augmenté £e $calcule[abs(£r)]$&nbsp;%.'],
      correctionTaux2: ['Or $\\frac{calcule[£f+£c]-£f}{£f}£s£t\\text{ soit £{e1}}£r$&nbsp;%.',
        'Donc le nombre de courriels échangés entre ces deux semaines a augmenté £e $calcule[abs(£r)]$&nbsp;%.'],
      correctionVD: ['Or $\\frac{£a}{1-\\frac{£d}{100}}£s£{r3}$.',
        "Donc entre le 1er et le 7 septembre, le nombre de courriels échangés s'élevait à £e£r."]
    },
    {
      nomSujet: '', // numéro 3
      consignes: ['En 2016, le conducteur d’un véhicule diesel a parcouru calcule[£a*100]&nbsp;km.'],
      variables: [['a', '[231;251]'], ['b', '[21;29]'], ['c', '[15;34]'], ['d', '[7;13]']],
      questionVA: ['En 2017, il a parcouru £b % de plus qu’en 2016.',
        'En 2017, il a ainsi parcouru £e&1& km avec son véhicule.'], // £e vaudra "environ" ou "" suivant que la valeur à calculer soit exacte ou non
      questionTaux1: ['En 2017, il a parcouru calcule[£a*100-£c*100]&nbsp;km.',
        'De 2016 à 2017, le nombre de kilomètres parcourus avec son véhicule a donc #1# £e &1& %.'], // questionTaux1 sera utilisée si questionVA n’est pas posée. Dans cette question #1# est une liste prenant les valeurs "augmenté" ou "diminué"
      questionTaux2: ['En 2017, il a ainsi parcouru £f km puis calcule[£f-£c*100]&nbsp;km en 2018.', // La variable £f sera la réponse à questionVA
        'De 2017 à 2018, le nombre de kilomètres parcourus avec son véhicule a donc #1# £e &1& %.'], // si questionVA est posée, on demandera le taux d’évolution de 2015 à 2016
      questionVD: ['Sachant qu’entre 2015 et 2016 le nombre de kilomètres parcourus a augmenté de £d&nbsp;%, en 2015 il valait £e:',
        '&1&.'],
      arrondisTxt: ['Arrondir à 1 km.', 'Arrondir à 0,1 %.', 'Arrondir à l’unité.'], // le premier arrondi est pour questionVA, le 2ème pour le taux et le troisième pour question VD
      arrondis: [1, 0.1, 1],
      valeurs: ['£a*100', '£b', '£d'], // la 1ère valeur est la valeur de départ pour q1 et la valeur finale pour q3, la 2ème, est le taux d’évolution dans q1 et la troisième le taux d’évolution dans q3
      // réponses associées
      reponses: { va: '£a*100*(1+£b/100)', tx1: '-£c/£a', tx2: '-£c*100/£f', vd: '£a*100/(1+£d/100)' },
      // Correction associée à la proba à calculer
      correctionVA: ['Or $calcule[£a*100]\\times (1+\\frac{£b}{100})£s£{r3}$.',
        'Donc en 2017, il a parcouru £e£r km avec son véhicule.'],
      correctionTaux1: ['Or $\\frac{calcule[£a*100-£c*100]-calcule[£a*100]}{calcule[£a*100]}£s£t\\text{ soit £{e1}}£r$&nbsp;%.',
        'Donc le nombre de kilomètres parcourus avec son véhicule a diminué £e $calcule[abs(£r)]$&nbsp;% de 2016 à 2017.'],
      correctionTaux2: ['Or $\\frac{calcule[£f-£c*100]-calcule[£f]}{calcule[£f]}£s£t\\text{ soit £{e1}}£r$&nbsp;%.',
        'Donc le nombre de kilomètres parcourus avec son véhicule a diminué £e $calcule[abs(£r)]$&nbsp;% de 2017 à 2018.'],
      correctionVD: ['Or $\\frac{calcule[£a*100]}{1+\\frac{£d}{100}}£s£{r3}$.',
        'Donc en 2015, il a parcouru £e£r km avec son véhicule.']
    },
    {
      nomSujet: '', // numéro 4
      consignes: ['En septembre, Quentin a relevé le nombre de pas effectués au cours d’une journée.',
        'Le 8, il a effectué calcule[£a*100] pas.'],
      variables: [['a', '[90;107]'], ['b', '[7;13]'], ['c', '[40;70]'], ['d', '[7;15]']],
      questionVA: ['Le 9 septembre, il en a effectué £b&nbsp;% de moins que le 8.',
        'Le 9, il a ainsi effectué £e&1& pas.'], // £e vaudra "environ" ou "" suivant que la valeur à calculer soit exacte ou non
      questionTaux1: ['Le 9 septembre, il en a effectué calcule[£a*100-£c*10].',
        'Du 8 au 9 septembre, le nombre de pas effectués a donc #1# £e &1& %.'], // questionTaux1 sera utilisée si questionVA n’est pas posée. Dans cette question #1# est une liste prenant les valeurs "augmenté" ou "diminué"
      questionTaux2: ['Le 9 septembre, il a ainsi effectué £f pas puis calcule[£f+£c*10] le 10.', // La variable £f sera la réponse à questionVA
        'Du 9 au 10 septembre, le nombre de pas effectués a donc #1# £e &1& %.'], // si questionVA est posée, on demandera le taux d’évolution de 2015 à 2016
      questionVD: ['Sachant qu’entre le 7 et le 8 septembre, le nombre de pas qu’il a effectué a diminué de £d&nbsp;%, le 7 il en a effectué £e:',
        '&1&.'],
      arrondisTxt: ['Arrondir à l’unité.', 'Arrondir à 0,1 %.', 'Arrondir à l’unité.'], // le premier arrondi est pour questionVA, le 2ème pour le taux et le troisième pour question VD
      arrondis: [1, 0.1, 1],
      valeurs: ['£a*100', '-£b', '-£d'], // la 1ère valeur est la valeur de départ pour q1 et la valeur finale pour q3, la 2ème, est le taux d’évolution dans q1 et la troisième le taux d’évolution dans q3
      // réponses associées
      reponses: { va: '£a*100*(1-£b/100)', tx1: '-£c*10/(£a*100)', tx2: '£c*10/£f', vd: '£a*100/(1-£d/100)' },
      // Correction associée à la proba à calculer
      correctionVA: ['Or $calcule[£a*100]\\times (1-\\frac{£b}{100})£s£{r3}$.',
        'Donc le 8 septembre, il a effectué £e£r pas.'],
      correctionTaux1: ['Or $\\frac{calcule[£a*100-£c*10]-calcule[£a*100]}{calcule[£a*100]}£s£t\\text{ soit £{e1}}£r$&nbsp;%.',
        'Donc le nombre de pas effectués a diminué £e $calcule[abs(£r)]$ % du 7 au 8 septembre.'],
      correctionTaux2: ['Or $\\frac{calcule[£f+£c*10]-calcule[£f]}{calcule[£f]}£s£t\\text{ soit £{e1}}£r$&nbsp;%.',
        'Donc le nombre de pas effectués a augmenté £e $calcule[abs(£r)]$ % du 8 au 9 septembre.'],
      correctionVD: ['Or $\\frac{calcule[£a*100]}{1-\\frac{£d}{100}}£s£{r3}$.',
        'Donc le 7 septembre, il a effectué £e£r pas.']
    },
    {
      nomSujet: '', // numéro 5
      consignes: ['Le 1er janvier 2016, une action côtée au CAC 40 valait £a&nbsp;€'],
      variables: [['a', '[75;97]'], ['b', '[6;14]'], ['c', '[40;70]', 10], ['d', '[7;15]']],
      questionVA: ['Un an plus tard, son cours avait augmenté de £b&nbsp;%.',
        'Le 1er janvier 2017, elle valait ainsi £e&1& €.'], // £e vaudra "environ" ou "" suivant que la valeur à calculer soit exacte ou non
      questionTaux1: ['Le premier janvier de l’année suivante, elle valait $calcule[£a+£c]$&nbsp;€.',
        'En un an, le prix de cette action a donc #1# £e &1& %.'], // questionTaux1 sera utilisée si questionVA n’est pas posée. Dans cette question #1# est une liste prenant les valeurs "augmenté" ou "diminué"
      questionTaux2: ['Un an après, elle valait ainsi $£f$ € puis calcule[£f-£c] le 1er janvier 2018.', // La variable £f sera la réponse à questionVA
        'De 2017 à 2018, le prix de cette action a donc #1# £e &1& %.'], // si questionVA est posée, on demandera le taux d’évolution de 2015 à 2016
      questionVD: ['Sachant que du 1er janvier 2015 au 1er janvier 2016, le prix de l’action a diminué de £d&nbsp;%, en 2015 elle valait £e:',
        '&1& €.'],
      arrondisTxt: ['Arrondir à 0,01 €.', 'Arrondir à 0,1 %.', 'Arrondir à 0,01 €.'], // le premier arrondi est pour questionVA, le 2ème pour le taux et le troisième pour question VD
      arrondis: [0.01, 0.1, 0.01],
      valeurs: ['£a', '£b', '-£d'], // la 1ère valeur est la valeur de départ pour q1 et la valeur finale pour q3, la 2ème, est le taux d’évolution dans q1 et la troisième le taux d’évolution dans q3
      // réponses associées
      reponses: { va: '£a*(1+£b/100)', tx1: '£c/£a', tx2: '-£c/£f', vd: '£a/(1-£d/100)' },
      // Correction associée à la proba à calculer
      correctionVA: ['Or $£a\\times (1+\\frac{£b}{100})£s£{r3}$.',
        'Donc le 1er janvier 2017, l’action valait £e$£r$&nbsp;€.'],
      correctionTaux1: ['Or $\\frac{calcule[£a+£c]-£a}{£a}£s£t\\text{ soit £{e1}}£r$&nbsp;%.',
        'Donc le prix de l’action a diminué £e $calcule[abs(£r)]$&nbsp;% de 2016 à 2017.'],
      correctionTaux2: ['Or $\\frac{calcule[£f-£c]-calcule[£f]}{calcule[£f]}£s£t\\text{ soit £{e1}}£r$&nbsp;%.',
        'Donc le prix de l’action a diminué £e $calcule[abs(£r)]$ % de 2017 à 2018.'],
      correctionVD: ['Or $\\frac{£a}{1-\\frac{£d}{100}}£s£{r3}$.',
        'Donc le 1er janvier 2015, l’action valait £e$£r$&nbsp;€.']
    },
    {
      nomSujet: '', // numéro 6
      consignes: ['Antoine relève chaque mois la quantité de pluie tombée.',
        'En mars 2018, il a relevé £a mm'],
      variables: [['a', '[75;91]'], ['b', '[50;85]', 10], ['c', '[5;13]'], ['d', '[65;120]', 10]],
      questionVA: ['En avril, les précipitations ont baissé de $£b$&nbsp;%.',
        'En avril 2018, il est ainsi tombé £e&1& mm de pluie.'], // £e vaudra "environ" ou "" suivant que la valeur à calculer soit exacte ou non
      questionTaux1: ['En avril, il est tombé calcule[£a-£c]&nbsp;mm.',
        'En un mois, le quantité de pluie tombée a donc #1# £e &1& %.'], // questionTaux1 sera utilisée si questionVA n’est pas posée. Dans cette question #1# est une liste prenant les valeurs "augmenté" ou "diminué"
      questionTaux2: ['En avril, il est ainsi tombé ainsi $£f$ mm de pluie puis calcule[£f-£c] mm en mai.', // La variable £f sera la réponse à questionVA
        'D’avril à mai, la quantité de pluie tombée a donc #1# £e &1& %.'], // si questionVA est posée, on demandera le taux d’évolution de 2015 à 2016
      questionVD: ['Sachant qu’entre février et mars, la quantité de pluie avait diminué de $£d$ %, en février 2018 il est tombé £e: &1& mm.'],
      arrondisTxt: ['Arrondir à l’unité.', 'Arrondir à 0,1&nbsp;%.', 'Arrondir à 0,1&nbsp;mm.'], // le premier arrondi est pour questionVA, le 2ème pour le taux et le troisième pour question VD
      arrondis: [1, 0.1, 0.1],
      valeurs: ['£a', '-£b', '-£d'], // la 1ère valeur est la valeur de départ pour q1 et la valeur finale pour q3, la 2ème, est le taux d’évolution dans q1 et la troisième le taux d’évolution dans q3
      // réponses associées
      reponses: { va: '£a*(1-£b/100)', tx1: '-£c/£a', tx2: '-£c/£f', vd: '£a/(1-£d/100)' },
      // Correction associée à la proba à calculer
      correctionVA: ['Or $£a\\times (1-\\frac{£b}{100})£s£{r3}$.',
        'Donc en avril 2018, il est tombé £e$£r$ mm de pluie.'],
      correctionTaux1: ['Or $\\frac{calcule[£a-£c]-£a}{£a}£s£t\\text{ soit £{e1}}£r$&nbsp;%.',
        'Donc la quantité de pluie tombée a diminué £e $calcule[abs(£r)]$&nbsp;% de mars à avril 2018.'],
      correctionTaux2: ['Or $\\frac{calcule[£f-£c]-calcule[£f]}{calcule[£f]}£s£t\\text{ soit £{e1}}£r$&nbsp;%.',
        'Donc la quantité de pluie tombée a diminué £e $calcule[abs(£r)]$ % d’avril à mai 2018.'],
      correctionVD: ['Or $\\frac{£a}{1-\\frac{£d}{100}}£s£{r3}$.',
        'Donc en février 2018, il est tombé £e$£r$&nbsp;mm de pluie.']
    }
  ]
}
