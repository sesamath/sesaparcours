export default {
  /* Dans toutes les consignes de ce type d’exercices, £a permet de déclarer la variable a. Il faut alors préciser l’intervalle dans lequel se trouve cette valeur
    Les consignes seront le plus souvent sur plusieurs lignes, d’où l’idée de créer un tableau
    la propriété variables de chaque énoncé est un tableau où chaque élément est un tableau de deux ou trois éléments
    Par exemple ['a','[5;15]'] signifie que a sera un entier de l’intervalle [5;15]
    ['a','[5;15]',100] signifie que a prend une valeur décimale de [0,05;0,15] avec 2 chiffres après la virgule
    */
  sujets: [
    {
      nomSujet: 'Centres étrangers juin 2015 S',
      consignesInit: ['Un fournisseur produit deux sortes de cadenas. Les uns sont premier prix, et les autres sont haut de gamme. Un magasin de bricolage dispose d’un stock de cadenas provenant de ce fournisseur.',
        'On admet que, dans le magasin :'],
      // consignesInit est l’énoncé commun
      consignesDonnees: [['calcule[10*£a]% des cadenas proposés à la vente sont premier prix, les autres haut de gamme ;',
        '£b% des cadenas haut de gamme sont défectueux ;',
        '$calcule[((10-£a)*£b+£a*£c)/10]$% des cadenas sont défectueux.'],
      ['calcule[100-10*£a]% des cadenas proposés à la vente sont haut de gamme, les autres premier prix ;',
        '£c% des cadenas premier prix sont défectueux ;',
        '$calcule[((10-£a)*£b+£a*£c)/10]$% des cadenas sont défectueux.'],
      ['calcule[10*£a]% des cadenas proposés à la vente sont premier prix, les autres haut de gamme ;',
        'calcule[100-£b]% des cadenas haut de gamme sont en bon état ;',
        '$calcule[100-((10-£a)*£b+£a*£c)/10]$% des cadenas sont en bon état.'],
      ['calcule[100-10*£a]% des cadenas proposés à la vente sont haut de gamme, les autres premier prix ;',
        'calcule[100-£c]% des cadenas premier prix sont en bon état ;',
        '$calcule[100-((10-£a)*£b+£a*£c)/10]$% des cadenas sont en bon état.']
      ],
      // consignesDonnees contient les différentes variantes de ce qui peut être donné dans l’énoncé
      consignesFin: ['On prélève au hasard un cadenas dans le magasin. On note :',
        '- $H$ l’évènement : «le cadenas prélevé est haut de gamme»;',
        '- $D$ l’évènement : «le cadenas prélevé est défectueux».'],
      // consignesFin est la fin de l’énoncé commun
      variables: [['a', '[6;9]'], ['b', '[2;5]'], ['c', '[8;11]']],
      // la longueur de evtsProba correspond au nombre de premières branches de l’arbre
      // evstProba[i][2] est un tableau de couples evt/proba correspondant aux secondes branches pour chaque premier evt
      evtsProba: [[['H', '1-£a/10', [['D', '£b/100'], ['\\overline{D}', '1-£b/100']]],
        ['\\overline{H}', '£a/10', [['D', ''], ['\\overline{D}', '']]]],
      [['H', '1-£a/10', [['D', ''], ['\\overline{D}', '']]],
        ['\\overline{H}', '£a/10', [['D', '£c/100'], ['\\overline{D}', '1-£c/100']]]],
      [['H', '1-£a/10', [['D', '£b/100'], ['\\overline{D}', '1-£b/100']]],
        ['\\overline{H}', '£a/10', [['D', ''], ['\\overline{D}', '']]]],
      [['H', '1-£a/10', [['D', ''], ['\\overline{D}', '']]],
        ['\\overline{H}', '£a/10', [['D', '£c/100'], ['\\overline{D}', '1-£c/100']]]]
      ],
      prbEnonce: [['£a/10', '£b/100', '((10-£a)*£b+£a*£c)/1000'], ['(100-10*£a)/100', '£c/100', '((10-£a)*£b+£a*£c)/1000'],
        ['£a/10', '(100-£b)/100', '1-((10-£a)*£b+£a*£c)/1000'], ['1-£a/10', '(100-£c)/100', '1-((10-£a)*£b+£a*£c)/1000']
      ],
      // correctionProba est la phrase qui traduit les proba de l’énoncé
      // les proba de l’énoncé doivent nécessairement être repérées par £a, £b, ... et cela correspond aux éléments de prbEnonce dans l’ordre
      correctionProba: ['D’après l’énoncé, $P(\\overline{H})=£a$, $P_{H}(D)=£b$ et $P(D)=£c$, ce qui nous donne l’arbre incomplet ci-contre :',
        'D’après l’énoncé, $P(H)=£a$, $P_{\\overline{H}}(D)=£b$ et $P(D)=£c$, ce qui nous donne l’arbre incomplet ci-contre :',
        'D’après l’énoncé, $P(\\overline{H})=£a$, $P_{H}(\\overline{D})=£b$ et $P(\\overline{D})=£c$, ce qui nous donne l’arbre incomplet ci-contre :',
        'D’après l’énoncé, $P(H)=£a$, $P_{\\overline{H}}(\\overline{D})=£b$ et $P(\\overline{D})=£c$, ce qui nous donne l’arbre incomplet ci-contre :'
      ],
      // Ici on demande la proba d’une intersection calculable avec les données précédentes
      questionInterFacile1: ['La probabilité que le cadenas prélevé soit haut de gamme et défectueux vaut &1&.',
        'La probabilité que le cadenas prélevé soit premier prix et défectueux vaut &1&.',
        'La probabilité que le cadenas prélevé soit haut de gamme et en bon état vaut &1&.',
        'La probabilité que le cadenas prélevé soit premier prix et en bon état vaut &1&.'],
      questionInterFacile: ['Quelle est la probabilité que le cadenas prélevé soit haut de gamme et défectueux?',
        'Quelle est la probabilité que le cadenas prélevé soit premier prix et défectueux?',
        'Quelle est la probabilité que le cadenas prélevé soit haut de gamme et en bon état?',
        'Quelle est la probabilité que le cadenas prélevé soit premier prix et en bon état?'],
      questionInterFacileFin: ['P(H\\cap D)', 'P(\\overline{H}\\cap D)', 'P(H\\cap \\overline{D})', 'P(\\overline{H}\\cap \\overline{D})'],
      probaInterFacile: ['(10-£a)*£b/1000', '£a*£c/1000', '(10-£a)*(100-£b)/1000', '£a*(100-£c)/1000'],
      // Correction associée à la proba à calculer
      correctionInterFacile: ['$P(H\\cap D)=P(H)\\times P_{H}(D)=calcule[(10-£a)/10]\\times calcule[£b/100]=calcule[(10-£a)*£b/1000]$',
        '$P(\\overline{H}\\cap D)=P(\\overline{H})\\times P_{\\overline{H}}(D)=calcule[£a/10]\\times calcule[£c/100]=calcule[£a*£c/1000]$',
        '$P(H\\cap \\overline{D})=P(H)\\times P_{H}(\\overline{D})=calcule[(10-£a)/10]\\times calcule[(100-£b)/100]=calcule[(10-£a)*(100-£b)/1000]$',
        '$P(\\overline{H}\\cap \\overline{D})=P(\\overline{H})\\times P_{\\overline{H}}(\\overline{D})=calcule[£a/10]\\times calcule[(100-£c)/100]=calcule[£a*(100-£c)/1000]$'],
      // Pour questionInter1, l’intersection n’est pas donnée
      questionInter1: ['La probabilité que le cadenas prélevé soit premier prix et défectueux vaut &1&.',
        'La probabilité que le cadenas prélevé soit haut de gamme et défectueux vaut &1&.',
        'La probabilité que le cadenas prélevé soit premier prix et en bon état vaut &1&.',
        'La probabilité que le cadenas prélevé soit haut de gamme et en bon état vaut &1&.'],
      questionInter: ['Quelle est la probabilité que le cadenas prélevé soit premier prix et défectueux?',
        'Quelle est la probabilité que le cadenas prélevé soit haut de gamme et défectueux?',
        'Quelle est la probabilité que le cadenas prélevé soit premier prix et en bon état?',
        'Quelle est la probabilité que le cadenas prélevé soit haut de gamme et en bon état?'],
      questionInterFin: ['P(\\overline{H}\\cap D)', 'P(H\\cap D)', 'P(\\overline{H}\\cap \\overline{D})', 'P(H\\cap \\overline{D})'],
      probaInter: ['£a*£c/1000', '£b*(10-£a)/1000', '£a*(100-£c)/1000', '(10-£a)*(100-£b)/1000'],
      // Correction associée à la proba à calculer
      correctionInter: [['D’après la formule des probabilités totales, on a $P(D)=P(H\\cap D)+P(\\overline{H}\\cap D)$, ce qui donne $calcule[(£a*£c+(10-£a)*£b)/1000]=P(H)\\times P_{H}(D)+P(\\overline{H}\\cap D)$.',
        'Ainsi $P(\\overline{H}\\cap D)=calcule[(£a*£c+(10-£a)*£b)/1000]-calcule[1-£a/10]\\times calcule[£b/100]=calcule[£a*£c/1000]$.'],
      ['D’après la formule des probabilités totales, on a $P(D)=P(H\\cap D)+P(\\overline{H}\\cap D)$, ce qui donne $calcule[(£a*£c+(10-£a)*£b)/1000]=P(H\\cap D)+P(\\overline{H})\\times P_{\\overline{H}}(D)$.',
        'Ainsi $P(H\\cap D)=calcule[(£a*£c+(10-£a)*£b)/1000]-calcule[£a/10]\\times calcule[£c/100]=calcule[(10-£a)*£b/1000]$.'],
      ['D’après la formule des probabilités totales, on a $P(\\overline{D})=P(H\\cap \\overline{D})+P(\\overline{H}\\cap \\overline{D})$, ce qui donne $calcule[1-(£a*£c+(10-£a)*£b)/1000]=P(H)\\times P_{H}(\\overline{D})+P(\\overline{H}\\cap \\overline{D})$.',
        'Ainsi $P(\\overline{H}\\cap \\overline{D})=calcule[1-(£a*£c+(10-£a)*£b)/1000]-calcule[(10-£a)/10]\\times calcule[(100-£b)/100]=calcule[£a*(100-£c)/1000]$.'],
      ['D’après la formule des probabilités totales, on a $P(\\overline{D})=P(H\\cap \\overline{D})+P(\\overline{H}\\cap \\overline{D})$, ce qui donne $calcule[1-(£a*£c+(10-£a)*£b)/1000]=P(H\\cap \\overline{D})+P(\\overline{H})\\times P_{\\overline{H}}(\\overline{D})$.',
        'Ainsi $P(H\\cap \\overline{D})=calcule[1-(£a*£c+(10-£a)*£b)/1000]-calcule[£a/10]\\times calcule[1-£c/100]=calcule[(10-£a)*(100-£b)/1000]$.']],
      // Correction similaire mais dans le cas où la première question à été posée
      correctionInterBis: [['D’après la formule des probabilités totales, on a $P(D)=P(H\\cap D)+P(\\overline{H}\\cap D)$, ce qui donne $calcule[(£a*£c+(10-£a)*£b)/1000]=calcule[(10-£a)*£b/1000]+P(\\overline{H}\\cap D)$.',
        'Ainsi $P(\\overline{H}\\cap D)=calcule[(£a*£c+(10-£a)*£b)/1000]-calcule[(10-£a)*£b/1000]=calcule[£a*£c/1000]$.'],
      ['D’après la formule des probabilités totales, on a $P(D)=P(H\\cap D)+P(\\overline{H}\\cap D)$, ce qui donne $calcule[(£a*£c+(10-£a)*£b)/1000]=P(H\\cap D)+calcule[£a*£c/1000]$.',
        'Ainsi $P(H\\cap D)=calcule[(£a*£c+(10-£a)*£b)/1000]-calcule[£a*£c/1000]=calcule[(10-£a)*£b/1000]$.'],
      ['D’après la formule des probabilités totales, on a $P(\\overline{D})=P(H\\cap \\overline{D})+P(\\overline{H}\\cap \\overline{D})$, ce qui donne $calcule[1-(£a*£c+(10-£a)*£b)/1000]=calcule[(10-£a)*(100-£b)/1000]+P(\\overline{H}\\cap \\overline{D})$.',
        'Ainsi $P(\\overline{H}\\cap \\overline{D})=calcule[1-(£a*£c+(10-£a)*£b)/1000]-calcule[(10-£a)*(100-£b)/1000]=calcule[£a*(100-£c)/1000]$.'],
      ['D’après la formule des probabilités totales, on a $P(\\overline{D})=P(H\\cap \\overline{D})+P(\\overline{H}\\cap \\overline{D})$, ce qui donne $calcule[1-(£a*£c+(10-£a)*£b)/1000]=P(H\\cap \\overline{D})+calcule[£a*(100-£c)/1000]$.',
        'Ainsi $P(H\\cap \\overline{D})=calcule[1-(£a*£c+(10-£a)*£b)/1000]-calcule[£a*(100-£c)/1000]=calcule[(10-£a)*(100-£b)/1000]$.']],
      // probabilité conditionnelles qui manque pour créer l’arbre (elle utilise la proba inter précédente)
      // probabilité conditionnelle en utilisant la proba précédemment calculée
      questionPrbCond1: ['Le cadenas prélevé est premier prix, la probabilité qu’il soit défectueux vaut &1&.',
        'Le cadenas prélevé est haut de gamme, la probabilité qu’il soit défectueux  vaut &1&.',
        'Le cadenas prélevé est premier prix, la probabilité qu’il soit en bon état vaut &1&.',
        'Le cadenas prélevé est haut de gamme, la probabilité qu’il soit en bon état vaut &1&.'],
      questionPrbSachant1: ['Sachant que le cadenas prélevé est premier prix, la probabilité qu’il soit défectueux vaut &1&.',
        'Sachant que le cadenas prélevé est haut de gamme, la probabilité qu’il soit défectueux vaut &1&.',
        'Sachant que le cadenas prélevé est premier prix, la probabilité qu’il soit en bon état vaut &1&.',
        'Sachant que le cadenas prélevé est haut de gamme, la probabilité qu’il soit en bon état vaut &1&.'],
      questionPrbCond: ['Le cadenas prélevé est premier prix. Quelle est la probabilité qu’il soit défectueux?',
        'Le cadenas prélevé est haut de gamme. Quelle est la probabilité qu’il soit défectueux?',
        'Le cadenas prélevé est premier prix. Quelle est la probabilité qu’il soit en bon état?',
        'Le cadenas prélevé est haut de gamme. Quelle est la probabilité qu’il soit en bon état?'],
      questionPrbSachant: ['Sachant que le cadenas prélevé est premier prix, quelle est la probabilité qu’il soit défectueux?',
        'Sachant que le cadenas prélevé est haut de gamme, quelle est la probabilité qu’il soit défectueux?',
        'Sachant que le cadenas prélevé est premier prix, quelle est la probabilité qu’il soit en bon état?',
        'Sachant que le cadenas prélevé est haut de gamme, quelle est la probabilité qu’il soit en bon état?'],
      questionPrbSachantFin: ['$P_{\\overline{H}}(D)$', '$P_{H}(D)$', '$P_{\\overline{H}}(\\overline{D})$', '$P_{H}(\\overline{D})$'],
      probaCond: ['£c/100', '£b/100', '1-£c/100', '1-£b/100'],
      // correction de la probabilité conditionnelle
      correctionCond: ['$P_{\\overline{H}}(D)=\\frac{P(\\overline{H}\\cap D)}{P(\\overline{H})}=\\frac{calcule[£a*£c/1000]}{calcule[£a/10]}=£r$',
        '$P_{H}(D)=\\frac{P(H\\cap D)}{P(H)}=\\frac{calcule[(10-£a)*£b/1000]}{calcule[(10-£a)/10]}=£r$',
        '$P_{\\overline{H}}(\\overline{D})=\\frac{P(\\overline{H}\\cap \\overline{D})}{P(\\overline{H})}=\\frac{calcule[£a*(100-£c)/1000]}{calcule[£a/10]}=£r$',
        '$P_{H}(\\overline{D})=\\frac{P(H\\cap \\overline{D})}{P(H)}=\\frac{calcule[(10-£a)*(100-£b)/1000]}{calcule[(10-£a)/10]}=£r$']

    },
    {
      nomSujet: 'Métropole juin 2016 ES',
      consignesInit: ['Un téléphone portable contient en mémoire calcule[100*£d] chansons archivées par catégories : rock, techno, rap, reggae ... dont certaines sont interprétées en français.',
        'Parmi toutes les chansons enregistrées, calcule[100*£d*£a] sont classées dans la catégorie rock.'],
      // consignesInit est l’énoncé commun
      consignesDonnees: [['calcule[50*£b]% des chansons de la catégorie rock sont interprétées en français.',
        'Parmi toutes les chansons enregistrées $calcule[50*(£a*£b+(1-£a)*£c)]$% sont interprétées en français.'],
      ['$calcule[100-50*£b]$% des chansons de la catégorie rock ne sont pas interprétées en français.',
        'Parmi toutes les chansons enregistrées $calcule[100-50*(£a*£b+(1-£a)*£c)]$% ne sont pas interprétées en français.'],
      ['$calcule[50*£c]$% des chansons qui ne font pas partie de la catégorie rock sont interprétées en français.',
        'Parmi toutes les chansons enregistrées $calcule[50*(£a*£b+(1-£a)*£c)]$% sont interprétées en français.'],
      ['$calcule[100-50*£c]$% des chansons qui ne font pas partie de la catégorie rock ne sont pas interprétées en français.',
        'Parmi toutes les chansons enregistrées $calcule[100-50*(£a*£b+(1-£a)*£c)]$% ne sont pas interprétées en français.']
      ],
      // consignesDonnees contient les différentes variantes de ce qui peut être donné dans l’énoncé
      consignesFin: ['Une des fonctionnalités du téléphone permet d’écouter de la musique en mode «lecture aléatoire» : les chansons écoutées sont choisies au hasard et de façon équiprobable parmi l’ensemble du répertoire.',
        'Au cours de son footing hebdomadaire, le propriétaire du téléphone écoute une chanson grâce à ce mode de lecture. On note :',
        '- $R$ l’évènement : «la chanson écoutée est une chanson de la catégorie rock»;',
        '- $F$ l’évènement : «la chanson écoutée est interprétée en français».'],
      // consignesFin est la fin de l’énoncé commun
      variables: [['a', '[3;4]', '10'], ['b', '[4;9]', '10'], ['c', '[11;15]', '10'], ['d', '[31;38]']],
      // la longueur de evtsProba correspond au nombre de premières branches de l’arbre
      // evstProba[i][2] est un tableau de couples evt/proba correspondant aux secondes branches pour chaque premier evt
      evtsProba: [[['R', '£a', [['F', '0.5*£b'], ['\\overline{F}', '1-0.5*£b']]],
        ['\\overline{R}', '1-£a', [['F', ''], ['\\overline{F}', '']]]],
      [['R', '£a', [['F', '0.5*£b'], ['\\overline{F}', '1-0.5*£b']]],
        ['\\overline{R}', '1-£a', [['F', ''], ['\\overline{F}', '']]]],
      [['R', '£a', [['F', ''], ['\\overline{F}', '']]],
        ['\\overline{R}', '1-£a', [['F', '0.5*£c'], ['\\overline{F}', '1-0.5*£c']]]],
      [['R', '£a', [['F', ''], ['\\overline{F}', '']]],
        ['\\overline{R}', '1-£a', [['F', '0.5*£c'], ['\\overline{F}', '1-0.5*£c']]]]
      ],
      prbEnonce: [['£a', '0.5*£b', '0.5*(£a*£b+(1-£a)*£c)', '100*£d*£a', '100*£d'], ['£a', '1-0.5*£b', '1-0.5*(£a*£b+(1-£a)*£c)', '100*£d*£a', '100*£d'],
        ['£a', '0.5*£c', '0.5*(£a*£b+(1-£a)*£c)', '100*£d*£a', '100*£d'], ['£a', '1-0.5*£c', '1-0.5*(£a*£b+(1-£a)*£c)', '100*£d*£a', '100*£d']
      ],
      // correctionProba est la phrase qui traduit les proba de l’énoncé
      // les proba de l’énoncé doivent nécessairement être repérées par £a, £b, ... et cela correspond aux éléments de prbEnonce dans l’ordre
      correctionProba: ['D’après l’énoncé, $P(R)=\\frac{£d}{£e}=£a$, $P_{R}(F)=£b$ et $P(F)=£c$, ce qui nous donne l’arbre incomplet ci-contre :',
        'D’après l’énoncé, $P(R)=\\frac{£d}{£e}=£a$, $P_{R}(\\overline{F})=£b$ et $P(\\overline{F})=£c$, ce qui nous donne l’arbre incomplet ci-contre :',
        'D’après l’énoncé, $P(R)=\\frac{£d}{£e}=£a$, $P_{\\overline{R}}(F)=£b$ et $P(F)=£c$, ce qui nous donne l’arbre incomplet ci-contre :',
        'D’après l’énoncé, $P(R)=\\frac{£d}{£e}=£a$, $P_{\\overline{R}}(\\overline{F})=£b$ et $P(\\overline{F})=£c$, ce qui nous donne l’arbre incomplet ci-contre :'
      ],
      // Ici on demande la proba d’une intersection calculable avec les données précédentes
      questionInterFacile1: ['La probabilité que la chanson écoutée soit une chanson de la catégorie rock et qu’elle soit interprétée en français vaut &1&.',
        'La probabilité que la chanson écoutée soit une chanson de la catégorie rock et qu’elle ne soit pas interprétée en français vaut &1&.',
        'La probabilité que la chanson écoutée ne soit pas une chanson de la catégorie rock et qu’elle soit interprétée en français vaut &1&.',
        'La probabilité que la chanson écoutée ne soit pas une chanson de la catégorie rock et qu’elle ne soit pas interprétée en français vaut &1&.'],
      questionInterFacile: ['Quelle est la probabilité que la chanson écoutée soit une chanson de la catégorie rock et qu’elle soit interprétée en français?',
        'Quelle est la probabilité que la chanson écoutée soit une chanson de la catégorie rock et qu’elle ne soit pas interprétée en français?',
        'Quelle est la probabilité que la chanson écoutée ne soit pas une chanson de la catégorie rock et qu’elle soit interprétée en français?',
        'Quelle est la probabilité que la chanson écoutée ne soit pas une chanson de la catégorie rock et qu’elle ne soit pas interprétée en français?'],
      questionInterFacileFin: ['P(R\\cap F)', 'P(R\\cap \\overline{F})', 'P(\\overline{R}\\cap F)', 'P(\\overline{R}\\cap \\overline{F})'],
      probaInterFacile: ['£a*0.5*£b', '£a*(1-0.5*£b)', '(1-£a)*0.5*£c', '(1-£a)*(1-0.5*£c)'],
      // Correction associée à la proba à calculer
      correctionInterFacile: ['$P(R\\cap F)=P(R)\\times P_{R}(F)=calcule[£a]\\times calcule[0.5*£b]=calcule[£a*0.5*£b]$',
        '$P(R\\cap \\overline{F})=P(R)\\times P_{R}(\\overline{F})=calcule[£a]\\times calcule[1-0.5*£b]=calcule[£a*(1-0.5*£b)]$',
        '$P(\\overline{R}\\cap F)=P(\\overline{R})\\times P_{\\overline{R}}(F)=calcule[1-£a]\\times calcule[0.5*£c]=calcule[(1-£a)*0.5*£c]$',
        '$P(\\overline{R}\\cap \\overline{F})=P(\\overline{R})\\times P_{\\overline{R}}(\\overline{F})=calcule[1-£a]\\times calcule[1-0.5*£c]=calcule[(1-£a)*(1-0.5*£c)]$'],
      // Pour questionInter1, l’intersection n’est pas donnée
      questionInter1: ['La probabilité que la chanson écoutée ne soit pas une chanson de la catégorie rock et qu’elle soit interprétée en français vaut &1&.',
        'La probabilité que la chanson écoutée ne soit pas une chanson de la catégorie rock et qu’elle ne soit pas interprétée en français vaut &1&.',
        'La probabilité que la chanson écoutée soit une chanson de la catégorie rock et qu’elle soit interprétée en français vaut &1&.',
        'La probabilité que la chanson écoutée soit une chanson de la catégorie rock et qu’elle ne soit pas interprétée en français vaut &1&.'],
      questionInter: ['Quelle est la probabilité que la chanson écoutée ne soit pas une chanson de la catégorie rock et qu’elle soit interprétée en français?',
        'Quelle est la probabilité que la chanson écoutée ne soit pas une chanson de la catégorie rock et qu’elle ne soit pas interprétée en français?',
        'Quelle est la probabilité que la chanson écoutée soit une chanson de la catégorie rock et qu’elle soit interprétée en français?',
        'Quelle est la probabilité que la chanson écoutée soit une chanson de la catégorie rock et qu’elle ne soit pas interprétée en français?'],
      questionInterFin: ['P(\\overline{R}\\cap F)', 'P(\\overline{R}\\cap \\overline{F})', 'P(R\\cap F)', 'P(R\\cap \\overline{F})'],
      probaInter: ['(1-£a)*0.5*£c', '(1-£a)*(1-0.5*£c)', '£a*0.5*£b', '£a*(1-0.5*£b)'],
      // Correction associée à la proba à calculer
      correctionInter: [['D’après la formule des probabilités totales, on a $P(F)=P(R\\cap F)+P(\\overline{R}\\cap F)$, ce qui donne $calcule[£a*0.5*£b+(1-£a)*0.5*£c]=P(R)\\times P_{R}(F)+P(\\overline{R}\\cap F)$.',
        '$P(\\overline{R}\\cap F)=calcule[£a*0.5*£b+(1-£a)*0.5*£c]-calcule[£a]\\times calcule[0.5*£b]=calcule[(1-£a)*0.5*£c]$.'],
      ['D’après la formule des probabilités totales, on a $P(\\overline{F})=P(R\\cap \\overline{F})+P(\\overline{R}\\cap \\overline{F})$, ce qui donne $calcule[£a*(1-0.5*£b)+(1-£a)*(1-0.5*£c)]=P(R)\\times P_{R}(\\overline{F})+P(\\overline{R}\\cap \\overline{F})$.',
        '$P(\\overline{R}\\cap \\overline{F})=calcule[£a*(1-0.5*£b)+(1-£a)*(1-0.5*£c)]-calcule[£a]\\times calcule[1-0.5*£b]=calcule[(1-£a)*(1-0.5*£c)]$.'],
      ['D’après la formule des probabilités totales, on a $P(F)=P(R\\cap F)+P(\\overline{R}\\cap F)$, ce qui donne $calcule[£a*0.5*£b+(1-£a)*0.5*£c0]=P(R\\cap F)+P(\\overline{R})\\times P_{\\overline{R}}(F)$.',
        '$P(R\\cap F)=calcule[£a*0.5*£b+(1-£a)*0.5*£c]-calcule[1-£a]\\times calcule[0.5*£c]=calcule[£a*0.5*£b]$.'],
      ['D’après la formule des probabilités totales, on a $P(\\overline{F})=P(R\\cap \\overline{F})+P(\\overline{R}\\cap \\overline{F})$, ce qui donne $calcule[£a*(1-0.5*£b)+(1-£a)*(1-0.5*£c)]=P(R\\cap \\overline{F})+P(\\overline{R})\\times P_{\\overline{R}}(\\overline{F})$.',
        '$P(R\\cap \\overline{F})=calcule[£a*0.5*£b+(1-£a)*(1-0.5*£c)]-calcule[1-£a]\\times calcule[1-0.5*£c]=calcule[£a*(1-0.5*£b)]$.']],
      // Correction similaire mais dans le cas où la première question à été posée
      correctionInterBis: [['D’après la formule des probabilités totales, on a $P(F)=P(R\\cap F)+P(\\overline{R}\\cap F)$, ce qui donne $calcule[£a*0.5*£b+(1-£a)*0.5*£c]=calcule[£a*0.5*£b]+P(\\overline{R}\\cap F)$.',
        '$P(\\overline{R}\\cap F)=calcule[£a*0.5*£b+(1-£a)*0.5*£c]-calcule[£a*0.5*£b]=calcule[(1-£a)*0.5*£c]$.'],
      ['D’après la formule des probabilités totales, on a $P(\\overline{F})=P(R\\cap \\overline{F})+P(\\overline{R}\\cap \\overline{F})$, ce qui donne $calcule[£a*(1-0.5*£b)+(1-£a)*(1-0.5*£c)]=calcule[£a*(1-0.5*£b)]+P(\\overline{R}\\cap \\overline{F})$.',
        '$P(\\overline{R}\\cap \\overline{F})=calcule[£a*(1-0.5*£b)+(1-£a)*(1-0.5*£c)]-calcule[£a*(1-0.5*£b)]=calcule[(1-£a)*(1-0.5*£c)]$.'],
      ['D’après la formule des probabilités totales, on a $P(F)=P(R\\cap F)+P(\\overline{R}\\cap F)$, ce qui donne $calcule[£a*0.5*£b+(1-£a)*0.5*£c]=P(R\\cap F)+calcule[(1-£a)*0.5*£c]$.',
        '$P(R\\cap F)=calcule[£a*0.5*£b+(1-£a)*0.5*£c]-calcule[(1-£a)*0.5*£c]=calcule[£a*0.5*£b]$.'],
      ['D’après la formule des probabilités totales, on a $P(\\overline{F})=P(R\\cap \\overline{F})+P(\\overline{R}\\cap \\overline{F})$, ce qui donne $calcule[£a*(1-0.5*£b)+(1-£a)*(1-0.5*£c)]=P(R\\cap \\overline{F})+calcule[(1-£a)*(1-0.5*£c)]$.',
        '$P(R\\cap \\overline{F})=calcule[£a*(1-0.5*£b)+(1-£a)*(1-0.5*£c)]-calcule[(1-£a)*(1-0.5*£c)]=calcule[£a*(1-0.5*£b)]$.']],
      // probabilité conditionnelles qui manque pour créer l’arbre (elle utilise la proba inter précédente)
      // probabilité conditionnelle en utilisant la proba précédemment calculée
      questionPrbCond1: ['La chanson écoutée n’est pas une chanson de la catégorie rock, la probabilité qu’elle soit interprétée en français vaut &1&.',
        'La chanson écoutée n’est pas une chanson de la catégorie rock, la probabilité qu’elle ne soit pas interprétée en français vaut &1&.',
        'La chanson écoutée est une chanson de la catégorie rock, la probabilité qu’elle soit interprétée en français vaut &1&.',
        'La chanson écoutée est une chanson de la catégorie rock, la probabilité qu’elle ne soit pas interprétée en français vaut &1&.'],
      questionPrbSachant1: ['Sachant que la chanson écoutée n’est pas une chanson de la catégorie rock, la probabilité qu’elle soit interprétée en français vaut &1&.',
        'Sachant que la chanson écoutée n’est pas une chanson de la catégorie rock, la probabilité qu’elle ne soit pas interprétée en français vaut &1&.',
        'Sachant que la chanson écoutée est une chanson de la catégorie rock, la probabilité qu’elle soit interprétée en français vaut &1&.',
        'Sachant que la chanson écoutée est une chanson de la catégorie rock, la probabilité qu’elle ne soit pas interprétée en français vaut &1&.'],
      questionPrbCond: ['La chanson écoutée n’est pas une chanson de la catégorie rock. Quelle est la probabilité qu’elle soit interprétée en français?',
        'La chanson écoutée n’est pas une chanson de la catégorie rock. Quelle est la probabilité qu’elle ne soit pas interprétée en français?',
        'La chanson écoutée est une chanson de la catégorie rock. Quelle est la probabilité qu’elle soit interprétée en français?',
        'La chanson écoutée est une chanson de la catégorie rock. Quelle est la probabilité qu’elle ne soit pas interprétée en français?'],
      questionPrbSachant: ['Sachant que la chanson écoutée n’est pas une chanson de la catégorie rock, quelle est la probabilité qu’elle soit interprétée en français?',
        'Sachant que la chanson écoutée n’est pas une chanson de la catégorie rock, quelle est la probabilité qu’elle ne soit pas interprétée en français?',
        'Sachant que la chanson écoutée est une chanson de la catégorie rock, quelle est la probabilité qu’elle soit interprétée en français?',
        'Sachant que la chanson écoutée est une chanson de la catégorie rock, quelle est la probabilité qu’elle ne soit pas interprétée en français?'],
      questionPrbSachantFin: ['$P_{\\overline{R}}(F)$', '$P_{\\overline{R}}(\\overline{F})$', '$P_{R}(F)$', '$P_{R}(\\overline{F})$'],
      probaCond: ['0.5*£c', '1-0.5*£c', '0.5*£b', '1-0.5*£b'],
      // correction de la probabilité conditionnelle
      correctionCond: ['$P_{\\overline{R}}(F)=\\frac{P(\\overline{R}\\cap F)}{P(\\overline{R})}=\\frac{calcule[(1-£a)*0.5*£c]}{calcule[1-£a]}=£r$',
        '$P_{\\overline{R}}(\\overline{F})=\\frac{P(\\overline{R}\\cap \\overline{F})}{P(\\overline{R})}=\\frac{calcule[(1-£a)*(1-0.5*£c)]}{calcule[1-£a]}=£r$',
        '$P_{R}(F)=\\frac{P(R\\cap F)}{P(R)}=\\frac{calcule[£a*0.5*£b]}{calcule[£a]}=£r$',
        '$P_{R}(\\overline{F})=\\frac{P(R\\cap \\overline{F})}{P(R)}=\\frac{calcule[£a*(1-0.5*£b)]}{calcule[£a]}=£r$']
    },

    {
      nomSujet: 'Pondichery avril 2016 ES',
      consignesInit: ['On dispose des renseignements suivants à propos du baccalauréat d’une certaine session:'],
      // consignesInit est l’énoncé commun
      consignesDonnees: [['- calcule[2*£a]% des inscrits ont passé un baccalauréat général, calcule[5*£b]% un baccalauréat technologique et les autres un baccalauréat professionnel;',
        '- calcule[5*£c]% des candidats au baccalauréat général ont été reçus ainsi que calcule[2*£d]% des candidats au baccalauréat technologique.',
        'Le ministère de l’Éducation Nationale a annoncé un taux global de réussite pour cette session de calcule[(2*£a*5*£c+5*£b*2*£d+(100-2*£a-5*£b)*£e)/100]% pour l’ensemble des candidats présentant l’un des baccalauréats.'],
      ['- calcule[2*£a]% des inscrits ont passé un baccalauréat général, calcule[100-2*£a-5*£b]% un baccalauréat professionnel et les autres un baccalauréat technologique;',
        '- calcule[5*£c]% des candidats au baccalauréat général ont été reçus ainsi que £e% des candidats au baccalauréat professionnel.',
        'Le ministère de l’Éducation Nationale a annoncé un taux global de réussite pour cette session de calcule[(2*£a*5*£c+5*£b*2*£d+(100-2*£a-5*£b)*£e)/100]% pour l’ensemble des candidats présentant l’un des baccalauréats.'],
      ['- calcule[5*£b]% des inscrits ont passé un baccalauréat technologique, calcule[100-2*£a-5*£b]% un baccalauréat professionnel et les autres un baccalauréat général;',
        '- calcule[2*£d]% des candidats au baccalauréat technologique ont été reçus ainsi que £e% des candidats au baccalauréat professionnel.',
        'Le ministère de l’Éducation Nationale a annoncé un taux global de réussite pour cette session de calcule[(2*£a*5*£c+5*£b*2*£d+(100-2*£a-5*£b)*£e)/100]% pour l’ensemble des candidats présentant l’un des baccalauréats.']
      ],
      // consignesDonnees contient les différentes variantes de ce qui peut être donné dans l’énoncé
      consignesFin: ['On choisit au hasard un candidat au baccalauréat de cette session et on considère les évènements suivants:',
        '- $G$:«Le candidat s’est présenté au baccalauréat général»;',
        '- $T$:«Le candidat s’est présenté au baccalauréat technologique»;',
        '- $S$:«Le candidat s’est présenté au baccalauréat professionnel»;',
        '- $R$:«Le candidat a été reçu».'],
      // consignesFin est la fin de l’énoncé commun
      variables: [['a', '[21;24]'], ['b', '[3;5]'], ['c', '[175;185]', '10'], ['d', '[410;450]', '10'], ['e', '[78;83]']],
      // la longueur de evtsProba correspond au nombre de premières branches de l’arbre
      // evstProba[i][2] est un tableau de couples evt/proba correspondant aux secondes branches pour chaque premier evt
      evtsProba: [[['G', '2*£a/100', [['R', '5*£c/100'], ['\\overline{R}', '1-5*£c/100']]],
        ['T', '5*£b/100', [['R', '2*£d/100'], ['\\overline{R}', '1-2*£d/100']]],
        ['S', '1-(2*£a+5*£b)/100', [['R', ''], ['\\overline{R}', '']]]],
      [['G', '2*£a/100', [['R', '5*£c/100'], ['\\overline{R}', '1-5*£c/100']]],
        ['T', '5*£b/100', [['R', ''], ['\\overline{R}', '']]],
        ['S', '1-(2*£a+5*£b)/100', [['R', '£e/100'], ['\\overline{R}', '1-£e/100']]]],
      [['G', '2*£a/100', [['R', ''], ['\\overline{R}', '']]],
        ['T', '5*£b/100', [['R', '2*£d/100'], ['\\overline{R}', '1-2*£d/100']]],
        ['S', '1-(2*£a+5*£b)/100', [['R', '£e/100'], ['\\overline{R}', '1-£e/100']]]]
      ],
      prbEnonce: [['2*£a/100', '5*£b/100', '5*£c/100', '2*£d/100', '(2*£a*5*£c+5*£b*2*£d+(100-2*£a-5*£b)*£e)/10000'],
        ['2*£a/100', '(100-2*£a-5*£b)/100', '5*£c/100', '£e/100', '(2*£a*5*£c+5*£b*2*£d+(100-2*£a-5*£b)*£e)/10000'],
        ['5*£b/100', '(100-2*£a-5*£b)/100', '2*£d/100', '£e/100', '(2*£a*5*£c+5*£b*2*£d+(100-2*£a-5*£b)*£e)/10000']
      ],
      // correctionProba est la phrase qui traduit les proba de l’énoncé
      // les proba de l’énoncé doivent nécessairement être repérées par £a, £b, ... et cela correspond aux éléments de prbEnonce dans l’ordre
      correctionProba: ['D’après l’énoncé, $P(G)=£a$, $P(T)=£b$, $P_{G}(R)=£c$, $P_{T}(R)=£d$ et $P(R)=£e$, ce qui nous donne l’arbre incomplet ci-contre :',
        'D’après l’énoncé, $P(G)=£a$, $P(S)=£b$, $P_{G}(R)=£c$, $P_{S}(R)=£d$ et $P(R)=£e$, ce qui nous donne l’arbre incomplet ci-contre :',
        'D’après l’énoncé, $P(S)=£a$, $P(T)=£b$, $P_{T}(R)=£c$, $P_{S}(R)=£d$ et $P(R)=£e$, ce qui nous donne l’arbre incomplet ci-contre :'
      ],
      // Ici on demande la proba d’une intersection calculable avec les données précédentes
      questionInterFacile1: ['La probabilité que le candidat choisi se soit présenté au baccalauréat technologique et l’ait obtenu vaut &1&.',
        'La probabilité que le candidat choisi se soit présenté au baccalauréat professionnel et l’ait obtenu vaut &1&.',
        'La probabilité que le candidat choisi se soit présenté au baccalauréat professionnel et l’ait obtenu vaut &1&.'],
      questionInterFacile: ['Quelle est la probabilité que le candidat choisi se soit présenté au baccalauréat technologique et l’ait obtenu?',
        'Quelle est la probabilité que le candidat choisi se soit présenté au baccalauréat professionnel et l’ait obtenu?',
        'Quelle est la probabilité que le candidat choisi se soit présenté au baccalauréat professionnel et l’ait obtenu?'],
      questionInterFacileFin: ['P(T\\cap R)', 'P(S\\cap R)', 'P(S\\cap R)'],
      probaInterFacile: ['5*£b*2*£d/10000', '(100-2*£a-5*£b)*£e/10000', '(100-2*£a-5*£b)*£e/10000'],
      // Correction associée à la proba à calculer
      correctionInterFacile: ['$P(T\\cap R)=P(T)\\times P_{T}(R)=calcule[5*£b/100]\\times calcule[2*£d/100]=calcule[5*£b*2*£d/10000]$',
        '$P(S\\cap R)=P(S)\\times P_{S}(R)=calcule[(100-2*£a-5*£b)/100]\\times calcule[£e/100]=calcule[(100-2*£a-5*£b)*£e/10000]$',
        '$P(S\\cap R)=P(S)\\times P_{S}(R)=calcule[(100-2*£a-5*£b)/100]\\times calcule[£e/100]=calcule[(100-2*£a-5*£b)*£e/10000]$'],
      // Pour questionInter1, l’intersection n’est pas donnée
      questionInter1: ['La probabilité que le candidat choisi se soit présenté au baccalauréat professionnel et l’ait obtenu vaut &1&.',
        'La probabilité que le candidat choisi se soit présenté au baccalauréat technologique et l’ait obtenu vaut &1&.',
        'La probabilité que le candidat choisi se soit présenté au baccalauréat général et l’ait obtenu vaut &1&.'],
      questionInter: ['Quelle est la probabilité que le candidat choisi se soit présenté au baccalauréat professionnel et l’ait obtenu?',
        'Quelle est la probabilité que le candidat choisi se soit présenté au baccalauréat technologique et l’ait obtenu?',
        'Quelle est la probabilité que le candidat choisi se soit présenté au baccalauréat général et l’ait obtenu?'],
      questionInterFin: ['P(S\\cap R)', 'P(T\\cap R)', 'P(G\\cap R)'],
      probaInter: ['(100-2*£a-5*£b)*£e/10000', '5*£b*2*£d/10000', '2*£a*5*£c/10000'],
      // Correction associée à la proba à calculer
      correctionInter: [['D’après la formule des probabilités totales, on a $P(R)=P(G\\cap R)+P(T\\cap R)+P(S\\cap R)$, ce qui donne $calcule[(2*£a*5*£c+5*£b*2*£d+(100-2*£a-5*£b)*£e)/10000]=P(G)\\times P_{G}(S)+P(T)\\times P_{T}(R)+P(S\\cap R)$.',
        '$P(S\\cap R)=calcule[(2*£a*5*£c+5*£b*2*£d+(100-2*£a-5*£b)*£e)/10000]-calcule[2*£a/100]\\times calcule[5*£c/100]-calcule[5*£b/100]\\times calcule[2*£d/100]=calcule[(100-2*£a-5*£b)*£e/10000]$.'],
      ['D’après la formule des probabilités totales, on a $P(R)=P(G\\cap R)+P(T\\cap R)+P(S\\cap R)$, ce qui donne $calcule[(2*£a*5*£c+5*£b*2*£d+(100-2*£a-5*£b)*£e)/10000]=P(G)\\times P_{G}(S)+P(T\\cap R)+P(S)\\times P_{S}(R)$.',
        '$P(T\\cap R)=calcule[(2*£a*5*£c+5*£b*2*£d+(100-2*£a-5*£b)*£e)/10000]-calcule[2*£a/100]\\times calcule[5*£c/100]-calcule[(100-2*£a-5*£b)/100]\\times calcule[£e/100]=calcule[5*£b*2*£d/10000]$.'],
      ['D’après la formule des probabilités totales, on a $P(R)=P(G\\cap R)+P(T\\cap R)+P(S\\cap R)$, ce qui donne $calcule[(2*£a*5*£c+5*£b*2*£d+(100-2*£a-5*£b)*£e)/10000]=P(G\\cap S)+P(T)\\times P_{T}(R)+P(S)\\times P_{S}(R)$.',
        '$P(G\\cap R)=calcule[(2*£a*5*£c+5*£b*2*£d+(100-2*£a-5*£b)*£e)/10000]-calcule[5*£b/100]\\times calcule[2*£d/100]-calcule[(100-2*£a-5*£b)/100]\\times calcule[£e/100]=calcule[2*£a*5*£c/10000]$.']],
      // Correction similaire mais dans le cas où la première question à été posée
      correctionInterBis: [['D’après la formule des probabilités totales, on a $P(R)=P(G\\cap R)+P(T\\cap R)+P(S\\cap R)$, ce qui donne $calcule[(2*£a*5*£c+5*£b*2*£d+(100-2*£a-5*£b)*£e)/10000]=calcule[2*£a*5*£c/10000]+calcule[5*£b*2*£d/10000]+P(S\\cap R)$.',
        '$P(S\\cap R)=calcule[(2*£a*5*£c+5*£b*2*£d+(100-2*£a-5*£b)*£e)/10000]-calcule[2*£a*5*£c/10000]-calcule[5*£b*2*£d/10000]=calcule[(100-2*£a-5*£b)*£e/10000]$.'],
      ['D’après la formule des probabilités totales, on a $P(R)=P(G\\cap R)+P(T\\cap R)+P(S\\cap R)$, ce qui donne $calcule[(2*£a*5*£c+5*£b*2*£d+(100-2*£a-5*£b)*£e)/10000]=calcule[2*£a*5*£c/10000]+P(T\\cap R)+calcule[(100-2*£a-5*£b)*£e/10000]$.',
        '$P(T\\cap R)=calcule[(2*£a*5*£c+5*£b*2*£d+(100-2*£a-5*£b)*£e)/10000]-calcule[2*£a*5*£c/10000]-calcule[(100-2*£a-5*£b)*£e/10000]=calcule[5*£b*2*£d/10000]$.'],
      ['D’après la formule des probabilités totales, on a $P(R)=P(G\\cap R)+P(T\\cap R)+P(S\\cap R)$, ce qui donne $calcule[(2*£a*5*£c+5*£b*2*£d+(100-2*£a-5*£b)*£e)/10000]=P(G\\cap S)+calcule[5*£b*2*£d/10000]+calcule[(100-2*£a-5*£b)*£e/10000]$.',
        '$P(G\\cap R)=calcule[(2*£a*5*£c+5*£b*2*£d+(100-2*£a-5*£b)*£e)/10000]-calcule[5*£b*2*£d/10000]-calcule[(100-2*£a-5*£b)*£e/10000]=calcule[2*£a*5*£c/10000]$.']],
      // probabilité conditionnelles qui manque pour créer l’arbre (elle utilise la proba inter précédente)
      // probabilité conditionnelle en utilisant la proba précédemment calculée
      questionPrbCond1: ['Le candidat s’est présenté au baccalauréat professionnel, la probabilité qu’il ait été reçu vaut &1&.',
        'Le candidat s’est présenté au baccalauréat technologique, la probabilité qu’il ait été reçu vaut &1&.',
        'Le candidat s’est présenté au baccalauréat général, la probabilité qu’il ait été reçu vaut &1&.'],
      questionPrbSachant1: ['Sachant que le candidat s’est présenté au baccalauréat professionnel, la probabilité qu’il ait été reçu vaut &1&.',
        'Sachant que le candidat s’est présenté au baccalauréat technologique, la probabilité qu’il ait été reçu vaut &1&.',
        'Sachant que le candidat s’est présenté au baccalauréat général, la probabilité qu’il ait été reçu vaut &1&.'],
      questionPrbCond: ['Le candidat s’est présenté au baccalauréat professionnel. Quelle est la probabilité qu’il ait été reçu',
        'Le candidat s’est présenté au baccalauréat technologique. Quelle est la probabilité qu’il ait été reçu?',
        'Le candidat s’est présenté au baccalauréat général. Quelle est la probabilité qu’il ait été reçu?'],
      questionPrbSachant: ['Sachant que le candidat s’est présenté au baccalauréat professionnel, quelle est la probabilité qu’il ait été reçu?',
        'Sachant que le candidat s’est présenté au baccalauréat technologique, quelle est la probabilité qu’il ait été reçu?',
        'Sachant que le candidat s’est présenté au baccalauréat général, quelle est la probabilité qu’il ait été reçu?'],
      questionPrbSachantFin: ['$P_{S}(R)$', '$P_{T}(R)$', '$P_{G}(R)$'],
      probaCond: ['£e/100', '2*£d/100', '5*£c/100'],
      // correction de la probabilité conditionnelle
      correctionCond: ['$P_{S}(R)=\\frac{P(S\\cap R)}{P(S)}=\\frac{calcule[(100-2*£a-5*£b)*£e/10000]}{calcule[(100-2*£a-5*£b)/100]}=£r$',
        '$P_{T}(R)=\\frac{P(T\\cap R)}{P(T)}=\\frac{calcule[5*£b*2*£d/10000]}{calcule[5*£b/100]}=£r$',
        '$P_{G}(R)=\\frac{P(G\\cap R)}{P(G)}=\\frac{calcule[2*£a*5*£c/10000]}{calcule[2*£a/100]}=£r$']
    },

    {
      nomSujet: 'Amérique du Nord juin 2015 ES',
      consignesInit: [''],
      // consignesInit est l’énoncé commun
      consignesDonnees: [['Dans un collège, calcule[(2*£a*5*£b+(100-2*£a)*£c)/100]% des élèves sont inscrits à l’association sportive.',
        'Une enquête a montré que calcule[2*£a]% des élèves de ce collège sont fumeurs.',
        'De plus, parmi les élèves non fumeurs, £c% sont inscrits à l’association sportive.'],
      ['Dans un collège, calcule[(2*£a*5*£b+(100-2*£a)*£c)/100]% des élèves sont inscrits à l’association sportive.',
        'Une enquête a montré que calcule[100-2*£a]% des élèves de ce collège sont non fumeurs.',
        'De plus, parmi les élèves fumeurs, calcule[5*£b]% sont inscrits à l’association sportive.']
      ],
      // consignesDonnees contient les différentes variantes de ce qui peut être donné dans l’énoncé
      consignesFin: ['On choisit au hasard un élève de ce collège. On note :',
        '- $S$ l’évènement «l’élève choisi est inscrit à l’association sportive»;',
        '- $F$ l’évènement «l’élève choisi est fumeur».'],
      // consignesFin est la fin de l’énoncé commun
      variables: [['a', '[7;9]'], ['b', '[16;22]', '10'], ['c', '[21;24]']],
      // la longueur de evtsProba correspond au nombre de premières branches de l’arbre
      // evstProba[i][2] est un tableau de couples evt/proba correspondant aux secondes branches pour chaque premier evt
      evtsProba: [[['F', '2*£a/100', [['S', ''], ['\\overline{S}', '']]],
        ['\\overline{F}', '1-2*£a/100', [['S', '£c/100'], ['\\overline{S}', '1-£c/100']]]],
      [['F', '2*£a/100', [['S', '5*£b/100'], ['\\overline{S}', '1-5*£b/100']]],
        ['\\overline{F}', '1-2*£a/100', [['S', ''], ['\\overline{S}', '']]]]
      ],
      prbEnonce: [['(2*£a*5*£b+(100-2*£a)*£c)/10000', '2*£a/100', '£c/100'], ['(2*£a*5*£b+(100-2*£a)*£c)/10000', '2*£a/100', '5*£b/100']
      ],
      // correctionProba est la phrase qui traduit les proba de l’énoncé
      // les proba de l’énoncé doivent nécessairement être repérées par £a, £b, ... et cela correspond aux éléments de prbEnonce dans l’ordre
      correctionProba: ['D’après l’énoncé, $P(S)=£a$, $P(F)=£b$ et $P_{\\overline{F}}(S)=£c$, ce qui nous donne l’arbre incomplet ci-contre :',
        'D’après l’énoncé, $P(S)=£a$, $P(F)=£b$ et $P_{F}(S)=£c$, ce qui nous donne l’arbre incomplet ci-contre :'
      ],
      // Ici on demande la proba d’une intersection calculable avec les données précédentes
      questionInterFacile1: ['La probabilité que l’élève choisi soit non fumeur et qu’il soit inscrit à l’association sportive vaut &1&.',
        'La probabilité que l’élève choisi soit fumeur et qu’il soit inscrit à l’association sportive vaut &1&.'],
      questionInterFacile: ['Quelle est la probabilité que l’élève choisi soit non fumeur et qu’il soit inscrit à l’association sportive?',
        'Quelle est la probabilité que l’élève choisi soit fumeur et qu’il soit inscrit à l’association sportive?'],
      questionInterFacileFin: ['P(\\overline{F}\\cap S)', 'P(F\\cap S)'],
      probaInterFacile: ['(100-2*£a)*£c/10000', '2*£a*5*£b/10000'],
      // Correction associée à la proba à calculer
      correctionInterFacile: ['$P(\\overline{F}\\cap S)=P(\\overline{F})\\times P_{\\overline{F}}(S)=calcule[(100-2*£a)/100]\\times calcule[£c/100]=calcule[(100-2*£a)*£c/10000]$',
        '$P(F\\cap S)=P(F)\\times P_{F}(S)=calcule[2*£a/100]\\times calcule[5*£b/100]=calcule[2*£a*5*£b/10000]$'],
      // Pour questionInter1, l’intersection n’est pas donnée
      questionInter1: ['La probabilité que l’élève soit fumeur et qu’il soit inscrit à l’association sportive vaut &1&.',
        'La probabilité que l’élève soit non fumeur et qu’il soit inscrit à l’association sportive vaut &1&.'],
      questionInter: ['Quelle est la probabilité que l’élève soit fumeur et qu’il soit inscrit à l’association sportive?',
        'Quelle est la probabilité que l’élève soit non fumeur et qu’il soit inscrit à l’association sportive?'],
      questionInterFin: ['P(F\\cap S)', 'P(\\overline{F}\\cap S)'],
      probaInter: ['2*£a*5*£b/10000', '(100-2*£a)*£c/10000'],
      // Correction associée à la proba à calculer
      correctionInter: [['D’après la formule des probabilités totales, on a $P(S)=P(F\\cap S)+P(\\overline{F}\\cap S)$, ce qui donne $calcule[(2*£a*5*£b+(100-2*£a)*£c)/10000]=P(F\\cap S)+P(\\overline{F})\\times P_{\\overline{F}}(S)$.',
        '$P(F\\cap S)=calcule[(2*£a*5*£b+(100-2*£a)*£c)/10000]-calcule[1-2*£a/100]\\times calcule[£c/100]=calcule[2*£a*5*£b/10000]$.'],
      ['D’après la formule des probabilités totales, on a $P(S)=P(F\\cap S)+P(\\overline{F}\\cap S)$, ce qui donne $calcule[(2*£a*5*£b+(100-2*£a)*£c)/10000]=P(F)\\times P_{F}(S)+P(\\overline{F}\\cap S)$.',
        '$P(\\overline{F}\\cap S)=calcule[(2*£a*5*£b+(100-2*£a)*£c)/10000]-calcule[2*£a/100]\\times calcule[5*£b/100]=calcule[(100-2*£a)*£c/10000]$.']],
      // Correction similaire mais dans le cas où la première question à été posée
      correctionInterBis: [['D’après la formule des probabilités totales, on a $P(S)=P(F\\cap S)+P(\\overline{F}\\cap S)$, ce qui donne $calcule[(2*£a*5*£b+(100-2*£a)*£c)/10000]=P(F\\cap S)+calcule[(100-2*£a)*£c/10000]$.',
        '$P(F\\cap S)=calcule[(2*£a*5*£b+(100-2*£a)*£c)/10000]-calcule[(100-2*£a)*£c/10000]=calcule[2*£a*5*£b/10000]$.'],
      ['D’après la formule des probabilités totales, on a $P(S)=P(F\\cap S)+P(\\overline{F}\\cap S)$, ce qui donne $calcule[(2*£a*5*£b+(100-2*£a)*£c)/10000]=calcule[2*£a*5*£b/10000]+P(\\overline{F}\\cap S)$.',
        '$P(\\overline{F}\\cap S)=calcule[(2*£a*5*£b+(100-2*£a)*£c)/10000]-calcule[2*£a*5*£b/10000]=calcule[(100-2*£a)*£c/10000]$.']],
      // probabilité conditionnelles qui manque pour créer l’arbre (elle utilise la proba inter précédente)
      // probabilité conditionnelle en utilisant la proba précédemment calculée
      questionPrbCond1: ['L’élève est fumeur, la probabilité qu’il soit inscrit à l’association sportive vaut &1&.',
        'L’élève n’est pas fumeur, la probabilité qu’il soit inscrit à l’association sportive vaut &1&.'],
      questionPrbSachant1: ['Sachant que l’élève est fumeur, la probabilité qu’il soit inscrit à l’association sportive vaut &1&.',
        'Sachant que l’élève n’est pas fumeur, la probabilité qu’il soit inscrit à l’association sportive vaut &1&.'],
      questionPrbCond: ['L’élève est fumeur. Quelle est la probabilité qu’il soit inscrit à l’association sportive?',
        'L’élève n’est pas fumeur. Quelle est la probabilité qu’il soit inscrit à l’association sportive?'],
      questionPrbSachant: ['Sachant que l’élève est fumeur, quelle est la probabilité qu’il soit inscrit à l’association sportive?',
        'Sachant que l’élève n’est pas fumeur, quelle est la probabilité qu’il soit inscrit à l’association sportive?'],
      questionPrbSachantFin: ['$P_{F}(S)$', '$P_{\\overline{F}}(S)$'],
      probaCond: ['5*£b/100', '£c/100'],
      // correction de la probabilité conditionnelle
      correctionCond: ['$P_{F}(S)=\\frac{P(F\\cap S)}{P(F)}=\\frac{calcule[2*£a*5*£b/10000]}{calcule[2*£a/100]}=£r$',
        '$P_{\\overline{F}}(S)=\\frac{P(\\overline{F}\\cap S)}{P(\\overline{F})}=\\frac{calcule[(100-2*£a)*£c/10000]}{calcule[(100-2*£a)/100]}=£r$']
    }
  ]
}
