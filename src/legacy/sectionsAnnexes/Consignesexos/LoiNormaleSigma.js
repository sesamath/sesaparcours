export default {
  /* Dans toutes les consignes de ce type d’exercices, £a permet de déclarer la variable a. Il faut alors préciser l’intervalle dans lequel se trouve cette valeur
    Les consignes seront le plus souvent sur plusieurs lignes, d’où l’idée de créer un tableau
    la propriété variables de chaque énoncé est un tableau où chaque élément est un tableau de deux ou trois éléments
    Par exemple ['a','[5;15]'] signifie que a sera un entier de l’intervalle [5;15]
    ['a','[5;15]',100] signifie que a prend une valeur décimale de [0,05;0,15] avec 2 chiffres après la virgule
    */
  sujets: [
    {
      nomSujet: 'Amérique du Nord juin 2016 S', // numéro 1
      consignes: ['Une entreprise fabrique des billes en bois sphériques grâce à une machine de production.',
        'L’entreprise considère qu’une bille peut être vendue uniquement lorsque son diamètre est compris entre $calcule[£a-£c*0.1]$&nbsp;cm et $calcule[£a+£c*0.1]$&nbsp;cm.',
        'Le diamètre d’une bille prélevée au hasard dans la production est modélisé à l’aide d’une variable aléatoire $Y$ qui suit une loi normale d’espérance $\\mu=£a$ et d’écart-type $\\sigma$, $\\sigma$ étant un réel strictement positif.'],
      // ce qui suit constitue une question prélimimaire qu’on pourra poser ou non (paramètre de la section)
      questComplementaire: ['On sait que $P(calcule[£a-£c*0.1]\\leq Y \\leq calcule[£a+£c*0.1])\\approx £p$.',
        'Soit $Z$ la variable aléatoire égale à $\\frac{Y-\\mu}{\\sigma}$.',
        'Déterminer une valeur approchée à $10^{-4}$ près du réel $u$ tel que $P(-u\\leq Z\\leq u)=£p$.'],
      questComplementaireTexas: ['On sait que $P(calcule[£a-£c*0.1]\\leq Y \\leq calcule[£a+£c*0.1])\\approx £p$.',
        'Soit $Z$ la variable aléatoire égale à $\\frac{Y-\\mu}{\\sigma}$.',
        'Déterminer une valeur approchée à $10^{-4}$ près du réel $u$ tel que $P(Z\\leq u)=calcule[£p+(1-£p)/2]$.'],
      infoComplementaire: ['D’après la question précédente, la variable aléatoire $Z=\\frac{Y-\\mu}{\\sigma}$ vérifie $P(-£u\\leq Z\\leq £u)\\approx £p$.'],
      infoComplementaireTexas: ['D’après la question précédente, la variable aléatoire $Z=\\frac{Y-\\mu}{\\sigma}$ vérifie $P(Z\\leq £u)\\approx calcule[£p+(1-£p)/2]$.'],
      question: 'Sachant que $P(calcule[£a-£c*0.1]\\leq Y \\leq calcule[£a+£c*0.1])\\approx £p$, déterminer la valeur de $\\sigma$.',
      questionComp: 'En utilisant ce résultat et sachant que $P(calcule[£a-£c*0.1]\\leq Y \\leq calcule[£a+£c*0.1])\\approx £p$, déterminer la valeur de $\\sigma$.',
      variables: [['a', '[2;4]'], ['b', '[81;89]', 1000], ['c', '[1;2]']],
      // je récupère les valeurs de mes variables
      valeurs: { mu: '£a', sigma: '£b', borneInf: '£a-£c*0.1', borneSup: '£a+£c*0.1', u: '£c*0.1/£b', q: '£p+(1-£p)/2' },
      // Pour l’affichage de la courbe, je donne les borne de l’intervalle que j’affiche
      // le premier cas de figure est celui donné par l’énoncé, le 2nd, celui qui est donné quand on prend une aire à gauche (ce sera parfois la même chose)
      bornesGraph: [['-£c*0.1/£b', '£c*0.1/£b'], ['-5', '£c*0.1/£b']],
      // explicationsComp sera affichée qu’on ait la question sur le calcul de sigma directement ou qu’on ait la question intermédiaire
      explicationsComp: ['$Y$ suivant une loi normale d’espérance $\\mu$ et d’écart-type $\\sigma$, $Z=\\frac{Y-\\mu}{\\sigma}$ suit la loi normale centrée réduite $\\Norm(0;1)$.',
        'A l’aide de la calculatrice, on détermine le réel $u$ tel que $P(-u\\leq Z\\leq u)=£p$.',
        'Dans la partie distributions, choisir "Inverse Normale" (InvN, invNorm ou FracNormale suivant les modèles). On complète alors par la valeur de la probabilité (qui est une aire centrée), $£p$ dans notre cas et par les valeurs de l’espérance $\\mu=0$ et de l’écart-type $\\sigma=1$ de $Z$.',
        'On obtient ainsi $u\\approx £u$ et on a $P(-£u\\leq Z\\leq £u)\\approx £p$ (voir figure ci-contre).'],
      explicationsCompTexas: ['$Y$ suivant une loi normale d’espérance $\\mu$ et d’écart-type $\\sigma$, $Z=\\frac{Y-\\mu}{\\sigma}$ suit la loi normale centrée réduite $\\Norm(0;1)$.',
        'A l’aide de la calculatrice, on détermine le réel $u$ tel que $P(Z\\leq u)=£q$',
        'Dans la partie distributions, choisir "Inverse Normale" (InvN, invNorm ou FracNormale suivant les modèles). On complète alors par la valeur de la probabilité (qui est une aire à gauche), $£q$ dans notre cas et par les valeurs de l’espérance $\\mu=0$ et de l’écart-type $\\sigma=1$ de $Z$.',
        'On obtient ainsi $u\\approx £u$ et on a $P(Z\\leq £u)\\approx £q$ (voir figure ci-contre).'],
      // explications n’est affichée que dans le cas où on demande sigma
      explications: ['De plus $calcule[£a-£c*0.1]\\leq Y \\leq calcule[£a+£c*0.1]\\iff calcule[-£c*0.1]\\leq Y-\\mu \\leq calcule[£c*0.1]\\iff \\frac{calcule[-£c*0.1]}{\\sigma}\\leq Z\\leq \\frac{calcule[£c*0.1]}{\\sigma}$.',
        'Sachant que $P(calcule[£a-£c*0.1]\\leq Y \\leq calcule[£a+£c*0.1])=£p$, on en déduit que <font color="red">$P(\\frac{calcule[-£c*0.1]}{\\sigma}\\leq Z\\leq \\frac{calcule[£c*0.1]}{\\sigma})=£p$</font>.'],
      explicationsTexas: ['De plus $calcule[£a-£c*0.1]\\leq Y \\leq calcule[£a+£c*0.1]\\iff calcule[-£c*0.1]\\leq Y-\\mu \\leq calcule[£c*0.1]\\iff \\frac{calcule[-£c*0.1]}{\\sigma}\\leq Z\\leq \\frac{calcule[£c*0.1]}{\\sigma}$.',
        'Sachant que $P(calcule[£a-£c*0.1]\\leq Y \\leq calcule[£a+£c*0.1])=£p$, on en déduit que $P(\\frac{calcule[-£c*0.1]}{\\sigma}\\leq Z\\leq \\frac{calcule[£c*0.1]}{\\sigma})=£p$ et par symétrie on obtient <font color="red">$P(Z\\leq \\frac{calcule[£c*0.1]}{\\sigma})=calcule[£p+(1-£p)/2]$</font>.'],
      explicationsComp2: ['$Y$ suivant une loi normale d’espérance $\\mu$ et d’écart-type $\\sigma$, $Z=\\frac{Y-\\mu}{\\sigma}$ suit la loi normale centrée réduite $\\Norm(0;1)$.',
        'A l’aide de la calculatrice, on détermine le réel $u$ tel que $P(-u\\leq Z\\leq u)=£p$.',
        'On obtient ainsi $u\\approx £u$ et on a <font color="red">$P(-£u\\leq Z\\leq £u)\\approx £p$</font> (voir figure ci-contre).'],
      explicationsCompTexas2: ['$Y$ suivant une loi normale d’espérance $\\mu$ et d’écart-type $\\sigma$, $Z=\\frac{Y-\\mu}{\\sigma}$ suit la loi normale centrée réduite $\\Norm(0;1)$.',
        'A l’aide de la calculatrice, on détermine le réel $u$ tel que $P(Z\\leq u)=£q$.',
        'On obtient ainsi $u\\approx £u$ et on a <font color="red">$P(Z\\leq £u)\\approx £q$</font> (voir figure ci-contre).'],
      explications2: ['On a $calcule[£a-£c*0.1]\\leq Y \\leq calcule[£a+£c*0.1]\\iff calcule[-£c*0.1]\\leq Y-\\mu \\leq calcule[£c*0.1]\\iff \\frac{calcule[-£c*0.1]}{\\sigma}\\leq Z\\leq \\frac{calcule[£c*0.1]}{\\sigma}$.',
        'Sachant que $P(calcule[£a-£c*0.1]\\leq Y \\leq calcule[£a+£c*0.1])=£p$, on en déduit que <font color="red">$P(\\frac{calcule[-£c*0.1]}{\\sigma}\\leq Z\\leq \\frac{calcule[£c*0.1]}{\\sigma})=£p$</font>.'],
      explicationsTexas2: ['$P(calcule[£a-£c*0.1]\\leq Y \\leq calcule[£a+£c*0.1])=£p$ signifie, en utilisant la symétrie de la courbe représentant la densité et l’aire totale sous cette courbe, que $P(Y\\leq calcule[£a+£c*0.1])=£q$.<br/>De plus $Y \\leq calcule[£a+£c*0.1]\\iff Y-\\mu \\leq calcule[£c*0.1]\\iff Z\\leq \\frac{calcule[£c*0.1]}{\\sigma}$.',
        'Sachant que $P(Y \\leq calcule[£a+£c*0.1])=£q$, on en déduit que <font color="red">$P(Z\\leq \\frac{calcule[£c*0.1]}{\\sigma})=£q$</font>.'],
      conclusion: ['D’après ce qui précède, on obtient ainsi $\\frac{calcule[£c*0.1]}{\\sigma}\\approx £u$ d’où $\\sigma\\approx \\frac{calcule[£c*0.1]}{£u}$ c’est-à-dire $\\sigma\\approx £b$.',
        'Grâce au résultat <font color="red">$P(-£u\\leq Z\\leq £u)\\approx £p$</font>, on obtient $\\frac{calcule[£c*0.1]}{\\sigma}\\approx £u$ d’où $\\sigma\\approx \\frac{calcule[£c*0.1]}{£u}$ c’est-à-dire $\\sigma\\approx £b$.'],
      conclusionTexas: ['D’après ce qui précède, on obtient ainsi $\\frac{calcule[£c*0.1]}{\\sigma}\\approx £u$ d’où $\\sigma\\approx \\frac{calcule[£c*0.1]}{£u}$ c’est-à-dire $\\sigma\\approx £b$.',
        'Grâce au résultat <font color="red">$P(Z\\leq £u)\\approx £q$</font>, on obtient $\\frac{calcule[£c*0.1]}{\\sigma}\\approx £u$ d’où $\\sigma\\approx \\frac{calcule[£c*0.1]}{£u}$ c’est-à-dire $\\sigma\\approx £b$.'],
      arrondi: 3, // tolérance pour l’arrondi de l’écart-type
      arrondiU: 4,
      arrondiProba: 3 // ça c’est l’arrondi de la proba donnée dans l’énoncé
    },
    {
      nomSujet: 'Métropole septembre 2016 S', // numéro 2
      consignes: ['Une personne est dite en hypoglycémie si sa glycémie à jeun est inférieure à 60 mg.dL$^{-1}$ et elle est en hyperglycémie si sa glycémie à jeun est supérieure à 110 mg.dL$^{−1}$.',
        'On choisit au hasard un adulte dans cette population. Une étude a permis d’établir que la probabilité qu’il soit en hyperglycémie est $£p$.',
        'On modélise la glycémie à jeun, exprimée en mg.dL$^{−1}$, d’un adulte d’une population donnée, par une variable aléatoire $X$ qui suit une loi normale d’espérance $\\mu=£a$ et d’écart-type $\\sigma$.'],
      // ce qui suit constitue une question prélimimaire qu’on pourra poser ou non (paramètre de la section)
      questComplementaire: ['Soit $Y$ la variable aléatoire égale à $\\frac{X-\\mu}{\\sigma}$.',
        'Déterminer une valeur approchée à $10^{-3}$ près du réel $u$ tel que $P(Y\\geq u)=£p$.'],
      questComplementaireTexas: ['Soit $Y$ la variable aléatoire égale à $\\frac{X-\\mu}{\\sigma}$.',
        'Déterminer une valeur approchée à $10^{-3}$ près du réel $u$ tel que $P(Y\\leq u)=calcule[1-£p]$.'],
      infoComplementaire: ['D’après la question précédente, la variable aléatoire $Y=\\frac{X-\\mu}{\\sigma}$ vérifie $P(Y\\geq £u)\\approx £p$.'],
      infoComplementaireTexas: ['D’après la question précédente, la variable aléatoire $Y=\\frac{X-\\mu}{\\sigma}$ vérifie $P(Y\\leq £u)\\approx calcule[1-£p]$.'],
      question: 'Sachant que la probabilité d’être en hyperglycémie vaut $£p$, déterminer la valeur de $\\sigma$.',
      questionComp: 'En utilisant ce résultat et sachant que la probabilité d’être en hyperglycémie vaut $£p$, déterminer la valeur de $\\sigma$.',
      variables: [['a', '[88;91]'], ['b', '[11;13]']],
      // je récupère les valeurs de mes variables
      valeurs: { mu: '£a', sigma: '£b', borneInf: '110', borneSup: '£a+5*£b', u: '(110-£a)/£b', q: '1-£p' },
      // Pour l’affichage de la courbe, je donne les bornes de l’intervalle que j’affiche
      // le premier cas de figure est celui donné par l’énoncé, le 2nd, celui qui est donné quand on prend une aire à gauche (ce sera parfois la même chose)
      bornesGraph: [['(110-£a)/£b', '5'], ['-5', '(110-£a)/£b']],
      // explicationsComp sera affichée qu’on ait la question sur le calcul de sigma directement ou qu’on ait la question intermédiaire
      explicationsComp: ['$X$ suivant une loi normale d’espérance $\\mu$ et d’écart-type $\\sigma$, $Y=\\frac{X-\\mu}{\\sigma}$ suit la loi normale centrée réduite $\\Norm(0;1)$.',
        'A l’aide de la calculatrice, on détermine le réel $u$ tel que $P(Y\\geq u)=£p$.',
        'Dans la partie distributions, choisir "Inverse Normale" (InvN, invNorm ou FracNormale suivant les modèles). On complète alors par la valeur de la probabilité (qui est une aire à droite), $£p$ dans notre cas et par les valeurs de l’espérance $\\mu=0$ et de l’écart-type $\\sigma=1$ de $Y$.',
        'On obtient ainsi $u\\approx £u$ et on a $P(Y\\geq £u)\\approx £p$ (voir figure ci-contre).'],
      explicationsCompTexas: ['$X$ suivant une loi normale d’espérance $\\mu$ et d’écart-type $\\sigma$, $Y=\\frac{X-\\mu}{\\sigma}$ suit la loi normale centrée réduite $\\Norm(0;1)$.',
        'A l’aide de la calculatrice, on détermine le réel $u$ tel que $P(Y\\leq u)=£q$',
        'Dans la partie distributions, choisir "Inverse Normale" (InvN, invNorm ou FracNormale suivant les modèles). On complète alors par la valeur de la probabilité (qui est une aire à gauche), $£q$ dans notre cas et par les valeurs de l’espérance $\\mu=0$ et de l’écart-type $\\sigma=1$ de $Y$.',
        'On obtient ainsi $u\\approx £u$ et on a $P(Y\\leq £u)\\approx £q$ (voir figure ci-contre).'],
      // explications n’est affichée que dans le cas où on demande sigma
      explications: ['De plus $X \\geq 110\\iff X-\\mu \\geq calcule[110-£a]\\iff Y\\geq \\frac{calcule[110-£a]}{\\sigma}$.',
        'Sachant que $P(X \\geq 110)=£p$, on en déduit que <font color="red">$P(Y\\geq \\frac{calcule[110-£a]}{\\sigma})=£p$</font>.'],
      explicationsTexas: ['De plus $X \\geq 110\\iff X-\\mu \\geq calcule[110-£a]\\iff Y\\geq \\frac{calcule[110-£a]}{\\sigma}$.',
        'Sachant que $P(X \\geq 110)=£p$, on en déduit que $P(Y\\geq \\frac{calcule[110-£a]}{\\sigma})=£p$ et on obtient <font color="red">$P(Y\\leq \\frac{calcule[110-£a]}{\\sigma})=calcule[1-£p]$</font>.'],
      explicationsComp2: ['$X$ suivant une loi normale d’espérance $\\mu$ et d’écart-type $\\sigma$, $Y=\\frac{X-\\mu}{\\sigma}$ suit la loi normale centrée réduite $\\Norm(0;1)$.',
        'A l’aide de la calculatrice, on détermine le réel $u$ tel que $P(Y\\geq u)=£p$.',
        'On obtient ainsi $u\\approx £u$ et on a <font color="red">$P(Y\\geq £u)\\approx £p$</font> (voir figure ci-contre).'],
      explicationsCompTexas2: ['$X$ suivant une loi normale d’espérance $\\mu$ et d’écart-type $\\sigma$, $Y=\\frac{X-\\mu}{\\sigma}$ suit la loi normale centrée réduite $\\Norm(0;1)$.',
        'A l’aide de la calculatrice, on détermine le réel $u$ tel que $P(Y\\leq u)=£q$.',
        'On obtient ainsi $u\\approx £u$ et on a <font color="red">$P(Y\\leq £u)\\approx £q$</font> (voir figure ci-contre).'],
      explications2: ['On a $X \\geq 110\\iff X-\\mu \\geq calcule[110-£a]\\iff Y\\geq \\frac{calcule[110-£a]}{\\sigma}$.',
        'Sachant que $P(X \\geq 110)=£p$, on en déduit que <font color="red">$P(Y\\geq \\frac{calcule[110-£a]}{\\sigma})=£p$</font>.'],
      explicationsTexas2: ['$P(X \\geq 110)=£p$ signifie, en utilisant l’aire totale sous la courbe représentant la densité, que $P(X\\leq 110)=£q$.<br/>De plus $X \\leq 110\\iff X-\\mu \\leq calcule[110-£a]\\iff Y\\leq \\frac{calcule[110-£a]}{\\sigma}$.',
        'Sachant que $P(X \\leq 110)=£q$, on en déduit que <font color="red">$P(Y\\leq \\frac{calcule[110-£a]}{\\sigma})=£q$</font>.'],
      conclusion: ['D’après ce qui précède, on obtient ainsi $\\frac{calcule[110-£a]}{\\sigma}\\approx £u$ d’où $\\sigma\\approx \\frac{calcule[110-£a]}{£u}$ c’est-à-dire $\\sigma\\approx £b$.',
        'Grâce au résultat <font color="red">$P(Y\\geq £u)\\approx £p$</font>, on obtient $\\frac{calcule[110-£a]}{\\sigma}\\approx £u$ d’où $\\sigma\\approx \\frac{calcule[110-£a]}{£u}$ c’est-à-dire $\\sigma\\approx £b$.'],
      conclusionTexas: ['D’après ce qui précède, on obtient ainsi $\\frac{calcule[110-£a]}{\\sigma}\\approx £u$ d’où $\\sigma\\approx \\frac{calcule[110-£a]}{£u}$ c’est-à-dire $\\sigma\\approx £b$.',
        'Grâce au résultat <font color="red">$P(Y\\leq £u)\\approx £q$</font>, on obtient $\\frac{calcule[110-£a]}{\\sigma}\\approx £u$ d’où $\\sigma\\approx \\frac{calcule[110-£a]}{£u}$ c’est-à-dire $\\sigma\\approx £b$.'],
      arrondi: 0, // tolérance pour l’arrondi de l’écart-type
      arrondiU: 3,
      arrondiProba: 3 // ça c’est l’arrondi de la proba donnée dans l’énoncé
    },
    {
      nomSujet: 'Amérique du Nord juin 2015 S', // numéro 3
      consignes: ['Une tablette de chocolat doit peser 100 grammes avec une tolérance de $calcule[£c*0.5]$ grammes en plus ou en moins.',
        'Elle est donc mise sur le marché si sa masse est comprise entre $calcule[100-£c*0.5]$ et $calcule[100+£c*0.5]$ grammes.',
        'La masse (exprimée en grammes) d’une tablette de chocolat peut être modélisée par une variable aléatoire $X$ suivant la loi normale d’espérance $\\mu=100$ et d’écart-type $\\sigma$.'],
      // ce qui suit constitue une question prélimimaire qu’on pourra poser ou non (paramètre de la section)
      questComplementaire: ['On sait que la probabilité que la tablette est mise sur le marché vaut $£p$.',
        'Soit $Y$ la variable aléatoire égale à $\\frac{X-\\mu}{\\sigma}$.',
        'Déterminer une valeur approchée à $10^{-4}$ près du réel $u$ tel que $P(-u\\leq Y\\leq u)=£p$.'],
      questComplementaireTexas: ['On sait que la probabilité que la tablette est mise sur le marché vaut $£p$.',
        'Soit $Y$ la variable aléatoire égale à $\\frac{X-\\mu}{\\sigma}$.',
        'Déterminer une valeur approchée à $10^{-4}$ près du réel $u$ tel que $P(Y\\leq u)=calcule[£p+(1-£p)/2]$.'],
      infoComplementaire: ['D’après la question précédente, la variable aléatoire $Y=\\frac{X-\\mu}{\\sigma}$ vérifie $P(-£u\\leq Y\\leq £u)\\approx £p$.'],
      infoComplementaireTexas: ['D’après la question précédente, la variable aléatoire $Y=\\frac{X-\\mu}{\\sigma}$ vérifie $P(Y\\leq £u)\\approx calcule[£p+(1-£p)/2]$.'],
      question: 'Sachant que la probabilité que la tablette est mise sur le marché vaut $£p$, déterminer la valeur de $\\sigma$.',
      questionComp: 'En utilisant ce résultat et sachant que la probabilité que la tablette est mise sur le marché vaut $£p$, déterminer la valeur de $\\sigma$.',
      variables: [['a', '[100;100]'], ['b', '[89;95]', 100], ['c', '[2;4]']],
      // je récupère les valeurs de mes variables
      valeurs: { mu: '£a', sigma: '£b', borneInf: '100-£c*0.5', borneSup: '100+£c*0.5', u: '£c*0.5/£b', q: '£p+(1-£p)/2' },
      // Pour l’affichage de la courbe, je donne les bornes de l’intervalle que j’affiche
      // le premier cas de figure est celui donné par l’énoncé, le 2nd, celui qui est donné quand on prend une aire à gauche (ce sera parfois la même chose)
      bornesGraph: [['-£c*0.5/£b', '£c*0.5/£b'], ['-5', '£c*0.5/£b']],
      // explicationsComp sera affichée qu’on ait la question sur le calcul de sigma directement ou qu’on ait la question intermédiaire
      explicationsComp: ['$X$ suivant une loi normale d’espérance $\\mu$ et d’écart-type $\\sigma$, $Y=\\frac{X-\\mu}{\\sigma}$ suit la loi normale centrée réduite $\\Norm(0;1)$.',
        'A l’aide de la calculatrice, on détermine le réel $u$ tel que $P(-u\\leq Y\\leq u)=£p$.',
        'Dans la partie distributions, choisir "Inverse Normale" (InvN, invNorm ou FracNormale suivant les modèles). On complète alors par la valeur de la probabilité (qui est une aire centrée), $£p$ dans notre cas et par les valeurs de l’espérance $\\mu=0$ et de l’écart-type $\\sigma=1$ de $Y$.',
        'On obtient ainsi $u\\approx £u$ et on a $P(-£u\\leq Y\\leq £u)\\approx £p$ (voir figure ci-contre).'],
      explicationsCompTexas: ['$X$ suivant une loi normale d’espérance $\\mu$ et d’écart-type $\\sigma$, $Y=\\frac{X-\\mu}{\\sigma}$ suit la loi normale centrée réduite $\\Norm(0;1)$.',
        'A l’aide de la calculatrice, on détermine le réel $u$ tel que $P(Y\\leq u)=£q$',
        'Dans la partie distributions, choisir "Inverse Normale" (InvN, invNorm ou FracNormale suivant les modèles). On complète alors par la valeur de la probabilité (qui est une aire à gauche), $£q$ dans notre cas et par les valeurs de l’espérance $\\mu=0$ et de l’écart-type $\\sigma=1$ de $Y$.',
        'On obtient ainsi $u\\approx £u$ et on a $P(Y\\leq £u)\\approx £q$ (voir figure ci-contre).'],
      // explications n’est affichée que dans le cas où on demande sigma
      explications: ['De plus $calcule[100-£c*0.5]\\leq X \\leq calcule[100+£c*0.5]\\iff -calcule[£c*0.5]\\leq X-\\mu \\leq calcule[£c*0.5]\\iff -\\frac{calcule[£c*0.5]}{\\sigma}\\leq Y\\leq \\frac{calcule[£c*0.5]}{\\sigma}$.',
        'Sachant que $P(calcule[100-£c*0.5]\\leq X \\leq calcule[100+£c*0.5])=£p$, on en déduit que <font color="red">$P(-\\frac{calcule[£c*0.5]}{\\sigma}\\leq Y\\leq \\frac{calcule[£c*0.5]}{\\sigma})=£p$</font>.'],
      explicationsTexas: ['De plus $calcule[100-£c*0.5]\\leq X \\leq calcule[100+£c*0.5]\\iff -calcule[£c*0.5]\\leq X-\\mu \\leq calcule[£c*0.5]\\iff -\\frac{calcule[£c*0.5]}{\\sigma}\\leq Y\\leq \\frac{calcule[£c*0.5]}{\\sigma}$.',
        'Sachant que $P(calcule[100-£c*0.5]\\leq X \\leq calcule[100+£c*0.5])=£p$, on en déduit que $P(-\\frac{calcule[£c*0.5]}{\\sigma}\\leq Y\\leq \\frac{calcule[£c*0.5]}{\\sigma})=£p$ et par symétrie on obtient <font color="red">$P(Y\\leq \\frac{calcule[£c*0.5]}{\\sigma})=calcule[£p+(1-£p)/2]$</font>.'],
      explicationsComp2: ['$X$ suivant une loi normale d’espérance $\\mu$ et d’écart-type $\\sigma$, $Y=\\frac{X-\\mu}{\\sigma}$ suit la loi normale centrée réduite $\\Norm(0;1)$.',
        'A l’aide de la calculatrice, on détermine le réel $u$ tel que $P(-u\\leq Y\\leq u)=£p$.',
        'On obtient ainsi $u\\approx £u$ et on a <font color="red">$P(-£u\\leq Y\\leq £u)\\approx £p$</font> (voir figure ci-contre).'],
      explicationsCompTexas2: ['$X$ suivant une loi normale d’espérance $\\mu$ et d’écart-type $\\sigma$, $Y=\\frac{X-\\mu}{\\sigma}$ suit la loi normale centrée réduite $\\Norm(0;1)$.',
        'A l’aide de la calculatrice, on détermine le réel $u$ tel que $P(Y\\leq u)=£q$.',
        'On obtient ainsi $u\\approx £u$ et on a <font color="red">$P(Y\\leq £u)\\approx £q$</font> (voir figure ci-contre).'],
      explications2: ['On a $calcule[100-£c*0.5]\\leq X \\leq calcule[100+£c*0.5]\\iff -calcule[£c*0.5]\\leq X-\\mu \\leq calcule[£c*0.5]\\iff -\\frac{calcule[£c*0.5]}{\\sigma}\\leq Y\\leq \\frac{calcule[£c*0.5]}{\\sigma}$.',
        'Sachant que $P(calcule[100-£c*0.5]\\leq X \\leq calcule[100+£c*0.5])=£p$, on en déduit que <font color="red">$P(-\\frac{calcule[£c*0.5]}{\\sigma}\\leq Y\\leq \\frac{calcule[£c*0.5]}{\\sigma})=£p$</font>.'],
      explicationsTexas2: ['$P(calcule[100-£c*0.5]\\leq X \\leq calcule[100+£c*0.5])=£p$ signifie, en utilisant la symétrie de la courbe représentant la densité et l’aire totale sous cette courbe, que $P(X\\leq calcule[100+£c*0.5])=£q$.<br/>De plus $X \\leq calcule[100+£c*0.5]\\iff X-\\mu \\leq calcule[£c*0.5]\\iff Y\\leq \\frac{calcule[£c*0.5]}{\\sigma}$.',
        'Sachant que $P(X \\leq calcule[100-£c*0.5])=£q$, on en déduit que <font color="red">$P(Y\\leq \\frac{calcule[£c*0.5]}{\\sigma})=£q$</font>.'],
      conclusion: ['D’après ce qui précède, on obtient ainsi $\\frac{calcule[£c*0.5]}{\\sigma}\\approx £u$ d’où $\\sigma\\approx \\frac{calcule[£c*0.5]}{£u}$ c’est-à-dire $\\sigma\\approx £b$.',
        'Grâce au résultat <font color="red">$P(-£u\\leq Y\\leq £u)\\approx £p$</font>, on obtient $\\frac{calcule[£c*0.5]}{\\sigma}\\approx £u$ d’où $\\sigma\\approx \\frac{calcule[£c*0.5]}{£u}$ c’est-à-dire $\\sigma\\approx £b$.'],
      conclusionTexas: ['D’après ce qui précède, on obtient ainsi $\\frac{calcule[£c*0.5]}{\\sigma}\\approx £u$ d’où $\\sigma\\approx \\frac{calcule[£c*0.5]}{£u}$ c’est-à-dire $\\sigma\\approx £b$.',
        'Grâce au résultat <font color="red">$P(Y\\leq £u)\\approx £q$</font>, on obtient $\\frac{calcule[£c*0.5]}{\\sigma}\\approx £u$ d’où $\\sigma\\approx \\frac{calcule[£c*0.5]}{£u}$ c’est-à-dire $\\sigma\\approx £b$.'],
      arrondi: 2, // tolérance pour l’arrondi de l’écart-type
      arrondiU: 3,
      arrondiProba: 3 // ça c’est l’arrondi de la proba donnée dans l’énoncé
    },
    {
      nomSujet: 'Amérique du Nord juin 2014 S', // numéro 4
      consignes: ['Une grande enseigne de cosmétiques lance une nouvelle crème hydratante.',
        'Cette enseigne souhaite vendre la nouvelle crème sous un conditionnement de $calcule[£a*10]$&nbsp;mL et dispose pour ceci de pots de contenance maximale calcule[£a*10+5]&nbsp;mL.',
        'On dit qu’un pot de crème est non conforme s’il contient moins de calcule[£a*10-1]&nbsp;mL de crème.',
        'Plusieurs séries de tests conduisent à modéliser la quantité de crème, exprimée en mL, contenue dans chaque pot par une variable aléatoire $X$ qui suit la loi normale d’espérance $\\mu=calcule[£a*10]$ et d’écart-type $\\sigma$.'],
      // ce qui suit constitue une question prélimimaire qu’on pourra poser ou non (paramètre de la section)
      questComplementaire: ['On sait que $calcule[100*£p]$% des pots sont non conforme.',
        'Soit $Y$ la variable aléatoire égale à $\\frac{X-\\mu}{\\sigma}$.',
        'Déterminer une valeur approchée à $10^{-4}$ près du réel $u$ tel que $P(Y\\leq u)=£p$.'],
      questComplementaireTexas: ['On sait que $calcule[100*£p]$% des pots sont non conforme.',
        'Soit $Y$ la variable aléatoire égale à $\\frac{X-\\mu}{\\sigma}$.',
        'Déterminer une valeur approchée à $10^{-4}$ près du réel $u$ tel que $P(Y\\leq u)=£p$.'],
      infoComplementaire: ['D’après la question précédente, la variable aléatoire $Y=\\frac{X-\\mu}{\\sigma}$ vérifie $P(Y\\leq £u)\\approx £p$.'],
      infoComplementaireTexas: ['D’après la question précédente, la variable aléatoire $Y=\\frac{X-\\mu}{\\sigma}$ vérifie $P(Y\\leq £u)\\approx £p$.'],
      question: 'Sachant que la probabilité que le pot de crème est non conforme vaut $£p$, déterminer la valeur de $\\sigma$.',
      questionComp: 'En utilisant ce résultat et sachant que la probabilité que le pot de crème est non conforme vaut $£p$, déterminer la valeur de $\\sigma$.',
      variables: [['a', '[4;6]'], ['b', '[58;67]', 100]],
      // je récupère les valeurs de mes variables
      valeurs: { mu: '£a*10', sigma: '£b', borneInf: '10*£a-5*£b', borneSup: '10*£a-1', u: '-1/£b', q: '£p' },
      // Pour l’affichage de la courbe, je donne les bornes de l’intervalle que j’affiche
      // le premier cas de figure est celui donné par l’énoncé, le 2nd, celui qui est donné quand on prend une aire à gauche (ce sera parfois la même chose)
      bornesGraph: [['-5', '-1/£b'], ['-5', '-1/£b']],
      // explicationsComp sera affichée qu’on ait la question sur le calcul de sigma directement ou qu’on ait la question intermédiaire
      explicationsComp: ['$X$ suivant une loi normale d’espérance $\\mu$ et d’écart-type $\\sigma$, $Y=\\frac{X-\\mu}{\\sigma}$ suit la loi normale centrée réduite $\\Norm(0;1)$.',
        'A l’aide de la calculatrice, on détermine le réel $u$ tel que $P(Y\\leq u)=£p$.',
        'Dans la partie distributions, choisir "Inverse Normale" (InvN, invNorm ou FracNormale suivant les modèles). On complète alors par la valeur de la probabilité (qui est une aire à gauche), $£p$ dans notre cas et par les valeurs de l’espérance $\\mu=0$ et de l’écart-type $\\sigma=1$ de $Y$.',
        'On obtient ainsi $u\\approx £u$ et on a $P(Y\\leq £u)\\approx £p$ (voir figure ci-contre).'],
      explicationsCompTexas: ['$X$ suivant une loi normale d’espérance $\\mu$ et d’écart-type $\\sigma$, $Y=\\frac{X-\\mu}{\\sigma}$ suit la loi normale centrée réduite $\\Norm(0;1)$.',
        'A l’aide de la calculatrice, on détermine le réel $u$ tel que $P(Y\\leq u)=£q$',
        'Dans la partie distributions, choisir "Inverse Normale" (InvN, invNorm ou FracNormale suivant les modèles). On complète alors par la valeur de la probabilité (qui est une aire à gauche), $£q$ dans notre cas et par les valeurs de l’espérance $\\mu=0$ et de l’écart-type $\\sigma=1$ de $Y$.',
        'On obtient ainsi $u\\approx £u$ et on a $P(Y\\leq £u)\\approx £q$ (voir figure ci-contre).'],
      // explications n’est affichée que dans le cas où on demande sigma
      explications: ['De plus $X \\leq calcule[10*£a-1]\\iff X-\\mu \\leq -1\\iff Y\\leq -\\frac{1}{\\sigma}$.',
        'Sachant que $P(X \\leq calcule[10*£a-1])=£p$, on en déduit que <font color="red">$P(Y\\leq -\\frac{1}{\\sigma})=£p$</font>.'],
      explicationsTexas: ['De plus $X \\leq calcule[10*£a-1]\\iff X-\\mu \\leq -1\\iff Y\\leq -\\frac{1}{\\sigma}$.',
        'Sachant que $P(X \\leq calcule[10*£a-1])=£p$, on en déduit que <font color="red">$P(Y\\leq -\\frac{1}{\\sigma})=£p$</font>.'],
      explicationsComp2: ['$X$ suivant une loi normale d’espérance $\\mu$ et d’écart-type $\\sigma$, $Y=\\frac{X-\\mu}{\\sigma}$ suit la loi normale centrée réduite $\\Norm(0;1)$.',
        'A l’aide de la calculatrice, on détermine le réel $u$ tel que $P(Y\\leq u)=£p$.',
        'On obtient ainsi $u\\approx £u$ et on a <font color="red">$P(Y\\leq £u)\\approx £p$</font> (voir figure ci-contre).'],
      explicationsCompTexas2: ['$X$ suivant une loi normale d’espérance $\\mu$ et d’écart-type $\\sigma$, $Y=\\frac{X-\\mu}{\\sigma}$ suit la loi normale centrée réduite $\\Norm(0;1)$.',
        'A l’aide de la calculatrice, on détermine le réel $u$ tel que $P(Y\\leq u)=£q$.',
        'On obtient ainsi $u\\approx £u$ et on a <font color="red">$P(Y\\leq £u)\\approx £q$</font> (voir figure ci-contre).'],
      explications2: ['On a $X \\leq calcule[10*£a-1]\\iff X-\\mu \\leq -1\\iff Y\\leq -\\frac{1}{\\sigma}$.',
        'Sachant que $P(X \\leq calcule[10*£a-1])=£p$, on en déduit que <font color="red">$P(Y\\leq -\\frac{1}{\\sigma})=£p$</font>.'],
      explicationsTexas2: ['De plus $X \\leq calcule[10*£a-1]\\iff X-\\mu \\leq -1\\iff Y\\leq -\\frac{1}{\\sigma}$.',
        'Sachant que $P(X \\leq calcule[10*£a-1])=£q$, on en déduit que <font color="red">$P(Y\\leq -\\frac{1}{\\sigma})=£q$</font>.'],
      conclusion: ['D’après ce qui précède, on obtient ainsi $-\\frac{1}{\\sigma}\\approx £u$ d’où $\\sigma\\approx -\\frac{1}{£u}$ c’est-à-dire $\\sigma\\approx £b$.',
        'Grâce au résultat <font color="red">$P(Y\\leq £u)\\approx £p$</font>, on obtient $-\\frac{1}{\\sigma}\\approx £u$ d’où $\\sigma\\approx -\\frac{1}{£u}$ c’est-à-dire $\\sigma\\approx £b$.'],
      conclusionTexas: ['D’après ce qui précède, on obtient ainsi $-\\frac{1}{\\sigma}\\approx £u$ d’où $\\sigma\\approx -\\frac{1}{£u}$ c’est-à-dire $\\sigma\\approx £b$.',
        'Grâce au résultat <font color="red">$P(Y\\leq £u)\\approx £q$</font>, on obtient $-\\frac{1}{\\sigma}\\approx £u$ d’où $\\sigma\\approx -\\frac{1}{£u}$ c’est-à-dire $\\sigma\\approx £b$.'],
      arrondi: 2, // tolérance pour l’arrondi de l’écart-type
      arrondiU: 3,
      arrondiProba: 3 // ça c’est l’arrondi de la proba donnée dans l’énoncé
    },
    {
      nomSujet: 'Asie juin 2014 S', // numéro 5
      consignes: ['Le taux d’hématocrite est le pourcentage du volume de globules rouges par rapport au volume total du sang.',
        'On note $X$ la variable aléatoire donnant le taux d’hématocrite d’un adulte choisi au hasard dans la population française.',
        'On admet que cette variable suit une loi normale de moyenne $\\mu=calcule[£a*0.5]$ et d’écart-type $\\sigma$.'],
      // ce qui suit constitue une question prélimimaire qu’on pourra poser ou non (paramètre de la section)
      questComplementaire: ['On sait que $calcule[100*£p]$% de la population a un taux d’hématocrite inférieur à 50.',
        'Soit $Z$ la variable aléatoire égale à $\\frac{X-\\mu}{\\sigma}$.',
        'Déterminer une valeur approchée à $10^{-4}$ près du réel $u$ tel que $P(Z\\leq u)=£p$.'],
      questComplementaireTexas: ['On sait que $calcule[100*£p]$% de la population a un taux d’hématocrite inférieur à 50.',
        'Soit $Z$ la variable aléatoire égale à $\\frac{X-\\mu}{\\sigma}$.',
        'Déterminer une valeur approchée à $10^{-4}$ près du réel $u$ tel que $P(Z\\leq u)=£p$.'],
      infoComplementaire: ['D’après la question précédente, la variable aléatoire $Z=\\frac{X-\\mu}{\\sigma}$ vérifie $P(Z\\leq £u)\\approx £p$.'],
      infoComplementaireTexas: ['D’après la question précédente, la variable aléatoire $Z=\\frac{X-\\mu}{\\sigma}$ vérifie $P(Z\\leq £u)\\approx £p$.'],
      question: 'Sachant que $calcule[£p*100]$% de la population a un taux d’hématocrite inférieur à 50, déterminer la valeur de $\\sigma$.',
      questionComp: 'En utilisant ce résultat et sachant que $calcule[£p*100]$% de la population a un taux d’hématocrite inférieur à 50, déterminer la valeur de $\\sigma$.',
      variables: [['a', '[90;92]'], ['b', '[32;39]', 10]],
      // je récupère les valeurs de mes variables
      valeurs: { mu: '£a*0.5', sigma: '£b', borneInf: '0.5*£a-5*£b', borneSup: '50', u: '(50-0.5*£a)/£b', q: '£p' },
      // Pour l’affichage de la courbe, je donne les bornes de l’intervalle que j’affiche
      // le premier cas de figure est celui donné par l’énoncé, le 2nd, celui qui est donné quand on prend une aire à gauche (ce sera parfois la même chose)
      bornesGraph: [['-5', '(50-0.5*£a)/£b'], ['-5', '(50-0.5*£a)/£b']],
      // explicationsComp sera affichée qu’on ait la question sur le calcul de sigma directement ou qu’on ait la question intermédiaire
      explicationsComp: ['$X$ suivant une loi normale d’espérance $\\mu$ et d’écart-type $\\sigma$, $Z=\\frac{X-\\mu}{\\sigma}$ suit la loi normale centrée réduite $\\Norm(0;1)$.',
        'A l’aide de la calculatrice, on détermine le réel $u$ tel que $P(Z\\leq u)=£p$.',
        'Dans la partie distributions, choisir "Inverse Normale" (InvN, invNorm ou FracNormale suivant les modèles). On complète alors par la valeur de la probabilité (qui est une aire à gauche), $£p$ dans notre cas et par les valeurs de l’espérance $\\mu=0$ et de l’écart-type $\\sigma=1$ de $Z$.',
        'On obtient ainsi $u\\approx £u$ et on a $P(Z\\leq £u)\\approx £p$ (voir figure ci-contre).'],
      explicationsCompTexas: ['$X$ suivant une loi normale d’espérance $\\mu$ et d’écart-type $\\sigma$, $Z=\\frac{X-\\mu}{\\sigma}$ suit la loi normale centrée réduite $\\Norm(0;1)$.',
        'A l’aide de la calculatrice, on détermine le réel $u$ tel que $P(Z\\leq u)=£q$',
        'Dans la partie distributions, choisir "Inverse Normale" (InvN, invNorm ou FracNormale suivant les modèles). On complète alors par la valeur de la probabilité (qui est une aire à gauche), $£q$ dans notre cas et par les valeurs de l’espérance $\\mu=0$ et de l’écart-type $\\sigma=1$ de $Z$.',
        'On obtient ainsi $u\\approx £u$ et on a $P(Z\\leq £u)\\approx £q$ (voir figure ci-contre).'],
      // explications n’est affichée que dans le cas où on demande sigma
      explications: ['De plus $X \\leq 50\\iff X-\\mu \\leq calcule[50-0.5*£a]\\iff Z\\leq \\frac{calcule[50-0.5*£a]}{\\sigma}$.',
        'Sachant que $P(X \\leq 50)=£p$, on en déduit que <font color="red">$P(Z\\leq \\frac{calcule[50-0.5*£a]}{\\sigma})=£p$</font>.'],
      explicationsTexas: ['De plus $X \\leq 50\\iff X-\\mu \\leq calcule[50-0.5*£a]\\iff Z\\leq \\frac{calcule[50-0.5*£a]}{\\sigma}$.',
        'Sachant que $P(X \\leq 50)=£p$, on en déduit que <font color="red">$P(Z\\leq \\frac{calcule[50-0.5*£a]}{\\sigma})=£p$</font>.'],
      explicationsComp2: ['$X$ suivant une loi normale d’espérance $\\mu$ et d’écart-type $\\sigma$, $Z=\\frac{X-\\mu}{\\sigma}$ suit la loi normale centrée réduite $\\Norm(0;1)$.',
        'A l’aide de la calculatrice, on détermine le réel $u$ tel que $P(Z\\leq u)=£p$.',
        'On obtient ainsi $u\\approx £u$ et on a <font color="red">$P(Z\\leq £u)\\approx £p$</font> (voir figure ci-contre).'],
      explicationsCompTexas2: ['$X$ suivant une loi normale d’espérance $\\mu$ et d’écart-type $\\sigma$, $Z=\\frac{X-\\mu}{\\sigma}$ suit la loi normale centrée réduite $\\Norm(0;1)$.',
        'A l’aide de la calculatrice, on détermine le réel $u$ tel que $P(Z\\leq u)=£q$.',
        'On obtient ainsi $u\\approx £u$ et on a <font color="red">$P(Z\\leq £u)\\approx £q$</font> (voir figure ci-contre).'],
      explications2: ['On a $X \\leq 50\\iff X-\\mu \\leq calcule[50-0.5*£a]\\iff Z\\leq \\frac{calcule[50-0.5*£a]}{\\sigma}$.',
        'Sachant que $P(X \\leq 50)=£p$, on en déduit que <font color="red">$P(Z\\leq \\frac{calcule[50-0.5*£a]}{\\sigma})=£p$</font>.'],
      explicationsTexas2: ['De plus $X \\leq 50\\iff X-\\mu \\leq calcule[50-0.5*£a]\\iff Z\\leq \\frac{calcule[50-0.5*£a]}{\\sigma}$.',
        'Sachant que $P(X \\leq 50)=£q$, on en déduit que <font color="red">$P(Z\\leq \\frac{calcule[50-0.5*£a]}{\\sigma})=£q$</font>.'],
      conclusion: ['D’après ce qui précède, on obtient ainsi $\\frac{calcule[50-0.5*£a]}{\\sigma}\\approx £u$ d’où $\\sigma\\approx \\frac{calcule[50-0.5*£a]}{£u}$ c’est-à-dire $\\sigma\\approx £b$.',
        'Grâce au résultat <font color="red">$P(Z\\leq £u)\\approx £p$</font>, on obtient $\\frac{calcule[50-0.5*£a]}{\\sigma}\\approx £u$ d’où $\\sigma\\approx \\frac{calcule[50-0.5*£a]}{£u}$ c’est-à-dire $\\sigma\\approx £b$.'],
      conclusionTexas: ['D’après ce qui précède, on obtient ainsi $\\frac{calcule[50-0.5*£a]}{\\sigma}\\approx £u$ d’où $\\sigma\\approx \\frac{calcule[50-0.5*£a]}{£u}$ c’est-à-dire $\\sigma\\approx £b$.',
        'Grâce au résultat <font color="red">$P(Z\\leq £u)\\approx £q$</font>, on obtient $\\frac{calcule[50-0.5*£a]}{\\sigma}\\approx £u$ d’où $\\sigma\\approx \\frac{calcule[50-0.5*£a]}{£u}$ c’est-à-dire $\\sigma\\approx £b$.'],
      arrondi: 1, // tolérance pour l’arrondi de l’écart-type
      arrondiU: 2,
      arrondiProba: 3 // ça c’est l’arrondi de la proba donnée dans l’énoncé
    },
    {
      nomSujet: 'Antilles septembre 2014 S', // numéro 6
      consignes: ['Une entreprise de jouets en peluche souhaite commercialiser un nouveau produit.',
        'Un cabinet de sondages et d’expertise souhaite savoir quel est le réel intérêt des enfants pour ce jouet.',
        'A la suite d’une étude, il apparaît que pour un enfant de quatre ans, le nombre de jours, noté $J$, où la peluche est son jouet préféré suit une loi normale de paramètres $\\mu=£a$ et d’écart type $\\sigma$.'],
      // ce qui suit constitue une question prélimimaire qu’on pourra poser ou non (paramètre de la section)
      questComplementaire: ['On sait que $P(J\\leq £a+£c)=£p$.',
        'Soit $X$ la variable aléatoire égale à $\\frac{J-\\mu}{\\sigma}$.',
        'Déterminer une valeur approchée à $10^{-3}$ près du réel $u$ tel que $P(X\\leq u)=£p$.'],
      questComplementaireTexas: ['On sait que $P(J\\leq £a+£c)=£p$.',
        'Soit $X$ la variable aléatoire égale à $\\frac{J-\\mu}{\\sigma}$.',
        'Déterminer une valeur approchée à $10^{-3}$ près du réel $u$ tel que $P(X\\leq u)=£p$.'],
      infoComplementaire: ['D’après la question précédente, la variable aléatoire $X=\\frac{J-\\mu}{\\sigma}$ vérifie $P(X\\leq £u)\\approx £p$.'],
      infoComplementaireTexas: ['D’après la question précédente, la variable aléatoire $X=\\frac{J-\\mu}{\\sigma}$ vérifie $P(X\\leq £u)\\approx £p$.'],
      question: 'Sachant que $P(J\\leq calcule[£a+£c])\\approx £p$, déterminer la valeur de $\\sigma$.',
      questionComp: 'En utilisant ce résultat et sachant que $P(J\\leq calcule[£a+£c])=£p$, déterminer la valeur de $\\sigma$.',
      variables: [['a', '[340;360]'], ['b', '[135;145]', 10], ['c', '[20;25]']],
      // je récupère les valeurs de mes variables
      valeurs: { mu: '£a', sigma: '£b', borneInf: '£a-5*£b', borneSup: '£a+£c', u: '£c/£b', q: '£p' },
      // Pour l’affichage de la courbe, je donne les bornes de l’intervalle que j’affiche
      // le premier cas de figure est celui donné par l’énoncé, le 2nd, celui qui est donné quand on prend une aire à gauche (ce sera parfois la même chose)
      bornesGraph: [['-5', '£c/£b'], ['-5', '£c/£b']],
      // explicationsComp sera affichée qu’on ait la question sur le calcul de sigma directement ou qu’on ait la question intermédiaire
      explicationsComp: ['$J$ suivant une loi normale d’espérance $\\mu$ et d’écart-type $\\sigma$, $X=\\frac{J-\\mu}{\\sigma}$ suit la loi normale centrée réduite $\\Norm(0;1)$.',
        'A l’aide de la calculatrice, on détermine le réel $u$ tel que $P(X\\leq u)=£p$.',
        'Dans la partie distributions, choisir "Inverse Normale" (InvN, invNorm ou FracNormale suivant les modèles). On complète alors par la valeur de la probabilité (qui est une aire à gauche), $£p$ dans notre cas et par les valeurs de l’espérance $\\mu=0$ et de l’écart-type $\\sigma=1$ de $X$.',
        'On obtient ainsi $u\\approx £u$ et on a $P(X\\leq £u)\\approx £p$ (voir figure ci-contre).'],
      explicationsCompTexas: ['$J$ suivant une loi normale d’espérance $\\mu$ et d’écart-type $\\sigma$, $X=\\frac{J-\\mu}{\\sigma}$ suit la loi normale centrée réduite $\\Norm(0;1)$.',
        'A l’aide de la calculatrice, on détermine le réel $u$ tel que $P(X\\leq u)=£q$',
        'Dans la partie distributions, choisir "Inverse Normale" (InvN, invNorm ou FracNormale suivant les modèles). On complète alors par la valeur de la probabilité (qui est une aire à gauche), $£q$ dans notre cas et par les valeurs de l’espérance $\\mu=0$ et de l’écart-type $\\sigma=1$ de $X$.',
        'On obtient ainsi $u\\approx £u$ et on a $P(X\\leq £u)\\approx £q$ (voir figure ci-contre).'],
      // explications n’est affichée que dans le cas où on demande sigma
      explications: ['De plus $J \\leq calcule[£a+£c]\\iff J-\\mu \\leq £c\\iff X\\leq \\frac{£c}{\\sigma}$.',
        'Sachant que $P(J \\leq calcule[£a+£c])=£p$, on en déduit que <font color="red">$P(X\\leq \\frac{£c}{\\sigma})=£p$</font>.'],
      explicationsTexas: ['De plus $J \\leq calcule[£a+£c]\\iff J-\\mu \\leq £c\\iff X\\leq \\frac{£c}{\\sigma}$.',
        'Sachant que $P(J \\leq calcule[£a+£c])=£p$, on en déduit que <font color="red">$P(X\\leq \\frac{£c}{\\sigma})=£p$</font>.'],
      explicationsComp2: ['$J$ suivant une loi normale d’espérance $\\mu$ et d’écart-type $\\sigma$, $X=\\frac{J-\\mu}{\\sigma}$ suit la loi normale centrée réduite $\\Norm(0;1)$.',
        'A l’aide de la calculatrice, on détermine le réel $u$ tel que $P(X\\leq u)=£p$.',
        'On obtient ainsi $u\\approx £u$ et on a <font color="red">$P(X\\leq £u)\\approx £p$</font> (voir figure ci-contre).'],
      explicationsCompTexas2: ['$J$ suivant une loi normale d’espérance $\\mu$ et d’écart-type $\\sigma$, $X=\\frac{X-\\mu}{\\sigma}$ suit la loi normale centrée réduite $\\Norm(0;1)$.',
        'A l’aide de la calculatrice, on détermine le réel $u$ tel que $P(X\\leq u)=£q$.',
        'On obtient ainsi $u\\approx £u$ et on a <font color="red">$P(X\\leq £u)\\approx £q$</font> (voir figure ci-contre).'],
      explications2: ['On a $J \\leq calcule[£a+£c]\\iff J-\\mu \\leq £c\\iff X\\leq \\frac{£c}{\\sigma}$.',
        'Sachant que $P(J \\leq calcule[£a+£c])=£p$, on en déduit que <font color="red">$P(X\\leq \\frac{£c}{\\sigma})=£p$</font>.'],
      explicationsTexas2: ['De plus $J \\leq calcule[£a+£c]\\iff J-\\mu \\leq £c\\iff X\\leq \\frac{£c}{\\sigma}$.',
        'Sachant que $P(J \\leq calcule[£a+£c])=£q$, on en déduit que <font color="red">$P(X\\leq \\frac{£c}{\\sigma})=£q$</font>.'],
      conclusion: ['D’après ce qui précède, on obtient ainsi $\\frac{£c}{\\sigma}\\approx £u$ d’où $\\sigma\\approx \\frac{£c}{£u}$ c’est-à-dire $\\sigma\\approx £b$.',
        'Grâce au résultat <font color="red">$P(X\\leq £u)\\approx £p$</font>, on obtient $\\frac{£c}{\\sigma}\\approx £u$ d’où $\\sigma\\approx \\frac{£c}{£u}$ c’est-à-dire $\\sigma\\approx £b$.'],
      conclusionTexas: ['D’après ce qui précède, on obtient ainsi $\\frac{£c}{\\sigma}\\approx £u$ d’où $\\sigma\\approx \\frac{£c}{£u}$ c’est-à-dire $\\sigma\\approx £b$.',
        'Grâce au résultat <font color="red">$P(X\\leq £u)\\approx £q$</font>, on obtient $\\frac{£c}{\\sigma}\\approx £u$ d’où $\\sigma\\approx \\frac{£c}{£u}$ c’est-à-dire $\\sigma\\approx £b$.'],
      arrondi: 1, // tolérance pour l’arrondi de l’écart-type
      arrondiU: 3,
      arrondiProba: 3 // ça c’est l’arrondi de la proba donnée dans l’énoncé
    },
    {
      nomSujet: 'Amérique du Sud novembre 2014 S', // numéro 7
      consignes: ['Un ballon de football est conforme à la réglementation s’il respecte, suivant sa taille, deux conditions à la fois (sur sa masse et sur sa circonférence).',
        'En particulier, un ballon de taille standard est conforme à la réglementation lorsque sa circonférence, exprimée en centimètres, appartient à l’intervalle $[calcule[£a-£c];calcule[£a+£c]]$.',
        'On note Y la variable aléatoire qui, à chaque ballon de taille standard choisi au hasard associe sa circonférence en centimètres.',
        'On admet que Y suit la loi normale d’espérance $£a$ et d’écart type $\\sigma$.'],
      // ce qui suit constitue une question prélimimaire qu’on pourra poser ou non (paramètre de la section)
      questComplementaire: ['On sait que $calcule[£p*100]$% des ballons de taille standard ont une circonférence conforme à la réglementation',
        'Soit $X$ la variable aléatoire égale à $\\frac{Y-\\mu}{\\sigma}$.',
        'Déterminer une valeur approchée à $10^{-3}$ près du réel $u$ tel que $P(-u\\leq X\\leq u)=£p$.'],
      questComplementaireTexas: ['On sait que $calcule[£p*100]$% des ballons de taille standard ont une circonférence conforme à la réglementation',
        'Soit $X$ la variable aléatoire égale à $\\frac{Y-\\mu}{\\sigma}$.',
        'Déterminer une valeur approchée à $10^{-3}$ près du réel $u$ tel que $P(X\\leq u)=£p$.'],
      infoComplementaire: ['D’après la question précédente, la variable aléatoire $X=\\frac{Y-\\mu}{\\sigma}$ vérifie $P(-£u\\leq X\\leq £u)\\approx £p$.'],
      infoComplementaireTexas: ['D’après la question précédente, la variable aléatoire $X=\\frac{Y-\\mu}{\\sigma}$ vérifie $P(X\\leq £u)\\approx £q$.'],
      question: 'Sachant que $P(calcule[£a-£c]\\leq Y\\leq calcule[£a+£c])=£p$, déterminer la valeur de $\\sigma$.',
      questionComp: 'En utilisant ce résultat et sachant que $P(calcule[£a-£c]\\leq Y\\leq calcule[£a+£c])=£p$, déterminer la valeur de $\\sigma$.',
      variables: [['a', '[68;70]'], ['b', '[47;53]', 100], ['c', '[1;1]']],
      // je récupère les valeurs de mes variables
      valeurs: { mu: '£a', sigma: '£b', borneInf: '£a-£c', borneSup: '£a+£c', u: '£c/£b', q: '£p+(1-£p)/2' },
      // Pour l’affichage de la courbe, je donne les bornes de l’intervalle que j’affiche
      // le premier cas de figure est celui donné par l’énoncé, le 2nd, celui qui est donné quand on prend une aire à gauche (ce sera parfois la même chose)
      bornesGraph: [['-£c/£b', '£c/£b'], ['-5', '£c/£b']], // explicationsComp sera affichée qu’on ait la question sur le calcul de sigma directement ou qu’on ait la question intermédiaire
      explicationsComp: ['$Y$ suivant une loi normale d’espérance $\\mu$ et d’écart-type $\\sigma$, $X=\\frac{Y-\\mu}{\\sigma}$ suit la loi normale centrée réduite $\\Norm(0;1)$.',
        'A l’aide de la calculatrice, on détermine le réel $u$ tel que $P(-u\\leq X\\leq u)=£p$.',
        'Dans la partie distributions, choisir "Inverse Normale" (InvN, invNorm ou FracNormale suivant les modèles). On complète alors par la valeur de la probabilité (qui est une aire centrée), $£p$ dans notre cas et par les valeurs de l’espérance $\\mu=0$ et de l’écart-type $\\sigma=1$ de $X$.',
        'On obtient ainsi $u\\approx £u$ et on a $P(-£u\\leq X\\leq £u)\\approx £p$ (voir figure ci-contre).'],
      explicationsCompTexas: ['$Y$ suivant une loi normale d’espérance $\\mu$ et d’écart-type $\\sigma$, $X=\\frac{Y-\\mu}{\\sigma}$ suit la loi normale centrée réduite $\\Norm(0;1)$.',
        'A l’aide de la calculatrice, on détermine le réel $u$ tel que $P(X\\leq u)=£q$',
        'Dans la partie distributions, choisir "Inverse Normale" (InvN, invNorm ou FracNormale suivant les modèles). On complète alors par la valeur de la probabilité (qui est une aire à gauche), $£q$ dans notre cas et par les valeurs de l’espérance $\\mu=0$ et de l’écart-type $\\sigma=1$ de $X$.',
        'On obtient ainsi $u\\approx £u$ et on a $P(X\\leq £u)\\approx £q$ (voir figure ci-contre).'],
      // explications n’est affichée que dans le cas où on demande sigma
      explications: ['De plus $calcule[£a-£c]\\leq Y \\leq calcule[£a+£c]\\iff -£c\\leq Y-\\mu \\leq £c\\iff -\\frac{£c}{\\sigma}\\leq X\\leq \\frac{£c}{\\sigma}$.',
        'Sachant que $P(calcule[£a-£c]\\leq Y \\leq calcule[£a+£c])=£p$, on en déduit que <font color="red">$P(-\\frac{£c}{\\sigma}\\leq X\\leq \\frac{£c}{\\sigma})=£p$</font>.'],
      explicationsTexas: ['De plus $calcule[£a-£c]\\leq Y \\leq calcule[£a+£c]\\iff -£c\\leq Y-\\mu \\leq £c\\iff -\\frac{£c}{\\sigma}\\leq X\\leq \\frac{£c}{\\sigma}$.',
        'Sachant que $P(calcule[£a-£c]\\leq Y \\leq calcule[£a+£c])=£p$, on en déduit que $P(-\\frac{£c}{\\sigma}\\leq X\\leq \\frac{£c}{\\sigma})=£p$ et par symétrie <font color="red">$P(X\\leq \\frac{£c}{\\sigma})=calcule[£p+(1-£p)/2]$</font>.'],
      explicationsComp2: ['$Y$ suivant une loi normale d’espérance $\\mu$ et d’écart-type $\\sigma$, $X=\\frac{Y-\\mu}{\\sigma}$ suit la loi normale centrée réduite $\\Norm(0;1)$.',
        'A l’aide de la calculatrice, on détermine le réel $u$ tel que $P(-u\\leq X\\leq u)=£p$.',
        'On obtient ainsi $u\\approx £u$ et on a <font color="red">$P(-£u\\leq X\\leq £u)\\approx £p$</font> (voir figure ci-contre).'],
      explicationsCompTexas2: ['$Y$ suivant une loi normale d’espérance $\\mu$ et d’écart-type $\\sigma$, $X=\\frac{Y-\\mu}{\\sigma}$ suit la loi normale centrée réduite $\\Norm(0;1)$.',
        'A l’aide de la calculatrice, on détermine le réel $u$ tel que $P(X\\leq u)=£q$.',
        'On obtient ainsi $u\\approx £u$ et on a <font color="red">$P(X\\leq £u)\\approx £q$</font> (voir figure ci-contre).'],
      explications2: ['On a $calcule[£a-£c]\\leq Y \\leq calcule[£a+£c]\\iff -£c\\leq Y-\\mu \\leq £c\\iff -\\frac{£c}{\\sigma}\\leq X\\leq \\frac{£c}{\\sigma}$.',
        'Sachant que $P(calcule[£a-£c]\\leq Y \\leq calcule[£a+£c])=£p$, on en déduit que <font color="red">$P(-\\frac{£c}{\\sigma}\\leq X\\leq \\frac{£c}{\\sigma})=£p$</font>.'],
      explicationsTexas2: ['$P(calcule[£a-£c]\\leq Y \\leq calcule[£a+£c])=£p$ signifie, en utilisant la symétrie de la courbe représentant la densité et l’aire totale sous cette courbe, que $P(Y\\leq calcule[£a+£c])=£q$.<br/>De plus $Y \\leq calcule[£a+£c]\\iff Y-\\mu \\leq £c\\iff X\\leq \\frac{£c}{\\sigma}$.',
        'Sachant que $P(Y \\leq calcule[£a+£c])=£q$, on en déduit que <font color="red">$P(X\\leq \\frac{calcule[£c*0.1]}{\\sigma})=£q$</font>.'],
      conclusion: ['D’après ce qui précède, on obtient ainsi $\\frac{£c}{\\sigma}\\approx £u$ d’où $\\sigma\\approx \\frac{£c}{£u}$ c’est-à-dire $\\sigma\\approx £b$.',
        'Grâce au résultat <font color="red">$P(-£u\\leq X\\leq £u)\\approx £p$</font>, on obtient $\\frac{£c}{\\sigma}\\approx £u$ d’où $\\sigma\\approx \\frac{£c}{£u}$ c’est-à-dire $\\sigma\\approx £b$.'],
      conclusionTexas: ['D’après ce qui précède, on obtient ainsi $\\frac{£c}{\\sigma}\\approx £u$ d’où $\\sigma\\approx \\frac{£c}{£u}$ c’est-à-dire $\\sigma\\approx £b$.',
        'Grâce au résultat <font color="red">$P(X\\leq £u)\\approx £q$</font>, on obtient $\\frac{£c}{\\sigma}\\approx £u$ d’où $\\sigma\\approx \\frac{£c}{£u}$ c’est-à-dire $\\sigma\\approx £b$.'],
      arrondi: 2, // tolérance pour l’arrondi de l’écart-type
      arrondiU: 3,
      arrondiProba: 3 // ça c’est l’arrondi de la proba donnée dans l’énoncé
    }
  ]
}
