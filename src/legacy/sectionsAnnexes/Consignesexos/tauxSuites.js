export default {
  /* Dans toutes les consignes de ce type d’exercices, £a permet de déclarer la variable a. Il faut alors préciser l’intervalle dans lequel se trouve cette valeur
    Les consignes seront le plus souvent sur plusieurs lignes, d’où l’idée de créer un tableau
    la propriété variables de chaque énoncé est un tableau où chaque élément est un tableau de deux ou trois éléments
    Par exemple ['a','[5;15]'] signifie que a sera un entier de l’intervalle [5;15]
    ['a','[5;15]',100] signifie que a prend une valeur décimale de [0,05;0,15] avec 2 chiffres après la virgule
    */
  sujets: [
    {
      nomSujet: 'Pondichéry avril 2016 ES', // numéro 1
      consignes: ['En janvier 2016, une personne se décide à acheter un scooter coûtant £a euros sans apport personnel. Le vendeur lui propose un crédit à la consommation d’un montant de £a euros, au taux mensuel de calcule[£b]%.',
        'Par ailleurs, la mensualité fixée à calcule[50*£c] euros est versée par l’emprunteur à l’organisme de crédit le 25 de chaque mois.',
        'Ainsi, le capital restant dû augmente de calcule[£b]% puis baisse de calcule[50*£c] euros.',
        'Le premier versement a lieu le 25 février 2016.',
        'On note $u_n$ le capital restant dû en euros juste après la $n$-ième mensualité ($n$ entier naturel non nul). On convient que $u_0=£a$.',
        'Pour tout entier naturel $n$, on souhaite exprimer $u_{n+1}$ en fonction de $u_n$.',
        'Le faire sous forme simplifiée !'],
      variables: [['a', '[52;59]', 0.01], ['b', '[12;17]', 10], ['c', '[5;7]']],
      questionSuite: 'u_{n+1}',
      // réponses associées
      // u_{n+1} est de la forme au_n+b où a est en position 1 et b en position 2. Le terme général de la suite est en position 3
      // Je donne également le rang initial en position 4 et le terme initial en position 5
      reponseSuite: ['(1+£b/100)u_n-50*£c', '1+£b/100', '-50*£c', 'u_n', '0', '£a'],
      // Correction associée à la proba à calculer
      correctionSuite: ['Le capital restant dû augmente de $calcule[£b]$% par mois, ce qui donne $\\left(1+\\frac{calcule[£b]}{100}\\right)u_n=calcule[1+£b/100]u_n$.',
        'De plus ce capital baisse de calcule[50*£c] euros, on obtient donc :',
        '$u_{n+1}=calcule[1+£b/100]u_n-calcule[50*£c]$.']
    },
    {
      nomSujet: 'Liban mai 2016 ES', // numéro 2
      consignes: ['L’entreprise PiscinePlus, implantée dans le sud de la France, propose des contrats annuels d’entretien aux propriétaires de piscines privées.',
        'Le patron de cette entreprise remarque que, chaque année, £a% de contrats supplémentaires sont souscrits et £b contrats résiliés. Il se fonde sur ce constat pour estimer le nombre de contrats annuels à venir.',
        'En 2015, l’entreprise PiscinePlus dénombrait £c contrats souscrits.',
        'On modélise la situation par une suite $(u_n)$ où $u_n$ représente le nombre de contrats souscrits auprès de l’entreprise PiscinePlus l’année $2015+n$.',
        'Pour tout entier naturel $n$, on souhaite exprimer $u_{n+1}$ en fonction de $u_n$.',
        'Le faire sous forme simplifiée !'],
      variables: [['a', '[11;17]'], ['b', '[5;8]'], ['c', '[70;80]']],
      questionSuite: 'u_{n+1}',
      // réponses associées
      // u_{n+1} est de la forme au_n+b où a est en position 1 et b en position 2. Le terme général de la suite est en position 3
      // Je donne également le rang initial en position 4 et le terme initial en position 5
      reponseSuite: ['(1+£a/100)u_n-£b', '1+£a/100', '-£b', 'u_n', '0', '£c'],
      // Correction associée à la proba à calculer
      correctionSuite: ['Le nombre de contrats augmente de $£a$% par an, ce qui donne $\\left(1+\\frac{£a}{100}\\right)u_n=calcule[1+£a/100]u_n$.',
        'De plus $£b$ contrats sont résiliés chaque année, on obtient donc :',
        '$u_{n+1}=calcule[1+£a/100]u_n-£b$.']
    },
    {
      nomSujet: 'Amérique du Nord juin 2016 ES', // numéro 3
      consignes: ['Une société propose un service d’abonnement pour jeux vidéo sur téléphone mobile.',
        'Le 1er janvier 2016, on compte £a abonnés.',
        'À partir de cette date, les dirigeants de la société ont constaté que d’un mois sur l’autre, £b% des anciens joueurs se désabonnent mais que, par ailleurs, £c nouvelles personnes s’abonnent.',
        'On modélise cette situation par une suite numérique $(u_n)$ où $u_n$ représente le nombre de milliers d’abonnés au bout de $n$ mois après le 1er janvier 2016.',
        'Pour tout entier naturel $n$, on souhaite exprimer $u_{n+1}$ en fonction de $u_n$.',
        'Le faire sous forme simplifiée !'],
      variables: [['a', '[7;10]', 0.002], ['b', '[5;8]'], ['c', '[6;9]', 0.001]],
      questionSuite: 'u_{n+1}',
      // réponses associées
      // u_{n+1} est de la forme au_n+b où a est en position 1 et b en position 2. Le terme général de la suite est en position 3
      // Je donne également le rang initial en position 4 et le terme initial en position 5
      reponseSuite: ['(1-£b/100)u_n+£c/1000', '1-£b/100', '£c/1000', 'u_n', '0', '£a/1000'],
      // Correction associée à la proba à calculer
      correctionSuite: ['Le nombre d’abonnements diminue de $£b$% par mois, ce qui donne $\\left(1-\\frac{£b}{100}\\right)u_n=calcule[1-£b/100]u_n$.',
        'De plus $£c$ nouvelles personnes s’abonnent chaque mois, on obtient donc :',
        '$u_{n+1}=calcule[1-£b/100]u_n+calcule[£c/1000]$.']
    },
    {
      nomSujet: 'Métropole juin 2016 ES', // numéro 4
      consignes: ['Un loueur de voitures dispose au 1er mars 2015 d’un total de £a voitures pour l’Europe.',
        'Afin d’entretenir son parc, il décide de revendre, au 1er mars de chaque année, £b% de son parc automobile et d’acheter £c voitures neuves.',
        'On modélise le nombre de voitures de l’agence à l’aide d’une suite $(u_n)$ où $u_n$ est le nombre de voitures présentes dans le parc automobile au 1er mars de l’année $2015+n$.',
        'Pour tout entier naturel $n$, on souhaite exprimer $u_{n+1}$ en fonction de $u_n$.',
        'Le faire sous forme simplifiée !'],
      variables: [['a', '[9;15]', 0.001], ['b', '[5;7]', 0.2], ['c', '[5;8]', 0.002]],
      questionSuite: 'u_{n+1}',
      // réponses associées
      // u_{n+1} est de la forme au_n+b où a est en position 1 et b en position 2. Le terme général de la suite est en position 3
      // Je donne également le rang initial en position 4 et le terme initial en position 5
      reponseSuite: ['(1-£b/100)u_n+£c', '1-£b/100', '£c', 'u_n', '0', '£a'],
      // Correction associée à la proba à calculer
      correctionSuite: ['Il revend $£b$% de son parc chaque année, ce qui donne $\\left(1-\\frac{£b}{100}\\right)u_n=calcule[1-£b/100]u_n$.',
        'De plus il achète $£c$ voitures neuves, on obtient donc :',
        '$u_{n+1}=calcule[1-£b/100]u_n+calcule[£c]$.']
    },
    {
      nomSujet: 'Asie juin 2016 ES', // numéro 5
      consignes: ['Le 1er septembre 2015, un ensemble scolaire compte £a élèves.',
        'Une étude statistique interne a montré que chaque 1er septembre :',
        '- £b% de l’effectif quitte l’établissement;',
        '- £c nouveaux élèves s’inscrivent.',
        'On cherche à modéliser cette situation par une suite $(u_n)$ où, pour tout entier naturel $n$, $u_n$ représente le nombre d’élèves le 1er septembre de l’année $2015+n$.',
        'Pour tout entier naturel $n$, on souhaite exprimer $u_{n+1}$ en fonction de $u_n$.',
        'Le faire sous forme simplifiée !'],
      variables: [['a', '[28;35]', 0.01], ['b', '[8;12]'], ['c', '[4;7]', 0.02]],
      questionSuite: 'u_{n+1}',
      // réponses associées
      // u_{n+1} est de la forme au_n+b où a est en position 1 et b en position 2. Le terme général de la suite est en position 3
      // Je donne également le rang initial en position 4 et le terme initial en position 5
      reponseSuite: ['(1-£b/100)u_n+£c', '1-£b/100', '£c', 'u_n', '0', '£a'],
      // Correction associée à la proba à calculer
      correctionSuite: ['Chaque 1er septembre £b% de l’effectif quitte l’établissement, ce qui donne $\\left(1-\\frac{£b}{100}\\right)u_n=calcule[1-£b/100]u_n$.',
        'De plus £c nouveaux élèves s’inscrivent, on obtient donc :',
        '$u_{n+1}=calcule[1-£b/100]u_n+calcule[£c]$.']
    },
    {
      nomSujet: 'Métropole septembre 2016 ES', // numéro 6
      consignes: ['Le 31 décembre 2015 une forêt comportait £a arbres. Les exploitants de cette forêt prévoient que chaque année, £b% des arbres seront coupés et £c arbres seront plantés.',
        'On modélise le nombre d’arbres de cette forêt par une suite $(u_n)$ où, pour tout entier naturel $n$, $u_n$ est le nombre d’arbres au 31 décembre de l’année $(2015+n)$.',
        'Pour tout entier naturel $n$, on souhaite exprimer $u_{n+1}$ en fonction de $u_n$.',
        'Le faire sous forme simplifiée !'],
      variables: [['a', '[12;18]', 0.01], ['b', '[4;7]'], ['c', '[9;12]', 0.2]],
      questionSuite: 'u_{n+1}',
      // réponses associées
      // u_{n+1} est de la forme au_n+b où a est en position 1 et b en position 2. Le terme général de la suite est en position 3
      // Je donne également le rang initial en position 4 et le terme initial en position 5
      reponseSuite: ['(1-£b/100)u_n+£c', '1-£b/100', '£c', 'u_n', '0', '£a'],
      // Correction associée à la proba à calculer
      correctionSuite: ['Chaque année, £b% des arbres seront coupés, ce qui donne $\\left(1-\\frac{£b}{100}\\right)u_n=calcule[1-£b/100]u_n$.',
        'De plus £c arbres seront plantés, on obtient donc :',
        '$u_{n+1}=calcule[1-£b/100]u_n+calcule[£c]$.']
    },
    {
      nomSujet: 'Antilles septembre 2016 ES', // numéro 7
      consignes: ['Une association confectionne et porte, chaque jour, à domicile des repas à des personnes dépendantes.',
        'En 2015, £a personnes étaient abonnées à ce service.',
        'Pour étudier son développement, cette association a fait une enquête selon laquelle l’évolution peut être modélisée de la façon suivante :',
        '- Chaque année, £b % des abonnements ne sont pas renouvelés.',
        '- Chaque année, on compte £c nouveaux abonnements à ce service.',
        'Cette évolution peut s’étudier à l’aide d’une suite $(u_n)$ où $u_n$ est le nombre d’abonnés pendant l’année $2015+n$.',
        'Pour tout entier naturel $n$, on souhaite exprimer $u_{n+1}$ en fonction de $u_n$.',
        'Le faire sous forme simplifiée !'],
      variables: [['a', '[11;14]', 0.02], ['b', '[4;7]'], ['c', '[15;18]', 0.2]],
      questionSuite: 'u_{n+1}',
      // réponses associées
      // u_{n+1} est de la forme au_n+b où a est en position 1 et b en position 2. Le terme général de la suite est en position 3
      // Je donne également le rang initial en position 4 et le terme initial en position 5
      reponseSuite: ['(1-£b/100)u_n+£c', '1-£b/100', '£c', 'u_n', '0', '£a'],
      // Correction associée à la proba à calculer
      correctionSuite: ['Chaque année, £b % des abonnements ne sont pas renouvelés, ce qui donne $\\left(1-\\frac{£b}{100}\\right)u_n=calcule[1-£b/100]u_n$.',
        'De plus on compte £c nouveaux abonnements à ce service, on obtient donc :',
        '$u_{n+1}=calcule[1-£b/100]u_n+calcule[£c]$.']
    },
    {
      nomSujet: 'Exercice 83 p. 36 manuel Magnard S', // numéro 8
      consignes: ['Cette année, Youssef a décidé de se mettre au sport.',
        'Pour cela, il s’inscrit dans un club où il pratique l’escalade une fois par semaine, ce qui lui fait perdre $£a$% de sa masse par séance.',
        'Par ailleurs, son club d’escalade ayant un partenariat avec la pizzeria d’à côté, il s’y réunit avec ses amis après chaque entraînement et profite de réductions sur les pizzas et les boissons. Cela a des conséquences : $£b$ grammes supplémentaires après chaque repas.',
        'On note $a_n$ sa masse, en kg, après $n$ semaines (donc après $n$ séances et repas). Comme il pèse £ckg au départ, on a $a_0=£c$.',
        'Pour tout entier naturel $n$, on souhaite exprimer $a_{n+1}$ en fonction de $a_n$.',
        'Le faire sous forme simplifiée !'],
      variables: [['a', '[21;27]', 100], ['b', '[5;7]', 0.02], ['c', '[65;75]']],
      questionSuite: 'a_{n+1}',
      // réponses associées
      // u_{n+1} est de la forme au_n+b où a est en position 1 et b en position 2. Le terme général de la suite est en position 3
      // Je donne également le rang initial en position 4 et le terme initial en position 5
      reponseSuite: ['(1-£a/100)a_n+£b/1000', '1-£a/100', '£b/1000', 'a_n', '0', '£c'],
      // Correction associée à la proba à calculer
      correctionSuite: ['Chaque semaine, il perd $£a$% de sa masse, ce qui donne $\\left(1-\\frac{£a}{100}\\right)u_n=calcule[1-£a/100]u_n$.',
        'De plus il prend £b grammes, soit $calcule[£b/1000]$kg, on obtient donc :',
        '$a_{n+1}=calcule[1-£a/100]a_n+calcule[£b/1000]$.']
    }
  ]
}
