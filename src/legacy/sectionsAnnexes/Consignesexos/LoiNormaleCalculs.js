export default {
  /* Dans toutes les consignes de ce type d’exercices, £a permet de déclarer la variable a. Il faut alors préciser l’intervalle dans lequel se trouve cette valeur
    Les consignes seront le plus souvent sur plusieurs lignes, d’où l’idée de créer un tableau
    la propriété variables de chaque énoncé est un tableau où chaque élément est un tableau de deux ou trois éléments
    Par exemple ['a','[5;15]'] signifie que a sera un entier de l’intervalle [5;15]
    ['a','[5;15]',100] signifie que a prend une valeur décimale de [0,05;0,15] avec 2 chiffres après la virgule
    */
  sujets: [
    {
      nomSujet: 'Amérique du Nord juin 2016 S', // numéro 1
      consignes: ['Une entreprise fabrique des billes en bois sphériques grâce à une machine de production.',
        'L’entreprise considère qu’une bille peut être vendue uniquement lorsque son diamètre est compris entre $calcule[£a-£c*0.1]$&nbsp;cm et $calcule[£a+£c*0.1]$&nbsp;cm.',
        'Une étude statistique conduit à modéliser le diamètre d’une bille prélevée au hasard dans la production de la machine par une variable aléatoire $X$ qui suit une loi normale d’espérance $\\mu=£a$ et d’écart-type $\\sigma=calcule[5*£b]$.'],
      question: ['La probabilité qu’une bille produite par la machine soit vendable vaut &1&.'],
      variables: [['a', '[1;3]'], ['b', '[12;17]', 1000], ['c', '[1;2]']],
      // je récupère les valeurs de mes variables
      valeurs: { mu: '£a', sigma: '£b*5', borneInf: ['£a-£c*0.1'], borneSup: ['£a+£c*0.1'] },
      // calculatrice est la phrase qui explique la marche à suivre pour effectuer le calcul à la calculatrice
      calculatrice: ['On utilise la calculatrice pour effectuer ce calcul : dans la partie distributions, choisir "loi Normale Cumulative" (Ncd, cdf ou FRep suivant les modèles). On complète ensuite par les bornes de l’intervalle ($calcule[£a-£c*0.1]$ puis $calcule[£a+£c*0.1]$) et par les valeurs de l’espérance $\\mu=£a$ et de l’écart-type $\\sigma=calcule[£b*5]$.'],
      // On donne alors le résultat obtenu
      reponse: ['On obtient ainsi $P(calcule[£a-£c*0.1]\\leq X\\leq calcule[£a+£c*0.1])\\approx £r$.'],
      arrondi: 4
    },
    {
      nomSujet: 'Métropole septembre 2016 S', // numéro 2
      consignes: ['Une personne est dite en hypoglycémie si sa glycémie à jeun est inférieure à 60&nbsp;mg.dL$^{-1}$ et elle est en hyperglycémie si sa glycémie à jeun est supérieure à 110&nbsp;mg.dL$^{-1}$.',
        'La glycémie à jeun est considérée comme «normale» si elle est comprise entre 70&nbsp;mg.dL$^{-1}$ et 110&nbsp;mg.dL$^{-1}$.',
        'On modélise la glycémie à jeun, exprimée en mg.dL$^{-1}$, d’un adulte d’une population donnée, par une variable aléatoire $X$ qui suit une loi normale d’espérance $£a$ et d’écart-type £b.'],
      question: ['La probabilité que la personne choisie soit en hypoglycémie vaut &1&.',
        'La probabilité que la personne choisie soit en hyperglycémie vaut &1&.',
        'La probabilité que la personne ait une glycémie considérée comme «normale» vaut &1&.'],
      variables: [['a', '[86;94]'], ['b', '[11;13]']],
      // je récupère les valeurs de mes variables
      valeurs: { mu: '£a', sigma: '£b', borneInf: ['£a-10*£b', '110', '70'], borneSup: ['60', '£a+10*£b', '110'] },
      // calculatrice est la phrase qui explique la marche à suivre pour effectuer le calcul à la calculatrice
      calculatrice: ['On utilise la calculatrice pour effectuer ce calcul : dans la partie distributions, choisir "loi Normale Cumulative" (Ncd, cdf ou FRep suivant les modèles). On complète ensuite par les bornes de l’intervalle ($-10000$ ou toute autre valeur qui remplacera $-\\infty$ puis $60$) et par les valeurs de l’espérance $\\mu=£a$ et de l’écart-type $\\sigma=£b$.',
        'On utilise la calculatrice pour effectuer ce calcul : dans la partie distributions, choisir "loi Normale Cumulative" (Ncd, cdf ou FRep suivant les modèles). On complète ensuite par les bornes de l’intervalle ($110$ puis $10000$ ou toute autre valeur qui remplacera $+\\infty$) et par les valeurs de l’espérance $\\mu=£a$ et de l’écart-type $\\sigma=£b$.',
        'On utilise la calculatrice pour effectuer ce calcul : dans la partie distributions, choisir "loi Normale Cumulative" (Ncd, cdf ou FRep suivant les modèles). On complète ensuite par les bornes de l’intervalle ($70$ puis $110$) et par les valeurs de l’espérance $\\mu=£a$ et de l’écart-type $\\sigma=£b$.'],
      // On donne alors le résultat obtenu
      reponse: ['On obtient ainsi $P(X\\leq 60)\\approx £r$.',
        'On obtient ainsi $P(X\\geq 110)\\approx £r$.',
        'On obtient ainsi $P(70\\leq X\\leq 110)\\approx £r$.'],
      arrondi: 3
    },
    {
      nomSujet: 'Pondichéry mai 2015 S', // numéro 3
      consignes: ['Des études statistiques ont permis de modéliser la durée de vie, en mois, d’un type de lave-vaisselle par une variable aléatoire $X$ suivant une loi normale $N(\\mu;\\sigma^2)$ de moyenne $\\mu=£a$ et d’écart-type $\\sigma=£b$.'],
      question: ['La probabilité que la durée de vie du lave-vaisselle soit comprise entre £c et £d ans vaut &1&.',
        'La  probabilité que le lave-vaisselle ait une durée de vie supérieure à £e ans vaut &1&.'],
      variables: [['a', '[80;90]'], ['b', '[190;210]', 10], ['c', '[1;3]'], ['d', '[4;5]'], ['e', '[7;10]']],
      // je récupère les valeurs de mes variables
      valeurs: { mu: '£a', sigma: '£b', borneInf: ['£c*12', '£e*12'], borneSup: ['£d*12', '£a+10*£b'] },
      // calculatrice est la phrase qui explique la marche à suivre pour effectuer le calcul à la calculatrice
      calculatrice: ['On utilise la calculatrice pour effectuer ce calcul : dans la partie distributions, choisir "loi Normale Cumulative" (Ncd, cdf ou FRep suivant les modèles). On complète ensuite par les bornes de l’intervalle ($calcule[£c*12]$ puis $calcule[£d*12]$, attention à bien convertir en mois) et par les valeurs de l’espérance $\\mu=£a$ et de l’écart-type $\\sigma=£b$.',
        'On utilise la calculatrice pour effectuer ce calcul : dans la partie distributions, choisir "loi Normale Cumulative" (Ncd, cdf ou FRep suivant les modèles). On complète ensuite par les bornes de l’intervalle ($calcule[£e*12]$ - attention à bien convertir en mois - puis $10000$ ou toute autre valeur qui remplacera $+\\infty$) et par les valeurs de l’espérance $\\mu=£a$ et de l’écart-type $\\sigma=£b$.'],
      // On donne alors le résultat obtenu
      reponse: ['On obtient ainsi $P(calcule[£c*12]\\leq X\\leq calcule[£d*12])\\approx £r$.',
        'On obtient ainsi $P(X\\geq calcule[£e*12])\\approx £r$.'],
      arrondi: 3
    },
    {
      nomSujet: 'Centres étrangers 2015 S', // numéro 4
      consignes: ['Un magasin de bricolage vend des cadenas. D’après une étude statistique faite sur plusieurs mois, on admet que le nombre $X$ de cadenas vendus par mois peut être modélisé par une variable aléatoire qui suit la loi normale de moyenne $\\mu=calcule[10*£a]$ et d’écart-type $\\sigma=£b$.'],
      question: ['La probabilité que le nombre de cadenas vendus soit inférieur à $calcule[10*£a-£c]$ vaut &1&.',
        'La probabilité que le nombre de cadenas vendus soit compris entre $calcule[10*£a-£c]$ et $calcule[10*£a+£d]$ vaut &1&.',
        'La probabilité que le nombre de cadenas vendus soit supérieur à $calcule[10*£a+£d]$ vaut &1&.'],
      variables: [['a', '[71;79]'], ['b', '[21;27]'], ['c', '[5;10]'], ['d', '[5;10]']],
      // je récupère les valeurs de mes variables
      valeurs: { mu: '10*£a', sigma: '£b', borneInf: ['10*£a-10*£b', '10*£a-£c', '10*£a+£d'], borneSup: ['10*£a-£c', '10*£a+£d', '10*£a+10*£b'] },
      // calculatrice est la phrase qui explique la marche à suivre pour effectuer le calcul à la calculatrice
      calculatrice: ['On utilise la calculatrice pour effectuer ce calcul : dans la partie distributions, choisir "loi Normale Cumulative" (Ncd, cdf ou FRep suivant les modèles). On complète ensuite par les bornes de l’intervalle ($-10000$ ou toute autre valeur qui remplacera $-\\infty$ puis $calcule[10*£a-£c]$) et par les valeurs de l’espérance $\\mu=calcule[10*£a]$ et de l’écart-type $\\sigma=£b$.',
        'On utilise la calculatrice pour effectuer ce calcul : dans la partie distributions, choisir "loi Normale Cumulative" (Ncd, cdf ou FRep suivant les modèles). On complète ensuite par les bornes de l’intervalle ($calcule[10*£a-£c]$ puis $calcule[10*£a+£d]$) et par les valeurs de l’espérance $\\mu=calcule[10*£a]$ et de l’écart-type $\\sigma=£b$.',
        'On utilise la calculatrice pour effectuer ce calcul : dans la partie distributions, choisir "loi Normale Cumulative" (Ncd, cdf ou FRep suivant les modèles). On complète ensuite par les bornes de l’intervalle ($calcule[10*£a+£d]$ puis $10000$ ou toute autre valeur qui remplacera $+\\infty$) et par les valeurs de l’espérance $\\mu=calcule[10*£a]$ et de l’écart-type $\\sigma=£b$.'],
      // On donne alors le résultat obtenu
      reponse: ['On obtient ainsi $P(X\\leq calcule[10*£a-£c])\\approx £r$.',
        'On obtient ainsi $P(calcule[10*£a-£c]\\leq X\\leq calcule[10*£a+£d])\\approx £r$.',
        'On obtient ainsi $P(X\\geq calcule[10*£a+£d])\\approx £r$.'],
      arrondi: 3
    },
    {
      nomSujet: 'Pondichéry 2017 S', // numéro 5
      consignes: ['On note $X$ la variable aléatoire donnant la teneur en cacao, exprimée en pourcentage, d’une tablette de 100&nbsp;g de chocolat commercialisable. On admet que $X$ suit la loi normale d’espérance $\\mu=£a$ et d’écart type $\\sigma=£b$.'],
      question: ['La probabilité que la teneur en cacao soit comprise entre $calcule[£a-£b-1]$% et $calcule[£a+£b+1]$% vaut &1&.',
        'La probabilité que la teneur en cacao soit supérieure à $calcule[£a+£b+1]$% vaut &1&.'],
      variables: [['a', '[82;87]'], ['b', '[2;4]']],
      // je récupère les valeurs de mes variables
      valeurs: { mu: '£a', sigma: '£b', borneInf: ['£a-£b-1', '£a+£b+1'], borneSup: ['£a+£b+1', '£a+10*£b'] },
      // calculatrice est la phrase qui explique la marche à suivre pour effectuer le calcul à la calculatrice
      calculatrice: ['On utilise la calculatrice pour effectuer ce calcul : dans la partie distributions, choisir "loi Normale Cumulative" (Ncd, cdf ou FRep suivant les modèles). On complète ensuite par les bornes de l’intervalle ($calcule[£a-£b-1]$ puis $calcule[£a+£b+1]$) et par les valeurs de l’espérance $\\mu=£a$ et de l’écart-type $\\sigma=£b$.',
        'On utilise la calculatrice pour effectuer ce calcul : dans la partie distributions, choisir "loi Normale Cumulative" (Ncd, cdf ou FRep suivant les modèles). On complète ensuite par les bornes de l’intervalle ($calcule[£a+£b+1]$ puis $10000$ ou toute autre valeur qui remplacera $+\\infty$) et par les valeurs de l’espérance $\\mu=£a$ et de l’écart-type $\\sigma=£b$.'],
      // On donne alors le résultat obtenu
      reponse: ['On obtient ainsi $P(calcule[£a-£b-1]\\leq X\\leq calcule[£a+£b+1])\\approx £r$.',
        'On obtient ainsi $P(X\\geq calcule[£a+£b+1])\\approx £r$.'],
      arrondi: 4
    },
    {
      nomSujet: 'Liban 2014 S', // numéro 6
      consignes: ['Un élève utilise le vélo pour se rendre à son lycée.',
        'On modélise son temps de parcours, exprimé en minutes, entre son domicile et son lycée par une variable aléatoire $T$ qui suit la loi normale d’espérance $\\mu=£a$ et d’écart-type $\\sigma=£b$.'],
      question: ['La probabilité que cet élève mette moins de $calcule[£a-£c]$ minutes pour se rendre au lycée vaut &1&.',
        'La probabilité que cet élève mette entre $calcule[£a-£c]$ et $calcule[£a+£d]$ minutes pour se rendre au lycée vaut &1&.'],
      variables: [['a', '[15;22]'], ['b', '[9;15]', 10], ['c', '[2;3]'], ['d', '[2;3]']],
      // je récupère les valeurs de mes variables
      valeurs: { mu: '£a', sigma: '£b', borneInf: ['£a-10*£b', '£a-£c'], borneSup: ['£a-£c', '£a+£d'] },
      // calculatrice est la phrase qui explique la marche à suivre pour effectuer le calcul à la calculatrice
      calculatrice: ['On utilise la calculatrice pour effectuer ce calcul : dans la partie distributions, choisir "loi Normale Cumulative" (Ncd, cdf ou FRep suivant les modèles). On complète ensuite par les bornes de l’intervalle ($-10000$ ou toute autre valeur qui remplacera $-\\infty$ puis $calcule[£a-£c]$) et par les valeurs de l’espérance $\\mu=£a$ et de l’écart-type $\\sigma=£b$.',
        'On utilise la calculatrice pour effectuer ce calcul : dans la partie distributions, choisir "loi Normale Cumulative" (Ncd, cdf ou FRep suivant les modèles). On complète ensuite par les bornes de l’intervalle ($calcule[£a-£c]$ puis $calcule[£a+£d]$) et par les valeurs de l’espérance $\\mu=£a$ et de l’écart-type $\\sigma=£b$.'],
      // On donne alors le résultat obtenu
      reponse: ['On obtient ainsi $P(T\\leq calcule[£a-£c])\\approx £r$.',
        'On obtient ainsi $P(calcule[£a-£c]\\leq T\\leq calcule[£a+£d])\\approx £r$.'],
      arrondi: 3
    },
    {
      nomSujet: 'Amérique du Nord 2014 S', // numéro 7
      consignes: ['Une grande enseigne de cosmétiques lance une nouvelle crème hydratante.',
        'Cette enseigne souhaite vendre la nouvelle crème sous un conditionnement de calcule[£a*5]&nbsp;mL et dispose pour ceci de pots de contenance maximale calcule[£a*5+5]&nbsp;mL.',
        'On dit qu’un pot de crème est non conforme s’il contient moins de calcule[£a*5-£c]&nbsp;mL de crème.',
        'Plusieurs séries de tests conduisent à modéliser la quantité de crème, exprimée en mL, contenue dans chaque pot par une variable aléatoire $X$ qui suit la loi normale d’espérance $\\mu=calcule[£a*5]$ et d’écart-type $\\sigma=£b$.'],
      question: ['La probabilité qu’un pot de crème soit non conforme vaut &1&.'],
      variables: [['a', '[10;13]'], ['b', '[9;12]', 10], ['c', '[1;2]']],
      // je récupère les valeurs de mes variables
      valeurs: { mu: '5*£a', sigma: '£b', borneInf: ['5*£a-10*£b'], borneSup: ['5*£a-£c'] },
      // calculatrice est la phrase qui explique la marche à suivre pour effectuer le calcul à la calculatrice
      calculatrice: ['On utilise la calculatrice pour effectuer ce calcul : dans la partie distributions, choisir "loi Normale Cumulative" (Ncd, cdf ou FRep suivant les modèles). On complète ensuite par les bornes de l’intervalle ($-10000$ ou toute autre valeur qui remplacera $-\\infty$ puis $calcule[5*£a-£c]$) et par les valeurs de l’espérance $\\mu=calcule[5*£a]$ et de l’écart-type $\\sigma=£b$.'],
      // On donne alors le résultat obtenu
      reponse: ['On obtient ainsi $P(X\\leq calcule[5*£a-£c])\\approx £r$.'],
      arrondi: 3
    },
    {
      nomSujet: 'Antilles 2014 S', // numéro 8
      consignes: ['La masse d’une huître peut être modélisée par une variable aléatoire $X$ suivant la loi normale de moyenne $\\mu=£a$ et d’écart-type $\\sigma=£b$.'],
      question: ['La probabilité que l’huître prélevée dans la production d’un ostréiculteur ait une masse comprise entre $calcule[£a-£c]$&nbsp;g et $calcule[£a+£d]$&nbsp;g vaut &1&.',
        'La probabilité que l’huître prélevée dans la production d’un ostréiculteur ait une masse inférieure à $calcule[£a-£c]$&nbsp;g vaut &1&.'],
      variables: [['a', '[87;93]'], ['b', '[17;22]', 10], ['c', '[2;3]'], ['d', '[2;3]']],
      // je récupère les valeurs de mes variables
      valeurs: { mu: '£a', sigma: '£b', borneInf: ['£a-£c', '£a-10*£b'], borneSup: ['£a+£d', '£a-£c'] },
      // calculatrice est la phrase qui explique la marche à suivre pour effectuer le calcul à la calculatrice
      calculatrice: ['On utilise la calculatrice pour effectuer ce calcul : dans la partie distributions, choisir "loi Normale Cumulative" (Ncd, cdf ou FRep suivant les modèles). On complète ensuite par les bornes de l’intervalle ($calcule[£a-£c]$ puis $calcule[£a+£d]$) et par les valeurs de l’espérance $\\mu=£a$ et de l’écart-type $\\sigma=£b$.',
        'On utilise la calculatrice pour effectuer ce calcul : dans la partie distributions, choisir "loi Normale Cumulative" (Ncd, cdf ou FRep suivant les modèles). On complète ensuite par les bornes de l’intervalle ($-10000$ ou toute autre valeur qui remplacera $-\\infty$ puis $calcule[£a-£c]$) et par les valeurs de l’espérance $\\mu=£a$ et de l’écart-type $\\sigma=£b$.'],
      // On donne alors le résultat obtenu
      reponse: ['On obtient ainsi $P(calcule[£a-£c]\\leq X\\leq calcule[£a+£d])\\approx £r$.',
        'On obtient ainsi $P(X\\leq calcule[£a-£c])\\approx £r$.'],
      arrondi: 4
    },
    {
      nomSujet: 'Asie 2014 S', // numéro 9
      consignes: ['Le taux d’hématocrite est le pourcentage du volume de globules rouges par rapport au volume total du sang.',
        'On note $X$ la variable aléatoire donnant le taux d’hématocrite d’un adulte choisi au hasard dans la population française.',
        'On admet que cette variable suit une loi normale de moyenne $\\mu=£a$ et d’écart-type $\\sigma=£b$.',
        'Pour un cycliste, une règle prévoit l’exclusion temporaire d’un coureur qui afficherait un taux d’hématocrite supérieur à 50&nbsp;%.'],
      question: ['La probabilité qu’un cycliste non dopé à l’EPO (produit responsable de l’augmentation du taux d’hématocrite) puisse concourir vaut &1&.'],
      variables: [['a', '[451;459]', 10], ['b', '[34;38]', 10]],
      // je récupère les valeurs de mes variables
      valeurs: { mu: '£a', sigma: '£b', borneInf: ['£a-10*£b'], borneSup: ['50'] },
      // calculatrice est la phrase qui explique la marche à suivre pour effectuer le calcul à la calculatrice
      calculatrice: ['On utilise la calculatrice pour effectuer ce calcul : dans la partie distributions, choisir "loi Normale Cumulative" (Ncd, cdf ou FRep suivant les modèles). On complète ensuite par les bornes de l’intervalle ($-10000$ ou toute autre valeur qui remplacera $-\\infty$ puis 50) et par les valeurs de l’espérance $\\mu=£a$ et de l’écart-type $\\sigma=£b$.'],
      // On donne alors le résultat obtenu
      reponse: ['On obtient ainsi $P(X\\leq 50)\\approx £r$.'],
      arrondi: 3
    },
    {
      nomSujet: 'Amérique du Sud 2014 S', // numéro 10
      consignes: ['Un ballon de football est conforme à la réglementation s’il respecte, suivant sa taille, deux conditions à la fois (sur sa masse et sur sa circonférence).',
        'En particulier, un ballon de taille standard est conforme à la réglementation lorsque sa masse, exprimée en grammes, appartient à l’intervalle $[calcule[5*£a];calcule[5*£b]]$ et sa circonférence, exprimée en centimètres, appartient à l’intervalle $[£c;calcule[£c+2]]$.',
        'On note $X$ la variable aléatoire qui, à chaque ballon de taille standard choisi au hasard dans une entreprise de fabrication, associe sa masse en grammes.',
        'On admet que $X$ suit la loi normale d’espérance $calcule[(5*(£a+£b)/2)]$ et d’écart type $£d$.'],
      question: ['La probabilité qu’un ballon choisi au hasard dans la production soit d’une masse conforme à la réglementation vaut &1&.'],
      variables: [['a', '[81;83]'], ['b', '[89;91]'], ['c', '[67;69]'], ['d', '[9;11]']],
      // je récupère les valeurs de mes variables
      valeurs: { mu: '5*(£a+£b)/2', sigma: '£d', borneInf: ['5*£a'], borneSup: ['5*£b'] },
      // calculatrice est la phrase qui explique la marche à suivre pour effectuer le calcul à la calculatrice
      calculatrice: ['On utilise la calculatrice pour effectuer ce calcul : dans la partie distributions, choisir "loi Normale Cumulative" (Ncd, cdf ou FRep suivant les modèles). On complète ensuite par les bornes de l’intervalle ($calcule[5*£a]$ puis $calcule[5*£b]$) et par les valeurs de l’espérance $\\mu=calcule[5*(£a+£b)/2]$ et de l’écart-type $\\sigma=£d$.'],
      // On donne alors le résultat obtenu
      reponse: ['On obtient ainsi $P(calcule[5*£a]\\leq X\\leq calcule[5*£b])\\approx £r$.'],
      arrondi: 3
    },
    {
      nomSujet: 'Amérique du Sud 2014 S version 2', // numéro 11
      consignes: ['Un ballon de football est conforme à la réglementation s’il respecte, suivant sa taille, deux conditions à la fois (sur sa masse et sur sa circonférence).',
        'En particulier, un ballon de taille standard est conforme à la réglementation lorsque sa masse, exprimée en grammes, appartient à l’intervalle $[calcule[5*£a];calcule[5*£b]]$ et sa circonférence, exprimée en centimètres, appartient à l’intervalle $[£c;calcule[£c+2]]$.',
        'On note $X$ la variable aléatoire qui, à chaque ballon de taille standard choisi au hasard dans une entreprise de fabrication, associe sa circonférence en centimètres.',
        'On admet que $X$ suit la loi normale d’espérance $calcule[£c+1]$ et d’écart type $£d$.'],
      question: ['La probabilité qu’un ballon choisi au hasard dans la production soit d’une circonférence conforme à la réglementation vaut &1&.'],
      variables: [['a', '[81;83]'], ['b', '[89;91]'], ['c', '[67;69]'], ['d', '[43;52]', 100]],
      // je récupère les valeurs de mes variables
      valeurs: { mu: '£c+1', sigma: '£d', borneInf: ['£c'], borneSup: ['£c+2'] },
      // calculatrice est la phrase qui explique la marche à suivre pour effectuer le calcul à la calculatrice
      calculatrice: ['On utilise la calculatrice pour effectuer ce calcul : dans la partie distributions, choisir "loi Normale Cumulative" (Ncd, cdf ou FRep suivant les modèles). On complète ensuite par les bornes de l’intervalle ($£c$ puis $calcule[£c+2]$) et par les valeurs de l’espérance $\\mu=calcule[£c+1]$ et de l’écart-type $\\sigma=£d$.'],
      // On donne alors le résultat obtenu
      reponse: ['On obtient ainsi $P(£c\\leq X\\leq calcule[£c+2])\\approx £r$.'],
      arrondi: 3
    }
  ]
}
