export default {
  /* Dans toutes les consignes de ce type d’exercices, £a permet de déclarer la variable a. Il faut alors préciser l’intervalle dans lequel se trouve cette valeur
        Les consignes seront le plus souvent sur plusieurs lignes, d’où l’idée de créer un tableau
        la propriété variables de chaque énoncé est un tableau où chaque élément est un tableau de deux ou trois éléments
        Par exemple ['a','[5;15]'] signifie que a sera un entier de l’intervalle [5;15]
        ['a','[5;15]',100] signifie que a prend une valeur décimale de [0,05;0,15] avec 2 chiffres après la virgule
        typeSujet peut prendre les valeurs "calculProportion", "calculEffectif" ou "calculEffTotal" suivant ce qui est demandé
        */
  sujets: [
    {
      typeSujet: 'calculProportion', // numéro 0
      consignes: ['Sur le parking du lycée, parmi les £a véhicules garés, calcule[£a-£b] sont d’une marque française.'],
      variables: [['a', '[42;65]'], ['b', '[11;17]']],
      // Question sous la forme d’un pourcentage
      questionPourc: ['Ainsi £e&1& % des voitures du parking sont d’une marque française.',
        'On souhaite calculer le pourcentage de voitures du parking qui sont d’une marque française, puis en donner le résultat.',
        'Calcul : &1& et le résultat est £e&2&&nbsp;%.'], // £e vaudra "environ" ou "" suivant que la valeur à calculer soit exacte ou non
      // Question sous la forme d’une proportion
      questionProp: ['La proportion des voitures du parking qui sont d’une marque française vaut, au format décimal, £e&1&.',
        'On souhaite calculer la proportion de voitures du parking qui sont d’une marque française, puis en donner le résultat.',
        'Calcul : &1& et le résultat est £e&2&.'], // £e vaudra "environ" ou "" suivant que la valeur à calculer soit exacte ou non
      arrondisTxt: ['Arrondir à 0,1&nbsp;%.', 'Arrondir à 0,001.'],
      arrondis: [0.1, 0.001],
      valeurs: ['£a', '£a-£b'],
      // réponses associées
      reponses: { pourcent: '(£a-£b)*100/£a', proport: '(£a-£b)/£a' },
      // Correction
      correctionPourc: ['On calcule $\\frac{calcule[£a-£b]}{£a} £scalcule[£{r1}/100]$ soit £e$£r$&nbsp;%.',
        'Donc £e£{r2} % des voitures du parking sont d’une marque française.'],
      correctionProp: ['On calcule $\\frac{calcule[£a-£b]}{£a} £s£{r1}$.',
        'Donc la proportion des voitures du parking qui sont d’une marque française vaut £e£{r2}.']
    }, {
      typeSujet: 'calculProportion', // numéro 1
      consignes: ['calcule[£a-£b] élèves d’une classe de £a ont eu la moyenne au dernier devoir de mathématiques.'],
      variables: [['a', '[27;35]'], ['b', '[8;16]']],
      // Question sous la forme d’un pourcentage
      questionPourc: ['Ainsi £e&1& % des élèves de la classe ont eu la moyenne au devoir.',
        'On souhaite calculer le pourcentage d’élèves de la classe qui ont eu la moyenne, puis en donner le résultat.',
        'Calcul : &1& et le résultat est £e&2&&nbsp;%.'], // £e vaudra "environ" ou "" suivant que la valeur à calculer soit exacte ou non
      // Question sous la forme d’une proportion
      questionProp: ['La proportion des élèves de la classe qui ont eu la moyenne vaut, au format décimal, £e&1&.',
        'On souhaite calculer la proportion d’élèves de la classe qui ont eu la moyenne, puis en donner le résultat.',
        'Calcul : &1& et le résultat est £e&2&.'], // £e vaudra "environ" ou "" suivant que la valeur à calculer soit exacte ou non
      arrondisTxt: ['Arrondir à 0,1&nbsp;%.', 'Arrondir à 0,001.'],
      arrondis: [0.1, 0.001],
      valeurs: ['£a', '£a-£b'],
      // réponses associées
      reponses: { pourcent: '(£a-£b)*100/£a', proport: '(£a-£b)/£a' },
      // Correction
      correctionPourc: ['On calcule $\\frac{calcule[£a-£b]}{£a} £scalcule[£{r1}/100]$ soit £e$£r$&nbsp;%.',
        'Donc £e£{r2} % des élèves de la classe ont eu la moyenne au devoir.'],
      correctionProp: ['On calcule $\\frac{calcule[£a-£b]}{£a} £s£{r1}$.',
        'Donc la proportion des élèves de la classe qui ont eu la moyenne vaut £e£{r2}.']
    }, {
      typeSujet: 'calculProportion', // numéro 2
      consignes: ['Un agriculteur céréalier dispose d’une surface cultivable de calcule[£a*10+£c] hectares.',
        '£{b2} hectares de cette surface est destinée à la culture du blé.'],
      variables: [['a', '[8;12]'], ['b', '[400;510]', 10], ['c', '[1;9]']],
      // Question sous la forme d’un pourcentage
      questionPourc: ['Ainsi £e&1& % de la surface cultivable est réservée pour la culture du blé.',
        'On souhaite calculer le pourcentage de la surface cultivable réservée pour la culture du blé, puis en donner le résultat.',
        'Calcul : &1& et le résultat est £e&2& %.'], // £e vaudra "environ" ou "" suivant que la valeur à calculer soit exacte ou non
      // Question sous la forme d’une proportion
      questionProp: ['La proportion de la surface cultivable réservée pour le blé vaut, au format décimal, £e&1&.',
        'On souhaite calculer la proportion de la surface cultivable réservée pour la culture du blé, puis en donner le résultat.',
        'Calcul : &1& et le résultat est £e&2&.'], // £e vaudra "environ" ou "" suivant que la valeur à calculer soit exacte ou non
      arrondisTxt: ['Arrondir à 0,1&nbsp;%.', 'Arrondir à 0,001.'],
      arrondis: [0.1, 0.001],
      valeurs: ['£a*10+£c', '£b'],
      // réponses associées
      reponses: { pourcent: '(£b)*100/(£a*10+£c)', proport: '£b/(£a*10+£c)' },
      // Correction
      correctionPourc: ['On calcule $\\frac{£b}{calcule[£a*10+£c]} £scalcule[£{r1}/100]$ soit £e$£r$&nbsp;%.',
        'Donc £e£{r2} % de la surface cultivable est réservée pour la culture du blé.'],
      correctionProp: ['On calcule $\\frac{£b}{calcule[£a*10+£c]} £s£{r1}$.',
        'Donc la proportion de la surface cultivable réservée pour la culture du blé vaut £e£{r2}.']
    }, {
      typeSujet: 'calculProportion', // numéro 3
      consignes: ["La population d’un village s'élève à calcule[£a*20] habitants.",
        'Parmi eux, calcule[£a*14-£b] ont voté au cours des dernières municipales et £c se sont exprimés en faveur de la liste défendue par Mme M.'],
      variables: [['a', '[78;112]'], ['b', '[110;160]'], ['c', '[200;300]']],
      // Question sous la forme d’un pourcentage
      questionPourc: ['Ainsi £e&1& % des votants se sont exprimés en faveur de la liste défendue par Mme M.',
        'On souhaite calculer le pourcentage de votants qui se sont exprimés en faveur de la liste défendue par Mme M, puis en donner le résultat.',
        'Calcul : &1& et le résultat est £e&2& %.'], // £e vaudra "environ" ou "" suivant que la valeur à calculer soit exacte ou non
      // Question sous la forme d’une proportion
      questionProp: ['La proportion des votants qui se sont exprimés en faveur de la liste défendue par Mme M vaut, au format décimal, £e&1&.',
        'On souhaite calculer la proportion de votants qui se sont exprimés en faveur de la liste défendue par Mme M, puis en donner le résultat.',
        'Calcul : &1& et le résultat est £e&2&.'], // £e vaudra "environ" ou "" suivant que la valeur à calculer soit exacte ou non
      arrondisTxt: ['Arrondir à 0,1&nbsp;%.', 'Arrondir à 0,001.'],
      arrondis: [0.1, 0.001],
      valeurs: ['£a*14-£b', '£c'],
      // réponses associées
      reponses: { pourcent: '(£c)*100/(£a*14-£b)', proport: '£c/(£a*14-£b)' },
      // Correction
      correctionPourc: ['On calcule $\\frac{£c}{calcule[£a*14-£b]} £scalcule[£{r1}/100]$ soit £e$£r$&nbsp;%.',
        'Donc £e£{r2} % des votants se sont exprimés en faveur de la liste défendue par Mme M.'],
      correctionProp: ['On calcule $\\frac{£c}{calcule[£a*14-£b]} £s£{r1}$.',
        'Donc la proportion des votants qui se sont exprimés en faveur de la liste défendue par Mme M vaut £e£{r2}.']
    }, {
      typeSujet: 'calculProportion', // numéro 4
      consignes: ['Au cours d’une journée très pluvieuse, £a&nbsp;mm de précipitation ont été relevés.',
        'Or à midi, le pluviomètre n’indiquait que £b&nbsp;mm de pluie.'],
      variables: [['a', '[40;60]'], ['b', '[6;15]']],
      // Question sous la forme d’un pourcentage
      questionPourc: ['Ainsi £e&1& % de la quantité la pluie tombée sur la journée l’a été avant midi.',
        'On souhaite calculer le pourcentage de la quantité de pluie tombée sur la journée qui l’a été avant midi, puis en donner le résultat.',
        'Calcul : &1& et le résultat est £e&2& %.'], // £e vaudra "environ" ou "" suivant que la valeur à calculer soit exacte ou non
      // Question sous la forme d’une proportion
      questionProp: ['La proportion de la quantité de pluie tombée avant midi par rapport à ce qui a été relevé sur la journée vaut, au format décimal, £e&1&.',
        'On souhaite calculer la proportion de la quantité de pluie tombée sur la journée qui l’a été avant midi, puis en donner le résultat.',
        'Calcul : &1& et le résultat est £e&2&.'], // £e vaudra "environ" ou "" suivant que la valeur à calculer soit exacte ou non
      arrondisTxt: ['Arrondir à 0,1&nbsp;%.', 'Arrondir à 0,001.'],
      arrondis: [0.1, 0.001],
      valeurs: ['£a', '£b'],
      // réponses associées
      reponses: { pourcent: '£b*100/£a', proport: '£b/£a' },
      // Correction
      correctionPourc: ['On calcule $\\frac{£b}{£a} £scalcule[£{r1}/100]$ soit £e$£r$&nbsp;%.',
        'Donc £e£{r2} % de la pluie tombée sur la journée l’a été avant midi.'],
      correctionProp: ['On calcule $\\frac{£b}{£a} £s£{r1}$.',
        'Donc la proportion de la quantité de pluie tombée avant midi par rapport à ce qui a été relevé sur la journée vaut £e£{r2}.']
    }, {
      typeSujet: 'calculProportion', // numéro 5
      consignes: ['Pour sensibiliser les élèves d’un lycée au gaspillage, le service de restauration a pesé les déchets de chaque assiette pendant une semaine.',
        'Il a relevé une moyenne de £a grammes de déchet par assiette.',
        'Une campagne a alors été réalisée et au bout d’un mois, la masse moyenne des déchets avait baissé de £b grammes.'],
      variables: [['a', '[102;125]'], ['b', '[21;35]']],
      // Question sous la forme d’un pourcentage
      questionPourc: ['Ainsi cette baisse représente £e&1& % de la masse initiale de déchets.',
        'On souhaite calculer le pourcentage que représente cette baisse par rapport à la masse initiale de déchets, puis en donner le résultat.',
        'Calcul : &1& et le résultat est £e&2& %.'], // £e vaudra "environ" ou "" suivant que la valeur à calculer soit exacte ou non
      // Question sous la forme d’une proportion
      questionProp: ['La proportion de cette baisse sur la masse initiale de déchets vaut, au format décimal, £e&1&.',
        'On souhaite calculer la proportion que représente cette baisse par rapport à la masse initiale de déchets, puis en donner le résultat.',
        'Calcul : &1& et le résultat est £e&2&.'], // £e vaudra "environ" ou "" suivant que la valeur à calculer soit exacte ou non
      arrondisTxt: ['Arrondir à 0,1&nbsp;%.', 'Arrondir à 0,001.'],
      arrondis: [0.1, 0.001],
      valeurs: ['£a', '£b'],
      // réponses associées
      reponses: { pourcent: '£b*100/£a', proport: '£b/£a' },
      // Correction
      correctionPourc: ['On calcule $\\frac{£b}{£a} £scalcule[£{r1}/100]$ soit £e$£r$&nbsp;%.',
        'Donc la baisse représente £e£{r2}&nbsp;% de la masse initiale de déchets.'],
      correctionProp: ['On calcule $\\frac{£b}{£a} £s£{r1}$.',
        'Donc la proportion de cette baisse sur la masse initiale de déchets vaut £e£{r2}.']
    }, {
      typeSujet: 'calculProportion', // numéro 6
      consignes: ['Une équipe de football dispose de £a joueurs.',
        'calcule[£a-£b] d’entre eux ont déjà joué au cours des cinq premières journées de championnat.'],
      variables: [['a', '[21;27]'], ['b', '[4;7]']],
      // Question sous la forme d’un pourcentage
      questionPourc: ['Ainsi £e&1& % des joueurs de l’équipe ont déjà joué au cours des cinq premières journées.',
        'On souhaite calculer le pourcentage de joueurs de l’équipe qui ont déjà joué au cours des cinq premières journées, puis en donner le résultat.',
        'Calcul : &1& et le résultat est £e&2&&nbsp;%.'], // £e vaudra "environ" ou "" suivant que la valeur à calculer soit exacte ou non
      // Question sous la forme d’une proportion
      questionProp: ['La proportion des joueurs de l’équipe qui ont déjà joué au cours des cinq premières journées vaut, au format décimal, £e&1&.',
        'On souhaite calculer la proportion de joueurs de l’équipe qui ont déjà joué au cours des cinq premières journées, puis en donner le résultat.',
        'Calcul : &1& et le résultat est £e&2&.'], // £e vaudra "environ" ou "" suivant que la valeur à calculer soit exacte ou non
      arrondisTxt: ['Arrondir à 0,1&nbsp;%.', 'Arrondir à 0,001.'],
      arrondis: [0.1, 0.001],
      valeurs: ['£a', '£a-£b'],
      // réponses associées
      reponses: { pourcent: '(£a-£b)*100/£a', proport: '(£a-£b)/£a' },
      // Correction
      correctionPourc: ['On calcule $\\frac{calcule[£a-£b]}{£a} £scalcule[£{r1}/100]$ soit £e$£r$&nbsp;%.',
        'Donc £e£{r2}&nbsp;% des joueurs de l’équipe ont déjà joué au cours des cinq premières journées.'],
      correctionProp: ['On calcule $\\frac{calcule[£a-£b]}{£a} £s£{r1}$.',
        'Donc la proportion de joueurs de l’équipe qui ont déjà joué au cours des cinq premières journées vaut £e£{r2}.']
    }, {
      typeSujet: 'calculProportion', // numéro 7
      consignes: ['Le buteur d’une équipe de rugby mesure son efficacité lors des coups de pied de pénalités au cours des derniers matchs.',
        'Sur les £a tentatives, il a marqué la pénalité à calcule[£a-£b] reprises.'],
      variables: [['a', '[29;38]'], ['b', '[8;13]']],
      // Question sous la forme d’un pourcentage
      questionPourc: ['Ainsi £e&1& % des tentatives ont été réussies.',
        'On souhaite calculer le pourcentage de tentatives réussies, puis en donner le résultat.',
        'Calcul : &1& et le résultat est £e&2&&nbsp;%.'], // £e vaudra "environ" ou "" suivant que la valeur à calculer soit exacte ou non
      // Question sous la forme d’une proportion
      questionProp: ['La proportion de tentatives réussies vaut, au format décimal, £e&1&.',
        'On souhaite calculer la proportion de tentatives réussies, puis en donner le résultat.',
        'Calcul : &1& et le résultat est £e&2&.'], // £e vaudra "environ" ou "" suivant que la valeur à calculer soit exacte ou non
      arrondisTxt: ['Arrondir à 0,1&nbsp;%.', 'Arrondir à 0,001.'],
      arrondis: [0.1, 0.001],
      valeurs: ['£a', '£a-£b'],
      // réponses associées
      reponses: { pourcent: '(£a-£b)*100/£a', proport: '(£a-£b)/£a' },
      // Correction
      correctionPourc: ['On calcule $\\frac{calcule[£a-£b]}{£a} £scalcule[£{r1}/100]$ soit £e$£r$&nbsp;%.',
        'Donc £e£{r2}&nbsp;% des tentatives ont été réussies.'],
      correctionProp: ['On calcule $\\frac{calcule[£a-£b]}{£a} £s£{r1}$.',
        'Donc la proportion de tentatives réussies vaut £e£{r2}.']
    },
    // Les sujets suivants sont pour le calcul d’effectif connaissant la proportion
    {
      typeSujet: 'calculEffectif', // numéro 8
      consignes: ["La facture d’électricité d’un ménage s'élève à calcule[£a*5]&nbsp;euros par mois.",
        'Il estime que £b&nbsp;% de cette consommation est dû à l’éclairage.'],
      variables: [['a', '[24;35]'], ['b', '[9;17]']],
      // Question
      questionEff: ["Le montant de la dépense du ménage pour l’éclairage s'élève donc à £e&1& euros.",
        'On souhaite calculer le montant de la dépense du ménage pour l’éclairage, puis en donner le résultat.',
        'Calcul : &1& et le résultat est £e&2&.'], // £e vaudra "environ" ou "" suivant que la valeur à calculer soit exacte ou non
      arrondisTxt: ['Arrondir à 0,01&nbsp;€.'],
      arrondis: [0.01],
      valeurs: ['£a*5', '£b'],
      // réponses associées
      reponses: { part: '(£a*5)*£b/100' },
      // Correction
      correctionEff: ['On calcule $calcule[£a*5]\\times \\frac{£b}{100} £s£{r1}$.',
        'Donc £e£{r2} euros sont consacrés à l’éclairage.']
    },
    {
      typeSujet: 'calculEffectif', // numéro 9
      consignes: ['Antoine a relevé le nombre de jours de pluie de l’année calcule[2017+£a].',
        'Environ £{b2} % des jours de l’année calcule[2017+£a] ont été des jours de pluie.'],
      variables: [['a', '[0;1]'], ['b', '[210;273]', 10]],
      // Question
      questionEff: ['Antoine a ainsi relevé £e&1& jours de pluie en calcule[2017+£a].',
        'On souhaite calculer le nombre de jours de pluie relevé par Antoine, puis en donner le résultat.',
        'Calcul : &1& et le résultat est £e&2&.'], // £e vaudra "environ" ou "" suivant que la valeur à calculer soit exacte ou non
      arrondisTxt: ['Arrondir à l’unité.'],
      arrondis: [1],
      valeurs: [365, '£b'],
      // réponses associées
      reponses: { part: '365*£b/100' },
      // Correction
      correctionEff: ['On calcule $365\\times \\frac{£b}{100} £s£{r1}$.',
        'Donc Antoine a relevé £e£{r2} jours de pluie en calcule[2017+£a].']
    },
    {
      typeSujet: 'calculEffectif', // numéro 10
      consignes: ['Comme chaque année scolaire, les élèves d’un lycée doivent élire leurs représentants au conseil de la vie lycéenne.',
        'Cette année, £a élèves sont inscrits dans l’établissement et le taux de participation à cette élection s’est élevé à £{b2}&nbsp;%.'],
      variables: [['a', '[1050;1250]'], ['b', '[150;273]', 10]],
      // Question
      questionEff: ['Ainsi £e&1& élèves du lycée ont voté au cours de cette élection.',
        'On souhaite calculer le nombre d’élèves du lycée ayant voté au cours de cette élection, puis en donner le résultat.',
        'Calcul : &1& et le résultat est £e&2&.'], // £e vaudra "environ" ou "" suivant que la valeur à calculer soit exacte ou non
      arrondisTxt: ['Arrondir à l’unité.'],
      arrondis: [1],
      valeurs: ['£a', '£b'],
      // réponses associées
      reponses: { part: '£a*£b/100' },
      // Correction
      correctionEff: ['On calcule $£a\\times \\frac{£b}{100} £s£{r1}$.',
        'Donc £e£{r2} élèves du lycée ont voté au cours de cette élection.']
    },
    {
      typeSujet: 'calculEffectif', // numéro 11
      consignes: ['Sur un paquet de gâteaux, il est indiqué que la farine de blé représente £{b2}&nbsp;% de la masse totale d’un biscuit.',
        'Chaque biscuit pèse aux alentours de £a&nbsp;g.'],
      variables: [['a', '[12;19]'], ['b', '[310;420]', 10]],
      // Question
      questionEff: ['Ainsi chaque biscuit contient £e&1& g de farine de blé.',
        'On souhaite calculer la quantité de farine de blé contenue dans chaque biscuit, puis en donner le résultat.',
        'Calcul : &1& et le résultat est £e&2&.'], // £e vaudra "environ" ou "" suivant que la valeur à calculer soit exacte ou non
      arrondisTxt: ['Arrondir à 0,1&nbsp;g.'],
      arrondis: [0.1],
      valeurs: ['£a', '£b'],
      // réponses associées
      reponses: { part: '£a*£b/100' },
      // Correction
      correctionEff: ['On calcule $£a\\times \\frac{£b}{100} £s£{r1}$.',
        'Donc chaque biscuit contient £e£{r2} g de farine de blé.']
    },
    {
      typeSujet: 'calculEffectif', // numéro 12
      consignes: ['En 2017, Quentin a parcouru calcule[£a*100] km avec sa voiture personnelle.',
        'Suite à un déménagement, l’année suivante ses déplacements ont augmenté de £{b2}&nbsp;%.'],
      variables: [['a', '[175;240]'], ['b', '[65;96]', 10]],
      // Question
      questionEff: ['En 2018, il a parcouru avec son véhicule personnel £e&1& km de plus qu’en 2017.',
        'On souhaite calculer le nombre de kilomètres supplémentaires effectués en 2018 par rapport à 2017, puis en donner le résultat.',
        'Calcul : &1& et le résultat est £e&2&.'], // £e vaudra "environ" ou "" suivant que la valeur à calculer soit exacte ou non
      arrondisTxt: ['Arrondir à l’unité.'],
      arrondis: [0.1],
      valeurs: ['£a*100', '£b'],
      // réponses associées
      reponses: { part: '£a*100*£b/100' },
      // Correction
      correctionEff: ['On calcule $calcule[£a*100]\\times \\frac{£b}{100} £s£{r1}$.',
        'Donc en 2018, il a parcouru £e£{r2} km de plus qu’en 2017.']
    },
    {
      typeSujet: 'calculEffectif', // numéro 13
      consignes: ['Un appareil électroménager est vendu calcule[£a*10+£c] euros hors taxes (HT).',
        "La marge brute réalisée par le vendeur sur la vente de cet appareil s'élève £{b2}&nbsp;% du prix de vente HT."],
      variables: [['a', '[7;13]'], ['b', '[65;96]', 10], ['c', '[1;9]']],
      // Question
      questionEff: ['Le vendeur réalise donc £e&1& euros de marge brute sur la vente de cette appareil.',
        'On souhaite calculer le montant de la marge brute réalisée par le vendeur, puis en donner le résultat.',
        'Calcul : &1& et le résultat est £e&2&.'], // £e vaudra "environ" ou "" suivant que la valeur à calculer soit exacte ou non
      arrondisTxt: ['Arrondir à 0,01&nbsp;euro.'],
      arrondis: [0.01],
      valeurs: ['£a*10+£c', '£b'],
      // réponses associées
      reponses: { part: '(£a*10+£c)*£b/100' },
      // Correction
      correctionEff: ['On calcule $calcule[£a*10+£c]\\times \\frac{£b}{100} £s£{r1}$.',
        'Donc le vendeur réalise £e£{r2} euros de marge brute sur la vente de cet appareil.']
    },
    // Les sujets suivants sont pour le calcul d’un effectif total connaissant l’effectif et la proportion d’une partie
    {
      typeSujet: 'calculTotal', // numéro 14
      consignes: ['Dans un lycée, £{b2}&nbsp;% des élèves sont en classe de Seconde.',
        'Cela représente £a élèves de seconde.'],
      variables: [['a', '[331;380]'], ['b', '[240;290]', 10]],
      // Question
      questionTotal: ["Le nombre total d’élèves du lycée s'élève ainsi à £e&1& élèves.",
        'On souhaite calculer le nombre total d’élèves dans ce lycée, puis en donner le résultat.',
        'Calcul : &1& et le résultat est £e&2&.'], // £e vaudra "environ" ou "" suivant que la valeur à calculer soit exacte ou non
      arrondisTxt: ['Arrondir à l’unité.'],
      arrondis: [1],
      valeurs: ['£a', '£b'],
      // réponses associées
      reponses: { total: '£a*100/£b' },
      // Correction
      correctionTotal: ['On calcule $\\frac{£a\\times 100}{£b} £s£{r1}$.',
        'Donc le lycée compte en tout £e£{r2} élèves.']
    },
    {
      typeSujet: 'calculTotal', // numéro 15
      consignes: ['Pour les soldes du mois de janvier, le prix d’un article a diminué de calcule[£a*5]&nbsp;%.',
        'Cela représente une économie de £{b2}&nbsp;euros.'],
      variables: [['a', '[4;7]'], ['b', '[240;290]', 10]],
      // Question
      questionTotal: ["Le prix non soldé de cet article s'élevait donc à £e&1& euros.",
        'On souhaite calculer le prix non soldé de cet article, puis en donner le résultat.',
        'Calcul : &1& et le résultat est £e&2&.'], // £e vaudra "environ" ou "" suivant que la valeur à calculer soit exacte ou non
      arrondisTxt: ['Arrondir à 0,01&nbsp;euro.'],
      arrondis: [0.01],
      valeurs: ['£a*5', '£b'],
      // réponses associées
      reponses: { total: '£b*100/(£a*5)' },
      // Correction
      correctionTotal: ['On calcule $\\frac{£b\\times 100}{calcule[£a*5]} £s£{r1}$.',
        'Donc l’article non soldé coûtait £e£{r2}&nbsp;euros.']
    },
    {
      typeSujet: 'calculTotal', // numéro 16
      consignes: ['Un cocktail de jus de fruit contient £a&nbsp;cL de jus d’ananas ce qui représente £{b2}&nbsp;% du volume total.'],
      variables: [['a', '[3;7]'], ['b', '[155;185]', 10]],
      // Question
      questionTotal: ["Le volume total de ce cocktail s'élève à £e&1& cL.",
        'On souhaite calculer le volume total de ce cocktail, puis en donner le résultat.',
        'Calcul : &1& et le résultat est £e&2&.'], // £e vaudra "environ" ou "" suivant que la valeur à calculer soit exacte ou non
      arrondisTxt: ['Arrondir au mL.'],
      arrondis: [0.1],
      valeurs: ['£b', '£a'],
      // réponses associées
      reponses: { total: '£a*100/£b' },
      // Correction
      correctionTotal: ['On calcule $\\frac{£a\\times 100}{£b} £s£{r1}$.',
        'Donc le cocktail contient £e£{r2}&nbsp;cL.']
    },
    {
      typeSujet: 'calculTotal', // numéro 17
      consignes: ['£{b2}&nbsp;% des électeurs d’un quartier se sont prononcés en faveur du retour d’un service de bus.',
        'Ceci représente £a voix.'],
      variables: [['a', '[130;164]'], ['b', '[740;820]', 10]],
      // Question
      questionTotal: ["Le nombre total de votants de ce quartier s'élève à £e&1& personnes.",
        'On souhaite calculer le nombre total de votants du quartier, puis en donner le résultat.',
        'Calcul : &1& et le résultat est £e&2&.'], // £e vaudra "environ" ou "" suivant que la valeur à calculer soit exacte ou non
      arrondisTxt: ['Arrondir à l’unité.'],
      arrondis: [1],
      valeurs: ['£a', '£b'],
      // réponses associées
      reponses: { total: '£a*100/£b' },
      // Correction
      correctionTotal: ['On calcule $\\frac{£a\\times 100}{£b} £s£{r1}$.',
        'Donc, dans le quartier, £e£{r2} personnes ont voté.']
    },
    {
      typeSujet: 'calculTotal', // numéro 18
      consignes: ['£{b2}&nbsp;% du chiffre d’affaires d’un magasin de jouet est effectué au mois de décembre.',
        'Cela représente calcule[£a*10]&nbsp;euros.'],
      variables: [['a', '[3550;4500]'], ['b', '[300;420]', 10]],
      // Question
      questionTotal: ["Sur l’année complète, le chiffre d’affaires du magasin s'élève donc à £e&1&&nbsp;euros.",
        'On souhaite calculer le chiffre d’affaires de ce magasin sur l’année complète, puis en donner le résultat.',
        'Calcul : &1& et le résultat est £e&2&.'], // £e vaudra "environ" ou "" suivant que la valeur à calculer soit exacte ou non
      arrondisTxt: ['Arrondir à l’euro.'],
      arrondis: [1],
      valeurs: ['£a*10', '£b'],
      // réponses associées
      reponses: { total: '£a*10*100/£b' },
      // Correction
      correctionTotal: ['On calcule $\\frac{calcule[£a*10]\\times 100}{£b} £s£{r1}$.',
        "Donc, sur l’année complète, le chiffre d’affaires de ce magasin s'élève à £e£{r2}&nbsp;euros."]
    },
    {
      typeSujet: 'calculTotal', // numéro 19
      consignes: ['Pour repeindre son salon, Myriam se rend dans un magasin de bricolage.',
        'Le pot de peinture de £{a2}&nbsp;litres qu’elle voit en rayon doit lui permettre de couvrir £{b2}&nbsp;% de la surface totale.'],
      variables: [['a', '[6;10]', 2], ['b', '[300;420]', 10]],
      // Question
      questionTotal: ['Pour repeindre entièrement son salon (sachant qu’elle ne passe qu’une couche), il lui faudra £e&1&&nbsp;litres de peinture.',
        'On souhaite calculer la quantité de peinture nécessaire pour peindre entièrement son salon (sachant qu’elle ne passe qu’une couche), puis en donner le résultat.',
        'Calcul : &1& et le résultat est £e&2&.'], // £e vaudra "environ" ou "" suivant que la valeur à calculer soit exacte ou non
      arrondisTxt: ['Arrondir à 0,1&nbsp;litre.'],
      arrondis: [0.1],
      valeurs: ['£a*0.5', '£b'],
      // réponses associées
      reponses: { total: '£a*100/£b' },
      // Correction
      correctionTotal: ['On calcule $\\frac{£{a2}\\times 100}{£b} £s£{r1}$.',
        'Donc, pour repeindre entièrement son salon, il lui faudra £e£{r2}&nbsp;litres de peinture.']
    }
  ]
}
