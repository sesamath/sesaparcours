export default {
  /* Dans toutes les consignes de ce type d’exercices, £a permet de déclarer la variable a. Il faut alors préciser l’intervalle dans lequel se trouve cette valeur
    Les consignes seront le plus souvent sur plusieurs lignes, d’où l’idée de créer un tableau
    la propriété variables de chaque énoncé est un tableau où chaque élément est un tableau de deux ou trois éléments
    Par exemple ['a','[5;15]'] signifie que a sera un entier de l’intervalle [5;15]
    ['a','[5;15]',100] signifie que a prend une valeur décimale de [0,05;0,15] avec 2 chiffres après la virgule
    */
  sujets: [
    {
      nomSujet: 'Liban juin 2017 ES', // numéro 0
      utilisableSeconde: false,
      consignes: ['A l’occasion de son inauguration, un hypermarché offre à ses clients un ticket à gratter par tranche de 10 euros d’achats.',
        'L’hypermarché affirme que £a % des tickets à gratter sont gagnants, c’est-à-dire donneront droit à un bon d’achat de 5 euros.',
        'Amandine a reçu calcule[£b*10] tickets à gratter après un achat de calcule[£b*100] euros dans cet hypermarché. £c d’entre eux étaient gagnants.',
        'Les bornes étant arrondies au millième, l’intervalle de fluctuation asymptotique au seuil de £s % de la fréquence observée de tickets gagnants dans un échantillon de $calcule[£b*10]$ tickets à gratter est :'],
      question: '&1&.',
      questFreq: ['Un intervalle de fluctuation au seuil de £s % de la fréquence de tickets gagnants dans cet échantillon est £i.',
        'Peut-on considérer, au risque d’erreur de calcule[100-£s] %, que l’affirmation de l’hypermarché est inexacte ?'],
      variables: [['a', '[13;18]'], ['b', '[5;8]'], ['c', '[3;7]']],
      // je récupère les valeurs de mes variables
      valeurs: { p: '£a/100', n: '£b*10', freq: '£c/(£b*10)' },
      donnees: 'On est en présence d’un échantillon de taille $n=calcule[£b*10]$. La proportion de tickets gagnants est $p=\\frac{£a}{100}=calcule[£a/100]$.',
      // On donne alors le résultat obtenu
      reponse: ['Sur l’échantillon de calcule[£b*100] tickets, la fréquence de tickets gagnants vaut $f=\\frac{£c}{calcule[£b*100]}£e$.',
        'Comme $f\\notin £i$, on considère au risque d’erreur de calcule[100-£s] % que l’affirmation de l’hypermarché est inexacte.',
        'Comme $f\\in £i$, on ne peut pas considérer que l’affirmation de l’hypermarché est inexacte.'],
      arrondi: 3
    },
    {
      nomSujet: 'Pondichéry avril 2017 S', // numéro 1
      utilisableSeconde: false,
      consignes: ['La chocolaterie « Choc’o » fabrique des tablettes de chocolat noir, de 100 grammes, dont la teneur en cacao annoncée est de 85 %.',
        'Elle vend un lot de 10 000 tablettes de chocolat à une enseigne de la grande distribution. Elle affirme au responsable achat de l’enseigne que, dans ce lot, £c % des tablettes ont un pourcentage de cacao appartenant à l’intervalle [81,7;88,3].',
        'Afin de vérifier si cette affirmation n’est pas mensongère, le responsable achat fait prélever calcule[£a*50] tablettes au hasard dans le lot et constate que, sur cet échantillon, calcule[£b*5] ne répondent pas au critère.',
        'Les bornes étant arrondies au millième, l’intervalle de fluctuation asymptotique au seuil de £s % de la fréquence observée de tablettes qui répondent au critère dans un échantillon de $calcule[£a*50]$ tablettes du lot est :'],
      question: '&1&.',
      questFreq: ['Un intervalle de fluctuation au seuil de £s % de la fréquence de tablettes qui repondent au critère dans cet échantillon est £i.',
        'Peut-on considérer, au risque d’erreur de calcule[100-£s] %, que l’affirmation de la chocolaterie est inexacte ?'],
      variables: [['a', '[9;13]'], ['b', '[15;22]'], ['c', '[85;92]']],
      // je récupère les valeurs de mes variables
      valeurs: { p: '£c/100', n: '£a*50', freq: '1-£b/(£a*10)' },
      donnees: 'On est en présence d’un échantillon de taille $n=calcule[£a*50]$. La proportion de tablettes qui répondent au critère est $p=\\frac{£c}{100}=calcule[£c/100]$.',
      // On donne alors le résultat obtenu
      reponse: ['Sur l’échantillon de calcule[£a*50] tablettes, la fréquence de tablettes qui répondent au critère vaut $f=\\frac{calcule[£b*5]}{calcule[£a*50]}£e$.',
        'Comme $f\\notin £i$, on considère au risque d’erreur de calcule[100-£s] % que l’affirmation de la chocolaterie est inexacte.',
        'Comme $f\\in £i$, on ne peut pas considérer que l’affirmation de la chocolaterie est inexacte.'],
      arrondi: 3
    },
    {
      nomSujet: 'Pondichéry avril 2017 STMG', // numéro 2
      utilisableSeconde: true,
      consignes: ['L’EFS affirme que dans une région donnée : « £a % de la population donne son sang au moins une fois par an ».',
        'On interroge au hasard un échantillon de calcule[£b*100] personnes habitant cette région. Parmi elles, calcule[£b*25+£c] ont donné au moins une fois leur sang au cours de la dernière année.',
        'Les bornes étant arrondies au millième, l’intervalle de fluctuation asymptotique au seuil de £s % de la fréquence observée de personnes donnant leur sang dans un échantillon de $calcule[£b*100]$ personnes de la région est :'],
      question: '&1&.',
      questFreq: ['Un intervalle de fluctuation au seuil de £s % de la fréquence de personnes donnant leur sang dans cet échantillon est £i.',
        'Peut-on, au risque d’erreur de $calcule[100-£s]$ %, mettre en doute l’affirmation de l’EFS ?'],
      variables: [['a', '[22;28]'], ['b', '[9;12]'], ['c', '[-15;20]']],
      // je récupère les valeurs de mes variables
      valeurs: { p: '£a/100', n: '£b*100', freq: '0.25+£c/(£b*100)' },
      donnees: 'On est en présence d’un échantillon de taille $n=calcule[£b*100]$. La proportion de personnes qui donne leur sang est $p=\\frac{£a}{100}=calcule[£a/100]$.',
      // On donne alors le résultat obtenu
      reponse: ['Sur l’échantillon de calcule[£b*100] personnes de la région, la fréquence de celles qui donnent leur sang vaut $f=\\frac{calcule[£b*25+£c]}{calcule[£b*100]}£e$.',
        'Comme $f\\notin £i$, on peut, au risque d’erreur de calcule[100-£s] %, mettre en doute l’affirmation de l’EFS.',
        'Comme $f\\in £i$, on ne peut mettre en doute l’affirmation de l’EFS.'],
      arrondi: 3
    },
    {
      nomSujet: 'Centres étrangers juin 2017 STMG', // numéro 3
      utilisableSeconde: true,
      consignes: ['Une étude menée par l’institut national de prévention et d’éducation à la santé évalue le comportement face au tabac en fonction de l’âge d’initiation.',
        'Cette étude menée auprès d’un panel de personnes âgées de 20 ans à 25 ans et ayant déjà testé la cigarette à l’adolescence montre que $£a$ % du panel sont des fumeurs réguliers.',
        'On interroge calcule[£b*50] personnes, choisies au hasard, âgées de 20 à 25 ans ayant déjà fumé. L’échantillon ainsi constitué compte calcule[£b*28+£c] fumeurs réguliers.',
        'Les bornes étant arrondies au millième, l’intervalle de fluctuation asymptotique au seuil de £s % de la fréquence observée de fumeurs réguliers dans un échantillon de $calcule[£b*50]$ personnes âgées de 20 ans à 25 ans et ayant déjà testé la cigarette à l’adolescence est :'],
      question: '&1&.',
      questFreq: ['Un intervalle de fluctuation au seuil de £s % de la fréquence de fumeurs réguliers dans cet échantillon est £i.',
        'Peut-on, au risque d’erreur de calcule[100-£s] %, considérer que le nombre de fumeurs réguliers de cet échantillon est anormalement élevé ?'],
      variables: [['a', '[510;579]', 10], ['b', '[9;13]'], ['c', '[15;30]']],
      // je récupère les valeurs de mes variables
      valeurs: { p: '£a/100', n: '£b*50', freq: '(£b*28+£c)/(£b*50)' },
      donnees: 'On est en présence d’un échantillon de taille $n=calcule[£b*50]$. La proportion de personnes qui donne leur sang est $p=\\frac{£a}{100}=calcule[£a/100]$.',
      // On donne alors le résultat obtenu
      reponse: ['Sur l’échantillon de $calcule[£b*50]$ personnes de la région, la fréquence de celles qui donnent leur sang vaut $f=\\frac{calcule[£b*28+£c]}{calcule[£b*50]}£e$.',
        'Comme $f\\notin £i$, on considère au risque d’erreur de calcule[100-£s] % que le nombre de fumeurs de cet échantillon est anormalement élevé.',
        'Comme $f\\in £i$, on considère que le nombre de fumeurs de cet échantillon n’est pas anormalement élevé.'],
      arrondi: 3
    },
    {
      nomSujet: 'Antilles juin 2017 STMG', // numéro 4
      utilisableSeconde: true,
      consignes: ['La part de consommateurs bio réguliers, c’est-à-dire ceux qui disent consommer bio au moins une fois par mois s’élève à £a % en France.',
        'On effectue un sondage dans une société de calcule[£b*50] personnes.',
        'calcule[£b*25+£c*2] d’entre elles affirment consommer bio au moins une fois par mois.',
        'Les bornes étant arrondies au millième, l’intervalle de fluctuation asymptotique au seuil de £s % de la fréquence observée de personnes consommant bio au moins une fois par mois dans un échantillon de $calcule[£b*50]$ personnes est :'],
      question: '&1&.',
      questFreq: ['Un intervalle de fluctuation au seuil de £s % de la fréquence de personnes consommant bio au moins une fois par mois dans cet échantillon est £i.',
        'Peut-on considérer, au risque d’erreur de calcule[100-£s] %, que la société n’est pas représentative de la population française sur la consommation de produits bio ?'],
      variables: [['a', '[41;49]'], ['b', '[7;12]'], ['c', '[-20;5]']],
      // je récupère les valeurs de mes variables
      valeurs: { p: '£a/100', n: '£b*50', freq: '(£b*25+£c*2)/(£b*50)' },
      donnees: 'On est en présence d’un échantillon de taille $n=calcule[£b*50]$. La proportion de personnes qui consomment bio au moins une fois par mois est $p=\\frac{£a}{100}=calcule[£a/100]$.',
      // On donne alors le résultat obtenu
      reponse: ['Sur l’échantillon de $calcule[£b*50]$ personnes de cette société, la fréquence de celles qui consomme bio au moins une fois par mois vaut $f=\\frac{calcule[£b*25+£c*2]}{calcule[£b*50]}£e$.',
        'Comme $f\\notin £i$, on considère au risque d’erreur de calcule[100-£s] % que la société n’est pas représentative de la population française sur la consommation de produits bio.',
        'Comme $f\\in £i$, on considère que la société est représentative de la population française sur la consommation de produits bio.'],
      arrondi: 3
    },
    {
      nomSujet: 'Antilles-Guyane septembre 2017 STMG', // numéro 5
      utilisableSeconde: false,
      consignes: ['Tous les 5 ans, l’établissement INVS (Institut national de veille sanitaire) réalise une enquête sur les infections nosocomiales (infections contractées au cours d’une hospitalisation).',
        'On suppose que la probabilité qu’un patient soit victime d’une infection nosocomiale est $calcule[£a/500]$.',
        'Dans un hôpital, calcule[£b*500] patients ont été hospitalisés lors du premier trimestre 2016.',
        'Parmi ces patients, calcule[£a*£b+£c] ont été victimes d’une infection nosocomiale lors de leur passage dans cet hôpital. Le directeur de cet établissement trouve inquiétant ces résultats.',
        'Les bornes étant arrondies au millième, l’intervalle de fluctuation asymptotique au seuil de £s % de la fréquence observée de patients victimes d’une infection nosocomiale dans un échantillon de $calcule[£a*100]$ patients est :'],
      question: '&1&.',
      questFreq: ['Un intervalle de fluctuation au seuil de £s % de la fréquence de patients victimes d’une infection nosocomiale dans cet échantillon est £i.',
        'Peut-on considérer, au risque d’erreur de calcule[100-£s] %, que les craintes du directeur sont justifiées ?'],
      variables: [['a', '[24;28]'], ['b', '[3;6]'], ['c', '[23;51]']],
      // je récupère les valeurs de mes variables
      valeurs: { p: '£a/500', n: '£b*500', freq: '(£b*£a+£c)/(£b*500)' },
      donnees: 'On est en présence d’un échantillon de taille $n=calcule[£b*500]$. La proportion de patients victimes d’une infection nosocomiale est $p=calcule[£a/500]$.',
      // On donne alors le résultat obtenu
      reponse: ['Sur l’échantillon de $calcule[£b*500]$ patients de cet hôpital, la fréquence des victimes d’une infection nosocomiale vaut $f=\\frac{calcule[£b*£a+£c]}{calcule[£b*500]}£e$.',
        'Comme $f\\notin £i$, on considère au risque d’erreur de calcule[100-£s] % que trop de patients de cet hôpital sont victimes d’infection nosocomiale et qu’ainsi les craintes du directeur sont justifiées.',
        'Comme $f\\in £i$, on considère que les craintes du directeurs ne sont pas justifiées.'],
      arrondi: 3
    },
    {
      nomSujet: 'Métropole septembre 2017 STMG', // numéro 6
      utilisableSeconde: true,
      consignes: ['Un comité d’entreprise décide de construire une structure supplémentaire pour améliorer le bien-être des salariés.',
        'Le président du comité déclare que £a % des salariés sont satisfaits de la qualité des nouvelles installations sportives.',
        'Alix mène une enquête auprès de calcule[£b*20] de ses collègues choisis au hasard. Parmi eux, calcule[(£b-£c)*15] se déclarent satisfaits des installations sportives.',
        'Les bornes étant arrondies au millième, l’intervalle de fluctuation asymptotique au seuil de £s % de la fréquence de personnes satisfaites dans un échantillon de $calcule[£b*20]$ salariés est :'],
      question: '&1&.',
      questFreq: ['Un intervalle de fluctuation au seuil de £s % de la fréquence de personnes satisfaites dans cet échantillon est £i.',
        'Peut-on, au risque d’erreur de calcule[100-£s] %, remettre en question les propos du président ?'],
      variables: [['a', '[71;79]'], ['b', '[14;20]'], ['c', '[1;4]']],
      // je récupère les valeurs de mes variables
      valeurs: { p: '£a/100', n: '£b*20', freq: '(£b-£c)*15/(£b*20)' },
      donnees: 'On est en présence d’un échantillon de taille $n=calcule[£b*20]$. La proportion de salariés satisfaits de la qualité des nouvelles installations sportives est $p=calcule[£a/100]$.',
      // On donne alors le résultat obtenu
      reponse: ['Sur l’échantillon de $calcule[£b*20]$ salariés de l’entreprise, la fréquence des salariés satisfaits des installations sportives vaut $f=\\frac{calcule[(£b-£c)*15]}{calcule[£b*20]}£e$.',
        'Comme $f\\notin £i$, on peut au risque d’erreur de calcule[100-£s] % remettre en question les propos du président.',
        'Comme $f\\in £i$, on ne peut pas remettre en cause les propos du président.'],
      arrondi: 3
    },
    {
      nomSujet: 'Pondichéry mai 2018 STMG', // numéro 7
      utilisableSeconde: false,
      consignes: ['Pour la fabrication de machines agricoles, une usine reçoit en grande quantité des plaques métalliques.',
        'Une plaque sera utilisable par l’usine si son épaisseur est inférieure à 3 millimètres.',
        'Le fournisseur affirme que £a % des plaques ont une épaisseur inférieure à 3 millimètres.',
        'On effectue le test sur calcule[£b*100] plaques et calcule[£b*88-£c] d’entre-elles le réussissent.',
        'Les bornes étant arrondies au millième, l’intervalle de fluctuation asymptotique au seuil de £s % de la fréquence de plaques dont l’épaisseur est inférieure à 3 mm dans un échantillon de $calcule[£b*100]$ plaques est :'],
      question: '&1&.',
      questFreq: ['Un intervalle de fluctuation au seuil de £s % de la fréquence de plaque d’épaisseur inférieure à 3 mm dans cet échantillon est £i.',
        'Peut-on, au risque d’erreur de calcule[100-£s] %, rejeter l’affirmation du fournisseur ?'],
      variables: [['a', '[85;93]'], ['b', '[21;28]'], ['c', '[30;52]']],
      // je récupère les valeurs de mes variables
      valeurs: { p: '£a/100', n: '£b*100', freq: '(£b*88-£c)/(£b*100)' },
      donnees: 'On est en présence d’un échantillon de taille $n=calcule[£b*100]$. La proportion de plaques dont l’épaisseur est inférieure à 3 mm est $p=calcule[£a/100]$.',
      // On donne alors le résultat obtenu
      reponse: ['Sur l’échantillon de $calcule[£b*100]$ plaques, la fréquence de celles dont l’épaisseur est inférieure à 3 mm vaut $f=\\frac{calcule[£b*88-£c]}{calcule[£b*100]}£e$.',
        'Comme $f\\notin £i$, on peut au risque d’erreur de calcule[100-£s] % rejeter l’affirmation du fournisseur.',
        'Comme $f\\in £i$, on ne peut pas rejeter l’affirmation du fournisseur.'],
      arrondi: 3
    },
    {
      nomSujet: 'Centres étrangers juin 2018 STMG', // numéro 8
      utilisableSeconde: false,
      consignes: ['Le gérant d’une entreprise de blanchisserie s’interroge sur la consommation en eau, par cycle de lavage, de ses machines.',
        'Il fait réaliser une étude par une société de conseil spécialisée dans l’accompagnement vers la transition énergétique.',
        'La société de conseil affirme au gérant que £a % des clients sont sensibles aux questions environnementales.',
        'Avant de remplacer son parc de machines, le gérant réalise un sondage auprès de calcule[10*£b] clients. Ce sondage révèle alors que, parmi eux, calcule[£b*9-£c] y sont sensibles.',
        'Les bornes étant arrondies au millième, l’intervalle de fluctuation asymptotique au seuil de £s % de la fréquence de clients sensibles aux questions environnementales dans un échantillon de $calcule[£b*10]$ clients de la blanchisserie est :'],
      question: '&1&.',
      questFreq: ['Un intervalle de fluctuation au seuil de £s % de la fréquence de clients sensibles aux questions environnementales dans cet échantillon est £i.',
        'Peut-on, au risque d’erreur de calcule[100-£s] %, remettre en cause l’affirmation de la société de conseil ?'],
      variables: [['a', '[85;93]'], ['b', '[31;43]'], ['c', '[20;35]']],
      // je récupère les valeurs de mes variables
      valeurs: { p: '£a/100', n: '£b*10', freq: '(£b*9-£c)/(£b*10)' },
      donnees: 'On est en présence d’un échantillon de taille $n=calcule[£b*10]$. La proportion de clients de la blanchisserie sensibles aux questions environnementales est $p=calcule[£a/100]$.',
      // On donne alors le résultat obtenu
      reponse: ['Sur l’échantillon de $calcule[£b*10]$ clients de la blanchisserie, la fréquence de ceux qui sont sensibles au questions environnementales vaut $f=\\frac{calcule[£b*9-£c]}{calcule[£b*10]}£e$.',
        'Comme $f\\notin £i$, on peut au risque d’erreur de calcule[100-£s] % remettre en cause l’affirmation de la société de conseil.',
        'Comme $f\\in £i$, on ne peut pas remettre en cause l’affirmation de la société de conseil.'],
      arrondi: 3
    },
    {
      nomSujet: 'Amérique du Nord mai 2018 ES', // numéro 9
      utilisableSeconde: false,
      consignes: ['Le site internet «ledislight.com» spécialisé dans la vente de matériel lumineux vend des rubans LED flexibles.',
        'Le fournisseur affirme que, parmi les rubans LED expédiés au site internet, calcule[£a/10] % sont défectueux.',
        'Le responsable internet désire vérifier la validité de cette affirmation.',
        'Dans son stock, il prélève au hasard calcule[£b*20] rubans LED parmi lesquels calcule[£b+£c] sont défectueux.',
        'Les bornes étant arrondies au millième, l’intervalle de fluctuation asymptotique au seuil de £s % de la fréquence de rubans LED défectueux dans un échantillon de $calcule[£b*20]$ rubans LED du stock est :'],
      question: '&1&.',
      questFreq: ['Un intervalle de fluctuation au seuil de £s % de la fréquence de rubans LED défectueux dans cet échantillon est £i.',
        'Peut-on, au risque d’erreur de calcule[100-£s] %, remettre en cause l’affirmation du fournisseur ?'],
      variables: [['a', '[40;55]'], ['b', '[18;26]'], ['c', '[7;15]']],
      // je récupère les valeurs de mes variables
      valeurs: { p: '£a/1000', n: '£b*20', freq: '(£b+£c)/(£b*20)' },
      donnees: 'On est en présence d’un échantillon de taille $n=calcule[£b*20]$. La proportion de rubans LED défectueux est $p=calcule[£a/1000]$.',
      // On donne alors le résultat obtenu
      reponse: ['Sur l’échantillon de $calcule[£b*20]$ rubans LED du stock, la fréquence de ceux qui sont défectueux vaut $f=\\frac{calcule[£b+£c]}{calcule[£b*20]}£e$.',
        'Comme $f\\notin £i$, on peut au risque d’erreur de calcule[100-£s] % remettre en cause l’affirmation du fournisseur.',
        'Comme $f\\in £i$, on ne peut pas remettre en cause l’affirmation du fournisseur.'],
      arrondi: 3
    },
    {
      nomSujet: 'Antilles-Guyane juin 2018 S', // numéro 10
      utilisableSeconde: true,
      consignes: ['L’exploitant d’une forêt communale affirme que les sapins représentent £a % des arbres de cette forêt.',
        'Sur une parcelle, on a compté calcule[£b*5+£c] sapins dans un échantillon de calcule[£b*10] arbres.',
        'Les bornes étant arrondies au millième, l’intervalle de fluctuation asymptotique au seuil de £s % de la fréquence de sapins dans un échantillon de $calcule[£b*10]$ arbres de cette forêt est :'],
      question: '&1&.',
      questFreq: ['Un intervalle de fluctuation au seuil de £s % de la fréquence de sapins dans cet échantillon est £i.',
        'Peut-on, au risque d’erreur de calcule[100-£s] %, remettre en cause l’affirmation de l’exploitant ?'],
      variables: [['a', '[42;58]'], ['b', '[18;26]'], ['c', '[-15;15]']],
      // je récupère les valeurs de mes variables
      valeurs: { p: '£a/100', n: '£b*10', freq: '(£b*5+£c)/(£b*10)' },
      donnees: 'On est en présence d’un échantillon de taille $n=calcule[£b*10]$. La proportion de sapins est $p=calcule[£a/100]$.',
      // On donne alors le résultat obtenu
      reponse: ['Sur l’échantillon de $calcule[£b*10]$ arbres de la forêts, la fréquence de sapins vaut $f=\\frac{calcule[£b*5+£c]}{calcule[£b*10]}£e$.',
        'Comme $f\\notin £i$, on peut au risque d’erreur de calcule[100-£s] % remettre en cause l’affirmation de l’exploitant.',
        'Comme $f\\in £i$, on ne peut pas remettre en cause l’affirmation de l’exploitant.'],
      arrondi: 3
    },
    {
      nomSujet: 'Centres étrangers juin 2018 S', // numéro 11
      utilisableSeconde: true,
      consignes: ['Un détaillant en fruits et légumes étudie l’évolution de ses ventes de melons afin de pouvoir anticiper ses commandes.',
        'Il constate que ses melons se vendent bien lorsque leur masse est comprise entre 900 g et 1200 g. Dans la suite, de tels melons sont qualifiés « conformes ».',
        'Un maraîcher affirme que £a % des melons de sa production sont conformes.',
        'Le détaillant doute de cette affirmation. Il constate que sur calcule[£b*20] melons livrés par ce maraîcher au cours d’une semaine, seulement calcule[£b*15-£c] sont conformes.',
        'Les bornes étant arrondies au millième, l’intervalle de fluctuation asymptotique au seuil de £s % de la fréquence de melons conformes dans un échantillon de $calcule[£b*20]$ melons du maraîcher est :'],
      question: '&1&.',
      questFreq: ['Un intervalle de fluctuation au seuil de £s % de la fréquence de melons conformes dans cet échantillon est £i.',
        'Peut-on, au risque d’erreur de calcule[100-£s] %, douter de l’affirmation du maraîcher ?'],
      variables: [['a', '[72;79]'], ['b', '[18;26]'], ['c', '[15;30]']],
      // je récupère les valeurs de mes variables
      valeurs: { p: '£a/100', n: '£b*20', freq: '(£b*15-£c)/(£b*20)' },
      donnees: 'On est en présence d’un échantillon de taille $n=calcule[£b*20]$. La proportion de melons conformes est $p=calcule[£a/100]$.',
      // On donne alors le résultat obtenu
      reponse: ['Sur l’échantillon de $calcule[£b*20]$ melons du maraîcher, la fréquence de melons conformes vaut $f=\\frac{calcule[£b*15-£c]}{calcule[£b*20]}£e$.',
        'Comme $f\\notin £i$, on peut au risque d’erreur de calcule[100-£s] % douter de l’affirmation du maraîcher.',
        'Comme $f\\in £i$, on ne peut pas douter de l’affirmation du maraîcher.'],
      arrondi: 3
    }
  ]
}
