export default {
  /* Dans toutes les consignes de ce type d’exercices, £a permet de déclarer la variable a. Il faut alors préciser l’intervalle dans lequel se trouve cette valeur
    Les consignes seront le plus souvent sur plusieurs lignes, d’où l’idée de créer un tableau
    la propriété variables de chaque énoncé est un tableau où chaque élément est un tableau de deux ou trois éléments
    Par exemple ['a','[5;15]'] signifie que a sera un entier de l’intervalle [5;15]
    ['a','[5;15]',100] signifie que a prend une valeur décimale de [0,05;0,15] avec 2 chiffres après la virgule
    */
  sujets: [
    {
      nomSujet: 'Pondichery avril 2018 S', // numéro 0
      freqDonnee: true,
      consignes: ['Une entreprise conditionne du sucre blanc issu de différentes exploitations en paquets de 1 kg.',
        'Le sucre extra fin est conditionné séparément dans des paquets portant le label « extra fin ».',
        'Un acheteur souhaite estimer la proportion de paquets de sucre provenant de l’exploitation U parmi les paquets portant le label « extra fin ».',
        'Il prélève calcule[£a*10] paquets pris au hasard dans la production de paquets labellisés « extra fin » de l’entreprise. Parmi ces paquets £b % contiennent du sucre provenant de l’exploitation U.',
        'Les bornes étant arrondies au millième, l’intervalle de confiance, au niveau de confiance 95 %, de la proportion de paquets de sucre labellisés « extra fin » provenant de cette exploitation U est'],
      question: '&1&.',
      variables: [['a', '[13;20]'], ['b', '[38;48]']],
      // je récupère les valeurs de mes variables
      valeurs: { f: '£b/100', n: '£a*10' },
      // On donne alors le résultat obtenu
      reponse: ['Sur l’échantillon de $calcule[£a*10]$ paquets, la fréquence de paquets de sucre labellisés « extra fin » vaut $f=\\frac{£b}{100}=calcule[£b/100]$.',
        'Au niveau de confiance de 95 %, l’intervalle de confiance de la proportion de paquets de sucre labellisés « extra fin » provenant de l’exploitation U est de la forme $[f-\\frac{1}{\\sqrt{n}};f+\\frac{1}{\\sqrt{n}}]$ à savoir environ £i en arrondissant à £j (les arrondis étant choisis de sorte qu’il contienne l’intervalle exact).',
        'Cela signifie qu’on peut considérer qu’on a 95 % de chances que la proportion de paquets de sucre labellisés « extra fin » provenant de l’exploitation U soit entre £p % et £q %.'],
      arrondi: 3
    },
    {
      nomSujet: 'Pondichery mai 2018 ES', // numéro 1
      freqDonnee: false,
      consignes: ['Un commerçant dispose dans sa boutique d’un terminal qui permet à ses clients, s’ils souhaitent régler leurs achats par carte bancaire, d’utiliser celle-ci en mode sans contact.',
        'Une enquête de satisfaction a été réalisée auprès d’un échantillon de calcule[£a*20] clients de cette boutique.',
        'Parmi eux, calcule[£a*£b] trouvent que le dispositif sans contact du terminal est pratique.',
        'Les bornes étant arrondies au millième, l’intervalle de confiance, au niveau de confiance 95 %, de la proportion de clients de cette boutique qui trouvent que le dispositif sans contact du terminal est pratique est'],
      question: '&1&.',
      variables: [['a', '[8;16]'], ['b', '[14;17]']],
      // je récupère les valeurs de mes variables
      valeurs: { f: '£b/20', n: '£a*20' },
      // On donne alors le résultat obtenu
      reponse: ['Sur l’échantillon de $calcule[£a*20]$ clients de cette boutique, la fréquence de clients qui trouvent que le dispositif sans constact du terminal est pratique vaut $f=\\frac{calcule[£a*£b]}{calcule[£a*20]}=calcule[£b/20]$.',
        'Au niveau de confiance de 95 %, l’intervalle de confiance de la proportion de clients de cette boutique qui trouvent que le dispositif sans contact du terminal est pratique est de la forme $[f-\\frac{1}{\\sqrt{n}};f+\\frac{1}{\\sqrt{n}}]$ à savoir environ £i en arrondissant à £j (les arrondis étant choisis de sorte qu’il contienne l’intervalle exact).',
        'Cela signifie qu’on peut considérer qu’on a 95 % de chances que la proportion des clients de la boutique trouvant pratique le dispositif sans contact du terminal soit entre £p % et £q %.'],
      arrondi: 3
    },
    {
      nomSujet: 'Amérique du Nord mai 2018 ES', // numéro 2
      freqDonnee: false,
      consignes: ['Le site internet « ledislight.com » spécialisé dans la vente de matériel lumineux vend des rubans LED flexibles d’intérieur.',
        'Le fournisseur n’a donné aucune information concernant la fiabilité de ces rubans LED.',
        'Le directeur du site souhaite estimer la proportion de ces rubans LED défectueux. Pour cela, il prélève un échantillon aléatoire de calcule[£a*200] rubans parmi lesquels calcule[£a*20+£b*3] sont défectueux.',
        'Les bornes étant arrondies au millième, l’intervalle de confiance, au niveau de confiance 95 %, de la proportion de rubans LED flexibles d\\\'intérieur défectueux est'],
      question: '&1&.',
      variables: [['a', '[1;3]'], ['b', '[-2;4]']],
      // je récupère les valeurs de mes variables
      valeurs: { f: '(£a*20+£b*3)/(£a*200)', n: '£a*200' },
      // On donne alors le résultat obtenu
      reponse: ['Sur l’échantillon de $calcule[£a*200]$ rubans LED flexibles, la fréquence de rubans défectueux vaut $f=\\frac{calcule[£a*20+£b*3]}{calcule[£a*200]}=calcule[(£a*20+£b*3)/(£a*200)]$.',
        'Au niveau de confiance de 95 %, l’intervalle de confiance de la proportion de rubans LED flexibles d’intérieur défectueux est de la forme $[f-\\frac{1}{\\sqrt{n}};f+\\frac{1}{\\sqrt{n}}]$ à savoir environ £i en arrondissant à £j (les arrondis étant choisis de sorte qu’il contienne l’intervalle exact).',
        'Cela signifie qu’on peut considérer qu’on a 95 % de chances que la proportion de rubans LED flexibles d’intérieur défectueux soit entre £p % et £q %.'],
      arrondi: 3
    },
    {
      nomSujet: 'Amérique du Nord juin 2017 ES', // numéro 3
      freqDonnee: false,
      consignes: ['Une entreprise fabrique des tubes métalliques de longueur 2 m.',
        'Un tube métallique est considéré comme étant dans la norme si sa longueur est comprise entre 1,98 m et 2,02 m. On prélève au hasard un échantillon de calcule[£a*1000] tubes, on observe que calcule[£a*£b] tubes sont dans la norme.',
        'Les bornes étant arrondies au millième, l’intervalle de confiance, au niveau de confiance 95 %, de la proportion de tubes dans la norme pour cette entreprise est'],
      question: '&1&.',
      variables: [['a', '[8;12]'], ['b', '[930;955]']],
      // je récupère les valeurs de mes variables
      valeurs: { f: '£b/1000', n: '£a*1000' },
      // On donne alors le résultat obtenu
      reponse: ['Sur l’échantillon de $calcule[£a*1000]$ tubes de cette entreprise, la fréquence de tubes dans la norme vaut $f=\\frac{calcule[£a*£b]}{calcule[£a*1000]}=calcule[£b/1000]$.',
        'Au niveau de confiance de 95 %, l’intervalle de confiance de la proportion de tubes dans la norme est de la forme $[f-\\frac{1}{\\sqrt{n}};f+\\frac{1}{\\sqrt{n}}]$ à savoir environ £i en arrondissant à £j (les arrondis étant choisis de sorte qu’il contienne l’intervalle exact).',
        'Cela signifie qu’on peut considérer qu’on a 95 % de chances que la proportion de tubes dans la norme soit entre £p % et £q %.'],
      arrondi: 3
    },
    {
      nomSujet: 'Candidats ayant repassé le bac juin 2017 ES', // numéro 4
      freqDonnee: false,
      consignes: ['Un institut de sondage réalise une enquête afin de mesurer le degré de satisfaction du service après-vente d’une société. ',
        'Cette première étude portant sur un échantillon aléatoire de calcule[£a*50] clients révèle que l’on dénombre calcule[£a*£b] clients satisfaits.',
        'Les bornes étant arrondies au millième, l’intervalle de confiance, au niveau de confiance 95 %, de la proportion de clients satisfaits est'],
      question: '&1&.',
      variables: [['a', '[9;12]'], ['b', '[41;45]']],
      // je récupère les valeurs de mes variables
      valeurs: { f: '£b/50', n: '£a*50' },
      // On donne alors le résultat obtenu
      reponse: ['Sur l’échantillon de $calcule[£a*50]$ clients de cette société, la fréquence de clients satisfaits vaut $f=\\frac{calcule[£a*£b]}{calcule[£a*50]}=calcule[£b/50]$.',
        'Au niveau de confiance de 95 %, l’intervalle de confiance de la proportion de clients satisfaits est de la forme $[f-\\frac{1}{\\sqrt{n}};f+\\frac{1}{\\sqrt{n}}]$ à savoir environ £i en arrondissant à £j (les arrondis étant choisis de sorte qu’il contienne l’intervalle exact).',
        'Cela signifie qu’on peut considérer qu’on a 95 % de chances que la proportion de clients satisfaits soit entre £p % et £q %.'],
      arrondi: 3
    },
    {
      nomSujet: 'Amérique du Sud novembre 2017 ES', // numéro 5
      freqDonnee: true,
      consignes: ['Une entreprise d’élevage de poissons en bassin a constaté qu’une partie de sa production est infectée par une nouvelle bactérie.',
        'Un laboratoire préleve au hasard calcule[£a*100] poissons parmi l’ensemble des poissons du bassin.',
        'La fréquence de poissons infectés par la bactérie dans cet échantillon est de £b %.',
        'Les bornes étant arrondies au millième, l’intervalle de confiance, au niveau de confiance 95 %, de la proportion de poissons infectés est'],
      question: '&1&.',
      variables: [['a', '[9;12]'], ['b', '[6;15]']],
      // je récupère les valeurs de mes variables
      valeurs: { f: '£b/100', n: '£a*100' },
      // On donne alors le résultat obtenu
      reponse: ['Sur l’échantillon de $calcule[£a*100]$ poissons du bassin, la fréquence de poissons infectés vaut $f=\\frac{£b}{100}=calcule[£b/100]$.',
        'Au niveau de confiance de 95 %, l’intervalle de confiance de la proportion de poissons infectés est de la forme $[f-\\frac{1}{\\sqrt{n}};f+\\frac{1}{\\sqrt{n}}]$ à savoir environ £i en arrondissant à £j (les arrondis étant choisis de sorte qu’il contienne l’intervalle exact).',
        'Cela signifie qu’on peut considérer qu’on a 95 % de chances que la proportion de poissons infectés soit entre £p % et £q %.'],
      arrondi: 3
    },
    {
      nomSujet: 'Nouvelle Calédonie novembre 2017 ES', // numéro 6
      freqDonnee: false,
      consignes: ['On a prélevé un échantillon aléatoire de calcule[£a*100] pièces dans une production et observé calcule[£a*£b] pièces défectueuses.',
        'Les bornes étant arrondies au millième, l’intervalle de confiance, au niveau de confiance 95 %, de la proportion de pièces défectueuses est'],
      question: '&1&.',
      variables: [['a', '[5;9]'], ['b', '[2;5]']],
      // je récupère les valeurs de mes variables
      valeurs: { f: '£b/100', n: '£a*100' },
      // On donne alors le résultat obtenu
      reponse: ['Sur l’échantillon de $calcule[£a*100]$ pièces de la production, la fréquence de pièces défectueuses vaut $f=\\frac{£b}{100}=calcule[£b/100]$.',
        'Au niveau de confiance de 95 %, l’intervalle de confiance de la proportion de pièces défectueuses est de la forme $[f-\\frac{1}{\\sqrt{n}};f+\\frac{1}{\\sqrt{n}}]$ à savoir environ £i en arrondissant à £j (les arrondis étant choisis de sorte qu’il contienne l’intervalle exact).',
        'Cela signifie qu’on peut considérer qu’on a 95 % de chances que la proportion de pièces défectueuses soit entre £p % et £q %.'],
      arrondi: 3
    }
  ]
}
